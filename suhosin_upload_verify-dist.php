#!/usr/bin/php
<?php
/**
 * Upload verification script called by Suhosin patch on every file upload.
 * (http://www.hardened-php.net/suhosin/index.html)
 * 
 * It can be placed anywhere, preferable outside web root.
 * 
 * Requirements:
 *   -- Suhosin patch must be installed as part of the Apache installation
 *   -- php5_suhosin extension must be installed
 *   -- The following options must be set in /etc/php5/conf.d/suhosin.ini   (Ubuntu):
 *      suhosin.filter.action = "402"     
 *      suhosin.upload.verification_script="/path/to/this/script"
 *   -- Clamav virus scanner must be installed and running (see CLAM_* defines bellow)
 *      (http://www.clamav.net/)   
 *      
 */

// Set to false to disabe checks
define('ENABLE', false);

define('CLAM_PORT',3310);
define('CLAM_HOST','localhost');

define ('MAX_STREAMSIZE', 838860800);
define ('STRICT', true);

define('LOG_FILE','/tmp/suhosin_upload_verify.log');
define('LOG',true);


global $errorMessage;

/**
 * Helper function to keep some log in /tmp
 * @param string $string
 */
function logfile($string) {
	if (LOG) {
		file_put_contents(LOG_FILE, "$string\n", FILE_APPEND);
	}
}


/**
 * Check if the uploaded file is an archive and then check it's content for files with BAD
 * extension.
 * 
 * @param string $fullFilePath
 * @return boolean
 */
function checkArchiveContent($fullFilePath) {
	return true;
}


/**
 * Connect to a virus scanner daemon and scan the file
 * @param string $file  The file just uploaded in PHP temporary uploading folder
 * @return boolean
 */
function virusScanner($fullFilePath) {
	
	global $errorMessage;
	
	logfile("Upload Verify Virus scanner, file: " . $fullFilePath);
	
	$port = CLAM_PORT;
	
	$chunksize = (8 * (1024 * 1024)) - 1;
	
	if (!is_readable($fullFilePath)) {
		return false;
	}
	
	if (filesize($fullFilePath) >= MAX_STREAMSIZE) {
		return false;
	}
	
	if (($f = @fopen($fullFilePath, 'rb')) !== false) {
		//logfile(1);
		ob_start();
		if ($socket = @fsockopen(CLAM_HOST, CLAM_PORT, $errno, $errstr, 5)) {
			//logfile(2);
			@stream_set_timeout($socket, 120);
			@fwrite($socket, "STREAM\n");
			if (!feof($socket)) {
				//logfile(3);
				$port = trim(fgets($socket, 128));
			}
			if (preg_match("/PORT ([\d]{1,5})$/", $port, $matches)) {
				//logfile(4);
				$port = $matches[1];
			}
			ob_end_flush();
			//logfile(5);
			if ($scanner = @fsockopen(CLAM_HOST, $port, $errno, $errstr, 5)) {
				//logfile(6);
				$i = 0;
				while (!feof($f)) {
					//logfile(66);
					$dataChunk = @fread($f, $chunksize);
					//logfile(67);
					@fwrite($scanner, $dataChunk);
				}
				@fclose($scanner);
				@fclose($f);
				$result = '';
				while (!feof($socket)) {
					$result .= @fgets($socket);
				}
				@fclose($socket);
	
				if (preg_match("/: (.*) FOUND.*$/", $result, $found)) {
					//logfile(7);
					$errorMessage = 'Malware detected in file: ' . $found[1];
					return false;
				}
				elseif (preg_match("/: OK.*$/", $result)) {
					//logfile(8);
					return true;
				}
				else {
					//logfile(9);
					$errorMessage = 'Can not scan the file [1]: ' . $fullFilePath;
					return !STRICT;
				}
			}
		}
		else {
			//logfile(10);
			$errorMessage = 'Can not scan the file [2]: ' . $fullFilePath;
			return !STRICT;
		}
	}
	else {
		//logfile(11);
		$errorMessage = 'File is unreadable: ' . $fullFilePath;
		return false;
	}
	
	
	return true;
}


/**
 * Do a mime check against whie/black lists
 * @param string $file The file just uploaded in PHP temporary uploading folder
 * @return boolean
 */
function mimeCheck($fullFilePath) {
	
	$blackList = array(

	);
	
	$whiteList = array(
		
	);
	
	
	logfile("Upload Verify Mime Check: ");
	$resourse = finfo_open(FILEINFO_MIME_TYPE + FILEINFO_CONTINUE );
	$fileInfo = finfo_file($resourse, $fullFilePath);

	logfile("File Info:" . print_r($fileInfo,true));
	
	if (in_array($fileInfo, $whiteList)) {
		return true;
	}
	
	
	if (in_array($fileInfo, $blackList)) {
		return false;
	}
	
	
	return true;
}


/**
 * Send output to STDOUT
 * @param number $result  0 or 1 (as per Suhosin requirements)
 */
function out($result) {
	if ($stdout = fopen("php://stdout", "w")) {
		fwrite($stdout, $result);
		fwrite($stdout, "\n");
		flush(); 
		fclose($stdout);
	}
}
/** RUN ****************************************/

$start = microtime(true);

if (!(ENABLE == true)) {
	out(1);
}

logfile("Upload Verify Started");

// Get temporarty file; like  /tmp/phpYhduyTghd
$file = isset($argv[1]) ? $argv[1] : false;
if ($file === false) {
	out(1);
}


// Call checks; all must succeeed
$scanResult = virusScanner($file);
$mimeCheckResult = mimeCheck($file);

logfile(microtime(true)-$start);

if (!$scanResult || !$mimeCheckResult) {
	//logfile(12);
	logfile($errorMessage);
	out(0);
	exit;
}


out(1);
exit;

?>
