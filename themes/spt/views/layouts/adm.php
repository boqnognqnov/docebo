<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, target-densitydpi=160dpi, minimal-ui">
	<!-- ,user-scalable=no - for safari bug -->
	<link rel="shortcut icon" href="<?php echo $this->faviconUrl; ?>" type="image/png" />

	<?php $this->widget("common.widgets.CloudfrontCookieBaker"); ?>

	<!-- Sprites (i-sprite, player-sprite) -->
	<?php DoceboAssetMerger::publishAndRender(array('/css/i-sprite.css', '/css/player-sprite.css'), true) ?>

	<!-- Application -->
	<?php
		preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);

		if(count($matches)<2)
		{
			preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
		}

		$jquery = 'jquery.js';
		$jquery_min = 'jquery.min.js';

		if (count($matches)>1)
		{
			$version = $matches[1];

			if($version == 8)
			{
				$jquery = 'jquery.old.js';
				$jquery_min = 'jquery.old.min.js';
			}
		}
	?>

	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/<?=YII_DEBUG ? $jquery : $jquery_min?>"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/<?=YII_DEBUG ? 'jquery-migrate.js' : 'jquery-migrate.min.js'?>"></script>


	<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/js/app.js?70"></script>

	<!-- base js -->
	<?php DoceboAssetMerger::publishAndRender(array(
		'/js/formstyler/jquery.formstyler.css',
		'/js/jscrollpane/jquery.jscrollpane.css',
		'/js/Jcrop/jquery.Jcrop.min.css',
	), true, DoceboAssetMerger::$TARGET_ASSETS_FOLDER) ?>
    <link href="<?php echo Yii::app()->theme->baseUrl ?>/css/print.css" rel="stylesheet" media="print">


    <?php

	$min = (YII_DEBUG ? '' : '.min');
    $enable_legacy_menu = Settings::get('enable_legacy_menu', 'on');
    $cssToMerge = '';
    if ($enable_legacy_menu == 'off') {
	    $cssToMerge = '/css/new-menu.css';
    }

    if (Lang::isRTL(Yii::app()->getLanguage())): ?>
		<!-- RTL styles -->
	    <?php DoceboAssetMerger::publishAndRender(array(
	    	'/css/bootstrap-rtl.min.css',
		    '/css/bootstrap-responsive-rtl.min.css',
			'/css/material-design-iconic-font.min.css',
	    ), true, DoceboAssetMerger::$TARGET_THEME_ASSETS_FOLDER) ?>

		 <?php DoceboAssetMerger::publishAndRender(array(
	    	'/js/confirm/jquery.confirm.css',
	    	'/css/admin.css',
			"/css/common$min.css",
			'/css/rtl.css',
			$cssToMerge
	    ), true, DoceboAssetMerger::$TARGET_THEME_ASSETS_FOLDER) ?>

	<?php else: ?>
	    <?php
			DoceboAssetMerger::publishAndRender(array(
				"/css/common$min.css",
				'/css/material-design-iconic-font.min.css',
				'/js/confirm/jquery.confirm.css'
			), true, DoceboAssetMerger::$TARGET_THEME_ASSETS_FOLDER);

			DoceboAssetMerger::publishAndRender(array(
				'/css/admin.css',
				$cssToMerge
			), true, DoceboAssetMerger::$TARGET_THEME_ASSETS_FOLDER);
		?>
	<?php endif; ?>

    <!-- template color scheme -->
	<link href="<?php echo Docebo::createAppUrl('admin:setup/colorschemeCss', array('time' => time())) ?>" rel="stylesheet" id="colorSchemeCssFile">

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/ie.css" rel="stylesheet">
		<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/html5.js"></script>
	<![endif]-->

	<!--[if lt IE 8]>
		<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/ie6.css" rel="stylesheet">
	<![endif]-->

	<!--[if lt IE 7]>
		<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/json2-for-ie7.js"></script>
	<![endif]-->

	<!--[if (lt IE 9) & (!IEMobile)]>
		<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/respond.min.js"></script>
	<![endif]-->

	<script type="text/javascript">var HTTP_HOST = '<?php echo Yii::app()->request->getBaseUrl(true).'/'; ?>';</script>
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/formstyler/jquery.formstyler.js"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/confirm/jquery.confirm.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/jscrollpane/jquery.jscrollpane.js"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/jscrollpane/jquery.mousewheel-3.0.4.js"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/menu.js"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/Jcrop/jquery.Jcrop.min.js"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/jquery.hashchange.min.js"></script>
	<!-- "admin" Yii app ?  -->
	<?php if (Yii::app()->name === 'admin') : ?>
		<script src="<?php echo Yii::app()->baseUrl ?>/js/script.js"></script>
		<script src="<?php echo Yii::app()->baseUrl ?>/js/scriptModal.js"></script>
		<script type="text/javascript">
			//<![CDATA[
			var yiiLang = {
				standardFilledLanguages: '<?= Yii::t('standard', 'Filled languages') ?>'
			};
			//]]>
		</script>
		<script src="<?php echo Yii::app()->baseUrl ?>/js/scriptTur.js"></script>
		<script src="<?php echo Yii::app()->baseUrl ?>/js/admin.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/scriptModal.js"></script>
	<?php endif; ?>


	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/app.js?70"></script>

	<!-- Open sans font -->
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<?= DoceboUI::custom_css(); ?>

	<!-- EVENT beforeHtmlHeadTagClose -->
	<?php Yii::app()->event->raise('beforeHtmlHeadTagClose', new DEvent($this, array())); ?>

	<?php Yii::app()->legacyWrapper->applyJsGuard(); ?>
</head>
<body class="docebo">

<?php Yii::app()->event->raise('AfterBodyTagOpen', new DEvent($this, array())); ?>
<?php
	$menuRenderer = 'common.widgets.MainMenu';
	if(Yii::app()->event->raise('getMenuRenderer', new DEvent($this, array('renderer'=>&$menuRenderer)))) {
		$this->widget( $menuRenderer, array(// no option at this point
		) );
	}
?>
<div id="wrapper">
	<div id="maincontent" class="container<?= property_exists(Yii::app()->controller, "useFluidContainer") && Yii::app()->controller->useFluidContainer ? "-fluid" : "" ?> clearfix">
		<div class="menu-spacer">
			<?php if (Yii::app()->event->raise('BeforeHeaderRender', new DEvent($this, array()))) : ?>
				<header id="header">
					<?php $this->widget('common.widgets.SiteHeader', array('headerType' => 'admin')); ?>
				</header>
			<?php endif; ?>
			<!-- <div id="dcb-feedback"></div> -->
			<?php /*
			$this->widget('zii.widgets.CBreadcrumbs', array(
				'links' => $this->breadcrumbs,
				'homeLink' => '<a href="' .Docebo::createAbsoluteLmsUrl('site/index'). '" class="home">Home</a>' ,
				'separator' => '<span class="arrow"></span>',
			)); */ ?>
			<div id="navigation">
				<?php
				// print the breadcrumbs
				$renderer = 'DBreadcrumbs';

				// Use $event->preventDefault() to avoid rendering the breadcrumbs at all
				// or override the $renderer with a path to the class/widget that is the
				// custom implementation of 'CBreadcrumbs'
				if(Yii::app()->event->raise('getBreadcrumbsRenderer', new DEvent($this, array(
					'renderer'=>&$renderer,
				)))) {
					$this->widget( $renderer, array(
						'links'                => $this->breadcrumbs,
						'tagName'              => 'ul',
						'htmlOptions'          => array( 'class' => 'breadcrumb' ),
						'homeLink'             => CHtml::link( '', Docebo::createAbsoluteLmsUrl( 'site/index' ) ),
						'activeLinkTemplate'   => '<a href="{url}">{label}</a>',
						'inactiveLinkTemplate' => ' <li class="active">{label}</li>',
						'separator'            => ' <span class="divider">&rsaquo;</span>',
					) );
				}?>
			</div>
			<div id="dcb-feedback"><?php DoceboUI::printFlashMessages(); ?></div>
			<div id="content">
				<?php echo $content; ?>
			</div>
			<?php if (Yii::app()->event->raise('BeforeFooterRender', new DEvent($this, array()))) : ?>
				<footer id="footer">
					<?php $this->widget('common.widgets.SitePoweredBy'); ?>
				</footer>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php Docebo::checkPlatformValidity(); ?>
<?php Docebo::checkReinsertCCData();?>
<div id="load-trigger">
	<img src="data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw%3D%3D"></div>

    <?php Yii::app()->event->raise('BeforeBodyTagClose', new DEvent($this, array())); ?>
</body>
</html>

<?php $senderEmail = Yii::app()->user->getState('senderEmail');
if($senderEmail == 1 && Yii::app()->user->getIsGodadmin()) { ?>
    <script>Docebo.Feedback.show('error', '<?= Yii::t('app', 'Please enter') . ' ' . Yii::t('configuration', '_MAIL_SENDER'); ?>');</script>
<?php }
Yii::app()->user->setState('senderEmail', null); ?>
