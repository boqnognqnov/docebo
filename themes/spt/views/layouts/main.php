<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, target-densitydpi=160dpi, minimal-ui">
	<!-- ,user-scalable=no - for safari bug -->
	<link rel="shortcut icon" href="<?php echo $this->faviconUrl; ?>">

	<?php $this->widget("common.widgets.CloudfrontCookieBaker"); ?>

	<!-- base js -->
	<?php
		preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);

		if(count($matches)<2)
		{
			preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
		}

		$jquery = 'jquery.js';
		$jquery_min = 'jquery.min.js';

		if (count($matches)>1)
		{
			$version = $matches[1];

			if($version == 8)
			{
				$jquery = 'jquery.old.js';
				$jquery_min = 'jquery.old.min.js';
			}
		}
	?>

	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/<?=YII_DEBUG ? $jquery : $jquery_min?>"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/<?=YII_DEBUG ? 'jquery-migrate.js' : 'jquery-migrate.min.js'?>"></script>

	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/app.js?70"></script>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<!-- i-sprite Icons -->
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/i-sprite.css" rel="stylesheet">
	<!-- p-sprite Icons -->
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/player-sprite.css" rel="stylesheet">

	<!-- Combined CSS, common for all layouts -->
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/common.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/material-design-iconic-font.min.css" rel="stylesheet">

	<link href="<?php echo Yii::app()->createUrl('site/colorschemeCss', array('time' => time())) ?>" rel="stylesheet" id="colorSchemeCssFile">

	<? if(Lang::isRTL(Yii::app()->getLanguage())): ?>
		<!-- RTL styles -->
		<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/bootstrap-rtl.min.css" rel="stylesheet">
		<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/bootstrap-responsive-rtl.min.css" rel="stylesheet">
		<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/rtl.css" rel="stylesheet">
	<? endif; ?>

	<?php
	$enable_legacy_menu = Settings::get('enable_legacy_menu', 'on');
	if ($enable_legacy_menu == 'off') { ?>
		<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/new-menu.css" rel="stylesheet">
	<?php } ?>

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/ie.css" rel="stylesheet">
		<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/html5.js"></script>
	<![endif]-->

	<!--[if lt IE 8]>
		<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/ie6.css" rel="stylesheet">
	<![endif]-->

	<!--[if (lt IE 9) & (!IEMobile)]>
		<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/respond.min.js"></script>
	<![endif]-->

	<!-- Open sans font -->
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<?= DoceboUI::custom_css(); ?>
	<?php Yii::app()->legacyWrapper->applyJsGuard(); ?>
</head>
<body class="">
	<div id="wrapper">
		<div id="maincontent" class="container">
			<header id="header">
				<?php $this->widget('common.widgets.SiteHeader'); ?>
			</header>
			<div id="dcb-feedback"><?php DoceboUI::printFlashMessages(); ?></div>
			<?php echo $content; ?>
		</div>
	</div>
	<footer id="footer">

        <?php if( !PluginManager::isPluginActive('WhitelabelApp') ) : ?>
            <?php $currentLanguage = Yii::app()->getLanguage(); ?>
            <?php ($currentLanguage == 'it' ? $it = 'it/' : $it = ''); ?>
            <?php ($currentLanguage != 'it' && $currentLanguage != 'en' ? $int = '-international' : $int = ''); ?>

            <p class="privacy-policy" style="display: inline; margin-right: 22px;">
                <a href="https://www.docebo.com/<?=$it?>docebo-privacy-policy<?=$int?>/" target="_blank">
                    <?=Yii::t('standard', 'Privacy Policy')?>
                </a>
            </p>
        <?php endif; ?>

		<div class="container-fluid" style="display: inline">
			<p class="powered-by">
				<a href="http://www.docebo.com/" onclick="window.open(this.href);return false;">Powered by Docebo &reg; Cloud</a>
			</p>
		</div>
	</footer>
	<div id="load-trigger"><img src="data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw%3D%3D"></div>
</body>
</html>

<?php $senderEmail = Yii::app()->user->getState('senderEmail');
if($senderEmail == 1 && Yii::app()->user->getIsGodadmin()) { ?>
    <script>Docebo.Feedback.show('error', '<?= Yii::t('app', 'Please enter') . ' ' . Yii::t('configuration', '_MAIL_SENDER'); ?>');</script>
<?php }
Yii::app()->user->setState('senderEmail', null); ?>
