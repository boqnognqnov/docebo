<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, target-densitydpi=160dpi, minimal-ui">
	<!-- ,user-scalable=no - for safari bug -->
	<link rel="shortcut icon" href="<?php echo $this->faviconUrl; ?>">

	<?php $this->widget("common.widgets.CloudfrontCookieBaker"); ?>

	<!-- base js -->
	<?php
		preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);

		if(count($matches)<2)
		{
			preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
		}

		$jquery = 'jquery.js';
		$jquery_min = 'jquery.min.js';

		if (count($matches)>1)
		{
			$version = $matches[1];

			if($version == 8)
			{
				$jquery = 'jquery.old.js';
				$jquery_min = 'jquery.old.min.js';
			}
		}
	?>

	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/<?=YII_DEBUG ? $jquery : $jquery_min?>"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/<?=YII_DEBUG ? 'jquery-migrate.js' : 'jquery-migrate.min.js'?>"></script>

	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/app.js?70"></script>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<!-- Combined CSS, common for all layouts -->
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/common.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/material-design-iconic-font.min.css" rel="stylesheet">

	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/login.css" rel="stylesheet">

	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/public.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/public-responsive.css" rel="stylesheet">

	<link href="<?php echo Yii::app()->createUrl('site/colorschemeCss', array('time' => time())) ?>" rel="stylesheet" id="colorSchemeCssFile">

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/ie.css" rel="stylesheet">
		<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/html5.js"></script>
	<![endif]-->

	<!--[if lt IE 8]>
		<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/ie6.css" rel="stylesheet">
	<![endif]-->

	<!--[if (lt IE 9) & (!IEMobile)]>
		<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/respond.min.js"></script>
	<![endif]-->

	<?php $this->widget('common.widgets.Vivocha'); ?>
	<?php $this->widget('common.widgets.GoogleAnalytics'); ?>

	<!-- Open sans font -->
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<?= DoceboUI::custom_css(); ?>

	<!-- EVENT beforeHtmlHeadTagClose -->
	<?php Yii::app()->event->raise('beforeHtmlHeadTagClose', new DEvent($this, array())); ?>

	<?php Yii::app()->legacyWrapper->applyJsGuard(true); ?>

</head>
<body id="loginLayout" class="">
<?php Yii::app()->event->raise('BeforeSiteHeaderRender', new DEvent($this, array())); ?>
<div id="loginBlock" style="">

	<div class="centered">
		<a class="header-logo" href="<?= Yii::app()->getBaseUrl() ?>">
			<?php echo CHtml::image(Yii::app()->theme->getLogoUrl(), Yii::t('login', '_HOMEPAGE')); ?>
		</a>
		<?php echo $content; ?>
		<div class="loginFooter">
			<?php $this->widget('common.widgets.SitePoweredBy'); ?>
		</div>
	</div>

</div>
<script type="text/javascript">
	$(function(){
		prepareContentHeight();
		$("#login_userid").focus();

		if ($("#loginRightCnt").length)
		{
			$("#loginRightCnt").css('height', ($("#lms-home-image").height()+2)+'px');
			$("#rightLoginWrapper").css('height', ($("#lms-home-image").height()+2)+'px');
		}
	})

	var resizeEventCalled = false; // used to avoid stacking events (and high CPU load)
	function prepareContentHeight(minimizeMargins) {
		var margins = ($('#loginBlock .centered').parent().height()- $('#loginBlock .centered').outerHeight())/2;
		$("#loginLayout").css("overflow",'auto');
		if(margins<0){
			$(".centered").css('max-width', 'none');
			margins=50;
		}else{
			$(".centered").css('max-width', '98%')
		}


		// margin-bottom is needed for iPads!!!
		$('#loginBlock .centered').css('margin-top', margins).css('margin-bottom', margins);
		resizeEventCalled = false;

		$(window).on('resize', function(){
			if(!resizeEventCalled){
				setTimeout(function(){
					prepareContentHeight(true);
				},200);
				resizeEventCalled = true;
			}
		});
	}

	function showPage(id)
	{
		$('body').css({overflow: 'auto'});
		$("#pagesContent").show();
		$('.pages').hide();
		$('#page-'+id).show();
		$('#loginPages li').removeClass('active');
		$('#btn-page-'+id).addClass('active');
	}
</script>

<?php Yii::app()->event->raise('loginPageBeforeBodyTagClose', new DEvent($this, array())); ?>
</body>
</html>
