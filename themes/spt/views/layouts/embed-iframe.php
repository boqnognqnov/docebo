<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, target-densitydpi=160dpi, minimal-ui">
	<!-- ,user-scalable=no - for safari bug -->
	<link rel="shortcut icon" href="<?php echo $this->faviconUrl; ?>" type="image/png" />

	<?php $this->widget("common.widgets.CloudfrontCookieBaker"); ?>

	<!-- base js -->
	<script type="text/javascript">var HTTP_HOST = '<?php echo Yii::app()->request->getBaseUrl(true).'/'; ?>';</script>

	<?php
		preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);

		if(count($matches)<2)
		{
			preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
		}

		$jquery = 'jquery.js';
		$jquery_min = 'jquery.min.js';

		if (count($matches)>1)
		{
			$version = $matches[1];

			if($version == 8)
			{
				$jquery = 'jquery.old.js';
				$jquery_min = 'jquery.old.min.js';
			}
		}
	?>

	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/<?=YII_DEBUG ? $jquery : $jquery_min?>"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/<?=YII_DEBUG ? 'jquery-migrate.js' : 'jquery-migrate.min.js'?>"></script>

	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/app.js?70"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/menu.js"></script>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php
		$cssToMerge = array();
		if (Lang::isRTL(Yii::app()->getLanguage())) {
			$cssToMerge[] = '/css/bootstrap-rtl.min.css';
			$cssToMerge[] = '/css/bootstrap-responsive-rtl.min.css';
//			$cssToMerge[] = '/css/rtl.css';  Commented out because breaks final merged file for some reason.
		}
		//$cssToMerge[] = '/css/admin.css';
		$cssToMerge[] = '/css/i-sprite.css';
		$cssToMerge[] = '/css/player-sprite.css';

		DoceboAssetMerger::publishAndRender($cssToMerge, true, DoceboAssetMerger::$TARGET_THEME_ASSETS_FOLDER);

		$cssToMerge = array();

		$cssToMerge[] = '/css/common.'. (YII_DEBUG ? '' : 'min.') . 'css';
		$cssToMerge[] = '/css/material-design-iconic-font.min.css';

		$enable_legacy_menu = Settings::get('enable_legacy_menu', 'on');
		if ($enable_legacy_menu == 'off') {
			$cssToMerge[] = '/css/new-menu.css';
		}


		DoceboAssetMerger::publishAndRender($cssToMerge, true, DoceboAssetMerger::$TARGET_THEME_ASSETS_FOLDER);
	?>
    <?php if(Lang::isRTL(Yii::app()->getLanguage())) { ?>
    <link href="<?= Yii::app()->theme->baseUrl ?>/css/rtl.css" rel="stylesheet">
    <?php } ?>
    <!-- template color scheme -->
	<link href="<?php echo Docebo::createLmsUrl('site/colorschemeCss', array('time' => time())) ?>" rel="stylesheet" id="colorSchemeCssFile">

    <link href="<?php echo Yii::app()->theme->baseUrl ?>/js/formstyler/jquery.formstyler.docebo.css" rel="stylesheet">
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/formstyler/jquery.formstyler.min.js"></script>

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/ie.css" rel="stylesheet">
		<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/html5.js"></script>
	<![endif]-->

	<!--[if lt IE 8]>
		<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/ie6.css" rel="stylesheet">
	<![endif]-->

	<!--[if (lt IE 9) & (!IEMobile)]>
		<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/respond.min.js"></script>
	<![endif]-->

	<!-- Open sans font -->
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<?= DoceboUI::custom_css(); ?>

	<!-- EVENT beforeHtmlHeadTagClose -->
	<?php Yii::app()->event->raise('beforeHtmlHeadTagClose', new DEvent($this, array())); ?>

	<!--[if IE 8]>
		<style>
			#player-quicknav-carousel .player-quicknav-object .object-data > span {margin-top:-8px\9\0;}
		</style>
	<![endif]-->
	<?php Yii::app()->legacyWrapper->applyJsGuard(); ?>
	
	
</head>
<body class="docebo embed-iframe">

    <?php Yii::app()->event->raise('AfterBodyTagOpen', new DEvent($this, array())); ?>
	<?php
	$menuRenderer = 'common.widgets.MainMenu';
	if(Yii::app()->event->raise('getMenuRenderer', new DEvent($this, array('renderer'=>&$menuRenderer)))) {
		$this->widget( $menuRenderer, array(// no option at this point
		) );
	} ?>
	<div class="container">
		<div class="menu-spacer">
			<?php if (Yii::app()->event->raise('BeforeHeaderRender', new DEvent($this, array()))) : ?>
				<header id="header">
					<?php $this->widget('common.widgets.SiteHeader'); ?>
				</header>
			<?php endif; ?>
			<?php
			$this->widget('common.widgets.UIOrientation', array(
				// no option at this point
			));
			?>
			<div id="maincontent" class="inner-container">
				<div id="dcb-feedback"><?php DoceboUI::printFlashMessages(); ?></div>
				<div id="content">
					<!-- Course content -->
					<?php echo $content; ?>
					<div class="clearfix"></div>
				</div>
			</div>
			<?php if (Yii::app()->event->raise('BeforeFooterRender', new DEvent($this, array()))) : ?>
			<footer id="footer">
				<?php $this->widget('common.widgets.SitePoweredBy'); ?>
			</footer>
			<?php endif; ?>
		</div>
	</div>
	<!-- Widgets -->
	<?php $this->widget('common.widgets.Vivocha'); ?>
	<?php $this->widget('common.widgets.GoogleAnalytics'); ?>
	<!-- Others checks -->
	<?php Docebo::checkPlatformValidity(); ?>

    <?php if( Yii::app()->user->isGodadmin && !Docebo::isTosApproved() && !Billing::isWireTransfer()): ?>
    <!-- Setup the TOS dialog -->
    <div id="tos-dialog" style="display: none">
	    <h1><?= Yii::t('userlimit', 'Terms Of Service') ?></h1>
	    <div>
		    <iframe src="http://www.docebo.com/tos/<?= (Yii::app()->getLanguage()=='it') ? 'it' : 'en'; ?>.html" frameBorder="0" style="width:100%; height: 400px;"></iframe>
	    </div>
	    <div class="form-actions">
		    <button class="btn btn-docebo green big approve-tos"><?=Yii::t('standard', '_APPROVE')?></button>
		    <button class="btn btn-docebo black big reject-tos" href="<?=Docebo::createLmsUrl('site/logout')?>"><?=Yii::t('standard', '_DENY')?></button>
	    </div>
    </div>
    <script type="text/javascript">
	    $(function() {
		    $("#tos-dialog").dialog2({
			    showCloseHandle: false,
			    removeOnClose: false,
			    autoOpen: false,
			    closeOnEscape: false,
			    closeOnOverlayClick: false
		    });

		    $("#tos-dialog").closest('.modal').addClass('tos-dialog');

		    $("#tos-dialog").dialog2("open");
		    $(document).on('click', '.approve-tos', function(e){
			    e.preventDefault();
			    $.ajax({
				    url: '<?=Docebo::createLmsUrl('site/axApproveLastTos')?>',
				    success: function(){
					    $('#tos-dialog').dialog2('close');
				    }
			    });
		    });
		    $(document).on('click', '.reject-tos', function(e){
			    e.preventDefault();
			    window.location.href = '<?=Docebo::createLmsUrl('site/logout')?>';
		    });
	    });
    </script>
    <?php endif; ?>

	<!-- show popup with maintenance date and text -->
	<?php if(
		isset(Yii::app()->params['maintenance_mode']) &&
		Yii::app()->user->getIsGodadmin() &&
		Yii::app()->params['maintenance_mode']['active'] &&
		!Docebo::isInMaintenance() &&
		!empty(Yii::app()->params['maintenance_mode']['from']) &&
		!empty(Yii::app()->params['maintenance_mode']['message']) &&
		Yii::app()->request->getParam('login', 0)
	): ?>
	<script>
		openDialog(null, '', '<?=Docebo::createLmsUrl('site/showMaintenancePopup')?>', 'maintenance-popup', 'modal-maintenance');
		$(document).delegate(".modal-maintenance", "dialog2.content-update", function() {
			var e = $(this);
			var autoclose = e.find("a.auto-close");
			console.log(e);
			console.log(autoclose.length);
			if (autoclose.length > 0) {
				e.find('.modal-body').dialog2("close");
			}
		});
	</script>
	<?php endif;?>

	<div id="load-trigger"><img src="data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw%3D%3D"></div>

	<?php $this->widget('common.widgets.CourseDetailsModalTrigger'); ?>

    <?php Yii::app()->event->raise('BeforeBodyTagClose', new DEvent($this, array())); ?>
</body>
</html>

<?php $senderEmail = Yii::app()->user->getState('senderEmail');
if($senderEmail == 1 && Yii::app()->user->getIsGodadmin()) { ?>
    <script>Docebo.Feedback.show('error', '<?= Yii::t('app', 'Please enter') . ' ' . Yii::t('configuration', '_MAIL_SENDER'); ?>');</script>
<?php }
Yii::app()->user->setState('senderEmail', null); ?>
