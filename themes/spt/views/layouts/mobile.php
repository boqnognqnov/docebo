<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">
	<link rel="shortcut icon" href="<?php echo $this->faviconUrl; ?>">

	<?php $this->widget("common.widgets.CloudfrontCookieBaker"); ?>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link href="<?php echo Docebo::createMobileUrl('site/colorschemeCss', array('time' => time())) ?>" rel="stylesheet" id="colorSchemeCssFile">

	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/mobile.css" rel="stylesheet">
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/mobile.js"></script>

	<?php $this->widget('common.extensions.jqm.JqmWidget', array()); ?>

</head>
<body>

<?php echo $content ?>

</body>
</html>