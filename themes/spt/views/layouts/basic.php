<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, target-densitydpi=160dpi, minimal-ui">
	<!-- ,user-scalable=no - for safari bug -->
	<link rel="shortcut icon" href="<?php echo $this->faviconUrl; ?>">

	<?php $this->widget("common.widgets.CloudfrontCookieBaker"); ?>

	<!-- Combined CSS, common for all layouts -->
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/common.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/material-design-iconic-font.min.css" rel="stylesheet">
	<?php
	$enable_legacy_menu = Settings::get('enable_legacy_menu', 'on');
	if ($enable_legacy_menu == 'off') { ?>
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/new-menu.css" rel="stylesheet">
	<?php } ?>

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/ie.css" rel="stylesheet">
		<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/html5.js"></script>
	<![endif]-->

	<!--[if lt IE 8]>
		<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/ie6.css" rel="stylesheet">
	<![endif]-->

	<!--[if (lt IE 9) & (!IEMobile)]>
		<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/respond.min.js"></script>
	<![endif]-->

	<!-- base js -->
	<?php
		preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);

		if(count($matches)<2)
		{
			preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
		}

		$jquery = 'jquery.js';
		$jquery_min = 'jquery.min.js';

		if (count($matches)>1)
		{
			$version = $matches[1];

			if($version == 8)
			{
				$jquery = 'jquery.old.js';
				$jquery_min = 'jquery.old.min.js';
			}
		}
	?>

	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/<?=YII_DEBUG ? $jquery : $jquery_min?>"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/<?=YII_DEBUG ? 'jquery-migrate.js' : 'jquery-migrate.min.js'?>"></script>

	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/app.js?70"></script>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?= DoceboUI::custom_css(); ?>
	<?php Yii::app()->legacyWrapper->applyJsGuard(); ?>
</head>
<body class="l-basic ">

<?php $this->widget("common.widgets.CloudfrontCookieBaker", array()); ?>

	<div id="wrapper">
		<header>
			<?php $this->widget('common.widgets.SiteHeader', array('headerType'=>'simple')); ?>
		</header>
		<div id="maincontent" class="container">

			<div class="inner-container">
				<div id="dcb-feedback"></div>
				<?php echo $content; ?>
			</div>
		</div>
	</div>
	<footer id="footer">
		<div class="container-fluid">
			<?php $this->widget('common.widgets.SitePoweredBy'); ?>
		</div>
	</footer>

</body>
</html>
