<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- ,user-scalable=no - for safari bug -->
	<link rel="shortcut icon" href="<?php echo $this->faviconUrl; ?>">

	<?php $this->widget("common.widgets.CloudfrontCookieBaker"); ?>

	<!-- Open sans font -->
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<!-- base js -->
	<?php
		preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);

		if(count($matches)<2)
		{
			preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
		}

		$jquery = 'jquery.js';
		$jquery_min = 'jquery.min.js';

		if (count($matches)>1)
		{
			$version = $matches[1];

			if($version == 8)
			{
				$jquery = 'jquery.old.js';
				$jquery_min = 'jquery.old.min.js';
			}
		}
	?>

	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/<?=YII_DEBUG ? $jquery : $jquery_min?>"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/<?=YII_DEBUG ? 'jquery-migrate.js' : 'jquery-migrate.min.js'?>"></script>

	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/app.js?70"></script>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<!-- Combined CSS, common for all layouts -->
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/common.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/material-design-iconic-font.min.css" rel="stylesheet">

	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/privacy.css" rel="stylesheet">

	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/public.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/public-responsive.css" rel="stylesheet">

	<?php
	$enable_legacy_menu = Settings::get('enable_legacy_menu', 'on');
	if ($enable_legacy_menu == 'off') { ?>
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/new-menu.css" rel="stylesheet">
	<?php } ?>

	<link href="<?php echo Yii::app()->createUrl('site/colorschemeCss', array('time' => time())) ?>" rel="stylesheet" id="colorSchemeCssFile">

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/ie.css" rel="stylesheet">
		<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/html5.js"></script>
	<![endif]-->

	<!--[if lt IE 8]>
		<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/ie6.css" rel="stylesheet">
	<![endif]-->

	<!--[if (lt IE 9) & (!IEMobile)]>
		<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/respond.min.js"></script>
	<![endif]-->

	<?php $this->widget('common.widgets.Vivocha'); ?>
	<?php $this->widget('common.widgets.GoogleAnalytics'); ?>

	<?= DoceboUI::custom_css(); ?>
	<?php Yii::app()->legacyWrapper->applyJsGuard(); ?>
</head>
<body id="loginLayout" class="">
<div id="loginBlock" style="">

	<div class="centered">
		<a class="header-logo" href="<?= Yii::app()->getBaseUrl() ?>">
			<?php echo CHtml::image(Yii::app()->theme->getLogoUrl(), Yii::t('login', '_HOMEPAGE')); ?>
		</a>
		<?php echo $content; ?>
		<div class="loginFooter">
			<?php $this->widget('common.widgets.SitePoweredBy'); ?>
		</div>
	</div>

</div>
</body>
</html>
