<?php
/**
 * Created by PhpStorm.
 * User: NikVas
 * Date: 17-Nov-15
 * Time: 17:13
 */
//$video = $module->getVideo();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">

    <?php $this->widget("common.widgets.CloudfrontCookieBaker"); ?>

    <?php
    preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);

    if(count($matches)<2)
    {
        preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
    }

    $jquery = 'jquery.js';
    $jquery_min = 'jquery.min.js';

    if (count($matches)>1)
    {
        $version = $matches[1];

        if($version == 8)
        {
            $jquery = 'jquery.old.js';
            $jquery_min = 'jquery.old.min.js';
        }
    }
    ?>

    <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/<?=YII_DEBUG ? $jquery : $jquery_min?>"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/<?=YII_DEBUG ? 'jquery-migrate.js' : 'jquery-migrate.min.js'?>"></script>

    <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/app.js?70"></script>

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <link href="<?php echo Yii::app()->theme->baseUrl ?>/css/common.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->theme->baseUrl ?>/css/material-design-iconic-font.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->theme->baseUrl ?>/css/public.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->theme->baseUrl ?>/css/public-responsive.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->theme->baseUrl ?>/css/minimal_login.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->createUrl('site/colorschemeCss', array('time' => time())) ?>" rel="stylesheet" id="colorSchemeCssFile">
    <link rel="shortcut icon" href="<?php echo $this->faviconUrl; ?>">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <link href="<?php echo Yii::app()->theme->baseUrl ?>/css/ie.css" rel="stylesheet">
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/html5.js"></script>
    <![endif]-->

    <!--[if lt IE 8]>
    <link href="<?php echo Yii::app()->theme->baseUrl ?>/css/ie6.css" rel="stylesheet">
    <![endif]-->

    <!--[if (lt IE 9) & (!IEMobile)]>
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/respond.min.js"></script>
    <![endif]-->

    <?php $this->widget('common.widgets.Vivocha'); ?>
    <?php $this->widget('common.widgets.GoogleAnalytics'); ?>

    <!-- Open sans font -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

    <?= DoceboUI::custom_css(); ?>

    <!-- EVENT beforeHtmlHeadTagClose -->
    <?php Yii::app()->event->raise('beforeHtmlHeadTagClose', new DEvent($this, array())); ?>

    <?php Yii::app()->legacyWrapper->applyJsGuard(true); ?>

    <style>
        .additionalStyle{
            margin-left: 48px !important;
            display: block !important;
        }
    </style>
</head>
<body class="login-body centered video ">

<!-- content -->
<?php echo $content; ?>
<!-- /content -->
<?php
$customText = BrandingWhiteLabelForm::getFooterText();
$customSite = BrandingWhiteLabelForm::getSiteReplacement();
if ($customText !== null)
    $customClass = 'additionalStyle';
else $customClass = '';
?>
<!-- footer -->
<footer id="footer">
    <div class="row">
        <div class="col-sm-6">
            <ul class="text-right">
                <li class="<?=$customClass?>">
                    <?php
                    if ($customText !== null) : ?>
<!--                        --><?// if ($customSite != 'www.docebo.com') : ?>
<!--                            <a href="--><?php //echo ( strpos($customSite, 'http://') === false ? 'http://' : '' )
//                                .$customSite; ?><!--" onclick="window.open(this.href);return false;">--><?php //echo $customText; ?><!--</a>-->
<!--                        --><?// else: ?>
                            <span><?= '<div class="allowOlUlNumbering">' . $customText . '</div>'; ?></span>
<!--                        --><?// endif; ?>
                    <?php else : ?>
                        <a href="http://www.docebo.com/" onclick="window.open(this.href);return false;">&copy; Powered by Docebo <?//= $this->year ?></a>
                    <?php endif; ?>
                </li>
            </ul>
        </div>
    </div>
</footer>
<!-- footer -->

<!-- header -->
<header id="header" class="header">
    <!-- logo -->
    <h1><a style="float: left" class="logo" href="/"><img src="<?php echo Yii::app()->theme->getLogoUrl(); ?>" alt="Company Logo"></a>
        <div id="languageBtn pull-right"  style="display: inline-block;float: right;">
            <?php
            $msg = Yii::app()->user->getFlash('email_status_change');
            if(!empty($msg)) {
                if ($msg == Yii::t('standard', 'You have confirmed your email!')) $class = 'success';
                elseif ($msg == Yii::t('standard', 'Invalid or expired email verification link!')) $class = 'danger';
                else $class = 'info';
                ?>
                <div class="alert alert-<?=$class?>" style="text-align: center">
                    <?=$msg;?>
                </div>
            <?php }?>
            <?=$this->renderPartial('//site/login/_langDropdown')?>
        </div>
        <?php if (Settings::get('register_type', 'admin') != 'admin') : ?>
            <div class="pull-right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
            <div class="pull-right">
                <a class="open-dialog dialog-links btn btn-docebo green"  id="register-modal-btn" data-dialog-class="register" rel="dialog-register" href="<?=Yii::app()->createUrl('user/axRegister')?>"><?=Yii::t('user','Register');?></a>
            </div>
        <?php endif; ?>

		<?php $customMessageInHeader = CoreLangWhiteLabelSetting::getCustomHeader();
        Yii::app()->event->raise('GetMultidomainHeaderMessage', new DEvent($this, array('customMessageInHeader' => &$customMessageInHeader)));
        if (!empty($customMessageInHeader))
            echo '<div class="headerMessage">' . $customMessageInHeader . '</div>'; ?>
    </h1>
    <!-- /logo -->
</header>
<!-- /header -->

<script type="text/javascript">
$(function(){
    //blue big
    var langDropDown = $('.btn-docebo.dropdown-toggle')
    langDropDown.removeClass('blue');
    langDropDown.removeClass('big');
    //<i class="fa fa-globe fa-2x"></i>
    langDropDown.html('<i class="fa fa-globe fa-2x"></i>'+langDropDown.html());
})
</script>
</body>
</html>
