<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- ,user-scalable=no - for safari bug -->
	<link rel="shortcut icon" href="<?php echo $this->faviconUrl; ?>">

	<?php $this->widget("common.widgets.CloudfrontCookieBaker"); ?>

	<!-- Open sans font -->
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<!-- base js -->
	<?php
		preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);

		if(count($matches)<2)
		{
			preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
		}

		$jquery = 'jquery.js';
		$jquery_min = 'jquery.min.js';

		if (count($matches)>1)
		{
			$version = $matches[1];

			if($version == 8)
			{
				$jquery = 'jquery.old.js';
				$jquery_min = 'jquery.old.min.js';
			}
		}
	?>

	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/<?=YII_DEBUG ? $jquery : $jquery_min?>"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/<?=YII_DEBUG ? 'jquery-migrate.js' : 'jquery-migrate.min.js'?>"></script>

	<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>

	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/app.js?70"></script>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<!-- Combined CSS, common for all layouts -->
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/common.<?= YII_DEBUG ? '' : 'min.' ?>css" rel="stylesheet">
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/material-design-iconic-font.min.css" rel="stylesheet">

	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/public.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/public-responsive.css" rel="stylesheet">

	<link href="<?php echo Yii::app()->theme->baseUrl ?>/js/formstyler/jquery.formstyler.docebo.css" rel="stylesheet">
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/formstyler/jquery.formstyler.min.js"></script>


	<!-- i-sprite Icons -->
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/i-sprite.css" rel="stylesheet">
	<!-- p-sprite Icons -->
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/player-sprite.css" rel="stylesheet">

	<link href="<?php echo Yii::app()->createUrl('site/colorschemeCss', array('time' => time())) ?>" rel="stylesheet" id="colorSchemeCssFile">

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/ie.css" rel="stylesheet">
		<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/html5.js"></script>
	<![endif]-->

	<!--[if lt IE 8]>
		<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/ie6.css" rel="stylesheet">
	<![endif]-->

	<!--[if (lt IE 9) & (!IEMobile)]>
		<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/respond.min.js"></script>
	<![endif]-->

	<?php $this->widget('common.widgets.Vivocha'); ?>
	<?php $this->widget('common.widgets.GoogleAnalytics'); ?>

	<!-- EVENT beforeHtmlHeadTagClose -->
	<?php Yii::app()->event->raise('beforeHtmlHeadTagClose', new DEvent($this, array())); ?>

	<?= DoceboUI::custom_css(); ?>

	<?php Yii::app()->legacyWrapper->applyJsGuard(true); ?>
</head>
<body>

	<div id="wrapper">
		<header>
			<?php $this->widget('common.widgets.SiteHeader'); ?>
		</header>
		<div id="maincontent" class="container">

			<?=DoceboUI::getLoginError();?>

			<? $userPages = DoceboUI::getUserPages(); ?>

			<? if($userPages): ?>
			<div class="user-pages-container">
				<ul>
					<li class="selected"><a href="<?=Yii::app()->request->baseUrl?>"><?php echo Yii::t('standard', 'Catalog'); ?></a></li>
					<?php foreach($userPages as $page): ?>
					<li><a href="#user-page-<?=$page->page_id?>" data-idpage="<?=$page->page_id?>"><?=$page->title?></a></li>
					<?php endforeach; ?>
				</ul>
			</div>
			<? endif; ?>

			<div class="inner-container public-catalog-layout">
				<div id="dcb-feedback"><?php DoceboUI::printFlashMessages(); ?></div>
				<?php echo $content; ?>
			</div>
		</div>
	</div>
	<footer id="footer">
		<div class="container-fluid container">
			<?php $this->widget('common.widgets.SitePoweredBy'); ?>
		</div>
	</footer>

<script type="text/javascript">
$(function(){
	// A custom user created page is clicked. Load it.
	$('.user-pages-container a').click(function(e){
		var context = $(this); // preserve this for later

		// Get the ID of the user created page which to display
		var idPage = $(this).data('idpage');

		if(idPage){
			e.preventDefault();

			// Load the user-created page by the specified ID in the
			// main container.
			$('.inner-container').load('<?= Docebo::createAppUrl('lms:site/getUserPage') ?>', {
				id: idPage
			}, function(){
				// Success, apply a special class to visually display
				// which link was clicked.
				$('.user-pages-container li').removeClass('selected');
				context.closest('li').addClass('selected');
			});
		}else{
			// Probably a regular link clicked. Allow it and
			// don't do anything else.
		}
	});
});
</script>
<div id="load-trigger"><img src="data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw%3D%3D"></div>
<?php $this->widget('common.widgets.CourseDetailsModalTrigger'); ?>

	<?php Yii::app()->event->raise('loginPageBeforeBodyTagClose', new DEvent($this, array())); ?>

</body>
</html>
