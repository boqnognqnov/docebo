<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, target-densitydpi=160dpi, minimal-ui">
	<!-- ,user-scalable=no - for safari bug -->
	<link rel="shortcut icon" href="<?php echo $this->faviconUrl; ?>" type="image/png" />

	<?php $this->widget("common.widgets.CloudfrontCookieBaker"); ?>

	<!-- Sprites (i-sprite, player-sprite) -->
	<?php DoceboAssetMerger::publishAndRender(array('/css/i-sprite.css', '/css/player-sprite.css'), true) ?>

	<!-- Application -->
	<?php
	preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);

	if(count($matches)<2)
	{
		preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
	}

	$jquery = 'jquery.js';
	$jquery_min = 'jquery.min.js';

	if (count($matches)>1)
	{
		$version = $matches[1];

		if($version == 8)
		{
			$jquery = 'jquery.old.js';
			$jquery_min = 'jquery.old.min.js';
		}
	}
	?>

	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/<?=YII_DEBUG ? $jquery : $jquery_min?>"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/<?=YII_DEBUG ? 'jquery-migrate.js' : 'jquery-migrate.min.js'?>"></script>


	<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/js/app.js?70"></script>

	<!-- base js -->
	<?php DoceboAssetMerger::publishAndRender(array(
		'/js/formstyler/jquery.formstyler.css',
		'/js/jscrollpane/jquery.jscrollpane.css',
		'/js/Jcrop/jquery.Jcrop.min.css',
	), true, DoceboAssetMerger::$TARGET_ASSETS_FOLDER) ?>
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/print.css" rel="stylesheet" media="print">


	<?php

	$enable_legacy_menu = Settings::get('enable_legacy_menu', 'on');
	$cssToMerge = '';
	if ($enable_legacy_menu == 'off') {
		$cssToMerge = '/css/new-menu.css';
	}

	if (Lang::isRTL(Yii::app()->getLanguage())): ?>
		<!-- RTL styles -->
		<?php DoceboAssetMerger::publishAndRender(array(
			'/css/bootstrap-rtl.min.css',
			'/css/bootstrap-responsive-rtl.min.css'
		), true, DoceboAssetMerger::$TARGET_THEME_ASSETS_FOLDER) ?>

		<?php DoceboAssetMerger::publishAndRender(array(
			'/js/confirm/jquery.confirm.css',
			'/css/admin.css',
			'/css/common.css',
			'/css/rtl.css',
			$cssToMerge
		), true, DoceboAssetMerger::$TARGET_THEME_ASSETS_FOLDER) ?>

	<?php else: ?>
		<?php
		DoceboAssetMerger::publishAndRender(array(
			'/css/common.css',
			'/js/confirm/jquery.confirm.css'
		), true, DoceboAssetMerger::$TARGET_THEME_ASSETS_FOLDER);

		DoceboAssetMerger::publishAndRender(array(
			'/css/admin.css',
			$cssToMerge
		), true, DoceboAssetMerger::$TARGET_THEME_ASSETS_FOLDER);
		?>
	<?php endif; ?>

	<!-- template color scheme -->
	<link href="<?php echo Docebo::createAppUrl('admin:setup/colorschemeCss', array('time' => time())) ?>" rel="stylesheet" id="colorSchemeCssFile">

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/ie.css" rel="stylesheet">
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/html5.js"></script>
	<![endif]-->

	<!--[if lt IE 8]>
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/ie6.css" rel="stylesheet">
	<![endif]-->

	<!--[if lt IE 7]>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/json2-for-ie7.js"></script>
	<![endif]-->

	<!--[if (lt IE 9) & (!IEMobile)]>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/respond.min.js"></script>
	<![endif]-->

	<script type="text/javascript">var HTTP_HOST = '<?php echo Yii::app()->request->getBaseUrl(true).'/'; ?>';</script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/formstyler/jquery.formstyler.js"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/confirm/jquery.confirm.js"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/jscrollpane/jquery.jscrollpane.js"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/jscrollpane/jquery.mousewheel-3.0.4.js"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/menu.js"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/Jcrop/jquery.Jcrop.min.js"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/jquery.hashchange.min.js"></script>
	<!-- "admin" Yii app ?  -->
	<?php if (Yii::app()->name === 'admin') : ?>
		<script src="<?php echo Yii::app()->baseUrl ?>/js/script.js"></script>
		<script src="<?php echo Yii::app()->baseUrl ?>/js/scriptModal.js"></script>
		<script type="text/javascript">
			//<![CDATA[
			var yiiLang = {
				standardFilledLanguages: '<?= Yii::t('standard', 'Filled languages') ?>'
			};
			//]]>
		</script>
		<script src="<?php echo Yii::app()->baseUrl ?>/js/scriptTur.js"></script>
		<script src="<?php echo Yii::app()->baseUrl ?>/js/admin.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/scriptModal.js"></script>
	<?php endif; ?>


	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/app.js?70"></script>

	<!-- Open sans font -->
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<?= DoceboUI::custom_css(); ?>

	<!-- EVENT beforeHtmlHeadTagClose -->
	<?php Yii::app()->event->raise('beforeHtmlHeadTagClose', new DEvent($this, array())); ?>

	<style type="text/css">
		/* NOTE: these are specific styles for this layout only */
		html {
			overflow: auto;
		}

		body.docebo {
			overflow: auto;
		}

		div.menu-spacer-second-gen.hydra-main-window {
			position: fixed;
			width: 100%;
			height: 100%;
			left: 0;
			top: 0;
			margin-left: 0;
			padding-left: 50px;
		}

		div.menu-spacer-second-gen.hydra-main-window .hydra-position-adjust {
			position: relative;
			/*width: 100%;*/
			/*height: 100%;*/
			left: 0;
			top: 0;
			padding-right: 50px;
		}

		div.hydra-internal-adjuster {
			width: 100%;
		}

		div.hydra-internal-adjuster div.header-logo-line {
			margin-top: 12px;
			margin-bottom: 20px;
		}

		div.hydra-internal-adjuster div.header-logo-line div.span4 {
			margin-left: 50px;
		}

		div.hydra-internal-adjuster div.header-logo-line div.span8 {
			margin-left: 0;
			margin-right: 0;
			width: 63.3%;
		}

		/*div.span4 a.header-logo img {
			margin-top: 0px;
		}*/

	</style>

	<?php Yii::app()->legacyWrapper->applyJsGuard(); ?>
</head>
<body class="docebo tiles-<?= Yii::app()->legacyWrapper->tileSize ?>">

<?php Yii::app()->event->raise('AfterBodyTagOpen', new DEvent($this, array())); ?>
<?php
$menuRenderer = 'common.widgets.MainMenu';
if(Yii::app()->event->raise('getMenuRenderer', new DEvent($this, array('renderer'=>&$menuRenderer)))) {
	$this->widget( $menuRenderer, array(// no option at this point
	) );
}
?>
<div id="wrapper">
	<div class="menu-spacer-second-gen hydra-main-window">
		<?php if (Yii::app()->event->raise('BeforeHeaderRender', new DEvent($this, array()))) : ?>
		<header id="header" class="hydra-position-adjust">
			<div class="hydra-internal-adjuster">
				<?php $this->widget('common.widgets.SiteHeader', array('headerType' => 'admin')); ?>
			</div>
		</header>
		<?php endif; ?>
		<!-- no navigation/bradcrumbs to be rendered in this layout -->
		<!--<div id="dcb-feedback"><?php DoceboUI::printFlashMessages(); ?></div>-->
		<div id="content" class="hydra-position-adjust">
			<?php echo $content; ?>
		</div>

		<!-- no footer to be rendered in this layout -->

		<script type="text/javascript">

			//force the #content div to fill the remaining browser space

			//set the content height to current browser viewport dimensions
			function resizeHydraContent() {
				var headerHeight = $('#header').height();
				var viewportHeight = $(window).height();
				var newHeight = (viewportHeight - headerHeight);
				if (newHeight < 100) { newHeight = 100; } //try to emulate a "min-height" property (minimum 100 pixels)
				$('#content').css('height', newHeight + "px");
			}

			$(function() {
				//initialialize content height and handle resizing events
				resizeHydraContent();
				$(window).on('resize', function() {
					resizeHydraContent();
				});
			});

			//=== main menu patch ===
			// NOTE: using this layout somehow interferes with some of the main menu features. Submenus did not disappear anymore
			// when user was leaving the mouse cursor from the menu block. The only way we could find to patch this was to force
			// the "mouseleave" event on some elements (assuming that the MainMenu javascript object has been initialized).
			// For more details have a look at "themes/spt/js/menu.js"
			var patchMainMenuTimeLimit = 5000; //max time limit in order to avoid an infinite loop
			var patchMainMenu = function() {
				if (patchMainMenuTimeLimit <= 0) return; //just interrupt the menu checking if too much time is passed
				try {
					//first make sure that the main menu javascript object exists and has been initialized
					if (typeof theMainMenu !== 'undefined' && typeof theMainMenu.isReady && theMainMenu.tiles.length > 0) {
						//Docebo.log("Patching main menu ...");
						//apply the event to menu elements
						theMainMenu.tiles.on("mouseleave", $.proxy(theMainMenu.close, theMainMenu));
					} else {
						//main menu is not ready yet, wait a bit and then re-check
						//Docebo.log("Main menu not ready for patching yet ("+patchMainMenuTimeLimit+" ms)...");
						patchMainMenuTimeLimit -= 100; //decrease time limit
						setTimeout(patchMainMenu, 100);
					}
				} catch (e) {
					//avoid interruption of the checks
					Docebo.log("Main menu patch error: " + e);
				}
			};
			$(function() {
				//start patching the main menu when the page has been loaded
				patchMainMenu();
			});
			//===
		</script>

	</div>
</div>
<?php Docebo::checkPlatformValidity(); ?>
<div id="load-trigger">
	<img src="data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw%3D%3D">
</div>

<?php Yii::app()->event->raise('BeforeBodyTagClose', new DEvent($this, array())); ?>
</body>
</html>

<?php $senderEmail = Yii::app()->user->getState('senderEmail');
if($senderEmail == 1 && Yii::app()->user->getIsGodadmin()) { ?>
	<script>Docebo.Feedback.show('error', '<?= Yii::t('app', 'Please enter') . ' ' . Yii::t('configuration', '_MAIL_SENDER'); ?>');</script>
<?php }
Yii::app()->user->setState('senderEmail', null); ?>