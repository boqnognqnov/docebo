/**
 * Created by asen on 03-Aug-16.
 */
(function ($) {
    var hidePanel = function hidePanel() {
        $('.rightPanelFlag:visible').rightPanel('hide');
    };
    var RightPanel = function ($el, options) {
        var settings = {
            trigger: '', // the element that is going to trigger the panel (optional),
            shown: function (panel, trigger) {
                console.log('after show right panel')
            },
            hidden: function (panel, trigger) {
                console.log('after close right panel')
            },
            show: function (panel, trigger) {
                console.log('before show right panel')
            },
            hide: function (panel, trigger) {
                console.log('before close right panel')
            },
            onBack: function(panel){
              console.log('onBack triggered');
            },
            onNext: function(){
                console.log('onNext triggered');
            },
            title: "",
            content: "",
            closeOnOverlayClick: true, // if false, the right panel is not closed upon clicking on the black overlay
            hasCloseButton: true, // if false the right panel has no X (close) button,
            width: 600 // Sets the right panel size (default 500px)
        };
        this.settings = $.extend(settings, options);
        this.element = $el;
        this.state = 0;
        this.originalTitle = '';
        this.originalContent = '';
        this.init();
    };

    $.extend(RightPanel.prototype, {
        init: function () {
            var el = this.element;
            var contentDiv = $('<div class="contentRightPanel">');
            el.css({
                'position': 'fixed',
                'top': '0',
                'right': '0px',
                'background-color': '#eee',
                'z-index': '1500',
                'width': this.settings.width + 'px'
            });
            contentDiv.html(this.settings.content);
            contentDiv.appendTo(el);
            el.hide();
            var header = $('<header>');
            header.prependTo(el);
            var title = $('<h4 class="right-panel-title">');
            title.text(this.settings.title);
            this.originalTitle = this.settings.title;
            title.prependTo(header);
            if (this.settings.hasCloseButton) {
                var closeButton = $('<i class="zmdi zmdi-close closeRightPanelButton" style="color: grey">');
                closeButton.prependTo(header);
            }
            el.appendTo($('body'));
            this.element = el;

            var $this = this;
            if(this.settings.trigger !== ''){
                this.settings.trigger.on('click', function(){
                    $this.show();
                });
            }
        },
        show: function () {
            this.settings.show();
            var modal = $('.modal-backdrop');
            if(modal.length < 1){
                $('body').append("<div class='modal-backdrop'></div>");
                modal = $('.modal-backdrop');
            }
            modal.off('click', hidePanel);
            if (this.settings.closeOnOverlayClick) {
                modal.click(hidePanel);
            }
            this.element.show();
            modal.css({'background-color': '#000', 'opacity': 0.5}).show();
            this.settings.shown();
        },
        hide: function () {
            this.settings.hide();
            this.element.hide();
            $('.modal-backdrop').hide();
            this.settings.hidden();
        },
        toggle: function () {
            if (this.element.css('display') != 'none') this.hide();
            else this.show();
        },

        back: function(){
            this.setTitle(this.originalTitle);
            this.setContent(this.originalContent);
            this.settings.onBack.apply(this);
            this.element.find('header').find('i.back-button').remove();
            this.element.find('header').removeClass('on-back');
        },

        next: function(){
            var $this = this;
            this.originalContent = $('.contentRightPanel').html();

            this.settings.onNext.apply(this);
            var backButton = $('<i class="zmdi zmdi-chevron-left back-button" aria-hidden="true"></i>');
            backButton.prependTo(this.element.find('header'));
            backButton.click(function(){
                $this.back();
            });
            this.element.find('header').addClass('on-back');
            this.element.find('header').append('');
        },

        setTitle: function(title){
            this.element.find('h4.right-panel-title').text(title);
        },

        setContent: function(content){
            this.element.find('.contentRightPanel').html(content);
        }
    });

    // jQuery collection methods
    $.fn.rightPanel = function (triggerEvents, eventParams) {
        if (triggerEvents !== undefined && typeof triggerEvents === 'string' && triggerEvents.length > 0) {
            var rightPanel = $(this).data('right-panel-model');
            if (triggerEvents === "show") {
                rightPanel.show();
            } else if (triggerEvents === "hide") {
                rightPanel.hide();
            } else if (triggerEvents === 'toggle') {
                rightPanel.toggle();
            } else if(triggerEvents === 'next'){
                rightPanel.next();
            } else if(triggerEvents === 'back'){
                rightPanel.back();
            }
        } else {
            $(this).addClass('rightPanelFlag');
            $(this).data('right-panel-model', new RightPanel($(this), triggerEvents));
        }
        return this;
    };
    $(document).on('click', '.closeRightPanelButton', hidePanel);
}(jQuery));