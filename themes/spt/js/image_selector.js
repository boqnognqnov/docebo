/**
 * Image Selector jQuery Plugin
 * A JS compagnion for displaying a Modal Dialog2 to select (and upload) Asset image(s) of a given Asset Type  
 * 
 * @param $
 */
(function($) {

	/* Hold pluging options in this closure variable */
	var settings = {
		formId			: 'image-selector-form',
		chunkSize		: 6,
		modalId 		: 'image-selector-modal',
		radioName		: 'image_id_radio',
		itemWidth		: false,
		itemHeight		: false
	};
	
	/* Enable disable log() output to console */
	var debug = true;
	
	/* Holds image selector jQuery Object */
	var $selectorInput;  		// the whole #mydashboard

	/* Modal object */
	var $modal = null;
	
	/**
	 * A nice logging/debugging function
	 */
    var log = function() {
		var me = this;

    	if (!debug) return;
    	if (window.console && window.console.log) {
    		var args = Array.prototype.slice.call(arguments);
    		if(args){
    			$.each(args, function(index, ev){
    				window.console.log(ev);
    			});
    		}
        }
    };
	

	/**
	 * Add event listeners
	 *
	 */
	var addListeners = function() {
		var me = this;

		/**
		 * Listen to clicks on an images (carousel sub-items)
		 */
		$modal.off('click', '.imsel-manage-panel .item > div')
		.on('click', '.imsel-manage-panel .item > div',function() {
			$('.imsel-manage-panel .item > div').removeClass('checked');
			$('.imsel-manage-panel .item > div input[type="radio"]').prop('checked', false);
			$(this).addClass('checked');
			$(this).find('input[type="radio"]').prop('checked', true);
		});

		/**
		 * Handle new image upload
		 */
		$modal.off('change', '.image-manage-file')
		.on('change', '.image-manage-file',function(e) {
		    var options = {
		      type        : 'post', 
		      dataType    : 'json',
		      data: {
		      }, 
		      beforeSubmit: function(){},
		      success     : function (res) {
			      if (res && res.success == true) {
			      	  liveAddImage('user-images', res.data);
			      }
		      }  
		    };
		    e.stopPropagation();
		    $('#' + settings.formId).ajaxForm(options).submit();

		    return false;
		});


		/**
		 * Final Form submition (returning image selection)
		 *
		 */
		$modal.off('click', 'a.confirm-save')
		.on('click', 'a.confirm-save', function(e){
		    var options = {
		      type        : 'post', 
		      dataType    : 'json',
		      data: {
			      confirm_save: true
		      }, 
		      success     : function (res) {
			    if (res && (res.success == true)) {
					var extraData = {
						data: res.data 
					};
					$selectorInput.val(res.data.image_id);
					$('img#image-' + $selectorInput.attr('id')).attr('src', res.data.image_url);
					$selectorInput.trigger('modal-image-selector.success', extraData);
			    }  
			    // Close the srrounding Dialog!
			    $('#' + settings.modalId).dialog2('close');  
		      }
		  	};
		    e.stopPropagation();
		    $('#' + settings.formId).ajaxForm(options).submit();
			return false;
		});

			
		/**
		 * DELETE
		 */
		$modal.off('click', '.user-images-slider .deleteicon')
		.on('click', '.user-images-slider .deleteicon', function(e) {

			var imageId = $(this).closest('.sub-item').find('input[type="radio"]').val();

		    var options = {
				type        : 'post', 
				dataType    : 'json',
				data: {
					delete_image	: true,
					image_id		: imageId
				}, 
				success     : function (res) {
					if (res && (res.success == true)) {
						$('.imsel-manage-panel').find('input[type="radio"][value="'+ imageId +'"]').closest('.sub-item').remove();
						$('#user-images-count').text($('#user-images-slider .sub-item').length);
						updateUi();
					}  
				}
			};
			
		    e.stopPropagation();
		    
		    $('#' + settings.formId).ajaxForm(options).submit();
			return false;
			
		});
		
	};
	
	
	/**
	 * Upon uploading a new image we have to add it to the slider, LIVE
	 */
	var liveAddImage = function(tabPaneId, data) {
		var me = this;

		// Uncheck all previosuly checked images
		$('.imsel-manage-panel .carousel .sub-item').removeClass('checked');

		// New sub-item, Both RADIO && IMAGE CHECKED!!!
		var newSubItem = '' +
			'<div class="sub-item checked">' +
			'  <img src="'+ data.image_url + '"> ' +
			'  <input type="radio" checked="checked" name="' + settings.radioName + '" value="' + data.image_id +'">' +
			'  <span class="checkmark"> ' +
			'    <span class="i-sprite is-check green large"></span> ' +
			'  </span> ' + 
			'  <span class="deleteicon"><span class="i-sprite is-remove red"></span></span> ' +
			'</div>' +  
		'';

		// New carousel Item (chucnk)
		var newItem = '' +
			'<div class="item">' +
			' ' + newSubItem + ''
			'</div>' +  
		'';		
		
		// Find a carousel item (chunk) having less than 'chunckSize' sub-items
		// and add new image into it. Otherwise, create new item (chunk)
		var found = false;

		// De-check all radios, just in case
		$('#' + tabPaneId + ' .carousel .item').find('input[type="radio"]').prop('checked', false);
		
		$('#' + tabPaneId + ' .carousel .item').each(function(){
			var $item = $(this);
			if ($item.find('.sub-item').length < settings.chunkSize) {
				$item.append(newSubItem);
				found = true;
				return false; // break each(), not returning from function!
			}
		});

		// If we did not find a proper chunk to insert the new image, just add a whole new chunck
		if (!found) {
			$('#' + tabPaneId + ' .carousel-inner').append(newItem);
		}
		
	    // Update HTML/etc.
	    updateUi();
		
		return true;
		
	};
	
	
	/**
	 *  Do styling and post-render stuff: counting, selecting, activating, etc.
	 *
	 *	This one must be called after HTML is rendered/changed, every time.
	 */
	var updateUi = function(firstTime) {
		var me = this;
		
		if (firstTime === true) {
		}
		
		// Broswe button styling (we should find some other way, not using styler)
		if (typeof Yii !== "undefined") {
			$('input[name="image_manage_file"]').styler({browseText:Yii.t('organization', 'Upload File')});
		}
		else {
			$('input[name="image_manage_file"]').styler({browseText:'Upload File'});
		}

		// Count sub-items (images) and show count information in tab selector
		$('#system-images-count')	.text($('#system-images-slider .sub-item').length);
		$('#user-images-count')		.text($('#user-images-slider .sub-item').length);

		
		// Find "checked" sub-item (image) and make its chunk (the carousel item) "active"; stay inside its slider!
		var $subItemChecked = $('.imsel-manage-panel .sub-item.checked');
		if ($subItemChecked.length > 0) {
			// "Deactivate" all chuncks (carousel items) && Activate "checked" sub-item's chunk  
			$subItemChecked.closest('.carousel').find('.item').removeClass('active');
			$subItemChecked.parent().addClass('active');

			// Deactivate all tab panes, activate the one where we have checked sub-item
			$('.imsel-manage-panel .tab-pane').removeClass('active');
			$subItemChecked.closest('.tab-pane').addClass('active');

			// Deactivate all tabs (the left menu) && Activate the one that corresponds to the activated pane
			var activePaneId = $('.imsel-manage-panel .tab-pane.active').prop('id');
			$('.imsel-manage-panel .nav-list > li').removeClass('active');
			$('.imsel-manage-panel .nav-list > li a[href="#'+activePaneId+'"]').closest('li').addClass('active');
		}

		if ($('#user-images-slider .sub-item').length > 0) {
			$('#user-images-slider .carousel-control.right').show();
			$('#user-images-slider .carousel-control.left').show();
		}
		else {
			$('#user-images-slider .carousel-control.right').hide();
			$('#user-images-slider .carousel-control.left').hide();
		}	
		
		if (settings.itemWidth !== false)
			$('.imsel .imsel-manage-panel .carousel-inner .item .sub-item').css({width: settings.itemWidth});
		
		if (settings.itemHeight !== false)
			$('.imsel .imsel-manage-panel .carousel-inner .item .sub-item').css({height: settings.itemHeight});

        /********** Resizing the images in the "Background Upload" section *********/
        if ($('.imsel .imsel-manage-panel .carousel-inner .item .sub-item').children('img').length)
        var subImg = $('.imsel .imsel-manage-panel .carousel-inner .item .sub-item').children('img');
		if(typeof subImg !== "undefined"){
			subImg.addClass("full-height");
		}
	};	


    
	/**
	 * Callable methods in form of 
	 * $('#element').imageSelector('<method-name>', <arguments>).
	 * 
	 */
	var methods = {
		/**
		 * init() is a special methods and is called if no method is specified, e.g. $('#image-selector').imageSelector({}) will call init().
		 * It must be called always first!!!!!! Without calling it first, the whole plugin won't work!!!!
		 */	
		init: function(options) {
			
			settings = $.extend({}, settings, options);
			
			if (typeof settings.debug !== "undefined")
				debug = settings.debug;
			
			$selectorInput = this;
			
			$modal = $('#' + settings.modalId).closest('.modal');
			
			$modal.addClass('imsel');
			
			addListeners();
			updateUi(true);
			
			return this;
			 
			
		},
		
		

		
	};
	
	/**
	 * Plug it in
	 */
	$.fn.imageSelector = function(methodOrOptions) {
        if ( methods[methodOrOptions] ) {
            return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
            // Default to "init"
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  methodOrOptions + ' does not exist in jQuery.imageSelector' );
        }    
    };

})(jQuery);
