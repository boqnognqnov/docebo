$(function(){
	inlineEditPrepare();
});
function inlineEditPrepare(id, data)
{
	//prevent grid row selection in case of CCheckBoxColumn usage
	$('.inlineEditTextField').click(function(e){
		return false;
	});
	prepareDatePickers();
}

//execute afterUpdate method if it's added
function customAfterAjaxUpdate(afterUpdate, id, data)
{
	if (typeof(afterUpdate) == 'function')
	{
		afterUpdate(id, data);
	}
	inlineEditPrepare();
}

function initInlineEdit(obj, pk)
{
	var parent = obj.parentElement;
	var inlineDiv = $("div", parent);
	if (inlineDiv.is(":visible") == false) {
		closeInlineEdit();
		var attribute = inlineDiv.attr('alt');
		//append div only if it's not already here
		if (inlineDiv.is(':empty'))
		{
			$("#inlineForm_"+attribute).appendTo(inlineDiv);
		}
		setInputDefaultValue($(obj).attr('alt'), inlineDiv);
		$("form", inlineDiv).attr('alt', pk);
		$("div", parent).fadeIn('fast');
	}
}

function closeInlineEdit()
{
	$('.inlineEditTextField').each(function(){
		$("div", this).fadeOut('fast');
	});
}

/**
 * Set the default value for current input in cell
 */
function setInputDefaultValue(selected, inlineDiv)
{
	if ($('p input', inlineDiv).length)
	{
		if ($('p input', inlineDiv).is(':checkbox'))
		{
			if (Number(selected))
				$('p input', inlineDiv).attr('checked', selected);
			else
				$('p input', inlineDiv).removeAttr('checked');
		}
		else
		{
			$('p input', inlineDiv).attr('value', selected);
		}
	}
	else if ($('p select', inlineDiv).length)
		$('p select', inlineDiv).val(selected);
	else if ($('p checkbox', inlineDiv).length)
		$('p checkbox', inlineDiv).val(selected);

	if ($('p input', inlineDiv) && $('p input', inlineDiv).attr('class') == 'hasDatepicker')
	{
		//addDatePickerId($('p input', inlineDiv).attr('id'));
	}
}

function submitInlineEdit(formId)
{
	var url = $("#"+formId).attr('action');
	var formData = $("#"+formId).serialize();
	var pk = $("#"+formId).attr('alt');
	var gridId = $("#"+formId).parents('.grid-view').attr("id");
	$.ajax({
		url: url+"&id="+pk,
		data: formData,
		type: 'post',
		success: function(data) {
//			var obj = JSON.parse(data);
//			if (obj.status == 'success')
			{
				$.fn.yiiGridView.update(gridId, {
					complete: function(jqXHR, status) {
						prepareDatePickers();
					}
				});
			}
		}
	});
}

function prepareDatePickers()
{
	var pickers = $('.inlineEditCalendar');
	for (var i = 0; i < pickers.length; i++)
	{
		$("#"+$(pickers[i]).attr('id')).datepicker({'dateFormat':$(pickers[i]).attr('alt')});
	}
}

/*ToolTip JS*/
function gridToolTipInit(obj){
	clearTimeout(timeout);
	var parent = obj.parentElement;
    if (parent.className != 'inlineEditTextField')
        parent = obj;
	var url = $(obj).attr('href');
	$.ajax({
		url: url,
		success: function(data) {
			$("#gridToolTip").html(data).appendTo(parent.parentElement).show();
		}
	});
}

function gridToolTipDivOver() {
	clearTimeout(timeout);
}

function gridToolTipClose() {
	hideGridToolTip("gridToolTip");
}

var timeout;

function hideGridToolTip(id)
{
	timeout = setTimeout( function(){
		$("#"+id).hide();
	}, 300 );
}