/*
 ### jQuery Like/Dislike MyBlog Plugin v1.0 - 2014-02-14 ###
 ###
 */

/*# AVOID COLLISIONS #*/
;
if (window.jQuery) (function ($) {
    /*# AVOID COLLISIONS #*/

    // plugin initialization
    $.fn.helpful = function (options) {
        if (this.length == 0) return this; // quick fail

        // Handle API methods
        if (typeof arguments[0] == 'string') {
            // Perform API methods on individual elements
            if (this.length > 1) {
                var args = arguments;
                return this.each(function () {
                    $.fn.helpful.apply($(this), args);
                });
            }
            ;
            // Invoke API method handler
            $.fn.helpful[arguments[0]].apply(this, $.makeArray(arguments).slice(1) || []);
            // Quick exit...
            return this;
        }
        ;

        // Initialize options for this call
        var options = $.extend(
            {}/* new object */,
            $.fn.helpful.options/* default options */,
            options || {} /* just-in-time options */
        );

        // Allow multiple controls with the same name by making each call unique
        $.fn.helpful.calls++;

        // loop through each matched element
        this
            .not('.docebo-helpful-applied')
            .addClass('docebo-helpful-applied')
            .each(function () {

                // Load control parameters / find context / etc
                var control, $el = $(this);
                var eid = (this.name || 'unnamed-helpful').replace(/\[|\]/g, '_').replace(/^\_+|\_+$/g, '');
                var context = $(this.form || document.body);

                $el.find('a').click(function(e) {
                    e.preventDefault();
                    var that = $(this);
                    if (that.parent().hasClass('readonly') || that.hasClass('my-vote'))
                        return;

                    $.post($(this).attr('href'), function(data) {
                        $el.find('a').removeClass('my-vote');
                        $el.find('.thumbs-up .count').html(data.likes);
                        $el.find('.thumbs-down .count').html(data.dislikes);
                        that.addClass('my-vote');
                    }, 'json');
                });

            }); // each element

        return this; // don't break the chain...
    };

    /*--------------------------------------------------------*/

    /*
     ### Core functionality and API ###
     */
    $.extend($.fn.helpful, {
        // Used to append a unique serial number to internal control ID
        // each time the plugin is invoked so same name controls can co-exist
        calls: 0

    });

    /*--------------------------------------------------------*/

    /*
     ### Default Settings ###
     eg.: You can override default control helpful this:
     $.fn.rating.options.cancel = 'Clear';
     */
    $.fn.helpful.options = { //$.extend($.fn.helpful, { options: {
        //NB.: These don't need to be pre-defined (can be undefined/null) so let's save some code!
        readOnly: false         // disable rating plugin interaction/ values cannot be
    }; //} });


    // auto-initialize plugin
    $(function(){
        $('.docebo-helpful').helpful();
    });

    /*--------------------------------------------------------*/

    /*# AVOID COLLISIONS #*/
})(jQuery);
/*# AVOID COLLISIONS #*/
