
var MainMenu = {};

(function ($) {

	MainMenu = function(options) {
		this.init(options);
	};

	MainMenu.prototype = {

		menu: false,

		tiles: [],

		tiles_div: [],

		links: [],

		links_fa: [],

		sub_menues: [],

		ajax_loaded: [],

		opened_onclick: false,

		disable_hover_open: false,

		second_gen_menu: false,

		// X button in Admin menu (new menu)
		close_menu_button: false,

		isReady: false,

		close_timer: null,
		open_timer: null,
		timer_delay: 100,

		init: function(options) {

			this.menu = $('#menu');

			this.tiles = $('#menu > li');
			this.tiles_div = $('#menu > li .tile');
			this.links = $('#menu > li .tile a');
			this.links_fa = $('#menu > li .tile a i');
			this.sub_menues = $('#menu > li div.second-menu');
			this.loStats = $("#menu #lo-stats");
			this.disable_hover_open = $('#menu').parent().hasClass('skip-menu-hover');
			this.second_gen_menu = $('#menu').parent().hasClass('second-gen-menu');
			this.close_menu_button = $('#admin.second-menu #close-button');

			this.updateLoStats();

			this.attachListener();

			if ($('#menu').parent().hasClass("animate")) {
				// perform intro animation
				this.animateIntro();
			}
			this.setupHelp();

			this.isReady = true;
		},

		animateIntro: function () {

			// fade in the main bar
			this.menu.parent().animate({
				opacity: 1
			}, 600);

			// let's make the tiles enter with some delay one from each other
			var timer = 250;
			this.tiles_div.each(function () {
				var tile = $(this);
				tile.queue('tiles', function (next) {
					tile.delay(timer).animate({
						left: 0
					}, 500, next);
				});
				tile.dequeue('tiles');
				timer += 75;
			});
		},

		setupHelp: function() {

            var $helpDesk = $('#help-desk-request');

            $(document).ready(function() {
                if ($.fn.styler) {
                    $helpDesk.find('input[type=radio]').styler();
                }
            });

            $helpDesk.find('button[type="reset"]').on('click', function(e) {
                $helpDesk.find('div[class*="request"]').removeClass("error");
                $helpDesk.find('div[class*="target"]').removeClass("error");
            });

            $helpDesk.on("submit", $.proxy(this.sendHelp, this));

            $('.menu-helpdesk-search .search input').on('keypress', function(e) {
                if (e.keyCode == 13) {
                    var q = $(this).val();
                    if (q.length) {
                        console.log('search.php?' + encodeURIComponent(q));
                    }
                }
            });
		},

        sendHelp: function(e) {

            // normal submit isn't needed
            var form = $(e.currentTarget);
            // check if there is some text writter
            var helpRequest = $.trim(form.find('#help-desk-text').val());
            if (!helpRequest.length) {
                form.find('div[class*="request"]').addClass("error");
                e.preventDefault();
                return;
            } else {
                form.find('div[class*="request"]').removeClass("error");
            }
            // check if a radio is selected
            if (!form.find('input[name="help-desk-target"]:checked').length) {
                form.find('div[class*="target"]').addClass("error");
                e.preventDefault();
                return;
            } else {
                form.find('div[class*="target"]').removeClass("error");
            }
            // ok at this point the standard submit is performed, the result will be display with flash message and the
            // user bringed back to the main page
        },

		attachListener: function() {
			this.links.on("click", $.proxy(this.open, this));

			// Open second menu on hover
			this.links.on("mouseenter", $.proxy(function(ev) {
				this.close.call(this, ev);
				this.open.call(this, ev);
			}, this));

			//this.links.on("mouseenter",this.open.bind(this));

			if (!this.second_gen_menu) {
				this.tiles.on("mouseleave", $.proxy(this.close, this));
			}

			// A hacky way to hide the menu when the main content is hovered
			// Fix for a bug where if you move the mouse too quickly over
			// the submenu, it stays open forever
			$('.container, .container-fluid').on('mouseenter', $.proxy(this.close, this));

			// Install Close handler for new Admin menu
			this.close_menu_button.on("click", $.proxy(this.close, this));


            // Add additional listener when cursor lands inside the menu
            $('.second-menu').on("mouseenter",$.proxy(this.resetTimers,this));
            $(window).blur($.proxy(this.close,this)); // close the menu when we jump to another browser tab
		},

		resetTimers: function(e) {
			clearTimeout(this.close_timer);
			clearTimeout(this.open_timer);
		},

		open: function(e) {
            this.resetTimers();
            // add timer delay for menu opening
            this.open_timer = setTimeout($.proxy(this.openInternal,this,e), this.timer_delay);
        },

		openInternal: function(e) {

			// Init menu tooltips if the new menu style is enabled !!!
			if (this.second_gen_menu) {
				// Hide callouts
				$('.callouts-wrapper:not(".btn-generic")').hide();
			}

			// Ensure all menu submenus are closed
			this.tiles_div.removeClass("active");
			this.sub_menues.removeClass("open");
			this.sub_menues.addClass("close");

			// make links gray
			this.tiles_div.addClass("gray_tile");

			// find the submenu for the current tile
			var tile = $(e.currentTarget).parent(),
				elem = tile.parent().find(".second-menu");

			// If menu we are opening is "globalsearch", call a method to load recent searches
			// Consequent opening will NOT load them again, unless we change "false" to "true" (forceLoad)
			if (elem.attr('id') === "globalsearch") {
				$.fn.globalsearch("loadRecent", false);
			}

			// Hide scrollbar if admin menu is entered
			if (this.second_gen_menu) {
				if($(tile).hasClass('sidebar-tile-admin')) {
					$('html').css('overflow', 'hidden');
				}
			}

			tile.addClass("active");
			// do this only if there is a submenu
			if (elem.length == 0) return;

			// prevent normal link to work
			e.preventDefault();
			if (elem.hasClass("open")) {
				// close the menu (toggle operation)

				if (this.opened_onclick) {
					this.opened_onclick = false;
					tile.removeClass("active");
					elem.removeClass("open").addClass("close");
				}
			} else {
				// if the event is on hover we open the menu only if another one was alredy opened,
				// otherwise the first action need to be a click
				if (e.type == 'click') {
					this.opened_onclick = true;
				}

				// Unconditionally Hide Play-list
				try {
					if (typeof Arena != "undefined") {
						Arena.hideLoNavigation();
					}
				} catch (exc) {}

				// if the submenu is closed, open it
				elem.removeClass("close").addClass("open");

				// ajax loader
				if ($(e.currentTarget).hasClass("ajax")) {
					// check if already loaded
					var url = $(e.currentTarget).attr("href"),
						target = '#'+$(e.currentTarget).data("target");
					if (this.ajax_loaded[target] == undefined) {
						this.ajax_loaded[target] = true;
						$(target).load(url);
					}
				}

			}
		},

		close: function(e) {
            clearTimeout(this.open_timer);
            // add timer delay for menu closing
            this.close_timer = setTimeout($.proxy(this.closeInternal,this,e), this.timer_delay);
		},

		closeInternal: function(e) {
			e.preventDefault();

			// Blur the focus from global search input box
			$('.menu #global-search-input').blur();

			// restore the overflow on the html
			if (this.second_gen_menu && !$('body').hasClass("erp")) {
				$('html').css('overflow', 'visible');
				$('body').css('overflow', 'visible');
			}

			// Destroy menu tooltips
			if (this.second_gen_menu) {
				// Show active callout if there is
				$('.callouts-wrapper:not(".hide")').show();
			}

			// make links gray
			this.tiles_div.removeClass("gray_tile");//"gray_item");

			var elem = $(e.currentTarget); // note: this may not always be a menu element
			this.tiles_div.removeClass("active");
			this.sub_menues.removeClass("open");
			this.sub_menues.addClass("close");
		},

		closeAll: function() {
			return;
		},

		/**
		 * Update completed/total counters in MainMenu->Play tile
		 */
		updateLoStats: function() {

			// Throttle this method
			if ( ! Throttle.canRun('updateLoStats', 5000)) return;

			if (this.loStats !== "undefined") {
				var second_gen_menu = this.second_gen_menu;
				this.loStats.load(this.loStats.data("href"), function() {
					// update menu tooltip if possible
					if (second_gen_menu) {
						var innerStatsHTML = Yii.t('standard', 'Course Chapters') + ' <strong>' +
								$('#menu-play-lo-stats-completed').html() + '</strong>' +'/'  + '<strong>'
								+ $('#menu-play-lo-stats-total').html() + '</strong>';

						$('.menu #course-play-tile').attr('data-original-title', innerStatsHTML);
					}
				});


			}
		}

	};

})(jQuery);