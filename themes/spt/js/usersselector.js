/**
 * JS compagnion to Selector UI/Dialog2
 * 
 *  Events fired:
 *  	userselector.closed.success(event, UsersSelector)
 *  
 *  Callbacks called:
 *  	options.successCallback(UsersSelector)
 *  	
 *  
 */
(function($){

	/**
	 * Return the same array, duplicates removed
	 */
	Array.prototype.getUnique = function(){
		   var u = {}, a = [];
		   for(var i = 0, l = this.length; i < l; ++i){
		      if(u.hasOwnProperty(this[i])) {
		         continue;
		      }
		      a.push(this[i]);
		      u[this[i]] = 1;
		   }
		   return a;
	}

	
	/**
	 * 
	 */
	UsersSelectorClass = function(options) {
		
		// Default options
		this.defaultOptions = {
			containerId 					: 'users-selector-container',	// contains all selector elements, surrounding the FORM as well
			dialogId						: 'users-selector',				// .modal-body ID
			dialogClass						: 'users-selector',				// .modal  class; See usersselector.css! This is hardcoded
			dialogTitle						: ' ',
			formId							: 'users-selector-form',		// Dialog global form
			orgchartsFancyTreeId			: 'fancytree-id',				// Fancytree ID
			selectedUsersFieldName			: 'selected_users',				// The INPUT name where selected users will be collected/saved
			selectedGroupsFieldName			: 'selected_groups',			// The INPUT name where selected Groups will be collected/saved
			selectedCoursesFieldName		: 'selected_courses',			// The INPUT name where selected Courses will be collected/saved
			selectedPlansFieldName			: 'selected_plans',				// The INPUT name where selected Plans will be collected/saved
			selectedPuProfilesFieldName		: 'selected_puprofiles',		// The INPUT name where selected PU Profiles will be collected/saved
			selectedCertificationsFieldName	: 'selected_certifications',		// The INPUT name where selected PU Profiles will be collected/saved
            selectedBadgesFieldName         : 'selected_badges',
			selectedContestsFieldName		: 'selected_contests',
			selectedAssetsFieldName			: 'selected_assets',
			selectedChannelsFieldName		: 'selected_channels',
			selectedExpertsFieldName		: 'selected_experts',

			successCallback					: false							// Name of a global JS function, called upon Close-Success
		};
		
		if (typeof options === "undefined" ) {
			var options = {};
		}
		
		// Initialize. This  would happens on page load only.
		this.init(options);
		
	}

	/**
	 * 
	 */
	UsersSelectorClass.prototype = {
			
		// Will hold selected users' IDs
		selectedUsers: 		[],    	
			
		// Will hold selected groups' IDs
		selectedGroups: 	[],   	
			
		// Will hold selected Courses' IDs
		selectedCourses: 	[],   	

		// Will hold selected Plans' IDs
		selectedPlans: 	[],   	
		
		// Will hold selected PU Profiles' IDs
		selectedPuProfiles: 	[],

        //Will hold selected Badges IDs
        selectedBadges:  [],

		selectedContests: [],
		
		selectedAssets: [],
		selectedChannels: [],
		
		/**
		 * Called when dialog is opened
		 * 
		 */
		init: function(options) {

			// Reset selections to default 
			this.selectedUsers = [];
			this.selectedGroups = [];
			this.selectedCourses = [];
			this.selectedPlans = [];
			this.selectedPuProfiles = [];
			this.selectedCertifications = [];
            this.selectedBadges = [];
			this.selectedContests = [];
			this.selectedAssets = [];
			this.selectedChannels = [];
			this.selectedExperts = [];

			// Get incoming options
			this.options = $.extend({}, this.defaultOptions, options);
			
			this.containerSelector = '#' + this.options.containerId;
			
			this.getSelectionsFromFormFields();
			
			// Add listeners
			this.addListeners();
			
			this.updateSelectionCounters();
			this.updateCurrentPageSelection(this.options.usersGridId		, 'selectedUsers');
			this.updateCurrentPageSelection(this.options.groupsGridId		, 'selectedGroups');
			this.updateCurrentPageSelection(this.options.coursesGridId		, 'selectedCourses');
			this.updateCurrentPageSelection(this.options.plansGridId		, 'selectedPlans');
			this.updateCurrentPageSelection(this.options.puProfilesGridId	, 'selectedPuProfiles');
			this.updateCurrentPageSelection(this.options.certificationGridId	, 'selectedCertifications');
            this.updateCurrentPageSelection(this.options.badgesGridId		, 'selectedBadges');
			this.updateCurrentPageSelection(this.options.contestsGridId		, 'selectedContests');
			this.updateCurrentPageSelection(this.options.assetsGridId		, 'selectedAssets');
			this.updateCurrentPageSelection(this.options.channelsGridId		, 'selectedChannels');
			this.updateCurrentPageSelection(this.options.expertsGridId		, 'selectedExperts');
			
			
			$('body').addClass(this.options.dialogClass + '-body');

			// Save .modal jQuery Object for further usage
			this.modalObject = $('#'+this.options.dialogId).closest('.modal');
			
			// If this dialog is running in jWizard  mode, anounce the required class for the dialog,
			// otherwise, just add the class to the dialog
			if ( (typeof jwizard !== 'undefined') && (typeof jwizard.setDialogClass == 'function') ) {
				jwizard.setDialogClass(this.options.dialogClass);
			}
			else {
				this.modalObject.addClass(this.options.dialogClass);
			}
			
			
			// Update title
			if (this.options.dialogTitle) {
				this.modalObject.find('.modal-header h3').html(this.options.dialogTitle);
			}


		},
		

		/**
		 * Called upon OrgChart tree node selection (set as a callback in Fancytree widget call)
		 */
		orgchartSelectCallBack: function(event, data) {
			setTimeout(function() {
				$('#orgcharts-selected-count').text(data.tree.countSelected());
			}, 200);
		},

		
		
		/**
		 * All listeners goes here: clicks, submissions, etc.
		 */
		addListeners: function() {
			var that = this;
			
			
			// Handle TAB icons. We are using "i-sprite"
		    $('#selector-tabs li.active a > span').addClass('white');
		    $( '#selector-tabs li > a' ).mouseenter(function() {
		        if (!$(this).closest('li').hasClass('active')) {
					$(this).children('span').addClass('white');
		        }
			});
		    $( '#selector-tabs li > a' ).mouseleave(function() {
		    	if (!$(this).closest('li').hasClass('active')) {
					$(this).children('span').removeClass('white');
		    	}
			});
		 	$('#selector-tabs li > a').on('shown', function (e) {
		 		$('#selector-tabs li > a > span').removeClass('white');
		 		$(this).children('span').addClass('white');
		 		
		 	});
			
			
			/**
			 * USERS grid events listeners (proxied)
			 */
			$(this.containerSelector + ' #users #users-search-input').	on('autocompleteselect', 	$.proxy(this.onAutoCompleteSelect, this));
			$(this.containerSelector + ' #users .select-all').			on('click', 				$.proxy(this.onSelectAllClick, this));
			$(this.containerSelector + ' #users .deselect-all').		on('click', 				$.proxy(this.onDeselectAllClick, this));
			$(this.containerSelector + ' #users-search-input').			live('keydown',				$.proxy(this.onSearchKey, this));
			$('input[name="'+this.options.selectedUsersFieldName+'"]').	on('change', 				$.proxy(this.onSelectedFieldChange, this));


			/**
			 * GROUPS grid events listeners (proxied)
			 */
			$(this.containerSelector + ' #groups #groups-search-input').on('autocompleteselect', 	$.proxy(this.onAutoCompleteSelect, this));
			$(this.containerSelector + ' #groups .select-all').			on('click', 				$.proxy(this.onSelectAllClick, this));
			$(this.containerSelector + ' #groups .deselect-all').		on('click', 				$.proxy(this.onDeselectAllClick, this));
			$(this.containerSelector + ' #groups-search-input').		live('keydown',				$.proxy(this.onSearchKey, this));
			$('input[name="'+this.options.selectedGroupsFieldName+'"]').on('change', 				$.proxy(this.onSelectedFieldChange, this));
			

			/**
			 * COURSES grid events listeners (proxied)
			 */
			$(this.containerSelector + ' #courses #courses-search-input').	on('autocompleteselect', 	$.proxy(this.onAutoCompleteSelect, this));
			$(this.containerSelector + ' #courses .select-all').			on('click',				$.proxy(this.onSelectAllClick, this));
			$(this.containerSelector + ' #courses .deselect-all').			on('click', 				$.proxy(this.onDeselectAllClick, this));
			$(this.containerSelector + ' #courses-search-input').			live('keydown',				$.proxy(this.onSearchKey, this));
			$('input[name="'+this.options.selectedCoursesFieldName+'"]').	on('change', 				$.proxy(this.onSelectedFieldChange, this));
			
			/**
			 * PLANS grid events listeners (proxied)
			 */
			$(this.containerSelector + ' #plans #plans-search-input').	on('autocompleteselect', 	$.proxy(this.onAutoCompleteSelect, this));
			$(this.containerSelector + ' #plans .select-all').			on('click',					$.proxy(this.onSelectAllClick, this));
			$(this.containerSelector + ' #plans .deselect-all').		on('click', 				$.proxy(this.onDeselectAllClick, this));
			$(this.containerSelector + ' #plans-search-input').			live('keydown',				$.proxy(this.onSearchKey, this));
			$('input[name="'+this.options.selectedPlansFieldName+'"]').	on('change', 				$.proxy(this.onSelectedFieldChange, this));
			
			/**
			 * PU Profiles grid events listeners (proxied)
			 */
			$(this.containerSelector + ' #puprofiles #puprofiles-search-input').	on('autocompleteselect', 	$.proxy(this.onAutoCompleteSelect, this));
			$(this.containerSelector + ' #puprofiles .select-all').					on('click',					$.proxy(this.onSelectAllClick, this));
			$(this.containerSelector + ' #puprofiles .deselect-all').				on('click', 				$.proxy(this.onDeselectAllClick, this));
			$(this.containerSelector + ' #puprofiles-search-input').				live('keydown',				$.proxy(this.onSearchKey, this));
			$('input[name="'+this.options.selectedPuProfilesFieldName+'"]').		on('change', 				$.proxy(this.onSelectedFieldChange, this));

			/**
			 * Certifications grid events listeners (proxied)
			 */
			$(this.containerSelector + ' #certification #certification-search-input').	on('autocompleteselect', 	$.proxy(this.onAutoCompleteSelect, this));
			$(this.containerSelector + ' #certification .select-all').					on('click',					$.proxy(this.onSelectAllClick, this));
			$(this.containerSelector + ' #certification .deselect-all').				on('click', 				$.proxy(this.onDeselectAllClick, this));
			$(this.containerSelector + ' #certification-search-input').					live('keydown',				$.proxy(this.onSearchKey, this));
			$('input[name="'+this.options.selectedCertificationsFieldName+'"]').			on('change', 				$.proxy(this.onSelectedFieldChange, this));

            /**
             * Badges grid events listeners (proxied)
             */
            $(this.containerSelector + ' #badges #badges-search-input').	on('autocompleteselect', 	$.proxy(this.onAutoCompleteSelect, this));
            $(this.containerSelector + ' #badges .select-all').					on('click',					$.proxy(this.onSelectAllClick, this));
            $(this.containerSelector + ' #badges .deselect-all').				on('click', 				$.proxy(this.onDeselectAllClick, this));
            $(this.containerSelector + ' #badges-search-input').					live('keydown',				$.proxy(this.onSearchKey, this));
            $('input[name="'+this.options.selectedBadgesFieldName+'"]').			on('change', 				$.proxy(this.onSelectedFieldChange, this));

			/**
			 * Contests grid events listeners (proxied)
			 */
			$(this.containerSelector + ' #contests #contests-search-input').	on('autocompleteselect', 	$.proxy(this.onAutoCompleteSelect, this));
			$(this.containerSelector + ' #contests .select-all').					on('click',					$.proxy(this.onSelectAllClick, this));
			$(this.containerSelector + ' #contests .deselect-all').				on('click', 				$.proxy(this.onDeselectAllClick, this));
			$(this.containerSelector + ' #contests-search-input').					live('keydown',				$.proxy(this.onSearchKey, this));
			$('input[name="'+this.options.selectedContestsFieldName+'"]').			on('change', 				$.proxy(this.onSelectedFieldChange, this));

			/**
			 * Assets grid events listeners (proxied)
			 */
			$(this.containerSelector + ' #assets #assets-search-input').		on('autocompleteselect', 	$.proxy(this.onAutoCompleteSelect, this));
			$(this.containerSelector + ' #assets .select-all').					on('click',					$.proxy(this.onSelectAllClick, this));
			$(this.containerSelector + ' #assets .deselect-all').				on('click', 				$.proxy(this.onDeselectAllClick, this));
			$(this.containerSelector + ' #assets-search-input').				live('keydown',				$.proxy(this.onSearchKey, this));
			$('input[name="'+this.options.selectedAssetsFieldName+'"]').		on('change', 				$.proxy(this.onSelectedFieldChange, this));

			/**
			 * Channels grid events listeners (proxied)
			 */
			$(this.containerSelector + ' #channels #channels-search-input').	on('autocompleteselect', 	$.proxy(this.onAutoCompleteSelect, this));
			$(this.containerSelector + ' #channels .select-all').				on('click',					$.proxy(this.onSelectAllClick, this));
			$(this.containerSelector + ' #channels .deselect-all').				on('click', 				$.proxy(this.onDeselectAllClick, this));
			$(this.containerSelector + ' #channels-search-input').				live('keydown',				$.proxy(this.onSearchKey, this));
			$('input[name="'+this.options.selectedChannelsFieldName+'"]').		on('change', 				$.proxy(this.onSelectedFieldChange, this));
			
			
			/**
			 * Experts grid events listeners (proxied)
			 */
			$(this.containerSelector + ' #experts #experts-search-input').	on('autocompleteselect', 	$.proxy(this.onAutoCompleteSelect, this));
			$(this.containerSelector + ' #experts .select-all').				on('click',					$.proxy(this.onSelectAllClick, this));
			$(this.containerSelector + ' #experts .deselect-all').				on('click', 				$.proxy(this.onDeselectAllClick, this));
			$(this.containerSelector + ' #experts-search-input').				live('keydown',				$.proxy(this.onSearchKey, this));
			$('input[name="'+this.options.selectedExpertsFieldName+'"]').		on('change', 				$.proxy(this.onSelectedFieldChange, this));
			
			
			
			
			/**
			 * Orgchart Select All/Deselect All clicks listener
			 */
			$(this.containerSelector + ' #orgchart .select-all').		on('click', 				$.proxy(this.onOrgchartSelectAllClick, this));
			$(this.containerSelector + ' #orgchart .deselect-all').		on('click', 				$.proxy(this.onOrgchartDeselectAllClick, this));
			
			
			/**
			 * Handle clicks on small 'x' buttons to clear search in all grids which support it (having the element)
			 */
			$(this.containerSelector).on('click','.grid-search-clear', $.proxy(this.onGridSearchClear, this));

			$(document).on('submit', '#'+this.options.formId, function(e) {
				$(document).trigger("users-selector-form-submission", that.collectSelectorData());
			});
		},

		collectSelectorData: function() {
			var data = {
				selectedOrgcharts : $('input[name="orgcharts_selected-json"]').length ? eval($('input[name="orgcharts_selected-json"]').val()) : [],
				selectedBadges: this.selectedBadges,
				selectedCertifications: this.selectedCertifications,
				selectedContests: this.selectedContests,
				selectedCourses: this.selectedCourses,
				selectedGroups: this.selectedGroups,
				selectedPlans: this.selectedPlans,
				selectedPuProfiles: this.selectedPuProfiles,
				selectedUsers: this.selectedUsers,
				selectedAssets: this.selectedAssets,
				selectedChannels: this.selectedChannels,
				selectedExperts: this.selectedExperts
			};

			return data;
		},

		/**
		 * Handle clicks on small "x" to clear the grid search input and update the grid
		 */
		onGridSearchClear: function(e) {
			var gridInfo = this.resolveGridInfoByEvent(e);
			$('#'+gridInfo.tabPane + ' input[name="search_input"]').val('');
			var data = {};
			$.fn.yiiGridView.update(gridInfo.gridId, data);
		},
		
		
		/**
		 * On SELECT ALL Click 
		 */
		onSelectAllClick: function(e) {
			
			var that = this;
			var gridInfo = this.resolveGridInfoByEvent(e);

			
			if (gridInfo.selectAllUrl === false) return;
			
			var data = {
				term: gridInfo.searchTerm
			};
			
			// Add selector parameters, whetever they are (passed to selector upon its creation/loading)
			for (var param in this.options.selectorParams) {
				data[param] = this.options.selectorParams[param];
			}
			
			// AJAX call to get list of IDs 
			$.post(gridInfo.selectAllUrl, data, function(data, textStatus, jqXHR){
				response = $.parseJSON(data);
				if (response.success === true) {
					for (var i in response.data) {response.data[i] = parseInt(response.data[i]); }
					that.addSelection(gridInfo.gridId, gridInfo.selectedArrayName, response.data);
					that.updateCurrentPageSelection(gridInfo.gridId, gridInfo.selectedArrayName);
					that.updateSelectionCounters();
				}
			});
		},
		

		/**
		 * On DE-SELECT ALL Clicks 
		 */
		onDeselectAllClick: function(e) {
			var gridInfo = this.resolveGridInfoByEvent(e);

			if ( (typeof gridInfo.selectedArrayName != 'string') || (typeof this[gridInfo.selectedArrayName] == "undefined")) return;
			
			this[gridInfo.selectedArrayName] = [];
			this.updateCurrentPageSelection(gridInfo.gridId, gridInfo.selectedArrayName);
			this.updateSelectionCounters();
		},
		


		
		/*
		 * Listen for keydown in search field
		 * On ENTER, update the grid
		 */
		onSearchKey: function(e) {
			var code = e.keyCode || e.which;
			if ((code == 13)) {
				var gridInfo = this.resolveGridInfoByEvent(e);			
				var data = {};
				$.fn.yiiGridView.update(gridInfo.gridId, data);
				$('.ui-autocomplete').hide();
				return false;
			}
			else {
				return true;
			}
			
		},
		
		
		/**
		 * IF/WHEN the hidden form field used to Submit the list of selected items is CHANGED, do the reverse: 
		 * i.e., select items in grid using the list FROM the field!
		 * 
		 */
		onSelectedFieldChange: function(e) {
			
			// NOTE: You can not use resolveGridInfoByEvent(e) here, because the hidden field is NOT part of any tab pane! 
			
			var fieldName = $(e.currentTarget).attr('name');
			var gridId = false;
			var selectedArrayName = false;
			
			switch (fieldName) {
				case this.options.selectedUsersFieldName:
					gridId = this.options.usersGridId;
					selectedArrayName = 'selectedUsers';
					break;
					
				case this.options.selectedGroupsFieldName:
					gridId = this.options.groupsGridId;
					selectedArrayName = 'selectedGroups';
					break;
					
				case this.options.selectedCoursesFieldName:
					gridId = this.options.coursesGridId;
					selectedArrayName = 'selectedCourses';
					break;
					
				case this.options.selectedPlansFieldName:
					gridId = this.options.plansGridId;
					selectedArrayName = 'selectedPlans';
					break;
					
				case this.options.selectedPuProfilesFieldName:
					gridId = this.options.puProfilesGridId;
					selectedArrayName = 'selectedPuProfiles';
					break;
				case this.options.selectedCertificationsFieldName:
					gridId = this.options.certificationGridId;
					selectedArrayName = 'selectedCertifications';
					break;
                case this.options.selectedBadgesFieldName:
                    gridId = this.options.badgesGridId;
                    selectedArrayName = 'selectedBadges';
                    break;
				case this.options.selectedContestsFieldName:
					gridId = this.options.contestsGridId;
					selectedArrayName = 'selectedContests';
					break;
				case this.options.selectedAssetsFieldName:
					gridId = this.options.assetsGridId;
					selectedArrayName = 'selectedAssets';
					break;
				case this.options.selectedChannelsFieldName:
					gridId = this.options.channelsGridId;
					selectedArrayName = 'selectedChannels';
					break;
				case this.options.selectedExpertsFieldName:
					gridId = this.options.expertsGridId;
					selectedArrayName = 'selectedExperts';
					break;
			}

			
			if (gridId === false) return;
			
			this.getSelectionsFromFormFields();
			this.updateCurrentPageSelection(gridId, selectedArrayName);
			this.updateSelectionCounters();
		},
		
		/**
		 * On Autocomplete item selection
		 * data: object, sent by Autocomplete script  data = { item: {label: <item-dropdown-display-text>, value: <item-dropdown-value> } } 
		 */
		onAutoCompleteSelect: function(e, data, xyz) {
			var gridInfo = this.resolveGridInfoByEvent(e);
			$('#' + gridInfo.gridId).yiiGridView('update', {data: {
				term: data.item.value
			}});
		},


		/********************************************* ORGCHART SPECIFIC ************************************************/
		
		/**
		 * On ORGCHART SELECT ALL
		 */
		onOrgchartSelectAllClick: function(e) {
			var tree = $('#' + this.options.orgchartsFancyTreeId).fancytree('getTree');
			tree.ext.docebo._selectAllNodes();
		},
		
		
		/**
		 * On ORGCHART DE-SELECT ALL
		 */
		onOrgchartDeselectAllClick: function(e) {
			var tree = $('#' + this.options.orgchartsFancyTreeId).fancytree('getTree');
			tree.ext.docebo._deSelectAllNodes();
		},
		

		
		/**
		 * Called before Grid Update Ajax call is made. 
		 * Use it to set additional POST variables/data, if any (like filtering)
		 */
		beforeGridAjaxUpdate: function(gridId, ajaxOptions) {
			var info = this.resolveInfoByGridId(gridId); 
			this.addAdditionalDataToGridUpdateAjaxRequest(ajaxOptions, info.searchInputSelector);
		},


		
		/**
		 * Called after Grid Update Ajax call is made & finished. 
		 */
		afterGridAjaxUpdate: function(gridId, data) {
			var info = this.resolveInfoByGridId(gridId);
			if (info.selectedArrayName === false) return;
			this.updateCurrentPageSelection(gridId, info.selectedArrayName);
		},
		

		/**
		 * Called Upon row/checkbox selection change in the grid 
		 */
		gridSelectionChanged: function(gridId) {

			var info = this.resolveInfoByGridId(gridId);
			
			if (info.selectedArrayName === false) return;
			
			// Get selections at the current grid PAGE
			var pageSelectionArray = $('#' + gridId).yiiGridView('getSelection');
			
			// Make it array of integers
			for (var i in pageSelectionArray) {pageSelectionArray[i] = parseInt(pageSelectionArray[i]); }

			// Update selector internal attributes holding selections
			this.updateSelectionArrayFromPage(gridId, info.selectedArrayName, pageSelectionArray);
			this.updateSelectionCounters();
			
		},		

		
		/**
		 * DIALOG2 listeners for 'auto-close' for our dialog (!).
		 * This is used when the dialog is standalone and not part of a Wizard, 
		 * in which case Wizard should take care about closing its dialogs
		 */
		addDialogAutoCloseListener: function() {
			var that = this;
			
			$(document).off("dialog2.content-update", ".modal-body#" + this.options.dialogId)
				.on("dialog2.content-update", ".modal-body#" + this.options.dialogId, function (e) {
					if ($(this).find("a.auto-close").length > 0) {
						
						// Check for 'success' and call a success callback, if any
						if ($(this).find("a.auto-close.success").length > 0) {
							
							// Trigger CLOSE+SUCCESS event
							$('#'+that.options.dialogId).trigger('userselector.closed.success', that);
							
							// Additionally, as an alternative, call assigned success-callback function, if defined. It must be JS Global
							if (false !== that.options.successCallback) {
								if (typeof window[that.options.successCallback] === 'function') {
									window[that.options.successCallback](that);
								}
							}
						}
						
						$(this).dialog2("close");
					}
				});
			
			$(document).off("dialog2.closed", ".modal-body#" + this.options.dialogId)
				.on("dialog2.closed", ".modal-body#" + this.options.dialogId, function () {
					$('body').removeClass(that.options.dialogClass + '-body');
					usersSelector = null;
				});
			
		}, 
		
		
		/**
		 * Vizualize (apply) current global selection on current page: check checkboxes, set row selection status, etc.
		 * Strangely enough, I can't find a nice API in yiigridview.js to do this in a single call.
		 * 
		 */
		updateCurrentPageSelection: function(gridId, selectedArrayName) {
			var that = this;
			var keys = [];
			$('#'+gridId).find('.keys span').each(function(){
				keys.push(parseInt($(this).text()));
			});
			
			var counter = 0;
			$('#' + gridId + ' .gridItemsContainer table.items').children('tbody').children().each(function (i) {
				if ($.inArray(keys[i], that[selectedArrayName]) > -1) {
					$(this).addClass('selected').find('input.select-on-check[type="checkbox"]').attr('checked', true);
					counter++;
				}
				else {
					$(this).removeClass('selected').find('input.select-on-check[type="checkbox"]').attr('checked', false);
				}
			});
			
			if (counter >= keys.length) {
				$('#' + gridId  + ' input.select-on-check-all').each(function(){
					$(this).attr('checked', true);
				});
			}
			else {
				$('#' + gridId  + ' input.select-on-check-all').each(function(){
					$(this).attr('checked', false);
				});
			}
			
		},
		
		
		
		
		
		/**
		 * Update selector's internal attributes holding selections.
		 * Called upon Grid Selection change, e.g. user selects or deselects an item, etc.
		 */
		updateSelectionArrayFromPage: function(gridId, selectedArrayName, selectionArray) {
			var that = this;
			var value = 0;
			var index = 0;
			
			// Enumerate all checkboxes in the given grid and update selected array
			$('#' + gridId + ' input[type="checkbox"]').not('.select-on-check-all').each(function(){
				value = parseInt($(this).val());
				if ($.inArray(value, selectionArray) > -1) {
					that[selectedArrayName].push(value);
				}
				else {
					index = $.inArray(value, that[selectedArrayName]);
					if (index > -1) {
						that[selectedArrayName].splice(index, 1);
					}
				}
			});
			
			// make selecetd array unique
			this[selectedArrayName] = this[selectedArrayName].getUnique();

			
		},

		
		
		/**
		 * Add list of IDs to selectedXXXX array
		 */
		addSelection: function(gridId, selectedArrayName, selectionArray) {
			$.merge(this[selectedArrayName], selectionArray);
			this[selectedArrayName] = this[selectedArrayName].getUnique();
		},
		
		
		
		/**
		 * 1) Update visual counters for grids 
		 * 2) PUT the list of selected items in the HIDDEN field used to submit the list later
		 */
		updateSelectionCounters: function() {
			$('#users-selected-count').text(this.selectedUsers.length);
			$('#groups-selected-count').text(this.selectedGroups.length);
			$('#courses-selected-count').text(this.selectedCourses.length);
			$('#plans-selected-count').text(this.selectedPlans.length);
			$('#puprofiles-selected-count').text(this.selectedPuProfiles.length);
			$('#certification-selected-count').text(this.selectedCertifications.length);
            $('#badges-selected-count').text(this.selectedBadges.length);
			$('#contests-selected-count').text(this.selectedContests.length);
			$('#assets-selected-count').text(this.selectedAssets.length);
			$('#channels-selected-count').text(this.selectedChannels.length);
			$('#experts-selected-count').text(this.selectedExperts.length);
			
			$('input[name="'+this.options.selectedUsersFieldName+'"]').val(JSON.stringify(this.selectedUsers));
			$('input[name="'+this.options.selectedGroupsFieldName+'"]').val(JSON.stringify(this.selectedGroups));
			$('input[name="'+this.options.selectedCoursesFieldName+'"]').val(JSON.stringify(this.selectedCourses));
			$('input[name="'+this.options.selectedPlansFieldName+'"]').val(JSON.stringify(this.selectedPlans));
			$('input[name="'+this.options.selectedPuProfilesFieldName+'"]').val(JSON.stringify(this.selectedPuProfiles));
			$('input[name="'+this.options.selectedCertificationsFieldName+'"]').val(JSON.stringify(this.selectedCertifications));
            $('input[name="'+this.options.selectedBadgesFieldName+'"]').val(JSON.stringify(this.selectedBadges));
			$('input[name="'+this.options.selectedContestsFieldName+'"]').val(JSON.stringify(this.selectedContests));
			$('input[name="'+this.options.selectedAssetsFieldName+'"]').val(JSON.stringify(this.selectedAssets));
			$('input[name="'+this.options.selectedChannelsFieldName+'"]').val(JSON.stringify(this.selectedChannels));
			$('input[name="'+this.options.selectedExpertsFieldName+'"]').val(JSON.stringify(this.selectedExperts));
		},
		

		
		/**
		 * This method is called by 'before grid update' callbacks.
		 * 
		 * Modify the POSTed data, basically adding more options and information passed to controller/dataprovider,
		 * based on current status of the grid, filter forms, etc.
		 * 
		 * Example: It checks if the ajax request already has the "term" parameter (data) in it (filtering).
		 * If NOT, it checks if there is "search term" in a given INPUT (by selector).
		 * If there is some, it adds the "term" parameter to ajax request data (through ajaxOptions)
		 * 
		 */
		addAdditionalDataToGridUpdateAjaxRequest: function(ajaxOptions, searchInputSelector) {

			var dataParams = {};
			if (typeof ajaxOptions.data == "string") {
				dataParams = $.deparam(ajaxOptions.data);
			}
			else {
				dataParams = ajaxOptions.data;
			}
			
			// All purpose; just haveing it in the POST
			dataParams.ajaxupdate = 1;
			
			// If there is NO "term" && there is some text in the SEARCH INPUT field...
			var searchTerm = $(searchInputSelector).val()
			if ( (typeof dataParams.term === "undefined") && (searchTerm !== '') ) {
				dataParams.term = searchTerm;
			}
			
			// Put back data into ajax options
			ajaxOptions.data = dataParams;
			
		},
		

		/**
		 * Inspect hidden field used to submit selection result and IF there is some value
		 * do the reverse: set current selection from them
		 */
		getSelectionsFromFormFields: function() {
			var tmp = '';
			tmp = $('input[name="'+this.options.selectedUsersFieldName+'"]').val();
			if ( (typeof tmp == "string") && tmp !== '') {
				this.selectedUsers = jQuery.parseJSON(tmp);
			}
			
			tmp = $('input[name="'+this.options.selectedGroupsFieldName+'"]').val();
			if ( (typeof tmp == "string") && tmp !== '') {
				this.selectedGroups = jQuery.parseJSON(tmp);
			}
			
			tmp = $('input[name="'+this.options.selectedCoursesFieldName+'"]').val();
			if ( (typeof tmp == "string") && tmp !== '') {
				this.selectedCourses = jQuery.parseJSON(tmp);
			}
			
			tmp = $('input[name="'+this.options.selectedPlansFieldName+'"]').val();
			if ( (typeof tmp == "string") && tmp !== '') {
				this.selectedPlans = jQuery.parseJSON(tmp);
			}
			
			tmp = $('input[name="'+this.options.selectedPuProfilesFieldName+'"]').val();
			if ( (typeof tmp == "string") && tmp !== '') {
				this.selectedPuProfiles = jQuery.parseJSON(tmp);
			}

			tmp = $('input[name="'+this.options.selectedCertificationsFieldName+'"]').val();
			if ( (typeof tmp == "string") && tmp !== '') {
				this.selectedCertifications = jQuery.parseJSON(tmp);
			}

            tmp = $('input[name="'+this.options.selectedBadgesFieldName+'"]').val();
            if ( (typeof tmp == "string") && tmp !== '') {
                this.selectedBadges = jQuery.parseJSON(tmp);
            }

			tmp = $('input[name="'+this.options.selectedContestsFieldName+'"]').val();
			if ( (typeof tmp == "string") && tmp !== '') {
				this.selectedContests = jQuery.parseJSON(tmp);
			}
			
			tmp = $('input[name="'+this.options.selectedAssetsFieldName+'"]').val();
			if ( (typeof tmp == "string") && tmp !== '') {
				this.selectedAssets = jQuery.parseJSON(tmp);
			}
			
			tmp = $('input[name="'+this.options.selectedChannelsFieldName+'"]').val();
			if ( (typeof tmp == "string") && tmp !== '') {
				this.selectedChannels = jQuery.parseJSON(tmp);
			}
			
			tmp = $('input[name="'+this.options.selectedExpertsFieldName+'"]').val();
			if ( (typeof tmp == "string") && tmp !== '') {
				this.selectedExperts = jQuery.parseJSON(tmp);
			}
		},
		
		
		/**
		 * Given an DOM element event, resolve all additional information we MAY need about the grid related to that element (if any)
		 * @param event
		 * @returns
		 */
		resolveGridInfoByEvent: function(event) {
			var curTarget = event.currentTarget;
			var gridId = false;
			var selectedArrayName = false;
			var tabPane = false;
			var searchTerm = '';
			var selectAllUrl = false;
			
			if ($(curTarget).parents(this.containerSelector + ' #users').length) {
				gridId = this.options.usersGridId;
				selectedArrayName = 'selectedUsers';
				tabPane = 'users';
				searchTerm = $(this.containerSelector + ' #users #users-search-input').val();
				selectAllUrl = this.options.selectAllUsersUrl;
			}
			else if ($(curTarget).parents(this.containerSelector + ' #groups').length) {
				gridId = this.options.groupsGridId;
				selectedArrayName = 'selectedGroups';
				tabPane = 'groups';
				searchTerm = $(this.containerSelector + ' #groups #groups-search-input').val();
				selectAllUrl = this.options.selectAllGroupsUrl;
			}
			else if ($(curTarget).parents(this.containerSelector + ' #courses').length) {
				gridId = this.options.coursesGridId;
				selectedArrayName = 'selectedCourses';
				tabPane = 'courses';
				searchTerm = $(this.containerSelector + ' #courses #courses-search-input').val();
				selectAllUrl = this.options.selectAllCoursesUrl;
			}
			else if ($(curTarget).parents(this.containerSelector + ' #plans').length) {
				gridId = this.options.plansGridId;
				selectedArrayName = 'selectedPlans';
				tabPane = 'plans';
				searchTerm = $(this.containerSelector + ' #plans #plans-search-input').val();
				selectAllUrl = this.options.selectAllPlansUrl;
			}
			else if ($(curTarget).parents(this.containerSelector + ' #puprofiles').length) {
				gridId = this.options.puProfilesGridId;
				selectedArrayName = 'selectedPuProfiles';
				tabPane = 'puprofiles';
				searchTerm = $(this.containerSelector + ' #puprofiles #puprofiles-search-input').val();
				selectAllUrl = this.options.selectAllPuProfilesUrl;
			}
			else if ($(curTarget).parents(this.containerSelector + ' #certification').length) {
				gridId = this.options.certificationGridId;
				selectedArrayName = 'selectedCertifications';
				tabPane = 'certification';
				searchTerm = $(this.containerSelector + ' #certification #certification-search-input').val();
				selectAllUrl = this.options.selectAllCertificationsUrl;
			}
			
			else if ($(curTarget).parents(this.containerSelector + ' #orgcharts').length) {
				gridId = false;
				selectedArrayName = false;
				tabPane = 'orgcharts';
				searchTerm = $(this.containerSelector + ' #orgcharts #orgcharts-search-input').val();
				selectAllUrl = false;
			}

            else if ($(curTarget).parents(this.containerSelector + ' #badges').length) {
                gridId = this.options.badgesGridId;
                selectedArrayName = 'selectedBadges';
                tabPane = 'badges';
                searchTerm = $(this.containerSelector + ' #badges #badges-search-input').val();
                selectAllUrl = this.options.selectAllBadgesUrl;
            }

			else if ($(curTarget).parents(this.containerSelector + ' #contests').length) {
				gridId = this.options.contestsGridId;
				selectedArrayName = 'selectedContests';
				tabPane = 'contests';
				searchTerm = $(this.containerSelector + ' #contests #contests-search-input').val();
				selectAllUrl = this.options.selectAllContestsUrl;
			}
			
			else if ($(curTarget).parents(this.containerSelector + ' #assets').length) {
				gridId = this.options.assetsGridId;
				selectedArrayName = 'selectedAssets';
				tabPane = 'assets';
				searchTerm = $(this.containerSelector + ' #assets #assets-search-input').val();
				selectAllUrl = this.options.selectAllAssetsUrl;
			}
			
			else if ($(curTarget).parents(this.containerSelector + ' #channels').length) {
				gridId = this.options.channelsGridId;
				selectedArrayName = 'selectedChannels';
				tabPane = 'channels';
				searchTerm = $(this.containerSelector + ' #channels #channels-search-input').val();
				selectAllUrl = this.options.selectAllChannelsUrl;
			}
			else if ($(curTarget).parents(this.containerSelector + ' #experts').length) {
				gridId = this.options.channelsGridId;
				selectedArrayName = 'selectedExperts';
				tabPane = 'experts';
				searchTerm = $(this.containerSelector + ' #experts #experts-search-input').val();
				selectAllUrl = this.options.selectAllExpertsUrl;
			}
			
			var info = {
					gridId: gridId,
					selectedArrayName: selectedArrayName,
					tabPane: tabPane,
					searchTerm: searchTerm,
					selectAllUrl: selectAllUrl
			};
			
			return info;
		},
		
		
		/**
		 * Given a grid ID, return related information
		 */
		resolveInfoByGridId: function(gridId) {
			
			var searchInputSelector = '';
			var selectedArrayName = false;
			
			if (gridId == this.options.usersGridId) {
				searchInputSelector = this.containerSelector + ' #users #users-search-input';
				selectedArrayName = 'selectedUsers';
			}
			else if (gridId == this.options.groupsGridId) {
				searchInputSelector = this.containerSelector + ' #groups #groups-search-input';
				selectedArrayName = 'selectedGroups';
			}
			else if (gridId == this.options.coursesGridId) {
				searchInputSelector = this.containerSelector + ' #courses #courses-search-input';
				selectedArrayName = 'selectedCourses';
			}
			else if (gridId == this.options.plansGridId) {
				searchInputSelector = this.containerSelector + ' #plans #plans-search-input';
				selectedArrayName = 'selectedPlans';
			}
			else if (gridId == this.options.puProfilesGridId) {
				searchInputSelector = this.containerSelector + ' #puprofiles #puprofiles-search-input';
				selectedArrayName = 'selectedPuProfiles';
			}
			else if (gridId == this.options.certificationGridId) {
				searchInputSelector = this.containerSelector + ' #certification #certification-search-input';
				selectedArrayName = 'selectedCertifications';
			}
            else if (gridId == this.options.badgesGridId) {
                searchInputSelector = this.containerSelector + ' #badges #badges-search-input';
                selectedArrayName = 'selectedBadges';
            }
			else if (gridId == this.options.contestsGridId) {
				searchInputSelector = this.containerSelector + ' #contests #contests-search-input';
				selectedArrayName = 'selectedContests';
			}
			else if (gridId == this.options.assetsGridId) {
				searchInputSelector = this.containerSelector + ' #assets #assets-search-input';
				selectedArrayName = 'selectedAssets';
			}
			else if (gridId == this.options.channelsGridId) {
				searchInputSelector = this.containerSelector + ' #channels #channels-search-input';
				selectedArrayName = 'selectedChannels';
			}
			else if (gridId == this.options.expertsGridId) {
				searchInputSelector = this.containerSelector + ' #experts #experts-search-input';
				selectedArrayName = 'selectedExperts';
			}



            // Fill info
			var info = {
					searchInputSelector: searchInputSelector,
					selectedArrayName: selectedArrayName
			}
			
			return info;
			
		},
		
		
		// keep this here to prevent putting COMMA after last property by accident, which kills IE :-)
		dummyCommaKiller: null
	};
	
})(jQuery);


