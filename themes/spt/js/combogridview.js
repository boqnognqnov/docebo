/**
 * 
 * 
 */
var CombogridViewClass = null; 
	
(function ( $ ) {

	var comboGridViewsArray = [];
	
	/**
	 * Return the same array, duplicates removed
	 */
	Array.prototype.getUnique = function(){
		   var u = {}, a = [];
		   for(var i = 0, l = this.length; i < l; ++i){
		      if(u.hasOwnProperty(this[i])) {
		         continue;
		      }
		      a.push(this[i]);
		      u[this[i]] = 1;
		   }
		   return a;
	}
	

	/**
	 * Sortable helper
	 */
	var fixHelper = function(e, ui) {
        ui.children().each(function() {
            $(this).width($(this).width());
        });
        ui.css('border', '2px dashed #ccc');
        return ui;
    };


    /**
     * 
     */
    var sortizeGrid = function(gridId, updateUrl, dragHandlerSelector) {
    	
    	$('#' + gridId + ' table.items tbody').sortable({
    		items: 'tr:not(.ui-state-disabled)',
    		helper: fixHelper,
    		cursor: "move",
    		opacity: 0.7,
    		tolerance: "pointer",
    		containment: "parent",
    		handle: dragHandlerSelector,
    		forcePlaceholderSize: true,
    		forceHelperSize: true,
    		key: 'items[]',
    		attribute: 'class',
			stop: function(e, ui){
				ui.item.css({
					opacity: '',
					border: ''
				});
			},
    		update: function(e, ui){
    			if ((typeof updateUrl === "string") && updateUrl !== '') {
    				var data = $.deparam($('#' + gridId + ' table.items tbody').sortable('serialize', {key: 'sorted[]', attribute: 'class'}));
    				var url = updateUrl;
					var $this = this;
    				$.post(url, data, function(response){
    					if (response.success) {
    						//$('#'+gridId).yiiGridView('update'); 
    					}
						$(document).trigger('combogridview-after-sortable-update', {instance: this, data: response});
    				},'json');
    			}
    		}
    	});
    }

    
	
	/**
	 * Constructor
	 */
	CombogridViewClass = function (options) {
		if (typeof options === "undefined" ) {
			var options = {};
		}
		this.init(options);
		
		if (this.options.sortableRows) {
			sortizeGrid(this.options.gridId, this.options.sortableUpdateUrl, this.options.dragHandlerSelector);
		}

	};


	/**
	 * Prototype
	 */
	CombogridViewClass.prototype = {
		
		defaultOptions: {
			debug: 							true,
			gridId:							"combo-grid",
			massSelectorUrl:				window.location.href 
		},
		
		options: {},
		
		/**
		 * Array of selected items
		 */
		selectedItems: 		[],
		
		$container: false,

		/**
		 * Initialize combo hrid object
		 */
		init: function(options) {
			var that = this;
			
			// Merge incoming options to default ones
			this.options = $.extend({}, this.defaultOptions, options);
			
			this.$container = $('#combo-grid-view-container-' + this.options.gridId);
			
			this.debug('Initialize ComboGridView');
			
			// Detach event listeners for Yii Gridview clicks. They will be attached again
			// This is to prevent double-listeners
			$(document).off('click.yiiGridView');
			
			this.addListeners();
			
			return this;

			
		},
		
		/**
		 * Make our live easier and provide some debug, if enabled
		 */
		debug: function(message) {
			if (this.options.debug === true) {
				Docebo.log('ComboGridView:', message);
			}
		},
		
		/**
		 * Set listeners
		 */
		addListeners: function() {
			this.debug('Attach event listeners');
			var that = this;
			var searchInputId = '#combo-grid-search-input-'+this.options.gridId;
			// Autocomplete item selected
			this.$container.find(searchInputId).on('autocompleteselect', $.proxy(this.onAutoCompleteSelect, this));
			
			// Clear search
			this.$container.find(".search-input-wrapper .clear-search").on('click', $.proxy(this.onClearSearchClick, this));

			// Catch ENTER key in search field
			this.$container.find(searchInputId).on('keydown', $.proxy(this.onSearchKeydown, this));

			// "search" icon clicked
			this.$container.find('.perform-search').on('click', $.proxy(this.onSearchButtonClick, this));
			
			// Select/Deselect all items
			this.$container.find('.combo-grid-select-all').on('click', $.proxy(this.onSelectAll, this));
			this.$container.find('.combo-grid-deselect-all').on('click', $.proxy(this.onDeSelectAll, this));
			
			// When massive action is selected
			this.$container.find('.combo-grid-selection-massive-action').on('change', $.proxy(this.onSelectionMassiveAction, this));
			
		},
		
		/**
		 * Get the form surrounding the grid. It MUST always be surrounded by a form!!!
		 */
		form: function() {
			return $('#' + this.options.gridId).closest('form');	
		},
		
		
		/**
		 * When Autcomplete item is selected
		 */
		onAutoCompleteSelect: function(e, data) {
			var $target = $(e.currentTarget);
			$target.val(data.item.value);
			this.updateGrid();
		},
		
		/**
		 * Update the grid: call grid's "update", feeding it with search form data (filter data)
		 */
		updateGrid: function() {
			$('#'+this.options.gridId).yiiGridView('update', {
				data: this.getFilterData()
			});
		},
		
		
		/**
		 * Serialize search form data and add more filtering attributes, if any
		 */
		getFilterData: function() {
			return this.form().serialize();
		},

		/**
		 * "X" clicked in the search input, to clear the search text
		 */
		onClearSearchClick: function(e) {
			this.form().find('#combo-grid-search-input-'+this.options.gridId).val('');
			this.updateGrid();
		},

		/**
		 * Catch keydown events in general text search field 
		 */
		onSearchKeydown: function(e) {
			var $target = $(e.currentTarget);
			var code = e.keyCode || e.which;
			if ((code == 13)) {
				e.preventDefault();
				this.updateGrid();
				return false;
			}
			else {
				return true;
			}
		},

		/**
		 * When "search icon" in general text search field is clicked 
		 */
		onSearchButtonClick: function(e) {
			this.updateGrid();
		},


		/**
		 * Select all requested
		 */
		onSelectAll: function(e) {
			var that = this;
			var $target = $(e.currentTarget);
			this.debug('Select All requested');

			
			$('#'+that.options.gridId).addClass('grid-view-loading');
			
			var url = that.options.massSelectorUrl;
			
			if (!url || (url === "") || (typeof url === "undefined"))
				url = window.location.href;
			
			var data = $.deparam(this.getFilterData());
			data.comboGridViewSelectAll = 1;

			$.post(url, data, function(data, textStatus, jqXHR){
				var response = $.parseJSON(data);
				that.addSelection(response);
				that.updateCurrentPageSelection();
				that.updateSelectionCounter();
				$('#'+that.options.gridId).removeClass('grid-view-loading');
			});
		},
		
		
		/**
		 * De-Select all requested
		 */
		onDeSelectAll: function(e) {
			var $target = $(e.currentTarget);
			this.debug('De-Select All requested');
			this.selectedItems = [];
			this.updateCurrentPageSelection();
			this.updateSelectionCounter();
		},
		
		
		onSelectionMassiveAction: function(e) {
			var $target = $(e.currentTarget);
			var data = {
				action: $target.val(),
				selection: this.getSelection(),
				gridId: this.options.gridId
			};

			// Trigger event to allow listeners to react
			$(document).trigger('combogridview.massive-action-selected', data);
		},
		
		/**
		 * Add an array of items IDs to current selection
		 */
		addSelection: function(selectionArray) {
			$.merge(this.selectedItems, selectionArray);
			this.selectedItems = this.selectedItems.getUnique();
		},


		/**
		 * Vizualize (apply) current global selection on current page: check checkboxes, set row selection status, etc.
		 * Strangely enough, I can't find a nice API in yiigridview.js to do this in a single call.
		 *
		 */
		updateCurrentPageSelection: function() {
			var that = this;
			var keys = [];
			var gridSelector = '#' + this.options.gridId;
			$(gridSelector).find('.keys span').each(function(){
				keys.push(parseInt($(this).text()));
			});

			var counter = 0;
			$(gridSelector + ' table.items').children('tbody').children().each(function (i) {
				if ($.inArray(keys[i], that.selectedItems) > -1) {
					$(this).addClass('selected').find('input.select-on-check[type="checkbox"]').attr('checked', true).trigger('refresh'); // Restyle as well
					counter++;
				}
				else {
					$(this).removeClass('selected').find('input.select-on-check[type="checkbox"]').attr('checked', false).trigger('refresh'); // Restyle as well
				}
			});

			if (counter >= keys.length) {
				$(gridSelector  + ' input.select-on-check-all').each(function(){
					$(this).attr('checked', true).trigger('refresh'); // Restyle as well
				});
			}
			else {
				$(gridSelector  + ' input.select-on-check-all').each(function(){
					$(this).attr('checked', false).trigger('refresh'); // Restyle as well
				});
			}

		},

		
		/**
		 * Update visual counters for grids
		 */
		updateSelectionCounter: function() {
			this.form().find('#combo-grid-selected-count').text(this.selectedItems.length);
		},
		
		
		/**
		 * Called Upon row/checkbox selection change in the grid
		 */
		gridSelectionChanged: function(gridId) {
			// Get selections at the current grid PAGE
			var pageSelectionArray = $('#' + gridId).yiiGridView('getSelection');

			// Make it array of integers
			for (var i in pageSelectionArray) {pageSelectionArray[i] = parseInt(pageSelectionArray[i]); }

			// Update selector internal attributes holding selections
			this.updateSelectionArrayFromPage(gridId, pageSelectionArray);
			this.updateSelectionCounter();

			$('#' + gridId).trigger('combogridview.selectionHasChanged', {selected: this.selectedItems});
		},
		
		/**
		 * Update selector's internal attributes holding selections.
		 * Called upon Grid Selection change, e.g. selects or deselects an item, etc.
		 */
		updateSelectionArrayFromPage: function(gridId, selectionArray) {
			var that = this;
			var value = 0;
			var index = 0;

			// Enumerate all checkboxes in the given grid and update selected array
			$('#' + gridId + ' input[type="checkbox"]').not('.select-on-check-all').each(function(){
				value = parseInt($(this).val());
				if ($.inArray(value, selectionArray) > -1) {
					that.selectedItems.push(value);
				}
				else {
					index = $.inArray(value, that.selectedItems);
					if (index > -1) {
						that.selectedItems.splice(index, 1);
					}
				}
			});

			// make selecetd array unique
			this.selectedItems = this.selectedItems.getUnique();
		},
		
		
		getSelection: function() {
			if (this.selectedItems)
				return this.selectedItems;
			else 
				return [];
		},
		
		
		setSelection: function(list) {
			this.selectedItems = list;
			this.updateCurrentPageSelection();
			this.updateSelectionCounter();
		},
		

		/**
		 * Return an instance of this
		 */
		getInstance: function() {
			return this;
		},


		/**
		 * To capture all updates to a specific ComboGridView:
		 * $(document).on('combogridview.grid-ajax-updated', function(element, options){
		 *      if(options.gridId === 'my-grid-id'){
		 *          // execute custom code
		 *      }
		 * }
		 */
		afterAjaxUpdate: function() {
			// Sortize, if requested
			if (this.options.sortableRows) {
				sortizeGrid(this.options.gridId, this.options.sortableUpdateUrl, this.options.dragHandlerSelector);
			}
			$(document).trigger('combogridview.grid-ajax-updated', this.options); 
		}
		
		
		
	};

	
	
	
	/**
	 * A little jQuery plugin to "attach" combogrid view JS to CGridView
	 * This will allow having many combo grid views in the same page.
	 */
	$.fn.comboGridView = function(method) {
		
		// Check for init call, where the argument is one and is an object (options)
		if (typeof method === 'object' || !method) {
			if (!method) method = {};
			method.gridId = $(this).attr('id');
			return comboGridViewsArray[method.gridId] = new CombogridViewClass(method);
		}

		// Else, we are called for some method. Get the object instance first (get out if none found)
		if (!comboGridViewsArray[$(this).attr('id')]) return false;
		
		var cgv = comboGridViewsArray[$(this).attr('id')];

		// Call the method
		if (cgv[method]) {
			return cgv[method].apply(cgv, Array.prototype.slice.call(arguments, 1));
		}
		else  {
			$.error('Method ' + method + ' does not exist on jQuery.comboGridView');
			return false;
		}
		
	}
	
	
	
}( jQuery ));
