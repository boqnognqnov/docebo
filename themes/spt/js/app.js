/*
 This file contains javascript functions available for anywhere in the application
 */

// Break out of frame (when coming back from Marketplace)
if (!location.href.indexOf('authoring/index.php?r=main/play')) {
	if (top.location != location)
		top.location.href = document.location.href;
}

/**
 * Docebo is initialized in /common/components/Controller.php.
 *
 */
var Docebo = {
	// Internally used, holding current options
	// Enable AJAX response filtering by default
	_options: {ajaxResponseFilterEnabled: true},
	/**
	 * Should be called ONLY ONCE (currently called in /common/components/Controller.php
	 */
	init: function (options) {
        this.setOptions(options);
	},
	ready: function () {
        // After page is reloaded, handle "placeholder"s for IE <= 9
        replacePlaceholder();
        var ngr = Docebo.getQueryParam("ngr");
        if (ngr && (typeof ngr !== "undefined")) {
            var readyUrl = this.convertUrlToHash(window.location.href, ngr);
			if (window.history.pushState) {
                window.history.pushState({}, '', readyUrl);
            } else {
                window.location = readyUrl;
            }
            return false;
        }
	},
    convertUrlToHash: function(url, hash) {
//          clear old hash if exists
        url = url.split('#')[0];
//          remove the ?ngr param from url
		url = this.delQueryParam(window.location.href, 'ngr');
//      check if hash not start with SLASH adding SLASH at begining
		if (hash.split('')[0] !== '/') {
            hash = '/' + hash;
        }
        return (url + "#" + hash);
    },
//  isIE: function() {
//      var myNav = navigator.userAgent.toLowerCase();
//      return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
//  },
	/**
	 * Given an URL, return specific Get/Query parameter from it
	 */
	getQueryParam: function (name, url) {
		if (!url)
			url = window.location.href;
	    name = name.replace(/[\[\]]/g, "\\$&");
	    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
	        results = regex.exec(url);
		if (!results)
			return null;
		if (!results[2])
			return '';
	    return decodeURIComponent(results[2].replace(/\+/g, " "));
	},
	/**
	 * Given an URL, remove a Get/Query parameter from it
	 * @TODO IMPLEMENT THIS PLEASE
	 */
	delQueryParam: function (url, parameter) {
        //prefer to use l.search if you have a location/link object
       var urlparts = url.split('?');
       if (urlparts.length >= 2) {

           var prefix = encodeURIComponent(parameter) + '=';
           var pars = urlparts[1].split(/[&;]/g);

           //reverse iteration as may be destructive
           for (var i = pars.length; i-- > 0; ) {
               //idiom for string.startsWith
               if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                   pars.splice(i, 1);
               }
           }

           url = urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : "");
           return url;
       } else {
           return url;
       }
	},

	/**
	 * Given an URL, remove HASH from it (# and everything after it)
	 * @TODO IMPLEMENT THIS PLEASE
	 */
	delQueryHash: function (url) {
		if (!url)
			url = window.location.href;
		return url;
	},

	/**
	 * Can be called many times to change/add options
	 */
	setOptions: function (options) {
		// Deep Extend _options to allow consequitive call to add more options.
		this._options = $.extend(true, this._options, options);

		// Create object attribute for every _options[k]
		for (var k in this._options)
			this[k] = this._options[k];
	},
	/**
	 * Browser-side email validation. Check it server-side as well!
	 */
	validateEmail: function (email) {
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	},
	/**
	 * Very simple logging method
	 */
	log: function () {
		if (window.console && window.console.log) {
			var args = Array.prototype.slice.call(arguments);
			if (args) {
				$.each(args, function (index, ev) {
					window.console.log(ev);
				});
			}
		}
	},
	/**
	 * Disable $.ajaxFilter-ing
	 */
	skipAjaxFilter: function () {
		this.ajaxResponseFilterEnabled = false;
	},
	/**
	 * Enable $.ajaxFilter-ing
	 */
	activateAjaxFilter: function () {
		this.ajaxResponseFilterEnabled = true;
	},
	/**
	 * Scroll the page to a selector
	 */
	scrollTo: function (targetSelector) {
		$('html,body').animate({scrollTop: $(targetSelector).offset().top}, 1000);
	},

	getHydraAccessToken: function(){
		if(this._options && typeof this._options.hydraAccessToken !== "undefined"){
			return this._options.hydraAccessToken;
		}
	},
	/**
	 * Object used to display alert messages into the existing #dcb-feedback container available in every layout
	 */
	Feedback: {
		settings: {
			// if the currently shown element's margin is less than this value
			margin: 100
		},
		show: function (type, message, showdismiss, autohide, multipleMessages, msgContainer) {
			showdismiss = showdismiss === undefined ? true : showdismiss;
			autohide = autohide === undefined ? true : autohide;
			var multipleMessages = multipleMessages === undefined ? false : multipleMessages;
			var msgContainer = msgContainer === undefined ? false : msgContainer;
			var html = '', typeClass = '', extraHtml = '';

			switch (type) {
				case 'success':
					typeClass = 'alert-success';
					extraHtml = '<span class="i-sprite is-check green"></span> ';
					break;
				case 'error':
					typeClass = 'alert-error';
					extraHtml = '<span class="i-sprite is-remove red"></span> ';
					break;
				case 'info':
					typeClass = 'alert-info';
					extraHtml = '<span class="i-sprite is-solid-exclam blue"></span> ';
					break;
				default:
					break;
			}

			if (multipleMessages === true) {
				message.forEach(function (single_message, iteration) {
					html += '<div class="alert ' + typeClass + ' in alert-block fade">' +
							(showdismiss ? '<button type="button" class="close" data-dismiss="alert">&times;</button>' : '') +
							extraHtml +
							Yii.t("standard", single_message) +
							'</div>';
				});
			} else {
				html += '<div class="alert ' + typeClass + ' in alert-block fade">' +
						(showdismiss ? '<button type="button" class="close" data-dismiss="alert">&times;</button>' : '') +
						extraHtml +
						message +
						'</div>';
			}


			// Make posible Error/Success message container to be changed
			var $feedbackDiv;
			if (msgContainer !== false) {
				$feedbackDiv = $(msgContainer);
			} else {
				$feedbackDiv = $('#dcb-feedback');
			}

			if (autohide) {
				$feedbackDiv.html(html).show(0).delay(5000).slideUp(1000);
			}
			else {
				$feedbackDiv.html(html).show();
			}

			//scroll if neccessary
			var docviewTop = $(window).scrollTop();
			var top = $feedbackDiv.offset().top;

			//distance between docviewTop & min.
			var topDistance = top - docviewTop;

			if (topDistance < this.settings.margin) //the element too up above
				$('html,body').animate({
					scrollTop: top - this.settings.margin},
				'slow');

			$feedbackDiv.trigger('docebo-feedback-shown');
		},
		hide: function () {
			$('#dcb-feedback').hide();
		}

	},
	Modal: {
		/**
		 * Centers the current modal dialog in the browser's window
		 */
		CenterPosition: function () {

			var $modal = $('.modal:visible');

			var w = $modal.width(),
					h = $modal.height(),
					ww = $(window).width(),
					wh = $(window).height();

			var cssPosition = 'absolute',
					cssTop = '50%',
					cssMarginTop = (-(h / 2)) + 'px',
					cssLeft = '50%',
					cssMarginLeft = (-(w / 2)) + 'px';

			if (ww < w) {
				cssLeft = 0;
				cssMarginLeft = 0;
			}
			else if (wh < h) {
				cssTop = 0;
				cssMarginTop = 0;
			}
			else {
				cssPosition = 'fixed';
			}
			;

			$modal.css({
				top: cssTop,
				marginTop: cssMarginTop,
				left: cssLeft,
				marginLeft: cssMarginLeft,
				position: cssPosition
			});
		}
	},
	/**
	 *
	 */
	CalloutMessages: {
		queue: [],
		/**
		 *
		 */
		fixArrowPositions: function () {
			// find the visible callout and position it absolutely if it's of a specific type
			var $callout = $('.callouts-wrapper:not(.hide)');

			var newMenuType = false;
			if ($('.menu.second-gen-menu').length > 0) {
				newMenuType = true;
			}

			if ($callout.data('target')) {
				var $target = $('.' + $callout.data('target'));

				if ($target.length > 0) {
					// For old legacy menu
					if (!newMenuType) {
						$callout.css({
							top: $target.offset().top, // +6
							//left: $('#menu').width() + 10
							left: $('.menu').position().left + $('.menu').width() + 20
						});
					} else { // For the new menu
						$callout.css({
							top: $target.offset().top - 6,
							left: $('.menu').position().left + $('.menu').width() + 15
						});
					}

				} else {
					$callout.find('.close').trigger('click');
				}
			}
		},
		/**
		 *
		 */
		showNext: function () {
			if (Docebo.CalloutMessages.queue.length > 0) {
				var $msg = $(Docebo.CalloutMessages.queue.shift());

				$msg.parent().removeClass('hide');

				$msg.find('.close').click(function (e) {
					e.preventDefault();
					var $msg = $(this).parent();

					// mark the message as read
					var msgId = $msg.data('id');
					$.post(Docebo.CalloutMessages.settings.readUrl, {id: msgId});

					$msg.parent().remove();
					Docebo.CalloutMessages.showNext();
				});

				Docebo.CalloutMessages.fixArrowPositions();
			}
		},
		/**
		 *
		 */
		init: function () {
			var messages = $('.callouts-wrapper > .message');

			for (var i = 0; i < messages.length; i++) {
				Docebo.CalloutMessages.queue.push(messages[i]);
			}

			$(window).resize(Docebo.CalloutMessages.fixArrowPositions).trigger('resize');

			// if it is an old legacy menu do fix the arrow position, otherwise we don't need it for the new menu
			if ($('.menu.second-gen-menu').length == 0) {
				$(window).on('scroll', Docebo.CalloutMessages.fixArrowPositions);
			}

			Docebo.CalloutMessages.showNext();
		},
		/**
		 *
		 */
		ready: function (oSettings) {
			Docebo.CalloutMessages.settings = $.extend(Docebo.CalloutMessages.settings, oSettings);
			Docebo.CalloutMessages.init();
		}
	},
	/**
	 * Create Absolute URL like
	 * http://mylms.docebosaas.com/<appName>/index.php?r=<route>&param1=value1&param2=value2....
	 *
	 * @route string
	 * @parameters Object
	 * @appName string
	 *
	 */
	createAbsoluteUrl: function (route, parameters, appName) {
		var base = this.absLmsUrl;
		if (typeof appName !== "undefined") {
			appName = appName.toLowerCase().replace(/\b[a-z]/g, function (letter) {
				return letter.toUpperCase();
			});
			var prop = 'abs' + appName + 'Url';
			if (this.hasOwnProperty(prop)) {
				base = this[prop];
			}
			else {
				Docebo.log('Warning: unknown App name, defaults to lms');
			}
		}
		else {
			Docebo.log('Warning: Missing App name, defaults to lms');
		}
		return base + '?r=' + route + '&' + jQuery.param(parameters);
	},
	/**
	 * Name says it all
	 */
	createAbsoluteLmsUrl: function (route, parameters) {
		return this.createAbsoluteUrl(route, parameters, 'lms');
	},
	/**
	 * Name says it all
	 */
	createAbsoluteAdminUrl: function (route, parameters) {
		return this.createAbsoluteUrl(route, parameters, 'admin');
	},
	/**
	 * Name says it all
	 */
	createAbsoluteApp7020Url: function (route, parameters) {
		return this.createAbsoluteUrl(route, parameters, 'app7020');
	},
	
	/**
	 * Clone a form, including "select" and "textarea" dynamic states
	 * Those fields must have "name" attributes set!
	 */
	cloneForm: function(form) {
		var copy = form.clone();
		$("select,textarea",form).each(function(){
			$('[name="'+$(this).attr("name")+'"]', copy).val($(this).val());
		});
		return copy;
	},
	
	isUrlValid: function(url) {
	    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
	}



};


/*
 * Avoid getting hard-to-find bugs due to widely used console.log.
 * In IE8+, console.log **IS** available, but only after you open Dev Tools (F12). Very stupid!
 * So, make a dummy function and if you are interested in console.log you gonna open Dev Tool anyway and
 * console.log() will be ok.
 */
if (typeof console === "undefined" || typeof console.log === "undefined") {
	var console = {};
	console.log = function () {
	};
}


/**
 * IE8 does not have Date.now(), lets 'emulate' it
 */
Date.now = Date.now || function () {
	return +new Date;
};

/**
 * fix for IE8
 */
if (!Object.keys) {
	Object.keys = (function() {
		'use strict';
		var hasOwnProperty = Object.prototype.hasOwnProperty,
			hasDontEnumBug = !({ toString: null }).propertyIsEnumerable('toString'),
			dontEnums = [
				'toString',
				'toLocaleString',
				'valueOf',
				'hasOwnProperty',
				'isPrototypeOf',
				'propertyIsEnumerable',
				'constructor'
			],
			dontEnumsLength = dontEnums.length;

		return function(obj) {
			if (typeof obj !== 'object' && (typeof obj !== 'function' || obj === null)) {
				throw new TypeError('Object.keys called on non-object');
			}

			var result = [], prop, i;

			for (prop in obj) {
				if (hasOwnProperty.call(obj, prop)) {
					result.push(prop);
				}
			}

			if (hasDontEnumBug) {
				for (i = 0; i < dontEnumsLength; i++) {
					if (hasOwnProperty.call(obj, dontEnums[i])) {
						result.push(dontEnums[i]);
					}
				}
			}
			return result;
		};
	}());
}

/**
 * Return the same array having duplicates removed
 * @param array
 * @returns array
 */
function arrayUnique(array) {
	var a = array.concat();
	for (var i = 0; i < a.length; ++i) {
		for (var j = i + 1; j < a.length; ++j) {
			if (a[i] === a[j])
				a.splice(j--, 1);
		}
	}

	return a;
}
;


/**
 * Opens a Dialog2
 */
function openDialog(event, title, url, id, modalClass, ajaxType, data) {
	if (typeof id == "undefined" || id === null) {
		id = "dialog_content";
	}

	if (typeof ajaxType == "undefined" || ajaxType === null) {
		ajaxType = "GET";
	}

	var settings = {
		title: title,
		content: url,
		ajaxType: ajaxType, // POST, GET, null
		id: id,
		modalClass: modalClass,
		closeOnOverlayClick: false
	};

	if (typeof data !== "undefined") {
		settings.data = data;
	}

	$('<div/>').dialog2(settings);
	return false;
}

$(function () {
	// [plamen] Just a note:
	// While this is a convenient approach to easy the Dialog2 closing process, it MAY have side effects someties.
	// If you ever notice strange Dialog2 closing behavior or something NOT executed in the order you expect, check this out.
	$(document).on('dialog2.content-update', '.modal', function () {
		var e = $(this);
		var autoclose = e.find(".auto-close");
		var autoredirect = e.find('.auto-close.redirect');

		if (autoredirect.length > 0) {
			var target = autoredirect.attr('href');
			if (target.length) {
				window.location = target;
			}
		}
		if (autoclose.length > 0) {
			e.find('.modal-body').dialog2("close");
		}
	})
});


/**
 * Enumerates inputs that might have 'placeholder' attribute and handles it.
 * This is a "hackish" fix for browsers not supporting "placeholder" (IE <= 9).
 * See:  /themes/spt/js/jquery.textPlaceholder.js
 */
function replacePlaceholder(replace) {
	var placeholder = 'placeholder' in document.createElement('input');
	if (!placeholder && typeof replace !== 'undefined') {
		$(":input").each(function () {
			$(this).textPlaceholder();
		});
	}
	;
}
;


// String.trim()
if (!String.prototype.trim) {
	String.prototype.trim = function () {
		return this.replace(/^\s+|\s+$/g, '');
	};
}


// Array.last()
if (!Array.prototype.last) {
	Array.prototype.last = function () {
		return this[this.length - 1];
	};
}
;
// A function to truncate a string: Somestrin.trunc(30)
if (!String.prototype.trunc) {
	String.prototype.trunc = function (n, useWordBoundary) {
		var isTooLong = this.length > n,
				s_ = isTooLong ? this.substr(0, n - 1) : this;
		s_ = (useWordBoundary && isTooLong) ? s_.substr(0, s_.lastIndexOf(' ')) : s_;
		return  isTooLong ? s_ + '&hellip;' : s_;
	};
}
;


// Array.filter()
if (!Array.prototype.filter) {
	Array.prototype.filter = function (fun/*, thisArg*/) {
		'use strict';

		if (this === void 0 || this === null) {
			throw new TypeError();
		}

		var t = Object(this);
		var len = t.length >>> 0;
		if (typeof fun !== 'function') {
			throw new TypeError();
		}

		var res = [];
		var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
		for (var i = 0; i < len; i++) {
			if (i in t) {
				var val = t[i];

				// NOTE: Technically this should Object.defineProperty at
				//       the next index, as push can be affected by
				//       properties on Object.prototype and Array.prototype.
				//       But that method's new, and collisions should be
				//       rare, so use the more-compatible alternative.
				if (fun.call(thisArg, val, i, t)) {
					res.push(val);
				}
			}
		}

		return res;
	};
}

// String.capitalize()
if (!String.prototype.capitalize) {
	String.prototype.capitalize = function () {
		return this.charAt(0).toUpperCase() + this.slice(1);
	}
}


/***********************/
/** AJAX data filter ***/
/***********************/

/**
 * Prevent JS and CSS re-loading in ajax calls.
 *
 * Note: This is very important method: It affects all ajax calls, filtering the respones.
 * Cahnces are, it may cause some unwanted effects! Improvements are welcome,
 * but without such filtering, plenty of functionality will be broken
 * (like Popups loaded by ajax, having forms and bootstrap dropdowns).
 *
 * This is related to Yii partial rendering views with processOutput = true,
 * where some JS/CSS can be repeatedly loaded during AJAX calls and break the
 * loaded page behavior.
 *
 */
$.ajaxSetup({
	global: true,
	cache: true, // New option added 22 Sep 2015 to try to avoid jQuery loading JS again and again in Ajax calls

	dataFilter: function (data, type) {

		//xmlhttp = new XMLHttpRequest();
		//Docebo.log(xmlhttp.readyState);

		//jqXHR = $.ajax();
		//Docebo.log(jqXHR);

		// Check if currently AJAX response filtering is enabled; just return data if not.
		if (!Docebo.ajaxResponseFilterEnabled)
			return data;

		// No type ? Return!
		if (!type) {
			return data;
		}

		// Make sure we don't miss 'JSON' or 'jSoN' :-)
		type = type.toLowerCase();

		// We are interested only in html, text and json data types
		if (type && type != 'html' && type != 'text' && type != 'json') {
			return data;
		}

		// This is very important:
		// Ajax RESPONSES from our code, if JSON, if sending back HTML code for display,
		// MUST be of the form data = {html: 'here goes the html string', .. other_arbitrary_object_attributes: 12, ...  }
		// The point is: use data.html as container for the HTML you want to control; otherwise it will just passed through!
		var jsonData;
		var html;
		if (type == 'json') {
			jsonData = $.parseJSON(data);
			// If there is NO html attribute, just return data
			try {
				if (!jsonData.html) {
					return data;
				}
			}
			catch (e) {
				return data;
			}
			// .. or get that damned HTML already
			html = jsonData.html;
		}
		else {
			// If not JSNO, we assume that data IS the html
			html = data;
		}

		// I can't remember WHY we had to filter SCRIPTS and NOT CSS... but looks like it is ok to filter both
		//var selector = 'script[src]';
		var selector = 'script[src],link[rel="stylesheet"]';

		// We keep loaded scripts/css in $._loadedScripts
		if (!$._loadedScripts) {
			$._loadedScripts = {};
			// We gonna put incoming html in this work-container... later
			$._dataHolder = $(document.createElement('div'));

			// Lets check what we have in the whole document
			var loadedScripts = $(document).find(selector);

			// Enumerate all js/css (resources! i.e. <script ... src="..."> <link ... href="..">
			// Save them in our global basket of loaded resources
			for (var i = 0, len = loadedScripts.length; i < len; i++) {
				if (loadedScripts[i].src) {
					$._loadedScripts[loadedScripts[i].src] = 1;
				}
				else if (loadedScripts[i].href) {
					$._loadedScripts[loadedScripts[i].href] = 1;
				}
			}
		}

		// Put incoming html into work-div
		if ($.browser.msie && $.browser.version < 9) {
			$._dataHolder[0].innerHTML = "<span style='display:none;'>&nbsp;</span>" + html;
		} else {
			$._dataHolder[0].innerHTML = html;
		}

		// iterate over new scripts and remove if source is already in DOM:
		var incomingScripts = $($._dataHolder).find(selector);

		for (var i = 0, len = incomingScripts.length; i < len; i++) {
			if (incomingScripts[i].src) {
				//Docebo.log('ajaxFilter: Incoming Script: ' + incomingScripts[i].src);

				if ($._loadedScripts[incomingScripts[i].src]) {
					//Docebo.log('ajaxFilter: SKipped:  ' + incomingScripts[i].src);
					$(incomingScripts[i]).remove();
				}
				else {
					$._loadedScripts[incomingScripts[i].src] = 1;
				}
			}
			else if (incomingScripts[i].href) {
				//Docebo.log('ajaxFilter: Incoming Stylesheet: ' + incomingScripts[i].href);

				if ($._loadedScripts[incomingScripts[i].href]) {
					//Docebo.log('ajaxFilter: Skipped:  ' + incomingScripts[i].href);
					$(incomingScripts[i]).remove();
				}
				else {
					$._loadedScripts[incomingScripts[i].href] = 1;
				}
			}
		}

		// If incoming type is JSON.. recompose, stringify.. return...
		if (type == 'json') {
			jsonData.html = $._dataHolder[0].innerHTML;
			return JSON.stringify(jsonData);
		}
		else {
			return $._dataHolder[0].innerHTML;
		}

		// mmm... that's it
	}

});



$(function () {

	/**
	 * Global ajaxSuccess listener
	 *
	 * Globally Listens for all ajaxSuccess and check HTTP Header X-Ajax-Redirect-Url.
	 * If it does exist redirect user to that URL.
	 *
	 * Server side, use the HEADER whenever you see an Ajax Call has been made, but
	 * you want to redirect user after ajax call is finished (forced redirect!).
	 * For example session is expired or access is denied for some reason.
	 *
	 */
	$(document).bind('ajaxSuccess', function (event, request, settings) {
		try {
			if (request.getResponseHeader('X-Ajax-Redirect-Url')) {
				var url = request.getResponseHeader('X-Ajax-Redirect-Url');
				// Redirect after a bit of timeout to let user read the short message
				setTimeout(function () {
					window.location = url;
				}, 3000);
			}
			;
		}
		catch (e) {
			// do nothing
		}
	});

	/**
	 * Redirect to '/' if we get 'bad request' during ajax calls.
	 * The idea to handle Ajax error at this level is not the best one though (plamen)
	 *
	 * This was added to avoid endless loading dialogs if for some reason the ajax call is BAD or Not Authorized
	 *
	 */
	$(document).bind('ajaxError', function (event, request, settings) {
		if (request.status == '400') {   // bad request
			window.location = '/';
		}
	});


	//$("[rel='tooltip']").tooltip();
	if (jQuery().tooltip) {
		$("[rel='tooltip']").tooltip();
	}

	// We fake input placeholders on some IE versions
	// so emulate the click-to-empty effect on textareas there
	$('#player-blocks-container').on('click', '.player-comments-block textarea[name="opening_message_text"]', function () {
		var placeholder = $(this).attr('placeholder');
		var html = $(this).html();
		if (placeholder && html && placeholder == html) {
			$(this).html('');
		}
	});

	$('body').on('click', 'a.readmore', function (e) {
		e.preventDefault();
		$(this).parent().hide().parent().find('.readmore-container').show();
	});
	$('body').on('click', 'a.readless', function (e) {
		e.preventDefault();
		$(this).parent().hide().parent().find('.readless-container').show();
	});

});





(function ($) {
	/**
	 * jQuery ext: Show Feedback div at the top/bottom of the caller DOM;
	 */
	$.fn.feedback = function (type, message, showdismiss, extraClass, append) {
		var $this = $(this);
		var html = '', typeClass = '';
		var $feedbackDiv = $('<div class="element-feedback" id="' + $this.attr('id') + '-feedback"></div>');
		var autohide = true;

		// Remove previos feedbacks, if any
		$this.find('#' + $this.attr('id') + '-feedback').remove();

		switch (type) {
			case 'success':
				typeClass = 'alert-success' + ' ' + extraClass;
				break;
			case 'warning':
				typeClass = 'alert-warning' + ' ' + extraClass;
				break;
			case 'error':
				typeClass = 'alert-error' + ' ' + extraClass;
				break;
			default:
				break;
		}

		style = 'opacity: 0.5; filter: alpha(opacity = 50);';
		html += '<div class="alert ' + typeClass + ' in alert-block fade">' +
				(showdismiss ? '<button type="button" class="close" data-dismiss="alert">&times;</button>' : '') +
				message +
				'</div>';

		$feedbackDiv.remove();
		$feedbackDiv.html(html);
		if (append)
			$this.append($feedbackDiv);
		else
			$this.prepend($feedbackDiv);

		if (autohide)
			$feedbackDiv.show(0).delay(5000).slideUp(1000);

	};


}(jQuery));

(function ($) {

	$.fn.serializeFormJSON = function () {

		var o = {};
		var a = this.serializeArray();
		$.each(a, function () {
			if (o[this.name] !== undefined) {
				if (!o[this.name].push) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};

}(jQuery));

function arrangeJson(data) {
	var initMatch = /^([a-z0-9]+?)\[/i;
	var first = /^\[[a-z0-9]+?\]/i;
	var isNumber = /^[0-9]$/;
	var bracers = /[\[\]]/g;
	var splitter = /\]\[|\[|\]/g;

	for (var key in data) {
		if (initMatch.test(key)) {
			data[key.replace(initMatch, '[$1][')] = data[key];
		}
		else {
			data[key.replace(/^(.+)$/, '[$1]')] = data[key];
		}
		delete data[key];
	}


	for (var key in data) {
		processExpression(data, key, data[key]);
		delete data[key];
	}

	function processExpression(dataNode, key, value) {
		var e = key.split(splitter);
		if (e) {
			var e2 = [];
			for (var i = 0; i < e.length; i++) {
				if (e[i] !== '') {
					e2.push(e[i]);
				}
			}
			e = e2;
			if (e.length > 1) {
				var x = e[0];
				var target = dataNode[x];
				if (!target) {
					if (isNumber.test(e[1])) {
						dataNode[x] = [];
					}
					else {
						dataNode[x] = {}
					}
				}
				processExpression(dataNode[x], key.replace(first, ''), value);
			}
			else if (e.length == 1) {
				dataNode[e[0]] = value;
			}
		}
	}
}

$(function () {

	$(window).bind("resize", function () {
		if ($(this).width() < 770) {
			$('div').removeClass('hidden-phone');
		}
	})
});

// Make d3 works with IE11 that emulate IE8

// https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Array/map
// Production steps of ECMA-262, Edition 5, 15.4.4.19
// Reference: http://es5.github.com/#x15.4.4.19
if (!Array.prototype.map) {
	Array.prototype.map = function (callback, thisArg) {

		var T, A, k;

		if (this == null) {
			throw new TypeError(" this is null or not defined");
		}

		// 1. Let O be the result of calling ToObject passing the |this| value as the argument.
		var O = Object(this);

		// 2. Let lenValue be the result of calling the Get internal method of O with the argument "length".
		// 3. Let len be ToUint32(lenValue).
		var len = O.length >>> 0;

		// 4. If IsCallable(callback) is false, throw a TypeError exception.
		// See: http://es5.github.com/#x9.11
		if ({}.toString.call(callback) != "[object Function]") {
			throw new TypeError(callback + " is not a function");
		}

		// 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
		if (thisArg) {
			T = thisArg;
		}

		// 6. Let A be a new array created as if by the expression new Array(len) where Array is
		// the standard built-in constructor with that name and len is the value of len.
		A = new Array(len);

		// 7. Let k be 0
		k = 0;

		// 8. Repeat, while k < len
		while (k < len) {

			var kValue, mappedValue;

			// a. Let Pk be ToString(k).
			//   This is implicit for LHS operands of the in operator
			// b. Let kPresent be the result of calling the HasProperty internal method of O with argument Pk.
			//   This step can be combined with c
			// c. If kPresent is true, then
			if (k in O) {

				// i. Let kValue be the result of calling the Get internal method of O with argument Pk.
				kValue = O[ k ];

				// ii. Let mappedValue be the result of calling the Call internal method of callback
				// with T as the this value and argument list containing kValue, k, and O.
				mappedValue = callback.call(T, kValue, k, O);

				// iii. Call the DefineOwnProperty internal method of A with arguments
				// Pk, Property Descriptor {Value: mappedValue, Writable: true, Enumerable: true, Configurable: true},
				// and false.

				// In browsers that support Object.defineProperty, use the following:
				// Object.defineProperty(A, Pk, { value: mappedValue, writable: true, enumerable: true, configurable: true });

				// For best browser support, use the following:
				A[ k ] = mappedValue;
			}
			// d. Increase k by 1.
			k++;
		}

		// 9. return A
		return A;
	};
}

// https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Array/forEach
// Production steps of ECMA-262, Edition 5, 15.4.4.18
// Reference: http://es5.github.com/#x15.4.4.18
if (!Array.prototype.forEach) {

	Array.prototype.forEach = function (callback, thisArg) {

		var T, k;

		if (this == null) {
			throw new TypeError(" this is null or not defined");
		}

		// 1. Let O be the result of calling ToObject passing the |this| value as the argument.
		var O = Object(this);

		// 2. Let lenValue be the result of calling the Get internal method of O with the argument "length".
		// 3. Let len be ToUint32(lenValue).
		var len = O.length >>> 0; // Hack to convert O.length to a UInt32

		// 4. If IsCallable(callback) is false, throw a TypeError exception.
		// See: http://es5.github.com/#x9.11
		if ({}.toString.call(callback) != "[object Function]") {
			throw new TypeError(callback + " is not a function");
		}

		// 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
		if (thisArg) {
			T = thisArg;
		}

		// 6. Let k be 0
		k = 0;

		// 7. Repeat, while k < len
		while (k < len) {

			var kValue;

			// a. Let Pk be ToString(k).
			//   This is implicit for LHS operands of the in operator
			// b. Let kPresent be the result of calling the HasProperty internal method of O with argument Pk.
			//   This step can be combined with c
			// c. If kPresent is true, then
			if (k in O) {

				// i. Let kValue be the result of calling the Get internal method of O with argument Pk.
				kValue = O[ k ];

				// ii. Call the Call internal method of callback with T as the this value and
				// argument list containing kValue, k, and O.
				callback.call(T, kValue, k, O);
			}
			// d. Increase k by 1.
			k++;
		}
		// 8. return undefined
	};
}

// https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Array/indexOf
if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function (searchElement /*, fromIndex */) {
		"use strict";
		if (this == null) {
			throw new TypeError();
		}
		var t = Object(this);
		var len = t.length >>> 0;
		if (len === 0) {
			return -1;
		}
		var n = 0;
		if (arguments.length > 0) {
			n = Number(arguments[1]);
			if (n != n) { // shortcut for verifying if it's NaN
				n = 0;
			} else if (n != 0 && n != Infinity && n != -Infinity) {
				n = (n > 0 || -1) * Math.floor(Math.abs(n));
			}
		}
		if (n >= len) {
			return -1;
		}
		var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
		for (; k < len; k++) {
			if (k in t && t[k] === searchElement) {
				return k;
			}
		}
		return -1;
	}
}

// https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Array/filter
if (!Array.prototype.filter)
{
	Array.prototype.filter = function (fun /*, thisp */)
	{
		"use strict";

		if (this == null)
			throw new TypeError();

		var t = Object(this);
		var len = t.length >>> 0;
		if (typeof fun != "function")
			throw new TypeError();

		var res = [];
		var thisp = arguments[1];
		for (var i = 0; i < len; i++)
		{
			if (i in t)
			{
				var val = t[i]; // in case fun mutates this
				if (fun.call(thisp, val, i, t))
					res.push(val);
			}
		}

		return res;
	};
}

// https://github.com/mbostock/d3/issues/199#issuecomment-1487129
if (typeof CSSStyleDeclaration !== 'undefined')
{
	if (!CSSStyleDeclaration.prototype.getProperty)
		CSSStyleDeclaration.prototype.getProperty = function (a) {
			return this.getAttribute(a);
		};

	if (!CSSStyleDeclaration.prototype.setProperty)
		CSSStyleDeclaration.prototype.setProperty = function (a, b) {
			return this.setAttribute(a, b);
		};

	if (!CSSStyleDeclaration.prototype.removeProperty)
		CSSStyleDeclaration.prototype.removeProperty = function (a) {
			return this.removeAttribute(a);
		};
}


// https://github.com/mbostock/d3/issues/42#issuecomment-1265410
if (!document.createElementNS)
	document.createElementNS = function (ns, name) {
		return document.createElement(name);
	};


if (typeof Object.create !== 'function') {
	Object.create = function (o) {
		function F() {
		}
		F.prototype = o;
		return new F();
	};
}

if (!window.getComputedStyle) {
	window.getComputedStyle = function (el, pseudo) {
		this.el = el;
		this.getPropertyValue = function (prop) {
			var re = /(\-([a-z]){1})/g;
			if (prop == 'float')
				prop = 'styleFloat';
			if (re.test(prop)) {
				prop = prop.replace(re, function () {
					return arguments[2].toUpperCase();
				});
			}
			return el.currentStyle[prop] ? el.currentStyle[prop] : null;
		}
		return this;
	}
}

if (Object.defineProperty && Object.getOwnPropertyDescriptor &&
		Object.getOwnPropertyDescriptor(Element.prototype, "textContent") &&
		!Object.getOwnPropertyDescriptor(Element.prototype, "textContent").get)
	(function () {
		var innerText = Object.getOwnPropertyDescriptor(Element.prototype, "innerText");
		Object.defineProperty(Element.prototype, "textContent",
				{// It won't work if you just drop in innerText.get
					// and innerText.set or the whole descriptor.
					get: function () {
						return innerText.get.call(this)
					},
					set: function (x) {
						return innerText.set.call(this, x)
					}
				}
		);
	})();



/**
 * Try to throttle tasks/calls: limit tasks to run way too frequently
 * Must be used very carefully and on your own risk!!!!
 *
 * 	 Usage:
 * 	    // While running some function, ask the Throttler if it (the function/task) can run, by giving the task a name
 *      // 'myarbitrarytaskname' and a minmum interval of 5000 MILIseconds.
 * 		// Throttler will examine the internal list of tasks and when they ran for the last time (miliseconds since epoch)
 * 	    // and if the task is trying to run again in less than the minInterval, returns false.
 *  	if ( ! Throttle.canRun('myarbitrarytaskname', 5000)) {
 *			return;
 *		}
 *
 */
var Throttle = {
	// Keeps list LAST time named tasks have been ran
	lastRuns: [],
	// The only method, doing it all: keep LAST times and check if task can be run
	// NOTE: minInterval is in MILISECONDS!!!!
	canRun: function (task, minInterval) {

		if (typeof minInterval === "undefined" || minInterval == null) {
			minInterval = 5000; // miliseconds, by default
		}

		minInterval = parseInt(minInterval)

		if (this.lastRuns.hasOwnProperty(task) && ((new Date().getTime() - this.lastRuns[task]) < minInterval)) {
			return false;
		}

		// Keep the last run of this task as NOW (miliseconds since epoch)
		this.lastRuns[task] = new Date().getTime();

		return true;
	}

};

/*************************************************************************/
/******************* Bootstrap tabs smooth active line *******************/
/*************************************************************************/
function boostrapSmoothActiveLine(element, options) {
	this.$element = null;
	this.$options = null;
	this.init(element, options);
}
boostrapSmoothActiveLine.prototype = {
	VERSION: '1.2',
	DEFAULTS: {
		lineClass: 'boostrapSmoothActiveLine',
		debug: false,
		background: '#0465AC',
		height: '4px',
		autoTrigger: true
	},
	init: function (element, options) {
		this.$element = $(element);
		this.$options = $.extend({}, this.DEFAULTS, options);

		if (this.getOptions().debug) {
			console.log("boostrapSmoothActiveLine.init() :: ", this.getOptions());
		}
		this.create();
	},
	getDefaults: function () {
		return this.DEFAULTS;
	},
	getOptions: function () {
		return this.$options;
	},
	destroy: function () {
		this.$element.removeData('boostrapSmoothActiveLine');
		this.$element.find('.' + this.getOptions().lineClass).remove();
	},
	recalc: function () {
		var $line = this.$element.find('.' + this.getOptions().lineClass);
		var $el = this.$element.find('li.active');
		$line.stop().animate({
			left: $el.position().left,
			width: $el.width()
		});
	},
	create: function () {
		this.$element.css({
			position: 'relative'
		});
		this.$element.append('<li class="' + this.getOptions().lineClass + '"></li>');
		var $line = this.$element.find('.' + this.getOptions().lineClass);
		$line.css({
			position: 'absolute',
			bottom: '0',
			left: '0px',
			width: '100px',
			height: this.getOptions().height,
			background: this.getOptions().background
		});
		var $el = this.$element.find('li').first();
		if (this.$element.find('li.active').length != 0) {
			$el = this.$element.find('li.active');
		}
		$line.width($el.width());
		$line.css({left: $el.position().left});

		if(this.getOptions().autoTrigger)
		this.$element.on('click', 'li:not(.skip)', function () {
			$line.stop().animate({
				left: $(this).position().left,
				width: $(this).width()
			});
		});
	},
}
/* boostrapSmoothActiveLine PLUGIN DEFINITION */
function BoostrapSmoothActiveLinePlugin(option) {
	return this.each(function () {
		var $this = $(this);
		var data = $this.data('boostrapSmoothActiveLine');
		var options = typeof option == 'object' && option;
		if (!data)
			$this.data('boostrapSmoothActiveLine', (data = new boostrapSmoothActiveLine(this, options)))
		if (typeof option == 'string')
			data[option]()
	});
}
$.fn.boostrapSmoothActiveLine = BoostrapSmoothActiveLinePlugin;
/*************************************************************************/
/*************************************************************************/

/*************************************************************************/
/******************* Bootstrap tabs smooth active line V2 *******************/
/*************************************************************************/
function activeLine(element, options) {
	this.$element = null;
	this.$options = null;
	this.init(element, options);
}

activeLine.prototype = {
	'default': {
		background: '#0465AC',
		height: '5px',
		debug: false
	},
	init: function (element, options) {
		this.$element = $(element);
		this.$options = $.extend({}, this['default'], options);

		if (this.$options.debug)
			console.log("ActiveLinePlugin.init() :: ", this.$options);
		this.create();
	},
	create: function () {
		var $this = this;
		var ul = this.$element;
		var li = ul.find('li');
		var a = li.find('a');

		/* Set active */
		if (!li.hasClass('active'))
			li.first().addClass('active');

		/* Generate line */
		li.children('.activeLine').remove();
		ul.find('.active').append('<i class="activeLine"></i>');

		/* Set position of <li> */
		li.css({
			position: 'relative'
		});

		/* Predefine line style and apply */
		ul.find('.active .activeLine').css({
			display: 'block',
			position: 'absolute',
			width: '100%',
			bottom: '0px',
			height: $this.$options.height,
			background: $this.$options.background
		});
	},
	refresh: function () {
		var $this = this;
		var ul = this.$element;
		var li = ul.find('li');
		var a = li.find('a');
		var next = ul.find('.active:not(.skip)');

		if(typeof next === 'undefined' || !next.length)
			return;

		var cLeft = li.children('.activeLine').closest('li').position().left;
		var nLeft = next.position().left - ul.position().left;
		li.children('.activeLine')
			.stop().animate({
			left: nLeft - cLeft,
			width: next.width()
		}, function () {
			li.children('.activeLine').remove();
			next.append(this);
			$(this).css({
				left: '0',
				width: '100%'
			});
		});
	}
};

function ActiveLinePlugin(option) {
	return this.each(function () {
		var $this = $(this);
		var data = $this.data('activeLine');
		var options = typeof option == 'object' && option;
		if (!data)
			$this.data('activeLine', (data = new activeLine(this, options)))
		else
			data['refresh']()
		if (typeof option == 'string')
			data[option]()
	});
}
$.fn.activeLine = ActiveLinePlugin;

/*************************************************************************/
/***************************** Dropdown image ****************************/
/*************************************************************************/
function dropdownImage_prototype(element, options) {
	this.$element = null;
	this.$options = null;
	this.init(element, options);
}
dropdownImage_prototype.prototype = {
	VERSION: '0.4',
	DEFAULTS: {
		debug: false,
	},
	init: function (element, options) {
		this.$element = $(element);
		this.$options = $.extend({}, this.DEFAULTS, options);

		if (this.getOptions().debug) {
			console.log("DropdownImage.init() :: ", this.getOptions());
		}
		this.create();
	},
	getDefaults: function () {
		return this.DEFAULTS;
	},
	getOptions: function () {
		return this.$options;
	},
	create: function () {
		var elementWidth = this.$element.css('width');
		var targetElement = this.$element.data('toggle');
		var targetDropdown = $('.' + targetElement);
		targetDropdown.css('width', elementWidth);
		var that = this;
		this.$element.on('mousedown', function (e) {
			e.preventDefault();
			targetDropdown.show();
			if (typeof that.getOptions().open !== "undefined" && typeof that.getOptions().open === "function") {
				that.getOptions().open();
			}

			$('.' + targetElement + ' li').off('click');

			$('.' + targetElement + ' li').each(function () {
				$(this).on('click', function () {
					var value = $(this).data('value');
					that.$element.html('<option value="' + value + '">' + $(this).text() + '</option>');
					targetDropdown.hide();
					if (typeof that.getOptions().select !== "undefined" && typeof that.getOptions().select === "function") {
						that.getOptions().select(this);
					}
				});
			});
		});

		$('body').on('mousedown', function (e) {
			if ($(e.target).closest(that.$element).length == 0 && $(e.target).closest(targetDropdown).length == 0) {
				targetDropdown.hide();
			}
		});
	}
}

/**
 * Passing a message to the top window if the current page is considered lodade in an iframe
 */
function sendMessageOnBeforeUnload()
{
	// window.onbeforeunload = function()
	// {
	// 	window.top.postMessage('lms-legacy:onbeforeunload', '*');
	// }
}
// not sure if needed, took the below function as example guess,
// somewhere might be needed as a visibility scope workaround?
$.fn.sendMessageOnBeforeUnload = sendMessageOnBeforeUnload;

function DropdownImagePlugin(option) {
	return this.each(function () {
		var $this = $(this);
		var data = $this.data('dropdownImage');
		var options = typeof option == 'object' && option;
		if (!data) {
			$this.data('dropdownImage', (data = new dropdownImage_prototype(this, options)));
		}
		if (typeof option == 'string')
			data[option]()
	});
}
$.fn.dropdownImage = DropdownImagePlugin;

/**
 * Scroll to the top of the screen when the modal is displayed
 */
$('document').ready(function () {
	// bootstrap modals
	$('body').on('show', '.modal:not(.in,:has(>div.modal-body.opened))', function () {
		document.body.scrollTop = document.documentElement.scrollTop = 0;
	});
	// dialog2
	$(document).on('dialog2.opened', function () {
		document.body.scrollTop = document.documentElement.scrollTop = 0;
	});

	$(document).on('click', 'a[data-edit="userprofile"]',  function(e){
		e.preventDefault();
		var element = $(this);
		openDialog(false, element.attr('title'), element.attr('href'), null, 'modal-user-profile', 'POST');
	});

    $(document).on('click', 'a[data-edit="my-profile"]',  function(e){
        e.preventDefault();
        if(window.parent)
            window.parent.postMessage({action: "legacy.updateRoute", message:'/learn/my-profile'}, '*');
    });
});


/**
 * Code for closing the datepicker pop-up on page scrolling
 */
$(document).on(
	'DOMMouseScroll mousewheel scroll',
	'body',
	function()
	{
		var pickers = $('.datepicker.dropdown-menu');
		var modal_with_pickers = $('.modal.in [data-date-format], .modal.in [data-date-language]');
		if(pickers && modal_with_pickers)
		{
			pickers.each(function(index){
				if($(this).css('display') == 'block' && !$(this).hasClass('session-dates'))
				{
					$(this).hide();
				}
			});
		}
	}
);

/*************************************************************************/
/***************************** Get URL Route *****************************/
/*************************************************************************/
function getUrlRoute(returnOpt) {
	var returnOpt = (typeof returnOpt === "undefined") ? false : returnOpt;

	var currentAction = 'site/index';
	var urlParamsStr = window.location.search;
	if (urlParamsStr.length > 0) {
		urlParamsStr = urlParamsStr.replace('?', '');
		var locObj = $.deparam(urlParamsStr);

		if ('r' in locObj) {
			currentAction = locObj['r'];
		}

		if (returnOpt && ('opt' in locObj)) {
			currentAction += '&opt=' + locObj['opt'];
		}
	}

	return currentAction;
}
/*************************************************************************/
/*************************************************************************/
