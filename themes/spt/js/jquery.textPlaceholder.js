(function( $ ){


   $.fn.textPlaceholder = function() {  
      var input = this;
      var text = input.attr('placeholder');  
      if (text) input.val(text).addClass('placeholder');
      input.focus(function(){  
         if (input.val() === text) input.css({ color:'transparent' }).selectRange(0,0).one('keydown', function(){
        	 input.val("").removeClass('placeholder');
        	 input.val("").css({ 'color':'#333333' });  
         });
      });
      input.blur(function(){ 
         if (input.val() == "" || input.val() === text) input.val(text).addClass('placeholder');
      }); 
      input.keyup(function(){ 
        if (input.val() == "") input.val(text).css({ color:'transparent' }).selectRange(0,0).one('keydown', function(){
            input.val("").css({ 'color':'#333333' });
            input.val("").removeClass('placeholder');
        });               
      });
      input.mouseup(function(){
        if (input.val() === text) input.selectRange(0,0); 
      });   
   };

   $.fn.selectRange = function(start, end) {
      return this.each(function() {
        if (this.setSelectionRange) { this.setSelectionRange(start, end);
        } else if (this.createTextRange) {
            var range = this.createTextRange();
            range.collapse(true); 
            range.moveEnd('character', end); 
            range.moveStart('character', start); 
            range.select(); 
        }
      });
   };

})( jQuery );
