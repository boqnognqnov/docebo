$(document).ready(function () {
	$('.advanced-search-link').click(function () {
		$('.group').toggleClass('group-padding');
	});

	$('.main-actions ul.clearfix li a').mouseenter(function () {
		var attrAlt = $(this).attr('alt');
		var aText = $(this).text();

		$('.info p').text(attrAlt);
		$('.info h4').append('<span></span>' + aText);
		$('.info').show();
	});

	$('.main-actions li a').mouseleave(function () {
		var attrAlt = $(this).attr('alt');

		$('.info p').text('');
		$('.info h4').text('');
		$('.info').hide();
	});


	$('.nodeTree-selectNode').live('click', function () {
		$(this).find('input').prop('checked', true).trigger('refresh');
		$('.nodeTree-margin').removeClass('checked-node');
		$(this).addClass('checked-node');
		var currentId = $('#currentNodeId').val();
		var url = '';
		if ($(this).parents('form').attr('id') == 'orgChart_move') {
			var selectedId = $(this).find('#CoreOrgChartTree_idOrg').val();
			url = HTTP_HOST + '?r=orgManagement/getNodeParents';
		}
		else if ($(this).parents('form').attr('id') == 'courseCategory_move') {
			var selectedId = $(this).find('#LearningCourseCategoryTree_idCategory').val();
			url = HTTP_HOST + '?r=courseCategoryManagement/getNodeParents';
		}
		$.get(url, {'selectedId' : selectedId, 'currentId' : currentId}, function (data) {
			$('.nodeParents').html(data.content);
		}, 'json');
		return false;

	});

	$('.currentNodes > ul > li, .ajaxLinkNode').live('mouseenter', function () {
		$(this).addClass('highlighted-node');
	});
	$('.currentNodes > ul > li, .ajaxLinkNode').live('mouseleave', function () {
		$(this).removeClass('highlighted-node');
	});

});
