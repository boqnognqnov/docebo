var AdvancedHttpsClass = null; 
/**
 * HTTPS Settings interface. Used in:
 * 	admin/Advanced Settings
 * 	multidomain/Settings
 * 
 * @param $
 */	
(function ( $ ) {

	var lastCustomCertOption = null;
	
	/**
	 * Constructor
	 */
	AdvancedHttpsClass = function (options) {
		if (typeof options === "undefined" ) {
			var options = {};
		}

		this.init(options);
	};


	/**
	 * Prototype
	 */
	AdvancedHttpsClass.prototype = {
		
		defaultOptions: {
		},
		
		options: {},
		
		/**
		 * Initialize 
		 */
		init: function(options) {
			
			var that = this;
			
			// Merge incoming options to default ones
			this.options = $.extend({}, this.defaultOptions, options);
			
			// Prepare the FORM, make it AJAX submitable
			var ajaxFormOptions = {
				success: this.onFormSubmitSuccess	
			};
			$('#settings-https-form').ajaxForm(ajaxFormOptions);

			// Add event listeners
			this.addListeners();
			
			// Update UI
			this.updateUi();
			
			
		},
		
		/**
		 * When AJAX form submition results with success
		 */
		onFormSubmitSuccess: function(data) {
			if (data.success) {
				
				$('.advanced-settings-https').replaceWith(data.html);
				
				// Bring the user back to the same "Custom certificate option"  
				if (lastCustomCertOption !== null) {
					$('input[name="CoreHttps[customCertificateOption]"][value="'+lastCustomCertOption+'"]').prop('checked', true).change();
					lastCustomCertOption = null;
				}
				$(document).controls();
			}
		},
		
		/**
		 * Add various event listeners.
		 * Note:  use  .off().on() mechanic!!!
		 */
		addListeners: function() {
			var that = this;
	
			/**
			 * When "Activate ON/OFF/etc. is changed
			 */
			$(document).on('change', 'input[name="CoreHttps[mode]"]', function(){
				that.updateUi();
			});
			
			/**
			 * When type of "custom certificate" optionis changed (uploading own certificate or requesting CSR)
			 */
			$(document).on('change', 'input[name="CoreHttps[customCertificateOption]"]', function(){
				that.updateUi();
			});
			
			
			/**
			 * On FORM submition: just stop it, but remember the current option (upload own or CSR generation)
			 */
			$(document).off('submit', '#settings-https-form')
				.on('submit', '#settings-https-form', function(e){
					
					// This is global to this class!
					lastCustomCertOption = $('input[name="CoreHttps[customCertificateOption]"]:checked').prop('value');
					
					e.preventDefault();
					e.stopPropagation();
					return false;
				});
			
			/**
			 * When we come back from "Confirm certificate deletion" we must reload the page
			 */
			$(document).off("dialog2.content-update", "#remove-certificate")
				.on("dialog2.content-update", "#remove-certificate", function (e) {
				if ($(this).find("a.auto-close.success").length > 0) {
					window.location.reload();
				}
			});

			
			/**
			 * When "remove this Key file" is clicked
			 */
			$(document).off('click', '.remove-key-file')
				.on('click', '.remove-key-file', function(e){
				
				// Show Prompt
				var dialogMessage = 
					'<div class="text-center">' + Yii.t('configuration', 'Are you sure you want to delete this Key file?') +    
					'<br><br> <span class="i-sprite is-solid-exclam red"></span> <strong>' + 
					Yii.t('configuration', 'Warning! This operation cannot be undone!') + '</strong>' + 
					'</div>';
				
				/**
				 * Show confirmation dialog to allow "key file deletion"
				 */
				bootbox.dialog(dialogMessage, 
						[
						 {
							 "label"	: Yii.t('standard', '_CONFIRM'),
							 "class"	: "btn-save",
							 "callback"	: function() { 
								var url = that.options.httpsControllerSettingsRoute + '&id=' + that.options.idHttps + '&action=removeKeyFile';
								$.getJSON(url, function(res){
									if (res.success) {
										that.options.keyFileAlreadySet = false;
										that.updateUi();
									} 
								});
							 }
						 }, 
						 {
							 "label"	: Yii.t('standard', '_CANCEL'),
							 "class"	: "btn-cancel"
						 }
						],
						{
							"classes" 	: "narrow fixed-dialog",
							"animate"	: false,
						});
				
			});
			
			
			/**
			 * When "Generate NEW CSR" is clicked, we must warn the user that current CSR related data will be lost
			 */
			$(document).off('click', 'input[name="generate_new_csr"]')
				.on('click', 'input[name="generate_new_csr"]', function(e){
				var me = this;
				e.preventDefault();
				e.stopPropagation();
				
				var dialogMessage = 
					'<div class="text-center">' + Yii.t('configuration', 'Are you sure you want to generate new CSR?') +    
					'<br><br> <span class="i-sprite is-solid-exclam red"></span> <strong>' + 
					Yii.t('configuration', 'Current CSR data will be lost and this operation cannot be undone!') + '</strong>' + 
					'</div>';

				/**
				 * Show confirmation dialog and upon YES/CONFIRM submit the form, otherwise, stop
				 */
				bootbox.dialog(dialogMessage, 
						[
						 {
							 "label"	: Yii.t('standard', '_CONFIRM'),
							 "class"	: "btn-save",
							 "callback"	: function() {
								 $(me).closest('form').append($('<input>').attr({
									    type: 'hidden',
									    name: 'generate_new_csr',
									    value: 'generate_new_csr'
									 })).submit();
							 }
						 }, 
						 {
							 "label"	: Yii.t('standard', '_CANCEL'),
							 "class"	: "btn-cancel"
						 }
						],
						{
							"classes" 	: "narrow fixed-dialog",
							"animate"	: false,
						});
				
				return false;
				
			});
			
			
			$(document).off('click', '#cancel-ssl-pending-installation')
				.on('click', '#cancel-ssl-pending-installation', function(){
					var url = that.options.httpsControllerSettingsRoute + '&id=' + that.options.idHttps + '&action=cancelPendingSslInstallation';
					$.getJSON(url, function(res){});
				});
			
			$(document).off('click', '#cancel-csrgen-pending-installation')
				.on('click', '#cancel-csrgen-pending-installation', function(){
					var url = that.options.httpsControllerSettingsRoute + '&id=' + that.options.idHttps + '&action=cancelPendingCsrGeneration';
					$.getJSON(url, function(res){});
				});
			
			
			
		},
		
		/**
		 * General method to update current UI based on available data and form values
		 */
		updateUi: function() {
			
			var currentMode 		= $('input[name="CoreHttps[mode]"]:checked').attr('value');
			var currentCertOwn 		= $('input[name="CoreHttps[customCertificateOption]"]:checked').attr('value');
			var mode 				= $('input[name="CoreHttps[mode]"]:checked').attr('value');
			
			// Custom domain Https settings?
			if (currentMode == 2) {
				$('.custom-certificate-data').show();
			}
			else {
				$('.custom-certificate-data').hide();
			}
			
			// Show/Hide CSR request form/ Keys Upload form depending on options
			if (currentCertOwn == 1) {
				$('.customer-has-ssl').show();
				$('.customer-needs-csr').hide();
			}
			else {
				$('.customer-has-ssl').hide();
				$('.customer-needs-csr').show();
			}
			
			// Hide "Save changes" button when CSR generation button is visible
			if ( (currentCertOwn == 2) && (mode==2) ) {
				$('input[name="save_https_settings_button"]').hide();
			}
			else {
				$('input[name="save_https_settings_button"]').show()
			}
			
			// Show or hide Key file Upload/Already set divs, depending on Key file status
			if (this.options.keyFileAlreadySet) {
				$('.keyfile-already-set').show();
				$('.keyfile-upload').hide();
			}
			else {
				$('.keyfile-already-set').hide();
				$('.keyfile-upload').show();
			}

			
		},
		
		
		/**
		 * Continuosly poll a controller/action to check SSL certificate installation status
		 */
		pollSslInstallStatus: function() {
			
			var that = this;
			
			var interval = 1000;
			var maxCounter = 102;

			
			var counter = 0;
			(function pollSslStatus(node, nodeSpan){
				counter++;
				var url = that.options.sslStatusCheckUrl;
				$.getJSON(url, function(res){
					if (res && res.success) {
						if (res.data.sslStatus && res.data.sslStatus == 'pending_installation') {
							if (counter <= maxCounter) {
								setTimeout(function() {pollSslStatus();},interval);
							}
							else {
								$('.ssl-pending-message').hide();
								showError('Timeout. Please reload the page!');
							}
						}
						else if (res.data.sslStatus && res.data.sslStatus == 'error') {
							$('.ssl-pending-message').hide();
							showError(res.data.sslStatusLastError);
						}
						else if (res.data.sslStatus && res.data.sslStatus == 'installed') {
							$('.ssl-pending-message').hide();
							$('.customer-has-ssl-successfully-uploaded').show();
						}
						else if (res.data.sslStatus && res.data.sslStatus == 'cancelled') {
							window.location.reload();
						}

						else {
							$('.ssl-pending-message').hide();
							showError('Unknown status detected ('+res.data.sslStatus+')');
						}
					}
					else {
						$('.ssl-pending-message').hide();
						showError('Bad AJAX response');
					}
					$(document).controls();
				});
				
				var showError = function(errorText){
					$('.ssl-pending-installation-error').show().text(errorText);
				};
				
			})();
		},
		
		
		
		/**
		 * Continuosly poll a controller/action to check CSR generation progress/status
		 */
		pollCsrGenStatus: function() {

			var that = this;
			
			var interval = 1000;
			var maxCounter = 102;

			
			var counter = 0;
			(function pollCsrStatus(node, nodeSpan){
				counter++;
				var url = that.options.csrStatusCheckUrl;
				$.getJSON(url, function(res){
					
					if (res && res.success) {
						
						if (res.data.csrStatus && res.data.csrStatus == 'pending_generation') {
							if (counter <= maxCounter) {
								setTimeout(function() {pollCsrStatus();},interval);
							}
							else {
								$('.csr-pending-message').hide();
								showError('Timeout. Please reload the page!');
							}
						}
						else if (res.data.csrStatus && res.data.csrStatus == 'error') {
							$('.csr-pending-message').hide();
							showError(res.data.sslStatusLastMessage);
						}
						else if (res.data.csrStatus && res.data.csrStatus == 'ready') {
							$('.csr-pending-message').hide();
							$('.customer-needs-csr-ready').show();
							that.options.keyFileAlreadySet = true;
							$('.keyfile-upload').hide();
							$('.keyfile-already-set').show();
							$('input[name="CoreHttps[keyFile]"]').val('');
							$('.key-file-name').text(res.data.keyFile);
						}
						else if (res.data.csrStatus && res.data.csrStatus == 'cancelled') {
							window.location.reload();
						}
						else {
							$('.csr-pending-message').hide();
							showError('Unknown status detected ('+res.data.csrStatus+')');
						}
					}
					else {
						$('.csr-pending-message').hide();
						showError('Bad AJAX response');
					}
					$(document).controls();
				});
				
				var showError = function(errorText){
					$('.csr-pending-generation-error').show().text(errorText);
				};
				
			})();
			
			
		}
		
		
		
	};
	
	
}( jQuery ));
