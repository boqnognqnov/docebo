/**
 * CListView JS copagnion...
 * The widget MUST always be surrounded by a form!!! 
 */
var ComboListViewClass = null; 
(function ($){
	var comboListViewsArray = [];
	
	/* Return the same array, duplicates removed */
	Array.prototype.getUnique = function(){
		   var u = {}, a = [];
		   for(var i = 0, l = this.length; i < l; ++i){
		      if(u.hasOwnProperty(this[i])) {
		         continue;
		      }
		      a.push(this[i]);
		      u[this[i]] = 1;
		   }
		   return a;
	}
	
	/* Constructor */
	ComboListViewClass = function (options) {
		if (typeof options === "undefined" ) {
			var options = {};
		}
		this.init(options);
	};
	
	/* Prototype */
	ComboListViewClass.prototype = {
		defaultOptions: {
			debug: false,
			listId: "combo-list"
		},
		options: {},
		
		/* Array of selected items */
		selectedItems: [],
		$container: false,
		
		/* Initialize combo hrid object */
		init: function(options) {
			var that = this;
			
			/* Merge incoming options to default ones */
			this.options = $.extend({}, this.defaultOptions, options);
			this.$container = $('#combo-list-view-container-' + this.options.listId);
			this.debug('Initialize Combolist');
			this.addListeners();
			return this;
		},
		
		/* Make our live easier and provide some debug, if enabled */
		debug: function(message) {
			if (this.options.debug === true) {
				Docebo.log('Combolist:', message);
			}
		},
		
		/* Set listeners */
		addListeners: function() {
			this.debug('Attach event listeners');
			var that = this;
			
			/* Autocomplete item selected */
			this.$container.find('#combo-list-search-input').on('autocompleteselect', $.proxy(this.onAutoCompleteSelect, this));
			/* $('#combo-list-search-input').on('autocompleteselect', $.proxy(this.onAutoCompleteSelect, this)); */
			
			/* Clear search */
			this.$container.find(".search-input-wrapper .clear-search").on('click', $.proxy(this.onClearSearchClick, this));
			
			/* Catch ENTER key in search field */
			this.$container.find('#combo-list-search-input').on('keydown', $.proxy(this.onSearchKeydown, this));
			
			/* "search" icon clicked */
			this.$container.find('.perform-search').on('click', $.proxy(this.onSearchButtonClick, this));
			
			/* Select/Deselect all items */
			this.$container.find('.combo-list-select-all').on('click', $.proxy(this.onSelectAll, this));
			this.$container.find('.combo-list-deselect-all').on('click', $.proxy(this.onDeSelectAll, this));
			this.$container.find('.combo-list-select-page-all').on('click', $.proxy(this.onSelectPageAll, this));
			this.$container.find('.combo-list-deselect-page-all').on('click', $.proxy(this.onDeSelectPageAll, this));
			
			/* When massive action is selected */
			this.$container.find('.combo-list-selection-massive-action').on('change', $.proxy(this.onSelectionMassiveAction, this));
			
			/* When checkbox is clicked and changed */
			this.$container.on('change', 'input[name^="combo-list-item-checkbox-"]', $.proxy(this.onSelectionChanged, this));			
		},
		
		/* Get the form surrounding the list. It MUST always be surrounded by a form!!! */
		form: function() {
			return $('#' + this.options.listId).closest('form');	
		},
		
		/* When Autcomplete item is selected */
		onAutoCompleteSelect: function(e, data) {
			var $target = $(e.currentTarget);
			$target.val(data.item.value);
			this.updateListView();
		},
		
		/* Update the list: call list's "update", feeding it with search form data (filter data) */
		updateListView: function() {
			$.fn.yiiListView.update(this.options.listId, {data: this.getFilterData()});
		},
		
		/* Serialize search form data and add more filtering attributes, if any */
		getFilterData: function() {
			return this.form().serialize();
		},
		
		/* "X" clicked in the search input, to clear the search text */
		onClearSearchClick: function(e) {
			this.form().find('#combo-list-search-input').val('');
			this.updateListView();
		},
		
		/* Catch keydown events in general text search field */
		onSearchKeydown: function(e) {
			var $target = $(e.currentTarget);
			var code = e.keyCode || e.which;
			if ((code == 13)) {
				e.preventDefault();
				this.updateListView();
				return false;
			}
			else {
				return true;
			}
		},
	
		/* When "search icon" in general text search field is clicked */
		onSearchButtonClick: function(e) {
			this.updateListView();
		},
	
		/* Select all requested */
		onSelectAll: function(e) {
			var that = this;
			var $target = $(e.currentTarget);
			this.debug('Select All requested');
			$('#'+that.options.listId).addClass('grid-view-loading'); // just re-use the one we use for girds
			var url = window.location.href;
			var data = $.deparam(this.getFilterData());
			data.comboListViewSelectAll = 1;
			$.post(url, data, function(data, textStatus, jqXHR){
				var response = $.parseJSON(data);
				that.addSelection(response);
				that.updatePageView();
				$('#'+that.options.listId).removeClass('grid-view-loading');

				//update the select all select page and other options after the grid refrsh
				if(typeof window.RewardsSet != "undefined") {
					RewardsSet.prototype.updateFilters();
				}
			});
		},
		
		/* De-Select all requested */
		onDeSelectAll: function(e) {
			var $target = $(e.currentTarget);
			this.debug('De-Select All requested');
			this.selectedItems = [];
			this.updatePageView();
		},
		
		/* Select page requested */
		onSelectPageAll: function(e) {
			var that = this;
			var $target = $(e.currentTarget);
			var selectionArray = [];
			$('#' + this.options.listId).find('.combo-list-item').each(function(i) {
				selectionArray.push($(this).data('key'));
			});
			this.addSelection(selectionArray);
			this.updatePageView();
		},
		
		/* De-Select page requested */
		onDeSelectPageAll: function(e) {
			var that = this;
			var $target = $(e.currentTarget);
			var selectionArray = [];
			$('#' + this.options.listId).find('.combo-list-item').each(function(i) {
				selectionArray.push($(this).data('key'));
			});
			this.substractSelection(selectionArray);
			this.updatePageView();
		},
		onSelectionMassiveAction: function(e) {
			var $target = $(e.currentTarget);
			var data = {
				action: $target.val(),
				selection: this.getSelection(),
				listId: this.options.listId
			};
		
			/* Trigger event to allow listeners to react */
			$(document).trigger('combolistview.massive-action-selected', data);
		},
	
		/* Called Upon checkbox selection change */
		onSelectionChanged: function(e) {
			var $target = $(e.currentTarget);
			var selectionArray = [];
		
			/* selectionArray.push(parseInt($target.val())); */
			selectionArray.push($target.val());
			if ($target.prop('checked') === true) {
				this.addSelection(selectionArray);
			}
			else {
				this.substractSelection(selectionArray);
			}
			this.updatePageView();
		},
	
		/* Add an array of items IDs to current selection */
		addSelection: function(selectionArray) {
			$.each(selectionArray, function(i,v) {
				selectionArray[i] = ''+v;
			});
			$.merge(this.selectedItems, selectionArray);
			this.selectedItems = this.selectedItems.getUnique();
		},
		substractSelection: function(selectionArray) {
			var that = this;
			$.each(selectionArray, function(i,v) {
				v = ''+v;
				index = $.inArray(v, that.selectedItems);
				if (index > -1) {
					that.selectedItems.splice(index, 1);
				}
			});
			this.selectedItems = this.selectedItems.getUnique();
		},
		
		/* Vizualize (apply) current global selection on current page */
		updateCurrentPageSelection: function() {
			var that = this;
			var listSelector = '#' + this.options.listId;
			$(listSelector).find('.combo-list-item').each(function(i) {
				var itemKey = ''+$(this).data('key');
				if ($.inArray(itemKey, that.selectedItems) > -1) {
					$(this).addClass('selected').find('input[type="checkbox"][name^="combo-list-item-checkbox"]').attr('checked', true).trigger('refresh');
				}
				else {
					$(this).removeClass('selected').find('input[type="checkbox"][name^="combo-list-item-checkbox"]').attr('checked', false).trigger('refresh');
				}
			});
		},
		
		/* Update visual counters for grids */
		updateSelectionCounter: function() {
			this.form().find('#combo-list-selected-count').text(this.selectedItems.length);
		},
		getSelection: function() {
			if (this.selectedItems)
				return this.selectedItems;
			else 
				return [];
		},
		setSelection: function(list) {
			this.selectedItems = list;
			this.updatePageView();
		},
		updatePageView: function() {
			this.updateCurrentPageSelection();
			this.updateSelectionCounter();
		},
	
		/* Return an instance of this */
		getInstance: function() {
			return this;
		}
	};
	
	/**
	 * A little jQuery plugin to "attach" combolist view JS to CListView
	 * This will allow having many combo list views in the same page.
	 */
	$.fn.comboListView = function(method) {
		
		/* Check for init call, where the argument is one and is an object (options) */
		if (typeof method === 'object' || !method) {
			if (!method) method = {};
			method.listId = $(this).attr('id');
			return comboListViewsArray[method.listId] = new ComboListViewClass(method);
		}
	
		/* Else, we are called for some method. Get the object instance first (get out if none found) */
		if (!comboListViewsArray[$(this).attr('id')]) return false;
		var cgv = comboListViewsArray[$(this).attr('id')];
		
		/* Call the method */
		if (cgv[method]) {
			return cgv[method].apply(cgv, Array.prototype.slice.call(arguments, 1));
		}
		else  {
			$.error('Method ' + method + ' does not exist on jQuery.comboListView');
			return false;
		}
	}
}( jQuery ));
