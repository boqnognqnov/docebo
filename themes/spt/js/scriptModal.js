$('document').ready(function() {

	$('.popup-handler').live('click', function() {
		showModal($(this));
		return false;
	});

	$('#popup .confirm-btn').live('click', function() {
		showModal($(this));
		return false;
	});

	$('#popup .close-btn').live('click', function() {
		$('#popup').modal('hide');
		return false;
	});

	$('#popup .redirect-btn').live('click', function() {
		showModal($(this));
		return false;
	});

});

function showModal(handler) {
	// create popup if doesn't exist
	if ($('#popup').length == 0) {
		var html = [
			'<div id="popup">',
			'<div class="modal-header">',
			'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
			'<h3></h3>',
			'</div>',
			'<div class="modal-body"></div>',
			'<div class="modal-footer"></div>',
			'</div>'
		].join('');

		$('body').append(html);
	}
	// set default classes to popup
	if (handler.hasClass('popup-handler')) {
		$('#popup').prop('class', 'modal hide fade');
	}

	if (handler.data('modal-title') !== undefined) {
		//$('#popup div.modal-header h3').html('<span></span>'+handler.data('modal-title'));
		$('#popup div.modal-header h3').html(handler.data('modal-title'));
	}
	// set additional classes to popup
	if (handler.data('modal-class') !== undefined) {
		$('#popup').addClass(handler.data('modal-class'));
	}

	if (handler.hasClass('popup-handler')) {
		if (handler.data('firstShow') === undefined) {
			handler.data('firstShow', 1);
		} else if (handler.data('firstShow') === 1) {
			handler.data('firstShow', 0);
		}

		$('#popup').data('firstShow', handler.data('firstShow'));
		$('#popup').data('afterClose', handler.data('after-close'));
		$('#popup').data('beforeSend', handler.data('before-send'));
		$('#popup').data('afterSend', handler.data('after-send'));
		$('#popup').data('afterShow', handler.data('after-show'));
	}

	$('#popup').data('handler', handler);
	$('#popup').modal('show');
}

$('#popup').live('show', function (e) {
	if ($(e.target).hasClass('modal')) {
		var handler = $(e.target).data('handler');
		var th = $(this);
		// default config
		var config = {
			dataType : 'json',
			type : 'POST',
			url : '',
			cache : false,
			data : {}
		};

		if (handler.hasClass('popup-handler')) {
			config.data.popupFirstShow = th.data('firstShow');
			//th.children('.modal-body').html('');
			th.children('.modal-body').html('<span class="loader"></span>');
			th.addClass('loading');
		}

		if (handler.length) {
			config.url = handler.prop('href');

			if(handler.data('datatype')){
				config.dataType = handler.data('datatype');
			}

			if (handler.closest('#popup').length) {
				if (handler.hasClass('confirm-btn')) {
					var form = $('#' + handler.data('submit-form'));
					config.type = 'POST';
					config.url = form.prop('action');
					th.children('.modal-body').append('<span class="loader"></span>');
					th.addClass('loading');
					if (form.data('grid-items') == true) {
						form.find('div.grid-view').each(function() {
							var selectedItems = $('#'+$(this).prop('id')+'-selected-items');
							if (selectedItems.length == 0) {
								selectedItems = $('#'+$(this).prop('id')+'-selected-items-list').clone();
								selectedItems.prop('name', $(this).prop('id')+'-selected-items')
									.prop('id', $(this).prop('id')+'-selected-items');
								form.append(selectedItems);
							} else {
								selectedItems.val($('#'+$(this).prop('id')+'-selected-items-list').val());
							}
						});
					}

					// add button value to form data
					if (handler.prop('name') !== '') {
						if (form.find('input[name=' + handler.prop('name') + ']').length == 0) {
							form.append('<input type="hidden" name="' + handler.prop('name') + '"/>');
						}
						form.find('input[name=' + handler.prop('name') + ']').val(handler.val());
					}

					// uses for getting content of tinymce to textarea
					form.find('textarea.tinymce').each(function() {
						$(this).html($(this).tinymce().getContent());
					});
					
					
					// If there is an orgchart Fancytree, generate form INPUT fields with selected nodes
					// Note the HARD CODED Fancytree ID ! Sorry! Just always use THIS one.
					// Also, names of the generated INPUT fields depends on how the Org Chart Fancy tree was loaded
					// Check both Widget AND Controller receiving data submission to use the same fields.
					var fancyTreeSelector = '#orgcharts-fancytree';
					if ($(fancyTreeSelector).length > 0) {
						try{
							$(fancyTreeSelector).fancytree("getTree").ext.docebo._generateFormElements(false);
						}catch(e){
							console.log('Can not initialize fancytree. Error follows below:'); // IE error logger
							console.log(e);
						}
					}
					

					// xxxxxxxxx
					
					config.data = form.serialize();
				} else if (handler.hasClass('redirect-btn')) {
					config.type = 'GET';
				}
			}
		}

		processCallback($(e.target).data('beforeSend'), config.data);

		$.ajax({
			dataType : config.dataType,
			type : config.type,
			data : config.data,
			url : config.url,
			cache : config.cache,
			success : function (data) {
				//th.attr("firstShow", false);
				if (this.dataType === "json") {
					if (data.html) {
						th.children(".modal-body").html(data.html);
					} else {
						$('#popup').modal('hide');
					}
				} else {
					th.children(".modal-body").html(data);
				}
				// move footer content
				var footer = th.find('div.modal-body div.modal-footer');
				if (footer.length > 0) {
					th.children('div.modal-footer').html(footer.html());
					footer.remove();
				} else {
					th.children('div.modal-footer').html('');
				}

				processCallback($(e.target).data('afterSend'), data);
				allModalAfterSend($(e.target));
				// call method "shown" if handler is placed in popup
				if (!handler.hasClass('popup-handler')) {
					th.trigger('shown');
				}
			},
			beforeSend : function () {
				//th.children(".modal-body").html("");
			},
			fail: function(jqXHR, status, errorThrown){
				console.log(jqXHR);
				console.log(status);
				console.log(errorThrown);
			}
		});
	}
});

$('#popup').live('shown', function(e) {
	if ($(e.target).hasClass('modal')) {
		// default actions
		allModalAfterShow($(e.target));
		// custom actions
		processCallback($(e.target).data('afterShow'));
	}
});

$('#popup').live('hidden', function(e) {
	processCallback($(e.target).data('afterClose'));
});

$(window).resize(function () {
	modalPosition();
}).trigger('resize');

// *-------------

function processCallback(callback, data) {
	modalPosition();
	if (typeof callback != 'undefined') {
		if (callback.match(/^function.*$/)) {
			callback = '(' + callback + '())';
		}
		return eval(callback);
	}
	return true;
}

// --- default callbacks

function allModalAfterSend($target) {

	// $target is jQuery object representing the modal
	// Apply styling to modal content only
	$target.find('input, select').styler();
	
	$('#userSlider input').styler({browseText: "UPLOAD YOUR THUMBNAIL"});
	$('#course_form input[type="file"]').styler({browseText: "BROWSE"});

	stopScroll();
	if ($('#popup .date').length) {
		$('#popup .date').bdatepicker();
	}

	replacePlaceholder();
	applyTypeahead($('.typeahead'));
}

function allModalAfterShow() {
	modalPosition();
}

// --- callbacks ---

function newUserAfterClose() {
	// USERMAN-v2
	if (typeof UserMan !== "undefined") {
		UserMan.updateUsersGrid();
	}
	$('#users-management-grid').yiiGridView('update');
}

function newUserAfterLoad() {
	$('#userForm-details, #userForm-orgchat').jScrollPane({
		autoReinitialise : true
	});
}

function disableModalSubmitButton(){
	var btn = $(".modal-footer .confirm-btn");
	btn.addClass('disabled');
	btn.prop('disabled', true);
}

function editUserAfterSubmit(data) {
	if (data.status == 'saved') {
		$('#popup').modal('hide');
		$('#users-management-grid').yiiGridView('update');
	
		// USERMAN-v2
		if (typeof UserMan !== "undefined") {
			UserMan.updateUsersGrid();
		}
		
	}
	$('#userForm-details, #userForm-orgchat').jScrollPane({
		autoReinitialise : true
	});
}

function editRecipientsAfterSubmit(data) {
	if (data.userCount !== undefined) {
		$('.user-count strong span').html(data.userCount);
	}
}

function massiveUserEditBeforeSend(data) {
	$.extend(data, $('#popup').data('handler').data('modal-send-data'));
}

function massiveUserEditAfterSend(data) {
	if ($('#popup').data('handler').hasClass('confirm-btn') && (data.status == 'saved')) {
		$('#popup').modal('hide');
		$('#users-management-grid-selected-items-list').val('');
		$('#users-management-grid').yiiGridView('update');

		// USERMAN-v2
		if (typeof UserMan !== "undefined") {
			UserMan.updateUsersGrid();
		}
		
		
	}
}

function powerUserAssignCourseAfterSubmit(data) {
	if (data.status == 'saved') {
		$('#popup').modal('hide');
		$('#poweruser-courses-management-list-selected-items-list').val('');
		$.fn.yiiListView.update('poweruser-courses-management-list');
	}
	return false;
}

function powerUserAssignLocationAfterSubmit(data) {
	if (data.status == 'saved') {
		$('#popup').modal('hide');
		$('#poweruser-locations-management-list-selected-items-list').val('');
		$.fn.yiiListView.update('poweruser-locations-management-list');
	}
	return false;
}

function courseEditEnrollmentAfterSubmit(data) {
	if (data.status == 'saved') {
		$('#popup').modal('hide');
		$('#course-enrollment-list-selected-items-list').val('');
		$.fn.yiiListView.update('course-enrollment-list');
	}
}

function newCurriculaAfterSubmit(data) {
	if (data.status == 'saved') {
		$('#popup').modal('hide');
		$('#curricula-management-grid').yiiGridView('update');
	}
}

function newCurriculaCourseAfterSubmit(data) {
	if (data.status == 'saved') {
		$.fn.yiiListView.update('curricula-course-list');
		$('#popup').modal('hide');
	}
}

function newCurriculaUsersAfterSubmit(data) {
	if (data.status == 'saved') {
		$.fn.yiiListView.update('curricula-user-list');
		$('#popup').modal('hide');
	}
}

function curriculaPrerequisitesAfterSubmit(data) {
	if (data.status == 'saved') {
		$.fn.yiiListView.update('curricula-course-list');
		$('#popup').modal('hide');
	}
}

function sessionEditEnrollmentAfterSubmit(data) {
	//refresh the listview in any case
	$.fn.yiiListView.update('session-enrollment-list');
	//check action status and message
	if (data.status == 'saved') {
		$('#popup').modal('hide');
		if (data.message)
			Docebo.Feedback.show('success', data.message);
		else
			Docebo.Feedback.hide();
	} else if (data.status == 'error') {
		// An error occurred
		if (data.message) {
			// An error message has been given -> show it
			Docebo.Feedback.show('error', data.message);
		}
	}
}