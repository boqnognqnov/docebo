/**
 * Docebo Bootcamp Menu plugin.
 * 
 * Requires:  docebo_bootcamp.css
 *  
	Example:
 
	<ul id="bootcamp-example" class="docebo-bootcamp">
		<li>
			<a href="#" data-helper-title="Lorem ipsum - title 1" data-helper-text="Lorem ipsum - text 1">
			Item 1
			</a>
		</li>
		<li>
			<a href="#" data-helper-title="Lorem ipsum - title 2" data-helper-text="Lorem ipsum - text 2">
			Item 1
			</a>
		</li>
		<li>
			<a href="#" data-helper-title="Lorem ipsum - title 4" data-helper-text="Lorem ipsum - text 3">
			Item 1
			</a>
		</li>

		<!-- Helper -->
		<li class="helper">
			<a href="#">
				<h4 class="helper-title"></h4>
				<p class="helper-text"></p>
			</a>
		</li>
		
   </ul>

	<script type="text/javascript">
		$(document).ready(function(){
			$("#bootcamp-example").doceboBootcamp();
		});
	</script>   
   
   
   
 *
 *  Options:
 *  	enableHelper:  true/false
 *  
 *  
 *  
 *  
 */
(function ( $ ) {
	$.fn.doceboBootcamp = function(options) {
		var settings = $.extend({
			
        }, options );

		var $anchors = $(this).find('li:not(.helper) > a');
		
		$anchors.hover(
			function() {
				var $this = $(this);
				if ($this.data('helper-title') && $this.data('helper-text')) {
					var liHelper = $this.parent().parent().find('li.helper');
					liHelper.find('.helper-title').html($this.data('helper-title'));
					liHelper.find('.helper-text').html($this.data('helper-text'));
					liHelper.show();
				}
			},
			function() {
				$(this).parent().parent().find('li.helper').hide();
			}
		);
	};
	
 
}( jQuery ));




