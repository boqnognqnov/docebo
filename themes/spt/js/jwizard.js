/**
 * 
 * @TODO:   
 * 			add callbacks so dialogs/form can do some manipulation or get info re: the wizard 
 * 
 *  Dialog2 based dialog FORM sequencer/wizard.
 *  Markup rules!
 *  
 *  Once again: This is STRICTLY Dialog2/ajaxForms based wizard!
 *  
 *  Separate dialogs are required to make little changes in order to become part of a wizard. 
 *  All the work is capsulted in this script. All what separate dialogs must do is to add
 *  a markup to indicate Step URLs, specify which fields are part of the wizard, final URL, etc (all through a markup, no JS!)
 *  
 *  Usage:
 *  	In the page going to launch a wizard, in JS section, put this:
 *  	$(function() {   // <--  if it does not work with document.ready, try without it!!
 *  		// Pass options to jWizard
 *			var options = {
 *				dialogId: "my-wizard-dialog-id" 	
 *			};
 *			var jwizard = new jwizardClass(options);
 *		});
 *  
 *  
 *  <<<<< unfinished documentation!! >>>>
 *  
 *  All navigations are done with INPUT type=submit BUTTONS put in div.form-actions !!!
 *
 *  OPTIONS
 *  -----------------------
 *  
 *  options.dialogId		!! Mandatory: Wizard must be passed the id of the dialog. 
 *  						String. This is the id of the TOP LEVEL Dialog2 modal-body, the one which will load all steps inside. 
 *
 *  options.dispatcherUrl	
 *  						String. URL of controller/action where all STEPS's form are submitted; AJAX called. 
 *  						The order of URL priority is:  1) [higher]  options.dispatcherUrl or 2) Button's data-jwizard-url or 3) FORM.action URL.
 *  						This can be used when you need to have a centralized STEPS's form submition endpoint (controller action) which 
 *  						decides what to do next, based on received information (the $_POST data), e.g. re-display the same STEP to show some errors,
 *  						skip some steps, preemtively close the wizard etc. 
 *  						This is the best way to use jWiazrd.
 *  						Default: ''
 *  
 *  options.successCallback 
 *  						String. A JS function to be called when FINISH Wizard button is clicked (confirm, success, but NOT cancel)
 *  						Default: none	
 *  
 *  options.alwaysPostGlobal 
 *  						Boolean. Ususally, the GLOBAL collected data of ALL steps (forms) is POSTed only on Confirm/Finish. Set this to true
 *  						if you want GLOBAL data to be POSTed on every move between steps.
 *  						Default: false
 *
 *  options.wizardPostName	
 *  						String. Name of the POST variable, containing Globally collected data from all wizard steps/dialogs forms
 *  						Default: Wizard (case sensitive)
 *  
 *  options.wizardFieldsSelector
 *  						String. A class added to form fields to mark them as "globaly saved" to wizard Global data array.
 *  						If set to FALSE, all fields are saved.
 *  						Default: false
 *  
 *  						NOTE: Special class "jwizard-no-save" can be added to prevent a form field to be saved.
 *
 *  options.debug
 *  						Boolean. Enable/Disable various console debug output
 *  				
 *  
 *
 *  EVENTS
 *  ----------------------
 *  
 *  
 *  jwizard.closed.success
 *  						When user clicks on ANY non-cancel, finishing button. Normally, this is CONFIRM button.
 *  						Use it to update main page.
 *  
 *  jwizard.form-saved			
 *  						When wizard leaves a dialog it saves all FORM data of that dialog, before moving to the next.
 *  						This event is fired just after all data are saved.
 *  						Listener ex: $(document).on('jwizard.form-saved', function(event, jwizard, form){});  
 *  
 *  jwizard.form-restored
 *  						The reverse of saved just happened (form data restored back from internal global data array)
 *  						Listener ex: $(document).on('jwizard.form-restored', function(event, jwizard, form){});
 *
 *  
 *    
 *  --------------------
 *  --------------------  
 *    
 *  More notes:  
 *    
 *  forms name/id			At least one (name or id) is mandatory and must be UNIQUE accross the wizard set!!!!
 *
 *	.jwizard (optional)		Form fields that "wants" to be saved Wizard-wide and sent as POST value(s)
 *							<input type='text' class='jwizard' value=''>
 *
 *  .jwizard-finish			SUBMIT! Button that requests a Wizard End and initiates Wizard-wide data submition
 *  						<input type='submit' class='jwizard-finish' value='FINISH'>
 *  
 *  .ajax.jwizard-form		Mandatory: Form participating in the Wizard
 *  						<form action="" class="ajax jwizard-form" method="POST">
 *  
 *	.jwizard-nav    		Special class added to navigational SUBMIT <input>s. They must be put in div.form-actions
 *							<input type="submit" class="jwizard-nav" data-jwizard-url="/url/to/action/for/another/dialog/step2" value="STEP 2">
 *
 *  data-jwizard-url		URL to load the next content or to send the final data; depends on button class:
 *   
 *  						jwizard-nav   : just load next dialog
 *  							<input type="submit" class="jwizard-nav" data-jwizard-url="/url/to/action/for/another/dialog/step2" value="STEP 2">
 *  
 *  						jwizard-nav.jwizard-finish	: finzlize the wizard
 *  							<input type="submit" class="jwizard-nav jwizard-finish" data-jwizard-url="/url/to/final/action" value="FINISH">
 *  
 *  .jwizard-no-save		Added as INPUT class will NOT save the field across wizard steps. Wizard will just doesn't about its existence
 *  
 *  .jwizard-no-restore		Added as INPUT class will NOT restore the field from global storage, even it is saved. 
 *  						This is to have some values that is changed in SOME step and stays unchanged until next change
 *  
 *   
 */



var jwizardClass = null;


(function($){

	jwizardClass = function(options) {
		
		// Default Wizard options
		this.defaultOptions = {
			
			// If wizard global data should be POSTed on every dialog submition
			alwaysPostGlobal: false,
			
			// Filter globaly saved form fields by this selector; Set to 'false' to save ALL
			wizardFieldsSelector: false,   // '.jwizard'
			
			// Wizard POST array name  (  Wizard[form-name][field-name] )
			wizardPostName: "Wizard",
			
			debug: false
			
		};
		
		
		
		
		if (typeof options === "undefined" ) {
			var options = {};
		}
		
		this.init(options);
		
	}
	
	
	jwizardClass.prototype = {
			
		// Global Wizard's data storage
		globalData: {},
			
		// A flag to indicate if FINISH button is clicked 	
		inFinal: false,	

		/**
		 * Initialize. Run at MAIN page load.
		 */
		init: function(options) {
			// Merge incoming options to default ones
			this.options = $.extend({}, this.defaultOptions, options);
			
			if (!this.options.dialogId) {
				Docebo.log('Warning: Dialog was not assigned an ID!');
			}
			
			// ADD LISTENERS
			this.addListeners();
			
			// Array of .modal classes, subject to removal upon dialog update
			this.classRegistryRemove = [];
			
			// Temporary holding list of .modal classes before & after dialog content is updated
			this.classesBefore = [];
			this.classesNow = [];
		},

		
		/**
		 * 
		 */
		addListeners: function() {
			var that = this;
			
			$(document).on('notify-jwizard-restore-data', 	$.proxy(this.onNotifyJwizardRestoreData, this));

			
			// LISTEN for Dialog2 "ajax-start" event; swap classes 
			$(document).on("dialog2.ajax-start", ".modal-body#" + that.options.dialogId, function () {
			});
			
			// LISTEN for Modal dialog Update
			$(document).on("dialog2.content-update", ".modal-body#" + that.options.dialogId, function () {
				
				var modalBody = this;
				var $modal = $(this).closest('.modal');

				
				// Resolve dialog class
				if (that.dialogClass) {
					$modal.addClass(that.dialogClass);
				}
				$.each(that.classRegistryRemove, function(index, aClass) {
					if (aClass != that.dialogClass) {
						$modal.removeClass(aClass);
					}
				});
				that.dialogClass = false;				
				
				
				// Watch for auto-close in the content of the dialog and close it
		        if ($(this).find("a.auto-close").length > 0) {
		            $(this).dialog2("close");
		        }
		        
		        // RESTORE data FROM global data into the loaded form inside the modal body
				that.restoreFormData(modalBody, {});

			});
			

			// Destroy saved data when dialog is closed (for any reason)
			$(document).on("dialog2.closed", ".modal-body#" + that.options.dialogId, function () {
				
				// Check for 'success' and call a success callback, if any
				if ($(this).find("a.auto-close.success").length > 0) {
					
					// Trigger CLOSE+SUCCESS event
					$(document).trigger('jwizard.closed.success', that);
					
					
					if (that.options.successCallback) {
						if (typeof window[that.options.successCallback] === 'function') {
							var data = {};
							window[that.options.successCallback](data);
						}
					}
					
				}

				that.globalData = {};
				
				return true;
			});

			
			
			// Catch wizard forms FORM PRE-serialize events
			$(document).on('form-pre-serialize', 'form.jwizard-form', function(event, $form, ajaxOptions, veto){
				// Depending on alwaysPostGlobal option, POST global data every time or ONLY when we are inFinal (when .jwizard-finish button is clicked)
				if ( (that.options.alwaysPostGlobal === true) || that.inFinal) {
					ajaxOptions.data = that.posterizeGlobalData();
					that.inFinal = false;
				}
			});

			
			// Catch wizard forms FORM VALIDATE events
			$(document).on('form-submit-validate', 'form.jwizard-form', function(event, formDataArray, $form, options, veto){
			});
			
			
			// Listen for Navigation clicks and change the parent's <form>'s action/url on the fly.
			$(document).on('click', 'form.jwizard-form input.jwizard-nav', function(){

				
				// Check if there is a Wizard Dispatcher URL set and use it (overrides FORM.action and buttons' navigational URLs)
				if (typeof that.options.dispatcherUrl == "string") {
					$(this).closest('form').attr('action', that.options.dispatcherUrl);
				} 
				else {
					// Change the form action URL to what is provided by the button data attribute, if any
					var url = $(this).data('jwizard-url');
					if (url) {
						$(this).closest('form').attr('action', url);
					}
				}
				// By now, submit URL is set, using either 1) Dispatcher URL  2) Button nav URL  3) FORM's action
				
				
				// Save form data to Wizard global storage
				$form = $(this).closest('form');
				that.saveFormData($form);

				// If the button is "finish", listen for the pre-serialize event of the ajaxForm and add saved global data for submition
				// This event will be raised just after click is catched by the Dialog2/ajaxForms handlers.
				if ($(this).hasClass('jwizard-finish')) {
					that.inFinal = true;
				}
				
			});

			// Form submition listener
			$(document).on('submit', 'form.jwizard-form', function(event) {
			});
			
			
			
		},

		
		
		/**
		 * Steps' dialogs can NOTIFY the wizard about different events....
		 * Listen for a call (well, triggered event)  from Step's dialog that it wants its data to be restored
		 */
		onNotifyJwizardRestoreData: function(e, extraData) {
			var modalBody = $('#'+this.options.dialogId);
			this.restoreFormData(modalBody[0], extraData);
		},
		
		
		
		/**
		 * Transform global saved data into a form suitable to be serialized into a POST data by ajaxForm
		 */
		posterizeGlobalData: function() {
			var that = this;
			var tmpData = [];
			
			$.each(that.globalData, function(formName, FormDataArray){
				$.each(FormDataArray, function(index, itemObject){
					
					if ( 
							((itemObject.type == "checkbox") && (itemObject.checked == "checked")) 	||  // checked Checkboxes
							((itemObject.type == 'radio')    && (itemObject.checked)) 				||  // checked Radios
							((itemObject.type != 'checkbox') && (itemObject.type != 'radio')) 			// Others	
						) { 
							var tmpItemObject = {};
							
							// No named objects are useless anyway, contnue with NEXT object (exit each function)
							if (itemObject.name === null || typeof itemObject.name == "undefined") {
								return;
							}
							
							// Translate array-like form field names 
							// ArrayFormFieldName[][][]... ==> [ArrayFormFieldName][][][]...  as being part of the "Wizard level" array
							// Get the base name, i.e. 'MyField' from  MyField[type]
							var key0 = itemObject.name.split(/\[(.+)/)[0];
								
							// Get the "key", i.e. get 'type' from 	MyField[type]
							//var re = /[^[\]]+(?=])/g;
							var re = /\[(.+)\]/g;
							var matches = itemObject.name.match(re);
							if (matches !== null) {
								if (typeof matches[0] !== "undefined")
									var key1 = matches[0];
							}

							// Build the field name:  e.g. Wizard[<form-name>][MyField][type]
							if (typeof key1 !== "undefined" ) {
								tmpItemObject.name  = that.options.wizardPostName + '[' + formName + '][' + key0 + ']' + key1;  
							}
							else {
								tmpItemObject.name  = that.options.wizardPostName + '[' + formName + '][' + key0 + ']';
							}

							// If the Value of the field is an array, we must enumerate values and add all elements as separate objects
							// Example: multiselect SELECTs
							if ($.isArray(itemObject.value)) {
								$.each(itemObject.value, function(index, value){
									var required = itemObject.required;
									var tmpObj;
									var last2 = tmpItemObject.name.slice(-2); // get last 2 characters

									// Create NEW object for every element that is gonna be pushed
									// !! Because Array holds reference to objects, not copies !!
									tmpObj = jQuery.extend({}, tmpItemObject);
									
									// Lets check if last 2 characters are []. If not, we must add them
									if (last2 != '[]') {
										tmpObj.name = tmpObj.name + '[]'; 
									}
									tmpObj.value = value;
									tmpObj.required = required;
									tmpData.push(tmpObj);
								});
							}
							else {
								tmpItemObject.value = itemObject.value;
								tmpItemObject.required = itemObject.required;
								tmpData.push(tmpItemObject);
							}

					}
					
				});
			});
			
			return tmpData;
			
		},
		
		
		/**
		 * Save FORM data into global wizard variable.
		 * 
		 * INPUT fields having 'jwizard-no-save' will NOT be saved accross Wizard Steps, they are like "local" to every step
		 * 
		 */
		saveFormData: function($form) {
			var that = this;
			var fieldNamesToSave = [];
			var filterByFieldName = false;
			var formName = $form.attr('name');
			var formId = $form.attr('id');
			
			// The option "options.wizardFieldsSelector" can be used to select fields to be SAVED as Wizrad global data
			// If it is false or null or not existent: we save ALL form fields (incl. buttons! just everything)
			var selector = (typeof that.options.wizardFieldsSelector === "string") ?
					":input." + that.options.wizardFieldsSelector
					: 
					":input";
			
			var tmpFormDataArray = [];
			
			$form.find(selector).each(function(){
				var elem = {
					name: $(this).attr("name"),
					required: false,
					type: $(this).attr("type"),
					value: $(this).val(),
					checked: $(this).attr("checked")
				};
				tmpFormDataArray.push(elem);
			});

			if ( (typeof formName === "undefined") && (typeof formId === "undefined") ) {
				Docebo.log('Warning: jWizard requires form to have either "name" or "id" defined! Neither have been found: form data are not saved!');
				return;
			}
			
			if (formName) {
				that.globalData[formName] = tmpFormDataArray;
			}
			else if (formId) {
				that.globalData[formId] = tmpFormDataArray;
			}
			
			// Inform the world we've just saved data of a form
			$(document).trigger('jwizard.form-saved', [that, $form]);

			
		},
		
		
		/**
		 * Restore data from global data container; update UI
		 * 
		 * INPUT fields having 'jwizard-no-restore' will NOT be restored, even they are saved in jWizard global data storage. 
		 * 
		 */
		restoreFormData: function(modalBody, data) {
			var that = this;

			$.each(this.globalData, function(formName, formDataArray){
				
				$form = $(modalBody).find('form.jwizard-form[name="' + formName + '"]');
				if ($form.length <= 0) {
					$form = $(modalBody).find('form.jwizard-form[id="' + formName + '"]');
				}
				
				if ($form.length > 0) {
					that.debug('Restore FORM: ' + formName);
					that.debug('Form data: ', formDataArray);
					$.each(formDataArray, function(index, fieldObject) {
						
						// Check if we have to restore data to a selected fields only (data.fieldNames)
						// READ carefully!!!!
						// Logic is: 
						//   IF we have some data.fieldNames defined
						//   AND IF it is an Object (array) having length > 0
						//   AND IF the current field name is NOT listed in that array
						//       ===> SKIP this field
						//
						// BUT: if data.fieldNames is not available OR its length is 0, then just go and update... no skip
						if ( ! ((typeof data.fieldNames == "object") && (data.fieldNames.length > 0) && ($.inArray(fieldObject.name, data.fieldNames) <= -1)) ) {
							$form.find(':input[name="' + fieldObject.name + '"]:not(.jwizard-no-restore)').each(function() {
								if (fieldObject.type == 'checkbox') {
									if (fieldObject.checked == 'checked') {
										$(this).prop('checked', true);
									}
									else {
										$(this).prop('checked', false);
									}
									$(this).val(fieldObject.value);
								}
								else if (fieldObject.type == 'radio') {
									if (fieldObject.value == $(this).val()) {
										$(this).attr('checked', fieldObject.checked == 'checked');
									}
								}
								else if (fieldObject.type == 'password') {
								}
								else {
									$(this).val(fieldObject.value);
								}
								$(this).trigger('change');

							});
						};
					});
					
					// Inform the world we've just restored data of a form
					$(document).trigger('jwizard.form-restored', [that, $form]);
					
					// Very often, we have Styled radio buttons. We must trigger "refresh" on them to update visual status of the styler, if any!
					$('input[type="radio"]').trigger("refresh");
					
				}
			});
			
		},
		
		/**
		 * Register a class to be removed from .modal AFTER dialog content is updated
		 */
		registerClassToRemove: function(aClass) {
			this.classRegistryRemove.push(aClass);
		},
		
		/**
		 * 
		 */
		setDialogClass: function(aClass) {
			this.dialogClass = aClass;
			this.registerClassToRemove(aClass);
		},
		
		
		debug: function() {
			if (this.options.debug && window.console && window.console.log) {
				var args = Array.prototype.slice.call(arguments);
				if(args){
					$.each(args, function(index, ev){
						window.console.log(ev);
					});
				}
		    }
		},

		resetFormData: function(formName){
			this.globalData = {};
		},
		
		
		// keep this here to prevent putting COMMA after last property by accident, which kills IE :-)
		dummyCommaKiller: null
	}

	
})(jQuery);
