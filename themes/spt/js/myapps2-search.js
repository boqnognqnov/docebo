var SearchEngine = SearchEngine || false;

(function($){

	if (SearchEngine !== false) {
		return;
	}

    /*
     * Constructor
     */
    var SearchEngineClass = function(options){
        this.mainContainer = false;
        this.initialUrl = false;
        this.initialDatUrl = false;
        this.buttonListener = false;
        this.lang = false;
        this.template = false;
        this.filterButtons = false;
        this.clearTextIconClass = false;
        this.backButtonText = '';
        this.resutsFoundText = '';
        this.isInTrialPeriod = false;

        this.data = false;
        this.dataLength = 0;
        this.filter = false;
        this.inFilterMode = false;
        this.filterText = "";

        this.defaultOptions = {
            debug: true
        };

        if(typeof options === "undefined"){
            var options = {};
        } else{
            this.mainContainer = options.mainContainer;
            this.initialUrl = options.url;
            this.initialDatUrl = options.dataUrl;
            this.buttonListener = options.buttonListener;
            this.filterButtons = options.filterButtons || false;
            this.clearTextIconClass = options.clearTextIconClass || false;
            this.backButtonText = options.backButtonText || '';
            this.resutsFoundText = options.resultsFoundText || '';
            this.isInTrialPeriod = options.isInTrialPeriod || false;
        }


        if(!this.mainContainer || !this.initialUrl || !this.buttonListener){
            return false;
        }

        this.init(options);
    };

    /**
     * Prototype
     */
    SearchEngineClass.prototype = {
    		
    	/**
    	 * 
    	 */
        init: function(options){

            var that = this;

            $.ajax({
                method: 'POST',
                url: this.initialUrl,
                data: this.initialDatUrl,
                dataType: 'json',
            })
            	.done(function(res){
                	if (res.success) {
                		that.data = res.data;
                		that.dataLength = Object.keys(that.data).length;
                	}
                });

            this.options = $.extend({}, this.defaultOptions, options);
            this.debug("Init Apps Search");
            this.addListeners(that);
            
        },

        debug: function(message){
            if(this.options.debug === true){
                console.log('Apps search: ', message);
            }
        },

        addListeners: function(that){
            $('input[name=' + this.buttonListener + ']').on('keyup', function(){
                that.filterText = $(this).val();
                that.searchForApp();
            });

            $('input[name=' + this.filterButtons + ']').on('change', function(){
                that.filter = $(this).val();
                if(that.inFilterMode !== false){
                    that.searchForApp();
                } else{
                    if(that.filter.length > 0){
                        that.searchForApp();
                    }
                }
            });

            $('.' + this.clearTextIconClass).on('click', function(){
                if(that.filterText.length > 0){
                    that.resetSearchField();
                    that.searchForApp();
                    that.resetFilterButtons();
                }
            });
        },

        resetSearchField: function(){
            $('input[name=' + this.buttonListener + ']').val('');
            this.filterText = '';
        },

        searchForApp: function(){
            var searchTerm = this.filterText.toLowerCase();
            var arrayOfFound = [];
            var name = "";
            var description = "";
            var availInPlan = "";
            var filter = this.filter;
            for(var elem in this.data){
                //I don't know why!!!!!!!
                if(elem == 'last') continue;

                name 		= String(this.data[elem]['name']).toLowerCase();
                description = String(this.data[elem]['description']).toLowerCase();
                availInPlan = String(this.data[elem].app.available_in_plan).toLowerCase();

                if(name.match(searchTerm) || description.match(searchTerm)){
                	if (!filter) {
                		arrayOfFound.push(elem);
                	}
                	else {
                		if (availInPlan == String(filter).toLowerCase()) {
                			arrayOfFound.push(elem);
                		}
                	}
                }
            }

            this.render(arrayOfFound);
        },

        render: function(indexes){
            var buildHtml = [];

            for(var elem in this.data){
                if(jQuery.inArray(elem + '', indexes) !== -1){
                    buildHtml.push(this.data[elem]['body']);
                }
            }

            this.renderData(buildHtml);
        },

        renderData: function(dataArray){
            var dataLen = dataArray.length;

            this.inFilterMode = true;
            
            $('.' + this.mainContainer + ' .un-filtered-apps').hide();
            
            $('.' + this.mainContainer + ' .filtered-apps').html('');

            var html = '<h3 class="title-bold back-button"><a id="back-button" href="#" >' + this.backButtonText +  '</a><span>' + dataLen + '&nbsp' + this.resutsFoundText + '</span></h3>';
            $('.' + this.mainContainer + ' .filtered-apps').append(html);
            for(var i = 0; i < dataLen; i++){
            	$('.' + this.mainContainer + ' .filtered-apps').append(dataArray[i]);
            }

            $('.' + this.mainContainer + ' .filtered-apps').show();
            
            var that = this;

            $(document).on("click", ".myapps-container a#back-button", function(){
                if(that.inFilterMode !== false){
                	
                	$('.' + that.mainContainer + ' .filtered-apps').hide();
                	$('.' + that.mainContainer + ' .un-filtered-apps').show();
                    that.inFilterMode = false;
                    that.resetFilterButtons(true);
                    that.resetSearchField();
                }
            });
            
            $(document).controls();
        },

        getFilterButtonIndex: function(reset){

            if(reset !== "undefined" && reset === true) return 0;

            switch (this.filter){
                case "":
                    return 0;
                case "starter":
                    return 1;
                case "advanced":
                    return 2;
                case "enterprise":
                    return 3;
                default:
                    return 0;
            }
        },

        resetFilterButtons: function(resetAll){

            $('span[id^=' + this.filterButtons +']').each(function(){
                $(this).removeClass('checked');
            });
            $('span#' + this.filterButtons + '_' + this.getFilterButtonIndex(resetAll) + '-styler').addClass('checked');

            if(resetAll !== "undefined" && resetAll === true){
                this.filter = "";
            }
        }
    };
    
    SearchEngine = SearchEngineClass;
    
}) (jQuery);