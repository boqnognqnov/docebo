// Links Select/Deselect all for GridView
$('div.grid-selections a.select-all').live('click', function() {
    var gridId = $(this).closest('div.grid-selections').data('grid');
    if ($('.modal.in').length > 0) {
        var gridView = $('.modal.in').find('#' + gridId);
    } else {
        var gridView = $('#' + gridId);
    }

    if (!gridView.find('thead .checkbox-column input[type=checkbox]').get(0).checked) {
        gridView.find('thead .checkbox-column > span').click();
    }
    if ($('.modal.in').length > 0) {
        $('.modal.in').find('#'+gridView.attr('id')+'-selected-items-list').val($('.modal.in').find('#'+gridView.attr('id')+'-all-items-list').val());
    } else {
        $('#'+gridView.attr('id')+'-selected-items-list').val($('#'+gridView.attr('id')+'-all-items-list').val());
    }
    updateSelectedCheckboxesCounter(gridView.attr('id'));
    return false;
});

$('div.grid-selections a.deselect-all').live('click', function() {
    var gridId = $(this).closest('div.grid-selections').data('grid');
    if ($('.modal.in').length > 0) {
        var gridView = $('.modal.in').find('#' + gridId);
    } else {
        var gridView = $('#' + gridId);
    }

    if (gridView.find('thead .checkbox-column input[type=checkbox]').get(0).checked) {
        gridView.find('thead .checkbox-column > span').click();
    } else {
        gridView.find('tr td input:checked').attr('checked', false).trigger('refresh');
    }

    if ($('.modal.in').length > 0) {
        $('.modal.in').find('#'+gridView.attr('id')+'-selected-items-list').val('');
    } else {
        $('#'+gridView.attr('id')+'-selected-items-list').val('');
    }
    updateSelectedCheckboxesCounter(gridView.attr('id'));
    return false;
});

function updateSelectedCheckboxesCounter(gridId) {
    var columnId = gridId + '-checkboxes';
    if ($('.modal.in').length > 0) {
        $('.modal.in').find('#'+gridId+'-selected-items-count').html(getSelectedCheckboxesCount(gridId, columnId));
    } else {
        $('#'+gridId+'-selected-items-count').html(getSelectedCheckboxesCount(gridId, columnId));
    }
}

function getSelectedCheckboxesCount(gridId, columnId) {
    if ($('.modal.in').length > 0) {
        var selectedItems = $('.modal.in').find('#'+gridId+'-selected-items-list').val();
    } else {
        var selectedItems = $('#'+gridId+'-selected-items-list').val();
    }

    var count = 0;
    if (selectedItems != '') {
        try
        {
            count = selectedItems.split(',').length;
        }
        catch(e)
        {
        }
    }
    return count;
}


//GRID VIEW SELECT

// click on checkbox for selecting whole page
$('.grid-view thead .checkbox-column input[type=checkbox]').live('change', function() {
    var checkboxes = $(this).parents('table').find('tbody span.jq-checkbox');

    if (this.checked) {
        checkboxes = checkboxes.not('.checked');
    } else {
        checkboxes = checkboxes.filter('.checked');
    }

    checkboxes.each(function() {
        $(this).click();
    });
});

// Update checkbox counter for GridView
$('div.grid-view td input[type=checkbox]').live('change', function() {
    var gridView = $(this).closest('.grid-view');

    saveCurrentCheckboxState(gridView.prop('id'), $(this));

    if ($(this).is(":checked")) {
        $(this).closest('tr').addClass('selected');
    } else {
        $(this).closest('tr').removeClass('selected');
    }

    if (gridView.find('td input').not(':checked').length == 0) {
        gridView.find('th input').attr('checked', true);
        gridView.find('th input').trigger('refresh');
    } else if (gridView.find('th input').is(':checked'))  {
        gridView.find('th input').attr('checked', false);
        gridView.find('th input').trigger('refresh');
    }
    updateSelectedCheckboxesCounter(gridView.attr('id'));
});

function saveCurrentCheckboxState(containerId, checkboxObj) {

    if ($('.modal.in').length > 0) {
        var selectedItemsObj = $('.modal.in').find('#' + containerId + '-selected-items-list');
    } else {
        var selectedItemsObj = $('#' + containerId + '-selected-items-list');
    }

    var selectedItems = selectedItemsObj.val();
    if (selectedItems != '') {
        selectedItems += ',';
    }
    if (checkboxObj.is(':checked')) {
        selectedItems += checkboxObj.val() + ',';
    } else {
        selectedItems = selectedItems.replace(checkboxObj.val() + ',', '');
    }
    selectedItemsObj.val(selectedItems.slice(0, -1));
    return true;
}