/**
 * Javascript Compagnion for Admin->Users Management UI
 *
 *  Require: jquery.cookie.js
 *
 *
 */

var AdminUserManagement = false;
var AdminUserManagementImportCsv = false;

(function ($) {

	/**
	 * Return the same array, duplicates removed
	 */
	Array.prototype.getUnique = function () {
		var u = {}, a = [];
		for (var i = 0, l = this.length; i < l; ++i) {
			if (u.hasOwnProperty(this[i])) {
				continue;
			}
			a.push(this[i]);
			u[this[i]] = 1;
		}
		return a;
	}



	/**
	 * Class constructor
	 *
	 * Used in main User Management process (actionIndex)
	 *
	 */
	AdminUserManagement = function (options) {
		this.defaultOptions = {
			debug: true,
			userManActionUrl: '', // MUST BE SET upon object initialization
			usersGridId: "users-management-grid",
			containerId: "userman-container",
			searchFormId: "userman-search-form", // Id of the search/filter form
			hiddenGridColumns: [], // List of columns to hide in the users grid
			orgchartFancyTreeId: false, // FancyTree ID
			selectedUsersFieldName: 'userman_selected_users', // The input name where selected users are also saved
			branchActionsTriggerClass: "userman-branch-actions-menu-trigger", // Per-branch actions trigger element class
			branchDragHandlerClass: "userman-drag-handler", // Per-branch actions trigger element class
			branchActionsContainerIdPrefix: "userman-branch-actions-po-container-", // ID prefix for Container for per-branch actions popover
			selectedColumnsCookieName: "userman_selected_grid_columns", // Cookie to save current columns selection to show in users grid
			fancytreeOrgchartWrapperId: "fancytree-orgchart-wrapper"

		};
		if (typeof options === "undefined") {
			var options = {};
		}
		this.init(options);
	};


	/**
	 * Prototype: AdminUserManagement
	 */
	AdminUserManagement.prototype = {
		/**
		 * Array of selected users
		 */
		selectedUsers: [],
		/**
		 * We need this semaphor to prevent attempting to update users grid upon fancy load,
		 * because at that point in time grid is NOT yet loaded.
		 */
		inFirstRun: true,
		/**
		 * An array, holding users grid columns to show
		 */
		hiddenGridColumns: [],
		/**
		 * If this is true, than we have detected a tree update and we need to reload the entire tree structure.
		 * A dialog with tree update message will be displayed while this is set to true.
		 */
		isRequestingUpdate: false,
		/**
		 * Dynamically resize the block that provides contextual information
		 * when one of the big/top action buttons are hovered (so it doesn't break line)
		 */
		fixContextInfoBlockWidth: function () {
			var container = $('.main-actions', '#' + this.options.containerId);
			var remainingWidth = container.innerWidth() - container.find('> ul').width() - 2;
			if ($('.info', container).width() > remainingWidth) {
				console.warn('Shrinking context info block for main action buttons. Try to reduce the number of buttons displayed at the top of the page');
				$('.info', container).css('max-width', remainingWidth);
			}
		},
		/**
		 * Initialize class object
		 */
		init: function (options) {
			this.options = $.extend({}, this.defaultOptions, options);

			this.debug('Initialize UserMan');
			this.addListeners();

			// Will hold current Advanced search filter data
			this.advancedSearchFilters = {};

			this.initGridColumnsSelectorPopover();

			this.hiddenGridColumns = this.options.hiddenGridColumns;

			this.setGridColumnsVisibility();

			if ($.cookie('userman-tree-collapse') > 0) {
				this.collapseTreeWrapper($("#" + this.options.containerId + " .userman-tree-show-hide"));
			} else {
				this.expandTreeWrapper($("#" + this.options.containerId + " .userman-tree-show-hide"));
			}

			this.fixContextInfoBlockWidth();

		},
		/**
		 * Make our live easier and provide some debug, if enabled
		 */
		debug: function (message) {
			if (this.options.debug === true) {
				console.log('UserMan:', message);
			}
		},
		/**
		 * Add Event listeners
		 */
		addListeners: function () {
			this.debug('Attach event listeners');
			var that = this;

			// NON-LIVE Events

			// CLICK: Collapse/Expand click
			$("#" + this.options.containerId + " .userman-tree-show-hide").on('click', $.proxy(this.onCollapseClick, this));

			// CLICK: Advanced search
			$("#" + this.options.containerId + " .userman-advanced-search-link").on('click', $.proxy(this.onAdvancedSearchClick, this));

			// When new filter selection is made (to be added)
			$("#" + this.options.containerId + ' #add-filter-select').on('change', $.proxy(this.onAddFilterChange, this));

			// Users type change (All, Active, Suspended,...)
			$("#" + this.options.containerId + ' .userman-select-users-type input[type="radio"]').on('change', $.proxy(this.onUsersTypeChange, this));

			// Include/Do Not include descendants handle clicked
			$("#" + this.options.containerId + ' .userman-include-descendants input[type="checkbox"]').on('change', $.proxy(this.onIncludeDesc, this));

			// Autocomplete item selected
			$("#" + this.options.containerId + ' #userman-search-input').on('autocompleteselect', $.proxy(this.onAutoCompleteSelect, this));

			// Clear search
			$("#" + this.options.containerId + ' .search-input-wrapper .clear-search').on('click', $.proxy(this.onClearSearchClick, this));

			// Catch ENTER key in search field
			$("#" + this.options.containerId + ' #userman-search-input').on('keydown', $.proxy(this.onSearchKeydown, this));


			// Select/Deselect all users
			$("#" + this.options.containerId + ' .userman-select-all').on('click', $.proxy(this.onGridSelectAll, this));
			$("#" + this.options.containerId + ' .userman-deselect-all').on('click', $.proxy(this.onGridDeSelectAll, this));


			// LIVE/DELEGATED Events, using decent jQuery format!
			// On breadcrumbs item click
			$("#" + this.options.containerId).on('click', 'a.userman-breadcrumbs-item', $.proxy(this.onBreadcrumbsItemClick, this));

			// Show/Hide columns/fields popover clicks
			$("#" + this.options.containerId).on('click', '.userman-select-columns li', $.proxy(this.onSelectGridColumnsClick, this));

			// Massive action selection
			$("#" + this.options.containerId).on('change', '#userman-users-massive-action', $.proxy(this.onMassiveActionChange, this));

			// Node actions popovers: on showN! event, run Dialog2 controls over their content to activate "open-dialog" links
			$("#" + this.options.containerId).on('shown.bs.popover', '#' + this.options.fancytreeOrgchartWrapperId, function (e) {
				that.debug('Node action popover shown');
				$(this).find('.popover.in .popover-content').controls();
			});

			// Handle the event triggered by Assign Users usersselector. Update users grid.
			// We can listen for .modal only, but lets be more specific: use the class
			$(document).on('userselector.closed.success', '.modal.node-assign-users-selector', function (e, data) {
				that.updateUsersGrid();
			});


		},
		/**
		 * ON Collapse/Expand handle click
		 */
		onCollapseClick: function (e) {
			var $target = $(e.currentTarget);
			if ($target.hasClass('hide-tree')) {
				this.collapseTreeWrapper($target);
			} else {
				this.expandTreeWrapper($target);
			}
		},
		collapseTreeWrapper: function ($target) {
			$("#" + this.options.containerId + ' #' + this.options.fancytreeOrgchartWrapperId).hide();
			$target.text(Yii.t('standard', '_EXPAND')).prepend('<span></span>').removeClass('hide-tree');
			$.cookie('userman-tree-collapse', 1, {path: '/'});
		},
		expandTreeWrapper: function ($target) {
			$("#" + this.options.containerId + ' #' + this.options.fancytreeOrgchartWrapperId).show();
			$target.text(Yii.t('standard', '_COLLAPSE')).prepend('<span></span>').addClass('hide-tree');
			$.cookie('userman-tree-collapse', 0, {path: '/'});
		},
		/**
		 * When "Advanced search" link is clicked in the search form
		 */
		onAdvancedSearchClick: function (e) {
			var $element = $('#userman-advanced-search');
			$element.toggle();
			$('#advancedSearchVisibility').val($element.css('display') == 'none' ? 0 : 1);
		},
		/**
		 * When new 'filter' is selectoed from dropdown list
		 */
		onAddFilterChange: function (e) {

			var $target = $(e.currentTarget);
			var $selected = $target.find('option:selected');

			if ($selected.val() === "")
				return true;

			// Add an advanced filter element in the list of filters
			this.addAdvancedFilter($selected);

		},
		/**
		 * Change All/Active/Suspended selector
		 */
		onUsersTypeChange: function (e) {
			var $target = $(e.currentTarget);
			this.debug('Change users type to : ' + $target.val());
			this.updateUsersGrid();
		},
		/**
		 * Change Show/Hide users from branch descendants as well
		 */
		onIncludeDesc: function (e) {
			this.updateUsersGrid();
		},
		/**
		 * When Autcomplete item is selected
		 */
		onAutoCompleteSelect: function (e, data) {
			var $target = $(e.currentTarget);
			$target.val(data.item.value);
			this.updateUsersGrid();
		},
		/**
		 * "X" clicked in the search input, to clear the search text
		 */
		onClearSearchClick: function (e) {
			$('#userman-search-input').val('');
			this.updateUsersGrid();
		},
		/**
		 * Catch keydown events in general text search field (NOT advanced filters!)
		 */
		onSearchKeydown: function (e) {
			var $target = $(e.currentTarget);
			var code = e.keyCode || e.which;
			if ((code == 13)) {
				e.preventDefault();
				this.updateUsersGrid();
				return false;
			} else {
				return true;
			}
		},
		/**
		 * When "search icon" in general text search field is clicked (NOT advanced search!)
		 */
		onSearchButtonClick: function (e) {
			this.updateUsersGrid();
		},
		/**
		 * Clicked a column/field in "grid columns show/hide" popover (the button next to "Advanced search"
		 */
		onSelectGridColumnsClick: function (e) {
			var $target = $(e.currentTarget);
			if ($target.prop('tagName') == 'span') {
				$target = $target.parent(); //$target should always refer to the <li> element of the menu
			}
			if ($target.hasClass('locked')) {
				return true;
			}

			var column = $target.data('column');
			var columnKey = $target.data('column-key');
			var index;
			if ($target.hasClass('selected')) {
				$target.removeClass('selected');
				$("#" + this.options.usersGridId).find('.' + column).hide();
				this.hiddenGridColumns.push($target.data('column-key'));
			} else {
				if ($target.closest('.userman-select-columns').find('li.selected').length < 5) {
					$target.addClass('selected');
					$("#" + this.options.usersGridId).find('.' + column).show();
					index = $.inArray(columnKey, this.hiddenGridColumns);
					if (index > -1) {
						this.hiddenGridColumns.splice(index, 1);
					}
				}
			}

		},
		/**
		 * Select all requested
		 */
		onGridSelectAll: function (e) {
			var that = this;
			var $target = $(e.currentTarget);
			this.debug('Select All requested');

			var data = '';
			data = this.getFilterData();
			data += '&selectAll=1';

			$('#' + that.options.usersGridId).addClass('grid-view-loading');
			$.post(that.options.userManActionUrl, data, function (data, textStatus, jqXHR) {
				response = $.parseJSON(data);
				that.addUsersSelection(response);
				that.updateCurrentPageSelection();
				that.updateSelectionCounter();
				$('#' + that.options.usersGridId).removeClass('grid-view-loading');
			});


		},
		/**
		 * De-Select all requested
		 */
		onGridDeSelectAll: function (e) {
			var $target = $(e.currentTarget);
			this.debug('De-Select All requested');
			this.selectedUsers = [];
			this.updateCurrentPageSelection();
			this.updateSelectionCounter();
		},
		/**
		 * Add an array of users IDs to current selection, saved in UserMan object attribute
		 */
		addUsersSelection: function (selectionArray) {
			$.merge(this.selectedUsers, selectionArray);
			this.selectedUsers = this.selectedUsers.getUnique();
		},
		/**
		 * Sub an array of users IDs to current selection, saved in UserMan object attribute
		 */
		subUsersSelection: function (selectionArray) {
			this.selectedUsers = $(this.selectedUsers).not(selectionArray).get();
			this.selectedUsers = this.selectedUsers.getUnique();
		},
		/**
		 * Vizualize (apply) current global selection on current page: check checkboxes, set row selection status, etc.
		 * Strangely enough, I can't find a nice API in yiigridview.js to do this in a single call.
		 *
		 */
		updateCurrentPageSelection: function () {
			var that = this;
			var keys = [];
			var gridSelector = '#' + this.options.usersGridId;
			$(gridSelector).find('.keys span').each(function () {
				keys.push(parseInt($(this).text()));
			});

			var counter = 0;
			$(gridSelector + ' table.items').children('tbody').children().each(function (i) {
				if ($.inArray(keys[i], that.selectedUsers) > -1) {
					$(this).addClass('selected').find('input.select-on-check[type="checkbox"]').attr('checked', true);
					counter++;
				} else {
					$(this).removeClass('selected').find('input.select-on-check[type="checkbox"]').attr('checked', false);
				}
			});

			if (counter >= keys.length) {
				$(gridSelector + ' input.select-on-check-all').each(function () {
					$(this).attr('checked', true);
				});
			} else {
				$(gridSelector + ' input.select-on-check-all').each(function () {
					$(this).attr('checked', false);
				});
			}

		},
		/**
		 * Update USers grid: call grid's "update", feeding it with search form data (filter data)
		 */
		updateUsersGrid: function () {
			$('#' + this.options.usersGridId).yiiGridView('update', {
				data: this.getFilterData()
			});
		},
		/**
		 * Add one more filter to advanced filters list
		 */
		addAdvancedFilter: function ($selected) {
			if ($selected.hasClass('additional'))
				this.addAdditionalFieldAdvancedFilter($selected);
			else
				this.addStandardAdvancedFilter($selected);
		},
		deSelectAllUsers: function () {
			$("#" + this.options.containerId + ' .userman-deselect-all').trigger('click');
		},
		/**
		 * Add new HTML for a standard advnaced filter (like Name, Email, ...)
		 */
		addStandardAdvancedFilter: function ($selected) {
			var that = this;

			// Update current list of adv. filters
			this.advancedSearchFilters[$selected.val()] = $selected.text();

			var label = $('<label/>', {'for': $selected.val(), 'text': $selected.text()});
			var filterRow = $('<div/>', {'class': 'filter-row clearfix'}).append(label);
			var fieldClass = 'remove-advanced-filter-link';
			if (!$selected.hasClass('dropdown-filter')) {
				var select = $('<select/>', {
					'id': $selected.val(),
					'name': 'advancedSearch[fields][' + $selected.val() + '][condition]'
				});
				var options = [
					{value: 'contains', 'text': 'Contains'},
					{value: 'equal', 'text': 'Equal to'},
					{value: 'not-equal', 'text': 'Not equal to'}
				];
				if ($selected.val() == 'expiration') {
					options = $(window['expirationJson']);
				}
				$(options).each(function () {
					select.append($('<option/>', {'value': this.value, 'text': this.text}));
				});

				var input = $('<input/>', {
					'type': 'textfield',
					'name': 'advancedSearch[fields][' + $selected.val() + '][keywords]'
				});

				if ($selected.val() == 'expiration') {
					var div = $('<div class="input-append date">');
					filterRow.css('position', 'relative');
					input.css({'readonly': 'readonly', 'position': 'relative'});
					var icon = '<span class="add-on"><i class="icon-calendar"></i></span>';
					div.append(select);
					div.append(input);
					div.append(icon);
					filterRow.append(div);
					var dateFormat = $selected.data('date-format');
					try {
						input.bdatepicker({format: dateFormat});
					} catch (e) {
					}
				} else {
					filterRow.append(select).append(input);
				}

			} else {
				var select = $('<select/>', {
					'id': $selected.val(),
					'name': 'advancedSearch[fields][' + $selected.val() + '][keywords]'
				});
				$(window[$selected.val() + 'Json']).each(function () {
					console.log(this);
					select.append($('<option/>', {'value': this.value, 'text': this.text}));
				});

				fieldClass += ' dropdown-filter'

				filterRow.append(select);
			}

			var removeLink = $('<a/>', {
				'class': fieldClass,
				'data-filter-value': $selected.val(),
				'data-filter-text': $selected.text(),
				'click': function () {
					that.removeFilter($(this));
				},
				'text': 'X'
			});

			filterRow.append(removeLink);

			$('#userman-advanced-search-filters').append(filterRow);

			$selected.remove();
			$('select').trigger('refresh');


		},
		/**
		 * Add new HTML for an ADDITIONAL FIELD advanced filter
		 */
		addAdditionalFieldAdvancedFilter: function ($selected) {
			var that = this, fieldType = $selected.data('field-type');
			// Keep it in the list
			this.advancedSearchFilters[$selected.val()] = $selected.text();

			var input = $("#" + this.options.containerId + ' .additional-fields-elements').find('.' + $selected.val()).html();

			var removeLink = $('<a/>', {
				'class': 'remove-advanced-filter-link additional',
				'data-filter-value': $selected.val(),
				'data-filter-text': $selected.text(),
				'data-filter-type': fieldType,
				'click': function () {
					that.removeFilter($(this));
				},
				'text': 'X'
			});

			var filterRow = $('<div/>', {'class': 'filter-row clearfix'}).append(input).append(removeLink);

			//some field type require JS post-processing operations
			// TODO: field type definitions should rely on the constants defined in PHP models
			switch (fieldType) {
				case 'date':
					//apply datepicker to date inputs
					try {
						filterRow.find('.input-append.date input').bdatepicker();
					} catch (e) {
					}
					break;
			}

			filterRow.find('select, input, textarea').removeAttr('disabled');

			$('#userman-advanced-search-filters').append(filterRow);

			$selected.remove();
			$('select').trigger('refresh');

		},
		/**
		 * Remove filter (the HTML) from filters based on clicked "remove" link
		 */
		removeFilter: function (link) {

			// Remove the filter from the list
			delete this.advancedSearchFilters[link.data('filter-value')];

			// Put back the filter in the dropdown
			var option = $('<option/>', {
				'value': link.data('filter-value'),
				'text': link.data('filter-text'),
				'class': link.hasClass('additional') ? 'additional' : (link.hasClass('dropdown-filter') ? 'dropdown-filter' : '')
			});
			option.data('field-type', link.data('filter-type'));

			$('#add-filter-select').append(option);
			link.closest('.filter-row').remove();
			$('#add-filter-select').trigger('refresh');
		},
		/**
		 * Fancytree Lazyloader Ajax object
		 */
		fancyChartTreeLazyLoaderAjaxObject: function (event, treeData) {
			var url = '/lms/index.php?r=usersSelector/axGetOrgchartFancytreeJson';
			var requestData = {
				nodeKey: treeData.node.key,
				lazyMode: 1,
				indents: 0,
				powerUserFilter: 1,
				expandRoot: 1,
				treeGenerationTime: $('#tree-generation-time').val()
			};

			var ajaxObject = {
				cache: false,
				async: true,
				type: 'POST',
				dataType: "json",
				url: url,
				data: requestData
			}
			return ajaxObject;
		},
		/**
		 * When orgchart tree is clicked, anywhere
		 */
		onFancyTreeClick: function (event, data) {
			return true;
		},
		/**
		 * Before node is activated
		 */
		beforeFancyTreeNodeActivate: function (event, data) {
			return true;
		},
		/**
		 * When a branch/node is activated
		 */
		onBranchActivate: function (event, data) {
			var breadCrumb = this.buildBreadcrumbs(data.node, " &raquo; ", false);
			$("#userman-tree-breadcrumbs").html(breadCrumb);

			// Show/Hide  "include descendants" option, based on activated node (root ==> hide)
			if (data.tree.getFirstChild().key == data.node.key) {
				$('#userman-include-descendants-controls').fadeOut();
				$('#CoreUser_containChildren').attr('checked', false);
			} else {
				$('#userman-include-descendants-controls').fadeIn();
			}

			// Ok, although we are using "persist" Fancytree extension,
			// we must preemtively set the cookie to the new Node, because
			// up until this point 'persist' did not yet set.. and it is too late after that..  a matter of timing..
			$.cookie('userman-fancytree-active', data.node.key, {path: '/'});

			if (!this.inFirstRun) {
				this.updateUsersGrid();
			}
			this.inFirstRun = false;
		},
		/**
		 * When a single OrgChart breadcrumbs item is clicked (i.e. some node)
		 * Result -> activate it
		 */
		onBreadcrumbsItemClick: function (e) {
			var $target = $(e.currentTarget);
			var key = $target.data('key');
			var tree = $('#' + this.options.orgchartFancyTreeId).fancytree('getTree');
			tree.activateKey(key);
			e.preventDefault();
			return false;
		},
		/**
		 * Build a 'breadcrumbs' path to a given node
		 */
		buildBreadcrumbs: function (node, sep, excludeSelf) {
			var path = [];
			var last = true;
			node.visitParents(function (n) {
				if (n.parent) {
					if (last) {
						var item = '<span class="last-crumb">' + n.title + '</span>';
					} else {
						var item = "<a class='userman-breadcrumbs-item' data-key='" + n.key + "' href='#'>" + n.title + "</a>";
					}
					path.unshift(item);
					last = false;
				}
			}, !excludeSelf);
			return path.join(sep);
		},
		/**
		 * Save in a cookie the current status of column selection.
		 * Require jquery.cookie.js!
		 */
		saveGridColumnsSelection: function () {
			var selectedFields = $('.userman-select-columns .selected, .userman-select-columns .locked');
			var selected = [];

			$(selectedFields).each(function () {
				selected.push($(this).data('column-key'));
			});

			// Set this cookie to hold visible columns
			$.cookie(this.options.selectedColumnsCookieName, JSON.stringify(selected));

			// Set back content of the trigger/picker
			var trigger = $('.filters .popover-trigger.userman-select-columns');
			var content = $(trigger).next('.popover').find('.popover-content').html();
			$(trigger).data('content', content);

		},
		/**
		 * Hide non-selected grid columns
		 */
		setGridColumnsVisibility: function () {
			var that = this;
			$.each(this.hiddenGridColumns, function (index, columnKey) {
				$("#" + that.options.usersGridId).find('.td-' + columnKey).hide();
			});

		},
		/**
		 * Initialize the popover for show/hide grid columns
		 */
		initGridColumnsSelectorPopover: function () {
			var that = this;
			$('.filters .popover-trigger.userman-select-columns').popover({
				placement: 'bottom',
				html: true,
				callback: function () {
					that.saveGridColumnsSelection();
				},
				afterRender: function () {
					that.updateMenuGridColumnsSelection();
				},
				afterDestroy: function () {
					that.initGridColumnsSelectorPopover();
				}
			});
		},
		/**
		 * Update showed selected columns when the selector menu is showed
		 */
		updateMenuGridColumnsSelection: function () {
			$('.userman-select-columns').find('li').each(function () {
				var li = $(this);
				if (li.hasClass('locked'))
					return; //userid column cannot be changed or hidden
				if (UserMan) { //make sure that global user management object exists
					var columnKey = li.data('column');
					if (columnKey) {
						columnKey = columnKey.replace('td-', ''); //retrieve column key from "data-column" attribute of the <li> element
						index = $.inArray(columnKey, UserMan.hiddenGridColumns);
						if (index > -1) {
							//the element is enlisted in the hidden columns, so no marker has to be showed in the menu row
							li.removeClass('selected');
						} else {
							//the element is not hidden, so show the selected marker
							li.addClass('selected');
						}
					}
				}
			});
		},
		/**
		 * Serialize search form data and add more filtering attributes, if any. Consumed by Yii Users grid update call
		 */
		getFilterData: function () {
			var $form = $('#' + this.options.searchFormId);
			var data = $form.serialize();
			var activeNode = $('#' + this.options.orgchartFancyTreeId).fancytree('getTree').activeNode;
			if (activeNode) {
				data = data + '&currentNodeId=' + activeNode.key;
			}
			return data;
		},
		/**
		 * Fired by Yii CGridView beforeAjaxUpdate event (see grid widget in the view)
		 */
		beforeUsersGridUpdate: function (id, options) {
			this.debug('Execute "before grid update"');
			options.data = this.getFilterData();
		},
		afterUsersGridUpdate: function (id, data) {
			this.updateCurrentPageSelection();
		},
		/**
		 * Called Upon row/checkbox selection change in the grid
		 */
		gridSelectionChanged: function (gridId) {
			// Get selections at the current grid PAGE
			var pageSelectionArray = $('#' + gridId).yiiGridView('getSelection');

			// Make it array of integers
			for (i in pageSelectionArray) {
				pageSelectionArray[i] = parseInt(pageSelectionArray[i]);
			}

			// Update selector internal attributes holding selections
			this.updateSelectionArrayFromPage(gridId, pageSelectionArray);
			this.updateSelectionCounter();
		},
		/**
		 * Update selector's internal attributes holding selections.
		 * Called upon Grid Selection change, e.g. user selects or deselects an item, etc.
		 */
		updateSelectionArrayFromPage: function (gridId, selectionArray) {
			var that = this;
			var value = 0;
			var index = 0;

			// Enumerate all checkboxes in the given grid and update selected array
			$('#' + gridId + ' input[type="checkbox"]').not('.select-on-check-all').each(function () {
				value = parseInt($(this).val());
				if ($.inArray(value, selectionArray) > -1) {
					that.selectedUsers.push(value);
				} else {
					index = $.inArray(value, that.selectedUsers);
					if (index > -1) {
						that.selectedUsers.splice(index, 1);
					}
				}
			});

			// make selecetd array unique
			this.selectedUsers = this.selectedUsers.getUnique();
		},
		/**
		 * 1) Update visual counters for grids
		 * 2) PUT the list of selected items in the HIDDEN field used to submit the list later
		 */
		updateSelectionCounter: function () {
			console.log(this.selectedUsers);
			$('#userman-users-selected-count').text(this.selectedUsers.length);
			//$('input[name="'+this.options.selectedUsersFieldName+'"]').val(JSON.stringify(this.selectedUsers));
		},
		onMassiveActionChange: function (e) {
			var $target = $(e.currentTarget);
			var selectedOption = $target.val();
			var selectedUsers = this.selectedUsers;
			this.selectedUsers = [];
			if (typeof (e) == "string") {
				selectedOption = e;
			}
			;
			if (selectedUsers.length > 0) {
				selectedUsers = selectedUsers.join();
				switch (selectedOption) {

					case 'deactivate':
						config = {
							'id': 'modal-' + this.getRandKey(),
							'modalClass': 'deactivate-action',
							'modalTitle': Yii.t('standard', '_DEACTIVATE'),
							'modalUrl': HTTP_HOST + '?r=userManagement/changeStatus&status=0',
							'modalRequestType': 'POST',
							'modalRequestData': {'idst': selectedUsers},
							'buttons': [
								{"type": "submit", "title": Yii.t("standard", Yii.t("standard", "_CONFIRM"))},
								{"type": "cancel", "title": Yii.t("standard", Yii.t("standard", "_CLOSE"))}
							],
							'afterLoadingContent': 'hideConfirmButton();',
							'afterSubmit': 'updateUserContent'
						};

						$('body').showModal(config);
						break;

					case 'activate':
						config = {
							'id': 'modal-' + this.getRandKey(),
							'modalClass': 'activate-action',
							'modalTitle': Yii.t("standard", "_ACTIVATE"),
							'modalUrl': HTTP_HOST + '?r=userManagement/changeStatus&status=1',
							'modalRequestType': 'POST',
							'modalRequestData': {'idst': selectedUsers},
							'buttons': [
								{"type": "submit", "title": Yii.t("standard", "_CONFIRM")},
								{"type": "cancel", "title": Yii.t("standard", "_CLOSE")}
							],
							'afterLoadingContent': 'hideConfirmButton();',
							'afterSubmit': 'updateUserContent'
						};

						$('body').showModal(config);
						break;


					case 'delete':
						config = {
							'id': 'modal-' + this.getRandKey(),
							'modalClass': 'delete-node',
							'modalTitle': Yii.t('standard', '_DEL_SELECTED'),
							'modalUrl': HTTP_HOST + '?r=userManagement/deleteUser',
							'modalRequestType': 'POST',
							'modalRequestData': {'idst': selectedUsers},
							'buttons': [
								{"type": "submit", "title": Yii.t("standard", "_CONFIRM")},
								{"type": "cancel", "title": Yii.t("standard", "_CLOSE")}
							],
							'afterLoadingContent': 'hideConfirmButton();',
							'afterSubmit': 'updateUserContent'
						};
						$('body').showModal(config);
						break;

					case 'edit':
						$('#user-massive-update').data('modal-send-data', {idst: selectedUsers});
						$('#user-massive-update').click();
						break;
					case 'assign_node_append':
						config = {
							'id': 'modal-' + this.getRandKey(),
							'modalClass': 'modal-' + $target.val(),
							'modalTitle': Yii.t('standard', 'Add to branch'),
							'modalUrl': HTTP_HOST + '?r=userManagement/assignToNode',
							'modalRequestType': 'POST',
							'modalRequestData': {'idst': selectedUsers, 'assign_type': 'append'},
							'buttons': [
								{"type": "submit", "title": Yii.t("standard", "_CONFIRM")},
								{"type": "cancel", "title": Yii.t("standard", "_CLOSE")}
							],
							//'afterLoadingContent': 'hideConfirmButton();',
							'afterSubmit': 'updateUserContent'
						};
						$('body').showModal(config);
						break;
					case 'assign_node_move':
						config = {
							'id': 'modal-' + this.getRandKey(),
							'modalClass': 'modal-' + $target.val(),
							'modalTitle': Yii.t('standard', 'Move to branch'),
							'modalUrl': HTTP_HOST + '?r=userManagement/assignToNode',
							'modalRequestType': 'POST',
							'modalRequestData': {'idst': selectedUsers, 'assign_type': 'move'},
							'buttons': [
								{"type": "submit", "title": Yii.t("standard", "_CONFIRM")},
								{"type": "cancel", "title": Yii.t("standard", "_CLOSE")}
							],
							//'afterLoadingContent': 'hideConfirmButton();',
							'afterSubmit': 'updateUserContent'
						};
						$('body').showModal(config);
						break;
					case 'assign_node_remove':
						var heading = Yii.t('standard', '_REMOVE_FROM_NODE');
						var link = HTTP_HOST + '?r=userManagement/unassignFromNode';
						var ids = 'modal-' + this.getRandKey();
						var className = 'ajax modal-' + $target.val();
						var ajax = 'post';
						var datas = {
							'idst': selectedUsers,
							'assign_type': 'remove'
						};
						openDialog(null, heading, link, ids, className, ajax, datas);

						break;
					case 'export_users_csv':
						var title = Yii.t('standard', '_EXPORT_CSV');
						var url = HTTP_HOST + 'index.php?r=userManagement/exportUsersToCsv';
						var id = 'users-export-csv-dialog';
						var dialogClass = 'users-export-csv';
						var ajaxType = 'post';
						var data = {
							'idst': selectedUsers,
							'fileType': $target.val()
						};

						openDialog(null, title, url, id, dialogClass, ajaxType, data);
						break;
					case 'export_users_xls':
						var xTitle = Yii.t('standard', '_EXPORT_XLS');
						var xUrl = HTTP_HOST + 'index.php?r=userManagement/exportUsersToCsv';
						var xId = 'users-export-xls-dialog';
						var xDialogClass = 'users-export-xls';
						var xAjaxType = 'post';
						var xData = {
							'idst': selectedUsers,
							'fileType': $target.val()
						};

						openDialog(null, xTitle, xUrl, xId, xDialogClass, xAjaxType, xData);
						break;

					case 'deleteExperts':
						this.updateCurrentPageSelection();
						this.updateSelectionCounter();
						openDialog(false, // Event
								Yii.t('app7020', 'Delete Experts'), // Title
								app7020Options.axExpertsChannelsController + '/deleteExpert', // Url
								'app7020-multipleDeleteExpertsChannels-dialog', // Dialog-Id
								'app7020-multipleDeleteExpertsChannels-dialog', // Dialog-Class
								'POST', // Ajax Method
								{// Ajax Data
									'idUser': selectedUsers
								}
						);
						break;
				}
			}

			setTimeout(function () {
				$('option:first-child', $target).attr("selected", "selected");
			}, 1000);
		},
		/**
		 * Render tree columns; We are mainly interested in the last one (Index: 4): User actions popover
		 */
		treeRenderColumns: function (event, data) {
			var node = data.node;
			var $tdList = $(node.tr).find(">td");
			var that = this;
			var poContainerId = this.options.branchActionsContainerIdPrefix + node.key;
			var html = '';

			// User Actions POPOVER trigger HTML (with a button inside)
			if (that.options.enableNodeActions) {
				html = '<div id="' + poContainerId + '"></div><div class="' + this.options.branchActionsTriggerClass + '"><span class="userman-menu"></span></div>';
				$tdList.last().html(html);
			}

			if ((that.options.allowMoveNode) && (data.tree.getFirstChild().key != data.node.key)) {
				html = '<div class="' + this.options.branchDragHandlerClass + '"><span class="i-sprite is-move grey"></span></div>';
				$tdList.eq(-2).html(html);
			}

			// On click event, load per-branch actions (last table column)
			$tdList.last().find('.' + that.options.branchActionsTriggerClass)
					.off('click')
					.on('click', function (e) {


						var poTrigger = $(this);

						// Next click will be handled by the Bootstrap Popover! This avoids double loading of the popover contecn
						// poTrigger.unbind('click');

						var url = '/admin/index.php?r=userManagement/axRenderBranchActionsMenu';
						var data = {
							idOrg: node.key
						};

						// Hide popover when mouse leaves the node row or the popover content container
						$('#' + poContainerId).on('mouseleave', function () {
							poTrigger.popover('hide');
						});
						$(node.tr).on('mouseleave', function () {
							poTrigger.popover('hide');
						});

						// Avoid "node activation" when user clicks on "node action popover trigger"
						e.stopPropagation();
						e.preventDefault();

						// Get branch actions HTML and put it into popover
						$.ajax({
							url: url,
							data: data,
							dataType: 'json',
							type: 'POST',
							success: function (response, textStatus, jqXHR) {
								if (response.success) {
									poTrigger.popover({
										content: response.html,
										placement: 'bottom',
										html: true,
										container: '#' + poContainerId,
										trigger: 'manual'
									})
											.popover('show')
											.on('click', function (e) {
												// Avoid "node activation" upon 'node action trigger' click. It just opens the popover again
												e.stopPropagation();
											});
								}
							}
						});

						return false;
					});


			// Prevent Fancytree's TD to catch the click on branch actions trigger button
			$tdList.last().on('click', function (e) {
				//e.stopPropagation();
				//return false;
			});

		},
		/**
		 * Drag & Drop:  drag started
		 */
		dnd_dragStart: function (node, data) {

			// Do NOT allow moving the top Org Chart (visible root, e.g. 'Docebo')
			if (data.tree.getFirstChild().key == data.node.key) {
				return false;
			}
			return true;
		},
		/**
		 * Drag & Drop:  Drag Marker entered a node
		 */
		dnd_dragEnter: function (node, data) {

			// Allow ONLY putting (hitMode=over) a node inside the top Org Chart node (visible root)
			// (Do not allow making any other node to be a root sibling)
			if (data.tree.getFirstChild().key == data.node.key) {
				return ['over'];
			}
			return true;
		},
		/**
		 * Drag & Drop: Drag marker dropped, node dropped.
		 * Make an AJAX call to update the tree in the backend
		 */
		dnd_dragDrop: function (node, data) {

			// Update backend
			var url = "/admin/index.php?r=orgManagement/axMoveNode";
			var that = this;
			var requestData = {
				idOrg: data.otherNode.key, // node which is moved, the moving node
				idOrgTarget: node.key, // the target node, where drop is happening (either bfore, after or over)
				hitMode: data.hitMode, // How/where to move the moving node in relation to target node: 'before', 'after', 'over'
				treeGenerationTime: $('#tree-generation-time').val()
			};

			// AJAX
			$.ajax({
				type: 'POST',
				dataType: "json",
				url: url,
				data: requestData,
				success: function (responseJSON) {
					//the org. chart tree needs to be reloaded due to changes in its structure
					if (responseJSON.update_tree || (responseJSON.data && responseJSON.data.update_tree)) {
						data.tree.ext.docebo._handleTreeReloading.call(data.tree, {tree: data.tree, node: node}, this);
						return;
					}
					if (responseJSON.new_tree_generation_time || (responseJSON.data && responseJSON.data.new_tree_generation_time)) {
						var new_tree_generation_time = '';
						if (responseJSON.new_tree_generation_time) {
							new_tree_generation_time = responseJSON.new_tree_generation_time;
						}
						if (responseJSON.data && responseJSON.data.new_tree_generation_time) {
							new_tree_generation_time = responseJSON.data.new_tree_generation_time;
						}
						if (new_tree_generation_time != '') {
							$('#tree-generation-time').val('' + new_tree_generation_time);
						}
					}
					//regular callback
					if (responseJSON.success) {
						node.setExpanded(true).always(function () {
							// Execute UI move, make it visible
							data.otherNode.moveTo(node, data.hitMode);
							data.tree.getFirstChild().render();
							if (data.hitMode == 'over') {
								node.folder = true;
								node.renderStatus();
							}
							that.updateUsersGrid();
						});
					} else {
						if (typeof responseJSON.message == "string") {
							Docebo.Feedback.show('error', responseJSON.message);
						}
					}
				}
			});

		},
		/**
		 * AJAX-loading orgchart fancytree in the designated DOM element.
		 * Execute callback (function), if passed
		 */
		loadOrgChartTree: function (callback, otherParams) {
			var url = '/admin/index.php?r=userManagement/axRenderOrgchartFancyTree&orgchartFancyTreeId=' + this.options.orgchartFancyTreeId;
			var that = this;

			//handle additional URL parameter, if requested in the arguments
			if (otherParams && $.isPlainObject(otherParams)) {
				url += '&' + $.param(otherParams);
			}

			$.ajax({
				url: url,
				type: "POST",
				dataType: "html",
				success: function (response, textStatus, jqHXR) {
					$('#' + that.options.fancytreeOrgchartWrapperId).html(response);
					if (typeof callback == 'function') {
						callback(response, textStatus, jqHXR);
					}
				}
			});

		},
		afterNodeAssignUsersSuccess: function (data) {
			this.updateUsersGrid();
		},
		/**
		 * Simple random generator: Generate and return random integer ID
		 */
		getRandKey: function () {
			return Math.floor(Math.random() * 10000);
		},
		reloadTreeHandler: function (ctx, source) {
			//refresh the tree (destroy and re-create it)
			var $div = ctx.tree.$div;
			var $id = 1;
			try {
				//$div.fancytree("destroy");
				ctx.tree.destroy();
				$div.html('');
			} catch (e) {
				Docebo.log(e);
			}
			this.loadOrgChartTree(false, {reload: 1});
		},
	};


	/**
	 * Class constructor
	 *
	 * Used in Import Users from CSV process (from Step 2, actionImportUsers)
	 *
	 */
	AdminUserManagementImportCsv = function (options) {
		this.defaultOptions = {
			debug: false
		};
		if (typeof options == null) {
			options = {};
		}
		this.init(options);
	};

	/**
	 * Prototype: AdminUserManagementImportCsv
	 */
	AdminUserManagementImportCsv.prototype = {
		/**
		 * Initialize class object
		 */
		init: function (options) {
			this.options = $.extend({}, this.defaultOptions, options);
			this.addListeners();
		},
		addListeners: function () {
			var that = this;

			// Add Step2 Form data to POST request upon opening the progressive dialog (ajaxType=POST)
			// Also, search for any form(s) inside the dialog (if any!!) and add fields to POST data
			$(document).on('dialog2.before-send', '#progressive-import-dialog', function (e, xhr, settings) {
				settings.data = $('#user-import-form-step2').serialize();
				var dialogForms = $('#progressive-import-dialog').find('form');
				$.each(dialogForms, function (i, aForm) {
					settings.data = settings.data + '&' + $(aForm).serialize();
				});
			});

		},
		/**
		 * Make our live easier and provide some debug, if enabled
		 */
		debug: function (message) {
			if (this.options.debug === true) {
				console.log('UserMan:', message);
			}
		}


	};





})(jQuery);


