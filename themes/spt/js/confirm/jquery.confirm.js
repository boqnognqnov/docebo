(function($){
	
	$.confirm = function(params){
		
		if($('#confirmOverlay').length){
			// A confirm is already shown on the page:
			return false;
		}
		if (!params.afterShow) {
			params.afterShow = function() {};
		}
		
		var buttonHTML = '';
		$.each(params.buttons,function(name,obj){
			
			// Generating the markup for the buttons:
			
			//buttonHTML += '<a href="#" class="button '+obj['class']+'">'+name+'<span></span></a>';
			buttonHTML += '<input type="button" class="button '+obj['class']+'" value="'+name+'"/>';

			if(!obj.action){
				obj.action = function(){};
			}
		});

		//boxClass = params.popupClass !== undefined ? ' class="'+params.popupClass+'"' : '';
		boxClass = ' class="modal delete-node" style="display:block;"';

		if (params.confirmMessage !== undefined) {
			confirmText = [
				'<div class="clearfix">',
				'<input id="confirmCheckbox" type="checkbox" onchange="(($(this).prop(\'checked\')) ? showConfirmButton() : hideConfirmButton());"/>',
				'<label for="confirmCheckbox">',params.confirmMessage,'</label>',
				'</div>'
			].join('');
		} else {
			confirmText = '';
		}

		var markup = [
			'<div id="confirmOverlay">',
			'<div id="confirmBox"',boxClass,'>',
			'<div class="modal-header">',
			'<input type="button" class="close" onclick="$.confirm.hide();"/>',
			'<h3><span></span>',params.title,'</h3>',
			'</div>',
			'<div class="modal-body">',
			'<div class="form">',
			'<p>',params.message,'</p>',
			'<div class="clearfix">',
			confirmText,
			'</div>',
			'</div>',
			'</div>',
			'<div id="confirmButtons" class="modal-footer">',
			buttonHTML,
			'</div></div></div>'
		].join('');

		$(markup).hide().appendTo('body').fadeIn(400, params.afterShow);

		if (params.confirmMessage !== undefined) {
			hideConfirmButton();
		}

		var buttons = $('#confirmBox .button'),
			i = 0;

		$.each(params.buttons,function(name,obj){
			buttons.eq(i++).click(function(){
				
				// Calling the action attribute when a
				// click occurs, and hiding the confirm.
				
				obj.action();
				$.confirm.hide();
				return false;
			});
		});
	}

	$.confirm.hide = function(){
		$('#confirmOverlay').fadeOut(function(){
			$(this).remove();
		});
	}
	
})(jQuery);