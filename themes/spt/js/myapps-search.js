/**
 * Created by Georgi on 10.11.2015 г..
 */
var SearchEngine = false;

(function($){

    /*
     * Constructor
     */
    SearchEngine = function(options){
        this.mainContainer = false;
        this.initialUrl = false;
        this.initialDatUrl = false;
        this.buttonListener = false;
        this.lang = false;
        this.template = false;
        this.filterButtons = false;
        this.clearTextIconClass = false;
        this.backButtonText = '';
        this.resutsFoundText = '';
        this.isInTrialPeriod = false;

        this.data = false;
        this.dataLength = 0;
        this.filter = false;
        this.initialContainer = false;
        this.filterText = "";

        this.defaultOptions = {
            debug: true
        };

        if(typeof options === "undefined"){
            var options = {};
        } else{
            this.mainContainer = options.mainContainer;
            this.initialUrl = options.url;
            this.initialDatUrl = options.dataUrl;
            this.buttonListener = options.buttonListener;
            this.filterButtons = options.filterButtons || false;
            this.clearTextIconClass = options.clearTextIconClass || false;
            this.backButtonText = options.backButtonText || '';
            this.resutsFoundText = options.resultsFoundText || '';
            this.isInTrialPeriod = options.isInTrialPeriod || false;
        }


        if(!this.mainContainer || !this.initialUrl || !this.buttonListener){
            return false;
        }

        this.init(options);
    };

    /**
     * Prototype
     */
    SearchEngine.prototype = {
        init: function(options){

            var that = this;

            $.ajax({
                method: 'POST',
                url: this.initialUrl,
                data: this.initialDatUrl

            })
                .done(function(res){
                    that.data = JSON.parse(res);
                    that.data = that.data.html;
                    that.dataLength = Object.keys(that.data).length;
                });

            this.options = $.extend({}, this.defaultOptions, options);
            this.debug('Initilize SearchEngine');
            this.addListeners(that);
        },

        debug: function(message){
            if(this.options.debug === true){
                console.log('SearchEngine: ', message);
            }
        },

        addListeners: function(that){
            $('input[name=' + this.buttonListener + ']').on('keyup', function(){
                that.filterText = $(this).val();
                that.searchForApp();
            });

            $('input[name=' + this.filterButtons + ']').on('change', function(){
                that.filter = $(this).val();
                if(that.initialContainer !== false){
                    that.searchForApp();
                } else{
                    if(that.filter.length > 0){
                        that.searchForApp();
                    }
                }
            });

            $('.' + this.clearTextIconClass).on('click', function(){
                if(that.filterText.length > 0){
                    that.resetSearchField();
                    that.searchForApp();
                    that.resetFilterButtons();
                }
            });
        },

        resetSearchField: function(){
            $('input[name=' + this.buttonListener + ']').val('');
            this.filterText = '';
        },

        searchForApp: function(){
            var searchTerm = this.filterText;
            var arrayOfFound = [];
            for(var elem in this.data){
                //I don't know why!!!!!!!
                if(elem == 'last') continue;

                var name = String(this.data[elem]['name']);
                var description = String(this.data[elem]['description']);

                if(name.toLowerCase().match(searchTerm.toLowerCase()) || description.toLowerCase().match(searchTerm.toLowerCase())){
                    if(this.filter === 'free' || this.filter === 'paid'){
                        if(this.filter === 'free'){
                            if(this.data[elem]['isFree'] == true){
                                arrayOfFound.push(elem);
                            } else{
                                if(this.data[elem].app.use_contact_us !== "undefined" && this.data[elem].app.use_contact_us == 0 && this.isInTrialPeriod === false && this.data[elem].app.free_on_premium == true && this.data[elem].isECSInstallation == true){
                                    arrayOfFound.push(elem);
                                }
                            }
                        } else if(this.filter === 'paid'){
                            if(this.data[elem]['isFree'] == false){
                                if(this.data[elem].app.use_contact_us !== "undefined" && this.data[elem].app.use_contact_us == 0 && this.isInTrialPeriod === false && this.data[elem].app.free_on_premium == true && this.data[elem].isECSInstallation == true){
                                    continue;
                                } else{
                                    arrayOfFound.push(elem);
                                }
                            }
                        }
                    } else{
                        arrayOfFound.push(elem);
                    }
                }
            }

            this.render(arrayOfFound);
        },

        render: function(indexes){
            var buildHtml = [];

            for(var elem in this.data){
                if(jQuery.inArray(elem + '', indexes) !== -1){
                    buildHtml.push(this.data[elem]['body']);
                }
            }

            this.renderData(buildHtml);
        },

        renderData: function(dataArray){
            var dataLen = dataArray.length;

            if(this.initialContainer === false){
                this.initialContainer = $('.' + this.mainContainer).html();
            }

            $('.' + this.mainContainer).html('');

            var html = '<h3 class="title-bold back-button"><a id="back-button" href="#" >' + this.backButtonText +  '</a><span>' + dataLen + '&nbsp' + this.resutsFoundText + '</span></h3>';
            $('.' + this.mainContainer).append(html);
            for(var i = 0; i < dataLen; i++){
                $('.' + this.mainContainer).append(dataArray[i]);
            }

            var that = this;

            $('a#back-button').on('click', function(){
                if(that.initialContainer !== false){
                    $('.' + that.mainContainer).html(that.initialContainer);
                    that.initialContainer = false;
                    that.resetFilterButtons(true);
                    that.resetSearchField();
                }
            });

            $(document).controls();
        },

        getFilterButtonIndex: function(reset){

            if(reset !== "undefined" && reset === true) return 0;

            switch (this.filter){
                case "":
                    return 0;
                    break;
                case "free":
                    return 1;
                    break;
                case "paid":
                    return 2;
                    break;
                default:
                    return 0;
                    break;
            }
        },

        resetFilterButtons: function(resetAll){

            $('span[id^=' + this.filterButtons +']').each(function(){
                $(this).removeClass('checked');
            });
            $('span#' + this.filterButtons + '_' + this.getFilterButtonIndex(resetAll) + '-styler').addClass('checked');

            if(resetAll !== "undefined" && resetAll === true){
                this.filter = "";
            }
        }
    };
}) (jQuery);