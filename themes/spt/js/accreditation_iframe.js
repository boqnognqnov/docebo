/**
 * jQuery Plugin for the Accreditation iframe (aka Thomson Reuters IFRAME)
 *
 * @param $
 */
(function($) {

    /* Enable disable log() output to console */
    var debug = true;

    /* Array of USA state names indexed by abbreviation */
    var usaStateNames = {
        'Alabama': 'AL',
        'Alaska': 'AK',
        'American Samoa': 'AS',
        'Arizona': 'AZ',
        'Arkansas': 'AR',
        'California': 'CA',
        'Colorado': 'CO',
        'Connecticut': 'CT',
        'Delaware': 'DE',
        'District Of Columbia': 'DC',
        'Federated States Of Micronesia': 'FM',
        'Florida': 'FL',
        'Georgia': 'GA',
        'Guam': 'GU',
        'Hawaii': 'HI',
        'Idaho': 'ID',
        'Illinois': 'IL',
        'Indiana': 'IN',
        'Iowa': 'IA',
        'Kansas': 'KS',
        'Kentucky': 'KY',
        'Louisiana': 'LA',
        'Maine': 'ME',
        'Marshall Islands': 'MH',
        'Maryland': 'MD',
        'Massachusetts': 'MA',
        'Michigan': 'MI',
        'Minnesota': 'MN',
        'Mississippi': 'MS',
        'Missouri': 'MO',
        'Montana': 'MT',
        'Nebraska': 'NE',
        'Nevada': 'NV',
        'New Hampshire': 'NH',
        'New Jersey': 'NJ',
        'New Mexico': 'NM',
        'New York': 'NY',
        'North Carolina': 'NC',
        'North Dakota': 'ND',
        'Northern Mariana Islands': 'MP',
        'Ohio': 'OH',
        'Oklahoma': 'OK',
        'Oregon': 'OR',
        'Palau': 'PW',
        'Pennsylvania': 'PA',
        'Puerto Rico': 'PR',
        'Rhode Island': 'RI',
        'South Carolina': 'SC',
        'South Dakota': 'SD',
        'Tennessee': 'TN',
        'Texas': 'TX',
        'Utah': 'UT',
        'Vermont': 'VT',
        'Virgin Islands': 'VI',
        'Virginia': 'VA',
        'Washington': 'WA',
        'West Virginia': 'WV',
        'Wisconsin': 'WI',
        'Wyoming': 'WY'
    };

    String.prototype.capitalize = function(){
        return this.toLowerCase().replace( /\b\w/g, function (m) {
            return m.toUpperCase();
        });
    };

    /**
     * A nice logging/debugging function
     */
    var log = function() {
        if (!debug) return;
        if (window.console && window.console.log) {
            var args = Array.prototype.slice.call(arguments);
            if(args){
                $.each(args, function(index, ev){
                    window.console.log(ev);
                });
            }
        }
    };


    $.fn.getType = function(){ return this[0].tagName == "INPUT" ? this[0].type.toLowerCase() : this[0].tagName.toLowerCase(); }
    
    /**
     * Builds and sends the object to send to TR iframe
     * @param targetIframeWindow
     * @param $iframe
     * @param initObject
     * @param $jurisdictionAreaFieldId
     * @param $accreditationDurationFieldId
     */
    var sendIframeMessage = function (targetIframeWindow, $iframe, initObject, $jurisdictionAreaField, $accreditationDurationFieldId, settings) {
        // Get Jurisdicition value from field and transcode it
        var jurisditionName = $jurisdictionAreaField.find('option:selected').text();
        var jurisdiction = jurisditionName ? usaStateNames[jurisditionName.capitalize()] : '';

        // Get duration value
        var duration = $accreditationDurationFieldId.val() ? $accreditationDurationFieldId.val() : '';

        // Fill in the proper field of the message
        initObject.jurisdictionArea = [jurisdiction];
        initObject.accreditationDuration = duration;

        // Also add Course Info
        initObject.courseInfo = settings.courseInfo;
        
        // Also, Course sessions (could be empty)
        initObject.courseClassroomSessions = settings.courseClassroomSessions;

        // Also, Course sessions (could be empty)
        initObject.courseWebinarSessions = settings.courseWebinarSessions;

        // And all additional fields, in bulk, just all from the settings form form
        var allInputs = $iframe.closest("#additional_fields").find(':input[type!="hidden"][name^="LearningCourse[additional]"]');
        var allInputsArray = [];
		allInputs.each(function(index, value){
			var fieldType = $(this).getType();
			allInputsArray.push({
				field_translation : $(this).data('default-lang-translation'),
				field_value		  : $(this).val(),
				field_type		  : fieldType,
				field_text		  : fieldType === "select" ? $(this).find('option:selected').text() : $(this).val()
			});
		});
        
        initObject.allAdditionalFields = allInputsArray; 
        
        log("Sending message to Accreditation Iframe '" + $iframe.attr('id') + "' with object:");
        log(initObject);
        
        targetIframeWindow.postMessage(initObject, '*');
    }
    
    
    var debounce = function (func, wait, immediate) {
    	var timeout;
    	return function() {
    		var context = this, args = arguments;
    		var later = function() {
    			timeout = null;
    			if (!immediate) func.apply(context, args);
    		};
    		var callNow = immediate && !timeout;
    		clearTimeout(timeout);
    		timeout = setTimeout(later, wait);
    		if (callNow) func.apply(context, args);
    	};
    };

    /**
     * Callable methods in form of
     * $('#iframeID').accreditation('<method-name>', <arguments>).
     *
     */
    var methods = {

        /**
         * init() is a special methods and is called if no method is specified, e.g. $('#iframeID').dashboard({}) will call init().
         * It must be called always first!!!!!! Without calling it first, the whole plugin won't work!!!!
         */
        init: function(options) {
        	
            var settings = options;

            // Save various instance variables in this closure
            var $iframe = this; /* Jquery object for TR iframe */
            var iframeId = $iframe.attr('id');
            var $iframeHiddenField =  $("#" + $iframe.attr('id') + "_field"); /* Jquery hidden input for this iframe */
            var iframeWindow = document.getElementById($iframe.attr('id')).contentWindow; /* Window object generated by the current iframe */
            var $accreditationDuration = $(settings.accreditationDurationFieldId); /* Accreditation duration input field */
            var $jurisdictionArea = $(settings.jurisdictionAreaFieldId); /* Jurisdiction area  field */

            // Install onload handler to send the initialization object to the iframe
            $iframe.load(function() {
                sendIframeMessage(iframeWindow, $iframe, settings.initObject, $jurisdictionArea, $accreditationDuration, settings);
            });

            // Use debouncing method to limit the message sending rate
            var sendDebounced = debounce(sendIframeMessage,800);
            
            $(':input[type="text"][name^="LearningCourse[additional]"]').keyup(function(){
            	sendDebounced(iframeWindow, $iframe, settings.initObject, $jurisdictionArea, $accreditationDuration, settings);
            })

            $(':input[type!="hidden"][name^="LearningCourse[additional]"]').change(function(){
            	sendDebounced(iframeWindow, $iframe, settings.initObject, $jurisdictionArea, $accreditationDuration, settings);
            })
            
            // Set up a listener to detect messages coming from the iframe
            $(window).on("onmessage message", function(e) {
                var event = e.originalEvent;
                log("Received message with object:");
                log(event.data);

                // Identify the sender iframe (check if sender is equal to iframe we've created)
                if ((iframeWindow === event.source) && event.data) {
                    log("Saving message in hidden field.");
                    // Save data inside the hidden field
                    $iframeHiddenField.val(JSON.stringify(event.data));

                    // If Accred iframe requested, change its height
                    if (typeof event.data.height !== "undefined") {
                    	$iframe.height(event.data.height);
                    }
                    
                } else
                    log("Ignoring message. Either sender is unknown or data object is empty");
            });

            return this;
        },
    };

    /**
     * Plug it in
     */
    $.fn.accreditation = function(methodOrOptions) {
        if ( methods[methodOrOptions] ) {
            return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
            // Default to "init"
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  methodOrOptions + ' does not exist on jQuery.accreditation' );
        }
    };


})(jQuery);
