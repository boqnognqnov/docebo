#!/bin/sh

# ----------------------------------------------------------------------------------
#  Yii command starter.
#  Differs from the default (yiic) in that it defines an
#  environment variable equal to the FIRST script parameter that is
#  used by the invoked PHP script to load per-domain configuration.
#  Final result is: we can run Yii commands based on domain name and configuration.
#  
#  Example call:  
#  	yiic-domain.sh  mylms.domain.com  migrate
#  
#  Don't forget to change the file and make it executable:
#   chmod ugo+x yiic-domain.sh
# ----------------------------------------------------------------------------------

# Set/export the following environment variable to the domain name (first parameter)
export LMS_DOMAIN=$1

# Use the rest parameters to make the call to PHP script that will actually run the command (up to 9)
php yiic-domain.php $2 $3 $4 $5 $6 $7 $8 $9
