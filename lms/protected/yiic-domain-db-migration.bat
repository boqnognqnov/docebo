@echo off

rem ----------------------------------------------------------------------------------
rem  README !!!!!
rem  This command differs from yiic-domain in that it adds "migrate --interactive=0" at the end of command parameters
rem  and specifically meant to be used for applying Yii Database Migrations (NOT Release database migration, but intra-release DB fixes)
rem  Do NOT use it for ANY other purposes!!!
rem 
rem  Yii command starter.
rem  Differs from the default (yiic) in that it defines an
rem  environment variable equal to the FIRST script parameter that is
rem  used by the invoked PHP script to load per-domain configuration.
rem  Final result is: we can run Yii commands based on domain name and configuration.
rem
rem  Example call:  
rem  	yiic-domain  mylms.domain.com  
rem ----------------------------------------------------------------------------------

rem @setlocal

rem Set/export the following environment variable to the domain name (first parameter)
set LMS_DOMAIN=%1

set BIN_PATH=%~dp0

if "%PHP_COMMAND%" == "" set PHP_COMMAND=php.exe

rem Use the rest parameters to make the call to PHP script that will actually run the command (up to %9)
"%PHP_COMMAND%" "%BIN_PATH%yiic-domain.php" %2 %3 %4 %5 %6 %7 %8 %9 migrate --interactive=0
if %ERRORLEVEL% == 0 (
	rem Also apply plugin/app specific migrations, only if the main migrations succeeded
	"%PHP_COMMAND%" "%BIN_PATH%yiic-domain.php" %2 %3 %4 %5 %6 %7 %8 %9 migratePlugins
)


rem @endlocal