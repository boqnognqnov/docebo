@echo off

rem ------------------------------------------------------------------------------------------------------------------------
rem  Yii command starter.
rem  Differs from the default (yiic) in that it defines an
rem  environment variable equal to the FIRST script parameter that is
rem  used by the invoked PHP script to load per-domain configuration.
rem  Final result is: we can run Yii commands based on domain name and configuration.
rem  
rem  Example call:  
rem  	yiic-domain  mylms.domain.com  migrate 
rem     yiic-domain.sh   mylms.domain.com  someCommand someAction --param=value      (UNIX shell) 
rem     yiic-domain.bat  mylms.domain.com  someCommand someAction "--param=value"    (Windows only, note the DOUBLE quotes)
rem ------------------------------------------------------------------------------------------------------------------------

rem @setlocal

rem Set/export the following environment variable to the domain name (first parameter)
set LMS_DOMAIN=%1

set BIN_PATH=%~dp0

if "%PHP_COMMAND%" == "" set PHP_COMMAND=php.exe

rem Use the rest parameters to make the call to PHP script that will actually run the command (up to %9)
"%PHP_COMMAND%" "%BIN_PATH%yiic-domain.php" %2 %3 %4 %5 %6 %7 %8 %9


rem @endlocal