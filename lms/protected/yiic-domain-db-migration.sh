#!/bin/sh

# ----------------------------------------------------------------------------------
#  README !!!!!
#  This command differs from yiic-domain in that it adds "migrate --interactive=0" at the end of command parameters
#  and specifically meant to be used for applying Yii Database Migrations (NOT Release database migration, but intra-release DB fixes)
#  Do NOT use it for ANY other purposes!!! 
#  
#  Yii command starter.
#  Differs from the default (yiic) in that it defines an
#  environment variable equal to the FIRST script parameter that is
#  used by the invoked PHP script to load per-domain configuration.
#  Final result is: we can run Yii commands based on domain name and configuration.
#
#  Example call:  
#   yiic-domain.sh  mylms.domain.com
#  
#  Don't forget to change the file and make it executable:
#   chmod ugo+x yiic-domain-db-migration.sh
# ----------------------------------------------------------------------------------

# Set/export the following environment variable to the domain name (first parameter)
export LMS_DOMAIN=$1

# Use the rest parameters to make the call to PHP script that will actually run the command (up to 9)
php yiic-domain.php $2 $3 $4 $5 $6 $7 $8 $9 migrate --interactive=0
migration_res=$?
if [ $migration_res = 0 ]; then
	# Also apply plugin/app specific migrations, only if the main migrations succeeded
	php yiic-domain.php $2 $3 $4 $5 $6 $7 $8 $9 migratePlugins
	migration_res=$?
fi

# Clean domain specific garbage/cache
# Move 2 levels up (going in the LMS root)
echo "\nClean up...\n"
cd ../..

# Clean old files-based tmp folder
domain_code=`echo $1 | sed -e 's/\W/_/g'`
rm -f files/${domain_code}/tmp/*.bin

# Clean new lmstmp-based tmp folder 
split_domain_code=$(echo $domain_code|cut -c -1)/$(echo $domain_code|cut -c -2|cut -c 2-)/$domain_code
rm -f lmstmp/${split_domain_code}/tmp/*.bin

cd lms/protected

exit $migration_res