<?php

class WebinarBaseController  extends Controller {

	// ** Internal variables for course and session models (if specified in input parameters) **

	/**
	 * @var LearningCourse
	 */
	protected $_courseModel = NULL;

	/**
	 * @var WebinarSession
	 */
	protected $_sessionModel = NULL;



	/**
	 * Preloads course model (and session model)
	 * @param $action
	 * @return bool
	 * @throws CException
	 */
	public function beforeAction($action) {
		$rq = Yii::app()->request;
		$cs = Yii::app()->getClientScript();

		//first check if we are requesting an autocomplete action:
		//these actions have different parameters from standard requests
		$input = $rq->getParam('input', false);
		if (is_array($input) && !empty($input) && isset($input['query'])) {
			$idCourse = $input['course_id'];
			$idSession = $input['id_session'];
		} else {
			$idCourse = $rq->getParam('course_id', $rq->getParam('idCourse', false));
			$idSession = $rq->getParam('id_session', $rq->getParam('idSession', false));
		}

		// Preload course module
		if ($idCourse !== false) {
			$this->_courseModel = LearningCourse::model()->findByPk($idCourse);

			// Do some validation on course type
			if (!$this->_courseModel)
				throw new CHttpException('Invalid course ('.$idCourse.')');
			else if ($this->_courseModel->course_type != LearningCourse::TYPE_WEBINAR)
				throw new CHttpException('Selected course ('.$idCourse.') is not a webinar course!');

			if (!LearningCourseuser::isSubscribed(Yii::app()->user->id, $idCourse)) {
				// Is this a power user and the course was assigned to him?
				$pUserCourseAssigned = CoreUserPuCourse::model()->findByAttributes(array(
					'puser_id' => Yii::app()->user->id,
					'course_id' => $idCourse
				));

				if (Yii::app()->user->getIsAdmin() && !$pUserCourseAssigned)
					throw new CHttpException('Cannot access course "' . $idCourse . '"');
			}

			// Let "player" Yii component know about the course we are in
			// This will allow Main Menu to show "course related" tiles: "play" and "manage courses"
			Yii::app()->player->setCourse($this->_courseModel);
		}

		// Preload session model
		if ($idSession !== false && $idSession) {
			$this->_sessionModel = WebinarSession::model()->findByPk($idSession);

			// Do some validation on session info
			if (!$this->_sessionModel)
				throw new CHttpException('Invalid session ('.$idSession.')');
			else if ($idCourse !== false) {
				if ($this->_sessionModel->course_id != $idCourse)
					throw new CHttpException('Session course is different from passed course');
			}
		}

		// Load common scripts and css
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/webinar.css');

		//standard function
		return parent::beforeAction($action);
	}

	// Getter methods
	public function getIdCourse() { return (!empty($this->_courseModel) ? $this->_courseModel->getPrimaryKey() : NULL); }
	public function getCourseModel() { return (!empty($this->_courseModel) ? $this->_courseModel : NULL); }
	public function getIdSession() { return (!empty($this->_sessionModel) ? $this->_sessionModel->getPrimaryKey() : NULL); }
	public function getSessionModel() { return (!empty($this->_sessionModel) ? $this->_sessionModel : NULL); }
}