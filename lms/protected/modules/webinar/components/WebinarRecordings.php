<?php

class WebinarRecordings extends CComponent {

	public static $sessionId;
	public static $day;


	public static function convertWebinar($sessionId, $date, $file){
		$result = array(
			'status' => 'incomplete',
			'filename' => '',
			'error' => ''
		);
		try{
			$uploadTmpFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $file;
			if (!is_file($uploadTmpFile)) {
				throw new CException("File '" . $file . "' does not exist.");
			}

			$recordingModel = WebinarSessionDateRecording::model()->findByAttributes(array(
				'id_session' => $sessionId,
				'day' => $date
			));
			if(empty($recordingModel)){
				throw new CException("The selected session and/or date does not exist in the database.");
			}

			// Create storage Object, use it to determine the VIDEO whitelist type to use
			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_WEBINAR_RECORDING);
			$whitelistType = ($storage->type == CFileStorage::TYPE_S3) ? FileTypeValidator::TYPE_VIDEO_EXTENDED_LIST : FileTypeValidator::TYPE_VIDEO;

			/*** File extension type validate ************************/
			$fileValidator = new FileTypeValidator($whitelistType);
			if (!$fileValidator->validateLocalFile($uploadTmpFile))
			{
				FileHelper::removeFile($uploadTmpFile);
				throw new CException($fileValidator->errorMsg);
			}
			/*********************************************************/

			// Rename uploaded file
			$extension = strtolower(CFileHelper::getExtension($file));
			$newFileName = Docebo::randomHash() . "." . $extension;
			$newTmpFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $newFileName;
			if (!@rename($uploadTmpFile, $newTmpFile)) {
				throw new CException("Error renaming uploaded file.");
			}

			// Move uploaded file to final storage for later usage
			// The result from store() is FALSE OR the path (key for S3) to the file
			$destFilePath = $storage->store($newTmpFile);
			if (!$destFilePath) {
				throw new CException("Error while saving file to final storage.");
			}
			// store() MAY change format and extension of the file! Lets get them back
			$destFilename = pathinfo($destFilePath, PATHINFO_BASENAME);
			$destExtension = strtolower(CFileHelper::getExtension($destFilename));

			// Save the video information in learning_video table
			$hls_id = VideoConverter::HLS_ID;
			$hlsIdValue = preg_replace('/\\.[^.\\s]{3,4}$/', '', $destFilename);
			$videoFormats = new stdClass();
			$videoFormats->$destExtension = $destFilename;
			$videoFormats->$hls_id = $hlsIdValue;

			$recordingModel->path = CJSON::encode($videoFormats);
			if ($storage->transcodeVideo) {
				$recordingModel->status = WebinarSessionDateRecording::RECORDING_STATUS_CONVERTING; // NOTE: status will be updated to "completed" later in: lms/protected/controllers/AmazonSnsController.php
				$result['status'] = 'transcoding';
			} else {
				$recordingModel->status = WebinarSessionDateRecording::RECORDING_STATUS_COMPLETED;
				$result['status'] = 'success';
			}
			$recordingModel->save();

			$result['filename'] = $destFilename;

			// DELETE tmp file
			FileHelper::removeFile($newTmpFile);

		} catch(CException $e){

			$result['status'] = 'failed';
			$result['error'] = $e->getMessage();
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
		}

		return $result;
	}


	protected static $apiDownloadProgress = NULL;

	public static function downloadFileWithCurl($sessionId, $date, $importFile){
		try {
			self::$sessionId = $sessionId;
			self::$day = $date;

			$addFollowLocation = false;

			$fileInfo = pathinfo($importFile);
			$extension = (strpos($fileInfo['extension'], '?') > 0) ? substr($fileInfo['extension'], 0, strpos($fileInfo['extension'], '?')) : $fileInfo['extension'];
			$fileNameStartParams = strpos($fileInfo['filename'], '?');
			if($fileNameStartParams !== false){
				$fileNameTmp = substr($fileInfo['filename'], 0, $fileNameStartParams);
				$fileInfo2 = pathinfo($fileNameTmp);
				$fileName = $fileInfo2['filename'];
				$extension = $fileInfo2['extension'];
			} else {
				$fileName = $fileInfo['filename'];
			}

			//If there's no extension to be found, we're likely dealing with a redirect URL to the actual file, so we need to add an additional param to the curl call
			if(empty($extension)){
				$extension = 'mp4';
				$addFollowLocation = true;
			}

			$fileName .= "." . $extension;
			$localPath = Docebo::getUploadTmpPath();
			$fullPath = $localPath . DIRECTORY_SEPARATOR . $fileName;

			//start new progressing checker
			self::$apiDownloadProgress = 0.0;

			//do effective video file downloading from webinar service
			$fp = fopen($fullPath, 'w+');
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $importFile);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
			curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
			curl_setopt($ch, CURLOPT_PROGRESSFUNCTION, array(self, 'importProgress'));
			curl_setopt($ch, CURLOPT_NOPROGRESS, false);
			if($addFollowLocation) curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_FILE, $fp);
			curl_exec($ch);
			curl_close($ch);
			fclose($fp);

			//put progress back to its non-progressing status
			self::$apiDownloadProgress = NULL;

		} catch(CException $e){
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
		}
	}


	public static function importProgress(/*$resource, $download_size, $downloaded, $upload_size, $uploaded*/) {
		/*
		NOTE:
		for libcurl older than 7.32.0 the callback function need has the following format:
		 - curl_progress_callback($resource, $dltotal, $dlnow, $ultotal, $ulnow)
		where as newer curl binary does not require the resource:
		 - curl_progress_callback($dltotal, $dlnow, $ultotal, $ulnow)
		(you may check your curl version version with command: "curl -V")
		(see also http://php.net/manual/it/function.curl-version.php)
		*/
		if (func_num_args() > 4) {
			$download_size = func_get_arg(1);
			$downloaded = func_get_arg(2);
		} else {
			$download_size = func_get_arg(0);
			$downloaded = func_get_arg(1);
		}

		//NOTE: some sites may not include the http "content-length" header, so $download_size may always be 0 in that case
		if ($download_size <= 0) { self::$apiDownloadProgress = 0.0; }

		if ($download_size > 0) {

			if (self::$apiDownloadProgress === NULL) { self::$apiDownloadProgress = 0.0; } //make sure that the value is properly initialized
			$previousProgress = self::$apiDownloadProgress;
			$currentProgress = (($downloaded / $download_size) * 100);
			$diffProgress = ($currentProgress - $previousProgress);

			$minimumProgressRequired = 0.01; //we artificially set a limit on progress increasing size in order to avoid too many database accesses (this is true especially with big files)
			if ($diffProgress > $minimumProgressRequired || $currentProgress >= 100) {

				//update global progress value (it will be the next "$previousProgress" value next time this function is called)
				self::$apiDownloadProgress = $currentProgress;

				//update the progress info in the DataBase (this is being read by the client in separate http requests)
				/* @var $cmd CDbCommand */
				$cmd = Yii::app()->db->createCommand();
				$cmd->update(
					WebinarSessionDateRecording::model()->tableName(),
					array('progress' => $currentProgress),
					"id_session = :id_session AND day = :day AND status = :status",
					array(':id_session' => self::$sessionId, ':day' => self::$day, ':status' => WebinarSessionDateRecording::RECORDING_STATUS_DOWNLOADING)
				);
			}
		}
	}


	/**
	 * Make sure that some video URLs can be embedded and played in an iframe (e.g. youtube and vimeo).
	 * Url is converted in its "embedded version" if possible. If no embedded version is available or it is unknown then
	 * no transformation is applied and the URL is returned without any change.
	 * @param string $videoUrl the url of the video
	 * @return string
	 */
	public static function transformVideoUrlIntoEmbeddedVersion($videoUrl) {
		//TODO: may this list of url and replacements be moved to global parameters?
		//NOTE 1: this list mey be updated in the future
		//NOTE 2: this list is similar to the one in "common/extensions/tinymce/assets/plugins/media2/plugin.js" file
		$urlPatterns = array(
			array('regex' => '/youtu\.be\/([\w\-.]+)/', 'url' => 'https://www.youtube.com/embed/$1'),
			array('regex' => '/youtube\.com(.+)v=([^&]+)/', 'url' => 'https://www.youtube.com/embed/$2'),
			array('regex' => '/vimeo\.com\/([0-9]+)/', 'url' => 'https://player.vimeo.com/video/$1?title=0&byline=0&portrait=0&color=8dc7dc'),
			array('regex' => '/player\.vimeo\.com\/video\/([0-9]+)/', 'url' => 'https://player.vimeo.com/video/$1?title=0&byline=0&portrait=0&color=8dc7dc'),
			array('regex' => '/brainshark\.com\/([\w]+)\/vu\?pi=([0-9]+)/', 'url' => 'https://www.brainshark.com/$1/vu?pi=$2&dm=5&pause=1&nrs=1'),
		);

		//init output
		$output = $videoUrl;

		//check url patterns and do replacements if needed
		$found = false;
		$i = 0;
		while ($i < count($urlPatterns) && !$found) {
			if (preg_match($urlPatterns[$i]['regex'], $videoUrl, $matches)) {
				$found = true;
				$url = $urlPatterns[$i]['url'];
				for ($index = 0; $index < count($matches); $index++) {
					$url = str_replace('$'.$index, $matches[$index], $url);
				}
				$output = $url;
			}
			$i++;
		}

		return $output;
	}


}