<?php

class WebinarModule extends CWebModule
{
	private $_assetsUrl;
	private $_playerAssetsUrl;
	public function init()
	{
		$r = Yii::app()->request->getParam("r");
		if(strtolower($r) == 'webinar/training/webinarsession') {
			$cs = Yii::app()->getClientScript();
			$cs->registerScriptFile($this->getAssetsUrl().'/js/sessionForm.js');
		}
		// import the module-level models and components
		$this->setImport(array(
			'webinar.models.*',
			'webinar.components.*',
			'webinar.widgets.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}

	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('webinar.assets'));
		}
		return $this->_assetsUrl;
	}


	/**
	 * Get player module assets url path
	 * @return mixed
	 */
	public function getPlayerAssetsUrl() {
		if ($this->_playerAssetsUrl === null) {
			$this->_playerAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('player.assets'));
		}
		return $this->_playerAssetsUrl;
	}
}
