<script type='text/javascript'>
	$(function(){
		$.post('<?=Docebo::createLmsUrl('progress/run')?>', {
			'type': <?=Progress::JOBTYPE_ENROLL_USERS_TO_WEBINAR_SESSION?>,
			'users': '<?= $users ?>',
			'idCourse': '<?= $idCourse ?>',
			'idSession': '<?= $idSession ?>'
		}, function(data){
			var dialogId = 'enrooll-users-webinar-session-progressive';

			$('<div/>').dialog2({
				autoOpen: true, // We will open the dialog later
				id: dialogId,
				title: '<?=addslashes(Yii::t('classroom', 'Enroll to a session'))?>'
			});
			$('#'+dialogId).html(data);
		});
	})
</script>
