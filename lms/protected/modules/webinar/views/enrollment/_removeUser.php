<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'remove-enroll-form',
		'htmlOptions' => array(
			'class' => 'ajax',
		)
	)); ?>
	<input type="hidden" id="remove-enroll-confirm" name="confirm" value="1" />
	<p><?=Yii::t('classroom', 'You are unenrolling the selected user from this session')?>.<?php if ($showAutoEnrollMessage) echo "*";?></p>

	<div class="clearfix">
		<?php echo $form->checkbox($sessionUserModel, 'confirm', array('id' => 'LearningCourseuserSession_confirm', 'onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed'), 'LearningCourseuserSession_confirm'); ?>
	</div>

	<?php $this->endWidget(); ?>
<?php if ($showAutoEnrollMessage) echo  '<br><p><small>* ' . Yii::t('classroom', 'Remember, if the course has the "automatic enrollment of users from waiting list when a seat is freed" setting enabled, users from the waiting list automatically become enrolled, taking the seats of the unenrolled users.') . '</small></p>';?>
</div>
<div class="modal-footer">
	<input class="btn btn-submit disabled confirm-btn" type="button" value="<?= Yii::t('standard','_CONFIRM') ?>" name="submit" data-submit-form="remove-enroll-form">
	<input class="btn close-btn" type="button" value="<?= Yii::t('standard', '_CANCEL') ?>">
</div>