<ul id="docebo-bootcamp-session-enroll" class="clearfix">
	<li>

		<?php
		$this->renderPartial('_enrollUsersActionButton', array('courseModel' => $courseModel, 'sessionModel' => $sessionModel));
		?>

	</li>
	<li>
		<div>
			<?php
			echo CHtml::link('<span></span>'.Yii::t('classroom', 'Import users from session'), Docebo::createAdminUrl('webinar/enrollment/importUsersFromSession', array(
					'id_session' => $sessionModel->getPrimaryKey()
				)), array(
					'class' => 'popup-handler session-import-users',
					'data-modal-title' => Yii::t('classroom', 'Import users from session'),
					'data-modal-class' => 'import-users-from-session copy-course-enrollment',
					'data-dialog-class' => 'import-users-from-session-dialog',
					'alt' => Yii::t('helper', 'Import users from session - lt'),
					'data-after-send' => 'sessionEditEnrollmentAfterSubmit(data)',
				)
			);
			?>
	</li>
</ul>
<div class="info">
	<div>
		<h4></h4>
		<p></p>
	</div>
</div>