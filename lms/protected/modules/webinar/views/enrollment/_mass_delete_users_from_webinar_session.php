<script type='text/javascript'>
	$(function(){
        //Closing the previous dialog for confirmation to delete users
        $('.delete-node').modal('hide');

		$.post('<?=Docebo::createLmsUrl('progress/run')?>', {
			'type': '<?=Progress::JOBTYPE_DELETE_USERS_FROM_WEBINAR_SESSION?>',
			'users': '<?= $users ?>',
			'idSession': '<?= $idSession ?>',
		}, function(data){
			var dialogId = 'delete-users-webinar-session-progressive';

			$('<div/>').dialog2({
				autoOpen: true, // We will open the dialog later
				id: dialogId,
				title: '<?=Yii::t('standard', '_DEL_SELECTED')?>'
			});
			$('#'+dialogId).html(data);
		});
	})
</script>
