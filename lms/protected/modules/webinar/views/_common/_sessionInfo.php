<div class="session-details-container clearfix">
	<table>
		<tbody>
		<tr>
			<td class="title">
				<!-- title display -->
				<h3 class="title-bold"><?= $title ?></h3>
			</td>

			<td class="dates">
				<!-- session details -->
				<div class="classroom-dates-container">
					<ul>
						<li class="session-name"><?= $sessionModel->description; ?></li>
						<li class="session-from_to">
							<?php
							echo $sessionModel->date_begin.' | '.$sessionModel->getTotalHours("%hh:%Im").' | '.$sessionModel->getToolName();
							?>
						</li>
					</ul>
				</div>
			</td>
		</tr>
		</tbody>
	</table>
</div>