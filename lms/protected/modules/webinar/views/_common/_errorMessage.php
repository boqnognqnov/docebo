<?php
/**
 * Author: Simeon Ivanov, Celtis Ltd
 * Date: 16-Feb-15 11:12 AM
 * Used for showing error messages due to some unexpected exceptions in modals
 */
?>
<div class="alert alert-error">
  <?
  if (is_array($message))
  {
	  foreach ($message as $msg)
		  echo $msg.'<br>';
  }
  else
  	echo $message;

  ?>
</div>