<?php
/* @var $this SessionController */
/* @var $dataProvider CActiveDataProvider */
/* @var $course LearningCourse */

$userIsSubscribed = LearningCourseuser::isSubscribed(Yii::app()->user->id, $course->idCourse);
if(!$userIsSubscribed) {
    $this->breadcrumbs[Yii::t('menu', 'Admin')] = Docebo::createAppUrl('admin:dashboard/index');
    $this->breadcrumbs[Yii::t('standard', '_COURSES')] = Docebo::createAppUrl('admin:courseManagement/index');
} else {
    $this->breadcrumbs = array(
        Yii::t('menu_over', '_MYCOURSES') => Docebo::createLmsUrl('site/index'),
    );
}
$this->breadcrumbs[] = Docebo::ellipsis($course->name, 60, '...');
$this->breadcrumbs[] = Yii::t('classroom', 'Sessions and Enrollments');

$this->widget('lms.protected.modules.player.components.widgets.CourseHeader', array(
	'course' => $course
));

$fieldsDataProvider = CoreUserField::model()->findAll();

?>

<h3 class="title-bold back-button">
	<?php echo CHtml::link(Yii::t('standard', '_BACK'), Docebo::createLmsUrl('player/training/webinarSession', array('course_id'=>$course->idCourse))); ?>
	<span><?= Yii::t('classroom', 'Users waiting to be assigned') ?></span>
</h3>
<br/>

<form id="session-assign-search-form" data-grid="#session-assign-grid" class="ajax-grid-form" method="post">
	<div class="filters-wrapper">
		<table class="filters">
			<tbody>
			<tr>
				<td class="table-padding">
					<table class="table-border">
						<tr>
							<td class="group" style="">
								<table style="width:100%;">
									<tr>
										<td>
											<div class="text-center" style="margin-top: 9px;
											padding-left: 30px;
											display: inline-block;">
												<input type="checkbox" checked="checked" value="1" name="courseList" id="courseList" />
												<label for="courseList" style="display: inline-block; margin-left: 5px;"><?=Yii::t('course', 'Course waiting list')?></label>
											</div>
											<div class="text-center" style="margin-top: 9px;
											padding-left: 30px;
											display: inline-block;">
												<input type="checkbox" value="0" name="sessionList" id="sessionList"/>
												<label for="sessionList" style="display: inline-block; margin-left: 5px;"><?=Yii::t('course', 'Session waiting list')?></label>
											</div>
											<div class="input-wrapper pull-right">
												<?
												$filterableColumns = array(
													array(
														'selector'=>'.column-username',
														'label'=>Yii::t('standard', '_USERNAME'),
														'class'=>"default locked",
													),
													array(
														'selector'=>'.column-fullname',
														'label'=>Yii::t('standard', '_FULLNAME'),
														'class'=>"default locked",
													),
													array(
														'selector'=>'.column-date_inscr',
														'label'=>Yii::t('report', '_DATE_INSCR'),
														'class'=>"default locked",
													),
													array(
														'selector'=>'.column-time',
														'label'=>Yii::t('report', 'Time'),
														'class'=>"default locked",
													)
												);

												foreach($fieldsDataProvider as $coreField){
													$filterableColumns[] = array(
														'selector'=>'.column-additional-'.$coreField->id_field,
														'label'=>$coreField->translation,
													);
												}
												$this->widget('common.components.doceboFilterIcon.DoceboFilterIcon', array(
													'id'=>'waiting-users-course-'.$course->idCourse,
													'label'=>Yii::t('standard', '_CLICK_TO_SELECT_COLUMNS_TO_DISPLAY', array(6)),
													'list'=>$filterableColumns
												)); ?>
											</div>
											<div class="input-wrapper pull-right">
												<input  data-url="<?= Docebo::createAppUrl('lms:webinar/session/assign') ?>"
														data-type="core"
														class="typeahead"
														data-course_id="<?= $this->getIdCourse() ?>"
														id="advanced-search-session"
														autocomplete="off"
														type="text"
														name="searchText"
														placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"
														data-autocomplete="true" />
												<span class="search-icon"></span>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
</form>

<div class="selections clearfix">
	<?php
	//This is used, because GridViewSelectAll reset the pagination to null!!
	$dataProviderTmp = clone $dataProvider;
    $sessionList = Yii::app()->request->getParam('sessionList');
    $idUser = ($sessionList == 0) ? 'idUser' : null;
    $this->widget('adminExt.local.widgets.GridViewSelectAll', array(
        'gridId' => 'session-assign-grid',
        'class' => 'left-selections clearfix',
        'dataProvider' => $dataProviderTmp,
        'itemValue' => $idUser, //give the exact column to be used as PK for the selections because in this table it's composite from 3 columns
    ));
    ?>

	<?php if ($permissions['mod'] || ($permissions['del'])): ?>
		<div class="right-selections clearfix">
			<select name="massive_action" id="webinar-session-assign-massive-action" <?php

				// If user is PU and has not waiting list permission disable the dropdown
				if(!Yii::app()->user->getIsGodadmin() && !Yii::app()->user->checkAccess("/lms/admin/course/moderate")) {
					?>
					class="no-waiting-list" disabled="disabled"
				<?php } ?>
			?>>
				<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
				<?php if ($permissions['assign']): ?>
					<option value="assign"><?php echo Yii::t('classroom', 'Assign to a session'); ?></option>
				<?php endif; ?>
				<?php if ($permissions['del']): ?>
					<option value="delete"><?php echo Yii::t('standard', '_DEL'); ?></option>
				<?php endif; ?>
			</select>
			<label for="users-management-massive-action"><?php echo Yii::t('standard', '_ON_SELECTED'); ?></label>

			<a class="open-dialog hide" data-dialog-class="modal-session-assign-users" id="session-assign-massive-link"
				 href="<?= $this->createUrl('axAssignUsers', array('id_course'=>$this->getIdCourse(),'isBulkAction'=>1)) ?>"></a>
			<a class="hide" data-dialog-class="delete-node" id="session-delete-massive-link"
				 href="<?= $this->createUrl('axDeleteUnassigned', array('id_course'=>$this->getIdCourse())) ?>"></a>
		</div>
	<?php endif; ?>

</div>

<!--locations grid-->
<div id="grid-wrapper" class="">
	<?php

	$_columnList = array();

	// If user is PU and has not waiting list permission don't create checkboxes
	if(Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess("/lms/admin/course/moderate")) {
		$_columnList[] = array(
			'class' => 'CCheckBoxColumn',
			'selectableRows' => 2,
			'id' => 'session-assign-grid-checkboxes',
			'checked' =>
				function ($data) {
					if ($data instanceof LearningCourseuser) {
						return in_array($data->learningUser->idst, Yii::app()->session["selectedItems"]);
					} elseif ($data instanceof WebinarSessionUser) {
						return false;
					}
				},
			'value' => function ($data) {
				if ($data instanceof LearningCourseuser) {
					return $data->learningUser->idst;
				} elseif ($data instanceof WebinarSessionUser) {
					return $data->userModel->idst;
				}
			},
		);
	}

	$_columnList[] = array(
		'name' => 'username',
		'type' => 'raw',
		'value' =>function($data){
				if($data instanceof LearningCourseuser){
					return $data->learningUser->getClearUserId();
				}elseif ($data instanceof WebinarSessionUser){
					return $data->userModel->getClearUserId();
				}
			},
		'header' => Yii::t('standard', '_USERNAME'),
		'headerHtmlOptions'=>array(
			'class'=>'column-username',
		),
		'htmlOptions'=>array(
			'class'=>'column-username',
		),
	);

	$_columnList[] = array(
		'name' => 'fullname',
		'type' => 'raw',
		'value' =>function($data){
				if($data instanceof LearningCourseuser){
					return $data->learningUser->getFullName();
				}elseif ($data instanceof WebinarSessionUser){
					return $data->userModel->getFullName();
				}
			},
		'header' => Yii::t('standard', '_FULLNAME'),
		'htmlOptions'=>array(
			'class'=>'column-fullname',
		),
		'headerHtmlOptions'=>array(
			'class'=>'column-fullname',
		)
	);

	$_columnList[] = array(
		'name' => 'date_inscr',
		'type' => 'raw',
		'value' =>  function($data){
				if($data instanceof LearningCourseuser){
					return $data->date_inscr;
				}elseif ($data instanceof WebinarSessionUser){
					return $data->date_subscribed;
				}
			},
		'header' => Yii::t('report', '_DATE_INSCR'),
		'htmlOptions'=>array(
			'class'=>'column-date_inscr',
		),
		'headerHtmlOptions'=>array(
			'class'=>'column-date_inscr',
		)
	);

	$_columnList[] = array(
		'name'=>'time',
		'value'=>function($data,$index){
			if($data instanceof LearningCourseuser){
				$t = Yii::app()->localtime->fromLocalDateTime($data->date_inscr);
			}elseif ($data instanceof WebinarSessionUser){
				$t = Yii::app()->localtime->fromLocalDateTime($data->date_subscribed);
			}

				$d1 = new DateTime($t); // past timestamp
				$d2 = new DateTime(); // now

				$interval = $d1->diff($d2); /* @var $interval DateInterval */

				$daysSinceNow = $interval->days;
				$weeksSinceNow = round($daysSinceNow/7);
				$monthsSinceNow = ceil(($interval->y * 12) + $interval->m);

				if($daysSinceNow <= 14){
					echo Yii::t('report', '{days} days', array('{days}'=>$daysSinceNow));
				}elseif($weeksSinceNow <= 8){
					if($weeksSinceNow===1){
						echo Yii::t('report', '1 weeks');
					}else{
						echo Yii::t('report', '{weeks} weeks', array('{weeks}'=>$weeksSinceNow));
					}
				}else{
					if($monthsSinceNow==1){
						echo Yii::t('report', '1 month');
					}else{
						echo Yii::t('report', '{months} months', array('{months}'=>$monthsSinceNow));
					}
				}


		},
		'header'=>Yii::t('report', 'Time'),
		'headerHtmlOptions'=>array(
			'class'=>'column-time',
		),
		'htmlOptions'=>array(
			'class'=>'column-time',
		),
	);


	$_columnList[] = array(
		'name' => 'session',
		'type' => 'raw',
		'value' => function($data){
				if($data instanceof WebinarSessionUser){

					$sessionName = $data->sessionModel->name;

					if(strlen($data->sessionModel->name) > 30){
						$sessionName = substr($data->sessionModel->name, 0, 30) . '...';
					}

					return '<div calss="defaultAnchor">' . CHtml::link($sessionName, $this->createUrl('session/waiting', array(
						'course_id' => $this->getIdCourse(),
						'id_session' => $data->sessionModel->id_session
					)), array(
						'rel' => 'tooltip',
						'title' => $data->sessionModel->name,
						'style' => 'text-decoration: underline; color: #0000EE;',
						'target' => '_blank'
					)) . '</div>';

				}
			},
		'header' => Yii::t('classroom', 'Session'),
		'htmlOptions' => array(
			'class' => 'column-session-name',
		),
		'headerHtmlOptions' => array(
			'class' => 'column-session-name',
		)
	);

	$data = $fieldsDataProvider;
	// Add all additional fields as columns
	foreach($data as $coreField){
		/* @var CoreUserField $coreField */

		$_columnList[] = array(
			'htmlOptions'=>array(
				'class'=>'column-additional-'.$coreField->getFieldId(),
				'style'=>'display:none' // will be shown/hidden by the DoceboFilterIcon dynamically
			),
			'headerHtmlOptions'=>array(
				'class'=>'column-additional-'.$coreField->getFieldId(),
				'style'=>'display:none' // will be shown/hidden by the DoceboFilterIcon dynamically
			),

			'type' => 'raw',
			'header'=>$coreField->translation,
			'value' => function($courseUser) use ($coreField){
					/* @var $courseUser LearningCourseuser */
					if($courseUser instanceof LearningCourseuser){
						$coreUser = $courseUser->learningUser;
					}elseif ($courseUser instanceof WebinarSessionUser){
						$coreUser = $courseUser->userModel;
					}

					if(isset($coreUser) && !empty($coreUser)){
						$additionalFieldValuesForSingleUser = current(CoreUserField::getValuesForUsers([$coreUser->idst], true));

						if(isset($additionalFieldValuesForSingleUser[$coreField->getFieldId()])){
							$fieldValue = $additionalFieldValuesForSingleUser[$coreField->getFieldId()];
							switch ($coreField->getFieldType()) {
								case CoreUserField::TYPE_YESNO:
									$fieldValue = FieldYesno::getLabelFromValue($fieldValue);
									break;
								case CoreUserField::TYPE_COUNTRY:
									$fieldValue = FieldCountry::model()->renderFieldValue($fieldValue);
									break;
								case CoreUserField::TYPE_UPLOAD:
									$fieldValue = FieldUpload::model()->renderFieldValue($fieldValue);
									break;
								case CoreUserField::TYPE_DROPDOWN:
									$model = new FieldDropdown();
									$model->lang_code = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
									$model->id_field = $coreField->getFieldId();
									$fieldValue = $model->renderFieldValue($fieldValue);
									break;
								default: //no transformation operation needed in this case
									$fieldValue = $coreField->renderValue($fieldValue, $additionalFieldValuesForSingleUser);
									break;
							}

							echo $fieldValue;
						}
					}
			},
		);
	}

	if (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('enrollment/view')) {
		$_columnList[] = array(
			'type' => 'raw',
			'value' => function($data, $index){
					if($data instanceof LearningCourseuser){
						return $this->sessionAssignGridRenderAssign($data, $index);
					}
				},
			'cssClassExpression' => '"center-aligned"'
		);
	}


		$_columnList[] = array(
			'type' => 'raw',
			'value' => function($data, $index){
						if($data instanceof WebinarSessionUser){
							$displayButton =  SessionController::checkWaitingPermissions($data->id_session);
							if($displayButton){
								return $this->sessionWaitingGridRenderApprove($data, $index);
							}

						}
				},

			'htmlOptions' => array(
				'class' => 'button-column-single'
			),
		);


		$_columnList[] = array(
			'type' => 'raw',
			'value' => function($data, $index){
					if($data instanceof WebinarSessionUser){
						if($data instanceof WebinarSessionUser){
							$displayButton =  SessionController::checkWaitingPermissions($data->id_session);
							if($displayButton){
								return $this->sessionWaitingGridRenderReject($data, $index, true);
							}
						}
					}
				},
			'htmlOptions' => array(
				'class' => 'button-column-single'
			),
		);


	Yii::app()->event->raise('onGetWebinarWaitlistColumns', new DEvent($this, array(
		'columns'=>&$_columnList,
	)));

	$this->widget('DoceboCGridView', array(
		'id' => 'session-assign-grid',
		'htmlOptions' => array('class' => 'grid-view'),
		'dataProvider' => $dataProvider,
		'columns' => $_columnList,
		'template' => '{items}{summary}{pager}<br/>',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'ajaxType' => 'POST',
		'pager' => array(
			'class' => 'admin.protected.extensions.local.pagers.DoceboLinkPager',
		),
		'ajaxUpdate' => 'session-assign-grid-all-items',
		'beforeAjaxUpdate' => 'function(id, options) {
				// The Request type MUST be POST!
				options.type = "POST";
				options.ajaxType = "POST";
				options.data = {};

				// Searching options are very complex and we must handle this in a weird way...
				var $ajaxGridForm = $(".ajax-grid-form");
				$ajaxGridForm.find(":radio:visible:checked, :checkbox:visible, select:visible, input[type=\"text\"]:visible, input[type=\"textfield\"]:visible").each(function(){
					options.data[this.name] = $(this).val();
				});
				if ($("#advancedSearchVisibility").val() == 1) {
					options.data["advancedSearch[visible]"] = 1;
				}

				// To allow passing selected items over pagination
				options.data.selectedItems = $(\'#\'+id+\'-selected-items-list\').val();

			}',
		'afterAjaxUpdate' => 'function(id, data){toggleMassActions();  $("[rel=\'tooltip\']").tooltip();  enableActions(); $(document).controls(); $("#session-assign-grid :checkbox").styler(); afterGridViewUpdate(\'session-assign-grid\'); DoceboGridFilter.getInstance(\'waiting-users-course-'.$course->idCourse.'\').applySelectionsToUI(); }'
	)); ?>
</div>

<br/>
<br/>
<br/>

<script type="text/javascript">
	$(document).ready(function() {
		$(document).delegate(".modal-session-assign-users", "dialog2.content-update", function() {
			var e = $(this);
			var courseList = $("#courseList");
			var sessionList = $("#sessionList");
			var autoclose = e.find("a.auto-close");
			if (autoclose.length > 0) {
				e.find('.modal-body').dialog2("close");

				//update selection
				var unselect = autoclose.data("unselect");
				if (unselect) {
					removeFromGridSelection('session-assign-grid', (unselect + '').split(','));
				}

				// reload grid
				$("#session-assign-grid").yiiGridView('update', {
					data: {
						sessionList: sessionList.val(),
						courseList: courseList.val()
					}
				});





				var href = autoclose.attr('href');
				if (href) {
					window.location.href = href;
				}
			}
		});

		var $grid = $('#session-assign-grid');
		$("input, select").styler();
		$grid.find(':checkbox').styler({});
		// Checkbox filters
		var courseList = $("#courseList");
		var sessionList = $("#sessionList");
		var updateSessionWaitingGrid = function () {
			$.fn.yiiGridView.update($grid.attr('id'), {
				data: {
					sessionList: sessionList.val(),
					courseList: courseList.val()
				}
			});

		};


		courseList.on('change', function(e){
			e.preventDefault();
			if($(this).is(':checked')){
				$(this).val(1);
			}else{
				$(this).val(0);
			}
			updateSessionWaitingGrid();
		});

		sessionList.on('change', function(e){
			e.preventDefault();
			if($(this).is(':checked')){
				$(this).val(1);
			}else{
				$(this).val(0);
			}
			updateSessionWaitingGrid();
		});

		$('#webinar-session-assign-massive-action').live('change', function() {

			var selectedUsers = [];

			var selectedItems = $('#session-assign-grid-selected-items-list').val();
			if (selectedItems.length) {
				selectedUsers = selectedItems.split(',');
			}

			if (selectedUsers.length) {
				if ($(this).val() == 'delete') {
					//$('#session-delete-massive-link').click();
					var a = $('#session-delete-massive-link');
					var data = {selected_users: selectedItems};
					openDialog(false, 'test', a.attr('href'), 'session-delete-massive-dialog', a.data('dialog-class'), 'POST', data);
				} else {
					$('#session-assign-massive-link').click();
				}
			}

			// clear selection
			$(this).find('option:first').attr('selected', true);
		});

	});

	function enableActions()
	{
		var $grid = $('#session-assign-grid');
		var courseList = $("#courseList");
		var sessionList = $("#sessionList");
		var updateSessionWaitingGrid = function () {
			$.fn.yiiGridView.update($grid.attr('id'), {
				data: {
					sessionList: sessionList.val(),
					courseList: courseList.val()
				}
			});
		};

		$grid.find('.suspend-action').on('click', function (e) {
			e.preventDefault();
			$.post($(this).attr('href'), {}, function () {
				updateSessionWaitingGrid();
			}, 'json');
		});
		$grid.find('.delete-action').on('click', function (e) {
			e.preventDefault();
			$.post($(this).attr('href'), {}, function () {
				updateSessionWaitingGrid();
			}, 'json');
		});
	};

	function toggleMassActions(){
		if($("#sessionList").attr('checked')){
			$("#webinar-session-assign-massive-action").attr('disabled', 'disabled');
			$("#session-assign-grid").find(".checkbox-column").remove();
		}else{
			$("#webinar-session-assign-massive-action").attr('disabled', false);

		}
		// If user is PU and has not waiting list permission
		if($("#webinar-session-assign-massive-action").hasClass("no-waiting-list")) {
			$("#webinar-session-assign-massive-action").attr('disabled', 'disabled');
		}
	}
</script>