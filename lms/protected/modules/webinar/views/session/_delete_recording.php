<h1><?=Yii::t('webinar', 'Delete')?></h1>
<?=CHtml::beginForm('', 'POST', array(
	'class'=>'ajax',
))?>
<p style="padding-bottom: 10px;"><?=Yii::t('webinar', 'Delete your webinar video recording')?></p>
<?=CHtml::checkBox('chkConfirm', false)?> <label for="chkConfirm" style="display: inline-block;"><?=Yii::t('webinar', 'Yes, I want to proceed')?></label>
<div class="deleteRecordingButtons">
	<input class="btn-docebo green big confirm-btn" type="submit" name="confirm_write" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" style="opacity: 0.5;" disabled="disabled" />
	<input class="btn-docebo black big cancel-btn" type="reset" value="<?php echo Yii::t('standard', '_CLOSE'); ?>" />
</div>
<?=CHtml::endForm()?>

<script type="text/javascript">
	$('#chkConfirm').styler();

	$(function(){
		$('.modal-footer').css('display', 'none');
	});

	$('#chkConfirm').on('change', function(){
		if($(this).is(':checked')){
			$('.confirm-btn').css('opacity', '1').removeAttr('disabled');
		} else {
			$('.confirm-btn').css('opacity', '0.5').attr('disabled', 'disabled');
		}
	});

	$('.cancel-btn').on('click', function(){
		$('a.close').trigger('click');
	});
</script>
