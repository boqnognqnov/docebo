<h1><?= Yii::t('standard', '_MAKE_A_COPY') ?></h1>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'copy-session-form',
		'htmlOptions' => array(
			'class' => 'ajax',
		)
	)); ?>

	<input type="hidden" name="confirm" value="1" />
	<input type="hidden" name="course_id" value="<?=$idCourse?>" />
	<input type="hidden" name="id_session" value="<?=$idSession?>" />

	<div class="control-group">
		<div class="controls">
			<?= CHtml::label(Yii::t('webinar', 'Assign a name to this new session'), 'new-name-input'); ?>
			<?= CHtml::textField('new-name', '', array('id' => 'new-name-input')); ?>
		</div>
	</div>

	<table>
		<tbody>
<!--		<tr>-->
<!--			<td>--><?//= CHtml::label(Yii::t('classroom', 'How many copies?'), 'num-copies-input'); ?><!--</td>-->
<!--			<td>--><?php //$_numCopiesList = array();
//				for ($i=1; $i<=10; $i++)
//					$_numCopiesList[$i] = $i;
//				echo CHtml::dropDownList('num-copies', WebinarSession::REPEAT_DAILY, $_numCopiesList, array('id' => 'num-copies-input', 'style' => 'width: 60px;'));?>
<!--			</td>-->
<!--		</tr>-->
		<tr>
			<td><?= CHtml::label(Yii::t('classroom', 'Clone all session dates by'), 'repeats-input'); ?></td>
			<td>
				<?php
				echo CHtml::textField('shift', '', array( 'style' => 'width: 50px;margin: 0px 8px 0px 0px;'));

				$_periodList = array(
					WebinarSession::PERIOD_DAY => Yii::t('standard', 'day(s)'),
					WebinarSession::PERIOD_WEEK => Yii::t('standard', 'week(s)'),
					WebinarSession::PERIOD_MONTH => Yii::t('standard', 'month(s)'),
					WebinarSession::PERIOD_YEAR => Yii::t('standard', 'year(s)'),
				);
				echo CHtml::dropDownList('repeats', 0, $_periodList, array('id' => 'repeats-input', 'style' => 'width: 150px;text-transform: capitalize;'));
				?>
			</td>
<!--		</tr>-->
<!--		<tr>-->
<!--			<td>--><?//= CHtml::label(Yii::t('classroom', 'Repeat every'), 'num-repeats-input'); ?><!--</td>-->
<!--			<td>--><?php
//				$_numRepeatsList = array();
//				for ($i=1; $i<=10; $i++)
//					$_numRepeatsList[$i] = $i;
//				echo CHtml::dropDownList('num-repeats', 1, $_numRepeatsList, array('id' => 'num-repeats-input', 'style' => 'width: 60px;'));
//				echo '<span id="repeat-type-def"></span>'; ?>
<!--			</td>-->
<!--		</tr>-->
		</tbody>
	</table>

	<div class="form-actions">
		<?php echo CHtml::submitButton(Yii::t('standard','_CONFIRM'), array('class' => 'btn confirm-btn')); ?>
		<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>

<script type="text/javascript">
	$(document).delegate(".modal-copy-session", "dialog2.content-update", function() {
		var e = $(this), autoclose = e.find("a.auto-close");
		if (autoclose.length > 0) {
			try {
				e.find('.modal-body').dialog2("close");
			} catch(e) {}
			$.fn.yiiGridView.update('sessions-management-grid', {
				data: $("#session-grid-search-form").serialize()
			});
		}
	});
</script>