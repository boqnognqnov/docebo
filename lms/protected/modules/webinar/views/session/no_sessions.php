<div class="well classroom-sessions-grid-container">
	<div class="inner">
		<h3 class="title-bold"><?= Yii::t('classroom', 'Select session') ?></h3>
		<p><?= Yii::t('classroom', 'This course has no sessions available at this time. Please come back later.') ?></p>
	</div>
</div>