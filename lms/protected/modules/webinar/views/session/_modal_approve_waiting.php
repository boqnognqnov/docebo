<?php
/* @var $this SessionController */
/* @var $form CActiveForm */
/* @var $sessionId string */
?>

<h1><?= Yii::t('standard', '_CONFIRM') ?></h1>


<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'session-approve-users-form',
	'method' => 'POST',
	'htmlOptions' => array(
		'class' => 'ajax docebo-form',
		'style' => 'padding-left: 20px;'
	),
));

echo CHtml::hiddenField('id_session', $sessionId);
echo CHtml::hiddenField('selected_users', '');

?>

<p><?= Yii::t('classroom', 'Approve {count} user(s)?', array('{count}'=>CHtml::tag('strong', array('id'=>'count-placeholder'), ''))) ?></p>

<div>
	<label for="confirm" class="checkbox" style="margin-left: 0;padding-left: 0;">
		<span style="margin-right: 10px;"><input type="checkbox" id="confirm"/></span> <?= Yii::t('standard', 'Yes, I confirm I want to proceed') ?>
	</label>
</div>


<div class="form-actions">
	<input class="btn-docebo green big btn-submit disabled" type="submit" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" />
	<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">

	(function() {

		$(document).delegate(".modal-session-approve-waiting", "dialog2.content-update", function() {
			var e = $(this);

			var autoclose = e.find("a.auto-close");

			if (autoclose.length > 0) {
				e.find('.modal-body').dialog2("close");

				// reload grid
				$.fn.yiiGridView.update('session-waiting-grid', {});

				var href = autoclose.attr('href');
				if (href) {
					window.location.href = href;
				}
			}
			else {

				$('.modal :checkbox').styler({});

				var $form = e.find('form');

				var countUsers = 1;
				var selectedItems = $('#session-waiting-grid-selected-items-list').val();
				$form.find('#selected_users').val(selectedItems);
				if (selectedItems.length) {
					var selectedUsers = selectedItems.split(',');
					if (selectedUsers.length) {
						countUsers = selectedUsers.length;
						$form.find('#count-placeholder').html(countUsers);
					}
				}

				$('.modal :checkbox').change(function() {
					if ($(this).is(':checked')) {
						e.find('.btn-submit').removeClass('disabled');
					} else {
						e.find('.btn-submit').addClass('disabled');
					}
				});

				e.find('.btn-submit').click(function() {
					if ($(this).hasClass('disabled'))
						return false;
				});
			}
		});

	})();

</script>