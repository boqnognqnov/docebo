<?php

// Check if HLS is disabled by global Yii parameter
$disableHls = false;
if (isset(Yii::app()->params['disableHls'])) {
	$disableHls = Yii::app()->params['disableHls'];
}

$playerWidget = $this->widget('common.widgets.Flowplayer', array(
		'disableHls' => $disableHls,
		'mediaUrlMp4' => $mediaUrlMp4,
		'mediaUrlHlsPlaylist' => $mediaUrlHlsPlaylist,
		'title' => $dateModel->name,
		'skin' => 'functional',
		'bookmark' => false,
		'seekable' => true,
		'subtitlesUrls' => false
	)
);

$containerId = $playerWidget->containerId;
?>

<script type="text/javascript">
	/*<![CDATA[*/
	var keepAliveUrl = <?= json_encode(Docebo::createLmsUrl('webinar/session/recordingPlayerKeepAlive', array())) ?>;
	var progress<?= $playerWidget->id ?> = 0;

	$(document).ready(function () {

		var containerId = <?= json_encode($containerId) ?>;
		var periodOfBookmarkSaving = 30;

		// Start Flowplayer
		var fpObject = flowplayer($(containerId));

		//Added functionality to the "X"-close button, added by the LMS, to save the bookmark
		$('#inline-player-close').on('click', function (e) {
			//...
		});

		function keepalive() {
			var d = new Date();
			var time = d.getTime();
			$.ajax({
				url: keepAliveUrl,
				dataType: 'json'
			});
			setTimeout("keepalive()", 5 * 60 * 1000);
		}

		setTimeout("keepalive()", 5 * 60 * 1000);
	});

	/*]]>*/
</script>

