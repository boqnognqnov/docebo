<?php
/* @var $this SessionController */
/* @var $form CActiveForm */
/* @var $sessionId string */
?>

<h1><?= Yii::t('standard', '_CONFIRM') ?></h1>


<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'session-approve-users-form',
	'method' => 'POST',
	'htmlOptions' => array(
		'class' => 'ajax docebo-form'
	),
));

echo CHtml::hiddenField('id_session', $sessionId);
echo CHtml::hiddenField('selected_users', '');

?>

<p><?= Yii::t('classroom', 'Reject {count} user(s)?', array('{count}'=>CHtml::tag('strong', array('id'=>'count-placeholder'), ''))) ?></p>

<div>
	<label for="confirm" class="checkbox" style="margin-left: 0;">
		<span style="margin-right: 10px;"><input type="checkbox" id="confirm"/></span> <?= Yii::t('standard', 'Yes, I confirm I want to proceed') ?>
	</label>
</div>

<div id="lp-block">
	<strong><?= Yii::t('standard', '_WARNING') ?>:</strong>
	&nbsp;
	<?= Yii::t('curricula', '{n} selected user(s) have been assigned to a learning plan and cannot be unenrolled from course', array('{n}' => '<strong id="lp-count-placeholder"></strong>')) ?>
</div>

<div class="form-actions">
	<input class="btn-docebo green big btn-submit disabled" type="submit" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" />
	<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">

	(function() {

		$(document).delegate(".modal-session-approve-waiting", "dialog2.content-update", function() {
			var e = $(this);

			var autoclose = e.find("a.auto-close");

			if (autoclose.length > 0) {
				e.find('.modal-body').dialog2("close");

				// reload grid
				$.fn.yiiGridView.update('session-waiting-grid', {});

				var href = autoclose.attr('href');
				if (href) {
					window.location.href = href;
				}
			}
			else {

				$('.modal :checkbox').styler({});

				var $form = e.find('form');

				var countUsers = 1;
				var countLPUsers = 0;
				var selectedItems = $('#session-waiting-grid-selected-items-list').val();
				$form.find('#selected_users').val(selectedItems);
				if (selectedItems.length) {
					var selectedUsers = selectedItems.split(',');
					var lpUsers = <?=CJavaScript::encode($lpAssignments); ?>;
					var selectedUsersFiltered = [];

					if(lpUsers.length)
					{
						var i;
						var search;

						for(i = 0; i < selectedUsers.length; i++)
						{
							search = $.inArray(selectedUsers[i], lpUsers);
							if(search != -1)
								countLPUsers++;
							else
								selectedUsersFiltered.push(selectedUsers[i]);
						}

						selectedUsers = selectedUsersFiltered;
					}

					if(countLPUsers > 0)
					{
						$form.find('#selected_users').val(selectedUsers.toString());
						$form.find('#lp-count-placeholder').html(countLPUsers);
					}
					else
						$form.find('#lp-block').remove();

					countUsers = selectedUsers.length;
                    $form.find('#count-placeholder').html(countUsers);

					if(countUsers == 0)
					{
						e.find('.btn-submit').remove();
						e.find('.checkbox').remove();
					}
				}

				$('.modal :checkbox').change(function() {
					if ($(this).is(':checked')) {
						e.find('.btn-submit').removeClass('disabled');
					} else {
						e.find('.btn-submit').addClass('disabled');
					}
				});

				e.find('.btn-submit').click(function() {
					if ($(this).hasClass('disabled'))
						return false;
				});
			}
		});

	})();

</script>