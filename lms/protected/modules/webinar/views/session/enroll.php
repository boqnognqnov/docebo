<?php
/* @var $dataProvider CActiveDataProvider */
/* @var $form CActiveForm */
/* @var $courseName string */

$this->breadcrumbs[] = Docebo::ellipsis($courseName, 60, '...');

?>

<?php
$this->widget('lms.protected.modules.player.components.widgets.CourseHeader', array(
	'course_id' => Yii::app()->request->getQuery('course_id')
));
?>

<div class="classroom-sessions-grid-container">
	<div class="inner">
		<h3 class="title-bold"><?= Yii::t('classroom', 'Enroll to a session') ?></h3>
		<p><?= Yii::t('classroom', 'Select session') ?></p>

		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'classroom-sessions-grid-form',
			'method' => 'POST',
			'htmlOptions' => array(
				'class' => 'docebo-form'
			),
		)); ?>

		<div class="classroom-sessions-grid">

			<?php $this->widget('DoceboCListView', array(
				'id' => 'classroom-sessions-list',
				'htmlOptions' => array('class' => 'docebo-list-view'),
				'template' => '{header}{items}{pager}',
				'pager' => array(
					'class' => 'ext.local.pagers.DoceboLinkPager',
				),
				'afterAjaxUpdate'	=> "function(id,data) {
					$('.classroom-sessions-grid-container :radio').styler();
            		$('a[rel=\"tooltip\"]').tooltip();
				}",

				'dataProvider' => $dataProvider,
				'itemView' => '_enrollListView',
				'columns' => array(
					array(
						'name' => 'id',
					),
					array(
						'name' => 'name',
						'header' => Yii::t('standard', '_NAME')
					),
					array(
						'name' => 'start',
						'value' => function($data){
							/* @var $data WebinarSession */
							return WebinarSession::getStartDate($data->id_session);
						},
						'header' => Yii::t('standard', '_START')
					),
					array(
						'name' => 'duration',
						'header' => Yii::t('statistic', '_TIME_IN')
					),
					array(
						'name' => 'tool',
						'header' => Yii::t('webinar', 'Tool')
					),
				)
			)); ?>

		</div>

		<div class="clearfix"></div>
		<div class="row-fluid">
			<div class="span12 text-right">
				<br>
				<?php if($dataProvider->totalItemCount > 0):?>
					<input class="btn-docebo green big" type="submit" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" />
				<?php endif;?>
				<a href="<?= Yii::app()->createUrl('site/index') ?>" class="btn-docebo black big"><?php echo Yii::t('standard', '_CANCEL'); ?></a>
			</div>
		</div>

		<?php $this->endWidget(); ?>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		var $sessionList = $('#classroom-sessions-list');

		// css class name for a selected list row
		var cssItemSelected = 'selected';
		$sessionList.find('.col-id input').eq(0).attr('checked', true);
		$sessionList.find(':radio').styler();

		// ??
		$(document).on('click', '.col-name', function(){
			$(this).closest('.item').find('.col-id input').attr('checked', true);
		});


	});
</script>