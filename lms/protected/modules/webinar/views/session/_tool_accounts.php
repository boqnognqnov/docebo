<?php
/* @var $model WebinarSession */
/* @var $tool string */
$toolName = isset($tool) && $tool ? $tool : null;
echo CHtml::activeLabelEx($model, 'id_tool_account');

$listData = CHtml::listData(WebinarSession::accountListByTool($toolName), 'id_account', 'name');
$disabled = WebinarSession::filterOutAccountsWithOverlappingSessions($model,$listData );
if(!$model->id_session)
	echo CHtml::dropDownList('webinar_id_tool_account', null, $listData, array('options' => $disabled ,'class' => 'webinar-dropdown-full', 'id' => 'toolDropdown', 'prompt' => Yii::t('webinar', 'Select account')));
else {
	echo CHtml::dropDownList('webinar_id_tool_account', $model->id_tool_account, $listData, array('options' => $disabled, 'disabled' => 'disabled', 'class' => 'webinar-dropdown-full', 'id' => 'toolDropdown', 'prompt' => Yii::t('webinar', 'Select account')));
	echo CHtml::hiddenField('webinar_id_tool_account' );
}

// Render custom tool settings (if any)
if($toolName) {
	$toolModel = WebinarTool::getById($toolName);
	if($toolModel)
		echo $toolModel->getCustomSettingsHTML(is_array($model->tool_params) ? $model->tool_params : CJSON::decode($model->tool_params));
}