<?php
/* @var $this SessionController */
/* @var $form CActiveForm */
/* @var $course LearningCourse */
?>

<h1>
	<?= Yii::t('classroom', 'Assign waiting user(s) to a session') ?>
</h1>

<?php
$this->widget('common.widgets.WebinarSessionGrid', array(
	'course'=>$course,
	'title'=>Yii::t('classroom', 'Select session'),
	'puFilter' => (Yii::app()->user->getIsPu() && !Yii::app()->user->checkAccess("/lms/admin/webinarsessions/assign"))
	//'enablePagination' => true,
));
?>



<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'session-assign-users-form',
	'method' => 'POST',
	'htmlOptions' => array(
		'class' => 'ajax docebo-form'
	),
));

echo CHtml::hiddenField('selected_users', '');
echo CHtml::hiddenField('id_session', '');

?>

<div class="form-actions">
	<input class="btn-docebo green big btn-assign" type="submit" value="<?php echo Yii::t('standard', '_ASSIGN'); ?>" />
	<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>

<?php $this->endWidget(); ?>


<script type="text/javascript">

	(function() {

		$(document).delegate(".modal-session-assign-users", "dialog2.content-update", function() {
			var e = $(this);

			var autoclose = e.find("a.auto-close");

			if (!(autoclose.length > 0)) {

				$('<div/>', {class:'info'}).prependTo('.modal-footer');

				var $form = $('#session-assign-users-form');

				var countUsers = 1;
				<?php if ($isBulkAction) : ?>
				var selectedItems = $('#session-assign-grid-selected-items-list').val();
				$form.find('#selected_users').val(selectedItems);
				if (selectedItems.length) {
					var selectedUsers = selectedItems.split(',');
					if (selectedUsers.length)
						countUsers = selectedUsers.length;
				}
				<?php endif; ?>

				// replace placeholders
				$(document).on('change', '.modal.modal-session-assign-users input[type="radio"]', function() {
					//$('.modal :radio').change(function() {

					var $that = $(this);

					$form.find('#id_session').val($that.val());

					var session = $that.closest('tr').find('td').eq(1).text();

					var infoText = '<?= addslashes(Yii::t('classroom', 'You are about to assign <strong>{count} user(s)</strong> to session <strong>{session}</strong> of <strong>{course}</strong> course')) ?>';
					infoText = infoText.replace('{count}', countUsers);
					infoText = infoText.replace('{course}', '<?= addslashes($course->name) ?>');
					infoText = infoText.replace('{session}', session);

					$('.modal-footer .info').html(infoText);
				});

				$('span#<?=$course->idCourse?>-id_session-styler:first').click();
				if($('.modal.modal-session-assign-users input[type="radio"]').length == 0) {
					$('.modal.modal-session-assign-users .btn-assign').hide();
				}

			}
		});

	})();

</script>