<h1><?= Yii::t('classroom', 'Delete session') ?></h1>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'delete-session-form',
		'htmlOptions' => array(
			'class' => 'ajax',
		)
	)); ?>
	<input type="hidden" name="confirm" value="1" />
	<p><?=Yii::t('classroom', 'You are deleting session "{name}". Can you confirm that?', array('{name}' => $model->name))?></p>
	<div class="form-actions">
		<?php echo CHtml::submitButton(Yii::t('standard','_CONFIRM'), array('class' => 'btn confirm-btn notification-submit')); ?>
		<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
	$(document).delegate(".modal-delete-session", "dialog2.content-update", function() {
		var e = $(this), autoclose = e.find("a.auto-close");
		if (autoclose.length > 0) {
			try {
				e.find('.modal-body').dialog2("close");
			} catch(e) {}
			$.fn.yiiGridView.update('sessions-management-grid', {
				data: $("#session-grid-search-form").serialize()
			});
		}
	});
</script>