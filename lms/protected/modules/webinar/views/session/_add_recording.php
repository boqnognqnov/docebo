<?php
if (!isset($recording_mode)) {
	$recording_mode = ($showApiGrabOption
		? WebinarSessionDateRecording::RECORDING_TYPE_API_GRAB
		: ($showApiLinkOption
			? WebinarSessionDateRecording::RECORDING_TYPE_API_LINK
			: WebinarSessionDateRecording::RECORDING_TYPE_UPLOAD));
}
$recording_api_grab = ($recording_mode == WebinarSessionDateRecording::RECORDING_TYPE_API_GRAB)?true:false;
$recording_upload = ($recording_mode == WebinarSessionDateRecording::RECORDING_TYPE_UPLOAD)?true:false;
$recording_weblink = ($recording_mode == WebinarSessionDateRecording::RECORDING_TYPE_WEBLINK)?true:false;
$recording_api_link = ($recording_mode == WebinarSessionDateRecording::RECORDING_TYPE_API_LINK)?true:false;

$_storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_WEBINAR_RECORDING);
$_whitelistType = ($_storage->type == CFileStorage::TYPE_S3) ? FileTypeValidator::TYPE_VIDEO_EXTENDED_LIST : FileTypeValidator::TYPE_VIDEO;

?>

<h1><?=Yii::t('webinar', 'Add webinar recording')?></h1>
<?=CHtml::beginForm('', 'POST', array(
	'id' => 'add-recording-form',
	'class'=>'ajax',
))?>

<div class="webinarSpinner dialog-content-spinner" style="display:none;">
	<span><?=Yii::t('standard', 'Uploading')?> ...</span>
</div>

<div class="dialog-content-main">

<?php if(!empty($errors)){ foreach ($errors as $error){ ?>
<div class="row-fluid">
    <div class="span12">
        <div class="errorSummary"><?=$error?></div>
    </div>
</div>
<?php } } ?>
<div class="row-fluid" id="importError" style="display:none;">
    <div class="span12">
        <div class="errorSummary"></div>
    </div>
</div>

<?php if($showApiGrabOption){ ?>
<div class="addRecordingBlock">
    <?=CHtml::radioButton('recording_mode', $recording_api_grab, array(
        'id' => 'api_grab',
        'value' => WebinarSessionDateRecording::RECORDING_TYPE_API_GRAB
    ))?><label for="api_grab"><?=Yii::t('webinar', 'Use {webinartool} recording', array('{webinartool}' => $webinarToolName))?></label>
    <div class="addRecordingInlineBlockContent api_grab_content" style="display:<?=($recording_api_grab)?"inline-block":"none"?>;">
        <div class="import-button">
            <a href="#" id="api_grab_recording" class="btn-docebo green big"><?= Yii::t('standard', '_IMPORT') ?></a>
        </div>
    </div>
    <div class="addRecordingBlockContent api_grab_content"<?=($recording_api_grab)?"":" style='display:none;'"?>>
        <div class="docebo-progress progress progress-striped active" id="api_grab_progress" style="width: 593px; float: right; display: none;">
            <div id="api_grab-progress-bar" class="bar full-height" style="width: 0%;"></div>
        </div>
    </div>
</div>
<?php } ?>

<?php if($showApiLinkOption){ ?>
    <div class="addRecordingBlock">
        <?=CHtml::radioButton('recording_mode', $recording_api_link, array(
            'id' => 'api_link',
            'value' => WebinarSessionDateRecording::RECORDING_TYPE_API_LINK
        ))?><label for="api_link"><?=Yii::t('webinar', 'Use {webinartool} recording', array('{webinartool}' => $webinarToolName))?></label>
    </div>
<?php } ?>

<div class="addRecordingBlock">
    <?=CHtml::radioButton('recording_mode', $recording_upload, array(
        'id' => 'upload',
        'value' => WebinarSessionDateRecording::RECORDING_TYPE_UPLOAD
    ))?><label for="upload"><?=Yii::t('webinar', 'Upload custom video recording')?></label>
    <div class="addRecordingInlineBlockContent upload_content" style="display:<?=($recording_upload)?"inline-block":"none"?>;">
        <div class="upload-button">
            <a href="#" id="upload_recording" class="btn-docebo green big"><?= Yii::t('organization', 'Upload file') ?></a>
        </div>
    </div>
    <span id="upload_file_name"></span>
    <div class="addRecordingBlockContent upload_content"<?=($recording_upload)?"":" style='display:none;'"?>>
        <div class="docebo-progress progress progress-striped active" id="upload_progress" style="width: 593px; float: right; display: none;">
            <div id="uploader-progress-bar" class="bar full-height" style="width: 0%;"></div>
        </div>
    </div>
</div>

<div class="addRecordingBlock">
    <?=CHtml::radioButton('recording_mode', $recording_weblink, array(
        'id' => 'weblink',
        'value' => WebinarSessionDateRecording::RECORDING_TYPE_WEBLINK
    ))?><label for="weblink"><?=Yii::t('webinar', 'Import video recording from an external link')?></label>
    <div class="addRecordingBlockContent weblink_content"<?=($recording_weblink)?"":" style='display:none;'"?>>
        <input type="text" name="weblink_address" id="weblink_address" value="<?=(!empty($weblink_address))?$weblink_address:"http://"?>" />
			<div class="warning-strip warning">
				<div class="exclamation-mark">
					<i class="fa fa-exclamation-circle"></i>
				</div>
				<div class="warning-text">
					<?= Yii::t('webinar', 'In order to use this functionality, video service provider must be whitelisted in the Advanced Settings') ?>
				</div>
			</div>
    </div>
</div>

<div class="form-actions">
    <input class="btn-docebo green big confirm-btn" type="submit" name="confirm_write" value="<?php echo Yii::t('standard', '_NEXT'); ?>" />
    <input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>" />
</div>

</div>

<?=CHtml::endForm()?>

<script type="text/javascript">
    $(function(){
        $('input[type="radio"]').styler();

        $('#weblink_address').on('focus', function(){
            var textContent = $(this).val();
            if(textContent == 'http://'){
                $(this).val('');
            }
        }).on('blur', function(){
            var textContent = $(this).val();
            if(textContent == ''){
                $(this).val('http://');
            }
        });

        $('input[type=radio]').on('change', function(){
            var activeRadio = $(this).attr('id');
            $('.addRecordingBlockContent, .addRecordingInlineBlockContent').css('display', 'none');
            $('.addRecordingBlockContent.'+activeRadio+'_content').css('display', 'block')
            $('.addRecordingInlineBlockContent.'+activeRadio+'_content').css('display', 'inline-block');
        });

        // Upload a file
        var pl_uploader = new plupload.Uploader({
            chunk_size:       '1mb',
            unique_names:     false,
            multi_selection:  false,
            runtimes:         'html5,flash',
            browse_button:    'upload_recording',
            max_file_size:    '800mb',
            url:              '<?=Docebo::createAppUrl('lms:site/axUploadFile')?>',
            multipart:        true,
            multipart_params: {
                YII_CSRF_TOKEN: '<?= Yii::app()->request->csrfToken ?>'
            },
            filters: [
                {title : "Video files", extensions : "<?= implode(',', FileTypeValidator::getWhiteListArray($_whitelistType)) ?>"}
            ],
            flash_swf_url : 'http://rawgithub.com/moxiecode/moxie/master/bin/flash/Moxie.cdn.swf',
            silverlight_xap_url : 'http://rawgithub.com/moxiecode/moxie/master/bin/silverlight/Moxie.cdn.xap'
        });


        // Init event hander
        pl_uploader.bind('Init', function (up, params) {
        });

        // Run uploader
        pl_uploader.init();


        // Just before uploading file
        pl_uploader.bind('BeforeUpload', function (up, file) {
            $('#upload_progress').css('display', 'block');
        });


        // Handle 'files added' event
        pl_uploader.bind('FilesAdded', function (up, files) {
            this.files = [files[0]];
            $('.confirm-btn').attr('disabled', 'disabled').css('opacity', '0.5');
            pl_uploader.start();
        });


        // Upload progress
        pl_uploader.bind('UploadProgress', function (up, file) {
            $('#uploader-progress-bar').css('width', file.percent + '%');
        });

        // On error
        pl_uploader.bind('Error', function (up, error) {
            console.log('something shitty just happened');
            //$('#player-save-uploaded-lo').removeClass("disabled");
            //Docebo.Feedback.show('error', 'Failed to upload a file: ' + error.message + ' (' + error.status + ')');
        });

        // On Upload complete
        pl_uploader.bind('UploadComplete', function (up, files) {
            // Add NAME of the uploaded file to the FORM.
            // Server knows where to find it (upload_tmp, usually)
            var theFile = up.files[0];
            $('#upload_file_name').text(theFile.name);
            $('#upload_progress').addClass('progress-success').removeClass('active, progress-striped');
            $('.confirm-btn').removeAttr('disabled').css('opacity', '1');
            var form = $('#upload_recording').closest('form');
            var fileNameFormField = $('<input type="hidden" name="FileUpload" value="'+theFile.name+'">');
            form.append(fileNameFormField);
        });

        // Handle api_grab import
        $('#api_grab_recording').on('click', function(){
            $('.confirm-btn').attr('disabled', 'disabled').css('opacity', '0.5');
            ApiGrabRecording();
            return false;
        });

			$(document).on('dialog2.before-send', '.addRecordingModal', function(e, xhr, settings) {
				var showUploading = false;
				<?php if ($showApiGrabOption) { ?>
				if ($('#api-grab').prop('checked')) { showUploading = true; }
				<?php } ?>

				<?php if ($showApiLinkOption) { ?>
				if ($('#api-link').prop('checked')) { showUploading = true; }
				<?php } ?>

				if ($('#upload').prop('checked')) { showUploading = true; }

				if (showUploading) {
					$('.addRecordingModal span.loader').css('display', 'none');
					$('.addRecordingModal div.dialog-content-main').css('display', 'none');
					$('.addRecordingModal div.dialog-content-spinner').css('display', 'block');
				}
			});
    });

    // Handle api_grab import
    function ApiGrabRecording(){
        $.ajax({
            dataType: 'json',
            url: "<?php echo Yii::app()->createUrl('webinar/session/axApiGrabRecording'); ?>",
            type: "POST",
            data: {
                sessionId : '<?=$session_id?>',
                day : '<?=$day?>'
            },
            success: function(data){
                if(data.data.status == 'success'){
                    $('#api_grab_progress').css('display', 'block');
                    $('#api_grab-progress-bar').css('width', '100%');
                    $('#api_grab_progress').addClass('progress-success').removeClass('active, progress-striped');
                    $('.confirm-btn').removeAttr('disabled').css('opacity', '1');
                } else if(data.data.status == 'failed') {
                    $('#api_grab_progress').css('display', 'none');
                    $('#importError').css('display', 'block');
                    $('.confirm-btn').removeAttr('disabled').css('opacity', '1');
                    $('#importError .errorSummary').text(data.data.error);
                } else {
                    $('#api_grab_progress').css('display', 'block');
                    $('#api_grab-progress-bar').css('width', data.data.progress + '%');
                    setTimeout(function(){
                        ApiGrabRecording();
                    }, 400);
                }
            }
        });
    }
</script>
