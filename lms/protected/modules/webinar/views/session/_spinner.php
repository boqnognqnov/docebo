<h1>
	<?= Yii::t('webinar', 'Add webinar recording') ?>
</h1>

<?php if(!empty($errors)){ foreach ($errors as $error){ ?>
	<div class="row-fluid">
		<div class="span12">
			<div class="errorSummary"><?=$error?></div>
		</div>
	</div>
<?php } } ?>

<div class="webinarSpinner" style="display:<?=($finished)?'none':'block'?>">
	<span><?=Yii::t('webinar', 'Importing and converting your webinar recording...')?></span>
</div>
<div class="webinarRecordingComplete" style="display:<?=($finished)?'block':'none'?>">
	<span class="tick"<?=(!empty($errors))?' style="visibility:hidden;"':''?>>&#xf00c;</span>
	<span class="text"<?=(!empty($errors))?' style="visibility:hidden;"':''?>><?=Yii::t('webinar', 'Successfully converted')?></span>
	<span class="link">
		<a href="#" class="btn btn-docebo green big addRecordingModal-finishButton"><?=Yii::t('iotask', '_FINISH')?></a>
	</span>
</div>

<script type="text/javascript">

	function updateRecordingButtonsAfterConversion() {
		//create new "View recording" stuff
		var day = '<?= $date ?>';
		var t = '<span class="video-recording-download recording-actions">'
			+'<a class="btn btn-docebo green" href="javascript:;" onclick="(function() { viewRecording(<?= $id_session ?>, \''+day+'\'); })()">'
			+<?= CJSON::encode(Yii::t('webinar', 'View recording')) ?>
			+'</a>'
			+'</span>';
		<?php if ($canEditCourse): ?>
		var url = '<?= Docebo::createLmsUrl('webinar/session/deleteRecording', array('session_id' => $id_session, 'date' => '{day}')) ?>'
			.replace('<?= urlencode('{day}') ?>', day)
			.replace('{day}', day);
		t += '<a href="'+url+'" class="deleteRecording open-dialog recording-actions" data-dialog-class="deleteRecordingModal"><span></span></a>';
		<?php endif; ?>
		//remove the dummy blue button and put the new stuff in its place
		var container = $('#timers_<?= $id_session ?>_'+day).find('.pull-right');
		if (container.length > 0) {
			container.find('.recording-actions').remove();
			container.append($(t));
			container.controls();
		}
	}


	function checkStatus(){
		$.ajax({
			dataType: 'json',
			url: "<?php echo Yii::app()->createUrl('webinar/session/axCheckRecordingStatus'); ?>",
			type: "POST",
			data: {
				session_id : '<?=$id_session?>',
				date : '<?=$date?>'
			},
			success: function(data){
				if(data.data.status == 'success'){
					$('.webinarSpinner').css('display', 'none');
					$('.webinarRecordingComplete').css('display', 'block');
					updateRecordingButtonsAfterConversion();
				} else if(data.data.status == 'failed') {
					$('.webinarSpinner').css('display', 'none');
					$('.webinarRecordingComplete').css('display', 'block');
				} else {
					setTimeout(function(){checkStatus();}, 1000);
				}
			}
		});
	}

	$(function(){
		checkStatus();
	});

	$('a.btn-docebo.addRecordingModal-finishButton').on('click', function(){
		$('a.close').trigger('click');
		return false;
	});
</script>
