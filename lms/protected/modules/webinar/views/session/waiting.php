<?php
/* @var $this SessionController */
/* @var $dataProvider CActiveDataProvider */
/* @var $course LearningCourse */
/* @var $session WebinarSession */

$this->breadcrumbs[] = Docebo::ellipsis($course->name, 60, '...');
$this->breadcrumbs[Yii::t('classroom', 'Sessions and Enrollments')] = Docebo::createAppUrl('lms:player/training/webinarSession', array('course_id'=>$course->getPrimaryKey()));
$this->breadcrumbs[] = Yii::t('course', '_ALLOW_OVERBOOKING');

$this->widget('lms.protected.modules.player.components.widgets.CourseHeader', array(
	'course' => $course
));

?>

<h3 class="title-bold back-button">
	<?php echo CHtml::link(Yii::t('standard', '_BACK'), Docebo::createAppUrl('lms:player/training/webinarSession', array('course_id'=>$course->idCourse))); ?>
	<span><?= $session->name ?></span>
</h3>
<br/>

<form id="session-waiting-search-form" data-grid="#session-waiting-grid" class="ajax-grid-form" method="post">
	<div class="filters-wrapper">
		<table class="filters">
			<tbody>
			<tr>
				<td class="table-padding">
					<table class="table-border">
						<tr>
							<td class="group" style="">
								<table style="width:100%;">
									<tr>
										<td>
											<div class="input-wrapper pull-right">
												<input  data-url="<?= Docebo::createAppUrl('lms:webinar/session/waiting') ?>"
														data-type="core"
														class="typeahead"
														data-course_id="<?= $this->getIdCourse() ?>"
														id="advanced-search-session"
														autocomplete="off"
														type="text"
														name="searchText"
														placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"
														data-autocomplete="true" />
												<span class="search-icon"></span>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
</form>

<div class="selections filter-border clearfix" style="padding-bottom: 20px;">
	<?php
	$selectAllDataProvider = clone $dataProvider;
	$this->widget('ext.local.widgets.GridViewSelectAll', array(
		'gridId' => 'session-waiting-grid',
		'class' => 'left-selections clearfix',
		'itemValue' => '$data["id_user"]',
		'dataProvider' => $selectAllDataProvider,
	));
	?>

	<?php if ($permissions['assign'] || ($permissions['del'])): ?>
		<div class="right-selections clearfix">
			<select name="massive_action" id="session-waiting-massive-action">
				<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
				<?php if ($permissions['assign']): ?>
					<option value="approve"><?php echo Yii::t('standard', '_APPROVE'); ?></option>
				<?php endif; ?>
				<?php if ($permissions['del']): ?>
					<option value="reject"><?php echo Yii::t('standard', '_DENY'); ?></option>
				<?php endif; ?>
			</select>
			<label for="users-management-massive-action"><?php echo Yii::t('standard', '_ON_SELECTED'); ?></label>

			<a class="open-dialog hide" data-dialog-class="delete-node modal-session-approve-waiting" id="session-approve-massive-link" href="<?= $this->createUrl('axApprove', array('id_course'=>$this->getIdCourse(),'id_session'=>$session->id_session,'isBulkAction'=>1)) ?>"></a>
			<a class="open-dialog hide" data-dialog-class="delete-node modal-session-approve-waiting" id="session-reject-massive-link" href="<?= $this->createUrl('axReject', array('id_course'=>$this->getIdCourse(),'id_session'=>$session->id_session,'isBulkAction'=>1)) ?>"></a>
		</div>
	<?php endif; ?>

</div>

<?php
$this->renderPartial('/_common/_sessionInfo', array(
	'title' => Yii::t('course', '_ALLOW_OVERBOOKING'),
	'sessionModel' => $session
));
?>

<!-- waiting users grid-->
<div id="grid-wrapper session-list-view" class="">
	<?php

	$_columnList = array();

	// If user is PU and has not waiting list permission don't create checkboxes
	if(Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess("/lms/admin/course/moderate")) {
		$_columnList[] = array(
			'class' => 'CCheckBoxColumn',
			'selectableRows' => 2,
			'id' => 'session-waiting-grid-checkboxes',
			'checked' => 'in_array($data->userModel->idst, Yii::app()->session["selectedItems"])',
			'value' => '$data->userModel->idst'
		);
	}

	$_columnList[] = array(
		'name' => 'username',
		'type' => 'raw',
		'value' => '$data->userModel->getClearUserId()',
		'header' => Yii::t('standard', '_USERNAME')
	);

	$_columnList[] = array(
		'name' => 'fullname',
		'type' => 'raw',
		'value' => '$data->userModel->getFullName()',
		'header' => Yii::t('standard', '_FULLNAME')
	);

	$_columnList[] = array(
		'name' => 'level',
		'type' => 'raw',
		'value' => 'Yii::t("levels", "_LEVEL_" . $data->learningCourseuser->level)',
		'header' => Yii::t('standard', '_LEVEL')
	);

	$_columnList[] = array(
		'name' => 'subscribed_by',
		'type' => 'raw',
		'value' => '$data->learningCourseuser->learningUserSubscribedBy ? $data->learningCourseuser->learningUserSubscribedBy->getClearUserId() : "-"',
		'header' => Yii::t('standard', '_SUBSCRIBED_BY')
	);

	$_columnList[] = array(
		'name' => 'date_subscribed',
		'type' => 'raw',
		'value' => '$data->date_subscribed',
		'header' => Yii::t('report', '_DATE_INSCR')
	);

	if ($permissions['assign']) {
		$_columnList[] = array(
			'type' => 'raw',
			'value' => array($this, 'sessionWaitingGridRenderApprove'),
			'htmlOptions' => array(
				'class' => 'button-column-single'
			),
		);
	}
	if ($permissions['del']) {
		$_columnList[] = array(
			'type' => 'raw',
			'value' => array($this, 'sessionWaitingGridRenderReject'),
			'htmlOptions' => array(
				'class' => 'button-column-single'
			),
		);
	}

	$this->widget('DoceboCGridView', array(
		'id' => 'session-waiting-grid',
		'htmlOptions' => array('class' => 'grid-view'),
		'dataProvider' => $dataProvider,
		'columns' => $_columnList,
		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'pager' => array(
			'class' => 'admin.protected.extensions.local.pagers.DoceboLinkPager',
		),
		'ajaxUpdate' => 'session-waiting-grid-all-items',
		'beforeAjaxUpdate' => 'function(id, options) {
				// The Request type MUST be POST!
				options.type = "POST";

				if (typeof options.data == "undefined") {
					options.data = {};
				}
				// Searching options are very complex and we must handle this in a weird way...
				var $ajaxGridForm = $(".ajax-grid-form");
				$ajaxGridForm.find(":radio:visible:checked, :checkbox:visible, select:visible, input[type=\"text\"]:visible, input[type=\"textfield\"]:visible").each(function(){
					options.data[this.name] = $(this).val();
				});
				if ($("#advancedSearchVisibility").val() == 1) {
					options.data["advancedSearch[visible]"] = 1;
				}

				// To allow passing selected items over pagination
				options.data.selectedItems = $(\'#\'+id+\'-selected-items-list\').val();

			}',
		'afterAjaxUpdate' => 'function(id, data){ $(document).controls(); $("#session-waiting-grid :checkbox").styler(); afterGridViewUpdate(\'session-waiting-grid\'); $(\'a[rel="tooltip"]\').tooltip();}'
	)); ?>
</div>

<br/>
<br/>
<br/>

<script type="text/javascript">
	$(document).ready(function() {
		var $grid = $('#session-waiting-grid');

		var updateSessionWaitingGrid = function() {
			$.fn.yiiGridView.update($grid.attr('id'));
		};

		$grid.find(':checkbox').styler({});

		$(document).on('click', 'a.user-manipulation-error', function (e) {
			var html = '<?= addslashes(Yii::t('user_management', 'Some of the users you are trying to manipulate are not active')); ?>';
			Docebo.Feedback.show('error', html);
		});

		$grid.find('.suspend-action').live('click', function(e) {
			e.preventDefault();
			$.post($(this).attr('href'), {}, function(data) {
				if($.type(data) != 'undefined' && $.type(data.data) == 'object'){
					var msg = data.data.error;
					Docebo.Feedback.show('error', msg);
				}
				updateSessionWaitingGrid();
			}, 'json');
		});
		$grid.find('.delete-action').live('click', function(e) {
			e.preventDefault();
			$.post($(this).attr('href'), {}, function() {
				updateSessionWaitingGrid();
			}, 'json');
		});

		$(document).delegate(".modal-session-approve-waiting", "dialog2.content-update", function() {
			var e = $(this);
			var err = IsJsonString(e.find(".opened").html());

			function IsJsonString(str) {
				try {
					return JSON.parse(str);
				} catch (e) {
					return false;
				}
			}

			if($.type(err) != 'undefined' && $.type(err.data) == 'object'){
				var msg = err.data.error;
				Docebo.Feedback.show('error', msg);
				e.find('.modal-body').dialog2("close");
				updateSessionWaitingGrid();
			}

		});


		$('#session-waiting-massive-action').live('change', function() {

			var selectedUsers = [];

			var selectedItems = $('#session-waiting-grid-selected-items-list').val();
			if (selectedItems.length) {
				selectedUsers = selectedItems.split(',');
			}

			if (selectedUsers.length) {
				$('#session-'+$(this).val()+'-massive-link').click();
			}


			// clear selection
			$(this).find('option:first').attr('selected', true);
		});
	});
</script>