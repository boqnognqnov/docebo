<?php
/* @var $this SessionController */
?>

<h1><?= Yii::t('standard', '_CONFIRM') ?></h1>

<div>
	<strong><?= Yii::t('standard', '_WARNING') ?>:</strong>
	&nbsp;
	<?= Yii::t('curricula', '{n} selected user(s) have been assigned to a learning plan and cannot be unenrolled from course', array('{n}' => '<strong>'.count($lpUserIds).'</strong>')) ?>
</div>

<div class="form-actions">
	<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>
