<?php
/* @var $this SessionController */
/* @var $form CActiveForm */
?>

<h1><?= Yii::t('standard', '_CONFIRM') ?></h1>


<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'session-delete-users-form',
	'method' => 'POST',
	'htmlOptions' => array(
		'class' => 'ajax docebo-form'
	),
));

echo CHtml::hiddenField('selected_users', implode(',', $userIds));
echo CHtml::hiddenField('confirm', 1);

?>

<p>
	<?= Yii::t('classroom', 'Delete {count} user(s)?', array('{count}'=>CHtml::tag('strong', array('id'=>'session-delete-count-users'), count($userIds)))) ?>
</p>

<div>
	<label for="confirm" class="checkbox">
		<span style="margin-right: 10px;"><input type="checkbox" id="confirm"/></span>
		&nbsp;<?= Yii::t('standard', 'Yes, I confirm I want to proceed') ?>
	</label>
</div>

<?php if (PluginManager::isPluginActive('CurriculaApp')): ?>
	<?php if (!empty($lpUserIds)): ?>
		<br/>
		<br/>
		<div>
			<strong><?= Yii::t('standard', '_WARNING') ?>:</strong>
			&nbsp;
			<?= Yii::t('curricula', '{n} selected user(s) have been assigned to a learning plan and cannot be unenrolled from course', array('{n}' => '<strong>'.count($lpUserIds).'</strong>')) ?>
		</div>
	<?php endif; ?>
<?php endif; ?>

<div class="form-actions">
	<input class="btn-docebo green big btn-submit disabled" type="submit" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" />
	<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">

	(function() {

		$(document).undelegate(".modal.delete-node", "dialog2.content-update"); //clear possible previously assigned events
		$(document).delegate(".modal.delete-node", "dialog2.content-update", function() {
			var e = $(this);

			var autoclose = e.find("a.auto-close");
			var courseList = $("#courseList");
			var sessionList = $("#sessionList");

			if (autoclose.length > 0) {
				e.find('.modal-body').dialog2("close");

				//update selection
				var unselect = autoclose.data("unselect");
				if (unselect) {
					removeFromGridSelection('session-assign-grid', (unselect + '').split(','));
				}

				if(courseList && sessionList){
					// reload grid
					$("#session-assign-grid").yiiGridView('update',  {
						data: {
							sessionList: sessionList.val(),
							courseList: courseList.val()
						}
					});
				}else{
					// reload grid
					$("#session-assign-grid").yiiGridView('update', {});
				}


				var href = autoclose.attr('href');
				if (href) {
					window.location.href = href;
				}
			}
			else {

				$('.modal :checkbox').styler({});

				var $form = $('#session-delete-users-form');

				/*
				var countUsers = 1;
				var selectedItems = $('#session-assign-grid-selected-items-list').val();
				$form.find('#selected_users').val(selectedItems);
				if (selectedItems.length) {
					var selectedUsers = selectedItems.split(',');
					if (selectedUsers.length) {
						countUsers = selectedUsers.length;
						$('.modal #session-delete-count-users').html(countUsers);
					}
				}
				*/

				$('.modal :checkbox').change(function() {
					if ($(this).is(':checked')) {
						e.find('.btn-submit').removeClass('disabled');
					} else {
						e.find('.btn-submit').addClass('disabled');
					}
				});

				e.find('.btn-submit').click(function() {
					if ($(this).hasClass('disabled'))
						return false;
				});
			}
		});

	})();

</script>