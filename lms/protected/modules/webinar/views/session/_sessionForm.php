<?php
/* @var $model WebinarSession */
?>
<h1>
	<i class='i-sprite white'></i>
	<?= (!$model->getPrimaryKey() ? Yii::t('classroom', 'New session') : Yii::t('classroom', 'Edit session')) ?>
</h1>

<div class="alert alert-error session-save-error" style="display:none;">
	<!-- Holds errors returned by the server (failed to save session, e.g. to issues with the Webinar tool -->
</div>

<div class="edit-session-tabs">
	<ul class="nav nav-tabs" id="sessionFormTab">
		<li class="active">
			<a data-target="#sessionForm-details" data-toggle="tab" href="#">
				<span class="user-form-details"></span><?= Yii::t('standard', '_DETAILS') ?>
			</a>
		</li>

		<li>
			<a data-target="#sessionForm-dates" data-toggle="tab" href="#">
				<span class="session-calendar-icon"></span><?= Yii::t('classroom', 'Session date(s)') ?>
			</a>

			<!-- Calendar widget, only visible when the "Session date(s)" tab is active -->
			<div id="session-dates-calendar"></div>
			<div class="popover bootstro fade bottom in" id="popup_bootstroAddAnotherDate" style="display: none;">
				<div class="popover-inner">
					<div class="popover-content">
						<div class="text-message"><span
								class="text-colored green"><?= Yii::t('classroom', 'Select a day') ?></span> <?= Yii::t('classroom', 'to add new date') ?>
						</div>
						<div class="bootstro-arrow arrow-nw"></div>
					</div>
				</div>
			</div>
		</li>

		<li>
			<a data-target="#sessionForm-evaluation" data-toggle="tab" href="#">
				<span
					class="user-form-evaluation"></span><?= Yii::t('classroom', 'Completion') ?>
			</a>
		</li>
	</ul>

	<div class="form new-session-form clearfix">
		<?php /* @var $form CActiveForm */
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'session_form',
			'htmlOptions' => array('class' => 'ajax'),
		));?>
		<div class="tab-content" id="sessionFormTabContent">
			<!-- details page -->
			<?php

			echo $form->hiddenField($model, 'course_id');
			echo $form->hiddenField($model, 'id_session');
			?>
			<div class="tab-pane active session-form-details-tab-content" id="sessionForm-details">
				<div class="mandatory">* <?= Yii::t('standard', '_MANDATORY') ?></div>
				<div class="row-fluid">
					<div class="span12">
						<div class="clearfix">
							<?php
							echo $form->labelEx($model, 'name');
							echo $form->textField($model, 'name', array('class' => 'session-name'));
							echo $form->error($model, 'name');
							?>

							<div id="session-name-error-message"  class="errorMessage"></div>
						</div>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12">
						<div class="clearfix">
							<?php echo CHtml::label(Yii::t('standard', '_DESCRIPTION'), CHtml::activeId($model, 'description')); ?>
							<div class="textarea-wrapper">
								<?php echo $form->textArea($model, 'description', array('id' => 'tiny_' . rand(1,10000), 'class' => 'session-other_info')); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12">
						<div class="clearfix">
							<span>
								<?=$model->getAttributeLabel('max_enroll');?> *
							</span>&nbsp;
							<?php echo $form->textField($model, 'max_enroll', array('class'=>'input-small')); ?>
							<div id="max-enroll-error-message" class="errorMessage"></div>
						</div>
					</div>
				</div>

				<div id="errorMessage" class="errorMessage"></div>

				<div class="session-form-actions">
					<input class="btn close-btn cancel-session-modal" type="button"
					       value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
					<input class="btn confirm-btn submit-session-button" type="button"
					       value="<?php echo Yii::t('standard', '_SAVE'); ?>"/>
					<span class="session-error"></span>
				</div>

			</div>

			<!-- session dates page -->
			<div class="tab-pane" id="sessionForm-dates">
				<!-- New date page -->
				<div id="no-date-form">
					<h2></h2>

					<div class="add-date-box">
						<p><?= Yii::t('classroom', 'No time available') ?></p>
						<input class="btn confirm-btn" type="button" value="<?php echo Yii::t('standard', '_ADD'); ?>"
						       id="session-date-add"/>
					</div>

					<div class="popover bootstro fade bottom in" id="popup_bootstroAddSessionDate"
					     style="width: 300px; max-width: 300px; top: 156px; right: 50px; display: block;">
						<div class="popover-inner">
							<div class="popover-content">
								<div class="text-message"><span
										class="text-colored green"><?= Yii::t('standard', '_ADD') ?></span> <?= Yii::t('course', '_PARTIAL_TIME') ?>
								</div>
								<div class="bootstro-arrow arrow-nw"></div>
							</div>
						</div>
					</div>

					<div class="session-form-actions">
						<input class="btn close-btn cancel-session-modal" type="button"
						       value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
						<input class="btn confirm-btn submit-session-button" type="button"
						       value="<?php echo Yii::t('standard', '_SAVE'); ?>"/>
						<span class="session-error"></span>
					</div>
				</div>
				<!-- end new date page -->

				<!-- Edit date page -->
				<div id="edit-date-form" class="date-forms" style="display: none;">
					<div class="mandatory">* <?= Yii::t('standard', '_MANDATORY') ?></div>
					<div class="date-line">
						<table>
							<tbody>
							<tr>
								<td><?php echo CHtml::label(Yii::t('standard', '_DATE') . " *", 'day'); ?></td>
								<td><?php echo CHtml::textField('day', '', array('class' => 'datepicker edit-day-input', 'id' => 'day-calendar', 'readonly' => 'readonly')); ?>
									<i class="p-sprite calendar-black-small date-icon"></i>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<div class="clearfix">
								<?php
								echo CHtml::label(Yii::t('standard', '_NAME').' *', 'name');
								echo CHtml::textField('name', '', array('id' => 'day-name'));
								?>
							</div>
						</div>
					</div>

					<div id="day-name-error" class="errorMessage"></div>

					<div class="row-fluid">
						<div class="span12">
							<div class="clearfix">
								<?php echo CHtml::label(Yii::t('standard', '_DESCRIPTION'), CHtml::activeId($model, 'description')); ?>
								<div class="textarea-wrapper">
									<?php echo CHtml::textArea('webinar_description', null, array('id' => 'tiny_' . rand(1,10000), 'class' => 'session-other_info description')); ?>
								</div>
							</div>
						</div>
					</div>

					<br/>

					<div >
						<div class="row-fluid" id="day-times">
							<div class="span4">
								<?php echo $form->labelEx($model, 'start', array('class'=>'label-no-break')) ?>
								<?php echo CHtml::dropDownList('webinar_start', null, WebinarSession::arrayForHours(), array('class' => 'webinar-start-input', 'prompt' => '', 'id' => 'webinar-start-time')); ?>
							</div>
							<div class="span5">
								<?php echo $form->labelEx($model, 'duration', array('class'=>'label-no-break')) ?>
								<?php echo CHtml::dropDownList('webinar_duration', null, WebinarSession::arrayForDuration(), array('class' => 'webinar-duration-input', 'prompt' => '', 'id' => 'webinar-duration-time')); ?> hh:mm
							</div>
							<div class="span3">
								<?php
								//$timezone = (!$model->getPrimaryKey()) ? Yii::app()->localtime->getTimezoneOffset(Yii::app()->localtime->getTimeZone()) : '';
								?>
								<label class="label-no-break text-center" id="day-timezone-label"></label>
								<br/>
								<a href="#" id="select-timezone-button">
									<?= Yii::t('classroom', 'Different timezone') ?>?
								</a>
							</div>
						</div>

						<div id="time-error-message" class="errorMessage"></div>
					</div>
					<div class="row-fluid" id="timezone-line" style="display: none">
						<div class="span12">
							<div class="clearfix">
								<?php
								echo CHtml::label(Yii::t('classroom', 'Select Timezone'), 'timezone');
								echo CHtml::dropDownList('timezone', '', Yii::app()->localtime->getTimezonesArray(), array('id' => 'day-timezone'));
								?>
							</div>
						</div>
					</div>

					<p class="webinar-advance-time-label">
						<?=Yii::t('webinar', 'How long would you like to display the "join" button before the webinar session starts?')?>
					</p>

					<br/>

					<div>
						<div class="row-fluid">
							<div class="span6">
								<span><?=Yii::t('levels', '_LEVEL_6')?>:</span>
								&nbsp;&nbsp;&nbsp;
								<?=CHtml::dropDownList('join_in_advance_time_teacher', null, WebinarSession::arrayForDuration(1), array('class'=>'webinar-duration-input'));?>
								hh:mm
							</div>
							<div class="span6">
								<span><?=Yii::t('levels', '_LEVEL_3')?>:</span>
								&nbsp;&nbsp;&nbsp;
								<?=CHtml::dropDownList('join_in_advance_time_user', null, WebinarSession::arrayForDuration(1), array('class'=>'webinar-duration-input'));?>
								hh:mm
							</div>
						</div>
						<br/>
						<p class="muted smaller">
							<?=Yii::t('webinar', 'Leave blank to display the button right at the beginning of webinar.')?>
						</p>
					</div>

					<br/>

					<? $this->widget('common.widgets.warningStrip.WarningStrip', array(
						'message'=>Yii::t('webinar', 'Do you know that Docebo is fully integrated with the most common web conference systems?').' '.CHtml::link(Yii::t('webinar', 'Click here to discover more'), Docebo::createAbsoluteUrl('app/index')),
						'type'=>'info'
					)); ?>

					<br/>

					<div class="row-fluid">
						<div class="span12">
							<div class="clearfix">
								<?php echo $form->labelEx($model, 'tool');
								//if(!$model->id_session)
									echo CHtml::dropDownList('webinar_tool', null, WebinarSession::toolList(), array('class' => 'webinar-dropdown-full', 'id' => 'toolDropdown', 'prompt' => Yii::t('webinar', 'Select webinar tool')));
//								else {
//									echo CHtml::dropDownList('webinar_tool', null, WebinarSession::toolList(), array('disabled' => 'disabled', 'class' => 'webinar-dropdown-full', 'id' => 'toolDropdown', 'prompt' => Yii::t('webinar', 'Select webinar tool')));
//									echo CHtml::hiddenField('webinar_tool');
//								}
								?>
							</div>
						</div>
					</div>

					<div class="row-fluid" id="tool_custom" style="display: <?=($model->tool && $model->tool == WebinarSession::TOOL_CUSTOM) ? 'block' : 'none'?>;">
						<div class="span12">
							<div class="clearfix">
								<?php
								echo $form->labelEx($model, 'custom_url');
								echo CHtml::textField('webinar_custom_url', null, array('class' => 'session-name'));
								?>
							</div>
						</div>
					</div>

					<br/>
					<div id="webinar-tool-error" class="errorMessage"></div>

					<?php if(Yii::app()->event->raise('onBeforeRenderWebinarSessionToolsAccounts', new DEvent($this, array('session' => $model)))): ?>
						<div class="row-fluid" id="tool_selection" style="display: <?=($model->tool && $model->tool != WebinarSession::TOOL_CUSTOM) ? 'block' : 'none'?>;">
							<div class="span12">
								<div class="clearfix tool-account-box">
									<?php
									$this->renderPartial('_tool_accounts', array('model' => $model));
									?>
								</div>
							</div>
						</div>
					<?php endif; ?>

					<div class="session-form-actions">
						<input id="cancel-edit-session-date" class="btn close-btn cancel-session-modal" type="button"
						       value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
						<input id="create-session-date" class="btn confirm-btn" type="button"
						       value="<?php echo Yii::t('classroom', 'Create session date'); ?>" name="submit"/>
					</div>
				</div>

				<!-- View date page -->
				<div id="view-date-form" class="date-forms" style="display: none;">
					<h2></h2>

					<div class="day-title clearfix"><i></i>

						<div>
							<div class="name-label"></div>
							<div class="description-label"></div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="day-time-line clearfix">
						<i></i>

						<h3><?= Yii::t('classroom', 'What time?'); ?></h3>

						<div></div>
					</div>
					<div class="day-location clearfix">
						<i></i>

						<h3><?= $model->getAttributeLabel('tool')?></h3>

						<div></div>
					</div>

					<div class="clearfix"></div>

					<div class="view-date-form-actions">
						<input id="make-copy-date" class="btn confirm-btn" type="button"
						       value="<?php echo Yii::t('standard', '_MAKE_A_COPY'); ?>"/>
						<input id="edit-session-date" class="btn edit-btn" type="button"
						       value="<?php echo Yii::t('standard', '_MOD'); ?>"/>
						<input id="delete-session-date" class="btn close-btn" type="button"
						       value="<?php echo Yii::t('standard', '_DEL'); ?>"/>

						<div id="copy-date-box" style="display: none">
							<div class="arrow"></div>
							<table>
								<tbody>
								<tr>
									<td><?= Yii::t('classroom', 'Copy this session date to') ?>&nbsp;</td>
									<td><?php echo CHtml::textField('destination-day', '', array('class' => 'datepicker edit-day-input', 'id' => 'destination-day', 'readonly' => 'readonly')); ?>
										<i class="p-sprite calendar-black-small date-icon"></i>
									</td>
									<td style="text-align: right; width: 35%;">
										<input id="confirm-copy-session-date" class="btn confirm-btn" type="button"
										       value="<?php echo Yii::t('standard', '_MAKE_A_COPY'); ?>" name="submit"/>
									</td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="session-form-actions">
						<input class="btn close-btn cancel-session-modal" type="button"
						       value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
						<input class="btn confirm-btn submit-session-button" type="button"
						       value="<?php echo Yii::t('standard', '_SAVE'); ?>"/>
						<span class="session-error"></span>
					</div>
				</div>
				<!-- end End date page -->
			</div>
			<!-- end dates page -->

			<!-- evaluation page -->
			<div class="tab-pane session-form-evaluation-tab-content" id="sessionForm-evaluation">

				<div class="row-fluid">
					<div class="span12">
						<div>
							<div class="radio-list-element session-form-evaluation-spacer">
								<?= $form->radioButton($model, 'evaluation_type', array('id' => 'evaluation-score-type-score', 'value' => WebinarSession::EVAL_TYPE_ON_JOIN, 'uncheckValue' => null)) ?>
							</div>
							<div class="radio-list-element session-form-evaluation-label">
								<?php
								echo CHtml::label(Yii::t('webinar', 'Mark as complete when the user joins the webinar'), 'evaluation-score-type-score', array());
								?>
							</div>
						</div>

						<div id="must-complete-count" style="display:none">
							<?=Yii::t('webinar', 'User must complete at least')?>
							<?=$form->textField($model, 'min_attended_dates_for_completion', array('style'=>'width:100px;'))?>
							/ <span id="count"><?=count($model->getDates())?></span>
							<?=Yii::t('webinar', 'session dates')?>

							<div id="must-complete-count-error" class="errorMessage"></div>
						</div>
						<div id="additional-completion">
							<?=$form->checkBox($model, 'allow_recording_completion', array('id' => 'allow_recording_completion', 'value' => 1, 'uncheckValue' => 0))?> <?=CHtml::label(Yii::t('webinar', 'Set as complete if the user performs access to the recordings'), 'allow_recording_completion')?>
						</div>
						<? /*
						 <div id="evaluation-score-base-option" style="display:<?= $model->evaluation_type == WebinarSession::EVAL_TYPE_ON_JOIN ? 'block' : 'none' ?>">
							 <div class="radio-list-element session-form-evaluation-spacer">
								 &nbsp;
							 </div>
							 <div class="radio-list-element">
								 <?php
								 echo $form->checkBox($model, 'complete_on_registration_watched');
								 echo $form->labelEx($model, 'complete_on_registration_watched', array('class' => 'webinar-form-inline-label'));
								 ?>
								 <br>
								 <?
								 echo $form->checkBox($model, 'eval_spent_time_on_webinar');
								 echo CHtml::label(Yii::t('webinar', 'and the user spent at least'), 'WebinarSession_eval_spent_time_on_webinar', array('class' => 'webinar-form-inline-label'));
								 echo $form->textField($model, 'time_spent_to_complete', array('style' => 'width: 40px;'));
								 echo CHtml::label(Yii::t('webinar', 'minutes of the webinar'), 'WebinarSession_eval_spent_time_on_webinar', array('class' => 'webinar-form-inline-label'));
								 ?>
							 </div>
						 </div>
 */ ?>
						<!-- evaluation by online test -->
						<div>
							<div class="radio-list-element session-form-evaluation-spacer">
								<?= $form->radioButton($model, 'evaluation_type', array('id' => 'evaluation-score-type-manual', 'value' => WebinarSession::EVAL_TYPE_MANUAL, 'uncheckValue' => null)) ?>
							</div>
							<div class="radio-list-element session-form-evaluation-label">
								<?php
								echo CHtml::label(Yii::t('standard', '_MANUAL'), 'evaluation-score-type-manual', array());
								?>
							</div>
						</div>

						<div>
							<div class="radio-list-element session-form-evaluation-spacer">
								<?= $form->radioButton($model, 'evaluation_type', array('id' => 'evaluation-score-type-online', 'value' => WebinarSession::EVAL_TYPE_ONLINE_TEST, 'uncheckValue' => null)) ?>
							</div>
							<div class="radio-list-element session-form-evaluation-label">
								<?php
								echo CHtml::label(Yii::t('classroom', 'Online test'), 'evaluation-score-type-online', array());
								?>
							</div>
						</div>
					</div>
				</div>

				<div class="session-form-actions">
					<input class="btn close-btn cancel-session-modal" type="button"
					       value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
					<input class="btn confirm-btn submit-session-button" type="button"
					       value="<?php echo Yii::t('standard', '_SAVE'); ?>"/>
					<span class="session-error"></span>
				</div>

			</div>
			<!-- end evaluation page -->

			<!-- end details page -->
		</div>
	</div>
	<div class="form-actions">
		<?php echo CHtml::submitButton(Yii::t('standard', '_CONFIRM'), array('class' => 'btn btn-submit webinar-form-submit')); ?>
		<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>

<span class="loader" style="display: none;"></span>

<script type="text/javascript">
	/* ===  Onload event handler  === */
	$(function () {
		$(document).controls();
		//because for some fucking reason it's not working by default! 0_o
		$('.webinar-form-submit').click(function () {
			$('#session_form').submit();
		});
		$('input').styler();
		$('.datepicker').bdatepicker({
			format: 'yyyy-mm-dd',
			language: yii.language,
			autoclose: true
		});
		$(".date-icon").on('click', function (ev) {
			$(this).prev('input').bdatepicker().data('datepicker').show();
		});

		$("#toolDropdown").change(function () {
			var selectedTool = $(this).val();
			if (selectedTool == '<?=WebinarSession::TOOL_CUSTOM?>') {
				$("#tool_custom").show();
				$("#tool_selection").hide();
			}else if(selectedTool==''){
				// No tool selected
				$('#tool_selection').hide();
				$('#tool_custom').hide();
			}else {
				loadToolAccounts($(this).val());
				$("#tool_custom").hide();
				$("#tool_selection").show();
			}
		});

		var evalutionTypeChanged = function(){
			if ($("input[name='WebinarSession[evaluation_type]']:checked").val() == '<?=WebinarSession::EVAL_TYPE_ON_JOIN?>') {
				$('#must-complete-count, #additional-completion').show();
			} else {
				$('#allow_recording_completion').prop('checked', false);
				$('#allow_recording_completion-styler').removeClass('checked');
				$('#must-complete-count, #additional-completion').hide();
			}
		};
		$("input[name='WebinarSession[evaluation_type]']").change(function () {
			evalutionTypeChanged();
		});
		evalutionTypeChanged();


		var translations = <?= CJSON::encode(array(
			'_SAVE' => Yii::t('standard', '_SAVE'),
			'_SET_UP_DATES' => Yii::t('classroom', 'Set up dates'),
			'_MISSING_SESSION_DATE' => Yii::t('classroom', 'You can\'t save unless you <br/><span class="bold">set up at least one session date</span>'),
			'_OPERATION_FAILURE' => Yii::t('standard', '_OPERATION_FAILURE'),
			'_SESSION_NAME_IS_REQUIRED' => Yii::t('classroom', 'Session name is required'),
			'_ENTER_A_NUMBER_GREATER_THAN_0' => Yii::t('classroom', 'Enter a number greater than 0'),
			'_MIN_COMPLETED_DATES_CAN_NOT_BE_BIGGER_THAN_NUMBER_OF_DATES'=>Yii::t('webinar', 'The number of minimum attendances can not be bigger than the number of total dates'),
			'_INVALID_OR_MISSING_DATE_HOURS' => Yii::t('classroom', 'Invalid or missing date hours'),
			'_NO_TOOL_SELECTED' => Yii::t('webinar', 'Invalid webinar tool'),
			'_NO_CUSTOM_TOOL_URL' => Yii::t('webinar', 'Please add custom webinar URl'),
			'_WEBINAR_ACCOUNT_MISSING' => Yii::t('webinar', 'Please select webinar account'),
			'_NO_CLASSROOM_SELECTED' => Yii::t('classroom', 'No classroom selected'),
			'_SEATS' => strtolower(Yii::t('standard', 'Available seats')),
			'_UPDATE_SESSION_DATE' => Yii::t('classroom', 'Update session date'),
			'_CREATE_SESSION_DATE' => Yii::t('classroom', 'Create session date'),
			'_ERROR_NONAME'=>Yii::t('standard', '_ERROR_NONAME'),
			'_DATE_CAN_NOT_BE_PAST'=>Yii::t('webinar', 'Can not create session date in the past')
		)) ?>;

		WebinarSessionForm.init({
			sessionDates: <?= $model->getSessionDatesAsJson() ?>,
			validateToolAccount: <?=CJSON::encode(Yii::app()->event->raise('onBeforeRenderWebinarSessionToolsAccounts', new DEvent($this, array('session' => $model))));?>
		},{
			months: <?=CJSON::encode(TimeHelpers::getLocalizedMonthNames())?>,
			translations: translations,
			timezoneOffsets: <?= CJSON::encode(Yii::app()->localtime->getTimezonesArray()) ?>,
			timezoneOffsetsSeconds: <?=CJSON::encode(Yii::app()->localtime->getTimezoneOffsetsSecondsArray()) ?>,
			currentTimezone: '<?= Yii::app()->localtime->getTimeZone()?>',
			webinarToolNames: <?=CJSON::encode(WebinarSession::toolList())?>,
			isEditingSession: <?=intval(!$model->getIsNewRecord())?>
		});
	});

	function loadToolAccounts(tool) {
		var session = '<?= $model->id_session ?>';
		var date = $('#day-calendar').val();
		var time = $("#webinar-start-time").val();
		var duration = $("#webinar-duration-time").val();
		$.ajax({
			type: "POST",
			data: {'date': date, 'time': time, 'duration': duration, 'session': session},
			url: "<?=Docebo::createLmsUrl('webinar/session/getAccountsByTool')?>&tool=" + tool,
			context: document.body
		}).done(function (data) {
			$('.tool-account-box').html(data);
			$('#toolDropdown').trigger('accountsListLoaded');
		});
	}

	function reloadAccounts() {
		var tool = $("#toolDropdown").val();
		if (!tool) return;
		var date = $('#day-calendar').val();
		var start = $("#webinar-start-time").val();
		var duration = $("#webinar-duration-time").val();
		if (date && start && duration)
			loadToolAccounts(tool);
	}

	$("#day-calendar").on("change", function () {
		var tool = $("#toolDropdown").val();
		if ('<?= $model->id_session ?>') return;
		// Reload the accounts
		reloadAccounts();
	});
	$("#webinar-start-time").on("change", function () {
		var tool = $("#toolDropdown").val();
		if ('<?= $model->id_session ?>') return;
		// Reload the accounts
		reloadAccounts();
	});
	$("#webinar-duration-time").on("change", function () {
		if ('<?= $model->id_session ?>') return;
		// Reload the accounts
		reloadAccounts();
	});

</script>
