<?php /* @var $data array  */ ?>
<div class="item">
	<div class="primary row-fluid">
		<div class="item-col col-id">
			<?= CHtml::radioButton('courseSessionId', false, array('value' => $data['id_session'])) ?>
		</div>
		<div class="item-col col-name text-colored">
			<?= ($data->isMaxEnrolledReached() ? '<i class="i-sprite is-clock orange" style="margin-bottom:4px;" rel="tooltip" title="" data-original-title="'.Yii::t("course", "_ALLOW_OVERBOOKING").'"></i> ' : '').$data->name; ?>
		</div>
		<div class="item-col col-start">
			<?= WebinarSession::getStartDate($data['id_session']); ?>
		</div>
		<div class="item-col col-duration">
			<?= $data->getTotalHours() ?>
		</div>
		<div class="item-col col-tool">
			<?= $data->getToolName() ?>
		</div>
	</div>
</div>
<script type="text/javascript">
	if($('i[rel="tooltip"]'))
		$('i[rel="tooltip"]').tooltip();
</script>