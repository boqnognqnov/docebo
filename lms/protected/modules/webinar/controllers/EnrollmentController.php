<?php
/**
 * Controller to manage session enrollments
 * Class EnrollmentController
 */
class EnrollmentController extends WebinarBaseController {

	/**
	 * Before action controller
	 * @param CAction $action
	 * @return bool
	 */
	public function beforeAction($action) {

		JsTrans::addCategories('standard', 'course', 'course_management');

		$rq = Yii::app()->request;

		switch ($action->id) {
			case 'index':
				//sometimes it may happen that some enrolled/imported users have a invalid enrollment level. Trying to fix it.
				LearningCourseuser::fixEnrollmentsLevel();
			case 'index': case 'enroll':
				$selectedItems = $rq->getParam('selectedItems', '');
				if (Yii::app()->request->isAjaxRequest && !empty($selectedItems)) {
					Yii::app()->session['selectedItems'] = explode(',', $selectedItems);
				} else {
					Yii::app()->session['selectedItems'] = array();
				}
			break;
		}

		return parent::beforeAction($action);
	}


	public function inSessionList($index, $value) {
		$list = Yii::app()->session['selectedItems'];
		return in_array($value, (!$list ? array() : $list));
	}


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array('editUser','enroll','importUsersFromSession','removeUser','removeUserMultiple');
		$res[] = array(
			'allow',
			'roles' => array('/lms/admin/course/mod'),
			'actions' => $admin_only_actions,
		);

		// Allow power users to access those actions
		// but in the actions themselves remember to check if the
		// session is owned by the PU himself (created by him)
		$res[] = array(
			'allow',
			'expression' => '(Yii::app()->user->getIsPu() && (Yii::app()->user->checkAccess("/lms/admin/webinarsessions/add") || Yii::app()->user->checkAccess("/lms/admin/webinarsessions/mod") || Yii::app()->user->checkAccess("/lms/admin/webinarsessions/assign")))',
			'actions' => array('removeUser', 'importUsersFromSession', 'editUser')
		);

		$res[] = array(
			'allow',
			'expression' => '(Yii::app()->user->getIsPu() && (Yii::app()->user->checkAccess("enrollment/create") || Yii::app()->user->checkAccess("/lms/admin/webinarsessions/assign") || (Yii::app()->user->checkAccess("/lms/admin/webinarsessions/add") && WebinarSession::isUserCreator(Yii::app()->request->getParam("id_session"), Yii::app()->user->id) ) ))',
			'actions' => array('index')
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		$res[] = array(
			'deny',
			'expression' => '(Yii::app()->user->getIsPu() && !Yii::app()->user->checkAccess("/lms/admin/webinarsessions/add") && !Yii::app()->user->checkAccess("/lms/admin/webinarsessions/mod") && !Yii::app()->user->checkAccess("/lms/admin/webinarsessions/assign"))',
			'actions' => array('removeUser', 'importUsersFromSession', 'editUser')
		);

		$res[] = array(
			'deny',
			'expression' => 'Yii::app()->user->getIsPu() && !Yii::app()->user->checkAccess("enrollment/create") && !Yii::app()->user->checkAccess("/lms/admin/webinarsessions/assign")',
			'actions' => array('index')
		);

		$res[] = array(
			'deny',
			'expression' => 'Yii::app()->user->getIsPu() && !Yii::app()->user->checkAccess("enrollment/create") && '.($this->courseModel && $this->_sessionModel && Yii::app()->user->checkPURights($this->courseModel->idCourse)->isInstructor ? 1 : 0),
			'actions' => array('index')
		);

        $res[] = array(
            'deny',
            'expression' => 'Yii::app()->user->getIsUser() && LearningCourseuser::userLevel(Yii::app()->user->id, Yii::app()->request->getParam("course_id")) != LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR',
            'actions' => array('index')
        );

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}



	/**
	 * The main action, it displays enrollments management interface.
	 *
	 * @throws CException
	 */
	public function actionIndex() {
        // FancyTree initialization
        Yii::app()->fancytree->init();

		if (!$this->courseModel || !$this->_sessionModel)
			throw new CHttpException("Invalid session information");
                  $pUserRights = Yii::app()->user->checkPURights($this->courseModel->idCourse);
		if($pUserRights->isPu && !Yii::app()->user->checkAccess("/lms/admin/webinarsessions/mod") && !$pUserRights->isInstructor && !Yii::app()->user->checkAccess("/lms/admin/webinarsessions/assign") ){
			if($this->_sessionModel->created_by!=Yii::app()->user->getIdst()){
				throw new CException("Power users can only view their own sessions");
			}
		}

		// Let "player" Yii component know about the course we are in
		// This will allow Main Menu to show "course related" tiles: "play" and "manage courses"
		Yii::app()->player->setCourse($this->courseModel);
                  $pUserRights = Yii::app()->user->checkPURights($this->courseModel->idCourse);
		if($pUserRights->isPu && !$pUserRights->courseIsAssigned && !$pUserRights->isInstructor) {
			$this->redirect(Docebo::createLmsUrl('player/training/webinarSession', array('course_id'=>$this->courseModel->idCourse)));
		}

		UsersSelector::registerClientScripts();

		Yii::app()->tinymce;

		$this->render('index', array(
			'courseModel' => $this->courseModel,
			'sessionModel' => $this->sessionModel,
			'searchInput' => Yii::app()->request->getParam('search_input', null),
			'orderByInput' => Yii::app()->request->getParam('order_by', null)
		));
	}



	/**
	 * Action to import users from another session
	 */
	public function actionImportUsersFromSession() {
		$result = array();

		if(Yii::app()->user->getIsPu() && !Yii::app()->user->checkAccess("/lms/admin/webinarsessions/mod") && !Yii::app()->user->checkAccess("/lms/admin/webinarsessions/assign")){
			if($this->_sessionModel->created_by!=Yii::app()->user->getIdst()){
				throw new CException("Power users can only view their own sessions");
			}
		}

		try {
			// Check input params
			$rq = Yii::app()->request;

			// - Session id
			$idSession = $rq->getParam('id_session', 0);
			if (!$idSession)
				throw new Exception("Invalid session id");

			$sessionModel = WebinarSession::model()->findByPk($idSession);

			// Are we saving a user enrollment?
			if ($rq->getParam('confirm', false) !== false) {

				// Check the session to import from
				$importFrom = $rq->getParam('import_from');
				if(!$importFrom)
					throw new Exception('Invalid source session provided');

				// Try to load the import from session (using the exact name)
				/* @var $importSession WebinarSession */
				$importSession = WebinarSession::model()->findByAttributes(array('name' => $importFrom, 'course_id'=>$sessionModel->course_id));
				if(!$importSession)
					throw new CException("The session your're trying to import from does not exist");

				// Check the session if different from the current one
				if ($importSession->id_session == $idSession)
					throw new CException('Source session cannot be the same as the destination one');

				// Ok, we can start filtering users, using the level
				$level = $rq->getParam('level', LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
				$importList = $importSession->getEnrolledUsers($level);
				$message = '';
				if (!empty($importList)) {

					// Check enrollment limit for our session
					$toImport = count($importList);
					$existingUsers = $this->_sessionModel->getEnrolledUsers();
					if (!WebinarSession::checkIfEnrollmentsAreAvailable($idSession, $toImport)) {
						// Not enough room for imported users, we are beyond enrollment limit
						throw new CException('Enrollment limit has been reached');
					}

					try {
						$db = Yii::app()->db;

						// Start transaction
						if ($db->getCurrentTransaction() === NULL)
							$transaction = $db->beginTransaction();

						// Collect already subscribed user ids
						$checkExisting = array();
						foreach ($existingUsers as $tmp)
							$checkExisting[] = $tmp['id_user'];

						$countImported = 0;
						foreach ($importList as $importEnroll) {
							if (!in_array($importEnroll['id_user'], $checkExisting)) {
								$user = new WebinarSessionUser();
								$user->id_session = $idSession;
								$user->id_user = $importEnroll['id_user'];
								$user->date_subscribed = Yii::app()->localtime->toLocalDateTime();

								if(!CoreUser::adminRuleEnabled(Yii::app()->user->id, 'direct_course_subscribe') && Yii::app()->user->getIsPu())
									$user->status = WebinarSessionUser::$SESSION_USER_WAITING_LIST;

								if(!$user->save())
									throw new Exception("Error importing a user");

								Yii::app()->event->raise(EventManager::EVENT_USER_ENROLLED_IN_WEBINAR_SESSION, new DEvent($this, array(
									'session_id' => $idSession,
									'user' => $importEnroll['id_user'],
								)));
								$countImported++;
							}
						}

						// End transaction
						if (isset($transaction))
							$transaction->commit();

						$message = Yii::t('classroom', 'Operation successfully completed. {count} users were imported', array('{count}' => $countImported));
						$result = array('status' => 'saved', 'message' => $message);

					} catch (Exception $e) {
						if (isset($transaction))
							$transaction->rollback();
						throw new Exception($e->getMessage());
					}
				} else
					throw new CException("No users were available to import");

			} else {
				// Just show the edit form
				$html = $this->renderPartial('_importUsersFromSession', array(
					'idCourse' => $this->_sessionModel->course_id,
					'idSession' => $idSession
				), true);

				$result = array('html' => $html);
			}
		} catch(CException $ex) {
			$message = Yii::t('classroom', $ex->getMessage());
			$result = array('status' => 'error', 'message' => $message);
		} catch(Exception $e) {
			$message = Yii::t('standard', '_OPERATION_FAILURE');
			$result = array('status' => 'error', 'message' => $message);
		}

		$this->sendJSON($result);
		Yii::app()->end();
	}


	/**
	 * Extract information from HTTP request (sent by UsersSelector) and enroll users to a Session
	 * @throws CException
	 */
	public function actionEnrollUsers() {

		$rq = Yii::app()->request;

		$idCourse = $rq->getParam('idCourse', 0);
		$idSession = $rq->getParam('idSession', 0);

		$usersList = UsersSelector::getUsersList($_REQUEST, false, true);
		if (!empty($usersList)) {

			try {

				$sessionModel = WebinarSession::model()->findByPk($idSession);
				if ($sessionModel->course_id != $idCourse) {
					throw new CException('Invalid session id ('.$idSession.')');
				}

				$sessionUsers = array();
				foreach ($sessionModel->getEnrolledUsers() as $enrolledUser) {
					$sessionUsers[] = $enrolledUser['id_user'];
				}

				$toEnroll = array();
				foreach ($usersList as $idUser) {
					if (!in_array($idUser, $sessionUsers)) {
						$toEnroll[] = $idUser;
					}
				}

				$idCourse = $sessionModel->course_id;

				// Try to find out which unique users we are enrolling now are enrolled
				// in a session in the course for the first time (exclude them if they
				// are enrolled to another session in this course already)
				// This is important for decreasing Power User's seats in the course
				// by those number of users
                /*
                 * Requested by QA's in the comments of https://docebo.atlassian.net/browse/LB-858
                 *
                $usersNewToCourse = array();
                $usersInCourseAlready = Yii::app()->getDb()->createCommand()
                    ->selectDistinct('wsu.id_user')
                    ->from(WebinarSessionUser::model()->tableName().' wsu')
                    ->join(WebinarSession::model()->tableName().' cs', 'cs.id_session=wsu.id_session AND cs.course_id=:idCourse', array(':idCourse'=>$idCourse))
                    ->queryColumn();

                foreach($toEnroll as $userId){
                    if(!in_array($userId, $usersInCourseAlready)){
                        $usersNewToCourse[] = $userId;
                    }
                }
                 */

                if(count($toEnroll) && $this->_courseModel->selling && CoreUserPU::isPUAndSeatManager()) {
                    // The current user is a Power user and he is
                    // allowed to enroll via "seats" only.
                    // Check if he has enough available for this course
                    if(!CoreUserPuCourse::checkIfPuHasEnoughSeatsInCourse(count($toEnroll), $idCourse)){
                        throw new CException(Yii::t('standard', "You don't have enough seats for this course"));
                    }
                }

				if(!$toEnroll){
					throw new CException(Yii::t('standard', 'Select users'));
				}

                if (!WebinarSession::checkIfEnrollmentsAreAvailable($idSession, count($toEnroll))) {
                    throw new CException(Yii::t('classroom', 'Limit of max enrollable users reached'));
                }
                else {
                	
                	if (Yii::app()->backgroundJob->bgJobEnabled(BackgroundJob::FAMILTY_MASSENROLL)) {
						$courseModel = LearningCourse::model()->findByPk($idCourse);
						$input = array(
							'idst' => $toEnroll,
							'courses' => array($idCourse)
						);
						$params = array(
							'webinarSessions' => array($idSession)
						);
						$this->createBackgroundJob(array(
							'module' => 'course',
							'value' => 'Enrolling {users} in session "{session_name}" for course "{course_name}"',
							'params' => array(
								'users' => count($toEnroll),
								'session_name' => $sessionModel->name,
								'course_name' => $courseModel->name
							)
						), 'lms.protected.modules.backgroundjobs.components.handlers.EnrollUsersToManyCourses', $input, $params);
	
						$response = new AjaxResult();
						$response->setStatus(true);
						$response->setHtml("<a class='auto-close'></a>")->toJSON();
						Yii::app()->end();
                	}



                    // Progressive enrolling users to webinar session
                    $html = $this->renderPartial('_mass_enroll_users_to_webinar_session', array(
                        'users' => implode(',', $toEnroll), // Build string with the all users ids
                        'idSession' => $idSession,
                        'idCourse' => $idCourse
                    ));

                    $response = new AjaxResult();
                    $response->setStatus(true);
                    $response->setHtml($html."<a class='auto-close'></a>")->toJSON();
                    Yii::app()->end();
                }
			}
			catch (CException $e) {
				Yii::log($e->getMessage(), 'error');
				Yii::app()->user->setFlash('error', $e->getMessage());
				$selectorDialogUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
					'type' 	=> UsersSelector::TYPE_WEBINAR_ENROLLMENT,
					'idCourse'	=> $idCourse,
					'idSession'	=> $idSession,
				));
				$this->redirect($selectorDialogUrl, true);
			}


		}

		echo '<a class="auto-close success"></a>';
		Yii::app()->end();


	}

    /**
     * Progressively enroll users to a Webinar session
     */
    public function actionAxProgressiveEnrollUserseToWebinarSession(){
        $rq = Yii::app()->request;
        $chunkSize = 100;
        $userIdsThisIteration = array();
        $users = $rq->getParam('users', FALSE);
        $idCourse = $rq->getParam('idCourse', 0);
        $idSession = $rq->getParam('idSession', NULL);

        // Check if this is string
        if(is_string($users))
        {
            // if is only one user
            if(!strpos($users,',') !== false)
                $userList = array($users);
            else
                $userList = explode(',', $users);
        }
        else{
            $this->sendJSON(array());
        }

        // Get current offset and calculate stop flag
        $offset = Yii::app()->request->getParam('offset', 0);
        $totalCount = count($userList);
        $newOffset = $offset + $chunkSize;
        $stop = ($newOffset < $totalCount) ? false : true;
        $counter = 0;
        for ($index = $offset; ($index < $newOffset && $index < $totalCount); $index++) { //NOTE: $newOffset may be greater than $totalCount, but we wouldn't count over $totalCount
            $counter ++;
            if ( $counter > $chunkSize ) break; //NOTE: this shouldn't happen
            // Get current enrollment "job". If we are over the limit, invalidate value and stop this iteration
            $userId = ( isset( $userList[ $index ] ) ? $userList[ $index ] : false );
            if (!$userId || empty($userId))continue;
            $userIdsThisIteration[] = $userId;
        }

	    // Try to find out which unique users we are enrolling now are enrolled
	    // in a session in the course for the first time (exclude them if they
	    // are enrolled to another session in this course already)
	    // This is important for decreasing Power User's seats in the course
	    // by those number of users
	    $usersNewToCourseInThisProgressiveIteration = array();
	    $usersInCourseAlready = Yii::app()->getDb()->createCommand()
	                               ->selectDistinct('cus.id_user')
	                               ->from(WebinarSessionUser::model()->tableName().' cus')
	                               ->join(WebinarSession::model()->tableName().' cs', 'cs.id_session=cus.id_session AND cs.course_id=:idCourse', array(':idCourse'=>$idCourse))
	                               ->queryColumn();

	    foreach($userIdsThisIteration as $userId){
		    if(!in_array($userId, $usersInCourseAlready)){
			    $usersNewToCourseInThisProgressiveIteration[] = $userId;
		    }
	    }

        //Enrolling the users
        foreach ($userIdsThisIteration as $idUser) {
            $user = new WebinarSessionUser();
            $user->id_session = $idSession;
            $user->id_user = $idUser;
            $user->status = WebinarSessionUser::$SESSION_USER_SUBSCRIBED;

			if(!CoreUser::adminRuleEnabled(Yii::app()->user->id, 'direct_course_subscribe') && Yii::app()->user->getIsPu())
				$user->status = WebinarSessionUser::$SESSION_USER_WAITING_LIST;

            $user->date_subscribed = Yii::app()->localtime->toLocalDateTime();
            if($user->save(false)){
                Yii::app()->event->raise(EventManager::EVENT_USER_ENROLLED_IN_WEBINAR_SESSION, new DEvent($this, array(
                    'user' => $idUser,
                    'session_id' => $idSession,
                    'course' => LearningCourseuser::model()->findByAttributes(array(
                        'idUser' => $idUser,
                        'idCourse' => $idCourse
                    ))
                )));
            }
        }

        if(count($usersNewToCourseInThisProgressiveIteration) && $this->_courseModel->selling && CoreUserPU::isPUAndSeatManager()){
            // The current user is a PU only allowed to enroll
            // users in courses using "seats", so decrease his available
            // seats in the course by the amount of users he enrolled
	        CoreUserPU::modifyAvailableSeats($idCourse, -count($usersNewToCourseInThisProgressiveIteration));
        }

        $completedPercent = $totalCount==0 ? 100 : $index/$totalCount*100;
        if($completedPercent>100) $completedPercent = 100;
        if($completedPercent<0) $completedPercent = 0;

        $html = $this->renderPartial('_progressive_enroll_users_to_webinar_session', array(
            'offset' 				=> $offset,
            'processedUsers' 		=> $index,
            'totalUsers' 			=> $totalCount,
            'completedPercent' 		=> intval($completedPercent),
            'start' 				=> ($offset == 0),
            'stop' 					=> $stop,
            'afterUpdateCallback'	=> ($stop ? 'var gridId = "session-enrollment-list"; $.fn.yiiListView.update(gridId);' : null)
        ), true, true);

        // Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
        // These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stopping when $stop == true)
        $data = array(
            'offset'				=> $newOffset,
            'stop'					=> $stop,
            'users'					=> $users,
            'idCourse'				=> $idCourse,
            'idSession'             => $idSession
        );

        $response = new AjaxResult();
        $response->setStatus(true);
        $response->setHtml($html)->setData($data)->toJSON();
        Yii::app()->end();
    }




	/**
	 * Controller that shows the user/group/orgchart selector to enroll users in a session
	 * @throws CException
	 */
	public function actionEnroll() {

		$rq = Yii::app()->request;

		$idCourse = $rq->getParam('course_id', 0);
		$idSession = $rq->getParam('id_session', 0);

		if ($rq->isAjaxRequest) {
			$result = array();

			$userModel = CoreUser::model();
			$userModel->scenario = 'search';
			$userModel->unsetAttributes();
			if (isset($_REQUEST['CoreUser'])) {
				$userModel->attributes = $_REQUEST['CoreUser'];
			}

			$userlist = array();

			if (isset($_REQUEST['user-grid-selected-items'])) {
				if ($_REQUEST['user-grid-selected-items'] != '') {
					$userlist = explode(',', $_REQUEST['user-grid-selected-items']);
				}
			}

			if (isset($_REQUEST['courseEnroll-orgchart'])) {
				$orgChartGroups = $_REQUEST['courseEnroll-orgchart'];

				$allNodes = array();
				// Get the root node to handle the "allnodes" case
				$rootOrgNode = CoreOrgChartTree::getOrgRootNode();
				foreach ($orgChartGroups as $id => $value) {
					if (($id == $rootOrgNode->idOrg) && ($value != '0')) {
						$allNodes = 'allnodes';
						break;
					}
					if ($value == '1') {
						if (!in_array($id, $allNodes)) {
							$allNodes[] = $id;
						}
					} elseif ($value == '2') {
						$allChildrenNodesIds = CoreUser::model()->getCurrentAndChildrenNodesList($id);
						if (!empty($allChildrenNodesIds)) {
							foreach ($allChildrenNodesIds as $childId) {
								if (!in_array($childId, $allNodes)) {
									$allNodes[] = $childId;
								}
							}
						}
					}
				}

				if (!empty($allNodes)) {
					$criteria = new CDbCriteria();

					// criteria for all users not in current course
					$criteriaAll = CoreUser::model()->dataProviderEnroll(Yii::app()->session['currentCourseId'])->criteria;

					if (($allNodes !== 'allnodes') && (is_array($allNodes))) {
						// criteria for selected nodes
						$criteria->with = array(
							'orgChartGroups' => array(
								'joinType' => 'INNER JOIN',
								'condition' => 'orgChartGroups.idOrg IN (\''.implode('\',\'', $allNodes).'\')',
							),
							'learningCourseusers' => $criteriaAll->with['learningCourseusers'],
						);
						$criteria->together = true;
					} else {
						$criteria = $criteriaAll;
					}

					$users = CoreUser::model()->findAll($criteria);
					foreach ($users as $user) {
						if (!in_array($user->idst, $userlist)) {
							$userlist[] = $user->idst;
						}
					}
				}
			}

			$groupModel = new CoreGroup();
			if (isset($_REQUEST['CoreGroup'])) {
				$groupModel->attributes = $_REQUEST['CoreGroup'];
			}

			if (isset($_REQUEST['group-grid-selected-items'])) {
				if ($_REQUEST['group-grid-selected-items'] != '') {
					$grouplist = $_REQUEST['group-grid-selected-items'];
					$criteria = new CDbCriteria();
					$criteria->with = array(
						'groups' => array(
							'joinType' => 'INNER JOIN',
							'condition' => 'groups.idst IN ('.$grouplist.')',
						),
					);
					$criteria->together = true;
					$users = CoreUser::model()->findAll($criteria);

					foreach ($users as $user) {
						if (!in_array($user->idst, $userlist)) {
							$userlist[] = $user->idst;
						}
					}
					Yii::app()->session['newsletterGroupList'] = explode(',', $_REQUEST['group-grid-selected-items']);
				}
			}

			// save selected users
			if (!empty($userlist)) {
				$sessionModel = WebinarSession::model()->findByPk($idSession);
				if ($sessionModel->course_id != $idCourse)
					throw new CHttpException('Invalid specified session (id : '.$idSession.')');

				$sessionUsers = array();
				foreach ($sessionModel->getEnrolledUsers() as $enrolledUser)
					$sessionUsers[] = $enrolledUser['id_user'];

				$toEnroll = array();
				foreach ($userlist as $userId) {
					if (!in_array($userId, $sessionUsers)) {
						$toEnroll[] = $userId;
					}
				}

				if (($sessionModel->countEnrolledUsers() + count($toEnroll)) > $sessionModel->max_enroll) {
					//limit of enrollable users has been reached
					$this->sendJSON(array('status' => 'error', 'message' => Yii::t('classroom', 'Limit of max enrollable users reached')));

				} else {

					$idCourse = $this->_sessionModel->course_id;

					// Try to find out which unique users we are enrolling now are enrolled
					// in a session in the course for the first time (exclude them if they
					// are enrolled to another session in this course already)
					// This is important for decreasing Power User's seats in the course
					// by those number of users
					$usersNewToCourse = array();
					$usersInCourseAlready = Yii::app()->getDb()->createCommand()
						->selectDistinct('wsu.id_user')
						->from(WebinarSessionUser::model()->tableName().' wsu')
						->join(WebinarSession::model()->tableName().' cs', 'cs.id_session=wsu.id_session AND cs.course_id=:idCourse', array(':idCourse'=>$idCourse))
						->queryColumn();

					foreach($toEnroll as $userId){
						if(!in_array($userId, $usersInCourseAlready)){
							$usersNewToCourse[] = $userId;
						}
					}

					if(count($usersNewToCourse) && $this->_courseModel->selling && CoreUserPU::isPUAndSeatManager()) {
						// The current user is a Power user and he is
						// allowed to enroll via "seats" only.
						// Check if he has enough available for this course
						if ( ! CoreUserPuCourse::checkIfPuHasEnoughSeatsInCourse( count( $usersNewToCourse ), $idCourse ) ) {
							$this->sendJSON(array('status' => 'error', 'message' => Yii::t('standard', "You don't have enough seats for this course")));
						}
					}

					foreach ($toEnroll as $userId) {
						$user = new WebinarSessionUser();
						$user->id_session = $idSession;
						$user->id_user = $userId;
						$user->status = WebinarSessionUser::$SESSION_USER_SUBSCRIBED;

						if(!CoreUser::adminRuleEnabled(Yii::app()->user->id, 'direct_course_subscribe') && Yii::app()->user->getIsPu())
							$user->status = WebinarSessionUser::$SESSION_USER_WAITING_LIST;

						$user->date_subscribed = Yii::app()->localtime->toLocalDateTime();
						if($user->save(false)){
                            Yii::app()->event->raise(EventManager::EVENT_USER_ENROLLED_IN_WEBINAR_SESSION, new DEvent($this, array(
                                'enrollment' => $user
                            )));
						}
					}

					if(count($usersNewToCourse) && $this->_courseModel->selling && CoreUserPU::isPUAndSeatManager()){
						// The current user is a PU only allowed to enroll
						// users in courses using "seats", so decrease his available
						// seats in the course by the amount of users he enrolled
						$puCourseModel = CoreUserPuCourse::model()->findByAttributes(array(
							'puser_id'=>Yii::app()->user->getIdst(),
							'course_id'=>$idCourse,
						));
						if($puCourseModel){
							$puCourseModel->available_seats = intval($puCourseModel->available_seats) - count($usersNewToCourse);
							$puCourseModel->save();
						}
					}

					$this->sendJSON(array('status' => 'saved'));
				}
			}

			$processOutput = false;
			if (isset($_REQUEST['popupFirstShow']) && $_REQUEST['popupFirstShow'] === '1') {
				$processOutput = true;
			}

			$fancyTreeData = CoreOrgChartTree::buildFancytreeDataArray(false, true, true);
			$result['html'] = $this->renderPartial('assignUser', array(
				'userModel' => $userModel,
				'groupModel' => $groupModel,
				'idCourse' => $idCourse,
				'idSession' => $idSession,
				'fancyTreeData' => $fancyTreeData
			), true, true);

			if (isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html')) {
				echo $result['html'];
			} else {
				$this->sendJSON($result);
			}
		}
	}

	/**
	 * Action that handles the edit user enrollment (in session) modal
	 */
	public function actionEditUser() {
		$result = array();

		if(Yii::app()->user->getIsPu() && !Yii::app()->user->checkAccess("/lms/admin/webinarsessions/mod") && !Yii::app()->user->checkAccess("/lms/admin/webinarsessions/assign")){
			if($this->_sessionModel->created_by != Yii::app()->user->getIdst()){
				throw new CException('Power users can only manage their own sessions');
			}
		}

		try {

			// Check input params
			$rq = Yii::app()->request;

			// - User id
			$idUser = $rq->getParam('id_user', 0);
			$user = CoreUser::model()->findByPk($idUser);
			if(!$user)
				throw new Exception("Wrong user id");

			// - Session id
			$idSession = $rq->getParam('id_session', 0);
			if (!$idSession)
				throw new Exception("Invalid session id");

			// Check user is subscribed to this classroom course
			$courseUserModel = LearningCourseuser::model()->findByAttributes(array(
				'idUser' => $idUser,
				'idCourse' => $this->_sessionModel->course_id
			));
			if (!$courseUserModel)
				throw new Exception("User is not subscribed to this course");

			// Check user is subscribed to this session
			$sessionUserModel = WebinarSessionUser::model()->findByAttributes(array(
				'id_user' => $idUser,
				'id_session' => $idSession
			));
			if (!$sessionUserModel)
				throw new Exception("User is not subscribed to this session");

			// Are we saving a user enrollment?
			if ($rq->getParam('confirm', false) !== false) {
				$input1 = $rq->getParam('LearningCourseuser', false);
				$input2 = $rq->getParam('WebinarSessionUser', false);
				if (!is_array($input1) || !is_array($input2))
					throw new Exception("Invalid form values submitted");



				// Save level in learning_courseuser
				$newLevel = $input1['level'];
				$courseUserModel->level = $newLevel;
				if (!$courseUserModel->save())
					throw new Exception(implode('\n',array_values($courseUserModel->getErrors())));

				// Save status in learning_courseuser_session
				$newStatus = $input2['status'];
				$sessionUserModel->status = $newStatus;

				if (!$sessionUserModel->save())
					throw new Exception(implode('\n',array_values($sessionUserModel->getErrors())));


				if(isset($sessionUserModel->status )){
					if ($sessionUserModel->status  == WebinarSessionUser::$SESSION_USER_WAITING_LIST ) {
						if ($sessionUserModel->hasAttribute('waiting')) { $sessionUserModel->waiting = 1; } // NOTE: "waiting" column is deprecated for "webinar_session_user" table. Use "status" instead.
						$sessionUserModel->save(false);

						$event = new DEvent($this, array(
							'enrollment' => $sessionUserModel,
							'session_id' => $sessionUserModel->id_session,
							'user' => $user->idst
						));
						Yii::app()->event->raise(EventManager::EVENT_USER_WEBINAR_SESSION_WAITING_FOR_APPROVAL, $event);

					} elseif ($sessionUserModel->status  == WebinarSessionUser::$SESSION_USER_CONFIRMED) {
						$courseUserModel->waiting = 1;
						if ($courseUserModel->save(false)) {
							//---
							//we have to unenroll the users from ALL session in which it may be enrolled (but not the course, of course!)
							$query = "SELECT wsu.id_session "
								." FROM ".WebinarSession::model()->tableName()." ws "
								." JOIN ".WebinarSessionUser::model()->tableName()." wsu ON (wsu.id_session = ws.id_session AND ws.course_id = :id_course)"
								." JOIN ".LearningCourseuser::model()->tableName()." cu ON (cu.idCourse = ws.course_id AND wsu.id_user = cu.idUser AND cu.idUser = :id_user) ";
							$command = Yii::app()->db->createCommand($query); /* @var $command CDbCommand */
							if (!$command) { throw new CException(Yii::t('standard', '_OPERATION_FAILURE')); }
							$otherUserSessions = $command->queryAll(true, array(
								':id_user' => $idUser,
								':id_course' => $this->_sessionModel->course_id
							));
							if (is_array($otherUserSessions) && !empty($otherUserSessions)) {
								foreach ($otherUserSessions as $otherUserSession) {
									if ($otherUserSession['id_session'] != $sessionUserModel->id_session) {
										$otherSessionUserModel = WebinarSessionUser::model()->findByAttributes(array(
											'id_user' => $idUser,
											'id_session' => $otherUserSession['id_session']
										));
										if ($otherSessionUserModel) {
											if (!$otherSessionUserModel->delete()) {
												$courseUserModel->waiting = 0;
												$courseUserModel->save(false);
												throw new Exception(implode('\n', array_values($otherSessionUserModel->getErrors())));
											}
										}
									}
								}
							}
							//---
							if (!$sessionUserModel->delete()) {
								$courseUserModel->waiting = 0;
								$courseUserModel->save(false);
								throw new Exception(implode('\n',array_values($sessionUserModel->getErrors())));
							}
						}
					}
				}


				$result = array('status' => 'saved', 'message' => '');

			} else {
				// Just show the edit form
				$html = $this->renderPartial('_editUser', array(
					'sessionUserModel' => $sessionUserModel,
					'courseUserModel' => $courseUserModel
				), true);

				$result = array('html' => $html);
			}
		} catch(CException $ex) {
			$message = Yii::t('classroom', $ex->getMessage());
			$result = array('status' => 'error', 'message' => $message);
		} catch(Exception $e) {
			$message = Yii::t('standard', '_OPERATION_FAILURE');
			$result = array('status' => 'error', 'message' => $message);
		}

		$this->sendJSON($result);
		Yii::app()->end();
	}

	/**
	 * Controller action that user can self-unenroll from a webinar
	 */
	public function actionWebinarSelfUnenroll() {

		try {

			// Check input params
			$rq = Yii::app()->request;

			// - User id
			$idUser = Yii::app()->user->getIdst();
			$user = CoreUser::model()->findByPk($idUser);
			if(!$user)
				throw new Exception("Wrong user id");

			// - Session id
			$idSession = $rq->getParam('id_session', 0);
			if (!$idSession)
				throw new Exception("Invalid session id");

			// Check user is subscribed to this classroom course
			$courseUserModel = LearningCourseuser::model()->findByAttributes(array(
				'idUser' => $idUser,
				'idCourse' => $this->_sessionModel->course_id
			));
			if (!$courseUserModel)
				throw new Exception("You are not subscribed to this course");

			// Check user is subscribed to this session
			$sessionUserModel = WebinarSessionUser::model()->findByAttributes(array(
				'id_user' => $idUser,
				'id_session' => $idSession
			));
			if (!$sessionUserModel)
				throw new Exception("You are not subscribed to this session");

			// Are we removing a user enrollment?
			$confirm = $rq->getParam('confirm', false);
			if (isset($confirm) && $confirm) {

				Yii::app()->event->raise(EventManager::EVENT_USER_UNENROLLED_FROM_WEBINAR_SESSION, new DEvent($this, array(
					'user_id' => $user->idst,
					'session' => $idSession,
					'course' => $courseUserModel
				)));

				//check seats management
				if ($this->_courseModel->selling && CoreUserPU::isPUAndSeatManager()) {
					//$userAlreadyAttendedCourse = $courseUserModel->status != LearningCourseuser::$COURSE_USER_SUBSCRIBED;
					$userAlreadyAttendedSession = false;
					if ($sessionUserModel->status == WebinarSessionUser::$SESSION_USER_SUSPEND) {
						if ($this->_sessionModel->userHasAlreadyPlayedSomeCourseLOs($idUser)) {
							$userAlreadyAttendedSession = true;
						}
					} else if ($sessionUserModel->status != WebinarSessionUser::$SESSION_USER_SUBSCRIBED) {
						$userAlreadyAttendedSession = true;
					}
					if ($userAlreadyAttendedSession) {
						throw new CException(Yii::t('standard', 'Can not unenroll users who have already used their seat'));
					}
				}

				$sessionUserModel->scenario = 'selfUnEnroll';
				// Remove user from learning_courseuser_session
				if (!$sessionUserModel->delete())
					throw new Exception(implode('\n',array_values($sessionUserModel->getErrors())));

//				AUTOMATICALY ENROLL WAITING USERS EXECUTION
				if($this->courseModel->allow_automatically_enroll == 1){
					$this->courseModel->automaticallyEnroll(LearningCourse::TYPE_WEBINAR, $this->_sessionModel, 1);
				}
//				AUTOMATICALY ENROLL WAITING USERS EXECUTION

				Yii::app()->event->raise(EventManager::EVENT_USER_UNENROLLED_FROM_WEBINAR_SESSION, new DEvent($this, array(
					'enrollment' => $sessionUserModel
				)));

				$sql = "
					SELECT COUNT(*) AS webinar_enrollment_count
					FROM `webinar_session` AS ws
					JOIN `webinar_session_user` wsu
					ON (ws.id_session = wsu.id_session)
					WHERE wsu.id_user = :user_id AND ws.course_id = :course_id
				";

				$webinarCountQuery = Yii::app()->db->createCommand($sql);
				$webinarCountQuery->bindParam(':user_id', $idUser);
				$webinarCountQuery->bindParam(':course_id', $this->_sessionModel->course_id);
				$userCountWebinarEnrolled = $webinarCountQuery->queryScalar();
				$enrolledIn = (int)$userCountWebinarEnrolled;

				if($enrolledIn > 0){
					$this->renderPartial('lms.protected.views.site._js_redirect', array('url' => Docebo::createLmsUrl("webinar/session/select", array('course_id' => $this->_sessionModel->course_id))));
				}else{
					$courseUserModel->status = -2;
					$courseUserModel->save();
					$this->renderPartial('lms.protected.views.site._js_redirect', array('url' => Docebo::createLmsUrl("webinar/session/enroll", array('course_id' => $this->_sessionModel->course_id))));
				}

			} else {
				// Just show the remove form
				$showAutoEnrollMessage = ($this->courseModel->getFreePositions($this->_sessionModel, 1) > 0);

				echo $this->renderPartial('_self_webinar_unenroll', array('sessionUserModel' => $sessionUserModel, 'showAutoEnrollMessage' => $showAutoEnrollMessage), true);

			}
		} catch(CException $ex) {
			$message = Yii::t('classroom', $ex->getMessage());
			throw new Exception($message);
		} catch(Exception $e) {
			$message = Yii::t('standard', '_OPERATION_FAILURE');
			throw new Exception($message);
		}

		Yii::app()->end();
	}

	/**
	 * Controller action that unenrolls a single user from a session
	 */
	public function actionRemoveUser() {
		$result = array();

		if(Yii::app()->user->getIsPu() && !Yii::app()->user->checkAccess("/lms/admin/webinarsessions/mod") && !Yii::app()->user->checkAccess("/lms/admin/webinarsessions/assign")){
			if($this->_sessionModel->created_by != Yii::app()->user->getIdst()){
				throw new CException('Power users can only manage their own sessions');
			}
		}

		try {

			// Check input params
			$rq = Yii::app()->request;

			// - User id
			$idUser = $rq->getParam('id_user', 0);
			$user = CoreUser::model()->findByPk($idUser);
			if(!$user)
				throw new Exception("Wrong user id");

			// - Session id
			$idSession = $rq->getParam('id_session', 0);
			if (!$idSession)
				throw new Exception("Invalid session id");

			// Check user is subscribed to this classroom course
			$courseUserModel = LearningCourseuser::model()->findByAttributes(array(
				'idUser' => $idUser,
				'idCourse' => $this->_sessionModel->course_id
			));
			if (!$courseUserModel)
				throw new Exception("User is not subscribed to this course");

			// Check user is subscribed to this session
			$sessionUserModel = WebinarSessionUser::model()->findByAttributes(array(
				'id_user' => $idUser,
				'id_session' => $idSession
			));
			if (!$sessionUserModel)
				throw new Exception("User is not subscribed to this session");

			// Are we removing a user enrollment?
			$confirm = $rq->getParam('WebinarSessionUser', array());
			if (isset($confirm['confirm']) && ($confirm['confirm'] == true)) {

                Yii::app()->event->raise(EventManager::EVENT_USER_UNENROLLED_FROM_WEBINAR_SESSION, new DEvent($this, array(
                    'user_id' => $user->idst,
                    'session' => $idSession,
                    'course' => $courseUserModel
                )));

				//check seats management
				if ($this->_courseModel->selling && CoreUserPU::isPUAndSeatManager()) {
					//$userAlreadyAttendedCourse = $courseUserModel->status != LearningCourseuser::$COURSE_USER_SUBSCRIBED;
					$userAlreadyAttendedSession = false;
					if ($sessionUserModel->status == WebinarSessionUser::$SESSION_USER_SUSPEND) {
						if ($this->_sessionModel->userHasAlreadyPlayedSomeCourseLOs($idUser)) {
							$userAlreadyAttendedSession = true;
						}
					} else if ($sessionUserModel->status != WebinarSessionUser::$SESSION_USER_SUBSCRIBED) {
						$userAlreadyAttendedSession = true;
					}
					if ($userAlreadyAttendedSession) {
						throw new CException(Yii::t('standard', 'Can not unenroll users who have already used their seat'));
					}
				}

				//check possible learning plans enrollments
				if (PluginManager::isPluginActive('CurriculaApp')) {
					//check if user can be effectively unenrolled
					$lpEnrollments = LearningCoursepath::checkEnrollment($idUser, $this->_sessionModel->course_id);
					if (!empty($lpEnrollments)) {
						throw new CException('User assigned to learning plan(s) containing this course!');
					}
				}

				// Remove user from learning_courseuser_session
				if (!$sessionUserModel->delete())
					throw new Exception(implode('\n',array_values($sessionUserModel->getErrors())));

//				AUTOMATICALY ENROLL WAITING USERS EXECUTION
				if($this->courseModel->allow_automatically_enroll == 1){
					$this->courseModel->automaticallyEnroll(LearningCourse::TYPE_WEBINAR, $this->_sessionModel, 1);
				}
//				AUTOMATICALY ENROLL WAITING USERS EXECUTION

				$result = array('status' => 'saved', 'message' => '');

                Yii::app()->event->raise(EventManager::EVENT_USER_UNENROLLED_FROM_WEBINAR_SESSION, new DEvent($this, array(
						'enrollment' => $sessionUserModel
                )));

			} else {
				// Just show the remove form
				$showAutoEnrollMessage = ($this->courseModel->getFreePositions($this->_sessionModel, 1) > 0);

				$html = $this->renderPartial('_removeUser', array('sessionUserModel' => $sessionUserModel, 'showAutoEnrollMessage' => $showAutoEnrollMessage), true);
				$result = array('html' => $html);
			}
		} catch(CException $ex) {
			$message = Yii::t('classroom', $ex->getMessage());
			$result = array('status' => 'error', 'message' => $message);
		} catch(Exception $e) {
			$message = Yii::t('standard', '_OPERATION_FAILURE');
			$result = array('status' => 'error', 'message' => $message);
		}

		$this->sendJSON($result);
		Yii::app()->end();
	}


	/**
	 * Controller that unenrolls multiple removeUserMultiple from the session
	 */
	public function actionRemoveUserMultiple()
	{
		$result = array();

		try {
			// Check input params
			$rq = Yii::app()->request;

			// - Session id
			$idSession = $rq->getParam('id_session', 0);
			$idCourse = $this->getIdCourse();
			if (!$idSession)
				throw new Exception("Invalid session id");

			$excludedUsers = 0;

			//confirm button pressed
			$confirmParams =  $rq->getParam('WebinarSessionUser', array());
			$confirm = (isset($confirmParams['confirm']) && ($confirmParams['confirm'] == true));

			//other checks
			$usersStr = trim($rq->getParam('users', '')); //this is empty in the first step, it will be populated only when confirm button is pressed
			if (!$confirm) {
				$idsList = explode(',', trim($rq->getParam('ids', '')));
			} else {
				$idsList = explode(',', $usersStr);
			}
			if (PluginManager::isPluginActive('CurriculaApp')) {
				//check if and which enrollments can be performed due to LPs constraints
				$lpEnrollments = LearningCoursepath::checkEnrollmentsMultipleUsers($idsList, $idCourse);
				if (is_array($lpEnrollments) && !empty($lpEnrollments)) {
					$toBeExcluded = array();
					foreach ($lpEnrollments as $lpIdUser => $lpList) {
						if (!empty($lpList)) {
							$toBeExcluded[] = $lpIdUser;
						}
					}
					//update input list, removing non-unenrollable users
					if (!empty($toBeExcluded)) {
						$idsList = array_diff($idsList, $toBeExcluded);
						if ($confirm) { $usersStr = implode(',', $idsList); }
						$excludedUsers = count($toBeExcluded);
					}
					//check if any user is remaining to be unenrolled, otherwise send a message to the user
					if (empty($idsList)) {
						$this->sendJSON(array(
							'status' => 'error', // success|error
							'html' => Yii::t('curricula', '{n} selected user(s) have been assigned to a learning plan and cannot be unenrolled from course', array('{n}' => '<strong>'.count($toBeExcluded).'</strong>')),
						));
						Yii::app()->end();
					}
				}
			}

			// Are we removing user enrollments?
			if ($confirm) {

				if (!empty($usersStr)) {

                    foreach($idsList as $user_id) {
                        $courseUserModel = LearningCourseuser::model()->findByAttributes(array(
                            'idUser' => $user_id,
                            'idCourse' => $this->_sessionModel->course_id
                        ));
                        
                        Yii::app()->event->raise(EventManager::EVENT_USER_UNENROLLED_FROM_WEBINAR_SESSION, new DEvent($this, array(
                            'user_id' => $user_id,
                            'session' => $idSession,
                            'course' => $courseUserModel
                        )));
                    }

					// Progressive deleting users to webinar session
					$html = $this->renderPartial('_mass_delete_users_from_webinar_session', array(
						'users' => $usersStr, // Build string with the all users ids
						'idSession' => $idSession
					), true);

					$response = new AjaxResult();
					$response->setStatus(true);
					$response->setHtml($html . '<a href="auto-close"></a>')->toJSON();
					Yii::app()->end();
				}
			} else {
				// Just show the remove form
				$showAutoEnrollMessage = ($this->courseModel->getFreePositions($this->_sessionModel, count($idsList)) > 0);

				$html = $this->renderPartial('_removeUserMultiple', array(
					'sessionUserModel' => new WebinarSessionUser(),
					'users' => $idsList,
					'excludedUsers' => $excludedUsers,
					'idSession' => $idSession,
					'idCourse' => $idCourse,
					'showAutoEnrollMessage' => $showAutoEnrollMessage
				), true);
				$result = array('html' => $html);
			}
		} catch (CException $ex) {
			$message = Yii::t('classroom', $ex->getMessage());
			$result = array('status' => 'error', 'message' => $message);
		} catch (Exception $e) {
			$message = Yii::t('standard', '_OPERATION_FAILURE');
			$result = array('status' => 'error', 'message' => $message);
		}

		$this->sendJSON($result);
		Yii::app()->end();
	}




    /*
     * Progressively delete users from Webinar session
     * @throws CException
     */
    public function actionAxProgressiveDeleteUsersFromWebinarSession(){

        $chunkSize = 100;
        $tmpUsers = array();
        $users = $_POST['users'];
        $idSession = $_POST['idSession'];

	    $idCourse = Yii::app()->getDb()->createCommand()
		    ->select('course_id')
		    ->from(WebinarSession::model()->tableName())
		    ->where('id_session=:idSession', array(':idSession'=>$idSession))
		    ->queryScalar();

	    $isPuAndCourseSeatManager = CoreUserPU::isPUAndSeatManager();
	    $course = null;
		$sessionModel = null;

	    // Try to get the course from the session
	    if($idSession){
		    $sessionModel = WebinarSession::model()->with('course')->findByPk($idSession);
		    $course = $sessionModel ? $sessionModel->course : null;
	    }

	    if(!$course) throw new CException('Can not find the course of this webinar session');

        $db = Yii::app()->db;

        // Check if this is tring
        if(is_string($users))
        {
            // if is only one user
            if(!strpos($users,',') !== false)
                $userList = array($users);
            else
                $userList = explode(',', $users);
        }
        else{
            $this->sendJSON(array());
        }

        // Get current offset and calculate stop flag
        $offset = Yii::app()->request->getParam('offset', 0);
        $totalCount = count($userList);
        $newOffset = $offset + $chunkSize;
	    $start = ($offset==0); // first run
        $stop = ($newOffset < $totalCount) ? false : true;
        $counter = 0;

	    $failedToUnenrollDueToTakenSeat = explode(',', Yii::app()->request->getParam('failedToUnenrollDueToTakenSeat', null));

	    if($start && LearningCourse::isCourseSelling($idCourse) && $isPuAndCourseSeatManager && !CoreUserPuCourse::checkIfPuHasEnoughSeatsInCourse(1, $idCourse)){
		    // At the start of this progressive operation a Power User
		    // who is a "seat manager" had no seats available in this course
		    // When the progressive operation ends, check if he now has some
		    // seats and if yes, reload the UI after he closes the progressive
		    // dialog so he now sees updated UI (accessible button to enroll new
		    // users, since he now has seats)
		    Yii::app()->user->setState('reload_page_after_dialog_close', true);
	    }

        for ($index = $offset; ($index < $newOffset && $index < $totalCount); $index++) { //NOTE: $newOffset may be greater than $totalCount, but we wouldn't count over $totalCount
            $counter ++;
            if ( $counter > $chunkSize ) break; //NOTE: this shouldn't happen
            // Get current enrollment "job". If we are over the limit, invalidate value and stop this iteration
            $userId = ( isset( $userList[ $index ] ) ? $userList[ $index ] : false );
            if (!$userId || empty($userId))continue;
            $tmpUsers[] = $userId;
        }

        try {
            // Remove users from learning_courseuser_session in a transaction
            if ($db->getCurrentTransaction() === NULL)
                $transaction = $db->beginTransaction();

            // Deleting the users
            foreach ($tmpUsers as $idUser) {
                $sessionUserModel = WebinarSessionUser::model()->with(array('userModel'))->findByAttributes(array(
                    'id_user' => $idUser,
                    'id_session' => $idSession
                ));

	            $learningCourseuser = $sessionUserModel->getCourseuser();

	            if($isPuAndCourseSeatManager && LearningCourse::isCourseSelling($learningCourseuser->idCourse)){
		            // Is Power User a "seat manager"
		            if($learningCourseuser->status != LearningCourseuser::$COURSE_USER_SUBSCRIBED){
			            // Seat consumed, don't restore
			            // Seat consumed, can not delete
			            $failedToUnenrollDueToTakenSeat[] = $idUser;
			            continue; // skip this user deletion/unenrollment
		            }
	            }

	            if (!$sessionUserModel || !$sessionUserModel->delete())
		            throw new CException(implode('\n',array_values($sessionUserModel->getErrors())));
            }

            // Commit transaction
            if (isset($transaction))
                $transaction->commit();

        } catch (CException $e) {
            if (isset($transaction))
                $transaction->rollback();
	        Yii::log($e->getMessage(), CLogger::LEVEL_ERROR, __CLASS__);
	        $response = new AjaxResult();
	        $response->setStatus(true);
	        $response->setHtml($e->getMessage())->setData(array('stop'=>1))->toJSON();
	        Yii::app()->end();
        }

        $completedPercent = $totalCount==0 ? 100 : $index/$totalCount*100;
        if($completedPercent>100) $completedPercent = 100;
        if($completedPercent<0) $completedPercent = 0;


	    $failedUsersFullNames = array();
	    if($stop){
		    $criteria = new CDbCriteria();
		    $criteria->compare('idst', $failedToUnenrollDueToTakenSeat);
		    $usersFailed = CoreUser::model()->findAll($criteria);
		    foreach($usersFailed as $failedUser){
			    $failedUsersFullNames[] = $failedUser->getFullName();
		    }

			//AUTOMATICALY ENROLL WAITING USERS EXECUTION
			if($course->allow_automatically_enroll == 1){
				$course->automaticallyEnroll(LearningCourse::TYPE_WEBINAR, $sessionModel, $index);
			}
			//AUTOMATICALY ENROLL WAITING USERS EXECUTION
	    }

        $html = $this->renderPartial('_progressive_delete_users_from_webinar_session', array(
            'offset' 				=> $offset,
            'processedUsers' 		=> $index,
            'totalUsers' 			=> $totalCount,
            'completedPercent' 		=> intval($completedPercent),
            'start' 				=> ($offset == 0),
            'stop' 					=> $stop,
            'failedUsersFullNames'=>$failedUsersFullNames,
            'afterUpdateCallback'	=> ($stop ? 'var gridId = "session-enrollment-list"; updateSelectAllCounter(gridId,""); $.fn.yiiListView.update(gridId);' : null)
        ), true, true);

        // Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
        // These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stopping when $stop == true)
        $data = array(
            'offset'				=> $newOffset,
            'stop'					=> $stop,
            'users'					=> $users,
            'idSession'             => $idSession,
            'failedToUnenrollDueToTakenSeat'=>implode(',', $failedToUnenrollDueToTakenSeat),
        );

        $response = new AjaxResult();
        $response->setStatus(true);
        $response->setHtml($html)->setData($data)->toJSON();
        Yii::app()->end();
    }
}
