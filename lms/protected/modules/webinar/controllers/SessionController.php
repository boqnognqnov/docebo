<?php

class SessionController extends WebinarBaseController
{
	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules()
	{
		return array(
			array(
				'allow',
				'users' => array('@'),
				'expression' =>'(Yii::app()->user->getIsUser() && (LearningCourseuser::userLevel(Yii::app()->user->id, Yii::app()->request->getParam("course_id")) == LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR))',
				'actions' =>array('manageLearningObjects'),
			),
			array(
				'allow',
				'roles' => array('/lms/admin/course/mod'),
				'actions' => array('create', 'edit', 'save', 'select', 'enroll', 'manageLearningObjects', 'getAccountsByTool', 'edit', 'axCopySession', 'axDeleteSession', 'assign', 'searchAutoCompleteImportFromSession', 'axAssignUsers', 'axDeleteUnassigned'),
			),
			// Allow access to other actions only to logged-in users:
			array(
				'allow',
				'users' => array('@'),
			),
			array('deny', // if some permission is wrong -> the script goes here
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
				'message' => Yii::t('course', '_NOENTER'),
			),

		);
	}



	/**
	 * Before action controller
	 * @param CAction $action
	 * @return bool
	 */
	public function beforeAction($action)
	{
		$rq = Yii::app()->request;

		JsTrans::addCategories(array('player', 'classroom', 'error'));

		switch ($action->id) {
			case 'assign':
			case 'waiting':
				$selectedItems = $rq->getParam('selectedItems', '');
				if (Yii::app()->request->isAjaxRequest && !empty($selectedItems)) {
					Yii::app()->session['selectedItems'] = explode(',', $selectedItems);
				} else {
					Yii::app()->session['selectedItems'] = array();
				}
				break;
		}

		return parent::beforeAction($action);
	}

	public function actionCreate()
	{
		$transaction = Yii::app()->getDb()->beginTransaction();

		try {
			// Check that course model has been saved
			if (!$this->_courseModel)
				throw new Exception("Invalid course id provided.", 1);

			// Show session creation form
			$model = new WebinarSession('create');

			$model->course_id = $this->_courseModel->idCourse;

			if (isset($_POST['WebinarSession']))
			{
				$model->attributes = $_POST['WebinarSession'];

				if ($model->validate())
				{
					$webinarSaved = $model->save(false);

					if($webinarSaved)
					{
						$idSession = $model->id_session;

						$datesSaved = true;
						$sessionDateModels = array();
						foreach($_POST['dates'] as $date=>$dateAttributes){
							$dateModel = new WebinarSessionDate();
							$dateModel->setAttributes($dateAttributes);
							$dateModel->id_session = $idSession;

							if($dateModel->webinar_tool==WebinarSession::TOOL_CUSTOM){
								$dateModel->webinar_custom_url = $dateAttributes['webinar_custom_url'];
							}

							$res = $dateModel->save();
							$sessionDateModels[] = $dateModel;

							if(!$res){
								Yii::log(print_r($dateModel->getErrors(), true), CLogger::LEVEL_ERROR);
								throw new CException('Error saving webinar date '.$dateModel->day);
							}

							$sessionDateRecording = new WebinarSessionDateRecording();
							$sessionDateRecording->id_session = $idSession;
							$sessionDateRecording->day = $dateModel->day;
							$sessionDateRecording->id_course = $this->_courseModel->idCourse;
							$sessionDateRecording->type = WebinarSessionDateRecording::RECORDING_TYPE_NONE;
							$sessionDateRecording->path = '-';
							$sessionDateRecording->status = WebinarSessionDateRecording::RECORDING_STATUS_NON_EXISTANT;
							$sessionDateRecording->save();

							// even if one date fails, $datesSaved will turn to false
							$datesSaved &= $res;
						}

						if($datesSaved)
						{
							$dates =	Yii::app()->db->createCommand()
										->select("min(CONVERT_TZ(CONCAT(day,' ',time_begin), timezone_begin, 'UTC')) as date_begin, max(CONVERT_TZ(adddate(CONCAT(day,' ',time_begin), interval duration_minutes minute), timezone_begin, 'UTC')) as date_end")
										->from(WebinarSessionDate::model()->tableName())
										->where("id_session = :id_session", array(':id_session' => $model->id_session))
										->group("id_session")
										->queryRow();

							$updateModel = WebinarSession::model()->findByPk($model->id_session);

							$updateModel->date_begin = Yii::app()->localtime->toLocalDateTime($dates['date_begin']);
							$updateModel->date_end = Yii::app()->localtime->toLocalDateTime($dates['date_end']);

							$updateModel->save();

							Yii::app()->event->raise('WebinarSessionCreated', new DEvent($this, array(
								'session' => $model,
								'session_dates' => $sessionDateModels
							)));
							echo CJSON::encode(array(
								'success'=>true,
							));
							$transaction->commit();

							Yii::app()->end();
						}
						else
						{
							throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
						}
					}else{
						Yii::log('Failed to save webinar: '.print_r($model->getErrors(), true), CLogger::LEVEL_ERROR);
						throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
					}

					$transaction->commit();

					echo CJSON::encode(array(
						'success'=>false,
						'message'=>Yii::t('standard', '_OPERATION_FAILURE'),
					));
					Yii::app()->end();
				}else{
					$transaction->rollback();
					Yii::log(print_r($model->getErrors(), true), CLogger::LEVEL_ERROR);
					echo CJSON::encode(array(
						'success'=>false,
						'message'=>Yii::t('standard', '_OPERATION_FAILURE'),
					));
					Yii::app()->end();
				}
			}

			$this->renderPartial('manageSession', array(
				'model' => $model,
			));

			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			Yii::log($e->getMessage().'----'.$e->getTraceAsString(), CLogger::LEVEL_ERROR);

			echo CJSON::encode(array(
				'success'=>false,
				'message'=>$e->getMessage(),
			));
			Yii::app()->end();
		}
	}

	public function actionEdit()
	{

		$transaction = Yii::app()->getDb()->beginTransaction();

		try {
			// Check that course model has been saved
			if (!$this->_courseModel)
				throw new Exception("Invalid course id provided.", 1);

			// Show session creation form
			$model = $this->_sessionModel;
			$model->setScenario('update');

			if (isset($_POST['WebinarSession'])) {
				$oldMaxEnroll = $model->max_enroll;
				$model->attributes = $_POST['WebinarSession'];
				if ($model->validate())
				{
					$webinarSaved = $model->save(false);

					if($webinarSaved){
						$idSession = $model->id_session;

						$datesBegingSaved = array();
						foreach($_POST['dates'] as $date => $dateAttributes){
							$d = new DateTime($date.' '.$dateAttributes['time_begin'], new DateTimeZone($dateAttributes['timezone_begin']));
							$d->setTimezone(new DateTimeZone('UTC'));
							$datesBegingSaved[] = $d->format('Y-m-d H:i:s');
						}

						if(!empty($datesBegingSaved)) {
							$oldDatesNowBeingDeleted = new CDbCriteria();
							//delete only the dates that have changed
							$oldDatesNowBeingDeleted->addNotInCondition("CONVERT_TZ(concat(day,' ',time_begin), timezone_begin, 'UTC')", $datesBegingSaved );
							$oldDatesNowBeingDeleted->compare('id_session', $idSession);
							$dates = WebinarSessionDate::model()->findAll($oldDatesNowBeingDeleted);
							foreach($dates as $date){
								$date->delete();
							}
						}

						$datesSaved = true;
						foreach($_POST['dates'] as $date=>$dateAttributes){
							$dateModel = WebinarSessionDate::model()->findByAttributes(array('day'=>$date, 'id_session'=>$idSession));
							if(!$dateModel){
								$dateModel = new WebinarSessionDate();
								$dateModel->attributes = $dateAttributes;
							}else{
								// Preserve old params if we are updating a webinar date
								$oldWebinarToolParams = CJSON::decode($dateModel->webinar_tool_params);
								$newParams = array_merge($oldWebinarToolParams, CJSON::decode($dateAttributes['webinar_tool_params']));
								$dateModel->attributes = $dateAttributes;
								$dateModel->webinar_tool_params = $newParams;
							}
							$dateModel->id_session = $idSession;

							if($dateModel->webinar_tool==WebinarSession::TOOL_CUSTOM){
								$dateModel->webinar_custom_url = $dateAttributes['webinar_custom_url'];
							}

							$res = $dateModel->save();

							$sessionDateRecording = WebinarSessionDateRecording::model()->findByAttributes(array(
								'id_session' => $idSession,
								'day' => $dateModel->day
							));
							if(empty($sessionDateRecording)){
								$sessionDateRecording = new WebinarSessionDateRecording();
								$sessionDateRecording->id_session = $idSession;
								$sessionDateRecording->day = $dateModel->day;
								$sessionDateRecording->id_course = $this->_courseModel->idCourse;
								$sessionDateRecording->type = WebinarSessionDateRecording::RECORDING_TYPE_NONE;
								$sessionDateRecording->path = '-';
								$sessionDateRecording->status = WebinarSessionDateRecording::RECORDING_STATUS_NON_EXISTANT;
								$sessionDateRecording->save();
							}

							if(!$res){
								Yii::log(print_r($dateModel->getErrors(), true), CLogger::LEVEL_ERROR);
								throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
							}

							// even if one date fails, $datesSaved will turn to false
							$datesSaved &= $res;
						}

						if($datesSaved)
						{
							$dates =	Yii::app()->db->createCommand()
										->select("min(CONVERT_TZ(CONCAT(day,' ',time_begin), timezone_begin, 'UTC')) as date_begin, max(CONVERT_TZ(adddate(CONCAT(day,' ',time_begin), interval duration_minutes minute), timezone_begin, 'UTC')) as date_end")
										->from(WebinarSessionDate::model()->tableName())
										->where("id_session = :id_session", array(':id_session' => $model->id_session))
										->group("id_session")
										->queryRow();

							$model->date_begin = Yii::app()->localtime->toLocalDateTime($dates['date_begin']);
							$model->date_end = Yii::app()->localtime->toLocalDateTime($dates['date_end']);
							$model->save(false);

							//AUTOMATICALY ENROLL WAITING USERS EXECUTION
							if($this->_courseModel->allow_automatically_enroll == 1 && $model->max_enroll > $oldMaxEnroll){
								$this->_courseModel->automaticallyEnroll(LearningCourse::TYPE_WEBINAR, $this->_sessionModel, $model->max_enroll - $oldMaxEnroll);
							}
							//AUTOMATICALY ENROLL WAITING USERS EXECUTION

							Yii::app()->event->raise('WebinarSessionChanged', new DEvent($this, array(
								'session' => $model,
							)));
							echo CJSON::encode(array(
								'success'=>true,
							));
							$transaction->commit();

							Yii::app()->end();
						}
						else
						{
							Yii::log('Failed to save webinar session and dates', CLogger::LEVEL_ERROR);
							throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
						}
						echo '<a class="auto-close"></a>';
						Yii::app()->end();
					}
				}
			}
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			Yii::log($e->getMessage().'----'.$e->getTraceAsString(), CLogger::LEVEL_ERROR);

			echo CJSON::encode(array(
				'success'=>false,
				'message'=>$e->getMessage(),
			));
			Yii::app()->end();
		}

		$model->course_id = $this->_courseModel->idCourse;
		$this->renderPartial('manageSession', array(
			'model' => $model,
		));
	}

	/**
	 * Returns rendered account tools dropdown (used from ajax call)
	 * @param $tool
	 */
	public function actionGetAccountsByTool($tool)
	{
		$model = new WebinarSession('create');
		$id_session = Yii::app()->request->getParam('session', null);
		$date = Yii::app()->request->getParam('date', null);

		if ($id_session != null && $date != null) {
			$webinarSession = WebinarSessionDate::model()->findByAttributes(array(
				'day' => $date,
				'id_session' => $id_session,
			));
			if ($webinarSession) {
				$model->tool_params = $webinarSession->webinar_tool_params;
				$model->id_tool_account = $webinarSession->id_tool_account;
				$model->id_session = $webinarSession->id_session;
			}
		}

		$model->tool = $tool;


		$model->date = $date;
		$model->start = Yii::app()->request->getParam('time', null);
		$model->duration = Yii::app()->request->getParam('duration', null);

		$this->renderPartial('_tool_accounts', array(
			'model' => $model,
			'tool'=>$tool,
		));
	}

	/**
	 * Copy Session model
	 * @throws CException
	 * @throws CHttpException
	 */
	public function actionAxCopySession() {

		if (!$this->_sessionModel)
			throw new CHttpException("Invalid session id provided");

		$idCourse = $this->_sessionModel->course_id;
		$idSession = $this->_sessionModel->getPrimaryKey();

		$rq = Yii::app()->request;
		if ($rq->getParam('confirm', false) !== false) {
			// Form submission part
			try {
				$newName 	= $rq->getParam('new-name', '');
				$period = $rq->getParam('repeats', WebinarSession::PERIOD_DAY);
				$shift 	= $rq->getParam('shift', '0');
				if (empty($shift))
					$shift = '0';
				$interval = 'P' . $shift . $period;
				$newSession = $this->_sessionModel->copy($newName, $interval);

				if (!$newSession) {
					throw new CHttpException('Error while copying session');
				}else{
					if(Yii::app()->user->getIsPu()) {
						// Make the new session "owned" by the current user
						// if that user is a PU, so he can later edit/delete
						// this new session. Otherwise, if created_by is blank
						// it will be editable only by Godadmins and PU users with
						// course modification permissions
						$newSession->created_by = Yii::app()->user->getIdst();
						$newSession->save();
					}
				}

				echo '<a class="auto-close"></a>';
			} catch (CException $e) {
				$this->renderPartial('/_common/_errorMessage', array(
					'message' => $e->getMessage()
				));
			}
			Yii::app()->end();
		}

		//if no confirmation, then show copy session dialog
		$this->renderPartial('_copySession', array(
			'idCourse' => $idCourse,
			'idSession' => $idSession
		));
	}

	/**
	 * Copy Session model
	 * @throws CException
	 * @throws CHttpException
	 */
	public function actionAxCopySessionOld() {

		if (!$this->_sessionModel)
			throw new CHttpException("Invalid session id provided");

		$idCourse = $this->_sessionModel->course_id;
		$idSession = $this->_sessionModel->getPrimaryKey();

		$rq = Yii::app()->request;
		if ($rq->getParam('confirm', false) !== false) {
			// Form submission part
			try {
				$newName = $rq->getParam('new-name', '');

				$repeats = $rq->getParam('repeats', WebinarSession::REPEAT_DAILY);
				$numRepeats = $rq->getParam('num-repeats', 1);

				$numCopies = $rq->getParam('num-copies', 1);
				if ($numCopies <= 0)
					$numCopies = 1;

				for ($i = 1; $i <= $numCopies; $i++) {
					$daysShift = 0;
					switch ($repeats) {
						case WebinarSession::REPEAT_DAILY: {
							$daysShift = $i * $numRepeats;
						} break;
						case WebinarSession::REPEAT_WEEKLY: {
							$daysShift = $i * $numRepeats * 7;
						} break;
						case WebinarSession::REPEAT_MONTHLY: {
							$daysShift = $i * $numRepeats * 30; // TODO: is this right? we should consider 31 days and 28/29days months ...
						} break;
						case WebinarSession::REPEAT_YEARLY: {
							$daysShift = $i * $numRepeats * 365;
						} break;
					}
					$newSession = $this->_sessionModel->copy($newName, $daysShift);
					if (!$newSession) {
						throw new CHttpException('Error while copying session');
					}else{
						if(Yii::app()->user->getIsPu()) {
							// Make the new session "owned" by the current user
							// if that user is a PU, so he can later edit/delete
							// this new session. Otherwise, if created_by is blank
							// it will be editable only by Godadmins and PU users with
							// course modification permissions
							$newSession->created_by = Yii::app()->user->getIdst();
							$newSession->save();
						}
					}
				}

				echo '<a class="auto-close"></a>';
			} catch (CException $e) {
				$this->renderPartial('/_common/_errorMessage', array(
					'message' => $e->getMessage()
				));
			}
			Yii::app()->end();
		}

		//if no confirmation, then show copy session dialog
		$this->renderPartial('_copySession', array(
			'idCourse' => $idCourse,
			'idSession' => $idSession
		));
	}

	/**
	 * this action manages session deletion. It displays delete dialog and, if confirmed,
	 * it performs database operations
	 *
	 * @throws CException
	 */
	public function actionAxDeleteSession() {

		try {
			// Check session
			/* @var $session WebinarSession */
			$session = $this->_sessionModel;
			if (!$session)
				throw new CException("Wrong session provided");

			// Check if this is a form submit
			$rq = Yii::app()->request;
			if ($rq->getParam('confirm', false) !== false) {
				// This will call delete events, deleting also dates and subscriptions associated to this session
				$rs = $session->deleteSession();
				echo '<a class="auto-close"></a>';
			} else {
				// If no confirm, then we are opening the confirmation dialog
				echo $this->renderPartial('_deleteSessionConfirm', array(
					'model' => $session
				), true);
			}
		} catch (CException $e) {
			$this->renderPartial('/_common/_errorMessage', array(
				'message' => $e->getMessage()
			));
		}

		Yii::app()->end();
	}

	/*
	 * Assign unassigned users to a session
	 */

	public function actionAssign() {

		/* @var $courseModel LearningCourse */
		$courseModel = LearningCourse::model()->findByPk($this->getIdCourse());
		$params = array();
		$params['applyPowerUserFilter'] = true;
		if (($data = Yii::app()->request->getParam('data')) != null) {

			if ($data['autocomplete']) {
				$params['user_query'] = $data['query'];
			}
			if(!$courseModel && isset($data['course_id']) && $data['course_id']){
				$courseModel = LearningCourse::model()->findByPk($data['course_id']);
			}
			$result = array('options' => array());


			if($courseModel){
				$params['courseId'] = $courseModel->idCourse;
				$courseDataProvider = $courseModel->unassignedWebinarUsersDataProvider($params);
				$sessionDataProvider = WebinarSession::model()->dataProviderUserWaiting($params);
				$providerParams = array(
					'keyField' => false,
				);

				$dataProvider = $this->mergeCourseSessionProviders($courseDataProvider, $sessionDataProvider, $providerParams , true);
				$dataProvider->pagination = false;
				foreach ($dataProvider->getData() as $waitingUser) {
					if($waitingUser instanceof LearningCourseuser){
						$user = $waitingUser->learningUser;
					}elseif ($waitingUser instanceof WebinarSessionUser){
						$user = $waitingUser->userModel;
					}
					$username = Yii::app()->user->getRelativeUsername($user->userid);
					$text = "{$username}";

					$names = array();
					if (!empty($user->firstname))
						$names[] = $user->firstname;
					if (!empty($user->lastname))
						$names[] = $user->lastname;
					$names = join(' ', $names);
					if ($names)
						$text .= " - $names";

					$result['options'][$username] = $text;
				}
			}
			$this->sendJSON($result);
		}

		$params['user_search'] = Yii::app()->request->getParam('searchText', '');
		$courseDataProvider = $courseModel->unassignedWebinarUsersDataProvider($params);
		$providerParams = array(
			'keyField' => false,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			)
		);


		$params = array();
		$params['applyPowerUserFilter'] = true;
		$params['user_search'] = Yii::app()->request->getParam('searchText', '');
		$params['courseId'] = $this->getIdCourse() ;
		$sessionDataProvider= WebinarSession::model()->dataProviderUserWaiting($params);
		$dataProvider = $this->mergeCourseSessionProviders($courseDataProvider, $sessionDataProvider, $providerParams);
		$this->render('assign', array(
			'dataProvider' => $dataProvider,
			'course' => $courseModel,
			'permissions' => array(
				'assign' => (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('enrollment/create')),
				'del' => (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('enrollment/delete'))
			)
		));
	}


	/**
	 * Merging both providers Course and Session and filter them
	 */
	private function mergeCourseSessionProviders(CActiveDataProvider $courseProvider, CActiveDataProvider $sessionProvider, $providerParams = array() ,$autocomplete = false){
		$sessionProv = array();

		$courseProvider->pagination = false;
		$sessionProvider->pagination = false;
		// If Course Waiting list (checkbox filter is checked)
		$courseList = Yii::app()->request->getParam('courseList', 0);
		if($courseList && !$autocomplete){
			Yii::app()->session['webinarCourseWaitingFilter'] = 1;
		}elseif( !$autocomplete){
			Yii::app()->session['webinarCourseWaitingFilter'] = 0;
		}


		// If Session Waiting list (checkbox filter is checked)
		$sessionList = Yii::app()->request->getParam('sessionList', 0);
		if($sessionList && !$autocomplete){
			Yii::app()->session['webinarSessionWaitingFilter'] = 1;
		}elseif(!$autocomplete){
			Yii::app()->session['webinarSessionWaitingFilter'] = 0;
		}

		// By default we're showing only the course waiting list
		$courseProv = $courseProvider->getData();
		// Here we filter which provider should be loaded

		if(($sessionList == 1 && !$autocomplete) || (isset(Yii::app()->session['webinarSessionWaitingFilter']) && Yii::app()->session['webinarSessionWaitingFilter']  && $autocomplete)){
			$sessionProv = $sessionProvider->getData();
		}

		if(Yii::app()->request->isAjaxRequest  ){
			if(($courseList == 0 && !$autocomplete) || ((isset(Yii::app()->session['webinarCourseWaitingFilter']) && !Yii::app()->session['webinarCourseWaitingFilter']) && $autocomplete)){
				$courseProv = array();
			}
		}

		$mergedProviders = CMap::mergeArray(
			$courseProv,
			$sessionProv
		);

		return new CArrayDataProvider($mergedProviders, $providerParams);

	}

		protected function sessionAssignGridRenderAssign($data, $index) {
			if (!Yii::app()->user->getIsGodadmin() && !Yii::app()->user->checkAccess("enrollment/create")) { return ''; }
			if (!Yii::app()->user->getIsGodadmin() && !Yii::app()->user->checkAccess("/lms/admin/course/moderate")) { return ''; }
		$url = $this->createUrl('axAssignUsers', array(
			'id_user' => $data->idUser,
			'id_course' => $this->getIdCourse()
		));
		return CHtml::link(Yii::t('standard', '_ASSIGN'), $url, array(
			'class' => 'custom-grid-link open-dialog',
			'data-dialog-class' => 'modal-session-assign-users'
		));
	}

	/**
	 * Autocomplete info for import enrollments dialog (it differs from previous autocomplete,
	 * since we have to exclude our own id_session from research)
	 */
	public function actionSearchAutoCompleteImportFromSession() {

		$inputData = Yii::app()->request->getParam('data', array());
		$output = array();

		if (!empty($inputData)) {
			$idCourse = $inputData['course_id'];
			$idSession = $inputData['id_session'];
			$searchQuery = $inputData['query'];

			$output = array();
			$criteria = new CDbCriteria();
			$criteria->addCondition("course_id = :id_course");
			$criteria->params[':id_course'] = $idCourse;
			$criteria->addCondition("id_session <> :id_session");
			$criteria->params[':id_session'] = $idSession;
			$criteria->addCondition("name LIKE :search");
			$criteria->params[':search'] = '%' . $searchQuery . '%';

			if (Yii::app()->user->getIsPu()){
				$criteria->addCondition("created_by = :idCreator");
				$criteria->params[':idCreator'] = Yii::app()->user->id;
			}

			$sessions = WebinarSession::model()->findAll($criteria);
			if (!empty($sessions)) {
				foreach ($sessions as $session) {
					//avoid sessions without enrollments
					if ($session->countEnrolledUsers() > 0)
						$output[] = $session->name;
				}
			}
		}

		$this->sendJSON(array('options' => $output));
	}

	public function actionAxAssignUsers() {
		$idCourse = Yii::app()->request->getParam('id_course');
		$course = LearningCourse::model()->findByPk($idCourse);

		$isBulkAction = Yii::app()->request->getParam('isBulkAction');

		if (($sessionId = intval(Yii::app()->request->getParam('id_session'))) > 0) {

			$userIds = array();
			$selectedUsers = Yii::app()->request->getParam('selected_users');

			if (empty($selectedUsers)) {
				$userIds[] = Yii::app()->request->getParam('id_user');
			} else {
				$userIds = explode(',', $selectedUsers);
			}

			// If a user is already in learning_courseuser, it's in waiting list and it should NOT take seat
			// This is important for decreasing Power User's seats in the course
			// by those number of users
			$usersNewToCourse = array();
			$usersInCourseAlready = LearningCourseuser::getEnrolledByCourse($idCourse);

			foreach($userIds as $userId){
				if(!in_array($userId, $usersInCourseAlready)){
					$usersNewToCourse[] = $userId;
				}
			}

			//check the session max users
			if(!WebinarSession::checkIfEnrollmentsAreAvailable($sessionId, count($userIds))){
				echo Yii::t('classroom', "Limit of max enrollable users reached");
				Yii::app()->end();
			}

			if(count($usersNewToCourse) && Yii::app()->user->getIsPu() && CoreUser::adminRuleEnabled(Yii::app()->user->id, 'allow_buy_seats')) {
				// The current user is a Power user and he is
				// allowed to enroll via "seats" only.
				// Check if he has enough available for this course
				if (!CoreUserPuCourse::checkIfPuHasEnoughSeatsInCourse( count( $usersNewToCourse ), $idCourse ) ) {
					echo Yii::t('standard', "You don't have enough seats for this course");
					Yii::app()->end();
				}
			}

			foreach ($userIds as $userId) {
				if (WebinarSessionUser::enrollUser($userId, $sessionId)) {

					//We are no more in "waiting" status for the course
					$courseUserModel = LearningCourseUser::model()->findByAttributes(array(
						'idUser' => $userId,
						'idCourse' => $idCourse
					));
					if ($courseUserModel && $courseUserModel->waiting > 0) {
						$courseUserModel->waiting = 0;
						$courseUserModel->save(false);
					}

					$userModel = CoreUser::model()->findByPk($userId);
					if($userModel){
						Yii::app()->event->raise('UserApproval', new DEvent($this, array(
							'course' => $course,
							'user' => $userModel,
						)));
					}
				}
			}

			if(count($usersNewToCourse) && Yii::app()->user->getIsPu() && CoreUser::adminRuleEnabled(Yii::app()->user->id, 'allow_buy_seats')){
				// The current user is a PU only allowed to enroll
				// users in courses using "seats", so decrease his available
				// seats in the course by the amount of users he enrolled
				$puCourseModel = CoreUserPuCourse::model()->findByAttributes(array(
					'puser_id'=>Yii::app()->user->getIdst(),
					'course_id'=>$idCourse,
				));
				if($puCourseModel){
					$puCourseModel->available_seats = intval($puCourseModel->available_seats) - count($usersNewToCourse);
					$puCourseModel->save();
				}
			}

			// close dialog and reload grid
			echo '<a href="#" class="auto-close" data-unselect="'.implode(',', $userIds).'">></a>';
			Yii::app()->end();
		}

		echo $this->renderPartial('_modal_assign', array(
			'course' => $course,
			'isBulkAction' => ($isBulkAction)
		), true, true);
	}

	/**
	 * Bulk delete the selected users
	 */
	public function actionAxDeleteUnassigned() {
		/* @var $rq CHttpRequest */
		$rq = Yii::app()->request;

		$idCourse = $rq->getParam('id_course');
		$selectedUsers = Yii::app()->request->getParam('selected_users');
		$userIds = explode(',', $selectedUsers);

		$lpUserIds = array();
		if (PluginManager::isPluginActive('CurriculaApp')) {
			if (LearningCoursepathCourses::isCourseAssignedToLearningPaths($idCourse)) {
				$lpAssignments = LearningCoursepath::checkEnrollmentsMultipleUsers($userIds, $idCourse);
				foreach ($lpAssignments as $lpIdUser => $lpList) {
					if (!empty($lpList)) {
						$toBeExcluded[] = $lpIdUser;
					}
				}
				//update input list, removing non-unenrollable users
				if (!empty($toBeExcluded)) {
					$userIds = array_diff($userIds, $toBeExcluded);
					$lpUserIds = $toBeExcluded;
				}
				//check if any user is remaining to be unenrolled, otherwise send a message to the user
				if (empty($userIds)) {
					$this->renderPartial('_modal_delete_unassigned_none', array(
						'userIds' => $userIds,
						'lpUserIds' => $lpUserIds
					));
					Yii::app()->end();
				}
			}
		}

		if ($rq->isPostRequest && $rq->getParam('confirm', false) !== false) {

			//in case of PU seats available, count real user deletion number and restore the seats
			$seatsToRestore = 0;
			foreach ($userIds as $userId) {
				$res = LearningCourseuser::model()->deleteAllByAttributes(array(
					'idUser' => $userId,
					'idCourse' => $idCourse
				));
				if ($res)
					$seatsToRestore++;
			}

			if ($seatsToRestore && Yii::app()->user->getIsPu() && CoreUser::adminRuleEnabled(Yii::app()->user->id, 'allow_buy_seats'))
			{
				// The current user is a Power user and he is
				// allowed to enroll via "seats" only.
				// make the seat available again
				$puCourseModel = CoreUserPuCourse::model()->findByAttributes(array(
					'puser_id'=>Yii::app()->user->getIdst(),
					'course_id'=>$idCourse,
				));
				if($puCourseModel){
					$puCourseModel->available_seats += $seatsToRestore;
					$puCourseModel->save();
				}
			}

			// close dialog and reload grid
			if (is_array(Yii::app()->session['selectedItems'])) {
				Yii::app()->session['selectedItems'] = array_diff(Yii::app()->session['selectedItems'], $userIds);
			}
			echo '<a href="#" class="auto-close" data-unselect="'.implode(',', $userIds).'"></a>';
			Yii::app()->end();
		}

		$this->renderPartial('_modal_delete_unassigned', array(
			'userIds' => $userIds,
			'lpUserIds' => $lpUserIds
		));
	}


	/**
	 * If classrooms LOs are enabled, load a page to manage them
	 */
	public function actionManageLearningObjects() {

		if (!$this->_courseModel)
			throw new CHttpException("Invalid course provided");

		$cs = Yii::app()->getClientScript();
		/* @var $cs CClientScript */

		// Core Yii
		$cs->registerCoreScript('jquery');
		$cs->registerCoreScript('jquery.ui');
		$cs->registerCssFile($cs->getCoreScriptUrl().'/jui/css/base/jquery-ui.css');
		// Player assets we share
		$cs->registerCssFile ($this->module->getPlayerAssetsUrl().'/css/base.css');
		$cs->registerCssFile($this->module->getPlayerAssetsUrl().'/css/base-responsive.css');
		$cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/yiiform.css');
		Yii::app()->getModule('centralrepo')->registerResources(true);
		// Load Tinymce jQuery integrator
		Yii::app()->tinymce->init();
		//load datepicker plugin
		DatePickerHelper::registerAssets(Yii::app()->getLanguage(), CClientScript::POS_HEAD);

		//others
		$basePath = Yii::getPathOfAlias('admin').'/protected';
		$assetsPath = Yii::app()->getAssetManager()->publish($basePath.'/../js/');
		$cs->registerScriptFile($assetsPath.'/scriptTur.js');
		$cs->registerScriptFile($assetsPath.'/scriptModal.js');
		$cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/Jcrop/jquery.Jcrop.min.js');
		$cs->registerCssFile(Yii::app()->theme->baseUrl.'/js/Jcrop/jquery.Jcrop.min.css');

		$cs->registerScriptFile($this->module->getPlayerAssetsUrl().'/js/jquery.blockUI.js');
		$cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/classroom.css');
		// Dynatree ("borrowed" by lms/player assets)
		$cs->registerScriptFile($this->module->getPlayerAssetsUrl().'/js/jquery.dynatree.js');
		$cs->registerCssFile($this->module->getPlayerAssetsUrl().'/css/ui.dynatree-modified.css');
		//our editing objects tree and launchpad
		$cs->registerScriptFile($this->module->getAssetsUrl().'/js/webinar-los-editing.js');
		$cs->registerScriptFile($this->module->getAssetsUrl().'/js/webinar-los-editing-items.js');

		$adminJs = Yii::app()->assetManager->publish(Yii::getPathOfAlias('admin.js'));
		$cs->registerScriptFile($adminJs . '/script.js');
		$cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/jwizard.js');

		$dummyStorage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_VIDEO);
		$whitelistArray = $dummyStorage->getVideoWhitelistArray();

		$params = array(
			'course_id' => $this->_courseModel->idCourse,      // set in BaseController
			'courseModel' => $this->_courseModel,  // set in BaseController
		);

		Yii::app()->event->raise('BeforeCoursePlayerViewRender', new DEvent($this, $params));

		$this->render('manageLearningObjects', array(
			'courseModel' => $this->_courseModel,
			'whitelistArray' => $whitelistArray
		));
	}

	/**
	 * Displays the list of course sessions for a student to select from
	 */
	public function actionSelect() {
		if (Yii::app()->request->isPostRequest) {
			$sessionId = intval($_POST['courseSessionId']);

			// redirect to classroom app learner view
			$redirectUrl = Docebo::createAppUrl('lms:player/training/webinarSession', array(
				'course_id' => $this->getIdCourse(),
				'session_id' => $sessionId
			));
			$this->redirect($redirectUrl);
		}

		$courseId = $this->getIdCourse();

		$courseSessions = WebinarSessionUser::model()->sessionSelectDataProvider(Yii::app()->user->id, $courseId);
		$courseModel = LearningCourse::model()->findByPk($this->getIdCourse());

		if (0 == $courseSessions->getTotalItemCount()) {
			$this->render('no_sessions', array('courseName' => $courseModel->name));
		} else {
			$this->render('select', array(
				'dataProvider' => $courseSessions,
				'courseName' => $courseModel->name
			));
		}
	}

	protected function sessionsSelectGridRenderStatus($data) {
		$css = '';
		switch ($data['status']) {
			case WebinarSessionUser::$SESSION_USER_END:
				$css = 'green';
				break;
			case WebinarSessionUser::$SESSION_USER_BEGIN:
				$css = 'orange';
				break;
			case WebinarSessionUser::$SESSION_USER_SUBSCRIBED:
			default:
				$css = 'grey';
				break;
		}
		$statuses = LtCourseuserSession::model()->getStatusesArray();
		$title = (isset($statuses[$data['status']]) ? $statuses[$data['status']] : '');
		$html = '<span class="i-sprite is-circle-check ' . $css . '"'.($title ? ' title='.CJSON::encode($title).' rel="tooltip"' : '').'></span>';
		return $html;
	}

	/**
	 * Displays a list of course sessions for a student to enroll to
	 */
	public function actionEnroll() {

		if (!$this->_courseModel) {
			throw new CHttpException("Invalid course provided");
		}

		if($this->_courseModel->selling && !LearningCourseuser::isSubscribed(Yii::app()->user->id, $this->_courseModel->idCourse))
			throw new Exception(Yii::t('standard', '_OPERATION_FAILURE'));

		/* @var $cs CClientScript */
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/webinar.css');

		if (Yii::app()->request->isPostRequest)
		{
			$sessionId = intval($_POST['courseSessionId']);

			$sessionModel = WebinarSession::model()->findByPk(array('id_session' => $sessionId));
			$status = WebinarSessionUser::$SESSION_USER_SUBSCRIBED;
			if($sessionModel->isMaxEnrolledReached())
				$status = LtCourseuserSession::$SESSION_USER_WAITING_LIST;

			if (WebinarSessionUser::enrollUser(Yii::app()->user->id, $sessionId, $status))
			{
				if($status == LtCourseuserSession::$SESSION_USER_SUBSCRIBED)
					$redirectUrl = $redirectUrl = Docebo::createAppUrl('lms:player/training/webinarSession', array(
						'course_id' => $this->getIdCourse(),
						'session_id' => $sessionId
					));
				else
				{
					//Yii::app()->user->setFlash('info', Yii::t('email', '_NEW_USER_SUBS_WAITING_SUBJECT'));

					$redirectUrl = Docebo::createAppUrl('lms:site/index', array(
							'opt' => 'fullmycourses'
					));
				}
				$this->redirect($redirectUrl);
			} else
				Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
		}

		$dataProvider = WebinarSession::model()->sessionEnrollmentDataProvider($this->_courseModel->getPrimaryKey(), $this->_courseModel->allow_overbooking);

		$this->render('enroll', array(
			'dataProvider' => $dataProvider,
			'courseName' => $this->_courseModel->name
		));
	}

	/**
	 * Renders a grid with the session users that have the status 'waiting'
	 */
	public function actionWaiting() {

		if(Yii::app()->user->getIsPu()){
			if(!Yii::app()->user->checkAccess('/lms/admin/webinarsessions/mod') && !Yii::app()->user->checkAccess('/lms/admin/webinarsessions/assign') &&
				(!Yii::app()->user->checkAccess('/lms/admin/webinarsessions/add') || $this->_sessionModel->created_by!=Yii::app()->user->getIdst())) {
				throw new CException('Power users can edit only their own sessions');
			}
		}

		/* @var $session WebinarSession */
		$session = WebinarSession::model()->with('course')->findByPk(Yii::app()->request->getParam('id_session'));
		$params = array();
		$params['applyPowerUserFilter'] = true;


		if (($data = Yii::app()->request->getParam('data')) != null) {

			if ($data['autocomplete']) {
				$params['user_query'] = $data['query'];
			}

			$dataProvider = $session->dataProviderWaiting($params);
			$dataProvider->pagination = false;
			$result = array('options' => array());
			foreach ($dataProvider->data as $courseuser) {
				$user = $courseuser->learningUser;
				$username = Yii::app()->user->getRelativeUsername($user->userid);
				$text = "{$username}";

				$names = array();
				if (!empty($user->firstname))
					$names[] = $user->firstname;
				if (!empty($user->lastname))
					$names[] = $user->lastname;
				$names = join(' ', $names);
				if ($names)
					$text .= " - $names";

				$result['options'][$username] = $text;
			}
			$this->sendJSON($result);
		}

		$params['user_search'] = Yii::app()->request->getParam('searchText', '');
		$dataProvider = $session->dataProviderWaiting($params);

		//--- check permissions for admins and sub-admins
		$canAssign = $canDelete = self::checkWaitingPermissions();
		//---

		$this->render('waiting', array(
			'dataProvider' => $dataProvider,
			'session' => $session,
			'course' => $session->learningCourse,
			'permissions' => array(
				'assign' => $canAssign,
				'del' => $canDelete
			)
		));
	}

	/**
	 * @param $data WebinarSessionUser
	 * @param $index
	 * @return string
	 */
	protected function sessionWaitingGridRenderApprove($data, $index) {

		$errorClass = null;

		if ($data->userModel->valid <= 0) {
			$errorClass = 'user-manipulation-error';
		}

		return CHtml::link('', $this->createUrl('axApprove', array(
			'id_user' => $data->id_user,
			'id_session' => $data->id_session
		)), array(
			'class' => 'suspend-action ' . $errorClass,
			'rel' => 'tooltip',
			'title' => Yii::t('standard', '_APPROVE')
		));
	}

	/**
	 * @param $data WebinarSessionUser
	 * @param $index
	 * @return string
	 */
	protected function sessionWaitingGridRenderReject($data, $index, $waitingSessionView = false) {
		if(PluginManager::isPluginActive('CurriculaApp') && !$waitingSessionView)
			$lpEnrollments = LearningCoursepath::checkEnrollment($data->id_user, $data['learningCourseuser']->idCourse, true);
		if(!empty($lpEnrollments))
		{
			$canUnenrollMessage = '<div class="cannot-unenroll-tooltip">';
			$canUnenrollMessage .= '<span>' . Yii::t('myactivities', 'Learning plans') . ':</span><br />';
			$canUnenrollMessage .= '<ul>';
			foreach ($lpEnrollments as $lpId => $lpInfo) {
				$canUnenrollMessage .= '<li>- ' . $lpInfo['path_name'] . '</li>';
			}
			$canUnenrollMessage .= '</ul>';
			$canUnenrollMessage .= '</div>';

			return CHtml::link('', 'javascript:return false;', array(
					'rel' => 'tooltip',
					'data-html' => 'true',
					'data-toggle' => 'tooltip',
					'data-placement' => 'top',
					'title' => $canUnenrollMessage,
					'class' => 'delete-action',
					'style' => "background-position:-714px -19px;"
				));
		}
		else
			return CHtml::link('', $this->createUrl('axReject', array(
				'id_user' => $data->id_user,
				'id_session' => $data->id_session
			)), array(
				'class' => 'delete-action',
				'rel' => 'tooltip',
				'title' => Yii::t('standard', '_DEL')
			));
	}


	/**
	 * Check specific permissions to access waiting management features
	 * @return bool
	 */
	public static function checkWaitingPermissions($idSession = 0) {
		if (Yii::app()->user->getIsGodAdmin()) {
			return true;
		} elseif (Yii::app()->user->getIsPu() && Yii::app()->user->checkAccess("/lms/admin/course/moderate")) {
			$puCanActivateEnrollments = CoreUser::adminRuleEnabled(Yii::app()->user->id, 'direct_course_subscribe');
			if ($puCanActivateEnrollments) {
				$idSession = Yii::app()->request->getParam('id_session', $idSession);
				if ($idSession <= 0) return false;
				$sessionModel = WebinarSession::model()->findByPk($idSession);
				if (!$sessionModel) return false;
				$sessionBelongsToPu = ($sessionModel->created_by == Yii::app()->user->id);
				$puHasSessionAddPerm = Yii::app()->user->checkAccess('/lms/admin/webinarsessions/add');
				$puHasSessionEditPerm = Yii::app()->user->checkAccess('/lms/admin/webinarsessions/mod');
				$puHasSessionAssignPerm = Yii::app()->user->checkAccess('/lms/admin/webinarsessions/assign');
				if ($puHasSessionEditPerm || ($puHasSessionAddPerm && $sessionBelongsToPu) || $puHasSessionAssignPerm) {
					return true;
				}
			}
		}
		return false;
	}


	private function managementError(){
	 $userManagementError = array();
	 $userManagementError['data']['error'] = Yii::t('user_management', 'Some of the users you are trying to manipulate are not active');
	 echo CJSON::encode($userManagementError);
 	 Yii::app()->end();
	}


	/**
	 * Changes a session subscribed user's status from 'waiting' to 'subscribed'
	 */
	public function actionAxApprove() {


		if (!self::checkWaitingPermissions()) { throw new CException('Access denied'); }

		$userIds = array();
		$sessionId = Yii::app()->request->getParam('id_session');

		if (Yii::app()->request->isPostRequest) {

			$selectedUsers = Yii::app()->request->getParam('selected_users');

			if (!empty($selectedUsers)) {
				$userIds = explode(',', $selectedUsers);
			} else {
				$userIds[] = Yii::app()->request->getParam('id_user');
			}

			if (!WebinarSession::checkIfEnrollmentsAreAvailable($sessionId, count($userIds))) {
				echo CJSON::encode(array('data' => array('error' => Yii::t('classroom', 'Limit of max enrollable users reached'))));
				Yii::app()->end();
			}

			foreach ($userIds as $userId) {
				/* @var $model WebinarSessionUser */
				$model = WebinarSessionUser::model()->with('sessionModel')->with('userModel')->findByAttributes(array(
					'id_user' => $userId,
					'id_session' => $sessionId
				));

				if ($model) {
					if ($model->userModel->valid <= 0) {
						$this->managementError();
					}
					$model->status = $model::$SESSION_USER_SUBSCRIBED;
					if ($model->hasAttribute('waiting')) { $model->waiting = 0; } // NOTE: "waiting" column is deprecated for "webinar_session_user" table. Use "status" instead.
					if($model->save()) {
						Yii::app()->event->raise(EventManager::EVENT_USER_WEBINAR_SESSION_ENROLL_ACTIVATED, new DEvent($this, array(
							'session_id'    => $sessionId,
							'user'          => $userId,
						)));
					}

					// also change the status of the corresponding _courseuser row
					$cuModel = LearningCourseuser::model()->findByAttributes(array(
						'idUser' => $userId,
						'idCourse' => $model->sessionModel->course_id
					));


					if ($cuModel) {
						$cuModel->status = LearningCourseuser::$COURSE_USER_SUBSCRIBED;
						$cuModel->waiting = 0;  // In case the subscription is moderated
						$cuModel->save();
					}
				}
			}

			// close dialog and reload grid
			$this->sendJSON('<a href="#" class="auto-close"></a>');
			Yii::app()->end();
		}

		if (1 === intval(Yii::app()->request->getParam('isBulkAction'))) {
			$this->renderPartial('_modal_approve_waiting', array('sessionId' => $sessionId));
		}
	}

	/**
	 * Deletes a user's session subscription and also the course enrollment of the user
	 * if this is the only session enrollment to this course
	 */
	public function actionAxReject() {

		if (!self::checkWaitingPermissions()) { throw new CException('Access denied'); }

		$userIds = array();
		$sessionId = Yii::app()->request->getParam('id_session');

		if (Yii::app()->request->isPostRequest) {
			$selectedUsers = Yii::app()->request->getParam('selected_users');

			if (!empty($selectedUsers)) {
				$userIds = explode(',', $selectedUsers);
			} else {
				$userIds[] = Yii::app()->request->getParam('id_user');
			}

			foreach ($userIds as $userId) {
				/* @var $model WebinarSessionUser */
				$model = WebinarSessionUser::model()->findByAttributes(array(
					'id_user' => $userId,
					'id_session' => $sessionId
				));

				if ($model) {
					// delete session enrollment
					if($model->delete()) {
						Yii::app()->event->raise(EventManager::EVENT_USER_WEBINAR_SESSION_ENROLL_DECLINED, new DEvent($this, array(
							'session_id'    => $sessionId,
							'user'          => $userId,
						)));
					}

					/* @var $session WebinarSession */
					$session = WebinarSession::model()->findByPk($sessionId);

					// find the other course sessions
					/* @var $sessions WebinarSession[] */
					$sessions = WebinarSession::model()->findAllByAttributes(array(
						'course_id' => $session->course_id
					));

					if ($sessions) {
						// check if user is subscribed to other sessions of this course
						$sessionIds = array_keys(CHtml::listData($sessions, 'id_session', ''));
						$criteria = new CDbCriteria();
						$criteria->addCondition('id_user = :id_user');
						$criteria->addInCondition('id_session', $sessionIds);
						$criteria->params[':id_user'] = Yii::app()->request->getParam('id_user');
						$courseuserSessions = WebinarSessionUser::model()->findAll($criteria);

						if (!$courseuserSessions) {
							// unsubscribe from course
							LearningCourseuser::model()->deleteAllByAttributes(array(
								'idUser' => $userId,
								'idCourse' => $session->course_id
							));
						}
					}
				}
			}

			// close dialog and reload grid
			$this->sendJSON('<a href="#" class="auto-close"></a>');
			Yii::app()->end();
		}

		if (1 === intval(Yii::app()->request->getParam('isBulkAction'))) {
			$lpAssignments = array();
			if (PluginManager::isPluginActive('CurriculaApp'))
			{
				$idCourse = Yii::app()->request->getParam('id_course');
				if (LearningCoursepathCourses::isCourseAssignedToLearningPaths($idCourse))
				{
					$waitingUsers =	Yii::app()->db->createCommand()
									->select('id_user')
									->from(WebinarSessionUser::model()->tableName())
									->where('id_session = :id_session AND status = :status', array(':id_session' => $sessionId, ':status' => WebinarSessionUser::$SESSION_USER_WAITING_LIST))
									->queryColumn();
					if(!empty($waitingUsers))
						$lpAssignments = LearningCoursepath::getEnrollments($waitingUsers, $idCourse);
				}
			}

			$this->renderPartial('_modal_reject_waiting', array('sessionId' => $sessionId, 'lpAssignments' => $lpAssignments));
		}
	}

	public function actionAxMarkCompleteOnJoin()
	{
		$id_session = Yii::app()->request->getParam('id_session');
		$day = Yii::app()->request->getParam('day');
		$event = Yii::app()->request->getParam('event');

		try {
			//validate input
			$session = WebinarSession::model()->findByPk($id_session);
			if (!$session) {
				throw new CException('Session with ID #' . $id_session . ' not found');
			}
			$sessionDate = WebinarSessionDate::model()->findByPk(array('id_session' => $id_session, 'day' => $day));
			if (!$sessionDate) {
				throw new CException('Session date with ID (' . $id_session . ', "' . $day . '") not found');
			}

			$model = WebinarSessionDateAttendance::model()->findByAttributes(array(
				'id_session' => $id_session,
				'day' => $day,
				'id_user' => Yii::app()->user->getIdst(),
			));
			if (!$model) {
				$model = new WebinarSessionDateAttendance();
				$model->id_session = $id_session;
				$model->day = $day;
				$model->id_user = Yii::app()->user->getidst();
			}
			switch ($event) {
				case WebinarSession::EVENT_WATCHED_LIVE:
					$model->watched_live = 1;
					break;
				case WebinarSession::EVENT_WATCHED_RECORDING:
					$model->watched_recording = 1;
					break;
			}
			if (!$model->save()) {
				Yii::log(print_r($model->getErrors(), true), CLogger::LEVEL_ERROR);
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			if ($session->evaluation_type == WebinarSession::EVAL_TYPE_ON_JOIN) {
				// Check how many dates of this session the user has attended
				// and if it's more than the required number for passing,
				// set him to that state
				$sessionEnrollment = WebinarSessionUser::model()->findByPk(array(
					'id_user' => Yii::app()->user->id,
					'id_session' => $session->getPrimaryKey()
				));
				if ($sessionEnrollment && !($sessionEnrollment->status  == WebinarSessionUser::$SESSION_USER_END)) {
					/* @var $sessionEnrollment WebinarSessionUser */
					$watchTypeCondition = '';
					if(!isset($session->allow_recording_completion) || !$session->allow_recording_completion){
						$watchTypeCondition = ' AND watched_live = 1';
					}

					$attendedDatesFromSession = Yii::app()->getDb()->createCommand()
						->select('COUNT(DISTINCT day)')
						->from(WebinarSessionDateAttendance::model()->tableName())
						->where('id_session=:idSession AND id_user=:idUser'. $watchTypeCondition, array(
							':idSession' => $id_session,
							':idUser' => Yii::app()->user->getIdst()
						))
						->queryScalar();
					if ($attendedDatesFromSession >= $session->min_attended_dates_for_completion) {
						$rs = $sessionEnrollment->setEvaluation(WebinarSessionUser::EVALUATION_STATUS_PASSED, 0, null);
						if (!$rs) {
							//TODO: what to do when errors occur ?
						}
					}
					else
					{
						$sessionEnrollment->status = WebinarSessionUser::$SESSION_USER_BEGIN;
						$rs = $sessionEnrollment->save();
						if (!$rs) {
							//TODO: what to do when errors occur ?
						}
					}
				}
			}

			$course =  LearningCourse::model()->findByPk($session->course_id);

			$courseUser = LearningCourseuser::model()->findByAttributes(array(
				'idUser' => Yii::app()->user->getidst(),
				'idCourse' => $session->course_id
			));

			$showUnenrollmentButtons = true;

			if ($course->enable_unenrollment_on_course_completion != 1 && $courseUser->status == LearningCourseuser::$COURSE_USER_END) {
				$showUnenrollmentButtons = false;
			}

			//prepare data for the client in order to update interface properly
			$output = array(
				'success' => true,
				'showUnenrollmentButtons' => $showUnenrollmentButtons
			);

			//session date label info
			if ($model->watched_live > 0 || $model->watched_recording > 0) {
				$output['new_status_label_class'] = 'label-green';
				$output['new_status_label_text'] = Yii::t('webinar', 'Attended');
			} else {
				$output['new_status_label_class'] = 'label-black';
				$output['new_status_label_text'] = Yii::t('webinar', 'Not attended');
			}

			//user evaluation (if needed)
			//NOTE: view parameters are retrieved in similar way as in lms/protected/modules/webinar/widgets/SessionInfo.php, init() method
			$courseEnrollment = LearningCourseuser::model()->findByAttributes(array(
				'idUser' => Yii::app()->user->id,
				'idCourse' => $session->course_id
			));
			if (!empty($courseEnrollment)) {

				if (!isset($sessionEnrollment)) {
					$sessionEnrollment = WebinarSessionUser::model()->findByPk(array(
						'id_user' => Yii::app()->user->id,
						'id_session' => $session->getPrimaryKey()
					));
				}

				$hasEvaluationStatus = in_array($sessionEnrollment->evaluation_status, array(WebinarSessionUser::EVALUATION_STATUS_FAILED, WebinarSessionUser::EVALUATION_STATUS_PASSED));
				$isLevelLessThanInstructor = ($courseEnrollment->level < LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR);
				if ($hasEvaluationStatus && $isLevelLessThanInstructor) {

					$score = null;
					if (($session->evaluation_type == WebinarSession::EVAL_TYPE_MANUAL) || ($session->evaluation_type == WebinarSession::EVAL_TYPE_ON_JOIN)) {
						$score =  $sessionEnrollment->evaluation_score;
					} elseif ($session->evaluation_type == WebinarSession::EVAL_TYPE_ONLINE_TEST && !empty($sessionEnrollment)) {
							$score = LearningCourseuser::getLastScoreByUserAndCourse($session->course_id, $sessionEnrollment->id_user, true);
							if (is_array($score) && isset($score['score']) && $score['score']) {
								$score['score'] = round(floatval($score['score']), 2);
							}
					}

					$attachment = array(
						'name' => $sessionEnrollment->getOriginalFileName(),
						'url' => ($sessionEnrollment->hasEvaluationFile()
							? Docebo::createLmsUrl('ClassroomApp/instructor/downloadEvaluationFile', array('id_user' => Yii::app()->user->id, 'id_session' => $session->getPrimaryKey(), 'course_id' => $session->course_id))
							: '#')
					);

					$output['new_evaluation_box'] = $this->renderPartial('lms.protected.modules.webinar.widgets.views._evaluation', array(
						'courseModel' => LearningCourse::model()->findByPk($session->course_id),
						'sessionModel' => $session,
						'evaluator' => CoreUser::model()->findByPk($sessionEnrollment->evaluator_id),
						'evaluationDate' => $sessionEnrollment->evaluation_date,
						'evaluationStatus' => (($sessionEnrollment->evaluation_status == WebinarSessionUser::EVALUATION_STATUS_PASSED) ? 'passed' : 'failed'),
						'evaluationScore' => $score,
						'evaluationComments' => $sessionEnrollment->evaluation_text,
						'evaluationAttachment' => $attachment
					), true);
				}
			}
		} catch (Exception $e) {
			$output = array(
				'success' => false,
				'message' => $e->getMessage()
			);
		}

		echo CJSON::encode($output);
	}

	public function actionJoin() {
		$id_session = Yii::app()->request->getParam('id_session');
		$day = Yii::app()->request->getParam('day');
		$url = false;
		$courseId = Yii::app()->request->getParam('course_id');
		$session = WebinarSessionDate::model()->findByPk(array(
			'id_session'=>$id_session,
			'day'=>$day
		));

		if($session){
			/* @var $session WebinarSessionDate */
			$url = $session->getWebinarUrl();
		}

		if(!$url) {
			$url = Docebo::createAbsoluteLmsUrl( 'site/index' );
			Yii::log("Unable to retrieve URL for webinar join. Day {$session->day}, session {$session->id_session}, tool {$session->webinar_tool}", CLogger::LEVEL_ERROR);
		}

		// Raising an event for other application to work with
		Yii::app()->event->raise(EventManager::EVENT_STUDENT_COMPLETED_WEBINAR_SESSION, new DEvent($this, array(
			'courseId' => $courseId,
			'userId' => Yii::app()->user->id
		)));

		$this->redirect($url);
	}

	public function actionSyncTimers() {
		$this->sendJSON(array(
			'now' => strtotime(Yii::app()->localtime->getUTCNow())
		));
		Yii::app()->end();
	}

	public function actionGotoWebinar(){
		$this->renderPartial('_webinarCantDeleteOrUpdate');
	}

	public function actionAddRecording(){
		$confirm = Yii::app()->request->getParam('confirm_write', null);
		$recording_mode = Yii::app()->request->getParam('recording_mode', null);
		$id_session = Yii::app()->request->getParam('session_id', null);
		$id_course = Yii::app()->request->getParam('course_id', null);
		$date = Yii::app()->request->getParam('date', null);
		$uploadedFile = Yii::app()->request->getParam('FileUpload', null);
		$weblink_address = Yii::app()->request->getParam('weblink_address', null);

		$errors = array();

		// switch $mode to change which partial to use
		$mode = "init";
		$finished = false; // set to true to immediately show the completed view

		//check session validity
		$sessionModel = WebinarSession::model()->findByPk($id_session);
		if (empty($sessionModel)) {
			throw new CException('Invalid webinar session ID');
		}
		$sessionDateModel = WebinarSessionDate::model()->findByPk(array(
			'id_session' => $id_session,
			'day' => $date
		));
		if (empty($sessionDateModel)) {
			throw new CException('Invalid webinar session date');
		}
		$tool = WebinarTool::getById($sessionDateModel->webinar_tool);
		$webinarTool = $tool->getId();
		$webinarToolName = $tool->getName();

		// determine whether or not to show the API grab/link functions
		$showApiGrabOption = $sessionDateModel->toolAllowsApiGrabRecording();
		$showApiLinkOption = $sessionDateModel->toolAllowsApiLinkRecording();
		$playRecordingType = $sessionDateModel->getPlayRecordingType();


		//check if the action has been confirmed by clicking "save" button
		if ($confirm) {

			$recordingModel = WebinarSessionDateRecording::model()->findByAttributes(array(
				'id_session' => $id_session,
				'day' => $date
			));
			if (empty($recordingModel)) {
				$recordingModel = new WebinarSessionDateRecording();
				$recordingModel->id_session = $id_session;
				$recordingModel->day = $date;
			}
			$recordingModel->id_course = $id_course;
			$recordingModel->type = $recording_mode;

			// add the recording data to the database
			switch ($recording_mode) {
				case WebinarSessionDateRecording::RECORDING_TYPE_API_GRAB:
					$arFile = @CJSON::decode($recordingModel->path);
					if(empty($arFile['tmp_file'])){
						$errors[] = Yii::t('webinar', 'Import a file first');
					} else {
						$importFile = $arFile['tmp_file'];
						$fileInfo = pathinfo($importFile);
						$extension = (strpos($fileInfo['extension'], '?') > 0) ? substr($fileInfo['extension'], 0, strpos($fileInfo['extension'], '?')) : $fileInfo['extension'];
						$fileNameStartParams = strpos($fileInfo['filename'], '?');
						if($fileNameStartParams !== false){
							$fileNameTmp = substr($fileInfo['filename'], 0, $fileNameStartParams);
							$fileInfo2 = pathinfo($fileNameTmp);
							$fileName = $fileInfo2['filename'];
							$extension = $fileInfo2['extension'];
						} else {
							$fileName = $fileInfo['filename'];
						}
						if(empty($extension)) $extension = 'mp4'; // required in case of redirected URLs
						$uploadedFile = $fileName . "." . $extension;
						$localPath = Docebo::getUploadTmpPath();
						$fullPath = $localPath . DIRECTORY_SEPARATOR . $uploadedFile;
						if(!file_exists($fullPath)){
							$errors[] = Yii::t('webinar', 'File was not stored properly in the LMS');
						} else {
							$recordingModel->path = CJSON::encode(array('tmp_file' => $uploadedFile));
							$recordingModel->status = WebinarSessionDateRecording::RECORDING_STATUS_CONVERTING;
							$recordingModel->progress = NULL; //reset download progress status
						}
					}
					$mode = 'converting'; // this changes the modal view
					break;
				case WebinarSessionDateRecording::RECORDING_TYPE_UPLOAD:
					if(!empty($uploadedFile)){
						$mode = 'converting'; // this changes the modal view
						$recordingModel->path = CJSON::encode(array('tmp_file' => $uploadedFile));
						$recordingModel->status = WebinarSessionDateRecording::RECORDING_STATUS_CONVERTING;
					} else {
						$errors[] = Yii::t('webinar', 'Please, upload a file first');
					}
					break;
				case WebinarSessionDateRecording::RECORDING_TYPE_WEBLINK:
					//first check if the provided URL is a valid URL
					$weblink_address = trim($weblink_address); //this comes as page input, make sure that unnecessary spaces are removed
					$pattern = "/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i";
					if (preg_match($pattern, $weblink_address)) {
						//URL is valid so now  we have to check if it is whitelisted as video URL

						// Try to get custom defined iframe sources(urls)
						$customIframeUrls = CoreSettingWhitelist::model()->findByAttributes(array('type' => 'lo_iframe_url'));
						$addIframeUrlRules = '';
						if ($customIframeUrls) {
							$allcustomIframeUrls = CJSON::decode($customIframeUrls->whitelist);
							foreach($allcustomIframeUrls as $customIframeUrl) {
								$customIframeUrl = preg_replace('%^(http://|https://)%', '', $customIframeUrl);
								$addIframeUrlRules .= '|'.$customIframeUrl .'$';
								// after slash
								if(!preg_match('%/$%', $customIframeUrl)) {
									$addIframeUrlRules .= '|'.$customIframeUrl . '/[\w]*';
								} else {
									$addIframeUrlRules .= '|'.$customIframeUrl . '[\w]*';
								}
								// after question mark
								if(!preg_match('%\?$%', $customIframeUrl)) {
									$addIframeUrlRules .= '|'.$customIframeUrl . '\?[\w]*';
								} else {
									$addIframeUrlRules .= '|'.$customIframeUrl . '[\w]*';
								}
							}
						}
						//retrieve default whitelist and join the custom ones (if any)
						$newIframeRules = Yii::app()->params['embed_whitelist'];
						if ($addIframeUrlRules != '') {
							$newIframeRules = substr($newIframeRules, 0, -2);
							$newIframeRules .= $addIframeUrlRules . ')%';
						}
						//do the matching
						$isWhitelisted = (!empty($newIframeRules) ? preg_match($newIframeRules.'im', $weblink_address) : false);
						if ($isWhitelisted) {
							//if all checks are passed then store the URL in the DB
							if ((strpos($weblink_address, "http://") !== 0) && strpos($weblink_address, "https://") !== 0) {
								$weblink_address = "http://" . $weblink_address;
							}
							//further step: some URLs may be valid, but should be transformed in their "embedded" version (see youtube or vimeo), otherwise they cannot be played in an iframe
							$weblink_address = WebinarRecordings::transformVideoUrlIntoEmbeddedVersion($weblink_address);
							//continue with other operations
							$mode = 'converting';
							$finished = true;
							$recordingModel->path = $weblink_address;
							$recordingModel->status = WebinarSessionDateRecording::RECORDING_STATUS_COMPLETED;
						} else {
							$errors[] = Yii::t('webinar', 'Video URL is not whitelisted');
						}
					} else {
						$errors[] = Yii::t('webinar', 'Please, provide a valid URL');
					}
					break;
				case WebinarSessionDateRecording::RECORDING_TYPE_API_LINK:
					$tool = WebinarTool::getById($webinarTool);
					$result = $tool->retrieveRecordingLink($sessionDateModel->id_tool_account, CJSON::decode($sessionDateModel->webinar_tool_params));
					if(!empty($result['recordingUrl'])){
						// Fetch the domain + subdomain (eg. https://meet12345678.adobeconnect.com)
						$domainPattern = '#^(https://){1}[a-zA-Z0-9]{5,}(\.adobeconnect\.com){1}#';
						preg_match($domainPattern, $result['recordingUrl'], $domainMatch);
						$customIframeUrls = CoreSettingWhitelist::model()->findByAttributes(array('type' => 'lo_iframe_url'));

						// If no previous whitelisted entries were made, create the model
						if(empty($customIframeUrls)){
							$customIframeUrls = new CoreSettingWhitelist();
							$customIframeUrls->type = 'lo_iframe_url';
							$customIframeUrls->whitelist = CJSON::encode(array($domainMatch[0] => $domainMatch[0]));
							$customIframeUrls->save();
						} else { // Else: add the new record to the list if it wasn't added yet before...
							$whiteListedUrls = CJSON::decode($customIframeUrls->whitelist);
							if(!in_array($domainMatch[0], $whiteListedUrls)){
								$whiteListedUrls[$domainMatch[0]] = $domainMatch[0];
								$customIframeUrls->whitelist = CJSON::encode($whiteListedUrls);
								$customIframeUrls->save();
							}
						}
						//continue with other operations
						$mode = 'converting';
						$finished = true;
						$recordingModel->path = $result['recordingUrl'];
						$recordingModel->status = WebinarSessionDateRecording::RECORDING_STATUS_COMPLETED;
					} else {
						$errors[] = $result['error'];
					}
					break;
				default: // if no recording mode has been set after clicking the submit button, the following default error is shown:
					$errors[] = Yii::t('webinar', 'Select your preferred recording type');
			}

			if (empty($errors)) {
				$recordingModel->save();

				// convert the video (api_grab & upload only)
				switch ($recording_mode) {
					case WebinarSessionDateRecording::RECORDING_TYPE_API_GRAB:
					case WebinarSessionDateRecording::RECORDING_TYPE_UPLOAD:
						/*
						$params = array(
							'task' => 'conversion',
							'session_id' => $id_session,
							'date' => $date,
							'filename' => $uploadedFile
						);
						$scheduler = Yii::app()->scheduler;
						$scheduler->createImmediateJob(WebinarJobs::JOB_NAME, WebinarJobs::id(), $params);
						*/
						//==========
						// NOTE: the above code using jobs does not work correctly in production environment. Being that a
						// multi-container installation it is not currently possible to refer the same local tmp file directory
						// for all the involved container (since any of them has its local fiel space separate from others)
						$result = WebinarRecordings::convertWebinar($id_session, $date, $uploadedFile);
						$log = new WebinarConversion();
						$log->session_id = $id_session;
						$log->date = $date;
						$log->conversion_date = date('Y-m-d H:i:s');
						$log->status = $result['status'];
						$log->filename = $result['filename'];
						$log->job_id = '-';
						$log->error = $result['error'];
						$log->save();
						//==========
						break;
					case WebinarSessionDateRecording::RECORDING_TYPE_WEBLINK:
					case WebinarSessionDateRecording::RECORDING_TYPE_API_LINK:
						//no more actions needed for web urls
						echo '<script type="text/javascript">try { afterAddWebLinkRecording('.$recordingModel->id_session.', "'.$recordingModel->day.'", "'.$recordingModel->path.'", "'.$playRecordingType.'"); } catch(e) { Docebo.log(e); }</script>';
						Yii::app()->end();
						break;
				}
			} else {
				$finished = true;
			}
		}

		switch($mode) {
			case "init":
				$this->renderPartial('_add_recording', array(
					'errors' => $errors,
					'recording_mode' => $recording_mode,
					'weblink_address' => $weblink_address,
					'webinarTool' => $webinarTool,
					'webinarToolName' => $webinarToolName,
					'showApiGrabOption' => $showApiGrabOption,
					'showApiLinkOption' => $showApiLinkOption,
					'session_id' => $id_session,
					'day' => $date
				));
				break;
			case "converting":
				$enrollment = LearningCourseuser::model()->findByAttributes(array(
					'idUser' => Yii::app()->user->id,
					'idCourse' => $id_course
				));
				$userLevel = (!empty($enrollment) ? $enrollment->level : false);
				$this->renderPartial('_spinner', array(
					'errors' => $errors,
					'id_session' => $id_session,
					'date' => $date,
					'finished' => $finished,
					'canEditCourse' => ($userLevel >= LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR || Yii::app()->user->getIsGodadmin())
				));
				break;
		}
	}

	public function actionDeleteRecording(){
		$confirm = Yii::app()->request->getParam('confirm_write', null);
		$id_session = Yii::app()->request->getParam('session_id', null);
		$date = Yii::app()->request->getParam('date', null);

		if($confirm){

			try {

				$recordingModel = WebinarSessionDateRecording::model()->findByAttributes(array(
					'id_session' => $id_session,
					'day' => $date
				));
				if (empty($recordingModel)) {
					throw new CException('Invalid input data');
				}

				// File can only be deleted if it was an api_grab or upload
				if (in_array($recordingModel->type, array(WebinarSessionDateRecording::RECORDING_TYPE_API_GRAB, WebinarSessionDateRecording::RECORDING_TYPE_UPLOAD))) {

					$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_WEBINAR_RECORDING);
					$old = CJSON::decode($recordingModel->path);

					// Check if old MP4 and HLS are NOT empty, then delete OLD mp4 file and the HLS folder
					if (is_array($old) && isset($old['mp4']) && !empty($old['mp4'])) {
						Yii::log('Removing MP4 file:' . $old['mp4'], CLogger::LEVEL_INFO);
						$storage->remove($old['mp4']);      // Remove the mp4 file outside the folder
					}

					if (is_array($old) && isset($old['hls_id']) && !empty($old['hls_id'])) {
						$folderToDelete = pathinfo($old['hls_id'], PATHINFO_FILENAME);
						Yii::log('Removing Folder:' . $folderToDelete, CLogger::LEVEL_INFO);
						$storage->removeFolder($folderToDelete); // Remove the folder from Amazon if there is existing one
					}
				}

				// Remove the log entry to allow for a new upload
				WebinarConversion::model()->deleteAllByAttributes(array(
					'session_id' => $id_session,
					'date' => $date
				));

				$recordingModel->path = '-';
				$recordingModel->status = WebinarSessionDateRecording::RECORDING_STATUS_NON_EXISTANT;
				$recordingModel->save();

				echo '<a class="auto-close"></a>';
				echo '<script type="text/javascript">try { afterDeleteRecording('.$recordingModel->id_session.', "'.$recordingModel->day.'"); } catch(e) { Docebo.log(e); }</script>';
				Yii::app()->end();

			} catch (Exception $e) {

				$msg1 = CJSON::encode(Yii::t('standard', '_OPERATION_FAILURE'));
				$msg2 = CJSON::encode($e->getMessage());
				echo '<a class="error-message" data-message='.$msg1.' data-internal-message='.$msg2.'></a>';
				Yii::app()->end();
			}
		}

		$this->renderPartial('_delete_recording', array(
			'id_session' => $id_session,
			'date' => $date
		));
	}

	public function actionAxCheckRecordingStatus(){
		$session_id = Yii::app()->request->getParam('session_id', null);
		$date = Yii::app()->request->getParam('date', null);
		$response = new AjaxResult();
		$recordingLog = WebinarConversion::model()->findByAttributes(array(
			'session_id' => $session_id,
			'date' => $date
		));
		if(empty($recordingLog)){
			$data = array(
				'status' => 'converting',
				'error' => ''
			);
		} else {
			$recording = WebinarSessionDateRecording::model()->findByPk(array('id_session' => $session_id, 'day' => $date));
			if (empty($recording)) {
				$data = array(
					'status' => 'error',
					'error' => 'Invalid recording'
				);
			} else {
				if ($recording->status == WebinarSessionDateRecording::RECORDING_STATUS_CONVERTING) {
					// if we came here then the video file is transcoding on S3
					$data = array(
						'status' => 'converting',
						'error' => ''
					);
				} else {
					$data = array(
						'status' => $recordingLog->status,
						'error' => $recordingLog->error
					);
				}
			}
		}
		$response->setStatus(true)->setData($data)->toJSON();
		Yii::app()->end();
	}


	/**
	 * Create async . job for video recording download and check process status
	 * @throws CHttpException
	 */
	public function actionAxApiGrabRecording() {
		//read input
		$sessionId = Yii::app()->request->getParam('sessionId', null);
		$day = Yii::app()->request->getParam('day', null);

		//retrieve session from DB
		$sessionModel = WebinarSessionDate::model()->findByAttributes(array(
			'id_session' => $sessionId,
			'day' => $day
		));

		//init output
		$data = array(
			'status' => '',
			'error' => '',
			'progress' => 0
		);
		if (empty($sessionModel)) {
			$data['status'] = 'failed';
			$data['error'] = 'Could not find a record for the requested session ('.$sessionId.'-'.$day.')';
		} elseif (!$sessionModel->toolAllowsApiGrabRecording()) {
			$data['status'] = 'failed';
			$data['error'] = 'Importing recordings via API call is not supported with this tool';
		} else { // in case no errors were found:
			// Check if there's a download in progress
			$recordingModel = WebinarSessionDateRecording::model()->findByAttributes(array(
				'id_session' => $sessionId,
				'day' => $day
			));
			$tool = WebinarTool::getById($sessionModel->webinar_tool);

			if(empty($recordingModel) || ($recordingModel->status == WebinarSessionDateRecording::RECORDING_STATUS_NON_EXISTANT)){ // new record or previously deleted record
				$result = $tool->getApiRecordings($sessionModel->id_tool_account, CJSON::decode($sessionModel->webinar_tool_params));
				if(!empty($result['error'])){ // No recording found --> show an error in the modal
					$data['status'] = 'failed';
					$data['error'] = $result['error'];
				} else {
					if(empty($recordingModel)){ // start a new recording model in case none existed before
						$recordingModel = new WebinarSessionDateRecording();
						$recordingModel->id_session = $sessionId;
						$recordingModel->day = $day;
						$recordingModel->id_course = Yii::app()->db->createCommand()
							->select('course_id')
							->from(WebinarSession::model()->tableName())
							->where('id_session = :sessionId', array(
								':sessionId' => $sessionId
							))
							->queryScalar();
					}
					$recordingModel->type = WebinarSessionDateRecording::RECORDING_TYPE_API_GRAB;
					$recordingModel->path = CJSON::encode(array('tmp_file' => $result['recordingUrl']));
					$recordingModel->status = WebinarSessionDateRecording::RECORDING_STATUS_DOWNLOADING;
					$recordingModel->progress = 0; //initialize download progress status
					$recordingModel->save();

					$params = array(
						'task' => 'download',
						'session_id' => $sessionId,
						'date' => $day,
						'filename' => $result['recordingUrl']
					);
					$scheduler = Yii::app()->scheduler;
					$scheduler->createImmediateJob(WebinarJobs::JOB_NAME, WebinarJobs::id(), $params);
					//$this->downloadRecordingByCurl($recordingModel);
					$data['status'] = 'downloading';
				}
			} elseif($recordingModel->status == WebinarSessionDateRecording::RECORDING_STATUS_CONVERTING) {
				// download is completed, ready to move on to converting phase
				$data['status'] = 'success';
				$data['progress'] = 100;
			} else { // download is in progress
				$progress = (($recordingModel->progress == null) ? 0 : $recordingModel->progress);
				if ($progress >= 100){
					$progress = 100; //make sure to have 100 as max value
					$data['status'] = 'success';
					$data['progress'] = 100;
					/*
					$recordingModel->status = WebinarSessionDateRecording::RECORDING_STATUS_CONVERTING;
					$recordingModel->progress = null; //reset progress status, since operation is finished
					$recordingModel->save();
					*/
				} else {
					$data['status'] = 'downloading';
				}
				$data['progress'] =  number_format($progress, 2, '.', '');
			}
		}

		$response = new AjaxResult();
		$response->setStatus(true)->setData($data)->toJSON();
		Yii::app()->end();
	}

	public function actionRecordingPlayer() {

		/* @var $rq CHttpRequest */
		$rq = Yii::app()->request;

		//read input
		$idSession = $rq->getParam('id_session', 0);
		$day = $rq->getParam('day', '');

		//validate input and retrieve session date model
		$date = WebinarSessionDate::model()->findByPk(array(
			'id_session' => $idSession,
			'day' => $day
		));
		if (empty($date)) {
			throw new CException('Invalid session date');
		}

		//retrieve session date recording model
		$dateRecording = $date->getRecording();
		if (empty($dateRecording)) {
			throw new CException('Invalid recording');
		}

		//build proper player, depending on recording type
		switch ($dateRecording->type) {
			case WebinarSessionDateRecording::RECORDING_TYPE_API_GRAB:
				//this is exactly the same as file upload, so go on
			case WebinarSessionDateRecording::RECORDING_TYPE_UPLOAD:

				$videoFiles = CJSON::decode($dateRecording->path);
				if (empty($videoFiles)) {
					throw new CException('Invalid recording');
					Yii::app()->end();
				}

				$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_WEBINAR_RECORDING);
				/*
				//$mediaUrl = $storage->fileUrl($dateRecording->path);
				$mediaUrlMp4 = $storage->fileUrl($dateRecording->path);
				$mediaUrlHlsPlaylist = null;
				*/
				$videoFileNames = array_values($videoFiles);

				if (isset($videoFiles['hls_id']) ) {
					$mediaUrl = '';
					$mediaUrlMp4 = $storage->fileUrl($videoFileNames[0], $videoFiles['hls_id']);
					$mediaUrlHlsPlaylist = $storage->fileUrl($videoFiles['hls_id']. ".m3u8", $videoFiles['hls_id']);
				} else {
					$mediaUrl = $storage->fileUrl($videoFileNames[0]);
					$mediaUrlMp4 = $storage->fileUrl($videoFileNames[0]);
					$mediaUrlHlsPlaylist = null;
				}

				$html = $this->renderPartial('_recording_player/_upload', array(
					'dateModel' => $date,
					'recordingModel' => $dateRecording,
					'mediaUrl' => $mediaUrl,
					'mediaUrlMp4' => $mediaUrlMp4,
					'mediaUrlHlsPlaylist' => $mediaUrlHlsPlaylist
				), true);
				break;
			case WebinarSessionDateRecording::RECORDING_TYPE_WEBLINK:
			case WebinarSessionDateRecording::RECORDING_TYPE_API_LINK:
				$html = $this->renderPartial('_recording_player/_weblink', array(
					'dateModel' => $date,
					'recordingModel' => $dateRecording
				), true);
				break;
			default:
				$html = '';
				break;
		}

		//send player content to the client
		echo CJSON::encode(array(
			'success' => true,
			'html' => $html
		));

		Yii::app()->end();
	}


	public function actionRecordingPlayerKeepAlive() {
		//this action doesn't need to do anything, it's just a keep alive function
	}


	public function actionCheckVideoConversionProgressStatus() {
		/* @var $rq CHttpRequest */
		$rq = Yii::app()->request;

		$idSession = $rq->getParam('id_session', 0);
		$days = $rq->getParam('days', array());

		if ($idSession <= 0 || empty($days) || !is_array($days)) {
			//invalid input
			echo CJSON::encode(array('success' => false, 'message' => 'invalid input'));
			Yii::app()->end();
		}

		$timeout = false;
		$startTime = microtime(true);
		$numMaxLoopCycles = 100;

		do {
			$outputStatuses = array();
			$someConversionHasFinished = false;

			//for every converting video check its progress status from DB and store it
			foreach ($days as $day) {
				$recording = WebinarSessionDateRecording::model()->findByPk(array(
					'id_session' => $idSession,
					'day' => $day['day']
				));
				if (!empty($recording)) {
					$outputStatuses[] = array(
						'id_session' => $recording->id_session,
						'day' => $recording->day,
						'status' => $recording->status
					);
					if ($day['status'] != $recording->status) {
						$someConversionHasFinished = true;
					}
				}
			}

			//check timeout
			$nowTime = microtime(true);
			if (($nowTime - $startTime) > 5.0) {
				$timeout = true;
			}

			//check if status has changed
			if (!$timeout && !$someConversionHasFinished) {
				usleep(100000); //wait one tenth of a second and then re-check statuses
			}

			//decrease loop cycles counter
			$numMaxLoopCycles--;

		} while (!$someConversionHasFinished && !$timeout && $numMaxLoopCycles > 0);

		//send info to the client
		echo CJSON::encode(array('success' => true, 'data' => $outputStatuses));
	}
}