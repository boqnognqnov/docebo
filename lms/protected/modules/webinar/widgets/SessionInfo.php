<?php

/**
 * Displays the basic session info, along with a list of session dates (accordion list type)
 */
class SessionInfo extends CWidget {

	public $courseModel;
	public $sessionId;
	public $hideScoreBase;

	/**
	 * @var WebinarSession
	 */
	protected $session;
	/**
	 * @var CActiveDataProvider
	 */
	protected $dataProvider;
	protected $evaluation;
	protected $userLevel;


	protected $_assetsUrl = null;
	protected $_playerAssetsUrl = null;

	/**
	 * Get widget assets url path
	 * @return mixed
	 */
	public function getAssetsUrl() {
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('plugin').'/ClassroomApp/assets');
		}
		return $this->_assetsUrl;
	}



	/**
	 * Get player module assets url path
	 * @return mixed
	 */
	public function getPlayerAssetsUrl()
	{
		if ($this->_playerAssetsUrl === null) {
			$this->_playerAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('player.assets'));
		}
		return $this->_playerAssetsUrl;
	}



	public function init()
	{
		parent::init();

		/* @var $cs CClientScript */
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/js/fancybox/jquery.fancybox.css');
		$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/fancybox/jquery.fancybox.pack.js');

		$this->session = WebinarSession::model()->findByPk($this->sessionId);
		$this->dataProvider = array(); //LtCourseSessionDate::sessionInfoDataProvider($this->sessionId);

		//Check if we have recording videos. If so then pre-load flowplayer stuff.
		if (!empty($this->session) && $this->session->hasRecordings()) {
			Flowplayer::registerFlowplayerScriptsAndCss();
		}

		// Check is user is enrolled in this session
		$courseSession = WebinarSessionUser::model()->findByAttributes(array(
			'id_session' => $this->sessionId,
			'id_user' => Yii::app()->user->id
		));
		/* @var $courseSession WebinarSessionUser */

		// Do we have to show session subscription info ? (only for subscribed users)
		$userLevel = 0;
		if ($courseSession) {
			$this->userLevel = $courseSession->level;
			$showEvaluation = in_array($courseSession->evaluation_status, array(
					WebinarSessionUser::EVALUATION_STATUS_FAILED,
					WebinarSessionUser::EVALUATION_STATUS_PASSED
				)) && ($this->userLevel < LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR);
			if ($showEvaluation) {
				$this->evaluation = array(
					'status' => ($courseSession->evaluation_status == WebinarSessionUser::EVALUATION_STATUS_PASSED)
							? 'passed' : 'failed',
					'score' => $this->getEvaluationScore($courseSession),
					'comments' => $courseSession->evaluation_text,
					'attachment' => array(
						'name' => $courseSession->getOriginalFileName(),
						'url' => ($courseSession->hasEvaluationFile() ? Docebo::createLmsUrl('ClassroomApp/instructor/downloadEvaluationFile', array(
								'id_user' => Yii::app()->user->id,
								'id_session' => $this->session->getPrimaryKey(),
								'course_id' => $this->session->course_id
							)) : '#')
					),
					'evaluator' => CoreUser::model()->findByPk($courseSession->evaluator_id),
					'date' => $courseSession->evaluation_date
				);
			}
		} else {
			if (!Yii::app()->user->getIsGodadmin() && !Yii::app()->user->getIsAdmin())
				throw new CHttpException("You are not subscribed to this session!");
		}
	}



	public function run() {

		/* @var $cs CClientScript */
		$cs = Yii::app()->getClientScript();
		// Dynatree ("borrowed" by lms/player assets)
		$cs->registerScriptFile($this->getPlayerAssetsUrl() . '/js/jquery.dynatree.js');
		$cs->registerCssFile($this->getPlayerAssetsUrl() . '/css/ui.dynatree-modified.css');
		//our editing objects tree and launchpad
		$cs->registerScriptFile($this->getAssetsUrl() . '/js/classroom-los-editing.js');
		$cs->registerScriptFile($this->getAssetsUrl() . '/js/classroom-los-launcher.js');

		Yii::app()->event->raise('BeforeSessionInfoWidgetRender', new DEvent($this, array('sessionInfo'=>&$this)));

		$_countObjects = Yii::app()->db->createCommand("SELECT COUNT(*) FROM " . LearningOrganization::model()->tableName()
			. " WHERE objectType NOT IN('" . LearningOrganization::OBJECT_TYPE_AICC
			. "','" . LearningOrganization::OBJECT_TYPE_SCORMORG
			. "','" . LearningOrganization::OBJECT_TYPE_TINCAN . "', '') "
			. " AND idCourse = :id_course")->queryScalar(array(
			':id_course' => $this->courseModel->getPrimaryKey()
		));

		//render the course page
		$this->render('sessionInfo', array(
			'canEditCourse' => (
				$this->userLevel >= LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR
				|| Yii::app()->user->getIsGodadmin()
				|| (Yii::app()->user->getIsPu() && CoreUserPuCourse::isCourseAssignedToPU(Yii::app()->user->id, $this->session->course_id) && Yii::app()->user->checkAccess('/lms/admin/webinarsessions/mod'))
			),
			'_countObjects' => $_countObjects
		));
	}


	public function  getEvaluationScore($courseSession){
		$score = null;
		if(($this->session->evaluation_type == WebinarSession::EVAL_TYPE_MANUAL) || ($this->session->evaluation_type == WebinarSession::EVAL_TYPE_ON_JOIN)){
			$score =  $courseSession->evaluation_score;
		}elseif($this->session->evaluation_type == WebinarSession::EVAL_TYPE_ONLINE_TEST){
			$learningCourseUserModel = WebinarSession::model()->findByAttributes(array('id_session'=>$courseSession->id_session));
			if($learningCourseUserModel){
				$score = LearningCourseuser::getLastScoreByUserAndCourse($learningCourseUserModel->course_id, $courseSession->id_user, true);
				if(isset($score['score']) && $score['score']){
					$score['score'] = round(floatval($score['score']), 2);
				}
			}
		}

		return $score;
	}


}