<?php
switch ($data->objectType) {
	case LearningOrganization::OBJECT_TYPE_POLL: $_playUrl = Docebo::createAppUrl('lms:poll/default/axLaunchpad'); break;
	case LearningOrganization::OBJECT_TYPE_TEST: $_playUrl = Docebo::createAppUrl('lms:test/default/axLaunchpad'); break;
	default: $_playUrl = '#';
}
?>
<div class="item classroom-lo-play" data-id_object="<?= $data->idOrg ?>" data-type="<?= $data->objectType ?>" data-title="<?= $data->title ?>" data-play_url="<?= $_playUrl ?>" data-id_resource="<?= $data->idResource?>">
	<div class="primary row-fluid">
		<!-- STATUS -->
		<div class="item-col col-status">
			<?php

			//TODO: check LO prerequisites and accessibility

			//$_status = $data->learningCommontrack->status;
			$_track = LearningCommontrack::model()->findByAttributes(array(
				'idUser' => Yii::app()->user->id,
				'idReference' => $data['idOrg']
			));
			$_status = ($_track ? $_track->status : false);
			switch ($_status) {
				case LearningCommontrack::STATUS_ATTEMPTED: $_playCss = 'i-sprite is-play orange'; break;
				case LearningCommontrack::STATUS_COMPLETED: $_playCss = 'i-sprite is-circle-check green'; break;
				case LearningCommontrack::STATUS_FAILED: $_playCss = 'i-sprite is-play red'; break;
				//case LearningCommontrack::STATUS_AB_INITIO: $_playCss = 'i-sprite is-play grey'; break;
				default: $_playCss = 'i-sprite is-play grey'; break;
			}

			$_list = LearningCommontrack::getStatusLabelsList();
			if ($_status && isset($_list[$_status])) {
				$_statusText = $_list[$_status];
			} else {
				$_statusText = $_list[LearningCommontrack::STATUS_AB_INITIO];
			}

			echo CHtml::tag('span', array('class' => $_playCss, 'rel' => 'tooltip', 'title' => $_statusText));
/*
			$_playIcon = CHtml::link('', $_playUrl, array(
				'id' => 'classroom-lo-play-'.$data->idOrg,
				'class' => 'classroom-lo-play '.$_playCss,
				'data-id_object' => $data->idOrg,
				'data-type' => $data->objectType,
				'data-title' => $data->title,
			));
			echo $_playIcon;
*/
			?>
		</div>
		<!-- NAME -->
		<div class="item-col col-name text-colored">
			<?= $data->title ?>
		</div>
		<div class="clearfix"></div>
	</div>
</div>