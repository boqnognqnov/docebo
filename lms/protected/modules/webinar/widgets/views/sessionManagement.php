<?php
/* @var $course LearningCourse */

if(Yii::app()->user->getIsGodadmin() || Yii::app()->user->getIsAdmin() || $this->userCanAdminCourse)
	$this->render('_mainSessionActions', array('course' => $course));
?>

<h3 class="title-bold back-button"><?= Yii::t('dashboard', 'Sessions'); ?></h3>
<!--search box-->
<form id="session-grid-search-form" class="ajax-grid-form">
	<div class="filters-wrapper">
		<table class="filters">
			<tbody>
			<tr>
				<td class="table-padding">
					<table class="table-border">
						<tr>
							<td class="group">
								<?php echo ucfirst(Yii::t('statistic', '_FOR_month')); ?>
							</td>
							<td class="group">
								<?php
								$_monthsList = array(0 => Yii::t('standard', '_ALL'));
								$_currentMonth = date("m");
								for($i = 1; $i <= 12; $i++)
									$_monthsList[$i] = Yii::t('standard', '_MONTH_' . str_pad('' . $i, 2, "0", STR_PAD_LEFT));
								echo CHtml::dropDownList('selectMonth', 0, $_monthsList, array('id' => 'searchfilter-selectMonth'));
								?>
							</td>
							<td class="group">
								<?php echo ucfirst(Yii::t('standard', '_YEAR')); ?>
							</td>
							<td class="group">
								<?php
								$_yearsList = array(0 => Yii::t('standard', '_ALL'));
								$_currentYear = date("Y");
								$_tmpList = $course->getWebinarSessionYears();
								rsort($_tmpList);
								foreach($_tmpList as $_year)
									$_yearsList[$_year] = $_year;
								echo CHtml::dropDownList('selectYear', 0, $_yearsList, array('id' => 'searchfilter-selectYear'));
								?>
							</td>
							<td class="group" style="width: 60%;">
								<table>
									<tr>
										<td>
											<div class="input-wrapper">
												<input
													data-url="<?= Docebo::createAppUrl('lms:ClassroomApp/session/axSearchAutoComplete') ?>"
													data-type="core"
													class="typeahead"
													data-course_id="<?= $course->getPrimaryKey() ?>"
													id="advanced-search-session"
													autocomplete="off"
													type="text"
													name="searchText"
													placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"
													data-autocomplete="true"/>
												<span>
													<button class="search-btn-icon"></button>
												</span>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
</form>

<!--locations grid-->
<div id="grid-wrapper" class="">
	<?php
	$_columnList = array();

	$_columnList[] = array(
		'name' => 'name',
		'type' => 'raw',
		'value' => '$data["name"];',
		'header' => Yii::t('standard', '_NAME')
	);

	$_columnList[] = array(
		'name' => 'date_begin',
		'type' => 'raw',
		'value' => function($data) {
			/* @var $data WebinarSession */
			return WebinarSession::getStartDate($data->id_session);
		},
		'header' => Yii::t('standard', '_START')
	);

	$_columnList[] = array(
		'name' => 'date_begin',
		'type' => 'raw',
		'value' => function($data) {
			/* @var $data WebinarSession */
			return WebinarSession::getEndDate($data->id_session);
		},
		'header' => Yii::t('standard', '_END')
	);

	$_columnList[] = array(
		'type' => 'raw',
		'value' => '$data->getTotalHours();',
		'headerHtmlOptions' => array('class' => 'center-aligned'),
		'header' => Yii::t('standard', '_HOURS'),
		'cssClassExpression' => '"center-aligned"'
	);

	$_columnList[] = array(
		'type' => 'raw',
		'value' => function($data) {
			/* @var $data WebinarSession */
			$toolCodes = Yii::app()->getDb()->createCommand()
				->selectDistinct('webinar_tool')
				->from(WebinarSessionDate::model()->tableName())
				->where('id_session=:idS', array(':idS' => $data->id_session))
				->queryColumn();
			$tools = WebinarSession::toolList();

			$toolNames = array();
			foreach($toolCodes as $toolCode) {
				$toolNames[] = (isset($tools[$toolCode])) ? $tools[$toolCode] : '';
			}

			if(count($toolNames) > 1) {
				return '<span data-toggle="tooltip" rel="tooltip" data-html="true" title="' . implode('<br/>', $toolNames) . '">' . $data->getToolName() . '</span>';
			} else {
				return $data->getToolName();
			}


		},
		'header' => Yii::t('webinar', 'Tool')
	);

	$pUserRights = Yii::app()->user->checkPURights($course->getPrimaryKey());
	if(Yii::app()->user->getIsGodadmin() || ($pUserRights->all && Yii::app()->user->checkAccess("enrollment/create") || Yii::app()->user->checkAccess("/lms/admin/webinarsessions/assign"))) {
		$_columnList[] = array(
			'type' => 'raw',
			'value' => '$data->renderWaiting();',
			'headerHtmlOptions' => array('class' => 'center-aligned'),
			'header' => Yii::t('standard', '_WAITING'),
			'cssClassExpression' => '"center-aligned"'
		);
	}

	$isInstructor = LearningCourseuser::isUserLevel(Yii::app()->user->id, $course->getPrimaryKey(), LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR);
	$_columnList[] = array(
		'type' => 'raw',
		'value' => '$data->renderEnrolled('.($isInstructor? 'true' : 'false').');',
		'headerHtmlOptions' => array('class' => 'center-aligned'),
		'header' => Yii::t('standard', 'Enrolled'),
		'cssClassExpression' => '"center-aligned"'
	);

	if(!Yii::app()->user->getIsGodadmin() && !Yii::app()->user->getIsAdmin() && !$isInstructor) {
//		$_columnList[] = array(
//			'type' => 'raw',
//			'value' => '$data->renderInstructorAction("print");',
//		);
//		$_columnList[] = array(
//			'type' => 'raw',
//			'value' => '$data->renderInstructorAction("attendance");',
//		);
//		$_columnList[] = array(
//			'type' => 'raw',
//			'value' => '$data->renderInstructorAction("details");',
//		);
	} else {
		$_columnList['actions'] = array(
			'type' => 'raw',
			'value' => '$data->renderSessionActions("webinar.widgets.views._sessionTableActions");',
		);
	}

	$PU_hideOtherSessions = false;
	if(Yii::app()->user->isPu) {
		$pUserRights = Yii::app()->user->checkPURights($course->getPrimaryKey());
		$isPuWithSessionEditPerm = $pUserRights->all && (Yii::app()->user->checkAccess("/lms/admin/webinarsessions/mod") || Yii::app()->user->checkAccess("/lms/admin/webinarsessions/assign") || ( Yii::app()->user->checkAccess("/lms/admin/webinarsessions/view") && !Yii::app()->user->checkAccess("/lms/admin/webinarsessions/add") && !Yii::app()->user->checkAccess("/lms/admin/webinarsessions/mod")));
		if(!$isPuWithSessionEditPerm) {
			$PU_hideOtherSessions = true;
		}
	}

	$this->widget('DoceboCGridView', array(
		'id' => 'sessions-management-grid',
		'htmlOptions' => array('class' => 'grid-view'),
		'dataProvider' => WebinarSession::model()->dataProvider($course->getPrimaryKey(), $PU_hideOtherSessions, true),
		'columns' => $_columnList,
		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'pager' => array(
			'class' => 'admin.protected.extensions.local.pagers.DoceboLinkPager',
		),
		'ajaxUpdate' => 'sessions-management-grid-all-items',
		'beforeAjaxUpdate' => 'function(id, options) {

            }',
		'afterAjaxUpdate' => 'function(id, data){
				$(document).controls();
				 $(\'[data-toggle="tooltip"]\').tooltip();
            }'
	));
	?>
</div>
<br/>

<script type="text/javascript">

	function resetSearchPlaceholder() {
		var $search = $('#advanced-search-session');
		if ($search.val() == "<?= Yii::t('standard', '_SEARCH'); ?>")
			$('#advanced-search-session').val('');
	}

	$(function () {
		$('#docebo-bootcamp-sessions').doceboBootcamp();

		var updateGrid = function () {
			$.fn.yiiGridView.update('sessions-management-grid', {
				data: $("#session-grid-search-form").serialize()
			});
		};
		$('#searchfilter-selectMonth').bind("change", function (e) {
			updateGrid();
		});
		$('#searchfilter-selectYear').bind("change", function (e) {
			updateGrid();
		});
		$('#session-grid-search-form').bind('submit', function (e) {
			updateGrid();
		});

		$(document).delegate(".modal-create-webinar-session", "dialog2.content-update", function () {
			var e = $(this), autoclose = e.find("a.auto-close");
			if (autoclose.length > 0) {
				e.find('.modal-body').dialog2("close");
				updateGrid();
			}
		});
	});
</script>