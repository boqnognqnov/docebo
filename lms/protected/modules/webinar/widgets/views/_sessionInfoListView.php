<?php /* @var $data array  */ ?>
<div class="item">
    <div class="primary row-fluid">
        <div class="item-col col-status">
            <?php
            $css = '';
            $_today = strtotime(Yii::app()->localtime->getLocalNow('Y-m-d'));
            $_day = strtotime($data->day);
            if ($_today > $_day) {
                $css = 'green';
            }
            else if ($_today == $_day) {
                $css = 'orange';
            }
            else {
                $css = 'grey';
            }
            $html = '<span class="i-sprite is-circle-check '.$css.'"></span>';
            echo $html;
            ?>
        </div>
        <div class="item-col col-name text-colored">
            <?= $data->name ?>
        </div>
        <div class="item-col col-toggle_handle">
            <?= CHtml::tag('span', array('class'=>'toggle-handle docebo-sprite ico-arrow-bottom', 'data-expand-class'=>'ico-arrow-top', 'data-retract-class'=>'ico-arrow-bottom'), '') ?>
        </div>
        <div class="item-col col-location">
            <?= $data->ltLocation->name ?>
        </div>
        <div class="item-col col-time">
            <?= $data->renderDaySchedule(' | ') ?>
        </div>
        <div class="item-col col-date">
            <?= Yii::app()->localtime->toLocalDate($data->day) ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row-fluid secondary hide">

        <div class="span6">
            <h4 class="info-section-title"><?= Yii::t('classroom', 'Location information') ?></h4>
            <div class="row-fluid">
                <div class="span6">
                    <div class="info-section">
                        <div class="info-section-icon">
                            <span class="classroom-sprite marker-tiny"></span>
                        </div>
                        <div class="info-section-content has-icon">
                            <?= $data->location->name ?><br/>
                            <?= $data->location->renderAddress() ?>
                        </div>
                    </div>
                </div>
                <div class="span6">
                    <?php if (!empty($data->location->telephone)) : ?>
                    <div class="info-section">
                        <div class="info-section-icon">
                            <span class="classroom-sprite phone"></span>
                        </div>
                        <div class="info-section-content has-icon" style="padding-top: 3px;"><?= $data->location->telephone ?><br/></div>
                    </div>
                    <?php endif; ?>
                    <?php if (!empty($data->location->email)) : ?>
                    <div class="info-section">
                        <div class="info-section-icon">
                            <span class="classroom-sprite mail"></span>
                        </div>
                        <div class="info-section-content has-icon text-colored"><?= $data->location->email ?></div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>

            <?php if ($data->ltClassroom) : ?>
            <h4 class="info-section-title"><?= Yii::t('classroom', 'Classroom information') ?></h4>
            <div class="row-fluid">
                <div class="span6">
                    <div class="info-section">
                        <div class="info-section-icon">
                            <span class="classroom-sprite classroom"></span>
                        </div>
                        <div class="info-section-content has-icon">
                            <?= $data->ltClassroom->name ?><br/>
                            <?= DoceboHtml::readmore($data->ltClassroom->details, 200) ?>
                        </div>
                    </div>
                    <?php if (isset($data->ltClassroom->seats)) : ?>
                    <div class="info-section">
                        <div class="info-section-icon">
                            <span class="classroom-sprite users"></span>
                        </div>
                        <div class="info-section-content has-icon" style="padding-top: 2px;"><?= Yii::t('classroom', '{count} available seats', array('{count}'=>$data->ltClassroom->seats)) ?><br/></div>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="span6">
                    <?php if (!empty($data->ltClassroom->equipment)) : ?>
                    <div class="info-section">
                        <div class="info-section-icon">
                            <span class="classroom-sprite settings"></span>
                        </div>
                        <div class="info-section-content has-icon"><?= $data->ltClassroom->equipment ?></div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php endif; ?>

            <?php if ($data->ltLocation->hasExtraInfo() && !empty($data->ltLocation->address)) : ?>
            <h4 class="info-section-title"><?= Yii::t('classroom', 'Location map') ?></h4>
            <div class="info-section">
                <div class="map-preview-container">
                    <?= CHtml::hiddenField('address', $data->ltLocation->renderAddress()) ?>
                    <span class="classroom-sprite map-placeholder"></span>
                    <div class="location-map-canvas"></div>
                </div>
            </div>
            <?php endif; ?>
        </div>

        <div class="span6">
            <?php if ($data->ltLocation->hasExtraInfo()) : ?>
                <?php if (!empty($data->ltLocation->reaching_info)) : ?>
                <h4 class="info-section-title"><?= Yii::t('classroom', 'How to reach the location') ?></h4>
                <div class="info-section">
                    <div class="info-section-content"><?= Yii::app()->htmlpurifier->purify($data->ltLocation->reaching_info) ?></div>
                </div>
                <?php endif; ?>
                <?php if (!empty($data->ltLocation->accomodations)) : ?>
                <h4 class="info-section-title"><?= Yii::t('classroom', 'Suggested accomodations') ?></h4>
                <div class="info-section">
                    <div class="info-section-content"><?= Yii::app()->htmlpurifier->purify($data->ltLocation->accomodations) ?></div>
                </div>
                <?php endif; ?>
                <?php if (!empty($data->ltLocation->other_info)) : ?>
                <h4 class="info-section-title"><?= Yii::t('classroom', 'Other info') ?></h4>
                <div class="info-section">
                    <div class="info-section-content"><?= Yii::app()->htmlpurifier->purify($data->ltLocation->other_info) ?></div>
                </div>
                <?php endif; ?>
                <?php if (!empty($data->ltLocation->photos)) : ?>
                <h4 class="info-section-title"><?= Yii::t('classroom', 'Location photos') ?></h4>
                <div class="info-section">
                    <div class="info-section-content">
                        <?php foreach ($data->ltLocation->photos as $_photo) : ?>
                            <a href="<?= $_photo->getUrl() ?>" class="thumbnail fancybox" rel="gallery">
                                <img src="<?= $_photo->getUrl() ?>" alt=""/>
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
                <?php endif; ?>
            <?php else: ?>

                <?php if (!empty($data->ltLocation->address)) : ?>
                    <h4 class="info-section-title"><?= Yii::t('classroom', 'Location map') ?></h4>
                    <div class="info-section">
                        <div class="map-preview-container">
                            <?= CHtml::hiddenField('address', $data->ltLocation->renderAddress()) ?>
                            <span class="classroom-sprite map-placeholder"></span>
                            <div class="location-map-canvas"></div>
                        </div>
                    </div>
                <?php endif; ?>

            <?php endif; ?>
        </div>
    </div>
</div>