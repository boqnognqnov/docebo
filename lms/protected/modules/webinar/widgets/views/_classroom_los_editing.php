<div id="classroom-los-editing-container">

	<div id="player-arena-container">

		<!-- ADD LO BUTTON -->
		<a id="arena-board-top" href="#arena-board-top"></a>

		<div class="row-fluid" data-bootstro-id="bootstroLaunchpad" id="player-arena-content">

			<!-- INLINE Edit pane -->
			<div id="player-lo-edit-inline-panel"></div>

			<!-- LAUNCHPAD  -->
			<div id="player-arena-launchpad"></div>

			<div class="clearfix"></div>

			<!-- LO MANAGER -->
			<div id="player-lo-manage-panel" class="player-arena-board">
				<div class="row-fluid player-lo-manage-header">
					<div class="span4">
						<span class="lom-title"><?php echo Yii::t('organization', 'Folders'); ?></span>
					</div>
					<div class="span8">
						<span class="lom-title"><?php echo Yii::t('standard', 'Training materials'); ?></span>


						<?php if (1==1/*$this->userCanAdminCourse*/) : ?>
							<?php
							$typeClass = "dev-desktop-tablet";
							if ($this->courseModel->isMobile()) {
								$typeClass = "dev-smartphone";
							}
							?>
							<div id="player-manage-add-lo-button" class="pull-right" data-bootstro-id="bootstroAddTrainingResources">
								<div class="dropdown">

									<!-- Toggle -->
									<i class="course-device-type devices-sprite <?= $typeClass ?>">&nbsp;</i>

									<a href="#" class="btn-docebo green big" data-toggle="dropdown">
										<span class="p-sprite plus-objects-white-green"></span> <?php echo Yii::t('organization', 'ADD TRAINING RESOURCES'); ?> <span class="p-sprite dropdown-objects-white-green"></span>
									</a>

									<ul class="dropdown-menu pull-right">

										<!-- HINT -->
										<li id="add-lo-type-hint" data-default-text="<?php echo Yii::t('organization', 'Select learning object type to add'); ?>">
											<span class="span12 text-left">
												<?php echo Yii::t('organization', 'Select learning object type to add'); ?>
											</span>
										</li>

										<!-- LO Types -->
										<!--<li>
											<span class="span3 add-lo-operation-type">
												<span><?php echo Yii::t('organization', 'ORGANIZE'); ?></span>
											</span>
											<span class="span9 add-lo-type-description"
														data-hint-icon-class="p-sprite new-folder"
														data-hint-text="<?php echo Yii::t('organization', 'Create new folder at the end of the course path'); ?>">
												<a id="create-folder" class="i-sprite-white p-hover" href="#"><span class="i-sprite is-add-folder"></span> <?php echo Yii::t('standard', '_NEW_FOLDER'); ?></a>
											</span>
										</li>-->

										<?php if (!$this->courseModel->isMobile()): ?>
											<li>
												<span class="span3 add-lo-operation-type">
													<span><?= Yii::t('standard', '_CREATE') ?></span>
												</span>
												<span class="span9 add-lo-type-description"
															data-hint-icon-class="p-sprite lo-poll"
															data-hint-text="<?= Yii::t('organization', 'Create a Poll'); ?>">
													<a class="lo-create-poll i-sprite-white p-hover" data-lo-type='<?= LearningOrganization::OBJECT_TYPE_POLL ?>' href="#"><span class="i-sprite is-poll"></span> <?php echo Yii::t('storage', '_LONAME_poll'); ?></a>
												</span>
											</li>
										<?php endif; ?>


										<li>
											<span class="span3"></span>
											<span class="span9 add-lo-type-description"
														data-hint-icon-class="p-sprite lo-test"
														data-hint-text="<?= Yii::t('organization', 'Create your own test assessment, using the Docebo built-in assessment creation tool!'); ?>">
												<a class="lo-create-test i-sprite-white p-hover" data-lo-type='<?= LearningOrganization::OBJECT_TYPE_TEST ?>' href="#"><span class="i-sprite is-test"></span> <?php echo Yii::t('storage', '_LONAME_test'); ?></a>
											</span>
										</li>

									</ul>
								</div>
							</div>
						<?php endif; ?>


					</div>
				</div>
				<div class="player-arena-overall-container">
					<div class="row-fluid">
						<div class="span4">
							<div id="player-arena-overall-folders" class="player-arena-overall-column">
								No folders
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="span8">
							<div id="player-arena-overall-objlist" class="player-arena-overall-column player-arena-overall-objlist"></div>
						</div>
					</div>
				</div>
			</div>
			<!-- /LO MANAGER -->
		</div>

		<!--<div class="player-quicknav-wrapper" id="player-quicknavigation"></div>-->

		<!--<?php //$this->widget('OverlayHints'); ?>-->
	</div>


</div>


<script type="text/javascript">
	$(function() {

		Arena.test = new LoTest({});
		Arena.poll = new LoPoll({});

		Arena.init({
			course_id: <?= $this->courseModel->getPrimaryKey() ?>,
			courseInfo: {
				idCourse: <?= $this->courseModel->getPrimaryKey() ?>,
				code: <?= CJSON::encode($this->courseModel->code) ?>,
				name: <?= CJSON::encode($this->courseModel->name) ?>,
				mpidCourse: <?= CJSON::encode($this->courseModel->mpidCourse) ?>,
				isMpCourse: <?= CJSON::encode($this->courseModel->mpidCourse > 0) ?>,
				//availableSeats: ,
				userHasSeat: <?= CJSON::encode(LearningLogMarketplaceCourseLogin::userHasSeat($this->courseModel->getPrimaryKey(), Yii::app()->user->id)) ?>,
				mpSeatsUnlimited: <?= CJSON::encode($this->courseModel->mpSeatsUnlimited()) ?>
			},
			getCourseLearningObjectsTreeUrl: "<?= Yii::app()->controller->createUrl('axGetCourseLearningObjectsTree') ?>",
			createFolderDialogUrl: "<?= Yii::app()->controller->createUrl('axCreateFolder', array('course_id' => $this->courseModel->getPrimaryKey())) ?>",
			uploadDialogUrl: "<?= Yii::app()->controller->createUrl('axLoUploadDialog') ?>",
			uploadFilesUrl: "<?= Yii::app()->controller->createUrl('axUpload') ?>",
			saveUploadedCoursedocUrl: "<?= Yii::app()->controller->createUrl('block/axSaveCoursedoc') ?>",
			openInCentralRepositoryUrl: "<?= (Yii::app()->user->getIsGodadmin() ? Yii::app()->createAbsoluteUrl("centralrepo/centralRepo/index") : "")?>",
			csrfToken: "<?= Yii::app()->request->csrfToken ?>",
			deleteLoUrl: "<?= Yii::app()->controller->createUrl('axDeleteLo', array('course_id' => $this->courseModel->getPrimaryKey())) ?>",
			sortLearningObjectUrl: "<?= Yii::app()->controller->createUrl('axReorderLo') ?>",
			moveInFolderLearningObjectUrl: "<?= Yii::app()->controller->createUrl('axMoveLoToFolder') ?>",
			userCanAdminCourse: true,
			axEditLoPrerequisitesUrl: "<?= Yii::app()->controller->createUrl('axEditLoPrerequisites') ?>",
			onLoadArenaMode: "<?= Yii::app()->request->getParam("mode", "view_course") ?>",
			createTestUrl: "<?= Yii::app()->controller->createUrl('//test/default/axCreateTest') ?>",
			createPollUrl: "<?= Yii::app()->controller->createUrl('//poll/default/axCreatePoll') ?>",
			isMobileCourse: <?= ($isMobile ? 'true' : 'false') ?>,
			reloadSocialRatingUrl: "<?= Yii::app()->controller->createUrl('axReloadSocialRatingPanel', array('courseId' => $this->course_id))?>"
		});
	});
</script>

