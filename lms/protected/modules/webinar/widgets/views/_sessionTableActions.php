<? /* @var $model LtCourseSession */ ?>
<?php
$pUserRights = Yii::app()->user->checkPURights($idCourse);
$isSubscribed = WebinarSessionUser::isSubscribed(Yii::app()->user->id, $idSession);
$isInstructor = LearningCourseuser::isUserLevel(Yii::app()->user->id, $idCourse, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR);

$canView = (Yii::app()->user->getIsGodadmin() //godadmin
	//PU with view permissions and assigned course
	|| ($pUserRights->isPu && $pUserRights->courseIsAssigned && Yii::app()->user->checkAccess('/lms/admin/webinarsessions/view'))
	//instructor, subscribed to the session
	|| ($isInstructor && $isSubscribed)
);
$canEdit = (Yii::app()->user->getIsGodadmin() //godadmin
	//PU with "mod" permissions OR with "add" permissions and the course is created by him
	|| ($pUserRights->isPu && $pUserRights->courseIsAssigned && (Yii::app()->user->checkAccess('/lms/admin/webinarsessions/mod') ||
					(Yii::app()->user->checkAccess('/lms/admin/webinarsessions/add') && $model->created_by==Yii::app()->user->getIdst()))));
?>
<div class="node-actions">
    <ul>

		<? if($canView): ?>
			<li class="node-action">
            <i class="classroom-sprite details"></i>
            <a style="padding: 0px 0px 0px 8px; background: none;" href="<?= Docebo::createLmsUrl('player/training/webinarSession', array('session_id' => $idSession, 'course_id' => $idCourse)) ?>">
                <?php echo Yii::t('player', 'Student view'); ?>
            </a>
        </li>
	    <? endif; ?>

        <?php if ($canEdit): ?>
		<li class="node-action"><hr></li>
        <li class="node-action">
            <a class="open-dialog course-copy"
               href="<?= Docebo::createLmsUrl('webinar/session/axCopySession', array('id_session' => $idSession, 'course_id' => $idCourse)) ?>"
               data-modal-class="modal-copy-session"
               data-dialog-class="modal-copy-session">
                <?php echo Yii::t('standard', '_MAKE_A_COPY'); ?>
            </a>
        </li>
        <? endif; ?>

        <?php if ($canEdit){ ?>
        <li class="node-action">
            <a class="open-dialog node-edit"
               href="<?=Docebo::createLmsUrl('webinar/session/edit', array('course_id' => $idCourse, 'id_session' => $idSession))?>"
               class="open-dialog"
               data-dialog-class="modal-create-webinar-session"
               data-before-send="beforeSessionSave(data)">
                <?php echo Yii::t('standard', '_MOD'); ?>
            </a>
        </li>
        <?php } ?>
	    <?php if(!$pUserRights->isPu  || Yii::app()->user->checkAccess('/lms/admin/webinarsessions/del')) { ?>
		    <li class="node-action">
			    <a class="open-dialog delete-node"
			       data-dialog-class="modal-delete-session"
			       href="<?= Docebo::createLmsUrl('webinar/session/axDeleteSession', array('id_session' => $idSession, 'course_id' => $idCourse)) ?>"
			       rel="delete-session">
				    <?php echo Yii::t('standard', '_DEL'); ?>
			    </a>
		    </li>
	    <?php } ?>
    </ul>
</div>