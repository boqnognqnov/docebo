<?php
/* @var $this SessionInfo */
/* @var $webinarSession WebinarSession */
/* @var $sessionDates WebinarSessionDate[] */

$webinarSession = $this->session;
$sessionDates = $webinarSession->getDateAttendanceList();
$toolList = WebinarSession::toolList();
$userValidation = $this->session->validateWebinarUser();
$isUserEnrolled = (Yii::app()->user->getIsGodadmin() || $this->session->isUserEnrolledInSession());

?>
<?php if($canEditCourse): ?>
	<div style="margin-bottom: 20px;">
		<a class="docebo-back"
		   href="<?= Docebo::createAppUrl('lms:player/training/webinarSession', array('course_id' => $this->courseModel->getPrimaryKey())) ?>">
			<span><?= Yii::t('standard', '_BACK') ?></span>
		</a>
		<h3 style="display: inline-block"><?= CHtml::encode($this->session->name) ?></h3>
	</div>
<?php endif; ?>

<div id="player-arena-launchpad"></div>

<div id="session-info-container" class="session-info-container">

	<div id="dates-listing">
		<h3 class="session-name"><?= $webinarSession->name ?></h3>

		<div>
			<?= $webinarSession->description; ?>
		</div>

		<?php if($webinarSession->evaluation_type == WebinarSession::EVAL_TYPE_ON_JOIN): ?>
			<br/>
			<?php $this->widget('common.widgets.warningStrip.WarningStrip', array(
				'id' => 'warning-strip-evaluation',
				'message' => Yii::t('webinar', 'You must attend at least {needed}/{total} session dates to complete this course', array(
					'{needed}' => intval($webinarSession->min_attended_dates_for_completion),
					'{total}' => count($sessionDates),
				)),
			)); ?>
		<?php endif; ?>

		<br/>

		<div id="dates-listing">
			<div class="date-row headers">
				<div class="row-fluid">
					<div class="span5"><?=Yii::t('standard', 'Name')?></div>
					<div class="span2"><?=Yii::t('standard', 'Date')?></div>
					<div class="span2"><?=Yii::t('standard', 'Duration')?></div>
					<div class="span3"><?=Yii::t('standard', 'Status')?></div>
				</div>
			</div>
			<?php foreach($sessionDates as $date): ?>
				<?php
				$_sessionDateIndex = '_'.$date['id_session'].'_'.$date['day'];

				$remainingSecondsToBegin = $date['remainingTimeToBegin'];
				$remainingSecondsToEnd = $date['remainingTimeToEnd'];
				$advanceJoinPossibleSeconds = 60 * WebinarSessionDate::getJoinInAdvanceButtonTimeoutMinutes(Yii::app()->user->getIdst(), $date['id_session'], $date['day']);

				$day = $date['day'];
				$webinarTool = $date['webinar_tool'];
				$webinarToolAccountId = $date['id_tool_account'];
				$webinarToolModel = WebinarToolAccount::model()->findByPk($webinarToolAccountId);
				$formattedDate = Yii::app()->localtime->toLocalDate($day, 'short');

				$recordingModel = WebinarSessionDateRecording::model()->findByAttributes(array(
					'id_session' => $date['id_session'],
					'day' => $date['day']
				));

				// format attendance label
				if($date['isAttendedByUser']){
					$label = '<span class="webinar-label label-green">'.Yii::t('webinar', 'Attended').'</span>';
				} elseif ($remainingSecondsToBegin > 0) {
					$label = '<span class="webinar-label label-blue">'.Yii::t('webinar', 'Upcoming').'</span>';
				}  else {
					$label = '<span class="webinar-label label-black">'.Yii::t('webinar', 'Not attended').'</span>';
				}
				?>
				<div class="date-row date sessionsRow collapsed"
					 data-id-session="<?= $date['id_session'] ?>"
					 data-day="<?= $date['day'] ?>"
					 data-remaining-seconds-to-begin="<?= $remainingSecondsToBegin ?>"
					 data-remaining-seconds-to-end="<?= $remainingSecondsToEnd ?>"
					 data-join-in-advance-settings="<?= $advanceJoinPossibleSeconds ?>"
					 data-permissions="<?=($canEditCourse)?'fullAdminRights123':'basic'?>">
					<div class="row-fluid collapsedOnly" id="label-container<?= $_sessionDateIndex ?>">
						<div class="span5"><?= $date['name'] ?></div>
						<div class="span2"><?= $formattedDate ?></div>
						<div class="span2">
							<?= (WebinarSessionDate::getDurationInFormat($date['duration_minutes'], '%H')*1);?>h <?= WebinarSessionDate::getDurationInFormat($date['duration_minutes'], '%I');?> min
						</div>
						<div class="span3">
							<span class="fa fa-chevron-down pull-right big-font"></span>
							<?=$label?>
						</div>
					</div>

					<div class="details">
						<div class="row-fluid">
							<div class="span5">
								<h3 class="name">
									<?= $date['name'] ?>
								</h3>
								<div class=description>
									<?= $date['description'] ?>
								</div>
							</div>
							<div class="span2 text-center">
								<i class="i-sprite large is-calendar-date"></i>
								<br/>
								<label
									class="lv-details-blue"><?= Yii::app()->localtime->toLocalDateTime($date['utcBeginTimestamp']) ?></label>
								<?= Yii::t('standard', '_START_DATE') ?>
							</div>
							<div class="span2 text-center">
								<i class="i-sprite large is-clock"></i>
								<br/>
								<label class="lv-details-blue">
									<?= (WebinarSessionDate::getDurationInFormat($date['duration_minutes'], '%H')*1);?>h <?= WebinarSessionDate::getDurationInFormat($date['duration_minutes'], '%I');?> min
								</label>
								<?= Yii::t('statistic', '_TIME_IN') ?>
							</div>
							<div class="span3 text-center">
								<i class="i-sprite large is-terminal"></i>
								<br/>
								<label class="lv-details-blue"><?= $toolList[$date['webinar_tool']] ?></label>
								<?= Yii::t('webinar', 'Tool') ?>
							</div>
						</div>

						<br/><br/>

						<div class="timers" id="timers<?= $_sessionDateIndex ?>">
							<div class="pull-left">
								<span><?=Yii::t('standard', 'Status')?>: </span><?=$label?>
								<?php if(!empty($webinarToolModel->additional_info)){ ?>
								<span class="webinarAdditionalInfo">
									<span class="webinarAdditionalInfoLeft">&#xf05a;</span>
									<span class="webinarAdditionalInfoRight"><span><?=nl2br($webinarToolModel->additional_info)?></span></span>
								</span>
								<?php } ?>
							</div>
							<div class="pull-right">
							<span class="timer-to-begin" style="display: none;">
								<?= Yii::t('webinar', 'Your webinar starts in') ?>
								<span></span>
							</span>
							<span class="join-button" style="display: none;">
								<?php
								$directJoinPossible = WebinarSession::model()->isToolDirectJoin($webinarTool);
								$webinarDateSession = WebinarSessionDate::model()->findByAttributes(array(
									'day' => $date['day'],
									'id_session' => $this->session->id_session,
								));
								$joinUrl = $directJoinPossible
									? Docebo::createLmsUrl('webinar/session/join', array('id_session' => $this->session->id_session, 'day' => $date['day'], 'course_id' => Yii::app()->request->getParam('course_id', null)))
									: $webinarDateSession->getWebinarUrl();
								?>

									<?php if(!is_array($userValidation) || !$userValidation['valid'] || !$isUserEnrolled ): //meeting is not joinable, prevent join button to be clicked ?>
										<a href="#" onclick="return false;" class="btn btn-submit disabled"><?= Yii::t('conference', 'Join'); ?></a>
									<?php else : // Meeting is joinable ?>
										<a href="<?= $joinUrl ?>" target="_blank"
											 class="btn btn-submit">
											<?= Yii::t('conference', 'Join'); ?>
										</a>
									<?php endif; ?>
								</span>
							<span class="timer-to-end" style="display: none;">
								<?= Yii::t('webinar', 'Your webinar ends in') ?>
								<span></span>
							</span>
							<span class="recording-buttons" style="display:none;">
							<?php
								if ($canEditCourse && (empty($recordingModel) || in_array($recordingModel->status, array(
									WebinarSessionDateRecording::RECORDING_STATUS_NON_EXISTANT,
									WebinarSessionDateRecording::RECORDING_STATUS_FAILED,
									WebinarSessionDateRecording::RECORDING_STATUS_DOWNLOADING
								)))) {
									if ($recordingModel->status == WebinarSessionDateRecording::RECORDING_STATUS_DOWNLOADING) {
										// NOTE: if we are in downloading status it means that we have aborted a previously started download operation
										$recordingModel->path = '-';
										$recordingModel->status = WebinarSessionDateRecording::RECORDING_STATUS_NON_EXISTANT;
										$recordingModel->progress = NULL;
										$recordingModel->save();
									}
									// Show "add recording" button only if no previous recording exists, the previous one got deleted or was failed
									echo CHtml::link(
										Yii::t('webinar', 'Add recording'),
										Docebo::createLmsUrl('webinar/session/addRecording', array(
											'session_id' => Yii::app()->request->getParam('session_id', null),
											'course_id' => Yii::app()->request->getParam('course_id', null),
											'date' => $date['day'],
											//'webinartool' => $webinarTool //NOTE: not needed, we already have it in the session date record
										)),
										array(
											'class' => 'btn btn-docebo green open-dialog recording-actions',
											'data-dialog-class' => 'addRecordingModal',
											'closeOnEscape' => "false",
											'closeOnOverlayClick' => "false",
										)
									);
								} elseif ($canEditCourse && ($recordingModel->status == WebinarSessionDateRecording::RECORDING_STATUS_CONVERTING)) {
									// Show "converting..." button during conversion
									echo '<div class="btn btn-docebo btn-dummy blue recording-actions converting" data-id_session="'.$recordingModel->id_session.'" data-day="'.$recordingModel->day.'">';
									echo '<i class="fa fa-spin fa-spinner"></i>&nbsp;&nbsp;'.Yii::t('standard', 'Converting');
									echo '...</div>';
								} elseif ($recordingModel->status == WebinarSessionDateRecording::RECORDING_STATUS_COMPLETED) {
									// Always show the view button
									?>
									<span class="video-recording-download recording-actions">
										<?php
										/**
										 * NOTE: some webinar tools require different ways to be displayed. E.g. Adobeconnect grabbed recordings should be played
										 * in a separate browser tab instead that a player into a dialog (due ta various issues).
										 */
										$_useStandardButton = true;
										if ($recordingModel->type == WebinarSessionDateRecording::RECORDING_TYPE_API_LINK) {
											$_tool = WebinarTool::getById($date['webinar_tool']);
											if (!empty($_tool) && $_tool->allowApiFindRecording() && $_tool->getPlayRecordingType() == WebinarTool::PLAY_RECORDING_TYPE_EXTERNAL) {
												$_useStandardButton = false;
												echo CHtml::link(
													Yii::t('webinar', 'View recording'), //button text
													$recordingModel->path, //url to be opened
													array( //link options
														'class' => 'btn btn-docebo green',
														'target' => '_blank'
													)
												);
											}
										}
										if ($_useStandardButton) {
										?>
										<a class="btn btn-docebo green" href="javascript:;" onclick="(function() { viewRecording(<?= $date['id_session'] ?>, '<?= $date['day'] ?>'); })()">
											<?php echo Yii::t('webinar', 'View recording') ?>
										</a>
										<?php
										}
										?>
									</span>
									<?php

									if ($canEditCourse) {
										echo "<a href='".Docebo::createLmsUrl('webinar/session/deleteRecording', array(
												'session_id' => Yii::app()->request->getParam('session_id', null),
												'date' => $date['day']
											))."' class='deleteRecording open-dialog recording-actions' "
											." data-dialog-class='deleteRecordingModal'><span></span></a>";
									}
								}
							?>
							</span>

							</div>
							<div class="clearfix"></div>
						</div>
					</div>

				</div>
			<? endforeach; ?>
		</div>
	</div>

	<br/>

	<div class="row-fluid" style="margin-top: 25px;">
		<h3 class="span12 title-bold">
			<?= Yii::t('standard', '_LEARNING_OBJECTS') ?>:
		</h3>
	</div>

	<?php
	if($_countObjects > 0):
		?>

		<div class="classroom-objects-grid">
			<?php
			$this->widget('DoceboCListView', array(
				'id' => 'classroom-objects-list',
				'htmlOptions' => array('class' => 'docebo-list-view'),
				'template' => '{header}{items}{pager}',
				'dataProvider' => LearningOrganization::model()->dataProviderClassroomLOs($this->courseModel->getPrimaryKey()),
				'itemView' => '_classroomLOsListView',
				'afterAjaxUpdate' => 'function(){attachEvents();}',
				'columns' => array(
					array(
						'name' => 'status',
						'header' => '&nbsp;'//Yii::t('standard', '_TYPE')
					),
					array(
						'name' => 'name',
						'header' => Yii::t('standard', '_NAME')
					)
				)
			));
			?>
		</div>
		<br/>
		<script type="text/javascript">
			$(function () {
				$(function () {
					attachEvents();
					Arena.init({
						course_id: <?= $this->courseModel->getPrimaryKey() ?>,
						uploadFilesUrl: "<?= Docebo::createLmsUrl('player/training/axUpload', array('course_id' => $this->courseModel->getPrimaryKey())) ?>",
						csrfToken: "<?=Yii::app()->request->csrfToken ?>"
					});
				});
			});
			function attachEvents() {
				var launcher = new ClassroomLoLauncher({
					course_id: <?= $this->courseModel->getPrimaryKey() ?>,
					session_id: <?= $this->session->getPrimaryKey() ?>,
					mpidCourse: <?= CJSON::encode($this->courseModel->mpidCourse) ?>,
					isMpCourse: <?= CJSON::encode(($this->courseModel->mpidCourse > 0)) ?>,
					mpSeatsUnlimited: <?= CJSON::encode($this->courseModel->mpSeatsUnlimited()) ?>,
					availableSeats: 0, //it seems that this parameter isn't used
					userHasSeat: <?= CJSON::encode(LearningLogMarketplaceCourseLogin::userHasSeat($this->courseModel->getPrimaryKey(), Yii::app()->user->id)) ?>
				});
			}
			;
		</script>
	<?php endif; ?>

	<div id="session-evaluation-container<?= $_sessionDateIndex ?>">
		<?php
		if (!empty($this->evaluation)) {
			$this->render('_evaluation', array(
				'courseModel' => $this->courseModel,
				'sessionModel' => $this->session,
				'evaluator' => $this->evaluation['evaluator'],
				'evaluationDate' => $this->evaluation['date'],
				'evaluationStatus' => $this->evaluation['status'],
				'evaluationScore' => $this->evaluation['score'],
				'evaluationComments' => $this->evaluation['comments'],
				'evaluationAttachment' => $this->evaluation['attachment']
			));
		}
		?>
	</div>

</div>

<script type="text/javascript">

	var geocoder;
	var map;

	function updateMapPreview($map) {
		if ($map.hasClass('loaded')) return;

		var $mapCanvas = $map.find('.location-map-canvas');
		var address = $map.find('input').val();

		if (address == '') {
			// clear preview
			$mapCanvas.html('').attr('style', '');
			return;
		}

		if (!geocoder)
			geocoder = new google.maps.Geocoder();

		geocoder.geocode({'address': address}, function (results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				var mapOptions = {
					zoom: 14
				}
				map = new google.maps.Map($mapCanvas[0], mapOptions);

				map.setCenter(results[0].geometry.location);
				var marker = new google.maps.Marker({
					map: map,
					position: results[0].geometry.location
				});

				$map.addClass('loaded');
			} else {
				Docebo.log('Geocode was not successful for the following reason: ' + status);
			}
		});
	}

	$(document).ready(function () {

		/**
		 * Start the countdowns for each "date" of the session
		 */
		$('.date-row.date').webinarSessionTimers();

		$('.date-row .fa-chevron-down, .date-row .fa-chevron-up').click(function()
		{
			var container = $(this).closest('.date-row');
			if (container.length > 0) {
				container.toggleClass('collapsed');
			}

			if($(this).hasClass('fa-chevron-down'))
			{
				$(this).toggleClass('fa-chevron-down');
				$(this).addClass('fa-chevron-up');
				container.addClass('secondary');
			}
			else
			{
				$(this).addClass('fa-chevron-down');
				$(this).toggleClass('fa-chevron-up');
				container.toggleClass('secondary');
			}
		});

		var $sessionDays = $('#session-days-list');

		// css class name for a selected list row
		var cssItemSelected = 'selected';

		$sessionDays.find('.item')
			.bind('expand-row', function () {
				var $item = $(this);
				var $handle = $item.find('.toggle-handle'),
					$target = $item.find('.secondary');

				// retract all other rows
				$item.parent().find('.item').siblings().trigger('retract-row');

				$item.addClass(cssItemSelected);
				$handle
					.removeClass($handle.data('retract-class'))
					.addClass($handle.data('expand-class'));
				$target.slideDown().removeClass('hide');

				// show google maps
				$map = $item.find('.map-preview-container');
				updateMapPreview($map);
			})
			.bind('retract-row', function () {
				var $item = $(this);
				var $handle = $item.find('.toggle-handle'),
					$target = $item.find('.secondary');

				$item.removeClass(cssItemSelected);
				$handle
					.removeClass($handle.data('expand-class'))
					.addClass($handle.data('retract-class'));
				$target.hide();
			});

		$sessionDays.find('.primary').click(function () {
			var $item = $(this).closest('.item');

			var fnName = $item.hasClass(cssItemSelected)
				? 'retract-row'
				: 'expand-row';

			$item.trigger(fnName);
		});

		$('.fancybox').fancybox();

//		var syncTime = function () {
//			$.ajax({
//				url: '<?//=Docebo::createLmsUrl('webinar/session/syncTimers', array('id' => $this->sessionId))?>//',
//				dataType: 'json',
//
//				success: function (data) {
//					time_to_begin = <?//=$startTimestamp?>// -data.now;
//					time_to_end = <?//=$endTimestamp?>// -data.now;
//					setTimeout(syncTime, 30 * 60 * 1000);
//				}
//			});
//		};
//		setTimeout(syncTime, 30 * 60 * 1000);

		//check video recording conversion status (if any conversion is progressing)
		checkVideoConversionsInProgress();
	});

	$(function () {
		$('.date-row.date').on('click', '.join-button a, .video-recording-download a', function(e) {
			var container = $(this).closest('.date');
			var isDisabled = $(this).hasClass('disabled') ? true : false;

			if(isDisabled){
				return false;
			}

			var idSession = container.data('id-session');
			var day = container.data('day');
			if($(this).parent().hasClass('join-button')){
				var event = "<?=WebinarSession::EVENT_WATCHED_LIVE?>";
			} else if($(this).parent().hasClass('video-recording-download')){
				var event = "<?=WebinarSession::EVENT_WATCHED_RECORDING?>";
			}

			$.ajax({
				url: "<?=Docebo::createLmsUrl('webinar/session/axMarkCompleteOnJoin')?>",
				data: {
					id_session: idSession,
					day: day,
					event: event
				},
				type: "POST",
				dataType: 'json'
			}).success(function(o) {
				if (!o.success) {
					Docebo.log(o.message || <?= CJSON::encode(Yii::t('standard', '_OPERATION_FAILURE')) ?>);
					return;
				}

				if(!o.showUnenrollmentButtons){
					$("#course-unenrollment-button").remove();
					$("#session-unenrollment-button").remove();
				}

				if (o.new_status_label_class && o.new_status_label_text) {
					var container = $('#label-container_' + idSession + '_' + day + ', #timers_' + idSession + '_' + day);
					if (container.length > 0) {
						container.each(function(index, el) {
							var label = $(el).find('.webinar-label');
							if (label.length > 0) {
								label.removeClass('label-blue label-green label-black');
								label.addClass(o.new_status_label_class);
								label.text(o.new_status_label_text);
							}
						});
					}
				}
				if (typeof o.new_evaluation_box !== 'undefined') {
					var container = $('#session-evaluation-container_' + idSession + '_' + day);
					if (container.length > 0) {
						container.html(o.new_evaluation_box);
					}
				}
			}).fail(function() {
				Docebo.log('Unable to update session date attendance status');
			});
		});
	});

	/* view recording video dialog */
	var recordingDialogId = false;

	/* view recording video dialog */
	function viewRecording(idSession, day) {

		//create unique dialog id
		var recordingDialogId = 'webinar-recording-player-' + (new Date()).getTime();

		//prepare player dialog
		$('<div/>').dialog2({
			autoOpen: true, // We will open the dialog later
			id: recordingDialogId,
			title: <?= CJSON::encode($webinarSession->name.' '.Yii::t('webinar', 'Recording')) ?>,
			modalClass: 'modal-webinar-recording-player',
			showCloseHandle: true,
			removeOnClose: true,
			closeOnEscape: true,
			closeOnOverlayClick: true
		});

		//put loading icon into the dialog while ajax request is performing
		$('.modal.modal-webinar-recording-player').find('.modal-body').html('<div class="loading-spinner-container"><i class="fa fa-spin fa-spinner"></i></div>');

		//request video player content from the server
		$.ajax({
			url: '<?= Docebo::createLmsUrl('webinar/session/recordingPlayer') ?>',
			method: 'POST',
			dataType: 'json',
			data: {
				id_session: idSession,
				day: day
			}
		}).done(function(o) {

			if (!o.success) {
				//TODO: show error message
				return;
			}

			var modal = $('.modal.modal-webinar-recording-player');
			modal.find('.modal-body').html(o.html || '');
			modal.find('.modal-footer').html(<?php
				echo CJSON::encode(CHtml::button(Yii::t('standard', '_CLOSE'), array(
					'class' => 'btn-docebo black big cancel-btn',
					'type' => 'button',
					'onclick' => "(function() { $('a.close').trigger('click'); })()"
				)));
			?>);

		}).fail(function() {

		}).always(function() {

		});
	}




	function afterAddWebLinkRecording(idSession, day, url, recordingType) {
		//move to "View recording" status
		console.log('idsession: '+idSession);
		console.log('day: '+day);
		console.log('url: '+url);
		console.log('type: '+recordingType);

		//this will close the dialog
		$('.addRecordingModal a.close').trigger('click');

		//update content

		if(recordingType == '<?=WebinarTool::PLAY_RECORDING_TYPE_EXTERNAL?>') {
			var t = '<span class="video-recording-download recording-actions">'
				+ '<a class="btn btn-docebo green" href="'+url+'" target="_blank">'
				+ <?= CJSON::encode(Yii::t('webinar', 'View recording')) ?> +'</a>'
				+ '</span>';
		} else {
			var t = '<span class="video-recording-download recording-actions">'
				+ '<a class="btn btn-docebo green" href="javascript:;" onclick="(function() { viewRecording(' + idSession + ', \'' + day + '\'); })()">'
				+ <?= CJSON::encode(Yii::t('webinar', 'View recording')) ?> +'</a>'
				+ '</span>';
		}
		<?php if ($canEditCourse): ?>
		var url = '<?= Docebo::createLmsUrl('webinar/session/deleteRecording', array('session_id' => '{session_id}', 'date' => '{day}')) ?>'
			.replace('<?= urlencode('{session_id}') ?>', idSession)
			.replace('<?= urlencode('{day}') ?>', day)
			.replace('{session_id}', idSession)
			.replace('{day}', day);
		t += '<a href="'+url+'" class="deleteRecording open-dialog recording-actions" data-dialog-class="deleteRecordingModal"><span></span></a>';
		<?php endif; ?>

		var container = $('#timers_'+idSession+'_'+day).find('.pull-right');
		if (container.length > 0) {
			container.find('.recording-actions').remove();
			container.append($(t));
			container.controls();
		}

	}



	function afterDeleteRecording(idSession, day) {
		//revert to "Add recording" status

		// 1) create new "add recording" button
		var btn = $('<a/>');
		btn.attr('href', '<?php
			echo Docebo::createLmsUrl('webinar/session/addRecording', array(
				'session_id' => '{session_id}',
				'course_id' => Yii::app()->request->getParam('course_id', null),
				'date' => '{day}'
			));
		?>'.replace('{session_id}', idSession)
			.replace('{day}', day)
			.replace('<?= urlencode('{session_id}') ?>', idSession)
			.replace('<?= urlencode('{day}') ?>', day));
		btn.attr('data-dialog-class', 'addRecordingModal');
		btn.attr('closeOnEscape', "false");
		btn.attr('closeOnOverlayClick', "false");
		btn.addClass('btn btn-docebo green open-dialog recording-actions');
		btn.text(<?= CJSON::encode(Yii::t('webinar', 'Add recording')) ?>);

		// 2) remove old buttons and insert the newly created button in the DOM
		var container = $('#timers_'+idSession+'_'+day).find('.pull-right');
		if (container.length > 0) {
			container.find('.recording-actions').remove();
			container.append(btn);
			container.controls();
		}

	}


	function checkVideoConversionsInProgress() {
		var t = $('.recording-actions.converting');
		if (t.length > 0) {
			//collect info on each progressing conversion
			var days = [];
			$.each(t, function(index, value) {
				days.push({
					day: $(value).data('day'),
					status: <?= WebinarSessionDateRecording::RECORDING_STATUS_CONVERTING ?>
				});
			});

			//poll function
			var poll = function() {
				$.ajax({
					url: '<?= Docebo::createLmsUrl('webinar/session/checkVideoConversionProgressStatus') ?>',
					type: 'POST',
					dataType: 'json',
					data: {
						id_session: <?= $webinarSession->id_session ?>,
						days: days
					}
				}).success(function (o) {

					if (o.success) {
						var x, y, continuePolling = false;
						if (o.data.length > 0) {
							for (x in o.data) {
								var row = o.data[x];
								if (row.status == <?= WebinarSessionDateRecording::RECORDING_STATUS_CONVERTING ?>) {
									//something is still converting, so continue polling
									continuePolling = true;
								}
								if (row.status == <?= WebinarSessionDateRecording::RECORDING_STATUS_COMPLETED ?>) {
									//create new "View recording" stuff
									var t = '<span class="video-recording-download recording-actions">'
										+'<a class="btn btn-docebo green" href="javascript:;" onclick="(function() { viewRecording(<?= $webinarSession->id_session ?>, \''+row.day+'\'); })()">'
										+<?= CJSON::encode(Yii::t('webinar', 'View recording')) ?>
										+'</a>'
										+'</span>';
									<?php if ($canEditCourse): ?>
									var url = '<?= Docebo::createLmsUrl('webinar/session/deleteRecording', array('session_id' => $webinarSession->id_session, 'date' => '{day}')) ?>'
										.replace('<?= urlencode('{day}') ?>', row.day)
										.replace('{day}', row.day);
									t += '<a href="'+url+'" class="deleteRecording open-dialog recording-actions" data-dialog-class="deleteRecordingModal"><span></span></a>';
									<?php endif; ?>
									//remove the dummy blue button and put the new stuff in its place
									var container = $('#timers_<?= $webinarSession->id_session ?>_'+row.day).find('.pull-right');
									if (container.length > 0) {
										container.find('.recording-actions').remove();
										container.append($(t));
										container.controls();
									}
									//update current polling status
									for (y in days) {
										if (days[y].day == row.day) {
											days[y].status = row.status;
										}
									}
								}
							}
						}
						if (continuePolling) {
							setTimeout(poll, 100);
						}
					} else {
						//TODO: how to handle this situation ?
					}
				}).fail(function () {

				}).always(function () {

				});
			};

			//begin polling
			poll();

		}
	}

</script>