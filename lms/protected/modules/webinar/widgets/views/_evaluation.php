<?php
/* @var $courseModel LearningCourse */
/* @var $sessionModel WebinarSession */
/* @var $evaluator CoreUser */
/* @var $evaluationDate string */
/* @var $evaluationStatus string */
/* @var $evaluationScore string */
/* @var $evaluationComments string */
/* @var $evaluationAttachment array */
?>
<br/>
<h3 class="title-bold"><?= Yii::t('classroom', 'Personal Performance Evaluation') ?>:</h3>
<div class="evaluation-container">
	<div class="row-fluid">
		<div class="evaluation-meta">
			<div class="evaluation-date">
				<em>
					<?php
					$_evaluationTimestamp = strtotime(Yii::app()->localtime->fromLocalDate($evaluationDate));
					echo Yii::t('standard', '_MONTH_' . date('m', $_evaluationTimestamp)) . ' ' . date('j, Y', $_evaluationTimestamp);
					?>
				</em>
			</div>
			<br/>

			<div class="evaluation-instructor">
				<?php if(!empty($evaluator)): ?>
					<div class="thumbnail"><?= $evaluator->getAvatarImage() ?></div>
					<div class="instructor-title"><?= Yii::t('menu_over', 'Teacher') ?></div>
					<div class="instructor-name"><?= $evaluator->getFullName() ?></div>
				<?php endif; ?>
			</div>
		</div>

		<div class="evaluation-status status-<?= $evaluationStatus ?>">
			<div class="row-fluid">
				<div class="span6">
					<div class="status-label-container">
						<?php if($evaluationStatus == 'passed') : ?>
							<h4 class="status-label text-colored-green"><?= Yii::t('coursereport', '_PASSED') ?></h4>
						<?php else: ?>
							<h4 class="status-label text-colored-red"><?= Yii::t('coursereport', '_NOT_PASSED') ?></h4>
						<?php endif; ?>
					</div>
				</div>
				<div class="span6">
					<?php if($evaluationStatus == 'passed') : ?>
						<span class="classroom-sprite valid-big"></span>
					<?php else: ?>
						<span class="classroom-sprite invalid-big"></span>
					<?php endif; ?>
				</div>
			</div>
			<br/>

			<div class="row-fluid">
				<?php if($evaluationScore > 0): ?>
					<div class="span6"><?= Yii::t('standard', '_SCORE') ?></div>
					<div class="span6">
						<?php
						$_evaluationMode = $this->session->evaluation_type; //it may be an online test session
						//TODO: this should be moved into widget controller "SessionInfo.php" (see line 92)
						switch ($_evaluationMode) {
							case WebinarSession::EVAL_TYPE_MANUAL: {
								//display score assigned to the session by instructor
								?>
								<div class="session-info-score-container">
													<span
														class="score-value <?= ($evaluationStatus == 'passed') ? 'text-colored-green' : 'text-colored-red' ?>">
														<?= $evaluationScore ?>
													</span>
									<!--													--><?php //if ($this->hideScoreBase !== true) : ?>
									<!--													<span class="score-sep">/</span>-->
									<!--													<span class="score-base">-->
									<!--													--><?php
									//													echo (isset(Yii::app()->params['classroom-default-session-score_base']) && (int)Yii::app()->params['classroom-default-session-score_base'] > 0
									//														? (int)Yii::app()->params['classroom-default-session-score_base']
									//														: '100');
									//													?>
									<!--													</span>-->
									<!--													--><?php //endif; ?>
								</div>
							<?php
							}
								break;
							case WebinarSession::EVAL_TYPE_ONLINE_TEST: {
								if(is_array($evaluationScore) && isset($evaluationScore['score']) && $evaluationScore['score']) {
									?>
									<span class="score <?= ($evaluationStatus == 'passed') ? 'text-colored-green' : 'text-colored-red' ?>">
										<?= $evaluationScore['score'] ?>
									</span>
								<?php
								} else {
									echo '-';
								}
							}
								break;
						}
						?>
					</div>
				<?php endif; ?>
			</div>
		</div>

		<div class="evaluation-content">
			<h4><?= Yii::t('classroom', 'Student Performance Evaluation') ?></h4>

			<div class="evaluation-comments">
				<?= $evaluationComments ?>
			</div>

			<?php if($evaluationAttachment['name']) : ?>
				<div class="evaluation-attachment">
					<h4><?= Yii::t('standard', 'Attached files') ?></h4>

					<div class="attached-file">
						<span class="i-sprite is-file black"></span>
						<a class="text-colored" href="<?= $evaluationAttachment['url'] ?>"
							 target="_blank"><?= $evaluationAttachment['name'] ?></a>
					</div>
				</div>
			<?php endif; ?>
		</div>

	</div>

	<?php
	// Trigger event to let plugins showing their custom informations
	$event = new DEvent($this, array(
		'courseModel' => $courseModel,
		'sessionModel' => $sessionModel
	));
	Yii::app()->event->raise('OnUserSessionInfoDisplay', $event);
	$continue = true;
	if($event->shouldPerformAsDefault() && !empty($event->return_value)) {
		//some plugins may not need normal subscribe actions .. let them avoid those
		if(isset($event->return_value['continue']) && !$event->return_value['continue']) {
			$continue = false;
		}
	}
	// end event
	?>

</div>