<?php
$pUserRights = Yii::app()->user->checkPURights($course->getPrimaryKey());

$canEditSessions = (Yii::app()->user->getIsGodadmin() || ($pUserRights->all && Yii::app()->user->checkAccess("/lms/admin/webinarsessions/add")));
$canEditWaitingList = (Yii::app()->user->getIsGodadmin() || ($pUserRights->all && Yii::app()->user->checkAccess("enrollment/create")));
$canEditTrainingMaterials = (Yii::app()->user->getIsGodadmin() || ($pUserRights->all && Yii::app()->user->checkAccess("/lms/admin/webinarsessions/mod")) || $this->userCanAdminCourse);
?>
<div class="session-main-actions-container">
	<ul id="docebo-bootcamp-sessions" class="docebo-bootcamp clearfix">
		<?php if($canEditSessions): ?>
		<li>
			<a id="add-new-session"
			   href="<?=Docebo::createLmsUrl('webinar/session/create', array('course_id' => $course->getPrimaryKey()))?>"
			   class="open-dialog"
			   data-dialog-class="modal-create-webinar-session"
			   data-helper-title="<?= Yii::t('classroom', 'New session') ?>"
			   data-helper-text="<?= Yii::t('helper', 'Create Session - lt') ?>">
				<span class="classroom-sprite new-date"></span>
				<span class="classroom-sprite new-date white"></span>
				<h4><?= Yii::t('classroom', 'New session') ?></h4>
			</a>
		</li>
		<?php endif; ?>
		<?php if($canEditWaitingList): ?>
			<li>
				<a id="users-waiting-to-be-approved"
				   href="<?=Docebo::createAdminUrl('webinar/session/assign', array('course_id' => $course->getPrimaryKey()))?>"
				   data-helper-title="<?php echo Yii::t('classroom', 'Users waiting to be assigned'); ?>"
				   data-helper-text="<?php echo Yii::t('helper', 'Unassigned Users - lt'); ?>">
					<span class="session-waiting-approval"></span>
					<span class="session-waiting-approval white"></span>
					<?php
					$waitingForSessions = $course->countWaitingForAllWebinarSessions();
					$_countUnassigned = $course->getUnassignedWebinarUsersCount() + ((is_int($waitingForSessions) && $waitingForSessions>0)  ? $waitingForSessions : 0); ?>
					<?php if ($_countUnassigned > 0) : ?>
						<span class="notification"><?= $_countUnassigned ?></span>
					<?php endif; ?>
					<h4><?php echo Yii::t('classroom', 'Users waiting to be assigned'); ?></h4>
				</a>
			</li>
		<?php endif; ?>
		<?php if ($canEditTrainingMaterials): ?>
			<li>
				<a id="classroom-los-management"
				   href="<?= Docebo::createLmsUrl('webinar/session/manageLearningObjects', array('course_id' => $course->getPrimaryKey())) ?>"
				   alt="<?php echo Yii::t('organization', 'ADD TRAINING RESOURCES');?>"
				   data-helper-title="<?= Yii::t('standard', 'Training materials') ?>"
				   data-helper-text="<?= Yii::t('organization', 'ADD TRAINING RESOURCES') ?>">
					<span class="classroom-sprite manage-objects black"></span>
					<span class="classroom-sprite manage-objects white"></span>
					<h4><?php echo Yii::t('standard', 'Training materials'); ?></h4>
				</a>
			</li>
		<?php endif; ?>
		<li class="helper">
			<a href="#">
				<span class="i-sprite is-circle-quest large"></span>
				<h4 class="helper-title"></h4>
				<p class="helper-text"></p>
			</a>
		</li>

	</ul>
</div>