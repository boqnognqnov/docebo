<?php

/**
 * Displays the list of sessions, to be used by instructors and superadmins
 */
class SessionManagement extends CWidget {

	/**
	 * @var LearningCourse
	 */
	public $courseModel;

	public $userCanAdminCourse = false;



	public function init() {
		parent::init();

		/* @var $cs CClientScript */
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/classroom-attendance-sheet.css');
		$adminPath = Yii::app()->getRequest()->getBaseUrl() . '/../admin';
		$cs->registerScriptFile($adminPath . '/js/admin.js');
		$cs->registerScriptFile($adminPath . '/js/script.js');
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/admin.css');
		// $cs->registerCssFile($adminPath . '/css/admin-temp.css');

		// Bootcamp menu
		$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/docebo_bootcamp.js');
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/docebo_bootcamp.css');

		// Load the datepicker (used in the new/edit session modal)
		DatePickerHelper::registerAssets();

		// Load bootstro styles (used in the new/edit session modal)
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/js/bootstro/bootstro.css');

		$pUserCourseAssigned = CoreUserPuCourse::model()->findByAttributes(array(
			'puser_id' => Yii::app()->user->id,
			'course_id' => $this->courseModel->idCourse
		));

		$courseLevel = LearningCourseuser::userLevel(Yii::app()->user->id, $this->courseModel->idCourse);

		// Set this controller attribute for later usage in forum admin UIs: Course Admins CAN admin course forums in any way
		if (Yii::app()->user->isGodAdmin){
			//if it's god admin, course user level doesn't matter
			$this->userCanAdminCourse = true;
		}elseif($courseLevel && $courseLevel > LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT){
			// User subscribed to course and his level is high enough
			$this->userCanAdminCourse = true;
		}elseif($pUserCourseAssigned && Yii::app()->user->checkAccess('/lms/admin/course/mod')){
			// User power user, course assigned to him, and he has permissions to modify
			$this->userCanAdminCourse = true;
		}
	}


	public function run() {

		//check provenience
		$setParam = false;
		switch (Yii::app()->request->getParam('ref_src', false)) {
			case 'admin': {
				if (Docebo::getDefaultUrlParam('ref_src') === NULL) {
					$setParam = true;
					Docebo::setDefaultUrlParam('ref_src', 'admin');
				}
			} break;
		}

		//render widget
		$this->render('sessionManagement', array(
			'course' => $this->courseModel
		));

		//flush provenience info, if needed
		if ($setParam) { Docebo::unsetDefaultUrlParam('ref_src'); }
	}

}