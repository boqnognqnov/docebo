/**
 * Created by Dzhuneyt on 14.3.2016 г..
 */
$(function () {
	$.fn.webinarSessionTimers = function (options) {
		this.each(function () {
			var defaults = {};
			var options = $.extend(options, defaults);

			var that = $(this);

			var idSession = that.data('id-session');
			var day = that.data('day');

			// Get some constants from data-* attributes
			var remainingSecondsToBegin = $(this).data('remaining-seconds-to-begin');
			var remainingSecondsToEnd = $(this).data('remaining-seconds-to-end');
			var advanceJoinPossibleSeconds = $(this).data('join-in-advance-settings');
			var permissions = $(this).data('permissions');

			/**
			 * When the Webinar will actually start
			 * @type {Date}
			 */
			var timeBegin = new Date();

			/**
			 * Similar to timeBegin, but takes into account
			 * the "show join button X minutes in advance"
			 * @type {Date}
			 */
			var joinPossibleTime = new Date();

			/**
			 * When the Webinar ends. Hides all UI elements like Join button and timers
			 * @type {Date}
			 */
			var timeEnd = new Date();

			timeBegin.setSeconds(timeBegin.getSeconds() + remainingSecondsToBegin);
			timeEnd.setSeconds(timeEnd.getSeconds() + remainingSecondsToEnd);
			joinPossibleTime.setSeconds(joinPossibleTime.getSeconds() + (remainingSecondsToBegin - advanceJoinPossibleSeconds));

			var webinarBeginCountdown = setInterval(function () {

				//console.log('Timeout for session ' + idSession + ', day ' + day);

				var currentTimestamp = new Date();

				// Join button offset started and the webinar has not ended yet
				if (currentTimestamp.getTime() > joinPossibleTime.getTime() && timeEnd.getTime() > currentTimestamp.getTime()) {
					console.log('Join button possible, showing...');
					$('.join-button', that).show();
				}

				if (currentTimestamp.getTime() > timeBegin.getTime()) {
					// Webinar has started
					console.log('Webinar has started. Stopping countdown to beginning. Starting countdown to end.');

					clearInterval(webinarBeginCountdown);

					$('.timer-to-begin', that).hide();
					if(timeEnd.getTime() > currentTimestamp.getTime()) { // Prevents join button from quickly flashing in webinar page
						$('.join-button', that).show();
					}

					var webinarEndCountdown = setInterval(function () {
						var currentTimestamp = new Date();

						if (timeEnd.getTime() < currentTimestamp.getTime()) {
							clearInterval(webinarEndCountdown);
							console.log('Webinar has ended');
							$('.join-button', that).hide();
							$('.timer-to-end', that).hide();
							$('.recording-buttons', that).show();
						} else {
							// variables for time units
							var days, hours, minutes, seconds;

							var seconds_left = ((timeEnd.getTime()) - (currentTimestamp.getTime())) / 1000;
							var seconds_left_calc;

							// do some time calculations
							days = parseInt(seconds_left / 86400);
							seconds_left = seconds_left % 86400;
							hours = parseInt(seconds_left / 3600);
							if (hours < 10) hours = '0' + hours;
							seconds_left_calc = seconds_left % 3600;
							minutes = parseInt(seconds_left_calc / 60);
							if (minutes < 10) minutes = '0' + minutes;
							seconds = parseInt(seconds_left_calc % 60);
							if (seconds < 10) seconds = '0' + seconds;

							$('.join-button', that).show();

							var remainingLabelStr;
							if (days > 0) {
								remainingLabelStr = days + " " + Yii.t('standard', '_DAYS') + ", " + hours + ":" + minutes + ":" + seconds;
							} else {
								remainingLabelStr = hours + ":" + minutes + ":" + seconds;
							}
							$('.timer-to-end', that).show().find('span').html(remainingLabelStr);
						}
					}, 1000);
				} else {
					// variables for time units
					var days, hours, minutes, seconds;

					var seconds_left = ((timeBegin.getTime()) - (currentTimestamp.getTime())) / 1000;
					var seconds_left_calc;

					// do some time calculations
					days = parseInt(seconds_left / 86400);
					seconds_left = seconds_left % 86400;
					hours = parseInt(seconds_left / 3600);
					if (hours < 10) hours = '0' + hours;
					seconds_left_calc = seconds_left % 3600;
					minutes = parseInt(seconds_left_calc / 60);
					if (minutes < 10) minutes = '0' + minutes;
					seconds = parseInt(seconds_left_calc % 60);
					if (seconds < 10) seconds = '0' + seconds;

					var remainingLabelStr;
					if (days > 0) {
						remainingLabelStr = days + " " + Yii.t('standard', '_DAYS') + ", " + hours + ":" + minutes + ":" + seconds;
					} else {
						remainingLabelStr = hours + ":" + minutes + ":" + seconds;
					}
					$('.timer-to-begin', that).show().find('span').html(remainingLabelStr);
				}

			}, 1000);
		});
	};
});