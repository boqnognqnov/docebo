
var Arena = {

	setViewCourseMode: function() {
		//$('#session-info-container').show();
		//$('#classroom-los-editing-container').hide();
		//this.setSwitchToLearnerView();
	},

	setEditCourseMode: function() {
		//$('#session-info-container').hide();
		$('#classroom-los-editing-container').show();
		//this.setSwitchToAdminView();
		this.showLoManagement();
		$(document).trigger('adminViewActivated');
	},

	/**
	 * Change the radio switcher to LEARNER position
	 */
	setSwitchToLearnerView: function() {
		//$('#player-manage-mode-selector label').removeClass('active').eq(0).addClass('active');
		//$('#player-manage-mode-selector-switcher').removeClass('active');
	},

	/**
	 * Change the radio switcher to ADMIN VIEW position
	 */
	setSwitchToAdminView: function() {
		//$('#player-manage-mode-selector label').removeClass('active').eq(1).addClass('active');
		//$('#player-manage-mode-selector-switcher').addClass('active');
	},


	/**
	 * Show LO management panel
	 */
	showLoManagement: function () {
		Arena.hideAllArenaContent();
		$('#player-lo-manage-panel').show();
	},


	hoverAndHintListeners: function () {
		$('#player-manage-add-lo-button .dropdown-menu .add-lo-type-description').hover(function () {
			$("#add-lo-type-hint").html('<span class="pull-left hint-ico"><span class="'+$(this).data("hint-icon-class")+'"></span></span> '+$(this).data("hint-text"));
		}, function () {
			$("#add-lo-type-hint").html('<span class="span12 text-left">'+$("#add-lo-type-hint").data("default-text")+'</span>');
		});
	},



	quickNavEnabled: true,
	firstLoToPlay: null,
	overallFadeSpeed: 300,
	hideMenuPosition: '-402px',
	ajaxLoader: '<div class="ajaxloader" style="display: none;"></div>',
	course_id: '',
	createFolderDialogUrl: '',
	uploadDialogUrl: '',
	uploadFilesUrl: '',
	saveUploadedCoursedocUrl: '',
	openInCentralRepositoryUrl: '',
	assetsBaseUrl: '',
	csrfToken: '',
	getCourseLearningObjectsTreeUrl: '',
	deleteLoUrl: '',
	sortLearningObjectUrl: '',
	moveInFolderLearningObjectUrl: '',
	userCanAdminCourse: false,
	courseInfo: {
		idCourse: 0,
		code: '',
		name: ''
	},
	onLoadArenaMode: 'view_course',
	createTestUrl: '',
	createPollUrl: '',
	videoExtensionsWhitelist: '',
	videoConversionInProgressHtml: '<a rel="tooltip" title="' + Yii.t('authoring', 'CONVERSION_IN_PROGRESS') + '"><span class="video-converting-animation"></span></a>',
	videoConversionErrorHtml: '<a title="'+ Yii.t('authoring', 'Error_while_converting_file') +'"><span class="lo-check i-sprite objlist-sprite is-circle-check red" ></span></a>',
	videoLoGreenHtml : '<span class="lo-check p-sprite objlist-sprite check-green"></span>',
	videoStatusPollJobs: [],

	init: function(options) {

		this.course_id = options.course_id;
		this.createFolderDialogUrl = options.createFolderDialogUrl;
		this.uploadDialogUrl = options.uploadDialogUrl;
		this.uploadFilesUrl = options.uploadFilesUrl;
		this.saveUploadedCoursedocUrl = options.saveUploadedCoursedocUrl;
		this.openInCentralRepositoryUrl = options.openInCentralRepositoryUrl;
		this.assetsBaseUrl = options.assetsBaseUrl;
		this.pluploadAssetsUrl = options.pluploadAssetsUrl;
		this.csrfToken = options.csrfToken;
		this.getCourseLearningObjectsTreeUrl = options.getCourseLearningObjectsTreeUrl;
		this.deleteLoUrl = options.deleteLoUrl;
		this.sortLearningObjectUrl = options.sortLearningObjectUrl;
		this.moveInFolderLearningObjectUrl = options.moveInFolderLearningObjectUrl;
		this.userCanAdminCourse = options.userCanAdminCourse;
		this.courseInfo = $.extend({}, this.courseInfo, options.courseInfo);
		this.onLoadArenaMode = options.onLoadArenaMode;
		this.createTestUrl = options.createTestUrl;
		this.createPollUrl = options.createPollUrl;
		this.videoExtensionsWhitelist = options.videoExtensionsWhitelist;

		this.learningObjectsManagement.init({
			onNodeMoved: function (ui, item, dropPosition, siblings, type) {
				switch (type) {
					case 'folder':
						Arena.learningObjectsManagement.blockTrees();
						var params = {
							'course_id': Arena.course_id,
							'id_object': item,
							'id_folder': dropPosition
						};
						$.ajax({
							url: Arena.moveInFolderLearningObjectUrl,
							type: 'post',
							dataType: 'JSON',
							data: params,
							beforeSend: function () {

							}
						}).done(function (res) {
							if (res.success) {
								if (res.data) {
									if (res.data.play) {
										//Arena.learningObjects.contentViewer.setTree(res.data.play);
									}
									if (res.data.management) {
										Arena.learningObjectsManagement.setTree(res.data.management);
									}
								}
							} else {
								$('#player-arena-overall-objlist').children('ul').sortable("cancel");
							}
						}).always(function () {
							Arena.learningObjectsManagement.unblockTrees();
						}).fail(function (jqXHR, textStatus, errorThrown) {
							Arena.learningObjectsManagement.unblockTrees();
						});
						break;
					case 'sort':
						// Here the ajax call
						Arena.learningObjectsManagement.blockTrees();
						var params = {
							course_id: Arena.course_id,
							id_object: item,
							new_position: dropPosition
						};
						if (siblings[0]) {
							params.before = siblings[0];
						}
						if (siblings[1]) {
							params.after = siblings[1];
						}

						$.ajax({
							url: Arena.sortLearningObjectUrl,
							type: 'post',
							dataType: 'JSON',
							data: params,
							beforeSend: function () {

							}
						}).done(function (res) {
							if (res.success) {
								if (res.data) {
									if (res.data.play) {
										//Arena.learningObjects.contentViewer.setTree(res.data.play);
									}
									if (res.data.management) {
										Arena.learningObjectsManagement.setTree(res.data.management);
									}
								}
							} else {
								$('#player-arena-overall-objlist').children('ul').sortable("cancel");
							}
						}).always(function () {
							Arena.learningObjectsManagement.unblockTrees();
						}).fail(function (jqXHR, textStatus, errorThrown) {
							Arena.learningObjectsManagement.unblockTrees();
						});
				}

			}

		});






		// Load Learning Objects Tree into Dynatrees
		Arena.learningObjectsManagement.ajaxUpdate(null);

		// Set initial arena mode (edit/view)
		if (Arena.onLoadArenaMode == 'edit_course'/* && !Arena.userCanAdminCourse*/) {
			Arena.onLoadArenaMode = 'view_course';
		}
		if (Arena.onLoadArenaMode == 'edit_course') {
			Arena.setEditCourseMode();
		} else {
			Arena.setViewCourseMode();
		}
		//Arena.hideLoNavigation();



		//*** LISTENERS ***

		$(document).controls();
		Arena.hoverAndHintListeners();

		// Listener for clicks on EDIT LO Dropdown menu item
		$('#player-lo-manage-panel').on('click', '.lo-edit', function (e) {
			e.preventDefault();
			var lo_type = $(this).data('lo-type');
			if (lo_type == 'test' || lo_type == 'poll' || lo_type == 'authoring') {
				window.location.href = $(this).attr('href');
			} else if (lo_type == 'htmlpage') {
					Arena.htmledit.show($(this).data('id-object'));
			}else if (lo_type == 'deliverable') {
				Arena.deliverable.show($(this).data('id-object'));
			}
		});


		// Catch the auto-close.success from Delete LO Dialog2 and reload Dynatree
		$(document).on("dialog2.content-update", '[id^="delete-single-lo-modal-"]', function () {
			if ($(this).find("a.auto-close.success").length > 0) {
				Arena.learningObjectsManagement.ajaxUpdate();
			}
		});


		// Catch the auto-close.success from Push LO Dialog2 and reload Dynatree
		$(document).on("dialog2.content-update", '[id^="push-lo-modal-"]', function () {
			if ($(this).find("a.auto-close.success").length > 0) {
				Arena.learningObjectsManagement.ajaxUpdate();
			}
		});

		/**
		 * Listen for click on ".lo-evaluate" LO dropdown menu items (Deliverable, Test,...)
		 * and redirects to the HREF of the target
		 */
		$('#player-lo-manage-panel').on('click', '.lo-evaluate', function(e){
			e.preventDefault();
			var url=$(this).attr('href');
			window.location.href = url;
		});




		// ON Clicking EDIT/VIEW Course button for Admins, in Arena (radios)
		var $playerModeSelectorButtons = $("#player-manage-mode-selector label");
		$playerModeSelectorButtons.on('click', function (e) {
			var targetMode = $('#player-manage-mode-selector input[name=target-mode]:checked').val();
			var label = $(e.currentTarget);

			if (label.attr('for') == 'target-mode-view') targetMode = 'view_course';
			else targetMode = 'edit_course';

			// Toggle target mode for next click(s)
			switch (targetMode) {
				case 'edit_course':
					$('#target-mode-view').prop('checked', false);
					$('#target-mode-edit').prop('checked', true);
					Arena.setEditCourseMode();
					break;
				case 'view_course':
				default:
					$('#target-mode-edit').prop('checked', false);
					$('#target-mode-view').prop('checked', true);
					Arena.setViewCourseMode();
					break;
			}
		});

		// manage click on central switch
		var $playerModeSelectorButtons = $("#player-manage-mode-selector-switcher");
		$playerModeSelectorButtons.on('click', function (e) {
			var targetMode;
			e.preventDefault();
			var currentMode = $('#player-manage-mode-selector input[name=target-mode]:checked').val();
			if (currentMode == 'view_course') targetMode = 'edit_course';
			else targetMode = 'view_course';
			// in this case the central switch was clicked we have to invert the current selection
			// by checking the other radio and switching

			switch (targetMode) {
				case 'edit_course':
					$('#target-mode-view').prop('checked', false);
					$('#target-mode-edit').prop('checked', true);
					Arena.setEditCourseMode();
					break;
				case 'view_course':
				default:
					$('#target-mode-edit').prop('checked', false);
					$('#target-mode-view').prop('checked', true);
					Arena.setViewCourseMode();
					break;
			}
		});

        //ADD LO menu listeners
        Arena.organizer.globalListeners();
        Arena.uploader.globalListeners();
        Arena.deliverable.globalListeners();
        Arena.htmledit.globalListeners();
        Arena.poll.globalListeners();
        Arena.test.globalListeners();
	},


	/**
	 * Hide the main area container
	 */
	hideAllArenaContent: function () {
		$("#player-arena-content > div").hide();
	},

	/**
	 * Show LO management panel
	 */
	showLoManagement: function () {
		Arena.hideAllArenaContent();
		$('#player-lo-manage-panel').show();
	},

	/**
	 * Show LO Arena Content & Launchpad
	 */
	showLoLaunchPad: function () {
		Arena.hideAllArenaContent();
		$('#player-arena-launchpad').show();
		Docebo.scrollTo('#arena-board-top');
	},




	learningObjectsManagement: {

		// Do an AJAX call to refresh Dynatrees
		ajaxUpdate: function (treeType, successCallback) {
			//validate treeType parameter
			treeType = 'management'; //no other types allowed for classroom courses
			var requestType = [];
			switch (treeType) {
				case 'play':
				case 'management':
				{
					requestType.push(treeType);
				}
					break;
				default:
				{ //unspecified or invalid type: ask for both trees
					requestType.push('play');
					requestType.push('management');
				}
					break;
			}
			//do ajax request
			$.ajax({
				url: Arena.getCourseLearningObjectsTreeUrl,
				type: 'post',
				dataType: 'JSON',
				data: {
					course_id: Arena.course_id,
					tree_type: requestType.join(',')
				}
			}).done(function (res) {
				if (res.success) {
					if (res.data) {

						/*if (res.data.play) {
						 // Some widgets are sensitive to training materials (status, etc.). Load ONLY them
						 $('.player-block-courseinfo').each(function () {
						 $(this).loadOneBlockContent();
						 });
						 Arena.learningObjects.contentViewer.setTree(res.data.play);
						 Arena.firstLoToPlay = res.data.firstToPlay;
						 if (!(Arena.firstLoToPlay == false)) {
						 // Navigate/Activate the first LO to play. All parents will be expanded as necessary
						 $('.player-lonav-tree').dynatree("getTree").activateKey(Arena.firstLoToPlay.key);
						 }
						 }*/
						if (res.data.management) {
							Arena.learningObjectsManagement.setTree(res.data.management);
						}

						/*
						 // Update LO Counters/Stats in Mainmenu
						 if (theMainMenu) {
						 theMainMenu.updateLoStats();
						 }
						 */

						/*
						 // Reload  Quicknavs to reflect changes in tree
						 quicknavUrl = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=player/training/axGetQuickNavHtml&course_id=' + Arena.course_id;
						 $(".player-quicknav-wrapper").load(quicknavUrl, function(){
						 if (!Arena.quickNavEnabled) {
						 Arena.blockQuickNav();
						 }

						 // Activate carousel item containing the First-to-lay object
						 if (Arena.firstLoToPlay) {
						 var carouselIndex = parseInt($('[id="player-quicknav-object-' + Arena.firstLoToPlay.key + '"]').data('carousel-index'));
						 $('#player-quicknav-carousel').carousel(carouselIndex);
						 // This is NON-sliding variant!
						 //$('#player-quicknav-carousel .item').removeClass('active');
						 //$('#player-quicknav-object-' + Arena.firstLoToPlay.key).closest('.item').addClass('active');
						 }

						 });
						 */

						// Call Success callback, if any
						if ($.isFunction(successCallback)) {
							successCallback(res.data);
						}
					}
				}
			}).always(function () {

			}).fail(function (jqXHR, textStatus, errorThrown) {

			});
		},


		currentZIndex: 1000,
		tree: [],

		hasFoldersInRoot: function () {
			var hasFolders = false;
			var tree = Arena.learningObjectsManagement.tree;
			for(var child in tree) {
				if (tree[child].isFolder) {
					hasFolders = true;
				}
			}
			return hasFolders;
		},

		hasSubfolders: function (elements, cache) {
			// Cache the information in the node for a little performance boost
			if (typeof elements.hasSubfolders == 'undefined' || cache !== true) {
				elements.hasSubfolders = false;
				for(key in elements) {
					if (elements[key].isFolder) {
						elements.hasSubfolders = true;
					}
				}

			}

			return elements.hasSubfolders;
		},

		options: {
			// Called when the node is dropped
			onNodeMoved: function (ui, item, dropPosition, siblings, type) {}
		},

		init: function (options) {

			$('.dynatree-container').parent().addClass('dynatree-container-parent');

			var me = this;
			this.options = $.extend({}, this.options, options);
			if (this.hasFoldersInRoot()) {
				$('#player-arena-overall-folders').dynatree({

					selectMode: 1,

					onPostInit: function (isReloading, isError) {
					},

					onCreate: function (node, nodeSpan) {
						var $nodeSpan = $(nodeSpan);
						var bgSpan = $('<span class="background-loitem"></span>');
						if (node.data.isFolder) {
							bgSpan.addClass('folder');
							$nodeSpan.before(bgSpan);
							$nodeSpan.siblings().addBack().hover(function () {
								bgSpan.addClass('hover');
								$(this).addClass('hover');
							}, function () {
								bgSpan.removeClass('hover');
								$(this).removeClass('hover');
							});
							$nodeSpan.parent().attr('data-object-id', node.data.key);
						} else {
							$nodeSpan.parent().css("display", "none");
						}
						$nodeSpan.parent().droppable({
							tolerance: "pointer",
							greedy: true,
							over: function (event, ui) {
								$(ui.helper).attr('data-drop-type', 'folder');
							},
							out: function (event, ui) {
								$(ui.helper).attr('data-drop-type', 'sort');
							},
							drop: function (event, ui) {
								var folderId = $(this).attr('data-object-id');
								me.options.onNodeMoved(ui, $(ui.helper).attr('data-object-id'), folderId, [], $(ui.helper).attr('data-drop-type'));
								return false; // Prevent propagation to parent
								//folder
							}
						});
					},

					onRender: function (node, nodeSpan) {
						var $nodeSpan = $(nodeSpan);
						if (node.data.isFolder) {
							var cssClass = '';
							// Use the first row for check if the content in right
							// panel is shown
							// var opened = ($nodeSpan.hasClass('dynatree-expanded')
							// || $nodeSpan.hasClass('dynatree-active')) &&
							// Arena.learningObjects.hasSubfolders(node.data.children,
							// true);
							var opened = $nodeSpan.hasClass('dynatree-expanded');
							cssClass = opened ? "is-folder opened" : "is-folder closed";
							if (node.data.sort == '1') {
								cssClass += ' is-root';
							}
							else if (Arena.learningObjectsManagement.hasSubfolders(node.data.children, true)) {
								cssClass += " has-children";
							}
							$nodeSpan.find('span.dynatree-icon').addClass('p-sprite '+cssClass).removeClass('dynatree-icon');
							$nodeSpan.parent().addClass('droppable-folder');
							if (node.data.sort == '1') { // Root node
								$nodeSpan.children('.dynatree-title')
									.html('<div class="course-name">'+Arena.courseInfo.name+'</div>')
									.addClass("is-root");
							}
						}
					},

					onClick: function (node, event) {
						me.showFolder(node.data.children, node);
					},

					onActivate: function (node) {
					},

					children: Arena.learningObjectsManagement.tree
				});
			} else {
				// Just make this element a Dynatree, having empty tree (is this
				// ok?)
				$('#player-arena-overall-folders').dynatree({
					children: []
				});
				this.showFolder(Arena.learningObjectsManagement.tree);
			}

		},

		reloadTree: function () {
			this.init(this.options);
			$('#player-arena-overall-folders').dynatree("getTree").reload();
			this.expandAll();
		},

		setTree: function (tree) {
			this.tree = tree;
			this.reloadTree();
		},


		showFolder: function (data, parentNode) {

			var me = this;
			var updateZIndex = function () {
				/*
				 $('.player-arena-buttongroup').reverse().each(function (index, element) {
				 $(this).css("z-index", index+2);
				 });
				 */
			};

			$('#player-arena-overall-objlist').dynatree({
				clickFolderMode: 1,
				selectMode: 1,

				onPostInit: function (isReloading, isError) {
					currentIndex = 0;
					updateZIndex();

					$('#player-arena-overall-objlist .droppable-folder').droppable({
						hoverClass: 'droppable-hovered',
						//greedy: true,
						accept: 'li',
						tolerance: 'pointer',
						over: function (event, ui) {
						},
						out: function (event, ui) {
							$(ui.helper).attr('data-drop-type', 'sort');
						},
						drop: function (event, ui) {
							// The new requirement then is to completely disable DROPPING items into folders in the RIGHT content panel ...
							return false

							$(ui.helper).attr('data-drop-type', 'folder');
							var folderId = $(this).attr('data-object-id');

							me.options.onNodeMoved(ui, $(ui.helper).attr('data-object-id'), folderId, [], $(ui.helper).attr('data-drop-type'));

							return false; // Prevent propagation to parent
						}
					});
				},

				onExpand: function (flag, node) {
					updateZIndex();
				},

				onCreate: function (node, nodeSpan) {

					var idOrg = parseInt(node.data.idOrganization);
					var $nodeSpan = $(nodeSpan);
					var buttons = '';

					// Data attribute to set
					$nodeSpan.parent().attr('data-object-id', node.data.key);
					$nodeSpan.parent().attr('id', node.data.key);

					// remove unused node and setup classes
					$nodeSpan.find('span.dynatree-icon').remove();
					$nodeSpan.parent().addClass('player-arena-li');
					if (node.data.isFolder) $nodeSpan.parent().addClass('player-arena-tree-folder');

					// prefix zone ------------------
					var prefix = $('<span class="player-prefix"></span>');
					$nodeSpan.prepend(prefix);

					// Drag Handler
					prefix.append( $('<span class="lo-drag p-sprite objlist-sprite drag-small-black"></span>') );
					$nodeSpan.find('.lo-drag').on("click", function () { return false; });

					// Type Icon
					var cssClass = '';
					if (!node.data.isFolder) {
						cssClass = 'object-icon i-sprite objlist-sprite ' + Arena.learningObjectsManagement.getSpriteForObject(node.data.type);
					} else {
						cssClass = "p-sprite folder-grey";
						$nodeSpan.parent().addClass('droppable-folder');
					}
					prefix.append( $('<span class="' + cssClass + '"></span>') );

					// postfix zone ---------------------
					var postfix = $('<span class="player-postfix"></span>');
					$nodeSpan.prepend(postfix);

					// Show animation icon if video is under conversion
					if (parseInt(node.data.is_video_transcoding) == 1) {
						//postfix.append($('<span class="video-conversion-in-progress">' + Arena.videoConversionInProgressHtml + '</span>'));
						//Arena.learningObjectsManagement.pollVideoConversionStatus(node, nodeSpan);
					}
					// Show "error" icon if we get some error during conversion
					else if (parseInt(node.data.is_video_transcoding_error) == 1) {
						//postfix.append($(Arena.videoConversionErrorHtml));
					}
					// Otherwise, show normal Green, "available" icon
					else {
						postfix.append( $('<span class="lo-check p-sprite objlist-sprite '
						+(parseInt(node.data.is_visible) > 0? 'check-green' : 'check-grey')+ '"></span>') );
					}

					// dropdown menu actions
					var actions = '';

					// CONFIGURE
					if ( (!node.data.isFolder) && (parseInt(node.data.is_video_transcoding) == 0) && (parseInt(node.data.is_video_transcoding_error) == 0)) {
						var configUrl = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=player/training/axConfigureLo&idOrg=' + idOrg + '&course_id=' + Arena.course_id;
						actions +=  '<li><a href="' + configUrl +
						'" class="open-dialog p-hover" rel="modal-configure-lo-' + idOrg +
						'" role="menuitem">' +
						'<span class="objlist-sprite i-sprite is-gearpair"></span>'+
						'<span class="menu-label"> '+ Yii.t('standard', 'Settings') + '</span> ' +
						'</a></li>';
					}

					// COPY
					//actions += '<li><a href="#" role="menuitem"><span class="objlist-sprite i-sprite is-clone"></span><span class="menu-label"> Copy</span></a></li>';

					actions += '<li class="lo-menu"><hr/></li>';

					// place an evaluation link for the deliverabels
					if (node.data.isEvaluationAllowed && (node.data.type == 'deliverable' || node.data.type == 'test' || node.data.type == 'poll')) {

						actions += '<li><a href="index.php?r=player/report&course_id=' + Arena.course_id+'" class="lo-evaluate p-hover" data-id-object="'+idOrg+'">'
						+'<span class="objlist-sprite i-sprite is-check"></span>'
						+'<span class="menu-label"> '+Yii.t('classroom', 'Evaluation')+'</span></a></li>'
						+ '<li class="lo-menu"><hr/></li>';
					}

					// EDIT FOLDER
					if (node.data.isFolder) {
						actions += '<li><a href="#" role="menuitem" '
						+'class="lo-edit-folder p-hover" data-id-object="'+idOrg+'">'
						+'<span class="objlist-sprite i-sprite is-edit"></span>'
						+'<span class="menu-label"> '+Yii.t('standard', '_MOD')+'</span></a></li>';
					}

					var editUrl = false;

					if(typeof node.data.id_object !== "undefined" && node.data.id_object !== null){
						editUrl = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=centralrepo/centralLo/editShared&id_course' + Arena.course_id + '&id_org=' + idOrg;
					}

					// EDIT
					if (!node.data.isFolder) {
						if(node.data.id_object !== null && typeof node.data.id_object !== "undefined" && $.inArray(node.data.type, ['scormorg', 'aicc', 'tincan', 'elucidat', 'file', 'video']) === -1){
							//do not show edit button if the repoObject has no versioning!!!!
						} else {
							switch (node.data.type) {
								case 'test':
								case 'poll':
									var editHref = (editUrl !== false) ? editUrl : Docebo.lmsAbsoluteBaseUrl + '/index.php?r=' + node.data.type + '/default/edit&id_object=' + idOrg + '&course_id=' + Arena.course_id;
									actions += '<li><a href="' + editHref + '" '
										+ 'role="menuitem" class="lo-edit p-hover" '
										+ 'data-lo-type="' + node.data.type + '" data-id-object="' + idOrg + '">'
										+ '<span class="objlist-sprite i-sprite is-edit"></span>'
										+ '<span class="menu-label"> ' + Yii.t('standard', '_MOD') + '</span></a></li>';
									break;
								case 'video':
								case 'file':
								case 'item':
									actions += '<li><a href="' + ((editUrl !== false) ? editUrl : Arena.uploadDialogUrl) + '" role="menuitem" '
										+ 'class="lo-upload p-hover" data-lo-type="' + node.data.type + '" data-id-object="' + idOrg + '">'
										+ '<span class="objlist-sprite i-sprite is-edit"></span>'
										+ '<span class="menu-label"> ' + Yii.t('standard', '_MOD') + '</span></a></li>';
									break;
								case 'authoring':
									if (parseInt(node.data.authoring_copy_of) <= 0) {
										var idResource = parseInt(node.data.idResource);
										var editHref = (editUrl !== false) ? editUrl : Docebo.rootAbsoluteBaseUrl + '/authoring/index.php?r=main/edit&id=' + idResource + '&id_course=' + Arena.course_id;
										actions += '<li><a href="' + editHref + '" '
											+ 'role="menuitem" class="lo-edit p-hover" '
											+ 'data-lo-type="' + node.data.type + '" data-id-object="' + idOrg + '">'
											+ '<span class="objlist-sprite i-sprite is-edit"></span>'
											+ '<span class="menu-label"> ' + Yii.t('standard', '_MOD') + '</span></a></li>';
									}
									break;
								case 'deliverable':
								default:
									var axEditUrl = (editUrl !== false) ? editUrl : Docebo.lmsAbsoluteBaseUrl + '/index.php?r=' + node.data.type + '/default/axEditLo&id_object=' + idOrg + '&course_id=' + Arena.course_id;
									actions += '<li><a href="' + axEditUrl + '" '
										+ 'role="menuitem" class="lo-edit p-hover" '
										+ 'data-lo-type="' + node.data.type + '" data-id-object="' + idOrg + '">'
										+ '<span class="objlist-sprite i-sprite is-edit"></span>'
										+ '<span class="menu-label"> ' + Yii.t('standard', '_MOD') + '</span></a></li>';
									break;
							} // end switch
						}
					}

					// Central LO push available for non placeholder objects
					if(Arena.openInCentralRepositoryUrl != '' && (node.data.type !== 'deliverable') && !node.data.isFolder && (typeof node.data.id_object == "undefined" || node.data.id_object == null )) {
						var pushLoUrl = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=centralrepo/centralRepo/pushFromLocalCourse&id_org=' + idOrg;
						var pushLoLabel = Yii.t('standard', 'Push object to Central Repository');
						actions += '<li><a href="' + pushLoUrl + '" rel="push-lo-modal-' + idOrg + '" class="open-dialog p-hover">' +
							'<span style="margin-right: 7px;"><i class="fa fa-hdd-o"></i></span>' +
							'<span class="menu-label"> '+pushLoLabel+'</span></a></li>';
					}

					// Delete Lo item (real LO or a Folder): launches a Dialog2.
					var deleteLoUrl = Arena.deleteLoUrl + '&id_object=' + idOrg + '&course_id=' + Arena.course_id;
					var deleteLabel = (typeof node.data.id_object !== "undefined" && node.data.id_object !== null) ? Yii.t('standard', 'Unlink') : Yii.t('standard', '_DEL');
					actions += '<li><a href="' + deleteLoUrl +
					'" rel="delete-single-lo-modal-' + idOrg +
					'" role="menuitem" class="open-dialog p-hover menuitem-delete-lo">' +
					'<span class="i-sprite is-remove red"></span>' +
					'<span class="menu-label"> '+deleteLabel+'</span></a></li>';


					// now we can display the menu of the object
					postfix.append( $('<div class="btn-group">' +
					'<a class="btn dropdown-toggle p-hover" data-toggle="dropdown" href="#"><i class="p-sprite ico-menu"></i><span class="caret"></span></a>' +
					'<ul class="dropdown-menu pull-right player-lo-actions" role="menu" aria-labelledby="dropdownMenu">' +
					actions +
					'</ul>' +
					'</div>') );
				},

				onRender: function (node, nodeSpan) {
					var versionName = node.data.versionName;
					var idObject = parseInt(node.data.id_object);
					var keepUpdated = node.data.keep_updated;
					var versionData = '';

					$(nodeSpan).children('.dynatree-title').text(node.data.title_shortened);

					var nodeTitleElement = $(nodeSpan).find('.player-postfix');

					if(typeof versionName !== "undefined" && idObject > 0){
						if(!keepUpdated || keepUpdated != 0){
							versionData = Yii.t('standard', 'Allways keep updated to the latest version');
						} else{
							var versionCreateDate = node.data.version_create_date;
							if(!node.data.showVersioning){
								versionData = versionCreateDate;
							} else{
								versionData = versionName + ' - ' + versionCreateDate;
							}
						}
						nodeTitleElement.prepend($('<a href="' + Arena.openInCentralRepositoryUrl + '"><span class="version-info"><i  rel="tooltip" title="' + versionData + '" class="fa fa-hdd-o"></i></span></a>'));
						nodeTitleElement.find('i').tooltip();
						if(Arena.openInCentralRepositoryUrl){
							nodeTitleElement.find('.version-info').on('click', function(){
								window.location.href = Arena.openInCentralRepositoryUrl;
							});
						}


					}
					$(document).trigger("lo_list_refresh_hook");
				},

				children: data

			});

			$('#player-arena-overall-objlist').dynatree("getTree").reload();

			$('#player-arena-overall-objlist').children('ul').sortable({
				appendTo: "body",
				handle: ".lo-drag",
				helper: "clone",
				zIndex: 9999,
				revert: true,
				connectWith: ".player-arena-tree-folder",
				placeholder: "player-arena-overall-placeholder",
				start: function (event, ui) {
					ui.placeholder.height(ui.item.siblings("li").height());
					$(ui.helper).addClass("player-drag-proxy");
					$(ui.helper).attr('data-drop-type', 'sort');
					$(ui.helper).css('position', 'absolute');
				},
				beforeStop: function (event, ui) {

					updateZIndex();
					if ($(ui.helper).attr('data-drop-type') === 'sort') {

						var elements = $(this).sortable("toArray").filter(function (v) {
							return v !== '';
						});
						var elementIndex = $(ui.item).index();
						var siblings = [ elementIndex > 0 ? elements[elementIndex-1] : null, elementIndex < elements.length-1 ? elements[elementIndex+1] : null ];

						me.options.onNodeMoved(ui, $(ui.item).attr('data-object-id'), elementIndex, siblings, "sort");
					}

				},
				received: function (event, ui) {
					$(ui.helper).attr('data-drop-type', 'sort');
				}

			}).disableSelection();

			// Dialog2 related: handle "open-dialog" markup
			$('#player-arena-overall-objlist').controls();
		},

		blockTrees: function () {
			$('#player-arena-overall-objlist').block({
				message: null,
				css: {
					border: '3px solid #a00'
				}
			});
			$('#player-arena-overall-folders').block({
				message: null,
				css: {
					border: '3px solid #a00'
				}
			});
		},

		unblockTrees: function () {
			$('#player-arena-overall-objlist').unblock();
			$('#player-arena-overall-folders').unblock();
		},

		expandAll: function () {
			$('#player-arena-overall-folders').dynatree("getRoot").visit(function (node) {
				node.expand(true);
			});
			$('#player-arena-overall-objlist').dynatree("getRoot").visit(function (node) {
				node.expand(true);
			});
			if (Arena.learningObjectsManagement.tree.length > 0 && Arena.learningObjectsManagement.tree[0].children !== undefined) {
				this.showFolder(Arena.learningObjectsManagement.tree[0].children);
			}

		},

		getActiveFolderKey: function (strict) {
			activeDynaTreeNode = $('#player-arena-overall-folders').dynatree('getActiveNode');
			if (!activeDynaTreeNode) {
				return 0; // root
			} else {
				//the 'strict' parameter will exclude fake scormorg folders
				if (!activeDynaTreeNode.data.isFolder || (strict && activeDynaTreeNode.data.type != '')) {
					return false;
				}
				return activeDynaTreeNode.data.key;
			}
		},

        _loToSprite: {
            'authoring': 'is-converter',
            'file': 'is-file',
            'item': 'is-file',
            'htmlpage': 'is-htmlpage',
            'deliverable': 'is-deliverable',
			'lti': 'is-lti',
            'poll': 'is-poll',
            'test': 'is-test',
            'video': 'is-video',
			'googledrive': 'is-googledrive'
        },
		getSpriteForObject: function (/* String */object) {
			return (this._loToSprite[object] ? this._loToSprite[object] : '');
		}
	}

};