var WebinarSessionForm = {

	/* Date picker object */
	datePicker: null,

	/* Array of session dates for the current session */
	sessionDates: {},

	/* Currently selected session date */
	selectedDate: null,

	options: {

		/* Array of months */
		months: null, /*<?= TimeHelpers::getLocalizedMonthNames() ?>*/

		/* Array of timezone names with their GMT offsets */
		timezoneOffsets: null, /*<?= $this->getTimezoneOffsetsAsJson() ?>,*/
	},

	/**
	 * Some helpers to avoid having to type IDs again and again
	 * Just call the specified methods and get the jQuery DOM element
	 */
	selectors: {
		dayDate: function(){
			return $('#day-calendar');
		},
		dayName: function(){
			return $('#day-name');
		},
		dayDescription: function(){
			return $('.session-other_info.description');
		},
		startTime: function(){
			return $('#webinar-start-time');
		},
		duration: function(){
			return $('#webinar-duration-time');
		},
		timezone: function(){
			return $('#day-timezone');
		},
		timezoneLabel: function(){
			return $('#day-timezone-label');
		},

		joinInAdvanceTeacher: function(){
			return $('#join_in_advance_time_teacher');
		},
		joinInAdvanceUser: function(){
			return $('#join_in_advance_time_user');
		},
		webinarTool: function(){
			return $('#toolDropdown');
		},
		webinarToolCustomUrl: function(){
			return $('#webinar_custom_url');
		},
		webinarToolAccount: function(){
			return $('#tool_selection [name=webinar_id_tool_account]');
		},
		webinarToolParams: function(){
			return $('.tool-account-box .tool_params');
		}
	},


	init: function (params, options) {
		this.sessionDates = params.sessionDates;

		this.options = $.extend(this.options, options);

		// Set current date to today
		this.selectedDate = new Date();

		this.registerListeners();

		// Install click handlers on form buttons
		this.initDateForms();
	},

	getObjectSize: function (obj) {
		var size = 0, key;
		for (key in obj) {
			if (obj.hasOwnProperty(key)) size++;
		}
		return size;
	},

	registerListeners: function () {
		// Apply style to inputs in the form, that is in a modal
		$('.modal input').styler();

		// Fill in the calendar with days already set up for this session
		this.initCalendar();

		var manager = this;

		$('.edit-session-tabs > ul.nav.nav-tabs li').bind('click', function (e) {

			e.preventDefault();

			switch ($(e.target).attr('data-target')) {
				case '#sessionForm-dates':
					e.preventDefault();

					// Show datepicker
					manager.datePicker.show();

					// Refresh the date form
					manager.showDateForm();

					break;
				default:
					// Hide datepicker
					manager.datePicker.hide();

					// Hide the blank slate under calendar
					$('#popup_bootstroAddAnotherDate').hide();
					break;
			}
		});

		// Install click handler on Add date form button
		$('#session-date-add').on('click', function (ev) {
			manager.dateUpdate();
		});
	},
	initCalendar: function () {
		var manager = this;
		var calendar = $('#session-dates-calendar');
		this.datePicker = calendar.bdatepicker({
			format: 'yyyy-mm-dd',
			inline: true,
			language: yii.language,
			onRender: function (date) {
				var mysql_date = manager.getMysqlDate(date);
				if (mysql_date in manager.sessionDates)
					return ' session-day';
				else
					return '';
			}
		}).data('datepicker');

		// fix calendar position
		var placeDatePicker = function(){manager.datePicker.place()};
		$(window).on('resize', placeDatePicker);
		// Customize datepicker with our class
		this.datePicker.picker.addClass('session-dates');

		// Intercept dialog2.closed and destroy the calendar
		$(document).delegate(".modal-create-webinar-session", "dialog2.closed", function () {
			manager.datePicker.picker.remove();
			$(window).off('resize', placeDatePicker);
		});
		//the above may not work for some IE versions < 11. This patch should solve the issue.
		if (navigator.userAgent.indexOf("MSIE") != -1) {
			$('.modal-create-session').on('remove', function () {
				if (manager && manager.datePicker) {
					manager.datePicker.picker.remove();
				}
			});
		}

		// Install day selection handler
		calendar.on('changeDate', function (ev) {
			manager.selectedDate = ev.date;
			manager.showDateForm();
		});
	},

	/* Copies the current selected session date to a destination date */
	copyCurrentSessionDateFrom: function (dstDate) {
		var srcDateString = this.getMysqlDate(this.selectedDate);
		var dstDateString = this.getMysqlDate(dstDate);

		// If target date Date already in session? -> we can't copy it
		if (!(dstDateString in this.sessionDates)) {
			var srcDayObject = this.sessionDates[srcDateString];
			var dstDayObject = {
				day: dstDateString,
				name: srcDayObject.name,
				description: srcDayObject.description,
				time_begin: srcDayObject.time_begin,
				duration_minutes: srcDayObject.duration_minutes,
				timezone_begin: srcDayObject.timezone_begin,
				join_in_advance_time_teacher: srcDayObject.join_in_advance_time_teacher,
				join_in_advance_time_user: srcDayObject.join_in_advance_time_user,
				webinar_tool: srcDayObject.webinar_tool,
				id_classroom: srcDayObject.id_classroom,
				webinar_custom_url: srcDayObject.webinar_custom_url,
				id_tool_account: srcDayObject.id_tool_account,
				webinar_tool_params: srcDayObject.webinar_tool_params,
				id_session: srcDayObject.id_session
			};

			this.sessionDates[dstDateString] = dstDayObject;
			$('#must-complete-count #count').html(this.getObjectSize(this.sessionDates));
		}
	},

	/* Initializes form buttons and other stuff */
	initDateForms: function () {
		var manager = this;
		// Install click handler on Add date form button
		$('#session-date-add').on('click', function (ev) {
			manager.dateUpdate();
		});

		// Install click handler on "Select timezone"
		$('#select-timezone-button').on('click', function (ev) {
			$('#timezone-line').show();
		});

		// Init edit form calendar
		$('.edit-day-input').bdatepicker({
			format: 'yyyy-mm-dd',
			language: yii.language,
			onRender: function (date) {
				var mysql_date = manager.getMysqlDate(date);
				if (mysql_date in manager.sessionDates)
					return ' disabled';
				else
					return '';
			}
		});

		$('.edit-day-input').each(function (index, elem) {
			$(elem).bdatepicker().data('datepicker').picker.addClass('day-inputs');
		});

		// Install day selection handler
		this.selectors.dayDate().on('changeDate', function (ev) {
			manager.previousDate = manager.selectedDate;
			manager.selectedDate = ev.date;
		});

		// Install click on calendar icon
		$(".date-icon").on('click', function (ev) {
			$(this).prev('input').bdatepicker().data('datepicker').show();
		});

		// Intercept dialog2.closed and destroy the calendar
		$(document).delegate(".modal-create-session", "dialog2.closed", function () {
			$(".day-inputs").each(function (index, elem) {
				$(elem).remove();
			});
		});
		//the above may not work for some IE versions < 11. This patch should solve the issue.
		if (navigator.userAgent.indexOf("MSIE") != -1) {
			$('.modal-create-session').on('remove', function () {
				$(".day-inputs").each(function (index, elem) {
					$(elem).remove();
				});
			});
		}

		// Install click handler on the create date button
		$('#create-session-date').on('click', function (ev) {
			if (manager.updateOrCreateSessionDate()) {
				// Refresh days calendar
				$('#session-dates-calendar').bdatepicker().data('datepicker').fill();

				// Check if the empty session message should be displayed on not
				manager.updateConfirmButton();

				// Show view form
				manager.showViewForm();
			}
		});

		// Install click handler on the edit date button
		$('#edit-session-date').on('click', function (ev) {
			// Show edit form
			manager.dateUpdate();
		});

		// Install click handler on cancel "edit session date"
		$('#cancel-edit-session-date').on('click', function (ev) {
			// Loose current modifications and show the view/add form
			manager.showDateForm();
		});

		// Install click handler on delete session date
		$('#delete-session-date').on('click', function (ev) {
			// Delete current date and show the add form
			if (manager.deleteSessionDate()) {
				// Refresh days calendar
				$('#session-dates-calendar').bdatepicker().data('datepicker').fill();

				// Check if the empty session message should be displayed on not
				manager.updateConfirmButton();

				manager.dateCreate();
			}
		});

		// Install the click handler for the copy date button
		$('#make-copy-date').on('click', function (ev) {
			$('#copy-date-box').slideToggle('fast');
			$('#destination-day').val('');
		});

		// Install the make a copy handler
		$('#confirm-copy-session-date').on('click', function (ev) {
			manager.copyCurrentSessionDateFrom($('#destination-day').bdatepicker().data('datepicker').getUTCDate());

			// Check if the empty session message should be displayed on not
			manager.updateConfirmButton();

			// Refresh days calendar
			$('#session-dates-calendar').bdatepicker().data('datepicker').fill();

			// Close the copy date box
			$('#copy-date-box').slideToggle('fast');
		});

		// Check if the empty session message should be displayed on not and install click handler
		this.updateConfirmButton();
		$('.session-form-actions > .submit-session-button').on('click', function (ev) {
			if ($(this).is('.disabled'))
				return;

			if (manager.getObjectSize(manager.sessionDates) == 0) {
				$('[data-target=#sessionForm-dates]').click();

				// Show datepicker
				manager.datePicker.show();

				// Refresh the date form
				manager.showDateForm();
				return;
			}

			manager.clearErrorFlagsFromTabs();

			if (manager.validateSessionData()) {

				// Prepare the object to submit
				var formData = $('#session_form').serializeArray();
				$.each(manager.sessionDates, function (day, value) {
					var field;
					for (field in manager.sessionDates[day]) {
						if (field != 'id_session') {
							var valueToInsert = manager.sessionDates[day][field];
							var typeOfOjb = $.type(valueToInsert);
							if(typeOfOjb === 'object'){
								valueToInsert = JSON.stringify(valueToInsert);
							}
							formData.push({
								name: "dates[" + day + "][" + field + "]",
								value: valueToInsert
							});
						}
					}
				});

				// Submit form via ajax
				$.ajax({
					dataType: 'json',
					type: 'POST',
					url: $("#session_form").attr('action'),
					cache: false,
					data: $.param(formData),
					success: function (data) {
						if (data.success == true) {
							$.fn.yiiGridView.update('sessions-management-grid', {
								data: $("#session-grid-search-form").serialize()
							});
							try {
								$('.modal-create-webinar-session').find('.modal-body').dialog2("close");
							} catch (e) {
							}
						} else {
							// Show error message
							$('.session-save-error').show().html(data.message);

							manager.clearErrorFlagsFromTabs();
							manager.resetDayErrors();

							// Restore edit form
							manager.toggleFormLoader(false);
						}
					},
					beforeSend: function () {
						manager.toggleFormLoader(true);
					},
					fail: function (jqXHR, status, errorThrown) {
						// Show error message
						$('.session-error').html('<span class="red">' + this.options.translations_OPERATION_FAILURE + '</span>').show();

						// Restore edit form
						manager.toggleFormLoader(false);
					}
				});
			} else {
				$('#sessionFormTab li.hasErrors').first().find('>a').click();

				// Hide the blank slate under calendar
				$('#popup_bootstroAddAnotherDate').hide();
			}
		});

		// Install click handler of cancel buttons
		$('.cancel-session-modal').on('click', function (ev) {
			$('.modal-create-webinar-session').find('.modal-body').dialog2("close");
		});
	},

	/* Remove the session date from the sessionsDate array */
	deleteSessionDate: function () {
		// Get the JS object for the day
		var dateString = this.getMysqlDate(this.selectedDate);
		if (dateString in this.sessionDates) { // Date already in session -> get object from sessionDates array
			delete this.sessionDates[dateString];
			$('#must-complete-count #count').html(this.getObjectSize(this.sessionDates));
			return true;
		} else
			return false;
	},

	// Helper methods follow:
	/* Converts a JS date object into a mysql string yyyy-MM-dd */
	getMysqlDate: function (date) {
		var d = date.getUTCDate();
		var m = date.getUTCMonth() + 1;
		var y = date.getUTCFullYear();
		d = (d < 10 ? '0' : '') + d;
		m = (m < 10 ? '0' : '') + m;
		return y + '-' + m + '-' + d;
	},

	/* Returns the suffix of the day of month */
	getMonthDaySuffix: function (n) {
		if (n >= 11 && n <= 13)
			return "th";
		else {
			switch (n % 10) {
				case 1:
					return "st";
				case 2:
					return "nd";
				case 3:
					return "rd";
				default:
					return "th";
			}
		}
	},

	/* Returns date name in long format */
	getLongDateName: function (date) {
		var suffix = (this.currentLanguage == 'english' ? this.getMonthDaySuffix(date.getUTCDate()) : '');
		var formattedDay = this.options.months[date.getUTCMonth()] + " " + date.getUTCDate() + suffix + ", " + date.getUTCFullYear();
		return formattedDay;
	},

	/**
	 * Shows the edit/create date form
	 * (depending on the currently selected date and whether or not there is already an entry for it)
	 */
	showDateForm: function () {
		var dateString = this.getMysqlDate(this.selectedDate);
		if (dateString in this.sessionDates) // Date already in session -> Show edit form
			this.showViewForm();
		else  // Date not in session -> Show new date form
			this.dateCreate();
	},

	clearErrorFlagsFromTabs: function(){
		$('#sessionFormTab li').removeClass('hasErrors');
	},

	/* Validates session data, before submitting the form to the controller (via ajax) */
	validateSessionData: function () {

		// Clear old errors from all tabs
		$('#WebinarSession_min_attended_dates_for_completion').removeClass('error');
		$('#must-complete-count-error').html('');
		$('#WebinarSession_max_enroll').removeClass('error');
		$('#errorMessage').html('');

		var result = true;

		$("#LtCourseSession_max_enroll").removeClass('error');
		$('#session-name-error-message').html('');
		$('#max-enroll-error-message').html('');

		// Check if session name is not empty
		var sessionName = $("#WebinarSession_name").val().trim();
		if (sessionName == '') {
			$("#WebinarSession_name").addClass('error');
			$('#session-name-error-message').html(this.options.translations._SESSION_NAME_IS_REQUIRED + '.');
			result = false;
			$('#sessionFormTab a[data-target="#sessionForm-details"]').closest('li').addClass('hasErrors');
		}

		// Check if max enrollments is greater than zero
		var maxEnrollments = $('#WebinarSession_max_enroll').val().trim();
		if ((maxEnrollments == '') || (maxEnrollments % 1 !== 0) || parseInt(maxEnrollments)<=0) {
			result = false;
			$("#WebinarSession_max_enroll").addClass('error');
			$('#max-enroll-error-message').html(this.options.translations._ENTER_A_NUMBER_GREATER_THAN_0 + '.');
			$('#sessionFormTab a[data-target="#sessionForm-details"]').closest('li').addClass('hasErrors');
		}

		var selectedCompletionType = $("input[name='WebinarSession[evaluation_type]']:checked").val();
		if(selectedCompletionType==='joined_webinar'){
			var enteredNumber = $('#WebinarSession_min_attended_dates_for_completion').val();
			var numberOfDates = this.getObjectSize(this.sessionDates);
			if((enteredNumber == '') || (enteredNumber % 1 !== 0) || parseInt(enteredNumber)<=0){
				result = false;
				$('#WebinarSession_min_attended_dates_for_completion').addClass('error');
				$('#must-complete-count-error').html(this.options.translations._ENTER_A_NUMBER_GREATER_THAN_0 + '.');
				$('#sessionFormTab a[data-target="#sessionForm-evaluation"]').closest('li').addClass('hasErrors');
			}else if(enteredNumber > numberOfDates){
				result = false;
				$('#WebinarSession_min_attended_dates_for_completion').addClass('error');
				$('#must-complete-count-error').html(this.options.translations._MIN_COMPLETED_DATES_CAN_NOT_BE_BIGGER_THAN_NUMBER_OF_DATES + '.');
				$('#sessionFormTab a[data-target="#sessionForm-evaluation"]').closest('li').addClass('hasErrors');
			}
		}

		return result;
	},

	/* Toggle the form loader view */
	toggleFormLoader: function (makeVisible) {
		var container = $('.modal-create-webinar-session');
		if (makeVisible) {
			// Hide current multi-tab div inside modal and date picker
			$('.edit-session-tabs').hide();
			this.datePicker.hide();

			// Show loader content
			container.find('.loader').show();
			container.addClass('loading');
		} else {
			// Hide loader content
			container.find('.loader').hide();
			container.removeClass('loading');

			// Show current multi-tab div inside modal and date picker
			$('.edit-session-tabs').show();
			if ($('#sessionForm-dates').is(':visible'))
				this.datePicker.show();
		}
	},

	/* Updates the status of the form confirm button */
	updateConfirmButton: function () {
		if (this.getObjectSize(this.sessionDates) > 0) {
			// Session can be saved -> unlock all submit buttons
			$('.session-error').html('').hide();
			$('.session-form-actions > .submit-session-button').removeClass('disabled');
			$('#sessionForm-details .session-form-actions > .submit-session-button').val(this.options.translations._SAVE);
		}
		else {
			// Can't save session -> disable all submit buttons ...
			$('.session-form-actions > .submit-session-button').addClass('disabled');
			// ... with the exception of Details form (should show "Set up dates" and lead to the Session Dates form when clicked)
			$('#sessionForm-details .session-form-actions > .submit-session-button').val(this.options.translations._SET_UP_DATES);
			$('#sessionForm-details .session-form-actions > .submit-session-button').removeClass('disabled');
			$('.session-error').html(this.options.translations._MISSING_SESSION_DATE).show();
		}
	},

	/* Validates the information in the date form */
	validateDayInformation: function () {
		var result = true;
		var manager = this;
		var regex = new RegExp("^(?:0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$"); // Regexp for time validation (24H)

		// Reset previous error messages
		this.resetDayErrors();

		// Check if time_begin and time_end are present and valid
		var webinarTimeBegin = this.selectors.startTime().val().trim();
		if ((webinarTimeBegin == '') || !regex.test(webinarTimeBegin)) {
			this.selectors.startTime().addClass('error');
			result = false;
		}

		var webinarDurationMinimum = 15;
		var webinarDurationMaximum = 1425;
		var webinarDuration = parseInt(this.selectors.duration().val().trim());
		if ((webinarDuration == '') || webinarDuration < webinarDurationMinimum || webinarDuration > webinarDurationMaximum) {
			this.selectors.duration().addClass('error');
			result = false;
		}

		if (result == false) {
			$('#time-error-message').html(this.options.translations._INVALID_OR_MISSING_DATE_HOURS + '.');
		}

		var isBeginTimeInThePast = function(){
			var dayName = manager.selectors.dayDate().val();
			var parts = dayName.split('-');

			var year = parts[0];
			var month = parts[1];
			var day = parts[2];

			var startTime = manager.selectors.startTime().val();
			parts = startTime.split(':');

			var hour = parts[0];
			var minutes = parts[1];
			
			var date = new Date();
			date.setUTCFullYear(year, month-1, day);
			date.setUTCHours(hour, minutes, 0, 0);

			var timezoneName = manager.selectors.timezone().val();
			var timezoneOffset = manager.options.timezoneOffsetsSeconds[timezoneName];

			var dateBeingAdded = new Date(date.getTime() - (timezoneOffset*1000));

			var dateBegingAddedUTC = dateBeingAdded.getTime();
			var currentDateUTC = new Date().getTime();
			if(dateBegingAddedUTC < currentDateUTC){
				return true;
			}else {
				return false;
			}

		};

		if(isBeginTimeInThePast()){
			result = false;
			$('#time-error-message').html(this.options.translations._DATE_CAN_NOT_BE_PAST + '.');
		}

		var webinarDateName = this.selectors.dayName().val().trim();
		if(!webinarDateName || webinarDateName == ''){
			this.selectors.dayName().addClass('error');
			$('#day-name-error').html(this.options.translations._ERROR_NONAME+'.');
			result = false;
		}

		var webinarTool = this.selectors.webinarTool().val();
		if (!webinarTool || webinarTool == '') {
			this.selectors.webinarTool().addClass('error');
			$('#webinar-tool-error').html(this.options.translations._NO_TOOL_SELECTED + '.');
			result = false;
		}

		if(webinarTool==='custom'){
			if(this.selectors.webinarToolCustomUrl().val().trim() == ''){
				this.selectors.webinarToolCustomUrl().addClass('error');
				$('#webinar-tool-error').html(this.options.translations._NO_CUSTOM_TOOL_URL + '.');
				result = false;
			}
		}else{
			// Some webinar tool selected
			if(this.options.validateToolAccount) {
				if(this.selectors.webinarToolAccount().val().trim()==''){
					this.selectors.webinarToolAccount().addClass('error');
					$('#webinar-tool-error').html(this.options.translations._WEBINAR_ACCOUNT_MISSING+'.');
					result = false;
				}
			}
		}

		return result;
	},

	/* Show the view date form */
	showViewForm: function () {
		var dateString = this.getMysqlDate(this.selectedDate);
		var dayObject = this.sessionDates[dateString];

		// Update static labels
		$('#view-date-form > h2').html(this.getLongDateName(this.selectedDate));

		$('#view-date-form > .day-title .name-label').html(dayObject.name);
		$('#view-date-form > .day-title .description-label').html(dayObject.description);

		/**
		 * Add a specified number of minuts to a time string in the format "09:00"
		 * @param timeBeginStr
		 * @param durationMinutes
		 * @returns {string}
		 */
		var calculateTimeEnd = function(timeBeginStr, durationMinutes){
			var parts = timeBeginStr.split(':');
			var hour = parseInt(parts[0]);
			var minute = parseInt(parts[1]);

			var tmpDate = new Date();
			tmpDate.setHours(hour);
			tmpDate.setMinutes(minute);
			tmpDate.setSeconds(tmpDate.getSeconds() + durationMinutes*60);

			var padWithleadingZero = function(n){
				return (n < 10) ? ("0" + n) : n;
			};
			return padWithleadingZero(tmpDate.getHours())+":"+padWithleadingZero(tmpDate.getMinutes());
		};

		var timeEndStr = calculateTimeEnd(dayObject.time_begin, dayObject.duration_minutes);

		$('#view-date-form > .day-time-line > div').html(dayObject.time_begin + ' - ' + timeEndStr
			+ '<span style="margin-left: 40px;">' + this.options.timezoneOffsets[dayObject.timezone_begin] + '</span>');

		$('#view-date-form > .day-location > div').html(this.options.webinarToolNames[dayObject.webinar_tool]);

		// Hide other tabs
		$('#edit-date-form').hide();
		$('#no-date-form').hide();

		// Close the clone day box
		$('#copy-date-box').hide();

		// Show the blank slate under calendar
		$('#popup_bootstroAddAnotherDate').show();

		// Show the view form
		$('#view-date-form').show();
	},

	/* Resets day error messages */
	resetDayErrors: function () {
		this.selectors.dayName().removeClass('error');
		this.selectors.startTime().removeClass('error');
		this.selectors.duration().removeClass('error');
		this.selectors.webinarTool().removeClass('error');
		this.selectors.webinarToolAccount().removeClass('error');
		this.selectors.webinarToolCustomUrl().removeClass('error');
		$('#time-error-message').html('');
		$('#webinar-tool-error').html('');
	},

	/* Creates a new session date or edits an existing one */
	updateOrCreateSessionDate: function () {

		// Perform validation
		if (this.validateDayInformation() == true) {

			// If the current day was changed, delete the old record in sessionsDate
			if (this.previousDate) {
				var previousDateString = this.getMysqlDate(this.previousDate);
				if (previousDateString in this.sessionDates)
					delete this.sessionDates[previousDateString];
				this.previousDate = null;
			}

			// Prepare the JS object for the day
			var dateString = this.getMysqlDate(this.selectedDate);
			var dayObject = null;
			if (dateString in this.sessionDates) // Date already in session -> get object from sessionDates array
				dayObject = this.sessionDates[dateString];
			else {
				dayObject = {}; // New session date -> create new object and add it to the array
				this.sessionDates[dateString] = dayObject;
			}

			// Fill in JS object from form fields
			dayObject.day = this.selectors.dayDate().val();
			dayObject.name = this.selectors.dayName().val();
			dayObject.description = this.selectors.dayDescription().val();
			dayObject.time_begin = this.selectors.startTime().val();
			dayObject.duration_minutes = this.selectors.duration().val();
			dayObject.timezone_begin = this.selectors.timezone().val();
			dayObject.join_in_advance_time_teacher = this.selectors.joinInAdvanceTeacher().val();
			dayObject.join_in_advance_time_user = this.selectors.joinInAdvanceUser().val();
			dayObject.webinar_tool = this.selectors.webinarTool().val();

			if (dayObject.webinar_tool === 'custom') {
				dayObject.webinar_custom_url = this.selectors.webinarToolCustomUrl().val();
			} else {
				dayObject.id_tool_account = this.selectors.webinarToolAccount().val();

				var params = {};
				this.selectors.webinarToolParams().each(function(){
					var param = $(this);
					params[param.attr('name')] = param.val();
				});
				dayObject.webinar_tool_params = params;
			}

			$('#must-complete-count #count').html(this.getObjectSize(this.sessionDates));
			return true;

		} else
			return false;
	},

	/* Shows the new date form */
	dateCreate: function () {
		// Update date label
		$('#no-date-form > h2').html(this.getLongDateName(this.selectedDate));

		// Hide the blank slate under calendar
		$('#popup_bootstroAddAnotherDate').hide();

		$('#edit-date-form').hide();
		$('#view-date-form').hide();
		$('#no-date-form').show();
	},

	/* Shows the edit date form */
	dateUpdate: function () {
		var that = this;

		$('#no-date-form').hide();
		$('#view-date-form').hide();

		// Reset previous error messages
		this.resetDayErrors();

		// Show edit form
		$('#edit-date-form').show();

		var isDateEdit = false;

		// Get information about the currently selected date
		var dateString = this.getMysqlDate(this.selectedDate);
		var dayObject = null;
		if (dateString in this.sessionDates) { // Date already in session -> get information from sessionDates array
			dayObject = this.sessionDates[dateString];
			$('#create-session-date').attr('value', this.options.translations._UPDATE_SESSION_DATE);
			isDateEdit = true;
		}
		else { // Date not in session -> Create new "empty" day object
			dayObject = {
				day: dateString,
				name: '',
				description: '',
				time_begin: '09:00',
				duration_minutes: 30,
				timezone_begin: this.options.currentTimezone,
				join_in_advance_time_teacher: 0,
				join_in_advance_time_user: 0
			};
			$('#create-session-date').attr('value', this.options.translations._CREATE_SESSION_DATE);
		}

		var timezoneOffset = this.options.timezoneOffsets[dayObject.timezone_begin].match(/(\(.*\))/g) ;
		// Fill in edit form fields
		this.selectors.dayDate().val(dayObject.day);
		this.selectors.dayName().val(dayObject.name);
		this.selectors.dayDescription().val(dayObject.description);
		this.selectors.startTime().val(dayObject.time_begin);
		this.selectors.duration().val(dayObject.duration_minutes);
		this.selectors.timezone().val(dayObject.timezone_begin);
		this.selectors.timezoneLabel().html(timezoneOffset);
		this.selectors.joinInAdvanceTeacher().val(dayObject.join_in_advance_time_teacher);
		this.selectors.joinInAdvanceUser().val(dayObject.join_in_advance_time_user);
		this.selectors.webinarTool().val(dayObject.webinar_tool);
		this.selectors.webinarToolAccount().val(dayObject.id_tool_account);
		this.selectors.webinarToolCustomUrl().val(dayObject.webinar_custom_url);
		// @TODO fix this: this.selectors.webinarToolParams.val(dayObject.webinar_tool_params);

		this.selectors.webinarTool().data('id-account', dayObject.id_tool_account).trigger('change');
		this.selectors.webinarTool().one('accountsListLoaded', function(){
			var accountIdToRestore = $(this).data('id-account');

			if(accountIdToRestore){
				that.selectors.webinarToolAccount().val(accountIdToRestore);

				if(isDateEdit && that.options.isEditingSession){
					that.selectors.webinarToolAccount().attr('disabled', 'disabled');

					// @TODO iterate this.selectors.webinarToolParams() and restore the values to inputs that match the name within the ".tool-account-box" container
				}else{
					that.selectors.webinarToolAccount().removeAttr('disabled');
				}
			}
		});

		if(isDateEdit && this.options.isEditingSession){
			this.selectors.webinarTool().attr('disabled', 'disabled');
		}else{
			this.selectors.webinarTool().removeAttr('disabled');
		}
	}


};