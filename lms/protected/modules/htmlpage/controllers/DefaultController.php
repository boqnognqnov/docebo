<?php

class DefaultController extends HtmlpageBaseController
{

	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
				'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {

		return array(

				// Admins and Teachers can do all
				array('allow',
						'users'=>array('@'),
						'expression' => 'Yii::app()->user->getIsPu() || Yii::app()->controller->userCanAdminCourse',
				),

				// For Subscribers
				array('allow',
						'users'=>array('@'),
						'actions'=>array('index', 'content', 'keepAlive'),
						'expression' => 'Yii::app()->user->getIsPu() || Yii::app()->controller->userIsSubscribed',
				),

				// Deny by default
				array('deny', // if some permission is wrong -> the script goes here
						'users' => array('*'),
						'deniedCallback' => array($this, 'accessDeniedCallback'),
						'message' => Yii::t('course', '_NOENTER'),
				),

		);
	}


	public function actionIndex()
	{
		$this->render('index');
	}


	/**
	 * Save html page content
	 * <root>/files/upload_tmp folder.
	 *
	 */
	public function actionAxSaveLo() {

		$courseId = Yii::app()->request->getParam("course_id", null);
		$parentId = Yii::app()->request->getParam("parent_id", null);

		$thumb = Yii::app()->request->getParam("thumb", null);
		$shortDescription = urldecode(Yii::app()->request->getParam("short_description", ''));

		$response = new AjaxResult();
		$transaction = Yii::app()->db->beginTransaction();

		try {
			if (isset($_POST['LearningHtmlpage'])) {
				$postData = $_POST['LearningHtmlpage'];
				$postData['title'] = trim($postData['title']);
//				$postData['textof'] = Yii::app()->htmlpurifier->purify($postData['textof'], 'allowFrameAndTarget');
				$postData['textof'] = Yii::app()->htmlpurifier->purify($postData['textof'], 'allowFrameAndTargetLo');
			}

			// Validate input parameters
			if (!$courseId) { throw new CException("Invalid specified course."); }
			if (!is_numeric($parentId) || $parentId < 0) { $parentId = 0; }
			if (empty($postData['title'])) { throw new CException(Yii::t('error', 'Enter a title!')); }


			$learningObject = null;

			// edit operation
			if (intval($postData['idPage']) > 0) {
				// get htmlpage object
				$htmlPageObj = LearningHtmlpage::model()->findByPk($postData['idPage']);
			} else {
				$htmlPageObj = new LearningHtmlpage();
				$htmlPageObj->author = Yii::app()->user->getIdst();
			}

			//prepare AR object to be saved
			$htmlPageObj->attributes = $postData;

			//these will be needed by learning organization table
			$htmlPageObj->setIdCourse($courseId);
			$htmlPageObj->setIdParent($parentId);

			//do actual saving
			if (!$htmlPageObj->save()) {
				$errorsHtmlPage = $htmlPageObj->getErrors();

				$returnErrors = Yii::t('course', 'Error while saving HTML content'). ': <br /><ul class="customErrorSummary">';
				foreach($errorsHtmlPage as $errorsHtmlPageField) {
					if (is_array($errorsHtmlPageField))  {
						foreach($errorsHtmlPageField as $errorHtmlPage) {
							$returnErrors .= '<li>' . $errorHtmlPage . '</li>';
						}
					}
				}

				$returnErrors .= '</ul>';

				throw new CException($returnErrors);
			}

			//verify inserted data
			$resourceId = (int)$htmlPageObj->getPrimaryKey();
			$learningObject = LearningOrganization::model()->findByAttributes(array(
				'idResource' => $resourceId,
				'objectType' => LearningOrganization::OBJECT_TYPE_HTMLPAGE,
				'idCourse' => $courseId
			));
			if (!$learningObject) { throw new CException(Yii::t('course', 'Invalid learning object'). "(".$resourceId.")"); }

			//update LO
			$learningObject->title = $htmlPageObj->title;

//			if($shortDescription)
				$learningObject->short_description = Yii::app()->htmlpurifier->purify($shortDescription);
			if($thumb){
				$learningObject->resource = intval($thumb);
			}elseif($learningObject->resource){
				$learningObject->resource = 0;
			}

			$learningObject->saveNode();

			// DB Commit
			$transaction->commit();

			// Trigger event to let plugins save their own data from POST
			Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
				'loModel' => $learningObject
			)));

			$data = array(
					'resourceId' => $resourceId,
					'organizationId' => (int)$learningObject->getPrimaryKey(),
					'parentId' => (int)$parentId,
					'title' => $htmlPageObj->title,
					'message' => Yii::t('standard', '_OPERATION_SUCCESSFUL')
			);
			$response->setData($data)->setStatus(true);

		} catch (CException $e) {
			$transaction->rollback();
			$response->setMessage($e->getMessage())->setStatus(false);
		}

		$response->toJSON();
		Yii::app()->end();
	}


	/**
	 * Echo HTML page content as pure HTML. (to be inserted into an IFRAME)
	 *
	 * @param number $idObject
	 * @param boolean $forFrame
	 * @throws CException
	 */
	public function actionContent($id_object = false) {

		$transaction = Yii::app()->db->beginTransaction();

		try {
			$idObject = $id_object;
			$object = LearningOrganization::model()->findByPk($idObject);
			if (!$object || $object->objectType != LearningOrganization::OBJECT_TYPE_HTMLPAGE) {
				throw new CException('Invalid object ID: '.$idObject);
			}

			// Learning Object ID must match the course ($this->course_id is an ID set by PlayerBaseController... always)
			if ($object->idCourse != $this->course_id) {
					throw new CException('Learning Object does not match the course. Are you a hacker or something.... ?');
			}

			//now load html page specific information
			$idPage = (int)$object->idResource;
			$page = LearningHtmlpage::model()->findByPk($idPage);
			if (!$page) {
				throw new CException('Invalid page ID: '.$idPage);
			}

			// Track the page:  HTML is considered "completed" when it is just loaded
			$idUser = Yii::app()->user->id;
			$object = $object->getMasterForCentralLo($idUser);

			$track = LearningMaterialsTrack::model()->findByAttributes(array('idReference' => $object->getPrimaryKey(), 'idUser' => $idUser));
			if (!$track) {
				//the object has not been tracked yet for this user: create the record now
				$track = new LearningMaterialsTrack();
				$track->idUser = (int)$idUser;
				$track->idReference = (int)$object->getPrimaryKey();
				$track->idResource = (int)$page->getPrimaryKey();
			}
			$track->setStatus(LearningCommontrack::STATUS_COMPLETED);
			$track->setObjectType(LearningOrganization::OBJECT_TYPE_HTMLPAGE);
			if (!$track->save()) {
				throw new CException("Error while saving track data");
			}
			UserLimit::logActiveUserAccess($idUser);

			//load content
			$html = $this->renderPartial("_launchpad", array(
				'pageModel' => $page,
				'idReference' => $idObject,
			), true);

			//finish action
			$transaction->commit();

			echo $html;
			Yii::app()->end();


		} catch (CException $e) {
			$transaction->rollback();
			echo $e->getMessage();
		}

	}

	public function actionKeepAlive() {
		// extend the session
		Yii::app()->user->getIsGuest();

		$data = array(
			'success' => true
		);

		header('Content-type: text/javascript');
		if (isset($_GET['jsoncallback'])) {
			// JSONP callback
			$jsoncallback = preg_replace('/[^a-zA-Z\._]+/', '', $_GET['jsoncallback']);
			echo $jsoncallback. '(' . CJSON::encode($data) . ')';
		} else {
			// standard Json
			echo CJSON::encode($data);
		}

		// Update course+user session tracking to register user's course activity (time in course)
		if (isset($_REQUEST['course_id']))
			ScormApiBase::trackCourseSession((int)Yii::app()->user->idst, $_REQUEST['course_id']);

		Yii::app()->end();
	}

	/**
	 * Edit HTML Page Dialog
	 *
	 */
	public function actionAxEditHtmlPage() {
		$htmlPageObj = new LearningHtmlpage();

		$idResource = intval(Yii::app()->request->getParam("idResource", null));
		$learningObject = null;
		if ($idResource > 0) {
			$learningObject = LearningOrganization::model()->findByAttributes(array('idOrg' => $idResource, 'objectType' => LearningOrganization::OBJECT_TYPE_HTMLPAGE));
			if ($learningObject) {
				// get htmlpage object
				$htmlPageObj = LearningHtmlpage::model()->findByPk($learningObject->idResource);
			}
		}

		$isListViewLayout = false;
		if($this->courseModel->idCourse)
			$isListViewLayout = LearningCourse::getPlayerLayout($this->courseModel->idCourse)==LearningCourse::$PLAYER_LAYOUT_LISTVIEW;

		$response = new AjaxResult();
		$html = $this->renderPartial("_edit_html",
			array(
				'htmlPageObj'=>$htmlPageObj,
				'loModel'=>$learningObject,
				'courseModel'=>$this->courseModel,
				'lo_type'=>LearningOrganization::OBJECT_TYPE_HTMLPAGE,
				'isListViewLayout' => $isListViewLayout
			), true, true);

		$response->setStatus(true)->setHtml($html)->toJSON();
		Yii::app()->end();
	}


	public function actionAxListAllowedIframeUrls() {
		$model = CoreSettingWhitelist::model()->findByAttributes(array('type' => 'lo_iframe_url'));
		$searchInput = Yii::app()->request->getParam("search_input", null);
		$search = (isset($searchInput) && !empty($searchInput));

		$items = array();
		$allSources = array();
		if($model) {
			// Get the list with whitelisted URLs
			$allSources = CJSON::decode($model->whitelist);
		}

		$i = 0;
		if (is_array($allSources)) {
			foreach ($allSources as $item) {
				if ($search) {
					if (stripos($item, $searchInput) !== false) {
						$items[] = array('id' => $i, 'url' => $item);
						$i++;
					}
				} else {
					$items[] = array('id' => $i, 'url' => $item);
					$i++;
				}
			}
		}

		$pageSize = Settings::get('elements_per_page', 10);
		if(!empty($customConfig['elements_per_page'])){
			$pageSize = $customConfig['elements_per_page'];
		}

		// Used to populate the grid
		$dataProvider = new CArrayDataProvider($items, array(
			'pagination' => array('pageSize' => $pageSize),
			'totalItemCount' => count($items),
		));

		$listViewParams = array(
			'disableMassSelection' => true,
			'disableMassActions' => true,
			'gridId' => "allowedUrlsComboListView",
			'dataProvider' => $dataProvider,
			'gridTemplate'	=> '{items}{summary}{pager}',
			'columns' => array(
				array(
					'name' => 'url',
					'value' => 'strip_tags($data[url])',
					'header' => Yii::t('admin', 'ALLOWED URLs'),
					'htmlOptions'=>array('style'=>'max-width: 300px; word-wrap: break-word; vertical-align:top;border-bottom: 1px solid #ddd;'),
					'headerHtmlOptions' => array(),
				))
		);

		$this->renderPartial('_whitelisted_urls', array(
			'listViewParams' => $listViewParams
		), false, true);

		Yii::app()->end();
	}

}