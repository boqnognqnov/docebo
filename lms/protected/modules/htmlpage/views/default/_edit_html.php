<?php
$centralRepoContext = isset($centralRepoContext) ? true : false;
?>
<div id="<?= isset($containerId) ? 'player-centralrepo-uploader-panel' : 'player-arena-htmlpage-editor-panel' ?>" class="player-arena-editor-panel" xmlns="http://www.w3.org/1999/html">
	<div class="header"><?php echo Yii::t('storage', '_LONAME_htmlpage'); ?></div>
	<div class="content">
		<?php
		/* @var $htmlPageObj LearningHtmlpage */
		/* @var $form CActiveForm */
		?>

		<?php
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'htmlpage-editor-form',
			'method' => 'post',
			'action' => isset($saveUrl) ? $saveUrl : $this->createUrl("/htmlpage/default/axSaveLo"),
			'htmlOptions' => array(
				'class' => 'form-horizontal',
				'enctype' => 'multipart/form-data',
			)
		));
		?>

		<?= $form->hiddenField($htmlPageObj, 'idPage'); ?>

		<div id="<?= isset($containerId) ? $containerId : 'player-arena-uploader' ?>">
			<div>
				<div class="tabbable"> <!-- Only required for left/right tabs -->
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab1" data-toggle="tab"><?=LearningOrganization::objectTypeValue($lo_type)?></a></li>
						<li><a href="#lo-additional-information" data-toggle="tab"><?=Yii::t('myactivities', 'Additional info')?></a></li>

						<?php
						// Raise event to let plugins add new tabs
						if(!$centralRepoContext){
							Yii::app()->event->raise('RenderEditLoTabs', new DEvent($this, array(
								'courseModel' => $courseModel,
								'loModel' => isset($loModel) ? $loModel : null
							)));
						}
						?>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab1">
							<div class="player-arena-uploader">

								<div class="control-group">
									<?php echo CHtml::label(Yii::t('standard', '_TITLE'), 'LearningHtmlpage_title', array(
										'class' => 'control-label'
									)); ?>
									<div class="controls">
										<?= $form->textField($htmlPageObj, 'title', array('class' => 'input-block-level')) ?>
									</div>
								</div>

								<?php
								$displayExtraInfo = '';
								if(Yii::app()->user->isGodAdmin) {
									$displayExtraInfo = Yii::t('course', 'If you\'re planning to insert an IFRAME into the learning object, you must whitelist the URL(s) to display in the Admin &gt; Advanced Settings area, to prevent securiry attacks.');
								} elseif(!isset($saveUrl) && LearningCourseuser::isUserLevel(Yii::app()->user->idst, $courseModel->idCourse, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR) ||  Yii::app()->user->isPU) {
									$displayExtraInfo = Yii::t('course', '<span id="additional_iframe_info" class="text-left">If you\'re planning to <strong>insert an IFRAME</strong> into the learning object, you must whitelist the URL(s) to display in the <br />Admin &gt; Advanced Settings area, to prevent securiry attacks.');
								}

								if ($displayExtraInfo) {
								?>
								<div class="control-group">
									<div class="controls" style="border: 1px solid #e5e5e5; padding: 10px; font-size: 11px">
										<?=$displayExtraInfo; ?>
										<br />
										<br />
										<a class="blue-text open-dialog" data-dialog-class="list-allowed-iframe-urls"
										   href="<?= isset($iframeWhitelistUrl) ? $iframeWhitelistUrl : Docebo::createAbsoluteLmsUrl('htmlpage/default/axListAllowedIframeUrls&course_id='.$courseModel->idCourse); ?>"
										   title="<?= Yii::t('admin', 'Iframe whitelist'); ?>"
			                                data-dialog-title="<?= Yii::t('admin', 'Iframe whitelist'); ?>">
											<?= Yii::t('course', 'Click here to view allowed URLs.'); ?>
										</a> <?php
										if((!(Yii::app()->user->isGodAdmin) && LearningCourseuser::isUserLevel(Yii::app()->user->idst, $courseModel->idCourse, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR)) || Yii::app()->user->isPU) {
											echo Yii::t('course', 'To add/edit URLs, <strong>please refer to your local LMS administrator</strong>');
										} ?>
									</div>
								</div>
								<?php
								}
								?>
								<div class="control-group">
									<?php echo CHtml::label(Yii::t('standard', '_DESCRIPTION'), 'LearningHtmlpage_textof', array(
										'class' => 'control-label'
									)); ?>
									<div class="controls">
										<?php echo $form->error($htmlPageObj,'textof'); ?>
										<?= $form->textArea($htmlPageObj, 'textof', array('id' => 'player-arena-htmlpage-editor-textarea')); ?>
									</div>
								</div>
								<br>

							</div>
						</div>
						<div class="tab-pane" id="lo-additional-information">
							<?php
							$additionalInfoWidgetParams = array(
								'loModel'=> $loModel,
								'idCourse' => isset($courseModel) ? $courseModel->idCourse : null,
							);

							if($centralRepoContext){
								$additionalInfoWidgetParams['refreshUrl'] = Docebo::createAbsoluteLmsUrl('centralrepo/centralLo/getSlidersContent');
								$additionalInfoWidgetParams['deleteUrl'] = Docebo::createAbsoluteLmsUrl('centralrepo/centralLo/deleteImage');
							}
							?>
							<? $this->widget('common.widgets.PlayerLayoutAdditionalInfo', $additionalInfoWidgetParams); ?>
						</div>

						<?php
						// Raise event to let plugins add new tab contents
						if(!$centralRepoContext){
							Yii::app()->event->raise('RenderEditLoTabsContent', new DEvent($this, array(
								'courseModel' => isset($courseModel) ? $courseModel : null,
								'loModel' => isset($loModel) ? $loModel : null
							)));
						}
						?>
					</div>
				</div>

			</div>

		</div>

		<div class="text-right">
			<input type="submit" name="confirm_save_htmlpage" class="btn-docebo green big"
			       value="<?php echo Yii::t('standard', '_SAVE'); ?>">&nbsp;&nbsp;
			<a id="cancel_save_htmlpage"
			   class="btn-docebo black big" <?= isset($backButtonUrl) ? "href='" . $backButtonUrl . "'" : "" ?>><?php echo Yii::t('standard', '_CANCEL'); ?></a>
		</div>

		<?php if($idObject): ?>
			<input type="hidden" name="id_object" value="<?=$idObject ?>">
		<?php endif; ?>

		<?php $this->endWidget(); ?>

	</div>
</div>
<script type="text/javascript">
	/*<![CDATA[*/
	(function ($) {
		$(function () {
			$(document).controls();

			// add the url to the advanced settings -> e-Learning -> URLs whitelist
			$('a#open-iframe-setting').attr('href', '<?=Docebo::createAdminUrl('advancedSettings/index#e-learning'); ?>');

			<?php if(isset($containerId)): ?>
			TinyMce.attach('#player-arena-htmlpage-editor-textarea', 'standard_embed', {height: 300});
			<?php endif; ?>

			$('#htmlpage-editor-form').submit(function(){
				$('#htmlpage-editor-form input[type=submit]').attr('disabled','disabled').addClass("disabled");
			});

			$('input[name="confirm_save_htmlpage"]').on('click', function(){
				var title = $('.form-horizontal').find('#LearningHtmlpage_title').val();
					if (title.length <= 0) {
						Docebo.Feedback.show('error', 'Enter a title!');
						return false;
					}
			});

		});
	})(jQuery);

/*]]>*/
</script>