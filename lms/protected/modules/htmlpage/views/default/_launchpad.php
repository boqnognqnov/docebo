<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, target-densitydpi=160dpi, minimal-ui">
		<link href="<?= Yii::app()->player->getPlayerAssetsUrl() . '/css/htmlpage.css'?>" rel="stylesheet">
		<style>
			<?php
				$htmlCss = Settings::get('html_page_css') ? Settings::get('html_page_css') : '';
				Yii::app()->event->raise('ResolvingCoursePlayerHtmlLoCss', new DEvent($this, array(
					'player_html_lo_css' => &$htmlCss,
				)));
				echo $htmlCss;
			?>
		</style>
	</head>
	<body>
		<div class="player-launchpad-html-container container">
			<div class="row-fluid">
				<div class="span12">
					<div id="htmlpage-content-<?php echo $pageModel->getPrimaryKey(); ?>" class="player-arena-html-inline">
						<?php
							if((strpos($pageModel->textof, '<iframe') !== false)){
								$html = Yii::app()->htmlpurifier->purify($pageModel->textof, 'allowFrameAndTargetLo');
								$html = str_replace( '<iframe', '<iframe allowfullscreen ', $html)  ;
								echo $html;
							}else{
								echo Yii::app()->htmlpurifier->purify($pageModel->textof, 'allowFrameAndTargetLo');
							}
						?>
					</div>
				</div>
			</div>
		</div>
		<?php /* // Uncomment this if needed --------------------------------------------
		<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/jquery.min.js"></script>
		<script type="text/javascript">
			$(function(){

			});
		</script>
		*/ ?>
	</body>
</html>