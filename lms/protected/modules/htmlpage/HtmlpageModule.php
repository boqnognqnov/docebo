<?php

class HtmlpageModule extends CWebModule
{
	
	private $_assetsUrl;
	
	public function init()
	{
		$this->setImport(array(
			'htmlpage.models.*',
			'htmlpage.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			return true;
		}
		else
			return false;
	}
	
	/**
	 * Get module assets url path
	 *
	 * @return mixed
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('htmlpage.assets'));
		}
		return $this->_assetsUrl;
	}
	
	
}
