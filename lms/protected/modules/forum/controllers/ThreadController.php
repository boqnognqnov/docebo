<?php

class ThreadController extends PermissionController
{

	/**
	 * (non-PHPdoc)
	 * @see PermissionController::init()
	 */
	public function init() {
		parent::init();
		$this->forumModel = LearningForum::model()->findByPk($this->forum_id);
	}


	/**
	 * (non-PHPdoc)
	 * @see ForumBaseController::beforeAction()
	 */
	public function beforeAction($action) {
		return parent::beforeAction($action);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {

		return array(

			// God Admin users can do all
			array('allow',
				'users'=>array('@'),
				'expression' => 'Yii::app()->user->isGodAdmin',
			),

			// Logged in users CAN go. Complex check is done already in ForumBaseController::init()
			array('allow',
				'users'=>array('@'),
			),

			// Deny by default
			array('deny', // if some permission is wrong -> the script goes here
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
				'message' => Yii::t('course', '_NOENTER'),
			),

		);
	}


	/**
	 * List threads of a given forum
	 *
	 * @throws CException
	 */
	public function actionIndex() {
		// If Thread ID is passed, forward directly to messages of the thread
		if ($this->thread_id) {
			$this->forward('messages', true);
		}

		// Get Forum model
		$forum = $this->forumModel;
		$thread = new LearningForumthread('search');

		if ($forum) {
			$thread->unsetAttributes();
			if(isset($_GET['LearningForumthread']))
				$thread->attributes = $_GET['LearningForumthread'];
			$thread->idForum = $forum->idForum;

			$this->render('index', array(
				'forum' => $forum,
				'thread' => $thread,
			));
		}
		// Forward to other actions if NO forum model
		else {
			if ($this->course_id) {
				$this->forward('courseForums', true);
			}
			else  {
				Yii::app()->user->setFlash('error', Yii::t('course', '_NOENTER'));
				$this->redirect(array('/site'),true);
			}
		}
	}


	/**
	 * Register module specific assets
	 */
	public function registerResources() {

		parent::registerResources();

		if (!Yii::app()->request->isAjaxRequest) {
			$cs = Yii::app()->getClientScript();
			$assetsUrl = $this->module->assetsUrl;

			// Register JS script(s)
			$options = array();
			$options = CJavaScript::encode($options);
			$script = "";
			$cs->registerScript("forumThreadControllerJs1", $script , CClientScript::POS_BEGIN);

		}

	}

	/**
	 * Create thread
	 */
	public function actionAxCreate()
	{
		$result = new AjaxResult();
		$forum_id = Yii::app()->request->getParam("forum_id", false);
		$message = new LearningForummessage('create');

		if ($this->_canCreateThread())
		{
			if (isset($_POST['LearningForummessage']))
			{
				//check block settings
				$forum = LearningForum::model()->findByPk($forum_id);
				$forumBlock = PlayerBaseblock::model()->findByAttributes(array('course_id' => $forum->idCourse, 'content_type' => PlayerBaseblock::TYPE_FORUM));
				$forumParams = ($forumBlock ? CJSON::decode( $forumBlock->params ) : array());
				if (!empty($forumParams) && isset($forumParams['word_count']) && (int)$forumParams['word_count'] > 0) {
					$countWords = ForumHelper::countWords($_POST['LearningForummessage']['textof']);
					if ($countWords > (int)$forumParams['word_count']) {
						//for some reason, we have more words than allowed: return an error to the client
						$result->setStatus(false)->setMessage('Words limit exceeded (counted words: '.$countWords.', max allowed: '.(int)$forumParams['word_count'].')')->toJSON();
						Yii::app()->end();
					}
				}
				//save the message text in DB
				$message->setAttributes($_POST['LearningForummessage']);
				if ($message->validate())
				{
					$thread = new LearningForumthread();
					$thread->title = $message->title;
					$thread->idForum = $forum_id;
					$thread->author = Yii::app()->user->id;
					$thread->posted = Yii::app()->localtime->toLocalDateTime();
					$thread->save(false);

					$event = new DEvent($this, array('thread' => $thread));
					Yii::app()->event->raise('onForumThreadCreate', $event);

					$message->idThread = $thread->idThread;
					$message->author = Yii::app()->user->id;
					$message->posted = Yii::app()->localtime->toLocalDateTime();
					$message->save(false);

					// So message is saved, now we need to check if there are files added to the message
					if(!empty($_POST['LearningForummessage']['file']['name']) && strlen($_POST['LearningForummessage']['file']['name']) > 0) {
						$message->original_filename = $_POST['LearningForummessage']['file']['name'];

						$fileHelper = new CFileHelper();
						$extension = strtolower($fileHelper->getExtension($message->original_filename));
						$message->path = $_POST['LearningForummessage']['file']['path'] . '.' . $extension;

						$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_FILE);
						$localFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $message->path;

						if ($storage->store($localFilePath)) {
							// Delete the temp file
							unlink(Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $message->path);

							$message->save(false);
						}

					}

					$event = new DEvent($this, array('message' => $message, 'thread_id' => $thread->idThread));
					Yii::app()->event->raise('onForumMessageCreate', $event);

					$result->setStatus(true)->toJSON();
				}
				else
					$result->setStatus(false)->setMessage(CHtml::errorSummary($message, '', ''))->toJSON();
			}
			else
			{
				$forumBlock = PlayerBaseblock::model()->findByAttributes(array('course_id' => Yii::app()->player->course->idCourse, 'content_type' => PlayerBaseblock::TYPE_FORUM));
				$forumParams = ($forumBlock ? CJSON::decode( $forumBlock->params ) : array());

				$html = $this->renderPartial('_create', array(
					'model'         => $message,
					'forum_id'      => $forum_id,
					'forumParams'   => $forumParams,
				), true, true);
				$result->setStatus(true)->setHtml($html)->toJSON();
			}
		}
		else
			$result->setStatus(false)->setMessage(Yii::t('course', '_NOENTER'))->toJSON();
	}

	public function actionAxUpdate()
	{
		$result = new AjaxResult();
		$thread_id = Yii::app()->request->getParam("thread_id", false);
		$forum_id = Yii::app()->request->getParam("forum_id", false);
		$thread = LearningForumthread::model()->findByPk($thread_id);
		if ($thread_id && $thread)
		{
			if ($this->_canUpdateThread($thread))
			{
				if (isset($_POST['LearningForumthread']))
				{
					$thread->setAttributes($_POST['LearningForumthread']);
					if ($thread->validate())
					{
						$thread->save(false);

						$event = new DEvent($this, array('thread' => $thread));
						Yii::app()->event->raise('onForumThreadUpdate', $event);

						$result->setStatus(true)->toJSON();
					}
					else
						$result->setStatus(false)->setMessage(CHtml::errorSummary($thread, '', ''))->toJSON();
				}
				else
				{
					$html = $this->renderPartial('_update', array(
						'model' => $thread,
						'forum_id' => $forum_id,
					), true, true);
					$result->setStatus(true)->setHtml($html)->toJSON();
				}
			}
			else
				$result->setStatus(false)->setMessage(Yii::t('course', '_NOENTER'))->toJSON();
		}
		else
			$result->setStatus(false)->setMessage(Yii::t('forum', 'Thread not found!'))->toJSON();
	}


	public function actionAxMove()
	{
		$result = new AjaxResult();
		if ($this->_canModerateThread())
		{
			$thread_id = Yii::app()->request->getParam("thread_id", false);
			$forum_id = Yii::app()->request->getParam("forum_id", false);
			$thread = LearningForumthread::model()->findByPk($thread_id);
			if ($thread_id && $thread)
			{

				if (isset($_POST['LearningForumthread']))
				{
					$thread->setAttributes($_POST['LearningForumthread']);
					if ($thread->validate())
					{
						$thread->save(false);
						$result->setStatus(true)->toJSON();
					}
					else
						$result->setStatus(false)->setMessage(CHtml::errorSummary($thread, '', ''))->toJSON();
				}
				else
				{
					$html = $this->renderPartial('_move', array(
						'model' => $thread,
						'forum_id' => $forum_id,
					), true, true);
					$result->setStatus(true)->setHtml($html)->toJSON();
				}
			}
			else
				$result->setStatus(false)->setMessage(Yii::t('forum', 'Thread not found!'))->toJSON();
		}
		else
			$result->setStatus(false)->setMessage(Yii::t('course', '_NOENTER'))->toJSON();
	}

	public function actionAxDelete()
	{
		$result = new AjaxResult();
		if ($this->_canModerateThread())
		{
			$thread_id = Yii::app()->request->getParam("thread_id", false);
			$thread = LearningForumthread::model()->findByPk($thread_id);
			if ($thread)
			{
				$thread->delete();

				$event = new DEvent($this, array('thread' => $thread));
				Yii::app()->event->raise('onForumThreadDelete', $event);

				$result->setStatus(true)->setMessage(Yii::t('standard', '_OPERATION_SUCCESSFUL'))->toJSON();
			}
			else
				$result->setStatus(false)->setMessage(Yii::t('forum', 'Thread not found!'))->toJSON();
		}
		else
			$result->setStatus(false)->setMessage(Yii::t('course', '_NOENTER'))->toJSON();
	}

	/**
	 * Lock/Unlock thread comments
	 */
	public function actionAxToggleLock()
	{
		$result = new AjaxResult();
		if ($this->_canModerateThread())
		{
			$thread_id = Yii::app()->request->getParam("thread_id", false);
			$thread = LearningForumthread::model()->findByPk($thread_id);
			if ($thread)
			{
				$thread->locked = $thread->locked ? 0 : 1;
				$thread->save(false);
				if ($thread->locked)
					$result->setStatus(true)->setMessage(Yii::t('standard', '_OPERATION_SUCCESSFUL'))->toJSON();
				else
					$result->setStatus(true)->setMessage(Yii::t('standard', '_OPERATION_SUCCESSFUL'))->toJSON();
			}
			else
				$result->setStatus(false)->setMessage(Yii::t('forum', 'Thread not found!'))->toJSON();
		}
		else
			$result->setStatus(false)->setMessage(Yii::t('course', '_NOENTER'))->toJSON();
	}

	public function actionAxToggleImportant()
	{
		$result = new AjaxResult();
		if ($this->_canModerateThread())
		{
			$thread_id = Yii::app()->request->getParam("thread_id", false);
			$thread = LearningForumthread::model()->findByPk($thread_id);
			if ($thread)
			{
				$thread->important = $thread->important ? 0 : 1;
				$thread->save(false);
				if ($thread->important)
					$result->setStatus(true)->setMessage(Yii::t('standard', '_OPERATION_SUCCESSFUL'))->toJSON();
				else
					$result->setStatus(true)->setMessage(Yii::t('standard', '_OPERATION_SUCCESSFUL'))->toJSON();
			}
			else
				$result->setStatus(false)->setMessage(Yii::t('forum', 'Thread not found!'))->toJSON();
		}
		else
			$result->setStatus(false)->setMessage(Yii::t('course', '_NOENTER'))->toJSON();
	}

	public function actionAxToggleAccess()
	{
		$result = new AjaxResult();
		if ($this->_canModerateThread())
		{
			$thread_id = Yii::app()->request->getParam("thread_id", false);
			$thread = LearningForumthread::model()->findByPk($thread_id);
			if ($thread)
			{
				$thread->disabled = $thread->disabled ? 0 : 1;
				$thread->save(false);
				if ($thread->disabled)
					$result->setStatus(true)->setMessage(Yii::t('standard', '_OPERATION_SUCCESSFUL'))->toJSON();
				else
					$result->setStatus(true)->setMessage(Yii::t('standard', '_OPERATION_SUCCESSFUL'))->toJSON();
			}
			else
				$result->setStatus(false)->setMessage(Yii::t('forum', 'Thread not found!'))->toJSON();
		}
		else
			$result->setStatus(false)->setMessage(Yii::t('course', '_NOENTER'))->toJSON();
	}

}