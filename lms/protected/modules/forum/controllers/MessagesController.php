<?php

class MessagesController extends PermissionController
{
	/**
	 * (non-PHPdoc)
	 * @see PermissionController::init()
	 */
	public function init()
	{
		parent::init();
	}

	/**
	 * (non-PHPdoc)
	 * @see ForumBaseController::beforeAction()
	 */
	public function beforeAction($action)
	{
		return parent::beforeAction($action);
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules()
	{
		return array(
			// God Admin users can do all
			array('allow',
				'users' => array('@'),
				'expression' => 'Yii::app()->user->isGodAdmin',
			),

			// Logged in users CAN go. Complex check is done already in ForumBaseController::init()
			array('allow',
				'users' => array('@'),
			),

			// Deny by default
			array('deny', // if some permission is wrong -> the script goes here
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
				'message' => Yii::t('course', '_NOENTER'),
			),
		);
	}

	/**
	 * List messages in a given forum discussion.
	 *
	 */
	public function actionIndex()
	{
		$thread = LearningForumthread::model()->with('forum')->findByPk($this->thread_id);
		$forum = LearningForum::model()->findByPk($thread->idForum);
		if ($thread)
			LearningForumTiming::trackAccess($thread->idThread);

		if ($thread->disabled && !$this->_canModerateThread())
		{
			Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
			$this->redirect(array('/site'),true);
		}

		$message = new LearningForummessage('search');
        $message->order_by = 'asc'; // Set Order by ASCENDING for default value

		$message->unsetAttributes();
		if(isset($_GET['LearningForummessage']))
			$message->attributes = $_GET['LearningForummessage'];
		$message->idThread = $thread->idThread;

		// Increase the number of views of the thread
		$thread->num_view++;
		$thread->save();

        // get message rating settings
        $forumBlock = PlayerBaseblock::model()->findByAttributes(array(
            'course_id' => $thread->forum->idCourse,
            'content_type' => PlayerBaseblock::TYPE_FORUM
        ));
        $forumParams = ($forumBlock) ? CJSON::decode( $forumBlock->params ) : array();

        Yii::app()->session['rating_settings_'.$forum->idCourse] = array(
            'show_set_tone' => intval($forumParams['show_set_tone']),
            'set_tone_icons' => $forumParams['set_tone_icons'],
            'show_helpful' => intval($forumParams['show_helpful'])
        );

		$this->render('index', array(
			'forum' => $forum,
			'thread' => $thread,
			'message' => $message,
            'forumParams' => $forumParams
		));
	}

	/**
	 * Leave reply OR edit a reply
	 */
	public function actionAxReply()
	{
		$result = new AjaxResult();
		if ($this->_canReplyMsg())
		{
			$thread_id = Yii::app()->request->getParam("thread_id", false);
			$message_id = Yii::app()->request->getParam("message_id", false);
			$message = new LearningForummessage('reply');
			if ($message_id)
				$inReplyOf = LearningForummessage::model()->findByPk($message_id);

			if (isset($_POST['LearningForummessage']))
			{
				//check block settings
				$thread = LearningForumthread::model()->with('forum')->findByPk($thread_id);
				$forumBlock = PlayerBaseblock::model()->findByAttributes(array('course_id' => $thread->forum->idCourse, 'content_type' => PlayerBaseblock::TYPE_FORUM));
				$forumParams = ($forumBlock ? CJSON::decode( $forumBlock->params ) : array());
				if (!empty($forumParams) && isset($forumParams['word_count']) && (int)$forumParams['word_count'] > 0) {
					$countWords = ForumHelper::countWords($_POST['LearningForummessage']['textof']);
					if ($countWords > (int)$forumParams['word_count']) {
						//for some reason, we have more words than allowed: return an error to the client
						$result->setStatus(false)->setMessage('Words limit exceeded (counted words: '.$countWords.', max allowed: '.(int)$forumParams['word_count'].')')->toJSON();
						Yii::app()->end();
					}
				}
				//save the message text in DB
				$message->setAttributes($_POST['LearningForummessage']);
				if ($message->validate())
				{
					$message->idThread = $thread_id;
					$message->author = Yii::app()->user->id;
					$message->posted = Yii::app()->localtime->toLocalDateTime();
					$message->save(false);

					// So message is saved, now we need to check if there are files added to the message
					if(!empty($_POST['LearningForummessage']['file']['name']) && strlen($_POST['LearningForummessage']['file']['name']) > 0) {
						$message->original_filename = $_POST['LearningForummessage']['file']['name'];

						$fileHelper = new CFileHelper();
						$extension = strtolower($fileHelper->getExtension($message->original_filename));
						$message->path = $_POST['LearningForummessage']['file']['path'] . '.' . $extension;

						$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_FILE);
						$localFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $message->path;

						if ($storage->store($localFilePath)) {
							// Delete the temp file
							unlink(Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $message->path);

							$message->save(false);
						}

					}

					$event = new DEvent($this, array('message' => $message, 'thread_id' => $thread_id));
					Yii::app()->event->raise('onForumMessageCreate', $event);

					$result->setStatus(true)->toJSON();
				}
				else
					$result->setStatus(false)->setMessage(CHtml::errorSummary($message, '', ''))->toJSON();
			}
			else
			{
				if ($inReplyOf)
					$message->textof = '[QUOTE='.$inReplyOf->msgUser->fullName.';]'.$inReplyOf->textof.'[/QUOTE]';

				$forumBlock = PlayerBaseblock::model()->findByAttributes(array('course_id' => Yii::app()->player->course->idCourse, 'content_type' => PlayerBaseblock::TYPE_FORUM));
				$forumParams = ($forumBlock ? CJSON::decode( $forumBlock->params ) : array());

				$html = $this->renderPartial('_reply', array(
					'model'         => $message,
					'thread_id'     => $thread_id,
					'forumParams'   => $forumParams
				), true, true);
				$result->setStatus(true)->setHtml($html)->toJSON();
			}
		}
		else
			$result->setStatus(false)->setMessage('Operation not permitted!')->toJSON();
	}

	public function actionAxUpdate()
	{
		$result = new AjaxResult();
		$thread_id = Yii::app()->request->getParam("thread_id", false);
		$message_id = Yii::app()->request->getParam("message_id", false);
		$message = LearningForummessage::model()->findByPk($message_id);
		if ($message)
		{
			if ($this->_canUpdateMsg($message))
			{
				$message->scenario = 'reply';
				if (isset($_POST['LearningForummessage']))
				{
					$message->setAttributes($_POST['LearningForummessage']);
					if ($message->validate())
					{
						$message->modified_by = Yii::app()->user->id;
						$message->modified_by_on = Yii::app()->localtime->toLocalDateTime();


						$oldPath = $message->path;
						$oldOriginalFilename = $message->original_filename;

						if(empty($_POST['LearningForummessage']['file']['name']) || strlen($_POST['LearningForummessage']['file']['name']) > 0) {
							$message->path = '';
							$message->original_filename = '';

							// Remove old file if there is already an uploaded file
							if (!empty($oldPath)) {
								// Try to remove old file
								$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_FILE);
								if ($storage->fileExists($oldPath)) {
									$storage->remove($oldPath);
								}
							}
						}

						$message->save(false);

						// So message is saved, now we need to check if there are files added to the message
						if(!empty($_POST['LearningForummessage']['file']['name']) && strlen($_POST['LearningForummessage']['file']['name']) > 0) {

							$message->original_filename = $_POST['LearningForummessage']['file']['name'];
							$fileHelper = new CFileHelper();
							$extension = strtolower($fileHelper->getExtension($message->original_filename));
							$message->path = $_POST['LearningForummessage']['file']['path']  . '.' . $extension;

							$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_FILE);
							$localFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $message->path;

							if ($storage->store($localFilePath)) {
								// Delete the temp file
								unlink($localFilePath);

								$message->save(false);
							}

						}

						$event = new DEvent($this, array('message' => $message, 'thread_id' => $thread_id));
						Yii::app()->event->raise('onForumMessageUpdate', $event);

						$result->setStatus(true)->toJSON();
					}
					else
						$result->setStatus(false)->setMessage(CHtml::errorSummary($message, '', ''))->toJSON();
				}
				else
				{
					$forumBlock = PlayerBaseblock::model()->findByAttributes(array('course_id' => Yii::app()->player->course->idCourse, 'content_type' => PlayerBaseblock::TYPE_FORUM));
					$forumParams = ($forumBlock ? CJSON::decode( $forumBlock->params ) : array());

					$html = $this->renderPartial('_reply', array(
						'model'         => $message,
						'thread_id'     => $thread_id,
						'forumParams'   => $forumParams
					), true, true);
					$result->setStatus(true)->setHtml($html)->toJSON();
				}
			}
			else
				$result->setStatus(false)->setMessage('Operation not permitted!')->toJSON();
		}
		else
			$result->setStatus(false)->setMessage('Message not found!')->toJSON();
	}

	public function actionAxDelete()
	{
		$result = new AjaxResult();
		if ($this->_canModerateMsg())
		{
			$message_id = Yii::app()->request->getParam("message_id", false);
			$message = LearningForummessage::model()->findByPk($message_id);
			if ($message)
			{
				// Get the real filename if set in the message
				$filePath = $message->path;
				$originalFile = $message->original_filename;

				// Remove file if there is already an uploaded file
				if (!empty($filePath)) {
					// Try to remove the file
					$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_FILE);
					if ($storage->fileExists($filePath)) {
						$storage->remove($filePath);
					}
				}


				$message->delete();

				$event = new DEvent($this, array('message' => $message));
				Yii::app()->event->raise('onForumMessageDelete', $event);

				$result->setStatus(true)->setMessage(Yii::t('standard', '_OPERATION_SUCCESSFUL'))->toJSON();
			}
			else
				$result->setStatus(false)->setMessage(Yii::t('standard', '_OPERATION_FAILURE'))->toJSON();
		}
		else
			$result->setStatus(false)->setMessage(Yii::t('course', '_NOENTER'))->toJSON();
	}

	public function actionAxToggleHide()
	{
		$result = new AjaxResult();
		if ($this->_canModerateMsg())
		{
			$message_id = Yii::app()->request->getParam("message_id", false);
			$message = LearningForummessage::model()->findByPk($message_id);
			if ($message)
			{
				$message->locked = $message->locked ? 0 : 1;
				$message->save(false);
				if ($message->locked == 1)
					$result->setStatus(true)->setMessage(Yii::t('standard', '_OPERATION_SUCCESSFUL'))->toJSON();
				else
					$result->setStatus(true)->setMessage(Yii::t('standard', '_OPERATION_SUCCESSFUL'))->toJSON();
			}
			else
				$result->setStatus(false)->setMessage(Yii::t('standard', '_OPERATION_FAILURE'))->toJSON();
		}
		else
			$result->setStatus(false)->setMessage(Yii::t('course', '_NOENTER'))->toJSON();
	}

	/**
	 * Register module specific assets
	 */
	public function registerResources()
	{
		parent::registerResources();

		if (!Yii::app()->request->isAjaxRequest) {
			$cs = Yii::app()->getClientScript();
			$assetsUrl = $this->module->assetsUrl;

			// Register JS script(s)
			$options = array();
			$options = CJavaScript::encode($options);
			$script = "";
			$cs->registerScript("forumMessagesControllerJs1", $script, CClientScript::POS_BEGIN);

		}
	}

    /**
     * Leave sub-reply OR edit a reply
     */
    public function actionAxSubReply()
    {
        $result = new AjaxResult();
        if ($this->_canReplyMsg())
        {
            $thread_id = Yii::app()->request->getParam("thread_id", false);
            $message_id = Yii::app()->request->getParam("message_id", false);
            $parentIdMessage = Yii::app()->request->getParam("parentIdMessage", false);
            $quote = Yii::app()->request->getParam("quote", null);
            $message = new LearningForummessage('reply');
            $parentIdMessage = $parentIdMessage ? $parentIdMessage : $message_id;

            if (isset($_POST['LearningForummessage']))
            {
                //check block settings
                $thread = LearningForumthread::model()->with('forum')->findByPk($thread_id);
                $forumBlock = PlayerBaseblock::model()->findByAttributes(array('course_id' => $thread->forum->idCourse, 'content_type' => PlayerBaseblock::TYPE_FORUM));
                $forumParams = ($forumBlock ? CJSON::decode( $forumBlock->params ) : array());
                if (!empty($forumParams) && isset($forumParams['word_count']) && (int)$forumParams['word_count'] > 0) {
                    $countWords = ForumHelper::countWords($_POST['LearningForummessage']['textof']);
                    if ($countWords > (int)$forumParams['word_count']) {
                        //for some reason, we have more words than allowed: return an error to the client
                        $result->setStatus(false)->setMessage('Words limit exceeded (counted words: '.$countWords.', max allowed: '.(int)$forumParams['word_count'].')')->toJSON();
                        Yii::app()->end();
                    }
                }
                //save the message text in DB
                $message->setAttributes($_POST['LearningForummessage']);
                if ($message->validate())
                {
                    $message->idThread = $thread_id;
                    $message->author = Yii::app()->user->id;
                    $message->posted = Yii::app()->localtime->toLocalDateTime();
                    $message->idReply = $parentIdMessage;
                    $message->save(false);

	                // So message is saved, now we need to check if there are files added to the message
	                if(!empty($_POST['LearningForummessage']['file']['name']) && strlen($_POST['LearningForummessage']['file']['name']) > 0) {
		                $message->original_filename = $_POST['LearningForummessage']['file']['name'];

		                $fileHelper = new CFileHelper();
		                $extension = strtolower($fileHelper->getExtension($message->original_filename));

		                $message->path = $_POST['LearningForummessage']['file']['path'] . '.' . $extension;

		                $storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_FILE);
		                $localFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $message->path;

		                if ($storage->store($localFilePath)) {
			                // Delete the temp file
			                unlink($localFilePath);
			                $message->save(false);
		                }

	                }

                    $event = new DEvent($this, array('message' => $message, 'thread_id' => $thread_id));
                    Yii::app()->event->raise('onForumMessageCreate', $event);

                    $result->setStatus(true)->toJSON();
                }
                else
                    $result->setStatus(false)->setMessage(CHtml::errorSummary($message, '', ''))->toJSON();
            }
            else
            {
                if ($quote){
                    $id = $message_id ? $message_id : $parentIdMessage;
                    $inReplyOf = LearningForummessage::model()->findByPk($id);
                    $message->textof = '[QUOTE='.$inReplyOf->msgUser->fullName.';]'.$inReplyOf->textof.'[/QUOTE]';
                }

	            $forumBlock = PlayerBaseblock::model()->findByAttributes(array('course_id' => Yii::app()->player->course->idCourse, 'content_type' => PlayerBaseblock::TYPE_FORUM));
	            $forumParams = ($forumBlock ? CJSON::decode( $forumBlock->params ) : array());

                $html = $this->renderPartial('_sub_reply', array(
                    'model'             => $message,
                    'thread_id'         => $thread_id,
                    'parentIdMessage'   => $parentIdMessage,
	                'forumParams'       => $forumParams
                ), true, true);
                $result->setStatus(true)->setHtml($html)->toJSON();
            }
        }
        else
            $result->setStatus(false)->setMessage('Operation not permitted!')->toJSON();
    }

    public function actionAxSubUpdate()
    {
        $result = new AjaxResult();
        $thread_id = Yii::app()->request->getParam("thread_id", false);
        $message_id = Yii::app()->request->getParam("message_id", false);
        $parentIdMessage = Yii::app()->request->getParam("parentIdMessage", false);
        $message = LearningForummessage::model()->findByPk($message_id);
        if ($message)
        {
            if ($this->_canUpdateMsg($message))
            {
                $message->scenario = 'reply';
                if (isset($_POST['LearningForummessage']))
                {
                    $message->setAttributes($_POST['LearningForummessage']);
                    if ($message->validate())
                    {
                        $message->modified_by = Yii::app()->user->id;
                        $message->modified_by_on = Yii::app()->localtime->toLocalDateTime();

	                    $oldPath = $message->path;
	                    $oldOriginalFilename = $message->original_filename;

	                    if(empty($_POST['LearningForummessage']['file']['name']) || strlen($_POST['LearningForummessage']['file']['name']) > 0) {
		                    $message->path = '';
		                    $message->original_filename = '';

		                    // Remove old file if there is already an uploaded file
		                    if (!empty($oldPath)) {
			                    // Try to remove old file
			                    $storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_FILE);
			                    if ($storage->fileExists($oldPath)) {
				                    $storage->remove($oldPath);
			                    }
		                    }
	                    }

                        $message->save(false);

	                    // So message is saved, now we need to check if there are files added to the message
	                    if(!empty($_POST['LearningForummessage']['file']['name']) && strlen($_POST['LearningForummessage']['file']['name']) > 0) {
		                    $message->original_filename = $_POST['LearningForummessage']['file']['name'];

		                    $fileHelper = new CFileHelper();
		                    $extension = strtolower($fileHelper->getExtension($message->original_filename));

		                    $message->path = $_POST['LearningForummessage']['file']['path'] . '.' . $extension;

		                    $storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_FILE);
		                    $localFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $message->path;

		                    if ($storage->store($localFilePath)) {
			                    // Delete the temp file
			                    unlink($localFilePath);
			                    $message->save(false);
		                    }

	                    }

                        $event = new DEvent($this, array('message' => $message, 'thread_id' => $thread_id));
                        Yii::app()->event->raise('onForumMessageUpdate', $event);

                        $result->setStatus(true)->toJSON();
                    }
                    else
                        $result->setStatus(false)->setMessage(CHtml::errorSummary($message, '', ''))->toJSON();
                }
                else
                {
	                $forumBlock = PlayerBaseblock::model()->findByAttributes(array('course_id' => Yii::app()->player->course->idCourse, 'content_type' => PlayerBaseblock::TYPE_FORUM));
	                $forumParams = ($forumBlock ? CJSON::decode( $forumBlock->params ) : array());

                    $html = $this->renderPartial('_sub_reply', array(
                        'model'             => $message,
                        'thread_id'         => $thread_id,
                        'parentIdMessage'   => $parentIdMessage,
	                    'forumParams'       => $forumParams
                    ), true, true);
                    $result->setStatus(true)->setHtml($html)->toJSON();
                }
            }
            else
                $result->setStatus(false)->setMessage('Operation not permitted!')->toJSON();
        }
        else
            $result->setStatus(false)->setMessage('Message not found!')->toJSON();
    }

    public function actionAxLoadSubReplies(){


        $thread = LearningForumthread::model()->with('forum')->findByPk($this->thread_id);
        $forumBlock = PlayerBaseblock::model()->findByAttributes(array(
            'course_id' => $thread->forum->idCourse,
            'content_type' => PlayerBaseblock::TYPE_FORUM
        ));
        $forumParams = ($forumBlock) ? CJSON::decode( $forumBlock->params ) : array();

        $id = Yii::app()->request->getParam('message_id', null);
        $parentCss = Yii::app()->request->getParam('parentCss', null);
        $currentPage = Yii::app()->request->getParam('page', 0);
        $criteria = new CDbCriteria(array(
            'condition' => 'idReply = :id',
            'order' => 'posted ASC',
            'limit' => 3,
            'params' => array(':id' => $id),
            'offset' => 3 * $currentPage
        ));
        $comments = LearningForummessage::model()->findAll($criteria);
        if(!empty($comments)){
            $html = $this->renderPartial('_sub_replies', array(
                'comments' => $comments,
                'parentCss'     => $parentCss,
                'forumParams'    => $forumParams
            ), true, true);
			$commentCount = LearningForummessage::model()->countByAttributes(array("idReply" => $id));
            echo json_encode(array('hasComments' => true, 'html' => $html, 'commentCount' => $commentCount));

        }else{
            echo json_encode(array('hasComments' => false));
        }
    }


	public function actionHandleFile() {
		if (isset($_FILES['file'])) {
			$fileDir = Docebo::getUploadTmpPath();
			$fileId = basename(Yii::app()->htmlpurifier->purify(Yii::app()->request->getParam('fileId', $_FILES['file']['name'])));

			$original_filename = $_FILES['file']['name'];

			// Run some extension checks on the file
			$fileHelper = new CFileHelper();
			$extension = strtolower($fileHelper->getExtension($original_filename ));

			// File extension check
			if (!in_array($extension, FileTypeValidator::getWhiteListArray(FileTypeValidator::TYPE_FORUM_UPLOADS))) {
				echo '{"jsonrpc" : "2.0", "error" : {"code": 500, "message": "'.Yii::t('fileValidator', 'File type not allowed').'"}, "id" : "id"}';
				Yii::app()->end();
			}

			if (file_exists($fileDir) && is_writable($fileDir) && is_dir($fileDir)) {
				$file = $_FILES['file'];

				// Mime type file check
				$mimeTypes = FileTypeValidator::getMimeWhiteListArray(FileTypeValidator::TYPE_FORUM_UPLOADS);
				if (!empty($mimeTypes)) {
					$fileMimeType = $fileHelper->getMimeType($file['tmp_name']);
					if(!$fileMimeType || !in_array($fileMimeType, $mimeTypes)) {
						@unlink($file['tmp_name']);
						echo '{"jsonrpc" : "2.0", "error" : {"code": 500, "message": "Detected file type "' . $fileMimeType . '" not allowed"}, "id" : "id"}';
						Yii::app()->end();
					}
				}

				file_put_contents( $fileDir . DIRECTORY_SEPARATOR . $fileId . '.' . $extension, file_get_contents($file['tmp_name']),FILE_APPEND );
			}

			$_SESSION['most_recent_activity'] = time();

			// if this is last portion, rename the file to the original name
			if ($_FILES['file']['size'] < ((LearningForummessage::model()->file_maxsize) * 1024 * 1024)) {
				rename($fileDir . DIRECTORY_SEPARATOR . $fileId . '.' . $extension, $fileDir . DIRECTORY_SEPARATOR . $fileId . '.' . $extension);
				unset($_SESSION['most_recent_activity']);
				unset($_SESSION['tempFileName']);
			}

			echo '{"jsonrpc" : "2.0", "error" : {"code": 200, "message": ""}, "id" : "id","status": 200, "success": true, "result" : ""}';
			Yii::app()->end();
		}
	}

    /**
     *  This action is called by Ajax from messages in forum
     */
    public function actionRemoveFile( ) {
            $fileId = Yii::app()->request->getParam('fileId');
            $idMessage = Yii::app()->request->getParam('idMessage');

        if ($_REQUEST['confirm']) {
            try {
                if ($fileId) {
                    // Remove the file in S3
                    $storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_FILE);
                    if ($storage->fileExists( $fileId )) {
                        $storage->remove( $fileId );
                    }
                    if ( $idMessage ) {
                        // Remove the file from DB
                        $learningForumMessage = LearningForummessage::model()->findByPk($idMessage);
                        $learningForumMessage->path = "";
                        $learningForumMessage->original_filename = "";
                        $learningForumMessage->save();
                    }
                }
                Yii::app()->end();
            } catch ( Exception $e) {
                $e->getMessage();
            }
        } else {
            $this->renderPartial('delete_file', array(
                'model'     => new LearningForummessage(),
                'fileId'    => $fileId,
                'idMessage' => $idMessage
            ));
        }
    }

    public function actionDownloadFile() {
        try {
            $fileId = Yii::app()->request->getParam('fileId');
            $fileName = Yii::app()->request->getParam('fileName');

            if ( $fileId ) {
                // Download File from S3
                $storage = CS3Storage::getDomainStorage(CFileStorage::COLLECTION_LO_FILE);
                if ($storage->fileExists( $fileId )) {
                    $storage->sendFile($fileId, $fileName);
                }
            }
        } catch ( Exception $e) {
            $e->getMessage();
        }
    }
}