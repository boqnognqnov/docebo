<?php

class DefaultController extends PermissionController {


	/**
	 * (non-PHPdoc)
	 * @see PermissionController::init()
	 */
	public function init() {
		parent::init();
	}


	/**
	 * (non-PHPdoc)
	 * @see ForumBaseController::beforeAction()
	 */
	public function beforeAction($action) {
		return parent::beforeAction($action);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
				'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {

		return array(

				// God Admin users can do all
				array('allow',
					'users'=>array('@'),
					'expression' => 'Yii::app()->user->isGodAdmin',
				),

				// Logged in users
				array('allow',
					'actions' => array('index', 'messages', 'axAddCommentFromBlock', 'axAddDiscussionFromBlock', 'axDeleteMessageFromBlock', 'axVoteTone', 'axVoteHelpful', 'axDeleteComment'),
					'users'=>array('@'),
				),

				// Logged in users, Course Admins
				array('allow',
						'actions' => array('courseForums', 'axUpdateForum', 'axRemoveForum', 'axManageThread', 'axEditForumPopup', 'axRatingSettings'),
						'users'=>array('@'),
						'expression' => 'Yii::app()->controller->userCanAdminCourse',
				),


				// Deny by default
				array('deny', // if some permission is wrong -> the script goes here
						'users' => array('*'),
						'deniedCallback' => array($this, 'accessDeniedCallback'),
						'message' => Yii::t('course', '_NOENTER'),
				),

		);
	}


	/**
	 * List threads of a given forum
	 *
	 * @throws CException
	 */
	public function actionIndex() {

		// If Thread ID is passed, forward directly to messages of the thread
		if ($this->thread_id) {
			$this->forward('messages', true);
		}

		// Get Forum model
		$forum = LearningForum::model()->findByPk($this->forum_id);
		$thread = new LearningForumthread('search');

		if ($forum) {
			$thread->unsetAttributes();
			if(isset($_GET['LearningForumthread']))
				$thread->attributes = $_GET['LearningForumthread'];
			$thread->idForum = $forum->idForum;

			$this->render('index', array(
				'forum' => $forum,
				'thread' => $thread,
			));
		}
		// Forward to other actions if NO forum model
		else {
			if ($this->course_id) {
				$this->forward('courseForums', true);
			}
			else  {
				Yii::app()->user->setFlash('error', Yii::t('course', '_NOENTER'));
				$this->redirect(array('/site'),true);
			}
		}
	}


	/**
	 * List all *course* forums
	 *
	 */
	public function actionCourseForums() {
		$courseModel = LearningCourse::model()->findByPk($this->course_id);
		$this->render('course_forums', array(
			'course_id' => $this->course_id,
			'courseModel' => $courseModel,
		));
	}


	/**
	 * List ALL forums. This is a godAdmin action!
	 *
	 * !NOTE: Not used yet (22 Jul 2013).
	 *
	 */
	public function actionAllForums() {
		$this->render('all_forums', array(
		));
	}


	/**
	 * Called from Comments Block environment to Add a comment (a Reply!) to a thread (which already exists!)
	 *
	 * @throws CException
	 */
	public function actionAxAddCommentFromBlock() {

		$response = new AjaxResult();

		try {


			$threadId = $this->thread_id;
			$commentText = Yii::app()->request->getParam("comment_text", false);
			$userId = Yii::app()->request->getParam("user_id", false);
			$courseId = Yii::app()->request->getParam("course_id", false);
			$title = Yii::app()->request->getParam("title", "-");

			$threadModel = LearningForumthread::model()->findByPk($threadId);
			if (!$threadModel) {
				throw new CException(Yii::t('forum', 'Invalid discussion Id'));
			}

			if ($threadModel->locked) {
				throw new CException(Yii::t('forum', '_LOCKEDMESS'));
			}

			$messageModel = new LearningForummessage();
			$messageModel->idThread = $threadId;
			$messageModel->idCourse = $courseId;
			$messageModel->posted = Yii::app()->localtime->toLocalDateTime();
			$messageModel->answer_tree = "/" . $messageModel->posted;
			$messageModel->title = $title;
			$messageModel->textof = $commentText;
			$messageModel->author = $userId;
			$messageModel->generator = 0;


			// Save message
			if (!$messageModel->save()) {
				throw new CException(implode(',', $messageModel->getErrors()));
			}

		}

		catch (CException $e) {
			$response->setStatus(false)->setMessage(Yii::t('forum', 'The Comments Forum has been deleted.'))->toJSON();
			Yii::app()->end();
		}

		$html = $this->renderPartial("player.views.block.comments._one_comment", array(
			"message" => $messageModel
		), true);

		$data = array(
				"html" => $html,
				"thread_id" => $threadId,
				"commentsCount" => $threadModel->getReplyNumber(),
				'commentsLimit' => Yii::app()->params["player"]["comments_block_comments"],
		);
		$response->setStatus(true)->setData($data)->toJSON();
		Yii::app()->end();


	}



	/**
	 * Called from Comments Block environment to Add a NEW discussion (topic)
	 *
	 * @throws CException
	 */
	public function actionAxAddDiscussionFromBlock() {

		$response = new AjaxResult();

		try {

			$forumId = $this->forum_id;
			$blockId = Yii::app()->request->getParam("block_id", false);
			$messageText = Yii::app()->request->getParam("opening_message_text", false);
			$userId = Yii::app()->request->getParam("user_id", false);
			$courseId = Yii::app()->request->getParam("course_id", false);
			$title = Yii::app()->request->getParam("title", false);

			if (empty($title)) {
				$title = strip_tags($messageText);
				$title = substr($title, 0, 72);
			}

			$blockModel = PlayerBaseblock::model()->findByPk($blockId);

			// Create a discussion
			$threadModel = new LearningForumthread();
			$threadModel->idForum = $forumId;
			$threadModel->posted = Yii::app()->localtime->toLocalDateTime();
			$threadModel->title = $title;
			$threadModel->author = $userId;

			// Save discussion
			if (!$threadModel->save()) {
				throw new CException(implode(',', $threadModel->getErrors()));
			}

			// Add opening message
			$messageModel = new LearningForummessage();
			$messageModel->idThread = $threadModel->idThread;
			$messageModel->idCourse = $courseId;
			$messageModel->posted = Yii::app()->localtime->toLocalDateTime();
			$messageModel->answer_tree = "/" . $messageModel->posted;
			$messageModel->title = $title;
			$messageModel->textof = $messageText;
			$messageModel->author = $userId;
			$messageModel->generator = 1;

			// Save message
			if (!$messageModel->save()) {
				throw new CException(implode(',', $messageModel->getErrors()));
			}


		}

		catch (CException $e) {
			$response->setStatus(false)->setMessage(Yii::t('forum', 'The Comments Forum has been deleted.'))->toJSON();
			Yii::app()->end();
		}


		$posterUserModel = CoreUser::model()->findByPk($userId);
		$userName = $posterUserModel->firstname . ' ' . $posterUserModel->lastname;
		$messages = $threadModel->messages;

		$html = Yii::app()->controller->renderPartial('player.views.block.comments._one_discussion', array(
			'messages' => $messages,
			'commentsOffset' => 0,
			'commentsLimit' => Yii::app()->params["player"]["comments_block_comments"],
			'thread' => $threadModel,
			'blockModel' => $blockModel,
			'currentUserName' => $userName,
			'showViewAll' => 0,
			'showAllComments' => 1,
		), true);


		$data = array(
				"html" => $html,
		);

		$response->setStatus(true)->setData($data)->toJSON();
		Yii::app()->end();


	}

	public function actionAxDeleteComment(){
		$response = new AjaxResult();

		try{
			$threadId = $this->thread_id;
			if (!$threadId) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			$threadModel = LearningForumthread::model()->findByPk($threadId);

			if (Yii::app()->user->isGodadmin) {
				$threadModel->delete();
			} else {
				throw new CException(Yii::t("standard",  "_OPERATION_FAILURE"));
			}

			$response->setStatus(true)->toJSON();
			Yii::app()->end();

		}catch (CException $e){
			$response->setStatus(false)->setMessage(Yii::t('forum', 'The Comments Forum has been deleted.'))->toJSON();
			Yii::app()->end();
		}
	}


	/**
	 * Called from Comments Block environment to delete a message
	 *
	 * @throws CException
	 */
	public function actionAxDeleteMessageFromBlock() {
		$response = new AjaxResult();

		try {
			$messageId = $this->message_id;
			if (!$messageId) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			$messageModel = LearningForummessage::model()->findByPk($messageId);
			$threadId = $messageModel->idThread;

			// Only god admins and message owner can delete it
			if (Yii::app()->user->isGodadmin || $messageModel->author == Yii::app()->user->id) {
				$messageModel->delete();
			}
			else {
				throw new CException(Yii::t("standard",  "_OPERATION_FAILURE"));
			}

			$threadModel = LearningForumthread::model()->findByPk($threadId);

			$data = array(
				"commentsCount" => $threadModel->getReplyNumber(),
				'thread_id' => $threadId,
				'commentsLimit' => Yii::app()->params["player"]["comments_block_comments"],
			);
			$response->setStatus(true)->setData($data)->toJSON();
			Yii::app()->end();

		}
		catch (CException $e) {
			$response->setStatus(false)->setMessage(Yii::t('forum', 'The Comments Forum has been deleted.'))->toJSON();
			Yii::app()->end();
		}

	}



	/**
	 * Register module specific assets
	 */
	public function registerResources() {

		parent::registerResources();

		if (!Yii::app()->request->isAjaxRequest) {
			$cs = Yii::app()->getClientScript();
			$assetsUrl = $this->module->assetsUrl;

			// Register JS script(s)
			$options = array();
			$options = CJavaScript::encode($options);
			$script = "";
			$cs->registerScript("forumDefaultControllerJs1", $script , CClientScript::POS_BEGIN);

		}

	}


	/**
	 * Render URL to list of threads in a forum
	 * @param unknown $row
	 * @param unknown $index
	 * @return string
	 */
	protected function _renderForumLink($row, $index) {
		return CHtml::link($row->title, $this->createUrl("//forum/thread/index", array("forum_id"=>$row->idForum)));
	}


	/**
	 *
	 * @param unknown $row
	 * @param unknown $index
	 * @return string
	 */
	protected function _renderCourseForumsOperationsColumn($row, $index) {
		$html = '';
		$editUrl 	= $this->createUrl("//forum/default/axUpdateForum", array("forum_id" => $row->idForum));
		$removeUrl 	= $this->createUrl("//forum/default/axRemoveForum", array("forum_id" => $row->idForum));
		//$lockUrl 	= $this->createUrl("//forum/default/axLockForum", 	array("forum_id" => $row->idForum));


		// EDIT
		$html .= CHtml::link('<span class="i-sprite is-edit"></span>', $editUrl, array(
				'data-forum-id' => $row->idForum,
				'class' => 'edit-forum-operation',
			)
		);
		$html .= "&nbsp;&nbsp;";

		// LOCK
// 		$html .= CHtml::link('<span class="i-sprite is-lock"></span>', $lockUrl, array(
// 				'data-forum-id' => $row->idForum,
// 				'class' => 'lock-forum-operation',
// 			)
// 		);
// 		$html .= "&nbsp;&nbsp;";

		// REMOVE
		$html .= CHtml::link('<span class="i-sprite is-remove red"></span>', $removeUrl, array(
				'data-forum-id' => $row->idForum,
				'class' => 'remove-forum-operation',
			)
		);




		return $html;

	}


	protected function _renderCourseForumsLockedColumn($row, $index) {

		$html = "";

		if ($row->locked) {
			$html = '<span class="i-sprite is-lock"></span>';
		}

		return $html;
	}


	/**
	 * Render a form and send back the result. Handle request to create a new forum OR to edit exisitng one
	 */
	public function actionAxUpdateForum() {

		$result = new AjaxResult();

		$forum_id = Yii::app()->request->getParam("forum_id", false);
		$course_id = Yii::app()->request->getParam("course_id", false);


		// No forum and no course id ? What we are doing here then ?
		if (!$forum_id && !$course_id) {
			$result->setStatus(false)->setMessage(Yii::t('standard', '_OPERATION_FAILURE'))->toJSON();
			Yii::app()->end();
		}

		$model = LearningForum::model()->findByPk($forum_id);

		if (!$model) {
			$model = new LearningForum();
			$model->idCourse = $course_id;
		}

		if ($_REQUEST["LearningForum"]) {
			$model->attributes = $_REQUEST["LearningForum"];

			if ($model->save()) {
				$result->setStatus(true)->setHtml('')->toJSON();
				Yii::app()->end();
			}
			else {
				$message = CHtml::errorSummary($model, '', '');
				$result->setStatus(false)->setMessage($message)->toJSON();
				Yii::app()->end();
			}
		}


		$html = $this->renderPartial('_update_forum', array(
				'model' => $model,
		), true, true);

		$result->setStatus(true)->setHtml($html)->toJSON();

		Yii::app()->end();

	}




	/**
	 * Show Popup dialog to edit/create a forum. Called from Forum block environment
	 *
	 */
	public function actionAxEditForumPopup() {

		$forum_id = Yii::app()->request->getParam("forum_id", false);
		$course_id = Yii::app()->request->getParam("course_id", false);

		// DB Start transaction
		$db_transaction = Yii::app()->db->beginTransaction();

		try {
			// No forum and no course id ? What we are doing here then ?
			if (!$forum_id && !$course_id) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				Yii::app()->end();
			}

			$model = LearningForum::model()->findByPk($forum_id);

			if (!$model) {
				$model = new LearningForum();
				$model->idCourse = $course_id;
			}


			if ($_REQUEST["LearningForum"]) {
				$model->attributes = $_REQUEST["LearningForum"];
				if ($model->validate()) {
					$model->save(false);
					$db_transaction->commit();
					echo "<a class='auto-close success'></a>";
					Yii::app()->end();
				}
			}

			$html = $this->renderPartial('_update_forum', array(
					'model' => $model,
					'popup' => true,
			), true, true);

			echo $html;

			Yii::app()->end();



		}
		catch (CException $e) {
			$db_transaction->rollback();
			$html = $this->renderPartial('_dialog2_error', array(
					'message' => $e->getMessage(),
			), true);
			echo $html;
			Yii::app()->end();
		}

	}

    /*
     * Show dialog to edit set tone / helpful settings
     */
    public function actionAxRatingSettings() {
        $block_id = Yii::app()->request->getParam("block_id", false);
        $block = PlayerBaseblock::model()->findByPk($block_id);

        // DB Start transaction
        $db_transaction = Yii::app()->db->beginTransaction();

        try {
            // No forum and no course id ? What we are doing here then ?
            if (!$block) {
                throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
                Yii::app()->end();
            }

            $model = new ForumRatingSettingsForm();

            $forumParams = CJSON::decode($block->params);
            $model->show_set_tone = intval($forumParams['show_set_tone']);
            $model->set_tone_icons = $forumParams['set_tone_icons'];
            $model->show_helpful = intval($forumParams['show_helpful']);
			$model->word_count = intval($forumParams['word_count']);
			$model->forum_upload = (isset($forumParams['forum_upload'])) ? intval($forumParams['forum_upload']) : 0;

            if ($_REQUEST["ForumRatingSettingsForm"]) {
                $model->attributes = $_REQUEST["ForumRatingSettingsForm"];
                if ($model->validate()) {

                    $courseForumWidgets = PlayerBaseblock::model()->findAllByAttributes(array(
                        'course_id' => $block->course_id,
                        'content_type' => PlayerBaseblock::TYPE_FORUM
                    ));
                    foreach ($courseForumWidgets as $forumBlock) {
                        $params = CJSON::decode($forumBlock->params);

                        $params['show_set_tone'] = $model->show_set_tone;
                        $params['set_tone_icons'] = $model->set_tone_icons;
                        $params['show_helpful'] = $model->show_helpful;
						$params['word_count'] = $model->word_count;
						$params['forum_upload'] = $model->forum_upload;

                        $forumBlock->params = CJSON::encode($params);
                        $forumBlock->save(false);
                    }
                    $db_transaction->commit();
                    echo "<a class='auto-close success'></a>";
                }
            }

            $html = $this->renderPartial('_rating_settings', array(
                'model' => $model,
                'block' => $block,
            ), true, true);

            echo $html;

            Yii::app()->end();
        }
        catch (CException $e) {
            $db_transaction->rollback();
            $html = $this->renderPartial('_dialog2_error', array(
                'message' => $e->getMessage(),
            ), true);
            echo $html;
            Yii::app()->end();
        }
    }






	/**
	 * Add OR update thread
	 */
	public function actionAxManageThread()
	{
		$forum_id = Yii::app()->request->getParam("forum_id", false);
		$thread_id = Yii::app()->request->getParam("thread_id", false);

		if (!$thread_id)
		{
			$thread = LearningForumthread::model()->findByPk($thread_id);
		}
		else
		{
			$thread = new LearningForumthread();
			$thread->idForum = $forum_id;
		}

		$html = $this->renderPartial('_manage_thread', array(
			'model' => $thread,
		), true, true);

		$result = new AjaxResult();
		$result->setStatus(true)->setHtml($html)->toJSON();
	}


	/**
	 * Remove Forum
	 * @throws CException
	 */
	public function actionAxRemoveForum() {

		$forum_id = Yii::app()->request->getParam("forum_id", false);
		$id_block = 0;
		$result = new AjaxResult();

		if(is_null(Yii::app()->db->getCurrentTransaction()))
			Yii::app()->db->beginTransaction();

		try {
			$model = LearningForum::model()->findByPk($forum_id);
			if (!$model) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				Yii::app()->end();
			}

			if (!$model->delete()) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				Yii::app()->end();
			}

			$block_model = PlayerBaseblock::model()->findByAttributes(array('course_id' => $this->course_id, 'content_type' => 'comments', 'params' => '{"forum_id":"'.$forum_id.'"}'));

			if($block_model)
			{
				$id_block = $block_model->id;
				if(!$block_model->delete())
				{
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
					Yii::app()->end();
				}
			}
		}
		catch (CException $e) {
			$result->setStatus(false)->setHtml($e->getMessage())->toJSON();
			Yii::app()->db->getCurrentTransaction()->rollback();
			Yii::app()->end();
		}


		$result->setStatus(true)->setData($id_block)->setMessage(Yii::t('standard', '_OPERATION_SUCCESSFUL'))->toJSON();
		Yii::app()->db->getCurrentTransaction()->commit();
		Yii::app()->end();
	}





	/**
	 * Lock/Unlock forum comments
	 */
	public function actionAxToggleLock()
	{
		$result = new AjaxResult();
		if ($this->_canModerateForum())
		{
			$forum_id = Yii::app()->request->getParam("forum_id", false);
			$forum = LearningForum::model()->findByPk($forum_id);
			if ($forum_id && $forum)
			{
				$forum->locked = $forum->locked ? 0 : 1;
				$forum->save(false);
				if ($forum->locked)
					$result->setStatus(true)->setMessage(Yii::t('standard', '_OPERATION_SUCCESSFUL'))->toJSON();
				else
					$result->setStatus(true)->setMessage(Yii::t('standard', '_OPERATION_SUCCESSFUL'))->toJSON();
			}
			else
				$result->setStatus(false)->setMessage(Yii::t('standard', '_OPERATION_FAILURE'))->toJSON();
		}
		else
			$result->setStatus(false)->setMessage(Yii::t('course', '_NOENTER'))->toJSON();
	}

    /**
     * Saves the star type rating value given by the user to a forum message
     */
    public function actionAxVoteTone() {

        if (Yii::app()->request->isAjaxRequest)
        {
            $idUser = Yii::app()->user->id;
            $idMessage = intval( Yii::app()->request->getParam('message_id') );
            $value = intval( Yii::app()->request->getParam('value') );

            // vote forum message
            $model = LearningForummessage::model()->findByPk($idMessage);

            if ($model) {
                try {
                    echo $model->saveRating($idUser, $value);
                }
                catch (CDbException $ex) {

                }
            }
        }

    }

    /**
     * Saves the like/dislike rating given by the user to a forum message
     */
    public function actionAxVoteHelpful() {

        if (Yii::app()->request->isAjaxRequest)
        {
            $idUser = Yii::app()->user->id;
            $idMessage = intval( Yii::app()->request->getParam('message_id') );
            $value = intval( Yii::app()->request->getParam('value') );

			// vote post or comment
			if ($idMessage > 0) {
				try {
					$model = new LearningForummessageVotes();

					$learningForummessageVotes = $model->tableName();
					Yii::app()->db->createCommand("
DELETE FROM $learningForummessageVotes
WHERE id_message=$idMessage AND id_user=$idUser AND type='helpful'")->query();

					$model->id_user = $idUser;
					$model->id_message = $idMessage;
					$model->type = 'helpful';
					$model->value = ($value > 0) ? 1 : -1;
					$model->save();
				}
				catch (CException $ex) {}

				// return the number of likes/dislikes
				$forumMessage = new LearningForummessage();
				$forumMessage->idMessage = $idMessage;
				$helpful = $forumMessage->getHelpful();
				echo CJSON::encode($helpful);
			}
        }

    }
}