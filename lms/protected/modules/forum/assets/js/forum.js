var LmsForum = function(options) {
	this.init(options);
};

/**
 * It is all about FORUM here 
 * 
 */
(function ( $ ) {


	/**
	 * Define 
	 */
	LmsForum.prototype = {

		/**
		 * Init is called at object creation
		 * 
		 * What it does is to take the incoming options object/array and create an attribute for every element.
		 * So, whatever we send as an option[k] will be a Forum attribute and globally available.
		 * Do not send security sensitive information as an option!!!
		 * 
		 * @param options
		 */
		init: function(options) {
			for(var k in options) {
				this[k] = options[k];
			}
		},
		
		/**
		 * Create/Edit Forum 
		 */
		updateForum: function(e) {
			e = $.event.fix(e); // IE fix
			var that = this;  // Forum object
			var elem = $(e.currentTarget);
			var url = elem.attr('href');
			var forum_id = elem.data('forum-id');
			
			$.ajax({
				url: url,
				type: 'post',
				dataType: 'json',
				data: {
					forum_id: forum_id
				},
				error: function (data) { Docebo.Feedback.show('error', 'Ajax error'); },
				beforeSend: function() {
				}
			}).done(function(res) {
				if (res.success == true) {
					if (res.html != '') {
						
						// Show Edit form
						that.showInlinePanel(res.html);
						TinyMce.attach('#forum-description', 'simple', {height: '250px'});
						
						// Handle Cancel form
						$('#update-forum-cancel').on('click', function() {
							that.hideInlinePanel();
							Docebo.Feedback.hide();
							return false;
						});
						
						// Handle form submit
						$('#update-forum-form').submit(function(){
							Docebo.Feedback.hide();
							var options = {
								dataType: 'json',
								success: function(res) {
									if (res.success) {
										that.hideInlinePanel();
										$.fn.yiiGridView.update('course-forums');
									}
									else {
										Docebo.Feedback.show('error', res.message);
									}
								}
							};
							$(this).ajaxSubmit(options);
							return false;
						});
						
					}
				}
			});
			
			return false;
		},
		
		/**
		 * Remove a forum. Forum ID is in the HREF of the element
		 * @param e Event
		 * @returns {Boolean}
		 */
		removeForum: function(e) {
			e = $.event.fix(e); // IE fix
			var elem = $(e.currentTarget);
			var url = elem.attr('href');
			
			// Show Prompt
			var dialogMessage = Yii.t('forum', 'Delete this forum and all its content (discussions and messages)? <br><br>' + 
			'<span class="i-sprite is-solid-exclam red"></span> <strong>Warning! This operation can not be undone!</strong>');
			
			// Show prompt dialog and handle answer
			bootbox.dialog(dialogMessage, 
				[
				 {
					 'label': Yii.t('standard', '_DEL'),
					 'class': 'btn-docebo big red',
					 'callback': function() {

						    $.post(url, {}, function(res) {
						        if (res.success == true) {
						        	Docebo.Feedback.show('success', res.message);
								    $.fn.yiiGridView.update('course-forums');
						        }
						        else {
						        	Docebo.Feedback.show('error', res.message);
						        }
						    }, 'json');
					 }
				 },
				 {
					 'label': Yii.t('standard', '_CANCEL'),
					 'class': 'btn-docebo big black',
					 'callback': function() {
						 
					 }
				 }
				 
				],
				{
					header: Yii.t('forum', 'Delete forum')
				}
			);
			
			return false;
		},
		
		
		/**
		 * This one must be called as Forum.addListeners() on DOM Ready
		 */
		addListeners: function() {
			// Create New forum menu action
			$('#docebo-bootcamp-course-forums').on("click", '#create-new-forum', $.proxy(this.updateForum, this));
			
			// Edit Forum icon in Forums grid view
			$(document).on("click", '.edit-forum-operation', $.proxy(this.updateForum, this));
			
			// Delete Forum icon in Forums grid view
			$(document).on("click", '.remove-forum-operation', $.proxy(this.removeForum, this));

			// checkbox on confirm
			$(document).on('change', '#confirmDelete', function(){
				if ($("#confirmDelete").is(':checked'))
					$('.confirmBootBox').removeClass('disabled');
				else
					$('.confirmBootBox').addClass('disabled');
			});

			$('#modal').on('show', function(){
				$("input").styler();
			});


		},

		/**
		 * search threads grid
		 */
		searchThreads: function()
		{
			if ($("#searchThreadsForm").length)
			{
				$.fn.yiiGridView.update('forum-threads', {
					data: $("#searchThreadsForm").find("input[type='hidden'], :input:not(:hidden)").serialize()
				});
			}
		},

		manageItem: function(e, url)
		{
			e = $.event.fix(e); // IE fix
			var params = [];
			var submit = false;
			if ($(e.target).is("#submit-manage-item"))
			{
				params = $("#manage-item-form").serialize();
				submit = true;
			}

			$.ajax({
				url: url,
				type: 'post',
				dataType: 'json',
				data: params,
				error: function (data) {
					Docebo.Feedback.show('error', 'Ajax error');
				},
				success: function (data) {
					if (data.success == true) {
						Docebo.Feedback.hide();
						if (submit)
						{
							Forum.hideInlinePanel();
							Forum.searchThreads();
							Forum.searchMessages();
						}
						else
						{
							Forum.showInlinePanel(data.html);
							TinyMce.attach('#message-body', 'simple', {height: '250px'});
						}
					}
					else {
						Docebo.Feedback.show('error', data.message);
					}
				}
			});
		},

        manageSubItem: function(e, url, id)
        {
            e = $.event.fix(e); // IE fix
            var params = [];
            var submit = false;
            if ($(e.target).is("#submit-manage-item"))
            {
                params = $("#manage-item-form").serialize();
                submit = true;
            }

            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: params,
                error: function (data) {
                    Docebo.Feedback.show('error', 'Ajax error');
                },
                success: function (data) {
                    if (data.success == true) {
                        Docebo.Feedback.hide();
                        if (submit)
                        {
                            Forum.hideSubInlinePanel(id);
                            // Refresh the new messages
                            Forum.refreshSubReplies(id);
                        }
                        else
                        {
                            Forum.showSubInlinePanel(id, data.html);
                            TinyMce.attach('#message-body' + id, 'simple', {height: '80px'});
                        }
                    }
                    else {
                        Docebo.Feedback.show('error', data.message);
                    }
                }
            });
        },

		/**
		 * Used for delete, lock...
		 * Url is called with ajax with GET and it need json result with the supported format (AjaxResult php class)
		 * @param url
		 */
		changeItemStatus: function(url, type)
		{
			$.ajax({
				url: url,
				dataType: 'json',
				error: function (data) {
					Docebo.Feedback.show('error', 'Ajax error');
				},
				success: function (data) {
					if (data.success == true) {
						Docebo.Feedback.show('success', data.message);
						Forum.refreshUIByType(type);
					}
					else {
						Docebo.Feedback.show('error', data.message);
					}
				}
			});
		},

		refreshUIByType: function(type)
		{
			if (type == 'lock' || type == 'unlock')
				Forum.toggleLockUnlock(type);
			else if (type == 'important' || type == 'unimportant')
				Forum.toggleImportant(type);
			else if (type == 'enable' || type == 'disable')
				Forum.toggleEnable(type);
			else
			{
				Forum.searchThreads();
				Forum.searchMessages();
			}
		},

		/**
		 * search messages list view
		 */
		searchMessages: function()
		{
			if ($("#searchMessagesForm").length)
			{
				$.fn.yiiListView.update('forum-messages', {
					data: $("#searchMessagesForm").find("input[type='hidden'], :input:not(:hidden)").serialize()
				});
			}
		},

		/**
		 * Show the one-for all inline edit panel, set the html. 
		 */
		showInlinePanel: function(html)
		{
			$("#forum-inline-edit-panel").html(html).show();
		},

        /**
         * Hide the one-for all inline edit panel. Destroy inner html
         *
         * @param keepTinyMces boolean True: do NOT detach TinyMces, False: detach
         */
        hideInlinePanel: function(keepTinyMces)
        {
            // NOTE: ** Before ** destroying an Html with 'textarea' having TinyMce attached (if attached),
            // we must detach TinyMce from it. If we do not destroy TinyMce, the next time you attach it to
            // the same textarea (same selector) the textarea content is replaced by previous TinyMce editor
            // content (perhaps staying in memory). This is put here just to be sure we destroy ALL tinymce's
            // in the inline edit panel when we hide it.

            if (!keepTinyMces)
                TinyMce.destroy('#forum-inline-edit-panel textarea');

            // Destroy HTML and hide the panel
            $("#forum-inline-edit-panel").html('').closest(".forum-message-new-sub-replies").hide();

        },

        /**
         * Show the one-for all inline edit panel, set the html.
         */
        showSubInlinePanel: function(id, html)
        {
            $(".forum-message-new-sub-reply-" + id).html(html).show().closest(".forum-message-new-sub-replies").show();
        },

        /**
         * Hide the one-for all inline edit panel. Destroy inner html
         *
         * @param keepTinyMces boolean True: do NOT detach TinyMces, False: detach
         */
        hideSubInlinePanel: function(id, keepTinyMces)
        {
            // NOTE: ** Before ** destroying an Html with 'textarea' having TinyMce attached (if attached),
            // we must detach TinyMce from it. If we do not destroy TinyMce, the next time you attach it to
            // the same textarea (same selector) the textarea content is replaced by previous TinyMce editor
            // content (perhaps staying in memory). This is put here just to be sure we destroy ALL tinymce's
            // in the inline edit panel when we hide it.

            if (!keepTinyMces)
                TinyMce.destroy(".forum-message-new-sub-reply-" + id + ' textarea');

            // Destroy HTML and hide the panel
            $(".forum-message-new-sub-reply-" + id).html('').hide().closest(".forum-message-new-sub-replies").hide();

        },

		deleteConfirmDialog: function(callback, url, dialogMessage)
		{
			dialogMessage = dialogMessage + '<br><br><input type="checkbox" id="confirmDelete" />&nbsp;&nbsp;<label for="confirmDelete" style="display: inline-block;">'+this.confirmMessage+'</label>';
			Forum.confirmDialog('delete', callback, url, dialogMessage);
		},

		confirmDialog: function(type, callback, url, dialogMessage)
		{
			var label = 'Dialog';
			var btnClass = '';
			switch (type)
			{
				case 'delete':
					label = Yii.t('standard', '_DEL');
					btnClass = 'disabled';
					break;
				case 'lock':
					label = Yii.t('forum', '_LOCKFORUM');
					break;
				case 'unlock':
					label = Yii.t('forum', '_FREE');
					break;
				case 'important':
					label = "Important";
					break;
				case 'unimportant':
					label = 'Remove important';
					break;
				case 'enable':
					label = 'Enable access';
					break;
				case 'disable':
					label = 'Disable access';
					break;
			}
			bootbox.dialog(dialogMessage,
				[
					{
						'label': Yii.t('forum', label),
						'class': 'btn-docebo big green confirm confirmBootBox '+btnClass,
						'callback': function() {
							if (type == 'delete')
							{
								if ($("#confirmDelete").is(":checked"))
									callback(url, type);
								else
									return false;
							}
							else
								callback(url, type);
						}
					},
					{
						'label': Yii.t('standard', '_CANCEL'),
						'class': 'btn-docebo big black'
					}
				],
				{
					header: Yii.t('forum', label)
				}
			);
			$('input').styler();
		},

		toggleLockUnlock: function(type)
		{
			if (type == 'lock')
			{
				$("#lock-button").hide();
				$("#unlock-button").show();
			}
			else
			{
				$("#lock-button").show();
				$("#unlock-button").hide();
			}
		},

		toggleImportant: function(type)
		{
			if (type == 'important')
			{
				$("#important-button").hide();
				$("#unimportant-button").show();
			}
			else
			{
				$("#important-button").show();
				$("#unimportant-button").hide();
			}
		},

		toggleEnable: function(type)
		{
			if (type == 'enable')
			{
				$("#enable-button").hide();
				$("#disable-button").show();
			}
			else
			{
				$("#enable-button").show();
				$("#disable-button").hide();
			}
		},

        loadSubReplies: function(comment, parentStyle, reload){
            // Set loader screen
            var loader = '<div class="ajaxloader" style="display: block;"></div>';
            if(reload)
                $('.forum-message-replies-' + comment + ' .forum-message-sub-replies').html(loader).addClass('loaderFix');
            else
                $('.forum-message-replies-' + comment + ' .forum-message-sub-replies').append(loader).addClass('loaderFix');
            // Get Url from which we will get the new replies
            var url = $('.forum-message-replies-' + comment).data('url');
            // Get current Page
            var currentPage = reload ? 0 : $('.forum-message-load-' + comment).find('.load-more-comments').data('page');
            $.ajax({
                type: 'POST',
                url: url,
                data: {page: currentPage, parentCss: parentStyle},
                success: function(data){
                    var response = JSON.parse(data);
                    $('.forum-message-replies-' + comment + ' .forum-message-sub-replies').removeClass('loaderFix');
                    $('.forum-message-replies-' + comment).find('.ajaxloader').remove();
                    if(response.hasComments){
                        // Set Current Page number
                        $('.forum-message-replies-' + comment).find('.load-more-comments').data('page', currentPage + 1);
                        if(reload)
                            $('.forum-message-replies-' + comment + ' .forum-message-sub-replies').html(response.html);
                        else
                            $('.forum-message-replies-' + comment + ' .forum-message-sub-replies').append(response.html);

						//Update Comment counter
						$('.forum-message-replies-' + comment).closest(".forum-message-box").find(".repliesCounter").html(response.commentCount);

                        $('.forum-message-load-' + comment).show();
                    }else{
                        $('.forum-message-load-' + comment).hide();
                    }
                },
            });
        },

        refreshSubReplies: function(id){
            // Refresh only the sub-replies of the replied reply
            var parentClass = $(".forum-message-replies-"+id).closest(".forum-message-box").children('.span9').attr("class");
            var parentStyle = (parentClass.indexOf("even") > 1 ? "even" : "odd");
            Forum.loadSubReplies(id, parentStyle, true)
        }
	};

}( jQuery ));


/**
 * Initialize global variable to null
 * 
 */
var Forum = null;

$(function(){
	bootbox.animate(false);
	Forum.addListeners();
});
