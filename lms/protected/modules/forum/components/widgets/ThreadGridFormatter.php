<?php

/**
 * Widget to display object status graph
 */
class ThreadGridFormatter extends CWidget
{
	public $data;
	public $column;

	public function init()
	{

	}

	public function run()
	{
		switch($this->column)
		{
			case 'title':
				$this->formatTitle();
				break;
			case 'author':
				$this->formatAuthor();
				break;
			case 'readStatus':
				$this->formatStatus();
				break;
			case 'lastMessage':
				$this->formatLastMsg();
				break;
		}
	}

	private function formatTitle()
	{
		$important = '';
		if ($this->data->important == 1)
			$important = '<i title="'.Yii::t('standard', '_IMPORTANT').'" class="i-sprite is-solid-exclam green"></i> ';

		if ($this->data->disabled && !$this->controller->_canModerateThread())
			echo $important.$this->data->title;
		else
			echo $important.CHtml::link($this->data->title, Yii::app()->createUrl("//forum/messages", array("thread_id"=>$this->data->idThread)), array('class'=>'forum-thread'));
	}

	private function formatAuthor()
	{
		echo '<strong>'.$this->data->threadUser->clearUserId.'</strong>';
	}

	private function formatStatus()
	{
		if ($this->data->disabled)
			echo '<div class="i-sprite is-nopass red"></div>';
		else if ($this->data->locked)
			echo '<div class="i-sprite is-lock"></div>';
		else if ($this->data->userTiming && ($this->data->userTiming->last_access > $this->data->lastMessage->posted))
			echo '<div class="i-sprite is-env-opened"></div>';
		else
			echo '<div class="i-sprite is-env-closed blue"></div>';
	}

	private function formatLastMsg()
	{
		$msg = $this->data->lastMessage;
		echo '<span style="font-size: 11px;">'.$msg->posted."</span><br>";
		echo $msg->title."<br>";
		echo Yii::t('standard', '_FROM').": <strong>".$msg->msgUser->clearUserId."</strong><br>";
	}
}