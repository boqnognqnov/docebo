<?php
class PermissionController extends ForumBaseController
{

	public function init()
	{
		parent::init();
	}

	/*################################
	* PERMISSIONS SECTION FOR FORUMS
	################################*/

	public function _canModerateForum()
	{
		$res = true;
		if (!$this->userCanAdminCourse)
			$res = false;
		return $res;
	}

	/*################################
	 * PERMISSIONS SECTION FOR THREADS
	 ################################*/

	public function _canCreateThread()
	{
		$res = true;
		if ($this->forumModel->locked && !$this->userCanAdminCourse)
			$res = false;
		return $res;
	}

	public function _canUpdateThread(LearningForumthread $thread)
	{
		$res = true;
		if (!$this->userCanAdminCourse)
		{
			if ($thread->locked || $thread->forum->locked)
				$res = false;
			else if ($thread->author != Yii::app()->user->id)
				$res = false;
		}
		return $res;
	}

	public function _canModerateThread()
	{
		$res = true;
		if (!$this->userCanAdminCourse)
			$res = false;
		return $res;
	}

	/*################################
	 * PERMISSIONS SECTION FOR MESSAGES
	 ################################*/

	public function _canReplyMsg()
	{
		$res = true;
		if (($this->threadModel->locked || $this->forumModel->locked) && !$this->userCanAdminCourse)
			$res = false;
		return $res;
	}

	public function _canUpdateMsg(LearningForummessage $thread)
	{
		$res = true;
		if (!$this->userCanAdminCourse)
		{
			if ($this->forumModel->locked || $thread->thread->locked)
				$res = false;
			else if ($thread->author != Yii::app()->user->id)
				$res = false;
		}
		return $res;
	}

	public function _canModerateMsg()
	{
		$res = true;
		if (!$this->userCanAdminCourse)
			$res = false;
		return $res;
	}
	/*####################################
	 * END PERMISSIONS SECTION FOR MESSAGES
	 ####################################*/
}