<?php
/**
 *
 * Forum Module base controller
 *
 */
class ForumBaseController extends Controller {

	public $course_id = false;
	public $forum_id = false;
	public $thread_id = false;
	public $message_id = false;
	public $userCanAdminCourse = false;
	public $courseUser = null;
	public $threadModel = null;
	public $forumModel = null;
	public $messageModel = null;

	public function init() {

		JsTrans::addCategories(array('forum', 'player'));
		
		// Set some globally inheritted controller properties && make some preliminary checks
		$this->forum_id = Yii::app()->request->getParam("forum_id", false);
		$this->thread_id = Yii::app()->request->getParam("thread_id", false);
		$this->message_id = Yii::app()->request->getParam("message_id", false);
		$this->course_id = Yii::app()->request->getParam("course_id", false);

		// If Thread ID is passed, find the forum and get forum ID
		if ($this->thread_id) {
			$threadModel = LearningForumthread::model()->findByPk($this->thread_id);
			$this->threadModel = $threadModel;
			if ($threadModel) {
				$this->forum_id = $threadModel->idForum;
			}
		}

		// If Message ID is passed, find the thread & forum and get IDs
		if ($this->message_id) {
			$messageModel = LearningForummessage::model()->findByPk($this->message_id);
			$this->messageModel = $messageModel;
			if ($messageModel) {
				$this->thread_id = $messageModel->idThread;
				$this->forum_id = $messageModel->thread->idForum;
			}
		}

		// Get forum model, if any
		$forumModel = LearningForum::model()->findByPk($this->forum_id);
		$this->forumModel = $forumModel;

		// No forum model and no course (from request) ?  Nothing to show here... get out..
		// (We are only supposed to show either List of Forum Threads OR List of forums for given course!!)
		if (!$forumModel && !$this->course_id) {
			Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
			$this->redirect(array('/site'),true);
		}


		// Lets get the course id  (we definitely have either course id or forum model)
		$this->course_id = $this->course_id ? $this->course_id : $forumModel->idCourse;

		// Is this a power user and the course was assigned to him?
		$pUserCourseAssigned = CoreUserPuCourse::model()->findByAttributes(array(
			'puser_id' => Yii::app()->user->id,
			'course_id' => $this->course_id
		));

		$courseLevel = LearningCourseuser::userLevel(Yii::app()->user->id, $this->course_id);

		// Set this controller attribute for later usage in forum admin UIs: Course Admins CAN admin course forums in any way
		if (Yii::app()->user->isGodAdmin){
			//if it's god admin, course user level doesn't matter
			$this->userCanAdminCourse = true;
		}elseif($courseLevel && $courseLevel > LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT){
			// User subscribed to course and his level is high enough
			$this->userCanAdminCourse = true;
		}elseif($pUserCourseAssigned && Yii::app()->user->checkAccess('/lms/admin/course/mod')){
			// User power user, course assigned to him, and he has permissions to modify
			$this->userCanAdminCourse = true;
		}


		// Access to this course forums is granted to course subscribers only
		if (!$this->userCanAdminCourse && !LearningCourseuser::isSubscribed(Yii::app()->user->id, $this->course_id) && !Yii::app()->user->isGodAdmin) {
			Yii::app()->user->setFlash('error', Yii::t('course', '_NOENTER'));
			$this->redirect(array('/site'),true);
		}

		// If no forum id has been specified, grant access to Admins only.
		// Students can access only a given FORUM, and not to LIST course forums!
		if (!$forumModel && !$this->userCanAdminCourse && !Yii::app()->user->isGodAdmin) {
			Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
			$this->redirect(array('/site'),true);
		}

		/**
		 * Load LearningCourseuser model for future checks (levels etc)
		 */
		$this->courseUser = LearningCourseuser::model()->findByAttributes(array(
			'idUser' => Yii::app()->user->id,
			'idCourse' => $this->course_id,
		));


		// By now we should have a course id available; Set Yii app "player" component to help other LMS parts to know where we are
		Yii::app()->player->setCourse(LearningCourse::model()->findByPk($this->course_id));

		// Load Tinymce jQuery integrator
		Yii::app()->tinymce->init();

		return parent::init();

	}

	/**
	 * Overwrite resolveLayout method to use module based layout
	 *
	 * (non-PHPdoc)
	 * @see Controller::resolveLayout()
	 */
	public function resolveLayout()
	{
		$this->layout = '/layouts/base';
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::beforeAction()
	 */
	public function beforeAction($action)
	{
		$this->registerResources();
		return parent::beforeAction($action);
	}


	/**
	 * Register JS/CSS
	 */
	public function registerResources() {
		if (!Yii::app()->request->isAjaxRequest) {

			$assetsUrl = $this->module->assetsUrl;
			$cs = Yii::app()->getClientScript();

			// Register forum.js
			$cs->registerScriptFile($assetsUrl . '/js/forum.js');

			// Register forum.css
			$cs->registerCssFile($assetsUrl . '/css/forum.css');

			// Bootcamp menu
			$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/docebo_bootcamp.js');
			$cs->registerCssFile (Yii::app()->theme->baseUrl . '/css/docebo_bootcamp.css');


			// Register JS script(s)
			$options = array(
				'confirmMessage' => Yii::t('standard', 'Yes, I confirm I want to proceed'),
			);
			$options = CJavaScript::encode($options);
			$script = "Forum = new LmsForum($options);";
			$cs->registerScript("forumBaseControllerJs1", $script , CClientScript::POS_HEAD);

		}
	}

	/**
	 * Returns the maximum word count for this forum
	 * @param $course_id
	 * @return int
	 */
	protected function getForumWordCount() {
		$course_id = Yii::app()->player->course->idCourse;
		if($course_id) {
			$courseForumWidgets = PlayerBaseblock::model()->findAllByAttributes(array(
				'course_id' => $course_id,
				'content_type' => PlayerBaseblock::TYPE_FORUM
			));

			foreach ($courseForumWidgets as $forumBlock) {
				$params = CJSON::decode($forumBlock->params);
				if(isset($params['word_count']))
					return $params['word_count'];
			}
		}

		return 0;
	}
}

