<?php

class ForumHelper extends CComponent {

	/**
	 * Try to detect how many words a message text contains
	 * @param $text the message from which words must be count
	 * @return int the number of counted words
	 */
	public static function countWords($text) {

		//handle and format input text
		$tmpText = strtolower($text); //lowerize all
		$tmpText = preg_replace('/<(\/(p|ul|ol|li)|br[\s]*\/)>/', ' ', $tmpText); //some tags are word separators and cannot just be removed, change them in spaces instead
		$tmpText = preg_replace('/(<([^>]*)>)/', '', $tmpText); //remove remaining html tags
		$tmpText = preg_replace('/[\n\r\s]+/', ' ', $tmpText); //remove carriages, new lines etc.
		$tmpText = str_replace('&nbsp;', ' ', $tmpText); //handle html space entities
		//then handle words spacing
		$tmpText = preg_replace('/^[\s]*/', '', $tmpText); //trimming left
		$tmpText = preg_replace('/[\s]*$/', '', $tmpText); //trimming right
		$tmpText = preg_replace('/[\s]+/', ' ', $tmpText); //normalize spaces between words

		//final words count
		if ($tmpText == '') return 0;
		$exploded = explode(' ', $tmpText);
		return count($exploded);
	}



}