<?php

class ForumRatingSettingsForm extends CFormModel {
    
    public $show_set_tone;
    public $set_tone_icons;
    public $show_helpful;
	public $word_count;
	public $forum_upload;


    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            array('show_set_tone, set_tone_icons, show_helpful, word_count, forum_upload', 'safe')
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels() {
        return array(
            'show_set_tone' => Yii::t('myblog', 'Show "Set tone"'),
            'set_tone_icons' => Yii::t('myblog', '"Set tone" icons'),
            'show_helpful' => Yii::t('myblog', 'Show "helpful"'),
			'word_count' => Yii::t('forum', 'Limit the number of words in posts & replies'),
			'forum_upload' => Yii::t('forum', 'Forum upload'),
        );
    }
} 