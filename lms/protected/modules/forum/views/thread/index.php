<?php
	$this->breadcrumbs[Yii::t('menu_over', '_MYCOURSES')] = Docebo::createLmsUrl('site/index');

	if($this->course_id)
	{
		$course = LearningCourse::model()->findByPk($this->course_id);
		$this->breadcrumbs[$course->name] = $this->createUrl('//player', array('course_id' => $this->course_id));
	}

	if($this->userCanAdminCourse)
		$this->breadcrumbs[Yii::t('standard', '_FORUM')] = $this->createUrl('//forum/default/courseForums', array('course_id' => $forum->idCourse));
	$this->breadcrumbs[] = $forum->title;

	$createThreadUrl = $this->createUrl('/forum/thread/axCreate', array("forum_id" => $forum->idForum));
?>
<div class="forum-title"><?=Yii::t('standard', '_FORUM')?> <?=$forum->title?></div>
<!-- See themes/spt/js/docebo_bootcamp.js (simple jQuery Plugin) -->
<ul id="docebo-bootcamp-forum" class="docebo-bootcamp clearfix">
	<? if($this->_canCreateThread()) : ?>
		<li>
			<a href="javascript:;" onclick="Forum.manageItem(event, '<?=$createThreadUrl?>')" data-helper-title="<?= Yii::t('forum', '_ADDTHREAD') ?>" data-helper-text="<?= Yii::t('forum', '_ADDTHREAD') ?>">
				<span class="i-sprite is-chat large"></span>
				<span class="i-sprite is-chat large white"></span>
				<h4><?= Yii::t('forum', '_ADDTHREAD') ?></h4>
			</a>
		</li>
	<? endif; ?>
	<? if ($this->_canModerateForum()) : ?>
		<li style="display: <?=!$forum->locked ? 'none' : 'block'?>" id="unlock-button">
			<a href="<?=Yii::app()->createUrl("/forum/default/axToggleLock", array("forum_id"=>$forum->primaryKey, "course_id" => $forum->idCourse))?>"
				onclick="Forum.confirmDialog('unlock', Forum.changeItemStatus, this.href, '<?=Yii::t('forum', '_UNLOCKFORUM')?>'); return false;" data-helper-title="<?= Yii::t('forum', '_UNLOCKFORUM') ?>" data-helper-text="<?= Yii::t('forum', 'Enable comments in forum thread') ?>">
				<span class="i-sprite is-lock large"></span>
				<span class="i-sprite is-lock large white"></span>
				<h4><?= Yii::t('forum', '_UNLOCKFORUM') ?></h4>
			</a>
		</li>
		<li style="display: <?=$forum->locked ? 'none' : 'block'?>" id="lock-button">
			<a href="<?=Yii::app()->createUrl("/forum/default/axToggleLock", array("forum_id"=>$forum->primaryKey, "course_id" => $forum->idCourse))?>"
			   onclick="Forum.confirmDialog('lock', Forum.changeItemStatus, this.href, '<?=Yii::t('forum', '_LOCKFORUM')?>'); return false;" data-helper-title="<?= Yii::t('forum', '_LOCKFORUM') ?>" data-helper-text="<?= Yii::t('forum', 'Disable comments in a forum thread') ?>">
				<span class="i-sprite is-lock large"></span>
				<span class="i-sprite is-lock large white"></span>
				<h4><?= Yii::t('forum', '_LOCKFORUM') ?></h4>
			</a>
		</li>
	<? endif; ?>
	<li class="helper">
		<a href="#">
			<span class="i-sprite is-circle-quest large"></span>
			<h4 class="helper-title"></h4>
			<p class="helper-text"></p>
		</a>
	</li>

</ul>
<div class="clearfix"></div>

<div id="forum-inline-edit-panel" class="inline-edit-panel" style="display: none;"></div>
<div class="forum-title"><?=Yii::t('forum', '_THREAD')?></div>
<div class="row-fluid">
	<div class="grid-nav-grey-box">
		<div class="span6"></div>
		<div class="span6">
			<form id="searchThreadsForm" class="grid-search-form" onsubmit="Forum.searchThreads();return false;">
				<?=CHtml::button("searchSubmit", array('value'=>'', 'onclick'=>'Forum.searchThreads()'));?>
				<?=CHtml::activeTextField($thread, 'search_input');?>
			</form>
		</div>
	</div>
</div>
<?php
$this->widget('DoceboSelGridView', array(
	'id' => 'forum-threads',
	'afterAjaxUpdate' => 'function(){$(document).controls();}',
	'dataProvider' => $thread->search(),
	'enableSorting' => false,
	'columns' => array(
		array(
			'name' => '',
			'type' => 'raw',
			'value' => '$this->grid->controller->widget("ThreadGridFormatter", array("data"=>$data, "column"=>"readStatus"), true)',
		),
		array(
			'name' => 'title',
			'type' => 'raw',
			'value' => '$this->grid->controller->widget("ThreadGridFormatter", array("data"=>$data, "column"=>"title"), true)',
		),
		array(
			'name' => 'num_post',
			'value' => '$data->getReplyNumber()'
		),
		array(
			'name' => 'author',
			'value' => '$this->grid->controller->widget("ThreadGridFormatter", array("data"=>$data, "column"=>"author"), true)',
			'type' => 'raw'
		),
		array(
			'name' => 'num_view',
			'value' => '$data->num_view'
		),
		array(
			'name' => 'last_post',
			'value' => '$this->grid->controller->widget("ThreadGridFormatter", array("data"=>$data, "column"=>"lastMessage"), true)',
			'type' => 'raw'
		),
		array
		(
			'class'=>'CButtonColumn',
			'template'=>'{move}{edit}{remove}',
			'htmlOptions'=>array('style'=>'min-width: 70px'),
			'buttons'=>array
			(
				'move' => array
				(
					'label'=>'',
					'url'=>'Yii::app()->createUrl("/forum/thread/axMove", array("thread_id"=>$data->primaryKey, "course_id" => '.$forum->idCourse.'))',
					'options' => array(
						'class' => 'i-sprite is-moveto',
					),
					'click'=>'function(event){Forum.manageItem(event, this.href);return false;}',
					'visible' => $this->_canModerateThread() ? 'true' : 'false',
				),
				'edit' => array
				(
					'label'=>'',
					'url'=>'Yii::app()->createUrl("/forum/thread/axUpdate", array("thread_id"=>$data->primaryKey, "course_id" => '.$forum->idCourse.'))',
					'options' => array(
						'class' => 'i-sprite is-edit',
					),
					'click'=>'function(event){Forum.manageItem(event, this.href);return false;}',
					'visible' => 'Yii::app()->controller->_canUpdateThread($data) ? true : false',
				),
				'remove' => array
				(
					'label'=>'',
					'url'=>'Yii::app()->createUrl("/forum/thread/axDelete", array("thread_id"=>$data->primaryKey, "course_id" => '.$forum->idCourse.'))',
					'options' => array(
						'class' => 'i-sprite is-remove red',
					),
					'click'=>'function(event){Forum.deleteConfirmDialog(Forum.changeItemStatus, this.href, "'.Yii::t("forum", "Delete this discussion and all messages?").'");return false;}',
					'visible' => $this->_canModerateThread() ? 'true' : 'false',
				),
			),
		),
	),
));
?>

<script type="text/javascript">
<!--
$(document).ready(function(){
	$("#docebo-bootcamp-forum").doceboBootcamp({});
});
//-->
</script>