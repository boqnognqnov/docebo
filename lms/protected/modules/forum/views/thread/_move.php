<div class="forum-update-forum">

	<div class="header">
		<?= Yii::t('forum', '_MOVE_TO_FORUM'); ?>
	</div>
	<div class="content">

		<?php
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'manage-item-form',
			'enableAjaxValidation' => false,
			'htmlOptions' => array(
				'class' => 'form-horizontal',
			),
		));
		?>

		<div class="control-group">
			<?php echo $form->label($model, 'idForum', array('class' => 'control-label')); ?>
			<div class="controls">
				<?php echo $form->dropDownList($model, 'idForum', CHtml::listData(LearningForum::model()->getForumsByCourse($this->course_id), 'idForum', 'title')); ?>
			</div>
		</div>

		<div class="buttons">
			<?= CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class' => 'btn-docebo green big', 'id' => 'submit-manage-item', 'onclick' => 'Forum.manageItem(event, "'.$this->createUrl('/forum/thread/axUpdate', array("thread_id" => $model->idThread)).'"); return false;')); ?>
			<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn-docebo black big', 'onclick' => 'Forum.hideInlinePanel()')); ?>
		</div>

		<?php $this->endWidget(); ?>
	</div>
</div>

