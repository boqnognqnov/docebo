<?php 
$this->widget('DoceboSelGridView', array(
	'id' => 'all-lms-forums',
	'afterAjaxUpdate' => 'function(){}',
	'dataProvider' => LearningForum::model()->getAllForumsDataProvider(),
	'columns' => array(
		array(
			'name' => 'Forum',
			'type' => 'raw',
			'value' => array($this, '_renderForumLink')
		),
	),
)); 
?>
