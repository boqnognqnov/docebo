<?php
/**
 * Dual-used form: once for INLINE forum create/edit and second, as POPUP dialog content.
 * Depends on the passed $popup variable: false for INLINE, true for POPUP
 *
 */
?>
<?php
	$radioTemplate = '<label class="radio">{input}{label}</label>';

	if ($popup) {
		$action = $this->createUrl("axEditForumPopup", array('forum_id' => $model->idForum,'course_id' => $model->idCourse));
		$ajaxClass = "ajax";
	}
	else {
		$action = $this->createUrl("axUpdateForum", array('forum_id' => $model->idForum,'course_id' => $model->idCourse));
		$ajaxClass = "";
	}

	$form = $this->beginWidget('CActiveForm', array(
			'action' => $action,
			'method' => 'POST',
			'id' => 'update-forum-form',
			'enableAjaxValidation' => false,
			'htmlOptions'=>array(
			'class' => 'form-horizontal ' . $ajaxClass,
		),
	));

	echo $form->hiddenField($model, 'idCourse');

?>


<?php if (!$popup) : ?>
	<div class="header"><?= Yii::t('standard', '_ADD'); ?></div>
<?php else : ?>
	<h1><?= Yii::t('standard', '_ADD'); ?></h1>
	<?= CHtml::errorSummary($model) ?>
<?php endif; ?>



<div class="content">

	<!-- Forum title -->
	<div class="control-group">
		<label class="control-label"><?= Yii::t('standard', '_TITLE'); ?></label>
		<div class="controls">
			<div class="row-fluid">
				<?php echo $form->textField($model, 'title', array('class' => 'span12')); ?>
			</div>
		</div>
	</div>


	<!-- Description -->
    <div class="control-group">
    	<label class="control-label"><?= Yii::t('standard', '_DESCRIPTION'); ?></label>
    	<div class="controls">
    		<?php
    			echo $form->textArea($model, 'description', array(
            		'id' => 'forum-description',
                	'class' => 'input-xlarge',
                	'rows' => 5
            	));
			?>
		</div>
    </div>

    <!-- Locked -->
	<div class="control-group">
		<label class="control-label" for="LearningForum[locked]"><?= Yii::t('forum', '_LOCKED'); ?></label>
		<div class="controls">
			<?php
				echo  $form->radioButtonList($model, 'locked',
					array(
						'1' => Yii::t('standard', '_YES'),
						'0' => Yii::t('standard', '_NO')
					),
					array('template' => $radioTemplate,'separator' => '')
				);
			?>
		</div>
	</div>

	<!-- Forrm Buttons -->
	<div class="buttons form-actions">
		<?= CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class' => 'btn-docebo green big', 'id' => 'update-forum-submit')); ?>
		<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'close-dialog btn-docebo black big', 'id' => 'update-forum-cancel')); ?>
	</div>


	<!-- Panel Footer -->
	<div class="footer">
	</div>


</div>

<?php $this->endWidget(); ?>

