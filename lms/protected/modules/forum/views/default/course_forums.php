<?php
    $this->breadcrumbs = array(
        Yii::t('menu_over', '_MYCOURSES') => Docebo::createLmsUrl('site/index'),
        Yii::t('player', $courseModel->name) => Docebo::createLmsUrl('player', array('course_id' => $courseModel->idCourse)),
        Yii::t('standard', '_FORUM'),
    );

	$createForumUrl = $this->createUrl("//forum/default/axUpdateForum", array("course_id" => $courseModel->idCourse));

?>

<ul id="docebo-bootcamp-course-forums" class="docebo-bootcamp clearfix">
	<li>
		<a id="create-new-forum" href="<?= $createForumUrl ?>">
			<span class="i-sprite is-chat large"></span>
			<span class="i-sprite is-chat large white"></span>
			<h4><?= Yii::t('forum', '_ADDFORUM') ?></h4>
		</a>
	</li>

	<li class="helper">
		<a href="#">
			<span class="i-sprite is-circle-quest large"></span>
			<h4 class="helper-title"></h4>
			<p class="helper-text"></p>
		</a>
	</li>

</ul>

<div class="clearfix"></div>



<!-- Here goes Create/Edit Forum interface (ajax loaded) -->
<div id="forum-inline-edit-panel" class="inline-edit-panel" style="display: none;"></div>




<?php
$this->widget('DoceboSelGridView', array(
	'id' => 'course-forums',
	'afterAjaxUpdate' => 'function(){}',
	'dataProvider' => LearningForum::model()->getCourseForumsDataProvider($courseModel->idCourse),
	'columns' => array(

		array(
			'name' => '',
			'type' => 'raw',
			'value' => array($this, '_renderCourseForumsLockedColumn'),
			'htmlOptions'=>array('width'=>'30px'),
		),

		array(
			'name' => Yii::t('standard','_TITLE'),
			'type' => 'raw',
			'value' => array($this, '_renderForumLink')
		),

		array(
			'name' => '',
			'cssClassExpression' => '"text-right"',
			'type' => 'raw',
			'value' => array($this, '_renderCourseForumsOperationsColumn')
		),

	),
));
?>


<script type="text/javascript">
<!--

	$(function(){
		$('#docebo-bootcamp-course-forums').doceboBootcamp();
	});

//-->
</script>