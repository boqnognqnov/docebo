<?php
/* @var $model ForumRatingSettingsForm */
/* @var $block PlayerBaseblock */

/* @var $form CActiveForm */

	$radioTemplate = '<label class="radio">{input}{label}</label>';

    $ajaxClass = "ajax";

	$form = $this->beginWidget('CActiveForm', array(
			'method' => 'POST',
			'id' => 'update-forum-form',
			'enableAjaxValidation' => false,
			'htmlOptions'=>array(
			'class' => 'player-forum-rating-settings-form form-horizontal ' . $ajaxClass,
		),
	));
?>


<h1><?= Yii::t('standard', 'Settings'); ?></h1>
<?= CHtml::errorSummary($model) ?>


<div class="content">

	<div class="control-group">
		<label class="control-label"><?= Yii::t('forum', 'Words limit for posts & replies') ?></label>
		<div class="controls">
			<div class="row-fluid">
				<?= $form->textField($model, 'word_count', array()) ?>
			</div>
		</div>
	</div>

    <hr />

    <div class="control-group">
        <label class="control-label" for="LearningForum[locked]"><?= Yii::t('myblog', 'Show'); ?></label>
        <div class="controls">
            <?= $form->checkBox($model, 'show_helpful', array()) ?>
            <label class="inline-label" for="ForumRatingSettingsForm_show_helpful"><?= Yii::t('myblog', 'Helpful'); ?></label>
        </div>

		<div class="controls">
			<div class="row-fluid">
                <?= $form->checkBox($model, 'show_set_tone', array()) ?>
                <label class="inline-label" for="ForumRatingSettingsForm_show_set_tone"><?= Yii::t('myblog', 'Rate'); ?></label>
			</div>
		</div>
	</div>

    <hr />

    <div class="control-group">
    	<label class="control-label"><?= Yii::t('myblog', '"Set tone" icons') ?></label>
    	<div class="controls">
            <div class="tone-icons-list">

                <div>
                    <div class="rating-container">
                        <?php
                        $this->widget('common.widgets.DoceboStarRating',array(
                            'name'=>'smiley_rating',
                            'type' => 'smiley',
                            'value' => 3
                        ));
                        ?>
                    </div>
                    <?= $form->radioButton($model, 'set_tone_icons', array('value'=>'smiley', 'uncheckValue'=>null)) ?>
                </div>

                <div>
                    <div class="rating-container">
                        <?php
                        $this->widget('common.widgets.DoceboStarRating',array(
                            'name'=>'star_rating',
                            'type' => 'star',
                            'value' => 3
                        ));
                        ?>
                    </div>
                    <?= $form->radioButton($model, 'set_tone_icons', array('value'=>'star', 'uncheckValue'=>null)) ?>
                </div>

                <div style="padding-left:5px;">
                    <div class="rating-container">
                        <?php
                        $this->widget('common.widgets.DoceboStarRating',array(
                            'name'=>'circle_rating',
                            'type' => 'circle',
                            'value' => 3
                        ));
                        ?>
                    </div>
                    <?= $form->radioButton($model, 'set_tone_icons', array('value'=>'circle', 'uncheckValue'=>null)) ?>
                </div>

                <div style="padding-left:5px;">
                    <div class="rating-container">
                        <?php
                        $this->widget('common.widgets.DoceboStarRating',array(
                            'name'=>'heart_rating',
                            'type' => 'heart',
                            'value' => 3
                        ));
                        ?>
                    </div>
                    <?= $form->radioButton($model, 'set_tone_icons', array('value'=>'heart', 'uncheckValue'=>null)) ?>
                </div>
            </div>
		</div>
    </div>

    <hr />

    <div class="control-group">
        <label class="control-label" for="LearningForum[locked]"><?= Yii::t('forum', 'Forum upload'); ?></label>
        <div class="controls">
            <?= $form->checkBox($model, 'forum_upload', array()) ?>
            <label class="inline-label" for="ForumRatingSettingsForm_forum_upload"><?= Yii::t('forum', 'Allow files upload in form messages'); ?></label>
        </div>
    </div>

    <hr />

	<!-- Forrm Buttons -->
	<div class="buttons form-actions">
		<?= CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class' => 'btn-docebo green big', 'id' => 'update-forum-submit')); ?>
		<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'close-dialog btn-docebo black big', 'id' => 'update-forum-cancel')); ?>
	</div>


</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    $('.modal input[type="checkbox"]').styler();
    $('.tone-icons-list > div > :radio').styler();
</script>