<?php

/* @var $data LearningForummessage */

$createReplyUrl = $this->createUrl('/forum/messages/axReply', array("message_id" => $data->idMessage, "thread_id" => $data->idThread));
$editReplyUrl = $this->createUrl('/forum/messages/axUpdate', array("message_id" => $data->idMessage, "thread_id" => $data->idThread));

if ($data->locked == 0 || ($data->locked == 1 && Yii::app()->controller->_canModerateMsg()))
	$hidden = false;
else
	$hidden = true;
?>
<div class="row-fluid forum-message-box">
	<div class="span3 avatar-area">
        <div class="replies">
            <?php echo Yii::t('forum', '_NUMREPLY'); ?>
        </div>
		<div class="forum-message-reply-avatar">
            <?=$data->msgUser->getAvatarImage()?>
		</div>
	</div>
	<div class="span9 forum-message-<?=$index%2 ? "even" : "odd"?>">
		<div class="forum-message-body">
            <label class="forum-message-author"><?=$data->msgUser->fullName?></label>
			<span class="forum-message-date"><?=Yii::t('forum', 'Posted on')?>: <?=$data->posted?></span>
			<p><?=$hidden ? "<b><i>".Yii::t('standard', '_HIDDEN')."</i></b>" : $data->formatQuotedText()?></p>
		</div>
		<?php
		$event = new DEvent($this, array('message' => $data));
		Yii::app()->event->raise('onForumMessageDisplay', $event);
		?>
		<div class="forum-message-actions">
			<? if (!$hidden) : ?>
				<? if ($index != 0 && Yii::app()->controller->_canModerateMsg()) : ?>
				<a href="<?=Yii::app()->createUrl("/forum/messages/axDelete", array("message_id"=>$data->primaryKey, "course_id" => $data->thread->forum->idCourse))?>" onclick="Forum.deleteConfirmDialog(Forum.changeItemStatus, this.href, '<?=Yii::t("forum", "Delete this message?")?>');return false;"><?=Yii::t('standard', '_DEL')?></a>
				<? endif ?>
				<? if (Yii::app()->controller->_canUpdateMsg($data)) : ?>
					<a href="javascript:;" onclick="Forum.manageItem(event, '<?=$editReplyUrl?>')"><?=Yii::t('standard', '_MOD')?></a>
				<? endif; ?>
				<? if (Yii::app()->controller->_canReplyMsg()) : ?>
					<a href="javascript:;" onclick="Forum.manageItem(event, '<?=$createReplyUrl?>')"><?=Yii::t('forum', '_QUOTE')?></a>
				<? endif; ?>
				<? if (Yii::app()->controller->_canModerateMsg()) : ?>
					<a href="<?=Yii::app()->createUrl("/forum/messages/axToggleHide", array("message_id"=>$data->primaryKey, "course_id" => $data->thread->forum->idCourse))?>" onclick="Forum.changeItemStatus(this.href, 'toggleHide');return false;"><?=Yii::t('forum', $data->locked ? "Unhide" : "Hide")?></a>
				<? endif; ?>

                <?php
                $ratingSettings = Yii::app()->session['rating_settings_'.$data->thread->forum->idCourse];
                if (!empty($ratingSettings)) {
                    if (intval($ratingSettings['show_set_tone'])) {
                        ?>
                        <div class="forum-message-rating">
                            <label><?= Yii::t('myblog', 'Set tone:') ?></label>
                            <?php
                            $this->widget('common.widgets.DoceboStarRating',array(
                                'id'=>'message-rating-'.$data->idMessage,
                                'name'=>'message_rating_'.$data->idMessage,
                                'type' => $ratingSettings['set_tone_icons'],
                                'readOnly' => ($data->author==Yii::app()->user->id),
                                'value' => $data->getRating(),
                                'ratingUrl' => Yii::app()->controller->createUrl('default/axVoteTone', array(
                                    'course_id' => $data->thread->forum->idCourse,
                                    'message_id' => $data->idMessage
                                ))
                            ));
                            ?>
                        </div>
                        <?php
                    }
                    if (intval($ratingSettings['show_helpful'])) {
                        $_messageHelpful = $data->getHelpful();
                        $_userHelpfulVote = $data->getUserHelpfulVote(Yii::app()->user->id);
                        $this->widget('common.widgets.DoceboHelpful',array(
                            'id'=>'message-helpful-'.$data->idMessage,
                            'likes' => $_messageHelpful['likes'],
                            'dislikes' => $_messageHelpful['dislikes'],
                            'vote' => ($data->author==Yii::app()->user->id) ? 'readonly' : $_userHelpfulVote,
                            'ratingUrl' => Yii::app()->controller->createUrl('default/axVoteHelpful', array(
                                'course_id' => $data->thread->forum->idCourse,
                                'message_id' => $data->idMessage
                            ))
                        ));
                    }
                }
                ?>


			<? endif; ?>
		</div>
	</div>
</div>