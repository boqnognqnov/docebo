<?php

/* @var $data LearningForummessage */

$createSubReplyUrl = $this->createUrl('/forum/messages/axSubReply', array("message_id" => $data->idMessage, "thread_id" => $data->idThread));
$quoteReplyUrl = $this->createUrl('/forum/messages/axSubReply', array("message_id" => $data->idMessage, "thread_id" => $data->idThread, 'quote' => true));
$editReplyUrl = $this->createUrl('/forum/messages/axUpdate', array("message_id" => $data->idMessage, "thread_id" => $data->idThread));

if ($data->locked == 0 || ($data->locked == 1 && Yii::app()->controller->_canModerateMsg()))
    $hidden = false;
else
    $hidden = true;
?>
<div class="row-fluid forum-message-box">
    <div class="span3">
        <div class="span3 forum-message-avatar">
            <?=$data->msgUser->getAvatarImage()?>
        </div>
        <div class="span9">
            <label class="forum-message-author"><?=$data->msgUser->fullName?></label>
            <?php /*<a href="#" class="forum-message-author-link"><?=Yii::t('standard', '_VIEW_PROFILE')?></a>*/ ?>
        </div>
        <div class="span12 forum-message-counter">
            <strong><?=Yii::t('forum', '_NUMREPLY')?>:</strong>
            <div class="repliesCounter"> <?=LearningForummessage::getCountSubReplies($data->idMessage)?> </div>
        </div>
    </div>
    <div class="span9 forum-message-<?=$index%2 ? "even" : "odd"?>">
        <div class="forum-message-body">
            <span class="forum-message-date"><?=Yii::t('forum', 'Posted on')?>: <?=$data->posted?></span>
            <p><?=$hidden ? "<b><i>".Yii::t('standard', '_HIDDEN')."</i></b>" : $data->formatQuotedText()?></p>

            <?php // Get the forumParams here because cannot pass it to the widget trough the index.php
            $forumBlock = PlayerBaseblock::model()->findByAttributes(array(
                'course_id' => $data->thread->forum->idCourse,
                'content_type' => PlayerBaseblock::TYPE_FORUM
            ));
            $forumParams = ($forumBlock) ? CJSON::decode( $forumBlock->params ) : array();
            ?>
            <!-- If there is file in database and the check for upload in Forum is ON -->
            <?php if ($data->original_filename && $forumParams['forum_upload'] == 1) : ?>
                <p class="file-container">
                    <a class="file-name" href="<?=Docebo::createAbsoluteUrl("forum/messages/downloadFile", array(
                        'thread_id' => $data->idThread, 'fileId' => $data->path, 'fileName' => $data->original_filename))?>" data-id="<?=$data->path?>">
                        <i class="fa fa-paperclip fa-lg"></i>
                        <b><?=$data->original_filename?></b>
                    </a>
                    <? if (Yii::app()->controller->_canUpdateMsg($data)) : ?>
                        <span class="close delete-file" data-message="<?=$data->idMessage?>" data-id="<?=$data->path?>">&times;</span>
                    <? endif; ?>
                </p>
            <?php endif; ?>
        </div>
        <?php
            $event = new DEvent($this, array('message' => $data));
            Yii::app()->event->raise('onForumMessageDisplay', $event);
        ?>
        <div class="forum-message-actions threadContent">
            <? if (!$hidden) : ?>
                <? if (Yii::app()->controller->_canModerateMsg()) : ?>
                    <?php $url = Yii::app()->createUrl("/forum/messages/axDelete", array("message_id"=>$data->primaryKey, "course_id" => $data->thread->forum->idCourse)); ?>
                    <a href="<?= $url ?>" onclick="Forum.deleteConfirmDialog(Forum.changeItemStatus, this.href, '<?=Yii::t("forum", "Delete this message?")?>');return false;"><?=Yii::t('standard', '_DEL')?></a>
                <? endif ?>
                <? if (Yii::app()->controller->_canUpdateMsg($data)) : ?>
                    <a href="javascript:;" onclick="Forum.manageItem(event, '<?=$editReplyUrl?>')"><?=Yii::t('standard', '_MOD')?></a>
                <? endif; ?>
                <? if (Yii::app()->controller->_canReplyMsg()) : ?>
                    <a href="javascript:;" onclick="Forum.manageSubItem(event, '<?=$quoteReplyUrl?>', '<?=$data->idMessage?>')"><?=Yii::t('forum', '_QUOTE')?></a>
                <? endif; ?>
                <? if (Yii::app()->controller->_canModerateMsg()) : ?>
                    <a href="<?=Yii::app()->createUrl("/forum/messages/axToggleHide", array("message_id"=>$data->primaryKey, "course_id" => $data->thread->forum->idCourse))?>" onclick="Forum.changeItemStatus(this.href, 'toggleHide');return false;"><?=Yii::t('forum', $data->locked ? "Unhide" : "Hide")?></a>
                <? endif; ?>
                <? if (Yii::app()->controller->_canReplyMsg()) : ?>
                    <a href="javascript:;" onclick="Forum.manageSubItem(event, '<?=$createSubReplyUrl?>', '<?=$data->idMessage?>')"><?=Yii::t('standard', '_REPLY')?></a>
                <? endif; ?>

                <?php
                    $ratingSettings = Yii::app()->session['rating_settings_'.$data->thread->forum->idCourse];
                    if (!empty($ratingSettings)) {
                        if (intval($ratingSettings['show_set_tone'])) {
                            ?>
                            <div class="forum-message-rating">
                                <label><?= Yii::t('myblog', 'Set tone:') ?></label>
                                <?php
                                $this->widget('common.widgets.DoceboStarRating',array(
                                    'id'=>'message-rating-'.$data->idMessage,
                                    'name'=>'message_rating_'.$data->idMessage,
                                    'type' => $ratingSettings['set_tone_icons'],
                                    'readOnly' => ($data->author==Yii::app()->user->id),
                                    'value' => $data->getRating(),
                                    'ratingUrl' => Yii::app()->controller->createUrl('default/axVoteTone', array(
                                        'course_id' => $data->thread->forum->idCourse,
                                        'message_id' => $data->idMessage
                                    ))
                                ));
                                ?>
                            </div>
                            <?php
                        }
                        if (intval($ratingSettings['show_helpful'])) {
                            $_messageHelpful = $data->getHelpful();
                            $_userHelpfulVote = $data->getUserHelpfulVote(Yii::app()->user->id);
                            $this->widget('common.widgets.DoceboHelpful',array(
                                'id'=>'message-helpful-'.$data->idMessage,
                                'likes' => $_messageHelpful['likes'],
                                'dislikes' => $_messageHelpful['dislikes'],
                                'vote' => ($data->author==Yii::app()->user->id) ? 'readonly' : $_userHelpfulVote,
                                'ratingUrl' => Yii::app()->controller->createUrl('default/axVoteHelpful', array(
                                    'course_id' => $data->thread->forum->idCourse,
                                    'message_id' => $data->idMessage
                                ))
                            ));
                        }
                    }
                ?>
            <? endif; ?>
        </div>
        <div class="forum-message-replies-<?= $data->idMessage ?>" data-message="<?= $data->idMessage ?>" data-url="<?= Yii::app()->createUrl("/forum/messages/axLoadSubReplies", array("message_id"=>$data->primaryKey)) ?>">
            <div class="forum-message-sub-replies"></div>
            <div class="forum-message-new-sub-replies">
                <div class="span3"></div>
                <div class="span9">
                    <div class="forum-message-new-sub-reply-<?= $data->idMessage ?>"></div>
                </div>
            </div>
            <div class="forum-message-load-more forum-message-load-<?= $data->idMessage ?>">
                <div class="span3"></div>
                <div class="span9">
                    <a class="text-colored load-more-comments" href="javascript:;" data-page="0" onclick="Forum.loadSubReplies('<?=$data->idMessage?>', '<?=$index%2 ? "even" : "odd"?>', false)"><?=Yii::t('standard', 'Load More')?></a>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        Forum.loadSubReplies("<?= $data->idMessage ?>", <?= "'" .($index%2 ? "even" : "odd") . "'" ?>);
        $(".close.delete-file").tooltip({
            'title'     : '<?=Yii::t('standard', '_DELETE_FILE')?>',
            'trigger'   : 'hover',
            'placement' : 'right',
            'delay'     :
                { 'hide' : 200 }
        });
        $(".file-container .file-name").tooltip({
            'title'     : '<?=Yii::t('forum', 'Download file')?>',
            'trigger'   : 'hover',
            'placement' : 'left',
            'delay'     :
                {   'hide' : 200,
                    'show' : 100}
        });

            var deleteFile = $("span.close.delete-file");
            deleteFile.off('click');

            deleteFile.on('click', function(){
                var fileId = $(this).data('id'),
                    messageId = $(this).data('message'),
                    /*=- Initialize Dialog2 delete confirmation -=*/
                    xTitle = Yii.t('standard', '_DELETE_FILE'),
                    xUrl = HTTP_HOST + 'index.php?r=forum/messages/removeFile&thread_id=' + <?=$data->idThread?>,
                    xId = 'delete-uploaded-file-dialog',
                    xDialogClass = 'delete-uploaded-file-dialog',
                    xAjaxType = 'post',
                    xData = {
                        'fileId'    : fileId,
                        'idMessage' : messageId
                    };
                //Open the dialog
                openDialog( null, xTitle, xUrl, xId, xDialogClass, xAjaxType, xData );
                // The dialog is closing in the view --> (delete_file)
            });
    });
</script>
