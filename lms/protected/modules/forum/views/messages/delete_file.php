<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'delete_file_form',
        'htmlOptions' => array('class' => 'ajax')
    ));	?>

    <p><?= Yii::t('standard', '_DELETE_FILE') ?></p>

    <div class="clearfix">
        <?php echo CHtml::checkbox('confirm', false, array('id' => $checkboxIdKey, 'onchange' => $onChange)); ?>
        <?php echo CHtml::label(Yii::t('standard','Yes, I confirm I want to proceed'), $checkboxIdKey, array('style' => 'display: inline-block; padding-left: 10px;')); ?>
    </div>

    <div class="form-actions">
        <?= CHtml::button(Yii::t('standard', '_CONFIRM'), array('class' => 'btn btn-docebo green big btn-submit disabled')); ?>
        <?= CHtml::button(Yii::t('standard', '_CLOSE'), array('class' => 'btn btn-docebo black big close-dialog')); ?>
    </div>


    <?php echo CHtml::hiddenField('fileId', $fileId); ?>
    <?php echo CHtml::hiddenField('idMessage', $idMessage); ?>
    <?php $this->endWidget(); ?>
</div>

<script type="text/javascript">

    $('input').styler();

    function hideConfirmButton() {
        var confirmButton = $('.modal.delete-uploaded-file-dialog').find('.btn-submit');
        confirmButton.addClass('disabled');
        return true;
    }

    function showConfirmButton() {
        var confirmButton = $('.modal.delete-uploaded-file-dialog').find('.btn-submit');
        confirmButton.removeClass('disabled');
        return true;
    }

    $('.btn-submit').on('click', function() {
        if(!($('.btn-submit').hasClass('disabled'))) {
            $( "#delete_file_form" ).submit();
            $('#delete-uploaded-file-dialog').dialog2('close');
           var fileContainer = $(document).find(".file-name[data-id='<?=$fileId?>']").parent('p.file-container');
            if ( fileContainer.length )
                 fileContainer.remove();
        }
    });



</script>