<?php
	$this->breadcrumbs[Yii::t('menu_over', '_MYCOURSES')] = Docebo::createLmsUrl('site/index');

	if($this->course_id)
	{
		$course = LearningCourse::model()->findByPk($this->course_id);
		$this->breadcrumbs[$course->name] = $this->createUrl('//player', array('course_id' => $this->course_id));
	}

	if ($this->userCanAdminCourse)
	$this->breadcrumbs[Yii::t('standard', '_FORUM')] = $this->createUrl('//forum/default/courseForums', array('course_id' => $forum->idCourse));

	$this->breadcrumbs[$forum->title] = $this->createUrl('/forum/thread/index', array('forum_id' => $forum->idForum));
	$this->breadcrumbs[] = $thread->title;

	$createReplyUrl = $this->createUrl('/forum/messages/axReply', array("thread_id" => $thread->idThread));
?>

<ul id="docebo-bootcamp-thread" class="docebo-bootcamp clearfix">
	<? if ($this->_canReplyMsg()) : ?>
		<li>
			<a href="javascript:;" onclick="Forum.manageItem(event, '<?=$createReplyUrl?>')" data-helper-title="<?= Yii::t('standard', '_REPLY') ?>" data-helper-text="<?= Yii::t('helper', 'Reply') ?>">
				<span class="i-sprite is-reply large"></span>
				<span class="i-sprite is-reply large white"></span>
				<h4><?= Yii::t('standard', '_REPLY') ?></h4>
			</a>
		</li>
	<? endif; ?>

	<? if ($this->_canModerateThread()) : ?>
		<li style="display: <?=!$thread->locked ? 'none' : 'block'?>" id="unlock-button">
			<a href="<?=Yii::app()->createUrl("/forum/thread/axToggleLock", array("thread_id"=>$thread->primaryKey, "course_id" => $forum->idCourse))?>"
			   onclick="Forum.confirmDialog('unlock', Forum.changeItemStatus, this.href, '<?=Yii::t("forum", "_FREETHREAD")?>'); return false;" data-helper-title="<?= Yii::t('forum', '_FREETHREAD') ?>" data-helper-text="<?= Yii::t('helper', 'Disable comments') ?>">
				<span class="i-sprite is-lock large"></span>
				<span class="i-sprite is-lock large white"></span>
				<h4><?= Yii::t('forum', '_FREETHREAD') ?></h4>
			</a>
		</li>
		<li style="display: <?=$thread->locked ? 'none' : 'block'?>" id="lock-button">
			<a href="<?=Yii::app()->createUrl("/forum/thread/axToggleLock", array("thread_id"=>$thread->primaryKey, "course_id" => $forum->idCourse))?>"
			   onclick="Forum.confirmDialog('lock', Forum.changeItemStatus, this.href, '<?=Yii::t("forum", "Lock this thread?")?>'); return false;" data-helper-title="<?= Yii::t('forum', '_LOCKTHREAD') ?>" data-helper-text="<?= Yii::t('helper', 'Disable comments') ?>">
				<span class="i-sprite is-lock large"></span>
				<span class="i-sprite is-lock large white"></span>
				<h4><?= Yii::t('forum', '_LOCKTHREAD') ?></h4>
			</a>
		</li>
		<li style="display: <?=$thread->important ? 'none' : 'block'?>" id="important-button">
			<a href="<?=Yii::app()->createUrl("/forum/thread/axToggleImportant", array("thread_id"=>$thread->primaryKey, "course_id" => $forum->idCourse))?>"
			   onclick="Forum.confirmDialog('important', Forum.changeItemStatus, this.href, '<?=Yii::t("forum", "_IMPORTANT_THREAD")?>'); return false;" data-helper-title="<?= Yii::t('standard', '_MARK_AS_IMPORTANT') ?>" data-helper-text="<?= Yii::t('helper', 'Flag the discussion as important') ?>">
				<span class="i-sprite is-solid-exclam large"></span>
				<span class="i-sprite is-solid-exclam large white"></span>
				<h4><?= Yii::t('standard', '_MARK_AS_IMPORTANT') ?></h4>
			</a>
		</li>
		<li style="display: <?=!$thread->important ? 'none' : 'block'?>" id="unimportant-button">
			<a href="<?=Yii::app()->createUrl("/forum/thread/axToggleImportant", array("thread_id"=>$thread->primaryKey, "course_id" => $forum->idCourse))?>"
			   onclick="Forum.confirmDialog('unimportant', Forum.changeItemStatus, this.href, '<?=Yii::t("forum", "_IMPORTANT_THREAD")?>'); return false;" data-helper-title="<?= Yii::t('forum', '_SET_NOT_IMPORTANT_THREAD') ?>" data-helper-text="<?= Yii::t('helper', 'Flag the discussion as important') ?>">
				<span class="i-sprite is-solid-exclam large"></span>
				<span class="i-sprite is-solid-exclam large white"></span>
				<h4><?= Yii::t('forum', '_SET_NOT_IMPORTANT_THREAD') ?></h4>
			</a>
		</li>
		<li style="display: <?=!$thread->disabled ? 'none' : 'block'?>" id="enable-button">
			<a href="<?=Yii::app()->createUrl("/forum/thread/axToggleAccess", array("thread_id"=>$thread->primaryKey, "course_id" => $forum->idCourse))?>"
			   onclick="Forum.confirmDialog('enable', Forum.changeItemStatus, this.href, '<?=Yii::t("forum", "_UNERASE")?>'); return false;" data-helper-title="<?= Yii::t("forum", "_UNERASE") ?>" data-helper-text="<?= Yii::t('helper', 'Disable access') ?>">
				<span class="i-sprite is-nopass red large"></span>
				<span class="i-sprite is-nopass large white"></span>
				<h4><?= Yii::t('forum', '_UNERASE') ?></h4>
			</a>
		</li>
		<li style="display: <?=$thread->disabled ? 'none' : 'block'?>" id="disable-button">
			<a href="<?=Yii::app()->createUrl("/forum/thread/axToggleAccess", array("thread_id"=>$thread->primaryKey, "course_id" => $forum->idCourse))?>"
			   onclick="Forum.confirmDialog('disable', Forum.changeItemStatus, this.href, '<?=Yii::t('standard', '_MODERATE')?>'); return false;" data-helper-title="<?= Yii::t('standard', '_MODERATE') ?>" data-helper-text="<?= Yii::t('helper', 'Disable access') ?>">
				<span class="i-sprite is-nopass large"></span>
				<span class="i-sprite is-nopass large white"></span>
				<h4><?= Yii::t('standard', '_MODERATE') ?></h4>
			</a>
		</li>
	<? endif; ?>


	<li class="helper">
		<a href="#">
			<span class="i-sprite is-circle-quest large"></span>
			<h4 class="helper-title"></h4>
			<p class="helper-text"></p>
		</a>
	</li>

</ul>

<div class="clearfix"></div>

<div id="forum-inline-edit-panel" class="inline-edit-panel" style="display: none;"></div>

<div class="row-fluid">
	<div class="grid-nav-grey-box">
        <form id="searchMessagesForm" class="grid-search-form" onsubmit="Forum.searchMessages();return false;">
            <div class="span6">
				<div class="select-wrapper orderForumMessages">
					<label for="LearningForummessage_order_by"><?= Yii::t('social', 'Sort by'); ?></label>
					<?php echo CHtml::activeDropDownList($message, 'order_by', array('asc' => Yii::t('forum', 'Post date (ascending)'), 'desc' => Yii::t('forum', 'Post date (descending)') ), array('onchange'=>"Forum.searchMessages()")); ?>
				</div>
            </div>
            <div class="span6">
                <div class="seachBy">
                    <?=CHtml::button("searchSubmit", array('value'=>'', 'onclick'=>'Forum.searchMessages()'));?>
                    <?=CHtml::activeTextField($message, 'search_input');?>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
    $this->widget('common.widgets.Forum', array(
        'dataProvider'=>$message->search(),
        'itemView'=>'_post',
        'id' => 'forum-messages',
        'afterAjaxUpdate' => 'function(id, data){manageWidgets(id,data);}'
    ));
?>

<script type="text/javascript">
<!--
$(document).ready(function(){
	$("#docebo-bootcamp-thread").doceboBootcamp({});
});

    function manageWidgets(id,data){
        $(".rating-container").each(function(i,e){
            var isReadonly = $(this).data("readonly") ? true : false;
            var id = $(this).attr("id").replace(/[^\d]/g, '');;
            var options = {readOnly: isReadonly, callback: function(value, link) {
                var data = {};
                data.value = value;
                var url = '<?php echo Yii::app()->controller->createUrl('default/axVoteTone', array('course_id' => $forum->idCourse,'message_id' => '')) ?>' + id;
                $.post(url, data, function(result){
                      var data = JSON.parse(result);
                      $("span[data-idcourse=\"\"]").html(data.stars);
                      $("span[data-idcourse=\"\"]").closest("div").find(".rating-value").html(data.rating);
                });
            }}
            jQuery('#'+$(this).attr("id")+' > input').rating(options);
        });

        $(".docebo-helpful").each(function(i,e){
            var parent  = $(this);
            $(this).find('a').click(function(e) {
                e.preventDefault();
                var that = $(this);
                if (that.parent().hasClass('readonly') || that.hasClass('my-vote'))
                    return;

                $.post($(this).attr('href'), function(data) {
                    parent.find('a').removeClass('my-vote');
                    parent.find('.thumbs-up .count').html(data.likes);
                    parent.find('.thumbs-down .count').html(data.dislikes);
                    that.addClass('my-vote');
                }, 'json');
            });
        });
    }
//-->
</script>