<div class="forum-update-forum">

	<div class="header">
		<?= Yii::t('standard', '_REPLY'); ?>
	</div>
	<div class="content">

		<?php
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'manage-item-form',
			'enableAjaxValidation' => false,
			'htmlOptions' => array(
				'class'     => 'form-horizontal',
				'enctype'   => 'multipart/form-data'
			),
		));
		?>

		<?php echo $form->textArea($model, 'textof', array('rows' => 5, 'class' => 'input-xlarge', 'id' => 'message-body')); ?>
		<div id="word-count" style="text-align: right; color: #666; margin: 10px 0px 20px 0px;"></div>
		<?php if(!(isset($forumParams['forum_upload']) && $forumParams['forum_upload'] == 1)) {  ?>
		<br />
		<?php }  ?>
		<div class="clearfix"></div>

		<?php
		$event = new DEvent($this, array('message' => $model, 'thread_id' => $thread_id));
		Yii::app()->event->raise('onForumMessageForm', $event);
		?>

		<div class="buttons">
			<?php if(isset($forumParams['forum_upload']) && $forumParams['forum_upload'] == 1) {  ?>
				<div style="float: left" class="upload-button-area">
					<div id="container">
						<div style="float: left">
							<a id="pickfiles" style="float: left" class="pickfiles confirm-btn btn-docebo green big" href="javascript:void(0);"><?php echo Yii::t('organization', 'Upload File'); ?></a>
							<br />
							<span><?php echo Yii::t('forum', 'Max. file size {size} MB', array('{size}' => LearningForummessage::model()->file_maxsize)); ?></span>
						</div>

						<div style="float: left">

							<div id="filelist">
								<?php if($model->original_filename) { ?>
									<div id="0" class="file">
										<span class="file-icon"></span>
										<span class="filename"><?= $model->original_filename; ?></span>
										<input type="hidden" name="LearningForummessage[file][name]" value="<?= $model->original_filename; ?>"/>
										<input type="hidden" name="LearningForummessage[file][path]" value="<?= $model->path; ?>"/>
										<span class="close">&times;</span>
									</div>
								<?php } ?>
								<div id="fileserror"></div>
							</div>
						</div>


					</div>
				</div>
			<?php } ?>

		</div>
		<div class="buttons">
			<? if ($model->isNewRecord) : ?>
				<?= CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class' => 'btn-docebo green big', 'id' => 'submit-manage-item', 'onclick' => 'Forum.manageItem(event, "'.$this->createUrl('/forum/messages/axReply', array("thread_id" => $thread_id)).'"); return false;')); ?>
			<? else : ?>
				<?= CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class' => 'btn-docebo green big', 'id' => 'submit-manage-item', 'onclick' => 'Forum.manageItem(event, "'.$this->createUrl('/forum/messages/axUpdate', array("message_id" => $model->idMessage, "thread_id" => $thread_id)).'"); return false;')); ?>
			<? endif; ?>
			<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn-docebo black big', 'id' => 'cancel-submit-manage-item','onclick' => 'Forum.hideInlinePanel()')); ?>
		</div>

		<?php $this->endWidget(); ?>
	</div>
</div>

<?php $wordlimit = $this->getForumWordCount();?>
<?php if($wordlimit > 0): ?>
	<script type="text/javascript">
		(function() {

			var wordlimit = <?= $wordlimit ?>;

			if ($('#word-count')) {
				$('#word-count').html(<?= CJSON::encode(Yii::t('forum', 'Words left')) ?>+": " + wordlimit);
			}

			tinyMCE.on('AddEditor', function(e) {

				var editor = e.editor;

				if (editor.id == "message-body") {

					//if no limit to words, no need to apply events
					if (wordlimit <= 0) return;

					var submitBtn = $('#submit-manage-item');
					//disable send message button
					var disableMessageSubmitButton = function() {
						submitBtn.addClass('disabled');
						submitBtn.prop('disabled', true);
					};
					//enable send message buttons
					var enableMessageSubmitButton = function() {
						submitBtn.removeClass('disabled');
						submitBtn.prop('disabled', false);
					};

					//words counter function
					var countWords = function(text) { var orig = text;
						//do some text handling and formatting
						text = text.toLowerCase(); //make sure all entities/tags/etc. can be recognized
						text = text.replace(/<(\/(p|ul|ol|li)|br[\s]*\/)>/g, ' '); //some tags are word separators and cannot just be removed, change them in spaces instead
						text = text.replace( /(<([^>]*)>)/g, ''); //remove remaining html tags
						text = text.replace(/[\n\r\s]+/g, ' '); //remove carriages, newlines etc.
						text = text.replace('&nbsp;', ' '); //handle html space entities
						text = text.replace(/^\s\s*/, ''); //trimming left
						text = text.replace(/\s\s*$/, ''); //trimming right
						text = text.replace(/[\s]+/g, ' '); //normalize spaces between words
						//final words count
						if (text == '') return 0;
						return text.split(' ').length;
					};

					//update displayed left words counter
					var updateWordsCount = function(count) {
						if ($('#word-count')) {
							$('#word-count').html(<?= CJSON::encode(Yii::t('forum', 'Words left')) ?>+": "+count);
						}
					}

					//update editor content event handler
					var updateContentHandler = function(e) {
						var text = editor.getContent({format:"text"});
						var wordcount = countWords(text);
						var wordleft = wordlimit - wordcount;
						updateWordsCount(wordleft);
						if (wordleft < 0) {
							disableMessageSubmitButton();
						} else {
							enableMessageSubmitButton();
						}
					};

					//handle content modifiying events
					editor.on('keyup',updateContentHandler);
					editor.on('cut', updateContentHandler);
					editor.on('paste', updateContentHandler);
					editor.on('setContent', updateContentHandler);
					editor.on('change', updateContentHandler);

				}
			});

		})();


	</script>
<?php endif; ?>

<?php if(isset($forumParams['forum_upload']) && $forumParams['forum_upload'] == 1) {  ?>
<script type="text/javascript">
	// Start file upload part
	$(function() {

		var allFilesSize = 0; //Current files size;
		var maxFilesSize = <?= LearningForummessage::model()->file_maxsize; ?>*1024*1024; //10MB
		var upUrl =  yii.urls.base + '/index.php?r=forum/messages/handleFile&thread_id=<?= $thread_id; ?>';

		var uploader = new plupload.Uploader({
			chunk_size: '<?= LearningForummessage::model()->file_maxsize; ?>mb',
			runtimes: 'html5,flash,html4,gears,silverlight,browserplus',
			browse_button: 'pickfiles',
			container: 'container',
			max_file_size: '<?= LearningForummessage::model()->file_maxsize; ?>mb',
			url : upUrl,
			flash_swf_url: yii.urls.base + '/../common/extensions/plupload/assets/plupload.flash.swf',
			silverlight_xap_url: '/plupload/js/plupload.silverlight.xap',
			multipart_params: {
				fileId: '',
				<?php echo Yii::app()->request->csrfTokenName; ?>: '<?php echo Yii::app()->request->csrfToken; ?>',
			}
		});

		uploader.bind('Init', function (up, params) {
			//$('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
		});

		uploader.init();

		uploader.bind('BeforeUpload', function (up, files) {
		});

		uploader.bind('FilesAdded', function (up, files) {

			$('#fileserror').html("");
			$.each(files, function (i, file) {

				//Calculate new total files size!
				allFilesSize += file.size;

				//If we outweigh the files limits!
				if(allFilesSize > maxFilesSize){
					$('#fileserror').append([
						'<div>Error: <?=Yii::t('forum', 'Max. file size {size} MB', array('{size}' => LearningForummessage::model()->file_maxsize));?></div>'
					].join(''));

					//Remove the uploaded file and change the files size!
					uploader.removeFile(file);
					allFilesSize -= file.size;
				} else{
					var fileSize = '';
					if (file.size != undefined) {
						fileSize = '(' + plupload.formatSize(file.size) + ')';
					}

					// Add file id as additional param in order to be used for temp file name(it is an unique hash)
					uploader.settings.multipart_params.fileId = file.id;

					$('#filelist').html([
						'<div id="' + file.id + '" class="file">',
						'<span class="file-icon"></span>',
						'<span class="filename">' + file.name + '</span>',
						'<input type="hidden" name="LearningForummessage[file][name]" value="'+file.name+'"/>',
						'<input type="hidden" name="LearningForummessage[file][path]" value="'+file.id+'"/>',
						'<span class="close">&times;</span></div>',
						'<div id="fileserror"></div>'
					].join(''));

					$('#filelist .filename').prepend('<i class="fa fa-refresh fa-spin"></i>&nbsp;');
				}
			});

			up.refresh(); // Reposition Flash/Silverlight

			uploader.start();
		});

		uploader.bind('UploadProgress', function (up, file) {
		});

		uploader.bind('Error', function (up, err) {
			Docebo.Feedback.show('error', Yii.t('app7020', 'Error') + ': ' + err.message);

			up.refresh(); // Reposition Flash/Silverlight
		});

		uploader.bind('FileUploaded', function (up, file, response) {
			try {
				response = jQuery.parseJSON(response.response);

				if (response.error.code == '500'){

					$('#filelist').html(''); // Clear the file uploaded data

					Docebo.Feedback.show('error', Yii.t('app7020', 'Error') + ': ' + response.error.message);

					file.status = plupload.FAILED;
				}
				else {
					// Success
					file.status = plupload.DONE;
				}
			}
			catch (e) {
				Docebo.log('Code Error: ' + e);
			}

			$('#filelist .filename .fa-refresh').remove();
		});

		//Event for removing a file - remove file and change the files size!
		$(document.body).off('click');
		$(document.body).on('click', 'span.close', function(e) {
			e.preventDefault();

			$('#filelist').html('');

			if(typeof uploader !== "undefined") {
				var id = $(this).closest("div.file").attr('id');
				if(typeof id !== "undefined") {
					uploader.removeFile(uploader.getFile(id));
				}
			}

			allFilesSize = 0;
		});
	});
</script>
<?php } ?>

<script type="text/javascript">
	$(function() {

		// Disable submit buttons on clicking them to prevent several posts at time
		$('#cancel-submit-manage-item').on('click', function() {
			$('#cancel-submit-manage-item').attr('disabled', true);
			$('#submit-manage-item').attr('disabled', true);
		});

		$('#submit-manage-item').on('click', function() {
			$('#cancel-submit-manage-item').attr('disabled', true);
			$('#submit-manage-item').attr('disabled', true);
		});

		// Enable form actions(buttons) on showing a feedback !!!
		$('#dcb-feedback').on('docebo-feedback-shown', function(e) {
			$('#cancel-submit-manage-item').attr('disabled', false);
			$('#submit-manage-item').attr('disabled', false);
		});
	});
</script>
