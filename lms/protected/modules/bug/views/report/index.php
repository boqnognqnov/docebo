<?php $this->breadcrumbs = array(
	Yii::t('bug_report', '_BUG_REPORT'),
); ?>
<div class="row-fluid">
	<div class="span6">
<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'bug-report-form',
	'enableAjaxValidation' => false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
));
?>
<div class="yiiForm form_fullw form-inline">
	<?php echo $form->errorSummary($model); ?>

	<div class="simple">
		<?php echo $form->labelEx($model, 'title'); ?>
		<?php echo $form->textField($model, 'title', array('style' => 'width: 220px;')); ?>
		<?php echo $form->error($model, 'title'); ?>
	</div>

	<div class="simple">
		<?php echo $form->labelEx($model, 'description'); ?>
		<?php echo $form->textArea($model, 'description', array('style' => 'width: 220px; height: 150px')); ?>
		<?php echo $form->error($model, 'description'); ?>
	</div>

	<div class="simple">
		<?php echo $form->labelEx($model, 'file'); ?>
		<?php echo $form->fileField($model, 'file', array('style' => 'width: 220px;')); ?>
		<?php echo $form->error($model, 'file'); ?>
	</div>

    <?php if (count($types) > 1) : ?>
	<div class="simple">
		<?php echo $form->labelEx($model, 'type'); ?>
		<?php echo $form->radioButtonList($model, 'type', $types); ?>
		<?php echo $form->error($model, 'type'); ?>
	</div>
    <?php elseif (count($types) == 1) : ?>
	<div class="simple">
		<?php echo $form->labelEx($model, 'type'); ?>
        <span>
            <?php echo current($types); ?>
        </span>
		<?php echo $form->hiddenField($model, 'type'); ?>
		<?php echo $form->error($model, 'type'); ?>
	</div>
    <?php endif; ?>
    <?php if (count($projects) > 1) : ?>
	<div class="simple">
		<?php echo $form->labelEx($model, 'project'); ?>
		<?php echo $form->dropDownList($model, 'project', $projects); ?>
		<?php echo $form->error($model, 'project'); ?>
	</div>
    <?php elseif (count($projects) == 1) : ?>
	<div class="simple">
		<?php echo $form->labelEx($model, 'project'); ?>
        <span>
            <?php echo current($projects); ?>
        </span>
		<?php echo $form->hiddenField($model, 'project'); ?>
		<?php echo $form->error($model, 'project'); ?>
	</div>
    <?php endif; ?>
	<div style="padding: 30px 0 0 22px;" class="action">
		<input type="submit" value="Send" class="btn-docebo green big">
	</div>
</div>
<?php $this->endWidget(); ?></div>
	<div class="span6 pull-right">
		<table class="table">
			<thead>
				<tr>
					<th>Last 10 reports</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lastBugs as $bug) : ?>
				<tr>
					<td style="text-indent: 10px; cursor: pointer;" id="bug_<?php echo $bug->getPrimaryKey() ?>">
						<?php echo $bug->title ?>
						<span style="display: none;"><?php echo $bug->description ?></span>
					</td>
				</tr>
				<script type="text/javascript">
					$(document).ready(function(){
						$('#bug_<?php echo $bug->getPrimaryKey() ?>').popover({
							html: true,
							content: $('#bug_<?php echo $bug->getPrimaryKey() ?> span').html(),
							trigger: 'hover',
							placement: 'left',
							title: null
						})
					});
				</script>
				<?php endforeach; ?>
		</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input,select').styler();
        });
    });
</script>