<?php $this->breadcrumbs = array(
	Yii::t('bug_report', '_BUG_REPORT'),
); ?>
<br/>
<div class="alert alert-error">
    <?php echo Yii::t('bug_report', '_BUG_REPORT_ERROR') ?>
    <?php if ($_GET['msg']) : ?>
    <p>
        <br>
        <strong><?php echo Yii::t('bug_report', '_BUG_REPORT_PIVOTAL_REQUEST') ?>:</strong> <i><?php echo $_GET['msg'] ?></i>
    </p>
    <?php endif; ?>
</div>