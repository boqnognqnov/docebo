<?php

class ReportController extends BugBaseController {

    /**
     * (non-PHPdoc)
     * @see CController::filters()
     */
    public function filters() {
        return array(
            'accessControl',
        );
    }

    /**
     * (non-PHPdoc)
     * @see CController::accessRules()
     */
    public function accessRules() {

        return array(
            array('allow',
                'users' => array('@'),
                'expression' => 'Yii::app()->user->isGodAdmin',
            ),

			array('deny',
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
			),
        );
    }

    /**
     *  Main action, though it does nothing really
     */
    public function actionIndex() {
        $types = CoreBug::getTypes();
        $projects = CoreBug::getProjectList();

		$model = new CoreBug('report');
		$model->status = 'pending';
		
        if (count($types) == 1) {
            $key = array_keys($types);
            $model->type = $key[0];
        }
        if (count($projects) == 1) {
            $key = array_keys($projects);
            $model->project = $key[0];
        }
		$model->created_at = Yii::app()->localtime->toLocalDateTime();
		if(isset($_POST['CoreBug']))
		{
			$model->attributes = $_POST['CoreBug'];
			$model->file = CUploadedFile::getInstance($model,'file');
			if($model->save()) {
				if ($model->file)
					$model->file->saveAs(Yii::app()->basePath.Yii::app()->params['bug_report']['folder'].$model->getPrimaryKey().'_'.$model->file->getName());
                
                $res = $model->sendToPivotal();
                
				if ($res === true) {
					$model->save();
					$this->redirect(Yii::app()->createUrl('bug/report/success'));
				} elseif ($res === false)
					$this->redirect(Yii::app()->createUrl('bug/report/error'));
				else
					$this->redirect(Yii::app()->createUrl('bug/report/error&msg='.  urlencode($res)));
			}
		}
		
		$bugs = CoreBug::model()->findAll(array(
				'order' => 't.created_at DESC',
				'limit' => 10,
			));
		
		$this->render( 'index', array(
			'model' => $model,
			'lastBugs' => $bugs,
            'types' => $types,
            'projects' => $projects,
		));
	}

	public function actionSuccess() {
		$this->render('success');
	}

	public function actionError() {
		$this->render('error');
	}
}