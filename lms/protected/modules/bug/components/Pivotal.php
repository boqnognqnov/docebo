<?php

/**
 * @package    Pivotal
 * @author     Valentin Hristov
 */
class Pivotal {

	protected static $_instance;
	protected $_url_retrieve_user_token = 'https://www.pivotaltracker.com/services/v3/tokens/active';
	protected $_isConnected = false;
	protected $_username;
	protected $_password;
	protected $token;
	protected $project;

    public $error;

	public static function getInstance() {
		if (self::$_instance === NULL)
			self::$_instance = new self();
		return self::$_instance;
	}

	public function __construct() {
		$this->_username = escapeshellcmd(Yii::app()->params['bug_report']['usr']);
		$this->_password = escapeshellcmd(Yii::app()->params['bug_report']['pwd']);
		$this->_connect();
	}

	protected function _connect() {
		$ch = curl_init ();
		curl_setopt($ch,CURLOPT_URL, "https://www.pivotaltracker.com/services/v3/tokens/active");
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_USERPWD,$this->_username.":".$this->_password);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10); //times out after 10s
		@curl_setopt($ch,CURLOPT_FOLLOWLOCATION,1);

		$result = curl_exec ($ch);
        $res = false;

        try {
            $res = @new SimpleXMLElement($result);
        } catch (Exception $e) {
            $this->error = $result;
        }

        if ($res)
            $this->token = $res->guid;
	}

	public function isError() {
		return $this->error ? true : false;
	}

	public function getError() {
		return $this->error;
	}

	public function setProject($project) {
		$this->project = $project;
		return $this;
	}

	public function getProjects() {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www.pivotaltracker.com/services/v3/projects");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-TrackerToken: {$this->token}"));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10); //times out after 10s
		@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

		$xml = curl_exec($ch);

		$projects = new SimpleXMLElement($xml);
		return $projects;
	}

	public function addStory($type, $name, $desc) {
		$desc = htmlentities($desc);

		$type = htmlentities($type);
		$name = htmlentities($name);

		$desc = "[".date('Y-m-d H:i')."] ".Yii::app()->user->loadUserModel()->email.": \n\n".$desc;

		$body = "<story>"
			. "<story_type>{$type}</story_type>"
			. "<name>{$name}</name>"
			. "<description>{$desc}</description>"
			. "</story>";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www.pivotaltracker.com/services/v3/projects/{$this->project}/stories");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/xml","X-TrackerToken: {$this->token}"));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10); //times out after 10s
		@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

		$xml = curl_exec($ch);

		$story = new SimpleXMLElement($xml);
		return $story;
	}

	public function addStoryAttachment($storyId, $file_path) {
//		$file_path = $file_path;
		$storyId = intval($storyId);

		$filename = basename($file_path);
		$file_type = mime_content_type($file_path);

		$size = filesize($file_path);
        $file = fopen($file_path,'r');
        $data = array('Filedata' => "@".$file_path);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://www.pivotaltracker.com/services/v3/projects/{$this->project}/stories/{$storyId}/attachments");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			"X-TrackerToken: {$this->token}",
		));
		curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		curl_setopt($ch, CURLOPT_INFILESIZE, $size);
        // load the file in by its resource handle
        curl_setopt($ch, CURLOPT_INFILE, $file);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10); //times out after 10s
		@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

		$xml = curl_exec($ch);

		$story = new SimpleXMLElement($xml);
		return $story;
	}

	public function addTask($story, $desc) {
		$desc = htmlentities($desc);

		$story = escapeshellcmd($story);

		$body = "<task><description>{$desc}</description></task>";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www.pivotaltracker.com/services/v3/projects/{$this->project}/stories/{$story}/tasks");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/xml","X-TrackerToken: {$this->token}"));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10); //times out after 10s
		@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

		$xml = curl_exec($ch);
	}

	public function addLabels($story, $labels) {

		$story = escapeshellcmd($story);
		$labels = escapeshellcmd($labels);

		$body = "<task><labels>{$labels}</labels></task>";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www.pivotaltracker.com/services/v3/projects/{$this->project}/stories/{$story}");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/xml","X-TrackerToken: {$this->token}"));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10); //times out after 10s
		@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

		$xml = curl_exec($ch);
	}

	public function getStories($project, $filter = '') {
		$filter = urlencode($filter);

		$filter = escapeshellcmd($filter);
		$project = escapeshellcmd($project);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www.pivotaltracker.com/services/v3/projects/{$project}/stories".($filter != '' ? "?filter=$filter" : null));
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/xml","X-TrackerToken: {$this->token}"));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10); //times out after 10s
		@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

		$xml = curl_exec($ch);

		$story = new SimpleXMLElement($xml);
		return $story;
	}

}