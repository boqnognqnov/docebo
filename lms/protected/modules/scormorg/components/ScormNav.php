<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */


/**
 * Class ScormNav
 * The main scope of this calss is to manage nnavigation between the sco, it will help in the retrivial of a link
 * to launch a specific sco or the link to launche the next one
 */
class ScormNav extends CComponent {

	protected $close_url = '';

	/**
	 * Class constructor
	 */
	public function __construct() {
		//$this->close_url = Yii::app()->createAbsoluteUrl(array('r' => 'scormorg/default/closePlayer'));
		$this->close_url = Yii::app()->createAbsoluteUrl('scormorg/default/closePlayer');
	}


	protected function buildScoInfo(LearningScormItems $scorm_item, $object, $id_user, $launch_type = false) {

		$id_scorm_item = $scorm_item->idscorm_item;
		$id_resource = $scorm_item->idscorm_resource;


		// retrive info about this chapter play
		$scorm_organization = LearningScormOrganizations::model()->findByPk($object->idResource);
		if (!$scorm_organization) {
			throw new CException('Invalid Scorm organization: ' . $object->idResource);
		}
		$scorm_package = LearningScormPackage::model()->findByPk($scorm_organization->idscorm_package);
		if (!$scorm_package) {
			throw new CException('Invalid Scorm package: ' . $scorm_organization->idscorm_package);
		}
		$scorm_resource = LearningScormResources::model()->findByPk($id_resource);
		if (!$scorm_resource) {
			throw new CException('Invalid Scorm resource: ' . $id_resource);
		}

		// @TODO  This ASSUMES that SCORMs are always CDN based. Abstract please :-)

		// Compose object url, to do this we need to differentiate marketplace objects
		$course = LearningCourse::model()->findByPk($object->idCourse);
		if ($course->mpidCourse > 0) {
			$storageManager = CFileStorage::getMarketplaceStorage(CFileStorage::COLLECTION_MP_SCORM, $scorm_package->path);
			$launch_url = $storageManager->getHostUrl()
				. ( $launch_type == 'fullscreen'
					? Yii::app()->params['scorm']['launcher_fullscreen']
					: Yii::app()->params['scorm']['launcher']
				);
		} else {
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_SCORM);
			$launch_url = $storageManager->getHostUrl()
				. ( $launch_type == 'fullscreen'
					? Yii::app()->params['scorm']['launcher_fullscreen']
					: Yii::app()->params['scorm']['launcher']
				);
		}

		$host = trim(preg_replace('/http[s]*:\/\//i', '', Yii::app()->request->getHostInfo()), '/');

		$auth = new AuthToken($id_user);
		$auth_token = $auth->get(true);

		$sco_url = $launch_url
			. "?host=".urlencode($host)
			. "&id_user=".urlencode($id_user)
			. "&id_reference=".urlencode($object->idOrg)
			. "&scorm_version=".urlencode($scorm_package->scormVersion ? $scorm_package->scormVersion : '1.2')
			. "&id_resource=".urlencode($id_resource)
			. "&id_item=".urlencode($id_scorm_item)
			. "&idscorm_organization=".urlencode($object->idResource)
			. "&id_package=".urlencode($scorm_organization->idscorm_package);
			// . "&auth_code=".urlencode($auth_token);

        //Adds course_id to the url string on every step of the scorm (in fullscreen!)
//		if($object->idCourse  && $object->idCourse!= "undefined"){
//			$sco_url .= '&id_course='.urlencode($object->idCourse);
//		}

		if (Settings::get('scorm_debug', 'off') == 'on') $sco_url .= "&debug=".urlencode("1");

		if ($launch_type) $sco_url .= '&launch_type=' . $launch_type;

		$data = array(
			'sco_url' => $sco_url,
			'launch_url' => $launch_url,
			'host' => $host,
			'id_user' => $id_user,
			'id_reference' => $object->idOrg,
			'scorm_version' => ($scorm_package->scormVersion ? $scorm_package->scormVersion : '1.2'),
			'id_resource' => $id_resource,
			'id_item' => $id_scorm_item,
			'idscorm_organization' => $object->idResource,
			'id_package' => $scorm_organization->idscorm_package,
			// 'auth' => $auth_token,
			'debug' => ( Settings::get('scorm_debug', 'off') == 'on' ? true : false )
		);
		return $data;
	}


	/**
	 * @param int $id_org the id of the learning_organization that represent the object
	 * @param LearningScormItems $scorm_item the AR for the sco to play
	 * @param $id_user
	 * @param $launch_type
	 * @return array
	 * @throws CException
	 */
	public function getScoLaunchInfo($id_org, LearningScormItems $scorm_item, $id_user, $launch_type = false) {
		$object = LearningOrganization::model()->findByPk($id_org);
		if (!$object || $object->objectType != LearningOrganization::OBJECT_TYPE_SCORMORG)
			throw new CException('Invalid Scorm Learning Object: ' . $id_org);

		$data = $this->buildScoInfo($scorm_item, $object, $id_user, $launch_type);
		return $data;
	}


	/**
	 * Based on a certain item and the performance of the user in that item, this metod try to find the
	 * launch url for the next object, if not found, the close player url will be returned
	 * @param $id_org
	 * @param $id_user
	 * @param LearningScormItems $scorm_item
	 * @param ScormApiBase $scormapi
	 * @param string|bool $launch_type
	 * @throws CException
	 * @return string
	 */
	public function getNextToPlay($id_org, $id_user, LearningScormItems $scorm_item, ScormApiBase $scormapi, $launch_type = false) {

		// based on the scorm_item we have to find the next for this scorm, if it's not present, or
		// the autoplay is off, we have to return the link to close the player
		$url_next_to_play = $this->close_url;

		$object = LearningOrganization::model()->findByPk($id_org);
		if (!$object || $object->objectType != LearningOrganization::OBJECT_TYPE_SCORMORG) {
			throw new CException('Invalid Scorm Learning Object: ' . $id_org);
		}
		if ($object->params_json) {
			$param = CJSON::decode($object->params_json);
			// TODO: this is 0 by default in 6.0 but, well, that's not how it should be ...
			// to solve that now no is 'no', otherwise is considered yes
			if ($param['autoplay'] == LearningOrganization::AUTOPLAY_NO) return $this->close_url;
		}

		// retrive info of this scorm and the user performance
		$scorm_items = $scormapi->getUserScormData($id_user, $scorm_item);

		// go trough it to find the next one that it's not a folder
		$found = false;
		$next_to_play = false;
		$prev_scoes_status = array();
		foreach ($scorm_items as $sco) {

			// save current scoes status for later check
			$prev_scoes_status[$sco->item_identifier] = $sco->scormItemsTrack[0]->status;

			if ($found == true && $next_to_play == false) {
				// we have already found the current played but not the next to play, let's try to see if it's the current one
				// check that this has a resource associated that can be played
				if ($sco->idscorm_resource != false) {
					// now check for prerequisite
					if ($sco->adlcp_prerequisites != false) {

						$satisfied = true;
						$prerequisites = explode(',', $sco->adlcp_prerequisites);
						foreach ($prerequisites as $prerequisite) {

							if (empty($prev_scoes_status[$prerequisite])
								|| (
									$prev_scoes_status[$prerequisite] != 'completed'
									&&
									$prev_scoes_status[$prerequisite] != 'passed'
								)
							) {
								// the prereq doesn't exists or isn't passed
								$satisfied = false;
							}
						}

						if ($satisfied == true) {
							$next_to_play = $sco;
						}
					} else {
						$next_to_play = $sco;
					}
				}
			}
			if ($sco->idscorm_item == $scorm_item->idscorm_item) {
				// we have found the current one !
				$found = true;
			}
		}

		// build info to play the next one
		if($next_to_play) {
			$next_to_play_info = $this->buildScoInfo($next_to_play, $object, $id_user, $launch_type);
			return $next_to_play_info['sco_url'];
		}

		// nothing found, let's close the player
		return $this->close_url;
	}

	/**
	 * Based on a certain item and the performance of the user in that item, this metod try to find the
	 * launch url for the next object, if not found, the close player url will be returned
	 * @param $id_object
	 * @param $id_user
	 * @param LearningScormItems $scorm_item
	 * @param ScormApiBase $scormapi
	 * @throws CException
	 * @return string
	 */
	public function getNextToPreview($object, $id_user, LearningScormItems $scorm_item) {

		// based on the scorm_item we have to find the next for this scorm, if it's not present, or
		// the autoplay is off, we have to return the link to close the player
		$url_next_to_play = $this->close_url;

		// retrive info of this scorm and the user performance
		$scorm_items = ScormPreview::getUserScormData($scorm_item);

		// go trough it to find the next one that it's not a folder
		$found = false;
		$next_to_play = false;
		foreach ($scorm_items as $sco) {


			if ($found == true && $next_to_play == false) {
				// we have already found the current played but not the next to play, let's try to see if it's the current one
				// check that this has a resource associated that can be played
				if ($sco->idscorm_resource != false) {
					$next_to_play = $sco;
				}
			}
			if ($sco->idscorm_item == $scorm_item->idscorm_item) {
				// we have found the current one !
				$found = true;
			}
		}

		// build info to play the next one
		if($next_to_play) {

			$next_to_play_info = ScormPreview::buildScoInfo($next_to_play, $object, $id_user);
			return $next_to_play_info['sco_url'];
		}

		// nothing found, let's close the player
		return $this->close_url;
	}

}