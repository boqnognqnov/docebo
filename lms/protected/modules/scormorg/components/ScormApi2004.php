<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */

class ScormApi2004 extends ScormApiBase {


	/**
	 * Class constructor
	 */
	public function __construct($id_reference) {
		parent::__construct($id_reference);
	}


	/**
	 * Return the base cmi for a clean Sco start
	 * @param CoreUser $user
	 * @param LearningScormItems $scorm_item
	 * @return array
	 */
	public function getStarterCmi(CoreUser $user, LearningScormItems $scorm_item) {

		// this is the start cmi for a new cmi
		$data = array(
			'cmi._version'				=> '1.0',
			'cmi.completion_status'		=> 'unknown',
			'cmi.completion_threshold'	=> $scorm_item->adlcp_completionthreshold,
			'cmi.credit'				=> 'credit',
			'cmi.entry'					=> 'ab-initio',
			'cmi.launch_data'			=> $scorm_item->adlcp_datafromlms,
			'cmi.learner_id'			=> $user->getClearUserId(),
			'cmi.learner_name'			=> $this->getScoLearnerName($user),
			'cmi.mode'					=> 'normal',
			'cmi.time_limit_action'		=> $scorm_item->adlcp_timelimitaction,
			'cmi.total_time'			=> 'PT0H0M0S',

			// Preferences
			'cmi.learner_preference.language' => Yii::app()->getLanguage(),
		);
		if ($scorm_item->adlcp_maxtimeallowed) {
			$data['cmi.student_data.max_time_allowed'] = $scorm_item->adlcp_maxtimeallowed;
		}

		/*
		// fake data for now
		$data = array_merge($data, array(
			// adl.data
			'adl.data.0.id'			=> 'tarID1',
			'adl.data.1.id'			=> 'tarID2',
			'adl.data.2.id'			=> 'tarID3',
			'adl.data.3.id'			=> 'tarID4',

			// just for test
			'adl.nav.request_valid.continue' => 'unknown',
			'adl.nav.request_valid.previous' => 'unknown',
			'adl.nav.request_valid.choice.{target=activity_2}' => 'unknown',
			'adl.nav.request_valid.jump.{target=activity_2}' => 'unknown'
		));*/
		return $data;
	}


	/**
	 * This method sum two time in the 0000:00:00.00 (hhhh:mm:ss:msms) format
	 * @param $session_time the last session time
	 * @param $total_time the previously accumulated time
	 * @return mixed the new total_time
	 */
	public function newTotalTime($session_time, $total_time) {

		if($total_time == '' || $total_time == '0000:00:00.00') $total_time = 'P0000Y00M00DT0000H00M00S';

		$t1_s = array();
		$t2_s = array();
		$re1 = preg_match ('/^P((\d*)Y)?((\d*)M)?((\d*)D)?(T((\d*)H)?((\d*)M)?((\d*)(\.(\d{1,2}))?S)?)?$/', $session_time, $t1_s );
		if(strpos($total_time, "P") === false) {
			$a2 = explode(":", $total_time);
			$t2_s[9] = $a2[0];
			$t2_s[11] = $a2[1];
			$pos = strpos($a2[2], ".");
			if( $pos === FALSE ) {
				$t2_s[13] = $a2[2];
				$t2_s[15] = 0;
			} else {
				$t2_s[13] = substr($a2[2], 0, $pos);
				$t2_s[15] = substr($a2[2], $pos+1);
			}
			$re2 = true;
		}
		else $re2 = preg_match ('/^P((\d*)Y)?((\d*)M)?((\d*)D)?(T((\d*)H)?((\d*)M)?((\d*)(\.(\d{1,2}))?S)?)?$/', $total_time, $t2_s );

		if(!$re1 || !$re2) return $total_time;

		if(!isset($t1_s[15])) $t1_s[15] = 0;
		if(!isset($t1_s[13])) $t1_s[13] = 0;
		if(!isset($t1_s[11])) $t1_s[11] = 0;
		if(!isset($t1_s[9])) $t1_s[9] = 0;

		if(!isset($t2_s[15])) $t2_s[15] = 0;
		if(!isset($t2_s[13])) $t2_s[13] = 0;
		if(!isset($t2_s[11])) $t2_s[11] = 0;
		if(!isset($t2_s[9])) $t2_s[9] = 0;

		$tot['cent'] 	= $t1_s[15] + $t2_s[15];
		if($tot['cent'] >= 100) {
			$remainder = floor($tot['cent']/100);
			$tot['cent'] = floor($tot['cent'] % 100 );
		} else $remainder = 0;

		$tot['second'] 	= $t1_s[13] + $t2_s[13] + $remainder;
		if($tot['second'] >= 60) {
			$remainder = floor($tot['second']/60);
			$tot['second'] = floor($tot['second'] % 60 );
		} else $remainder = 0;

		$tot['minute'] 	= $t1_s[11] + $t2_s[11] + $remainder;
		if($tot['minute'] >= 60) {
			$remainder = floor($tot['minute']/60);
			$tot['minute'] = floor($tot['minute'] % 60 );
		} else $remainder = 0;

		$tot['hour'] 	= $t1_s[9] + $t2_s[9] + $remainder;

		return sprintf("PT%04uH%02uM%02uS", $tot['hour'], $tot['minute'], $tot['second'], $tot['cent']);
	}

	/**
	 * Should be the same as the scorm rte Initialize, it will initialize the cmi for that scorm for the user
	 * @param $id_user
	 * @param LearningScormItems $scorm_item
	 * @return array|bool
	 */
	public function Initialize($id_user, LearningScormItems $scorm_item) {

		// let's retrive the user info
		$user = CoreUser::model()->findByPk($id_user);
		if (!$user) return false;

		// check if we have the master tracking in scorm_items_tracking
		$scorm_item_track = LearningScormItemsTrack::model()->find('idscorm_item IS NULL AND idUser=:idUser AND idscorm_organization=:idscorm_organization', array(
			':idUser' => $id_user,
			':idscorm_organization' => $scorm_item->idscorm_organization
		));
		if (!$scorm_item_track) {
			// no master tracking, we have to create them
			$this->setupScormItemsTrack($id_user, $scorm_item->idscorm_organization);
			// ok we can now proceed with the specific sco tracking
		}

		// let's check if we have the data for this user
		$scorm_tracking = LearningScormTracking::model()->find('idUser=:idUser AND idscorm_item=:idscorm_item', array(
			':idUser' => $id_user,
			':idscorm_item' => $scorm_item->idscorm_item,
		));

		// if this item is an asset, at this point we can skip the cmi and set it as completed
		if ($scorm_item->scorm_resource->scormtype == 'asset') {

			// the first time we should mark it as completed, nothing to do the other times
			$scorm_items_track = LearningScormItemsTrack::model()->find('idUser=:idUser AND idscorm_item=:idscorm_item', array(
				':idUser' => $id_user,
				':idscorm_item' => $scorm_item->idscorm_item,
			));

			if ($scorm_items_track->status != LearningCommontrack::STATUS_COMPLETED) {

				$scorm_items_track->status 	= LearningCommontrack::STATUS_COMPLETED;
				$scorm_items_track->save();
				// like for a sco, propagate it's completion
				$this->propagateCompletion($id_user, $scorm_item, true);
			}

			// there is no cmi to return
			return array();
		}

		// this is a sco, so the full cmi management is needed
		$cmi = array();
		if ($scorm_tracking) {
			// see if the data are in xml from the old system
			if (!empty($scorm_tracking->xmldata) && $scorm_tracking->xmldata{0} == '<') {
				// reparse xml data into array
				$cmi = $this->reparseXml($scorm_tracking->xmldata);
			} else {
				// decode the json cmi saved in database
				$cmi = CJSON::decode($scorm_tracking->xmldata);
			}

			// if there aren't data from the previous session, we have to return the base tracking
			if (empty($cmi)) {
				$cmi = $this->getStarterCmi($user, $scorm_item);
			}

			// if there are data from the previous session, we have to use them and update some value
			$cmi['cmi.learner_id'] 			= $user->getClearUserId();
			$cmi['cmi.learner_name'] 		= $this->getScoLearnerName($user);
			$cmi['cmi.session_time'] 		= 'PT0H0M0S';

			// if the lo is completed (status passed or complete)) let's change the cmi.core.lesson_mode to review
			// in some LOs this will change the behaviour of the LO removing the possibility to recompile a test
			// for example
			/*
			 * TODO: check scorm 2004 policy
			 */
			//cmi.completion_status = 'completed' and cmi.success_status = 'passed' or 'unknown'
			if ($cmi['cmi.completion_status'] == 'completed' && $cmi['cmi.success_status'] != 'failed') {
				$cmi['cmi.mode'] = 'review';
			}
		} else {

			// if there aren't data from the previous session, we have to return the base tracking
			$cmi = $this->getStarterCmi($user, $scorm_item);

			// prepare the tracking !
			$new_scorm_tracking = new LearningScormTracking();
			$new_scorm_tracking->idUser 		= $id_user;
			$new_scorm_tracking->idReference	= $this->id_reference;
			$new_scorm_tracking->idscorm_item 	= $scorm_item->idscorm_item;
			$new_scorm_tracking->user_name 		= $this->getScoLearnerName($user);
			$new_scorm_tracking->first_access 	= Yii::app()->localtime->toLocalDateTime();
			$new_scorm_tracking->last_access 	= Yii::app()->localtime->toLocalDateTime();
			$new_scorm_tracking->save();

			// update the main track table for the table
			$scorm_items_track = LearningScormItemsTrack::model()->find('idUser=:idUser AND idscorm_item=:idscorm_item', array(
				':idUser' => $id_user,
				':idscorm_item' => $scorm_item->idscorm_item,
			));
			$scorm_items_track->idscorm_tracking = $new_scorm_tracking->idscorm_tracking;

			$scorm_items_track->save();
		}
		return $cmi;
	}


	/**
	 * Should map to the Commit of the scorm RTE, it should elaborate the cmi params whe needed
	 * like for computing the status or the total_time etcetera and save them in database
	 * @param int $id_user
	 * @param LearningScormItems $scorm_item
	 * @param array $cmi the array with the cmi data
	 * @return bool
	 * @throws CException
	 */
	public function Commit($id_user, LearningScormItems $scorm_item, $cmi, $isFinish=false) {

		// Perform operation based on the cmi status

		// Save in database the cmi for this sco
		$scorm_tracking = LearningScormTracking::model()->find('idUser=:idUser AND idscorm_item=:idscorm_item', array(
			':idUser' => $id_user,
			':idscorm_item' => $scorm_item->idscorm_item,
		));
		if (!$scorm_tracking) {
			throw new CException('Invalid tracking data');
		}
		// normalize, at the 6.1 beginning it wasn't populated
		$scorm_tracking->idReference	= $this->id_reference;

		// elaborate CMI data to perform scorm action =============================

		// compute the completion based on scorm 2004 spec

		if ($scorm_tracking->lesson_status != 'completed' && $scorm_tracking->lesson_status != 'passed') {

			if ($cmi['cmi.credit'] == 'credit') {

				if (!empty($cmi['cmi.completion_threshold']) && $cmi['cmi.completion_threshold'] > 0) {

					if (!empty($cmi['cmi.progress_measure'])) {
						// completion threshold and score setted, need to check the score
						if ((double)$cmi['cmi.progress_measure'] >= (double)$cmi['cmi.completion_threshold']) {

							$cmi['cmi.completion_status'] = 'completed';
							$scorm_tracking->lesson_status = 'completed';
						} else {

							$cmi['cmi.completion_status'] = 'incomplete';
							$scorm_tracking->lesson_status = 'incomplete';
						}
					} else {
						// threshold setting, but no progress
						$cmi['cmi.completion_status'] = 'unknow';
						$scorm_tracking->lesson_status = 'incomplete';
					}
				} else {

					// no threshold set and no progress passed, just copy the completion status
					if ($cmi['cmi.completion_status'] == 'completed') {
						if ($cmi['cmi.success_status'] == 'passed') {
							// passed
							$scorm_tracking->lesson_status = 'completed';
						} elseif ($cmi['cmi.success_status'] == 'failed') {
							// failed
							$scorm_tracking->lesson_status = $cmi['cmi.success_status'];

						} else {
							// unknow status, better revert on completion status
							$scorm_tracking->lesson_status = 'completed';
						}
					} else {
						$scorm_tracking->lesson_status = $cmi['cmi.completion_status'];
					}
				}
				if ($scorm_tracking->lesson_status  == 'completed') {
					// no more credit after first completion
					$cmi['cmi.credit'] = 'no-credit';
				}

			} // end of credit check

		} // end of already completed

		// end of CMI elaboration =================================================

		$scorm_tracking->score_raw 		= $cmi['cmi.score.raw'];
		$scorm_tracking->score_max 		= $cmi['cmi.score.max'];
		$scorm_tracking->score_min 		= $cmi['cmi.score.min'];
		$scorm_tracking->total_time 	= $cmi['cmi.total_time'];
		$scorm_tracking->xmldata 		= CJSON::encode($cmi);
		$scorm_tracking->last_access 	= Yii::app()->localtime->toLocalDateTime();
		$scorm_tracking->save(false);

		$scorm_items_track = LearningScormItemsTrack::model()->find('idUser=:idUser AND idscorm_item=:idscorm_item', array(
			':idUser' => $id_user,
			':idscorm_item' => $scorm_item->idscorm_item,
		));

		// check if it's the first completion
		$first_completion = false;
		if ($scorm_items_track->status != $scorm_tracking->lesson_status
			&& $scorm_tracking->lesson_status == 'completed') {
			$first_completion = true;
		}
		if ($scorm_items_track->status != 'completed') {
			$scorm_items_track->status = $scorm_tracking->lesson_status;
		}
		$scorm_items_track->save(false);

		// compute the new status into is parent's folder if needed, this will "propagate" the completion to
		// the folder's of the scorm up to the organiztion, this is needed because the whole scorm package
		// will be marked as completed only if all it's children are completed
		//if ($first_completion) $this->propagateCompletion($id_user, $scorm_item);

		// Always propagate the completion!
		if ($scorm_tracking->lesson_status == 'completed') {
			$this->propagateCompletion($id_user, $scorm_item, $first_completion);
		}elseif($scorm_tracking->lesson_status == 'incomplete'){
			// A SCO was just played (not completion), but we still need to update LearningCommonTrack
			// If we don't do it here but instead only on 'cmi.core.lesson_status'='completed', we have
			// a bug where if you just play the first SCO in the SCORM and not complete it the whole SCORM
			// is not marked as In progress
			$org_track = LearningScormItemsTrack::model()->find('idscorm_item IS NULL AND idUser=:idUser AND idscorm_organization=:idscorm_organization', array(
				':idUser' => $id_user,
				':idscorm_organization' => $scorm_item->idscorm_organization
			));

			$commonTrack = LearningCommontrack::model()->findByAttributes(array(
				'idReference' => $org_track->idReference,
				'idUser' => $org_track->idUser,
				'objectType' => LearningOrganization::OBJECT_TYPE_SCORMORG
			));
			if (!$commonTrack || ($commonTrack->status != LearningCommontrack::STATUS_COMPLETED && $commonTrack->status != LearningCommontrack::STATUS_PASSED)) {
				CommonTracker::track($org_track->idReference, $org_track->idUser, LearningCommontrack::STATUS_ATTEMPTED);
			}

		} elseif($scorm_tracking->lesson_status == 'failed') {
			$org_track = LearningScormItemsTrack::model()->find('idscorm_item IS NULL AND idUser=:idUser AND idscorm_organization=:idscorm_organization', array(
				':idUser' => $id_user,
				':idscorm_organization' => $scorm_item->idscorm_organization
			));

			$commonTrack = LearningCommontrack::model()->findByAttributes(array(
				'idReference' => $org_track->idReference,
				'idUser' => $org_track->idUser,
				'objectType' => LearningOrganization::OBJECT_TYPE_SCORMORG
			));
			if (!$commonTrack || ($commonTrack->status != LearningCommontrack::STATUS_COMPLETED && $commonTrack->status != LearningCommontrack::STATUS_PASSED)) {
				CommonTracker::track($org_track->idReference, $org_track->idUser, LearningCommontrack::STATUS_FAILED, array(
				    'countFailedAttempt' => $isFinish,
				));
			}
		}

		parent::Commit($id_user, $scorm_item, $cmi);

		return $cmi;
	}


	/**
	 * Should map to the Finish of the scorm RTE, it should elaborate the cmi params whe needed
	 * like for computing the status or the total_time etcetera and save them in database
	 * @param int $id_user
	 * @param LearningScormItems $scorm_item
	 * @param array $cmi the array with the cmi data
	 * @oaram $skiCmiElaboration whether CMI elaborations (e.g. session time + total_time) must be performed. OLP does it on its own.
	 * @return bool
	 * @throws CException
	 */
	public function Finish($id_user, LearningScormItems $scorm_item, $cmi, $skipCmiElaboration = false) {

		if(!$skipCmiElaboration) {
			// elaborate CMI data to perform scorm action =============================

			// first of all add the session_time to the total time
			if ($cmi['cmi.session_time'] != 'PT0H0M0S') {
				$cmi['cmi.total_time'] = $this->newTotalTime($cmi['cmi.session_time'], $cmi['cmi.total_time']);
			}

			// now, fix the cmi.core.entry based on the exit mode
			if ($cmi['cmi.exit'] == 'suspend') {
				$cmi['cmi.entry'] = 'resume';
			} else {
				$cmi['cmi.entry'] = '';
			}

			// end of CMI elaboration =================================================
		}

		// save in database the cmi for this sco, this is the normal commit operation, so we can recall it
		$cmi = $this->Commit($id_user, $scorm_item, $cmi, true);

		// compute the new status into is parent's folder if needed, this will "propagate" the completion to
		// the folder's of the scorm up to the organiztion, this is needed because the whole scorm package
		// will be marked as completed only if all it's children are completed

		return $cmi;
	}

}