<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */

class ScormApiBase extends CComponent {

	/**
	 * @var int will containt the main id or this scorm organization into learning_organization
	 */
	protected $id_reference = null;

	private $tmp_xml_parsed = array();

	/**
	 * Class constructor
	 */
	public function __construct($id_reference) {

		$this->id_reference = $id_reference;
	}


	/**
	 * Return the learner name in the scorm format
	 * @param CoreUser $user
	 * @return string Lastname,Firtname
	 */
	public function getScoLearnerName(CoreUser $user) {
		if (!empty($user->firstname) && !empty($user->lastname)) {
			$res = $user->lastname.','.$user->firstname;
		}
		else {
			$res = $user->getClearUserId();
		}

		return $res;
	}


	/**
	 * Return the base cmi for a clean Sco start
	 * @param CoreUser $user
	 * @param LearningScormItems $scorm_item
	 * @return array
	 */
	public function getStarterCmi(CoreUser $user, LearningScormItems $scorm_item) {

		return array();
	}


	/**
	 * This method must be reimplemented because change based on scorm version
	 * @param $session_time the last session time
	 * @param $total_time the previously accumulated time
	 * @return mixed the new total_time
	 */
	public function newTotalTime($session_time, $total_time) {

		return $session_time + $total_time;
	}


	/**
	 * Should be the same as the scorm rte Initialize, it will initialize the cmi for that scorm for the user
	 * @param $id_user
	 * @param LearningScormItems $scorm_item
	 * @return array|bool
	 */
	public function Initialize($id_user, LearningScormItems $scorm_item) {

		return array();
	}


	/**
	 * Recursive build of an array from the xmldata property
	 * @param $node SimpleXMLElement object to start from
	 * @param $path with dots (cmi.core)
	 */
	private function buildArrFromXml($node, $path) {

		if (count($node->children()) > 0) {
			foreach ($node->children() as $child) {
				$index = (isset($node['index']) && (string)$node['index'] != 'no') ? '.'.(string)$node['index'] : '';
				$tmpPath = ($path ? $path.".".(string)$node->getName() : (string)$node->getName()).$index;
				$this->buildArrFromXml($child, $tmpPath);
			}
		} else {
			$this->tmp_xml_parsed[$path.".".(string)$node->getName()] = (string)$node;
		}
	}


	/**
	 * Parse an xml track
	 * @param $xmldata
	 * @return array
	 */
	public function reparseXml($xmldata) {

		$xml = new SimpleXMLElement($xmldata);
		$this->buildArrFromXml($xml->cmi, '');
		return $this->tmp_xml_parsed;
	}



	/**
	 * The first time that a user do a scorm we have to copy it's structure into scorm_items_track for the next tracking,
	 * this is done using learning_scorm_items as the master to replicate the number of chapters of the scorm and it's
	 * propertyes into the tracking table adding a master row that will repsent the organization, that one will be the
	 * linkage with commontrack
	 * @param int $id_user
	 * @param int $idscorm_organization
	 * @return bool
	 * @throws CException
	 */
	public function setupScormItemsTrack($id_user, $idscorm_organization) {
		return LearningScormTracking::setupScormItemsTrack($id_user, $idscorm_organization, $this->id_reference);
	}


	/**
	 * Should map to the Commit of the scorm RTE, it should elaborate the cmi params whe needed
	 * like for computing the status or the total_time etcetera and save them in database
	 * @param int $id_user
	 * @param LearningScormItems $scorm_item
	 * @param array $cmi the array with the cmi data
	 * @return bool
	 */
	public function Commit($id_user, LearningScormItems $scorm_item, $cmi) {

		if (((int) $id_user > 0) && ( (int)$this->id_reference > 0) ) {
			// update session time for the current course. Use id_reference from the request,
			// because $this->id_reference is overwritten in ScormapiController::actionCommit()
			// and the session time might be tracked to a wrong course, if the LO is CLOR
			$id_reference = Yii::app()->request->getParam("id_reference", 0);
			if(!$id_reference) $id_reference = $this->id_reference;
			$loModel = LearningOrganization::model()->findByPk($id_reference);
			if ($loModel) {
				self::trackCourseSession($id_user, $loModel->idCourse);
			}
		}

		return true;
	}


	/**
	 * Retrive the information about the performance of the user inside the scorm
	 * @param $id_user
	 * @param LearningScormItems $scorm_item
	 * @return array
	 */
	public function getUserScormData($id_user, LearningScormItems $scorm_item) {

		$criteria = new CDbCriteria();
		$criteria->with = 'scormItemsTrack';
		$criteria->condition = 'idUser = :id_user AND t.idscorm_organization=:idscorm_organization';
		$criteria->params = array(
			':id_user' => $id_user,
			':idscorm_organization' => $scorm_item->idscorm_organization,
		);
		$criteria->order = 't.idscorm_item ASC';
		$track_items = LearningScormItems::model()->findAll($criteria);

		// read all the items of the lo, then we will "recurse" the completion
		$tracked_items = array();
		foreach ($track_items as $track) {

			$tracked_items[$track->idscorm_item] = $track;
		}
		return $tracked_items;
	}

	/**
	 * Propagate completion on master folders
	 *
	 * @param int                $id_user
	 * @param LearningScormItems $scorm_item
	 * @param                    $first_completion
	 */
	public function propagateCompletion($id_user, LearningScormItems $scorm_item, $first_completion) {


		$org_track = LearningScormItemsTrack::model()->find('idscorm_item IS NULL AND idUser=:idUser AND idscorm_organization=:idscorm_organization', array(
			':idUser' => $id_user,
			':idscorm_organization' => $scorm_item->idscorm_organization
		));
		if (!$first_completion) {

			// Update "organization" status, is the only operation needed in case of a second completion
			CommonTracker::track($org_track->idReference, $org_track->idUser, $org_track->status);
			return;
		}

		$tracked_items = $this->getUserScormData($id_user, $scorm_item);
		$parent = $tracked_items[$scorm_item->idscorm_parentitem];
		while ($parent) {

			// so, let's see if we have to increase the counters ([0] is a findAll, but we have a single track for each)
			$item_track = $parent->scormItemsTrack[0];
			if ($item_track->status != 'completed') {

				$item_track->nDescendantCompleted = $item_track->nDescendantCompleted + 1;
				if ($item_track->nDescendantCompleted >= $item_track->nDescendant) {
					$item_track->status = LearningCommontrack::STATUS_COMPLETED;
				} else {

					$item_track->status = LearningCommontrack::STATUS_ATTEMPTED;
				}
				$item_track->save();
			}

			if ($parent->idscorm_parentitem && isset($tracked_items[$parent->idscorm_parentitem])) {
				$parent = $tracked_items[$parent->idscorm_parentitem];
			} else {
				// set parent to false to end the cycle
				$parent = false;
			}
		}

		// Forward completion to the main "item", that it's the representation of the organization
		if ($org_track->status != 'completed') {
			$org_track->nDescendantCompleted = $org_track->nDescendantCompleted + 1;
		}

		if ($org_track->nDescendantCompleted >= $org_track->nDescendant) {
			$org_track->status = LearningCommontrack::STATUS_COMPLETED;
		} else {
			$org_track->status = LearningCommontrack::STATUS_ATTEMPTED;
		}
		$org_track->save();

		// Update "organization" status
		CommonTracker::track($org_track->idReference, $org_track->idUser, $org_track->status);
	}

	/**
	 * Should map to the Finish of the scorm RTE, it should elaborate the cmi params whe needed
	 * like for computing the status or the total_time etcetera and save them in database
	 * @param int $id_user
	 * @param LearningScormItems $scorm_item
	 * @param array $cmi the array with the cmi data
	 * @return bool
	 */
	public function Finish($id_user, LearningScormItems $scorm_item, $cmi) {

		return true;
	}
	
	
	/**
	 * "Steal/Resurect" the idEnter (session primary Key) from currently active course tracking session (record) and set the SESSION[] to it.
	 * This will make main page AND SCORM player to track/update the same.
	 * 
	 * @param number $idUser
	 * @param number $idCourse
	 */
	public static function resurectCourseTrackingSession($idUser, $idCourse) {
		$courseSessionKey = 'id_course_' . (int) $idCourse;
		$model = LearningTracksession::model()->findByAttributes(array('idUser' => $idUser, 'idCourse' => $idCourse, 'active' => 1));
		if ($model) {
			Yii::app()->session['id_enter_course'] = array($courseSessionKey => $model->idEnter);
		}
	}
	

	/**
	 * Update  Learning Track Session (course+user session tracking)
	 * 
	 * @param number $idUser
	 * @param number $idCourse
	 */
	public static function trackCourseSession($idUser, $idCourse) {
		self::resurectCourseTrackingSession($idUser, $idCourse);
		LearningTracksession::track($idUser, $idCourse);
	}
	
	

}