<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */

class ScormApi12 extends ScormApiBase {


	/**
	 * Class constructor
	 */
	public function __construct($id_reference) {
		parent::__construct($id_reference);
	}


	/**
	 * Return the base cmi for a clean Sco start
	 * @param CoreUser $user
	 * @param LearningScormItems $scorm_item
	 * @return array
	 */
	public function getStarterCmi(CoreUser $user, LearningScormItems $scorm_item) {

		return array(
			'cmi.core.lesson_status' 	=> 'not attempted',
			'cmi.core.student_id' 		=> $user->getClearUserId(),
			'cmi.core.student_name' 	=> $this->getScoLearnerName($user),
			'cmi.core.credit' 			=> 'credit',
			'cmi.core.lesson_mode' 		=> 'normal',
			'cmi.core.entry' 			=> 'ab-initio',
			'cmi.core.total_time' 		=> '0000:00:00.00',

			'cmi.launch_data' 						=> $scorm_item->adlcp_datafromlms,
			'cmi.student_data.mastery_score' 		=> $scorm_item->adlcp_masteryscore,
			'cmi.student_data.max_time_allowed' 	=> $scorm_item->adlcp_maxtimeallowed,
			'cmi.student_data.time_limit_action' 	=> $scorm_item->adlcp_timelimitaction,
			'cmi.student_preference.language' 		=> Yii::app()->getLanguage(),
		);
	}


	/**
	 * This method sum two time in the 0000:00:00.00 (hhhh:mm:ss:msms) format
	 * @param $session_time the last session time
	 * @param $total_time the previously accumulated time
	 * @return mixed the new total_time
	 */
	public function newTotalTime($session_time, $total_time) {

		$a1 = explode(":", $session_time);
		$h1 = $a1[0];
		$m1 = $a1[1];
		$pos = strpos($a1[2], ".");
		if( $pos === FALSE ) {
			$s1 = $a1[2];
			$c1 = 0;
		} else {
			$s1 = substr($a1[2], 0, $pos);
			$c1 = substr($a1[2], $pos+1);
		}

		$a2 = explode(":", $total_time);
		$h2 = $a2[0];
		$m2 = $a2[1];
		$pos = strpos($a2[2], ".");
		if( $pos === FALSE ) {
			$s2 = $a2[2];
			$c2 = 0;
		} else {
			$s2 = substr($a2[2], 0, $pos);
			$c2 = substr($a2[2], $pos+1);
		}

		$h = 0;
		$m = 0;
		$s = 0;
		$c = 0;

		$c += $c1 + $c2;
		if( $c >= 100 ) {
			$s++;
			$c -= 100;
		}
		$s += $s1 + $s2;
		if( $s >= 60 ) {
			$m++;
			$s -= 60;
		}
		$m += $m1 + $m2;
		if( $m >= 60 ) {
			$h++;
			$m -= 60;
		}
		$h += $h1 + $h2;

		return sprintf("%04u:%02u:%02u.%02u", $h, $m, $s, $c);
	}


	/**
	 * Should be the same as the scorm rte Initialize, it will initialize the cmi for that scorm for the user
	 * @param $id_user
	 * @param LearningScormItems $scorm_item
	 * @return array|bool
	 */
	public function Initialize($id_user, LearningScormItems $scorm_item) {

		// let's retrive the user info
		$user = CoreUser::model()->findByPk($id_user);
		if (!$user) return false;

		// check if we have the master tracking in scorm_items_tracking
		$scorm_item_track = LearningScormItemsTrack::model()->find('idscorm_item IS NULL AND idUser=:idUser AND idscorm_organization=:idscorm_organization', array(
			':idUser' => $id_user,
			':idscorm_organization' => $scorm_item->idscorm_organization
		));
		if (!$scorm_item_track) {
			// no master tracking, we have to create them
			$this->setupScormItemsTrack($id_user, $scorm_item->idscorm_organization);
			// ok we can now proceed with the specific sco tracking
		}

		// let's check if we have the data for this user
		$scorm_tracking = LearningScormTracking::model()->find('idUser=:idUser AND idscorm_item=:idscorm_item', array(
			':idUser' => $id_user,
			':idscorm_item' => $scorm_item->idscorm_item,
		));

		// if this item is an asset, at this point we can skip the cmi and set it as completed
		if ($scorm_item->scorm_resource->scormtype == 'asset') {

			// the first time we should mark it as completed, nothing to do the other times
			$scorm_items_track = LearningScormItemsTrack::model()->find('idUser=:idUser AND idscorm_item=:idscorm_item', array(
				':idUser' => $id_user,
				':idscorm_item' => $scorm_item->idscorm_item,
			));

			if ($scorm_items_track->status != LearningCommontrack::STATUS_COMPLETED) {

				$scorm_items_track->status 	= LearningCommontrack::STATUS_COMPLETED;
				$scorm_items_track->save();
				// like for a sco, propagate it's completion
				$this->propagateCompletion($id_user, $scorm_item, true);
			}

			// there is no cmi to return
			return array();
		}

		// this is a sco, so the full cmi management is needed
		$cmi = array();
		if ($scorm_tracking) {
			// see if the data are in xml from the old system
			if (!empty($scorm_tracking->xmldata) && $scorm_tracking->xmldata{0} == '<') {
				// reparse xml data into array
				$cmi = $this->reparseXml($scorm_tracking->xmldata);
			} else {
				// decode the json cmi saved in database
				$cmi = CJSON::decode($scorm_tracking->xmldata);
			}

			// if there aren't data from the previous session, we have to return the base tracking
			if (empty($cmi)) {
				$cmi = $this->getStarterCmi($user, $scorm_item);
			}

			// if there are data from the previous session, we have to use them and update some value
			$cmi['cmi.core.student_id'] 	= $user->getClearUserId();
			$cmi['cmi.core.student_name'] 	= $this->getScoLearnerName($user);
			$cmi['cmi.core.session_time'] = '0000:00:00.00';

			// if the lo is completed (status passed or complete)) let's change the cmi.core.lesson_mode to review
			// in some LOs this will change the behaviour of the LO removing the possibility to recompile a test
			// for example
			if ($cmi['cmi.core.lesson_status'] == 'completed' || $cmi['cmi.core.lesson_status'] == 'passed') {
				$cmi['cmi.core.lesson_mode'] = 'review';
			}
		} else {

			// if there aren't data from the previous session, we have to return the base tracking
			$cmi = $this->getStarterCmi($user, $scorm_item);

			// prepare the tracking !
			$new_scorm_tracking = new LearningScormTracking();
			$new_scorm_tracking->idUser 		= $id_user;
			$new_scorm_tracking->idReference	= $this->id_reference;
			$new_scorm_tracking->idscorm_item 	= $scorm_item->idscorm_item;
			$new_scorm_tracking->user_name 		= $this->getScoLearnerName($user);
			$new_scorm_tracking->first_access 	= Yii::app()->localtime->toLocalDateTime();
			$new_scorm_tracking->last_access 	= Yii::app()->localtime->toLocalDateTime();
			$new_scorm_tracking->save();

			// update the main track table for the table
			$scorm_items_track = LearningScormItemsTrack::model()->find('idUser=:idUser AND idscorm_item=:idscorm_item', array(
				':idUser' => $id_user,
				':idscorm_item' => $scorm_item->idscorm_item,
			));
			$scorm_items_track->idscorm_tracking = $new_scorm_tracking->idscorm_tracking;

			$scorm_items_track->save();
		}
		return $cmi;
	}


	/**
	 * Should map to the Commit of the scorm RTE, it should elaborate the cmi params whe needed
	 * like for computing the status or the total_time etcetera and save them in database
	 * @param int $id_user
	 * @param LearningScormItems $scorm_item
	 * @param array $cmi the array with the cmi data
	 * @return bool
	 * @throws CException
	 */
	public function Commit($id_user, LearningScormItems $scorm_item, $cmi, $isFinish=false) {

		// Perform operation based on the cmi status

		// Save in database the cmi for this sco
		$scorm_tracking = LearningScormTracking::model()->find('idUser=:idUser AND idscorm_item=:idscorm_item', array(
			':idUser' => $id_user,
			':idscorm_item' => $scorm_item->idscorm_item,
		));
		if (!$scorm_tracking) {
			throw new CException('Invalid tracking data');
		}
		// normalize, at the 6.1 beginning it wasn't populated
		$scorm_tracking->idReference	= $this->id_reference;

		// elaborate CMI data to perform scorm action =============================

		// compute the cmi.core_lesson_status based on the mastery score

		if ($scorm_tracking->lesson_status != 'completed' && $scorm_tracking->lesson_status != 'passed') {
			if ($cmi['cmi.core.credit'] == 'credit'
				&& $cmi['cmi.core.lesson_status'] != 'completed'
				&& $cmi['cmi.core.lesson_status'] != 'passed') {

				if ($scorm_item->adlcp_masteryscore > 0 && !empty($cmi['cmi.core.score.raw'])) {
					if ($cmi['cmi.core.score.raw'] >= $scorm_item->adlcp_masteryscore  ) {
						$cmi['cmi.core.lesson_status'] = 'passed';
						$cmi['cmi.core.credit'] = 'no-credit';
					} else {
						$cmi['cmi.core.lesson_status'] = 'failed';
					}
				}
			}
		}

		// end of CMI elaboration =================================================

		$scorm_tracking->lesson_status 	= $cmi['cmi.core.lesson_status'];
		$scorm_tracking->score_raw 		= $cmi['cmi.core.score.raw'];
		$scorm_tracking->score_max 		= $cmi['cmi.core.score.max'];
		$scorm_tracking->score_min 		= $cmi['cmi.core.score.min'];
		$scorm_tracking->total_time 	= $cmi['cmi.core.total_time'];
		$scorm_tracking->xmldata 		= CJSON::encode($cmi);
		$scorm_tracking->last_access 	= Yii::app()->localtime->toLocalDateTime();
		$scorm_tracking->save(false);

		$scorm_items_track = LearningScormItemsTrack::model()->find('idUser=:idUser AND idscorm_item=:idscorm_item', array(
			':idUser' => $id_user,
			':idscorm_item' => $scorm_item->idscorm_item,
		));

		// check if it's the first completion
		$first_completion = false;
		if ($scorm_items_track->status != $cmi['cmi.core.lesson_status']
			&& ($cmi['cmi.core.lesson_status'] == 'completed' || $cmi['cmi.core.lesson_status'] == 'passed')) {
			$first_completion = true;
		}
		if ($scorm_items_track->status != 'completed' && $scorm_items_track->status != 'passed') {
			$scorm_items_track->status = $cmi['cmi.core.lesson_status'];
		}
		$scorm_items_track->save(false);

		// compute the new status into is parent's folder if needed, this will "propagate" the completion to
		// the folder's of the scorm up to the organiztion, this is needed because the whole scorm package
		// will be marked as completed only if all it's children are completed
		//if ($first_completion) $this->propagateCompletion($id_user, $scorm_item);

		// Always propagate the completion!
		if ($cmi['cmi.core.lesson_status'] == 'completed' || $cmi['cmi.core.lesson_status'] == 'passed') {
			$this->propagateCompletion($id_user, $scorm_item, $first_completion);
		}elseif($cmi['cmi.core.lesson_status']=='incomplete'){
			// A SCO was just played (not completion), but we still need to update LearningCommonTrack
			// If we don't do it here but instead only on 'cmi.core.lesson_status'='completed', we have
			// a bug where if you just play the first SCO in the SCORM and not complete it the whole SCORM
			// is not marked as In progress
			$org_track = LearningScormItemsTrack::model()->find('idscorm_item IS NULL AND idUser=:idUser AND idscorm_organization=:idscorm_organization', array(
				':idUser' => $id_user,
				':idscorm_organization' => $scorm_item->idscorm_organization
			));

			$commonTrack = LearningCommontrack::model()->findByAttributes(array(
				'idReference' => $org_track->idReference,
				'idUser' => $org_track->idUser,
				'objectType' => LearningOrganization::OBJECT_TYPE_SCORMORG
			));
			if (!$commonTrack || ($commonTrack->status != LearningCommontrack::STATUS_COMPLETED && $commonTrack->status != LearningCommontrack::STATUS_PASSED)) {
				CommonTracker::track($org_track->idReference, $org_track->idUser, LearningCommontrack::STATUS_ATTEMPTED);
			}

		} elseif($cmi['cmi.core.lesson_status']=='failed') {
			$org_track = LearningScormItemsTrack::model()->find('idscorm_item IS NULL AND idUser=:idUser AND idscorm_organization=:idscorm_organization', array(
				':idUser' => $id_user,
				':idscorm_organization' => $scorm_item->idscorm_organization
			));

			$commonTrack = LearningCommontrack::model()->findByAttributes(array(
				'idReference' => $org_track->idReference,
				'idUser' => $org_track->idUser,
				'objectType' => LearningOrganization::OBJECT_TYPE_SCORMORG
			));
			if (!$commonTrack || ($commonTrack->status != LearningCommontrack::STATUS_COMPLETED && $commonTrack->status != LearningCommontrack::STATUS_PASSED)) {
				CommonTracker::track($org_track->idReference, $org_track->idUser, LearningCommontrack::STATUS_FAILED, array(
				    'countFailedAttempt' => $isFinish,
				));
			}

		}

		parent::Commit($id_user, $scorm_item, $cmi);

		return $cmi;
	}


	/**
	 * Should map to the Finish of the scorm RTE, it should elaborate the cmi params whe needed
	 * like for computing the status or the total_time etcetera and save them in database
	 * @param int $id_user
	 * @param LearningScormItems $scorm_item
	 * @param array $cmi the array with the cmi data
	 * @oaram $skiCmiElaboration whether CMI elaborations (e.g. session time + total_time) must be performed. OLP does it on its own.
	 * @return bool
	 * @throws CException
	 */
	public function Finish($id_user, LearningScormItems $scorm_item, $cmi, $skipCmiElaboration = false) {

		if(!$skipCmiElaboration) {
			// elaborate CMI data to perform scorm action =============================

			// first of all add the session_time to the total time
			if ($cmi['cmi.core.session_time'] != '0000:00:00.00') {
				$cmi['cmi.core.total_time'] = $this->newTotalTime($cmi['cmi.core.session_time'], $cmi['cmi.core.total_time']);
			}

			// now, fix the cmi.core.entry based on the exit mode
			if ($cmi['cmi.core.exit'] == 'suspend') {
				$cmi['cmi.core.entry'] = 'resume';
			} else {
				$cmi['cmi.core.entry'] = '';
			}

			// end of CMI elaboration =================================================
		}

		// save in database the cmi for this sco, this is the normal commit operation, so we can recall it
		$cmi = $this->Commit($id_user, $scorm_item, $cmi, true);

		// compute the new status into is parent's folder if needed, this will "propagate" the completion to
		// the folder's of the scorm up to the organiztion, this is needed because the whole scorm package
		// will be marked as completed only if all it's children are completed

		return $cmi;
	}

}