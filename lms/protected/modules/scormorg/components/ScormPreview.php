<?php


class ScormPreview
{

    /**
     * Load sco for remote, we must return the address to load
     * we need to pass from this function in order to check the setup of the record called to load sco with
     */
    public static function loadScoPreviewMode(){

        $id_reference = Yii::app()->request->getParam("id_reference", 0);
        $id_scorm_item = Yii::app()->request->getParam("id_item", 0);
        $id_scorm_resource = Yii::app()->request->getParam("id_resource", 0);
        $id_user = Yii::app()->request->getParam("id_user", 0);

        $object = LearningRepositoryObjectVersion::model()->findByAttributes(array(
            'id_resource' => $id_reference,
            'object_type' => LearningOrganization::OBJECT_TYPE_SCORMORG
        ));

        try {
            if (!$object || $object->object_type != LearningOrganization::OBJECT_TYPE_SCORMORG) {
                throw new CException('Invalid Scorm Learning Object: ' . $id_reference);
            }

            // retrive info about this chapter play
            $scorm_organization = LearningScormOrganizations::model()->findByPk($object->id_resource);
            if (!$scorm_organization) {
                throw new CException('Invalid Scorm organization: ' . $object->id_resource);
            }
            $scorm_package = LearningScormPackage::model()->findByPk($scorm_organization->idscorm_package);
            if (!$scorm_package) {
                throw new CException('Invalid Scorm package: ' . $scorm_organization->idscorm_package);
            }
            $scorm_item = LearningScormItems::model()->findByPk($id_scorm_item);
            if (!$scorm_item) {
                throw new CException('Invalid Scorm item: ' . $id_scorm_item);
            }
            $scorm_resource = LearningScormResources::model()->findByPk($id_scorm_resource);
            if (!$scorm_resource) {
                throw new CException('Invalid Scorm resource: ' . $id_scorm_resource);
            }

            if ($scorm_package->location != '') {
                $storageManager = CFileStorage::getMarketplaceStorage(CFileStorage::COLLECTION_MP_SCORM, $scorm_package->path);
                $launch_url = $storageManager->fileUrl($scorm_resource->href);
            } else {
                $storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_SCORM);
                $launch_url = $storageManager->fileUrl($scorm_resource->href, $scorm_package->path);
            }

            if(!empty($scorm_item->parameters) && strpos($launch_url, '?') === false && strpos($scorm_item->parameters, '?') === false) {
                $launch_url = $launch_url . '?' . $scorm_item->parameters;
            } else {
                $launch_url = $launch_url . $scorm_item->parameters;
            }

            // TODO: starting data for scorm 1.2 or 2004
            if ($scorm_package->scormVersion == '1.2') {
                $scormapi = new ScormApi12($id_reference);
            } else {
                $scormapi = new ScormApi2004($id_reference);
            }

            $tracking_data = self::getFakeInitialization($id_user, $scorm_item);

            // compose the array for the json callback
            $data = array(
                'success' => true,
                'launch_url' => $launch_url,
                'title' => $object->version_name,
                'initialize' => $tracking_data
            );
        } catch (CException $e) {

            $data = array(
                'success' => false,
                'message' => $e->getMessage()
            );
        }

        header('Content-type: text/javascript');
        if (isset($_GET['jsoncallback'])) {
            // JSONP callback
            $jsoncallback = preg_replace('/[^a-zA-Z\._]+/', '', $_GET['jsoncallback']);
            echo $jsoncallback. '(' . CJSON::encode($data) . ')';
        } else {
            // standard Json
            echo CJSON::encode($data);
        }
        Yii::app()->end();
    }

    /**
     * Called by SCORM player API (DCD) every N minutes to keep the user session alive.
     * Additionally doing time related updates in tracking tables.
     *
     */
    public static function keepPreviewAlive() {

        // extend the session
        Yii::app()->user->getIsGuest();

        $data = array(
            'success' => true
        );

        header('Content-type: text/javascript');
        if (isset($_GET['jsoncallback'])) {
            // JSONP callback
            $jsoncallback = preg_replace('/[^a-zA-Z\._]+/', '', $_GET['jsoncallback']);
            echo $jsoncallback. '(' . CJSON::encode($data) . ')';
        } else {
            // standard Json
            echo CJSON::encode($data);
        }

        Yii::app()->end();
    }

    /**
     * This will be called on a commit call on the scorm rte (js) by the lo
     */
    public static function finishPreview() {

        $id_user      = Yii::app()->request->getParam("id_user", 0);
        $id_reference = Yii::app()->request->getParam("id_reference", 0);
        $id_item      = Yii::app()->request->getParam("id_item", 0);

        // thiss will be an array
        $cmi = Yii::app()->request->getParam("cmi", array());

        $object = LearningRepositoryObjectVersion::model()->findByAttributes(array(
            'id_resource' => $id_reference,
            'object_type' => LearningOrganization::OBJECT_TYPE_SCORMORG
        ));

        try {
            if (!$object || $object->object_type != LearningOrganization::OBJECT_TYPE_SCORMORG) {
                throw new CException('Invalid Scorm Learning Object: ' . $id_reference);
            }

            // retrive info about this chapter play
            $scorm_organization = LearningScormOrganizations::model()->findByPk($object->id_resource);
            if (!$scorm_organization) {
                throw new CException('Invalid Scorm organization: ' . $object->id_resource);
            }
            $scorm_package = LearningScormPackage::model()->findByPk($scorm_organization->idscorm_package);
            if (!$scorm_package) {
                throw new CException('Invalid Scorm package: ' . $scorm_organization->idscorm_package);
            }
            $scorm_item = LearningScormItems::model()->findByPk($id_item);
            if (!$scorm_item) {
                throw new CException('Invalid Scorm item: ' . $id_item);
            }

            // it's a finish so we will add the link to play the next sco, it can be a "lms" url if the player need to close
            $scorm_nav = new ScormNav();
            $next_scorm_item_url = $scorm_nav->getNextToPreview($object, $id_user, $scorm_item);

            // Let's check if this is the last "CHAPTER", if so we need to reload the same chapter AGAIN
            if( strpos($next_scorm_item_url, 'scormorg/default/closePlayer') !== false ) {
                $scorm_items = ScormPreview::getUserScormData($scorm_item);
                $next_to_play = false;
                foreach ($scorm_items as $sco) {
                    if ($sco->idscorm_item == $scorm_item->idscorm_item) {
                        // we have found the current one !
                        $next_to_play = $sco;
                        break;
                    }
                }
                // build info to play the next one
                if($next_to_play) {
                    $next_to_play_info = ScormPreview::buildScoInfo($next_to_play, $object, $id_user);
                    $next_scorm_item_url = $next_to_play_info['sco_url'];
                }
            }

            $data = array(
                'success' => true,
                'message' => "finish successfull",
                'next_to_play' => $next_scorm_item_url
            );

        } catch (CException $e) {
            $data = array(
                'success' => false,
                'message' => $e->getMessage(),
            );
        }

        echo CJSON::encode($data);
        Yii::app()->end();
    }

    private static function getFakeInitialization($id_user, LearningScormItems $scorm_item) {

        // let's retrive the user info
        $user = CoreUser::model()->findByPk($id_user);
        if (!$user) return false;

        // if this item is an asset, at this point we can skip the cmi and set it as completed
        if ($scorm_item->scorm_resource->scormtype == 'asset') {

            // there is no cmi to return
            return array();
        }

        // Get and return the CMI
        $cmi = self::getStarterCmi($user, $scorm_item);
        return $cmi;
    }

    /**
     * Return the base cmi for a clean Sco start
     * @param CoreUser $user
     * @param LearningScormItems $scorm_item
     * @return array
     */
    public static function getStarterCmi(CoreUser $user, LearningScormItems $scorm_item) {

        return array(
            'cmi.core.lesson_status' 	=> 'not attempted',
            'cmi.core.student_id' 		=> $user->getClearUserId(),
            'cmi.core.student_name' 	=> self::getScoLearnerName($user),
            'cmi.core.credit' 			=> 'credit',
            'cmi.core.lesson_mode' 		=> 'normal',
            'cmi.core.entry' 			=> 'ab-initio',
            'cmi.core.total_time' 		=> '0000:00:00.00',

            'cmi.launch_data' 						=> $scorm_item->adlcp_datafromlms,
            'cmi.student_data.mastery_score' 		=> $scorm_item->adlcp_masteryscore,
            'cmi.student_data.max_time_allowed' 	=> $scorm_item->adlcp_maxtimeallowed,
            'cmi.student_data.time_limit_action' 	=> $scorm_item->adlcp_timelimitaction,
            'cmi.student_preference.language' 		=> Yii::app()->getLanguage(),
        );
    }

    /**
     * Return the learner name in the scorm format
     * @param CoreUser $user
     * @return string Lastname,Firtname
     */
    public static function getScoLearnerName(CoreUser $user) {
        if (!empty($user->firstname) && !empty($user->lastname)) {
            $res = $user->lastname.','.$user->firstname;
        }
        else {
            $res = $user->getClearUserId();
        }

        return $res;
    }

    public static function getUserScormData(LearningScormItems $scorm_item) {

        $criteria = new CDbCriteria();
        $criteria->condition = 't.idscorm_organization=:idscorm_organization';
        $criteria->params = array(':idscorm_organization' => $scorm_item->idscorm_organization);
        $criteria->order = 't.idscorm_item ASC';
        $track_items = LearningScormItems::model()->findAll($criteria);

        // read all the items of the lo, then we will "recurse" the completion
        $tracked_items = array();
        foreach ($track_items as $track) {

            $tracked_items[$track->idscorm_item] = $track;
        }
        return $tracked_items;
    }

    public static function buildScoInfo(LearningScormItems $scorm_item, $object, $id_user) {

        $id_scorm_item = $scorm_item->idscorm_item;
        $idscorm_resource = $scorm_item->idscorm_resource;


        // retrive info about this chapter play
        $scorm_organization = LearningScormOrganizations::model()->findByPk($object->id_resource);
        if (!$scorm_organization) {
            throw new CException('Invalid Scorm organization: ' . $object->id_resource);
        }
        $scorm_package = LearningScormPackage::model()->findByPk($scorm_organization->idscorm_package);
        if (!$scorm_package) {
            throw new CException('Invalid Scorm package: ' . $scorm_organization->idscorm_package);
        }
        $scorm_resource = LearningScormResources::model()->findByPk($idscorm_resource);
        if (!$scorm_resource) {
            throw new CException('Invalid Scorm resource: ' . $idscorm_resource);
        }

        $storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_SCORM);
        $launch_url = $storageManager->getHostUrl() . Yii::app()->params['scorm']['launcher'];
        $host = trim(preg_replace('/http[s]*:\/\//i', '', Yii::app()->request->getHostInfo()), '/');

        $auth = new AuthToken($id_user);
        $auth_token = $auth->get(true);

        $sco_url = $launch_url
            . "?host=".urlencode($host)
            . "&id_user=".urlencode($id_user)
            . "&id_reference=".urlencode($object->id_resource)
            . "&id_resource=".urlencode($idscorm_resource)
            . "&id_item=".urlencode($id_scorm_item)
            . "&scorm_version=".urlencode($scorm_package->scormVersion ? $scorm_package->scormVersion : '1.2')
            . "&id_package=".urlencode($scorm_organization->idscorm_package)
            //. "&auth_code=".urlencode($auth_token)
            . "&preview_mode=".urlencode(true);

        if (Settings::get('scorm_debug', 'off') == 'on') $sco_url .= "&debug=".urlencode("1");

        $data = array(
            'sco_url' => $sco_url,
            'launch_url' => $launch_url,
            'host' => $host,
            'id_user' => $id_user,
            'id_reference' => $object->id_resource,
            'scorm_version' => ($scorm_package->scormVersion ? $scorm_package->scormVersion : '1.2'),
            'id_resource' => $idscorm_resource,
            'id_item' => $id_scorm_item,
            'idscorm_organization' => $object->id_resource,
            'id_package' => $scorm_organization->idscorm_package,
            //'auth' => $auth_token,
            'debug' => ( Settings::get('scorm_debug', 'off') == 'on' ? true : false )
        );
        return $data;
    }
}