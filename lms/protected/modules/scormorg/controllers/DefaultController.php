<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */

class DefaultController extends ScormorgBaseController {


	/**
	 * By default, just redirect to main site.
	 */
	public function actionIndex() {
		$this->redirect($this->createUrl('/site'), true);
	}


	/**
	 * Register Controller specific assets
	 *
	 * (non-PHPdoc)
	 * @see ScormorgBaseController::registerResources()
	 */
	public function registerResources() {
		parent::registerResources();

		if (!Yii::app()->request->isAjaxRequest) {
		}

	}


	/**
	 * Update existing scorm package file structure using just uploaded ZIP file
	 *
	 * @param number $idOrg The LO to update
	 * @param array $file Uploaded file (<i>plupload</i> array format)
	 * @throws CException
	 */
	protected function updateScormPackage($idOrg, $file) {

		$response = new AjaxResult();

		try {
			// Validate file input
			if (!is_array($file)) { throw new CException("Invalid file."); }
			if (!isset($file['name']) || ($file['name'] == '')) { throw new CException("Invalid file name."); }
			if (!isset($file['percent']) || $file['percent'] < 100) { throw new CException("Incomplete file upload."); }
			if ('zip' != strtolower(CFileHelper::getExtension($file['name']))) { throw new CException("Invalid file format."); }

			$uploadTmpFile = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR . $file['name'];
			if (!is_file($uploadTmpFile)) { throw new CException("File '" . $file['name'] . "' does not exist."); }


			$loModel = LearningOrganization::model()->findByPk($idOrg);
			if (!$loModel) { throw new CException("Invalid learning object"); }

			$package = $loModel->scormOrganization->scormPackage;
			if (!$package) { throw new CException("Old SCORM package not found"); }


			$package->extractScormPackage($uploadTmpFile, $package->path);

			$data = array(
					'idPackage' => $package->getPrimaryKey(),
					'message' => Yii::t('standard', '_OPERATION_SUCCESSFUL'),
			);
			$response->setData($data)->setStatus(true);

		} catch (CException $e) {
			$response->setMessage($e->getMessage())->setStatus(false);
		}

		return $response;

	}


	/**
	 * Save uploaded LO. Caller must already saved the file into
	 * <root>/files/upload_tmp folder.
	 */
	public function actionAxSaveLo() {

		$courseId = Yii::app()->request->getParam("course_id", null);
		$parentId = Yii::app()->request->getParam("parent_id", null);
		$file = Yii::app()->request->getParam("file", null);

		// Use the physically uploaded filename, put by PLUpload in the "target_name" element
		// This is true when "unique_names" option is used for plupload JavaScript object
		if (is_array($file) && isset($file['name'])) {
			$originalFileName = urldecode($file['name']);
			if (isset($file['target_name'])) {
				$file['name'] = $file['target_name'];
			}
		}


		$shortDescription = Yii::app()->request->getParam('short_description', '');
		$thumb = Yii::app()->request->getParam('thumb', null);

		// URLDecode
		if (is_array($file) && isset($file['name'])) {
			$file['name'] = urldecode($file['name']);
		}

		// In case we are doing SCORM Package update ... we will get idOrg != false ... (from edit form)
		$idOrg = (int) Yii::app()->request->getParam("id_org", false);
		if ($idOrg > 0) {

			// IF we have file uploaded, update scorm package
			if (($file !== 'false') && ($file !== null) && ($file !== false)) {
				$response = $this->updateScormPackage($idOrg, $file);
			}
			// Otherswise, just update options, if any
			else {
				$response = new AjaxResult();
				$loModel = LearningOrganization::model()->findByPk($idOrg);
				$data = array(
					'idPackage' => $loModel->scormOrganization->scormPackage->getPrimaryKey(),
					'message' => Yii::t('standard', '_OPERATION_SUCCESSFUL'),
				);
				$response->setData($data)->setStatus(true);
			}
			// Some LO types are passing specific options upon uploading/saving REQUEST; lets handle them
			$lo = LearningOrganization::model()->findByPk($idOrg);
			if ($lo)  {
//				if($shortDescription)
				$lo->short_description = Yii::app()->htmlpurifier->purify($shortDescription);
				if($thumb){
					$lo->resource = intval($thumb);
				}elseif($lo->resource){
					$lo->resource = 0;
				}

				$this->saveLoOptions($lo);

				//update title displayed in LMS
				$newTitle = trim(Yii::app()->request->getParam("new-scorm-title", null));
				$lo->title = $newTitle;
				$lo->saveNode();

				// Trigger event to let plugins save their own data from POST
				Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
					'loModel' => $lo
				)));
			}
			$response->toJSON();
			Yii::app()->end();
		}

		// Otherwise, we are obviosly going to create NEW One... lets go...

		$response = new AjaxResult();
		Yii::app()->db->beginTransaction();

		try {
			// Validate input parameters
			if (!$courseId) { throw new CException("Invalid specified course."); }
			//if (empty($title)) { throw new CException("Invalid title."); }

			// Validate file input
			if (!is_array($file)) { throw new CException("Invalid file."); }
			if (!isset($file['name']) || ($file['name'] == '')) { throw new CException("Invalid file name."); }
			if (!isset($file['percent']) || $file['percent'] < 100) { throw new CException("Incomplete file upload."); }

			$fullFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $file['name'];
			/*********************************************************/
			if (isset(Yii::app()->params['virusscan_enable']) && (Yii::app()->params['virusscan_enable'] == true)) {
				$vs = new VirusScanner();
				$clean = $vs->scanFile($fullFilePath);
				if (!$clean) {
					FileHelper::removeFile($fullFilePath);
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				}
			}

			/*********************************************************/

			/*** File extension type validate ************************/
			$fileValidator = new FileTypeValidator(FileTypeValidator::TYPE_ARCHIVE);
			if (!$fileValidator->validateLocalFile($fullFilePath))
			{
				FileHelper::removeFile($fullFilePath);
				throw new CException($fileValidator->errorMsg);
			}
			/*********************************************************/

			//open package file and elaborate content
			$package = new LearningScormPackage();
			$package->idUser = Yii::app()->user->id;
			$package->setFile($file['name']);
			$package->setIdCourse($courseId);
			$package->setIdParent($parentId);

			$rs = $package->save();
			if (!$rs) {
				//TO DO: revert file storing
				throw new CException('Error while saving DB information');
			}

			// By now LearningOrganization is already saved. Lets get it back to save some options, if any (passed with the REQUEST)
			$lo = LearningOrganization::model()->findByAttributes(array(
				'objectType'	=> 	LearningOrganization::OBJECT_TYPE_SCORMORG,
				'idResource'	=> 	$package->getPrimaryKey()
			));
			if ($lo) {
//				if($shortDescription)
				$lo->short_description = Yii::app()->htmlpurifier->purify($shortDescription);
				if($thumb)
					$lo->resource = intval($thumb);

				$this->saveLoOptions($lo);

				// Trigger event to let plugins save their own data from POST
				Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
					'loModel' => $lo
				)));
			}

			$transaction = Yii::app()->getDb()->getCurrentTransaction();
			if($transaction) {
				$transaction->commit();
			}

			$data = array(
				'idPackage' => $package->getPrimaryKey(),
				//'title' => $title,
				'message' => Yii::t('standard', '_OPERATION_SUCCESSFUL'),
			);
			$response->setData($data)->setStatus(true);

		} catch (CException $e) {

			$transaction = Yii::app()->getDb()->getCurrentTransaction();
			if($transaction) {
				$transaction->rollback();
			}
			$response->setMessage($e->getMessage())->setStatus(false);
		}

		$response->toJSON();
		Yii::app()->end();

	}


	/**
	 * Return all the data in order to launch the scorm object requested
	 */
	public function actionAxGetLaunchInfo() {

		$response = new AjaxResult();

		// not used anymore
		$data_key = Yii::app()->request->getParam("data_key", 0);
		$chapter = Yii::app()->request->getParam("chapter", "");
		$id_resource = Yii::app()->request->getParam("id_resource", 0);

		$id_org = Yii::app()->request->getParam("id_org", 0);
		$id_scorm_item = Yii::app()->request->getParam("id_scorm_item", 0);

		$launch_type = Yii::app()->request->getParam("launch_type", '');


		try {
			$scorm_item = LearningScormItems::model()->findByPk($id_scorm_item);
			if (!$scorm_item) {
				throw new CException('Invalid Scorm item: ' . $id_scorm_item);
			}

			$scorm_nav = new ScormNav();
			$data = $scorm_nav->getScoLaunchInfo($id_org, $scorm_item, Yii::app()->user->id, $launch_type);

			$response->setStatus(true);
			$response->setData($data);
			$response->toJSON();
			Yii::app()->end();

		}
		catch (CException $e) {
			$response->setStatus(false)->setMessage($e->getMessage())->toJSON();
			Yii::app()->end();
		}

	}


	/**
	 * This action will be called when the player must be closed, in this case we return a simple js to close
	 * the, depending on the launch mode, the player
	 *
	 * @param $launch_type
	 * @param $course_id
	 */
	public function actionClosePlayer($launch_type, $course_id) {

		switch($launch_type) {
			case "popup": {
				// this is more complex, we have to refresh the opener and close ourself
				echo "<script type=\"text/javascript\">
					try {
						window.opener.location = '" . $this->createUrl('/player') . "';
					} catch(e) {}
					window.top.close();
					</script>";
			};break;
			case "fullscreen": {
				// shouldn't be used, the player is doing this automatically, but just in case
				echo "<script type=\"text/javascript\">
					window.top.location =  '" . $this->createUrl('/player') . "';
					</script>";
			};break;
			case "lightbox":
			case "inline":
			default: {
				if (PluginManager::isPluginActive('WebApp') && $launch_type == "undefined" && ($course_id != "undefined" && LearningCourse::model()->findByPk($course_id))) {
					list($course_id)=explode(',', $course_id);
					echo "<script type=\"text/javascript\">
							window.top.location = '".Docebo::getWebappUrl(true).'/#/course/'.$course_id."';
					</script>";
				}
				else if($launch_type == "undefined" && ($course_id != "undefined") &&  (LearningCourse::model()->findByPk($course_id))){
					echo "<script type=\"text/javascript\">
							window.top.location =  '" .Docebo::createAppUrl('mobile:mplayer/index', array('course_id' => $course_id)) . "';
					</script>";
				}else{
				    if(!Yii::app()->legacyWrapper->hydraFrontendEnabled()) {
                        echo "<script type=\"text/javascript\">
                        window.top.location.reload();
                        </script>";
                    }
				}
			};break;

		}
		Yii::app()->end();
	}


	/**
	 * A work towards saving LO specific options/settings based on the current REQUEST to axSaveLo() action
	 *
	 * @param object|bool $loModel
	 */
	private function saveLoOptions($loModel = false) {

		if (!$loModel) {
			return;
		}

		// These are pretty common and can be sent by all LO types being saved/uploaded
		$loModel->desktop_openmode    = Yii::app()->request->getParam("desktop_openmode", LearningOrganization::OPENMODE_LIGHTBOX);
		$loModel->tablet_openmode     = Yii::app()->request->getParam("tablet_openmode", LearningOrganization::OPENMODE_FULLSCREEN);
		$loModel->smartphone_openmode = Yii::app()->request->getParam("smartphone_openmode", LearningOrganization::OPENMODE_FULLSCREEN);

		$loModel->saveNode(false);

	}



}
