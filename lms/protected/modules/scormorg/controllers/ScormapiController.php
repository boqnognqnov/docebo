<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */

class ScormapiController extends Controller {

	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		return array(

			array('allow',
				'actions' => array('LoadRemoteSco'),
				'users' => array('@'),
			),
			array('allow',
				'actions' => array('keepAlive', 'commit', 'finish'),
				'users' => array('*'),
			),
			array('deny',
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
			)
		);
	}

	public function checkAuthCode() {

		$id_user 	= Yii::app()->request->getParam("id_user", 0);
		$auth_code 	= Yii::app()->request->getParam("auth_code", "undefined");

		$auth_token = new AuthToken($id_user);

		if ($auth_token->get(false) != $auth_code) {

			$data = array(
				'success' => 'false',
				'message' => 'Invalid Authorization, try a new login.'
			);
			echo CJSON::encode($data);
			Yii::app()->end();
		}
	}

	/**
	 * By default, just redirect to main site.
	 */
	public function actionIndex() {
		// $this->redirect($this->createUrl('/site'), true);
	}

	/**
	 * Load sco for remote, we must return the address to load
	 * we need to pass from this function in order to check the setup of the record called to load sco with
	 */
	public function actionLoadRemoteSco() {
		// We are trusting the session for this behaviour ! The auth_code will be returned by this method and used later for the
		// sync calls
		// $this->checkAuthCode();

		// Check if the preview mode is requested (without tracking)
		$preview_mode = filter_var(Yii::app()->request->getParam("preview_mode", false), FILTER_VALIDATE_BOOLEAN);
		if($preview_mode)
			ScormPreview::loadScoPreviewMode();

		$id_user = Yii::app()->request->getParam("id_user", 0);
		$id_reference = Yii::app()->request->getParam("id_reference", 0);
		$id_resource = Yii::app()->request->getParam("id_resource", 0);
		$id_item = Yii::app()->request->getParam("id_item", 0);

		$object = LearningOrganization::model()->findByPk($id_reference);

		try {
			if (!$object || $object->objectType != LearningOrganization::OBJECT_TYPE_SCORMORG) {
				throw new CException('Invalid Scorm Learning Object: ' . $id_reference);
			}

			// retrive info about this chapter play
			$scorm_organization = LearningScormOrganizations::model()->findByPk($object->idResource);
			if (!$scorm_organization) {
				throw new CException('Invalid Scorm organization: ' . $object->idResource);
			}
			$scorm_package = LearningScormPackage::model()->findByPk($scorm_organization->idscorm_package);
			if (!$scorm_package) {
				throw new CException('Invalid Scorm package: ' . $scorm_organization->idscorm_package);
			}
			$scorm_item = LearningScormItems::model()->findByPk($id_item);
			if (!$scorm_item) {
				throw new CException('Invalid Scorm item: ' . $id_item);
			}
			$scorm_resource = LearningScormResources::model()->findByPk($id_resource);
			if (!$scorm_resource) {
				throw new CException('Invalid Scorm resource: ' . $id_resource);
			}

			// Compose object url
			$course = LearningCourse::model()->findByPk($object->idCourse);

			if ($scorm_package->location != '' && $course->mpidCourse > 0) {
				$storageManager = CFileStorage::getMarketplaceStorage(CFileStorage::COLLECTION_MP_SCORM, $scorm_package->path);
				$launch_url = $storageManager->fileUrl($scorm_resource->href);
			} else {
				$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_SCORM);
				$launch_url = $storageManager->fileUrl($scorm_resource->href, $scorm_package->path);
			}

			// Adjust the object reference (in case this is a central repo LO)
			$object = $object->getMasterForCentralLo($id_user);
			$id_reference = $object->idOrg;

			if ($scorm_package->scormVersion == '1.2') {
				$scormapi = new ScormApi12($id_reference);
			} else {
				$scormapi = new ScormApi2004($id_reference);
			}
			$tracking_data = $scormapi->Initialize($id_user, $scorm_item);
			UserLimit::logActiveUserAccess($id_user);

			if(!empty($scorm_item->parameters) && strpos($launch_url, '?') === false && strpos($scorm_item->parameters, '?') === false) {
				$launch_url = $launch_url . '?' . $scorm_item->parameters;
			} else {
				$launch_url = $launch_url . $scorm_item->parameters;
			}

			$oAuth_token = new AuthToken($id_user);
			$auth_code = $oAuth_token->get();
			if(!$auth_code) {

				$data = array(
					'success' => 'false',
					'message' => 'Invalid Authorization, try a new login.'
				);
			} else {

				// compose the array for the json callback
				$data = array(
					'success' => true,
					'auth_code' => $auth_code,
					'launch_url' => $launch_url,
					'title' => $object->title,
					'initialize' => $tracking_data
				);
			}
		} catch (CException $e) {

			$data = array(
				'success' => false,
				'message' => $e->getMessage()
			);
		}

		header('Content-type: text/javascript');
		if (isset($_GET['jsoncallback'])) {
			// JSONP callback
			$jsoncallback = preg_replace('/[^a-zA-Z\._]+/', '', $_GET['jsoncallback']);
			echo $jsoncallback. '(' . CJSON::encode($data) . ')';
		} else {
			// standard Json
			echo CJSON::encode($data);
		}
		Yii::app()->end();
	}

	/**
	 * Called by SCORM player API (DCD) every N minutes to keep the user session alive.
	 * Additionally doing time related updates in tracking tables.
	 * 
	 */
	public function actionKeepAlive() {
		$this->checkAuthCode();

		$preview_mode = filter_var(Yii::app()->request->getParam("preview_mode", false), FILTER_VALIDATE_BOOLEAN);
		if($preview_mode)
			ScormPreview::keepPreviewAlive();

		// extend the session
		Yii::app()->user->getIsGuest();

		$data = array(
			'success' => true
		);

		header('Content-type: text/javascript');
		if (isset($_GET['jsoncallback'])) {
			// JSONP callback
			$jsoncallback = preg_replace('/[^a-zA-Z\._]+/', '', $_GET['jsoncallback']);
			echo $jsoncallback. '(' . CJSON::encode($data) . ')';
		} else {
			// standard Json
			echo CJSON::encode($data);
		}
		
		// Update course+user session tracking to register user's course activity (time in course)
		if (	isset($_REQUEST['id_reference']) 		&& 
				isset($_REQUEST['id_user']) 			&&
				((int) $_REQUEST['id_reference'] > 0) 	&&
				((int) $_REQUEST['id_user'] > 0) 
		) {
			$loModel = LearningOrganization::model()->findByPk((int) $_REQUEST['id_reference']);
			if ($loModel) {
				ScormApiBase::trackCourseSession((int) $_REQUEST['id_user'], $loModel->idCourse);
			}
		}
		
		Yii::app()->end();
	}

	/**
	 * This will be called on a commit call on the scorm rte (js) by the lo
	 */
	public function actionCommit() {
		$this->checkAuthCode();

		$preview_mode = filter_var(Yii::app()->request->getParam("preview_mode", false), FILTER_VALIDATE_BOOLEAN);
		// If preview mode is requested just return success, we don't need to track anything
		if($preview_mode) {
			// compose the array for the return
			echo CJSON::encode(array(
				'success' => true,
				'message' => "commit successfull",
			));
			Yii::app()->end();
		}

		$id_user = Yii::app()->request->getParam("id_user", 0);
		$id_reference = Yii::app()->request->getParam("id_reference", 0);
		$id_item = Yii::app()->request->getParam("id_item", 0);

		// thiss will be an array
		$cmi = Yii::app()->request->getParam("cmi", array());

		$object = LearningOrganization::model()->findByPk($id_reference);

		try {
			if (!$object || $object->objectType != LearningOrganization::OBJECT_TYPE_SCORMORG) {
				throw new CException('Invalid Scorm Learning Object: ' . $id_reference);
			}
			Yii::app()->session['idCourse'] = $object->idCourse;

			// Adjust the object reference (in case this is a central repo LO)
			$object = $object->getMasterForCentralLo($id_user);
			$id_reference = $object->idOrg;

			// retrive info about this chapter play
			$scorm_organization = LearningScormOrganizations::model()->findByPk($object->idResource);
			if (!$scorm_organization) {
				throw new CException('Invalid Scorm organization: ' . $object->idResource);
			}
			$scorm_package = LearningScormPackage::model()->findByPk($scorm_organization->idscorm_package);
			if (!$scorm_package) {
				throw new CException('Invalid Scorm package: ' . $scorm_organization->idscorm_package);
			}
			$scorm_item = LearningScormItems::model()->findByPk($id_item);
			if (!$scorm_item) {
				throw new CException('Invalid Scorm item: ' . $id_item);
			}

			// TODO: starting data for scorm 1.2 or 2004
			if ($scorm_package->scormVersion == '1.2') {
				$scormapi = new ScormApi12($id_reference);
			} else {
				$scormapi = new ScormApi2004($id_reference);
			}

			// let's save the data, if somethign goes wrong this will launch an exception
			$cmi = $scormapi->Commit($id_user, $scorm_item, $cmi);

			// compose the array for the return
			$data = array(
				'success' => true,
				'message' => "commit successfull",
				// 'received_message' => $cmi, // enable this for debug if you want to have back the saved cmi
			);

		} catch (CException $e) {
			$data = array(
				'success' => false,
				'message' => $e->getMessage(),
				// 'received_message' => $_POST,  // enable this for debug if you want to have back the saved cmi
			);
		}

		echo CJSON::encode($data);
		Yii::app()->end();
	}

	/**
	 * This will be called on a commit call on the scorm rte (js) by the lo
	 */
	public function actionFinish() {
		$this->checkAuthCode();

		$preview_mode = filter_var(Yii::app()->request->getParam("preview_mode", false), FILTER_VALIDATE_BOOLEAN);
		if($preview_mode)
			ScormPreview::finishPreview();

		$id_user      = Yii::app()->request->getParam("id_user", 0);
		$id_reference = Yii::app()->request->getParam("id_reference", 0);
		$id_item      = Yii::app()->request->getParam("id_item", 0);
		$launch_type  = Yii::app()->request->getParam("launch_type", false);

		// thiss will be an array
		$cmi = Yii::app()->request->getParam("cmi", array());

		$object = LearningOrganization::model()->findByPk($id_reference);

		try {
			if (!$object || $object->objectType != LearningOrganization::OBJECT_TYPE_SCORMORG) {
				throw new CException('Invalid Scorm Learning Object: ' . $id_reference);
			}
			Yii::app()->session['idCourse'] = $object->idCourse;

			// Adjust the object reference (in case this is a central repo LO)
			$object = $object->getMasterForCentralLo($id_user);
			$id_reference = $object->idOrg;

			// retrive info about this chapter play
			$scorm_organization = LearningScormOrganizations::model()->findByPk($object->idResource);
			if (!$scorm_organization) {
				throw new CException('Invalid Scorm organization: ' . $object->idResource);
			}
			$scorm_package = LearningScormPackage::model()->findByPk($scorm_organization->idscorm_package);
			if (!$scorm_package) {
				throw new CException('Invalid Scorm package: ' . $scorm_organization->idscorm_package);
			}
			$scorm_item = LearningScormItems::model()->findByPk($id_item);
			if (!$scorm_item) {
				throw new CException('Invalid Scorm item: ' . $id_item);
			}

			// TODO: starting data for scorm 1.2 or 2004
			if ($scorm_package->scormVersion == '1.2') {
				$scormapi = new ScormApi12($id_reference);
			} else {
				$scormapi = new ScormApi2004($id_reference);
			}

			// let's save the data, if somethign goes wrong this will launch an exception
			$cmi = $scormapi->Finish($id_user, $scorm_item, $cmi);

			// compose the array for the return

			// it's a finish so we will add the link to play the next sco, it can be a "lms" url if the player need to close
			$scorm_nav = new ScormNav();
			$next_scorm_item_url = $scorm_nav->getNextToPlay($id_reference, $id_user, $scorm_item, $scormapi, $launch_type);

			$data = array(
				'success' => true,
				'message' => "finish successfull",
				'next_to_play' => $next_scorm_item_url
				// 'received_message' => $cmi, // enable this for debug if you want to have back the saved cmi
			);

		} catch (CException $e) {
			$data = array(
				'success' => false,
				'message' => $e->getMessage(),
				// 'received_message' => $cmi,  // enable this for debug if you want to have back the saved cmi
			);
		}

		echo CJSON::encode($data);
		Yii::app()->end();
	}

}