<?php

use Elasticsearch\ClientBuilder;
use Aws\Common\Credentials\Credentials;
use Aws\Common\Signature\SignatureV4;
/**
 *
 * A Yii Application component, wrapping up Elasticsearch Client.
 *
 * Component parameters (public attributes) can be set in main-devel/main-live configuration file(s).
 *
 */
class EsClient extends CApplicationComponent {

    const MIN_SCORE = 0.7;            // Default Minimal score for a document to consider it a valid hit
	const RESULT_PAGE_SIZE = 20;    // How many results a Page would consists of (UI, caller, etc.)
	const SLOP_VALUE = 10;          // how far words can be apart from each other (the higher the further they can be; recommended value is 50-100)


    const SERVICE_NAME = 'es';

	const ACTION_INDEX 		= 'index';
	const ACTION_CREATE 	= 'create';
	const ACTION_UPDATE 	= 'update';
	const ACTION_DELETE 	= 'delete';

	const LOG_CATEGORY      = 'es';

    const UI_SUGGESTIONS    = 'suggestions';
    const UI_RESULTS        = 'results';

    const LIMIT_RESULT_TITLE        = 60;
    const LIMIT_RESULT_DESCRIPTION  = 140;

    const ALL = 'all';

    /**
     * @var string The endpoint of the Elasticsearch domain
     */
    public $host;

    /**
     * @var integer The port of the Elasticsearch domain
     */
    public $port;

    /**
     * @var string
     */
    public $awsRegion   = '';

    /**
     * @var string
     */
    public $awsKey      = '';

    /**
     *
     * @var string
     */
    public $awsSecret   = '';

    /**
     * If set to TRUE, request to Amazon will be signed. This allows calls from anywhere.
     * If set to FALSE, Amazon ElasticSearch policy should be set to "by caller IP".
     *
     * @var boolean
     */
    public $useSignedRequest = true;


    public $disabled = false;


    /**
     * Holds cached item visibility for current user
     * @var unknown
     */
    protected $itemsVisibilityCache = null;

	/**
	 * Holds the module instance
	 * @var ElasticSearchModule
	 */
	protected $_module = null;

	/**
	 *
	 * @var Elasticsearch\Client
	 */
	protected $_client = null;

	/**
	 * If the client is used in Console environment, user is irrelevant. Lets keep if this run is user-aware in this attribute (loaded later)
	 * @var string
	 */
	protected $userAware = false;


	//-- Data, preloaded before Search Cycling started
	protected $catalogAppActive = false;   // If CatalogApp is active
	private   $_userCatalogs = array();     // list of catalog IDs current user is a member of, if any


	protected $availableTypes = null;

	/**
	 * Based on various LMS options build ElasticSearch FILTER (ES version 1.5).
	 * @param string $type Document type filtering
	 */
	private function buildElasticsearchVisibilityFilter($type=false) {

	    $filterArray = array();
	    $shouldArray = array();

	    $idsFilter = $this->itemsVisibilityCache === null ? array() : $this->itemsVisibilityCache;

	    // Check if courses/plans are visible as part of generic catalog
	    // Defined as: when user is not assigned to ANY catalog and "on_catalogue_empty" is ON (to show generic catalog to user in such cases)
	    $genericCatalogVisibility = false;

	    if (!PluginManager::isPluginActive('CoursecatalogApp') || (empty($this->userCatalogs) && (Settings::get('on_catalogue_empty', 'off') === 'on'))) {
	        $genericCatalogVisibility = true;
	    }

	    if (is_array($idsFilter) && !empty($idsFilter)) {

	        foreach ($idsFilter as $documentType => $documentFilterIds) {

	            if ( (($type && ($type === $documentType)) || !$type) ) {

	            	// Knowledge assets have this visibleToAll set to true if they are ... visible to all :-)
	            	if ($documentType === EsAgentKnowledgeAsset::TYPE) {

	            		$tmp = array();

	            		$tmp["and"] = array(
	            			array(
	            				"type"   => array(
	            					"value"    => $documentType,
	            				),
	            			),
	            		);
	            		$tmp["and"][] = array(
	            			"terms"	=> array(
	            				"visibleToAll"	=> array(1),
	            			),
	            		);
	            		$shouldArray[] = $tmp;
	            	}
	            	// Questions have this visibleToAll set to true if they are ... visible to all :-)
	            	else if ($documentType === EsAgentQandA::TYPE) {
	            		$tmp = array();

	            		$tmp["and"] = array(
	            			array(
	            				"type"   => array(
	            					"value"    => $documentType,
	            				),
	            			),
	            		);
	            		$tmp["and"][] = array(
	            			"terms"	=> array(
	            				"visibleToAll"	=> array(1),
	            			),
	            		);
	            		$shouldArray[] = $tmp;
	            	}

	                // This is not a Knowledge Asset nor a Question: we need to apply some generic catalog logic
	                // If we need to apply Generic Catalog visibility, we add also to the "should" list
	                // all courses/los/plans which are part of the so called "generic catalog"
	                else if ($genericCatalogVisibility) {

	                    $tmp = array();

	                    $tmp["and"] = array(
	                        array(
	                            "type"   => array(
	                                "value"    => $documentType,
	                            ),
	                        ),
	                    );

	                    // Course has "show_rules" = 0|1 (Catalog Options -> Show..to... > Everyone, and show on home page|Only for logged in users
	                    // Also if LO has the same "artifical" field set to the same values (inheriting it from the course, during indexing process)
	                    if ($documentType === EsAgentCourse::TYPE || $documentType === EsAgentLo::TYPE) {
	                        $tmp["and"][] = array(
	                            "terms"	=> array(
	                                "show_rules"	=> array(0,1),
	                            ),
	                        );
	                        // COURSE and LO documents have "status" attribute (both taken from Course)
	                        // When ES searches for documents which are possibly Course or LO, they also SHOULD have this status = <publishd>
	                        $tmp["and"][] = array(
	                            "terms"	=> array(
	                                "status"	=> array(LearningCourse::$COURSE_STATUS_EFFECTIVE),
	                            ),
	                        );
	                    }
	                     
	                    // Learning plan has "visible_in_catalog" = 1 ?
	                    if ($documentType === EsAgentPlan::TYPE) {
	                        $tmp["and"][] = array(
	                            "terms"	=> array(
	                                "visible_in_catalog"	=> array(1),
	                            ),
	                        );
	                    }

	                	$shouldArray[] = $tmp;

	                }

	                // Of course, search SHOULD also include documents of ANY type, having one of the "explicitely visible IDs"
	                // List of IDs is specific for every type though!!
	                $generalShoulds = array(
               			array(
               				"ids"   => array(
             					"type"      => $documentType,
               					"values"    => $documentFilterIds,
               				)
               			),
               		); 
	                
	                // Yet again, if it is course or LO, it SHOULD have status = <published> (BUT together with IDs above)
	                if ($documentType === EsAgentCourse::TYPE || $documentType === EsAgentLo::TYPE) {
	                	$generalShoulds[] = array(
	                    	"terms"	=> array(
	                        	"status"	=> array(LearningCourse::$COURSE_STATUS_EFFECTIVE),
							),
	                	);
	                }
	                // BOTH above form the logic like this:
	                // If document wants to be in the search result... 
	                // it SHOULD be of type <type>, have one of the listed <ids> AND
	                // if it is a course or LO, it also SHOULD have status=<published>. 
	                // If it is NOT course or LO, the <ids> filter is enough.
	                
	                
	                // So, add these last SHOULD (which means... to show ..it should.. if having.. it is NOT a MUST..!)
                	$shouldArray[] = array(
                		"and" => $generalShoulds,
                	);
                	
	            }
	        }
	    }

	    $filterArray['should'] = $shouldArray;

	    // On top of everything:
	    // Exclude ("must_not") all documents of type who's Agent is NOT AVAIL
	    $mustNots = $this->buildDisabledTypesFilter();
	    if (!empty($mustNots)) {
	    	$filterArray['must_not'] = $mustNots;
	    }

	    return $filterArray;

	}


	/**
	 * Build Elastic search array of "must_not", for disabled types (e.g. disabled Curricula or C&S)
	 *
	 * @return array
	 */
	public function buildDisabledTypesFilter() {
		$mustNots = array();
		foreach (self::getAvailableTypes() as $avType) {
			$agent = EsAgent::getAgent($avType);
			if (!$agent->isAvail()) {
				$mustNots[] = array(
						"type" => array(
								'value' => $avType,
						),
				);
			}
		}
		return $mustNots;
	}


	/**
	 * [Re]Index all course's LOs
	 *
	 * @param LearningCourse $model
	 */
	private function indexCourseLearningMaterials(LearningCourse $model) {
	    $loIds = false;
	    foreach ($model->learningOrganizations as $loModel) {
	        $loIds[] = $loModel->idOrg;
	    }
	    if ($loIds !== false) {
	      		return EsAgent::getAgent(EsAgentLo::TYPE)->indexListOfDocuments($loIds);
	    }
	    else {
	        return true;
	    }
	}

	/**
	 * Given a Data Provider, index all assets in a chunked way
	 * @param CSqlDataProvider $dp
	 */
	private function indexAssetsFromDataProvider($dp) {
	    $iterator = new CDataProviderIterator($dp);
	    $chunk = array();
	    foreach ($iterator as $item) {
	        $chunk[] = $item["id"];
	        if (count($chunk) > 100) {
	            EsAgent::getAgent(EsAgentKnowledgeAsset::TYPE)->indexListOfDocuments($chunk);
	            $chunk = array();
	        }
	    }
	    if (count($chunk) > 0) {
	        EsAgent::getAgent(EsAgentKnowledgeAsset::TYPE)->indexListOfDocuments($chunk);
	    }
	}


	/**
	 * Index array of assets (by IDs)
	 *
	 * @param array
	 */
	private function indexAssets($assets) {
		$chunk = array();
		foreach ($assets as $item) {
			$chunk[] = $item["id"];
			if (count($chunk) > 100) {
				EsAgent::getAgent(EsAgentKnowledgeAsset::TYPE)->indexListOfDocuments($chunk);
				$chunk = array();
			}
		}
		if (count($chunk) > 0) {
			EsAgent::getAgent(EsAgentKnowledgeAsset::TYPE)->indexListOfDocuments($chunk);
		}
	}


	/**
	 * Load item visibility cache for this user (preloading), before cycling all the hits)
	                    *
	                    */
	private function loadCachedVisibility() {

	    // We do not load cache for console applications or godadmins
	    if (!$this->userAware || Yii::app()->user->isGodAdmin) {
	        return null;
	    }

	    // If NOT yet loaded and cache is enabled, load the cache
	    if (($this->itemsVisibilityCache === null) && isset(Yii::app()->cache_items_visibility)) {
	        $idUser = Yii::app()->user->id;
	        $this->itemsVisibilityCache = Yii::app()->cache_items_visibility->get($idUser);

	        // We need to (re)build the cache if it is not available (and this user is NOT an admin as it seems)
	        if (!$this->itemsVisibilityCache) {
	        	$visCache = $this->buildVisibility($idUser);
	        	if (is_array($visCache)) {
	        		$this->itemsVisibilityCache = $visCache;
	        		Yii::app()->cache_items_visibility->set($idUser, $visCache);
	        	}
	        }
	    }

	}


	/**
	 * Invalidate (delete) visibility cache files for selected users
	 *
	 * @param integer|array $users
	 */
	private function invalidateCaches($users) {

	    // Maybe the input is a SQL Data Provider?
	    if ($users instanceof CSqlDataProvider) {
	        /* @var $dp CSqlDataProvider */
	        $dp = $users;
	        $dp->pagination = false;
            $iterator = new CDataProviderIterator($dp);
            foreach ($iterator as $user) {
                Yii::log("Removing cached visibility for user " . $user["id"], CLogger::LEVEL_INFO);
                Yii::app()->cache_items_visibility->delete((int) $user["id"]);
            }
	    }
	    else {
	       if (!is_array($users)) {
	           $users = array((int) $users);
	       }
	       foreach ($users as $idUser) {
	           Yii::app()->cache_items_visibility->delete((int) $idUser);
	       }
	    }
	}


	/**
	 * Make sure we return always an array as a response, including exception code, if any.
	 * This is to allow caller to truly understand if something went wrong with the call to ES
	 *
	 * @param array
	 */
	protected function parseResponse($response, $exceptionCode=false) {

	    $result = array();

	    if (($exceptionCode !== false) && ($exceptionCode !== null)) {
	        $result['exception_code'] = $exceptionCode;
	    }

	    if (Docebo::isJson($response)) {
	        $result['message'] = CJSON::decode($response);
	    }
	    else if (is_string($response) || is_array($response)){
	        $result['message'] = $response;
	    }
	    else {
	        $result['message'] = print_r($response, true);
	    }

	    return $result;
	}



	/**
	 * Make sure we return always an array as a response, including exception code, if any.
	 * This is to allow caller to truly understand if something went wrong with the call to ES
	 *
	 * @param array
	 */
	protected function parseSearchResponse($response, $exceptionCode=false) {

	    $result = array();

	    if (($exceptionCode !== false) && ($exceptionCode !== null)) {
	        $result['exception_code'] = $exceptionCode;
	    }

	    if (Docebo::isJson($response)) {
	        $result['message'] = CJSON::decode($response);
	    }
	    else if (is_string($response)){
	        $result['message'] = $response;
	    }
	    else if (is_array($response)) {
	        $result = $response;
	    }
	    else {
	        $result['message'] = print_r($response, true);
	    }

	    return $result;
	}


	/**
	 * Strip all tags from the value. If it is an array, walk it recursively.
	 *
	 * @param mixed $value
	 */
	protected function stripTags($value) {
	    if (is_array($value)) {
	        array_walk_recursive($value , function(&$item, $key) {
	            $item = strip_tags($item);
	        });
	    }
	    else if (is_string($value)) {
	        $value = strip_tags($value);
	    }
	    return $value;
	}



    /**
     * @see CApplicationComponent::init()
     */
    public function init() {

        parent::init();

        spl_autoload_unregister(array('YiiBase','autoload'));
        Yii::import('common.vendors.elasticsearch2.autoload', true);
        Yii::import('common.vendors.aws2.aws-autoloader', true);
        spl_autoload_register(array('YiiBase','autoload'));

        $this->_module = Yii::app()->getModule('elasticsearch');

        $params = array();
        $hosts  = array (
            $this->host . ":" . $this->port,
        );


        $client = ClientBuilder::create();
        $client->setHosts($hosts);
        if ($this->useSignedRequest) {
            $credentials = new Credentials($this->awsKey, $this->awsSecret);
            $signature   = new SignatureV4();
            $signature->setRegionName($this->awsRegion);
            $signature->setServiceName(self::SERVICE_NAME);
            $middleware = new AwsSignatureMiddleware($credentials, $signature);
            $defaultHandler = ClientBuilder::defaultHandler();
            $awsHandler = $middleware($defaultHandler);
            $client->setHandler($awsHandler);
        }

        $this->_client = $client->build();


        // Set Event listeners
        if (!$this->disabled && !Yii::app() instanceof CConsoleApplication) {

            // When user logs in
            // If Legacy is inside LefacyWrapper IFRAME, skip this, 
            // because "login" concept changes: we login user legacy on every Hydra request
            // Visibility should be rebuilt on Hydra side
            if (!Yii::app()->legacyWrapper->hydraFrontendEnabled()) {
                Yii::app()->event->on(EventManager::EVENT_ON_AFTER_LOGIN,                   array($this, 'onAfterLogin'));
            }

        	// Course (add, change, delete)
        	Yii::app()->event->on(EventManager::EVENT_NEW_COURSE, 						array($this, 'onCoursePropChanged'));
        	Yii::app()->event->on(EventManager::EVENT_COURSE_PROP_CHANGED, 				array($this, 'onCoursePropChanged'));
        	Yii::app()->event->on(EventManager::EVENT_COURSE_DELETED, 					array($this, 'onCourseDeleted'));

        	// LO (create/save, delete)
        	Yii::app()->event->on(EventManager::EVENT_LO_AFTER_SAVE, 					array($this, 'onLoAfterSave'));
        	Yii::app()->event->on(EventManager::EVENT_DELETED_COURSE_LO, 				array($this, 'onDeletedCourseLo'));

			// Central Repository - Update Learning Repository Object
			Yii::app()->event->on(EventManager::EVENT_LRO_AFTER_SAVE, 					array($this, 'onLroAfterSave'));

        	// Plan (create/save), delete
        	Yii::app()->event->on(EventManager::EVENT_LEARNING_PLAN_AFTER_SAVE, 		array($this, 'onLearningPlanAfterSave'));
        	Yii::app()->event->on(EventManager::EVENT_DELETED_LEARNING_COURSEPATH, 		array($this, 'onDeletedLearningCoursepath'));

        	// 7020 KA create, update, delete
        	Yii::app()->event->on(EventManager::EVENT_APP7020_ASSET_CREATED, 		    array($this, 'onApp7020AssetUpdated'));
        	Yii::app()->event->on(EventManager::EVENT_APP7020_ASSET_UPDATED, 		    array($this, 'onApp7020AssetUpdated'));
        	Yii::app()->event->on(EventManager::EVENT_APP7020_ASSET_DELETED, 		    array($this, 'onApp7020AssetDeleted'));
        	Yii::app()->event->on(EventManager::EVENT_APP7020_ASSET_UNPUBLISHED,	    array($this, 'onApp7020AssetDeleted'));
        	Yii::app()->event->on(EventManager::EVENT_APP7020_BEFORE_ASSET_SAVE,	    array($this, 'onApp7020BeforeAssetSave'));


        	// App7020 channels
        	Yii::app()->event->on(EventManager::EVENT_APP7020_CHANNEL_BEFORE_VISIBILITY_UPDATED, 	array($this, 'onApp7020ChannelVisibilityUpdated'));
        	Yii::app()->event->on(EventManager::EVENT_APP7020_CHANNEL_VISIBILITY_UPDATED, 			array($this, 'onApp7020ChannelVisibilityUpdated'));
        	Yii::app()->event->on(EventManager::EVENT_APP7020_CHANNEL_BEFORE_VISIBILITY_DELETED, 	array($this, 'onApp7020ChannelVisibilityUpdated'));
        	Yii::app()->event->on(EventManager::EVENT_APP7020_CHANNEL_VISIBILITY_DELETED, 	        array($this, 'onApp7020ChannelVisibilityUpdated'));

        	Yii::app()->event->on(EventManager::EVENT_APP7020_CHANNEL_UPDATED, 	        			array($this, 'onApp7020ChannelUpdated'));
        	Yii::app()->event->on(EventManager::EVENT_APP7020_CHANNEL_TOGGLE_SHOW_HIDE,    			array($this, 'onApp7020ChannelToggleShowHide'));


        	// 7020 QA create, update, delete
        	Yii::app()->event->on(EventManager::EVENT_APP7020_QUESTION_CREATED, 	    array($this, 'onApp7020QuestionUpdated'));
        	Yii::app()->event->on(EventManager::EVENT_APP7020_QUESTION_UPDATED, 	    array($this, 'onApp7020QuestionUpdated'));
        	Yii::app()->event->on(EventManager::EVENT_APP7020_QUESTION_DELETED, 	    array($this, 'onApp7020QuestionDeleted'));

        	// 7020 QA->answer create, update, delete
        	Yii::app()->event->on(EventManager::EVENT_APP7020_ANSWER_CREATED, 	        array($this, 'onApp7020AnswerUpdated'));
        	Yii::app()->event->on(EventManager::EVENT_APP7020_ANSWER_UPDATED, 	        array($this, 'onApp7020AnswerUpdated'));
        	Yii::app()->event->on(EventManager::EVENT_APP7020_ANSWER_DELETED, 	        array($this, 'onApp7020AnswerDeleted'));

        	// User subscribed/unsubscribed to/from course
        	Yii::app()->event->on(EventManager::EVENT_USER_SUBSCRIBED_COURSE,           array($this, 'onUserSubscribedCourse'));
        	Yii::app()->event->on(EventManager::EVENT_USER_UNSUBSCRIBED, 	            array($this, 'onUserUnsubscribed'));

        	// User enrolled/unenrolled to/from Learnin Plan
        	Yii::app()->event->on(EventManager::EVENT_USER_ENROLLED_IN_LEARNING_PLAN,      array($this, 'onUserPlanEnrollmentChange'));
        	Yii::app()->event->on(EventManager::EVENT_USER_UNENROLLED_FROM_LEARNING_PLAN,  array($this, 'onUserPlanEnrollmentChange'));

        	// Member (user, group, branch,..) assigned/unassign to/from catalog
        	Yii::app()->event->on(EventManager::EVENT_MEMBER_ASSIGNED_TO_CATALOG,       array($this, 'onMemberAssignmentToCatalogChanged'));
        	Yii::app()->event->on(EventManager::EVENT_MEMBER_UNASSIGNED_FROM_CATALOG,   array($this, 'onMemberAssignmentToCatalogChanged'));


        	// Entries (courses, plans) assigned/unassigned to/from catalog
        	Yii::app()->event->on(EventManager::EVENT_ENTRY_ASSIGNED_TO_CATALOG,       array($this, 'onEntryAssignmentToCatalogChanged'));
        	Yii::app()->event->on(EventManager::EVENT_ENTRIES_ASSIGNED_TO_CATALOG,     array($this, 'onEntryAssignmentToCatalogChanged'));
        	Yii::app()->event->on(EventManager::EVENT_ENTRY_UNASSIGNED_FROM_CATALOG,   array($this, 'onEntryAssignmentToCatalogChanged'));
        	Yii::app()->event->on(EventManager::EVENT_ENTRIES_UNASSIGNED_FROM_CATALOG, array($this, 'onEntryAssignmentToCatalogChanged'));

        	// User is assigned/removed to/from a group
        	Yii::app()->event->on(EventManager::EVENT_USER_SUBSCRIBED_GROUP,            array($this, 'onUserSubscribedGroup'));
        	Yii::app()->event->on(EventManager::EVENT_USER_REMOVED_FROM_GROUP,          array($this, 'onUserRemovedFromGroup'));

        	// User assigned/removed to/from branch

        	Yii::app()->event->on(EventManager::EVENT_NEW_BRANCH_USER,                  array($this, 'onUserAssignedToBranch'));
        	Yii::app()->event->on(EventManager::EVENT_USER_ASSIGNED_TO_BRANCH,          array($this, 'onUserAssignedToBranch'));
        	Yii::app()->event->on(EventManager::EVENT_USER_REMOVED_FROM_BRANCH,         array($this, 'onUserRemovedFromBranch'));


        	Yii::app()->event->on(EventManager::EVENT_POWER_USER_OBJECTS_SELECTION_UPDATED, array($this, 'onPowerUserObjectsSelectionUpdated'));


        	Yii::app()->event->on(EventManager::EVENT_AFTER_COURSE_ADDITIONAL_FIELD_DELETED, array($this, 'onAfterCourseAdditionalFieldDeleted'));



        }

        // If this application run is User-aware
        $this->userAware = Yii::app() instanceof CWebApplication;

        //
        $this->getAvailableTypes();

    }


    /**
     *
     * @return ElasticSearchModule
     */
    public function getModule(){
        return $this->_module;
    }



    /**
     *
     * @return \Elasticsearch\Client
     */
    public function getClient() {
        return $this->_client;
    }


    /**
     * Perform a bulk action
     *
     *
     * @link https://www.elastic.co/guide/en/elasticsearch/guide/current/bulk.html
     *
     * @param string $action 'create', 'update', 'index', 'delete'
     * @param array  $documents Array of data, all documents, subject to the action
     * @param string $type Document type, ex. 'course', 'plan', ...
     * @param string $idAttributeName The name of the document field we consider it the ID of the document
     * @param string $dataFields List of field names to include in the Elasticsearch, among all incoming document fields
     * @param string $index The index name
     */
    public function bulk($action, $documents, $type, $idAttributeName, $dataFields=false, $index=false) {

        if ($this->disabled) {
            return;
        }


        Yii::log('>>> Start Elasticsearch Bulk Action: ' . $action, CLogger::LEVEL_INFO, self::LOG_CATEGORY);

        // Allow specifying a index other than current LMS domain-code
    	$index = ($index === false) ? Docebo::getOriginalDomainCode() : $index;

    	// Create index (if it does not exists yet)
    	Yii::app()->es->createIndex($index);

    	Yii::log('Index: ' . $index, CLogger::LEVEL_INFO, self::LOG_CATEGORY);
    	Yii::log('Type: ' . $type, CLogger::LEVEL_INFO, self::LOG_CATEGORY);
    	Yii::log('Number of documents: ' . count($documents), CLogger::LEVEL_INFO, self::LOG_CATEGORY);

    	try {
    		$params = array();

    		// Build parameters for BULK operation
    		foreach ($documents as $documentAttributes) {
    			$params['body'][] = array(
    					$action	=> array(
    							'_index'	=> $index,
    							'_type'		=> $type,
    							'_id'		=> $documentAttributes[$idAttributeName],
    					),
    			);

                if ($action != self::ACTION_DELETE) {
                    $bodyItem = array();
                    foreach ($documentAttributes as $fieldName => $fieldValue) {
						if (in_array($fieldName, $dataFields)) {
							$bodyItem[$fieldName] = $this->stripTags($fieldValue);
    				    }
                    }
                    $params['body'][] = $bodyItem;
                }

    		}

    		$response = array();
    		if (is_array($params) && !empty($params)) {
    		  $this->_client->bulk($params);
    		}

    		Yii::log('SUCCESS', CLogger::LEVEL_INFO, self::LOG_CATEGORY);

    	}
    	catch (Exception $e) {
    		$response = $e->getMessage();
    		$exceptionCode = $e->getCode();
    		Yii::log(var_export($response, true), CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
    	}

    	Yii::log('<<< Finished Elasticsearch Bulk Action', CLogger::LEVEL_INFO, self::LOG_CATEGORY);

    	return $this->parseResponse($response, $exceptionCode);

    }


    /**
     *
     * @param unknown $index
     */
    public function createIndex($index) {

        if ($this->disabled) {
            return;
        }


        try {

        	$exists = $this->_client->indices()->exists(array('index' => $index));
        	if ($exists) {
        		return;
        	}
        	Yii::log('>>> Start Elasticsearch CREATE Index: ' . $index, CLogger::LEVEL_INFO, self::LOG_CATEGORY);
            $params = array(
                'index'     => $index,
                'body'      => array(
                    'settings'  => array(
                        "number_of_shards"  => 1,
                        "analysis"  => array(
                            "filter"    => array(
                                "autocomplete_filter"   => array(
                                    "type"          => "edge_ngram",
                                    "min_gram"      => 3,
                                    "max_gram"      => 20,
                                ),
                            ),
                            "analyzer"  => array(
                                "autocomplete"  => array(
                                    "type"      => "custom",
                                    "tokenizer" => "standard",
                                    "filter"    => array(
                                        "lowercase",
                                        "autocomplete_filter",
                                    ),
                                ),
                            ),
                        ),
                    ),
                    'mappings'  => array(
                        "_default_"     => array(
                            "_all"  => array(
                                "type"      => "string",
                                "analyzer"  => "autocomplete",
                            ),
                        ),
                    ),

                )
            );
            $response = $this->_client->indices()->create($params);

            Yii::log('SUCCESS', CLogger::LEVEL_INFO, self::LOG_CATEGORY);
        }
        catch (Exception $e) {
            Yii::log('ERROR', CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
            $response = $e->getMessage();
            $exceptionCode = $e->getCode();
        }

        Yii::log('<<< Finished Elasticsearch CREATE Index', CLogger::LEVEL_INFO, self::LOG_CATEGORY);

        return $this->parseResponse($response, $exceptionCode);

    }


    /**
     * Delete a whole index
     *
     * @param string $index
     */
    public function deleteIndex($index) {

        if ($this->disabled) {
            return;
        }

        Yii::log('>>> Start Elasticsearch DELETE Index: ' . $index, CLogger::LEVEL_INFO, self::LOG_CATEGORY);

    	try {
    		$params['index'] = $index;
    		$response = $this->_client->indices()->delete($params);
    		Yii::log('SUCCESS', CLogger::LEVEL_INFO, self::LOG_CATEGORY);
    	}
    	catch (Exception $e) {
    	    Yii::log('ERROR', CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
    		$response = $e->getMessage();
    		$exceptionCode = $e->getCode();
    		Yii::log(var_export($response, true), CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
    	}

    	Yii::log('<<< Finished Elasticsearch DELETE Index', CLogger::LEVEL_INFO, self::LOG_CATEGORY);

    	return $this->parseResponse($response, $exceptionCode);
    }


    /**
     * Delete list of ES Types from a given index, i.e. delete all 'course', 'plan' etc. documents
     *
     * @param array|string $types
     * @param string $index
     */
    public function deleteTypes($types, $index=false) {

        if ($this->disabled) {
            return;
        }

        if (empty($types)) {
            return array(
                'message'   => 'No types passed, but this is not an error!',
            );
        }

        if (!is_array($types)) {
            $types = array($types);
        }

        $index = ($index === false) ? Docebo::getOriginalDomainCode() : $index;

        Yii::log('>>> Start Elasticsearch Mass TYPE deletion: ' . implode(',', $types), CLogger::LEVEL_INFO, self::LOG_CATEGORY);

        try {
            $params['index'] = array($index);
            $params['type']  = $types;
            $params['body']['query'] = array('match_all' => array());
            $response = $this->_client->deleteByQuery($params);
        }
        catch (Exception $e) {
            $response = $e->getMessage();
            $exceptionCode = $e->getCode();
        }

        $parsedResponse = $this->parseResponse($response, $exceptionCode);

        Yii::log('<<< Finished Elasticsearch Mass TYPE deletion', CLogger::LEVEL_INFO, self::LOG_CATEGORY);

        return $parsedResponse;

    }


    /**
     * Delete single document
     *
     * @param string $index
     * @param string $type
     * @param string $id
     *
     * @return array
     */
    public function deleteDocument($index, $type, $id) {

        if ($this->disabled) {
            return;
        }

        try {
            $response = $this->_client->delete(array(
                'index' => $index,
                'type'  => $type,
                'id'    => $id,
            ));
        }
        catch (Exception $e) {
            $response = $e->getMessage();
            $exceptionCode = $e->getCode();
        }

        return $this->parseResponse($response, $exceptionCode);
    }

    /**
     *
     * @param DEvent $event
     */
    public function onCoursePropChanged(DEvent $event) {
        if (isset($event->params['course']) && ($model=$event->params['course'])) {
            EsAgent::getAgent(EsAgentCourse::TYPE)->indexListOfDocuments(array($model->idCourse));
            $this->indexCourseLearningMaterials($model);
            $this->invalidateCaches($this->courseAffectedUsers($model->idCourse));
        }
    }

    /**
     *
     * @param DEvent $event
     */
    public function onCourseDeleted(DEvent $event) {
    	if (isset($event->params['course']) && ($model=$event->params['course'])) {
    		$this->deleteDocument(Docebo::getOriginalDomainCode(), EsAgentCourse::TYPE, $model->idCourse);
    		$this->invalidateCaches($this->courseAffectedUsers($model->idCourse));
    	}
    }


    /**
     *
     * @param DEvent $event
     */
    public function onLoAfterSave(DEvent $event) {
    	if (isset($event->params['learningobject']) && ($model=$event->params['learningobject'])) {
    		if (!empty($model->objectType)) {
    			EsAgent::getAgent(EsAgentLo::TYPE)->indexListOfDocuments(array($model->idOrg));
    			$this->invalidateCaches($this->courseAffectedUsers($model->idCourse));
    		}
    	}
    }

	/**
	 * @param DEvent $event
	 */
	public function onLroAfterSave(DEvent $event) {
		if (isset($event->params['learningobject']) && ($model=$event->params['learningobject'])) {
			if (!empty($model->objectType)) {
				EsAgent::getAgent(EsAgentLo::TYPE)->indexListOfDocuments(array($model->idOrg));
				$this->invalidateCaches($this->courseAffectedUsers($model->idCourse));
			}
		}
	}

    /**
     *
     * @param DEvent $event
     */
    public function onDeletedCourseLo(DEvent $event) {
    	if (isset($event->params['learningobject']) && ($model=$event->params['learningobject'])) {
    		$this->deleteDocument(Docebo::getOriginalDomainCode(), EsAgentLo::TYPE, $model->idOrg);
    		$this->invalidateCaches($this->courseAffectedUsers($model->idCourse));
    	}
    }

    /**
     *
     * @param DEvent $event
     */
    public function onLearningPlanAfterSave(DEvent $event) {
    	if (isset($event->params['model']) && ($model=$event->params['model'])) {
    		EsAgent::getAgent(EsAgentPlan::TYPE)->indexListOfDocuments(array($model->id_path));
    		$this->invalidateCaches($this->planAffectedUsers($model));
    	}
    }

    /**
     *
     * @param DEvent $event
     */
    public function onDeletedLearningCoursepath(DEvent $event) {
    	if (isset($event->params['model']) && ($model=$event->params['model'])) {
    		$this->deleteDocument(Docebo::getOriginalDomainCode(), EsAgentPlan::TYPE, $model->id_path);
    		$this->invalidateCaches($this->planAffectedUsers($model));
    	}
    }

    /**
     *
     * @param unknown $idUser
     */
    public function orderVisibilityGeneration($users, $types=false) {

        if (!isset(Yii::app()->cache_items_visibility)) {
            return;
        }

        $params = array(
            'task'          => EsJobHandler::TASK_CACHE_USER_ITEMS_VISIBILITY,
            'users'         => $users,
        );
        if ($types !== false && is_array($types)) {
            $params['types'] = $types;
        }

        $job = Yii::app()->scheduler->createImmediateJob(EsJobHandler::TASK_CACHE_USER_ITEMS_VISIBILITY, EsJobHandler::id(), $params);

        if (!$job) {
            Yii::log('Error while creating ElasticSearch job');
        }

    }

    /**
     *
     * @param DEvent $event
     */
    public function onAfterLogin(DEvent $event) {
    	if (!isset($event->params['user']) || !( ($userModel = $event->params['user']) instanceof CoreUser)) {
    		return;
    	}
    	// We always order visibility cache building after login
    	$this->orderVisibilityGeneration(array($userModel->idst), false, false);
    }


    /**
     *
     * @param DEvent $event
     */
    public function onApp7020AssetUpdated(DEvent $event) {
    	/** @var App7020Assets $model */
        if (isset($event->params['model']) && ($model=$event->params['model'])) {
        	// If Asset is approved : index it
        	if ((int) $model->conversion_status === (int) App7020Assets::CONVERSION_STATUS_APPROVED) {
            	EsAgent::getAgent(EsAgentKnowledgeAsset::TYPE)->indexListOfDocuments(array($model->id));
        	}
        	// If it is not - delete it
        	else {
        		$this->deleteDocument(Docebo::getOriginalDomainCode(), EsAgentKnowledgeAsset::TYPE, $model->id);
        	}
            if (isset($model->id)) {
                $this->invalidateCaches($this->knowledgeAssetAffectedUsersDataProvider($model));
            }
        }
    }


    /**
     *
     * @param DEvent $event
     */
    public function onApp7020ChannelVisibilityUpdated(DEvent $event) {
    	// First, update all Channel-affected users (invalidate visibility)
    	if (isset($event->params['model']) && ($model=$event->params['model'])) {
    	    if (isset($model->idChannel)) {
    		  $this->invalidateCaches($this->channelVisibilityAffectedUsersDataProvider($model));
    	    }
    	}

    	// Index all assets related to the channel
    	if (in_array($event->event_name, array(EventManager::EVENT_APP7020_CHANNEL_VISIBILITY_DELETED, EventManager::EVENT_APP7020_CHANNEL_VISIBILITY_UPDATED))) {
    		if (isset($model->idChannel)) {
    	   		$affectedAssetsDp = $this->channelVisibilityAffectedAssetsDataProvider($model);
				$this->indexAssetsFromDataProvider($affectedAssetsDp);
    		}
    	}
    }



    public function onApp7020ChannelUpdated(DEvent $event) {
    	if (isset($event->params['model']) && ($model=$event->params['model'])) {
    		if (isset($model->id)) {
    			$visRecords = App7020ChannelVisibility::model()->findAllByAttributes(array("idChannel" => $model->id));
    			if (!empty($visRecords)) {
    				foreach ($visRecords as $visModel) {
     					$this->invalidateCaches($this->channelVisibilityAffectedUsersDataProvider($visModel));
     					$this->indexAssetsFromDataProvider($this->channelVisibilityAffectedAssetsDataProvider($visModel));
    				}
    			}
    		}
    	}
    }

	/**
	 * @param DEvent $event
	 */
	public function onApp7020ChannelToggleShowHide(DEvent $event) {
		if (isset($event->params['model']) && ($model=$event->params['model'])) {
			if(isset($model->id)) {
				$params = array(
					"idChannel" => (int) $model->id,
					"idsOnly" => true,
				);
				$dp = DataProvider::factory("ChannelAssets", $params)->provider();
				$this->indexAssetsFromDataProvider($dp);
			}
		}
	}

    /**
     *
     * @param DEvent $event
     */
    public function onApp7020AssetDeleted(DEvent $event) {
        if (isset($event->params['model']) && ($model=$event->params['model'])) {
            $this->deleteDocument(Docebo::getOriginalDomainCode(), EsAgentKnowledgeAsset::TYPE, $model->id);
        }
    }

    public function onApp7020BeforeAssetSave(DEvent $event) {
        if (isset($event->params['model']) && ($model=$event->params['model'])) {
            $this->invalidateCaches($this->knowledgeAssetAffectedUsersDataProvider($model));
        }
    }

    public function onApp7020QuestionUpdated(DEvent $event) {
        if (isset($event->params['model']) && ($model=$event->params['model'])) {
            EsAgent::getAgent(EsAgentQandA::TYPE)->indexListOfDocuments(array($model->id));
        }
    }

    public function onApp7020QuestionDeleted(DEvent $event) {
        if (isset($event->params['model']) && ($model=$event->params['model'])) {
            $this->deleteDocument(Docebo::getOriginalDomainCode(), EsAgentQandA::TYPE, $model->id);
        }
    }

    public function onApp7020AnswerUpdated(DEvent $event) {
        if (isset($event->params['model']) && ($model=$event->params['model'])) {
        	if ($model->question instanceof App7020Question && $model->question) {
            	EsAgent::getAgent(EsAgentQandA::TYPE)->indexListOfDocuments(array($model->question->id));
        	}
        }
    }

    public function onApp7020AnswerDeleted(DEvent $event) {
        if (isset($event->params['model']) && ($model=$event->params['model'])) {
        	if ($model->question instanceof App7020Question && $model->question) {
        	    // Just reindex the question in Elasticsearch
        	    EsAgent::getAgent(EsAgentQandA::TYPE)->indexListOfDocuments(array($model->question->id));
        	}
        }
    }


    /**
     *
     * @param DEvent $event
     */
    public function onUserUnsubscribed(DEvent $event) {
        if (isset($event->params['user']) && ($model=$event->params['user'])) {
            $this->invalidateCaches(array($model->idst));
        }
    }


    /**
     *
     * @param DEvent $event
     */
    public function onUserSubscribedCourse(DEvent $event) {
        if (isset($event->params['user']) && ($model=$event->params['user'])) {
            $this->invalidateCaches(array($model->idst));
        }
    }



    /**
     *
     * @param DEvent $event
     */
    public function onUserPlanEnrollmentChange(DEvent $event) {
        if (isset($event->params['user_id'])) {
            $idUser = (int) $event->params['user_id'];
            $this->invalidateCaches(array($idUser));
        }
        else if (isset($event->params['user']) && ($userModel=$event->params['user'])) {
            $this->invalidateCaches(array($userModel->idst));
        }
    }


    /**
     *
     * @param DEvent $event
     */
    public function onMemberAssignmentToCatalogChanged(DEvent $event) {
        if (isset($event->params['model']) && ($model=$event->params['model'])) {
            $this->invalidateCaches($model->getMemberUsers());
        }
    }

    /**
     *
     * @param DEvent $event
     */
    public function onEntryAssignmentToCatalogChanged(DEvent $event) {
        $idCatalog = false;
        if (isset($event->params['model']) && ($model=$event->params['model']) && ($model instanceof LearningCatalogueEntry)) {
            $idCatalog = $model->idCatalogue;
        }
        else if (isset($event->params['idCatalogue'])) {  //
            $idCatalog = (int) $event->params['idCatalogue'];
        }

        if ($idCatalog !== false) {
            $this->invalidateCaches(LearningCatalogue::getCatalogUsers(array($idCatalog)));
        }
    }

    /**
     *
     * @param DEvent $event
     */
    public function onUserSubscribedGroup(DEvent $event) {
        if ($event->sender instanceof CoreGroupMembers) {
            $this->invalidateCaches(array($event->sender->idstMember));
        }
    }


    /**
     *
     * @param DEvent $event
     */
    public function onUserRemovedFromGroup(DEvent $event) {
        if ($event->sender instanceof CoreGroupMembers) {
            $this->invalidateCaches(array($event->sender->idstMember));
        }
    }


    /**
     *
     * @param DEvent $event
     */
    public function onUserAssignedToBranch(DEvent $event) {
        if ($event->sender instanceof CoreGroupMembers) {
            $this->invalidateCaches(array($event->sender->idstMember));
        }
    }

    /**
     *
     * @param DEvent $event
     */
    public function onUserRemovedFromBranch(DEvent $event) {
        if (isset($event->params['targetUser'])) {
            $this->invalidateCaches(array((int) $event->params['targetUser']));
        }
    }



    public function onPowerUserObjectsSelectionUpdated(DEvent $event) {
        if (isset($event->params['puser_id'])) {
            $this->invalidateCaches(array((int) $event->params['puser_id']));
        }
    }


    /**
     *
     * @param DEvent $event
     */
    public function onAfterCourseAdditionalFieldDeleted(DEvent $event) {
    	$type = $event->params['field_type'];
    	if ($type && in_array($type, array(LearningCourseField::TYPE_FIELD_TEXTAREA, LearningCourseField::TYPE_FIELD_TEXTFIELD))) {
    		Yii::log("Course Additional field has been deleted. Reindexing LMS Courses...", "info", "es");
    		EsAgent::getAgent(EsAgentCourse::TYPE)->index();
    	}
    }

    /**
     * Reset THIS LMS index (the whole index!)
     */
    public function resetIndex() {
    	$domainCode = Docebo::getOriginalDomainCode();
    	$this->deleteIndex($domainCode);
    	$this->createIndex($domainCode);
    	$this->massIndex();
    }


    /**
     * Massive index of THIS LMS content (everything available to index)
     */
    public function massIndex() {
    	$agents = $this->getAvailableTypes();
    	$responses = EsAgent::massIndex($agents);
    	foreach ($responses as $response) {
    		if (isset($response['exception_code'])) {
    			Yii::log('Something went wrong with ElastiSearch indexing: ' . print_r($response,true), CLogger::LEVEL_ERROR, EsClient::LOG_CATEGORY);
    			Yii::log('Exception code: ' . $response['exception_code'], CLogger::LEVEL_ERROR, EsClient::LOG_CATEGORY);
    			Yii::log($response, CLogger::LEVEL_ERROR, EsClient::LOG_CATEGORY);
    		}
    	}
    }



    /**
     *
     * @param unknown $search
     * @param string $type
     * @param string $from
     * @param string $pageSize
     * @param string $phraseSearch
     * @param string $minScore
     * @param string $index
     *
     * @return array
     */
    public function search(	$search, $type=false, $from=false, $pageSize=false, $minScore=false, $index=false) {


        // Pre-load Items visibility for current user
        $this->loadCachedVisibility();

        $emptyResult = array(
            'hits'                  => array(),
            'max_score'             => 0,
            'next_from'             => 0,
            'grand_total'           => 0,
            'end_of_results'        => 1,
        );


        $search      = trim($search);
        $isQuoted    = Docebo::isStringQuoted($search);
        $search      = trim($search, "'\"");
        $search		 = strtolower($search); // All information is lowercased by ElasticSearch analyzer
        $index       = ($index === false) ? Docebo::getOriginalDomainCode() : $index;
        $from        = $from === false ? 0 : (int) $from;
        $pageSize    = $pageSize === false ? self::RESULT_PAGE_SIZE : (int) $pageSize;
        $minScore    = $minScore === false ? self::MIN_SCORE : floatval($minScore);
        $exactPhrase = $isQuoted ? true : false;
        $slop        = $exactPhrase === true ? null : self::SLOP_VALUE;
        $batchSize   = $pageSize;

        $batchSize   = $pageSize;

        $params = array(
            'size'          => $batchSize,
            'index'         => $index,
            'body'          => array(
                'track_scores'  => true,
                'min_score'     => $minScore,
            ),
            'sort'  => array(
                '_score'    => array(
                    'order'    => 'desc',
                ),
            ),
        );

        // AWS SignatureV4 has a little bug and mistreats zeroes (0, '0'), so do not send FROM if it is 0
        if ($from) {
            $params['from'] = (int) $from;
        }

        $query = array(
            'filtered' => array(
                'query'	=> array(
                	'bool' => array(

                		// Document SHOULD have in ANY of its fields a string SIMILAR to <search>
                		// and the <string> is relevant, having <score> higher then $minScore
                		'should' => array(
							'dis_max' => array(
								'queries' => array(
                				
									array(
										'match' =>  array(
											"_all"  => array(
												"query"   => $search,
												"type"    => "phrase",
												"slop"    => $slop,
											),
										),
									),

									// Increase the weight of the title of the document
									array(
										'match' => array(
											'name' => array(
												"query"   => $search,
												"boost" => 1.5,
											)
										)
									),

									// .. OR ANY of document's "tag" is EXACTLY equal to <search>
									array(
										'constant_score' => array(
											'filter' => array(
												'terms' =>  array(
													"tags"  => array($search)
												),
											),
											"boost" => 5
										),
									),
								),
							),
                				
                		),
						// Exclude knowledge assets, for which all related channels are disabled
						'must_not' => array(
							"terms"	=> array(
								"isRelatedToDisabledChannels" => array(1),
							),
						),
                	),
                ),
            ),
        );

        $params['body']['query'] = $query;

        // If script is user-aware (i.e. user has meaning in this call)
        $filterArray = array();
        if ($this->userAware) {
        	// Non-admins must obey more rules & visibility checks
        	if (!Yii::app()->user->isGodAdmin) {
        		$filterArray = $this->buildElasticsearchVisibilityFilter($type);
        	}
        	// While Admin must just NOT see disabled Types, e.g. C&S disabled
        	else {
        		$mustNots = $this->buildDisabledTypesFilter();
        		if (!empty($mustNots)) {
        			$filterArray['must_not'] = $mustNots;
        		}
        	}
        }

        if (!empty($filterArray)) {
        	$params['body']['query']['filtered']['filter']["bool"] = $filterArray;
        }
        else if (!Yii::app()->user->isGodAdmin) {
        	return $emptyResult;
        }


        if ($type) {
            $params['type'] = $type;
        }

        try {
            $hitsCollected      = array();      // All collected hits for this function call (single search)
            $globalIndexPointer = $from;        // The "running" pointer through results (starts from $from and goes up)
            $grandTotal         = 0;            // Number of hits based on the search terms, no filtering whatsoever, just total hits
            $nextFrom           = false;        // What should be the next FROM index, if any
            $needMoreN          = $pageSize;    // How many hits we need more to fill the search "page"
            $endOfResults       = false;        // Is the global pointer ended up at end of results?

            // Cycle and return results in batches until we get what we need or end-of-results is reached
            $cycles = 0;

            while ((count($hitsCollected) < $pageSize)) {
                $cycles++;


                $response = $this->_client->search($params);

                $maxScore = $response['hits']['max_score'];

                // Prepare the next FROM
                $params['from'] = $params['from'] + $batchSize;


                // If there are new hits, collect data
                if (count($response['hits']['hits']) > 0) {

                    // Update Grand Total (should not change anyway during cycles)
                    $grandTotal         = $response['hits']['total'];

                    // Collect valid hits into a local variable
                    $hitsCollected      = array_merge($hitsCollected, $response['hits']['hits']);

                    // Set the next "from", in case we break the cycle below
                    $nextFrom           = $globalIndexPointer + count($response['hits']['hits']);

                    // Update the global index
                    $globalIndexPointer = $nextFrom;

                    // If we collected enough valid hits (page size), break the cycle
                    if (count($hitsCollected) >= $pageSize) {
                        break;
                    }

                }
                // .. otherwise.. break the cycle
                else {
                    $endOfResults = true;
                    break;
                }

                $needMoreN = $pageSize - count($hitsCollected);

                // Leave the cycle if GrandTotal is less than a single batch size
                if ($grandTotal <= $batchSize) {
                	$endOfResults = true;
                	break;
                }

            }

            // Compose the result
            $result = array(
                'hits'                  => $hitsCollected,
                'max_score'             => $maxScore,
                'next_from'             => $nextFrom,
                'grand_total'           => $grandTotal,
                'end_of_results'        => $endOfResults,
            );

            return $result;
        }
        catch (Exception $e) {
            Yii::log('ERROR', CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
            Yii::log('Full ES Response was: ', CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
            $response = $e->getMessage();
            Yii::log(var_export($response, true), CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
            $exceptionCode = $e->getCode();
            return $this->parseSearchResponse($response, $exceptionCode);
        }

    }

    public function setUserCatalogs($userCatalogs) {
        $this->_userCatalogs = $userCatalogs;
    }


    public function getUserCatalogs() {
        return $this->_userCatalogs;
    }


    /**
     * Make list of available Type Agents (EsAgentCourse, EsAgentLo,....)
     *
     * NOTE: <font color=red>DO NOT FILTER THIS BY Plugin status! This MUST BE THE FULL LIST of all <strong>possible</strong> document types and agents</font>
     */
    public function getAvailableTypes() {
        if ($this->availableTypes) {
            return $this->availableTypes;
        }
        $files = CFileHelper::findFiles(Yii::getPathOfAlias("elasticsearch.components.agents"), array('fileTypes' => array('php')));
        if (is_array($files)) {
            foreach ($files as $file) {
                $fname = pathinfo($file, PATHINFO_FILENAME);
                if (!in_array($fname, array('EsAgent', 'IEsAgent'))) {
                    $this->availableTypes[] = $fname;
                }
            }
        }
        return $this->availableTypes;
    }


    /**
     * List of users affected by change of a Course - through direct enrollment and through catalog membership of the course
     * @param integer $id  Course ID
     */
    public function courseAffectedUsers($id) {

        $affectedUsers = array();

        // Enrolled users
        $enrollments = LearningCourseuser::getEnrollments($id);
        if (is_array($enrollments)) {
            foreach ($enrollments as $enrollment) {
                $affectedUsers[] = $enrollment['idUser'];
            }
        }

        // Affected through catalogs
        $catalogs = LearningCatalogue::getCourseCatalogs($id);
        if (is_array($catalogs)) {
            $affectedUsers = array_merge($affectedUsers, LearningCatalogue::getCatalogUsers($catalogs));
        }

        return array_values(array_unique($affectedUsers));
    }

    /**
     * List of users affected by change of a Learning Plan - through catalog membership of the plan
     * @param LearningCoursepath $model  Learning Plan model
     */
    public function planAffectedUsers($model) {
        // Enrolled
        $affectedUsers  = $model->getEnrolledUsers();

        // Through catalogs
        $catalogs = LearningCatalogue::getPlanCatalogs($model->id_path);
        if (is_array($catalogs)) {
            $affectedUsers = array_merge($affectedUsers, LearningCatalogue::getCatalogUsers($catalogs));
        }

        return array_values(array_unique($affectedUsers));
    }


    /**
     * Return data provider for users affected by change of an Asset
     *
     * @param App7020Assets $model
     * @return CSqlDataProvider
     */
    public function knowledgeAssetAffectedUsersDataProvider($model) {
    	$params = array(
    		"idAsset" 					=> (int) $model->id,
    		"ignoreVisallChannels" 		=> true,
    		"ignoreGodAdminEffect" 		=> true,
    		"idsOnly" 					=> true,
    		"ignoreInvited" 			=> true,
    	);
    	$dp = DataProvider::factory("AssetUsers", $params)->provider();
    	return $dp;
    }


    /**
     * Return data provider for users affected by change of a Channel-Visibility
     *
     * @param App7020ChannelVisibility $model
     * @return CSqlDataProvider
     */
    public function channelVisibilityAffectedUsersDataProvider($model) {
    	$params = array(
    		"idChannel" 				=> (int) $model->idChannel,
    		"ignoreVisallChannels" 		=> true,
    		"ignoreGodAdminEffect" 		=> true,
    		"idsOnly" 					=> true,
    		"ignoreInvited" 			=> true,
    	);
    	$dp = DataProvider::factory("ChannelUsers", $params)->provider();
    	return $dp;
    }

    /**
     * Return data provider for ASSETS affected by change of a Channel-Visibility
     *
     * @param App7020ChannelVisibility $model
     * @return CSqlDataProvider
     */
    public function channelVisibilityAffectedAssetsDataProvider($model) {
    	$params = array(
   			"idChannel" 				=> (int) $model->idChannel,
   			"idsOnly" 					=> true,
    	);
    	$dp = DataProvider::factory("ChannelAssets", $params)->provider();
    	return $dp;
    }

    /**
     * Build visibility cache file for selected user (one user!)
     *
     * @param integer $idUser
     * @param array $types
     */
    public function buildVisibility($idUser, $types=false) {
        Yii::log("Building visibility cache for user: " . $idUser, CLogger::LEVEL_INFO, "es");
        $allTypes   = Yii::app()->es->getAvailableTypes();
        $types      = is_array($types) ? $types : $allTypes;
        $v          = Yii::app()->cache_items_visibility->get($idUser);
        if (!is_array($v)) {
        	$v = array();
        }
        foreach ($types as $type) {
        	$agent = EsAgent::getAgent($type);
        	if ($agent && $agent->isAvail()) {
        		Yii::log('Type:' . $type, CLogger::LEVEL_INFO, "es");
            	$v[$type] = $agent->buildVisibilityForUser($idUser);
        	}
        }
        return $v;
    }


}


