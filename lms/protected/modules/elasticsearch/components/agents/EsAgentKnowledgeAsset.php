<?php
/**
 * 
 * Elasticsearch agent to index/search 7020 Knowledge Assets
 *
 */
class EsAgentKnowledgeAsset extends EsAgent implements IEsAgent  {
	
	const TYPE = __CLASS__;
	const TYPE_SHORT = 'kna';
	
	
    /**
     *
     * @var string
     */
    protected $es_type          = self::TYPE;
    
    /**
     * Final list of fields for Elasticsearch
     * @var array
     */
    protected $fields = array();
    
    /**
     * ID field name
     * @var string
     */
    protected $idName = 'id';
    
    
    /**
     * 
     * {@inheritDoc}
     * @see IEsAgent::isAvail()
     */
    public function isAvail() {
    	return PluginManager::isPluginActive('Share7020App');
    }
    
    /**
     * 
     */
    public function __construct() {
        $this->fields = array('title', 'description','tags','channels','channelIds','visibleToAll','isRelatedToDisabledChannels');
    }
     
    /**
     *
     * {@inheritDoc}
     * @see IEsAgent::buildCommandForListOfDocuments()
     */
    public function buildCommandForListOfDocuments($ids=false) {
        
        $commandBase    = Yii::app()->db->createCommand();
        $commandCounter = Yii::app()->db->createCommand();
        
        $commandBase->from('app7020_content content');
        
        // To get tags, comma separated list (group_concat)
        $commandBase->leftJoin('app7020_tag_link tl', 'content.id=tl.idContent');
        $commandBase->leftJoin('app7020_tag tag', 'tl.idTag=tag.id');
        
        // To get channels (all translations), comma separated list (group_concat)
        $commandBase->leftJoin('app7020_channel_assets ca', 'content.id=ca.idAsset AND ca.asset_type='.App7020ChannelAssets::ASSET_TYPE_COACH_SHARE_ASSET);
        $commandBase->leftJoin('app7020_channel_translation translation', 'translation.idChannel=ca.idChannel');
        
        // Also, join channel visibility so we can calculate "visible to ALL = true/false)
        $commandBase->leftJoin('app7020_channel_visibility cv', 'cv.idChannel=ca.idChannel');

		// Join channels to get the number of enabled channels
		$commandBase->leftJoin('app7020_channels channels', 'channels.id=ca.idChannel');
        
        
        // Group by idOrg (allows group_concat)
        $commandBase->group('content.id');
        
        if ($ids !== false && is_array($ids) && !empty($ids)) {
            $commandBase->andWhere(array('IN', 'content.id', $ids));
        }
        
        $commandData    = clone $commandBase;
        
        $selectArray = array(
	  		'content.id',
	    	'title',
	    	'content.description as description',
        	'content.is_private as isPrivate', 
        	'GROUP_CONCAT(DISTINCT(tag.tagText)) as allTagsString', 
        	'GROUP_CONCAT(DISTINCT(translation.name)) as allChannelsString', 
        	'GROUP_CONCAT(DISTINCT(translation.idChannel)) as allChannelIds', 
        	'GROUP_CONCAT(DISTINCT(cv.idChannel)) AS visibleToAll',
			'SUM(channels.enabled) AS numberOfEnabledChannels'
		);
        $select         = implode(',', $selectArray);
        $commandData->select($select);
        
        //var_dump($commandData->text);die;
        
        $commandCounter->select('count(*)');
        $commandCounter->from("(" . $commandData->text . ") SUBTABLE");

        $result = array(
            'commandCounter'    => $commandCounter,
            'commandData'       => $commandData,
        );
        
        return $result;
        
    }
    

    public function getEsDocumentTitle($document) {
    	return $document['title'];
    }
    
    public function getLocalData($id) {
    	$result = array(
    		'model'	=> App7020Assets::model()->findByPk($id),
    	);
    	return $result;
    }
 
    
    /**
     *
     * {@inheritDoc}
     * @see EsAgent::bulkIndexTypeDataBatch()
     */
    public function bulkIndexTypeDataBatch($page, $data) {
    	
        $documents = array();
        foreach ($data as $row) {
            $tags             = explode(",", $row['allTagsString']);
            $channels         = explode(",", $row['allChannelsString']);
            $channelIds       = explode(",", $row['allChannelIds']);
            // Assets not realated to any channel AND NOT private are visible to all (1)
            $visibleToAll     = empty($row['visibleToAll']) && !$row["isPrivate"] ? 1 : 0;
            $isRelatedToDisabledChannels = $row['numberOfEnabledChannels'] > 0 || $row["isPrivate"] ? 0 : 1;
            $documents[]      = array_merge($row, array(
                'tags'          => $tags,
                'channels'      => $channels,
                'channelIds'    => $channelIds, 
                'visibleToAll'  => $visibleToAll,
                'isRelatedToDisabledChannels' => $isRelatedToDisabledChannels,
            ));
        }
        return $this->bulkIndex($documents, $this->idName);
    }
    
    /**
     *
     * @param unknown $idUser
     */
    public function buildVisibilityForUser($idUser) {
    	if (!isset(Yii::app()->cache_items_visibility)) {
    		return;
    	}
    	
    	if (!$this->isAvail()) {
    		return array();
    	}
    
    	$type 			= self::TYPE;
    	$allItems       = array();
    	
    	// 1. Get all assets explicitly visible to the user
    	$dp = DataProvider::factory("UserAssets", array(
    	    "idUser"               => $idUser,
    	    "ignoreVisallChannels" => true, // "visible to all" items are maerked as such in ElasticSearch and will be visible to all, noo need to add them here
    	    "ignoreGodAdminEffect" => true, // Admins will see everything in search anyway 
    	    "conversionStatus"     => App7020Assets::CONVERSION_STATUS_APPROVED,   // Approved only
    	))->provider();
    	
    	// Get iterator and iterate over all items (iterator does not paginate!)
    	$iterator = new CDataProviderIterator($dp);
    	foreach ($iterator as $asset) {
    	    $allItems[] = $asset['id'];
    	}
    	
    	//----- finalize
     	return $allItems;
    	
    }
    
    
    
}