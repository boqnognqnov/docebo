<?php
/**
 *
 * Elasticsearch agent to index/search Courses
 *
 */
class EsAgentCourse extends EsAgent implements IEsAgent  {

	const TYPE         = __CLASS__;
	const TYPE_SHORT = 'crs';

    /**
     *
     * @var string
     */
    protected $es_type          = self::TYPE;

    /**
     * Fields (SQL) to include in the indexed documents
     * @var array
     */
    protected $fields = array(
        'idCourse',
        'name',
        'description',
        'code',
    	'show_rules',
   		'additional_fields',
		'status'
    );


    /**
     *
     * @var string
     */
    protected $idName = 'idCourse';

    /**
     *
     * {@inheritDoc}
     * @see IEsAgent::isAvail()
     */
    public function isAvail() {
    	return true;
    }


    /**
     *
     * {@inheritDoc}
     * @see IEsAgent::buildCommandForListOfDocuments()
     */
    public function buildCommandForListOfDocuments($ids=false) {

        $commandBase = Yii::app()->db->createCommand();
        $commandBase->from('learning_course course');

        if ($ids !== false && is_array($ids) && !empty($ids)) {
            $commandBase->andWhere(array('IN', 'idCourse', $ids));
        }


        $selectArray = array(
			'idCourse',
			'name',
			'description',
			'code',
			'show_rules',
			'status'
        );

        // Index also Course Additional fields, field values concatenated into one string
        // First, get list of fields, text type only
        // If we got fields, create proper SELECT element for every course
        $additionalFields = LearningCourseField::getAllFieldNames();
        if (!empty($additionalFields) && is_array($additionalFields)) {
        	$commandBase->leftJoin('learning_course_field_value fv', 'fv.id_course=course.idCourse');
        	foreach ($additionalFields as $index => $field) {
        		$additionalFields[$index] = "COALESCE(fv.$field, '')";  // Very important!!!!!!!!!
        	}
        	$concat = implode(", ' ',", $additionalFields);
        }
        else {
        	$concat = "''";
        }
        $selectArray[] = 'CONCAT('.$concat.') as additional_fields';


        $commandCounter = clone $commandBase;
        $commandData    = clone $commandBase;

        $commandCounter ->select('count(*)');


        $commandData->select($selectArray);

        $result = array(
            'commandCounter'    => $commandCounter,
            'commandData'       => $commandData,
        );

        return $result;
    }


    /**
     *
     * {@inheritDoc}
     * @see IEsAgent::getEsDocumentTitle()
     */
    public function getEsDocumentTitle($document) {
    	return $document['name'];
    }

    /**
     * $result array of structured data returned to te renderer/view/
     *
     * {@inheritDoc}
     * @see IEsAgent::getLocalData()
     */
    public function  getLocalData($id, $idUser=false) {

        $results = array();

        $isEnrolled = true;
        $courseType = "";
        if (!$idUser) {
    		$idUser = Yii::app()->user->id;
        }

        $courseModel = LearningCourse::model()->findByPk($id);
        if ($courseModel) {
        	$courseType = $courseModel->course_type;
        }

        $enrollment = LearningCourseuser::model()->findByAttributes(array("idUser" => $idUser, "idCourse" => $id));
       	$isEnrolled = !empty($enrollment);
        $lockedCourse = $this->isCourseLocked($enrollment, $courseModel);

        $results = array(
            'lockedCourse'    	=> $lockedCourse,
        	'isEnrolled'		=> $isEnrolled,
        	'courseModel'		=> $courseModel,
        );

        return $results;
    }


    /**
     *
     * @param unknown $idUser
     */
    public function buildVisibilityForUser($idUser) {

        if (!isset(Yii::app()->cache_items_visibility)) {
    		return;
    	}

        if (!$this->isAvail()) {
    		return array();
    	}

    	$type 			= self::TYPE;
    	$allItems       = array();

    	// 1. Courses user is enrolled in
    	$params = array(
    	    ':idUser' => $idUser
    	);
    	$command = Yii::app()->db->createCommand();
    	$command->select('lcu.idCourse');
    	$command->from('learning_courseuser lcu');
    	$command->where('lcu.idUser=:idUser');
    	if (($items  = $command->queryColumn($params)) && is_array($items )) {
    	    $allItems = array_merge($allItems, $items);
    	}

    	// 2. Courses Visible through catalogs
        $userCatalogs = LearningCatalogue::getUserCatalogs($idUser);

        if (is_array($userCatalogs) && !empty($userCatalogs)) {
            $params = array(
                ':entryType'	=> LearningCatalogueEntry::ENTRY_COURSE,
            );
            $command = Yii::app()->db->createCommand();
            $command->select("lce.idEntry");
            $command->from("learning_catalogue lc");
            $command->join("learning_catalogue_entry lce", "lc.idCatalogue=lce.idCatalogue AND lce.type_of_entry=:entryType");
            $command->join("learning_course course", "(course.idCourse=lce.idEntry) AND (course.show_rules IN (0,1))");
            $command->andWhere(array('IN', 'lc.idCatalogue', $userCatalogs));
            if (($items  = $command->queryColumn($params)) && is_array($items )) {
                $allItems = array_merge($allItems, $items);
            }
        }

        // 3. Courses Visible because user is a power user of a course
        $params = array(
            ':idUser' => $idUser
        );
        $command = Yii::app()->db->createCommand();
        $command->select("cupuc.course_id");
        $command->from("core_user_pu_course cupuc");
        $command->where("cupuc.puser_id=:idUser");

        if (($items  = $command->queryColumn($params)) && is_array($items )) {
            $allItems = array_merge($allItems, $items);
        }

        // 4. Courses Visible because user is a power user of the Catalog where courses are in
        $items = CoreAdminCourse::getPowerUserEntriesOfPuCatalogs($idUser, CoreAdminCourse::TYPE_COURSE);
        $allItems = array_merge($allItems, $items);

        //----- finalize
        return $allItems;

    }


}