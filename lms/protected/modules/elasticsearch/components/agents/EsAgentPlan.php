<?php
/**
 * 
 * Elasticsearch agent to index/search Learning Plans
 *
 */
class EsAgentPlan extends EsAgent implements IEsAgent  {
    
    const TYPE = __CLASS__;
    const TYPE_SHORT = 'lp';
    
    /**
     *
     * @var unknown
     */
    protected $es_type          = self::TYPE;
    
    /**
     *
     * @var unknown
     */
    protected $fields = array(
        'id_path',
        'path_name',
        'path_descr',
        'path_code',
    	'visible_in_catalog',	
    );
    
    /**
     *
     * @var unknown
     */
    protected $idName = 'id_path';

    
    /**
     * 
     * {@inheritDoc}
     * @see IEsAgent::isAvail()
     */
    public function isAvail() {
    	return PluginManager::isPluginActive('CurriculaApp');
    }
    
    
    /**
     * 
     * {@inheritDoc}
     * @see IEsAgent::buildCommandForListOfDocuments()
     */
    public function buildCommandForListOfDocuments($ids=false) {
        
        $commandBase = Yii::app()->db->createCommand();
        $commandBase->from('learning_coursepath');
        
        if ($ids !== false && is_array($ids) && !empty($ids)) {
            $commandBase->andWhere(array('IN', 'id_path', $ids));
        }
        
        $commandCounter = clone $commandBase;
        $commandData    = clone $commandBase;
        
        $commandCounter ->select('count(*)');
        $commandData    ->select(implode(',', $this->fields));
        
        $result = array(
            'commandCounter'    => $commandCounter,
            'commandData'       => $commandData,
        );
        
        return $result;
        
    }
    
 
    /**
     * 
     * {@inheritDoc}
     * @see IEsAgent::getEsDocumentTitle()
     */
    public function getEsDocumentTitle($document) {
    	return $document['path_name'];
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see IEsAgent::getLocalData()
     */
    // Return array with values
    public function getLocalData($id, $idUser=false) {
    	
        if (!$idUser) {
        	$idUser = Yii::app()->user->id;
        }

        $isInCatalog = LearningCatalogueEntry::model()->exists('idEntry = :idEntry AND type_of_entry = "coursepath" ', array(":idEntry" => $id));

        $isEnrolled = LearningCoursepathUser::model()->exists('id_path = :id_path AND idUser = :idUser', array(
            ":id_path"  => $id,
            ":idUser"   => $idUser
        ));
        
        $planModel = LearningCoursepath::model()->findByPk($id); 

        $result = array(
        	'planModel'			=> $planModel,
            'isInCatalog'       => $isInCatalog,
            'isEnrolled' 		=> $isEnrolled
        );

    	return $result;
    }
    
    
    
    /**
     *
     * @param unknown $idUser
     */
    public function buildVisibilityForUser($idUser) {
    	 
    	if (!isset(Yii::app()->cache_items_visibility)) {
    		return;
    	}
    	
    	if (!$this->isAvail()) {
    		return array();
    	}
    	 
    	$type 			= self::TYPE;
    	$allItems       = array();
    	 
    	// 1. Plans user is enrolled in
    	$params = array(
    		':idUser' => $idUser
    	);
    	$command = Yii::app()->db->createCommand();
    	$command->select('lcu.id_path as id_path');
    	$command->from('learning_coursepath_user lcu');
    	$command->where('lcu.idUser=:idUser');
    	if (($items  = $command->queryColumn($params)) && is_array($items )) {
    		$allItems = array_merge($allItems, $items);
    	}
    	 
    	// 2. Plans Visible through catalogs
    	$userCatalogs = LearningCatalogue::getUserCatalogs($idUser);
    	if (is_array($userCatalogs) && !empty($userCatalogs)) {
    		$params = array(
    			':entryType'	=> LearningCatalogueEntry::ENTRY_COURSEPATH,	
    		);
    		$command = Yii::app()->db->createCommand();
    		$command->select("lce.idEntry");
    		$command->from("learning_catalogue lc");
    		$command->join("learning_catalogue_entry lce", "lc.idCatalogue=lce.idCatalogue AND lce.type_of_entry=:entryType");
    		$command->join("learning_coursepath lcp", "lce.idEntry=lcp.id_path AND lcp.visible_in_catalog=1");
    		$command->where(array('IN', 'lc.idCatalogue', $userCatalogs));
    		if (($items  = $command->queryColumn($params)) && is_array($items )) {
    			$allItems = array_merge($allItems, $items);
    		}
    	}
    	 
    	// 3. Plans Visible because user is a power user of a course
    	$params = array(
    		':idUser' => $idUser,
    	);
    	$command = Yii::app()->db->createCommand();
    	$command->select("cupup.path_id");
    	$command->from("core_user_pu_coursepath cupup");
    	$command->where("cupup.puser_id=:idUser");
    	if (($items  = $command->queryColumn($params)) && is_array($items )) {
    		$allItems = array_merge($allItems, $items);
    	}
    	
    	
    	
    	// 4. Plans Visible because user is a power user of the Catalog where plans are in
    	$items = CoreAdminCourse::getPowerUserEntriesOfPuCatalogs($idUser, CoreAdminCourse::TYPE_COURSEPATH);
    	$allItems = array_merge($allItems, $items);
    	 
    	

        //----- finalize
        return $allItems;
    	 
    	 
    }
 
    
}