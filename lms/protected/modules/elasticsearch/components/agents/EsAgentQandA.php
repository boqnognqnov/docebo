<?php
/**
 * 
 * Elasticsearch agent to index/search 7020 Questions and Answers
 *
 */
class EsAgentQandA extends EsAgent implements IEsAgent  {
	
	const TYPE = __CLASS__;
	const TYPE_SHORT = 'qa';
	
	/**
	 * Fields directly retrieved from this document DB table
	 * @var array
	 */
	static $baseFields = array(
	    'q.id',
	    'title',
	);
	
    /**
     *
     * @var string
     */
    protected $es_type          = self::TYPE;
    
    /**
     * Final list of fields for Elasticsearch
     * @var array
     */
    protected $fields = array();
    
    /**
     * ID field name
     * @var string
     */
    protected $idName = 'id';
    
    
    /**
     * 
     * {@inheritDoc}
     * @see IEsAgent::isAvail()
     */
   	public function isAvail() {
    	return PluginManager::isPluginActive('Share7020App');
    }
    
    /**
     * 
     */
    public function __construct() {
        $this->fields = array(
       		'question_id',
       		'title',
       		'answers',
       		'channels',
       		'channelIds',
       		'visibleToAll',
        );
    }
     
    /**
     *
     * {@inheritDoc}
     * @see IEsAgent::buildCommandForListOfDocuments()
     */
    public function buildCommandForListOfDocuments($ids=false) {
        
        
        
        $commandBase    = Yii::app()->db->createCommand();
        $commandCounter = Yii::app()->db->createCommand();
        
        $commandBase->from('app7020_question q');
        
        // To get answers, comma separated list (group_concat)
        $commandBase->leftJoin('app7020_answer answer', 'q.id=answer.idQuestion');
        
        // To get channels (all translations), comma separated list (group_concat)
        $commandBase->leftJoin('app7020_channel_questions cq', 'q.id=cq.idQuestion');
        $commandBase->leftJoin('app7020_channel_translation translation', 'translation.idChannel=cq.idChannel');
        
        // Also, join channel visibility so we can calculate "visible to ALL = true/false)
        $commandBase->leftJoin('app7020_channel_visibility cv', 'cv.idChannel=cq.idChannel');
        
        
        // Group by idOrg (allows group_concat)
        $commandBase->group('q.id');
        
        if ($ids !== false && is_array($ids) && !empty($ids)) {
            $commandBase->andWhere(array('IN', 'q.id', $ids));
        }
        
        $commandData    = clone $commandBase;
        
        $selectArray = array(
        	'q.id as question_id',
	    	'title',
        	'GROUP_CONCAT(answer.content) as allAnswersString', 
        	'GROUP_CONCAT(translation.name) as allChannelsString', 
        	'GROUP_CONCAT(translation.idChannel) as allChannelIds', 
        	'GROUP_CONCAT(cv.idChannel) AS visibleToAll'        		
        );
        $select         = implode(',', $selectArray);
        $commandData->select($select);
        
        $commandCounter->select('count(*)');
        $commandCounter->from("(" . $commandData->text . ") SUBTABLE");
        
        //var_dump($commandCounter->text);die;
        
        
        $result = array(
            'commandCounter'    => $commandCounter,
            'commandData'       => $commandData,
        );
        
        return $result;
        
        
        
    }
    
    public function getEsDocumentTitle($document) {
    	return $document['title'];
    }
    
    public function getLocalData($id) {

    	$hasBestAnswer = false;
    	$questionModel = App7020Question::model()->findByPk($id);
    	if ($questionModel) {
    		$hasBestAnswer = Yii::app()->db->createCommand()
    			->select('count(id)')
    			->from('app7020_answer a')
    			->where('a.idQuestion = :idQuestion')
    			->andWhere('a.bestAnswer = 2')   // 2 ????? @todo check please
    			->queryScalar(array(
    				':idQuestion'  => $questionModel->id,
    			));
    	}
    	
    	$result = array(
    		'questionModel'		=> $questionModel,
    		'hasBestAnswer'		=> $hasBestAnswer,
    	);
    	return $result;
    }
 
    
    /**
     *
     * {@inheritDoc}
     * @see EsAgent::bulkIndexTypeDataBatch()
     */
    public function bulkIndexTypeDataBatch($page, $data) {
        $documents = array();
        foreach ($data as $row) {
            $answers    	= explode(",", $row['allAnswersString']);
            $channels     	= explode(",", $row['allChannelsString']);
            $channelIds       = explode(",", $row['allChannelIds']);
            $visibleToAll   = empty($row['visibleToAll']) ? 1 : 0;
            
            $documents[] = array_merge($row, array(
                'answers'       => $answers,
                'channels'        => $channels,
                'channelIds'      => $channelIds, 
                'visibleToAll'  => $visibleToAll, 
            ));
        }
        return $this->bulkIndex($documents, $this->idName);
    }
    
    
    /**
     *
     * @param unknown $idUser
     */
    public function buildVisibilityForUser($idUser) {
    	 
    	if (!isset(Yii::app()->cache_items_visibility)) {
    		return;
    	}

    	if (!$this->isAvail()) {
    		return array();
    	}
    	
    	$type 			= self::TYPE;
    	$allItems       = array();
    	
    	// 1. Get all questions explicitly visible to the user
    	$allItems = App7020Question::getQuestionToUserRuledVisibility($idUser);
    	
    
    	//----- finalize
     	return $allItems;
    	 
    }
    
    
}