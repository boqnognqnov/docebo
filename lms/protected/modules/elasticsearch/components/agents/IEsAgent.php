<?php

interface IEsAgent  {

    /**
     * Build a DB Command to retrieve list of "documents" (an array of array of attributes)
     * 
     * @param array $ids
     */
    function buildCommandForListOfDocuments($ids=false);
    
    function getEsDocumentTitle($document);
    
    function getLocalData($id);
    
    /**
     * Agent reports its availability status
     */
    function isAvail();
    
    
}