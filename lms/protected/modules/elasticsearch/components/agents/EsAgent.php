<?php
/**
 * 
 * 
 *
 */
abstract class EsAgent extends stdClass {

    const VIEWS = 'elasticsearch.views';
    
    const VIEW_SUGGESTION_ITEM  = 'suggestion_item';
    const VIEW_RESULT_ITEM      = 'result_item';

    /**
     * @var string Type of the handled entity, e.g. 'course', 'user', 'plan', etc.
     * Set by class descendants
     */
    protected $es_type;
    
    /**
     * @var array Array of field names to index.
     * Set by class descendants
     */
    protected $fields = array();


    /**
     * @var string Name of the ID field/attribute fo the document
     * Set by class descendants
     */
    protected $idName;


    /**
     * Perform bulk INDEX action on passed documents
     *  
     * @param array $documents
     * @param string $idAttributeName
     * @param string $index
     */
    protected function bulkIndex($documents, $idAttributeName) {
    	if (!is_array($documents)) {
    	    return array(
    	        'message'   => 'No documents passed, but we consider this not an error in this context!',
    	    );
    	}
    	return Yii::app()->es->bulk(EsClient::ACTION_INDEX, $documents, $this->es_type, $idAttributeName, $this->fields);
    }
    

    /**
     * Index array of data (many documents).
     * Override this method in descendant classes if indexing is more specific.
     * This one is only suitable for the simple case where every document is a SQL result row. 
     * 
     * @param array|false $ids
     */
    public function indexListOfDocuments($ids=false) {
        
        $commands = $this->buildCommandForListOfDocuments($ids);
        
        $commandCounter     = $commands['commandCounter'];
        $commandData        = $commands['commandData'];
        
        $totalCount = $commandCounter->queryScalar();
        
        $config = array(
            'totalItemCount'    => $totalCount,
            'pagination'        => array(
                'pageSize'  => 5000,
            ),
        );

        $dataProvider = new CSqlDataProvider($commandData->getText(), $config);
        
        return $this->iterateDataProvider($dataProvider);

    }
    
    
    /**
     * Index the Descendants' entities (courses, los, etc.) 
     * 
     * @param string|array|bool $id  
     */
    public function index($id=false) {
        
        if ($id !== false && !is_array($id)) {
            $id = array($id);
        }
    
        if ($id === false) {
            return $this->indexListOfDocuments();
        }
        else {
            return $this->indexListOfDocuments($id);
        }
    
    }

    /**
     * Index an array of data as a document in ElasticSearch.
     * The array MUST contain the ID of the document.
     * 
     * @param array $document
     * @param string $idAttributeName The key name in the array, which value to use as Document ID
     */
    public function indexDocument($document, $idAttributeName) {
        return $this->bulkIndex(array($document), $idAttributeName);
    }

    /**
     * Accept list of descendant classes and perform a real MASS indexing using their native index() methods. No filtering, just all!
     * 
     * @param array $agentClasses  Array of Class Names (e.g. EsAgent???????)
     */
    public static function massIndex($types) {
        
        if (!is_array($types)) {
            return array(
                'message'   => 'No agent classes passed, but we consider this not an error in this context!',
            );
        }

        // Enumerate all class names, check if their files exist, instantinate object, call ->index()....
        $responses = array();
        foreach ($types as $type) {
        	$agent = EsAgent::getAgent($type);
        	if ($agent->isAvail()) {
        		$responses[] = $agent->index();
        	}
        }
        
        return $responses;
    }
    
    
    /**
     * Helper method to get ES agent of given class
     * 
     * @param string $className
     * @return EsAgent
     */
    public static function getAgent($type) {
        $agent = new $type();
        return $agent;
    }
    

    /**
     * Iterate over passed DataProvider data and call the current object's bulk indexing method, specific for every agent/type  
     * 
     * @param CSqlDataProvider $dataProvider
     */
    protected function iterateDataProvider(CSqlDataProvider $dataProvider) {
        
        $dataProvider->pagination->currentPage = 0;
        $dataProvider->pagination->itemCount = $dataProvider->totalItemCount; 
        $pageCount = $dataProvider->pagination->getPageCount();
        
        $lastResponse = array();
        for ($page=0; $page<$pageCount; $page++) {
            $dataProvider->pagination->setCurrentPage($page);
            $lastResponse = $this->bulkIndexTypeDataBatch($page, $dataProvider->getData(true));
        }
        
        return $lastResponse;
    }
    
    
    /**
     * Generic batch data (page) indexing method for all types.
     * Must be overriden by descendant types if they need to do it in a specific way (index) 
     *  
     * @param integer $page
     * @param array $data
     * @return string[]
     */
    public function bulkIndexTypeDataBatch($page, $data) {
        $documents = array();
        foreach ($data as $row) {
            $documents[] = $row;
        }
        return $this->bulkIndex($documents, $this->idName);
    }
    
 
    /**
     * Return a string respresenting the reason user sees the item in the list of search results
     * 
     * @param integer $id
     * @param string $type
     * @param boolean $isGodAdmin
     * @param boolean $isPu
     * @param boolean $isEnrolled
     * @param array $puItems List of items assigned to PU, if the current user is PU
     * @param array $userCatalogs List of catalogs which current user is assiegned to
     * 
     * @return string
     */
    public static function reasonToSeeItem($id, $type, $isGodAdmin, $isPu, $isEnrolled, $puItems, $userCatalogs) {
    
    	if ($isGodAdmin || ($isPu && in_array($id, $puItems))) {
    		return Yii::t('standard', '');
    	}
    	if ($isEnrolled) {
    		return Yii::t('menu_over', '_MYCOURSES');
    	}
    	if ( !$isEnrolled && PluginManager::isPluginActive('CoursecatalogApp') && LearningCatalogue::isEntryInCatalogs($id, $userCatalogs, $type)) {
    		return Yii::t('standard', 'Catalog');
    	}
    
    	return "";
    
    }
    

    /**
     * Check if course (actually Subscription and/or Subscription process) is locked
     *  
     * @param LearningCourseuser $enrollment
     * @param LearningCourse $course
     */
    protected function isCourseLocked($enrollment, $course) {

        $locked = false;
        $reason = "";
        
        // Can user enter the course (as a subscriber!) ?
        if ($enrollment instanceof LearningCourseuser) {
            $canEnterCourseArr = $enrollment->canEnterCourse();
            if (!is_array($canEnterCourseArr) || !$canEnterCourseArr['can']) {
                $locked = true;
                $reason = $canEnterCourseArr['reason'];
            }
        }
        
        // Can user freely subscribe (or at least initiate subscription) ?
        if (!$enrollment && $course instanceof LearningCourse) {        
            $policyResult = $course->getCoursePolicy();
            if (is_array($policyResult) && isset($policyResult['canSubscribe'])) {
                if (!$policyResult['canSubscribe']) {
                    $locked = true;
                    $reason = $policyResult['label'];
                }
            }
            if (is_array($policyResult) && isset($policyResult['canSelfSubscribe'])) {
                if (!$policyResult['canSelfSubscribe']) {
                    $locked = true;
                    $reason = $policyResult['label'];
                }
            }
        }
        
        $result = array(
            'locked'    => $locked,
            'reason'    => $reason,
        );
        
        return $result;
        
    }

}


