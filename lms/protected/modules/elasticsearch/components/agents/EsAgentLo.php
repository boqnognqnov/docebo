<?php
/**
 *
 * Elasticsearch agent to index/search Learning Objects
 *
 */
class EsAgentLo extends EsAgent implements IEsAgent  {

	const TYPE = __CLASS__;
	const TYPE_SHORT = 'lo';

    /**
     *
     * @var unknown
     */
    protected $es_type = self::TYPE;

    /**
     * Final list of fields for Elasticsearch
     * @var array
     */
    protected $fields = array();

    /**
     * ID field name
     * @var string
     */
    protected $idName = 'idOrg';

    /**
     *
     * {@inheritDoc}
     * @see IEsAgent::isAvail()
     */
    public function isAvail() {
    	return true;
    }


    /**
     *
     */
    public function __construct() {
        $this->fields = array(
       		'idOrg',
       		'title',
       		'description',
       		'short_description',
       		'show_rules', // from course
       		'status', // from course
       		'tags',
        );
    }


    /**
     *
     * {@inheritDoc}
     * @see IEsAgent::buildCommandForListOfDocuments()
     */
    public function buildCommandForListOfDocuments($ids=false) {

        $commandBase    = Yii::app()->db->createCommand();
        $commandCounter = Yii::app()->db->createCommand();

        $commandBase->from('learning_organization lo');

        // To get tags, comma separated list (group_concat)
        $commandBase->leftJoin('app7020_tags_lo taglo', 'lo.idOrg=taglo.idLo');
        $commandBase->leftJoin('app7020_tag tag', 'taglo.idTag=tag.id');

        // Lets join Video, File, Poll and Test
        // Because these LO types have their descriptions in their own tables and fields
        // Please SEE the CASE structure in the select later!!!!!!!!!!!!
        $commandBase->leftJoin('learning_video video', 'video.id_video=lo.idResource');
        $commandBase->leftJoin('learning_materials_lesson lml', 'lml.idLesson=lo.idResource');
        $commandBase->leftJoin('learning_poll poll', 'poll.id_poll=lo.idResource');
        $commandBase->leftJoin('learning_test test', 'test.idTest=lo.idResource');

        // We need the course
        $commandBase->join('learning_course course', 'course.idCourse=lo.idCourse');

        // Exclude "root" LOs (every course has a root one, not a real LO)
        $commandBase->andWhere("lo.objectType IS NOT NULL AND lo.objectType <> ''");

        // Group by idOrg (allows group_concat)
        $commandBase->group('lo.idOrg');

        if ($ids !== false && is_array($ids) && !empty($ids)) {
            $commandBase->andWhere(array('IN', 'lo.idOrg', $ids));
        }

        $commandData    = clone $commandBase;

        $selectArray = array(
        	'idOrg',
        	'lo.title AS title',
        	'lo.short_description AS short_description',
        	'show_rules', // from course
			'course.status as status',// from course
        	// Now the specific LO types having their own description fields in their own tables
        	"( CASE
                    WHEN lo.objectType='video' THEN video.description
                    WHEN lo.objectType='file'  THEN lml.description
                    WHEN lo.objectType='poll'  THEN poll.description
                    WHEN lo.objectType='test'  THEN test.description
                    ELSE lo.description
                END
              ) AS description",
        );

        $selectArray[]  = 'GROUP_CONCAT(tag.tagText) as allTagsString';
        $select         = implode(',', $selectArray);
        $commandData->select($select);

        $commandCounter->select('count(*)');
        $commandCounter->from("(" . $commandData->text . ") SUBTABLE");

        $result = array(
            'commandCounter'    => $commandCounter,
            'commandData'       => $commandData,
        );

        return $result;

    }


    /**
     *
     * {@inheritDoc}
     * @see IEsAgent::getEsDocumentTitle()
     */
    public function getEsDocumentTitle($document) {
    	return $document['title'];
    }

    /**
     *
     * {@inheritDoc}
     * @see IEsAgent::getLocalData()
     */
    public function getLocalData($id, $idUser=false) {

    	$loModel = LearningOrganization::model()->findByPk($id);
    	$enrollment = false;
    	$isEnrolled = true;
    	if (!$idUser) {
    		$idUser = Yii::app()->user->id;
    	}


    	if ($loModel) {
    		$courseModel = LearningCourse::model()->findByPk($loModel->idCourse);
    		if ($courseModel) {
    			$enrollment = LearningCourseuser::model()->findByAttributes(array("idUser" => $idUser, "idCourse" => $loModel->idCourse));
    		}
    	}

       	$isEnrolled = !empty($enrollment);
    	$lockedCourse = $this->isCourseLocked($enrollment, $courseModel);

    	$result = array(
   			'lockedCourse'  => $lockedCourse,
    		'courseModel' 	=> $courseModel,
    		'isEnrolled'	=> $isEnrolled,
    		'loModel'		=> $loModel,
    	);

    	return $result;

    }



    /**
     *
     * @param unknown $idUser
     */
    public function buildVisibilityForUser($idUser) {

        if (!isset(Yii::app()->cache_items_visibility)) {
            return;
        }

        if (!$this->isAvail()) {
        	return array();
        }

        $type 			= self::TYPE;
        $allItems       = array();

        // 1. LOs in Courses user is enrolled in
        $params = array(
            ':idUser' => $idUser
        );
        $command = Yii::app()->db->createCommand();
        $command->select('lo.idOrg');
        $command->from('learning_organization lo');
        $command->join('learning_courseuser lcu', 'lo.idCourse=lcu.idCourse');
        $command->where('lcu.idUser=:idUser');
        $command->andWhere("lo.objectType IS NOT NULL AND lo.objectType <> ''");
        if (($items = $command->queryColumn($params)) && is_array($items)) {
            $allItems = array_merge($allItems, $items);
        }


        // 2. LOs in Courses Visible through catalogs
        $userCatalogs   = LearningCatalogue::getUserCatalogs($idUser);
        $puCatalogs     = array_keys(CoreAdminCourse::getPowerUserCatalogs($idUser)); // also, if this user is a PU, there might be PU managed catalogs
        $userCatalogs = array_merge($userCatalogs, $puCatalogs);
        if (is_array($userCatalogs) && !empty($userCatalogs)) {
            $params = array(
                ':entryType'	=> LearningCatalogueEntry::ENTRY_COURSE,
            );
            $command = Yii::app()->db->createCommand();
            $command->select("lo.idOrg");
            $command->from("learning_catalogue lc");
            $command->join("learning_catalogue_entry lce", "lc.idCatalogue=lce.idCatalogue AND lce.type_of_entry=:entryType");
            $command->join("learning_organization lo", "lce.idEntry=lo.idCourse");
            $command->join("learning_course course", "(course.idCourse=lo.idCourse) AND (course.show_rules IN (0,1))");
            $command->andWhere("lo.objectType IS NOT NULL AND lo.objectType <> ''");
            $command->andWhere(array('IN', 'lc.idCatalogue', $userCatalogs));
            if (($items  = $command->queryColumn($params)) && is_array($items )) {
                $allItems = array_merge($allItems, $items);
            }
        }


        // 3. LOs in Courses Visible because user is a power user of a course
        $params = array(
            ':idUser' => $idUser
        );
        $command = Yii::app()->db->createCommand();
        $command->select("lo.idOrg");
        $command->from("core_user_pu_course cupuc");
        $command->join("learning_organization lo", "lo.idCourse=cupuc.course_id");
        $command->where("cupuc.puser_id=:idUser");
        if (($items  = $command->queryColumn($params)) && is_array($items )) {
            $allItems = array_merge($allItems, $items);
        }

        //----- finalize
        return $allItems;

    }


    /**
     *
     * {@inheritDoc}
     * @see EsAgent::bulkIndexTypeDataBatch()
     */
    public function bulkIndexTypeDataBatch($page, $data) {
        $documents = array();
        foreach ($data as $row) {
            $tags   = explode(",", $row['allTagsString']);
            $documents[] = array_merge($row, array(
                'tags'      => $tags,
            ));
        }

        return  $this->bulkIndex($documents, $this->idName);
    }


}