<?php
use Aws\Common\Credentials\CredentialsInterface;
use Aws\Common\Signature\SignatureInterface;
use Guzzle\Http\Message\EntityEnclosingRequest;
use Guzzle\Http\Message\Request;
/**
 * 
 * A request-middleware used to sign requests to Amazon Elastic Search service.
 * For this to work, Access policy must be set to IAM/User
 *
 */
class AwsSignatureMiddleware
{

    /**
     *
     * @var Aws\Common\Credentials\CredentialsInterface
     */
    protected $credentials;

    /**
     *
     * @var Aws\Common\Signature\SignatureInterface
     */
    protected $signature;

    /**
     *
     * @param CredentialsInterface $credentials            
     * @param SignatureInterface $signature            
     */
    public function __construct(CredentialsInterface $credentials, SignatureInterface $signature)
    {
        $this->credentials = $credentials;
        $this->signature = $signature;
    }

    /**
     *
     * @param $handler
     * @return callable
     */
    public function __invoke($handler)
    {
        return function ($request) use($handler) {
            
            $headers = $request['headers'];
            if ($headers['host']) {
                if (is_array($headers['host'])) {
                    $headers['host'] = array_map([
                        $this,
                        'removePort'
                    ], $headers['host']);
                } else {
                    $headers['host'] = $this->removePort($headers['host']);
                }
            }
            
            
            if (!empty($request['body'])) {
                $psrRequest = new EntityEnclosingRequest($request['http_method'], $request['uri'], $headers);
                $psrRequest->setBody($request['body']);
            }
            else {
                $psrRequest = new Request($request['http_method'], $request['uri'], $headers);                
            }

            
            // Get path and urlencode it (because AWS does the same before signature calculation)
            $path = implode('/', array_map('rawurlencode', explode('/', $psrRequest->getPath())));
            $psrRequest->setPath($path);
            
            // Sign the request
            $this->signature->signRequest($psrRequest, $this->credentials);
            
            // Set back request headers
            $request['headers'] = array_merge($psrRequest->getHeaders()->toArray(), $request['headers']);
            
            // Pass the request to handler
            return $handler($request);
        };
    }

    /**
     * AWS api seems to doesn't use port part in host field
     * 
     * @param string $host            
     * @return string
     */
    protected function removePort($host)
    {
        $parse = parse_url($host);
        return $parse['host'];
    }
}
