<?php

/**
 *
 *
 */
class EsJobHandler extends JobHandler implements IJobHandler
{

    /**
     * THIS handler identifier
     * 
     * @var string
     */
    const HANDLER_ID = 'elasticsearch.components.EsJobHandler';

    const TASK_MASS_INDEX = 'mass_index';

    const TASK_CACHE_USER_ITEMS_VISIBILITY = 'cache_items_visibility';

    /**
     *
     * @see IJobHandler::init()
     */
    public function init($params = false)
    {
        // do some initialization stuff here, specific for this job handler
    }

    /**
     *
     * @see IJobHandler::run()
     */
    public function run($params = false)
    {
        
        // Must be already assigned at upper level
        $job = $this->job;
        $jobParams = json_decode($job->params, true);
        $logCat = "job-" . $job->id_job . " " . EsClient::LOG_CATEGORY;
        
        // We need TASK parameter to know what to do
        if (! isset($jobParams['task'])) {
            return;
        }
        
        switch ($jobParams['task']) {
            
            // Mass Indexing
            case self::TASK_MASS_INDEX:
                // We need list of Agents (or types) to index
                if (! isset($jobParams['agents']) || ! is_array($jobParams['agents'])) {
                    return;
                }
                
                // Do the actual work: call ElasticSearch Agent to index all listed types
                // Log all errors
                $responses = EsAgent::massIndex($jobParams['agents']);
                
                foreach ($responses as $response) {
                    if (isset($response['exception_code'])) {
                        Yii::log('Something went wrong with ElastiSearch indexing: ' . print_r($response, true), CLogger::LEVEL_ERROR, $logCat);
                        Yii::log('Job: ' . $job->id_job, CLogger::LEVEL_ERROR, $logCat);
                        Yii::log('Job Parameters: ' . print_r($jobParams, true), CLogger::LEVEL_ERROR, $logCat);
                        Yii::log('Exception code: ' . $response['exception_code'], CLogger::LEVEL_ERROR, $logCat);
                    }
                }
                
                break;
            
            case self::TASK_CACHE_USER_ITEMS_VISIBILITY:
                
                $idUsers    = $jobParams['users'];
                if (! is_array($idUsers)) {
                    $idUsers = array(
                        (int) $idUsers
                    );
                }
                $allTypes   = Yii::app()->es->getAvailableTypes();
                $types      = isset($jobParams['types']) ? $jobParams['types'] : $allTypes;
                foreach ($idUsers as $idUser) {
                    $visCache = Yii::app()->es->buildVisibility((int) $idUser, $types);
                    if (is_array($visCache)) {
                        Yii::app()->cache_items_visibility->set($idUser, $visCache);
                    }
                }
                break;
            
            default:
                break;
        }
    }

    /**
     *
     * @see IJobHandler::id()
     */
    public static function id($params = false)
    {
        return self::HANDLER_ID;
    }
}
