(function($) {

	/** @memberOf GlobalSearch */
	var UI_TYPE_SUGGESTIONS 	= 'suggestions';
	/** @memberOf GlobalSearch */
	var UI_TYPE_RESULTS 		= 'results';
	
	/** @memberOf GlobalSearch */
	var LIMIT_RECENT_LENGTH = 60;
	
	/* Hold pluging options in this closure variable */
	/** @memberOf GlobalSearch */
	var settings = {
		minScore: 0.3,  
		suggestionsNum: 8,
		recentSearchesNum: 5,
		searchResultsNum: 20
	};

	//Enable disable log() output to console
	/** @memberOf GlobalSearch */
	var debug = true;

	/* Holds the whole Globalsearch element this plugin is attached to (jQuery Object) */
	/** @memberOf GlobalSearch */
	var $gs;

	/** @memberOf GlobalSearch */
	var globalSearchTimeOut = null;
	
	/** @memberOf GlobalSearch */
	var globalSearchScrollTimeOut = null;

	/** @memberOf GlobalSearch */
	var that;

	/** @memberOf GlobalSearch */
	var noResultsHtml = '<div class="elastic-result-box">' + Yii.t('app7020', 'No results found') + '</div>';
	
	/** @memberOf GlobalSearch */
	var noRecentHtml  = '<div class="elastic-result-box">' + Yii.t('standard', 'No recent searches found') + '</div>';

	/** @memberOf GlobalSearch */
	var globalSearchType = 'all'; // agent type
	/** @memberOf GlobalSearch */
	var globalSearchUiType = 'results'; // UI type - results or suggestions
	/** @memberOf GlobalSearch */
	var globalSearchPageSize = 20; // found items per page
	/** @memberOf GlobalSearch */
	var globalSearchFrom = 0; // start search from
	/** @memberOf GlobalSearch */
	var globalSearchString = ''; // search string
	/** @memberOf GlobalSearch */
	var globalSearchSelectedTab = '#tab-all-global-search'; // current search tab
	/** @memberOf GlobalSearch */
	var globalSearchOnScroll = true; // should search on scroll be enabled
	/** @memberOf GlobalSearch */
	var recentSearchesLoaded = false; 

	

	/**
	 * A nice logging/debugging function
	 * @memberOf GlobalSearch
	 */
    function log() {
    	if (!debug) return;
    	if (window.console && window.console.log) {
    		var args = Array.prototype.slice.call(arguments);
    		if(args){
    			$.each(args, function(index, ev){
    				window.console.log(ev);
    			});
    		}
        }
    };


	/**
	 * Get suggestions usually called on keyup event inside the search (input) box
	 * @memberOf GlobalSearch
	 * @param e Event
     */
	function getSuggestions(e) {
		
		// Clear the timeout if it is set
		if(globalSearchTimeOut !== null) {
			clearTimeout(globalSearchTimeOut);
		}

		// Get the keyCode crossbrowser
		var code = e.keyCode || e.which;

		// If enter key pressed start search
		if (code === 13) {
			globalSearchFrom = 0;
			loadSearchResults(false, true);
			return;
		}

		// List of non-alphabetical + not punctual + not math operators codes. For more details see the following link:
		// http://www.cambiaresearch.com/articles/15/javascript-char-codes-key-codes
		var specialKeyCodes = [9,13,16,17,18,19,20,27,33,34,35,36,37,38,39,40,45,112,113,114,115,116,117,118,119,120,121,122,123,144,145];

		// Get and show suggestions only if the none of the above keys(keyCodes) are pressed
		if ($.inArray(code, specialKeyCodes) === -1) {
			globalSearchTimeOut = setTimeout(loadSuggestions, 600);
		}

	};


	/**
	 * clear global search on cross button click
	 * @memberOf GlobalSearch
	 */
	function clearGlobalSearch() {
		$('#global-search-input').val('');
		$('#global-search-suggestions').html('');
		$('a[href="#tab-all-global-search"]').click();
		showSuggestions();
	};


	/**
	 * @memberOf GlobalSearch
	 */
	function showSuggestions() {
		hideSearchResults();
		$('#global-search-wrapper #global-search-suggestions-wrapper').show();
	};
	
	/**
	 * @memberOf GlobalSearch
	 */
	function hideSuggestions() {
		$('#global-search-wrapper #global-search-suggestions-wrapper').hide();
	};

	/**
	 * @memberOf GlobalSearch
	 */
	function showSearchResults() {
		hideSuggestions();
		$('#global-search-wrapper #global-search-results-container').show();
	};
	
	/**
	 * @memberOf GlobalSearch
	 */
	function hideSearchResults() {
		$('#global-search-wrapper #global-search-results-container').hide();
	};

	/**
	 * Changes the global search term if user selects one from the suggestions or recent searches
	 * @param e event
	 * @memberOf GlobalSearch
	 */
	function selectSuggestionOrRecentSearch(e) {
		var elem = e.currentTarget;
		globalSearchFrom = 0;
		$('#global-search-input').val($(elem).data('select'));
		loadSearchResults();
	};


	/**
	 * 
	 * @memberOf GlobalSearch
     */
	function changeTypeTab(e) {
		
		var searchTabId = this.href; // Get URL of the selected tab
		searchTabId = searchTabId.substring(searchTabId.indexOf("#")+1);
		searchTabId = '#' + searchTabId;
		globalSearchSelectedTab = searchTabId;
		globalSearchFrom = 0;

		var tabInfo = resolveTabInfo(searchTabId);
		
		globalSearchSelectedTab = tabInfo.tabId;
		globalSearchType = tabInfo.type;
		
		$('a[data-toggle="tab"][href="'+ globalSearchSelectedTab +'"]').tab('show');

		loadSearchResults(false, false);
	
	};
	
	/**
	 * 
	 * @memberOf GlobalSearch 
	 */
	function resolveTabInfo(tabId) {
		
		var type;
		
		switch (tabId) {
			case "#tab-all-global-search":
				type = 'all';
				break;

			case "#tab-courses-global-search":
				type = 'EsAgentCourse';
				break;

			case "#tab-qa-global-search":
				type = 'EsAgentQandA';
				break;

			case "#tab-ka-global-search":
				type = 'EsAgentKnowledgeAsset';
				break;

			case "":
				type = '';
				break;

			case "#tab-coursepath-global-search":
				type = 'EsAgentPlan';
				break;

			case "#tab-courselo-global-search":
				type = 'EsAgentLo';
				break;

			case "#tabLearningObject":
				type = 'EsAgentLo';
				tabId = '#tab-ellipsis-global-search';
				$('a[href="#tab-ellipsis-global-search"]').parent().removeClass('skip').trigger('click').addClass('skip');
				break;

			case "#tabLearningPlan":
				type = 'EsAgentPlan';
				tabId = '#tab-ellipsis-global-search';
				$('a[href="#tab-ellipsis-global-search"]').parent().removeClass('skip').trigger('click').addClass('skip');
				break;
				
		}
		
		return {
			type: 	type,
			tabId: 	tabId
		}
		
	};

	
	/**
	 * do search on scrolling down to the bottom of the container
	 * @param div
	 * @param wrapper
	 * @memberOf GlobalSearch
     */
	function attachInfiniteSearchScroll(div, wrapper) {
		$('div#globalsearch').on('scroll', function (e) {
			var elem = e.currentTarget;
			var beforeBottom = 100; // pixels
			if (globalSearchOnScroll) {
				if (($(elem).scrollTop() + $(elem).height()) > ($(wrapper).height() - beforeBottom)) {
					globalSearchOnScroll = false; // temporary disable the search as you scroll
					loadSearchResults(true, false);
				}
			}
		});
	};

	
    /**
     * Add event listeners / handlers
	 * @memberOf GlobalSearch
     */
    function addListeners() {
		// Get suggestions on keyup inside the search input box
		$('#global-search-input').on('keyup', $.proxy(getSuggestions, this));

		// Click search clear ( X ) icon handle
		$('#global-search-clear').on('click', $.proxy(clearGlobalSearch, this));

		// Click start button handle
		$('#global-search-start').on('click', function() {
			globalSearchFrom = 0;
			loadSearchResults(false, true);
		});

		// Suggestion or Recent search selected
		$(document).on('click', '#recent-global-searches-container li', 						$.proxy(selectSuggestionOrRecentSearch, this));
		$(document).on('click', '#global-search-suggestions li', 								$.proxy(selectSuggestionOrRecentSearch, this));

		// Search tabs clicking handling
		$('a[href="#tab-all-global-search"], a[href="#tab-courses-global-search"], a[href="#tab-qa-global-search"], a[href="#tab-ka-global-search"], a[href="#tab-coursepath-global-search"], a[href="#tab-courselo-global-search"]').on('click', changeTypeTab);

		// Search tab Ellipsis sub menu should be catched here
		$(document).on('click', 'a[href="#tabLearningPlan"], a[href="#tabLearningObject"]', changeTypeTab);
    };
	
    
    
    /**
	 * @memberOf GlobalSearch
     */
    function loadRecentSearches() {
    	var url = Docebo.createAbsoluteUrl("es/main/axGetRecentSearches", {}, "lms");
    	
		showLoadingMessage("#recent-global-searches-container", Yii.t('standard', 'Loading recent searches'));
    	
    	$.ajax({
    		url: url,
			method: "POST",
			data: {},
			dataType: 'json',
    	}).done(function(response) {
			if (typeof response.success && response.data !== "undefined") {
				$('#recent-global-searches-container').html('');
				if (Object.keys(response.data).length > 0) {
					$.each(response.data, function(index, item) {
						$('<li>' + item.trunc(LIMIT_RECENT_LENGTH) + '</li>').attr('data-select', item).appendTo($('#recent-global-searches-container'));
					});
				}
				else {
					$('#recent-global-searches-container').html(noRecentHtml);
				}
			}
    	}).fail(function(data) {
    		$('#recent-global-searches-container').html(noRecentHtml);
    	}).always(function(){
    		removeLoadingMessage("#recent-global-searches-container");
    		recentSearchesLoaded = true;
    	});
    };
    
    
	/**
	 * Get and display suggestions
	 * @memberOf GlobalSearch
	 */
	function loadSuggestions(type, minScore) {
		
		showSuggestions();
		
		$('#global-search-suggestions').html('');
		
		showLoadingMessage('#global-search-suggestions-container');

		
		$.ajax({
			url: Docebo.createAbsoluteUrl("es/main/search", {}, "lms"),
			method: "POST",
			dataType: 'json',
			data: {
				search: 	$('#global-search-input').val(),
				type: 		globalSearchType, 
				from: 		0,
				pageSize: 	settings.suggestionsNum,
				uiType: 	UI_TYPE_SUGGESTIONS,
				minScore: 	settings.minScore
			},
		}).done(function (response) {
			if (response.success && response.data !== "undefined") {
				if (Object.keys(response.data.suggestions).length > 0) {
					$.each(response.data.suggestions, function(index, item) {
						$('<li>' + item + '</li>').attr('data-select', index).appendTo($('#global-search-suggestions'));
					});
				}
			}
		}).fail(function (data) {
			//
		}).always(function() {
			removeLoadingMessage('#global-search-suggestions-container');
		});
		
	};
    
	
	/**
	 * Get and display suggestions
	 * @memberOf GlobalSearch
	 */
	function loadSearchResults(append, updateRecentSearches) {
		
		if (typeof append === "undefined" || !append) {
			$(globalSearchSelectedTab).html('');
		}
		
		var searchString = $('#global-search-input').val();
		if (searchString === "") {
			return;
		}
		
		showLoadingMessage(globalSearchSelectedTab);
		showSearchResults();
		$('.tabbed-content.tabbed-global-search-results .tabbed-content-nav').boostrapSmoothActiveLine({background: '#0465AC'});

		$.ajax({
			url: Docebo.createAbsoluteUrl("es/main/search", {}, "lms"),
			method: "POST",
			dataType: 'json',
			data: {
				search: 	searchString,
				type: 		globalSearchType, 
				from: 		globalSearchFrom,
				pageSize: 	settings.searchResultsNum,
				uiType: 	UI_TYPE_RESULTS,
				minScore: 	settings.minScore
			},
		}).done(function (response) {
			if (response.success && response.data !== "undefined") {
				
				if (globalSearchScrollTimeOut !== null) {
					clearTimeout(globalSearchScrollTimeOut);
				}

				
				if (Object.keys(response.data.results).length > 0) {
					
					$.each(response.data.results, function() {
						$(globalSearchSelectedTab).append(this.html);
					});
					
					globalSearchFrom 		= response.data.next_from;
					globalSearchOnScroll 	= !(response.data.end_of_results);
					
					$('.open-popover').popover();
					
					if (updateRecentSearches === true || typeof updateRecentSearches === "undefined") {
						loadRecentSearches();
					}
					
				}
				else if (typeof append === "undefined" || !append) {
					$(globalSearchSelectedTab).html(noResultsHtml);
				}
				
			}
			
		}).fail(function (data) {
			if (typeof append === "undefined" || !append) {
				$(globalSearchSelectedTab).html(noResultsHtml);
			}
		}).always(function() {
			removeLoadingMessage(globalSearchSelectedTab);
		});
		
	};

	
	/**
	 * 
	 * @memberOf GlobalSearch
	 */
	function showLoadingMessage(targetSelector, messageText) {
		removeLoadingMessage(targetSelector);
		if (!messageText) {
			messageText = Yii.t('standard', 'Searching for relevant matches');
		}
		var html = '<div class="global-search-loading"><i class="fa fa-spinner fa-pulse"></i>   ' + messageText + '...</div>'; 
		$(targetSelector).append(html);
	}
	
	/**
	 * 
	 * @memberOf GlobalSearch
	 */
	function removeLoadingMessage(targetSelector) {
		if (typeof targetSelector !== "undefined") {
			$('.global-search-loading', $(targetSelector)).remove();	
		}
		else {
			$('#global-search-wrapper .global-search-loading').remove();
		}
	}
	
	
    
	/**
	 * Callable methods in form of
	 * $('#mydashboard').dashboard('<method-name>', <arguments>).
	 * @memberOf GlobalSearch
	 */
	var methods = {
	    /**
	     * Init method, called upon plugin attachment
	     * @memberOf methods
	     */
	    init: function(options) {

			// Define the max tab content height in order to be used in the infinite scroll searching logic
			$('.menu-square #global-search-results-container .tabbed-content-tab-content').css('max-height', this.height - 180);

	    	// Declared earlier! 
	    	settings = $.extend({}, settings, options);
	    	
	    	// Declared earlier! Holds the DOM element jQuery object
	    	$gs = this;
			
			// Destroy the 'boostrapSmoothActiveLine' instance, because of the extra ellipsis tab
			$('.tabbed-content.tabbed-global-search-results .tabbed-content-nav').boostrapSmoothActiveLine('destroy');

			// Prepare the popover content from the global variable
			if (typeof globalEllipsisTabContent != "undefined") {
				var elipsisHtml = globalEllipsisTabContent;
			} else {
				elipsisHtml = '';
			}

			// Prepare the Popover and init it here only if there is a ellipsis tab !!!
			if ($('a[href="#tab-ellipsis-global-search"]').length > 0) {
				$('a[href="#tab-ellipsis-global-search"]').attr('data-html', true);
				$('a[href="#tab-ellipsis-global-search"]').attr('data-content', elipsisHtml);
				$('a[href="#tab-ellipsis-global-search"]').attr('data-placement', 'bottom');

				// Detect the iOS or not in order to apply specific popover fix
				var iOS = ['iPad', 'iPhone', 'iPod'].indexOf(navigator.platform) >= 0;

				if (!iOS) {
					// Not iOS - trigger popover on focus
					$('a[href="#tab-ellipsis-global-search"]').attr('data-trigger', 'focus');
				} else {
					// trigger on click - iOS fix http://stackoverflow.com/questions/15467873/bootstrap-popover-not-working-on-ipad
					$('a[href="#tab-ellipsis-global-search"]').attr('data-trigger', 'click');
				}

				// Init the popover
				$('a[href="#tab-ellipsis-global-search"]').popover({'container': '#globalsearch'});
			}

			// Add class 'skip' to the ellipsis tab if it exists !!!
			if ($('a[href="#tab-ellipsis-global-search"]').length > 0) {
				$('a[href="#tab-ellipsis-global-search"]').parent().addClass('skip');
			}
			
			// Add events listeners here
	    	addListeners();


			// Attach event listener for scrolls 
			attachInfiniteSearchScroll('.menu-square #global-search-results-container .tabbed-content-tab-content', '.tabbed-content-tab-pane.active');
			
			// On init, preload recent searches and "show" them (they stay hidden because the second menu is still not visible of course)
			//loadRecentSearches();
			showSuggestions();
			
	    	return this;
	    },
	    
	    /**
	     * A method, called from outside to load recent searches. 
	     * Internally, avoid real loading if this method is already called, unless forceLoad=true is passed
	     * @memberOf methods
	     */
	    loadRecent: function(forceLoad) {
	    	if (recentSearchesLoaded && !forceLoad) {
	    		return;
	    	}
	    	loadRecentSearches();
	    },
	    
	    /**
	     * Set ElasticSearch minimal relevance score
	     * @memberOf methods
	     */
	    setMinScore: function(score) {
	    	settings.minScore = score;
	    }
	    
	    
	    
	};


	/**
	 * Plug it in
     * @memberOf GlobalSearch
	 */
	$.fn.globalsearch = function(methodOrOptions) {
		
        if ( methods[methodOrOptions] ) {
            return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  methodOrOptions + ' does not exist on jQuery.dashboard' );
        }
    };

	
})(jQuery);
