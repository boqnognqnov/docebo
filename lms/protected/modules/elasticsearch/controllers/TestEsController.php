<?php

class TestEsController extends Controller {

	public function actionSearch() {
	    
	    function listResult($result) {
	        echo "<hr>";
	        foreach ($result['hits'] as $hit) {
	            CVarDumper::dump($hit['_id'] . ' (' . $hit['_score'] . ') ', 80, true);
	            CVarDumper::dump($hit, 80, true);
                echo "<br>";	            
	        }
	        unset($result['hits']);
	        CVarDumper::dump($result, 80, true);
	    }
	    
		/* @var Elasticsearch\Client $client */
		/* @var EsClient $es */
		
		$es = Yii::app()->es;
		
		$search = "Documentation";
		$type = false;
		$from=0;
		$pageSize=EsClient::RESULT_PAGE_SIZE;
		$pageSize=20;
		$visibleOnly=true;
		$phraseSearch=true;
		$minScore=1;
		$exactPhrase=false;
		$index=false;
		
		$previousTime = microtime(true);
		
        $result = $es->search($search, $type, $from, $pageSize, $visibleOnly, $phraseSearch, $minScore, $exactPhrase, $index);
        echo microtime(true) - $previousTime;
        
		listResult($result);
		
		/*
		while (!$result['end_of_results']) {
		    $previousTime = microtime(true);
		    $result = $es->search($search, $type, $result['next_from'], $pageSize, $visibleOnly, $phraseSearch, $minScore, $exactPhrase, $index);
            echo microtime(true) - $previousTime;
		    listResult($result);
		}
		*/
		
		
	}
	
	public function actionIndex() {
	    
	}
	
	public function actionDeleteIndex() {
	    $result = Yii::app()->es->deleteIndex('lms68_petkoff_eu');
	    CVarDumper::dump($result, 80, true);
	}
	
	public function actionInit() {
	    
        $indexer = new EsAgentCourse();
        $result = $indexer->index();
        CVarDumper::dump($result, 80, true);
     
        $indexer = new EsAgentLo();
        $result = $indexer->index();
        CVarDumper::dump($result, 80, true);
	    
        $indexer = new EsAgentPlan();
        $result = $indexer->index();
        CVarDumper::dump($result, 80, true);

        $indexer = new EsAgentQandA();
        $result = $indexer->index();
        CVarDumper::dump($result, 80, true);
	    	  
        $indexer = new EsAgentKnowledgeAsset();
        $result = $indexer->index();
        CVarDumper::dump($result, 80, true);
        
	     
	}

	
	
}





