<?php

class MainController extends Controller {


	public function actionIndex() {

	}

	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
	    return array(
	        'accessControl',
	    );
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {
	    
	    return array(
	        array('allow',
	            'actions' => array("search", "axGetRecentSearches"),
	            'users' => array('@'),
	        ),
	    
	        // Deny everything else to all non-logged in users
	        array('deny',
	            'users' => array('*'),
	            'deniedCallback' => array($this, 'accessDeniedCallback'), // This one will trigger user redirect to home page, even in AJAX calls
	        ),
	    );
	}
	
	
    /**
     * @throws CException
     */
    public function actionSearch() {
    	
        $response = new AjaxResult();
    	
        $search     = Yii::app()->request->getParam('search', '');
        $type       = Yii::app()->request->getParam('type', false);
        $from       = Yii::app()->request->getParam('from', 0);
        $pageSize   = Yii::app()->request->getParam('pageSize', false);
        $highlights = Yii::app()->request->getParam('highlights', false);
        $minScore   = Yii::app()->request->getParam('minScore', false);
        $uiType     = Yii::app()->request->getParam('uiType', EsClient::UI_RESULTS); // suggestions OR results
        $viewType   = EsAgent::VIEW_RESULT_ITEM;

        $currentUser = Yii::app()->user->id;
        
        
        $userCatalogs = array();
        if (PluginManager::isPluginActive('CoursecatalogApp')) {
            $userCatalogs = LearningCatalogue::getUserCatalogs($currentUser);
        }
        
        $es     = Yii::app()->es;
        
        // Very important!!!!
        $es->setUserCatalogs($userCatalogs);
        
        $type   = ($type === 'all' ? $type = false : $type); // If 'All' $type = false
        $result = $es->search($search, $type, $from, $pageSize, $minScore);
        
        $searchSuggestions    = array();
        $searchResults        = array(); 

        // Prepare some data used in the foreach cycle below
        $isPu = false;
        $puCourses = array();
        $puCatalogs = array();
        $puCatalogsCourses = array();   // courses in catalogs PU can manage
        $puCatalogsPlans = array();     // plans in catalogs PU can manage
        $puPlans = array();
        $isGodAdmin = Yii::app()->user->isGodAdmin;
        $puManagesAllCourse = false;
	    $puPermissions = array();

        if (PluginManager::isPluginActive('PowerUserApp')) {
        	$isPu  = Yii::app()->user->getIsPu();
        	if ($isPu) {
		        $puPermissions = array(
			        'viewCourses' => Yii::app()->user->checkAccess('/lms/admin/course/view'),
			        'viewCatalogs' => Yii::app()->user->checkAccess('/framework/admin/catalogManagement/mod'),
			        'viewLearningPlans' => Yii::app()->user->checkAccess('/lms/admin/coursepath/view'),
					'viewClassroomSessions' => Yii::app()->user->checkAccess('/lms/admin/classroomsessions/view'),
					'viewWebinarSessions' => Yii::app()->user->checkAccess('/lms/admin/webinarsessions/view'),
		        );

        	    // If PU is assigned "ALL courses", set this variable
        	    if (CoreAdminCourse::getCourseAssignModeForPU(Yii::app()->user->id) === CoreAdminCourse::COURSE_SELECTION_MODE_ALL) {
        	        $puManagesAllCourse = true;
        	    }
        	    
        	    // Courses and plans. member of catalogs, the PU can manage (NOT directly managed!!!!) 
        	    $puCatalogsCourses = CoreAdminCourse::getPowerUserEntriesOfPuCatalogs($currentUser, CoreAdminCourse::TYPE_COURSE, true);
        	    $puCatalogsPlans   = CoreAdminCourse::getPowerUserEntriesOfPuCatalogs($currentUser, CoreAdminCourse::TYPE_COURSEPATH, true);
        		$puCourses         = CoreUserPuCourse::model()->getList($currentUser);
        		
        		if (PluginManager::isPluginActive('CurriculaApp')) {
        			$tmp = CoreAdminCourse::getPowerUserCoursepaths($currentUser);
        			if (is_array($tmp)) {
        				$puPlans = array_keys($tmp);
        			}
        		}
        		
        	}
        }
        
        foreach ( $result['hits'] as $hit ) {

            $agentType  = $hit['_type'];
            
            if ( !in_array($agentType, $es->getAvailableTypes())) {
                continue;
            }
            else {
                $agent = EsAgent::getAgent($agentType);
            }
            
            $view  = EsAgent::VIEWS . "." . strtolower($agentType) . "_". $viewType;
            
            switch ( $uiType ) {
                case EsClient::UI_SUGGESTIONS :
                    $name               		= trim($agent->getEsDocumentTitle($hit['_source']));
                    $searchSuggestions[$name] 	= str_replace($search, "<b>".$search."</b>", Docebo::ellipsis($name, EsClient::LIMIT_RESULT_TITLE));
                    break;
                    
                case EsClient::UI_RESULTS :
                    // If it is first page first thing update Recent Search
                    if ( $from == 0 ) {
                        CoreUser::updateRecentSearch($currentUser, $search);
                    }
                        
					$params = array();
					$params['score']        = $hit['_score'];
					$params['idItem']		= $hit['_id'];
					$params['agent']        = $agentType; // Type of the Agent / Course, LO, Webinar /
					$params['highlights']   = $search; // Word to be highligthed
					$params['document']   	= $hit['_source']; // 
					$params['localData']    = $agent->getLocalData($hit['_id']);
					$params['isPu'] 		= $isPu;
					$params['puCourses']    = $puCourses;
					$params['isGodAdmin']	= $isGodAdmin;
					$params['userCatalogs']	= $userCatalogs;
					$params['puPlans']    	= $puPlans;
					$params['puCatalogsCourses']    	= $puCatalogsCourses;
					$params['puCatalogsPlans']    	    = $puCatalogsPlans;
					$params['puManagesAllCourse']    	= $puManagesAllCourse;
	                $params['puPermissions']       = $puPermissions;
					
						
					$html = $this->renderPartial($view, $params, true);

                    $searchResults[] = array(
						'type'  	=> $hit['_type'],
						'index' 	=> $hit['_index'],
						'score' 	=> $hit['_score'],
						'document'	=> $hit['_source'],
						'html'		=> $html,
					);
                    break;
            }
        }
        
       	$output = array(
       	    'suggestions'       	=> $searchSuggestions,
   			'results'			=> $searchResults,
   			'next_from'			=> $result['next_from'],
            'end_of_results'	=> $result['end_of_results'],
            'grand_total'       => $result['grand_total'],
            'max_score'         => $result['max_score'],
            'recentSearches'    => CoreUser::getRecentSearch($currentUser),
        );
        
        $response->setStatus(true)->setData($output)->toJSON();

    }
    public function actionItemResult() {
        $type   = Yii::app()->request->getParam('type');
        $search = Yii::app()->request->getParam('search');

    }
    
    
    public function actionAxGetRecentSearches() {
        $result = new AjaxResult();
        $recentSearch = CoreUser::getRecentSearch(Yii::app()->user->id);
        $result->setStatus(true)->setData($recentSearch)->toJSON();
    }
    
}




