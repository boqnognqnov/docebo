<?php

extract($localData, EXTR_PREFIX_ALL, "data");

$idPlan 	 		= $idItem;
$name        		= Docebo::ellipsis($document['path_name'], EsClient::LIMIT_RESULT_TITLE);
$description 		= Docebo::ellipsis($document['path_descr'], EsClient::LIMIT_RESULT_DESCRIPTION);
$resultType  		= strtoupper(Yii::t('report_filters', '_FIELDS_COURSEPATHS'));

$reasonToSee 		= EsAgent::reasonToSeeItem($idPlan, LearningCatalogueEntry::ENTRY_COURSEPATH, $isGodAdmin, $isPu, $data_isEnrolled, $puPlans, $userCatalogs);

if ( ($isPu && in_array($idPlan, $puPlans) && (isset($puPermissions['viewLearningPlans']) && $puPermissions['viewLearningPlans'])) || $isGodAdmin ) {
	$url = Docebo::createAbsoluteAdminUrl('CurriculaApp/curriculaManagement/courses', array('id' => $idPlan));
}
else if ($isPu && in_array($idPlan, array_keys($puCatalogsPlans)) && (isset($puPermissions['viewCatalogs']) && $puPermissions['viewCatalogs'])) {
    $url = Docebo::createAbsoluteAdminUrl('CoursecatalogApp/CourseCatalogManagement/assignCourses', array('id' => $puCatalogsPlans[$idPlan]));
}
else if ($data_isInCatalog){
	if (!$data_isEnrolled) {
		$url = Docebo::createAbsoluteLmsUrl('coursepath/details', array('id_path' => $idPlan));
	}
	else {
		$url = Docebo::createAbsoluteLmsUrl('curricula/show', array('id_path' => $idPlan));
	}
}
else if ($data_isEnrolled) {
	$url = Docebo::createAbsoluteLmsUrl('curricula/show', array('id_path' => $idPlan));
}
else {
    $url = Docebo::createAbsoluteLmsUrl('coursepath/details', array('id_path' => $idPlan));
}



?>
<div id="document-<?= EsAgentPlan::TYPE_SHORT . "-" . $idItem ?>" class="elastic-result-box <?=$agent?>">
	<span class="es-score"><?= number_format($score,3) ?></span>
    <span class="type-label"><?=$resultType?></span>
    <span class="reason-to-see"><b><?=$reasonToSee?></b></span>
    <div class="item-title"><a href="<?=$url?>"><?=$name?></a></div>
    <div class="item-description"><?=$description?></div>
</div>
<hr class="elastic-search-line" />