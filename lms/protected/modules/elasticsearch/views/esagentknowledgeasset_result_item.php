<?php
/* @var $data_model App7020Content */

extract($localData, EXTR_PREFIX_ALL, "data");

$id             = $data_model->id;
$firstname 		= $data_model->contributor->firstname;
$lastname  		= $data_model->contributor->lastname;
$title          = Docebo::ellipsis($document['title'], EsClient::LIMIT_RESULT_TITLE);
$description    = Docebo::ellipsis($document['description'], EsClient::LIMIT_RESULT_DESCRIPTION);
$created        = Yii::app()->localtime->toLocalDate($data_model->created);
$knowledgeUrl   = Docebo::createApp7020AssetsViewUrl($id);

?>

<div id="document-<?= EsAgentKnowledgeAsset::TYPE_SHORT . "-" . $id ?>" class="elastic-result-box <?=$agent?>">
	<span class="es-score"><?= number_format($score,3) ?></span>
    <span class="type-label"><?=Yii::t('app7020', 'KNOWLEDGE ASSET')?></span>
    <div class="item-title"><a href="<?=$knowledgeUrl?>"><?=$title?></a></div>
    <div class="item-description"><?=$description?></div>
    
    <div class="info-small">
    	<span class="system-label">
    		<?= Yii::t('app7020', 'Posted by') ?>
    	</span>
    	<?= $firstname. ' ' . $lastname ?>
    	<span class="system-label">
    		<?= Yii::t('standard', 'on') ?>
    	</span>
    	<?= $created ?>
    </div>
    
</div>
<hr class="elastic-search-line" />
