<?php 

extract($localData, EXTR_PREFIX_ALL, "data");

$idQuestion		= $idItem;
$title     		= Docebo::ellipsis($document['title'], EsClient::LIMIT_RESULT_TITLE);
$firstname 		= $data_questionModel->user->firstname;
$lastname  		= $data_questionModel->user->lastname;
$created   		= Yii::app()->localtime->toLocalDate($data_questionModel->created);
$bestAnswerLabel= $data_hasBestAnswer ? '<span class="elastic-result-best-answer">' . Yii::t('app7020', 'Best Answer') . '</span>' : '';
$url 			= Docebo::createAbsoluteApp7020Url("askTheExpert/index", array("#" => "/question/$idQuestion"));
?>

<div id="document-<?= EsAgentQandA::TYPE_SHORT . "-" . $idQuestion ?>" class="elastic-result-box <?=$agent?>">
	<span class="es-score"><?= number_format($score,3) ?></span>
    <span class="type-label"><?=Yii::t('app7020', 'QUESTION & ANSWERS')?></span> <?= $bestAnswerLabel; ?>
    <div class="item-title"><a href="<?=$url?>"><?=$title?></a></div>
    <div class="info-small">
    	<span class="system-label">
    		<?= Yii::t('app7020', 'Posted by') ?>
    	</span>
    	<?= $firstname. ' ' . $lastname ?>
    	<span class="system-label">
    		<?= Yii::t('standard', 'on') ?>
    	</span>
    	<?= $created ?>
    </div>
</div>
<hr class="elastic-search-line" />
