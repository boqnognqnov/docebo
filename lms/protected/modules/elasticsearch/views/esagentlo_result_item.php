<?php

extract($localData, EXTR_PREFIX_ALL, "data");

$resultType  	= strtoupper(Yii::t('standard', 'Training materials'));
$name        	= Docebo::ellipsis($document['title'], EsClient::LIMIT_RESULT_TITLE);
$description 	= Docebo::ellipsis($document['short_description'], EsClient::LIMIT_RESULT_DESCRIPTION);
$courseName  	= $data_courseModel->name;
$idCourse    	= $data_courseModel->idCourse;
$reasonToSee 	= EsAgent::reasonToSeeItem($idCourse, LearningCatalogueEntry::ENTRY_COURSE, $isGodAdmin, $isPu, $data_isEnrolled, $puCourses, $userCatalogs);


if ((($isPu && in_array($idCourse, $puCourses) && (isset($puPermissions['viewCourses']) && $puPermissions['viewCourses']) || ($isPu && $data_isEnrolled))) || $data_isEnrolled || $isGodAdmin) {
    if ($isPu || $isGodAdmin) {
        $data_lockedCourse['locked'] = false;
    }
	$courseUrl = Docebo::createAbsoluteUrl('player', array('course_id' => $idCourse));
}
else if ($isPu && in_array($idCourse, array_keys($puCatalogsCourses)) && (isset($puPermissions['viewCatalogs']) && $puPermissions['viewCatalogs'])) {
    $courseUrl = Docebo::createAbsoluteAdminUrl('CoursecatalogApp/CourseCatalogManagement/assignCourses', array('id' => $puCatalogsCourses[$idCourse]));
}
else {
	$courseUrl = Docebo::createAbsoluteUrl('course/details', array('id'=> $idCourse));
}


if (isset($data_lockedCourse['locked']) && $data_lockedCourse['locked']) {
    if (isset($data_lockedCourse['reason'])) {
        $courseName  = '<span class="locked-has-reason" title="'.$data_lockedCourse['reason'].'"><i class="fa fa-lock fa-lg"></i></span>  ' . $courseName;
    }
    else {
        $courseName = '<i class="fa fa-lock fa-lg"></i>  ' . $courseName;
    }
}
else {
    $courseName = '<a href="'.$courseUrl.'"> ' . $courseName . '</a>';
}


?>


<div id="document-<?= EsAgentLo::TYPE_SHORT . "-" . $idCourse ?>" class="elastic-result-box <?=$agent?>" data-hit-score="<?= $score ?>">
	<span class="es-score"><?= number_format($score,3) ?></span>
    <span class="type-label"><?=$resultType?></span>
    <span class="reason-to-see-label"><b><?= $reasonToSee ?></b></span>
    <div class="item-title"><?=$name?></div>
    <div class="item-description"><?=$description?></div>
    <div class="info-small"><span class="info-label"><?= Yii::t('standard', '_COURSE') ?>: </span><?= $courseName ?></div>
</div>
<hr class="elastic-search-line" />