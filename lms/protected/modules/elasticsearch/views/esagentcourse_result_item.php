<?php

extract($localData, EXTR_PREFIX_ALL, "data");

$currentUser 	= Yii::app()->user->id;
$name        	= Docebo::ellipsis($document['name'], EsClient::LIMIT_RESULT_TITLE);
$description 	= Docebo::ellipsis($document['description'], EsClient::LIMIT_RESULT_DESCRIPTION);
$courseType  	= strtoupper($data_courseModel->course_type);
$idCourse 	 	= $idItem;
$reasonToSee 	= EsAgent::reasonToSeeItem($idCourse, LearningCatalogueEntry::ENTRY_COURSE, $isGodAdmin, $isPu, $data_isEnrolled, $puCourses, $userCatalogs);


if ((($isPu && ($puManagesAllCourse || in_array($idCourse, $puCourses)) && (isset($puPermissions['viewCourses']) && $puPermissions['viewCourses'])) || ($isPu  && $data_isEnrolled)) || $data_isEnrolled || $isGodAdmin) {
    if ($isPu || $isGodAdmin) {
        $data_lockedCourse['locked'] = false;
    }
	if(in_array($data_courseModel->course_type, array(LearningCourse::TYPE_CLASSROOM, LearningCourse::TYPE_WEBINAR))
		&& $data_courseModel->selling
		&& $isPu
		&& !$data_isEnrolled
	) {
		if($data_courseModel->course_type == LearningCourse::TYPE_CLASSROOM && $puPermissions['viewClassroomSessions'])
			$courseUrl = Docebo::createLmsUrl('player/training/session', array('course_id' => $idCourse));
		elseif($data_courseModel->course_type == LearningCourse::TYPE_WEBINAR && $puPermissions['viewWebinarSessions'])
			$courseUrl = Docebo::createLmsUrl('player/training/webinarSession', array('course_id' => $idCourse));
		else
			$courseUrl = Docebo::createAbsoluteLmsUrl('course/details', array('id'=> $idCourse));
	}
	else
		$courseUrl = Docebo::createAbsoluteLmsUrl('player', array('course_id' => $idCourse));
}
else if ($isPu && in_array($idCourse, array_keys($puCatalogsCourses)) && (isset($puPermissions['viewCatalogs']) && $puPermissions['viewCatalogs'])) {
    $courseUrl = Docebo::createAbsoluteAdminUrl('CoursecatalogApp/CourseCatalogManagement/assignCourses', array('id' => $puCatalogsCourses[$idCourse]));
}
else {
	$courseUrl = Docebo::createAbsoluteLmsUrl('course/details', array('id'=> $idCourse));
}


if (isset($data_lockedCourse['locked']) && $data_lockedCourse['locked']) {
    if (isset($data_lockedCourse['reason'])) {
        $name = '<span class="locked-has-reason" title="'.$data_lockedCourse['reason'].'"><i class="fa fa-lock fa-lg"></i></span>  ' . $name;
    }
    else {
        $name = '<i class="fa fa-lock fa-lg"></i>  ' . $name;
    }
}
else {
    $name = '<a href="'.$courseUrl.'">' . $name . '</a>';
}


?>
<div id="document-<?= EsAgentCourse::TYPE_SHORT . "-" . $idCourse ?>" class="elastic-result-box <?=$agent?>">
	<span class="es-score"><?= number_format($score,3) ?></span>
    <span class="type-label"><?=$courseType?></span>
    <span class="reason-to-see"><b><?=$reasonToSee?></b></span>
    <div class="item-title"><?= $name; ?></div>
    <div class="item-description"><?=$description?></div>
</div>
<hr class="elastic-search-line" />