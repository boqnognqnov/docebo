<?php
/**
 * 
 *
 */
class ElasticSearchModule extends CWebModule
{
    
    private $_assetsUrl;
    
	public function init() {
		$this->setImport(array(
			'elasticsearch.components.*',
		    'elasticsearch.components.agents.*',
		));
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see CWebModule::beforeControllerAction()
	 */
	public function beforeControllerAction($controller, $action) {
		if(parent::beforeControllerAction($controller, $action)) {
			return true;
		}
		else {
		    return false;
		}
			
	}
	
	
	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl()
	{
	    if ($this->_assetsUrl === null) {
	        $this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('elasticsearch.assets'));
	    }
	    return $this->_assetsUrl;
	}
	
	
}
