<?php
/**
 * 
 * Module specific base controller
 * 
 */
class AuthoringBaseController extends LoBaseController
{
    /**
     * (non-PHPdoc)
     * @see CController::init()
     */
    public function init()
	{
		return parent::init();
	}
	
	/**
	 * (non-PHPdoc)
	 * Method invoked before any action
	 * @param CAction $action
	 * @return bool
	 */
	public function beforeAction($action)
	{
		return parent::beforeAction($action);
	}

	

	/**
	 * Register module specific assets
	 * 
	 * (non-PHPdoc)
	 * @see LoBaseController::registerResources()
	 */
	public function registerResources() {
		
		parent::registerResources();
		
		if (!Yii::app()->request->isAjaxRequest) {
			
			// Use
			// $this->module->assetsUrl
			// $this->getPlayerAssetsUrl() <-- shared between all player related modules 
			
			$cs = Yii::app()->getClientScript();
			
		}
	}
		
}