<?php

class DefaultController extends AuthoringBaseController
{
	public function actionIndex()
	{		
		$this->render('index');
	}
	
	
	/**
	 * Show LO launchpad
	 */
	public function actionAxLaunchpad() {
		
		$response = new AjaxResult();
		
		//load content
		$html = $this->renderPartial("_launchpad", array(
		), true);
		
		$response->setHtml($html);
		$response->setStatus(true);

		//send result
		$response->toJSON();
		Yii::app()->end();
		
		
	}

	public function actionKeepAlive() {
		// extend the session
		Yii::app()->user->getIsGuest();

		$data = array(
			'success' => true
		);

		header('Content-type: text/javascript');
		if (isset($_GET['jsoncallback'])) {
			// JSONP callback
			$jsoncallback = preg_replace('/[^a-zA-Z\._]+/', '', $_GET['jsoncallback']);
			echo $jsoncallback. '(' . CJSON::encode($data) . ')';
		} else {
			// standard Json
			echo CJSON::encode($data);
		}

		// Update course+user session tracking to register user's course activity (time in course)
		if (isset($_REQUEST['course_id']))
			ScormApiBase::trackCourseSession((int)Yii::app()->user->idst, $_REQUEST['course_id']);

		Yii::app()->end();
	}

	
	
}