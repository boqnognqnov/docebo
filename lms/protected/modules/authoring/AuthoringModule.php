<?php

class AuthoringModule extends CWebModule
{
	private $_assetsUrl;
	
	public function init()
	{
		$this->setImport(array(
			'authoring.models.*',
			'authoring.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			return true;
		}
		else
			return false;
	}
	

	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('authoring.assets'));
		}
		return $this->_assetsUrl;
	}
	
	
}
