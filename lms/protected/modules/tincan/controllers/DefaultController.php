<?php

class DefaultController extends TincanBaseController
{

	//get/set

	public function getLrsEndpoint() {
		return Docebo::getRootBaseUrl(true) . "/tcapi/";
	}


	//actions

	/**
	 * By default, just redirect to main site.
	 *
	 */
	public function actionIndex() {
		$this->redirect($this->createUrl('/site'), true);
	}

	/**
	 * Update existing TinCan package file structure using just uploaded ZIP file
	 *
	 * @param number $idOrg The LO to update
	 * @param array $file Uploaded file (<i>plupload</i> array format)
	 * @throws CException
	 */
	protected function updateTincanPackage($idOrg, $file, $tinCanTitle) {

		$response = new AjaxResult();

		try {
		    
		    $shortDescription = urldecode(Yii::app()->request->getParam("short_description", ''));

			// Use the physically uploaded filename, put by PLUpload in the "target_name" element
			// This is true when "unique_names" option is used for plupload JavaScript object
			if (is_array($file) && isset($file['name'])) {
				$originalFileName = urldecode($file['name']);
				if (isset($file['target_name'])) {
					$file['name'] = $file['target_name'];
				}
			}

			// Validate file input
			if (!is_array($file)) { throw new CException("Invalid file."); }
			if (!isset($file['name']) || ($file['name'] == '')) { throw new CException("Invalid file name."); }
			if (!isset($file['percent']) || $file['percent'] < 100) { throw new CException("Incomplete file upload."); }
			if ('zip' != strtolower(CFileHelper::getExtension($file['name']))) { throw new CException("Invalid file format."); }

			$uploadTmpFile = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR . $file['name'];
			if (!is_file($uploadTmpFile)) { throw new CException("File '" . $file['name'] . "' does not exist."); }


			$loModel = LearningOrganization::model()->findByPk($idOrg);
			if (!$loModel) { throw new CException("Invalid learning object"); }

			$package = isset($loModel->tincanMainActivity->ap) ? $loModel->tincanMainActivity->ap : FALSE;
			if (!$package) { throw new CException("Old TinCan package not found"); }

			$package->extractTincanPackage($uploadTmpFile, $package->path);

			$package->save();
			
			$loModel->title = $tinCanTitle;
			$loModel->idResource = $package->mainActivity->id_tc_activity;
			$loModel->short_description = $shortDescription;
			$loModel->enable_oauth = (bool) Yii::app()->request->getParam("enable_oauth", false);
			$loModel->oauth_client = Yii::app()->request->getParam("oauth_client", null);
			$loModel->use_xapi_content_url = Yii::app()->request->getParam("use_xapi_content_url", false);
				
			if(!$tinCanTitle){
				$loModel->title = $package->getTitle();
			}

			$loModel->saveNode();

			$data = array(
				'idPackage' => $package->getPrimaryKey(),
				'message' => Yii::t('standard', '_OPERATION_SUCCESSFUL'),
			);
			$response->setData($data)->setStatus(true);

		} catch (CException $e) {
			$response->setMessage($e->getMessage())->setStatus(false);
		}

		$response->toJSON();
		Yii::app()->end();

	}


	/**
	 * Save uploaded LO. Caller must already saved the file into
	 * <root>/files/upload_tmp folder.
	 *
	 */
	public function actionAxSaveLo() {

		$courseId         = Yii::app()->request->getParam("course_id", null);
		$parentId         = Yii::app()->request->getParam("parent_id", null);
		$file             = Yii::app()->request->getParam("file", null);
		$useXapiUrl       = (bool) Yii::app()->request->getParam("use_xapi_content_url", false);
		$idOrg            = (int) Yii::app()->request->getParam("id_org", false);
		
		// URLDecode
		$title = urldecode(trim(Yii::app()->request->getParam("title", null)));
		$description = urldecode(Yii::app()->request->getParam("description", null));
		$shortDescription = urldecode(Yii::app()->request->getParam("short_description", ''));
		$thumb = Yii::app()->request->getParam("thumb", null);
		$tinCanTitle =  trim(strip_tags(Yii::app()->request->getParam("new-tincan-title", null)));

		if (is_array($file) && isset($file['name'])) {
			// Use the physically uploaded filename, put by PLUpload in the "target_name" element
			// This is true when "unique_names" option is used for plupload JavaScript object
			$originalFileName = urldecode($file['name']);
			if (isset($file['target_name'])) {
				$file['name'] = $file['target_name'];
			}
			$file['name'] = urldecode($file['name']);
		}

		$response = new AjaxResult();
		
		
		if ($idOrg > 0) {
		    try {
		        
    		    $lo = LearningOrganization::model()->findByPk($idOrg);
    		    
    		    if ($useXapiUrl) {
    		        $file = LearningTcAp::createTemporaryTincanZip(
    		            (int) Yii::app()->request->getParam("xapi_content_url", 0),
    		            Yii::app()->request->getParam("xapi_content_url_title", ""),
    		            Yii::app()->request->getParam("xapi_content_url_description", "")
    		        );
    		    }
    		    
    			if (($file !== 'false') && ($file !== null) && ($file !== false)) {
    			    
    			    // This method will update and FINISH THE SCRIPT!!!
    			    if ($useXapiUrl) {
    			        $tinCanTitle = null;
    			    }
    				$this->updateTincanPackage($idOrg, $file, $tinCanTitle);
    				
    			} else {
       				if ($lo)  {
    					$lo->title = $tinCanTitle;
    					$lo->short_description = $shortDescription;
    					$lo->enable_oauth = (bool) Yii::app()->request->getParam("enable_oauth", false);
    					$lo->oauth_client = Yii::app()->request->getParam("oauth_client", null);
    					$lo->use_xapi_content_url = Yii::app()->request->getParam("use_xapi_content_url", false);
    					if (intval($thumb) > 0){
    						$lo->resource = intval($thumb);
    					} elseif($lo->resource && !$thumb){
    						$lo->resource = 0;
    					}
    					$response->setData(array('message' => Yii::t('standard', '_OPERATION_SUCCESSFUL')))->setStatus(true);
    				}
    			}
    			
    			$lo->use_xapi_content_url = (bool) $useXapiUrl;
    			$lo->saveNode();
    
    			// Trigger event to let plugins save their own data from POST
    			Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
    				'loModel' => LearningOrganization::model()->findByPk($idOrg)
    			)));
    			$response->toJSON();
    			Yii::app()->end();
    			
		    }
		    catch (CException $e) {
		        $response->setMessage($e->getMessage())->setStatus(false);
		        echo $response->toJSON();
		        Yii::app()->end();
		    }
		    
		}
		

		$transaction = Yii::app()->db->beginTransaction();

		try {
		    
		    // If TinCan is from a URL, lets generate the temporary zip file (instead of using an uploaded one)
		    if ($useXapiUrl) {
		        $file = LearningTcAp::createTemporaryTincanZip(
		            (int) Yii::app()->request->getParam("xapi_content_url", 0),
		            Yii::app()->request->getParam("xapi_content_url_title", ""),
		            Yii::app()->request->getParam("xapi_content_url_description", "")
	            );
		    }
		    
			// Validate input parameters
			if (!$courseId) { throw new CException("Invalid specified course."); }
			
			// Validate file input
			if (!is_array($file)) { throw new CException("Invalid file."); }
			if (!isset($file['name']) || ($file['name'] == '')) { throw new CException("Invalid file name."); }
			if (!isset($file['percent']) || $file['percent'] < 100) { throw new CException("Incomplete file upload."); }

			$fullFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $file['name'];
			/*** VIRUS SCAN ******************************************/
			if (isset(Yii::app()->params['virusscan_enable']) && (Yii::app()->params['virusscan_enable'] == true)) {
				$vs = new VirusScanner();
				$clean = $vs->scanFile($fullFilePath);
				if (!$clean) {
					FileHelper::removeFile($fullFilePath);
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				}
			}
			/*********************************************************/

			/*** File extension type validate ************************/
			$fileValidator = new FileTypeValidator(FileTypeValidator::TYPE_ARCHIVE);
			if (!$fileValidator->validateLocalFile($fullFilePath))
			{
				FileHelper::removeFile($fullFilePath);
				throw new CException($fileValidator->errorMsg);
			}
			/*********************************************************/


			$object = new LearningTcAp();
			$object->registration = LearningTcAp::generateStatementUUID();
			$object->location = ''; //TO DO: get cloudfront information from storage class

			$object->setFile($file['name']);
			$object->setTitle($title);
			$object->setIdCourse($courseId);
			$object->setIdParent($parentId);

			$rs = $object->save();
			if (!$rs) {
				//TO DO: revert file storing
				Yii::log('Error while saving DB information; error(s): '.print_r($object->getErrors(), true), CLogger::LEVEL_ERROR);
				throw new CException('Error while saving DB information; error(s): '.print_r($object->getErrors(), true));
			}

			$org = LearningOrganization::model()->findByAttributes(array(
				'idResource' => $object->mainActivity->getPrimaryKey(),
				'objectType'=>LearningOrganization::OBJECT_TYPE_TINCAN,
				'idCourse'   => $courseId
			));

			if($org){
			    $org->enable_oauth 		        = (bool) Yii::app()->request->getParam("enable_oauth", false);
			    $org->oauth_client    	        = Yii::app()->request->getParam("oauth_client", null);
			    $org->use_xapi_content_url    	= Yii::app()->request->getParam("use_xapi_content_url", false);
				$org->short_description         = $shortDescription;
				if($thumb)
					$org->resource = intval($thumb);
				if(!$org->saveNode()){
					Yii::log(var_export($org->getErrors(),true), CLogger::LEVEL_ERROR);
				}
			}else{
				throw new CException('Can not find organization for TinCan. Unable to save short description and thumbnail');
			}

			
			// Create OAuth2 Client Application 
			$this->createOAuth2Client($object->registration, $org->title, $shortDescription);

            if ($transaction->getActive() === true) {
                $transaction->commit();
            }

			//---

			// Trigger event to let plugins save their own data from POST
			Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
				'loModel' => $org
			)));

			$data = array(
				'title' => $title,
				'message' => Yii::t('standard', '_OPERATION_SUCCESSFUL')
			);
			$response->setData($data)->setStatus(true);

			// remove tmp unpack dir and zip file
			// FileHelper::removeDirectory($unpackPath);
			// FileHelper::removeFile($uploadTmpFile);

		} catch (CException $e) {

            if ($transaction->getActive() === true) {
                $transaction->rollback();
            }

            $response->setMessage($e->getMessage())->setStatus(false);
		}

		echo $response->toJSON();
		Yii::app()->end();
	}



	public function actionAxGetLaunchInfo() {

		$idObject = Yii::app()->request->getParam("id_object", 0);
		$object = LearningOrganization::model()->findByPk($idObject);
		$response = new AjaxResult();

		try {
			if (!$object || $object->objectType != LearningOrganization::OBJECT_TYPE_TINCAN) {
				throw new CException('Invalid TinCan Learning Object: ' . $idObject);
			}

			// Launcher Activity (as per tincan.xml)
			$activity = LearningTcActivity::model()->findByPk($object->idResource);
			if (!$activity) {
				throw new CException('Invalid Activity: '.$object->idResource);
			}

			// Activity Provider (not to mess with Activity (which are children of AP)
			$activityProvider = $activity->ap;  // using AR relation

			$userData = CoreUser::model()->findByPk(Yii::app()->user->id);
			$auth = LearningTcAp::buildBasicAuth($userData);
			$actor = $activityProvider->buildActor($userData);

			// If the TinCan LAUNCH attribute is an absolute URI, we use it to launch it
			// Otherwise, we build an URL to access the TinCan, assumably, uploaded to S3
			if (Docebo::isAbsoluteUri($activity->launch)) { 
				$launch = $activity->launch;
			}
			else {
				$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_TINCAN);
				$launch = $storageManager->fileUrl($activity->launch, $activityProvider->path);
			}

			// Check if the URL has a query and send the result back to JS; just to easy the process
			$parsed = parse_url($launch);
			$hasQueryString = isset($parsed['query']);

			$backUrl = '#';
			
			$data = array(
				'idReference' => $idObject,
				'activityId' => $activity->id,
				'endpoint' => $this->getLrsEndpoint(),
				'launch' => $launch,
				'actor' => CJSON::encode($actor),
				'auth' => $auth,
				'registration' => $activityProvider->registration,
				'back_url' => $backUrl,
				'acceptLanguage' => Yii::app()->getLanguage(),
			    'content_token'  => $activityProvider->registration,
				'hasQueryString'	=> $hasQueryString,
			);
			
			// Some customers, (Thromson Reuters,..) asked us to send a precreated OAuth2 Authorization code
			// It expires in 30 secs (by default). This is for TinCan/XAPI only!
			if ($object->enable_oauth) {
			    $data['xapi_auth_code'] = LearningTcAp::getOAuthAuthorizationCode($object->oauth_client);
			}
			
			// Also, based on Advanced Settings -> Advanced -> XAPI -> 
			// Add more parameters to URL... see list below
			if (Settings::get('extended_tincan_launch_params', 'off') === 'on') {
			    $course = $object->course;
			    $data['extended_params'] = array(
                    'course_code'       => urlencode($course->code),
			        'course_id'         => (int) $course->idCourse,
			        'username'          => urlencode($userData->getClearUserId()),
			        'user_id'           => (int) $userData->idst,
                );
			}
			
			// Let custom plugins intercept this and change params for the launch
			Yii::app()->event->raise('BeforeBuildingXapiLaunchUrl', new DEvent($this, array('data' => &$data)));

			$response->setStatus(true);
			$response->setData($data);
			$response->toJSON();
			Yii::app()->end();

		}
		catch (CException $e) {
			$response->setStatus(false)->setMessage($e->getMessage())->toJSON();
			Yii::app()->end();
		}

	}

	public function actionKeepAlive() {
		// extend the session
		Yii::app()->user->getIsGuest();

		$data = array(
			'success' => true
		);

		header('Content-type: text/javascript');
		if (isset($_GET['jsoncallback'])) {
			// JSONP callback
			$jsoncallback = preg_replace('/[^a-zA-Z\._]+/', '', $_GET['jsoncallback']);
			echo $jsoncallback. '(' . CJSON::encode($data) . ')';
		} else {
			// standard Json
			echo CJSON::encode($data);
		}

		// Update course+user session tracking to register user's course activity (time in course)
		if (isset($_REQUEST['course_id']))
			ScormApiBase::trackCourseSession((int)Yii::app()->user->idst, $_REQUEST['course_id']);

		Yii::app()->end();
	}



	/**
	 * Register Controller specific assets
	 *
	 * (non-PHPdoc)
	 * @see TincanBaseController::registerResources()
	 */
	public function registerResources() {
		parent::registerResources();

		if (!Yii::app()->request->isAjaxRequest) {
		}

	}

	
	private function createOAuth2Client($id, $name, $description) {
	    
	    $clientModel = new OauthClients();
	    $clientModel->client_id = $id;
	    $clientModel->client_secret = Docebo::randomHash();
	    $clientModel->redirect_uri = "";
	    $clientModel->app_name = $name;
	    $clientModel->app_description = $description ? $description : $name;
	    $clientModel->hidden = 1;
	    
	    // False is important, because redirect_uri is REQUIRED by the model, but we have to set it empty
	    if (!$clientModel->save(false)) {
	        Yii::log('Failed to save OAuthClient', CLogger::LEVEL_ERROR);
	        Yii::log('Errors:', CLogger::LEVEL_ERROR);
	        Yii::log($clientModel, CLogger::LEVEL_ERROR);
	    }
	    
	}
	
	
}