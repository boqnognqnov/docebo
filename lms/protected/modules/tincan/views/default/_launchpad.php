<div class="row-fluid">
	<div class="span10">
		<h2 class="page-title"><?=$title?></h2>
		<?php
		if (PluginManager::isPluginActive('Share7020App')) {
			$this->widget('common.widgets.CourseLoHeaderApp7020', array('loID' => $idReference));
		}
		?>
	</div>
	<div class="span2 text-right close-tincan">
		<a href="<?=$backUrl?>"><i class="icon-remove icon-black">&nbsp;</i><?php echo Yii::t('standard', '_CLOSE'); ?></a>
	</div>
</div>
<div>
	<?php
		echo '<iframe style="display: block; width: 100%; border: none;" id="tincan_apiframe" frameborder="0" scrolling="yes" src=""></iframe>';
	?>
</div>
<script type="text/javascript">
// Building the launcher link in order to deliver this activity
$(document).ready(function(){
	// Make IFRAME height 75% of the window
	var iframeHeight = window.innerHeight;
	if (iframeHeight === undefined) {
		iframeHeight = document.documentElement.clientHeight;
		if (iframeHeight === undefined) {
			iframeHeight = document.body.clientHeight;
		}
	}
	iframeHeight = iframeHeight * 75 / 100;
	var link = "<?php echo $launch; ?>"+"?endpoint="+encodeURIComponent('<?php echo $endpoint; ?>')
		+"&auth=" + encodeURIComponent('<?php echo $auth; ?>')
		+"&actor=" + encodeURIComponent('<?php echo $actor; ?>')
		+"&registration=" + encodeURIComponent('<?php echo $registration; ?>');
	// Set height and load source
	$("#tincan_apiframe").css("height", iframeHeight).css("border", "none").attr("src",link);
});
</script>