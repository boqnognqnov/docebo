<style>
<!--
	.modal {
		width: 460px;
		margin-left: -230px;
	}
	
	.modal-footer {
		text-align: center;
	}
	
-->
</style>
<h1><?= Yii::t('standard','_ERRORS') ?></h1>
<div class="row-fluid" style="text-align: center;">
	<div class="<?= $type ?>"><?= $message ?></div>
</div>



<div class="form-actions">
	<?= CHtml::button(Yii::t('standard', '_CLOSE'), array('class' => 'btn-cancel close-dialog')); ?>
</div>


