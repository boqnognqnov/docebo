<?php

class DefaultController extends PermissionController {


	/**
	 * (non-PHPdoc)
	 * @see PermissionController::init()
	 */
	public function init() {
		parent::init();
	}


	/**
	 * (non-PHPdoc)
	 * @see ForumBaseController::beforeAction()
	 */
	public function beforeAction($action) {
		return parent::beforeAction($action);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
				'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {

		return array(

				// God Admin users can do all
				array('allow',
					'users'=>array('@'),
					'expression' => 'Yii::app()->user->isGodAdmin',
				),

				// Logged in users
				array('allow',
					'actions' => array('index'),
					'users'=>array('@'),
				),

				// Logged in users, Course Admins
				array('allow',
						'actions' => array(),
						'users'=>array('@'),
						'expression' => 'Yii::app()->controller->userCanAdminCourse',
				),


				// Deny by default
				array('deny', // if some permission is wrong -> the script goes here
						'users' => array('*'),
						'deniedCallback' => array($this, 'accessDeniedCallback'),
						'message' => Yii::t('course', '_NOENTER'),
				),

		);
	}


	/**
	 * List threads of a given forum
	 *
	 * @throws CException
	 */
	public function actionIndex() {

	}

	/**
	 * Register module specific assets
	 */
	public function registerResources() {

		parent::registerResources();

	}

}