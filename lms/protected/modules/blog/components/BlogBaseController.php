<?php
/**
 *
 * Blog Module base controller
 *
 */
class BlogBaseController extends Controller {

	public $course_id = false;
	public $forum_id = false;
	public $thread_id = false;
	public $message_id = false;
	public $userCanAdminCourse = false;
	public $courseUser = null;
	public $threadModel = null;
	public $forumModel = null;
	public $messageModel = null;

	public function init() {

		$this->course_id = Yii::app()->request->getParam("course_id", false);

		// No course (from request) ?  Nothing to show here... get out..
		if (!$this->course_id) {
			Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
			$this->redirect(array('/site'),true);
		}

		// Is this a power user and the course was assigned to him?
		$pUserCourseAssigned = CoreUserPuCourse::model()->findByAttributes(array(
			'puser_id' => Yii::app()->user->id,
			'course_id' => $this->course_id
		));

		$courseLevel = LearningCourseuser::userLevel(Yii::app()->user->id, $this->course_id);

		// Set this controller attribute for later usage in forum admin UIs: Course Admins CAN admin course forums in any way
		if (Yii::app()->user->isGodAdmin){
			//if it's god admin, course user level doesn't matter
			$this->userCanAdminCourse = true;
		}elseif($courseLevel && $courseLevel > LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT){
			// User subscribed to course and his level is high enough
			$this->userCanAdminCourse = true;
		}elseif($pUserCourseAssigned && Yii::app()->user->checkAccess('/lms/admin/course/mod')){
			// User power user, course assigned to him, and he has permissions to modify
			$this->userCanAdminCourse = true;
		}


		// Access to this course forums is granted to course subscribers only
		if (!$this->userCanAdminCourse && !LearningCourseuser::isSubscribed(Yii::app()->user->id, $this->course_id) && !Yii::app()->user->isGodAdmin) {
			Yii::app()->user->setFlash('error', Yii::t('course', '_NOENTER'));
			$this->redirect(array('/site'),true);
		}

		// If no forum id has been specified, grant access to Admins only.
		// Students can access only a given FORUM, and not to LIST course forums!
		if (!$this->userCanAdminCourse && !Yii::app()->user->isGodAdmin) {
			Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
			$this->redirect(array('/site'),true);
		}

		/**
		 * Load LearningCourseuser model for future checks (levels etc)
		 */
		$this->courseUser = LearningCourseuser::model()->findByAttributes(array(
			'idUser' => Yii::app()->user->id,
			'idCourse' => $this->course_id,
		));


		// By now we should have a course id available; Set Yii app "player" component to help other LMS parts to know where we are
		Yii::app()->player->setCourse(LearningCourse::model()->findByPk($this->course_id));

		// Load Tinymce jQuery integrator
		Yii::app()->tinymce->init();

		return parent::init();
	}

	/**
	 * Overwrite resolveLayout method to use module based layout
	 *
	 * (non-PHPdoc)
	 * @see Controller::resolveLayout()
	 */
	public function resolveLayout()
	{
		$this->layout = '/layouts/base';
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::beforeAction()
	 */
	public function beforeAction($action)
	{
		$this->registerResources();
		return parent::beforeAction($action);
	}


	/**
	 * Register JS/CSS
	 */
	public function registerResources() {

		if (!Yii::app()->request->isAjaxRequest) {

			$assetsUrl = $this->module->assetsUrl;
			$cs = Yii::app()->getClientScript();

            // Register forum.css
            $cs->registerCssFile(Yii::app()->baseUrl.'/../plugins/MyBlogApp/assets/css/myblog.css');
		}
	}
}