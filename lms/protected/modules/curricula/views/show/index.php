<?php
    if ( $currentCurricula ) {
        $this->breadcrumbs = array(
            Yii::t('standard', '_COURSEPATH'),
            $currentCurriculaModel->path_name,
        );
        ?>
    <h3 class="title-bold back-button">
        <?php
        $backUrl = Yii::app()->createUrl('curricula/show');
        echo CHtml::link(Yii::t('standard', '_BACK'), $backUrl);
    }
?>
    </h3>
<hr style="margin-top: 0px;" />

<?php if (!$currentCurricula) : ?>
    <?php $this->breadcrumbs = array(
        Yii::t('standard', '_COURSEPATH'),
    ); ?>

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'curricula-management-form',
        'htmlOptions' => array(
            'class' => 'ajax-grid-form',
            'data-grid' => '#group-management-grid'
        ),
    )); ?>

    <div class="main-section group-management">
        <h3 class="title-bold"><?php echo Yii::t('standard', '_COURSEPATH') ?></h3>

        <div class="filters-wrapper">
            <div class="filters clearfix">
                <div class="radio-filter">
                    <label for="radio_filter_1"><input type="radio" name="filter" id="radio_filter_1" value="all" checked="checked"> <?php echo Yii::t('standard', '_ALL'); ?></label>
                    <label for="radio_filter_2"><input type="radio" name="filter" id="radio_filter_2" value="in_progress"> <?php echo Yii::t('standard', '_USER_STATUS_BEGIN'); ?></label>
                    <label for="radio_filter_3"><input type="radio" name="filter" id="radio_filter_3" value="completed"> <?php echo Yii::t('standard', 'Completed'); ?></label>
                </div>
                <div class="input-wrapper">
                    <input type="text" id="advanced-search-curricula" class="typeahead" placeholder="<?= Yii::t('standard', '_SEARCH'); ?>">
                <span class="search-icon"></span>
                </div>
                <div class="radio-filter-dropdown">
                    <a href="javascript:;" id="curricula-dropdown-ctrl-expand" onclick="dropdownExpandAll();return false;"><?php echo Yii::t('curricula_management', 'Expand all'); ?></a> |
                    <a href="javascript:;" id="curricula-dropdown-ctrl-collapse" onclick="dropdownCollapseAll();return false;"><?php echo Yii::t('curricula_management', 'Collapse all'); ?></a>
                </div>
            </div>
        </div>
    </div>
    <?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
    <?php $this->endWidget(); ?>
<?php endif; // end if !$currentCurricula ?>


<?php

    $event = new DEvent($this, array( 'rows' => $rows, 'currentCurricula' => $currentCurricula));
    // Trigger event to let plugins perform custom actions before rendering the shopping cart
    Yii::app()->event->raise('OnBeforeRenderLearningPlanCourses', $event);
    if(!$event->shouldPerformAsDefault() && !empty($event->return_value)){
        echo $event->return_value;
    }else{
        echo $this->renderPartial('_curricula-list', array('rows' => $rows, 'currentCurricula'=>$currentCurricula));
    }

?>

<script type="text/javascript">
$(document).ready(function(){
	$("input[type='radio'][name='filter']").styler();

	$('.curricula-list .dropdown-row.header').live('click', function(event){
		dropdownClick($(this));
		return false;
	});

	$('#curricula-management-form').submit(function(e){
		curriculaFilters('<?= addslashes(Yii::t('standard', '_SEARCH')); ?>');
		e.preventDefault();
	});

	$(".radio-filter .jq-radio").click(function(e){
		curriculaFilters('<?= addslashes(Yii::t('standard', '_SEARCH')); ?>');
		e.preventDefault();
	});
});
</script>