<?php
/* @var $row LearningCoursepath */
$data = $row->getCoursepathPercentage();
$courses = $row->getCoursepathCourseDetails();
$certificateTemplate = $row->certificate; /* @var $certificateTemplate LearningCertificateCoursepath */
if($certificateTemplate && $certificateTemplate->id_certificate != 0)
    $generatedCertificate = LearningCertificateAssignCp::loadByUserPath(Yii::app()->user->id, $certificateTemplate->id_path, $certificateTemplate->id_certificate);
else
    $generatedCertificate = null;

$currencySymbol = Settings::get("currency_symbol");

?>
<div class="curricula-conteiner" id="curricula-<?= $row->id_path?>">
    <div class="curricula-progress">
		<div id="player-course-percentgraph">
            <input class="player-course-graph-<?= $row->id_path ?>" value="<?php echo $data['percentage'] ?>" style="display: none;">
            <div id="player-course-percentage">
                <?php echo $data['percentage'] ?><span>%</span>
            </div>
        </div>
        <script type="text/javascript">
            $(function() {
                $(".player-course-graph-<?= $row->id_path ?>").knob({
                    'min': 0,
                    'max': 100,
                    'readOnly': true,
                    'fgColor': '<?= ($data['percentage'] == 0 ? '#E4E6E5' : '#53AB53'); ?>',
                    'bgColor': '<?= ($data['percentage'] == 100 ? '#53AB53' : '#E4E6E5'); ?>',
                    'width': 66,
                    'height': 66,
                    'thickness': .25,
                    'value': 0
                });
            });
        </script>
	</div>
	<div class="curricula-header" style="height:auto; margin-bottom:10px;">
		<?php if($certificateTemplate && $certificateTemplate->id_certificate != 0): ?>
			<div class="certificate-box" id="certificate-box_<?php echo $row->id_path; ?>">
				<div class="pull-left">
					<i class="i home-ico certificate"></i>
				</div>
				<?php if($generatedCertificate): ?>
					<a class="dl-certificate" href="<?=Docebo::createLmsUrl('curricula/show/downloadCertificate', array('id_path' => $row->id_path))?>" target="_blank">
						<?=Yii::t('certificate', '_TAKE_A_COPY')?>
					</a>
				<?php else: ?>
					<span><?= Yii::t('player', 'A certificate will be made available to the user after learning plan completion') ?></span>
				<?php endif; ?>
			</div>
		<?php endif; ?>
		<h3><?php echo $row->path_name ?></h3>
		<div class="curricula-description">
			<?php echo $row->path_descr ?>
		</div>
        <?php if ($row->deep_link) : ?>
            <!-- Link format: http(s)://<my LMS DOMAIN>/lms/index.php?r=coursepath/deeplink&id_path=<ID LEARNING PLAN>&generated_by=<USER ID THAT GENERATED THE LINK>&hash=<DIGITAL SIGNATURE>  -->
            <?php $deepLink = $row->getDeeplink(); ?>
            <div class="row sharable-link-container">
                <div class="span6">
                    <span style="background-color: black; border: 1px solid black; border-radius: 3px; width: 20px; height: 20px">
                            <i style="color: white; margin-left: 4px; margin-right: 2px;" class="fa fa-link "></i>
                    </span>
                        <span id="deep-link-<?=$row->id_path?>" data-id="<?= $row->id_path ?>" style="display: inline; margin-left: 9px; text-decoration: underline;">
                            <a style="color: #0465ac; cursor: pointer"><?=Yii::t('course', 'Get sharable link');?></a>
                    </span>
                    <span class="deep-link-container-<?=$row->id_path?> hidden">
                    <input readonly="readonly" style="margin-left: 15px;height: 28px; width: 84%" id="copy-learning-plan-<?=$row->id_path?>" type="text" class="form-control" value="<?=$deepLink?>"/>
                    <input id="copy-button-plan-<?=$row->id_path?>" data-clipboard-target="#copy-learning-plan-<?=$row->id_path?>" style="min-width: 30px!important; ;height: 30px;" class="btn btn-docebo black big tooltip" type="button" value="<?php echo Yii::t('coursepath', 'Copy'); ?>"/>
                </span>
                </div>
            </div>

        <?php endif; ?>
		<?php
                $extra_output = '';
                $coursepathUser = LearningCoursepathUser::model()->findAllByAttributes(array('id_path' => $row->id_path, 'idUser' => Yii::app()->user->id));
                if(!empty($coursepathUser) && is_array($coursepathUser) && !empty($coursepathUser[0])) { $coursepathUser = $coursepathUser[0]; }
                Yii::app()->event->raise('LearningPlanBeforeCourseActivityRender',
                    new DEvent($this,
                        array('coursepathCourses'   =>  $courses,
                            'coursepath'            =>  $row,
                            'coursepathUser'        =>  $coursepathUser,
                            'extra_output'          => &$extra_output
                        )));

                if (!empty($extra_output)) {
                    echo  $extra_output;
                } else {
                    echo '<p>&nbsp</p>'; //to prevent showing a dash over the curricula progress
                }
        ?>
    </div>
    <div class="curricula-details">
        <?php if ($courses) : ?>
        <div class="dropdown">
            <div class="dropdown-row header <?=$currentCurricula && $currentCurricula!=$row->id_path ? 'dropdown-close' : ''?>">
                <div class="dropdown-context" style="height:auto;">
                    <strong><?= Yii::t('coursepath', '_CURRENT_ACTIVITY') ?>:</strong>
                    <?php
                        foreach ($courses as $course) {
                            $locked = $course->isLocked();
							$enrollment =  LearningCourseuser::model()->findByAttributes(array('idCourse' => $course->id_item, 'idUser' => Yii::app()->user->id));
							 if($enrollment)
								$canEnterCourse = $enrollment->canEnterCourse();
							else
								$canEnterCourse = array('can' => false);
							$locked = $course->isLocked() || !$canEnterCourse['can'];
							if ($enrollment && $enrollment->status != LearningCourseuser::$COURSE_USER_END && !$locked) {
                                echo $course->learningCourse->name;
                                break;
                            }
                        }
                    ?>
                    <span class="icon"></span>
                </div>
            </div>
			<?php $hasCoursesToBuy = false; ?>
            <?php foreach ($courses as $course) : ?>
				<?php
				$courseType = '';
				switch ($course->learningCourse->course_type) {
					case LearningCourse::TYPE_ELEARNING:
						$courseType = 'E-Learning';
						break;
					case LearningCourse::TYPE_CLASSROOM:
						$courseType = 'Classroom';
						break;
					case LearningCourse::TYPE_MOBILE:
						$courseType = 'Mobile';
						break;
					default:
						$courseType = ucfirst($course->learningCourse->course_type);
						break;
				}

				$isFailed = false;
				$catchup = null;
				$catchupEnrollment = null;
				$isFailed = $course->learningCourse->isCourseFailedByUser(Yii::app()->user->id);

	            $eventFailed = new DEvent($this, array (
		            'isFailed' => &$isFailed,
		            'idCourse' => $course->learningCourse->idCourse,
		            'idUser' => Yii::app()->user->id
	            ));
	            Yii::app()->event->raise('OnUserFailedEvaluation', $eventFailed);

				$enrollment =  LearningCourseuser::model()->findByAttributes(array('idCourse' => $course->id_item, 'idUser' => Yii::app()->user->id));
				$canHaveCatchup = $course->learningCourse->isCatchupType();
				if($enrollment)
					$canEnterCourse = $enrollment->canEnterCourse();
				else
					$canEnterCourse = array('can' => false);
				$locked = $course->isLocked() || !$canEnterCourse['can'];

				if ($canHaveCatchup) {
					$catchup = LearningCourseCatchUp::loadAssignedCatchup($row->id_path, $course->learningCourse->idCourse);
					if ($catchup) {
						$catchupEnrollment = LearningCourseuser::model()->findByAttributes(array('idCourse' => $catchup->id_catch_up, 'idUser' => Yii::app()->user->id));
                        if(!$catchupEnrollment)
							LearningCourseuser::subscribeUsersToCourses(array(Yii::app()->user->id), array($catchup->id_catch_up));
					}
				}

                $started = false; // The person is not started the course
                if ($enrollment["date_first_access"] != NULL ) // If the person access the course date_first_access is not empty
                    $started = true; // The person has played the course once
	            ?>
	            <div class="dropdown-row drop" <?=$currentCurricula && $currentCurricula!=$row->id_path ? ' style="display:none;" ' : ''?>>
	                <div class="dropdown-context">
							<?php
								// Only show course price and selection checkbox if the LP is for sale and the user has not purchased this course already
								if($row->is_selling && (!$enrollment || $enrollment->status==LearningCourseuser::$COURSE_USER_WAITING_LIST)):
									$hasCoursesToBuy = true;
									if($row->purchase_type == LearningCoursepath::PURCHASE_TYPE_SINGLE_COURSES_IN_SEQUENCE) { ?>
								<div class="curricula-course-list-buy">
									<?=CHtml::checkBox('', false, array(
										'class'=>'course-check-to-buy sequence-'.$course->sequence,
										'data-idCourse'=>$course->learningCourse->idCourse,
										'data-price'=>$course->learningCourse->getCoursePrice(),
										'data-sequence'=>$course->sequence,
										'data-idPath'=>$row->id_path
									))?>
								</div>
								<?php } else { ?>
								<div class="curricula-course-list-icon">
									<i class="i-sprite is-lock black" title="<?= CHtml::encode(Yii::t('coursepath', 'Users must buy the full learning plan')) ?>" rel="tooltip"></i>
								</div>
								<?php } ?>
							<?php else: ?>
								<div class="curricula-course-list-icon">
								<?php if ($canHaveCatchup && ($enrollment->status != LearningCourseuser::$COURSE_USER_END) && $isFailed) : ?>
									<?php
										if(!$locked)
											echo '<a href="'.Yii::app()->createUrl('player', array('course_id' => $course->learningCourse->idCourse, 'coming_from' => 'lp', 'id_plan' => $course->id_path)).'">';
									?>
										<div class="p-sprite circle-arrow-small-red"></div>
									<?php
										if(!$locked)
											echo '</a>';
									?>
								<?php else : ?>
									<?php if ($enrollment && $enrollment->status == LearningCourseuser::$COURSE_USER_END) : ?>
										<?php
											if(!$locked)
												echo '<a href="'.Yii::app()->createUrl('player', array('course_id' => $course->learningCourse->idCourse, 'coming_from' => 'lp', 'id_plan' => $course->id_path)).'">';
										?>
											<div class="p-sprite circle-check-small-green"></div>
										<?php
											if(!$locked)
												echo '</a>';
										?>
									<?php elseif($locked): ?>

									<?php
										$title = '';

										if($enrollment) {
											$title = $enrollment->cantEnterCourseMessage();
										}
										$prerequisiteMessage = $course->getPrerequisiteMessage($enrollment);
										if($prerequisiteMessage) {
											$title = $prerequisiteMessage;
										}
									?>
									<i class="i-sprite is-lock black" title="<?= CHtml::encode($title) ?>" rel="tooltip" data-html="true"></i>
									   <!-- If the learner has not started the course, it's not locked for him and he is enrolled (grey arrow) -->
									<?php elseif(!$started && !$locked && $enrollment):?>
										<a href="<?= Yii::app()->createUrl('player', array('course_id' => $course->learningCourse->idCourse, 'coming_from' => 'lp', 'id_plan' => $course->id_path)); ?>">
											<i class="i-sprite is-play grey"></i>
										</a>
									<?php else: ?>
										<?php
											if(!$locked && $enrollment)
												echo '<a href="'.Yii::app()->createUrl('player', array('course_id' => $course->learningCourse->idCourse, 'coming_from' => 'lp', 'id_plan' => $course->id_path)).'">';
										?>
											<div class="p-sprite circle-arrow-small-orange"></div>
										<?php
											if(!$locked && $enrollment)
												echo '</a>';
										?>
									<?php endif; ?>
								<?php endif; ?>

								</div>
		                    <?php endif; ?>
							<div class="curricula-course-thumbnail">
								<?= CHtml::image($course->learningCourse->getCourseLogoUrl(CoreAsset::VARIANT_SMALL, true), $course->learningCourse->name, array()) ?>
							</div>
	                        <div class="curricula-course-list-title">
	                            <?php if ($locked && !$isFailed): ?>
	                            <?php echo $course->learningCourse->name ?>
	                            <?php else: ?>
	                            <a href="<?php echo Yii::app()->createUrl('player', array('course_id' => $course->learningCourse->idCourse, 'coming_from' => 'lp', 'id_plan' => $course->id_path)) ?>"><?php echo $course->learningCourse->name ?></a>
	                            <?php endif; ?>
	                        </div>
							<div class="curricula-course-list-type">
								<?php echo $courseType ?>
							</div>
	                </div>
					<?php
					$event = new DEvent($this, array (
						'showCatchup' => &$canHaveCatchup,
						'idCourse' => $course->learningCourse->idCourse,
						'idUser' => Yii::app()->user->id
					));
					Yii::app()->event->raise('onUserSessionEvaluationSubmit', $event);

					if ( $row->catchup_enabled && $catchup && $canHaveCatchup )
						$this->renderPartial('_catchupRow', array('catchup' => $catchup, 'catchupEnrollment' => $catchupEnrollment, 'pathModel' => $row, 'courseModel' => $course->learningCourse, 'isFailed' => $isFailed))
					?>
	            </div>
            <?php endforeach; ?>
        </div>
	        <?php if($hasCoursesToBuy): // Learning plan is for sale and we still have courses to buy
				// Calculate total LP price to show
		        $lpFullPackPrice = 0;
		        if($row->purchase_type == LearningCoursepath::PURCHASE_TYPE_FULL_PACK_ONLY) {
			        $lpFullPackPrice =($row->price > 0) ? $row->price : 0; // LP full pack price set, use it
				} else
			        $lpFullPackPrice = 0.0; // LP full pack price is the sum of all course prices
		    ?>
				<div class="price-row">
			        <?php if($row->purchase_type == LearningCoursepath::PURCHASE_TYPE_SINGLE_COURSES_IN_SEQUENCE): ?>
				        <div class="pull-left">
					        <?=Yii::t('coursepath', 'You have selected')?>
					        <b><?=Yii::t('coursepath', '<span class="count" id="course-counter-"'.$row->id_path.'>0</span> course')?></b>
				        </div>
		            <?php endif; ?>
					<div class="pull-right total-price-container">
						<?=Yii::t('billing', '_AMOUNT')?>:
						<span class="total-price">
							<?=$currencySymbol?>
							<span id="select-total-sum-<?= $row->id_path ?>" data-coursepath-count="<?=count($courses)?>" data-coursepath-price="<?=$row->getCoursepathPrice()?>" data-purchase-type="<?=$row->purchase_type?>">
								<?=number_format($lpFullPackPrice, 2, '.', ' ')?>
							</span>
						</span>
					</div>
			        <div class="clearfix"></div>
		        </div>
		        <br/>
		        <div class="pull-right">
			        <?=CHtml::button(Yii::t('order', "Buy"), array('class'=>'btn btn-docebo green buy-lp-button', 'id'=>'axBuyLPSelectionNow-'.$row->id_path, 'data-idPath' => $row->id_path))?>
		        </div>
			<?php endif; // end of if($hasCoursesToBuy): ?>
        <?php endif; // end of if($courses): ?>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        var copy_button = $('#copy-button-plan-<?=$row->id_path?>');
        var clipboard = new Clipboard('#copy-button-plan-<?=$row->id_path?>.btn');
        var options = {
            'title'   : '<?=Yii::t('course','Copied to Clipboard!');?>',
            'trigger' : 'click'
        };
        $("#deep-link-<?=$row->id_path?>").on('click', function(){
            $(this).hide();
            $("span.deep-link-container-<?=$row->id_path?>").removeClass('hidden');
        });
        $("#copy-button-plan-<?=$row->id_path?>").on('click', function(){
            /* Copy To Clipboard */
            clipboard.on('success', function(e) {
                Docebo.log(e);
                copy_button.tooltip(options);
                copy_button.tooltip('show');

                copy_button.on('mouseleave', function() {
                    copy_button.tooltip('hide');
                });
                e.clearSelection();
            });
        });
    });

</script>