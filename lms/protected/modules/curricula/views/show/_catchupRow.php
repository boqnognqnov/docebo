<?php
/* @var $pathModel LearningCoursepath */

$reachedLimit = false; // is the catch-up limit reached
$playedCatchups = $pathModel->countPlayedCatchupCourses(Yii::app()->user->id);
$learningCoursepathUser = LearningCoursepathUser::model()->findByAttributes(array(
	'id_path' => $pathModel->id_path,
	'idUser' => Yii::app()->user->id,
));

if($catchupEnrollment)
	$canEnterCourse = $catchupEnrollment->canEnterCourse();
else
	$canEnterCourse = array('can' => false);

$locked = !$canEnterCourse['can'];

if ( $isFailed && $catchupEnrollment && $catchupEnrollment->status != LearningCourseuser::$COURSE_USER_END): ?>
<div class="catch-up-warning">
	<div class="pull-left">
		<i class="fa fa-exclamation-circle"></i>
	</div>
    <div class="pull-left">
    <span>
        <?=Yii::t('coursepath', 'You <b>didn\'t pass</b> the classroom course: <b>{course_name}</b>', array('{course_name}' => $courseModel->name))?></span><br>
    <?php if ( ($pathModel->catchup_limit && $pathModel->catchup_limit <= $playedCatchups && $learningCoursepathUser->catchup_user_limit < 1 ) ) : $reachedLimit = true; ?>
            <span>
                <?=Yii::t('coursepath', 'You can\'t proceed with this learning plan because you reached the maximum limit of '. $pathModel->catchup_limit.' catch-up sessions. Please <u> contact your instructor</u>')?></span>
        </div></div>
        <?php elseif ( $learningCoursepathUser->catchup_user_limit && $learningCoursepathUser->catchup_user_limit <= $playedCatchups ) : $reachedLimit = true; ?>
        <span><?=Yii::t('coursepath', 'You can\'t proceed with this learning plan because you reached the maximum limit of '. $learningCoursepathUser->catchup_user_limit.' catch-up sessions. Please <u> contact your instructor</u>')?></span></div></div>
    <?php else: ?>
		<span><?=Yii::t('coursepath', 'In order to proceed with this learning plan, <b>you must complete the following catch-up course</b>')?></span>
	</div>
</div>
<?php endif; ?>
<?php endif; ?>

<?php if ( !$reachedLimit && ($isFailed ||  $catchupEnrollment->status == LearningCourseuser::$COURSE_USER_END) ): ?>

<div class="catch-up-course-to-play">
	<div class="pull-left">
		<i class="fa fa-life-buoy"></i>
	</div>
	<div class="pull-left catchup-course-logo">
		<?= CHtml::image($catchup->catchUpCourseModel->getCourseLogoUrl(CoreAsset::VARIANT_SMALL, true), $catchup->catchUpCourseModel->name, array()) ?>
	</div>
	<div class="pull-left catchup-course-info">
		<?php if(!$locked) : ?>
			<a href="<?=Docebo::createLmsUrl('player', array('course_id' => $catchup->catchUpCourseModel->idCourse, 'coming_from' => 'lp', 'id_plan' => $pathModel->id_path))?>">
		<?php endif; ?>
			<b><?=$catchup->catchUpCourseModel->name?></b>
		<?php if(!$locked) : ?>
			</a>
		<?php endif; ?>
		<br>
		<span><?=$catchup->catchUpCourseModel->getCourseTypeTranslation()?> <?=Yii::t('coursepath', 'Catch-up course')?></span>
	</div>
	<div class="pull-right catchup-course-play-icon">
		<div class="curricula-course-list-icon">
			<?php if ($catchupEnrollment && $catchupEnrollment->status == LearningCourseuser::$COURSE_USER_END) : ?>
				<?php if(!$locked) : ?>
					<a href="<?=Docebo::createLmsUrl('player', array('course_id' => $catchup->catchUpCourseModel->idCourse, 'coming_from' => 'lp', 'id_plan' => $pathModel->id_path))?>">
				<?php endif; ?>
					<div class="p-sprite circle-check-small-green"></div>
				<?php if(!$locked) : ?>
					</a>
				<?php endif; ?>
			<?php elseif($catchupEnrollment && $catchupEnrollment->date_first_access != null):?>
				<?php if(!$locked) : ?>
					<a href="<?=Docebo::createLmsUrl('player', array('course_id' => $catchup->catchUpCourseModel->idCourse, 'coming_from' => 'lp', 'id_plan' => $pathModel->id_path))?>">
				<?php endif; ?>
					<div class="p-sprite circle-arrow-small-orange"></div>
				<?php if(!$locked) : ?>
					</a>
				<?php endif; ?>
			<?php else: ?>
				<?php if(!$locked) : ?>
					<a href="<?=Docebo::createLmsUrl('player', array('course_id' => $catchup->catchUpCourseModel->idCourse, 'coming_from' => 'lp', 'id_plan' => $pathModel->id_path))?>">
				<?php endif; ?>
					<div class="p-sprite circle-arrow-small-grey"></div>
				<?php if(!$locked) : ?>
					</a>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php endif; ?>