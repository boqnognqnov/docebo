<ul class="curricula-list">
    <?php foreach ($rows as $row) : ?>
        <?php if ($currentCurricula == $row->id_path) : ?>
            <li>
                <?php $this->renderPartial('_row', array('row' => $row, 'currentCurricula'=>$currentCurricula)) ?>
            </li>
     <?php break; ?>
        <?php endif; ?>
        <?php if (!$currentCurricula) : ?>
            <li>
                <?php $this->renderPartial('_row', array('row' => $row, 'currentCurricula'=>$currentCurricula)) ?>
            </li>
        <?php endif; ?>
    <?php endforeach; ?>
</ul>

<?php if(!empty($rows)): ?>
	<script type="text/javascript">
		$(function(){

			// Style each checkbox
			$('.course-check-to-buy').styler();

			// Handler for buy course button
			$('.buy-lp-button').click(function(){
				var idPath = $(this).data('idpath');
				if(!idPath)
					return;
				var selectedCheckboxes = $('#curricula-'+idPath+' .course-check-to-buy:checked');
				var coursesArr = [];
				selectedCheckboxes.each(function(){
					coursesArr[coursesArr.length] = $(this).data('idcourse');
				});

				$.ajax('<?=Docebo::createLmsUrl('coursepath/buy')?>', {
					data: {'courses': coursesArr, 'id': idPath},
					cache: false,
					success: function(data, status, xhr){
						if(data.success){
							// Redirect to Cart
							window.location.replace("<?=Docebo::createLmsUrl('cart/index')?>");
						}
					},
					dataType: 'json',
					type: 'POST'
				});
			});

			// Install change handler to recalculate LP total price
			$('.course-check-to-buy').change(function() {
				var idPath = $(this).data('idpath');
				if(!idPath)
					return;

				updateTotalSelectedCoursesCount(idPath);
				updateTotalPrice(idPath);
			});

			// Updates the total selected courses "counter"
			var updateTotalSelectedCoursesCount = function(idPath) {
				var count = $('#curricula-'+idPath+' .course-check-to-buy:checked').length;
				$('#curricula-'+idPath+' .price-row .count').html(count);
			};

			// Updates the total price
			var updateTotalPrice = function(idPath) {
				var purchaseType = $('#select-total-sum-'+idPath).data('purchase-type');
				var wholeLpPrice = $('#select-total-sum-'+idPath).data('coursepath-price');
				var calculatedPrice = 0;

				if(purchaseType == <?=LearningCoursepath::PURCHASE_TYPE_SINGLE_COURSES_IN_SEQUENCE?>) {
					//if the checked inputs matches the all input count AND total count of courses is the same - only then show the total
					//else.. just sum of the selected
					if(wholeLpPrice > 0 && ($('#curricula-'+idPath+' input.course-check-to-buy:checked').length == $('#curricula-'+idPath+' input.course-check-to-buy').length) && $('#select-total-sum-'+idPath).data('coursepath-count') == $('#curricula-'+idPath+' input.course-check-to-buy').length)
						calculatedPrice = wholeLpPrice;
					else {
						$('#curricula-'+idPath+' input.course-check-to-buy:checked').each(function(){
							var price = parseFloat($(this).data('price'));
							calculatedPrice = calculatedPrice + (!isNaN(price) ? price : 0);
						});
					}

					$('#select-total-sum-'+idPath).html(calculatedPrice.toFixed(2));
				}
			}
		});
	</script>
<?php endif; ?>