<?php
/**
 *
 * Module specific base controller
 *
 */
class CurriculaBaseController extends LoBaseController {

	public $userCanAdminCourse = false;
	public $userIsSubscribed = false;
	public $courseModel = null;

	/**
	 * (non-PHPdoc)
	 * @see CController::init()
	 */
	public function init()
	{
		parent::init();

		$this->registerResources();
	}

	/**
	 * (non-PHPdoc)
	 * Method invoked before any action
	 * @param CAction $action
	 * @return bool
	 */
	public function beforeAction($action)
	{
		return parent::beforeAction($action);
	}



	/**
	 * Register module specific assets
	 *
	 * (non-PHPdoc)
	 * @see LoBaseController::registerResources()
	 */
	public function registerResources() {

		parent::registerResources();

		if (!Yii::app()->request->isAjaxRequest) {
			$cs = Yii::app()->getClientScript();
			$assetsUrl = $this->module->assetsUrl;
			$cs->registerCssFile($assetsUrl.'/css/main.css');
			$cs->registerCssFile(Yii::app()->theme->baseUrl . '/js/formstyler/jquery.formstyler.css');
            $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/admin.css');

			$cs->registerScriptFile($this->getPlayerAssetsUrl() . '/js/jquery.knob.js');
			$cs->registerScriptFile($assetsUrl . '/js/main.js');
            $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/formstyler/jquery.formstyler.js');
		}
	}


}