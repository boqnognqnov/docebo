<?php

/**
 * User interface for listing curiculas
 *
 */
class ShowController extends CurriculaBaseController {

	/**
	 * (non-PHPdoc)
	 * @see CController::init()
	 */
	public function init() {
		parent::init();
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl',
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see PlayerBaseController::registerResources()
	 */
	public function registerResources()
	{
		parent::registerResources();

		if (!Yii::app()->request->isAjaxRequest)
		{
			Yii::app()->clientScript->registerScriptFile(Yii::app()->findModule('player')->assetsUrl.'/js/excanvas.js');
		}

	}

	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		return array(

			array('allow',
				'users' => array('@'),
			),

			array('deny',
				'users'          => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
				'message' => Yii::t('course', '_NOENTER'),
			),
		);
	}

	/**
	 * Index action
	 */
	public function actionIndex() {
        /* REGISTER CLIPBOARD */
        $cs = Yii::app()->getClientScript();
        $themeUrl = Yii::app()->theme->baseUrl;
        $cs->registerScriptFile($themeUrl . '/js/clipboard.min.js');
        /* END REGISTER CLIPBOARD */

		if (Yii::app()->request->isAjaxRequest) {
			$status = isset($_REQUEST['status']) ? $_REQUEST['status'] : 'all';
			$q      = isset($_REQUEST['q']) ? $_REQUEST['q'] : NULL;

			$rows = $this->getList($status, $q);

			$content = $this->renderPartial('_curricula-list', array(
				'rows' => $rows,
			), TRUE);

			$result = array(
				'status'  => 'success',
				'content' => $content,
			);

			$this->sendJSON($result);
			Yii::app()->end();
		}

		$rows = $this->getList();

		// Certification Plugin Related: RESET
		if(PluginManager::isPluginActive('CertificationApp')) {
			// If we find a reset token in the request, check if the token is still available (it is for ONE TIME usage)
			// If it is, reset course+user information (tracking and everything else) for ALL courses in this plan
			// Then remove the token from plugin table(s)
			// Also, set the "in_renew" mode to true (1)
			$resetToken = Yii::app()->request->getParam("reset", false);
			if($resetToken) {
				$certUser = CertificationUser::model()->findByAttributes(array('reset_token' => $resetToken));
				if ($certUser) {
					$certItem = $certUser->certificationItem;
					if ($certItem && $certItem->item_type == CertificationItem::TYPE_LEARNING_PLAN) {
						LearningCoursepath::reset($certItem->id_item, Yii::app()->user->id);
					}
					$certUser->in_renew = 1;
					$certUser->reset_token = null;
					$certUser->save();
				}
			}

		}


        $id_path = Yii::app()->request->getParam('id_path', false);
        if ($id_path) $currentCurriculaModel = LearningCoursepath::model()->findByPk($id_path);
        $this->render('index', array(
            'rows' => $rows,
            'currentCurricula'=>isset($id_path) && $id_path ? $id_path : false,
            'currentCurriculaModel' => $currentCurriculaModel
        ));
	}

	/**
	 * Get list of curiculas based on the provided criteria (status and search keyword)
	 *
	 * @param string $status - 'all' (default), 'in_progress', 'completed'
	 * @param string $q - a search query (searches in the curricula's title or description)
	 *
	 * @return array of LearningCoursepath models
	 */
	protected function getList($status = 'all', $q = NULL) {

		$criteria                    = new CDbCriteria();
		$criteria->with['coreUsers'] = array('joinType' => 'INNER JOIN');
		$criteria->with['certificate'] = array('joinType' => 'LEFT JOIN');
		$criteria->addCondition('coreUsers_coreUsers.idUser = :idUser');
		$criteria->params[':idUser']   = Yii::app()->user->id;

		if ($q) {
			$criteria->addCondition('(t.path_name LIKE :search OR t.path_descr LIKE :search)');
			$criteria->params[':search'] = "%{$q}%";
		}

		$criteria->order = 't.path_name ASC';
		$criteria->group = 't.id_path';
		$endResult            = LearningCoursepath::model()->with('coursesCount')->findAll($criteria);

		if ($status == 'in_progress' || $status == 'completed') {
			$criteria                             = new CDbCriteria();
			$criteria->select                     = 't.id_path, COUNT(t.id_item) as courses';
			$criteria->with['learningCourseuser'] = array(
				'joinType' => 'INNER JOIN',
			);
			$criteria->condition                  = 'learningCourseuser.status = :status AND learningCourseuser.idUser = :idUser';
			$criteria->params[':status']          = LearningCourseuser::$COURSE_USER_END;
			$criteria->params[':idUser']          = Yii::app()->user->id;
			$criteria->group                      = 't.id_path';
			$criteria->distinct                   = FALSE;

			$completed = LearningCoursepathCourses::model()->findAll($criteria);

			$completedCurriculas = array();
			foreach ($completed as $_row) {
				foreach ($endResult as $row) {
					// completed
					if ($row->id_path == $_row->id_path) {

						if($row->coursesCount == (int) $_row->courses) {
							// This curricula is completed. The number of courses in completed
							// status by the user is equal to the number of courses in the curricula
							$completedCurriculas[$row->id_path] = $row;
						}
					}
				}
			}

			if ($status == 'in_progress') {
				// Exclude the "completed" Curriculas from the full list of
				// Curriculas available to the user. The result should be
				// Curriculas "in progress"
				foreach ($endResult as $x => $row) {
					if(!empty($completedCurriculas) && in_array($row->id_path, array_keys($completedCurriculas))){
						// The currently looped Curricula is in the "completed" curriculas list
						// so we better exclude it from the end result array, since the user has
						// requested just "In progress" curriculas
						unset($endResult[$x]);
					}
				}
			} elseif($status == 'completed'){
				// Limit the end result to just those Curriculas that are fully completed
				$endResult = $completedCurriculas;
			}

			// not needed anymore
			unset($completedCurriculas);
		}

		return $endResult;
	}

	/**
	 * Download certificate file for this learning plan & user
	 */
	public function actionDownloadCertificate() {
		$id_path = Yii::app()->request->getParam("id_path", FALSE);
		$id_user = Yii::app()->request->getParam("id_user", Yii::app()->user->id);
		if (!$id_path)
			throw new CHttpException("Missing id path param");
		else {
			$certificate = LearningCertificateCoursepath::model()->findByAttributes(array('id_path' => $id_path));
			if (!$certificate)
				throw new CHttpException("No certificate to download");

			$model = LearningCertificateAssignCp::loadByUserPath($id_user, $id_path, $certificate->id_certificate);
			if (!$model)
				throw new CHttpException("No certificate available for this user");

			$model->downloadCertificate();
		}
	}

}