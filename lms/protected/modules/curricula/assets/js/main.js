function dropdownExpandAll() {
    $('.dropdown .drop').show();
    $('.dropdown .dropdown-row.header').removeClass('dropdown-close');
}

function dropdownCollapseAll() {
    $('.dropdown .drop').hide();
    $('.dropdown .dropdown-row.header').addClass('dropdown-close');
}

function dropdownClick(node) {
    if(node.hasClass('dropdown-close')) {
        node.parent().find('.drop').show();
        node.removeClass('dropdown-close');
    } else {
        node.parent().find('.drop').hide();
        node.addClass('dropdown-close');
    }
}

var curriculaFiltersStatus = 'all';
var curriculaFiltersQ = '';

function curriculaFilters(excluded_text) {
    var status = $('.radio-filter input:checked').val();
    var q = $('#advanced-search-curricula').val();

    if ((status != curriculaFiltersStatus) || (q != curriculaFiltersQ && q != excluded_text)) {
        curriculaFiltersStatus = status;
	   if(q == excluded_text)
	   	q = '';
        curriculaFiltersQ = q;

        $.ajax({
            url: $('#curricula-management-form').attr('action'),
            data: {
                status: status,
                q: q
            },
            success: function(result){
                $('.curricula-list').replaceWith($(result.content));
            }
        });
    }
}