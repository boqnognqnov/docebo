<?php
/**
 * 
 * Module specific base controller
 * 
 * Extending PlayerBaseController inherits all the goodies of it: permissions prechesk, course model, etc.
 * This makes this controller firmly tied to Player Context!!
 *  
 * If that's not ok, it should extend FileBaseController instead!
 * 
 */
class FileBaseController extends PlayerBaseController
{
	
    /**
     * (non-PHPdoc)
     * @see CController::init()
     */
    public function init()
	{
		return parent::init();
	}
	
	/**
	 * (non-PHPdoc)
	 * Method invoked before any action
	 * @param CAction $action
	 * @return bool
	 */
	public function beforeAction($action)
	{
		return parent::beforeAction($action);
	}

	

	/**
	 * Register module specific assets
	 * 
	 * (non-PHPdoc)
	 * @see LoBaseController::registerResources()
	 */
	public function registerResources() {
		parent::registerResources();
		
		if (!Yii::app()->request->isAjaxRequest) {
			$cs = Yii::app()->getClientScript();
			
		}
	}
			
}