<div class="container">
	<div class="row-fluid player-launchpad-header">
		<div class="span12">
			<h2><?= $model->title ?></h2>
			<?php
			if (PluginManager::isPluginActive('Share7020App')) {
				$this->widget('common.widgets.CourseLoHeaderApp7020', array('loID' => $idReference));
			}
			?>
			<a id="inline-player-close" class="player-close" href="#"><?= Yii::t('standard', '_CLOSE') ?> </a>
		</div>
	</div>
	<div class="player-arena-file-inline">
		<div class="row-fluid">
			<div class="span2 text-center">
				<span class="p-sprite file-extra-large"></span>
			</div>
			<div class="span10">
				<div class="player-launchpad-content-text">
					<h3 class="file-title">
						<?php echo $originalFilename; ?>
					</h3>
					<div class="file-description native-styled">
						<?php
						echo Yii::app()->htmlpurifier->purify($fileDescription); ?>
					</div>
					<div class="row-fluid">
						<div class="span6">
							<p class="file-type">
								<strong><?php echo ucfirst(Yii::t('item', '_MIME')); ?>:</strong>
								<span><?php echo $fileType; ?></span>
							</p>
							<p class="file-size">
								<strong><?php echo ucfirst(Yii::t('file', 'Size')); ?>:</strong>
								<span><?php echo $fileSize; ?></span>
							</p>
						</div>
						<div class="span6 text-right">
							<br/>
							<a id="file-download-<?php echo $idReference; ?>" href="<?php echo $downloadUrl; ?>" class="btn-docebo green big">
								<span class="i-sprite is-download white"></span> <?php echo Yii::t('standard', '_DOWNLOAD'); ?>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function(){
	$("#file-download-<?php echo $idReference; ?>").on("click", function(){
		// Arena.showQuickNav();
		// After click on the Download link,
		// give 5 secs to Download process to start and do the FILE COMPLETED Tracking (in the controller action)
		setTimeout(function(){
            if(typeof Arena.learningObjects !== "undefined") {
                Arena.learningObjects.ajaxUpdate();
            }
			if(typeof PlayerNavigator !== "undefined") {
				PlayerNavigator.init({ajaxUrl: '<?=Docebo::createLmsUrl('player/training/axGetPlayerNavigator', array('course_id' => $this->course_id))?>'});
				PlayerNavigator.setCurrentKey(<?=$idReference?>);
				PlayerNavigator.reload();
			}
		}, 5000);
	});
});
</script>