<?php
/* @var $fileObj LearningMaterialsLesson */
/* @var $form CActiveForm */
?>


<style>
<!--

-->
</style>



<h1><?php echo Yii::t('standard', '_MOD'); ?></h1>

<?php
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'edit-file-modal',
		'method' => 'post',
		'htmlOptions' => array(
			'class' => 'ajax'
		)
	));
?>
<form action="" class="ajax" method="POST">

	<?= CHtml::errorSummary($fileObj) ?>

	<div class="row-fluid">
		<div class="span2"><?= $form->labelEx($fileObj, 'title', array('label' => Yii::t('standard', '_TITLE'))); ?></div>
		<div class="span10">
			<?= $form->textField($fileObj, 'title', array('class'=>'span12 squared')) ?>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span2"><?= $form->labelEx($fileObj, 'description', array('label' => Yii::t('standard', '_DESCRIPTION'))); ?></div>
		<div class="span10">
			<?= $form->textArea($fileObj, 'description', array('class'=>'span12 squared', 'id' => 'lo-description')) ?>
		</div>
	</div>

	<div class="form-actions">
        <input class="btn-docebo green big" type="submit" value="<?= Yii::t('standard', '_SAVE'); ?>">
	</div>
<?php $this->endWidget(); ?>


<script type="text/javascript">
	$(function(){
	    TinyMce.attach('#lo-description',{height: 200});
	});
</script>