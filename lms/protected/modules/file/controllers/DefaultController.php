<?php

class DefaultController extends FileBaseController
{

	public $validLo = true;
	
	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
				'accessControl',
		);
	}
	
	
	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		return array(
	
	
				// Admins and Teachers can do all
				array('allow',
						'users'=>array('@'),
						'expression' => 'Yii::app()->user->getIsPu() || Yii::app()->controller->userCanAdminCourse && Yii::app()->controller->validLo',
				),
	
				// For Subscribers
				array('allow',
						'actions' => array('index', 'axLaunchpad', 'sendFile'),
						'users'=>array('@'),
						'expression' => 'Yii::app()->user->getIsPu() || Yii::app()->controller->userIsSubscribed && Yii::app()->controller->validLo',
				),
				
				array('deny', // if some permission is wrong -> the script goes here
						'users' => array('*'),
						'deniedCallback' => array($this, 'accessDeniedCallback'),
						'message' => Yii::t('course', '_NOENTER'),
				),
		);
	
	}
	
	
	
	/**
	 * (non-PHPdoc)
	 * @see CController::init()
	 */
	public function init()
	{
		parent::init();
		
		// Lets check if someone is trying to access File + Course combination and if it is valid
		// It does NOT matter what the reason is! We just check if request FILE+LO is a valid combination
		$this->validLo = true;
		$idFile = intval( Yii::app()->request->getParam('id_file') );
		
		if ($idFile && $this->getIdCourse()) {
			$object = LearningOrganization::model()->findByAttributes(array(
					'idResource' => $idFile,
					'objectType' => LearningOrganization::OBJECT_TYPE_FILE,
					'idCourse' => $this->getIdCourse(),
			));
			if (!$object) {
				$this->validLo = false;
			}		
		}
		
	}
	
	
	
	
	//these format function should be moved into an appropriate CFormatter class

	protected function formatFileSize($size) {
		if ($size >= 1099511627776) {
			$bytes = number_format($size / 1099511627776, 2).' TB';
		} elseif ($size >= 1073741824) {
			$bytes = number_format($size / 1073741824, 2).' GB';
		} elseif ($size >= 1048576) {
			$bytes = number_format($size / 1048576, 2).' MB';
		} elseif ($size >= 1024) {
			$bytes = number_format($size / 1024, 2).' KB';
		} elseif ($size > 1) {
			$bytes = $size.' bytes';
		} elseif ($size == 1) {
			$bytes = $size.' byte';
		} else {
			$bytes = '0 bytes';
		}
		return $bytes;
	}

	protected function formatFileType($name) {
		$extension = strtolower(FileHelper::getExtension($name));
		switch ($extension) {

			//documents
			case 'pdf': { $type ='PDF'; } break;
			case 'rtf': { $type ='RTF'; } break;

			case 'doc':
			case 'docx': { $type ='Word'; } break;
			case 'xls':
			case 'xlsb':
			case 'xlsx': { $type ='Excel'; } break;
			case 'ppt':
			case 'pptx': { $type ='Powerpoint'; } break;

			case 'sxw':
			case 'odt': { $type ='Openoffice document'; } break;
			case 'sxc':
			case 'ods': { $type ='Openoffice spreadsheet'; } break;
			case 'sxi':
			case 'opd': { $type ='Openoffice presentation'; } break;

			case 'txt': { $type ='TXT'; } break;
			case 'csv': { $type ='CSV'; } break;

			case 'htm':
			case 'html': { $type ='HTML'; } break;

			//images
			case 'tif':
			case 'tiff': { $type ='TIFF'; } break;
			case 'jpeg':
			case 'jpg':
			case 'jif':
			case 'jfif': { $type ='JPEG'; } break;

			case 'jp2':
			case 'jpx':
			case 'j2k':
			case 'j2c': { $type ='JPEG 2000'; } break;

			case 'fpx': { $type ='FPX'; } break; //Flashpix
			case 'pcd': { $type ='PCD'; } break; //ImagePac, Photo CD
			case 'gif': { $type ='GIF'; } break;
			case 'png': { $type ='PNG'; } break;

			//archives
			case 'zip': { $type ='ZIP'; } break;
			case 'rar': { $type ='RAR'; } break;
			case 'tar': { $type ='TAR'; } break;
			case '7z': { $type ='7zip'; } break;
			//...
			default: { $type = $extension; } break;
		}
		return $type;
	}



	//actions

	public function actionIndex()
	{
		$this->redirect($this->createUrl('/site'), true);
	}



	/**
	 * Save uploaded LO. Caller must already saved the file into
	 * <root>/files/upload_tmp folder.
	 *
	 */
	public function actionAxSaveLo()
	{
		$idOrg = Yii::app()->request->getParam("id_org");
		if (intval($idOrg) > 0) {
			// edit operation
			$this->forward('default/axEditLo');
		}

		$courseId = Yii::app()->request->getParam("course_id", null);
		$parentId = Yii::app()->request->getParam("parent_id", null);
		$file = Yii::app()->request->getParam("file", null);
		$title = urldecode(trim(Yii::app()->request->getParam("title", null)));
		$description = urldecode(Yii::app()->request->getParam("description", null));
		$short_description = urldecode(Yii::app()->request->getParam("short_description", ''));
		
		if (is_array($file) && isset($file['name'])) {
			$originalFileName = urldecode($file['name']);
			if (isset($file['target_name'])) {
				$file['name'] = $file['target_name'];
			}
			$file['name'] = urldecode($file['name']);
			$thumb = urldecode(Yii::app()->request->getParam("thumb", null));
		}
		
		$response = new AjaxResult();
		$transaction = Yii::app()->db->beginTransaction();

		try {
			// Validate input parameters
			if (!$courseId) { throw new CException("Invalid specified course."); }
			if (!is_numeric($parentId) || $parentId < 0) { $parentId = 0; }
                        $title = trim($title);
			if (empty($title) || count($title) <= 0) { throw new CException(Yii::t('error', 'Enter a title!')); }

			// Validate file input
			if (!is_array($file)) { throw new CException("Invalid file object."); }
			if (!isset($file['name']) || ($file['name'] == '')) { throw new CException("Invalid file name."); }
			if (!isset($file['percent']) || $file['percent'] < 100) { throw new CException("Incomplete file upload."); }

			// Check if uploaded file exists
			$uploadTmpFile = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR.$file['name'];
			if (!is_file($uploadTmpFile)) { throw new CException("File '" . $file['name'] . "' does not exist."); }

			/*********************************************************/

			/*** File extension type validate ************************/
			$fileValidator = new FileTypeValidator(FileTypeValidator::TYPE_ITEM);
			if (!$fileValidator->validateLocalFile($uploadTmpFile))
			{
				FileHelper::removeFile($uploadTmpFile);
				throw new CException($fileValidator->errorMsg);
			}
			/*********************************************************/

			// Rename uploaded file
			$extension = strtolower(CFileHelper::getExtension($file['name']));
			$newFileName = Docebo::randomHash() . "." . $extension;
			$newTmpFile = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR.$newFileName;

			if (!@rename($uploadTmpFile, $newTmpFile)) { throw new CException("Error renaming uploaded file."); }

			// Move uploaded file to final storage for later usage
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_FILE);
			if ( ! $storageManager->store($newTmpFile)){ throw new CException("Error while saving file to final storage."); }

			// Delete tmp file
			FileHelper::removeFile($newTmpFile);


			//save info in database
			$fileObj = new LearningMaterialsLesson();
			$fileObj->author = Yii::app()->user->getIdst();
			$fileObj->title = $title;
			$fileObj->description = $description;
			$fileObj->path = $newFileName;
			$fileObj->original_filename = $originalFileName;
			if ( ! $fileObj->save()) {
				throw new CException('Invalid file');
			}

			// Resource ID
			$resourceId = $fileObj->idLesson;

			// Now save info in learning_object table
			$loManager = new LearningObjectsManager();
			$loManager->setIdCourse($courseId);

			$loParams = array(
				'description' => $description,
				'short_description' => $short_description,
				'resource' => $thumb
			);

			$loModel = $loManager->addLearningObject(LearningOrganization::OBJECT_TYPE_FILE, $resourceId, $title, $parentId, $loParams);
			if (!$loModel) { throw new CException("Error while creating organization object."); }

			// Save storage type in location field
			$loModel->location = $storageManager->type;
			$loModel->saveNode();

			// DB Commit
			$transaction->commit();

			// Trigger event to let plugins save their own data from POST
			Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
				'loModel' => $loModel
			)));

			$data = array(
					'resourceId' => (int)$resourceId,
					'organizationId' => (int)$loModel->idOrg,
					'parentId' => (int)$parentId,
					'title' => $title,
					'message' => Yii::t('standard', '_OPERATION_SUCCESSFUL')
			);
			$response->setData($data)->setStatus(true);

		} catch (CException $e) {
			$transaction->rollback();
			$response->setMessage($e->getMessage())->setStatus(false);
		}

		$response->toJSON();
		Yii::app()->end();

	}


	/**
	 * Show/Edit File LO attributes
	 *
	 * @throws CException
	 */
	public function actionAxEditLo() {
		$courseId = Yii::app()->request->getParam("course_id", null);
		$parentId = Yii::app()->request->getParam("parent_id", null);
		$file = Yii::app()->request->getParam("file", null);

		// URLDecode
		$title = urldecode(trim(Yii::app()->request->getParam("title", null)));
		$description = urldecode(Yii::app()->request->getParam("description", null));
		$short_description = urldecode(Yii::app()->request->getParam("short_description", ''));
		
		if (is_array($file) && isset($file['name'])) {
			// Use the physically uploaded filename, put by PLUpload in the "target_name" element
			// This is tru when "unique_names" option is used for plupload JavaScript object
			$originalFileName = urldecode($file['name']);
			if (isset($file['target_name'])) {
				$file['name'] = $file['target_name'];
			}
			$file['name'] = urldecode($file['name']);
		}
		$thumb = urldecode(Yii::app()->request->getParam("thumb", null));
		
		
		$response = new AjaxResult();
		$transaction = Yii::app()->db->beginTransaction();

		try {
			$loModel = LearningOrganization::model()->findByPk(Yii::app()->request->getParam("id_org"));
			$fileObj = LearningMaterialsLesson::model()->findByPk($loModel->idResource);

			if (!$fileObj) { throw new CException('Invalid file'); }

			// Validate input parameters
			if (!$courseId) { throw new CException("Invalid specified course."); }
			if (!is_numeric($parentId) || $parentId < 0) { $parentId = 0; }
			if (empty($title)) { throw new CException(Yii::t('error', 'Enter a title!')); }

			// when editing, if the file wasn't change, we expect to receive 'file'==>false
			if ($file !== 'false') {
				// Validate file input
				if (!is_array($file)) { throw new CException("Invalid file object."); }
				if (!isset($file['name']) || ($file['name'] == '')) { throw new CException("Invalid file name."); }
				if (!isset($file['percent']) || $file['percent'] < 100) { throw new CException("Incomplete file upload."); }

				// Check if uploaded file exists
				$uploadTmpFile = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR.$file['name'];
				if (!is_file($uploadTmpFile)) { throw new CException("File '" . $file['name'] . "' does not exist."); }

                /*********************************************************/

                /*** File extension type validate ************************/
                $fileValidator = new FileTypeValidator(FileTypeValidator::TYPE_ITEM);
                if (!$fileValidator->validateLocalFile($uploadTmpFile))
                {
                    FileHelper::removeFile($uploadTmpFile);
                    throw new CException($fileValidator->errorMsg);
                }
                /*********************************************************/

                $oldFileName = $fileObj->path;

                // Rename uploaded file
                $extension = strtolower(CFileHelper::getExtension($file['name']));
                $newFileName = Docebo::randomHash() . "." . $extension;
                $newTmpFile = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR.$newFileName;

                if (!@rename($uploadTmpFile, $newTmpFile)) { throw new CException("Error renaming uploaded file."); }

				// Move uploaded file to final storage for later usage
				$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_FILE);
				if ( ! $storageManager->store($newTmpFile)){ throw new CException("Error while saving file to final storage."); }

				// Now remove the old file to keep the collection clean
				$occurencies = LearningMaterialsLesson::model()->count('path = :path', array(
					':path' => $oldFileName
				));
				if ($occurencies <= 1) {
					$storageManager->remove($oldFileName);
				}

				// update object with new file
				$fileObj->path = $newFileName;
                $fileObj->original_filename = $originalFileName;

				// Delete tmp file
				FileHelper::removeFile($newTmpFile);
			}

			// update info in database
			$fileObj->title = $title;
			$fileObj->description = $description;
			if ( ! $fileObj->save()) {
				throw new CException('Invalid file');
			}

			// update learning organization
			$loModel->title = $fileObj->title;
			$loModel->short_description = $short_description;

			if(intval($thumb) > 0){
				$loModel->resource = intval($thumb);
			}elseif($loModel->resource && !$thumb){
				$loModel->resource = 0;
			}
			$loModel->saveNode();

			// DB Commit
			$transaction->commit();

			// Trigger event to let plugins save their own data from POST
			Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
				'loModel' => $loModel
			)));

			$data = array(
				'organizationId' => (int)$loModel->idOrg,
				'parentId' => (int)$parentId,
				'title' => $title,
				'message' => Yii::t('standard', '_OPERATION_SUCCESSFUL')
			);
			$response->setData($data)->setStatus(true);

		} catch (CException $e) {
			$transaction->rollback();
			$response->setMessage($e->getMessage())->setStatus(false);
		}

		$response->toJSON();
		Yii::app()->end();
		return;


	}


	/**
	 * Loads the "launchpad" UI to present the LO to the user and allow downloading the file
	 *
	 * @throws CException
	 */
	public function actionAxLaunchpad() {

		$response = new AjaxResult();
		$transaction = Yii::app()->db->beginTransaction();

		try {

			//load LO info from learning_organization table to get idResource of the file
			$idObject = intval( Yii::app()->request->getParam('id_object') );
			$object = LearningOrganization::model()->findByPk($idObject);
			if (!$object || $object->objectType != LearningOrganization::OBJECT_TYPE_FILE) {
				throw new CException('Invalid object ID: '.$idObject);
			}

            //retreive info on the file itself
            $idReference = $object->getPrimaryKey();

            //now load file specific information
            $idFile = (int)$object->idResource;
            $file = LearningMaterialsLesson::model()->findByPk($idFile);
            if (!$file) {
                throw new CException('Invalid file ID: '.$idFile);
            }

			$idUser = Yii::app()->user->id;
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_FILE);
			$size = $storageManager->fileSize($file->path);

			// This is actually SednFile URL
			$downloadUrl = Docebo::createLmsUrl("//file/default/sendFile", array("id_file" => $file->idLesson, 'course_id' => $this->course_id));

			$html = $this->renderPartial("_launchpad", array(
				'fileName' => $file->path,
				'originalFilename' => empty($file->original_filename) ? $file->path : $file->original_filename,
				'fileSize' => $this->formatFileSize($size),
				'fileType' => $this->formatFileType($file->path),
				'fileDescription' => $file->description,
				'downloadUrl' => $downloadUrl,
				'idReference' => $idReference,
				'model'		=> $file,
			), true);

			// Adjust the object reference (in case this is a central repo LO)
			$masterLo = $object->getMasterForCentralLo($idUser);
			$track = LearningMaterialsTrack::model()->findByAttributes(array('idReference' => $masterLo->idOrg, 'idUser' => $idUser));
			if (!$track) {
				//the object has not been tracked yet for this user: create the record now
				$track = new LearningMaterialsTrack();
				$track->idUser = $idUser;
				$track->idReference = $masterLo->idOrg;
				$track->idResource = $masterLo->idResource;
				$track->setStatus(LearningCommontrack::STATUS_AB_INITIO);
				$track->setObjectType(LearningOrganization::OBJECT_TYPE_FILE);
				if (!$track->save()) {
					throw new CException("Error while saving track data");
				}
			}

			//finish action
			$transaction->commit();

			$response->setHtml($html);
			$response->setStatus(true);

		} catch(CException $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			$transaction->rollback();
			$response->setMessage($e->getMessage());
			$response->setStatus(false);
		}

		//send result
		$response->toJSON();
	}






	/**
	 * Send file to user. Only for files stored in local file system!
	 *
	 * @throws CException
	 */
	public function actionSendFile() {

		$response = new AjaxResult();
		$transaction = Yii::app()->db->beginTransaction();

		try {

			$idFile = (int)Yii::app()->request->getParam('id_file', 0);

			$file = LearningMaterialsLesson::model()->findByPk($idFile);
			if (!$file) {
				throw new CException('Invalid file ID: '.$idFile);
			}

			$criteria = new CDbCriteria();
			$criteria->addCondition('idResource=:idFile');
			$criteria->addCondition('objectType=:objType');
			$criteria->addCondition('idCourse=:idCourse');
			$criteria->params = array(
					':idCourse' => $this->getIdCourse(),
					':idFile' => $idFile,
					':objType' => LearningOrganization::OBJECT_TYPE_FILE,
			);
			$object = LearningOrganization::model()->find($criteria);

			if (!$object) {
				throw new CException('Invalid object ID: '.$idFile);
			}


			// @TODO Check if current user has parmission to download this Object (file)
			// 1. Get Course
			// 2. Check Learning_courseuser (isSubscribed)
			// 3. Throw an exception if NOT or even Redirect to home with flash image



			//track the page
			$idUser = Yii::app()->user->id;
			$object = $object->getMasterForCentralLo($idUser);

			$track = LearningMaterialsTrack::model()->findByAttributes(array('idReference' => $object->idOrg, 'idUser' => $idUser));
			if (!$track) {
				//the object has not been tracked yet for this user: create the record now
				$track = new LearningMaterialsTrack();
				$track->idUser = (int)$idUser;
				$track->idReference = (int)$object->getPrimaryKey();
				$track->idResource = $idFile;
			}
			$track->setStatus(LearningCommontrack::STATUS_COMPLETED);
			$track->setObjectType(LearningOrganization::OBJECT_TYPE_FILE);
			if (!$track->save()) {
				throw new CException("Error while saving track data");
			}
			UserLimit::logActiveUserAccess($idUser);

			//finish action
			$transaction->commit();

			// Use file storage manage
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_FILE);
			$file->original_filename = Sanitize::fileName($file->original_filename,true);
			$storageManager->sendFile($file->path, $file->original_filename);

		} catch (CException $e) {

			$transaction->rollback();
			//... TO DO : show an error page ...
			echo $e->getMessage();
		}
	}

}
