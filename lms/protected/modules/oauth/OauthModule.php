<?php

/**
 * Implements the Oauth server module
 * Class OauthModule
 */
class OauthModule extends CWebModule {

	public function init()
	{
		// import the module-level models and components
		$this->setImport(array(
			'oauth.models.*',
			'oauth.components.*',
			'common.components.oauth.*'
		));
	}
}
