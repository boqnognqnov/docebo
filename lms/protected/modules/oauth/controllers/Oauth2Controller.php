<?php

/**
 * Main Oauth2 server controller
 * Class Oauth2Controller
 */
class Oauth2Controller extends Controller {

	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		return array(
			array('allow',
				'users' => array('*'),
				'actions' => array('token', 'authorize')
			)
		);
	}

	/**
	 * Token request + refresh action
	 */
	public function actionToken() {
		$server = DoceboOauth2Server::getInstance();
		if($server) {
			$server->init();
			$server->handleTokenRequest();
		} else
			throw new CHttpException(500, "Could not initialize Oauth2 server component");
	}

	/**
	 * Main Authorization request flow
	 */
	public function actionAuthorize() {
		$server = null;

		try {
			// Load server instance
			$server = DoceboOauth2Server::getInstance();
			if ($server) {
				// Initialize the server object
				$server->init();

				// Check that the authorization request is a valid one
				$server->validateAuthorizeRequest();

				// Is this a cancel request?
				$cancel_action = Yii::app()->request->getParam('cancel_button', false);
				if($cancel_action) {
					$server->handleAuthorizeRequest(false);
					$server->sendResponse();
				} else {
					$shouldAuthorize = false;
					$login_action = Yii::app()->request->getParam('login_button', false);
					$authorize_action = Yii::app()->request->getParam('authorize_button', false);
					if ($login_action) {
						$username = Yii::app()->request->getParam('username', '');
						$password = Yii::app()->request->getParam('password', '');
						$ui = new DoceboUserIdentity($username, $password);
						if ($ui->authenticate()) {
							if ($ui->checkUserAlreadyLoggedIn())
								Yii::app()->user->setFlash('error', Yii::t('login', '_TWO_USERS_LOGGED_WITH_SAME_USERNAME'));
							elseif (Yii::app()->user->login($ui)) {
								$shouldAuthorize = true;
							} else
								Yii::app()->user->setFlash('error', Yii::t("login", '_NOACCESS'));
						} else
							Yii::app()->user->setFlash('error', Yii::t("login", '_NOACCESS'));
					} else if (!Yii::app()->user->getIsGuest() && $authorize_action)
						$shouldAuthorize = true;

					// If the scope of the request is "api", only superadmins are allowed
					if($shouldAuthorize && (Yii::app()->request->getParam('scope') == 'api') && !CoreUser::isUserGodadmin(Yii::app()->user->idst)) {
						$shouldAuthorize = false;
						Yii::app()->user->setFlash('error', Yii::t("oauth2", "Only superadministrators are allowed to use the \"api\" scope."));
					}

					if ($shouldAuthorize) {
						// Grant authorization code
						$server->handleAuthorizeRequest(true);
						$server->sendResponse();
					} else {
						// Render authorization view
						$this->layout = '//layouts/login';
						$this->render('authorize', array(
							'client_id' => $server->getClientId()
						));
					}
				}

			} else
				throw new CException("Could not initialize Oauth2 server component");

		} catch(Exception $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			if($server)
				$server->sendResponse();
		}

		Yii::app()->end();
	}
	
	/**
	 * Empty action, because XAPI has this endpoint defined as standard 
	 */
	public function actionInitiate() {
		Yii::app()->end();		
	}
	
	
}