<?php

// ************ Temporary view to be designed *****
/* @var $client_id */
$isGuest = Yii::app()->user->getIsGuest();
$model = OauthClients::model()->findByPk($client_id);
?>
<div id="authorization-top" class="top-bar">
    <span><?=Yii::t('oauth','Authorization required')?></span>
</div>
<div id="authorization-body" class="container-fluid">
	<div class="row-fluid">
			<div id="welcome-message" style="<?=($isGuest)?'':'margin-bottom:25px;'?>"><?= ($isGuest)?' ':Yii::t('standard','Welcome {user}!',array('{user}'=>Yii::app()->user->getUsername()));?> <strong><?=Yii::t('oauth','Authorize "{clientId}" to access your Docebo account?',array('{clientId}'=>$model->app_name))?></strong></div>
            <?php if($isGuest):?><span id="input-hint"><?=Yii::t('oauth','Please provide username and password')?></span><?php endif;?>
			<?= CHtml::beginForm('', 'POST', array('class' => 'form-horizontal', 'id' => 'login-form')); ?>

			<?php if (Yii::app()->user->hasFlash('error')) : ?>
				<div class='row-fluid'>
					<div class="span12 alert alert-error">
						<?php echo Yii::app()->user->getFlash('error'); ?>
					</div>
				</div>
			<?php endif; ?>

			<?php if($isGuest): ?>

					<div class='row-fluid'>
						<div id="username-input-container" class="control-group">
							<label class="control-label" for="username"><?=Yii::t('standard','_USERNAME'); ?> </label>
							<div class="controls">
								<input type='text' id="username" name='username' placeholder='<?=Yii::t('standard','_USERNAME'); ?>' maxlength='255'>
							</div>
						</div>

						<div id="password-input-container" class="control-group">
							<label class="control-label" for="password"><?=Yii::t('standard','_PASSWORD'); ?> </label>
							<div class="controls">
								<input type="password" id="password" name="password" placeholder="<?=Yii::t('standard','_PASSWORD'); ?>" maxlength="255" autocomplete="off">
							</div>
                            <label id="forgotten-password-label" class="control-label" for="login_button">
                                <a class="open-dialog blue-text lostdata" rel="dialog-lostdata" data-dialog-class="lostdata" href="<?=Yii::app()->createUrl('user/axLostdata');?>"><?php echo Yii::t('login', '_LOG_LOSTPWD'); ?>?</a>
                            </label>
						</div>
                        <div id="app-container" class="control-group">
                            <div class="app-icon"<?php if ($model->app_icon) { echo ' style="background-image: url('.CoreAsset::url($model->app_icon).');"'; } ?>>
                                <?= ($model->app_icon)?'':'<i class="fa fa-picture-o "></i>';?>
                            </div>
                            <div class="app-info">
                                <span class="app-name"><strong><?= $model->app_name?></strong><br>
                                <?= $model->app_description ?>
                                </span>
                            </div>
                            <div class="lock-icon">
                                <i class="fa fa-lock"></i>
                            </div>
                            <div class="logo">
                                <a class="header-logo" href="<?php echo Docebo::getUserHomePageUrl(); ?>">
                                    <?php echo CHtml::image(Yii::app()->theme->getLogoUrl(), Yii::t('login', '_HOMEPAGE')); ?>
                                </a>
                            </div>
                        </div>
						<div id="buttons-container" class="control-group">
							<div class="controls">
								<input class="confirm-save btn btn-docebo green big" type="submit" name="login_button" value="<?= Yii::t('login', 'Authorize') ?>">
							</div>
							<div class="controls">
								<input class="btn btn-docebo black big" type="submit" name="cancel_button" value="<?= Yii::t('standard', '_CANCEL') ?>">
							</div>
						</div>
					</div>

			<?php else: ?>
                <div id="app-container" class="control-group">
                    <div class="app-icon"<?php if ($model->app_icon) { echo ' style="background-image: url('.CoreAsset::url($model->app_icon).');"'; } ?>>
                        <?= ($model->app_icon)?'':'<i class="fa fa-picture-o "></i>';?>
                    </div>
                    <div class="app-info">
                                <span class="app-name"><strong><?= $model->app_name?></strong><br>
                                    <?= $model->app_description ?>
                                </span>
                    </div>
                    <div class="lock-icon" >
                        <i class="fa fa-lock"></i>
                    </div>
                    <div class="logo" >
                        <a class="header-logo" href="<?php echo Docebo::getUserHomePageUrl(); ?>">
                            <?php echo CHtml::image(Yii::app()->theme->getLogoUrl(), Yii::t('login', '_HOMEPAGE')); ?>
                        </a>
                    </div>
                </div>
				<div id="buttons-container" class="control-group">
					<div class="controls">
						<input class="confirm-save btn btn-docebo green big" type="submit" name="authorize_button" value="<?= Yii::t('login', 'Authorize') ?>">
					</div>
					<div class="controls controls-2nd-btn">
						<input class="btn btn-docebo black big" type="submit" name="cancel_button" value="<?= Yii::t('standard', '_CANCEL') ?>">
					</div>
				</div>
			<?php endif; ?>

			<?php echo CHtml::endForm(); ?>
	</div>
</div>
<script type="text/javascript">
    $(function(){
        $('body').css({
            'background':'url("/themes/spt/images/lock_bg.png")',
            'background-repeat': 'no-repeat',
            'background-position-y': 'center'
    });
        $('.header-logo,.loginFooter').css({'display':'none'});
        $('.logo .header-logo').show().css({
            'margin':'0px'
        });
        $('.logo .header-logo img').css({
            'margin-top':'5px',
            'vertical-align':'middle'
        });
        $('#loginBlock .centered').css({'padding':'0','width':'610px'});
    });
</script>