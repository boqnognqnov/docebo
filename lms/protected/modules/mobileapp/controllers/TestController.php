<?php

/**
 * Mobile version of the test controller to manage test !
 */
class TestController extends MobileappPlayerController {

	protected $test;

	/**
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl',
		);
	}


	/**
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		return array(

			// Admins and Teachers can do all
			array('allow',
				'users' => array('@'),
				'expression' => 'Yii::app()->controller->userCanAdminCourse',
			),

			// For Subscribers
			array('allow',
				'actions' => array('play', 'launch', 'review'),
				'users' => array('@'),
				'expression' => 'Yii::app()->controller->userIsSubscribed',
			),

			array('deny', // if some permission is wrong -> the script goes here
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
				'message' => Yii::t('course', '_NOENTER'),
			),

		);

	}

	/**
	 *
	 * @see PlayerBaseController::init()
	 */
	public function init() {
		parent::init();
		$this->registerResources();
	}

	/**
	 * Return path/alias to type-specific views
	 * @return string
	 */
	public function getQuestionViewsPath() {
		return "mobileapp.views.test.playQuestion/";
	}

	/**
	 * Return an active record of the current test in play
	 * @return mixed
	 */
	public function getTest() {
		return $this->test;
	}

	/**
	 * Render TEST LO launching UI (first page, welcome page)
	 */
	public function actionLaunch() {
		$rq = Yii::app()->request;
		$idObject = (int)$rq->getParam('id_object', false);
		$idCourse = (int)$rq->getParam('course_id', false);
		$idUser = (int)$rq->getParam('id_user', false);
		$csrfToken = Yii::app()->request->csrfToken;

		// Collect TEST/USER information for the launcing UI
		$testInfo = LearningTest::collectTestInfoByLoAndUser($idObject, $idUser);
		$loModel = LearningOrganization::model()->findByPk($idObject);
		$courseModel = LearningCourse::model()->findByPk($idCourse);


		$html = $this->render("launch", array(
			'mainInfo' => $testInfo['loModel'],
			'testInfo' => $testInfo['testModel'],
			'trackInfo' => $testInfo['trackModel'],
			'commonTrackInfo' => $testInfo['commonTrackModel'],
			'testScores' => $testInfo['testScores'],
			'courseUserModel' => $testInfo['courseUserModel'],
			'max_attemp_reached' => $testInfo['max_attemp_reached'],
			'suspended' => $testInfo['suspended'],
			'suspend_time_left' => $testInfo['suspend_time_left'],
			'need_prerequisites' => $testInfo['need_prerequisites'],
			'courseModel' => $courseModel,
			'timeReadable' => $testInfo['timeReadable'],
			'loModel' => $loModel,
			'csrfToken' => $csrfToken
		), true);

		echo $html;

	}


	/**
	 * Perform tracking of various types
	 *
	 * @param object $testModel
	 * @param object $trackModel
	 * @param number $lastPageSeen
	 * @param int|bool $lastPageSaved
	 * @return bool
	 */
	public function doComboTracking($testModel, $trackModel, $lastPageSeen, $lastPageSaved = false, $questions = array(), $status = LearningTesttrack::STATUS_DOING) {

		$trackModel->score_status = $status;
		$trackModel->last_page_seen = $lastPageSeen;

		if ($lastPageSaved !== FALSE) {
			if ($testModel->mod_doanswer || (!$testModel->mod_doanswer && $lastPageSaved > $trackModel->last_page_saved)) {
				$trackModel->last_page_saved = $lastPageSaved;
				$trackModel->storePage($lastPageSaved, $testModel->mod_doanswer);
				$trackModel->closeTrackPageSession($lastPageSaved);
			}
		}
		$trackModel->update();
		$trackModel->updateTrackForPage($lastPageSeen);

		// Track questions
		foreach ($questions as $question) {
			$trackQuestion = LearningTesttrackQuest::getTrack($trackModel->getPrimaryKey(), $question->getPrimaryKey(), $lastPageSeen);
		}

		return true;

	}


	/**
	 * Play TEST LO
	 */
	public function actionPlay() {

		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile(Yii::app()->theme->baseUrl . "/js/jquery.form.min.js");

		$rq = Yii::app()->request;
		$idTest = (int)$rq->getParam('id_test', false);
		$idCourse = (int)$rq->getParam('course_id', false);
		$refUrl = $rq->getParam('refurl', false);
		$pageToSave = $rq->getParam('page_to_save', FALSE);
		$previousPage = $rq->getParam('previous_page', FALSE);
		$continue = $rq->getParam('continue', FALSE);
		$pageContinue = $rq->getParam('page_continue', FALSE);
		$nextPage = $rq->getParam('next_page', FALSE);
		$prevPage = $rq->getParam('prev_page', FALSE);
		$showResults = $rq->getParam('show_result', FALSE);
		$restartTest = $rq->getParam('restart', FALSE);
		$testSaveKeep = $rq->getParam('test_save_keep', FALSE);
		$timeElapsed = $rq->getParam('time_elapsed', FALSE);
		$begin = $rq->getParam('begin', FALSE);
		$redirectParentWindow = false;

		$launchTest = $rq->getParam('launch_test', FALSE);
		if ($launchTest) {
			$this->forward('launch', true);
		}


		$showReview = $rq->getParam('show_review', FALSE);
		if ($showReview) {
			$this->forward('review', true);
		}


		try {

			$idUser = Yii::app()->user->id;
			$trackModel = LearningTesttrack::getTrack($idTest, $idUser, $idCourse);

			$testModel = LearningTest::model()->findByPk($idTest);
			if (!$testModel) {
				throw new CException(Yii::t('standard', 'Invalid TEST learning object'));
			}
			$this->test = $testModel;

			$loModel = LearningOrganization::model()->findByAttributes(array(
				'idCourse' => $idCourse,
				'idResource' => $idTest,
				'objectType' => LearningOrganization::OBJECT_TYPE_TEST
			));
			if (!$loModel) {
				throw new CException(Yii::t('standard', 'Invalid learning object'));
			}

			// Since this is a MobileApp TEST player, lets force display type to "single question per page"
			$displayType = LearningTest::DISPLAY_TYPE_SINGLE;
			$testModel->display_type = $displayType;

			$totalPages = $testModel->getTotalPageNumber();

			if ($previousPage === FALSE) {
				if ($pageContinue !== FALSE && $continue !== FALSE) {
					$pageToDisplay = $pageContinue;
				} else {
					if($rq->getRequestType() == 'GET' && !$rq->isAjaxRequest) {
						$page = (int)$rq->getParam('page', FALSE);
						if($page && $page <= $totalPages)
							$pageToDisplay = $page;
						else
							$pageToDisplay = $trackModel->last_page_seen;
					} else {
						$pageToDisplay = 1;
					}
				}
			} else {
				$pageToDisplay = $previousPage;
				if ($nextPage !== FALSE) {
					$pageToDisplay++;
				}
				if ($prevPage !== FALSE && $testModel->can_travel) {
					$pageToDisplay--;
				}
			}


			if (!$pageToDisplay) {
				$pageToDisplay = 1;
			}

			if (($pageToDisplay < $trackModel->last_page_seen) && (!$testModel->can_travel && !$restartTest && !$begin)) {
				$pageToDisplay = $trackModel->last_page_seen;
				$refUrl = $this->createUrl('play', array('id_test' => $testModel->getPrimaryKey(), 'course_id' => $idCourse, 'page' => $pageToDisplay));

				throw new CException(Yii::t('test', 'Backtracking is not allowed in this test'));
			}

			// Get list of questions to DISPLAY on this RUN. This is mobile player, we shuld get ONLY ONE
			$questions = $testModel->getQuestionsForPage($pageToDisplay, $idUser);

			if (TestUtils::readSessionValue($idTest, 'testDateBegin') === NULL) {
				TestUtils::setSessionValue($idTest, 'testDateBegin', Yii::app()->localtime->UTCNow);
			}


			// Handle various PLAY requests --------------------------------------------------

			// RESTART TEST
			if ($restartTest) {
				if (!$trackModel) {
					throw new CException(Yii::t('test', '_TEST_TRACK_FAILURE'));
				}
				$scoreStatus = $trackModel->score_status;
				$isEnd = ($scoreStatus == LearningTesttrack::STATUS_VALID
					|| $scoreStatus == LearningTesttrack::STATUS_NOT_CHECKED
					|| $scoreStatus == LearningTesttrack::STATUS_PASSED
					|| $scoreStatus == LearningTesttrack::STATUS_NOT_PASSED);

				if ($scoreStatus == LearningTesttrack::STATUS_NOT_COMPLETE || $isEnd) {
					TestUtils::resetTrack($testModel, $trackModel);
				}

			} // SAVE & EXIT
			else if ($testSaveKeep) {
				if (!$trackModel) {
					throw new CException(Yii::t('test', '_TEST_TRACK_FAILURE'));
				}

				if ($testModel->save_keep <= 0) {
					throw new CException(Yii::t('test', '_TEST_YOUCANNOT_SAVEKEEP'));
				}

				if ($trackModel->score_status != LearningTesttrack::STATUS_NOT_COMPLETE && $trackModel->score_status != LearningTesttrack::STATUS_DOING) {
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				}

				$result = $this->doComboTracking($testModel, $trackModel, $pageToDisplay, $pageToSave, array(), LearningTesttrack::STATUS_NOT_COMPLETE);

				$params = array();
				$params['backUrl'] = Docebo::createAbsoluteLmsUrl('//mobileapp/test/launch', array(
					'id_object' => $loModel->idOrg,
					'course_id' => $idCourse,
					'id_user' => $idUser,
				));
				$params['testModel'] = $testModel;
				$params['successful'] = ($result !== false) && ($result !== null);
				$this->render('save_keep', $params);
				Yii::app()->end();


			} // SHOW RESULTS
			else if ($showResults) {
				$this->results($testModel, $trackModel, $loModel, $previousPage, $pageToSave);
				Yii::app()->end();
			} // TIME ELAPSED
			else if (($timeElapsed == 1) && ($testModel->time_dependent == 1)) {
				$this->results($testModel, $trackModel, $loModel, $previousPage, $pageToSave);
				Yii::app()->end();
			} // BEGIN
			else if ($begin !== FALSE) {
				$scoreStatus = isset($trackModel->score_status) ? $trackModel->score_status :  null;
				$isEnd = $scoreStatus ?  ($scoreStatus == LearningTesttrack::STATUS_VALID
					|| $scoreStatus == LearningTesttrack::STATUS_NOT_CHECKED
					|| $scoreStatus == LearningTesttrack::STATUS_PASSED
					|| $scoreStatus == LearningTesttrack::STATUS_NOT_PASSED) : false;
				if ($isEnd && $scoreStatus && isset($idCourse) && $rq->isAjaxRequest){
					$response = new AjaxResult();
					$response->setStatus(true);
					$response->setData(array(
						'redirectUrl' =>  $this->createUrl('play', array(
								'id_test' => $this->test->getPrimaryKey(),
								'course_id' => $this->getIdCourse(),
								'page' => $pageToDisplay
							))
					));
					$response->toJSON();
					Yii::app()->end();
				}else{
					$trackModel->reset();
				}
			}
			// --------------------------------------------------


			// We are going to play the test,  it is NOT possible to have other status than these 2
			if ($trackModel->score_status != LearningTesttrack::STATUS_NOT_COMPLETE && $trackModel->score_status != LearningTesttrack::STATUS_DOING) {
				$refUrl = Docebo::getWebappUrl(true).'/#/course/'.$idCourse;
				$redirectParentWindow = true;
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}


			// Normal tracking, we are gonna play this thingie
			$this->doComboTracking($testModel, $trackModel, $pageToDisplay, $pageToSave, $questions);


			$lockEdit = FALSE;
			$checkTime = FALSE;
			$startTime = FALSE;
			$timeInQuest = FALSE;

			if ($testModel->time_dependent == 1 || $testModel->time_dependent == 2) {

				$checkTime = TRUE;

				if ($testModel->time_dependent == 1) {
					//time limit is for test
					$startTime = $testModel->time_assigned - $trackModel->userTimeInTheTest();

					if ($startTime <= 0) {
						$this->results($testModel, $trackModel, $loModel, $previousPage, $pageToSave);
						return;
					}

				} elseif ($testModel->time_dependent == 2) {
					//time limit is for question
					$startTime = $questions[0]['time_assigned'];

					$timeInQuest = $trackModel->userTimeInThePage($pageToDisplay);

					$startTime = $startTime - $timeInQuest;
					if ($startTime <= 0) {
						$lockEdit = TRUE;
					}

				}

			}


			// If answers are "mandatory", register the required Javascript
			if ($testModel->mandatory_answer) {
				$cs = Yii::app()->getClientScript();
				$cs->registerScriptFile(Yii::app()->findModule('test')->assetsUrl . '/js/mandatory_answers.js');
			}


			if($rq->isAjaxRequest) {
				$response = new AjaxResult();
				$response->setStatus(true);
				$redirectUrls = Docebo::createAbsoluteLmsUrl('//mobileapp/test/play', array(
					'id_test' => $this->test->getPrimaryKey(),
					'course_id' => $this->getIdCourse(),
					'page' => $pageToDisplay,
					"YII_CSRF_TOKEN" => $csrfToken1
				));
				$response->setData(array(
					'redirectUrl' => $redirectUrls
				));
				$response->toJSON();
			} else {
				$html = $this->render('play', array(

					'idCourse' => $idCourse,

					'idTest' => (int)$idTest,

					'testModel' => $testModel,
					'trackModel' => $trackModel,

					'pageToDisplay' => $pageToDisplay,
					'totalPages' => $totalPages,

					'lockEdit' => $lockEdit,
					'displayType' => $displayType,

					'checkTime' => $checkTime,
					'startTime' => $startTime,
					'timeInQuest' => $timeInQuest,

					'questions' => $questions,
					'startQuestionIndex' => $testModel->getInitQuestionSequenceNumberForPage($pageToDisplay),

					'mandatoryAnswers' => (bool)$testModel->mandatory_answer,
				), true);

				echo $html;
			}
			Yii::app()->end();

		} catch (CException $e) {
			$courseId = (int)$rq->getParam('course_id', false);
			Yii::app()->user->setFlash('error', $e->getMessage());
			if (($refUrl !== false) && is_string($refUrl)) {
				$url = "/webapp/#/course/".$courseId;
			} else {
				$url = "/webapp/#";
			}
			if($redirectParentWindow){
				echo CHtml::script("window.top.postMessage('message', '*');");
			}
			else
				$this->redirect($url);
		}

	}


	/**
	 * Show Test Outcome/Results as part of the TEST workflow
	 * For purely showing TEST results (review), use actionReview()
	 *
	 * @param object $testModel
	 * @param object $trackModel
	 * @throws CException
	 * @return boolean
	 */
	protected function results($testModel, $trackModel, $loModel, $previousPage, $pageToSave) {

		$idTest = $testModel->idTest;

		// NO idea why is this ...
		if ($trackModel->score_status == LearningTesttrack::STATUS_VALID && $showReview === FALSE) {
			throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			return false;
		}

		if ($trackModel->score_status != LearningTesttrack::STATUS_NOT_COMPLETE && $trackModel->score_status != LearningTesttrack::STATUS_DOING) {
			throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
		}

		if (TestUtils::readSessionValue($idTest, 'testDateBegin') === NULL) {
			TestUtils::setSessionValue($idTest, 'testDateBegin', Yii::app()->localtime->UTCNow);
		}

		// Do some tracking about seen pages and saved pages
		$result = $this->doComboTracking($testModel, $trackModel, $previousPage, $pageToSave);

		$testOutcome = TestUtils::getTestOutcomeByTrack($testModel, $trackModel);

		$trackingResult = TestUtils::doFinalizingTrack($testModel, $trackModel, $loModel, $testOutcome);

		$stdBackUrl = Docebo::createAbsoluteLmsUrl('//mobileapp/test/launch', array(
			'id_object' => $loModel->idOrg,
			'course_id' => $loModel->idCourse,
		));

		$this->render('results', array(
			'loModel' => $loModel,
			'testModel' => $testModel,
			'trackModel' => $trackModel,
			'totalUserScore' => $testOutcome['totalUserScore'],
			'totalMaxScore' => $testOutcome['totalMaxScore'],
			'countManual' => $testOutcome['countManual'],
			'manualScore' => $testOutcome['manualScore'],
			'categoriesScore' => $testOutcome['categoriesScore'],
			'backUrl' => $stdBackUrl,
			'testFailed' => $testOutcome['testFailed'],
			'feedbackText' => LearningTestFeedback::getFeedbackText($testModel->getPrimaryKey(), $testOutcome['totalUserScore']),
			'mandatoryAnswers' => (bool)$testModel->mandatory_answer,
		));

	}


	/**
	 * Review test results
	 */
	public function actionReview() {

		$rq = Yii::app()->request;
		$idTest = (int)$rq->getParam('id_test', false);
		$idCourse = (int)$rq->getParam('course_id', false);

		$loModel = LearningOrganization::model()->findByAttributes(array(
			'idCourse' => $idCourse,
			'idResource' => $idTest,
			'objectType' => LearningOrganization::OBJECT_TYPE_TEST
		));


		$rq = Yii::app()->request;
		$idUser = Yii::app()->user->id;

		$trackModel = LearningTesttrack::model()->with('questsWithTitles')->findByAttributes(array(
			'idReference' => $loModel->idOrg,
			'idUser' => $idUser
		));

		$testModel = LearningTest::model()->findByPk($idTest);

		$questionsLang = LearningTestquest::getQuestionTypesTranslationsList();

		$stdBackUrl = Docebo::createAbsoluteLmsUrl('//mobileapp/test/launch', array(
			'id_object' => $loModel->idOrg,
			'course_id' => $idCourse,
		));

		if (LearningTesttrackAnswer::model()->countByAttributes(array('idTrack' => $trackModel->idTrack)) == 0) {
			$this->redirect($stdBackUrl);
		}

		$answersVisible = $testModel->answersVisible(Yii::app()->user->id);
		if (!$answersVisible) {
			$this->redirect($stdBackUrl);
		}

		$this->render('review', array(
			'testModel' => $testModel,
			'loModel' => $loModel,
			'trackModel' => $trackModel,
			'idUser' => $idUser,
			'questionsLang' => $questionsLang,
		));


	}


}