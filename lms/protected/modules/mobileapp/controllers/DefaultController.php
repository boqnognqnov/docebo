<?php
/**
 * Default controller 
 */
class DefaultController extends MobileappBaseController {
	
	/**
	 * overwrite resolveLayout method to use module based layout
	 */
	public function resolveLayout()
	{
		$this->layout = '/layouts/base';
	}
	

	/**
	 * @see Controller::init()
	 */
	public function init() {
		parent::init();
	}
	
	
	public function actionIndex()
	{
		// Check if WhiteLabel is active is requires redirecting to Desktop version
		$whiteLabelMobileForceDesktop = false;
		/*
		if (PluginManager::isPluginActive('WhitelabelApp')) {
			if (Settings::get('whitelabel_mobile', false) == BrandingWhiteLabelForm::MOBILE_REDIRECT_DESKTOP) {
				$whiteLabelMobileForceDesktop = true;
			}
		}
		*/
		
		// If we land here, getting force_desktop parameter set, set user session variable and redirect to Desktop
		$notSmartphone = Yii::app()->request->getParam('not_smartphone', false); 
		if ($notSmartphone || $whiteLabelMobileForceDesktop) {
			$url = Docebo::createAbsoluteLmsUrl('site/index', array());
			$_SESSION['force_desktop'] =  1;
			$_SESSION['lasturl'] = $url;
			$this->redirect($url, true);
		}
		$this->render('index');
	}
}