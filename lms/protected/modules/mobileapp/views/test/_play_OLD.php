<div class="container-fluid">

	<div class="col-md-12">
		<?= $testModel->title ?>
	</div>

	<?php
		echo CHtml::form($this->createUrl('play', array('id_test' => $testModel->getPrimaryKey(), 'course_id' => $idCourse)),'POST', array(
			'id' 		=> 'test-play-form', 
			'enctype' 	=> 'multipart/form-data')
		);

		// Standard info
		echo CHtml::hiddenField('next_step', 'play', array('id' => 'next-step'));

		// Page info
		echo CHtml::hiddenField('page_to_save', $pageToDisplay, array('page-to-save'));
		echo CHtml::hiddenField('previous_page', $pageToDisplay, array('previous-page'));


		// Other info
		if ($checkTime) {
			echo CHtml::hiddenField('time_elapsed', 0, array('id' => 'time-elapsed'));
			?>
			<div class="test-time-clock">
				<?= Yii::t('test', '_TIME_LEFT') . ' : ' ?><span id="time-clock"> <?= (int)($startTime/60).' m '.($startTime%60) ?> 
				</span>
			</div>
			<?php 
		}

		//$questions = $testInfo->getQuestionsForPage();
		$_index = $startQuestionIndex;
		if (!empty($questions)) {
			foreach ($questions as $question) {

				if ($questionPlayer = CQuestionComponent::getQuestionManager($question->type_quest)) {

					$questionPlayer->setController($this);

					if ($questionPlayer->isInteractive()) {

						if ($displayType != LearningTest::DISPLAY_TYPE_SINGLE) { $_index++; }
						$questionIndex = $_index; //TO DO: fix this

						?>

						<div class="block <?= (!($_index%2)) ? 'odd' : '' ?>">

							<div class="block-header">
								<?= Yii::t('test', '_QUEST_'.strtoupper($question->type_quest)) ?>
							</div>
							<div class="block-content">

								<div data-question-index="<?= $question->idQuest ?>">
									<table class="play-question">
										<tbody>
											<tr>
												<td class="question-index"><?php echo $questionIndex.')'; ?></td>
												<td class="question-title"><?php
													echo $questionPlayer->applyTransformation('title', $question->title_quest, array(
														'idQuestion' => $question->idQuest,
														'questionManager' => $questionPlayer
													));
												?></td>
											</tr>
											<tr>
												<td></td>
												<td>
													<div class="answers-container <?php echo $question->type_quest; ?>">
														<?php $questionPlayer->play($question->getPrimaryKey()); ?>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								</div>

							</div>
						</div>
						<div class="block-hr"></div>
					<?php

					} else {
						//titles, break pages ...
						echo $questionPlayer->play($question->getPrimaryKey());
					}
				} else {
					//throw ...
				}
			}
		}

	?>

	<?php if ($mandatoryAnswers): ?>
	<div class="answer-info text-right">
		<span id="answer-info">
			<?php echo Yii::t('test', '_NEED_ANSWER'); ?>
		</span>
	</div>
	<?php endif; ?>

	<div class="row-fluid">
		<div class="span4">
			<div class="page-info"><?php
				echo Yii::t('test', '_TEST_PAGES').': ';
				echo '<span class="info-page-to-display">'.(int)$pageToDisplay.'</span>';
				echo ' / ';
				echo '<span class="info-total-pages">'.(int)$totalPages.'</span>';
			?>
			</div>
		</div>
		<div class="span8">
			<div class="form-actions">
				<?php
				if ($testInfo->save_keep == 1) {
					//save and exit
					echo CHtml::submitButton(Yii::t('test', '_TEST_SAVE_KEEP'), array('name' => 'test_save_keep', 'id' => 'save-keep-btn', 'class' => 'btn-docebo grey verybig'));
				}
				if ($testInfo->can_travel && ($pageToDisplay != 1)) {
					//back to the next page
					echo CHtml::submitButton(Yii::t('test', '_TEST_PREV_PAGE'), array('name' => 'prev_page', 'id' => 'prev-page-btn', 'class' => 'btn-docebo grey verybig'));
				}
				if ($pageToDisplay != $totalPages) {
					//button to the next page
					echo CHtml::submitButton(Yii::t('test', '_TEST_NEXT_PAGE'), array('name' => 'next_page', 'id' => 'next-page-btn', 'class' => 'btn-docebo grey verybig'));
					//$GLOBALS['page']->add(Form::getButton('next_page', 'next_page', $lang->def('_TEST_NEXT_PAGE'), '', ($tot_question > 0 && $test_info['mandatory_answer'] == 1 ? ' disabled="disabled"' : '')), 'content');
				} else {
					//button to the result page
					echo '&nbsp;&nbsp;<button type="submit" name="show_result" id="test-show-result" class="btn-submit-test btn-docebo big green pull-right">'
						.'<span class="i-sprite is-circle-check large white"></span>&nbsp;'
						.Yii::t('test', '_TEST_END_PAGE')
						.'</button>';
				}
				?>
			</div>
		</div>
	</div>

	<?php
		echo CHtml::closeTag('form');
	?>
	
</div>

<script type="text/javascript">

setTimeout("keepalive()", 1*60*1000);
function keepalive() {
	var d = new Date();
	var time = d.getTime();
	$.ajax({
		url: <?=CJavaScript::encode($this->createUrl('keepAlive', array('course_id' => $idCourse, 'id_test' => (int) $idTest)))?>
	});
	setTimeout("keepalive()", 1*60*1000);
}


<?php if ($checkTime): ?>

	var start_count_from = <?=CJavaScript::encode((int)$startTime)?>;
	var step = 1;
	var time_elapsed = 0;
	
	var id_interval;
	var id_timeout;
	
	
	function counter() {
	
		time_elapsed += step;
	
		var display = start_count_from - time_elapsed;
		var elem = $('#time-clock');
	
		if (display <  0) return;
	
		var value = display/60;
		var minute = Math.floor(value).toString(10);
		if (minute.length <= 1) { minute = '0' + minute; }
		value = display%60;
		var second = Math.floor(value).toString(10);
		if (second.length <= 1) { second = '0' + second; }
		elem.html('' + minute + 'm ' + second  + ' s');
	}
	
	function whenTimeElapsed() {
	
		window.clearInterval(id_interval);
		window.clearTimeout(id_timeout);
	
		var submit_to_end = document.getElementById('test-play-form');
		var time_elapsed = document.getElementById('time-elapsed');
		time_elapsed.value = 1;
		alert(<?=CJavaScript::encode(Yii::t('test', '_TIME_ELAPSED'))?>);
		<?php if ($testInfo->time_dependent == 2): ?>
		var nxt = document.createElement('INPUT');
		nxt.type = 'hidden';
		nxt.name = '<?=($pageToDisplay != $totalPages ? 'next_page' : 'show_result')?>';
		nxt.value = '1';
		submit_to_end.appendChild(nxt);
		<?php endif; ?>
		submit_to_end.submit();
	}
	
	function activateCounter() {
		counter();
		id_interval = window.setInterval("counter()", step * 1000);
		id_timeout = window.setTimeout("whenTimeElapsed()", (start_count_from - 1) * 1000);
	}
	
	$(document).ready(function() { activateCounter() });

<?php endif; ?>



<?php if ($mandatoryAnswers):

	$qlist = array();
	if (!empty($questions)) {
		foreach ($questions as $question) {
			$qlist[] = array('id' => $question->getPrimaryKey(), 'type' => $question->type_quest);
		}
	}
	?>
	try {
		$(function(){ Mandatory.init(<?php echo CJavaScript::encode($qlist); ?>); });
	} catch(e) {}
	
<?php endif; ?>



</script>