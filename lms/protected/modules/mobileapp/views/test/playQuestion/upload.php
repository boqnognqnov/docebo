<?php

$idQuestion = $question->getPrimaryKey();

//check test settings
$disabled = !$questionManager->canBeAnswered($idQuestion, Yii::app()->user->id, $idTest);

$userAnswer = $questionManager->recoverUserAnswer($idQuestion, Yii::app()->user->id, $idTest);
$value = (!empty($userAnswer) ? array_shift($userAnswer)->more_info : '');

echo CHtml::fileField(
	$questionManager->getInputName($idQuestion), 
	$value, 
	array(
		'id' => $questionManager->getInputId($idQuestion),
		'class' => 'mobile-entry',
		'disabled' => (!empty($value) ? $disabled : false) //if empty path is present in DB as answer, then consider the question as unanswered anyway
	)
);

?>