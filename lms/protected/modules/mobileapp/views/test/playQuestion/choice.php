<?php

$checked = array();
$userAnswer = $questionManager->recoverUserAnswer($idQuestion, Yii::app()->user->id, $idTest);
foreach ($userAnswer as $answer) {
	if ($answer->user_answer > 0) $checked[] = $answer->idAnswer;
}

$index = 0;
if (is_array($answers)) {
	foreach ($answers as $answer) {
		$index++;
		$idFor = $questionManager->getInputId($idQuestion).'-'.$index;
		$idAnswer = $answer['id_answer'];
		echo '<div class="row test-info-row"><div class="col-md-12 col-xs-12">';
		echo CHtml::label(
			CHtml::radioButton(
				$questionManager->getInputName($idQuestion).'[]',
				in_array($idAnswer, $checked),
				array('id' => $idFor, 'value' => $idAnswer)
			).' '.$answer['answer'],
			$idFor,
			array('class' => 'radio-inline')
		);
		echo '</div></div>';
	}

	if(Settings::get('no_answer_in_test', 'off') == 'on')
	{
		$index++;
		$idFor = $questionManager->getInputId($idQuestion).'-'.$index;
		echo '<div class="row test-info-row"><div class="col-md-12 col-xs-12">';
		echo CHtml::label(
			CHtml::radioButton(
				$questionManager->getInputName($idQuestion).'[]',
				in_array(0, $checked),
				array('id' => $idFor, 'value' => 0)
			).' '.Yii::t('standard', '_NO_ANSWER'),
			$idFor,
			array('class' => 'radio-inline')
		);
		echo '</div></div>';
	}
}