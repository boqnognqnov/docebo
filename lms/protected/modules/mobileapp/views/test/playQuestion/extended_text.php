<?php

//check test settings
$disabled = !$questionManager->canBeAnswered($idQuestion, Yii::app()->user->id, $idTest);

$answer = $questionManager->recoverUserAnswer($idQuestion, Yii::app()->user->id, $idTest);

$text = (!empty($answer) && isset($answer[0]) ? $answer[0]->more_info : "");

echo CHtml::textArea(
	$questionManager->getInputName($idQuestion), 
	$text,
	array(
		'id' => $questionManager->getInputId($idQuestion),
		'disabled' => (!empty($text) ? $disabled : false) //if empty text is present in DB as answer, then consider the question as unanswered anyway
	)
);

?>
