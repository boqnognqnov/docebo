<?
/* @var $this QuestionAssociate */
/* @var $associations array */
/* @var $answers array */
?>
<table>
	<tbody>
		<?php

			//check test settings
			$disabled = !$questionManager->canBeAnswered($idQuestion, Yii::app()->user->id, $idTest);

			if (!empty($answers)) {

				$options = array(0 => '--');
				if (!empty($associations)) {
					foreach ($associations as $association) {
						$options[$association->idAnswer] = $association->answer;
					}
				}

				foreach ($answers as $answer) {
					$idAnswer = $answer->getPrimaryKey();
					$idFor = $questionManager->getInputId($idQuestion).'-'.$idAnswer;
					$selectedAnswer = (isset($previous[$idAnswer]) ? $previous[$idAnswer]->more_info : 0);
					$dropDown = CHtml::dropDownList(
						$questionManager->getInputName($idQuestion).'['.$idAnswer.']',
						$selectedAnswer,
						$options,
						array('id' => $idFor, 'disabled' => ($selectedAnswer > 0 ? $disabled : false)) //always enable not-answered associations
					);

					echo '<div class="col-md-12 col-xs-12">'.CHtml::label($answer->answer, $idFor).'</div>';
					echo '<div class="col-md-12 col-xs-12">'.$dropDown.'</div>';
				}
			}

		?>
	</tbody>
</table>