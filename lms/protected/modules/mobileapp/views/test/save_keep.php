<div class="test-launch-container container-fluid">

	<div class="row">
		<div class="col-md-12 col-xs-12">
			<h4 class="test-title"><?= $testModel->title ?></h4>
		</div>
	</div>
	
	<div class="row test-info-wrapper">
		<div class="col-md-12 col-xs-12 test-description">
			<?php echo Yii::t('standard', ($successful ? '_OPERATION_SUCCESSFUL' : '_OPERATION_FAILURE')); ?>
		</div>

		<div class="row button-row">
			<div class="col-md-12 text-center">
				<?php echo CHtml::link(
					Yii::t('test', 'Go back'),
					$backUrl,
					array(
						'name' => 'launch_test',
						'id' => 'launch-test-btn',
						'class' => 'btn btn-default btn-lg col-md-12 col-xs-12'
					)); ?>
			</div>
		</div>
	</div>
</div>
