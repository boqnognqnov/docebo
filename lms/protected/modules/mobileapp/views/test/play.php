<?php 

	$answerQuestionButtonHtml 	= '<div class="row button-row"><div class="col-md-12 text-center">' . CHtml::submitButton(Yii::t('test', 'Answer question'), 	array('name' => 'next_page', 'id' => 'next-page-btn', 'class' => 'btn btn-success btn-lg col-md-12 col-xs-12')) . '</div></div>';
	$saveKeepButtonHtml 		= '<div class="row button-row"><div class="col-md-12 text-center">' . CHtml::submitButton(Yii::t('test', '_TEST_SAVE_KEEP'), 	array('name' => 'test_save_keep', 'id' => 'save-keep-btn', 'class' => 'btn btn-default btn-lg col-md-12 col-xs-12')) . '</div></div>';
	$prevPageButtonHtml 		= '<div class="row button-row"><div class="col-md-12 text-center">' . CHtml::submitButton(Yii::t('test', 'Previous Question'), 	array('name' => 'prev_page', 'id' => 'prev-page-btn', 'class' => 'btn btn-default btn-lg col-md-12 col-xs-12')) . '</div></div>';
	$showResultsButtonHtml 		= '<div class="row button-row"><div class="col-md-12 text-center">' . CHtml::submitButton(Yii::t('test', '_TEST_END_PAGE'), 	array('name' => 'show_result', 'id' => 'test-show-result', 'class' => 'btn btn-success btn-lg col-md-12 col-xs-12')) . '</div></div>';
	
	// FORM
	echo CHtml::beginForm($this->createUrl('play', array('id_test' => $testModel->getPrimaryKey(), 'course_id' => $idCourse, 'page' => $pageToDisplay)),'POST', array(
			'id' 		=> 'test-play-form',
			'enctype' 	=> 'multipart/form-data',
			'role'		=> 'form')
	);
	
	// Standard info
	echo CHtml::hiddenField('next_step', 'play', array('id' => 'next-step'));
	
	// Page info
	echo CHtml::hiddenField('page_to_save', $pageToDisplay, array('page-to-save'));
	echo CHtml::hiddenField('previous_page', $pageToDisplay, array('previous-page'));
	
	if ($checkTime) {
		echo CHtml::hiddenField('time_elapsed', 0, array('id' => 'time-elapsed'));
	}
	
	
?>

<div class="test-launch-container container-fluid">

	<div class="row">
		<div class="col-md-12 col-xs-12">
			<h4 class="test-title"><?= $testModel->title ?></h4>
		</div>
	</div>

	
	<div class="row test-info-wrapper"><div class="col-md-12 col-xs-12">
	
		<div class="row item-counter">
			<div class="col-md-6 col-xs-6">
				<?= Yii::t('test', '_QUEST'); ?> <?= (int) $pageToDisplay ?>/<?= (int) $totalPages ?>
			</div>
			<div class="text-right col-md-6 col-xs-6">
				<?php if ($checkTime) : ?>
					<div class="test-time-clock"><?= Yii::t('test', '_TIME_LEFT') ?>:
						<span id="time-clock"><?= (int)($startTime/60)?> m <?= ($startTime%60) ?> s</span>
					</div>
				<?php endif;?>
			</div>
		</div>
	
	
		<?php if ($mandatoryAnswers): ?>
			<div class="row" id="answer-info">
				<div class="col-md-12 col-xs-12">
					<span class="status-bad">
						<?php echo Yii::t('test', 'You must answer this question'); ?>
					</span>
				</div>
			</div>
		<?php endif; ?>
	
		
		<?php 
			// Show questions
			foreach ($questions as $question) {
				$questionPlayer = CQuestionComponent::getQuestionManager($question->type_quest);
				$questionPlayer->setController($this);
				
				// Is this a real question or meta-question (like page break, etc.)
				if ($questionPlayer->isInteractive()) {
				?>
				
				<div class="row">
					<div class="col-md-12 col-xs-12 play-title <?= $question->type_quest; ?>">
						<p><?= $questionPlayer->applyTransformation('title', $question->title_quest, array(
							'idQuestion' => $question->idQuest,
							'questionManager' => $questionPlayer
						)); ?></p>
					</div>
				</div>
				
				<div class="row answers-block">
					<div class="col-md-12 col-xs-12">
						<?php $questionPlayer->play($question->getPrimaryKey()); ?>
					</div>
				</div>
				

				<?php 
				}
		
			}
		?>
		
		
		<?php
			// Navigation buttons, depending on various conditions
			
			if ($pageToDisplay != $totalPages) {
				echo $answerQuestionButtonHtml;
			}
			if ($testModel->can_travel && ($pageToDisplay != 1)) {
				echo $prevPageButtonHtml;
			}
				
		
			if ($testModel->save_keep == 1) {
				echo $saveKeepButtonHtml;
			}
				
			if ($pageToDisplay >= $totalPages) {
				echo $showResultsButtonHtml;
			}
				
		?>
		
		
	</div></div>
	

</div>



<?php 
	echo CHtml::endForm();
?>



<script type="text/javascript">
	var keepAliveTimeout = 60;  // seconds

	setTimeout("keepalive()", 5*keepAliveTimeout*1000);
	function keepalive() {
		var d = new Date();
		var time = d.getTime();
		$.ajax({
			url: <?=CJavaScript::encode($this->createUrl('//test/player/keepAlive', array('course_id' => $idCourse, 'id_test' => (int) $idTest)))?>
		});
		setTimeout("keepalive()", 5*keepAliveTimeout*1000);
	}


	<?php if ($checkTime): ?>

		var start_count_from = <?=CJavaScript::encode((int)$startTime)?>;
		var step = 1;
		var time_elapsed = 0;
		
		var id_interval;
		var id_timeout;
		
		
		function counter() {
		
			time_elapsed += step;
		
			var display = start_count_from - time_elapsed;
			var elem = $('#time-clock');
		
			if (display <  0) return;
		
			var value = display/60;
			var minute = Math.floor(value).toString(10);
			if (minute.length <= 1) { minute = '0' + minute; }
			value = display%60;
			var second = Math.floor(value).toString(10);
			if (second.length <= 1) { second = '0' + second; }
			elem.html('' + minute + 'm ' + second  + ' s');
		}

		
		function whenTimeElapsed() {
			window.clearInterval(id_interval);
			window.clearTimeout(id_timeout);
		
			var submit_to_end = document.getElementById('test-play-form');
			var time_elapsed = document.getElementById('time-elapsed');
			time_elapsed.value = 1;
			alert(<?=CJavaScript::encode(Yii::t('test', '_TIME_ELAPSED'))?>);
			
			<?php if ($testModel->time_dependent == 2): ?>
				var nxt = document.createElement('INPUT');
				nxt.type = 'hidden';
				nxt.name = '<?=($pageToDisplay != $totalPages ? 'next_page' : 'show_result')?>';
				nxt.value = '1';
				submit_to_end.appendChild(nxt);
			<?php endif; ?>
			
			submit_to_end.submit();
		}
		
		function activateCounter() {
			counter();
			id_interval = window.setInterval("counter()", step * 1000);
			id_timeout = window.setTimeout("whenTimeElapsed()", (start_count_from - 1) * 1000);
		}
		
		$(document).ready(function() { activateCounter() });

	<?php endif; ?>

	$(document).ready(function() {
		window.top.postMessage({
			event_id: 'disable',
			data: {
				url: 'empty'
			}
		}, '*');
		$($(document)[0].body).on('click', 'input.btn, #restart, #show-review', function (e) {
			/* Start Loader */
			window.top.postMessage('enable', '*');
		});

		$('#test-play-form').ajaxForm({
			dataType: 'json',
			success: function(o) {
				if (o.success) {
					window.top.postMessage({
						event_id: 'disable',
							data: {
							url: o.data.redirectUrl
						}
					}, '*');
				}
			}
		});

		$('#test-show-result, #save-keep-btn').click(function(e){
			$('#test-play-form').ajaxFormUnbind();
		});
		$($(document)[0].body).on('click', '#test-show-result, #save-keep-btn', function (e) {
			window.top.postMessage('enable', '*');
			/* Stop Loader */
			setTimeout(function () {
				window.top.postMessage({
					event_id: 'disable',
					data: {
						url: 'empty'
					}
				}, '*');
			}, 1000);
		});
	});

	<?php if ($mandatoryAnswers):
		$qlist = array();
		if (!empty($questions)) {
			foreach ($questions as $question) {
				$qlist[] = array('id' => $question->getPrimaryKey(), 'type' => $question->type_quest);
			}
		}
		?>
		try {
			$(function(){ Mandatory.init(<?php echo CJavaScript::encode($qlist); ?>); });
		} 
		catch(e) {
			Docebo.log(102);
		}
		
	<?php endif; ?>
</script>
