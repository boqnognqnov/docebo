<?php

	$numberOfQuestions = $testInfo->getNumberOfQuestionsForStudent();
	$endStatuses = array(
		LearningTesttrack::STATUS_VALID,
		LearningTesttrack::STATUS_NOT_CHECKED,
		LearningTesttrack::STATUS_PASSED,
		LearningTesttrack::STATUS_NOT_PASSED
	);

	$isEnd = in_array($trackInfo->score_status, $endStatuses);
	$prerequisites = $mainInfo->prerequisites;
	$mustPlayUntilComplete 	= stripos($prerequisite, 'incomplete') !== false;
	$hasNULLPrerequisite 	= stripos($prerequisite, 'NULL') !== false;
	
	if ($trackInfo->score_status == LearningTesttrack::STATUS_PASSED) {
		$incomplete = FALSE;
	} 
	else if ($trackInfo->score_status == LearningTesttrack::STATUS_VALID) {
		if ($trackInfo->score >= $testInfo->point_required) {
			$incomplete = FALSE;
		} 
		else {
			$incomplete = TRUE;
		}
	} 
	else {
		$incomplete = TRUE;
	}
	
	$isNotComplete = $trackInfo->score_status == LearningTesttrack::STATUS_NOT_COMPLETE;
	$hasQuestions =  $numberOfQuestions > 0;
	$isAboveStudent = $courseUserModel->level > 3;
	$isNotPassedAndNotValid = $trackInfo->score_status !== LearningTesttrack::STATUS_VALID && $trackInfo->score_status !== LearningTesttrack::STATUS_PASSED;
	$testPassed = $trackInfo->score_status == LearningTesttrack::STATUS_PASSED;
	$testNotPassed = $trackInfo->score_status == LearningTesttrack::STATUS_NOT_PASSED;
	$hasAnswers = LearningTesttrackAnswer::model()->countByAttributes(array('idTrack' => $trackInfo->idTrack)) > 0;
	
	if (
		$testInfo->show_solution == 0
		|| $testInfo->show_doanswer == 0
		|| ($testInfo->show_solution == 2 && $testPassed)
		|| ($testInfo->show_doanswer == 2 && $testPassed)
	) {
		$showResult = true;
	} 
	else {
		$showResult = false;
	}
	
	
	$playUrl = Docebo::createAbsoluteLmsUrl('//mobileapp/test/play', array(
		'id_test' 	=> $testInfo->getPrimaryKey(),
		'course_id' => $courseModel->idCourse,
	));	
	
	$refUrl = Docebo::createAbsoluteLmsUrl('//mobileapp/test/launch', array(
			'id_object' 	=> $loModel->getPrimaryKey(),
			'course_id' 	=> $courseModel->idCourse,
			'id_user'		=> $courseUserModel->idUser,
	));
	
	// Some HTML
	$restartButtonHtml 	= '<div class="row button-row"><div class="col-md-12 text-center">' . CHtml::submitButton(Yii::t('test', '_TEST_RESTART'), 	array('name' => 'restart', 'id' => 'restart', 'class' => 'btn btn-success btn-lg col-md-12 col-xs-12')) . '</div></div>';
	$beginButtonHtml 	= '<div class="row button-row"><div class="col-md-12 text-center">' . CHtml::submitButton(Yii::t('test', '_TEST_BEGIN'), 	array('name' => 'begin', 'id' => 'begin', 'class' => 'btn btn-success btn-lg col-md-12 col-xs-12')) . '</div></div>';
	$reviewButtonHtml  = '<div class="row button-row"><div class="col-md-12 text-center">' . CHtml::submitButton(Yii::t('test', '_TEST_SHOW_REVIEW'), array('name' => 'show_review', 'id' => 'show-review', 'class' => 'btn btn-primary btn-lg col-md-12 col-xs-12')) . '</div></div>';
	$continueButtonLms	= '<div class="row button-row"><div class="col-md-12 text-center">' . CHtml::submitButton(Yii::t('test', '_TEST_CONTINUE'), array('name' => 'continue', 'id' => 'continue', 'class' => 'btn btn-success btn-lg col-md-12 col-xs-12')) . '</div></div>';

	$maxAttemptsReachedHtml = '<span class="status-bad">' . Yii::t('test', '_MAX_ATTEMPT_REACH') . '</span>';
	
	
?>

<div class="test-launch-container container-fluid">

	<div class="row">
		<div class="col-md-12 col-xs-12">
			<h4 class="test-title"><?= $testInfo->title ?></h4>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12 col-xs-12">
			<p>Final test</p>
		</div>
	</div>
	
	
	
	<div class="row test-info-wrapper"><div class="col-md-12 col-xs-12">

	
		<div class="row test-description">
			<div class="col-md-12 col-xs-12">
				<?php if ($testInfo->description): ?>
					<?= $testInfo->description ?>
				<?php endif;?>
			</div>
		</div>
		
		
		<?php if ($testPassed) : ?>
			<div class="container-fluid test-completion-block">
				<div class="row">
					<div class="col-md-12 col-xs-12">
				
						<div class="pull-left completion-icon">
							<?php
								echo ( $testNotPassed
									? '<span class="p-sprite lo-failed-icon-big"></span>'
									: '<span class="p-sprite lo-passed-icon-big"></span>');
							?>
						</div>
						<div class="completion-info">
					
							<div class="textual text-uppercase">
								<?php
									echo ( $testNotPassed
										? '<span class="status-bad">' . Yii::t('test', '_TEST_FAILED') . '</span>' 
										: '<span class="status-good">' .Yii::t('test', '_TEST_COMPLETED') . '</span>'
									);
								?>
							</div>
							
						
							<div class="score">
								<?php 
									if ($testInfo->show_score) {
										switch ($testInfo->point_type) {
											case LearningTest::POINT_TYPE_NORMAL : {
												echo Yii::t('test', '_TEST_TOTAL_SCORE') 
													.'<b class="' . ($trackInfo->score_status == LearningTesttrack::STATUS_NOT_PASSED ? 'score-failed' : 'score-passed' ) . '">'
													. ( $trackInfo->score + $trackInfo->bonus_score)
													. ( $totalMaxScore ? ' / ' . $totalMaxScore : '' )
													. '</b>';
												break;
											}
											case LearningTest::POINT_TYPE_PERCENT : {
												$score = $trackInfo->score + $trackInfo->bonus_score;
												$score = ($score > 0)? $score : 0;
												echo Yii::t('test', '_TEST_TOTAL_SCORE') 
													.'<b class="' . ($trackInfo->score_status == LearningTesttrack::STATUS_NOT_PASSED ? 'score-failed' : 'score-passed' ) . '">'
													. $score . '</b> '.($commonTrackInfo && $commonTrackInfo->score_max == 100 ? '%' : '');
												break;
											}
										}
									}
								?>
							</div>
						</div>
					
					</div>
					
					
				</div>
			</div>
		<?php endif;?>
		
		<?php if ($testInfo->hide_info <= 0) : ?>
		
			<div class="row">
				<div class="col-md-12">
					<h5><?= Yii::t('test', '_TEST_INFO') ?>:</h5>
				</div>
			</div>
			
			<div class="container-fluid test-info-block">
				<div class="row">
					<div class="col-md-12">
					
						<?php if ($testInfo->order_type < 2) : ?>
							<div class="row test-info-row">
								<div class="col-md-12">
									<?php 
										$replaceValue = $testInfo->getMaxScore();
										echo Yii::t('test', '_TEST_MAXSCORE', array('[max_score]' => $replaceValue));
									?>
								</div>
							</div>
						<?php endif; ?>
						
					
						<div class="row test-info-row">
							<div class="col-md-12">
								<?php
									$replaceValue = ''.$testInfo->getNumberOfQuestionsForStudent();
									echo Yii::t('test', '_TEST_QUESTION_NUMBER', array('[question_number]' => $replaceValue));
								?>
							</div>
						</div>
						
						<?php if ($testInfo->point_required != 0): ?>
							<div class="row test-info-row">
								<div class="col-md-12">
									<?php
										$replaceValue = ''.Yii::app()->format->formatNumber($testInfo->point_required);
										if($testInfo->point_type==LearningTest::POINT_TYPE_PERCENT){
											$replaceValue .= ' %';
										}
										echo Yii::t('test', '_TEST_REQUIREDSCORE', array('[score_req]' => $replaceValue));
									?>
								</div>
							</div>
						<?php endif; ?>
						
						
						<div class="row test-info-row">
							<div class="col-md-12">
								<?php 
									echo ($testInfo->save_keep ? Yii::t('test', '_TEST_SAVEKEEP') : Yii::t('test', '_TEST_SAVEKEEP_NO')); 
								?>	
							</div>
						</div>
						
						
						<div class="row test-info-row">
							<div class="col-md-12">
								<?php echo ($testInfo->mod_doanswer ? Yii::t('test', '_TEST_MOD_DOANSWER') : Yii::t('test', '_TEST_MOD_DOANSWER_NO')); ?>
							</div>
						</div>
						
						<div class="row test-info-row">
							<div class="col-md-12">
								<?php echo ($testInfo->can_travel ? Yii::t('test', '_TEST_CAN_TRAVEL') : Yii::t('test', '_TEST_CAN_TRAVEL_NO')); ?>
							</div>
						</div>
						
						<div class="row test-info-row">
							<div class="col-md-12">
								<?php
									echo (($testInfo->show_score || $testInfo->show_score_cat)
										? Yii::t('test', '_TEST_SHOW_SCORE')
										: Yii::t('test', '_TEST_SHOW_SCORE_NO'));
								?>
							</div>
						</div>
						
						<div class="row test-info-row">
							<div class="col-md-12">
								<?php echo ($testInfo->show_solution != 1 ? Yii::t('test', '_TEST_SHOW_SOLUTION') : Yii::t('test', '_TEST_SHOW_SOLUTION_NO')); ?>	
							</div>
						</div>
						
						<div class="row test-info-row">
							<div class="col-md-12">
								<?php
									switch ($testInfo->time_dependent) {
										case 0 : echo Yii::t('test', '_TEST_TIME_ASSIGNED_NO'); break;
										case 1 : echo $timeReadable; break; 
										case 2 : echo Yii::t('test', '_TEST_TIME_ASSIGNED_QUEST'); break;
									}
								?>
							</div>
						</div>
						
						<?php if ($testInfo->max_attempt > 0): ?>
							<div class="row">
								<div class="col-md-12">
									<?php
										$replaceValue = ($testInfo->max_attempt - $trackInfo->number_of_attempt);
										echo Yii::t('test', '_NUMBER_OF_ATTEMPT', array('[remaining_attempt]' => $replaceValue));
									?>
								</div>
							</div>
							
							<?php if ($max_attemp_reached) : ?>
								<div class="row">
									<div class="col-md-12">
										<?php
											echo $maxAttemptsReachedHtml;
										?>
									</div>
								</div>
							<?php endif; ?>
							
						<?php endif; ?>		
						
					</div>
				</div>
			</div>
		
		
		<?php endif; ?>
		
		
		<!-- FORM:BEGIN -->
		<?php if ($hasQuestions) : ?>
			<?php echo CHtml::form($playUrl, 'POST', array('id' => 'play-test-form')); ?>
            <?= CHtml::hiddenField('YII_CSRF_TOKEN', $csrfToken) ?>
			<?php echo CHtml::hiddenField('refurl', $refUrl);?>

			<?php

			$refUrl = $refUrl . "&YII_CSRF_TOKEN=" . $csrfToken;
				if ($isNotComplete) {
					echo CHtml::hiddenField('page_continue', $trackInfo->last_page_seen, array('id' => 'page-continue'));
				}
				
				if ($isEnd) {
					echo CHtml::hiddenField('show_result', 1, array('id' => 'show-result'));
				}
				if ($testInfo->save_keep && $isNotComplete && !$suspended && !$max_attemp_reached) {
					echo Yii::t('test', '_TEST_SAVED');
					echo '<div class="row"><div class="col-md-12 text-center">';
					echo $continueButtonLms;
					echo '</div></div>';
				}
				
				if ($suspended) {
					if ($need_prerequisites && !$max_attemp_reached) {
						echo Yii::t('test', 'Your test is suspended. In order to retake this test, you must review all prerequisite material.');
					}
					else {
						echo Yii::t('test', 'Your test is suspended. You will be able to retake your test in {time left}.', array('{time left}' => $suspend_time_left));
					}
				}
				else if ($isNotComplete) {
					if (!$max_attemp_reached) {
						echo $restartButtonHtml;
					}
				}
				else if ($isEnd) {
					if ($isAboveStudent) {
						if(!$max_attemp_reached) {
							echo $restartButtonHtml;
						}
					} 
					else if ($mustPlayUntilComplete) {
						if ($incomplete) {
							if(!$max_attemp_reached)
								echo $restartButtonHtml;
						} 
						else {
							echo Yii::t('test', '_TEST_COMPLETED');
						}
					} 
					else if ($hasNULLPrerequisite) {
						if ($isNotPassedAndNotValid) {
							if(!$max_attemp_reached) {
								echo $restartButtonHtml;
							}	
						} 
						else {
							echo Yii::t('test', '_TEST_COMPLETED');
						}
					} 
					else {
						if(!$max_attemp_reached) {
							echo '<div class="row"><div class="col-md-12 text-center">';
							echo $restartButtonHtml;
							echo '</div></div>';
						}	
					}
	
				}	
				else {
					if ($trackInfo) $trackInfo->reset();
					if (!$max_attemp_reached) {
						echo '<div class="row"><div class="col-md-12 text-center">';
						echo $beginButtonHtml;
						echo '</div></div>';
					}
				}
				
				
				if ($isEnd && $showResult && $hasAnswers) {
					echo '<div class="row"><div class="col-md-12 text-center">';
					echo $reviewButtonHtml;
					echo '</div></div>';
				}
					
				
			?>
		
		
			<?php echo CHtml::closeTag('form'); ?>	
		<?php endif; ?>
		<!-- FORM:END -->
		
		
	</div></div>
	
	
	
</div>
<script>
	$('#restart, #begin, #continue').click(function(e) {
        window.parent.postMessage({
            type: 'testStatus',
            status: 'in_progress'
        }, '*');

		var formData = $(this).closest('form').serializeArray();
		formData.push({ name: this.name, value: this.value});

		$.ajax({
				url: $('#play-test-form').attr('action'),
				beforeSend: function( xhr ) {
					/* Start Loader */
					window.top.postMessage('enable', '*');
				},
                dataType: 'JSON',
				type: 'POST',
				data: formData
			}).done(function(o) {
				if (o.success) {
					/* Stop Loader */
					window.top.postMessage({
						event_id: 'disable',
						data: {
							url: o.data.redirectUrl
						}
					}, '*');
				}
			}).fail(function(error){
			    console.log(error);
        });
			e.stopPropagation();
			return false;
		});
</script>