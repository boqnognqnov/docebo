<?php

	$points = $totalUserScore + $trackModel->bonus_score;
	$requiredPoints = $testModel->point_required;
	$testPassed = ($points >= $testModel->point_required) ? true : false;
	$testNotPassed = $testFailed == LearningTesttrack::STATUS_NOT_PASSED;
	$suspended = false;
	if ($testModel->use_suspension && $testModel->max_attempt > 0 && $testModel->suspension_num_hours > 0 && !is_null($trackModel)) {
		$current_time = Yii::app()->localtime->getUTCNow();
		$suspendedUntil = Yii::app()->localtime->fromLocalDateTime($trackModel->suspended_until);
		if(strtotime($current_time) < strtotime($suspendedUntil))
			$suspended = true;
	}
	$maxAttemptsReached = false;
	if($testModel->max_attempt > 0 && ($testModel->max_attempt - $trackModel->number_of_attempt <= 0))
		$maxAttemptsReached = true;
	
	$restartButtonHtml 			= '<div class="row button-row"><div class="col-md-12 text-center">' . CHtml::submitButton(Yii::t('test', '_TEST_RESTART'), 		array('name' => 'restart', 'id' => 'restart', 'class' => 'btn btn-success btn-lg col-md-12 col-xs-12')) . '</div></div>';
	$reviewButtonHtml  			= '<div class="row button-row"><div class="col-md-12 text-center">' . CHtml::submitButton(Yii::t('test', '_TEST_SHOW_REVIEW'), 	array('name' => 'show_review', 'id' => 'show-review', 'class' => 'btn btn-primary btn-lg col-md-12 col-xs-12')) . '</div></div>';
	$launchButtonHtml 			= '<div class="row button-row"><div class="col-md-12 text-center">' . CHtml::submitButton(Yii::t('test', 'Go back'), 	array('name' => 'launch_test', 'id' => 'launch-test-btn', 'class' => 'btn btn-default btn-lg col-md-12 col-xs-12')) . '</div></div>';
	
	$formAction = Docebo::createAbsoluteLmsUrl('//mobileapp/test/play', array(
			'id_object' 	=> $loModel->idOrg,
			'course_id' 	=> $loModel->idCourse,
	));


	$textStatusClass = $testNotPassed ?  "text-status-bad" : "text-status-good";
	
	switch ($testModel->point_type) {
		case LearningTest::POINT_TYPE_NORMAL :
			$showMaxScore = $totalMaxScore && $testModel->order_type < 2;
			$maxScore = $totalMaxScore;
			$percentSign = '';    
			break;
	
		case LearningTest::POINT_TYPE_PERCENT :
			$showMaxScore = true;
			$maxScore = 100; 
			$percentSign = '%';
			break;
		
	}

	echo CHtml::form($formAction, 'POST', array());
	
	echo CHtml::hiddenField('id_test', $testModel->idTest);
	
		

?>


<div class="test-launch-container results-container container-fluid">

	<div class="row">
		<div class="col-md-12 col-xs-12">
			<h4 class="test-title"><?= $testModel->title ?></h4>
		</div>
	</div>

	<div class="row test-info-wrapper"><div class="col-md-12 col-xs-12">
	
		<div class="row item-counter text-uppercase">
			<div class="col-md-6 col-xs-6">
				Test result
			</div>
		</div>
		
		<div class="row test-completion-block text-uppercase">
			<div class="col-md-12 col-xs-12">
				<div class="row">
					<div class="text-left col-md-6 col-xs-6">
						<strong>
						Your<br>score
						</strong>
					</div>
					<div class="text-right col-md-6 col-xs-6 score-numbers">
						<span class="<?= $textStatusClass ?>"><strong><?= $points ?></strong></span><span><?= $showMaxScore ? '/' . $maxScore : '' ?>&nbsp;<?= $percentSign ?></span>
					</div>
				</div>
			
				<div class="row">
					<div class="text-left col-md-6 col-xs-6">
						Minimum<br>score
					</div>
					<div class="text-right col-md-6 col-xs-6 score-numbers">
						<span class="text-status-good"><strong><?= $requiredPoints ?></strong></span><span><?= $showMaxScore ? '/' . $maxScore : '' ?>&nbsp;<?= $percentSign ?></span>
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="row test-success-note">
			<div class="col-md-12 col-xs-12">
				<div class="pull-left success-icon">
					<?= $testNotPassed ? '<span class="p-sprite lo-failed-icon-big"></span>' : '<span class="p-sprite lo-passed-icon-big"></span>' ?>
				</div>
				<div class="success-text">
					<?=  $testNotPassed ? 
						  '<span class="">' . Yii::t('test', 'You did not pass the test') . '</span>'  
						: '<span class="">' . Yii::t('test', 'You passed the test') . '</span>' ?>
				</div>
			</div>
		</div>
		
		
		<?php
			if(!$suspended && !$maxAttemptsReached)
				echo $restartButtonHtml;
			echo $reviewButtonHtml;
			// echo $launchButtonHtml;
		?>		
	
	
	</div>
	
	
</div>
<script>
	var	passed = '<?=$testPassed?>';
	window.parent.postMessage({
		type: 'testStatus',
		status: ( passed ? true : false)
	}, '*');
</script>

<?php 
	echo CHtml::closeTag('form'); 
?>

<script>
$(document).ready(function() {
	window.top.postMessage({
		event_id: 'disable',
		data: {
			url: 'empty'
		}
	}, '*');
});
</script>

