<?php
 
	$answeredQuestions = $trackModel->getAnsweredQuestions();
	$allQuestions = array();
	foreach ($trackModel->questsWithTitles as $i => $question) {
		$allQuestions[$question->getPrimaryKey()] = $question;
	}
	
	$totalCountQuestions = count($answeredQuestions);
	
	$testNotPassed = $testTrack->score_status == LearningTesttrack::STATUS_NOT_PASSED;	

	$NextQuestionButtonHtml 			= '<div class="row button-row"><div class="col-md-12 text-center">' . CHtml::submitButton(Yii::t('test', 'Next question'), 		array('name' => 'next_question', 'id' => 'next-question', 'class' => 'btn btn-default btn-lg col-md-12 col-xs-12')) . '</div></div>';
	$PrevQuestionButtonHtml  			= '<div class="row button-row"><div class="col-md-12 text-center">' . CHtml::submitButton(Yii::t('test', 'Previous question'), 	array('name' => 'prev_question', 'id' => 'prev-question', 'class' => 'btn btn-default btn-lg col-md-12 col-xs-12')) . '</div></div>';
	
	
	$questionIdPrefix = "question-block-entity-";	
	

?>


<div class="test-launch-container review-container container-fluid">

	<div class="row">
		<div class="col-md-12 col-xs-12">
			<h4 class="test-title"><?= $testModel->title ?></h4>
		</div>
	</div>

	
	<div class="row test-info-wrapper"><div class="col-md-12 col-xs-12">

	
		<?php foreach ($answeredQuestions as $i => $answeredQuestion) : ?>
		
			<?php
				$question = $allQuestions[$answeredQuestion['idQuest']];
			?>
		
			<!-- Question+Answers -->
			<div id="<?= $questionIdPrefix ?><?= $i+1 ?>" class="row question-and-answers-block <?= $question->type_quest; ?>"><div class="col-md-12 col-xs-12">
		
				<div class="row item-counter">
					<div class="col-md-6 col-xs-6">
						QUESTION <?= (int) $i+1 ?>/<?= (int) $totalCountQuestions ?>
					</div>
					<div class="text-right col-md-6 col-xs-6">
						
					</div>
				</div>
				
				<div class="row question-title">
					<div class="col-md-12 col-xs-12">
						<?php if($question->type_quest == LearningTestquest::QUESTION_TYPE_FITB){ ?>
							<p><?php
								$questionPlayer = CQuestionComponent::getQuestionManager($question->type_quest);
								echo $questionPlayer->applyTransformation('review', $question->title_quest, array(
									'idQuestion' => $question->idQuest,
									'idTrack' => $trackModel->idTrack,
									'questionManager' => $questionPlayer
								));

								?></p>
						<?php }else{ ?>
							<p><?= $question->title_quest ?></p>
						<?php } ?>
					</div>
				</div>
				
				<div class="row">
					<div class="container-fluid">
						<div class="col-md-12 col-xs-12 review-answers-block">
							<div class="row">
								<div class="col-md-12 col-xs-12">
								<?php
								$_showCorrectAnswers = $testModel->correctAnswersVisibleOnReview($idUser);
								$_showAnswers = $testModel->answersVisibleOnReview($idUser);

								$this->widget('player.components.widgets.TestReportsAnswerFormatter', array(
									'questionType' => $question->type_quest,
									'questId' => $question->idQuest,
									'trackId' => $trackModel->idTrack,
									'testId' => $trackModel->idTest,
									'userId' => $idUser,
									'isTeacher' => false,
									'showCorrectAnswers' => $_showCorrectAnswers,
									'showAnswers' => $_showAnswers
								));
								?>
								</div>
							</div>
						</div>
					</div>	
				</div>
			
			
			</div></div>
			<!-- Question+Answers:END -->
			
		<?php endforeach; ?>
		
		<?php 
			echo $NextQuestionButtonHtml;
			echo $PrevQuestionButtonHtml;
		?>
		
				
	</div></div>
	
	
</div>			


<script type="text/javascript">

	var totalCountQuestions 	= <?= (int) $totalCountQuestions ?>;
	var idPrefix 				= '<?= $questionIdPrefix ?>';
	var currentQuestionIndex 	= 1; 

	/**
	 * Hide all questions and then show current one
	 */
	function updateQuestion() {
		$('[id^="'+idPrefix+'"]').hide();
		$("#"+idPrefix+currentQuestionIndex).show();

		if (currentQuestionIndex == 1) {
			$('#prev-question').addClass('disabled');
		}
		else {
			$('#prev-question').removeClass('disabled');
		}

		if (currentQuestionIndex == totalCountQuestions) {
			$('#next-question').addClass('disabled');
		}
		else {
			$('#next-question').removeClass('disabled');
		}
		
		
	}

	updateQuestion();

	$('#next-question').on('click', function(){
		if ((currentQuestionIndex + 1) <= totalCountQuestions) {
			currentQuestionIndex = currentQuestionIndex + 1;
		}
		updateQuestion();
	});

	$('#prev-question').on('click', function(){
		if ((currentQuestionIndex - 1) > 0) {
			currentQuestionIndex = currentQuestionIndex - 1;
		}
		updateQuestion();
	});
	
	
		
</script>