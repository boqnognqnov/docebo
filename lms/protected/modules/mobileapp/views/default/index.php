<?php

	$forceDesktopUrl = Docebo::createAbsoluteLmsUrl('//mobileapp/default/index', array('force_desktop' => 1));
	$themeUrl = Yii::app()->theme->baseUrl;
	$smartphonnesImageUrl = $themeUrl . "/images/smartphones.png";
	
	// Default body BG color for Docebo App splashscreen
	$backgroundColor = '#0465AC';
	
	$replaceSplashScreen = false;
	$downloadAppUrl = "";
	$whitelabelMobileOption = Settings::get('whitelabel_mobile', BrandingWhiteLabelForm::MOBILE_DOCEBO_APP);
	
	if (PluginManager::isPluginActive('WhitelabelApp') && ($whitelabelMobileOption == BrandingWhiteLabelForm::MOBILE_OWN_APP)) {
		$replaceOption 		= Settings::get('whitelabel_mobile_replace_splashscreen', false);
		$splashScreenFile 	= Settings::get('whitelabel_mobile_splashscreen_file', false);
		$appleStoreUrl 		= Settings::get('whitelabel_mobile_apple_store_link', false);
		$googleStoreUrl 	= Settings::get('whitelabel_mobile_google_play_store_link', false);
			
		if ($this->mobileOs == MobileApp::MOBILE_OS_IOS) {
			$downloadAppUrl = $appleStoreUrl; 
		}
		else if ($this->mobileOs == MobileApp::MOBILE_OS_ANDROID) {
			$downloadAppUrl = $googleStoreUrl;
		}
			
		if ($replaceOption && is_numeric($splashScreenFile)) {
			$replaceSplashScreen = true;
			$backgroundColor = '#FFFFFF';
			$smartphonnesImageUrl = CoreAsset::url($splashScreenFile);
		}
	}
	else {
		if ($this->mobileOs == MobileApp::MOBILE_OS_IOS) {
			$downloadAppUrl = Yii::app()->params['docebo_mobile_app_ios_url'];
		}
		else if ($this->mobileOs == MobileApp::MOBILE_OS_ANDROID) {
			$downloadAppUrl = Yii::app()->params['docebo_mobile_app_android_url'];
		}
	}
	
	
	// Make sure there is a schema in the URL
	$downloadAppUrl = Docebo::fixUrlSchema($downloadAppUrl, 'http');
	
	
?>




<?php if ($replaceSplashScreen) : ?>

	<div class="container-fluid">
		<div class="row smartphones-image">
			<div class="text-center col-md-12 col-xs-12">
				<a href="<?= $downloadAppUrl ?>">
					<img src="<?= $smartphonnesImageUrl ?>" class="img-responsive">
				</a>
			</div>
		</div>
	</div>
	
<?php else : ?>

	<div class="container-fluid">
	
		<div class="row welcome-title">
			<div class="text-center col-md-12 col-xs-12">
				<?php if ($this->mobileOs == MobileApp::MOBILE_OS_ANDROID) : ?>
					<div class="welcome-title2">Available</div> 
				<?php else: ?>
					<div class="welcome-title1">Mobile app</div>
				<?php endif; ?>	
			</div>
		</div>
		
		
		<div class="row smartphones-image">
			<div class="text-center col-md-12 col-xs-12">
				<img src="<?= $smartphonnesImageUrl ?>" class="img-responsive">
				<div class="text-center get-app-now">
					<?php if ($this->mobileOs == MobileApp::MOBILE_OS_ANDROID) : ?>
						<a href="<?= $downloadAppUrl ?>"><strong>Get the app now!</strong></a>
					<?php else: ?>	
						<a href="<?= $downloadAppUrl ?>"><strong>Get the app now!</strong></a>
					<?php endif; ?>
				</div>
				
			</div>
		</div>
		
		<div class="row no-thanks">
			<div class="text-center col-md-12 col-xs-12">
				<!-- <a href="<?= $forceDesktopUrl ?>">No, thanks, go to the <strong>desktop version</strong></a> -->
			</div>
		</div>
		
	
	</div>


<?php endif;?>

<script type="text/javascript">

	$('body').addClass('welcome-page');
	$('body').css({'background-color' : '<?= $backgroundColor ?>'});

	function effectiveDeviceWidth() {
		  var deviceWidth = window.orientation == 0 ? window.screen.width : window.screen.height;
		  // iOS returns available pixels, Android returns pixels / pixel ratio
		  // http://www.quirksmode.org/blog/archives/2012/07/more_about_devi.html
		  if (navigator.userAgent.indexOf('Android') >= 0 && window.devicePixelRatio) {
		    deviceWidth = deviceWidth / window.devicePixelRatio;
		  }
		  return deviceWidth;
	}
</script>