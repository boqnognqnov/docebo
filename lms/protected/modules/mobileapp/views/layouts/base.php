<?php
    // If the "window" width is more than "max smartphone width", user will be redirected and forced to run the desktop version 
	$notSmartphoneUrl = Docebo::createAbsoluteLmsUrl('//mobileapp/default/index', array('not_smartphone' => 1));
?>
<!DOCTYPE html>
<html>
<?php

	$assetsUrl = Yii::app()->getModule('mobileapp')->getAssetsUrl(); 

?>

<head>

	<meta charset="utf-8">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1" />

	<!-- jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	
	<!-- Bootstrap 3.x -->
	<link href="<?php echo $assetsUrl . "/css/bootstrap.css" ?>" rel="stylesheet">	
	<link href="<?php echo $assetsUrl . "/css/bootstrap-docebo-theme.css" ?>" rel="stylesheet">
	<script src="<?php echo $assetsUrl . "/js/bootstrap.min.js" ?>" type="text/javascript"></script>
	
	<!-- Docebo  -->
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/app.js?69"></script>
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/i-sprite.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/player-sprite.css" rel="stylesheet">
	
	<!-- Mobile App  -->
	<link href="<?php echo $assetsUrl . "/css/mobileapp.css" ?>" rel="stylesheet">
	
    <!-- Template color scheme -->
<!--	<link href="--><?php //echo Docebo::createLmsUrl('site/colorschemeCss', array('time' => time())) ?><!--" rel="stylesheet" id="colorSchemeCssFile">-->

	<? if(Lang::isRTL(Yii::app()->getLanguage())): ?>
		<!-- RTL styles -->
		<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/rtl.css" rel="stylesheet">
	<? endif; ?>
	
	
<?php if (Yii::app()->user->isGuest) : ?>	
	<script type="text/javascript">
	<!--
		var maxWidthSmartphone = <?= (isset(Yii::app()->params['max_width_smartphone']) ? (int) Yii::app()->params['max_width_smartphone'] : 767) ?>; 
		var w = $(window).width();
		var h = $(window).height();
		var ww = Math.max(w,h);
			
		if (ww > maxWidthSmartphone) {
			// Redirect back to home, passing GET parameter to warn the application controller this is NOT a smartphone device
			window.location.replace('<?= $notSmartphoneUrl ?>');
		}
	//-->
	</script>
<?php endif; ?>
	
	
</head>


<body class="docebo-mobileapp">
    <?php Yii::app()->event->raise('AfterBodyTagOpen', new DEvent($this, array())); ?>
    <div id="dcb-mobile-feedback"><?php DoceboUI::printFlashMessages(); ?></div>
	<?php echo $content; ?>
	<?php $this->widget('common.widgets.GoogleAnalytics'); ?>
    <?php Yii::app()->event->raise('BeforeBodyTagClose', new DEvent($this, array())); ?>
</body>


</html>
