<?php

/**
 * A MobileApp module compagnion
 * 
 *
 */
class MobileApp extends CComponent {

	/**
	 * General mobile OS names
	 * 
	 * @var string
	 */
	const MOBILE_OS_ANDROID 	= 'android';
	const MOBILE_OS_IOS 		= 'ios';
	const MOBILE_OS_WINDOWS 	= 'windows';
	const MOBILE_OS_OTHER 		= 'other';
		
	
}