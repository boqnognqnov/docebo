<?php
/**
 * Sometimes Mobile LMS client App requires a data structure, such as 
 * 	array(
 * 		"js" 	=> array(<javascript-URL>, <javascript-URL>, <javascript-URL>, ...),
 * 		"css" 	=> array(<css-URL>, <css-URL>, <css-URL>, ...),
 * 		"html"  => "<html-body-text>"
 * 	)
 * 
 * This widget is serving this purpose, i.e. renders an HTML and returns the result HTML and JS/CSS arrays of URLs (absolute URLs!!) registered by the widget methods and views. 
 *  
 * Please do not use this calss directly, but extend it with the rendering methods and specifi attributes, if any.
 *
 */
class MobileappDataWidget extends CWidget {
	
	/**
	 * Data items returned as part of data array, @see getData()
	 * @var array
	 */
	protected $_jsArray 	= array();
	protected $_cssArray 	= array();
	protected $_html 		= '';
	
	/**
	 * Theme component
	 * @var object
	 */	
	protected $_theme;

	
	protected $_adminAssetsUrl;
	
	/**
	 * Run parent init() and render this object instance data
	 * @see CWidget::init()
	 */
	public function init() {
		parent::init();
		
		// We DO need to set the theme
		Yii::app()->themeManager->setBasePath(dirname(Yii::app()->getRequest()->getScriptFile()) . DIRECTORY_SEPARATOR . '../themes');
		Yii::app()->themeManager->setBaseUrl(Docebo::getRootBaseUrl(true) . '/themes');
		$this->_theme = Yii::app()->theme;
		$baseUrl = Yii::app()->theme->baseUrl;
		$cs = Yii::app()->getClientScript();
		
		// Register "standard" JS/CSS we use everywhere
		// $cs->registerScriptFile($baseUrl . "/js/jquery.min.js");
		$cs->registerScriptFile("https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js");

		/*
		Yii::app()->bootstrap->init();
		Yii::app()->bootstrap->registerResponsiveCss();
		*/
		
		$cs->registerCssFile(	"//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css");
		$cs->registerCssFile(	"//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css");
		$cs->registerScriptFile("//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js");
		
		$cs->registerScriptFile($baseUrl . "/js/app.js?69");
		$cs->registerCssFile(	$baseUrl . "/css/base.css");
		$cs->registerCssFile(	$baseUrl . "/css/base-responsive.css");
		//$cs->registerCssFile(	$baseUrl . "/css/admin.css");
		
		// Mobileapp module's CSS
		$cs->registerCssFile(	Yii::app()->getModule('mobileapp')->getAssetsUrl() . "/css/mobileapp.css");
		
		// Publish "admin" assets and save the path
		$this->_adminAssetsUrl 	= Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin.js'));

		
		$this->renderData();
	}
	
	/**
	 * Return data as an array
	 * 
	 * @return array
	 */
	public function getData() {
		$result = array(
				'html' 	=> $this->_html,
				'js'	=> $this->_jsArray,
				'css'	=> $this->_cssArray
		);
		return $result;
	}
	
	/**
	 * "Render" HTML and collect othe data items
	 */
	protected function renderData() {
		// Axtend the class and implement your data rendering logic
		// At the end of the method, just set these class attributes  
		$this->_html			= '';
		$this->_jsArray 		= array();
		$this->_cssArray 		= array();
		
	}
	
	
}