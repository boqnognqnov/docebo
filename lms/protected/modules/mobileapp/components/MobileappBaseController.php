<?php
/**
 * 
 * 
 *
 */
class MobileappBaseController extends Controller {
	
	/**
	 * Keep general Mobile OS type
	 * @var string
	 */
	public $mobileOs = false;
	
	/**
	 * @see Controller::init()
	 */
	public function init() {
		
		parent::init();
		
		$this->mobileOs = $this->getMobileOs();
	}
	
	
	/**
	 * Gets detected Mobile OS
	 */
	public function getMobileOs() {
		$md = new Mobile_Detect();
		$result = '';
		
		if ($md->isIOS()) {
			$result = MobileApp::MOBILE_OS_IOS;
		}
		else if ($md->isAndroidOs()) {
			$result = MobileApp::MOBILE_OS_ANDROID;
		}
		else if ($md->isWindowsMobileOS()) {
			$result = MobileApp::MOBILE_OS_WINDOWS;
		}
		else {
			$result = MobileApp::MOBILE_OS_OTHER;
		}
		
		return $result;
		
	}
	
	
}