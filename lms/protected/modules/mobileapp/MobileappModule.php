<?php

class MobileappModule extends CWebModule
{
	
	public $forceCopyAssets = false;
	
	
	
	/**
	 * Module specific assets URL, where JS/CSS are web-published (taken from mudule's "assets" folder
	 * @var string
	 */
 	protected $_assetsUrl;
	
 	/**
 	 * 
 	 * @see CModule::init()
 	 */
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'mobileapp.models.*',
			'mobileapp.components.*',
		));
	}

	/**
	 * 
	 * @see CWebModule::beforeControllerAction()
	 */
	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
	
	
	
	/**
	 * Get module assets url
	 * @return string
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('mobileapp.assets'));
		}
		return $this->_assetsUrl;
	}
	
	
}
