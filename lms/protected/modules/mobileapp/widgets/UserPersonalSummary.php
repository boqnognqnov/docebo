<?php
/**
 * Render and return data for User personal summary report, suitable for Mobile Applications
 * 
 * Usage:
 * 		$widget = Yii::app()->controller->createWidget('mobileapp.widgets.UserPersonalSummary', array('idUser' => $idUser));
 * 		$dataArray = $widget->getData();
 * 
 */
class UserPersonalSummary extends MobileappDataWidget {
	
	/**
	 * User ID 
	 * @var integer
	 */
	public $idUser = null;
	
	/**
	 * User model
	 * @var CoreUser
	 */
	protected $userModel=null;

	/**
	 * Render data
	 * 
	 * @see MobileappDataWidget::renderData()
	 */
	protected function renderData() {
		
		parent::renderData();
		
		// Register all required JS/CSS.
		// Note, this doesn't mean it is rendered (getting part of HTML)! We just make a list of what should be rendered by the consumer of this widget
		$cs = Yii::app()->getClientScript();
		
		$adminAssetsUrl = $this->_adminAssetsUrl;
		$cs->registerScriptFile(Yii::app()->getModule('player')->getAssetsUrl() . "/js/jquery.knob.js", CClientScript::POS_BEGIN);
		
		$this->userModel = CoreUser::model()->findByPk($this->idUser);
		$totalTime = LearningTracksession::model()->getUserTotalTime($this->userModel->idst);
		$subscriptionsCount 	= LearningCourseuser::getCountSubscriptions($this->userModel->idst);
		$courseSummaryList 	= LearningCourseuser::model()->getUserCourseSummary($this->userModel->idst, $subscriptionsCount);
		$enrollmens = $this->userModel->learningCourseusers;
		
		// Render HTML! JS/CSS are NOT rendered, this pretty much like renderPartial() with NO processOutput.
		$html = $this->render("userPersonalSummary/index", array(
				'userModel' 			=> $this->userModel,
				'totalTime' 			=> $totalTime,
				'enrollmens'			=> $enrollmens,
				'subscriptionsCount'	=> $subscriptionsCount,
				'courseSummaryList'		=> $courseSummaryList,
		),true);
		
		// Collect all JS/CSS abslute URL registered during HTML rendering		
		$clientResources = Yii::app()->getClientScript()->getClientResourcesUrls();
		
		$this->_html			= $this->_html . $html;
		$this->_jsArray 		= array_merge($this->_jsArray, $clientResources['js']);
		$this->_cssArray 		= array_merge($this->_cssArray, $clientResources['css']);
		
	}
	
}