<?php

	$knobWidth = 70;
	$knobHeight = 70;
	$knobBgColor = "#FFFFFF";
	$knobTickness = 0.5;
	$knobColorNotStarted 	= "#003D6A";
	$knobColorInProgress 	= "#0465AC";
	$knobColorCompleted 	= "#52A1DC";

?>

<div class="user-report container-fluid">

	<div class="row user-header">
		<div class="pull-left">
			<?php
				echo CHtml::image(
					$userModel->getAvatarImage(true),
					$userModel->firstname . " " . $userModel->lastname,
					array('class' => 'user-avatar')
				);
			?>
		</div>
		<div class="">
			<div class="user-name"><?=  $userModel->firstname . " " . $userModel->lastname ?></div>
			<div class="user-email">
				<?= $userModel->email ?>
			</div>
		</div>
	</div>


	<div class="row text-uppercase section-title">
		<div class="col-md-12">
			<?= Yii::t('standard', '_STATISTICS')?>
		</div>
	</div>


	<div class="row data-row odd">
		<div class="col-md-8 col-xs-7">
			<?= Yii::t('report', '_DATE_INSCR')?>
		</div>
		<div class="col-md-4 col-xs-5 text-right data-column">
			<?= Yii::app()->localtime->toLocalDate($userModel->register_date) ?>
		</div>
	</div>


	<div class="row data-row even">
		<div class="col-md-8 col-xs-7">
			<?= Yii::t('standard', '_DATE_LAST_ACCESS')?>
		</div>
		<div class="col-md-4 col-xs-5 text-right data-column">
			<?= Yii::app()->localtime->toLocalDate($userModel->lastenter) ?>
		</div>
	</div>



	<div class="row data-row odd">
		<div class="col-md-8 col-xs-7">
			<?= Yii::t('standard', '_TOTAL_TIME')?>
		</div>
		<div class="col-md-4 col-xs-5 text-right data-column">
			<?= $totalTime ?>
		</div>
	</div>


	<div class="row data-row even">
		<div class="col-md-8 col-xs-7">
			<?= Yii::t('dashboard', '_ACTIVE_COURSE')?>
		</div>
		<div class="col-md-4 col-xs-5 text-right data-column">
			<?= $subscriptionsCount ?>
		</div>
	</div>


	<div class="row data-row knobs">
		<div class="col-md-4 col-xs-4">
			<div class="text-center user-courses-knob">
				<input data-role="none" type="text" data-min="0" data-max="<?= $subscriptionsCount ?>" id="knob-not-started" value="<?= $courseSummaryList[0]['count'] ?>" />
			</div>
			<div class="chart-legend text-uppercase text-center"><?= Yii::t('standard', 'To start'); ?></div>
		</div>
		<div class="col-md-4 col-xs-4">
			<div class="text-center user-courses-knob">
				<input data-role="none" type="text" data-min="0" data-max="<?= $subscriptionsCount ?>" id="knob-in-progress" value="<?= $courseSummaryList[1]['count'] ?>" />
			</div>
			<div class="chart-legend text-uppercase text-center"><?= Yii::t('standard', '_USER_STATUS_BEGIN');?></div>
		</div>
		<div class="col-md-4 col-xs-4">
			<div class="text-center user-courses-knob">
				<input data-role="none" type="text" data-min="0" data-max="<?= $subscriptionsCount ?>" id="knob-completed" value="<?= $courseSummaryList[2]['count'] ?>" />
			</div>
			<div class="chart-legend text-uppercase text-center"><?= Yii::t('standard', '_USER_STATUS_END');?></div>
		</div>
	</div>


	<div class="row text-uppercase section-title">
		<div class="col-md-12 col-xs-12">
			<?= Yii::t('standard', '_COURSES')?>
		</div>
	</div>


	<?php foreach($enrollmens as $enrollmen) : ?>

		<div class="row data-row">
			<div class="col-md-12 col-xs-12" style="text-overflow:ellipsis; overflow: hidden;  white-space: nowrap;">
				<?= $enrollmen->course->name ?>
			</div>
		</div>

	<?php endforeach;?>



</div>


<script type="text/javascript">

	var knobWidth 			= '<?= $knobWidth ?>';
	var knobHeight 			= '<?= $knobHeight ?>';
	var knobColorNotStarted = '<?= $knobColorNotStarted ?>';
	var knobColorInProgress = '<?= $knobColorInProgress ?>';
	var knobColorCompleted 	= '<?= $knobColorCompleted ?>';
	var knobBgColor			= '<?= $knobBgColor ?>';
	var knobTickness		= '<?= $knobTickness ?>';

	$(function(){

		$("#knob-not-started").knob({
			'readOnly'	: true,
			'fgColor'	: knobColorNotStarted,
			'bgColor'	: knobBgColor,
			'width'		: knobWidth,
			'height'	: knobHeight,
			'thickness'	: knobTickness
		});


		$("#knob-in-progress").knob({
			'readOnly'	: true,
			'fgColor'	: knobColorInProgress,
			'bgColor'	: knobBgColor,
			'width'		: knobWidth,
			'height'	: knobHeight,
			'thickness'	: knobTickness
		});


		$("#knob-completed").knob({
			'readOnly'	: true,
			'fgColor'	: knobColorCompleted,
			'bgColor'	: knobBgColor,
			'width'		: knobWidth,
			'height'	: knobHeight,
			'thickness'	: knobTickness
		});




	});
</script>