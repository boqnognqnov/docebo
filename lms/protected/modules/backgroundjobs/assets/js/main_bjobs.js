/**
 * Created by Georgi on 2.6.2016 г..
 */
/*var scripts = document.getElementsByTagName("script");
var currentScriptPath = scripts[scripts.length - 1].src;
var currentScriptPathArray = currentScriptPath.split("/");
currentScriptPathArray.pop();*/
var bjobsDirectory = bjobsScriptPath.replace('../', '/');
require.config({
    baseUrl: bjobsDirectory, //app7020Options.assetUrl + '/',
    enforceDefine: false,
    paths: {
        moment: bjobsDirectory + '/js/moment',
        bjobs: bjobsDirectory + '/js/backgroundjobs',
        eio: bjobsDirectory + '/js/engine.io'
    },
    shim: {
        'bjobs': {
            deps: ['moment', 'eio'],
            exports: "BGJobsLoader"
        },
        'moment': {
            exports: "moment"
        },
        'eio': {
            exports: "eio"
        }
    },
    priority: [
        "moment",
        'eio',
        "backgroundjobs"
    ]
});
require([
    'moment',
    'eio',
    'bjobs'
], function () {

});
