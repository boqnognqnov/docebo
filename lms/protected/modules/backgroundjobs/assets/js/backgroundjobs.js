/**
 * Created by NikVas on 11-Mar-16.
 */

(function($){


    BGJobsLoader = function (options) {
        this.init(options);
    };

    BGJobsLoader.prototype = {
         userId                 : '',
         domain                 : '',//'<?=Docebo::getOriginalDomain();?>';
         session                : '',//'<?=Yii::app()->request->cookies['docebo_session']?>';
         wsHost                 : '',
         wsPort                 : '',
         wsUseHttps				: false,
         // Job data
         redisHost              : '',
         redisPort              : '',
         redisJobDb             : 0,
         // Sessions
         redisSessionHost       : '',
         redisSessionPort   	: '',
         redisSessionDb     	: 0,
        		 
         notificationUrl        : '',//'<?=Yii::app()->createUrl('/backgroundjobs/default/notify')?>';
         abortUrl               : '',//'<?=Yii::app()->createUrl('/backgroundjobs/default/delete')?>'
         downloadUrl            : '',//'<?=Yii::app()->createUrl('/backgroundjobs/default/downloaderrors')?>'
         isWidget               : false,
         //counting               : false,
         isPU                   : false,
         finishedInit           : false,
         BackgroundJobs         : [],
         AllJobsDiv             : 'div#alljobs',
         MyJobsDiv              : 'div#myjobs',
         InboxJobs              : 'div#inbox-bg-jobs',
         selectorName           : '.job-info .job-summary strong',
         selectorAuthor         : '.job-info .job-author',
         authorTranslation      : Yii.t('backgroundjobs', 'Started by {user_name} on {start_time}'),
         selectorRunningTime    : '.job-progress .job-running-time',
         runTimeTranslation     : Yii.t('backgroundjobs', 'Running time') + ': ',
         abortedRuntimeTrans    : Yii.t('backgroundjobs','Aborted on {abort_time} by {user_name}'),
         selectorNotify         : '.job-progress .job-notify',
         notifyMeTranslation    : '<a data-id="{hash_id}">' + Yii.t('backgroundjobs', 'Notify me when finished') + '</a>',
         notifiedTranslation    : '<i class="fa fa-check"></i>' + Yii.t('backgroundjobs', "You'll be notified when finished"),
         noRunningJobsTrans     : '<div class="none-running text-center">'+Yii.t('backgroundjobs', 'You don’t have any running jobs')+'</div>',
         noBGJobsTrans          : '<div class="none-running text-center">'+Yii.t('backgroundjobs', 'You don’t have any background jobs')+'</div>',
         selectorProgressBar    : '.job-progress .job-progress-bar .job-progress-green',
         selectorPercent        : '.job-progress .job-percentage strong',
         selectorActions        : '.job-actions',
         actionStop             : '<a data-id="{hash_id}" class="abort-action" ><i class="job-stop-icon fa fa-times"></i></a>',
         actionError            : '<i class="job-error-icon fa fa-exclamation-triangle"></i><a data-id="{hash_id}" class="list-of-errors">({errorCount})</a>',
         jobTemplate            : '<div id="{jobid}" class="background-job"><div class="job-info horizontal"><div class="job-summary"><strong></strong></div><div class="job-author"></div></div><div class="job-progress horizontal"><div class="job-running-time"></div><div class="job-notify"></div><div class="job-progress-bar"><div class="job-progress-green"></div></div><div class="job-percentage"><strong></strong></div></div><div class="job-actions horizontal"></div></div>',
         dateFormat             : 'YYYY-MM-DD HH:mm:ss',
         timeOffset             : '+00:00',

         socket                 : '',

        openPopup               : false,

        init: function(options){
            this.domain             = options.domain;
            this.session            = options.session;
            this.notificationUrl    = options.notificationUrl;
            this.abortUrl           = options.abortUrl;
            this.downloadUrl        = options.downloadUrl;
            this.wsHost             = options.wsHost;//192.168.0.201
            this.wsPort             = options.wsPort;//33380
            this.wsUseHttps			= options.wsUseHttps;
            
            // Jobs Data
            this.redisHost          = options.redisHost;
            this.redisPort          = options.redisPort;
            this.redisJobDb         = options.redisJobDb;
            
            // PHP Sessions
            this.redisSessionHost     = options.redisSessionHost;
            this.redisSessionPort     = options.redisSessionPort;
            this.redisSessionDb       = options.redisSessionDb;
            
            this.userId             = options.userId;
            this.isWidget           = (options.isWidget!=null)? options.isWidget : false ;
            this.BackgroundJobs     = [];
            this.isPU               = (options.isPU!=null)? options.isPU : false ;
            this.sendRedisConfig    = options.sendRedisConfig;
            this.dateFormat         = (options.dateFormat)? options.dateFormat.replace(/y/g,'Y') : this.dateFormat;
            this.timeOffset         = (options.timeOffset)? options.timeOffset : this.timeOffset;
            this.openPopup          = options.openWidget;
            
            this.socket = new eio.Socket(this.resolveWsSchema() + '://'+this.wsHost+':'+this.wsPort+'/', { transports: ['websocket', 'polling'],    });            

            this.socketListener();
            var that = this;
            $(document).ready(function () {
                var thisContainer = (that.isWidget)? that.InboxJobs : that.AllJobsDiv + ' ' + that.selectorNotify + ' a,' + that.MyJobsDiv;
                $(document).on('click',thisContainer + ' ' + that.selectorNotify + ' a', function () {
                    //var jobDivId = $(this).parent().parent().parent().attr('id');
                    //var jobId = jobDivId.substring(jobDivId.indexOf('_') + 1);
                    //var type = jobDivId.substring(0, jobDivId.indexOf('_'));
                    var hash_id = $(this).data('id');
                    $.ajax({
                        url: that.notificationUrl,
                        data: {'hash_id': hash_id},
                        success: function (result) {
                            if(that.isWidget){
                                $('div#inboxBGJobs_'+hash_id+' '+that.selectorNotify).html(that.notifiedTranslation);
                            }
                            else{
                                $('div#all_'+hash_id+' '+that.selectorNotify+' ,div#my_'+hash_id+' '+that.selectorNotify).html(that.notifiedTranslation);
                            }
                        }
                    });

                });
                thisContainer = (that.isWidget)? that.InboxJobs : that.AllJobsDiv + ' ' + that.selectorActions + ' a.abort-action,' + that.MyJobsDiv;
                $(document).on('click',thisContainer + ' ' + that.selectorActions + ' a.abort-action',function(){
                    var hash_id = $(this).data('id');
                    $.ajax({
                        url: that.abortUrl,
                        data: {'hash_id': hash_id},
                        success: function (result) {
                            var thisBGJob = null;
                            var thisBGJobIndex = 0;
                            that.BackgroundJobs.forEach(function (element, index) {
                                if (element.hash_id == hash_id) {
                                    thisBGJob = element;
                                    thisBGJobIndex = index;
                                }
                            });

                            //Selectors
                            var selectorPrefixInbox = 'div#inboxBGJobs_' + hash_id + ' ';
                            var selectorPrefixAll   = 'div#all_' + hash_id + ' ';
                            var selectorPrefixMy    = 'div#my_' + hash_id + ' ';
                            var selectorNotify      = (that.isWidget)?   selectorPrefixInbox+that.selectorNotify            :   selectorPrefixAll + that.selectorNotify      +   ' ,' + selectorPrefixMy + that.selectorNotify   ;
                            var selectorActions     = (that.isWidget)?   selectorPrefixInbox+that.selectorActions           :   selectorPrefixAll + that.selectorActions     +   ' ,' + selectorPrefixMy + that.selectorActions   ;
                            var selectorRunningTime = (that.isWidget)?   selectorPrefixInbox + that.selectorRunningTime     :   selectorPrefixAll + that.selectorRunningTime +   ' ,' + selectorPrefixMy + that.selectorRunningTime   ;

                            //Data for the aborted job
                            var actionsHtml = '';
                            if(thisBGJob.errors && thisBGJob.errors!=0){
                                actionsHtml = that.actionError.replace('{errorCount}', thisBGJob.errors);
                                actionsHtml = actionsHtml.replace('{hash_id}', thisBGJob.hash_id);
                            }
                            var currAbortAuthorTranslation = that.getJobAbortDateString(thisBGJob, true);

                            //Showing the new data for the aborted job
                            $(selectorNotify).html('');
                            $(selectorActions).html(actionsHtml);
                            $(selectorRunningTime).text(currAbortAuthorTranslation);
                            if(that.isWidget){
                                that.BackgroundJobs.splice(thisBGJobIndex,1);
                                $(selectorPrefixInbox).fadeOut(400,function(){
                                    if(that.BackgroundJobs.length===0){
                                        $('.tasks-viewer .count').hide();//text(that.BackgroundJobs.length);
                                        $(that.InboxJobs).css({
                                            'display': 'block',
                                            'margin-left': '20px',
                                            'margin-top': '20px'
                                        });
                                        $(that.InboxJobs).html(that.noRunningJobsTrans);
                                        that.socket.close();
                                    }
                                });
                            }
                        }
                    });
                });
                thisContainer = (that.isWidget)? that.InboxJobs : that.AllJobsDiv + ' ' + that.selectorActions + ' a.list-of-errors,' + that.MyJobsDiv;
                $(document).on('click',thisContainer + ' ' + that.selectorActions + ' a.list-of-errors',function(){
                    var hash_id = $(this).data('id');
                    window.location = that.downloadUrl + '&hash_id=' + hash_id;
                });
            });
        },

        getJobAbortDateString: function(bgJob, clickingOnAbort){
            if(!clickingOnAbort){
                clickingOnAbort = false;
            }
            var dateObj = new Date(bgJob.endDate*1000);
            var abortDateStr = dateObj.toISOString().replace('T',' ');
            abortDateStr = abortDateStr.substring(0,abortDateStr.indexOf('.'));
            abortDateStr = moment.utc(abortDateStr).utcOffset(this.timeOffset).format(this.dateFormat);
            var abortAuthorName = (bgJob.abortAuthorId == this.userId || clickingOnAbort) ? Yii.t('backgroundjobs', 'Me') : bgJob.abortAuthorName;
            var currAbortAuthorTranslation = this.abortedRuntimeTrans.replace('{user_name}', abortAuthorName);
            currAbortAuthorTranslation = currAbortAuthorTranslation.replace('{abort_time}', abortDateStr);

            return currAbortAuthorTranslation;
        },

        socketListener: function(){
            var that = this;

            that.socket.on('open', function () {
                //console.log('Opened');

                var filter = (that.isWidget)? 'author='+that.userId+';aborted=0;percent!=100' : null;
                var updateFilter = (that.isWidget)? 'author='+that.userId+';aborted=0;' : null;
                if(!that.isWidget && that.isPU){
                    filter = 'author='+that.userId;
                }
                var config = {};
                if (that.sendRedisConfig) {
	                config =  {
	                    redisHost: that.redisHost,
	                    redisPort: that.redisPort,
	                    redisJobDb: that.redisJobDb,
	                    redisSessionHost: that.redisSessionHost,
	                    redisSessionPort: that.redisSessionPort,
	                    redisSessionDb: that.redisSessionDb
	                };
                }
                that.socket.send(JSON.stringify({
                    domain: that.domain,
                    session: that.session,
                    config: config,
                    filter: filter,
                    updateFilter: updateFilter
                }), {}, function () {
                    that.socket.on('message', function (jsonStr) {
                        //console.log(jsonStr);
                        if(jsonStr=='end'){
                            if(that.BackgroundJobs.length===0){
                                if(that.isWidget) {
                                    $('.tasks-viewer .count').hide();//text(that.BackgroundJobs.length);
                                    $(that.InboxJobs).css({
                                        'display': 'block',
                                        'margin-left': '20px',
                                        'margin-top': '20px'
                                    });
                                    $(that.InboxJobs).html(that.noRunningJobsTrans);
                                    that.socket.close();
                                }else{
                                    $(that.AllJobsDiv).html(that.noBGJobsTrans);
                                    $(that.MyJobsDiv).html(that.noBGJobsTrans);
                                }
                            }
                            if(that.isWidget) {
                                that.finishedInit = true;
                            }
                        }else{
                            if(jsonStr!='end') {
                                var data = JSON.parse(jsonStr);
                                var job = data;

                                var name = JSON.parse(job.name);
                                var authorId = job.author;
                                var authorName = job.authorName;
                                var startDate = job.date_created;
                                var endDate   = (job.date_finished!=null)? job.date_finished : parseInt($.now() / 1000) ;
                                var dateObj = new Date(startDate*1000);

                                var translatedName = Yii.t(name.module, name.value, name.params);
                                var elapsedTime = endDate - startDate;
                                var hours = parseInt(elapsedTime / 3600);
                                var minutes = parseInt((elapsedTime - hours * 3600) / 60);
                                var seconds = parseInt((elapsedTime - hours * 3600 - minutes * 60));
                                var elapsedTimeStr = hours + 'h ' + minutes + 'm ' + seconds + 's';
                                var percent = job.percent;

                                var isFound = false;
                                var dataToRecord = {
                                    'hash_id': job.hash,
                                    'errors' : job.errors,
                                    'aborted' : job.aborted,
                                    'translatedName': translatedName,
                                    'authorId': authorId,
                                    'authorName': authorName,
                                    'abortAuthorId': job.abortAuthorId,
                                    'abortAuthorName': job.abortAuthorName,
                                    'notificationArr': job.notify,
                                    'endDate': endDate,
                                    //'startDateStr': startDateStr,
                                    'elapsedTimeStr': elapsedTimeStr,
                                    'percent': percent,
                                    'startDateTimeStamp': startDate,
                                    'insertAfter': '',
                                    'type': job.type,
                                    'nameParams': name.params
                                };
                                var lastSooner = -1;
                                that.BackgroundJobs.forEach(function (element, index) {
                                    if (element.hash_id == job.hash) {
                                        isFound = true;
                                        dataToRecord['startDateStr'] = element.startDateStr;
                                        dataToRecord['isFound'] = true;
                                    }
                                    if(element.startDateTimeStamp > dataToRecord.startDateTimeStamp){
                                        lastSooner = index;
                                        dataToRecord.insertAfter = element.hash_id;
                                        if(element.authorId==that.userId) {
                                            dataToRecord.insertAfterMine = element.hash_id;
                                        }
                                    }
                                });
                                if (isFound === false) {
                                    var startDateStr = dateObj.toISOString().replace('T',' ');//dateObj.getFullYear() + '-' + dateObj.getMonth() + '-' + dateObj.getDate() + ' ' + dateObj.getHours();
                                    startDateStr = startDateStr.substring(0,startDateStr.indexOf('.'));
                                    startDateStr = moment.utc(startDateStr).utcOffset(that.timeOffset).format(that.dateFormat);
                                    dataToRecord['startDateStr'] = startDateStr;
                                    dataToRecord['isFound'] = false;
                                    that.BackgroundJobs.splice(lastSooner+1,0,dataToRecord);
                                }

                                if(that.isWidget){
                                    if(that.BackgroundJobs.length>5){
                                        that.BackgroundJobs.pop();
                                    }
                                    if(that.BackgroundJobs.length>0){
                                        $('.tasks-viewer .count').show();
                                    }
                                    $('.tasks-viewer .count').text(that.BackgroundJobs.length);
                                }
                                $(document).trigger('backgroundjobUpdate', [job.hash, percent, job.type, name.params]);
                                that.showJobs(dataToRecord, that.userId);
                            }
                        }
                    });
                });

                that.socket.on('close', function () {
                    //console.log('Closing socket');
                });
            });
        },

        resolveWsSchema: function() {
            var result = "ws";
            if (this.wsUseHttps) {
            	result = "wss";
            }
            return result;
        },
        
        openSocket: function(){
            this.socket = new eio.Socket(this.resolveWsSchema() + '://'+this.wsHost+':'+this.wsPort+'/', { transports: ['websocket', 'polling'],    });            
            this.socketListener();
        },

        closeSocket: function(){
            this.socket.close();
        },

        showJobs: function (jobs, id) {
            var currentJobTemplate = this.jobTemplate;
            var author = (jobs.authorId == id) ? Yii.t('backgroundjobs', 'Me') : jobs.authorName;
            if (!jobs.isFound){
                if(this.isWidget){
                    //adding new job to widget
                    if (jobs.authorId == id) {
                        currentJobTemplate = this.jobTemplate;
                        currentJobTemplate = currentJobTemplate.replace('{jobid}', 'inboxBGJobs_' + jobs.hash_id);

                        if(this.BackgroundJobs.length > 0 || this.openPopup){
                            $(this.InboxJobs+' .none-running').remove();
                            $(this.InboxJobs).css({
                                'display': 'inline-block',
                                'margin-left': '5px',
                                'margin-top': '0px'
                            });
                            if(this.finishedInit || this.openPopup){
                                setTimeout(function() {
                                    $('.tasks-viewer .unread-tooltip').show();
                                }, 1000);
                            }
                        }

                        if(jobs.insertAfter!=null && jobs.insertAfter!=''){
                            $('#inboxBGJobs_' + jobs.insertAfter).after(currentJobTemplate);
                        }else{
                            $(this.InboxJobs).html(currentJobTemplate + $(this.InboxJobs).html());
                        }
                    }
                    $(this.InboxJobs+' .background-job').each(function(index){
                        if(index>=5){
                            $(this).hide();
                        }
                    });
                }else{
                    //adding new job to All jobs tab
                    currentJobTemplate = currentJobTemplate.replace('{jobid}', 'all_' + jobs.hash_id);
                    $(this.AllJobsDiv+' .none-running').remove();
                    if(jobs.insertAfter!=null && jobs.insertAfter!=''){
                        $('#all_' + jobs.insertAfter).after(currentJobTemplate);
                    }else{
                        $(this.AllJobsDiv).html(currentJobTemplate + $(this.AllJobsDiv).html());
                    }
                    //adding new job to My jobs tab
                    if (jobs.authorId == id) {
                        currentJobTemplate = this.jobTemplate;
                        currentJobTemplate = currentJobTemplate.replace('{jobid}', 'my_' + jobs.hash_id);
                        $(this.MyJobsDiv+' .none-running').remove();
                        if(jobs.insertAfterMine!=null && jobs.insertAfterMine!=''){
                            $('#my_' + jobs.insertAfterMine).after(currentJobTemplate);
                        }else{
                            $(this.MyJobsDiv).html(currentJobTemplate + $(this.MyJobsDiv).html());
                        }
                    }
                }
            }


            var currAuthorTranslation = this.authorTranslation.replace('{user_name}', author);
            currAuthorTranslation = currAuthorTranslation.replace('{start_time}', jobs.startDateStr);
            var arrNotify = JSON.parse(jobs.notificationArr);
            var notifyHtml = '';
            if(jobs.percent<100 && jobs.aborted!=1) {
                if (arrNotify.length > 0 && $.inArray(id, jobs.notificationArr)) {
                    notifyHtml = this.notifiedTranslation;
                } else {
                    notifyHtml = this.notifyMeTranslation.replace('{hash_id}', jobs.hash_id);
                }
            }
            var actionsHtml = '';


            if(jobs.aborted==1 || jobs.percent >= 100) {
                if (jobs.errors && jobs.errors != 0) {
                    actionsHtml = this.actionError.replace('{errorCount}', jobs.errors);
                    actionsHtml = actionsHtml.replace('{hash_id}',jobs.hash_id);
                }
            }else{
                actionsHtml = this.actionStop.replace('{hash_id}', jobs.hash_id);
            }

            if(this.isWidget){
                if(jobs.authorId == id) {
                    var currInboxJobDiv = '#inboxBGJobs_' + jobs.hash_id + ' ';
                    $(currInboxJobDiv + this.selectorName).text(jobs.translatedName);
                    $(currInboxJobDiv + this.selectorAuthor).text(currAuthorTranslation);
                    if(jobs.aborted != '1') {
                        $(currInboxJobDiv + this.selectorRunningTime).text(this.runTimeTranslation + jobs.elapsedTimeStr);
                    }else{
                        var currAbortAuthorTranslation = this.getJobAbortDateString(jobs);
                        $(currInboxJobDiv + this.selectorRunningTime).text(currAbortAuthorTranslation);
                    }
                    $(currInboxJobDiv + this.selectorNotify).html(notifyHtml);
                    $(currInboxJobDiv + this.selectorPercent).text(jobs.percent + '%');
                    $(currInboxJobDiv + this.selectorProgressBar).css('width', jobs.percent + '%');
                    $(currInboxJobDiv + this.selectorActions).html(actionsHtml);

                    if(jobs.percent==100){
                        $(currInboxJobDiv + this.selectorPercent).css('color','#5EBE5D');

                        var that = this;
                        $(currInboxJobDiv).fadeOut(700,function(){
                            that.BackgroundJobs.forEach(function (element, index) {
                                if (element.hash_id == jobs.hash_id) {
                                    that.BackgroundJobs.splice(index,1);
                                }
                            });

                            $('.tasks-viewer .count').text(that.BackgroundJobs.length);
                            if(that.BackgroundJobs.length===0){
                                $('.tasks-viewer .count').hide();//text(that.BackgroundJobs.length);
                                $(that.InboxJobs).css({
                                    'display': 'block',
                                    'margin-left': '20px',
                                    'margin-top': '20px'
                                });
                                $(that.InboxJobs).html(that.noRunningJobsTrans);
                                $('.tasks-viewer .unread-tooltip').hide();
                                that.socket.close();
                            }
                        });


                        $(document).trigger('backgroundjobFinished', [jobs.hash, jobs.percent, jobs.type, jobs.nameParams]);
                    }
                }
            }else{
                var currAllJobDiv = '#all_' + jobs.hash_id + ' ';
                var currMyJobDiv = '#my_' + jobs.hash_id + ' ';

                $(currAllJobDiv + this.selectorName + ',' + currMyJobDiv + this.selectorName).text(jobs.translatedName);
                $(currAllJobDiv + this.selectorAuthor + ',' + currMyJobDiv + this.selectorAuthor).text(currAuthorTranslation);
                if(jobs.aborted != '1'){
                    $(currAllJobDiv + this.selectorRunningTime + ',' + currMyJobDiv + this.selectorRunningTime).text(this.runTimeTranslation + jobs.elapsedTimeStr);
                }else{
                    var currAbortAuthorTranslation = this.getJobAbortDateString(jobs);
                    $(currAllJobDiv + this.selectorRunningTime + ',' + currMyJobDiv + this.selectorRunningTime).text(currAbortAuthorTranslation);
                }
                $(currAllJobDiv + this.selectorNotify + ',' + currMyJobDiv + this.selectorNotify).html(notifyHtml);
                $(currAllJobDiv + this.selectorPercent + ',' + currMyJobDiv + this.selectorPercent).text(jobs.percent + '%');
                //console.log(jobs.percent);
                if(jobs.percent=='100'){
                    $(currAllJobDiv + this.selectorPercent + ',' + currMyJobDiv + this.selectorPercent).css('color','#5EBE5D');
                }
                $(currAllJobDiv + this.selectorProgressBar + ',' + currMyJobDiv + this.selectorProgressBar).css('width', jobs.percent + '%');
                $(currAllJobDiv + this.selectorActions + ',' + currMyJobDiv + this.selectorActions).html(actionsHtml);
            }

        }
    };
})(jQuery);