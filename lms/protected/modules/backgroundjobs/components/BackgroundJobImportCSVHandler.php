<?php
/**
 * Created by PhpStorm.
 * User: NikVasilev
 * Date: 29.3.2016 г.
 * Time: 12:03
 */
class BackgroundJobImportCSVHandler extends BackgroundJobHandler {

    public $structure       =   self::DATA_STRUCTURE_CUSTOM;
    public $storage         =   self::STORAGE_S3;

    public function getCustomData()
    {
        $domainCode = preg_replace('/(\W)/is', '_', Docebo::getOriginalDomain());
        $splitDomainCode = $domainCode[0] . "/" . $domainCode[1] . "/" . $domainCode;
        $path  = _base_ . DIRECTORY_SEPARATOR . "lmstmp" 	. DIRECTORY_SEPARATOR . $splitDomainCode . DIRECTORY_SEPARATOR . "upload_tmp";
        $files = FileHelper::findFiles($path ,array('fileTypes'=>array('csv'), 'absolutePaths'=>false));

        $filename           =   $this->getParam('filename',false);
        $charset            =   $this->getParam('charset','UTF-8');
        $separator          =   $this->getParam('separator','auto');
        $manualSeparator    =   $this->getParam('manualSeparator',null);
        $firstRowAsHeader   =   $this->getParam('firstRowAsHeader',true);

        if(!$filename)
            return array();

        //If the file doesn't exist, copy it to the upload_tmp directory
        if(!in_array($filename, $files)){
            $storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_COMMON);
            $storage->downloadAs($filename, $path . DIRECTORY_SEPARATOR . $filename, 'jobs');
        }

        $chunkSize = intval($this->_jobParams['chunkSize']);
        $offset = $this->_jobParams['currentChunk'] * $chunkSize;
        //$limit = $offset + $chunkSize;

        $delimiter = '';
        switch ($separator) {
            case 'comma':
                $delimiter = ',';
                break;
            case 'semicolon':
                $delimiter = ';';
                break;
            case 'manual':
                $delimiter = $manualSeparator;
                break;
            case 'auto':
                $delimiter = 'auto';
                break;
        }
        //----------------opening file--------------
        $csvFile = new QsCsvFile(array(
            'path' => $path . DIRECTORY_SEPARATOR . $filename,
            'charset' => $charset,
            'delimiter' => $delimiter,
            'firstRowAsHeader' => $firstRowAsHeader,
            'numLines' => 1, // excluding header, if any
        ));

        $tmpData = $csvFile->getChunkNoEmptyLines($chunkSize,$offset);

        return $tmpData['data'];
    }



}