<?php
/**
 * Created by PhpStorm.
 * User: NikVas
 * Date: 17-Mar-16
 * Time: 22:16
 */

/**
 * Class BackgroundJobsWidget
 *
 * Renders the "tasks" icon with a counter at the top right with how
 * much active background jobs(created by the current user) are there - up to 5. If the "tasks"
 * is clicked, it displays a tooltip with a (truncated) list of active background jobs.
 */
class BackgroundJobsWidget extends CWidget{

    public $viewerId = 'backgroundJobsViewer';
    public $viewerIcon = 'fa-tasks';
    public $viewerSize = 0;
    public $loadTooltipUrl = null;
    public $activeCount = 0;
    public $defaultHeaderTitle = '';

    public $forceShowPopup = false;

    public function init() {
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile(self::getBackgroundJobsAssetsUrl().'/js/moment.js');
        Yii::app()->getClientScript()->registerScriptFile(Yii::app()->engineio->getAssetsUrl().'/js/engine.io.js');
        $cs->registerCssFile(self::getBackgroundJobsAssetsUrl().'/css/backgroundjobs.css');
        $cs->registerScriptFile(self::getBackgroundJobsAssetsUrl().'/js/backgroundjobs.js');
        
        if(isset(Yii::app()->session['openJobsPanel']) && Yii::app()->session['openJobsPanel'] == true){
            $this->forceShowPopup = true;
        }

        unset(Yii::app()->session['openJobsPanel']);

        $this->render('backgroundjobs_widget', array());
    }

    public static function getBackgroundJobsAssetsUrl(){
        if(!is_dir(Yii::getPathOfAlias('backgroundjobs.assets'))){
            Yii::log("Can not publish background jobs assets", CLogger::LEVEL_ERROR);
            return false;
        }

        $result = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('backgroundjobs.assets'));

        return $result;
    }

}