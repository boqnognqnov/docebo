<?php
/**
 * 
 *
 */
class BackgroundJob extends CApplicationComponent {

	const RUN_AGAIN  = 100;
	const JOB_FAILED = 1;
	const JOB_DONE   = 0;
	
    // Job family names
    const FAMILTY_ALL           = 'all';
    const FAMILTY_NOTIFICATIONS = 'notifications';
    const FAMILTY_MASSENROLL    = 'massenroll';
    const FAMILTY_IMPORTFROMCSV = 'importfromcsv';
    
    /**
     * Websocket server (node.js) parameters
     * @var string
     */
	public $websocketServerIp      = '192.168.0.201';  // IMPORTANT: Must be accessible by the client's browser!! So, public!
	public $websocketServerPort    = '33380';          // Must be allowed (e.g. in firewall)
	public $websocketUseHttps      = false;
	public $websocketHasOwnConfig  = false;
	public $enabledTypes           = array(self::FAMILTY_ALL => true);
    
    const INPUT_SIZE_MINI = 'mini';
    const INPUT_SIZE_NORMAL = 'normal';
    const INPUT_SIZE_MEDIUM = 'medium';
    const INPUT_SIZE_BIG = 'big';

    const INPUT_STRUCTURE_MULTI = 'multi';
    const INPUT_STRUCTURE_SINGLE = 'single';
    const INPUT_STRUCTURE_NONE = 'none';

    const MAX_MULTI_ITEMS = 500;
    
    private $_domain;
    private $_hash_id;
    
    private $_redisJobsData;
    private $_redisSidekiqQueues;  
    
    public function init(){
        parent::init();

        // Force Websocket using SSL when connection is secure
        if (Yii::app()->request->isSecureConnection) {
            $this->websocketUseHttps = true;
        }
        
        require_once(Yii::getPathOfAlias('common.vendors.Predis') 				. "/Autoloader.php");
        require_once(Yii::getPathOfAlias('common.vendors.SidekiqJobPusher') 	. "/Autoloader.php");
        Predis\Autoloader::register();
        SidekiqJobPusher\Autoloader::register();

        $this->_redisJobsData = new Predis\Client(array(
            'host' 		=> Yii::app()->params['jobs_data_redis_host'],
            'port' 		=> Yii::app()->params['jobs_data_redis_port'],
            'database' 	=> Yii::app()->params['jobs_data_redis_db'],
        ));
        
        $this->_redisSidekiqQueues = new Predis\Client(array(
            'host' 		=> Yii::app()->params['jobs_redis_host'],
            'port' 		=> Yii::app()->params['jobs_redis_port'],
            'database' 	=> Yii::app()->params['jobs_redis_db'],
        ));
        

        $this->_domain = Docebo::getOriginalDomain();
    }

    /**
     * Helper method to build key for ":job_metadata:"
     * Metadata Key : where we keep job overall information
     * 
     * @param string $domain
     * @param string $hash
     */
    protected function keyMetaData() {
        return $this->_domain . ":job_metadata:" . $this->_hash_id;
    }
    
    /**
     * Helper method to build key for ":job_data:"
     * Data Key(s) : where we keep job RUNTIME data, like courses, users and so on
     * 
     * @param string $domain
     * @param string $hash
     */
    protected function keyData() {
        return $this->_domain . ":job_data:" . $this->_hash_id;
    }
    
    
    /**
     * Creating a new BackgroundJob
     *
     *
     * Usage:
     *
     * Yii::app()->backgroundJob->createJob(
     *      array(
            'module' => 'course',
     *      'value' => 'Enrolling {users} in {courses} courses,
     *      'params' => array(
                '{users}' => 100,
     *          '{courses} => 200
     *      )
     * ),
     * , 'lms.protected.modules.backgroundjobs.components.handlers.AddUsers', array(
            array(
     *          'idst' => 123
     *      ),
     *      array(
     *          'idst' => 123
     *      ),
     *      array(
     *          'idst' => 123
     *      )
     * ), array('test' => '1234'));
     *
     *
     * @param $name array Name of the job
     * @param $handler string Absolute path to the job handler
     * @param array $data Input data
     * @param array $params Additional params
     * @return mixed Result of adding new job - OK or error
     */
    public function createJob($name, $handler, $data = array(), $params = array()){
        if($handler){

            $hash = Docebo::randomHash();
            
            $this->_hash_id = $hash;

            $jh = Yii::createComponent(array(
                'class' => $handler
            ));

            if(($jh instanceof BackgroundJobHandler) !== true){
                return false;
            }

            /**
             * @var $jh BackgroundJobHandler
             */

            $jh->init($hash);

            if(empty($data)){
                $data = $jh->collectData();
            }

            $preparedData = $jh->prepareData($hash, $data);

            //$timezone = Settings::get('timezone_default', null);
            //$nowTime = new DateTime('now', new DateTimeZone($timezone));
            $dateCreated = time();//Yii::app()->localtime->getUTCnow('Y-m-d H:i:s');//$nowTime->format('Y-m-d H:i:s');
            $userModel = CoreUser::model()->findByPk(Yii::app()->user->id);
            /**
             * @var $userModel CoreUser
             */

            $release = $GLOBALS['cfg']['release'];

            $params = array(
                'hash_id' => $hash,
                'author' => $userModel->idst,
                'authorName' => $userModel->getFullName(),
                'abortAuthorName' => '',
                'abortAuthorId' => 0,
                'date_created' => $dateCreated,
                'notify' => CJSON::encode(array()),
                'name' => CJSON::encode($name),
                'params' => json_encode($params),
                'handler' => $handler,
                'domain' => $this->_domain,
                'release' => $release,
                'percent' => 0,
                'chunkSize' => $preparedData['chunkSize'],
                'errors' => 0,
                'aborted' => 0,
                'type' => $jh->jobType,
                //Also cache the client IP address
                'client_ip_address' => isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']
            );

            if($jh->structure === BackgroundJobHandler::DATA_STRUCTURE_SIMPLE){
                $inputCount = count($preparedData);
                if($inputCount > 0){
                    $params['totalChunks']  = ceil(count($data) / $preparedData['chunkSize']);
                } else{
                    $params['totalChunks']  = 0;
                }

                $params['chunk'] = 0;
            } else if($jh->structure === BackgroundJobHandler::DATA_STRUCTURE_MATRIX){
                $params['totalChunks'] = $preparedData['totalChunks'];

                $chunkData = array();

                for($i = 0; $i < $preparedData['columns']; $i++){
                    $chunkData[] = 0;
                }

                if(isset($preparedData['chunk'])){
                    if(is_array($preparedData['chunk'])){
                        $params['chunk'] = CJSON::encode($preparedData['chunk']);
                    } else{
                        $params['chunk'] = $preparedData['chunk'];
                    }
                }

                $params['currentChunk'] = 0;
            } else if($jh->structure === BackgroundJobHandler::DATA_STRUCTURE_NONE){
                $params['totalChunks'] = 0;
            } else{
                if(isset($preparedData['totalChunks'])){
                    $params['totalChunks'] = $preparedData['totalChunks'];
                }

                if(isset($preparedData['chunk'])){
                    if(is_array($preparedData['chunk'])){
                        $params['chunk'] = CJSON::encode($preparedData['chunk']);
                    } else{
                        $params['chunk'] = $preparedData['chunk'];
                    }
                }

                if(isset($preparedData['currentChunk'])){
                    $params['currentChunk'] = $preparedData['currentChunk'];
                }
            }

            if(isset($preparedData['trash']) && !empty($preparedData['trash'])){
                $params['trash'] = CJSON::encode($preparedData['trash']);
            }

            $this->_redisJobsData->hmset($this->keyMetaData(), $params);
            $this->_redisJobsData->expire($this->keyMetaData(), $jh->expireInDays * 24 * 60 * 60);

            if($jh->jobType == BackgroundJobHandler::JOB_TYPE_USER){
                $this->_redisJobsData->publish('job_' . $this->_domain, $hash);
            }

            
            // Now that we have all the Job's data prepared and put in Redis 
            // push this job to Docker-Sidekiq Jobs Processor
            $this->pushJobToContainers($hash, $jh);
            
            return $hash;
        }

        return false;
    }

    /**
     * Fake login to the console application, so the Y::app()->user can be used here also
     *
     * @param $idUser int|string Idst of the LMS user
     */
    private function _setUser($idUser){
        Yii::app()->user->setUser($idUser);
    }

    /**
     *
     * Run the specific job by HASH_ID
     *
     * @param $hash_id string Hash ID of the job
     * @return bool|int Returns status of the job - continue or stop
     * @throws CException
     */
    public function run($hash_id){

        if(!$hash_id){
        	Yii::log('Empty Job hash ID', CLogger::LEVEL_WARNING);
            return self::JOB_DONE;
        }

        $this->_hash_id = $hash_id;
        
        try {
        	$data = $this->_redisJobsData->hgetall($this->keyMetaData());
        	if(empty($data)){
            	throw new Exception('Job ' . $hash_id . ' metadata not found in Redis!');
        	}
        }
        catch (Exception $e) {
        	Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
        	return self::JOB_FAILED; // ERROR
        }
        
        $exitCode = self::JOB_DONE; // ALL GOOD by default
        
        if(isset($data['aborted']) && $data['aborted'] != 1){
            $handler = $data['handler'];

            if(!$handler){
                Yii::log('Handler not found!', 'warning');
            }

            $jh = Yii::createComponent(array(
                'class' => $handler
            ));
            
            try{
                $this->_setUser($data['author']);

                //Fake the remote address IP here, because some functionality requires access to the client IP address!
                if(isset($data['client_ip_address'])){
                    $_SERVER['REMOTE_ADDR'] = $data['client_ip_address'];
                }

                $jh->init($hash_id);
                /** @var $jh BackgroundJobHandler */
                $jh->beforeRun();
                $jh->run();
                $exitCode = $jh->afterRun();
            } catch(Exception $e){
                Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
                $jh->addError($e->getMessage());
                $exitCode = self::JOB_FAILED; // ERROR 
            }
            
        }
        
        return $exitCode; 
        
    }

    /**
     * Stop future execution of a background job. Also can be provided some message to error log, cleaning the
     * temporary storage and deleting the entire record in Redis.
     *
     * @param $hash_id string Hash ID of the background job
     * @param array $messages Array of messages to be added to the error log
     * @param bool $clean Drop the temporary storage
     * @param bool $deleteJob Delete redis record
     * @throws CException
     */
    public function stop($hash_id, $messages = array(), $clean = true, $deleteJob = false){
        if(!$hash_id){
            Yii::log('No hash_id provided for stopping a background job!!', 'warning');
            return;
        }

        $this->_hash_id = $hash_id;
        
        $data = $this->_redisJobsData->hgetall($this->keyMetaData());

        if(empty($data)){
            Yii::log('Could not find job with hash ID: ' . $hash_id, 'warning');
        }

        $handler = $data['handler'];

        if(!$handler){
            Yii::log('Handler not found for job: ' . $hash_id, 'warning');
        }

        $jh = Yii::createComponent(array(
            'class' => $handler
        ));

        if($jh instanceof BackgroundJobHandler){
            /**
             * @var $jh BackgroundJobHandler
             */

            $jh->init($hash_id);

            if(!empty($messages)){
                foreach($messages as $message){
                    $jh->addError($message);
                }
                $data['errors'] += $jh->saveErrors();
            }

            if($clean){
                $jh->clean();
            }
        }

        $data['aborted'] = 1;

        if(!$deleteJob){
            $this->_redisJobsData->hmset($this->keyMetaData(), $data);
        } else{
            $this->_redisJobsData->del($this->keyMetaData());
        }
    }
    
    /**
     * Send/Push job for execution by Docker-Sidekiq jobs processor
     * @param string $hash
     * @param BackgroundJobHandler $jh
     */
    private function pushJobToContainers($hash, $jh) {
        
        $sidekiq = new \SidekiqJobPusher\Client($this->_redisSidekiqQueues, false);
        $retry 	= true;
        
        // 1. Push the job in our queue (lms's queue)
        // We can push here MANY jobs, before next step (2) !!!!!!
        $queue 	= $this->_domain;
        $args = array($this->_domain, $hash);
        $queuePriority = $jh->jobType === "user" ? "high" : "low";
        $queue = $queue . ":" . $queuePriority;
        $sidekiq->perform('ProcessSingleLmsJob', $args, $retry, $queue);
        
        // 2. Then, push a job in "container-trigger" queue, to make master-sidekiq to run a contaner to handle jobs for this LMS
        // This is to TRIGGER container creation
        $queue 	= "container-trigger";
        
        // Pass required parameters to Scheduler
        // Since 7.0 we must also pass Host:port/db of the Redis Config host to "LaunchLmsJobsProcessor" worker
        // It will launch an LMS Sidekiq container and inject these values into container environment
        $args = array($this->_domain, Docebo::platformVersion(), 
        		Yii::app()->params['config_redis_host'], 
        		Yii::app()->params['config_redis_port'], 
        		Yii::app()->params['config_redis_db']
        );
        
        $sidekiq->perform('LaunchLmsJobsProcessor', $args, $retry, $queue);
        
        // That's it... Container should be created, the it will start popping jobs from step (1) and execute
        // yiic-background-job  <domain> <hash>
        // for every job found in <domain> queue
        
    }
    
    
    /**
     * Check if specified family type of job(s)  (mass enrollment, notifications, ...) is ENABLED or DISABLED
     * 
     * @param string $family
     * @return boolean
     */
    public function bgJobEnabled($family) {
        
        
        // Container based BG JObs are no more an option in Legacy Code!!
        return false;
        
//         if (isset($this->enabledTypes[$family])) {
//             return (boolval($this->enabledTypes[$family]));
//         }
//         if (isset($this->enabledTypes[self::FAMILTY_ALL]) && $this->enabledTypes[self::FAMILTY_ALL]) {
//             return true;
//         }
        
    }

    /**
     * Check if the installation has ANY job type enabled
     *
     * @return bool
     */
    public function hasAnyEnabledJob(){
        
        // Container based BG JObs are no more an option in Legacy Code!!
        return false;
        
//         foreach ($this->enabledTypes as $type){
//             if($type == true){
//                 return true;
//             }
//         }

    }
    
}
