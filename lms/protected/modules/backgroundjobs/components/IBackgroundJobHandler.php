<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 2/26/2016
 * Time: 3:33 PM
 */
interface IBackgroundJobHandler
{
    public function run();
}