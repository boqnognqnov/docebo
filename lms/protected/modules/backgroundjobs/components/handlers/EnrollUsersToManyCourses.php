<?php
/**
 * 
 * 
 *
 */
class EnrollUsersToManyCourses extends BackgroundJobHandler implements IBackgroundJobHandler{

    public $structure = self::DATA_STRUCTURE_MATRIX;
    public $chunkSize = 50;

    const JOB_HANDLER = 'lms.protected.modules.backgroundjobs.components.handlers.EnrollUsersToManyCourses';

    /**
     * 
     * {@inheritDoc}
     * @see IBackgroundJobHandler::run()
     */
    public function run()
    {

        $idst = $this->get('idst');
        $role = $this->getParam('role', 3);
        $courses = $this->get('courses');
        $sessions = $this->getParam('sessions', array());
        $webinarSessions = $this->getParam('webinarSessions', array());
        $date_begin_validity = $this->getParam('date_begin_validity', false);
        $date_expire_validity = $this->getParam('date_expire_validity', false);
		$fields = $this->getParam('fields', array());
        $currentUser = Yii::app()->user->id;

        if($date_expire_validity !== false && $date_begin_validity !== false){
            $date_expire_validity = Yii::app()->localtime->fromUserLocalDatetime($currentUser, $date_expire_validity);
            $date_begin_validity = Yii::app()->localtime->fromUserLocalDatetime($currentUser, $date_begin_validity);
        }

        $isUserPu = Yii::app()->user->getIsPu();

        if($isUserPu){
            // Anti tampering checks follow below (possible hack attempts)

            // Filter out any users the PU doesn't have assigned
            $newIdst = Yii::app()->getDb()->createCommand()
                ->selectDistinct('t.user_id')
                ->from(CoreUserPU::model()->tableName().' t')
                ->join(CoreUser::model()->tableName().' u', 'u.idst=t.user_id')
                ->where('puser_id=:currentUser', array(':currentUser'=>$currentUser))
                ->andWhere('user_id IN (' . implode(',', $idst) . ')')
                ->queryColumn();

            //Add error with any user that cannot be enolled by this PU
            $notManagedByPU = array_diff($idst,$newIdst);
            if(count($notManagedByPU) > 0 ){
                foreach($notManagedByPU as $uid){
                    $this->addError('Job Author is a PU (id: '.Yii::app()->user->idst.') and is not allowed to manage this user', CoreUser::getUserIdByIdst($uid));
                }
            }

            $idst = $newIdst;

            // Filter out any passed courses the PU doesn't have assigned
            $newCourses = Yii::app()->getDb()->createCommand()
                ->selectDistinct('course_id')
                ->from(CoreUserPuCourse::model()->tableName())
                ->where('puser_id=:idUser', array(':idUser'=>$currentUser))
                ->andWhere(array('IN', 'course_id', $courses))
                ->queryColumn();

            //Add error with any course that cannot be admninistrated by this PU
            $notManagedByPU = array_diff($courses,$newCourses);
            if(count($notManagedByPU) > 0 ){
                foreach($notManagedByPU as $cid){
                    $this->addError('Job Author is a PU (id: '.Yii::app()->user->idst.') and is not allowed to manage the course '.LearningCourse::getNameById($cid));
                }
            }

            $courses = $newCourses;

            // Filter out any passed sessions that do not belong to courses assigned to the PU
            $sessions = Yii::app()->getDb()->createCommand()
                ->selectDistinct('id_session')
                ->from(LtCourseSession::model()->tableName())
                ->andWhere(array('IN', 'course_id', $courses))
                ->andWhere(array('IN', 'id_session', $sessions))
                ->queryColumn();

            $webinarSessions = Yii::app()->getDb()->createCommand()
                ->selectDistinct('id_session')
                ->from(WebinarSession::model()->tableName())
                ->andWhere(array('IN', 'course_id', $courses))
                ->andWhere(array('IN', 'id_session', $webinarSessions))
                ->queryColumn();
        }

        $allowBuySeats = CoreUser::adminRuleEnabled(Yii::app()->user->id, 'allow_buy_seats');
        $directCourseSubscribe = CoreUser::adminRuleEnabled(Yii::app()->user->id, 'direct_course_subscribe');
        $timeUTCNow = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');

        foreach($idst as $idUser){
            foreach($courses as $idCourse){

                try{

                    $sql = 'SELECT * FROM learning_courseuser where idUser = ' . $idUser . ' AND idCourse = ' . $idCourse;

                    $enrollment = Yii::app()->db->createCommand($sql)->execute();

                    if($enrollment > 0) continue;

                    $insertParams = array(
                        $idUser,
                        $idCourse,
                        0,
                        "'" . $timeUTCNow . "'",
                        $currentUser,
                        0,
                        $role, 
						"'".json_encode($fields) ."'",  
                    );

                    if($date_begin_validity !== false){
                        $insertParams[] = "'" . $date_begin_validity . "'";
                    }

                    if($date_expire_validity !== false){
                        $insertParams[] = "'" . $date_expire_validity . "'";
                    }

                    $sql = 'INSERT INTO learning_courseuser (idUser, idCourse, edition_id, date_inscr, subscribed_by, waiting, level, enrollment_fields '   . ($date_begin_validity !== false ? ',date_begin_validity' : '') . ($date_begin_validity !== false ? ',date_expire_validity' : '') .') VALUES (' . implode(',', $insertParams) . ')';

                    $enrolled = Yii::app()->db->createCommand($sql)->execute();

                    $course = LearningCourse::model()->findByPk($idCourse);
                    $user = CoreUser::model()->findByPk($idUser);

                    if($enrolled !== 0){
						if($course->course_type == LearningCourse::TYPE_ELEARNING)
							$course->updateMaxSubscriptionQuota();

                        Yii::app()->event->raise(EventManager::EVENT_USER_SUBSCRIBED_COURSE, new DEvent($this, array(
                            'course' => $course,
                            'user' => $user,
                            'date_expire_validity' => Yii::app()->localtime->fromLocalDateTime($date_expire_validity),
                            'date_end' => Yii::app()->localtime->fromLocalDate($course->date_end),
                        )));

                    }

                    if($enrolled !== 0 && $course->selling == 1 && $isUserPu && $allowBuySeats){
                        // The current user is a PU only allowed to enroll
                        // users in courses using "seats", so decrease his available
                        // seats in the course by the amount of users he enrolled
                        CoreUserPU::modifyAvailableSeats($idCourse, -1);

                        Yii::app()->event->raise('onPuSeatConsume', new DEvent($this, array(
                            'idCourse' => $idCourse,
                            'idUser' => $idUser,
                            'idPowerUser' => $currentUser,
                        )));
                    }
                } catch (Exception $e){
                    $this->addError($e->getMessage());
                }
            }

            // Also enroll the user to the selected sessions in this course
            foreach($sessions as $idSession){
                $sql = 'SELECT * FROM lt_courseuser_session WHERE id_user = ' . $idUser . ' AND id_session = ' . $idSession;

                $enrollment = Yii::app()->db->createCommand($sql)->execute();

                if($enrollment > 0){
                    continue;
                }

                $status = LtCourseuserSession::$SESSION_USER_SUBSCRIBED;

                if(!$directCourseSubscribe && $isUserPu){
                    $status = LtCourseuserSession::$SESSION_USER_WAITING_LIST;
                }

                $sql = 'INSERT INTO lt_courseuser_session (id_user, id_session, date_subscribed, status) VALUES (' . implode(',', array(
                            $idUser,
                            $idSession,
                            "'" . $timeUTCNow . "'",
                            $status
                        )) . ')';

                $enrolled = Yii::app()->db->createCommand($sql)->execute();

                if($enrolled > 0){
                    Yii::app()->event->raise(EventManager::EVENT_USER_ENROLLED_IN_CLASSROOM_SESSION, new DEvent(new self(), array(
                        'session_id'=>$idSession,
                        'user'=>$idUser,
                    )));
					$session = LtCourseSession::model()->findByPk($idSession);
					$session->course->updateMaxSubscriptionQuota();

					if($status != LtCourseuserSession::$SESSION_USER_WAITING_LIST) {
						$courseEnroll = LearningCourseuser::model()->findByAttributes(array('idUser' => $idUser, 'idCourse' => $session->course_id));
						if($courseEnroll->waiting == 1) {
							$courseEnroll->waiting = 0;
							$courseEnroll->save();
						}
					}
                }
            }

            // enroll user to the selected webinar session in this course
            foreach ($webinarSessions as $idSession)
            {
                $sql = 'SELECT * FROM webinar_session_user WHERE id_user = ' . $idUser . ' AND id_session = ' . $idSession;

                $enrollment = Yii::app()->db->createCommand($sql)->execute();

                if($enrollment > 0){
                    continue;
                }

                $status = LtCourseuserSession::$SESSION_USER_SUBSCRIBED;

                if(!$directCourseSubscribe && $isUserPu){
                    $status = LtCourseuserSession::$SESSION_USER_WAITING_LIST;
                }

                $sql = 'INSERT INTO webinar_session_user (id_user, id_session, date_subscribed, status) VALUES (' . implode(',', array(
                            $idUser,
                            $idSession,
                            "'" . $timeUTCNow . "'",
                            $status
                        )) . ')';

                $enrolled = Yii::app()->db->createCommand($sql)->execute();

                if($enrolled > 0){
                    Yii::app()->event->raise(EventManager::EVENT_USER_ENROLLED_IN_WEBINAR_SESSION, new DEvent(new self(), array(
                        'session_id'=>$idSession,
                        'user'=>$idUser,
                    )));
					$session = WebinarSession::model()->findByPk($idSession);
					$session->course->updateMaxSubscriptionQuota();

					if($status != WebinarSessionUser::$SESSION_USER_WAITING_LIST) {
						$courseEnroll = LearningCourseuser::model()->findByAttributes(array('idUser' => $idUser, 'idCourse' => $session->course_id));
						if($courseEnroll->waiting == 1) {
							$courseEnroll->waiting = 0;
							$courseEnroll->save();
						}
					}
                }
            }
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see BackgroundJobHandler::onStart()
     */
    public function onStart()
    {
        $now = DateTime::createFromFormat('U.u', microtime(true));
        Yii::log('Start: ' . $now->format("m-d-Y H:i:s.u"), CLogger::LEVEL_INFO);
    }

    
    /**
     * 
     * {@inheritDoc}
     * @see BackgroundJobHandler::onFinish()
     */
    public function onFinish()
    {
        $now = DateTime::createFromFormat('U.u', microtime(true));
        Yii::log('Finish: ' . $now->format("m-d-Y H:i:s.u"), CLogger::LEVEL_INFO);
    }
}