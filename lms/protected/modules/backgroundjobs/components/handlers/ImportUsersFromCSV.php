<?php

/**
 * Created by PhpStorm.
 * User: NikVasilev
 * Date: 6.4.2016 г.
 * Time: 12:01
 */
class ImportUsersFromCSV extends BackgroundJobImportCSVHandler
{
    public $chunkSize = 50;


    public function run(){

        $data   =   $this->getCustomData();

        //Job params--------
        //UserImportForm params
        $filename           =   $this->getParam('filename',false);
        $charset            =   $this->getParam('charset','UTF-8');
        $separator          =   $this->getParam('separator','auto');
        $manualSeparator    =   $this->getParam('manualSeparator',null);
        $node               =   $this->getParam('node',null);
        $firstRowAsHeader   =   $this->getParam('firstRowAsHeader',true);
        $passwordChanging   =   $this->getParam('passwordChanging','no');
        $branchOption       =   $this->getParam('branchOption','existing');
        $UserImportForm     =   $this->getParam('UserImportForm', array());
        $importMap		    =   $this->getParam('import_map', array(
            'userid',
            'firstname',
            'lastname',
            'email',
            'new_password',
        ));
        $insertUpdate	    =   $this->getParam('insertUpdate', false);
        $totalUsers         =   $this->getParam('totalUsers',0);
        $unenroll_deactivated=  $this->getParam('unenroll_deactivated',false);
        $activation         =   $this->getParam('activation','');
        $powerUserBranches  =   $this->getParam('powerUserBranches',array());
        $importTabType      =   $this->getParam('importTabType', false);
	    $autoAssignBranches =   $this->getParam('autoAssignBranches', true);
        //------------------

        try {
            $importForm = new UserImportForm();

            //Setting the import form instance
            $importForm->scenario = 'step_two';
            $importForm->localFile = $filename;
            $importForm->charset = $charset;
            $importForm->separator = $separator;
            $importForm->manualSeparator = $manualSeparator;
            $importForm->node = $node;
            $importForm->firstRowAsHeader = $firstRowAsHeader;
            $importForm->passwordChanging = $passwordChanging;
            $importForm->branchOption = $branchOption;
            $importForm->insertUpdate = $insertUpdate;
            $importForm->totalUsers = $totalUsers;
            $importForm->unenroll_deactivated = $unenroll_deactivated;
            $importForm->activation = $activation;
            $importForm->powerUserBranches = $powerUserBranches;
            $importForm->importTabType = $importTabType;
	        $importForm->autoAssignBranches = $autoAssignBranches;
            $importForm->setImportMap($importMap);
            $importForm->setData($data);


            $chunkSize = intval($this->_jobParams['chunkSize']);
            $offset = $this->_jobParams['currentChunk'] * $chunkSize;

            $importForm->saveUsers(false, $offset === 0, $offset);

             $importForm->cumulativeFailedLines = array_merge($importForm->cumulativeFailedLines, $importForm->failedLines);

//            foreach ($importForm->failedLines as $key => $value) {
//                $errorElementss = explode(',',$value);
//                $this->addError($errorElementss[0]);
//            }

            foreach ($importForm->cumulativeFailedLines as $failedLine) {
                $errorElements = explode(',',$failedLine);
                $this->addError($errorElements[0], $errorElements[1]);
            }

        } catch (Exception $e){
            $this->addError($e->getMessage());
        }

    }

    /**
     *
     * {@inheritDoc}
     * @see BackgroundJobHandler::onStart()
     */
    public function onStart()
    {
        $now = DateTime::createFromFormat('U.u', microtime(true));
        Yii::log('Start: ' . $now->format("m-d-Y H:i:s.u"), CLogger::LEVEL_INFO);
    }

    /**
     *
     * {@inheritDoc}
     * @see BackgroundJobHandler::onFinish()
     */
    public function onFinish()
    {
        $now = DateTime::createFromFormat('U.u', microtime(true));
        Yii::log('Finish: ' . $now->format("m-d-Y H:i:s.u"), CLogger::LEVEL_INFO);
    }

    /** Prepare the input data for the handler cycles
     * @param array $input
     * @param string $hash_id
     * @return array
     */
    protected function prepareCustomInput($input = array(), $hash_id){
        $chunkSize = self::MAX_IMPORT_CSV_ITEMS;

        if($this->chunkSize !== self::CHUNK_SIZE_AUTO && is_numeric($this->chunkSize)){
            $chunkSize = $this->chunkSize;
        }

        if($this->storage === self::STORAGE_S3){

            $chunk = array('csv'=>0);

            //Get total number of lines
			$totalLines = $input['totalUsers'];
			if($input['firstRowAsHeader'])
				$totalLines += 1;
            $totalChunks = ceil($totalLines/$chunkSize);

            //Save file to S3
            $this->saveCSVToS3($input['filename']);

            return array(
                'totalChunks' => $totalChunks,
                'chunkSize' => $chunkSize,
                'chunk' => $chunk,
                'currentChunk' => 0
            );
        } else if($this->storage === self::STORAGE_REDIS){
            //TODO - to Redis - No such case for now
        }
        elseif($this->storage === self::STORAGE_MYSQL){
            //TODO - to MySQL - No such case for now
        }
    }

    /** Saving the source CSV to S3. We expect the source file is present in upload_tmp
     * @param string $fname
     * @param string $subFolder
     * @return bool $success
     */
    protected function saveCSVToS3($fname, $subFolder = ''){
        $subFolder = ($subFolder != '')? $subFolder . DIRECTORY_SEPARATOR : $subFolder ;
        $fullPath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $subFolder . $fname;

        $storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_COMMON);
        $success = $storage->storeAs($fullPath,$fname,'jobs');

        return $success;
    }



    /** Overriding the creation of the CSV error file for the purpose of adding a "User" column
     *
     */
    protected function initErrorFile(){
        $this->_filename = 'errors_' . $this->_hash_id . '.csv';
        $this->_filePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $this->_filename;

        //If file not exist, add the CSV headers
        if(!file_exists($this->_filePath)){
            $fh = fopen($this->_filePath, 'a');
            fputs($fh, "Datetime, User, Message\n");
            fclose($fh);
        }
    }

    /** Overriding the adding of the error to the CSV file for the purpose of passing a username for the "User" column
     *
     * Add custom error message to job errors
     *
     * @param $message string Error Message
     */
    public function addError($message, $username=false) {
        if($message){
            $this->_errors[] = array(
                'time' => Yii::app()->localtime->getUTCNow('Y-m-d H:i:s').' UTC',
                'user' => ($username)? $username : '',
                'message' => $message
            );
        }
    }
}