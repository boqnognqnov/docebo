<?php
/**
 * Created by PhpStorm.
 * User: NikVas
 * Date: 17-Mar-16
 * Time: 09:13
 */?>
<?php /** @var $this InboxBGJobsIconWidget */ ?>
<div id="<?=$this->viewerId?>" class="inbox-bell tasks-viewer inbox-wrapper text-right">
	<div class="count" style="display:none;"><?=$this->activeCount;?></div>
    <i class="fa fa-tasks fa-lg"></i>
	<div class="unread-tooltip text-left">
    	<p><b><?=$this->defaultHeaderTitle?></b></p>
    	<div class="ajax-container">
            <!-- Non-render -->
        	<div id="inbox-bg-jobs" class="tab-pane background-jobs-viewer background-jobs-tabs"></div>
            <!-- End of-non-render -->
        	<div class="view-all-jobs text-center"><a href="<?= Docebo::createLmsUrl('//backgroundjobs/default/index');?>"><?= Yii::t('standard', 'View all');?></a></div>
    	</div>
	</div>
</div>
<script>
	var bgJobsOptions = {
        domain: 		    <?= json_encode(Docebo::getOriginalDomain()) ?>,
        session: 		    <?= json_encode(Yii::app()->request->cookies['docebo_session']->value) ?>,
		wsHost:             <?= json_encode(Yii::app()->backgroundJob->websocketServerIp) ?>,
	    wsPort:             <?= json_encode(Yii::app()->backgroundJob->websocketServerPort) ?>,
	    wsUseHttps:         <?= json_encode(Yii::app()->backgroundJob->websocketUseHttps) ?>,
	    notificationUrl:    <?= json_encode(Yii::app()->createUrl('/backgroundjobs/default/notify')) ?>,
	    abortUrl:           <?= json_encode(Yii::app()->createUrl('/backgroundjobs/default/delete')) ?>,
	    downloadUrl: 	    <?= json_encode(Yii::app()->createUrl('/backgroundjobs/default/downloaderrors')) ?>,
	    userId:             <?= json_encode(Yii::app()->user->idst) ?>,
		openWidget:			<?= json_encode($this->forceShowPopup) ?>,
	    isWidget: 			true,
	    isPU:               <?= (Yii::app()->user->getIsPu())? 'true' : 'false' ;?>,
	    	    
    	// Jobsa Data				                
    	redisHost:          null,
    	redisPort:          null,
    	redisJobDb:         null,
    	// PHP Sessions                
    	redisSessionHost:   null,
    	redisSessionPort:   null,
    	redisSessionDb:     null,
    	
	  	sendRedisConfig: 	false
	};

	<?php 
   	   // If Websocket dow not have its own configuration loaded, SEND config over wire 
   	   if (!Yii::app()->backgroundJob->websocketHasOwnConfig) { 
   	?>       
		bgJobsOptions = $.extend({}, bgJobsOptions, {
	    	// Jobsa Data				                
	    	redisHost:          <?= json_encode(Yii::app()->params['jobs_data_redis_host']) ?>,
	    	redisPort:          <?= json_encode(Yii::app()->params['jobs_data_redis_port']) ?>,
	    	redisJobDb:         <?= json_encode(Yii::app()->params['jobs_data_redis_db']) ?>,
	    	// PHP Sessions                
	    	redisSessionHost:   <?= json_encode(Yii::app()->params['redis_sessions_host']) ?>,
	    	redisSessionPort:   <?= json_encode(Yii::app()->params['redis_sessions_port']) ?>,
	    	redisSessionDb:     <?= json_encode(Yii::app()->params['redis_sessions_db']) ?>,
			dateFormat: 		<?= json_encode(Yii::app()->localtime->getLocalDateTimeFormat()) ?>,
			timeOffset:         <?= json_encode(Yii::app()->localtime->getTimezoneOffset(Yii::app()->localtime->getTimeZone(), true)) ?>,
	    	    	
	    	sendRedisConfig: 	true    	
		});
	<?php } ?>
		InboxBGJobs = new BGJobsLoader(bgJobsOptions);

</script>

<? $script = "
	$(function(){

		$('#".$this->viewerId.".inbox-bell .count, #".$this->viewerId.".inbox-bell .fa').click(function(){
			$('#".$this->viewerId." .unread-tooltip').show();
		});
	    
		$(document).click(function(event) {
			if($(event.target).closest('#".$this->viewerId.".inbox-bell').length <= 0) {
				// Clicked outside the tooltip, hide the tooltip
				$('#".$this->viewerId." .unread-tooltip').hide();
			}
		})
                
	});";
Yii::app()->clientScript->registerScript('inbox_widget_bell_'.$this->viewerId, $script);
