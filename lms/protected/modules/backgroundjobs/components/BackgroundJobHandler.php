<?php
/**
 * 
 *
 */
class BackgroundJobHandler extends CComponent {

    /**
     * Hash ID of the job
     *
     * @var $_hash_id string
     */
    protected $_hash_id;

    /**
     * Temporary table for the job
     *
     * @var $_tableName string
     */
    protected $_tableName;

    /**
     * Current chunk iteration
     *
     * @var $_chunk integer|array
     */
    protected $_chunk;

    /**
     * Total chunk iterations
     *
     * @var $_totalChunks integer
     */
    protected $_totalChunks;

    /**
     * Limit of items for every chunk
     *
     * @var $_limit integer
     */
    protected $_limit;

    /**
     * Job Completed percentage
     *
     * @var $_completePercentage integer
     */
    protected $_completePercentage;

    /**
     * Current installation domain name
     *
     * @var $_domain string
     */
    protected $_domain;

    /**
     * Job data - coming from Redis
     *
     * @var $_jobParams array
     */
    protected $_jobParams;

    /**
     * @var $_redis Predis\Client
     */
    protected $_redis;

    /**
     * Items for the current iteration
     *
     * @var $data array
     */
    protected $data;

    /**
     * Additional Job params
     *
     * @var $params array
     */
    protected $params;

    /**
     * Keeping errors during the execution of the job
     *
     * @var $_errors array
     */
    protected $_errors;

    /**
     * Filename of the CSV file contains the errors
     *
     * @var $_filename string
     */
    protected $_filename;

    /**
     * Path to the CSV file contains the errors
     *
     * @var $_filePath string
     */
    protected $_filePath;

    /**
     * Variable holding bad passed or formatted array
     *
     * @var $_trash array
     */
    protected $_trash;

    const DATA_STRUCTURE_SIMPLE = 'simple';
    const DATA_STRUCTURE_MATRIX = 'matrix';
    const DATA_STRUCTURE_NONE   = 'none';
    const DATA_STRUCTURE_CUSTOM = 'custom';

    const INPUT_SIZE_MINI       = 'mini';
    const INPUT_SIZE_NORMAL     = 'normal';
    const INPUT_SIZE_MEDIUM     = 'medium';
    const INPUT_SIZE_BIG        = 'big';

    const STORAGE_REDIS         = 'redis';
    const STORAGE_MYSQL         = 'mysql';
    const STORAGE_S3            = 's3';

    const MAX_MATRIX_ITEMS      = 500;
    const MAX_IMPORT_CSV_ITEMS  = 500;

    const CHUNK_SIZE_AUTO       = 'auto';

    const JOB_TYPE_USER         = 'user';
    const JOB_TYPE_SYSTEM       = 'system';

    public $structure       = self::DATA_STRUCTURE_SIMPLE;
    public $jobType         = self::JOB_TYPE_USER;
    public $storage         = self::STORAGE_REDIS;
    public $chunkSize       = self::CHUNK_SIZE_AUTO;
    public $expireInDays    = 10;

    private $_time;

    /**
     * Initialize the Background job handler.
     * Preparing redis and collecting job parameters
     *
     *
     * @param $hash_id string Hash ID of the job
     */
    public function init($hash_id){
        $this->_hash_id = $hash_id;
        $this->_tableName = 'job_' . $this->_hash_id;

        require_once(Yii::getPathOfAlias('common.vendors.Predis') . "/Autoloader.php");
        Predis\Autoloader::register();

        // Get REDIS info from params
        $_redisHost = Yii::app()->params['jobs_data_redis_host'];
        $_redisPort = Yii::app()->params['jobs_data_redis_port'];
        $_redisDb   = Yii::app()->params['jobs_data_redis_db'];

        /**
         * Redis client initialization
         */
        $this->_redis = new Predis\Client(array(
            'host' 		=> $_redisHost,
            'port' 		=> $_redisPort,
            'database' 	=> $_redisDb,
        ));

        $this->_domain = Docebo::getOriginalDomain();

        $this->_jobParams = $this->_redis->hgetall($this->keyMetaData());

        if(!empty($this->_jobParams)){
            if($this->structure !== self::DATA_STRUCTURE_NONE){
                $this->_chunk = $this->_jobParams['chunk'];

                $this->_trash = $this->_jobParams['trash'];
                $this->_trash = CJSON::decode($this->_trash);

                if(strpos($this->_chunk, '[') !== FALSE || strpos($this->_chunk, '{') !== FALSE){
                    $this->_chunk = CJSON::decode($this->_chunk);
                } else{
                    $this->_chunk = intval($this->_chunk);
                }

                $this->_limit = intval($this->_jobParams['chunkSize']);
                $this->_totalChunks = intval($this->_jobParams['totalChunks']);

                $this->initErrorFile();
            }
            $this->params = CJSON::decode($this->_jobParams['params']);
        }
    }

    protected function initErrorFile(){
        $this->_filename = 'errors_' . $this->_hash_id . '.csv';
        $this->_filePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $this->_filename;

        //If file not exist, add the CSV headers
        if(!file_exists($this->_filePath)){
            $fh = fopen($this->_filePath, 'a');
            fputs($fh, "Datetime, Message\n");
            fclose($fh);
        }
    }

    protected function getSimpleData(){

        $data = array();

        if($this->storage === self::STORAGE_MYSQL){
            $command = Yii::app()->db->createCommand();
            /**
             * @var $command CDbCommand
             */

            /**
             * Getting data from the current iteration
             */
            $command->select('data')
                ->from($this->_tableName)
                ->order('id')
                ->limit($this->_limit)
                ->offset($this->_chunk * $this->_limit);

            $result = $command->queryAll();

            if (!empty($result)) {
                foreach ($result as $key => $value) {
                    $data[] = CJSON::decode($value['data']);
                }
            }
        } else if($this->storage === self::STORAGE_REDIS){
            $data = $this->_redis->lpop($this->keyData());

            return CJSON::decode($data);
        }

        return $data;
    }

    protected function getMatrixData(){

        $data = array();

        if($this->storage === self::STORAGE_MYSQL){
            $command = Yii::app()->db->createCommand();
            /**
             * @var $command CDbCommand
             */

            $command->select('*')
                ->from($this->_tableName)
                ->order('id')
                ->limit(1);
            $resultColumns = $command->queryRow();

            if (!empty($resultColumns['id'])) {
                unset($resultColumns['id']);
            }

            $columnNames = array_keys($resultColumns);

            $index = 0;
            foreach ($this->_chunk as $key => $chunk) {

                //Name of the current column
                $currentColumn = $columnNames[$index];

                $commandData = Yii::app()->db->createCommand();
                /**
                 * @var $commandData CDbCommand
                 */

                //Get all data for the current column
                $commandData->select($currentColumn)
                    ->from($this->_tableName)
                    ->offset($chunk * $this->_limit)
                    ->limit($this->_limit);
                $result = $commandData->queryAll();

                $columnData = array();

                foreach ($result as $row) {
                    $columnData[] = $row[$currentColumn];
                }

                $data[$currentColumn] = $columnData;
                if ($chunk + 1 === $this->_totalChunks) {
                    //If we are at the last chunk - increment the next chunk (if exist)
                    if (!empty($this->_chunk[$key + 1])) {
                        $this->_chunk[$key + 1] = $this->_chunk[$key + 1] + 1;
                    }
                    //Reset the current column
                    $this->_chunk[$key] = 0;
                }

                if ($index === 0) {
                    //If the iterations of the most left column are just 1 - increment the next column
                    if ($chunk + 1 < $this->_totalChunks) {
                        $this->_chunk[$key] = $this->_chunk[$key] + 1;
                    }
                }

                $this->_jobParams['chunk'] = CJSON::encode($this->_chunk);
                $this->_redis->hmset($this->keyMetaData(), $this->_jobParams);
                $index++;
            }
        } else if($this->storage === self::STORAGE_REDIS){
            $columns = array();

            $index = 0;
            foreach($this->_chunk as $key => $chunk){
                $columns[$index] = $key;
                $index++;
            }

            $index = 0;
            foreach($this->_chunk as $key => $chunk){
                $listKey = $this->keyData() . ':' . $key;
                $jobData = $this->_redis->lrange($listKey, $chunk, $chunk);
                if(!empty($jobData)){
                    $listLength = $this->_redis->llen($listKey);

                    $data[$key] = CJSON::decode($jobData[0]);

                    if($index === 0){
                        $this->_chunk[$columns[$index]] = $this->_chunk[$columns[$index]] + 1;
                    }

                    if($chunk + 1 == $listLength){
                        if(isset($this->_chunk[$columns[$index + 1]])){
                            $this->_chunk[$columns[$index + 1]] = $this->_chunk[$columns[$index + 1]] + 1;
                            $this->_chunk[$columns[$index]] = 0;
                        }
                    }
                    $index++;
                }
            }

            $this->_jobParams['chunk'] = CJSON::encode($this->_chunk);

        }

        return $data;
    }

    /**
     * Custom method to get data from temporary place.
     *
     * @NOTE: override this function and set $structure to self::DATA_STRUCTURE_CUSTOM to use it
     *
     * @return array
     */
    protected function getCustomData(){

    }

    /**
     * Gets data from the temporary storage.
     *
     * @NOTE: For simple and matrix structure, the temporary storage will be the DB
     *
     * @return array
     */
    protected function getData(){

        $data = array();

        switch($this->structure){
            case self::DATA_STRUCTURE_SIMPLE:
                $data = $this->getSimpleData();
                break;
            case self::DATA_STRUCTURE_MATRIX:
                $data = $this->getMatrixData();
                break;
            case self::DATA_STRUCTURE_CUSTOM:
                $data = $this->getCustomData();
                break;
        }

        return $data;
    }
    
    
    /**
     * Helper method to build key for ":job_metadata:"
     * Metadata Key : where we keep job overall information
     * 
     * @param string $domain
     * @param string $hash
     */
    protected function keyMetaData() {
        return $this->_domain . ":job_metadata:" . $this->_hash_id;
    }
    
    /**
     * Helper method to build key for ":job_data:"
     * Data Key(s) : where we keep job RUNTIME data, like courses, users and so on
     * 
     * @param string $domain
     * @param string $hash
     */
    protected function keyData() {
        return $this->_domain . ":job_data:" . $this->_hash_id;
    }
    

    /**
     * Executed before every run of Background Job Handler, for preparing data!
     *
     * NOTE: If you want to override this, PLEASE first do parent::beforeRun()
     */
    public function beforeRun(){
        if($this->structure !== self::DATA_STRUCTURE_NONE){
            if($this->_totalChunks > 0) {
                $this->data = $this->getData();
            }

            $executeStart = false;

            if($this->structure === self::DATA_STRUCTURE_SIMPLE || (isset($this->_jobParams['chunk']) && is_numeric($this->_chunk))){
                if($this->_chunk == 0){
                    $executeStart = true;
                }
            } else if($this->structure == self::DATA_STRUCTURE_MATRIX){
                if($this->_jobParams['currentChunk'] == 0){
                    $executeStart = true;
                }
            }

            if($executeStart){
                $this->onStart();
            }
        }
    }

    /**
     * Executed after every job handler.
     *
     * NOTE: IF YOU WANT TO OVERRIDE THIS PLEASE DO return parent::afterRun();
     *
     * @return int Status of the current job execution
     */
    public function afterRun(){

        //Default exit code to run the iteration again
        $exitCode = BackgroundJob::RUN_AGAIN;

        if($this->structure !== self::DATA_STRUCTURE_NONE){
            if(!is_array($this->_chunk)){
                if($this->_totalChunks > 0){
                    $newChunk = $this->_chunk + 1;

                    $this->_completePercentage = ($newChunk / $this->_totalChunks ) * 100;
                    $this->_jobParams['percent'] = intval($this->_completePercentage);
                    $this->_jobParams['chunk'] = $newChunk;

                } else{
                    $this->_completePercentage = 100;
                    $this->_jobParams['percent'] = $this->_completePercentage;
                }

            } else{
                $newChunk = $this->_jobParams['currentChunk'] + 1;
                $this->_completePercentage = ($newChunk / $this->_totalChunks ) * 100;
                $this->_jobParams['percent'] = intval($this->_completePercentage);
                $this->_jobParams['currentChunk'] = $newChunk;
            }

            $newJobData = $this->_redis->hgetall($this->keyMetaData());

            if($newChunk >= $this->_totalChunks){

                if($this->_totalChunks > 0) {
                    //Clean the temporary places
                    $this->clean();
                }

                $notify = CJSON::decode($newJobData['notify']);

                if(!empty($notify)){
                    //Notify the users
                    try{
                        $this->notifyClients($notify);
                    } catch(Exception $e){
                        $this->addError($e->getMessage());
                    }
                }
                //$timezone = Settings::get('timezone_default', 'Europe/Rome');
                //$nowTime = new DateTime('now', new DateTimeZone($timezone));
                
                $this->_jobParams['date_finished'] = time();//$nowTime->format('Y-m-d H:i:s');

                $this->onFinish();

                /**
                 * If we finish with all iterations - return status 0, so the job should not be executed anymore
                 */
                $exitCode = BackgroundJob::JOB_DONE;
            }

            //Save the errors and add the number of new error to the current number
            $newJobData['errors'] += $this->saveErrors();

             //Get the params from redis again, because someone may change something and we will override them
            $newJobData['percent'] = $this->_jobParams['percent'];
            $newJobData['chunk'] = $this->_jobParams['chunk'];
            if(isset($this->_jobParams['currentChunk'])){
                $newJobData['currentChunk'] = $this->_jobParams['currentChunk'];
            }
            if(isset($this->_jobParams['date_finished'])){
                $newJobData['date_finished'] = $this->_jobParams['date_finished'];
            }

            //Replace the old params to the Redis
            $newerJobData = $this->_redis->hgetall($this->keyMetaData());
            $newJobData['abortAuthorName'] = $newerJobData['abortAuthorName'];
            $newJobData['abortAuthorId'] = $newerJobData['abortAuthorId'];
            $newJobData['aborted'] = $newerJobData['aborted'];

            $this->_redis->hmset($this->keyMetaData(), $newJobData);

            //Publish to all clients, so the nodejs server can do the live update;
            if($this->jobType === self::JOB_TYPE_USER){
                $this->_redis->publish('job_' . $this->_domain, $this->_hash_id);
            }
        } else{
            $this->_redis->del($this->keyMetaData());
            $exitCode = BackgroundJob::JOB_DONE;
        }
        /**
         * If we still did not finish the process - return status 1 and run the job again
         */
        return $exitCode;
    }

    /**
     *  @TODO (describe)
     */
    public function clean(){
        if($this->storage === self::STORAGE_MYSQL){
            if(in_array($this->structure, array(self::DATA_STRUCTURE_SIMPLE, self::DATA_STRUCTURE_MATRIX))){
                $this->dropTemporaryTable();
            }
        } else if($this->storage === self::STORAGE_REDIS){
            if($this->structure === self::DATA_STRUCTURE_SIMPLE){
                $this->_redis->del($this->keyData());
            } else if($this->structure === self::DATA_STRUCTURE_MATRIX){
                $jobs = $this->_redis->keys($this->keyData() . ':*');

                if(!empty($jobs)){
                    foreach($jobs as $job){
                        $this->_redis->del($job);
                    }
                }
            }
        }
    }

    /**
     * Drops the temporary table for the current job
     */
    private function dropTemporaryTable(){
        $dropCommand = Yii::app()->db->createCommand();
        /**
         * @var $dropCommand CDbCommand
         */
        try{
            $dropCommand->dropTable($this->_tableName);
        } catch (Exception $e){
            $this->addError($e->getMessage());
        }
    }

    /**
     * Send notification (email) to the subscribed users
     *
     * @param $clients array ID's of the users, who want to be notified
     * @return bool|int Result of the sending the email
     */
    private function notifyClients($clients){
        $command = Yii::app()->db->createCommand();
        /**
         * @var $command CDbCommand
         */
        $command->select('email, idst')
            ->from(CoreUser::model()->tableName())
            ->where('idst IN (' . implode(',', $clients) . ')');

        $mails = $command->queryAll();

        $res = array();

        foreach($mails as $mail){
            $userMail = $mail['email'];

            $mm = new MailManager();
            $mm->setContentType('text/html');

            $now = Yii::app()->localtime->toUserLocalDatetime($mail['idst'], Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'));
            $sender = array(Settings::get('mail_sender', 'noreply@docebo.com'));

            $name = CJSON::decode($this->_jobParams['name']);

            $params = array();
            foreach ($name['params'] as $key => $param){
                $params['{' . $key . '}'] = $param;
            }

            $nameString = Yii::t($name['module'], $name['value'], $params);


            $userLang = CoreUser::getUserLanguage($mail['idst']);

            $langCode = Lang::getBrowserCodeByCode($userLang);

            $message = Yii::t('backgroundjobs', "Job <strong>{jobName}</strong> has been completed on {time}", array(
                '{jobName}' => $nameString,
                '{time}' => $now
            ), null, $langCode);

            $result = $mm->mail($sender, array($userMail), Yii::t('backgroundjobs','Background job finished', array(), $langCode), $message);

            if($result){
                $res[] = true;
            }
        }

        return !empty($res);
    }

    /**
     *
     * Add custom error message to job errors
     *
     * @param $message string Error Message
     */
    public function addError($message, $username=false) {
        if($message){
            $this->_errors[] = array(
                'time' => Yii::app()->localtime->getUTCNow('Y-m-d H:i:s').' UTC',
                'user'  => ($username)? $username : '',
                'message' => $message
            );
        }
    }

    /**
     * Get the chunked data for the specific key
     *
     * For example: $users = $this->get('users', array(1234, 1234));
     *
     * @param $column string The name of the array the the handler wants to get
     * @param array $default Default value to be returned if the column not found
     * @return array Data
     */
    public function get($column, $default = array()){
        $result = $default;
        if(!empty($column)){
            if(!empty($this->data[$column])){

                if(!is_array($this->data[$column])){
                    $result = $this->data[$column];
                } else{
                    foreach($this->data[$column] as $data){
                        if($data){
                            $result[] = $data;
                        }
                    }
                }
            }else if(!empty($this->_trash[$column])){
                $result = $this->_trash[$column];
            }
        }

        return $result;
    }

    /**
     * Gets a param with the specific name. I there is not such a param, the default value will be returned
     *
     * @param $paramName string Param name
     * @param string $default Default value to be returned if there is no such a param
     * @return string|array The additional param value
     */
    public function getParam($paramName, $default = ''){
        $result = $default;
        if(!empty($paramName)){
            if(isset($this->params[$paramName])){
                $result = $this->params[$paramName];
            }
        }

        return $result;
    }

    /**
     * Saving errors in CSV and upload the file to S3 storage
     *
     * @return int Number of errors, stored in CSV
     * @throws CException
     */
    public function saveErrors(){
        $errorsCount = 0;
        if(!empty($this->_errors)){
            $fh = fopen($this->_filePath, 'a');
            foreach($this->_errors as $error) {
                $errorsCount++;
                $output = CSVParser::arrayToCsv($error);
                fputs($fh, $output . "\n");
            }

            fclose($fh);

            $storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_COMMON);
            $storage->storeAs($this->_filePath, $this->_filename, 'jobs');
        }

        return $errorsCount;
    }

    /**
     * Collects custom data for the job.
     * Its used if you don't want to pass some input the the job, but you want to collect this data dynamically.
     *
     * @return array Custom data
     */
    public function collectData(){
        return array();
    }

    /**
     * @TODO (describe)
     * 
     * @param unknown $hash_id
     * @param array $inputData
     */
    public function prepareData($hash_id, $inputData = array()){

        /**
         * @var $transaction CDbTransaction
         */

        $data = array();

        switch($this->structure){
            case self::DATA_STRUCTURE_MATRIX:
                $data = $this->prepareMatrixInput($inputData, $hash_id);
                break;
            case self::DATA_STRUCTURE_SIMPLE:
                $data = $this->prepareSimpleInput($inputData, $hash_id);
                break;
            case self::DATA_STRUCTURE_CUSTOM:
                $data = $this->prepareCustomInput($inputData, $hash_id);
                break;
        }

        return $data;
    }

    /**
     * Prepare data for matrix like input structure
     * @NOTE: you should change $structure to self::DATA_STRUCTURE_MATRIX to use this method
     *
     * @param array $input Input data
     * @param $hash_id string
     * @return array
     */
    protected function prepareMatrixInput($input = array(), $hash_id){

        $chunkSize = self::MAX_MATRIX_ITEMS;

        if($this->chunkSize !== self::CHUNK_SIZE_AUTO && is_numeric($this->chunkSize)){
            $chunkSize = $this->chunkSize;
        }

        if($this->storage === self::STORAGE_MYSQL){
            $tableName = 'job_' . $hash_id;

            $command = Yii::app()->db->createCommand();


            $tableColumns = $this->_prepareMultiColumns($input);

            /**
             * Creating temporary table for that job to store data on it!
             *
             * @var $command CDbCommand
             */
            $command->createTable($tableName, $tableColumns);

            $columnNames = $this->getColumnNames($input);
            $columnsCount = count($columnNames);

            $totalChunks = $this->getMatrixTotalChunkNumber($input, $columnsCount, $chunkSize, true);

            //Prepare data chunked for better insertion in DB
            $chunkedData = $this->getChunkedMatrixData($input, $columnsCount, $chunkSize);

            //Insert the chunked data in DB
            foreach($chunkedData['data'] as $data){
                $insertValues = $this->_buildInsertValuesForMulti($data, $columnNames);

                $sql = "INSERT INTO `" . $tableName . "` (" . implode(',', $columnNames) . ") VALUES " . $insertValues;
                Yii::app()->db->createCommand($sql)->execute();
            }

            return array(
                'chunkSize' => ceil($chunkSize / $columnsCount),
                'columns' => $columnsCount,
                'totalItems' => $chunkedData['totalItems'],
                'totalChunks' => $totalChunks
            );
        } else if($this->storage === self::STORAGE_REDIS){
            $columnNames = $this->getColumnNames($input);
            $columnsCount = count($columnNames);

            $totalChunks = $this->getMatrixTotalChunkNumber($input, $columnsCount, $chunkSize);

            $result = $this->getChunkedMatrixData($input, $columnsCount, $chunkSize);

            $data = $result['data'];
            $chunk = $this->initMatrixChunk($columnNames, true);

            foreach($data as $item){
                $index = 0;
                foreach($item as $key => $value){
                    if(count($value) <= 0) continue;

                    $this->_redis->rpush($this->keyData() . ':' . $columnNames[array_search($key,$columnNames)], CJSON::encode($value));

                    $index++;
                }
            }

            return array(
                'totalChunks' => $totalChunks,
                'chunkSize' => ceil($chunkSize / $columnsCount),
                'chunk' => $chunk,
                'currentChunk' => 0
            );
        }
    }

    /**
     * Get the number of total chunks
     *
     * The result found by:
     *
     * Foreach element we get the total items and divide it by the max chunk size divided by the number of columns
     * if the result of the divide is less that 1, we should consider it as 1, otherwise we will receive a
     * bigger chance if mistake.
     * And then multiply the result for each item of the array
     *
     * @NOTE We don't round the $tmpResult after first calculation, because we want to find the minimal mistake
     */
    protected function getMatrixTotalChunkNumber($input, $columnsCount, $chunkSize = self::MAX_MATRIX_ITEMS, $useMinimalMistake = false){

        $result = 1;
        foreach($input as $key => $value){
            $itemsCount = count($value);
            if($itemsCount > 0){
                $tmpResult = $itemsCount / ($chunkSize / $columnsCount);
                if($tmpResult < 1){
                    $tmpResult = ceil($tmpResult);
                }
                if($useMinimalMistake){
                    $result = $result * $tmpResult;
                } else{
                    $result = $result * ceil($tmpResult);
                }
            }
        }

        return ceil($result);
    }

    /**
     * @TODO (describe)
     * 
     * @param unknown $input
     */
    protected function getColumnNames($input){
        $index = 0;
        $columnNames = array();
        //Get the names of keys for each array. If the current array is empty - skip it
        foreach($input as $key => $value){
            if(count($value) === 0) continue;
            $columnName = 'data_' . $index;

            if(is_string($key)){
                $columnName = $key;
            }

            $columnNames[] = $columnName;
        }

        return $columnNames;
    }

    /**
     * @TODO (describe)
     * 
     * @param unknown $columns
     * @param string $includeColumnNames
     */
    protected function initMatrixChunk($columns, $includeColumnNames = false){
        $chunk = array();
        $columnsCount = count($columns);

        for($i = 0; $i < $columnsCount; $i++){
            if($includeColumnNames){
                $chunk[$columns[$i]] = 0;
            } else{
                $chunk[$i] = 0;
            }
        }

        return $chunk;
    }

    /**
     * Returns input data, chunked in parts
     *
     * @param $input array Input data
     * @return array Array contains: chunked data and number of all items
     */
    protected function getChunkedMatrixData($input, $columnsCount, $chunkSize = self::MAX_MATRIX_ITEMS){
        $data = array();
        $totalItems = 1;

        foreach($input as $key => $value){
            $valueCount = count($value);

            //If there is empty array - skip it.
            if($valueCount === 0) continue;

            $totalItems *= $valueCount;

            $index = 0;
            for($i = 0; $i < $valueCount; $i++){
                if($i >= (($index + 1) * ceil($chunkSize / $columnsCount))){
                    $index++;
                }

                $data[$index][$key][] = $value[$i];
            }
        }

        return array(
            'data' => $data,
            'totalItems' => $totalItems
        );
    }

    /**
     * Build a SQL query for multiple inserts.
     * At this point we are assuming that the given array is chunked already, because it may cause a BIG problem if
     * the array is too big
     *
     * @param $data array Array of data.
     * @param $columnNames array All columns that will be stored in the temporary table
     * @return string SQL like query for inserting multiple items at once
     */
    private function _buildInsertValuesForMulti($data, $columnNames){
        $output = '';
        $totalKeys = count($data);
        $index = 0;

        //Get the number of the larger array, so then we can build a proper data
        $maxItems = $this->getMaxArraySize($data);

        for($i = 0; $i < $maxItems; $i++){
            $output .= '(';
            $dataItems = array();
            $countColumns = count($columnNames);

            if($totalKeys !== $countColumns){
                $columns = array_keys($data);
                foreach($columnNames as $columnName){
                    if(!in_array($columnName, $columns)){
                        $dataItems[$columnName] = 'NULL';
                    }
                }
            }

            foreach($data as $key => $value){
                if(isset($value[$i])){
                    //Encode to JSON in case of array
                    $encodeValue = CJSON::encode($value[$i]);
                    $dataItems[$key] = $encodeValue;
                } else{
                    //If there is no value here - fill it with NULL
                    $dataItems[$key] = 'NULL';
                }
            }

            //Arrange the data by the columns
            $arrangeData = array();
            foreach($columnNames as $columnName){
                $arrangeData[] = $dataItems[$columnName];
            }

            $output .= implode(',', $arrangeData);
            $output .= ')';
            if($index < $maxItems - 1){
                $output .= ', ';
            }
            $index++;
        }

        return $output;
    }

    /**
     * Return the max number of the items from the given multidimensional array
     *
     * @param $array array Input array to be found the max items
     * @return int Max number of elements from each arrays
     */
    private function getMaxArraySize($array){
        $max = 0;
        foreach($array as $key => $value){
            if(count($value) > $max){
                $max = count($value);
            }
        }

        return $max;
    }

    /**
     * Prepare column structure for MULTI structured input data
     *
     * @param $input array Input array
     * @return array Columns structure, used by yii framework -> createTable()
     */
    private function _prepareMultiColumns($input){
        $output = array(
            'id' => 'pk'
        );

        $index = 0;
        foreach($input as $key => $value){
            if(count($value) === 0) continue;
            $columnName = 'data' . $index;

            if(is_string($key)){
                $columnName = $key;
            }
            $output = array_merge($output, array(
                $columnName => 'text'
            ));
            $index++;
        }

        return $output;
    }

    /**
     * Prepare data fro simple input structure
     * @NOTE: you should change $structure to self::DATA_STRUCTURE_SIMPLE to use this method
     *
     * @param array $input Input data
     * @param $hash_id string
     * @return array
     */
    protected function prepareSimpleInput($input = array(), $hash_id){

        if($this->storage === self::STORAGE_MYSQL){
            $command = Yii::app()->db->createCommand();

            $tableName = 'job_' . $hash_id;


            $tableColumns = array(
                'id' => 'pk',
                'data' => 'text'
            );

            /**
             * Creating temporary table for that job to store data on it!
             *
             * @var $command CDbCommand
             */
            $command->createTable($tableName, $tableColumns);

            $chunkSize = $this->_calculateChunk($input);

            $data = array();
            foreach($input as $key => $value){
                //Encode the value in case of array
                $data[] = CJSON::encode($value);
            }

            $chunkArray = $this->chunkData($data, $chunkSize);

            foreach($chunkArray as $key => $val){
                $sql = "INSERT INTO `" . $tableName . "` (data) VALUES ('" . implode("'), ('", $val) . "')";
                Yii::app()->db->createCommand($sql)->execute();
            }

            return array(
                'chunkSize' => $chunkSize,
                'columns' => 1
            );
        } else if($this->storage === self::STORAGE_REDIS){
            $chunkSize = $this->_calculateChunk($input);

            $chunkedData = $this->chunkData($input, $chunkSize);

            $totalChunk = count($chunkedData);

            foreach($chunkedData as $data){
                $this->_redis->rpush($this->keyData(), CJSON::encode($data));
            }

            return array(
                'totalChunks' => $totalChunk,
                'chunkSize' => $chunkSize,
                'chunk' => 0
            );
        }
    }

    /**
     * @TODO (describe)
     * 
     * @param unknown $input
     * @param unknown $chunkSize
     */
    protected function chunkData($input, $chunkSize){
        return array_chunk($input, $chunkSize);
    }

    /**
     * Calculates the chunk size depending of the size of the input array
     *
     * @param $input array Array of items
     * @return int Chunk size
     */
    protected function _calculateChunk($input){

        $countInput = count($input);

        if($countInput <= 1000){
            return $this->_calculateArraySize($input, self::INPUT_SIZE_MINI);
        } else if($countInput > 1000 && $countInput <= 100000){
            return $this->_calculateArraySize($input, self::INPUT_SIZE_NORMAL);
        } else if($countInput > 100000 && $countInput <= 500000){
            return $this->_calculateArraySize($input, self::INPUT_SIZE_MEDIUM);
        } else if($countInput > 500000 && $countInput <= 1000000){
            return $this->_calculateArraySize($input, self::INPUT_SIZE_BIG);
        } else {
            return 500;
        }
    }

    /**
     * Calculate the chunk size of the input array depending of how many keys, each item has
     *
     * @param $input array Input array
     * @param $sizeType string Max size of the Input array
     * @return int Chunk size
     */
    private function _calculateArraySize($input, $sizeType){
        $maxArrayKeys = $this->_getArrayMaxKeysLength($input);

        switch($sizeType){
            case self::INPUT_SIZE_MINI:
                return 250;
                break;
            case self::INPUT_SIZE_NORMAL:
                if($maxArrayKeys === 0 ){
                    return 1000;
                } else if($maxArrayKeys < 5){
                    return 10000;
                } else{
                    return 5000;
                }
                break;
            case self::INPUT_SIZE_MEDIUM:
                if($maxArrayKeys < 3){
                    return 5000;
                } else{
                    return 2000;
                }
                break;
            case self::INPUT_SIZE_BIG:
                if($maxArrayKeys < 2){
                    return 2000;
                } else{
                    return 1000;
                }
                break;
        }

        return 500;
    }

    /**
     * Get the max number of the keys. If empty return false
     *
     * @param $input array Input array
     * @return bool|int Max keys size or false if empty input
     */
    private function _getArrayMaxKeysLength($input){
        if(!empty($input)){
            $max = 0;
            foreach($input as $key => $value){
                if(is_array($value)){
                    $keys = array_keys($value);
                    $keysCount = count($keys);

                    if($keysCount > $max){
                        $max = $keysCount;
                    }
                }
            }

            return $max;
        }

        return false;
    }

    /**
     * Custom preparing of input data.
     *
     * @NOTE: you should change $structure to self::DATA_STRUCTURE_CUSTOM to use this method
     *
     * The function should ALWAYS return:
     *  - totalChunks: count of all chunks
     *  - chunkSize: number of items that will be processed for every chunk
     *  - chunk: It can be integer or array, holding the current processed chunk. At the beginning is should be 0
     *           or something like [0, 0]
     *
     * Optional returning values could be:
     *  - trash: array to be passed thought the redis and job handler
     *
     * @param array $input Input data
     * @param $hash_id string
     * @return array
     */
    protected function prepareCustomInput($input = array(), $hash_id){

    }

    /**
     * Use this function when the job is runned for the first time
     *
     * @return bool
     */
    public function onStart(){
        return false;
    }

    /**
     * Use this function when the job finish all iterations and cleans the temporary storage
     *
     * @return bool
     */
    public function onFinish(){
        return false;
    }
}
