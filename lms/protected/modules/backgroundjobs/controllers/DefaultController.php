<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 2/26/2016
 * Time: 1:38 PM
 */
class DefaultController extends AdminController {

    /**
     * @var $_redis Predis\Client
     */
    private $_redis;
    private $_domain;



    /**
     * (non-PHPdoc)
     * @see CController::filters()
     */
    public function filters() {
        return array(
            'accessControl',
        );
    }


    /**
     * (non-PHPdoc)
     * @see CController::accessRules()
     */
    public function accessRules() {
        return array(


            // Admins and Teachers can do all
            array('allow',
                'users'=>array('@'),
                'expression' => 'Yii::app()->user->getIsGodadmin() || Yii::app()->user->getIsPu()',
            ),

            // For Subscribers
//            array('allow',
//                'actions' => array('index', 'axLaunchpad', 'sendFile'),
//                'users'=>array('@'),
//                'expression' => 'Yii::app()->controller->userIsSubscribed && Yii::app()->controller->validLo',
//            ),

            array('deny', // if some permission is wrong -> the script goes here
                'users' => array('*'),
                'deniedCallback' => array($this, 'accessDeniedCallback'),
                'message' => Yii::t('course', '_NOENTER'),
            ),
        );

    }




    public function init()
    {
        parent::init();

        $this->_domain = Docebo::getOriginalDomain();

        require_once(Yii::getPathOfAlias('common.vendors.Predis'). "/Autoloader.php");
        Predis\Autoloader::register();

        $_redisHost = Yii::app()->params['jobs_data_redis_host'];
        $_redisPort = Yii::app()->params['jobs_data_redis_port'];
        $_redisDb   = Yii::app()->params['jobs_data_redis_db'];

        /**
         * Redis client initialization
         */
        $this->_redis = new Predis\Client(array(
            'host' 		=> $_redisHost,
            'port' 		=> $_redisPort,
            'database' 	=> $_redisDb,
        ));
    }

    public function actionIndex(){
        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/admin.css');
        $cs->registerScriptFile(BackgroundJobsWidget::getBackgroundJobsAssetsUrl().'/js/moment.js');
        Yii::app()->getClientScript()->registerScriptFile(Yii::app()->engineio->getAssetsUrl().'/js/engine.io.js');
        $cs->registerCssFile(BackgroundJobsWidget::getBackgroundJobsAssetsUrl().'/css/backgroundjobs.css');
        $cs->registerScriptFile(BackgroundJobsWidget::getBackgroundJobsAssetsUrl().'/js/backgroundjobs.js');
        $this->render('index');
    }

    public function actionCreateDummy(){

        echo Yii::app()->backgroundJob->createJob('test 1', 'lms.protected.modules.backgroundjobs.components.handlers.AddUsers');
    }

    public function actionNotify(){
        $request = Yii::app()->request;

        /**
         * @var $request CHttpRequest
         */

        $jobHash = $request->getParam('hash_id', false);

        if($jobHash){
            $jobParams = $this->_redis->hgetall($this->_domain . ':job_metadata:' . $jobHash);

            if($jobParams){
                $notify = CJSON::decode($jobParams['notify']);

                $userId = Yii::app()->user->id;

                if(!in_array($userId, $notify)){
                    $notify[] = $userId;

                    $jobParams['notify'] = CJSON::encode($notify);
                    $this->_redis->hmset($this->_domain . ':job_metadata:' . $jobHash, $jobParams);

                    $this->sendJSON(array(
                        'success' => true
                    ));
                }
            }
        }

        $this->sendJSON(array(
            'success' => false
        ));
    }

    public function actionDelete(){
        $request = Yii::app()->request;

        /**
         * @var $request CHttpRequest
         */

        $hash_id = $request->getParam('hash_id', false);

        if($hash_id){
            if($this->_redis->exists($this->_domain . ':job_metadata:' . $hash_id)){
                $jobParams = $this->_redis->hgetall($this->_domain . ':job_metadata:' . $hash_id);

                $userModel = CoreUser::model()->findByPk(Yii::app()->user->id);
                $jobParams['aborted'] = 1;
                $jobParams['date_finished'] = time();
                $jobParams['abortAuthorId'] = $userModel->idst;
                $jobParams['abortAuthorName'] = $userModel->getFullName();

                $this->_redis->hmset($this->_domain . ':job_metadata:' . $hash_id, $jobParams);
                $this->_redis->publish('job_' . $this->_domain , $hash_id);

                $this->sendJSON(array(
                    'success' => true
                ));
            }
        }

        $this->sendJSON(array(
            'success' => false
        ));
    }

    /**
     * Download CSV file with job errors.
     *
     * Expected request params:
     *
     *  - hash_id : Hash ID of the job
     *
     * @throws CException
     */
    public function actionDownloadErrors(){
        $request = Yii::app()->request;

        /**
         * @var $request CHttpRequest
         */

        $hash_id = $request->getParam('hash_id', false);

        if($hash_id){
            $storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_COMMON);
            $storage->sendFile('errors_' . $hash_id . '.csv', 'errors_' . $hash_id . '.csv', 'jobs');
        }
    }

    public function actionListOfBGJobs(){
        $this->renderPartial('_jobsList');
    }

}