<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 2/26/2016
 * Time: 1:30 PM
 */
class BackgroundjobsModule extends CWebModule{
    public function init()
    {
        parent::init();

        $this->setImport(array(
            'backgroundjobs.components.*',
            'backgroundjobs.components.handlers.*',
            'backgroundjobs.widgets.*'
        ));

		JsTrans::addCategories(array('backgroundjobs'));
        Yii::app()->event->on('onBeforeLogoutLink', array($this, 'onBeforeLogoutLink'));
    }

    public function onBeforeLogoutLink(DEvent $event) {
        $descriptor = array(
            'viewerId' => 'backgroundJobsViewer',
            'viewerIcon' => 'fa-tasks',
            'viewerSize' => 1,
//            'loadTooltipUrl' => Docebo::createLmsUrl('//backgroundjobs/default/listOfBGJobs'),
            'defaultHeaderTitle' => Yii::t('backgroundjobs', 'Background jobs'),
            'activeCount' => 0
        );

        if(!(strrpos(Yii::app()->request->requestUri,'backgroundjobs/default') && Yii::app()->controller->action->id=='index' && Yii::app()->controller->id=='default')) {
            if (Yii::app()->backgroundJob->hasAnyEnabledJob()) {
                Yii::app()->getController()->widget('BackgroundJobsWidget', $descriptor);
            }
        }


    }

    public function registerResources(){
        $cs = Yii::app()->getClientScript();
        $themeUrl = Yii::app()->theme->baseUrl;
        $cs->registerScriptFile($themeUrl . '/js/require_js/require.js');
		$cs->registerScript("bjobs_path", 'bjobsScriptPath = "'.BackgroundJobsWidget::getBackgroundJobsAssetsUrl().'"', CClientScript::POS_HEAD);
		//$cs->registerScript("mainWidget", "require(['".BackgroundJobsWidget::getBackgroundJobsAssetsUrl()."/js/main_bjobs.js']);", CClientScript::POS_READY);
// $cs->registerScriptFile(BackgroundJobsWidget::getBackgroundJobsAssetsUrl() . '/js/main_bjobs.js');
    }
}