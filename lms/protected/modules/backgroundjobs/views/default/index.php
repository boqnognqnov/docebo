<style>

</style>
<?php $this->breadcrumbs = array(
    Yii::t('backgroundjobs', 'Background jobs'),
); ?>
<div class="background-jobs-tabs tabbable">
    <ul class="nav nav-tabs advanced-sidebar">
        <? if(Yii::app()->user->getIsGodadmin()){?>
        <li class="active hover-cancel">
            <a class="ajax-change active  hover-cancel" data-toggle="tab" data-target="#alljobs" href="#alljobs">
<!--                <i class="gamification-sprite star-square-small-white when-active"></i>-->
<!--                <i class="gamification-sprite star-square-small-black"></i>-->
                <?= Yii::t('backgroundjobs','All Jobs');?>
            </a>
        </li>
        <? }?>
        <li>
            <a class="ajax-change <?=(Yii::app()->user->getIsGodadmin())? '' : 'active  hover-cancel' ;?>" data-toggle="tab" data-target="#myjobs" href="#myjobs">
<!--                <i class="gamification-sprite podium-small-white when-active"></i>-->
<!--                <i class="gamification-sprite podium-small-black"></i>-->
                <?= Yii::t('backgroundjobs','My Jobs');?>
            </a>
        </li>
    </ul>

    <div class="tab-content <?= (Lang::isRTL(Yii::app()->getLanguage())) ? 'rightToLeftCustom' : '' ?>">
        <? if(Yii::app()->user->getIsGodadmin()){?>
            <div id="alljobs" class="tab-pane active">
            </div>
        <? }?>
        <div id="myjobs" class="tab-pane <?=(Yii::app()->user->getIsGodadmin())? '' : 'active';?>">
        </div>
    </div>
</div>
<script type="text/javascript">

</script>
<script>

    $(document).ready(function () {

		var bgJobsOptions = {
            domain: '<?=Docebo::getOriginalDomain();?>',
            session: <?= json_encode(Yii::app()->request->cookies['docebo_session']->value) ?>,
            wsHost: <?= json_encode(Yii::app()->backgroundJob->websocketServerIp) ?>,
            wsPort: <?= json_encode(Yii::app()->backgroundJob->websocketServerPort) ?>,
       		wsUseHttps: <?= json_encode(Yii::app()->backgroundJob->websocketUseHttps) ?>,

	    	// Jobsa Data				                
	    	redisHost:          null,
	    	redisPort:          null,
	    	redisJobDb:         null,
	    	// PHP Sessions                
	    	redisSessionHost:   null,
	    	redisSessionPort:   null,
	    	redisSessionDb:     null,
                    
            notificationUrl: '<?=Yii::app()->createUrl('/backgroundjobs/default/notify')?>',
            abortUrl: '<?=Yii::app()->createUrl('/backgroundjobs/default/delete')?>',
            downloadUrl : '<?=Yii::app()->createUrl('/backgroundjobs/default/downloaderrors')?>',
            userId: <?= Yii::app()->user->idst;?>,
            isWidget: false,
            isPU: <?= (Yii::app()->user->getIsPu())? 'true' : 'false' ;?>,

       	  	sendRedisConfig: 	false

		};
    	
    	<?php 
    	   // If Websocket dow not have its own configuration loaded, SEND config over wire 
    	   if (!Yii::app()->backgroundJob->websocketHasOwnConfig) { 
    	?>
    		bgJobsOptions = $.extend({}, bgJobsOptions, {
	    		// Jobsa Data				                
	    		redisHost:          <?= json_encode(Yii::app()->params['jobs_data_redis_host']) ?>,
	    		redisPort:          <?= json_encode(Yii::app()->params['jobs_data_redis_port']) ?>,
	    		redisJobDb:         <?= json_encode(Yii::app()->params['jobs_data_redis_db']) ?>,
		    	// PHP Sessions                
		    	redisSessionHost:   <?= json_encode(Yii::app()->params['redis_sessions_host']) ?>,
	    		redisSessionPort:   <?= json_encode(Yii::app()->params['redis_sessions_port']) ?>,
	    		redisSessionDb:     <?= json_encode(Yii::app()->params['redis_sessions_db']) ?>,
                dateFormat: 		<?= json_encode(Yii::app()->localtime->getLocalDateTimeFormat()) ?>,
                timeOffset:         <?= json_encode(Yii::app()->localtime->getTimezoneOffset(Yii::app()->localtime->getTimeZone(), true)) ?>,

	    		sendRedisConfig: 	true    	
			});
	   <?php } ?>

        var BGJobs = new BGJobsLoader(bgJobsOptions);

        $('.background-jobs-tabs a.ajax-change').each(function(){
            $(this).on('click', function(event){
                $('.background-jobs-tabs .nav.nav-tabs.advanced-sidebar li').removeClass('hover-cancel');
                $('.background-jobs-tabs .nav.nav-tabs.advanced-sidebar li').removeClass('active');
                $('.background-jobs-tabs .nav.nav-tabs.advanced-sidebar a').removeClass('hover-cancel').removeClass('active');
                $(this).parent().addClass('hover-cancel');
                $(this).addClass('active hover-cancel');
            });
        });
        $('.background-jobs-tabs .nav.nav-tabs.advanced-sidebar a').mouseout(function() {
            if (!$(this).hasClass('hover-cancel'))
                $(this).removeClass('active');
            if (!$(this).parent().hasClass('hover-cancel'))
                $(this).parent().removeClass('active');

        }).mouseenter(function() {
            if (!$(this).hasClass('active'))
                $(this).addClass('active');
            if (!$(this).parent().hasClass('active'))
                $(this).parent().addClass('active');

        });
    });

</script>