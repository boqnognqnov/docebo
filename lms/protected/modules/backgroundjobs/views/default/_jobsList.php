<?php
/**
 * Created by PhpStorm.
 * User: NikVas
 * Date: 14-Mar-16
 * Time: 15:37
 */
?>
<div id="inbox-bg-jobs" class="tab-pane background-jobs-viewer background-jobs-tabs">
</div>
<script>
    /*
    We reset the BGJobsLoader object we used to count the active jobs
    * */
    InboxBGJobs.BackgroundJobs = [];
    InboxBGJobs.counting = false;
    InboxBGJobs.openSocket();
</script>
