<?php
/**
 * A component directly responsible to execute a job by its HASH ID.
 * It will load the job model, inspect it, find the right JobHandler class, create an instance of it and call uts own run() method in turn.
 *
 * This component basically does not depend on anything else but on the job hash id. So, it can be called from anywhere (like models, API, ...)
 *
 * @package lms.modules
 * @subpackage job
 *
 */
class JobExecutor extends CApplicationComponent {

	/**
	 * Enable/Disable logging job runs in core_job_log table. Set it in main.php
	 * @var boolean
	 */
	public $log = false;



	public function init() {
		parent::init();
	}

	/**
	 * Run the job, using its handler class
	 *
	 * @param string $hash_id Job Hash Id
	 * @throws CException
	 */
	public function run($hash_id) {

		// Get job model by hash_id
		$job = CoreJob::model()->findByAttributes(array('hash_id' => $hash_id));
		$logCat = "job-" . $job->id_job . " " . $job->handler_id;

		// Validate job call 
		if (!$hash_id || !$job) {
			$message = "Invalid job called";
			if ($hash_id) {
				$message .= " (hash ID: $hash_id) ";
			}
			else {
				$message .= ": Missing hash ID";
			}
			Yii::log($message, 'warning', $logCat);
			
			// Just ignore the call
			return;
		}
		// Inactive jobs are .. NOT executed at all (controlled by LMS administrative UI)
		if (!$job->active)
			return;

		// Do some profiling/logging
		$timeStart = microtime(true);
		Yii::log('>>> Starting Job: ' . $job->name, 'debug', $logCat);

		$_jobIsReusable = false;

		try {

			// Job Logger
			if ($this->log) {
				$jobLog = new CoreJobLog();
				$jobLog->id_job = $job->id_job;
			}

			// Get Job Handler component (using job model to get the handler type/id/class path)
			$jh = JobHandler::getInstance($job);

			// Allow more time to run
			set_time_limit(1800); // seconds

			// RUN Forest RUN.. but wait... ?
			// Is this job is of type recurring (like custom report generation)  ?
			if ($job->type == CoreJob::TYPE_RECURRING) {
				Yii::log('Type: RECURRING', 'debug', $logCat);
				if ($job->isRecurringJobDue()) {
					Yii::log('IS Due', 'debug');
					// Mark the starting time
					$job->last_started = Yii::app()->localtime->getLocalNow();

					if ($this->log) {
						$jobLog->started = $job->last_started;
					}

					// Ook... Run!
					$jh->run();

					$_jobIsReusable = $jh->canReuseSameImmediateJob();

					$job->last_status = CoreJob::STATUS_SUCCESS;
					$job->last_error_message = null;
				}
				else {
					Yii::log('IS NOT Due yet', 'debug', $logCat);
				}
			}
			// Ok, looks like we just have to run the job handler whenever we are asked
			else {
				$job->last_started = Yii::app()->localtime->getLocalNow();
				if ($this->log) {
					$jobLog->started = $job->last_started;
				}

				// Ook... Run!
				$jh->run();

				$_jobIsReusable = $jh->canReuseSameImmediateJob();

				$job->last_status = CoreJob::STATUS_SUCCESS;
				$job->last_error_message = null;
			}


		}
		catch (Exception $e) {
			// We log all exception errors
			Yii::log($e->getMessage(), 'error', $logCat);
			$job->last_status = CoreJob::STATUS_FAILED;
			$job->last_error_message = $e->getMessage();
		}


		$timeEnd = microtime(true);
		$elapsedTime = $timeEnd - $timeStart;
		Yii::log('<<< Finished Job: ' . $job->name . " ($elapsedTime sec) ", 'debug', $logCat);

		// Mark the ending time
		$job->last_finished = Yii::app()->localtime->getLocalNow();
		$job->save();

		// JOB Logger
		if ($this->log) {
			$jobLog->finished = $job->last_finished;
			$jobLog->status = $job->last_status;
			$jobLog->message = $job->last_error_message;
			$jobLog->json_job = json_encode($job->attributes);
			$jobLog->save();
		}

		// Self-delete ONE-TIME jobs; They are supposed to run only once and die
		if (($job->type == CoreJob::TYPE_ONETIME) && !$_jobIsReusable)
			$job->delete();

		// Roll a dice and create a Garbage Cleaning job (another one) for Core Job table
		// Prevent Garbage Cleaner to invoke itself again
		if (($job->handler_id != GarbageCleaner::id()) && Docebo::dice(GarbageCleaner::getProbability(GarbageCleaner::TARGET_JOBS))) {
			Yii::app()->scheduler->createImmediateJob('GarbageCleaner', GarbageCleaner::id(), array(
				'target' => GarbageCleaner::TARGET_JOBS,
				'days'	 => 7,	
			));
		}
				
			
	}


}