<?php
/**
 * 
 * @package lms.modules
 * @subpackage job
 *
 */
class DefaultController extends Controller
{

	/**
	 * Enable the job executor to wait some seconds if the job record is not found initially. It will check continuously
	 * for job existence until this time limit ends. If job is not found yet, error behavior is applied, otherwise
	 * it starts the job regularly.
	 */
	const JOB_WAITING_SECONDS = 10;

	/**
	 * We call this module/controller mainly for RUNNING a job
	 * @var string
	 */
	public $defaultAction = 'run';


	/**
	 * Run the called job, using JobExecutor 
	 * 
	 */
	public function actionRun() {
		$hash_id = Yii::app()->request->getParam("hash_id", false);

		$job = CoreJob::model()->findByAttributes( array( 'hash_id' => $hash_id ) );

		// ---
		if (!empty($hash_id) && empty($job) && self::JOB_WAITING_SECONDS > 0) {
			// sometimes the job record may be created with some delay, so wait a bit and try to check for job record until a given time
			$waitingTime = self::JOB_WAITING_SECONDS * 1000000; // {n} seconds, max waiting time for the job record generation to be awaited (in microseconds)
			$sleepTime = 50000; //check roughly every 0.05 seconds
			$passedTime = 0;
			$startTime = floor(microtime(true) * 1000000);
			$securityCheck = ceil($waitingTime / $sleepTime) + 10; // avoid every possibility to generate infinite loop, just keep slightly above max possible attempts
			while (empty($job) && $passedTime < $waitingTime && $securityCheck > 0) {
				usleep($sleepTime); // wait a bit before retrying to read job record
				$job = CoreJob::model()->findByAttributes(array('hash_id' => $hash_id));
				$currentTime = floor(microtime(true) * 1000000);
				$passedTime = $currentTime - $startTime;
				$securityCheck--;
			}
		}
		// ---

		if($job && $job->handler_id == 'plugin.AutomationApp.components.AutomationRuleJobHandler' ) {
			/* @see AutomationRuleJobHandler::id() */
			$runner      = new CConsoleCommandRunner();
			$commandPath = Yii::getPathOfAlias('common.commands');
			$runner->addCommands( $commandPath );
			$args = array(
				'yiic-domain ' . Docebo::getOriginalDomain(),
				'job',
				'execute',
				"--hash_id={$hash_id}",
			);
			ob_start();
			$runner->run( $args );
			$result = htmlentities( ob_get_clean(), NULL, Yii::app()->charset );
			return;
		}

		Yii::app()->jobexec->run($hash_id);
	}

}