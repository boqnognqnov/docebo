var inboxApp = {
	ajaxUrl: null,
	onlyUnread: 0,
	currentPage: 1,
	that: this,
	init: function(ajaxUrl){
		this.ajaxUrl = ajaxUrl;
		return this;
	},
	/**
	 * Refreshes the UI by using the current
	 * properties in this JS class and making
	 * an ajax call. This will call
	 * "this.callback(returnedHtml)" afterwards
	 */
	load: function(){
		var that = this;
		$.ajax(this.ajaxUrl, {
			data: {
				onlyUnread: this.onlyUnread,
				currentPage: this.currentPage
			},
			dataType: 'json',
			success: function(data){
				that.callback(data)
			}
		});
	},
	loadNextPage: function(){
		this.currentPage = this.currentPage + 1;
		this.load();
	},
	/**
	 * What happens after each ajax call to retrieve items
	 * (also called on pagination)
	 * @param res - the returned HTML by the ajax
	 */
	callback: function(res){
		if(res.success) {
			if (this.currentPage === 1) {
				$('.inbox-wrapper.full .items-wrapper').html(res.html);
			} else if (this.currentPage > 1) {
				$('.inbox-wrapper.full .items-wrapper').append(res.html);
			}
		}else{
			// Hide the "Load more" link
			$('.load-more').hide();
			$('.no-more').show();
		}

	}
};

$(function(){
	$(document).on('click', '.inbox-wrapper .unread-tooltip .circle', function(){

		var defaultInbox = $("#defaultInbox");
		var notification = defaultInbox.find('div[class="count"]');
		var notificationCount = parseInt(notification.html());

		var markAsRead = $(this).hasClass('read');


		$.post('?r=inbox/inbox/axMarkUnmarkAsRead', {
			itemId: $(this).data('item-id')
		}, function(){
			// Refresh the tooltip
			if(markAsRead){
				notification.html(notificationCount + 1);
			}else{
				notification.html(notificationCount - 1);
			}

			$('.inbox-bell > .fa.fa-bell').click();
		});
	});

	$(document).delegate("#inbox-dialog", "dialog2.ajax-complete", function () {

		var inboxDialogClass = $('.inbox-dialog');
		var defaultInbox = $("#defaultInbox");
		var notification = defaultInbox.find('div[class="count"]');
		var notificationCount = parseInt(notification.html());
		
		if (!inboxDialogClass.hasClass('inbox-item-readed')) {
			$.post('/lms/index.php?r=inbox/inbox/axMarkUnmarkAsRead', {
				itemId: $(this).find('#inbox-id').val()
			}, function () {
				notification.html(notificationCount - 1);
			});
		}
	});

});