<style>
	.iconClass{
		display: table-cell;
		vertical-align: middle;
	}
	.dotLibContainer{
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden;
		position: relative;
	}
	.readFullHTML{
		float: right;
		z-index: 100;
		position: relative;
	}
	.bodyContainer{
		display: inline-block;
		overflow: hidden;
		text-overflow: ellipsis;
	}
</style>
<?php /* @var $itemsOrganized array */ ?>
<? foreach($itemsOrganized as $unixTimestamp => $itemsArr): ?>
	<p><b><?=$itemsArr['label']?></b></p>

	<?php foreach($itemsArr['items'] as $renderers): ?>
		<?php foreach($renderers as $renderer): ?>
		<? /* @var $renderer BaseNotificationRenderer */ ?>
		<div class="inbox-item" style="display: table;">
			<div class="iconClass">
				<a href="javascript:void(0);">
					<div data-item-id="<?=$renderer->id?>" data-item-readed="<?=$renderer->is_read; ?>" class="circle <?=$renderer->is_read ? 'read' : ''?>" style="margin-top: 2px"></div>
				</a>
			</div>
			<div class="iconClass">
				<div style="background-color: #E4E6E5;border-radius: 50%;width: 32px;height: 32px">
					<a data-dialog-class="inbox-dialog <?=$renderer->is_read ? 'inbox-item-readed' : ''?>" class="open-dialog fa fa-bell" style="padding: 10px 9px" href="<?=Docebo::createLmsUrl('inbox/inbox/renderFullHTMLMessage', array('id' => $renderer->id))?>"></a>
				</div>
<!--				--><?// if($renderer->iconType==BaseNotificationRenderer::ICON_TYPE_URL): ?>
<!--					<img class="icon" src="--><?//=$renderer->getIcon()?><!--" alt=""/>-->
<!--				--><?// elseif($renderer->iconType==BaseNotificationRenderer::ICON_TYPE_HTML): ?>
<!--					--><?//=$renderer->getIcon()?>
<!--				--><?// endif; ?>
			</div>

			<div class="right-side">
				<div>
					<?=$renderer->getMessageText()?>
				</div>
				<div class="muted">
					<?=Yii::app()->localtime->toLocalDateTime($renderer->timestamp)?>
				</div>
			</div>
		</div>
		<br/>
		<div class="clearfix"></div>
		<?php endforeach; ?>
	<? endforeach; ?>
<? endforeach;?>
<script>
	function resizeTextBody(){
		// TODO 76 is the "magic" sum of the width of the two .iconClass elements plus margin-left of the .right-side element,
		// TODO must be updated within any change of these three elements
		var w = $('.inbox-wrapper.full').width() - 76;
		$('.dotLibContainer').width(w);
		var linkWidth = $('.readFullHTML').first().width();
		$('.bodyContainer').width(w-linkWidth);
	}
	$(function(){
		$(document).controls();
		resizeTextBody();
	})
	$(window).resize(resizeTextBody);
</script>
