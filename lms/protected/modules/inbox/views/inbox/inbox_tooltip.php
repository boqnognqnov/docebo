<style>
	.iconClass{
		display: table-cell;
		vertical-align: middle;
	}
</style>
<? foreach($items as $renderer): ?>
	<? /* @var $renderer BaseNotificationRenderer */ ?>
	<div class="inbox-item" style="display: table">
		<div class="iconClass">
			<a href="javascript:void(0);">
				<div data-item-id="<?=$renderer->id?>" class="circle  <?=$renderer->is_read ? 'read' : ''?>" style="margin-top: 2px"></div>
			</a>
		</div>

		<div class="iconClass bell-icon">
			<div style="background-color: #E4E6E5;border-radius: 50%;width: 32px;height: 32px">
				<a data-dialog-id="inbox-dialog" data-dialog-class="inbox-dialog <?=$renderer->is_read ? 'inbox-item-readed' : ''?>" class="open-dialog fa fa-bell" style="padding: 10px 9px; text-decoration: none;" href="<?=Docebo::createLmsUrl('inbox/inbox/renderFullHTMLMessage', array('id' => $renderer->id))?>"></a>
			</div>
<!--			--><?// if($renderer->iconType==BaseNotificationRenderer::ICON_TYPE_URL): ?>
<!--				<img class="icon" src="--><?//=$renderer->getIcon()?><!--" alt=""/>-->
<!--			--><?// elseif($renderer->iconType==BaseNotificationRenderer::ICON_TYPE_HTML): ?>
<!--				--><?//=$renderer->getIcon()?>
<!--			--><?// endif; ?>
		</div>

		<div class="right-side">
			<div>
				<?=$renderer->getMessageText(true)?>
			</div>
			<div class="muted">
				<?=Yii::app()->localtime->toLocalDateTime($renderer->timestamp)?>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<br/>
<? endforeach; ?>

<p class="text-center view-all">
	<a href="<?=Docebo::createLmsUrl('//inbox/inbox/all',array('type' => Yii::app()->request->getParam('type')))?>">
		<?=Yii::t('standard', 'View all')?>
	</a>
</p>
<script>
	$(document).controls();

	$('a.fa.fa-bell').on('click', function(){
		$('.unread-tooltip.text-left').hide();
	});

	$('.dotLibContainer').on('click', function(){

		var parentDiv = $(this).parent();
		var bellIcon = parentDiv.parent().parent().find('.bell-icon');
		var bellAnchor = bellIcon.find('a');

		if(bellAnchor.length > 0){
			bellAnchor.click();
		}
	});

</script>