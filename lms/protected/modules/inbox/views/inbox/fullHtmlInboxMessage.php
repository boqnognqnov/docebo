<style>
	.modal{
		width: 650px;
		margin-left: -325px;
		position: absolute !important;
	}
	.modal-body{
		max-height: none;
	}
	#notification_shortcodes_list{
		max-height: none;
	}
</style>
<?php
/**
 *
 * @var InboxController $this
 */
DoceboUI::printFlashMessages(); ?>
<?php if ($data) { ?>
<h1><?= Yii::t('standard', '_VIEW') . ' ' . Yii::t('notification', 'notification') ?></h1>
<h2 style="text-align: center; word-break: break-word"><?= $data->subject ?></h2>
<div class="row-fluid">
	<div class="span12">
		<?= $data->body ?>
	</div>
	<?php } ?>
	<div class="form-actions">
		<input class="btn-docebo black big close-dialog" type="button" value="<?php echo Yii::t('standard', '_CLOSE'); ?>"/>
	</div>
	<input type="hidden" value="<?php echo $model->id; ?>" id="inbox-id" />
	<input type="hidden" value="<?php echo $delivered['is_read']; ?>" id="inbox-readed" />
</div>