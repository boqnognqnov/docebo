<div class="inbox-wrapper full">
	<div class="filter-wrapper">
		<div class="filters">

			<p class="pull-right">
				<i class="fa fa-check"></i>
				<a href="<?=Docebo::createLmsUrl('//inbox/inbox/axMarkUnmarkAsRead', array('all'=>1))?>" class="mark-all-read">
					<?=Yii::t('inbox', 'Mark all as read')?>
				</a>
				&nbsp;&nbsp;
			</p>
			&nbsp;&nbsp;&nbsp;
			<?=CHtml::checkBox('onlyUnread', false)?>
			&nbsp;
			<?=Yii::t('inbox', 'Show only unread notifications')?>
		</div>
	</div>

	<br/>

	<div class="items-wrapper">
		<!-- filled by ajax -->
	</div>

	<p>
		<a href="" class="load-more"><?= Yii::t('standard', 'Load More'); ?></a>
		<span class="no-more" style="display:none;"><?=Yii::t('inbox', 'No more notifications')?></span>
	</p>
</div>

<script>
	$(function(){
		$('.inbox-wrapper select,.inbox-wrapper input').styler();

		var inboxAppModule = inboxApp.init('<?=Docebo::createLmsUrl('//inbox/inbox/axGetItems', array('type' => Yii::app()->request->getParam('type')))?>');
		inboxAppModule.load();

		$(document).on('click', '.inbox-wrapper.full .circle', function(){

			var defaultInbox = $("#defaultInbox");
			var notification = defaultInbox.find('div[class="count"]');
			var notificationCount = parseInt(notification.html());

			var markAsRead = $(this).data('item-readed');

			$.post('<?=Docebo::createLmsUrl('//inbox/inbox/axMarkUnmarkAsRead')?>', {
				itemId: $(this).data('item-id')
			}, function(){
				if(markAsRead == 1){
					notification.html(notificationCount + 1);
				}else{
					notification.html(notificationCount - 1);
				}
				// Refresh the UI
				inboxAppModule.load();

				/**
				 * Post message to update notifications counter in the Hydra FE
				 */
				if(guard !== 'undefined'){
					guard.updateNotificationsCounter();
				}

			});
		});

		$(document).delegate(".inbox-dialog", "dialog2.ajax-complete", function () {
			var defaultInbox = $("#defaultInbox");
			var notification = defaultInbox.find('div[class="count"]');
			var notificationCount = parseInt(notification.html());

			var self = $(this);
			var isReaded = self.find('#inbox-readed').val();

			if (isReaded <= 0) {
				$.post('/lms/index.php?r=inbox/inbox/axMarkUnmarkAsRead', {
					itemId: $(this).find('#inbox-id').val()
				}, function () {
					notification.html(notificationCount - 1);
					inboxAppModule.load();
				});

			}
		});

		$('#onlyUnread').change(function(){
			$('.load-more').show();
			$('.no-more').hide();
			inboxAppModule.currentPage = 1;
			inboxAppModule.onlyUnread = $(this).is(':checked') ? 1 : 0;
			inboxAppModule.load();
		});

		$('.load-more').click(function(e){
			e.preventDefault();

			inboxAppModule.loadNextPage();
		});

		$('.mark-all-read').click(function(e){
			e.preventDefault();
			$.post($(this).attr('href'), function(){
				// Make all "green" circles grey
				$('.inbox-wrapper.full .circle').addClass('read');
				/**
				 * Post message to update notifications counter in the Hydra FE
				 */
				if(guard !== 'undefined'){
					guard.updateNotificationsCounter();
				}
			});
		});
	})
</script>