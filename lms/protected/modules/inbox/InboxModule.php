<?php
/**
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */

class InboxModule extends CWebModule {

	private $_assetsUrl;

	public function init(){
		// import the module-level models and components
		$this->setImport(array(
				'inbox.models.*',
				'inbox.components.*',
				'inbox.controllers.*'
		));
	}

	/**
	 * Register resources needed only in inbox
	 *
	 * @param CController $controller
	 * @param CAction $action
	 * @return bool
	 */
	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action)) {
			$this->registerResources();
			return true;
		}
		else
			return false;
	}

	public function registerResources($registerFancyTree = false) {

		if (!Yii::app()->request->isAjaxRequest) {
			JsTrans::addCategories('inbox');
			$cs = Yii::app()->getClientScript();
			$cs->registerCoreScript('jquery.ui');
		}
	}

	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('inbox.assets'));
		}
		return $this->_assetsUrl;
	}
}