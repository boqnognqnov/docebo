<?php
/**
 * The controller that will handle all "Inbox" related actions, e.g.:
 * - get unread items with ajax
 * - mark an item as read
 * - show the "My notifications" page
 * - etc
 *
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */

class InboxController extends Controller {

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// Allow access to all actions to logged in users
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}



	public function actionAxGetUnreadInboxTooltip(){

		$command = Yii::app()->getDb()->createCommand()
			->select('*')
			->from('inbox_app_notification i')
			->join('inbox_app_notification_delivered d', 'd.notification_id=i.id')
			->where('d.user_id=:idUser', array(':idUser' => Yii::app()->user->getIdst()))
			->order('i.timestamp DESC')
			->limit(5);

		$event = new DEvent($this, array('command' => $command));
		Yii::app()->event->raise('OnInboxItemsFilter', $event);

		$unread = $command->queryAll();
		$renderers = array();
		foreach($unread as $item){
			$rendererObj = CoreInboxItems::getRenderer($item['type']); /* @var $rendererObj BaseNotificationRenderer */
			if($rendererObj){
				$rendererObj->setDetails($item);
				$renderers[] = $rendererObj;
			}else{
				Yii::log('Invalid renderer class for inbox notification of type: '.$item['type'], CLogger::LEVEL_ERROR);
			}
		}

		$this->renderPartial('inbox_tooltip', array(
			'items'=>$renderers
		));
	}



	public function actionAxGetItems($onlyUnread = false, $currentPage = 1){

		$perPage = Settings::get('elements_per_page', 20);
		$offset = ($currentPage - 1) * $perPage;
		$query = Yii::app()->getDb()->createCommand()
		             ->select('i.id, i.type, i.timestamp, i.json_data, d.is_read')
		             ->from('inbox_app_notification i')
		             ->join('inbox_app_notification_delivered d', 'd.notification_id=i.id')
		             ->where('d.user_id=:idUser', array(':idUser'=>Yii::app()->user->getIdst()))
		             ->order('i.timestamp DESC')
					 ->limit($perPage, $offset);

		if($onlyUnread)
			$query->andWhere('d.is_read=0');


		$event = new DEvent($this, array('command' => $query));
		Yii::app()->event->raise('OnInboxItemsFilter', $event);

		$tmp = $query->queryAll();
		$renderers = array();

		foreach($tmp as $item) {
			$rendererObj = CoreInboxItems::getRenderer($item['type']); /* @var $rendererObj BaseNotificationRenderer */
			if($rendererObj){
				$rendererObj->setDetails($item);
				$renderers[strtotime($rendererObj->timestamp)][] = $rendererObj;
			} else
				Yii::log('Invalid renderer class for inbox notification of type: '.$item['type'], CLogger::LEVEL_ERROR);
		}

		// The incoming arrays' keys are the unix timestamps of the "item"
		$itemsUnorganized = $renderers;

		$localFormat = Yii::app()->localtime->getPHPLocalDateTimeFormat();

		$itemsWithLocalTimestampAsKey = array();

		// Convert the keys of the array (the unix timestamps) to timestamps
		// in the user's local time zone. Then we will organize this array
		// in a more user friendly array in a $arr[year][month][day] format
		foreach($itemsUnorganized as $unixTimestamp => $items) {
			foreach($items as $item) {
				// Convert the item's unix timestamp to a local time zone unix timestamp
				$unixTimestampFormatted = date('Y-m-d H:i:s', $unixTimestamp);
				$localTimestamp = Yii::app()->localtime->toLocalDateTime($unixTimestampFormatted);
				$tmpLocalTimestamp = DateTime::createFromFormat($localFormat, $localTimestamp);
				$itemsWithLocalTimestampAsKey[$tmpLocalTimestamp->getTimestamp()][] = $item;
			}
		}

		// Just to make sure we don't accidentally use this anymore below (helps IDE autocorrection)
		// Use $itemsWithLocalTimestampAsKey from now on
		unset($itemsUnorganized);

		// Organize all items in a new array that is based on the format:
		// $arr[Ymd] = array of items from that date, ordered by timestamp descending
		$makeAssocArrayDateBased = function($items) {
			$res = array();

			foreach($items as $timestamp => $elems) {
				$year = date('Y', $timestamp);
				$month = date('m', $timestamp);
				$day = date('d', $timestamp);
				if (!is_array($res[$year . $month . $day])) {
					if ($year . $month . $day == date('Ymd'))
						$label = Yii::t('calendar', '_TODAY');
					elseif ($year . $month . $day == date('Ymd', strtotime('-1 day')))
						$label = Yii::t('calendar', 'Yesterday');
					else
						$label = date(Yii::app()->localtime->getPHPLocalDateFormat(LocalTime::MEDIUM), $timestamp);

					$res[$year . $month . $day] = array(
						'items' => array(),
						'label' => $label
					);
				}

				foreach($elems as $elem) {
					// Put the item in its correct array key
					$res[$year . $month . $day]['items'][$timestamp][] = $elem;

					// Sort items inside a given date
					krsort($res[$year . $month . $day]['items']);
				}
			}

			// And sort the dates themselves in descending order
			krsort($res);
			return $res;
		};

		$itemsOrganized = $makeAssocArrayDateBased($itemsWithLocalTimestampAsKey, true);

		if(empty($itemsOrganized)){
			if($currentPage==1){
				// First page and no items. That is totally possible
				echo CJSON::encode(array(
					'success'=>true,
					'html'=>'',
				));
			}else{
				// This pagination resulted in no more items
				echo CJSON::encode(array(
					'success'=>false,
				));
			}
		}else{
			$html = $this->renderPartial('inbox_all_items', array(
				'itemsOrganized'=>$itemsOrganized
			), true);
			echo CJSON::encode(array(
				'success'=>true,
				'html'=>$html,
			));
		}
	}

	public function actionAxMarkUnmarkAsRead(){
		if(isset($_REQUEST['all']) && $_REQUEST['all']){
			// mark all user's notifications as read
			InboxAppNotificationDelivered::model()->updateAll(
				array('is_read'=>1), // new attribute values
				'user_id=:idUser', // criteria
				array(':idUser'=>Yii::app()->user->getIdst()) // params
			);
		}else{
			// mark this individual notification as read/unread (reverses its state)
			$itemId = isset($_REQUEST['itemId']) ? $_REQUEST['itemId'] : false;
			$model = InboxAppNotificationDelivered::model()->findByAttributes(array(
				'notification_id'=>$itemId,
				'user_id'=>Yii::app()->user->getIdst(),
			));
			if($model) {
				$model->is_read = $model->is_read ? 0 : 1;
				$model->save();
			}
		}
	}

	public function actionAll(){
		Yii::app()->clientScript->registerScriptFile(Yii::app()->inbox->getAssetUrl().'/inboxapp.js');
		$this->render('all');
	}

	public function actionRenderFullHTMLMessage(){
		$id = intval(Yii::app()->request->getParam('id'));
		$model = InboxAppNotification::model()->findByPk($id);

		$delivered = Yii::app()->getDb()->createCommand()
			->select('is_read')
			->from('inbox_app_notification_delivered')
			->where('user_id = :userid', array(':userid' => Yii::app()->user->getIdst()))
			->andWhere('notification_id = :notificationid', array(':notificationid' => $id))
			->queryRow();

		if (!$model) {
			Yii::app()->user->setFlash('error', Yii::t('notification', 'No such notification'));
			$this->renderPartial('fullHtmlInboxMessage', array('data' => null));
			Yii::app()->end();
		}
		$data = json_decode($model->json_data);
		$data->body = str_replace(array('<p>&nbsp;</p>', '<p> </p>', '<p></p>'), '', $data->body);
		$this->renderPartial('fullHtmlInboxMessage', array('data' => $data, 'model' => $model, 'delivered' => $delivered));
	}
}