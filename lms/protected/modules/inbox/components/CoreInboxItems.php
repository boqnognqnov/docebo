<?php
/**
 * @author Dzhuneyt
 */

class CoreInboxItems {

	public static $TYPE_COMPLETED_COURSE = 'completed_course';
	public static $TYPE_COURSE_RECENTLY_ASSIGNED = 'recently_assigned_course';
	public static $TYPE_COMPLETED_RECENTLY_TEST = 'recently_completed_test_lo';
	public static $TYPE_COURSE_EXPIRING_SOON = 'course_expiring_soon';
	public static $TYPE_NEWSLETTER_MESSAGE = 'newsletter';

	public static function getCoreRenderersMap(){
		return array(
			self::$TYPE_COMPLETED_COURSE=>'inbox.components.renderers.CompletedCourseRenderer',
			self::$TYPE_COURSE_RECENTLY_ASSIGNED=>'inbox.components.renderers.CourseRecentlyAssignedRenderer',
			self::$TYPE_COMPLETED_RECENTLY_TEST=>'inbox.components.renderers.FinalTestTakenRenderer',
			self::$TYPE_COURSE_EXPIRING_SOON=>'inbox.components.renderers.CourseExpiringSoonRenderer',
			self::$TYPE_NEWSLETTER_MESSAGE=>'inbox.components.renderers.NewsletterInboxRenderer',
		);
	}

	/**
	 * @param $itemType
	 *
	 * @return BaseNotificationRenderer|bool
	 * @throws CException
	 */
	public static function getRenderer($itemType){
		Yii::import('inbox.components.renderers.*');

		$coreRenderers = self::getCoreRenderersMap();

		$rendererObj = false;

		if(in_array($itemType, array_keys($coreRenderers))){
			// It's a core renderer

			$baseRendererAlias = $coreRenderers[$itemType];

			Yii::import($baseRendererAlias, true);

			$parts = explode('.', $baseRendererAlias);
			$className = end($parts);

			if(class_exists($className) && method_exists($className, 'newInstance')){
				$rendererObj = call_user_func(array($className, 'newInstance'));
			}else{
				Yii::log('Invalid core renderer for inbox notification of type: '.$itemType, CLogger::LEVEL_ERROR);
			}
		}else{
			$event = new DEvent(self, array(
				'type'=>$itemType,
			));

			// Allow plugins to provide custom renderer class aliases or directly objects
			// implementing IBaseNotificationRenderer
			Yii::app()->event->raise('onInboxNotificationRenderer', $event);

			if($event->return_value){
				// Some plugin has returned a custom renderer

				if(is_string($event->return_value)){

					// Try to create a renderer object/class instance
					// from the class name, returned by the plugin
					Yii::import($event->return_value, true);

					$parts = explode('.', $event->return_value);
					$className = end($parts);

					if(class_exists($className) && method_exists($className, 'newInstance')){
						$rendererObj = call_user_func(array($className, 'newInstance'));
					}else{
						Yii::log('Invalid renderer by plugin for inbox notification: '.$event->return_value, CLogger::LEVEL_ERROR);
					}
				}elseif($event->return_value instanceof BaseNotificationRenderer){
					// The event returned an object/renderer directly

					$rendererObj = $event->return_value;
				}
			}
		}

		return $rendererObj;
	}
}