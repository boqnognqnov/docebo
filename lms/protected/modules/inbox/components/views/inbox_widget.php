<?php /** @var $this InboxIconWidget */ ?>
<span id="<?= $this->inboxId ?>" class="inbox-bell inbox-wrapper text-right">
	<?php if($this->unreadCount > 0): ?><div class="count"><?=$this->unreadCount?></div><?php endif; ?>
	<i class="fa <?= $this->inboxIcon ?> fa-lg"></i>
	<div class="unread-tooltip text-left">
		<p><b><?=$this->defaultHeaderTitle?></b></p>
		<div class="ajax-container"></div>
	</div>
</span>

<? $script = "
	$(function(){

		var inboxViewingStatus = 'hidden';
		var loader = '<div class=\"centered-ajax-loader\"><img src=\"/themes/spt/img/ajax-loader.gif\" /></div>';

		$('#".$this->inboxId.".inbox-bell .count, #".$this->inboxId.".inbox-bell .fa').click(function(){
			if (inboxViewingStatus == 'loading') return; //we are already showing it, no need to messing with another ajax request !!
			inboxViewingStatus = 'loading';
			$('#".$this->inboxId." .unread-tooltip').show().find('.ajax-container').html(loader);
			// Open the 'tooltip' with the truncated inbox items and a 'View all' link
			$.post('".$this->loadTooltipUrl."', {}, function(dataHtml){
				$('#".$this->inboxId." .unread-tooltip')/*.show()*/.find('.ajax-container').html(dataHtml);
			}).always(function() {
				if (inboxViewingStatus == 'loading') { inboxViewingStatus = 'showed'; }
			});
		});

		$(document).click(function(event) {
			if(!$(event.target).closest('#".$this->inboxId.".inbox-bell').length) {
				// Clicked outside the tooltip, hide the tooltip
				$('#".$this->inboxId." .unread-tooltip').hide();
				inboxViewingStatus = 'hidden';
			}
		})
	});";
Yii::app()->clientScript->registerScript('inbox_widget_bell_'.$this->inboxId, $script);