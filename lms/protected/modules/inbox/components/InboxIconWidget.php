<?

/**
 * Class InboxIconWidget
 *
 * Renders the "bell" icon with a counter at the top right with how
 * much unread items the current user has in his "inbox". If the "bell"
 * is clicked, it displays a tooltip with a (truncated) list of unread inbox items.
 */
class InboxIconWidget extends CWidget{

	public $inboxId = 'defaultInbox';
	public $inboxIcon = 'fa-bell';
	public $inboxSize = 0;
	public $loadTooltipUrl = null;
	public $unreadCount = 0;
	public $defaultHeaderTitle = '';

	public function init() {
		Yii::app()->clientScript->registerScriptFile(Yii::app()->inbox->getAssetUrl().'/inboxapp.js');
		$this->render('inbox_widget', array());
	}

}