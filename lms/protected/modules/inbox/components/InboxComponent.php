<?php
/**
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */

class InboxComponent extends CComponent{


	public function init(){
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		Yii::import('lms.protected.modules.inbox.components.*');
		Yii::import('lms.protected.modules.inbox.widgets.*');
		Yii::import('lms.protected.modules.inbox.models.*');

//		Yii::app()->event->on('onBeforeLogoutLink', array($this, 'onBeforeLogoutLink'));
	}

	/**
	 * Show one or more icons near the logout link, if the user has at least one notification (read or unread)
	 * @param DEvent $event
	 * @throws Exception
	 */
	public function onBeforeLogoutLink(DEvent $event) {
//

		// Prepare default descriptor
		$descriptors['defaultInbox'] = array(
			'inboxId' => 'defaultInbox',
			'inboxIcon' => 'fa-bell',
			'inboxSize'	=> InboxAppNotificationDelivered::model()->countByAttributes(array('user_id' => Yii::app()->user->getIdst())),
			'loadTooltipUrl' => Docebo::createLmsUrl('//inbox/inbox/axGetUnreadInboxTooltip'),
			'defaultHeaderTitle' => Yii::t('inbox', 'Your notifications'),
			'unreadCount' => Yii::app()->getDb()->createCommand()
				->select('COUNT(i.id)')
				->from('inbox_app_notification i')
				->join('inbox_app_notification_delivered d', 'd.notification_id=i.id')
				->where('d.user_id=:idUser AND d.is_read=0', array(':idUser'=>Yii::app()->user->getIdst()))
				->queryScalar()
		);


		// Trigger event to let plugins create other descriptors or change default ones
		$descriptorsEvent = new DEvent($this, array('descriptors' => &$descriptors));
		Yii::app()->event->raise('CollectNotificationDescriptors', $descriptorsEvent);
		foreach($descriptors as $descriptor) {
			if($descriptor['inboxSize'] > 0)
				Yii::app()->getController()->widget('InboxIconWidget', $descriptor);
		}
	}

	public function getAssetUrl(){
		return Yii::app()->assetManager->publish(Yii::getPathOfAlias('lms.protected.modules.inbox.assets'));
	}

}