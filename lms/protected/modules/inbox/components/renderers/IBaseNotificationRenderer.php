<?php

interface IBaseNotificationRenderer{
	public static function newInstance();
	public function getIcon();
	public function getMessageText();
}