<?php

/**
 * Class NewsletterInboxRenderer
 *
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */

class NewsletterInboxRenderer extends BaseNotificationRenderer{

	public $iconType = self::ICON_TYPE_HTML;

	public function getIcon() {
		return '';
	}

	public function getMessageText($shortVersion = false) {
		if($this->data){
			$subject = isset($this->data['subject']) ? Yii::app()->htmlpurifier->purify($this->data['subject']) : null;
			$body = isset($this->data['body']) ? Yii::app()->htmlpurifier->purify($this->data['body']) : null;
			$body = strip_tags($body);
			$readMore = '...';
			$limit = 50;
			$body = NotificationInboxRenderer::cleanStringFromEncodingSpacesV2($body);
			if ($shortVersion) {
				if (strlen($body) > $limit)
					$body = substr(trim($body), 0, $limit) . $readMore;
				else $body = trim($body);
				if (strlen($subject) > $limit)
					$subject = substr($subject, 0, $limit).'...';
				return '<div class="dotLibContainer"><a class="open-dialog" href="' . Docebo::createLmsUrl('inbox/inbox/renderFullHTMLMessage', array('id' => $this->id)) . '">' . $subject . '</a><br/>' . $body . '</div>';
			} else {
				$readMore = ' ' . CHtml::link('<i class="fa fa-search-plus"></i> '.Yii::t('standard', '_VIEW') . ' ' . Yii::t('notification', 'notification'), Docebo::createLmsUrl('inbox/inbox/renderFullHTMLMessage', array('id' => $this->id)), array('class' => 'open-dialog readFullHTML'));
				$subject = CHtml::link($subject, Docebo::createLmsUrl('inbox/inbox/renderFullHTMLMessage', array('id' => $this->id)), array('class' => 'open-dialog'));
				return '<div class="dotLibContainer"><div class="bodyContainer"><b>' . $subject . '</b></div><br/>'.$readMore.'<div class="bodyContainer">' . $body . '</div></div>';
			}
		}
	}
}