<?php
/**
 * The base renderer class which all renderers should extend from
 *
 * @author Dzhuneyt
 *
 */

abstract class BaseNotificationRenderer implements IBaseNotificationRenderer{

	const ICON_TYPE_URL = 'url';
	const ICON_TYPE_HTML = 'html';

	public $type;
	public $id; // ID of the notification (PK of inbox_app_notification table)
	public $timestamp; // e.g. 2015-03-02 12:12:03. Always UTC. Comes from inbox_app_notification.timestamp)
	public $data;
	public $is_read = false;

	// Controls what to expect as return value from getIcon() of this renderer
	// If "url", the getIcon() method should return a URL to the image
	// If "html", the getIcon() method should return the full HTML to render the icon (recommended size: 50x34px)
	public $iconType = self::ICON_TYPE_URL;

	public function getIcon() {
		if($this->data){
			if(!empty($this->data)){
				// If inbox item has a specific asset icon set
				if(isset($this->data['icon'])){
					$model = CoreAsset::model()->findByPk($this->data['icon']);
					if($model){
						return $model->getUrl();
					}
				}elseif(isset($this->data['idCourse'])){
					// Course based inbox item; retrieve it from the course

					$courseModelTmp = LearningCourse::model()->findByPk($this->data['idCourse']);

					if($courseModelTmp) return $courseModelTmp->getCourseLogoUrl();
				}
			}
		}

		return Docebo::getRootBaseUrl(true) . '/themes/spt/images/course/course_nologo.png';
	}

	/**
	 * @param bool $shortVersion
	 * @throws CException
	 * @return string
	 */
	public function getMessageText($shortVersion = false) {
		// TODO: Implement getMessageText() method.
		throw new CException(get_called_class().' should implement its own getMessageText() method');
	}

	public function setDetails($detailsArr){
		if(isset($detailsArr['notification_id'])){
			$this->id = $detailsArr['notification_id'];
		}elseif(isset($detailsArr['id'])){
			$this->id = $detailsArr['id'];
		}
		if(isset($detailsArr['timestamp'])){
			$this->timestamp = $detailsArr['timestamp'];
		}
		if(isset($detailsArr['type'])){
			$this->type = $detailsArr['type'];
		}
		if(isset($detailsArr['json_data'])){
			$this->data = json_decode($detailsArr['json_data'], true);
		}
		if(isset($detailsArr['is_read'])){
			$this->is_read = (bool) $detailsArr['is_read'];
		}
	}

	public static function newInstance() {
		$className = get_called_class();
		return new $className;
	}

}