<?php

class QuestionTitle extends CQuestionComponent {
	
	
	public function __construct() {
		parent::__construct();
		$this->setType(LearningTestquest::QUESTION_TYPE_TITLE);
		$this->setInteractive(false);
		$this->setEditProperty('category', false);
		$this->setEditProperty('difficulty', false);
		$this->setEditProperty('shuffle', false);
		$this->setEditProperty('answeringMaxTime', false);
	}



	public function play($idQuestion) {
		$question = LearningTestquest::model()->findByPk($idQuestion);
		$controller = $this->getController();
		$controller->renderPartial($this->getQuestionViewsPath().$this->getType(), array(
			'title' => $question->title_quest
		));
	}


	public function getScoreType() {
		return self::SCORE_TYPE_NONE;
	}

}