<?php

class QuestionBreakPage extends CQuestionComponent {


	public function __construct() {
		parent::__construct();
		$this->setType(LearningTestquest::QUESTION_TYPE_BREAK_PAGE);
		$this->setInteractive(false);
		$this->setEditProperty('category', false);
		$this->setEditProperty('difficulty', false);
		$this->setEditProperty('shuffle', false);
		$this->setEditProperty('maxAnsweringTime', false);
	}



	public function beforeEdit($event) {
		if (!$event->params['isEditing']) {

			try {

				$idTest = (int)$event->params['idTest'];
				$opt = Yii::app()->request->getParam("opt", false);
				$controller = $this->getController();

				// If We are in Central Repo mode then we should not make any Checks
				if($opt === "centralrepo") {
					$backUrl = Yii::app()->request->urlReferrer;
				} else {

					$object = LearningOrganization::model()->findByAttributes(array(
						'idResource' => $idTest,
						'objectType' => LearningOrganization::OBJECT_TYPE_TEST,
						'idCourse'   => $controller->getIdCourse()
					));

					if (!$object ) {
						throw new CException('Invalid test');
					}
					$backUrl = $controller->createUrl('default/edit', array(
						'course_id' => $controller->getIdCourse(),
						'id_object' => $object->getPrimaryKey()
					));
				}

				$br = new LearningTestquest();
				$br->idTest = $idTest;
				$br->type_quest = LearningTestquest::QUESTION_TYPE_BREAK_PAGE;
				$br->title_quest = "--- BREAK PAGE ---";
				$br->idCategory = 0;
				$br->difficult = 0;
				$br->time_assigned = 0;
				$br->sequence = LearningTestquest::getTestNextSequence($idTest);
				$br->page = LearningTestquest::getTestPageNumber($idTest);
				$br->shuffle = 0;

				if (!$br->save()) {
					throw new CException('Error while saving break page');
				}

				Yii::app()->user->setFlash('success', Yii::t('standard', '_OPERATION_SUCCESSFUL'));

			} catch (CException $e) {

				Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
			}

			$controller->redirect($backUrl);
		}
	}


	public function getScoreType() {
		return self::SCORE_TYPE_NONE;
	}

}