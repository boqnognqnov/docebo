<?php

class QuestionUpload extends CQuestionComponent {


	public function __construct() {
		parent::__construct();
		$this->setType(LearningTestquest::QUESTION_TYPE_UPLOAD);
		$this->setInteractive(true);
		$this->setEditProperty('category', true);
		$this->setEditProperty('difficulty', false);
		$this->setEditProperty('shuffle', false);
		$this->setEditProperty('answeringMaxTime', false);
	}



	public function play($idQuestion) {
		$question = LearningTestquest::model()->findByPk($idQuestion);
		$test = $this->_controller->getTest();
		$this->getController()->renderPartial($this->getQuestionViewsPath().$this->getType(), array(
			'question' => $question,
			'questionManager' => $this,
			'idTest' => $test ? $test->idTest : null,
		));
	}

/*
	public function getScoreType() {
		return self::SCORE_TYPE_NONE;
	}
*/
	public function getScoreType() {
		return self::SCORE_TYPE_MANUAL;
	}


	public function edit($event) {
		$controller = $this->getController();
		if ($event->params['isEditing']) {
			//$answer = LearningTestquestanswer::model()->findByAttributes(array('idQuest' => $event->params['idQuestion']));
			$answer = $this->getAnswer($event->params['idQuestion']);
			if (!$answer) { throw new CException('Invalid question answer'); }
			$maxScore = $answer->score_correct;
		}
		if (!isset($maxScore)) { $maxScore = 0.0; }
		$controller->renderPartial('edit/_'.$this->getType(), array(
			'questionManager' => $this,
			'isEditing' => $event->params['isEditing'],
			'idQuestion' => $event->params['idQuestion'],
			'maxScore' => $maxScore
		));
	}



	public function create($event) {
		
		$rq = Yii::app()->request;
		
		$question = $event->params['question'];
		$maxScore = $rq->getParam('max_score', array());

		$ar = new LearningTestquestanswer();
		$ar->idQuest = $question->getPrimaryKey();
		$ar->is_correct = 1;
		$ar->answer = '';
		$ar->comment = '';
		$ar->sequence = 1;
		$ar->score_correct = (double)$maxScore;
		$ar->score_incorrect = 0.0;
		
		if (!$ar->save()) {
			throw new CException('Error while saving answer');
		}
		
	}

	public function update($event) {

		$rq = Yii::app()->request;
		
		$question = $event->params['question'];
		$maxScore = $rq->getParam('max_score', array());

		$ar = $this->getAnswer($question->getPrimaryKey());//LearningTestquestanswer::model()->findByPk($answer['id_answer']);
		if (!$ar || $ar->idQuest != $question->getPrimaryKey()) {
			throw new CException('Invalid answer');
		}
		$ar->is_correct = 1;
		$ar->answer = '';
		$ar->comment = '';
		$ar->sequence = 1;
		$ar->score_correct = (double)$maxScore;
		$ar->score_incorrect = 0.0;

		if (!$ar->save()) {
			throw new CException('Error while saving answer');
		}
		
	}
	
	public function delete($event) {
		$question = $event->params['question'];
		$rs = LearningTestquestanswer::model()->deleteAllByAttributes(array('idQuest' => $question->getPrimaryKey()));
		if ($rs === false) {
			throw new CException('Error while deleting answer');
		}
	}



	public function getAnswer($idQuestion) {
		$answers = LearningTestquestanswer::model()->findAllByAttributes(array('idQuest' => $idQuestion));
		return (!empty($answers) ? $answers[0] : false);
	}


	public function userDoAnswer($idTrack, $idQuestion) {
		$info = LearningTesttrackAnswer::model()->findByAttributes(array('idTrack' => $idTrack, 'idQuest' => $idQuestion));
		if (!empty($info) && empty($info->more_info)) { return false; }
		return (!empty($info));
	}



	public function storeUserAnswer($idTrack, $idQuestion, $answersInput, $canOverwrite) {
		$result = true;

		if (!is_array($answersInput)) {
			$answersInput = $this->readAnswerInput($idQuestion);
		}

		if ($this->userDoAnswer($idTrack, $idQuestion)) {
			if ($canOverwrite) {
				if (!$this->deleteUserAnswer($idTrack, $idQuestion)) return false;
			}
		}

		$saveFile = '';
		if (isset($_FILES['question']['name'][$idQuestion]) && ($_FILES['question']['name'][$idQuestion] != '')) {
			$saveFile = $idTrack.'_'.$idQuestion.'_'.mt_rand(0, 100).time().'_'.$_FILES['question']['name'][$idQuestion];
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_TEST);
			$storageManager->storeAs($_FILES['question']['tmp_name'][$idQuestion], $saveFile);
		}

		//handle empty answer case:
		//if empty path answer exists in DB, then consider the question as unanswered anyway and allow to update existing value
		$existing = LearningTesttrackAnswer::model()->findByAttributes(array('idTrack' => $idTrack, 'idQuest' => $idQuestion));

		if (!empty($existing)) {
			if (!$canOverwrite && !empty($existing->more_info)) { return $result; } //cannot overwrite a real existing answer
			$ar = $existing;
		} else {
			$ar = new LearningTesttrackAnswer();
			$ar->idTrack = $idTrack;
			$ar->idQuest = $idQuestion;
		}
		$ar->idAnswer = 0;
		$ar->score_assigned = 0;
		$ar->more_info = $saveFile;
		$ar->user_answer = (!empty($saveFile) ? 1 : 0);

		if (!$ar->save()) {
			throw new CException('Error while storing user answer');
		}

	}

}