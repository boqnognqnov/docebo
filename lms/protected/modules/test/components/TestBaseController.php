<?php
/**
 *
 * Module specific base controller
 *
 */
class TestBaseController extends LoBaseController
{

	public $userCanAdminCourse = false;
	public $userIsSubscribed = false;
	public $courseModel = null;


  /**
   * (non-PHPdoc)
   * @see CController::init()
   */
	public function init()
	{
		parent::init();

		JsTrans::addCategories(array('player', 'test'));
		
		$this->registerResources();

		$pUserCourseAssigned = CoreUserPuCourse::model()->findByAttributes(array(
			'puser_id' => Yii::app()->user->id,
			'course_id' => $this->getIdCourse()
		));

		$this->userCanAdminCourse = (LearningCourseuser::userLevel(Yii::app()->user->id, $this->getIdCourse()) > LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT) || Yii::app()->user->isGodAdmin || ($pUserCourseAssigned && Yii::app()->user->checkAccess('/lms/admin/course/mod'));
		$this->userIsSubscribed = LearningCourseuser::isSubscribed(Yii::app()->user->id, $this->getIdCourse());

		// Player App. component is preloaded. Here we just set its attributes
		Yii::app()->player->setCourse($this->courseModel);
	}

	/**
	 * (non-PHPdoc)
	 * Method invoked before any action, used for register some scripts or other prepares
	 * @param CAction $action
	 * @return bool
	 */
	public function beforeAction($action)
	{
		$this->courseModel = LearningCourse::model()->findByPk($this->getIdCourse());
		$opt = Yii::app()->request->getParam('opt', false);
		if (!$this->courseModel && (!$opt && $opt != 'centralrepo')) {
			//redirect only if it's not update/create question actions, as it can be used without course ID (for bank questions)
			if (!($this->id == 'question' && ($action->id == 'update' || $action->id == 'create')))
				$this->redirect(array('/site'),true);
		}

		$this->buildBreadCrumbs($action);
		return parent::beforeAction($action);
	}

	/**
	 * Register module specific assets
	 *
	 * (non-PHPdoc)
	 * @see LoBaseController::registerResources()
	 */
	public function registerResources() {

		parent::registerResources();

		if (!Yii::app()->request->isAjaxRequest) {

			// Use
			// $this->module->assetsUrl
			// $this->getPlayerAssetsUrl() <-- shared between all player related modules

			$cs = Yii::app()->getClientScript();
			//$assetsUrl = $this->module->assetsUrl;
			$cs->registerCssFile($this->getPlayerAssetsUrl().'/css/testpoll.css');

			// Bootcamp menu
			$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/docebo_bootcamp.js');
			$cs->registerCssFile (Yii::app()->theme->baseUrl . '/css/docebo_bootcamp.css');

		}
	}

	protected function notEnrolledCourseAdmin()
	{
		return $this->userCanAdminCourse && !$this->userIsSubscribed;
	}

	/**
	 * @param $action
	 */
	protected function buildBreadCrumbs($action)
	{
		if (!(strpos($action->id, "ax") === 0)) // non ajax actions
		{
			if ($this->notEnrolledCourseAdmin())
			{
				$this->breadcrumbs[Yii::t('menu', 'Admin')] = Docebo::createAppUrl('admin:dashboard/index');
				$this->breadcrumbs[Yii::t('standard', '_COURSES')] = Docebo::createAppUrl('admin:courseManagement/index');
			} else
			{
				$this->breadcrumbs[] = Yii::t('menu_over', '_MYCOURSES');
			}

			$this->breadcrumbs[$this->courseModel->name] = $this->createUrl('/player/training/index');
			$this->breadcrumbs[] = Yii::t('standard', 'Training materials');
		}
	}
}