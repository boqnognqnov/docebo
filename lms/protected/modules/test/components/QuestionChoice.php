<?php

class QuestionChoice extends CQuestionWithAnswers {


	public function __construct() {
		parent::__construct();
		$this->setType(LearningTestquest::QUESTION_TYPE_CHOICE);
		$this->setEditProperty('category', true);
		$this->setEditProperty('difficulty', true);
		$this->setEditProperty('shuffle', true);
		$this->setEditProperty('answeringMaxTime', true);
	}



	public function play($idQuestion) {
		$answers = $this->_getAnswersList($idQuestion);

		$question = LearningTestquest::model()->findByPk($idQuestion);

		$test_shuffle = false;
		$test = $this->_controller->getTest();

		if(is_object($test))
			$test_shuffle = $test->getAttribute('shuffle_answer') == 1;

		if ($question->shuffle || $test_shuffle) {
			$answers = $this->shuffleAnswers($idQuestion, $answers);
		}

		$controller = $this->getController();
		$controller->renderPartial($this->getQuestionViewsPath().$this->getType(), array(
			'questionManager' => $this,
			'idQuestion' => $idQuestion,
			'answers' => $answers,
			'idTest' => $test->idTest,
		));
	}


	public function edit($event) {
		$controller = $this->getController();
		$cs = Yii::app()->getClientScript();
		$cs->registerCoreScript('jquery.ui');
		$assetsUrl = $controller->getPlayerAssetsUrl();
		// $cs->registerCssFile($assetsUrl.'/css/player-sprite.css');  // moved to themes
		$cs->registerScriptFile($assetsUrl.'/js/testpoll.edit_question_choice.js');
		$controller->renderPartial('edit/_'.$this->getType(), array(
			'questionManager' => $this,
			'isEditing' => $event->params['isEditing'],
			'idQuestion' => $event->params['idQuestion']
		));
	}



	public function _getAnswersList($idQuestion) {
		$criteria = new CDbCriteria();
		$criteria->order = 'sequence ASC';

		$answers = LearningTestquestanswer::model()->findAllByAttributes(array('idQuest' => $idQuestion), $criteria);
		if (!is_array($answers)) { return false; }
		$index = 0;
		$output = array();
		foreach ($answers as $answer) {
			//$trimmedAnswer = trim(preg_replace('/(^([\xA0\xC2]|&nbsp;|\s){1,})|(([\xA0\xC2]|&nbsp;|\s){1,}$)/','',$answer->answer));
			//NOTE: for some reasons above regex breaks UTF-8 encoding for some characters like 'à', 'è' etc.
			$trimmedAnswer = trim($answer->answer);
			$output[] = array(
				'index' => $index,
				'sequence' => (int)$answer->sequence,
				'id_answer' => (int)$answer->idAnswer,
				'is_correct' => ($answer->is_correct > 0 ? true : false),
				'answer' => (($trimmedAnswer != '')? $trimmedAnswer : 'error'),
				'comment' => $answer->comment,
				'answer_stripped' => strip_tags($answer->answer),
				'comment_stripped' => strip_tags($answer->comment),
				'score_correct' => (double)$answer->score_correct,
				'score_incorrect' => (double)$answer->score_incorrect
			);
			$index++;
		}
		return $output;
	}

}