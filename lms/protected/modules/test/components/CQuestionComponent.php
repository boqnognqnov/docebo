<?php

class CQuestionComponent extends CComponent {


	const INPUT_PREFIX = 'question';

	const SCORE_TYPE_NONE = 'none';
	const SCORE_TYPE_MANUAL = 'manual';
	const SCORE_TYPE_AUTO = 'auto';


	protected $_type = false;

	protected $_controller = NULL;

	protected $_interactive = true;

	protected $_editProperties = array(
		'category' => true,
		'difficulty' => true,
		'shuffle' => false,
		'answeringMaxTime' => true,
		'extraInfo' => false,
	);

	protected $_transformations = array();


	protected $_test = NULL;
	protected $_question = NULL;

	//get / set methods

	public function getType() {
		return $this->_type;
	}

	protected function setType($type) {
		if (LearningTestquest::isValidType($type)) {
			$this->_type = $type;
		} else {
			throw new CException('Invalid specified question type: '.$type);
		}
	}


	public function getController() {
		return $this->_controller;
	}

	public function setController($controller) {
		if (is_subclass_of($controller, 'CController')) {
			$this->_controller = $controller;
			if (get_class($this->_controller) == 'QuestionController') {
				foreach ($this->getEditProperties() as $editProperty => $value) {
					switch ($editProperty) {
						case 'category': { $this->_controller->setUseCategory($value); } break;
						case 'difficulty': { $this->_controller->setUseDifficulty($value); } break;
						case 'shuffle': { $this->_controller->setUseShuffle($value); } break;
						case 'answeringMaxTime': { $this->_controller->setUseAnsweringMaxTime($value); } break;
						case 'extraInfo': { $this->_controller->setUseExtraInfo($value); } break;
					}
				}
			}
			return true;
		}
		return false;
	}

	public function setQuestion($question) {
		$this->_question = $this->_validateQuestionInput($question);
	}

	public function getQuestion() {
		return $this->_question;
	}

	public function setInteractive($value) {
		$this->_interactive = (bool)$value;
	}

	public function getInteractive() {
		return (bool)$this->_interactive;
	}

	public function getEditProperties() {
		return $this->_editProperties;
	}

	public function setEditProperty($property, $value) {
		if (array_key_exists($property, $this->_editProperties)) {
			$this->_editProperties[$property] = (bool)$value;
			return true;
		}
		return false;
	}

	public function getEditProperty($property) {
		return (isset($this->_editProperties[$property])
			? (bool)$this->_editProperties[$property]
			: NULL);
	}

	public function getHasFullSupport() {
		// TODO: change this for import answer score behavior changes in order to obtain full support (this property has been inherited from previous versions, but it is of unclear purpose)
		return false;
	}

	/**
	 * retrieve test AR by question
	 * @return null|LearningTest
	 */
	public function getTest() {
		if ($this->_test === NULL) {
			$question = $this->getQuestion();
			if (!empty($question)) {
				/* @var $question LearningTestquest */
				$test = LearningTest::model()->findByPk($question->idTest);
				if (!empty($test)) {
					$this->_test = $test;
				}
			}
		}
		return $this->_test;
	}

	//methods


	public function __construct() {
		//...
	}



	protected function _validateQuestionInput($input) {
		if (is_numeric($input) && (int)$input > 0) {
			$output = LearningTestquest::model()->findByPk((int)$input);
			if (!$output || $output->type_quest != $this->getType()) {
				throw new CException('Invalid question');
			}
		} elseif (get_class($input) == 'LearningTestquest') {
			if ($input->type_quest != $this->getType()) {
				throw new CException('Invalid question');
			}
			$output = $input;
		} else {
			throw new CException('Invalid question');
		}
		return $output;
	}



	public function getQuestionViewsPath() {
		if (method_exists($this->controller, 'getQuestionViewsPath')) {
			return $this->controller->getQuestionViewsPath();
		}
		return "";
	}


	/**
	 * @param $questionType
	 * @return CQuestionWithAnswers
	 */
	public static final function getQuestionManager($questionType) {
		if (LearningTestquest::isValidType($questionType)) {
			//$component = 'Question'.ucfirst(preg_replace("/\_(.)/e", "strtoupper('\\1')", $questionType));
			$component = 'Question'.ucfirst(preg_replace_callback("/\_(.)/", function($matches){
				return strtoupper($matches[1]);
			}, $questionType));
			return new $component();
		}
	}


	public function isInteractive() {
		return $this->getInteractive();
	}



	public function getInputId($idQuestion) {
		return self::INPUT_PREFIX.'-'.(int)$idQuestion;
	}

	public function getInputName($idQuestion) {
		return self::INPUT_PREFIX.'['.(int)$idQuestion.']';
	}

	public function readAnswerInput($idQuestion) {
		$input = Yii::app()->request->getParam(self::INPUT_PREFIX, false);
		return (is_array($input) && isset($input[$idQuestion])
			? $input[$idQuestion]
			: NULL);
	}


	public function setTransformation($key, $transformFunction) {
		if (empty($key) || !is_string($key) || is_numeric($key)) { return false; }
		$this->_transformations[$key] = $transformFunction;
		return true;
	}


	/**
	 * @param $key
	 * @param $input
	 * @param bool|array $arguments
	 * @return mixed
	 */
	public function applyTransformation($key, $input, $arguments = false) {
		if (!isset($this->_transformations[$key])) {
			//no transformation has been specified for this key, return input data as it is
			return $input;
		}
		//process and validate input arguments
		$other = new stdClass();
		if (is_array($arguments) && !empty($arguments)) {
			foreach ($arguments as $reference => $value) {
				if (is_string($reference) && !empty($reference) && !is_numeric($reference)) {
					$other->$reference = $value;
				}
			}
		}
		//call transformation function
		return call_user_func($this->_transformations[$key], $input, $other);
	}


	/**
	 * Plays the question
	 */
	public function play($idQuestion) { }


	public function beforeEdit($event) { }

	public function edit($event) { }

	public function create($event) { }

	public function update($event) { }

	public function delete($event) { }



	public function userDoAnswer($idTrack, $idQuestion) {
		$info = LearningTesttrackAnswer::model()->findByAttributes(array('idTrack' => $idTrack, 'idQuest' => $idQuestion));
		return (!empty($info));
	}

	public function storeUserAnswer($idTrack, $idQuestion, $answersInput, $canOverwrite) {
		//...
	}

	public function deleteUserAnswer($idTrack, $idQuestion) {
		$userAnswers = LearningTesttrackAnswer::model()->findAllByAttributes(array('idTrack' => $idTrack, 'idQuest' => $idQuestion));
		if (!empty($userAnswers)) {
			foreach ($userAnswers as $userAnswer) {
				if (!$userAnswer->delete()) {
					throw new CException('Error while deleting user answers');
				}
			}
		}
		return true;
	}



	public function recoverUserAnswer($idQuestion, $idUser = false, $idTest = null) {
		if (!$idUser) { $idUser = Yii::app()->user->id; }

		if(!$idTest){
			$idTest = LearningTestquest::model()->findByPk($idQuestion)->idTest;
		}

		$track = LearningTesttrack::model()->findByAttributes(array('idUser' => $idUser, 'idTest' => $idTest));
		$idTrack = ($track ? $track->getPrimaryKey() : FALSE);

		$output = array();
		if ($idTrack) {
			$answers = LearningTesttrackAnswer::model()->findAllByAttributes(array('idTrack' => $idTrack, 'idQuest' => $idQuestion));
			if (!empty($answers)) {
				foreach ($answers as $answer) {
					$output[$answer->idAnswer] = $answer;
				}
			}
		}

		return $output;
	}


	public function setMaxScore($idQuestion, $score) {

		$answers = LearningTestquestanswer::model()->findAllByAttributes(array('idQuest' => $idQuestion));
		if (!empty($answers)) {

			$countCorrect = 0;
			foreach ($answers as $answer) {
				if ($answer->is_correct) {
					$countCorrect++;
				}
			}

			$scoreAssigned = ($countCorrect <= 0 ? 0 : round($score / $countCorrect, 2));
			$diff = 0;

			if($countCorrect > 0)
				if(($scoreAssigned * $countCorrect) != $score)
					$diff = $score - ($scoreAssigned * $countCorrect);

			$db = Yii::app()->db;
			if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

			try {
				foreach ($answers as $answer) {
					if ($answer->is_correct) {
						$answer->score_correct = $scoreAssigned + $diff;
						$diff = 0;
					} else {
						$answer->score_correct = 0;
					}
					if (!$answer->save()) {
						throw new CException('Error while updating question score');
					}
				}

				if (isset($transaction)) { $transaction->commit(); }

			} catch(CException $e) {

				if (isset($transaction)) { $transaction->rollback(); }
				return false;
			}
		}

		return true;
	}


	public function getMaxScore($idQuestion) {

		$answers = LearningTestquestanswer::model()->findAllByAttributes(array('idQuest' => $idQuestion));
		$maxScore = 0;
		foreach ($answers as $answer) {
			$maxScore = round($maxScore + $answer->score_correct, 2);
		}

		return $maxScore;
	}


	public function getRealMaxScore($idQuestion, $score) {

		$rs = Yii::app()->db->createCommand()
			->select("COUNT(*) AS num_correct")
			->from(LearningTestquestanswer::model()->tableName())
			->where("idQuest = :id_question", array(':id_question' => $idQuestion))
			->andWhere("is_correct = '1'")
			->queryRow();
		$countCorrect = (int)$rs['num_correct'];

		if ($countCorrect <= 0) {
			$scoreAssigned = 0;
		} else {
			$scoreAssigned = round($score / $countCorrect, 2);
		}

		return round($scoreAssigned * $countCorrect, 2);
	}


	public function getUserScore($idQuestion, $idTrack) {
		$score = 0;
		$list = LearningTesttrackAnswer::model()->findAllByAttributes(array('idQuest' => $idQuestion, 'idTrack' => $idTrack));
		if (!empty($list)) {
			foreach ($list as $item) {
				$score = round($score + $item->score_assigned, 2);
			}
		}
		return $score;
	}


	public function getScoreType() {
		return self::SCORE_TYPE_AUTO;
	}



	protected static $_categories = NULL;
	protected static function getCategoryName($idCategory) {
		if (self::$_categories === NULL) {
			self::$_categories = array();
			$list = LearningQuestCategory::model()->findAll();
			if (!empty($list)) {
				foreach ($list as $category) {
					self::$_categories[$category->getPrimaryKey()] = $category->name;
				}
			}
		}
		return isset(self::$_categories[$idCategory]) ? self::$_categories[$idCategory] : '';
	}

	public static function exportToRaw($input) {

		//retriving question information
		if (is_numeric($input)) {
			$question = LearningTestquest::model()->findByPk($input);
			$idQuestion = $question->getPrimaryKey();
		} elseif (get_class($input) == 'LearningTestquest') {
			$question = $input;
			$idQuestion = $input->getPrimaryKey();
		} else {
			throw new CException('Invalid input');
		}

		if (!$question) {
			throw new CException('Invalid question');
		}

		//insert the question copy
		$oQuest = new QuestionRaw();
		$oQuest->id = $idQuestion;
		$oQuest->qtype = $question->type_quest;

		$oQuest->id_category = self::getCategoryName($question->idCategory);
		$oQuest->quest_text = $question->title_quest;
		$oQuest->difficult = $question->difficult;
		$oQuest->time_assigned = $question->time_assigned;

		$oQuest->answers = array();
		$oQuest->extra_info = array();

		//retriving new answer
		$criteria = new CDbCriteria();
		$criteria->order = "sequence ASC";
		$answers = LearningTestquestanswer::model()->findAllByAttributes(array('idQuest' => $idQuestion), $criteria);
		if (!empty($answers)) {
			foreach ($answers as $answer) {
				$oAnswer = new AnswerRaw();
				$oAnswer->is_correct = $answer->is_correct;
				$oAnswer->text = $answer->answer;
				$oAnswer->comment = $answer->comment;
				$oAnswer->score_correct = $answer->score_correct;
				$oAnswer->score_penalty = $answer->score_incorrect;
				$oQuest->answers[] = $oAnswer;
			}
		}

		return $oQuest;
	}



	public function importFromRaw($rawQuestion, $idTest, $sequence = false, $page = false) {

		if (get_class($rawQuestion) == 'QuestionRaw' && $rawQuestion->qtype == $this->getType()) {

			if (Yii::app()->db->getCurrentTransaction() === NULL) { $transaction = Yii::app()->db->beginTransaction(); }

			try {

				if (!$idTest || $idTest <= 0) { $idTest = 0; }
				if (!$sequence) { $sequence = 999999; }
				if (!$page) { $page = 1; }

				//insert question in DB
				$question = new LearningTestquest();
				$question->idTest = (int)$idTest;
				$question->idCategory = $rawQuestion->id_category;
				$question->type_quest = $rawQuestion->qtype;
				$question->title_quest = $rawQuestion->quest_text;
				$question->difficult = $rawQuestion->difficult;
				$question->time_assigned = $rawQuestion->time_assigned;
				$question->sequence = $sequence;
				$question->page = $page;

				if (!$question->save()) { throw new CException('Error while saving question'); }

				$checkScore = function($score) {
					$score = preg_replace('/,/', '.', $score);
					if ($score{0} == '.') { $score = '0'.$score; }
					return $score;
				};

				if (is_array($rawQuestion->answers)) {
					foreach ($rawQuestion->answers as $rawAnswer) {

						if ($this->hasFullSupport) {
							if($rawAnswer->score_correct > 0 && $rawAnswer->score_correct < 1  && $rawAnswer->is_correct) {
								// a little bit tricky but needed in order to obtain full support
								$rawAnswer->score_penalty = (-1) * $rawAnswer->score_correct;
								$rawAnswer->score_correct = 0;
							}
						}

						//insert answer in DB
						$answer = new LearningTestquestanswer();
						$answer->idQuest = $question->getPrimaryKey();
						$answer->is_correct = ($rawAnswer->is_correct ? 1 : 0);
						$answer->answer = Docebo::nbspToSpace($rawAnswer->text);//preg_replace('/[^(\x20-\x7F)]*/','', Docebo::nbspToSpace($rawAnswer->text));
						// NOTE: the above regex was breaking the inserted text, removing characters such as "à", "è" etc.
						$answer->comment = $rawAnswer->comment;
						$answer->score_correct = $checkScore($rawAnswer->score_correct);
						$answer->score_incorrect = $checkScore($rawAnswer->score_penalty);

						if (!$answer->save()) { throw new CException('Error while saving question answers'); }
					}
				}

				if (isset($transaction)) { $transaction->commit(); }

				Yii::app()->event->raise('QuestionRawImportAfterSave', new DEvent($this, array(
					'question' => $question
				)));

				return true;

			} catch (CException $e) {

				if (isset($transaction)) { $transaction->rollback(); }
				return false;
			}

		} else {
			return false;
		}

	}


	/**
	 * While playing the test, detect if the question can be answered or less due to test settings
	 * @param $idQuestion the numerical ID of the question to analyze
	 * @param $idUser the numerical ID of the user to be checked
	 * @param $idTest the ID of the test where the question belongs
	 */
	public function canBeAnswered($idQuestion, $idUser, $idTest = null) {

		//validate input and get AR model object of the question
		$this->_validateQuestionInput($idQuestion);

		if (!LearningTestQuestRel::isAssignedToTest($idTest, $idQuestion))
		{
			throw new CException('Question not assigned to the test');
		}
		//retrieve test AR (throw exception if for some reasons the test is invalid
		$test = LearningTest::model()->findByPk($idTest);
		if (!$test) {
			throw new CException('Invalid test');
		}

		//
		/* @var $test LearningTest */
		if ($test->mod_doanswer) {

			//answers can be edited in this test, so the user must be allowed to do it: do not disable answers inputs
			return true;

		} else {

			//retrieve track info
			$track = LearningTesttrack::model()->findByAttributes(array('idTest' => $test->getPrimaryKey(), 'idUser' => $idUser));
			if (!$track) {
				//no track record exists for this test and user, so no answers have been given yet.
				return true;
			}

			/* @var $track LearningTesttrack */
			//check if some answers already exists for this question and user
			$query = "SELECT COUNT(*) FROM ".LearningTesttrackAnswer::model()->tableName()." WHERE idTrack = :id_track AND idQuest = :id_question";
			$params = array(':id_track' => $track->getPrimaryKey(), ':id_question' => $idQuestion);
			$count = Yii::app()->db->createCommand($query)->queryScalar($params);
			//if $count > 0 then some answers had already been given by this user and cannot change it
			return ($count <= 0);

		}
	}

}