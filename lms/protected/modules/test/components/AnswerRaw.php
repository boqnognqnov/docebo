<?php

class AnswerRaw {

	public $is_correct = 0;
	public $text = false;
	public $comment = false;
	public $score_correct = 0;
	public $score_penalty = 0;

	public function __construct() { }

}