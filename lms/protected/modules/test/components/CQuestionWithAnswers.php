<?php

class CQuestionWithAnswers extends CQuestionComponent {


	private $multipleChoice = false;


	//get / set methods

	protected function getMultipleChoice() {
		return (bool)$this->multipleChoice;
	}

	protected function setMultipleChoice($value) {
		$this->multipleChoice = (bool)$value;
	}

	public function useMultipleChoice() {
		return $this->getMultipleChoice();
	}



	// events

	public function onBeforeAnswerCreate($event) {
		$this->raiseEvent('onBeforeAnswerCreate', $event);
	}

	public function onAfterAnswerCreate($event) {
		$this->raiseEvent('onAfterAnswerCreate', $event);
	}

	public function onBeforeAnswerUpdate($event) {
		$this->raiseEvent('onBeforeAnswerUpdate', $event);
	}

	public function onAfterAnswerUpdate($event) {
		$this->raiseEvent('onAfterAnswerUpdate', $event);
	}

	public function onBeforeAnswerDelete($event) {
		$this->raiseEvent('onBeforeAnswerDelete', $event);
	}

	public function onAfterAnswerDelete($event) {
		$this->raiseEvent('onAfterAnswerDelete', $event);
	}



	//methods

	public function __construct() {
		parent::__construct();
	}



	protected function getAnswersInput() {
		return Yii::app()->request->getParam('answers', array());
	}



	public function create($event) {

		$db = Yii::app()->db;
		$answers = $this->getAnswersInput();

		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {

			$question = $event->params['question'];

			foreach ($answers as $sequence => $answer) {

				$ar = new LearningTestquestanswer();
				$ar->idQuest = $question->idQuest;
				$ar->is_correct = ((isset($answer['correct']) && $answer['correct'] > 0) ? 1 : 0);
				$ar->answer = Yii::app()->htmlpurifier->purify($answer['answer']);
				$ar->comment = Yii::app()->htmlpurifier->purify($answer['comment']);
				$ar->sequence = $sequence+1;
				$ar->score_correct = (double)$answer['score_correct'];
				$ar->score_incorrect = (double)$answer['score_incorrect'];

				$this->onBeforeAnswerCreate(new CEvent($this, array('answer' => $ar, 'action' => 'create')));
				if (!$ar->save()) {
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				}
				$this->onAfterAnswerCreate(new CEvent($this, array('answer' => $ar, 'action' => 'create')));
			}

			if (isset($transaction)) { $transaction->commit(); }

		} catch (CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}

	}

	public function update($event) {

		$db = Yii::app()->db;
		$answers = $this->getAnswersInput();

		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {

			$question = $event->params['question'];

			$checkList = array();
			$keepList = array();

			foreach ($answers as $answer) {
				if (isset($answer['id_answer']) && (int)$answer['id_answer'] > 0) {
					$checkList[] = (int)$answer['id_answer'];
				}
			}

			$oldAnswers = LearningTestquestanswer::model()->findAllByAttributes(array('idQuest' => $question->idQuest));
			foreach ($oldAnswers as $oldAnswer) {
				$toCheck = (int)$oldAnswer->getPrimaryKey();
				if (!in_array($toCheck, $checkList)) {
					if (!$oldAnswer->delete()) {
						throw new CException('Error while updating answers');
					}
				} else {
					$keepList[$toCheck] = $oldAnswer;
				}
			}

			foreach ($answers as $sequence => $answer) {

				$action = false;
				if (isset($answer['id_answer']) && $answer['id_answer'] > 0) {
					$index = (int)$answer['id_answer'];
					if (!isset($keepList[$index])) { throw new CException('Error while updating answers'); }
					$ar = $keepList[$index];
					$action = 'update';
				} else {
					$ar = new LearningTestquestanswer();
					$ar->idQuest = $question->idQuest;
					$action = 'create';
				}

				$ar->is_correct = ((isset($answer['correct']) && $answer['correct'] > 0) ? 1 : 0);
				$ar->answer = Yii::app()->htmlpurifier->purify($answer['answer']);
				$ar->comment = Yii::app()->htmlpurifier->purify($answer['comment']);
				$ar->sequence = $sequence+1;
				$ar->score_correct = (double)$answer['score_correct'];
				$ar->score_incorrect = (double)$answer['score_incorrect'];

				switch ($action) {
					case 'create': { $this->onBeforeAnswerCreate(new CEvent($this, array('answer' => $ar, 'action' => 'update'))); } break;
					case 'update': { $this->onBeforeAnswerUpdate(new CEvent($this, array('answer' => $ar, 'action' => 'update'))); } break;
				}
				if (!$ar->save()) {
					throw new CException('Error while updating answers');
				}
				switch ($action) {
					case 'create': { $this->onAfterAnswerCreate(new CEvent($this, array('answer' => $ar, 'action' => 'update'))); } break;
					case 'update': { $this->onAfterAnswerUpdate(new CEvent($this, array('answer' => $ar, 'action' => 'update'))); } break;
				}
			}

			if (isset($transaction)) { $transaction->commit(); }

		} catch (CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}

	}



	public function delete($event) {
		//...
	}



	public function storeUserAnswer($idTrack, $idQuestion, $answersInput, $canOverwrite) {

		if (!is_array($answersInput)) {
			$answersInput = $this->readAnswerInput($idQuestion);
			if (is_array($answersInput) && !$this->useMultipleChoice() && count($answersInput) > 1) {
				//error ...
			}
		}

		$forceMultipleChoice = ($this->useMultipleChoice() && Settings::get('test_force_multichoice', 'off') == 'on');

		if ($this->userDoAnswer($idTrack, $idQuestion)) {
			if (!$canOverwrite) { return true; }
			$this->deleteUserAnswer($idTrack, $idQuestion);
		}

		if ($forceMultipleChoice) {
			$numberCorrect = 0;
			$numberCorrectAnswered = 0;
			$numberAnswered = 0;
			$incorrectScoreList = array();
		}

		$storedAnswers = $this->getAnswers($idQuestion);
		if (empty($storedAnswers)) { return; } //TO DO: what to do now?
		foreach ($storedAnswers as $storedAnswer) {

			if ($forceMultipleChoice) {
				if ($storedAnswer->is_correct) {
					$numberCorrect++;
				}
				if ($storedAnswer->score_incorrect != 0) {
					$incorrectScoreList[$storedAnswer->idAnswer] = $storedAnswer->score_incorrect;
				}
			}

			$insert = false;
			if (is_array($answersInput) && in_array($storedAnswer->idAnswer, $answersInput)) {
				//answer checked by the user
				if ($forceMultipleChoice) {
					if ($storedAnswer->is_correct) { $numberCorrectAnswered++; }
					$numberAnswered++;
				}
				$insert = true;
				$scoreAssigned = ($storedAnswer->is_correct ? $storedAnswer->score_correct : -$storedAnswer->score_incorrect);
				$userAnswer = 1;
			} elseif ($storedAnswer->is_correct && ($storedAnswer->score_incorrect != 0)) {
				//answer correct with penalty but not checked by the user
				$insert = true;
				$scoreAssigned = -$storedAnswer->score_incorrect;
				$userAnswer = 0;
			} elseif (!$storedAnswer->is_correct && ($storedAnswer->score_correct != 0)) {
				//answer incorrect with penalty but not checked by the user
				$insert = true;
				$scoreAssigned = $storedAnswer->score_correct;
				$userAnswer = 0;
			}

			if ($insert) {
				$newTrack = new LearningTesttrackAnswer();
				$newTrack->idTrack = $idTrack;
				$newTrack->idQuest = $idQuestion;
				$newTrack->idAnswer = $storedAnswer->idAnswer;
				$newTrack->score_assigned = $scoreAssigned;
				$newTrack->more_info = "";
				$newTrack->user_answer = $userAnswer;
				if (!$newTrack->save()) {
					throw new CException('Error while storing user answer');
				}
			}
		}

		if ($forceMultipleChoice) {
			if($numberCorrect != $numberCorrectAnswered || $numberCorrect != $numberAnswered) {
				$rs = LearningTesttrackAnswer::model()->updateAll(
					array('score_assigned' => 0),
					"idTrack = :id_track AND idQuest = :id_question",
					array(':id_track' => $idTrack, ':id_question' => $idQuestion)
				);
				if ($rs === false) { throw new CException('Error while storing user answer'); }

				foreach ($incorrectScoreList as $idAnswer => $scoreIncorrect) {
					$ar = LearningTesttrackAnswer::model()->findByPk(array('idTrack' => $idTrack, 'idQuest' => $idQuestion, 'idAnswer' => $idAnswer));
					if ($ar) {
						$ar->score_assigned = -$scoreIncorrect;
					} else {
						$ar = new LearningTesttrackAnswer();
						$ar->idTrack = $idTrack;
						$ar->idQuest = $idQuestion;
						$ar->idAnswer = $idAnswer;
						$ar->score_assigned = -$scoreIncorrect;
						$ar->more_info = "";
						$ar->user_answer = 0;
					}
					if (!$ar->save()) {
						throw new CException('Error while storing user answer');
					}
				}
			}
		}

		return true;
	}



	public function deleteUserAnswer($idTrack, $idQuestion) {
		$rs = LearningTesttrackAnswer::model()->deleteAllByAttributes(array('idTrack' => $idTrack, 'idQuest' => $idQuestion));
		if ($rs === false) {
			throw new CException('Error while deleting stored answer');
		}
		return true;
	}



	public function getAnswers($idQuestion) {
		$criteria = new CDbCriteria();
		$criteria->order = "sequence ASC";
		return LearningTestquestanswer::model()->findAllByAttributes(array('idQuest' => $idQuestion), $criteria);
	}


	/**
	 * Shuffle question answers. Shuffling order will be stored in session, to avoid different ordering when reloading page
	 * @param $idQuestion int the id of the question
	 * @param $answers array
	 */
	public function shuffleAnswers($idQuestion, $answers) {

		if (!is_array($answers) || empty($answers)) { return $answers; } //TODO: should this raise an exception ?

		//check the controller, only the player controller has the ability to store session info about the played test
		if (get_class(Yii::app()->controller) == 'PlayerController') {

			//check if in the session there are already shuffled answers for this question
			$shuffledAnswers = Yii::app()->controller->readSessionValue('shuffledAnswers');
			if (is_array($shuffledAnswers) && isset($shuffledAnswers[$idQuestion])) {

				//shuffling order is already present in session for this question, so order its answers accordingly
				$tmpAnswers = array();
				foreach ($shuffledAnswers[$idQuestion] as $idAnswer) {
					$found = false;
					$i = 0;
					while (!$found && $i < count($answers)) { //NOTE: $answers can be an array of arrays or an array of ARs objects
						if ($answers[$i]['id_answer'] == $idAnswer) {
							$found = true;
							$tmpAnswers[] = $answers[$i];
						}
						$i++;
					}
				}
				$answers = $tmpAnswers;

			} else {

				//check by database if answers have already been ordered
				$info = $this->retrieveQuestionTrackInfo($idQuestion);
				//TODO: handle error, $info === false
				if (is_array($info) && isset($info['shuffled_answers_order']) && !empty($info['shuffled_answers_order'])) {

					$tmpAnswers = array();
					foreach ($info['shuffled_answers_order'] as $idAnswer) {
						$found = false;
						$i = 0;
						while (!$found && $i < count($answers)) { //NOTE: $answers can be an array of arrays or an array of ARs objects
							if ($answers[$i]['id_answer'] == $idAnswer) {
								$found = true;
								$tmpAnswers[] = $answers[$i];
							}
							$i++;
						}
					}
					$answers = $tmpAnswers;

				} else {

					//no order in session neither in database, so shuffle the answers and store their order
					shuffle($answers);
					$order = array();
					foreach ($answers as $answer) { $order[] = (int)$answer['id_answer']; }  //NOTE: $answers can be an array of arrays or an array of ARs objects
					if (!is_array($shuffledAnswers)) { $shuffledAnswers = array(); }
					$shuffledAnswers[$idQuestion] = $order;
					Yii::app()->controller->setSessionValue('shuffledAnswers', $shuffledAnswers);

					//since user answer may be re-played or reported, we need to store the shuffled order in database too
					$this->storeQuestionTrackInfo(array(
						'shuffled_answers_order' => $order
					), $idQuestion);
				}

			}

		} else {

			//we are not playing the test. but shuffling is requested anyway
			shuffle($answers);

		}

		return $answers;
	}


	/**
	 * Given specific question and (optional)user, retrieve the tracking record for question
	 * @param $idQuestion
	 * @param bool $idUser
	 * @return LearningTestquestTrack|bool the found tracking record, or false if no record has been found
	 */
	protected function retrieveQuestionTrack($idQuestion, $idUser = false) {
		//check input parameters
		if (!$idUser) { $idUser = Yii::app()->user->id; }
		if ($idQuestion <= 0) return false;

		//retrieve the record (false if no record is found)
		$question = LearningTestquest::model()->findByPk($idQuestion);
		if (!$question) return false;
		$idTest = $question->idTest;
		if(!$idTest)
			$idTest = Yii::app()->request->getParam('id_test', false);
		$track = LearningTesttrack::model()->findByAttributes(array('idUser' => $idUser, 'idTest' => $idTest));
		if (!$track) return false;
		return $track;
	}


	/**
	 * Store additional info relative to question tracking
	 * @param $info an associative array containing the info to be stored
	 * @param $idQuestion
	 * @param bool $idUser
	 * @return bool if the operation was successful or not
	 */
	public function storeQuestionTrackInfo($info, $idQuestion, $idUser = false) {
		//check input validity
		if (!is_array($info)) return false;

		//retrieve question tracking
		$track = $this->retrieveQuestionTrack($idQuestion, $idUser);
		if (!$track) return false;
		$idTrack = $track->getPrimaryKey();

		//store data
		if ($idTrack) {
			$trackQuestion = LearningTesttrackQuest::model()->findByAttributes(array('idTrack' => $idTrack, 'idQuest' => $idQuestion));
			if (!empty($trackQuestion)) {
				$trackQuestion->more_info = CJSON::encode($info);
				$trackQuestion->save();
			}
		}

		return true;
	}


	/**
	 * Clear additional info relative to question tracking
	 * @param $idQuestion
	 * @param bool $idUser
	 * @return bool if the operation was successful or not
	 */
	public function clearQuestionTrackInfo($idQuestion, $idUser = false) {
		//retrieve question tracking
		$track = $this->retrieveQuestionTrack($idQuestion, $idUser);
		if (!$track) return false;
		$idTrack = $track->getPrimaryKey();

		//clear stored data by writing NULL value directly, whatever it was previously stored
		if ($idTrack) {
			$trackQuestion = LearningTesttrackQuest::model()->findByAttributes(array('idTrack' => $idTrack, 'idQuest' => $idQuestion));
			if (!empty($trackQuestion)) {
				$trackQuestion->more_info = NULL;
				$trackQuestion->save();
			}
		}

		return true;
	}


	/**
	 * Retreive additional info relative to question tracking
	 * @param $idQuestion
	 * @param bool $idUser
	 * @return array|bool|mixed the info retrieved and json decoded (if any info has been found, empty array otherwise)
	 */
	public function retrieveQuestionTrackInfo($idQuestion, $idUser = false) {
		//retrieve question tracking
		$track = $this->retrieveQuestionTrack($idQuestion, $idUser);
		if (!$track) return false;
		$idTrack = $track->getPrimaryKey();

		//read info from record and decode it
		$output = array();
		if ($idTrack) {
			$trackQuestion = LearningTesttrackQuest::model()->findByAttributes(array('idTrack' => $idTrack, 'idQuest' => $idQuestion));
			if (!empty($trackQuestion)) {
				if (!empty($trackQuestion->more_info)) {
					$output = CJSON::decode($trackQuestion->more_info);
				}
			}
		}

		return $output;
	}

}