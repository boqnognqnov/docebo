<?php

class QuestionAssociate extends CQuestionWithAnswers {


	public function __construct() {
		parent::__construct();
		$this->setType(LearningTestquest::QUESTION_TYPE_ASSOCIATE);
		$this->setEditProperty('category', true);
		$this->setEditProperty('difficulty', true);
		$this->setEditProperty('shuffle', true);
		$this->setEditProperty('answeringMaxTime', true);
		$this->setEditProperty('extraInfo', false);

		//set events
		//$this->attachEventHandler('onAfterAnswerCreate', array($this, 'createAssociation'));
		//$this->attachEventHandler('onAfterAnswerUpdate', array($this, 'updateAssociation'));
		$this->attachEventHandler('onBeforeAnswerDelete', array($this, 'deleteAssociation'));
	}


	public function play($idQuestion) {
		$answers = array();

		$question = LearningTestquest::model()->findByPk($idQuestion);
		$answers = $this->getAnswers($idQuestion);

		$test_shuffle = false;
		$test = $this->_controller->getTest();
		$testId = null;
		if($test){
			$testId = $test->idTest;
		}
		if(is_object($test))
			$test_shuffle = $test->getAttribute('shuffle_answer') == 1;

		$associations = LearningTestquestanswerAssociate::model()->findAllByAttributes(array('idQuest' => $idQuestion), ($question->shuffle || $test_shuffle ? array('order'=>'RAND()') : ''));

		if ($question->shuffle || $test_shuffle) {
			$answers = $this->shuffleAnswers($idQuestion, $answers);
		}

		$previous = $this->recoverUserAnswer($idQuestion, Yii::app()->user->id, $testId);

		$controller = $this->getController();
		$controller->renderPartial($this->getQuestionViewsPath() . $this->getType(), array(
			'questionManager' => $this,
			'idQuestion' => $idQuestion,
			'question' => $question,
			'answers' => $answers,
			'associations' => $associations,
			'previous' => $previous,
			'idTest' => $test ? $test->idTest : null,
		));

	}


	public function edit($event) {
		$controller = $this->getController();
		$cs = Yii::app()->getClientScript();
		$cs->registerCoreScript('jquery.ui');
		$assetsUrl = $controller->getPlayerAssetsUrl();
		// $cs->registerCssFile($assetsUrl.'/css/player-sprite.css');  // moved to themes
		$idQuestion = $event->params['idQuestion'];
		$controller->renderPartial('edit/_' . $this->getType(), array(
			'questionManager' => $this,
			'isEditing' => $event->params['isEditing'],
			'idQuestion' => $idQuestion,
			'answers' => $this->getAnswers($idQuestion),
			'associations' => QuestionAssociate::getAssociations($idQuestion)
		));
	}

	public function create($event) {
		//in this handler answers and association of the question are being stored

		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) {
			$transaction = $db->beginTransaction();
		}

		try {
			$question = $event->params['question'];

			$purifier = new CHtmlPurifier();
			$purifier = $purifier->setOptions(array(
				'Attr.AllowedFrameTargets' => array('_blank')
			));

			//first: check associations
			$bind = array(); //bind input indexes t oDB associations ID, either existent or newly created
			$existent = array();
			$existentAR = LearningTestquestanswerAssociate::model()->findAllByAttributes(array('idQuest' => $question->getPrimaryKey()));
			if (!empty($existentAR)) {
				foreach($existentAR as $ar) {
					$existent[$ar->getPrimaryKey()] = $ar;
				}
			}

			$associations = Yii::app()->request->getParam('elements_b');
			if (!empty($associations)) {
				$toKeep = array();
				$toCreate = array();
				foreach($associations as $index => $association) {

					if (isset($association['b_id']) && $association['b_id'] > 0) {
						$toKeep[] = $association['b_id'];
					} else {
						$toCreate[] = $association['value'];
					}
				}
				$toDelete = array_diff(array_keys($existent), $toKeep);
				if (!empty($toDelete)) {
					foreach($toDelete as $idAnswerToDelete) {
						$rs = LearningTestquestanswerAssociate::model()->deleteByPk($idAnswerToDelete);
					}
				}
				foreach($associations as $index => $association) {
					if (isset($association['b_id']) && $association['b_id'] > 0) {
						$ar = LearningTestquestanswerAssociate::model()->findByPk($association['b_id']);
					} else {
						$ar = new LearningTestquestanswerAssociate();
						$ar->idQuest = $question->getPrimaryKey();
					}
					$ar->answer = $association['value'];
					$rs = $ar->save();
					$bind[$index] = $ar->getPrimaryKey();
				}
			}

			//second: check answers
			$existent = array();
			$existentAR = LearningTestquestanswer::model()->findAllByAttributes(array('idQuest' => $question->getPrimaryKey()));
			if (!empty($existentAR)) {
				foreach($existentAR as $ar) {
					$existent[$ar->getPrimaryKey()] = $ar;
				}
			}
			$answers = Yii::app()->request->getParam('elements_a');
			if (!empty($answers)) {
				$toKeep = array();
				$toCreate = array();
				foreach($answers as $index => $answer) {
					if (isset($answer['a_id']) && $answer['a_id'] > 0) {
						$toKeep[] = $answer['a_id'];
					} else {
						$toCreate[] = $answer;
					}
				}
				//...
				$toDelete = array_diff(array_keys($existent), $toKeep);
				if (!empty($toDelete)) {
					foreach($toDelete as $idAnswerToDelete) {
						$rs = LearningTestquestanswer::model()->deleteByPk($idAnswerToDelete);
					}
				}
				$sequence = 0;
				foreach($answers as $index => $answer) {
					$sequence++;
					$associationIndex = (int)$answer['is_correct_index'];
					if (isset($answer['a_id']) && $answer['a_id'] > 0) {
						$ar = LearningTestquestanswer::model()->findByPk($answer['a_id']);
					} else {
						$ar = new LearningTestquestanswer();
						$ar->idQuest = $question->getPrimaryKey();
					}

					$ar->is_correct = $bind[$associationIndex]; //((isset($answer['correct']) && $answer['correct'] > 0) ? 1 : 0);
					$ar->answer = Yii::app()->htmlpurifier->purify($answer['value']);
					$ar->comment = Yii::app()->htmlpurifier->purify($answer['comment']);
					$ar->sequence = $sequence;
					$ar->score_correct = (double)$answer['score_correct'];
					$ar->score_incorrect = (double)$answer['score_incorrect'];
					$rs = $ar->save();
					if (!$rs) {
						//...
					}
				}
			}

			if (isset($transaction)) {
				$transaction->commit();
			}

		} catch (CException $e) {

			if (isset($transaction)) {
				$transaction->rollback();
			}
			throw $e;
		}
	}

	public function update($event) {
		//in this handler answers and association of the question are being stored

		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) {
			$transaction = $db->beginTransaction();
		}

		try {
			$question = $event->params['question'];

			$purifier = new CHtmlPurifier();
			$purifier = $purifier->setOptions(array(
				'Attr.AllowedFrameTargets' => array('_blank')
			));

			//first: check associations
			$bind = array(); //bind input indexes t oDB associations ID, either existent or newly created
			$existent = array();
			$existentAR = LearningTestquestanswerAssociate::model()->findAllByAttributes(array('idQuest' => $question->getPrimaryKey()));
			if (!empty($existentAR)) {
				foreach($existentAR as $ar) {
					$existent[$ar->getPrimaryKey()] = $ar;
				}
			}

			$associations = Yii::app()->request->getParam('elements_b');
			if (!empty($associations)) {
				$toKeep = array();
				$toCreate = array();
				foreach($associations as $index => $association) {

					if (isset($association['b_id']) && $association['b_id'] > 0) {
						$toKeep[] = $association['b_id'];
					} else {
						$toCreate[] = $association['value'];
					}
				}
				$toDelete = array_diff(array_keys($existent), $toKeep);
				if (!empty($toDelete)) {
					foreach($toDelete as $idAnswerToDelete) {
						$rs = LearningTestquestanswerAssociate::model()->deleteByPk($idAnswerToDelete);
					}
				}
				foreach($associations as $index => $association) {
					if (isset($association['b_id']) && $association['b_id'] > 0) {
						$ar = LearningTestquestanswerAssociate::model()->findByPk($association['b_id']);
					} else {
						$ar = new LearningTestquestanswerAssociate();
						$ar->idQuest = $question->getPrimaryKey();
					}
					$ar->answer = $association['value'];
					$rs = $ar->save();
					$bind[$index] = $ar->getPrimaryKey();
				}
			}

			//second: check answers
			$existent = array();
			$existentAR = LearningTestquestanswer::model()->findAllByAttributes(array('idQuest' => $question->getPrimaryKey()));
			if (!empty($existentAR)) {
				foreach($existentAR as $ar) {
					$existent[$ar->getPrimaryKey()] = $ar;
				}
			}
			$answers = Yii::app()->request->getParam('elements_a');
			if (!empty($answers)) {
				$toKeep = array();
				$toCreate = array();
				foreach($answers as $index => $answer) {
					if (isset($answer['a_id']) && $answer['a_id'] > 0) {
						$toKeep[] = $answer['a_id'];
					} else {
						$toCreate[] = $answer;
					}
				}
				//...
				$toDelete = array_diff(array_keys($existent), $toKeep);
				if (!empty($toDelete)) {
					foreach($toDelete as $idAnswerToDelete) {
						$rs = LearningTestquestanswer::model()->deleteByPk($idAnswerToDelete);
					}
				}
				$sequence = 0;
				foreach($answers as $index => $answer) {
					$sequence++;
					$associationIndex = (int)$answer['is_correct_index'];
					if (isset($answer['a_id']) && $answer['a_id'] > 0) {
						$ar = LearningTestquestanswer::model()->findByPk($answer['a_id']);
					} else {
						$ar = new LearningTestquestanswer();
						$ar->idQuest = $question->getPrimaryKey();
					}

					$ar->is_correct = $bind[$associationIndex]; //((isset($answer['correct']) && $answer['correct'] > 0) ? 1 : 0);
					$ar->answer = Yii::app()->htmlpurifier->purify($answer['value']);
					$ar->comment = Yii::app()->htmlpurifier->purify($answer['comment']);
					$ar->sequence = $sequence;
					$ar->score_correct = (double)$answer['score_correct'];
					$ar->score_incorrect = (double)$answer['score_incorrect'];
					$rs = $ar->save();
					if (!$rs) {
						//...
					}
				}
			}

			if (isset($transaction)) {
				$transaction->commit();
			}

		} catch (CException $e) {

			if (isset($transaction)) {
				$transaction->rollback();
			}
			throw $e;
		}

	}


	public function deleteAssociation($event) {
		//...
	}


	public static function getAssociations($idQuestion) {
		return LearningTestquestanswerAssociate::model()->findAllByAttributes(array('idQuest' => $idQuestion));
	}


	public static function getAssociation($idAnswer) {
		return LearningTestquestanswerAssociate::model()->findByPk(array('idAnswer' => $idAnswer));
	}


	/**
	 * Check if a user has already answered a question or not. Note that in association question it is not enough to check
	 * the presence of answering records in DB since they may all set to "not answered"
	 * @param $idTrack the ID of the test tracking
	 * @param $idQuestion the ID of the question to be analyzed
	 * @return bool user has already answered this question or not
	 */
	public function userDoAnswer($idTrack, $idQuestion) {
		$info = LearningTesttrackAnswer::model()->findAllByAttributes(array('idTrack' => $idTrack, 'idQuest' => $idQuestion));
		if (is_array($info) && !empty($info)) {
			//some records have been found in DB, if all of them are set to "0" (aka they are unanswered) the question has to be considered unanswered
			$unanswered = true;
			//check every single association answer. If at least one of them has been answered, then stop checking and consider the question answered
			for ($i=0; $i<count($info) && $unanswered; $i++) {
				if ($info[$i]->more_info != 0) {
					$unanswered = false;
				}
			}
			return $unanswered;
		}
		return (!empty($info));
	}


	public function storeUserAnswer($idTrack, $idQuestion, $answersInput, $canOverwrite) {

		if (!is_array($answersInput)) {
			$answersInput = $this->readAnswerInput($idQuestion);
			if (is_array($answersInput) && !$this->useMultipleChoice() && count($answersInput) > 1) {
				//error ...
			}
		}

		if ($this->userDoAnswer($idTrack, $idQuestion)) {
			if ($canOverwrite) {
				$this->deleteUserAnswer($idTrack, $idQuestion); //question answer will be entirely re-set from scratch
			}
			//NOTE: user may have answered only SOME of the possible associations, allow it to set the still unanswered associations
		}

		$storedAnswers = $this->getAnswers($idQuestion);
		if (empty($storedAnswers)) {
			return;
		}

		//since the "not answered" option is stored too, there may exist some records in DB. If this is the case then just update them.
		$userAnswers = array();
		$existing = LearningTesttrackAnswer::model()->findAllByAttributes(array('idTrack' => $idTrack, 'idQuest' => $idQuestion));
		if (is_array($existing)) {
			//make an indexed list of user existing answers
			foreach ($existing as $userAnswer) {
				$userAnswers[$userAnswer->idAnswer] = $userAnswer;
			}
		}

		//assign answers by input and calculate score
		foreach($storedAnswers as $storedAnswer) {

			$ida = $storedAnswer->getPrimaryKey();

			if (is_array($answersInput) && isset($answersInput[$ida])) {

				$existing = isset($userAnswers[$storedAnswer->idAnswer]);
				if ($existing && $existing->more_info > 0 && !$canOverwrite) { //NOTE: if "more_info" == 0 then the association has not been set
					//we can't update this answer due to test settings, so skip it and jump to check the next one
					continue;
				}

				//answer incorrect with penalty but not checked by the user
				$scoreAssigned = $storedAnswer->is_correct == $answersInput[$ida]
					? $storedAnswer->score_correct
					: -$storedAnswer->score_incorrect;

				if ($existing) {
					$newTrack = $userAnswers[$storedAnswer->idAnswer];
				} else {
					$newTrack = new LearningTesttrackAnswer();
					$newTrack->idTrack = $idTrack;
					$newTrack->idQuest = $idQuestion;
					$newTrack->idAnswer = $storedAnswer->idAnswer;
				}
				$newTrack->score_assigned = $scoreAssigned;
				$newTrack->more_info = $answersInput[$ida];
				$newTrack->user_answer = 0;
				if (!$newTrack->save()) {
					throw new CException('Error while storing user answer');
				}
			}
		}

		return true;
	}


	public static function exportToRaw($input) {

		//retriving question information
		if (is_numeric($input)) {
			$question = LearningTestquest::model()->findByPk($input);
			$idQuestion = $question->getPrimaryKey();
		} elseif (get_class($input) == 'LearningTestquest') {
			$question = $input;
			$idQuestion = $input->getPrimaryKey();
		} else {
			throw new CException('Invalid input');
		}

		if (!$question) {
			throw new CException('Invalid question');
		}

		//insert the question copy
		$oQuest = new QuestionRaw();
		$oQuest->id = $idQuestion;
		$oQuest->qtype = $question->type_quest;

		$oQuest->id_category = self::getCategoryName($question->idCategory);
		$oQuest->quest_text = $question->title_quest;
		$oQuest->difficult = $question->difficult;
		$oQuest->time_assigned = $question->time_assigned;

		$oQuest->answers = array();
		$oQuest->extra_info = array();

		$associationsList = array();

		//retrieveing associations
		$associations = LearningTestquestanswerAssociate::model()->findAllByAttributes(array('idQuest' => $idQuestion));
		if (!empty($associations)) {
			foreach($associations as $association) {
				$oAnswer = new AnswerRaw();
				$oAnswer->text = $association->answer;
				$associationsList[$association->idAnswer] = $oAnswer;
			}
		}

		//retriving new answer
		$criteria = new CDbCriteria();
		$criteria->order = "sequence ASC";
		$answers = LearningTestquestanswer::model()->findAllByAttributes(array('idQuest' => $idQuestion), $criteria);
		if (!empty($answers)) {
			foreach($answers as $answer) {
				$oAnswer = new AnswerRaw();
				$oAnswer->is_correct = $answer->is_correct;
				$oAnswer->text = $answer->answer;
				$oAnswer->comment = $answer->comment;
				$oAnswer->score_correct = $answer->score_correct;
				$oAnswer->score_penalty = $answer->score_incorrect;
				$oQuest->answers[] = $oAnswer;
				$oQuest->extra_info[] = $associationsList[$answer->is_correct];
			}
		}

		return $oQuest;
	}


	public function importFromRaw($rawQuestion, $idTest, $sequence = false, $page = false) {

		if (get_class($rawQuestion) == 'QuestionRaw' && $rawQuestion->qtype == $this->getType()) {

			if (Yii::app()->db->getCurrentTransaction() === NULL) {
				$transaction = Yii::app()->db->beginTransaction();
			}

			try {

				if (!$idTest || $idTest <= 0) {
					$idTest = 0;
				}
				if (!$sequence) {
					$sequence = 999999;
				}
				if (!$page) {
					$page = 1;
				}

				//insert question in DB
				$question = new LearningTestquest();
				$question->idTest = (int)$idTest;
				$question->idCategory = $rawQuestion->id_category;
				$question->type_quest = $rawQuestion->qtype;
				$question->title_quest = $rawQuestion->quest_text;
				$question->difficult = $rawQuestion->difficult;
				$question->time_assigned = $rawQuestion->time_assigned;
				$question->sequence = $sequence;
				$question->page = $page;

				if (!$question->save()) {
					throw new CException('Error while saving question');
				}

				//save associations
				$newCorrect = array();
				if (is_array($rawQuestion->extra_info)) {
					reset($rawQuestion->extra_info);
					foreach($rawQuestion->extra_info as $index => $rawAssociation) {
						$ass = new LearningTestquestanswerAssociate();
						$ass->idQuest = $question->getPrimaryKey();
						$ass->answer = $rawAssociation->text;
						if (!$ass->save()) {
							throw new CException('Error while saving question answers');
						}
						$newCorrect[$index] = $ass->getPrimaryKey();
					}
				}

				//save answers

				$checkScore = function ($score) {
					$score = '' . $score; //force casting as string
					if (empty($score)) {
						return '0';
					}
					$score = preg_replace(',', '.', $score);
					if (isset($score{0}) && $score{0} == '.') {
						$score = '0' . $score;
					}
					return $score;
				};

				if (is_array($rawQuestion->answers)) {
					foreach($rawQuestion->answers as $index => $rawAnswer) {

						if ($this->hasFullSupport) {
							if ($rawAnswer->score_correct > 0 && $rawAnswer->score_correct < 1 && $rawAnswer->is_correct) {
								// a little bit tricky but needed in order to obtain full support
								$rawAnswer->score_penalty = (-1)*$rawAnswer->score_correct;
								$rawAnswer->score_correct = 0;
							}
						}

						//insert answer in DB
						$answer = new LearningTestquestanswer();
						$answer->idQuest = $question->getPrimaryKey();
						$answer->is_correct = $newCorrect[$index];
						$answer->answer = $rawAnswer->text;
						$answer->comment = $rawAnswer->comment;
						$answer->score_correct = $checkScore($rawAnswer->score_correct);
						$answer->score_incorrect = $checkScore($rawAnswer->score_penalty);

						if (!$answer->save()) {
							throw new CException('Error while saving question answers');
						}
					}
				}

				if (isset($transaction)) {
					$transaction->commit();
				}
				return true;

			} catch (CException $e) {

				if (isset($transaction)) {
					$transaction->rollback();
				}
				return false;
			}

		} else {
			return false;
		}

	}

}