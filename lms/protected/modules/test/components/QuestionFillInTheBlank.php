<?php

class QuestionFillInTheBlank extends CQuestionWithAnswers {


	public function __construct() {
		parent::__construct();
		$this->setType(LearningTestquest::QUESTION_TYPE_FITB);
		$this->setEditProperty('category', true);
		$this->setEditProperty('difficulty', true);
		$this->setEditProperty('shuffle', false);
		$this->setEditProperty('answeringMaxTime', true);
		$this->setTransformation('title', array($this, 'transformTitle'));
		$this->setTransformation('review', array($this, 'transformTitleReview'));
	}

	public function transformTitle($inputData, $other) {

		$shortCodes = array();
		$inputs = array();
		$answersCount = Yii::app()->db->createCommand()
			->select(array('idAnswer', 'sequence'))
			->from(LearningTestquestanswer::model()->tableName())
			->where('idQuest = :quest')
			->queryAll(true, array(":quest" => $other->idQuestion));

		// Get everything the user has answered so far
		$trackAnswers = $other->questionManager->recoverUserAnswer($other->idQuestion, Yii::app()->user->id, isset($_GET['id_test']) ? $_GET['id_test'] : null);
		$userAnswers = array();
		foreach($trackAnswers as $track){
			$userAnswers[$track->idAnswer] = $track->more_info;
		}
		// Generate the user answers
		foreach($answersCount as $count){
			$shortCodes[] = '[answ '.$count['sequence'].']';
			$answer = isset($userAnswers[$count['idAnswer']]) ? $userAnswers[$count['idAnswer']] : null;
			$inputs[] = CHtml::textField('question['.$other->idQuestion.'][answ_'.$count['sequence'].']', $answer, array('class' => 'fitb'));
		}

		return str_replace(
			$shortCodes,
			$inputs,
			$inputData
		);
	}

	public function transformTitleReview($inputData, $other){
		$shortCodes = array();
		$inputs = array();
		$tmpUserAnswers = array();
		$systemsAnswers = LearningTestquestanswer::model()->findAllByAttributes(array('idQuest' => $other->idQuestion));
		$userAnswers = LearningTesttrackAnswer::model()->findAllByAttributes(array('idTrack' => $other->idTrack ,'idQuest' => $other->idQuestion));

		// Get the test Instance
		$idTest = Yii::app()->db->createCommand()
			->select('idTest')
			->from(LearningTesttrack::model()->tableName())
			->where('idTrack = :track')
			->queryScalar(array(':track' => $other->idTrack));
		$test = LearningTest::model()->findByPk($idTest);

		foreach($userAnswers as $key => $userAnswer){
			$tmpUserAnswers[$userAnswer->idAnswer] = $userAnswer;
		}
		foreach($systemsAnswers as $systemAnswer){
			$shortCodes[] = '[answ '.$systemAnswer->sequence.']';
			$userAnswer = isset($tmpUserAnswers[$systemAnswer->idAnswer]) ? $tmpUserAnswers[$systemAnswer->idAnswer] : null;
			if($userAnswer !== null && $userAnswer->user_answer){
				if(Yii::app()->controller->action->id == "testByUser"){
					$showAnswers = true;
				}else{
					$showAnswers = $test && $test->answersVisibleOnReview(Yii::app()->user->id);
				}
				if($showAnswers){
					$correct = self::isAnswerCorrect($systemAnswer->idAnswer,$other->idTrack);
					$style = $correct ? 'correct' : 'incorrect';
					$correctAnswerStyle = (isset($other->correctAnswerStyle))? $other->correctAnswerStyle : '';
					$incorrectAnswerStyle = (isset($other->incorrectAnswerStyle))? $other->incorrectAnswerStyle : '';

					$inputs[] = '<span class="'. $style .'" style="'.(($correct)? $correctAnswerStyle : $incorrectAnswerStyle ).'">' . $userAnswer->more_info . '</span>';
				} else {
					$tmpString = '';
					for ($i=0; $i < strlen($userAnswer->more_info); $i++){
						$tmpString .= "*";
					}
					$inputs[] = $tmpString;
				}
			}else{
				$inputs[] = '';
			}
		}

		return str_replace(
			$shortCodes,
			$inputs,
			$inputData
		);
	}

	public function play($idQuestion) {
	}

	public function storeUserAnswer($idTrack, $idQuestion, $answersInput, $canOverwrite) {
		$result = true;

		if (!is_array($answersInput)) {
			$answersInput = $this->readAnswerInput($idQuestion);
		}

		if ($this->userDoAnswer($idTrack, $idQuestion)) {
			if ($canOverwrite) {
				if (!$this->deleteUserAnswer($idTrack, $idQuestion)) return false;
			}
		}

		if(is_array($answersInput)){
			foreach($answersInput as $key => $input){
				unset($answersInput[$key]);
				$newKey = preg_replace("/[^0-9]*/", "", $key);
				$answersInput[$newKey] = $input;
			}
		}
		$storedAnswers = $this->getAnswers($idQuestion);

		$allShouldBeCorrect = CJSON::decode(Yii::app()->db->createCommand()
			->select('settings')
			->from(LearningTestquest::model()->tableName())
			->where('idQuest = :idQuest')
			->queryScalar(array(':idQuest' => $idQuestion )));

		$allCorrect = isset($allShouldBeCorrect['consider_correct_if_all_answers_are_correct']) && $allShouldBeCorrect['consider_correct_if_all_answers_are_correct'] == 'on' ? self::isAllQuestionAnswersCorrect($storedAnswers, $answersInput) : true;

		foreach($storedAnswers as $answer){
			// Check if the user has answered correct
			$userAnswer = '';
			$userScore = "-" . $answer->score_incorrect;
			// Let's check if the user has ANSWERED on this question
			if(isset($answersInput[$answer->sequence])){
				// Let's say by default that the user has NOT answered correctly
				$correct = false;
				// OK so now we will check if HE DID answered correctly in order to change the initial state
				$possibleAnswers = explode(',', $answer->answer);
				$userAnswer = $answersInput[$answer->sequence];
				foreach($possibleAnswers as $possible){
					// If the CASE SENSITIVE FLAG IS CHECKED then let's check them IN CASE SENSITIVE WAY
					if(isset($answer->settings['case_sensitive']) && $answer->settings['case_sensitive']){
						if ($possible === $userAnswer) {
							$correct = true;
							break;
						}
					}else{
						if (strcasecmp($possible, $userAnswer) == 0) {
							$correct = true;
							break;
						}
					}
				}
				// Let's assign some points to the user
				$userScore = $correct && $allCorrect ? $answer->score_correct : "-" . $answer->score_incorrect;
			}

			$existing = LearningTesttrackAnswer::model()->findByAttributes(array('idTrack' => $idTrack, 'idQuest' => $idQuestion, 'idAnswer' => $answer->idAnswer));
			if (!empty($existing)) {
				if (!$canOverwrite && !empty($existing->more_info)) { return $result; } //cannot overwrite a real existing answer
				$ar = $existing;
			} else {
				$ar = new LearningTesttrackAnswer();
				$ar->idTrack = $idTrack;
				$ar->idQuest = $idQuestion;
				$ar->idAnswer = $answer->idAnswer;
			}
			$ar->score_assigned = $userScore;
			$ar->more_info = $userAnswer;
			$ar->user_answer = (!empty($userAnswer) ? 1 : 0);

			if (!$ar->save()) {
				throw new CException('Error while storing user answer');
			}
		}
	}

	public static function isAllQuestionAnswersCorrect($storedAnswers,$answersInput){
		foreach($storedAnswers as $answer) {
			// Check if the user has answered correct
			// Let's check if the user has ANSWERED on this question
			if (isset($answersInput[$answer->sequence])) {
				// Let's say by default that the user has NOT answered correctly
				$correct = false;
				// OK so now we will check if HE DID answered correctly in order to change the initial state
				$possibleAnswers = explode(',', $answer->answer);
				$userAnswer = $answersInput[$answer->sequence];
				foreach ($possibleAnswers as $possible) {
					$settings = is_string($answer->settings) ? json_decode($answer->settings) : $answer->settings;
					// If the CASE SENSITIVE FLAG IS CHECKED then let's check them IN CASE SENSITIVE WAY
					if (isset($settings['case_sensitive']) && $settings['case_sensitive']) {
						if ($possible === $userAnswer) {
							$correct = true;
							break;
						}
					} else {
						if (strcasecmp($possible, $userAnswer) == 0) {
							$correct = true;
							break;
						}
					}
				}
				// If one of the answers is INCORRECT the all is incorrect
				if($correct == false)
					return false;
			}else{
				// If the user did not answer by default is INCORRECT
				return false;
			}
		}

		return true;
	}

	public function getAnswers($idQuestion) {
		$criteria = new CDbCriteria();
		$criteria->order = "sequence ASC";
		return LearningTestquestanswer::model()->findAllByAttributes(array('idQuest' => $idQuestion), $criteria);
	}

	public function create($event) {

		$db = Yii::app()->db;
		$answers = $this->getAnswersInput();

		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {

			$question = $event->params['question'];

			foreach ($answers as $answer) {

				$ar = new LearningTestquestanswer();
				$ar->idQuest = $question->idQuest;
				$ar->answer = Yii::app()->htmlpurifier->purify($answer['answers']);
				$ar->comment = Yii::app()->htmlpurifier->purify($answer['comment']);
				$ar->sequence = $answer['sequence'];
				$ar->is_correct = 1;
				$ar->score_correct = (double)$answer['score_correct'];
				$ar->score_incorrect = (double)$answer['score_incorrect'];
				$ar->settings = array('case_sensitive' => $answer['case_sensitive']);

				$this->onBeforeAnswerCreate(new CEvent($this, array('answer' => $ar, 'action' => 'create')));
				if (!$ar->save()) {
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				}
				$this->onAfterAnswerCreate(new CEvent($this, array('answer' => $ar, 'action' => 'create')));
			}

			if (isset($transaction)) { $transaction->commit(); }

		} catch (CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}

	}

	public function update($event) {

		$db = Yii::app()->db;
		$answers = $this->getAnswersInput();

		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {

			$question = $event->params['question'];

			$keepList = array();
			$checkList = array();
			foreach ($answers as $answer) {
				if (isset($answer['id_answer']) && (int)$answer['id_answer'] > 0) {
					$checkList[] = (int)$answer['id_answer'];
				}
			}

			$oldAnswers = LearningTestquestanswer::model()->findAllByAttributes(array('idQuest' => $question->idQuest));
			foreach ($oldAnswers as $oldAnswer) {
				$toCheck = (int)$oldAnswer->getPrimaryKey();
				if (!in_array($toCheck, $checkList)) {
					if (!$oldAnswer->delete()) {
						throw new CException('Error while updating answers');
					}
				} else {
					$keepList[$toCheck] = $oldAnswer;
				}
			}

			foreach ($answers as $answer) {

				$action = false;
				if (isset($answer['id_answer']) && $answer['id_answer'] > 0) {
					$index = (int)$answer['id_answer'];
					if (!isset($keepList[$index])) { throw new CException('Error while updating answers'); }
					$ar = $keepList[$index];
					$action = 'update';
				} else {
					$ar = new LearningTestquestanswer();
					$ar->idQuest = $question->idQuest;
					$action = 'create';
				}

				$ar->answer = Yii::app()->htmlpurifier->purify($answer['answers']);
				$ar->comment = Yii::app()->htmlpurifier->purify($answer['comment']);
				$ar->sequence = $answer['sequence'];
				$ar->is_correct = 1;
				$ar->score_correct = (double)$answer['score_correct'];
				$ar->score_incorrect = (double)$answer['score_incorrect'];
				$ar->settings = array('case_sensitive' => $answer['case_sensitive']);

				switch ($action) {
					case 'create': { $this->onBeforeAnswerCreate(new CEvent($this, array('answer' => $ar, 'action' => 'update'))); } break;
					case 'update': { $this->onBeforeAnswerUpdate(new CEvent($this, array('answer' => $ar, 'action' => 'update'))); } break;
				}
				if (!$ar->save()) {
					throw new CException('Error while updating answers');
				}
				switch ($action) {
					case 'create': { $this->onAfterAnswerCreate(new CEvent($this, array('answer' => $ar, 'action' => 'update'))); } break;
					case 'update': { $this->onAfterAnswerUpdate(new CEvent($this, array('answer' => $ar, 'action' => 'update'))); } break;
				}
			}

			if (isset($transaction)) { $transaction->commit(); }

		} catch (CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}

	}


	public function edit($event) {
		$controller = $this->getController();
		$cs = Yii::app()->getClientScript();
		$cs->registerCoreScript('jquery.ui');
		$assetsUrl = $controller->getPlayerAssetsUrl();
		// $cs->registerCssFile($assetsUrl.'/css/player-sprite.css');  // moved to themes

		// FCBK
		$themeUrl = Yii::app()->theme->baseUrl;
		$cs->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
		$cs->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');

		$cs->registerScriptFile($assetsUrl.'/js/testpoll.edit_question_fitb.js');
		$controller->renderPartial('edit/_'.$this->getType(), array(
			'questionManager' => $this,
			'isEditing' => $event->params['isEditing'],
			'idQuestion' => $event->params['idQuestion']
		));
	}

	public function _getAnswersList($idQuestion) {
		$criteria = new CDbCriteria();
		$criteria->order = 'sequence ASC';

		$answers = LearningTestquestanswer::model()->findAllByAttributes(array('idQuest' => $idQuestion), $criteria);
		if (!is_array($answers)) { return false; }
		$index = 0;
		$output = array();
		foreach ($answers as $answer) {
			//$trimmedAnswer = trim(preg_replace('/(^([\xA0\xC2]|&nbsp;|\s){1,})|(([\xA0\xC2]|&nbsp;|\s){1,}$)/','',$answer->answer));
			//NOTE: for some reasons above regex breaks UTF-8 encoding for some characters like 'à', 'è' etc.
			$trimmedAnswer = trim($answer->answer);
			$output[] = array(
				'index' => $index,
				'sequence' => (int)$answer->sequence,
				'id_answer' => (int)$answer->idAnswer,
				'is_correct' => ($answer->is_correct > 0 ? true : false),
				'answer' => (($trimmedAnswer != '')? $trimmedAnswer : 'error'),
				'comment' => $answer->comment,
				'answer_stripped' => strip_tags($answer->answer),
				'comment_stripped' => strip_tags($answer->comment),
				'score_correct' => (double)$answer->score_correct,
				'score_incorrect' => (double)$answer->score_incorrect
			);
			$index++;
		}
		return $output;
	}

	public static function isAnswerCorrect($idAnswer, $idTrack){
		$data = Yii::app()->db->createCommand()
			->from(LearningTesttrackAnswer::model()->tableName() . ' ltta')
			->leftJoin(LearningTestquestanswer::model()->tableName() . ' ltqa', 'ltqa.idAnswer = ltta.idAnswer')
			->where(array('and', 'ltta.idTrack = :track', 'ltta.idAnswer = :answ'))
			->queryRow(true, array(':track' => $idTrack, ':answ' => $idAnswer));
		$userAnswer = $data['more_info'];
		$possibleAnswers = explode(',', $data['answer']);
		$settings = CJSON::decode($data['settings']);
		foreach($possibleAnswers as $possible){
			// If the CASE SENSITIVE FLAG IS CHECKED then let's check them IN CASE SENSITIVE WAY
			if(isset($settings['case_sensitive']) && $settings['case_sensitive']){
				if ($possible === $userAnswer)
					return true;
			}else{
				if (strcasecmp($possible, $userAnswer) == 0)
					return true;
			}
		}
		return false;
	}

	public static function isCorrect($idTrack, $idQuest){
		$testSettings = CJSON::decode(Yii::app()->db->createCommand()
			->select('settings')
			->from(LearningTestquest::model()->tableName())
			->where('idQuest = :quest', array(':quest' => $idQuest))
			->queryScalar());
		$useAll = isset($testSettings['consider_correct_if_all_answers_are_correct']) && $testSettings['consider_correct_if_all_answers_are_correct'];
		$answers = Yii::app()->db->createCommand()
			->select('idAnswer')
			->from(LearningTestquestanswer::model()->tableName())
			->where('idQuest = :quest', array(':quest' => $idQuest))
			->queryColumn();
		$correct = 0;
		foreach($answers as $idAnswer){
			$isAnswerCorrect = self::isAnswerCorrect($idAnswer, $idTrack);
			if($isAnswerCorrect === true)
				$correct++;
		}

		// Ok let's check if we need to see if all the answers are correct in order the whole question to be correctly answered
		// otherwise we need just one correct
		return $useAll ? $correct == count($answers) : $correct > 0;
	}

}