<?php

class GiftFormatManager extends CComponent {

    // It is used for inline_choice to find out is text closed by punctuation
    private $closing_punctuation_mark = array('.', '!', '?');

	public function answerWeightParser(&$answer) {
		$answer = substr($answer, 1);
		$end_position = strpos($answer, "%");
		$answer_weight = substr($answer, 0, $end_position);	// gets weight as integer
		$answer_weight = $answer_weight / 100; // converts into percentage
		$answer = substr($answer, $end_position + 1); // removes comment from answer
		return $answer_weight;
	}



	public function commentParser(&$answer) {
		if (strpos($answer, "#") > 0) {
			$hashpos = strpos($answer, "#");
			$comment = substr($answer, $hashpos + 1);
			$comment = trim($this->escapedchar_post($comment));
			$answer = substr($answer, 0, $hashpos);
		} else {
			$comment = " ";
		}
		return $comment;
	}



	public function split_truefalse_comment($comment) {
		// splits up comment around # marks
		// returns an array of true/false feedback
		$bits = explode('#', $comment);
		$feedback = array('wrong' => $bits[0]);
		if (count($bits) >= 2) {
			$feedback['right'] = $bits[1];
		} else {
			$feedback['right'] = '';
		}
		return $feedback;
	}



	public function escapedchar_pre($string) {
		//Replaces escaped control characters with a placeholder BEFORE processing

		$escapedcharacters = array("\\:", "\\#", "\\=", "\\{", "\\}", "\\~", "\\n");	//dlnsk
		$placeholders = array("&&058;", "&&035;", "&&061;", "&&123;", "&&125;", "&&126;", "&&010");	//dlnsk

		$string = str_replace("\\\\", "&&092;", $string);
		$string = str_replace($escapedcharacters, $placeholders, $string);
		$string = str_replace("&&092;", "\\", $string);
		return $string;
	}



	public function escapedchar_post($string) {
		//Replaces placeholders with corresponding character AFTER processing is done
		$placeholders = array("&&058;", "&&035;", "&&061;", "&&123;", "&&125;", "&&126;", "&&010"); //dlnsk
		$characters = array(":", "#", "=", "{", "}", "~", "\n"); //dlnsk
		$string = str_replace($placeholders, $characters, $string);
		return $string;
	}

	/**
	 * Remove UTF8 Bom
	 *
	 * @param string $text
	 *
	 * @return string
	 */
	function remove_utf8_bom($text)
	{
		$bom = pack('H*','EFBBBF');
		$text = preg_replace("/^$bom/", '', $text);
		return $text;
	}


	public function checkAnswerCount($min, $answers, $text) {
		$countanswers = count($answers);
		if ($countanswers < $min) {
			//$importminerror = get_string( 'importminerror', 'quiz' );
			//$this->error( $importminerror, $text );
			return false;
		}

		return true;
	}



	public function defaultQuestion() {

		//require_once($GLOBALS['where_lms'] . '/modules/question/class.question.php');
		$question = new QuestionRaw();

		return $question;
	}



	public function readQuestions($lines) {

		$questions = array();
		$currentquestion = array();

		foreach ($lines as $line) {
			$line = trim($line);
			if (empty($line)) {
				if (!empty($currentquestion)) {
					if ($question = $this->readQuestion($currentquestion)) {
						$questions[] = $question;
					}
					$currentquestion = array();
				}
			} else {
				$currentquestion[] = $line;
			}
		}

		if (!empty($currentquestion)) {	// There may be a final question
			if ($question = $this->readQuestion($currentquestion)) {
				$questions[] = $question;
			}
		}

		return $questions;
	}



	public function readQuestion($lines) {
		// Given an array of lines known to define a question in this format, this function
		// converts it into a question object suitable for processing and insertion.

		$question = $this->defaultQuestion();
		$comment = NULL;

		// define replaced by simple assignment, stop redefine notices
		$gift_answerweight_regex = "^%\-*([0-9]{1,2})\.?([0-9]*)%";

		// REMOVED COMMENTED LINES and IMPLODE
		foreach ($lines as $key => $line) {
			$line = $this->remove_utf8_bom($line);
			$line = trim($line);
			if (substr($line, 0, 2) == "//") {
				$lines[$key] = " ";
			}
		}

		$text = trim(implode(" ", $lines));

		if ($text == "") {
			return false;
		}

		// Substitute escaped control characters with placeholders
		$text = $this->escapedchar_pre($text);

		// Look for category modifier ---------------------------------------------------------
		if (preg_match('/^\$CATEGORY:/', $text)) {
			// $newcategory = $matches[1];
			$newcategory = trim(substr($text, 10));

			// build fake question to contain category
			// XXX: create a category !
			return true;
		}

		// QUESTION NAME parser --------------------------------------------------------------
		if (substr($text, 0, 2) == "::") {
			$text = substr($text, 2);

			$namefinish = strpos($text, "::");
			if ($namefinish === false) {
				$question->prompt = false;
				// name will be assigned after processing question text below
			} else {
				$questionname = substr($text, 0, $namefinish);
				$question->prompt = trim($this->escapedchar_post($questionname));
				$text = trim(substr($text, $namefinish + 2)); // Remove name from text
			}
		} else {
			$question->prompt = false;
		}
        
        
        // It is used to find out is "inline_choice (Mising word by GIFT standard)" 
        $pattern = preg_quote(implode('', $this->closing_punctuation_mark));
        if(preg_match("/[A-Za-z0-9$pattern]+\z/", $text)) {
            $is_inline_choice = true;
        }  
        
		// FIND ANSWER section -----------------------------------------------------------------
		// no answer means its a description
		$answerstart = strpos($text, "{");
		$answerfinish = strpos($text, "}");

		$description = false;
		if (($answerstart === false) and ($answerfinish === false)) {
			$description = true;
			$answertext = '';
			$answerlength = 0;
		} elseif (!(($answerstart !== false) and ($answerfinish !== false))) {
			//$this->error( get_string( 'braceerror', 'quiz' ), $text );
			return false;
		} else {
			$answerlength = $answerfinish - $answerstart;
			$answertext = trim(substr($text, $answerstart + 1, $answerlength - 1));
		}


		// Format QUESTION TEXT without answer, inserting "_____" as necessary
		if ($description) {
			$text = $text;
		} elseif (substr($text, -1) == "}" || $is_inline_choice) {
			// no blank line if answers follow question, outside of closing punctuation
			$text = substr_replace($text, "", $answerstart, $answerlength + 1);
		} else {
			// inserts blank line for missing word format
			$text = substr_replace($text, "_____", $answerstart, $answerlength + 1);
		}

		// get text format from text
		$oldtext = $text;
		$textformat = 0;
		if (substr($text, 0, 1) == '[') {
			$text = substr($text, 1);
			$rh_brace = strpos($text, ']');
			$qtformat = substr($text, 0, $rh_brace);
			$text = substr($text, $rh_brace + 1);
		}
		// i must find out for what this param is used
		$question->textformat = $textformat;

		// question text
		$question->quest_text = trim($this->escapedchar_post($text));

		// set question name if not already set
		if ($question->prompt === false) {
			$question->prompt = $question->quest_text;
		}

		// ensure name is not longer than 250 characters
		$question->prompt = $question->prompt;
		$question->prompt = strip_tags(substr($question->prompt, 0, 250));

		// determine QUESTION TYPE -------------------------------------------------------------
		$question->qtype = NULL;

		// give plugins first try
		// plugins must promise not to intercept standard qtypes
		// MDL-12346, this could be called from lesson mod which has its own base class =(
		/*
		  if (method_exists($this, 'try_importing_using_qtypes') && ($try_question = $this->try_importing_using_qtypes( $lines, $question, $answertext ))) {
		  return $try_question;
		  }
		 */
		if ($description) {
			$question->qtype = 'title';
		} elseif ($answertext == '') {
			$question->qtype = 'extended_text';
		} elseif ($answertext{0} == "#") {
			$question->qtype = 'numerical';
		} elseif (substr_count($answertext, "=") > 0 && (substr_count($answertext, '~') < 1) && (substr_count($answertext, '->') < 1)) {
			$question->qtype = 'text_entry';
        } elseif($is_inline_choice){
            $question->qtype = 'inline_choice';
        //"inline_choice" is equal of "Missing word" type in GIFT standard 
		} elseif (substr_count($answertext, "=") == 1) {
			// only one answer allowed (the default)
			$question->qtype = 'choice';
		} elseif(substr_count($answertext, '=') > 1 || substr_count($answertext, '~%') > 1){
			if(strpos($answertext, "->") === false){
				// Multiplechoice questions can have more than one correct answer
				$question->qtype = 'choice_multiple';
			}elseif(strpos($answertext, "->") !== false){
				// only Matching contains both = and ->
				$question->qtype = 'associate';
			}else{
				throw new CException('Invalid question type. It looks like a Multiple Choice or Association question, but has an invalid format.');
			}
		} else { // either TRUEFALSE or SHORTANSWER
			// TRUEFALSE question check
			$truefalse_check = $answertext;
			if (strpos($answertext, "#") > 0) {

				// strip comments to check for TrueFalse question
				$truefalse_check = trim(substr($answertext, 0, strpos($answertext, "#")));
			}
			$valid_tf_answers = array("T", "TRUE", "F", "FALSE");
			if (in_array($truefalse_check, $valid_tf_answers)) {

				$question->qtype = 'truefalse';
			} else { // Must be SHORTANSWER
				$question->qtype = 'shortanswer';
			}
		}

		if (!isset($question->qtype)) {
			return false;
		}

		switch ($question->qtype) {
			case 'extended_text' :
			case 'title' : {

					return $question;
				};
				break;
            case 'text_entry' :
			case 'choice' :
            case 'inline_choice' :    
			case 'choice_multiple' : {

					$answertext = str_replace("=", "~=", $answertext);
					$answers = explode("~", $answertext);
					if (isset($answers[0])) {
						$answers[0] = trim($answers[0]);
					}
					if (empty($answers[0])) {
						array_shift($answers);
					}
					$countanswers = count($answers);

					if (!$this->checkAnswerCount(2, $answers, $text) && $question->qtype != 'text_entry') {
						return false;
					}
					$num_correct = 0;
					foreach ($answers as $key => $answer) {
						$answer = trim($answer);

						$oAnswer = new AnswerRaw();

						// determine answer weight
						if ($answer[0] == "=") {
							$answer = substr($answer, 1);
							if (preg_match('/'.$gift_answerweight_regex.'/', $answer))
								$answer_weight = $this->answerWeightParser($answer);
							else
								$answer_weight = 1;

							$oAnswer->is_correct = 1;
						} elseif (preg_match('/'.$gift_answerweight_regex.'/', $answer)) {		// check for properly formatted answer weight
							$answer_weight = $this->answerWeightParser($answer);
							$oAnswer->is_correct = ($answer_weight > 0 ? 1 : 0 );
						} else {		 //default, i.e., wrong anwer
							$answer_weight = 0;
							$oAnswer->is_correct = 0;
						}

						if ($oAnswer->is_correct)
							$num_correct++;
						if ($answer_weight >= 0)
							$oAnswer->score_correct = $answer_weight;
						else
							$oAnswer->score_penalty = (-1) * $answer_weight;
						$oAnswer->comment = $this->commentParser($answer); // commentParser also removes comment from $answer
						$oAnswer->text = $this->escapedchar_post($answer);

						$question->answers[] = $oAnswer;
					}	// end foreach answer
					if ($num_correct > 1)
						$question->qtype = 'choice_multiple';

					return $question;
				};
				break;
			case 'associate' : {
					$answers = explode("=", $answertext);
					if (isset($answers[0])) {
						$answers[0] = trim($answers[0]);
					}
					if (empty($answers[0])) {
						array_shift($answers);
					}

					if (!$this->checkAnswerCount(2, $answers, $text)) {
						return false;
					}

					foreach ($answers as $key => $answer) {
						$answer = trim($answer);
						if (strpos($answer, "->") === false) {

							return false;
						}

						$oAnswer = new AnswerRaw();
						$oExtra = new AnswerRaw();

						$marker = strpos($answer, "->");
						$oAnswer->text = trim($this->escapedchar_post(substr($answer, 0, $marker)));
						$oExtra->text = trim($this->escapedchar_post(substr($answer, $marker + 2)));

						$question->answers[] = $oAnswer;
						$question->extra_info[] = $oExtra;
					}	// end foreach answer

					return $question;
				};
				break;
			case 'truefalse' : {

					$answer = $answertext;
					$comment = $this->commentParser($answer); // commentParser also removes comment from $answer
					$feedback = $this->split_truefalse_comment($comment);

					$true_answer = new AnswerRaw();
					$true_answer->text = Yii::t('test', '_TRUE');
					$false_answer = new AnswerRaw();
					$false_answer->text = Yii::t('test', '_FALSE');

					if ($answer == "T" OR $answer == "TRUE") {
						$true_answer->is_correct = 1;
						$true_answer->score_correct = 1;
					} else {
						$false_answer->is_correct = 1;
						$false_answer->score_correct = 1;
					}

					$true_answer->comment = $feedback['right'];
					$false_answer->comment = $feedback['wrong'];

					$question->answers[] = $true_answer;
					$question->answers[] = $false_answer;

					// change because now we emulate this type of question
					$question->qtype = 'choice';

					return $question;
				};
				break;
			case 'shortanswer' : {

					$answers = explode("~", $answertext);
					if (isset($answers[0])) {
						$answers[0] = trim($answers[0]);
					}
					if (empty($answers[0])) {
						array_shift($answers);
					}
					$countanswers = count($answers);

					if (!$this->checkAnswerCount(2, $answers, $text)) {
						return false;
					}

					foreach ($answers as $key => $answer) {

						$answer = trim($answer);

						$oAnswer = new AnswerRaw();
						// Answer Weight
						if (preg_match('/'.$gift_answerweight_regex.'/', $answer)) {		// check for properly formatted answer weight
							$answer_weight = $this->answerWeightParser($answer);
						} else {		 //default, i.e., full-credit anwer
							$answer_weight = 1;
						}
						$oAnswer->is_correct = ($answer_weight > 0 ? 1 : 0 );

						if ($answer_weight >= 0)
							$oAnswer->score_correct = $answer_weight;
						else
							$oAnswer->score_penalty = (-1) * $answer_weight;
						$oAnswer->comment = $this->commentParser($answer); // commentParser also removes comment from $answer
						$oAnswer->text = $this->escapedchar_post($answer);

						$question->answers[] = $oAnswer;
					}	// end foreach answer
					// change because now we emulate this type of question
					$question->qtype = 'choice';

					return $question;
				};
				break;
			case 'numerical' : {
					// Note similarities to ShortAnswer
					$answertext = substr($answertext, 1); // remove leading "#"
					// If there is feedback for a wrong answer, store it for now.
					if (($pos = strpos($answertext, '~')) !== false) {
						$wrongfeedback = substr($answertext, $pos);
						$answertext = substr($answertext, 0, $pos);
					} else {
						$wrongfeedback = '';
					}

					$answers = explode("=", $answertext);
					if (isset($answers[0])) {
						$answers[0] = trim($answers[0]);
					}
					if (empty($answers[0])) {
						array_shift($answers);
					}

					if (count($answers) == 0) {
						// invalid question
						return false;
					}

					foreach ($answers as $key => $answer) {
						$answer = trim($answer);

						$oAnswer = new AnswerRaw();

						// Answer weight
						if (preg_match('/'.$gift_answerweight_regex.'/', $answer)) {
							// check for properly formatted answer weight
							$answer_weight = $this->answerWeightParser($answer);
						} else {
							//default, i.e., full-credit anwer
							$answer_weight = 1;
						}

						$oAnswer->score_correct = $answer_weight;
						$oAnswer->comment = $this->commentParser($answer); //commentParser also removes comment from $answer
						//Calculate Answer and Min/Max values
						if (strpos($answer, "..") > 0) { // optional [min]..[max] format
							$marker = strpos($answer, "..");
							$max = trim(substr($answer, $marker + 2));
							$min = trim(substr($answer, 0, $marker));
							$ans = ($max + $min) / 2;
							$tol = $max - $ans;
						} elseif (strpos($answer, ":") > 0) { // standard [answer]:[errormargin] format
							$marker = strpos($answer, ":");
							$tol = trim(substr($answer, $marker + 1));
							$ans = trim(substr($answer, 0, $marker));
						} else { // only one valid answer (zero errormargin)
							$tol = 0;
							$ans = trim($answer);
						}

						if (!(is_numeric($ans) || $ans = '*') || !is_numeric($tol)) {

							return false;
						}

						// store results
						$oAnswer->text = $ans;
						$oAnswer->tolerance = $tol;

						$question->answers[] = $oAnswer;
					} // end foreach

					/*
					  if ($wrongfeedback) {
					  $oAnswer = new AnswerRaw();
					  $oAnswer->score_correct = 0;
					  $oAnswer->comment = $this->commentParser($wrongfeedback);
					  $oAnswer->text = '';
					  $oAnswer->tolerance = '';
					  $question->answers[] = $oAnswer;
					  } */

					return $question;
				};
				break;
			default: {
					//$giftnovalidquestion = get_string('giftnovalidquestion','quiz');
					//$this->error( $giftnovalidquestion, $text );
					return false;
				};
				break;
		} // end switch ($question->qtype)
	}



	public function repchar($text, $format = 0) {
		// escapes 'reserved' characters # = ~ { ) : and removes new lines
		// also pushes text through format routine
		$reserved = array('#', '=', '~', '{', '}', ':', "\n", "\r");
		$escaped = array('\#', '\=', '\~', '\{', '\}', '\:', '\n', ''); //dlnsk

		$newtext = str_replace($reserved, $escaped, $text);
		$format = 0; // turn this off for now
		if ($format) {
			//$newtext = format_text( $format );
		}
		return $newtext;
	}



	public function writeQuestion($question) {
		// turns question into string
		// question reflects database fields for general question and specific to type
		// initial string;
		$expout = "";

		// add comment
		$expout .= "// question: $question->id  \n";

		// get  question text format
		/* $textformat = $question->textformat;
		  $question->text = "";
		  if ($textformat!=FORMAT_MOODLE) {
		  $question->text = text_format_name( (int)$textformat );
		  $question->text = "[$question->text]";
		  } */
		$qtext_format = "[" . $question->qtype . "]";
		// output depends on question type
		switch ($question->qtype) {
			case 'category' : {
					// not a real question, used to insert category switch
					$expout .= "\$CATEGORY: $question->category\n";
				};
				break;
			case 'title' : {
					if ($question->prompt != '')
						$expout .= '::' . $this->repchar($question->prompt) . '::';
					$expout .= $qtext_format;
					$expout .= $this->repchar($question->quest_text);
					$expout .= "\n";
				};
				break;
			case 'extended_text' : {
					$expout .= '::' . $this->repchar($question->prompt) . '::';
					$expout .= $qtext_format;
					$expout .= $this->repchar($question->quest_text);
					$expout .= "{}\n";
				};
				break;
			case 'truefalse' : {/*
				  $trueanswer = $question->options->answers[$question->options->trueanswer];
				  $falseanswer = $question->options->answers[$question->options->falseanswer];
				  if ($trueanswer->fraction == 1) {
				  $answertext = 'TRUE';
				  $right_feedback = $trueanswer->feedback;
				  $wrong_feedback = $falseanswer->feedback;
				  } else {
				  $answertext = 'FALSE';
				  $right_feedback = $falseanswer->feedback;
				  $wrong_feedback = $trueanswer->feedback;
				  }

				  $wrong_feedback = $this->repchar($wrong_feedback);
				  $right_feedback = $this->repchar($right_feedback);
				  $expout .= "::".$this->repchar($question->prompt)."::".$qtext_format.$this->repchar( $question->quest_text )."{".$this->repchar( $answertext );
				  if ($wrong_feedback) {
				  $expout .= "#" . $wrong_feedback;
				  } else if ($right_feedback) {
				  $expout .= "#";
				  }
				  if ($right_feedback) {
				  $expout .= "#" . $right_feedback;
				  }
				  $expout .= "}\n"; */
				}//;break;

			case 'shortanswer' : {/*
				  $expout .= "::".$this->repchar($question->prompt)."::".$qtext_format.$this->repchar( $question->quest_text )."{\n";
				  foreach($question->options->answers as $answer) {
				  $weight = 100 * $answer->score_correct;
				  $expout .= "\t=%".$weight."%".$this->repchar( $answer->text )."#".$this->repchar( $answer->comment )."\n";
				  }
				  $expout .= "}\n"; */
				}//;break;
			case 'text_entry' : {
				$expout .= "::".$this->repchar($question->prompt)."::".$qtext_format.$this->repchar( $question->quest_text )."{";
				  foreach($question->answers as $answer) {
				  	$weight = 100 * $answer->score_correct;
				  	$expout .= "=%".$weight."%".$this->repchar( $answer->text )."#".$this->repchar( $answer->comment )."";
				  	}
				  $expout .= "}\n"; 
				}//;
				break;
			case 'choice' : {
					$expout .= "::" . $this->repchar($question->prompt) . "::" . $qtext_format . $this->repchar($question->quest_text) . "{\n";

					foreach ($question->answers as $answer) {
						if ($answer->is_correct) {
							$export_weight = $answer->score_correct * 100;
							$answertext = "=%$export_weight%";
						} else {
							$export_weight = $answer->score_correct * 100;
							$answertext = "~%$export_weight%";
						}
						$expout .= "\t" . $answertext . $this->repchar($answer->text);
						if ($answer->comment != "") {
							$expout .= "#" . $this->repchar($answer->comment);
						}
						$expout .= "\n";
					}
					$expout .= "}\n";
				};
				break;
			case 'choice_multiple' : {
					$expout .= "::" . $this->repchar($question->prompt) . "::" . $qtext_format . $this->repchar($question->quest_text) . "{\n";

					foreach ($question->answers as $answer) {
						if ($answer->is_correct) {
							$export_weight = $answer->score_correct * 100;
							$answertext = "~%$export_weight%";
						} else {
							$export_weight = (-$answer->score_penalty) * 100;
							$answertext = "~%$export_weight%";
						}
						$expout .= "\t" . $answertext . $this->repchar($answer->text);
						if ($answer->comment != "") {
							$expout .= "#" . $this->repchar($answer->comment);
						}
						$expout .= "\n";
					}
					$expout .= "}\n";
				};
				break;
            
            //"inline_choice" is equal of "Missing word" type in GIFT standard 
            case 'inline_choice': {
                    $question_text = trim(str_replace(array("\xC2", "\xA0", '\n'), " ",strip_tags($this->repchar($question->quest_text))));
                    $str_last_element = substr($question_text, -1); 
                    
                    $key_closing_punctuation_mark = array_search($str_last_element, $this->closing_punctuation_mark, true);
                    
                    // If there is no closing punctuation I add "."
                    if ($key_closing_punctuation_mark === false) {
                        $str_last_element = '.';
                    } else {
                        $str_last_element = $this->closing_punctuation_mark[$key_closing_punctuation_mark];
                        $question_text = substr($question_text, 0, -1);
                    } 
                    
					$expout .= "::" . $this->repchar($question->prompt) . "::" . $qtext_format . $question_text . "{\n";
                    
					foreach ($question->answers as $answer) {
						if ($answer->is_correct) {
							$export_weight = $answer->score_correct * 100;
							$answertext = "=%$export_weight%";
						} else {
							$export_weight = $answer->score_correct * 100;
							$answertext = "~%$export_weight%";
						}
						$expout .= "\t" . $answertext . $this->repchar($answer->text);
						if ($answer->comment != "") {
							$expout .= "#" . $this->repchar($answer->comment);
						}
						$expout .= "\n";
					}
					$expout .= "}$str_last_element\n";
				};
                break;
                
			case 'text_entry' : {
					$expout .= "::" . $this->repchar($question->prompt) . "::" . $qtext_format . $this->repchar($question->quest_text) . "{\n";
					foreach ($question->answers as $answer) {
							$export_weight = $answer->score_correct * 100;
							$answertext = "=%$export_weight%";
						$expout .= "\t" . $answertext . $this->repchar($answer->text);
						if ($answer->comment != "") {
							$expout .= "#" . $this->repchar($answer->comment);
						}
						$expout .= "\n";
					}
					$expout .= "}\n";
				};
				break;
			case 'associate' : {
					$expout .= "::" . $this->repchar($question->prompt) . "::" . $qtext_format . $this->repchar($question->quest_text) . "{\n";
					foreach ($question->answers as $i => $subquestion) {
						$expout .= "\t=" . $this->repchar($subquestion->text) . " -> " . $this->repchar($question->extra_info[$i]->text) . "\n";
					}
					$expout .= "}\n";
				};
				break;
			case 'NUMERICAL':
				$expout .= "::" . $this->repchar($question->prompt) . "::" . $qtext_format . $this->repchar($question->quest_text) . "{#\n";
				foreach ($question->options->answers as $answer) {
					if ($answer->text != '') {
						$percentage = '';
						if ($answer->score_correct < 1) {
							$pval = $answer->score_correct * 100;
							$percentage = "%$pval%";
						}
						$expout .= "\t=$percentage" . $answer->text . ":" . (float) $answer->tolerance . "#" . $this->repchar($answer->comment) . "\n";
					} else {
						$expout .= "\t~#" . $this->repchar($answer->comment) . "\n";
					}
				}
				$expout .= "}\n";
				break;
			case 'DESCRIPTION':
				$expout .= "// DESCRIPTION type is not supported\n";
				break;
			case 'MULTIANSWER':
				$expout .= "// CLOZE type is not supported\n";
				break;
			default:

				return false;
		}
		// add empty line to delimit questions
		$expout .= "\n";
		return $expout;
	}

}
