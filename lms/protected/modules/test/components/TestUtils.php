<?php

class TestUtils extends CComponent {
	

	const TEST_SESSION_PREFIX = 'LO_test_session_data';
	
	/**
	 * returns the users score for a list of test
	 * @param array $idTestList	an array with the id of the test for which the function must retrive scores
	 * @param array $idUserList	the students of the course
	 * @param boolean $pure if set to true, do not add bonus score
	 *
	 * @return array 	a matrix with the index [id_test] [id_user] and the values in
	 *					['idTest',' idUser', 'date_attempt', 'type_of_result', 'result', 'score_status', 'comment']
	 */
	public static function getTestsScores($idTestList, $idUserList = false, $pure = false) {
//return array();
		if (is_numeric($idTestList) && (int)$idTestList > 0) { $idTestList = array((int)$idTestList); }
		if (!is_array($idTestList) || empty($idTestList)) { return array(); }

		if (is_numeric($idUserList) && (int)$idUserList > 0) { $idUserList = array((int)$idUserList); }
		if (!is_array($idUserList) || !empty($idUserList)) { $idUserList = false; }
		
		$output = array();
		
		/*
		 * Example query:
		$query = "SELECT tt.* , COUNT(*) AS times
			FROM learning_testtrack tt
			LEFT JOIN learning_testtrack_times ttt ON (tt.idTrack = ttt.idTrack AND tt.idTest = ttt.idTest)
			WHERE tt.idUser IN (20041,20050)
			AND tt.idTest IN (20,23,27,24,26,28,83,225,278)
			GROUP BY tt.idTrack, tt.idTest";
		*/
		
		$t1 = LearningTesttrack::model()->tableName();
		$t2 = LearningTesttrackTimes::model()->tableName();
		
		$command = Yii::app()->db->createCommand()
			->select($t1.".*, COUNT(*) AS times")
			->from($t1)
			->leftJoin($t2, $t1.".idTrack = ".$t2.".idTrack AND ".$t1.".idTest = ".$t2.".idTest")
			->where(array('IN', $t1.".idTest", $idTestList));
		if (!empty($idUserList)) { $command->andWhere(array('IN', $t1.".idUser", $idUserList)); }
		$command->group(array($t1.".idTest", $t1.".idTrack"));
		
		$rs = $command->query();
		while ($record = $rs->read()) {
			$output[(int)$record['idTest']][(int)$record['idUser']] = $record;
		}

		return $output;
		/*
			if($test_data['date_attempt_mod'] != NULL && $test_data['date_attempt_mod'] !== '0000-00-00 00:00:00') {
				$test_data['date_attempt'] = $test_data['date_attempt_mod'];
			}
			if(!$pure) $test_data['score'] = $test_data['score'] + $test_data['bonus_score'];
			$data[$test_data['idTest']][$test_data['idUser']] = $test_data;
		 */
	}


	/**
	 * Get average score for a given test
	 * If $idUser is present - it will exclude the user (i.e. give OTHER users avg)
	 * @param $idTest
	 * @param null $idUser
	 * @param bool $includeBonus
	 * @return string
	 */
	public static function getAverageScoresByTest($idTest, $idUser = null, $includeBonus = false, $percentage = false)
	{
		$t1 = LearningTesttrack::model()->tableName();
		$t2 = LearningTesttrackTimes::model()->tableName();

		$command = Yii::app()->db->createCommand()
			->select($t1.".score, ".$t1.".bonus_score AS times")
			->from($t1)
			->join($t2, $t1.".idTrack = ".$t2.".idTrack AND ".$t1.".idTest = ".$t2.".idTest")
			->where($t1.'.idTest = '.(int)$idTest);
		if ($idUser !== null) { $command->andWhere($t1.".idUser != ".(int)$idUser); }
		$command->group(array($t1.".idTest", $t1.".idTrack"));

		$rs = $command->query();
		$i = 0;
		$total = 0;
		$avg = 0.00;
		while ($record = $rs->read()) {
			$i++;
			$total += ($includeBonus ? ($record['score'] + $record['bonus_score']) : $record['score']);
		}

		if ($total > 0 && $i > 0)
			$avg = $total / $i;

		if ($percentage)
		{
			$test = LearningTest::model()->findByPk($idTest);
			if ($test && $test->score_max > 0 && $avg > 0)
			{
				$avg = $avg / $test->score_max * 100;
			}
		}

		return number_format((float)$avg, 2, '.', '');
	}
	
	public static function getMaxScore($idTest, $idUser) {
		return 0;
	}
	
	
	public static function getNumberOfQuestion($idTest) {
		return 0;
	}

	
	/**
	 * Start a TEST session
	 * @param number  $idTest
	 */
	public static function startSession($idTest) {
		$sessionValue = Yii::app()->session[self::TEST_SESSION_PREFIX];
		if (!is_array($sessionValue)) { $sessionValue = array(); }
		if (!isset($sessionValue[$idTest])) { $sessionValue[$idTest] = array(); }
		Yii::app()->session[self::TEST_SESSION_PREFIX] = $sessionValue;
	}

	
	/**
	 * End TEST session
	 * @param number $idTest
	 */
	public static function endSession($idTest) {
		$sessionValue = Yii::app()->session[self::TEST_SESSION_PREFIX];
		if (is_array($sessionValue) && isset($sessionValue[$idTest])) {
			$newSessionValue = array();
			foreach ($sessionValue as $key => $value) {
				if ($key != $idTest) { $newSessionValue[$key] = $value; }
			}
			Yii::app()->session[self::TEST_SESSION_PREFIX] = $newSessionValue;
		}
	}
	

	/**
	 * Set a TEST session key => value pair
	 * 
	 * @param number $idTest
	 * @param mixed $key
	 * @param mixed $value
	 */
	public static function setSessionValue($idTest, $key, $value) {
		$sessionValue = Yii::app()->session[self::TEST_SESSION_PREFIX];
		if (isset($sessionValue[$idTest])) {
			$sessionValue[$idTest][$key] = $value;
			Yii::app()->session[self::TEST_SESSION_PREFIX] = $sessionValue;
		}
	}

	
	/**
	 * Unset a TEST session key
	 *  
	 * @param number $idTest
	 * @param mixed $key
	 */
	public static function unSetSessionValue($idTest, $key) {
		$sessionValue = Yii::app()->session[self::TEST_SESSION_PREFIX];
		if (is_array($sessionValue) && isset($sessionValue[$idTest])) {
			$newSessionValue = array();
			foreach ($sessionValue[$idTest] as $index => $content) {
				if ($key != $index) { $newSessionValue[$index] = $content; }
			}
			$sessionValue[$idTest] = $newSessionValue;
			Yii::app()->session[self::TEST_SESSION_PREFIX] = $sessionValue;
		}
	}
	
	
	/**
	 * Read a TEST session key value
	 *  
	 * @param number $idTest
	 * @param mixed $key
	 * @return mixed|null
	 */
	public static function readSessionValue($idTest, $key) {
		$sessionValue = Yii::app()->session[self::TEST_SESSION_PREFIX];
		return (isset($sessionValue[$idTest][$key]) ? $sessionValue[$idTest][$key] : NULL);
	}
	

	
	/**
	 * Rest tracking for given TEST and Track
	 * 
	 * @param number $idTest
	 * @param number $idTrack
	 */
	public static function resetTrack($testModel, $trackModel) {
		
		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }
		
		try {
			$idTest 	= $testModel->getPrimaryKey();
			$idTrack 	= $trackModel->getPrimaryKey();

			$questionsCriteria = new CDbCriteria();
			$questionsCriteria->join = "INNER JOIN ".LearningTestQuestRel::model()->tableName()." qr ON qr.id_question = idQuest";
			$questionsCriteria->addCondition('qr.id_test = :id_test');
			$questionsCriteria->params[':id_test'] = $idTest;
			$questions = LearningTestquest::model()->findAll($questionsCriteria);
			if (!empty($questions)) {
				foreach ($questions as $question) {
					$questionManager = CQuestionComponent::getQuestionManager($question->type_quest);
					if ($questionManager && method_exists($questionManager, 'deleteUserAnswer')) {
						$questionManager->deleteUserAnswer($idTrack, $question->getPrimaryKey());
					}
				}
			}
	
			$rs = LearningTesttrackPage::model()->deleteAllByAttributes(array('idTrack' => $idTrack));
			if ($rs === FALSE) { throw new CException('Error while resetting track info'); }
			
			$rs = LearningTesttrackQuest::model()->deleteAllByAttributes(array('idTrack' => $idTrack));
			if ($rs === FALSE) { throw new CException('Error while resetting track info'); }
	
			$newTrackInfo = array(
					'date_attempt' => Yii::app()->localtime->toLocalDateTime(),
					'date_end_attempt' => Yii::app()->localtime->toLocalDateTime(),
					'date_attempt_mod' => NULL,
					'last_page_seen' => 0,
					'last_page_saved' => 0,
					'score' => 0,
					'bonus_score' => 0,
					'score_status' => LearningTesttrack::STATUS_NOT_COMPLETE
			);
			$trackModel->setAttributes($newTrackInfo);
			$trackModel->save();
			
			if (isset($transaction)) { $transaction->commit(); }
		
		}
		catch (CException $e) {
			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}
	
	}
	
	
	/**
	 * Return an overall result about a test by track [id]
	 * 
	 * @param number $testModel
	 * @param number $trackModel
	 */
	public static function getTestOutcomeByTrack($testModel, $trackModel) {
		
		$table1 = LearningTestquest::model()->tableName();
		$table2 = LearningTesttrackQuest::model()->tableName();
		$table3 = LearningTestQuestRel::model()->tableName();
		$idTest = $testModel->getPrimaryKey();
		$idTrack = $trackModel->getPrimaryKey();
		
		$questions = Yii::app()->db->createCommand()
			->select("q.idQuest, q.type_quest, q.idCategory")
			->from($table1." q")
			->join($table3." qr", "qr.id_question = q.idQuest")
			->join($table2." t", "t.idQuest = q.idQuest")
			->where("qr.id_test = :id_test", array(':id_test' => $idTest))
			->andWhere("t.idTrack = :id_track", array(':id_track' => $idTrack))
			->andWhere("q.type_quest <> :type_1", array(':type_1' => LearningTestquest::QUESTION_TYPE_BREAK_PAGE))
			->andWhere("q.type_quest <> :type_2", array(':type_2' => LearningTestquest::QUESTION_TYPE_TITLE))
			->order("qr.sequence ASC")
			->queryAll();
		
		$totalUserScore = 0;
		$totalMaxScore = 0;
		$countManual = 0;
		$manualScore = 0;
		$categoriesScore = array();
		
		foreach ($questions as $question) {
		
			$questionManager = CQuestionComponent::getQuestionManager($question['type_quest']);
			$userScore = $questionManager->getUserScore($question['idQuest'], $idTrack);
			$maxScore = $questionManager->getMaxScore($question['idQuest']);
		
			if ($questionManager->getScoreType() == CQuestionComponent::SCORE_TYPE_MANUAL) {
				$countManual++;
				$manualScore = round($manualScore + $maxScore, 2);
			}
		
			$totalUserScore = round($totalUserScore + $userScore, 2);
			$totalMaxScore += $maxScore;
		
			$idCategory = (int)$question['idCategory'];
			if (!isset($categoriesScore[$idCategory])) {
				$categoriesScore[$idCategory] = round($userScore, 2);
			} else {
				$categoriesScore[$idCategory] = round($userScore + $categoriesScore[$idCategory], 2);
			}
		
		}
		
		if ($testModel->point_type == 1) {
			// Prevent division by zero if NO score points have been assigned to questions
			if (!($totalMaxScore >0)) {
				$totalUserScore = 0;
			}
			else {
				$totalUserScore = round(100 * $totalUserScore / $totalMaxScore);
			}
		}
		
		$saveScore = $totalUserScore;
		
		if ($totalUserScore >= $testModel->point_required) {
			$nextStatus = LearningTesttrack::STATUS_PASSED;
			$scoreStatus = LearningTesttrack::STATUS_PASSED;
		} else {
			$nextStatus = LearningTesttrack::STATUS_NOT_PASSED;
			$scoreStatus = LearningTesttrack::STATUS_NOT_PASSED;
		}
		
		if ($testModel->show_only_status) {
			if ($countManual > 0) {
				$scoreStatus = LearningTesttrack::STATUS_NOT_CHECKED;
			} else {
				$scoreStatus = LearningTesttrack::STATUS_VALID;
			}
		}
		
		$result = array(
			'totalUserScore'		=> $totalUserScore,
			'totalMaxScore' 		=> $totalMaxScore,
			'countManual' 			=> $countManual,
			'manualScore' 			=> $manualScore,
			'categoriesScore' 		=> $categoriesScore,
			'testFailed' 			=> ($nextStatus === LearningTesttrack::STATUS_NOT_PASSED),
			'scoreStatus'			=> $scoreStatus,	
			'nextStatus'			=> $nextStatus,	
		);
		
		return $result;
		
	} 
	

	/**
	 * Based on TEST outcome, do some final tracking. This is usualy called at "show TEST result" phase.
	 * 
	 * @param LearningTest $testModel
	 * @param LearningTesttrack $trackModel
	 * @param LearningOrganization $loModel
	 * @param array $testOutcome
	 * @param string $trackTimes
	 * @throws CException
	 */
	public static function doFinalizingTrack($testModel, $trackModel, $loModel, $testOutcome = false) {
		
		// Get TEST outcome if not passed
		if ($testOutcome === false) {
			$testOutcome = self::getTestOutcomeByTrack($testModel, $trackModel);
		}
		
		$idTest 	= $testModel->getPrimaryKey();
		$idTrack 	= $trackModel->getPrimaryKey();
		
		// Prepare an array of attributes to save into tracking tables
		$newTrackInfo = array();
		
		// TEST TRACK: TIMES  (describes/saves/tracks the outcome of a given TEST, taken/re-taken several times, if any
		switch ($testOutcome['scoreStatus']) {
			case LearningTesttrack::STATUS_VALID:
			case LearningTesttrack::STATUS_NOT_CHECKED:
			case LearningTesttrack::STATUS_PASSED:
			case LearningTesttrack::STATUS_NOT_PASSED: {
		
				$newTrackInfo['date_end_attempt'] = Yii::app()->localtime->toLocalDateTime();
				$newTrackInfo['number_of_save'] = $trackModel->number_of_save + 1;
				$newTrackInfo['score'] = $testOutcome['totalUserScore'];
				$newTrackInfo['score_status'] = $testOutcome['scoreStatus'];
				$newTrackInfo['number_of_attempt'] 	= $trackModel->number_of_attempt + 1;
		
				$utcTestDateBegin = TestUtils::readSessionValue($idTest, 'testDateBegin');
				$timeElapsed = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s')) - strtotime($utcTestDateBegin);
	
				$trackTime = new LearningTesttrackTimes();
				$trackTime->setAttributes(array(
						'idTrack' => $idTrack,
						'idReference' => $loModel->idOrg,
						'idTest' => $idTest,
						'date_attempt' => Yii::app()->localtime->toLocalDateTime(),
						'number_time' => $newTrackInfo['number_of_save'],
						'score' => $newTrackInfo['score'],
						'score_status' => $newTrackInfo['score_status'],
						'date_begin' => Yii::app()->localtime->toLocalDateTime($utcTestDateBegin),
						'date_end' => Yii::app()->localtime->toLocalDateTime(),
						'time' => $timeElapsed
				));
				
				if (!$trackTime->save()) {
					throw new CException('Error while saving track time data'.print_r($trackTime->getErrors(), TRUE));
				}
				TestUtils::unSetSessionValue($idTest, 'testDateBegin');
		
			} break;
		}
		
		
		// TEST TRACK: core tracking
		if ($testModel->use_suspension && $testModel->max_attempt > 0 && $testModel->suspension_num_hours > 0) {
			switch ($testOutcome['nextStatus']) {
				case LearningTesttrack::STATUS_NOT_PASSED: {
					$newTrackInfo['number_of_attempt'] = $trackModel->number_of_attempt + 1;
					if ($testModel->max_attempt > 0 && $testModel->suspension_num_hours > 0) {
						//should we reset learning_test.suspension_num_attempts ??
						//$newTrackInfo['number_of_attempt'] = 0; //from now on, it will use the suspended_until parameter, so only the date is needed, we can reset the attempts count
						$suspendDateUTC = date('Y-m-d H:i:s', strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s')) + $testModel->suspension_num_hours*3600);
						$newTrackInfo['suspended_until'] = Yii::app()->localtime->toLocalDateTime($suspendDateUTC);
					} else {
						//if max_attempt is <= 0, never update attempts counter, so user won't never be de-suspended
					}
				} break;
				case LearningTesttrack::STATUS_COMPLETED:
				case LearningTesttrack::STATUS_PASSED: {
					//$newTrackInfo['attempts_for_suspension'] = 0;
					$newTrackInfo['number_of_attempt'];
				} break;
			}
		}
		
		$trackModel->setAttributes($newTrackInfo); //TO DO: check commontrack status too, date_attempt and status
		if (!$trackModel->save()) {
			throw new CException('Error while updating tracking info');
		}
		
		
	}
	
	
}