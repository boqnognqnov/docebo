<?php

class QuestionInlineChoice extends CQuestionWithAnswers {


	public function __construct() {
		parent::__construct();
		$this->setType(LearningTestquest::QUESTION_TYPE_INLINE_CHOICE);
		$this->setEditProperty('category', true);
		$this->setEditProperty('difficulty', true);
		$this->setEditProperty('shuffle', true);
		$this->setEditProperty('answeringMaxTime', true);
		$this->setTransformation('title', array($this, 'transformTitle'));
	}



	public function getAnswerTag() {
		return '[answer]';
	}



	public function transformTitle($inputData, $other) {
		$questionManager = $other->questionManager;
		$idQuestion = (int)$other->idQuestion;
		$test = $this->_controller->getTest();
		$testId = null;
		if($test){
			$testId = $test->idTest;
		}
		$userAnswer = $questionManager->recoverUserAnswer($idQuestion, Yii::app()->user->id, $testId);
		$answerValue = (!empty($userAnswer) ? array_pop($userAnswer)->idAnswer : false);
		//retrieve all question answers
		$answersList = array();
		if(Settings::get('no_answer_in_test', 'off') == 'on')
			$answersList[0] = Yii::t('standard', '_NO_ANSWER');
		$answersData = $this->getAnswers($idQuestion);
		$test = $this->_controller->getTest();
		if (!empty($answersData)) {
			//checking shuffle option ...
			$question = LearningTestquest::model()->findByPk($idQuestion);

			$test_shuffle = false;
			if(is_object($test))
				$test_shuffle = $test->getAttribute('shuffle_answer') == 1;

			if ($question->shuffle || $test_shuffle) { shuffle($answersData); }
			//make answers list for dropdown
			foreach ($answersData as $answer) {
				$answersList[(int)$answer->getPrimaryKey()] = CHtml::decode(strip_tags($answer->answer));
			}
		}
		//check test settings
		$disabled = !$questionManager->canBeAnswered($idQuestion, Yii::app()->user->id, $test->idTest);

		// custom plugin set property disabled if test is in study mode
		$event = new DEvent($this, array('disabled' => $disabled));
		Yii::app()->event->raise('CheckForStudyMode', $event);
		if (!$event->shouldPerformAsDefault()) {
			$disabled = true;
			if ($answerValue == false) {
				$answerValue = 1;
			}
			foreach ($answersData as $answer) {
				if ($answer->is_correct == 1) {
					$answersList[(int)$answer->getPrimaryKey()] = strip_tags($answer->answer);
					break;
				}
			}
			$span = ' <span class="p-sprite check-solid-small-green" style="vertical-align: baseline"></span>';
		}
		//make dropdown
		$dropDown = CHtml::dropDownList(
			$questionManager->getInputName($idQuestion).'[]',
			$answerValue,
			$answersList,
			array(
				'id' => $questionManager->getInputId($idQuestion),
				'disabled' => ($answerValue > 0 ? $disabled : false)
			)
		);
		if (strpos($inputData, $questionManager->getAnswerTag()) === false) {
			return $inputData.'<br />'.$dropDown.$span;
		}
		return str_replace(
			$questionManager->getAnswerTag(),
			$dropDown,
			$inputData
		);
	}


	public function play($idQuestion) {
		//NOTE: see "transformTitle" method
	}


	public function edit($event) {
		$controller = $this->getController();
		$cs = Yii::app()->getClientScript();
		$cs->registerCoreScript('jquery.ui');
		$assetsUrl = $controller->getPlayerAssetsUrl();
		// $cs->registerCssFile($assetsUrl.'/css/player-sprite.css');  // moved to themes
		$cs->registerScriptFile($assetsUrl.'/js/testpoll.edit_question_choice.js');
		$controller->renderPartial('edit/_'.$this->getType(), array(
			'questionManager' => $this,
			'isEditing' => $event->params['isEditing'],
			'idQuestion' => $event->params['idQuestion']
		));
	}

}