<?php

class QuestionRaw {

	public $qtype = NULL;

	public $id_category = 0;
	public $prompt = false;
	public $quest_text = false;
	public $difficult = 3;
	public $time_assigned = 0;
	public $answers = array();
	public $extra_info = array();



	public function __construct() { }



	public function setCategoryFromName($categoryName) {

		$criteria = new CDbCriteria();
		$criteria->addCondition("author = 0 OR author = :author");
		$criteria->params[':author'] = Yii::app()->user->id;
		$rs = LearningQuestCategory::model()->findByAttributes(array('name' => $categoryName), $criteria);
		if (empty($rs)) {
			$this->id_category = 0;
		} else {
			$this->id_category = $rs->getPrimaryKey();
		}
	}

}