<?php

class QuestionExtendedText extends CQuestionComponent {
	
	
	public function __construct() {
		parent::__construct();
		$this->setType(LearningTestquest::QUESTION_TYPE_EXTENDED_TEXT);
		$this->setEditProperty('category', true);
		$this->setEditProperty('difficulty', true);
		$this->setEditProperty('shuffle', false);
		$this->setEditProperty('answeringMaxTime', true);
	}



	public function play($idQuestion) {
		$controller = $this->getController();
		$test = $this->_controller->getTest();
		$controller->renderPartial($this->getQuestionViewsPath().$this->getType(), array(
			'questionManager' => $this,
			'idQuestion' => $idQuestion,
			'idTest' => $test ? $test->idTest : null,
		));
	}



	public function edit($event) {
		$controller = $this->getController();
		if ($event->params['isEditing']) {
			//$answer = LearningTestquestanswer::model()->findByAttributes(array('idQuest' => $event->params['idQuestion']));
			$answer = $this->getAnswer($event->params['idQuestion']);
			if (!$answer) { throw new CException('Invalid question answer'); }
			$maxScore = $answer->score_correct;
		}
		if (!isset($maxScore)) { $maxScore = 0.0; }
		$controller->renderPartial('edit/_'.$this->getType(), array(
			'questionManager' => $this,
			'isEditing' => $event->params['isEditing'],
			'idQuestion' => $event->params['idQuestion'],
			'maxScore' => $maxScore
		));
	}



	public function create($event) {
		
		$rq = Yii::app()->request;
		
		$question = $event->params['question'];
		$maxScore = $rq->getParam('max_score', array());

		$ar = new LearningTestquestanswer();
		$ar->idQuest = $question->getPrimaryKey();
		$ar->is_correct = 1;
		$ar->answer = '';
		$ar->comment = '';
		$ar->sequence = 1;
		$ar->score_correct = (double)$maxScore;
		$ar->score_incorrect = 0.0;
		
		if (!$ar->save()) {
			throw new CException('Error while saving answer');
		}
		
	}

	public function update($event) {

		$rq = Yii::app()->request;
		
		$question = $event->params['question'];
		$maxScore = $rq->getParam('max_score', array());

		$ar = $this->getAnswer($question->getPrimaryKey());//LearningTestquestanswer::model()->findByPk($answer['id_answer']);
		if (!$ar || $ar->idQuest != $question->getPrimaryKey()) {
			throw new CException('Invalid answer');
		}
		$ar->is_correct = 1;
		$ar->answer = '';
		$ar->comment = '';
		$ar->sequence = 1;
		$ar->score_correct = (double)$maxScore;
		$ar->score_incorrect = 0.0;

		if (!$ar->save()) {
			throw new CException('Error while saving answer');
		}
		
	}
	
	public function delete($event) {
		$question = $event->params['question'];
		$rs = LearningTestquestanswer::model()->deleteAllByAttributes(array('idQuest' => $question->getPrimaryKey()));
		if ($rs === false) {
			throw new CException('Error while deleting answer');
		}
	}



	public function getAnswer($idQuestion) {
		$answers = LearningTestquestanswer::model()->findAllByAttributes(array('idQuest' => $idQuestion));
		return (!empty($answers) ? $answers[0] : false);
	}



	public function userDoAnswer($idTrack, $idQuestion) {
		$info = LearningTesttrackAnswer::model()->findByAttributes(array('idTrack' => $idTrack, 'idQuest' => $idQuestion));
		if (!empty($info) && empty($info->more_info)) { return false; }
		return (!empty($info));
	}



	public function storeUserAnswer($idTrack, $idQuestion, $answersInput, $canOverwrite) {
		$result = true;
		
		if (!is_array($answersInput)) {
			$answersInput = $this->readAnswerInput($idQuestion);
		} else {
			$answersInput = implode('', $answersInput);
		}

		if ($this->userDoAnswer($idTrack, $idQuestion)) {
			if ($canOverwrite) {
				if (!$this->deleteUserAnswer($idTrack, $idQuestion)) return false;
			}
		}

		//handle empty answer case:
		//if empty text answer exists in DB, then consider the question as unanswered anyway and allow to update existing value
		$existing = LearningTesttrackAnswer::model()->findByAttributes(array('idTrack' => $idTrack, 'idQuest' => $idQuestion));

		if ($answersInput !== NULL) {

			$answerText = Yii::app()->htmlpurifier->purify(trim($answersInput));

			if (!empty($existing)) {
				if (!$canOverwrite && !empty($existing->more_info)) { return $result; } //cannot overwrite a real existing answer
				$ar = $existing;
			} else {
				$ar = new LearningTesttrackAnswer();
				$ar->idTrack = $idTrack;
				$ar->idQuest = $idQuestion;
			}
			$ar->idAnswer = 0;
			$ar->score_assigned = 0;
			$ar->more_info = $answerText;
			$ar->user_answer = (!empty($answerText) ? 1 : 0);
			
			if (!$ar->save()) {
				throw new CException('Error while storing user answer');
			}
		}
	}


	public function getScoreType() {
		return self::SCORE_TYPE_MANUAL;
	}

	public function importFromRaw($rawQuestion, $idTest, $sequence = false, $page = false) {

		if (Yii::app()->db->getCurrentTransaction() === NULL) { $transaction = Yii::app()->db->beginTransaction(); }

		try {

			if (!$idTest || $idTest <= 0) { $idTest = 0; }
			if (!$sequence) { $sequence = 999999; }
			if (!$page) { $page = 1; }

			//insert question in DB
			$question = new LearningTestquest();
			$question->idTest = (int)$idTest;
			$question->idCategory = self::getCategoryName($question->idCategory);
			$question->type_quest = $rawQuestion->qtype;
			$question->title_quest = $rawQuestion->quest_text;
			$question->difficult = $rawQuestion->difficult;
			$question->time_assigned = $rawQuestion->time_assigned;
			$question->sequence = $sequence;
			$question->page = $page;

			if (!$question->save()) { throw new CException('Error while saving question'); }

			//insert answer in DB
			$answer = new LearningTestquestanswer();
			$answer->idQuest = $question->getPrimaryKey();
			$answer->sequence = 1;
			$answer->is_correct = 1;

			if (!$answer->save()) { throw new CException('Error while saving question answers'); }

			if (isset($transaction)) { $transaction->commit(); }

			Yii::app()->event->raise('QuestionRawImportAfterSave', new DEvent($this, array(
				'question' => $question
			)));

			return true;

		} catch (CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			return false;
		}
	}

}