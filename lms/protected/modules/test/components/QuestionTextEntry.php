<?php

class QuestionTextEntry extends CQuestionComponent {


	public function __construct() {
		parent::__construct();
		$this->setType(LearningTestquest::QUESTION_TYPE_TEXT_ENTRY);
		$this->setEditProperty('category', true);
		$this->setEditProperty('difficulty', true);
		$this->setEditProperty('shuffle', false);
		$this->setEditProperty('answeringMaxTime', true);
		$this->setTransformation('title', array($this, 'transformTitle'));
	}



	public function getAnswerTag() {
		return '[answer]';
	}


	public function transformTitle($inputData, $other) {
		$questionManager = $other->questionManager;
		$idQuestion = (int)$other->idQuestion;
		$test = $this->_controller->getTest();
		$testId = null;
		if($test){
			$testId = $test->idTest;
		}
		$userAnswer = $questionManager->recoverUserAnswer($idQuestion, Yii::app()->user->id, $testId);
		$answerValue = (!empty($userAnswer) ? array_pop($userAnswer)->more_info : "");
		//check test settings
		$test = $this->_controller->getTest();
		$disabled = !$questionManager->canBeAnswered($idQuestion, Yii::app()->user->id, $test->idTest);

		// custom plugin set property disabled if test is in study mode
		$event = new DEvent($this, array('disabled' => $disabled));
		Yii::app()->event->raise('CheckForStudyMode', $event);
		if (!$event->shouldPerformAsDefault()) {
			$disabled = true;
			$span = ' <span class="p-sprite check-solid-small-green" style="vertical-align: baseline"></span>';
			$answerValue = Yii::app()->db->createCommand()->
				select('answer')->
				from('learning_testquestanswer')->
				where('idQuest=:idQuestion',array(':idQuestion'=>$idQuestion))->andWhere('is_correct=1')->queryRow();
			$answerValue = strip_tags($answerValue['answer']);
		}

		//render input field
		$textField = CHtml::textField(
			$questionManager->getInputName($idQuestion),
			$answerValue,
			array(
				'id' => $questionManager->getInputId($idQuestion),
				'class' => 'mobile-entry',
				'placeholder' => "???",
				'disabled' => (!empty($answerValue) ? $disabled : false) //consider the question as "not answered" if an empty text is present in DB
			)
		);
		if (strpos($inputData, $questionManager->getAnswerTag()) === false) {
			return $inputData.'<br />'.$textField.$span;
		}
		return str_replace(
			$questionManager->getAnswerTag(),
			$textField,
			$inputData
		);
	}


	public function play($idQuestion) {
		//NOTE: see "transformTitle" method
	}


	public function edit($event) {
		$controller = $this->getController();
		$controller->renderPartial('edit/_'.$this->getType(), array(
			'questionManager' => $this,
			'isEditing' => $event->params['isEditing'],
			'idQuestion' => $event->params['idQuestion']
		));
	}


	public function create($event) {

		$rq = Yii::app()->request;

		$question = $event->params['question'];
		$answer = $rq->getParam('answer', array());

		$ar = new LearningTestquestanswer();
		$ar->idQuest = $question->getPrimaryKey();
		$ar->is_correct = 1;
		$ar->answer = $answer['answer'];



		$ar->comment = Yii::app()->htmlpurifier->purify($answer['comment']);
		$ar->sequence = 1;
		$ar->score_correct = (double)$answer['score_correct'];
		$ar->score_incorrect = (double)$answer['score_incorrect'];

		if (!$ar->save()) {
			throw new CException('Error while saving answer');
		}

	}

	public function update($event) {

		$rq = Yii::app()->request;

		$question = $event->params['question'];
		$answer = $rq->getParam('answer', array());

		$ar = LearningTestquestanswer::model()->findByPk($answer['id_answer']);
		if (!$ar || $ar->idQuest != $question->getPrimaryKey()) {
			throw new CException('Invalid answer');
		}
		$ar->is_correct = 1;
		$ar->answer = $answer['answer'];
		$ar->comment = Yii::app()->htmlpurifier->purify($answer['comment']);
		$ar->sequence = 1;
		$ar->score_correct = (double)$answer['score_correct'];
		$ar->score_incorrect = (double)$answer['score_incorrect'];

		if (!$ar->save()) {
			throw new CException('Error while saving answer');
		}

	}

	public function delete($event) {
		$question = $event->params['question'];
		$rs = LearningTestquestanswer::model()->deleteAllByAttributes(array('idQuest' => $question->getPrimaryKey()));
		if ($rs === false) {
			throw new CException('Error while deleting answer');
		}
	}



	public function getAnswer($idQuestion) {
		$answers = LearningTestquestanswer::model()->findAllByAttributes(array('idQuest' => $idQuestion));
		if (empty($answers)) {
			// If we arrive here then it means that the text entry question has not an answer set in database.
			// Since that may lead to issues, then create a new row with an empty answer in database.
			$emptyAnswer = new LearningTestquestanswer();
			$emptyAnswer->idQuest = $idQuestion;
			$emptyAnswer->sequence = 1;
			$emptyAnswer->is_correct = 1;
			$emptyAnswer->answer = "";
			$emptyAnswer->comment = "";
			$emptyAnswer->score_correct = 0;
			$emptyAnswer->score_incorrect = 0;
			$emptyAnswer->save();
			return $emptyAnswer;
		}
		return (!empty($answers) ? $answers[0] : false);
	}



	public function storeUserAnswer($idTrack, $idQuestion, $answersInput, $canOverwrite) {

		if (!is_array($answersInput)) {
			$answersInput = $this->readAnswerInput($idQuestion);
		} else {
			$answersInput = implode('', $answersInput);
		}

		if ($this->userDoAnswer($idTrack, $idQuestion)) {
			if ($canOverwrite) {
				if (!$this->deleteUserAnswer($idTrack, $idQuestion)) return false;
			}
		}

		if ($answersInput !== NULL) {

			$answersInput = trim($answersInput);

			$questionAnswer = $this->getAnswer($idQuestion);
			if (!$questionAnswer) {
				throw new CException('Invalid answer');
			}

			//handle empty answer case:
			//if empty text answer exists in DB, then consider the question as unanswered anyway and allow to update existing value
			$existing = LearningTesttrackAnswer::model()->findByAttributes(array('idTrack' => $idTrack, 'idQuest' => $idQuestion, 'idAnswer' => $questionAnswer->getPrimaryKey()));

			$check1 = trim(strtolower(strip_tags($questionAnswer->answer)));
			$check2 = trim(strtolower(strip_tags($answersInput)));
			$isCorrect = (strcmp($check1, $check2) == 0);

			if ($existing) {
				if (!$canOverwrite && !empty($existing->more_info)) { return true; } //can't overwrite existing non-empty text answer
				$ar = $existing;
			} else {
				$ar = new LearningTesttrackAnswer();
				$ar->idTrack = $idTrack;
				$ar->idQuest = $idQuestion;
				$ar->idAnswer = $questionAnswer->getPrimaryKey();
			}
			$ar->score_assigned = ($isCorrect ? $questionAnswer->score_correct : -$questionAnswer->score_incorrect);
			$ar->more_info = $answersInput;
			$ar->user_answer = (!empty($answersInput) ? 1 : 0);

			if (!$ar->save()) {
				throw new CException('Error while storing user answer');
			}
		}

		return true;
	}
}