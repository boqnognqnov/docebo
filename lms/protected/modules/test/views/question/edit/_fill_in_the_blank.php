<?php
$numbersPattern = '\d+\.?\d*';
?>


<br>
<div class="block no-padding">
	<div class="block-header">
		<?=Yii::t('test', '_TEST_ANSWER');?>
		<div class="FITB-info">
			<?=Yii::t('test', 'Copy and paste the answers shortcode in the question body text to allow users to input their answers');?>
		</div>
	</div>
	<div class="block-content fitb-block-content">
		<div id="no-answers-placeholder">
			<span>(<?php echo Yii::t('standard', '_NO_ANSWER'); ?>)</span>
		</div>
		<div id="answers-table-container">
			<table id="answers-table" class="table">
				<tbody id="answers-tbody">
				</tbody>
			</table>
		</div>
	</div>
</div>


<div class="block no-padding add_new_answer_button">

	<div class="block-content fitb-block-content">
		<!--<div id="add-answer-block-header">-->
		<div id="add-answer-link-container">
			<a href="#" id="toggle-add-answer">
				<span class="i-sprite is-plus-solid green"></span>&nbsp;<strong><?=Yii::t('poll', '_POLL_ADD_ONE_ANSWER');?></strong>
			</a>
		</div>
		<!--</div>-->
		<!--<div id="edit-answer-block-header" style="display:none;">
			<?=Yii::t('standard', '_MOD');?>
		</div>-->
	</div>
</div>


<div class="block no-padding" id="form-box-container">

	<div class="block-header" id="add-answer-block-header">
		<a href="#" id="toggle-add-answer"><?=Yii::t('poll', '_POLL_ADD_ONE_ANSWER');?></a>
	</div>
	<div class="block-header" id="edit-answer-block-header" style="display:none;">
		<?=Yii::t('standard', '_MOD');?>
	</div>

	<div class="block-content fitb-block-content">

		<!--<div>-->
		<div class="block-hr"></div>
		<div class="block odd FITB">
			<div class="block-contents">
				<div class="row-fluid">
					<div class="span12">
						<?= CHtml::label(Yii::t('test', 'Correct answers'), 'new_correct_answers') ?>
						<div id="selected-sorting-fields">
							<select name="selected_sorting_fields" multiple="multiple" id="new_correct_answers">
							</select>
						</div>
					</div>
				</div>

				<div class="row-fluid answers_are_case_sensitive">
					<div class="span12">
						<?= CHtml::checkBox('answers_are_case_sensitive', false, array('class' => 'case_sensitive' ))?>
						<?= CHtml::label(Yii::t('test', 'Answers are case sensitive'), 'answers_are_case_sensitive')?>
					</div>
				</div>

				<div class="row-fluid scores">
					<div class="span12">
						<div class="span1">
							<?= CHtml::label(Yii::t('standard', '_SCORE') . ':' , false)?>
						</div>
						<div class="span2 correct_score">
							<?php
							$textField = CHtml::textField('new-answer-score_correct', '0', array('id' => 'new-answer-score_correct', 'class'=>'span5', 'pattern' => $numbersPattern));
							echo CHtml::label(Yii::t('test', '_TEST_IFCORRECT').'&nbsp;+&nbsp;'.$textField, 'new-answer-score_correct');
							?>
						</div>
						<div class="span2 incorrect_score">
							<?php
							$textField = CHtml::textField('new-answer-score_incorrect', '0', array('id' => 'new-answer-score_incorrect', 'class'=>'span5', 'pattern' => $numbersPattern));
							echo CHtml::label(Yii::t('test', '_TEST_IFINCORRECT').'&nbsp;-&nbsp;'.$textField, 'new-answer-score_incorrect');
							?>
						</div>
					</div>
				</div>

				<div class="row-fluid">
					<div class="span12">
						<?= CHtml::label(Yii::t('standard', '_COMMENTS'), 'new-answer-comment') ?>
						<?= CHtml::textArea(false, '', array('id' => 'new-answer-comment', 'class'=>'input-block-level')) ?>
					</div>
				</div>

				<br>
				<div class="text-right" id="add-answer-buttons">
					<input type="button" id="add-answer-button" class="btn-docebo green big" value="<?=Yii::t('standard', '_ADD');?>" />
					<input type="button" id="undo-add-answer-button" class="btn-docebo black big" value="<?=Yii::t('standard', '_CLOSE');?>" />
				</div>
				<div class="text-right" id="mod-answer-buttons" style="display:none">
					<input type="button" id="mod-answer-button" class="btn-docebo green big" value="<?=Yii::t('standard', '_SAVE');?>" />
					<input type="button" id="undo-edit-answer-button" class="btn-docebo black big" value="<?=Yii::t('standard', '_CLOSE');?>" />
				</div>
			</div>
		</div>
		<div class="block-hr"></div>
		<!--</div>-->

	</div>
</div>

<style>
	table#answers-table tr th, table#answers-table tr td{
		text-align: center;
	}

	table#answers-table tr.answers-header th{
		color: black;
		font-size: 11px;
	}
</style>

<script type="text/javascript">

	//var numbersPattern = <?='/'.$numbersPattern.'/'?>;

	var InitialAnswers = [];
	<?php

    //$answers = ($isEditing ? $questionManager->_getAnswersList($idQuestion) : array());
    $answers = ($isEditing ? $questionManager->getAnswers($idQuestion) : array());

    if (!empty($answers)):
        foreach ($answers as $answer):
            $toJavascript = array(
                'sequence' => $answer['sequence'],
                'case_sensitive' => $answer['settings']['case_sensitive'],
                'answers' => explode(",", Docebo::nbspToSpace($answer['answer'])),//preg_replace('/[^(\x20-\x7F)]*/','', Docebo::nbspToSpace($answer['answer'])),
                // NOTE: the above regex was breaking the inserted text, removing characters such as "à", "è" etc.
                'comment' => $answer['comment'],
                'scoreCorrect' => $answer['score_correct'],
                'scoreIncorrect' => $answer['score_incorrect']
            );
			if (isset($answer['idAnswer']) && $answer['idAnswer'] > 0) {
				$toJavascript['id_answer'] = $answer['idAnswer'];
			}
    ?>
	InitialAnswers.push(<?=CJavaScript::encode($toJavascript)?>);
	<?php
        endforeach;
    endif;
    ?>

	$(document).ready(function(e) {

		$('#answers_are_case_sensitive').styler();

		var selectElem = $('select#new_correct_answers');

		selectElem.fcbkcomplete({
			width: '98.5%',
			addontab: false, // <==buggy
			newel: true,
			cache: false,
			complete_text: '',
			input_name: 'maininput-name',
			filter_selected: true,
			maxitems:35
		});

		AnswersManager.init({
			numberPattern: <?php echo '/'.$numbersPattern.'/'?>,
			answerHtmlEditor: true,
			singleCorrectAnswer: true,
			answers: InitialAnswers,
			modality: 'full'
		});

	});

</script>

<style>

	span.make-copy{
		background-position: -49px -273px
	}
	table thead th, table tbody td{
		cursor: default;
	}
	table tbody td.rowIndex{
		cursor: pointer;
	}
	div.facebook-auto{
		display: none !important;
	}

	table tbody td.rowIndex > p{
		border-bottom: solid 1px;
		display: inline;
	}
</style>