<?php

$this->renderPartial('edit/templates/_template_choice', array(
	'questionType' => LearningTestquest::QUESTION_TYPE_INLINE_CHOICE,
	'singleCorrectAnswer' => true,
	'questionManager' => $questionManager,
	'isEditing' => $isEditing,
	'idQuestion' => $idQuestion,
	'answerHtmlEditor' => true,
	'modality' => 'full'
));

?>