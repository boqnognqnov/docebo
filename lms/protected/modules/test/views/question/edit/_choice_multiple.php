<?php

$this->renderPartial('edit/templates/_template_choice', array(
	'questionType' => LearningTestquest::QUESTION_TYPE_CHOICE_MULTIPLE,
	'singleCorrectAnswer' => false,
	'questionManager' => $questionManager,
	'isEditing' => $isEditing,
	'idQuestion' => $idQuestion,
	'answerHtmlEditor' => true,
	'modality' => 'full'
));

?>