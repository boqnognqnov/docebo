<?php

$elements_a = array();
$elements_b = array();
$b_map = array();
$i = 0;
$j = 0;

foreach($associations as $element_b) {
	$elements_b[$j] = array(
		'index' 	=> $j,
		'b_id' 		=> $element_b->idAnswer,
		'value' 	=> $element_b->answer
	);
	$b_map[$element_b->idAnswer] = $j;
	$j++;
}

foreach($answers as $element_a) {

	$elements_a[$i] = array(
		'index' 			=> $i,
		'a_id' 				=> $element_a->idAnswer,
		'value' 			=> $element_a->answer,
		'comment' 			=> $element_a->comment,
		'score_correct' 	=> $element_a->score_correct,
		'score_incorrect' 	=> $element_a->score_incorrect,
		'is_correct_index' 	=> ( isset($b_map[$element_a->is_correct]) ? $b_map[$element_a->is_correct] : 0 )
	);
	if (!isset($elements_b[$i])) {
		$elements_b[$i] = array(
			'index' 	=> $j,
			'b_id' 		=> 0,
			'value' 	=> ''
		);
	}
	$i++;
}
?>
<div id="tabs">
	<!-- First associate step -->
	<div id="tab-elements">

		<!-- Elements -->
		<div class="block no-padding">
			<div class="block-header"><?= Yii::t('test', '_TEST_ANSWER'); ?></div>
			<div class="block-content">
				<table id="elements-table" class="table">
					<thead>
						<tr>
							<th><?php echo Yii::t('test', '_TEST_QUEST_ELEMENTS_A'); ?></th>
							<th><?php echo Yii::t('test', '_TEST_QUEST_ELEMENTS_B'); ?></th>
							<th></th>
						</tr>
					</thead>
					<tbody id="elements-tbody"></tbody>
				</table>
			</div>
		</div>

		<!-- buttons -->
		<div class="block no-padding" id="form-box-container">
			<div class="block-header"><?=Yii::t('standard', '_ADD');?></div>
			<div class="block-content">

				<div class="block-hr"></div>
				<div class="block odd">

					<div class="row-fluid">
						<div class="span6">
							<?= CHtml::label(Yii::t('test', '_TEST_QUEST_ELEMENTS_A'), 'new-element-a') ?>
							<?= CHtml::textArea(false, '', array('id' => 'new-element-a', 'class'=>'input-block-level')) ?>
						</div>
						<div class="span6">
							<?= CHtml::label(Yii::t('test', '_TEST_QUEST_ELEMENTS_B'), 'new-element-b') ?>
							<?= CHtml::textArea(false, '', array('id' => 'new-element-b', 'class'=>'input-block-level')) ?>
						</div>
					</div>

					<div id="add-elements" class="text-right">
						<input type="button" id="add-element-button" class="span2 btn-docebo green big" value="<?=Yii::t('standard', '_ADD')?>" />&nbsp;
						<input type="button" id="go-to-associations" class="span2 btn-docebo green big" value="<?=Yii::t('standard', '_NEXT')?>" />
					</div>
					<div id="mod-elements" class="text-right">
						<input type="button" id="mod-element-button" class="span2 btn-docebo green big" value="<?=Yii::t('standard', '_SAVE')?>" />&nbsp;
						<input type="button" id="undo-element-button" class="span2 btn-docebo black big" value="<?=Yii::t('standard', '_UNDO')?>" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Second associate step -->
	<div id="tab-association">

		<!-- Associations -->
		<div class="block no-padding">
			<div class="block-header"><?=Yii::t('test', '_QUEST_ASSOCIATE');?></div>
			<div class="block-content">
				<table id="associations-table" class="table">
					<thead id="association-thead">
						<tr>
							<!-- <th>&nbsp;</th> -->
							<th><?php echo Yii::t('test', '_TEST_QUEST_ELEMENTS_A'); ?></th>
							<th><?php echo Yii::t('test', '_TEST_QUEST_ELEMENTS_B'); ?></th>
							<th><?php echo Yii::t('standard', '_COMMENTS'); ?></th>
							<th colspan="2"><?php echo Yii::t('test', '_TEST_IFCORRECT'); ?></th>
							<th colspan="2"><?php echo Yii::t('test', '_TEST_IFINCORRECT'); ?></th>
						</tr>
					</thead>
					<tbody id="associations-tbody"></tbody>
				</table>
				<div id="elements_b_hidden"></div>
			</div>
		</div>

		<!-- buttons -->
		<div class="text-right">
			<input type="button" id="go-to-elements" class="span2 btn-docebo green big" value="<?=Yii::t('standard', '_BACK')?>" />
		</div>
	</div>
</div>
<script type="text/javascript">

var Association = function( config ) {

	this.elements_a = config.elements_a || [];
	this.elements_b = config.elements_b || [];

	this.init();
};
Association.prototype = {

	elements_a: [],
	elements_b: [],

	add_quest_buttons: false,

	edit_quest_buttons: false,

	tab_elements: false,

	tab_association: false,

	current_edit_index: false,

	init: function() {

		this.tab_elements = $('#tab-elements');
		this.tab_association = $('#tab-association');
		this.final_form_buttons = $("form[id^='question-']").find('.form-actions');

		$('#mod-elements').hide();
		this.tab_association.hide();
		this.final_form_buttons.hide();

		$('#go-to-associations').on('click', $.proxy(function(e) {
			// step 1 listeners
			this.tab_elements.hide();
			this.tab_association.show();
			this.final_form_buttons.show();
			this.generateSelectTable();
		}, this ));

		$('#go-to-elements').on('click', $.proxy(function(e) {
			// step 2 listeners
			this.tab_elements.show();
			this.tab_association.hide();
			this.final_form_buttons.hide();
			this.updateElements();
			this.generateElementsTable();
		}, this ));

		$('#add-element-button').on('click', $.proxy(this.addElements, this ));
		$('#mod-element-button').on('click', $.proxy(this.modElements, this ));
		$('#undo-element-button').on('click', $.proxy(this.undoElements, this ));

		this.generateElementsTable();
	},

	addElements: function(e) {

		var element_a = $('#new-element-a'),
			element_b = $('#new-element-b');

		if (element_a.val() == '' || element_b.val() == '') return;

		try{
			new_index = this.elements_a.last().index + 1;
		} catch(ev){
			new_index = 0;
		}
		this.elements_a.push({
			index: new_index,
			a_id: 0,
			value: $("<div/>").text(element_a.val()).html(),
			comment: "",
			score_correct: "0",
			score_incorrect: "0",
			is_correct_index: 0
		});

		try{
			new_index = this.elements_b.last().index + 1;
		} catch(ev){
			new_index = 0;
		}
		this.elements_b.push({
			index: new_index,
			b_id: 0,
			value: $("<div/>").text(element_b.val()).html()
		});

		this.generateElementsTable();
		element_a.val('');
		element_b.val('');
	},

	modifyElement: function(e) {
		e.preventDefault();
		var a = $(e.currentTarget),
			edit_index = a.attr('id').replace('edit_answer_', '');

		this.current_edit_index = edit_index;

		$('#new-element-a').val(this.elements_a[edit_index].value);
		$('#new-element-b').val(this.elements_b[edit_index].value);

		$('#add-elements').hide();
		$('#mod-elements').show();
	},

	modElements: function(e) {
		var element_a = $('#new-element-a'),
			element_b = $('#new-element-b');

		if (element_a.val() == '' || element_b.val() == '' ) return;

		this.elements_a[this.current_edit_index].value = element_a.val();
		this.elements_b[this.current_edit_index].value = element_b.val();

		this.current_edit_index = false;

		$('#new-element-a').val('');
		$('#new-element-b').val('');

		$('#add-elements').show();
		$('#mod-elements').hide();

		this.generateElementsTable();
	},

	undoElements: function(e) {
		this.current_edit_index = false;

		$('#new-element-a').val('');
		$('#new-element-b').val('');

		$('#add-elements').show();
		$('#mod-elements').hide();
	},

	askForDeleteElement: function(e) {

		e.preventDefault();
		var a = $(e.currentTarget),
			del_index = a.attr('id').replace('del_answer_', '');

		bootbox.dialog(Yii.t('standard', '_AREYOUSURE'), [{
			label: Yii.t('standard', '_CONFIRM'),
			'class': 'btn-docebo big green',
			callback: $.proxy(this.deleteElement, this, del_index)
		}, {
			label: Yii.t('standard', '_CANCEL'),
			'class': 'btn-docebo big black',
			callback: function() { }
		}],
		{
			header: Yii.t('standard', '_DEL')
		});
	},

	deleteElement: function(del_index) {
		var new_elements_a = [],
			new_a_index = 0,

			new_elements_b = [],
			new_b_index = 0;

		for(var i=0;i < this.elements_a.length;i++) {

			if (i != del_index) {
				new_elements_a[new_a_index] = this.elements_a[i];
				new_elements_a[new_a_index].index = new_a_index;
				if (new_elements_a[new_a_index].is_correct_index == del_index) {

					new_elements_a[new_a_index].is_correct_index = 0;
				} else if (new_elements_a[new_a_index].is_correct_index > del_index) {

					new_elements_a[new_a_index].is_correct_index--;
				}
				new_a_index++;
			}
		}

		for(var j=0;j < this.elements_b.length;j++) {

			if (j != del_index) {
				new_elements_b[new_b_index] = this.elements_b[j];
				new_elements_b[new_b_index].index = new_b_index;
				new_b_index++;
			}
		}

		this.elements_a = new_elements_a;
		this.elements_b = new_elements_b;

		this.generateElementsTable();
	},

	generateElementsTable: function() {

		// get the table body
		var tbody = $('#elements-tbody');
		tbody.html('');

		// now we can add all the needed row
		for(var i=0;i < this.elements_a.length;i++) {

			tbody.append('<tr></tr>');
			var table_row = tbody.find("tr:last");
			table_row.append([
				'<td>' + this.elements_a[i].value + '</td>',
				( this.elements_b[i] != undefined
					? '<td>' + this.elements_b[i].value + '</td>'
					: '<td></td>' ),
				'<td class="img-cell">'
					+'<a href="#" id="edit_answer_' + this.elements_a[i].index + '">'
					+'<span class="objlist-sprite p-sprite edit-black"></span>'
					+'</a>'
				+'</td>',
				'<td class="img-cell">'
					+'<a href="#" id="del_answer_' + this.elements_a[i].index + '">'
					+'<span class="objlist-sprite p-sprite cross-small-red"></span>'
					+'</a>'
				+'</td>'
			]);
		}

		$("a[id^='edit_answer_']").on('click', $.proxy(this.modifyElement, this));
		$("a[id^='del_answer_']").on('click', $.proxy(this.askForDeleteElement, this));
	},

	generateSelectTable: function() {
		// get the table body
		var tbody = $('#associations-tbody'),
			elements_b_hidden = $('#elements_b_hidden');

		tbody.html('');
		elements_b_hidden.html('');

		// now we can add all the needed row
		for(var i=0;i < this.elements_a.length;i++) {

			var base_id = 'elements_a_' + this.elements_a[i].index,
				base_name = 'elements_a[' + this.elements_a[i].index + ']';

			var hidden = '<input type="hidden" id="' + base_id + 'answer" name="' + base_name + '[index]" value="' + this.elements_a[i].index + '" />'
				+ '<input type="hidden" id="' + base_id + 'answer" name="' + base_name + '[a_id]" value="' + this.elements_a[i].a_id + '" />'
				+ '<input type="hidden" id="' + base_id + 'answer" name="' + base_name + '[value]" value="' + $("<div/>").text(this.elements_a[i].value).html().replace(/"/g, "&quot;") + '" />';

			tbody.append('<tr></tr>');
			var table_row = tbody.find("tr:last");
			table_row.append([
				//'<td><span class="p-sprite drag-small-black"></span></td>',
				'<td>' + this.elements_a[i].value + hidden + '</td>',
				'<td>' + '<select class="input-medium" id="' + base_id + '_is_correct" name="' + base_name + '[is_correct_index]"></select>' + '</td>',
				'<td><label><input type="text" id="' + base_id + '_comment" name="' + base_name + '[comment]" value="' + (this.elements_a[i].comment ? this.elements_a[i].comment : '') + '"/></label></td>',
				'<td>+</td>',
				'<td><input type="text" class="input-mini" id="' + base_id + '_score_correct" name="' + base_name + '[score_correct]" value="' + (this.elements_a[i].score_correct ? this.elements_a[i].score_correct : '') + '" /></td>',
				'<td>-</td>',
				'<td><input type="text" class="input-mini" id="' + base_id + '_score_incorrect" name="' + base_name + '[score_incorrect]" value="' + (this.elements_a[i].score_incorrect ? this.elements_a[i].score_incorrect : '') + '" /></td>',
			]);

			var select = $('#elements_a_' + this.elements_a[i].index + '_is_correct');
			for(var j=0;j < this.elements_b.length;j++) {

				var option = $('<option>', {
					value: this.elements_b[j].index,
					text: $("<div/>").html(this.elements_b[j].value).text(),
					selected: (this.elements_a[i].is_correct_index == this.elements_b[j].index ? true : false)
				});
				select.append(option);
			}
		}
		for(var j=0;j < this.elements_b.length;j++) {

			var option_v = $('<input>', {
				type: 'hidden',
				id: 'elements_b_' + this.elements_b[j].index + '_value',
				name: 'elements_b[' + this.elements_b[j].index + '][value]',
				value: this.elements_b[j].value
			});
			var option_id = $('<input>', {
				type: 'hidden',
				id: 'elements_b_' + this.elements_b[j].index + '_b_id',
				name: 'elements_b[' + this.elements_b[j].index + '][b_id]',
				value: this.elements_b[j].b_id
			});
			elements_b_hidden.append([option_v, option_id]);
		}
	},

	updateElements: function(e) {

		var rows = $('#associations-tbody').find('tr');

		for(var i=0;i < rows.length;i++) {

			var select = $(rows[i]).find('select');
			var new_index = select.val(),
				base_id = select.attr('id').replace('_is_correct', ''),
				a_index = base_id.replace('elements_a_', '');

			// alert(select_id);
			var a = this.elements_a[a_index];
			a.comment 			= $('#' + base_id + '_comment').val();
			a.score_correct 	= $('#' + base_id + '_score_correct').val();
			a.score_incorrect	= $('#' + base_id + '_score_incorrect').val();
			a.is_correct_index	= new_index;
		}
	}
}

var oAssociation = false;
$(function(){
	// Let's configure the visible parts
	oAssociation = new Association({
		elements_a: <?= CJavaScript::encode($elements_a); ?>,
		elements_b: <?= CJavaScript::encode($elements_b); ?>
	});
	oAssociation.init();
});
</script>