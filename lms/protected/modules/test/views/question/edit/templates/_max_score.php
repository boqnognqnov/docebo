<div class="block">
	<div class="block-header"><?= CHtml::label(Yii::t('standard', '_MAX_SCORE'), 'answer-max-score') ?></div>
	<div class="block-content">
			<div class="row-fluid">
				<div class="span4">
					<?php
					/*echo CHtml::label(Yii::t('standard', '_MAX_SCORE').':', 'answer-max-score');*/
					echo CHtml::textField('max_score', $maxScore, array('id' => 'answer-max-score'));
					?>
				</div>
			</div>
	</div>
</div>