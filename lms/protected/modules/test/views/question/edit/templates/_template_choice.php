<?php
$numbersPattern = '\d+\.?\d*';
?>

<br>
<div class="block no-padding">
	<div class="block-header" <?php if ( $questionType == LearningTestquest::QUESTION_TYPE_INLINE_CHOICE):?> style="margin-top: 9px;" <? endif;?>><?=Yii::t('test', '_TEST_ANSWER');?></div>
	<div class="block-content">
		<div id="no-answers-placeholder" <?php if ( $questionType == LearningTestquest::QUESTION_TYPE_INLINE_CHOICE):?> style="margin-top: 9px;" <? endif;?>>
			<span>(<?php echo Yii::t('standard', '_NO_ANSWER'); ?>)</span>
		</div>
		<div id="answers-table-container">
			<table id="answers-table" class="table">
				<?php if ( $questionType == LearningTestquest::QUESTION_TYPE_INLINE_CHOICE):?>
				<thead>
				<tr>
					<th></th>
					<th></th>
					<th class="sort"><?=Yii::t('test', '_ANSWER');?> &nbsp;<i class="fa"></i></th>
					<th></th>
					<th class="text-center"><?=Yii::t('test', '_TEST_IFCORRECT');?></th>
					<th></th>
					<th class="text-center"><?=Yii::t('test', '_TEST_IFINCORRECT');?>
					<th></th>
					<th></th>
				</tr>
				</thead>
				<?php endif;?>
				<tbody id="answers-tbody">
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="block no-padding">

	<div class="block-content" >
		<!--<div id="add-answer-block-header">-->
			<div id="add-answer-link-container">
				<a href="#" id="toggle-add-answer">
					<span class="i-sprite is-plus-solid green"></span>&nbsp;<strong><?=Yii::t('poll', '_POLL_ADD_ONE_ANSWER');?></strong>
				</a>
			</div>
		<!--</div>-->
		<!--<div id="edit-answer-block-header" style="display:none;">
			<?=Yii::t('standard', '_MOD');?>
		</div>-->
	</div>
</div>


<div class="block no-padding" id="form-box-container">

	<div class="block-header" id="add-answer-block-header">
		<a href="#" id="toggle-add-answer"><?=Yii::t('poll', '_POLL_ADD_ONE_ANSWER');?></a>
	</div>
	<div class="block-header" id="edit-answer-block-header" style="display:none;">
		<?=Yii::t('standard', '_MOD');?>
	</div>

	<div class="block-content" >

		<!--<div>-->
			<div class="block-hr"></div>
			<div class="block odd">
				<div class="block-header">
					<?php
					echo CHtml::label(CHtml::checkBox(false, false, array('id' => 'new-answer-correct')) . ' ' . Yii::t('test', '_TEST_CORRECT'), 'new-answer-correct', array('class' => 'checkbox'));
					?>
				</div>
				<div class="block-content">
					<?= CHtml::label(Yii::t('poll', '_ANSWER_TEXT'), 'new-answer-text') ?>
					<?= CHtml::textArea(false, '', array('id' => 'new-answer-text', 'class'=>'input-block-level')) ?>

					<div class="row-fluid">
						<div class="span6">
							<?= CHtml::label(Yii::t('standard', '_COMMENTS'), 'new-answer-comment') ?>
							<?= CHtml::textArea(false, '', array('id' => 'new-answer-comment', 'class'=>'input-block-level')) ?>
						</div>
						<div class="span6">
							<?= CHtml::label(Yii::t('standard', '_SCORE'), false) ?>

							<div class="row-fluid">
								<div class="span6">
									<?php
										$textField = CHtml::textField('new-answer-score_correct', '0', array('id' => 'new-answer-score_correct', 'class'=>'span5', 'pattern' => $numbersPattern));
										echo CHtml::label(Yii::t('test', '_TEST_IFCORRECT').'&nbsp;+&nbsp;'.$textField, 'new-answer-score_correct');
									?>
								</div>
								<div class="span6">
									<?php
										$textField = CHtml::textField('new-answer-score_incorrect', '0', array('id' => 'new-answer-score_incorrect', 'class'=>'span5', 'pattern' => $numbersPattern));
										echo CHtml::label(Yii::t('test', '_TEST_IFINCORRECT').'&nbsp;-&nbsp;'.$textField, 'new-answer-score_incorrect');
									?>
								</div>
							</div>
						</div>
					</div>

					<br>
					<div class="text-right" id="add-answer-buttons">
						<input type="button" id="add-answer-button" class="btn-docebo green big" value="<?=Yii::t('standard', '_ADD');?>" />
						<input type="button" id="undo-add-answer-button" class="btn-docebo black big" value="<?=Yii::t('standard', '_CLOSE');?>" />
					</div>
					<div class="text-right" id="mod-answer-buttons" style="display:none">
						<input type="button" id="mod-answer-button" class="btn-docebo green big" value="<?=Yii::t('standard', '_SAVE');?>" />
						<input type="button" id="undo-edit-answer-button" class="btn-docebo black big" value="<?=Yii::t('standard', '_CLOSE');?>" />
					</div>
				</div>
			</div>
			<div class="block-hr"></div>
		<!--</div>-->

	</div>
</div>
<script type="text/javascript">

//var numbersPattern = <?='/'.$numbersPattern.'/'?>;

var InitialAnswers = [];
<?php

//$answers = ($isEditing ? $questionManager->_getAnswersList($idQuestion) : array());
$answers = ($isEditing ? $questionManager->getAnswers($idQuestion) : array());

if (!empty($answers)):
	foreach ($answers as $answer):
		$toJavascript = array(
			'correct' => ($answer['is_correct'] > 0),
			'answer' => Docebo::nbspToSpace($answer['answer']),//preg_replace('/[^(\x20-\x7F)]*/','', Docebo::nbspToSpace($answer['answer'])),
			// NOTE: the above regex was breaking the inserted text, removing characters such as "à", "è" etc.
			'comment' => $answer['comment'],
			'scoreCorrect' => $answer['score_correct'],
			'scoreIncorrect' => $answer['score_incorrect']
		);
		if (isset($answer['idAnswer']) && $answer['idAnswer'] > 0) {
			$toJavascript['idAnswer'] = Docebo::nbspToSpace($answer['idAnswer']);//preg_replace('/[^(\x20-\x7F)]*/','', Docebo::nbspToSpace($answer['idAnswer']));
			// NOTE: the above regex was breaking the inserted text, removing characters such as "à", "è" etc.
		}
?>
InitialAnswers.push(<?=CJavaScript::encode($toJavascript)?>);
<?php
	endforeach;
endif;
?>

$(document).ready(function(e) {

//	try {

		AnswersManager.init({
			numberPattern: <?php echo '/'.$numbersPattern.'/'?>,
			answerHtmlEditor: <?php echo $answerHtmlEditor ? 'true' : 'false'; ?>,
			singleCorrectAnswer: <?php echo $singleCorrectAnswer ? 'true' : 'false'; ?>,
			answers: InitialAnswers,
			modality: <?php echo CJavaScript::encode($modality); ?>,
			questionType: '<?=$questionType?>'
		});

//	} catch (e) {
//		alert(e);
//	}

});

</script>