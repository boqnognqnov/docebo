<?php
if ($isEditing) {
    $answer = $questionManager->getAnswer($idQuestion);
    echo CHtml::hiddenField('answer[id_answer]', $answer->getPrimaryKey(), array('id' => 'answer-id'));
}
?>

<div class="block no-padding">
    <div class="block-header"><?= Yii::t('test', '_TEST_ANSWER') ?></div>
    <div class="block-content">
        <div class="block-hr"></div>
        <div class="block odd">
            <div class="row-fluid">
                <div class="span6">
                    <?php
                    echo CHtml::label(Yii::t('standard', '_ANSWER'), 'answer-answer');
                    echo CHtml::textField('answer[answer]', (isset($answer) ? $answer->answer : ""),
                        array(
                            'id'    => 'answer-answer',
                            'class' => 'input-block-level',
                            'style' => 'width: 438px'));
                    ?>
                </div>
                <div class="span6">
                    <?= CHtml::label(Yii::t('standard', '_SCORE'), false) ?>

                    <div class="row-fluid">
                        <div class="span6">
                            <?php
                            echo
                            CHtml::label(Yii::t('test', '_TEST_IFCORRECT').'&nbsp;+'.CHtml::textField('answer[score_correct]', (isset($answer) ? $answer->score_correct : '0'), array('id' => 'answer-score_correct', 'class'=>'span5')), 'answer-score_correct');
                            ?></div>
                        <div class="span6">
                            <?php
                            echo
                            CHtml::label(Yii::t('test', '_TEST_IFINCORRECT').'&nbsp;-'.CHtml::textField('answer[score_incorrect]', (isset($answer) ? $answer->score_incorrect : '0'), array('id' => 'answer-score_incorrect', 'class'=>'span5')), 'answer-score_incorrect');
                            ?>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row-fluid">
                <div class="span11">
                    <?= CHtml::label(Yii::t('standard', '_COMMENTS'), 'answer-comment') ?>
                    <?= CHtml::textArea('answer[comment]', (isset($answer) ? $answer->comment : ""), array('id' => 'answer-comment', 'class'=>'input-block-level')) ?>
                </div>
            </div>
        </div>
        <div class="block-hr"></div>
    </div>
</div>