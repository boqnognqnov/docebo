<?php

$this->renderPartial('edit/templates/_max_score', array(
	'questionType' => LearningTestquest::QUESTION_TYPE_EXTENDED_TEXT,
	'questionManager' => $questionManager,
	'isEditing' => $isEditing,
	'idQuestion' => $idQuestion,
	'maxScore' => $maxScore
));

?>