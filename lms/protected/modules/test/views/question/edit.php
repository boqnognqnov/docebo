<?php /* @var $test LearningTest */
$this->breadcrumbs[$test->title] = $backUrl;
$this->breadcrumbs[] = ($isEditing ? Yii::t('standard', '_MOD') : Yii::t('test', '_TEST_ADDQUEST'));
?>
<script type="text/javascript">
if (!String.prototype.trim) { String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, ''); }; }
if (!Array.prototype.insert) { Array.prototype.insert = function (index, item) { this.splice(index, 0, item); }; }

var Validation = {
	checkList: [],
	addValidator: function(validator, message, priority) {
		var i, done = false;
		if (!priority) { priority = 0; }
		if (this.checkList.length > 0) {
			for (i=0; i<this.checkList.length && !done; i++) {
				if (this.checkList[i].priority > priority) {
					this.checkList.insert(i, {
						validator: validator,
						message: message,
						priority: priority
					});
					done = true;
				}
			}
		}
		if (!done) {
			this.checkList.push({
				validator: validator,
				message: message,
				priority: priority
			});
		}
	},
	check: function() {
		var i, output = {success: true};
		for (i=0; i<this.checkList.length && output.success; i++) {
			if (!this.checkList[i].validator.call(this)) {
				output.success = false;
				output.message = this.checkList[i].message;
			}
		}
		return output;
	}
};
</script>
<?php

$this->onBeforeQuestionEditRender(new CEvent($this, array(
	'isEditing' => $isEditing,
	'idTest' => $idTest,
	'idQuestion' => (isset($idQuestion) ? (int)$idQuestion : NULL),
	'questionText' => $questionText,
	'questionCategory' => $questionCategory,
	'difficulty' => $difficulty,
	'useShuffle' => $useShuffle,
	'answeringMaxTime' => $answeringMaxTime,
	'sequence' => $sequence
)));

?>
<div class="l-testpoll">
	<h2 class="subtitle">
		<a class="docebo-back" href="<?echo $backUrl; ?>">
			<span><?= Yii::t('standard', '_BACK') ?></span>
		</a>
		<?php
			$acronyms = LearningTestquest::getQuestionTypesAcronymsList();
			$translations = LearningTestquest::getQuestionTypesTranslationsList();
			if ($isEditing) {
				echo Yii::t('standard', '_MOD').' ';
				echo $acronyms[$questionType].' - ';
				echo $translations[$questionType];
			} else {
				echo Yii::t('standard', '_ADD').' ';
				echo $acronyms[$questionType].' - ';
				echo $translations[$questionType];
			}
		?>
	</h2>
	<br>
	<? if ($isEditing && $is_bank) : ?>
	<div id="bank_question_alert">
		<i class="fa fa-exclamation-circle"></i>&nbsp;<span><?=Yii::t('test', 'You are editing question from the question bank. <strong>Note that every change will be applied to every test where you imported this question.</strong>')?></span>
	</div>
	<? endif; ?>
	<?php
		$action = $isEditing
			? $this->createUrl('update', array('id_test' => (int)$idTest, 'id_question' => $idQuestion, 'opt' => $centralRepoContext ? 'centralrepo' : ''))
			: $this->createUrl('create', array('id_test' => (int)$idTest, 'opt' => $centralRepoContext ? 'centralrepo' : ''));
		echo CHtml::form($action, 'POST', array('id' => 'question-'.($isEditing ? 'edit' : 'create').'-form'));
		//if (isset($idTest) && (int)$idTest > 0) { echo CHtml::hiddenField('id_test', (int)$idTest); }
		//if (isset($idQuestion) && (int)$idQuestion > 0) { echo CHtml::hiddenField('id_question', (int)$idQuestion); }
		echo CHtml::hiddenField('question_type', $questionType, array('id' => 'question-type'));
	?>

	<div class="block-hr"></div>
	<?php if ($this->getUseExtraInfo()): ?>
	<div class="block odd">
		<div class="block-header"><?php echo Chtml::label(Yii::t('standard', '_TITLE'), 'question-extra'); ?></div>
		<div class="block-content">
			<?php echo CHtml::textField('question', $questionExtra, array('id' => 'question-extra')); ?>
		</div>
	</div>
	<?php endif; ?>
	<div class="block odd">
		<div class="block-header"><?php echo Chtml::label(Yii::t('test', '_QUEST'), 'question-text'); ?></div>
		<div class="block-content">

			<?php if ($this->getUseShuffle()): ?>
				<div class="clearfix">
					<div class="pull-right">
						<?php
							$options = array('id' => 'question-shuffle');
							if($test && $test->shuffle_answer == LearningTest::SHUFFLE_ANSWER_RANDOM)
								$options['disabled'] = 'disabled';
						?>
						<?php echo Chtml::label(CHtml::checkBox('shuffle', $useShuffle, $options) . ' ' . Yii::t('test', '_TEST_QUEST_SHUFFLE'), 'question-shuffle', array('class'=>'checkbox')); ?>
					</div>
				</div>
			<?php endif; ?>

			<?php echo CHtml::textArea('question', $questionText, array('id' => 'question-text')); ?>

			<br>
			<div class="row-fluid">
				<?php if ($this->getUseDifficulty()): ?>
					<div class="span4">
						<?php echo Chtml::label(Yii::t('test', '_QUEST_TM2_SETDIFFICULT'), 'question-difficulty'); ?>
						<?php echo CHtml::dropDownList('difficulty', $difficulty, LearningTestquest::getDifficultyList(), array('id' => 'question-difficulty')); ?>
					</div>
				<?php endif; ?>

				<?php if ($this->getUseCategory()): ?>
					<?php
					$criteria = new CDbCriteria();
					$criteria->order = 'name ASC';
					Yii::app()->event->raise('BeforeListingTestCategories', new DEvent($this, array('criteria' => &$criteria)));
					$categories = LearningQuestCategory::model()->findAll($criteria);
					if (!empty($categories)):
						$categoriesList = array(0 => '( '.Yii::t('standard', '_NONE').' )');
						foreach ($categories as $category) {
							$categoriesList[(int)$category['idCategory']] = $category['name'];
						}
						?>
					<div class="span4">
						<?php echo Chtml::label(Yii::t('test', '_TEST_QUEST_CATEGORY'), 'question-category'); ?>
						<?php echo CHtml::dropDownList('id_category', $questionCategory, $categoriesList, array('id' => 'question-category')); ?>
					</div>
					<?php endif; ?>
				<?php endif; ?>

				<?php if ($this->getUseAnsweringMaxTime()): ?>
					<div class="span4">
						<?php echo Chtml::label(Yii::t('test', '_TEST_QUEST_TIMEASS'), 'question-time-assigned'); ?>
						<?php echo CHtml::textField('time_assigned', $answeringMaxTime, array('id' => 'question-time-assigned')); ?>&nbsp;<i>(<?php echo Yii::t('standard', '_SECONDS'); ?>)</i>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="block-hr"></div>

	<?php
	$this->onAfterQuestionEditRender(new CEvent($this, array(
		'isEditing' => $isEditing,
		'idTest' => $idTest,
		'idQuestion' => (isset($idQuestion) ? (int)$idQuestion : NULL),
		'questionText' => $questionText,
		'questionCategory' => $questionCategory,
		'difficulty' => $difficulty,
		'useShuffle' => $useShuffle,
		'answeringMaxTime' => $answeringMaxTime,
		'sequence' => $sequence
	)));
	?>

	<? if($questionType == LearningTestquest::QUESTION_TYPE_FITB) : ?>
		<div class="consider_correct_if_all_answers_are_correct">
			<?php $checked = isset($settings) ? (isset($settings['consider_correct_if_all_answers_are_correct']) ? 'checked' : false) : false ?>
			<input type="checkbox" <?=$checked;?> name="Settings[consider_correct_if_all_answers_are_correct]" id="consider_correct_if_all_answers_are_correct" />
			<label style="display: inline-block;" for="consider_correct_if_all_are_correct"><?=Yii::t('test', 'Consider this question correct only if all the answers are correct')?></label>
		</div>
	<? endif; ?>
	<div class="form-actions">
		<? if (!$isEditing && $idTest != 0) : ?>
			<input type="checkbox" name="is_bank" id="is_bank" />
			<label style="display: inline-block;" for="is_bank"><?=Yii::t('test', 'Add this question to the question bank')?></label>
		<? endif; ?>
		<input type="submit" name="save" class="span2 btn-docebo green big" value="<?php echo Yii::t('standard', '_SAVE'); ?>">
		<input type="submit" name="undo" class="span2 btn-docebo black big" value="<?php echo Yii::t('standard', '_CANCEL'); ?>">
	</div>

	<?=	CHtml::endForm() ?>
<div>


<script type="text/javascript">
$(document).ready(function() {
	$("#is_bank").styler();
	$("#consider_correct_if_all_answers_are_correct").styler();
	TinyMce.attach("#question-text", {
		height: '170px',
		// Setup event that is needed only for the FITB question type
		setup: function(editor) {
			editor.on('focus', function(e) {
				if(AnswersManager.hasOwnProperty('focusedElement')){
					AnswersManager.focusedElement = true;
				}
			});
		}
	});

	Validation.addValidator(function() {
		var content = $('#question-text').tinymce().getContent();
		return (content.trim() != "");
	}, <?php echo CJavaScript::encode(Yii::t('test', "Empty question title is not allowed")); ?>, 0);

	var form = $('#question-<?=($isEditing ? 'edit' : 'create')?>-form'), submitActor = false;

	form.find(":submit").click(function () {
		submitActor = $(this).attr('name');
	});

	var submitBtn = form.find('input[name="save"]');

	form.on('submit', function(e) {
		if (submitActor == 'save') {
			if (form.data('submitted') === true) {
				e.preventDefault();
			} else {
				form.data('submitted', true);
				submitBtn.addClass('disabled');
			}

			var result = Validation.check();
			if (!result.success) {
				//prevent multiple form submits on clicks
				setTimeout(function(){
					form.data('submitted', false);
					submitBtn.removeClass('disabled');
				},2000);

				e.preventDefault();
				/*
				bootbox.alert(result.message);
				*/
				bootbox.dialog(result.message, [{
						label: Yii.t('standard', '_CANCEL'),
						'class': 'btn-docebo big black',
						callback: function() { }
					}],
					{ header: Yii.t('standard', '_WARNING') }
				);
			}
		}
	});
});
</script>

