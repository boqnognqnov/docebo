<h1><?= Yii::t('standard', '_CONFIRM') ?></h1>
<?php

	$form = $this->beginWidget('CActiveForm', array(
		'action' => $this->createUrl('axDelete', array('id_test' => (int)$idTest, 'course_id' => $this->getIdCourse())),
		'method' => 'post',
		'htmlOptions' => array(
			'class' => 'ajax',
			'id' => 'confirm-question-delete-dialog-'.(int)$question->idQuest)
		)
	);

	echo Yii::t('standard', "_AREYOUSURE");
	echo ':<br />';
	echo $question->title_quest;

	//echo CHtml::hiddenField('confirm', 1, array('id' => 'confirm-question-delete-'.(int)$question->idQuest));

	echo CHtml::hiddenField('id_test', (int)$question->idTest, array('id' => 'question-delete-id_test-'.(int)$idTest));
	echo CHtml::hiddenField('id_question',(int)$question->idQuest, array('id' => 'question-delete-id_question-'.(int)$question->idQuest));

?>
<div class="form-actions">
	<input class="btn-docebo green big" type="submit" name="confirm" value="<?=Yii::t('standard', '_DEL')?>">
	<input class="btn-docebo black big close-dialog" type="reset" value="<?=Yii::t('standard', '_CANCEL')?>">
</div>
<?php $this->endWidget(); ?>