<?php /* @var $model LearningTestFeedback */ ?>
<?php /* @var $closeDialog bool */ ?>

<h1><?= Yii::t("standard","_CONFIRM") ?></h1>

<?php if ($closeDialog === true) : ?>

	<a class="auto-close success"></a>

	<script type="text/javascript">

			$.fn.yiiListView.update('test-feedback-list');

	</script>

<?php else: ?>

	<?php
	$form = $this->beginWidget('CActiveForm', array(
		'method' => 'post',
		'htmlOptions' => array(
			'class' => 'ajax'
		)
	));

		echo Yii::t('standard', '_AREYOUSURE');
	?>

	<div class="form-actions">
		<input class="btn-docebo green big" type="submit" value="<?= Yii::t('standard', '_YES') ?>">
		<input class="btn-docebo black big close-dialog" type="reset" value="<?= Yii::t('standard', '_CANCEL') ?>">
	</div>

	<?php $this->endWidget(); ?>
<?php endif; ?>