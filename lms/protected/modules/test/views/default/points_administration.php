<?php /* @var $test LearningTest */
$this->breadcrumbs[] = $test->title;
$this->breadcrumbs[Yii::t('standard', '_MOD')] = $backUrl;
$this->breadcrumbs[] = Yii::t('test', '_TEST_POINT_MANAGEMENT');
?>
<div class="l-testpoll">

	<?php

	$questions = $this->test->getQuestions();

	echo CHtml::form(
		'',
		'POST',
		array('id' => 'points-administration-form')
	);

	//echo CHtml::hiddenField('id_test', (int)$idTest, array('id' => 'id-test-input'));
	echo CHtml::hiddenField('jsonQuestions', '', array('id' => 'questions-json'));
	?>

	<h2 class="subtitle">
		<a class="docebo-back" href="<?echo $backUrl; ?>">
			<span><?= Yii::t('standard', '_BACK') ?></span>
		</a>
		<?php echo Yii::t('test', '_TEST_POINT_MANAGEMENT') ?>
	</h2>

	<br />

	<div class="block-hr"></div>
	<div class="block odd">
		<div class="block-header"><?php echo Yii::t('standard', '_MIN_SCORE'); ?></div>
		<div class="block-content">
			<?= CHtml::label(
				Yii::t('test', '_TEST_PMM_REQUIREDSCORE_POINT').'&nbsp;'
				.CHtml::textField('point_required', $this->test->point_required, array('class'=>'input-small', 'id' => 'point-required-input')), 'point-required-input');
			?>
		</div>
	</div>
	<div class="block-hr"></div>

	<div class="block">
		<div class="block-header"><?php echo Yii::t('test', '_TEST_PM_ONE'); ?></div>
		<div class="block-content">
			<?php
				foreach($this->test->getPointTypes() as $type => $label) {
					$radio = CHtml::radioButton('point_type', $this->test->point_type == $type, array('value' => $type, 'id' => 'point-type-'.$type, 'disabled' => $disableFinalScoreCalculation));
					echo CHtml::label(
						$radio.'&nbsp;'.$label, 'point-type-'.$type, array('class'=>'radio')
					);
				}
			?>
		</div>
	</div>

	<div class="block-hr"></div>
	<div class="block odd">
		<div class="block-header"><?= Yii::t('test', '_TEST_PM_TWO') ?></div>
		<div class="block-content">
			<?php
				$testMaxScore = $this->test->getMaxScore(false, true);
				echo CHtml::label(Yii::t('test', '_TEST_QUEST_MAXTESTSCORE').'&nbsp;'.CHtml::tag('strong', array(), round($testMaxScore, 2)), '');
			?>

			<br>
			<?php
				echo CHtml::label(
						Yii::t('test', '_TEST_QUEST_NEWMAXTESTSCORE').':&nbsp;'.
						CHtml::textField('new_max_score', round($testMaxScore, 2), array('class'=>'input-small', 'id' => 'new-max-score')),
					'new-max-score');
			?>

			<br>
			<label for=""><?php echo Yii::t('test', '_TEST_PM_SUBD_BY'); ?></label>

			<?php
			foreach($this->test->getPointAssingmentTypes() as $pointAssignmentType => $label) {
				$radio = CHtml::radioButton('point_assignment', $this->test->point_assignment == $pointAssignmentType, array('id'=>'point-assignment-'.$pointAssignmentType, 'value' => $pointAssignmentType));
				echo CHtml::label($radio.'&nbsp;'.$label, 'point-assignment-'.$pointAssignmentType, array('class'=>'radio'));
			}
			?>
		</div>
	</div>
	<div class="block-hr"></div>

	<div class="block" id="questions-table-container">
		<div class="block-header"><?= Yii::t('test', '_TEST_TM2_CAPTIONSETTIME') ?></div>
		<div class="block-content">
			<?php
				if (!empty($questions)):
			?>
			<div>
				<table class="table" id="questions-table">
					<thead>
					<tr>
						<th class="text-center"><?php echo Yii::t('test', '_TEST_QUEST_ORDER'); ?></th>
						<th><?php echo Yii::t('test', '_QUEST'); ?></th>
						<th><?php echo Yii::t('standard', '_TYPE'); ?></th>
						<th><?php echo Yii::t('test', '_QUEST_TM2_SETDIFFICULT'); ?></th>
						<th><?php echo Yii::t('test', '_QUEST_TM2_SETSCORE'); ?></th>
					</tr>
					</thead>
					<tbody>
						<?php
							$index = 0;
							foreach ($questions as $question) {
								$index++;
								$idQuestion = $question->getPrimaryKey();
								$questionManager = CQuestionComponent::getQuestionManager($question->type_quest);
								$namePrefix = "questions[$idQuestion]";

								echo '<tr>';
								echo '<td class="text-center">'.$index.'</td>';
								echo '<td>'.Docebo::formatQuestionText($question->title_quest, 80).'</td>';
								echo '<td>'.Yii::t('test', '_QUEST_'.strtoupper($question->type_quest)).'</td>';
								echo '<td>'.CHtml::dropDownList(
									$namePrefix.'[difficulty]',
									$question->difficult,
									LearningTestquest::getDifficultyList(),
									array('id' => 'question-difficulty-'.$idQuestion, 'class' => 'input-small')
								).'</td>';
								echo '<td>'.CHtml::textField(
									$namePrefix.'[score]',
									round($questionManager->getMaxScore($idQuestion)),
									array('id' => 'question-score-'.$idQuestion, 'class' => 'input-small', 'pattern' => '[0-9]+')
								).'</td>';
								echo '</tr>';
							}
						?>
						<tr class="tbl-row-total">
							<td colspan="3"></td>
							<td class="text-center"><strong><?php echo Yii::t('test', '_TEST_TOTAL_SCORE'); ?></strong></td>
							<!--<td class="text-center"><strong>12</strong></td>-->
							<td><strong id="total-score"></strong></td>
						</tr>
					</tbody>
				</table>
			</div>
			<?php
				endif;
			?>
		</div>
	</div>

	<div class="form-actions">
		<input type="submit" name="save" class="span2 btn-docebo green big" value="<?php echo Yii::t('standard', '_SAVE'); ?>">
		<input type="submit" name="undo" class="span2 btn-docebo black big" value="<?php echo Yii::t('standard', '_CANCEL'); ?>">
	</div>

	<?php

	echo CHtml::closeTag('form');

	?>
</div>
<script type="text/javascript">

if (!String.prototype.trim) {
	String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, ''); };
}



$(document).ready(function() {
	$('input').styler();
	$("#points-administration-form").submit(function() {
		var data = $('input[name^="questions"], select[name^="questions"]').serializeFormJSON();
		arrangeJson(data);
		$("#questions-json").val(JSON.stringify(data));
		$('input[name^="questions"], select[name^="questions"]').prop('disabled', true);
	});

	var $form = $('#points-administration-form');

	var scores = $('input[id^=question-score-]');
	var getTotalScore = function() {
		var totalScore = 0;
		scores.each(function() {
			var value = $(this).val(), regexp = /^[0-9]+$/;
			if (regexp.test(value)) {
				totalScore += parseInt(value);
			} else {
				$(this).val('0');
			}
		});
		return totalScore;
	};
	var updateTotalScore = function() {
		$('#total-score').html(''+getTotalScore());
	};

	var getTotalDifficulty = function() {
		var output = 0;
		$('select[id^=question-difficulty-]').each(function() {
			output += parseInt($(this).val());
		});
		return output;
	};

	var updateDifficultyValues = function() {
		var newMaxScore = $('#new-max-score').val();
		newMaxScore = newMaxScore.trim();
		newMaxScore = parseInt(newMaxScore);
		/*
		if (regexp.test(newMaxScore)) {
			newMaxScore = parseInt(newMaxScore);
		} else {
			newMaxScore = 0;
			$('#new-max-score').val('455');
		}
		*/
		var totalDifficulty = getTotalDifficulty();
		$('input[id^=question-score-]').each(function() {
			if (totalDifficulty <= 0) { return; }
			var index = $(this).attr('id').replace('question-score-', '');
			var difficulty = $('#question-difficulty-'+index).val();
			$(this).val(Math.floor((newMaxScore / totalDifficulty) * difficulty));
		});
		updateTotalScore();
	}

	var setQuestionInputs = function(mode) {
		switch (''+mode) {
			case '<?php echo LearningTest::POINT_ASSIGNMENT_DO_NOTHING ?>': {
				$('#questions-table-container').css('display', 'none');
				$('#new-max-score').attr('readonly', true);
				$('select[id^=question-difficulty-]').attr('readonly', true);
				$('input[id^=question-score-]').attr('readonly', true);
				$('#new-max-score').off('change');
				$('select[id^=question-difficulty-]').off('change');
			} break;
			case '<?php echo LearningTest::POINT_ASSIGNMENT_TEST_PM_DIFFICULT ?>': {
				$('#questions-table-container').css('display', 'block');
				$('#new-max-score').attr('readonly', false);
				$('select[id^=question-difficulty-]').attr('readonly', false);
				$('input[id^=question-score-]').attr('readonly', true);
				$('#new-max-score').on('change', function(e) { updateDifficultyValues(); });
				$('select[id^=question-difficulty-]').on('change', function(e) { updateDifficultyValues(); });
				updateDifficultyValues();
			} break;
			case '<?php echo LearningTest::POINT_ASSIGNMENT_TEST_PM_EQUAL_TO_ALL ?>': {
				$('#questions-table-container').css('display', 'none');
				$('#new-max-score').attr('readonly', false);
				$('select[id^=question-difficulty-]').attr('readonly', true);
				$('input[id^=question-score-]').attr('readonly', true);
				$('#new-max-score').off('change');
				$('select[id^=question-difficulty-]').off('change');
			} break;
			case '<?php echo LearningTest::POINT_ASSIGNMENT_TEST_PM_MANUAL ?>': {
				$('#questions-table-container').css('display', 'block');
				$('#new-max-score').attr('readonly', true);
				$('select[id^=question-difficulty-]').attr('readonly', false);
				$('input[id^=question-score-]').attr('readonly', false);
				$('#new-max-score').off('change');
				$('select[id^=question-difficulty-]').off('change');
			} break;
			case '<?php echo LearningTest::POINT_ASSIGNMENT_TEST_PM_EQUAL_TO_ALL_VISIBLE ?>': {
				$('#questions-table-container').css('display', 'none');
				$('#new-max-score').attr('readonly', false);
				$('select[id^=question-difficulty-]').attr('readonly', true);
				$('input[id^=question-score-]').attr('readonly', true);
				$('#new-max-score').off('change');
				$('select[id^=question-difficulty-]').off('change');
			} break;

		}
	};
	setQuestionInputs('<?php echo $this->test->point_assignment; ?>');

	$('input[id^=point-assignment-]').on('change', function(e) {
		var index = $(this).attr('id').replace('point-assignment-', '');
		setQuestionInputs(index);
	});

	var scores = $('input[id^=question-score-]');
	var getTotalScore = function() {
		var totalScore = 0;
		scores.each(function() {
			var value = $(this).val(), regexp = /^[0-9]+$/;
			if (regexp.test(value)) {
				totalScore += parseInt(value);
			} else {
				$(this).val('0');
			}
		});
		return totalScore;
	};
	var updateTotalScore = function() {
		$('#total-score').html(''+getTotalScore());
	};

	scores.on('change', function(e) { updateTotalScore(); });
	updateTotalScore();
});

</script>