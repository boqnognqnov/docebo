<?php /* @var $test LearningTest */
$this->breadcrumbs[] = $test->title;
$this->breadcrumbs[Yii::t('standard', '_MOD')] = $backUrl;
$this->breadcrumbs[] = Yii::t('test', '_TEST_MODALITY_CONTENT');
?>
<div class="l-testpoll">
	<?php

	$activeTab = 1;
	echo CHtml::form(
		'',
		'POST',
		array('id' => 'test-options-form')
	);

	echo CHtml::hiddenField('id_test', (int)$idTest, array('id' => 'id-test-input'));
	?>
	<h2 class="subtitle">
		<a class="docebo-back" href="<? echo $backUrl; ?>">
			<span><?= Yii::t('standard', '_BACK') ?></span>
		</a>
		<?php echo Yii::t('test', '_TEST_MODALITY_CONTENT'); ?>
	</h2>

	<br/>

	<div class="row-fluid">

		<div class="span3">

			<ul class="test-tabs">
				<li class="<?= ($activeTab == 1 ? 'active' : '') ?>">
					<a href="#tab-1">
						<i class="i-sprite is-settings"></i>
						<i class="i-sprite is-settings white"></i>
						<?php echo Yii::t('test', '_TEST_MM_ONE'); ?>
					</a>
				</li>
				<li class="<?= ($activeTab == 2 ? 'active' : '') ?>">
					<a href="#tab-2">
						<i class="i-sprite is-list"></i>
						<i class="i-sprite is-list white"></i>
						<?php echo Yii::t('test', '_TEST_MM_TWO'); ?>
					</a>
				</li>
				<li class="<?= ($activeTab == 3 ? 'active' : '') ?>">
					<a href="#tab-3">
						<i class="i-sprite is-flag2"></i>
						<i class="i-sprite is-flag2 white"></i>
						<?php echo Yii::t('test', '_TEST_MM_FOUR'); ?>
					</a>
				</li>
			</ul>

		</div>

		<div class="span9">

			<div class="tab-content" id="tab-1" style="display:<?php echo $activeTab == 1 ? 'block' : 'none'; ?>">

				<div class="block-hr"></div>

				<div class="block odd">
					<!-- BLOCK 1 -->
					<div class="block-header">
						<?php echo Yii::t('statistic', '_PAGE_VIEW'); ?>
					</div>
					<div class="block-content">
						<p>
							<label class="radio"
								   for="display_type-0"><?php echo CHtml::radioButton('display_type', $test->display_type == LearningTest::DISPLAY_TYPE_GROUPED, array('id' => 'display_type-0', 'value' => LearningTest::DISPLAY_TYPE_GROUPED, 'disabled' => $disablePageView)); ?>
								<?php echo Yii::t('test', '_TEST_MM1_GROUPING'); ?></label>
						</p>

						<p>
							<label class="radio"
								   for="display_type-1"><?php echo CHtml::radioButton('display_type', $test->display_type == LearningTest::DISPLAY_TYPE_SINGLE, array('id' => 'display_type-1', 'value' => LearningTest::DISPLAY_TYPE_SINGLE, 'disabled' => $disablePageView)); ?>
								<?php echo Yii::t('test', '_TEST_MM1_ONEFORPAGE'); ?></label>
						</p>
					</div>
				</div>

				<div class="block-hr"></div>
				<div class="block even">
					<!-- BLOCK 2 -->
					<div class="block-header">
						<?php echo Yii::t('standard', '_QUESTION'); ?>
					</div>
					<div class="block-content">
						<p>
							<label class="radio"
								   for="order_type-0"><?php echo CHtml::radioButton('order_type', $test->order_type == LearningTest::ORDER_TYPE_SEQUENCE, array('id' => 'order_type-0', 'value' => LearningTest::ORDER_TYPE_SEQUENCE, 'disabled' => $disablePageView)); ?>
								<?php echo Yii::t('test', '_TEST_MM1_SEQUENCE'); ?></label>
						</p>

						<p>
							<label class="radio"
								   for="order_type-1"><?php echo CHtml::radioButton('order_type', $test->order_type == LearningTest::ORDER_TYPE_RANDOM, array('id' => 'order_type-1', 'value' => LearningTest::ORDER_TYPE_RANDOM, 'disabled' => $disablePageView)); ?>
								<?php echo Yii::t('test', '_TEST_MM1_RANDOM'); ?></label>
						</p>

						<p>
							<label class="radio" for="order_type-2">
								<?php
								echo CHtml::radioButton('order_type', $test->order_type == LearningTest::ORDER_TYPE_RANDOM_SUBGROUP, array('id' => 'order_type-2', 'value' => LearningTest::ORDER_TYPE_RANDOM_SUBGROUP, 'disabled' => $disablePageView))
									. ' ' . Yii::t('test', '_TEST_MM1_QUESTION_RANDOM_NUMBER', array(
									'[random_quest]' => CHtml::textField('question_random_number', (int)$test->question_random_number, array('id' => 'question_random_number', 'class' => 'span1', 'disabled' => $disablePageView)),
									'[tot_quest]' => $test->getNumberOfQuestions()
								)); ?>

							</label>
						</p>

						<p>
							<label class="radio" for="order_type-3">
								<?php echo CHtml::radioButton('order_type', $test->order_type == LearningTest::ORDER_TYPE_RANDOM_CATEGORY, array('id' => 'order_type-3', 'value' => LearningTest::ORDER_TYPE_RANDOM_CATEGORY, 'disabled' => $disablePageView)); ?>
								<?php echo Yii::t('test', '_ORDER_TYPE_CATEGORY'); ?>
							</label>

						<div id="order-type-3-options"
							 style="display:<?php echo $test->order_type == LearningTest::ORDER_TYPE_RANDOM_CATEGORY ? 'block' : 'none'; ?>;">
							<?php
							if (!empty($questionCategories)):

								echo '<table><tbody>';
								foreach($questionCategories as $idCategory => $infoCategory) {

									$input = CHtml::textField(
										'question_random_category[' . $idCategory . ']',
										(!empty($orderInfo) && isset($orderInfo[$idCategory]) ? (int)$orderInfo[$idCategory] : '0'),
										array('id' => 'question-random-category-' . $idCategory, 'class' => 'span1', 'disabled' => $disablePageView)
									);
									$content = Yii::t('test', '_TEST_MM1_QUESTION_RANDOM_NUMBER', array(
										'[random_quest]' => $input,
										'[tot_quest]' => $infoCategory['total']
									));

									echo '<tr>';
									echo '<td>' . CHtml::label($infoCategory['name'], 'question-random-category-' . $idCategory) . '</td>';
									echo '<td>' . $content . '</td>';
									echo '</tr>';
								}
								echo '</tbody></table>'; else:

							endif;
							?>
						</div>
						</p>
					</div>
				</div>
				<div class="block-hr"></div>
				<div class="block odd">
					<!-- BLOCK 3 -->
					<div class="block-header">
						<?php echo Yii::t('test', '_TEST_MM1_ANSWER_ORDER'); ?>
					</div>
					<div class="block-content">
						<p>
							<label class="radio" for="shuffle_answer-0">
								<?php echo CHtml::radioButton('shuffle_answer', $test->shuffle_answer == LearningTest::SHUFFLE_ANSWER_SEQUENCE, array('id' => 'shuffle_answer-0', 'value' => LearningTest::SHUFFLE_ANSWER_SEQUENCE)); ?>
								<?php echo Yii::t('test', '_TEST_MM1_ANSWER_SEQUENCE'); ?>
							</label>
						</p>

						<p>
							<label class="radio" for="shuffle_answer-1">
								<?php echo CHtml::radioButton('shuffle_answer', $test->shuffle_answer == LearningTest::SHUFFLE_ANSWER_RANDOM, array('id' => 'shuffle_answer-1', 'value' => LearningTest::SHUFFLE_ANSWER_RANDOM)); ?>
								<?php echo Yii::t('test', '_TEST_MM1_ANSWER_RANDOM'); ?></label>
						</p>
					</div>
				</div>
				<div class="block-hr"></div>
			</div>

			<div class="tab-content" id="tab-2" style="display:<?php echo $activeTab == 2 ? 'block' : 'none'; ?>">

				<div class="block-hr"></div>
				<div class="block odd">
					<div class="block-header">
						<?php echo Yii::t('test', '_TEST_MODALITY_CONTENT'); ?>
					</div>
					<div class="block-content">
						<!-- we allow the custom plugin to add checkboxes to settings of test in course-->
						<?php Yii::app()->event->raise('ShowCheckBoxUp', new DEvent($this, array('test'=>$test)));?>
						<p>
							<label class="checkbox" for="mandatory_answer">
								<?php echo CHtml::checkBox('mandatory_answer', $test->mandatory_answer > 0, array('id' => 'mandatory_answer', 'value' => 1)); ?>
								<?php echo Yii::t('test', '_MANDATORY_ANSWER'); ?>
							</label>
						</p>

						<p>
							<label class="checkbox" for="hide_info">
								<?php echo CHtml::checkBox('hide_info', $test->hide_info > 0, array('id' => 'hide_info', 'value' => 1)); ?>
								<?php echo Yii::t('test', '_TEST_MM2_HIDE_INFO'); ?>
							</label>
						</p>

						<p>
							<label class="checkbox" for="mod_doanswer">
								<?php echo CHtml::checkBox('mod_doanswer', $test->mod_doanswer > 0, array('id' => 'mod_doanswer', 'value' => 1)); ?>
								<?php echo Yii::t('test', '_TEST_MM2_MODANSWER'); ?>
							</label>
						</p>

						<p>
							<label class="checkbox" for="can_travel">
								<?php echo CHtml::checkBox('can_travel', $test->can_travel, array('id' => 'can_travel', 'value' => 1)); ?>
								<?php echo Yii::t('test', '_TEST_MM2_CANTRAVEL'); ?>
							</label>
						</p>

						<p>
							<label class="checkbox" for="save_keep">
								<?php echo CHtml::checkBox('save_keep', $test->save_keep > 0, array('id' => 'save_keep', 'value' => 1)); ?>
								<?php echo Yii::t('test', '_TEST_MM2_SAVEKEEP'); ?>
							</label>
						</p>

						<p>
							<label for="max_attempt">
								<?php echo CHtml::textField('max_attempt', (int)$test->max_attempt, array('id' => 'max_attempt', 'class' => 'span1')); ?>
								<?php echo Yii::t('test', '_MAX_ATTEMPT'); ?>
							</label>
						</p>
						<!-- we allow the custom plugin to add checkboxes to settings of test in course-->
						<?php Yii::app()->event->raise('ShowCheckBoxDown', new DEvent($this, array('test'=>$test)));?>
					</div>
				</div>
				<div class="block-hr test-suspension-options"></div>
				<div class="block test-suspension-options">
					<div class="block-header">
						<?php echo Yii::t('test', 'Suspension'); ?>
					</div>
					<div class="block-content">
						<p>
							<label class="checkbox" for="use_suspension">
								<?php echo CHtml::checkBox('use_suspension', $test->use_suspension > 0, array('id' => 'use_suspension', 'value' => 1)); ?>
								<?php echo Yii::t('test', '_USE_SUSPENSION'); ?>
							</label>
						</p>

						<p>
							<label for="suspension_num_hours">
								<?php echo Yii::t('test', '_SUSPENSION_NUM_HOURS'); ?>:&nbsp;
								<?php echo CHtml::textField('suspension_num_hours', (int)$test->suspension_num_hours, array('id' => 'suspension_num_hours', 'class' => 'span1')); ?>
							</label>
						</p>

						<p>
							<label class="checkbox" for="suspension_prerequisites">
								<?php echo CHtml::checkBox('suspension_prerequisites', $test->suspension_prerequisites > 0, array('id' => 'suspension_prerequisites', 'value' => 1)); ?>
								<?php echo Yii::t('test', '_SUSPENSION_PREREQUISITES'); ?>
							</label>
						</p>
					</div>
				</div>
			</div>

			<div class="tab-content" id="tab-3" style="display:<?php echo $activeTab == 3 ? 'block' : 'none'; ?>">

				<div class="block-hr"></div>
				<div class="block odd">
					<div class="block-header">
						<?php echo Yii::t('player', 'General'); ?>
					</div>
					<div class="block-content">
						<p>
							<label class="checkbox" for="show_score">
								<?php echo CHtml::checkBox('show_score', $test->show_score > 0, array('id' => 'show_score', 'value' => 1)); ?>
								<?php echo Yii::t('test', '_TEST_MM4_SHOWTOT'); ?>
							</label>
						</p>

						<p>
							<label class="checkbox" for="show_users_avg">
								<?php echo CHtml::checkBox('show_users_avg', $test->show_users_avg > 0, array('id' => 'show_users_avg', 'value' => 1)); ?>
								<?php echo Yii::t('test', 'Show average score, based on test results of other users that have already completed the test'); ?>
							</label>
						</p>

						<p>
							<label class="checkbox" for="show_score_cat">
								<?php echo CHtml::checkBox('show_score_cat', $test->show_score_cat > 0, array('id' => 'show_score_cat', 'value' => 1)); ?>
								<?php echo Yii::t('test', '_TEST_MM4_SHOWCAT'); ?>
							</label>
						</p>
					</div>
				</div>
				<div class="block-hr"></div>

				<div class="block">
					<div class="block-header">
						<?php echo Yii::t('test', '_TEST_MM4_SHOWDOANSWER'); ?>
					</div>
					<div class="block-content">
						<p><?php
							echo CHtml::label(CHtml::radioButton('show_doanswer', $test->show_doanswer == 0, array('id' => 'show_doanswer-0', 'value' => 0)) . ' ' . Yii::t('standard', '_YES'), 'show_doanswer-0', array('class' => 'radio'));
						?></p>
						<p><?php
							echo CHtml::label(CHtml::radioButton('show_doanswer', $test->show_doanswer == 1, array('id' => 'show_doanswer-1', 'value' => 1)) . ' ' . Yii::t('standard', '_NO'), 'show_doanswer-1', array('class' => 'radio'));
						?></p>
						<p><?php
							echo CHtml::label(CHtml::radioButton('show_doanswer', $test->show_doanswer == 2, array('id' => 'show_doanswer-2', 'value' => 2)) . ' ' . Yii::t('test', '_YES_IF_PASSED'), 'show_doanswer-2', array('class' => 'radio'));
						?></p>
					</div>
				</div>
				<div class="block-hr"></div>

				<div class="block odd">
					<div class="block-header"><?php echo Yii::t('test', '_TEST_MM4_SHOWSOL'); ?></div>
					<div class="block-content">
						<p><?php
							echo CHtml::label(CHtml::radioButton('show_solution', $test->show_solution == 0, array('id' => 'show_solution-0', 'value' => 0)) . ' ' . Yii::t('standard', '_YES'), 'show_solution-0', array('class' => 'radio'));
						?></p>
						<p><?php
							echo CHtml::label(CHtml::radioButton('show_solution', $test->show_solution == 1, array('id' => 'show_solution-1', 'value' => 1)) . ' ' . Yii::t('standard', '_NO'), 'show_solution-1', array('class' => 'radio'));
						?></p>
						<p><?php
							echo CHtml::label(CHtml::radioButton('show_solution', $test->show_solution == 2, array('id' => 'show_solution-2', 'value' => 2)) . ' ' . Yii::t('test', '_YES_IF_PASSED'), 'show_solution-2', array('class' => 'radio'));
						?></p>
						<p><?php
							echo CHtml::label(CHtml::radioButton('show_solution', $test->show_solution == 3, array('id' => 'show_solution-3', 'value' => 3)) . ' ' . Yii::t('test', 'Yes, starting from the following date'), 'show_solution-3', array('class' => 'radio'));
						?></p>
						<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php
							$display = ($test->show_solution == LearningTest::SHOW_SOLUTION_YES_FROM_DATE) ? 'inline-block' : 'none';
							echo CHtml::textField('show_solution_date', $test->show_solution_date, array('id' => 'show_solution_date', 'style' => 'display: '.$display));
						?></p>
					</div>
				</div>
				<div class="block-hr"></div>

			</div>

		</div>

	</div>

	<div class="form-actions">
		<input type="submit" name="save" class="span2 btn-docebo green big"
			   value="<?php echo Yii::t('standard', '_SAVE'); ?>">
		<input type="submit" name="undo" class="span2 btn-docebo black big"
			   value="<?php echo Yii::t('standard', '_CANCEL'); ?>">
	</div>

	<?php echo CHtml::closeTag('form'); ?>

</div>
<script type="text/javascript">

	var $container = $('#test-options-form');

	var initTabs = function () {
		var $tabs = $container.find('ul.test-tabs > li');
		$tabs.find('a').click(function (e) {
			e.preventDefault();
			$container.find('.tab-content').hide().filter($(this).attr('href')).show();
			$tabs.removeClass('active');
			$(this).parent().addClass('active');
		});
	};

	var adjustOrderTypeOptions = function (index) {
		switch (index) {
			case '0':
			case '1':
			case '2':
			{
				$('#order-type-3-options').css('display', 'none');
			}
				break;
			case '3':
			{
				$('#order-type-3-options').css('display', 'block');
			}
				break;
		}
	};

	var toggleSuspensionOptions = function() {
		var val = $('#max_attempt').val();
		if (val == '') return; //do nothing, wait for the user typing something
		var num = parseInt(val);
		if (num < 1) {
			$('.test-suspension-options').hide();
		} else {
			$('.test-suspension-options').show();
		}
	};

	var toggleSuspensionDetails = function() {
		if ($('#use_suspension').prop('checked')) {
			$('#suspension_num_hours').prop('disabled', false);
			$('#suspension_prerequisites').prop('disabled', false);
		} else {
			$('#suspension_num_hours').prop('disabled', true);
			$('#suspension_prerequisites').prop('disabled', true);
		}
		checkSuspensionNumHoursValidity();
	};

	var checkSuspensionNumHoursValidity = function() {
		var enabled;
		if ($('#use_suspension').prop('checked')) {
			//suspension has been enabled: check validity of number of hours value
			var val = $('#suspension_num_hours').val();
			val = parseInt(val);
			if (isNaN(val)) { val = 0; } //non-numeric value is interpreted as 0
			enabled = (val >= 0);
		} else {
			//suspension has not been enabled, so no check has to be performed
			enabled = true;
		}
		//handle button enabling
		var button = $('input[name="save"]');
		if (button.length > 0) {
			if (enabled) {
				button.removeClass('disabled');
				button.prop('disabled', false);
			} else {
				button.addClass('disabled');
				button.prop('disabled', true);
			}
		}
	};


	$(document).ready(function () {
		$('input').styler();
		initTabs();

		$('input[id^=order_type-]').each(function () {
			$(this).on('change', function (e) {
				adjustOrderTypeOptions($(this).attr('id').replace('order_type-', ''));
			});
		});

		$('#max_attempt').on('keyup', function(e) {
			toggleSuspensionOptions();
		});
		toggleSuspensionOptions();

		$('#suspension_num_hours').on('keyup', function(e) {
			checkSuspensionNumHoursValidity();
		});

		$('#use_suspension').on('change', function(e) {
			toggleSuspensionDetails();
		});
		toggleSuspensionDetails();

		$("#show_solution_date").datepicker({
			dateFormat: 'yy-mm-dd'
		});

		$("input[name='show_solution']").on('change', function() {
			if (this.value == 3)
				$("#show_solution_date").show();
			else
				$("#show_solution_date").hide();
		});
	});
</script>