<?php

$testInfo = LearningTest::model()->findByPk($idTest);

$criteria = new CDbCriteria();
$criteria->select = 'COUNT(*)';
$criteria->addCondition('idTest = :id_test');
$criteria->addCondition('type_quest <> :type_quest_title');
$criteria->addCondition('type_quest <> :type_quest_break_page');
$criteria->params = array(
	':id_test' => (int)$idTest, 
	':type_quest_title' => LearningTestquest::QUESTION_TYPE_TITLE, 
	':type_quest_break_page' => LearningTestquest::QUESTION_TYPE_BREAK_PAGE
);
$numQuestions = LearningTestquest::model()->find($criteria);

//.'<div class="title_big">'.$lang->def('_TEST_MODALITY').'</div>'
		
echo CHtml::hiddenField('id_form', 'modality', array('id' => 'modality-id-form'));
echo CHtml::hiddenField('id_test', (int)$idTest, array('id' => 'modality-id-test'));

?>

<fieldset class="">
	<legend><?php echo Yii::t('test', '_TEST_MM_ONE'); ?></legend>
	<div class="">
		<?php
			echo CHtml::radioButton('display_type', ($testInfo->display_type == 0), array('id' => 'modality-display-type-page', 'value' => 0));
			echo CHtml::label(Yii::t('test', '_TEST_MM1_GROUPING'), 'modality-display-type-page');
			echo CHtml::radioButton('display_type', ($testInfo->display_type == 1), array('id' => 'modality-display-type-one', 'value' => 1));
			echo CHtml::label(Yii::t('test', '_TEST_MM1_ONEFORPAGE'), 'modality-display-type-one');
		?>
		<br />
	</div>
</fieldset>