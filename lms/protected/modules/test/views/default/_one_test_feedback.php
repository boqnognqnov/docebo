<?php /* @var $data LearningTestFeedback */ ?>
<?php /* @var $index int */ ?>
<?php /* @var $allowAdminOperations bool */ ?>
<tr data-id="<?= $data->id_feedback ?>">
	<td><?= $data->from_score . ' - ' . $data->to_score ?></td>
	<td><?= $data->feedback_text ?></td>

	<?php
		if($allowAdminOperations):

			if($opt)
			{
				$edit = array('id_test' => $data->id_test, 'id_feedback' => $data->id_feedback, 'opt' => $opt);
				$del = array('id_test' => $data->id_test, 'id' => $data->id_feedback, 'opt' => $opt);
			}
			else
			{
				$edit = array('id_test' => $data->id_test, 'id_feedback' => $data->id_feedback);
				$del = array('id_test' => $data->id_test, 'id' => $data->id_feedback);
			}
	?>
	<td class="img-cell">
		<a href="<?= $this->createUrl('feedbackManagement', $edit) ?>" class="inline-block test-feedback-edit">
			<span class="p-sprite edit-black"></span>
		</a>
	</td>
	<td class="img-cell">
		<a href="<?= $this->createUrl('axDeleteFeedback', $del) ?>" class="inline-block open-dialog" rel="dialog-delete-test-feedback" data-dialog-class="edit-block-modal">
			<span class="p-sprite cross-small-red"></span>
		</a>
	</td>
	<?php endif; ?>

</tr>