<?php
/* @var $testInfo LearningTest */
?>
<div class="container">
	<div class="row-fluid player-launchpad-header">
		<div class="span12">
			<h2><?php echo $testInfo->title; ?></h2>
			<?php
				if (PluginManager::isPluginActive('Share7020App')) {
					$this->widget('common.widgets.CourseLoHeaderApp7020', array('loID' => $idReference));
				}
			?>
			<a id="player-inline-close" class="player-close" href="#"><?= Yii::t('standard', '_CLOSE') ?> </a>
		</div>
	</div>
	<?php
	$numberOfQuestions = $testInfo->getNumberOfQuestionsForStudent();

	$endStatuses = array(
		LearningTesttrack::STATUS_VALID,
		LearningTesttrack::STATUS_NOT_CHECKED,
		LearningTesttrack::STATUS_PASSED,
		LearningTesttrack::STATUS_NOT_PASSED
	);

	$isEnd = in_array($trackInfo->score_status, $endStatuses);
	// Avoid buisiness/data logic inside views. Do that in controllers (plamen)
	// $userCourse = LearningCourseuser::model()->findByAttributes(array('idCourse' => $this->getIdCourse(), 'idUser' => Yii::app()->user->id));
	$prerequisites = $mainInfo->prerequisites;

	if ($trackInfo->score_status == LearningTesttrack::STATUS_PASSED) {
		$incomplete = FALSE;
	} elseif ($trackInfo->score_status == LearningTesttrack::STATUS_VALID) {
		if ($trackInfo->score >= $testInfo->point_required) {
			$incomplete = FALSE;
		} else {
			$incomplete = TRUE;
		}
	} else {
		$incomplete = TRUE;
	}

	$testPassed = $trackInfo->score_status == LearningTesttrack::STATUS_PASSED;
	if ($testInfo->answersVisible(Yii::app()->user->id))
		$showResult = true;
	else
		$showResult = false;
	?>
	<div class="player-arena-test-inline">
		<div class="row-fluid">
			<div class="span6">
				<div class="row-fluid no-space">
					<div class="span12">
						<div class="player-launchpad-test-head-image">
							<?php if (!$isEnd): ?>
								<img src="<?php echo $this->getPlayerAssetsUrl().'/images/testpoll-img.png'; ?>" />
							<? else: ?>
								<br/>
								<div class="review-result">
									<div class="row-fluid">
										<div class="span2 text-center"><?php
											echo ( $trackInfo->score_status == LearningTesttrack::STATUS_NOT_PASSED
												? '<span class="p-sprite lo-failed-icon-big"></span>'
												: '<span class="p-sprite lo-passed-icon-big"></span>' );
											?>
										</div>
										<div class="span10">
											<p><?php
												if($manualQuestionsCount > 0 && $trackInfo->score_status != LearningTesttrack::STATUS_VALID){
													echo '<span class="review-result-text">' . Yii::t('test', 'Waiting for final grade') . '</span>';
												}else{
													echo (  $trackInfo->score_status == LearningTesttrack::STATUS_NOT_PASSED
														? '<span class="review-result-text">' . Yii::t('test', '_TEST_FAILED') . '</span>'
														: '<span class="review-result-text">' . Yii::t('test', '_TEST_COMPLETED') . '</span>' );
												}
											?></p>
											<!-- score -->
											<?php if ($testInfo->show_score) : ?>

											<div class=" review-scores">
												<?php
												// user score
												$score = $trackInfo->getCurrentScore();
												switch ($testInfo->point_type) {
													case LearningTest::POINT_TYPE_NORMAL : {
														echo '<span class="test_score_note">' . Yii::t('test', '_TEST_TOTAL_SCORE') . '</span> '
															.'<b class="' . ($trackInfo->score_status == LearningTesttrack::STATUS_NOT_PASSED ? 'score-failed' : 'score-passed' ) . '">'
															. number_format($score, 2)
															. ( $totalMaxScore ? ' / ' . $totalMaxScore : '' )
															. '</b>';
													};break;
													case LearningTest::POINT_TYPE_PERCENT : {
														echo '<span class="test_score_note">' . Yii::t('test', '_TEST_TOTAL_SCORE') . '</span> '
															.'<b class="' . ($trackInfo->score_status == LearningTesttrack::STATUS_NOT_PASSED ? 'score-failed' : 'score-passed' ) . '">'
															. $score . '</b>'.($commonTrackInfo && $commonTrackInfo->score_max == 100 ? ' <span class="review-percentage">%</span>' : '');
													};break;
												}
												?>
											</div>
											<?php endif; ?>
										</div>
									</div>
								</div>

							<? endif;?>
						</div>
						<?php if ($testInfo->description): ?>
						<div class="player-launchpad-test-description">
							<?php echo $testInfo->description; ?>
						</div>
						<?php endif; ?>
					</div>
				</div>


				<?php // HIDE START/RUN button if there are no questions  ?>
				<?php if ($numberOfQuestions > 0) : ?>

				<div class="row-fluid no-space">
						<?php

						$playUrl = $this->createUrl('player/play', array(
							'id_test' => $testInfo->getPrimaryKey(),
							'course_id' => $this->getIdCourse()
						));

						echo CHtml::form($playUrl, 'post', array('id' => 'play-test-form'));
						echo '<div class="span6 text-left">';
						if ($trackInfo->score_status == LearningTesttrack::STATUS_NOT_COMPLETE || $trackInfo->score_status == LearningTesttrack::STATUS_DOING) {
							echo CHtml::hiddenField('page_continue', $trackInfo->last_page_seen, array('id' => 'page-continue'));
						}
						if ($isEnd) {
							echo CHtml::hiddenField('show_result', 1, array('id' => 'show-result'));
						}
						// we allow the custom plugin to add checkboxes to settings of test in course
						Yii::app()->event->raise('ShowAdditionalButton', new DEvent($this, array('data'=>$_data_,'url'=>$playUrl)));

                        if ($testInfo->save_keep && ($trackInfo->score_status == LearningTesttrack::STATUS_NOT_COMPLETE || $trackInfo->score_status == LearningTesttrack::STATUS_DOING)) {
                            echo '<div class="text_bold">'.Yii::t('test', '_TEST_SAVED').'</div><br />';
                        }


                        if ($isEnd && $showResult && LearningTesttrackAnswer::model()->countByAttributes(array('idTrack' => $trackInfo->idTrack)) > 0) {
							//echo '&nbsp;';
							//echo CHtml::submitButton(Yii::t('test', '_TEST_SHOW_REVIEW'), array('name' => 'show_review', 'id' => 'show-review', 'class' => 'btn-docebo verybig'));
							echo '<a class="btn-docebo light-grey verybig" href="'.$this->createUrl('player/review', array(
									'id_test' => $testInfo->getPrimaryKey(),
									'course_id' => $this->getIdCourse()
								)).'">'
								.Yii::t('test', '_TEST_SHOW_REVIEW')
								.'</a>';

						} elseif ($testInfo->save_keep && ($trackInfo->score_status == LearningTesttrack::STATUS_NOT_COMPLETE || $trackInfo->score_status == LearningTesttrack::STATUS_DOING) && !$suspended) {
							echo '&nbsp;';
							echo CHtml::submitButton(Yii::t('test', '_TEST_CONTINUE'), array('name' => 'continue', 'id' => 'continue', 'class' => 'btn-docebo green verybig'));
						}


						echo '</div>';
						echo '<div class="span6 text-right">';

						if($suspended)
						{//TODO: Change the translations key for the suspension
							/*if($max_attemp_reached)
								echo '<span class="btn-docebo light-grey verybig">'.Yii::t('test', '_MAX_ATTEMPT_REACH').'</span>';
							elseif($need_prerequisites)
								echo '&nbsp;';
							else
								echo '<span class="btn-docebo light-grey verybig">'.Yii::t('test', 'Your test is suspended. You will be able to retake your test in {time left}.', array('{time left}' => $suspend_time_left)).'</span>';*/
							if (!empty($suspend_time_left)) {
								echo '<span class="btn-docebo light-grey verybig">'.Yii::t('test', 'Your test is suspended. You will be able to retake your test in {time left}.', array('{time left}' => $suspend_time_left)).'</span>';
							} elseif($max_attemp_reached) {
								echo '<span class="btn-docebo light-grey verybig">'.Yii::t('test', '_MAX_ATTEMPT_REACH').'</span>';
							} else {
								echo '&nbsp;';
							}
						}
						elseif ($trackInfo->score_status == LearningTesttrack::STATUS_NOT_COMPLETE) {
							if($max_attemp_reached)
								echo '<span class="btn-docebo light-grey verybig">'.Yii::t('test', '_MAX_ATTEMPT_REACH').'</span>';
							else
								echo CHtml::submitButton(Yii::t('test', '_TEST_BEGIN'), array('name' => 'restart', 'id' => 'restart', 'class' => 'btn-docebo green verybig'));
						} elseif ($isEnd) {
							if ($courseUserModel->level > 3) {
								if($max_attemp_reached)
									echo '<span class="btn-docebo light-grey verybig">'.Yii::t('test', '_MAX_ATTEMPT_REACH').'</span>';
								else
									echo CHtml::submitButton(Yii::t('test', '_TEST_RESTART'), array('name' => 'restart', 'id' => 'restart', 'class' => 'btn-docebo green verybig'));
							} elseif (str_replace('incomplete', '', $prerequisites) !== $prerequisites) {
								if ($incomplete) {
									if($max_attemp_reached)
										echo '<span class="btn-docebo light-grey verybig">'.Yii::t('test', '_MAX_ATTEMPT_REACH').'</span>';
									else
										echo CHtml::submitButton(Yii::t('test', '_TEST_RESTART'), array('name' => 'restart', 'id' => 'restart', 'class' => 'btn-docebo green verybig'));
								} else {
									echo Yii::t('test', '_TEST_COMPLETED');
								}
							} elseif (str_replace('NULL', '', $prerequisites) !== $prerequisites) {
								if ($trackInfo->score_status !== LearningTesttrack::STATUS_VALID && $trackInfo->score_status !== LearningTesttrack::STATUS_PASSED) {
									if($max_attemp_reached)
										echo '<span class="btn-docebo light-grey verybig">'.Yii::t('test', '_MAX_ATTEMPT_REACH').'</span>';
									else
										echo CHtml::submitButton(Yii::t('test', '_TEST_RESTART'), array('name' => 'restart', 'id' => 'restart', 'class' => 'btn-docebo green verybig'));
								} else {
									echo Yii::t('test', '_TEST_COMPLETED');
								}
							} else {
								if($max_attemp_reached)
									echo '<span class="btn-docebo light-grey verybig">'.Yii::t('test', '_MAX_ATTEMPT_REACH').'</span>';
								else
									echo CHtml::submitButton(Yii::t('test', '_TEST_RESTART'), array('name' => 'restart', 'id' => 'restart', 'class' => 'btn-docebo green verybig'));
							}
						} else {
							//if ($trackInfo) $trackInfo->reset();
							if($max_attemp_reached)
								echo '<span class="btn-docebo light-grey verybig">'.Yii::t('test', '_MAX_ATTEMPT_REACH').'</span>';
							else
								echo CHtml::submitButton(Yii::t('test', '_TEST_BEGIN'), array('name' => 'begin', 'id' => 'begin', 'class'=>'btn-docebo green verybig'));
						}

						echo '</div>';

						echo CHtml::closeTag('form');
						?>

					<?php

						if($need_prerequisites) {
							echo '<div class="clearfix"></div><br/>'
								.'<p><b>'.Yii::t('test', 'Your test is suspended. In order to retake this test, you must review all prerequisite material.').'</b></p>';
						}
						if (!empty($prerequisites_obj)) {

							echo '<div class="native-styled"><ul>';
							foreach($prerequisites_obj as $id_prerequisites => $prerequisites_title) {

								echo '<li>' . $prerequisites_title . '</li>';
							}
							echo '</ul></div>';

						}

					?>
				</div>


				<?php endif; ?>

				<?php
					$feedbackTestText = LearningTestFeedback::getFeedbackText($this->test->getPrimaryKey(), $score);

					$statusCompleted = (
						$trackInfo->score_status != '' &&
						$trackInfo->score_status != LearningTesttrack::STATUS_DOING
					) ? true : false;
				?>

				<?php if(!empty($feedbackTestText) && $statusCompleted) : ?>
					<div class="launchpad-test-feedback">
						<?php echo $feedbackTestText; ?>
					</div>
				<?php endif; ?>

			</div>
			<div class="span6 player-launchpad-test-info player-launchpad-content-text">
				<?php if ($testInfo->hide_info <= 0): ?>
				<h3><?php echo Yii::t('test', '_TEST_INFO'); ?>:</h3>
				<ul class="test_info_list">
					<?php if ($testInfo->order_type < 2): ?>
					<li>
						<?php
							$replaceValue = $testInfo->getMaxScore();//''.($testInfo->point_type != 1 ? TestUtils::getMaxScore($testInfo->idTest, Yii::app()->user->id) : 100);
							echo Yii::t('test', '_TEST_MAXSCORE', array('[max_score]' => $replaceValue)).($testInfo->point_type==LearningTest::POINT_TYPE_PERCENT ? ' %' : '');
						?>
					</li>
					<?php endif; ?>
					<li>
						<?php
							$replaceValue = ''.$testInfo->getNumberOfQuestionsForStudent();
							echo Yii::t('test', '_TEST_QUESTION_NUMBER', array('[question_number]' => $replaceValue));
						?>
					</li>
					<?php if ($testInfo->point_required != 0): ?>
					<li>
						<?php
							$replaceValue = ''. round($testInfo->point_required, 2);
							if($testInfo->point_type==LearningTest::POINT_TYPE_PERCENT){
								$replaceValue .= ' %';
							}
							echo Yii::t('test', '_TEST_REQUIREDSCORE', array('[score_req]' => $replaceValue));
						?>
					</li>
					<?php endif; ?>
					<li>
						<?php echo ($testInfo->save_keep ? Yii::t('test', '_TEST_SAVEKEEP') : Yii::t('test', '_TEST_SAVEKEEP_NO')); ?>
					</li>
					<li>
						<?php echo ($testInfo->mod_doanswer ? Yii::t('test', '_TEST_MOD_DOANSWER') : Yii::t('test', '_TEST_MOD_DOANSWER_NO')); ?>
					</li>
					<li>
						<?php echo ($testInfo->can_travel ? Yii::t('test', '_TEST_CAN_TRAVEL') : Yii::t('test', '_TEST_CAN_TRAVEL_NO')); ?>
					</li>
					<li>
						<?php
							echo (($testInfo->show_score || $testInfo->show_score_cat)
								? Yii::t('test', '_TEST_SHOW_SCORE')
								: Yii::t('test', '_TEST_SHOW_SCORE_NO'));
						?>
					</li>
					<li>
						<?php echo ($testInfo->show_solution != 1 ? Yii::t('test', '_TEST_SHOW_SOLUTION') : Yii::t('test', '_TEST_SHOW_SOLUTION_NO')); ?>
					</li>
					<li>
						<?php
							switch ($testInfo->time_dependent) {
								case 0 : echo Yii::t('test', '_TEST_TIME_ASSIGNED_NO'); break;
								case 1 : echo $time_readable; break;
								case 2 : echo Yii::t('test', '_TEST_TIME_ASSIGNED_QUEST'); break;
							}
						?>
					</li>
					<?php if ($testInfo->max_attempt > 0): ?>
					<li>
						<?php
							//attempts are counted differently in case of suspension enabled
							$numberOfAttempts = $trackInfo->number_of_attempt;
							$replaceValue = ($testInfo->max_attempt - $numberOfAttempts);
							echo Yii::t('test', '_NUMBER_OF_ATTEMPT', array('[remaining_attempt]' => $replaceValue));
						?>
					</li>
					<?php endif; ?>
				</ul>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<!--[if IE 8]>
<script type="text/javascript">
	$(function(){
		$('.player-launchpad-test-description img').each(function(e){
			// IE needs this fix, otherwise images are always shown in their original size
			if($(this).attr('width'))
				$(this).css('width', $(this).attr('width'));
			if($(this).attr('height'))
				$(this).css('height', $(this).attr('height'));
		});
	});

</script>
<![endif]-->
<script>
	$('#play-test-form input:submit').click(function() {
		var formData = $(this).closest('form').serializeArray();
		formData.push({ name: this.name, value: this.value });

		$.ajax({
			url: $('#play-test-form').attr('action'),
			type: 'POST',
			dataType: 'JSON',
			data: formData
		}).done(function(o) {
			if (o.success) {
				window.location.href = o.data.redirectUrl;
			}
		});

		return false;
	});
</script>