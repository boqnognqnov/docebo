<h1>
	<?php echo Yii::t('standard', '_MOD'); ?>
</h1>


<?php
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'test-edit-title-form',
		'method' => 'post',
		'action' => $this->createUrl('axEditTitle', array('id_test' => $test->getPrimaryKey())),
		'htmlOptions' => array(
			'class' => 'ajax'
		)
	));
?>

	<?= CHtml::label(Yii::t('standard', '_TITLE'), 'poll-title', array('class'=>'')) ?>
	<?= CHtml::textField("title", isset($test) ? $test->title : '', array('id' => 'test-title-input', 'class' => 'input-block-level squared')); ?>
	<!--<input type="text" id="poll-title" name="title" class="input-block-level squared">-->

	<br>

	<?= CHtml::label(Yii::t('standard', '_DESCRIPTION'), 'description', array('class'=>'')) ?>
	<?= CHtml::textArea('description', isset($test) ? $test->description : '', array('id' => 'test-description-input', 'class' => 'input-block-level squared')); ?>
	<!--<textarea id="player-arena-poll-editor-textarea" name="description" class="input-block-level squared"></textarea>-->

	<br>

	<?= CHtml::hiddenField('confirm', 1, array('id' => 'edit-title-confirm')) ?>

	<div class="form-actions">
		<input class="btn-docebo green big" id="edit-title-save-btn" type="submit" value="<?=Yii::t('standard', '_SAVE')?>">
		<input class="btn-docebo black big close-dialog" type="button" value="<?=Yii::t('standard', '_CANCEL')?>">
	</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
$(document).ready(function() {
	TinyMce.attach("#test-description-input", {height: '140px'});
});
</script>