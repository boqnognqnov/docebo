<div id="player-arena-test-create-panel" class="player-arena-editor-panel">
	<div class="header"><?php echo Yii::t('organization', 'Create test'); ?></div>

	<div class="content">

		<?php
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'test-create-form',
			'method' => 'post',
			'action' => $this->createUrl("axSaveLo"),
			'htmlOptions' => array(
				'class' => 'form-horizontal',
				'enctype' => 'multipart/form-data',
			)
		));
		?>

		<div id="player-arena-uploader">

			<div class="tabbable"> <!-- Only required for left/right tabs -->
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab1" data-toggle="tab"><?=LearningOrganization::objectTypeValue($lo_type)?></a></li>
					<li><a href="#lo-additional-information" data-toggle="tab"><?=Yii::t('myactivities', 'Additional info')?></a></li>

					<?php
					// Raise event to let plugins add new tabs
					Yii::app()->event->raise('RenderEditLoTabs', new DEvent($this, array(
						'courseModel' => $courseModel,
						'loModel' => $loModel
					)));
					?>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tab1">
						<div id="player-uploader-wrapper" class="player-arena-uploader">

							<div class="control-group">
								<?php echo CHtml::label(Yii::t('standard', '_TITLE'), 'title', array(
									'class' => 'control-label'
								)); ?>
								<div class="controls">
									<?php echo CHtml::textField('title', (isset($folderObj) && $folderObj ? $folderObj->title : ''), array('id' => 'title', 'class' => 'input-block-level')); ?>
								</div>
							</div>
							<div class="control-group">
								<?= CHtml::label(Yii::t('standard', '_DESCRIPTION'), 'description', array(
									'class' => 'control-label'
								)) ?>
								<div class="controls">
									<textarea id="player-arena-test-editor-textarea" name="description"
									          class="input-block-level"></textarea>
								</div>
							</div>

						</div>
					</div>
					<div class="tab-pane" id="lo-additional-information">
						<? $this->widget('common.widgets.PlayerLayoutAdditionalInfo', array(
							'loModel'=> isset($loModel) ? $loModel : null,
							'idCourse'=>$idCourse
						)); ?>
					</div>

					<?php
					// Raise event to let plugins add new tab contents
					Yii::app()->event->raise('RenderEditLoTabsContent', new DEvent($this, array(
						'courseModel' => $courseModel,
						'loModel' => $loModel
					)));
					?>
				</div>
			</div>

		</div>


		<div class="text-right">
			<input type="submit" name="confirm_save_test" class="btn-docebo green big"
				   value="<?php echo Yii::t('standard', '_SAVE'); ?>">&nbsp;&nbsp;
			<a id="cancel_save_test"
			   class="btn-docebo black big"><?php echo Yii::t('standard', '_CANCEL'); ?></a>
		</div>

		<?php $this->endWidget(); ?>

	</div>

</div>
