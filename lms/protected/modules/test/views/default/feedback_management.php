<?php /* @var $model LearningTestFeedback */ ?>
<?php /* @var $htmlFeedbackList string */ ?>
<?php /* @var $test LearningTest */
$this->breadcrumbs[] = $test->title;
$this->breadcrumbs[Yii::t('standard', '_MOD')] = $backUrl;
$this->breadcrumbs[] = Yii::t('test', '_FEEDBACK_MANAGEMENT');
?>
<div class="l-testpoll">
	<h2 class="subtitle">
		<a class="docebo-back" href="<?echo $backUrl; ?>">
			<span><?= Yii::t('standard', '_BACK') ?></span>
		</a>
		<?php echo Yii::t('test', '_FEEDBACK_MANAGEMENT') ?>
	</h2>

	<br />

	<div class="test-feedback-container">
		<table class="table">
			<thead>
				<tr>
					<th><?=Yii::t('standard', '_SCORE')?></th>
					<th><?=Yii::t('test', '_FEEDBACK_TEXT')?></th>
					<?php if ($allowAdminOperations): ?>
					<th class="img-cell"></th>
					<th class="img-cell"></th>
					<?php endif; ?>
				</tr>
			</thead>
		</table>

		<!-- start feedback list table -->
		<?php
			//echo $htmlFeedbackList
			$this->renderPartial('_feedback_list', array(
				'allowAdminOperations' => $allowAdminOperations,
				'dataProvider' => LearningTestFeedback::model()->getDataProvider($idTest),
				'opt' => $opt
			));
		?>
		<!-- end feedback list table -->
	</div>

	<br>

	<div class="add-feedback-form-container">
		<?php

		echo CHtml::form(
			$this->createUrl('addFeedback', array('course_id' => $this->getIdCourse(), 'opt' => $opt)),
			'POST',
			array('id' => 'add-feedback-form')
		);

			echo CHtml::activeHiddenField($model, 'id_feedback');
			echo CHtml::activeHiddenField($model, 'id_test');
		?>

		<div class="block-hr"></div>
		<div class="block odd">
			<div class="block-header"><?php echo Yii::t('standard', '_ADD'); ?></div>
			<div class="block-content">
				<?= CHtml::label(Yii::t('standard', '_SCORE').':  ', CHtml::activeId($model, 'from_score')) ?>
				<?= ucfirst(Yii::t('standard', '_FROM')) . ' ' .
					CHtml::activeTextField($model, 'from_score', array('class'=>'span1')) . '&nbsp;&nbsp;&nbsp;' .
					ucfirst(Yii::t('standard', '_TO')) . ' ' .
					CHtml::activeTextField($model, 'to_score', array('class'=>'span1'))
				?>
				<br /><br />
				<?= CHtml::label(Yii::t('test', '_FEEDBACK_TEXT'), 'feedback-text-input') ?>
				<?= CHtml::activeTextArea($model, 'feedback_text', array('class'=>'input-block-level')) ?>
				<br />
				<div class="form-actions">
					<input type="submit" name="save" class="span2 btn-docebo green big"  value="<?php echo Yii::t('standard', '_SAVE'); ?>">
					<input type="button" id="cancel" class="span2 btn-docebo black big"  value="<?php echo Yii::t('standard', '_RESET'); ?>">
				</div>
			</div>
		</div>
<!--
		<div class="block odd hide">
			<div class="block-header"><?= Yii::t('standard', '_COMPETENCES') ?></div>
			<div class="block-content">
				<?= CHtml::label('Name', '') ?>
				<?= CHtml::textField('', '', array('class'=>'input-block-level')) ?>
			</div>
		</div>

		<div class="block odd">
			<div class="block-header"><?= Yii::t('standard', '_COURSES') ?></div>
			<div class="block-content">
				<div class="hide">
					<?= CHtml::label('New course', '') ?>
					<?= CHtml::textField('', '', array('class'=>'input-block-level')) ?>
				</div>

				<?= CHtml::label(Yii::t('test', '_FEEDBACK_TEXT'), 'feedback-text-input') ?>
				<?= CHtml::activeTextArea($model, 'feedback_text', array('class'=>'input-block-level')) ?>
			</div>
		</div>
		<div class="block-hr"></div>
-->



		<?php
		echo CHtml::closeTag('form');
		?>
	</div>

	<script type="text/javascript">

		if (!String.prototype.trim) {
			String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, ''); };
		}

		var oFeedbackMgmt = {
			clearInputs: function() {
				$('#<?= CHtml::activeId($model, 'from_score') ?>').val('');
				$('#<?= CHtml::activeId($model, 'to_score') ?>').val('');
				$('#<?= CHtml::activeId($model, 'id_feedback') ?>').val('');
				$('#<?= CHtml::activeId($model, 'feedback_text') ?>').tinymce().setContent('');
				//$('#add-feedback-form :reset').hide();
			},
			checkInputValidity: function() {
				var output = {success: true},
					text = $('#<?= CHtml::activeId($model, 'feedback_text') ?>').tinymce().getContent(),
					fromScore = parseFloat($('#<?= CHtml::activeId($model, 'from_score') ?>').val()),
					toScore = parseFloat($('#<?= CHtml::activeId($model, 'to_score') ?>').val());

				if (text.trim() == "") {
					output.success = false;
					output.message = Yii.t('test', 'Invalid feedback text');
				} else if (fromScore >= toScore) {
					output.success = false;
					output.message = Yii.t('test', 'Invalid feedback score');
				}

				return output;
			},
			init: function() {
				TinyMce.attach('#<?= CHtml::activeId($model, 'feedback_text') ?>');

				$('#add-feedback-form').submit(function(e) {
					e.preventDefault();

					var check = oFeedbackMgmt.checkInputValidity();
					if (!check.success) {
						/*
						bootbox.alert(check.message);
						*/
						bootbox.dialog(check.message, [{
								label: Yii.t('standard', '_CANCEL'),
								'class': 'btn-docebo big black',
								callback: function() { }
							}],
							{ header: Yii.t('standard', '_WARNING') }
						);
						return false;
					}

					var $form = $(this);

					$.ajax({
						url: $(this).attr('action'),
						type: 'post',
						dataType: 'JSON',
						data: $form.serialize()
					}).done(function(o) {
						if (o.success) {
							//refresh table
							oFeedbackMgmt.clearInputs();
							//... show success message ?
							$.fn.yiiListView.update('test-feedback-list');
						} else {
							//show error message
						}
					}).fail(function() {
						//show error message
					}).always(function() {
						//...
					});
				});

				$('#add-feedback-form input[id="cancel"]').click(function(e){
					oFeedbackMgmt.clearInputs();
				});
			}
		};
	$(document).ready(function() {

		$('body').on('click', '.test-feedback-edit', function(e) {
			e.preventDefault();
			TinyMce.destroy('#<?= CHtml::activeId($model, 'feedback_text') ?>');

			$('.add-feedback-form-container').load($(this).attr('href') + ' #add-feedback-form', function() {
				var $form = $(this);

				var top = $form.offset().top;
				$('html,body').animate({scrollTop: top}, 'slow');

				//$('#add-feedback-form :reset').show();
				oFeedbackMgmt.init();
			});
		});

		oFeedbackMgmt.init();

		<?php if ($model->isNewRecord) : ?>
		//$('#add-feedback-form :reset').hide();
		<?php endif; ?>
	});

	</script>
</div>