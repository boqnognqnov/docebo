<?php /* @var $test LearningTest */
$this->breadcrumbs[] = $test->title;
$this->breadcrumbs[Yii::t('standard', '_MOD')] = $backUrl;
$this->breadcrumbs[] = Yii::t('test', 'Import as GIFT');
?>
<div class="l-testpoll">

	<h2 class="subtitle">
		<a class="docebo-back" href="<?echo $backUrl; ?>">
			<span><?= Yii::t('standard', '_BACK') ?></span>
		</a>
		<?php echo Yii::t('test', 'Import as GIFT'); ?>
	</h2>

	<br />

<?php
	$form = $this->beginWidget('CActiveForm', array(
			'action' => $this->createUrl('doImportAsGift', array('id_test' => $idTest, 'opt' => $opt)),
			'method' => 'post',
			'htmlOptions' => array(
				'id' => 'import-as-gift-form',
				'enctype' => 'multipart/form-data'
			),
	));

?>

	<div class="block-hr"></div>
	<div class="block odd">
		<div class="block-header"><?php echo Yii::t('standard', '_IMPORT'); ?></div>
		<div class="block-content">
			<?php
				echo CHtml::label(Yii::t('standard', '_FILE').'&nbsp;'.CHtml::fileField('gift_file', '', array('id' => 'gift-file-input')), 'gift-file-input');
				echo '<br/><br />';
				echo CHtml::label(Yii::t('organization_chart', '_ORG_CHART_IMPORT_CHARSET').'&nbsp;'.CHtml::textField('charset', 'utf-8', array('id' => 'charset-input')), 'charset-input');
			?>
		</div>
	</div>
	<div class="block-hr"></div>

	<div class="form-actions">
		<input class="span2 btn-docebo green big" type="submit" value="<?=Yii::t('standard', '_SAVE')?>">
		<input class="span2 btn-docebo black big" type="submit" value="<?=Yii::t('standard', '_CANCEL')?>" name="undo">
	</div>

<?php $this->endWidget(); ?>

</div>
