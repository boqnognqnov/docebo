<?php /* @var $dataProvider CActiveDataProvider */ ?>
<?php /* @var $allowAdminOperations bool */ ?>

<?php
	$this->widget('zii.widgets.CListView', array(
		'dataProvider' => $dataProvider,
		'template' => "{sorter}\n{items}",
		'ajaxUpdate' => true,
		'afterAjaxUpdate' => 'function(){$(document).controls()}',
		'itemsTagName' => 'table',
		'itemsCssClass' => 'table',
		'itemView' => 'test.views.default._one_test_feedback',
		'viewData' => array(
			'allowAdminOperations' => $allowAdminOperations,
			'opt' => $opt
		),
		'id' => 'test-feedback-list',
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
		'ajaxVar' => 'htmlOnly',
		'emptyText' => Yii::t('standard', '_NO_CONTENT')
	));
?>