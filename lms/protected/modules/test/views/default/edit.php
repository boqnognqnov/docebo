<?php /* @var $test LearningTest */
$this->breadcrumbs[] = $test->title;
$this->breadcrumbs[] = Yii::t('standard', '_MOD');

$typeClass = "dev-desktop-tablet";

if($courseModel){
	$_backUrl = PluginManager::isPluginActive('ClassroomApp') && $this->courseModel->isClassroomCourse()
		? Docebo::createLmsUrl('ClassroomApp/session/manageLearningObjects', array('course_id' => $this->getIdCourse()) )
		: $this->createUrl('//player/training/index', array('course_id' => $this->getIdCourse()) );
} else if($centralRepoContext){
	$_backUrl = Docebo::createAbsoluteLmsUrl('centralrepo/centralRepo/index');
}

?>

<div id="test-editor-main-container" class="l-testpoll">

	<h2 class="subtitle">
		<a class="docebo-back" href="<?php echo $_backUrl; ?>">
			<span><?= Yii::t('standard', '_BACK') ?></span>
		</a>
		<?php echo Yii::t('standard', '_MOD'); ?>
	</h2>
	<br/>
	<ul id="test-edit-main-actions" class="docebo-bootcamp clearfix">
		<li>
			<a href="<?=$this->createUrl('testOptions', array('id_test' => (int)$idTest, 'course_id' => $this->getIdCourse(), 'opt' => $centralRepoContext ? 'centralrepo' : ''))?>" data-helper-title="<?php echo Yii::t('test', '_TEST_MODALITY_CONTENT') ?>" data-helper-text="<?php echo Yii::t('helper', 'Test options') ?>">
				<span class="i-sprite is-list large"></span>
				<span class="i-sprite is-list large white"></span>
				<h4 class="helper-title"><?= Yii::t('test', '_TEST_MODALITY_CONTENT') ?></h4>
			</a>
		</li>
		<li>
			<a href="<?=$this->createUrl('timeAdministration', array('id_test' => (int)$idTest, 'course_id' => $this->getIdCourse(), 'opt' => $centralRepoContext ? 'centralrepo' : ''))?>" data-helper-title="<?php echo Yii::t('test', '_TEST_COMPILE_TIME') ?>" data-helper-text="<?php echo Yii::t('helper', 'Time administration') ?>">
				<span class="i-sprite is-clock large"></span>
				<span class="i-sprite is-clock large white"></span>
				<h4><?= Yii::t('test', '_TEST_COMPILE_TIME') ?></h4>
			</a>
		</li>
		<li>
			<a href="<?=$this->createUrl('pointsAdministration', array('id_test' => (int)$idTest, 'course_id' => $this->getIdCourse(), 'opt' => $centralRepoContext ? 'centralrepo' : ''))?>" data-helper-title="<?php echo Yii::t('test', '_TEST_POINT_MANAGEMENT') ?>" data-helper-text="<?php echo Yii::t('helper', 'Score management') ?>">
				<span class="i-sprite is-edit large"></span>
				<span class="i-sprite is-edit large white"></span>
				<h4><?= Yii::t('test', '_TEST_POINT_MANAGEMENT') ?></h4>
			</a>
		</li>
		<li>
			<a href="<?=$this->createUrl('feedbackManagement', array('id_test' => (int)$idTest, 'course_id' => $this->getIdCourse(), 'opt' => $centralRepoContext ? 'centralrepo' : ''))?>" data-helper-title="<?php echo Yii::t('test', '_FEEDBACK_TEXT') ?>" data-helper-text="<?php echo Yii::t('helper', 'Feedback management') ?>">
				<span class="i-sprite is-square-check large"></span>
				<span class="i-sprite is-square-check large white"></span>
				<h4><?= Yii::t('test', '_FEEDBACK_MANAGEMENT') ?></h4>
			</a>
		</li>
		<li>
			<a href="<?=$this->createUrl('importAsGift', array('id_test' => (int)$idTest, 'course_id' => $this->getIdCourse(), 'opt' => $centralRepoContext ? 'centralrepo' : ''))?>" data-helper-title="<?php echo Yii::t('test', 'Import as GIFT') ?>" data-helper-text="<?php echo Yii::t('helper', 'Import as GIFT') ?>">
				<span class="i-sprite is-putin large"></span>
				<span class="i-sprite is-putin large white"></span>
				<h4><?= Yii::t('test', 'Import as GIFT') ?></h4>
			</a>
		</li>
		<li>
			<a href="<?=$this->createUrl('exportAsGift', array('id_test' => (int)$idTest, 'course_id' => $this->getIdCourse(), 'opt' => $centralRepoContext ? 'centralrepo' : ''))?>" data-helper-title="<?php echo Yii::t('test', 'Export as GIFT') ?>" data-helper-text="<?php echo Yii::t('helper', 'Export as GIFT') ?>">
				<span class="i-sprite is-pullout large"></span>
				<span class="i-sprite is-pullout large white"></span>
				<h4><?= Yii::t('test', 'Export as GIFT') ?></h4>
			</a>
		</li>
		<li class="helper">
			<a href="#">
				<span class="i-sprite is-circle-quest large"></span>
				<h4 class="helper-title"></h4>
				<p class="helper-text"></p>
			</a>
		</li>
	</ul>
	<div class="clearfix"></div>

	<div class="row-fluid">
		<div class="span8">
			<!--<h2 class="subtitle test-editor-title"><?= $test->title ?></h2>-->
			<a id="edit-title" href="<?=$this->createUrl('axEditTest', array('id_test' => (int)$idTest, 'opt' => $centralRepoContext ? 'centralrepo' : ''))?>" data-dialog-class="edit-lo-dialog" class="ajax open-dialog" title="<?=Yii::t('standard', 'Edit test')?>">
				<h2 id="test-title-container" class="subtitle test-editor-title">
					<span id="test-title"><?= $test->title ?></span>
					<span class="objlist-sprite p-sprite edit-black"></span>
				</h2>
			</a>
		</div>
		<div class="span4">
			<div class="dropdown-docebo dropdown pull-right">
				<!-- Toggle -->
				<i class="course-device-type devices-sprite <?= $typeClass ?>">&nbsp;</i>
				<a href="#" class="btn-docebo green big dropdown-toggle" data-toggle="dropdown">
					<span class="i-sprite is-list white"></span>
					<?php echo Yii::t('test', '_TEST_ADDQUEST'); ?>
					<span class="caret white"></span>
				</a>
				<ul class="dropdown-menu">
					<li style="border-bottom: 1px solid #eee;"><a href="<?=Docebo::createAdminUrl('questionBank/import', array('idTest' => $idTest, 'idOrg' => isset($object['idOrg']) ? $object['idOrg'] : null, 'idCourse' => $this->getIdCourse()))?>" class="open-dialog" rel="import-questions"><i class="fa fa-database"></i>&nbsp;&nbsp;<?=Yii::t('test', 'Import from Question Bank')?></a></li>
					<?php
						foreach (LearningTestquest::getQuestionTypesTranslationsList($courseModel) as $questionType => $questionTranslation) {
							//$questionController = 'question'.ucfirst(preg_replace("/\_(.)/e", "strtoupper('\\1')", $questionType));
							//$questionUrl = $this->createUrl($questionController.'/createQuestion', array('id_test' => (int)$idTest, 'course_id' => $this->getIdCourse()));
							$questionUrl = $this->createUrl('question/create', array(
									'id_test' => (int)$idTest,
									'course_id' => $this->getIdCourse(),
									'question_type' => $questionType,
									'opt' => $centralRepoContext ? 'centralrepo' : ''
							));
							echo '<li>';
							echo '<a href="'.$questionUrl.'">'.$questionTranslation.'</a>';
							echo '</li>';
						}
					?>
				</ul>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<br/>

	<div id="test-editor-question-list-container">
		<div class="row-fluid">
			<div class="span12">

				<table id="test-editor-question-list"></table>
			</div>
		</div>
	</div>

	<br />
	<br />
</div>

<?php
	$scores = array();
	$list = $test->getQuestions(true);
	$pages = array();
	if (!empty($list)) {
		foreach ($list as $question) {
			$qm = CQuestionComponent::getQuestionManager($question->type_quest);
			$score = (!empty($qm) ? $qm->getMaxScore($question->idQuest) : false); //NOTE: $qm shouldn't be empty
			$scores['_'.$question->idQuest] = ($score !== false ? round($score, 2) : '');
			if (!in_array($question->page, $pages)) {
				$pages[] = $question->page;
			}
		}
	}
	$numQuestions = count($list);
	$numPages = count($pages);
?>


<script type="text/javascript">
/*<![CDATA[*/

<?php $translations = LearningTestquest::getQuestionTypesTranslationsList(); ?>

var QuestionTypes = {
	<?=LearningTestquest::QUESTION_TYPE_CHOICE?>: {name: "<?=addslashes($translations[LearningTestquest::QUESTION_TYPE_CHOICE])?>"},
	<?=LearningTestquest::QUESTION_TYPE_CHOICE_MULTIPLE?>: {name: "<?=addslashes($translations[LearningTestquest::QUESTION_TYPE_CHOICE_MULTIPLE])?>"},
	<?=LearningTestquest::QUESTION_TYPE_EXTENDED_TEXT?>: {name: "<?=addslashes($translations[LearningTestquest::QUESTION_TYPE_EXTENDED_TEXT])?>"},
	<?=LearningTestquest::QUESTION_TYPE_TEXT_ENTRY?>: {name: "<?=addslashes($translations[LearningTestquest::QUESTION_TYPE_TEXT_ENTRY])?>"},
	<?=LearningTestquest::QUESTION_TYPE_INLINE_CHOICE?>: {name: "<?=addslashes($translations[LearningTestquest::QUESTION_TYPE_INLINE_CHOICE])?>"},
	<?=LearningTestquest::QUESTION_TYPE_FITB?>: {name: "<?=addslashes($translations[LearningTestquest::QUESTION_TYPE_FITB])?>"},
	<?=LearningTestquest::QUESTION_TYPE_UPLOAD?>: {name: "<?=addslashes($translations[LearningTestquest::QUESTION_TYPE_UPLOAD])?>"},
	<?=LearningTestquest::QUESTION_TYPE_ASSOCIATE?>: {name: "<?=addslashes($translations[LearningTestquest::QUESTION_TYPE_ASSOCIATE])?>"},
	<?=LearningTestquest::QUESTION_TYPE_TITLE?>: {name: "<?=addslashes($translations[LearningTestquest::QUESTION_TYPE_TITLE])?>"},
	<?=LearningTestquest::QUESTION_TYPE_BREAK_PAGE?>: {name: "<?=addslashes($translations[LearningTestquest::QUESTION_TYPE_BREAK_PAGE])?>"}
};

var QuestionScores = <?php
	echo CJavaScript::encode($scores);
?>;

var QuestionCategories = <?php
	$list = LearningQuestCategory::model()->findAll();
	$categories = array();
	if (!empty($list)) {
		foreach ($list as $category) {
			$categories['_'.$category->idCategory] = array(
				'name' => $category->name,
				'description' => strip_tags($category->textof),
				'author' => (int)$category->author
			);
		}
	}
	echo CJavaScript::encode($categories);
?>;

var Langs = {
	_BREAK_PAGE: '---- <?= strtoupper($translations[LearningTestquest::QUESTION_TYPE_BREAK_PAGE]); ?> ----',
	_QUEST_IGNORED: ' (<?= addslashes(Yii::t('test', 'This question will be ignored due to test configurations')); ?>)'
};
var baseUpdateUrl = <?php echo CJavaScript::encode($this->createUrl('question/update', array('id_test' => $idTest, 'course_id' => $this->getIdCourse(), 'opt' => $centralRepoContext ? 'centralrepo' : ''))); ?>;
var baseDeleteUrl = <?php echo CJavaScript::encode($this->createUrl('question/axDelete', array('id_test' => $idTest, 'course_id' => $this->getIdCourse(), 'opt' => $centralRepoContext ? 'centralrepo' : ''))); ?>;

$(document).ready(function(){
	$('#import-form-grid').find('.doceboPager li').live('click', function (e) {
			if(!(this).hasClass('first')){
				e.preventDefault();
				e.stopPropagation();
				e.stopImmediatePropagation();

				$.fn.yiiGridView.update("bank-import-grid", {
					data:$(this).find("a").attr('href')
				});
			}
	});

	$('#bank-import-grid').find('.items tr td').live('click', function (e) {
		if (!$(this).hasClass('checkbox-column')) {
			e.preventDefault();
			e.stopPropagation();
			e.stopImmediatePropagation();
		}else if($(this).hasClass('checkbox-column')){
			e.stopImmediatePropagation();
			var checkbox = $(this).find(':checkbox');
			$(this).parent().toggleClass('selected');
			checkbox.attr('checked',$(this).parent().hasClass('selected'));
			updateSelectionCounter();
		}



	});

	try {

		$('#test-edit-main-actions').doceboBootcamp();
		$(document).delegate("#import-questions", "dialog2.content-update", function() {
			var e = $(this), autoclose = e.find("a.auto-close");
			if (autoclose.length > 0) {
				window.location.reload(true); //TMP fiximport-questions
			}
		});
		$(document).on("dialog2.content-update", ".modal", function () {
			var res = $(this).find("a.auto-close.success");
			if (res.length > 0) {
				$('#test-title').html(res.data('new_title'));
			}
		});

		//create questions table
		$('#test-editor-question-list').dataTable({
			bProcessing: true,
			bPaginate: false,
			//bServerSide: true,
			bFilter: false,
			sAjaxSource: '<?php echo $this->createUrl('axGetQuestionsList', array('id_test' => (int)$idTest, 'course_id' => $this->getIdCourse(), 'opt' => $centralRepoContext ? 'centralrepo' : '')); ?>',
			//aaData: Questions,
			//bJQueryUI: true,
			sDom: 'rt',
			//iDisplayLength: 15,
			oLanguage: {
				sEmptyTable: <?=CJSON::encode(Yii::t('standard', '_EMPTY_SELECTION'))?>,
				sProcessing: <?=CJSON::encode(Yii::t('standard', '_LOADING').'...')?>,
				sLoadingRecords: <?=CJSON::encode(Yii::t('standard', '_LOADING').'...')?>
			},
			fnCreatedRow: function( nRow, aData, iDataIndex ) {
				$(nRow).attr('id', 'questions-list-row-'+aData.id_question);
			},
			/*fnDrawCallback: function(oSettings) {
				try {
				var rowIndex;
				$('#test-editor-question-list').find('tbody tr').each(function() {
					$(this).find('td').eq(1).find('span').html(""+this.rowIndex);
				});
				} catch(e) {}
			},*/
			aoColumns: [
				{
					sTitle: "",
					mData: "id_question",
					bSortable: false,
					sClass: "img-cell",
					mRender: function(data, type, row) {
						return '<span class="p-sprite drag-small-black"></span>';
					}
				},
				{
					sTitle: "",
					bSortable: false,
					mData: "sequence",
					sClass: "img-cell"/*,
					mRender: function(data, type, row) {
						return '<span></span>';
					}*/
				},
				{
					sTitle: "<?php
						echo Yii::t('test', '_QUEST').'&nbsp;&nbsp;'
							.'<span class=\\"table-title-info\\">'
							.'('.Yii::t('test', '_TEST_CAPTION', array(
									'%tot_element%' => $numQuestions,
									'%tot_page%' => $numPages
								)).')'
							.'</span>';
					?>",
					mData: "question",
					bSortable: false,
					sClass: "pre text-left", // was nowrap class
					mRender: function(data, type, row) {
						if (row.type == 'break_page')
						{
							if(row.quest_ignored)
								return Langs._BREAK_PAGE+Langs._QUEST_IGNORED;
							else
								return Langs._BREAK_PAGE;
						}

						if(row.quest_ignored)
							return data+Langs._QUEST_IGNORED;
						else
							return data;
					}
				},
				{
					sTitle: "<?php echo Yii::t('test', '_TEST_PAGES'); ?>",
					mData: "page",
					bSortable: false,
					sClass: "text-center",
					mRender: function(data, type, row) {
						return '<span id="page-number-'+row.id_question+'">'+data+'</span>';
					}
				},
				{
					sTitle: "<?php echo Yii::t('standard', '_CATEGORY'); ?>",
					mData: "id_category",
					bSortable: false,
					sClass: "text-left",
					mRender: function(data, type, row) {
						if (row.type != 'break_page') {
							if (data && data > 0 && QuestionCategories['_'+data]) {
								return QuestionCategories['_'+data].name;
							}
						}
						return '';
					}
				},
				{
					sTitle: "<?php echo Yii::t('standard', '_SCORE'); ?>",
					mData: "score",
					bSortable: false,
					sClass: "text-center",
					mRender: function(data, type, row) {
						var output;
						switch (row.type) {
							case 'break_page':
							case 'title':
							case 'extended_text':
							case 'upload': { output = ''; } break;
							default: { output = (QuestionScores['_'+row.id_question] || ""); } break;
						}
						return output;
					}
				},
				{
					sTitle: "<?php echo Yii::t('standard', '_TYPE'); ?>",
					mData: "type",
					sClass: "text-left question-type",
					bSortable: false,
					mRender: function(data, type, row) {
						return QuestionTypes[data].name;
					}
				},
				{
					sTitle: "",
					mData: "id_question",
					bSortable: false,
					sClass: "img-cell",
					mRender: function(data, type, row) {
						if (row.type != 'break_page') {
							return '<a id="question-update-'+row.id_question+'" href="'+baseUpdateUrl+'&id_question='+row.id_question+'">'
								+'<span class="objlist-sprite p-sprite edit-black"></span>'
								+'</a>';
						}
						return '';
					}
				},
				{
					sTitle: "",
					mData: "id_question",
					bSortable: false,
					sClass: "img-cell",
					mRender: function(data, type, row) {
						var icon_class = 'objlist-sprite p-sprite cross-small-red';
						if (row.is_bank == 1)
							icon_class = 'fa fa-unlink';
						return '<a id="question-delete-'+row.id_question+'" href="'+baseDeleteUrl+'&id_question='+row.id_question+'">'
							+'<span class="delete-icon '+icon_class+'"></span>'
							+'</a>'
							+'</div>';
					},
					fnCreatedCell: function(nTd, sData, oData, iRow, iCol) {
						/*$(nTd).controls();*/
						$(nTd).delegate('.delete-icon', 'click', function(e) {
							e.preventDefault();

							bootbox.dialog(Yii.t('standard', '_AREYOUSURE'), [{
									label: Yii.t('standard', '_CONFIRM'),
									'class': 'btn-docebo big green',
									callback: function() {
										$.ajax({
											url: $('#question-delete-'+oData.id_question).attr('href'),
											type: 'post',
											dataType: 'json',
											data: {
												confirm: 1
											}
										}).done(function (o) {
											if (o.success) {
												var nTr, t = $('#test-editor-question-list').dataTable();
												if (o.data.id_question) {
													$nTr = $('#questions-list-row-'+o.data.id_question);
													if ($nTr) { t.fnDeleteRow(t.fnGetPosition($nTr[0])); }
												}
											} else {
												//...
											}
										}).fail(function(o) {
											//..
										});
									}
								}, {
									label: Yii.t('standard', '_CANCEL'),
									'class': 'btn-docebo big black',
									callback: function() { }
								}],
								{ header: Yii.t('standard', '_DEL') }
							);
						});
					}
				}
			]
		}).rowReordering({
			sURL: '<?php echo $this->createUrl('axReorderQuestion', array('id_test' => $idTest, 'course_id' => $this->getIdCourse(), 'opt' => $centralRepoContext ? 'centralrepo' : '')); ?>',
			handle: '.drag-small-black',
			iIndexColumn: 1
		});

	} catch(e) {
		//alert(e);
	}
});

/*<![CDATA[*/
</script>

<style>
	.fa.fa-unlink {
		color: #d90000;
		font-size: 16px;
	}
    .pre {
        white-space: pre;
    }

	.pre > p {
		margin: 5px 0px 5px 0px;
	}
</style>