<h1>
	<?php echo $player->getTest()->title; ?>
</h1>
<div id="test-questions-main-container">
<?php






echo CHtml::form(
	$this->createUrl('play', array('id_test' => $idTest, 'course_id' => $this->getIdCourse())), 
	'POST', 
	array('id' => 'test-questions-form')
);


$i = 0;
foreach ($questions as $question) {
	
	
	
	//TO DO: make a playQuestion function for every question type.
	if ($questionPlayer = CQuestionComponent::getQuestionManager($question->type_quest)) {

		$questionPlayer->setController($this);

		if ($questionPlayer->isInteractive()) {
			
			$i++;
	
			$questionIndex = $i; //TO DO: fix this
	
			echo '<div id="question-container-'.$question->idQuest.' '.(($i % 2) > 0 ? 'odd' : 'even').'">';

			echo '<div id="question-index-'.$question->idQuest.'">';
			echo $questionIndex.' ) '.$question->title_quest;
			echo '</div>';

			echo '<div id="question-type-'.$question->idQuest.'">';
			echo Yii::t('test', '_QUEST_'.strtoupper($question->type_quest));
		}

		$questionPlayer->play($question->getPrimaryKey());
		
		if ($questionPlayer->isInteractive()) {
			echo '</div>';
		}
	} else {
		//throw ...
	}

}



?>
	
<br />
<br />

<div id="test-play-buttons">
	<?php
	
	
	?>
</div>

<?php


echo CHtml::closeTag('form');

?>
</div>