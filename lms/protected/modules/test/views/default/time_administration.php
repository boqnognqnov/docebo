<?php /* @var $test LearningTest */
$this->breadcrumbs[] = $test->title;
$this->breadcrumbs[Yii::t('standard', '_MOD')] = $backUrl;
$this->breadcrumbs[] = Yii::t('test', '_TEST_COMPILE_TIME');
?>
<div class="l-testpoll">

	<?php

	$test = LearningTest::model()->findByPk($idTest);

	echo CHtml::form(
		'',
//		$this->createUrl('timeAdministration', array('course_id' => $this->getIdCourse(), 'id_test' => $this->getIdTest())),
		'POST',
		array('id' => 'time-administration-form')
	);

	echo CHtml::hiddenField('id_test', (int)$idTest, array('id' => 'id-test-input'));
	echo CHtml::hiddenField('jsonQuestions', '', array('id' => 'questions-json'));


	//calculated time limit policy from test AR info
	$newTime = '';

	?>

	<h2 class="subtitle">
		<a class="docebo-back" href="<?echo $backUrl; ?>">
			<span><?= Yii::t('standard', '_BACK') ?></span>
		</a>
		<?php echo Yii::t('test', '_TEST_COMPILE_TIME'); ?>
	</h2>

	<br />

	<div class="row-fluid">

		<div class="span12">

			<div class="block-hr"></div>
			<div class="block odd">
				<div class="block-header"><?php echo Yii::t('test', '_TEST_TM_ONE');?></div>
				<?php
				if (!$test->time_dependent) {
					$timeLimit = LearningTest::TIME_NO;
					//$jsInputs = '1';
				} else {
					if($test->time_dependent == LearningTest::TIME_YES)
					{
						$timeLimit = LearningTest::TIME_YES;
						//$jsInputs = '2';
					}
					else
					{
						$timeLimit = LearningTest::TIME_YES_QUEST;
						//$jsInputs = '3';
					}
				}
				$jsInputs = '3'; //when choosing time for single questions, start with manual selection by default every time
				?>
				<div class="block-content">
					<?php
						$checked = $timeLimit == LearningTest::TIME_NO;
						echo CHtml::label(
							CHtml::radioButton('time_limit', $checked, array('value' => LearningTest::TIME_NO, 'id'=>'opt1', 'class'=>'toggle-options'))
							.'&nbsp;'
							.Yii::t('test', '_TEST_TIME_NO'), 'opt1', array('class'=>'radio')
						);
					?>
					<br>

					<?php
						$checked = $timeLimit == LearningTest::TIME_YES;
						echo CHtml::label(
							CHtml::radioButton('time_limit', $checked, array('value' => LearningTest::TIME_YES, 'id'=>'opt2', 'class'=>'toggle-options'))
							.'&nbsp;'
							.Yii::t('test', '_TEST_TIME_YES'), 'opt2', array('class'=>'radio')
						);
					?>
					<div class="togglable-options" id="opt2-box" style="display:<?php echo ($checked ? 'block' : 'none'); ?>;">
						<label class="radio" for="time-assigned-input">
							<strong><?php echo Yii::t('test', '_TEST_TM_TWO'); ?></strong>
							<br>
							<?php echo Yii::t('standard', '_TOTAL_TIME'); ?>:
							&nbsp;
							<strong><?php echo $test->time_assigned; ?></strong>
							<br>
							<?php
								echo Yii::t('test', '_TEST_TM2_NEWTIME').':&nbsp;';
								echo CHtml::textField(
									'time_assigned',
									$test->time_assigned,
									array('id' => 'time-assigned-input', 'class'=>'input-small', 'pattern' => '[0-9]+')
								);
								echo '&nbsp;'.Yii::t('standard', '_SECONDS'); ?>
						</label>
					</div>
					<br>

					<?php
						$checked = $timeLimit == LearningTest::TIME_YES_QUEST;
						echo CHtml::label(
							CHtml::radioButton('time_limit', $checked, array('value' => LearningTest::TIME_YES_QUEST, 'id'=>'opt3', 'class'=>'toggle-options'))
							.'&nbsp;'
							.Yii::t('test', '_TEST_TIME_YES_QUEST'), 'opt3', array('class'=>'radio')
						);
					?>
					<div class="togglable-options" id="opt3-box" style="display:<?php echo ($checked ? 'block' : 'none'); ?>;">
						<?php
							$questions = $test->getQuestions();
							$oldTotalTime = 0;
							if (!empty($questions)) {
								foreach ($questions as $question) {
									$oldTotalTime += $question->time_assigned;
								}
							}
						?>
						<div class="row-fluid">
							<div class="span5">
								<label class="radio" for="">
									<strong><?php echo Yii::t('test', '_TEST_TM_TWO'); ?></strong>
									<br>
									<?php echo Yii::t('standard', '_TOTAL_TIME'); ?>:
									&nbsp;
									<strong><span id="total-time-seconds"><?php echo $oldTotalTime; ?></span>
									&nbsp;
									<?php echo Yii::t('standard', '_SECONDS'); ?></strong>
									<br>
									<?php
										echo Yii::t('test', '_TEST_TM2_NEWTIME').':&nbsp;';
										echo CHtml::textField('new_time', $oldTotalTime, array('id' => 'new-time-input', 'class' => 'input-small', 'pattern' => '[0-9]+'));
										echo '&nbsp;'.Yii::t('standard', '_SECONDS');
									?>
								</label>

							</div>
							<div class="span7">
								<label for=""><?php echo Yii::t('test', '_TEST_PM_SUBD_BY'); ?></label>

								<?php
								//start by default with manual assignment
								$radio1 = CHtml::radioButton('time_assignment', ($jsInputs == '1'), array('id'=>'time-assignment-1', 'value' => 0));
								$radio2 = CHtml::radioButton('time_assignment', ($jsInputs == '2'), array('id'=>'time-assignment-2', 'value' => 1));
								$radio3 = CHtml::radioButton('time_assignment', ($jsInputs == '3'), array('id'=>'time-assignment-3', 'value' => 2));
								echo CHtml::label($radio1.'&nbsp;'.Yii::t('test', '_TEST_PM_DIFFICULT'), 'time-assignment-1', array('class'=>'radio'));
								echo CHtml::label($radio2.'&nbsp;'.Yii::t('test', '_TEST_TM2_EQUALTOALL'), 'time-assignment-2', array('class'=>'radio'));
								echo CHtml::label($radio3.'&nbsp;'.Yii::t('test', '_TEST_TM2_MANUAL'), 'time-assignment-3', array('class'=>'radio'));
								?>

							</div>
						</div>


						<?php
							if (!empty($questions)) {
								$sequence = 0;
						?>
						<div id="questions-table-container">
							<table class="table" id="questions-table">
								<thead>
								<tr>
									<th class="text-center"><?php echo Yii::t('test', '_TEST_QUEST_ORDER'); ?></th>
									<th><?php echo Yii::t('test', '_QUEST'); ?></th>
									<th><?php echo Yii::t('item', '_MIME'); ?></th>
									<th><?php echo Yii::t('test', '_QUEST_TM2_SETDIFFICULT'); ?></th>
									<th><?php echo Yii::t('test', '_QUEST_TM2_SETTIME'); ?></th>
								</tr>
								</thead>
								<tbody>
							<?php

									foreach ($questions as $question) {
										$sequence++;
										$idQuestion = $question->getPrimaryKey();
										$namePrefix = 'questions['.$idQuestion.']';
										echo '<tr>';
										echo '<td class="text-center">'.$sequence.'</td>';
										echo '<td>'.Docebo::formatQuestionText($question->title_quest, 80).'</td>';
										echo '<td>'.Yii::t('test', '_QUEST_'.strtoupper($question->type_quest));
										echo '<td>'.CHtml::dropDownList(
											$namePrefix.'[difficulty]',
											$question->difficult,
											LearningTestquest::getDifficultyList(),
											array('id' => 'question-difficulty-'.$idQuestion, 'class' => 'input-small')
										).'</td>';
										echo '<td>'.CHtml::textField(
											$namePrefix.'[time]',
											$question->time_assigned,
											array('id' => 'question-time-'.$idQuestion, 'class' => 'input-small', 'pattern' => '[0-9]+')
										).'</td>';
										echo '<td><i class="muted">('.Yii::t('standard', '_SECONDS').')</i></td>';
										echo '</tr>';
									}

								?>
									<tr class="tbl-row-total">
										<td colspan="3"></td>
										<td class="text-center"><strong><?php echo Yii::t('standard', '_TOTAL_TIME'); ?></strong></td>
										<td><strong id="total-time"></strong></td>
										<td><i class="muted">(<?php echo Yii::t('standard', '_SECONDS'); ?>)</i></td>
									</tr>
								<?
									}

								?>

								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>
			<div class="block-hr"></div>

		</div>

		<div class="form-actions">
			<input type="submit" name="save" class="span2 btn-docebo green big"  value="<?php echo Yii::t('standard', '_SAVE'); ?>">
			<input type="submit" name="undo" class="span2 btn-docebo black big"  value="<?php echo Yii::t('standard', '_CANCEL'); ?>">
		</div>
		
	</div>
	<?php
	echo CHtml::closeTag('form');
	?>
</div>
<script type="text/javascript">

if (!String.prototype.trim) {
	String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, ''); };
}


$(document).ready(function() {
	$('input').styler();
try {


	$('#time-administration-form').submit(function() {
		var data = $('input[name^="questions"], select[name^="questions"]').serializeFormJSON();
		arrangeJson(data);
		$("#questions-json").val(JSON.stringify(data));
		$('input[name^="questions"], select[name^="questions"]').prop('disabled', true);
	});

	var $form = $('#time-administration-form');


	$form.find('.toggle-options').on('change', function() {
		var $this = $(this), box2 = $('#opt2-box'), box3 = $('#opt3-box');
		if ($this.attr('checked')) {
			switch ($this.attr('id')) {
				case 'opt1': { box2.hide(); box3.hide(); } break;
				case 'opt2': { box2.show(); box3.hide(); } break;
				case 'opt3': { box2.hide(); box3.show(); } break;
			}
		}
	});

	var times = $('input[id^=question-time-]');
	var getTotalTime = function() {
		var totalTime = 0;
		times.each(function() {
			var value = $(this).val(), regexp = /^[0-9]+$/;
			if (regexp.test(value)) {
				totalTime += parseInt(value);
			} else {
				$(this).val('0');
			}
		});
		return totalTime;
	};
	var updateTotalTime = function() {
		var t = getTotalTime();
		$('#total-time').html(''+(t || '0'));
	};

	var getTotalDifficulty = function() {
		var output = 0;
		$('select[id^=question-difficulty-]').each(function() {
			output += parseInt($(this).val());
		});
		return output;
	};

	var updateDifficultyValues = function() {
		var newTime = $('#new-time-input').val(), regexp = /^[0-9]+$/;
		newTime = newTime.trim();
		if (regexp.test(newTime)) {
			newTime = parseInt(newTime);
		} else {
			newTime = 0;
			$('#new-time-input').val('0');
		}
		var totalDifficulty = getTotalDifficulty();
		$('input[id^=question-time-]').each(function() {
			if (totalDifficulty <= 0) { return; }
			var index = $(this).attr('id').replace('question-time-', '');
			var difficulty = $('#question-difficulty-'+index).val();
			$(this).val(Math.floor((newTime / totalDifficulty) * difficulty));
		});
		updateTotalTime();
	}

	var setQuestionInputs = function(mode) {
		switch (''+mode) {
			case '1': {
				$('#questions-table-container').css('display', 'block');
				$('#new-time-input').attr('disabled', false);
				$('select[id^=question-difficulty-]').attr('disabled', false);
				$('input[id^=question-time-]').attr('disabled', true);
				$('#new-time-input').on('change', function(e) { updateDifficultyValues(); });
				$('select[id^=question-difficulty-]').on('change', function(e) { updateDifficultyValues(); });
				updateDifficultyValues();
			} break;
			case '2': {
				$('#questions-table-container').css('display', 'none');
				$('#new-time-input').attr('disabled', false);
				$('select[id^=question-difficulty-]').attr('disabled', true);
				$('input[id^=question-time-]').attr('disabled', true);
				$('#new-time-input').off('change');
				$('select[id^=question-difficulty-]').off('change');
			} break;
			case '3': {
				$('#questions-table-container').css('display', 'block');
				$('#new-time-input').attr('disabled', true);
				$('select[id^=question-difficulty-]').attr('disabled', false);
				$('input[id^=question-time-]').attr('disabled', false);
				$('#new-time-input').off('change');
				$('select[id^=question-difficulty-]').off('change');
			} break;
		}
	};
	setQuestionInputs('<?php echo $jsInputs; ?>');

	$('input[id^=time-assignment-]').on('change', function(e) {
		var index = $(this).attr('id').replace('time-assignment-', '');
		setQuestionInputs(index);
	});

	times.on('change', function(e) { updateTotalTime(); });
	updateTotalTime();
} catch(e) { /*alert(e);*/ }
});

</script>