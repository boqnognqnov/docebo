<?php
	$test = $this->test;
?>
<div class="l-testpoll review-answers">
	<h1 class="page-title">
		<a class="docebo-back" href="<?php echo $backUrl; ?>">
			<span><?= Yii::t('standard', '_BACK') ?></span>
		</a>
		<?php echo $test->title; ?>
	</h1>
	<div>
		<?php
		$this->renderPartial('application.modules.player.views.report._test_by_user', array(
			'organization' => $organization,
			'userId' => $userId,
			'testTrack' => $testTrack,
			'questionsLang' => $questionsLang,
			'test' => $test,
			'isTeacher' => false,
			'isPlaying' => true,
			'commonTrack' => $commonTrack
		), false, false);
		?>
	</div>
</div>