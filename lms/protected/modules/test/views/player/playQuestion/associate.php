<?
/* @var $this QuestionAssociate */
/* @var $associations array */
/* @var $answers array */
?>
<table>
	<tbody>
		<?php

			//check test settings
			$disabled = !$questionManager->canBeAnswered($idQuestion, Yii::app()->user->id, $idTest);

			// custom plugin set property disabled if test is in study mode
			$event = new DEvent($this, array('disabled' => $disabled));
			Yii::app()->event->raise('CheckForStudyMode', $event);
			if (!$event->shouldPerformAsDefault()) {
				$disabled = true;
			}

			if (!empty($answers)) {

				$options = array(0 => '--');
				if (!empty($associations)) {
					foreach ($associations as $association) {
						$options[$association->idAnswer] = CHtml::decode($association->answer);
					}
				}
				foreach ($answers as $answer) {
					$idAnswer = $answer->getPrimaryKey();
					$idFor = $questionManager->getInputId($idQuestion).'-'.$idAnswer;
					$selectedAnswer = (isset($previous[$idAnswer]) ? $previous[$idAnswer]->more_info : 0);
					$dropDown = CHtml::dropDownList(
						$questionManager->getInputName($idQuestion).'['.$idAnswer.']',
						$selectedAnswer,
						$options,
						array('id' => $idFor, 'disabled' => ($selectedAnswer > 0 ? $disabled : false)) //always enable not-answered associations
					);
					echo '<tr>';
					echo '<td>'.CHtml::label($answer->answer, $idFor).'</td>';
					echo '<td>'.$dropDown.'</td>';
					echo '</tr>';
				}
			}

		?>
	</tbody>
</table>