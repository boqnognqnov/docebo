<?php

$disabled = !$questionManager->canBeAnswered($idQuestion, Yii::app()->user->id, $idTest);

// custom plugin set property disabled if test is in study mode
$event = new DEvent($this, array('disabled' => $disabled));
Yii::app()->event->raise('CheckForStudyMode', $event);
if (!$event->shouldPerformAsDefault()) {
	$disabled = true;
	$eventIsActive = true;
}

$checked = array();
$eventIsActive == true ? $userAnswer = array() : $userAnswer = $questionManager->recoverUserAnswer($idQuestion, Yii::app()->user->id, $idTest);
foreach ($userAnswer as $answer) {
	if ($answer->user_answer > 0) $checked[] = $answer->idAnswer;
}

$index = 0;
if (is_array($answers)) {
	foreach ($answers as $answer) {
		$index++;
		$idFor = $questionManager->getInputId($idQuestion).'-'.$index;
		$idAnswer = $answer['id_answer'];
		($answer['is_correct'] === true && $eventIsActive == true) ? $span = ' <span class="p-sprite check-solid-small-green"></span>' : $span = '';
		echo CHtml::label(
			CHtml::radioButton(
				$questionManager->getInputName($idQuestion).'[]',
				in_array($idAnswer, $checked),
				array('id' => $idFor, 'value' => $idAnswer, 'disabled' => $disabled)
			).' '.Docebo::nbspToSpace($answer['answer']).$span,
			$idFor,
			array('class' => 'radio')
		);

	}

	if(Settings::get('no_answer_in_test', 'off') == 'on')
	{
		$index++;
		$idFor = $questionManager->getInputId($idQuestion).'-'.$index;

		echo CHtml::label(
				CHtml::radioButton(
					$questionManager->getInputName($idQuestion).'[]',
					in_array(0, $checked),
					array('id' => $idFor, 'value' => 0, 'disabled' => $disabled)
				).' '.Yii::t('standard', '_NO_ANSWER'),
				$idFor,
				array('class' => 'radio')
			);
	}
}