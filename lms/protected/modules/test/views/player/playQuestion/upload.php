<?php

$idQuestion = $question->getPrimaryKey();



//check test settings
$disabled = !$questionManager->canBeAnswered($idQuestion, Yii::app()->user->id, $idTest);

$userAnswer = $questionManager->recoverUserAnswer($idQuestion, Yii::app()->user->id , $idTest);
$value = (!empty($userAnswer) ? array_shift($userAnswer)->more_info : '');

// custom plugin set property disabled if test is in study mode
$event = new DEvent($this, array('disabled' => $disabled));
Yii::app()->event->raise('CheckForStudyMode', $event);
if (!$event->shouldPerformAsDefault()) {
	$disabled = true;
	$value = 'can\'t fill in study mode';
}

echo CHtml::fileField(
	$questionManager->getInputName($idQuestion), 
	$value, 
	array(
		'id' => $questionManager->getInputId($idQuestion),
		'disabled' => (!empty($value) ? $disabled : false) //if empty path is present in DB as answer, then consider the question as unanswered anyway
	)
);

?>