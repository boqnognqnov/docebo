<?php

$points = $totalUserScore + $this->track->bonus_score;
$testPassed = ($points >= $this->test->point_required) ? true : false;

?>
<div class="l-testpoll review">

	<h1 class="page-title"><?= $this->test->title ?></h1>
	<br/>

	<div class="row-fluid">
		<div class="span6">
			<div class="header-review">
				<img src="<?php echo $this->getPlayerAssetsUrl() . '/images/testpoll-img.png'; ?>"/>
			</div>
			<div class="header-review-description">
				<?php echo $this->test->description; ?>
			</div>
		</div>
		<div class="span6">
			<div class="block-hr"></div>
			<div class="review-result">
				<div class="row-fluid">
					<div class="span2 text-center"><?php
						echo ( $testFailed == LearningTesttrack::STATUS_NOT_PASSED
							? '<span class="p-sprite lo-failed-icon-big"></span>'
							: '<span class="p-sprite lo-passed-icon-big"></span>' );
						?>
					</div>
					<div class="span10">
						<p class="span7"><?php
							if ($countManual > 0 && $this->track->score_status != LearningTesttrack::STATUS_VALID) {
								echo '<span class="review-result-text">' . Yii::t('test', 'Waiting for final grade') . '</span>';
							}else{
								echo ( $testFailed == LearningTesttrack::STATUS_NOT_PASSED
									? '<span class="review-result-text">' . Yii::t('test', '_TEST_FAILED') . '</span>'
									: '<span class="review-result-text">' . Yii::t('test', '_TEST_COMPLETED') . '</span>' );
							}
						?></p>
						<div class="span5" style="font-size: 16px; text-align: right;"><?php
							// pass score
							if ($this->test->point_required > 0) {
								echo '<span class="test_score_note">' . Yii::t('test', '_TEST_REQUIREDSCORE_RESULT') . '</span> '
									. '<b>' . $this->test->point_required . '</b>'
									. ( $this->test->point_type == LearningTest::POINT_TYPE_PERCENT ? ' <span class="review-percentage">%</span>' : '');
							}
							?>
						</div>
						<!-- score -->
						<?php if ($this->test->show_score) : ?>

						<div class="row-fluid review-scores">
							<div class="span7">
								<?php
								$lang = Yii::app()->getLanguage();
								// user score
								$percentage = false;
								$score = ($totalUserScore+$this->track->bonus_score > 0)? $totalUserScore+$this->track->bonus_score : 0;

								$shortSpan = ($lang == 'de') ? 'span4' : 'span6';
								$longSpan = ($lang == 'de') ? 'span8' :'span6';


								switch ($this->test->point_type) {
									case LearningTest::POINT_TYPE_NORMAL : {

										echo '<span class="test_score_note '.$longSpan.' no-margin">' . Yii::t('test', '_TEST_TOTAL_SCORE') . '</span> '
											. '<div class="'.$shortSpan.' no-margin"><b class="' . ($testFailed == LearningTesttrack::STATUS_NOT_PASSED ? 'score-failed' : 'score-passed') . '">'
											. $score . ' / ' . $this->test->getMaxScore(Yii::app()->user->id) . '</b></div>';
										$percentage = false;
									};
										break;
									case LearningTest::POINT_TYPE_PERCENT : {
										echo '<span class="test_score_note '.$longSpan.' no-margin">' . Yii::t('test', '_TEST_TOTAL_SCORE') . '</span> '
											. '<div class="'.$shortSpan.' no-margin"><b class="' . ($testFailed == LearningTesttrack::STATUS_NOT_PASSED ? 'score-failed' : 'score-passed') . '">'
											. $score . '</b> <span class="review-percentage">%</span></div>';
										$percentage = true;
									};
										break;
								}



								?><br>
								<?php if ($this->test->show_users_avg) : ?>
									<?php
									$avg = TestUtils::getAverageScoresByTest($this->test->idTest, null, false);
									?>
									<span class="test_score_note span6 no-margin"><?=Yii::t('test', 'Average score')?>:</span>
									<div class="span6 no-margin">
									<b>
										<?=$avg?><?=($percentage ? '%' : '')?>
									</b>
									<?=($percentage ? '' : '<b> / '. $this->test->getMaxScore(Yii::app()->user->id).'</b>')?>
									</div>
								<?php endif; ?>
							</div>
							<?php if ($this->test->show_users_avg) : ?>
								<?php
								$standing = ($score >= $avg) ? Yii::t('test', 'above') : Yii::t('test', 'below');
								$completedCnt = LearningTesttrack::getUserCountByTest($this->test->idTest);
								$totalCnt = LearningCourseuser::getCountEnrolledByLevel($this->getIdCourse());
								?>
								<div class="span12 no-margin" style="font-size: 12px">
									<?=Yii::t('test', 'Right now <b>{completed_cnt}/{total_cnt}</b> have already completed the test <span {color}>and you are {standing} the average!</span>', array(
										'{completed_cnt}' => $completedCnt,
										'{standing}' => $standing,
										'{total_cnt}' => $totalCnt,
										'{color}' => ($score >= $avg) ? 'style="color: #6FCF6F;"' : '',
									));?>
								</div>
							<?php endif; ?>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<?php
				// manual score
				if ($countManual > 0 && $this->track->score_status != LearningTesttrack::STATUS_VALID) {
					echo '<p class="review-manual-score">'
						. Yii::t('test', '_TEST_MANUAL_SCORE')
						. ' <b>' . $manualScore . '</b> '
						. ($this->test->point_type == LearningTest::POINT_TYPE_PERCENT && $this->test->show_score ? ' <span class="review-percentage">%</span>' : '')
						. '</p>';
				}
				?>
			</div>
			<div class="block-hr"></div>
			<?php
				// Score per category
				if ($this->test->show_score_cat) {
					$categories = Yii::app()->db->createCommand()
								->select("c.idCategory, c.name, COUNT(q.idQuest) AS num_questions")
								->from(LearningTestquest::model()->tableName() . " q")
								->join(LearningTestQuestRel::model()->tableName()." qr", "qr.id_question = q.idQuest")
								->join(LearningTesttrackQuest::model()->tableName()." t", "t.idQuest = q.idQuest AND t.idTrack = ".$this->track->idTrack)
								->join(LearningQuestCategory::model()->tableName() . " c", "c.idCategory = q.idCategory")
								->where("qr.id_test = :id_test", array(':id_test' => $this->test->getPrimaryKey()))
								->andWhere("q.idCategory != 0")
								->group("c.idCategory")
								->order("c.name")
								->queryAll();
					if (!empty($categories)) {
						?>
						<br>
						<table class="table table-condensed">
							<tbody>
							<thead>
							<tr>
								<th>
									<?php echo Yii::t('test', '_TEST_QUEST_CATEGORY'); ?>
								</th>
								<th>
									<?php echo Yii::t('coursereport', '_TOT_QUESTION'); ?>
								</th>
								<th>
									<?php echo Yii::t('test', '_TEST_TOTAL_SCORE'); ?>
								</th>
							</tr>
							</thead>
							<tbody>
							<?php
							foreach($categories as $category) {
								echo '<tr>';
								echo '<td>' . $category['name'] . '</td>';
								echo '<td>' . $category['num_questions'] . '</td>';
								echo '<td>' . (isset($categoriesScore[$category['idCategory']]) ? $categoriesScore[$category['idCategory']] : '0').($this->test->point_type == LearningTest::POINT_TYPE_PERCENT ? ' <span class="review-percentage">%</span>' : '').'</td>';
								echo '</tr>';
							}
							?>
							</tbody>
						</table>
					<?php
					}
				}
				if (!empty($feedbackText)) :
					?>
					<br/>
					<div class="block-hr"></div>
					<p class="feedback-text"><?php echo $feedbackText; ?></p>
					<div class="block-hr"></div>
					<?php
				endif;

				$formUrl = ($this->courseModel->isClassroomCourse()
					? $this->createUrl('//player/training/session', array('course_id' => $this->getIdCourse()/*, 'id_session' => $this->getIdSession()*/)) //session id is automatically added to url in LoBaseController class
					: $this->createUrl('//player/training', array('course_id' => $this->getIdCourse()))
				);

				$reviewUrl = $this->createUrl('player/review', array('id_test' => $this->test->getPrimaryKey(), 'course_id' => $this->getIdCourse()));

				echo '<br/>'
					.'<div class="row-fluid">';

				echo '<div class="span6 text-center">'
					. '<a href="' . $formUrl . '" class="btn-docebo green verybig" id="backUrlLink">' . Yii::t('test', '_TEST_SAVEKEEP_BACK') . '</a>'
					. '</div>';

				// Show "View answers" button IF it is allowed (by TEST options)
				if ($testInfo->answersVisible(Yii::app()->user->id)) {
					echo '<div class="span6 text-center">'
						. '<a href="' . $reviewUrl . '" class="btn-docebo black verybig">' . Yii::t('test', '_TEST_REVIEW_ANSWER') . '</a>'
						. '</div>';
				}

				echo '</div>';
			?>
		</div>
	</div>
</div>

