<?php

?>
<div class="l-testpoll">
	<h1 class="page-title">
		<?php echo $test->title; ?>
	</h1>
	
	<div>
		<p>
		<?php echo Yii::t('standard', ($successful ? '_OPERATION_SUCCESSFUL' : '_OPERATION_FAILURE')); ?>
		</p>

		<div class="form-actions">
		<?php echo CHtml::link(
				Yii::t('standard', '_BACK'),
				$backUrl,
				array(
					'class' => 'span2 btn btn-docebo green big pull-right'
				)
			)?>
		</div>
	</div>
</div>