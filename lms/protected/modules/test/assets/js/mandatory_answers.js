
var Mandatory = {

	questions: [],

	init: function(q) {
		for (var i=0; i<q.length; i++) {
			this.configureQuestion(q[i].id, q[i].type);
		}
		this.check();
	},

	configureQuestion: function(id, type) {
		switch (type) {
			case 'choice': this.configureChoice(id); break;
			case 'inline_choice': this.configureInlineChoice(id); break;
			case 'choice_multiple': this.configureChoiceMultiple(id); break;
			case 'associate': this.configureAssociate(id); break;
			case 'extended_text': this.configureExtendedText(id); break;
			case 'text_entry': this.configureTextEntry(id); break;
			case 'upload': this.configureUpload(id); break;
		}
	},

	configureChoice: function(idQuestion) {
		this.questions.push({id: idQuestion, type: 'choice'});
		var answer = $('input[id^=question-'+idQuestion+'-]'), oScope = this;
		answer.on('click', function(e) {
			oScope.check();
		});
	},
	
	
	configureInlineChoice: function(idQuestion) {
		this.questions.push({id: idQuestion, type: 'inline_choice'});
		var answer = $('select[id^=question-'+idQuestion+']'), oScope = this;
		answer.on('change', function(e) {
			oScope.check();
		});
	},
	
	
	configureChoiceMultiple: function(idQuestion) {
		this.questions.push({id: idQuestion, type: 'choice_multiple'});
		var answer = $('input[id^=question-'+idQuestion+'-]'), oScope = this;
		answer.on('click', function(e) {
			oScope.check();
		});
	},
	

	configureAssociate: function(idQuestion) {
		this.questions.push({id: idQuestion, type: 'associate'});
		var answer = $('select[id^=question-'+idQuestion+'-]'), oScope = this;
		answer.on('change', function(e) {
			oScope.check();
		});
	},

	configureExtendedText: function(idQuestion) {
		this.questions.push({id: idQuestion, type: 'extended_text'});
		var answer = $('#question-'+idQuestion), oScope = this;
		answer.on('keyup', function(e) {
			oScope.check();
		});
	},

	configureTextEntry: function(idQuestion) {
		this.questions.push({id: idQuestion, type: 'text_entry'});
		var answer = $('#question-'+idQuestion), oScope = this;
		answer.on('keyup', function(e) {
			oScope.check();
		});
	},
	
	configureUpload: function(idQuestion) {
		this.questions.push({id: idQuestion, type: 'upload'});
		var answer = $('#question-'+idQuestion), oScope = this;
		answer.on('change', function(e) {
			oScope.check();
		});
	},


	check: function() {
		/**
		 * NOTE: it seems that setTimeout() function calls have been used to handle an IE8 issue. Anyway they are not
		 * working properly on other browsers (expecially when multiple questions are displayed in the same page).
		 */
		var idq, type, i, valid, count = this.questions.length;
		var that = this;
		for (i=0; i<this.questions.length; i++) {
			idq = this.questions[i].id;
			type = this.questions[i].type;
			//setTimeout(function() {
				switch (type) {
					case 'choice': valid = that.checkChoice(idq); break;
					case 'inline_choice': valid = that.checkInlineChoice(idq); break;
					case 'choice_multiple': valid = that.checkChoiceMultiple(idq); break;
					case 'associate': valid = that.checkAssociate(idq); break;
					case 'text_entry' :
					case 'extended_text': valid = that.checkExtendedText(idq); break;
					case 'upload': valid = that.checkUpload(idq); break;
					default: valid = false; break; //this shouldn't happen
				}
				if (valid) { count--; }
			//}, 0);
		}
		//setTimeout(function(){
			if (count <= 0) {
				that.allow();
			} else {
				that.deny();
			}
		//}, 1);
	},


	checkChoice: function(idQuestion) {
		var answer = $('input[id^=question-'+idQuestion+']'), oScope = this, output = false;
		var output = false;
		if (answer.length > 0) {
			var selectedVal =  $("[name='"+answer[0].name+"']:checked").val()
			if (typeof selectedVal != "undefined") {
				output = true;
			}
		}
		return output;
	},
	
	checkInlineChoice: function(idQuestion) {
		var answer = $('select[id^=question-'+idQuestion+']'), oScope = this, output = true;
		answer.each(function(e) {
			if ($(this).val() == 0) {
				output = false;
			}
		});
		return output;
	},
	
	
	checkChoiceMultiple: function(idQuestion) {
		var checked = $('input[id^=question-'+idQuestion+']:checked');
		return (checked.length > 0);
	},
	

	checkAssociate: function(idQuestion) {
		var answer = $('select[id^=question-'+idQuestion+']'), oScope = this, output = true;
		answer.each(function(e) {
			if ($(this).val() == 0) {
				output = false;
			}
		});
		return output;
	},

	checkExtendedText: function(idQuestion) {
		return ($('#question-'+idQuestion).val() != "");
	},

	checkTextEntry: function(idQuestion) {
		return ($('#question-'+idQuestion).val() != "");
	},
	
	checkUpload: function(idQuestion) {
		return ($('#question-'+idQuestion).val() != "");
	},
	

	nextBtn: $('#next-page-btn'),

	resultBtn: $('#test-show-result'),

	infoBox: $('#answer-info'),

	allow: function() {
		if (this.nextBtn) {
			$('#next-page-btn').removeAttr("disabled").removeClass('disabled');
		}
		if (this.resultBtn) {
			$('#test-show-result').removeAttr("disabled").removeClass('disabled');
		}
		if (this.infoBox) {
			$('#answer-info').css('display', 'none');
		}
	},

	deny: function() {
		if (this.nextBtn) {
			$('#next-page-btn').attr("disabled", "disabled").addClass('disabled');
		}
		if (this.resultBtn) {
			$('#test-show-result').attr("disabled", "disabled").addClass('disabled');
		}
		if (this.infoBox) {
			$('#answer-info').css('display', 'block');
		}
	}

};