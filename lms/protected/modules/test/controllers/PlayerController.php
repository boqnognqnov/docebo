<?php


class PlayerController extends TestBaseController {

	protected $test = NULL;
	protected $object = NULL;
	protected $track = NULL;
	protected $validTest = true;


	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters()
	{
		return array(
				'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules()
	{
		return array(

				array('allow',
						'users'=>array('@'),
						'expression' => ' (Yii::app()->controller->validTest) && (Yii::app()->controller->userIsSubscribed || Yii::app()->controller->userCanAdminCourse) || Yii::app()->user->getIsPu()',
				),

				array('deny', // if some permission is wrong -> the script goes here
						'users' => array('*'),
						'deniedCallback' => array($this, 'accessDeniedCallback'),
						'message' => Yii::t('course', '_NOENTER'),
				),
		);
	}


	//methods

	public function init() {
		parent::init();
		$idCourse = $this->getIdCourse();
		$idTest = Yii::app()->request->getParam('id_test', 0);
		$test = LearningTest::model()->findByPk($idTest);
		$object = LearningOrganization::model()->findByAttributes(array(
				'idResource' => $idTest,
				'objectType' => LearningOrganization::OBJECT_TYPE_TEST,
				'idCourse' => $this->getIdCourse(),
		));

		// We need a valid test: must exist as an object and must belong to the requested course id
		if (!$test || !$object) {
			$this->validTest = false;
			return;
		}

		$this->test = $test;

		$idUser = Yii::app()->user->id;
		$object = $object->getMasterForCentralLo($idUser);

		$this->object = $object;

		Yii::app()->tinymce->init();
	}


	const TEST_SESSION_PREFIX = 'LO_test_session_data';

	protected $sessionStarted = false;

	public function isSessionStarted() {
		return (bool)$this->sessionStarted;
	}

	public function startSession() {
		$idTest = $this->test->getPrimaryKey();
		$sessionValue = Yii::app()->session[self::TEST_SESSION_PREFIX];
		if (!is_array($sessionValue)) { $sessionValue = array(); }
		if (!isset($sessionValue[$idTest])) { $sessionValue[$idTest] = array(); }
		Yii::app()->session[self::TEST_SESSION_PREFIX] = $sessionValue;
		$this->sessionStarted = true;
	}

	public function endSession() {
		$idTest = $this->test->getPrimaryKey();
		$sessionValue = Yii::app()->session[self::TEST_SESSION_PREFIX];
		if (is_array($sessionValue) && isset($sessionValue[$idTest])) {
			$newSessionValue = array();
			foreach ($sessionValue as $key => $value) {
				if ($key != $idTest) { $newSessionValue[$key] = $value; }
			}
			Yii::app()->session[self::TEST_SESSION_PREFIX] = $newSessionValue;
		}
		$this->sessionStarted = false;
	}

	public function setSessionValue($key, $value) {
		if (!$this->isSessionStarted()) return false;
		$idTest = $this->test->getPrimaryKey();
		$sessionValue = Yii::app()->session[self::TEST_SESSION_PREFIX];
		if (isset($sessionValue[$idTest])) {
			$sessionValue[$idTest][$key] = $value;
			Yii::app()->session[self::TEST_SESSION_PREFIX] = $sessionValue;
		}
		return true;
	}

	public function unSetSessionValue($key) {
		if (!$this->isSessionStarted()) return false;
		$idTest = $this->test->getPrimaryKey();
		$sessionValue = Yii::app()->session[self::TEST_SESSION_PREFIX];
		if (is_array($sessionValue) && isset($sessionValue[$idTest])) {
			$newSessionValue = array();
			foreach ($sessionValue[$idTest] as $index => $content) {
				if ($key != $index) { $newSessionValue[$index] = $content; }
			}
			$sessionValue[$idTest] = $newSessionValue;
			Yii::app()->session[self::TEST_SESSION_PREFIX] = $sessionValue;
		}
		return true;
	}

	public function readSessionValue($key) {
		if (!$this->isSessionStarted()) return NULL;
		$idTest = $this->test->getPrimaryKey();
		$sessionValue = Yii::app()->session[self::TEST_SESSION_PREFIX];
		return (isset($sessionValue[$idTest][$key]) ? $sessionValue[$idTest][$key] : NULL);
	}


	public function getQuestionViewsPath() {
		return 'playQuestion/';
	}

	/**
	 * Register Controller specific assets
	 *
	 * (non-PHPdoc)
	 * @see TestBaseController::registerResources()
	 */
	public function registerResources() {
		parent::registerResources();

		if (!Yii::app()->request->isAjaxRequest) {
			/* @var $cs CClientScript */
			$cs = Yii::app()->getClientScript();
			//$cs->registerCssFile($this->module->assetsUrl . '/css/testpoll.css');
		}

	}


	public function beforeAction($action) {
		$this->beginTrack();

		if ($action->id == 'play') {
			$this->startSession();
		}

		return parent::beforeAction($action);
	}

	protected function buildBreadCrumbs($action)
	{
		switch ($action->id) {
			case 'play':
			case 'results':
			case 'review':
				$this->breadcrumbs[] = Yii::t('menu_over', '_MYCOURSES');
				$this->breadcrumbs[$this->courseModel->name] = $this->createUrl('/player/training/index');
				break;
		}
	}

	protected function beginTrack() {
		if (!$this->track) {
			$idTest = $this->test->getPrimaryKey();
			$idUser = Yii::app()->user->id;
			$trackInfo = LearningTesttrack::model()->findByAttributes(array('idTest' => $idTest, 'idUser' => $idUser));
			if (!$trackInfo) {
				$trackInfo = new LearningTesttrack();
				$trackInfo->idTest = $idTest;
				$trackInfo->idUser = $idUser;
				$trackInfo->idReference = $this->object->getPrimaryKey();
				$trackInfo->date_attempt = Yii::app()->localtime->toLocalDateTime();
				$trackInfo->date_end_attempt = Yii::app()->localtime->toLocalDateTime();
				$trackInfo->last_page_seen = 0;
				$trackInfo->number_of_save = 0;
				$trackInfo->number_of_attempt = 0;
				$trackInfo->score_status = LearningTesttrack::STATUS_DOING;
				if (!$trackInfo->save()) {
					throw new CException('Error while saving tracking info');
				}
			}
			$this->track = $trackInfo;
		}
	}



	protected function resetTrack() {

		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {

			// custom plugin event to customise behavior of this function
			$event = new DEvent($this, array('this' => $this));
			Yii::app()->event->raise('CustomCriteriaToGetIncorrectAnswers', $event);
			if($event->shouldPerformAsDefault()) {
				$questions = $this->test->questionsOnly;
				if (!empty($questions)) {
					foreach ($questions as $question) {
						$questionManager = CQuestionComponent::getQuestionManager($question->type_quest);
						if ($questionManager && method_exists($questionManager, 'deleteUserAnswer')) {
							$questionManager->deleteUserAnswer($this->track->getPrimaryKey(), $question->getPrimaryKey());
						}
					}
				}

				$rs = LearningTesttrackPage::model()->deleteAllByAttributes(array('idTrack' => $this->track->getPrimaryKey()));
				if ($rs === FALSE) {
					throw new CException('Error while resetting track info');
				}
				$rs = LearningTesttrackQuest::model()->deleteAllByAttributes(array('idTrack' => $this->track->getPrimaryKey()));
				if ($rs === FALSE) {
					throw new CException('Error while resetting track info');
				}
			}
			$newTrackInfo = array(
				'date_attempt' => Yii::app()->localtime->toLocalDateTime(),
				'date_end_attempt' => Yii::app()->localtime->toLocalDateTime(),
				'date_attempt_mod' => NULL,
				'last_page_seen' => 0,
				'last_page_saved' => 0,
				'score' => 0,
				'bonus_score' => 0,
				'score_status' => LearningTesttrack::STATUS_NOT_COMPLETE,
				'suspended_until' => NULL //if we are started a test, it means it's no more suspended
			);
			$this->track->setAttributes($newTrackInfo);
			if (!$this->track->save()) {
				throw new CException('Error while resetting track info');
			}

			if (isset($transaction)) { $transaction->commit(); }

		} catch (CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}
	}


	public function actionPlay() {
		Yii::app()->clientScript->registerMetaTag('no-store,no-cache,must-revalidate,no-transform,max-age=0,post-check=0,pre-check=0', 'Cache-Control');
		Yii::app()->clientScript->registerMetaTag('no-cache', 'Pragma');
		Yii::app()->clientScript->registerMetaTag('Sat, 26 Jul 1997 05:00:00 GMT', 'Expires');

		header('Cache-Control:no-store,no-cache,must-revalidate,no-transform,max-age=0,post-check=0,pre-check=0');
		header('Pragma:no-cache');
		header('Expires:Sat, 26 Jul 1997 05:00:00 GMT');

		$rq = Yii::app()->request;

		$transaction = Yii::app()->db->beginTransaction();

		try {
			if ($rq->getParam('restart', FALSE) !== FALSE) {

				//delete existing track if needed, then begin the test

				$scoreStatus = $this->track->score_status;

				$isEnd = ($scoreStatus == LearningTesttrack::STATUS_VALID
					|| $scoreStatus == LearningTesttrack::STATUS_NOT_CHECKED
					|| $scoreStatus == LearningTesttrack::STATUS_PASSED
					|| $scoreStatus == LearningTesttrack::STATUS_NOT_PASSED);
				if ($scoreStatus == LearningTesttrack::STATUS_NOT_COMPLETE || $isEnd) {
					$this->resetTrack();
				}

				//clean potential previous session data
				$this->unSetSessionValue('shuffledAnswers'); //this is necessary, otherwise when re-playing a test it may conflict with database storing of answer ordering

				$this->play();

			} elseif ($rq->getParam('test_save_keep', FALSE) !== FALSE) {

				// stop the text, continue it later
				$this->saveAndExit();

			} elseif ($rq->getParam('show_result', FALSE) !== FALSE) {
				// continue a test completed, show the result
				$this->results();

			} elseif ($rq->getParam('time_elapsed', FALSE) !== FALSE && $rq->getParam('time_elapsed', 0) == 1) {

				if ($this->test->time_dependent == LearningTest::TIME_YES) {
					// continue a test completed, show the result
					// NOTE: since the test timer expirated, it is not guaranteed that the user has answered all questions, so
					// try to fill all test answers with unanswered values (this will be useful for reporting purpose).
					// For some ordering types, this is not needed/applicable
					$this->track->fillUnansweredQuestions();
					$this->results();
				} else {
					// continue test
					$this->play();
				}

			} elseif ($rq->getParam('begin', FALSE) !== FALSE) {

				$scoreStatus = isset($this->track->score_status) ? $this->track->score_status :  null;
				if($scoreStatus && $this->getIdCourse()){
					$isEnd = ($scoreStatus == LearningTesttrack::STATUS_VALID
						|| $scoreStatus == LearningTesttrack::STATUS_NOT_CHECKED
						|| $scoreStatus == LearningTesttrack::STATUS_PASSED
						|| $scoreStatus == LearningTesttrack::STATUS_NOT_PASSED);
					if($isEnd && $rq->isAjaxRequest){
						$response = new AjaxResult();
						$response->setStatus(true);
						$response->setData(array(
							'redirectUrl' =>  $this->createUrl('//player/training/index', array('course_id' => $this->getIdCourse()
								))
						));
						$response->toJSON();
						Yii::app()->end();
					}
				}



				if ($this->track->score_status != LearningTesttrack::STATUS_DOING) { //this may happen when reloading page after starting a new test
					$this->track->reset();
				}

				//clean potential previous session data
				$this->unSetSessionValue('shuffledAnswers'); //this is necessary, otherwise when re-playing a test it may conflict with database storing of answer ordering

				$this->play();

			} else {

					// play test
					$this->play();
			}

			$transaction->commit();

		} catch (CException $e) {

			$transaction->rollback();
			throw ($e);

		}

	}


	public function actionResults() {
		$this->results();
	}

	public function actionKeepAlive()
	{
		// extend the session
		Yii::app()->user->getIsGuest();

		$data = array(
			'success' => true
		);

		header('Content-type: text/javascript');
		if (isset($_GET['jsoncallback'])) {
			// JSONP callback
			$jsoncallback = preg_replace('/[^a-zA-Z\._]+/', '', $_GET['jsoncallback']);
			echo $jsoncallback. '(' . CJSON::encode($data) . ')';
		} else {
			// standard Json
			echo CJSON::encode($data);
		}

		// Update course+user session tracking to register user's course activity (time in course)
		if (isset($_REQUEST['course_id']) && isset($_REQUEST['id_test']))
			ScormApiBase::trackCourseSession((int)Yii::app()->user->idst, $_REQUEST['course_id']);

		Yii::app()->end();
	}

	//operative methods

	protected function play() {

		$db = Yii::app()->db;
		$rq = Yii::app()->request;

		$refUrl = false;
		$showError = true;

		if ($db->getCurrentTransaction() === NULL) { $transaction = Yii::app()->db->beginTransaction(); }

		$idUser = Yii::app()->user->id;
		$idTest = $this->test->getPrimaryKey();

		if ($this->readSessionValue('testDateBegin') === NULL) {
			$this->setSessionValue('testDateBegin', Yii::app()->localtime->UTCNow);
		}

		try {

			//$this->getTrack(); //already initialized in beforeAction

			// cast display to one quest at time if the time is by quest
			$displayType = ($this->test->time_dependent == LearningTest::TIME_YES_QUEST
				? LearningTest::DISPLAY_TYPE_SINGLE
				: $this->test->display_type);

			//number of test pages
			$totalPages = $this->test->getTotalPageNumber();

			// find the page to display
			$previousPage = $rq->getParam('previous_page', FALSE);
			if ($previousPage === FALSE) {

				$continue = $rq->getParam('continue', FALSE);
				$pageContinue = $rq->getParam('page_continue', FALSE);
				if ($pageContinue !== FALSE && $continue !== FALSE) {
					$pageToDisplay = $pageContinue;
				} else {
					if($rq->getRequestType() == 'GET' && !$rq->isAjaxRequest) {
						$page = (int)$rq->getParam('page', FALSE);
						if($page && $page <= $totalPages)
							$pageToDisplay = $page;
						else
							$pageToDisplay = $this->track->last_page_seen;
					} else {
						$pageToDisplay = 1;
					}
				}

			} else {

				$pageToDisplay = $previousPage;
				$nextPage = $rq->getParam('next_page', FALSE);
				$prevPage = $rq->getParam('prev_page', FALSE);
				if ($nextPage !== FALSE) { $pageToDisplay++; }
				if ($prevPage !== FALSE && $this->test->can_travel) { $pageToDisplay--; }

			}

			$invalidBackward = false;
			if ($pageToDisplay < $this->track->last_page_seen && !$this->test->can_travel) {
				//throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				//since the user is not allowed to go back in pages (but we can't block backward button in browsers), then "redirect" it to the origin page
				$pageToDisplay = $this->track->last_page_seen;
				$invalidBackward = true;
			}
			$event = new DEvent();
			Yii::app()->event->raise('CheckForStudyMode', $event);
			if($event->shouldPerformAsDefault()){

				// old logic, without custom plugin enabled
				if ($this->track->score_status != LearningTesttrack::STATUS_NOT_COMPLETE && $this->track->score_status != LearningTesttrack::STATUS_DOING) {
					$refUrl = $this->createUrl('//player/training/index', array('course_id' => $this->getIdCourse()));
					$showError = false;
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				}
				$newInfo = array(
					'last_page_seen' => $pageToDisplay,
					'score_status' => LearningTesttrack::STATUS_DOING
				);
			}
			else{

				// do not allow study mode to modify score_status, in order to preserve the current state of the test
				$newInfo = array(
					'last_page_seen' => $pageToDisplay,
				);
			}



			$pageToSave = $rq->getParam('page_to_save', FALSE);
			if ($pageToSave !== FALSE) {

				//if ($this->test->mod_doanswer || (!$this->test->mod_doanswer && $pageToSave > $this->track->last_page_saved)) {
					//NOTE: above condition was incorrect when page navigation is allowed. Answers editing/rewriting is managed in answer storage (see question class components)
					//TODO: should we check "$this->test->can_travel" option?
					$newInfo['last_page_saved'] = $pageToSave;
					$this->track->storePage($pageToSave, $this->test->mod_doanswer);
					$this->track->closeTrackPageSession($pageToSave);
				//}

			}

			$this->track->setAttributes($newInfo);
			$rs = $this->track->update();
			if ($rs === FALSE) {
				throw new CException('Error in tracking update');
			}

			// save page track info
			if($pageToDisplay !== $pageToSave || ($pageToSave === false))
				if (!$this->track->updateTrackForPage($pageToDisplay)) {
					throw new CException('Error in page tracking update');
				}

			//$quest_sequence_number = $this->test->getInitQuestionSequenceNumberForPage($pageToDisplay);
			//$query_question = $this->test->getQuestionsForPage($pageToDisplay, $idUser);
			//$time_in_test = $this->track->userTimeInTheTest();

			if(!$pageToDisplay)
				$pageToDisplay = 1;
			$questions = $this->test->getQuestionsForPage($pageToDisplay, $idUser);
			if (empty($questions)) {
//				throw new CException('Error in question retrieval');
				$_SESSION['correctAnswersMsg'] = Yii::t('test', "All questions in this page have already been answered correctly");
			}
			$i = 1;
			foreach ($questions as $question) {
				$trackQuestion = LearningTesttrackQuest::model()->findByPk(array('idTrack' => $this->track->getPrimaryKey(), 'idQuest' => $question->getPrimaryKey()));
				if (!$trackQuestion) {
					$trackQuestion = new LearningTesttrackQuest();
					$trackQuestion->idTrack = $this->track->getPrimaryKey();
					$trackQuestion->idQuest = $question->getPrimaryKey();
					$trackQuestion->page = $pageToDisplay;
					$trackQuestion->quest_sequence = $i;
					if (!$trackQuestion->save()) {
						throw new CException('Error while saving questions tracking info');
					}
					$i++;
				}
			}

			$lockEdit = FALSE;

			$checkTime = FALSE;
			$startTime = FALSE;
			$timeInQuest = FALSE;

			if ($this->test->time_dependent == LearningTest::TIME_YES || $this->test->time_dependent == LearningTest::TIME_YES_QUEST) {

				if ($this->test->time_dependent == LearningTest::TIME_YES) {
					$checkTime = TRUE;

					//time limit is for test
					$startTime = $this->test->time_assigned - $this->track->userTimeInTheTest();

					if ($startTime <= 0) {

						$this->results();
						if (isset($transaction)) { $transaction->commit(); }
						return;
					}

				} elseif ($this->test->time_dependent == LearningTest::TIME_YES_QUEST) {

					//time limit is for question
					$startTime = $questions[0]['time_assigned'];
					if($startTime > 0)
						$checkTime = TRUE;

					$timeInQuest = $this->track->userTimeInThePage($pageToDisplay);
					$startTime = $startTime - $timeInQuest;
					if ($startTime <= 0) {
						$lockEdit = TRUE;
					}

				}

			}

			if ($this->test->mandatory_answer) {

				$cs = Yii::app()->getClientScript();
				$cs->registerScriptFile($this->module->assetsUrl.'/js/mandatory_answers.js');
			}

			if($rq->isAjaxRequest) {
				$response = new AjaxResult();
				$response->setStatus(true);
				$response->setData(array(
					'redirectUrl' =>  $this->createUrl('play', array(
						'id_test' => $this->test->getPrimaryKey(),
						'course_id' => $this->getIdCourse(),
						'page' => $pageToDisplay
					))
				));
				$response->toJSON();
			} else {

				$renderData = array(
					'idTest' => (int)$idTest,
					'testInfo' => $this->test,
					'trackInfo' => $this->track,
					'pageToDisplay' => $pageToDisplay,
					'totalPages' => $totalPages,
					'lockEdit' => $lockEdit,
					'displayType' => $displayType,
					'checkTime' => $checkTime,
					'startTime' => $startTime,
					'timeInQuest' => $timeInQuest,
					'questions' => $questions,
					'startQuestionIndex' => $this->test->getInitQuestionSequenceNumberForPage($pageToDisplay),
					'mandatoryAnswers' => (bool)$this->test->mandatory_answer,
					'invalidBackward' => $invalidBackward
				);
				$event = new DEvent($this, $renderData);
				Yii::app()->event->raise('RenderStudyMode', $event);

				if ($event->shouldPerformAsDefault()) {
					$this->render('play', $renderData);
				}
			}
			if (isset($transaction)) { $transaction->commit(); }

		} catch(CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			if($showError)
				Yii::app()->user->setFlash('error', $e->getMessage());
			if($refUrl)
				$this->redirect($refUrl);
		}
	}


	//other methods

	public function results() {

		$db = Yii::app()->db;
		$rq = Yii::app()->request;
		if ($this->track->score_status != LearningTesttrack::STATUS_NOT_COMPLETE && $this->track->score_status != LearningTesttrack::STATUS_DOING) {
			throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
		}
		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {
//
//			$courseUser = LearningCourseuser::model()->findByAttributes(array('idUser' => Yii::app()->user->id, 'idCourse' => $this->getIdCourse()));
//			$oldStatus = $courseUser->status;
			$showReview = $rq->getParam('show_review', FALSE);

			//note: this shouldn't be necessary, but anyway keep it for safe
			if ($this->readSessionValue('testDateBegin') === NULL) {
				$this->setSessionValue('testDateBegin', Yii::app()->localtime->UTCNow);
			}

			if ($this->track->score_status == LearningTesttrack::STATUS_VALID && $showReview === FALSE) {
				return;
			}

			$previousPage = $rq->getParam('previous_page', FALSE);
			$pageToSave = $rq->getParam('page_to_save', FALSE);

			$newInfo = array(
				'last_page_seen' => $previousPage,
				'score_status' => LearningTesttrack::STATUS_DOING
			);

			if (($pageToSave !== FALSE && $pageToSave > $this->track->last_page_saved) || $this->test->mod_doanswer) {
				$this->track->storePage($pageToSave, $this->test->mod_doanswer);
				$this->track->closeTrackPageSession($pageToSave);
			}


			$table1 = LearningTestquest::model()->tableName();
			$table2 = LearningTesttrackQuest::model()->tableName();
			$table3 = LearningTestQuestRel::model()->tableName();

			$questions = Yii::app()->db->createCommand()
				->select("q.idQuest, q.type_quest, q.idCategory")
				->from($table1." q")
				->join($table2." t", "t.idQuest = q.idQuest")
				->join($table3." qr", "qr.id_question = q.idQuest")
				->where("qr.id_test = :id_test", array(':id_test' => $this->test->getPrimaryKey()))
				->andWhere("t.idTrack = :id_track", array(':id_track' => $this->track->getPrimaryKey()))
				->andWhere("q.type_quest <> :type_1", array(':type_1' => LearningTestquest::QUESTION_TYPE_BREAK_PAGE))
				->andWhere("q.type_quest <> :type_2", array(':type_2' => LearningTestquest::QUESTION_TYPE_TITLE))
				->order("qr.sequence ASC")
				->queryAll();

			$totalUserScore = 0;
			$totalMaxScore = 0;
			$countManual = 0;
			$manualScore = 0;
			$categoriesScore = array();
			$categoriesScoreMax = array();

			foreach ($questions as $question) {

				$questionManager = CQuestionComponent::getQuestionManager($question['type_quest']);
				$userScore = $questionManager->getUserScore($question['idQuest'], $this->track->getPrimaryKey());
				$maxScore = $questionManager->getMaxScore($question['idQuest']);

				if ($questionManager->getScoreType() == CQuestionComponent::SCORE_TYPE_MANUAL) {
					$countManual++;
					$manualScore = round($manualScore + $maxScore, 2);
				}

				$totalUserScore = round($totalUserScore + $userScore, 2);
				$totalMaxScore += $maxScore;

				$idCategory = (int)$question['idCategory'];
				if(!isset($categoriesScore[$idCategory]))
				{
					$categoriesScore[$idCategory] = round($userScore, 2);
					$categoriesScoreMax[$idCategory] = $maxScore;
				}
				else
				{
					$categoriesScore[$idCategory] = round($userScore + $categoriesScore[$idCategory], 2);
					$categoriesScoreMax[$idCategory] += $maxScore;
				}
			}


			if ($this->test->point_type == LearningTest::POINT_TYPE_PERCENT) {
				// Prevent division by zero if NO score points have been assigned to questions
				if (!($totalMaxScore >0)) {
					$totalUserScore = 0;
					foreach($categoriesScore as &$score) {
						$score = 0;
					}
				}
				else {
					$totalUserScore = round(100 * $totalUserScore / $totalMaxScore, 2, PHP_ROUND_HALF_EVEN);
					$manualScore = round(100 * $manualScore / $totalMaxScore, 2, PHP_ROUND_HALF_EVEN);
					foreach($categoriesScore as $idCategory => &$score) {
						if($categoriesScoreMax[$idCategory] > 0) {
							$score = Yii::app()->format->formatNumber(($score / $categoriesScoreMax[$idCategory]) * 100);
						} else {
							$score = 0;
						}
					}
				}

			}

			$saveScore = $totalUserScore;


			if ($totalUserScore >= $this->test->point_required) {
				$nextStatus = LearningTesttrack::STATUS_PASSED;
				/*if ($this->test->show_only_status)*/ { $scoreStatus = LearningTesttrack::STATUS_PASSED; }
			} else {
				$nextStatus = LearningTesttrack::STATUS_NOT_PASSED;
				/*if ($this->test->show_only_status)*/ { $scoreStatus = LearningTesttrack::STATUS_NOT_PASSED; }
			}

			if ($this->test->show_only_status) {
				if ($countManual > 0) {
					$scoreStatus = LearningTesttrack::STATUS_NOT_CHECKED;
				} else {
					$scoreStatus = LearningTesttrack::STATUS_VALID;
				}
			}


			$newTrackInfo = array();

			switch ($scoreStatus) {
				case LearningTesttrack::STATUS_VALID:
				case LearningTesttrack::STATUS_NOT_CHECKED:
				case LearningTesttrack::STATUS_PASSED:
				case LearningTesttrack::STATUS_NOT_PASSED: {

					$newTrackInfo['date_end_attempt'] = Yii::app()->localtime->toLocalDateTime();
					$newTrackInfo['number_of_save'] = count($this->track->times) + 1;
					$newTrackInfo['score'] = $saveScore;
					$newTrackInfo['score_status'] = $scoreStatus;
					$newTrackInfo['number_of_attempt'] 	= $this->track->number_of_attempt + 1;

					//update track
					if ($showReview === FALSE) {

						$utcTestDateBegin = $this->readSessionValue('testDateBegin');
						$timeElapsed = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s')) - strtotime($utcTestDateBegin);

						$trackTime = new LearningTesttrackTimes();
						$trackTime->setAttributes(array(
							'idTrack' => $this->track->getPrimaryKey(),
							'idReference' => $this->object->getPrimaryKey(),
							'idTest' => $this->test->getPrimaryKey(),
							'date_attempt' => Yii::app()->localtime->toLocalDateTime(),
							'number_time' => $newTrackInfo['number_of_save'],
							'score' => $newTrackInfo['score'],
							'score_status' => $newTrackInfo['score_status'],
							'date_begin' => Yii::app()->localtime->toLocalDateTime($utcTestDateBegin),
							'date_end' => Yii::app()->localtime->toLocalDateTime(),
							'time' => $timeElapsed
						));
						if (!$trackTime->save()) {
							throw new CException('Error while saving track time data'.print_r($trackTime->getErrors(), TRUE));
						}

						$this->unsetSessionValue('testDateBegin');
					}

				} break;
			}


			if ($this->test->use_suspension && $this->test->max_attempt > 0 && $this->test->suspension_num_hours > 0) {

				switch ($nextStatus) {
					case LearningTesttrack::STATUS_NOT_PASSED: {
						$newTrackInfo['number_of_attempt'] = $this->track->number_of_attempt + 1;
						if ($newTrackInfo['number_of_attempt'] >= $this->test->max_attempt && $this->test->suspension_num_hours > 0) {
							//should we reset learning_test.suspension_num_attempts ??
							//$newTrackInfo['number_of_attempt'] = 0; //from now on, it will use the suspended_until parameter, so only the date is needed, we can reset the attempts count
							$suspendDateUTC = date('Y-m-d H:i:s', strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s')) + $this->test->suspension_num_hours*3600);
							$newTrackInfo['suspended_until'] = Yii::app()->localtime->toLocalDateTime($suspendDateUTC);
						} else {
							//if max_attempt is <= 0, never update attempts counter, so user won't never be de-suspended
						}
					} break;
					case LearningTesttrack::STATUS_COMPLETED:
					case LearningTesttrack::STATUS_PASSED: {
						//$newTrackInfo['attempts_for_suspension'] = 0;
						//$newTrackInfo['number_of_attempt'] = 0;
					} break;
				}
			}

			$this->track->setAttributes($newTrackInfo); //TO DO: check commontrack status too, date_attempt and status
			if (!$this->track->save()) {
				throw new CException('Error while updating tracking info');
			}


			//----
		//	$this->setRedirectSessionParams();

			$stdBackUrl = $this->createUrl('//player/training', array('course_id' => $this->getIdCourse(), 'mode' => 'view_course'));

			$this->render('results', array(
				'totalUserScore' => $totalUserScore,
				'totalMaxScore' => $totalMaxScore,
				'countManual' => $countManual,
				'manualScore' => $manualScore,
				'categoriesScore' => $categoriesScore,
				'backUrl' => $rq->getParam('back_url', $stdBackUrl),
				'testFailed' => ($nextStatus === LearningTesttrack::STATUS_NOT_PASSED),
				'feedbackText' => LearningTestFeedback::getFeedbackText($this->test->getPrimaryKey(), $totalUserScore),
				'mandatoryAnswers' => (bool)$this->test->mandatory_answer,
				'testInfo' => $this->test
			));

			if (isset($transaction)) { $transaction->commit(); }

		} catch(CException $e) {
			//...
			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}
	}

	public function actionReview() {
		$this->review();
	}

	/**
	 *
	 * Allows the download of the attachment that the user uploaded
	 * on a Test question of the type "file upload"
	 *
	 * @param int $id_test
	 * @param int $idQuestion
	 * @param int $idTrack
	 * @param int $idUser
	 *
	 * @throws Exception
	 */
	public function actionDownloadUpQuestAnswer($id_test = NULL, $idQuestion = NULL, $idTrack = NULL, $idUser = NULL){

		$idUser = $idUser ? $idUser : Yii::app()->user->id;

		$theQuestion = LearningTestQuestRel::model()->findByAttributes(array(
			'id_test' => $id_test,
			'id_question' => $idQuestion,
		));

		if($theQuestion && $theQuestion->question && $theQuestion->question->type_quest != LearningTestquest::QUESTION_TYPE_UPLOAD){
			throw new Exception('This is not a valid File Upload type question');
		}

		$track = LearningTesttrack::model()->findByAttributes(array(
			'idTrack' => $idTrack,
			'idUser' => $idUser,
		));

		if($track){
			$answer = LearningTesttrackAnswer::model()->findByAttributes(array(
				'idTrack'=>$idTrack,
				'idQuest'=>$idQuestion
			));
			if(!$answer){
				throw new Exception('Question not answered by user');
			}else{
				if(isset($answer->more_info) && !empty($answer->more_info)){
					$filename = $answer->more_info;
					$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_TEST);
					$storage->sendFile($filename);
					Yii::app()->end();
				}
				else{
					throw new Exception('No file uploaded by user');
				}
			}

		}else{
			throw new Exception('Track not found');
		}
	}

	public function review() {
		$rq = Yii::app()->request;
		$idUser = Yii::app()->user->id;
		$testTrack = LearningTesttrack::model()->with('questsWithTitles')->findByAttributes(array(
			'idReference' => $this->object->getPrimaryKey(),
			'idUser' => $idUser
		));
		$idTest = Yii::app()->request->getParam('id_test', 0);
		$test = LearningTest::model()->findByPk($idTest);
		$this->test = $test;
		$questionsLang = LearningTestquest::getQuestionTypesTranslationsList();
		$stdBackUrl = $this->createUrl('//player/training', array('course_id' => $this->getIdCourse(), 'mode' => 'view_course'));
		$commonTrack = LearningCommontrack::model()->findByPk(array(
			'idTrack' => $testTrack->idTrack,
			'objectType' => LearningOrganization::OBJECT_TYPE_TEST,
			'idReference' => $this->object->getPrimaryKey()
		));

		if(LearningTesttrackAnswer::model()->countByAttributes(array('idTrack' => $testTrack->idTrack)) == 0)
			$this->redirect($stdBackUrl);

		$answersVisible = $this->test->answersVisible(Yii::app()->user->id);
		if (!$answersVisible) {
			$this->redirect($stdBackUrl);
		}

		$this->render('review', array(
			'organization' => $this->object,
			'userId' => $idUser,
			'testTrack' => $testTrack,
			'questionsLang' => $questionsLang,
			'backUrl' => $rq->getParam('back_url', $stdBackUrl),
			'commonTrack' => $commonTrack
		), FALSE, TRUE);
	}



	public function saveAndExit() {

		if ($this->track->score_status != LearningTesttrack::STATUS_NOT_COMPLETE && $this->track->score_status != LearningTesttrack::STATUS_DOING) {
			throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
		}
		$db = Yii::app()->db;
		$rq = Yii::app()->request;

		if ($db->getCurrentTransaction() === NULL) { $transaction = Yii::app()->db->beginTransaction(); }

		$idUser = Yii::app()->user->id;
		$idTest = $this->test->getPrimaryKey();

		if ($this->readSessionValue('testDateBegin') === NULL) {
			$this->setSessionValue('testDateBegin', Yii::app()->localtime->UTCNow);
		}

		try {

			if (!$this->track) { //at this point a track record must exists, otherwise something is gone wrong
				throw new CException(Yii::t('test', '_TEST_TRACK_FAILURE'));
			}

			if ($this->test->save_keep > 0) {


				$previousPage = $rq->getParam('previous_page', FALSE);

				$newInfo = array(
					'last_page_seen' => $previousPage, //TODO: what about if ($previousPage === false) ?
					'score_status' => LearningTesttrack::STATUS_NOT_COMPLETE
				);

				$pageToSave = $rq->getParam('page_to_save', FALSE);
				if ($pageToSave !== FALSE) {

					if ($this->test->mod_doanswer/* || (!$this->test->mod_doanswer && $pageToSave > $this->track->last_page_saved)*/) {
						$newInfo['last_page_saved'] = $pageToSave;
						$this->track->storePage($pageToSave, $this->test->mod_doanswer);
						$this->track->closeTrackPageSession($pageToSave);
					} else {
						if ($pageToSave > $this->track->last_page_saved) {
							$newInfo['last_page_saved'] = $pageToSave;
							$this->track->storePage($pageToSave, $this->test->mod_doanswer);
							$this->track->closeTrackPageSession($pageToSave);
						}
					}
				}

				$this->track->setAttributes($newInfo);
				$rs = $this->track->update();
				if ($rs === FALSE) {
					throw new CException('Error in tracking update');
				}

				$params = array();
				$params['backUrl'] = $this->createUrl('/player/training/index', array('mode' => 'view_course'));
				if($rs)  {
					$params['successful'] = TRUE;
				} else {
					$params['successful'] = FALSE;
				}

				$this->render('save_keep', $params);

			} else {

				throw new CException(Yii::t('test', '_TEST_YOUCANNOT_SAVEKEEP'));
			}


			if (isset($transaction)) { $transaction->commit(); }

		} catch (CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}

	}

	public function getTest()
	{
		return $this->test;
	}
}