<?php

class DefaultController extends TestBaseController {

	protected $idTest = NULL;
	protected $test = NULL;
	protected $object = NULL;

	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters()
	{
		return array(
				'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules()
	{
		return array(

			array(
				'allow',
				'users'=>array('@'),
				'expression' => 'Yii::app()->user->getIsPu() || Yii::app()->controller->userCanAdminCourse'
			),

			array(
				'allow',
				'actions' => array('axLaunchpad'),
				'users'=>array('@'),
				'expression' => 'Yii::app()->user->getIsPu() || Yii::app()->controller->userIsSubscribed'
			),

			array(
				'deny',
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
				'message' => Yii::t('course', '_NOENTER'),
			),
		);
	}


	//get / set methods

	public function getIdTest() {
		return $this->idTest;
	}

	public function setIdTest($value) {
		if (is_numeric($value) && (int)$value > 0) {
			$this->idTest = (int)$value;
			$this->test = LearningTest::model()->findByPk($value);
			$idCourse = Yii::app()->request->getParam("course_id", null);
			$this->object = LearningOrganization::model()->findByAttributes(array(
				'idResource' => $value,
				'objectType' => LearningOrganization::OBJECT_TYPE_TEST,
				'idCourse'	 => $idCourse
			));
			return true;
		}
		return false;
	}

	public function getIdReference() {
		if ($this->object) { return $this->object->getPrimaryKey(); }
		return NULL;
	}

	public function setIdReference($value) {
		if (is_numeric($value) && (int)$value > 0) {
			$this->object = LearningOrganization::model()->findByPk($value);
			if (!$this->object || $this->object->objectType != LearningOrganization::OBJECT_TYPE_TEST) { return false; }
			$this->idTest = (int)$this->object->idResource;
			$this->test = LearningTest::model()->findByPk($this->idTest);
			return true;
		}
		return false;
	}


	//methods

	public function init() {
		$idTest = (int)Yii::app()->request->getParam('id_test', 0);
		if ($idTest > 0) {
			$this->setIdTest($idTest);
		} else {
			$idObject = (int)Yii::app()->request->getParam('id_object', 0);
			if ($idObject > 0) {
				$this->setIdReference($idObject);
			}
		}
		Yii::app()->tinymce->init();
		parent::init();
	}

	public function formatQuestionText($question, $length = 0) {
		$output =  strip_tags($question, "<p><ul><ol><li>");
		if ($length > 3 && mb_strlen($output, 'UTF-8') > $length) {
			$output = mb_substr($output, 0, $length - 3, 'UTF-8').'...';
		}
		return $output;
	}

	//actions

	public function actionIndex()
	{
		$this->render('index');
	}



	public function actionAxSaveLo() {
		$courseId = Yii::app()->request->getParam("course_id", null);
		$parentId = Yii::app()->request->getParam("parent_id", null);
		$title = trim(Yii::app()->request->getParam("title", null));
		$description = Yii::app()->request->getParam("description", null);

		$thumb = Yii::app()->request->getParam("thumb", null);
		$shortDescription = urldecode(Yii::app()->request->getParam("short_description", null));

		$response = new AjaxResult();
		$transaction = Yii::app()->db->beginTransaction();

		try {
			// Validate input parameters
			if (!$courseId) { throw new CException("Invalid specified course."); }
			if (!is_numeric($parentId) || $parentId < 0) { $parentId = 0; }
			if (empty($title)) { throw new CException(Yii::t('error', 'Enter a title!')); }

			//instantiate HTML purifier instance

			//prepare AR object to be saved
			$testObj = new LearningTest();
			$testObj->author = Yii::app()->user->getIdst();
			$testObj->title = $title;
			$testObj->description = Yii::app()->htmlpurifier->purify($description);

			//these will be needed by learning organization table
			$testObj->setIdCourse($courseId);
			$testObj->setIdParent($parentId);

			//do actual saving
			if (!$testObj->save()) {
				throw new CException('Error while saving the test.');
			}

			//verify inserted data
			$resourceId = (int)$testObj->getPrimaryKey();
			$learningObject = LearningOrganization::model()->findByAttributes(array(
				'idResource' => $resourceId,
				'objectType' => LearningOrganization::OBJECT_TYPE_TEST,
				'idCourse'   => $courseId
			));
			if (!$learningObject) { throw new CException("Invalid learning object(".$resourceId.")"); }

			// The test is invisible/unpublished by default (until we add some questions to it)
			$learningObject->visible = 0;

			if($shortDescription)
				$learningObject->short_description = $shortDescription;
			if($thumb)
				$learningObject->resource = intval($thumb);

			$learningObject->saveNode();

			// allow custom plugin to save test info and set custom settings to this new test
			$idTest = $testObj->idTest;
			$event = new DEvent($this, array('that' => $this, 'testCreated' => 1, 'idTest' => $idTest));
			Yii::app()->event->raise('SaveUserChoices',$event);

			// DB Commit
			$transaction->commit();

			// Trigger event to let plugins save their own data from POST
			Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
				'loModel' => $learningObject
			)));

			$data = array(
					'resourceId' => $resourceId,
					'organizationId' => (int)$learningObject->getPrimaryKey(),
					'parentId' => (int)$parentId,
					'title' => $title,
					'message' => Yii::t('standard', '_OPERATION_SUCCESSFUL')
			);
			$response->setData($data)->setStatus(true);

		} catch (CException $e) {
			$transaction->rollback();
			$response->setMessage($e->getMessage())->setStatus(false);
		}

		$response->toJSON();
		Yii::app()->end();
	}


	/**
	 * Show LO launchpad
	 */
	public function actionAxLaunchpad() {
		$response = new AjaxResult();
		$rq = Yii::app()->request;
		$idObject = (int)$rq->getParam('id_object', 0);

		// Collect TEST/USER information for the launcing UI
		$testInfo = LearningTest::collectTestInfoByLoAndUser($idObject, Yii::app()->user->id);


		$html = $this->renderPartial("_launchpad", array(
			'mainInfo' 				=> $testInfo['loModel'],
			'testInfo' 				=> $testInfo['testModel'],
			'trackInfo' 			=> $testInfo['trackModel'],
			'commonTrackInfo'		=> $testInfo['commonTrackModel'],
			'testScores' 			=> $testInfo['testScores'],
			'courseUserModel' 		=> $testInfo['courseUserModel'],
			'max_attemp_reached' 	=> $testInfo['max_attemp_reached'],
			'suspended' 			=> $testInfo['suspended'],
			'suspend_time_left' 	=> $testInfo['suspend_time_left'],
			'need_prerequisites' 	=> $testInfo['need_prerequisites'],
			'timeReadable'			=> $testInfo['timeReadable'],
			'prerequisites_obj'		=> $testInfo['prerequisites_obj'],
			'idReference'			=> $idObject,
			'manualQuestionsCount'	=> $this->getManualQuestionsCount()
		), true);

		$response->setHtml($html);
		$response->setStatus(true);

		//send result
		$response->toJSON();
		Yii::app()->end();

	}


	public function getManualQuestionsCount(){
		$idObject = (int)Yii::app()->request->getParam('id_object', 0);
		$testInfo = LearningTest::collectTestInfoByLoAndUser($idObject, Yii::app()->user->id);

		$trackModel = $testInfo['trackModel'];
		$table1 = LearningTestquest::model()->tableName();
		$table2 = LearningTesttrackQuest::model()->tableName();
		$table3 = LearningTestQuestRel::model()->tableName();

		$questions = Yii::app()->db->createCommand()
			->select("q.idQuest, q.type_quest, q.idCategory")
			->from($table1 . " q")
			->join($table2 . " t", "t.idQuest = q.idQuest")
			->join($table3 . " qr", "qr.id_question = q.idQuest")
			->where("qr.id_test = :id_test", array(':id_test' => $trackModel->idTest))
			->andWhere("t.idTrack = :id_track", array(':id_track' => $trackModel->idTrack))
			->andWhere("q.type_quest <> :type_1", array(':type_1' => LearningTestquest::QUESTION_TYPE_BREAK_PAGE))
			->andWhere("q.type_quest <> :type_2", array(':type_2' => LearningTestquest::QUESTION_TYPE_TITLE))
			->order("qr.sequence ASC")
			->queryAll();

		$countManual = 0;

		if (!empty($questions)) {
			foreach ($questions as $question) {
				$questionManager = CQuestionComponent::getQuestionManager($question['type_quest']);
				if ($questionManager->getScoreType() == CQuestionComponent::SCORE_TYPE_MANUAL) {
					$countManual++;
				}
			}
		}
		return $countManual;
	}



	public function actionAxCreateTest() {

		$isListViewLayout = false;
		if($this->getIdCourse())
			$isListViewLayout = LearningCourse::getPlayerLayout($this->getIdCourse())==LearningCourse::$PLAYER_LAYOUT_LISTVIEW;

		$response = new AjaxResult();
		$html = $this->renderPartial("_create_test", array(
			'idCourse'=>$this->getIdCourse(),
			'lo_type'=>LearningOrganization::OBJECT_TYPE_TEST,
			'isListViewLayout' => $isListViewLayout
		), true, true);
		$response->setStatus(true)->setHtml($html)->toJSON();
		Yii::app()->end();

	}

	public function actionAxEditTest(){
		$rq = Yii::app()->getRequest();
		$idTest = $rq->getParam('id_test', null);

		$test = LearningTest::model()->findByPk($idTest);
		$object = LearningOrganization::model()->findByAttributes(array(
			'idResource' => $idTest,
			'objectType' => LearningOrganization::OBJECT_TYPE_TEST,
			'idCourse'   => $this->getIdCourse()
		));

		$opt = $rq->getParam('opt', false);

		if($opt !== false && $opt === 'centralrepo' ){
			$versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
				'id_resource' => $idTest,
				'object_type' => LearningOrganization::OBJECT_TYPE_TEST
			));

			if($versionModel){
				$object = LearningRepositoryObject::model()->findByPk($versionModel->id_object);
			}
		}

		if ($rq->getParam('confirm', false) !== false) {

			$test->title = $rq->getParam('title', "");
			$test->description = $rq->getParam('description', "");

			$shortDescription = $rq->getParam('short_description', "");
			$thumb = $rq->getParam('thumb', null);

			if (!$test->save()) {
				echo '<a class="auto-close error"></a>';
				return;
			}else{
				$object->title = $test->title;

//				if($shortDescription)
					$object->short_description = Yii::app()->htmlpurifier->purify($shortDescription);

				if($thumb){
					$object->resource = intval($thumb);
				}elseif($object->resource){
					$object->resource = 0;
				}

				if($object instanceof LearningOrganization){
					$object->saveNode();
				} else if($object instanceof LearningRepositoryObject){
					$object->save();
				}


				// Trigger event to let plugins save their own data from POST
				Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
					'loModel' => $object
				)));

				echo CHtml::link('', FALSE, array(
					'class' => 'auto-close success',
					'data-new_title' => $test->title
				));
				return;
			}
		}

		$isListViewLayout = false;
		if($this->getIdCourse())
			$isListViewLayout = LearningCourse::getPlayerLayout($this->getIdCourse())==LearningCourse::$PLAYER_LAYOUT_LISTVIEW;

		$html = $this->renderPartial("_edit_test", array(
			'idCourse'=>$this->getIdCourse(),
			'id_test'=>$idTest,
			'loModel'=>$object,
			'description'=>$test->description,
			'lo_type'=>LearningOrganization::OBJECT_TYPE_TEST,
			'isListViewLayout' => $isListViewLayout,
			'opt' => $opt
		), true, true);
		echo $html;
		Yii::app()->end();
	}



	public function actionEdit() {

		$resourceId = null;
		$centralRepoContext = false;

		// DataTables
		$cs = Yii::app()->getClientScript();
		$assetsUrl = $this->getPlayerAssetsUrl();//$this->module->assetsUrl;
		$cs->registerScriptFile($assetsUrl.'/js/jquery.dataTables.min.js');
		$cs->registerCssFile($assetsUrl.'/css/datatables.css');
		$cs->registerScriptFile($assetsUrl.'/js/testpoll.datatable_reordering.js');

		//load some external scripts for editing actions (LO info additional info custom thumbnails management)
		$basePath = Yii::getPathOfAlias('admin').'/protected';
		$assetsPath = Yii::app()->getAssetManager()->publish($basePath.'/../js/');
		$cs->registerScriptFile($assetsPath.'/scriptTur.js');
		$cs->registerScriptFile($assetsPath.'/scriptModal.js');
		$cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/Jcrop/jquery.Jcrop.min.js');
		$cs->registerCssFile(Yii::app()->theme->baseUrl.'/js/Jcrop/jquery.Jcrop.min.css');

		$idObject = (int)Yii::app()->request->getParam('id_object', 0);
		$object = LearningOrganization::model()->findByPk($idObject);
		$centralRepoContext = Yii::app()->request->getParam('opt', false);
		if (!$object || $centralRepoContext === 'centralrepo') {
			if($centralRepoContext === 'centralrepo'){
				$object = LearningRepositoryObject::model()->findByPk($idObject);
				if($object){
					$versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
						'id_object' => $object->id_object
					));

					if(!$versionModel || $versionModel->object_type !== LearningOrganization::OBJECT_TYPE_TEST){
						throw new CException('Invalid specified test');
					}

					$resourceId = $versionModel->id_resource;
					$centralRepoContext = true;
				}
			} else{
				throw new CException('Invalid specified test');
			}
		} else{
			$resourceId = $object->idResource;
		}

		$test = LearningTest::model()->findByPk($resourceId);
		if (!$test) {
			throw new CException('Invalid specified test');
		}

		$courseModel = null;

		if($this->getIdCourse()) {
			$courseModel = LearningCourse::model()->findByPk($this->getIdCourse());
		}

		$this->render("edit", array(
			'idTest' => $test->getPrimaryKey(),
			'test' => $test,
			'object' => $object,
			'courseModel' => $courseModel,
			'centralRepoContext' => $centralRepoContext
		));
	}



	public function actionAxGetQuestionsList() {
		$request = Yii::app()->request;
		$idTest = $request->getParam('id_test', 0);

		//try to prevent any possible sequence error
		LearningTestquest::fixTestSequence($idTest);

		//load questions through the test relation as we have now many-many relation
		$test = LearningTest::model()->findByPk($idTest);
		$questions = ($test !== null ? $test->questions : array());

		$records = array();
		foreach ($questions as $question) {
			$score = $question->getScore();
			$questionText = $this->formatQuestionText($question->title_quest, 80);
			$quest_ignored = false;
			if($question->type_quest == LearningTestquest::QUESTION_TYPE_BREAK_PAGE)
			{
				if(
					$test->display_type == LearningTest::DISPLAY_TYPE_SINGLE
					|| ($test->order_type != LearningTest::ORDER_TYPE_SEQUENCE && $test->order_type != LearningTest::ORDER_TYPE_RANDOM)
				)
					$quest_ignored = true;
			}

			$records[] = array(
				'id_question' => (int)$question->idQuest,
				'is_bank' => $question->is_bank,
				'id_category' => (int)$question->idCategory,
				'sequence' => (int)$question->sequence,
				'page' => (int)$question->page,
				'question' => ($question->is_bank ? '<i class="fa fa-database" title="'.Yii::t('test', 'Question bank').'"></i>&nbsp;'.$questionText : $questionText),
				'type' => $question->type_quest,
				'score' => ($score !== false ? round($score, 2) : ''),
				'quest_ignored' => $quest_ignored
			);
		}

		echo CJSON::encode(array('aaData' => $records));
	}



	/**
	 * Change question sequence
	 * @throws CException
	 */
	public function actionAxReorderQuestion() {

		$request = Yii::app()->request;

		$idTest = $request->getParam('id_test', 0);

		$qid = $request->getParam('id');
		$direction = $request->getParam('direction');
		$fromPosition = $request->getParam('fromPosition', -1); //sequence number of source
		$toPosition = $request->getParam('toPosition', -1); //sequence number of destination

		if ($fromPosition < 0 || $toPosition < 0) {
			throw new CException('Invalid position specification');
		}

		$checkDestination = LearningTestQuestRel::model()->findAllByAttributes(array('id_test' => $idTest, 'sequence' => $toPosition));

		if (empty($checkDestination) || count($checkDestination) != 1) {
			throw new CException('Invalid sequence index');
		}
		$destination = $checkDestination[0];

		$arr = explode('-', $qid);
		$idQuestion = (int)end($arr);
		$source = LearningTestQuestRel::getByTestQuest($idTest, $idQuestion);
		if (!$source) { throw new CException('Invalid question'); }

		$db = Yii::app()->db;
		$transaction = $db->beginTransaction();

		$ajaxResult = new AjaxResult();

		try {

			switch ($direction) {

				case 'back': {
					$newSequence = $destination->sequence;

					$command = $db->createCommand("UPDATE learning_test_quest_rel
						SET sequence = sequence+1
						WHERE id_test = :id_test
						AND sequence >= :sequence_begin
						AND sequence <= :sequence_end
						AND id_question <> :id_question");
					$command->bindParam(':id_test', $idTest, PDO::PARAM_INT);
					$command->bindParam(':sequence_begin', $toPosition, PDO::PARAM_INT);
					$command->bindParam(':sequence_end', $fromPosition, PDO::PARAM_INT);
					$command->bindParam(':id_question', $source->id_question, PDO::PARAM_INT);
					$rs = $command->query();
					if (!$rs) { throw new CException('Error while executing operation'); }

					$source->sequence = $newSequence;
					$rs = $source->save();
					if (!$rs) { throw new CException('Error while executing operation'); }

					if (!LearningTestquest::fixTestPages($idTest)) {
						throw new CException('Error while executing operation');
					}

				} break;

				case 'forward': {

					$newSequence = $destination->sequence;

					$command = $db->createCommand('UPDATE learning_test_quest_rel
						SET sequence = sequence-1
						WHERE id_test = :id_test
						AND sequence <= :sequence_begin
						AND sequence >= :sequence_end
						AND id_question <> :id_question');
					$command->bindParam(':id_test', $idTest, PDO::PARAM_INT);
					$command->bindParam(':sequence_begin', $toPosition, PDO::PARAM_INT);
					$command->bindParam(':sequence_end', $fromPosition, PDO::PARAM_INT);
					$command->bindParam(':id_question', $source->id_question, PDO::PARAM_INT);
					$rs = $command->query();
					if (!$rs) { throw new CException('Error while executing operation'); }

					$source->sequence = $newSequence;
					$rs = $source->save();
					if (!$rs) { throw new CException('Error while executing operation'); }

					if (!LearningTestquest::fixTestPages($idTest)) {
						throw new CException('Error while executing operation');
					}

				} break;

				default: { throw new CException('Invalid specified move direction'); } break;
			}

			$test = LearningTest::model()->findByPk($idTest);

			if($test->display_type == LearningTest::DISPLAY_TYPE_GROUPED && ($test->order_type == LearningTest::ORDER_TYPE_SEQUENCE || $test->order_type == LearningTest::ORDER_TYPE_RANDOM))
			{
				$question = LearningTestQuestRel::model()->findByAttributes(array('id_test' => $idTest, 'id_question' => $idQuestion));

				$page_sequence = LearningTestQuestRel::model()->countByAttributes(array('id_test' => $idTest, 'page' => $question->page)) + 1;

				$query = "
					INSERT IGNORE INTO ".LearningTesttrackQuest::model()->tableName()."
					SELECT t.idTrack, ".$question->id_question.", ".$question->page.", ".$page_sequence.", NULL
					FROM ".LearningTesttrack::model()->tableName()." AS t
					JOIN ".LearningTesttrackPage::model()->tableName()." AS tp ON tp.idTrack = t.idTrack
					WHERE t.idTest = ".(int)$idTest."
					AND tp.page = ".(int)$question->page."
					AND t.score_status IN ('".LearningTesttrack::STATUS_DOING."', '".LearningTesttrack::STATUS_NOT_COMPLETE."')";

				if(Yii::app()->db->createCommand($query)->execute() === false)
					throw new CException('Error while executing operation');

				//Check if there's some running test to update
				$query = "
					UPDATE ".LearningTesttrackQuest::model()->tableName()." AS tq
					JOIN ".LearningTestQuestRel::model()->tableName()." AS r ON r.id_question = tq.idQuest AND r.id_test = ".(int)$idTest."
					JOIN ".LearningTesttrack::model()->tableName()." as t on t.idTrack = tq.idTrack
					SET tq.page = r.page,
					tq.quest_sequence = r.sequence
					WHERE t.idTest = ".(int)$idTest."
					AND t.score_status IN ('".LearningTesttrack::STATUS_DOING."', '".LearningTesttrack::STATUS_NOT_COMPLETE."')";

				if(Yii::app()->db->createCommand($query)->execute() === false)
					throw new CException('Error while executing operation');
			}
			elseif($test->display_type == LearningTest::DISPLAY_TYPE_SINGLE && $test->order_type == LearningTest::ORDER_TYPE_SEQUENCE)
			{
				$question = LearningTestQuestRel::model()->findByAttributes(array('id_test' => $idTest, 'id_question' => $idQuestion));
				$question_details = LearningTestQuest::model()->findByAttributes(array('idQuest' => $idQuestion));

				if($question_details->type_quest != LearningTestQuest::QUESTION_TYPE_BREAK_PAGE && $question_details->type_quest != LearningTestQuest::QUESTION_TYPE_TITLE)
				{
					$query = "
						INSERT IGNORE INTO ".LearningTesttrackQuest::model()->tableName()."
						SELECT t.idTrack, ".$question->id_question.", ".$question->sequence.", ".$question->sequence.", NULL
						FROM ".LearningTesttrack::model()->tableName()." AS t
						JOIN ".LearningTesttrackPage::model()->tableName()." AS tp ON tp.idTrack = t.idTrack
						WHERE t.idTest = ".(int)$idTest."
						AND tp.page = ".(int)$question->sequence."
						AND t.score_status IN ('".LearningTesttrack::STATUS_DOING."', '".LearningTesttrack::STATUS_NOT_COMPLETE."')";

					if(Yii::app()->db->createCommand($query)->execute() === false)
						throw new CException('Error while executing operation');

					//Check if there's some running test to update
					$query = "
						UPDATE ".LearningTesttrackQuest::model()->tableName()." AS tq
						JOIN ".LearningTestQuestRel::model()->tableName()." AS r ON r.id_question = tq.idQuest AND r.id_test = ".(int)$idTest."
						JOIN ".LearningTesttrack::model()->tableName()." as t on t.idTrack = tq.idTrack
						SET tq.page = r.sequence,
						tq.quest_sequence = r.sequence
						WHERE t.idTest = ".(int)$idTest."
						AND t.score_status IN ('".LearningTesttrack::STATUS_DOING."', '".LearningTesttrack::STATUS_NOT_COMPLETE."')";

					if(Yii::app()->db->createCommand($query)->execute() === false)
						throw new CException('Error while executing operation');
				}
			}

			$transaction->commit();

			$ajaxResult->setStatus(true);

		} catch (CException $e) {

			$transaction->rollback();
			$ajaxResult->setStatus(false)->setMessage(Yii::t('standard', '_OPERATION_FAILURE').' - '.$e->getMessage());
			//throw $e;
		}

		$ajaxResult->toJSON();
	}



	/**
	 * Manage test options in editing section
	 */
	public function actionTestOptions() {

		$rq = Yii::app()->request;

		$idTest = $this->getIdTest();
		$test = $this->test;
		$object = $this->object;

		$opt = Yii::app()->request->getParam('opt', false);
		$id_object = $this->getIdReference();

		if($opt !== false && $opt === 'centralrepo'){
			$versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
				'id_resource' => $idTest,
				'object_type' => LearningOrganization::OBJECT_TYPE_TEST
			));

			if($versionModel){
				$id_object = $versionModel->id_object;
			}
		}

		$backUrl = $this->createUrl('edit', array('id_object' => $id_object, 'opt' => $opt && $opt === 'centralrepo' ? 'centralrepo' : ''));

		if ($rq->getParam('undo', false) !== false) {
			//just go back
			$this->redirect($backUrl);
		}

		$disablePageView = (LearningTesttrack::model()->countByAttributes(array('idTest' => $idTest, 'score_status' => array(LearningTesttrack::STATUS_DOING, LearningTesttrack::STATUS_NOT_COMPLETE))) > 0)? true : false;
		if ($rq->getParam('save', false) !== false) {

			try {

				$newDisplayType = $rq->getParam('display_type', LearningTest::DISPLAY_TYPE_GROUPED);
				$newOrderType = $rq->getParam('order_type', LearningTest::ORDER_TYPE_SEQUENCE);
				$shuffleAnswer = $rq->getParam('shuffle_answer', LearningTest::SHUFFLE_ANSWER_SEQUENCE);

				switch ($newDisplayType) {
					case LearningTest::DISPLAY_TYPE_GROUPED:
					case LearningTest::DISPLAY_TYPE_SINGLE: break;
					default: {
						throw new CException('Invalid "display type" option');
					} break;
				}

				switch ($newOrderType) {
					case LearningTest::ORDER_TYPE_SEQUENCE:
					case LearningTest::ORDER_TYPE_RANDOM:
					case LearningTest::ORDER_TYPE_RANDOM_SUBGROUP:
					case LearningTest::ORDER_TYPE_RANDOM_CATEGORY: break;
					default: {
						throw new CException('Invalid "order answer" option');
					} break;
				}

				switch ($shuffleAnswer) {
					case LearningTest::SHUFFLE_ANSWER_SEQUENCE:
					case LearningTest::SHUFFLE_ANSWER_RANDOM: break;
					default: {
						throw new CException('Invalid "shuffle answer" option');
					} break;
				}

				$newOrderInfo = "";
				if ($newOrderType == LearningTest::ORDER_TYPE_RANDOM_CATEGORY) {
					$orderInfoInput = $rq->getParam('question_random_category', array());
					$orderInfoOutput = array();
					foreach ($orderInfoInput as $idCategory => $numQuestions) {
						$orderInfoOutput[] = array('id_category' => (int)$idCategory, 'selected' => (int)$numQuestions);
					}
					$newOrderInfo = CJSON::encode($orderInfoOutput);
				}

				if(!$disablePageView)
				{
					$test->display_type = $newDisplayType;
					$test->order_type = $newOrderType;
					$test->order_info = $newOrderInfo;
					$question_random_number = intval($rq->getParam('question_random_number', 0));
					$test->question_random_number = ($question_random_number > 0) ? $question_random_number : $test->getNumberOfQuestions();
				}
				$test->shuffle_answer = $shuffleAnswer;
				$test->mandatory_answer = $rq->getParam('mandatory_answer', 0) > 0 ? 1 : 0;
				$test->hide_info = $rq->getParam('hide_info', 0) > 0 ? 1 : 0;
				$test->mod_doanswer = $rq->getParam('mod_doanswer', 0) > 0 ? 1 : 0;
				$test->can_travel = $rq->getParam('can_travel', 0) > 0 ? 1 : 0;
				$test->save_keep = $rq->getParam('save_keep', 0) > 0 ? 1 : 0;
				$maxAttempts = (int)$rq->getParam('max_attempt', 0);
				$test->max_attempt = ($maxAttempts < 0 ? 0 : $maxAttempts);
				if ($maxAttempts > 0) {
					$useSuspension = ($rq->getParam('use_suspension', 0) > 0);
					$test->use_suspension = ($useSuspension ? 1 : 0);
					if ($useSuspension) {
						$suspensionNumHours = (int)$rq->getParam('suspension_num_hours', 0);
						if ($suspensionNumHours < 0) { throw new CException('Invalid suspension number of hours (it cannot be less then 0)'); }
						$test->suspension_num_hours = $suspensionNumHours;
						$test->suspension_prerequisites = ($rq->getParam('suspension_prerequisites', 0) > 0 ? 1 : 0);
					}
				}
				$test->show_score = $rq->getParam('show_score', 0) > 0 ? 1 : 0;
				$test->show_score_cat = $rq->getParam('show_score_cat', 0) > 0 ? 1 : 0;
				$test->show_doanswer = (int)$rq->getParam('show_doanswer', 0);
				$test->show_solution = (int)$rq->getParam('show_solution', LearningTest::SHOW_SOLUTION_YES);
				$test->show_solution_date = $rq->getParam('show_solution_date', NULL);
				$test->show_users_avg = $rq->getParam('show_users_avg', 0);

				if ($test->show_solution != LearningTest::SHOW_SOLUTION_YES_FROM_DATE && $test->show_solution_date)
					$test->show_solution_date = null;
				$rs = $test->save();
				if (!$rs) {
					throw new CException('Error while saving data');
				}
				// we allow the custom plugin to save plugin settings to settings of test in course
				Yii::app()->event->raise('SaveUserChoices', new DEvent($this, array('post'=>$_POST)));

			} catch (CException $e) {

				Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
				$this->redirect($backUrl);
			}

			Yii::app()->user->setFlash('success', Yii::t('standard', '_OPERATION_SUCCESSFUL'));
			$this->redirect($backUrl);

		} else {

			$test = LearningTest::model()->findByPk($idTest);
			$questions = $test->questionsOnly ? $test->questionsOnly : array();

			$categories = array();
			foreach (LearningQuestCategory::model()->findAll() as $category) {
				$categories[(int)$category->getPrimaryKey()] = $category->name;
			}

			$questionCategories = array();
			if (!empty($questions)) {
				foreach ($questions as $question) {
					$idCategory = (int)$question->idCategory;
					if (!isset($questionCategories[$idCategory])) {
						$name = ($idCategory == 0 ? Yii::t('standard', '_NO_CATEGORY') : $categories[$idCategory]);
						$questionCategories[$idCategory] = array(
							'idCategory' => $idCategory,
							'name' => $name,
							'total' => 0
						);
					}
					$questionCategories[$idCategory]['total']++;
				}
			}
			if (!empty($questionCategories)) {
				uasort($questionCategories, function($a, $b) {
					if ($a['idCategory'] == 0) return -1;
					return strcasecmp($a['name'], $b['name']);
				});
			}

			$orderInfo = array();
			$decodedOrderInfo = (!empty($test->order_info) ? CJSON::decode($test->order_info) : false);
			if (!empty($decodedOrderInfo)) {
				foreach ($decodedOrderInfo as $orderItem) {
					$orderInfo[(int)$orderItem['id_category']] = (int)$orderItem['selected'];
				}
			}

			$this->render('test_options', array(
				'idTest' => $idTest,
				'test' => $test,
				'backUrl' => $backUrl,
				'questions' => $questions,
				'categories' => $categories,
				'questionCategories' => $questionCategories,
				'orderInfo' => $orderInfo,
				'disablePageView' => $disablePageView
			));

		}

	}



	/**
	 * Manage time administration options
	 */
	public function actionTimeAdministration() {

		$rq = Yii::app()->request;


		$idTest = $this->getIdTest();//(int)$rq->getParam('id_test', 0);

		$opt = $rq->getParam('opt', false);
		$id_object = $this->getIdReference();

		if($opt !== false && $opt === 'centralrepo'){
			$versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
				'id_resource' => $idTest,
				'object_type' => LearningOrganization::OBJECT_TYPE_TEST
			));

			if($versionModel){
				$id_object = $versionModel->id_object;
			}
		}

		$backUrl = $this->createUrl('edit', array('course_id' => $this->getIdCourse(), 'id_object' => $id_object, 'opt' => $opt && $opt === 'centralrepo' ? 'centralrepo': ''));

		if ($rq->getParam('undo', false) !== false) {
			//just go back
			$this->redirect($backUrl);
		}

		if ($rq->getParam('save', false) !== false) {

			$db = Yii::app()->db;
			$transaction = $db->beginTransaction();

			try {

				$test = $this->test;//LearningTest::model()->findByPk($idTest);
				if (!$test) {
					throw new CException('Invalid test');
				}


				$timeLimit = $rq->getParam('time_limit', 0);
				switch ($timeLimit) {
					case LearningTest::TIME_YES: {
						$test->time_dependent = 1;
						$test->time_assigned = $rq->getParam('time_assigned', 0);
					} break;
					case LearningTest::TIME_YES_QUEST: {
						$test->time_dependent = 2;
						$test->display_type = LearningTest::DISPLAY_TYPE_SINGLE; //in this modality, only single question mode is possible, so force it

						$timeAssignment = $rq->getParam('time_assignment', 2);
						$testQuestions = $test->getQuestions();

						switch ($timeAssignment) {
							case 0: { //by difficulty
								$totalDifficulty = 0;
//								$newQuestionsInfo = $rq->getParam('questions', array());
								$json =  $rq->getParam('jsonQuestions', null);
								$newQuestionsInfo = json_decode($json, true);
								$newQuestionsInfo = $newQuestionsInfo['questions'];
								foreach ($testQuestions as $testQuestion) {
									$idQuestion = $testQuestion->getPrimaryKey();
									$newDifficulty = (int)$newQuestionsInfo[$idQuestion]['difficulty'];
									$totalDifficulty += (LearningTestquest::isValidDifficulty($newDifficulty)
										? $newDifficulty
										: LearningTestquest::DIFFICULTY_MEDIUM);
								}
								$newTime = (int)$rq->getParam('new_time', 0);
							} break;
							case 1:  { //by questions number
								$totalQuestions = count($testQuestions);
								$newTime = (int)$rq->getParam('new_time', 0);
							} break;
							case 2:  { //by manual assignment
//								$newQuestionsInfo = $rq->getParam('questions', array());
								$json =  $rq->getParam('jsonQuestions', null);
								$newQuestionsInfo = json_decode($json, true);
							    $newQuestionsInfo = $newQuestionsInfo['questions'];
							} break;
						}

						foreach ($testQuestions as $testQuestion) {
							$idQuestion = $testQuestion->getPrimaryKey();
							switch ($timeAssignment) {
								case 0: { //by difficulty
									$newDifficulty = (int)$newQuestionsInfo[$idQuestion]['difficulty'];
									$testQuestion->difficult = (LearningTestquest::isValidDifficulty($newDifficulty)
										? $newDifficulty
										: LearningTestquest::DIFFICULTY_MEDIUM
									);
									$testQuestion->time_assigned = (int)(( $newTime / $totalDifficulty ) * $testQuestion->difficult);
								} break;
								case 1:  { //by questions number
									$testQuestion->time_assigned = (int)($newTime / $totalQuestions);
								} break;
								case 2:  { //by manual assignment
									if (isset($newQuestionsInfo[$idQuestion])) {
										$newDifficulty = (int)$newQuestionsInfo[$idQuestion]['difficulty'];
										$newTime = (int)$newQuestionsInfo[$idQuestion]['time'];
									} else {
										throw new CException('Invalid input data');
									}
									$testQuestion->difficult = (LearningTestquest::isValidDifficulty($newDifficulty) ? $newDifficulty : LearningTestquest::DIFFICULTY_MEDIUM);
										$testQuestion->time_assigned = ($newTime > 0 ? $newTime : 0); //avoid possible negative numbers
								} break;
							}
							if (!$testQuestion->save()) {
								throw new CException('Error while saving question time info');
							}
						}

					} break;
					case LearningTest::TIME_NO:
					default: {
						//assume "No time limit" as default option
						$test->time_dependent = 0;
					} break;
				}

				$rs = $test->save();
				if (!$rs) { throw new CException('Error while saving time options'); }

				$transaction->commit();

			} catch(CException $e) {

				$transaction->rollback();
				Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
				$this->redirect($backUrl);
			}

			Yii::app()->user->setFlash('success', Yii::t('standard', '_OPERATION_SUCCESSFUL'));
			$this->redirect($backUrl);

		} else {

			$this->render('time_administration', array(
				'idTest' => $idTest,
				'test' => $this->test,
				'backUrl' => $backUrl
			));

		}
	}





	public function actionPointsAdministration() {

		$rq = Yii::app()->request;

		$idTest = $this->getIdTest();

		$opt = $rq->getParam('opt', false);

		$id_object = $this->getIdReference();

		if($opt !== false && $opt === 'centralrepo'){
			$versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
				'id_resource' => $idTest,
				'object_type' => LearningOrganization::OBJECT_TYPE_TEST
			));

			if($versionModel){
				$id_object = $versionModel->id_object;
			}
		}

		$backUrl = $this->createUrl('edit', array('course_id' => $this->getIdCourse(), 'id_object' => $id_object, 'opt' => $opt && $opt === 'centralrepo' ? 'centralrepo' : ''));

		// If user is already started a test we disable "Set final score calculation" option for the current test.
		$learningTrackModel = LearningTesttrack::model()->findByAttributes(array("idTest" => $idTest));
		$disableFinalScoreCalculation = false;

		if($learningTrackModel || count($learningTrackModel) > 0){
			$disableFinalScoreCalculation = true;
		}

		if ($rq->getParam('undo', false) !== false) {
			//just go back
			$this->redirect($backUrl);
		}

		if ($rq->getParam('save', false) !== false) {

			$transaction = Yii::app()->db->beginTransaction();

			try {

				$this->test->point_required = $rq->getParam('point_required', 0);

				if(!$disableFinalScoreCalculation) {
					$this->test->point_type = $rq->getParam('point_type', LearningTest::POINT_TYPE_NORMAL);
				}

				$this->test->point_assignment = $rq->getParam('point_assignment', LearningTest::POINT_ASSIGNMENT_DO_NOTHING);
				if (!$this->test->save()) { throw new CException('Error while updating test'); }

				$questions = $this->test->getQuestions();
				if (!empty($questions)) {

					if ($this->test->point_assignment != LearningTest::POINT_ASSIGNMENT_DO_NOTHING) {

//						$newQuestionsInfo = $rq->getParam('questions', array());
						$json =  $rq->getParam('jsonQuestions', null);
						$newQuestionsInfo = json_decode($json, true);
						$newQuestionsInfo = $newQuestionsInfo['questions'];
						switch ($this->test->point_assignment) {
							case LearningTest::POINT_ASSIGNMENT_TEST_PM_DIFFICULT: { //by difficulty
								$totalDifficulty = 0;
								foreach ($questions as $question) {
									$totalDifficulty += (LearningTestquest::isValidDifficulty($question->difficult)
										? $question->difficult
										: LearningTestquest::DIFFICULTY_MEDIUM);
								}
								$newMaxScore = (double)$rq->getParam('new_max_score', 0);
							} break;
							case LearningTest::POINT_ASSIGNMENT_TEST_PM_EQUAL_TO_ALL:  { //by questions number
								$totalQuestions = count($questions);
								$newMaxScore = (double)$rq->getParam('new_max_score', 0);
							} break;
							case LearningTest::POINT_ASSIGNMENT_TEST_PM_MANUAL:  { //by manual assignment

//								$newQuestionsInfo = $rq->getParam('questions', array());
								$json =  $rq->getParam('jsonQuestions', null);
								$newQuestionsInfo = json_decode($json, true);
								$newQuestionsInfo = $newQuestionsInfo['questions'];
							} break;

							case LearningTest::POINT_ASSIGNMENT_TEST_PM_EQUAL_TO_ALL_VISIBLE: {
								$totalQuestions = $this->test->getNumberOfQuestionsForStudent();
								$newMaxScore = (double)$rq->getParam('new_max_score', 0);
							} break;

						}



						foreach ($questions as $question) {

							$idQuestion = $question->getPrimaryKey();
							$questionManager = CQuestionComponent::getQuestionManager($question->type_quest);

							$newDifficulty = (isset($newQuestionsInfo[$idQuestion]['difficulty']) ? $newQuestionsInfo[$idQuestion]['difficulty'] : false);
							if ($newDifficulty !== false && LearningTestquest::isValidDifficulty($newDifficulty)) {
								if ($question->difficult != $newDifficulty) {
									$question->difficult = $newDifficulty;
									if (!$question->save()) {
										throw new CException('Error while updating question score');
									}
								}
							}

							switch ($this->test->point_assignment) {
								case LearningTest::POINT_ASSIGNMENT_TEST_PM_DIFFICULT: {
									$newQuestionScore = round(($newMaxScore / $totalDifficulty ) * $question->difficult, 2);
								} break;
								case LearningTest::POINT_ASSIGNMENT_TEST_PM_EQUAL_TO_ALL: {
									$newQuestionScore = round($newMaxScore / $totalQuestions, 2);
								} break;
								case LearningTest::POINT_ASSIGNMENT_TEST_PM_MANUAL: {
									$newQuestionScore = (isset($newQuestionsInfo[$idQuestion]['score'])
										? (double)$newQuestionsInfo[$idQuestion]['score']
										: 0.0);
								} break;
								case LearningTest::POINT_ASSIGNMENT_TEST_PM_EQUAL_TO_ALL_VISIBLE: {
									$newQuestionScore = round($newMaxScore / $totalQuestions, 2);
								} break;
							}
							$questionManager->setMaxScore($idQuestion, $newQuestionScore);

						}

					}
				}

				$transaction->commit();

			} catch(CException $e) {

				$transaction->rollback();
				Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE')/*$e->getMessage()*/);
				$this->redirect($backUrl);
			}

			Yii::app()->user->setFlash('success', Yii::t('standard', '_OPERATION_SUCCESSFUL'));
			$this->redirect($backUrl);

		}

		$this->render('points_administration', array(
			'idTest' => $idTest,
			'test' => $this->test,
			'backUrl' => $backUrl,
			'disableFinalScoreCalculation' => $disableFinalScoreCalculation
		));

	}




	public function actionFeedbackManagement() {

		// todo: set this
		$allowAdminOperations = true;

		$rq = Yii::app()->request;

		$idTest = intval($rq->getParam('id_test'));

		$test = LearningTest::model()->findByPk($idTest);
		$object = LearningOrganization::model()->findByAttributes(array(
			'idResource' => $idTest,
			'objectType' => LearningOrganization::OBJECT_TYPE_TEST,
			'idCourse'   => $this->getIdCourse()
		));

		$opt = $rq->getParam('opt', false);
		if($opt !== false && $opt === 'centralrepo'){
			$versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
				'id_resource' => $idTest,
				'object_type' => LearningOrganization::OBJECT_TYPE_TEST
			));

			if($versionModel){
				$object = LearningRepositoryObject::model()->findByPk($versionModel->id_object);
			}
		}
		if (!$test || !$object) { throw new CException('Invalid test'); }

		$backUrl = $this->createUrl('edit', array('id_object' => $object->getPrimaryKey(), 'opt' => $opt && $opt === 'centralrepo' ? 'centralrepo' : ''));

		try {

			$test = LearningTest::model()->findByPk($idTest);
			if (!$test) {
				throw new CException('Invalid test');
			}

		} catch(CException $e) {

			Yii::app()->user->setFlash('test-time-administration-failure', $e->getMessage());
			$this->redirect($backUrl);
		}

		$model = null;
		$idFeedback = Yii::app()->request->getParam('id_feedback');

		if (intval($idFeedback) > 0) {
			$model = LearningTestFeedback::model()->findByPk($idFeedback);
		}
		if (!$model) {
			// initialize if empty
			$model = new LearningTestFeedback();
			$model->id_test = $idTest;
		}

		$this->render('feedback_management', array(
			'model' => $model,
			'idTest' => $idTest,
			'test' => $test,
			'backUrl' => $backUrl,
			'allowAdminOperations' => $allowAdminOperations,
			'opt' => $opt
			/*'htmlFeedbackList' => $this->renderPartial('_feedback_list', array(
				'allowAdminOperations' => $allowAdminOperations,
				'dataProvider' => LearningTestFeedback::model()->getDataProvider($idTest),
			), true)*/
		));
	}



	public function actionAddFeedback() {

		$response = new AjaxResult();

		if (isset($_POST['LearningTestFeedback'])) {
			$postData = $_POST['LearningTestFeedback'];

			$model = LearningTestFeedback::model()->findByPk($postData['id_feedback']);
			if (!$model) {
				$model = new LearningTestFeedback();
			}

			$model->attributes = $_POST['LearningTestFeedback'];
			$model->feedback_text = Yii::app()->htmlpurifier->purify($model->feedback_text);

			if ($model->save()) {
				$response->setStatus(true);
			}
			else {
				$response->setStatus(false);
				$response->setMessage('');
			}
		}

		$response->toJSON();
	}

	public function actionAxDeleteFeedback() {

		$model = LearningTestFeedback::model()->findByPk( Yii::app()->request->getParam('id') );

		if (!$model) {
			echo Yii::t('standard', '_OPERATION_FAILURE');
			Yii::app()->end();
		}

		$closeDialog = false;

		if (Yii::app()->request->isPostRequest) {
			try {
				$closeDialog = $model->delete();

			} catch (CException $e) {

				echo $e->getMessage();
				Yii::app()->end();
			}
		}

		$this->renderPartial('_delete_feedback', array(
			'model' => $model,
			'closeDialog' => $closeDialog
		));
	}


	public function actionAxGetFeedbackList() {
		// todo: set this
		$allowAdminOperations = true;

		$idTest = intval(Yii::app()->request->getParam('id_test'));

		$this->render('feedback_management', array(
			'idTest' => $idTest,
			'htmlFeedbackList' => $this->renderPartial('_feedback_list', array(
				'allowAdminOperations' => $allowAdminOperations,
				'dataProvider' => LearningTestFeedback::model()->getDataProvider($idTest),
			), true)
		));
	}



	/**
	 * Change test title and description
	 */
	public function actionAxEditTitle() {

		$rq = Yii::app()->request;
		$idTest = $rq->getParam('id_test', 0);
		$test = LearningTest::model()->findByPk($idTest);
		$object = LearningOrganization::model()->findByAttributes(array(
			'idResource' => $idTest,
			'objectType' => LearningOrganization::OBJECT_TYPE_TEST,
			'idCourse'   => $this->getIdCourse()
		));

		if ($rq->getParam('confirm', false) !== false) {
			$test->title = $rq->getParam('title', "");
			$test->description = $rq->getParam('description', "");
			if (!$test->save()) {
				echo '<a class="auto-close error"></a>';
				return;
			} else {

				$object->title = $test->title ;
				$object->saveNode();

				echo CHtml::link('', FALSE, array(
					'class' => 'auto-close success',
					'data-new_title' => $test->title
				));
				return;
			}
		}

		$this->renderPartial('_edit_title', array(
			//'idPoll' => $idPoll,
			'test' => $test
		));

	}



	public function actionExportAsGift() {

		$rq = Yii::app()->request;
		$idTest = $rq->getParam('id_test', 0);
		$test = LearningTest::model()->findByPk($idTest);
		if (!$test) {
			throw new CException('Invalid test');
		}

		$id_quests = Yii::app()->db->createCommand('select id_question from '.LearningTestQuestRel::model()->tableName().' where id_test = '.$idTest)->queryColumn();

        $criteria = new CDbCriteria();
        $criteria->condition = 'id_test = :id_test';
        $criteria->params = array(':id_test' => $idTest);
		$criteria->order = "sequence ASC";
		$criteria->with = array('testAssociation');
		$questions = LearningTestquest::model()->findAllByAttributes(array('idQuest' => $id_quests), $criteria);

		$output = '';
		$gift = new GiftFormatManager();
		foreach ($questions as $question) {
			$questionManager = CQuestionComponent::getQuestionManager($question->type_quest);
			$oRawQuest = $questionManager->exportToRaw($question);
			if ($oRawQuest) {
				$output .= $gift->writeQuestion($oRawQuest);
			}
		}

		//send output ...
		$fileName = $test->title.'_export_'.date("Y-m-d").'.txt';
    	Yii::app()->request->sendFile($fileName, $output);
	}



	public function actionImportAsGift() {

		$rq = Yii::app()->request;
		$idTest = $rq->getParam('id_test', 0);

		$test = LearningTest::model()->findByPk($idTest);
		$object = LearningOrganization::model()->findByAttributes(array(
			'idResource' => $idTest,
			'objectType' => LearningOrganization::OBJECT_TYPE_TEST,
			'idCourse'   => $this->getIdCourse()
		));

		$opt= $rq->getParam('opt', false);

		if($opt !== false && $opt === 'centralrepo'){
			$versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
				'id_resource' => $idTest,
				'object_type' => LearningOrganization::OBJECT_TYPE_TEST
			));

			if($versionModel){
				$object = LearningRepositoryObject::model()->findByPk($versionModel->id_object);
			}
		}
		if (!$test || !$object) { throw new CException('Invalid test'); }

		$backUrl = $this->createUrl('edit', array('id_object' => $object->getPrimaryKey(), 'opt' => $opt && $opt === 'centralrepo' ? 'centralrepo' : ''));

		$this->render('_import_as_gift', array(
			'idTest' => $idTest,
			'test' => $test,
			'backUrl' => $backUrl,
			'opt' => $opt
		));
	}



	public function actionDoImportAsGift() {
		$rq = Yii::app()->request;
		$idTest = (int)$rq->getParam('id_test', 0);
		$idCategory = (int)$rq->getParam('id_category', 0);

		$test = LearningTest::model()->with('organization')->findByPk($idTest);
		$object = $test ? $test->organization : false;

		$opt = $rq->getParam('opt', false);

		if($opt !== false && $opt === 'centralrepo'){
			$versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
				'id_resource' => $idTest,
				'object_type' => LearningOrganization::OBJECT_TYPE_TEST
			));

			if($versionModel){
				$object = LearningRepositoryObject::model()->findByPk($versionModel->id_object);
			}
		}
		if (!$test || !$object) { throw new CException('Invalid test'); }

		$backUrl = $this->createUrl('edit', array('id_object' => $object->getPrimaryKey(), 'opt' => $opt && $opt === 'centralrepo' ? 'centralrepo' : ''));

		if ($rq->getParam('undo', false) !== false) { $this->redirect($backUrl); }

		if ($rq->getParam('save', false) === false) {
			//perform import action

			//check if a valid file has been uploaded
			if (!isset($_FILES['gift_file']) || $_FILES['gift_file']['name'] == "" || $_FILES['gift_file']['tmp_name'] == "") {
				Yii::app()->user->setFlash('error', Yii::t('test', '_TEST_FILE_NOT_ATTACH'));
				$url = $this->createUrl('importAsGift', array('id_test' => $idTest, 'opt' => $opt && $opt === 'centralrepo' ? 'centralrepo' : ''));
				$this->redirect($url);
				return;
			}

			$file = file($_FILES['gift_file']['tmp_name']);

			$gift = new GiftFormatManager();
			$formatted = $gift->readQuestions($file);

			if (!empty($formatted)) {

				$transaction = Yii::app()->db->beginTransaction();

				try {

					$sequence = $test->getMaxSequence() + 1;
					$page = $test->getMaxPage();
					$question_types = LearningTestquest::getQuestionTypesList();
					if ($page <= 0) { $page = 1; }

					foreach ($formatted as $question) {
						if(!in_array($question->qtype, $question_types))
							continue;

						if ((int)$idCategory > 0 && is_object($question)) {
							$question->id_category = (int)$idCategory;
						}

						$questionManager = CQuestionComponent::getQuestionManager($question->qtype);

						if ($questionManager) {
							$rs = $questionManager->importFromRaw($question, $idTest, $sequence, $page);
							if (!$rs) { throw new CException('Error while importing questions'); }
						}

						$sequence++; //increment question sequence at every imported row
					}

					//make sure that values for questions sequence and page are correct
					LearningTestquest::fixTestSequence($idTest);
					LearningTestquest::fixTestPages($idTest);

					if(isset($object->openmode)) {
						$object->visible = 1;
						$object->saveNode();
					}

					$transaction->commit();
					Yii::app()->user->setFlash('success', Yii::t('standard', '_OPERATION_SUCCESSFUL'));

				} catch (CException $e) {

					$transaction->rollback();
					Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
				}

			}
		}

		$this->redirect($backUrl);
	}



	/**
	 * Register Controller specific assets
	 *
	 * (non-PHPdoc)
	 * @see TestBaseController::registerResources()
	 */
	public function registerResources() {
		parent::registerResources();

		if (!Yii::app()->request->isAjaxRequest) {
			/* @var $cs CClientScript */
			$cs = Yii::app()->getClientScript();
			//$cs->registerCssFile($this->module->assetsUrl . '/css/testpoll.css');
		}

	}

}