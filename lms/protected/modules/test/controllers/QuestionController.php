<?php


class QuestionController extends TestBaseController {

	protected $questionType = '';

	protected $useCategory = true;
	protected $useDifficulty = true;
	protected $useShuffle = false;
	protected $useAnsweringMaxTime = true;
	protected $useExtraInfo = false;


	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters()
	{
		return array(
				'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules()
	{
		return array(

			array('allow',
					'users'=>array('@'),
					'expression' => '(Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess("/lms/admin/course/mod") || Yii::app()->controller->userCanAdminCourse)',
			),

			array('deny', // if some permission is wrong -> the script goes here
					'users' => array('*'),
					'deniedCallback' => array($this, 'accessDeniedCallback'),
					'message' => Yii::t('course', '_NOENTER'),
			),
		);
	}



	//get / set methods

	public function getQuestionType() {
		return $this->questionType;
	}

	public function setQuestionType($value) {
		$this->questionType = $value;
	}

	public function getUseCategory() {
		return $this->useCategory;
	}

	public function setUseCategory($value) {
		$this->useCategory = (bool)$value;
	}

	public function getUseDifficulty() {
		return $this->useDifficulty;
	}

	public function setUseDifficulty($value) {
		$this->useDifficulty = (bool)$value;
	}

	public function getUseShuffle() {
		return $this->useShuffle;
	}

	public function setUseShuffle($value) {
		$this->useShuffle = (bool)$value;
	}

	public function getUseAnsweringMaxTime() {
		return $this->useAnsweringMaxTime;
	}

	public function setUseAnsweringMaxTime($value) {
		$this->useAnsweringMaxTime = (bool)$value;
	}

	public function getUseExtraInfo() {
		return $this->useExtraInfo;
	}

	public function setUseExtraInfo($value) {
		$this->useExtraInfo = (bool)$value;
	}



	//events

	public function beforeAction($event) {

		$rq = Yii::app()->request;

		switch ($event->id) {
			case 'create': {
				$questionType = $rq->getParam('question_type', false);
			} break;
			case 'update':
			case 'axDelete': {
				$idQuestion = $rq->getParam('id_question', false);
				$question = LearningTestquest::model()->findByPk($idQuestion);
				if (!$question) { throw new CException('Invalid question'); }
				$questionType = $question->type_quest;
			} break;
			default: {
				return true;
			} break;
		}

		if (!LearningTestquest::isValidType($questionType)) {
			throw new CException('Invalid question');
		}

		$questionManager = CQuestionComponent::getQuestionManager($questionType);
		if (!$questionManager) {
			throw new CException('Invalid question');
		}

		$questionManager->setController($this);

		switch ($event->id) {
			case 'create': {
				$this->attachEventHandler('onAfterQuestionCreate', array($questionManager, 'create'));
				$this->attachEventHandler('onBeforeQuestionEditRender', array($questionManager, 'beforeEdit'));
				$this->attachEventHandler('onAfterQuestionEditRender', array($questionManager, 'edit'));
			} break;
			case 'update': {
				$this->attachEventHandler('onAfterQuestionUpdate', array($questionManager, 'update'));
				$this->attachEventHandler('onBeforeQuestionEditRender', array($questionManager, 'beforeEdit'));
				$this->attachEventHandler('onAfterQuestionEditRender', array($questionManager, 'edit'));
			} break;
			case 'axDelete': {
				$this->attachEventHandler('onAfterQuestionDelete', array($questionManager, 'delete'));
			} break;
		}

        Yii::app()->event->raise("RegisterTestQuestionHandlers", new DEvent($this, array('event' => $event)));

		return parent::beforeAction($event);
	}

	//custom events

	public function onBeforeQuestionCreate($event) {
		$this->raiseEvent('onBeforeQuestionCreate', $event);
	}

	public function onAfterQuestionCreate($event) {
		$this->raiseEvent('onAfterQuestionCreate', $event);
	}

	public function onBeforeQuestionUpdate($event) {
		$this->raiseEvent('onBeforeQuestionUpdate', $event);
	}

	public function onAfterQuestionUpdate($event) {
		$this->raiseEvent('onAfterQuestionUpdate', $event);
	}

	public function onBeforeQuestionDelete($event) {
		$this->raiseEvent('onBeforeQuestionDelete', $event);
	}

	public function onAfterQuestionDelete($event) {
		$this->raiseEvent('onAfterQuestionDelete', $event);
	}

	public function onBeforeQuestionEditrender($event) {
		$this->raiseEvent('onBeforeQuestionEditRender', $event);
	}

	public function onAfterQuestionEditRender($event) {
		$this->raiseEvent('onAfterQuestionEditRender', $event);
	}



	//other methods

	public function init() {
		parent::init();
		Yii::app()->tinymce->init();
	}



	//actions

	public function actionCreate() {

		$request = Yii::app()->request;

		$idTest = (int)$request->getParam('id_test', 0);
		$centralRepoContext = false;

		$questionType = $request->getParam('question_type', false);
		if (!LearningTestquest::isValidType($questionType)) {
			throw new CException('Invalid question type: '.$questionType);
		}

		$opt = Yii::app()->request->getParam('opt', false);

		$object = null;

		if($opt && $opt == 'centralrepo'){
			$versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
				'id_resource' => $idTest,
				'object_type' => LearningOrganization::OBJECT_TYPE_TEST
			));

			if($versionModel){
				$object = LearningRepositoryObject::model()->findByPk($versionModel->id_object);
				$centralRepoContext = true;
			}
		} else{
			$object = LearningOrganization::model()->findByAttributes(array(
				'idResource' => $idTest,
				'objectType' => LearningOrganization::OBJECT_TYPE_TEST,
				'idCourse'   => $this->getIdCourse()
			));
		}

		$backUrl = Docebo::createAdminUrl('questionBank/index');
		if ($idTest){
			$courseId = $this->getIdCourse();
			if($courseId){
				$backUrl = $this->createUrl('default/edit', array('course_id' => $this->getIdCourse(), 'id_object' => $object->getPrimaryKey(), 'opt' => $centralRepoContext ? 'centralrepo' : ''));
			}elseif($opt && $opt == 'centralrepo' && $object){
				 $backUrl = Docebo::createAbsoluteLmsUrl('test/default/edit', array(
						'id_object' => $object->id_object,
						'opt' => 'centralrepo'
				));
			}
		}


		if ($request->getParam('undo', false) !== false) {
			$this->redirect($backUrl);
		}

		if ($request->getParam('save', false) !== false) {

			$transaction = Yii::app()->db->beginTransaction();

			try {

				$questionText = Yii::app()->htmlpurifier->purify($request->getParam('question', ""), 'allowFrameAndTarget'); //TO DO: can't be empty

				$question = new LearningTestquest();
				$question->idTest = $idTest;
				$question->type_quest = $questionType;
				$question->title_quest = $questionText;
				$question->idCategory = (int)$request->getParam('id_category', 0);
				$question->difficult = (int)$request->getParam('difficulty', LearningTestquest::DIFFICULTY_MEDIUM);
				$question->time_assigned = (int)$request->getParam('time_assigned', 0);
				$question->sequence = LearningTestquest::getTestNextSequence($idTest);
				$question->page = LearningTestquest::getTestPageNumber($idTest);
				$question->shuffle = ((int)$request->getParam('shuffle', 0) > 0 ? 1 : 0);
				$question->settings = $request->getParam('Settings', array());
				if ($idTest) //if created in a test - else, it's from bank management
					$question->is_bank = ($request->getParam('is_bank') == 'on' ? 1 : 0);
				else
					$question->is_bank = 1;

				$this->onBeforeQuestionCreate(new CEvent($this, array('question' => $question)));
				$rs = $question->save();
				if (!$rs) {
					throw new CException('Error while saving question');
				}
				$this->onAfterQuestionCreate(new CEvent($this, array('question' => $question)));

				// Automatically make the LO visible (because we added a question to it)
				if($object && $object instanceof LearningOrganization && !$object->visible){
					$object->visible = 1;
					$object->saveNode();
				}

				if($questionType !== LearningTestquest::QUESTION_TYPE_BREAK_PAGE && $idTest > 0)
				{
					$test = LearningTest::model()->findByPk($idTest);

					if($test->display_type == LearningTest::DISPLAY_TYPE_GROUPED && ($test->order_type == LearningTest::ORDER_TYPE_SEQUENCE || $test->order_type == LearningTest::ORDER_TYPE_RANDOM))
					{
						$question_rel = LearningTestQuestRel::model()->findByAttributes(array('id_test' => $idTest, 'id_question' => $question->idQuest));

						$page_sequence = LearningTestQuestRel::model()->countByAttributes(array('id_test' => $idTest, 'page' => $question->page)) + 1;

						$query = "
							INSERT INTO ".LearningTesttrackQuest::model()->tableName()."
							SELECT t.idTrack, ".$question_rel->id_question.", ".$question_rel->page.", ".$page_sequence.", NULL
							FROM ".LearningTesttrack::model()->tableName()." AS t
							JOIN ".LearningTesttrackPage::model()->tableName()." AS tp ON tp.idTrack = t.idTrack
							WHERE t.idTest = ".(int)$idTest."
							AND tp.page = ".(int)$question_rel->page."
							AND t.score_status IN ('".LearningTesttrack::STATUS_DOING."', '".LearningTesttrack::STATUS_NOT_COMPLETE."')";

						if(Yii::app()->db->createCommand($query)->execute() === false)
							throw new CException('Error while executing operation');
					}
				}

				$transaction->commit();
				Yii::app()->user->setFlash('success', Yii::t('standard', '_OPERATION_SUCCESSFUL'));

			} catch (CException $e) {

				$transaction->rollback();
				Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));

				Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			}

			$this->redirect($backUrl);
		}

		$this->render('edit', array(
			'isEditing' => false,
			'idTest' => (int)$idTest,
			'test' => LearningTest::model()->findByPk($idTest),
			'backUrl' => $backUrl,
			'questionType' => $questionType,
			'questionText' => "",
			'questionCategory' => 0,
			'difficulty' => LearningTestquest::DIFFICULTY_MEDIUM,
			'useShuffle' => false,
			'answeringMaxTime' => 0,
			'centralRepoContext' => $centralRepoContext
		));
	}




	public function actionUpdate() {

		$request = Yii::app()->request;

		$idTest = (int)$request->getParam('id_test', 0);

		$idQuestion = (int)$request->getParam('id_question', 0);
		$centralRepoContext = false;

		$question = LearningTestquest::model()->findByPk($idQuestion);
		if (!$question) {
			throw new CException('Invalid question');
		}

		$opt = Yii::app()->request->getParam('opt', false);
		$object = false;

		if($opt && $opt == 'centralrepo'){
			$versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
				'id_resource' => $idTest,
				'object_type' => LearningOrganization::OBJECT_TYPE_TEST
			));

			if($versionModel){
				$object = LearningRepositoryObject::model()->findByPk($versionModel->id_object);
				$centralRepoContext = true;
			}
		} else{
			$object = LearningOrganization::model()->findByAttributes(array(
				'idResource' => $idTest,
				'objectType' => LearningOrganization::OBJECT_TYPE_TEST,
				'idCourse'   => $this->getIdCourse()
			));
		}

		if ($idTest)
			$backUrl = $this->createUrl('default/edit', array('course_id' => $this->getIdCourse(), 'id_object' => $object->getPrimaryKey(), 'opt' => $centralRepoContext ? 'centralrepo' : ''));
		else
			$backUrl = Docebo::createAdminUrl('questionBank/index');

		if ($request->getParam('undo', false) !== false) {
			$this->redirect($backUrl);
		}

		if ($request->getParam('save', false) !== false) {

			$transaction = Yii::app()->db->beginTransaction();

			try {

				$questionText = Yii::app()->htmlpurifier->purify($request->getParam('question', ""), 'allowFrameAndTarget'); //TO DO: can't be empty

				//$question->idTest = $idTest;
				//$question->type_quest = $questionType;
				$question->title_quest = $questionText;
				$question->idCategory = (int)$request->getParam('id_category', 0);
				$question->difficult = (int)$request->getParam('difficulty', LearningTestquest::DIFFICULTY_MEDIUM);
				$question->time_assigned = (int)$request->getParam('time_assigned', 0);
				//$question->sequence = $questionSequence; //LearningTestquest::getMaxSequenceNumber($idTest)+1;
				//$question->page = 1;
				$test = LearningTest::model()->findByPk($idTest);
				if(!$test || $test->shuffle_answer != LearningTest::SHUFFLE_ANSWER_RANDOM)
					$question->shuffle = ((int)$request->getParam('shuffle', 0) > 0 ? 1 : 0);
				$question->settings = $request->getParam('Settings', array());

				$this->onBeforeQuestionUpdate(new CEvent($this, array('question' => $question)));

				$rs = $question->save();
				if (!$rs) {
					throw new CException('Error while saving question');
				}

				$this->onAfterQuestionUpdate(new CEvent($this, array('question' => $question)));

				$transaction->commit();
				Yii::app()->user->setFlash('success', Yii::t('standard', '_OPERATION_SUCCESSFUL'));

			} catch (CException $e) {

				$transaction->rollback();
				Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
			}

			$this->redirect($backUrl);

		} else {

			$this->render('edit', array(
				'isEditing' => true,
				'is_bank' => $question->is_bank,
				'idTest' => (int)$idTest,
				'test' => LearningTest::model()->findByPk($idTest),
				'backUrl' => $backUrl,
				'idQuestion' => $question->idQuest,
				'questionType' => $question->type_quest,
				'questionText' => $question->title_quest,
				'questionCategory' => $question->idCategory,
				'difficulty' => $question->difficult,
				'useShuffle' => ($question->shuffle > 0),
				'answeringMaxTime' => $question->time_assigned,
				'centralRepoContext' => $centralRepoContext,
				'settings' => $question->settings,
			));

		}
	}



	public function actionAxDelete() {

		$request = Yii::app()->request;

		$idTest = (int)$request->getParam('id_test', 0);
		$test = LearningTest::model()->findByPk($idTest);
		if (!$test) {
			throw new CException('Invalid test');
		}

		$idQuestion = (int)$request->getParam('id_question', 0);

		$question = LearningTestQuestRel::getByTestQuest($idTest, $idQuestion);
		if (!$question) {
			throw new CException('Invalid question');
		}
		$transaction = Yii::app()->db->beginTransaction();
		$response = new AjaxResult();

		try {

			$this->onBeforeQuestionDelete(new CEvent($this, array('question' => $question)));
			if (!$question->delete()) { throw new CException('Error while deleting question'); }
			LearningTestquest::fixTestSequence($idTest);
			LearningTestquest::fixTestPages($idTest);
			$this->onAfterQuestionDelete(new CEvent($this, array('question' => $question)));

			//Updating question and pages number after delete a question
			$list = $test->getQuestions(true);
			$pages = array();
			if (!empty($list)) {
				foreach ($list as $question) {
					if (!in_array($question->page, $pages)) {
						$pages[] = $question->page;
					}
				}
			}

			$updatedTitleCaption = '('.Yii::t('test', '_TEST_CAPTION', array(
					'%tot_element%' => count($list),
					'%tot_page%' => count($pages)
				)).')';

			$test = LearningTest::model()->findByPk($idTest);

			$query = "
				DELETE tq
				FROM ".LearningTesttrackQuest::model()->tableName()." AS tq
				JOIN ".LearningTesttrack::model()->tableName()." AS t ON t.idTrack = tq.idTrack
				WHERE tq.idQuest = ".(int)$idQuestion."
				AND t.idTest = ".(int)$idTest."
				AND t.score_status IN ('".LearningTesttrack::STATUS_DOING."', '".LearningTesttrack::STATUS_NOT_COMPLETE."')";

			if(Yii::app()->db->createCommand($query)->execute() === false)
				throw new CException('Error while executing operation');

			$query = "
				DELETE ta
				FROM ".LearningTesttrackAnswer::model()->tableName()." AS ta
				JOIN ".LearningTesttrack::model()->tableName()." AS t ON t.idTrack = ta.idTrack
				WHERE ta.idQuest = ".(int)$idQuestion."
				AND t.idTest = ".(int)$idTest."
				AND t.score_status IN ('".LearningTesttrack::STATUS_DOING."', '".LearningTesttrack::STATUS_NOT_COMPLETE."')";

			if(Yii::app()->db->createCommand($query)->execute() === false)
				throw new CException('Error while executing operation');

			if($test->display_type == LearningTest::DISPLAY_TYPE_GROUPED && ($test->order_type == LearningTest::ORDER_TYPE_SEQUENCE || $test->order_type == LearningTest::ORDER_TYPE_RANDOM))
			{
				$query = "
					UPDATE ".LearningTesttrackQuest::model()->tableName()." AS tq
					JOIN ".LearningTestQuestRel::model()->tableName()." AS r ON r.id_question = tq.idQuest AND r.id_test = ".(int)$idTest."
					JOIN ".LearningTesttrack::model()->tableName()." as t on t.idTrack = tq.idTrack
					SET tq.page = r.page,
					tq.quest_sequence = r.sequence
					WHERE t.idTest = ".(int)$idTest."
					AND t.score_status IN ('".LearningTesttrack::STATUS_DOING."', '".LearningTesttrack::STATUS_NOT_COMPLETE."')";

				if(Yii::app()->db->createCommand($query)->execute() === false)
					throw new CException('Error while executing operation');
			}
			elseif($test->display_type == LearningTest::DISPLAY_TYPE_SINGLE && $test->order_type == LearningTest::ORDER_TYPE_SEQUENCE)
			{
				//Check if there's some running test to update
				$query = "
					UPDATE ".LearningTesttrackQuest::model()->tableName()." AS tq
					JOIN ".LearningTestQuestRel::model()->tableName()." AS r ON r.id_question = tq.idQuest AND r.id_test = ".(int)$idTest."
					JOIN ".LearningTesttrack::model()->tableName()." as t on t.idTrack = tq.idTrack
					SET tq.page = r.sequence,
					tq.quest_sequence = r.sequence
					WHERE t.idTest = ".(int)$idTest."
					AND t.score_status IN ('".LearningTesttrack::STATUS_DOING."', '".LearningTesttrack::STATUS_NOT_COMPLETE."')";

				if(Yii::app()->db->createCommand($query)->execute() === false)
					throw new CException('Error while executing operation');
			}

			$transaction->commit();
			$response->setStatus(true)->setData(array('id_question' => $idQuestion));

		} catch (CException $e) {

			$transaction->rollback();
			$response->setStatus(false)->setMessage($e->getMessage());
		}

		$response->toJSON();
	}


}