<style>
	.tooltipevent p.section{
		display: block;
		margin: 4px 0 0 0;
	}
</style>
<img class="close" src="<?= Yii::app()->theme->baseUrl; ?>/images/icon-close-black-medium.png"/>
<?php $show_course_details_dedicated_page = (Settings::getStandard('show_course_details_dedicated_page') === 'on' ? true : false) ?>
<? foreach($items as $item): ?>
	<p class="section"><?
		$ex = explode('_', $item['id']);
		$id_course = $ex[0];
		$id_session = $ex[1];
		$day = $ex[2];
		$id_videoconference = $ex[3];

		$className = '';
		if ($show_course_details_dedicated_page) {
			$courseDetailsUrl = Docebo::createLmsUrl('course/details', array('id' => $id_course));
		} else {
			$className = 'open-dialog';
			$courseDetailsUrl 	= Docebo::createLmsUrl('course/axDetails', array('id' => $id_course));
		}

		$enrollLinkTag 	= "<a href='$courseDetailsUrl' class='$className'>{$item['courseName']}</a>";

		switch($item['type']){
			case 'elearning':
				echo Yii::t('course', '_COURSE_TYPE_ELEARNING');
				break;
			case 'classroom':
				echo Yii::t('standard', '_CLASSROOM');
				break;
			case 'webinar':
				echo Yii::t('webinar', 'Webinar');
				break;
			case 'conference':
				echo Yii::t('standard', '_VIDEOCONFERENCE');
				break;
			default:
				echo 'Unknown type';
		}?></p>
	<p class="section"><u>
			<b>
			<?=$enrollLinkTag?>
			</b>
		</u>
		<p class="section">
			<i><?=$item['name']?></i><br />
			<i><?=strip_tags($item['sessionDescription'])?></i>
		</p>
		<p class="section">
			<i><?=strip_tags($item['dateName'])?></i><br />
			<?php
			if(!empty($item['dateDescription'])){ ?>
			<i><?=strip_tags($item['dateDescription'])?></i><br />
			<?php } ?>
			<i><?=	Yii::app()->localtime->toLocalDateTime($item['start'], LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, null, null, true, true)?></i><br />
			<i><?= Yii::app()->localtime->toLocalDateTime($item['end'], LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, null, null, true, true)?></i><br />
			<?php
			if(!empty($item['location'])){ ?>
			<span><strong><?= Yii::t('classroom', 'Location'); ?></strong></span><br />
			<i><?=$item['location']?></i>
			<?php } ?>
		</p>
	</p>
<? endforeach; ?>
<div class="triangle">
	<img src="<?= Yii::app()->getModule('mycalendar')->getAssetsUrl(); ?>/images/triangle.png" />
</div>


<script type="text/javascript">
	$(document).ready(function() {
		<?php if(!$show_course_details_dedicated_page) { ?>
		$(document).controls();

		$(document).off('dialog2.opened', 'div.modal.course-details-modal');

		$(document).on('dialog2.opened', 'div.modal.course-details-modal', function () {
			$('html, body').animate({
				scrollTop: $("div.modal.course-details-modal").offset().top
			}, 1000);
		});
		<?php } ?>

		$(document).mouseup(function (e)
		{
			var container = $("div.tooltipevent");

			if (!container.is(e.target) // if the target of the click isn't the container...
				&& container.has(e.target).length === 0) // ... nor a descendant of the container
			{
				container.remove();
			}
		});
	});
</script>