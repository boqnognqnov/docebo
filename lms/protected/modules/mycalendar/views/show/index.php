<?php
	if(!(Yii::app()->user->isGuest)) {
		$this->breadcrumbs=array(
				Yii::t('calendar', 'My Calendar'),
		);
		$hash = sha1(Yii::app()->user->id.ShowController::INTERNAL_SECRET_KEY);
	} else {
		Yii::app()->getModule('mycalendar');
		$hash = sha1(Yii::app()->user->id.ShowController::INTERNAL_SECRET_KEY);//"DoceboInternalSecretKey");//
	}
?>
<div class="row-fluid">
	<div class="span6">
		<h3 class="title-bold"><?= (empty($custom_title)) ?  Yii::t('calendar', 'My Calendar') : $custom_title; ?></h3>

		<?php
		// Event raised for YnY app to hook on to
		Yii::app()->event->raise('BeforeRenderMyCalendarForm', new DEvent($this, array()));
		?>
	</div>
	<div class="span6 text-right">
		<a class="my-calendar-export" href="#" rel="popover" data-html="true" data-placement="left" data-content='<div class="mycalendar-popover">
		<input id="copy-example" type="text" class="form-control" readonly="readonly" value="<?= Docebo::createAbsoluteUrl('/mycalendar/show/download', array(
				'id_user' => Yii::app()->user->id,
				'hash' => $hash
		))?>"/>
		<input data-toggle="tooltip" data-placement="right" title="<?=Yii::t('course','Copied to Clipboard!');?>" id="copy_button" data-clipboard-target="#copy-example" style="background-color: black !important; margin-left: -4px; padding-top: 7px;" class="btn btn-docebo black big" type="button" value="<?php echo Yii::t('coursepath', 'Copy'); ?>"/>
		<br><p style="display:inline-block" class="muted">Use this URL into your calendar software/app (e.g. Outlook) to keep synchronized your LMS events</p>
	</div>'>
			<?= (!(Yii::app()->user->isGuest)) ? Yii::t('calendar', 'Export calendar') : ''; ?>
		</a>
	</div>
</div>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'my-calendar-form',
	'htmlOptions' => array(
		'class' => 'ajax-grid-form'
	),
)); ?>
<div class="main-section">
	<div class="filters-wrapper">
		<table class="filters">
			<tbody>
			<tr>
				<td class="table-padding">
					<table class="table-border">
						<tr>
							<td class="group">
								<table>
									<tr>
									<?php if (!(Yii::app()->user->isGuest)) { ?>
										<td><?= CHtml::checkBox('elearning_filter', true); ?></td>
										<td class="elearning" style="background-color: #0566af;"><label for="elearning_filter" style="color: #fff;"><?= Yii::t('course', '_COURSE_TYPE_ELEARNING'); ?></label></td>
										<?php if(PluginManager::isPluginActive('ClassroomApp')) : ?>
											<td><?= CHtml::checkBox('classroom_filter',true); ?></td>
											<td class="classroom" style="background-color: #5ebf62;"><label for="classroom_filter" style="color: #fff;"><?= Yii::t('standard', 'Classroom Course'); ?></label></td>
										<?php endif; ?>
										<td><?= CHtml::checkBox('videoconference_filter', true); ?></td>
										<td class="videoconference" style="background-color: #8d44ad;"><label for="videoconference_filter" style="color: #fff;"><?= Yii::t('webinar', 'Webinar'); ?></label></td>
									<?php } else { ?>
										<td><?= CHtml::checkBox('videoconference_filter', true); ?></td>
										<td class="videoconference" style="background-color: #8d44ad;"><label for="videoconference_filter" style="color: #fff;"><?= Yii::t('webinar', 'Webinar'); ?></label></td>
										<?php if(PluginManager::isPluginActive('ClassroomApp')) : ?>
											<td><?= CHtml::checkBox('classroom_filter',true); ?></td>
											<td class="classroom" style="background-color: #5ebf62;"><label for="classroom_filter" style="color: #fff;"><?= Yii::t('standard', 'Classroom Course'); ?></label></td>
										<?php endif; ?>
									<?php } ?>
									</tr>
								</table>
							</td>
							<td class="group advanced-search-box clearfix">
								<table>
									<tr>
										<td>
											<div class="input-wrapper">
												<input class="typeahead"
													   id="text_filter" autocomplete="off" type="text"
													   name="text_filter"
													   placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"/>
												<!--<label for="advanced-search-course">Search course</label>   -->
												<span class="search-icon"></span>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			</tbody>
		</table>
		<div id="view_switch">
			<span class="view_switch"><?php echo Yii::t('standard', 'Switch view'); ?></span><?= CHtml::button(Yii::t('statistic', '_FOR_week'), array('class' => 'btn mycalendar-btn mycalendar-btn-std', 'id' => 'toweek')).CHtml::button(Yii::t('statistic', '_FOR_month'), array('class' => 'btn close-btn mycalendar-btn-std', 'id' => 'tomonth')); ?>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>

<div id='calendar'></div>

<script type="text/javascript">
var rendered = 0;

	$(document).ready(function()
	{
		//Istance of the calendar
		$('#calendar').fullCalendar({
			//To display Saturday and Sunday
			weekends: true,
			//Time format, hide it in month view
			timeFormat: {
				'month': '',
				'agendaDay': 'HH:mm{ - HH:mm}',
				'agendaWeek': 'HH:mm{ - HH:mm}'
			},
			//Translation of months and days
			monthNames: [
				'<?= addslashes(Yii::t('calendar', '_JANUARY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_FEBRUARY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_MARCH')); ?>',
				'<?= addslashes(Yii::t('calendar', '_APRIL')); ?>',
				'<?= addslashes(Yii::t('standard', '_MONTH_05')); ?>',
				'<?= addslashes(Yii::t('calendar', '_JUNE')); ?>',
				'<?= addslashes(Yii::t('calendar', '_JULY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_AUGUST')); ?>',
				'<?= addslashes(Yii::t('calendar', '_SEPTEMBER')); ?>',
				'<?= addslashes(Yii::t('calendar', '_OCTOBER')); ?>',
				'<?= addslashes(Yii::t('calendar', '_NOVEMBER')); ?>',
				'<?= addslashes(Yii::t('calendar', '_DECEMBER')); ?>'
			],
			monthNamesShort: [
				'<?= addslashes(Yii::t('calendar', '_JAN')); ?>',
				'<?= addslashes(Yii::t('calendar', '_FEB')); ?>',
				'<?= addslashes(Yii::t('calendar', '_MAR')); ?>',
				'<?= addslashes(Yii::t('calendar', '_APR')); ?>',
				'<?= addslashes(Yii::t('calendar', '_MAY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_JUN')); ?>',
				'<?= addslashes(Yii::t('calendar', '_JUL')); ?>',
				'<?= addslashes(Yii::t('calendar', '_AUG')); ?>',
				'<?= addslashes(Yii::t('calendar', '_SEP')); ?>',
				'<?= addslashes(Yii::t('calendar', '_OCT')); ?>',
				'<?= addslashes(Yii::t('calendar', '_NOV')); ?>',
				'<?=addslashes( Yii::t('calendar', '_DEC')); ?>'
			],
			dayNames: [
				'<?= addslashes(Yii::t('calendar', '_SUNDAY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_MONDAY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_TUESDAY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_WEDNESDAY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_THURSDAY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_FRIDAY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_SATURDAY')); ?>'
			],
			dayNamesShort: [
				'<?= addslashes(Yii::t('calendar', '_SUN')); ?>',
				'<?= addslashes(Yii::t('calendar', '_MON')); ?>',
				'<?= addslashes(Yii::t('calendar', '_TUE')); ?>',
				'<?= addslashes(Yii::t('calendar', '_WED')); ?>',
				'<?= addslashes(Yii::t('calendar', '_THU')); ?>',
				'<?= addslashes(Yii::t('calendar', '_FRI')); ?>',
				'<?= addslashes(Yii::t('calendar', '_SAT')); ?>'
			],
			//Text to be displayed on calenda buttons
			buttonText: {
				month: '<?= addslashes(Yii::t('statistic', '_FOR_month')); ?>',
				week: '<?= addslashes(Yii::t('statistic', '_FOR_week')); ?>',
				day: '<?= addslashes(Yii::t('standard', '_DAY')); ?>'
			},
			//Calendar header element disposition
			header: {
				left: '',
				center: 'prevYear prev title next nextYear',
				right: ''//'agendaWeek,month'
			},
			//To set Monday as the first day of the week
			firstDay: 1,
			//RTL setting
			isRTL: <?= (Lang::isRTL(Yii::app()->getLanguage()) ? 'true' : 'false') ?>,
			//For display the full name of the day into the mounth view
			columnFormat: {
				month: 'dddd',
				week: 'dddd dd/MM',
				day: 'dddd dd/MM'
			},
			//Title format for the views
			titleFormat: {
				month: 'MMMM yyyy',
				week: "MMM dd[ yyyy]{ '-'[ MMM] dd, yyyy}",
				day: 'dddd, dd MMMM, yyyy'
			},
			//Url for data fetch
			eventSources: [
				{
					url: './index.php?r=mycalendar/show/fetchElearning',
					color: '#0566af',
					textColor: '#ffffff'
				}
				<?php if(PluginManager::isPluginActive('ClassroomApp')) : ?>
					,{
						url: './index.php?r=mycalendar/show/fetchClassroom',
						color: '#5ebf62',
						textColor: '#ffffff'
					}
				<?php endif; ?>
					,{
						url: './index.php?r=mycalendar/show/fetchVideoconference',
						color: '#8d44ad',
						textColor: '#ffffff'
					}
			],
			//Remove the top corner text into week view
			allDayText: '',
			//Set the time format for the left cols into week and day views
			axisFormat: 'HH:mm',
			//Set the starting hour for the calendar week and day views
			firstHour: 0,
			//Set the action to do un event click, in our case the show of the tooltip
			eventClick: function(event, jsEvent, view)
			{
				//Manually create the tooltip
				if($('.tooltipevent'))
					$('.tooltipevent').remove();
				var tooltip = '<div class="tooltipevent"></div>';
				$("body").append(tooltip);

				//Load the tooltip body
				$.ajax({
					type: "POST",
					url: "./index.php?r=mycalendar/show/Tooltip",
					async: false,
					data: {id:event.id},
					success:function(result)
					{
						$('.tooltipevent').html(result);
					}
				});

				//Display the tooltip
				$('.tooltipevent').fadeIn('500');
				$('.tooltipevent').fadeTo('10', 1.9);

				//Set the tooltip position
				var position = $(this).offset();
				$('.tooltipevent').css('top', position.top - $('.tooltipevent').height() - 40);
				$('.tooltipevent').css('left', position.left + ($(this).width() - $('.tooltipevent').width()) / 2 - 6);

				$(this).mouseover(function(e) {
					$(this).css('z-index', 10000);
				});
			},
			loading: function(is_loading, view)
			{
				if(is_loading)
					closePopup();
			},
			eventAfterRender: function(view)
			{
				//Needed for check if at last one event is rendered
				rendered = 1;
			},
			loading: function(loading, view)
			{
				if(loading)
					rendered = 0;
				else if(rendered == 0)
				{
					$.ajax({
						type: "POST",
						url: "./index.php?r=mycalendar/show/getNextEvent",
						async: true,
						data: {},
						success:function(result)
						{
							var json = jQuery.parseJSON(result);
							if(json.success)
							{
								$('#calendar').fullCalendar('gotoDate', json.year, json.month, json.day);
							}
						}
					});
				}
			}
		});

		$('#elearning_filter').styler();
		$('#elearning_filter').change(function(e){
			var is_checked = $('#elearning_filter').is(':checked')?1:0;

			//Set the filter and refresh the caldenda data
			$.ajax({
				type: "POST",
				url: "./index.php?r=mycalendar/show/setFilters",
				async: true,
				data: {elearning_filter:is_checked},
				success:function(result)
				{
					$('#calendar').fullCalendar('refetchEvents');
				}
			});
		});
		if($('#classroom_filter'))
		{
			$('#classroom_filter').styler();
			$('#classroom_filter').change(function(e){
				var is_checked = $('#classroom_filter').is(':checked')?1:0;

				//Set the filter and refresh the caldenda data
				$.ajax({
					type: "POST",
					url: "./index.php?r=mycalendar/show/setFilters",
					async: true,
					data: {classroom_filter:is_checked},
					success:function(result)
					{
						$('#calendar').fullCalendar('refetchEvents');
					}
				});
			});
		}
		if($('#videoconference_filter'))
		{
			$('#videoconference_filter').styler();
			$('#videoconference_filter').change(function(e){
				var is_checked = $('#videoconference_filter').is(':checked')?1:0;

				//Set the filter and refresh the caldenda data
				$.ajax({
					type: "POST",
					url: "./index.php?r=mycalendar/show/setFilters",
					async: true,
					data: {videoconference_filter:is_checked},
					success:function(result)
					{
						$('#calendar').fullCalendar('refetchEvents');
					}
				});
			});
		}

		$("#toweek").click(function(){
			if($('#toweek').hasClass('mycalendar-btn'))
			{
				$('#toweek').removeClass('mycalendar-btn');
				$('#toweek').addClass('close-btn');

				$('#tomonth').removeClass('close-btn');
				$('#tomonth').addClass('mycalendar-btn');

				$('#calendar').fullCalendar('changeView', 'agendaWeek');
			}
		});

		$("#tomonth").click(function(){
			if($('#tomonth').hasClass('mycalendar-btn'))
			{
				$('#tomonth').removeClass('mycalendar-btn');
				$('#tomonth').addClass('close-btn');

				$('#toweek').removeClass('close-btn');
				$('#toweek').addClass('mycalendar-btn');

				$('#calendar').fullCalendar('changeView', 'month');
			}
		});

		$('#my-calendar-form').submit(function(e){
			//Set the filter and refresh the caldenda data
			$.ajax({
				type: "POST",
				url: "./index.php?r=mycalendar/show/setFilters",
				async: true,
				data: {text_filter:$('#text_filter').val()},
				success:function(result)
				{
					$('#calendar').fullCalendar('refetchEvents');
				}
			});
			e.preventDefault();
		});

		var clipboard = new Clipboard('#copy_button.btn');
		var copy_button = $('#copy_button');
		var options = {
			trigger : 'click',
			title : 'test'
		};

		clipboard.on('success', function(e) {
			$('[data-toggle="tooltip"]').tooltip(options);
			$('[data-toggle="tooltip"]').tooltip('show');
			$(document).off('mouseleave','#copy_button');
			$('[data-toggle="tooltip"]').on('mouseleave', function() {
				$('[data-toggle="tooltip"]').tooltip('hide');
			});
			e.clearSelection();
		});
	});
	function closePopup()
	{
		//Remove the tooltip on closing
		if($('.tooltipevent'))
			$('.tooltipevent').remove();
	}
</script>

<style>
	.fc-event-title {
		color: #fff;
	}
</style>