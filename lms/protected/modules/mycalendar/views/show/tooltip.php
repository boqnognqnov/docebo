<style>
	.tooltipevent div.section{
		display: block;
		margin: 4px 0 0 0;
	}
</style>
<img class="close" src="<?= Yii::app()->theme->baseUrl; ?>/images/icon-close-black-medium.png" onClick="closePopup()" />
<span class="date"><?= $day_t; ?>, <?= $day_n; ?> <?= $month_t; ?> <?= $year; ?></span><br />
<?php if(isset($time_begin)): ?>
	<span class="time"><?= $time_begin; ?> - <?= $time_end; ?></span><br />
<!--	<span class="time">--><?//= $timezone; ?><!--</span><br />-->
<?php endif; ?>
<br />
<?php if(isset($course_name)): ?>
	<?php if($is_enrolled){ ?>

		<?php if(!$is_elearning): ?>
			<span><strong><?= $course_name; ?></strong></span><br />
		<?php else: ?>
			<span><strong><a class="link" href="<?=Docebo::createLmsUrl('player', array('course_id'=>$id_course))?>"><?= $session_name; ?></a></strong></span>
		<?php endif;?>

	<?php } else { ?>
	<?php   if($show_course_details_dedicated_page == 'off') { ?>
	<a class="link open-dialog" data-dialog-class="course-details-modal" rel="dialog-course-detail-<?= $id_course; ?>" href="./index.php?r=course/axDetails&id=<?= $id_course; ?>"><strong><?= $course_name; ?></strong></a><br />
	<?php   } else { ?>
			<a class="link" rel="dialog-course-detail-<?= $id_course; ?>" href="./index.php?r=course/details&id=<?= $id_course; ?>"><strong><?= $course_name; ?></strong></a><br />
	<?php   } ?>
	<?php } ?>
<?php endif; ?>
<?php if($can_enter): ?>
	<?php if (isset($course_type) && $course_type == LearningCourse::TYPE_WEBINAR && $webinar_session_enrolled) :?>
		<?php $url = Docebo::createLmsUrl('player/training/webinarSession', array('course_id'=>$id_course,'session_id'=>$session_id));?>
	<?php elseif($course_type == LearningCourse::TYPE_CLASSROOM && $classroom_session_enrolled): ?>
		<?php $url = Docebo::createLmsUrl('player/training/session', array('course_id' => $id_course, 'session_id' => $id_session)); ?>
	<?php else: ?>
		<?php $url = Docebo::createLmsUrl('player', array('course_id'=>$id_course));?>
	<?php endif; ?>
	<div class="section">
		<?php if(!$is_elearning):?>
			<a class="link" href="<?=$url?>"><?= $session_name; ?></a><br />
		<?php endif;?>
		<div class="session-description"><?= $session_description; ?></div>
	</div>
<?php elseif(!$is_elearning) : ?>
	<br/>
	<div class="section">
		<span><?= $session_name; ?></span><br/>
		<span><?= $session_description; ?></span><br/>
	</div>
<?php endif; ?>
<?php if(!$is_elearning) :; ?>
	<div class="section">
		<span><?= $date_name; ?></span><br/>
		<?php
		if(!empty($date_description)) { ?>
			<span><?= $date_description; ?></span><br/>
		<?php } ?>
	</div>
<?php endif; ?>
<br />
<?php if(isset($location)): ?>
	<span><strong><?= Yii::t('classroom', 'Location'); ?></strong></span><br />
	<span class="location"><?= $location; ?></span>
<?php endif; ?>
<img class="triangle" src="<?= $this->module->getAssetsUrl(); ?>/images/triangle.png" />

<script type="text/javascript">
	$(document).ready(function() {
		<?php if($show_course_details_dedicated_page == 'off') { ?>
		$(document).controls();

		$(document).off('dialog2.opened', 'div.modal.course-details-modal');

		$(document).on('dialog2.opened', 'div.modal.course-details-modal', function () {
			$('html, body').animate({
				scrollTop: $("div.modal.course-details-modal").offset().top
			}, 1000);
		});
		<?php } ?>

		$(document).mouseup(function (e)
		{
			var container = $("div.tooltipevent");

			if (!container.is(e.target) // if the target of the click isn't the container...
					&& container.has(e.target).length === 0) // ... nor a descendant of the container
			{
				container.remove();
			}
		});
	});
</script>
