<?php

class MycalendarModule extends CWebModule
{
	private $_assetsUrl;

	public function init()
	{
		parent::init();

		$this->setImport(array(
			'mycalendar.models.*',
			'mycalendar.components.*',
			'mycalendar.controllers.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}


	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('mycalendar.assets'));
		}
		return $this->_assetsUrl;
	}
}
