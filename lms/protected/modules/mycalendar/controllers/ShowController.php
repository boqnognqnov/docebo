<?php

class ShowController extends Controller
{
	protected $day_translation;//Translation array for days
	protected $month_translation;//Translation array for months
	public $colors;

	CONST INTERNAL_SECRET_KEY = "DoceboInternalSecretKey";

	/**
	* (non-PHPdoc)
	* @see CController::init()
	*/
	public function init()
	{
		parent::init();

		$this->registerResources();

		$this->colors = $colors = Yii::app()->theme->getChartColors();

		$this->day_translation = array(
			Yii::t('calendar', '_SUNDAY'),
			Yii::t('calendar', '_MONDAY'),
			Yii::t('calendar', '_TUESDAY'),
			Yii::t('calendar', '_WEDNESDAY'),
			Yii::t('calendar', '_THURSDAY'),
			Yii::t('calendar', '_FRIDAY'),
			Yii::t('calendar', '_SATURDAY')
		);

		$this->month_translation = array(
			'01' => Yii::t('calendar', '_JANUARY'),
			'02' => Yii::t('calendar', '_FEBRUARY'),
			'03' => Yii::t('calendar', '_MARCH'),
			'04' => Yii::t('calendar', '_APRIL'),
			'05' => Yii::t('standard', '_MONTH_05'),
			'06' => Yii::t('calendar', '_JUNE'),
			'07' => Yii::t('calendar', '_JULY'),
			'08' => Yii::t('calendar', '_AUGUST'),
			'09' => Yii::t('calendar', '_SEPTEMBER'),
			'10' => Yii::t('calendar', '_OCTOBER'),
			'11' => Yii::t('calendar', '_NOVEMBER'),
			'12' => Yii::t('calendar', '_DECEMBER')
		);
	}

	/**
	  * (non-PHPdoc)
	  * @see CController::filters()
	  */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	/**
	  * (non-PHPdoc)
	  * @see CController::accessRules()
	  */
	public function accessRules()
	{
		return array(

			array('allow',
				'users' => array('@'),
			),
			array('allow', // allow all users to perform 'index' and 'view' actions
					'actions' => array('download'),
					'users' => array('*'),
			),

			array('allow',
					'users'=>array('*'),
					'actions' => array('fetchElearning', 'fetchClassroom', 'fetchVideoconference', 'getNextEvent',
							'fetchCalendarItems', 'Tooltip'
					),
			),

			array('deny',
				'users'=>array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
			),
		);
	}

	/**
	 * Register module specific assets
	 *
	 * (non-PHPdoc)
	 * @see LoBaseController::registerResources()
	 */
	public function registerResources()
	{
		if (!Yii::app()->request->isAjaxRequest)
		{
			//Load some specific assets needed for the calendar

			$cs = Yii::app()->getClientScript();
			$assetsUrl = $this->module->assetsUrl;
			$cs->registerCssFile(Yii::app()->theme->baseUrl . '/js/formstyler/jquery.formstyler.css');
			$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/admin.css');
			// $cs->registerCssFile(Yii::app()->baseUrl.'/../admin/css/admin-temp.css');
			$cs->registerCssFile($assetsUrl.'/css/fullcalendar.css');
			$cs->registerCssFile($assetsUrl.'/css/docebotheme.css');
			$cs->registerScriptFile($assetsUrl.'/js/fullcalendar.js');
			$cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/clipboard.min.js');

		}
	}


	/**
	 * Display the default calendar view
	 */
	public function actionIndex()
	{
		//Here we set the initial value for filters
		Yii::app()->session['elearning_filter'] = 1;
		Yii::app()->session['classroom_filter'] = 1;
		Yii::app()->session['videoconference_filter'] = 1;
		Yii::app()->session['text_filter'] = '';

		$this->render('index');
	}

	public function actionDownload(){
		$request = Yii::app()->request;

		$userId = $request->getParam('id_user', false);
		$hash = $request->getParam('hash', false);

		$internalHash = sha1($userId.self::INTERNAL_SECRET_KEY);

		if($hash !== $internalHash){
			header('User not found', true, 404);
			Yii::app()->end();
		}

		if(!$userId){
			header('User not found', true, 404);
			Yii::app()->end();
		}

		$userModel = CoreUser::model()->findByPk($userId);

		if(($userModel instanceof CoreUser) === false){
			header('User not found', true, 404);
			Yii::app()->end();
		}


		$filter = '';

		$result = array();
		$result = $this->getElearning($filter, $userModel->idst, true);
		$classroom = $this->getClassroom($filter, $userModel->idst, true);
		$result = array_merge($result, $classroom);
		$webinars = WebinarSession::getWebinar($filter, $userModel->idst, true, true);
		$result = array_merge($result, $webinars);

		$output = "BEGIN:VCALENDAR\r\n
VERSION:2.0\r\n
PRODID:Docebo Calendar\r\n
";
		foreach($result as $key => $event){
			$time = time();
			if($event['start'] == '0000-00-00 00:00:00' || $event['end'] == '0000-00-00 00:00:00') continue;

			$output .=
					"BEGIN:VEVENT\r\n"
					."DTSTART:".str_replace(array('-', ':'), array('',''), substr(Yii::app()->localtime->fromLocalDateTime($event['start']), 0, 19)).'Z'."\r\n"
					."DTEND:".str_replace(array('-', ':'), array('',''), substr(Yii::app()->localtime->fromLocalDateTime($event['end']), 0, 19)).'Z'."\r\n"
					."SEQUENCE:0\r\n"
					."UID:".$event['id']."\r\n"
					."DTSTAMP:".date("Ymd\THis\Z")."\r\n"
					."SUMMARY:".$event['title']."\r\n"
					."BEGIN:VALARM\r\n"
					."TRIGGER:-PT15M\r\n"
					."ACTION:DISPLAY\r\n"
					."END:VALARM\r\n"
					."TRANSP:OPAQUE\r\n"
					."END:VEVENT\r\n";
		}

		$output .= 'END:VCALENDAR' ."\r\n";

		header('Content-type: text/calendar');
		header('Content-Disposition: inline; filename=calendar.ics');
		echo $output;

		exit;
	}

	private function getElearning($filter, $userId, $infinity = false){

		$result = array();

		if(!$infinity){
			$start_date = date('Y-m-d', Yii::app()->request->getParam('start', time())).' 00:00:00';
			$end_date = date('Y-m-d', Yii::app()->request->getParam('end', time())).' 23:59:59';
		}

		if(PluginManager::isPluginActive('CurriculaApp')) {
			$command = Yii::app()->db->createCommand()
				->select('c.idCourse, c.name, cpu.date_end_validity')
				->from(LearningCoursepath::model()->tableName().' cp')
				->join(LearningCoursepathCourses::model()->tableName().' cpc', 'cpc.id_path = cp.id_path AND cp.days_valid IS NOT NULL')
				->join(LearningCourse::model()->tableName().' c', 'c.idCourse = cpc.id_item')
				->join(LearningCoursepathUser::model()->tableName().' cpu', 'cpu.id_path = cp.id_path')
				->andWhere('c.course_type = :course_type', array(':course_type' => LearningCourse::TYPE_ELEARNING))
				->andWhere('cpu.idUser = :idUser', array(':idUser' => Yii::app()->user->id))
				->andWhere('cpu.date_end_validity IS NOT NULL AND date_end_validity <> "0000-00-00 00:00:00"');
			if(!$infinity){
				$command->andWhere('cpu.date_end_validity BETWEEN :start_date AND :end_date', array(
						':start_date' => Yii::app()->localtime->toUTC($start_date),
						':end_date' => Yii::app()->localtime->toUTC($end_date)
					)
				);
			}

			if(Yii::app()->session['text_filter'] != '')
			{
				$command->andWhere('c.name LIKE :name');
				$command->params[':name'] = '%'.Yii::app()->session['text_filter'].'%';
			}

			$rows = $command->queryAll();

			foreach($rows as $course) {
				$result[$course['idCourse']] = array(
					'id' => $course['idCourse'].'_0_0_0',
					'title' => (strlen($course['name']) > 23 ? substr($course['name'], 0, 23).'...' : $course['name']),
					'start' => Yii::app()->localtime->toLocalDateTime($course['date_end_validity'], 'short', 'medium', 'Y-m-d H:i:s'),
					'end' => Yii::app()->localtime->toLocalDateTime($course['date_end_validity'], 'short', 'medium', 'Y-m-d H:i:s'),
					'allDay' => true
				);
			}
		}

		//Course with subscription date validity
		$command = Yii::app()->db->createCommand();
		$command->select('c.idCourse, c.name, cu.date_expire_validity');
		$command->from(LearningCourse::model()->tableName().' as c');
		$command->join(LearningCourseuser::model()->tableName().' as cu', 'c.idCourse = cu.idCourse');
		$command->where('cu.idUser = :id_user');
		$command->params[':id_user'] = $userId;
		$command->andWhere('cu.date_expire_validity IS NOT NULL');
		$command->andWhere('cu.date_expire_validity <> "0000-00-00 00:00:00"');
		if(!$infinity){
			$command->andWhere('cu.date_expire_validity BETWEEN :start_date AND :end_date');
			$command->params[':start_date'] = Yii::app()->localtime->toUTC($start_date, 'Y-m-d H:i:s');
			$command->params[':end_date'] = Yii::app()->localtime->toUTC($end_date, 'Y-m-d H:i:s');
		}
		//Apply text filter if needed
		if($filter != '')
		{
			$command->andWhere('c.name LIKE :name');
			$command->params[':name'] = '%'.$filter.'%';
		}
		$rows = $command->queryAll();

		foreach($rows as $course)
		{
			if(!isset($result[$course['idCourse']]))
				$result[$course['idCourse']] = array(
						'id' => $course['idCourse'].'_0_0_0',
						'title' => (strlen($course['name']) > 23 ? substr($course['name'], 0, 23).'...' : $course['name']),
					'start' => Yii::app()->localtime->toLocalDateTime($course['date_expire_validity'], 'short', 'medium', 'Y-m-d H:i:s'),
					'end' => Yii::app()->localtime->toLocalDateTime($course['date_expire_validity'], 'short', 'medium', 'Y-m-d H:i:s'),
						'allDay' => true
				);
		}

		//Course with corswe end date set
		$command = Yii::app()->db->createCommand();
		$command->select('c.idCourse, c.name, c.date_end');
		$command->from(LearningCourse::model()->tableName().' as c');
		$command->join(LearningCourseuser::model()->tableName().' as cu', 'c.idCourse = cu.idCourse');
		$command->where('cu.idUser = :id_user');
		$command->params[':id_user'] = $userId;
		$command->andWhere('cu.date_expire_validity IS NULL');
		if(!$infinity){
			$command->andWhere('c.date_end BETWEEN :start_date AND :end_date');
			$command->params[':start_date'] = substr($start_date, 0, 10);
			$command->params[':end_date'] = substr($end_date, 0, 10);
		}
		//Apply text filter if needed
		if($filter != '')
		{
			$command->andWhere('c.name LIKE :name');
			$command->params[':name'] = '%'.$filter.'%';
		}
		$rows = $command->queryAll();

		foreach($rows as $course)
		{
			if(!isset($result[$course['idCourse']]))
				$result[$course['idCourse']] = array(
						'id' => $course['idCourse'].'_0_0_0_0',
						'title' => (strlen($course['name']) > 23 ? substr($course['name'], 0, 23).'...' : $course['name']),
						'start' => $course['date_end'].' 00:00:00',
						'end' => $course['date_end'].' 00:00:00',
						'allDay' => true
				);
		}

		return $result;
	}

	private function getClassroom($filter, $userId, $infinity = false){

		$result = array();

		if(!$infinity){
			$start_date = date('Y-m-d', Yii::app()->request->getParam('start', time())).' 00:00:00';
			$end_date = date('Y-m-d', Yii::app()->request->getParam('end', time())).' 23:59:59';
		}

		$command = Yii::app()->db->createCommand();
		$command->select('cs.course_id, cs.id_session, cs.name, csd.name as ses_name, csd.day, csd.time_begin, csd.time_end, csd.timezone,
		CONVERT_TZ( CONCAT_WS(" ", csd.day, csd.time_begin), csd.timezone, "GMT") as dateBegin,
		CONVERT_TZ( CONCAT_WS(" ", csd.day, csd.time_end), csd.timezone, "GMT") as dateEnd');
		$command->from(LtCourseSession::model()->tableName().' cs');
		$command->join(LtCourseSessionDate::model()->tableName().' csd', 'cs.id_session = csd.id_session');
		$command->join(LtCourseuserSession::model()->tableName().' cus', 'cs.id_session = cus.id_session');
		$command->where('cus.id_user = :id_user');
		$command->params[':id_user'] = $userId;
		if(!$infinity){
			$command->andWhere('convert_tz(concat(csd.day, " ", csd.time_begin), csd.timezone, "GMT") BETWEEN :start_date AND :end_date');
			$command->params[':start_date'] = Yii::app()->localtime->toUTC($start_date, 'Y-m-d H:i:s');
			$command->params[':end_date'] = Yii::app()->localtime->toUTC($end_date, 'Y-m-d H:i:s');
		}
		//Apply text filter if needed
		if($filter != '')
		{
			$command->andWhere('cs.name LIKE :name');
			$command->params[':name'] = '%'.$filter.'%';
		}
		$rows = $command->queryAll();

		$result = array();
		foreach($rows as $course)
		{
			$result[] = array(
					'id' => $course['course_id'].'_'.$course['id_session'].'_'.str_replace('-', '', $course['day']).'_0_0',
					'title' => (strlen($course['name']) > 23 ? substr($course['name'], 0, 23).'...' : $course['name']),
					'start' =>(isset($course['timezone']) && $course['timezone']  && $infinity) ? Yii::app()->localtime->toLocalDateTime($course['day'].' '.$course['time_begin'], 'short', 'short', 'Y-m-d H:i:s', $course['timezone']) : Yii::app()->localtime->toLocalDateTime($course['dateBegin'], LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, 'Y-m-d H:i:s'),
					'end' =>(isset($course['timezone']) && $course['timezone'] && $infinity ) ? Yii::app()->localtime->toLocalDateTime($course['day'].' '.$course['time_end'], 'short', 'short', 'Y-m-d H:i:s', $course['timezone']) : Yii::app()->localtime->toLocalDateTime($course['dateEnd'], LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, 'Y-m-d H:i:s'),
					'allDay' => false
			);


		}

		return $result;
	}

//	private function getWebinar($filter, $userId, $infinity = false, $calendarView = false){
//
//		$result = array();
//
//		//Add first the Web Conferences that are part of the Web Conference widget!!!
//		$command = Yii::app()->db->createCommand();
//		$command->select('c.idCourse, ws.id_session, ws.name, wsd.day, wsd.time_begin, wsd.duration_minutes, wsd.timezone_begin, c.course_type');
//		$command->from(LearningCourse::model()->tableName().' c');
//		$command->join(LearningCourseuser::model()->tableName().' cu', 'c.idCourse = cu.idCourse');
//		$command->join(WebinarSession::model()->tableName().' ws', 'c.idCourse = ws.course_id');
//		$command->join(WebinarSessionDate::model()->tableName().' wsd', 'ws.id_session = wsd.id_session');
//		$command->leftJoin(WebinarSessionUser::model()->tableName().' cus', 'ws.id_session = cus.id_session');
//
//		$command->where('cu.idUser = :id_user');
//		$command->andWhere('cus.id_user = :id_user OR (cus.id_user IS NULL  AND c.course_type !="webinar")');
//
//		$command->params[':id_user'] = $userId;
//		if(!$infinity){
//			$start_date = date('Y-m-d', Yii::app()->request->getParam('start', time())).' 00:00:00';
//			$end_date = date('Y-m-d', Yii::app()->request->getParam('end', time())).' 23:59:59';
//
//			$command->andWhere('wsd.day BETWEEN :start_date AND :end_date');
//			$command->params[':start_date'] =  Yii::app()->localtime->toUTC($start_date, 'Y-m-d H:i:s');
//			$command->params[':end_date'] =  Yii::app()->localtime->toUTC($end_date, 'Y-m-d H:i:s');
//		}
//
//		//Apply text filter if needed
//		if($filter != '')
//		{
//			$command->andWhere('ws.name LIKE :name');
//			$command->params[':name'] = '%'.$filter.'%';
//		}
//
//		$rows = $command->queryAll();
//
//		foreach($rows as $course)
//		{
//			$startTime = $course['day'] . " " . $course['time_begin'];
//			$startTimestamp = strtotime($startTime);
//			$endTimestamp = $startTimestamp + ($course['duration_minutes'] * 60);
//
//			$result[] = array(
//					//'id' => $course['idCourse'].'_0_0_0_'.$course['id_session'],
//					'id' => $course['idCourse'] . '_0_' . $course['day'] . '_' . $course['id_session'] . '_0',
//					'title' => (strlen($course['name']) > 23 ? substr($course['name'], 0, 23).'...' : $course['name']),
//					'start' => Yii::app()->localtime->toLocalDateTime($course['day'] . " " . $course['time_begin'], LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, 'Y-m-d H:i:s', $course['timezone_begin']),
//					'end' => Yii::app()->localtime->toLocalDateTime(date("Y-m-d H:i:s", $endTimestamp), LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, 'Y-m-d H:i:s', $course['timezone_begin']),
//					'allDay' => false
//			);
//		}
//
//		if(!$calendarView){
//			//Then Webinars
//			$start_date = date('Y-m-d', Yii::app()->request->getParam('start', time())).' 00:00:00';
//			$end_date = date('Y-m-d', Yii::app()->request->getParam('end', time())).' 23:59:59';
//
//			$command = Yii::app()->db->createCommand();
//			$command->select('cs.course_id, cs.id_session, CONVERT_TZ( CONCAT_WS(" ", wsd.day, wsd.time_begin), wsd.timezone_begin, :timezone) as dateBegin, wsd.day, wsd.duration_minutes, wsd.name');
//			$command->from(WebinarSession::model()->tableName().' cs');
//			$command->join(WebinarSessionUser::model()->tableName().' cus', 'cs.id_session = cus.id_session');
//			$command->join(WebinarSessionDate::model()->tableName().' wsd', 'cs.id_session = wsd.id_session');
//			$command->where('cus.id_user = :id_user');
//			$command->params[':id_user'] = $userId;
//			$command->andWhere('CONVERT_TZ(cs.date_begin, "GMT", :timezone) BETWEEN :start_date AND :end_date');
//			$command->params[':start_date'] = Yii::app()->localtime->toUTC($start_date);
//			$command->params[':end_date'] = Yii::app()->localtime->toUTC($end_date);
//			$command->params[':timezone'] = Yii::app()->localtime->getTimeZone();
//
//			//Apply text filter if needed
//			if($filter != '')
//			{
//				$command->andWhere('wsd.name LIKE :name');
//				$command->params[':name'] = '%'.$filter.'%';
//			}
//			$rows = $command->queryAll();
//
//			foreach($rows as $course)
//			{
//				$startTimestamp = strtotime($course['dateBegin']);
//				$endTimestamp = $startTimestamp + ($course['duration_minutes'] * 60);
//
//				$result[] = array(
//					'id' => $course['course_id'].'_0_'.str_replace('-', '', $course['day']).'_'.$course['id_session'].'_0',
//					'title' => (strlen($course['name']) > 23 ? substr($course['name'], 0, 23).'...' : $course['name']),
//					'start' => $course['dateBegin'],
//					'end' => $endTimestamp,
//					'allDay' => false
//				);
//			}
//		}
//
//		return $result;
//	}


	/**
	 * Load the event for elearning courses
	 */
	public function actionFetchElearning($forceFetch = false)
	{
		//Check if we need to load the data
		if(Yii::app()->session['elearning_filter'] == 0 && !$forceFetch)
		{
			echo CJSON::encode(array());
			Yii::app()->end();
		}

		$elearningCourses = $this->getElearning(Yii::app()->session['text_filter'], Yii::app()->user->id);

		$result = array_values($elearningCourses);

		foreach($result as &$res) {
			if(isset($res['title'])){
				$res['title'] = htmlspecialchars($res['title'], ENT_IGNORE, 'UTF-8');
			}
		}

		echo json_encode($result);

		Yii::app()->end();
	}

	/**
	 * Load the event for classroom courses
	 */
	public function actionFetchClassroom($forceFetch = false)
	{
		//Check if we need to load the data
		if(Yii::app()->session['classroom_filter'] == 0 && !$forceFetch)
		{
			echo CJSON::encode(array());
			Yii::app()->end();
		}

		$result = $this->getClassroom(Yii::app()->session['text_filter'], Yii::app()->user->id);

		foreach($result as &$res) {
			if(isset($res['title'])){
				$res['title'] = htmlspecialchars($res['title'], ENT_IGNORE, 'UTF-8');
			}
		}

		echo json_encode($result);
		Yii::app()->end();
	}


	/**
	 * Load the scheduled meetings for videoconference
	 */
	public function actionFetchVideoconference($forceFetch = false)
	{
		//Check if we need to load the data
		if(Yii::app()->session['videoconference_filter'] == 0 && !$forceFetch)
		{
			echo CJSON::encode(array());
			Yii::app()->end();
		}
		$start =  Yii::app()->localtime->toUTC(date('Y-m-d', Yii::app()->request->getParam('start', time())).' 00:00:00', 'Y-m-d H:i:s');
		$end =  Yii::app()->localtime->toUTC(date('Y-m-d', Yii::app()->request->getParam('end', time())).' 23:59:59', 'Y-m-d H:i:s');

		$result = WebinarSession::getWebinar(Yii::app()->session['text_filter'], Yii::app()->user->id, false, true, strtotime($start), strtotime($end));

		//json_encode returns false if some value is not utf-8 (names usually are not)
		foreach($result as &$res) {
			if(isset($res['title'])){
				$res['title'] = htmlspecialchars($res['title'], ENT_IGNORE, 'UTF-8');
			}
		}

		echo json_encode($result);
		Yii::app()->end();
	}


	/**
	 * Print the data to display into the tooltip into the calendar
	 */
	public function actionTooltip()
	{
		$id = Yii::app()->request->getParam('id', '');
		$ex = explode('_', $id);

		$id_course = $ex[0];
		$id_session = $ex[1];
		$day = $ex[2];
		$id_videoconference = $ex[3];
		$id_conferenceroom = $ex[4];

		$day_string = substr($day, 0, 4).'-'.substr($day, 4, 2).'-'.substr($day, 6, 2);

		if($id_session != 0)
		{
			//Retrive the classrom session info
			$command = Yii::app()->db->createCommand();
			$command->select('cs.name as session_name, cs.other_info as session_description, csd.name as date_name, csd.day, csd.time_begin, csd.time_end, l.name as loc_name, l.address, cc.name_country, c.name as cla_name, csd.timezone');
			$command->from(LtCourseSession::model()->tableName().' cs');
			$command->join(LtCourseSessionDate::model()->tableName().' csd', 'cs.id_session = csd.id_session');
			$command->join(LtLocation::model()->tableName().' l', 'csd.id_location = l.id_location');
			$command->leftJoin(LtClassroom::model()->tableName().' c', 'csd.id_classroom = c.id_classroom');
			$command->join(CoreCountry::model()->tableName().' cc', 'l.id_country = cc.id_country');
			$command->where('cs.id_session = :id_session');
			$command->andWhere("csd.day = :day");
			$command->bindParam(':id_session', $id_session);
			$command->bindParam(':day', $day_string);
			$row = $command->queryRow();

			$userTimezone = Yii::app()->localtime->getTimezoneFromSettings(Yii::app()->user->id);
			$offsetInHours = Yii::app()->localtime->getTimezoneOffsetSecondsFromUTC($row['timezone']) / 3600;
			$offsetInHoursUser = Yii::app()->localtime->getTimezoneOffsetSecondsFromUTC($userTimezone) / 3600;
			$totalOffset = $offsetInHours - $offsetInHoursUser;

			$dateTimeUTC = new DateTime($row['day'] . ' ' . $row['time_begin'], new DateTimeZone($row['timezone']) );
			$dateTimeUTC->setTimezone(new DateTimeZone($userTimezone));
			$dateTimeEndUTC = new DateTime($row['day'] . ' ' . $row['time_end'], new DateTimeZone($row['timezone']) );
			$dateTimeEndUTC->setTimezone(new DateTimeZone($userTimezone));

			$timestamp = $dateTimeUTC->getTimestamp();

			$beginHour = substr($row['time_begin'], 0, 2);
			$beginHour -= $totalOffset;
			$newDayBegin = $row['day'];
			if ($beginHour >= 24) {
				$beginHour -= 24;
				$newDayBegin = date('Y-m-d', strtotime($row['day']. ' +1 day'));
			}
			if ($beginHour < 0) {
				$beginHour += 24;
				$newDayBegin = date('Y-m-d', strtotime($row['day']. ' -1 day'));
			}
			$beginMinute = substr($row['time_begin'], 3, 2);
			$row['time_begin'] = $beginHour . ':' . $beginMinute;
			if (strlen($row['time_begin']) < 5) $row['time_begin'] = '0' . $row['time_begin'];
			$row['time_begin'].=':00';

			$endHour = substr($row['time_end'], 0, 2);
			$endHour -= $totalOffset;
			$newDay = $row['day'];
			if ($endHour >= 24) {
				$endHour -= 24;
				$newDay = date('Y-m-d', strtotime($row['day']. ' +1 day'));
			}
			if ($endHour < 0) {
				$endHour += 24;
				$newDay = date('Y-m-d', strtotime($row['day']. ' -1 day'));
			}
			$endMinute = substr($row['time_end'], 3, 2);
			$row['time_end'] = $endHour . ':' . $endMinute;
			if (strlen($row['time_end']) < 5) $row['time_end'] = '0' . $row['time_end'];
			$row['time_end'].=':00';

			$day = $dateTimeUTC->format('w');
			$day_n = $dateTimeUTC->format('d');
			$month = $dateTimeUTC->format('m');
			$year = $dateTimeUTC->format('Y');
			$row['time_begin'] = $dateTimeUTC->format('H:i');
			$row['time_end'] = $dateTimeEndUTC->format('H:i');

			$time_begin = substr($row['time_begin'], 0, 5) ;
			$time_end = substr($row['time_end'], 0, 5);

			$courseModel = LearningCourse::model()->findByPk($id_course);
			if($courseModel && isset($courseModel->course_type) && ( $courseModel->course_type == 'telephone' ||  $courseModel->course_type == 'videoconference')){
				$time_begin = substr(str_replace(Yii::app()->localtime->stripTimeFromLocalDateTime($row['time_begin']),'',Yii::app()->localtime->toLocalDateTime($time_begin)),0, 6);
				$time_end =  substr(str_replace(Yii::app()->localtime->stripTimeFromLocalDateTime($row['time_end']),'',Yii::app()->localtime->toLocalDateTime($time_end)),0, 6);
			}

//			$timestamp = (int)$this->fromDatetimeToTimestamp($row['day']);
			$commonParams = $this->getCommonParamsForTooltip($timestamp, ($row['date_name'] != '' ? $row['date_name'] : $row['session_name']), $courseModel->name);

			$params = array(
				'day_t' => $this->day_translation[$day],
				'day_n' => $day_n,
				'month_t' => $this->month_translation[$month],
				'year' => $year,
				'course_name' => $courseModel->name,
				'session_name' => $row['session_name'],
				'session_description' => $this->formatDescription($row['session_description']),
				'date_name' => $row['date_name'],
				'time_begin' => Yii::app()->localtime->toLocalTime($time_begin, LocalTime::SHORT),
				'time_end' => Yii::app()->localtime->toLocalTime($time_end, LocalTime::SHORT),
//				'timezone' => '(GMT '.$offset.') '.$timezone->getName(),
				'location' => ''
					.$row['loc_name'].'<br />'
					.$row['cla_name'].'<br />'
					.nl2br($row['address']).' '.$row['name_country']
			);

			$params = array_merge($commonParams, $params);
		}
		elseif($id_videoconference != 0 || $id_conferenceroom != 0)
		{
			$row = array();
			if($id_conferenceroom){
				//Retrive the videoconference session info
				$command = Yii::app()->db->createCommand();
				$command->select('ws.name as session_name, ws.date_begin, ws.date_end, ws.description as session_description, c.name as course_name, c.course_type, wsd.name as date_name, wsd.description as date_description');
				$command->from(LearningCourse::model()->tableName().' c');
				$command->join(LearningCourseuser::model()->tableName().' cu', 'c.idCourse = cu.idCourse');
				$command->join(WebinarSession::model()->tableName().' ws', 'c.idCourse = ws.course_id');
				$command->leftJoin(WebinarSessionDate::model()->tableName().' wsd', 'ws.id_session = wsd.id_session');
				$command->where('ws.id_session = :id');
				$command->params[':id'] = $id_conferenceroom;
				$row = $command->queryRow();
			}

			$commonParams = array();
			if(!$row && $id_videoconference){
				$command = Yii::app()->db->createCommand();
				$command->select('wsd.day, wsd.time_begin, wsd.duration_minutes, wsd.timezone_begin, c.name as course_name, c.course_type, wsd.name as date_name, wsd.description as date_description, cs.description as session_description, cs.name as session_name');
				$command->from(WebinarSession::model()->tableName().' cs');
				$command->join(WebinarSessionDate::model()->tableName().' wsd', 'cs.id_session = wsd.id_session');
				$command->join(LearningCourse::model()->tableName().' c', 'c.idCourse = cs.course_id');


				// Check is user not guest and is he/she enrolled to the course
				$is_enrolled = (!(Yii::app()->user->isGuest)) ? (LearningCourseuser::isSubscribed(Yii::app()->user->id, $id_course)) : false;
				if($is_enrolled) {
					$command->leftJoin(WebinarSessionUser::model()->tableName().' cus', 'cs.id_session = cus.id_session and cus.id_user = :id_user');
					//$command->where('cus.id_user = :id_user OR cus.id_user IS NULL');
					$command->params[':id_user'] = Yii::app()->user->id;
				}


				$command->andWhere('cs.id_session = :id_session');
				$command->params[':id_session'] = $id_videoconference;

				$rows = $command->queryAll();

				foreach($rows as $row)
				{
					$userTimezone = Yii::app()->localtime->getTimezoneFromSettings(Yii::app()->user->id);

					$newDayBeginUserTz = new DateTime($row['day']. ' '. $row['time_begin'], new DateTimeZone($row['timezone_begin']));
					$newDayBeginUserTz->setTimezone(new DateTimeZone('GMT'));

					if($newDayBeginUserTz->format('Y-m-d') != $day)
						continue;

					$dateBegin = new DateTime($row['day']. ' '. $row['time_begin'], new DateTimeZone($row['timezone_begin']));
                    $dateBegin->setTimezone(new DateTimeZone($userTimezone));

					$row['date_begin'] = strtotime($dateBegin->format('Y-m-d H:i:s'));
                    $row['time_begin'] = $dateBegin->format('H:i:s');
					$row['date_end'] = $row['date_begin'] + ($row['duration_minutes'] * 60);

					$commonParams = $this->getCommonParamsForTooltip($row['date_begin'], $row['name'], $row['course_name']);
					$commonParams['course_type'] = $row['course_type'];
					$commonParams['session_id'] = $id_videoconference;

					break;
				}

			}elseif($row){
				$row['date_begin'] = strtotime(Yii::app()->localtime->toLocalDateTime($row['date_begin'], '', '', 'Y-m-d H:i:s'));
				$row['date_end'] = strtotime(Yii::app()->localtime->toLocalDateTime($row['date_end'], '', '', 'Y-m-d H:i:s'));
				$commonParams = $this->getCommonParamsForTooltip($row['date_begin'], ($row['date_name'] != '' ? $row['date_name'] : $row['session_name']), $row['course_name']);
			}


			$day = date('w', $row['date_begin']);
			$day_n = date('d', $row['date_begin']);
			$month = date('m', $row['date_begin']);
			$year = date('Y', $row['date_begin']);
			$time_begin = Yii::app()->localtime->toLocalTime($row['date_begin'], LocalTime::SHORT);
			$time_end = Yii::app()->localtime->toLocalTime($row['date_end'], LocalTime::SHORT);

			$params = array(
				'day_t' => $this->day_translation[$day],
				'day_n' => $day_n,
				'month_t' => $this->month_translation[$month],
				'year' => $year,
				'course_name' => !empty($row['course_name']) ? $row['course_name'] : '',
				'time_begin' => $time_begin,
				'time_end' => $time_end,
				'date_name' => $row['date_name'],
				'date_description' => $this->formatDescription($row['date_description']),
				'session_description' => $this->formatDescription($row['session_description']),
				'session_name' => $row['session_name'],
			);
			$params = array_merge($commonParams, $params);
		}else{
			//Retrive the elearning course info
			$command = Yii::app()->db->createCommand();
			$command->select('c.name, c.description, cu.date_expire_validity, c.date_end, cu.date_first_access');
			$command->from(LearningCourse::model()->tableName().' as c');
			$command->join(LearningCourseuser::model()->tableName().' as cu', 'c.idCourse = cu.idCourse');
			$command->where('c.idCourse = :id_course');
			$command->andWhere('cu.idUser = :id_user');
			$command->bindParam(':id_course', $id_course);
			$command->bindParam(':id_user', Yii::app()->user->id);
			$row = $command->queryRow();

			$date = $row['date_expire_validity'];
			if(is_null($row['date_expire_validity']) || $row['date_expire_validity'] == '0000-00-00 00:00:00')
				$date = $row['date_end'].' 00:00:00';

			if (PluginManager::isPluginActive("CurriculaApp")) {
				$curriculaAppRow = Yii::app()->db->createCommand()
					->select('c.idCourse, c.name, cpu.date_end_validity')
					->from(LearningCoursepath::model()->tableName().' cp')
					->join(LearningCoursepathCourses::model()->tableName().' cpc', 'cpc.id_path = cp.id_path AND cp.days_valid IS NOT NULL')
					->join(LearningCourse::model()->tableName().' c', 'c.idCourse = cpc.id_item')
					->join(LearningCoursepathUser::model()->tableName().' cpu', 'cpu.id_path = cp.id_path')
					->andWhere('cpu.idUser = :idUser', array(':idUser' => Yii::app()->user->id))
					->andWhere('cpu.date_end_validity IS NOT NULL AND date_end_validity <> "0000-00-00 00:00:00"')
					->andWhere('c.idCourse = :idCourse', array(':idCourse' => $id_course))
					->queryRow();

				if($curriculaAppRow)
					$date = $curriculaAppRow['date_end_validity'];
			}

			$timestamp = strtotime(Yii::app()->localtime->toLocalDateTime($date, LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, 'Y-m-d H:i:s'));
			$params = $this->getCommonParamsForTooltip($timestamp, $row['name'], '');
			$params['is_elearning'] = true;
			$params['session_name'] = $row['name'];
			$params['course_name'] = $row['name'];
			$params['session_description'] = $this->formatDescription($row['description']);
		}

		//Check if the user can enter into the course
		$courseUserModel = LearningCourseuser::model()->findByAttributes(array('idUser' => Yii::app()->user->id, 'idCourse' => $id_course));
		$can_enter = false;

		$is_enrolled = (!(Yii::app()->user->isGuest)) ? (LearningCourseuser::isSubscribed(Yii::app()->user->id, $id_course)) : false;

		if(!is_null($courseUserModel)) {
            $can_enter = $courseUserModel->canEnterCourse();
            $can_enter = $can_enter['can'];
        }

		$webinar_session_enrolled = false;
		$classroom_session_enrolled = false;

		if($is_enrolled && ($id_videoconference || $id_session))
		{
			if($id_session)
				$classroom_session_enrolled = (bool)LtCourseuserSession::model()->findByAttributes(array('id_user' => Yii::app()->user->id, 'id_session' => $id_session));
			elseif($id_videoconference)
				$webinar_session_enrolled = (bool)WebinarSessionUser::model()->findByAttributes(array('id_user' => Yii::app()->user->id, 'id_session' => $id_videoconference));
		}

		$params['can_enter'] = $can_enter;
		$params['id_course'] = $id_course;
		$params['is_enrolled'] = $is_enrolled;
		$params['classroom_session_enrolled'] = $classroom_session_enrolled;
		$params['webinar_session_enrolled'] = $webinar_session_enrolled;
		$params['show_course_details_dedicated_page'] = Settings::get('show_course_details_dedicated_page', 'off');

		//Print the tooltip content to be displayed
		echo $this->renderPartial('tooltip', $params, true);
		Yii::app()->end();
	}

	public function formatDescription($description) {
		$str = strip_tags($description);
		if(mb_strlen($str) > 100) {
			$str = substr($str, 0, 97);
			$str .= '...';
		}

		return $str;
	}

	public function getCommonParamsForTooltip($date, $name, $courseName){
		$day = date('w', $date);
		$day_n = date('d', $date);
		$month = date('m', $date);
		$year = date('Y', $date);

		return array(
			'course_name' => $courseName,
			'day_t' => $this->day_translation[$day],
			'day_n' => $day_n,
			'month_t' => $this->month_translation[$month],
			'year' => $year,
			'name' => $name
		);
	}

	/**
	 *
	 * @param type $datetime
	 * @return string
	 */
	private function fromDatetimeToTimestamp($datetime)
	{
		$timestamp = '';
		if($datetime == '') return $timestamp;

		if(strlen($datetime) < 11) {

			$timestamp = mktime(	0, 0, 0,
				substr($datetime, 5, 2), substr($datetime, 8, 2), substr($datetime, 0, 4) );
		} else {

			$timestamp = mktime(	substr($datetime, 11, 2), substr($datetime, 14, 2), substr($datetime, 17, 2),
				substr($datetime, 5, 2), substr($datetime, 8, 2), substr($datetime, 0, 4) );
		}
		return $timestamp;
	}


	/**
	 * Set the filters when needed
	 */
	public function actionSetFilters()
	{
		if(isset($_POST['elearning_filter']))
			Yii::app()->session['elearning_filter'] = Yii::app()->request->getParam('elearning_filter', 0);
		if(isset($_POST['classroom_filter']))
			Yii::app()->session['classroom_filter'] = Yii::app()->request->getParam('classroom_filter', 0);
		if(isset($_POST['videoconference_filter']))
			Yii::app()->session['videoconference_filter'] = Yii::app()->request->getParam('videoconference_filter', 0);
		if(isset($_POST['text_filter']))
			Yii::app()->session['text_filter'] = Yii::app()->request->getParam('text_filter', '');
		Yii::app()->end();
	}

	public function actionGetNextEvent()
	{
		$result = array();

		if((Yii::app()->session['elearning_filter'] != 0 || Yii::app()->session['videoconference_filter'] != 0 || Yii::app()->session['classroom_filter'] != 0) && Yii::app()->session['text_filter'] != '')
		{
			$union = false;
			$base_command = Yii::app()->db->createCommand();
			if(Yii::app()->session['elearning_filter'] != 0)
			{
				$base_command->select('unix_timestamp(cu.date_expire_validity) as start_date');
				$base_command->from(LearningCourse::model()->tableName().' as c');
				$base_command->join(LearningCourseuser::model()->tableName().' as cu', 'c.idCourse = cu.idCourse');
				$base_command->where('cu.idUser = :id_user');
				$base_command->params[':id_user'] = Yii::app()->user->id;
				$base_command->andWhere('cu.date_expire_validity IS NOT NULL');
				$base_command->andWhere('cu.date_expire_validity >= :date');
				$base_command->params[':date'] = Yii::app()->localtime->getUTCNow();
				$base_command->andWhere('c.name LIKE :name');
				$base_command->params[':name'] = '%'.Yii::app()->session['text_filter'].'%';

				$command = Yii::app()->db->createCommand();
				$command->select('unix_timestamp(c.date_end) as start_date');
				$command->from(LearningCourse::model()->tableName().' as c');
				$command->join(LearningCourseuser::model()->tableName().' as cu', 'c.idCourse = cu.idCourse');
				$command->where('cu.idUser = :id_user');
				$command->params[':id_user'] = Yii::app()->user->id;
				$command->andWhere('cu.date_expire_validity IS NULL');
				$command->andWhere('c.date_end >= :date');
				$command->params[':date'] = substr(Yii::app()->localtime->getUTCNow(), 0, 10);
				$command->andWhere('c.name LIKE :name');
				$command->params[':name'] = '%'.Yii::app()->session['text_filter'].'%';

				$base_command->union($command->getText());

				$union = true;
			}
			if(Yii::app()->session['classroom_filter'] != 0)
			{
				if($union)
				{
					$command = Yii::app()->db->createCommand();
					$command->select('unix_timestamp(convert_tz(concat(csd.day, " ", csd.time_begin), csd.timezone, "GMT")) as start_date');
					$command->from(LtCourseSession::model()->tableName().' cs');
					$command->join(LtCourseSessionDate::model()->tableName().' csd', 'cs.id_session = csd.id_session');
					$command->join(LtCourseuserSession::model()->tableName().' cus', 'cs.id_session = cus.id_session');
					$command->where('cus.id_user = :id_user');
					$command->params[':id_user'] = Yii::app()->user->id;
					$command->andWhere('convert_tz(concat(csd.day, " ", csd.time_begin), csd.timezone, "GMT") >= :date');
					$command->params[':date'] = Yii::app()->localtime->getUTCNow();
					$command->andWhere('cs.name LIKE :name');
					$command->params[':name'] = '%'.Yii::app()->session['text_filter'].'%';
					$base_command->union($command->getText());
				}
				else
				{
					$base_command->select('unix_timestamp(convert_tz(concat(csd.day, " ", csd.time_begin), csd.timezone, "GMT")) as start_date');
					$base_command->from(LtCourseSession::model()->tableName().' cs');
					$base_command->join(LtCourseSessionDate::model()->tableName().' csd', 'cs.id_session = csd.id_session');
					$base_command->join(LtCourseuserSession::model()->tableName().' cus', 'cs.id_session = cus.id_session');
					$base_command->where('cus.id_user = :id_user');
					$base_command->params[':id_user'] = Yii::app()->user->id;
					$base_command->andWhere('convert_tz(concat(csd.day, " ", csd.time_begin), csd.timezone, "GMT") >= :date');
					$base_command->params[':date'] = Yii::app()->localtime->getUTCNow();
					$command->andWhere('cs.name LIKE :name');
					$command->params[':name'] = '%'.Yii::app()->session['text_filter'].'%';
					$union = true;
				}
			}
			if(Yii::app()->session['videoconference_filter'] != 0)
			{
				if($union)
				{
					$command = Yii::app()->db->createCommand();
					$command->select('cr.starttime as start_date');
					$command->from(LearningCourse::model()->tableName().' c');
					$command->join(LearningCourseuser::model()->tableName().' cu', 'c.idCourse = cu.idCourse');
					$command->join(ConferenceRoom::model()->tableName().' cr', 'c.idCourse = cr.idCourse');
					$command->where('cu.idUser = :id_user');
					$command->params[':id_user'] = Yii::app()->user->id;
					$command->andWhere('cr.starttime >= :date');
					$command->params[':date'] = strtotime(Yii::app()->localtime->getUTCNow());
					$command->andWhere('cr.name LIKE :name');
					$command->params[':name'] = '%'.Yii::app()->session['text_filter'].'%';
					$base_command->union($command->getText());
				}
				else
				{
					$base_command = Yii::app()->db->createCommand();
					$base_command->select('cr.starttime as start_date');
					$base_command->from(LearningCourse::model()->tableName().' c');
					$base_command->join(LearningCourseuser::model()->tableName().' cu', 'c.idCourse = cu.idCourse');
					$base_command->join(ConferenceRoom::model()->tableName().' cr', 'c.idCourse = cr.idCourse');
					$base_command->where('cu.idUser = :id_user');
					$base_command->params[':id_user'] = Yii::app()->user->id;
					$base_command->andWhere('cr.starttime >= :date');
					$base_command->params[':date'] = strtotime(Yii::app()->localtime->getUTCNow());
					$command->andWhere('cr.name LIKE :name');
					$command->params[':name'] = '%'.Yii::app()->session['text_filter'].'%';
				}
			}

			$base_command->setOrder('start_date asc');
			//var_dump($base_command->getText());die();
			$row = $base_command->queryRow();
			$result['year'] = date('Y', $row['start_date']);
			$result['month'] = date('m', $row['start_date']) - 1;
			$result['day'] = date('d', $row['start_date']);
			$result['success'] = is_array($row);
		}

		echo CJSON::encode($result);
		Yii::app()->end();
	}


	public function actionLoadCatalogCalendar() {
		//Here we set the initial value for filters
		if (!(Yii::app()->user->isGuest)) {
			Yii::app()->session['elearning_filter'] = 1;
		} else {
			Yii::app()->session['elearning_filter'] = 0;
		}

		Yii::app()->session['classroom_filter'] = 1;
		Yii::app()->session['videoconference_filter'] = 1;
		Yii::app()->session['text_filter'] = '';

		$this->renderPartial('index', array());
	}

	/**
	 * Load the event for calendar
	 */
	public function actionFetchCalendarItems($forceFetch = false)
	{
		$result = $this->getCalendarItems(); //text_filter

		echo CJSON::encode($result);
		Yii::app()->end();
	}

	private function getCalendarItems($filter = false, $infinity = false, $getTooltip = false){
		$result = array();
		$getTooltip = Yii::app()->request->getParam('getTooltip', false);
		$limit = -1;
		$fltNotEnrolled	= Yii::app()->request->getParam('not_enrolled_courses', 0);
		$coursesToNotDisplayList = array();

		$dashletId = Yii::app()->request->getParam('dashletId', false);
		$filter = Yii::app()->request->getParam('search_input', false);
		$fltItemType = Yii::app()->request->getParam('item_type', 'all');//$this->getParam('initial_item_type', self::TYPE_ELEARNING));
		$fltCourseFields = Yii::app()->request->getParam('advancedSearch', array('additional' => array()));

		// An array that will hold the mixture of all
		// classroom and videoconference items.
		// It will later be ordered by key (which is the timestamp
		// of the item)
		$allItems = array();

		// *** Category filter ***
		$fltCategory = Yii::app()->request->getParam('category', 'all');

		// *** Catalog filter ***
		// Check if the current catalog ID filter (e.g. initial dashlet setting) is among the catalogs visible to the current user. If not, revert to all
		$fltCatalog = Yii::app()->request->getParam('catalog', 'all');
		$enrolledCoursesList = CoreUser::model()->getSubscribedCoursesList(Yii::app()->user->idst, false);

		$userCatalogs = false;
		if(!(Yii::app()->user->isGuest)) {
			$dashletCatalog = new DashletCatalog();
			$userVisibleCatalogs = $userCatalogs = $dashletCatalog->getUserAvailableCatalogs(true);//false); - to exclude empty catalogs

			// If we need to get only the courses user is not enrolled in
			if(($fltNotEnrolled == 1)) {
				$coursesToNotDisplayList = array_merge($coursesToNotDisplayList, $enrolledCoursesList);
			}
		} else {
			// Get External Catalogs
			$catalog_external_selected_catalogs = Settings::get('catalog_external_selected_catalogs', '');
			if ($catalog_external_selected_catalogs != '') {
				$userVisibleCatalogs = $userCatalogs = array();
				$catalog_external_selected_catalogs = explode(',', $catalog_external_selected_catalogs);

				if (is_array($catalog_external_selected_catalogs) && count($catalog_external_selected_catalogs) > 0) {
					$criteria = new CDbCriteria();
					$criteria->addInCondition('idCatalogue', $catalog_external_selected_catalogs);
					$sql = 'SELECT cat.idCatalogue, cat.name FROM ' . LearningCatalogue::model()->tableName().' cat WHERE '
							. $criteria->condition;
					$command = Yii::app()->db->createCommand($sql);

					$userCatalogs = $command->queryAll(true, $criteria->params);
					if (!empty($userCatalogs)) {
						foreach($userCatalogs as $catalog) {
							$catalogs[$catalog['idCatalogue']] = $catalog['name'];
						}

						$userVisibleCatalogs = $userCatalogs = $catalogs;
					}
				}
			}
		}

		$excludedCoursesBySetting = LearningCourse::model()->getCoursesToNotDisplay($enrolledCoursesList);

		if(!empty($excludedCoursesBySetting)){
			$coursesToNotDisplayList = array_merge($coursesToNotDisplayList, $excludedCoursesBySetting);
		}

		if($userCatalogs === array()) {
			$userCatalogs = false;
		}

		if(!isset($userVisibleCatalogs[$fltCatalog]))
			$fltCatalog = 'all';

		if($fltCatalog <> 'all') {
			$userCatalogs = array($fltCatalog => $fltCatalog);
		}

		if(!Settings::get("module_ecommerce", "off") == "on") {
			$fltPricingType 	= DashletCatalog::PRICING_TYPE_FREE; // Force display of only Free courses if the ecommerce app is disabled
		} else {
			$fltPricingType = Yii::app()->request->getParam('pricing_type', 'all');
		}

		if(!$infinity && !$getTooltip){
			$start_date = strtotime(date('Y-m-d', Yii::app()->request->getParam('start', time())). ' -1 month');
			$start_date = date('Y-m-d', $start_date).' 00:00:00';
			$end_date = strtotime(date('Y-m-d', Yii::app()->request->getParam('end', time())) .  ' +1 month');
			$end_date = date('Y-m-d', $end_date).' 23:59:59';
		} elseif($getTooltip) {
			$start_date = date('Y-m-d H:i:s', Yii::app()->request->getParam('start'));
			$end_date = date('Y-m-d H:i:s', Yii::app()->request->getParam('end'));
		}

		// Get classroom courses
		if($fltItemType == 'all' || $fltItemType == 'classroom') {
			$classroomIdsStr = '';
			if(Yii::app()->user->idst) {
				$classroomIds = CoreUser::getClassroomsIfUserIsEnrolledInAnySession(Yii::app()->user->idst);

				foreach ($classroomIds as $crId) {
					$classroomIdsStr = $classroomIdsStr . $crId . ',';
				}
				$classroomIdsStr = substr($classroomIdsStr, 0, strlen($classroomIdsStr) - 1);
			}
			$command = Yii::app()->db->createCommand();
			$command->select('cs.course_id, cs.id_session, cs.name as session_name, cs.other_info as session_description,csd.name as date_name, csd.day, csd.time_begin,
			csd.time_end,course.name, course.description, course.selling, course.code, course.course_type, course.idCourse, course.status, csd.timezone,
			cl.name AS cla_name, loc.name AS loc_name, loc.address AS address, con.name_country');
			$command->from(LtCourseSession::model()->tableName() . ' cs');
			$command->join(LtCourseSessionDate::model()->tableName() . ' csd', 'cs.id_session = csd.id_session');
			$command->leftJoin(LtCourseuserSession::model()->tableName() . ' cus', 'cus.id_session = cs.id_session');
			$command->join(LearningCourse::model()->tableName() . ' course', 'cs.course_id = course.idCourse');
			$command->leftJoin(LearningCourseFieldValue::model()->tableName() ." as lcfv", "lcfv.id_course = course.idCourse");
			$command->leftJoin(LtClassroom::model()->tableName().' cl', 'csd.id_classroom=cl.id_classroom');
			$command->join(LtLocation::model()->tableName().' loc', 'loc.id_location=csd.id_location');
			$command->join(CoreCountry::model()->tableName().' con', 'loc.id_country=con.id_country');
			$whereStatement = 'course.course_type = :course_type';
			if($classroomIdsStr && $classroomIdsStr != '') {
				$whereStatement = $whereStatement . ' AND ((cus.id_user = ' . Yii::app()->user->idst . ' AND course.idCourse IN(' . $classroomIdsStr . ')) OR course.idCourse NOT IN(' . $classroomIdsStr . '))';
			}
			//$command->group('cs.id_session');
			$command->group('csd.id_session, csd.day, csd.timezone');
			$command->andWhere($whereStatement);
			$command->params[':course_type'] = 'classroom';

			if (!$infinity || $getTooltip) {
				$command->andWhere('convert_tz(concat(csd.day, " ", csd.time_begin), csd.timezone, "GMT") BETWEEN :start_date AND :end_date');
				$command->params[':start_date'] = $start_date;//Yii::app()->localtime->toUTC($start_date, 'Y-m-d H:i:s');
				$command->params[':end_date'] = $end_date;//Yii::app()->localtime->toUTC($end_date, 'Y-m-d H:i:s');
			}

			// Exclude the courses we have to skip from showing
			if (is_array($coursesToNotDisplayList) && count($coursesToNotDisplayList)) {
				$command->andWhere(array('NOT IN', 'course.idCourse', $coursesToNotDisplayList));
			}

			if($userCatalogs && is_array($userCatalogs) && count($userCatalogs)) {
				if(!(count($userCatalogs) == 1 && !empty($userCatalogs['all']) && (Settings::get('on_catalogue_empty', 'off') != 'off'))) {
					$courseOnCondition = 'course.status IN (:effective, :available)';
					$command->params[':effective'] = LearningCourse::$COURSE_STATUS_EFFECTIVE;
					$command->params[':available'] = LearningCourse::$COURSE_STATUS_AVAILABLE;

					$command->join(LearningCatalogueEntry::model()->tableName() . ' t', 't.type_of_entry="course" AND t.idEntry=course.idCourse AND ' . $courseOnCondition);
					// Only display courses/LPs from catalogs the user is assigned to
					$command->andWhere(array('IN', 't.idCatalogue', array_keys($userCatalogs)));
				}
			}

			//Apply text filter if needed
			if ($filter != '') {
				$command->andWhere('((cs.name LIKE :name) OR (course.name LIKE :name) OR (course.description LIKE :name) OR (course.code LIKE :name))');
				$command->params[':name'] = '%' . $filter . '%';
			}

			// filter by price
			if ($fltPricingType != 'all') {
				$command->andWhere('course.selling = :selling');
				$command->params[':selling'] = ($fltPricingType == 'paid') ? 1 : 0;
			}

			// Filter by category
			if($fltCategory && $fltCategory != 'all') {
				$categories = array();
				$categories = LearningCourseCategoryTree::model()->getCategoryAndDescendants($fltCategory);
				$command->andWhere(array('IN', 'course.idCategory', $categories));
			}

			// Additional fields filtering
			foreach($fltCourseFields['additional'] AS $filterItemId => $filterItem) {
				if (!empty($filterItem['type'])) {
					switch ($filterItem['type']) {
						case 'dropdown':
							if (!empty($filterItem['value'])) {
								$command->andWhere('lcfv.field_' . $filterItemId . ' = :lcfvfield_' . $filterItemId);
								$command->params[':lcfvfield_' . $filterItemId] = $filterItem['value'];
							}
							break;

						case 'date':
							if (!empty($filterItem['value'])) {
								$command->andWhere('lcfv.field_' . $filterItemId . $filterItem['condition'] . ' :lcfvfield_' . $filterItemId);
								$command->params[':lcfvfield_' . $filterItemId] = Yii::app()->localtime->fromLocalDate($filterItem['value']);
							}

							break;

						case 'textfield':
							if (!empty($filterItem['value'])) {
								switch($filterItem['condition']) {
									case 'contains':
										$command->andWhere('lcfv.field_' . $filterItemId . ' LIKE :lcfvfield_' . $filterItemId);
										$command->params[':lcfvfield_' . $filterItemId] = "%" . $filterItem['value'] ."%";
										break;

									case 'equal':
										$command->andWhere('lcfv.field_' . $filterItemId . ' = :lcfvfield_' . $filterItemId);
										$command->params[':lcfvfield_' . $filterItemId] = $filterItem['value'];
										break;

									case 'not-equal':
										$command->andWhere('lcfv.field_' . $filterItemId . ' != :lcfvfield_' . $filterItemId);
										$command->params[':lcfvfield_' . $filterItemId] = $filterItem['value'];
										break;

									default:
										break;
								}
							}

							break;

						case 'textarea':
							if (!empty($filterItem['value'])) {
								switch($filterItem['condition']) {
									case 'contains':
										$command->andWhere('lcfv.field_' . $filterItemId . ' LIKE :lcfvfield_' . $filterItemId);
										$command->params[':lcfvfield_' . $filterItemId] = "%" . $filterItem['value'] ."%";
										break;

									case 'not-contains':
										$command->andWhere("COALESCE(lcfv.field_" . $filterItemId . ", '') NOT LIKE :lcfvfield_" . $filterItemId);
										$command->params[':lcfvfield_' . $filterItemId] = "%" . $filterItem['value'] ."%";
										break;

									default:
										break;
								}
							}

							break;
						default:
							// Not an internal field type -> instantiate the custom field handler and let it process the case
							$fieldName = 'CourseField'.ucfirst($filterItem['type']);
							$sql = '';
							if(method_exists($fieldName, "applyCourseCatalogFilter")) {
								$fieldName::applyCourseCatalogFilter($filterItemId, $filterItem['value'], $sql);
							}

							if($sql != '') {
								$sql = preg_replace('/^([\s]+)?and/i', '', $sql);
								$command->andWhere($sql);
							}

							break;
					}
				}
			}

			if(Yii::app()->user->isGuest){
				// show only visible to all users courses and learning paths
				$visibleCoursesIds = LearningCourse::getCoursesIdsVisibleInExternalCatalog();
				$command->andWhere(array('IN', 'course.idCourse', $visibleCoursesIds));
			}
			$rows = $command->queryAll();

			foreach ($rows as $course) {
				$userTimezone = Yii::app()->localtime->getTimezoneFromSettings(Yii::app()->user->id);
				$offsetInHours = Yii::app()->localtime->getTimezoneOffsetSecondsFromUTC($course['timezone']) / 3600;

                $courseStart = new DateTime($course['day'] . ' ' . $course['time_begin'], new DateTimeZone($course['timezone']));
                $courseStart->setTimezone(new DateTimeZone($userTimezone));
                $dateStart = $courseStart->format('Y-m-d h:i:s');

                $courseEnd = new DateTime($course['day'] . ' ' . $course['time_end'], new DateTimeZone($course['timezone']));
                $courseEnd->setTimezone(new DateTimeZone($userTimezone));
                $dateEnd = $courseEnd->format('Y-m-d h:i:s');

                //no idea what this thing does, it does not convert to utc that's for sure...
				if ($offsetInHours > 0) {
                    $dateTimeUTC = $dateStart . ' +' . $offsetInHours;
				} elseif ($offsetInHours < 0) {
                    $dateTimeUTC = $dateStart . ' ' . $offsetInHours;
				} else {
                    $dateTimeUTC = $dateStart;
                }

				$timestamp = strtotime(Yii::app()->localtime->toLocalDateTime($dateTimeUTC, LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, 'Y-m-d H:i:s'));

				if(!$getTooltip) {
					$result[] = array(
							'id' => $course['course_id'] . '_' . $course['id_session'] . '_' . str_replace('-', '', $course['day']) . '_0_0',
							// Fix the problem with no utf8 characters
							'title' => (mb_strlen($course['name']) > 23 ? mb_substr($course['name'], 0, 23) . '...' : $course['name']),
							'start' => $dateStart,
							'end' => $dateEnd,
							'allDay' => false,
							'color' => '#5ebf62',
							'textColor' => '#ffffff'
					);
				} else {
					// add tooltip items here
					$allItems[] = array(
							'id' => $course['course_id'] . '_' . $course['id_session'] . '_' . str_replace('-', '', $course['day']) . '_0_0',
							'name' => (strlen($course['session_name']) > 23 ? substr($course['session_name'], 0, 23) . '...' : $course['session_name']),
							'timestamp' => $timestamp,
							'start' => $dateStart,
							'end' => $dateEnd,
							'type'=>'classroom',
							'courseName' => $course['name'],
							'sessionDescription' => $course['session_description'],
							'dateName' => $course['date_name'],
							'location' => ''
									.$course['loc_name'].'<br />'
									.$course['cla_name'].'<br />'
									.nl2br($course['address']).' '.$course['name_country']
					);
				}
			}
		}

		if($fltItemType == 'webinar' || $fltItemType == 'all') {
			$webinarIdsStr = '';
			if(Yii::app()->user->idst) {
				$webinarIds = CoreUser::getWebinarsIfUserIsEnrolledInAnySession(Yii::app()->user->idst);

				foreach ($webinarIds as $wId) {
					$webinarIdsStr = $webinarIdsStr . $wId . ',';
				}
				$webinarIdsStr = substr($webinarIdsStr, 0, strlen($webinarIdsStr) - 1);
			}
			//Add first the Web Conferences that are part of the Web Conference widget!!!
			$command = Yii::app()->db->createCommand();
			$command->select('course.idCourse, ws.id_session, ws.name AS session_name, ws.date_begin, ws.description as session_description, ws.date_end, course.name,
			course.description, course.selling, course.code, course.course_type, course.status,
			wsd.day, wsd.time_begin, wsd.duration_minutes, wsd.timezone_begin,
			CONVERT_TZ( CONCAT_WS(" ", wsd.day, wsd.time_begin), wsd.timezone_begin, "GMT") as dateBegin, wsd.name as date_name, wsd.description as date_description');
			$command->from(LearningCourse::model()->tableName().' course');
			$command->join(WebinarSession::model()->tableName().' ws', 'course.idCourse = ws.course_id');
			$command->join(WebinarSessionDate::model()->tableName().' wsd', 'ws.id_session = wsd.id_session');
			$command->leftJoin(WebinarSessionUser::model()->tableName() . ' wsu', 'wsu.id_session = ws.id_session');
			$command->leftJoin(LearningCourseFieldValue::model()->tableName() ." as lcfv", "lcfv.id_course = course.idCourse");
			$whereStatement = 'course.course_type = :course_type';
			if($webinarIdsStr && $webinarIdsStr != '') {
				$whereStatement = $whereStatement . ' AND ((wsu.id_user = ' . Yii::app()->user->idst . ' AND course.idCourse IN(' . $webinarIdsStr . ')) OR course.idCourse NOT IN(' . $webinarIdsStr . '))';
			}

//			$command->group('ws.id_session');
			$command->group('wsd.id_session, wsd.day, wsd.timezone_begin');
			$command->andWhere($whereStatement);
			$command->params[':course_type'] = 'webinar';

			if(!$infinity || $getTooltip){
				$command->having('dateBegin BETWEEN :start_date AND :end_date');
				$command->params[':start_date'] =  Yii::app()->localtime->toUTC($start_date, 'Y-m-d H:i:s');
				$command->params[':end_date'] =  Yii::app()->localtime->toUTC($end_date, 'Y-m-d H:i:s');
			}

			// Exclude the courses we have to skip from showing
			if (is_array($coursesToNotDisplayList) && count($coursesToNotDisplayList)) {
				$command->andWhere(array('NOT IN', 'course.idCourse', $coursesToNotDisplayList));
			}

			//Apply text filter if needed
			if($filter != '')
			{
				$command->andWhere('((ws.name LIKE :name)  OR (course.name LIKE :name) OR (course.description LIKE :name) OR (course.code LIKE :name))');
				$command->params[':name'] = '%'.$filter.'%';
			}

			// Filter by price
			if ($fltPricingType != 'all') {
				$command->andWhere('course.selling = :selling');
				$command->params[':selling'] = ($fltPricingType == 'paid') ? 1 : 0;
			}

			// Filter by category
			if($fltCategory && $fltCategory != 'all') {
				$categories = array();
				$categories = LearningCourseCategoryTree::model()->getCategoryAndDescendants($fltCategory);
				$command->andWhere(array('IN', 'course.idCategory', $categories));
			}

			if($userCatalogs && is_array($userCatalogs) && count($userCatalogs)) {
				if(!(count($userCatalogs) == 1 && !empty($userCatalogs['all']) && (Settings::get('on_catalogue_empty', 'off') != 'off'))) {
					$courseOnCondition = 'course.status IN (:effective, :available)';
					$command->params[':effective'] = LearningCourse::$COURSE_STATUS_EFFECTIVE;
					$command->params[':available'] = LearningCourse::$COURSE_STATUS_AVAILABLE;

					$command->join(LearningCatalogueEntry::model()->tableName() . ' t', 't.type_of_entry="course" AND t.idEntry=course.idCourse AND ' . $courseOnCondition);
					// Only display courses/LPs from catalogs the user is assigned to
					$command->andWhere(array('IN', 't.idCatalogue', array_keys($userCatalogs)));
				}
			}

			// Additional fields filtering
			foreach($fltCourseFields['additional'] AS $filterItemId => $filterItem) {
				if (!empty($filterItem['type'])) {
					switch ($filterItem['type']) {
						case 'dropdown':
							if (!empty($filterItem['value'])) {
								$command->andWhere('lcfv.field_' . $filterItemId . ' = :lcfvfield_' . $filterItemId);
								$command->params[':lcfvfield_' . $filterItemId] = $filterItem['value'];
							}
							break;

						case 'date':
							if (!empty($filterItem['value'])) {
								$command->andWhere('lcfv.field_' . $filterItemId . $filterItem['condition'] . ' :lcfvfield_' . $filterItemId);
								$command->params[':lcfvfield_' . $filterItemId] = Yii::app()->localtime->fromLocalDate($filterItem['value']);
							}

							break;

						case 'textfield':
							if (!empty($filterItem['value'])) {
								switch($filterItem['condition']) {
									case 'contains':
										$command->andWhere('lcfv.field_' . $filterItemId . ' LIKE :lcfvfield_' . $filterItemId);
										$command->params[':lcfvfield_' . $filterItemId] = "%" . $filterItem['value'] ."%";
										break;

									case 'equal':
										$command->andWhere('lcfv.field_' . $filterItemId . ' = :lcfvfield_' . $filterItemId);
										$command->params[':lcfvfield_' . $filterItemId] = $filterItem['value'];
										break;

									case 'not-equal':
										$command->andWhere('lcfv.field_' . $filterItemId . ' != :lcfvfield_' . $filterItemId);
										$command->params[':lcfvfield_' . $filterItemId] = $filterItem['value'];
										break;

									default:
										break;
								}
							}

							break;

						case 'textarea':
							if (!empty($filterItem['value'])) {
								switch($filterItem['condition']) {
									case 'contains':
										$command->andWhere('lcfv.field_' . $filterItemId . ' LIKE :lcfvfield_' . $filterItemId);
										$command->params[':lcfvfield_' . $filterItemId] = "%" . $filterItem['value'] ."%";
										break;

									case 'not-contains':
										$command->andWhere("COALESCE(lcfv.field_" . $filterItemId . ", '') NOT LIKE :lcfvfield_" . $filterItemId);
										$command->params[':lcfvfield_' . $filterItemId] = "%" . $filterItem['value'] ."%";
										break;

									default:
										break;
								}
							}

							break;
						default:
							// Not an internal field type -> instantiate the custom field handler and let it process the case
							$fieldName = 'CourseField'.ucfirst($filterItem['type']);
							$sql = '';
							if(method_exists($fieldName, "applyCourseCatalogFilter")) {
								$fieldName::applyCourseCatalogFilter($filterItemId, $filterItem['value'], $sql);
							}

							if($sql != '') {
								$sql = preg_replace('/^([\s]+)?and/i', '', $sql);
								$command->andWhere($sql);
							}

							break;
					}
				}
			}

			$rows = array();
//			$command->group('ws.id_session');
			if(Yii::app()->user->isGuest){
				// show only visible to all users courses and learning paths
				$visibleCoursesIds = LearningCourse::getCoursesIdsVisibleInExternalCatalog();
				$command->andWhere(array('IN', 'course.idCourse', $visibleCoursesIds));
			}
			$rows = $command->queryAll();

			foreach($rows as &$course)
			{
				$course['day'] = date('Y-m-d', strtotime(/*Yii::app()->localtime->toLocalDate(*/$course['dateBegin']/*, LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, 'Y-m-d H:i:s')*/));

				$startTimestamp = strtotime($course['dateBegin']);
				$endTimestamp = $startTimestamp + ($course['duration_minutes'] * 60);

				if(!$getTooltip) {
					$result[] = array(
							'id' => $course['idCourse'] . '_0_' . $course['day'] . '_' . $course['id_session'] . '_0',
							'title' => mb_strlen($course['name']) > 23 ? mb_substr($course['name'], 0, 23) . '...' : $course['name'],
							'start' => Yii::app()->localtime->toLocalDateTime(date("Y-m-d H:i:s", $startTimestamp), LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, 'Y-m-d H:i:s'),
							'end' => Yii::app()->localtime->toLocalDateTime(date("Y-m-d H:i:s", $endTimestamp), LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, 'Y-m-d H:i:s'),
							'allDay' => false,
							'color' => '#8d44ad',
							'textColor' => '#ffffff'
					);
				} else {
					// add tooltip items here
					$allItems[] = array(
							'id' => $course['idCourse'] . '_0_' . $course['day'] . '_' . $course['id_session'] . '_0',
							'name' => (mb_strlen($course['session_name']) > 23 ? mb_substr($course['session_name'], 0, 23) . '...' : $course['session_name']),
							'timestamp' => strtotime(Yii::app()->localtime->toLocalDateTime($course['dateBegin'], LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, 'Y-m-d H:i:s')),
							'type'=>'webinar',
							'start' => Yii::app()->localtime->toLocalDateTime(date("Y-m-d H:i:s", $startTimestamp), LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, 'Y-m-d H:i:s'),
							'end' => Yii::app()->localtime->toLocalDateTime(date("Y-m-d H:i:s", $endTimestamp), LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, 'Y-m-d H:i:s'),
							'courseName' => $course['name'],
							'sessionDescription' => $course['session_description'],
							'dateName' => $course['date_name'],
							'dateDescription' => $course['date_description'],
					);
				}
			}
		}

		if(!$getTooltip) {
			return $result;
		} else {
			$timestamps = array();
			foreach ($allItems as $key => $item) {
				$timestamps[$key] = $item['timestamp'];
			}
			array_multisort($timestamps, SORT_ASC, $allItems);

			if($limit>0)
				$allItems = array_slice($allItems, 0, $limit, true);

			$this->renderPartial('calendar_day_items_tooltip', array(
					'items'=>$allItems,
			));
			Yii::app()->end();
		}
	}
}