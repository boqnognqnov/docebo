<?php

/**
 * Main module controller
 * Class IndexController
 */
class IndexController extends Controller {

	/**
	 * Access filters
	 * @return array
	 */
	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}

	/**
	 * Access rules for this controller
	 * @return array
	 */
	public function accessRules() {
		$res = array();

		// Allow access only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);
		return $res;
	}


}
