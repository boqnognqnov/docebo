<?php

/**
 * Class QuestionsModule
 * Main Questions module
 */
class QuestionsModule extends CWebModule
{
	
	/**
	 * Module initialization
	 */
	public function init() {

		// import the module-level models and components
		$this->setImport(array(
			'questions.components.*'
		));
	}
	
	/**
	 * @param CController $controller
	 * @param CAction $action
	 * @return bool
	 */
	public function beforeControllerAction($controller, $action) {
		if(parent::beforeControllerAction($controller, $action)) {
			return true;
		}
		else
			return false;
	}

}
