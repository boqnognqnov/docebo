<?php
/* @var $deliverableModel LearningDeliverable */
/* @var $deliverableObjects LearningDeliverableObject[] */
/* @var $deliverableUploadForm PlayerDeliverableUploadForm */
/* @var $form CActiveForm */
/* @var $showUploadForm bool */
$_uploadFormId = 'player-deliverable-upload-form_' . time();
?>
<div class="player-launchpad-html-container container">
	<div class="row-fluid player-launchpad-header">
		<div class="span12">
			<h2><?php echo $deliverableModel->title; ?></h2>
			<?php
				if (PluginManager::isPluginActive('Share7020App')) {
					$this->widget('common.widgets.CourseLoHeaderApp7020', array('loID' => $idReference));
				}
			?>
			<a id="player-inline-close" class="player-close" href="#"><?= Yii::t('standard', '_CLOSE') ?> </a>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div id="deliverable-content-<?php echo $deliverableModel->getPrimaryKey(); ?>" class="player-arena-deliverable-inline">

                <div class="row-fluid">
                    <span class="i-sprite is-poll large pull-left"></span>
                    <div class="description native-styled">
                        <strong class="text-colored"><?= Yii::t('deliverable', 'Job description') ?></strong>
                        <?php
							echo Yii::app()->htmlpurifier->purify($deliverableModel->description, 'allowFrame');
                        ?>
                    </div>
                </div>
                <br>

                <div class="row-fluid">

                    <!--upload video form-->
                    <div class="span6">
                        <h4 class="title-bold"><?= Yii::t('course', '_DOCUMENT_UPLOAD') ?></h4>

                        <div class="upload-form-container" id="upload-form-container">
                            <?php if ($showUploadForm) : ?>
                            <?php
                            $form = $this->beginWidget('CActiveForm', array(
                                'id' => $_uploadFormId,
                                'method' => 'POST',
                                'action' => $this->createUrl('axUploadDeliverable', array(
                                    'id_deliverable' => $deliverableModel->id_deliverable
                                )),
                                'htmlOptions' => array(
                                    'enctype' => 'multipart/form-data',
                                    'class' => 'docebo-form ajax player-deliverable-upload-form'
                                )
                            ));
                            ?>

                                <span class="menu-loading"></span>

                                <div class="well upload-form-result"></div>

                                <div class="well upload-form-content">

                                    <div class="error-summary"></div>

                                    <div class="clearfix" style="margin-bottom: 15px;">
                                        <?= $form->fileField($deliverableUploadForm, 'file', array(
                                                'class' => 'btn-docebo green big',
                                                'label' => Yii::t('organization', 'Upload File'),
                                                'id' => $_uploadFormId . '_browse',
                                                'name' => 'file',
												'style' => 'width:100%;height:30px;padding-top:30px;z-index:1;cursor:pointer;',
                                                'onmouseover' => 'this.style.cursor=\'hand\''
                                        )) ?>
                                        <span class="video-separator"><?= Yii::t('authoring', 'or') ?></span>
                                        <div class="deliverable-video-url">
                                            <?= $form->textField($deliverableUploadForm, 'videoUrl', array(
                                                'style' => 'width:95%;margin:0;',
	                                            'placeholder'=> Yii::t('deliverable', 'Paste here your Youtube or Vimeo link')
                                            )) ?>
                                        </div>
                                    </div>

									<div class="multiupload-container hide">
										<div class="msg"><?= Yii::t('deliverable', 'Max {count} files allowed', array('{count}'=>10)) ?></div>
										<ul></ul>
									</div>

                                    <?= CHtml::activeLabelEx($deliverableUploadForm, 'name', array(
                                        'label' => Yii::t('standard', '_NAME').' <span class="required">*</span>'
                                    )) ?>
                                    <?= $form->textField($deliverableUploadForm, 'name', array(
                                        'class' => 'input-block-level'
                                    )) ?>

                                    <?= CHtml::activeLabelEx($deliverableUploadForm, 'comments', array(
                                        'label' => Yii::t('standard', '_COMMENTS')
                                    )) ?>
                                    <?= $form->textArea($deliverableUploadForm, 'comments', array(
                                        'class' => 'input-block-level',
                                        'rows' => 4
                                    )) ?>

                                    <input class="btn-docebo green big btn-upload-video" type="submit" value="<?php echo Yii::t('standard', '_SEND'); ?>" />
                                </div>

                            <?php $this->endWidget(); ?>
                            <?php else: ?>
                            <div class="well">
                                <p> <?php

	                                // If there is no coaching activated per this course
	                                if (empty($messageReplace)) {
		                                echo Yii::t('deliverable', 'Your deliverable has been submitted');
	                                } else {
		                                echo $messageReplace;
		                                // Add extra processing for request a new coach button click
		                                if($actionProcessCoachRequest <> ''): ?>
			                                <script type="text/javascript">
				                                (function ($) {
					                                if ($('#requestCoachingSessionBtn').length > 0) {
						                                $('#requestCoachingSessionBtn').click(function () {
							                                window.location.href = "<?=$actionProcessCoachRequest ?>";
						                                });
					                                }

				                                })(jQuery);
			                                </script>
		                                <?php endif;

	                                }

	                                ?></p>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <!--my deliverables list-->
                    <div class="span6">
                        <h4 class="title-bold"><?= Yii::t('deliverable', 'My deliverables list') ?></h4>

                        <?php if (count($deliverableObjects) > 0) : ?>
                            <ul class="player-arena-deliverable-list">
                                <?php foreach ($deliverableObjects as $deliverableObject) : ?>
								<?php $fileUrl = null; ?>
                                <li class="">
                                    <?php $_isPending = ($deliverableObject->evaluation_status == LearningDeliverableObject::STATUS_PENDING); ?>
                                    <?php if ( ! $_isPending) : ?>
                                    <a href="<?= $this->createUrl('axViewEvaluation', array('id' => $deliverableObject->idObject)) ?>" class="view-evaluation open-dialog" data-dialog-class="modal-evaluate-deliverable">
                                    <?php else: ?>
									<?php $fileUrl = (count($deliverableObject->files) == 1) ? $deliverableObject->files[0] : null; ?>
                                    <div class="view-evaluation pending">
                                    <?php endif; ?>

                                        <span class="evaluation">
                                            <span class="i-sprite is-circle-check <?= $deliverableObject->getDeliverablesListStatusIconCss() ?>"></span>
                                            <?php if ($_isPending) : ?>
                                            <label class="status-waiting" for=""><?= Yii::t('classroom', 'Waiting for evaluation') ?></label>
                                            <?php else: ?>
                                            <label for=""><?= $deliverableObject->evaluation_score ?>/100</label>

                                            <span class="arrow-container">
                                                <span class="docebo-sprite ico-arrow-right"></span>
                                            </span>
                                            <?php endif; ?>
                                        </span>

                                        <span class="row-content">
                                            <span class="i-sprite large <?= $deliverableObject->getVideoIconCss() ?>"></span>
                                            <?= $deliverableObject->name ?>
											<?php if($fileUrl): ?>
											<a class="download-assignment" target="_blank" href="<?= $fileUrl ?>"><div class="i-sprite is-download"></div></a>
											<?php endif; ?>
                                        </span>
									<?php if($_isPending): ?>
                                    </div>
									<?php else: ?>
									</a>
									<?php endif; ?>
                                </li>

                                <?php endforeach; ?>
                            </ul>
                        <?php else: ?>
                            <div class="well" style="height: 185px;line-height: 185px;text-align: center;">
                                <em><?= Yii::t('deliverable', 'You haven\'t uploaded any video yet') ?></em>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    function reloadDeliverableLaunchpad() {
        // Load the launchpad
        $.ajax({
            url:        '<?= $this->createUrl('axLaunchpad') ?>',
            type:       'post',
            dataType:   'json',
            data:       {
                course_id: Arena.course_id,  // We need course_id for ALL requests
                id_object: '<?= $idReference ?>'
            },
            beforeSend: function () {
            }
        })
            .done(function (res) {
                if (res.success == true) {
                    // We expect res.data.html OR res.html
                    // res.html will be subject to global .ajaxFilter!!
                    // res.data.html - not. You decide which one to use
                    if (res.data != null && typeof (res.data.html) !== 'undefined') {
                        $('#player-arena-launchpad').html(res.data.html);
                    }
                    else {
                        $('#player-arena-launchpad').html(res.html);
                    }
                    //Arena.disableQuickNav();
                    $(document).controls();
                }
                else {
                    Docebo.Feedback.show('error', res.message);
                }
            })
            .always(function () {
                Docebo.activateAjaxFilter();
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
            });
    }
    $(function() {
        $(document).controls();

        var $form = $('#<?= $_uploadFormId ?>');

        if (!$form.length) return false;

        $form.find('input:file').styler({browseText:'<?= addslashes(Yii::t('organization', 'Upload File')) ?>' });

        $form.ajaxForm({
            dataType: 'json',
            beforeSend: function() {
                $form.find('.error-summary').html('').hide();
            },
            success: function(response) {
                if (response.success === true) {
                    $form.find('.upload-form-content').hide();
                    $form.find('.upload-form-result').html(response.message).show();
                    reloadDeliverableLaunchpad();
                } else {
                    $form.find('.error-summary').html(response.message).show();
                }
            }
        });

        // Create new Uploader object
        var DeliverableUploader = new FileUploaderClass();

		$(document).on('click', '.btn-upload-video.disabled', function(){
			return false;
		});

        // PL Uploader options
        DeliverableUploader.init({

			<?php if ($deliverableModel->allow_multiupload==1) : ?>
			multi_selection: true,
			max_file_count: 10,
			onFilesAdded: function(up, files) {
				var maxfiles = up.settings.max_file_count;
				if(up.files.length > maxfiles ) {
					up.splice(maxfiles);
				}

				var $container = $('#' + up.settings.container);
				var ul = '';
				for (var index in up.files) {
					file = up.files[index];
					if(file.name)
						ul += '<li><a data-file="'+file.id+'" class="delete-upload"></a>'+file.name+'</li>';
				}
				var $multiupload = $container.find('.multiupload-container');
				$multiupload.removeClass('hide');
				$multiupload.find('> ul').html(ul);

				$multiupload.find('button').unbind('click').bind('click', function() {
					up.start();
					return false;
				});

				$("a.delete-upload").unbind('click').bind('click', function() {
					for (var i = up.files.length - 1; i >= 0; i--) {
						if (up.files[i].id === $(this).data('file')) {
							up.splice(i, 1)[0];
							break;
						}
					}
					$(this).parent().remove();
				});

				var $btnSubmit = $form.find('.btn-upload-video');
				$btnSubmit
					.addClass('pending-upload')
					.val('<?= CHtml::encode(Yii::t('deliverable', 'Upload selected files')) ?>')
					.unbind('click').bind('click', function() {
						up.start();
						return false;
				});
			},
			<?php endif; ?>

            browse_button: '<?= $_uploadFormId . '_browse' ?>',
            container: 'upload-form-container',
            //drop_element: 'x-pl-uploader-dropzone',
            max_file_size: '400mb',  // ???????????? (@Fabio??)
            url: Arena.uploadFilesUrl+'&deliverableUpload=true',
            multipart: true,
            multipart_params: {
                course_id: '<?= intval( $this->getIdCourse() ) ?>',
                YII_CSRF_TOKEN: Arena.csrfToken
            },
            flash_swf_url: Arena.pluploadAssetsUrl + '/plupload.flash.swf',

            // -- custom settings; not PL Uploader specific; accessible inside uploaded code as pl_upload.settings.xyz
            form_selector: '#<?= $_uploadFormId ?>',
            filters: [{title: "Upload File", extensions: '<?= implode(',', array_unique(array_merge(FileTypeValidator::getWhiteListArray(FileTypeValidator::TYPE_ITEM), FileTypeValidator::getWhiteListArray(FileTypeValidator::TYPE_VIDEO)))); ?>'}],
            //runtimes: 'flash',

            onBeforeUpload: function() {
                $form.find('.menu-loading').show();
                // change label on the upload button
                var jqFileBrowse = $form.find('.jq-file__browse');
                jqFileBrowse.data('label', jqFileBrowse.text());
                jqFileBrowse.html('<?= Yii::t('standard', 'Uploading') ?>');
                // capture mouse clicks on the upload button
                var jqCaptureClick = $form.find('.jq-file > .jq-file__capture-click');
                if (!jqCaptureClick.length) {
                    $form.find('.jq-file').append('<div class="jq-file__capture-click"></div>');
                    jqCaptureClick = $form.find('.jq-file > .jq-file__capture-click');
                }
                jqCaptureClick.show().click(function(e) {
                    e.preventDefault();
                    return false;
                });
                $('#<?= CHtml::activeId($deliverableUploadForm, 'videoUrl') ?>').attr('disabled', true);
                $('.btn-upload-video').addClass('disabled');
            },
            onUploadComplete: function(files, formSelector) {
                // hide loading animation
                $form.find('.menu-loading').hide();
                // show initial label
                var jqFileBrowse = $form.find('.jq-file__browse');
                jqFileBrowse.html('<?= addslashes(Yii::t('standard', '_COMPLETED')) ?>');

				if (files.length <= 0) {
                    return;
                }
                var html = '', i=0;

                for (var index in files) {
					file = files[index];
					for (var key in file)
                    	html += '<input type="hidden" name="<?= CHtml::activeName($deliverableUploadForm, 'file') ?>['+i+'][' + key + ']" value="' + file[key] + '">\n';
					i++;
				}

                // Remove previously added file[]
                $(formSelector).find('input[name^="file["]').remove();

                $(formSelector).prepend(html);

				$btnSubmit = $form.find('.btn-upload-video');
				$btnSubmit.removeClass('disabled');
				if ($btnSubmit.hasClass('pending-upload')) {
					$btnSubmit
						.val('<?= CHtml::encode(Yii::t('standard', '_SEND')) ?>')
						.unbind('click').bind('click', function() {
							return true;
						});
				}

                return true;
            },
            onUploadCancel: function() {
                // hide loading animation
                $form.find('.menu-loading').hide();
                // show initial label
                var jqFileBrowse = $form.find('.jq-file__browse');
                jqFileBrowse.html(jqFileBrowse.data('label'));
                // remove click catcher
                var jqCaptureClick = $form.find('.jq-file > .jq-file__capture-click');
                if (jqCaptureClick.length) {
                    jqCaptureClick.hide();
                }
                $('#<?= CHtml::activeId($deliverableUploadForm, 'videoUrl') ?>').attr('disabled', false);
            }
        });

        // Run uploader
        DeliverableUploader.run();
    });
</script>