<?php
$this->widget('common.extensions.jplayer.jPlayerWidget', array(
    'mediaUrl' => $deliverableModel->getUploadedVideoUrl(),
    'videoId' => $deliverableModel->id_deliverable,
    'title' => $deliverableModel->name,
    'layout' => 'dialog2_videoplayer',
    'skin' => 'dialog2_videoplayer',
));