<?php
/* @var $deliverableModel LearningDeliverableObject */
/* @var $form CActiveForm */
/* @var $userModel CoreUser */
/* @var $evaluationForm DeliverableEvaluationForm */
?>

<h1><?= Yii::t('deliverable', 'Your file evaluation') ?></h1>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'deliverable-evaluation-form_' . time(),
    'method' => 'POST',
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
        'class' => 'docebo-form ajax'
    )
));
?>
    <?= $form->hiddenField($evaluationForm, 'status') ?>
    <input type="text" style="display: none;"/>

    <div class="media-container">
        <?php
        $isPlainLink = FALSE;
        switch($deliverableModel->type){
	        case 'upload':
		        $extension = strtolower(CFileHelper::getExtension($deliverableModel->content));
				if($extension=='mp4'){
			        echo $this->renderPartial('_jplayer', array(
				        'deliverableModel' => $deliverableModel
			        ), true, true); // post-process!
				}else{
					// The uploaded file is not a video, so just show
					// a link to download it
					$isPlainLink = TRUE;
				}
		        break;
	        case 'link':
				$embedEnabledForVideo = PlayerDeliverableUploadForm::isEmbedUrlWhitelisted($deliverableModel->content);

				if($embedEnabledForVideo){
		            echo $deliverableModel->loadVideo();
				}else{
					$isPlainLink = TRUE;
					// The 'content' attribute in this case is a URL, not a file name
					$fileUrl = $deliverableModel->content;
				}
		        break;
        }
        ?>

	    <? if($isPlainLink): ?>
		    <div class="deliverable-external-link">
			    <!--<div class="pull-right">
				    <a target="_blank" href="<?/*=$fileUrl*/?>"><div class="i-sprite is-moveto"></div></a>
			    </div>-->
			    <h6 class="text-left"><?=Yii::t('deliverable', 'Submitted file')?>:</h6>
		    </div>

			<?php $_fileIndex = 1; ?>
			<?php foreach ($deliverableModel->files as $fileName => $fileUrl) : ?>
				<div class="player-grey-box text-left deliverable-download">
					<div class="deliverable-name">
						<div class="pull-right">
							<a target="_blank" href="<?= $fileUrl ?>"><div class="i-sprite is-download"></div></a>
						</div>
						<span class="i-sprite large <?= $deliverableModel->getVideoIconCss() ?>"></span>
						<?= Yii::t('standard', '_FILE') .' '.($_fileIndex++) ?>
					</div>
				</div>
			<? endforeach; ?>
			<br/>
	    <? endif; ?>
    </div>

    <div class="media-author-container row-fluid">
        <div class="span6">
	        <?php  if($userModel):?>
            <h6><?= Yii::t('deliverable', 'Evaluated by') ?></h6>
            <div class="media-author">
                <div class="media-author-image">
                    <?php echo CHtml::image($userModel->getAvatarImage(true), $userModel->firstname . " " . $userModel->lastname, array('width' => 50) ); ?>
                </div>
                <h5 class="text-colored media-author-name"><?= $userModel->firstname . " " . $userModel->lastname ?></h5>
                <div class="media-author-info">
                    <strong><?= $userModel->email ?></strong>
                    <br>
                    <?= Yii::t('classroom', 'Evaluated on') . ' ' . $deliverableModel->evaluation_date ?>
                </div>
            </div>
	        <?php endif;?>
        </div>
        <div class="span6">
            <h6><?= Yii::t('deliverable', 'Your score and status') ?></h6>
            <div class="score-box status-<?= ($deliverableModel->evaluation_status == LearningDeliverableObject::STATUS_ACCEPTED ? 'passed' : 'rejected') ?>">
                <span class="status-icon pull-right"><span class="i-sprite <?= ($deliverableModel->evaluation_status == LearningDeliverableObject::STATUS_ACCEPTED ? 'is-check green' : 'is-remove red') ?> large"></span></span>
                <div class="score"><span class="score-value"><?= $deliverableModel->evaluation_score ?></span><span class="sep">/</span>100</div>
                <h4 class="score-status">
                    <?= ($deliverableModel->evaluation_status == LearningDeliverableObject::STATUS_ACCEPTED
                        ? Yii::t('standard', 'passed')
                        : Yii::t('standard', 'failed'))
                    ?>
                </h4>
            </div>
        </div>
    </div>

    <div class="instructor-section">
        <h6><?= Yii::t('deliverable', 'Instructor\'s comments') ?></h6>
        <br>
        <div class="instructor-comments">
            <?= $deliverableModel->evaluation_comments ?>
        </div>
        <div class="reply-holder" style="<?=$deliverableModel->getUserReplyToInstructorFeedback() ? '' : 'display:none;'?>">
            <br/>
            <h6><?= Yii::t('deliverable', "User's reply to comment") ?></h6>
            <br/>
            <p>
                <?=$deliverableModel->getUserReplyToInstructorFeedback()?>
            </p>
        </div>

        <? if(!$deliverableModel->getUserReplyToInstructorFeedback() && ($deliverableModel->evaluation_status == LearningDeliverableObject::STATUS_ACCEPTED || $deliverableModel->evaluation_status == LearningDeliverableObject::STATUS_REJECTED)): ?>
            <div class="reply-to-instructor-form">
                <hr/>
                <p><?=Yii::t('deliverable', "Reply to instructor's comment")?>:</p>
                <textarea class="user-reply-text" style="width: 100%; padding:0" rows="3"></textarea>
                <a class="btn-docebo black big user-reply-to-instructor"><?=Yii::t('standard', 'Reply')?></a>

                <hr/>

                <script type="text/javascript">
                    $(function(){
                        $('a.user-reply-to-instructor').click(function(){
                            var userReplyText = $('.user-reply-text').val();

                            $.post('<?=Docebo::createLmsUrl('deliverable/default/replyToInstructorComment')?>', {
                                reply: userReplyText,
                                id: <?=$deliverableModel->idObject?>
                            }, function(data){
                                if(data.success){
                                    $('.reply-holder').show().find('p').html(data.reply);
                                    $('.reply-to-instructor-form').remove();
                                }
                            });
                        });
                    })
                </script>
            </div>
        <? endif; ?>

        <?php if ($deliverableModel->evaluation_file) : ?>
        <h6><?= Yii::t('standard', 'Attached files') ?></h6>
        <br>
        <div class="attached-file">
            <span class="i-sprite is-file black"></span>
            <a href="<?= $deliverableModel->getEvaluationAttachmentUrl() ?>" target="_blank"><?= $deliverableModel->evaluation_file ?></a>
        </div>
        <?php endif; ?>

    </div>

<div class="form-actions">
    <input type="reset" class="btn-docebo black big close-dialog" value="<?= Yii::t('standard', '_CLOSE') ?>" />
</div>

<?php $this->endWidget(); ?>


<script type="text/javascript">

    (function() {

        $(document).delegate(".modal-evaluate-deliverable", "dialog2.content-update", function() {
            var e = $(this);

            var autoclose = e.find("a.auto-close");

            if (autoclose.length > 0) {
                // reload locations grid
                $("#player-reports-deliverables-grid").yiiGridView('update');

                e.find('.modal-body').dialog2("close");

                var href = autoclose.attr('href');
                if (href) {
                    window.location.href = href;
                }
            }
            else {

                e.find('.modal-body').find(':file,:checkbox').styler({browseText:'<?=Yii::t('standard', '_UPLOAD')?>'});

                e.find('.form-actions > a').click(function(e) {
                    e.preventDefault();
                    // set status
                    var status = $(this).hasClass('evaluation-passed')
                        ? '<?= LearningDeliverableObject::STATUS_ACCEPTED ?>'
                        : '<?= LearningDeliverableObject::STATUS_REJECTED ?>';

                    var $form = $('.modal-evaluate-deliverable form');
                    $form.find('#<?= CHtml::activeId($evaluationForm, 'status') ?>').val(status);

                    $form.submit();
                });

            }
        });
    })();
</script>