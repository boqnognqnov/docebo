<div id="player-arena-deliverable-editor-panel" class="player-arena-editor-panel">
	<div class="header"><?php echo Yii::t('deliverable', '_LONAME_deliverable'); ?></div>
	<div class="content">
		<?php
		/* @var $deliverableObj LearningDeliverable */
		/* @var $form CActiveForm */
		?>

		<?php
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'deliverable-editor-form',
			'method' => 'post',
			'action' => $this->createUrl("/deliverable/default/axSaveLo"),
			'htmlOptions' => array(
				'class' => 'form-horizontal',
				'enctype' => 'multipart/form-data',
			)
		));
		?>

		<?= $form->hiddenField($deliverableObj, 'id_deliverable'); ?>

		<div id="player-arena-uploader">
			<div>
				<div class="tabbable"> <!-- Only required for left/right tabs -->
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab1" data-toggle="tab"><?=LearningOrganization::objectTypeValue($lo_type)?></a></li>
						<li><a href="#lo-additional-information" data-toggle="tab"><?=Yii::t('myactivities', 'Additional info')?></a></li>

						<?php
						// Raise event to let plugins add new tabs
						Yii::app()->event->raise('RenderEditLoTabs', new DEvent($this, array(
							'courseModel' => $courseModel,
							'loModel' => $loModel
						)));
						?>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab1">
							<div class="player-arena-uploader">

								<div class="control-group">
									<?= $form->labelEx($deliverableObj, 'title', array(
										'class' => 'control-label',
										'label' => Yii::t('standard', '_TITLE')
									)); ?>
									<div class="controls">
										<?= $form->textField($deliverableObj, 'title', array('class' => 'input-block-level')) ?>
									</div>
								</div>
								<div class="control-group">
									<?= $form->labelEx($deliverableObj, 'description', array(
										'class' => 'control-label',
										'label' => Yii::t('standard', '_DESCRIPTION')
									)); ?>
									<div class="controls">
										<?= $form->textArea($deliverableObj, 'description', array('id' => 'player-arena-deliverable-editor-textarea')); ?>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label"></label>
									<div class="controls">
										<?= $form->labelEx($deliverableObj, 'allow_multiupload', array(
											'class' => 'checkbox',
											'label' => $form->checkBox($deliverableObj, 'allow_multiupload', array(
												'uncheckValue' => null
											)) . ' ' . Yii::t('deliverable', 'Allow multiple files to be uploaded')
										)) ?>
									</div>
								</div>
								<div class="control-group">
									<?= $form->labelEx($deliverableObj, 'learner_policy', array(
										'class' => 'control-label',
										'label' => Yii::t('deliverable', 'Learner policy')
									)) ?>
									<div class="controls">
										<label class="radio">
											<?php echo $form->radioButton($deliverableObj, 'learner_policy', array('value' => LearningDeliverable::NAVRULE_PASSED, 'uncheckValue' => NULL)); ?>
											<?php echo Yii::t('deliverable', 'Learner can progress when assignment has been evaluated with a passing score'); ?>
										</label>
										<label class="radio">
											<?php echo $form->radioButton($deliverableObj, 'learner_policy', array('value' => LearningDeliverable::NAVRULE_SUBMITTED, 'uncheckValue' => NULL)); ?>
											<?php echo Yii::t('deliverable', 'Learner can progress when assignment is submitted'); ?>
										</label>
									</div>
								</div>
								<br>

							</div>
						</div>
						<div class="tab-pane" id="lo-additional-information">
							<? $this->widget('common.widgets.PlayerLayoutAdditionalInfo', array(
								'loModel'=> $loModel,
								'idCourse' => $courseModel->idCourse,
							)); ?>
						</div>

						<?php
						// Raise event to let plugins add new tab contents
						Yii::app()->event->raise('RenderEditLoTabsContent', new DEvent($this, array(
							'courseModel' => $courseModel,
							'loModel' => $loModel
						)));
						?>

					</div>
				</div>

			</div>

		</div>

		<div class="text-right">
			<input type="submit" name="confirm_save_deliverable" class="btn-docebo green big"
				   value="<?php echo Yii::t('standard', '_SAVE'); ?>">&nbsp;&nbsp;
			<a id="cancel_save_deliverable"
			   class="btn-docebo black big"><?php echo Yii::t('standard', '_CANCEL'); ?></a>
		</div>

		<?php $this->endWidget(); ?>

	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
	    $('#<?= CHtml::activeId($deliverableObj, 'allow_multiupload') ?>').styler({});
		$('.player-arena-uploader :radio').styler({});
	});
</script>