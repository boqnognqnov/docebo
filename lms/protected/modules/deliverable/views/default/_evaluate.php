<?php
/* @var $deliverableModel LearningDeliverableObject */
/* @var $form CActiveForm */
/* @var $userModel CoreUser */
/* @var $evaluationForm DeliverableEvaluationForm */
/* @var $isReadOnly bool */
?>

<h1><?= ($isReadOnly) ? Yii::t('deliverable', 'View video evaluation') : Yii::t('deliverable', 'Evaluate a file') ?></h1>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'deliverable-evaluation-form_' . time(),
    'method' => 'POST',
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
        'class' => 'docebo-form ajax'
    )
));
?>
    <?= $form->hiddenField($evaluationForm, 'status') ?>
    <input type="text" style="display: none;"/>

    <div class="media-container">
        <?php
        $isPlainLink = FALSE;
        switch($deliverableModel->type){
	        case 'upload':
		        $extension = strtolower(CFileHelper::getExtension($deliverableModel->content));
		        if($extension=='mp4'){
			        echo $this->renderPartial('_jplayer', array(
				        'deliverableModel' => $deliverableModel
			        ), true, true); // post-process!
		        }else{
			        // The uploaded file is not a video, so just show
			        // a link to download it
			        $isPlainLink = TRUE;
		        }
		        break;
	        case 'link':
		        $embedEnabledForVideo = PlayerDeliverableUploadForm::isEmbedUrlWhitelisted($deliverableModel->content);

		        if($embedEnabledForVideo){
			        echo $deliverableModel->loadVideo();
		        }else{
			        $isPlainLink = TRUE;
			        // The 'content' attribute in this case is a URL, not a file name
			        $fileUrl = $deliverableModel->content;
		        }
		        break;
        }
        ?>

	    <? if($isPlainLink): ?>
		    <div class="deliverable-external-link">
			    <!--<div class="pull-right">
				    <a target="_blank" href="<?/*=$fileUrl*/?>"><div class="i-sprite is-moveto"></div></a>
			    </div>-->
			    <h6 class="text-left"><?=Yii::t('deliverable', 'Submitted file')?>:</h6>
		    </div>

			<?php $_fileIndex = 1; ?>
			<?php foreach ($deliverableModel->files as $fileName => $fileUrl) : ?>
		    <div class="player-grey-box text-left deliverable-download">
			    <div class="deliverable-name">
				    <div class="pull-right">
					    <a target="_blank" href="<?= $fileUrl ?>"><div class="i-sprite is-download"></div></a>
				    </div>
				    <span class="i-sprite large <?= $deliverableModel->getVideoIconCss() ?>"></span>
				    <?= Yii::t('standard', '_FILE') .' '.($_fileIndex++) ?>
			    </div>
		    </div>
	    	<? endforeach; ?>
			<br/>
	    <? endif; ?>
    </div>

    <div class="media-author-container row-fluid">
        <div class="span6">
            <h6><?= Yii::t('deliverable', 'Video submitted by') ?></h6>
            <div class="media-author">
                <div class="media-author-image">
                    <?php echo CHtml::image($userModel->getAvatarImage(true), $userModel->firstname . " " . $userModel->lastname, array('width' => 50) ); ?>
                </div>
                <h5 class="text-colored media-author-name"><?= $userModel->firstname . " " . $userModel->lastname ?></h5>
                <div class="media-author-info">
                    <strong><?= $userModel->email ?></strong>
                    <br>
                    <?= Yii::t('deliverable', 'Submitted on') . ' ' . $deliverableModel->created ?>
                </div>
            </div>
        </div>
        <div class="span6">
            <h6><?= Yii::t('deliverable', 'User comments') ?></h6>
            <div class="comments">
                <?= $deliverableModel->student_comments ?>
            </div>
        </div>
    </div>

    <div class="instructor-section">
        <div class="row-fluid">
            <div class="span8">
                <?= $form->label($evaluationForm, 'comments', array(
                    'label' => Yii::t('standard', '_COMMENTS')
                )) ?>

                <?php if ($isReadOnly) : ?>
                    <div class="instructor-comments">
                        <?= $deliverableModel->evaluation_comments ?>
                    </div>
                    <? if($deliverableModel->getUserReplyToInstructorFeedback() && $deliverableModel->evaluation_status == LearningDeliverableObject::STATUS_ACCEPTED || $deliverableModel->evaluation_status== LearningDeliverableObject::STATUS_REJECTED): ?>
                        <br/>
                        <label><?=Yii::t('deliverable', "Reply to instructor's comment")?>:</label>
                        <div>
                            <?=$deliverableModel->getUserReplyToInstructorFeedback()?>
                        </div>
                    <? endif; ?>
                <?php else : ?>
                    <?= $form->textArea($evaluationForm, 'comments', array(
                        'rows' => 4,
                        'class' => 'input-block-level'
                    )) ?>
                <?php endif; ?>
            </div>
            <div class="span4">
                <?= $form->labelEx($evaluationForm, 'score', array(
                    'label' => Yii::t('standard', '_SCORE')
                )) ?>
                <div>
                    <?php if ($isReadOnly) : ?>
                    <strong><?= $deliverableModel->evaluation_score ?>/100</strong>
                    <?php else: ?>
                    <?= $form->textField($evaluationForm, 'score', array('class' => 'score-input')) ?>
                    <strong>/100</strong>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <?php if ($isReadOnly) : ?>
            <?php if ($deliverableModel->evaluation_file): ?>
            <div class="attached-file">
                <span class="i-sprite is-file black"></span>
                <a href="<?= $deliverableModel->getEvaluationAttachmentUrl() ?>" target="_blank"><?= $deliverableModel->evaluation_file ?></a>
            </div>
            <?php endif; ?>
        <?php else: ?>
            <div class="upload-container">
                <?= $form->label($evaluationForm, 'file', array(
                    'label' => Yii::t('standard', '_ATTACHMENT'),
                )) ?>
                <?= $form->fileField($evaluationForm, 'file') ?>
            </div>
        <?php endif; ?>

        <?= $form->label($evaluationForm, 'allowReupload', array(
            'label' => $form->checkBox($evaluationForm, 'allowReupload', array('disabled'=>$isReadOnly)) . ' ' . Yii::t('deliverable', 'Allow the user to re-upload a video if marked as FAILED'),
            'class' => 'checkbox'
        )) ?>
    </div>

<div class="form-actions">
    <?php if ($isReadOnly) : ?>
    <input type="reset" class="btn-docebo black big close-dialog" value="<?= Yii::t('standard', '_CLOSE') ?>" />
    <?php else: ?>
    <a href="#" class="btn btn-docebo big evaluation-passed"><?= Yii::t('deliverable', 'Mark as passed') ?></a>
    <a href="#" class="btn btn-docebo big evaluation-failed"><?= Yii::t('deliverable', 'Mark as failed') ?></a>
    <input type="reset" class="btn-docebo black big close-dialog" value="<?= Yii::t('standard', '_CANCEL') ?>" />
    <?php endif; ?>
</div>

<?php $this->endWidget(); ?>


<script type="text/javascript">

    (function() {

        $(document).delegate(".modal-evaluate-deliverable", "dialog2.content-update", function() {
            var e = $(this);

            var autoclose = e.find("a.auto-close");

            if (autoclose.length > 0) {
                // reload report grids
                $("#player-reports-deliverables-grid").yiiGridView('update');
                $('#player-reports').yiiGridView('update');
                $('#player-objects').yiiGridView('update');

                e.find('.modal-body').dialog2("close");

                var href = autoclose.attr('href');
                if (href) {
                    window.location.href = href;
                }
            }
            else {

                e.find('.modal-body').find(':file,:checkbox').styler({browseText:'<?=Yii::t('standard', '_UPLOAD')?>'});

                e.find('.form-actions > a').click(function(e) {
                    e.preventDefault();
                    // set status
                    var status = $(this).hasClass('evaluation-passed')
                        ? '<?= LearningDeliverableObject::STATUS_ACCEPTED ?>'
                        : '<?= LearningDeliverableObject::STATUS_REJECTED ?>';

                    var $form = $('.modal-evaluate-deliverable form');
                    $form.find('#<?= CHtml::activeId($evaluationForm, 'status') ?>').val(status);

                    $form.submit();

					e.find('.modal-body').dialog2("close");
                });

            }
        });
    })();
</script>