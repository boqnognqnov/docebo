<?php

class DefaultController extends DeliverableBaseController
{


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		return array(
			array(
				'deny',
				'expression' => '(!Yii::app()->user->id || Yii::app()->user->getIsGuest())',
				'actions' => array('AxEvaluate'),
				'deniedCallback' => array($this, 'adminHomepageRedirect')
			)
		);
	}

	public function adminHomepageRedirect($rule = false) {
		parent::accessDeniedCallback($rule);
	}

	public function actionIndex()
	{
		$this->render('index');
	}


	/**
	 * Save deliverable content
	 * <root>/files/upload_tmp folder.
	 *
	 */
	public function actionAxSaveLo() {

		$courseId = Yii::app()->request->getParam("course_id", null);
		$parentId = Yii::app()->request->getParam("parent_id", null);

		$thumb = Yii::app()->request->getParam("thumb", null);
		$shortDescription = Yii::app()->htmlpurifier->purify(urldecode(Yii::app()->request->getParam("short_description", '')));

		$response = new AjaxResult();
		$transaction = Yii::app()->db->beginTransaction();

		try {
			if (isset($_POST['LearningDeliverable'])) {
				$postData = $_POST['LearningDeliverable'];
				$postData['title'] = trim($postData['title']);
				$postData['description'] = Yii::app()->htmlpurifier->purify($postData['description'], 'allowFrame');
				$postData['allow_multiupload'] = empty($postData['allow_multiupload']) ? 0 : 1;
				$postData['learner_policy'] = empty($postData['learner_policy']) ? 0 : 1;
			}

			// Validate input parameters
			if (!$courseId) { throw new CException("Invalid specified course."); }
			if (!is_numeric($parentId) || $parentId < 0) { $parentId = 0; }
			if (empty($postData['title'])) { throw new CException(Yii::t('error', 'Enter a title!')); }


			$learningObject = null;

			// edit operation
			if (intval($postData['id_deliverable']) > 0) {
				// get deliverable object
				$deliverableObj = LearningDeliverable::model()->findByPk($postData['id_deliverable']);
			} else {
				$deliverableObj = new LearningDeliverable();
			}

			//prepare AR object to be saved
			$deliverableObj->attributes = $postData;

			//these will be needed by learning organization table
			$deliverableObj->setIdCourse($courseId);
			$deliverableObj->setIdParent($parentId);

			//do actual saving
			if (!$deliverableObj->save()) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			//verify inserted data
			$resourceId = (int)$deliverableObj->getPrimaryKey();
			$learningObject = LearningOrganization::model()->findByAttributes(array('idResource' => $resourceId, 'objectType' => LearningOrganization::OBJECT_TYPE_DELIVERABLE));

			if (!$learningObject) { throw new CException("Invalid learning object(".$resourceId.")"); }

			//update LO
			$learningObject->title = $deliverableObj->title;

			if($thumb){
				$learningObject->resource = intval($thumb);
			}elseif($learningObject->resource){
				$learningObject->resource = 0;
			}

//			if($shortDescription)
				$learningObject->short_description = $shortDescription;

			$learningObject->saveNode();

			// DB Commit
			$transaction->commit();

			// Trigger event to let plugins save their own data from POST
			Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
				'loModel' => $learningObject
			)));

			$data = array(
					'resourceId' => $resourceId,
					'organizationId' => (int)$learningObject->getPrimaryKey(),
					'parentId' => (int)$parentId,
					'title' => $deliverableObj->title,
					'message' => Yii::t('standard', '_OPERATION_SUCCESSFUL')
			);
			$response->setData($data)->setStatus(true);

		} catch (CException $e) {
			$transaction->rollback();
			$response->setMessage($e->getMessage())->setStatus(false);
		}

		$response->toJSON();
		Yii::app()->end();
	}



	public function actionAxLaunchpad() {

		$response = new AjaxResult();
		$transaction = Yii::app()->db->beginTransaction();

		try {

			//load LO info from learning_organization table to get idResource of the page
			$idObject = (int)Yii::app()->request->getParam('id_object', 0);
			$object = LearningOrganization::model()->findByPk($idObject);
			if (!$object || $object->objectType != LearningOrganization::OBJECT_TYPE_DELIVERABLE) {
				throw new CException('Invalid object ID: '.$idObject);
			}

			//now load html page specific information
			$idDeliverable = (int)$object->idResource;

			$deliverableObj = LearningDeliverable::model()->findByPk($idDeliverable);
			if (!$deliverableObj) {
				throw new CException('Invalid deliverable ID: '.$idDeliverable);
			}

            $deliverableUploadForm = new PlayerDeliverableUploadForm();
            $deliverableObjects = $deliverableObj->getUserDeliverableObjects(Yii::app()->user->id);

            $showUploadForm = true;
            $countDelivered = count($deliverableObjects);
            if ($countDelivered > 0) {
                // the delivered objects are ordered by date DESC
                /* @var $lastDeliveredVideo LearningDeliverableObject */
                $lastDeliveredVideo = $deliverableObjects[0];

                // hide the upload form if the evaluation for the latest video is pending
                // or if the video was evaluated as 'failed' and new upload is not allowed
	            $showUploadForm &= ($lastDeliveredVideo->evaluation_status!=LearningDeliverableObject::STATUS_ACCEPTED && $lastDeliveredVideo->evaluation_status!=LearningDeliverableObject::STATUS_PENDING);
	            $showUploadForm &= $lastDeliveredVideo->evaluation_status!=LearningDeliverableObject::STATUS_REJECTED || ($lastDeliveredVideo->evaluation_status==LearningDeliverableObject::STATUS_REJECTED && $lastDeliveredVideo->allow_another_upload);
            }

			// Start Course Coaching related operations
			// Skip default message
			$skipDefaultMessage = false;
			$messageReplace = ''; // a message to replace default message

			// get course id first
			$idCourse = $this->getIdCourse();

			// Then get the course model
			$courseModel = LearningCourse::model()->findByPk($idCourse);

			// Get user id too
			$userId = Yii::app()->user->id;

			// If course coaching is enabled start further processing
			if ($courseModel->enable_coaching) {
				// Check is there any active coaching session for this user
				$userActiveCoachingSessions = LearningCourseCoachingSessionUser::model()->getUserActiveCoachingSessions($userId, $idCourse);

				// If it is allowed a user to start the course even if it's coaching session is not started
				if ($courseModel->coaching_option_allow_start_course) {
					// Get not started coaching sessions
					$userNotStartedCoachingSessions = LearningCourseCoachingSessionUser::model()->getUserNotStartedCoachingSessions($userId, $idCourse);
					$userActiveCoachingSessions = (count($userActiveCoachingSessions) > 0) ? $userActiveCoachingSessions : $userNotStartedCoachingSessions;
				}

				if (count($userActiveCoachingSessions) > 0) {
					$userActiveCoachingSessions = current($userActiveCoachingSessions);

					if($userActiveCoachingSessions != false) {
						$coachId                  = $userActiveCoachingSessions['idCoach'];

						$coachingSessionDateStart = $userActiveCoachingSessions['datetime_start'];
						$coachingSessionDateStart = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($coachingSessionDateStart));

						$coachingSessionDateEnd   = $userActiveCoachingSessions['datetime_end'];
						$coachingSessionDateEnd =  Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($coachingSessionDateEnd));

						$coachModel = CoreUser::model()->findByPk($coachId);
						$avatarCoach = $coachModel->getAvatarImage(false, '', 'width: 50px;');
						$coachFirstName = $coachModel->firstname;
						$coachLastName = $coachModel->lastname;

						// Start extra coaching session stuff
						$skipDefaultMessage = true;
						$messageReplace = '<div  class="row-fluid">
														<div class="span1 green-text success">
			                                                <i class="fa fa-check fa-2x"></i></div><div class="span11"><span class="green-text success coaching-lo-deliverable-success"> '
							. Yii::t('deliverable', 'Your deliverable has been submitted') . '</span><br />'
							. Yii::t('deliverable', 'It will be evaluated during your coaching period by your coach:')
							. '<div class="row-fluid coaching-lo-deliverable-avatar"><div class="span2">' . $avatarCoach . '</div>'
							. '<div class="span9 coaching-lo-deliverable-coachbox">'
							. '<div class="row-fluid" style=""><div class="span12 blue-text coaching-lo-deliverable-coachnames">' . $coachFirstName . '&nbsp;' . $coachLastName . '</div></div>'
							. '<div class="row-fluid" style=""><div class="span12">'
							. Yii::t('course', 'Your coaching period:') . ' ' . Yii::t('standard', '_FROM') . ' <b>'
							. $coachingSessionDateStart . '</b> ' . Yii::t('standard', '_TO')  . ' <b>' .  $coachingSessionDateEnd
							. '</b></div></div>'
							. '</div></div></div>
													</div>';
					}
				}

				// Default value for the request a new coach action url
				$actionProcessCoachRequest = '';
				// Check is there any expired coaching sessions
				if ($showUploadForm) {
					// NOTE: if the user is here - there are two options(note, that course coaching is enabled)
					// Option 1 - there is an active user coaching session - we don't need to modify nothing here in this case
					// Option 2 - user has expired coaching session and can play course, but should not be able to submit
					// any assignments and we need to bypass this case and show a message instead of the form !!!
					// There shouldn't  be  any other options

					// Check is there any user coaching sessions
					$userCoachingSessions = LearningCourseCoachingSession::model()->userAssignedSessions($userId, true, $idCourse);

					// Verify, that there is a session
					if(count($userCoachingSessions) > 0) {
						// Indicator for expired user coaching session, by default is false(there is no expired user coaching session)
						$coachingSessionExpired = false;

						// Get the user local date now(today):
						$dateToday = Yii::app()->localtime->toLocalDate();
						$dateToday = DateTime::createFromFormat(Yii::app()->localtime->getPHPLocalDateFormat('short'), $dateToday);

						foreach($userCoachingSessions as $coachingSession) {
							// Prepare date start
							$coachingSessionDateStart = $coachingSession->datetime_start;
							$coachingSessionDateStart = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($coachingSessionDateStart));
							$tempStartDate = DateTime::createFromFormat(Yii::app()->localtime->getPHPLocalDateFormat('short'), $coachingSessionDateStart);

							// Prepare date end
							$coachingSessionEndDate = $coachingSession->datetime_end;
							$coachingSessionEndDate = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($coachingSessionEndDate));
							$tempEndDate = DateTime::createFromFormat(Yii::app()->localtime->getPHPLocalDateFormat('short'), $coachingSessionEndDate);

							// Is the user coaching session expired
							if ($tempEndDate < $dateToday) {
								$coachingSessionExpired = true;
							} else {
								// The user coaching session is not expired yet
								$coachingSessionExpired = false;
								// NOTE: this is set to prevent showing an expired user coaching session if there are more,
								// then one user coaching session in the db(expired and started or not yet started too)
							}
						}

						if ($coachingSessionExpired) {
							$actionProcessCoachRequest = Docebo::createLmsUrl('player/training/userRequestedNewCoachingSession&course_id='
								. $idCourse . '&user_id=' . $userId . '&renew=1');

							// Get a button code
							$requestCoachBtn = '<input id="requestCoachingSessionBtn" class="btn btn-docebo blue big center" name="requestCoachingSessionBtn" type="button" value="' . Yii::t('course', 'REQUEST ANOTHER COACH') . '" />';

							// If there is an expired user coaching session
							$showUploadForm = false;
							$messageReplace = '<div  class="row-fluid">
							<div class="span1 red-text error"><i class="fa fa-ban fa-2x"></i>' .
								'</div><div class="span11 coaching-lo-deliverable-coachbox"><span class="red-text error coaching-lo-deliverable-error">' .
								Yii::t('course', 'You cannot upload any assignment') . '</span><br />' .
								Yii::t('course', 'Your coaching session') . ' (' . Yii::t('standard', '_FROM') . ' <b>' . $coachingSessionDateStart . '</b> ' .
								Yii::t('standard', '_TO') . ' <b>' . $coachingSessionEndDate . '</b>) <span class="red-text error">' . Yii::t('course', 'has expired') . '</span><br /><br /> ' .
								$requestCoachBtn .
								'</div>' .
							'</div>'
							;
						} // Endif we need to bypass showing the upload form and replace it with expired session message

					} // Endif there are user coaching sessions
				} // Endif it is set to display the upload form
			} // Endif course coaching is enabled

			//load content
			$html = $this->renderPartial("_launchpad", array(
				'deliverableModel'          => $deliverableObj,
				'deliverableObjects'        => $deliverableObjects,
				'idReference'               => $idObject,
                'showUploadForm'            => $showUploadForm,
                'deliverableUploadForm'     => $deliverableUploadForm,
                'skipDefaultMessage'        => $skipDefaultMessage,
                'idCourse'                  => $idCourse,
				'courseModel'               => $courseModel,
				'messageReplace'            => $messageReplace,
				'actionProcessCoachRequest' => $actionProcessCoachRequest
			), true);

			//finish action
			$transaction->commit();

			$response->setHtml($html);
			$response->setStatus(true);

		} catch (CException $e) {

			$transaction->rollback();
			$response->setMessage($e->getMessage());
			$response->setStatus(false);
		}

		//send result
		$response->toJSON();
	}


    public function actionAxUploadDeliverable() {

        $response = new AjaxResult();
        $response->setStatus(false);

        if (Yii::app()->request->isAjaxRequest) {

            $idDeliverable = Yii::app()->request->getParam('id_deliverable');
			/** @var LearningDeliverable $deliverableObj */
            $deliverableObj = LearningDeliverable::model()->findByPk($idDeliverable);

            if (!$deliverableObj) {
                $response->setMessage("Invalid deliverable object.")->setStatus(false);
            }
            else {
                $deliverableDataObj = new LearningDeliverableObject();
                $deliverableDataObj->id_deliverable = $deliverableObj->id_deliverable;
                $deliverableDataObj->id_user = Yii::app()->user->id;

                if (isset($_POST['PlayerDeliverableUploadForm'])) {

                    $uploadForm = new PlayerDeliverableUploadForm();
                    $uploadForm->attributes = $_POST['PlayerDeliverableUploadForm'];
                    $uploadForm->deliverableObject = $deliverableDataObj;

                    if (!$uploadForm->save()) {
                        $errors = array();
                        foreach ($uploadForm->getErrors() as $field => $fieldErrors) {
                            $errors = array_merge($errors, $fieldErrors);
                        }
                        $htmlError = '';
                        foreach ($errors as $error) {
                            $htmlError .= CHtml::label($error, '', array('class'=>'error'));
                        }
                        $response->setMessage($htmlError);
                        $response->setData(array())->setStatus(false);
                    }
                    else {

						// raise event for plugins
						Yii::app()->event->raise('CourseDeliverableSubmitted', new DEvent($this, array(
							'deliverableObject' => $uploadForm->deliverableObject,
							'courseId' => Yii::app()->request->getParam("course_id", null)
						)));

	                    // Start Course Coaching related operations
	                    // Skip default message
	                    $message = Yii::t('deliverable', 'Your deliverable has been submitted'); // Default message

	                    // get course id first
	                    $idCourse = $this->getIdCourse();

	                    // Then get the course model
	                    $courseModel = LearningCourse::model()->findByPk($idCourse);

	                    // Get user id too
	                    $userId = Yii::app()->user->id;

	                    // If course coaching is enabled start further processing
	                    if ($courseModel->enable_coaching) {
		                    // Check is there any active coaching session for this user
		                    $userActiveCoachingSessions = LearningCourseCoachingSessionUser::model()->getUserActiveCoachingSessions($userId, $idCourse);

		                    if (count($userActiveCoachingSessions) > 0) {
			                    $userActiveCoachingSessions = current($userActiveCoachingSessions);

			                    if($userActiveCoachingSessions != false) {
				                    $coachId                  = $userActiveCoachingSessions['idCoach'];
				                    $coachingSessionDateStart = $userActiveCoachingSessions['datetime_start'];
				                    $coachingSessionDateStart = Yii::app()->localtime->toLocalDate($coachingSessionDateStart);
				                    $coachingSessionDateEnd   = $userActiveCoachingSessions['datetime_end'];
				                    $coachingSessionDateEnd = Yii::app()->localtime->toLocalDate($coachingSessionDateEnd);

				                    $coachModel = CoreUser::model()->findByPk($coachId);
				                    $avatarCoach = $coachModel->getAvatarImage(false, '', 'width: 50px;');
				                    $coachFirstName = $coachModel->firstname;
				                    $coachLastName = $coachModel->lastname;

				                    // Start extra coaching session stuff
				                    $message = '';
			                    }
		                    }
	                    }

                        // all good
                        $response->setMessage($message);
                        $response->setData(array())->setStatus(true);
                    }
                }
            }
        }

        $response->toJSON();
        Yii::app()->end();
    }


	/**
	 * Edit Deliverable Page Dialog
	 *
	 */
	public function actionAxEditDeliverable() {
		$deliverableObj = new LearningDeliverable();
		$learningObject = null;

		$idResource = intval(Yii::app()->request->getParam("idResource", null));
		$idCourse = $this->getIdCourse(); // Set in init() of parent class automatically

		if ($idResource > 0) {
			$learningObject = LearningOrganization::model()->findByAttributes(array('idOrg' => $idResource, 'objectType' => LearningOrganization::OBJECT_TYPE_DELIVERABLE));
			if ($learningObject) {
				// get htmlpage object
				$deliverableObj = LearningDeliverable::model()->findByPk($learningObject->idResource);
			}
		}

		$isListViewLayout = false;
		if($idCourse)
			$isListViewLayout = LearningCourse::getPlayerLayout($idCourse)==LearningCourse::$PLAYER_LAYOUT_LISTVIEW;

		$response = new AjaxResult();
		$html = $this->renderPartial("_edit_html", array(
			'deliverableObj'=>$deliverableObj,
			'loModel'=>$learningObject,
			'courseModel'=>LearningCourse::model()->findByPk($idCourse),
			'lo_type'=>LearningOrganization::OBJECT_TYPE_DELIVERABLE,
			'isListViewLayout' => $isListViewLayout
		), true, true);
		$response->setStatus(true)->setHtml($html)->toJSON();
		Yii::app()->end();
	}


    public function actionAxEvaluate() {
        $id = Yii::app()->request->getParam('id');
        /* @var $deliverableModel LearningDeliverableObject */
        $deliverableModel = LearningDeliverableObject::model()->findByPk($id);
        if (!$deliverableModel) {
            Yii::app()->end();
            return;
        }

        $isReadOnly = ($deliverableModel->evaluation_user_id != null);
        if ( ! $isReadOnly) {
            $deliverableModel->evaluation_user_id = Yii::app()->user->id;
        }

        $userModel = CoreUser::model()->findByPk($deliverableModel->id_user);

        $form = new DeliverableEvaluationForm();
        $form->deliverable = $deliverableModel;
        $form->init();

        if (isset($_POST['DeliverableEvaluationForm'])) {
            $postData = $_POST['DeliverableEvaluationForm'];
            $form->attributes = $postData;
            $form->file = CUploadedFile::getInstance($form, 'file');

            if ($form->validate()) {
                if ($form->save()) {
                    // if the instructor marks the deliverable as passed, mark the LO as 'complete'
                    if ($form->status == LearningDeliverableObject::STATUS_ACCEPTED) {


                    }

					// Trigger the InstructorEvaluatedDeliverable event
					Yii::app()->event->raise('CourseDeliverableEvaluated', new DEvent($this, array(
						'deliverableObject' => $deliverableModel,
						'evaluated_user' => $userModel
					)));
					$event = new DEvent($this, array(
						'course' => $_SESSION['idCourse'],
						'user' => $userModel,
						'idObject' => $id,
						'type' => "assignment",
					));
					Yii::app()->event->raise('InstructorEvaluatedAssignment', $event);
                    // close dialog and reload grid
                    echo '<a href="#" class="auto-close"></a>';
                    Yii::app()->end();
                }
            }
        }

        $this->renderPartial('_evaluate', array(
            'evaluationForm' => $form,
            'userModel' => $userModel,
            'deliverableModel' => $deliverableModel,
            'isReadOnly' => $isReadOnly
        ));
    }

    public function actionAxViewEvaluation() {
        $id = Yii::app()->request->getParam('id');
        /* @var $deliverableModel LearningDeliverableObject */
        $deliverableModel = LearningDeliverableObject::model()->findByPk($id);
        if (!$deliverableModel) {
            Yii::app()->end();
            return;
        }

        $userModel = CoreUser::model()->findByPk($deliverableModel->evaluation_user_id);

        $form = new DeliverableEvaluationForm();
        $form->deliverable = $deliverableModel;
        $form->init();

        if (isset($_POST['DeliverableEvaluationForm'])) {
            $postData = $_POST['DeliverableEvaluationForm'];
            $form->attributes = $postData;
            $form->file = CUploadedFile::getInstance($form, 'file');

            if ($form->validate()) {
                if ($form->save()) {
                    // close dialog and reload grid
                    echo '<a href="#" class="auto-close"></a>';
                    Yii::app()->end();
                }
            } else {

            }
        }

        $this->renderPartial('_view_evaluation', array(
            'evaluationForm' => $form,
            'userModel' => $userModel,
            'deliverableModel' => $deliverableModel,
        ));
    }

	public function actionReplyToInstructorComment(){
		$id = Yii::app()->request->getParam('id');
		$reply = Yii::app()->request->getParam('reply');

		/* @var $deliverableModel LearningDeliverableObject */
		$deliverableModel = LearningDeliverableObject::model()->findByPk($id);
		if (!$deliverableModel) {
			throw new CException('Invalid deliverable ID');
		}

		if($deliverableModel->id_user != Yii::app()->user->getIdst()){
			throw new CException('Invalid deliverable ID');
		}

		if($reply){
			$deliverableModel->user_reply = Yii::app()->htmlpurifier->purify($reply);
			$deliverableModel->save();
		}

		$this->sendJSON(array(
			'success'=>true,
			'reply'=>$deliverableModel->user_reply,
		));
	}
//ivo bug research
	public function actionStreamFile($file = false, $ext = false)
	{
		if($file && $ext)
		{
			if(strpos(LearningDeliverableObject::EXTENSIONS_WHITELIST, $ext) !== false)
			{
				echo '<object width="100%" height="100%"
					type="video/x-ms-asf" url="' . $file . '" data="' . $file . '"
					classid="CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6">
					<param name="url" value="' . $file . '">
					<param name="filename" value="' . $file . '">
					<param name="autostart" value="1">
					<param name="uiMode" value="full">
					<param name="autosize" value="1">
					<param name="playcount" value="1">
					<embed type="application/x-mplayer2" src="' . $file . '" width="100%" height="100%" autostart="true" showcontrols="true" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/"></embed>
				</object>';
			}
			else
			{

				header("Content-Type: application/octet-stream");
				header("Content-Transfer-Encoding: Binary");
				header("Content-disposition: attachment; filename=\"$file\"");
				readfile($file);
				exit;
			}
		}
		Yii::app()->end();
	}


}