<?php


class DeliverableEvaluationForm extends CFormModel {
    /**
     * @var LearningDeliverableObject
     */
    public $deliverable;
    /**
     * @var CUploadedFile
     */
    public $file;
    /**
     * @var integer
     */
    public $score;
    /**
     * @var integer
     */
    public $status;
    /**
     * @var string
     */
    public $comments;
    /**
     * @var bool
     */
    public $allowReupload;


    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            array('file,score,status,comments,allowReupload', 'safe'),
            array('file', 'file', 'allowEmpty'=>true),
            array('score', 'numerical', 'integerOnly'=>true),
            array('score', 'validScore'),
            array('score, status', 'required')
        );
    }

    public function validScore($attribute, $params) {
        $score = intval( $this->score );
        if (! ($score >= 0 && $score <= 100)) {
            $this->addError('score', Yii::t('deliverable', 'Invalid score'));
        }
    }

    public function init() {
        parent::init();

        $this->score = $this->deliverable->evaluation_score;
        $this->status = $this->deliverable->evaluation_status;
        $this->comments = $this->deliverable->evaluation_comments;
        $this->allowReupload = $this->deliverable->allow_another_upload;
    }

    /**
     * @return bool
     */
    public function save() {
        $this->deliverable->evaluation_score = $this->score;

        if ($this->file) {
            // We must make a COPY of the uploaded file, while renaming it
            $filename = 'deliverable_evaluation_attachment_' . $this->deliverable->idObject . '.' . CFileHelper::getExtension($this->file->getName());
            $uploadTmpFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $filename;
            @copy($this->file->tempName, $uploadTmpFilePath);

            // Now we have the new file, lets save it to the storage (store())
            $storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_DELIVERABLE);
            $storageManager->store($uploadTmpFilePath);

            // Save filename (only! no path) to $branding model
            $this->deliverable->evaluation_file = $filename;
        }

        // set status
        $status = intval($this->status);
        $this->deliverable->evaluation_status = ($status > 0)
            ? LearningDeliverableObject::STATUS_ACCEPTED
            : LearningDeliverableObject::STATUS_REJECTED;

        $this->deliverable->evaluation_comments = $this->comments;
        $this->deliverable->evaluation_date = Yii::app()->localtime->toLocalDateTime();
        $this->deliverable->allow_another_upload = $this->allowReupload;

        $result = $this->deliverable->save();

        return $result;
    }
}