<?php


class PlayerDeliverableUploadForm extends CFormModel {
    public $file;
    public $videoUrl;
    public $name;
    public $comments;

    /**
     * upload|link
     * @var string
     */
    public $type;

    /**
     * The deliverable that will be saved in the db
     * @var LearningDeliverableObject
     */
    public $deliverableObject;

	public static $allowedEmbed = array('vimeo', 'youtube', 'youtu.be');


    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            array('file,videoUrl,name,comments', 'safe'),
            array('name', 'required'),
            array('videoUrl', 'validateUrl'),
            array('file', 'validateFile'),
            array('deliverable_content', 'validateContent'),
        );
    }

	/**
	 * Overwriting the parent required() method
	 * because we need to use custom error messages
	 * when some required fields are missing
	 *
	 * @param $attribute
	 * @param $params
	 */
	public function required($attribute,$params)
	{
		if(!isset($this->$attribute) || empty($this->$attribute)){
			$this->addError($attribute, Yii::t('register', '_SOME_MANDATORY_EMPTY'));
		}
		return;
	}

    public function beforeValidate() {

        // clean the url before validation
        $this->normalizeVideoUrl();

        return parent::beforeValidate();
    }

    public function normalizeVideoUrl() {
        // normalize url
        $url = trim( $this->videoUrl );

        if ($url != '') {

            $url = str_replace('https://', 'http://', $url);

            // enforce http://
            $pos = strpos($url,'http://');
            if ($pos > 0) {
                $url = substr($url, $pos);
            }
            else if ($pos === false) {
                $url = 'http://' . $url;
            }

        }

        $this->videoUrl = $url;
    }

    public function validateContent($attribute, $params) {

        // either a video url must be set or a video file must be uploaded
        if ( ! in_array($this->type, array('upload', 'link')) ) {
            $this->addError('deliverable_content', Yii::t('field', '_NO_FILE_UPLOADED'));
        }
    }

	static public function isEmbedUrlWhitelisted($url){
		if (!empty($url)) {
			foreach (self::$allowedEmbed as $linkType) {
				if (strpos($url, $linkType) > 0) {
					return true;
				}
			}
		}

		return false;
	}

    public function validateUrl($attribute, $params) {
        if (!empty($this->videoUrl)) {
	        if(self::isEmbedUrlWhitelisted($this->videoUrl)){
		        $isValid = true;
		        // flag that we have a valid video url
		        $this->type = 'link';
	        }
        }
    }

    public function validateFile($attribute, $params) {

        // validate only if a file is uploaded
        if ( ! empty($this->file)) {

			foreach ($this->file as $file) {
				try {
					// Validate file input
					if (!is_array($file)) { throw new CException("Invalid file object."); }
					if (!isset($file['name']) || ($file['name'] == '')) { throw new CException("Invalid file name."); }
					if (!isset($file['percent']) || $file['percent'] < 100) { throw new CException("Incomplete file upload."); }

					// Check if uploaded file exists
					$uploadTmpFile = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR.$file['name'];
					if (!is_file($uploadTmpFile)) { throw new CException("File '" . $file['name'] . "' does not exist."); }

					/*********************************************************/

					/*** File extension type validate ************************/
					$generalFileValidator = new FileTypeValidator(FileTypeValidator::TYPE_ITEM);
					$videoFileValidator = new FileTypeValidator(FileTypeValidator::TYPE_VIDEO);

					if (!$generalFileValidator->validateLocalFile($uploadTmpFile) && !$videoFileValidator->validateLocalFile($uploadTmpFile))
					{
						FileHelper::removeFile($uploadTmpFile);
						if($generalFileValidator->errorMsg){
							throw new CException($generalFileValidator->errorMsg);
						}elseif($videoFileValidator->errorMsg){
							throw new CException($videoFileValidator->errorMsg);
						}
					}
					/*********************************************************/

					// flag that we have a ready video file
					$this->type = 'upload';
				}
				catch (CException $ex) {
					$this->addError('file', $ex->getMessage());
				}
            }
        }
    }

	/**
	 * Saves the uploaded video
	 * @throws CException
	 * @return string
	 */
    private function saveUploadedFile() {

		$content = array();

		foreach ($this->file as $file) {
			// Rename uploaded file
			$extension = strtolower(CFileHelper::getExtension($file['name']));
			$newFileName = Docebo::randomHash() . "." . $extension;
			$newTmpFile = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR.$newFileName;

			$uploadTmpFile = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR.$file['name'];

			if (!@rename($uploadTmpFile, $newTmpFile)) { throw new CException("Error renaming uploaded file."); }

			// Move uploaded file to final storage for later usage
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_DELIVERABLE);
			if ( ! $storageManager->store($newTmpFile)){ throw new CException("Error while saving file to final storage."); }

			// Delete tmp file
			FileHelper::removeFile($newTmpFile);

			$content[] = $newFileName;
		}

        return CJSON::encode($content);
    }

    /**
     * Saves the deliverable object
     * @return bool
     */
    public function save() {
        try {
            // check if form is valid
            if ( ! $this->validate())
                return false;

            $this->deliverableObject->content = ($this->type == 'link')
                ? $this->videoUrl
                : $this->saveUploadedFile();

            $this->deliverableObject->type = $this->type;
            $this->deliverableObject->name = $this->name;
            $this->deliverableObject->student_comments = $this->comments;
            $this->deliverableObject->created = Yii::app()->localtime->toLocalDateTime();

            return $this->deliverableObject->save();
        }
        catch (CException $ex) {
	        Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
            return false;
        }
    }
}