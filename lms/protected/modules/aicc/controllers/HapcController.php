<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */

class HapcController extends Controller {

	
	const CMI_MAX_VERSION = "4.0";
	
	/**
	 * @var LearningAiccSession
	 */
	protected $sessionModel;
	
	
	/**
	 * @var LearningAiccItem
	 */
	protected $itemModel;
	
	/**
	 * @var LearningAiccPackage
	 */
	protected $packageModel;

	/**
	 * @var LearningOrganization
	 */
	protected $loModel;
	
	/**
	 * @var LearningCourse
	 */
	protected $courseModel;
	
	/**
	 * @var CoreUser
	 */
	protected $userModel;
	
	/**
	 * @var LearningAiccItemTrack
	 */
	protected $itemTrackModel;
	
	/**
	 * @var LearningAiccPackageTrack
	 */
	protected $packageTrackModel;

	
	
	protected $aiccData;
	
	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		
		return array(
			'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		return array(

			array('allow',
				'users' => array('*'),
			),
		);
	}


	/**
	 * This action is the main entry point for HAPC communication with AU 
	 */
	public function actionIndex() {

		// Lower the KEYs case of the REQUEST array, because parameters are case insensitive by specification
		$_REQUEST = array_change_key_case($_REQUEST, CASE_LOWER);
		
		// We expect ... at least command && session
		$command		= isset($_REQUEST['command']) ? $_REQUEST['command'] : false;
		$sessionId		= isset($_REQUEST['session_id']) ? $_REQUEST['session_id'] : false;
		$version		= isset($_REQUEST['version']) ? $_REQUEST['version'] : false;
		$this->aiccData	= isset($_REQUEST['aicc_data']) ? $_REQUEST['aicc_data'] : false;

		// Check if the request LOOKS good
		if (!$command || !$sessionId) {
			echo "
				error=1
				error_text=Invalid command or session
				aicc_data=
			";
			Yii::log("Invalid AICC HAPC Request (no session or command)", CLogger::LEVEL_WARNING);
			Yii::app()->end();
		}


		// Check version compatibility 
		if (!$this->checkVersion($version)) {
			echo "
				error=1
				error_text=Version $version is not supported by this LMS
				aicc_data=
			";
			Yii::log("CMI001 Version $version call has been received. Max supported version is " . self::CMI_MAX_VERSION, CLogger::LEVEL_INFO);
			Yii::app()->end();
		}

		/*
		 *  NO we are ADDING preview mode
		 * So we need to fake the traking of the AICC and return fake taka as well
		 */
		if ($sessionId === 'preview_mode') {
			$response = $this->handlePreviewMode($command);
			echo $response;
			Yii::app()->end();
		}
		
		// Check/Validate session. "false" or null means "bad" session. 
		// Good session is a returned model which is used further down the request and even modified and saved. 
		$this->sessionModel = LearningAiccSession::checkSession($sessionId);
		if (!$this->sessionModel) {
			throw new CHttpException(500, 'Session timeout');
		}
		
		// Load controller-wide models
		if (!$this->loadModels()) {
			echo "error=500\r\nerror_text=Invalid data\r\n";
		}
		
		// Check the incoming command
		$command = strtolower($command);
		
		
		// In case of "getparam" command, change Session status to RUNNING if it is still not initialized
		if ($command == 'getparam') {
			if ($this->sessionModel->status == LearningAiccSession::STATUS_NOTINIT) {
				$this->sessionModel->status  = LearningAiccSession::STATUS_RUNNING;
			}
		}
		
		// Every command requires a "running" session. Echo the check response if it is not
		$checkResponse = $this->isRunningSession();
		if ($checkResponse !== true) {
			echo $checkResponse;
		}

		
		// Execute incoming command
		if (method_exists($this, $command)) {
			$response = $this->{$command}();
		}
		else {
			throw new CHttpException(500, 'Unknown command: ' . $command);
		}

		// If command result has something to echo back to the caller (AU), just echo it
		if ($response) {
			echo $response;
		}					
		
		
	}

	/**
	 * Load a set of AR models used across the whole controller
	 * 
	 */
	protected function loadModels() {
		
		$this->itemModel		= $this->sessionModel->item;
		$this->packageModel   	= $this->itemModel->package;

		$this->loModel			= $this->packageModel->learningObject;
		// If loModel is NULL, we may have a central LO AICC here. Retrieve the id_org from the $_REQUEST
		if(!$this->loModel) {
			$idOrg = isset($_REQUEST['id_org']) ? $_REQUEST['id_org'] : false;
			if($idOrg)
				$this->loModel = LearningOrganization::model()->findByPk($idOrg);
		}

		$this->courseModel		= $this->loModel ? $this->loModel->course : null;
		$this->userModel 		= $this->sessionModel->user;

		if (!$this->itemModel || !$this->packageModel || !$this->userModel || !$this->loModel || !$this->courseModel) {
			return false;
		}


		$this->itemTrackModel 		= LearningAiccItemTrack::getTrack($this->userModel->idst, $this->itemModel->id, $this->loModel->idOrg);
		$this->packageTrackModel 	= LearningAiccPackageTrack::getTrack($this->userModel->idst, $this->packageModel->id);
		
		
		return true;
	}
	
	
	/**
	 * Compare AU CMI001 version (if sent along with the command) to our (supported version)
	 * 
	 * @param unknown $version
	 * @return boolean
	 */
	protected function checkVersion($version) {
		if (is_string($version))
			return version_compare($version, self::CMI_MAX_VERSION, '<=');
		return true;
	}
	
	/**
	 * 
	 * @return string
	 */
	private function getparam() {
		/* @var $itemModel LearningAiccItem  */
		
		$studentName 	= $this->userModel->lastname . ', '. $this->userModel->firstname;
		
		// Save session 
		$this->sessionModel->save(false);
		
		$track = $this->itemTrackModel;
		$cmi = false;
		$entry = "";
		
		$cmi = $track->cmi;
		$lessonStatus = $track->lesson_status;
		
		// Lesson Status
		if (!$lessonStatus) {
			$lessonStatus = LearningAiccItemTrack::LESSON_STATUS_NOT_ATTEMPTED;
		}
		if ($cmi == false) {
			$entry = "," . LearningAiccItemTrack::ENTRY_AB_INITIO;
		}
		else if (isset($cmi['cmi.core.exit']) &&  ($cmi['cmi.core.exit'] == 'suspend')) {
			$entry = "," . LearningAiccItemTrack::ENTRY_RESUME;
		}
		$lessonStatus = $lessonStatus . $entry;
		
		
		// Suspend data
		$suspendData = $track->suspend_data ? trim(rawurldecode($track->suspend_data)) : "";

		// Lesson mode & Credit
		$credit = "no-credit";
		if ($this->sessionModel->mode == LearningAiccItemTrack::LESSON_MODE_NORMAL) {
			$credit = "credit";
		}
		
		// Time
		$time = "00:00:00";
		if ($track->total_time) {
			$time = $track->total_time;
		}
		
		// Response
		$response = "
			error=0 
			error_text=Successful
			aicc_data=
			[Core]
				Student_ID=". $this->userModel->idst ." 
  				Student_Name= $studentName 
   				Lesson_Location=".$track->lesson_location." 
   				Credit=$credit 
   				Lesson_Status=$lessonStatus
   				Score=$track->score_raw,$track->score_min,$track->score_min 
  				Time=$time
   				Lesson_Mode=".$this->sessionModel->mode." 
			[Core_Lesson]".($suspendData ? "
				".$suspendData : "")."
			[Core_Vendor]".($this->itemModel->datafromlms ? "
				".$this->itemModel->datafromlms : "")."
			[Evaluation]
				{".$this->courseModel->idCourse."}
			[Student_Data]
				Mastery_Score=".$this->itemModel->mastery_score."
				Max_Time_Allowed=".$this->itemModel->max_time_allowed."
				Time_Limit_Action=".$this->itemModel->time_limit_action."				
		";
		
		return $response;
		
	}
	
	
	
	/**
	 * 
	 * @return string
	 */
	private function putparam() {
		
		// Put param requires AICC DATA POST parameter
		if (!$this->aiccData) {
			return "
				error=500
				error_text=Missing Aicc Data during putParam
			";
		}

		$track = $this->itemTrackModel;
		$track->timemodified 	= Yii::app()->localtime->getUTCNow('Y:m:d H:i:s');
		
		// Now lets analyze the incoming data
		$initLessonStatus 	= LearningAiccItemTrack::LESSON_STATUS_NOT_ATTEMPTED;
		if (isset($this->sessionModel->lesson_status)) {
			$initLessonStatus = $this->sessionModel->lesson_status;
		}
		$score = '';

		// Parse data
		$aiccDataArray = LearningAiccItemTrack::parseAiccIniData($this->aiccData);

		if(isset($aiccDataArray['core']['lesson_status'])) {
			$status = strtolower($aiccDataArray['core']['lesson_status']);
			if(isset(LearningAiccItemTrack::$LESSON_STATUSES_MAP[$status]))
				$aiccDataArray['core']['lesson_status'] = LearningAiccItemTrack::$LESSON_STATUSES_MAP[$status];
		}
		
		// Save tracking data to tracking table (for the item/user) and also some data in session record
		$track->lesson_location 	= isset($aiccDataArray['core']['lesson_location']) ? $aiccDataArray['core']['lesson_location'] : '';
		
		// Only register lesson status if Lesson Mode (session mode) is NORMAL, i.e. not "browsing", for example
		if (isset($aiccDataArray['core']['lesson_status']) && ($this->sessionModel->mode == LearningAiccItemTrack::LESSON_MODE_NORMAL)) {
			$track->lesson_status = strtolower($aiccDataArray['core']['lesson_status']);
		}

		// Scores
		if (isset($aiccDataArray['core']['score'])) {
			list($raw, $max, $min) = explode(',', $aiccDataArray['core']['score']);
			if (!$raw) $raw = 0;
			if (!$max) $max = 0;
			if (!$min) $min = 0;
			$track->score_raw = $raw;
			$track->score_max = $max;
			$track->score_min = $min;
		}
		
		if (isset($aiccDataArray['core']['time']) )
			$this->sessionModel->session_time = $aiccDataArray['core']['time'];

		// Free form sections
		if (isset($aiccDataArray['core_lesson'])) {
			$track->suspend_data = rawurlencode($aiccDataArray['core_lesson']); 
		}
		if (isset($aiccDataArray['comments'])) {
			$track->comments = rawurlencode($aiccDataArray['comments']);
		}
		
		
		if (($this->sessionModel->mode == LearningAiccItemTrack::LESSON_MODE_BROWSE) && ($initLessonStatus == LearningAiccItemTrack::LESSON_STATUS_NOT_ATTEMPTED)) {
			$track->lesson_status = LearningAiccItemTrack::LESSON_STATUS_BROWSED;
		}
		else if ($this->sessionModel->mode == LearningAiccItemTrack::LESSON_MODE_NORMAL) {
			if ($this->itemModel->mastery_score && is_numeric($this->itemModel->mastery_score)) {
				if ($track->score_raw && is_numeric($track->score_raw)) {
					if ($track->score_raw >= $this->itemModel->mastery_score) {
						$track->lesson_status = LearningAiccItemTrack::LESSON_STATUS_PASSED;
					} else {
						$track->lesson_status = LearningAiccItemTrack::LESSON_STATUS_FAILED;
					}
						
				}
			}
		}
		
		// Save the current "lesson mode"
		$track->lesson_mode = $this->sessionModel->mode;

		
		// --- SAVE tracking and session BEFORE propagating the lesson status !!!!
		if (!$track->save()) {
			Yii::log('Error saving tracking data: ' . var_export($track->errors,true), CLogger::LEVEL_ERROR);
			$response = "
				error=500
				error_text=Could not save tracking data
			";
			return $response;
		}
		$this->sessionModel->lesson_status = $track->lesson_status;
		$this->sessionModel->save();
		// ---

		
		// Is the lesson status changing ??
		if ($initLessonStatus != $track->lesson_status) {
			
			// Lesson becomes completed/passed ??
			if (in_array($track->lesson_status, array(LearningAiccItemTrack::LESSON_STATUS_COMPLETED, LearningAiccItemTrack::LESSON_STATUS_PASSED))) {
				$track->propagateCompletion();
			}
			else if ($track->lesson_status == LearningAiccItemTrack::LESSON_STATUS_INCOMPLETE) {
				$track->propagateIncomplete();
			}
			else if ($track->lesson_status == LearningAiccItemTrack::LESSON_STATUS_FAILED) {
				$track->propagateFailed();
			}
				
		}
		
		
		$response = "
			error=0
			error_text=Successful
		";

		return $response;
		
	}
	
	/**
	 * 
	 * @return string
	 */
	private function putcomments() {
		$response = "
			error=0
			error_text=Successful
		";
		return $response;
	}
	
	/**
	 * 
	 * @return string
	 */
	private function putinteractions() {
		$response = "
			error=0
			error_text=Successful
		";
		return $response;
	}
	
	/**
	 * 
	 * @return string
	 */
	private function putobjectives() {
		$response = "
			error=0
			error_text=Successful
		";
		return $response;
	}
	
	/**
	 * 
	 * @return string
	 */
	private function putpath() {
		$response = "
			error=0
			error_text=Successful
		";
		return $response;
	}
		
	/**
	 * 
	 * @return string
	 */
	private function putperformance() {
		$response = "
			error=0
			error_text=Successful
		";
		return $response;
	}
	
	
	/**
	 * 
	 * @return string
	 */
	private function exitau() {
		
		$track = $this->itemTrackModel;
		
		if ($this->sessionModel->session_time) {
			$track->timemodified 	= Yii::app()->localtime->getUTCNow('Y:m:d H:i:s');
			$track->total_time 		= LearningAiccItemTrack::addSessionTime($track->total_time, $this->sessionModel->session_time);
			$track->save();
		}
		
		$this->sessionModel->status = LearningAiccSession::STATUS_TERMINATED;
		$this->sessionModel->session_time = '';
		$this->sessionModel->save();
		
		return "error=0\r\nerror_text=Successful\r\n";
	}

	
	/**
	 * Check if session is "Running". If NOT, return error "response" text, otherwise "true"
	 * 
	 * @return string|boolean
	 */
	private function isRunningSession() {
		if ($this->sessionModel->status != LearningAiccSession::STATUS_RUNNING) {
			return "
				error=1
				error_text=Session is not properly initialized or already terminated
			";
		}
		else {
			return true;
		}
	}

	private function handlePreviewMode($command) {
		// If the command is different than getparam
		// then we Always should return Success
		if($command !== 'getparam')
			return "error=0\r\nerror_text=Successful\r\n";

		// But if the command is 'getparam' then we need more advanced response
		// Response
		$id_user		= isset($_REQUEST['id_user']) ? $_REQUEST['id_user'] : false;
		$id_item		= isset($_REQUEST['id_item']) ? $_REQUEST['id_item'] : false;
		$user = CoreUser::model()->findByPk($id_user);
		$item = LearningAiccItem::model()->findByPk($id_item);
		// Define some variables
		$studentName = $user->lastname . ', '. $user->firstname;
		$lesson_location = "";
		$credit = "no-credit";
		$lessonStatus = LearningAiccItemTrack::ENTRY_AB_INITIO;
		$time = "00:00:00";
		$suspendData = "";
		$idCourse = "1"; // ??? WTF ???

		$response = "
			error=0
			error_text=Successful
			aicc_data=
			[Core]
				Student_ID=". $id_user ."
  				Student_Name= $studentName
   				Lesson_Location=".$lesson_location."
   				Credit=$credit
   				Lesson_Status=$lessonStatus
   				Score=0,0,0
  				Time=$time
   				Lesson_Mode=".LearningAiccItemTrack::LESSON_MODE_NORMAL."
			[Core_Lesson]".($suspendData ? "
				".$suspendData : "")."
			[Core_Vendor]".($item->datafromlms ? "
				".$item->datafromlms : "")."
			[Evaluation]
				{".$idCourse."}
			[Student_Data]
				Mastery_Score=".$item->mastery_score."
				Max_Time_Allowed=".$item->max_time_allowed."
				Time_Limit_Action=".$item->time_limit_action."
		";

		return $response;
	}
	

}