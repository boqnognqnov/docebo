<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */

class DefaultController extends LoBaseController {


	/**
	 * By default, just redirect to main site.
	 */
	public function actionIndex() {
		$this->redirect($this->createUrl('/site'), true);
	}


	/**
	 * Register Controller specific assets
	 *
	 * (non-PHPdoc)
	 * @see ScormorgBaseController::registerResources()
	 */
	public function registerResources() {
		parent::registerResources();

		if (!Yii::app()->request->isAjaxRequest) {
		}

	}


	/**
	 * Update an existing package OR create new one, depending on idOrg
	 * 
	 * @param integer $idOrg
	 * @param array $file
	 * @param string $originalFileName
	 * @param integer $thumb
	 * @param string $shortDescription
	 * @param integer $courseId
	 * 
	 * @throws CException
	 * 
	 * @return LearningAiccPackage
	 */
	protected function updatePackage($idOrg, $file, $originalFileName, $thumb, $shortDescription, $courseId=false) {
		
		$scenario = $idOrg ?  LearningAiccPackage::SCENARIO_UPDATEOLD  : LearningAiccPackage::SCENARIO_CREATENEW;
		
		// Validate file input if NO idOrg is specified, i.e. when we are CREATING NEW
		if ($scenario == LearningAiccPackage::SCENARIO_CREATENEW) {
			if (!is_array($file)) 									{ throw new CException("Invalid file."); }
			if (!isset($file['name']) || ($file['name'] == '')) 	{ throw new CException("Invalid file name."); }
			if (!isset($file['percent']) || $file['percent'] < 100) { throw new CException("Incomplete file upload."); }
		}
		
		$fullFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $file['name'];
		/*********************************************************/
		if (isset(Yii::app()->params['virusscan_enable']) && (Yii::app()->params['virusscan_enable'] == true)) {
			$vs = new VirusScanner();
			$clean = $vs->scanFile($fullFilePath);
			if (!$clean) {
				FileHelper::removeFile($fullFilePath);
				Yii::log('File is not clean (VirusScanner): ' . $fullFilePath, CLogger::LEVEL_ERROR);
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}
		}
		/*********************************************************/
		
		/*** File extension type validate ************************/
		$fileValidator = new FileTypeValidator(FileTypeValidator::TYPE_ARCHIVE);
		if (!$fileValidator->validateLocalFile($fullFilePath))
		{
			FileHelper::removeFile($fullFilePath);
			throw new CException($fileValidator->errorMsg);
		}
		/*********************************************************/
		
		$lo = LearningOrganization::model()->findByPk($idOrg);
		$parentId = (int)$_POST['parent_id'];

		if ($scenario == LearningAiccPackage::SCENARIO_UPDATEOLD) {
			if (!$lo) {
				throw new CException("Invalid Learning Object");
			}
			$package = LearningAiccPackage::model()->findByPk($lo->idResource);
			if (!$package) {
				throw new CException("Invalid Package");
			}
		}
		else {
			$package = new LearningAiccPackage(LearningAiccPackage::SCENARIO_CREATENEW);
			$package->setIdCourse($courseId);
			$package->setShortDescription($shortDescription);
			$package->setIdResource($thumb);
			$package->idReference = null;
			$package->lms_id_user = Yii::app()->user->id;
			$package->setIdParent($parentId);
		}
		
		$package->lms_original_filename = $originalFileName;
			
		// Try to extract the package into the final destination (S3, file system,...)
		if ($package->extractPackage($fullFilePath)) {
			// Save uploaded package (handle its content, create all the intermediate table records, creating LO and so on)
			// Be aware: behavior is attached !!!
			if (!$package->save()) {
				Yii::log('Error saving AICC package: ' . $originalFileName, CLogger::LEVEL_ERROR);
				Yii::log(var_export($package->errors, true), CLogger::LEVEL_ERROR);
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}
			else {
				// Find the just created LO and set the package->idReference to point to the package ID
				$tmpLo = LearningOrganization::model()->findByAttributes(array(
					'objectType'	=> 	LearningOrganization::OBJECT_TYPE_AICC,
					'idResource'	=> 	$package->id
					// idCourse not needed here
				));
				if ($tmpLo) {
					$package->idReference = $tmpLo->idOrg;
					$package->save(false);
					$this->saveLoOptions($tmpLo);
				}
			}
		}
		else {
			Yii::log('Unable to extract AICC package: ' . $originalFileName, CLogger::LEVEL_ERROR);
			throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
		}

		return $package;

	}
	
	
	/**
	 * Just a synonim for updatePackage to avoid confusion 
	 * 
	 * @param unknown $idOrg
	 * @param unknown $file
	 * @param unknown $originalFileName
	 */
	protected function createPackage($idOrg, $file, $originalFileName, $thumb, $shortDescription, $courseId) {
		return $this->updatePackage($idOrg, $file, $originalFileName, $thumb, $shortDescription, $courseId);
	}
	
	
	/**
	 * Save uploaded LO. Caller must already saved the file into
	 * <root>/files/upload_tmp folder.
	 */
	public function actionAxSaveLo() {
		
		$courseId 	= Yii::app()->request->getParam("course_id", null);
		$parentId 	= Yii::app()->request->getParam("parent_id", null);
		$file 		= Yii::app()->request->getParam("file", null);

		
		$response = new AjaxResult();
		$transaction = Yii::app()->db->beginTransaction();
		
		try {
			
			// Use the physically uploaded filename, put by PLUpload in the "target_name" element
			// This is tru when "unique_names" option is used for plupload JavaScript object
			if (is_array($file) && isset($file['name'])) {
				$originalFileName = urldecode($file['name']);
				if (isset($file['target_name'])) {
					$file['name'] = $file['target_name'];
				}
			}
			
			$shortDescription 	= Yii::app()->request->getParam('short_description', '');
			$thumb 				= Yii::app()->request->getParam('thumb', null);
	
			// URLDecode
			if (is_array($file) && isset($file['name'])) {
				$file['name'] = urldecode($file['name']);
			}
			
			// In case we are doing SCORM Package update ... we will get idOrg != false ... (from edit form)
			$idOrg = (int) Yii::app()->request->getParam("id_org", false);
			
			if ($idOrg > 0) {
				$data = array(
					'message' => Yii::t('standard', '_OPERATION_SUCCESSFUL'),
				);
				$response->setData($data)->setStatus(true);
					
				// IF we have file uploaded, update scorm package
				if (($file !== 'false') && ($file !== null) && ($file !== false)) {
					$this->updatePackage($idOrg, $file, $originalFileName, $thumb, $shortDescription);
				}
				// Otherswise, just update options of the LO, if any
				else {
					$loModel = LearningOrganization::model()->findByPk($idOrg);
				}
				
				// Some LO types are passing specific options upon uploading/saving REQUEST; lets handle them
				$lo = LearningOrganization::model()->findByPk($idOrg);
				if ($lo)  {
//					if($shortDescription)
						$lo->short_description = Yii::app()->htmlpurifier->purify($shortDescription);
					if($thumb){
						$lo->resource = intval($thumb);
					}elseif($lo->resource){
						$lo->resource = 0;
					}

					$this->saveLoOptions($lo);
	
					//update title displayed in LMS
					$newTitle = urldecode(trim(Yii::app()->request->getParam("new-aicc-title", null)));
					$lo->title = $newTitle;
					$lo->saveNode();
				}
				
				$transaction->commit();

				// Trigger event to let plugins save their own data from POST
				Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
					'loModel' => $lo
				)));

				$response->toJSON();
				Yii::app()->end();
			}
	
			
			/// We are here because we are creating NEW package
			$package = $this->createPackage($idOrg, $file, $originalFileName, $thumb, $shortDescription, $courseId);
			if (!$package) {
				Yii::log('Invalid package returned from updatePackage', CLogger::LEVEL_ERROR);
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			$transaction->commit();

			// Trigger event to let plugins save their own data from POST
			Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
				'loModel' => $package->learningObject
			)));

			$data = array(
				'message' => Yii::t('standard', '_OPERATION_SUCCESSFUL'),
			);
			$response->setData($data)->setStatus(true);

		} catch (CException $e) {
			$transaction->rollback();
			$response->setMessage($e->getMessage())->setStatus(false);
		}

		$response->toJSON();
		Yii::app()->end();

	}


	/**
	 * Return all the data in order to launch the scorm object requested
	 */
	public function actionAxGetLaunchInfo() {

		$response = new AjaxResult();

		$idOrg 			= Yii::app()->request->getParam("id_org", 0);
		$idAiccItem 	= Yii::app()->request->getParam("id_aicc_item", 0);
		$launch_type 	= Yii::app()->request->getParam("launch_type", '');
		$mode			= Yii::app()->request->getParam("mode", LearningAiccItemTrack::LESSON_MODE_NORMAL);

		try {
			$item = LearningAiccItem::model()->findByPk($idAiccItem);
			if (!$item) {
				throw new CException('Invalid Aicc item: ' . $idAiccItem);
			}

			$session = LearningAiccSession::getSession($item->id, $mode, $idOrg);
			if (!$session) {
				throw new CException(Yii::t('standard', 'Invalid session ID'));
			}
			
			$launchUrl = $item->launchUrl($session, $idOrg);
			
			$data = array(
				'launch_url'	=> $launchUrl,	
			);
			
			$response->setStatus(true);
			$response->setData($data);
			$response->toJSON();
			Yii::app()->end();

		}
		catch (CException $e) {
			$response->setStatus(false)->setMessage($e->getMessage())->toJSON();
			Yii::app()->end();
		}

	}


	/**
	 * This action will be called when the player must be closed, in this case we return a simple js to close
	 * the, depending on the launch mode, the player
	 *
	 * @param $launch_type
	 * @param $course_id
	 */
	public function actionClosePlayer($launch_type, $course_id) {

		switch($launch_type) {
			case "popup": {
				// this is more complex, we have to refresh the opener and close ourself
				echo "<script type=\"text/javascript\">
					try {
						window.opener.location = '" . $this->createUrl('/player') . "';
					} catch(e) {}
					window.top.close();
					</script>";
			};break;
			case "fullscreen": {
				// shouldn't be used, the player is doing this automatically, but just in case
				echo "<script type=\"text/javascript\">
					window.top.location =  '" . $this->createUrl('/player') . "';
					</script>";
			};break;
			case "lightbox":
			case "inline":
			default: {
				echo "<script type=\"text/javascript\">
					window.top.location.reload();
					</script>";
			};break;

		}
		Yii::app()->end();
	}

	
	/**
	 * A work towards saving LO specific options/settings based on the current REQUEST to axSaveLo() action
	 * 
	 * @param object|bool $loModel
	 */
	private function saveLoOptions($loModel = false) {
		
		if (!$loModel) {
			return;
		}	
		
		// These are pretty common and can be sent by all LO types being saved/uploaded
		$loModel->desktop_openmode    = Yii::app()->request->getParam("desktop_openmode", LearningOrganization::OPENMODE_LIGHTBOX);
		$loModel->tablet_openmode     = Yii::app()->request->getParam("tablet_openmode", LearningOrganization::OPENMODE_FULLSCREEN);
		$loModel->smartphone_openmode = Yii::app()->request->getParam("smartphone_openmode", LearningOrganization::OPENMODE_FULLSCREEN);
		
		$loModel->saveNode(false);
		
	}
	
	

}
