<?php


class XapiBaseController extends Controller {	

	/**
	 * overwrite resolveLayout method to use module based layout
	 */
	public function resolveLayout()
	{
		$this->layout = '/layouts/base';
	}
	
	
	/**
	 * @see CController::filters()
	 */
	public function filters() 
	{
		return array(
			'accessControl',
		);
	}
	
	
	/**
	 * @see CController::accessRules()
	 */
	public function accessRules() 
	{
		return array(
			array('allow',
				'users'=>array('*'),
			),
		);
	}
	
	
}
