<?php
/* @var $resultModel LrsResult */

class XapiTrack extends CComponent {

	/**
	 * 
	 * @param LrsStatement $statementModel
	 * @return NULL
	 */
    public static function trackStatement(LrsStatement $statementModel) {

    	
    	
        // Check if it is a model
        if (!$statementModel) return null;
        
        if (!$statementModel->id_context) return null;
        
        if ($statementModel->object_type != Xapi::OBJ_TYPE_ACTIVITY) return null;
        
        
        $activityModel  = LrsActivity::model()->findByPk($statementModel->id_object);
        $contextModel   = LrsContext::model()->findByPk($statementModel->id_context);
        
        if (!$activityModel) return null;
        if (!$contextModel) return null;
        
        $registration = $contextModel->registration_id;
        
        // Retrieve User authenticated data
        //@TODO AUTH ISSUE
        $user = XapiAuth::getInstance()->user;
        $idUser     = $user['idst'];
        
        // Get Learning Org
        $loModel = self::getLoModelFromContext($contextModel);
        $idCourse = $loModel->idCourse; 
        
        if (!$idUser || !$idCourse) return null;
        
        // Update Active user and Course session access
        UserLimit::logActiveUserAccess($idUser);
        LearningCourseuser::updateCourseUserAccess($idUser, $idCourse);

        // We track ONLY MAIN Activity into common tracking table
        $activityProvider = LearningTcAp::model()->findByAttributes(array(
            'registration'	=> $registration,
        ));
        $mainActivityId = $activityProvider->mainActivity->id;
        if ($mainActivityId != $activityModel->activity_id) {
            return null;
        }
		// Adjust the object reference (in case this is a central repo LO)
		$loModel = $loModel->getMasterForCentralLo($idUser);

        // Get/Create TinCan specific tracking model
        $tcTrack = LearningTcTrack::model()->findByAttributes(array(
            'idReference'       => $loModel->idOrg,
            'idResource'        => $loModel->idResource,
            'idUser'            => $idUser,
        ));
        if (!$tcTrack) {
            $tcTrack = new LearningTcTrack();
        }
        $tcTrack->idReference   = $loModel->idOrg;
        $tcTrack->idResource    = $loModel->idResource;
        $tcTrack->idUser        = $idUser;
        $tcTrack->save(false);

        $idTcTrack = $tcTrack->idTrack;
        
        // Reset score given (see comment in the function)
       	self::resetCourseuserScore($idCourse, $idUser);

       	// Determine the STATUS purely based on VERB, but read below (because of RESULT!)
       	$status = LrsVerb::verbIdToCommontrackStatus($statementModel->verb, LearningCommontrack::STATUS_ATTEMPTED);
       	
       	// Unhandled status (they are many!)
       	if (!$status) return true;
       	
       	// Get RESULT, if any, and get scoring and success info (and resolve STATUS)
       	$score = null;
       	$score_max = null;
       	$total_time = null;
       	$resultModel = LrsResult::model()->findByPk($statementModel->id_result);
       	if ($resultModel) {
       	     
       	    if ($resultModel->score_scaled != null) {
       	        $score = $resultModel->score_scaled * 100;
       	    }
       	    else if ($resultModel->score_raw != null) {
       	        $score = $resultModel->score_raw;
       	    }
       	     
       	    $score_max = $resultModel->score_max;
       	
       	    if ($resultModel->duration != null) {
       	        $resultTime = self::calcFinishTime($resultModel->duration);
       	        $total_time = $resultTime["seconds"];
       	    }
       	     
       	    // !IMPORTANT
       	    // Respect the Result->Success value, is passed, as a final status - if it is FALSE (0), status is FAILED
       	    // If Success is NOT set, keep using the Status resolved from VERB
       	    if ($resultModel->success !== null) {
       	        $status = (bool) $resultModel->success ? $status : LearningCommontrack::STATUS_FAILED;
       	    }
       	    
       	    self::resetCourseuserScore($idCourse, $idUser);
       	     
       	}

       	// Track the status (get the common tracking model back)
       	$commonTrack = CommonTracker::track($loModel->idOrg, $idUser, $status, array('preventStatusDowngrade' => true));
       	if (!($commonTrack instanceof LearningCommontrack)) {
       		throw new XapiException("Failed to save common tracking status");
       	}
       	
       	// LO Statuses which imply a "final scoring" for common tracking
       	// Many other status MAY come but we consider only these to be "score giving"
       	$finalResultStatuses = array(
			LearningCommontrack::STATUS_FAILED,
       		LearningCommontrack::STATUS_COMPLETED,
       		LearningCommontrack::STATUS_PASSED
       	);
       	if ($resultModel && in_array($status, $finalResultStatuses)) {
            $commonTrack->score = $score;
       		$commonTrack->score_max = $score_max;
       		$commonTrack->total_time = $total_time;
       		if (!$commonTrack->save()) {
       			throw new XapiException("Failed to save common tracking results");
       		}
       		self::resetCourseuserScore($idCourse, $idUser);
       	}
       	
    }
    

    /**
     * 
     * @param unknown $contextModel
     * @return LearningOrganization
     */
    private static function getLoModelFromContext($contextModel) {
         
        $params = array();
        $command = Yii::app()->db->createCommand();
         
        $command->select('idOrg')
            ->from("learning_organization lo")
            ->join("learning_tc_activity tca"   , "lo.idResource=tca.id_tc_activity AND (lo.objectType=:tincan OR lo.objectType=:elucidat)")
            ->join("learning_tc_ap tcap"        , "tcap.id_tc_ap=tca.id_tc_ap AND tcap.registration=:registration");
         
        $params[":registration"] = $contextModel->registration_id;
        $params[":tincan"]      = LearningOrganization::OBJECT_TYPE_TINCAN;
        $params[":elucidat"]    = LearningOrganization::OBJECT_TYPE_ELUCIDAT;
         
        $idOrg = $command->queryScalar($params);
         
        $model = LearningOrganization::model()->findByPk($idOrg);
        
        return $model;
    
    }
    

    /**
     * (Comment copied from old code):
     *  
     * A bit of a workaround to accomodate the new "final score" logic
     * Since the Tincan api is using the old code and in some "final score"
     * modes the final score needs to be recalculated when a Tincan object
     * is completed, we clear the learning_courseuser.score_given value
     * so the main Yii code can recalculate it the next time it's requested
     * to be viewed (e.g. a report is opened or exported)
     * 
     * @param unknown $idCourse
     * @param unknown $idUser
     */
    private static function resetCourseuserScore($idCourse, $idUser){
        /* @var $model LearningCourseuser */
        $model = LearningCourseuser::model()->findByAttributes(array(
            'idCourse'  => $idCourse,
            'idUser'    => $idUser,
        ));
        
        if ($model) {
            $model->score_given = null;
            $model->save(false);
        }
        
    }
    
    /**
     * ISO 8601 period format PnYnMnDTnHnMnS (e.g., P0Y0M2DT23H32M51S) to seconds
     * @param $timeleft duration in ISO 8601 format
     * @return array with key seconds
     */
    private static function calcFinishTime($timeleft)
    {
    	$parsed = array('Y' => 0, 'Mo' => 0, 'D' => 0, 'H' => 0, 'Mi' => 0, 'S' => 0,'seconds' =>0,'endtime' => 0);
    	$calcSec = array(12, 31, 24, 60, 60);
    	$toParse = preg_split('/T/', strtoupper($timeleft));
    	foreach($toParse as $i => $array) {
    		$split = preg_split('/([PYMDTHMS]+)/', $array, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
    		$count = count($split);
    		for($j = 0; $j < $count; $j++) {
    			if(is_numeric($split[$j])) {
    				$type = substr($split[$j +1], 0, 1);
    				if($i < 1 && $type == 'M')	$type = 'Mo';
    				elseif($type == 'M')		$type = 'Mi';
    				$parsed[$type] += (int)$split[$j];
    			}
    		}
    	}
    	$k = 0;
    	foreach($parsed as $type) {
    		$sec = 1;
    		for($j = $k; $j < 5; $j++) {
    			$sec *= $calcSec[$j];
    		}
    		$parsed['seconds'] += $sec * $type;
    		$k++;
    	}
    	$parsed['endtime'] = time() + $parsed['seconds'];
    	return $parsed;
    }
    
    
    
}