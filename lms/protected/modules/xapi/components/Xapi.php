<?php

/**
 * Helper component for xAPI module
 *
 */
class Xapi extends CComponent {
	

    const EXTRA_PARAM_LMSREG    = 'lmsreg';
    const EXTRA_PARAM_IDUSER    = 'iduser';
    
	const STATEMENT_FORMAT_EXACT 		= 'exact';
	const STATEMENT_FORMAT_IDS 			= 'ids';
	const STATEMENT_FORMAT_CANONICAL 	= 'canonical';
	const STATEMENT_FORMAT_DEFAULT 		= self::STATEMENT_FORMAT_EXACT;
	
    /**
     * Lets have some max URL length
     * 
     * @var number
     */
    const MAX_URL_LENGTH = 2000;
    
    
    /**
     * Context Activity types
     * 
     * @var string
     */
    const CONTEXT_ACTIVITY_TYPE_PARENT      = 'parent';
    const CONTEXT_ACTIVITY_TYPE_GROUPING    = 'grouping';
    const CONTEXT_ACTIVITY_TYPE_CATEGORY    = 'category';
    const CONTEXT_ACTIVITY_TYPE_OTHER       = 'other';
    
    
    public static $CONTEXT_ACTIVITY_TYPES = array(
        self::CONTEXT_ACTIVITY_TYPE_PARENT,
        self::CONTEXT_ACTIVITY_TYPE_GROUPING,
        self::CONTEXT_ACTIVITY_TYPE_CATEGORY,
        self::CONTEXT_ACTIVITY_TYPE_OTHER,
    );
    
    
	/**
	 * Min & Max versions this LRS conforms to
	 * 
	 * @var string
	 */
	const VERSION_MIN = '0.9';  // 
	const VERSION_MAX = '1.0.2';  //
	
	const VERSION_CODE_0 = null;
	const VERSION_CODE_1 = "1";
	const VERSION_CODE_2 = "2";
	
	/**
	 * Agent types (actors)
	 * @var string
	 */
	const AGENT_TYPE_AGENT 	= 'Agent';
	const AGENT_TYPE_PERSON	= 'Person';
	const AGENT_TYPE_GROUP 	= 'Group';
	
	
	/**
	 * List of ever possible status codes LRS can return, according to xAPI specification
	 * 
	 * @var number
	 */
	const HTTP_OK                      = 200;
	const HTTP_NO_CONTENT              = 204;
	const HTTP_BAD_REQUEST             = 400;
	const HTTP_UNAUTHORIZED            = 401;
	const HTTP_ACCESS_FORBIDDEN        = 403;
	const HTTP_NOT_FOUND               = 404;
	const HTTP_METHOD_NOT_ALLOWED      = 405;
	const HTTP_CONFLICT                = 409;
	const HTTP_PRECONDITION_FAILED     = 412;
	const HTTP_SERVER_ERROR            = 500;
	
	/**
	 * API Endpoints
	 * 
	 * @var string
	 */
	const API_TYPE_STATEMENTS          = 'statements';
	const API_TYPE_ACTIVITIES          = 'activities';
	const API_TYPE_ACTIVITIES_PROFILE  = 'activities_profile';
	const API_TYPE_ACTIVITIES_STATE    = 'activities_state';
	const API_TYPE_AGENTS              = 'agents';
	const API_TYPE_AGENTS_PROFILE      = 'agents_profile';
	const API_TYPE_OAUTH               = 'oauth';
	const API_TYPE_ABOUT               = 'about';
	const API_TYPE_CONTENT             = 'content';
	
	/**
	 * oAuth Endpoints
	 * 
	 * @var string
	 */
	const OAUTH_INITIATE               = 'initiate';
	const OAUTH_AUTHORIZE              = 'authorize';
	const OAUTH_TOKEN                  = 'token';

	
	/**
	 * Methods
	 * 
	 * @var string
	 */
	const METHOD_PUT                   = "put";
	const METHOD_POST                  = 'post';
	const METHOD_GET                   = "get";
	const METHOD_DELETE                = "delete";
	const PAYLOAD_TYPE_JSON            = 'json';
	const PAYLOAD_TYPE_DOC             = 'document';
	
	
	
	const OBJ_TYPE_ACTIVITY 			= 'Activity';
	const OBJ_TYPE_PERSON 				= 'Person';
	const OBJ_TYPE_STATEMENT 			= 'Statement';
	const OBJ_TYPE_AGENT 				= 'Agent';
	const OBJ_TYPE_GROUP 				= 'Group';
	const OBJ_TYPE_SUB_STATEMENT        = 'SubStatement';
	const OBJ_TYPE_STATEMENT_REF        = 'StatementRef';
	
	
	const VERB_VOIDED                   = 'http://adlnet.gov/expapi/verbs/voided';
	
	/**
	 * Holds the client version of the current request
	 * @var string
	 */
	private static $_XAPI_CLIENT_VERSION;
	
	
	public static $VERBS_09X = array(
		'experienced',
		'attended',
		'attempted',
		'completed',
		'passed',
		'failed',
		'mastered',
		'answered',
		'interacted',
		'imported',
		'created',
		'shared',
		'voided'
	);
	
	
	public static $ACTIVITY_TYPES_09X = array(
		"course",
		"module",
		"meeting",
		"media",
		"performance",
		"simulation",
		"assessment",
		"interaction",
		"cmi.interaction",
		"question",
		"objective",
		"link",
	);
	
	
	
	

	public static function setClientVersion($version) {
		self::$_XAPI_CLIENT_VERSION = $version;
	}
	
	
	public static function getClientVersion() {
		return self::$_XAPI_CLIENT_VERSION;
	}
	
	public static function getLrsVersion() {
		return self::VERSION_MAX;
	}
	
	/**
	 * List of possible Inverse Functional Identifier (IFI)
	 * 
	 * @var array
	 */
	protected $possibleIfis = array('mbox', 'mbox_sha1sum', 'account', 'openid');
	
	
	
	/**
	 * Array of supported OAuth scope values.
	 */
	protected $supportedScopes = array(
	    'statements/write',        // write any statement
	    'statements/read/mine',    // read statements written by me (current token?? )
	    'statements/read',         // read any statements
	    'state',                   // read/write state data, limited to activities and actors associated with the current token
	    'define',                  // (re) Define activities and actors.
	    'profile',                 // read/write profile data, limited to activities and actors associated with the current token
	    'all/read',                // unrestricted read access
	    'all',                     // unrestricted access
	);
	
	
	/**
	 * HTTP statuses
	 * 
	 * @var array of Code => Status
	 */
	public static $HTTP_STATUS = array (
			self::HTTP_OK                    => 'HTTP/1.1 200 OK',
			self::HTTP_NO_CONTENT            => 'HTTP/1.1 204 No Content',
			self::HTTP_BAD_REQUEST           => 'HTTP/1.1 400 Bad Request',
			self::HTTP_UNAUTHORIZED          => 'HTTP/1.1 401 Unauthorized',
			self::HTTP_ACCESS_FORBIDDEN      => 'HTTP/1.1 403 Access Forbidden',
			self::HTTP_NOT_FOUND             => 'HTTP/1.1 404 Not Found',
			self::HTTP_METHOD_NOT_ALLOWED    => 'HTTP/1.1 405 Method Not Allowed',
			self::HTTP_CONFLICT              => 'HTTP/1.1 409 Conflict',
			self::HTTP_PRECONDITION_FAILED   => 'HTTP/1.1 412 Precondition Failed',
			self::HTTP_SERVER_ERROR          => 'HTTP/1.1 500 Internal server error' 
	);

	
	/**
	 * 
	 * @param string $code
	 */
	public static function getHttpStatusText($code) {
	    return self::$HTTP_STATUS[$code];
	}
	
	
	/**
	 * Finish the LRS work and send back status code and content, if any
	 *  
	 * @param string $code
	 * @param string $contents
	 * @param string $json
	 */
    public static function out($code = null, $contents=null, $json = false) {
        if ( $code ) {
            if ( array_key_exists($code, self::$HTTP_STATUS) ) {
                self::header(null, self::$HTTP_STATUS[$code]);
            }
        }
        if ( $contents ) {
            if ( $json === true ) {
                $contents = CJSON::encode($contents);
            }
            echo $contents;
        }
        Yii::app()->end();
    }
    
    /**
     * Send a header ('Name: Value'  -or-  'Value')
     *
     * @param string $name
     * @param string $value
     */
    public static function header($name, $value) {
        header($name . (!empty($name) ? ": " : "") . $value);
    }
    
    
    /**
     * Wrapper, for easier usage, to get a GET parameter
     * 
     * @param string $name
     * @param mixed $defaultValue
     */
    public static function getGetParam($name, $defaultValue=null) {
        return Yii::app()->request->getQuery($name, $defaultValue);
    }
    
    
    /**
     * Wrapper, for easier usage, to get a POST parameter
     *
     * @param string $name
     * @param mixed $defaultValue
     */
    public static function getPostParam($name, $defaultValue=null) {
        return Yii::app()->request->getPost($name, $defaultValue);
    }
    
    /**
     * Wrapper, for easier usage, to get a GET/POST parameter
     *
     * @param string $name
     * @param mixed $defaultValue
     */
    public static function getParam($name, $defaultValue=null) {
        return Yii::app()->request->getParam($name, $defaultValue);
    }
 
    
    /**
     * Wrapper around Yii::t()
     * 
     * @param unknown $message
     * @param unknown $params
     * @param string $source
     * @param string $language
     */
    public static function t($message, $params=array(), $source=null, $language=null) {
        return Yii::t('xapi', $message, $params, $source, $language);
    }
    
    /**
     * 
     * @param unknown $message
     * @param string $level
     */
    public static function log($message, $level='info') {
        Yii::log(print_r($message, true), $level, 'XAPI');
    }
    
    
    
    /**
     * Prepare common RAW data for saving in DB
     *
     * @param array 
     */
    public static function dataDumpForDb($arr) {
        $json_data = CJSON::encode($arr);
        $hash_data = sha1($json_data);
        $json_data = $json_data;
        $res = array(
            'json_data' => $json_data,
            'hash_data' => $hash_data,
        );
        return $res;
    }
    
    
    /**
     * Make a JSON-ed value (for TEXT DB fields) out of a named array item, which is an array!
     * 
     * @param array Array of data
     * @param string $property_name Array key
     */
    public static function jsonPropertyForDb($arr, $property_name) {
    
        if (isset($arr[$property_name]) ) {
            $json_data = CJSON::encode($arr[$property_name]);
        }
        else {
            $json_data = "";
        }
    
        $json_data_escaped = $json_data;
    
        return $json_data_escaped;
    
    }
    
    
    
    public static function iriToUri($string) {
        
        static $non_ascii;
        if (!$non_ascii)
        {
            $non_ascii = implode('', range("\x80", "\xFF"));
        }
        
        $position = 0;
        $strlen = strlen($string);
        while (($position += strcspn($string, $non_ascii, $position)) < $strlen)
        {
            $string = substr_replace($string, sprintf('%%%02X', ord($string[$position])), $position, 1);
            $position += 3;
            $strlen += 2;
        }
        
        return $string;
        
    }
    
 

    /**
     * Get internal version coding, integer value, for better indexing.
     * 
     * These codes are mainly used to identify/distinguish major Database Structure and data format versions, NOT XAPI versions.
     * The idea is that xAPI specification may evolve (change), but DB remain the same, in which case
     * different xAPI versions will have the same internal CODE. 
     * 
     * @param string $version Semantic versioning string
     */
    public static function getVersionCode($version = self::VERSION_MAX) {
        static $cache;
        
        if (isset($cache[$version])) return $cache[$version]; 
        
        if      (version_compare($version, "0.95", ">=") && version_compare($version, "1.1.0", "<")) $cache[$version] = self::VERSION_CODE_1;
        else if (version_compare($version, "1.1.0", ">=") && version_compare($version, "1.2.0", "<")) $cache[$version] = self::VERSION_CODE_2;
        else $cache[$version] = self::VERSION_CODE_0;
        
        return $cache[$version];
        
    }
    

    /**
     * Get internal version coding of the Client of the current request
     */
    public static function getClientVersionCode() {
    	return self::getVersionCode(self::$_XAPI_CLIENT_VERSION);
    }
    

    /**
     * Get internal version coding of the Client of the current request
     */
    public static function getLrsVersionCode() {
    	return self::getVersionCode(self::VERSION_MAX);
    }
    
    /**
     * Check if a string is a valid JSON
     * 
     * @param string $string
     * @return boolean
     */
    public static function isJson($string) {
    	return is_string($string) && is_object(json_decode($string)) ? true : false;

    	// Here is another, faster but not so good approach
    	//return preg_match('/^\s*[\[{]/', $string) > 0;
    }
    
    
    /**
     * Concurency Etag check
     *  
     * @param string $currentContent
     * @param string $newContent
     * @param array $postedData
     * 
     */
    public static function checkETag($currentContent, $newContent, $postedData) {
        
        if (!$currentContent) return true;
    	
    	$ifMatch 		= $postedData['ifMatch'];
    	$ifNoneMatch	= $postedData['ifNoneMatch'];
    	
		$etag = self::makeETag($currentContent);  // sha1 of the current content
		
		// Both null and we have old/current content? That means there is no concurency check! Issue a conflict!
        if ( !$ifMatch && !$ifNoneMatch) {  
            Xapi::out(self::HTTP_CONFLICT, "xAPI consumer MUST 1) Check the current state of the resource and 2) Set the \"If-Match\" header with the current ETag to resolve the conflict");
        }
        
		// Posteg ETag differs from current contents' Etag: do not proceed, something went wrong between consequent calls
        if ($ifMatch && ($ifMatch != $etag)) {
            Xapi::out(self::HTTP_PRECONDITION_FAILED);
        }

        // If client requested to proceed ONLY if the current content is missing, reject if it does
        if ($ifNoneMatch && ($ifNoneMatch == '*')) {
            Xapi::out(self::HTTP_PRECONDITION_FAILED);
        }
        
        // Everything is ok, return true
		return true;    	 
    	
    }

    
    public static function makeETag($value) {
    	return '"' . sha1($value) . '"';
    }
    
    
    public static function isVersionSupported($version) {
        if (version_compare($version, Xapi::VERSION_MAX, "<=") && version_compare($version, Xapi::VERSION_MIN, ">=")) {
            return true;
        }
        return false;
    }
    

    /**
     * Check if a string is a valid URI
     * 
     * @param string $string
     * @return boolean
     */    
    public static function isUri($string) {
        $scheme = parse_url($string, PHP_URL_SCHEME);
        if (!is_string($scheme)) return false;
        return true;
    }
    
    
    public static function getAllowedApiTypes() {
        
        $allowedApiTypes = array(
            self::API_TYPE_ABOUT,
            self::API_TYPE_ACTIVITIES,
            self::API_TYPE_ACTIVITIES_PROFILE,
            self::API_TYPE_ACTIVITIES_STATE,
            self::API_TYPE_AGENTS,
            self::API_TYPE_AGENTS_PROFILE,
            self::API_TYPE_CONTENT,
            self::API_TYPE_OAUTH,
            self::API_TYPE_STATEMENTS,
        );
        
        return $allowedApiTypes;
        
    }
    
}