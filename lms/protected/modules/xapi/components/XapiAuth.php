<?php
/**
 * Singleton pattern class
 * http://en.wikipedia.org/wiki/Design_pattern_(computer_science)
 * http://en.wikipedia.org/wiki/Singleton_pattern
 *
 * tldr; Ensure we have one global Auth/User instance of this object to access authenticated user data
 *
 * Usage:
 *     XapiAuth::getInstance()->authenticate()
 *     XapiAuth::getInstance()->user
 *
 * @author Plamen
 *
 */
class XapiAuth {
    
    
    const BASIC_AUTHENTICATION         = "Basic";
    const OAUTH_AUTHENTICATION         = "OAuth";
    const NO_AUTHENTICATION            = "none";
    
    const SCOPE_ALL                    = "all";
    const SCOPE_STATEMENT_WRITE        = "statement/write";
    const SCOPE_STATEMENT_READ_MINE    = "statement/read/mine";
    const SCOPE_STATEMENT_READ         = "statement/read";
    const SCOPE_STATE                  = "state";
    const SCOPE_DEFINE                 = "define";
    const SCOPE_PROFILE                = "profile";
    const SCOPE_ALL_READ               = "all/read";
    
    // Basic Auth has no Scope concept, so set granted scopes to array("all")
    public static $SCOPE_DEFAULT_BASIC_AUTH     = array(self::SCOPE_ALL);
    
    // Set granted scopes to this when OAuth does not provide scope resolution
    public static $SCOPE_DEFAULT_OAUTH          = array(self::SCOPE_ALL);   
    
    // Used in several places to indicate the direction of the operation
    const OPERATION_READ               = "read";
    const OPERATION_WRITE              = "write";
    
    // Hold the authentication type used
    public $authentication_type = "";
    
    // Hold authenticated user LMS/LRS data/information (array)
    public $user = array();
    
    // Hold an instance of Docebo Database object
    private $db = null;
    
    // Hold an instance of this object
    private static $_instance = null;
    
    /**
     * Set to true to skip authorization (for testing purposes, for example)
     *
     * @var boolean
     */
    public static $skip_authorization = true;
    
    
    /**
     * Returns an instance of the class (creates new if does not exist yet)
     *
     * @return XapiAuth
     */
    public static function getInstance() {
    
        $className=__CLASS__;
    
        if(isset(self::$_instance)) {
            return self::$_instance;
        }
        else {
            $instance = self::$_instance = new $className(null);
            return $instance;
        }
    }
    
    
    
    /**
     * Constructor
     */
    public function __construct() {
    }
    
    
    /**
     * Detect the authentication method
     */
    public function detectAuthMethod($reqData){
        
        $authroziationHeader = "";
        foreach ($reqData as $key => $value) {
            if (strtolower($key) == "authorization") {
                $authroziationHeader = $value;
            }
        }
        
        if ( preg_match('/Basic/i', $authroziationHeader )) {
            return self::BASIC_AUTHENTICATION;
        }
        else if (preg_match('/Bearer/i', $authroziationHeader )) {
            return self::OAUTH_AUTHENTICATION;
        }
        return self::NO_AUTHENTICATION;
    }
    
    
    
    /**
     * Authenticate the Call
     *
     */
    public function authenticate($reqData) {
    
        $result = array('success' => false);
    
        //Detect Auth method and keep it in a class property
        $this->authentication_type = $this->detectAuthMethod($reqData);
        
        
        // Do the authentication
        if ( $this->authentication_type == self::OAUTH_AUTHENTICATION ) {
            $result = $this->authenticateOauth($reqData);
        }
        else if ( $this->authentication_type == self::BASIC_AUTHENTICATION ) {
            $result = $this->authenticateBasic($reqData);
        }
    
        // If we have a valid/authenticated user...
        if ( $result['success'] == true ) {
    
            // Get list of subscribed Registrations, i.e. courses, in LMS
            $regsArr = $this->getLmsUserRegistrations($result['data']['idst']);
            $result['data']['registrations'] = $regsArr;
             
            // Make an "Actor" object out of User data
            $result['data']['inferred_actor'] = LrsAgent::makeActorFromLmsUserData($result['data']);
             
            // Keep user data in class attribute
            $this->user = $result['data'];
    
        }
        else {
            Xapi::log("User authentication failed");
        }
    
        return $result;
    
    }
    
    
    
    /**
     * Authenticate using Basic method
     *
     */
    public function authenticateBasic($reqData) {
    
        $result = array('success' => false, 'data' => array());
    
        // "Authorization" is populated during data collection in controller
        // Authorization: Basic 123456ABDCD761256152BNMBMSBD576572453234
        $temp = preg_split('/ /', $reqData["Authorization"]);

        if (empty($temp[1])) {
            Xapi::out(Xapi::HTTP_BAD_REQUEST, "Invalid authorization header");
        }
        
        $temp = base64_decode($temp[1]);
        $temp = preg_split('/:/', $temp);
        
        if (empty($temp[0]) || empty($temp[1])) {
            Xapi::out(Xapi::HTTP_BAD_REQUEST, "Invalid authorization header");
        }
    
        $username = $temp[0];
        $password = $temp[1];
    
        $result = array('success' => false, 'data' => array());
        
        // Now, this is the AUTHENTICATED user
        $authenticatedUuserModel = CoreUser::model()->findByAttributes(array('userid' => $username, 'pass' => $password));
        
        // But if we got an extra User ID (integer) in the URL, we use THAT ID as the actual user (to track), like a proxy
        if (isset($reqData[Xapi::EXTRA_PARAM_IDUSER])) {
            $userModel = CoreUser::model()->findByPk($reqData[Xapi::EXTRA_PARAM_IDUSER]);
        }
        else {
            $userModel = $authenticatedUuserModel;
        }
        
        if (!$userModel) {
            Xapi::out(Xapi::HTTP_UNAUTHORIZED);
        }
        
        $row = $userModel->attributes;
        $row['authorized_scopes'] = self::$SCOPE_DEFAULT_BASIC_AUTH;
        $result = array('success' => true, 'data' => $row);
        
        return $result;
    }
    
    
    
    /**
     * Authenticate using OAuth method
     *
     */
    public function authenticateOauth($reqData) {
        
        $temp  = preg_split('/ /', $reqData["Authorization"]);
        $token = $temp[1];
        
        $result = array('success' => false, 'data' => array());
        if (!$token) {
            return $result;
        }

        $oauthServer = DoceboOauth2Server::getInstance();
        $oauthServer->init();

        //if (!$oauthServer->verifyResourceRequest('all')) {
        //    return $result;
        //}
        
        
        $userModel = $oauthServer->getAuthorizedUser();
        if (!$userModel) {
            return $result;
        }

        $data = $userModel->attributes;
        $data['authorized_scopes'] = array(self::SCOPE_ALL);
        $result['data'] = $data;
        
        $result['success'] = true;
        
        return $result;
    }
    
    
    
    /**
     * Authorize writing statements
     *
     * @param array $statementArr
     */
    public function authorizeStatementWrite($statementArr) {
    
        if ( self::$skip_authorization ) {
            return true;
        }
    
        // Scope/permissions check
        $operation_scopes = array(self::SCOPE_STATEMENT_WRITE);
        if ( ! $this->isAnyScopeGranted($operation_scopes) ) {
        	return false;
		}
    
        return true;
        
    }
    
    
    /**
     * Authorize STATE/read/write operation.
     * 
     * @param string $operation (see self::$OPERATION_XXXX constants)
     * @param array $agentArr
     * @param string $activityId
     * @param string $registration
     */
    public function authorizeStateOperation($operation, $agentArr, $activityId, $registration=null) {
    
        if ( self::$skip_authorization ) {
            return true;
        }
    
        // This LRS policy requires "registration" to be attached to requests
        if ( ! $registration ) {
            return false;
        }
    
        // Authorize by registration
        //@TODO AUTH ISSUE
        if ( ! in_array($registration, $this->user['registrations']) ) {
            return false;
        }
    
        $oper_scopes = array();
        if ( $operation == self::OPERATION_WRITE ) {
            $oper_scopes = array(self::SCOPE_STATE);
        }
        else if ( $operation == self::OPERATION_READ ) {
            $oper_scopes = array(self::SCOPE_STATE, self::SCOPE_ALL_READ);
        }
    
        if ( ! $this->isAnyScopeGranted($oper_scopes) ) {
            return false;
        }
    
        return true;
    
    }
    
    
    /**
     * Authorize PROFILE/read/write operation.
     * @param string $operation (see self::$OPERATION_XXXX constants)
     * @param array $agentArr
     * @param string $activityId
     * @param string $registration
     */
    public function authorizeProfileOperation($operation, $activity_id=null, $actorArr=null) {
    
        if ( self::$skip_authorization ) {
            return true;
        }
    
        // Authorize by scope
        $oper_scopes = array();
        if ( $operation == self::OPERATION_WRITE ) {
            $oper_scopes = array(self::SCOPE_PROFILE);
        }
        else if ( $operation == self::OPERATION_READ ) {
            $oper_scopes = array(self::SCOPE_PROFILE, self::SCOPE_ALL_READ);
        }
    
        if ( ! $this->isAnyScopeGranted($oper_scopes) ) {
            return false;
        }
    
        return true;
    
    }
    
    
    
    /**
     * Read User record from LMS database, based on User id (idst)
     *
     * @param int $idst
     */
    public function getLmsUserRecord($idst) {
    
        $result = false;
        
        $userModel = CoreUser::model()->findByPk((int) $idst);
        
        if ($userModel) {
            $result = $userModel->attributes;
        }
    
        return $result;
    }
    
    
    
    
    /**
     * Retrieve list of "registrations" (courses) the authenticated user is subscribed to
     *
     * @param int $idst  User Id, as per LMS database
     */
    public function getLmsUserRegistrations($idst) {
    
        $command = Yii::app()->db->createCommand();
        
        $command->select('*, tc_ap.registration as registration')
            ->from("learning_tc_ap tc_ap")
            ->join("learning_tc_activity tc_activity", "tc_activity.id_tc_ap=tc_ap.id_tc_ap")
            ->join("learning_organization org", "tc_activity.id_tc_activity=org.idResource")
            ->join("learning_courseuser lcu", "org.idCourse=lcu.idCourse")
            ->where("org.objectType='tincan'")
            ->andWhere("lcu.idUser=:idst");

        $params[':idst'] = (int) $idst;
        
        $rows = $command->queryAll(true, $params);
        $regsArr = array();
        
        foreach ($rows as $row) {
            $regsArr[] = $row['registration'];
        }
        
        return $regsArr;
    }
    
    
    
    /**
     * Checks if a given scope is granted to current authenticated user
     * @param string
     */
    public function isScopeGranted($scope) {
    
        // No scope given? What do you want to check anyway? Return false;
        if ( empty($scope) ) {
            return false;
        }
    
        $auth_scopes = isset($this->user['authorized_scopes']) ? $this->user['authorized_scopes'] : array();
    
        $result = in_array($scope, $auth_scopes);
    
        return 	$result;
    }
    
    
    /**
     * Check if ALL of the scopes are granted
     * @param array $scopes Array of strings
     */
    public function areAllScopesGranted($scopes) {
    
        // Non-array or empty array is bogus (what scope you want to check anyway?), return false
        if ( !is_array($scopes) || (count($scopes) <= 0) ) {
            return false;
        }
    
        $auth_scopes = isset($this->user['authorized_scopes']) ? $this->user['authorized_scopes'] : array();
    
        foreach ( $scopes as $scope ) {
            if ( ! in_array($scope, $auth_scopes) ) {
                return false;
            }
        }
    
        return true;
    }
    
    
    /**
     * Check if ANY of the scopes is granted
     * 
     * @param array $scopes Array of strings
     */
    public function isAnyScopeGranted($scopes) {
    
        // User is authorized for these scopes:: 
        $userAuthorizedScopes = isset($this->user['authorized_scopes']) ? $this->user['authorized_scopes'] : array();
        
        // If user has an authorization for "all scopes", no need to check, he is a GOD, he can do anything!
        if (in_array(self::SCOPE_ALL, $userAuthorizedScopes)) {
            return true;
        }
        
        // Bad chcek ?!
        if ( empty($scopes) ) {
            throw new XapiException("Invalid operation scope(s) check.", Xapi::HTTP_SERVER_ERROR);
        }

        $intersect = array_intersect($userAuthorizedScopes, $scopes);
        if ( !empty($intersect) ) return true;
        
        return false;
    }
    
    
    
}
 