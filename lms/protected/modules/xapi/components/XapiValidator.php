<?php
/**
 * Helper component to validate various XAPI objects
 *        
 */
class XapiValidator extends CComponent {

    
    /**
     * IFI: Account Object
     * @param array $dataArr
     */
    public static function validateAccountObject($dataArr) {
        
        $versionCode = Xapi::getClientVersionCode();
        
        switch ($versionCode) {
        
            case Xapi::VERSION_CODE_0:
                if (!isset($dataArr['account'])) return false;
                if (!is_array($dataArr['account'])) return false;
                foreach ($dataArr['account'] as $account) {
                    if ( !isset($account['accountServiceHomePage']) || empty($account['accountServiceHomePage']) ) return false;
                    if ( !isset($account['accountName']) || empty($account['accountName']) ) return false;
                    if ( !Xapi::isUri($account['accountServiceHomePage'])) return false;
                }
                break;
        
            case Xapi::VERSION_CODE_1:
            default:
                if ( !isset($dataArr['account']) || !isset($dataArr['account']['name']) || !isset($dataArr['account']['homePage'])) return false;
                if ( !is_string($dataArr['account']['name']) || !is_string($dataArr['account']['homePage'])) return false;
                if ( !Xapi::isUri($dataArr['account']['homePage'])) return false;
                break;
        
        }
        
        
    
        return true;
    }
    
    /**
     * IFI: OpenID
     * 
     * @param array $dataArr
     */
    public static function validateOpenId($dataArr) {
        
        $versionCode = Xapi::getClientVersionCode();
        
        switch ($versionCode) {
        
            case Xapi::VERSION_CODE_0:
                if ( !isset($dataArr['openid']) || !is_array($dataArr['openid'])) return false;
                
                foreach ($dataArr['openid'] as $openid) 
                    if ( !is_string($dataArr['openid']) || !Xapi::isUri($dataArr['openid'])) return false;
                
                break;
        
            case Xapi::VERSION_CODE_1:
            default:
                if ( !isset($dataArr['openid']) || !is_string($dataArr['openid'])) return false;
                if ( !Xapi::isUri($dataArr['openid'])) return false;
                break;
        
        }
        
        return true;
    }
    

    /**
     * IFI: Mbox
     * @param array $dataArr
     */
    public static function validateMbox($dataArr) {

        $versionCode = Xapi::getClientVersionCode();
        
        switch ($versionCode) {
            
            case Xapi::VERSION_CODE_0:
                if (!isset($dataArr['mbox']) || !is_array($dataArr['mbox'])) return false;
                
                foreach ($dataArr['mbox'] as $mailto) {
                    $parts = preg_split('/:/', $mailto);
                    if (!strtolower($parts[0]) == 'mailto') return false;
                    if (empty($parts[1])) return false;
                    if (!self::validateEmail($parts[1])) return false;
                }
                
                break;
            
            case Xapi::VERSION_CODE_1:
            default:
                if (!isset($dataArr['mbox']) || !is_string($dataArr['mbox'])) return false;
                
                $parts = preg_split('/:/', $dataArr['mbox']);
                if (!strtolower($parts[0]) == 'mailto') return false;
                if (empty($parts[1])) return false;
                if (!self::validateEmail($parts[1])) return false;
                
                break;
                
        }
        
		return true;
    }
    
    
    /**
     * IFI: Mbox SHA1 Sum
     * @param array $dataArr
     */
    public static function validateMboxSha1Sum($dataArr) {
        
        $versionCode = Xapi::getClientVersionCode();
        
        switch ($versionCode) {
        
            case Xapi::VERSION_CODE_0:
                if (!isset($dataArr['mbox_sha1sum']) || !is_array($dataArr['mbox_sha1sum'])) return false;
                break;
        
            case Xapi::VERSION_CODE_1:
            default:
                if (!isset($dataArr['mbox_sha1sum']) || !is_string($dataArr['mbox_sha1sum'])) return false;
                break;
        
        }
        
        
        return true;
    }
    
    
    /**
     * Validate a whole Agent Object 
     * 
     * @param array $dataArr
     */
    public static function validateAgent($dataArr) {
        
        $okType    = !isset($dataArr['objectType']) || (isset($dataArr['objectType']) && in_array($dataArr['objectType'], LrsAgent::$AGENT_TYPES) );
        $okIfi     = self::validateAccountObject($dataArr) || self::validateOpenId($dataArr) || self::validateMbox($dataArr) || self::validateMboxSha1Sum($dataArr);
        
        // GROUP agent must have member property
        if ( isset($dataArr['objectType']) && (($dataArr['objectType'] == Xapi::OBJ_TYPE_GROUP) && (!isset($dataArr['member']) || !is_array($dataArr['member']))) ) return false;
        
        if ($okType && $okIfi) {
            return true;
        }
        
        return false;
        
    }
    
    /**
     * @param array $dataArr
     */
    public static function validateAnonymousGroup($dataArr) {
        
        $versionCode = Xapi::getClientVersionCode();
        
        switch ($versionCode) {
        
            case Xapi::VERSION_CODE_0:
                return true;
                break;
        
            case Xapi::VERSION_CODE_1:
            default:
                $okType    = isset($dataArr['objectType']) && ($dataArr['objectType'] == Xapi::AGENT_TYPE_GROUP);
                $okMember  = isset($dataArr['member']) && is_array($dataArr['member']);
                if ($okType && $okMember)
                    return true;
                break;
        
        }
        
        return true;
        
    }
    
    /**
     * @param array $dataArr
     */
    public static function validateIdentifiedGroup($dataArr) {
        
        $versionCode = Xapi::getClientVersionCode();
        
        
        switch ($versionCode) {
        
            case Xapi::VERSION_CODE_0:
                return true;
                break;
        
            case Xapi::VERSION_CODE_1:
            default:
                $okType    = isset($dataArr['objectType']) && ($dataArr['objectType'] == Xapi::AGENT_TYPE_GROUP);
                $okIfi     = self::validateAccountObject($dataArr) || self::validateOpenId($dataArr) || self::validateMbox($dataArr) || self::validateMboxSha1Sum($dataArr);
                if ($okType && $okIfi)
                    return true;
                break;
        
        }
        
        return true;
        
    }
    
 

    /**
     * 
     * @param string $verb IRI
     */
    public static function validateVerb($verb) {

        $versionCode  =Xapi::getClientVersionCode();
        
        if ($versionCode === Xapi::VERSION_CODE_0) {
            if (!is_string($verb)) return false;
            if (preg_match('/\s/', $verb)) return false;
        }
        else {
            if (empty($verb['id'])) return false;
            if (!is_string($verb['id'])) return false;
            if (preg_match('/\s/', $verb['id'])) return false;
            if (!Xapi::isUri($verb['id'])) return false;
        }
        return true;
    }
    
    
    /**
     * Validate data.
     * @param array $dataArr
     */
    public static function validateStatement($dataArr) {
        $result = true;
    
        
        $versionCode  =Xapi::getClientVersionCode();
        
        if ($versionCode === Xapi::VERSION_CODE_0) return true;
        
        if (isset($dataArr['statementId'])) {
            if(!self::validateUUID($dataArr['statementId'])) {
                Xapi::out(Xapi::HTTP_BAD_REQUEST, "Invalid UUID");
            }
        }
    
        if(isset($dataArr['verb'])) {
            if(!self::validateVerb($dataArr['verb'])) {
                Xapi::out(Xapi::HTTP_BAD_REQUEST, "Invalid Verb");
            }
        }
    
        if ( isset($dataArr['timestamp']) ) {
            if ( ! self::validateISO8601Timestamp($dataArr['timestamp'])) {
                Xapi::out(Xapi::HTTP_BAD_REQUEST, "Invalid Timestamp");
            }
        }
    
        if ( isset($dataArr['object']['objectType']) && ($dataArr['object']['objectType'] == Xapi::OBJ_TYPE_STATEMENT) ) {
            if ( $dataArr['verb'] != Xapi::VERB_VOIDED ) {
                Xapi::out(Xapi::HTTP_BAD_REQUEST, "Statement object type is allowed in Voiding statements only.");
            }
        }
    
        if (!isset($dataArr['verb']) || !isset($dataArr['actor']) || !isset($dataArr['object'])) {
            Xapi::out(Xapi::HTTP_BAD_REQUEST, "Statement must contain at least Actor, Verb and Object");
        }
        
        return $result;
    }
    
 
    public static function validateFetchStatementsFilter($data) {
        
        $versionCode = Xapi::getClientVersionCode();
        
        // Related_XX must be BOOLEAN
        if ( isset($data['related_activities'])) {
            if ($data['related_activities'] != 'true' && $data['related_activities'] != 'false') return false;
        }
        
        if ( isset($data['related_agents'])) {
            if ($data['related_agents'] != 'true' && $data['related_agents'] != 'false') return false;
        }
        
        // Reject requests where Statement ID OR Voided Statement ID is specified and
        // parameters OTHER than "attachments" or "format" is provided.
        // That means, if a single Statement is requested, don't ask for anything eles, other than those two allowed parameters
        $tmpDisallowedForSingle = array(
            "agent",
            "actor",
            "verb",
            "activity",
            "registration",
            "related_activities",
            "related_agents",
            "since",
            "until",
            "limit",
            "ascending",
        );
        if (isset($data['statementId']) || isset($data['voidedStatementId'])) {
            foreach ($tmpDisallowedForSingle as $key) {
                if (isset($data[$key])) return false;
            }
        }

        // If Statement ID is set.. and..
        if (isset($data['statementId'])) {
            $reject = false;
            if (isset($data['since']) || isset($data['until'])) $reject = true;
            if (isset($data['voidedStatementId'])) $reject = true;
            if ($reject) return false;
        }
        
        // Verb must be valid
        if ( !empty($data['verb']) ) {
            if (!self::validateVerb($data['verb'])) return false;
        }

        // Activity must be Valid URI (v1)
        if ( !empty($data['activity']) && $versionCode >= Xapi::VERSION_CODE_1) {
            if (!Xapi::isUri($data['activity'])) return false;
        }
        
        // Agent must be Valid
        if ( !empty($data['agent']) ) {
            if (!self::validateAgent($data['agent'])) return false;
        }
        
        return true;
        
        
    }
    
    
    public static function validateUUID($uuid) {
        if(preg_match('/^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$/', $uuid)) {
            return true;
        }
        return false;
    }
 
    
    /**
     * See: https://en.wikipedia.org/wiki/ISO_8601#Dates
     * Example: 2012-08-26T18:12:17.272Z
     */
    public static function validateISO8601Timestamp($date)
    {
    	try {
    		if (!preg_match('/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})/', $date, $parts) == true) {
    			return false;
    		}
    		$dt = new DateTime($date);
    		return true;
    	}
    	catch (Exception $e) {
    		return false;
    	}
    }
    
    
    /**
     * Validate Context data
     * 
     * @param array $dataArr
     */
    public static function validateContext($dataArr) {
        return true;    
    }
    
    
    public static function validateEmail($email) {
        
        // !!! Note: abc@localhost does not validate!!
        
        // Email with NO name part
        $pattern = '/^[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/';
        
        // For emails WITH name part
        $fullPattern='/^[^@]*<[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?>$/';
        
        if ( !preg_match($pattern, $email) && !preg_match($fullPattern, $email) ) return false;
        
        return true;
        
    }
    

    /**
     * 
     * @param unknown $dataArr
     */
    public static function validateActivity($dataArr) {
        return LrsActivity::validateActivity($dataArr);
    }
 
    
    /**
     * 
     * @param unknown $registration
     */
    public static function validateRegistration($registration) {
        if (preg_match('/\s/', $registration)) return false;
        return true;
    }
    

    /**
     * 
     * @param unknown $format
     */
    public static function validateFetchStatementsFormat($format) {
        return in_array($format, array(Xapi::STATEMENT_FORMAT_CANONICAL, Xapi::STATEMENT_FORMAT_DEFAULT, Xapi::STATEMENT_FORMAT_EXACT, Xapi::STATEMENT_FORMAT_IDS));
    }
    
}