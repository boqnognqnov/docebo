<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, target-densitydpi=160dpi, minimal-ui">
</head>



<body class="docebo">

	<div class="container">
		<div id="maincontent" class="inner-container">
			<div id="content">
				<?php echo $content; ?>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>		

</body>



<?php
echo $content;