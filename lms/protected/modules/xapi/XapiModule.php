<?php

class XapiModule extends CWebModule {

	private $_assetsUrl;
	
	public function init()
	{
		// import the module-level models and components
		$this->setImport(array(
		    'xapi.models.*',
		    'xapi.components.*',
		));
		
		
	}

	public function beforeControllerAction($controller, $action)
	{
		
		// Set our own error handler
		set_error_handler('exceptions_error_handler');
		
		if(parent::beforeControllerAction($controller, $action))
		{
			return true;
		}
		else
			return false;
	}
	
	
	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('xapi.assets'));
		}
		return $this->_assetsUrl;
	}
	
	
	
}




/**
 * Handle PHP errors. We need this to catch all those undefined indexes.
 * Tincan API is fuzzy and blury, plenty of things MAY or may NOT be sent, etc.
 * We need to catch all Warnings and Notices!
 *
 * @param integer $errorno
 * @param string $message
 * @param string $filename
 * @param integer $lineno
 * @throws ErrorException
 */
function exceptions_error_handler($errorno, $message, $filename, $lineno) {
	if (error_reporting() == 0) {
		return;
	}
	if (error_reporting() & $errorno) {
		throw new ErrorException($message, 0, $errorno, $filename, $lineno);
	}
}

