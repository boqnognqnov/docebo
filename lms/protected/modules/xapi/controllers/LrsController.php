<?php
/**
 * 
 * Main LRS controller
 * 
 * @TODO  READ AGAIN THE WHOLE XAPI SPEC!!!
 *
 */
class LrsController extends XapiBaseController
{
    
    /**
     * Version of client
     * 
     * @var string
     */
    protected $_client_version;

    /**
     * Contains all incoming data as an assoc array (grand collection: everything incoming through the request).
     *
     * @var array
     */
    protected $_data = array();
    
    
    /**
     * List here ALL possible GET parameter names.
     * Their values will be extracted from URL string and put as
     * $this->_data['get-param-name']. Of course $_GET/$_POST/$_REQUEST
     * are still there at your disposal :-)
     *
     * @var array
     */
    protected $_possibleParamNames = array(
        // XAPI endpoint type (statements, activity, etc., see Xapi::API_TYPE_*)
        'type',
        
        // XAPI provided
        'statementId',
        'activityId',
        'stateId',
        'profileId',
        'verb',
        'object',
        'registration',
        'context',
        'actor',
        'agent',
        'since',
        'until',
        'limit',
        'offset',
        'authoritative',
        'sparse',
        'instructor',
        'method',
        'ascending',
   		'activity',
    	'voidedStatementId',	
    	'related_activities',
    	'related_agents',
    	'format',		
    	'attachments',
    
        // oAuth related (fill it up please)
        'endpoint',
        'oauth_callback',
        'oauth_consumer_key',
        'consumer_name',
        'oauth_timestamp',
        'oauth_nonce',
        'oauth_signature_method',
        'oauth_signature',
        'oauth_token',
        'scope'
    
    );
    
    
    protected $_possibleHeaderNames = array(
        "Authorization",
        "authorization",
        "Origin",
        "User-Agent",
        "Accept",
        "Accept-Language",
    	"X-Experience-API-Version",
        "x-experience-api-version",
    );
    
    
    
    /**
     * Check if we've got OPTIONS browser Preflight request or a real request.
     * In case of OPTIONS we tell the browser we allow OR origins and other stuff.
     * 
     */
    protected function checkOrigin() {
        
        // Handle Browser Preflight
        if (strtolower($_SERVER["REQUEST_METHOD"]) == "options") {
            $origin = $_SERVER["HTTP_ORIGIN"];
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Methods: GET,PUT,DELETE,POST,OPTIONS');
            header('Access-Control-Allow-Headers: Content-Type,Authorization,origin,X-Experience-API-Version');
            header('Access-Control-Allow-Credentials: true');
            header('X-Experience-API-Version: ' . Xapi::VERSION_MAX);
            header('Content-Type: text/plain');
            Xapi::out(Xapi::HTTP_OK);
        } else {
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Methods: GET,PUT,DELETE,POST,OPTIONS');
            header('Access-Control-Allow-Headers: Content-Type,Authorization,origin,X-Experience-API-Version');
            header('Access-Control-Allow-Credentials: true');
            header('X-Experience-API-Version: ' . Xapi::VERSION_MAX);
        }
        
    }
    

    /**
     * 
     * @param unknown $type
     */
    protected function checkApiType($type) {
        return in_array($type, Xapi::getAllowedApiTypes());
    }
    
    
    /**
     * Check the client version and if version is not among supported, send back BAD REQUEST
     */
    protected function checkVersion() {
    	
    	$incomingVersion = isset($this->_data["X-Experience-API-Version"]) ? $this->_data["X-Experience-API-Version"] : "0.9" ;

    	// If Activity Provider is set, we use it to force the incmoning "client version"
    	// This request parameter is set when an "internal" request is made from LMS for reporting purposes
    	// In that case, the TinCan objects (being reported) could be any version. We need that info in advance to properly handle reports.
    	// This way we make the rest of the code izolated and abstract, as if it always is handling a call from a "real" AP client
    	if (isset($this->_data['apVersion'])) {
    	    if (!empty($this->_data['apVersion'])) {
    	        $incomingVersion = $this->_data['apVersion'];
    	    }
    	    else {
    	        $incomingVersion = "0.9";
    	    }
    	}
    	
    	// For now, disable version check please
    	$doCheck = true;
    	
    	if ($this->_data["type"] == Xapi::API_TYPE_ABOUT) {
    		$doCheck = false;
    	}
    	
    	if ($this->_data["type"] == Xapi::API_TYPE_STATEMENTS && $this->_data["method"] == Xapi::METHOD_GET) {
    		//$doCheck = false;
    	}

    	if ($doCheck) {
    		if (version_compare($incomingVersion, Xapi::VERSION_MAX, ">") || version_compare($incomingVersion, Xapi::VERSION_MIN, "<")) {
    			Xapi::out(Xapi::HTTP_BAD_REQUEST, "Client xAPI Version ($incomingVersion) is not supported by this LRS. Supported versions are between " . Xapi::VERSION_MIN . " and " . Xapi::VERSION_MAX);
    		}
    	}
    	
    	$this->_client_version = $incomingVersion;
    	
    	Xapi::setClientVersion($incomingVersion);
    	
    	
    	
    }
    
    

    /**
     * 
     */
    private function updateApVersion() {
        
        if (isset($this->_data['registration'])) {
            $model = LearningTcAp::model()->findByAttributes(array('registration' => $this->_data['registration']));
            if ($model) {
                $model->xapi_version = Xapi::getClientVersion();
                $model->save(false);
            }
        }
        
    }
    	
    
    public function actionTest() {
    	

		
    	$ts = "2014-07-23T12:34:02.656+03";
    	$x = XapiValidator::validateISO8601Timestamp($ts);
    	
    	
    	header('Content-Type: text/html');
    	
    	CVarDumper::dump($x, 80, true);
        
        
        
        
    }
    
    
    /**
     * DEVELOPMENT TOOL ONLY!!
     * Delete all records from some tables
     */
    public function actionPurgeTables() {
        Yii::app()->db->createCommand("DELETE FROM lrs_activity")->execute();
        Yii::app()->db->createCommand("DELETE FROM lrs_agent")->execute();
        Yii::app()->db->createCommand("DELETE FROM lrs_context")->execute();
        Yii::app()->db->createCommand("DELETE FROM lrs_profile")->execute();
        Yii::app()->db->createCommand("DELETE FROM lrs_result")->execute();
        Yii::app()->db->createCommand("DELETE FROM lrs_state")->execute();
        Yii::app()->db->createCommand("DELETE FROM lrs_statement")->execute();
        Yii::app()->db->createCommand("DELETE FROM lrs_statement_attachment")->execute();
        Yii::app()->db->createCommand("DELETE FROM lrs_verb")->execute();
    }
    
    
    
    /**
     * Map LMS provided registration ID (through a specially named GET parameter) to the registration ID set in the statement context 
     * 
     * @param array $data Incoming data
     *  
     */
    private function mapContextRegistration(&$data) {
       
        // Only do this when we SAVE statements data, i.e. method is PUT|POST
        if ( ($data['method'] == Xapi::METHOD_PUT || $data['method'] == Xapi::METHOD_POST) && $data['type'] == Xapi::API_TYPE_STATEMENTS) {
            
            $lmsRegId = Xapi::getGetParam(Xapi::EXTRA_PARAM_LMSREG, false);
            if ($lmsRegId) {
                if (isset($data['json_to_array']['context']['registration'])) {
                    $apReg = $data['json_to_array']['context']['registration'];
                    $regMap = LrsRegistrationMap::model()->findByAttributes(array("ap_registration" => $apReg));
                    if (!$regMap) {
                        $regMap = new LrsRegistrationMap();
                    }
                    $regMap->ap_registration = $apReg;
                    $regMap->lms_registration = $lmsRegId;
                    if ($regMap->save()) {
                        $data['json_to_array']['context']['registration'] = $lmsRegId;
                        $data['json'] = CJSON::encode($data['json_to_array']);
                    }
                }
            }
            
        }
        
    }
    
    
    /**
     * Collect all possible data from the request.
     * Data collection is sort of twisted: it depends if the request is made in "IE" mode or not.
     * 
     */
    private function collectData() {
    
        // Initialize
        $data           = array();
        $forcedMethod   = strtolower(Xapi::getGetParam("method", false));
        $type           = strtolower(Xapi::getGetParam("type", false));
        $requestType    = strtolower(Yii::app()->request->requestType);
        
        // Are we dealing with IE Mode requests ??
        $IEMode = ($forcedMethod && ($requestType == 'post'));
        if ($IEMode) {
            $restParams = Yii::app()->request->getRestParams();
            $raw_post_array = array();
            foreach ($restParams as $key => $value) {
                $raw_post_array[rawurldecode($key)] = rawurldecode($value);
            }
            
            // Payload should be in "content"
            // Do not be misleaded by "payload" name, here it is just part of the actual PAYLOAD (body)
            $payload = isset($raw_post_array['content']) ? $raw_post_array['content'] : "";
    
            foreach ($this->_possibleHeaderNames as $pname) {
                $data[$pname] = isset($raw_post_array[$pname]) ? $raw_post_array[$pname] : null;
            }
    
            // Possible parameters found in payload
            foreach ($this->_possibleParamNames as $pname) {
                $data[$pname] = isset($raw_post_array[$pname]) ? $raw_post_array[$pname] : null;
                if (Xapi::isJson($data[$pname])) {
                    $data[$pname] = CJSON::decode($data[$pname]);
                }
            }
    
    
            $data['method']     = $forcedMethod;
            $data['type']       = $type;
    
            // Any Concurency to handle?
            $data['ifMatch']      = isset($raw_post_array['If-Match'])          ? $raw_post_array['If-Match'] : null;
            $data['ifNoneMatch']  = isset($raw_post_array['If-None-Match'])     ? $raw_post_array['If-None-Match'] : null;
    
    
        } else {
            // Headers
            if (function_exists('apache_request_headers')) {
                $allHeaders = apache_request_headers();
                ksort($allHeaders);
            } else {
                $allHeaders = array();
                foreach ($_SERVER as $key => $value) {
                    if (substr($key, 0, 5) == 'HTTP_') {
                        $allHeaders[str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))))] = $value;
                    }
                }
            }
            foreach ($this->_possibleHeaderNames as $pname) {
                $data[$pname] = isset($allHeaders[$pname]) ? $allHeaders[$pname] : null;
            }
            
            foreach ($this->_possibleParamNames as $pname) {
                $data[$pname] = Xapi::getGetParam($pname, null); 
            }
    
            $data['method'] = $requestType;
            
            $data['ifMatch'] = isset($_SERVER['HTTP_IF_MATCH']) ? $_SERVER['HTTP_IF_MATCH'] : null;
            $data['ifNoneMatch'] = isset($_SERVER['HTTP_IF_NONE_MATCH']) ? $_SERVER['HTTP_IF_NONE_MATCH'] : null;
    
            // No stripslashes !!! NO!
            $payload = file_get_contents('php://input');
        }
    
    
        // Once again, just to be sure, set all empty parameters to NULL
        foreach ($data as $key => $value) {
            if (empty($value)) {
                $data[$key] = null;
            }
        }
    
        // This must be here !! Below NULL-ing
        $data['ie_mode'] = $IEMode;
    
        // Just in case, keep this in a separate key
        foreach ($_POST as $key => $value) {
            $data['post_data'][$key] = Xapi::getPostParam($key, null);
        }
    
        // What is the API sub type (statements, activity_state, actor, profile, etc...)
        $data['apiType'] = strtolower($data['type']);
    
        // Just keep the Query string here, may come in handy later
        $data['queryString'] = Yii::app()->request->getQueryString();
        
        // Get and store all Query String parameters which are NOT expected and are NOT yet saved in $data array for some other reason
        // Example: an extra parameter in LAUNCH URL
        $tmpArray = array();
        parse_str($data['queryString'], $tmpArray);
        foreach ($tmpArray as $key => $value) {
        	if (!in_array($key, $this->_possibleParamNames) && !isset($data[$key])) {
			    $data[$key] = $value;				    
        	}
        	
        	// Some Query parameters might be a JSON, lets decode
        	if (Xapi::isJson($value)) {
        	    $data[$key] = CJSON::decode(stripcslashes($value), true);
        	}
        	
        }
    
        // If there is a payload, check if it is a json or document
        // Document is used for State and for "logical GET with POST" (wow!)  (see the Specification)
        $data["document"] = "";
        $data["json"] = "";
    
        if (!empty($payload)) {
            $payload_type       = preg_match('/^\s*[\[{]/', $payload) > 0 ? Xapi::PAYLOAD_TYPE_JSON : Xapi::PAYLOAD_TYPE_DOC;
            $data["json"]       = $payload_type == Xapi::PAYLOAD_TYPE_JSON  ? $payload : "";
            $data["document"]   = $payload_type == Xapi::PAYLOAD_TYPE_DOC   ? $payload : "";
        }
    
        if (isset($data["json"])) {
            $data["json_to_array"] = CJSON::decode($data["json"]);
        }

        
        // In case a special Xapi::EXTRA_PARAM_LMSREG parameter is found in the QUERY string and if the context registration is set,
        // Override the context one with the QUERY string one and save the mapping
        $this->mapContextRegistration($data);
        
        // Registration not yet found? Check the context
        if (!isset($data['registration']) && isset($data['json_to_array']['context']['registration'])) {
            $data['registration'] = $data['json_to_array']['context']['registration'];
        }

        if (!empty($data['authorization'])) {
            $data['Authorization'] = $data['authorization'];
        }

        if (!empty($data['x-experience-api-version'])) {
            $data['X-Experience-API-Version'] = $data['x-experience-api-version'];
        }

        unset($data['authorization']);
        unset($data['x-experience-api-version']);
        
        $this->_data = $data;
        
    }
    
    
    /**
     * Handle "statements"
     */
    private function handler_statements() {
        
        //@TODO AUTH ISSUE
        
        $method = $this->_data['method'];
        $data = $this->_data;
        
        $user_data = XapiAuth::getInstance()->user;
        $auth = XapiAuth::getInstance();
        
        if ($method == Xapi::METHOD_PUT) {

            // PUT requires JSON payload
            if (empty ($data['json'])) {
                Xapi::out(Xapi::HTTP_BAD_REQUEST);
            }
            
            // PUT expects ONE statement in JSON, wrap it with an array
            $aStatement = CJSON::decode($data['json']); // ASSOC ARRAY!!
            
            // Internal usage key: statementId: "" if not set by AP
            $aStatement['statementId'] = isset($data['statementId']) ? $data['statementId'] : "";
            
            // If not set, actor is inferred from authentication (already prepared at auth phase)
            if ((!isset($aStatement['actor'])) || !XapiValidator::validateAgent($aStatement['actor'])) {
                $aStatement['actor'] = $user_data['inferred_actor'];
            }
            
            // Validate VERB
            if (!isset($aStatement['verb']) || !XapiValidator::validateVerb($aStatement['verb'])) {
                Xapi::out(Xapi::HTTP_BAD_REQUEST, "Invalid Verb");
            }

            $model = LrsStatement::storeOneStatement($aStatement);
            if (!$model) {
                Xapi::log('Failed to save a Statement', CLogger::LEVEL_ERROR);
                Xapi::log($aStatement, CLogger::LEVEL_ERROR);
                Xapi::out(Xapi::HTTP_SERVER_ERROR);
            }
            
            Xapi::out(Xapi::HTTP_OK);
            
        }
        else if (($method == Xapi::METHOD_POST) && !empty($data['json'])) {
            
            // It is NOT allowed to POST statements in Payload and specify Statement Id as GET parameter
            if ($data["statementId"]) {
                Xapi::out(Xapi::HTTP_METHOD_NOT_ALLOWED, 'POSTing statement(s) and GET Statement Id is not allowed');
            }

            $data['statements'] = CJSON::decode($data['json']); // Array of Assoc Arrays  !!!
            
            // Check if the expected array is really an array of statements or a single statement (.. to convert to array)
            $keys = array_keys($data['statements']);
            if (is_string($keys[0])) {
                $data['statements'] = array($data['statements']);
            }
            
            // Prepare statements
            for ($i = 0; $i < count($data['statements']); $i++) {

                // Add 'statementId' key to all statements, for internal usage
                $statementId = isset($data['statements'][$i]['id']) ? $data['statements'][$i]['id'] : "";
                $data['statements'][$i]['statementId'] = $statementId;
                
                // If not set, actor is inferred from authentication (already prepared at auth phase)
                if (!XapiValidator::validateAgent($$data['statements'][$i]['actor'])) {
                    $data['statements'][$i]['actor'] = $user_data['inferred_actor'];
                }
                
            }

            $statement_uuids = LrsStatement::storeManyStatements($data['statements']);
            
            Xapi::out(Xapi::HTTP_OK);
            
            
        }
        // Return list of Statements
        else if ($method == Xapi::METHOD_GET) {
            
            $result = LrsStatement::getStatements($data);
            $consistentTiemstamp = Yii::app()->localtime->getUTCNow();
            header('X-Experience-API-Consistent-Through: ' . $consistentTiemstamp);
            Xapi::out(Xapi::HTTP_OK, CJSON::encode($result));
        }
        
        else {
            Yii::log("No valid request method: " . $method, CLogger::LEVEL_ERROR);
            Xapi::out(Xapi::HTTP_BAD_REQUEST, "No valid request method: " . $method);
        }
        
    }
    
    /**
     * Handle "activities"
     */
    private function handler_activities() {
    }
    
    /**
     * Handle "activities/state"
     */
    private function handler_activities_state() {
        
        $data 				= $this->_data;
        $method 			= $data['method'];
        $agentArr 		    = isset($data['agent']) ? $data['agent'] : $data['actor'];
        $activityId 		= isset($data['activityId']) ? $data['activityId'] : null; // is is a string here, NOT JSON/XML!!
        $stateId 			= isset($data['stateId']) ? $data['stateId'] : null;
        $registration 		= isset($data['registration']) ? $data['registration'] : null;
        $state 				= !empty($data['document']) ? $data['document'] : $data['json'];
        $since 				= isset($data['since']) ? $data['since'] : null;
        
        
        if (!$activityId) {
            Xapi::out(Xapi::HTTP_BAD_REQUEST, "Invalid Activity");    
        }
        
        if (!XapiValidator::validateAgent($agentArr)) {
            Xapi::out(Xapi::HTTP_BAD_REQUEST, "Invalid Agent/Actor");
        }
        
        switch ($method) {
            case Xapi::METHOD_GET:
                if ($stateId) {
                    $state = LrsState::getState($stateId, $agentArr, $activityId, $registration);

					// this is commented out, because in the specification is not written that the state has to be deleted
					// and sometimes the resuming of the LO doesn't work
                    //$result = LrsState::deleteState($agentArr, $activityId, $stateId, $registration);
                    
                    if ($state) {
                        // If the saved STATE Document is a JSON, send application/json content type header.
                        // Default one is text/plain
                        if (Xapi::isJson($state)) {
                            header('Content-Type: application/json');
                        }
                        header('ETag: ' . Xapi::makeETag($state));
                        Xapi::out(Xapi::HTTP_OK, $state);
                    }
                    else 
                        Xapi::out(Xapi::HTTP_NOT_FOUND);
                }
                else { 
                    $stateIds = LrsState::getStates($agentArr, $activityId, $registration, $since);
                    if (!empty($stateIds)) {
                        header('Content-Type: application/json');
                        Xapi::out(Xapi::HTTP_OK, $stateIds);
                    }
                    Xapi::out(Xapi::HTTP_NOT_FOUND);
                }
                break;
                
            case Xapi::METHOD_DELETE:
                if (!$stateId) $stateId = null;
                $result = LrsState::deleteState($agentArr, $activityId, $stateId, $registration);
                Xapi::out(Xapi::HTTP_NO_CONTENT);
                break;
            
            case Xapi::METHOD_PUT:
            case Xapi::METHOD_POST:
                if (!LrsState::storeState($data, $agentArr, $activityId, $stateId, $state, $registration)) {
                    Xapi::out(Xapi::HTTP_SERVER_ERROR, 'Failed to store state');
                }
                Xapi::out(Xapi::HTTP_NO_CONTENT);
                break;
                
        }
        
        
    }
    
    /**
     * Handle "agents"
     */
    private function handler_agents() {
    	
    	$method = $this->_data['method'];
     	$data = $this->_data;
    	$agentArr = $data['actor'] ? $data['actor'] : $data['agent'];

    	switch ($method) {
    	    case Xapi::METHOD_GET:
    	        if ($agentArr === null) {
    	            Xapi::out(Xapi::HTTP_NOT_FOUND);
    	        }
    	        
    	        $agent_model = LrsAgent::findAgent($agentArr);
    	        
    	        if (!$agent_model) {
    	            Xapi::out(Xapi::HTTP_NOT_FOUND);
    	        }
    	        
    	        // Create an Expanded Agent object (a Person special object)
    	        $agentArr = CJSON::decode($agent_model->json_data);
    	        
    	        if (Xapi::getClientVersionCode() >= Xapi::VERSION_CODE_1) {
                    $personArr = LrsAgent::toExpandedAgent($agentArr);
                    Xapi::out(Xapi::HTTP_OK, CJSON::encode($personArr));
    	        }
    	        else {
                    Xapi::out(Xapi::HTTP_OK, CJSON::encode($agentArr));
    	        }
    	         
    	        break;
    	        
    	    default:
    	        Xapi::out(Xapi::HTTP_METHOD_NOT_ALLOWED);
    	        break;
    	        
    	}
    	
    }
    
    /**
     * Handle "activities/profile"
     */
    private function handler_activities_profile() {
        $this->handle_profile(Xapi::OBJ_TYPE_ACTIVITY);
    }
    
    /**
     * Handle "agents/profile"
     */
    private function handler_agents_profile() {
        $this->handle_profile(Xapi::OBJ_TYPE_AGENT);
    }

    /**
     * 
     * @throws XapiException
     */
    private function handle_profile($objType) {
        
        // Because of versions... if v0.9 is incoming ('actors/profile'), copy its value to 'agent' and use agent from that point on
        if (!isset($data['agent']) && isset($data['actor'])) {
            $data['agent'] = $data['actor'];
        }
        
        $method = $this->_data['method'];
        $data 	= $this->_data;
        $activity_id    = null;
        $agentArr       = null;
        
        if ($objType == Xapi::OBJ_TYPE_ACTIVITY) {
            if (empty($data['activityId'])) {
                Xapi::out(Xapi::HTTP_BAD_REQUEST, "Invalid Activity for Profiles");
            }
            $activity_id = $data['activityId'];
        }
        else if ($objType == Xapi::OBJ_TYPE_AGENT) {
            if (empty($data['agent']) || !XapiValidator::validateAgent($data['agent'])) {
                Xapi::out(Xapi::HTTP_BAD_REQUEST, "Invalid Agent/Actor for Profiles");
            }
            $agentArr 	 = $data['agent'];
        }
        else {
            Xapi::out(Xapi::HTTP_BAD_REQUEST);
        }
         
        $profile_id 	= isset($data['profileId']) ? $data['profileId'] : null;
        $since 			= isset($data['since']) ? $data['since'] : null;
        
        switch ($method) {
            
            case Xapi::METHOD_PUT:
            case Xapi::METHOD_POST:
                LrsProfile::storeProfile($activity_id, $agentArr, $profile_id, $data);
                Xapi::out(Xapi::HTTP_NO_CONTENT);
                break;
                
            case Xapi::METHOD_GET:
                
                $result = LrsProfile::getProfiles($activity_id, $agentArr, $profile_id, $since);
                
                if ($result['success'] === true) {
                    // Returned data format depends on GET parameters
                
                    // Expect a raw contents (activity's document)
                    if (($activity_id) && $profile_id) {
                        $contents = $result['data'];
                    }
                    // Expect an array of values (activity's data)
                    else if ($activity_id) {
                        $contents = CJSON::encode($result['data']);
                    }
                    // Expect a raw contents (actor's document)
                    else if ($agentArr && $profile_id) {
                        $contents = $result['data'];
                    }
                    // Expect an array of values (actor's data)
                    else if ($agentArr) {
                        $contents = CJSON::encode($result['data']);
                    }
                
                    // Add ETag
                    if (!empty($result['etag'])) {
                        header("ETag:" . $result['etag']);
                    }
                    Xapi::out(Xapi::HTTP_OK, $contents);
                } 
                else {
                    Xapi::out(Xapi::HTTP_NOT_FOUND);
                }
                
                break;
                
            case Xapi::METHOD_DELETE:
                if ($objType == Xapi::OBJ_TYPE_AGENT) {
                    LrsProfile::deleteAgentProfile($agentArr, $profile_id);
                }
                else if ($objType == Xapi::OBJ_TYPE_ACTIVITY) {
                    LrsProfile::deleteActivityProfile($activity_id, $profile_id);
                }
                Xapi::out(Xapi::HTTP_NO_CONTENT);
                break;
                
                
            default:
                throw new XapiException(Xapi::HTTP_METHOD_NOT_ALLOWED);
                break;
                
        }
        
        
    }
    
    
    /**
     * Handle "about"
     */
    private function handler_about() {
        
        if ($this->_data["method"] != Xapi::METHOD_GET) {
            Xapi::out(Xapi::HTTP_METHOD_NOT_ALLOWED, "Invalid request method for 'ABOUT' request: " . $this->_data["method"]);
        }
           
    	$output = new stdClass();
    	$output->version = array("1.0.0", Xapi::VERSION_MAX);
    	$output->extensions = new stdClass();
    	header('Content-Type: application/json');
    	Xapi::out(Xapi::HTTP_OK, CJSON::encode($output));
    }
    

    /**
     * Authenticate the call
     */
    private function authenticate() {
        
    	// For tests: skip authentication for "private content requests"
    	if ($this->_data['type'] == Xapi::API_TYPE_CONTENT) return true;
    	
        $authenticate_result = XapiAuth::getInstance()->authenticate($this->_data);
        
        // If Not Authenticated: send response and die
        if (!$authenticate_result || ($authenticate_result['success'] == false)) {
            Xapi::out(Xapi::HTTP_UNAUTHORIZED);
        }
        
        $this->_data['user_data'] = $authenticate_result['data'];
        
        return true;
    }
    
        
    
    /**
     *
     * @see CController::init()
     */
    public function init() 
    {
        
        try {
            $this->checkOrigin();
        }
        catch (Exception $e) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
            Xapi::out(Xapi::HTTP_SERVER_ERROR);
        }
    }

    /**
     * Entry point for everything 
     */
    public function actionIndex()
    {
        
        try {
            
        	header('Content-Type: text/plain');
        	
        	
        	
        	
            // Collect all incoming data
            $this->collectData();
            
            // Check XAPI versioning
            $this->checkVersion();
            
            // Get the type of XAPI endpoint/type
            $type = $this->_data['type'];
            
            if (!$this->checkApiType($type)) {
                //Xapi::out(Xapi::HTTP_NOT_FOUND, "Invalid XAPI endpoint: " . $type);
                Xapi::out(Xapi::HTTP_BAD_REQUEST, "Invalid XAPI endpoint: " . $type);
            }

            // I'm now sure if this CHECK should go here
            // But this should prevent the TINCAN to be tracked when a user is previewing it from the Central LO Repository
            if ( strpos(Yii::app()->request->urlReferrer, "&preview_mode=true") !== false ) {
                Xapi::out(Xapi::HTTP_OK);
                Yii::app()->end();
            }
            
            // If this is not an internally generated request, update Activity Provider package version
            if (!isset($this->_data['internal'])) {
                $this->updateApVersion();
            }
            
            // Authenticate the user
            // ABOUT MAY not be authenticated
            if ($type !== Xapi::API_TYPE_ABOUT) {
            	$this->authenticate();
            }

            
            
            // Handle the request
            if ($type) {
                //Xapi::log('Handling ' . $type);
                $handler = 'handler_' . $type;
                $this->$handler();
            }
            
            // Handlers must end therequest with their own HTTP status
            // If we end up here, something went wrong, so throw an exception
            Xapi::out(Xapi::HTTP_BAD_REQUEST, "Invalid XAPI request");
            throw new Exception('XAPI request ended up not being handled properly by any endpoint handler');
            
        }
        
        catch (Exception $e) {
            Xapi::log($e->getMessage(), CLogger::LEVEL_ERROR);
            Xapi::log($this->_data, CLogger::LEVEL_ERROR);
            Xapi::out(Xapi::HTTP_SERVER_ERROR, $e->getMessage());
        }
        
        catch (XapiException $e) {
            Xapi::log($e->getMessage(), CLogger::LEVEL_ERROR);
            Xapi::log($this->_data, CLogger::LEVEL_ERROR);
            if ($e->getCode())
                Xapi::out($e->getCode(), $e->getMessage());
            else {
                Xapi::out(Xapi::HTTP_SERVER_ERROR, $e->getMessage());
            }
        }
        
        
    }
    
    /**
     * Serves private content requests (See https://github.com/RusticiSoftware/launch/blob/master/lms_lrs.md)
     * 
     * Launch URL now contains "content_token" parameter which validates and identifies the resources (content) specific to the launched XAPI.
     * 
     * NOTE: see /tcapi/.htaccess
     * 
     */
    public function actionContent() {
    
    	/* @var $activityProvider LearningTcAp */
        
    	$this->collectData();
    	
    	$this->authenticate();
    	
        $zipRequested   = Yii::app()->request->getParam('zip', false);
        $subContent     = Yii::app()->request->getParam('sub', false);
        
        // Send the ZIPed package file to the client 
        if ($zipRequested) {
            $result = $this->zipPackage();
            if ($result) {
            	Yii::app()->request->sendFileDirect($result['zipFilePath'], $result['downloadFilename'], true);
            	Yii::app()->end();
            }
            // Maybe is better to redirect the user to the ZIP file ?
            // But then URL is public
            //if ($result) {
            //	$this->redirect($result['zipFileUrl'], true, 301);
            //	Yii::app()->end();
            //}
            Xapi::out(Xapi::HTTP_SERVER_ERROR, "TinCan Archive not available");
        }
        
        // Redirect the client to an Arbitrary private content
        if ($subContent) {
            $url = $this->contentUrl($subContent);
            if ($url) {
                $this->redirect($url, true, 301);
                Yii::app()->end();
            }
            Xapi::out(Xapi::HTTP_SERVER_ERROR, "TinCan private content not available");
        }
    	 
        Xapi::out(Xapi::HTTP_BAD_REQUEST, "Invalid request");
    
    
    }
    

    /**
     * Archive tincan package and return an URL to it (in uploads temporary folder)
     * 
     */
    private function zipPackage() {

        $contentToken = Yii::app()->request->getParam('content_token', false);
        if (!$contentToken) {
            Xapi::out(Xapi::HTTP_BAD_REQUEST, 'Missing content token');
        }
         
        $activityProvider = LearningTcAp::model()->findByAttributes(array(
            'registration'	=> $contentToken,
        ));
         
        if (!$activityProvider) {
            Xapi::out(Xapi::HTTP_BAD_REQUEST, 'Invalid content token');
        }
        
        $mainActivity = $activityProvider->mainActivity;
        $downloadFileName = $mainActivity ? Sanitize::fileName($mainActivity->name, true) . ".zip" : false;
        
        $result = $activityProvider->archivePackage();
        $result['downloadFilename'] = $downloadFileName;
        
        return $result;
        
    }
    
    
    /**
     * Return an URL to an arbitrary content located INSIDE the tincan package folder
     * @param unknown $subContent
     */
    private function contentUrl($subContent) {
     
        $contentToken = Yii::app()->request->getParam('content_token', false);
        if (!$contentToken) {
            Xapi::out(Xapi::HTTP_BAD_REQUEST, 'Missing content token');
        }
         
        $activityProvider = LearningTcAp::model()->findByAttributes(array(
            'registration'	=> $contentToken,
        ));
         
        if (!$activityProvider) {
            Xapi::out(Xapi::HTTP_BAD_REQUEST, 'Invalid content token');
        }
        
        
        $storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_TINCAN);

        $url = $storage->fileUrl($subContent, $activityProvider->path);
        
        return $url;
        
        
    }
    
    
    
}
