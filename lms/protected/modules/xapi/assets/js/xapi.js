/**
 * 
 * XAPI Javascript compagnion 
 *  
 */
var XAPI = (XAPI || {});

(function($) {
	
	XAPI.Helper = function(options) {
		this.options = $.extend({}, this.defaultOptions, options);
	}
	
	XAPI.Helper.prototype = {
		
		defaultOptions: {
			language: "en-US"
		},
		
		options: {},
			
		
		/**
		 * Get a value from language dictionary property of an object (like display, name, etc.)
		 */
        getLangDictValue: function (object, prop, lang) {
        	
        	if (typeof prop === "undefined" || !prop) {
        		prop = "display";
        	}
        	
        	if (typeof object[prop] === "undefined") {
        		return "";
        	} 
        	
            var key;
            var dict = object[prop];
            
            if (typeof lang !== "undefined" && typeof dict[lang] !== "undefined") {
                return dict[lang];
            }
            if (typeof dict.und !== "undefined") {
                return dict.und;
            }

            if (typeof dict["en-US"] !== "undefined") {
                return dict["en-US"];
            }
            
            for (key in dict) {
                if (dict.hasOwnProperty(key)) {
                    return dict[key];
                }
            }

            return "";
        },

		
        /**
         * Escape HTML
         */
		escapeHtml : function (text) {
		    var html = text + "";
		    return html.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/\'/g,"'");
		},
		
		
		/**
		 * 
		 */
	    truncateString: function(str, length) {
	        if (str === null || str.length < 4 || str.length <= length) {
	            return str;
	        }
	        return str.substr(0, length - 3) + '...';
	    },

	    
	    /**
	     * Render Actor (Agent)
	     */
	    renderActor: function (actor, versionCode) {
	    	
	    	var text = "";
	    	
            if (actor.name !== null) {
            	text = actor.name + "";
            }
            else if (actor.mbox !== null) {
            	text = actor.mbox + "";
            	text = text.replace("mailto:", "");
            }
            else if (actor.mbox_sha1sum !== null) {
                text = actor.mbox_sha1sum + "";
            }
            else if (actor.openid !== null) {
            	text = actor.openid + "";
            }
            else if (actor.account !== null) {
            	if (versionCode !== null) {
            		text += actor.account.name !== null ? actor.account.name : "-";
            		text += ":";
            		text += actor.account.homePage !== null ? actor.account.homePage : "-";
            	}
            }

	        return text;
	        
	    },
	    
	    /**
	     * 
	     */
	    renderVerb: function(verb, versionCode) {
	    	
	    	var display;
	    	
	    	if (versionCode === null) {
	    		display = verb;
	    	} 
	    	else if (verb.display !== null) {
           		display = this.getLangDictValue(verb, "display", this.options.language);
           	}
           	else if (verb.id !== null) {
           		display = verb.id;  
           	}
           	else {
           		display = verb;
           	}
           	
           	return display;
           	
	    },

	    
	    /**
	     * Render Activity
	     */
	    renderActivity: function(activity) {
	    	var display;
	    	
	    	if (activity.objectType === null || activity.objectType == "Activity") {
	    		if (typeof activity.definition !== "undefined") {
	    			display = this.getLangDictValue(activity.definition, "name", this.options.language);
	    			if (!display && typeof activity.definition.description !== "undefined") {
	    				display = this.getLangDictValue(activity.definition, "description", this.options.language);
	    			}
	    		}
	    		else {
	    			display = activity.id; 
	    		}
	    	}
	    	else if (activity.objectType !== null) {
	    		display = "(" + activity.objectType + ")";
	    	}
	    	else {
	    		display = "(undefined)";
	    	}
	    	
            return display;
	    },
	    
	    
	    /**
	     * Render Result Score
	     */
	    renderScore: function(score) {
	    	
	    	var text = "";
	    	
			if (typeof score.scaled !== "undefined") {
				text = Math.round((score.scaled * 100.0)) + "%";
			} else if (score.raw !== null) {
				text = score.raw ;
			}

			return text;
			
	    },
	    
	    /**
	     * Render ONE statement, respecting the XAPI Version code (NULL: 0.9,  1: 0.95..1.0.x, ...)
	     */
	    renderStatementAsVersion: function(stmt, versionCode) {
	    	
	    	var stmtStr = [];
	    	
	    	// -- ROW
	        stmtStr.push("<tr class='statementRow'>");
	        
	        // Time (stored)
	        stmtStr.push("<td class='date'><div class='statementDate'>" + (stmt.stored !== null ? stmt.stored.replace('Z','') : "") + "</div></td>");
	        
	        // --
	        stmtStr.push("<td class='statementCell'>");
	        
	        stmtStr.push("<div class='statement unwired' tcid='" + stmt.id + "'>");
	        
	        try {
	        	
	        	// Actor
	            stmtStr.push(
                    "<span class='actor'>" + 
                        (stmt.actor !== null ? this.escapeHtml(this.renderActor(stmt.actor, versionCode)) : "No Actor") + 
                    "</span> ");
	        	

	        	// Verb
	            stmtStr.push(
                    "<span class='verb'>" + 
                        (stmt.verb !== null ? this.escapeHtml(this.renderVerb(stmt.verb, versionCode)) : "No Verb") + 
                    "</span> ");

	        	// Activity
	            var actDisplay = ( stmt.object !== null ? this.escapeHtml(this.renderActivity(stmt.object)) : "No Activity");
	            stmtStr.push(
                    '<a title="' + this.escapeHtml(actDisplay) + '"><span class="target">' + 
                    this.escapeHtml(this.truncateString(actDisplay,60)) + 
                    "</span></a> ");
	            

	            // Score RESULT
	            if (typeof stmt.result !== "undefined" && typeof stmt.result.score !== "undefined") {
	            	stmtStr.push(" with score <span class='score'>" + this.renderScore(stmt.result.score)) + "</span>";
	            }

	            // RESPONSE RESULT
	            if (typeof stmt.result !== "undefined") {
	            	
	            	// If we got "success" status...
	            	if (typeof stmt.result.success !== "undefined") {
		            	if (!stmt.result.success) {
		            		stmtStr.push(" with <span class='response'>INCORRECT</span> response");
		            	}
		            	else {
		            		stmtStr.push(" with <span class='response'>CORRECT</span> response");
		            	}
	            	}
	            	
	            	// Also, show the actual response given
					if (typeof stmt.result.response !== "undefined"){
						stmtStr.push("  (" + this.truncateString(this.getResponseText(stmt), 30) + ")  ");
					}

	            }
	        	
	            // Statement Raw Data (original JSON)
	            if (includeRawData) {
	            	var rawDataStr = JSON.stringify(stmt.originalJSON);
	                stmtStr.push("<div class='tc_rawdata' tcid_data='" + stmt.id + "'>");
	                stmtStr.push("<pre>" + this.escapeHtml(rawDataStr) + "</pre>");
	                stmtStr.push("</div>");
	            }

	        }
	        catch (error) {
	            Docebo.log("Error occurred while trying to display statement with id " + stmt.id + ": " + error.message);
	            stmtStr.push("<span class='stId'>" + stmt.id + "</span>");
	        }
	        
	        
	        stmtStr.push("</div>");
	        
	        
	        stmtStr.push("</td>");
	        // --
	        
	        
	        stmtStr.push("</tr>");
	        // -- ROW
	    	
	        return stmtStr.join('');
	    	
	    },
	    
	    
		/**
		 * Render array of stetements into a full TABLE structure
		 */
		renderStatements : function(statements, includeRawData){
			
			var allStmtStr = [];
			var stmt;
			var verbDisplay;
			var objectDisplay;
			var stmtText = "";
			
			allStmtStr.push("<table>");
			
			
		    for (i = 0; i < statements.length; i += 1) {
		    	
		    	stmt = statements[i];
		    	
		    	versionCode = null;
		    	if (typeof stmt.xapi_version_code !== "undefined") {
		    		versionCode = stmt.xapi_version_code;
		    	}
		    	
		    	stmtText = this.renderStatementAsVersion(stmt, versionCode);
		        
		        allStmtStr.push(stmtText);
		        
		    }
		    
		    allStmtStr.push("</table>");
		    
		    return allStmtStr.join('');
		    
		},
		
		getDateString: function(dt) {
	    	var now = new Date();
	    	var usemonths = false;
	    	var usedays = false;
	    	var usehours = false;
	    	var useminutes = false;
	    	var useseconds = false;
	    
	    	if (now.getFullYear() != dt.getFullYear()) {
	    		if (now.getFullYear()-dt.getFullYear() <= 1) {
	    			usemonths = true;
	    		} else {
	    			return (now.getFullYear()-dt.getFullYear())+" years ago"
	    		}
	    	}
	    	if (usemonths || (now.getUTCMonth() != dt.getUTCMonth())) {
	    		if (now.getUTCMonth()-dt.getUTCMonth() <= 2) {
	    			usedays = true;
	    		} else {
	    			return (now.getUTCMonth()-dt.getUTCMonth())+" months ago"
	    		}
	    	}
	    	if (usedays || (now.getUTCDate() != dt.getUTCDate())) {
	    		if (now.getUTCDate()-dt.getUTCDate() <= 2) {
	    			usehours = true;
	    		} else {
	    			return (now.getUTCDate()-dt.getUTCDate())+" days ago"
	    		}
	    	}
	    	if (usehours || (now.getUTCHours() != dt.getUTCHours())) {
	    		if (now.getUTCHours()-dt.getUTCHours() <= 2) {
	    			useminutes = true;
	    		} else {
	    			return (now.getUTCHours()-dt.getUTCHours())+" hours ago"
	    		}
	    	}
	    	if (useminutes || (now.getUTCMinutes() != dt.getUTCMinutes())) {
	    		if (now.getUTCMinutes()-dt.getUTCMinutes() <= 2) {
	    			useseconds = true;
	    		} else {
	    			return (now.getUTCMinutes()-dt.getUTCMinutes())+" minutes ago"
	    		}
	    	}
	    	if (useseconds) {
	    		return (now.getUTCSeconds()-dt.getUTCSeconds())+" seconds ago"
	    	}
	    },
	    
	    getResponseText: function(stmt){
	    	
	        if(stmt == undefined || stmt.result == undefined || stmt.result.response == undefined){
	            return null;
	        }
	        var response = stmt.result.response;

	        if(stmt.object == undefined 
	            || stmt.object.definition == undefined 
	            || stmt.object.definition.type != "http://adlnet.gov/expapi/adlnetctivities/cmi.interaction"
	            || stmt.object.definition.interactionType == undefined){
	                return response;
	        }

	        var objDef = stmt.object.definition;

	        var componentName = null;
	        if(objDef.interactionType == "choice" || objDef.interactionType == "sequencing"){
	            componentName = "choices";
	        }
	        else if (objDef.interactionType == "likert"){
	            componentName = "scale";
	        }
	        else if (objDef.interactionType == "performance"){
	            componentName = "steps";
	        }

	        Docebo.log(componentName);
	        
	        if(componentName != null){
	            var components = objDef[componentName];
	            if (components != undefined && components.length > 0){
	                var responses = response.split("[,]");
	                var responseStr = [];
	                var first = true;
	                for(var i = 0; i < responses.length; i++){
	                    for(var j = 0; j < components.length; j++){
	                        var responseId = responses[i];
	                        if(objDef.interactionType == "performance"){
	                            responseId = responses[i].split("[.]")[0];
	                        }
	                        if(responseId == components[j].id){
	                            if(!first){
	                                responseStr.push(", ");
	                            }
	                            responseStr.push(this.getLangDictValue(components[j], "description", this.options.language)); 
	                            if(objDef.interactionType == "performance"){
	                                responseStr.push(" -> ");
	                                responseStr.push(responses[i].split("[.]")[1]);
	                            }
	                            first = false;
	                        }
	                    }
	                }
	                if(responseStr.length > 0){
	                    return responseStr.join('');
	                }
	            }
	            return response;
	        }

	        if(objDef.interactionType == "matching"){
	            if (objDef.source != undefined && objDef.source.length > 0
	                 && objDef.target != undefined && objDef.target.length > 0){

	                var source = objDef.source;
	                var target = objDef.target;
	                var responses = response.split("[,]");
	                var responseStr = [];
	                var first = true;

	                for(var i = 0; i < responses.length; i++){
	                    var responseParts = responses[i].split("[.]");
	                    for(var j = 0; j < source.length; j++){
	                        if(responseParts[0] == source[j].id){
	                            if(!first){
	                                responseStr.push(", ");
	                            }
	                            responseStr.push(this.getLangDictValue(source[j], "description", this.options.language));
	                            first = false;
	                        }
	                    }
	                    for(var j = 0; j < target.length; j++){
	                        if(responseParts[1] == target[j].id){
	                            responseStr.push(" -> ");
	                            responseStr.push(getLangDictionaryValue(target[j], "description", this.options.language));
	                        }
	                    }
	                }

	                if(responseStr.length > 0){
	                    return responseStr.join('');
	                }
	            }
	            return response;
	        }

	        return response;
	    }
	    

	
		
	
	};
	
	
})(jQuery);