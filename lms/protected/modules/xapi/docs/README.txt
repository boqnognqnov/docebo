Please do not delete this folder 

Collecting TinCan/xAPI documentation, beside the specification itself, can be a tedious and annoying process,
as it is scattered across many locations: Adlnet github, TinCan web site, Rustici softaware, etc.

Indeed, if you find more documentation or sources/links, please add them here. Thanks

Plamen Petkov


Sources:

https://github.com/adlnet
https://github.com/RusticiSoftware
http://projecttincan.com
http://tincanapi.com/