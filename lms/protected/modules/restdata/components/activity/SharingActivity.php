<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SharingActivity extends RestData {

		/**
		 * Constructor
		 *
		 * @param string $params
		 */
		public function __construct($params=false) {
			parent::__construct($params);
		}

		public function getSharingData() {
			$sharingActivity = App7020SharingStatistics::getAllSharingStatistics((int)$this->timeframe,$this->id_user);
			return $sharingActivity;
		}
	    public function run(){
			$sharingData = $this->getSharingData();
			$result = array(
				"success" => true,
				"sharingData" => $sharingData
			);
			return $result;
		}
}