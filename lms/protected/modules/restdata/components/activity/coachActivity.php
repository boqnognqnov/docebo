<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class coachActivity extends RestData {

		/**
		 * Constructor
		 *
		 * @param string $params
		 */
		public function __construct($params=false) {
			parent::__construct($params);
		}

		public function getActivityData() {
			$coachActivity = App7020QuestionAnswerStatistic::getAllCoachActivity((int)$this->timeframe,$this->id_user);
			$peerActivity = App7020PeerReviewStatistic::getAllPeerReviewActivity((int)$this->timeframe,$this->id_user);
			$coachActivity = array_merge($coachActivity,$peerActivity);
			return $coachActivity;
		}
	    public function run(){
			$activityData = $this->getActivityData();
			$result = array(
				"success" => true,
				"activityData" => $activityData
			);
			return $result;
		}
}