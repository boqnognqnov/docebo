<?php
/**
 * Description of peerActivity
 *
 * @author Trayan
 */
class peerActivity extends RestData {

		/**
		 * Constructor
		 *
		 * @param string $params
		 */
		public function __construct($params=false) {
			parent::__construct($params);
		}

		public function getPeerActivity() {
			$peerActivity = App7020PeerReviewStatistic::getAllPeerReviewActivity((int)$this->timeframe,$this->id_user);
			return $peerActivity;
		}
		public function run(){
			$peerActivity = $this->getPeerActivity();
			$result = array(
				"success" => true,
				"channel" => $peerActivity,
			);
			return $result;
		}
}
