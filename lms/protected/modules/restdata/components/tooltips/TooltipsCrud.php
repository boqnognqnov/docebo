<?php

/**
 *
 */
class TooltipsCrud extends RestData {

	/**
	 * Constructor
	 *
	 * @param array $params
	 */
	public function __construct($params = false) {
		parent::__construct($params);
	}

	/**
	 * Execute operation
	 */
	public function run() {
		if ($this->endpoint == RestData::ENDPOINT_TOOLTIPS_DATA) {
			return $this->_tooltipsData();
		}

		if ($this->endpoint == RestData::ENDPOINT_TOOLTIPS_CRUD) {
			$result['success'] = false;
			if ($this->action == 'edit') {

				if (count($this->items)) {
					foreach ($this->items as $item) {
						$itemModel = App7020Tooltips::model()->findByPk($item['id']);
						if ($itemModel->id) {
							$itemModel->tooltipRow = $item['row'];
							$itemModel->save(false);
						}
					}
				}

				$tooltip = App7020Tooltips::model()->findByPk($this->tooltip['id']);
				if (!$tooltip) {
					$newTooltip = new App7020Tooltips();
					$newTooltip->idContent = $this->assetId;
					$newTooltip->idExpert = Yii::app()->user->idst;
					$newTooltip->tooltipRow = $this->tooltip['tooltipRow'];
					$newTooltip->tooltipStyle = $this->tooltip['tooltipStyle'];
					$newTooltip->durationFrom = $this->tooltip['durationFrom'];
					$newTooltip->durationTo = $this->tooltip['durationTo'];
					$newTooltip->text = $this->tooltip['text'];
					
					$result['success'] = (bool) $newTooltip->save();
					if($newTooltip->errors){
						$result['errors'] = $newTooltip->errors;
					}
					$result['itemId'] = $newTooltip->id;
					$result['tooltip_text'] = $newTooltip->text;
				} else {
					$tooltip->idExpert = Yii::app()->user->idst;
					$tooltip->dateUpdated = Yii::app()->localtime->getUTCNow();
					$tooltip->tooltipRow = $this->tooltip['tooltipRow'];
					$tooltip->tooltipStyle = $this->tooltip['tooltipStyle'];
					$tooltip->durationFrom = $this->tooltip['durationFrom'];
					$tooltip->durationTo = $this->tooltip['durationTo'];
					$tooltip->text = $this->tooltip['text'];
					
					$result['success'] = (bool) $tooltip->save();
					if($tooltip->errors){
						$result['errors'] = $tooltip->errors;
					}
					$result['tooltip_text'] = $tooltip->text;
					Yii::app()->event->raise('App7020NewTooltip', new DEvent($this, array('tooltipId' => $tooltip->id)));
				}
				$result['cuepoints'] = App7020Tooltips::generateQuepoints($this->assetId, true);
				$result['tooltips'] = $this->_getTooltips();
			}

			if ($this->action == 'delete') {
				$tooltip = App7020Tooltips::model()->findByPk($this->tooltip['id']);
				$result['success'] = (bool) $tooltip->delete();
				$result['cuepoints'] = App7020Tooltips::generateQuepoints($this->assetId, true);
				$result['tooltips'] = $this->_getTooltips();
			}

			if ($this->action == 'drag') {
				$tooltip = App7020Tooltips::model()->findByPk($this->tooltip['id']);
				$tooltip->idExpert = Yii::app()->user->idst;
				$tooltip->dateUpdated = Yii::app()->localtime->getUTCNow();
				$tooltip->durationFrom = $this->item['durationFrom'];
				$tooltip->durationTo = $this->item['durationTo'];
				$tooltip->save();
				$result['success'] = true;
			}

			return $result;
		}
	}

	private function _tooltipsData() {
		$tooltips = App7020Tooltips::model()->findAllByAttributes(array('idContent' => $this->assetId));
		$result['success'] = true;
		$result['tooltips'] = $tooltips;
		return $result;
	}
	
	private function _getTooltips() {
		$tArray = array();
		$assetModel = App7020Assets::model()->findByPk($this->assetId);
		if ($assetModel) {
			foreach ($assetModel->tooltips as $value) {
				$tArray[] = array(
					'tooltipStyle' => App7020Tooltips::$_themes[$value->tooltipStyle],
					'avatar_owner' => Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $value->idExpert), true),
					'id' => $value->id,
					'from' => $value->durationFrom,
					'to' => $value->durationTo,
					'tooltip_text' => $value->text
				);
			}
		}
		return $tArray;
	}

}
