<?php

/**
 *
 * @property integer $id_question
 * @property array $item
 */
class ETCrud extends RestData {

	/**
	 * Constructor
	 *
	 * @param array $params
	 */
	public function __construct($params = false) {
		parent::__construct($params);
	}

	/**
	 * Execute operation
	 */
	public function run() {
		if ($this->endpoint == RestData::ENDPOINT_ET_SILENCE_CRUD) {
			if ($this->action == 'silence') {
				return $this->_silence();
			}
			if ($this->action == 'unsilence') {
				return $this->_unsilence();
			}
		}

		if ($this->endpoint == RestData::ENDPOINT_ET_QUICK_ANSWER) {
			return $this->_quickAnswer();
		}

		if ($this->endpoint == RestData::ENDPOINT_ET_CLEAN_SILENCED) {
			return $this->_cleanSilenced();
		}
		
		if ($this->endpoint == RestData::ENDPOINT_ET_ASSIGN_CRUD) {
			return $this->_assign();
		}
		
	}

	private function _silence() {
		$id = $this->id;
		$objectType = $this->type;
		$result = array();
		/* Add Question TO IGNORE LIST TABLE if not exist */
		if (!App7020IgnoreList::model()->findByAttributes(
						array(
							'idUser' => Yii::app()->user->idst,
							'idObject' => $id,
							'objectType' => $objectType))) {

			$ignoreListModel = new App7020IgnoreList();
			$ignoreListModel->idUser = Yii::app()->user->idst;
			$ignoreListModel->idObject = $id;
			$ignoreListModel->objectType = $objectType;
			$ignoreListModel->save();
			$result['success'] = true;
		} else {
			$result['success'] = false;
		}
		return $result;
	}

	private function _unsilence() {
		$id = $this->id;
		$objectType = $this->type;
		$result = array();
		$ignoreListModel = App7020IgnoreList::model()->findByAttributes(array('idUser' => Yii::app()->user->idst, 'idObject' => $id, 'objectType' => $objectType));
		if ($ignoreListModel->id) {
			$ignoreListModel->delete();
			$result['success'] = true;
		} else {
			$result['success'] = false;
		}
		return $result;
	}

	private function _quickAnswer() {
		$idQuestion = $this->id_question;
		$content = $this->content;
		$result = array();
		$answerModel = new App7020Answer();
		$answerModel->idQuestion = $idQuestion;
		$answerModel->idUser = Yii::app()->user->idst;
		$answerModel->content = $content;
		$answerModel->bestAnswer = 1;
		$answerModel->save();
		$result['success'] = true;
	}

	private function _cleanSilenced() {
		$result = array();
		$objectType = ($this->type == 'request') ? 'question' : $this->type;
		$ignoreListModel = App7020IgnoreList::model()->findAllByAttributes(array('idUser' => Yii::app()->user->idst, 'objectType' => $objectType));
		foreach ($ignoreListModel as $object) {
			if ($this->type == 'request') {
				$request = App7020QuestionRequest::model()->findByAttributes(array('idQuestion' => $object->idObject));
				if ($request) {
					$object->delete();
				}
			} else {
				$object->delete();
			}
		}
		$result['success'] = true;
		return $result;
	}
	
	private function _assign(){
		$result = array();
		$request = App7020QuestionRequest::model()->findByAttributes(array('idQuestion' => $this->id));
		if($request->id){
			$request->idExpert = Yii::app()->user->idst;
			$request->tooked = Yii::app()->localtime->getUTCNow();
			$request->save();
			$result['success'] = true;
		}else{
			$result['success'] = false;
		}
		return $result;
	}
}
