<?php

/**
 *
 *
 */
class AssetRaData extends RestData {
	
	const ACTION_UID_UNIGNORE = 'unignore_asset';
	const ACTION_UID_IGNORE = 'ignore_asset';
	const ACTION_UID_EDIT_ASSET = 'edit_asset';
	
	/**
	 * Constructor
	 *
	 * @param array $params
	 */
	public function __construct($params = false) {
		parent::__construct($params);
	}

	public function run() {

		$items = array();

		if ($this->endpoint == RestData::ENDPOINT_ASSET_RA_IN_REVIEW) {
			$requestCount = App7020Assets::assetToReviewApprove(Yii::app()->user->idst,array('all'=>1));
			$items = $this->_getInReview();

			$result = array(
				'success' => true,
				'title' => Yii::t('app7020', 'In Review'),
				'description' => '...',
				'asset_count' => count($requestCount),
				'total_asset_count' => count($requestCount),
				'items' => $items,
			);
		}

		if ($this->endpoint == RestData::ENDPOINT_ASSET_RA_NEW_ONLY) {
			$requestCount = App7020Assets::assetToReviewApprove(Yii::app()->user->idst, array('onlyNew' => true,'all'=>1));
			$items = $this->_getNewOnly();

			$result = array(
				'success' => true,
				'title' => Yii::t('app7020', 'New Only'),
				'description' => '...',
				'asset_count' => count($requestCount),
				'total_asset_count' => count($requestCount),
				'items' => $items,
			);
		}

		if ($this->endpoint == RestData::ENDPOINT_ASSET_RA_SILENCED) {
			$requestCount = App7020Assets::assetToReviewApprove(Yii::app()->user->idst, array('ignored' => true,'all'=>1));
			$items = $this->_getSilenced();

			$result = array(
				'success' => true,
				'title' => Yii::t('app7020', 'Silenced'),
				'description' => '...',
				'asset_count' => count($requestCount),
				'total_asset_count' => count($requestCount),
				'items' => $items,
			);
		}

		return $result;
	}

	private function _getInReview() {
		$request = App7020Assets::assetToReviewApprove(Yii::app()->user->idst,array('from' => $this->from, 'count' => $this->count));
		$result = array();
		foreach ($request as $r) {
			$result[] = array(
				'asset_id' => (int)$r['asset_id'],
				'thumbnail' => App7020Assets::getAssetThumbnailUri($r['asset_id']),
				'asset_type' => App7020Assets::$contentTypes[$r['asset_type']]['outputPrefix'],
				'duration' => $r['duration'],
				'title' => $r['title'],
				'link_to_asset' => Docebo::createApp7020Url('site/index', array("#" => "/assets/peerReview/".$r['asset_id'])),
				'owner_channel_link' => Docebo::createLmsUrl('channels/index', array('#' => '/pchannel/'.$r['owner_id'])),
				'owner' => CoreUser::getForamattedNames($r['owner_id']),
				'owner_id' => $r['owner_id'],
				'update_date' => App7020Helpers::timeAgo($r['update_date']),
				'is_new' => $r['is_new'] ? true : false,
				'total_peer_reviews' => (int)$r['total_peer_reviews'],
				'ignored' => false,
				'ignore_state' => 0, /* 0-(hide ignore template) 1-(show before step) 2-( show after step)*/
				'type_uid' => 'asset',
				'actions' => array(
					array(
						'action_type' => 'link',
						'action_uid' => self::ACTION_UID_EDIT_ASSET,
						'action_params' => array(
							'url' => Docebo::createApp7020Url('site/index', array("#" => "/assets/peerReview/".$r['asset_id']))
						),
						'label' => Yii::t('standard', 'Edit asset'),
						'icon_name' => 'fa-pencil-square-o',
						'icon_color' => '#5DBE5D'
					),
					array(
						'action_type' => 'action',
						'action_uid' => self::ACTION_UID_IGNORE,
						'label' => Yii::t('standard', 'Silence asset'),
						'icon_name' => 'fa-bell-slash-o',
						'icon_color' => '#333'
					)
				)
			);
		}

		return $result;
	}

	private function _getNewOnly() {
		$request = App7020Assets::assetToReviewApprove(Yii::app()->user->idst, array('onlyNew' => true,'from' => $this->from, 'count' => $this->count));

		$result = array();
		foreach ($request as $r) {
			$result[] = array(
				'asset_id' => $r['asset_id'],
				'thumbnail' => App7020Assets::getAssetThumbnailUri($r['asset_id']),
				'asset_type' => App7020Assets::$contentTypes[$r['asset_type']]['outputPrefix'],
				'duration' => $r['duration'],
				'title' => $r['title'],
				'link_to_asset' => Docebo::createApp7020Url('site/index', array("#" => "/assets/peerReview/".$r['asset_id'])),
				'owner_channel_link' => Docebo::createLmsUrl('channels/index', array("#" => "/pchannel/".$r['owner_id'])),
				'owner' => CoreUser::getForamattedNames($r['owner_id']),
				'owner_id' => $r['owner_id'],
				'update_date' => App7020Helpers::timeAgo($r['update_date']),
				'is_new' => $r['is_new'] ? false : true,
				'total_peer_reviews' => $r['total_peer_reviews'],
				'ignored' => false,
				'ignore_state' => 0, /* 0-(hide ignore template) 1-(show before step) 2-( show after step)*/
				'type_uid' => 'asset',
				'actions' => array(
					array(
						'action_type' => 'link',
						'action_uid' => self::ACTION_UID_EDIT_ASSET,
						'action_params' => array(
							'url' => Docebo::createApp7020Url('site/index', array("#" => "/assets/peerReview/".$r['asset_id']))
						),
						'label' => Yii::t('standard', 'Edit asset'),
						'icon_name' => 'fa-pencil-square-o',
						'icon_color' => '#5DBE5D'
					),
					array(
						'action_type' => 'action',
						'action_uid' => self::ACTION_UID_IGNORE,
						'label' => Yii::t('standard', 'Silence asset'),
						'icon_name' => 'fa-bell-slash-o',
						'icon_color' => '#333'
					)
				)
			);
		}

		return $result;
	}

	private function _getSilenced() {

		$request = App7020Assets::assetToReviewApprove(Yii::app()->user->idst, array('ignored' => true,'from' => $this->from, 'count' => $this->count));

		$result = array();
		foreach ($request as $r) {
			$result[] = array(
				'asset_id' => $r['asset_id'],
				'thumbnail' => App7020Assets::getAssetThumbnailUri($r['asset_id']),
				'asset_type' => App7020Assets::$contentTypes[$r['asset_type']]['outputPrefix'],
				'duration' => $r['duration'],
				'title' => $r['title'],
				'link_to_asset' => Docebo::createApp7020Url('site/index', array("#" => "/assets/peerReview/".$r['asset_id'])),
				'owner_channel_link' => Docebo::createLmsUrl('channels/index', array("#" => "/pchannel/".$r['owner_id'])),
				'owner' => CoreUser::getForamattedNames($r['owner_id']),
				'owner_id' => $r['owner_id'],
				'update_date' => App7020Helpers::timeAgo($r['update_date']),
				'is_new' => $r['is_new'] ? false : true,
				'total_peer_reviews' => $r['total_peer_reviews'],
				'ignored' => true,
				'ignore_state' => 0, /* 0-(hide ignore template) 1-(show before step) 2-( show after step)*/
				'type_uid' => 'asset',
				'actions' => array(
					array(
						'action_type' => 'link',
						'action_uid' => self::ACTION_UID_EDIT_ASSET,
						'action_params' => array(
							'url' => Docebo::createApp7020Url('site/index', array("#" => "/assets/peerReview/".$r['asset_id']))
						),
						'label' => Yii::t('standard', 'Edit asset'),
						'icon_name' => 'fa-pencil-square-o',
						'icon_color' => '#5DBE5D'
					),
					array(
						'action_type' => 'action',
						'action_uid' => self::ACTION_UID_UNIGNORE,
						'label' => Yii::t('standard', 'Notify me'),
						'icon_name' => 'fa-bell-o',
						'icon_color' => '#333'
					)
				)
			);
		}

		return $result;
	}

}
