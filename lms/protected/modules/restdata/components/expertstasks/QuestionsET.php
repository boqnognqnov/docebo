<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class QuestionsET extends RestData {

	const ACTION_UID_UNIGNORE = 'unignore_question';
	const ACTION_UID_IGNORE = 'ignore_question';
	const ACTION_UID_QUICK_ANSWER = 'quick_answer';
	const ACTION_UID_VIEW_QUESTION = 'view_question';

	/**
	 * Constructor
	 *
	 * @param string $params
	 */
	public function __construct($params = false) {
		parent::__construct($params);
	}

	public function run() {

		if ($this->only_new) {
			$questionsToAnswerCount = count(App7020Question::getQuestionsToAnswer(Yii::app()->user->id, array("onlyNew" => 1, "all" => 1)));
			$questionsToAnswer = App7020Question::getQuestionsToAnswer(Yii::app()->user->id, array("onlyNew" => 1, "from" => $this->from, "count" => $this->count));
			$sectionTitle = Yii::t("standard", "New only");
			$sectionDescription = Yii::t("standard", "These are the NEW question assigned to you, where you haven't provided an answer yet.");
		} elseif ($this->ignored) {
			$questionsToAnswerCount = count(App7020Question::getQuestionsToAnswer(Yii::app()->user->id, array("ignored" => 1, "all" => 1)));
			$questionsToAnswer = App7020Question::getQuestionsToAnswer(Yii::app()->user->id, array("ignored" => 1, "from" => $this->from, "count" => $this->count));
			$sectionTitle = Yii::t("standard", "Silenced");
			$sectionDescription = Yii::t("standard", "These are your silenced questions. You won't be notified for any change that happens. You can re-enable notifications or, if no action is taken, they will be automatically removed from here as soon as a best answer is choosen. Alternatively, you can manua");
		} else {
			$questionsToAnswerCount = count(App7020Question::getQuestionsToAnswer(Yii::app()->user->id, array("all" => 1)));
			$questionsToAnswer = App7020Question::getQuestionsToAnswer(Yii::app()->user->id, array("from" => $this->from, "count" => $this->count));
			$sectionTitle = Yii::t("standard", "Open questions");
			$sectionDescription = Yii::t("standard", "These are the questions assigned to you, where you and/or other experts already provided an answer. "
							. "<b>Open questions will be automaticcaly removed from here as soon as a best answer is choosen</b>");
		}

		$items = array();

		foreach ($questionsToAnswer as $value) {
			$items[] = array(
				'question_id' => $value['id'],
				'question' => $value['title'],
				'avatar' => Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $value['idUser']), true),
				'current_user_avatar' => Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => Yii::app()->user->idst), true),
				'owner' => CoreUser::getForamattedNames($value['idUser'],' '),
				'owner_channel_link' => Docebo::createLmsUrl('channels/index', array("#" => "/pchannel/" . $value['idUser'])),
				'views' => $value['views'],
				'update_date' => App7020Helpers::timeAgo($value['created']),
				'total_number_answers' => $value['answers'],
				'link_to_question' => Docebo::createApp7020Url('askTheExpert/index', array("#" => "/question/" . $value['id'])),
				'is_new' => $value['answers'] ? false : true,
				'ignored' => ($this->ignored === null) ? false : true,
				'ignore_state' => 0, /* 0-(hide ignore template) 1-(show before step) 2-( show after step)*/
				'type_uid' => 'question',
				'answer' => array(
					'answer_id' => $value['lastAnswerId'],
					'text' => $value['lastAnswerText']
				),
				'actions' => $this->_getActions($value['id']),
				'other_fields' => $this->_getOtherFields($value)
			);
		}
		$result = array(
			"success" => true,
			"title" => $sectionTitle,
			"description" => $sectionDescription,
			"questions_count" => $questionsToAnswerCount,
			"items" => $items
		);
		return $result;
	}

	/**
	 * Get Question Actions
	 */
	private function _getActions($idQuestion) {

		$actions = array();

		$actions[] = array(
			'action_type' => 'link',
			'action_uid' => self::ACTION_UID_VIEW_QUESTION,
			'action_params' => array(
				'url' => Docebo::createApp7020Url('askTheExpert/index', array("#" => "/question/" . $idQuestion))
			),
			'label' => Yii::t('standard', 'View question'),
			'icon_name' => 'fa-comment-o',
			'icon_color' => '#0465AC'
		);
		$actions[] = array(
			'action_type' => 'action',
			'action_uid' => self::ACTION_UID_QUICK_ANSWER,
			'label' => Yii::t('standard', 'Quick answer'),
			'icon_name' => 'fa-reply',
			'icon_color' => '#5DBE5D'
		);
		$actions[] = array(
			'action_type' => 'action',
			'action_uid' => self::ACTION_UID_IGNORE,
			'label' => Yii::t('standard', 'Silence question'),
			'icon_name' => 'fa-bell-slash-o',
			'icon_color' => '#333'
		);
		$actions[] = array(
			'action_type' => 'action',
			'action_uid' => self::ACTION_UID_UNIGNORE,
			'label' => Yii::t('standard', 'Notify me'),
			'icon_name' => 'fa-bell-o',
			'icon_color' => '#333'
		);

		return $actions;
	}

	/**
	 * Get Question Actions
	 */
	private function _getOtherFields($item) {

		$otherFields = array();

		if (!empty($item['idContent'])) {
			$assetObject = App7020Assets::model()->findByPk($item['idContent']);
			if ($assetObject->id) {
				$otherFields['asset'] = array(
					'asset_id' => (int) $assetObject->id,
					'asset_title' => $assetObject->title,
					'asset_page_url' => Docebo::createApp7020AssetsViewUrl($assetObject->id),
					'asset_preview' => App7020Assets::getAssetThumbnailUri($assetObject->id)
				);
			}
		}

		if (!empty($item['idLearningObject'])) {
			$learningOrganizationModel = LearningOrganization::model()->findByPk($item['idLearningObject']);
			if ($learningOrganizationModel->idOrg) {
				$otherFields['asset'] = array(
					'asset_id' => $learningOrganizationModel->idOrg,
					'asset_title' => $learningOrganizationModel->title,
					'asset_page_url' => Docebo::createLmsUrl('player', array('course_id' => $item['idCourse'], 'lo_key' => $learningOrganizationModel->idOrg)),
					'asset_preview' => CoreAsset::url($learningOrganizationModel->resource, CoreAsset::VARIANT_SMALL)
				);
			}
		}

		if (!empty($item['idChannel'])) {
			$channelObject = App7020Channels::model()->findByPk($item['idChannel']);
			if ($channelObject->id) {
				$translation = $channelObject->translation();
				$otherFields['channel'] = array(
					'channel_id' => $channelObject->id,
					'channel_title' => $translation['name'],
					'channel_url' => Docebo::createLmsUrl('channels/index', array("#" => "/channel/" . $channelObject->id)),
					'channel_icon' => $channelObject->icon_fa,
					'channel_icon_color' => $channelObject->icon_color,
					'channel_color' => $channelObject->icon_bgr
				);
			}
		}

		return $otherFields;
	}

}
