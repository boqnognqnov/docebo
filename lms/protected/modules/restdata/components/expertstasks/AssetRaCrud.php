<?php

/**
 *
 * @property integer $id_asset
 * @property array $item
 */
class AssetRaCrud extends RestData {

	/**
	 * Constructor
	 *
	 * @param array $params
	 */
	public function __construct($params = false) {
		parent::__construct($params);
	}

	/**
	 * Execute operation
	 */
	public function run() {
		if ($this->endpoint == RestData::ENDPOINT_ASSET_RA_IGNORE) {
			return $this->_assetIgnore();
		}

		if ($this->endpoint == RestData::ENDPOINT_ASSET_RA_UNIGNORE) {
			return $this->_assetUnignore();
		}
	}


	private function _assetIgnore(){
		$idAsset = $this->id_asset;
		$objectType = App7020IgnoreList::OBJECT_TYPE_ASSET;

		/* Add ASSET TO IGNORE LIST TABLE if not exist */
		if (App7020Assets::model()->findByPk($idAsset)) {
			if (!App7020IgnoreList::model()->findByAttributes(
				array(
					'idUser' => Yii::app()->user->idst,
					'idObject' => $idAsset,
					'objectType' => $objectType))) {

				$ignoreListModel = new App7020IgnoreList();
				$ignoreListModel->idUser = Yii::app()->user->idst;
				$ignoreListModel->idObject = $idAsset;
				$ignoreListModel->objectType = $objectType;
				$ignoreListModel->save();
			}
		}
	}

	private function _assetUnignore(){
		$idAsset = $this->id_asset;
		$objectType = App7020IgnoreList::OBJECT_TYPE_ASSET;

		$ignoreListModel = App7020IgnoreList::model()->findByAttributes(array('idUser' => Yii::app()->user->idst, 'idObject' => $idAsset, 'objectType' => $objectType));
		if ($ignoreListModel->id) {
			$ignoreListModel->delete();
		}
	}


}