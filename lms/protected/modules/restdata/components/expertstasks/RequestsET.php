<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class RequestsET extends RestData {

	const ACTION_UID_UNIGNORE = 'unignore_question';
	const ACTION_UID_IGNORE = 'ignore_question';
	const ACTION_UID_QUICK_ANSWER = 'quick_answer';
	const ACTION_UID_VIEW_QUESTION = 'view_question';

	/**
	 * Constructor
	 *
	 * @param string $params
	 */
	public function __construct($params = false) {
		parent::__construct($params);
	}

	public function run() {
		if ($this->only_new) {
			$requestsToSatisfyCount = count(App7020QuestionRequest::getRequestsToSatisfy(Yii::app()->user->id, array("onlyNew" => 1, "all" => 1)));
			$requestsToSatisfy = App7020QuestionRequest::getRequestsToSatisfy(Yii::app()->user->id, array("onlyNew" => 1, "from" => $this->from, "count" => $this->count));
			$sectionTitle = Yii::t("standard", "New requests");
			$sectionDescription = Yii::t("standard", "These are the new requests that you or other experts can take in charge");
		} elseif ($this->ignored) {
			$requestsToSatisfyCount = count(App7020QuestionRequest::getRequestsToSatisfy(Yii::app()->user->id, array("ignored" => 1, "all" => 1)));
			$requestsToSatisfy = App7020QuestionRequest::getRequestsToSatisfy(Yii::app()->user->id, array("ignored" => 1, "from" => $this->from, "count" => $this->count));
			$sectionTitle = Yii::t("standard", "Silenced");
			$sectionDescription = Yii::t("standard", "These are your silenced requests. You won't be notified for any change that happens. "
							. "You can re-enable the notifications or, if no action is taken, "
							. "<b>they will be automaticcaly removed from here as soon as a best answer is choosen. "
							. "Alternatively you can manually clean the silenced list.</b>");
		} else {
			$requestsToSatisfyCount = count(App7020QuestionRequest::getRequestsToSatisfy(Yii::app()->user->id, array("taken" => 1, "all" => 1)));
			$requestsToSatisfy = App7020QuestionRequest::getRequestsToSatisfy(Yii::app()->user->id, array("taken" => 1, "from" => $this->from, "count" => $this->count));
			$sectionTitle = Yii::t("standard", "In charge");
			$sectionDescription = Yii::t("standard", "These are the requests you took in charge. Don't forget to upload the requested asset"
							. " and spread the knowledge!!");
		}

		$items = array();

		foreach ($requestsToSatisfy as $value) {

			if (!empty($value)) {
	 
				$items[] = array(
					'question_id' => $value['idQuestion'],
					'question' => $value['title'],
					'avatar' => Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $value['idUser']), true),
					'current_user_avatar' => Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => Yii::app()->user->idst), true),
					'owner' => CoreUser::getForamattedNames($value['idUser'],' '),
					'owner_channel_link' => Docebo::createLmsUrl('channels/index', array("#" => "/pchannel/" . $value['idUser'])),
					'views' => $value['views'],
					'update_date' => App7020Helpers::timeAgo($value['created']),
					'total_number_answers' => $value['answers'],
					'link_to_question' => Docebo::createApp7020Url('askTheExpert/index', array("#" => "/question/" . $value['idQuestion'])),
					'is_new' => $value['answers'] ? false : true,
					'ignored' => ($this->ignored === null) ? false : true,
					'ignore_state' => 0, /* 0-(hide ignore template) 1-(show before step) 2-( show after step) */
					'type_uid' => 'request',
					'tooked' => ($value['idExpert']) ? true : false,
					'tooked_time' => App7020Helpers::timeAgo($value['tooked']),
					'can_upload' =>  ($value['idContent'] && $value['conversion_status'] > 6)  ? false : true,
					'conversion_status' => (int) $value['conversion_status'],
					'assign_state' => 0,
					'assigner_avatar' => Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $value['idExpert']), true),
					'answer' => array(
						'answer_id' => $value['lastAnswerId'],
						'text' => $value['lastAnswerText'],
						'author'=> CoreUser::getForamattedNames(App7020Answer::model()->findByPk($value['lastAnswerId'])->idUser,' ')
					),
					'actions' => $this->_getActions($value['idQuestion']),
					'other_fields' => $this->_getOtherFields($value)
				);
			}
		}

		$result = array(
			"success" => true,
			"title" => $sectionTitle,
			"description" => $sectionDescription,
			"questions_count" => $requestsToSatisfyCount,
			"items" => $items
		);
		return $result;
	}

	/**
	 * Get Question Actions
	 */
	private function _getActions($idQuestion) {

		$actions = array();

		$actions[] = array(
			'action_type' => 'link',
			'action_uid' => self::ACTION_UID_VIEW_QUESTION,
			'action_params' => array(
				'url' => Docebo::createApp7020Url('askTheExpert/index', array("#" => "/question/" . $idQuestion))
			),
			'label' => Yii::t('standard', 'View question'),
			'icon_name' => 'fa-comment-o',
			'icon_color' => '#0465AC'
		);
		$actions[] = array(
			'action_type' => 'action',
			'action_uid' => self::ACTION_UID_QUICK_ANSWER,
			'label' => Yii::t('standard', 'Quick answer'),
			'icon_name' => 'fa-reply',
			'icon_color' => '#5DBE5D'
		);
		$actions[] = array(
			'action_type' => 'action',
			'action_uid' => self::ACTION_UID_IGNORE,
			'label' => Yii::t('standard', 'Silence question'),
			'icon_name' => 'fa-bell-slash-o',
			'icon_color' => '#333'
		);
		$actions[] = array(
			'action_type' => 'action',
			'action_uid' => self::ACTION_UID_UNIGNORE,
			'label' => Yii::t('standard', 'Notify me'),
			'icon_name' => 'fa-bell-o',
			'icon_color' => '#333'
		);

		return $actions;
	}

	/**
	 * Get Question Actions
	 */
	private function _getOtherFields($item) {

		$otherFields = array();

		if (!empty($item['idContent'])) {
			$assetObject = App7020Assets::model()->findByPk($item['idContent']);
			if ($assetObject->id) {
				$otherFields['asset'] = array(
					'asset_id' => (int) $assetObject->id,
					'asset_title' => $assetObject->title,
					'asset_page_url' => Docebo::createApp7020AssetsViewUrl($assetObject->id),
					'asset_preview' => App7020Assets::getAssetThumbnailUri($assetObject->id)
				);
			}
		}

		if (!empty($item['idLearningObject'])) {
			$learningOrganizationModel = LearningOrganization::model()->findByPk($item['idLearningObject']);
			if ($learningOrganizationModel->idOrg) {
				$otherFields['asset'] = array(
					'asset_id' => $learningOrganizationModel->idOrg,
					'asset_title' => $learningOrganizationModel->title,
					'asset_page_url' => Docebo::createLmsUrl('player', array('course_id' => $item['idCourse'], 'lo_key' => $learningOrganizationModel->idOrg)),
					'asset_preview' => CoreAsset::url($learningOrganizationModel->resource, CoreAsset::VARIANT_SMALL)
				);
			}
		}

		if (!empty($item['idChannel'])) {
			$channelObject = App7020Channels::model()->findByPk($item['idChannel']);
			if ($channelObject->id) {
				$translation = $channelObject->translation();
				$otherFields['channel'] = array(
					'channel_id' => $channelObject->id,
					'channel_title' => $translation['name'],
					'channel_url' => Docebo::createLmsUrl('channels/index', array("#" => "/channel/" . $channelObject->id)),
					'channel_icon' => $channelObject->icon_fa,
					'channel_icon_color' => $channelObject->icon_color,
					'channel_color' => $channelObject->icon_bgr
				);
			}
		}

		return $otherFields;
	}

}
