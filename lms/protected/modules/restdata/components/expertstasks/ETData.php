<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ETData extends RestData {



	/**
	 * Constructor
	 *
	 * @param string $params
	 */
	public function __construct($params = false) {
		parent::__construct($params);
	}

	public function run() {
		$returnArray = array();
		$requests_incharge = RestData::factory(RestData::ENDPOINT_ET_REQUESTS, array('from' => 0, 'count' => 5));
		$requests_new = RestData::factory(RestData::ENDPOINT_ET_REQUESTS, array('only_new' => 1, 'from' => 0, 'count' => 5));
		$requests_ignored = RestData::factory(RestData::ENDPOINT_ET_REQUESTS, array('ignored' => 1, 'from' => 0, 'count' => 5));
		$questions_open = RestData::factory(RestData::ENDPOINT_ET_QUESTIONS, array('from' => 0, 'count' => 5));
		$questions_new = RestData::factory(RestData::ENDPOINT_ET_QUESTIONS, array('only_new' => 1, 'from' => 0, 'count' => 5));
		$questions_ignored = RestData::factory(RestData::ENDPOINT_ET_QUESTIONS, array('ignored' => 1, 'from' => 0, 'count' => 5));
		$assets_inreview = RestData::factory(RestData::ENDPOINT_ASSET_RA_IN_REVIEW, array('from' => 0, 'count' => 5));
		$assets_new = RestData::factory(RestData::ENDPOINT_ASSET_RA_NEW_ONLY, array('from' => 0, 'count' => 5));
		$assets_ignored = RestData::factory(RestData::ENDPOINT_ASSET_RA_SILENCED, array('from' => 0, 'count' => 5));
		$returnArray['requests']['incharge'] = $requests_incharge->run();
		$returnArray['requests']['new'] = $requests_new->run();
		$returnArray['requests']['ignored'] = $requests_ignored->run();
		$returnArray['questions']['open'] = $questions_open->run();
		$returnArray['questions']['new'] = $questions_new->run();
		$returnArray['questions']['ignored'] = $questions_ignored->run();
		$returnArray['assets']['inreview'] = $assets_inreview->run();
		$returnArray['assets']['new'] = $assets_new->run();
		$returnArray['assets']['ignored'] = $assets_ignored->run();
		return $returnArray;
	}
	
}
