<?php

/**
 *
 */
class Invitations extends RestData {

	public $assetObject;
	

	/**
	 * Constructor
	 *
	 * @param array $params
	 */
	public function __construct($params = false) {
		parent::__construct($params);
	}

	public function run() {
		if (!$this->id_asset || !$assetObject = App7020Assets::model()->findByPk($this->id_asset)) {
			$result = array(
				'success' => false,
				'error' => Yii::t("standard", "Invalid data provided")
			);
			return $result;
		} else {
			$this->assetObject = $assetObject;
		}
		
		if($this->endpoint == RestData::ENDPOINT_INVITE_GET_USERS){
			return $this->_getUsersForInvitation();
		}
		
		if($this->endpoint == RestData::ENDPOINT_INVITE_SET_USERS){
			return $this->_inviteUsers();
		}
		
	}
	
	private function _getUsersForInvitation(){
		return App7020Invitations::getVisibleUsers($this->id_asset);
	}
	
	private function _inviteUsers(){
		$invitedPeople = array();
		
		$items = $this->invited_items;
		foreach ($items as $item) {
			if (!in_array($item, $invitedPeople)) {
				$invitedPeople[] = $item;
				$invitationObject = new App7020Invitations();
				$invitationObject->idInvited = $item;
				$invitationObject->idContent = $this->id_asset;
				$invitationObject->idInviter = $this->inviter;
				$invitationObject->save(false);
			}
		}
		Yii::app()->event->raise('App7020Invitation', new DEvent($this, array('users' => $invitedPeople,
			'contentId' => $this->id_asset)));

		$redirect = $this->redirectToEditMode ? 1 : 0;

		return array('invitedPeople' => count($invitedPeople), 'contentId' => $this->id_asset, 'redirect' => $redirect);
	}
	
}
