<?php

/**
 * @author Stanimir Simeonov <s.simeonov@scavaline.com>
 */
class InviteToWatch extends RestData {

	/**
	 * 
	 * @var App7020Assets
	 */
	public $assetObject;

	/**
	 * Store all already invited user ids
	 * we use negative value  for default value
	 * @var array 
	 */
	private $assetInvitedUsers = array(-1);

	/**
	 * Store all prepared ids of users ready to be send for invite
	 * @var array 
	 */
	private $assetPreparedUsers = array(-1);

	/**
	 * Store all prepared ids for channels ready to be invited
	 * @var array 
	 */
	private $assetPreparedChannels = array(-1);

	/**
	 * Limit for channels's criteria
	 * @var integer 
	 */
	private $_channel_limit = 3;

	/**
	 * final user limit
	 * @var integer
	 */
	private $_user_limit = 5;

	/**
	 * Store all items send by front - end for invite 
	 * @var array 
	 */
	private $prepareObjects = array();

	/**
	 * Storage which user is invited and when 
	 * @var array 
	 */
	private $_invitedData = array();

	/**
	 * Url for default avatr
	 * @var string 
	 */
	private $default_avatar = null;

	/**
	 * Remote storage folder URL 
	 * @var string 
	 */
	private $avatar_path = null;

	/**
	 * General  DP arguments
	 * @var array 
	 */
	private $dataProviderArgs = array();

	/**
	 * Custom additional arguments by some called action
	 * @var array 
	 */
	private $dataAdditionalProviderArgs = array();

	/**
	 * Store all public channel; If we have selected public channel all users are prepared excerpt the invited 
	 * @var array 
	 */
	private $visibleToAllChannels = array();

	/**
	 * Is used some public channel
	 * @var boolean 
	 */
	private $_is_used_public_channel = false;

	/**
	 * When is used some public channel we have to store first value and use later for counters only once
	 * @var integer
	 */
	private $_public_channel = null;
	private $_errors = array();

	/**
	 * Return status of used or not bublic channel into invite to watch
	 * That will replace searching into algoritym channel to user to found prpeared users for invite by selected channel 
	 * 
	 * @return boolean
	 */
	public function getIsUsedPublicChannel() {
		return $this->_is_used_public_channel;
	}

	/**
	 * Constructor
	 *
	 * @param array $params
	 */
	public function __construct($params = false) {

		parent::__construct($params);
		$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
		$defaultLanguage = Lang::getBrowserCodeByCode(Settings::get('default_language', 'english'));

		$this->default_avatar = '';
		$this->avatar_path = $storage->absoluteBaseUrl . '/avatar/';

		//set defaults to providers 
		$this->dataProviderArgs = array(
			"idUser" => Yii::app()->user->idst,
			"ignoreGodAdminEffect" => true,
			//"search" => 'free',
			"ignoreVisallChannels" => false,
			"ignoreSystemChannels" => true,
			"appendEnabledEffect" => true,
			'ignoreInvited' => true,
			"igonreAvatar" => false,
			"ignoreSuspendedUsers" => true,
		);

		//found the asset and build the model 
		$this->assetObject = App7020Assets::model()->with('invitations')->findByPk($this->id_asset);

		//found all current invitations for user
		foreach ($this->assetObject->invitations as $invitation) {
			$this->assetInvitedUsers[] = $invitation->idInvited;
			$this->_invitedData[$invitation->idInvited] = $invitation->created;
		}
	}

	public function run() {
		//we have to check for actual action 
		if (empty($this->action)) {
			throw new Exception(Yii::t('app7020', 'Missing action for processing'), 500);
		}

		//we have to check for send id an asset 
		if (empty($this->id_asset)) {
			throw new Exception(Yii::t('app7020', 'Missing id for an asset'), 500);
		}
		$this->visibleToAllChannels = App7020Channels::getVisibleToAllChannels();

		//we have to correct arrays for return 
		switch ($this->action) {
			case 'subscribe':
			case 'unsubscribe':
			case 'search':
			case 'invite':
				//proccess all prepend for invite users 
				$prepared = $this->items;
				if (empty($this->items)) {
					$prepared = array();
				}
				if (!empty($this->invite)) {
					$new_for_prepare = $this->invite;
					$prepared[$new_for_prepare['id']] = $new_for_prepare;
				}
				$this->prepareObjects = $prepared;
				break;
			default:
				break;
		}


		//append all existing channels and users who are sended by front-end
		if (!empty($this->prepareObjects)) {
			foreach ($this->prepareObjects as $item) {
				if ((int) $item['typeId'] == 1) {
					$this->assetPreparedUsers[] = intval($item['id']);
				} else {
					$this->assetPreparedChannels[] = intval($item['id']);
				}
			}
		}
		$p_channel_intersect_value = array_intersect($this->visibleToAllChannels, $this->assetPreparedChannels);
		if (!empty($p_channel_intersect_value)) {
			$this->_is_used_public_channel = true;
			$this->_public_channel = $p_channel_intersect_value[key($p_channel_intersect_value)];
		}

		switch ($this->action) {
			case 'initial':
			case 'clear_all':
				$this->assetPreparedChannels = array(-1);
				$this->assetPreparedUsers = array(-1);
				return $this->_initial();
			case 'search':
				return $this->_search();
			case 'subscribe':
				return $this->_subscribe();
			case 'unsubscribe':
				return $this->_unsubscribe();
			case 'invite':
				return $this->_invite();
		}
	}

	/**
	 * Extract all channels as suggestions with all needed information  
	 * we have to use referrence to pull an existing array
	 * @param array $items
	 */
	protected function _extractChannels(&$suggestions) {
		$items = array();
		if ($this->action != 'initial') {
			if ($this->getIsUsedPublicChannel()) {
				return true;
			}
		}
		//get channels which my visibility filtered by idAsset
		$custom_arguments = array(
			'idAsset' => $this->assetObject->id,
			'idUser' => Yii::app()->user->idst
		);

		//build args with additional appended data
		$args = array_merge($this->dataProviderArgs, $custom_arguments);
		$args = array_merge($args, $this->dataAdditionalProviderArgs);
		$args['pagination'] = false;
		$args['search'] = $this->search;



		$dp = DataProvider::factory("UserChannels", $args)->provider();
		$dp->sort->defaultOrder = array(
			'id' => CSort::SORT_ASC,
			'name' => CSort::SORT_ASC
		);
		$p = new CPagination;
		$p->setPageSize($this->_channel_limit);
		$p->setCurrentPage(0);
		$dp->setPagination($p);



		//append items to common array
		$items = array_merge($items, $dp->data);
		unset($args['idAsset']);



		foreach ($items as $item_key => $item) {
			$items[$item_key]['status_invite'] = 0;
			if (in_array((int) $item['id'], $this->assetPreparedChannels)) {
				$items[$item_key]['status_invite'] = 1;
			}
		}


		//build array for FE 
		foreach ($items as $item) {
			$item_avatr = explode("|", $item['avatar']);
			$suggestions[$item['id']] = array(
				"id" => $item['id'],
                "name" => mb_strlen($item['name']) < 30 ? $item['name'] : mb_substr($item['name'], 0, 30) . '...',
				"firstname" => null,
				"lastname" => null,
				"username" => null,
				"type" => 'channel',
				"typeId" => 2,
				"avatar" => array("background" => $item_avatr[0], "color" => $item_avatr[1], "icon" => 'fa ' . $item_avatr[2]),
				"invited" => 0,
				"date" => null,
				"users" => App7020ChannelVisibility::getNumUsersVisibilirtyToChannels($item['id'], array(Yii::app()->user->idst))
			);
		}
	}

	/**
	 * Extract all users as suggestions with all needed information  
	 * we have to use referrene to pull an existing array
	 * @param array $items
	 */
	protected function _extractUsers(&$suggestions) {
		$items = array();

		if ($this->action != 'initial') {
			if ($this->getIsUsedPublicChannel()) {
				return true;
			}
		}

		//get channels which my visibility filtered by idAsset
		if (isset($this->dataProviderArgs['idUser'])) {
			unset($this->dataProviderArgs['idUser']);
		}


		$custom_arguments = array(
			'idAsset' => $this->assetObject->id,
			'customSelect' => array(
				'#alias#.userid' => 'username',
				'#alias#.firstname' => 'firstname',
				'#alias#.lastname' => 'lastname',
				"(CONCAT(#alias#.firstname, ' ', #alias#.lastname))" => 'fullname',
				"#alias#.avatar" => 'avatar',
			)
		);

		$args = array_merge($this->dataProviderArgs, $custom_arguments);
		$args = array_merge($args, $this->dataAdditionalProviderArgs);

		$args['idChannel'] = App7020ChannelAssets::getChannelsByContent($this->assetObject->id);


		//unset($args['idChannel']);
		//unset($args['idAsset']);
		$dp = DataProvider::factory($this->assetObject->isPrivate() ? "CommonUsers" : "AssetUsers", $args)->provider();

		$dp->sort->defaultOrder = array(
			'id' => CSort::SORT_ASC,
			'name' => CSort::SORT_ASC
		);

		//dfefine dthe final limit
		$limit = $this->_user_limit;
		if ($this->assetObject->isPrivate()) {
			$limit = 8;
		}

		$dp->pagination = array(
			'pageSize' => $limit,
			'currentPage' => 0
		);

		$items = $dp->data;

		foreach ($items as $item_key => $item) {

			$items[$item_key]['status_invite'] = 0;

			$conf_extr_channels = array(
				'idUser' => $item['id'],
				'idAsset' => $this->assetObject->id,
				'idsOnly' => true,
				'ignoreChannels' => $this->assetPreparedChannels
			);
			$visibleChannels = App7020Channels::extractUserChannels($conf_extr_channels, false);
			if (in_array($item['id'], $this->assetPreparedUsers) || array_intersect($visibleChannels)) {
				$items[$item_key]['status_invite'] = 1;
			}
			if (in_array($item['id'], array_keys($this->_invitedData))) {
				$items[$item_key]['status_invite'] = 2;
			}
		}
		foreach ($items as $item) {



			$suggestions[$item['id']] = array(
				"id" => $item['id'],
				"name" => trim($item['name']),
				"firstname" => $item['firstname'],
				"lastname" => $item['lastname'],
				"username" => ltrim($item['username'], '/'),
				"type" => 'user',
				"typeId" => 1,
				"avatar" => !empty($item['avatar']) ? $this->avatar_path . $item['avatar'] : '',
				"invited" => $item['status_invite'],
				"date" => isset($this->_invitedData[$item['id']]) ? $this->_invitedData[$item['id']] : null,
				"users" => false
			);
		}
	}

	/**
	 *
	 * @return int
	 */
	private function _invite() {
		try {
			if (empty($this->items)) {
				throw new Exception(Yii::t('app7020', 'Missing channels or users prepared to invite'), 500);
			}
			$result = array();
			//all users as unique value 
			$allUniqueUsers = $this->assetPreparedUsers;
			// array that should be stored 
			$array_to_batch_invite = array();


			foreach ($this->assetPreparedChannels as $channel_id) {
				//Get all users from channel visibility
				$allUniqueUsers = array_unique(array_merge($allUniqueUsers, App7020Channels::extractUserIds($channel_id, true, $this->assetObject->id)));
			}


			unset($allUniqueUsers[0]); // remove the -1 the 
			$already_invited = array_unique($this->assetInvitedUsers);
			if (isset($already_invited[0]) && $already_invited[0] == -1) {
				unset($already_invited[0]);
			}

			$allUniqueUsers = array_diff($allUniqueUsers, $already_invited); //remove already invited users for invite again

			foreach ($allUniqueUsers as $id_to_invite) {
				if ((int) $id_to_invite > 0 && $id_to_invite != Yii::app()->user->idst && $id_to_invite != $this->assetObject->userId) {
					$array_to_batch_invite[] = array(
						'idInvited' => $id_to_invite,
						'idContent' => $this->assetObject->id,
						'idInviter' => Yii::app()->user->idst
					);
				}
			}

			$result['type'] = 'invite';
			$result['success'] = true;
			if (!empty($array_to_batch_invite)) {
				$result['invitedUsers'] = App7020Invitations::saveInvitations($array_to_batch_invite, $this->assetObject->id);
			} else {
				$result['invitedUsers'] = 0;
			}
			return $result;
		} catch (Exception $ex) {
			Log::_($ex->getMessage());
			$result['success'] = false;
			$result['errors'] = $this->getErrors();
			return $result;
		}
	}

	/**
	 *
	 * @param type $rawSqlResult
	 * @return type
	 */
	private function _proccessSuggestions() {
		$suggestions = array();
		if (!$this->assetObject->isPrivate()) {
			$this->_extractChannels($suggestions);
		} else {
			$this->_user_limit = 8;
		}

		$this->_extractUsers($suggestions);
		//we provide to user friendly message if already is used publuc channel 
		if ($this->getIsUsedPublicChannel() || empty($suggestions) && $this->action != 'search') {
			$this->setError(Yii::t('app7020', 'You have invited all the users.'));
		}

		if (empty($suggestions) && $this->action == 'search') {
			$this->setError(Yii::t('app7020', 'No users found.'));
		}
		return $suggestions;
	}

	/**
	 *
	 * @return type
	 */
	private function _initial() {
		try {
			if (!isset($this->dataProviderArgs['ignoreInvited'])) {
				$this->dataProviderArgs['ignoreInvited'] = true;
			}
			$this->_ignoreSuggestions();
			$result = array();
			$result['type'] = 'initial';
			$result['success'] = true;
			$result['suggestions'] = $this->_proccessSuggestions();

			if ($this->action == 'clear_all') {
				$result['invitedUsers'] = $this->_countPreparedUsersForInvite(); //we decrement because init array has -1 as default;
			}
			$result['errors'] = $this->getErrors();
			return $result;
		} catch (Exception $ex) {
			Log::_($ex->getMessage());
			$result['success'] = false;
			$result['errors'] = $this->getErrors();
			return $result;
		}
	}

	/**
	 *
	 * @return type
	 */
	private function _search() {
		try {

			//we have to failback on blank search statement to initial case 
			if (isset($this->search) && strlen($this->search) == 0) {
				return $this->_initial();
			}
			$result = array();
			$this->_deatachIgnoredSuggestions();
			$this->dataAdditionalProviderArgs['ignoreUsers'][] = Yii::app()->user->idst;
			$this->dataAdditionalProviderArgs['search'] = $this->search;
			$result['type'] = 'search';
			$result['search'] = $this->search;
			$result['success'] = true;
			$result['suggestions'] = $this->_proccessSuggestions();
			$result['errors'] = $this->getErrors();
			return $result;
		} catch (Exception $ex) {
			Log::_($ex->getMessage());
			$result['success'] = false;
			$result['errors'] = $this->getErrors();
			return $result;
		}
	}

	/**
	 *
	 * @return type
	 */
	private function _subscribe() {
		try {
			if (!isset($this->dataProviderArgs['ignoreInvited'])) {
				$this->dataProviderArgs['ignoreInvited'] = true;
			}
			$this->_ignoreSuggestions();
			$result = array();
			$result['type'] = 'subscribe';
			$result['success'] = true;
			$result['suggestions'] = $this->_proccessSuggestions();
			$result['prepare'] = $this->prepareObjects;
			$result['invitedUsers'] = $this->_countPreparedUsersForInvite(); //we decrement because init array has -1 as default;
			$result['errors'] = $this->getErrors();
			return $result;
		} catch (Exception $ex) {
			Log::_($ex->getMessage());
			$result['success'] = false;
			$result['errors'] = $this->getErrors();
			return $result;
		}
	}

	/**
	 * Return number of total prepared users for invite
	 * @return int
	 */
	function _countPreparedUsersForInvite() {
		$count = count($this->assetPreparedUsers) - 1; // i decrease the initial negative default value used in diferent criterias

		if ($this->getIsUsedPublicChannel()) {
			return App7020ChannelVisibility::getNumUsersVisibilirtyToChannels($this->_public_channel, array(Yii::app()->user->idst));
		}
		foreach ($this->assetPreparedChannels as $channel_id) {
			$count += App7020ChannelVisibility::getNumUsersVisibilirtyToChannels($channel_id, array_merge($this->assetPreparedUsers, array(Yii::app()->user->idst)));
		}
		return $count;
	}

	/**
	 *
	 * @return type
	 */
	private function _unsubscribe() {
		try {
			if (!isset($this->dataProviderArgs['ignoreInvited'])) {
				$this->dataProviderArgs['ignoreInvited'] = true;
			}
			$result = array();
			$this->_ignoreSuggestions();
			$result['type'] = 'unsubscribe';
			$result['success'] = true;
			$result['suggestions'] = $this->_proccessSuggestions();
			$result['prepare'] = $this->prepareObjects;
			$result['invitedUsers'] = $this->_countPreparedUsersForInvite(); //we decrement because init array has -1 as default;
			$result['errors'] = $this->getErrors();
			return $result;
		} catch (Exception $ex) {
			Log::_($ex->getMessage());
			$result['success'] = false;
			$result['errors'] = $this->getErrors();
			return $result;
		}
	}

	/**
	 * 
	 * @param array $appendToIgnoreArray
	 * @return boolean
	 */
	function _ignoreSuggestions($appendToIgnoreArray = array()) {
		try {
			$this->dataAdditionalProviderArgs['ignoreUsers'] = $this->assetPreparedUsers;
			$this->dataAdditionalProviderArgs['ignoreUsers'][] = Yii::app()->user->idst;
			$this->dataAdditionalProviderArgs['ignoreChannels'] = $this->assetPreparedChannels;
			$appendedUsersForIgnore = array();

			if (!$this->getIsUsedPublicChannel()) {

				foreach ($this->assetPreparedChannels as $channel_id) {
					//Get all users from channel visibility
					$appendedUsersForIgnore = array_unique(array_merge($appendedUsersForIgnore, App7020Channels::extractUserIds($channel_id, true, $this->assetObject->id)));
				}
			}
			$this->dataAdditionalProviderArgs['ignoreUsers'] = array_merge($this->dataAdditionalProviderArgs['ignoreUsers'], $appendedUsersForIgnore);
			//we have to add additional array if it is needed 
			if (!empty($appendedUsersForIgnore)) {
				$this->dataAdditionalProviderArgs['ignoreUsers'] = array_merge($this->dataAdditionalProviderArgs['ignoreUsers'], $appendToIgnoreArray);
			}
			return true;
		} catch (Exception $ex) {
			Log::_($ex->getMessage());
			return false;
		}
	}

	/**
	 * Deatch ignoring criteria for suggesions 
	 */
	function _deatachIgnoredSuggestions() {
		if (isset($this->dataAdditionalProviderArgs['ignoreChannels'])) {
			unset($this->dataAdditionalProviderArgs['ignoreChannels']);
		}
		if (isset($this->dataAdditionalProviderArgs['ignoreChannels'])) {
			unset($this->dataAdditionalProviderArgs['ignoreUsers']);
		}

		if (isset($this->dataProviderArgs['ignoreInvited'])) {
			unset($this->dataProviderArgs['ignoreInvited']);
		}
	}

	/**
	 * Get all the errors 
	 * @return array
	 */
	public function getErrors() {
		return $this->_errors;
	}

	/**
	 * set stack from errors
	 * @param array $errors
	 * @return \InviteToWatch
	 */
	public function setErrors($errors) {
		$this->_errors = $errors;
		return $this;
	}

	/**
	 * Set/Append new erro
	 * @param string $error
	 * @param string|integer $code
	 * @return \InviteToWatch
	 */
	public function setError($error, $code = false) {
		if ($code) {
			$this->_errors[] = $error;
		} else {
			$this->_errors[$code] = $error;
		}
		return $this;
	}

}
