<?php
/**
 *
 */
class AnswerItem extends stdClass 
{
    
    private $item;
    private $endpoint = false;
    private $endpointParams;
	
	const ACTION_UID_DELETE_ANSWER = "deleteAnswer";
	const ACTION_UID_EDIT = "edit";
    
    
    /**
     *
     * @param array $item
     * @param stdClass $creator
     */
    public function __construct($item, $creator) {
        $this->item = $item;
        if (is_a($creator, "RestData")) {
            $this->endpoint = $creator->endpoint;
            $this->endpointParams = $creator->endpointParams;
        }
    }
    
    /**
     * 
     * @return array
     */
    public function getData() {
        $item = $this->item;
        $result = array(
            //"avatar"            => CoreUser::getAvatarByUserId($item['idUser']),
            "avatar"            => Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $item['idUser']),true),
            "author"            => CoreUser::getForamattedNames($item["idUser"], " ", false),
			"author_id"			=> $item["idUser"],
            "is_expert"         => App7020Experts::model()->findByAttributes(array("idUser" => $item['idUser']))->id ? true : false, 
            "id_answer"         => $item["id"],
            "text"              => $item["content"],
            "is_best_answer"    => $item["bestAnswer"] == 2,
            "likes"             => $item["likes"],
            "dislikes"          => $item["dislikes"],
            "actions"           => $this->getActions(),
			"update_date"		=> App7020Helpers::timeAgo($this->item['created']),
			"readOnly"			=> $item["idUser"] == Yii::app()->user->idst
        );
        return $result;
    }

    /**
     * 
     * @return array
     */
    private function getActions() {
        
        $actions = array();
        $viewerId = Yii::app()->user->id;

        if ($viewerId == $this->item["idUser"]) {
            $actions[] = array(
                'action_type' => 'drop_down',
				'action_uid' => self::ACTION_UID_EDIT,
                'action_params' => array(
                    'url' => 'javascript::'
                ),
                'label' => Yii::t('standard', 'Edit'),
                'text_color' => '#333333'
            );
            $actions[] = array(
                'action_type' => 'drop_down',
				'action_uid' => self::ACTION_UID_DELETE_ANSWER,
                'action_params' => array(
                    'url' => 'javascript::'
                ),
                'label' => Yii::t('standard', 'Delete'),
                'text_color' => '#e84b3c'
            );
        } elseif (Yii::app()->user->getIsGodadmin()) {
			$actions[] = array(
                'action_type' => 'drop_down',
				'action_uid' => self::ACTION_UID_DELETE_ANSWER,
                'action_params' => array(
                    'url' => 'javascript::'
                ),
                'label' => Yii::t('standard', 'Delete'),
                'text_color' => '#e84b3c'
            );
		}
		
        return $actions;
    }

}