<?php

/**
 * 
 * @property integer $id_question
 * @property string $followLabel
 * @property bool $followed
 * @property array $item
 */
class QuestionCrud extends RestData {

	/**
	 * Constructor
	 *
	 * @param array $params
	 */
	public function __construct($params = false) {
		parent::__construct($params);
	}

	/**
	 * Execute operation
	 */
	public function run() {
		$result = array();

		//Save or Update a question
		if ($this->endpoint == RestData::ENDPOINT_SAVE_QUESTION) {

			if (!$this->id_question) {
				//SAVING NEW QUESTION
				$operationResult = $this->_saveNewQuestion();
				if ($operationResult === true) {

					$questionObject = App7020Question::model()->findByPk($this->id_question);
					$actions = array();

					$actions[] = array(
						'action_type' => 'drop_down',
						'action_uid' => QuestionItem::ACTION_UID_FOLLOW,
						'label' => Yii::t('standard', 'Follow'),
						'text_color' => '#333333'
					);

					$actions[] = array(
						'action_type' => 'drop_down',
						'action_uid' => QuestionItem::ACTION_UID_EDIT,
						'action_params' => array(
							'url' => 'javascript::'
							),
						'label' => Yii::t('standard', 'Edit'),
						'text_color' => '#333333'
					);

					$actions[] = array(
						'action_type' => 'drop_down',
						'action_uid' => QuestionItem::ACTION_UID_DELETE_QUESTION,
						'action_params' => array(
							'url' => 'javascript::'
							),
						'label' => Yii::t('standard', 'Delete'),
						'text_color' => '#e84b3c'
					);

					$qObject = array(
						"question_id" => (int) $questionObject->id,
						//"avatar" => CoreUser::getAvatarByUserId($questionObject->idUser),
						"avatar" => Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $questionObject->idUser), true),
						"question" => $questionObject->title,
						"owner" => CoreUser::getForamattedNames($questionObject->idUser),
						"owner_id" => (int) $questionObject->idUser,
					    "owner_channel_link" => Docebo::createLmsUrl('channels/index', array("#" => "/pchannel/".$questionObject->idUser)),
						"views" => 0,
						"update_date" => App7020Helpers::timeAgo($questionObject->created),
						"total_number_answers" => 0,
						"total_number_new_answers" => 0,
						"following_icon" => "", // @TODO
						"is_following" => false,
						"is_new" => true,
						"have_best_answer" => false,
						"answer" => array(),
						"actions" => $actions,
						"other_fields" => $this->getOtherFields($questionObject),
						"tags" => array(),
					);
					if ($questionObject->idContent) {
						Yii::app()->event->raise('App7020NewContentsQuestion', new DEvent($this, array('questionId' => $questionObject->id)));
					}
					$result = array(
						"success" => true,
						"question" => $qObject
					);
				} else {
					$result = array(
						"success" => false,
						"errors" => $operationResult
					);
				}
			} else {
				//EDITING AN EXISTING QUESTION
				$operationResult = $this->_editQuestion();
				if ($operationResult === true) {
					$result = array(
						"success" => true,
						"id_question" => $this->id_question
					);
				} else {
					$result = array(
						"success" => false,
						"errors" => $operationResult
					);
				}
			}
		}

		//Delete a question
		if ($this->endpoint == RestData::ENDPOINT_DELETE_QUESTION) {
			$operationResult = $this->_deleteQuestion();
			if ($operationResult === true) {
				$result = array(
					"success" => true,
					"id_question" => $this->id_question
				);
			} else {
				$result = array(
					"success" => false,
					"errors" => $operationResult
				);
			}
		}

		//Follow a question
		if ($this->endpoint == RestData::ENDPOINT_FOLLOW_QUESTION) {
			$operationResult = $this->_followQuestion();
			if ($operationResult === true) {
				$result = array(
					"success" => true,
					"id_question" => $this->id_question,
					"followLabel" => $this->followLabel,
					"followed" => $this->followed
				);
			} else {
				$result = array(
					"success" => false,
					"errors" => $operationResult
				);
			}
		}

		return $result;
	}

	protected function _saveNewQuestion() {

		$returnErrors = array();
		if(PluginManager::isPluginActive('MultidomainApp')){

			if(!$this->id_channel && !$this->id_content){
				$returnErrors['channel'][] = Yii::t('standard', 'You must select a channel');
				return $returnErrors;
			} else if(!$this->id_channel && isset($this->id_content)){
				$channels = App7020Assets::model()->findByPk($this->id_content)->getCheckedChannels();
				if(count($channels) > 0) {
					$this->id_channel = App7020Assets::model()->findByPk($this->id_content)->getCheckedChannels()[0];
				}
			}
		} else {
			if (($this->is_request && !$this->id_channel)) {
				$returnErrors['channel'][] = Yii::t('standard', 'You must select a channel');
				return $returnErrors;
			}
		}

		$newQuestion = new App7020Question();
		$newQuestion->title = $this->title;
		$newQuestion->idContent = $this->id_content;
		$newQuestion->idLearningObject = $this->id_lo;
		$newQuestion->idUser = Yii::app()->user->id;
		$newQuestion->save();

		if ($newQuestion->id) {
			Yii::app()->event->raise('UserReachedAGoal', new DEvent($this, array('goal' => 0,'user'=>Yii::app()->user->idst)));
			//ADD tags if exists
			if ($this->tags) {
				$tagsArray = explode(',', $this->tags);
				foreach ($tagsArray as $value) {
					$tagObject = App7020Tag::model()->findByAttributes(array('tagText' => $value));
					if ($tagObject->id) {
						$qtObject = new App7020QuestionsTags();
						$qtObject->idQuestion = $newQuestion->id;
						$qtObject->idTag = $tagObject->id;
						$qtObject->save();
					}
				}
			}
			//Assign question to channel if is present
			if ($this->id_channel) {
				if ($idChannel = App7020Channels::model()->findByPk($this->id_channel)->id) {
					$cqObject = new App7020ChannelQuestions();
					$cqObject->idChannel = $idChannel;
					$cqObject->idQuestion = $newQuestion->id;
					if ($cqObject->save()) {
						Yii::app()->event->raise(
								'App7020NewQuestion', 
								new DEvent(
										$this, 
										array(
											'users' =>App7020ChannelExperts::getChannelExpertIds($idChannel),
											'questionId' => $newQuestion->id
										)
								)
						);
					}
				}
			}
			//Create new request if is present
			if ($this->is_request) {
				$requestModel = new App7020QuestionRequest();
				$requestModel->idQuestion = $newQuestion->id;
				$requestModel->save();
			}

			$this->id_question = $newQuestion->id;
			return true;
		} else {

			$errors = $newQuestion->getErrors();
			foreach ($errors as $key => $error) {
				if ($error && is_array($error)) {
					foreach ($error as $errorText) {
						$returnErrors[$key][] = $errorText;
					}
				}
			}

			return $returnErrors;
		}
	}

	protected function _editQuestion() {

		$returnErrors = array();
		$modelQuestion = $this->_checkQuestionCompability();
		if (($modelQuestion instanceof App7020Question) === false) {
			return $modelQuestion;
		}

		$modelQuestion->title = $this->title;
		$operationResult = $modelQuestion->save();
		if ($operationResult === true) {
			return true;
		} else {
			$errors = $modelQuestion->getErrors();
			foreach ($errors as $key => $error) {
				if ($error && is_array($error)) {
					foreach ($error as $errorText) {
						$returnErrors[$key][] = $errorText;
					}
				}
			}
			return $returnErrors;
		}
	}

	protected function _deleteQuestion() {

		$modelQuestion = $this->_checkQuestionCompability();
		if (($modelQuestion instanceof App7020Question) === false) {
			return $modelQuestion;
		}

		return $modelQuestion->delete();
	}

	protected function _followQuestion() {

		$modelQuestion = $this->_checkQuestionCompability(false);
		if (($modelQuestion instanceof App7020Question) === false) {
			return $modelQuestion;
		}

		$followed = App7020QuestionFollow::model()->findByAttributes(array('idQuestion' => $this->id_question, 'idUser' => Yii::app()->user->idst));
		if ($followed->id) {
			$this->followLabel = Yii::t("standard", "Follow");
			$this->followed = false;
			return $followed->delete();
		} else {
			$followed = new App7020QuestionFollow();
			$followed->idQuestion = $this->id_question;
			$followed->idUser = Yii::app()->user->idst;
			$resultOperation = $followed->save();
			if ($resultOperation) {
				$this->followLabel = Yii::t("standard", "Unfollow");
				$this->followed = true;
				return true;
			} else {
				$returnErrors = array();
				$errors = $followed->getErrors();
				foreach ($errors as $key => $error) {
					if ($error && is_array($error)) {
						foreach ($error as $errorText) {
							$returnErrors[$key][] = $errorText;
						}
					}
				}
				return $returnErrors;
			}
		}
	}

	protected function _checkQuestionCompability($checkOwner = true) {
		//check if id_question is provided
		if (!$this->id_question) {
			$returnErrors = array(0 => Yii::t("standard", "Invalid question"));
			return $returnErrors;
		}

		$modelQuestion = App7020Question::model()->findByPk($this->id_question);

		//check if correct question ID is provided
		if (!$modelQuestion->id) {
			$returnErrors = array(0 => Yii::t("standard", "Invalid question"));
			return $returnErrors;
		}

		if ($checkOwner) {
			//check if the logged user is the owner of the question
			if ($modelQuestion->idUser != Yii::app()->user->id && !Yii::app()->user->getIsGodadmin()) {
				$returnErrors = array(0 => Yii::t("standard", "You have no rights to edit this question"));
				return $returnErrors;
			}
		}

		return $modelQuestion;
	}

	/**
	 * Get question other fields
	 */
	private function getOtherFields($questionObject) {
		$otherFields = array();

		if (!empty($questionObject->idContent)) {
			$assetObject = App7020Assets::model()->findByPk($questionObject->idContent);
			if ($assetObject->id) {
				$otherFields['asset'] = array(
					'asset_id' => (int) $assetObject->id,
					'asset_title' => $assetObject->title,
				    'asset_page_url' => Docebo::createApp7020AssetsViewUrl($assetObject->id),
					'asset_preview' => App7020Assets::getAssetThumbnailUri($assetObject->id)
				);
			}
		}

		if (!empty($questionObject->idLearningObject)) {
			$learningOrganizationModel = LearningOrganization::model()->findByPk($questionObject->idLearningObject);
			if ($learningOrganizationModel->idOrg) {
				$otherFields['asset'] = array(
					'asset_id' => $learningOrganizationModel->idOrg,
					'asset_title' => $learningOrganizationModel->title,
					'asset_page_url' => Docebo::createLmsUrl('player', array('course_id' => $questionObject->idCourse, 'lo_key' => $learningOrganizationModel->idOrg)),
					'asset_preview' => CoreAsset::url($learningOrganizationModel->resource, CoreAsset::VARIANT_SMALL)
				);
			}
		}

		if ($cqObject = App7020ChannelQuestions::model()->findByAttributes(array("idQuestion" => $questionObject->id))) {
			$channelObject = App7020Channels::model()->findByPk($cqObject->idChannel);

			if ($channelObject->id) {
				$translation = $channelObject->translation();
				$otherFields['channel'] = array(
					'channel_id' => $channelObject->id,
					'channel_title' => $translation['name'],
				    'channel_url' => Docebo::createLmsUrl('channels/index', array("#" => "/channel/".$channelObject->id)),
					'channel_icon' => $channelObject->icon_fa,
					'channel_icon_color' => $channelObject->icon_color,
					'channel_color' => $channelObject->icon_bgr
				);
			}
		}

		return $otherFields;
	}
	private function _viewQuestion(){
		$idQuestion = $this->id_question;
		App7020Question::viewQuestion($idQuestion);
		$questionObject = App7020Question::model()->findByPk($idQuestion);
		return count($questionObject->viewes);
	}

}
