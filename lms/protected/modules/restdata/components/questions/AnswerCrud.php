<?php

/**
 * 
 * @property integer $id_answer
 * @property integer $id_question
 * @property string $markLabel
 * @property bool $liked
 * @property integer $likes
 * @property integer $dislikes
 */
class AnswerCrud extends RestData {

	/**
	 * Constructor
	 *
	 * @param array $params
	 */
	public function __construct($params = false) {
		parent::__construct($params);
	}

	/**
	 * Execute operation
	 */
	public function run() {
		$result = array();

		//Save or Update a answer
		if ($this->endpoint == RestData::ENDPOINT_SAVE_ANSWER) {

			if (!$this->id_answer) {
				//SAVING NEW ANSWER
				$operationResult = $this->_saveNewAnswer();
				if ($operationResult === true) {

					$answerItem = $this->getAnswerItem();
					$answerItem["actions"] = array(
						array(
							'action_type' => 'drop_down',
							'action_uid' => AnswerItem::ACTION_UID_EDIT,
							'action_params' => array(
								'url' => 'javascript::'
							),
							'label' => Yii::t('standard', 'Edit'),
							'text_color' => '#333333'
						),
						array(
							'action_type' => 'drop_down',
							'action_uid' => AnswerItem::ACTION_UID_DELETE_ANSWER,
							'action_params' => array(
								'url' => 'javascript::'
							),
							'label' => Yii::t('standard', 'Delete'),
							'text_color' => '#e84b3c'
						)
					);

					$result = array(
						"success" => true,
						"answer" => $answerItem,
						"question" => $this->getQuestionItem()
					);
				} else {
					$result = array(
						"success" => false,
						"errors" => $operationResult
					);
				}
			} else {
				//EDITING AN EXISTING ANSWER
				$operationResult = $this->_editAnswer();
				if ($operationResult === true) {
					$result = array(
						"success" => true,
						"answer" => $this->getAnswerItem(),
						"question" => $this->getQuestionItem()
					);
				} else {
					$result = array(
						"success" => false,
						"errors" => $operationResult
					);
				}
			}
		}

		//Delete an answer
		if ($this->endpoint == RestData::ENDPOINT_DELETE_ANSWER) {
			$operationResult = $this->_deleteAnswer();
			if ($operationResult === true) {
				$result = array(
					"success" => true,
					"question" => $this->getQuestionItem()
				);
			} else {
				$result = array(
					"success" => false,
					"errors" => $operationResult
				);
			}
		}

		// Like/Dislike an answer
		if ($this->endpoint == RestData::ENDPOINT_LIKE_ANSWER) {
			$operationResult = $this->_likeAnswer();
			if ($operationResult === true) {
				$result = array(
					"success" => true,
					"id_answer" => $this->id_answer,
					"likes" => count(App7020AnswerLike::model()->findAllByAttributes(array('idAnswer' => $this->id_answer, 'type' => 1))),
					"dislikes" => count(App7020AnswerLike::model()->findAllByAttributes(array('idAnswer' => $this->id_answer, 'type' => 2)))
				);
			} else {
				$result = array(
					"success" => false,
					"errors" => $operationResult
				);
			}
		}

		// Mark/Unmark as best answer
		if ($this->endpoint == RestData::ENDPOINT_MARK_ANSWER) {
			$operationResult = $this->_markAnswer();
			if ($operationResult === true) {
				$result = array(
					"success" => true,
					"id_answer" => $this->id_answer,
					"question" => $this->getQuestionItem()
				);
			} else {
				$result = array(
					"success" => false,
					"errors" => $operationResult
				);
			}
		}

		return $result;
	}

	protected function _saveNewAnswer() {
 		$returnErrors = array();
		if (!$this->id_question || !App7020Question::model()->findByPk($this->id_question)->id) {
			$returnErrors[] = Yii::t("standard", "Invalid question");
			return $returnErrors;
		}

		if(!App7020Question::canAnswerQuestion($this->id_question)){
			$returnErrors[] = Yii::t("standard", "Can't answer to this question");
			return $returnErrors;
		}

		$newAnswer = new App7020Answer();
		$newAnswer->content = preg_replace('!\s+!', ' ', html_entity_decode($this->title));
		$newAnswer->idQuestion = $this->id_question;
		$newAnswer->idUser = Yii::app()->user->id;
		$newAnswer->bestAnswer = 1;
		$newAnswer->save();
		if ($newAnswer->id) {
			$this->id_answer = $newAnswer->id;
			
			$mentionedIds = App7020Helpers::getMentionedUsers($newAnswer->content);
			if (!empty($mentionedIds)) {
				Yii::app()->event->raise('App7020MentionedAnswer', new DEvent($this, array(
					'mentionedIds' => $mentionedIds,
					'questionId' => $newAnswer->idQuestion,
					'mentionedByUserId' => $newAnswer->idUser,
						)));
			}
			
			
			return true;
		} else {

			$errors = $newAnswer->getErrors();
			foreach ($errors as $error) {
				if ($error && is_array($error)) {
					foreach ($error as $errorText) {
						$returnErrors[] = $errorText;
					}
				}
			}
			return $returnErrors;
		}
	}

	protected function _editAnswer() {

		$returnErrors = array();
		$modelAnswer = $this->_checkAnswerCompability();
		if (($modelAnswer instanceof App7020Answer) === false) {
			return $modelAnswer;
		}

		$modelAnswer->content = $this->title;
		$operationResult = $modelAnswer->save();
		if ($operationResult === true) {
			return true;
		} else {
			$errors = $modelAnswer->getErrors();
			foreach ($errors as $error) {
				if ($error && is_array($error)) {
					foreach ($error as $errorText) {
						$returnErrors[] = $errorText;
					}
				}
			}
			return $returnErrors;
		}
	}

	protected function _deleteAnswer() {

		$modelAnswer = $this->_checkAnswerCompability();
		if (($modelAnswer instanceof App7020Answer) === false) {
			return $modelAnswer;
		}
		$this->id_question = $modelAnswer->idQuestion;
		return $modelAnswer->delete();
	}

	protected function _likeAnswer() {

		$modelAnswer = $this->_checkAnswerCompability(false);
		if (($modelAnswer instanceof App7020Answer) === false) {
			return $modelAnswer;
		}
		//like_vector must be 1 or 2. 1-like, 2-unlike
		if (!in_array($this->like_vector, array(1, 2))) {
			return Yii::t("standard", "Invalid data provided");
		}

		$checkLikedModel = App7020AnswerLike::model()->findByAttributes(array('idAnswer' => $this->id_answer, 'idUser' => Yii::app()->user->idst));
		if (!$checkLikedModel) {
			$likeModel = new App7020AnswerLike();
			$likeModel->idAnswer = $this->id_answer;
			$likeModel->idUser = Yii::app()->user->idst;
			$likeModel->type = $this->like_vector;
			$likeModel->save();
		} else {
			$checkLikedModel->type = $this->like_vector;
			$checkLikedModel->save();
		}
		return true;
	}

	protected function _markAnswer() {

		$returnErrors = array();

		$modelAnswer = $this->_checkAnswerCompability(false);
		if (($modelAnswer instanceof App7020Answer) === false) {
			return $modelAnswer;
		}

		$modelQuestion = App7020Question::model()->findByPk($this->id_question);
		$checkAnswerOwner = App7020Answer::model()->findByAttributes(array('id' => $this->id_answer, 'idUser' => Yii::app()->user->idst));

		//If the logged user is the owner of the answer or is not the owner of the question, 
		//he/she cannot mark any answer as best 
		//or unmark the best answers
		if (!$modelQuestion->id || $modelQuestion->idUser != Yii::app()->user->idst || $checkAnswerOwner->id) {
			$returnErrors[] = Yii::t("standard", "You have no rights to mark/unmark this answer");
			return $returnErrors;
		}

		if ($modelAnswer->bestAnswer == 1) {
			//Mark the answer as best
			//first unmark the other answers of the question
			App7020Answer::model()->updateAll(array('bestAnswer' => 1), 'idQuestion = "' . $this->id_question . '"');

			//mark selected answer as best
			$modelAnswer->bestAnswer = 2;
			$modelAnswer->save(false);

			//close the question
			$modelQuestion->open = 0;
			$modelQuestion->save(false);

			//send notification
			Yii::app()->event->raise('App7020BestAnswer', new DEvent($this, array('answerId' => $this->id_answer)));
			Yii::app()->event->raise('UserReachedAGoal', new DEvent($this, array('goal' => 3,'user'=>$modelAnswer->idUser)));

			return true;
		} elseif ($modelAnswer->bestAnswer == 2) {
			//unmark selected answer
			$modelAnswer->bestAnswer = 1;
			$modelAnswer->save(false);

			//open the question
			$modelQuestion->open = 1;
			$modelQuestion->save(false);

			return true;
		} else {
			$returnErrors[] = Yii::t("standard", "Cannot mark/unmark this answer");
			return $returnErrors;
		}
	}

	protected function _checkAnswerCompability($checkOwner = true) {
		//check if id_answer is provided
		if (!$this->id_answer) {
			$returnErrors = array(0 => Yii::t("standard", "Invalid answer 1"));
			return $returnErrors;
		}

		$modelAnswer = App7020Answer::model()->findByPk($this->id_answer);

		//check if correct answer ID is provided
		if (!$modelAnswer->id) {
			$returnErrors = array(0 => Yii::t("standard", "Invalid answer"));
			return $returnErrors;
		}

		if ($checkOwner) {
			//check if the logged user is the owner of the answer
			if ($modelAnswer->idUser != Yii::app()->user->id && !Yii::app()->user->getIsGodadmin()) {
				$returnErrors = array(0 => Yii::t("standard", "You have no rights to edit/delete this answer"));
				return $returnErrors;
			}
		}
		return $modelAnswer;
	}

	public function getAnswerItem() {

		$answerModel = App7020Answer::model()->findByPk($this->id_answer);
		$userModel = CoreUser::model()->findByPk($answerModel->idUser);
		$likes = count(App7020AnswerLike::model()->findAllByAttributes(array('idAnswer' => $this->id_answer, 'type' => 1)));
		$dislikes = count(App7020AnswerLike::model()->findAllByAttributes(array('idAnswer' => $this->id_answer, 'type' => 2)));

		$result = array(
			"avatar" => Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $userModel->idst), true),
			"author" => CoreUser::getForamattedNames($userModel->idst, " ", false),
			"author_id" => $userModel->idst,
			"is_expert" => App7020Experts::model()->findByAttributes(array("idUser" => $userModel->idst))->id ? true : false,
			"id_answer" => $this->id_answer,
			"text" => $answerModel->content,
			"is_best_answer" => $answerModel->bestAnswer == 2,
			"likes" => $likes,
			"readOnly" => $answerModel->idUser == Yii::app()->user->idst,
			"dislikes" => $dislikes,
			"update_date" => App7020Helpers::timeAgo($answerModel->created)
		);

		return $result;
	}

	public function getQuestionItem() {
		$answer = array();
		if ($this->endpoint == RestData::ENDPOINT_DELETE_ANSWER) {
			$questionId = $this->id_question;
		} else {
			$questionId = App7020Answer::model()->findByPk($this->id_answer)->idQuestion;
		}


		$bestLastAnswer = App7020Question::getLastAnswerByQuestionId($questionId);

		if ($bestLastAnswer) {
			$answer = array(
				'answer_id' => $bestLastAnswer['id'],
				'text' => $bestLastAnswer['content'],
				'is_best_answer' => $bestLastAnswer['bestAnswer'] == 2,
				'likes' => count(App7020AnswerLike::model()->findAllByAttributes(array('idAnswer' => $bestLastAnswer['id'], 'type' => 1))),
				'dislikes' => count(App7020AnswerLike::model()->findAllByAttributes(array('idAnswer' => $bestLastAnswer['id'], 'type' => 2)))
			);
		}

		return array('answer' => $answer, 'question_id' => $questionId);
	}

}
