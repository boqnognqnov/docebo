<?php

/**
 * 
 * 
 */
class QuestionsData extends RestData {

	/**
	 * Constructor 
	 * 
	 * @param array $params
	 */
	public function __construct($params = false) {
		parent::__construct($params);
	}

	public function run() {

		// Build parameters for data provider
		$params = $this->buildParamsForDp();
		if ($params['options']) {
			$options = json_decode($params['options']);
			foreach ($options as $value) {
				if ($value->name == 'sort') {
					$params['sort'] = $value->value;
				}
				if ($value->name == 'show') {
					switch ($value->value) {
						case 1:
							$params['open'] = 1;
							break;
						case 2:
							$params['closed'] = 1;
							break;
						case 3:
							$params['last_day'] = 1;
							break;
					}
				}
				if ($value->name == 'qtype') {
					$params['type'] = $value->value;
				}
			}
		}
		// Set idUser only if it is meaningfull for current endpoint
		$idQuestionUser = (isset($this->id_user) && ($this->endpoint == RestData::ENDPOINT_MY_QUESTIONS)) ? $this->id_user : false;

		// Set idAnswerUser only if it is meaningfull for current endpoint
		$idAnswerUser = (isset($this->id_user) && ($this->endpoint == RestData::ENDPOINT_MY_ANSWERS)) ? $this->id_user : false;

		$idViewerUser = Yii::app()->user->id;

		// Get data provider 
		$dp = App7020Question::sqlRestDataProvider($params, $idQuestionUser, $idAnswerUser, $idViewerUser);
		$allQuestionsCount = (int) $dp->getTotalItemCount();

		// Start building the output array of data
		$section = array(
			'questions_count' => $allQuestionsCount,
		);

		// Now collect questions        
		$questions = $dp->data;
		$items = array();
		$allAnswersCount = 0;

		// And build output items
		foreach ($questions as $value) {
			$feed = new QuestionItem($value, $this);
			$item = $feed->getData();
			$item['answer']['readOnly'] = $item['answer']['author_id'] == Yii::app()->user->idst;
			$item['answer']['text'] = strip_tags($item['answer']['text'], '<span><p><strong><em><a><img>');
			$allAnswersCount = $allAnswersCount + $item["total_number_answers"];
			$items[] = $item;
		}

		// Finish output data
		$section["answers_count"] = $allAnswersCount;
		$section["selected_filters"] = $this->filters;
		$section["items"] = $items;

		$userModel = CoreUser::model()->findByPk(Yii::app()->user->id);
	 $avatar = utf8_decode(Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $userModel->idst),true));
						

		$section["logged_user"] = array(
			//"avatar" => CoreUser::getAvatarByUserId($userModel->idst),
		 	"avatar" => $avatar,
			"name" => $userModel->firstname . " " . $userModel->lastname,
			"is_expert" => false, // @TODO What constitues a user as an expert.. for given question? .. or answer?
			"id_user" => Yii::app()->user->id
		);


		$result = array(
			"success" => true,
			"section" => $section,
		);

		return $result;
	}

	/**
	 * Build/modify parameters for data provider
	 */
	private function buildParamsForDp() {
		$params = $this->endpointParams;

		if ($this->endpoint == RestData::ENDPOINT_FOLLOWING) {
			$params["followed"] = 1;
		}

		// For SINGLE question endpoint, we are looking for only one result, the first question found.
		// i.e. NOT applying the incoming from/count parameters, which are supposed to be for paginationg ANSWERS data provider
		if ($this->endpoint == RestData::ENDPOINT_SINGLE_QUESTION) {
			$params["from"] = 0;
			$params["count"] = 1;
		}

		// Extract filter information from params["filters"] (if any) and move them to 1st level of the params array
		// Because dataprovider works that way!
		if (isset($params["filters"]) && is_array($params["filters"])) {
			foreach ($params["filters"] as $fltName => $fltValue) {
				$params[$fltName] = $fltValue;
			}
		}
		return $params;
	}

}
