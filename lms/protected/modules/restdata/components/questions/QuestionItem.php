<?php
/**
 *
 */
class QuestionItem extends stdClass
{

	private $item;
	private $endpoint;
	private $endpointParams;
	
	const ACTION_UID_DELETE_QUESTION = "deleteQuestion";
	const ACTION_UID_FOLLOW = "follow";
	const ACTION_UID_EDIT = "editq";
	
	/**
	 * 
	 * @param array $item
	 * @param RestData $creator
	 */
	public function __construct($item, $creator) {
		$this->item = $item;
		if (is_a($creator, "RestData")) {
		    $this->endpoint = $creator->endpoint;
		    $this->endpointParams = $creator->endpointParams;
		}
		
		
	}
	
	/**
	 * 
	 */
    public function getData() {

		$result = array(
            "question_id"			     => (int) $this->item['id'],
			//"avatar"				     => CoreUser::getAvatarByUserId($this->item['idUser']),
			"avatar"				     => Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $this->item['idUser']),true),
            "question"				     => $this->item['title'],
			"owner"					     => CoreUser::getForamattedNames($this->item['idUser']),
		    "owner_id"			         => (int) $this->item['idUser'],
		    "owner_channel_link"	     => Docebo::createLmsUrl('channels/index', array("#" => "/pchannel/".$this->item['idUser'])),
            "views"					     => App7020QuestionHistory::getQuestionViewes($this->item['id']),
			"update_date"			     => App7020Helpers::timeAgo($this->item['created']),
			"total_number_answers"	     => (int) $this->item['answers'],
		    "total_number_new_answers"   => (int) $this->item['newAnswersCount'],
		    "following_icon"             => "", // @TODO
			"is_following"			     => $this->item['followId'] ? true : false,
		    "is_new"                     => $this->item["isNew"] ? true : false,
			"have_best_answer"			 => ($this->item['bestAnswerId']) ? true : false,
            "answer"				     => $this->getBestLastAnswer(),
            "actions"				     => $this->getActions(),
            "other_fields"			     => $this->getOtherFields(),
		    "tags"                       => $this->getTags(),
			"can_answer"				 => App7020Question::canAnswerQuestion($this->item['id'])
        );
		
		if ($this->endpoint === RestData::ENDPOINT_SINGLE_QUESTION) {
		    $result["answers"]    = $this->getAnswers();
		}
        return $result;
    }
    
    
    /**
     * 
     */
    private function getAnswers() {
        $dp = App7020Answer::sqlRestDataProvider($this->endpointParams, array($this->item["id"]));
        $result = array();	
        foreach ($dp->data as $answer) {
			$answer['content'] = strip_tags($answer['content'], '<span><p><strong><em><a><img>');
            $answerItem = new AnswerItem($answer, $this);
            $result[] = $answerItem->getData();
        }
        return $result;        
    }
    
    
    /**
     * 
     */
    private function getTags() {
        $result = array();
        if (isset($this->item["tags"])) {
            $result = explode(",", $this->item["tags"]);
        }
        return $result;
    }
	
	/**
     * Get question other fields
     */
    private function getOtherFields() {
        $otherFields = array();
		
        if (!empty($this->item['idContent'])){
            $assetObject = App7020Assets::model()->findByPk($this->item['idContent']);
			if($assetObject->id && $assetObject->conversion_status == App7020Assets::CONVERSION_STATUS_APPROVED){
				$otherFields['asset'] = array(
					'asset_id'			=> (int) $assetObject->id,
					'asset_title'		=> $assetObject->title,
				    'asset_page_url' => Docebo::createApp7020AssetsViewUrl($assetObject->id),
					'asset_preview'		=> App7020Assets::getAssetThumbnailUri($assetObject->id)
				);
			}
        }
		
		if (!empty($this->item['idLearningObject'])){
            $learningOrganizationModel = LearningOrganization::model()->findByPk($this->item['idLearningObject']);
			if($learningOrganizationModel->idOrg){
				$otherFields['asset'] = array(
					'asset_id'			=> $learningOrganizationModel->idOrg,
					'asset_title'		=> $learningOrganizationModel->title,
					'asset_page_url'	=> Docebo::createLmsUrl('player', array('course_id' => $this->item['idCourse'], 'lo_key' => $learningOrganizationModel->idOrg)),
					'asset_preview'		=> CoreAsset::url($learningOrganizationModel->resource, CoreAsset::VARIANT_SMALL)
				);
			}
        }
		
		if(!empty($this->item['idChannel'])){
			$channelObject = App7020Channels::model()->findByPk($this->item['idChannel']);
			if($channelObject->id){
				$translation = $channelObject->translation();
				$otherFields['channel'] = array(
					'channel_id'			=> $channelObject->id,
					'channel_title'			=> $translation['name'],
				    'channel_url'			=> Docebo::createLmsUrl('channels/index', array("#" => "/channel/".$channelObject->id)),
					'channel_icon'			=> $channelObject->icon_fa,
					'channel_icon_color'	=> $channelObject->icon_color,
					'channel_color'			=> $channelObject->icon_bgr
				);
			}
		}
		
        return $otherFields;
    }
	
	/**
     * Get Question Actions
     */
    private function getActions() {

		$actions = array();
		$viewerId = Yii::app()->user->id;
		
        if ($this->item['followId']) {
			$actions[] = array(
				'action_type' => 'drop_down',
				'action_uid' => self::ACTION_UID_FOLLOW,
//				'action_params' => array(
//					'url' => 'javascript::'
//					),
				'label' => Yii::t('standard', 'Unfollow'),
				'text_color' => '#333333'
			);
		} else {
			$actions[] = array(
				'action_type' => 'drop_down',
				'action_uid' => self::ACTION_UID_FOLLOW,
//				'action_params' => array(
//					'url' => 'javascript::'
//					),
				'label' => Yii::t('standard', 'Follow'),
				'text_color' => '#333333'
			);
		}
		if ($this->item['idUser'] == $viewerId){

			$actions[] = array(
                'action_type' => 'drop_down',
				'action_uid' => self::ACTION_UID_EDIT,
                'action_params' => array(
                    'url' => 'javascript::'
                ),
                'label' => Yii::t('standard', 'Edit'),
                'text_color' => '#333333'
            );
			
			
		}

        if ($this->item['idUser'] == $viewerId || Yii::app()->user->getIsGodadmin()){

			
			if($this->endpoint == RestData::ENDPOINT_MY_ANSWERS){
				$actionUid = AnswerItem::ACTION_UID_DELETE_ANSWER;
			} else {
				$actionUid = self::ACTION_UID_DELETE_QUESTION;
			}
			
			$actions[] = array(
                'action_type' => 'drop_down',
				'action_uid' => $actionUid,
                'action_params' => array(
                    'url' => 'javascript::'
                ),
                'label' => Yii::t('standard', 'Delete'),
                'text_color' => '#e84b3c'
            );
			
			
		}
		
		
        return $actions;
    }
	/**
     * Get questions' best or last answer
     */
    private function getBestLastAnswer() {

        $answer = array();
		if($this->item['bestAnswerId']){
            $answer = array(
				'author_id'			=> App7020Answer::model()->findByPk($this->item['bestAnswerId'])->idUser,
				'answer_id'			=> $this->item['bestAnswerId'],
				'text'				=> $this->item['bestAnswerText'],
				'is_best_answer'	=> true,
                'likes'             => $this->item['bestAnswerLikes'],
                'dislikes'          => $this->item['bestAnswerDislikes'],
            ); 
		} elseif($this->item['lastAnswerId']){
			$answer = array(
				'author_id'			=> App7020Answer::model()->findByPk($this->item['lastAnswerId'])->idUser,
				'answer_id'			=> $this->item['lastAnswerId'],
				'text'				=> $this->item['lastAnswerText'],
				'is_best_answer'	=> false,
                'likes'             => $this->item['lastAnswerLikes'],
                'dislikes'          => $this->item['lastAnswerDislikes'],
				'answerauthor'		=> CoreUser::getForamattedNames( App7020Answer::getUserByAnswerId($this->item['lastAnswerId']), ' ', false),
		    ); 
		}
		
		
        return $answer;
    }
	
}