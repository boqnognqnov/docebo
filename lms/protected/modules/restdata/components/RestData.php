<?php

/**
 * Base class for all REST endpoint handlers
 * 
 * NOTE: It is assumed that all calls to this class method (and its descendants) are enclosed in try/catch block!
 * Please do not try/catch here!
 * 
 * Request parameters are saved in dynamically created class attributes.
 * 
 * @property string $endpoint Endpoint (or what we are going to)
 * @property string $endpointParams incoming params
 * @property string $lang_code e.g. "english", "italian", "bulgarian"
 * @property string $lang e.g. "en", "it", "bg"
 * @property integer $from Offset
 * @property integer $count Limit
 * @property integer $id_user User ID
 * @property string $sort_by Sorting parameter
 * @property string $sort_dir Sorting direction: ASC|DESC
 * @property array $filters Selected filtering information 
 * 
 */
class RestData extends stdClass {

	public $endpoint;
	public $endpointParams;

	/**
	 * Define "endpoints"
	 *
	 * @var string
	 */
	const ENDPOINT_ALL_QUESTIONS = 'all_questions';
	const ENDPOINT_MY_QUESTIONS = 'my_questions';
	const ENDPOINT_MY_ANSWERS = 'my_answers';
	const ENDPOINT_FOLLOWING = 'following';
	const ENDPOINT_SINGLE_QUESTION = 'single_question';
	const ENDPOINT_SAVE_QUESTION = 'save_question';
	const ENDPOINT_FOLLOW_QUESTION = 'follow_question';
	const ENDPOINT_DELETE_QUESTION = 'delete_question';
	const ENDPOINT_SAVE_ANSWER = 'save_answer';
	const ENDPOINT_LIKE_ANSWER = 'like_answer';
	const ENDPOINT_DELETE_ANSWER = 'delete_answer';
	const ENDPOINT_MARK_ANSWER = 'mark_answer';
	const ENDPOINT_CHANNELS = 'channels';
	const ENDPOINT_SINGLE_CHANNEL = 'single_channel';
	const ENDPOINT_CHANNEL_ITEMS = 'channel_items';
	const ENDPOINT_USER_CHANNELS = 'user_channels';
	const ENDPOINT_USER_CHANNEL_ITEMS = 'user_channel_items';
	const ENDPOINT_USER_CHANNEL_IN_REVIEW = 'user_channel_in_review';
	const ENDPOINT_USER_CHANNEL_MY_CONTRIBUTION = 'user_channel_my_contributions';
	const ENDPOINT_OTHER_PERSONAL_CHANNEL = 'other_personal_channel';
	const ENDPOINT_UPDATE_CHANNEL_DESCRIPTION = 'update_channel_description';
	const ENDPOINT_QUESTION_VIEW = 'question_view';
	//Coach and share activity endpoints
	const ENDPOINT_SHARING_ACTIVITY = 'sharing_activity';
	const ENDPOINT_GET_COACH_ACTIVITY = 'coach_activity';
	const ENDPOINT_GET_PEER_ACTIVITY = 'peer_activity';
	//Endpoints using for App7020assets CRUD operations
	const ENDPOINT_ASSET_UPLOADED = 'asset_uploaded';
	const ENDPOINT_ASSET_FAILED_UPLOAD = 'asset_failed_upload';
	const ENDPOINT_ASSET_START_UPLOAD = 'asset_start_upload';
	const ENDPOINT_ASSET_ADDED = 'asset_added';
	const ENDPOINT_ASSET_SUBMITTED = 'asset_submitted';
	const ENDPOINT_ASSET_PUBLISHED = 'asset_published';
	const ENDPOINT_ASSET_UNPUBLISHED = 'asset_unpublished';
	const ENDPOINT_ASSET_UPDATED = 'asset_updated';
	const ENDPOINT_ASSET_DELETED = 'asset_deleted';
	const ENDPOINT_ASSET_GET_ACTIVE = 'asset_get_active';
	const ENDPOINT_ASSET_DISMISS = 'asset_dismiss';
	const ENDPOINT_ASSET_UNDO_DISMISS = 'asset_undo_dismiss';
	const ENDPOINT_ASSET_VIEW = 'asset_view';
	const ENDPOINT_SPENT_TIME = 'spent_time';
	//Endpoints using for App7020assets data feeders
	const ENDPOINT_ASSET_DATA = 'asset_data';
	const ENDPOINT_ASSET_SIMILAR = 'asset_similar';
	const ENDPOINT_ASSET_PR = 'asset_pr';
	//Endpoints using for App7020assets PR CRUD operations
	const ENDPOINT_PR_ADDED = 'pr_added';
	const ENDPOINT_PR_UPDATED = 'pr_updated';
	const ENDPOINT_PR_DELETED = 'pr_deleted';
	//Endpoints using for App7020assets Review/Approve operations
	const ENDPOINT_ASSET_RA_IN_REVIEW = 'asset_ra_in_review';
	const ENDPOINT_ASSET_RA_NEW_ONLY = 'asset_ra_new_only';
	const ENDPOINT_ASSET_RA_SILENCED = 'asset_ra_silenced';
	const ENDPOINT_ASSET_RA_IGNORE = 'asset_ra_ignore';
	const ENDPOINT_ASSET_RA_UNIGNORE = 'asset_ra_unignore';
	//Endpoints using for Experts tasks - questions to answer and requests
	const ENDPOINT_ET_QUESTIONS = 'et_questions';
	const ENDPOINT_ET_REQUESTS = 'et_requests';
	const ENDPOINT_RATE_ASSET = 'rate_asset';
	const ENDPOINT_ET_ALL = 'et_all';
	const ENDPOINT_ET_SILENCE_CRUD = 'et_silence_crud';
	const ENDPOINT_ET_QUICK_ANSWER = 'et_quick_answer';
	const ENDPOINT_ET_CLEAN_SILENCED = 'et_clean_silenced';
	const ENDPOINT_ET_ASSIGN_CRUD = 'et_assign_crud';
	const SORT_BY_NAME = "name";
	const SORT_BY_CREATION_DATE = "datecreated";
	const SORT_BY_ASC = "asc";
	const SORT_BY_DESC = "desc";
	const SORT_META_KEY_NAME = "__sort_meta_key__";
	//Endpoints using for invitations
	const ENDPOINT_INVITE_GET_USERS = 'invite_get_users';
	const ENDPOINT_INVITE_SET_USERS = 'invite_set_users';
	const ENDPOINT_INVITE_TO_WATCH = 'invite_to_watch';
	//Endpoint for navButtons single page
	const ENDPOINT_ASSET_VIEW_NAV_BUTTONS = 'asset_view_nav_buttons';
	//Endpoints for tooltips
	const ENDPOINT_TOOLTIPS_DATA = 'tooltips_data';
	const ENDPOINT_TOOLTIPS_CRUD = 'tooltips_crud';
	//Endpoint for Splash Screen Tracking
	const ENDPOINT_SPLASH_SCREEN = 'splash_screen';
	//Endpoint for my courses
	const ENDPOINT_MY_COURSES = 'my/courses';
	//Endpoint for my learning plans
	const ENDPOINT_MY_LP = 'my/lp';

	/**
	 * Constructor 
	 * 
	 * @param string $params
	 */
	public function __construct($params = false) {

		// Create class attributes from request parameters
		$this->mapParamsToClassAttributes($params);

		// Lets have all params saved into this attribute
		$this->endpointParams = $params;

		// After mapping all incoming data to class attributes....
		// If no user ID is passed by the caller, we assume that all data must be "current user" aware.
		// Note, we always have Yii::app()->user, even in API and Console Applivations, that's the new approach 
		if (!isset($this->id_user)) {
			$this->id_user = Yii::app()->user->id;
		}

		$this->resolveLanguageCode();

		// Finally, If sorting is not yet specified (as rest parameter sort_by), 
		// use Sorting by creation date, descending (last items go first)
		// Every descendant class can change this of course. Do it as early as possible!!!!!!
		$this->sort_by = $this->sort_by ? $this->sort_by : self::SORT_BY_CREATION_DATE;
		$this->sort_dir = $this->sort_dir ? $this->sort_dir : self::SORT_BY_DESC;
	}

	/**
	 * Factory, returning endpoint dependent descendant class 
	 *
	 * @param string $endpoint
	 * @param array $params
	 *
	 * @throws Exception
	 */
	public static function factory($endpoint, $params) {

		$params['endpoint'] = $endpoint;
		switch ($endpoint) {

			// All questions and My questions differs in the way id_user is used, but is all the same
			case self::ENDPOINT_ALL_QUESTIONS:
			case self::ENDPOINT_MY_QUESTIONS:
			case self::ENDPOINT_MY_ANSWERS:
			case self::ENDPOINT_FOLLOWING:
				return new QuestionsData($params);
				break;

			case self::ENDPOINT_SINGLE_QUESTION:
				if (!isset($params["id_question"])) {
					throw new Exception('Invalid Question ID. Please provide valid id_question');
				}
				App7020Question::viewQuestion($params["id_question"]);
				return new QuestionsData($params);
				break;
			case self::ENDPOINT_CHANNELS:
				return new ChannelDataAllChannels($params);
				break;
			case self::ENDPOINT_SINGLE_CHANNEL:
				
				return new ChannelDataSingleChannel($params);
				break;
			case self::ENDPOINT_CHANNEL_ITEMS:
				$idChannel = ChannelData::extractArgument($params, 'id_channel');
				$channel = App7020Channels::model()->findByPk($idChannel);
				if (!$channel) {
					throw new Exception('Invalid Channel ID');
				}
				$className = ($channel->type == 1) ? 'ChannelData' . ucfirst($channel->predefined_uid) : "ChannelDataAdmin";
			
				return new $className($params);
				break;

			case self::ENDPOINT_USER_CHANNELS:
			case self::ENDPOINT_USER_CHANNEL_MY_CONTRIBUTION:
			case self::ENDPOINT_USER_CHANNEL_IN_REVIEW:
				return new ChannelDataPersonal($params);
				break;

			case self::ENDPOINT_OTHER_PERSONAL_CHANNEL:
				return new ChannelDataOtherPersonal($params);
				break;
			case self::ENDPOINT_UPDATE_CHANNEL_DESCRIPTION:
				return new ChannelUpdateDescription($params);
				break;

			case self::ENDPOINT_SAVE_QUESTION:
			case self::ENDPOINT_FOLLOW_QUESTION:
			case self::ENDPOINT_DELETE_QUESTION:
				return new QuestionCrud($params);
				break;

			case self::ENDPOINT_SAVE_ANSWER:
			case self::ENDPOINT_DELETE_ANSWER:
			case self::ENDPOINT_LIKE_ANSWER:
			case self::ENDPOINT_MARK_ANSWER:
				return new AnswerCrud($params);
				break;
			case self::ENDPOINT_GET_COACH_ACTIVITY:
				return new coachActivity($params);
				break;
			case self::ENDPOINT_GET_PEER_ACTIVITY:
				return new peerActivity($params);

			case self::ENDPOINT_SHARING_ACTIVITY:
				return new SharingActivity($params);
				break;

			case self::ENDPOINT_ASSET_ADDED:
			case self::ENDPOINT_ASSET_DELETED:
			case self::ENDPOINT_ASSET_PUBLISHED:
			case self::ENDPOINT_ASSET_SUBMITTED:
			case self::ENDPOINT_ASSET_UNPUBLISHED:
			case self::ENDPOINT_ASSET_UPDATED:
			case self::ENDPOINT_ASSET_UPLOADED:
			case self::ENDPOINT_ASSET_DISMISS:
			case self::ENDPOINT_ASSET_UNDO_DISMISS:
			case self::ENDPOINT_ASSET_VIEW:
			case self::ENDPOINT_SPENT_TIME:
				return new AssetCrud($params);
				break;

			case self::ENDPOINT_ASSET_SIMILAR:
			case self::ENDPOINT_ASSET_PR:
				return new AssetRelatedData($params);
				break;

			case self::ENDPOINT_ASSET_DATA:
				return new AssetItem($params);
				break;


			case self::ENDPOINT_ASSET_RA_IN_REVIEW:
			case self::ENDPOINT_ASSET_RA_NEW_ONLY:
			case self::ENDPOINT_ASSET_RA_SILENCED:
				return new AssetRaData($params);
				break;

			case self::ENDPOINT_ASSET_RA_IGNORE:
			case self::ENDPOINT_ASSET_RA_UNIGNORE:
				return new AssetRaCrud($params);
				break;



			case self::ENDPOINT_ET_QUESTIONS:
				return new QuestionsET($params);
				break;

			case self::ENDPOINT_ET_REQUESTS:
				return new RequestsET($params);
				break;

			case self::ENDPOINT_ET_SILENCE_CRUD:
			case self::ENDPOINT_ET_CLEAN_SILENCED:
			case self::ENDPOINT_ET_QUICK_ANSWER:
			case self::ENDPOINT_ET_ASSIGN_CRUD:
				return new ETCrud($params);
				break;

			case self::ENDPOINT_RATE_ASSET:
				return new RateAsset($params);
				break;
			case self::ENDPOINT_PR_ADDED:
			case self::ENDPOINT_PR_UPDATED:
			case self::ENDPOINT_PR_DELETED:
				return new PeerReviewCrud($params);
				break;
			case self::ENDPOINT_ASSET_VIEW_NAV_BUTTONS:
				return new AssetViewNavButtons($params);
				break;
			case self::ENDPOINT_INVITE_GET_USERS:
			case self::ENDPOINT_INVITE_SET_USERS:
			case self::ENDPOINT_INVITE_TO_WATCH:
				return new InviteToWatch($params);
				break;

			case self::ENDPOINT_SPLASH_SCREEN:
				return new SplashScreen($params);
				break;

			case self::ENDPOINT_ET_ALL:
				return new ETData($params);
				break;

			case self::ENDPOINT_TOOLTIPS_DATA:
			case self::ENDPOINT_TOOLTIPS_CRUD:
				return new TooltipsCrud($params);
				break;
			case self::ENDPOINT_MY_COURSES:
				$params['idUser'] = Yii::app()->user->id;
				return new MyCourses($params);
			case self::ENDPOINT_MY_LP:
				$params['idUser'] = Yii::app()->user->id;
				return new MyLearningPlans($params);
			default:
				throw new Exception("Invalid endpoint");
				break;
		}
	}

	/**
	 * Assign array values to named class attributes. If they are not defined, PHP will create them (this is StdClass)
	 * 
	 * @param array $params
	 */
	private function mapParamsToClassAttributes($params) {
		if (is_array($params)) {
			foreach ($params as $key => $value) {
				$this->$key = $value;
			}
		}
	}

	/**
	 * Helper: extract named key value from an array OR object
	 * 
	 * @param array $params
	 * @param string $arg
	 * @param mixed $default Default value, if key not found in the array
	 */
	protected static function extractArgument(&$params, $arg, $default = null) {
		if (is_object($params) === true) {
			$params = (array) $params;
		}
		if (isset($params[$arg]) === true) {
			$val = $params[$arg];
			return $val;
		} else {
			return $default;
		}
	}

	/**
	 * Check if value is null or empty
	 * @param mixed $var
	 */
	protected static function isNullOrEmpty($var) {
		if ($var === null) {
			return true;
		}

		if (is_string($var)) {
			if (strlen($var) === 0) {
				return true;
			}
		}

		if (is_array($var)) {
			if (strlen(implode("", $var)) === 0) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Resolve language code
	 */
	protected function resolveLanguageCode() {
		if ($this->lang_code) {
			if (strlen($this->lang_code) === 2) {
				$this->lang = $this->lang_code;
			} else {
				$this->lang = Lang::getBrowserCodeByCode($this->lang_code);
			}
		} else {
			$this->lang = Lang::getBrowserCodeByCode(Settings::get('default_language', 'english'));
		}
	}

	/**
	 * Check input for required parameters. This method should be overriden by descendant classes
	 * to define their own validation logic. Should throw an exception to cancel the whole process.
	 */
	protected function checkRequired() {
		return true;
	}

	/**
	 * Takes an array of items with meta keys and returns a sorted slice, according to the sorting options and requested pagination
	 *
	 * @param $items The big array to sort and slice
	 * 
	 * @return array The sorted [slice of] items
	 */
	protected function sortAndSlice($items) {
		// Sort items (if needed)
		$items = $this->sort($items);

		// Get desired slice only
		if ($this->from >= 0 && $this->count > 0 && count($items) > 0)
			return array_slice($items, $this->from, $this->count);
		else
			return $items;
	}

	/**
	 * Takes an array of items with meta keys and returns a sorted slice, according to the sorting options
	 *
	 * @param $items The big array to sort
	 * @return array The sorted slice of items
	 */
	protected function sort($items) {
		$sort_by = $this->sort_by ? $this->sort_by : self::SORT_BY_CREATION_DATE;

		if ($this->channel->sort_items != '' || $this->channel->sort_items != NULL) {
			if (count($items) > 0) {
				switch ($this->channel->sort_items) {
					case 'name_asc':
						$sortType = $this->sortTypeBySortBy(self::SORT_BY_NAME);
						$sort_dir = self::SORT_BY_ASC;
						$field = self::SORT_BY_NAME;
						break;
					case 'name_desc':
						$sortType = $this->sortTypeBySortBy(self::SORT_BY_NAME);
						$sort_dir = self::SORT_BY_DESC;
						$field = self::SORT_BY_NAME;
						break;
					case 'date_asc':
						$sortType = $this->sortTypeBySortBy(self::SORT_BY_CREATION_DATE);
						$sort_dir = self::SORT_BY_ASC;
						$field = self::SORT_META_KEY_NAME;
						break;
					case 'date_desc':
						$sortType = $this->sortTypeBySortBy(self::SORT_BY_CREATION_DATE);
						$sort_dir = self::SORT_BY_DESC;
						$field = self::SORT_META_KEY_NAME;
						break;
					default:
						$sort_dir = strtolower($this->sort_dir ? $this->sort_dir : self::SORT_BY_ASC);
						$sortType = $this->sortTypeBySortBy($sort_by);
						$field = self::SORT_META_KEY_NAME;
						break;
				}


				if ($sortType == 'string') {
					return App7020Helpers::sortArrayAlphabetical($items, $field, $sort_dir);
				}
				return Docebo::sort2DArray($items, $sortType, $sort_dir, $field);
			}
		} else {
			if ($sort_by && count($items) > 0) {
				$sort_dir = strtolower($this->sort_dir ? $this->sort_dir : self::SORT_BY_ASC);
				$sortType = $this->sortTypeBySortBy($sort_by);
				return Docebo::sort2DArray($items, $sortType, $sort_dir, self::SORT_META_KEY_NAME);
				//return App7020Helpers::sortMultyArray($items, $sort_by, $sort_dir);
			} else {
				return $items;
			}
		}
	}

	/**
	 * Return sort TYPE (in terms of Docebo::sort2DArray() based on Sort BY [what]
	 *
	 * @param string $sortBy
	 * @return string
	 */
	protected function sortTypeBySortBy($sortBy) {
		$map = array(
			self::SORT_BY_NAME => Docebo::SORT_TYPE_STRING,
			self::SORT_BY_CREATION_DATE => Docebo::SORT_TYPE_TIMESTAMP,
		);
		if (isset($map[$sortBy])) {
			return $map[$sortBy];
		}
		return Docebo::SORT_TYPE_NUMBER;
	}

	/**
	 * Try to load REST input from every possible place: GET/POST/Request body
	 */
	public static function resolveRestInput() {

		$endpoint = false;
		$params = false;

		// Get the payload, if any, and get all input data from there, if any
		$payloadJson = file_get_contents('php://input');
		if (!empty($payloadJson)) {
			$payloadArray = json_decode($payloadJson, true);
			$endpoint = isset($payloadArray["endpoint"]) ? $payloadArray["endpoint"] : false;
			$params = isset($payloadArray["params"]) ? $payloadArray["params"] : false;
		}

		// If payload didn't provide us endpoint or params, try to get then from $REQUEST
		$endpoint = ($endpoint === false) ? Yii::app()->request->getParam('endpoint', false) : false;
		$params = ($params === false) ? Yii::app()->request->getParam('params', array()) : array();

		$input = array(
			"endpoint" => $endpoint,
			"params" => $params,
		);

		return $input;
	}

	/**
	 * Returns the default counter of channel items
	 * @return array
	 */
	public function getStdCounter($counter = null) {
		if ($counter > 0) {
			return array(
				"count" => $counter,
				"text" => Yii::t('standard', 'Items')
			);
		} else {
			return array();
		}
	}

}
