<?php

/**
 *
 */
class ChannelDataPersonal extends ChannelData implements IChannelData {

	public function getChannelInfo() {

		$userModel = CoreUser::model()->findByPk($this->id_user);
		if (!$userModel->idst) {
			return array();
		}

		$result = parent::getChannelInfo();
		$channelDescription = App7020PersonalChannelsSettings::model()->findByAttributes(array('idUser' => $this->id_user))->description;
		$result["description"] = $channelDescription ? $channelDescription : '';
		$result["name"] = $userModel->firstname ?
				$userModel->firstname . " " . Yii::t("app7020", "Channel") : ltrim($userModel->userid, "/") . " " . Yii::t("app7020", "Channel");
		$ownInfo = array(
			"other_fields" => array(
				"avatar" => Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $this->id_user), true)
			),
		);
        if (Yii::app()->legacyWrapper->hydraFrontendEnabled()) {
            $result['items_count'] = App7020Assets::getUsersContributions($this->id_user,[
                'ignoreBlankPlaylists' => false,
                'withPlaylist' => true,
            ])->totalItemCount;
        } else {
            $result['items_count'] = App7020Assets::getUsersContributions($this->id_user)->totalItemCount;
        }
		$result = array_merge($result, $ownInfo);
		return $result;
	}

	public function getChannelItems() {
		$items = array();
		$assetsInReview = array();
		$assetsPublished = array();
        if (Yii::app()->legacyWrapper->hydraFrontendEnabled()) {
            if ($this->endpoint == parent::ENDPOINT_USER_CHANNEL_IN_REVIEW) {
                $assetsInReview = App7020Assets::getUsersContributions($this->id_user,
                    array(
                        'assetStatus' => array(10, 15),
                        'from' => $this->from,
                        'count' => $this->count
                    ))->rawData;
            } elseif ($this->endpoint == parent::ENDPOINT_USER_CHANNEL_MY_CONTRIBUTION) {
                $assetsPublished = App7020Assets::getUsersContributions($this->id_user,
                    array(
                        'assetStatus' => array(10, 15, 18, 20),
                        'ignoreBlankPlaylists' => false,
                        'withPlaylist' => true,
                        'from' => $this->from,
                        'count' => $this->count
                    ),
                    true)->rawData;
            } else {
                $assetsInReview = App7020Assets::getUsersContributions($this->id_user, array(
                    'assetStatus' => array(10, 15),
                    'from' => $this->from,
                    'count' => $this->count['inReviewCount'],
                    'ignoreBlankPlaylists' => false,
                    'withPlaylist' => true,
                ), true)->rawData;
                $assetsPublished = App7020Assets::getUsersContributions($this->id_user, array(
                    'assetStatus' => array(10,15,18, 20),
                    'from' => $this->from,
                    'count' => $this->count['myContributionsCount'],
                    'ignoreBlankPlaylists' => false,
                    'withPlaylist' => true,
                ), true)->rawData;
            }
            $allAssets = App7020Assets::getUsersContributions($this->id_user,
                [
                    'assetStatus' => [10, 15, 18, 20],
                    'ignoreBlankPlaylists' => false,
                    'withPlaylist' => true,
                ])->rawData;
        } else {
            if ($this->endpoint == parent::ENDPOINT_USER_CHANNEL_IN_REVIEW) {
                $assetsInReview = App7020Assets::getUsersContributions($this->id_user,
                    array('assetStatus' => array(10, 15), 'from' => $this->from, 'count' => $this->count))->rawData;
            } elseif ($this->endpoint == parent::ENDPOINT_USER_CHANNEL_MY_CONTRIBUTION) {
                $assetsPublished = App7020Assets::getUsersContributions($this->id_user,
                    array('assetStatus' => array(18, 20), 'from' => $this->from, 'count' => $this->count))->rawData;
            } else {
                $assetsInReview = App7020Assets::getUsersContributions($this->id_user, array(
                    'assetStatus' => array(10, 15),
                    'from' => $this->from,
                    'count' => $this->count['inReviewCount']
                ))->rawData;
                $assetsPublished = App7020Assets::getUsersContributions($this->id_user, array(
                    'assetStatus' => array(18, 20),
                    'from' => $this->from,
                    'count' => $this->count['myContributionsCount']
                ))->rawData;
            }
            $allAssets = App7020Assets::getUsersContributions($this->id_user)->rawData;
        }


        foreach ($assetsInReview as $asset) {
			$feed = AssetData::factory($asset, $this->id_user);
			$items[] = $feed->getData();
		}

		foreach ($assetsPublished as $asset) {
			$feed = AssetData::factory($asset, $this->id_user);
			$items[] = $feed->getData();
		}


		for ($i = count($assetsInReview); $i < count($items); $i++) {
			$originalActions = $items[$i]['actions'];

			$statsActions = $this->addStatsAction($items[$i]['id']);
			$addActions = $this->addEditAction($items[$i]['id']);
			array_splice($originalActions, 1, 0, $statsActions);
			array_splice($originalActions, 2, 0, $addActions);
			$items[$i]['actions'] = $originalActions;

		}



		$result = array(
			"items_count" => count($allAssets),
			"items" => $items,
			"counters" => array(),
		);
		return $result;
	}

	private function addStatsAction($assetId){
			$actions[] = array(
				//array(
					'action_type' => 'same_window',
					'action_params' => array(
						'url' => Docebo::createAbsoluteApp7020Url('analytics/index', array('id' => $assetId)),
						'method' => 'Analytics'),
					'label' => Yii::t('standard', 'Analytics'),
					'fa_icon' => 'fa-bar-chart',
					'text_color' => '#333',
					'id' => 'app7020-stats-asset',
					'class' => 'app7020-stats-asset'
				//)
			);

		return $actions;
	}

	private function addEditAction($assetId){
		$actions[] = array(
			//array(
				'action_type' => 'same_window',
				'action_params' => array(
			    'url' => Docebo::createApp7020Url("", array("#" => "/assets/peerReview/$assetId")),
				'method' => 'Edit'),
				'label' => Yii::t('standard', 'Edit'),
				'fa_icon' => 'fa-edit',
				'text_color' => '#0465AC',
				'id' => 'app7020-edit-asset',
				'class' => 'app7020-edit-asset'
			//)

		);

		return $actions;
	}

	public function run() {
		
		if ($this->endpoint == parent::ENDPOINT_USER_CHANNEL_IN_REVIEW || $this->endpoint == parent::ENDPOINT_USER_CHANNEL_MY_CONTRIBUTION) {
			$result = $this->getChannelItems();
		} else {
			$info = $this->getChannelInfo();
			$items = $this->getChannelItems();
			$channelOutput = array_merge($info, $items);

			$result = array(
				"success" => true,
				"channel" => $channelOutput,
			);
		}

		return $result;
	}

}
