<?php

/**
 * Channels specific class attributes.
 * 
 * @property string $description the new description of personal channel
 * 
 */
class ChannelUpdateDescription extends RestData {

	/**
	 * Constructor 
	 * 
	 * @param string $params
	 */
	public function __construct($params = false) {
		parent::__construct($params);
		if ($this->id_channel) {
			$this->channel = App7020Channels::model()->findByPk($this->id_channel);
		}
	}

	public function run() {
		$pChannelDescription = $this->description;
		$idUser = Yii::app()->user->id;
		if ($pChannelDescription) {
			$channelSettinsObject = App7020PersonalChannelsSettings::model()->findByAttributes(array('idUser' => $idUser));
			if(empty($channelSettinsObject)){
				$channelSettinsObject = new App7020PersonalChannelsSettings();
				$channelSettinsObject->idUser = $idUser;
				$channelSettinsObject->save();
			}
			$channelSettinsObject->description = $pChannelDescription;
			if ($channelSettinsObject->update()) {
				$result = array(
					"success" => true,
					"message" => Yii::t('standard', 'The description has been updated successfully')
				);
			} else {
				$result = array(
					"success" => false,
				);
			}
 			return $result;
			
		}	
	}
}