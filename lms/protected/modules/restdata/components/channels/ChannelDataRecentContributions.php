<?php
/**
 * 
 */
class ChannelDataRecentContributions extends ChannelData implements IChannelData
{

    public function getChannelInfo() {
        $result = parent::getChannelInfo();
        $ownInfo = array(
            "other_fields"      => array(),
        );
        $result = array_merge($result, $ownInfo);
        return $result;
    }
    
    
    public function getChannelItems(){
        $items = array();
        $result = array(
            "items"         => $items,
            "items_count"   => 0,  // total count, not the page only
            "counters"      => array(),
        );
        return $result;
    }
    
}