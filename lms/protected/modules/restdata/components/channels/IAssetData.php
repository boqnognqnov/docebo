<?php
/**
 * Created by PhpStorm.
 * User: scava1001
 * Date: 2/26/2016
 * Time: 2:01 PM
 */

interface IAssetData {
    
    public function getData();
    
}