<?php

/**
 *
 */
class ChannelDataOtherPersonal extends ChannelData implements IChannelData {

	public function getChannelInfo() {

		$userModel = CoreUser::model()->findByPk($this->id_user);
		if (!$userModel->idst) {
			return array();
		}

		$result = parent::getChannelInfo();
		$channelDescription = App7020PersonalChannelsSettings::model()->findByAttributes(array('idUser' => $this->id_user))->description;
		$result["description"] = $channelDescription ? $channelDescription : '';
		$result["name"] = $userModel->firstname ?
				$userModel->firstname . " " . Yii::t("app7020", "Channel") : ltrim($userModel->userid, "/") . " " . Yii::t("app7020", "Channel");
		$ownInfo = array(
			"other_fields" => array(
				"avatar" => Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $this->id_user), true)
			),
		);
        if (Yii::app()->legacyWrapper->hydraFrontendEnabled()) {
            $result['items_count'] = App7020Assets::getUsersContributions($this->id_user,
                ['withPlaylist' => true, 'ignoreBlankPlaylists' => true])->totalItemCount;
        } else {
            $result['items_count'] = App7020Assets::getUsersContributions($this->id_user)->totalItemCount;
        }


		$result = array_merge($result, $ownInfo);
		return $result;
	}

	public function getChannelItems() {
		$items = array();
		$assetsInReview = array();
		$assetsPublished = array();
        if (Yii::app()->legacyWrapper->hydraFrontendEnabled()) {
            $assetsPublished = App7020Assets::getUsersContributions($this->id_user,
                array(
                    'assetStatus' => array(20),
                    'from' => $this->from,
                    'withPlaylist' => true,
                    'ignoreBlankPlaylists' => true,
                    'count' => $this->count))->rawData;
            $allAssets = App7020Assets::getUsersContributions($this->id_user,
                [
                    'assetStatus' => array(20),
                    'withPlaylist' => true,
                    'ignoreBlankPlaylists' => true,
                ]
            )->rawData;
        } else {
            $assetsPublished = App7020Assets::getUsersContributions($this->id_user,
                array('assetStatus' => array(20),
                    'withPlaylist' => true,
                    'ignoreBlankPlaylists' => true, 'from' => $this->from, 'count' => $this->count))->rawData;
            $allAssets = App7020Assets::getUsersContributions($this->id_user,
                array('assetStatus' => array(20)))->rawData;
        }

		foreach ($assetsInReview as $asset) {
			$feed = AssetData::factory($asset, $this->id_user);
			$items[] = $feed->getData();
		}

		foreach ($assetsPublished as $asset) {
			$feed = AssetData::factory($asset, $this->id_user);
			$items[] = $feed->getData();
		}

		$result = array(
			"items_count" => count($allAssets),
			"items" => $items,
			"counters" => array(),
		);
		return $result;
	}

	public function run() {
		/* @var $channelDataObject ChannelData */
		/* @var $channel App7020Channels */

		$info = $this->getChannelInfo();
		$items = $this->getChannelItems();
		$channelOutput = array_merge($info, $items);

		$result = array(
			"success" => true,
			"channel" => $channelOutput,
		);


		return $result;
	}

}
