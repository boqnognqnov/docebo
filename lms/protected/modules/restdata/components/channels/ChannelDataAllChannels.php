<?php

/**
 *
 */
class ChannelDataAllChannels extends ChannelData implements IChannelData {

    /**
     * How many channel items to return
     * @var number
     */
    protected $items_per_channel;

    public function run() {
        /* @var $channelDataObject ChannelData */
        /* @var $channel App7020Channels */

        // Get list of channels, eligible to show (visible, enabled, ordered, ...)
        $custom_Select = App7020Channels::customProviderSelect(array('num_experts' => false, 'num_visibility_structures' => false, 'friendly_permissions' => false));
        $custom_channel_visibility_select = array(
            "idUser" => Yii::app()->user->idst,
            "ignoreVisallChannels" => false,
            "ignoreGodAdminEffect" => !Yii::app()->user->getIsGodadmin(),
            'appendEnabledEffect' => true,
            'ignoreSystemChannels' => false,
            'customSelect' => $custom_Select,
            'sort' => array(
                'channelOrdering' => array(
                    CSort::SORT_ASC => 'channelOrdering',
                    CSort::SORT_DESC => 'channelOrdering DESC',
                    'default' => CSort::SORT_ASC
                )
            ),
            'return' => 'provider'
        );
        $channelsDataProvider = App7020Channels::extractUserChannels($custom_channel_visibility_select, false);
        $channelsCount = $channelsDataProvider->totalItemCount;
        $channels = $channelsDataProvider->data;

        // Starting from requested index ("from"), cycle channels and collect output, using only NON-empty channels, up to "count" channels
        $channelsOutput = array();
        $nextFrom = $this->from;
        for ($i = $this->from; $i < $channelsCount; $i++) {
            $channel = $channels[$i];
            $className = ($channel["channelType"] == 1) ? 'ChannelData' . ucfirst($channel["channelPredefinedUid"]) : "ChannelDataAdmin";
            $channelDataObject = new $className(array(
                "lang_code" => $this->lang_code,
                "id_channel" => $channel['idChannel'],
                "from" => 0,
                "count" => $this->items_per_channel,
                "id_user" => $this->id_user,
            ));
            $info = $channelDataObject->getChannelInfo();
            $items = $channelDataObject->getChannelItems();

            // We are interesed in non-empt channels only, i.e. having items in it
            if ($items["items_count"] > 0) {
                $channelsOutput[] = array_merge($info, $items);
            }

            $nextFrom = $i + 1;

            // Stop collecting channels as we already have enough non-empty ones?
            if (count($channelsOutput) >= $this->count) {
                break;
            }
        }

        // If this is the first page requested and no channels are visible to the superadmin, return blank slate info
        $blankSlate = array();
        if (($this->from == 0) && (count($channelsOutput) == 0)) {
            $blankSlate = array(
                'is_superadmin' => Yii::app()->user->getIsGodadmin(),
                'is_coach_share_active' => PluginManager::isPluginActive('Share7020App'),
                'is_smb' => Docebo::isSMB(),
                'urls' => array(
                    'manage_users' => Docebo::createAbsoluteAdminUrl('userManagement/index'),
                    'manage_channels' => Docebo::createAbsoluteUrl('app7020:channelsManagement/index'),
                    'manage_courses' => Docebo::createAbsoluteAdminUrl('courseManagement/index'),
                    'map_experts' => Docebo::createAbsoluteUrl('app7020:expertsChannels/index'),
                    'upload_assets' => Docebo::createAbsoluteUrl('app7020:assets/index'),
                )
            );
        }

        $result = array(
            "success" => true,
            "total" => $channelsCount, // could be 0
            "channels" => $channelsOutput, // could be empty
            "next_from" => $nextFrom, // passing back the NEXT channel index (in the global eligible channels list); coming back later as "from"
            'blank_slate' => $blankSlate
        );

        return $result;
    }

    /**
     * Dummy
     * {@inheritDoc}
     * @see ChannelData::getChannelInfo()
     */
    public function getChannelInfo() {
        return array();
    }

    /**
     * Dummy
     * {@inheritDoc}
     * @see IChannelData::getChannelItems()
     */
    public function getChannelItems() {
        return array();
    }

}
