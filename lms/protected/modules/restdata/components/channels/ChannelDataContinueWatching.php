<?php

/**
 * Channels containing all non finished items (i.e. courses/LP in progress or knowledge assets not finished)
 */
class ChannelDataContinueWatching extends ChannelData implements IChannelData {

	/**
	 * Returns channel info (no other_fields is needed for this special channel)
	 * {@inheritDoc}
	 * @see ChannelData::getChannelInfo()
	 */
	public function getChannelInfo() {
		return parent::getChannelInfo();
	}

	/**
	 * Returns items for this channel, sorted and paginated
	 * {@inheritDoc}
	 * @see IChannelData::getChannelItems()
	 */
	public function getChannelItems() {
		$allItems = array();

		// !If you need this channel sorted in a specific order and by specific attribute (name, creation date,..), set sort_by/sort_dir here
		// Get all user's Courses & LPs in "in progress" status
		$allItems = $this->getFormalItems($this->id_user, "in_progress");
		$assets = array();

		if (PluginManager::isPluginActive('Share7020App')) {
			// Get Docs in progress
			$viewedDocs = App7020DocumentTracking::getProceedingDocs($this->id_user);
			$viewedVideos = App7020SpentTimeHistory::getContinueWatchAssetsByUser($this->id_user);
			foreach ($viewedVideos as $videoid){
				$check2 = App7020DismissHistory::model()->findByAttributes(array('idUser' => $this->id_user, 'idContent' => $videoid['id'], 'contentType' => 1));
				if (!$check2) {
					$videoAssets[] = $this->getAssetWithMetadata($videoid,true);
				}
			}
			foreach ($viewedDocs as $doc) {
				$check = App7020DismissHistory::model()->findByAttributes(array('idUser' => $this->id_user, 'idContent' => $doc['id'], 'contentType' => 1));
				if (!$check) {
					$assets[] = $this->getAssetWithMetadata($doc,true);
				}
			}
		}
		$allItems = array_merge($allItems, $assets);
		if(is_array($videoAssets) && !empty($videoAssets)){
			$allItems = array_merge($allItems, $videoAssets);
		}
		// Sort the BIG (combo) array
		$itemsSlice = $this->sortAndSlice($allItems);
		// Build the final array of data
		$channelItems = $this->buildChannelItems($itemsSlice);
		$newChannelItems = array();
		foreach ($channelItems as $item) {
			$item['pop_status'] = 0;
			$item['dismiss'] = 0;
			$item['dismiss_from'] = 1;
			$item['channel_name'] = Yii::t('app7020', 'Continue watch and learning');
			$new_action = array(
				'action_type' => 'pop',
				'action_params' => array(
					'url' => '#',
					'method' => '#'
				),
				'label' => Yii::t('app7020', 'Dismiss'),
				'fa_icon' => 'fa-ban',
				'text_color' => '#333',
				'id' => '',
				'class' => ''
			);
			if ($item[type_id] == 'knowledge_asset') {
				array_unshift($item[actions], $new_action);
			}
			$newChannelItems[] = $item;
		}

		// Produce the response
		$countItems = count($allItems);
		$result = array(
			//"items"         => $channelItems,
			"items" => $newChannelItems,
			"items_count" => $countItems,
			"counters" => $this->getStdCounter($countItems)
		);
		return $result;
	}

}
