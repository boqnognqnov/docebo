<?php

/**
 * 
 * @property integer $id_user User ID
 *
 */
class AssetData extends stdClass {

	/**
	 * Constructor 
	 * 
	 */
	public function __construct($asset = array(), $idUser) {
		$this->asset = $asset;
		$this->id_user = $idUser;
	}

	/**
	 * Factory static method to return proper Asset Data Feed oject
	 * 
	 *
	 * @throws Exception
	 */
	public static function factory($asset = array(), $idUser) {
		$type = ($asset['contentType'] == App7020Assets::CONTENT_TYPE_VIDEO || $asset['contentType'] == App7020Assets::CONTENT_TYPE_DEFAULT_MUSIC || $asset['contentType'] == App7020Assets::CONTENT_TYPE_DEFAULT_MUSIC) ?
				App7020Assets::$contentTypes[$asset['contentType']]['outputPrefix'] : "file";
		if (App7020Assets::isGoogleDriveTypes($asset['contentType'])) {
			$type = "Google";
		}
		$className = 'AssetData' . ucfirst($type);
		return new $className($asset, $idUser);
	}

	/**
	 * Get asset durations
	 */
	public function getRibbon() {

		if ($this->asset['is_private']) {
			return array(
				"fa_icon" => 'fa-lock',
				"text_color" => '#fff',
				"icon_bg_color" => '#333',
				"message" => Yii::t('app7020', 'This asset is private')
			);
		}

		if ($this->asset['conversion_status'] == App7020Assets::CONVERSION_STATUS_UNPUBLISH) {
			return array(
				"fa_icon" => 'fa-eye-slash',
				"text_color" => '#fff',
				"icon_bg_color" => '#999',
				"message" => Yii::t('app7020', 'This asset is unpublished')
			);
		}
		return array();
	}

	/**
	 * Get asset durations
	 */
	public function getAssetDuration() {
		if (!empty($this->asset['duration']))
			return sprintf('%2d:%02d:%02d', ($this->asset['duration'] / 3600), ($this->asset['duration'] / 60 % 60), $this->asset['duration'] % 60);

		return "";
	}

	/**
	 * Get Asset other fields
	 */
	public function getCommonOtherFields() {
		$otherFields = array(
			"views" => App7020ContentHistory::getContentViews($this->asset['id']),
			"author" => mb_strtolower($this->asset['author'], 'UTF-8'),
			"last_viewed" => App7020Helpers::timeAgo($this->asset['created']) . ' ' . Yii::t('app7020', 'ago'),
			"status" => (int) $this->asset['conversion_status']
		);


		if (empty($otherFields["author"])) {
			$userModel = CoreUser::model()->findByPk($this->asset["userId"]);
			if ($userModel->idst) {
				$otherFields["author"] = mb_strtolower($userModel->firstname . "." . $userModel->lastname, 'UTF-8');
			}
		}

		if (!trim($otherFields["author"]) || trim($otherFields["author"]) == ".") {
			$userModel = CoreUser::model()->findByPk($this->asset["userId"]);
			if ($userModel->idst) {
				$otherFields["author"] = ltrim($userModel->userid, "/");
				$otherFields["author"] = mb_strtolower($otherFields["author"], 'UTF-8');
			}
		}
		if ( !$otherFields["author"] || empty($otherFields["author"])){
			$otherFields["author"] = "";
		}
		$otherFields["peer_reviews"] = App7020ContentReview::getNewPeerReviewesByContent($this->asset['id'], $this->id_user);
		$otherFields["personal_channel"] = Docebo::createApp7020Url('channels/index', array("#" => '/pchannel/' . $this->asset["userId"]));

		return $otherFields;
	}

	/**
	 * Get Asset Actions
	 */
	public function getActions() {
		$actions = array();
		if ($this->asset['conversion_status'] == 20) {
			$actions[] = array(
						'action_type' => 'modal',
						'action_params' => array(
							'url' => Docebo::createApp7020Url('inviteToWatch/index', array("id" => $this->asset['id'])),
							'method' => 'inviteToWatch',
						),
						'label' => Yii::t('standard', 'Invite to watch'),
						'fa_icon' => 'fa-user-plus',
						'text_color' => '#333',
						'id' => 'app7020-invite-dialog',
						'class' => 'wide app7020-invite-dialog'
			);
		}
        $isExpert = false;
        if (in_array(Yii::app()->user->idst, App7020Assets::getAssetExperts($this->asset['id']))) {
            $isExpert = true;
        }
        if ($this->asset['userId'] == Yii::app()->user->id || Yii::app()->user->isGodAdmin || $isExpert)
			$actions[] = array(
				'action_type' => 'modal',
				'action_params' => array(
					'url' => Docebo::createApp7020Url('axAssets/axShowConfirmDeleteAssetModal', array("assetId" => $this->asset['id'])),
					'method' => 'delete'),
				'label' => Yii::t('standard', 'Delete'),
				'fa_icon' => 'fa-times',
				'text_color' => '#ff0000',
				'id' => 'app7020-confirm-delete-modal-dialog',
				'class' => 'app7020-confirm-delete-modal-dialog'
			);

		return $actions;
	}

	/**
	 * Get Asset Thumbnail Uri
	 */
	public function getAssetThumbnailUri() {
		$imgUri = App7020Assets::getPublcPreviewThumbnail($this->asset, 'small', false);
		preg_match_all('/src="([^"]+)/', $imgUri, $imgageAttr);

		if (count($imgageAttr) > 0)
			return$imgageAttr[1][0];

		return null;
	}

}
