<?php
/**
 * Channels containing all non started items (i.e. courses/LP not yet started or knowledge assets not yet played)
 */
class ChannelDataInvitations extends ChannelData implements IChannelData
{
    /**
     * Returns channel info (no other_fields is needed for this special channel)
     * {@inheritDoc}
     * @see ChannelData::getChannelInfo()
     */
    public function getChannelInfo() {
        return parent::getChannelInfo();
    }

    /**
     * Returns items for this channel, sorted and paginated
     * {@inheritDoc}
     * @see IChannelData::getChannelItems()
     */
    public function getChannelItems(){
        // Get all user's Courses & LPs in "not started" status
        $formalItems = $this->getFormalItems($this->id_user, "not_started");

        $assets = array();

        if (PluginManager::isPluginActive('Share7020App')) {
            // Get invitations for this user
            $invitations = App7020Invitations::getAllInvitationsByInvited($this->id_user, 0, 0, true);
            foreach ($invitations as $invitiaton) {
                $check = App7020DismissHistory::model()->findByAttributes(array('idUser'=>$this->id_user,'idContent'=>$invitiaton['id'],'contentType'=>2));
                if(!$check) {
                    $assets[] = $this->getAssetWithMetadata($invitiaton);
                }
            }
        }
		
        // Merge the two arrays
        $allItems = array_merge($formalItems, $assets);

        // Sort the BIG (combo) array
        $itemsSlice = $this->sortAndSlice($allItems);

        	// Build the final array of data
		$channelItems = [];
		if(!empty($itemsSlice)) {
			$channelItems = $this->buildChannelItems($itemsSlice);
		}

        $newChannelItems=array();
        foreach($channelItems as $item){
            $item['pop_status'] = 0;
            $item['dismiss'] = 0;
            $item['dismiss_from'] = 2;
            $item['channel_name'] = Yii::t('app7020','New content for you');
            $new_action = array(
                'action_type'=>'pop',
                'action_params'=>array(
                    'url'=>'#',
                    'method'=>'#'
                ),
                'label'=>Yii::t('app7020','Dismiss'),
                'fa_icon'=>'fa-ban',
                'text_color'=>'#333',
                'id'=>'',
                'class'=>''

            );
            if($item[type_id]=='knowledge_asset'){
                array_unshift($item[actions],$new_action);
            }
            $newChannelItems[] = $item;
        }

        // Produce the response
        $countItems = count($allItems);

        $result = array(
            //"items"         => $channelItems,
            "items"         => $newChannelItems,
            "items_count"   => $countItems,
            "counters"      => $this->getStdCounter($countItems)
        );
        return $result;
    }
}