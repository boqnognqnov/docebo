<?php

/**
 * Channels containing all non started items (i.e. courses/LP not yet started or knowledge assets not yet played)
 */
class ChannelDataSingleChannel extends ChannelData implements IChannelData {

	/**
	 * Returns channel info (no other_fields is needed for this special channel)
	 * {@inheritDoc}
	 * @see ChannelData::getChannelInfo()
	 */
	public function getChannelInfo() {
		return parent::getChannelInfo();
	}

	/**
	 * Returns items for this channel, sorted and paginated
	 * {@inheritDoc}
	 * @see IChannelData::getChannelItems()
	 */
	public function getChannelItems() {
		$allItems = array();
		$channelItems = array();
		$info = $this->getChannelInfo();
		if ($info['type'] == 'invitations') {
			$contentType = 2;
		}
		if ($info['type'] == 'continuewatching') {
			$contentType = 3;
		}


		// Extract assets from the "custom" channel
		$assets = App7020ChannelAssets::getChannelAssets($this->id_channel, 0, 0, $this->id_user);
        Log::_($assets);
		foreach ($assets as $asset) {
			$item = null;
			switch ($asset['asset_type']) {
				case App7020Assets::ASSET_TYPE_COURSE: // Course item
					$item = $this->getCourseWithMetadata($asset['idAsset']);
					break;
				case App7020Assets::ASSET_TYPE_LEARNINGPLAN: // Learningplan item
					$item = $this->getPlanWithMetadata($asset['idAsset']);
					break;
				case App7020Assets::ASSET_TYPE_KNOWLEDGE_ASSET: // Asset
					if (PluginManager::isPluginActive('Share7020App'))
						$check = App7020DismissHistory::model()->findByAttributes(array('idUser' => $this->id_user, 'idContent' => $asset['idAsset'], 'contentType' => $contentType));
					if (!$check && ($info['type'] == 'invitations' || $info['type'] == 'continuewatching')) {
						$item = $this->getAssetWithMetadata($asset);
					}
					break;
				case App7020Assets::ASSET_TYPE_PLAYLIST:
                    if (Yii::app()->legacyWrapper->hydraFrontendEnabled() && PluginManager::isPluginActive('Share7020App'))
                        $check = App7020DismissHistory::model()->findByAttributes(array('idUser' => $this->id_user, 'idContent' => $asset['idAsset'], 'contentType' => $contentType));
                    if (!$check && ($info['type'] == 'invitations' || $info['type'] == 'continuewatching')) {
                        $item = $this->getAssetWithMetadata($asset);
                    }
					break;
				default:
					break;
			}
			if ($item)
				$allItems[] = $item;

			// Sort and slice allItems
		}
		$itemsSlice = $this->sortAndSlice($allItems);
		// Build the final array of data
		$channelItems = [];
		if (!empty($itemsSlice)) {
			$channelItems = $this->buildChannelItems($itemsSlice);
		}
		$newChannelItems = array();
		foreach ($channelItems as $item) {
			$item['pop_status'] = 0;
			$item['dismiss'] = 0;
			$item['dismiss_from'] = $contentType;
			$item['channel_name'] = $info['name'];
			$new_action = array(
				'action_type' => 'pop',
				'action_params' => array(
					'url' => '#',
					'method' => '#'
				),
				'label' => Yii::t('app7020', 'Dismiss'),
				'fa_icon' => 'fa-ban',
				'text_color' => '#333',
				'id' => '',
				'class' => ''
			);
			if ($item[type_id] == 'knowledge_asset' && ($info['type'] == 'invitations' || $info['type'] == 'continuewatching')) {
				array_unshift($item[actions], $new_action);
			}
			$newChannelItems[] = $item;
		}



		// Produce the response
		$result = array(
			//"items_count" => count($allItems),
			"items_count" => count($newChannelItems),
			//"items" => $channelItems,
			"items" => $newChannelItems,
			"counters" => array_merge($this->getStdCounter(count($allItems)), $this->getCounters())
		);

		return $result;
	}

	public function getCounters($counter = null) {
		if (PluginManager::isPluginActive('Share7020App')) {
			$experts = App7020Channels::countExperts($this->id_channel);
			if ($experts > 0) {
				return array(
					"count" => $experts,
					"text" => Yii::t('app7020', 'Experts')
				);
			} else
				return array();
		} else
			return array();
	}

	public function run() {
		$info = $this->getChannelInfo();
		$items = $this->getChannelItems();
		$channelOutput = array_merge($info, $items);

		$result = array(
			"success" => true,
			"channel" => $channelOutput,
		);
		return $result;
	}

}
