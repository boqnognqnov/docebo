<?php
/**
 * Channel embedding the logic of My Courses and My Learning Plans
 */
class ChannelDataMyCourses extends ChannelData implements IChannelData
{
	/**
	 * Returns channel info (no other_fields is needed for this special channel)
	 * {@inheritDoc}
	 * @see ChannelData::getChannelInfo()
	 */
    public function getChannelInfo() {
        return parent::getChannelInfo();
    }


    /**
     * Returns items for this channel, sorted and paginated
     * {@inheritDoc}
     * @see IChannelData::getChannelItems()
     */
    public function getChannelItems() {
        $allItems = array();
        
        // Get all user's Courses & LPs in whatever status
        $allItems = $this->getFormalItems($this->id_user);
        
        // Sort the BIG (combo) array
        $itemsSlice = $this->sortAndSlice($allItems);
            
        	// Build the final array of data
		$channelItems = [];
		if(!empty($itemsSlice)) {
			$channelItems = $this->buildChannelItems($itemsSlice);
		}


        // Produce the response
        $countItems = count($allItems);
        $result = array(
            "items"         => $channelItems,
            "items_count"   => $countItems,
            "counters"      => $this->getStdCounter($countItems),
        );

        return $result;
    }
}