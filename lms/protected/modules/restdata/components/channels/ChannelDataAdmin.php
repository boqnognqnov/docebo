<?php

/**
 * Channel implementation for custom created channels (admin)
 */
class ChannelDataAdmin extends ChannelData implements IChannelData {

	/**
	 *
	 * {@inheritDoc}
	 * @see ChannelData::getChannelInfo()
	 */
	public function getChannelInfo() {
		$result = parent::getChannelInfo();
		$ownInfo = array();
 
		if ($this->channel instanceof App7020Channels && PluginManager::isPluginActive('Share7020App')) {
			$ownInfo = array(
				"other_fields" => array(
					"count_experts" => count(App7020ChannelExperts::model()->findAllByAttributes(array('idChannel' => $this->id_channel)))
				),
			);
		}
		$result = array_merge($result, $ownInfo);
		return $result;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see IChannelData::getChannelItems()
	 */
	public function getChannelItems() {
		$allItems = array();
		$channelItems = array();


		// Extract assets from the "custom" channel
		$assets = App7020ChannelAssets::getChannelAssets($this->id_channel, 0, 0, $this->id_user);
		foreach ($assets as $asset) {
			$item = null;
			switch ($asset['asset_type']) {
				case App7020Assets::ASSET_TYPE_COURSE: // Course item
					$item = $this->getCourseWithMetadata($asset['idAsset']);
					break;
				case App7020Assets::ASSET_TYPE_LEARNINGPLAN: // Learningplan item
					$item = $this->getPlanWithMetadata($asset['idAsset']);
					break;
				case App7020Assets::ASSET_TYPE_KNOWLEDGE_ASSET: // Asset
					if (PluginManager::isPluginActive('Share7020App'))
						$item = $this->getAssetWithMetadata($asset);
					break;
				case App7020Assets::ASSET_TYPE_PLAYLIST:
                    if (Yii::app()->legacyWrapper->hydraFrontendEnabled() && PluginManager::isPluginActive('Share7020App')){
                        $item = $this->getAssetWithMetadata($asset);
                    }
					break;
				default:
					break;
			}
			if ($item)
				$allItems[] = $item;
		}

		// Sort and slice allItems
		$itemsSlice = $this->sortAndSlice($allItems);

		// Build the final array of data
		$channelItems = [];
		if(!empty($itemsSlice)) {
			$channelItems = $this->buildChannelItems($itemsSlice);
		}

		// Produce the response
		$result = array(
			"items_count" => count($allItems),
			"items" => $channelItems,
			"counters" => array_merge($this->getStdCounter(count($allItems)), $this->getCounters())
		);

		return $result;
	}

	/**
	 * 
	 */
	public function getCounters($counter = null) {
		$experts = App7020Channels::countExperts($this->id_channel);
		if($experts > 0) {
			return array(
				"count" => $experts,
				"text" => Yii::t('app7020', 'Experts')
			);
		} else
			return array();
	}

}
