<?php

/**
 * Channels specific class attributes.
 * 
 * @property integer $id_channel Channel ID
 * @property integer $items_per_channel Number of items per channel
 * @property App7020Channels $channel Channel model, if id_channel is available
 * 
 */
class ChannelData extends RestData {

	// Channel item types
	const TYPE_ID_COURSE = 'course';
	const TYPE_ID_COURSE_ELEARNING = 'course_learning';
	const TYPE_ID_COURSE_CLASSROOM = 'course_classroom';
	const TYPE_ID_COURSE_WEBINAR = 'course_webinar';
	const TYPE_ID_PLAN = 'learningplan';
	const TYPE_ID_KNOWLEDGE_ASSET = 'knowledge_asset';

	/**
	 * Constructor 
	 * 
	 * @param string $params
	 */
	public function __construct($params = false) {
		parent::__construct($params);
		if ($this->id_channel) {
			$this->channel = App7020Channels::model()->findByPk($this->id_channel);
		}
		$this->lang_code = App7020Channels::resolveLanguage();
	}

	/**
	 * Get channel items (and some more info about items)
	 */
	public function run() {
		$this->visible = false;
		$args['idUser'] = Yii::app()->user->idst;
		$args['idChannel'] = $this->id_channel;
		$args['return'] = 'count';
		$visibility_to_channel = App7020Channels::extractUserChannels($args);
		if (!empty($visibility_to_channel)) {
			$this->visible = true;
		} else {
			 return array(
				"success" => false,
				"error" => 'Access Denied!',
			);
		}

		if (!$this->single_channel) {
			// Call descendant object instances method to get CHANNEL specific items
			$result = $this->getChannelItems();
			$result["success"] = true;

			// Also, include the id_channel in the response, to easy the front end job, if any
			if (isset($this->id_channel)) {
				$result["id_channel"] = $this->id_channel;
			}
		} else {
			$info = $this->getChannelInfo();
			$items = $this->getChannelItems();
			$channelOutput = array_merge($info, $items);

			$result = array(
				"success" => true,
				"channel" => $channelOutput,
			);
		}


		return $result;
	}

	/**
	 * Build an array of common channel info. Additional information is added by individual channels, if any
	 * 
	 * @return array
	 */
	public function getChannelInfo() {
		if (!$this->channel) {
			return array();
		}
		$c = $this->channel;
		$translatedInfo = $c->translation($this->lang_code);
		$result = array(
			"id" => $c->id,
			"type" => $c->predefined_uid ? strtolower($c->predefined_uid) : 'admin',
			"name" => $translatedInfo["name"],
			"description" => $translatedInfo["description"],
			"fa_icon" => $c->icon_fa,
			"icon_bg_color" => $c->icon_bgr,
			"fa_icon_color" => $c->icon_color,
			"other_fields" => array(),
		);
		return $result;
	}

	/**
	 * Populates learning plan channel item details based on the user enrollment
	 * 
	 * @param $idUser
	 * @param $plan LearningCoursepath
	 */
	protected function resolveLearningPlanAccessInfo($idUser, $plan) {
		// Get the enrollment of the user, if any
		$enrollment = LearningCoursepathUser::model()->findByAttributes(array(
			'idUser' => $idUser,
			'id_path' => $plan->id_path
		));

		// If user is enrolled...
		if ($enrollment) {
			// Determine completion percentage and courses count
			$completionInfo = $plan->getCoursepathPercentage($idUser);

			// Populate actions menu
			$actions = array();
			if ($plan->deep_link) {
				$actions[] = array(
					"action_type" => "deeplink",
					"action_params" => array("url" => $plan->getDeeplink()),
					"label" => Yii::t('course', 'Get sharable link'),
					"fa_icon" => "fa-share",
					"text_color" => "#333333"
				);
			}

			$result = array(
				'is_new' => $completionInfo['total'] == 0,
				'can_enter' => true,
				'ribbon' => array(),
				'actions' => $actions,
				'other_fields' => array(
					"courses_count" => $completionInfo['total'],
					"percentage" => $completionInfo['percentage']
				)
			);

			return $result;
		} else {
			if (Yii::app()->user->getIsGodadmin()) {
				$result = array(
					"can_enter" => false,
					"ribbon" => array(
						"text_color" => "#FFFFFF",
						"icon_bg_color" => "#333333",
						"fa_icon" => "fa-lock",
						"message" => Yii::t("standard", "Not enrolled")
					),
					"actions" => array(),
				);
				return $result;
			}
			throw new CException("Cannot populate channel item '" . $plan->id_path . "' because user with ID = " . $idUser . " is not enrolled to the LP");
		}
	}

	/**
	 * Populates course channel item details based on the user enrollment
	 * 
	 * @param integer $idUser        	
	 * @param LearningCourse $course        	
	 * @return array
	 */
	protected function resolveCourseAccessInfo($idUser, $course) {
		// Get the enrollment of the user, if any
		$enrollment = LearningCourseuser::model()->findByAttributes(array(
			'idUser' => $idUser,
			'idCourse' => $course->idCourse
		));

		// If user is enrolled...
		if ($enrollment) {
			$statuses_translations = LearningCourseuser::getStatusesArray();
			$status = $statuses_translations[$enrollment->status];
			$level = $enrollment->getLevelLabel();
			$ribbon = array();

			// Check if user can enter the course
			$canEnterCourse = $enrollment->canEnterCourse();
			if ($canEnterCourse['can']) {
				$canPlay = true;

				// Ok... We can enter. Is there a deadline upcoming?
				if ($deadline = $course->getDeadlineForUser($idUser)) {
					$ribbon = array(
						"text_color" => "#FFFFFF",
						"icon_bg_color" => "#E74C3C",
						"fa_icon" => "fa-clock",
						"message" => Yii::t('course', 'Deadline') . ': ' . Yii::app()->localtime->stripTimeFromLocalDateTime($deadline)
					);
				}
			} else { // If, not, state the reason as an action label, or tag
				$canPlay = false;
				switch ($canEnterCourse['reason']) {
					case 'prerequisites':
						$lockReason = Yii::t('organization', '_ORGLOCKEDTITLE');
						break;
					case 'waiting':
						$lockReason = Yii::t('catalogue', '_WAITING_APPROVAL');
						break;
					case 'user_status':
					case 'course_status':
					case 'subscription_expired':
					case 'subscription_not_started':
					case 'course_date':
					default:
						$lockReason = Yii::t('storage', '_LOCKED');
						if ($canEnterCourse['reason'] == 'user_status') {
							$statusesList = LearningCourseuser::getStatusesArray();
							$lockReason = Yii::t('standard', 'Enrollment status') . ': ' . (isset($statusesList[$enrollment->status]) ? strtolower($statusesList[$enrollment->status]) : '');
						} else if ($canEnterCourse['reason'] == 'course_status') {
							$statusesList = LearningCourse::statusesList();
							$lockReason = Yii::t('standard', 'Status') . ': ' . (isset($statusesList[$course->status]) ? strtolower($statusesList[$course->status]) : '');
						} else if ($canEnterCourse['reason'] == 'subscription_expired') {
							$lockReason = Yii::t('subscribe', '_DATE_EXPIRE_VALIDITY') . ': ' . (($canEnterCourse['expiring_in']) ? $canEnterCourse['expiring_in'] : (($enrollment->date_expire_validity && $enrollment->date_expire_validity != '0000-00-00 00:00:00') ? $enrollment->date_expire_validity : $course->date_end));
						} else if ($canEnterCourse['reason'] == 'subscription_not_started') {
							$lockReason = Yii::t('subscribe', '_DATE_BEGIN_VALIDITY') . ': ' . (($enrollment->date_begin_validity && $enrollment->date_begin_validity != '0000-00-00 00:00:00') ? $enrollment->date_begin_validity : $course->date_begin);
						} else if ($canEnterCourse['reason'] == 'course_date') {
							$_tmp = array();
							if (!empty($course->date_begin) && $course->date_begin != '0000-00-00')
								$_tmp[] = Yii::t('subscribe', '_DATE_BEGIN_VALIDITY') . ': ' . $course->date_begin;
							if (!empty($course->date_end) && $course->date_end != '0000-00-00')
								$_tmp[] = Yii::t('subscribe', '_DATE_EXPIRE_VALIDITY') . ': ' . $course->date_end;
							if (!empty($_tmp))
								$lockReason = implode(' ', $_tmp);
						}
				}

				$ribbon = array(
					"text_color" => "#FFFFFF",
					"icon_bg_color" => "#333333",
					"fa_icon" => "fa-lock",
					"message" => $lockReason
				);
			}

			// Populate actions menu
			$actions = array();
			if ($course->deep_link) {
				$actions[] = array(
					"action_type" => "deeplink",
					"action_params" => array("url" => $course->getDeepLink()),
					"label" => Yii::t('course', 'Get sharable link'),
					"fa_icon" => "fa-share",
					"text_color" => "#333333"
				);
			}

			$result = array(
				'is_new' => $enrollment->status == LearningCourseuser::$COURSE_USER_SUBSCRIBED,
				'can_enter' => $canPlay,
				'ribbon' => $ribbon,
				'actions' => $actions,
				'other_fields' => array(
				    "status_code" => $enrollment->status,
					"status" => $status,
					"level" => $level,
					"language" => Lang::getNameByCode($course->lang_code),
					"duration" => $course->formatAverageCourseTime()
				)
			);

			return $result;
		} else {
			if (Yii::app()->user->getIsGodadmin()) {
				$result = array(
					"can_enter" => false,
					"ribbon" => array(
						"text_color" => "#FFFFFF",
						"icon_bg_color" => "#333333",
						"fa_icon" => "fa-lock",
						"message" => Yii::t("standard", "Not enrolled")
					),
					"actions" => array(),
				);
				return $result;
			}
			throw new CException("Cannot populate channel item '" . $course->idCourse . "' because user with ID = " . $idUser . " is not enrolled to the course");
		}
	}

	/**
	 * Returns the list of formal items (i.e. courses & LPs) for the passed user, optionally filtering them by enrollment status
	 * 
	 * @param $idUser The user enrolled in the formal items
	 * @param string $status An optional filter status (possible values "completed", "in_progress", "not_started"
	 *
	 */
	protected function getFormalItems($idUser, $status = null) {
		$sort_by = $this->sort_by ? $this->sort_by : false;
		$sort_dir = strtolower($this->sort_dir ? $this->sort_dir : RestData::SORT_BY_ASC);
		$params = array();
		if ($status)
			$params['fltEnrollmentStatus'] = $status;

		// Fetch my courses, optionally filtered by status
		// and add sorting meta keys
		$courses = LearningCourseuser::sqlDataProviderMyCourses($idUser, $params)->data;
		if (!empty($courses)) {
			array_walk($courses, function (&$item, $index, $sort_by) {
				$item["__type_id__"] = self::TYPE_ID_COURSE; // Add this meta key, to recognize courses, later
				switch ($sort_by) {
					case RestData::SORT_BY_NAME:
						$item[RestData::SORT_META_KEY_NAME] = "name";
						break;
					case RestData::SORT_BY_CREATION_DATE:
						$item[RestData::SORT_META_KEY_NAME] = "create_date";
						break;
					default:
						$item[RestData::SORT_META_KEY_NAME] = "idCourse";
						break;
				}
			}, $sort_by);
		}

		// Fetch learning plans, optionally filtered by status
		// and add sorting meta keys
		$plans = $this->getMyLearningPlans($idUser, $params);
		array_walk($plans, function(&$item, $index, $sort_by) {
			$item["__type_id__"] = self::TYPE_ID_PLAN;
			switch ($sort_by) {
				case RestData::SORT_BY_NAME:
					$item[RestData::SORT_META_KEY_NAME] = "path_name";
					break;
				case RestData::SORT_BY_CREATION_DATE:
					$item[RestData::SORT_META_KEY_NAME] = "create_date";
					break;
				default:
					$item[RestData::SORT_META_KEY_NAME] = "id_path";
					break;
			}
		}, $sort_by);

		// Combine them in a single array
		return array_merge($courses, $plans);
	}

	/**
	 * Receives an array of items of various types (LP, courses or knowledge assets)
	 * and returns array of items of all items' data filled up
	 *
	 * @param $itemsInput Mixed array of items 
	 * @param $idUser The user for which items must be populated (e.g. used to determine enrollment status information for courses & LPs)
	 *
	 * @return array The json-ready array of items
	 */
	protected function buildChannelItems($itemsInput) {

		$items = array();

		if (!empty($itemsInput) && is_array($itemsInput)) {
			foreach ($itemsInput as $item) {

				switch ($item['__type_id__']) {

					case self::TYPE_ID_COURSE:
						$filledItem = $this->buildCourseItem($item);
						break;
					case self::TYPE_ID_PLAN:
						$filledItem = $this->buildPlanItem($item);
						break;
					case self::TYPE_ID_KNOWLEDGE_ASSET:
						$feed = AssetData::factory($item, $this->id_user);
						$filledItem = $feed->getData();
						break;
				}

				if ($filledItem && !empty($filledItem)) {
					$items[] = $filledItem;
				}
			}
		}
		return $items;
	}

	/**
	 * Returns a course record by id and inserts the needed sorting metadata
	 *
	 * @param $idCourse The ID of the course to build
	 *
	 * @return array The course record with the needed metadata
	 */
	protected function getCourseWithMetadata($idCourse) {
		$command = Yii::app()->db->createCommand();
		$command->select('*');
		$command->from('learning_course lc');
		$command->where('lc.idCourse=:idCourse');
		$item = $command->queryRow(true, array(':idCourse' => $idCourse));
		$sort_by = $this->sort_by ? $this->sort_by : false;
		$item["__type_id__"] = self::TYPE_ID_COURSE; // Add this meta key, to recognize courses, later
		switch ($sort_by) {
			case RestData::SORT_BY_NAME:
				$item[RestData::SORT_META_KEY_NAME] = "name";
				break;
			case RestData::SORT_BY_CREATION_DATE:
				$item[RestData::SORT_META_KEY_NAME] = "create_date";
				break;
			default:
				$item[RestData::SORT_META_KEY_NAME] = "idCourse";
				break;
		}

		return $item;
	}

	/**
	 * Returns a learningplan record by id and inserts the needed sorting metadata
	 *
	 * @param $idPlan The ID of the plan to build
	 *
	 * @return array The plan record with the needed metadata
	 */
	protected function getPlanWithMetadata($idPlan) {
		$command = Yii::app()->db->createCommand();
		$command->select('*');
		$command->from('learning_coursepath lcp');
		$command->where('lcp.id_path=:idPlan');
		$item = $command->queryRow(true, array(':idPlan' => $idPlan));
		$sort_by = $this->sort_by ? $this->sort_by : false;
		$item["__type_id__"] = self::TYPE_ID_PLAN; // Add this meta key, to recognize learning plans later
		switch ($sort_by) {
			case RestData::SORT_BY_NAME:
				$item[RestData::SORT_META_KEY_NAME] = "path_name";
				break;
			case RestData::SORT_BY_CREATION_DATE:
				$item[RestData::SORT_META_KEY_NAME] = "create_date";
				break;
			default:
				$item[RestData::SORT_META_KEY_NAME] = "id_path";
				break;
		}

		return $item;
	}

	/**
	 * Returns a asset record by id and inserts the needed sorting metadata
	 *
	 * @param $id The ID of the asset to build
	 *
	 * @return array The asset record with the needed metadata
	 */
    protected function getAssetWithMetadata($item, $setWatched = false)
    {
		$sort_by = $this->sort_by ? $this->sort_by : false;
		$item["__type_id__"] = self::TYPE_ID_KNOWLEDGE_ASSET; // Add this meta key, to recognize knowledge asset later
		switch ($sort_by) {
			case RestData::SORT_BY_NAME:
				$item[RestData::SORT_META_KEY_NAME] = "title";
				$item['name'] = $item['title'];
                if (!$setWatched) {
                    $item['watched'] = App7020ContentHistory::isContentViewedByUser($item['id']);
                } else {
                    $item['watched'] = 1;
                }
				break;
			case RestData::SORT_BY_CREATION_DATE:
				$item[RestData::SORT_META_KEY_NAME] = "created";
				$item['name'] = $item['title'];
                if (!$setWatched) {
                    $item['watched'] = App7020ContentHistory::isContentViewedByUser($item['id']);
                } else {
                    $item['watched'] = 1;
                }
                break;
			default:
				$item[RestData::SORT_META_KEY_NAME] = "id";
				$item['name'] = $item['title'];
                if (!$setWatched) {
                    $item['watched'] = App7020ContentHistory::isContentViewedByUser($item['id']);
                } else {
                    $item['watched'] = 1;
                }
                break;
		}

		return $item;
	}

	/**
	 * Returns an array of learning plan items, optionally filtered by enrollment status
	 * @param $idUser The user enrolled in the formal items
	 * @param string $status An optional filter status (possible values "completed", "in_progress", "not_started"
	 *
	 * @return array
	 */
	protected function getMyLearningPlans($idUser, $params = array()) {
		$sqlParams = array(
			':idUser' => $idUser
		);
		$command = Yii::app()->db->createCommand();
		$command->select('lp.*, COUNT(cpc.id_item) AS totalCourses, SUM(cu.status=1) AS inProgressCourses, SUM(cu.status=0) AS nonStartedCourses, SUM(cu.status=2) AS completedCourses');
		$command->from('learning_coursepath_user lcu');
		$command->join('learning_coursepath lp', 'lp.id_path=lcu.id_path');
		$command->leftJoin('learning_coursepath_courses cpc', 'lp.id_path=cpc.id_path');
		$command->leftJoin('learning_courseuser cu', 'cpc.id_item=cu.idCourse AND cu.idUser=:idUser');
		$command->where('lcu.idUser=:idUser');
		$command->group('lp.id_path');

		switch ($params['fltEnrollmentStatus']) {
			case 'not_started':
				$command->having('totalCourses = nonStartedCourses');
				break;
			case 'in_progress':
				$command->having('inProgressCourses > 0');
				break;
			case 'completed':
				$command->having('totalCourses = completedCourses');
				break;
			default:
		}

		return $command->queryAll(true, $sqlParams);
	}

	/**
	 * Returns channel item details for single course
	 *
	 * @param array $courseItem The course item read from DB
	 *
	 * @return array The json ready channel item
	 */
	protected function buildCourseItem($courseItem) {
		$result = array();

		// Time to load to load the AR model for course
		// We should be safe here, because items arriving at this stage are already paginated!
		// So no risk of memory blowing up
		$course = LearningCourse::model()->findByPk($courseItem['idCourse']);
		$typeIdentifier = self::TYPE_ID_COURSE_ELEARNING;
		if ($course->isClassroomCourse())
			$typeIdentifier = self::TYPE_ID_COURSE_CLASSROOM;
		else if ($course->isWebinarCourse())
			$typeIdentifier = self::TYPE_ID_COURSE_WEBINAR;

		// Get course access info
		$accessInfo = $this->resolveCourseAccessInfo(Yii::app()->user->id, $course);
		$result = array_merge(array(
			"type_id" => $typeIdentifier,
			"type_label" => $course->getCourseTypeTranslation($course->course_type),
			"id" => $course->idCourse,
			"title" => html_entity_decode($course->name),
			"uri" => Docebo::createAbsoluteLmsUrl('player', array('course_id' => $course->idCourse)),
			"image_uri" => $course->getCourseLogoUrl(CoreAsset::VARIANT_ORIGINAL, true),
			"rating" => $course->canViewRating() ? $course->getRating() : null,
			"create_date" => $courseItem['create_date']
				), $accessInfo);

		return $result;
	}

	/**
	 * Returns channel item details for single learning plan
	 *
	 * @param array $planItem The LP item read from DB
	 *
	 * @return The json ready channel item
	 */
	protected function buildPlanItem($planItem) {
		$result = array();

		// Time to load to load the AR model for LP
		// We should be safe here, because items arriving at this stage are already paginated!
		// So no risk of memory blowing up
		$plan = LearningCoursepath::model()->findByPk($planItem["id_path"]);

		// Get LP access info
		$accessInfo = $this->resolveLearningPlanAccessInfo(Yii::app()->user->id, $plan);
		$result = array_merge(array(
			"type_id" => self::TYPE_ID_PLAN,
			"type_label" => Yii::t('standard', '_COURSEPATH'),
			"id" => $plan->id_path,
			"title" => $plan->path_name,
			"uri" => Docebo::createAbsoluteLmsUrl('curricula/show', array('id_path' => $plan->id_path)),
			"image_uri" => $plan->getLogo(CoreAsset::VARIANT_ORIGINAL, true),
			"rating" => null,
			"create_date" => $planItem['create_date'],
				), $accessInfo);


		return $result;
	}

}
