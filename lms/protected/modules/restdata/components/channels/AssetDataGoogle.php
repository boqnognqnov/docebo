<?php
/**
 *
 */
class AssetDataGoogle extends AssetData implements IAssetData
{

    public function getData() {
        $type = (App7020Assets::$contentTypes[$this->asset['contentType']]['outputPrefix']) ? App7020Assets::$contentTypes[$this->asset['contentType']]['outputPrefix'] : "other";
        $url = Docebo::createApp7020AssetsViewUrl($this->asset['id']);
		if(Yii::app()->legacyWrapper->hydraFrontendEnabled())
            $url= '/share/asset/view/'.$this->asset['id'];

		if($this->asset['created']){
			$dateCreated = $this->asset['created'];
		} elseif ($this->asset['create_date']) {
			$dateCreated = $this->asset['create_date'];
		}
        $result = array(
            "type_id"       => ChannelData::TYPE_ID_KNOWLEDGE_ASSET,
            "type_label"    => $type,
            "id"            => $this->asset['id'],
            "title"         => $this->asset['title'],
            "uri"           => $url,
            "image_uri"     => $this->getAssetThumbnailUri(),
            "rating"        => round($this->asset['contentRating'], 2),
            "is_new"        => ($this->asset['watched'] == 0) && ($this->asset['userId'] != Yii::app()->user->idst),
            "can_enter"     => false,
            "ribbon"        => $this->getRibbon(),
            "actions"       => $this->getActions(),
            "other_fields"  => $this->getOtherFields(),
			"create_date"	=> $dateCreated
        );

        return $result;
    }

    /**
     * Get Asset other fields
     */
    public function getOtherFields() {
        $otherFields = $this->getCommonOtherFields();

        if (!empty($this->asset['inviterId'])){
            $inviterAvatar = Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $this->asset['inviterId']), true);
            $otherFields['invited_by'] = $this->asset['invitedBy'];
			if(strlen($otherFields["invited_by"])<="1") {
				$otherFields['invited_by'] = CoreUser::getForamattedNames($this->asset['inviterId']);
			}
            $otherFields['invited_by_url'] = $inviterAvatar;
        }

        $otherFields["duration"] = $this->getAssetDuration();

        return $otherFields;
    }
	public function getAssetThumbnailUri() {
		switch ($this->asset['contentType']) {
			case App7020Assets::CONTENT_TYPE_GOOGLE_DRIVE_DOCS :
				$thumbnail = Yii::app()->theme->baseUrl . "/img/gdoc.gif";
				break;
			case App7020Assets::CONTENT_TYPE_GOOGLE_DRIVE_SHEETS :
				$thumbnail = Yii::app()->theme->baseUrl . "/img/gsheet.gif";
				break;
			case App7020Assets::CONTENT_TYPE_GOOGLE_DRIVE_SLIDES :
				$thumbnail = Yii::app()->theme->baseUrl . "/img/gslides.gif";
				break;
		}
		return $thumbnail;
	}
}