<?php
/**
 * Channels containing all completed items (i.e. courses/LP completed or knowledge assets viewed)
 */
class ChannelDataMyHistory extends ChannelData implements IChannelData
{
    /**
     * Returns channel info (no other_fields is needed for this special channel)
     * {@inheritDoc}
     * @see ChannelData::getChannelInfo()
     */
    public function getChannelInfo() {
        return parent::getChannelInfo();
    }

    /**
     * Returns items for this channel, sorted and paginated
     * {@inheritDoc}
     * @see IChannelData::getChannelItems()
     */
    public function getChannelItems(){
        $allItems = array();

        // Get all user's Courses & LPs in completed status
        $allItems = $this->getFormalItems($this->id_user, "completed");
        $assets = array();
		
        if (PluginManager::isPluginActive('Share7020App')) {
            $viewed = App7020ContentHistory::getContentViewedByUser($this->id_user);
			
            foreach ($viewed as $item) {
                //$check = App7020DismissHistory::model()->findByAttributes(array('idUser'=>$this->id_user,'idContent'=>$item['id'],'contentType'=>3));
                //if(!$check) {
                    $assets[] = $this->getAssetWithMetadata($item);
                //}
            }
        }

        $allItems = array_merge($allItems, $assets);
		 
        // Sort the BIG (combo) array
        $itemsSlice = $this->sortAndSlice($allItems);
		
       	// Build the final array of data
		$channelItems = [];
		if(!empty($itemsSlice)) {
			$channelItems = $this->buildChannelItems($itemsSlice);
		}
        $newChannelItems=array();
        foreach($channelItems as $item){
            $item['pop_status'] = 0;
            $item['dismiss'] = 0;
            $item['dismiss_from'] = 3;
            $item['channel_name'] = Yii::t('app7020','My learning activity history');
            $new_action = array(
                'action_type'=>'pop',
                'action_params'=>array(
                    'url'=>'#',
                    'method'=>'#'
                ),
                'label'=>Yii::t('app7020','Dismiss'),
                'fa_icon'=>'fa-ban',
                'text_color'=>'#333',
                'id'=>'',
                'class'=>''

            );
            //if($item[type_id]=='knowledge_asset'){
            //    array_unshift($item[actions],$new_action);
            //}
            $newChannelItems[] = $item;
        }

		
        // Produce the response
        $countItems = count($allItems);
        $result = array(
            //"items"         => $channelItems,
            "items"         => $newChannelItems,
            "items_count"   => $countItems,
            "counters"      => $this->getStdCounter($countItems)
        );
        return $result;
    }
}