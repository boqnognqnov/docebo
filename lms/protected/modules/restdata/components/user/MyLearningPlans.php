<?php
/**
 * Learning plan specific class attributes.
 * 
 * @property integer idUser Id for the current logged user
 * 
 */
class MyLearningPlans extends ChannelData {
	
	public function __construct($params = false) {
		parent::__construct($params);
	}

	public function run(){
		$learningPlans = $this->getMyLearningPlans($this->idUser);
		$result = $this->getInfo($learningPlans);
		return $result;
	}
	
	protected function getInfo($items){
		$info = array(
            "id"                => null,
            "name"              => Yii::t('mobile', 'My Learning Plans'),
            "description"       => "&nbsp;",
            "fa_icon"           => "fa-th-list",
			"other_fields"      => array(),
			"count" => count($items),
			"items" => array()
        );
		$items_result = $this->iterateResults($items);
		$result = array_merge($info, $items_result);
		return $result;
	}
	
	protected function iterateResults($items){
		$result = array();
		foreach($items as $key => $value){
			$result['items'][$key] = $this->buildPlanItem($value);
		}
        $result['items'] = $this->sortAndSlice($result['items']);
		return $result;
	}
}