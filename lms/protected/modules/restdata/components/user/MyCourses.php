<?php
/**
 * Courses specific class attributes.
 * 
 * @property integer idUser Id for the current logged user
 * 
 */
class MyCourses extends ChannelData {
	
	public function __construct($params = false) {
		parent::__construct($params);
	}

	public function run(){
		$courses = LearningCourseuser::sqlDataProviderMyCourses($this->idUser)->data;
		$result = $this->getInfo($courses);
		return $result;
	}
	
	protected function getInfo($items){
		$info = array(
            "id"                => null,
            "name"              => Yii::t('mobile', 'My Courses'),
            "description"       => "&nbsp;",
            "fa_icon"           => "fa-book",
			"other_fields"      => array(),
			"count" => count($items),
			"items" => array()
        );
		$items_result = $this->iterateResults($items);
		$result = array_merge($info, $items_result);
		return $result;
	}
	
	protected function iterateResults($items){
		$result = array();
		foreach($items as $key => $value){
			$result['items'][$key] = $this->buildCourseItem($value);
		}
        $result['items'] = $this->sortAndSlice($result['items']);
		return $result;
	}
	
}