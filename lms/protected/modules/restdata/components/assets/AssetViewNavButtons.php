<?php

class AssetViewNavButtons extends RestData {

	/**
	 * Constructor
	 *
	 * @param array $params
	 */
	public function __construct($params = false) {
		parent::__construct($params);
	}

	/**
	 * Execute operation
	 */
	public function run() {
		$buttons = array(
			'actions' => $this->_assetNavActions()
		);
		return $buttons;
	}

	private function _assetNavActions() {
		$experts = App7020Assets::getAssetExperts($this->id_asset);
		$assetObject = App7020Assets::model()->findByPk($this->id_asset);
		if ($this->mode == 'view') {
            if (Yii::app()->user->isGodAdmin || in_array(Yii::app()->user->idst, $experts) || $assetObject->userId == Yii::app()->user->id) {
				/* Default Rules */
				$actions = array('ask', 'stats', 'edit', 'delete');
				return $actions;
			} else {
				return array('ask');
			}
		}

		if ($this->mode == 'edit') {
			if (Yii::app()->user->isGodAdmin   || $assetObject->userId == Yii::app()->user->id || in_array(Yii::app()->user->idst, $experts)) {
				/* Default Rules */
				$actions = array('stats', 'view', 'delete', 'menu_peerReview');
				/* Show tooltips tab in navigation */
				if ($assetObject->contentType == App7020Assets::CONTENT_TYPE_VIDEO)
					array_push($actions, 'menu_tooltips');
				
				return $actions;
			}else {
				return array();
			}
		}
	}

	public function getUsersDataFromArray($arrayUsers = array()) {
		$users = array();
		$languages = array();
		foreach ($arrayUsers as $userModel) {

			$users[] = $userModel['idst'];
		}

		return $users;
	}

}
