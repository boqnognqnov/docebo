<?php

/**
 * 
 * 
 */
class AssetRelatedData extends RestData {

	const ACTION_UID_MAKE_TOOLTIP = "make_tooltip";
	const ACTION_UID_EDIT = "edit";
	const ACTION_UID_DELETE = "delete";

	/**
	 * Constructor 
	 * 
	 * @param array $params
	 */
	public function __construct($params = false) {
		parent::__construct($params);
	}

	public function run() {
		if (!$this->id_asset || !$assetObject = App7020Assets::model()->findByPk($this->id_asset)) {
			$result = array(
				'success' => false,
				'error' => Yii::t("standard", "Invalid data provided")
			);
			return $result;
		} else {
			$items = array();

			if (!$this->from)
				$this->from = 0;

			if (!$this->count)
				$this->count = 5;

			if ($this->endpoint == RestData::ENDPOINT_ASSET_SIMILAR) {
 
				$items = $this->_getSimilarAssets();
			}

			if ($this->endpoint == RestData::ENDPOINT_ASSET_PR) {
				$items = $this->_getAssetPR();
			}

			return $items;
		}
	}

	private function _getSimilarAssets() {
		$items = array();
		
		$similarAssets = App7020Assets::getSimilarContents($this->id_asset, $this->count, $this->from, $this->hasFailback);

		if ($similarAssets) {
			foreach ($similarAssets as $value) {

				$items[] = array(
					'asset_id' => $value['id'],
					'asset_page_link' => Docebo::createApp7020Url('assets/view') . $value['id'],
					'asset_type' => (App7020Assets::$contentTypes[$value['contentType']]['outputPrefix']) ? App7020Assets::$contentTypes[$value['contentType']]['outputPrefix'] : "other",
					'thumbnail_url' => App7020Assets::getAssetThumbnailUri($value['id']),
					'views' => $value['contentViews'],
					'update_date' => App7020Helpers::timeAgo($value['created']),
					'title' => $value['title'],
					'other_fields' => $this->getOtherFields($value['id'])
				);
			}
		} 
		return $items;
	}

	private function _getAssetPR() {
		$experts = array();
		$items = array();

		$attendedExperts = App7020ContentReview::getAttendedExperts($this->id_asset, true);
		if ($attendedExperts) {
			foreach ($attendedExperts as $value) {
				$experts[] = array(
					//'thumbnail_url' => CoreUser::getAvatarByUserId($value->idst),
					'thumbnail_url' => Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $value->idst),true),
					'expert_names' => CoreUser::getForamattedNames($value->idst, " ", false),
					'expert_channel_url' => Docebo::createLmsUrl('channels/index/#/pchannel/' . $value->idst)
				);
			}
		}

		$prArray = App7020ContentReview::getPeerReviewesByContent($this->id_asset, $this->from, 0);
		if ($prArray) {
			$counter = 0;
			foreach ($prArray as $value) {
				$date=date_create($value['created']);
				$hours = Yii::app()->localtime->toLocalTime($value['created']);
				$items[$value['id']] = array(
					'id' => $value['id'],
					'pr_owner' => Yii::app()->user->id == $value['idUser'] ? Yii::t("standard", "Me") : CoreUser::getForamattedNames($value['idUser'], " ", false),
					//'avatar' => CoreUser::getAvatarByUserId($value['idUser']),
					'avatar' => Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $value['idUser']),true),
					'type' => Yii::app()->user->id == $value['idUser'] ? "mine" : "others",
					'update_date' => App7020Helpers::timeAgo($value['created']),
					'date' => date_format($date, 'Y-m-d'),
					'title' => $value['message'],
					'actions' => $this->_getPRActions($value['id']),
					'counter' =>$counter,
					'status' =>$value['systemMessage'],
					//'hour'=>date_format(date_create($hours), 'H:i'),
				);
				$ids[] = $value["id"];
			$counter++;}
		}

		return array('items' => $items, 'experts' => $experts , 'ids' => $ids);
	}

	private function _getPRActions($idPR) {

		$actions = array();
		$prObject = App7020ContentReview::model()->findByPk($idPR);
		$prAsset = App7020Assets::model()->findByPk($prObject->idContent);
		if($prAsset->contentType == App7020Assets::CONTENT_TYPE_VIDEO) {
			$actions[] = array(
				'action_type' => 'drpo_down',
				'action_uid' => self::ACTION_UID_MAKE_TOOLTIP,
				'action_params' => array(
					'url' => 'javascript::'
				),
				'label' => Yii::t('standard', 'Make tooltip'),
				'text_color' => '#333333'
			);
		}
		if ($prObject->idUser == Yii::app()->user->id) {

			$actions[] = array(
				'action_type' => 'modal',
				'action_uid' => self::ACTION_UID_EDIT,
				'action_params' => array(
					'url' => 'javascript::'
				),
				'label' => Yii::t('standard', 'Edit'),
				'text_color' => '#333333'
			);
		}

		if ($prObject->idUser == Yii::app()->user->id || Yii::app()->user->isGodAdmin) {

			$actions[] = array(
				'action_type' => 'modal',
				'action_uid' => self::ACTION_UID_DELETE,
				'action_params' => array(
					'url' => 'javascript::'
				),
				'label' => Yii::t('standard', 'Delete'),
				'text_color' => '#e84b3c'
			);
		}

		return $actions;
	}

	/**
	 * Get Asset other fields
	 */
	public function getOtherFields($idAsset) {
		$assetObject = App7020Assets::model()->findByPk($idAsset);
		if (!$assetObject->id)
			return array();

		$lastView = "";
		if ($assetObject->viewes) {
			$lastView = App7020ContentHistory::model()->findByAttributes(array('idContent' => $assetObject->id), array('order' => 'viewed DESC'))->viewed;
		}

		$otherFields = array(
			"views" => count($assetObject->viewes),
			"last_viewed" => $lastView ? App7020Helpers::timeAgo($lastView) . ' ' . Yii::t('app7020', 'ago') : "",
			"status" => (int) $assetObject->conversion_status, 
			"author" => CoreUser::getForamattedNames($assetObject->userId),
			"peer_reviews" => App7020ContentReview::getNewPeerReviewesByContent($assetObject->id, $this->id_user),
			"personal_channel" => Docebo::createLmsUrl('channels/index') . '/#/pchannel/' . $assetObject->userId
		);

		return $otherFields;
	}

}
