<?php

/**
 *
 */
class AssetItem extends RestData {

	const ACTION_UID_DOWNLOAD = "download";
	const ACTION_UID_INVITE = "invite";
	const PAGINATION_ITEMS_STEP = 5;

	public $assetObject;
	public $hasFailback = false;

	/**
	 * Constructor
	 *
	 * @param array $params            
	 */
	public function __construct($params = false) {
		parent::__construct($params);
	}

	public function run() {

		$isMobile = false;
		$onlySrc = false;
		if (isset($this->endpointParams['is_mobile']) && !empty($this->endpointParams['is_mobile']))
			$isMobile = true;

		if (isset($this->endpointParams['onlySrc']))
			$onlySrc = true;

		if (!$this->id_asset || !$assetObject = App7020Assets::model()->findByPk($this->id_asset)) {
			$result = array(
				'success' => false,
				'error' => Yii::t("standard", "Invalid data provided")
			);
			return $result;
		} else {
			$this->assetObject = $assetObject;
			if (!$this->_checkAccess()) {
				$result = array(
					'success' => false,
					'error' => Yii::t("standard", "Unauthorized access")
				);
				return $result;
			}

			$lastEdit = App7020ContentPublished::getLastEdited($assetObject->id);
			$date = $assetObject->published->datePublished;
			$publishedString = date('Y-m-d', strtotime($date));

			if ($onlySrc) {
				$user = App7020Helpers::getUserAvatar($assetObject->userId);
				$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
				$avatarImg = $storage->fileUrl($user['avatar']);
				$similarTotal = '';
			} else {
				/* Temp fix for mobile, because it is breaking asset view ::TODO NEEED TO BE REMOVED WHEN MERGED FIX FROM 7020 */
				$similarTotal = !$assetObject->isPrivate() ? count(App7020Assets::getSimilarContents($this->id_asset, 0, 0, 1)) : 0;
			}
			//get simillar videos 
			$countSimilarVideos = count(App7020Assets::getSimilarContents($this->id_asset, 0, 0, false, true));
			$this->hasFailback = false;
			if ($countSimilarVideos < self::PAGINATION_ITEMS_STEP) {
				$this->hasFailback = true;
			}
			$similar_videos = $this->_getSimilarAssets();
			if (!$assetObject->isVisibleToUser()) {
				return array(
					'success' => false,
					'error' => 'Access Denied'
				);
			}

			$assetItem = array(
				'success' => true,
				'asset_id' => $this->id_asset,
				'details' => array(
					'title' => $assetObject->title,
					'name' => $assetObject->title,
					'description' => $assetObject->description,
					'avatar' => ($onlySrc ? $avatarImg : Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $assetObject->userId), true)),
					'owner' => CoreUser::getForamattedNames($assetObject->userId),
					'owner_channel_url' => Docebo::createLmsUrl('channels/index', array('#' => '/pchannel/' . $assetObject->userId)),
					'uploaded_date' => App7020Helpers::timeAgo($assetObject->created),
					'tags' => $this->_getTags(),
					'status' => $this->_getAssetStatus($assetObject->conversion_status),
					'published' => $assetObject->conversion_status == App7020Assets::CONVERSION_STATUS_APPROVED,
					'unpublished' => $assetObject->conversion_status == App7020Assets::CONVERSION_STATUS_UNPUBLISH,
					'isvisibletothisuser' => $assetObject->isVisibleToUser(),
					'published_by' => $assetObject->published ? CoreUser::getForamattedNames($assetObject->published->idUser, ".", false) : '',
					'published_datetime' => $assetObject->published ? Yii::app()->localtime->toLocalDateTime($assetObject->published->datePublished) : '',
					'published_string' => $publishedString,
					'last_edited' => $lastEdit ? true : false,
					'last_edit_by' => $lastEdit ? CoreUser::getForamattedNames($lastEdit->idUser, ".", false) : '',
					'last_edit_datetime' => $lastEdit ? Yii::app()->localtime->toLocalDateTime($lastEdit->datePublished) : '',
                    'channels' => $this->assetObject->is_private != 1 ? $this->_getChannels() : array(), 
					'rating' => $this->_getRating(),
					'owner_id' => $assetObject->userId,
					'canrate' => App7020Assets::canRate($assetObject->id),
					'duration' => $assetObject->duration,
					'status_bar' => $assetObject->publishingRights(),
					'is_viewed_already' => App7020ContentHistory::isContentViewedByUser($assetObject->id),
					'logged_user' => Yii::app()->user->id
				),
				//'questions' => $this->_getQuestions(),
				//'tooltips' => $this->_getTooltips(),
				'similar' => $similar_videos,
				'similar_total' => $countSimilarVideos,
				//'peer_reviews' => $this->_getAssetPR(),
				'actions' => $this->_getActions(),
				'player' => $this->_getPlayerConfig($assetObject)
			);

			if (!$isMobile) {
				$assetItem['questions'] = $this->_getQuestions();
				$assetItem['tooltips'] = $this->_getTooltips();
				$assetItem['peer_reviews'] = $this->_getAssetPR();
			}


			switch ($assetObject->contentType) {
				case App7020Assets::CONTENT_TYPE_VIDEO:
					$assetItem['details']['type'] = $assetObject->contentType;
					$storage = CS3Storage::getDomainStorage(CS3StorageSt::COLLECTION_7020);
					$assetItem['details']['sources'] = CS3StorageSt::getPlayerSources($assetObject->filename, $storage);
					break;
				case App7020Assets::CONTENT_TYPE_DOC:
					$assetItem['details']['type'] = App7020Assets::CONTENT_TYPE_DOC;
					$assetItem['details']['image'] = $assetObject->getPreviewThumbnailImage(false, 'original');
					break;
				case App7020Assets::CONTENT_TYPE_EXCEL:
					$assetItem['details']['type'] = App7020Assets::CONTENT_TYPE_EXCEL;
					$assetItem['details']['image'] = $assetObject->getPreviewThumbnailImage(false, 'original');
					break;
				case App7020Assets::CONTENT_TYPE_PPT:
					$assetItem['details']['type'] = App7020Assets::CONTENT_TYPE_PPT;
					$assetItem['details']['image'] = $assetObject->getPreviewThumbnailImage(false, 'original');
					break;
				case App7020Assets::CONTENT_TYPE_PDF:
					$assetItem['details']['type'] = App7020Assets::CONTENT_TYPE_PDF;
					$assetItem['details']['image'] = $assetObject->getPreviewThumbnailImage(false, 'original');
					break;
				case App7020Assets::CONTENT_TYPE_TEXT:
					$assetItem['details']['type'] = App7020Assets::CONTENT_TYPE_TEXT;
					$assetItem['details']['image'] = $assetObject->getPreviewThumbnailImage(false, 'original');
					break;
				case App7020Assets::CONTENT_TYPE_IMAGE:
					$assetItem['details']['type'] = App7020Assets::CONTENT_TYPE_IMAGE;
					$assetItem['details']['image'] = $assetObject->getPreviewThumbnailImage(false, 'original');
					break;
				case App7020Assets::CONTENT_TYPE_OTHER:
					$assetItem['details']['type'] = App7020Assets::CONTENT_TYPE_OTHER;
					$assetItem['details']['image'] = $assetObject->getPreviewThumbnailImage(false, 'original');
					break;
				case App7020Assets::CONTENT_TYPE_DEFAULT_OTHER:
					$assetItem['details']['type'] = App7020Assets::CONTENT_TYPE_DEFAULT_OTHER;
					$assetItem['details']['image'] = $assetObject->getPreviewThumbnailImage(false, 'original');
					break;
				case App7020Assets::CONTENT_TYPE_DEFAULT_MUSIC:
					$assetItem['details']['type'] = App7020Assets::CONTENT_TYPE_DEFAULT_MUSIC;
					$assetItem['details']['image'] = $assetObject->getPreviewThumbnailImage(false, 'original');
					break;
				case App7020Assets::CONTENT_TYPE_LINKS:
					$assetItem['details']['url'] = Docebo::createAbsoluteApp7020Url("site/index/", array('#' => '/assets/view/' . $assetObject->id));
					$assetItem['details']['type'] = App7020Assets::CONTENT_TYPE_LINKS;
					$assetItem['details']['image'] = $assetObject->getPreviewThumbnailImage(false, 'original');
					$link = $assetObject->originalFilename;
					if (App7020WebsiteProceeder::isYoutubeLink($link) || App7020WebsiteProceeder::isVimeoLink($link) || App7020WebsiteProceeder::isWistiaLink($link)) {
						$assetItem['details']['video'] = 1;
					}
					break;
			}
			$downloadable = App7020CustomContentSettings::getContentCustomSetting('allow_assets_dowload', $assetObject->id);
			$globalDownload = CoreSetting::get("allow_assets_dowload");
			if ($downloadable == 'on' or $globalDownload == 'on') {
				$assetItem['details']['downloadable'] = 1;
				$assetItem['details']['download_link'] = Docebo::createAbsoluteApp7020Url("knowledgeLibrary/forceDownload/", array('file' => '' . $assetObject->id));
				$assetItem['details']['filename'] = $assetObject->originalFilename;
			}
			$images = array();
			$images = App7020DocumentImages::getDocumentImages($assetObject->id);
			$url = App7020DocumentImages::getDocumentImagesUrl($assetObject->filename);
			foreach ($images as $image) {

				$arrayImages[] = $url . $image['imageName'];
			}
			$assetItem['details']['convertable'] = App7020Assets::isConvertableDocument($assetObject->filename);
			$assetItem['details']['document_images'] = $arrayImages;
			$assetItem['details']['total_views'] = App7020ContentHistory::getContentViews($assetObject->id);
			return $assetItem;
		}
	}

	private function _getPlayerConfig($assetObject) {
		$result = array();

		switch ((int) $assetObject->contentType) {

			/* Lms uploaded Video asset */
			case App7020Assets::CONTENT_TYPE_VIDEO:
			case App7020Assets::CONTENT_TYPE_DEFAULT_MUSIC:
				$flowplayer = new App7020Flowplayer($this->assetObject);
				return array(
					'init' => $flowplayer->get(),
					'containerId' => $flowplayer->containerId,
					'contentId' => $this->assetObject->id,
					'has_cue' => isset($flowplayer->playerOptions['cuepoints']),
					'assets' => $flowplayer->registerFlowplayerScriptsAndCss(),
					'bookmark' => $flowplayer->bookmark,
					'seekable' => $flowplayer->seekable,
					'type' => (string) $this->assetObject->contentType,
					'conf' => array(
						'swf' => Flowplayer::getFlowplayerJSUrl() . "flowplayer.swf",
						'swfHls' => Flowplayer::getFlowplayerJSUrl() . "flowplayerhls.swf"
					)
				);
				break;

			/* Lms uploaded Image asset */
			case App7020Assets::CONTENT_TYPE_IMAGE:
				//if user is not the contributor add record to database
				if ($assetObject->userId != Yii::app()->user->id) {
					App7020Assets::viewContent($assetObject->id);
				}
				$result = array(
					'id' => $assetObject->id,
					'type' => $assetObject->contentType,
					'title' => $assetObject->originalFilename,
					'image' => $assetObject->getPreviewThumbnailImage(false, 'original')
				);
				break;
			/* Google Drive Uploaded */
			case App7020Assets::CONTENT_TYPE_GOOGLE_DRIVE_DOCS:
			case App7020Assets::CONTENT_TYPE_GOOGLE_DRIVE_SHEETS:
			case App7020Assets::CONTENT_TYPE_GOOGLE_DRIVE_SLIDES:
				//if user is not the contributor add record to database
				if ($assetObject->userId != Yii::app()->user->id) {
					App7020Assets::viewContent($assetObject->id);
				}
				$result = array(
					"status" => App7020Helpers::curl($assetObject->originalFilename)['http_code'],
					"contentId" => $assetObject->id,
					'id' => $assetObject->id,
					'type' => 'google_drive_file',
					'title' => $assetObject->originalFilename,
					'image' => $assetObject->getPreviewThumbnailImage(false, 'original')
				);
				break;

			/* Youtube | Vimeo | Wistia | Share link */
			case App7020Assets::CONTENT_TYPE_LINKS:
				if ($youtube = App7020WebsiteProceeder::isYoutubeLink($assetObject->originalFilename)) {
					$result = array(
						'id' => $assetObject->id,
						'type' => 'youtube',
						'hash' => $youtube
					);
				} elseif ($vimeo = App7020WebsiteProceeder::isVimeoLink($assetObject->originalFilename)) {
					$result = array(
						'contentId' => $assetObject->id,
						'cuepoint_view' => App7020Assets::VIDEO_VIEW_PERCENTAGE,
						'type' => 'vimeo',
						'hash' => $vimeo
					);
				} elseif ($wistia = App7020WebsiteProceeder::isValidWistiaLink($assetObject->originalFilename, true)) {

					$result = array(
						'contentId' => $assetObject->id,
						'cuepoint_view' => App7020Assets::VIDEO_VIEW_PERCENTAGE,
						'type' => 'wistia',
						'hash' => $wistia
					);
				} else {
//					//if user is not the contributor add record to database
//					if ($assetObject->userId != Yii::app()->user->id) {
//						App7020Assets::viewContent($assetObject->id);
//					}
					$result = array(
						'id' => $assetObject->id,
						'type' => $assetObject->contentType,
						'title' => $assetObject->title,
						'description' => strip_tags($assetObject->description),
						'filename' => $assetObject->originalFilename,
						'image' => $assetObject->getPreviewThumbnailImage(false, 'original')
					);
				}
				break;

			/* All convertable types files or all other types of file for download */
			default:
				if (App7020Assets::isConvertableDocument($assetObject->filename)) {
					$result = array(
						'id' => $assetObject->id,
						'type' => 'convertable',
						'title' => $assetObject->originalFilename,
						'page' => App7020DocumentTracking::getLastViewedPage($assetObject->id, Yii::App()->user->idst)['Page'],
						'pagecurr' => App7020DocumentTracking::getLastViewedPage($assetObject->id, Yii::App()->user->idst)['Page'] + 1,
						'url' => App7020DocumentImages::getDocumentImagesUrl($assetObject->filename),
						'items' => App7020DocumentImages::getDocumentImages($assetObject->id)
					);
					//if document pages are 1 add view in content_history
					if (count($result['items']) == 1 && $assetObject->userId != Yii::app()->user->idst) {
						App7020Assets::viewContent($assetObject->id);
					}
				} else {
					$result = array(
						'id' => $assetObject->id,
						'type' => 'download',
						'icon' => App7020Assets::$contentTypes[$assetObject->contentType]['icon'],
						'filename' => $assetObject->originalFilename,
						'url' => Docebo::createAbsoluteApp7020Url('knowledgeLibrary/forceDownload', array('file' => $assetObject->id)),
					);
				}
				break;
		}
		return $result;
	}

	private function _getRating() {
		$assetRating = App7020ContentRating::model()->calculateContentRating($this->id_asset);
		return $assetRating;
	}

	private function _getAssetStatus($statusId) {
		switch ($statusId) {
			case App7020Assets::CONVERSION_STATUS_FINISHED:
			case App7020Assets::CONVERSION_STATUS_INREVIEW:
				return Yii::t("app7020", "In review");
				break;
			case App7020Assets::CONVERSION_STATUS_UNPUBLISH:
				return Yii::t("app7020", "Unpublished");
				break;
			case App7020Assets::CONVERSION_STATUS_APPROVED:
				return Yii::t("app7020", "Published");
				break;
			default:
				return Yii::t("app7020", "Invalid status");
				break;
		}
	}

	private function _checkAccess() {
        $experts = [];
		if ($this->assetObject->conversion_status < App7020Assets::CONVERSION_STATUS_FINISHED) {
			return false;
        } elseif (!in_array(Yii::app()->user->idst, App7020Assets::getUsersWhichCanViewAnAsset($this->assetObject->id, true)) && $this->assetObject->conversion_status < App7020Assets::CONVERSION_STATUS_APPROVED) {
			return false;
        } elseif (!$this->assetObject->isVisibleToUser()) {
            return false;
        } else {
            return true;
        }

	}

	private function _getTags() {
		$tagsArray = array();
		if ($this->assetObject->tags) {
			foreach ($this->assetObject->tags as $value) {
				$tagsArray[] = App7020Tag::model()->findByPk($value->idTag)->tagText;
			}
		}
		return $tagsArray;
	}

	private function _getQuestions() {
		try {
			$restData = RestData::factory(RestData::ENDPOINT_ALL_QUESTIONS, array(
						'idAsset' => $this->id_asset
			));
			$result = $restData->run();

			return $result['section'];
		} catch (Exception $ex) {
			throw new Exception($ex->getMessage());
		}
	}

	private function _getTooltips() {
		$tArray = array();
		if ($this->assetObject->tooltips) {
			foreach ($this->assetObject->tooltips as $value) {
				$tArray[] = array(
					'tooltipStyle' => App7020Tooltips::$_themes[$value->tooltipStyle],
					'avatar_owner' => Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $value->idExpert), true),
					'id' => $value->id,
					'from' => $value->durationFrom,
					'to' => $value->durationTo,
					'tooltip_text' => $value->text
				);
			}
		}
		return $tArray;
	}

	private function _getActions() {
		$actions = array();



		if ($this->assetObject->contentType != App7020Assets::CONTENT_TYPE_LINKS && !in_array($this->assetObject->contentType,  App7020Assets::googleDriveTypes())) {
			if (App7020CustomContentSettings::getContentCustomSetting('allow_assets_dowload', $this->assetObject->id)=='on') {

				$actions[] = array(
					'action_type' => 'link',
					'action_uid' => self::ACTION_UID_DOWNLOAD,
					'action_params' => array(
						'url' => Docebo::createAbsoluteApp7020Url('knowledgeLibrary/forceDownload', array(
							'file' => $this->id_asset
						)),
						'asset_id' => $this->assetObject->id
					),
					'tooltip_text' => Yii::t('app7020', 'Download asset'),
					'tooltip_color' => '#333333'
				);
			}
		}

		$actions[] = array(
			'action_type' => 'modal',
			'action_uid' => self::ACTION_UID_INVITE,
			'action_params' => array(
				'url' => 'javascript::',
				'asset_id' => $this->assetObject->id
			),
			'tooltip_text' => Yii::t('app7020', 'Invite people to watch'),
			'tooltip_color' => '#333333'
		);

		return $actions;
	}

	/**
	 * fire the endpoint for getting simillar videos
	 * @return array
	 */
	private function _getSimilarAssets() {
		$restData = RestData::factory(RestData::ENDPOINT_ASSET_SIMILAR, array(
					'id_asset' => $this->id_asset,
					'hasFailback' => $this->hasFailback
		));
		$result = $restData->run();
		return $result;
	}

	/**
	 * fire the endpoint for getting experts' peer reviews 
	 * @return array
	 */
	private function _getAssetPR() {
		$restData = RestData::factory(RestData::ENDPOINT_ASSET_PR, array(
					'id_asset' => $this->id_asset
		));
		$result = $restData->run();
		return $result;
	}

	private function _getChannels() {
		$channelsArray = array();
		$isAdmin = Yii::app()->user->isGodAdmin;
		$conf_extr_channels = array(
			'idUser' => Yii::app()->user->idst,
			'idAsset' => $this->assetObject->id,
            "appendEnabledEffect" => true,
			'return' => 'data'
		);
		$channels = App7020Channels::extractUserChannels($conf_extr_channels, false);
//		if ($isAdmin && $this->assetObject->asset_channels) {
//			foreach ($this->assetObject->asset_channels as $value) {
//				$channelTranslation = App7020Channels::model()->findByPk($value->idChannel)->translation($this->lang_code);
//				$channelsArray[] = array(
//					'channel_name' => $channelTranslation['name'],
//					'channel_url' => Docebo::createLmsUrl('channels/index', array('#' => '/channel/' . $value->idChannel)),
//					'idChannel' => $value->idChannel
//				);
//			}
//		} else {
			if (!empty($channels)) {
				foreach ($channels as $value) {
					$channelTranslation = App7020Channels::model()->findByPk($value['id'])->translation($this->lang_code);
					$channelsArray[] = array(
						'channel_name' => $channelTranslation['name'],
						'channel_url' => Docebo::createLmsUrl('channels/index', array('#' => '/channel/' . $value['id'])),
						'idChannel' => $value['id']
					);
				}
			}
//            else{
//                $channelsArray[] = [
//                    'channel_name'=> 'No channels',
//                ];
//            }
//		}
		return $channelsArray;
	}

	public function getUsersDataFromArray($arrayUsers = array()) {
		$users = array();
		$languages = array();
		foreach ($arrayUsers as $userModel) {

			$users[] = $userModel['idst'];
		}

		return $users;
	}

}
