<?php

/**
 * 
 * @property integer $id_asset
 * @property array $item
 * @author Ivaylo Ivanov
 */
class AssetCrud extends RestData {

	/**
	 * Constructor
	 *
	 * @param array $params
	 */
	public function __construct($params = false) {
		parent::__construct($params);
	}

	/**
	 * Execute operation
	 */
	public function run() {
		if ($this->endpoint == RestData::ENDPOINT_ASSET_ADDED) {
			if ($this->is_link) {
				return $this->_addLink();
			} else {
				return $this->_addFiles();
			}
		}

		if ($this->endpoint == RestData::ENDPOINT_ASSET_START_UPLOAD) {
			return $this->_startUpload();
		}

		if ($this->endpoint == RestData::ENDPOINT_ASSET_UPLOADED) {
			return $this->_assetUploaded();
		}

		if ($this->endpoint == RestData::ENDPOINT_ASSET_FAILED_UPLOAD) {
			return $this->_assetFailedUpload();
		}

		if ($this->endpoint == RestData::ENDPOINT_ASSET_GET_ACTIVE) {
			return $this->_assetGetActive();
		}

		if ($this->endpoint == RestData::ENDPOINT_ASSET_SUBMITTED) {
			return $this->_assetSubmitted();
		}

		if ($this->endpoint == RestData::ENDPOINT_ASSET_DELETED) {
			return $this->_deleteAsset();
		}

		if ($this->endpoint == RestData::ENDPOINT_ASSET_PUBLISHED) {
			return $this->_publishAsset();
		}

		if ($this->endpoint == RestData::ENDPOINT_ASSET_UNPUBLISHED) {
			return $this->_unpublishAsset();
		}

		if ($this->endpoint == RestData::ENDPOINT_ASSET_DISMISS) {
			return $this->_dismissAsset();
		}

		if ($this->endpoint == RestData::ENDPOINT_ASSET_UNDO_DISMISS) {
			return $this->_undoDismissAsset();
		}

		if ($this->endpoint == RestData::ENDPOINT_ASSET_VIEW) {
			return $this->_viewAsset();
		}

		if ($this->endpoint == RestData::ENDPOINT_SPENT_TIME) {
			return $this->_spentTime();
		}
	}

	private function _addFiles() {
		$files = $this->data;
		$objects = array();
		foreach ($files as $key => $file) {
			$extension = App7020Helpers::getExtentionByFilename($file->name);
			$fileType = App7020Assets::getContentCodeByExt(strtolower($extension));

			if (!$fileType) {
				$objects[$key] = array(
					'status' => false,
					'error' => Yii::t("app7020", "File type is not allowed for upload") . " - " . $fileType
				);
			} else {
				$filename = $file->id . "." . $extension;

				$assetObject = new App7020Assets();
				$assetObject->conversion_status = App7020Assets::CONVERSION_STATUS_PENDING;
				$assetObject->originalFilename = $file->name;
				$assetObject->filename = $filename;
				$assetObject->contentType = $fileType;
				$assetObject->idSource = 1;
				$assetObject->userId = Yii::app()->user->idst;
				$assetObject->save(false);

				$fileStorageObject = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);
				$fileStorageObject->setKey($assetObject->filename);
				$outputPath = ($fileType == App7020Assets::CONTENT_TYPE_VIDEO) ? $fileStorageObject->getKey() : $fileStorageObject->getOutputKey();

				if ($assetObject->id) {
					$this->_proceedRequest($assetObject->id);
					$assetObject->refresh();
					$objects[$key]['asset'] = (array) $assetObject->getAttributes();
					$objects[$key]['status'] = true;
					$objects[$key]['asset'] ['outputPath'] = ltrim($outputPath, "/");
				} else {
					$objects[$key]['error'] = Yii::t("app7020", "Cannot add the selected asset") . " - " . $file->name;
					$objects[$key]['status'] = false;
				}
			}
		}
		return array(
			'result' => !empty($objects) ? true : false,
			'assets' => $objects
		);
	}

	private function _addLink() {
		$sharedLink = $this->data;
		$objects = array();
		if ($sharedLink = App7020Helpers::isValidUrl($sharedLink)) {
			$assetObject = new App7020Assets();
			$assetObject->title = '';
			$assetObject->description = '';
			$assetObject->filename = md5($sharedLink . time() . Yii::app()->user->idst);
			$assetObject->originalFilename = $sharedLink;
			$assetObject->contentType = App7020Assets::CONTENT_TYPE_LINKS;
			$assetObject->idSource = 1;
			$assetObject->conversion_status = App7020Assets::CONVERSION_STATUS_INPROGRESS;
			$assetObject->userId = Yii::app()->user->idst;
			$assetObject->duration = 0;
			$assetObject->save(false);

			if ($assetObject->id) {
				$this->_proceedRequest($assetObject->id);
				App7020WebsiteProceeder::saveWebsiteInitData($assetObject->id);
				$objects[0]['asset'] = (array) $assetObject->getAttributes();
				$objects[0]['status'] = true;
			} else {
				$objects[0]['error'] = Yii::t("app7020", "Cannot add the selected link") . " - " . $sharedLink;
				$objects[0]['status'] = false;
			}
		} else {
			$objects[0]['error'] = Yii::t("app7020", "Invalid Link") . " - " . $sharedLink;
			$objects[0]['status'] = false;
		}
	}

	/**
	 * 
	 * @param type $assetId
	 */
	private function _proceedRequest($assetId) {
		$idQuestion = $this->id_question;

		$requestModel = App7020QuestionRequest::model()->findByAttributes(array('idQuestion' => $idQuestion));
		if ($requestModel->id) {

			$requestModel->idContent = $assetId;
			$requestModel->save();

			$questionObject = App7020Question::model()->findByPk($idQuestion);
			if ($questionObject->id) {
				$questionObject->setScenario(App7020Question::SCENARIO_OPEN_CLOSE);
				$questionObject->idContent = $assetId;
				$questionObject->save(false);
			}
		}
	}

	private function _startUpload() {
		$idAsset = $this->id_asset;
		$assetObject = App7020Assets::model()->findByPk($idAsset);
		if ($assetObject->id) {
			$assetObject->conversion_status = App7020Assets::CONVERSION_STATUS_INPROGRESS;
			$assetObject->save(false);
			$fileStorageObject = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);
			$fileStorageObject->setKey($assetObject->filename);
			$outputPath = ($assetObject->contentType == App7020Assets::CONTENT_TYPE_VIDEO) ? $fileStorageObject->getKey() : $fileStorageObject->getOutputKey();
			return array(
				'result' => true,
				'outputPath' => ltrim($outputPath, "/"),
				'assetId' => $assetObject->id
			);
		} else {
			return array(
				'result' => false,
				'error' => Yii::t("app7020", "Cannot upload the selected asset"),
				'assetId' => $idAsset
			);
		}
	}

	private function _assetUploaded() {
		$idAsset = $this->id_asset;
		$assetObject = App7020Assets::model()->findByPk($idAsset);
		if ($assetObject->id) {
			$assetObject->conversion_status = App7020Assets::CONVERSION_STATUS_S3;
			$assetObject->startProccessingAfterFileUploaded();
			$assetObject->save(false);
			return array(
				'result' => true,
				'assetId' => $assetObject->id
			);
		} else {
			return array(
				'result' => false,
				'error' => Yii::t("app7020", "Failed uploading the selected asset"),
				'assetId' => $idAsset
			);
		}
	}

	private function _assetFailedUpload() {
		$idAsset = $this->id_asset;
		$assetObject = App7020Assets::model()->findByPk($idAsset);

		if ($assetObject->id) {
			$assetObject->conversion_status = App7020Assets::CONVERSION_STATUS_SNS_ERROR;
			$assetObject->save(false);
			return array(
				'result' => true,
				'assetId' => $assetObject->id
			);
		} else {
			return array(
				'result' => false,
				'error' => Yii::t("app7020", "Failed uploading the selected asset"),
				'assetId' => $idAsset
			);
		}
	}

	private function _assetGetActive() {

		$activeAssetsConversionStatuses = array(0, 1, 3, 6, 7, 8);
		$assets = App7020Assets::getUploadedFiles(array('proccessing_criteria' => $activeAssetsConversionStatuses));
		return $assets;
	}

	public function _assetSubmitted() {
		$assetId = $this->id_asset;
		$assetDetails = $this->App7020Assets;
		$tagsToAssign = $this->hiddenTags;
		$activeThumbnail = $this->thumbs;
		$assetDownload = $this->assetDownload;
		$channnelsCheckList = $this->channnelsCheckList;
		$assetObject = App7020Assets::model()->findByPk($assetId);

		//save the 'allow_asstes_download' custom setting if the option is different from the global setting
		$assetDownload = !empty($assetDownload) ? 'on' : 'off';



		if (empty($assetId) || empty($assetObject)) {
			return array(
				'res' => false,
				'errors' => array(
					Yii::t('app7020', 'The asset was not found!')
				)
			);
		}

		$assetObject->setAttribute("tagsToAssign", $tagsToAssign);
		$assetObject->setAttribute("activeThumbnail", $activeThumbnail);
		$assetObject->setAttribute("title", $assetDetails['title']);
		$assetObject->setAttribute("description", $assetDetails['description']);

		if ($assetObject->isSuperAdminAccess() && $assetObject->conversion_status >= App7020Assets::CONVERSION_STATUS_FINISHED) {
			$assetObject->setAttribute("is_private", ShareApp7020Settings::isGlobalAllowPrivateAssets() == false ? App7020Assets::PRIVATE_STATUS_INIT : $assetDetails['is_private']);
			$assetObject->setAttribute("relatedChannels", $channnelsCheckList);
		} elseif ($assetObject->conversion_status < App7020Assets::CONVERSION_STATUS_FINISHED) {
			$assetObject->setAttribute("is_private", ShareApp7020Settings::isGlobalAllowPrivateAssets() == false ? App7020Assets::PRIVATE_STATUS_INIT : $assetDetails['is_private']);
			$assetObject->setAttribute("relatedChannels", $channnelsCheckList);
		}


		//if is checked the allow download for any asset
		if ($assetDownload != Settings::get(App7020Assets::SETTING_ALLOW_DOWNLOAD)) {
			$customSettingObject = new App7020CustomContentSettings();
			$customSettingObject->idContent = $assetId;
			$customSettingObject->settingName = App7020Assets::SETTING_ALLOW_DOWNLOAD;
			$customSettingObject->settingValue = $assetDownload;
			$customSettingObject->save();
		}
		if ($assetObject->validate()) {
			//if validation is ok then will going to save methods
			if ($assetObject->conversion_status == App7020Assets::CONVERSION_STATUS_SNS_SUCCESS) {
				$assetObject->conversion_status = (int) $assetObject->is_private == App7020Assets::PRIVATE_STATUS_PRIVATE ? App7020Assets::CONVERSION_STATUS_APPROVED : App7020Assets::getFinalConversionStatus($channnelsCheckList);
				Yii::app()->event->raise('App7020NewContent', new DEvent($assetObject, array('contentId' => $assetObject->id)));
			}
			$save = $assetObject->save();
			return array(
				'res' => true,
				'assetId' => $assetObject->id,
				'assetStatus' => $assetObject->conversion_status,
				'assetThumb' => App7020Assets::getAssetThumbnailUri($assetObject->id),
				'assetOriginalName' => $assetObject->originalFilename,
				'message' => Yii::t('app7020', 'The asset was updated successfull!')
			);
		}

		return array(
			'res' => false,
			'errors' => App7020Helpers::convertErrorsToFlatArray($assetObject->getErrors())
		);
	}

	private function _deleteAsset() {
		$local = $this->local;
		$idAsset = $this->id_asset;
		$assetObject = App7020Assets::model()->findByPk($idAsset);

		$output = array();
		if (!empty($assetObject) && App7020Assets::canUserDeleteContent($idAsset)) {
			if ($local == 'true') {
				$output['status'] = $assetObject->delete();
			} else {
				CoreAsset::model()->deleteByPk($assetObject->idThumbnail);
				App7020Helpers::deleteCloudFile($assetObject);
				App7020Helpers::deleteLocalFile($assetObject);
				$output['status'] = $assetObject->delete();
			}
		} else {
			$output['status'] = false;
		}

		return $output;
	}

	/**
	 * Call to publish an asset 
	 * @return boolean
	 */
	private function _publishAsset() {
		$contentObject = App7020Assets::model()->findByPk($this->id_asset);

		if (
			($contentObject->hasReviewsAccess() || Yii::app()->user->isGodAdmin || $contentObject->userId == Yii::app()->user->idst) &&
			(int) $contentObject->conversion_status >= App7020Assets::CONVERSION_STATUS_FINISHED &&
			(int) $contentObject->conversion_status < App7020Assets::CONVERSION_STATUS_APPROVED
		) {
			$contentObject->conversion_status = App7020Assets::CONVERSION_STATUS_APPROVED;

			$requestModel = App7020QuestionRequest::model()->findByAttributes(array('idContent' => $this->id_asset, 'autoInvite' => 1));
			if ($requestModel->id) {
				$questionModel = App7020Question::model()->findByPk($requestModel->idQuestion);
				$invitedUsers = array($questionModel->idUser);
				foreach ($invitedUsers as $value) {
					$invitationObject = new App7020Invitations();
					$invitationObject->idInvited = $value;
					$invitationObject->idContent = $this->id_asset;
					$invitationObject->save();
				}
				Yii::app()->event->raise('App7020Invitation', new DEvent($this, array('users' => $invitedUsers,
					'contentId' => $this->id_asset)));
			}


			$contentObject->save(false);
			$lastEdit = App7020ContentPublished::getLastEdited($contentObject->id);
			$output = array(
				'status' => true,
				'published' => $contentObject->conversion_status == App7020Assets::CONVERSION_STATUS_APPROVED,
				'published_by' => $contentObject->published ? CoreUser::getForamattedNames($contentObject->published->idUser, ".", false) : '',
				'published_datetime' => $contentObject->published ? $contentObject->published->datePublished : '',
				'last_edited' => $lastEdit ? true : false,
				'last_edit_by' => $lastEdit ? CoreUser::getForamattedNames($lastEdit->idUser, ".", false) : '',
				'last_edit_datetime' => $lastEdit ? $lastEdit->datePublished : '',
			);
			Yii::app()->event->raise('App7020Publishing', new DEvent($this, array('contentId' => $this->id_asset, 'idPublisher' => Yii::app()->user->idst)));
		} else {
			$output['status'] = false;
		}
		return $output;
	}

	/**
	 * Unpublishing the asset
	 * @return boolean
	 */
	private function _unpublishAsset() {
		$contentObject = App7020Assets::model()->findByPk($this->id_asset);
		if (
			($contentObject->hasReviewsAccess() || Yii::app()->user->isGodAdmin || $contentObject->userId == Yii::app()->user->idst) && $contentObject->conversion_status == App7020Assets::CONVERSION_STATUS_APPROVED
		) {
			if ($contentObject->is_private != 1) {
				//here check for visibility rules
				$channel_list = App7020ChannelAssets::getChannelsByContent($contentObject->id);
				if (App7020Channels::checkChannelsForPeerReviewReqirement($channel_list)) {
					$contentObject->conversion_status = App7020Assets::CONVERSION_STATUS_FINISHED;
				} else {
					$contentObject->conversion_status = App7020Assets::CONVERSION_STATUS_UNPUBLISH;
				}
			}else{
				$contentObject->conversion_status = App7020Assets::CONVERSION_STATUS_UNPUBLISH;
			}
			$publishedBy = $contentObject->published ? CoreUser::getForamattedNames($contentObject->published->idUser, ".", false) : '';
			$publishedDateTime = $contentObject->published ? $contentObject->published->datePublished : '';

			$publishedObject = App7020ContentPublished::model()->findByAttributes(array('idContent' => $this->id_asset, 'actionType' => App7020ContentPublished::ACTION_TYPE_PUBLISH));
			if ($publishedObject) {
				$publishedObject->delete();
			}

			$contentObject->save(false);
			$lastEdit = App7020ContentPublished::getLastEdited($contentObject->id);
			$output = array(
				'status' => true,
				'published' => $contentObject->conversion_status == App7020Assets::CONVERSION_STATUS_APPROVED,
				'published_by' => $publishedBy,
				'published_datetime' => $publishedDateTime,
				'last_edited' => $lastEdit ? true : false,
				'last_edit_by' => $lastEdit ? CoreUser::getForamattedNames($lastEdit->idUser, ".", false) : '',
				'last_edit_datetime' => $lastEdit ? $lastEdit->datePublished : '',
			);
		} else {
			$output['status'] = false;
		}
		return $output;
	}

	/**
	 * We dismiss an asset with that call
	 * @return boolean
	 */
	private function _dismissAsset() {
		$dismiss = new App7020DismissHistory();
		$dismiss->idUser = $this->id_user;
		$dismiss->idContent = $this->id_channel;
		$dismiss->contentType = $this->from;
		$dismiss->save();
		return true;
	}

	/**
	 * 
	 * @return boolean
	 */
	private function _undoDismissAsset() {
		$undo_dismiss = App7020DismissHistory::model()->findByAttributes(array('idUser' => $this->id_user, 'idContent' => $this->id_channel, 'contentType' => $this->from));
		if ($undo_dismiss) {
			$undo_dismiss->delete();
		}
		return true;
	}

	/**
	 * check views for an asset 
	 * @return integer
	 */
	private function _viewAsset() {
		$idAsset = $this->id_asset;
		$assetObject = App7020Assets::model()->findByPk($idAsset);
 		if (Yii::app()->user->id != $assetObject->userId) {
			App7020Assets::viewContent($idAsset);
		}
		return count($assetObject->viewes);
	}


	private function _spentTime(){
		$idAsset = $this->id_asset;
		$time = $this->time;
		$user = Yii::app()->user->idst;
		$response['success'] = false;
	 
		$obj = App7020SpentTimeHistory::model()->findByAttributes(array('idUser'=>$user,'idContent'=>$idAsset));
		if($obj){
			$old_time = $obj->spentTime;
			$new_time = $time + $old_time;
			$obj->spentTime = $new_time;
			$obj->dateUpdated = Yii::app()->localtime->getUTCNow();
			$obj->save();
			if($obj->save()){
				$response['success'] = true;
			}
		} else {
			$new_obj = new App7020SpentTimeHistory();
			$new_obj->idContent = $idAsset;
			$new_obj->idUser = $user;
			$new_obj->dateCreated = Yii::app()->localtime->getUTCNow();
			$new_obj->dateUpdated = Yii::app()->localtime->getUTCNow();
			$new_obj->spentTime = $time;
			$new_obj->save();
			if($new_obj->save()){
				$response['success'] = true;
			}
		}

		return $response['success'];
	}

}
