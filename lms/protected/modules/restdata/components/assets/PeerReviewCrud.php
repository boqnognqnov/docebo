<?php

/**
 * 
 * @property integer $id_asset
 * @property array $item
 * @author Ivaylo Ivanov
 */
class PeerReviewCrud extends RestData {

	/**
	 * Constructor
	 *
	 * @param array $params
	 */
	public function __construct($params = false) {
		parent::__construct($params);
	}

	/**
	 * Execute operation
	 */
	public function run() {
		if ($this->endpoint == RestData::ENDPOINT_PR_ADDED) {
			return $this->_addPR();
		}

		if ($this->endpoint == RestData::ENDPOINT_PR_UPDATED) {
			return $this->_updatePR();
		}

		if ($this->endpoint == RestData::ENDPOINT_PR_DELETED) {
			return $this->_deletePR();
		}

	}

	private function _addPR() {
		$data['idContent'] = $this->id_asset;
		$data['message'] = $this->message;
		$data['collapsed'] = false;
		$tmp = $this->idTooltip;
		$data['idTooltip'] = empty($tmp) ? null : $this->idTooltip;
		$tmp = $this->systemMessage;
		$data['systemMessage'] = empty($tmp) ? 0 : $this->systemMessage;
		$data['idUser'] = Yii::app()->user->idst;

		if (empty($data['idContent'])) {
			return array('reviewId' => 0);
		}

		$assetObject = App7020Assets::model()->findByPk($data['idContent']);

		if (empty($assetObject->id) && $assetObject->defineContentAdminAccess() == false) {
			return array('reviewId' => 0);
		}

		$reviewObject = new App7020ContentReview();
		$reviewObject->setAttributes($data);
		$reviewObject->save(false);
		if ($reviewObject->id) {
			Yii::app()->event->raise('App7020NewPeerReview', new DEvent($this, array('prId' => $reviewObject->id)));
			return array('reviewId' => (int) $reviewObject->id);
		} else {
			return array('reviewId' => 0);
		}
	}

	/**
	 * 
	 * @param type $assetId
	 */
	private function _updatePR() {
		$id = $this->id;
		$message = $this->message;

		$reviewObject = App7020ContentReview::model()->findByPk($id);

		if (empty($reviewObject->id)) {
			return array('result' => 0);
		}

		$assetObject = App7020Assets::model()->findByPk($reviewObject->idContent);

		if (empty($assetObject->id) && $assetObject->defineContentAdminAccess() == false) {
			return array('reviewId' => 0);
		}
		
		$reviewObject->message = $message;

		return array('result' => $reviewObject->update());
	}

	private function _deletePR() {
		$id = $this->id;

		$tmp = intval($id);
		if (empty($tmp)) {
			return array('result' => false);
		}
		$reviewObject = App7020ContentReview::model()->findByPk($id);
		
		if (empty($reviewObject->id)) {
			return array('result' => 0);
		}

		$assetObject = App7020Assets::model()->findByPk($reviewObject->idContent);

		if (empty($assetObject->id) && $assetObject->defineContentAdminAccess() == false) {
			return array('reviewId' => 0);
		}
		
		return array('result' => (bool) App7020ContentReview::model()->deleteByPk($id));
	}

}
