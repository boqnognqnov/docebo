<?php

/**
 * 
 * @property integer $userId
 * @property integer $id_asset
 * @property integer $rating
 * @author Trayan Stoyanov
 */
class RateAsset extends RestData {
	/**
	 * Constructor 
	 *
	 * @param array $params
	 */
	public function __construct($params = false) {
		parent::__construct($params);
	}
	public function run() {
		$oldRating = App7020ContentRating::model()->findByAttributes(array('idContent'=>$this->id_asset,'idUser'=>$this->userId));
		if($oldRating) {
			$oldRating->delete();
		}
		$rating = new App7020ContentRating(); 
		$rating->idContent = $this->id_asset;
		$rating->idUser = $this->userId;
		$rating->rating = $this->rating+1;
		if($rating->save()) {
			$newRating = App7020ContentRating::calculateContentRating($this->id_asset,1);
			if($rating->rating == 5) { // Raise badge event, if the asset is rated with 5 stars.
				$contentOwner = App7020Assets::model()->findByPk($this->id_asset);
				Yii::app()->event->raise('AssetReachedAGoal', new DEvent($rating, array('userId'=>  $contentOwner->userId,'contentId' => $this->id_asset , 'goal'=>0)));
			}
		}
		
		return $newRating;
	}
}