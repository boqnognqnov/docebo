<?php

class SplashScreen extends RestData {

	/**
	 * Constructor
	 *
	 * @param string $params
	 */
	public function __construct($params = false) {
		parent::__construct($params);
	}

	public function run() {
		if($this->action == 'check'){
			return $this->_check();
		}

		if($this->action == 'gotit'){
			return $this->_gotit();
		}
	}


	private function _check(){

	}

	private function _gotit(){

	}
}