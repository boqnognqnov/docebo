<?php
/**
 * 
 *
 */
class RestdataModule extends CWebModule
{
	
	/**
	 * Module initialization
	 */
	public function init() {

		// import the module-level models and components
		$this->setImport(array(
			'restdata.components.*',
			'restdata.components.channels.*',
			'restdata.components.questions.*',
			'restdata.components.activity.*',
			'restdata.components.tooltips.*',
			'restdata.components.splashscreen.*',
			'restdata.components.user.*',

		));
	}
	

}
