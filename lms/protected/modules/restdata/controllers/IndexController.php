<?php

/**
 * 
 *
 */
class IndexController extends Controller {

	public function init() {
		parent::init();
		$this->defaultAction = "index";
	}

	/**
	 * Access filters
	 * @return array
	 */
	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}

	/**
	 * Access rules for this controller
	 * @return array
	 */
	public function accessRules() {
		$res = array();

		// Allow access only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);
		return $res;
	}

	/**
	 * Implements AJAX data feed for channels dashboard
	 *
	 * @param $endpoint It can be "channels" or "channel_items"
	 */
	public function actionIndex() {
		$input = RestData::resolveRestInput();
		$endpoint = $input["endpoint"];
		$params = $input["params"];
		$result = array("success" => true);

		try {
			$handler = RestData::factory($endpoint, $params);
			$result = $handler->run();
			
		} catch (Exception $e) {
			Yii::log("REST Exception:", CLogger::LEVEL_ERROR);
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			$result["success"] = false;
			$result["message"] = $e->getMessage();
		}
 		echo json_encode($result);
	}

}
