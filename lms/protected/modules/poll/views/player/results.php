<div class="l-testpoll review">
	<h1 class="page-title">
		<?php echo $this->poll->title ?>
	</h1>
	<div class="row-fluid">
		<div class="span6">
			<div>
				<img src="<?php echo $this->getPlayerAssetsUrl() . '/images/testpoll-img.png'; ?>"/>
			</div>
		</div>

		<div class="span6">
			<br/>
			<p class="text-center">
				<?php echo Yii::t('poll', '_POLL_COMPLETED'); ?>
			</p>
			<div>
				<?php
				echo '<br/><div class="text-center">'
					. '<a href="' . $backUrl . '" class="btn-docebo green verybig">' . Yii::t('test', '_TEST_SAVEKEEP_BACK') . '</a>'
					. '</div>';
				?>
			</div>
		</div>

	</div>

</div>