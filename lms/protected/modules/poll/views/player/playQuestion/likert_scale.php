<?php

$checked = array();
$userAnswer = $questionManager->recoverUserAnswer($idQuestion, Yii::app()->user->id);
foreach ($userAnswer as $answer) {
    $checked[$answer['id_answer']][$answer['more_info']] = true;
}

$likertScales = $questionManager->getPollScales($id_poll);
$scaleData = array();
foreach($likertScales as $scale){
    $scaleData[$scale->id] = '';
}

$index = 0;
?>

<?php if (is_array($answers)) : ?>
    <table class="playLikertScale">
        <thead>
            <tr>
                <th></th>
                <?php
                    foreach($likertScales as $scale){
                        echo '<th class="scaleCheckboxHeader">' . $scale->title . '</th>';
                    }
                ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($answers as $answer) : ?>
                <tr>
                   <td> <?= $answer['answer'] ?> </td>
                   <?php
                        $index++;
                        foreach($likertScales as $scale){
                            $name =  $questionManager->getInputName($idQuestion). ('['.$answer['id_answer'].']');
                            $idFor =  $questionManager->getInputId($idQuestion). '-' . $index . '-'. $scale->id;
                            echo '<td class="scaleCheckBox">';
                            echo CHtml::radioButton($name, isset($checked[$answer['id_answer']][$scale->id]), array('id' => $idFor, 'value' => $scale->id));
                            echo '</td>';
                        }
                   ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>

<script>
    $('table.playLikertScale input').styler();
</script>
