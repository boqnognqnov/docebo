<?php

$answer = $questionManager->recoverUserAnswer($idQuestion, Yii::app()->user->id);

echo CHtml::textArea(
	$questionManager->getInputName($idQuestion), 
	(!empty($answer) && isset($answer[0]) ? $answer[0]->more_info : ""),
	array(
		'id' => $questionManager->getInputId($idQuestion),
		//'class' => ''
	)
);

?>
