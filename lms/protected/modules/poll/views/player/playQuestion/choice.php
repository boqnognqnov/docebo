<?php

$checked = array();
$userAnswer = $questionManager->recoverUserAnswer($idQuestion, Yii::app()->user->id);
foreach ($userAnswer as $answer) {
	$checked[] = $answer->id_answer;
}

$index = 0;
if (is_array($answers)) {
	foreach ($answers as $answer) {
		$index++;
		$idFor = $questionManager->getInputId($idQuestion).'-'.$index;
		$idAnswer = $answer['id_answer'];
		echo CHtml::label(
			CHtml::radioButton(
				$questionManager->getInputName($idQuestion).'[]',
				in_array($idAnswer, $checked),
				array('id' => $idFor, 'value' => $idAnswer)
			).' '.$answer['answer'],
			$idFor,
			array('class' => 'radio')
		);
	}

	if(Settings::get('no_answer_in_poll', 'off') == 'on')
	{
		$index++;
		$idFor = $questionManager->getInputId($idQuestion).'-'.$index;

		echo CHtml::label(
				CHtml::radioButton(
					$questionManager->getInputName($idQuestion).'[]',
					in_array(0, $checked) || empty($checked),
					array('id' => $idFor, 'value' => 0)
				).' '.Yii::t('standard', '_NO_ANSWER'),
				$idFor,
				array('class' => 'radio')
			);
	}
}