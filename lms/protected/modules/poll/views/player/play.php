<div class="l-testpoll">

	<div class="page-title"><?= $this->poll->title ?></div>

	<div class="test-play-container">
	<?php

	echo CHtml::form(
		$this->createUrl('play', array('id_poll' => $this->poll->getPrimaryKey(), 'course_id' => $this->getIdCourse())),
		'POST',
		array('id' => 'poll-play-form', 'enctype' => 'multipart/form-data')
	);

	// Standard info
	echo CHtml::hiddenField('next_step', 'play', array('id' => 'next-step'));

	// Page info
	echo CHtml::hiddenField('page_to_save', $pageToDisplay, array('page-to-save'));
	echo CHtml::hiddenField('previous_page', $pageToDisplay, array('previous-page'));


		//$questions = $testInfo->getQuestionsForPage();
		$_index = $startQuestionIndex;
		if (!empty($questions)) {
			foreach ($questions as $question) {

				if ($questionPlayer = PollQuestion::getQuestionManager($question->type_quest)) {

					$questionPlayer->setController($this);
					$questionPlayer->setTrack($trackInfo);

					if ($questionPlayer->isInteractive()) {

						$_index++;

						$questionIndex = $_index; //TO DO: fix this
					?>

					<?php if (!($_index%2)) : ?>
					<div class="block-hr"></div>
					<?php endif; ?>

					<div class="block <?= (!($_index%2)) ? 'odd' : '' ?> type-<?= $question->type_quest ?>">

						<div class="block-header">
							<?= Yii::t('poll', '_QUEST_'.strtoupper($question->type_quest)) ?>
						</div>
						<div class="block-content">

							<div class="question-title-wrapper poll" data-question-index="<?= $question->id_quest ?>">
									<div class="question-number">
										<?=$questionIndex?>)
									</div>
									<div class="question-title">
										<?=$questionPlayer->applyTransformation('title', $question->title_quest, array(
											'idQuestion' => $question->id_quest,
											'questionManager' => $questionPlayer
										));?>
									</div>
							</div>

							<div class="answers-container">
								<?php $questionPlayer->play($question->getPrimaryKey()); ?>
							</div>

						</div>
					</div>

					<?php if (!($_index%2)) : ?>
						<div class="block-hr"></div>
					<?php endif; ?>

					<?php
					} else {
						//titles, break pages ...
						echo $questionPlayer->play($question->getPrimaryKey());
					}
				} else {
					//throw ...
				}
			}
		}

	?>

	<div class="page-info">
		<?php
			echo Yii::t('test', '_TEST_PAGES').': ';
			echo '<span class="info-page-to-display">'.(int)$pageToDisplay.'</span>';
			echo ' / ';
			echo '<span class="info-total-pages">'.(int)$totalPages.'</span>';
		?>
	</div>

	<div class="form-actions">
		<?php

		$buttons = array();

		if ($pageToDisplay != 1) {
			//back to the next page
			$buttons[] = CHtml::submitButton(Yii::t('standard', '_BACK'), array('name' => 'prev_page', 'id' => 'prev-page-btn', 'class' => 'btn-docebo grey big'));
		}
		if ($pageToDisplay != $totalPages) {
			//button to the next page
			$buttons[] = CHtml::submitButton(Yii::t('test', '_TEST_NEXT_PAGE'), array('name' => 'next_page', 'id' => 'next-page-btn', 'class' => 'btn-docebo grey big'));
			//$GLOBALS['page']->add(Form::getButton('next_page', 'next_page', $lang->def('_TEST_NEXT_PAGE'), '', ($tot_question > 0 && $test_info['mandatory_answer'] == 1 ? ' disabled="disabled"' : '')), 'content');
		} else {
			//button to the result page
			$buttons[] = '<button type="submit" name="show_result" id="test-show-result" class="btn-docebo green big">'
				.'<span class="i-sprite is-circle-check large white"></span>'
				.Yii::t('standard', '_SAVE')
				.'</button>';
		}

		echo implode('&nbsp;', $buttons);

		?>
	</div>

	<?php
		echo CHtml::closeTag('form');
	?>
	</div>
</div>

<script type="text/javascript">

setTimeout("keepalive()", 5*60*1000);
function keepalive() {
	var d = new Date();
	var time = d.getTime();
	$.ajax({
		url: <?=CJavaScript::encode($this->createUrl('keepAlive', array('course_id' => $this->getIdCourse(), 'id_poll' => (int)$idPoll)))?>
	});
	setTimeout("keepalive()", 5*60*1000);
}

	$(document).ready(function() {
	    $('.block.type-evaluation :radio').styler({});
	});

</script>