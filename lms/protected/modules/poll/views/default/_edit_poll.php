<?php if(isset($centralRepoContext)) {
	DoceboUI::printFlashMessages();
} ?>
<div id="<?= $centralRepoContext ? 'player-centralrepo-uploader-panel' : 'player-arena-poll-create-panel' ?>" <?= $centralRepoContext ? 'class="player-arena-editor-panel"' : '' ?>>

	<?php if(isset($centralRepoContext)): ?>
		<div class="header"><?php echo Yii::t('poll', '_POLL_ADD_FORM'); ?></div>
	<?php endif; ?>
	<div class="content">
<div id="dcb-feedback-custom" style="display: none;"></div>
		<?php
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'test-create-form',
			'method' => 'post',
			'action' => isset($saveUrl) ? $saveUrl :$this->createUrl("axEditPoll",array( 'opt' => $opt)),
			'htmlOptions' => array(
				'class' => 'ajax form-horizontal',
				'enctype' => 'multipart/form-data'
			)
		));

		if($loModel instanceof LearningRepositoryObject){
			echo CHtml::hiddenField('id_object', $loModel->id_object);
		}
		?>

		<?=CHtml::hiddenField('id_poll', $poll->id_poll)?>
		<?=CHtml::hiddenField('confirm', 1)?>

		<div id="<?= isset($centralRepoContext) ? 'player-centralrepo-uploader' : 'player-arena-uploader' ?>">

			<div class="tabbable"> <!-- Only required for left/right tabs -->
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab1" data-toggle="tab"><?=LearningOrganization::objectTypeValue($lo_type)?></a></li>
					<li><a href="#lo-additional-information" data-toggle="tab"><?=Yii::t('myactivities', 'Additional info')?></a></li>

					<?php
					// Raise event to let plugins add new tabs
					if(!$centralRepoContext){
						Yii::app()->event->raise('RenderEditLoTabs', new DEvent($this, array(
							'courseModel' => $courseModel,
							'loModel' => $loModel
						)));
					}
					?>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tab1">
						<div id="player-uploader-wrapper" class="player-arena-uploader">

							<div class="control-group">
								<?php echo CHtml::label(Yii::t('standard', '_TITLE'), 'title', array(
									'class' => 'control-label'
								)); ?>
								<div class="controls">
									<?php echo CHtml::textField('title', (isset($loModel) && $loModel ? $loModel->title : ''), array('id' => 'title', 'class' => 'input-block-level')); ?>
								</div>
							</div>
							<div class="control-group">
								<?= CHtml::label(Yii::t('standard', '_DESCRIPTION'), 'description', array(
									'class' => 'control-label'
								)) ?>
								<div class="controls">
									<?=CHtml::textArea('description', $description ? $description : $loModel->description, array(
										'class'=>'input-block-level',
										'id'=>'player-arena-test-editor-textarea',
									))?>
								</div>
							</div>

							<?php
							if($centralRepoContext && !$loModel->getPrimaryKey())
								$this->renderPartial('lms.protected.modules.centralrepo.views.centralRepo.tracking_mode', array('loModel' => $loModel));
							?>
						</div>
					</div>
					<div class="tab-pane" id="lo-additional-information">
						<?php
						$additionalInfoWidgetParams = array(
							'loModel'=> $loModel,
							'idCourse' => isset($courseModel) ? $courseModel->idCourse : null,
						);

						if($centralRepoContext){
							$additionalInfoWidgetParams['refreshUrl'] = Docebo::createAbsoluteLmsUrl('centralrepo/centralLo/getSlidersContent');
							$additionalInfoWidgetParams['deleteUrl'] = Docebo::createAbsoluteLmsUrl('centralrepo/centralLo/deleteImage');
						}
						?>
						<? $this->widget('common.widgets.PlayerLayoutAdditionalInfo', $additionalInfoWidgetParams); ?>
					</div>

					<?php
					// Raise event to let plugins add new tab contents
					if(!$centralRepoContext){
						Yii::app()->event->raise('RenderEditLoTabsContent', new DEvent($this, array(
							'courseModel' => $courseModel,
							'loModel' => $loModel
						)));
					}
					?>
				</div>
			</div>

		</div>


		<div class="text-right <?= !$centralRepoContext ? 'form-actions' : '' ?>">
			<input type="submit" name="confirm_save_poll" class="btn-docebo green big"
			       value="<?php echo Yii::t('standard', '_SAVE'); ?>">&nbsp;&nbsp;
			<button id="cancel_save_poll"
			        class="btn-docebo black big close-dialog"><?php echo Yii::t('standard', '_CANCEL'); ?></button>
		</div>

		<?php $this->endWidget(); ?>

	</div>

</div>

<script>
	$(function(){

		$('input[name="confirm_save_poll"]').on('click', function(){
			var title = $('.form-horizontal').find('#title').val();
			if (title.length <= 0) {
				//Docebo.Feedback.show('error', 'Enter a title!');
				$('#dcb-feedback-custom').html('<div class="alert alert-error in alert-block fade"><button type="button" class="close" data-dismiss="alert">&times;</button><span class="i-sprite is-remove red"></span>Enter a title!</div>');
				$('#dcb-feedback-custom').show();
				return false;
			}
		});

		if(TinyMce.checkEditorExistenceById('player-arena-test-editor-textarea')){
			TinyMce.removeEditorById('player-arena-test-editor-textarea');
		}
		TinyMce.attach($('#player-arena-test-editor-textarea'));

		$('#test-create-form').submit(function(){
			$('#test-create-form input[type=submit]').attr('disabled','disabled').addClass("disabled");
		});

		<?php if(isset($centralRepoContext)): ?>
			$("#cancel_save_poll").on("click", function(e){
				e.preventDefault();
				window.location.href = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=centralrepo/centralRepo/index';
			});
		<?php endif; ?>
	})
</script>