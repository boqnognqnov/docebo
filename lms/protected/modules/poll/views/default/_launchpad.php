<div class="container">

	<div class="row-fluid player-launchpad-header">
		<div class="span12">
			<h2><?php echo $pollInfo->title; ?> </h2>
			<?php
				if (PluginManager::isPluginActive('Share7020App')) {
					$this->widget('common.widgets.CourseLoHeaderApp7020', array('loID' => $idReference));
				}
			?>
			<a id="player-inline-close" class="player-close" href="#"><?= Yii::t('standard', '_CLOSE') ?> </a>
		</div>
	</div>
	
	
	<div class="player-arena-test-inline">
	
	<div class="row-fluid">
		<div class="span12"><!--<div class="span6">-->
			<div class="row-fluid no-space">
				<div class="span12">
					<div class="player-launchpad-test-head-image">
						<img src="<?php echo $this->getPlayerAssetsUrl().'/images/testpoll-img.png'; ?>" />
					</div>
					<div class="player-launchpad-test-description">
						<?php 
							echo $pollInfo->description;
						?>
					</div>
				</div>
					<?php
/*
						$endStatuses = array(
							LearningPolltrack::STATUS_VALID,
							LearningPolltrack::COMPLETED
						);
					
						$isEnd = in_array($trackInfo->score_status, $endStatuses);
*/

						$playUrl = $this->createUrl('player/play', array(
							'id_poll' => $pollInfo->getPrimaryKey(), 
							'course_id' => $this->getIdCourse()
						));
						echo CHtml::form($playUrl, 'post', array('id' => 'play-poll-form'));

						//if ($trackInfo) { $trackInfo->reset(); }
						
						if (count($pollInfo->getQuestions()) <= 0) {
							echo '<p>'.Yii::t('poll', '_NO_QUESTION_IN_POLL').'</p>';
						} elseif ($trackInfo && $trackInfo->status == LearningPolltrack::STATUS_VALID) {
							echo '<p>'.Yii::t('poll', '_POLL_ALREDY_VOTED').'</p>';
						} else {
							echo CHtml::submitButton(Yii::t('poll', '_POLL_BEGIN'), array(
								'name' => 'begin', 
								'id' => 'begin',
								'class' => 'btn-docebo big green'
							));
						}

						//echo CHtml::submitButton(Yii::t('test', '_TEST_BEGIN'), array('name' => 'restart', 'class' => 'btn-docebo green big'));
						echo CHtml::closeTag('form');
					?>
			</div>
		
		</div>

	</div>
	
	
	</div>
	
</div>
