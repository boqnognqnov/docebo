<?php /* @var $poll LearningPoll */
$this->breadcrumbs[] = $poll->title;
$this->breadcrumbs[] = Yii::t('standard', '_MOD');

if(isset($this->courseModel)){
	$_backUrl = PluginManager::isPluginActive('ClassroomApp') && $this->courseModel->isClassroomCourse()
		? Docebo::createLmsUrl('ClassroomApp/session/manageLearningObjects', array('course_id' => $this->getIdCourse()) )
		: $this->createUrl('//player/training/index', array('course_id' => $this->getIdCourse()) );
} else if(isset($opt) && $opt === 'centralrepo'){
	$_backUrl = Docebo::createAbsoluteLmsUrl('centralrepo/centralRepo/index');
}

?>

<div id="poll-editor-main-container" class="l-testpoll">

	<h2 class="subtitle">
		<a class="docebo-back" href="<?php echo $_backUrl; ?>">
			<span><?= Yii::t('standard', '_BACK') ?></span>
		</a>
		<?php echo Yii::t('standard', '_MOD'); ?>
	</h2>
	<div class="clearfix"></div>

	<br />

	<div class="row-fluid">
		<div class="span8">
			<a data-dialog-class="edit-lo-dialog" title="<?=Yii::t('standard', 'Edit poll')?>" href="<?=$this->createUrl('axEditPoll', array('id_poll' => (int)$idPoll , 'opt' => $opt))?>" class="ajax open-dialog" rel="modal-edit-title">
			<h2 id="poll-title-container" class="subtitle test-editor-title">
				<span id="poll-title"><?= $poll->title ?></span>
				<span class="objlist-sprite p-sprite edit-black"></span>
			</h2>
			</a>
		</div>
		<div class="span4">
			<div class="dropdown-docebo dropdown pull-right">
				<!-- Toggle -->
				<a href="#" class="btn-docebo green big" data-toggle="dropdown">
					<span class="i-sprite is-list white"></span>
					<?php echo Yii::t('poll', '_POLL_ADDQUEST'); ?>
					<span class="caret white"></span>
				</a>
				<ul class="dropdown-menu">
					<?php
						foreach (LearningPollquest::getPollTypesTranslationsList() as $questionType => $questionTranslation) {
							$questionUrl = $this->createUrl('question/create', array(
								'id_poll' => (int)$idPoll,
								'course_id' => $this->getIdCourse(),
								'question_type' => $questionType,
								'opt' => $opt
							));
							echo '<li>';
							echo '<a href="'.$questionUrl.'">'.$questionTranslation.'</a>';
							echo '</li>';
						}
					?>
				</ul>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<br/>

	<div id="poll-editor-question-list-container">
		<div class="row-fluid">
			<div class="span12">

				<table id="poll-editor-question-list"></table>
			</div>
		</div>
	</div>

	<br />
	<br />
</div>

<script type="text/javascript">
/*<![CDATA[*/

<?php $translations = LearningPollquest::getPollTypesTranslationsList(); ?>

var QuestionTypes = <?php echo CJavaScript::encode($translations); ?>;

var Questions = <?php
	$questions = array();
	$scores = array();
	$list = $poll->getQuestions(true);
	$pages = array();
	if (!empty($list)) {
		foreach ($list as $question) {
			$questions[] = array(
				'id_question' => (int)$question->id_quest,
				'id_category' => (int)$question->id_category,
				'question' => $this->formatQuestionText($question->title_quest, 80),//strip_tags($question->title_quest),
				'type' => $question->type_quest,
				'sequence' => (int)$question->sequence,
			);
			if (!in_array($question->page, $pages)) {
				$pages[] = $question->page;
			}
		}
	}
	$numQuestions = count($list);
	$numPages = count($pages);
	echo CJavaScript::encode($questions);
?>;

var QuestionCategories = <?php
	$list = LearningQuestCategory::model()->findAll();
	$categories = array();
	if (!empty($list)) {
		foreach ($list as $category) {
			$categories['_'.$category->idCategory] = array(
				'name' => $category->name,
				'description' => strip_tags($category->textof),
				'author' => (int)$category->author
			);
		}
	}
	echo CJavaScript::encode($categories);
?>;

var Langs = {
	_BREAK_PAGE: '---- <?php echo strtoupper($translations[LearningPollquest::POLL_TYPE_BREAK_PAGE]); ?> ----'
};
var baseUpdateUrl = <?php echo CJavaScript::encode($this->createUrl('question/update', array('id_poll' => $idPoll, 'course_id' => $this->getIdCourse(), 'opt' => $opt))); ?>;
var baseDeleteUrl = <?php echo CJavaScript::encode($this->createUrl('question/axDelete', array('id_poll' => $idPoll, 'course_id' => $this->getIdCourse(), 'opt' => $opt))); ?>;

$(document).ready(function(){
	try {

		$('#poll-edit-main-actions').doceboBootcamp();

		$(document).on("dialog2.content-update", ".modal", function () {
			var res = $(this).find("a.auto-close.success");
			if (res.length > 0) {
				$('#poll-title').html(res.data('new_title'));
			}
		});

		//create questions table
		$('#poll-editor-question-list').dataTable({
			bProcessing: true,
			bPaginate: false,
			//bServerSide: true,
			bFilter: false,
			sAjaxSource: '<?php echo $this->createUrl('axGetQuestionsList', array('id_poll' => (int)$idPoll, 'course_id' => $this->getIdCourse(), 'opt' => $opt)); ?>',
			//aaData: Questions,
			//bJQueryUI: true,
			sDom: 'rt',
			//iDisplayLength: 15,
			oLanguage: {
				sEmptyTable: <?=CJSON::encode(Yii::t('standard', '_EMPTY_SELECTION'))?>,
				sProcessing: <?=CJSON::encode(Yii::t('standard', '_LOADING').'...')?>,
				sLoadingRecords: <?=CJSON::encode(Yii::t('standard', '_LOADING').'...')?>
			},
			fnCreatedRow: function( nRow, aData, iDataIndex ) {
				$(nRow).attr('id', 'questions-list-row-'+aData.id_question);
			},
			/*fnDrawCallback: function(oSettings) {
				try {
				var rowIndex;
				$('#poll-editor-question-list').find('tbody tr').each(function() {
					$(this).find('td').eq(1).find('span').html(""+this.rowIndex);
				});
				} catch(e) {}
			},*/
			aoColumns: [
				{
					sTitle: "",
					mData: "id_question",
					bSortable: false,
					sClass: "img-cell",
					mRender: function(data, type, row) {
						return '<span class="p-sprite drag-small-black"></span>';
					}
				},
				{
					sTitle: "",
					bSortable: false,
					mData: "sequence",
					sClass: "img-cell"
				},
				{
					sTitle: "<?php
						echo Yii::t('test', '_QUEST').'&nbsp;&nbsp;'
							.'<span class=\\"table-title-info\\">'
							.'('.Yii::t('test', '_TEST_CAPTION', array(
								'%tot_element%' => $numQuestions,
								'%tot_page%' => $numPages
							)).')'
							.'</span>';
					?>",
					mData: "question",
					bSortable: false,
					sClass: "nowrap text-left",
					mRender: function(data, type, row) {
						if (row.type == 'break_page') {
							return Langs._BREAK_PAGE;
						}
						return data;
					}
				},
				{
					sTitle: "<?php echo Yii::t('test', '_TEST_PAGES'); ?>",
					mData: "page",
					bSortable: false,
					sClass: "text-center"
				},
				/*{
					sTitle: "<?php echo Yii::t('standard', '_CATEGORY'); ?>",
					mData: "id_category",
					bSortable: false,
					sClass: "text-left",
					mRender: function(data, type, row) {
						if (row.type != 'break_page') {
							if (data && data > 0 && QuestionCategories['_'+data]) {
								return QuestionCategories['_'+data].name;
							}
						}
						return '';
					}
				},*/
				{
					sTitle: "<?php echo Yii::t('standard', '_TYPE'); ?>",
					mData: "type",
					sClass: "text-left question-type",
					bSortable: false,
					mRender: function(data, type, row) {
						return QuestionTypes[data];
					}
				},
				{
					sTitle: "",
					mData: "id_question",
					bSortable: false,
					sClass: "img-cell",
					mRender: function(data, type, row) {
						if (row.type != 'break_page') {
							return '<a id="question-update-'+row.id_question+'" href="'+baseUpdateUrl+'&id_question='+row.id_question+'">'
								+'<span class="objlist-sprite p-sprite edit-black"></span>'
								+'</a>';
						}
						return '';
					}
				},
				{
					sTitle: "",
					mData: "id_question",
					bSortable: false,
					sClass: "img-cell",
					mRender: function(data, type, row) {
						return '<a id="question-delete-'+row.id_question+'" href="'+baseDeleteUrl+'&id_question='+row.id_question+'">'
							+'<span class="objlist-sprite p-sprite cross-small-red"></span>'
							+'</a>'
							+'</div>';
					},
					fnCreatedCell: function(nTd, sData, oData, iRow, iCol) {
						/*$(nTd).controls();*/
						$(nTd).delegate('.cross-small-red', 'click', function(e) {
							e.preventDefault();

							bootbox.dialog(Yii.t('standard', '_AREYOUSURE'), [{
									label: Yii.t('standard', '_CONFIRM'),
									'class': 'btn-docebo big green',
									callback: function() {
										$.ajax({
											url: $('#question-delete-'+oData.id_question).attr('href'),
											type: 'post',
											dataType: 'json',
											data: {
												confirm: 1
											}
										}).done(function (o) {
											if (o.success) {
												var nTr, t = $('#poll-editor-question-list').dataTable();
												if (o.data.id_question) {
													$nTr = $('#questions-list-row-'+o.data.id_question);
													if ($nTr) { t.fnDeleteRow(t.fnGetPosition($nTr[0])); }
												}
											} else {
												//...
											}
										}).fail(function(o) {
											//..
										});
									}
								}, {
									label: Yii.t('standard', '_CANCEL'),
									'class': 'btn-docebo big black',
									callback: function() { }
								}],
								{ header: Yii.t('standard', '_DEL') }
							);
						});
					}
				}
			]
		}).rowReordering({
			sURL: '<?php echo $this->createUrl('axReorderQuestion', array('id_poll' => $idPoll, 'course_id' => $this->getIdCourse(), 'opt' => $opt)); ?>',
			handle: '.drag-small-black',
			iIndexColumn: 1
		});

	} catch(e) {
		//alert(e);
	}
});

/*]]>*/
</script>
