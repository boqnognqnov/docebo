<div id="player-arena-poll-create-panel" class="player-arena-editor-panel">
	<div class="header">
		<?php echo Yii::t('poll', isset($poll) ? '_MOD' : '_POLL_ADD_FORM'); ?>
	</div>

	<div class="content">

	<?php
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'poll-'.(isset($poll) ? 'edit' : 'create').'-form',
			'method' => 'post',
			'action' => $this->createUrl((isset($poll) ? "editTitle" : "axSaveLo")),
			'htmlOptions' => array(
				'class'=>'form-horizontal',
				'enctype' => 'multipart/form-data'
			)
		));

		if (isset($poll)) { echo CHtml::hiddenField('id_poll', $poll->getPrimaryKey(), array('id' => 'id-poll')); }
	?>

		<div id="player-arena-uploader">

		<div class="tabbable"> <!-- Only required for left/right tabs -->
			<ul class="nav nav-tabs">
				<li class="active"><a href="#tab1" data-toggle="tab"><?=LearningOrganization::objectTypeValue($lo_type)?></a></li>
				<li><a href="#lo-additional-information" data-toggle="tab"><?=Yii::t('myactivities', 'Additional info')?></a></li>

				<?php
				// Raise event to let plugins add new tabs
				Yii::app()->event->raise('RenderEditLoTabs', new DEvent($this, array(
					'courseModel' => $courseModel,
					'loModel' => $loModel
				)));
				?>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tab1">
					<div id="player-uploader-wrapper" class="player-arena-uploader">

						<div class="control-group">
							<?= CHtml::label(Yii::t('standard', '_TITLE'), 'poll-title', array('class'=>'control-label')) ?>
							<div class="controls">
								<?= CHtml::textField("title", isset($poll) ? $poll->title : '', array('id' => 'poll-title', 'class' => 'input-block-level squared')); ?>
							</div>
						</div>

						<div class="control-group">
							<?= CHtml::label(Yii::t('standard', '_DESCRIPTION'), 'description', array('class'=>'control-label')) ?>
							<div class="controls">
								<?= CHtml::textArea('description', isset($poll) ? $poll->description : '', array('id' => 'player-arena-poll-editor-textarea', 'class' => 'input-block-level squared')); ?>
							</div>
						</div>

						<br>

					</div>
				</div>
				<div class="tab-pane" id="lo-additional-information">
					<? $this->widget('common.widgets.PlayerLayoutAdditionalInfo', array(
						'loModel'=> isset($loModel) ? $loModel : null,
						'idCourse'=>$idCourse
					)); ?>
				</div>

				<?php
				// Raise event to let plugins add new tab contents
				Yii::app()->event->raise('RenderEditLoTabsContent', new DEvent($this, array(
					'courseModel' => $courseModel,
					'loModel' => $loModel
				)));
				?>
			</div>
		</div>

		</div>

		<div class="text-right">
			<input type="submit" name="confirm_save_poll" class="btn-docebo green big"  value="<?php echo Yii::t('standard', '_SAVE'); ?>">
			<?php if (isset($poll)): ?>
				<input type="submit" name="cancel" class="btn-docebo black big"  value="<?php echo Yii::t('standard', '_CANCEL'); ?>">
			<?php else: ?>
				<a id="cancel_save_poll" class="btn-docebo black big"><?php echo Yii::t('standard', '_CANCEL'); ?></a>
			<?php endif; ?>
		</div>

	<?php $this->endWidget(); ?>

	</div>

</div>