<?php
/* @var $poll LearningPoll */
/* @var $question LearningPollquest */

$this->breadcrumbs[$poll->title] = $backUrl;
$this->breadcrumbs[] = ($isEditing ? Yii::t('standard', '_MOD') : Yii::t('test', '_TEST_ADDQUEST'));
?>
<script type="text/javascript">
if (!String.prototype.trim) { String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, ''); }; }
if (!Array.prototype.insert) { Array.prototype.insert = function (index, item) { this.splice(index, 0, item); }; }

var Validation = {
	checkList: [],
	addValidator: function(validator, message, priority) {
		var i, done = false;
		if (!priority) { priority = 0; }
		if (this.checkList.length > 0) {
			for (i=0; i<this.checkList.length && !done; i++) {
				if (this.checkList[i].priority > priority) {
					this.checkList.insert(i, {
						validator: validator,
						message: message,
						priority: priority
					});
					done = true;
				}
			}
		}
		if (!done) {
			this.checkList.push({
				validator: validator,
				message: message,
				priority: priority
			});
		}
	},
	check: function() {
		var i, output = {success: true};
		for (i=0; i<this.checkList.length && output.success; i++) {
			if (!this.checkList[i].validator.call(this)) {
				output.success = false;
				output.message = this.checkList[i].message;
			}
		}
		return output;
	}
};
</script>
<?php

$this->onBeforeQuestionEditRender(new CEvent($this, array(
	'isEditing' => $isEditing,
	'idPoll' => $idPoll,
	'idQuestion' => (isset($idQuestion) ? (int)$idQuestion : NULL),
	'questionText' => $questionText,
	'questionCategory' => $questionCategory,
	'sequence' => $sequence,
	'opt' => $opt
)));

?>
<div class="l-testpoll">
<!--
	<h1 class="page-title">
		<?php echo $poll->title; ?>
	</h1>
-->
	<h2 class="subtitle">
		<a class="docebo-back" href="<?echo $backUrl; ?>">
			<span><?= Yii::t('standard', '_BACK') ?></span>
		</a>
		<?php
			$acronyms = LearningPollquest::getPollTypesAcronymsList();
			$translations = LearningPollquest::getPollTypesTranslationsList();
			if ($isEditing) {
				echo Yii::t('standard', '_MOD').' ';
				echo $acronyms[$questionType].' - ';
				echo $translations[$questionType];
			} else {
				echo Yii::t('standard', '_ADD').' ';
				echo $acronyms[$questionType].' - ';
				echo $translations[$questionType];
			}
		?>
<!--
		<p class="help">
			<span class="i-sprite is-list large"></span>
			<?= Yii::t('poll', '_QUESTION_DESC_'.strtoupper($questionType)) ?>
		</p>
-->
	</h2>
	<br>

	<?php
		$action = $isEditing
			? $this->createUrl('update', array('id_poll' => (int)$idPoll, 'id_question' => $idQuestion, 'opt' => $opt))
			: $this->createUrl('create', array('id_poll' => (int)$idPoll, 'opt' => $opt));
		echo CHtml::form($action, 'POST', array('id' => 'question-'.($isEditing ? 'edit' : 'create').'-form'));
		//if (isset($idTest) && (int)$idTest > 0) { echo CHtml::hiddenField('id_test', (int)$idTest); }
		//if (isset($idQuestion) && (int)$idQuestion > 0) { echo CHtml::hiddenField('id_question', (int)$idQuestion); }
		echo CHtml::hiddenField('question_type', $questionType, array('id' => 'question-type'));
	?>

	<div class="block-hr"></div>

	<div class="block odd">
		<div class="block-header"><?php echo Chtml::label(Yii::t('test', '_QUEST'), 'question-text'); ?></div>
		<div class="block-content">

			<?php echo CHtml::textArea('question', $questionText, array('id' => 'question-text')); ?>

			<br>
			<!--<div class="row-fluid">
				<?php if ($this->getUseCategory()): ?>
					<?php
					$criteria = new CDbCriteria();
					$criteria->order = 'name ASC';
					Yii::app()->event->raise('BeforeListingTestCategories', new DEvent($this, array('criteria' => &$criteria)));
					$categories = LearningQuestCategory::model()->findAll($criteria);
					if (!empty($categories)):
						$categoriesList = array(0 => '( '.Yii::t('standard', '_NONE').' )');
						foreach ($categories as $category) {
							$categoriesList[(int)$category['idCategory']] = $category['name'];
						}
						?>
					<div class="span4">
						<?php echo Chtml::label(Yii::t('test', '_TEST_QUEST_CATEGORY'), 'question-category'); ?>
						<?php echo CHtml::dropDownList('id_category', $questionCategory, $categoriesList, array('id' => 'question-category')); ?>
					</div>
					<?php endif; ?>
				<?php endif; ?>

			</div>-->
		</div>
	</div>
	<div class="block-hr"></div>

	<?php
	$this->onAfterQuestionEditRender(new CEvent($this, array(
		'isEditing' => $isEditing,
		'idPoll' => $idPoll,
		'question' => $question,
		'idQuestion' => (isset($idQuestion) ? (int)$idQuestion : NULL),
		'questionText' => $questionText,
		'questionCategory' => $questionCategory,
		'sequence' => $sequence,
		'settings' => $question->settings
	)));
	?>

	<div class="form-actions">
		<input type="submit" name="save" class="span2 btn-docebo green big" value="<?php echo Yii::t('standard', '_SAVE'); ?>">
		<input type="submit" name="undo" class="span2 btn-docebo black big" value="<?php echo Yii::t('standard', '_CANCEL'); ?>">
	</div>

	<?=	CHtml::endForm() ?>
<div>


<script type="text/javascript">
$(document).ready(function() {
	TinyMce.attach("#question-text", {height: '170px'});

	Validation.addValidator(function() {
		var content = $('#question-text').tinymce().getContent();
		return (content.trim() != "");
	}, <?php echo CJavaScript::encode(Yii::t('test', 'Empty question title is not allowed')); ?>, 0);

	var form = $('#question-<?=($isEditing ? 'edit' : 'create')?>-form'), submitActor = false;

	form.find(":submit").click(function () {
		submitActor = $(this).attr('name');
	});

	form.on('submit', function(e) {
		if (submitActor == 'save') {
			var result = Validation.check();
			if (!result.success) {
				e.preventDefault();
				/*
				bootbox.alert(result.message);
				*/
				bootbox.dialog(result.message, [{
						label: Yii.t('standard', '_CANCEL'),
						'class': 'btn-docebo big black',
						callback: function() { }
					}],
					{ header: Yii.t('standard', '_WARNING') }
				);
			}
		}
	});
});
</script>

