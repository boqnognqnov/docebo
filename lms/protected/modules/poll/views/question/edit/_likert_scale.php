<?php

/* @var $question LearningPollquest */

$this->renderPartial('edit/templates/_template_liker_scale', array(
    'questionType' => LearningPollquest::POLL_TYPE_LIKERT_SCALE,
    'singleCorrectAnswer' => false,
    'questionManager' => $questionManager,
    'isEditing' => $isEditing,
    'question' => $question,
    'idQuestion' => $idQuestion,
    'idPoll' => $id_poll
));