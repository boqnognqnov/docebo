<?php

/* @var $question LearningPollquest */

$this->renderPartial('edit/templates/_template_evaluation', array(
	'questionType' => LearningPollquest::POLL_TYPE_EVALUATION,
	'singleCorrectAnswer' => true,
	'questionManager' => $questionManager,
	'isEditing' => $isEditing,
	'question' => $question,
	'idQuestion' => $idQuestion,
	'answerHtmlEditor' => true,
	'modality' => 'simple'
));