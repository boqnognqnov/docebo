<?php

$this->renderPartial('edit/templates/_template_choice', array(
	'questionType' => LearningPollquest::POLL_TYPE_CHOICE,
	'singleCorrectAnswer' => true,
	'questionManager' => $questionManager,
	'isEditing' => $isEditing,
	'idQuestion' => $idQuestion,
	'answerHtmlEditor' => true,
	'modality' => 'simple'
));