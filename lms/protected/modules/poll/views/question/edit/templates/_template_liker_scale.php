<?php
$questionManager = PollQuestion::getQuestionManager($questionType);
$answers = ($isEditing ? $questionManager->getAnswers($idQuestion) : array());
$scales = $questionManager->getPollScales($idPoll);
if (empty($answers) && empty($scales)) {
    $showHints = array(
        'bootstroAddLikertScaleSurveyQuestion',
        'bootstroAddLikertScaleSurveyScales'
    );
    $this->widget('OverlayHints', array('hints'=> implode(',', $showHints) ));
}
?>

<div class="block even likertScaleContainer">
    <div class="leftColumn">
        <div class="header"><?= strtoupper(Yii::t('poll', 'Add questions')); ?></div>
        <div class="headerContainer" data-bootstro-id="bootstroAddLikertScaleSurveyQuestion">
            <?= CHtml::textField('questions_input', null, array('id' => 'addQuestionInput'))?>
            <a href="javascript:;" class="add-question-button"><i class="fa fa-plus-circle"></i></a>
        </div>
        <div class="leftContainer">
            <div class="likert-questions-block-content">
                <div id="questions-table-container">
                    <table id="questions-table" class="table">
                        <thead><tr><th><?= Yii::t('app7020', 'Questions'); ?></th><th colspan="3"></th></tr></thead>
                        <tbody id="questions-tbody">
                            <tr id="no-question-holder"><td colspan="4"><span><?php echo Yii::t('standard', 'No Questions'); ?></span></td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="rightColumn">
        <div class="header"><?= strtoupper(Yii::t('poll', 'Define likert scale')); ?></div>
        <div class="headerContainer" data-bootstro-id="bootstroAddLikertScaleSurveyScales">
            <?= CHtml::textField('scales_input', null, array('id' => 'addScaleInput'))?>
            <a href="javascript:;" class="add-scale-button"><i class="fa fa-plus-circle"></i></a>
        </div>
        <div class="rightContainer">
            <div class="likert-questions-block-content">
                <div id="scales-table-container">
                    <table id="scales-table" class="table">
                        <thead><tr><th></th><th><?= Yii::t('poll', 'Scales'); ?></th><th colspan="3"></th></tr></thead>
                        <tbody id="scales-tbody">
                            <tr id="no-scales-holder"><td colspan="4"><span><?php echo Yii::t('standard', 'No Scales'); ?></span></td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="deleteScaleMessage" style="display: none">
    <?= $questionManager->getDeleteScaleMessage() ?>
</div>

<style>
    div.form-actions{
        clear: both;
    }
</style>

<script type="text/javascript">
    $(function(){

        var InitialAnswers = [];
        var InitialScales = [];
        <?php
            if (!empty($answers)):
                foreach ($answers as $answer):
                    $toJavascript = array(
                        //'correct' => $answer['is_correct'],
                        'title' => $answer['answer'],
                        //'comment' => $answer['comment'],
                        //'scoreCorrect' => $answer['score_correct'],
                        //'scoreIncorrect' => $answer['score_incorrect']
                    );
                    if (isset($answer['id_answer']) && $answer['id_answer'] > 0) {
                        $toJavascript['idQuestion'] = $answer['id_answer'];
                    }
        ?>
        InitialAnswers.push(<?=CJavaScript::encode($toJavascript)?>);
        <?php
                endforeach;
            endif;
        ?>

        <?php
            if (!empty($scales)):
                foreach ($scales as $scale):
                    $toJavascript = array( 'idScale' => $scale['id'], 'title' => $scale['title']);
        ?>
        InitialScales.push(<?=CJavaScript::encode($toJavascript)?>);
        <?php
                endforeach;
            endif;
        ?>

        AnswersManager.init({
            questions: InitialAnswers,
            scales: InitialScales
        });
    });

</script>