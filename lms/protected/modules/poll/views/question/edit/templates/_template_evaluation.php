<?php

/* @var $question LearningPollquest */

$_maxScore = PollQuestionEvaluation::MAX_SCORE;

if ($question) {
	if ($isEditing && isset($question->settings['max_score'])) {
		$settingMaxScore = intval($question->settings['max_score']);
		if ($settingMaxScore >= 1 && $settingMaxScore <= 100) {
			$_maxScore = $settingMaxScore;
		}
	}
}

/**
 * Render the answers range as hidden fields
 */

for ($i = 0; $i < $_maxScore; $i++) {
	echo CHtml::hiddenField("answers[$i][answer]", $i+1);
}

?>

<br>
<div class="block">
	<div class="block-header">
		<?=Yii::t('standard', '_MAX_SCORE');?>
		<br/>
		<span><em>(1 - 100)</em></span>
	</div>
	<div class="block-content">
		<?= CHtml::numberField('max_score', $_maxScore, array('max'=>100, 'min'=>1)) ?>
	</div>
</div>


<!--<br>
<div class="block odd no-padding">
	<div class="block-header"><?/*=Yii::t('standard', '_PREVIEW');*/?></div>
	<div class="block-content">
	</div>
</div>-->


<script type="text/javascript">

$(document).ready(function(e) {

	$('input#max_score').keyup(function() {
		var input = parseInt($(this).val());
		var submit = $(this).closest('form').find(':submit');
		if (isNaN(input) || input<1 || input>100) {
			submit.addClass('disabled');
		} else {
			submit.removeClass('disabled');
		}
	}).trigger('keyup');

	$('#question-edit-form').submit(function(e) {
		if ($(this).find(':submit').hasClass('disabled')) {
			e.preventDefault();
			return false;
		}
	});

});

</script>