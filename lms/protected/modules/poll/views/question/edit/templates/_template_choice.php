<?php
$numbersPattern = '\d+\.?\d*';
?>


<br>
<div class="block no-padding">
	<div class="block-header" <?php if ( $questionType == LearningTestquest::QUESTION_TYPE_INLINE_CHOICE):?> style="margin-top: 9px;" <? endif;?>><?=Yii::t('test', '_TEST_ANSWER');?></div>
	<div class="block-content">
		<div id="no-answers-placeholder" <?php if ( $questionType == LearningTestquest::QUESTION_TYPE_INLINE_CHOICE):?> style="margin-top: 9px;" <? endif;?>>
			<span>(<?php echo Yii::t('standard', '_NO_ANSWER'); ?>)</span>
		</div>
		<div id="answers-table-container">
			<table id="answers-table" class="table">
				<?php if ( $questionType == LearningTestquest::QUESTION_TYPE_INLINE_CHOICE):?>
					<thead>
					<tr>
						<th></th>
						<th class="sort"><?=Yii::t('test', '_ANSWER');?> &nbsp;<i class="fa"></i></th>
						<th></th>
						<th></th>
					</tr>
					</thead>
				<?php endif;?>
				<tbody id="answers-tbody">
				</tbody>
			</table>
		</div>
	</div>
</div>


<div class="block no-padding">

	<div class="block-content" >
		<!--<div id="add-answer-block-header">-->
			<div id="add-answer-link-container">
				<a href="#" id="toggle-add-answer">
					<span class="i-sprite is-plus-solid green"></span>&nbsp;<strong><?=Yii::t('poll', '_POLL_ADD_ONE_ANSWER');?></strong>
				</a>
			</div>
		<!--</div>-->
		<!--<div id="edit-answer-block-header" style="display:none;">
			<?=Yii::t('standard', '_MOD');?>
		</div>-->
	</div>
</div>


<div class="block no-padding" id="form-box-container">

	<div class="block-header" id="add-answer-block-header">
		<a href="#" id="toggle-add-answer"><?=Yii::t('poll', '_POLL_ADD_ONE_ANSWER');?></a>
	</div>
	<div class="block-header" id="edit-answer-block-header" style="display:none;">
		<?=Yii::t('standard', '_MOD');?>
	</div>

	<div class="block-content" >

		<!--<div>-->
			<div class="block-hr"></div>
			<div class="block odd">
				<!--<div class="block-header">
					<?php
					echo CHtml::label(CHtml::checkBox(false, false, array('id' => 'new-answer-correct')) . ' ' . Yii::t('test', '_TEST_CORRECT'), 'new-answer-correct', array('class' => 'checkbox'));
					?>
				</div>-->
				<div class="block-content">
					<?= CHtml::label(Yii::t('poll', '_ANSWER_TEXT'), 'new-answer-text') ?>
					<?= CHtml::textArea(false, '', array('id' => 'new-answer-text', 'class'=>'input-block-level')) ?>
					<br>
					<div class="text-right" id="add-answer-buttons">
						<input type="button" id="add-answer-button" class="btn-docebo green big" value="<?=Yii::t('standard', '_ADD');?>" />
						<input type="button" id="undo-add-answer-button" class="btn-docebo black big" value="<?=Yii::t('standard', '_CLOSE');?>" />
					</div>
					<div class="text-right" id="mod-answer-buttons" style="display:none">
						<input type="button" id="mod-answer-button" class="btn-docebo green big" value="<?=Yii::t('standard', '_SAVE');?>" />
						<input type="button" id="undo-edit-answer-button" class="btn-docebo black big" value="<?=Yii::t('standard', '_CLOSE');?>" />
					</div>
				</div>
			</div>
			<div class="block-hr"></div>
		<!--</div>-->

	</div>
</div>
<script type="text/javascript">

var InitialAnswers = [];
<?php

//$answers = ($isEditing ? $questionManager->_getAnswersList($idQuestion) : array());
$answers = ($isEditing ? $questionManager->getAnswers($idQuestion) : array());

if (!empty($answers)):
	foreach ($answers as $answer):
		$toJavascript = array(
			//'correct' => $answer['is_correct'],
			'answer' => $answer['answer'],
			//'comment' => $answer['comment'],
			//'scoreCorrect' => $answer['score_correct'],
			//'scoreIncorrect' => $answer['score_incorrect']
		);
		if (isset($answer['id_answer']) && $answer['id_answer'] > 0) {
			$toJavascript['idAnswer'] = $answer['id_answer'];
		}
?>
InitialAnswers.push(<?=CJavaScript::encode($toJavascript)?>);
<?php
	endforeach;
endif;
?>


$(document).ready(function(e) {

//	try {

		AnswersManager.init({
			numberPattern: <?php echo '/'.$numbersPattern.'/'?>,
			answerHtmlEditor: <?php echo $answerHtmlEditor ? 'true' : 'false'; ?>,
			singleCorrectAnswer: <?php echo $singleCorrectAnswer ? 'true' : 'false'; ?>,
			answers: InitialAnswers,
			modality: <?php echo CJavaScript::encode($modality); ?>,
			questionType: '<?=$questionType?>'
		});

//	} catch (e) {
//		alert(e);
//	}

});

</script>