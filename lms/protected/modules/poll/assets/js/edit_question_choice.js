var AnswersManager = {

	init: function(options) {

		var numbersPattern = options.numberPattern || /\d+\.?\d*/;
		var answerHtmlEditor = (options.answerHtmlEditor || false);
		var singleCorrectAnswer = (options.singleCorrectAnswer || false);
		var modality = options.modality;

		switch (modality.toLowerCase()) {
			case 'full': modality = 'full'; break;
			default: { modality = 'simple'; } break;
		}

		if (answerHtmlEditor) {
			TinyMce.attach('#new-answer-text', 'simple', {height: '100px'});
		}

		$('#answers-tbody').sortable({
			handle: '.drag-small-black',
			helper: function(e, ui) {
				ui.children().each(function() {
					$(this).width($(this).width());
				});
				return ui;
			},
			update: function(e, ui) {
				fixIndexes();
			}
		});

		var setAllIncorrect = function() {
			$('#answers-tbody > tr').each(function() {
				$('td:eq(0)', this).html(getCorrectnessText(false));
				var index = $(this).attr('id').replace('answers-table-input-row-', '');
				$('#answers-'+index+'-correct').val('0');
			});
		};

		var inputCounter = 0;

		var getCorrectnessText = function(isCorrect) {
			return '<span class="'+(isCorrect ? 'answer-correct' : 'answer-incorrect')+'">'
				+(isCorrect ? Yii.t('test', '_TEST_CORRECT') : Yii.t('test', '_TEST_INCORRECT'))
				+'</span>';
		};

		var addTableRow = function(values) {
			var preName = 'answers['+inputCounter+']', preId = 'answers-'+inputCounter+'-';

			var inputs = '<input type="hidden" name="'+preName+'[answer]" id="'+preId+'answer" />';
			if (modality == 'full') {
				inputs += '<input type="hidden" name="'+preName+'[comment]" id="'+preId+'comment" />'
				+'<input type="hidden" name="'+preName+'[correct]" id="'+preId+'correct" />'
				+'<input type="hidden" name="'+preName+'[score_correct]" id="'+preId+'score_correct" />'
				+'<input type="hidden" name="'+preName+'[score_incorrect]" id="'+preId+'score_incorrect" />';
			}
			if (values.idAnswer) {
				inputs += '<input type="hidden" name="'+preName+'[id_answer]" id="'+preId+'id_answer" />';
			}

			if (modality == 'full') {
				if (singleCorrectAnswer) {
					if (values.correct) { setAllIncorrect(); }
				}
			}

			var html = '<tr id="answers-table-input-row-'+inputCounter+'">';
			if (modality == 'full') { html += '<td>'+getCorrectnessText(values.correct)+'</td>'; }
			html += '<td>'+values.answer+'</td>';
			if (modality == 'full') {
				html += '<td>'+Yii.t('test', '_TEST_IFCORRECT')+'</td>'
				+'<td>'+parseFloat(values.scoreCorrect)+'</td>'
				+'<td>'+Yii.t('test', '_TEST_IFINCORRECT')+'</td>'
				+'<td>'+parseFloat(values.scoreIncorrect)+'</td>';
			}
			html += '<td class="img-cell"><span class="p-sprite drag-small-black"></span>'+inputs+'</td>'
				+'<td class="img-cell">'+'<a href="#" id="update-answer-'+inputCounter+'"><span class="objlist-sprite p-sprite edit-black"></span></a>'+'</td>'
				+'<td class="img-cell">'+'<a href="#" id="delete-answer-'+inputCounter+'"><span class="objlist-sprite p-sprite cross-small-red"></span></a>'+'</td>'
				+'</tr>';

			$('#answers-tbody').append(html);

			$('#'+preId+'answer').val(values.answer);
			if (modality == 'full') {
				$('#'+preId+'comment').val(values.comment);
				$('#'+preId+'correct').val(values.correct ? '1' : '0');
				$('#'+preId+'score_correct').val(values.scoreCorrect);
				$('#'+preId+'score_incorrect').val(values.scoreIncorrect);
			}
			if (values.idAnswer) { $('#'+preId+'id_answer').val(values.idAnswer); }

			$('#update-answer-'+inputCounter).on("click", function(e) {
				e.preventDefault();
				var index = $(this).attr('id').replace('update-answer-', ''), pre = 'answers-'+index+'-';

				var values = { answer: $('#'+pre+'answer').val() };
				if (modality == 'full') {
					values.correct = ($('#'+pre+'correct').val() > 0);
					values.comment = $('#'+pre+'comment').val();
					values.scoreCorrect = $('#'+pre+'score_correct').val();
					values.scoreIncorrect = $('#'+pre+'score_incorrect').val();
				}

				activateEditMode(index, values);

				var top = $('#form-box-container').offset().top;
				$('body').animate({scrollTop: top}, 'slow');
			});

			$('#delete-answer-'+inputCounter).on("click", function(e) {
				e.preventDefault();
				var index = $(this).attr('id').replace('delete-answer-', '');
				bootbox.confirm(Yii.t('standard', '_AREYOUSURE'), function(result) {
					if (result) {
						deleteTableRow(index);
					}
				});
			});

			inputCounter++;

			setAnswersVisibility();

			fixIndexes();
		};

		var deleteTableRow = function(index) {
			$('#answers-table-input-row-'+index).remove();
			fixIndexes();
			setAnswersVisibility();
		};

		var setAnswersVisibility = function() {
			if (getNumAnswers() > 0) {
				$('#no-answers-placeholder').css('display', 'none');
				$('#answers-table-container').css('display', 'block');
			} else {
				$('#no-answers-placeholder').css('display', 'block');
				$('#answers-table-container').css('display', 'none');
			}
		};

		var fixIndexes = function() {
			$('#answers-tbody > tr').each(function() {
				var rowIndex = $(this).index();
				$('input[name^=answers]', this).each(function() {
					var oldName = $(this).attr('name');
					var newName = oldName.replace(/answers\[[0-9]\]*/, 'answers['+rowIndex+']');
					$(this).attr('name', newName);
				});
			});
		};

		var clearForm = function() {
			if (answerHtmlEditor) {
				$('#new-answer-text').tinymce().setContent("");
			} else {
				$('#new-answer-text').val("");
			}
			if (modality == 'full') {
				$('#new-answer-correct').attr('checked', false);
				$('#new-answer-comment').val("");
				$('#new-answer-score_correct').val('0');
				$('#new-answer-score_incorrect').val('0');
			}
		};

		var isEditing = false, wasMaskHidden = true, selectedRow = false;

		var selectEditRow = function(index) {
			var tr = $('#answers-'+index+'-answer').parent().parent();
			if (tr) {
				selectedRow = tr;
				tr.addClass('selected-table-row');
			}
		};

		var unselectEditRow = function() {
			if (selectedRow) { selectedRow.removeClass('selected-table-row'); }
			selectedRow = false;
		};

		var activateEditMode = function(index, values) {
			if (answerHtmlEditor) {
				$('#new-answer-text').tinymce().setContent(values.answer || "");
			} else {
				$('#new-answer-text').val(values.answer || "")
			}
			if (modality == 'full') {
				$('#new-answer-correct').attr('checked', values.correct);
				$('#new-answer-comment').val(values.comment);
				$('#new-answer-score_correct').val(values.scoreCorrect);
				$('#new-answer-score_incorrect').val(values.scoreIncorrect);
			}

			$('#add-answer-buttons').css('display', 'none');
			$('#mod-answer-buttons').css('display', 'block');
			isEditing = index;

			$('#add-answer-block-header').css('display', 'none');
			$('#edit-answer-block-header').css('display', 'block');
			//wasMaskHidden = AddMask.hidden();
			AddMask.show();

			unselectEditRow();
			selectEditRow(index);
		};

		var deactivateEditMode = function() {
			$('#add-answer-buttons').css('display', 'block');
			$('#mod-answer-buttons').css('display', 'none');
			isEditing = false;
			clearForm();

			$('#add-answer-block-header').css('display', 'block');
			$('#edit-answer-block-header').css('display', 'none');
			AddMask.hide();//if (wasMaskHidden) { AddMask.hide(); } else { AddMask.show(); }
			unselectEditRow();
		};

		var validateInput = function(values) {
			var output = {valid: true, message: ""};
			if (!values.answer) {
				output.valid = false; output.message = Yii.t('test', '_EMPTY_ANSWER');
			} else {
				if (modality == 'full') {
					if (!numbersPattern.test(values.scoreCorrect)) { output.valid = false; output.message = Yii.t('standard', '_OPERATION_FAILURE'); }
					else if (!numbersPattern.test(values.scoreIncorrect)) { output.valid = false; output.message = Yii.t('standard', '_OPERATION_FAILURE'); }
				}
			}
			return output;
		};


		$('#add-answer-button').on("click", function(e) {
			var input = {
				answer: (answerHtmlEditor ? $('#new-answer-text').tinymce().getContent() : $('#new-answer-text').val()).replace(/(^(&nbsp;|\s){1,})|((&nbsp;|\s){1,}$)/g,'').replace(/(^(<p>)(&nbsp;|\s){1,})/g,'<p>').replace(/(&nbsp;|\s){1,}(<\/p>)$/g,'</p>').trim(),
				correct: (modality == 'full' ? $('#new-answer-correct').attr('checked') : null),
				comment: (modality == 'full' ? $('#new-answer-comment').val() : null),
				scoreCorrect: (modality == 'full' ? $('#new-answer-score_correct').val() : null),
				scoreIncorrect: (modality == 'full' ? $('#new-answer-score_incorrect').val() : null)
			}, result = validateInput(input);
			if (result.valid) {
				addTableRow(input);
				clearForm();
			} else {
				bootbox.alert(result.message);
			}
		});

		$('#mod-answer-button').on("click", function(e) {
			if (!isEditing) return;
			var pre = 'new-answer-', $tr = $('#answers-table-input-row-'+isEditing);
			var input = {
				answer: (answerHtmlEditor ? $('#'+pre+'text').tinymce().getContent() : $('#'+pre+'text').val()).replace(/(^(&nbsp;|\s){1,})|((&nbsp;|\s){1,}$)/g,'').replace(/(^(<p>)(&nbsp;|\s){1,})/g,'<p>').replace(/(&nbsp;|\s){1,}(<\/p>)$/g,'</p>').trim()
			}
			if (modality == 'full') {
				input.correct = $('#'+pre+'correct').attr('checked');
				input.comment = $('#'+pre+'comment').val();
				input.scoreCorrect = $('#'+pre+'score_correct').val();
				input.scoreIncorrect = $('#'+pre+'score_incorrect').val();
			}
			var result = validateInput(input);
			if (!result.valid) {
				bootbox.alert(result.message);
				return;
			}

			if (modality == 'full') {
				$tr.find('td:eq(0)').html(getCorrectnessText(input.correct));
				$tr.find('td:eq(1)').html(input.answer);
				$tr.find('td:eq(3)').html(input.scoreCorrect);
				$tr.find('td:eq(5)').html(input.scoreIncorrect);
				$('#answers-'+isEditing+'-comment').val(input.comment);
			} else {
				$tr.find('td:eq(0)').html(input.answer);
			}

			var preId = 'answers-'+isEditing+'-';
			$('#'+preId+'answer').val(input.answer);
			if (modality == 'full') {
				$('#'+preId+'comment').val(input.comment);
				$('#'+preId+'correct').val(input.correct ? '1' : '0');
				$('#'+preId+'score_correct').val(input.scoreCorrect);
				$('#'+preId+'score_incorrect').val(input.scoreIncorrect);
			}

			var top = $tr.offset().top;
			$('body').animate({scrollTop: top}, 'slow');

			deactivateEditMode();
		});

		$('#undo-edit-answer-button').on("click", function(e) {
			deactivateEditMode();
		});


		var AddMask = {
			id: 'form-box-container',
			headerId: 'add-answer-link-container',
			hide: function() {
				$('#'+this.id).css('display', 'none');
				$('#'+this.headerId).css('display', 'block');
			},
			show: function() {
				$('#'+this.id).css('display', 'block');
				$('#'+this.headerId).css('display', 'none');
			},
			hidden: function() { return $('#'+this.id).css('display') == 'none'; },
			showed: function() { return $('#'+this.id).css('display') == 'block'; },
			toggle: function() {
				if (this.hidden()) {
					this.show();
				} else {
					this.hide();
				}
			},
			init: function() {
				this.hide();
				var oScope = this;
				$('#toggle-add-answer').on('click', function(e) {
					e.preventDefault();
					oScope.toggle();
				});
				$('#undo-add-answer-button').on('click', function(e) {
					oScope.hide();
				});
			}
		};

		AddMask.init();

		var checkCorrectAnswers = function() {
			var hasSome = false;
			$('#answers-tbody').find('input[id$=-correct]').each(function() {
				if ($(this).val() != 0) { hasSome = true; }
			});
			return hasSome;
		};

		var getNumAnswers = function() { return $('#answers-tbody > tr').length; }
		var checkNumAnswers = function() {
			return (getNumAnswers() >= 2);
		};

		if (Validation) {
			Validation.addValidator(checkNumAnswers, Yii.t('test', 'Invalid question number'), 1);
			if (modality == 'full') {
				Validation.addValidator(checkCorrectAnswers, Yii.t('test', 'At least one answer has to be correct'), 2);
			}
		}

		if (options.answers) {
			var Answers = $.type(options.answers) == 'array' ? options.answers : [];
			for (var i=0; i<Answers.length; i++) {
				addTableRow(Answers[i]);
			}
		}


	}

};