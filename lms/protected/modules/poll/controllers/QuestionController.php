<?php


class QuestionController extends PollBaseController {

	protected $questionType = '';

	protected $useCategory;


	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters()
	{
		return array(
				'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules()
	{
		return array(

				array('allow',
						'users'=>array('@'),
				),

				array('deny', // if some permission is wrong -> the script goes here
						'users' => array('*'),
						'deniedCallback' => array($this, 'accessDeniedCallback'),
						'message' => Yii::t('course', '_NOENTER'),
				),
		);
	}


	//get / set methods

	public function getUseCategory() {
		return FALSE;//$this->useCategory;//polls do not use questions category by specification
	}

	public function setUseCategory($value) {
		$this->useCategory = (bool)$value;
	}


	//other methods

	public function beforeAction($event) {

		$rq = Yii::app()->request;

		switch ($event->id) {
			case 'create': {
				$questionType = $rq->getParam('question_type', false);
			} break;
			case 'update':
			case 'axDelete': {
				$idQuestion = $rq->getParam('id_question', false);
				$question = LearningPollquest::model()->findByPk($idQuestion);
				if (!$question) { throw new CException('Invalid question'); }
				$questionType = $question->type_quest;
			} break;
			default: {
				return true;
			} break;
		}

		if (!LearningPollquest::isValidType($questionType)) {
			throw new CException('Invalid question');
		}

		$questionManager = PollQuestion::getQuestionManager($questionType);
		if (!$questionManager) {
			throw new CException('Invalid question');
		}

		$questionManager->setController($this);

		switch ($event->id) {
			case 'create':
			{
				$this->attachEventHandler('onAfterQuestionCreate', array($questionManager, 'create'));
				$this->attachEventHandler('onBeforeQuestionEditRender', array($questionManager, 'beforeEdit'));
				$this->attachEventHandler('onAfterQuestionEditRender', array($questionManager, 'edit'));
			}
				break;
			case 'update':
			{
				$this->attachEventHandler('onAfterQuestionUpdate', array($questionManager, 'update'));
				$this->attachEventHandler('onBeforeQuestionEditRender', array($questionManager, 'beforeEdit'));
				$this->attachEventHandler('onAfterQuestionEditRender', array($questionManager, 'edit'));
			}
				break;
			case 'axDelete':
			{
				$this->attachEventHandler('onAfterQuestionDelete', array($questionManager, 'delete'));
			}
				break;
		}

		Yii::app()->event->raise("RegisterPollQuestionHandlers", new DEvent($this, array('event' => $event)));

		return parent::beforeAction($event);
	}

	//custom events

	public function onBeforeQuestionCreate($event) {
		$this->raiseEvent('onBeforeQuestionCreate', $event);
	}

	public function onAfterQuestionCreate($event) {
		$this->raiseEvent('onAfterQuestionCreate', $event);
	}

	public function onBeforeQuestionUpdate($event) {
		$this->raiseEvent('onBeforeQuestionUpdate', $event);
	}

	public function onAfterQuestionUpdate($event) {
		$this->raiseEvent('onAfterQuestionUpdate', $event);
	}

	public function onBeforeQuestionDelete($event) {
		$this->raiseEvent('onBeforeQuestionDelete', $event);
	}

	public function onAfterQuestionDelete($event) {
		$this->raiseEvent('onAfterQuestionDelete', $event);
	}

	public function onBeforeQuestionEditRender($event) {
		$this->raiseEvent('onBeforeQuestionEditRender', $event);
	}

	public function onAfterQuestionEditRender($event) {
		$this->raiseEvent('onAfterQuestionEditRender', $event);
	}



	//other methods

	public function init() {
		parent::init();
		Yii::app()->tinymce->init();
	}



	//actions

	public function actionCreate() {

		$request = Yii::app()->request;

		$idPoll = (int)$request->getParam('id_poll', 0);

		$questionType = $request->getParam('question_type', false);
		if (!LearningPollquest::isValidType($questionType)) {
			throw new CException('Invalid question type: '.$questionType);
		}

		$object = LearningOrganization::model()->findByAttributes(array(
			'idResource' => $idPoll,
			'objectType' => LearningOrganization::OBJECT_TYPE_POLL,
			'idCourse'   => $this->getIdCourse()
		));

		$opt = $request->getParam('opt', false);

		if($opt !== false && $opt === 'centralrepo'){
			$versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
				'id_resource' => $idPoll,
				'object_type' => LearningOrganization::OBJECT_TYPE_POLL
			));

			if($versionModel){
				$object = LearningRepositoryObject::model()->findByPk($versionModel->id_object);
			}
		}
		if (!$object) {
			//throw ...
		}

		$backUrl = $this->createUrl('default/edit', array('course_id' => $this->getIdCourse(), 'id_object' => $object->getPrimaryKey(), 'opt' => $opt));

		if ($request->getParam('undo', false) !== false) {
			$this->redirect($backUrl);
		}

		if ($request->getParam('save', false) !== false) {

			$transaction = Yii::app()->db->beginTransaction();

			try {

				$questionText = Yii::app()->htmlpurifier->purify($request->getParam('question', "")); //TO DO: can't be empty

				$question = new LearningPollquest();
				$question->id_poll = $idPoll;
				$question->type_quest = $questionType;
				$question->title_quest = $questionText;
				$question->id_category = (int)$request->getParam('id_category', 0);
				$question->sequence = LearningPollquest::getPollNextSequence($idPoll);
				$question->page = LearningPollquest::getPollPageNumber($idPoll);

				$this->onBeforeQuestionCreate(new CEvent($this, array('question' => $question)));
				$rs = $question->save();
				if (!$rs) {
					Yii::log(var_export($question->getErrors(),true), CLogger::LEVEL_ERROR);
					throw new CException('Error while saving question');
				}
				$this->onAfterQuestionCreate(new CEvent($this, array('question' => $question)));

				// Automatically make the LO visible (because we added a question to it)
				if($object instanceof LearningOrganization && !$object->visible){
					$object->visible = 1;
					$object->saveNode();
				}

				$transaction->commit();
				Yii::app()->user->setFlash('success', Yii::t('standard', '_OPERATION_SUCCESSFUL'));

			} catch (CException $e) {

				$transaction->rollback();
				Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE').': '.$e->getMessage());

				Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			}

			$this->redirect($backUrl);
		}

		$this->render('edit', array(
			'isEditing' => false,
			'idPoll' => (int)$idPoll,
			'poll' => LearningPoll::model()->findByPk($idPoll),
			'backUrl' => $backUrl,
			'question' => $question,
			'questionType' => $questionType,
			'questionText' => "",
			'questionCategory' => 0,
			'opt' => $opt
		));
	}




	public function actionUpdate() {

		$request = Yii::app()->request;

		$idPoll = (int)$request->getParam('id_poll', 0);

		$idQuestion = (int)$request->getParam('id_question', 0);

		$question = LearningPollquest::model()->findByPk($idQuestion);
		if (!$question) {
			throw new CException('Invalid question');
		}

		$object = LearningOrganization::model()->findByAttributes(array(
			'idResource' => $idPoll,
			'objectType' => LearningOrganization::OBJECT_TYPE_POLL,
			'idCourse'   => $this->getIdCourse()
		));

		$opt = $request->getParam('opt', false);

		if($opt !== false && $opt === 'centralrepo'){
			$versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
				'id_resource' => $idPoll,
				'object_type' => LearningOrganization::OBJECT_TYPE_POLL
			));

			if($versionModel){
				$object = LearningRepositoryObject::model()->findByPk($versionModel->id_object);
			}
		}

		if (!$object) {
			//throw ...
		}

		$backUrl = $this->createUrl('default/edit', array('course_id' => $this->getIdCourse(), 'id_object' => $object->getPrimaryKey() , 'opt' => $opt));

		if ($request->getParam('undo', false) !== false) {
			$this->redirect($backUrl);
		}

		if ($request->getParam('save', false) !== false) {

			$transaction = Yii::app()->db->beginTransaction();

			try {

				$questionText = Yii::app()->htmlpurifier->purify($request->getParam('question', "")); //TO DO: can't be empty

				//$question->idTest = $idTest;
				//$question->type_quest = $questionType;
				$question->title_quest = $questionText;
				$question->id_category = (int)$request->getParam('id_category', 0);

				$this->onBeforeQuestionUpdate(new CEvent($this, array('question' => $question)));
				$rs = $question->save();
				if (!$rs) {
					throw new CException('Error while saving question');
				}
				$this->onAfterQuestionUpdate(new CEvent($this, array('question' => $question)));

				$transaction->commit();
				Yii::app()->user->setFlash('success', Yii::t('standard', '_OPERATION_SUCCESSFUL'));

			} catch (CException $e) {
                Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
				$transaction->rollback();
				Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
			}

			$this->redirect($backUrl);

		} else {

			$this->render('edit', array(
				'isEditing' => true,
				'idPoll' => (int)$idPoll,
				'poll' => LearningPoll::model()->findByPk($idPoll),
				'question' => $question,
				'idQuestion' => $question->id_quest,
				'questionType' => $question->type_quest,
				'questionText' => $question->title_quest,
				'questionCategory' => $question->id_category,
				'backUrl' => $backUrl,
				'opt' => $opt
			));

		}
	}



	public function actionAxDelete() {

		$request = Yii::app()->request;

		$idPoll = (int)$request->getParam('id_poll', 0);
		$poll = LearningPoll::model()->findByPk($idPoll);
		if (!$poll) {
			throw new CException('Invalid poll');
		}

		$idQuestion = (int)$request->getParam('id_question', 0);

		$question = LearningPollquest::model()->findByPk($idQuestion);
		if (!$question || $question->id_poll != $idPoll) {
			throw new CException('Invalid question');
		}
/*
		if ($request->getParam('confirm', false) === false) {
			$this->renderPartial('_delete_confirm_dialog', array(
				'idTest' => $idTest,
				'idQuestion' => $idQuestion,
				'question' => $question
			));
			Yii::app()->end();
		}
*/
		$transaction = Yii::app()->db->beginTransaction();
		$response = new AjaxResult();

		try {

			$this->onBeforeQuestionDelete(new CEvent($this, array('question' => $question)));
			if (!$question->delete()) { throw new CException('Error while deleting question'); }
			LearningPollquest::fixPollSequence($idPoll);
			LearningPollquest::fixPollPages($idPoll);
			$this->onAfterQuestionDelete(new CEvent($this));

			$transaction->commit();
			$response->setStatus(true)->setData(array('id_question' => $idQuestion));

		} catch (CException $e) {

			$transaction->rollback();
			$response->setStatus(false)->setMessage($e->getMessage());
		}

		$response->toJSON();
	}


}