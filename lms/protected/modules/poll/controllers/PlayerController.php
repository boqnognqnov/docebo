<?php

class PlayerController extends PollBaseController {

	protected $poll = NULL;

	/**
	 * @var LearningOrganization
	 */
	protected $object = NULL;

	/**
	 * @var LearningPolltrack
	 */
	protected $track = NULL;


	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters()
	{
		return array(
				'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules()
	{
		return array(

				array('allow',
						'users'=>array('@'),
				),

				array('deny', // if some permission is wrong -> the script goes here
						'users' => array('*'),
						'deniedCallback' => array($this, 'accessDeniedCallback'),
						'message' => Yii::t('course', '_NOENTER'),
				),
		);
	}


	public function getQuestionViewsPath() {
		return 'playQuestion/';
	}


	/**
	 * Register Controller specific assets
	 *
	 * (non-PHPdoc)
	 * @see PollBaseController::registerResources()
	 */
	public function registerResources() {
		parent::registerResources();
	}


	public function beforeAction($action) {

		switch ($action->id) {

			case 'keepAlive':
				// extend the session
				Yii::app()->user->getIsGuest();

				$data = array(
					'success' => true
				);

				header('Content-type: text/javascript');
				if (isset($_GET['jsoncallback'])) {
					// JSONP callback
					$jsoncallback = preg_replace('/[^a-zA-Z\._]+/', '', $_GET['jsoncallback']);
					echo $jsoncallback. '(' . CJSON::encode($data) . ')';
				} else {
					// standard Json
					echo CJSON::encode($data);
				}

				// Update course+user session tracking to register user's course activity (time in course)
				if (isset($_REQUEST['course_id']) && isset($_REQUEST['id_poll']))
					ScormApiBase::trackCourseSession((int)Yii::app()->user->idst, $_REQUEST['course_id']);

				Yii::app()->end();
			break;

			default: {

				$idCourse = $this->getIdCourse();
				$idPoll = Yii::app()->request->getParam('id_poll', 0);
				$poll = LearningPoll::model()->findByPk($idPoll);
				$object = LearningOrganization::model()->findByAttributes(array(
					'idResource' => $idPoll,
					'objectType' => LearningOrganization::OBJECT_TYPE_POLL,
					'idCourse'   => $idCourse
				));

				if (!$poll || !$object)
					throw new CException('Invalid test');

				$idUser = Yii::app()->user->id;
				$object = $object->getMasterForCentralLo($idUser);

				$this->poll = $poll;
				$this->object = $object;
				$this->beginTrack();
			} break;
		}

		return parent::beforeAction($action);
	}



	protected function beginTrack() {
		if (!$this->track) {
			$idPoll = $this->poll->getPrimaryKey();
			$idUser = Yii::app()->user->id;

			if($this->object->repositoryObject && $this->object->repositoryObject->shared_tracking)
				$trackInfo = LearningPolltrack::model()->findByAttributes(array('id_poll' => $idPoll, 'id_user' => $idUser));
			else
				$trackInfo = LearningPolltrack::model()->findByAttributes(array('id_poll' => $idPoll, 'id_user' => $idUser, 'id_reference' => $this->object->getPrimaryKey()));

			if (!$trackInfo) {
				$trackInfo = new LearningPolltrack();
				$trackInfo->id_poll = $idPoll;
				$trackInfo->id_user = $idUser;
				$trackInfo->id_reference = $this->object->getPrimaryKey();
				$trackInfo->date_attempt = Yii::app()->localtime->toLocalDateTime();
				$trackInfo->status = LearningPolltrack::STATUS_NOT_COMPLETE;
				if (!$trackInfo->save()) {
					throw new CException('Error while saving tracking info');
				}
			}
			$this->track = $trackInfo;
		}
	}



	protected function resetTrack() {

		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {

			$questions = LearningPollquest::model()->findByAttributes(array('id_poll' => $this->poll->getPrimaryKey()));
			if (!empty($questions)) {
				foreach ($questions as $question) {
					$questionManager = PollQuestion::getQuestionManager($question->type_quest);
					if ($questionManager && method_exists($questionManager, 'deleteUserAnswer')) {
						$questionManager->deleteUserAnswer($this->track->getPrimaryKey(), $question->getPrimaryKey());
					}
				}
			}

			$newTrackInfo = array(
				'date_attempt' => Yii::app()->localtime->toLocalDateTime(),
				'status' => LearningPolltrack::STATUS_NOT_COMPLETE
			);
			$this->track->setAttributes($newTrackInfo);
			if (!$this->track->save()) {
				throw new CException('Error while resetting track info');
			}

			if (isset($transaction)) { $transaction->commit(); }

		} catch (CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}
	}


	// actions

	public function actionPlay() {

		$rq = Yii::app()->request;

		if ($rq->getParam('show_result', false) !== false) {

			// continue a test completed, show the result
			$this->results();

		} else {

			$this->play();
		}

	}


	public function actionResults() {
		$this->results();
	}



	//---------------------------------------------------
	protected function play() {

		$rq = Yii::app()->request;
		$transaction = Yii::app()->db->beginTransaction();
		$idUser = Yii::app()->user->id;
		if ($this->track->status == LearningPolltrack::STATUS_VALID) {
			throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
		}

		$now = date("Y-m-d H:i:s");

		try {


			//number of test pages
			$totalPages = $this->poll->getTotalPageNumber();

			// find the page to display
			$previousPage = $rq->getParam('previous_page', false);
			if ($previousPage === false) {

				$continue = $rq->getParam('continue', false);
				$pageContinue = $rq->getParam('page_continue', false);
				if ($pageContinue !== false && $continue !== false) {
					$pageToDisplay = $pageContinue;
				} else {
					$pageToDisplay = 1;
				}

			} else {

				$pageToDisplay = $previousPage;
				$nextPage = $rq->getParam('next_page', false);
				$prevPage = $rq->getParam('prev_page', false);
				if ($nextPage !== false) { $pageToDisplay++; }
				if ($prevPage !== false) { $pageToDisplay--; }

			}

			$newInfo = array(
				'status' => LearningPolltrack::STATUS_NOT_COMPLETE
			);

			$pageToSave = $rq->getParam('page_to_save', false);
			if ($pageToSave !== false) {

				$this->track->storePage($pageToSave);
			}

			$this->track->setAttributes($newInfo);
			$rs = $this->track->update();
			if ($rs === false) {
				throw new CException('Error in tracking update');
			}
/*
			// save page track info
			if (!$this->track->updateTrackForPage($pageToDisplay)) {
				throw new CException('Error in page tracking update');
			}
*/
			//$quest_sequence_number = $this->test->getInitQuestionSequenceNumberForPage($pageToDisplay);
			//$query_question = $this->test->getQuestionsForPage($pageToDisplay, $idUser);
			//$time_in_test = $this->track->userTimeInTheTest();

			$questions = $this->poll->getQuestionsForPage($pageToDisplay, $idUser);
			if (empty($questions)) {
				throw new CException('Error in question retrieval');
			}



			$this->render('play', array(
				'idPoll' => (int)$idPoll,

				'pollInfo' => $this->poll,
				'trackInfo' => $this->track,

				'pageToDisplay' => $pageToDisplay,
				'totalPages' => $totalPages,

				'questions' => $questions,
				'startQuestionIndex' => $this->poll->getInitQuestionSequenceNumberForPage($pageToDisplay)
			));

			$transaction->commit();

		} catch(CException $e) {

			$transaction->rollback();
			echo $e->getMessage();
		}
	}



	public function results() {

		$db = Yii::app()->db;
		$rq = Yii::app()->request;
		if ($this->track->status == LearningPolltrack::STATUS_VALID) {
			throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
		}
		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {

			$showReview = $rq->getParam('show_review', false);

			$previousPage = $rq->getParam('previous_page', false);
			$pageToSave = $rq->getParam('page_to_save', false);

			if ($pageToSave !== false) {
				$this->track->storePage($pageToSave);
			}

			$newTrackInfo = array(
				'date_attempt' => Yii::app()->localtime->toLocalDateTime(),
				'status' => LearningPolltrack::STATUS_VALID
			);

			$this->track->setAttributes($newTrackInfo); //TO DO: check commontrack status too, date_attempt and status
			if (!$this->track->save()) {
				throw new CException('Error while updating tracking info');
			}

			$stdBackUrl = PluginManager::isPluginActive('ClassroomApp') && $this->courseModel->isClassroomCourse()
				? $this->createUrl('//player/training/session', array('course_id' => $this->getIdCourse(), 'mode' => 'view_course'))
				: $this->createUrl('//player/training', array('course_id' => $this->getIdCourse(), 'mode' => 'view_course'));


			$backUrl = $rq->getParam('back_url', $stdBackUrl);

			$this->render('results', array(
				'backUrl' => $backUrl
			));

			if (isset($transaction)) { $transaction->commit(); }

		} catch(CException $e) {
			//...
			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}
	}

	public function actionKeepAlive() {

	}

	public function getPoll()
	{
		return $this->poll;
	}
}