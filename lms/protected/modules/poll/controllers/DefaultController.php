<?php

class DefaultController extends PollBaseController {

	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters()
	{
		return array(
				'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules()
	{
		return array(

				array('allow',
						'users'=>array('@'),
				),

				array('deny', // if some permission is wrong -> the script goes here
						'users' => array('*'),
						'deniedCallback' => array($this, 'accessDeniedCallback'),
						'message' => Yii::t('course', '_NOENTER'),
				),
		);
	}


	public function init() {
		parent::init();
		Yii::app()->tinymce->init();
	}


	public function formatQuestionText($question, $length = 0) {
		$output =  html_entity_decode(strip_tags($question, "<p><ul><ol><li>"), ENT_QUOTES, 'UTF-8');
		if ($length > 3 && mb_strlen($output, 'UTF-8') > $length) {
			$output = mb_substr($output, 0, $length - 3, 'UTF-8').'...';
		}
		return $output;
	}


	//actions

	public function actionIndex()
	{
		$this->render('index');
	}



	public function actionAxSaveLo() {
		$courseId = Yii::app()->request->getParam("course_id", null);
		$parentId = Yii::app()->request->getParam("parent_id", null);
		$title = trim(Yii::app()->request->getParam("title", null));
		$description = Yii::app()->request->getParam("description", null);

		$shortDescription = Yii::app()->request->getParam('short_description', '');
		$thumb = Yii::app()->request->getParam('thumb', null);

		$response = new AjaxResult();
		$transaction = Yii::app()->db->beginTransaction();

		try {
			// Validate input parameters
			if (!$courseId) { throw new CException("Invalid specified course."); }
			if (!is_numeric($parentId) || $parentId < 0) { $parentId = 0; }
			if (empty($title)) { throw new CException(Yii::t('error', 'Enter a title!')); }

			//prepare AR object to be saved
			$pollObj = new LearningPoll();
			$pollObj->author = Yii::app()->user->getIdst();
			$pollObj->title = $title;
			$pollObj->description = Yii::app()->htmlpurifier->purify($description);

			//these will be needed by learning organization table
			$pollObj->setIdCourse($courseId);
			$pollObj->setIdParent($parentId);

			//do actual saving
			if (!$pollObj->save()) {
				throw new CException('Error while saving the poll.');
			}

			//verify inserted data
			$resourceId = (int)$pollObj->getPrimaryKey();
			$learningObject = LearningOrganization::model()->findByAttributes(
				array(
					'idResource' => $resourceId,
					'objectType' => LearningOrganization::OBJECT_TYPE_POLL,
					'idCourse'   => $courseId
				)
			);
			if (!$learningObject) { throw new CException("Invalid learning object(".$resourceId.")"); }

			// The survey is invisible/unpublished by default (until we add some questions to it)
			$learningObject->visible = 0;
//			if($shortDescription)
				$learningObject->short_description = Yii::app()->htmlpurifier->purify($shortDescription);
			if($thumb)
				$learningObject->resource = intval($thumb);

			$learningObject->saveNode();

			// DB Commit
			$transaction->commit();

			// Trigger event to let plugins save their own data from POST
			Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
				'loModel' => $learningObject
			)));

			$data = array(
					'resourceId' => $resourceId,
					'organizationId' => (int)$learningObject->getPrimaryKey(),
					'parentId' => (int)$parentId,
					'title' => $title,
					'message' => Yii::t('standard', '_OPERATION_SUCCESSFUL')
			);
			$response->setData($data)->setStatus(true);

		} catch (CException $e) {
			$transaction->rollback();
			$response->setMessage($e->getMessage())->setStatus(false);
		}

		$response->toJSON();
		Yii::app()->end();
	}



	/**
	 * Show LO launchpad
	 */
	public function actionAxLaunchpad() {
		$response = new AjaxResult();

		$rq = Yii::app()->request;
		$idObject = (int)$rq->getParam('id_object', 0);
		$mainInfo = LearningOrganization::model()->findByPk($idObject);
		if (!$mainInfo)
			throw new CException('Invalid specified object');

		$idPoll = $mainInfo->idResource;
		$pollInfo = LearningPoll::model()->findByPk($idPoll);
		if (!$pollInfo)
			throw new CException('Invalid specified poll');

		if($mainInfo->repositoryObject && $mainInfo->repositoryObject->shared_tracking) {
			$trackInfo = LearningPolltrack::model()->findByAttributes(array(
				'id_user' => Yii::app()->user->id,
				'id_poll' => $idPoll
			));
		} else {
			$trackInfo = LearningPolltrack::model()->findByAttributes(array(
				'id_user' => Yii::app()->user->id,
				'id_poll' => $idPoll,
				'id_reference' => $idObject
			));
		}

		$courseUserModel = LearningCourseuser::model()->findByAttributes(array('idCourse' => $this->getIdCourse(), 'idUser' => Yii::app()->user->id));

		//load content
		$html = $this->renderPartial("_launchpad", array(
			'mainInfo' => $mainInfo,
			'pollInfo' => $pollInfo,
			'trackInfo' => $trackInfo,
			'courseUserModel' => $courseUserModel
		), true);

		$response->setHtml($html);
		$response->setStatus(true);

		//send result
		$response->toJSON();
		Yii::app()->end();

	}

	public function actionAxCreatePoll() {

		$isListViewLayout = false;
		if($this->getIdCourse())
			$isListViewLayout = LearningCourse::getPlayerLayout($this->getIdCourse())==LearningCourse::$PLAYER_LAYOUT_LISTVIEW;

		$response = new AjaxResult();
		$html = $this->renderPartial("_create_poll", array(
			'lo_type'=>LearningOrganization::OBJECT_TYPE_POLL,
			'poll'=>null,
			'loModel'=>null,
			'idCourse'=>$this->getIdCourse(),
			'isListViewLayout' => $isListViewLayout
		), true, true);
		$response->setStatus(true)->setHtml($html)->toJSON();
		Yii::app()->end();

	}

	public function actionAxEditPoll() {

		$rq = Yii::app()->getRequest();
		$idPoll = $rq->getParam('id_poll', null);

		$poll = LearningPoll::model()->findByPk($idPoll);
		$object = LearningOrganization::model()->findByAttributes(array(
			'idResource' => $idPoll,
			'objectType' => LearningOrganization::OBJECT_TYPE_POLL,
			'idCourse'   => $this->getIdCourse()
		));

		$opt = $rq->getParam('opt', false);

		if($opt !== false && $opt == 'centralrepo'){
			$versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
				'id_resource' => $idPoll,
				'object_type' => LearningOrganization::OBJECT_TYPE_POLL
			));

			if($versionModel){
				$object = LearningRepositoryObject::model()->findByPk($versionModel->id_object);
			}
		}

		if ($rq->getParam('confirm', false) !== false) {
			$poll->title = $rq->getParam('title', "");
			$poll->description = $rq->getParam('description', "");

//			if($rq->getParam('short_description', "")){
				$object->short_description = Yii::app()->htmlpurifier->purify($rq->getParam('short_description', ""));
//			}
			$thumb = $rq->getParam('thumb', "");
			if($thumb){
				$object->resource = intval($thumb);
			}elseif($object->resource){
				$object->resource = 0;
			}

			$shortDescription = $rq->getParam('short_description');
			$thumb = $rq->getParam('thumb');

			if (!$poll->save()) {
				echo '<a class="auto-close error"></a>';
				return;
			}else{
				$object->title = $poll->title;

				if($shortDescription)
					$object->short_description = Yii::app()->htmlpurifier->purify($shortDescription);

				if($thumb)
					$object->resource = intval($thumb);

				if($object instanceof LearningOrganization){
					$object->saveNode();
				} else if($object instanceof LearningRepositoryObject){
					$object->save();
				}


				// Trigger event to let plugins save their own data from POST
				Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
					'loModel' => $object
				)));

				echo CHtml::link('', FALSE, array(
					'class' => 'auto-close success',
					'data-new_title' => $poll->title
				));
				return;
			}
		}

		$isListViewLayout = false;
		if($this->getIdCourse())
			$isListViewLayout = LearningCourse::getPlayerLayout($this->getIdCourse())==LearningCourse::$PLAYER_LAYOUT_LISTVIEW;

		$html = $this->renderPartial("_edit_poll", array(
			'lo_type'=>LearningOrganization::OBJECT_TYPE_POLL,
			'poll'=>$poll,
			'loModel'=>$object,
			'description'=>$poll->description,
			'idCourse'=>$this->getIdCourse(),
			'isListViewLayout' => $isListViewLayout,
			'opt' => $opt
		), true, true);
		echo $html;

	}





	/**
	 * Register Controller specific assets
	 *
	 * (non-PHPdoc)
	 * @see PollBaseController::registerResources()
	 */
	public function registerResources() {
		parent::registerResources();

		if (!Yii::app()->request->isAjaxRequest) {
			/* @var $cs CClientScript */
			$cs = Yii::app()->getClientScript();
			//$cs->registerCssFile($this->module->assetsUrl . '/css/testpoll.css');
		}
	}




	public function actionEdit() {

		// DataTables
		$cs = Yii::app()->getClientScript();
		$assetsUrl = $this->getPlayerAssetsUrl();//$this->module->assetsUrl;
		$cs->registerScriptFile($assetsUrl.'/js/jquery.dataTables.min.js');
		$cs->registerCssFile($assetsUrl.'/css/datatables.css');
		$cs->registerScriptFile($assetsUrl.'/js/testpoll.datatable_reordering.js');

		//load some external scripts for editing actions (LO info additional info custom thumbnails management)
		$basePath = Yii::getPathOfAlias('admin').'/protected';
		$assetsPath = Yii::app()->getAssetManager()->publish($basePath.'/../js/');
		$cs->registerScriptFile($assetsPath.'/scriptTur.js');
		$cs->registerScriptFile($assetsPath.'/scriptModal.js');
		$cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/Jcrop/jquery.Jcrop.min.js');
		$cs->registerCssFile(Yii::app()->theme->baseUrl.'/js/Jcrop/jquery.Jcrop.min.css');

		$idObject = (int)Yii::app()->request->getParam('id_object', 0);
		$object = LearningOrganization::model()->findByPk($idObject);

		$opt = Yii::app()->request->getParam('opt', false);

		if($opt !== false && $opt === 'centralrepo'){
			$object = LearningRepositoryObject::model()->findByPk($idObject);
		}



		if (!$object) {
			throw new CException('Invalid specified poll');
		}

		$poll = null;

		if($object instanceof LearningOrganization){
			$poll = LearningPoll::model()->findByPk($object->idResource);
		} else if($object instanceof LearningRepositoryObject){
			$versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
				'id_object' => $object->id_object
			));

			if($versionModel){
				$poll = LearningPoll::model()->findByPk($versionModel->id_resource);
			}
		}
		if (!$poll) {
			throw new CException('Invalid specified poll');
		}

		$this->render("edit", array(
			'idPoll' => $poll->getPrimaryKey(),
			'poll' => $poll,
			'object' => $object,
			'opt' => $opt
		));
	}



	public function actionAxGetQuestionsList() {
		$request = Yii::app()->request;
		$idPoll = $request->getParam('id_poll', 0);

		//try to prevent any possible sequence error
		LearningPollquest::fixPollSequence($idPoll);

		$criteria = new CDbCriteria();
		$criteria->order = 'sequence ASC';
		$questions = LearningPollquest::model()->findAllByAttributes(array('id_poll' => $idPoll), $criteria);

		$records = array();
		foreach ($questions as $question) {
			$records[] = array(
				'id_question' => (int)$question->id_quest,
				'id_category' => (int)$question->id_category,
				'sequence' => (int)$question->sequence,
				'page' => (int)$question->page,
				'question' => $this->formatQuestionText($question->title_quest, 80),//strip_tags($question->title_quest),
				'type' => $question->type_quest
			);
		}

		echo CJSON::encode(array('aaData' => $records));
	}



	/**
	 * Change question sequence
	 * @throws CException
	 */
	public function actionAxReorderQuestion() {

		$request = Yii::app()->request;

		$idPoll = $request->getParam('id_poll', 0);

		$qid = $request->getParam('id');
		$direction = $request->getParam('direction');
		$fromPosition = $request->getParam('fromPosition', -1); //sequence number of source
		$toPosition = $request->getParam('toPosition', -1); //sequence number of destination

		if ($fromPosition < 0 || $toPosition < 0) {
			throw new CException('Invalid position specification');
		}

		$checkDestination = LearningPollquest::model()->findAllByAttributes(array('id_poll' => $idPoll, 'sequence' => $toPosition));

		if (empty($checkDestination) || count($checkDestination) != 1) {
			throw new CException('Invalid sequence index');
		}
		$destination = $checkDestination[0];

		$arr = explode('-', $qid);
		$idQuestion = (int)end($arr);
		$source = LearningPollquest::model()->findByPk($idQuestion);
		if (!$source) { throw new CException('Invalid question'); }

		$db = Yii::app()->db;
		$transaction = $db->beginTransaction();

		$ajaxResult = new AjaxResult();

		try {

			switch ($direction) {

				case 'back': {
					$newSequence = $destination->sequence;

					$command = $db->createCommand("UPDATE learning_pollquest
						SET sequence = sequence+1
						WHERE id_poll = :id_poll
						AND sequence >= :sequence_begin
						AND sequence <= :sequence_end
						AND id_quest <> :id_question");
					$command->bindParam(':id_poll', $idPoll, PDO::PARAM_INT);
					$command->bindParam(':sequence_begin', $toPosition, PDO::PARAM_INT);
					$command->bindParam(':sequence_end', $fromPosition, PDO::PARAM_INT);
					$command->bindParam(':id_question', $source->id_quest, PDO::PARAM_INT);
					$rs = $command->query();
					if (!$rs) { throw new CException('Error while executing operation'); }

					$source->sequence = $newSequence;
					$rs = $source->save();
					if (!$rs) { throw new CException('Error while executing operation'); }

					if (!LearningPollquest::fixPollPages($idPoll)) {
						throw new CException('Error while executing operation');
					}

				} break;

				case 'forward': {

					$newSequence = $destination->sequence;

					$command = $db->createCommand('UPDATE learning_pollquest
						SET sequence = sequence-1
						WHERE id_poll = :id_poll
						AND sequence <= :sequence_begin
						AND sequence >= :sequence_end
						AND id_quest <> :id_question');
					$command->bindParam(':id_poll', $idPoll, PDO::PARAM_INT);
					$command->bindParam(':sequence_begin', $toPosition, PDO::PARAM_INT);
					$command->bindParam(':sequence_end', $fromPosition, PDO::PARAM_INT);
					$command->bindParam(':id_question', $source->id_quest, PDO::PARAM_INT);
					$rs = $command->query();
					if (!$rs) { throw new CException('Error while executing operation'); }

					$source->sequence = $newSequence;
					$rs = $source->save();
					if (!$rs) { throw new CException('Error while executing operation'); }

					if (!LearningPollquest::fixPollPages($idPoll)) {
						throw new CException('Error while executing operation');
					}

				} break;

				default: { throw new CException('Invalid specified move direction'); } break;
			}

			$transaction->commit();

			$ajaxResult->setStatus(true);

		} catch (CException $e) {

			$transaction->rollback();
			$ajaxResult->setStatus(false)->setMessage(Yii::t('standard', '_OPERATION_FAILURE'));
			//throw $e;
		}

		$ajaxResult->toJSON();
	}



	/**
	 * Change poll title and description
	 */
	public function actionAxEditTitle() {

		$rq = Yii::app()->request;
		$idPoll = $rq->getParam('id_poll', 0);
		$poll = LearningPoll::model()->findByPk($idPoll);
		$object = LearningOrganization::model()->findByAttributes(
			array(
				'idResource' => $idPoll,
				'objectType' => LearningOrganization::OBJECT_TYPE_POLL,
				'idCourse'   => $this->getIdCourse()
			)
		);

		if ($rq->getParam('confirm', false) !== false) {
			$poll->title = $rq->getParam('title', "");
			$poll->description = $rq->getParam('description', "");

			$shortDescription = $rq->getParam('short_description');
			$thumb = $rq->getParam('thumb');

			if (!$poll->save()) {
				echo '<a class="auto-close error"></a>';
				return;
			} else {

				$object->title = $poll->title;

				if($shortDescription)
					$object->short_description = Yii::app()->htmlpurifier->purify($shortDescription);

				if($thumb)
					$object->resource = intval($thumb);

				$object->saveNode();

				echo CHtml::link('', FALSE, array(
					'class' => 'auto-close success',
					'data-new_title' => $poll->title
				));
				return;
			}
		}

		$this->renderPartial('_edit_title', array(
			//'idPoll' => $idPoll,
			'poll' => $poll
		));

	}

}