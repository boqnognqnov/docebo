<?php

class PollQuestionInlineChoice extends PollQuestionWithAnswers {


	public function __construct() {
		parent::__construct();
		$this->setType(LearningTestquest::QUESTION_TYPE_INLINE_CHOICE);
		$this->setEditProperty('category', true);
		$this->setEditProperty('difficulty', true);
		$this->setEditProperty('shuffle', true);
		$this->setEditProperty('answeringMaxTime', true);
		$this->setTransformation('title', array($this, 'transformTitle'));
	}

	public function getAnswerTag() {
		return '[answer]';
	}

	public function transformTitle($inputData, $other) {
		$questionManager = $other->questionManager;
		$idQuestion = (int)$other->idQuestion;
		$poll = $this->_controller->getPoll();
		$pollId = ($poll) ? $poll->id_poll : null;

		$userAnswer = $questionManager->recoverUserAnswer($idQuestion, Yii::app()->user->id, $pollId);
		$answerValue = (!empty($userAnswer) ? array_pop($userAnswer)->id_answer : false);
		//retrieve all question answers
		$answersList = array();
		if(Settings::get('no_answer_in_test', 'off') == 'on')
			$answersList[0] = Yii::t('standard', '_NO_ANSWER');
		$answersData = $this->getAnswers($idQuestion);
		//$test = $this->_controller->getTest();
		if (!empty($answersData)) {
			//checking shuffle option ...
			//$question = LearningPollquest::model()->findByPk($idQuestion);

			//if ($question->sequence) { shuffle($answersData); }
			//make answers list for dropdown
			foreach ($answersData as $answer) {
				$answersList[(int)$answer->getPrimaryKey()] = CHtml::decode(strip_tags($answer->answer));
			}
		}

		// custom plugin set property disabled if test is in study mode
		if ($answerValue == false) {
			$answerValue = 1;
		}

		//make dropdown
		$dropDown = CHtml::dropDownList(
			$questionManager->getInputName($idQuestion).'[]',
			$answerValue,
			$answersList,
			array(
				'id' => $questionManager->getInputId($idQuestion)
			)
		);
		if (strpos($inputData, $questionManager->getAnswerTag()) === false) {
			return $inputData.'<br />'.$dropDown;
		}
		return str_replace(
			$questionManager->getAnswerTag(),
			$dropDown,
			$inputData
		);
	}


	public function play($idQuestion) {
		//NOTE: see "transformTitle" method
	}


	public function edit($event) {
		$controller = $this->getController();
		$cs = Yii::app()->getClientScript();
		$cs->registerCoreScript('jquery.ui');
		$assetsUrl = $controller->getPlayerAssetsUrl();
		// $cs->registerCssFile($assetsUrl.'/css/player-sprite.css');  // moved to themes
		$cs->registerScriptFile($assetsUrl.'/js/testpoll.edit_question_choice.js');
		$controller->renderPartial('edit/_'.$this->getType(), array(
			'questionManager' => $this,
			'isEditing' => $event->params['isEditing'],
			'idQuestion' => $event->params['idQuestion']
		));
	}

}