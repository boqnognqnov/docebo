<?php

class PollQuestionEvaluation extends PollQuestionWithAnswers {

	const MAX_SCORE = 5;

	public function __construct() {
		parent::__construct();
		$this->setType(LearningPollquest::POLL_TYPE_EVALUATION);
		$this->setEditProperty('category', true);
	}

	public function play($idQuestion) {
		$answers = $this->_getAnswersList($idQuestion);
		$controller = $this->getController();
		$controller->renderPartial($this->getQuestionViewsPath().$this->getType(), array(
			'questionManager' => $this,
			'idQuestion' => $idQuestion,
			'answers' => $answers
		));
	}

	public function edit($event) {
		$controller = $this->getController();
		$cs = Yii::app()->getClientScript();
		$cs->registerCoreScript('jquery.ui');
		$assetsUrl = $controller->getPlayerAssetsUrl();
		//$cs->registerCssFile($assetsUrl.'/css/player-sprite.css');   // moved to themes
		$cs->registerScriptFile($assetsUrl.'/js/testpoll.edit_question_choice.js');
		$controller->renderPartial('edit/_'.$this->getType(), array(
			'questionManager' => $this,
			'question' => isset($event->params['question']) ? $event->params['question'] : new LearningPollquest(),
			'isEditing' => $event->params['isEditing'],
			'idQuestion' => $event->params['idQuestion']
		));
	}
	
	public function _getAnswersList($idQuestion) {
		$criteria = new CDbCriteria();
		$criteria->order = 'sequence ASC';
		$answers = LearningPollquestanswer::model()->findAllByAttributes(array('id_quest' => $idQuestion), $criteria);
		if (!is_array($answers)) { return false; }
		$index = 0;
		$output = array();
		foreach ($answers as $answer) {
			$output[] = array(
				'index' => $index,
				'sequence' => (int)$answer->sequence,
				'id_answer' => (int)$answer->id_answer,
				'answer' => $answer->answer,
				'answer_stripped' => strip_tags($answer->answer)
			);
			$index++;
		}
		return $output;
	}

	public function update($event) {
		/** @var LearningPollquest $question */
		$question = $event->params['question'];

		// the question has been saved and the 'settings' field has been serialized
		// therefore we need to unserialize it
		$settings = $question->unserializeSettings();

		// update max_score with the data from the post request
		$max_score = intval(Yii::app()->request->getParam('max_score'));
		if ($max_score < 1 || $max_score > 100)
			$max_score = self::MAX_SCORE;

		$settings['max_score'] = $max_score;

		// update the question's 'settings' field
		$question->settings = $settings;
		$question->save();

		/**
		 * Now let's update the answers
		 */
		$db = Yii::app()->db;

		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {

			$savedAnswers = array();

			/**
			 * Delete old answers and recreate them
			 */
			$oldAnswers = LearningPollquestanswer::model()->findAllByAttributes(array('id_quest' => $question->id_quest));
			foreach ($oldAnswers as $oldAnswer) {
				if (!$oldAnswer->delete()) {
					throw new CException('Error while updating answers');
				}
				$this->onBeforeAnswerDelete(new CEvent($this, array('answer' => $oldAnswer, 'action' => 'delete')));
			}

			for ($i=0; $i<$max_score; $i++) {
				$ar = new LearningPollquestanswer();
				$ar->id_quest = $question->id_quest;
				$ar->answer = $i+1;
				$ar->sequence = $i+1;

				$this->onBeforeAnswerCreate(new CEvent($this, array('answer' => $ar, 'action' => 'create')));
				if (!$ar->save()) {
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				}
				$this->onAfterAnswerCreate(new CEvent($this, array('answer' => $ar, 'action' => 'create')));
			}

			if (isset($transaction)) { $transaction->commit(); }

		} catch (CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}
	}

	public function create($event) {
		/** @var LearningPollquest $question */
		$question = $event->params['question'];

		// the question has been saved and the 'settings' field has been serialized
		// therefore we need to unserialize it
		$settings = $question->unserializeSettings();

		// update max_score with the data from the post request
		$max_score = intval(Yii::app()->request->getParam('max_score'));
		if ($max_score < 1 || $max_score > 100)
			$max_score = self::MAX_SCORE;

		$settings['max_score'] = $max_score;

		// update the question's 'settings' field
		$question->settings = $settings;
		$question->save();

		/**
		 * Now let's save the answers
		 */
		$db = Yii::app()->db;

		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {

			$question = $event->params['question'];

			for ($i=0; $i<$max_score; $i++) {
				$ar = new LearningPollquestanswer();
				$ar->id_quest = $question->id_quest;
				$ar->answer = $i+1;
				$ar->sequence = $i+1;

				$this->onBeforeAnswerCreate(new CEvent($this, array('answer' => $ar, 'action' => 'create')));
				if (!$ar->save()) {
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				}
				$this->onAfterAnswerCreate(new CEvent($this, array('answer' => $ar, 'action' => 'create')));
			}

			if (isset($transaction)) { $transaction->commit(); }

		} catch (CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}
	}
}