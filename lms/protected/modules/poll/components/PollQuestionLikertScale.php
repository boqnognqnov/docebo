<?php

class PollQuestionLikertScale extends PollQuestionWithAnswers {

    public function __construct() {
        parent::__construct();
        $this->setType(LearningPollquest::POLL_TYPE_LIKERT_SCALE);
        $this->setEditProperty('category', true);
    }

    public function play($idQuestion) {
        $answers = $this->_getAnswersList($idQuestion);
        $controller = $this->getController();
        $poll = Yii::app()->request->getParam('id_poll', null);
        $controller->renderPartial($this->getQuestionViewsPath().$this->getType(), array(
            'questionManager' => $this,
            'idQuestion' => $idQuestion,
            'answers' => $answers,
            'id_poll' => $poll
        ));
    }

    public function edit($event) {
        $controller = $this->getController();
        $poll = Yii::app()->request->getParam('id_poll', null);
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery.ui');
        $assetsUrl = $controller->getPlayerAssetsUrl();
        $cs->registerScriptFile($assetsUrl.'/js/testpoll.edit_question_likert_scale.js');
        $controller->renderPartial('edit/_'.$this->getType(), array(
            'questionManager' => $this,
            'isEditing' => $event->params['isEditing'],
            'idQuestion' => $event->params['idQuestion'],
            'id_poll' => $poll
        ));
    }

    public function create($event) {

        $db = Yii::app()->db;
        $questions = $this->getQuestionsInput();

        if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

        try {
            // Manage the questions
            $question = $event->params['question'];
            foreach ($questions as $sequence => $answer) {
                $ar = new LearningPollquestanswer();
                $ar->id_quest = $question->id_quest;
                $ar->answer = Yii::app()->htmlpurifier->purify($answer['question']);
                $ar->sequence = $answer['sequence'];

                $this->onBeforeAnswerCreate(new CEvent($this, array('answer' => $ar, 'action' => 'create')));
                if (!$ar->save()) {
                    throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
                }
                $this->onAfterAnswerCreate(new CEvent($this, array('answer' => $ar, 'action' => 'create')));
            }

            // Manage the Likert Scale
            $this->manageLikertScale($question->id_poll);

            if (isset($transaction)) { $transaction->commit(); }

        } catch (CException $e) {

            if (isset($transaction)) { $transaction->rollback(); }
            throw $e;
        }

    }

    public function update($event) {

        $db = Yii::app()->db;
        $questions = $this->getQuestionsInput();

        if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

        try {

            $question = $event->params['question'];

            $checkList = array();
            $keepList = array();

            foreach ($questions as $quest) {
                if (isset($quest['id_question']) && (int)$quest['id_question'] > 0) {
                    $checkList[] = (int)$quest['id_question'];
                }
            }

            $oldQuestions = LearningPollquestanswer::model()->findAllByAttributes(array('id_quest' => $question->id_quest));
            foreach ($oldQuestions as $oldQuestion) {
                $toCheck = (int)$oldQuestion->getPrimaryKey();
                if (!in_array($toCheck, $checkList)) {
                    if (!$oldQuestion->delete()) {
                        throw new CException('Error while updating answers');
                    }
                } else {
                    $keepList[$toCheck] = $oldQuestion;
                }
            }

            foreach ($questions as $quest) {

                $action = false;
                if (isset($quest['id_question']) && $quest['id_question'] > 0) {
                    $index = (int)$quest['id_question'];
                    if (!isset($keepList[$index])) { throw new CException('Error while updating answers'); }
                    $ar = $keepList[$index];
                    $action = 'update';
                } else {
                    $ar = new LearningPollquestanswer();
                    $ar->id_quest = $question->id_quest;
                    $action = 'create';
                }

                $ar->answer = Yii::app()->htmlpurifier->purify($quest['question']);
                $ar->sequence = $quest['sequence'];

                switch ($action) {
                    case 'create': { $this->onBeforeAnswerCreate(new CEvent($this, array('answer' => $ar, 'action' => 'update'))); } break;
                    case 'update': { $this->onBeforeAnswerUpdate(new CEvent($this, array('answer' => $ar, 'action' => 'update'))); } break;
                }
                if (!$ar->save()) {
                    throw new CException('Error while updating answers');
                }
                switch ($action) {
                    case 'create': { $this->onAfterAnswerCreate(new CEvent($this, array('answer' => $ar, 'action' => 'update'))); } break;
                    case 'update': { $this->onAfterAnswerUpdate(new CEvent($this, array('answer' => $ar, 'action' => 'update'))); } break;
                }
            }

            // Manage the likert scale
            $this->manageLikertScale($question->id_poll);

            if (isset($transaction)) { $transaction->commit(); }

        } catch (CException $e) {

            if (isset($transaction)) { $transaction->rollback(); }
            throw $e;
        }

    }

    public function storeUserAnswer($idTrack, $idQuestion, $answersInput, $canOverwrite) {

        if (!is_array($answersInput)) {
            $answersInput = $this->readAnswerInput($idQuestion);
            if (is_array($answersInput) && !$this->useMultipleChoice() && count($answersInput) > 1) {
                //error ...
            }
        }


        if ($this->userDoAnswer($idTrack, $idQuestion)) {
            if (!$canOverwrite) { return true; }
            $this->deleteUserAnswer($idTrack, $idQuestion);
        }

        $storedAnswers = $this->getAnswers($idQuestion);
        if (empty($storedAnswers)) { return; } //TO DO: what to do now?
        foreach ($storedAnswers as $storedAnswer) {

            $insert = false;
            if (is_array($answersInput) && isset($answersInput[$storedAnswer->id_answer])) {
                //answer checked by the user
                $insert = true;
            }

            if ($insert) {
                $newTrack = new LearningPolltrackAnswer();
                $newTrack->id_track = $idTrack;
                $newTrack->id_quest = $idQuestion;
                $newTrack->id_answer = $storedAnswer->id_answer;
                $newTrack->more_info = $answersInput[$storedAnswer->id_answer];
                if (!$newTrack->save()) {
                    throw new CException('Error while storing user answer');
                }
            }
        }
        return true;
    }

    public function _getAnswersList($idQuestion) {
        $criteria = new CDbCriteria();
        $criteria->order = 'sequence ASC';
        $answers = LearningPollquestanswer::model()->findAllByAttributes(array('id_quest' => $idQuestion), $criteria);
        if (!is_array($answers)) { return false; }
        $index = 0;
        $output = array();
        foreach ($answers as $answer) {
            $trimmedAnswer = trim(preg_replace('/(^(<p>)([\x00-\x1F\x80-\xFF]|&nbsp;|\s){1,})/','<p>',$answer->answer));
            $trimmedAnswer = preg_replace('/([\x00-\x1F\x80-\xFF]|&nbsp;|\s){1,}(<\/p>)$/','</p>',$trimmedAnswer);

            $output[] = array(
                'index' => $index,
                'sequence' => (int)$answer->sequence,
                'id_answer' => (int)$answer->id_answer,
                'answer' => (strlen($trimmedAnswer) > 0 ? $trimmedAnswer : 'error'),
                'answer_stripped' => strip_tags($answer->answer)
            );
            $index++;
        }
        return $output;
    }

    private function getQuestionsInput(){
        return Yii::app()->request->getParam('questions', array());
    }

    private function getScalesInput(){
        return Yii::app()->request->getParam('scales', array());
    }

    private function manageLikertScale($id_poll){

        $scales = $this->getScalesInput();
        $checkList = array();
        $keepList = array();
        foreach ($scales as $scale) {
            if (isset($scale['id_scale']) && (int)$scale['id_scale'] > 0) {
                $checkList[] = (int)$scale['id_scale'];
            }
        }

        $oldScales = LearningPollLikertScale::model()->findAllByAttributes(array('id_poll' => $id_poll));
        foreach ($oldScales as $oldScale) {
            $toCheck = (int)$oldScale->getPrimaryKey();
            // Check if the old ones exists in the new incoming array
            if (!in_array($toCheck, $checkList)) {
                if (!$oldScale->delete()) {
                    throw new CException('Error while updating answers');
                }
            } else {
                $keepList[$toCheck] = $oldScale;
            }
        }

        foreach ($scales as $sequence => $scale) {

            if (isset($scale['id_scale']) && $scale['id_scale'] > 0) {
                $index = (int)$scale['id_scale'];
                if (!isset($keepList[$index])) { throw new CException('Error while updating answers'); }
                $ar = $keepList[$index];
            } else {
                $ar = new LearningPollLikertScale();
                $ar->id_poll = $id_poll;
            }

            $ar->title = Yii::app()->htmlpurifier->purify($scale['scale']);
            $ar->sequence = $scale['sequence'];

            if (!$ar->save()) {
                throw new CException('Error while updating answers');
            }
        }
    }

    public function getPollScales($id_poll){
        $criteria = new CDbCriteria();
        $criteria->order = "sequence ASC";
        return LearningPollLikertScale::model()->findAllByAttributes(array('id_poll' => $id_poll), $criteria);
    }

    public function getDeleteScaleMessage(){
        $idQuestion = Yii::app()->request->getParam('id_question', null);
        // If the idQuestion is null then we are in edit mode so show the normal message
        $normalMessage = Yii::t('standard', '_AREYOUSURE');
        $advancedMessage = Yii::t('poll', '<b>Deleting this scale will remove it from all Likert questions inside this survey!</b> <br /><br />Are you sure you want to proceed?');
        $idPoll = Yii::app()->request->getParam('id_poll', null);
        $scalesCount = (int) Yii::app()->db->createCommand()
            ->select('COUNT(1)')
            ->from(LearningPollquest::model()->tableName())
            ->where(array('and', 'id_poll = :poll', 'type_quest = :type'))
            ->queryScalar(array(':poll' => $idPoll, ':type' => LearningPollquest::POLL_TYPE_LIKERT_SCALE));
        
        if($scalesCount > 1 || ($scalesCount == 1 && $idQuestion === null))
            return $advancedMessage;

        return $normalMessage;
    }

}