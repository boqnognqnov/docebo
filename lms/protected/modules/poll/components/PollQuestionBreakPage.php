<?php

class PollQuestionBreakPage extends PollQuestion {
	
	
	public function __construct() {
		parent::__construct();
		$this->setType(LearningPollquest::POLL_TYPE_BREAK_PAGE);
		$this->setInteractive(false);
		$this->setEditProperty('category', false);
	}



	public function beforeEdit($event) {
		if (!$event->params['isEditing']) {
			
			try {

				$idPoll = (int)$event->params['idPoll'];

				$controller = $this->getController();
				$object = LearningOrganization::model()->findByAttributes(array(
					'idResource' => $idPoll, 
					'objectType' => LearningOrganization::OBJECT_TYPE_POLL,
					'idCourse'   => $controller->getIdCourse()
				));

				if(isset($event->params['opt']) && $event->params['opt'] === 'centralrepo'){
					$versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
						'id_resource' => $idPoll,
						'object_type' => LearningOrganization::OBJECT_TYPE_POLL
					));

					if($versionModel){
						$object = LearningRepositoryObject::model()->findByPk($versionModel->id_object);
					}
				}

				if (!$object) { throw new CException('Invalid poll'); }
				$backUrl = $controller->createUrl('default/edit', array(
					'course_id' => $controller->getIdCourse(), 
					'id_object' => $object->getPrimaryKey(),
					'opt' => $event->params['opt']
				));

				$br = new LearningPollquest();
				$br->id_poll = $idPoll;
				$br->type_quest = LearningPollquest::POLL_TYPE_BREAK_PAGE;
				$br->title_quest = "--- BREAK PAGE ---";
				$br->id_category = 0;
				$br->sequence = LearningPollquest::getPollNextSequence($idPoll);
				$br->page = LearningPollquest::getPollPageNumber($idPoll);

				if (!$br->save()) {
					throw new CException('Error while saving break page');
				}

				Yii::app()->user->setFlash('success', Yii::t('standard', '_OPERATION_SUCCESSFUL'));
				
			} catch (CException $e) {

				Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
			}
			
			$controller->redirect($backUrl);
		}
	}


}