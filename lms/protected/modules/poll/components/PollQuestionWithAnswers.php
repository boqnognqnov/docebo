<?php

class PollQuestionWithAnswers extends PollQuestion {


	private $multipleChoice = false;


	//get / set methods

	protected function getMultipleChoice() {
		return (bool)$this->multipleChoice;
	}

	protected function setMultipleChoice($value) {
		$this->multipleChoice = (bool)$value;
	}

	public function useMultipleChoice() {
		return $this->getMultipleChoice();
	}



	// events

	public function onBeforeAnswerCreate($event) {
		$this->raiseEvent('onBeforeAnswerCreate', $event);
	}

	public function onAfterAnswerCreate($event) {
		$this->raiseEvent('onAfterAnswerCreate', $event);
	}

	public function onBeforeAnswerUpdate($event) {
		$this->raiseEvent('onBeforeAnswerUpdate', $event);
	}

	public function onAfterAnswerUpdate($event) {
		$this->raiseEvent('onAfterAnswerUpdate', $event);
	}

	public function onBeforeAnswerDelete($event) {
		$this->raiseEvent('onBeforeAnswerDelete', $event);
	}

	public function onAfterAnswerDelete($event) {
		$this->raiseEvent('onAfterAnswerDelete', $event);
	}



	//methods

	public function __construct() {
		parent::__construct();
	}



	protected function getAnswersInput() {
		return Yii::app()->request->getParam('answers', array());
	}



	public function create($event) {

		$db = Yii::app()->db;
		$answers = $this->getAnswersInput();

		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {

			$question = $event->params['question'];

			foreach ($answers as $sequence => $answer) {
				$ar = new LearningPollquestanswer();
				$ar->id_quest = $question->id_quest;
				$ar->answer = Yii::app()->htmlpurifier->purify($answer['answer']);
				$ar->sequence = $sequence+1;

				$this->onBeforeAnswerCreate(new CEvent($this, array('answer' => $ar, 'action' => 'create')));
				if (!$ar->save()) {
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				}
				$this->onAfterAnswerCreate(new CEvent($this, array('answer' => $ar, 'action' => 'create')));
			}

			if (isset($transaction)) { $transaction->commit(); }

		} catch (CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}

	}

	public function update($event) {

		$db = Yii::app()->db;
		$answers = $this->getAnswersInput();

		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {

			$question = $event->params['question'];

			$checkList = array();
			$keepList = array();

			foreach ($answers as $answer) {
				if (isset($answer['id_answer']) && (int)$answer['id_answer'] > 0) {
					$checkList[] = (int)$answer['id_answer'];
				}
			}

			$oldAnswers = LearningPollquestanswer::model()->findAllByAttributes(array('id_quest' => $question->id_quest));
			foreach ($oldAnswers as $oldAnswer) {
				$toCheck = (int)$oldAnswer->getPrimaryKey();
				if (!in_array($toCheck, $checkList)) {
					if (!$oldAnswer->delete()) {
						throw new CException('Error while updating answers');
					}
				} else {
					$keepList[$toCheck] = $oldAnswer;
				}
			}

			foreach ($answers as $sequence => $answer) {

				$action = false;
				if (isset($answer['id_answer']) && $answer['id_answer'] > 0) {
					$index = (int)$answer['id_answer'];
					if (!isset($keepList[$index])) { throw new CException('Error while updating answers'); }
					$ar = $keepList[$index];
					$action = 'update';
				} else {
					$ar = new LearningPollquestanswer();
					$ar->id_quest = $question->id_quest;
					$action = 'create';
				}

				$ar->answer = Yii::app()->htmlpurifier->purify($answer['answer']);
				$ar->sequence = $sequence+1;

				switch ($action) {
					case 'create': { $this->onBeforeAnswerCreate(new CEvent($this, array('answer' => $ar, 'action' => 'update'))); } break;
					case 'update': { $this->onBeforeAnswerUpdate(new CEvent($this, array('answer' => $ar, 'action' => 'update'))); } break;
				}
				if (!$ar->save()) {
					throw new CException('Error while updating answers');
				}
				switch ($action) {
					case 'create': { $this->onAfterAnswerCreate(new CEvent($this, array('answer' => $ar, 'action' => 'update'))); } break;
					case 'update': { $this->onAfterAnswerUpdate(new CEvent($this, array('answer' => $ar, 'action' => 'update'))); } break;
				}
			}

			if (isset($transaction)) { $transaction->commit(); }

		} catch (CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}

	}



	public function delete($event) {
		//...
	}



	public function storeUserAnswer($idTrack, $idQuestion, $answersInput, $canOverwrite) {

		if (!is_array($answersInput)) {
			$answersInput = $this->readAnswerInput($idQuestion);
			if (is_array($answersInput) && !$this->useMultipleChoice() && count($answersInput) > 1) {
				//error ...
			}
		}


		if ($this->userDoAnswer($idTrack, $idQuestion)) {
			if (!$canOverwrite) { return true; }
			$this->deleteUserAnswer($idTrack, $idQuestion);
		}

		$storedAnswers = $this->getAnswers($idQuestion);
		if (empty($storedAnswers)) { return; } //TO DO: what to do now?
		foreach ($storedAnswers as $storedAnswer) {

			$insert = false;
			if (is_array($answersInput) && in_array($storedAnswer->id_answer, $answersInput)) {
				//answer checked by the user
				$insert = true;
			}

			if ($insert) {
				$newTrack = new LearningPolltrackAnswer();
				$newTrack->id_track = $idTrack;
				$newTrack->id_quest = $idQuestion;
				$newTrack->id_answer = $storedAnswer->id_answer;
				$newTrack->more_info = "";
				if (!$newTrack->save()) {
					throw new CException('Error while storing user answer');
				}
			}
		}

		return true;
	}



	public function deleteUserAnswer($idTrack, $idQuestion) {
		$rs = LearningPolltrackAnswer::model()->deleteAllByAttributes(array('id_track' => $idTrack, 'id_quest' => $idQuestion));
		if ($rs === false) {
			throw new CException('Error while deleting stored answer');
		}
		return true;
	}



	public function getAnswers($idQuestion) {
		$criteria = new CDbCriteria();
		$criteria->order = "sequence ASC";
		return LearningPollquestanswer::model()->findAllByAttributes(array('id_quest' => $idQuestion), $criteria);
	}

}