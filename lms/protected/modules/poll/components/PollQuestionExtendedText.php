<?php

class PollQuestionExtendedText extends PollQuestion {
	
	
	public function __construct() {
		parent::__construct();
		$this->setType(LearningPollquest::POLL_TYPE_TEXT);
		$this->setEditProperty('category', true);
	}



	public function play($idQuestion) {
		$controller = $this->getController();
		$controller->renderPartial($this->getQuestionViewsPath().$this->getType(), array(
			'questionManager' => $this,
			'idQuestion' => $idQuestion
		));
	}



	public function storeUserAnswer($idTrack, $idQuestion, $answersInput, $canOverwrite) {
		$result = true;
		
		if (!is_array($answersInput)) {
			$answersInput = $this->readAnswerInput($idQuestion);
		}

		if ($this->userDoAnswer($idTrack, $idQuestion)) {
			if (!$canOverwrite) return true;
			if (!$this->deleteUserAnswer($idTrack, $idQuestion)) return false;
		}

		if ($answersInput !== NULL) {
	
			$ar = new LearningPolltrackAnswer();
			$ar->id_track = $idTrack;
			$ar->id_quest = $idQuestion;
			$ar->id_answer = 0;
			$ar->more_info = $answersInput;
			
			if (!$ar->save()) {
				throw new CException('Error while storing user answer');
			}
		}
	}

}