<?php

class PollQuestionTitle extends PollQuestion {
	
	
	public function __construct() {
		parent::__construct();
		$this->setType(LearningPollquest::POLL_TYPE_TITLE);
		$this->setInteractive(false);
		$this->setEditProperty('category', false);
	}



	public function play($idQuestion) {
		$question = LearningPollquest::model()->findByPk($idQuestion);
		$controller = $this->getController();
		$controller->renderPartial($this->getQuestionViewsPath().$this->getType(), array(
			'title' => $question->title_quest
		));
	}

}