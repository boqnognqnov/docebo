<?php

class PollQuestionChoice extends PollQuestionWithAnswers {
	
	
	public function __construct() {
		parent::__construct();
		$this->setType(LearningPollquest::POLL_TYPE_CHOICE);
		$this->setEditProperty('category', true);
	}



	public function play($idQuestion) {
		$answers = $this->_getAnswersList($idQuestion);
		$controller = $this->getController();
		$controller->renderPartial($this->getQuestionViewsPath().$this->getType(), array(
			'questionManager' => $this,
			'idQuestion' => $idQuestion,
			'answers' => $answers
		));
	}


	public function edit($event) {		
		$controller = $this->getController();
		$cs = Yii::app()->getClientScript();
		$cs->registerCoreScript('jquery.ui');
		$assetsUrl = $controller->getPlayerAssetsUrl();
		//$cs->registerCssFile($assetsUrl.'/css/player-sprite.css');   // moved to themes
		$cs->registerScriptFile($assetsUrl.'/js/testpoll.edit_question_choice.js');
		$controller->renderPartial('edit/_'.$this->getType(), array(
			'questionManager' => $this,
			'isEditing' => $event->params['isEditing'],
			'idQuestion' => $event->params['idQuestion']
		));
	}

	
	
	public function _getAnswersList($idQuestion) {
		$criteria = new CDbCriteria();
		$criteria->order = 'sequence ASC';
		$answers = LearningPollquestanswer::model()->findAllByAttributes(array('id_quest' => $idQuestion), $criteria);
		if (!is_array($answers)) { return false; }
		$index = 0;
		$output = array();
		foreach ($answers as $answer) {
            $trimmedAnswer = trim(preg_replace('/(^(<p>)([\x00-\x1F\x80-\xFF]|&nbsp;|\s){1,})/','<p>',$answer->answer));
            $trimmedAnswer = preg_replace('/([\x00-\x1F\x80-\xFF]|&nbsp;|\s){1,}(<\/p>)$/','</p>',$trimmedAnswer);

            $output[] = array(
				'index' => $index,
				'sequence' => (int)$answer->sequence,
				'id_answer' => (int)$answer->id_answer,
				'answer' => (strlen($trimmedAnswer) > 0 ? $trimmedAnswer : 'error'),
				'answer_stripped' => strip_tags($answer->answer)
			);
			$index++;
		}
		return $output;
	}
	
}