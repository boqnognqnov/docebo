<?php

class PollQuestion extends CComponent {


	const INPUT_PREFIX = 'question';

	protected $_type = false;

	protected $_controller = NULL;
	
	protected $_interactive = true;

	protected $_transformations = array();

	protected $_editProperties = array(
		'category' => true,
	);

	protected $_question = NULL;

	/**
	 * @var LearningPolltrack
	 */
	protected $_track = NULL;
	
	//get / set methods
	
	public function getType() {
		return $this->_type;
	}
	
	protected function setType($type) {
		if (LearningPollquest::isValidType($type)) {
			$this->_type = $type;
		} else {
			throw new CException('Invalid specified question type: '.$type);
		}
	}

	public function setTrack($track) {
		$this->_track = $track;
	}

	public function getController() {
		return $this->_controller;
	}

	public function setController($controller) {
		if (is_subclass_of($controller, 'CController')) {
			$this->_controller = $controller;
			if (get_class($this->_controller) == 'QuestionController') {
				foreach ($this->getEditProperties() as $editProperty => $value) {
					switch ($editProperty) {
						case 'category': { $this->_controller->setUseCategory($value); } break;
					}
				}
			}
			return true;
		}
		return false;
	}

	public function setQuestion($question) {
		$this->_question = $this->_validateQuestionInput($question);
	}

	public function getQuestion() {
		return $this->_question;
	}

	public function setInteractive($value) {
		$this->_interactive = (bool)$value;
	}
	
	public function getInteractive() {
		return (bool)$this->_interactive;
	}

	public function getEditProperties() {
		return $this->_editProperties;
	}
	
	public function setEditProperty($property, $value) {
		if (array_key_exists($property, $this->_editProperties)) {
			$this->_editProperties[$property] = (bool)$value;
			return true;
		}
		return false;
	}
	
	public function getEditProperty($property) {
		return (isset($this->_editProperties[$property]) 
			? (bool)$this->_editProperties[$property] 
			: NULL);
	}


	//methods
	
	
	public function __construct() {
		//...
	}



	protected function _validateQuestionInput($input) {
		if (is_numeric($input) && (int)$input > 0) {
			$output = LearningPollquest::model()->findByPk((int)$input);
			if (!$output || $output->type_quest != $this->getType()) {
				throw new CException('Invalid question');
			}
		} elseif (get_class($input) == 'LearningPollquest') {
			if ($input->type_quest != $this->getType()) {
				throw new CException('Invalid question');
			}
			$output = $input;
		} else {
			throw new CException('Invalid question');
		}
		return $output;
	}



	public function getQuestionViewsPath() {
		if (method_exists($this->controller, 'getQuestionViewsPath')) {
			return $this->controller->getQuestionViewsPath();
		}
		return "";
	}



	public static final function getQuestionManager($questionType) {
		if (LearningPollquest::isValidType($questionType)) {
			//$component = 'PollQuestion'.ucfirst(preg_replace("/\_(.)/e", "strtoupper('\\1')", $questionType));
			$component = 'PollQuestion'.ucfirst(preg_replace_callback("/\_(.)/", function($matches){
				return strtoupper($matches[1]);
			}, $questionType));
			return new $component();
		}
	}


	public function isInteractive() {
		return $this->getInteractive();
	}



	public function getInputId($idQuestion) {
		return self::INPUT_PREFIX.'-'.(int)$idQuestion;
	}

	public function getInputName($idQuestion) {
		return self::INPUT_PREFIX.'['.(int)$idQuestion.']';
	}

	public function readAnswerInput($idQuestion) {
		$input = Yii::app()->request->getParam(self::INPUT_PREFIX, false);
		return (is_array($input) && isset($input[$idQuestion])
			? $input[$idQuestion]
			: NULL);
	}


	public function setTransformation($key, $transformFunction) {
		if (empty($key) || !is_string($key) || is_numeric($key)) { return false; }
		$this->_transformations[$key] = $transformFunction;
		return true;
	}

	public function applyTransformation($key, $input, $arguments = false) {
		if (!isset($this->_transformations[$key])) {
			//no transformation has been specified for this key, return input data as it is
			return $input;
		}
		//process and validate input arguments
		$other = new stdClass();
		if (is_array($arguments) && !empty($arguments)) {
			foreach ($arguments as $reference => $value) {
				if (is_string($reference) && !empty($reference) && !is_numeric($reference)) {
					$other->$reference = $value;
				}
			}
		}
		//call transformation function
		return call_user_func($this->_transformations[$key], $input, $other);
	}


	/**
	 * Plays the question
	 */
	public function play($idQuestion) { }


	public function beforeEdit($event) { }

	public function edit($event) { }

	public function create($event) { }

	public function update($event) { }

	public function delete($event) { }



	public function userDoAnswer($idTrack, $idQuestion) {
		$info = LearningPolltrackAnswer::model()->findByAttributes(array('id_track' => $idTrack, 'id_quest' => $idQuestion));
		return (!empty($info));
	}

	public function storeUserAnswer($idTrack, $idQuestion, $answersInput, $canOverwrite) {
		//...
	}

	public function deleteUserAnswer($idTrack, $idQuestion) {
		$userAnswers = LearningPolltrackAnswer::model()->findAllByAttributes(array('id_track' => $idTrack, 'id_quest' => $idQuestion));
		if (!empty($userAnswers)) {
			foreach ($userAnswers as $userAnswer) {
				if (!$userAnswer->delete()) {
					throw new CException('Error while deleting user answers');
				}
			}
		}
		return true;
	}


	
	public function recoverUserAnswer($idQuestion, $idUser = false) {
		if (!$idUser) { $idUser = Yii::app()->user->id; }
		
		$output = array();
		$idPoll = LearningPollquest::model()->findByPk($idQuestion)->id_poll;

		$object = LearningOrganization::model()->findByPk($this->_track->id_reference);
		if($object->repositoryObject && $object->repositoryObject->shared_tracking)
			$track = LearningPolltrack::model()->findByAttributes(array('id_user' => $idUser, 'id_poll' => $idPoll));
		else
			$track = LearningPolltrack::model()->findByAttributes(array('id_user' => $idUser, 'id_poll' => $idPoll, 'id_reference' => $object->idOrg));

		// If there is no track, then this questions is presented for first time to the user
		if ($track === null)
			return $output;

		$idTrack = $track->getPrimaryKey();
		$answers = LearningPolltrackAnswer::model()->findAllByAttributes(array('id_track' => $idTrack, 'id_quest' => $idQuestion));
		if (!empty($answers)) {
			foreach ($answers as $answer) {
				$output[$answer->id_answer] = $answer;
			}
		}
		
		return $output;
	}


	/**
	 * While playing the test, detect if the question can be answered or less due to test settings
	 * @param $idQuestion the numerical ID of the question to analyze
	 * @param $idUser the numerical ID of the user to be checked
	 * @param $idTest the ID of the test where the question belongs
	 */
	public function canBeAnswered($idQuestion, $idUser, $idTest = null) {

		//validate input and get AR model object of the question
		$this->_validateQuestionInput($idQuestion);

//		if (!LearningTestQuestRel::isAssignedToTest($idTest, $idQuestion))
//		{
//			throw new CException('Question not assigned to the test');
//		}
		//retrieve test AR (throw exception if for some reasons the test is invalid
		$test = LearningPoll::model()->findByPk($idTest);
		if (!$test) {
			throw new CException('Invalid test');
		}

		//
		/* @var $test LearningPoll */
//		if ($test->mod_doanswer) {
//
//			//answers can be edited in this test, so the user must be allowed to do it: do not disable answers inputs
//			return true;
//
//		} else {

			//retrieve track info
			$track = LearningPolltrack::model()->findByAttributes(array('id_poll' => $test->getPrimaryKey(), 'id_user' => $idUser));
			if (!$track) {
				//no track record exists for this test and user, so no answers have been given yet.
				return true;
			}

			/* @var $track LearningTesttrack */
			//check if some answers already exists for this question and user
			$query = "SELECT COUNT(*) FROM ". LearningPolltrackAnswer::model()->tableName()." WHERE id_track = :id_track AND id_quest = :id_question";
			$params = array(':id_track' => $track->getPrimaryKey(), ':id_question' => $idQuestion);
			$count = Yii::app()->db->createCommand($query)->queryScalar($params);
			//if $count > 0 then some answers had already been given by this user and cannot change it
			return ($count <= 0);

		}
//	}

}