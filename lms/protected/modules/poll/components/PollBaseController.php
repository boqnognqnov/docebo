<?php
/**
 *
 * Module specific base controller
 *
 */
class PollBaseController extends LoBaseController {

	public $userCanAdminCourse = false;
	public $userIsSubscribed = false;
	public $courseModel = null;

	/**
	 * (non-PHPdoc)
	 * @see CController::init()
	 */
	public function init()
	{
		parent::init();

		JsTrans::addCategories(array('player', 'test', 'poll'));
		
		$this->registerResources();

		$this->courseModel = LearningCourse::model()->findByPk($this->getIdCourse());
		$opt = Yii::app()->request->getParam('opt', false);
		if (!$this->courseModel && (!$opt && $opt !== 'centralrepo')) {
			$this->redirect(array('/site'),true);
		}

		$this->userCanAdminCourse = (LearningCourseuser::userLevel(Yii::app()->user->id, $this->getIdCourse()) > LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT) || Yii::app()->user->isGodAdmin ;
		$this->userIsSubscribed = LearningCourseuser::isSubscribed(Yii::app()->user->id, $this->getIdCourse());

		// Player App. component is preloaded. Here we just set its attributes
		Yii::app()->player->setCourse($this->courseModel);
	}

	/**
	 * (non-PHPdoc)
	 * Method invoked before any action
	 * @param CAction $action
	 * @return bool
	 */
	public function beforeAction($action)
	{
		$this->buildBreadCrumbs($action);
		return parent::beforeAction($action);
	}

	/**
	 * Register module specific assets
	 *
	 * (non-PHPdoc)
	 * @see LoBaseController::registerResources()
	 */
	public function registerResources() {

		parent::registerResources();

		if (!Yii::app()->request->isAjaxRequest) {

			// Use
			// $this->module->assetsUrl
			// $this->getPlayerAssetsUrl() <-- shared between all player related modules

			$cs = Yii::app()->getClientScript();
			$cs->registerCssFile($this->getPlayerAssetsUrl().'/css/testpoll.css');

			// Bootcamp menu
			$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/docebo_bootcamp.js');
			$cs->registerCssFile (Yii::app()->theme->baseUrl . '/css/docebo_bootcamp.css');
		}
	}

	protected function notEnrolledCourseAdmin()
	{
		return $this->userCanAdminCourse && !$this->userIsSubscribed;
	}

	/**
	 * @param $action
	 */
	protected function buildBreadCrumbs($action)
	{
		if (!(strpos($action->id, "ax") === 0)) // non ajax actions
		{
			$cmodel = LearningCourse::model()->findByPk($this->getIdCourse());

			if ($this->notEnrolledCourseAdmin())
			{
				$this->breadcrumbs[Yii::t('menu', 'Admin')] = Docebo::createAppUrl('admin:dashboard/index');
				$this->breadcrumbs[Yii::t('standard', '_COURSES')] = Docebo::createAppUrl('admin:courseManagement/index');
			} else
			{
				$this->breadcrumbs[] = Yii::t('menu_over', '_MYCOURSES');
			}

			$this->breadcrumbs[$cmodel->name] = $this->createUrl('/player/training/index');
			$this->breadcrumbs[] = Yii::t('standard', 'Training materials');
		}
	}
}