(function ($) {

    /************************************************************************************************************/
    /* RIGHT SIDE*/

    // Central LO GRID Listeners
    $(document).on('change', 'input#LearningRepositoryObject_containChildren', function(){
        $('#repo-objects-grid').yiiGridView('update');
    });

    $(document).on('change', 'select#filter_by_type', function(){
        $('#repo-objects-grid').yiiGridView('update');
    });

    $(document).on('change', 'select#filter_by_csp', function(){
        $('#repo-objects-grid').yiiGridView('update');
    });

    /* -------------- ADD LO button ---------------- */
    $(document).on('mouseenter', '#central-lo-manage-add-lo-button .add-lo-type-description', function(){
        var hintText = $(this).attr('data-hint-text');
        var hintIco = $(this).attr('data-hint-icon-class');
        var hintElement = $('#central-lo-manage-add-lo-button #add-lo-type-hint');
        var defaultHintText = hintElement.attr('data-default-text');
        hintElement.html('<span class="pull-left hint-ico"><span class="' + hintIco + '"></span></span>' + hintText);
        $(this).on('mouseleave', function(){
            hintElement.html('<span class="span12 text-left">' + defaultHintText + '</span>');
        });
    });

    /* RIGHT SIDE*/
    /************************************************************************************************************/

})(jQuery);

var CentralLoUploader = function (options) {
    if(typeof options !== "undefined"){
        this.init(options);
    }
};

CentralLoUploader.prototype = {
    init: function(options){
        if(typeof options !== "undefined" && typeof options === 'object'){
            this.options = options;
        }

        this.run();
    },
    options: {},
    uploadFilters: [],
    lo_type: null,
    /**
     * Ask plUploader to re-render; used to fight IE super browser :-)
     */
    refresh: function () {
        this.pluploader.refresh();
    },
    
    
	/**
	 * Update UI to respect 'use_xapi_content_url' checkbox status 
	 */
	showHideXapiFromUrl: function() {
		
		if (this.options.loType === "tincan") {
			if ($('[name=use_xapi_content_url]').is(':checked')) {
				$('.version-control').show();
				$('#player-centralrepo-uploader-panel #xapi-content-from-url-settings').show();
				$('#player-centralrepo-uploader-panel #new-tincan-title').closest('.control-group').hide();
				$('#player-centralrepo-uploader-panel #upload-file').hide();
			}
			else {
				$('#player-centralrepo-uploader-panel #xapi-content-from-url-settings').hide();
				$('#player-centralrepo-uploader-panel #new-tincan-title').closest('.control-group').show();
				$('#player-centralrepo-uploader-panel #upload-file').show();
			}
		}
	},
    
    
    // Started after _upload partial view is loaded
    run: function () {

    	this.showHideXapiFromUrl();
    	
        var $this = this;
        // Create uploader instance
        var pl_uploader = new plupload.Uploader({
            unique_names: true, // If true, you will find the PHYSICAL file name in  $_REQUEST['file']['target_name'], otherwise: $_REQUEST['file']['name']
            chunk_size: '1mb',
            multi_selection: false,
            runtimes: 'html5,flash',
            browse_button: 'pickfiles',
            container: 'player-pl-uploader',
            drop_element: 'player-pl-uploader-dropzone',
            max_file_size: '1024mb',
            url: this.options.uploadFilesUrl,
            multipart: true,
            multipart_params: {
                YII_CSRF_TOKEN: this.options.csrfToken
            },
            flash_swf_url: this.options.pluploadAssetsUrl + '/plupload.flash.swf',
            filters: this.options.filters[this.options.loType]
        });

        this.pluploader = pl_uploader;

        // Init event hander
        pl_uploader.bind('Init', function (up, params) {

        });

        // Run uploader
        pl_uploader.init();


        /**
         * Check the upload feedback (info)
         * Normally "info" is:  Object {response: "{"jsonrpc" : "2.0", "result" : null, "id" : "id"}", status: 200}
         * In case of some errors, it is changing to: Object {response: "", status: 0}
         * We analyze this to decide if upload is going well or not
         */
        var checkInfo = function (up, file, info) {
            if (info.response === "" || info.status === 0 || info.status >= 400) {
                up.trigger('Error', {
                    code: plupload.HTTP_ERROR,
                    message: Yii.t('standard', 'Error occured during file upload, please check your connection and try again.'),
                    file: file,
                    status: info.status
                })
                $('#player-uploaded-cancel-upload').click();
                return false;
            }
            return true;
        };


        pl_uploader.bind('ChunkUploaded', function (up, file, info) {
            checkInfo(up, file, info);
        });


        pl_uploader.bind('FileUploaded', function (up, file, info) {
            checkInfo(up, file, info);
        });


        // Just before uploading file
        pl_uploader.bind('BeforeUpload', function (up, file) {
            $('#player-save-uploaded-lo').addClass("disabled");

            $('.upload-button').hide();
            $('.upload-progress').show();
            $('.player-uploaded-percent').show();
            bootstro.stop('bootstroAddBlockLauncher');
        });


        // Handle 'files added' event
        pl_uploader.bind('FilesAdded', function (up, files) {
            // Check if the added file is the correct format
            if(typeof up.settings.filters.extensions != "undefined") {
                var fileName = files[0].name.split(".");
                var fileExt = fileName[fileName.length - 1];
                if(up.settings.filters.extensions.indexOf(fileExt.toLowerCase()) == -1){
                    // Clear the added file from the queue
                    up.files = [];
                    Docebo.Feedback.show('error', Yii.t('app7020', 'File extension error.'));
                    return false;
                }
            }

            // in edit mode, mark the file as a new upload
            $('#player-centralrepo-uploader').find('#file-new-upload').val(1);

            this.files = [files[0]];
            $("#player-uploaded-filename").html(files[0].name);
            $("#player-uploaded-filesize").html(plupload.formatSize(files[0].size));
            pl_uploader.start();
        });

        // Upload progress
        pl_uploader.bind('UploadProgress', function (up, file) {
            $('#uploader-progress-bar').css('width', file.percent + '%');
            $('#player-uploaded-percent-gauge').html('&nbsp;' + file.percent + '%');
        });

        // On error
        pl_uploader.bind('Error', function (up, error) {
            $('#player-save-uploaded-lo').removeClass("disabled");
            Docebo.Feedback.show('error', error.message, true, false);
        });

        // On Upload complete
        pl_uploader.bind('UploadComplete', function (up, files) {
            $('.docebo-progress')
                .addClass("progress-success")
                .removeClass("progress-striped");
            $('#player-save-uploaded-lo').removeClass("disabled");
            $('#player-uploaded-cancel-upload').hide();

            if(typeof $this.options.isUpdate !== "undefined" && $this.options.isUpdate == true) {
                $('.version-control').show();
            }
        });

        // cancel upload cancel
        $('#player-uploaded-cancel-upload').on('click', function (e) {
            e.preventDefault();

            // Inline Upload area may have Tinymce attached to 'file-description' which must be destroyed
            // TinyMce.removeEditorById('file-description');

            $('#player-save-uploaded-lo').removeClass("disabled");
            if (pl_uploader) {
                pl_uploader.stop();
                pl_uploader.splice();
            }
            $('.upload-button').show();
            $('.upload-progress').hide();
        });


        // On SAVE BUTTON click (actual saving of the LO), ajax-submit the form
        $('#upload-lo-form').submit(function () {

            // Button may still be disabled (upload not finished??)
            if ($(this).hasClass('disabled')) {
                return false;
            }

            var $playerArenaUploader = $('#player-centralrepo-uploader');
            var idOrg = parseInt($playerArenaUploader.find('#file-id-org').val());
            var isNewUpload = parseInt($playerArenaUploader.find('#file-new-upload').val());
            var theFile = false;
            var videoSource = $playerArenaUploader.find('input[name=video-source]:checked').val();
            var externalXapi = $playerArenaUploader.find('#use-xapi-content-url').is(':checked');

            if (!isNaN(idOrg) && !isNewUpload || videoSource == 1 || externalXapi) {
                // do not check if a new file was uploaded
            } else {
                if (pl_uploader.files.length <= 0) {
                    Docebo.Feedback.show('error', Yii.t('error', 'Select a file to upload!'));
                    return false;
                }
                theFile = pl_uploader.files[0];

                // Make some checks
                if (theFile.percent < 100) {
                    Docebo.Feedback.show('error', 'Failed to upload 100% of the file!');
                    return false;
                }
            }

            var title = "", $title = $playerArenaUploader.find('#file-title, #new-scorm-title, #new-tincan-title, #new-aicc-title');
            if ($title.length > 0) {
                title = $title.val();
                if (title.length <= 0) {
                    Docebo.Feedback.show('error', 'Enter a title!');
                    return false;
                }
            }

            var description = "";
            var $description = $playerArenaUploader.find('textarea[id^="file-description"]');
            if ($description.length > 0) {
                description = $description.val();
            }

            var videoUrl = "";
            if(videoSource == 1) {
                var $videoUrl = $playerArenaUploader.find('input[name=video_url]').val();
                if ($videoUrl.length > 0) {
					if(!validateVideoUrl($videoUrl)){
						Docebo.Feedback.show('error', Yii.t('standard', 'The URL is not valid'));
						return false;
					}
                    videoUrl = $videoUrl;
                } else {
                    Docebo.Feedback.show('error', 'Enter a training matirial URL');
                    return false;
                }
            }


            // URI Encode text data (because of IE!)
            // Do NOT forget to URL Decose PHP side
            theFile.name = encodeURIComponent(theFile.name);
            title = encodeURIComponent(title);
            description = encodeURIComponent(description);

            var dataObject = {
                file: theFile,
                title: title,
                description: description,
                videoSource: videoSource,
                videoUrl: videoUrl
            };

            var formOptions = {
                dataType: 'json',
                data: dataObject,
                beforeSubmit: function (a, form, o) {
                    // No need to show 'Upload...' message for small files
                    if (theFile.size > 30000) {
                        $('#player-centralrepo-uploader').slideUp();
                        $('#upload-stuff-message').slideDown();
                    }
                    return true;
                },
                beforeSend: function (x) {
                    if (x && x.overrideMimeType) {
                        x.overrideMimeType('application/x-www-form-urlencoded; charset=UTF-8');
                    }
                    $('#upload-lo-form input[type=submit]').attr('disabled','disabled').addClass("disabled");
                    return true;
                },
                success: function (res) {
                    // Destroy TinyMce editor and switch to Admin view
                    TinyMce.removeEditorById('file-description');
                    if (res.success) {
                        $('#upload-stuff-message').slideUp();
                        Docebo.Feedback.show('success', res.message);
                        window.location.href = HTTP_HOST + 'index.php?r=centralrepo/centralRepo/index';
                    }
                    else {
                        $('#upload-stuff-message').slideUp();
                        $('#player-centralrepo-uploader').slideDown();
                        Docebo.Feedback.show('error', res.message);
						$('#upload-lo-form input[type=submit]').removeAttr('disabled').removeClass("disabled");
                    }
                }
            };

            $('#upload-lo-form').ajaxSubmit(formOptions);
            return false;
        });
    },
    globalListeners: function () {
        var $this= this;
        var oldValue = $('#player-centralrepo-uploader input[name=video_url]').val();
        var oldOption = $('#player-centralrepo-uploader input[name=video-source]:checked').val();

        $('#player-centralrepo-uploader').on('change', 'input[name=video-source]', function (){
            if($(this).val() == 1){
                $('#upload-file').addClass('hidden');
                $('#video-seekable').addClass('hidden');
                $('#video-subtitles').addClass('hidden');
                $('#video-url').removeClass('hidden');
            } else{
                $('#video-url').addClass('hidden');
                $('#upload-file').removeClass('hidden');
                $('#video-seekable').removeClass('hidden');
                $('#video-subtitles').removeClass('hidden');
            }

            if($(this).val() != oldOption){
                if(typeof $this.options.isUpdate !== "undefined" && $this.options.isUpdate == true){
                    $('.version-control').show();
                }
            } else{
                var urlValue = $('#player-centralrepo-uploader input[name=video_url]').val();
                if(urlValue!= oldValue){
                    if(typeof $this.options.isUpdate !== "undefined" && $this.options.isUpdate == true){
                        $('.version-control').show();
                    }
                } else{
                    $('.version-control').hide();
                }
            }
        });

        $('#player-centralrepo-uploader').on('keyup change paste', 'input[name=video_url]', function(){
            setTimeout(function(){
                var element = $('#player-centralrepo-uploader input[name=video_url]');
                var value = element.val();
                var infoBox = element.parent().find('.url-valudator-info');

                if(validateVideoUrl(value)){
                    infoBox.html('<i class="fa fa-check-circle-o fa-2x" aria-hidden="true"></i>');
                    element.val(value.trim());
                } else{
                    infoBox.html('<i rel="tooltip" data-original-title="' + Yii.t('standard', 'The URL is not valid') + '" class="fa fa-times fa-2x" aria-hidden="true"></i>');
                }

                infoBox.find('i').tooltip('show');

                if(value != oldValue){
                    if(typeof $this.options.isUpdate !== "undefined" && $this.options.isUpdate == true){
                        $('.version-control').show();
                    }
                } else{
                    $('.version-control').hide();
                }
            }, 100);
        });
        // Bind LO Upload clicks
        $('#central-lo-manage-add-lo-button').on('click', '.lo-upload', function (e) {
            e.preventDefault();

            var $this = $(this);
            var lo_type = $this.data('lo-type');

            window.location.href = HTTP_HOST + 'index.php?r=centralrepo/' + lo_type + 'Lo/edit';

        });
        
        
		$('#player-centralrepo-uploader-panel').on('change', '#use-xapi-content-url', function(){
			$this.showHideXapiFromUrl();
		});
        

    }
};



/**********************************************************************************************
 * LTI Resources
 *
 *
 * Show Create/Edit LTI dialog: title and editable area. Handles the submission process.
 *
 */
var CentralLoLtiEdit = function (options) {
    this.init(options);
};


CentralLoLtiEdit.prototype = {
    init: function (options) {
        this.editLtiUrl = options.editLtiUrl;
    },
    formListeners: function () {
        // ON LTI Resource URL entry
        var element = $('#player-centralrepo-uploader input#LearningLTI_url');
        $(element).on('focus', function(){
            if(element.val() == 'https://'){
                element.val('');
            }
        }).on('keypress paste', function(){
            setTimeout(function(){
                var value = element.val();
                var ltiRegex = /^(https:\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
                var ltiMatch = value.match(ltiRegex);
                var infoBox = element.parent().find('.url-valudator-info');

                if(ltiMatch){
                    infoBox.html('<i class="fa fa-check-circle-o fa-2x" aria-hidden="true"></i>');
                    element.val(value.trim());
                } else {
                    infoBox.html('<i rel="tooltip" data-original-title="' + Yii.t('standard', 'The URL is not valid') + '" data-placement="bottom" class="fa fa-times fa-2x" aria-hidden="true"></i>');
                }

                infoBox.find('i').tooltip('show');
            }, 100);
        }).on('blur', function(){
            if(element.val() == ''){
                element.val('https://');
            }
        });

        // ON Cancel button click
        $("#cancel_save_lti").on("click", function (e) {
            window.history.back();
        });

        // AJAX-Submit the form (jQuery forms is already loaded)
        $('#central-lo-lti-editor-form').submit(function () {
            var options = {
                data: {},
                dataType: 'json',
                success: function (res) {
                    if (res.success == true) {
                        window.location.href = Docebo.createAbsoluteLmsUrl('centralrepo/centralRepo/index', []);
                    } else {
                        Docebo.Feedback.show('error', res.message);
                    }
                }
            };

            $(this).ajaxSubmit(options);
            return false;
        });
    }
};

/**********************************************************************************************
 * Elucidat
 *
 *
 * Show Create/Edit Elucidat dialog: title and editable area. Handles the submition process.
 *
 */
var CentralLoElucidatEdit = function (options) {
    this.init(options);
    //this.globalListeners();
};


CentralLoElucidatEdit.prototype = {
    init: function (options) {
        if(typeof options !== "undefined"){
            this.editPageUrl = options.editPageUrl;
        }
    },
    show: function (idObject, step) {
        // Show loader, because Elucidat must do some background work before show some html (API calls etc.)
        $('.ajaxloader').show();

        var url = this.editPageUrl;

        url = url + "&step=" + step;
        window.location.href = url;

    },
    formListeners: function () {
        var that = this;

        // ON Cancel button click
        $("#cancel_save_elucidat").on("click", function (e) {
            window.history.back();
        });
        // AJAX-Submit the form (jQuery forms is already loaded)
        $('#elucidat-editor-form').submit(function () {
            var options = {
                data: {
                },
                dataType: 'json',
                success: function (res) {
                    if (res.success == true) {
                        window.location.href = Docebo.createAbsoluteLmsUrl('centralrepo', []);
                    } else {
                        Docebo.Feedback.show('error', res.message);
                    }
                }
            };

            $(this).ajaxSubmit(options);
            return false;
        });


    },
    globalListeners: function () {
        var that = this;
        $('#player-centralrepo-uploader-panel').on('click', '.lo-select-elucidat', function (e) {
            e.preventDefault();
            that.show(null, 2);
        });

        $(document).on("click", "div#main-elucidat-section a#create-elucidat-btn", function(){
            var element = $(this);
            if(element.hasClass("disabled")){
                event.preventDefault();
            }
        });
    }
};

/**********************************************************************************************
 * Google Drive
 *
 *
 * Show Create/Edit Elucidat dialog: title and editable area. Handles the submition process.
 *
 */
var CentralLoGoogleDriveEdit = function (options) {
    this.init(options);
};


CentralLoGoogleDriveEdit.prototype = {
    init: function (options) {
        this.editGoogleDriveUrl = options.editGoogleDriveUrl;
    },
    formListeners: function () {
        // ON Google Drive URL entry
        $('#player-centralrepo-uploader').on('keypress paste', 'input#LearningGoogledoc_url', function(){
            setTimeout(function(){
                var element = $('#player-centralrepo-uploader input#LearningGoogledoc_url');
                var value = element.val();
                var googleDriveRegex = /^(?:https?:\/\/docs.google.com\/)(a\/.{3,}\/)?(?:document|spreadsheets|presentation|drawings)(?:\/d\/)(.{10,})(\/pub|\/pubhtml|\/embed|\/preview|\/htmlembed)/;
                var googleDriveMatch = value.match(googleDriveRegex);
                var infoBox = element.parent().find('.url-valudator-info');

                if(googleDriveMatch){
                    infoBox.html('<i class="fa fa-check-circle-o fa-2x" aria-hidden="true"></i>');
                    element.val(value.trim());
                } else {
                    infoBox.html('<i rel="tooltip" data-original-title="' + Yii.t('standard', 'The URL is not valid') + '" data-placement="bottom" class="fa fa-times fa-2x" aria-hidden="true"></i>');
                }

                infoBox.find('i').tooltip('show');
            }, 100);
        });

        // ON Cancel button click
        $("#cancel_save_googledrive").on("click", function (e) {
            window.history.back();
        });

        // AJAX-Submit the form (jQuery forms is already loaded)
        $('#central-lo-gdrive-editor-form').submit(function () {
            var options = {
                data: {},
                dataType: 'json',
                success: function (res) {
                    if (res.success == true) {
                        window.location.href = Docebo.createAbsoluteLmsUrl('centralrepo/centralRepo/index', []);
                    } else {
                        Docebo.Feedback.show('error', res.message);
                    }
                }
            };

            $(this).ajaxSubmit(options);
            return false;
        });
    }
};

/**********************************************************************************************
 * LO Repository Categories prototype
 *
 */
var LoRepoCategories = function (options) {
    //FancyTree
    this.defaultOptions = {
        enableNodeActions: false,
        branchActionsTriggerClass: 		"lorepo-branch-actions-menu-trigger",		// Per-branch actions trigger element class
        branchActionsContainerIdPrefix: "lorepo-branch-actions-po-container-",		// ID prefix for Container for per-branch actions popover
    };
    if (typeof options === "undefined" ) {
        var options = {};
    }

    this.init(options);
};


LoRepoCategories.prototype = {

    normalizeContext: function(context){
        if(context.slice(-1) !== ' ')
            context = context + ' ';
        return context;
    },

    init: function(options){
        this.options = $.extend({}, this.defaultOptions, options);
    },

    //Modal Specific
    initModal: function(context){
        context = this.normalizeContext(context);

        $(document).on('change',  context + 'select#push-lo-to-courses-filter-by-type', function(){
            $('div[id*="lorepo-push-lo-modal-objects-grid"]').yiiGridView('update');
        });

        $(document).on('change',  context + 'input#push-lo-to-courses-show-descendants', function(){
            var curState = this.checked ? 1 : 0;
            $('div[id*="lorepo-push-lo-modal-objects-grid"]').yiiGridView('update', {
                data: {
                    pushContainChildren: curState
                }
            });
        });

        $(document).on('click', context + 'select.lorepo-push-lo-modal-version-select',function(event){
            //TODO
            event.stopPropagation();
        }).off('change', context + 'select.lorepo-push-lo-modal-version-select').on('change', context + 'select.lorepo-push-lo-modal-version-select', function(){
            var selectedObjects = JSON.parse($('#selected-objects').val());
            var versionId    = $(this).val();
            var objectId     = $(this).closest('tr').find('td.checkbox-column input').val();
            selectedObjects.forEach(function(item, index){
                if(item.objectId === objectId)
                selectedObjects[index].versionId = versionId;
            });
            $('#selected-objects').val(JSON.stringify(selectedObjects));
        });

        $(document).off('change', context + 'td.checkbox-column input').on('change', context + 'td.checkbox-column input', function(){
            var selectedObjects = JSON.parse($('#selected-objects').val());
            if($(this).prop('checked')){
                var objSelected = {};
                objSelected.objectId    = $(this).val();
                var closestVersionDropdown = $(this).closest('tr').find('td.object-version select.lorepo-push-lo-modal-version-select');
                var versionContainerSelector = (closestVersionDropdown.length > 0)? closestVersionDropdown : $(this).closest('tr').find('td.object-version input.lorepo-push-lo-modal-version-single-hidden');
                objSelected.versionId   =  versionContainerSelector.val() ;
                $(this).closest('tr').find('td.object-version select.lorepo-push-lo-modal-version-select').show();
                selectedObjects.push(objSelected);
            }
            else{
                var selectedObjects = JSON.parse($('#selected-objects').val());
                var key = $(this).val();
                selectedObjects.forEach(function(item, index){
                    if(item.objectId == key){
                        selectedObjects.splice(index,1);
                    }
                });
                $(this).closest('tr').find('td.object-version select.lorepo-push-lo-modal-version-select').hide();
            }
            $('#selected-objects').val(JSON.stringify(selectedObjects));
        });
    },

    // Tree management listeners
    managementListeners: function(){
        // Repo Category management, when the user change language input
        $(document).on('change', '#CoreOrgChart_lang_code, #CoreField_lang_code, #LearningCourseCategory_lang_code', function () {
            $('.languagesName').removeClass('show').addClass('hide');
            if ($('#NodeTranslation_' + $(this).val()).length > 0) $('#NodeTranslation_' + $(this).val()).addClass('show').removeClass('hide');
            else $('#CoreOrgChart_' + $(this).val()).addClass('show').removeClass('hide');
            $('.show > input').focus();
        });
        // Handle when the user is inserting translation for some language
        $(document).on('keyup', '.orgChart_translation',function () {
            var assignedCount = 0;
            $('.orgChart_translationsList .orgChart_translation').each(function (index) {
                if ($(this).val() != '') {
                    assignedCount++;
                }
            });
            $('#orgChart_assignedCount').html( Yii.t('standard', 'Filled languages') +': <span>' + assignedCount + '</span>');
        });


        // Handle the move
        $(document).on('click', 'ul.nodeUl div.nodeTree-selectNode', function (e) {

            $(this).find('input').prop('checked', true).trigger('refresh');
            $('.nodeTree-margin').removeClass('checked-node');
            $(this).addClass('checked-node');
            var currentId = $('#currentNodeId').val();
            var selectedId = $(this).find('#LearningCourseCategoryTree_idCategory').val();
            var url = '/../admin/index.php?r=courseCategoryManagement/getNodeParents';
            $.get(url, {'selectedId' : selectedId, 'currentId' : currentId}, function (data) {
                $('.nodeParents').html(data.content);
            }, 'json');

            // Prevent further execution
            e.preventDefault();
            e.stopPropagation();
            return false;
        });

        // Handle the deletion
        /**
         * Update Central Repo Category Tree in CentralRepo
         */
        $.fn.loadCentralRepoTree = function(data) {
            if (data.html) {
                $('.modal.in .modal-body').html(data.html);
            } else {
                $('.modal.in').modal('hide');
                if ($('#repo-tree-wrapper').length) {
                    LORepoFTree.loadTree();
                }
            }
        }

    },

    loadTree: function() {
        var source = $("#repo-tree-wrapper").data("source");
        $.ajax({
            method: "POST",
            url: source,
            success: function(data){
                $("#repo-tree-wrapper").html(data);
            }
        })
    },

    loadRepoGrid: function(event, data) {
        $('#repo-objects-grid').yiiGridView('update', {
            data: { id: data.node.key },
        } );
    },

    // Method that are required while pushing multiple object to courses
    loadPushLoTree: function() {
        var source = $("#push-lo-tree-wrapper").data("source");
        $.ajax({
            method: "POST",
            url: source,
            success: function(data){
                $("#push-lo-tree-wrapper").html(data);
            }
        })
    },

    loadPushLoRepoGrid: function(event, data) {
        $('div[id*="lorepo-push-lo-modal-objects-grid"]').yiiGridView('update', {
            data: { id: data.node.key },
        } );
    },

    deleteNodeAfterLoadingContent: function(data){
        if (data && ((data.data && data.data.update_tree) || data.update_tree)) {
            $('.modal.in.delete-node').modal('hide');
            CentraLoCategoriesTreeHelper.reloadCoursesCategoriesTree({
                title: Yii.t('course_management', 'Courses categories tree update') ,
                text: Yii.t('course_management', 'Courses categories tree has changed and needs to be updated') ,
                button: Yii.t('standard', '_CONFIRM')
            });
        }

        hideConfirmButton();

    },

    //FancyTree
    /**
     * Render tree columns; We are mainly interested in the last one (Index: 4): User actions popover
     */
    treeRenderColumns: function(event,data) {

        var node = data.node;
        var $tdList = $(node.tr).find(">td");
        var that = this;
        var poContainerId = this.options.branchActionsContainerIdPrefix + node.key;
        var html = '<div class="action-height-filler"></div><div class="ftree-new-check"></div>';

        //Fake collapse/expand arrow
        if($(node.tr).find('.fancytree-expander').css('background-image') != 'none'){
            html += '<div class="ftree-new-arrow" style=""></div>';
        }

        $tdList.last().html(html);
    },

    attachTreeDrag: function() {
        $('#repo-objects-grid a.drag-lo').draggable({
            cursor: 'move',
            helper: function(event) {
                var element = $(event.target).closest("tr").clone();
                // Get the element and destroy hi's tooltip
                var pointer = element.find("td:first-child").find('a').tooltip("destroy");

                var name = element.find("td:nth-child(2)").html();
                var helper = $("<div class='drag-helper'></div>").append(pointer).append(name);
                //debugger;
                return helper;
            },
        });
    },

    /**
     * Create a repeated API call to check if the video is converted from Amazon services
     */
    attachVideoConversionStatus: function(){
        //Get all items from the grid-view
      $('#repo-objects-grid a.drag-lo').each(function(){
          //Object type
            var type = $(this).attr('data-type');
            //Object ID
            var id = $(this).attr('data-id');
            //Parent element (tr-element)
            var parent = $(this).parent().parent();
            //Green icon element(td);
            var checker = parent.find('td.conversion-status i');
            //"Push to courses" element (td)
            var pusher = parent.find('td.push-to-course');
            //Filter by object type and if the status is not "ready" (its set earlier when populating the grid-view)
            if(type == 'video' && !checker.hasClass('ready')){
                //Create a dynamic variable, so any thread can be stopped separately
                window['centralRepoVideoConversion' + id] = setInterval(function(){
                    $.ajax({
                        method: 'GET',
                        url: Docebo.lmsAbsoluteBaseUrl + '/index.php?r=centralrepo/centralRepo/checkVideoConversionStatus&id_object=' + id,
                        success: function(response){
                            //Decode the response
                            response = JSON.parse(response);                        
                            var stopInterval = true;
                            if(response.data && response.data.status){     
                                //Default red icon                       
                                var iconClass = 'fa fa-check-circle-o';
                                //Default red color
                                var iconColor = 'color: #ff2222;';
                                if(response.data.status == 'converted'){
                                    // ^ Object is converted
                                    iconColor = 'color: #5fbf5f';
                                    if(pusher.find('a').length <= 0){
                                        var iElement = pusher.find('i');
                                        var pusherTitle = iElement.attr('data-title');
                                        var pusherUrl = iElement.attr('data-url');
                                        var title = Yii.t('central_repo', "Push <span class='object-name'>{title}</span> to courses", {
                                            title: pusherTitle
                                        });
                                        pusher.html('<a class="push_los_to_courses push-repo-object open-dialog" href="' + pusherUrl + '" alt="' + title + '" data-dialog-title="' + title + '" data-dialog-class="lorepo-push-to-courses users-selector" data-dialog-id="push-to-course-dialog"><strong><i class="fa fa-share"></i></strong></a>');                                    
                                        //Trigger controls, to updated the dialog2 functionalities
                                        $(document).controls();                                                                  
                                    }
                                } else if(response.data.status == 'in_progress'){
                                    // ^ Object not ready
                                    iconClass = 'fa fa-spinner fa-pulse';
                                    iconColor = 'color: #666';
                                    stopInterval = false;
                                }

                                //Update the icon
                                checker.attr('class', iconClass); 
                                checker.css('color', iconColor);

                                //If the status is "converted", stopInterval will be true and we need to stop API calls
                                if(stopInterval){
                                    //Clear interval dynamically - again
                                    clearInterval(window['centralRepoVideoConversion' + id]);
                                }
                            }
                        }
                    })
                }, 3000);            
            }        
      })
    },

    attachTreeDrop: function(e, data){
        $(data.node.span).closest("tr").droppable({

            tolerance: "pointer",
            greedy: true,
            over: function (event, ui) {
                $(event.target).addClass("drop-over");
            },
            out: function(event, ui) {
                $(event.target).removeClass("drop-over");
            },
            drop: function (event, ui) {
                var rawData = {
                    object: $(ui.draggable.context).data("id"),
                    category: $(event.target).attr("id").replace(/\D/g,'')
                };
                var url = $("div#repo-tree-wrapper").data("drop");
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: rawData,
                    success: function(){
                        LORepoFTree.loadTree();
                    }
                });

                return false; // Prevent propagation to parent
                //folder
            }
        });
    },

    showHideDescendantsSwitch: function(event, data) {
        // Check if the element Is in the main repo page or not
        var elementId = $(data.tree.$div).attr("id")
        // If is the root node hide the Descendants switch
        var visible = data.node.data.is_root ? 0 : 1;
        var contex = elementId.indexOf('push') > -1 ? "div#lorepo-push-lo-to-course-modal" : "div.centralRepoManagement";
        $(contex + " td.descendantsSwitch > table").css({"opacity": visible});
    }

};

var CentraLoCategoriesTreeHelper = {
    isRequestingUpdate: false,
    reloadCoursesCategoriesTree: function (params) {

        if (!CentraLoCategoriesTreeHelper.isRequestingUpdate) {
            //opening a dialog informing the user that the tree has changed and needs to be reloaded
            CentraLoCategoriesTreeHelper.isRequestingUpdate = true;

            //create the reload message dialog
            var unique = '_' + (new Date().getTime());
            var dialogId = 'update_tree_confirm' + unique;
            $('<div/>').dialog2({
                autoOpen: true,
                id: dialogId,
                modalClass: 'modal-update-tree',
                title: params.title || '',
                showCloseHandle: true,
                removeOnClose: false,
                closeOnEscape: true,
                closeOnOverlayClick: true
            });
            $('.modal.modal-update-tree').find('.modal-body').html(params.text || '');
            $('.modal.modal-update-tree').find('.modal-footer').html('<input class="btn confirm-btn" type="button" value="' + (params.button || '') + '" name="confirm" id="update-tree-confirm-button">');

            //manage the dialog closing
            $('.modal.modal-update-tree').off('dialog2.closed');
            $('.modal.modal-update-tree').on('dialog2.closed', function () {
                CentraLoCategoriesTreeHelper.isRequestingUpdate = false;
                //allow to call a custom function for tree reloading
                LORepoFTree.loadTree();
            });

            //manage dialog confirm button click
            $('.modal.modal-update-tree .modal-footer .confirm-btn').off('click');
            $('.modal.modal-update-tree .modal-footer .confirm-btn').on('click', function () {
                CentraLoCategoriesTreeHelper.isRequestingUpdate = false;
                $('#' + dialogId).dialog2('close');
            });

        } else {
            //avoid the opening of multiple confirm dialog, we need just one !
            Docebo.log("Tree updating dialog already opened ...");
        }
    }
};

function hideConfirmButton() {
    var confirmButton = $('.modal.fade.in').find('.btn-submit');
    confirmButton.addClass('disabled');
    return true;
}

function showConfirmButton() {
    var confirmButton = $('.modal.fade.in').find('.btn-submit');
    confirmButton.removeClass('disabled');
    return true;
}

function updateSelectedLoAfterGridUpdate() {
    var selectedObjects = JSON.parse($('#selected-objects').val());
    selectedObjects.forEach(function(item, index){
        var selItem = $(".checkbox-column input[value=" + item.objectId + "]");
        $(selItem).attr('checked', true).trigger('refresh');
        $(selItem).closest('tr').find('td.object-version select.lorepo-push-lo-modal-version-select').val(item.versionId).show();
    });
}

// Confirm Change Version
function updateConfirmSelectedCoursesAfterGridUpdate(grid_id) {
    var selectedObjects = JSON.parse($('#force_change_courses').val());
    selectedObjects.forEach(function(item, index){
        var selItem = $("#" + grid_id + " .checkbox-column input[value=" + item + "]");
        $(selItem).attr('checked', true).trigger('refresh');
    });
}
function validateVideoUrl(videoUrl) {
	var youtubeRegex = /(youtube.com|youtu.be)\/(watch)?(\?v=)?([a-zA-Z0-9\+\-\_^\s-]+)?/;
	var vimeoRegex = /(?:https?:\/\/)?(?:www\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/;
	var wistiaRegex = /https?:\/\/(.+)?(wistia.com|wi.st)\/(medias|embed)\/.*/;

	var youtubeMatch = videoUrl.match(youtubeRegex);
	if(youtubeMatch !== null){
		youtubeMatch = youtubeMatch[4];
		if(typeof youtubeMatch !== "undefined" && youtubeMatch.length !== 11){
			youtubeMatch = false;
		}
	}

	var vimeoMatch = videoUrl.match(vimeoRegex);
	if(typeof vimeoMatch !== "undefined" && vimeoMatch !== null){
		vimeoMatch = vimeoMatch[3];
	}

	var wistiaMatch = videoUrl.match(wistiaRegex);

	return (youtubeMatch || vimeoMatch || wistiaMatch);
}

/*********************************************************************************************
 * */