/**********************************************************************************************
 *  Launcher
 *
 *  All the LO launching business is done here - dispatching, opening lightboxes,
 *  opening new windows, redirecting and so.
 */

$(document).on('change', 'div.preview-lo div.previewCentralRepoObject select#object_version', function(){
    // If so let's relod the new version
    $('form#centralRepo_preview').submit();
});

var LoPreview = function (options) {
    this.init(options);
};

LoPreview.prototype = {

    init: function (options) {},

    // Choose (dispatch) appropriate LO launcher (per type) and run it
    dispatch: function (loData, callback) {
        // No folders, no locked LOs!
        if (loData.isFolder ) {
            return false;
        }

        var that = this;
        switch (loData.type) {

            case 'sco':
                that.scormSco(loData);
                break;

            case 'tincan':
                that.tincan(loData);
                break;

            case 'aicc':
                that.aicc(loData);
                break;

            case 'elucidat':
                that.elucidat(loData);
                break;

            default:
                Docebo.Feedback.show('error', loData.type + " launcher is coming soon....");
                break;
        }

        return true;
    },

    // SCO launcher
    scormSco: function (loData) {
        var that = this; // i.e. Launcher object
        // Get launching information and continue
        $.ajax({
            url: Docebo.lmsAbsoluteBaseUrl + '/index.php?r=centralrepo/centralRepoPreview/getScormItem',
            type: 'post',
            dataType: 'json',
            data: {
                id_object:          loData.id_object,
                id_resource:        loData.id_resource,
                idscorm_item:       loData.idscorm_item,
                idscorm_resource:   loData.idscorm_resource,
                chapter:            loData.identifiers
            }
        }).done(function (res) {
            if (res.success == true) {
                var data = res.data;
                that.inline(data.sco_url, loData);
            }
            else {
                Docebo.Feedback.show('error', res.message);
            }
        });
    },

    // AICC Item (AU) launcher
    // Very similar to SCO launcher, copy & paste & modify
    aicc: function (loData) {
        var that = this; // i.e. Launcher object
        // Get launching information and continue
        $.ajax({
            url: Docebo.lmsAbsoluteBaseUrl + '/index.php?r=centralrepo/centralRepoPreview/getAiccItem',
            type: 'post',
            dataType: 'json',
            data: {
                id_object:      loData.id_object,
                id_resource:    loData.id_resource,
                id_aicc_item:   loData.idAiccItem,
                chapter:        loData.aiccSystemId
            }
        }).done(function (res) {
            if (res.success == true) {
                var data = res.data;
                that.inline(data.launch_url, data);
            } else {
                Docebo.Feedback.show('error', res.message);
            }
        });

    },

    // TINCAN Launcher
    tincan: function (loData) {
        var data = loData;

        // Presense of a query string is determined server-side; see the controller/action
        var glue = data.hasQueryString ? "&" : "?";

        var launchLink = data.launch + glue + "endpoint=" + encodeURIComponent(data.endpoint)
            + "&auth=" + encodeURIComponent(data.auth)
            + "&actor=" + encodeURIComponent(JSON.stringify(data.actor))
            + "&registration=" + encodeURIComponent(data.registration)
            + "&activity_id=" + encodeURIComponent(data.activityId)
            + "&Accept-Language=" + data.acceptLanguage
            + "&content_token=" + data.content_token
            + "&preview_mode=" + true;

        this.inline(launchLink, loData);

    },

    // Elucidat Launcher - it is based on TinCan
    elucidat: function(loData) {

        var launchLink = loData.launch;

        this.inline(launchLink, loData);
    },

    inline: function (url, loData) {
        var _this = this;

        $(document).off("click", "a#previewLoFullscreen").on("click", "a#previewLoFullscreen", function(){
            var zoomButton = $(this).find("i");
            var modal = $("div.preview-lo");
            var iFrameParent = $("div#object_preview div#innerContent");

            // Handle the Modal Size
            if(modal.hasClass("fullscreen")) {

                modal.removeClass( "fullscreen" );
                $("html").css({overflow: "visible"});
                iFrameParent.removeAttr("style");
                zoomButton.addClass("fa-search-plus").removeClass("fa-search-minus")

            } else {

                modal.addClass( "fullscreen" );
                $("html").css({overflow: "hidden"});
                var modalHeaderH = $('.modal-header').height()  + 60 + 'px';
                var height = "calc(100vh - " + modalHeaderH + ")";
                iFrameParent.css({ height: height});
                zoomButton.addClass("fa-search-minus").removeClass("fa-search-plus")

            }

            $(document).on('dialog2.closed', 'div.preview-lo',function(event) {
                $("html").css({overflow: "visible"});
            });

        });

        // If this is the first request and the zoom buttons is still not attached then attach it
        if( !$('.modal.preview-lo div.modal-header a#previewLoFullscreen').length ) {
            var fullScreen = "<a href='#' id='previewLoFullscreen'><i class='fa fa-search-plus' aria-hidden='true'></i></a>";
            $('.modal.preview-lo div.modal-header h3').after(fullScreen);
        }

        var iframe = $(document.createElement('iframe')).attr({
            allowfullscreen: 'allowfullscreen',
            src: url,
            style: 'width: 100%; height: 100%; min-height: 400px',
            id: 'inline-' + loData.idResource,
            class: 'player-arena-inline ' + loData.type
        });
        $('div#player-content div#innerContent').html(iframe);
    }

};

var Preview = new LoPreview({});
Preview.init();