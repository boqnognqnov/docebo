<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 14.4.2016 г.
 * Time: 15:32
 */
class TestLoController extends CentralLoController{
    public function actionEdit(){

        Yii::app()->tinymce->init();

        $cs = Yii::app()->getClientScript();
        $playerAssetsUrl = Yii::app()->findModule('player')->assetsUrl;
        $cs->registerCssFile ($playerAssetsUrl.'/css/base.css');
        $cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/central_repo.css');

        $rq = Yii::app()->getRequest();
        /**
         * @var $rq CHttpRequest
         */
        $object = new LearningRepositoryObject();
        $idObject = $rq->getParam('id_object', false);
        $idTest = null;

        if($idObject){

            $object = LearningRepositoryObject::model()->findByPk($idObject);
            /**
             * @var $object LearningRepositoryObject
             */

            if($object){

                $versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
                    'id_object' => $object->id_object
                ));

                /**
                 * @var $versionModel LearningRepositoryObjectVersion
                 */

                if($versionModel){
                    $idTest = $versionModel->id_resource;
                }
            }
        }

        $this->render("lms.protected.modules.test.views.default._edit_test", array(
            'id_test'=>$idTest,
            'loModel'=>$object,
            'lo_type'=>LearningOrganization::OBJECT_TYPE_TEST,
            'saveUrl' => Docebo::createAbsoluteLmsUrl('centralrepo/testLo/save'),
            'centralRepoContext' => true
        ), false, true);
    }
    
    public function actionSave(){
        $request = Yii::app()->request;
        /**
         * @var $request CHttpRequest
         */

        $idObject = $request->getParam('id_object', false);
        $title = $request->getParam('title', false);
        $description = $request->getParam('description', false);
        $thumb = $request->getParam('thumb', false);
        $shortDescription = urldecode($request->getParam('short_description', false));

        if(!$title){
            Yii::app()->user->setFlash('error', Yii::t('error', 'Enter a title!'));

            $this->redirect(Docebo::createAbsoluteLmsUrl('centralrepo/testLo/edit', array(
                'id_object' => $idObject
            )));
        }

        if($idObject){
            $repoObject = LearningRepositoryObject::model()->findByPk($idObject);

            /**
             * @var $repoObject LearningRepositoryObject
             */

            if($repoObject){
                $versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
                    'id_object' => $repoObject->id_object
                ));

                /**
                 * @var $versionModel LearningRepositoryObjectVersion
                 */
                
                if($versionModel){
                    $testObject = LearningTest::model()->findByPk($versionModel->id_resource);

                    /**
                     * @var $testObject LearningTest
                     */

                    if($testObject){
                        $testObject->detachBehavior('learningObjects');

                        $testObject->title = $title;
                        $testObject->description = $description;
                        $testObject->author = Yii::app()->user->id;

                        if($testObject->save()){
                            $repoObject->title = $title;
                            $repoObject->description = $description;
                            $repoObject->short_description = $shortDescription;
                            $repoObject->resource = intval($thumb);

                            if($repoObject->save()){
                                $this->redirect(Docebo::createAbsoluteLmsUrl('test/default/edit', array(
                                    'id_object' => $repoObject->id_object,
                                    'opt' => 'centralrepo'
                                )));
                            }
                        }
                    }
                }
            }
        }

        $repoObject = new LearningRepositoryObject();
        $repoObject->object_type = LearningOrganization::OBJECT_TYPE_TEST;
        $categoryNodeId = LearningCourseCategoryTree::getRootNodeId();
        if(isset(Yii::app()->session['currentCentralRepoNodeId'])){
            $categoryNodeId = Yii::app()->session['currentCentralRepoNodeId'];
        }
        $repoObject->id_category = $categoryNodeId;
        $repoObject->title = $title;
        $repoObject->description = $description;
        $repoObject->short_description = $shortDescription;
        $repoObject->resource = intval($thumb);
        if($repoObject->save()){
            $testObject = new LearningTest();
            $testObject->detachBehavior('learningObjects');
            $testObject->title = $repoObject->title;
            $testObject->description = $repoObject->description;
            $testObject->author = Yii::app()->user->id;
            if($testObject->save()){
                $versionModel = new LearningRepositoryObjectVersion();
                $versionModel->object_type = LearningOrganization::OBJECT_TYPE_TEST;
                $versionModel->id_object = $repoObject->id_object;
                $versionModel->version = 1;
                $versionModel->version_name = 'V1';
                $versionModel->version_description = '';
                $userModel = Yii::app()->user->loadUserModel();
                /**
                 * @var $userModel CoreUser
                 */
                $authorData = array(
                    'idUser' => $userModel->idst,
                    'username' => $userModel->getUserNameFormatted()
                );
                $versionModel->author = CJSON::encode($authorData);
                $versionModel->id_resource = $testObject->idTest;
                $versionModel->create_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
                if($versionModel->save()){
                    $this->redirect(Docebo::createAbsoluteLmsUrl('test/default/edit', array(
                        'id_object' => $repoObject->id_object,
                        'opt' => 'centralrepo'
                    )));
                }
            }
        }
    }
}