<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 7.4.2016 г.
 * Time: 16:11
 */
class CentralLoController extends Controller {

    public function filters() {
        return array(
            'accessControl' => 'accessControl',
        );
    }

    public function accessRules() {

        $res = array();

        //Additional permission check for the courseAutocomplete function
        $res[] = array(
            'allow',
            'roles' => array('/framework/admin/setting/view'),
            'actions' => array(
                'edit',
                'axUpload',
                'save',
                'AxSubtitleToggleFallback',
                'AxDeleteSubtitle',
                'axListAllowedIframeUrls',
                'getSlidersContent',
                'deleteImage',
                'editShared',
                'saveShared'
            ),
        );

        $res[] = array(
            'allow',
            'roles' => array('/lms/admin/course/mod'),
            'actions' => array(
                'editShared',
                'saveShared'
            )
        );

        $res[] = array(
            'deny',
            // deny all
            'users' => array('*'),
            'deniedCallback' => array($this,'accessDeniedCallback'),
        );

        return $res;
    }

    public function beforeAction($action){
        if($action->id == 'edit'){
        	$this->registerSomeAssets();
        }
        return parent::beforeAction($action);
    }

    /**
     * Ajax action that handles custom image uploaded deletion icon click
     */
    public function actionDeleteImage(){
        $imageId = Yii::app()->getRequest()->getParam('image_id');

        $success = false;

        if ((int) $imageId > 0) {
            $asset = CoreAsset::model()->findByPk($imageId);
            if ($asset) {
                $success = $asset->delete();
            }

            $ajaxResult = new AjaxResult();
            $data = array();
            $ajaxResult->setStatus($success)->setData($data)->toJSON();
            Yii::app()->end();
        }else{
            throw new CException('Invalid image ID');
        }
    }

    public function actionGetThumbnails() {

        $rq = Yii::app()->request;
        $pageId = $rq->getParam('pageId', 0);
        $totalCount = $rq->getParam('totalCount', false);
        $id_object = Yii::app()->request->getParam("id_object", false);

        if(!$id_object){
            $id_object = Yii::app()->request->getParam("loId", false);
        }

        $selectedImageId = $rq->getParam('selectedImageId', null);
        $assets = new CoreAsset();
        $userImages = array();

        $offset = $pageId*10;
        $maxPages =  ceil($totalCount/10) - 1;
        if($offset < 0){
            $offset = $maxPages*10;
            $pageId =  $maxPages;
        }elseif($pageId > $maxPages){
            $offset = 0;
            $pageId = 0;
        }
        if($id_object){
            $learningOrganization = LearningRepositoryObject::model()->findByAttributes(array('id_object' => $id_object));

            if($learningOrganization){
                if($learningOrganization->resource && !$selectedImageId){
                    $selectedImageId = $learningOrganization->resource;
                }
                $userImages  = $assets->urlList(CoreAsset::TYPE_LO_IMAGE, CoreAsset::SCOPE_PRIVATE, false, CoreAsset::VARIANT_SMALL);
                foreach ($userImages as $key => $value) {
                    if ($key == $learningOrganization->resource) {
                        $this->reorderArray($userImages, $key, $value);
                        break;
                    }
                }
                $userImages = array_chunk($userImages, 10, true);
            }
            if(isset($userImages[$pageId])){
                $userImages = $userImages[$pageId];
            }elseif(isset($userImages[$pageId-1])){
                $userImages = $userImages[$pageId-1];
                $pageId = $pageId-1;
            }elseif(isset($userImages[0])){
                $userImages = $userImages[0];
                $pageId = 0;
            }

        }else{
            $learningOrganization = new LearningRepositoryObject();
            $userImages = $assets->urlList(CoreAsset::TYPE_LO_IMAGE, CoreAsset::SCOPE_PRIVATE, false, CoreAsset::VARIANT_SMALL, $offset , 10);

        }

        if(!$userImages){
            $pageId = 0;
            if($pageId < 0){
                $pageId = $maxPages-1;
            }
            $offset = $pageId*10;
            $userImages = $assets->urlList(CoreAsset::TYPE_LO_IMAGE, CoreAsset::SCOPE_PRIVATE, false, CoreAsset::VARIANT_SMALL, $offset , 10);
        }

        $html = $this->renderPartial('application.modules.player.views.training._carouselThumbnails', array(
            'model' => $learningOrganization,
            'userImages' => $userImages,
            'selectedImageId' => $selectedImageId,

        ),true,true);
        $this->sendJSON(array(
            'html' => $html,
            'currentPage' => $pageId,
        ));

        Yii::app()->end();
    }
    protected function reorderArray(&$array, $key, $value) {
        unset($array[$key]);
        $array = array($key => $value) + $array;
    }

    public function actionGetSlidersContent(){

        // default crop size
        $cropSize = array(
            'width' => 300,
            'height' => 300,
        );
        Yii::app()->event->raise('OverrideCourseImageCropSize', new DEvent($this, array(
            'cropSize' => &$cropSize
        )));

        list($count, $defaultImages, $userImages, $selected) = LearningRepositoryObject::prepareThumbnails();

        $uniqueId = substr(md5(uniqid(rand(), true)), 0, 10);

        $html = $this->renderPartial('lms.protected.modules.player.views.training._sliders', array(
            'defaultImages' => $defaultImages,
            'userImages' => $userImages,
            'count' => $count,
            //'selectedTab' => $selected,
            'selectedTab' => 'user',
            'uniqueId' => $uniqueId,
            'cropSize' => $cropSize,
        ), true);

        echo CJSON::encode(array(
            'content'=>$html
        ));
    }

    public function editUploadLo($loType, $idObject = false){
		
    	$this->registerSomeAssets();
    	
        /* @var $cs CClientScript */
    	
        // We need some helper methods located in these files in the
        // Player List View layout LO add/edit UI: Additional info tab, custom thumbnail uploading logic
    	$cs = Yii::app()->getClientScript();
        $cs->registerCssFile(Yii::app()		->theme->baseUrl . '/css/common.css');
    	
        // Load bootstro styles (used in the new/edit video LO modal)
        $cs->registerCssFile(Yii::app()		->theme->baseUrl . '/js/bootstro/bootstro.css');
        $cs->registerScriptFile(Yii::app()	->theme->baseUrl . '/js/bootstro/bootstro.js');
        
        // Also, Player base.css
        $cs->registerCssFile ($this->getModule()->getPlayerAssetsUrl()   . '/css/base.css');

        $requestTitle 		= false;
        $requestDescription = false;
        $videoSubtitles = array(); // array to hold video subtitles if LO is a video and there are any subtitles
        $loModel = null;
        $versionModel = null;

        switch ($loType) {
            case LearningOrganization::OBJECT_TYPE_SCORMORG:
            case LearningOrganization::OBJECT_TYPE_TINCAN:
            case LearningOrganization::OBJECT_TYPE_AICC:
            case LearningOrganization::OBJECT_TYPE_ELUCIDAT:
                break;
            default:
                $requestTitle = true;
                $requestDescription = true;
                break;
        }

        // if we're editing, get the LO instance
        if($idObject){
            $loModel = LearningRepositoryObject::model()->findByPk($idObject);
        }

        $resource = null;
        if ($loModel) {
            /**
             * @var $loModel LearningRepositoryObject
             */
            $version = $loModel->getLatestVersion();

            if(!empty($version)){
                $versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
                    'id_resource' => $version['id_resource'],
                    'object_type' => $version['object_type']
                ));

                switch ($loType) {
                    case 'video':
                        $resVideo = LearningVideo::model()->findByPk($versionModel->id_resource);
                        $resource = $resVideo->attributes;
                        //$resource['id_org'] = $loModel->idOrg;
                        $videoFormats = CJSON::decode($resVideo->video_formats);
                        $videoNameArr = array_values($videoFormats);
                        $resource['filename'] = $videoNameArr[0];

                        $videoSubtitles = LearningVideoSubtitles::model()->findAllByAttributes(array('id_video' => $versionModel->id_resource));
                        break;
                    case 'file':
                        $resFile = LearningMaterialsLesson::model()->findByPk($versionModel->id_resource);
                        $resource = $resFile->attributes;
                        $resource['filename'] = empty($resFile->original_filename) ? $resFile->path : $resFile->original_filename;
                        break;
                    case 'scormorg':
                        $scormModel = LearningScormOrganizations::model()->findByPk($versionModel->id_resource);
                        $resource['filename'] = $scormModel->scormPackage->path;
                        break;
                    case 'tincan':
                        $tinCanModel = LearningTcActivity::model()->findByPk($versionModel->id_resource);
                        $resource['filename'] = $tinCanModel->ap->path;
                        break;
                    case 'aicc':
                        $aiccPackage = LearningAiccPackage::model()->findByPk($versionModel->id_resource);
                        $resource['filename'] = $aiccPackage->path;
                        break;

                    default:
                        break;
                }
            }

        }

        // Tab-based view will be used always, as per UX designer recommendations
        $view = 'lms.protected.modules.centralrepo.views.centralLo._upload_detailed';
        $isListViewLayout = false;

        // List of all active languages
        $activeLanguagesList = CoreLangLanguage::getActiveLanguages();

        // We need to know the currently configured File Storage list of allowed Video Formats
        // We create a dummy storage object and retrieve an array of allowed extensions.
        // (see below the Arena init() )
        $dummyStorage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_VIDEO);
        $whitelistArray = $dummyStorage->getVideoWhitelistArray();
        Yii::app()->tinymce->init();

        $showVideoSubsHints = (count($videoSubtitles) == 0) ? true : false;


        $this->render($view, array(
            'uploadUrl' 			=> $this->createUrl("axLoUpload"),
            'assetsBaseUrl' 		=> $this->getModule()->getPlayerAssetsUrl(),
            'lo_type' 				=> $loType,
            'courseModel' 			=> new LearningCourse(),
            'requestTitle' 			=> $requestTitle,
            'requestDescription' 	=> $requestDescription,
            'resource' 				=> $resource,
            'loModel' 				=> $loModel,
            'isListViewLayout'		=> $isListViewLayout,
            'resVideo'              => $resVideo,
            'activeLanguagesList'   => $activeLanguagesList,
            'showHints'             => $showVideoSubsHints,
            'videoSubtitles'        => $videoSubtitles,
            'loRepositoryContext'   => true,
            'videoExtensionsWhitelist' => $whitelistArray,
            'versionModel'          => $versionModel
        ));
    }

    public function actionAxUpload(){
        // HTTP headers for no cache etc
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $targetDir = Docebo::getUploadTmpPath();
        $cleanupTargetDir = true; // Remove old files
        $maxFileAge = 5 * 3600; // Temp file age in seconds

        // 5 minutes execution time
        @set_time_limit(5 * 60);

        // Uncomment this one to fake upload time
        //usleep(5000);


        // Get parameters
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $fileName = isset($_REQUEST["name"]) ? basename(Yii::app()->htmlpurifier->purify($_REQUEST["name"])) : '';
        $deliverableUpload =  Yii::app()->request->getParam("deliverableUpload", false);

        // Clean the fileName for security reasons
        // This generates a bug, since space is replaced by '_' and
        // $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

        // Make sure the fileName is unique but only if chunking is disabled
        if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName) && !$deliverableUpload) {
            $ext = strrpos($fileName, '.');
            $fileName_a = substr($fileName, 0, $ext);
            $fileName_b = substr($fileName, $ext);
            $count = 1;
            while(file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b)) {
                $count++;
            }
            $fileName = $fileName_a . '_' . $count . $fileName_b;
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

        // Remove old temp files
        if ($cleanupTargetDir) {
            if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
                while (($file = readdir($dir)) !== false) {
                    $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                    // Remove temp file if it is older than the max age and is not the current file
                    if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
                        @unlink($tmpfilePath);
                    }
                }
                closedir($dir);
            } else {
                die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory: ' . $targetDir . '."}, "id" : "id"}');
            }
        }


        // Look for the content type header
        if (isset($_SERVER["HTTP_CONTENT_TYPE"])) {
            $contentType = $_SERVER["HTTP_CONTENT_TYPE"];
        }

        if (isset($_SERVER["CONTENT_TYPE"])) {
            $contentType = $_SERVER["CONTENT_TYPE"];
        }


        // Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
        if (strpos($contentType, "multipart") !== false) {
            if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
                // Open temp file
                $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
                if ($out) {
                    // Read binary input stream and append it to temp file
                    $in = @fopen($_FILES['file']['tmp_name'], "rb");

                    if ($in) {
                        while($buff = fread($in, 4096)) {
                            fwrite($out, $buff);
                        }
                    } else {
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                    }
                    @fclose($in);
                    @fclose($out);
                    @unlink($_FILES['file']['tmp_name']);
                } else {
                    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
                }
            } else {
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
            }
        } else {
            // Open temp file
            $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
            if ($out) {
                // Read binary input stream and append it to temp file
                $in = @fopen("php://input", "rb");

                if ($in) {
                    while($buff = fread($in, 4096)) {
                        fwrite($out, $buff);
                    }
                } else {
                    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                }

                @fclose($in);
                @fclose($out);
            } else {
                die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
            }
        }

        // Check if file has been uploaded
        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);

            // Mime type file check
            /*$mimeTypes = FileTypeValidator::getMimeWhiteListArray(FileTypeValidator::TYPE_ITEM);
            if (!empty($mimeTypes)) {
                $fileHelper = new CFileHelper();
                $fileMimeType = $fileHelper->getMimeType($filePath);
                if(!$fileMimeType || !in_array($fileMimeType, $mimeTypes)) {
                    @unlink($filePath);
                    die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Detected file type "' . $fileMimeType . '" not allowed"}, "id" : "id", "extra" : "' . $extraParam . '"}');
                }
            }*/
        }

        die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');


    }

    public function actionEditShared(){
        $request = Yii::app()->request; /** @var $request CHttpRequest */

        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile ($this->getModule()->getAssetsUrl()   . '/css/central_repo.css');

        $courseId = $request->getParam('course_id', false);
        $idOrg = $request->getParam('id_org', false);
        if(!$courseId || !$idOrg)
            throw new CException('No course or idOrg specified');

        $orgModel = LearningOrganization::model()->findByPk($idOrg);

        $versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
            'id_resource' => $orgModel->idResource,
            'object_type' => $orgModel->objectType
        ));

        $command = Yii::app()->db->createCommand();
        $command->select('*')
            ->from(LearningRepositoryObjectVersion::model()->tableName())
            ->where('id_object = :object', array(
                ':object' => $versionModel->id_object
            ));

        $versions = $command->queryAll();
        $availableVersions = array();
        if(!empty($versions)){
            foreach ($versions as $version){
                $authorData = CJSON::decode($version['author']);
                $userModel = CoreUser::model()->findByPk($authorData['idUser']);
                if($userModel)
                    $userName = $userModel->getFullName();
                else
                    $userName = "Unknown";
                $availableVersions[$version['id_resource'] . '-' . $version['object_type']] = $version['version_name'] . ' - ' . Yii::app()->localtime->toLocalDateTime($version['create_date']) . ' ' . Yii::t('standard', 'uploaded by') . ' ' . $userName;
            }
        }

        $activeVersion = $versionModel->id_resource . '-' . $versionModel->object_type;

        $html = $this->renderPartial('_edit_shared', array(
            'versionModel' => $versionModel,
            'availableVersions' => $availableVersions,
            'activeVersion' => $activeVersion,
            'idOrg' => $idOrg,
            'idCourse' => $courseId,
            'loModel' => $orgModel
        ), true, true);

        $response = new AjaxResult();
        $response->setHtml($html)->setStatus(true)->toJSON();
    }

    public function actionSaveShared(){
        $request = Yii::app()->request; /** @var $request CHttpRequest */
        $version = $request->getParam('version', false);
        $idOrg = $request->getParam('id_org', false);
        $idCourse = $request->getParam('course_id', false);
        $confirm = $request->getParam('confirm', false);
        $customView = $request->getParam('custom_view', false);
        $desktopMode = $request->getParam('desktop_openmode', false);
        $smartphoneMode = $request->getParam('smartphone_openmode', false);
        $tabletMode = $request->getParam('tablet_openmode', false);

        $status = true;
        $html = '';
        $data = array();
        $message = Yii::t('standard', '_OPERATION_SUCCESSFUL');

        try {
            if(!$version || !$idOrg)
                throw new CException('Required parameters are missing');

            $loModel = LearningOrganization::model()->findByPk($idOrg); /** @var $loModel LearningOrganization */
            if(!$loModel)
                throw new CException('Invalid object ID');

            list($idResource, $objectType) = explode('-', $version);
            $usersInProgressInLo = $loModel->countInProgressUsers();
            if(($usersInProgressInLo == 0) || ($loModel->idResource == $idResource))
                $confirm = true;

            if($confirm !== false) {
                $newVersionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array('id_resource' => $idResource, 'object_type' => $objectType));
                if ($newVersionModel) {
                    $oldIdResource = $loModel->idResource;
                    $loModel->idResource = $newVersionModel->id_resource;
                    if ($customView !== false) {
                        $loModel->desktop_openmode = $desktopMode;
                        $loModel->smartphone_openmode = $smartphoneMode;
                        $loModel->tablet_openmode = $tabletMode;
                    } else{
                        $loModel->saveViewMode = false;
                    }

                    if ($loModel->saveNode()) {
                        if ($usersInProgressInLo > 0) {
                            // Reset shared trackings for this LO version for all "in progress" users in this course
                            CommonTracker::updateTracksAfterVersionSwitch($oldIdResource, $loModel);
                        }
                    } else
                        throw new CException(CHtml::errorSummary($loModel));
                } else
                    throw new CException("Enable to load version object");
            } else {
                $html = $this->renderPartial('_version_change', array(
                    'version' => $version,
                    'idOrg' => $idOrg,
                    'customView' => $customView,
                    'desktopMode' => $desktopMode,
                    'smartphoneMode' => $smartphoneMode,
                    'tabletMode' => $tabletMode,
                    'idCourse' => $idCourse,
                    'loUsers' => $usersInProgressInLo
                ), true, true);

                $data['showConfirmationDialog'] = true;
            }
        } catch(Exception $e) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
            $status = false;
            $message = Yii::t('standard', '_OPERATION_FAILURE');
        }

        $response = new AjaxResult();
        $response
            ->setHtml($html)
            ->setMessage($message)
            ->setData($data)
            ->setStatus($status)->toJSON();
        Yii::app()->end();
    }
    
    
    private function registerSomeAssets() {
    	
    	$cs = Yii::app()->getClientScript();
    	
    	$adminAssetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias("admin.js"));
    	$cs->registerScriptFile($adminAssetsUrl."/scriptModal.js");
    	$cs->registerScriptFile($adminAssetsUrl."/scriptTur.js");
    	
    	$cs->registerScriptFile(Yii::app()	->theme->baseUrl.'/js/Jcrop/jquery.Jcrop.min.js');
    	$cs->registerCssFile(Yii::app()		->theme->baseUrl.'/js/Jcrop/jquery.Jcrop.min.css');
    	$cs->registerCssFile(Yii::app()		->theme->baseUrl.'/css/admin.css');
    	 
    }
    
}