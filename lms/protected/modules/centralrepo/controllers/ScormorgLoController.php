<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 7.4.2016 г.
 * Time: 16:09
 */
class ScormorgLoController extends CentralLoController {

    public function actionEdit(){
        $idObject = Yii::app()->request->getParam('id_object', false);

        $this->editUploadLo(LearningOrganization::OBJECT_TYPE_SCORMORG, $idObject);
    }

    protected function updateScormPackage($idObject, $file, $version) {
        try {
            // Validate file input
            if (!is_array($file)) { throw new CException("Invalid file."); }
            if (!isset($file['name']) || ($file['name'] == '')) { throw new CException("Invalid file name."); }
            if (!isset($file['percent']) || $file['percent'] < 100) { throw new CException("Incomplete file upload."); }
            if ('zip' != strtolower(CFileHelper::getExtension($file['name']))) { throw new CException("Invalid file format."); }

            $uploadTmpFile = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR . $file['name'];
            if (!is_file($uploadTmpFile)) { throw new CException("File '" . $file['name'] . "' does not exist."); }

            $loModel = LearningRepositoryObject::model()->findByPk($idObject);

            /**
             * @var $loModel LearningRepositoryObject
             */

            if (!$loModel) { throw new CException("Invalid learning object"); }

            $scormOrgModel = LearningScormOrganizations::model()->findByPk($version['id_resource']);
            $package = $scormOrgModel->scormPackage;
            if (!$package) { throw new CException("Old SCORM package not found"); }


            $res = $package->extractScormPackage($uploadTmpFile, $package->path);

            if($res){
                $versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
                    'id_resource' => $version['id_resource'],
                    'object_type' => LearningOrganization::OBJECT_TYPE_SCORMORG,
                    'version' => $version['version']
                ));

                if($versionModel){
                    $versionModel->update_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');

                    return $versionModel->save();
                }
            }

        } catch (CException $e) {
        	Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
        }

        return false;
    }

    protected function createNewVersion($idObject, $file, $prevVersion = 0){

        $versionName = Yii::app()->request->getParam('version_name', 'V1');
        $versionDesc = Yii::app()->request->getParam('version_description', false);

        $package = new LearningScormPackage();
        $package->idUser = Yii::app()->user->id;
        $package->setFile($file['name']);
        $package->setScenario('centralRepo');

        if($package->save()){

            $repoObjectVersion = new LearningRepositoryObjectVersion();
            $userModel = Yii::app()->user->loadUserModel();
            /**
             * @var $userModel CoreUser
             */
            $authorData = array(
                'idUser' => $userModel->idst,
                'username' => $userModel->getUserNameFormatted()
            );
            $repoObjectVersion->author = CJSON::encode($authorData);
            $repoObjectVersion->create_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
            $repoObjectVersion->id_object = $idObject;
            $repoObjectVersion->object_type = LearningOrganization::OBJECT_TYPE_SCORMORG;
            $repoObjectVersion->id_resource = $package->scormOrganization->idscorm_organization;
            $repoObjectVersion->version = $prevVersion + 1;
            $repoObjectVersion->version_name = $versionName;
            if($versionDesc !== false){
                $repoObjectVersion->version_description = $versionDesc;
            }

            if($repoObjectVersion->save()){

                $repoObject = LearningRepositoryObject::model()->findByPk($idObject);

                if($repoObject){
                    $repoObject->title = $package->scormOrganization->title;

                    return $repoObject->save();
                }
            }
        }

        return false;
    }

    public function actionSave(){
        $file = Yii::app()->request->getParam("file", null);
        $success = false;
        $message = Yii::t('standard', '_OPERATION_FAILURE');
        $response = new AjaxResult();

        // Use the physically uploaded filename, put by PLUpload in the "target_name" element
        // This is true when "unique_names" option is used for plupload JavaScript object
        if (is_array($file) && isset($file['name'])) {
            $originalFileName = urldecode($file['name']);
            if (isset($file['target_name'])) {
                $file['name'] = $file['target_name'];
            }
        }


        $shortDescription = Yii::app()->request->getParam('short_description', '');
        $thumb = Yii::app()->request->getParam('thumb', null);

        // URLDecode
        if (is_array($file) && isset($file['name'])) {
            $file['name'] = urldecode($file['name']);
        }

        // In case we are doing SCORM Package update ... we will get idOrg != false ... (from edit form)
        $idObject = (int) Yii::app()->request->getParam("id_object", false);

        if ($idObject > 0) {

            $lo = LearningRepositoryObject::model()->findByPk($idObject);
            $versionStatus = Yii::app()->request->getParam('version-control', 0);


            // IF we have file uploaded, update scorm package
            if (($file !== 'false') && ($file !== null) && ($file !== false)) {

                $version = $lo->getLatestVersion();

                if(!$versionStatus){
                    $success = $this->updateScormPackage($idObject, $file, $version);
                } else{
                    $versionModel = LearningRepositoryObjectVersion::model()->findByAttributes($version);
                    $success = $this->createNewVersion($idObject, $file, $versionModel->version);
                }
            }
            // Some LO types are passing specific options upon uploading/saving REQUEST; lets handle them
            /**
             * @var $lo LearningRepositoryObject
             */
            if ($lo)  {
                $lo->short_description = Yii::app()->htmlpurifier->purify($shortDescription);
                if($thumb){
	                $lo->resource = intval($thumb);
                }elseif( $lo->resource){
	                $lo->resource = 0;
                }

                $lo->params_json = CJSON::encode($this->prepareLoOptions());

                //update title displayed in LMS
                $newTitle = trim(Yii::app()->htmlpurifier->purify(Yii::app()->request->getParam("new-scorm-title", null)));
                $lo->title = $newTitle;

                if($lo->save()){
                    $success = true;
                }

                // Trigger event to let plugins save their own data from POST
                Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
                    'loModel' => $lo
                )));
            }

            if($success){
                $message = Yii::t('standard', '_OPERATION_SUCCESSFUL');
            }
            $response->setMessage($message)->setStatus($success);
            $response->toJSON();
            Yii::app()->end();
        }


        // Otherwise, we are obviosly going to create NEW One... lets go...


        Yii::app()->db->beginTransaction();

        try {

            // Validate file input
            if (!is_array($file)) { throw new CException("Invalid file."); }
            if (!isset($file['name']) || ($file['name'] == '')) { throw new CException("Invalid file name."); }
            if (!isset($file['percent']) || $file['percent'] < 100) { throw new CException("Incomplete file upload."); }

            $fullFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $file['name'];
            /*********************************************************/
            if (isset(Yii::app()->params['virusscan_enable']) && (Yii::app()->params['virusscan_enable'] == true)) {
                $vs = new VirusScanner();
                $clean = $vs->scanFile($fullFilePath);
                if (!$clean) {
                    FileHelper::removeFile($fullFilePath);
                    throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
                }
            }

            /*********************************************************/

            /*** File extension type validate ************************/
            $fileValidator = new FileTypeValidator(FileTypeValidator::TYPE_ARCHIVE);
            if (!$fileValidator->validateLocalFile($fullFilePath))
            {
                FileHelper::removeFile($fullFilePath);
                throw new CException($fileValidator->errorMsg);
            }
            /*********************************************************/

            $repoObject = new LearningRepositoryObject();
            $repoObject->short_description = $shortDescription;
            $repoObject->resource = intval($thumb);
            $repoObject->object_type = LearningOrganization::OBJECT_TYPE_SCORMORG;
            $repoObject->params_json = CJSON::encode($this->prepareLoOptions());
            $categoryNodeId = LearningCourseCategoryTree::getRootNodeId();
            if(isset(Yii::app()->session['currentCentralRepoNodeId'])){
                $categoryNodeId = Yii::app()->session['currentCentralRepoNodeId'];
            }
            $repoObject->id_category = $categoryNodeId;

            if($repoObject->save()){
                if($this->createNewVersion($repoObject->id_object, $file)){
                    $success = true;
                    $message = Yii::t('standard', '_OPERATION_SUCCESSFUL');
                }
            }

            $transaction = Yii::app()->getDb()->getCurrentTransaction();
            if($transaction) {
                $transaction->commit();
            }
            $response->setMessage($message)->setStatus($success);

        } catch (CException $e) {

            $transaction = Yii::app()->getDb()->getCurrentTransaction();
            if($transaction) {
                $transaction->rollback();
            }
            $response->setMessage($e->getMessage())->setStatus(false);
        }

        $response->toJSON();
        Yii::app()->end();
    }

    /**
     * A work towards saving LO specific options/settings based on the current REQUEST to axSaveLo() action
     *
     * @param object|bool $loModel
     */
    private function prepareLoOptions() {

        $data = array();

        // These are pretty common and can be sent by all LO types being saved/uploaded
        $data['desktop_openmode']    = Yii::app()->request->getParam("desktop_openmode", LearningOrganization::OPENMODE_LIGHTBOX);
        $data['tablet_openmode']     = Yii::app()->request->getParam("tablet_openmode", LearningOrganization::OPENMODE_FULLSCREEN);
        $data['smartphone_openmode'] = Yii::app()->request->getParam("smartphone_openmode", LearningOrganization::OPENMODE_FULLSCREEN);

        return $data;
    }
}