<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 11.4.2016 г.
 * Time: 13:45
 */
class AiccLoController extends CentralLoController {
    public function actionEdit(){
        $idObject = Yii::app()->request->getParam('id_object', false);

        $this->editUploadLo(LearningOrganization::OBJECT_TYPE_AICC, $idObject);
    }

    public function actionSave(){
        $file 		= Yii::app()->request->getParam("file", null);
        $response = new AjaxResult();
        $message = Yii::t('standard', '_OPERATION_FAILURE');
        $success = false;
        $transaction = Yii::app()->db->beginTransaction();

        try {

            // Use the physically uploaded filename, put by PLUpload in the "target_name" element
            // This is tru when "unique_names" option is used for plupload JavaScript object
            if (is_array($file) && isset($file['name'])) {
                $originalFileName = urldecode($file['name']);
                if (isset($file['target_name'])) {
                    $file['name'] = $file['target_name'];
                }
            }

            $shortDescription 	= Yii::app()->request->getParam('short_description', '');
            $thumb 				= Yii::app()->request->getParam('thumb', null);

            // URLDecode
            if (is_array($file) && isset($file['name'])) {
                $file['name'] = urldecode($file['name']);
            }

            // In case we are doing SCORM Package update ... we will get idOrg != false ... (from edit form)
            $idObject = (int) Yii::app()->request->getParam("id_object", false);

            if ($idObject > 0) {
                $lo = LearningRepositoryObject::model()->findByPk($idObject);

                // IF we have file uploaded, update scorm package
                if (($file !== 'false') && ($file !== null) && ($file !== false)) {
                    $versionStatus = Yii::app()->request->getParam('version-control', 0);

                    $version = $lo->getLatestVersion();
                    $versionData = array(
                        'id_resource' => $version['id_resource'],
                        'currentVersion' => $version['version'],
                        'versionName' => Yii::app()->request->getParam('version_name', 'V1'),
                        'versionDesc' => Yii::app()->request->getParam('version_description'. '')
                    );
                    $res = $this->updatePackage($idObject, $file, $originalFileName, $thumb, $shortDescription, $versionStatus != 0, $versionData);

                    if($res !== false){
                        $message = Yii::t('standard', '_OPERATION_SUCCESSFUL');
                        $success = true;
                    }
                }

                // Some LO types are passing specific options upon uploading/saving REQUEST; lets handle them
                if ($lo)  {
                    $lo->short_description = Yii::app()->htmlpurifier->purify($shortDescription);
                    if($thumb){
	                    $lo->resource = intval($thumb);
                    }elseif($lo->resource){
	                    $lo->resource = 0;
                    }

                    $lo->params_json = CJSON::encode($this->prepareLoOptions());

                    //update title displayed in LMS
                    $newTitle = urldecode(trim(Yii::app()->request->getParam("new-aicc-title", null)));
                    $lo->title = $newTitle;
                    if($lo->save()){
                        $message = Yii::t('standard', '_OPERATION_SUCCESSFUL');
                        $success = true;
                    }
                }

                $transaction->commit();

                // Trigger event to let plugins save their own data from POST
                Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
                    'loModel' => $lo
                )));

                $response->setMessage($message)->setStatus($success);
                $response->toJSON();
                Yii::app()->end();
            }


            /// We are here because we are creating NEW package
            $package = $this->createPackage($idObject, $file, $originalFileName, $thumb, $shortDescription);
            if (!$package) {
                Yii::log('Invalid package returned from updatePackage', CLogger::LEVEL_ERROR);
                throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
            }

            $message = Yii::t('standard', '_OPERATION_SUCCESSFUL');
            $success = true;

            $transaction->commit();

            // Trigger event to let plugins save their own data from POST
            Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
                'loModel' => $package->learningObject
            )));

            $response->setMessage($message)->setStatus($success);

        } catch (CException $e) {
            $transaction->rollback();
            $response->setMessage($e->getMessage())->setStatus(false);
        }

        $response->toJSON();
        Yii::app()->end();
    }

    protected function updateAiccPackage($package, $originalFileName, $fullFilePath){
        $package->lms_original_filename = $originalFileName;

        // Try to extract the package into the final destination (S3, file system,...)
        if ($package->extractPackage($fullFilePath)) {
            // Save uploaded package (handle its content, create all the intermediate table records, creating LO and so on)
            // Be aware: behavior is attached !!!
            return $package;
        }

        return false;
    }

    protected function createNewVersion($originalFileName, $fullFilePath, $id_object, $versionName = 'V1', $versionDesc = '', $nextVersion = 0){
        $package = new LearningAiccPackage();
        $package->idReference = null;
        $package->lms_id_user = Yii::app()->user->id;

        $package->lms_original_filename = $originalFileName;
        $package->setScenario('centralRepo');

        if($this->updateAiccPackage($package, $originalFileName, $fullFilePath) !== false){
            if (!$package->save()) {
                Yii::log('Error saving AICC package: ' . $originalFileName, CLogger::LEVEL_ERROR);
                Yii::log(var_export($package->errors, true), CLogger::LEVEL_ERROR);
                throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
            }
            else {
                $versionModel = new LearningRepositoryObjectVersion();
                $versionModel->version = $nextVersion + 1;
                $versionModel->version_name = $versionName;
                $versionModel->version_description = $versionDesc;
                $versionModel->create_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
                $userModel = Yii::app()->user->loadUserModel();
                /**
                 * @var $userModel CoreUser
                 */
                $authorData = array(
                    'idUser' => $userModel->idst,
                    'username' => $userModel->getUserNameFormatted()
                );
                $versionModel->author = CJSON::encode($authorData);
                $versionModel->object_type = LearningOrganization::OBJECT_TYPE_AICC;
                $versionModel->id_resource = $package->id;
                $versionModel->id_object = $id_object;

                if($versionModel->save()){
                    $repoObject = LearningRepositoryObject::model()->findByPk($id_object);

                    if($repoObject){
                        $repoObject->title = $package->course_title;

                        if($repoObject->save()){
                            return $package;
                        }
                    }
                } else{
                	Yii::log(var_export($versionModel->errors,true), CLogger::LEVEL_ERROR);
                }
            }
        }

        return false;
    }

    protected function createPackage($idObject, $file, $originalFileName, $thumb, $shortDescription) {
        return $this->updatePackage($idObject, $file, $originalFileName, $thumb, $shortDescription);
    }

    /**
     * Update an existing package OR create new one, depending on idOrg
     *
     * @param integer|null $idObject
     * @param array $file
     * @param string $originalFileName
     * @param integer $thumb
     * @param string $shortDescription
     * @param $updateVersion boolean
     * @param $versionData array
     *
     * @throws CException
     *
     * @return LearningAiccPackage
     */
    protected function updatePackage($idObject, $file, $originalFileName, $thumb, $shortDescription, $updateVersion = false, $versionData = array()) {

        // Validate file input if NO idOrg is specified, i.e. when we are CREATING NEW
        if ($idObject) {
            if (!is_array($file)) 									{ throw new CException("Invalid file."); }
            if (!isset($file['name']) || ($file['name'] == '')) 	{ throw new CException("Invalid file name."); }
            if (!isset($file['percent']) || $file['percent'] < 100) { throw new CException("Incomplete file upload."); }
        }

        $fullFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $file['name'];
        /*********************************************************/
        if (isset(Yii::app()->params['virusscan_enable']) && (Yii::app()->params['virusscan_enable'] == true)) {
            $vs = new VirusScanner();
            $clean = $vs->scanFile($fullFilePath);
            if (!$clean) {
                FileHelper::removeFile($fullFilePath);
                Yii::log('File is not clean (VirusScanner): ' . $fullFilePath, CLogger::LEVEL_ERROR);
                throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
            }
        }
        /*********************************************************/

        /*** File extension type validate ************************/
        $fileValidator = new FileTypeValidator(FileTypeValidator::TYPE_ARCHIVE);
        if (!$fileValidator->validateLocalFile($fullFilePath))
        {
            FileHelper::removeFile($fullFilePath);
            throw new CException($fileValidator->errorMsg);
        }
        /*********************************************************/

        if ($idObject) {

            $lo = LearningRepositoryObject::model()->findByPk($idObject);
            $versionModelData = $lo->getLatestVersion();

            if (!$lo) {
                throw new CException("Invalid Learning Object");
            }


            if($updateVersion){
                $package = $this->createNewVersion($originalFileName, $fullFilePath, $lo->id_object, $versionData['versionName'], $versionData['versionDesc'], $versionData['currentVersion']);

                if($package !== false){
                    return true;
                }
            } else{
                $package = LearningAiccPackage::model()->findByPk($versionModelData['id_resource']);
                $package->setScenario('centralRepo');

                if($this->updateAiccPackage($package, $originalFileName, $fullFilePath)){
                    if($package->save()){

                        $versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
                            'id_resource' => $versionModelData['id_resource'],
                            'object_type' => LearningOrganization::OBJECT_TYPE_AICC,
                            'version' => $versionModelData['version']
                        ));

                        if($versionModel){
                            $versionModel->update_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');

                            if($versionModel->save()){
                                return true;
                            }
                        }
                    }
                }

                return false;
            }

            return false;
        }
        else {
            $categoryNodeId = LearningCourseCategoryTree::getRootNodeId();
            if(isset(Yii::app()->session['currentCentralRepoNodeId'])){
                $categoryNodeId = Yii::app()->session['currentCentralRepoNodeId'];
            }

            $lo = new LearningRepositoryObject();
            $lo->short_description = $shortDescription;
            $lo->resource = $thumb;
            $lo->object_type = LearningOrganization::OBJECT_TYPE_AICC;
            $lo->params_json = CJSON::encode($this->prepareLoOptions());
            $lo->id_category = $categoryNodeId;
            if($lo->save()){
                return $this->createNewVersion($originalFileName, $fullFilePath, $lo->id_object);
            }
        }
        return false;
    }

    /**
     * A work towards saving LO specific options/settings based on the current REQUEST to axSaveLo() action
     *
     * @param object|bool $loModel
     */
    private function prepareLoOptions() {

        $data = array();

        // These are pretty common and can be sent by all LO types being saved/uploaded
        $data['desktop_openmode']    = Yii::app()->request->getParam("desktop_openmode", LearningOrganization::OPENMODE_LIGHTBOX);
        $data['tablet_openmode']     = Yii::app()->request->getParam("tablet_openmode", LearningOrganization::OPENMODE_FULLSCREEN);
        $data['smartphone_openmode'] = Yii::app()->request->getParam("smartphone_openmode", LearningOrganization::OPENMODE_FULLSCREEN);

        return $data;
    }
}