<?php

class CentralRepoVersionController extends Controller {


    public function filters() {
        return array(
            'accessControl' => 'accessControl',
        );
    }

    public function accessRules() {

        $res = array();

        //Additional permission check for the courseAutocomplete function
        $res[] = array(
            'allow',
            'roles' => array('/framework/admin/setting/view'),
            'actions' => array(
                'index',
                'axEdit',
                'axDelete'
            ),
        );

        $res[] = array(
            'deny',
            // deny all
            'users' => array('*'),
            'deniedCallback' => array($this,'accessDeniedCallback'),
        );

        return $res;
    }

	public function beforeAction($action)
	{
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/admin.css');

		return parent::beforeAction($action);
	}

    /**
 * This is the default 'index' action that is invoked
 * when an action is not explicitly requested by users.
 */
    public function actionIndex() {

        $loRepositoryId = Yii::app()->request->getParam('id', '');
        $loRepository = LearningRepositoryObject::model()->findByPk($loRepositoryId);

        $dataProvider = LearningRepositoryObjectVersion::model()->sqlDataProvider();

        $listViewParams = array(
            'disableFiltersWrapper' => true,
            'disableMassSelection' => true,
            'disableMassActions' => true,
            'listId' => "loRepoVersionsComboListView",
            'dataProvider' => $dataProvider,
            'noItemViewContainer' => true,
            'template' => '{items}{pager}',
            'listItemView' => 'lms.protected.modules.centralrepo.views.centralRepoVersion._versionItem',
            'hiddenFields' => array('id' => $loRepositoryId)
        );

        $this->render('index', array(
            'listViewParams' => $listViewParams,
            'loRepository' => $loRepository,
        ), false, true);
    }

    /**
 * Edit version
 * @throws CException
 * @throws CHttpException
 */
    public function actionAxEdit() {
        $rq = Yii::app()->request;
        $id = $rq->getParam('id_resource', '');
        $objectType = $rq->getParam('object_type', '');
        $version = LearningRepositoryObjectVersion::model()->findByAttributes(array('id_resource' => $id, 'object_type' => $objectType));

        if ($rq->getParam('confirm', false) !== false) {
            // Form submission part
            try {
                $version->attributes = $_REQUEST['LearningRepositoryObjectVersion'];
                $version->save();

                echo '<a class="auto-close"></a>';
            } catch (CException $e) {
                $this->renderPartial('/_common/_errorMessage', array(
                    'message' => $e->getMessage()
                ));
            }
            Yii::app()->end();
        }

        $this->renderPartial('edit', array(
            'model' => $version,
        ));
    }

    /**
     * Edit version
     * @throws CException
     * @throws CHttpException
     */
    public function actionAxDelete() {
        $rq = Yii::app()->request;
        $id = $rq->getParam('id_resource', '');
        $objectType = $rq->getParam('object_type', '');
        $version = LearningRepositoryObjectVersion::model()->findByAttributes(array('id_resource' => $id, 'object_type' => $objectType));
        $coursesCount = count(LearningOrganization::getLOsForLORepoVersion($version->object_type, $version->id_resource));

        $lro = LearningRepositoryObject::model()->findByPk($version->id_object);

        if ($rq->getParam('confirm', false) !== false) {
            // Form submission part
            try {
                // Delete the version at hand
                $version->delete();

                // If no other versions are present, delete the entire LO
                if (count($lro->versions) == 0)
                    $lro->delete();

                echo '<a class="auto-close" data-value="' . count($lro->versions) . '"></a>';
            } catch (CException $e) {
                $this->renderPartial('/_common/_errorMessage', array(
                    'message' => $e->getMessage()
                ));
            }
            Yii::app()->end();
        }

        $this->renderPartial('delete', array(
            'model' => $version,
            'coursesCount' => $coursesCount
        ));
    }
}