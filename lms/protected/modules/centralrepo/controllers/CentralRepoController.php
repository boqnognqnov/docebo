<?php

class CentralRepoController extends Controller {

    public $_playerAssetsUrl;

    public function filters() {
        return array(
            'accessControl' => 'accessControl',
        );
    }

    public function accessRules() {

        $res = array();

        //Additional permission check for the courseAutocomplete function
        $res[] = array(
            'allow',
            'roles' => array('/framework/admin/setting/view'),
            'actions' => array(
                'index',
                'objectsAutocomplete',
                'pushToCourse',
                'versions',
                'axDelete',
                'axSimpleDelete',
                'getLosGrid',
                'viewUsage',
                'courseUsegaAutocomplete',
                'pushFromLocalCourse',
                'checkVideoConversionStatus'
            ),
        );

        $res[] = array(
            'deny',
            // deny all
            'users' => array('*'),
            'deniedCallback' => array($this,'accessDeniedCallback'),
        );

        return $res;
    }

	public function beforeAction($action)
	{
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/admin.css');

		return parent::beforeAction($action);
	}

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery.ui');
        $cs->registerCssFile	($cs->getCoreScriptUrl() . '/jui/css/base/jquery-ui.css');

        $repo = LearningRepositoryObject::model();
        $repo->attributes = $_REQUEST['LearningRepositoryObject'];

        // Assign the "currentCentralRepoNodeId" only if the user just clicked on certain Node
        $selectedId = Yii::app()->request->getParam("id", false);
        if ($selectedId) {
            Yii::app()->session['currentCentralRepoNodeId'] = $selectedId;
        }
        $dataProvider = $repo->dataProvider();

        if(Yii::app()->request->getIsAjaxRequest()){
            $this->renderPartial('_losGrid', array(
                'repo' => $repo,
                'dataProvider' => $dataProvider,
                'data_grid_id' => 'repo-objects-grid',
                'filter_form_id' => 'repo-management-form'
            ));
            return;
        }

        $usedTypes = self::getAllObjectTypes(false, true);
        $usedCSPs = self::getAllCSPs(true);

        // User Selector
        UsersSelector::registerClientScripts();

        if (PluginManager::isPluginActive('LectoraApp') && !empty(Settings::get('lectora_url'))) {
            $cs = Yii::app()->getClientScript();
            $cs->registerCssFile(Yii::app()->baseUrl.'/../plugins/LectoraApp/assets/css/lectora.css');
        }

        $this->render('index', array(
            'repo' => $repo,
            'usedTypes' => $usedTypes,
            'usedCSPs' => $usedCSPs,
            'dataProvider' => $dataProvider
        ));
    }

    public function actionObjectsAutocomplete(){
        $requestData = Yii::app()->request->getParam('data', array());
        $skipScormTincan = strpos(Yii::app()->request->urlReferrer, 'session') !== false;

        $query = '';
        if(!empty($requestData['query'])) {
            $query = addcslashes($requestData['query'], '%_');
        }
        $objectsQuery = Yii::app()->db->createCommand()
            ->select(array('id_object', 'title'))
            ->from(LearningRepositoryObject::model()->tableName())
            ->where(array('like', 'title', "%".$query."%"));

        if($skipScormTincan) {
            $excludedTypes = array(LearningOrganization::OBJECT_TYPE_SCORMORG, LearningOrganization::OBJECT_TYPE_AICC, LearningOrganization::OBJECT_TYPE_ELUCIDAT, LearningOrganization::OBJECT_TYPE_TINCAN);
            $objectsQuery->andWhere(array("not in", "object_type", $excludedTypes));
        }

        $objects = $objectsQuery->queryAll(true);

        $list = CHtml::listData($objects, 'id_object', 'title');
        $this->sendJSON(array('options' => $list));
    }

    public function actionPushToCourse(){
        $assetsPath = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin.js'));
        Yii::app()->clientScript->registerCoreScript('jquery.ui');
        Yii::app()->clientScript->registerScriptFile($assetsPath . '/jquery.cookie.js');


        $repo = LearningRepositoryObject::model();
        $repo->attributes = $_REQUEST['LearningRepositoryObject'];

        $usedTypes = self::getAllObjectTypes();

        $this->renderPartial('_push_lo_to_courses',array(
            'repo' => $repo,
            'usedTypes' => $usedTypes,

        ), false, true);
    }

    /**
     * Edit version
     * @throws CException
     * @throws CHttpException
     */
    public function actionAxDelete() {
        $rq = Yii::app()->request;
        $id = $rq->getParam('id_object', '');
        $learningRepository = LearningRepositoryObject::model()->findByAttributes(array('id_object' => $id));
        $coursesCount = count(LearningOrganization::getLOsForLORepo($id));

        if ($rq->getParam('confirm', false) !== false) {
            // Form submission part
            try {
                foreach($learningRepository->versions as $version) {
                    $version->delete();
                }
				Log::_('DELETION: Object with id '.$id.' deleted from CLOR by user '.Yii::app()->user->id, CLogger::LEVEL_WARNING);
                echo '<a class="auto-close"></a>';
            } catch (CException $e) {
                Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
                $this->renderPartial('/_common/_errorMessage', array(
                    'message' => $e->getMessage()
                ));
            }
            Yii::app()->end();
        }

        $this->renderPartial('delete', array(
            'model' => $learningRepository,
            'coursesCount' => $coursesCount
        ));
    }

    /**
     * Edit version
     * @throws CException
     * @throws CHttpException
     */
    public function actionAxSimpleDelete() {
        $rq = Yii::app()->request;
        $id = $rq->getParam('id_object', '');
        $learningRepository = LearningRepositoryObject::model()->findByAttributes(array('id_object' => $id));

        if ($rq->getParam('confirm', false) !== false) {
            // Form submission part
            try {
                foreach($learningRepository->versions as $version) {
                    $version->delete();
                }
                $learningRepository->delete();
                echo '<a class="auto-close"></a>';
            } catch (CException $e) {
                Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
                $this->renderPartial('/_common/_errorMessage', array(
                    'message' => $e->getMessage()
                ));
            }
            Yii::app()->end();
        }

        $this->renderPartial('simpleDelete', array(
            'model' => $learningRepository
        ));
    }

    public function createLoUrl($lo_type, $action, $params = array()){
        return Docebo::createAbsoluteUrl('centralrepo/' . $lo_type . 'Lo/' . $action, $params);
    }


    /**
     * @param bool|false $removeSETA - S.E.T.A. - SCORM, Elucidat, TinCan, AICC
     * @return array
     */
    public static function getAllObjectTypes($removeSETA = false, $prefix = false){
        $usedTypes = array();
        $usedTypesQuery = Yii::app()->db->createCommand()
            ->select('object_type')
            ->from(LearningRepositoryObject::model()->tableName())
            ->group('object_type')
            ->queryColumn();
        $objectTypesTitles = LearningOrganization::objectTypes();
        foreach($usedTypesQuery as $type){
            if(!$removeSETA || ($removeSETA && ($type != LearningOrganization::OBJECT_TYPE_AICC && $type != LearningOrganization::OBJECT_TYPE_ELUCIDAT && $type != LearningOrganization::OBJECT_TYPE_TINCAN && $type != LearningOrganization::OBJECT_TYPE_SCORMORG)))
            {
                $usedTypes[$type] = ($prefix ? Yii::t('standard', '_TYPE').': ' : '').$objectTypesTitles[$type];
            }
        }

        return $usedTypes;
    }


    /**
     * Retrieve all used C.S.P.s in central repo objects
     * @return array
     */
    public static function getAllCSPs($prefix = false) {
        $output = array();
        $query = Yii::app()->db->createCommand()
          ->select('lpi.id, lpi.name')
          ->from(LearningRepositoryObject::model()->tableName().' lpo')
          ->join('lti_providers_info lpi', "lpo.from_csp = lpi.id")
          ->group('lpi.id')
          ->query();
        if ($query) {
            while ($record = $query->read()) {
                $output[$record['id']] = ($prefix ? Yii::t('marketplace', 'C.S.P.').': ' : '').$record['name'];
            }
        }
        return $output;
    }


    public function actionGetLosGrid() {

        Yii::app()->session['currentCentralRepoNodeId'] = Yii::app()->request->getParam("id", LearningCourseCategoryTree::getRootNodeId());

        $this->renderPartial('_losGrid', array(
            'repo' => LearningRepositoryObject::model(),
            'data_grid_id' => 'repo-objects-grid',
            'filter_form_id' => 'repo-management-form'
        ));
    }

    public function actionViewUsage(){
        $request = Yii::app()->request;
        /**
         * @var $request CHttpRequest
         */

        $idObject = $request->getParam('id_object', false);
        $searchInput = $request->getParam('search-input', false);

        if($idObject){
            $command = Yii::app()->db->createCommand();
            /**
             * @var $command CDbCommand
             */
            $command->select(array(
                    "lc.name as courseName",
                    "lrov.version_name as versionName",
                    "lrov.create_date as createDate",
                    "CASE WHEN (SELECT version from learning_repository_object_version WHERE id_object = lrov.id_object ORDER BY version DESC LIMIT 1) = lrov.version THEN ' Latest Version ' ELSE '' END as versionStatus",
                    "lrov.author as authorData",
                    "lc.idCourse",
                    "lc.course_type",
                    "lrov.object_type as object_type"
                ))
                ->from(LearningOrganization::model()->tableName() . ' lo')
                ->leftJoin(LearningCourse::model()->tableName() . ' lc', 'lo.idCourse = lc.idCourse')
                ->leftJoin(LearningRepositoryObjectVersion::model()->tableName() . ' lrov', 'lo.idResource = lrov.id_resource AND lo.objectType = lrov.object_type')
                ->where('lo.id_object = ' . $idObject);
            if($searchInput !== false){
                $command->andWhere('lc.name LIKE "%' . $searchInput . '%"');
            }
                $command->order('lc.idCourse');

            $commandCounter = clone $command;
            $commandCounter->select('count(*)');
            $numRecords = $commandCounter->queryScalar();

            $pageSize = Settings::get('elements_per_page', 10);

            $config = array(
                'totalItemCount' => $numRecords,
                'pagination' => array('pageSize' => $pageSize),
                'keyField' => 'id_resource',
            );

            $dataProvider = new CSqlDataProvider($command, $config);
            $model = LearningRepositoryObject::model()->findByPk($idObject);

            $this->renderPartial('_usage', array(
                'dataProvider' => $dataProvider,
                'idObject' => $idObject,
                "model" => $model
            ), false, true);
        }
    }

    public function actionCourseUsegaAutocomplete() {
        $requestData = Yii::app()->request->getParam('data', array());
        $idObject = Yii::app()->request->getParam("idObject", false);

        $query = '';
        if(!empty($requestData['query'])) {
            $query = addcslashes($requestData['query'], '%_');
        }

        if(!$query){
            $term = Yii::app()->request->getParam('term', '');
            $query = addcslashes($term, '%_');
        }

        $criteria =  new CDbCriteria();
        $criteria->condition = "(name LIKE :match OR name LIKE :match2) AND lro.id_object = :object";
        $criteria->join =  "LEFT JOIN learning_organization lo ON lo.idCourse = t.idCourse";
        $criteria->join .= " LEFT JOIN learning_repository_object lro ON lro.id_object = lo.id_object";
        $criteria->params = array(
            ':match' => "/%$query%",
            ':match2' => "%$query%",
            ':object' => $idObject
        );

        $courses = LearningCourse::model()->findAll($criteria);
        if(isset($requestData['formattedCourseName']) && $requestData['formattedCourseName']) {
            $coursesList = CHtml::listData($courses, 'idCourse', 'courseNameFormatted');
        } else {
            $coursesList = CHtml::listData($courses, 'idCourse', 'name');
        }


        $this->sendJSON(array('options' => $coursesList));
    }

    /**
     * Pushes a course local object into the central repository
     */
    public function actionPushFromLocalCourse() {

        // Check input params
        $idOrg = Yii::app()->request->getParam("id_org", false);
        $options = Yii::app()->request->getParam("options", array());
        $confirmPush = Yii::app()->request->getParam('confirm_push_lo', false);
        $loModel = LearningOrganization::model()->findByPk($idOrg);
        if (!$loModel) {
            echo Yii::t('standard', '_OPERATION_FAILURE');
            Yii::app()->end();
        }

        // Is this a post request
        if (Yii::app()->request->isPostRequest && $confirmPush) {
            try {
                // Push to central LO
                $sharedLo = LearningRepositoryObject::createFromCourseLearningObject($loModel, $options);
                if($sharedLo) {
                    echo "<a class='auto-close success'></a>";
                    Yii::app()->end();
                } else
                    throw new CException(Yii::t("standard", "Unable to push training material to Central Repository"));
            } catch (CException $e) {
                echo $e->getMessage();
                Yii::app()->end();
            }
        } else {
            // Default is shared tracking
            $options['local_tracking'] = false;

            // Need to ask for confirmation first
            $this->renderPartial('_push_to_central_lo', array(
                'loModel' => $loModel,
                'options' => $options
            ));
        }
    }

    /**
     * Action return the status of the video inside CLOR
     *
     * Statuses:
     * not_found - wrong ID of version
     * in_progress - video still waiting to be converted
     * converted - video is ready to be played
     *
     * Required parameters: id_object - learning_object_repository ID
     */
    public function actionCheckVideoConversionStatus(){
        $idObject = Yii::app()->request->getParam('id_object', false);
        $status = 'not_found';
        $response = new AjaxResult();
        if($idObject){
            $objectModel = LearningRepositoryObject::model()->findByPk($idObject);
            /**
             * @var $objectModel LearningRepositoryObject
             */
            if($objectModel && $objectModel->object_type === LearningOrganization::OBJECT_TYPE_VIDEO){
                $versionData = $objectModel->getLatestVersion();

                if($versionData){
                    $versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
                        'id_resource' => $versionData['id_resource'],
                        'object_type' => $versionData['object_type']
                    ));

                    if($versionModel){
                        switch ($versionModel->visible){
                            case 0:
                            case 1:
                                $status = 'converted';
                                break;
                            case -1:
                                $status = 'in_progress';
                                break;
                            case -2:
                                $status = 'error';
                                break;
                        }
                    }
                }
            }
        }

        $response->setStatus(true)->setData(array('status' => $status))->toJSON();
        Yii::app()->end();
    }
}