<?php

class CentralRepoPreviewController extends Controller {

    public $_playerAssetsUrl;

    public function filters() {
        return array(
            'accessControl' => 'accessControl',
        );
    }

    public function accessRules() {

        $res = array();

        //Additional permission check for the courseAutocomplete function
        $res[] = array(
            'allow',
            'roles' => array('/framework/admin/setting/view'),
            'actions' => array(
                'preview',
                'axLoadObject',
                'keepAlive',
                'sendFile',
                'getScormItem',
                'getAiccItem'
            ),
        );

        $res[] = array(
            'deny',
            // deny all
            'users' => array('*'),
            'deniedCallback' => array($this,'accessDeniedCallback'),
        );

        return $res;
    }

	public function beforeAction($action)
	{
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/admin.css');

		return parent::beforeAction($action);
	}


    public function actionPreview($id_object){

        $obj = Yii::app()->request->getParam('id_object', $id_object);
        $version = Yii::app()->request->getParam('version', null);
        $model = LearningRepositoryObjectVersion::getInstanceVersion($obj, $version);
        if($model->object_type === LearningOrganization::OBJECT_TYPE_VIDEO){
            $cs = Yii::app()->getClientScript();
            /**
             * @var $cs CClientScript
             */
            $assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias("lms.protected.modules.video.assets"));
            $cs->registerScriptFile($assetsUrl.'/js/embedPlayer.js');
            $cs->registerCssFile($assetsUrl . '/css/playerStyle.css');
        }
        $objectPreview = Preview::getPreviewManager($model);
        $objectPreview->registerResources();

        $versionsCount = LearningRepositoryObject::getObjectVersionsCount($model->id_object);
        $hasVersioning = in_array($model->object_type, LearningRepositoryObject::$objects_with_versioning ) && $versionsCount > 1;

        $this->renderPartial('index', array(
            'model' => $model,
            'objectPreview' => $objectPreview,
            'hasVersioning' => $hasVersioning
        ), false, true);
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionAxLoadObject() {

        $type = Yii::app()->request->getParam('type', null);
        if(method_exists($this,$type)){
            $this->$type();
        }else{
            $this->accessDeniedCallback();
        }
    }

    private function loadObject(){
        $idObject = (int)Yii::app()->request->getParam('id_object', 0);
        $version = (int)Yii::app()->request->getParam('version', 0);
        return LearningRepositoryObjectVersion::model()->findByAttributes(array(
            'id_object' => $idObject,
            'version' => $version
        ));
    }

    /*************** Used from the FIles LO *******************/
    /**
     * Send file to user. Only for files stored in local file system!
     *
     * @throws CException
     */
    public function actionSendFile() {

        $response = new AjaxResult();
        try {
            $idFile = (int)Yii::app()->request->getParam('id_file', 0);
            $file = LearningMaterialsLesson::model()->findByPk($idFile);
            if (!$file) {
                throw new CException('Invalid file ID: '.$idFile);
            }

            $object = $this->loadObject();
            if (!$object) {
                throw new CException('Invalid object ID: '.$idFile);
            }

            UserLimit::logActiveUserAccess(Yii::app()->user->id);

            // Use file storage manage
            $storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_FILE);
            $file->original_filename = Sanitize::fileName($file->original_filename,true);
            $storageManager->sendFile($file->path, $file->original_filename);

        } catch (CException $e) {
            //... TO DO : show an error page ...
            echo $e->getMessage();
        }
    }

    public function getQuestionViewsPath(){
        $idObject = Yii::app()->request->getParam('id_object', null);
        $type = Yii::app()->db->createCommand()
            ->select('object_type')
            ->from(LearningRepositoryObjectVersion::model()->tableName())
            ->where('id_object = :id', array(':id' =>$idObject))
            ->queryScalar();
        return '../../../'.$type.'/views/player/playQuestion/';
    }

    public function getTest(){
        $idObject = Yii::app()->request->getParam('id_object', null);
        $version = Yii::app()->request->getParam('version', null);
        $query = Yii::app()->db->createCommand()
            ->select('id_resource')
            ->from(LearningRepositoryObjectVersion::model()->tableName())
            ->where('id_object = :id', array(':id' =>$idObject));
        if($version !== null){
            $query->andWhere('version = :ver', array(':ver' => $version));
        }
        $idResource = $query->queryScalar();
        return LearningTest::model()->findByPk($idResource);
    }

    public function getPoll(){
        $idObject = Yii::app()->request->getParam('id_object', null);
        $version = Yii::app()->request->getParam('version', null);
        $query = Yii::app()->db->createCommand()
            ->select('id_resource')
            ->from(LearningRepositoryObjectVersion::model()->tableName())
            ->where('id_object = :id', array(':id' =>$idObject));
        if($version !== null){
            $query->andWhere('version = :ver', array(':ver' => $version));
        }
        $idResource = $query->queryScalar();
        return LearningPoll::model()->findByPk($idResource);
    }

    public function actionGetScormItem(){

        $response = new AjaxResult();

        $id_resource    = Yii::app()->request->getParam("id_resource", 0);
        $id_object      = Yii::app()->request->getParam("id_object", 0);
        $id_scorm_item  = Yii::app()->request->getParam("idscorm_item", 0);

        try {

            $scorm_item = LearningScormItems::model()->findByPk($id_scorm_item);
            if (!$scorm_item) {
                throw new CException('Invalid Scorm item: ' . $id_scorm_item);
            }

            $model = LearningRepositoryObjectVersion::model()->findByAttributes(array('id_object' => $id_object, 'id_resource' => $id_resource));
            $objectPreview = Preview::getPreviewManager($model);
            $data = $objectPreview->buildScoInfo($scorm_item, $model, Yii::app()->user->Id);

            $response->setStatus(true);
            $response->setData($data);
            $response->toJSON();
            Yii::app()->end();

        }
        catch (CException $e) {
            $response->setStatus(false)->setMessage($e->getMessage())->toJSON();
            Yii::app()->end();
        }
    }

    public function actionGetAiccItem(){

        $response = new AjaxResult();

        $id_object      = Yii::app()->request->getParam("id_object", 0);
        $id_resource    = Yii::app()->request->getParam("id_resource", 0);
        $idAiccItem 	= Yii::app()->request->getParam("id_aicc_item", 0);

        try {
            $item = LearningAiccItem::model()->findByPk($idAiccItem);
            if (!$item) {
                throw new CException('Invalid Aicc item: ' . $idAiccItem);
            }

            $session = 'preview_mode';
            $launchUrl = $item->previewUrl($session, $id_object);

            $data = array(
                'launch_url'	=> $launchUrl,
                'type'          => LearningOrganization::OBJECT_TYPE_AICC
            );

            $response->setStatus(true);
            $response->setData($data);
            $response->toJSON();
            Yii::app()->end();

        }
        catch (CException $e) {
            $response->setStatus(false)->setMessage($e->getMessage())->toJSON();
            Yii::app()->end();
        }
    }


}
