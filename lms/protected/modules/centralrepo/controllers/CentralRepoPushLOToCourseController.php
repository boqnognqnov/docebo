<?php
/**
 * Created by PhpStorm.
 * User: NikVasilev
 * Date: 27.4.2016 г.
 * Time: 16:20
 */
class CentralRepoPushLOToCourseController extends DoceboWizardController {
    /**
     * Filters actions based on permissions
     * @return array
     */
    public function filters()
    {
        return array(
            'accessControl' => 'accessControl',
        );
    }

	public function beforeAction($action)
	{
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/admin.css');

		return parent::beforeAction($action);
	}

	/**
     * Returns list of allowed actions.
     * @return array actions configuration.
     */
    public function accessRules()
    {
        $res = array();

        $res[] = array(
            'allow',
            'users' => array('@')
        );

        $res[] = array(
            'deny',
            'users' => array('*'),
            'deniedCallback' => array($this, 'accessDeniedCallback'),
        );

        return $res;
    }


    public function actionWizard()
    {
        $params = array();
        $params['skipCoursesStep'] = Yii::app()->request->getParam('skipCoursesStep',false);
        $params['selected_courses'] = Yii::app()->request->getParam('selected_courses',false);
        $params['singleObjectId'] = Yii::app()->request->getParam('singleObjectId',false);
        $enableRecallOnSameStep = true;
        if($params['singleObjectId']){
            $enableRecallOnSameStep = false;
        }

        $this->navigate($params, $enableRecallOnSameStep);
    }

    /**
     * Returns an array of step methods that this wizard has (ordered)
     * @throws \CException
     * @return array
     */
    public function steps () {
        return array(
            array(
                'alias'   => 'selectLearningObjects',
                'handler' => array( $this, 'selectLearningObjects' ),
                'validator' => function ($stepName = '', $inData = array(), $params = array(), $goBack, &$nextStep){

                    $singleObjectId = Yii::app()->request->getParam('singleObjectId',false);
                    $selectedObjects = Yii::app()->request->getParam('selectedObjects','[]');
                    $selectedObjects = CJSON::decode($selectedObjects);

                    if((!is_array($selectedObjects) || empty($selectedObjects) )  && !$singleObjectId){
                        Yii::app()->user->setFlash( 'error', Yii::t( 'standard', 'Please select training materials' ) );
                        Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;
                        Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
                        return FALSE;
                    }

                    //CASE: Pushing from course page
                    if(isset($params['skipCoursesStep']) && $params['skipCoursesStep'] == true){
                        $nextStep = 'finished';
                    }

                    return TRUE;
                }
            ),
            array(
                'alias'     => 'selectCourses',
                'handler'   => array( $this, 'selectCourses' ),
                'validator' => function ( $fromStep, $dataFromAllSteps, $params = array(), $goBack, &$nextStep ) {

                    if($goBack){
                        return true;
                    }

                    $collectedData = $this->collectStepData();
                    $selectedSingleObject = Yii::app()->request->getParam('selectedSingleObject',false);
                    $singleObjectId = Yii::app()->request->getParam('idGeneral',false);
                    if(isset($collectedData['selectedObjects'])){
                        $objects = CJSON::decode($collectedData['selectedObjects']);
                    }
                    if($selectedSingleObject) {
                        $objects = CJSON::decode($selectedSingleObject);
                    }

                    if(!$objects || !is_array($objects) || empty($objects)){
                        Yii::app()->user->setFlash( 'error', Yii::t( 'standard', 'No learning object to be pushed' ) );
                        return FALSE;
                    }

                    if(isset($_REQUEST['selected_courses'])) {
                        $selectedCourses = UsersSelector::getCoursesListOnly($_REQUEST);
                    }

                    if(!isset($_REQUEST['selected_courses']) || !$selectedCourses || !is_array($selectedCourses) || empty($selectedCourses)) {
                        Yii::app()->user->setFlash( 'error', Yii::t( 'standard', 'Please select courses' ) );
                        return FALSE;
                    }


                    // By default the next step is the "finished"
                    // But if this is Pushing of Single object and the object has many versions
                    // that can override the old one in certain courses
                    // Let's show to the user, so he can take care of it
                    $nextStep = 'finished';
                    if( $singleObjectId ){
                        // Check if there is potential risk of overriding object versions in course
                        $goConfirm = LearningRepositoryObject::canObjectInCourseBeOverridden($singleObjectId);
                        if( $goConfirm == true) {
                            // Ok there is a risk of overriding a Object version in course
                            // but we must check if any of the courses will be affected
                            $currentObject = reset($objects);
                            $affectedCourses = false;
                            // Get the id_resource of the Object depending on hi's idObject and version_id
                            $resource = LearningRepositoryObject::getVersionByObjectAndVersion($singleObjectId, $currentObject["versionId"], array("id_resource") );
                            foreach($selectedCourses as $courseId){
                                // Now we only should check if the Selected LearningRepositoryObject participate in the given course
                                // BUT NOT with the current selected version AND IF there are users in progress
                                $participate = Yii::app()->db->createCommand()
                                    ->select("count(*)")
                                    ->from(LearningOrganization::model()->tableName() . " lo")
                                    ->leftJoin(LearningCommontrack::model()->tableName() . " lct", "lct.idReference = lo.idOrg")
                                    ->where(array(
                                        "and",
                                        "id_object = :object",
                                        "idCourse = :course",
                                        "lo.idResource != :resource",
                                        array("OR", "lct.status = :abInito", "lct.status = :attempted")
                                    ))
                                    ->queryScalar(array(
                                        ":object" => $singleObjectId,
                                        ":course" => $courseId,
                                        ":resource" => $resource["id_resource"],
                                        ":abInito" => LearningCommontrack::STATUS_AB_INITIO,
                                        ":attempted" => LearningCommontrack::STATUS_ATTEMPTED
                                    ));
                                if($participate){
                                    $affectedCourses = true;
                                    break;
                                }
                            }

                            // If there are any affected courses then let's show this to the user
                            $nextStep = $affectedCourses ? 'confirm' : 'finished';
                        }

                    }

                    return TRUE;
                }
            ),
            // This Step is only Required when pushing Single object to many courses
            array(
                'alias'     => 'confirm',
                'handler'   => array( $this, 'confirm' ),
                'validator' => function ( $fromStep, $dataFromAllSteps, $params = array(), $goBack, &$nextStep ) {

                    $objects = null;
                    $wizParam = Yii::app()->request->getParam("Wizard", false);
                    $obj = isset($wizParam['courses-users-selector']['selectedSingleObject']) && $wizParam['courses-users-selector']['selectedSingleObject'] ? $wizParam['courses-users-selector']['selectedSingleObject'] : '';
                    if($obj) {
                        $objects = CJSON::decode($obj, true);
                    }

                    if(!$objects || !is_array($objects) || empty($objects)){
                        throw new CException( 'No objects to push into courses in Pushing wizard' );
                    }

                    if (!$goBack) {
                        $selectedCourses = CJSON::decode($dataFromAllSteps['selected_courses'], true);
                        $potentially_affected = CJSON::decode($dataFromAllSteps['potentially_affected'], true);
                        $moreTobeChanged = count($selectedCourses) - count($potentially_affected);
                        $showError = false;
                        // If there are more Courses that are not listed but it will be changed we will go directly
                        if ($moreTobeChanged) {
                            $showError = false;
                        } else if (isset($dataFromAllSteps['force_change_courses']) && $dataFromAllSteps['force_change_courses'] == '[]') {
                            // But if there are no other courses then we rely only on the user's selection
                            $showError = true;
                        }

                        if( $showError) {
                            Yii::app()->user->setFlash( 'error', Yii::t( 'standard', 'Please select courses' ) );
                            return FALSE;
                        }
                    }

                    return TRUE;
                }
            ),
            array(
                'alias'     => 'finished',
                'handler'   => array( $this, 'finished' ),
                'validator' => function ( $fromStep, $dataFromAllSteps, $params = array() ) {
                    $collectedData = $this->collectStepData();
                    $selectedSingleObject = Yii::app()->request->getParam('selectedSingleObject',false);
                    if(isset($collectedData['selectedObjects'])){
                        $objects = CJSON::decode($collectedData['selectedObjects']);
                    }
                    if($selectedSingleObject) {
                        $objects = CJSON::decode($selectedSingleObject);
                    }

                    if(!$objects || !is_array($objects) || empty($objects)){
                        throw new CException( 'No objects to push into courses in Pushing wizard' );
                    }

                    if(isset($_REQUEST['selected_courses'])) {
                        $selectedCourses = UsersSelector::getCoursesListOnly($_REQUEST);
                    }

                    if(!isset($_REQUEST['selected_courses']) || !$selectedCourses || !is_array($selectedCourses) || empty($selectedCourses)) {
                        throw new CException( 'No courses to push LOs into in Pushing wizard' );
                    }

                    return TRUE;
                }
            ),
        );
    }


    /** Handler for STEP #1 - LOs
     * @param array $params
     * @throws CException
     */
    public function selectLearningObjects ( $params = array() ) {

        $repo = LearningRepositoryObject::model();
        $repo->attributes = $_REQUEST['LearningRepositoryObject'];

        //SCORM, Elucidat, TinCan or AICC (S.E.T.A.)
        $removeSETA = false;
        $courseType = Yii::app()->request->getParam('courseType',false);
        if($courseType == LearningCourse::TYPE_CLASSROOM || $courseType == LearningCourse::TYPE_WEBINAR){
            $removeSETA = true;
        }


        $usedTypes = CentralRepoController::getAllObjectTypes($removeSETA);

        $options = array(
            'id' => Yii::app()->request->getParam('id', LearningCourseCategoryTree::getRootNodeId()),
            'courseId' => Yii::app()->request->getParam('courseId',false),
            'courseType' => $courseType,
            'containChildren' => Yii::app()->request->getParam('pushContainChildren',false)
        );

        $dataProvider = $repo->pushDataProvider($options);
        $skipCoursesStep = Yii::app()->request->getParam('skipCoursesStep',false);
        $actionParams = array(
            'repo' => $repo,
            'usedTypes' => $usedTypes,
            'dataProvider' => $dataProvider,
            'skipCoursesStep' => $skipCoursesStep,
            'courseId' => Yii::app()->request->getParam('courseId',false),
            'dataGridId' => Yii::app()->request->getParam("ajax", 'lorepo-push-lo-modal-objects-grid-' . time())
        );

        $stepHtml = $this->renderPartial('_push_lo_to_courses', $actionParams, true, false);

        $stepAlias = 'selectLearningObjects';
        $this->renderStep( $stepAlias, $stepHtml, false, false, false, false, (($skipCoursesStep)? Yii::t('standard', '_CONFIRM') : false) );
    }


    /** Handler for STEP #2 - Courses
     * @param array $params
     */
    public function selectCourses( $params = array() ){

        //Check if any of the selected LO's are of type SCORM, Elucidat, TinCan or AICC (S.E.T.A.)
        $collectedData = $this->collectStepData();
        $singleObjectId = Yii::app()->request->getParam('singleObjectId', false);
        $selectedSingleObject = Yii::app()->request->getParam('selectedSingleObject', false);
        $wizParams = Yii::app()->request->getParam("Wizard", false);
        $confirmSelectedObject = isset($wizParams['courses-users-selector']['selectedSingleObject']) && $wizParams['courses-users-selector']['selectedSingleObject'] ? $wizParams['courses-users-selector']['selectedSingleObject'] : '';
        if(isset($collectedData['selectedObjects'])){
            $objects = CJSON::decode($collectedData['selectedObjects']);
        }
        elseif($singleObjectId){
            $objects = CJSON::decode('[{"objectId":"'.$singleObjectId.'"}]');
        } elseif ($selectedSingleObject) {
            $objects = CJSON::decode($selectedSingleObject, true);
            $objId = reset($objects);
            $singleObjectId = $objId["objectId"];
        } elseif ($confirmSelectedObject) {
            $objects = CJSON::decode($confirmSelectedObject, true);
            $objId = reset($objects);
            $singleObjectId = $objId["objectId"];
        }

        $hasSETA = false;
        foreach($objects as $obj){
            $type = LearningRepositoryObject::getObjectTypeById($obj['objectId']);

            if($type == LearningOrganization::OBJECT_TYPE_AICC
                || $type == LearningOrganization::OBJECT_TYPE_ELUCIDAT
                || $type == LearningOrganization::OBJECT_TYPE_SCORMORG
                || $type == LearningOrganization::OBJECT_TYPE_TINCAN)
            {
                $hasSETA = true;
                break;
            }
        }

        $nextButtonLabel =  Yii::t('standard', '_CONFIRM');
        if( $singleObjectId && isset($type)){
            $canParticipate = LearningRepositoryObject::canObjectInCourseBeOverridden($singleObjectId);
            if ( $canParticipate === true ) {
                $nextButtonLabel = Yii::t('standard', '_NEXT');
            }
        }

        $userSelectorParams = array(
            'type'  		=> UsersSelector::TYPE_CENTRALREPO_PUSH_TO_COURSE,
            'idForm' 		=> 'courses-users-selector',
            'name'          => 'selectCourses',
            'noyiixviewjs'	=> true,  // very important: main page already has widget(s) loading yiigridview.js, so, don't
            'tabs'          => UsersSelector::TAB_COURSES,
            'showLeftBar'   => false,
            'btnNextTxt'    => $nextButtonLabel,
            'collectorUrl'	=> Docebo::createAdminUrl('centralrepo/CentralRepoPushLOToCourse/wizard', array(
                'from_step'     => 'selectCourses',
            )),
        );

        //Send the filter for the course dataprovider
        if($hasSETA){
            $userSelectorParams['filterCoursesByType'] = LearningCourse::ELEARNING;//'elearning';
        }

        //Additional top panel with filters for the second step when pushing for a single object from the index page
        if($singleObjectId){
            $userSelectorParams['tp'] = 'lms.protected.modules.centralrepo.views.centralRepoPushLOToCourse.courses_top_panel';
            $userSelectorParams['singleObjectId'] = $singleObjectId;
        }

        $usersSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', $userSelectorParams);

        $this->redirect($usersSelectorUrl, true);
    }

    /** Handler for STEP #3|4 - Confirm Pushing
     * @param array $params
     * @throws CException
     * @throws CHttpException
     */
    public function confirm( $params = array() ){

        Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;
        Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;

        $object = null;
        $singleObjectId = Yii::app()->request->getParam('selectedSingleObject',false);
        if ($singleObjectId){
            $object = CJSON::decode($singleObjectId, true);
        }

        // Collect the SelectedCourses of the User in the Confirmation Screen
        $forceChangeCourses = Yii::app()->request->getParam('force_change_courses', '[]');

        $object = reset($object);
        $resource = LearningRepositoryObject::getVersionByObjectAndVersion($object['objectId'], $object['versionId'], array("id_resource"));
        $options = array(
            'id_object' => $object['objectId'],
            'id_resource' => $resource['id_resource'],
            'courses' => CJSON::decode($params['selected_courses'], true)
        );
        $versionOverlaps = LearningRepositoryObject::getCourseObjectVersionOverlapping($options);
        $potentially_affected = CJSON::encode($versionOverlaps['keys']);
        $agreeMessage = Yii::t("app7020", "Yes, I want to proceed!");

        $selectedCoursesArray = CJSON::decode($params['selected_courses'], true);
        $moreTobeChanged = count($selectedCoursesArray) - $versionOverlaps['dataProvider']->getItemCount(true);
        if ($moreTobeChanged) {
            $versionName = Yii::app()->db->createCommand()
                ->select("version_name")
                ->from(LearningRepositoryObjectVersion::model()->tableName())
                ->where(array(
                    "AND",
                    "id_object = :object",
                    "version = :version"
                ))
                ->queryScalar(array(":object" => $object['objectId'], ":version" => $object['versionId']));
            $agreeMessage = Yii::t('central_repo', "Yes, I want to push {version} to the selected courses and {courses} more courses with no users in progress" ,
                array("{version}" => $versionName, "{courses}" => $moreTobeChanged));
        }

        $actionParams = array(
            'dataProvider' => $versionOverlaps['dataProvider'],
            'data_grid_id' => Yii::app()->request->getParam("ajax", 'lorepo-push-lo-modal-objects-confirm-grid-' . time()),
            'selectedSingleObject' => $singleObjectId,
            'forceChangeCourses' => $forceChangeCourses,
            'selected_courses' => $params['selected_courses'],
            'potentially_affected' => $potentially_affected,
            'idGeneral' => $object['objectId'],
            'agreeMessage' => $agreeMessage
        );

        $stepHtml = $this->renderPartial('_confirm_push', $actionParams, true, true);

        $stepAlias = 'confirm';
        $nextButtonLabel = Yii::t('standard', '_CONFIRM');
        $this->renderStep( $stepAlias, $stepHtml, false, false, false, $prevButtonLabel = false, $nextButtonLabel);
    }

    /** Handler for STEP #3|4 - Pushing
     * @param array $params
     * @throws CException
     * @throws CHttpException
     */
    public function finished( $params = array() ){
        $collectedData = $this->collectStepData();
        $selectedSingleObject = Yii::app()->request->getParam('selectedSingleObject',false);
        $parentId =  Yii::app()->request->getParam('folder_key', false);
        $newPush = true;
        $manyObjects = false;
        $objectName = '';
        if(isset($collectedData['selectedObjects'])){
            $manyObjects =  true;
            $objects = CJSON::decode($collectedData['selectedObjects']);
        }
        if($selectedSingleObject){
            $manyObjects = false;
            $objects = CJSON::decode($selectedSingleObject);
            $object = current($objects);
            $objectName = LearningRepositoryObject::model()->findByPk($object['objectId'])->title;
        }
        $selectedCourses = array();

        // Ok We have a Push of a single object
        $singleObjectId = Yii::app()->request->getParam("idGeneral", false);
        if($singleObjectId) {
            // By Default the selected Courses are those that are selected In the Initial Screen
            $selectedCourses = UsersSelector::getCoursesListOnly($_REQUEST);
            $obj = reset($objects);
            // We may have Learning Object That Potentially can override hi's versions in certain courses
            $canOverride = LearningRepositoryObject::canObjectInCourseBeOverridden($obj['objectId']);
            if($canOverride) {
                // If there are ANY courses that will FORCE override hi's object's version
                if (isset($collectedData['force_change_courses'])){
                    $forceChange = CJSON::decode($collectedData['force_change_courses'], true);
                    $potentially_affected = CJSON::decode($collectedData['potentially_affected'], true);
                    $selectedCourses = UsersSelector::getCoursesListOnly($_REQUEST);

                    // Let calculate which courses will be override
                    // First we must get the courses that was selected in the First STEP
                    // AND that are not presented as potential threat for overriding
                    // THEN we will get the courses that were selected as "force override"
                    // And MERGE them with $notPotentialThreat
                    $notPotentialThreat = array_diff($selectedCourses, $potentially_affected);
                    $selectedCourses = array_merge($notPotentialThreat, $forceChange);

                    // We have a CASE where some of the courses are NEW to the object and other that should be overriden with different version
                    // Let's get the Courses that should be UPDATED
                    $courseParticipation = LearningRepositoryObject::getCourseAssignmentsIds($obj['objectId']);
                    $coursesForUpdate = array_intersect($selectedCourses, $courseParticipation);
                    $coursesForAdd = array_diff($selectedCourses, $coursesForUpdate);
                    foreach($selectedCourses as $key => $course){
                        // If check if the course is for Adding and not for updating
                        if (in_array($course, $coursesForAdd) && !in_array($course, $coursesForUpdate)){
                            $selectedCourses[$key] = array('add' => true , 'idCourse' => $course);
                        }
                    }

                } else {
                    // We have a CASE where some of the courses are NEW to the object and other that should be overriden with different version
                    // Let's get the Courses that should be UPDATED
                    $courseParticipation = LearningRepositoryObject::getCourseAssignmentsIds($obj['objectId']);
                    $coursesForUpdate = array_intersect($selectedCourses, $courseParticipation);
                    $coursesForAdd = array_diff($selectedCourses, $coursesForUpdate);
                    foreach($selectedCourses as $key => $course){
                        // If check if the course is for Adding and not for updating
                        if (in_array($course, $coursesForAdd) && !in_array($course, $coursesForUpdate)){
                            $selectedCourses[$key] = array('add' => true , 'idCourse' => $course);
                        }
                    }
                }
                $newPush = false;
            }

        } elseif (isset($_REQUEST['selected_courses'])) {
            $selectedCourses = UsersSelector::getCoursesListOnly($_REQUEST);
        }

        reset($objects);

        //prepare an array in case some objects could not be pulled into destination courses due to C.S.P. limitations
        $cspErrors = array();
        $totalImports = count($selectedCourses) * count($objects);
        $effectiveImports = 0; //count how many objects we were effectively able to import into the course(s)

        foreach($selectedCourses as $key=>$idCourse){

            //--- CSP check ---
            //first retrieve course info (we need to know if the course is selling)
            /* @var $course LearningCourse */
            $course = LearningCourse::model()->findByPk($idCourse);
            if (!$course) {
                Yii::log('Invalid course #'.$idCourse, CLogger::LEVEL_ERROR);
                continue;
                //TODO: should we throw an exception?
            }
            //---

            foreach($objects as $obj){

                //--- CSP check ---
                if ($course->selling || $course->isPartOfSellingLearningPlans()) {
                    $object = LearningRepositoryObject::model()->findByPk($obj['objectId']);
                    if (!$object) {
                        Yii::log('Invalid repository object #'.$obj['objectId'], CLogger::LEVEL_ERROR);
                        continue;
                        //TODO: should we throw an exception?
                    }
                    if ($object->from_csp != '') {
                        //we have detected a non-allowed course, so store the course nad object info in errors list and
                        //go on with the next object, nothing else to do here.
                        $cspErrors[] = array(
                            'id_course' => $idCourse,
                            'id_object' => $obj['objectId'],
                            'title' => $object->title
                        );
                        continue;
                    }
                }
                //---

                $version = LearningRepositoryObjectVersion::model()->findByAttributes(array('id_object' => $obj['objectId'], 'version' => $obj['versionId']));
                // If this is new push to LO to Course, then PUSH it.... otherwise we will update the LO Version to course
                if($newPush){
                    // If object is video and it's in course already, just update version
                    $idExstingLo = Yii::app()->db->createCommand()
                        ->select("idOrg")
                        ->from(LearningOrganization::model()->tableName())
                        ->where("id_object = :idObject", array('idObject' => $version->id_object))
                        ->andWhere("idCourse = :idCourse", array('idCourse' => $idCourse))
                        ->andWhere("objectType = :objType", array('objType' => "video"))
                        ->queryScalar();

                    if($idExstingLo && !$idCourse['add']) {
                        $version->updateVersionToCourse($idCourse);
                    } else {
                        $version->pushToCourse($idCourse, $parentId);
                    }
                } else {
                    // Ok we have an UPDATE PUSH
                    // but there is CASE where the push can be update for some courses and ADD for other courses
                    if(isset($idCourse['add']) && $idCourse['add']) {
                        $version->pushToCourse($idCourse['idCourse']);
                    } else {
                        $version->updateVersionToCourse($idCourse);
                    }
                }

                $effectiveImports++;
            }
        }

        if(isset($params['skipCoursesStep']) && $params['skipCoursesStep'] == true){
            if (!empty($cspErrors)) {
                Yii::log('Not allowed to add the following CLOR objects into sellable courses: '.print_r($cspErrors, true), CLogger::LEVEL_ERROR);
                //TODO: how to handle error messsage?
                $this->renderPartial('_csp_errors', array(
                  'totalImports' => $totalImports,
                  'effectiveImports' => $effectiveImports,
                  'cspErrors' => $cspErrors
                ));
            } else {
                echo "<a class='auto-close'></a>";
            }
            Yii::app()->end();
        }


        $stepAlias = 'selectLearningObjects';
        $stepHtml = $this->renderPartial('_finished', array(
            'manyObjects' => $manyObjects,
            'objectName'    =>  $objectName,
            'countObjects' => count($objects),
            'countCourses' => count($selectedCourses),
            'cspErrors' => $cspErrors
        ), true, true);
        $this->renderStep( $stepAlias, $stepHtml, true, true, true);
    }

}