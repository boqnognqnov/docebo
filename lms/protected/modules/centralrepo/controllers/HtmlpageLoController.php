<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 14.4.2016 г.
 * Time: 9:48
 */
class HtmlpageLoController extends CentralLoController {
    public function actionEdit(){
        $htmlPageObj = new LearningHtmlpage();
        $request = Yii::app()->request;
        $loModel = new LearningRepositoryObject();
        /**
         * @var $request CHttpRequest
         */

        Yii::app()->tinymce->init();

        $cs = Yii::app()->getClientScript();
        $playerAssetsUrl = Yii::app()->findModule('player')->assetsUrl;
        $cs->registerCssFile ($playerAssetsUrl.'/css/base.css');

        $idObject = $request->getParam('id_object', false);
        if ($idObject) {

            $loModel = LearningRepositoryObject::model()->findByPk($idObject);
            /**
             * @var $repoObject LearningRepositoryObject
             */

            if($loModel){

                $versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
                    'id_object' => $loModel->id_object
                ));

                /**
                 * @var $versionModel LearningRepositoryObjectVersion
                 */

                if($versionModel){
                    $htmlPageObj = LearningHtmlpage::model()->findByPk($versionModel->id_resource);
                }
            }
        }

        $this->render("lms.protected.modules.htmlpage.views.default._edit_html",
            array(
                'htmlPageObj'=>$htmlPageObj,
                'lo_type'=>LearningOrganization::OBJECT_TYPE_HTMLPAGE,
                'saveUrl' => Docebo::createAbsoluteLmsUrl('centralrepo/htmlpageLo/save'),
                'iframeWhitelistUrl' => Docebo::createAbsoluteLmsUrl('centralrepo/htmlpageLo/axListAllowedIframeUrls'),
                'backButtonUrl' => Docebo::createAbsoluteLmsUrl('centralrepo/centralRepo/index'),
                'containerId' => 'player-centralrepo-uploader',
                'idObject' => $idObject,
                'loModel' => $loModel,
                'centralRepoContext' => true
            ));
    }

    public function actionSave(){
        $idObject = Yii::app()->request->getParam('id_object', false);
        $thumb = Yii::app()->request->getParam("thumb", null);
        $shortDescription = urldecode(Yii::app()->request->getParam("short_description", ''));

        $transaction = Yii::app()->db->beginTransaction();

        try {
            if (isset($_POST['LearningHtmlpage'])) {
                $postData = $_POST['LearningHtmlpage'];
                $postData['title'] = trim($postData['title']);
                $postData['textof'] = Yii::app()->htmlpurifier->purify($postData['textof'], 'allowFrameAndTargetLo');
            }

            // Validate input parameters
            if (empty($postData['title'])) { throw new CException(Yii::t('error', 'Enter a title!')); }


            $learningObject = null;

            // edit operation
            if (intval($postData['idPage']) > 0) {
                // get htmlpage object
                $htmlPageObj = LearningHtmlpage::model()->findByPk($postData['idPage']);
            } else {
                $htmlPageObj = new LearningHtmlpage();
                $htmlPageObj->author = Yii::app()->user->getIdst();
            }

            $htmlPageObj->detachBehavior('learningObjects');

            //prepare AR object to be saved
            $htmlPageObj->attributes = $postData;

            //do actual saving
            if (!$htmlPageObj->save()) {
                $errorsHtmlPage = $htmlPageObj->getErrors();
                $returnErrors = Yii::t('course', 'Error while saving HTML content'). ': <br /><ul class="customErrorSummary">';
                foreach($errorsHtmlPage as $errorsHtmlPageField) {
                    if (is_array($errorsHtmlPageField))  {
                        foreach($errorsHtmlPageField as $errorHtmlPage) {
                            $returnErrors .= '<li>' . $errorHtmlPage . '</li>';
                        }
                    }
                }

                $returnErrors .= '</ul>';

                throw new CException($returnErrors);
            }

            /**
             * @var $repoObject LearningRepositoryObject
             */

            if($idObject){
                $repoObject = LearningRepositoryObject::model()->findByPk($idObject);
                $repoObject->title = $htmlPageObj->title;
                $repoObject->short_description = Yii::app()->htmlpurifier->purify($shortDescription);
	            if($thumb){
		            $repoObject->resource = intval($thumb);
	            }elseif($repoObject->resource){
		            $repoObject->resource = 0;
	            }


                if(!$repoObject->save()){
                    Yii::log(var_export($repoObject->getErrors(),true), CLogger::LEVEL_ERROR);
                }
            } else{
                $repoObject = new LearningRepositoryObject();
                $repoObject->title = $postData['title'];
                $repoObject->short_description = Yii::app()->htmlpurifier->purify($shortDescription);
                $repoObject->resource = intval($thumb);
                $categoryNodeId = LearningCourseCategoryTree::getRootNodeId();
                if(isset(Yii::app()->session['currentCentralRepoNodeId'])){
                    $categoryNodeId = Yii::app()->session['currentCentralRepoNodeId'];
                }
                $repoObject->id_category = $categoryNodeId;
                $repoObject->params_json = CJSON::encode(array());
                $repoObject->object_type = LearningOrganization::OBJECT_TYPE_HTMLPAGE;

                if($repoObject->save()){
                    $versionModel = new LearningRepositoryObjectVersion();
                    $versionModel->id_resource = $htmlPageObj->idPage;
                    $versionModel->object_type = LearningOrganization::OBJECT_TYPE_HTMLPAGE;
                    $versionModel->version = 1;
                    $versionModel->id_object = $repoObject->id_object;
                    $versionModel->version_name = 'V1';
                    $versionModel->version_description = '';
                    $userModel = Yii::app()->user->loadUserModel();
                    /**
                     * @var $userModel CoreUser
                     */
                    $authorData = array(
                        'idUser' => $userModel->idst,
                        'username' => $userModel->getUserNameFormatted()
                    );
                    $versionModel->author = CJSON::encode($authorData);
                    $versionModel->create_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');

                    if(!$versionModel->save()){
                    	Yii::log(var_export($versionModel->getErrors(),true), CLogger::LEVEL_ERROR);
                    }

                } else{
                	Yii::log(var_export($repoObject->getErrors(),true), CLogger::LEVEL_ERROR);
                }
            }

            // DB Commit
            $transaction->commit();


        } catch (CException $e) {
        	Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
            $transaction->rollback();
        }

        $this->redirect(Docebo::createAbsoluteLmsUrl('centralrepo/centralRepo/index'));
    }

    public function actionAxListAllowedIframeUrls() {
        $model = CoreSettingWhitelist::model()->findByAttributes(array('type' => 'lo_iframe_url'));
        $searchInput = Yii::app()->request->getParam("search_input", null);
        $search = (isset($searchInput) && !empty($searchInput));

        $items = array();
        $allSources = false;
        if($model) {
            // Get the list with whitelisted URLs
            $allSources = CJSON::decode($model->whitelist);
        }

        $i = 0;
        if(!empty($allSources)){
            foreach($allSources as $item){
                if ($search){
                    if (stripos($item, $searchInput) !== false) {
                        $items[] = array('id' => $i, 'url' => $item);
                        $i++;
                    }
                } else {
                    $items[] = array('id' => $i, 'url' => $item);
                    $i++;
                }
            }
        }

        $pageSize = Settings::get('elements_per_page', 10);
        if(!empty($customConfig['elements_per_page'])){
            $pageSize = $customConfig['elements_per_page'];
        }

        // Used to populate the grid
        $dataProvider = new CArrayDataProvider($items, array(
            'pagination' => array('pageSize' => $pageSize),
            'totalItemCount' => count($items),
        ));

        $listViewParams = array(
            'disableMassSelection' => true,
            'disableMassActions' => true,
            'gridId' => "allowedUrlsComboListView",
            'dataProvider' => $dataProvider,
            'gridTemplate'	=> '{items}{summary}{pager}',
            'columns' => array(
                array(
                    'name' => 'url',
                    'value' => 'strip_tags($data[url])',
                    'header' => Yii::t('admin', 'ALLOWED URLs'),
                    'htmlOptions'=>array('style'=>'max-width: 300px; word-wrap: break-word; vertical-align:top;border-bottom: 1px solid #ddd;'),
                    'headerHtmlOptions' => array(),
                ))
        );

        $this->renderPartial('lms.protected.modules.htmlpage.views.default._whitelisted_urls', array(
            'listViewParams' => $listViewParams
        ), false, true);

        Yii::app()->end();
    }
}