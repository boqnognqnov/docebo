<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 13.4.2016 г.
 * Time: 15:16
 */
class FileLoController extends CentralLoController{
    public function actionEdit(){
        $idObject = Yii::app()->request->getParam('id_object', false);

        $this->editUploadLo(LearningOrganization::OBJECT_TYPE_FILE, $idObject);
    }

    public function actionSave(){
        $idObject = Yii::app()->request->getParam("id_object");

        $file = Yii::app()->request->getParam("file", null);
        $title = urldecode(trim(Yii::app()->request->getParam("title", null)));
        $description = urldecode(Yii::app()->request->getParam("description", null));
        $short_description = urldecode(Yii::app()->request->getParam("short_description", ''));
        $thumb = urldecode(Yii::app()->request->getParam("thumb", null));

        $success = false;
        $message = Yii::t('standard', '_OPERATION_FAILURE');

        $response = new AjaxResult();
        $transaction = Yii::app()->db->beginTransaction();

        try {

            if($idObject){
                $success = $this->editFile($idObject, $file, $title, $description, $short_description, $thumb);
            } else{

                if (is_array($file) && isset($file['name'])) {
                    $originalFileName = urldecode($file['name']);
                    if (isset($file['target_name'])) {
                        $file['name'] = $file['target_name'];
                    }
                    $file['name'] = urldecode($file['name']);
                }

                // Validate input parameters
                $title = trim($title);
                if (empty($title) || count($title) <= 0) { throw new CException(Yii::t('error', 'Enter a title!')); }

                // Validate file input
                if (!is_array($file)) { throw new CException("Invalid file object."); }
                if (!isset($file['name']) || ($file['name'] == '')) { throw new CException("Invalid file name."); }
                if (!isset($file['percent']) || $file['percent'] < 100) { throw new CException("Incomplete file upload."); }

                // Check if uploaded file exists
                $uploadTmpFile = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR.$file['name'];
                if (!is_file($uploadTmpFile)) { throw new CException("File '" . $file['name'] . "' does not exist."); }

                /*********************************************************/

                /*** File extension type validate ************************/
                $fileValidator = new FileTypeValidator(FileTypeValidator::TYPE_ITEM);
                if (!$fileValidator->validateLocalFile($uploadTmpFile))
                {
                    FileHelper::removeFile($uploadTmpFile);
                    throw new CException($fileValidator->errorMsg);
                }
                /*********************************************************/

                // Rename uploaded file
                $extension = strtolower(CFileHelper::getExtension($file['name']));
                $newFileName = Docebo::randomHash() . "." . $extension;
                $newTmpFile = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR.$newFileName;

                if (!@rename($uploadTmpFile, $newTmpFile)) { throw new CException("Error renaming uploaded file."); }

                // Move uploaded file to final storage for later usage
                $storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_FILE);
                if ( ! $storageManager->store($newTmpFile)){ throw new CException("Error while saving file to final storage."); }

                // Delete tmp file
                FileHelper::removeFile($newTmpFile);

                $loObject = new LearningRepositoryObject();
                $loObject->object_type = LearningOrganization::OBJECT_TYPE_FILE;
                $categoryNodeId = LearningCourseCategoryTree::getRootNodeId();
                if(isset(Yii::app()->session['currentCentralRepoNodeId'])){
                    $categoryNodeId = Yii::app()->session['currentCentralRepoNodeId'];
                }
                $loObject->id_category = $categoryNodeId;
                $loObject->title = $title;
                $loObject->description = $description;
                $loObject->short_description = $short_description;
                $loObject->params_json = CJSON::encode($this->prepareLoOptions());
	            if($thumb){
		            $loObject->resource = $thumb;
	            }elseif($loObject->resource){
		            $loObject->resource = 0;
	            }

                if($loObject->save()){
                    $res = $this->createNewVersion($loObject->id_object, $title, $description, $newFileName, $originalFileName);

                    if($res !== false){
                        $success = true;
                    }
                }
            }

            // DB Commit
            $transaction->commit();

            if($success){
                $message = Yii::t('standard', '_OPERATION_SUCCESSFUL');
            }

            $response->setMessage($message)->setStatus($success);

        } catch (CException $e) {
            $transaction->rollback();
            $response->setMessage($e->getMessage())->setStatus($success);
        }

        $response->toJSON();
        Yii::app()->end();
    }

    /**
     * @param $idObject
     * @param $title
     * @param $description
     * @param $newFileName
     * @param $originalFileName
     * @param string $versionName
     * @param string $versionDescription
     * @param int $currentVersion
     * @return bool|LearningMaterialsLesson
     */
    protected function createNewVersion($idObject, $title, $description, $newFileName, $originalFileName, $versionName = 'V1', $versionDescription = '', $currentVersion = 0){

        $currentUserId = Yii::app()->user->id;

        $fileObj = new LearningMaterialsLesson();
        $fileObj->author = $currentUserId;
        $fileObj->title = $title;
        $fileObj->description = $description;
        $fileObj->path = $newFileName;
        $fileObj->original_filename = $originalFileName;
        if ( $fileObj->save()) {
            $newVersion = new LearningRepositoryObjectVersion();
            $newVersion->id_resource = $fileObj->idLesson;
            $newVersion->object_type = LearningOrganization::OBJECT_TYPE_FILE;
            $newVersion->id_object = $idObject;
            $newVersion->version_name = $versionName;
            $newVersion->version_description = $versionDescription;
            $newVersion->version = $currentVersion + 1;
            $userModel = Yii::app()->user->loadUserModel();
            /**
             * @var $userModel CoreUser
             */
            $authorData = array(
                'idUser' => $userModel->idst,
                'username' => $userModel->getUserNameFormatted()
            );
            $newVersion->author = CJSON::encode($authorData);
            $newVersion->create_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');

            if($newVersion->save()){
                return $fileObj;
            } else{
                $message = '';
                foreach($newVersion->getErrors() as $error){
                    $message .= $error[0]. "<br />";
                }
                throw new CException($message);
            }
        } else{
            $message = '';
            foreach($fileObj->getErrors() as $error){
                $message .= $error[0]. "<br />";
            }
            throw new CException($message);
        }

        return false;
    }

    protected function editFile($idObject, $file, $title, $description, $short_description, $thumb){
        $loModel = LearningRepositoryObject::model()->findByPk($idObject);

        $versionData = $loModel->getLatestVersion();

        if(!empty($versionData)){
            $versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
                'id_resource' => $versionData,
                'object_type' => LearningOrganization::OBJECT_TYPE_FILE,
                'version' => $versionData['version']
            ));

            if($versionModel){
                $fileObj = LearningMaterialsLesson::model()->findByPk($versionModel->id_resource);

                if($fileObj){
                    if ($file !== 'false') {
                        if (is_array($file) && isset($file['name'])) {
                            // Use the physically uploaded filename, put by PLUpload in the "target_name" element
                            // This is tru when "unique_names" option is used for plupload JavaScript object
                            $originalFileName = urldecode($file['name']);
                            if (isset($file['target_name'])) {
                                $file['name'] = $file['target_name'];
                            }
                            $file['name'] = urldecode($file['name']);
                        }

                        // Validate file input
                        if (!is_array($file)) { throw new CException("Invalid file object."); }
                        if (!isset($file['name']) || ($file['name'] == '')) { throw new CException("Invalid file name."); }
                        if (!isset($file['percent']) || $file['percent'] < 100) { throw new CException("Incomplete file upload."); }

                        // Check if uploaded file exists
                        $uploadTmpFile = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR.$file['name'];
                        if (!is_file($uploadTmpFile)) { throw new CException("File '" . $file['name'] . "' does not exist."); }

                        /*********************************************************/

                        /*** File extension type validate ************************/
                        $fileValidator = new FileTypeValidator(FileTypeValidator::TYPE_ITEM);
                        if (!$fileValidator->validateLocalFile($uploadTmpFile))
                        {
                            FileHelper::removeFile($uploadTmpFile);
                            throw new CException($fileValidator->errorMsg);
                        }
                        /*********************************************************/

                        $oldFileName = $fileObj->path;

                        // Rename uploaded file
                        $extension = strtolower(CFileHelper::getExtension($file['name']));
                        $newFileName = Docebo::randomHash() . "." . $extension;
                        $newTmpFile = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR.$newFileName;

                        if (!@rename($uploadTmpFile, $newTmpFile)) { throw new CException("Error renaming uploaded file."); }

                        // Move uploaded file to final storage for later usage
                        $storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_FILE);
                        if ( ! $storageManager->store($newTmpFile)){ throw new CException("Error while saving file to final storage."); }

                        $versionControl = Yii::app()->request->getParam('version-control', 0);

                        if($versionControl == 1){
                            $versionName = Yii::app()->request->getParam('version_name', 'V1');
                            $versionDescription = Yii::app()->request->getParam('version_description', '');

                            $fileObj = $this->createNewVersion($loModel->id_object, $title, $description, $newFileName, $originalFileName, $versionName, $versionDescription, $versionModel->version);
                        } else{

                            // Now remove the old file to keep the collection clean
                            $occurencies = LearningMaterialsLesson::model()->count('path = :path', array(
                                ':path' => $oldFileName
                            ));
                            if ($occurencies <= 1) {
                                $storageManager->remove($oldFileName);
                            }

                            // update object with new file
                            $fileObj->path = $newFileName;
                            $fileObj->original_filename = $originalFileName;
                        }

                        // Delete tmp file
                        FileHelper::removeFile($newTmpFile);
                    }

                    // update info in database
                    $fileObj->title = $title;
                    $fileObj->description = $description;
                    if ( ! $fileObj->save()) {
                        throw new CException('Invalid file');
                    }

                    // update learning organization
                    $loModel->title = $fileObj->title;
                    $loModel->short_description = $short_description;
	                if($thumb){
		                $loModel->resource = $thumb;
	                }elseif($loModel->resource){
		                $loModel->resource = 0;
	                }

                    if($loModel->save()){
                        $versionModel->update_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');

                        if($versionModel->save()){
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * A work towards saving LO specific options/settings based on the current REQUEST to axSaveLo() action
     *
     * @param object|bool $loModel
     */
    private function prepareLoOptions() {

        $data = array();

        // These are pretty common and can be sent by all LO types being saved/uploaded
        $data['desktop_openmode']    = Yii::app()->request->getParam("desktop_openmode", LearningOrganization::OPENMODE_LIGHTBOX);
        $data['tablet_openmode']     = Yii::app()->request->getParam("tablet_openmode", LearningOrganization::OPENMODE_FULLSCREEN);
        $data['smartphone_openmode'] = Yii::app()->request->getParam("smartphone_openmode", LearningOrganization::OPENMODE_FULLSCREEN);

        return $data;
    }
}