<?php

class CentralRepoCategoryController extends Controller
{

    public function filters()
    {
        return array(
            'accessControl' => 'accessControl',
        );
    }


    public function accessRules()
    {
        $res = array();
        $res[] = array(
            'allow',
            'roles' => array('/framework/level/godadmin'),
            'actions' => array( 'getCategoryTree', 'moveLoInCategory'),
        );
        $res[] = array(
            'allow',
            'roles' => array('/framework/level/godadmin' , '/framework/level/admin'),
            'actions' => array( 'getPushLoCategoryTree'),
        );
        $res[] = array(
            'deny',
            // deny all
            'users' => array('*'),
            'deniedCallback' => array($this, 'accessDeniedCallback'),
        );
        return $res;
    }

	public function beforeAction($action)
	{
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/admin.css');

		return parent::beforeAction($action);
	}


    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        echo 'index';
        exit;
    }

    public function actionGetCategoryTree(){

        $newTreeGenerationTime = CoreRunningOperation::getCurrentTimeMicroseconds();

        $fancyTreeData = TreeGenerator::getCategoriesTree();
        $this->renderPartial("_tree", array(
            'fancyTreeData' => $fancyTreeData,
            'treeGenerationTime' => $newTreeGenerationTime,
        ), false, true);
    }

    public function actionGetPushLoCategoryTree(){

        $newTreeGenerationTime = CoreRunningOperation::getCurrentTimeMicroseconds();

        $fancyTreeData = TreeGenerator::getCategoriesTree();
        $this->renderPartial("_push_lo_tree", array(
            'fancyTreeData' => $fancyTreeData,
            'treeGenerationTime' => $newTreeGenerationTime,
        ));
    }

    public function actionMoveLoInCategory() {
        // Get the params from POST
        $object = Yii::app()->request->getParam("object", false);
        $category = Yii::app()->request->getParam("category", false);

        // And if all the params are passed move the object in other category
        if($object && $category){
            Yii::app()->db->createCommand()->update(
                LearningRepositoryObject::model()->tableName(),
                array('id_category' => $category),
                'id_object = :object',
                array( ':object' => $object)
            );
        }
    }

}