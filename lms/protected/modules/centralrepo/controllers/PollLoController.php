<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 15.4.2016 г.
 * Time: 11:49
 */
class PollLoController extends CentralLoController{

    public function actionSave(){
        $rq = Yii::app()->getRequest();
        /**
         * @var $rq CHttpRequest
         */
        $idObject = $rq->getParam('id_object', false);
        $shortDescription = Yii::app()->htmlpurifier->purify($rq->getParam('short_description', ""));
        $thumb = $rq->getParam('thumb', 0);
        $title = $rq->getParam('title', false);
        $description = $rq->getParam('description', "");
        $shared_tracking = $rq->getParam('shared_tracking', false);

        $poll = new LearningPoll();
        $object = new LearningRepositoryObject();

        if(!$title){
            Yii::app()->user->setFlash('error', Yii::t('error', 'Enter a title!'));

            $this->redirect(Docebo::createAbsoluteLmsUrl('centralrepo/pollLo/edit', array(
                'id_object' => $idObject
            )));
        }

        if($idObject){
            $version = LearningRepositoryObjectVersion::model()->findByAttributes(array(
                'id_object' => $idObject
            ));

            if($version){
                $object = LearningRepositoryObject::model()->findByPk($idObject);

                $poll = LearningPoll::model()->findByPk($version->id_resource);
                $poll->detachBehavior('learningObjects');

                if ($rq->getParam('confirm', false) !== false) {
                    $poll->title = $title;
                    $poll->description = $description;

                    $object->short_description = $shortDescription;

                    if (!$poll->save()) {
                        echo '<a class="auto-close error"></a>';
                        return;
                    }else{
                        $object->title = $poll->title;

                        if($shortDescription)
                            $object->short_description = Yii::app()->htmlpurifier->purify($shortDescription);

                        $object->description = $poll->description;

                        if($thumb)
                            $object->resource = intval($thumb);

                        if($object->save()){
                            $this->redirect(Docebo::createAbsoluteLmsUrl('poll/default/edit', array(
                                'id_object' => $object->id_object,
                                'opt' => 'centralrepo'
                            )));
                        }


                        // Trigger event to let plugins save their own data from POST
                        Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
                            'loModel' => $object
                        )));
                    }
                }
            }
        }

        $object->title = $title;
        $object->description = $description;
        $object->short_description = $shortDescription;
        $object->resource = intval($thumb);
        $categoryNodeId = LearningCourseCategoryTree::getRootNodeId();
        if(isset(Yii::app()->session['currentCentralRepoNodeId'])){
            $categoryNodeId = Yii::app()->session['currentCentralRepoNodeId'];
        }
        $object->id_category = $categoryNodeId;
        $object->object_type = LearningOrganization::OBJECT_TYPE_POLL;
        if($shared_tracking !== false)
            $object->shared_tracking = $shared_tracking;

        if($object->save()){
            $poll->title = $object->title;
            $poll->description = $object->description;
            $poll->author = Yii::app()->user->id;

            $poll->detachBehavior('learningObjects');

            if($poll->save()){
                $version = new LearningRepositoryObjectVersion();
                $version->id_resource = $poll->id_poll;
                $version->object_type = LearningOrganization::OBJECT_TYPE_POLL;
                $version->version = 1;
                $version->id_object = $object->id_object;
                $version->version_name = 'V1';
                $version->version_description = '';
                $userModel = Yii::app()->user->loadUserModel();
                /**
                 * @var $userModel CoreUser
                 */
                $authorData = array(
                    'idUser' => $userModel->idst,
                    'username' => $userModel->getUserNameFormatted()
                );
                $version->author = CJSON::encode($authorData);
                $version->create_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');

                if($version->save()){
                    $this->redirect(Docebo::createAbsoluteLmsUrl('poll/default/edit', array(
                        'id_object' => $object->id_object,
                        'opt' => 'centralrepo'
                    )));
                }
            }
        }

        $this->render("lms.protected.modules.poll.views.default._edit_poll", array(
            'lo_type'=>LearningOrganization::OBJECT_TYPE_POLL,
            'poll'=>$poll,
            'loModel'=>$object,
            'description'=>$poll->description,
        ));
    }

    public function actionEdit(){
        $rq = Yii::app()->getRequest();
        /**
         * @var $rq CHttpRequest
         */
        $idObject = $rq->getParam('id_object', false);

        Yii::app()->tinymce->init();

        $cs = Yii::app()->getClientScript();
        $playerAssetsUrl = Yii::app()->findModule('player')->assetsUrl;
        $cs->registerCssFile ($playerAssetsUrl.'/css/base.css');
        $this->getModule()->registerResources();

        $poll = new LearningPoll();
        $object = new LearningRepositoryObject();
        if($idObject){
            $version = LearningRepositoryObjectVersion::model()->findByAttributes(array('id_object' => $idObject));
            if($version) {
                $object = LearningRepositoryObject::model()->findByPk($idObject);
                $poll = LearningPoll::model()->findByPk($version->id_resource);
            }
        }

        $this->render("lms.protected.modules.poll.views.default._edit_poll", array(
            'lo_type'=>LearningOrganization::OBJECT_TYPE_POLL,
            'poll'=>$poll,
            'loModel'=>$object,
            'description'=>$poll->description,
            'centralRepoContext' => true,
            'saveUrl' => Docebo::createAbsoluteLmsUrl('centralrepo/pollLo/save')
        ));
    }
}