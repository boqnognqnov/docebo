<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 11.4.2016 г.
 * Time: 17:20
 */
class TincanLoController extends CentralLoController {

    public function actionEdit(){
        $idObject = Yii::app()->request->getParam('id_object', false);

        $this->editUploadLo(LearningOrganization::OBJECT_TYPE_TINCAN, $idObject);
    }

    protected function updateTincanPackage($idObject, $file, $thumb, $tinCanTitle) {

        $response = new AjaxResult();

        try {

            // Use the physically uploaded filename, put by PLUpload in the "target_name" element
            // This is true when "unique_names" option is used for plupload JavaScript object
            if (is_array($file) && isset($file['name'])) {
                $originalFileName = urldecode($file['name']);
                if (isset($file['target_name'])) {
                    $file['name'] = $file['target_name'];
                }
            }

            // Validate file input
            if (!is_array($file)) { throw new CException("Invalid file."); }
            if (!isset($file['name']) || ($file['name'] == '')) { throw new CException("Invalid file name."); }
            if (!isset($file['percent']) || $file['percent'] < 100) { throw new CException("Incomplete file upload."); }
            if ('zip' != strtolower(CFileHelper::getExtension($file['name']))) { throw new CException("Invalid file format."); }

            $uploadTmpFile = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR . $file['name'];
            if (!is_file($uploadTmpFile)) { throw new CException("File '" . $file['name'] . "' does not exist."); }

            
            $loModel = LearningRepositoryObject::model()->findByPk($idObject);
            $loModel->params_json = CJSON::encode($this->prepareLoOptions());
            
            if (!$loModel) {
                throw new CException("Invalid learning object");
            }

            $version = $loModel->getLatestVersion();

            $activity = LearningTcActivity::model()->findByAttributes(array(
                'id_tc_activity' => $version['id_resource']
            ));

            $package = $activity->ap;

            /**
             * @var $package LearningTcAp
             */
            if (!$package) { throw new CException("Old TinCan package not found"); }

            $package->extractTincanPackage($uploadTmpFile, $package->path);

            $package->save();

            $versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
                'id_resource' => $version['id_resource'],
                'object_type' => LearningOrganization::OBJECT_TYPE_TINCAN,
                'version' => $version['version']
            ));

            $versionModel->id_resource = $package->mainActivity->id_tc_activity;
            $versionModel->update_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
            if($versionModel->save()) {
                $loModel->short_description = Yii::app()->htmlpurifier->purify(Yii::app()->request->getParam("short_description", ''));
                if(!$tinCanTitle){
                    $loModel->title = $package->getTitle();
                }else{
                    $loModel->title = $tinCanTitle;
                }
                $loModel->resource = $thumb;
                return $loModel->save();
            }

        } catch (CException $e) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
        }

        return false;
    }

    public function actionSave(){
        
        
        $useXapiUrl         = (bool) Yii::app()->request->getParam("use_xapi_content_url", false);
        $file               = Yii::app()->request->getParam("file", null);
        
        $success            = false;
        $message = Yii::t('standard', '_OPERATION_FAILURE');

        // URLDecode
        $title = urldecode(trim(Yii::app()->request->getParam("title", null)));
        $shortDescription = urldecode(Yii::app()->request->getParam("short_description", ''));
        $thumb = Yii::app()->request->getParam("thumb", null);
        $tinCanTitle =  trim(strip_tags(Yii::app()->request->getParam("new-tincan-title", null)));

        if (is_array($file) && isset($file['name'])) {
            // Use the physically uploaded filename, put by PLUpload in the "target_name" element
            // This is true when "unique_names" option is used for plupload JavaScript object
            $originalFileName = urldecode($file['name']);
            if (isset($file['target_name'])) {
                $file['name'] = $file['target_name'];
            }
            $file['name'] = urldecode($file['name']);
        }


        // In case we are doing SCORM Package update ... we will get idOrg != false ... (from edit form)
        $idObject = (int) Yii::app()->request->getParam("id_object", false);
        if ($idObject > 0) {
            
            if ($useXapiUrl) {
                $file = LearningTcAp::createTemporaryTincanZip(
                    (int) Yii::app()->request->getParam("xapi_content_url", 0),
                    Yii::app()->request->getParam("xapi_content_url_title", ""),
                    Yii::app()->request->getParam("xapi_content_url_description", "")
                    );
            }
            
            if (($file !== 'false') && ($file !== null) && ($file !== false)) {

                $versionControl = (int) Yii::app()->request->getParam('version-control', 0);

                if($versionControl === 0){
                    // IF we have file uploaded, update tin can package
                    $this->updateTincanPackage($idObject, $file, $thumb, $tinCanTitle);
                    $success = true;
                    $message = Yii::t('standard', '_OPERATION_SUCCESSFUL');
                } else{
                    $repoObject = LearningRepositoryObject::model()->findByPk($idObject);

                    if($repoObject){
                        $versionData = $repoObject->getLatestVersion();

                        $versionName = Yii::app()->request->getParam('version_name', 'V1');
                        $versionDescription = Yii::app()->request->getParam('version_description', '');

                        if(!empty($versionData)){
                            $this->createNewVersion($file, $title, $shortDescription, $repoObject->id_object, $versionName, $versionDescription, $versionData['version'], $tinCanTitle);
                            $success = true;
                            $message = Yii::t('standard', '_OPERATION_SUCCESSFUL');
                        }
                    }
                }
            } else {
                $lo = LearningRepositoryObject::model()->findByPk($idObject);
                if ($lo)  {
                    $lo->title = $tinCanTitle;
                    $lo->short_description = Yii::app()->htmlpurifier->purify($shortDescription);
                    if($thumb){
	                    $lo->resource = intval($thumb);
                    }elseif($lo->resource){
	                    $lo->resource = 0;
                    }


                    if($lo->save()){
                        $success = true;
                        $message = Yii::t('standard', '_OPERATION_SUCCESSFUL');
                    }
                }
            }

            $response = new AjaxResult();
            $response->setMessage($message)->setStatus($success);
            $response->toJSON();
            Yii::app()->end();
        }

        $response = new AjaxResult();
        $transaction = Yii::app()->db->beginTransaction();

        try {
            
            // If TinCan is from a URL, lets generate the temporary zip file (instead of using an uploaded one)
            if ($useXapiUrl) {
                $file = LearningTcAp::createTemporaryTincanZip(
                    (int) Yii::app()->request->getParam("xapi_content_url", 0),
                    Yii::app()->request->getParam("xapi_content_url_title", ""),
                    Yii::app()->request->getParam("xapi_content_url_description", "")
                );
            }
            
            // Validate file input
            if (!is_array($file)) { throw new CException("Invalid file."); }
            if (!isset($file['name']) || ($file['name'] == '')) { throw new CException("Invalid file name."); }
            if (!isset($file['percent']) || $file['percent'] < 100) { throw new CException("Incomplete file upload."); }

            $fullFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $file['name'];
            /*** VIRUS SCAN ******************************************/
            if (isset(Yii::app()->params['virusscan_enable']) && (Yii::app()->params['virusscan_enable'] == true)) {
                $vs = new VirusScanner();
                $clean = $vs->scanFile($fullFilePath);
                if (!$clean) {
                    FileHelper::removeFile($fullFilePath);
                    throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
                }
            }
            /*********************************************************/

            /*** File extension type validate ************************/
            $fileValidator = new FileTypeValidator(FileTypeValidator::TYPE_ARCHIVE);
            if (!$fileValidator->validateLocalFile($fullFilePath))
            {
                FileHelper::removeFile($fullFilePath);
                throw new CException($fileValidator->errorMsg);
            }
            /*********************************************************/


            $repoObject = new LearningRepositoryObject();
            $repoObject->object_type = LearningOrganization::OBJECT_TYPE_TINCAN;
            $categoryNodeId = LearningCourseCategoryTree::getRootNodeId();
            if(isset(Yii::app()->session['currentCentralRepoNodeId'])){
                $categoryNodeId = Yii::app()->session['currentCentralRepoNodeId'];
            }
            $repoObject->id_category = $categoryNodeId;
            $repoObject->title = $title;
            $repoObject->short_description = $shortDescription;
            $repoObject->params_json = CJSON::encode($this->prepareLoOptions());
            $repoObject->resource = $thumb;

            if($repoObject->save()){
                $res = $this->createNewVersion($file, $title, $shortDescription, $repoObject->id_object, 'V1', '', 0, $tinCanTitle);

                if($res){
                    $success = true;
                    $message = Yii::t('standard', '_OPERATION_SUCCESSFUL');
                }
            }

            if($transaction->getActive()) {
                $transaction->commit();
            }

            $response = new AjaxResult();
            $response->setMessage($message)->setStatus($success);

        } catch (CException $e) {
            if($transaction->getActive()) {
                $transaction->rollback();
            }

            $response->setMessage($e->getMessage())->setStatus(false);
        }

        echo $response->toJSON();
        Yii::app()->end();
    }

    protected function createNewVersion($file, $title, $short_description, $idObject, $versionName = 'V1', $versionDescription = '', $currnetVersion = 0, $tinCanTitle){
        $object = new LearningTcAp();
        $object->setScenario('centralRepo');
        $object->registration = LearningTcAp::generateStatementUUID();
        $object->location = ''; //TO DO: get cloudfront information from storage class

        $object->setFile($file['name']);

        $rs = $object->save();
        if (!$rs) {
            //TO DO: revert file storing
            Yii::log('Error while saving DB information; error(s): '.print_r($object->getErrors(), true), CLogger::LEVEL_ERROR);
            throw new CException('Error while saving DB information; error(s): '.print_r($object->getErrors(), true));
        }

        $version = new LearningRepositoryObjectVersion();
        $version->id_resource = $object->mainActivity->getPrimaryKey();
        $version->object_type = LearningOrganization::OBJECT_TYPE_TINCAN;
        $version->version = $currnetVersion + 1;
        $version->id_object = $idObject;
        $version->version_name = $versionName;
        $version->version_description = $versionDescription;
        $userModel = Yii::app()->user->loadUserModel();
        /**
         * @var $userModel CoreUser
         */
        $authorData = array(
            'idUser' => $userModel->idst,
            'username' => $userModel->getUserNameFormatted()
        );
        $version->author = CJSON::encode($authorData);
        $version->create_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
        
        if($version->save()){
            // Create OAuth2 Client Application
            $repoObject = LearningRepositoryObject::model()->findByPk($idObject);

            if($repoObject){
                if($tinCanTitle){
                    $repoObject->title = $tinCanTitle;
                }else{
                    $repoObject->title = $object->mainActivity->name;
                }

                if($repoObject->save()){
                    return $this->createOAuth2Client($object->registration, $title, $short_description);
                }
            }
        }

        return false;
    }

    /**
     * A work towards saving LO specific options/settings based on the current REQUEST to axSaveLo() action
     *
     * @param object|bool $loModel
     */
    private function prepareLoOptions() {

        $data = array();

        // These are pretty common and can be sent by all LO types being saved/uploaded
        $data['desktop_openmode']       = Yii::app()->request->getParam("desktop_openmode", LearningOrganization::OPENMODE_LIGHTBOX);
        $data['tablet_openmode']        = Yii::app()->request->getParam("tablet_openmode", LearningOrganization::OPENMODE_FULLSCREEN);
        $data['smartphone_openmode']    = Yii::app()->request->getParam("smartphone_openmode", LearningOrganization::OPENMODE_FULLSCREEN);
        $data['enable_oauth'] 		    = (bool) Yii::app()->request->getParam("enable_oauth", false);
        $data['oauth_client']    	    = Yii::app()->request->getParam("oauth_client", null);
        $data['use_xapi_content_url']   = (bool) Yii::app()->request->getParam("use_xapi_content_url", false);
        
        return $data;
    }

    private function createOAuth2Client($id, $name, $description) {

        $clientModel = new OauthClients();
        $clientModel->client_id = $id;
        $clientModel->client_secret = Docebo::randomHash();
        $clientModel->redirect_uri = "";
        $clientModel->app_name = $name;
        $clientModel->app_description = $description ? $description : $name;
        $clientModel->hidden = 1;

        // False is important, because redirect_uri is REQUIRED by the model, but we have to set it empty
        if (!$clientModel->save(false)) {
            Yii::log('Failed to save OAuthClient', CLogger::LEVEL_ERROR);
            Yii::log('Errors:', CLogger::LEVEL_ERROR);
            Yii::log($clientModel, CLogger::LEVEL_ERROR);

            return false;
        }

        return true;

    }

}