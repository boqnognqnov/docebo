<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 12.4.2016 г.
 * Time: 13:41
 */
class VideoLoController extends CentralLoController{
    public function actionEdit(){
        $idObject = Yii::app()->request->getParam('id_object', false);

        $this->editUploadLo(LearningOrganization::OBJECT_TYPE_VIDEO, $idObject);
    }

    public function actionSave(){
        $idObject = Yii::app()->request->getParam("id_object", null);
        $file = Yii::app()->request->getParam("file", null);
        $seekable = Yii::app()->request->getParam("seekable", 0);
        $fallbackSub = Yii::app()->request->getParam("fallbackSub", '');
        $subtitlesArray = Yii::app()->request->getParam("subtitles", array());
        $shortDescription = urldecode(Yii::app()->request->getParam("short_description", ''));
        $videoSource = Yii::app()->request->getParam('videoSource', false);
        $videoUrl = Yii::app()->request->getParam('videoUrl', false);

        $response = new AjaxResult();
        $success = false;
        $message = Yii::t('standard', '_OPERATION_FAILURE');

        if($videoSource != 1) {
            // Use the physically uploaded filename, put by PLUpload in the "target_name" element
            // This is tru when "unique_names" option is used for plupload JavaScript object
            if (is_array($file) && isset($file['name'])) {
                $originalFileName = urldecode($file['name']);
                if (isset($file['target_name'])) {
                    $file['name'] = $file['target_name'];
                }
            }
        }

        $thumb = Yii::app()->request->getParam("thumb", null);

        // URLDecode
        $title = urldecode(trim(Yii::app()->request->getParam("title", null)));
        $description = urldecode(Yii::app()->request->getParam("description", null));
        if (is_array($file) && isset($file['name'])) {
            $file['name'] = urldecode($file['name']);
        }

        $transaction = Yii::app()->db->beginTransaction();

        try {
            if (empty($title)) {
                throw new CException(Yii::t('error', 'Enter a title!'));
            }

            if ($file !== 'false') {
                // Validate file input
                if (!is_array($file)) {
                    throw new CException("Invalid file.");
                }
                if (!isset($file['name']) || ($file['name'] == '')) {
                    throw new CException("Invalid file name.");
                }
                if (!isset($file['percent']) || $file['percent'] < 100) {
                    throw new CException("Incomplete file upload.");
                }

                // Check if uploaded file exists
                $uploadTmpFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $file['name'];
                if (!is_file($uploadTmpFile)) {
                    throw new CException("File '" . $file['name'] . "' does not exist.");
                }

                // Create storage Object, use it to determine the VIDEO whitelist type to use
                $storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_VIDEO);
                $whitelistType = ($storage->type == CFileStorage::TYPE_S3) ? FileTypeValidator::TYPE_VIDEO_EXTENDED_LIST : FileTypeValidator::TYPE_VIDEO;

                /*** File extension type validate ************************/
                $fileValidator = new FileTypeValidator($whitelistType);
                if (!$fileValidator->validateLocalFile($uploadTmpFile)) {
                    FileHelper::removeFile($uploadTmpFile);
                    throw new CException($fileValidator->errorMsg);
                }
                /*********************************************************/

                // Rename uploaded file
                $extension = strtolower(CFileHelper::getExtension($file['name']));
                $newFileName = Docebo::randomHash() . "." . $extension;
                $newTmpFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $newFileName;
                if (!@rename($uploadTmpFile, $newTmpFile)) {
                    throw new CException("Error renaming uploaded file.");
                }

                // Move uploaded file to final storage for later usage
                // store() must return either FALSE or PATH/Key to the destination file
                $destFilePath = $storage->store($newTmpFile);
                if (!$destFilePath) {
                    throw new CException("Error while saving file to final storage.");
                }
                // store() MAY change format and extension of the file! Lets get them back
                $destExtension =  strtolower(CFileHelper::getExtension(pathinfo($destFilePath, PATHINFO_BASENAME)));
                $destFilename  =  pathinfo($destFilePath, PATHINFO_BASENAME);

                // DELETE tmp file
                FileHelper::removeFile($newTmpFile);
            }

            if($idObject){
                $resourceId = $this->editVideo($idObject, $title, $description, $shortDescription, $thumb, $videoSource, $videoUrl, $seekable, $destFilename, $destExtension, $storage);
                if($resourceId !== false){
                    $success = true;
                }
            } else{
                $repoObject = new LearningRepositoryObject();
                $repoObject->object_type = LearningOrganization::OBJECT_TYPE_VIDEO;
                $categoryNodeId = LearningCourseCategoryTree::getRootNodeId();
                if(isset(Yii::app()->session['currentCentralRepoNodeId'])){
                    $categoryNodeId = Yii::app()->session['currentCentralRepoNodeId'];
                }
                $repoObject->id_category = $categoryNodeId;
                $repoObject->title = $title;
                $repoObject->description = $description;
                $repoObject->short_description = $shortDescription;
                $repoObject->params_json = CJSON::encode($this->prepareLoOptions());
	            if($thumb){
		            $repoObject->resource = $thumb;
	            }elseif($repoObject->resource){
		            $repoObject->resource =0;
	            }


                if($repoObject->save()){
                    $resourceId = $this->createNewVersion($repoObject->id_object, $title, $description, $videoSource, $videoUrl, $seekable, $destFilename, $destExtension, $storage);
                    if($resourceId !== false){
                        $success = true;
                    }
                }
            }

            // Add subtitles here
            if (count($subtitlesArray) > 0) {
                foreach ($subtitlesArray as $subLangCode => $subtitle) {
                    // There are no subtitles per this language add new subtitle record and new core asset and save them
                    // 1. Create new Video Subtitle model
                    $videoSubModel = new LearningVideoSubtitles();

                    // 2. Set the Video Subtitle Details
                    $videoSubModel->date_added = Yii::app()->localtime->getUTCNow();
                    $videoSubModel->added_by = Yii::app()->user->idst;
                    $videoSubModel->lang_code = $subLangCode;
                    $videoSubModel->fallback = 0;
                    $videoSubModel->id_video = $resourceId;

                    // 3. Create new CoreAsset
                    $subAsset = new CoreAsset();

                    // 4. Set the temporary sub file details
                    $subAsset->original_filename = $subtitle['original_filename'];
                    $subAsset->type = CoreAsset::TYPE_SUBTITLES . "/" . $resourceId;
                    // Set the actual temp filepath
                    $subAsset->sourceFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $subtitle['target_filename'];

                    // 5. Save the asset
                    if ($subAsset->save()) {
                        // 6. Update the video subtitle db record via the corresponding model
                        $videoSubModel->asset_id = $subAsset->id;

                        // 7. Save the video subtitle model
                        $videoSubModel->save();
                    }

                    // 8. Clean the temporary subtitle file
                    FileHelper::removeFile(Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $subtitle['target_filename']);
                }
            }

            // If there is fallback subtitle passed for this video, update all the video subtitles records according to it !!!
            if ($fallbackSub) {
                // Update
                if (preg_match('#[a-z]#i', $fallbackSub)){
                    LearningVideoSubtitles::model()->updateAll(array('fallback' => 0), 'id_video = ' . $resourceId . ' AND lang_code != "' . $fallbackSub . '"');
                    LearningVideoSubtitles::model()->updateAll(array('fallback' => 1), 'id_video = ' . $resourceId . ' AND lang_code = "' . $fallbackSub . '"');
                } else {
                    LearningVideoSubtitles::model()->updateAll(array('fallback' => 0), 'id_video = ' . $resourceId . ' AND id != ' . $fallbackSub);
                    LearningVideoSubtitles::model()->updateAll(array('fallback' => 1), 'id_video = ' . $resourceId . ' AND id = ' . $fallbackSub);
                }
            }

            // DB Commit
            $transaction->commit();

        } catch (CException $e) {
            $uploadTmpFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $file['name'];

            $msg = 'Video upload error:<br />
<br />
Platform: '.Yii::app()->createAbsoluteUrl('site/index').'
Uploaded file: '.$uploadTmpFile.'
Error code: '.$e->getCode().'<br />
Error message: '.$e->getMessage().'<br />
Trace: '.$e->getTrace().'<br />
POST: '.nl2br(var_export($_POST, true)).'<br />
GET: '.nl2br(var_export($_GET, true)).'';

            Yii::log($msg, 'mail');

            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
        }

        if($success){
            $message = Yii::t('standard', '_OPERATION_SUCCESSFUL');
        }
        $response->setMessage($message)->setStatus($success);
        $response->toJSON();
        Yii::app()->end();
    }

    protected function editVideo($idObject, $title, $description, $shortDescription, $thumb, $videoSource, $videoUrl, $seekable, $destFilename = null, $destExtension = null, $storage){
        try {
            $file = Yii::app()->request->getParam("file", false);
            $repoObject = LearningRepositoryObject::model()->findByPk($idObject);

            if (($repoObject instanceof LearningRepositoryObject) === false) {
                return false;
            }

            $versionData = $repoObject->getLatestVersion();

            if (empty($versionData)){
                return false;
            }

            $versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
                'id_resource' => $versionData['id_resource'],
                'object_type' => LearningOrganization::OBJECT_TYPE_VIDEO,
                'version' => $versionData['version']
            ));

            if(($versionModel instanceof LearningRepositoryObjectVersion) === false){
                return false;
            }

            $versionControl = Yii::app()->request->getParam('version-control', 0);

            if($versionControl == 1){
                $newVersionName = Yii::app()->request->getParam('version_name', 'V1');
                $newVersionDescription = Yii::app()->request->getParam('version_description', '');

                return $this->createNewVersion($repoObject->id_object, $title, $description, $videoSource, $videoUrl, $seekable, $destFilename, $destExtension, $storage, $newVersionName, $newVersionDescription, $versionModel->version);
            }

            $videoObj = LearningVideo::model()->findByPk($versionModel->id_resource);

            if (!$videoObj) {
                throw new CException('Invalid file');
            }
            if (!$videoObj->isVideoFileReferencedElsewhere() && !$file) {

                /** @var  $old - the video_formats from the Old Object * */
                $old = CJSON::decode($videoObj->video_formats);

                $storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_VIDEO);

                // Check if old MP4 and HLS are NOT empty, then delete OLD mp4 fil and the HLS folder
                if (isset($old['mp4']) && !empty($old['mp4'])) {
                    Yii::log('Removing MP4 file:' . $old['mp4'], CLogger::LEVEL_INFO);
                    $storage->remove($old['mp4']);      // Remove the mp4 file outside the folder
                }

                if (isset($old['hls_id']) && !empty($old['hls_id'])) {
                    $folderToDelete = pathinfo($old['hls_id'], PATHINFO_FILENAME);
                    Yii::log('Removing Folder:' . $folderToDelete, CLogger::LEVEL_INFO);
                    $storage->removeFolder($folderToDelete); // Remove the folder from Amazon if there is existing one
                }

            }


            $videoTracks = LearningVideoTrack::model()->findAllByAttributes(array(
                'idResource' => (int)$videoObj->id_video
            ));
            if (is_array($videoTracks)) {
                foreach ($videoTracks as $trackModel) {
                    $trackModel->bookmark = (int)0;
                    $trackModel->save();
                }
            }

            if($destExtension && $destFilename){
                $hlsIdValue = pathinfo($destFilename, PATHINFO_FILENAME);
                $hls_id = VideoConverter::HLS_ID;
                $videoFormats = new stdClass();
                $videoFormats->$destExtension = $destFilename;
                $videoFormats->$hls_id = $hlsIdValue;
            }

            $videoType = LearningVideo::VIDEO_TYPE_UPLOAD;

            if($videoSource == 1){
                $videoId = '';

                if(($id = App7020WebsiteProceeder::isVimeoLink($videoUrl)) !== false){
                    $videoId = $id;
                    $videoType = LearningVideo::VIDEO_TYPE_VIMEO;
                } else if(($id = App7020WebsiteProceeder::isYoutubeLink($videoUrl)) !== false){
                    $videoId = $id;
                    $videoType = LearningVideo::VIDEO_TYPE_YOUTUBE;
                } else if(($id = App7020WebsiteProceeder::isWistiaLink($videoUrl)) !== false){
                    $videoId = $id;
                    $videoType = LearningVideo::VIDEO_TYPE_WISTIA;
                } else {
                    throw new CException("Video URL is not valid");
                }

                $videoFormats = array(
                    'id' => $videoId,
                    'url' => urlencode($videoUrl)
                );
            }

            // Save the video information in learning_video table
            $videoObj->title = $title;
            $videoObj->description = $description;
            $videoObj->seekable = $seekable;
            $videoObj->type = $videoType;

            if (isset($videoFormats)) {
                $videoObj->video_formats = CJSON::encode($videoFormats);
            }

            if (!$videoObj->save()) {
                throw new CException("Error while creating or saving Video object.");
            }

            // update learning organization
            $repoObject->title = $videoObj->title;

            // If the storage implies a video transcoding, we must set the visibility to (-1)
            // The transcoding process will change it to -2 (error) or 1 (success)
            if ($storage && $storage->transcodeVideo) {
                $versionModel->visible = -1;
            }

            if (intval($thumb) > 0) {
                $repoObject->resource = intval($thumb);
            }elseif(!$thumb && $repoObject->resource){
	            $repoObject->resource = 0;
            }


            $repoObject->short_description = $shortDescription;

            if($repoObject->save()){
                $versionModel->update_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');

                if($versionModel->save()){
                    return $videoObj->id_video;
                }
            }
        } catch (Exception $e){
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
        }

        return false;
    }

    protected function createNewVersion($idObject, $title, $description, $videoSource, $videoUrl, $seekable, $destFilename, $destExtension, $storage, $versionName = 'V1', $versionDescription = '', $currentVersion = 0){
        $videoType = LearningVideo::VIDEO_TYPE_UPLOAD;

        if($videoSource !=1 ){
            // Save the video information in learning_video table
            $hls_id = VideoConverter::HLS_ID;
            $hlsIdValue = preg_replace('/\\.[^.\\s]{3,4}$/', '', $destFilename);
            $videoFormats = new stdClass();
            $videoFormats->$destExtension = $destFilename;
            $videoFormats->$hls_id = $hlsIdValue;
        } else{
            $videoId = '';

            if(($id = App7020WebsiteProceeder::isVimeoLink($videoUrl)) !== false){
                $videoId = $id;
                $videoType = LearningVideo::VIDEO_TYPE_VIMEO;
            } else if(($id = App7020WebsiteProceeder::isYoutubeLink($videoUrl)) !== false){
                $videoId = $id;
                $videoType = LearningVideo::VIDEO_TYPE_YOUTUBE;
            } else if(($id = App7020WebsiteProceeder::isWistiaLink($videoUrl)) !== false){
                $videoId = $id;
                $videoType = LearningVideo::VIDEO_TYPE_WISTIA;
            } else {
                throw new CException("Video URL is not valid");
            }

            $videoFormats = array(
                'id' => $videoId,
                'url' => urlencode($videoUrl)
            );
        }

        $video = new LearningVideo();
        $video->title = $title;
        $video->description = $description;
        $video->video_formats = CJSON::encode($videoFormats);
        $video->seekable = $seekable;
        $video->type = $videoType;
        if (!$video->save()) {
            return false;
        }

        $newVersion = new LearningRepositoryObjectVersion();
        $userModel = Yii::app()->user->loadUserModel();
        /**
         * @var $userModel CoreUser
         */
        $authorData = array(
            'idUser' => $userModel->idst,
            'username' => $userModel->getUserNameFormatted()
        );
        $newVersion->author = CJSON::encode($authorData);
        $newVersion->create_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
        $newVersion->id_object = $idObject;
        $newVersion->id_resource = $video->id_video;
        $newVersion->object_type = LearningOrganization::OBJECT_TYPE_VIDEO;
        $newVersion->version_name = $versionName;
        $newVersion->version_description = $versionDescription;
        $newVersion->version = $currentVersion + 1;
        // If the storage implies a video transcoding, we must set the visibility to (-1)
        // The transcoding process will change it to -2 (error) or 1 (success)
        if ($storage && $storage->transcodeVideo) {
            $newVersion->visible = -1;
        }

        if($newVersion->save()){


            return $video->id_video;
        }

        return false;
    }

    /**
     * A work towards saving LO specific options/settings based on the current REQUEST to axSaveLo() action
     *
     * @param object|bool $loModel
     */
    private function prepareLoOptions() {

        $data = array();

        // These are pretty common and can be sent by all LO types being saved/uploaded
        $data['desktop_openmode']    = Yii::app()->request->getParam("desktop_openmode", LearningOrganization::OPENMODE_LIGHTBOX);
        $data['tablet_openmode']     = Yii::app()->request->getParam("tablet_openmode", LearningOrganization::OPENMODE_FULLSCREEN);
        $data['smartphone_openmode'] = Yii::app()->request->getParam("smartphone_openmode", LearningOrganization::OPENMODE_FULLSCREEN);

        return $data;
    }

    public function actionAxDeleteSubtitle() {
        $idSub  = Yii::app()->request->getParam('id', false);
        $confirm = Yii::app()->request->getParam('confirm_button', false);
        $agreed = Yii::app()->request->getParam('agree_deletion', false);
        $transaction = null;

        try {
            $subModel = false;
            // Check if there is passed id of a subtitle db table
            if (intval($idSub)) {
                // Load subtitle model
                $subModel = LearningVideoSubtitles::model()->findByPk($idSub);
                if (!$subModel)
                    throw new Exception('Invalid subtitle');
            }

            if($confirm && $agreed) {
                // If it is a temporary, not yet saved subtitle file to be deleted
                if (preg_match('#[a-z]#i', $idSub)) {
                    // Get the actual filepath
                    $tmpFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $idSub;

                    // Clean up
                    FileHelper::removeFile($tmpFilePath);

                    // close the dialog
                    echo '<a class="auto-close"></a>';
                    Yii::app()->end();
                }

                if (Yii::app()->db->getCurrentTransaction() === NULL)
                    $transaction = Yii::app()->db->beginTransaction();

                // Delete the asset first
                if ($subModel->asset_id) {
                    CoreAsset::model()->deleteByPk($subModel->asset_id);
                }

                // Delete the subtitle
                if(!$subModel->delete())
                    throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));

                if ($transaction)
                    $transaction->commit();

                echo '<a class="auto-close"></a>';
                Yii::app()->end();

            } else {
                $this->renderPartial('lms.protected.modules.player.views.training._delete_subtitle', array(
                    'model' => $subModel,
                ));
            }
        }
        catch (Exception $e) {
            if ($transaction)
                $transaction->rollback();

            $this->renderPartial('admin.protected.views.common/_dialog2_error', array(
                'message' 	=> $e->getMessage(),
                'type'		=> 'error',
            ));
            Yii::app()->end();
        }
    }

    public function actionAxSubtitleToggleFallback() {
        $id = Yii::app()->request->getParam('subId', false);
        $idVideo = Yii::app()->request->getParam('idVideo', false);
        $model = false;

        // Get the subtitle model if there is an id of subtitle passed
        if ($id) {
            $model = LearningVideoSubtitles::model()->findByPk($id);
        }

        // If there is an id of a video passed do fallback for all of its subtitles
        if ($idVideo) {
            LearningVideoSubtitles::model()->updateAll(array('fallback'=>0),'id_video=:id_video',array(':id_video'=>$idVideo));
        }

        if ($model) {
            $model->fallback = $model->fallback == 1 ? 0 : 1;
            $model->save();

            $result['success'] = true;
        } else {
            $result['success'] = false;
        }

        $this->sendJSON($result);
    }
}