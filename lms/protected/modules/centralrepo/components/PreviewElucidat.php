<?php

class PreviewElucidat extends Preview
{
    public static $view = 'elucidat';

    public function getContent() {
        $object = $this->getObject();
        if (!$object || $object->object_type != LearningOrganization::OBJECT_TYPE_ELUCIDAT) {
            throw new Exception('Invalid Object');
        }
        $learningElucidat = LearningElucidat::model()->findByAttributes(array('id_resource' => $object->id_resource));

        if (!$learningElucidat) {
            throw new Exception('Invalid Elucidat Learning Object');
        }

        // Launcher Activity (as per tincan.xml)
        $activity = LearningTcActivity::model()->findByPk($object->id_resource);
        if (!$activity) {
            throw new Exception('Invalid Activity: '.$object->id_resource);
        }

        // Activity Provider (not to mess with Activity (which are children of AP)
        $activityProvider = $activity->ap;  // using AR relation

        // Get Elucidat keys from the settings
        $publicKey = Settings::get('elucidat_consumer_key', null);
        $secretKey = Settings::get('elucidat_secret_key', null);

        $client = new ElucidatApiClient($publicKey, $secretKey);

        // Get the Elucidat launch url
        $res = $client->getLaunchUrl($learningElucidat->r_release_code  , Yii::app()->user->loadUserModel()->getFullName(), Yii::app()->user->loadUserModel()->email, $res->data['url'] . '&iduser='.Yii::app()->user->id.'&lmsreg='.$activityProvider->registration);

        if ($res->success == true) {
            // Build Learner specific temporary URL
            $launch = $res->data['url'];
        } else {
            $message = Yii::t('elucidat', 'Failed to launch Elucidat object. Maybe it is still not released. Please try again in few minutes');
            throw new Exception($message);
        }

        $data = array(
            'isFolder' => false,
            'type' => LearningOrganization::OBJECT_TYPE_ELUCIDAT,
            'idResource' => $object->id_object,
            'launch' => $launch,
        );

        $this->showContent( array( 'data' => $data ) );
    }

}