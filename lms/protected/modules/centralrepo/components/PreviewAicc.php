<?php

class PreviewAicc extends Preview
{
    public static $view = 'aicc';

    public function getContent() {

        $object = $this->getObject();
        if (!$object || $object->object_type != LearningOrganization::OBJECT_TYPE_AICC) {
            throw new Exception('Invalid Object');
        }
        $this->registerResources();
        $tree = new TreeGenerator($object);
        $objectsTree = $tree->generate();

        $this->showContent(array('data' => $objectsTree));
    }

    public function registerResources(){
        $assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('player.assets'));

        $cs = Yii::app()->getClientScript();
        // Core Yii
        $cs->registerCoreScript('jquery');
        $cs->registerCoreScript('jquery.ui');
        $cs->registerScriptFile($assetsUrl.'/js/jquery.dynatree.js');
        $cs->registerCssFile($assetsUrl.'/css/ui.dynatree-modified.css');
    }
}