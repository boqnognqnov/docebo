<?php

class CentralRepo
{
    public static function getAllObjectTypes(){
        $usedTypes = array();
        $usedTypesQuery = Yii::app()->db->createCommand()
            ->select('object_type')
            ->from(LearningRepositoryObject::model()->tableName())
            ->group('object_type')
            ->queryColumn();
        $objectTypesTitles = LearningOrganization::objectTypes();
        foreach($usedTypesQuery as $type){
            $usedTypes[$type] = $objectTypesTitles[$type];
        }

        return $usedTypes;
    }


    public static function createLoUrl($lo_type, $action, $params = array()){
        return Docebo::createAbsoluteUrl('centralrepo/' . $lo_type . 'Lo/' . $action, $params);
    }
}