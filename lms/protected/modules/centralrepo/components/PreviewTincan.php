<?php

class PreviewTincan extends Preview
{
    public static $view = 'tincan';

    public function getContent() {

        $object = $this->getObject();
        if (!$object || $object->object_type != LearningOrganization::OBJECT_TYPE_TINCAN) {
            throw new CException('Invalid TinCan Learning Object');
        }

        // Launcher Activity (as per tincan.xml)
        $activity = LearningTcActivity::model()->findByPk($object->id_resource);
        if (!$activity) {
            throw new CException('Invalid Activity: '.$object->id_resource);
        }

        // Activity Provider (not to mess with Activity (which are children of AP)
        $activityProvider = $activity->ap;  // using AR relation

        $userData = CoreUser::model()->findByPk(Yii::app()->user->id);
        $auth = LearningTcAp::buildBasicAuth($userData);
        $actor = $activityProvider->buildActor($userData);

        // If the TinCan LAUNCH attribute is an absolute URI, we use it to launch it
        // Otherwise, we build an URL to access the TinCan, assumably, uploaded to S3
        if (Docebo::isAbsoluteUri($activity->launch)) {
            $launch = $activity->launch;
        }
        else {
            $storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_TINCAN);
            $launch = $storageManager->fileUrl($activity->launch, $activityProvider->path);
        }

        // Check if the URL has a query and send the result back to JS; just to easy the process
        $parsed = parse_url($launch);
        $hasQueryString = isset($parsed['query']);

        $backUrl = '#';

        $data = array(
            'type' => LearningOrganization::OBJECT_TYPE_TINCAN,
            'idReference' => $object->id_object,
            'activityId' => $activity->id,
            'endpoint' => $this->getLrsEndpoint(),
            'launch' => $launch,
            'actor' => $actor,
            'auth' => $auth,
            'registration' => $activityProvider->registration,
            'back_url' => $backUrl,
            'acceptLanguage' => Yii::app()->getLanguage(),
            'content_token'  => $activityProvider->registration,
            'hasQueryString'	=> $hasQueryString,
        );

        // Let custom plugins intercept this and change params for the launch
        Yii::app()->event->raise('BeforeBuildingXapiLaunchUrl', new DEvent($this, array('data' => &$data)));

        $this->showContent( array(
            'data' => $data,
        ));
    }

    private function getLrsEndpoint(){
        return Docebo::getRootBaseUrl(true) . "/tcapi/";
    }
}