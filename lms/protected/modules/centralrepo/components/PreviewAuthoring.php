<?php

class PreviewAuthoring extends Preview
{
    public static $view = 'authoring';

    public function getContent() {

        $object = $this->getObject();
        if (!$object || $object->object_type != LearningOrganization::OBJECT_TYPE_AUTHORING) {
            throw new CException('Invalid Authoring object');
        }

        //now load html page specific information
        $idAuthoring = (int)$object->id_resource;
        $authoring = LearningAuthoring::model()->findByPk($idAuthoring);
        if (!$authoring) {
            throw new CException('Invalid page ID: '.$idAuthoring);
        }

        // Collect JPG URLs
        $jpgUrls = $authoring->getJpgUrls('full');

        $this->showContent( array(
            'jpgUrls' => $jpgUrls,
            'model' => $authoring,
            'fullscreen' => Yii::app()->request->getParam('fullscreen', false),
        ));
    }
}