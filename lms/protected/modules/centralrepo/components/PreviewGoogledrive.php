<?php

class PreviewGoogledrive extends Preview
{
	public static $view = 'googledrive';

	public function getContent() {

		$object = $this->getObject();
		if (!$object || $object->object_type != LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE) {
			throw new CException('Invalid Google Drive object');
		}

		//now load html page specific information
		$idPage = (int)$object->id_resource;
		$document = LearningGoogledoc::model()->findByPk($idPage);
		if (!$document) {
			throw new CException('Invalid google document ID: '.$idPage);
		}

		$validation = LearningGoogledoc::validateGoogleDriveUrl($document->url);

		$this->showContent( array(
			'document' => $document,
			'error' => $validation
		));
	}
}