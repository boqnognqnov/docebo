<?php

/**
 * Created by PhpStorm.
 * User: ivelin
 * Date: 2016-04-14
 * Time: 9:26 AM
 */
class TreeGenerator
{
    private $object;
    private $params;
    private $scorm_chapter_list  = array();
    private $aicc_chapter_list  = array();

    public function __construct($data, $params = array()){
        $this->object = $data;
        $this->params = $params;
    }

    public function getScormInitTitle($id){
        return Yii::app()->db->createCommand()
            ->select('title')
            ->from(LearningRepositoryObject::model()->tableName())
            ->where('id_object = :id', array(':id' => $id))
            ->queryScalar();
    }

    private function makeShorterDescription($text){
        return mb_strlen($text, 'UTF-8') > 70 ? mb_substr($text, 0, 70 - 3, 'UTF-8').'...' : $text;
    }


    public function generate(){
        //now transform the recordset in a tree structure
        $output = array();
        if (!empty($this->object)) {

            if ($this->object->object_type === LearningOrganization::OBJECT_TYPE_SCORMORG) {
                $this->generateScrom($output);
            }
            else if ( $this->object->object_type === LearningOrganization::OBJECT_TYPE_AICC ) {
                $this->generateAicc($output);
            }
        }

        //return the tree array
        return $output;
    }

    private function generateScrom(&$output) {
        // SCORM data
        $chaptersData = Yii::app()->db->createCommand()
            ->from(LearningScormItems::model()->tableName())
            ->where('idscorm_organization = :id', array(':id' => $this->object->id_resource))
            ->order('idscorm_item')
            ->queryAll(true);

        //store chapters in an array variable, grouping them by id organization of the scorm; map item indentifiers too
        foreach ($chaptersData as $row) {
            if (!isset($this->scorm_chapter_list[ (int) $row['idscorm_organization']])) {
                $this->scorm_chapter_list[(int)$row['idscorm_organization']] = array();
            }
            $this->scorm_chapter_list[(int)$row['idscorm_organization']][(int)$row['idscorm_item']] = $row;
        }

        $shortenedTitle = $this->makeShorterDescription($this->object->version_name);
        if (count($this->scorm_chapter_list[(int)$this->object->id_resource]) == 1) {

            reset($this->scorm_chapter_list[$this->object->id_resource]);
            $idScormItem = key($this->scorm_chapter_list[$this->object->id_resource]);
            $chapter = $this->scorm_chapter_list[$this->object->id_resource][$idScormItem];
            $shortenedTitle = $this->makeShorterDescription($chapter['title']);
            $node = array(
                'level'             => 1,
                'id_object'         => (int)$this->object->id_object,
                'id_resource'       => (int)$this->object->id_resource,
                'title'             => $chapter['title'],
                'scormTitle'        => $this->getScormInitTitle($this->object->id_object),
                'title_shortened'   => $shortenedTitle,
                'short_description' => $this->object->version_description,
                'isRoot'            => true,
                'isFolder'          => false,
                'type'              => 'sco', //change it ?
                'idscorm_item'      => $chapter['idscorm_item'],
                'idscorm_resource'  => $chapter['idscorm_resource'],
                'is_visible'        => (int)$this->object->visible,
            );
        }
        else {
            $node = array(
                'level'             => 1,
                'id_object'         => (int)$this->object->id_object,
                'id_resource'       => (int)$this->object->id_resource,
                'title'             => $this->object->version_name,
                'description'       => $this->object->version_description,
                'short_description' => $this->object->version_description,
                'resource'          => '',
                'isRoot'            => true,
                'title_shortened'   => $shortenedTitle,
                'isFolder'          => true,
                'type'              => 'sco',
                'is_visible'        => (int)$this->object->visible,
            );
        }

        $this->makeScormTree($this->object, $node);
        $output = isset($node['children']) ? $node['children'] : $node;
    }

    private function makeScormTree(&$object, &$parentNode ){
        $idObject       = (int)$object['id_object'];
        $id_resource    = (int)$object['id_resource'];
        if (isset($this->scorm_chapter_list[$id_resource]) && !empty($this->scorm_chapter_list[$id_resource])) {
            $parentItem = (isset($parentNode['idscorm_item']) ? (int)$parentNode['idscorm_item'] : -1);
            $hasChildren = false;

            foreach ($this->scorm_chapter_list[$id_resource] as $chapter) {
                if (($parentItem <= 0 && empty($chapter['idscorm_parentitem'])) || ($chapter['idscorm_parentitem'] == $parentItem)) {

                    if (!$hasChildren) {
                        $hasChildren = true;
                        if (empty($parentNode['id_resource'])) { $parentNode['isFolder'] = true; }
                    }

                    if (!isset($parentNode['children'])) { $parentNode['children'] = array(); }

                    $shortenedTitle = mb_strlen($chapter['title'], 'UTF-8') > 70 ? mb_substr($chapter['title'], 0, 70 - 3, 'UTF-8').'...' : $chapter['title'];

                    $chapterNode = array(
                        'level'             => $chapter['lev']+1,
                        'id_object'         => $idObject,
                        'id_resource'       => $id_resource,
                        'idscorm_item'      => $chapter['idscorm_item'],
                        'title'             => $chapter['title'],
                        'scormTitle'        => $object['version_name'],
                        'title_shortened'   => $shortenedTitle,
                        'short_description' => $object['version_description'],
                        'isFolder'          => empty($id_resource) || ((int) $chapter['nChild'] > 0 || ((int) $chapter['nDescendant'] > 0)),
                        'isRoot'            => false,
                        'type'              => 'sco', //change it ?
                        'identifier'        => $chapter['item_identifier'],
                        'idscorm_resource'  => $chapter['idscorm_resource'],
                        'identifierRef'     => $chapter['identifierref'],
                        'is_visible'        => (int) $object['visible']
                    );
                    //check if this scorm item has children
                    $this->makeScormTree($object, $chapterNode);
                    $parentNode['children'][] = $chapterNode;
                }
            }
        }
    }

    public function generateAicc(&$output){
        //find all AICC in the objects list
        $aiccChaptersData = $this->getAiccItemsData($this->object->id_resource);

        // store chapters in an array variable, grouping them by package ID; map item indentifiers too
        foreach ($aiccChaptersData as $row) {
            $this->aicc_chapter_list[$row['id']] = $row;
        }

        $node = array(
            'level'             => 1,
            'id_object'         => (int)$this->object->id_object,
            'id_resource'       => (int)$this->object->id_resource,
            'title'             => $this->object->version_name,
            'description'       => $this->object->version_description,
            'short_description' => $this->object->version_description,
            'resource'          => '',
            'isRoot'            => true,
            'title_shortened'   => $this->object->version_name,
            'isFolder'          => true,
            'type'              => 'sco',
            'is_visible'        => (int)$this->object->visible,
        );

        $masterAiicItemSystemId = 'NOPARENTSYSTEMID';
        foreach ($this->aicc_chapter_list as $tmpChapter) {
            if ($tmpChapter['parent'] == '/') {
                $masterAiicItemSystemId = $tmpChapter['system_id'];
            }
        }
        //TODO: manage to generate the tree properly

        $this->makeAiccTree($this->object, $node, $masterAiicItemSystemId);
        $output = isset($node['children']) ? $node['children'] : $node;
    }

    private function makeAiccTree(&$object, &$parentNode, &$masterAiicItemSystemId){
        $idResource = $object->id_resource;

        if (!empty($this->aicc_chapter_list)) {

            $parentSystemId = (isset($parentNode['aiccSystemId']) ? $parentNode['aiccSystemId'] : 'NOPARENTSYSTEMID');
            $hasChildren = false;

            // Count number of chapters in this AIIC LO
            foreach ($this->aicc_chapter_list as $chapter) {

                if (
                    (($parentSystemId == 'NOPARENTSYSTEMID') && ($chapter['parent'] == $masterAiicItemSystemId))
                    || 	($chapter['parent'] === $parentSystemId)
                ) {

                    if (!$hasChildren) {
                        $hasChildren = true;
                        if (!$parentNode['hasLaunch']) {
                            $parentNode['isFolder'] = true;
                        }
                    }

                    if ($chapter['item_type'] == LearningAiccPackage::ITEM_TYPE_OBJECTIVE) {
                        $parentNode['hasAiccObjectives'] = true;
                    }


                    if (!isset($parentNode['children'])) {
                        $parentNode['children'] = array();
                    }

                    $shortenedTitle = $this->makeShorterDescription($chapter['title']);

                    // THIS node is going to be sent to the JS Tree structure
                    $chapterNode = array(
                        'level' 			=> $parentNode['level'] + 1,
                        'id_object'      	=> $object['id_object'],
                        'id_resource'       => $object['id_resource'],
                        'idAiccItem' 		=> $chapter['id'],
                        'aiccItemType'		=> $chapter['item_type'],
                        'aiccSystemId'		=> $chapter['system_id'],
                        'hasAiccObjectives'	=> false,
                        'hasLaunch'			=> !empty($chapter['launch']),
                        'title' 			=> $chapter['title'],
                        'title_shortened' 	=> $shortenedTitle,
                        'isFolder' 			=> empty($chapter['launch']),
                        'type' 				=> 'aicc',
                        'prerequisites' 	=> $chapter['prerequisite'],
                        'maxTimeAllowed' 	=> $chapter['max_time_allowed'],
                        'timeLimitAction' 	=> $chapter['time_limit_action'],
                        'masteryScore' 		=> $chapter['mastery_score'],
                        'identifier' 		=> $chapter['system_id'],
                        'is_visible' 		=> (int)$object['visible'],
                        'short_description'	=> $object['version_description']
                    );

                    // Check and use if this item has children
                    $this->makeAiccTree($object, $chapterNode, $masterAiicItemSystemId);

                    // Skip this chapter/item/node if it is an OBJECTIVES having NO children
                    if ( ($chapterNode['aiccItemType'] !== LearningAiccPackage::ITEM_TYPE_OBJECTIVE) || (count($chapterNode['children']) > 0)) {
                        $parentNode['children'][] = $chapterNode;
                    }

                }
                else {
                    // Skipped chapter
                }
            }
        }
    }

    private function getAiccItemsData($id_resource){

        $command = Yii::app()->db->createCommand();
        $command->select("items.*")
            ->from("learning_aicc_item items")
            ->join(LearningAiccPackage::model()->tableName() . ' pac', 'pac.id = items.id_package')
            ->where('pac.id = :ref' , array(":ref" => $id_resource))
            ->order("items.sorting");

        $data = $command->queryAll();

        foreach($data as $key => $info)
        {
            if($info['launch'] === '' && $info['item_type'] === LearningAiccPackage::ITEM_TYPE_AU)
                $data[$key]['launch'] = 'story.html';
        }

        return $data;
    }


    /**
     * Returns the tree of category node for the fancy tree
     */
    public static function getCategoriesTree() {

        $language = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
        $defaultLangCode = Settings::get('default_language', '');
        $includeRoot = true;

        // Cache Category tree in a file
        // NOTE: We must include in the cache key ALL possible factors (actors) that may change the tree!
        if (isset(Yii::app()->cache_categorytree)) {

            $cacheKeyActors = array(
                $language,
                $defaultLangCode,
                $includeRoot,
                Yii::app()->user->id
            );

            $cacheKey = md5(json_encode($cacheKeyActors));

            $tree = Yii::app()->cache_categorytree->get($cacheKey);
            if ($tree) {
                return $tree;
            }
        }

        $_treeData = self::getCatalogTreeData($language);

        // Cache the result
        if (isset(Yii::app()->cache_categorytree)) {
            Yii::app()->cache_categorytree->set($cacheKey, $_treeData);
        }

        return $_treeData;
    }

    private static function getCatalogTreeData($language = false) {

        $showRoot = true;

        //initialize output variable
        $output = array();

        //detect language, if not specified
        if (!$language) { $language = Lang::getCodeByBrowserCode(Yii::app()->getLanguage()); }

        //retrieve nodes data from DB
        $data = Yii::app()->db->createCommand()
            ->select("ct.*, c.translation")
            ->from(LearningCourseCategoryTree::model()->tableName()." ct")
            ->leftJoin(
                LearningCourseCategory::model()->tableName()." c",
                "c.idCategory = ct.idCategory AND c.lang_code = :lang_code",
                array(':lang_code' => $language)
            )
            ->order("c.translation")
            ->queryAll();

        //default english node titles translations, in case of untranslated node names
        $defaultLangCode = Settings::get('default_language', '');
        $defaultTitles = array();
        $criteria = new CDbCriteria();
        $criteria->addCondition("lang_code = :lang_code");
        $criteria->params[':lang_code'] = $defaultLangCode;
        $_transList = LearningCourseCategory::model()->findAll($criteria);
        if (!empty($_transList)) {
            foreach ($_transList as $_transItem) {
                $defaultTitles[(int)$_transItem->idCategory] = trim($_transItem->translation);
            }
        }

        //create tree structure from DB data
        if (!empty($data)) {
            //tree retrieval function
            $retrieveChildren = function($node = false) use(&$retrieveChildren, &$data, &$defaultTitles, $showRoot) {
                $output = array();
                $firstLevel = ($showRoot ? 1 : 2);
                $checkLevel = ($node ? ($node['lev'] + 1) : $firstLevel); //children level
                if ($checkLevel < $firstLevel) { $checkLevel = $firstLevel; }
                for ($i=0; $i<count($data); $i++) {
                    //check if the node is a child of the function argument's node
                    $isChild = false;
                    if ($data[$i]['lev'] == $checkLevel) {
                        if ($node) {
                            $isChild = ($data[$i]['iLeft'] > $node['iLeft'] && $data[$i]['iRight'] < $node['iRight']);
                        } else {
                            $isChild = true;
                        }
                    }
                    //if it is, then create node structure
                    if ($isChild) {
                        $isRoot = ($data[$i]['lev'] <= 1);
                        $key = (int)$data[$i]['idCategory'];
                        $_nodeTitle = trim($data[$i]['translation']);
                        if (empty($_nodeTitle)) {
                            if (isset($defaultTitles[$key])) {
                                $_nodeTitle = $defaultTitles[$key];
                            }
                        }
                        $output[] = array(
                            'folder' => true,
                            'key' => $key,
                            'title' => $_nodeTitle,
                            'data' => array(
                                'is_root' => (bool)$isRoot
                            ),
                            'expanded' => ($isRoot ? true : false), //only root node is eventually pre-expanded
                            'children' => $retrieveChildren($data[$i])
                        );
                    }
                }
                return $output;
            };

            $output = $retrieveChildren();
        }

        return $output;
    }



}