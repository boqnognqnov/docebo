<?php

class PreviewScormorg extends Preview
{
    public static $view = 'scorm';

    public function getContent() {
        $object = $this->getObject();
        if (!$object || $object->object_type != LearningOrganization::OBJECT_TYPE_SCORMORG) {
            throw new Exception('Invalid Object');
        }
        $this->registerResources();
        $tree = new TreeGenerator($object);
        $objectsTree = $tree->generate();

        $this->showContent(array('data' => $objectsTree));
    }

    public function buildScoInfo(LearningScormItems $scorm_item, $object, $id_user) {

        $id_scorm_item = $scorm_item->idscorm_item;
        $idscorm_resource = $scorm_item->idscorm_resource;


        // retrive info about this chapter play
        $scorm_organization = LearningScormOrganizations::model()->findByPk($object->id_resource);
        if (!$scorm_organization) {
            throw new CException('Invalid Scorm organization: ' . $object->id_resource);
        }
        $scorm_package = LearningScormPackage::model()->findByPk($scorm_organization->idscorm_package);
        if (!$scorm_package) {
            throw new CException('Invalid Scorm package: ' . $scorm_organization->idscorm_package);
        }
        $scorm_resource = LearningScormResources::model()->findByPk($idscorm_resource);
        if (!$scorm_resource) {
            throw new CException('Invalid Scorm resource: ' . $idscorm_resource);
        }

        $storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_SCORM);
        $launch_url = $storageManager->getHostUrl() . Yii::app()->params['scorm']['launcher'];
        $host = trim(preg_replace('/http[s]*:\/\//i', '', Yii::app()->request->getHostInfo()), '/');

        $auth = new AuthToken($id_user);
        $auth_token = $auth->get(true);

        $sco_url = $launch_url
            . "?host=".urlencode($host)
            . "&id_user=".urlencode($id_user)
            . "&id_reference=".urlencode($object->id_resource)
            . "&id_resource=".urlencode($idscorm_resource)
            . "&id_item=".urlencode($id_scorm_item)
            . "&scorm_version=".urlencode($scorm_package->scormVersion ? $scorm_package->scormVersion : '1.2')
            . "&id_package=".urlencode($scorm_organization->idscorm_package)
            . "&auth_code=".urlencode($auth_token)
            . "&preview_mode=".urlencode(true);

        if (Settings::get('scorm_debug', 'off') == 'on') $sco_url .= "&debug=".urlencode("1");

        return array('sco_url' => $sco_url);
    }

}