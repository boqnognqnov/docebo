<?php

class PreviewPoll extends Preview
{
    public static $view = 'poll';
    public static $view_launchpad = '_poll_launchpad';

    public function getContent() {

        $rq = Yii::app()->request;
        $object = $this->getObject();
        $back= $rq->getParam('back', null);
        $play = $back ? false : $rq->getParam('begin' . $object->id_resource, null);
        if (!$object || $object->object_type != LearningOrganization::OBJECT_TYPE_POLL) {
            throw new CException('Invalid Poll object');
        }

        //now load html page specific information
        $idPoll = (int)$object->id_resource;
        $poll = LearningPoll::model()->findByPk($idPoll);
        if (!$poll) {
            throw new CException('Invalid survey ID: '.$idPoll);
        }

        //number of test pages
        $totalPages = $poll->getTotalPageNumber();

        // find the page to display
        $previousPage = $rq->getParam('previous_page', false);
        if ($previousPage === false) {
            $continue = $rq->getParam('continue', false);
            $pageContinue = $rq->getParam('page_continue', false);
            $pageToDisplay = ($pageContinue !== false && $continue !== false) ? $pageContinue : 1;
        } else {
            $pageToDisplay = $previousPage;
            $nextPage = $rq->getParam('next_page', false);
            $prevPage = $rq->getParam('prev_page', false);
            if ($nextPage !== false) { $pageToDisplay++; }
            if ($prevPage !== false) { $pageToDisplay--; }

        }

        $questions = $poll->getQuestionsForPage($pageToDisplay);

        $display = ('objectTypes' . DIRECTORY_SEPARATOR) . (!$play ? self::$view_launchpad : self::$view);
        $this->showContent(array(
            'idPoll' => (int)$idPoll,
            'poll' => $poll,
            'pageToDisplay' => $pageToDisplay,
            'totalPages' => $totalPages,
            'questions' => $questions,
            'startQuestionIndex' => $poll->getInitQuestionSequenceNumberForPage($pageToDisplay),
        ), $display);
    }

    public function registerResources(){

        $assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('player.assets'));

        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile($assetsUrl.'/css/testpoll.css');
    }

}