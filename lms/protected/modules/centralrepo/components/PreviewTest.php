<?php

class PreviewTest extends Preview
{
    public static $view = 'test';
    public static $view_launchpad = '_test_launchpad';

    public function getContent() {

        $rq = Yii::app()->request;
        $object = $this->getObject();
        $back= $rq->getParam('back', null);
        $play = $back ? false : $rq->getParam('begin' . $object->id_resource, null);
        // Order
        $order = $rq->getParam("questionOrder", CJSON::encode(array()));
        if (!$object || $object->object_type != LearningOrganization::OBJECT_TYPE_TEST) {
            throw new CException('Invalid Test object');
        }

        // Collect TEST/USER information for the launcing UI
        if(!$play){
            $testInfo = self::collectTestInfoByLoAndUser($object, Yii::app()->user->id);
            $options = array(
                'mainInfo' 				=> $testInfo['loModel'],
                'testInfo' 				=> $testInfo['testModel'],
                'trackInfo' 			=> $testInfo['trackModel'],
                'commonTrackInfo'		=> $testInfo['commonTrackModel'],
                'testScores' 			=> $testInfo['testScores'],
                'courseUserModel' 		=> $testInfo['courseUserModel'],
                'max_attemp_reached' 	=> $testInfo['max_attemp_reached'],
                'suspended' 			=> $testInfo['suspended'],
                'suspend_time_left' 	=> $testInfo['suspend_time_left'],
                'need_prerequisites' 	=> $testInfo['need_prerequisites'],
                'timeReadable'			=> $testInfo['timeReadable'],
                'prerequisites_obj'		=> $testInfo['prerequisites_obj'],
            );

            /*
             * Since we don't have any tracking data
             * If the test has Single Question per page and Random order
             * We must somehow keep track of what is happening
             * in order not to show the same question twice
             *
             * On the very first page if the selected test have those options (Single question per page, Random order of the questions)
             * we will calculate the random order only the first time
             * and then we will show the the questions BASED on the order that we will make here
             */

            $test = $testInfo['testModel'] !== null ? $testInfo['testModel'] : null;
            if( $test !== null) {

                if( $test->display_type == LearningTest::DISPLAY_TYPE_SINGLE && $test->order_type != LearningTest::ORDER_TYPE_SEQUENCE ) {

                    $questionsOrder = $this->getSingleQuestionPerPaegeRandomOrder($test);
                } else if( $test->display_type == LearningTest::DISPLAY_TYPE_GROUPED && $test->order_type == LearningTest::ORDER_TYPE_RANDOM ) {
                    $questionsOrder = $this->getQuestionsRandomOrder($test);
                }

                $options['questionOrder'] = CJSON::encode($questionsOrder);
            }

        }else{
            $options = self::collectPlayTextQuestions($object, $order);
        }

        $display = ('objectTypes' . DIRECTORY_SEPARATOR) . (!$play ? self::$view_launchpad : self::$view);
        $this->showContent($options, $display);
    }

    private function getSingleQuestionPerPaegeRandomOrder($test){

        $questionsOrder = array();
        $questionsFormated = array();

        $questionsQuery = Yii::app()->db->createCommand()
            ->select(array( "idQuest", "idCategory"))
            ->from(LearningTestQuestRel::model()->tableName() . " ltqr")
            ->leftJoin(LearningTestquest::model()->tableName() . " ltq", "ltq.idQuest = ltqr.id_question")
            ->where(array(
                "and",
                "ltqr.id_test = :test",
                array("not in", "ltq.type_quest", array(LearningTestquest::QUESTION_TYPE_BREAK_PAGE, LearningTestquest::QUESTION_TYPE_TITLE))
            ))
            ->queryAll(true,array(":test" => $test->idTest));

        // Check if we should gourp the questions by Category
        if($test->order_type == LearningTest::ORDER_TYPE_RANDOM_CATEGORY) {

            // Let's order the questions by their category
            $questCat = array();
            foreach ($questionsQuery as $question) {
                $questCat[$question["idCategory"]][] = $question["idQuest"];
            }

            // Let's check how much questions from category we should get
            $orderInfo = CJSON::decode($test->order_info);

            foreach($orderInfo as $info) {
                // Does the category exists at all ???
                if ( isset($questCat[$info["id_category"]]) ) {
                    // Get all the questions for CERTAIN category
                    $categoryQuestions = $questCat[$info["id_category"]];
                    // We got all the question for Certain Category but since the order is RANDOM
                    // we should reorder them first
                    shuffle($categoryQuestions);
                    // After the reordering let's get the exact amount of questions from this category
                    $selectedCateQuest = array_slice($categoryQuestions, 0, $info["selected"]);
                    // After we obtained the question's from this category let's add them to all the questions for the test
                    $questionsFormated = array_merge($questionsFormated, $selectedCateQuest);
                }
            }
        } else {
            foreach($questionsQuery as $question) {
                $questionsFormated[] = $question['idQuest'];
            }
        }

        shuffle($questionsFormated);

        foreach($questionsFormated as $key => $quest){
            $questionsOrder[$key+1] = $quest;
        }

        return $questionsOrder;
    }

    private function getQuestionsRandomOrder($test) {
        $questionsOrder = array();
        $questionsQuery = Yii::app()->db->createCommand()
            ->select(array( "idQuest", "page"))
            ->from(LearningTestQuestRel::model()->tableName() . " ltqr")
            ->leftJoin(LearningTestquest::model()->tableName() . " ltq", "ltq.idQuest = ltqr.id_question")
            ->where(array(
                "and",
                "ltqr.id_test = :test",
                array("not in", "ltq.type_quest", array(LearningTestquest::QUESTION_TYPE_BREAK_PAGE))
            ))
            ->queryAll(true,array(":test" => $test->idTest));

        $questionsFormatted = array();
        foreach($questionsQuery as $question) {
            $questionsFormatted[$question["page"]][] = $question['idQuest'];
        }

        foreach($questionsFormatted as $page => $questions){
            shuffle($questions);
            $questionsOrder[$page] = $questions;
        }

        return $questionsOrder;
    }

    public static function collectPlayTextQuestions($object, $order){
        $rq = Yii::app()->request;
        $idUser = Yii::app()->user->id;
        $test = LearningTest::model()->findByPk($object->id_resource);
        $idTest = $test->getPrimaryKey();

        // cast display to one quest at time if the time is by quest
        $displayType = ($test->time_dependent == LearningTest::TIME_YES_QUEST ? LearningTest::DISPLAY_TYPE_SINGLE : $test->display_type);
        //number of test pages
        $totalPages = $test->getTotalPageNumber();
        // find the page to display
        $previousPage = $rq->getParam('previous_page', FALSE);
        if ($previousPage === FALSE) {
            $continue = $rq->getParam('continue', FALSE);
            $pageContinue = $rq->getParam('page_continue', FALSE);
            if ($pageContinue !== FALSE && $continue !== FALSE) {
                $pageToDisplay = $pageContinue;
            } else {
                if($rq->getRequestType() == 'GET' && !$rq->isAjaxRequest) {
                    $page = (int)$rq->getParam('page', FALSE);
                    if($page && $page <= $totalPages){
                        $pageToDisplay = $page;
                    }
                } else {
                    $pageToDisplay = 1;
                }
            }
        } else {
            $pageToDisplay = $previousPage;
            $nextPage = $rq->getParam('next_page', FALSE);
            $prevPage = $rq->getParam('prev_page', FALSE);
            if ($nextPage !== FALSE) { $pageToDisplay++; }
            if ($prevPage !== FALSE) { $pageToDisplay--; }

        }

        $invalidBackward = false;

        if(!$pageToDisplay)
            $pageToDisplay = 1;

        $questions = $test->getPreviewQuestionsForPage($pageToDisplay, $order);

        if (empty($questions)) {
            $_SESSION['correctAnswersMsg'] = Yii::t('test', "All questions in this page have already been answered correctly");
        }

        $lockEdit = FALSE;
        $startTime = FALSE;
        $timeInQuest = FALSE;

        if ($test->time_dependent == LearningTest::TIME_YES || $test->time_dependent == LearningTest::TIME_YES_QUEST) {
            if ($test->time_dependent == LearningTest::TIME_YES) {
                //time limit is for test
                $startTime = $test->time_assigned;
            } elseif ($test->time_dependent == LearningTest::TIME_YES_QUEST) {
                //time limit is for question
                $startTime = $questions[0]['time_assigned'];
            }
        }

        $pageIndex = $test->display_type == LearningTest::DISPLAY_TYPE_SINGLE ? ($pageToDisplay - 1) : $pageToDisplay;

        return array(
            'idTest' => (int)$idTest,
            'test' => $test,
            'pageToDisplay' => $pageToDisplay,
            'totalPages' => $totalPages,
            'lockEdit' => $lockEdit,
            'displayType' => $displayType,
            'startTime' => $startTime,
            'timeInQuest' => $timeInQuest,
            'questions' => $questions,
            'startQuestionIndex' => $test->getInitQuestionSequenceNumberForPage($pageIndex),
            'mandatoryAnswers' => (bool)$test->mandatory_answer,
            'invalidBackward' => $invalidBackward,
            'questionOrder' => $order
        );
    }

    /**
     * Collect various TEST LO information (regarding specific USER), used by the TEST "launch" UI
     * NOTE: Do not use it in loops!!!!
     *
     * @param number $idOrg
     * @param number $idUser
     * @throws CException
     * @return array
     */
    public static function collectTestInfoByLoAndUser($object, $idUser) {

        $result = array(
            'loModel' 				=> null,
            'testModel' 			=> null,
            'trackModel' 			=> null,
            'commonTrackModel'		=> null,
            'testScores' 			=> null,
            'courseUserModel' 		=> null,
            'max_attemp_reached' 	=> false,
            'suspended' 			=> false,
            'suspend_time_left' 	=> 0,
            'need_prerequisites' 	=> false,
            'courseModel'			=> null,
        );

        $loModel 	= $object;
        $idTest 	= $loModel->id_resource;
        $testModel 	= LearningTest::model()->findByPk($idTest);
        if (!$testModel) {
            return $result;
        }

        if ($testModel->time_dependent && $testModel->time_assigned) {
            $minutesAssigned = (int)($testModel->time_assigned / 60);
            $secondsAssigned = (int)($testModel->time_assigned % 60);
            $secondsAssigned = str_pad(''.$secondsAssigned, 2, '0', STR_PAD_LEFT);
            $timeReadable = Yii::t('test', '_TEST_TIME_ASSIGNED', array(
                '[time_assigned]' => $minutesAssigned.':'.$secondsAssigned,
                '[second_assigned]' => $secondsAssigned,
                '[minute_assigned]' => $minutesAssigned
            ));
        }

        $testScores = TestUtils::getTestsScores($idTest, $idUser);
        $suspended = false;
        $suspend_time_left = '';
        $need_prerequisites = false;
        $prerequisites_obj = array();

        $result = array(
            'loModel' 				=> $loModel,
            'testModel' 			=> $testModel,
            'testScores' 			=> $testScores,
            'suspended' 			=> $suspended,
            'suspend_time_left' 	=> $suspend_time_left,
            'need_prerequisites' 	=> $need_prerequisites,
            'timeReadable'			=> $timeReadable,
            'prerequisites_obj'     => $prerequisites_obj,
        );

        return $result;

    }
}