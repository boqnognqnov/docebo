<?php

class PreviewLti extends Preview
{
	public static $view = 'lti';

	public function getContent() {

		$object = $this->getObject();
		if (!$object || $object->object_type != LearningOrganization::OBJECT_TYPE_LTI) {
			throw new CException('Invalid LTI Resource');
		}

		//now load LTI resource specific information
		$idResource = (int)$object->id_resource;
		$ltiResource = LearningLti::model()->findByPk($idResource);
		if (!$ltiResource) {
			throw new CException('Invalid LTI Resource ID: '.$idResource);
		}

		// Preparing the LTI Launch link parameters:
		$launchLinkParams = LearningLti::getLaunchParams($object, $ltiResource, null, array('centralRepo' => true));

		$this->showContent( array(
			'launchLinkParams' => $launchLinkParams,
			'resource' => $ltiResource
		));
	}
}