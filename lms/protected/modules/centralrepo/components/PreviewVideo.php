<?php

class PreviewVideo extends Preview
{
    public static $view = 'video';

    public function getContent() {
        $object = $this->getObject();
        if (!$object || $object->object_type != LearningOrganization::OBJECT_TYPE_VIDEO) {
            throw new Exception('Invalid Video Object');
        }

        // Get Video model
        $videoModel = LearningVideo::model()->findByPk($object->id_resource);
        if (!$videoModel) {
            throw new Exception('Invalid Video Object');
        }

        $videoFiles = CJSON::decode($videoModel->video_formats);
        if (empty($videoFiles)) {
            throw new Exception('No valid video file stored');
        }

        $videoFileNames = array_values($videoFiles);
        $storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_VIDEO);
        if (isset($videoFiles['hls_id']) ) {
            $mediaUrl = '';
            $mediaUrlMp4 = $storage->fileUrl($videoFileNames[0], $videoFiles['hls_id']);
            $mediaUrlHlsPlaylist = $storage->fileUrl($videoFiles['hls_id']. ".m3u8", $videoFiles['hls_id']);
        } else {
            $mediaUrl = $storage->fileUrl($videoFileNames[0]);
            $mediaUrlMp4 = $storage->fileUrl($videoFileNames[0]);
            $mediaUrlHlsPlaylist = null;
        }

        $videoSubtitles = LearningVideoSubtitles::model()->findAllByAttributes(array('id_video' => $object->id_resource));
        $subs = array();
        foreach ($videoSubtitles as $videoSubtitle) {
            $subs[$videoSubtitle->lang_code]['url'] = CoreAsset::url($videoSubtitle->asset_id);
            $subs[$videoSubtitle->lang_code]['fallback'] = $videoSubtitle->fallback == 1;
        }

        $this->showContent(array(
            'videoModel'            => $videoModel,
            'mediaUrlMp4'           => $mediaUrlMp4,
            'mediaUrlHlsPlaylist'   => $mediaUrlHlsPlaylist,
            'mediaUrl'              => $mediaUrl,
            'idReference'           => $object->id_resource,
            'subs'                  => $subs
        ));
    }
}