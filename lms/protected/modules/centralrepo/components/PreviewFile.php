<?php

class PreviewFile extends Preview
{
    public static $view = 'file';

    public function getContent() {

        //load LO info from learning_organization table to get idResource of the file
        $object = $this->getObject();
        if (!$object || $object->object_type != LearningOrganization::OBJECT_TYPE_FILE) {
            throw new CException('Invalid File Object');
        }

        //now load file specific information
        $idFile = (int)$object->id_resource;
        $file = LearningMaterialsLesson::model()->findByPk($idFile);
        if (!$file) {
            throw new CException('Invalid file ID: '.$idFile);
        }

        //track the page
        $storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_FILE);
        $size = $storageManager->fileSize($file->path);
        // This is actually SednFile URL
        $downloadUrl = Docebo::createLmsUrl("//centralrepo/centralRepoPreview/sendFile", array(
            "id_file" => $file->idLesson,
            "id_object" => $object->id_object,
            'version' => $object->version
        ));

        $this->showContent( array(
            'fileName' => $file->path,
            'originalFilename' => empty($file->original_filename) ? $file->path : $file->original_filename,
            'fileSize' => $this->formatFileSize($size),
            'fileType' => $this->formatFileType($file->path),
            'fileDescription' => $file->description,
            'downloadUrl' => $downloadUrl,
            'idReference' => $idFile,
            'model'		=> $file,
        ));
    }

    //these format function should be moved into an appropriate CFormatter class

    protected function formatFileSize($size) {
        if ($size >= 1099511627776) {
            $bytes = number_format($size / 1099511627776, 2).' TB';
        } elseif ($size >= 1073741824) {
            $bytes = number_format($size / 1073741824, 2).' GB';
        } elseif ($size >= 1048576) {
            $bytes = number_format($size / 1048576, 2).' MB';
        } elseif ($size >= 1024) {
            $bytes = number_format($size / 1024, 2).' KB';
        } elseif ($size > 1) {
            $bytes = $size.' bytes';
        } elseif ($size == 1) {
            $bytes = $size.' byte';
        } else {
            $bytes = '0 bytes';
        }
        return $bytes;
    }

    protected function formatFileType($name) {
        $extension = strtolower(FileHelper::getExtension($name));
        switch ($extension) {

            //documents
            case 'pdf': { $type ='PDF'; } break;
            case 'rtf': { $type ='RTF'; } break;

            case 'doc':
            case 'docx': { $type ='Word'; } break;
            case 'xls':
            case 'xlsb':
            case 'xlsx': { $type ='Excel'; } break;
            case 'ppt':
            case 'pptx': { $type ='Powerpoint'; } break;

            case 'sxw':
            case 'odt': { $type ='Openoffice document'; } break;
            case 'sxc':
            case 'ods': { $type ='Openoffice spreadsheet'; } break;
            case 'sxi':
            case 'opd': { $type ='Openoffice presentation'; } break;

            case 'txt': { $type ='TXT'; } break;
            case 'csv': { $type ='CSV'; } break;

            case 'htm':
            case 'html': { $type ='HTML'; } break;

            //images
            case 'tif':
            case 'tiff': { $type ='TIFF'; } break;
            case 'jpeg':
            case 'jpg':
            case 'jif':
            case 'jfif': { $type ='JPEG'; } break;

            case 'jp2':
            case 'jpx':
            case 'j2k':
            case 'j2c': { $type ='JPEG 2000'; } break;

            case 'fpx': { $type ='FPX'; } break; //Flashpix
            case 'pcd': { $type ='PCD'; } break; //ImagePac, Photo CD
            case 'gif': { $type ='GIF'; } break;
            case 'png': { $type ='PNG'; } break;

            //archives
            case 'zip': { $type ='ZIP'; } break;
            case 'rar': { $type ='RAR'; } break;
            case 'tar': { $type ='TAR'; } break;
            case '7z': { $type ='7zip'; } break;
            //...
            default: { $type = $extension; } break;
        }
        return $type;
    }
}