<?php

class Preview extends CComponent {

    public static $view;

    protected $_object = null;

    protected $_type = false;

    protected $_version = false;


    protected $_controller = NULL;

    //get / set methods
    public function getType() {
        return $this->_type;
    }

    protected function setType($type) {
        $this->_type = $type;
    }

    protected function setVersion($version) {
        $this->_version = $version;
    }

    public function getVersion() {
        return $this->_version;
    }

    public function getObject() {
        return $this->_object;
    }

    public function setObject(LearningRepositoryObjectVersion $model){
        $this->_object = $model;
        $this->setType($model->object_type);
        $this->setVersion($model->version);
    }

    public function __construct(LearningRepositoryObjectVersion $model) {
        $this->setObject($model);
    }

    public function getContent() { }

    public function showContent($params = array(), $view = null){
        $show = 'objectTypes' . DIRECTORY_SEPARATOR . static::$view;
        if($view !== null){
            $show = $view;
        }
        Yii::app()->controller->renderPartial($show, $params, false, true);
    }

    public static function isValidType($type){
        return LearningOrganization::objectTypes()[$type];
    }

    public static function getPreviewManager(LearningRepositoryObjectVersion $model){
        if (self::isValidType($model->object_type)) {
            $component = 'Preview'.ucfirst(preg_replace_callback("/\_(.)/", function($matches){
                    return strtoupper($matches[1]);
                }, $model->object_type));
            return new $component($model);
        }
    }

    public function registerResources() {}

    public static function generatePreviewUrl($data){
        $params = array("id_object" => $data->id_object );
        switch($data->object_type){
            case LearningOrganization::OBJECT_TYPE_POLL: {
                $idResource = Yii::app()->db->createCommand()
                    ->select("id_resource")
                    ->from(LearningRepositoryObjectVersion::model()->tableName())
                    ->where("id_object = :object")
                    ->order('version DESC')
		            ->limit(1)
                    ->queryScalar(array(":object" => $data->id_object));
                $params["id_poll"] = $idResource;
            } break;
        }
        return Yii::app()->createUrl("centralrepo/centralRepoPreview/preview", $params );
    }

}