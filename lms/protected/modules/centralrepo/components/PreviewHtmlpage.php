<?php

class PreviewHtmlpage extends Preview
{
    public static $view = 'htmlpage';

    public function getContent() {

        $object = $this->getObject();
        if (!$object || $object->object_type != LearningOrganization::OBJECT_TYPE_HTMLPAGE) {
            throw new CException('Invalid Htmlpage object');
        }

        //now load html page specific information
        $idPage = (int)$object->id_resource;
        $page = LearningHtmlpage::model()->findByPk($idPage);
        if (!$page) {
            throw new CException('Invalid page ID: '.$idPage);
        }

        $this->showContent( array(
            'pageModel' => $page,
            'idReference' => $object->id_object,
        ));
    }
}