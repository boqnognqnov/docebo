<div class="player-launchpad-html-container container">
    <div class="row-fluid player-launchpad-header">
        <div class="span12">
            <h2><?= $pageModel->title ?></h2>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div id="htmlpage-content-<?php echo $pageModel->getPrimaryKey(); ?>" class="player-arena-html-inline">
                <?php
                if((strpos($pageModel->textof, '<iframe') !== false)){
                    $html = Yii::app()->htmlpurifier->purify($pageModel->textof, 'allowFrameAndTargetLo');
                    $html = str_replace( '<iframe', '<iframe allowfullscreen ', $html)  ;
                    echo $html;
                }else{
                    echo Yii::app()->htmlpurifier->purify($pageModel->textof, 'allowFrameAndTargetLo');
                }
                ?>
            </div>
        </div>
    </div>
</div>