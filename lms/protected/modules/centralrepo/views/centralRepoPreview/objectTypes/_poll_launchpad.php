<div class="prePreview">

    <div class="row-fluid player-launchpad-header">
        <div class="span12">
            <h2><?php echo $poll->title; ?> </h2>
        </div>
    </div>

    <div class="player-arena-test-inline">
        <div class="row-fluid">
            <div class="span12"><!--<div class="span6">-->
                <div class="row-fluid no-space">
                    <div class="span12">
                        <div class="player-launchpad-test-head-image">
                            <img src="<?= Yii::app()->controller->module->getPlayerAssetsUrl().'/images/testpoll-img.png'; ?>" />
                        </div>
                        <div class="player-launchpad-test-description">
                            <?= $poll->description; ?>
                        </div>
                    </div>
                    <?php
                    if (count($questions) <= 0) {
                        echo '<p>'.Yii::t('poll', '_NO_QUESTION_IN_POLL').'</p>';
                    } else {
                        echo CHtml::submitButton(Yii::t('poll', '_POLL_BEGIN'), array(
                            'name' => 'begin' . $poll->id_poll,
                            'id' => 'begin',
                            'class' => 'btn-docebo big green'
                        ));
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>