<div id="player-lonav">
    <strong><?= Yii::t('central_repo', 'TOC')?></strong>
    <div class="player-lonav-content">
        <div class="player-lonav-tree dynatree-container-parent"></div>
    </div>
</div>

<div id="player-content">
    <strong><?= strtoupper(Yii::t('standard', '_PREVIEW')); ?></strong>
    <div id="innerContent">
        <div class='loadScoContent'></div>
    </div>
</div>

<script>
    $(function(){

        ChapterTree = {
            tree: [],
            options: {
                onChapterSelected: function (chapterData, event) {
                    if(!chapterData.isFolder){
                        Preview.dispatch(chapterData);
                    }
                }
            },

            init: function (options) {
                this.options = $.extend({}, this.options, options);
                var me = this;

                $('.player-lonav-tree').each(function(){
                    $(this).dynatree({
                        selectMode: 1,

                        onCreate: function (node, nodeSpan) {
                            var $nodeSpan = $(nodeSpan);
                            var bgSpan = $('<span class="background-loitem"></span>');

                            if (node.data.isFolder || (node.data.type == 'sco' && node.hasChildren() && !node.data.id_resource)) {
                                bgSpan.addClass('folder');
                            }
                            $nodeSpan.before(bgSpan);

                            $nodeSpan.siblings().addBack().hover(function () {
                                bgSpan.addClass('hover');
                                $(this).addClass('hover');
                                return false;
                            }, function () {
                                bgSpan.removeClass('hover');
                                $(this).removeClass('hover');
                                return false;
                            });
                        },

                        onRender: function (node, nodeSpan) {
                            var $nodeSpan = $(nodeSpan);
                            var cssClass = 'grey is-play';

                            if (!node.data.isFolder && !(node.data.type == 'sco' && node.hasChildren() && !node.data.id_resource)) {

                                // Always try to expand AICC AU items, they MAY have some children
                                // AICC AUs (assignable units) are Playable && Can have children (go figure)
                                if ( (node.data.type == 'aicc') && ((node.data.aiccItemType == 'au') || (node.data.aiccItemType == 'block')) ) {
                                    // @TODO Decide what to do with AU's : expand or not ? (they CAN have children)
                                    //node.expand();
                                }

                            } else {
                                var opened = ($nodeSpan.hasClass('dynatree-expanded'));
                                cssClass = opened ? "is-folder opened" : "is-folder closed ";
                                if (node.hasChildren()) {
                                    cssClass += ' has-children';
                                }
                            }

                            if (node.data.isFolder) {
                                if (node.data.locked) {
                                    $nodeSpan.find('span.dynatree-icon').addClass('i-sprite '+cssClass).removeClass('dynatree-icon');
                                }
                                else {
                                    // AICC "Objectives" are very special.
                                    // Bascially Objectives CAN be folders and are treated as "folders" BY DEFAULT in the sense of LO objects tree
                                    // However, they can have "status" (their own status as part of AICC lessons, NOT reflected by their children)
                                    // So, in case the objective is NOT a REAL FOLDER (no chilren), we treat it as an item with its status.
                                    // For such Objectives, we use the "check" icon (fa-check)
                                    if ((node.data.aiccItemType === 'objective') && (!node.hasChildren())) {
                                        $nodeSpan.find('span.dynatree-icon').addClass('fa fa-check ' + cssClass + ' fg-color-grey').removeClass('dynatree-icon');
                                        $nodeSpan.addClass('aicc-objective');
                                        $nodeSpan.removeClass('dynatree-folder');
                                    }
                                    else {
                                        $nodeSpan.find('span.dynatree-icon').addClass('p-sprite ' + cssClass).removeClass('dynatree-icon');
                                    }
                                }
                            }
                            else {
                                $nodeSpan.find('span.dynatree-icon').addClass('i-sprite '+cssClass).removeClass('dynatree-icon');
                                if(node.data.type == 'sco')
                                    $nodeSpan.addClass('node' + node.data.idscorm_item);
                            }

                            if (node.data.isRoot) { // Root node
                                $nodeSpan.find('.is-folder').addClass('is-root');
                            }


                        },

                        onClick: function (node, event) {
                            if (!node.data.locked)
                                me.options.onChapterSelected(node.data, event);
                        },

                        onActivate: function (node) {
                        },

                        onQueryExpand: function(flag,node) {
                        },
                    });
                });
            },
            setTree: function (tree) {
                this.tree = tree;
                this.reloadTree();
            },
            reloadTree: function () {
                $('.player-lonav-tree').each(function(){
                    $(this).dynatree({
                        children: ChapterTree.tree
                    });
                    $(this).dynatree("getTree").reload();
                });
                this.expandAll();
            },
            expandAll: function () {
                $('.player-lonav-tree').dynatree("getRoot").visit(function (node) {
                    node.expand(true);
                });
                // Try to play the first Object
                $('span.dynatree-node:not(.dynatree-folder)').first().click();
            },
            expandRootOnly: function () {
                $('.player-lonav-tree').each(function(){
                    $(this).dynatree("getRoot").visit(function (node) {
                        node.expand(true);
                        return false;
                    });
                });
            },
            updateNodeProperties: function (node, properties) {
                if (properties.status) {
                    node.data.status = properties.status;
                    var el = $('#lostatus-indicator-'+node.data.key);
                    el.removeClass();
                    el.addClass('player-lonav-lostate '+properties.status);
                    //to do: update ancestors status
                }
            }
        };

        ChapterTree.init();
        ChapterTree.setTree(<?= CJSON::encode($data)?>);
    });
</script>
