<div class="l-testpoll">

    <div class="page-title"><?= $poll->title ?></div>
    <div class="test-play-container">
        <?php
            // Standard info
            echo CHtml::hiddenField('begin' . $poll->id_poll, 'play', array('id' => 'begin'));
            echo CHtml::hiddenField('next_step', 'play', array('id' => 'next-step'));
            // Page info
            echo CHtml::hiddenField('page_to_save', $pageToDisplay, array('page-to-save'));
            echo CHtml::hiddenField('previous_page', $pageToDisplay, array('previous-page'));

            //$questions = $testInfo->getQuestionsForPage();
            $_index = $startQuestionIndex;
            if (!empty($questions)) {
                foreach ($questions as $question) {

                    if ($questionPlayer = PollQuestion::getQuestionManager($question->type_quest)) {

                        $questionPlayer->setController($this);
                        if ($questionPlayer->isInteractive()) {
                            $_index++;
                            $questionIndex = $_index; //TO DO: fix this
                            ?>

                            <?php if (!($_index%2)) : ?>
                                <div class="block-hr"></div>
                            <?php endif; ?>

                            <div class="block <?= (!($_index%2)) ? 'odd' : '' ?> type-<?= $question->type_quest ?>">

                                <div class="block-header">
                                    <?= Yii::t('poll', '_QUEST_'.strtoupper($question->type_quest)) ?>
                                </div>
                                <div class="block-content">

                                    <div class="question-title-wrapper poll" data-question-index="<?= $question->id_quest ?>">
                                        <div class="question-number">
                                            <?=$questionIndex?>)
                                        </div>
                                        <div class="question-title">
                                            <?=$questionPlayer->applyTransformation('title', $question->title_quest, array(
                                                'idQuestion' => $question->id_quest,
                                                'questionManager' => $questionPlayer
                                            ));?>
                                        </div>
                                    </div>

                                    <div class="answers-container">
                                        <?php $questionPlayer->play($question->getPrimaryKey()); ?>
                                    </div>

                                </div>
                            </div>

                            <?php if (!($_index%2)) : ?>
                                <div class="block-hr"></div>
                            <?php endif; ?>

                            <?php
                        } else {
                            //titles, break pages ...
                            echo $questionPlayer->play($question->getPrimaryKey());
                        }
                    } else {
                        //throw ...
                    }
                }
            }
        ?>

        <div class="page-info">
            <?php
            echo Yii::t('test', '_TEST_PAGES').': ';
            echo '<span class="info-page-to-display">'.(int)$pageToDisplay.'</span>';
            echo ' / ';
            echo '<span class="info-total-pages">'.(int)$totalPages.'</span>';
            ?>
        </div>

        <div class="form-actions">
            <?php
                $buttons = array();
                if ($pageToDisplay != 1) {
                    $buttons[] = CHtml::submitButton(Yii::t('test', '_TEST_PREV_PAGE'), array('name' => 'prev_page', 'id' => 'prev-page-btn', 'class' => 'btn-docebo grey big'));
                }

                if($pageToDisplay == 1 || $totalPages <= 1){
                    $buttons[] = CHtml::submitButton(Yii::t('test', '_TEST_PREV_PAGE'), array('name' => 'back', 'id' => 'prev-page-btn', 'class' => 'btn-docebo grey big'));
                }

                if ($pageToDisplay != $totalPages) {
                    $buttons[] = CHtml::submitButton(Yii::t('test', '_TEST_NEXT_PAGE'), array('name' => 'next_page', 'id' => 'next-page-btn', 'class' => 'btn-docebo grey big'));
                }

                echo implode('&nbsp;', $buttons);
            ?>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function() {
        $('div#object_preview input').styler({});
    });

</script>