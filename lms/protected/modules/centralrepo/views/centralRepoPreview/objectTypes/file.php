<div class="file_container">
    <div class="row-fluid player-launchpad-header">
        <div class="span12">
            <h2><?= $model->title ?></h2>
            <?php
            if (PluginManager::isPluginActive('Share7020App')) {
                $this->widget('common.widgets.CourseLoHeaderApp7020', array('loID' => $idReference));
            }
            ?>
        </div>
    </div>
    <div class="player-arena-file-inline">
        <div class="row-fluid">
            <div class="span1"></div>
            <div class="span2 text-center">
                <span class="p-sprite file-extra-large"></span>
            </div>
            <div class="span8">
                <div class="player-launchpad-content-text">
                    <h3 class="file-title">
                        <?php echo $originalFilename; ?>
                    </h3>
                    <div class="file-description native-styled">
                        <?php
                        echo Yii::app()->htmlpurifier->purify($fileDescription); ?>
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <p class="file-type">
                                <strong><?php echo ucfirst(Yii::t('item', '_MIME')); ?>:</strong>
                                <span><?php echo $fileType; ?></span>
                            </p>
                            <p class="file-size">
                                <strong><?php echo ucfirst(Yii::t('file', 'Size')); ?>:</strong>
                                <span><?php echo $fileSize; ?></span>
                            </p>
                        </div>
                        <div class="span6 text-right">
                            <br/>
                            <a id="file-download-<?php echo $idReference; ?>" href="<?php echo $downloadUrl; ?>" class="btn-docebo green big">
                                <span class="i-sprite is-download white"></span> <?php echo Yii::t('standard', '_DOWNLOAD'); ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>