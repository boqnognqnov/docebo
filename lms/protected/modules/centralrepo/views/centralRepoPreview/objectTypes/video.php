<?php /* @var $videoModel LearningVideo */ ?>

<div class="player-launchpad-header" style="display: block;">
    <h2><?= $videoModel->title ?></h2>
    <?php
    if (PluginManager::isPluginActive('Share7020App')) {
        $this->widget('common.widgets.CourseLoHeaderApp7020', array('loID' => $idReference));
    }
    ?>
</div>

<?php

if($videoModel->type === LearningVideo::VIDEO_TYPE_UPLOAD):

// Check if HLS is disabled by global Yii parameter
$disableHls = false;
if (isset(Yii::app()->params['disableHls'])) {
    $disableHls = Yii::app()->params['disableHls'];
}

$playerWidget = $this->widget('common.widgets.Flowplayer', array(
        'disableHls' => $disableHls,
        'mediaUrlMp4' => $mediaUrlMp4,
        'mediaUrlHlsPlaylist' => $mediaUrlHlsPlaylist,
        'title' => $videoModel->title,
        'skin' => 'functional',
        'seekable' => $videoModel->seekable,
        'subtitlesUrls' => $subs
    )
);

$containerId = $playerWidget->containerId;
?>

<script type="text/javascript">
    /*<![CDATA[*/
    var progress<?= $playerWidget->id ?> = 0;
    $(document).ready(function () {

        // Make all PHP based JS variable here, in advance, for the sake of readbility
        var idReference = <?= json_encode((int) $idReference) ?>;
        var containerId = <?= json_encode($containerId) ?>;

        var statusAbInito = <?= json_encode(LearningCommontrack::STATUS_AB_INITIO) ?>;
        var statusAttempted = <?= json_encode(LearningCommontrack::STATUS_ATTEMPTED) ?>;
        var statusCompleted = <?= json_encode(LearningCommontrack::STATUS_COMPLETED) ?>;
        var periodOfBookmarkSaving = 30;
        // Start Flowplayer
        var fpObject = flowplayer($(containerId));


        //Reset the bookmarking counter if the user changes the playback pointer(seeks by hand)
        fpObject.on("beforeseek", function (event) {
            progress<?= $playerWidget->id ?> = 0;
        });

    });
    /*]]>*/
</script>

<?php else: ?>
    <div id="embed-video-preview">
    <?php
    $options = array(
        'videoId' => $videoModel->videoId,
        'url' => $videoModel->url
    );
    if($videoModel->type === LearningVideo::VIDEO_TYPE_YOUTUBE){
        $this->renderPartial('lms.protected.modules.video.views.default._youtube_launchpad', $options);
    } else if($videoModel->type === LearningVideo::VIDEO_TYPE_VIMEO){
        $this->renderPartial('lms.protected.modules.video.views.default._vimeo_launchpad', $options);
    } else if($videoModel->type === LearningVideo::VIDEO_TYPE_WISTIA){
        $this->renderPartial('lms.protected.modules.video.views.default._wistia_launchpad', $options);
    }
    ?>
    </div>
<?php endif; ?>
