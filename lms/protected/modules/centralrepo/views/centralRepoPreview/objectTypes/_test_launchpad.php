<div class="prePreview">
    <div class="row-fluid player-launchpad-header">
        <div class="span12">
            <h2><?php echo $testInfo->title; ?></h2>
        </div>
    </div>
    <?php
        echo CHtml::hiddenField('questionOrder', $questionOrder);
        $numberOfQuestions = $testInfo->getNumberOfQuestionsForStudent();
        $showResult = false;
    ?>
    <div class="player-arena-test-inline">
        <div class="row-fluid">
            <div class="span6">
                <div class="row-fluid no-space">
                    <div class="span12">
                        <div class="player-launchpad-test-head-image">
                            <?php if (!$isEnd): ?>
                                <img src="<?php echo Yii::app()->controller->module->getPlayerAssetsUrl().'/images/testpoll-img.png'; ?>" />
                            <? else: ?>
                                <br/>
                                <div class="review-result">
                                    <div class="row-fluid">
                                        <div class="span2 text-center">
                                            <span class="p-sprite lo-failed-icon-big"></span>
                                        </div>
                                        <div class="span10">
                                            <p><span class="review-result-text"><?= Yii::t('test', '_TEST_FAILED') ?></span></p>
                                            <!-- score -->
                                            <?php if ($testInfo->show_score) : ?>

                                                <div class=" review-scores">
                                                    <?php
                                                    // user score
                                                    $score = 0;
                                                    switch ($testInfo->point_type) {
                                                        case LearningTest::POINT_TYPE_NORMAL : {
                                                            echo '<span class="test_score_note">' . Yii::t('test', '_TEST_TOTAL_SCORE') . '</span> '
                                                                .'<b class="score-passed">'
                                                                . number_format($score, 2)
                                                                . ( $totalMaxScore ? ' / ' . $totalMaxScore : '' )
                                                                . '</b>';
                                                        };break;
                                                        case LearningTest::POINT_TYPE_PERCENT : {
                                                            echo '<span class="test_score_note">' . Yii::t('test', '_TEST_TOTAL_SCORE') . '</span> '
                                                                .'<b class="score-passed">'
                                                                . $score . '</b>';
                                                        };break;
                                                    }
                                                    ?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>

                            <? endif;?>
                        </div>
                        <?php if ($testInfo->description): ?>
                            <div class="player-launchpad-test-description">
                                <?php echo $testInfo->description; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

                <?php if ($numberOfQuestions > 0) : ?>
                    <div class="row-fluid no-space">
                        <?php
                            echo '<div class="span6 text-left">';
                            if ($testInfo->save_keep) {
                                echo '<div class="text_bold">'.Yii::t('test', '_TEST_SAVED').'</div><br />';
                            }
                            if ($testInfo->save_keep) {
                                echo '&nbsp;';
                                echo CHtml::submitButton(Yii::t('test', '_TEST_CONTINUE'), array('name' => 'continue', 'id' => 'continue', 'class' => 'btn-docebo green verybig'));
                            }
                            echo '</div>';
                            echo '<div class="span6 text-right">';
                                echo CHtml::submitButton(Yii::t('test', '_TEST_BEGIN'), array('name' => 'begin' . $testInfo->idTest, 'id' => 'begin', 'class'=>'btn-docebo green verybig'));
                            echo '</div>';
                        ?>
                    </div>
                <?php endif; ?>

            </div>
            <div class="span6 player-launchpad-test-info player-launchpad-content-text">
                <?php if ($testInfo->hide_info <= 0): ?>
                    <h3><?php echo Yii::t('test', '_TEST_INFO'); ?>:</h3>
                    <ul class="test_info_list">
                        <?php if ($testInfo->order_type < 2): ?>
                            <li>
                                <?= Yii::t('test', '_TEST_MAXSCORE', array('[max_score]' => $testInfo->getMaxScore())).($testInfo->point_type==LearningTest::POINT_TYPE_PERCENT ? ' %' : '');?>
                            </li>
                        <?php endif; ?>
                        <li>
                            <?= Yii::t('test', '_TEST_QUESTION_NUMBER', array('[question_number]' => $testInfo->getNumberOfQuestionsForStudent())); ?>
                        </li>
                        <?php if ($testInfo->point_required != 0): ?>
                            <li>
                                <?php
                                $replaceValue = ''.Yii::app()->format->formatNumber($testInfo->point_required);
                                if($testInfo->point_type==LearningTest::POINT_TYPE_PERCENT){
                                    $replaceValue .= ' %';
                                }
                                echo Yii::t('test', '_TEST_REQUIREDSCORE', array('[score_req]' => $replaceValue));
                                ?>
                            </li>
                        <?php endif; ?>
                        <li>
                            <?= ($testInfo->save_keep ? Yii::t('test', '_TEST_SAVEKEEP') : Yii::t('test', '_TEST_SAVEKEEP_NO')); ?>
                        </li>
                        <li>
                            <?=($testInfo->mod_doanswer ? Yii::t('test', '_TEST_MOD_DOANSWER') : Yii::t('test', '_TEST_MOD_DOANSWER_NO')); ?>
                        </li>
                        <li>
                            <?= ($testInfo->can_travel ? Yii::t('test', '_TEST_CAN_TRAVEL') : Yii::t('test', '_TEST_CAN_TRAVEL_NO')); ?>
                        </li>
                        <li>
                            <?= (($testInfo->show_score || $testInfo->show_score_cat) ? Yii::t('test', '_TEST_SHOW_SCORE') : Yii::t('test', '_TEST_SHOW_SCORE_NO')); ?>
                        </li>
                        <li>
                            <?= ($testInfo->show_solution != 1 ? Yii::t('test', '_TEST_SHOW_SOLUTION') : Yii::t('test', '_TEST_SHOW_SOLUTION_NO')); ?>
                        </li>
                        <li>
                            <?php
                                switch ($testInfo->time_dependent) {
                                    case 0 : echo Yii::t('test', '_TEST_TIME_ASSIGNED_NO'); break;
                                    case 1 : echo $time_readable; break;
                                    case 2 : echo Yii::t('test', '_TEST_TIME_ASSIGNED_QUEST'); break;
                                }
                            ?>
                        </li>
                        <?php if ($testInfo->max_attempt > 0): ?>
                            <li><?=  Yii::t('test', '_NUMBER_OF_ATTEMPT', array('[remaining_attempt]' => 0));?></li>
                        <?php endif; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<!--[if IE 8]>
<script type="text/javascript">
    $(function(){
        $('.player-launchpad-test-description img').each(function(e){
            // IE needs this fix, otherwise images are always shown in their original size
            if($(this).attr('width'))
                $(this).css('width', $(this).attr('width'));
            if($(this).attr('height'))
                $(this).css('height', $(this).attr('height'));
        });
    });
</script>
<![endif]-->