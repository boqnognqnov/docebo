<div class="l-testpoll play">

    <div class="page-title"><?= $test->title ?></div>

    <div class="test-play-container">
        <?php
        if(isset($_SESSION['correctAnswersMsg'])){
            echo $_SESSION['correctAnswersMsg'];
            unset($_SESSION['correctAnswersMsg']);
        }

        echo CHtml::hiddenField('questionOrder', $questionOrder);
        // Standard info
        echo CHtml::hiddenField('begin' . $test->idTest, 'play', array('id' => 'begin'));
        echo CHtml::hiddenField('next_step', 'play', array('id' => 'next-step'));
        // Page info
        echo CHtml::hiddenField('page_to_save', $pageToDisplay, array('page-to-save'));
        echo CHtml::hiddenField('previous_page', $pageToDisplay, array('previous-page'));

        //$questions = $testInfo->getQuestionsForPage();
        $_index = $startQuestionIndex;
        if (!empty($questions)) {
            foreach ($questions as $question) {

                if ($questionPlayer = CQuestionComponent::getQuestionManager($question->type_quest)) {
                    $questionPlayer->setController($this);
                    if ($questionPlayer->isInteractive()) {
                        $_index++;
                        $questionIndex = $_index; //TO DO: fix this
                        ?>

                        <div class="block <?= (!($_index%2)) ? 'odd' : '' ?>">

                            <div class="block-header">
                                <?= Yii::t('test', '_QUEST_'.strtoupper($question->type_quest)) ?>
                            </div>
                            <div class="block-content">

                                <div data-question-index="<?= $question->idQuest ?>">
                                    <table class="play-question">
                                        <tbody>
                                        <tr>
                                            <td class="question-index"><?php echo $questionIndex.')'; ?></td>
                                            <td class="question-title"><?php
                                                echo DOCEBO::nbspToSpace($questionPlayer->applyTransformation('title', $question->title_quest, array(
                                                    'idQuestion' => $question->idQuest,
                                                    'questionManager' => $questionPlayer
                                                )));
                                                ?></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <div class="answers-container <?php echo $question->type_quest; ?>">
                                                    <?php $questionPlayer->play($question->getPrimaryKey()); ?>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                        <div class="block-hr"></div>
                        <?php

                    } else {
                        //titles, break pages ...
                        echo $questionPlayer->play($question->getPrimaryKey());
                    }
                } else {
                    //throw ...
                }
            }
        }

        ?>

        <!--[if IE 8]>
        <script type="text/javascript">
            $(function(){
                $('.question-title img').each(function(e){
                    // IE needs this fix, otherwise images are always shown in their original size
                    if($(this).attr('width'))
                        $(this).css('width', $(this).attr('width'));
                    if($(this).attr('height'))
                        $(this).css('height', $(this).attr('height'));
                });
            });
        </script>
        <![endif]-->

        <?php if ($mandatoryAnswers): ?>
            <div class="answer-info text-right">
		<span id="answer-info">
			<?php echo Yii::t('test', '_NEED_ANSWER'); ?>
		</span>
            </div>
        <?php endif; ?>

        <div class="row-fluid">
            <div class="span4">
                <div class="page-info"><?php
                    echo Yii::t('test', '_TEST_PAGES').': ';
                    echo '<span class="info-page-to-display">'.(int)$pageToDisplay.'</span>';
                    echo ' / ';
                    echo '<span class="info-total-pages">'.(int)$totalPages.'</span>';
                    ?>
                </div>
            </div>
            <div class="span8">
                <div class="form-actions">
                    <?php
                        if ($pageToDisplay != 1) {
                            echo CHtml::submitButton(Yii::t('test', '_TEST_PREV_PAGE'), array('name' => 'prev_page', 'id' => 'prev-page-btn', 'class' => 'btn-docebo grey verybig'));
                        }

                        if($pageToDisplay == 1 || $totalPages <= 1){
                            echo CHtml::submitButton(Yii::t('test', '_TEST_PREV_PAGE'), array('name' => 'back', 'id' => 'prev-page-btn', 'class' => 'btn-docebo grey verybig'));
                        }

                        if ($pageToDisplay != $totalPages) {
                            echo CHtml::submitButton(Yii::t('test', '_TEST_NEXT_PAGE'), array('name' => 'next_page', 'id' => 'next-page-btn', 'class' => 'btn-docebo grey verybig'));
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function() {
        $('div#object_preview input').styler({});
    });
    
    $('input').on('keypress', function(e){
        return e.keyCode != 13;
    });

</script>