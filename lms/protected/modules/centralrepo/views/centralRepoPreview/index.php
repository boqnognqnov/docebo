<div class="form previewCentralRepoObject">
    <h1><?= Yii::t('central_repo', 'Training material preview'); ?></h1>

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'centralRepo_preview',
        'htmlOptions' => array(
            'class' => 'ajax',
        )
    )); ?>

    <div class="object_info">
        <?php if( $hasVersioning ) : ?>
            <div class="leftSide">
                <?= CHtml::hiddenField('id_object', $model->id_object)?>
                <?= CHtml::label(Yii::t('central_repo', 'Select version') . ':', 'object_version') ?>
                <?= CHtml::dropDownList('version', $model->version, $model->getObjectVersionsFormatted(), array('id' => 'object_version'))?>
            </div>
        <?php endif; ?>
        <div class="rightSide">
            <div class="versionDetailsTitle">
                <?= ($hasVersioning ? Yii::t('central_repo', 'Version details') : Yii::t('standard',"_DETAILS") ) . ':' ?>
            </div>
            <div class="versionDetails">
                <p><b><?= Yii::t('central_repo', 'Used in') ?>:</b> <?= Yii::t('standard', '{x} courses', array('{x}' => count(LearningOrganization::getLOsForLORepoVersion($model->object_type, $model->id_resource)))) ?></p>
                <p><b><?= Yii::t('central_repo', 'Uploaded By') ?>:</b> <?= $model->getAuthorName() ?></p>
                <?php if($model->version_description) :?>
                    <p><b><?= Yii::t('standard', '_DESCRIPTION') ?>:</b> <?= $model->version_description ?></p>
                <?php endif; ?>
                <?php if( ! $hasVersioning  ) : ?>
                    <p><b><?= Yii::t("stats", "Active since") ?>:</b> <?=Yii::app()->localtime->toLocalDateTime($model->create_date) ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php $data =  CJSON::encode(array('id_object' => $model->id_object, 'version'=> $model->version, 'type' => $model->object_type));?>
    <div id="object_preview" data-object='<?= $data ?>' class="<?= $model->object_type ?>">
        <?php $objectPreview->getContent(); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>