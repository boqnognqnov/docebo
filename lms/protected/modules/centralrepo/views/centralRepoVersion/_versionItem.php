<div class="versionItem">
    <div class="circle ">&nbsp;</div>
    <div class="versionDate">
        <?php
        $userName = '';
        if (isset($data['author'])){
            $authorData = CJSON::decode($data['author']);
            if(isset($authorData['username'])) {
                $userName = $authorData['username'];
            }
        }
        ?>
        <?php echo Yii::app()->localtime->toLocalDateTime($data['create_date']);?>  - <?=Yii::t('standard', 'uploaded by')?> <?=$userName;?>
        <?php
        // Show icon for visible status of current version (for videos only)
        if($data['object_type'] == "video") {
            $versionStatus = LearningRepositoryObjectVersion::getVersionStatus($data['id_resource'], $data['id_object']);
            if ($versionStatus == 1) {
                $class = "fa-check-circle-o";
                $styles = 'style="color: #5fbf5f;"';
            } elseif($versionStatus == -1) {
                $class = "fa-spinner fa-pulse";
                $styles = 'style-color: #666;';
            } else {
                $class = "fa-check-circle-o";
                $styles = 'style="color: #ff2222;"';
            }
        }

        ?>
        <strong><i class="fa <?php echo $class; ?>" <?php echo $styles; ?>></i></strong>
    </div>
    <div class="versionInfo">
        <div class="versionInfoBox">
        <?php
            $coursesCount = count(LearningOrganization::getLOsForLORepoVersion($data['object_type'], $data['id_resource']));
            $class = ($coursesCount == 0) ? 'text-colored-orange text-bold' : 'text-bold';
            $description = (isset($data['version_description']) && !empty($data['version_description'])) ? ' - ' . $data['version_description'] : '';
        ?>
            <?=$data['version_name'];?> - <?=Yii::t('standard', 'Used in')?> <span class="<?=$class?>"> <?=Yii::t('standard', '{n} course|{n} courses', $coursesCount);?></span>  <?=$description;?>
        </div>
        <div class="button-column">
            <?php
            echo CHtml::link('',
                Yii::app()->createUrl("centralrepo/centralRepoVersion/axEdit", array("id_resource" => $data['id_resource'], "object_type" => $data['object_type'])),
                array(
                    'class' => 'edit-field open-dialog',
                    'data-dialog-title' => Yii::t('standard', 'Edit version details'),
                    'data-dialog-class' => 'modal-centralrepo-version-edit',
                ));

            echo CHtml::link('',
                Yii::app()->createUrl("centralrepo/centralRepoVersion/axDelete", array("id_resource" => $data['id_resource'], "object_type" => $data['object_type'])),
                array(
                    'data-dialog-title' => Yii::t('standard', '_DEL'),
                    'class' => 'delete open-dialog',
                    'data-dialog-class' => 'modal-version-delete-message',
                ));
            ?>
        </div>
    </div>
</div>

