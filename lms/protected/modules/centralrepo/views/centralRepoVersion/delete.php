<script>
    function confirmDeleteCategory(ev) {
        if(!$('input[type=checkbox]').is(':checked')){
            ev.preventDefault();
            ev.stopImmediatePropagation();
            return false;
        }
    }
</script>
<style>
    .modal.modal-version-delete-message {height: 330px}
</style>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'version_form',
        'htmlOptions' => array(
            'class' => 'ajax',
        )
    )); ?>
    <input type="hidden" name="id_resource" value="<?=$model->id_resource?>">
    <input type="hidden" name="confirm" value="1" />

    <div class="deleteMessage">
        <i class="fa fa-exclamation-triangle fa-3" aria-hidden="true"></i><br>
        <span class="message-black"><?=Yii::t('standard', 'The training material you want to delete is used in')?> <strong> <?=Yii::t('standard', '{n} course|{n} courses', $coursesCount);?></strong></span><br>
        <span class="message-red"><?=Yii::t('standard', "By deleting this training material, it will be removed from every course where it's used.")?></span>
        <div class="agreeCheck">
            <?php $checkboxIdKey = time(); ?>
            <?php echo $form->checkbox($model, 'confirm', array('id' => CHtml::activeId($model, 'confirm').$checkboxIdKey)); ?>
            <?php echo CHtml::label(Yii::t('standard', 'Yes, I want to proceed!'), CHtml::activeId($model, 'confirm').$checkboxIdKey); ?>
            <?php echo $form->error($model, 'confirm'); ?>
        </div>
    </div>
    <div class="form-actions">
        <!-- No SUBMIT BUTTON here! We handle 'clicks' and do AJAX form submition -->
        <?= CHtml::submitButton(Yii::t('standard', '_CONFIRM'), array('class' 	=> 'confirm-save btn-docebo green big disabled')); ?>
        <?= CHtml::button(Yii::t('standard', '_CLOSE'), array('class' 	=> 'btn-docebo black big close-dialog')); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
    $(document).delegate(".modal-version-delete-message", "dialog2.content-update", function() {
        var e = $(this), autoclose = e.find("a.auto-close");
        if (autoclose.length > 0) {
            try {
                e.find('.modal-body').dialog2("close");
            } catch(e) {}
            var versions = $(autoclose).attr('data-value');
            if (versions == 0) {
                document.location = '<?=Docebo::createAppUrl('centralrepo/centralRepo')?>';
            } else {
                $.fn.yiiListView.update('loRepoVersionsComboListView', {
                    data: {
                        id: <?=$model->id_object;?>
                    }
                });
            }
        }
    });
    (function ($) {
        $('input[type=checkbox]').styler();

        $(document).on('change', 'div.modal-version-delete-message input[type=checkbox]', function(){
            if($(this).is(':checked')){
                $('div.modal-footer a.confirm-save').removeClass('disabled');
            }else{
                $('div.modal-footer a.confirm-save').addClass('disabled');
            }
        });

    })(jQuery);
</script>