<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'version_form',
        'htmlOptions' => array(
            'class' => 'ajax',
        )
    )); ?>
    <?php echo $form->hiddenField($model, 'id_resource'); ?>
    <input type="hidden" name="confirm" value="1" />
    <div class="row-fluid">
        <div class="control-group">
            <div class="controls">
                <label for="<?= CHtml::activeId($model, 'version_name') ?>"><?php echo Yii::t('standard', 'Version'); ?></label>
                <?php echo $form->textField($model, 'version_name'); ?>
                <?php echo $form->error($model, 'version_name'); ?>
            </div>
        </div>

        <div class="control-group">
            <div class="controls">
                <label for="<?= CHtml::activeId($model, 'version_description') ?>"><?php echo Yii::t('standard', 'Description'); ?></label>
                <?php echo $form->textarea($model, 'version_description'); ?>
                <?php echo $form->error($model, 'version_description'); ?>
            </div>
        </div>
    </div>
    <div class="form-actions">
        <!-- No SUBMIT BUTTON here! We handle 'clicks' and do AJAX form submition -->
        <?= CHtml::submitButton(Yii::t('standard', '_CONFIRM'), array('class' 	=> 'confirm-save btn-docebo green big')); ?>
        <?= CHtml::button(Yii::t('standard', '_CLOSE'), array('class' 	=> 'btn-docebo black big close-dialog')); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
    (function ($) {
        $('input').styler();
    })(jQuery);
    $(document).delegate(".modal-centralrepo-version-edit", "dialog2.content-update", function() {
        var e = $(this), autoclose = e.find("a.auto-close");
        if (autoclose.length > 0) {
            try {
                e.find('.modal-body').dialog2("close");
            } catch(e) {}

            $.fn.yiiListView.update('loRepoVersionsComboListView', {
                data:  {
                    id: <?=$model->id_object;?>
                }
            });
        }
    });
</script>