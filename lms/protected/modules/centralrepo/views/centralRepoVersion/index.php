<?php $this->breadcrumbs = array(
    Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
    Yii::t('standard', 'Central Repository') => Docebo::createAppUrl('centralrepo/centralRepo'),
    Yii::t('standard', 'Versions history')
); ?>


<div class="main-actions clearfix">
    <h3 class="title-bold back-button">
        <?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('centralrepo/centralRepo'));?>
        <span>
			<?=Yii::t('standard', 'Versions history')?>: <?=$loRepository->title;?>
		</span>
    </h3>
    <ul class="clearfix">
</div>

<div class="centralRepoVersions grid-view">
    <?php $this->widget('common.widgets.ComboListView', $listViewParams); ?>
</div>

