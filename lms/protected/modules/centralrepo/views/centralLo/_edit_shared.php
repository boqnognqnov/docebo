<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 27.4.2016 г.
 * Time: 15:17
 *
 * @var $versionModel LearningRepositoryObjectVersion
 * @var $this CentralLoController
 * @var $activeVersion integer
 * @var $availableVersions array
 * @var $useCustomView boolean
 * @var $form CActiveForm
 * @var $idOrg integer
 * @var $idCourse integer
 * @var $loModel LearningOrganization
 */
?>

<?php

$htmlOptions = array(
    'id' => 'edit-shared-lo-form',
    'class' => 'form-horizontal',
);

$form = $this->beginWidget('CActiveForm', array(
    'action' => Docebo::createAbsoluteLmsUrl('centralrepo/centralLo/saveShared'),
    'method' => 'post',
    'htmlOptions' => $htmlOptions
));

echo CHtml::hiddenField('id_org', $idOrg);
echo CHtml::hiddenField('course_id', $idCourse);
?>
<div id="player-arena-uploader-panel" class="player-arena-editor-panel player-central-lo-shared-settings">
   <div class="header">
        <?= Yii::t('standard', 'Edit File') ?>
   </div>
   <div class="content">
       <div class="row-fluid">
           <div class="span6">
               <p><?= Yii::t('standard', 'This Learning Object is imported from Central Repository') ?></p>
           </div>
           <div class="span6 text-right">
               <label for="version">
                   <?= Yii::t('standard', 'Select version') ?>
                   <?= CHtml::dropDownList('version', $activeVersion, $availableVersions) ?>
               </label>
           </div>
       </div>
       <?php if(in_array($versionModel->object_type, array(LearningOrganization::OBJECT_TYPE_SCORMORG, LearningOrganization::OBJECT_TYPE_AICC, LearningOrganization::OBJECT_TYPE_TINCAN, LearningOrganization::OBJECT_TYPE_ELUCIDAT))): ?>
       <div class="row-fluid">
           <label for="custom-view">
               <?= Yii::t('standard', 'Enable custom view mode for this course') ?>
               <?= CHtml::checkBox('custom_view', $loModel->params_json !== "{}", array(
                   'value' => 1,
                   'id' => 'enable-custom-view'
               )) ?>
           </label>
       </div>
       <br>
       <div class="row-fluid central-lo-viewmode">
           <div class="overlay <?= $loModel->params_json !== "{}" ? 'active' : '' ?>"></div>
           <div class="row-fluid">
               <strong><?= Yii::t('standard', 'View Mode') ?></strong>
           </div>
           <div class="row-fluid scorm-upload-options">
               <div class="span4 text-center">
                   <div class="row-fluid device-type">
                       <i class="course-device-type devices-sprite dev-desktop-tablet">&nbsp;</i><?= Yii::t('standard', 'Desktop') ?>
                   </div>
                   <div class="row-fluid">
                       <div class="control-group text-left">
                           <div class="controls">
                               <label class="radio" for="desktop_openmode_1"><?= LearningOrganization::openModeName(LearningOrganization::OPENMODE_INLINE) ?>
                                   <input type="radio" id="desktop_openmode_1" name="desktop_openmode" value="<?= LearningOrganization::OPENMODE_INLINE ?>" <?= $loModel->desktop_openmode === LearningOrganization::OPENMODE_INLINE ? 'checked' : '' ?> >
                               </label>
                           </div>
                       </div>
                       <div class="control-group text-left">
                           <div class="controls">
                               <label class="radio" for="desktop_openmode_2"><?= LearningOrganization::openModeName(LearningOrganization::OPENMODE_LIGHTBOX) ?>
                                   <input type="radio" id="desktop_openmode_2" name="desktop_openmode" value="<?= LearningOrganization::OPENMODE_LIGHTBOX ?>" <?= $loModel->desktop_openmode === LearningOrganization::OPENMODE_LIGHTBOX || $loModel->params_json === "{}" ? 'checked' : '' ?> >
                               </label>
                           </div>
                       </div>
                       <div class="control-group text-left">
                           <div class="controls">
                               <label class="radio" for="desktop_openmode_3"><?= LearningOrganization::openModeName(LearningOrganization::OPENMODE_FULLSCREEN) ?>
                                   <input type="radio" id="desktop_openmode_3" name="desktop_openmode" value="<?= LearningOrganization::OPENMODE_FULLSCREEN ?>" <?= $loModel->desktop_openmode === LearningOrganization::OPENMODE_FULLSCREEN ? 'checked' : '' ?>>
                               </label>
                           </div>
                       </div>
                       <div class="control-group text-left">
                           <div class="controls">
                               <label class="radio" for="desktop_openmode_4"><?= LearningOrganization::openModeName(LearningOrganization::OPENMODE_POPUP) ?>
                                   <input type="radio" id="desktop_openmode_4" name="desktop_openmode" value="<?= LearningOrganization::OPENMODE_POPUP ?>" <?= $loModel->desktop_openmode === LearningOrganization::OPENMODE_POPUP ? 'checked' : '' ?>>
                               </label>
                           </div>
                       </div>
                   </div>
               </div>
               <div class="span4 text-center">
                   <div class="row-fluid device-type">
                       <i class="course-device-type devices-sprite dev-tablet dev-large">&nbsp;</i><?=Yii::t('standard', 'Tablet')?>
                   </div>
                   <div class="row-fluid">

                       <div class="control-group text-left">
                           <div class="controls">
                               <label class="radio" for="tablet_openmode_1"><?= LearningOrganization::openModeName(LearningOrganization::OPENMODE_FULLSCREEN) ?>
                                   <input type="radio" id="tablet_openmode_1" name="tablet_openmode" value="<?= LearningOrganization::OPENMODE_FULLSCREEN ?>" <?= $loModel->tablet_openmode === LearningOrganization::OPENMODE_FULLSCREEN || $loModel->params_json === "{}" ? 'checked' : '' ?>>
                               </label>
                           </div>
                       </div>
                       <div class="control-group text-left">
                           <div class="controls">
                               <label class="radio" for="tablet_openmode_2"><?= LearningOrganization::openModeName(LearningOrganization::OPENMODE_POPUP) ?>
                                   <input type="radio" id="tablet_openmode_2" name="tablet_openmode" value="<?= LearningOrganization::OPENMODE_POPUP ?>"  <?= $loModel->tablet_openmode === LearningOrganization::OPENMODE_POPUP ? 'checked' : '' ?>>
                               </label>
                           </div>
                       </div>

                   </div>

               </div>
               <?php // all elearning are smatphone capable now // if ($courseModel->course_type == LearningCourse::TYPE_MOBILE) : ?>
               <div class="span4 text-center">
                   <div class="row-fluid device-type">
                       <i class="course-device-type devices-sprite dev-smartphone">&nbsp;</i><?=Yii::t('standard', 'Smartphone')?>
                   </div>
                   <div class="row-fluid">

                       <div class="control-group text-left">
                           <div class="controls">
                               <label class="radio" for="smartphone_openmode_1"><?= LearningOrganization::openModeName(LearningOrganization::OPENMODE_FULLSCREEN) ?>
                                   <input type="radio" id="smartphone_openmode_1" name="smartphone_openmode" value="<?= LearningOrganization::OPENMODE_FULLSCREEN ?>" checked>
                               </label>
                           </div>
                       </div>

                   </div>
               </div>
               <?php // endif; ?>
           </div>
       </div>
       <?php endif; ?>
       <br><br>
       <div class="text-right">
           <input type="submit" id="player-save-uploaded-lo"
                  class="btn-docebo green big" value="<?php echo Yii::t('standard', '_SAVE'); ?>">&nbsp;&nbsp;
           <a id="player-cancel-uploaded-lo" href="#"
              class="btn-docebo black big"><?php echo Yii::t('standard', '_CANCEL'); ?></a>
       </div>
   </div>
</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">
    $(function() {
        $('.player-central-lo-shared-settings input').styler();
        var centralLo = new LoCentralRepoEdit();
        centralLo.formListeners();
    });
</script>