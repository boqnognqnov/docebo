<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 28.4.2016 г.
 * Time: 13:43
 *
 * @var $this CentralLoController
 * @var $idOrg integer
 * @var $idCourse integer
 * @var $version string
 * @var $loUsers integer
 * @var $form CActiveForm
 * @var $customView integer
 * @var $desktopMode string
 * @var $smartphoneMode string
 * @var $tabletMode string
 */

$form = $this->beginWidget('CActiveForm', array(
    'htmlOptions' => array(
        'id' => 'save-shared-lo-form',
        'class' => 'form-horizontal',
    ),
));

echo CHtml::hiddenField('version', $version);
echo CHtml::hiddenField('id_org', $idOrg);
echo CHtml::hiddenField('course_id', $idCourse);
echo CHtml::hiddenField('custom_view', $customView);
echo CHtml::hiddenField('desktop_openmode', $desktopMode);
echo CHtml::hiddenField('smartphone_openmode', $smartphoneMode);
echo CHtml::hiddenField('tablet_openmode', $tabletMode);
echo CHtml::hiddenField('confirm', true);
?>
<div class="version-change-modal">
    <div style="margin-bottom: 20px;"><?=Yii::t('standard', "There are <b>{users}</b> users currently using this training material.", array('{users}' => $loUsers))?></div>
    <?php
    $this->widget('common.widgets.warningStrip.WarningStrip', array (
        'message'=> Yii::t('standard', "By clicking on <b>{button_text}</b>, such users will have to start this object from the beginning.",
            array('{button_text}' => Yii::t('standard', '_SAVE'))),
        'type'=>'warning'
    ));
    ?>
    <br>
    <div class="pull-right">
        <input type="submit" id="player-save-uploaded-lo" class="btn-docebo green big" value="<?php echo Yii::t('standard', '_SAVE'); ?>" name="confirm-button">&nbsp;&nbsp;
        <a id="player-cancel-save-version" href="#" class="btn-docebo black big"><?php echo Yii::t('standard', '_CANCEL'); ?></a>
    </div>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    $(function() {
        var centralRepo = new LoCentralRepoEdit();
        centralRepo.formListeners();
    });
</script>
