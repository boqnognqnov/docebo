<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 10.5.2016 г.
 * Time: 14:08
 *
 * @var $dataProvider CActiveDataProvider
 */

echo "<h1>" . Yii::t('standard', 'Training material Usage') . '</h1>';
?>
<div id="training-material-usage">
    <?= CHtml::beginForm('', 'post', array(
        'class' => 'ajax'
    )) ?>
    <div class="filters-wrapper">
        <table class="filters filters_course_management">
            <tbody>
            <tr>
                <td class="table-padding">
                    <div class="input-wrapper">
                        <input data-url="<?= Docebo::createAppUrl('admin:centralrepo/centralRepo/courseUsegaAutocomplete', array('idObject' => $idObject)) ?>" class="typeahead"
                               id="repo-search-input" autocomplete="off" type="text"
                               name="search-input"
                               placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"/>
                        <span>
                            <button class="search-btn-icon"></button>
                        </span>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
<?php

$versionsCount = LearningRepositoryObject::getObjectVersionsCount($idObject);
$isAvailableForVersioning = in_array($model->object_type, LearningRepositoryObject::$objects_with_versioning) && $versionsCount > 1;

$columns = array(
    array(
        'type' => 'raw',
        'header' => Yii::t('standard', '_COURSE'),
        'value' => function ($data) {
            $name = $data['courseName'];
            $url = '';
            switch ($data["course_type"]) {
                case LearningCourse::ELEARNING: {
                    $url = Docebo::createLmsUrl("player/training", array("course_id" => $data["idCourse"]));
                } break;
                case LearningCourse::CLASSROOM: {
                    $url = Docebo::createLmsUrl("ClassroomApp/session/manageLearningObjects", array("course_id" => $data["idCourse"]));
                } break;
                case LearningCourse::TYPE_WEBINAR: {
                    $url = Docebo::createLmsUrl("webinar/session/manageLearningObjects", array("course_id" => $data["idCourse"]));
                } break;
            }
            $options = array(
                'target' => "_blank"
            );
            return '<span class="underline">' . CHtml::link($name, $url, $options) . '</span>';
        },
        'htmlOptions' => array(
            'class' => 'course-usage'
        )
    ),

    array(
        'type' => 'raw',
        'header' => $isAvailableForVersioning ? Yii::t('standard','Version details:') : (Yii::t('standard','_DETAILS').':'),
        'value' => function($data) use ($isAvailableForVersioning) {
            $value = $isAvailableForVersioning ? $data['versionName'] . ' - ' . Yii::app()->localtime->toLocalDateTime($data['createDate']) . ' ' : Yii::app()->localtime->toLocalDateTime($data['createDate']) . ' ';
            if(!empty($data['versionStatus']) && $isAvailableForVersioning){
                $value  .= '<b>(' . Yii::t('standard', 'Latest version') . ')</b>';
            }

            $authorName = CJSON::decode($data['authorData']);

            if(isset($authorName['username'])){
                $value .= ' ' . Yii::t('standard', 'uploaded by') . ' ' . $authorName['username'];
            }
            return $value;
        }
    )
);

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'repo-usage-grid',
    'cssFile'=>false,
    'htmlOptions' => array('class' => 'grid-view clearfix'),
    'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
    'dataProvider' => $dataProvider,
    'template' => '{items}{summary}{pager}',
    'summaryText' => Yii::t('standard', '_TOTAL'),
    'pager' => array(
        'class' => 'DoceboCLinkPager',
        'maxButtonCount' => 8,
    ),
    'ajaxUpdate' => 'all-items',
    'beforeAjaxUpdate' => 'function(id, options) {
                    resetSearchPlaceholder();
                    options.data = $("#' . $filter_form_id . '").serialize();
                }',
    'afterAjaxUpdate' => 'function(id, data){
                    $("#course-management-grid input").styler();
                    $("#'.$data_grid_id.' input").styler();
                    $(document).controls();
                 }',
    'columns' => $columns,
));

echo CHtml::endForm();
?>
</div>
<script>
    applyTypeahead($("#training-material-usage .typeahead"));
</script>
