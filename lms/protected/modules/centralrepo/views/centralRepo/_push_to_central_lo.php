<?php
/* @var $options array */
/* @var $loModel LearningOrganization */
?>

<h1><?= Yii::t('standard', 'Push object to Central Repository') ?></h1>

<?php

if (Yii::app()->user->hasFlash('error')) {
	echo "<div class='alert'>" . Yii::app()->user->getFlash('error') . "</div>";
}
?>

<?php
$form = $this->beginWidget('CActiveForm', array(
	'method' => 'post',
	'htmlOptions' => array(
		'class' => 'ajax'
	)
));

echo Yii::t('player', "Are you sure you want to push training material <strong>'[objectName]'</strong> to Central Repository?", array('[objectName]' => $loModel->title));

?>
<?php if($loModel->objectType == LearningOrganization::OBJECT_TYPE_POLL):?>
<div style="margin-top: 10px;">
	<? $this->renderPartial('lms.protected.modules.centralrepo.views.centralRepo.tracking_mode', array('loModel' => $loModel, 'fieldName' => 'options[local_tracking]')); ?>
</div>
<?php endif; ?>

<div class="form-actions">
	<input class="btn-docebo green big" type="submit" name="confirm_push_lo" value="<?= Yii::t('standard', '_YES'); ?>">
	<input class="btn-docebo black big close-dialog" type="reset" value="<?= Yii::t('standard', '_CANCEL'); ?>">
</div>

<?php $this->endWidget(); ?>


