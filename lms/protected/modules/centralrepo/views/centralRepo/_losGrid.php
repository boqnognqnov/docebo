<?php
    /* @var $data LearningRepositoryObject */

    $columns = array(
        array(
            'type' => 'raw',
            'value' => function($data) {
                return CHtml::link(
                    Yii::t('standard', '_MOVE'),
                    "#" . $data->id_object,
                    array(
                        'class' => 'drag-lo' ,
                        'data-id' => $data->id_object,
                        'data-type' => $data->object_type,
                        "rel" => "tooltip",
                        "title" => Yii::t('central_repo',"Drag on a category to assign this item to it")
                    )
                );
            },
        ),
        array(
            'type' => 'raw',
            'header' => Yii::t('standard', '_NAME'),
            'value' => function ($data) {
                return $data->getTitleWithIcon();
            },
        ),
        array(
            'type' => 'raw',
            'header' => Yii::t('standard', 'Usage'),
            'htmlOptions' => array("style" => "min-width: 100px"),
            'value' => function ($data) {
                $coursesCount = count(LearningOrganization::getLOsForLORepo($data['id_object']));
                $empty = $coursesCount === 0 ? true : false;
                return CHtml::link(Yii::t('standard', '{n} course|{n} courses', $coursesCount), $empty ? '#' : Docebo::createAbsoluteLmsUrl('centralrepo/centralRepo/viewUsage', array(
                    'id_object' => $data->id_object
                )), array(
                    'class' => 'underlined ' . ($empty ? 'empty' : ' open-dialog '),
                    'data-dialog-class' => 'material-usage-dialog'
                ));
            },
        ),
        // Additional column for visible status (for videos only)
        array(
            'type' => 'raw',
            'htmlOptions' => array(
                'style' => 'text-align: right; font-size: 19px;',
                'class' => 'conversion-status'
            ),
            'value' => function($data) {
                if($data->object_type == "video") {
                    $errorVersions = $data->checkForProgressVersion($data->id_object);
                    if ($errorVersions > 0) {
                        $class = "fa-spinner fa-pulse";
                        $styles = 'style = "color: #666;"';
                    } else {
                        $errorVersions2 = $data->checkAllVersionErrors($data->id_object);
                        if(!$errorVersions2) {
                            $class = "fa-check-circle-o ready";
                            $styles = 'style = "color: #ff2222;"';
                        } else {
                            $class = "fa-check-circle-o ready";
                            $styles = 'style = "color: #5fbf5f;"';
                        }
                    }
                    return '<strong><i class="fa '.$class.'" ' . $styles . '></i></strong>';
                }
            }
        ),
        array(
          'type' => 'raw',
          'htmlOptions' => array('class' => 'single-icon'),
          'value' => function($data) {
            if (!empty($data->from_csp)) {
              $record = Yii::app()->db->createCommand("SELECT * FROM lti_providers_info WHERE id = :id")->queryRow(true, array(':id' => $data->from_csp));
              $title = '';
              if (!empty($record) && is_array($record) && isset($record['name']) && !empty($record['name'])) {
                $title = ' title='.CJSON::encode($record['name']).' rel="tooltip"';
              }
              return '<i class="fa fa-shopping-cart"'.$title.'></i>';
            }
            return '';
          }
        ),
        array(
            'type' => 'raw',
            'htmlOptions' => array('class' => 'single-icon push-to-course'),
            'value' => function($data){
                $title = Yii::t('central_repo', 'Push <span class="object-name">{object_name}</span> to courses', array('{object_name}'=>$data->title));
                // Check visible of all versions and make button active or not (for videos only)
                if($data->object_type == "video") {
                    $checkForProgress = $data->checkForProgressVersion($data->id_object);
                    if($checkForProgress > 0) {
                        $errorVersions = false;
                    } else {
                        $errorVersions = $data->checkAllVersionErrors($data->id_object);
                    }

                    if (!$errorVersions) {
                        return '<strong><i data-url="' . Docebo::createAdminUrl('centralrepo/CentralRepoPushLOToCourse/wizard', array('singleObjectId' => $data->id_object, 'from_step' => 'selectLearningObjects')) . '" data-title="' . $data->title . '" class="fa fa-share" style="color: #999;"></i></strong>';
                    } else {
                        return CHtml::link('<strong><i class="fa fa-share"></i></strong>', Docebo::createAdminUrl('centralrepo/CentralRepoPushLOToCourse/wizard', array('singleObjectId' => $data->id_object, 'from_step' => 'selectLearningObjects')), array(
                            'class' => 'push_los_to_courses push-repo-object open-dialog',
                            'alt' => $title,
                            'data-dialog-title' => $title,
                            'data-dialog-class' => 'lorepo-push-to-courses users-selector',
                            'data-dialog-id' => 'push-to-course-dialog',
                        ));
                    }
                } else {
                    return CHtml::link('<strong><i class="fa fa-share"></i></strong>', Docebo::createAdminUrl('centralrepo/CentralRepoPushLOToCourse/wizard', array('singleObjectId' => $data->id_object, 'from_step' => 'selectLearningObjects')), array(
                        'class' => 'push_los_to_courses push-repo-object open-dialog',
                        'alt' => $title,
                        'data-dialog-title' => $title,
                        'data-dialog-class' => 'lorepo-push-to-courses users-selector',
                        'data-dialog-id' => 'push-to-course-dialog',
                    ));
                }
            }
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{play}{playDisabled}{versions}{versionsDisabled}{versionsDisabledNotUploadable}{edit}{delete}',
            'htmlOptions' => array('class' => 'button-column', 'style' => 'width: 130px'),
            'buttons' => array(
                'play' => array(
                    'url' => 'Preview::generatePreviewUrl($data)',
                    'label' => '<i class="fa fa-play-circle-o" aria-hidden="true"></i>',
                    'options' => array('class' => 'play-repo-object open-dialog', 'data-dialog-class' => 'preview-lo' , "rel" => "tooltip", "title" => Yii::t('central_repo',"Preview this object")),
                    'visible' => '($data->object_type != LearningOrganization::OBJECT_TYPE_LTI)'
                ),
                'playDisabled' => array(
                    'url' => '',
                    'label' => '<i class="fa fa-play-circle-o" aria-hidden="true"></i>',
                    'options' =>array('class' => 'play-repo-object muted', "rel" => "tooltip", "title" => Yii::t('lti', 'No preview possible for LTI objects')),
                    'visible' => '($data->object_type == LearningOrganization::OBJECT_TYPE_LTI)'
                ),
                'versions' => array(
                    'url' => 'Yii::app()->createUrl("centralrepo/centralRepoVersion", array("id" => $data->id_object ) )',
                    'label' => "",
                    'options' => array('class' => 'versions fa fa-history'),
                    'visible' => '(count($data->versions) > 1)'
                ),
                'versionsDisabled' => array(
                    'url' => '',
                    'label' => "",
                    'options' => array('class' => 'versions fa fa-history muted', "rel" => "tooltip", "title" => Yii::t('central_repo', 'The version history is available when more than one version is uploaded')),
                    'visible' => '(count($data->versions) == 1 && in_array($data->object_type, LearningOrganization::objectUplodableTypes()))'
                ),
                'versionsDisabledNotUploadable' => array(
                    'url' => '',
                    'label' => "",
                    'options' => array('class' => 'versions fa fa-history muted', "rel" => "tooltip", "title" => Yii::t('central_repo', 'This object does not support versioning')),
                    'visible' => '(count($data->versions) == 1 &&  !in_array($data->object_type, LearningOrganization::objectUplodableTypes()))'
                ),
                'edit' => array(
                    'url' => '$data->getEditUrl()',
                    'label' => Yii::t('standard', '_MOD'),
                    'options' => array('class' => 'edit-field'),
                ),
                'delete' => array(
                    'url' => function ($data) {
                        $coursesCount = count(LearningOrganization::getLOsForLORepo($data->id_object));
                        if ($coursesCount == 0)
                            return Yii::app()->createUrl("centralrepo/centralRepo/axSimpleDelete", array("id_object" => $data->id_object));
                        else
                            return Yii::app()->createUrl("centralrepo/centralRepo/axDelete", array("id_object" => $data->id_object));
                    },
                    'options' => array(
                        "class" => 'delete open-dialog',
                        "data-dialog-id" => "delete-dashboard-layout",
                        "data-dialog-class" => "modal-version-delete-message",
                        "data-dialog-title" => Yii::t("standard", "_DEL"),
                        "title" => Yii::t("standard", "_DEL"),
                    ),
                ),
            ),
        ),
    );

    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => $data_grid_id,//'repo-objects-grid',
        'cssFile'=>false,
        'htmlOptions' => array('class' => 'grid-view clearfix'),
        'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
        'dataProvider' => $dataProvider,
        'template' => '{items}{summary}{pager}',
        'summaryText' => Yii::t('standard', '_TOTAL'),
        'pager' => array(
            'class' => 'DoceboCLinkPager',
            'maxButtonCount' => 8,
        ),
        'ajaxUpdate' => 'all-items',
        'beforeAjaxUpdate' => 'function(id, options) {
                    resetSearchPlaceholder();
                    options.data = $("#' . $filter_form_id . '").serialize();
                }',
        'afterAjaxUpdate' => 'function(id, data){
                    $("#course-management-grid input").styler();
                    $("#'.$data_grid_id.' input").styler();
                    $(document).controls();
                    LORepoFTree.attachTreeDrag();
                    LORepoFTree.attachVideoConversionStatus();
                    $("[rel=\'tooltip\']").tooltip();
                 }',
        'columns' => $columns,
    ));
?>
<script type="text/javascript">
    setTimeout(function(){
        LORepoFTree.attachVideoConversionStatus();
    }, 3000);
</script>
