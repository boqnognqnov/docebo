<div class="main-actions clearfix">
    <h3 class="title-bold"><?php echo Yii::t('standard', 'Central Repository'); ?></h3>
    <ul class="clearfix" data-bootstro-id="bootstroNewCourse">
        <li>
            <div>
                <?php
                $newCat = Yii::t('standard', '_NEW_CATEGORY');
                $newCatHelper = Yii::t('helper', 'New category');
                $confirm = Yii::t('standard', '_CONFIRM');
                $cancel = Yii::t('standard', '_CLOSE');
                $orgCreateWidget = $this->widget('admin.protected.extensions.local.widgets.ModalWidget', array(
                    'type' => 'link',
                    'handler' => array(
                        'title' => '<span></span>'.$newCat,
                        'htmlOptions' => array(
                            'class' => 'new-category',
                            'alt' => $newCatHelper,
                        ),
                    ),
                    'modalClass' => 'new-category',
                    'headerTitle' => $newCat,
                    'url' => 'admin:courseCategoryManagement/create',
                    'urlParams' => array( 'central_lo' => true ),
                    'updateEveryTime' => true,
                    'remoteDataType' => 'json',
                    'buttons' => array(
                        array(
                            'title' => $confirm,
                            'type' => 'confirm',
                            'class' => 'confirm-btn',
                        ),
                        array(
                            'title' => $cancel,
                            'type' => 'close',
                            'class' => 'close-btn',
                        ),
                    ),
                    'handleUrlBeforeSend' => 'var tgt = $("#tree-generation-time"); '
                        .'if (tgt.length > 0 && tgt.val() != "") {'
                        .'url += "&treeGenerationTime=" + tgt.val();'
                        .'}',
                    'preProcessAjaxResponse' => 'if (data && ((data.data && data.data.update_tree) || data.update_tree)) { '
                        .'$(".modal.in").modal("hide"); '
                        .'CentraLoCategoriesTreeHelper.reloadCoursesCategoriesTree({'
                        .' title: '.CJSON::encode(Yii::t('course_management', 'Courses categories tree update')).', '
                        .' text: '.CJSON::encode(Yii::t('course_management', 'Courses categories tree has changed and needs to be updated')).', '
                        .' button: '.CJSON::encode(Yii::t('standard', '_CONFIRM'))
                        .'}); '
                        .'return; '
                        .'}'
                ));
                ?>
                <script type="text/javascript">
                    $(document).on('click', '#modal-<?php echo $orgCreateWidget->uniqueId; ?> .confirm-btn',function () {
                        var modal = $('#modal-<?php echo $orgCreateWidget->uniqueId; ?> .modal-body');
                        $.ajax({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': modal.find('form').attr('action'),
                            'cache': false,
                            'data': modal.find("form").serialize(),
                            'success': function (data) {
                                //first check if a tree update has been requested
                                if (data && ((data.data && data.data.update_tree) || data.update_tree)) {
                                    $('#modal-<?php echo $orgCreateWidget->uniqueId; ?>').modal('hide');
                                    CentraLoCategoriesTreeHelper.reloadCoursesCategoriesTree({
                                        title: <?= CJSON::encode(Yii::t('course_management', 'Courses categories tree update')) ?>,
                                        text: <?= CJSON::encode(Yii::t('course_management', 'Courses categories tree has changed and needs to be updated'))?>,
                                        button: <?= CJSON::encode(Yii::t('standard', '_CONFIRM')) ?>
                                    });
                                    return;
                                }
                                //proceed with regular execution
                                if (data.html) {
                                    modal.html(data.html);
                                } else {
                                    $('#modal-<?php echo $orgCreateWidget->uniqueId; ?>').modal('hide');
                                    LORepoFTree.loadTree();
                                }
                            }
                        });
                    });
                </script>
            </div>
        </li>
        <li>
            <div>
                <?php
                echo CHtml::link('<strong><i class="fa fa-share"></i></strong><br />'.Yii::t('central_repo', 'Push training materials to courses'),
                    Docebo::createAdminUrl('centralrepo/CentralRepoPushLOToCourse/wizard'),
                    array(
                        'class' => 'push_los_to_courses open-dialog',
                        'alt' => Yii::t('central_repo', 'Push training materials to courses'),
                        'data-dialog-class' => 'lorepo-push-to-courses users-selector',
                        'data-dialog-title' =>  Yii::t('central_repo','Push training materials to courses'),
                        'data-dialog-id' => 'push-to-course-dialog',
                        'removeOnClose' => 'true',
                    ));
                ?>
            </div>
        </li>
    </ul>
    <div class="info">
        <div>
            <h4></h4>
            <p></p>
        </div>
    </div>
</div>
<script>
    $(function(){
        var jwizard = new jwizardClass({
            dialogId		: 'push-to-course-dialog',
            dispatcherUrl	: <?= json_encode(Docebo::createAdminUrl('centralrepo/CentralRepoPushLOToCourse/wizard')) ?>,
            alwaysPostGlobal: true,
            debug			: false
        });
    });


</script>