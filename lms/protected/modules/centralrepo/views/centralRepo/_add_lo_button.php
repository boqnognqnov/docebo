<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 6.4.2016 г.
 * Time: 9:56
 *
 * @var $this CentralRepoController
 */

?>
    <div id="central-lo-manage-add-lo-button" class="pull-right" data-bootstro-id="bootstroAddTrainingResources">
        <div class="dropdown">
            <a href="#" class="btn-docebo green big" data-toggle="dropdown">
                <span class="p-sprite plus-objects-white-green"></span> <?php echo Yii::t('organization', 'ADD TRAINING RESOURCES'); ?> <span class="p-sprite dropdown-objects-white-green"></span>
            </a>
            <ul class="dropdown-menu pull-right">
                <!-- HINT -->
                <li id="add-lo-type-hint" data-default-text="<?php echo Yii::t('organization', 'Select learning object type to add'); ?>">
					<span class="span12 text-left">
						<?php echo Yii::t('organization', 'Select learning object type to add'); ?>
					 </span>
                </li>
                <!-- LO Types -->
                <li>
                    <div class="row-fluid">
                        <span class="span3 add-lo-operation-type">
                            <span><?php echo Yii::t('organization', 'UPLOAD'); ?></span>
                        </span>
                        <span class="span9 add-lo-type-description"
                              data-hint-icon-class="p-sprite lo-scorm"
                              data-hint-text="<?php echo Yii::t('organization', 'Enable end users to play SCORM content (1.2 and 2004).'); ?>">

                            <a class="lo-upload i-sprite-white p-hover" data-lo-type="<?= LearningOrganization::OBJECT_TYPE_SCORMORG ?>" href="#"><span class="i-sprite is-zip"></span> <?php echo Yii::t('storage', '_LONAME_scormorg'); ?></a>
                        </span>
                    </div>
                </li>
                <li>
                    <div class="row-fluid">
                        <span class="span3"></span>
                        <span class="span9 add-lo-type-description"
                              data-hint-icon-class="fa fa-archive fa-3x"
                              data-hint-text="<?= Yii::t('organization', 'Upload your AICC/HAPC package') ?>">

                            <a class="lo-upload" data-lo-type="<?= LearningOrganization::OBJECT_TYPE_AICC ?>" href="#"><span class="fa fa-archive"></span>&nbsp;&nbsp;AICC</a>
                        </span>
                    </div>
                </li>
                <li>
                    <div class="row-fluid">
                        <span class="span3"></span>
                        <span class="span9 add-lo-type-description"
                              data-hint-icon-class="p-sprite lo-tin-can"
                              data-hint-text="<?= Yii::t('organization', 'Enable your users to play with <strong>TIN CAN - EXPERIENCE API</strong> content.') ?>">

                            <a class="lo-upload i-sprite-white p-hover" data-lo-type="<?= LearningOrganization::OBJECT_TYPE_TINCAN ?>" href="#"><span class="i-sprite is-tincan"></span> <?php echo Yii::t('standard', 'Tin Can'); ?></a>
                        </span>
                    </div>
                </li>
                <li>
                    <div class="row-fluid">
                        <span class="span3"></span>
                        <span class="span9 add-lo-type-description"
                              data-hint-icon-class="p-sprite lo-video"
                              data-hint-text="<?= Yii::t('organization', 'Enable your users to watch video resources in their course (MP4) or embed a video from YouTube, Vimeo, Wistia.') ?>">

                            <a class="lo-upload i-sprite-white p-hover" data-lo-type="<?= LearningOrganization::OBJECT_TYPE_VIDEO ?>" href="#"><span class="i-sprite is-video"></span> <?php echo Yii::t('organization', 'Video'); ?></a>
                        </span>
                    </div>
                </li>
                <li>
                    <div class="row-fluid">
                        <span class="span3"></span>
                        <span class="span9 add-lo-type-description"
                              data-hint-icon-class="p-sprite lo-file"
                              data-hint-text="<?= Yii::t('organization', 'Enable your users to download any type of File locally.') ?>">

                            <a class="lo-upload i-sprite-white p-hover" data-lo-type="<?= LearningOrganization::OBJECT_TYPE_FILE ?>" href="#"><span class="i-sprite is-file"></span> <?php echo Yii::t('standard', '_FILE'); ?></a>
                        </span>
                    </div>
                </li>
                <?php
                // INSERTING POSSIBLE IMPORT TOOLS
                $importTitleShown = false;
                ?>
                <?php if (PluginManager::isPluginActive('ElucidatApp')) { ?>
                    <li>
                        <div class="row-fluid">
                            <span class="span3 add-lo-operation-type">
                                <span><?=(!$importTitleShown)?Yii::t('standard', 'Import'):''?></span>
					        </span>
                            <span class="span9 add-lo-type-description"
                                  data-hint-icon-class="p-sprite is-elucidat"
                                  data-hint-text="<?= Yii::t('standard', 'Enable your users to play Elucidat course materials.') ?>">

                                <a class="lo-create-elucidat i-sprite-white p-hover"
                                   data-lo-type="<?= LearningOrganization::OBJECT_TYPE_ELUCIDAT ?>"
                                   href="<?= Docebo::createAbsoluteAdminUrl('ElucidatApp/elucidatApp/editCentralRepo') ?>">
                                    <span class="i-sprite is-elucidat"></span> Elucidat
                                    <span class="pull-right new-label">NEW</span></a>
                            </span>
                        </div>
                    </li>
                <?php
                    $importTitleShown = true;
                }
                ?>
                <?php if (PluginManager::isPluginActive('GoogleDriveApp')) { ?>
                    <li>
                        <div class="row-fluid">
                            <span class="span3 add-lo-operation-type">
                                <span><?=(!$importTitleShown)?Yii::t('standard', 'Import'):''?></span>
					        </span>
                            <span class="span9 add-lo-type-description"
                                  data-hint-icon-class="p-sprite is-googledrive"
                                  data-hint-text="<?= Yii::t('googledrive', 'Enable your users to view Google Drive documents.') ?>">

                                <a class="lo-create-googledrive i-sprite-white p-hover"
                                   data-lo-type="<?= LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE ?>"
                                   href="<?= Docebo::createAbsoluteLmsUrl('GoogleDriveApp/googleDriveApp/editCentralRepo') ?>">
                                    <span class="i-sprite is-googledrive"></span> <?=Yii::t('google_drive', 'Google Drive')?>
                                    <span class="pull-right new-label">NEW</span></a>
                            </span>
                        </div>
                    </li>
                    <?php
                    $importTitleShown = true;
                }
                ?>
                <?php if (PluginManager::isPluginActive('LectoraApp') && !empty(Settings::get('lectora_url'))) { ?>
                    <li>
                        <div class="row-fluid">
                            <span class="span3 add-lo-operation-type">
                                <span><?=(!$importTitleShown)?Yii::t('standard', 'Import'):''?></span>
					        </span>
                            <span class="span9 add-lo-type-description"
                                  data-hint-icon-class="icon-lectora"
                                  data-hint-text="<?= Yii::t('lectora', 'Enable your users to play Lectora On-Line course materials.') ?>">

                                <a class="lo-create-lectora p-hover"
                                   data-lo-type="<?= LearningOrganization::OBJECT_TYPE_LECTORA ?>"
                                   href="<?=Settings::get('lectora_url')?>" target="_blank">
                                    <span class="icon-lectora"></span> Lectora
                                    <span class="pull-right new-label">NEW</span></a>
                            </span>
                        </div>
                    </li>
                    <?php
                    $importTitleShown = true;
                }
                ?>

                <?php if (PluginManager::isPluginActive('LtiApp')) : ?>
                    <li>
                        <div class="row-fluid">
                            <span class="span3 add-lo-operation-type">
                                <span><?=(!$importTitleShown)?Yii::t('standard', 'Import'):''?></span>
					        </span>
                            <span class="span9 add-lo-type-description"
                                  data-hint-icon-class="p-sprite is-lti"
                                  data-hint-text="<?= Yii::t('standard', 'Enable your users to view LTI resources.') ?>">

                                <a class="lo-create-lti i-sprite-white p-hover"
                                   data-lo-type="<?= LearningOrganization::OBJECT_TYPE_LTI ?>"
                                   href="<?= Docebo::createAbsoluteAdminUrl('LtiApp/LtiApp/editCentralRepo') ?>">
                                    <span class="i-sprite is-lti"></span> LTI
                                    <span class="pull-right new-label">NEW</span></a>
                            </span>
                        </div>
                    </li>
                    <?php
                    $importTitleShown = true;
                endif;
                ?>

                <li>
                    <div class="row-fluid">
                        <span class="span3 add-lo-operation-type">
                            <span><?= Yii::t('standard', '_CREATE') ?></span>
                        </span>
                        <span class="span9 add-lo-type-description"
                              data-hint-icon-class="p-sprite lo-doc-converter"
                              data-hint-text="<?= Yii::t('authoring', 'Convert your Power Point presentations and PDFs documents in a proper Learning Object.') ?>">

                            <a class="lo-authoring i-sprite-white p-hover" data-lo-type="<?= LearningOrganization::OBJECT_TYPE_AUTHORING?>" href="<?= Docebo::createAbsoluteAuthoringUrl('main/create', array('opt' => 'centralrepo')) ?>">
                                <span class="i-sprite is-converter"></span> <?php echo Yii::t('authoring', 'Slides converter'); ?>
                            </a>
                        </span>
                    </div>
                </li>
                <li>
                    <div class="row-fluid">
                        <span class="span3 add-lo-operation-type">
                        </span>
                        <span class="span9 add-lo-type-description"
                              data-hint-icon-class="p-sprite lo-html"
                              data-hint-text="<?= Yii::t('organization', 'Create HTML Page using our inline editing tool'); ?>">

                            <a class="lo-create-htmlpage i-sprite-white p-hover" data-lo-type='<?= LearningOrganization::OBJECT_TYPE_HTMLPAGE ?>' href="<?= CentralRepo::createLoUrl(LearningOrganization::OBJECT_TYPE_HTMLPAGE, 'edit') ?>"><span class="i-sprite is-htmlpage"></span> <?php echo Yii::t('storage', '_LONAME_htmlpage'); ?></a>
                        </span>
                    </div>
                </li>
                <li>
                    <div class="row-fluid">
                        <span class="span3"></span>
                        <span class="span9 add-lo-type-description"
                              data-hint-icon-class="p-sprite lo-poll"
                              data-hint-text="<?= Yii::t('organization', 'Create a Poll'); ?>">

                            <a class="lo-create-poll i-sprite-white p-hover" data-lo-type='<?= LearningOrganization::OBJECT_TYPE_POLL ?>' href="<?= CentralRepo::createLoUrl(LearningOrganization::OBJECT_TYPE_POLL, 'edit') ?>"><span class="i-sprite is-poll"></span> <?php echo Yii::t('storage', '_LONAME_poll'); ?></a>
                        </span>
                    </div>
                </li>
                <li>
                    <div class="row-fluid">
                        <span class="span3"></span>
                        <span class="span9 add-lo-type-description"
                              data-hint-icon-class="p-sprite lo-test"
                              data-hint-text="<?= Yii::t('organization', 'Create your own test assessment, using the Docebo built-in assessment creation tool!'); ?>">

                            <a class="lo-create-test i-sprite-white p-hover" data-lo-type='<?= LearningOrganization::OBJECT_TYPE_TEST ?>' href="<?= CentralRepo::createLoUrl(LearningOrganization::OBJECT_TYPE_TEST, 'edit') ?>"><span class="i-sprite is-test"></span> <?php echo Yii::t('storage', '_LONAME_test'); ?></a>
                        </span>
                    </div>
                </li>
            </ul>
        </div>
    </div>
<script type="text/javascript">
    var centralRepo = new CentralLoUploader();
    centralRepo.globalListeners();
</script>