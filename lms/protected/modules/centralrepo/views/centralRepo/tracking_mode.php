<?php /** @var $loModel LearningRepositoryObject | LearningOrganization */ ?>
<?php /** @var $fieldName string */ ?>
<?php if(!$fieldName)
	$fieldName = "shared_tracking";
	$hasSharedTracking = ($loModel instanceof LearningRepositoryObject) ? $loModel->shared_tracking : 1;
?>
<div class="version-control" style="display: block;">
	<div class="control-group">
		<?php echo CHtml::label(Yii::t('standard', 'Tracking mode'), ' ', array('class' => 'control-label')); ?>
		<div class="controls options">
			<div class="row-fluid">
				<div class="span6">
					<div class="control-group">
						<div class="controls">
							<input type="radio" id="local_tracking" <?=!$hasSharedTracking ? 'checked="checked"': ''?> name="<?=$fieldName?>" value="0">
							<label class="radio" for="shared_tracking">
								<?= Yii::t('standard', 'Local tracking') ?>
								<p class="muted"><?= Yii::t('standard', 'This options will require learners to play again the training material in each course where it is pushed') ?>.</p>
							</label>
						</div>
					</div>
				</div>
				<div class="span6">
					<div class="control-group">
						<div class="controls">
							<input type="radio" id="shared_tracking" <?=$hasSharedTracking ? 'checked="checked"': ''?> name="<?=$fieldName?>" value="1">
							<label class="radio text-left" for="shared_tracking-version">
								<?= Yii::t('standard', 'Shared tracking') ?>
								<p class="muted"><?= Yii::t('standard', 'Use this option if you want learners to take the training material once for each course where it is pushed') ?>.</p>
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function () {
		// Styler, I hate it, but....
		$('input[type="radio"]').styler();
	});
</script>


