<?php $this->breadcrumbs = array(
    Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
    Yii::t('standard', 'Central Repository'),
); ?>

<?php $this->renderPartial('_main_actions'); ?>

<div class="centralRepoManagement">

    <div class="row-fluid centralRepoManagement-header">
        <div class="span4">
            <div class="header"> <?= Yii::t('standard', 'Categories') ?></div>
        </div>
        <div class="span8">
            <div class="header">
                <?= Yii::t('standard', 'Training materials') ?>
                <?= $this->renderPartial("_add_lo_button") ?>
            </div>
        </div>
    </div>

    <div class="row-fluid centralRepoManagement-content">
        <div class="leftSide span4">
            <div class="innerContent">
                <div id="repo-tree-wrapper" class="categories"
                     data-source="<?= Docebo::createLmsUrl('centralrepo/centralRepoCategory/GetCategoryTree')?>"
                     data-drop="<?= Docebo::createLmsUrl('centralrepo/centralRepoCategory/moveLoInCategory')?>"></div>
            </div>
        </div>

        <div class="rightSide span8">
            <div class="innerContent">
                <div class="bottom-section clearfix">
                    <?php $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'repo-management-form',
                        'htmlOptions' => array(
                            'class' => 'ajax-grid-form',
                            'data-grid' => '#repo-objects-grid'
                        ),
                    )); ?>

                    <div class="main-section">
                        <div class="filters-wrapper">
                            <table class="filters filters_course_management">
                                <tbody>

                                <tr>
                                    <td class="table-padding">
                                        <table class="table-border">
                                            <tr>
                                                <td class="group long-search">
                                                    <div class="input-wrapper repo-input-wrapper">
                                                        <input data-url="<?= Docebo::createAppUrl('admin:centralrepo/centralRepo/objectsAutocomplete') ?>"
                                                               class="typeahead repo-text-search-filter"
                                                               id="repo-search-input" autocomplete="off" type="text"
                                                               name="LearningRepositoryObject[title]"
                                                               placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"/>
                                                        <span>
                                                            <button class="search-btn-icon"></button>
                                                        </span>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="table-padding">
                                        <table class="table-border">
                                            <tr>
                                                <td class="group smaller-size">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <?= Yii::t('dashboard', 'Filters').':' ?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="group">
                                                    <table class="full-width">
                                                        <tr>
                                                            <!--<td><?= Yii::t('central_repo', 'Filter by type'); ?></td>-->
                                                            <td>
                                                                <?= $form->dropdownList($repo, 'object_type', $usedTypes, array('prompt' => Yii::t('central_repo', 'Select type'), 'id' => 'filter_by_type'));?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="group<?= (empty($usedCSPs) ? ' hidden-filter' : '') ?>">
                                                    <table class="full-width">
                                                        <tr>
                                                            <!--<td><?= Yii::t('central_repo', 'Filter by C.S.P.'); ?></td>-->
                                                            <td>
                                                                <?= $form->dropdownList($repo, 'from_csp', $usedCSPs, array('prompt' => Yii::t('central_repo', 'Select C.S.P.'), 'id' => 'filter_by_csp'));?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="group descendantsSwitch smaller-size descendants-filter-cell">
                                                    <table>
                                                        <tr>
                                                            <td><?= Yii::t('standard', '_INHERIT').':'; ?></td>
                                                            <td class="yes-no">
                                                                <?php echo Yii::t('standard', '_YES'); ?><?php echo $form->checkbox($repo, 'containChildren', array('value' => 1)); ?><?php echo Yii::t('standard', '_NO'); ?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
                    <?php $this->endWidget(); ?>
                    <div id="grid-wrapper">
                        <?php $this->renderPartial('_losGrid', array(
                            'repo' => $repo,
                            'dataProvider' => $dataProvider,
                            'data_grid_id' => 'repo-objects-grid',
                            'filter_form_id' => 'repo-management-form'
                        )); ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<script type="text/javascript">

    <?php if (!Yii::app()->request->getIsAjaxRequest()) :?>
        function resetSearchPlaceholder() {
            var $search = $('#repo-search-input');
            if ($search.val() == "<?= Yii::t('standard', '_SEARCH'); ?>")
                $('#repo-search-input').val('');
        }
        // Initialize the tree
        LORepoFTree = new LoRepoCategories();
        LORepoFTree.managementListeners();

        $(function(){
            // Polish all the inputs
            $('input').styler();

            // When the page is loaded .... load the tree
            LORepoFTree.loadTree();


            $(document).on("dialog2.closed", 'div.lorepo-push-to-courses', function(e){
                var updateGrid = $(this).find("div#updateCentralRepoGrid")
                if (updateGrid.length > 0) {
                    $('#repo-objects-grid').yiiGridView('update', { data: { id: updateGrid.data("id") } });
                }
            });

            $(document).on('dialog2.content-update', 'div.lorepo-push-to-courses', function(){

                // Check if we are at Confirm Dialog page
                var confirmPage = $("div.modal.lorepo-push-to-courses div.confirmCheckboxDialog");
                if (confirmPage.length > 0) {
                    var confirmButton = $("div.modal.lorepo-push-to-courses a.jwizard-nav.next_button");
                    confirmButton.addClass("disabled");
                }
            });

        });

    <?php endif; ?>


</script>