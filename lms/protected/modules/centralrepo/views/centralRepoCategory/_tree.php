<?php
    $id = 'repo-tree-cat-wrapper';
    $this->widget('common.extensions.fancytree.FancyTree', array(
        'id' => $id,
        'skin' => 'skin-docebo-userman',
        'treeData' => $fancyTreeData,
        'preSelected' => $preselected,
        'fancyOptions' => array(
            //'autoCollapse'	=> true,
            'checkbox' 	=> false,
            'icons'		=> true,
            'extensions' => array('docebo','filter', 'persist', 'table'),
            'persist' => array(
                'store' => 'cookie',
                'types' => 'expanded active focus selected',
                'cookiePrefix' => 'lorepocat-fancytree-',
                'cookie' => array('path' => '/'),
            ),

            //some parameters to handle customized orgchart reloading
            'docebo' => '{}',

            'renderColumns' => 'function(event,data) {return LORepoFTree.treeRenderColumns(event,data);}',
        ),
        'htmlOptions'	=> array(
            'class' => '',
        ),
        'eventHandlers' => array(

            'beforeActivate' => 'function(event, data){ LORepoFTree.showHideDescendantsSwitch(event,data); LORepoFTree.loadRepoGrid(event, data); }',

            'createNode' => 'function(event, data){ LORepoFTree.attachTreeDrop(event, data); }',

        ),
        'showActions' => true,
        'actions' => array(
            'edit' => array(
                'title' => Yii::t('standard', '_MOD'),
                'url' => urldecode(Docebo::createAdminUrl("courseCategoryManagement/edit", array('id' => '{nodeKey}'))),
                'class' => 'node-edit new-category-handler',
                'data-modal-class' => 'edit-category',
                'modal-title' => Yii::t('standard', '_MOD'),
            ),
            'move' => array(
                'title' => Yii::t('standard', '_MOVE'),
                'url' =>  urldecode(Docebo::createAdminUrl("courseCategoryManagement/move", array('id' => '{nodeKey}'))),
                'urlParams' => array(),
                'class' => 'node-move new-category-handler',
                'data-modal-class' => 'move',
                'modal-title' => Yii::t('standard', '_MOVE'),
            ),
            'delete' => array(
                'title' => Yii::t('standard', '_DEL'),
                'class' => 'ajaxModal delete-node',
                'url' => "javascri:void(0)",
                'data-toggle' => "modal",
                'data-modal-class' => "delete-category delete-node",
                'data-modal-title' => Yii::t('standard', '_DEL'),
                'data-url' =>  urldecode(Docebo::getRootBaseUrl(true) . Docebo::createAdminUrl("courseCategoryManagement/delete", array('id' => '{nodeKey}'))),
                'data-buttons' => htmlspecialchars(CJSON::encode( array(
                    array(
                        'type' => 'submit',
                        'title' => Yii::t('standard', '_CONFIRM'),
                    ),
                    array(
                        'type' => 'cancel',
                        'title' => Yii::t('standard', '_CANCEL'),
                    ),
                ) ) ,ENT_QUOTES,Yii::app()->charset) ,
                'modal-request-type' => 'GET',
                'data-before-loading-content' => '',
                'data-after-loading-content' =>  'LORepoFTree.deleteNodeAfterLoadingContent(data)',
                'data-before-submit' => '',
                'data-after-submit' => 'loadCentralRepoTree'
            ),
        ),
    ));

    echo CHtml::hiddenField('tree-generation-time', $treeGenerationTime, array('id' => 'tree-generation-time', 'data-reference-tree' => $id));

?>