<?php
$id = 'push-lo-tree-cat-wrapper';
$config = array(
    'id' => $id,
    'skin' => 'skin-docebo-userman',
    'treeData' => $fancyTreeData,
    'preSelected' => $preselected,
    'fancyOptions' => array(
        //'autoCollapse'	=> true,
        'checkbox' 	=> false,
        'icons'		=> true,
        'extensions' => array('docebo','filter', 'table'),

        //some parameters to handle customized orgchart reloading
        'docebo' => '{}',

        'renderColumns' => 'function(event,data) {return LORepoFTree.treeRenderColumns(event,data);}',
    ),
    'htmlOptions'	=> array(
        'class' => '',
    ),
    'eventHandlers' => array(

        'beforeActivate' => 'function(event, data){ LORepoFTree.showHideDescendantsSwitch(event,data); LORepoFTree.loadPushLoRepoGrid(event, data); }',

    ),
);

$this->widget('common.extensions.fancytree.FancyTree', $config);

echo CHtml::hiddenField('tree-generation-time', $treeGenerationTime, array('id' => 'tree-generation-time', 'data-reference-tree' => $id));

?>