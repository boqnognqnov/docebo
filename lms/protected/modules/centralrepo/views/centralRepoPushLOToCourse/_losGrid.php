<?php
    $columns = array(
        array(
            'value' => '$data->id_object',
            'class' => 'CCheckBoxColumn',
            // Set checkbox disabled if object has not anyone version with visible status 1 or has a version in progress (for videos only)
            'disabled' => 'LearningRepositoryObject::model()->checkProgressAndErrors($data->id_object, $data->object_type) ? false : true',
        ),
        // Additional column for visible status (for videos only)
        array(
            'type' => 'raw',
            'header' => Yii::t('standard', '_STATUS'),
            'htmlOptions' => array("style" => "width: 20px; text-align: center; font-size: 18px;"),
            'value' => function($data){
                if($data->object_type == "video") {
                    $errorVersions = $data->checkForProgressVersion($data->id_object);
                    if ($errorVersions > 0) {
                        $class = "fa-spinner fa-pulse";
                        $styles = 'style = "color: #666;"';
                    } else {
                        $errorVersions2 = $data->checkAllVersionErrors($data->id_object);
                        if(!$errorVersions2) {
                            $class = "fa-check-circle-o";
                            $styles = 'style = "color: #ff2222;"';
                        } else {
                            $class = "fa-check-circle-o";
                            $styles = 'style = "color: #5fbf5f;"';
                        }
                    }
                    return '<strong><i class="fa '.$class.'" ' . $styles . '></i></strong>';
                }
            }
        ),
        array(
            'type' => 'raw',
            'header' => Yii::t('standard', '_NAME'),
            'htmlOptions' => array("style" => "width: 33%;"),
            'value' => function ($data) {
                $hasNewTitleName = isset($data->titleObj);
                return $data->getTitleWithIcon($hasNewTitleName);
            },
        ),
        array(
            'type'   => 'raw',
            'header' => Yii::t('standard', '_VERSION'),
            'htmlOptions' => array("style" => "width: 30%;"),
            'value'  => function ($data) {
                $value = '';

                if(in_array($data->object_type, LearningRepositoryObject::$objects_with_versioning)) {
                    if($data->object_type == "video") {
                        $checkProgress = LearningRepositoryObject::model()->checkForProgressVersion($data->id_object);
                        if($checkProgress < 1) {
                            $visibleVersions = LearningRepositoryObject::model()->checkAllVersionErrors($data->id_object);
                            // Create dropdown list with versions only if object has at least one version with visible status 1 (for videos only)
                            if ($visibleVersions) {
                                $value = $data->getVisibleVersionsDropdown();
                            }
                        }
                    } else {
                        $value = $data->getVersionsDropdown();
                    }
                } else{
                    $version = $data->getLatestVersion();
                    $value = CHtml::hiddenField('objectSingleVersion',$version['version'],array(
                        'class' => 'lorepo-push-lo-modal-version-single-hidden',
                        'style' => 'display: none;'
                    ));
                }


                return $value;
            },
            'htmlOptions'   => array(
                'class'  => 'object-version',
            ),
        ),
    );

    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => $data_grid_id,
        'selectableRows' => 3,
        'cssFile'=>false,
        'htmlOptions' => array('class' => 'grid-view clearfix'),
        'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
        'dataProvider' => $dataProvider,
        'rowCssClassExpression' => 'LearningRepositoryObject::model()->checkProgressAndErrors($data->id_object, $data->object_type) ? "" : disabled',
        'rowHtmlOptionsExpression' => 'LearningRepositoryObject::model()->checkProgressAndErrors($data->id_object, $data->object_type) ? array() : array("style" => "text-decoration: line-through;pointer-events: none;display: none;")',
        'template' => '{items}{summary}{pager}',
        'summaryText' => Yii::t('standard', '_TOTAL'),
        'pager' => array(
            'class' => 'DoceboCLinkPager',
            'maxButtonCount' => 8,
        ),
        'ajaxUpdate' => $data_grid_id,
        'beforeAjaxUpdate' => 'function(id, options) {
                    resetSearchPlaceholder();
                    options.data = $("#' . $filter_form_id . ' input, #' . $filter_form_id . ' select").serialize();
                }',
        'afterAjaxUpdate' => 'function(id, data){
                    $("#course-management-grid input").styler();
                    $("#'.$data_grid_id.' input").styler();
                    $(document).controls();
                    updateSelectedLoAfterGridUpdate();
                 }',
        'columns' => $columns,
    ));
?>
<script type="text/javascript">
    $(function(){
        $(document).on('click', '#lorepo-push-lo-to-course-modal .grid-view .items tr.disabled', function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            return false;
        });

        $(document).on('click', '#lorepo-push-lo-to-course-modal .grid-view .items tr.disabled td', function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            return false;
        });
    });
</script>
