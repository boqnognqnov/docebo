<?php
/**
 * Created by PhpStorm.
 * User: NikVasilev
 * Date: 4.5.2016 г.
 * Time: 14:18
 */
?>
<div class="finished-step-icon" >
    <i class="fa fa-check-circle-o fa-3x" style="color: #5FBF5F;"></i>
</div>
<?php if($manyObjects){ ?>
<div class="finished-step-text" >
    <span>
        <?= Yii::t('standard','<strong>{objects} Training Materials</strong> have been pushed to <strong>{courses} courses</strong>', array('{objects}'=>$countObjects, '{courses}'=>$countCourses));?>
    </span>
</div>
<?php }else{?>
    <div class="finished-step-text">
        <span>
            <?= Yii::t('standard','<strong>{objects_name} </strong> have been pushed to <strong>{courses} courses</strong>', array('{objects_name}'=>$objectName, '{courses}'=>$countCourses));?>
        </span>
    </div>
<?php } ?>

<?php if (!empty($cspErrors)): ?>

    <div class="warning-message-container">
        <div class="warning-message-yellow">

            <i class="fa fa-info-circle fa-2x"></i>
            <?= Yii::t('marketplace', 'You cannot add Marketplace LOs into selling courses').'.' ?>
            <br/>
            <?= Yii::t('marketplace', 'The following training materials could not be imported').':'; ?>
            <ul>
                <?php
                foreach ($cspErrors as $cspError) {
                    $_course = LearningCourse::model()->findByPk($cspError['id_course']);
                    $_object = LearningRepositoryObject::model()->findByPk($cspError['id_object']);
                    //TODO: provide a good graphic format for LOs list
                    echo '<li>';
                    echo Yii::t('marketplace', 'training material {{training_material_name}} in course {{course_name}}', array(
                      '{{training_material_name}}' => '<span class="bold">'.$_object->title.'</span>',
                      '{{course_name}}' => '<span class="bold">'.$_course->name.'</span>'
                    ));
                    echo '</li>';
                }
                ?>
            </ul>
        </div>
    </div>

<?php endif; ?>

<div id="updateCentralRepoGrid" data-id="<?= isset(Yii::app()->session['currentCentralRepoNodeId']) ? Yii::app()->session['currentCentralRepoNodeId'] : LearningCourseCategoryTree::getRootNodeId() ?>"></div>

<style>
    div.lorepo-push-to-courses {
        width: 600px !important;
        margin-left: -250px !important;
        top: 20% !important;
    }

    div.lorepo-push-to-courses div.modal-footer, div.lorepo-push-to-courses .form-actions {
        display: none;
    }

    div.lorepo-push-to-courses div.warning-message-container {
        padding: 0 12px  0 0;
    }

    div.lorepo-push-to-courses div.warning-message-container div.warning-message-yellow {
        padding: 8px 1% 7px 1%;
    }

    div.lorepo-push-to-courses div.warning-message-container ul {
        list-style: none;
        margin-left: 0;
        padding-left: 15px;
        text-indent: -10px;
    }

    div.lorepo-push-to-courses div.warning-message-container li:before {
        content: "-";
        display: block;
        float: left;
        width: 10px;
    }

</style>