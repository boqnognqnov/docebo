<?php
/**
 * Created by PhpStorm.
 * User: NikVasilev
 * Date: 9.5.2016 г.
 * Time: 22:14
 */
$objId = Yii::app()->request->getParam('singleObjectId',false);
$objModel = LearningRepositoryObject::model()->findByPk($objId);
$versionsCount = LearningRepositoryObject::getObjectVersionsCount($objId);
$isAvailableForVersioning = in_array($objModel->object_type, LearningRepositoryObject::$objects_with_versioning ) && $versionsCount > 1;
?>
<br />
<div class="repo-object-version-top-panel">
    <?php
    if($isAvailableForVersioning) { ?>
    <div class="top-panel-left">

        <div class="select-version-label"><?= Yii::t('standard','Select version you want to push:'); ?></div>
    <?php
        if($objModel) {
            if($objModel->object_type == "video") {
                // Get dropdown with versions only with visible status 1 (for videos only)
                echo $objModel->getVisibleVersionsDropdown();
            } else {
                echo $objModel->getVersionsDropdown();
            }
            echo CHtml::hiddenField('selectedSingleObject','[{"objectId":"'.$objId.'", "versionId":""}]', array("id" => "selected-objects"));
        }
    ?>
    </div>
    <?php }else{
        if($objModel) {
            $version = $objModel->getLatestVersion();
            echo CHtml::hiddenField('selectedSingleObject', '[{"objectId":"' . $objId . '", "versionId":"' . $version['version'] . '"}]', array("id" => "selected-objects"));
        }
    }
    ?>

    <div class="version-details-panel top-panel-<?= ($isAvailableForVersioning)? 'right' : 'left' ?>">
        <div class="version-details"><?= ($isAvailableForVersioning) ? Yii::t('standard','Version details:') : (Yii::t('standard','_DETAILS').':') ?></div>
        <div class="version-used-in"><strong><?= Yii::t('standard','Used in:')?></strong> <span></span></div>
        <div class="version-uploaded-by"><strong><?= Yii::t('standard','Uploaded by:')?></strong> <span></span></div>
        <div class="version-description"></div>
    </div>
</div>
<div class="top-panel-select-courses-label">
    <?= Yii::t('standard','Select one or more courses:');?>
</div>

<script>
    $(function(){
        versionsInfoList = <?= CJSON::encode($objModel->getVersionsListData())?>;

        $(document).off('change','select.lorepo-push-lo-modal-version-select').on('change','select.lorepo-push-lo-modal-version-select', function(){
            var versionId = $(this).val();
            var versionObject = null;
            versionsInfoList.forEach(function(item, index){
                if(item.version == versionId){
                    versionObject = item;
                }
            });
            var selectedObjects = JSON.parse($('#selected-objects').val());
            selectedObjects.forEach(function(item, index){
                    selectedObjects[index].versionId = versionId;
            });
            $('#selected-objects').val(JSON.stringify(selectedObjects));
            $('.version-details-panel .version-used-in span').text(versionObject.usedIn + ' <?= Yii::t('standard', 'courses')?>');
            $('.version-details-panel .version-uploaded-by span').text(versionObject.author);
            $('.version-details-panel .version-description').html("");
            if (versionObject.version_description != null && versionObject.version_description.trim() != '') {
                $('.version-details-panel .version-description').html("<strong><?= Yii::t('standard','Description:')?></strong> <span>" + versionObject.version_description + "</span>");
            }
        });


        var versionId = ($('select.lorepo-push-lo-modal-version-select').length)? $('select.lorepo-push-lo-modal-version-select').val() : <?php echo CJSON::encode($version['version']) ?> ;
        var versionObject = null;
        versionsInfoList.forEach(function(item, index){
            if(item.version == versionId){
                versionObject = item;
            }
        });
        var selectedObjects = JSON.parse($('#selected-objects').val());
        selectedObjects.forEach(function(item, index){
            selectedObjects[index].versionId = versionId;
        });
        $('#selected-objects').val(JSON.stringify(selectedObjects));
        $('.version-details-panel .version-used-in span').text(versionObject.usedIn + ' <?= Yii::t('standard', 'courses')?>');
        $('.version-details-panel .version-uploaded-by span').text(versionObject.author);
        $('.version-details-panel .version-description').html("");
        if (versionObject.version_description != null && versionObject.version_description.trim() != '') {
            $('.version-details-panel .version-description').html("<strong><?= Yii::t('standard','Description:')?></strong> <span>" + versionObject.version_description + "</span>");
        }
    });
</script>