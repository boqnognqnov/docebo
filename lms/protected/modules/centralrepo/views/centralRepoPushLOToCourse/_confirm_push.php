<?php
    $this->widget('common.widgets.warningStrip.WarningStrip', array (
        'message'=> Yii::t('central_repo', "By pushing another version of this object into the following courses, all users in progress will have to restart from the beginning." ),
        'type'=>'warning'
    ));
?>

<?php
    echo CHtml::hiddenField('selectedSingleObject', $selectedSingleObject);
    echo CHtml::hiddenField('idGeneral', $idGeneral);
    echo CHtml::hiddenField('singleObjectId', $idGeneral);
    echo CHtml::hiddenField('selected_courses', $selected_courses);
    echo CHtml::hiddenField('force_change_courses', $forceChangeCourses);
    echo CHtml::hiddenField('potentially_affected', $potentially_affected);
?>

<div class="confirmTitleMessage"><?= Yii::t("central_repo", "Please confirm the courses for which you want to change the version:") ?></div>
<?php
    $columns = array(
        array(
            'class' => 'CCheckBoxColumn',
            'value' => '$data["idCourse"]',
        ),
        array(
            'type' => 'raw',
            'header' => Yii::t('standard', '_COURSE_CODE'),
            'value' => '$data["code"]',
        ),
        array(
            'type' => 'raw',
            'header' => Yii::t('standard', '_COURSE_NAME'),
            'value' => '$data["name"]',
        ),
        array(
            'type' => 'raw',
            'header' => Yii::t('standard', 'Version name'),
            'value' => '$data["version_name"]',
        ),
        array(
            'type' => 'raw',
            'header' => Yii::t('report', 'number_of_users_in_progress'),
            'value' => '$data["inProgress"]',
        ),
    );

    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => $data_grid_id,
        'selectableRows' => 3,
        'cssFile'=>false,
        'htmlOptions' => array('class' => 'grid-view clearfix'),
        'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
        'dataProvider' => $dataProvider,
        'template' => '{items}{summary}{pager}',
        'summaryText' => Yii::t('standard', '_TOTAL'),
        'pager' => array(
            'class' => 'DoceboCLinkPager',
            'maxButtonCount' => 8,
        ),
        'ajaxUpdate' => $data_grid_id,
        'beforeAjaxUpdate' => 'function(id, options) { options.data = $("div.lorepo-push-to-courses input").serialize(); }',
        'afterAjaxUpdate' => 'function(id, data){
                        $("#'.$data_grid_id.' input").styler();
                        $(document).controls();
                        updateConfirmSelectedCoursesAfterGridUpdate("'.$data_grid_id.'");
                     }',
        'columns' => $columns,
    ));
?>
<div class="confirmCheckboxDialog">
    <?php
        echo CHtml::checkBox("agree", false, array("id" => "agree", 'class' => 'jwizard-no-save jwizard-no-restore'));
        echo CHtml::label($agreeMessage, "agree");
    ?>
</div>

<style>
    div#push-to-course-dialog table tbody tr td:nth-child(3) {
        word-wrap: break-word;
        word-break: break-all;
    }

    div.selectLearningObjects {
        padding: 5px;
    }

    div#warning-strip {
        height: 42px !important;
        min-height: 42px;
        padding-top: 11px;
    }

    div.confirmTitleMessage{
        margin: 20px 0px;
    }

    div#warning-strip div.exclamation-mark {
        margin-top: 3px;
    }

    div.confirmCheckboxDialog span ,div.confirmCheckboxDialog label{
        float: left;
    }

    div.confirmCheckboxDialog span {
        margin-left: 13px;
    }
    div.confirmCheckboxDialog label {
        margin-left: 15px;
    }
</style>

<script type="text/javascript">
    $(function () {
        // Disable the Next Button
        var dialogContainer = $('div.modal.lorepo-push-to-courses');
        $('input,select', dialogContainer).styler();

        $(".next_button", dialogContainer).click(function(e) {
            if($(".next_button", dialogContainer).hasClass('disabled')) {
                e.preventDefault();
                return;
            }
        });

        $('#agree').change(function () {
            if ($(this).is(':checked')) {
                $('a.jwizard-nav.next_button', dialogContainer).removeClass('disabled');
            } else {
                $('a.jwizard-nav.next_button', dialogContainer).addClass('disabled');
            }
        }).trigger('change');

        // Collect the Selected Courses from this Step
        var context = '<?= $data_grid_id ?>';
        $(document).off('change', '#' + context + ' td.checkbox-column input').on('change', '#' + context + ' td.checkbox-column input', function(){
            var selectedObjects = JSON.parse($('#force_change_courses').val());
            // If the Checkbox is check, then add it to the selected objects
            if($(this).prop('checked')){
                selectedObjects.push($(this).val());
            }
            else{
                var selectedObjects = JSON.parse($('#force_change_courses').val());
                var key = $(this).val();
                selectedObjects.forEach(function(item, index){
                    if(item == key){
                        selectedObjects.splice(index,1);
                    }
                });
            }
            $('#force_change_courses').val(JSON.stringify(selectedObjects));
        });

    });
</script>