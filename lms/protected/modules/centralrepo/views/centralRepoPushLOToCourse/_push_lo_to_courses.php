<div id="lorepo-push-lo-to-course-modal">
    <div class="lorepo-push-modal-filter-header">
        <div class="filters-wrapper">
            <table class="filters filters_course_management">
                <tbody>
                <tr>
                    <td class="table-padding">
                        <table id="lorepo-push-lo-modal-filter-table" class="table-border">
                            <tr>
                                <td class="group">
                                    <table>
                                        <tr>
                                            <td><?= Yii::t('central_repo', 'Filter by type'); ?></td>
                                            <td class="object-type-select">
                                                <?= CHtml::dropDownList( "object_type", 0, $usedTypes, array('prompt' => Yii::t('central_repo', 'Select type'), 'id' => 'push-lo-to-courses-filter-by-type')); ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="group descendantsSwitch">
                                    <?php
                                        echo CHtml::hiddenField('selectedObjects','[]', array("id" => "selected-objects"));
                                        echo CHtml::hiddenField('folder_key', '', array("id" => "lo_folder_key"));
                                        if($skipCoursesStep)
                                            echo CHtml::hiddenField('skipCoursesStep',($skipCoursesStep)? true : false);
                                        if($courseId)
                                            echo CHtml::hiddenField('selected_courses',CJSON::encode(array((int)$courseId)));
                                    ?>

                                    <table>
                                        <tr>
                                            <td><?php echo Yii::t('standard', '_INHERIT'); ?></td>
                                            <td class="yes-no">
                                                <?= Yii::t('standard', '_YES'); ?>
                                                <?= CHtml::checkBox("pushContainChildren", false,  array("id" => "push-lo-to-courses-show-descendants")); ?>
                                                <?= Yii::t('standard', '_NO'); ?>
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                                <td class="group clearfix search">
                                    <table>
                                        <tr>
                                            <td>
                                                <div class="input-wrapper">
                                                    <input data-url="<?= Docebo::createAppUrl('admin:centralrepo/centralRepo/objectsAutocomplete') ?>" class="typeahead"
                                                           id="push-lo-to-courses-search-input" autocomplete="off" type="text"
                                                           name="LearningRepositoryObject[title]"
                                                           placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"/>
                                                    <span>
                                                        <button class="search-btn-icon"></button>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div id="lorepo-push-modal-leftside">
        <div id="push-lo-tree-wrapper" class="categories" data-source="<?= Docebo::createLmsUrl('centralrepo/centralRepoCategory/GetPushLoCategoryTree')?>"></div>
    </div>

    <div id="lorepo-push-modal-rightside">
        <div id="grid-wrapper">
            <?php $this->renderPartial('_losGrid', array(
                'repo' => $repo,
                'dataProvider' => $dataProvider,
                'data_grid_id' => $dataGridId,
                'filter_form_id' => 'lorepo-push-lo-modal-filter-table',
            )); ?>
        </div>
    </div>
</div>
<script>

//    applyTypeahead($("#lorepo-push-lo-to-course-modal .typeahead"));
    $('#lorepo-push-lo-to-course-modal input').styler();

    function resetSearchPlaceholder() {
        var $search = $('#repo-search-input');
        if ($search.val() == "<?= Yii::t('standard', '_SEARCH'); ?>")
            $('#repo-search-input').val('');
    }

    LORepoFTree = new LoRepoCategories();

    $(function(){
	    var folder = $('#player-arena-overall-folders').dynatree('getActiveNode');
	    if(folder && folder.data.isFolder && folder.data.key ){
		    $("#lo_folder_key").val(folder.data.key);
	    }

        LORepoFTree.initModal('#lorepo-push-lo-to-course-modal');
        LORepoFTree.loadPushLoTree();

        $("#lorepo-push-lo-to-course-modal .typeahead").each(function() {
            var th = $(this);
            var convertList = new Object();
            th.typeahead({
                source: function (query, process) {
                    var input = th;
                    var icon = input.closest('.input-wrapper').find('.search-icon');

                    var data = input.data();
                    delete data.typeahead;

                    data.query = query;

                    input.addClass('loading');
                    icon.hide();

                    return $.post(data.url, { data: data }, function (data) {
                        input.removeClass('loading');
                        icon.show();

                        if (th.attr('data-source-desc')) {
                            // If the typeahead input receives AJAX results in the
                            // format: [username]=>[username - firstname lastname]
                            var x = 0;
                            var result = [];
                            convertList = new Object();
                            $.each(data.options, function(index, value) {
                                convertList[value] = index;
                                result[x] = value;
                                x++;
                            });
                            data.options = result;

                        }else{
                            // If the typeahead input receives AJAX results in the
                            // format: [index/int]=>[username - firstname lastname]
                            var x = 0;
                            var result = [];
                            $.each(data.options, function(index, value) {
                                result[x] = value;
                                x++;
                            });
                            data.options = result;
                        }

                        return process(data.options);
                    });
                },
                updater: function(title) {
                    if (th.attr('data-source-desc') && (title in convertList)) {
                        title = convertList[title];
                    }
                    th.val(title);

                    $('div[id*="lorepo-push-lo-modal-objects-grid"]').yiiGridView('update');

                    return title;
                },
                matcher: function(title) {
                    return true;
                },
            });
        });

        $("#lorepo-push-lo-to-course-modal .typeahead").off('keydown').on('keydown', function(e){

            // If the user hit enter
            if(e.keyCode == 13) {

                event.stopImmediatePropagation();
                event.stopPropagation();
                event.preventDefault();

                // Update the Grid
                $('div[id*="lorepo-push-lo-modal-objects-grid"]').yiiGridView('update');

                return false;
            }


        });
    });

</script>

