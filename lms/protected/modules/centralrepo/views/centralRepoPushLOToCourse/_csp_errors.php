<div class="finished-step-icon" >
	<i class="fa fa-warning fa-3x" style="color: #e67e22;"></i>
</div>

<div class="csp-errors-list" >
	<span>
		<?= Yii::t('marketplace', '{{objects}} training materials could not be pushed to course because they come from a C.S.P. vendor catalog', array(
			'{{objects}}' => '<strong>'.count($cspErrors).'/'.$totalImports.'</strong>'
		)).':' ?>
	</span>
	<br/>
	<ul>
		<?php
		foreach ($cspErrors as $cspError) {
			$_course = LearningCourse::model()->findByPk($cspError['id_course']);
			$_object = LearningRepositoryObject::model()->findByPk($cspError['id_object']);
			//TODO: provide a good graphic format for LOs list
			echo '<li>';
			echo htmlentities($_object->title); //title may contain special characters
			echo '</li>';
		}
		?>
	</ul>
</div>

<style>
	div.lorepo-push-to-courses {
		width: 600px !important;
		margin-left: -300px !important;
		top: 20% !important;
	}

	div.lorepo-push-to-courses div.modal-footer, div.lorepo-push-to-courses .form-actions {
		display: none;
	}

	div.csp-errors-list {
		text-align: left;
		margin: 20px 12px;
	}
	div.csp-errors-list ul {
		list-style: none;
		margin-left: 0;
		padding-left: 15px;
		text-indent: -10px;
	}

	div.csp-errors-list li:before {
		content: "-";
		display: block;
		float: left;
		width: 10px;
	}
</style>