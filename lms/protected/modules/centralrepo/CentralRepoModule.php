<?php

class CentralRepoModule extends CWebModule
{

    private $_assetsUrl;
    private $_playerAssetsUrl;
    private $_adminAssetsUrl;

    public function init()
    {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application

        // import the module-level models and components
        $this->setImport(array(
            'centralrepo.models.*',
            'centralrepo.components.*',
            'centralrepo.widgets.*',
            'centralrepo.controllers.*'
        ));

        Yii::app()->event->on(EventManager::EVENT_USER_SUBSCRIBED_COURSE, array($this, 'onUserSubscribedCourse'));
    }

    /**
     * Register resources needed only in central repo
     *
     * @param CController $controller
     * @param CAction $action
     * @return bool
     */
    public function beforeControllerAction($controller, $action)
    {
        if(parent::beforeControllerAction($controller, $action)) {
            $this->registerResources();
            return true;
        }
        else
            return false;
    }

    /**
     * Handler that creates slave commontracks for the newly enrolled user
     *
     * @param DEvent $event
     */
    public function onUserSubscribedCourse(DEvent $event) {
        $course = $event->params['course']; /* @var $course LearningCourse */
        $user = $event->params['user']; /* @var $user CoreUser */

        // Loop over central LO objects with shared tracking inside this course
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.id_object IS NOT NULL");
        $criteria->addCondition("t.idCourse = :idCourse");
        $criteria->addCondition("repositoryObject.shared_tracking = 1");
        $criteria->params[':idCourse'] = $course->idCourse;
        $centralLos = LearningOrganization::model()->with('repositoryObject')->findAll($criteria);
        foreach($centralLos as $lo) { /* @var $lo LearningOrganization */
            // Search for a master track for this LO/user
            $criteria = new CDbCriteria();
            $criteria->addCondition('(idUser = :idUser) AND (idResource = :idResource) AND (objectType = :objectType)');
            $criteria->params = array(
                ':idUser' => $user->idst,
                ':idResource' => $lo->idResource,
                ':objectType' => $lo->objectType
            );
            $criteria->addCondition('idMasterOrg IS NULL');
            $track = LearningCommontrack::model()->find($criteria);
            if($track) {
                try {
                    // We may already have a slave track from a previous enrollment
                    $slaveTrack = LearningCommontrack::model()->findByPk(array(
                        'objectType' => $lo->objectType,
                        'idReference' => $lo->idOrg,
                        'idTrack' => $track->idTrack
                    ));

                    if(!$slaveTrack) {
                        $slaveTrack = new LearningCommontrack();
                        $slaveTrack->idReference = $lo->idOrg;
                        $slaveTrack->idUser = $user->idst;
                        $slaveTrack->idTrack = $track->idTrack;
                        $slaveTrack->objectType = $lo->objectType;
                        $slaveTrack->idMasterOrg = $track->idReference;
                        $slaveTrack->idResource = $track->idResource;
                    }

                    $slaveTrack->dateAttempt = $track->dateAttempt;
                    $slaveTrack->status = $track->status;
                    $slaveTrack->firstAttempt = $track->firstAttempt;
                    $slaveTrack->first_complete = $track->first_complete;
                    $slaveTrack->last_complete = $track->last_complete;
                    $slaveTrack->first_score = $track->first_score;
                    $slaveTrack->score = $track->score;
                    $slaveTrack->score_max = $track->score_max;
                    $slaveTrack->total_time = $track->total_time;
					$slaveTrack->setScenario(LearningCommontrack::SCENARIO_USER_SUBSCRIBED_COURSE);
                    $slaveTrack->save();

                } catch (Exception $e) {
                    Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
                }
            }
        }
		// Update 'score_given' in learning_courseuser
		LearningCourseuser::recalculateLastScoreByUserAndCourse($course->idCourse, $user->idst);
		// Update initial_score_given in learning_courseuser
		LearningCourseuser::recalculateInitialScoreGiven($course->idCourse, $user->idst);
    }

    public function registerResources($registerFancyTree = false) {
        if (!Yii::app()->request->isAjaxRequest) {
            JsTrans::addCategories('centralrepo');
            $cs = Yii::app()->getClientScript();
            $cs->registerCoreScript('jquery.ui');
            $cs->registerCssFile ($this->getAssetsUrl()   . '/css/central_repo.css');
            $cs->registerScriptFile ($this->getAssetsUrl()   . '/js/centralRepo.js');
            $cs->registerScriptFile ($this->getAssetsUrl()   . '/js/centralRepoPreview.js');

            $adminAssetsUrl = $this->getAdminAssetsUrl();
            $cs->registerScriptFile($adminAssetsUrl . '/admin.js');
            $cs->registerScriptFile($adminAssetsUrl . '/script.js');
            $cs->registerScriptFile($adminAssetsUrl . '/scriptModal.js');
            $cs->registerScriptFile($adminAssetsUrl . '/jquery.cookie.js');

            // jscrollpane
            $cs->registerCssFile(	Yii::app()->theme->baseUrl . '/js/jscrollpane/jquery.jscrollpane.css');
            $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jscrollpane/jquery.jscrollpane.js');

            // Load the Slides Converter HERE
            // Owl Carousel js
            $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/owl/owl.carousel.min.js');
            
            // Authoring Assets
            $authoringCssUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias("root.authoring.css"));
            $authoringJsUrl  = Yii::app()->assetManager->publish(Yii::getPathOfAlias("root.authoring.js"));
            $cs->registerScriptFile($authoringJsUrl . '/owlCarousel.js');
            $cs->registerCssFile(	$authoringCssUrl . '/owl.carousel.css');

            // Add the Player Assets
            // Core Yii
            $playerAssetsUrl = $this->getPlayerAssetsUrl();
            $cs->registerCoreScript('jquery');
            $cs->registerCoreScript('jquery.ui');
            $cs->registerScriptFile($playerAssetsUrl . '/js/jquery.dynatree.js');
            $cs->registerCssFile($playerAssetsUrl    . '/css/ui.dynatree-modified.css');

            if($registerFancyTree){
                //Register FancyTree assets
                UsersSelector::registerClientScripts();
                // Register path alias
                if (Yii::getPathOfAlias('fancytree') === false) {
                    Yii::setPathOfAlias('fancytree', realpath(dirname(__FILE__)));
                }

                $cs = Yii::app()->getClientScript();
                $assetsPath = Yii::getPathOfAlias('common.extensions.fancytree');
                $baseUrl = Yii::app()->getAssetManager()->publish($assetsPath);

                // jQuery UI is Required
                $cs->registerPackage('jquery.ui');

                // Register Fancytree JS/CSS
                $cs->registerScriptFile($baseUrl . '/jquery.fancytree-all.js', CClientScript::POS_HEAD);
                $cs->registerScriptFile($baseUrl . '/jquery.fancytree.docebo.js', CClientScript::POS_HEAD);
                $cs->registerCssFile(   $baseUrl . '/skin-docebo/ui.fancytree.css');

            }
        }
    }


    /**
     * Get module assets url path
     * @return mixed
     */
    public function getAssetsUrl()
    {
        if ($this->_assetsUrl === null) {
            $this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('centralrepo.assets'));
        }
        return $this->_assetsUrl;
    }

    /**
     * Get PLAYER's assets url path
     * We have plenty of CSS/JS code in Player module's assets already, which we do share.
     *
     * @return mixed
     */
    public function getPlayerAssetsUrl()
    {
        if ($this->_playerAssetsUrl === null) {
            $this->_playerAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('player.assets'));
        }
        return $this->_playerAssetsUrl;
    }
    
    /**
     * Get ADMIN.js assets URL
     *
     * @return mixed
     */
    public function getAdminAssetsUrl()
    {
    	if ($this->_adminAssetsUrl === null) {
    		$this->_adminAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin.js'));
    	}
    	return $this->_adminAssetsUrl;
    }
    
    
}