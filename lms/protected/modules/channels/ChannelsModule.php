<?php

/**
 * Class ChannelsModule
 * Main channels dashboard module
 */
class ChannelsModule extends CWebModule
{
	/**
	 * @var Module assets URL
	 */
	private $_assetsUrl;

	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('channels.assets'));
		}
		return $this->_assetsUrl;
	}

	/**
	 * Module initialization
	 */
	public function init() {
		// import the module-level models and components
		$this->setImport(array(
			'channels.widgets.*'
		));
		Yii::app()->event->on('CollectMyDashboardDashletWidgets', array($this, 'provideDashletDescriptor'));
	}

	/**
	 * Respond to the dashboard dashlet collector
	 * @param DEvent $event
	 */
	public function provideDashletDescriptor(DEvent $event){
		if(!isset($event->params['descriptors']))
			return;

		// Collect already installed descriptors
		$collectedSoFarHandlers = array();
		foreach($event->params['descriptors'] as $descriptorObj) {
			if ($descriptorObj instanceof DashletDescriptor)
				$collectedSoFarHandlers[] = $descriptorObj->handler;
		}

		// Add our new descriptors, checking for duplicates
		$descriptors = array(DashletChannelsList::descriptor(), DashletSingleChannel::descriptor());
		foreach($descriptors as $descriptor) {
			if (!in_array($descriptor->handler, $collectedSoFarHandlers))
				$event->params['descriptors'][] = $descriptor;
		}
	}

	/**
	 * The right place to inject the needed JS
	 * @param CController $controller
	 * @param CAction $action
	 * @return bool
	 */
	public function beforeControllerAction($controller, $action) {
		if(parent::beforeControllerAction($controller, $action)) {
			$this->registerResources();
			return true;
		}
		else
			return false;
	}

	/**
	 * Register resources
	 */
	private function registerResources() {
		if (!Yii::app()->request->isAjaxRequest) {
			$cs = Yii::app()->getClientScript();
			$themeUrl = Yii::app()->theme->baseUrl;
			$cs->registerCssFile($this->assetsUrl."/css/swiper.min.css");
			$cs->registerCssFile($this->assetsUrl."/css/style.css");
			$cs->registerScriptFile($themeUrl . '/js/require_js/require.js', CClientScript::POS_END);
			$cs->registerScript('channelsPath', 'channelsScriptPath = "'.str_replace("../", "/", $this->assetsUrl).'"', CClientScript::POS_HEAD);
			//$cs->registerScriptFile($this->assetsUrl . '/js/main.js', CClientScript::POS_END);
			$cs->registerScript('channelsBootstrap', "require([channelsScriptPath+'/js/main.js'], function(){ require([
	'angular',
	'angularRoute',
	'angularSanitize',
	'angularClipboard',
	'infiniteScroll',
	'controllers',
	'services',
	'directives',
	'app'
], function () {
	angular.element(document).ready(function () {
		angular.bootstrap(document.body, ['Channels']);
	});
});});", CClientScript::POS_END);
			$token = Yii::app()->request->csrfToken;
			$cs->registerScript('csrfTokenAngular', 'window.ngCsrfToken = "'.$token.'";');
			$cs->registerScript('share7020app', 'window.share7020app = '.(PluginManager::isPluginActive('Share7020App') ? 'true' : 'false').';');
			$cs->registerScript('user_id', 'window.$user_id = '.Yii::app()->user->id.';');
			$cs->registerScript('app7020AssetPath', 'app7020AssetPath = "'.App7020Helpers::getAssetsUrl().'/";', CClientScript::POS_HEAD);
		}
	}
}
