<!-- Channel main wrapper  -->

<div class="channels-app widget-channels-functions" ng-cloak id="appwidget_<?php echo $this->dashletModel->id; ?>" data-channels="2">
	<div ng-controller="DashboardController" ng-init="init();" class="cell_<?php echo $this->dashletModel->dashboardCell->width; ?>">
		<div class="channels-wrapper">
			<div class="channel-wrap c_two col-md-12" ng-class="{'last-wrap': $last}" ng-repeat="channel in channels track by $index">
				<div class="header-channel col col-12">
					<div class="col col-10-main">
						<div class="col col-2 title-label">
							<div ng-if="channel.fa_icon">
								<span class="label-icon" style="color:{{channel.fa_icon_color}};background-color: {{channel.icon_bg_color}}"><i class="fa {{channel.fa_icon}}"></i></span>
							</div>
						</div>
						<div class="col col-10 title-details">
							<h3 class="view-more" data-id="{{channel.id}}" data-dashlet="<?php echo $this->dashletModel->id; ?>" ng-click="reloadView($event)">
								{{channel.name | cut:true:35:' ...'}}
								<i class="fa fa-angle-right"></i>
							</h3>
							<div ng-bind-html="channel.description | cut:true:75:' ...'"></div>
						</div>
					</div>
					<div class="col col-2 right-align">
						<div class="experts" ng-if="channel.other_fields.count_experts" modal  data-title="<?= Yii::t('standard','View experts') ?>" id="modal_{{channel.id}}" data-url="index.php?r=channels/index/channelExperts&id={{channel.id}}"><i class="fa fa-users"></i> {{channel.other_fields.count_experts}} <?= Yii::t('standard','experts') ?></div>
					</div>
				</div>
				<div ng-if="channel.items" ng-mouseenter="show = true" ng-mouseleave="show = false">
					<carousel data-id="channel-{{channel.id}}" data="{{channel.items}}" class="tiles-wrap swiper-container"></carousel>
				</div>
			</div>

			<!-- Clearfix -->
			<div class="clear-both"></div>
		</div>

			<!-- Lazyload -->
			<div class="channel-wrap placeholder-wrap c_two col-md-12" ng-show="showLazyLoad">
				<div class="header-channel col col-12">
					<div class="col col-10-main">
						<div class="col col-2 title-label">
							<span class="label-icon"></span>
						</div>
						<div class="col col-10 title-details">
							<h3></h3>
							<p></p>
						</div>
					</div>
				</div>
				<div class="tiles-wrap swiper-container">
					<div class="swiper-wrapper">
						<div class="swiper-slide" ng-repeat="i in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]">
							<div class="col col-tile">
								<div class="tile-thumb">
									<div class="thumb-img"><i class="fa fa-spinner"></i></div>
								</div>
								<div class="tile-meta">
									<div class="main-metas">
										<span></span>
									</div>
									<a href='#' class="tile-title"></a>
									<div class="author-metas">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Clearfix -->
			<div class="clear-both"></div>
			<div class="load-more" ng-show="showLoadmore" ng-click="infiniteScrollVertical();"><?= Yii::t('standard','Load More') ?></div>
	</div>
</div>


<script>
	require([mainWidget+'/js/mainWidget.js'], function(){
		require([
			'angular',
			'angularRoute',
			"angularSanitize",
			"angularClipboard",
			'infiniteScroll',
			'controllersWidget',
			'services',
			'directives',
			'app',
		], function () {
			angular.bootstrap(angular.element('#appwidget_<?php echo $this->dashletModel->id; ?>'), ['Channels']);
		});
	});
</script>