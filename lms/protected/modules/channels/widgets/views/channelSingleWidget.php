<!-- Channel view all items -->

<?php

//Set the channel id
	if($this->getParam('selected_channel', null) != ''){
		$channel = $this->getParam('selected_channel', null);
		$chunk = $this->getParam('display_number_items', 10);
	}else{
		$channel = $_POST['channel'];
		$chunk = '10';
	}

?>
<div class="single-channel-app widget-channels-functions" id="appwidget_<?php echo $this->dashletModel->id; ?>" ng-cloak>
	<div ng-controller="ChannelSingleWidgetController" class="cell_<?php echo $this->dashletModel->dashboardCell->width; ?>" data-channel="<?php echo $channel; ?>" data-chunk="<?php echo $chunk; ?>">
		<?php if($_POST['channel'] != ''){ ?>
			<div class="back-arrow"><i class="fa fa-angle-left" data-dashlet="<?php echo $this->dashletModel->id; ?>" ng-click="reloadViewChannel($event)"></i></div>
		<?php } ?>
		<div class="channels-wrapper viewall-page">
			<!-- Channel header -->
			<div class="header-channel col col-12">
				<div class="col col-10-main">
					<div class="col col-2 title-label">
						<div ng-if="channel.fa_icon">
							<span class="label-icon" style="color:{{channel.fa_icon_color}};background-color: {{channel.icon_bg_color}}"><i class="fa {{channel.fa_icon}}"></i></span>
						</div>
					</div>
					<div class="col col-10 title-details">
						<h3>{{channel.name}}</h3>
					</div>
					<div class="col col-10 description-details">
						<p class="desc"  dd-text-collapse dd-text-collapse-max-length="200" dd-text-collapse-text="{{channel.description}}"></p>
					</div>
					<div class="col col-12 channel-details">
						<div class="number-items"><span class="blue-color">{{channel.items_count}}</span> <span class="ïtems-text" ng-show="channel.items_count"><?php echo Yii::t('standard', 'Items') ?></span></div>
						<div class="col details-col2" ng-if="channel.other_fields.count_experts">
							<span class="blue-color">{{channel.other_fields.count_experts}}</span>
							<span ng-if="channel.other_fields.count_experts"><?php echo Yii::t('standard', 'Experts') ?></span>
							<a href="" class="experts view-expert"  modal  data-title="<?php echo Yii::t('standard', 'View assigned experts') ?>" id="modal_{{channel.id}}" data-url="index.php?r=channels/index/channelExperts&id={{channel.id}}"><?php echo Yii::t('standard', 'View assigned experts') ?></a>
						</div>

					</div>
				</div>
			</div>
			<!-- Channel items wrapper -->
			<div class="items-wrapper">
				<div class="col col-tile" ng-class="{'first-child' : $first}" ng-repeat="(key,item) in channel.items track by $index" ng-mouseenter="showDots = true" ng-mouseleave="showDots = false">
					<!-- Item top overflay details and image -->
					<div class="tile-thumb">
						<div class="thumb-img" style="background: url('{{item.image_uri}}') no-repeat; background-size: cover;  background-position: center;"><a target="_parent" ng-href="{{item.can_enter || item.type_id === 'knowledge_asset' ? item.uri:''}}"></a></div>
						<div class="overlay-details">
							<a target="_parent" ng-href="{{item.can_enter || item.type_id === 'knowledge_asset' ? item.uri:''}}" class="label-type">{{item.type_label}}</a>
							<div class="label-right-topcorner">
								<a ng-href="{{item.can_enter || item.type_id === 'knowledge_asset' ? item.uri:''}}">
									<div class="courses-count" ng-if="item.other_fields.courses_count">{{item.other_fields.courses_count}} <?php echo Yii::t('standard', 'Courses') ?></div>
									<div class="label-new" ng-if="item.is_new"><?php echo Yii::t('standard', 'New') ?></div>
									<div class="label-duration" ng-if="item.other_fields.duration && item.other_fields.duration != '00:00'">{{item.other_fields.duration}}</div>
								</a>
							</div>
							<div class="label-invititedby" ng-if="item.other_fields.invited_by">
								<a target="_parent"  ng-href="{{item.can_enter || item.type_id === 'knowledge_asset' ? item.uri:''}} class="invite-image" data-toggle="tooltip" data-placement="top" title="<?php echo Yii::t('standard', 'Invited by:') ?> {{item.other_fields.invited_by}}" data-original-title="<?php echo Yii::t('standard', 'Invited by:') ?> {{item.other_fields.invited_by}}" rel="tooltip" >
									<span ng-if="item.other_fields.invited_by_url"><img ng-src="{{item.other_fields.invited_by_url}}" alt="{{item.other_fields.invited_by}}" /></span>
								</a>
							</div>
							<div class="label-left-topcorner" ng-if="item.ribbon.fa_icon" ng-class="tooltip-{{$index}}">
								<a class="label-left-corner" data-toggle="tooltip" data-placement="top" title="{{item.ribbon.message}}" data-original-title="{{item.ribbon.message}}" rel="tooltip" ng-mouseenter="tooltipPosition($event)" style="color: {{item.ribbon.text_color}}; border-color: {{item.ribbon.icon_bg_color}} transparent transparent transparent;">
									<i class="fa {{item.ribbon.fa_icon}}"></i>
								</a>
							</div>
						</div>
					</div>

					<!-- Item meta details -->
					<div class="tile-meta">
						<div class="main-metas">
							<span ng-if="item.other_fields.views">{{item.other_fields.views}} <?= Yii::t('standard', 'views') ?></span>
							<span ng-if="item.other_fields.last_viewed">{{item.other_fields.last_viewed}}</span>
							<span ng-if="item.other_fields.language">{{item.other_fields.language}}</span>
							<span ng-if="item.other_fields.level">{{item.other_fields.level}}</span>
						</div>
						<a target="_parent" ng-href="{{item.can_enter || item.type_id === 'knowledge_asset' ? item.uri:''}}" class="tile-title">{{item.title| cut:true:55:' ...'}}</a>
						<div class="course-status" ng-class="{0:'Subscribed', 1:'progress', 2:'Completed'}[item.other_fields.status_code]" ng-if="item.other_fields.status && !item.other_fields.author">
							{{item.other_fields.status}}
						</div>
						<div class="progress-metas" ng-if="item.other_fields.percentage">
							<span class="progress-percent">{{item.other_fields.percentage}}%</span>
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="{{item.other_fields.percentage}}" aria-valuemin="0" aria-valuemax="100" style="width: {{item.other_fields.percentage}}%">

								</div>
							</div>
						</div>
						<div class="author-metas" ng-if="item.other_fields.author">
							<span><?= Yii::t('standard', 'by') ?></span>
							<a href="{{item.other_fields.personal_channel}}">{{item.other_fields.author}}</a>
						</div>

						<!-- Actions menu -->
						<span class="dots-link" ng-if="item.actions.length > 0" ng-show="showDots" ng-click="open($event)">...</span>
						<div class="clear-both"></div>
						<div class="dots-menu" ng-if="item.actions.length > 0">
							<div class="bg-overlay"></div>
							<div class="link-overlay">
								<div class="dots-menu-content">
									<ul>
										<li ng-repeat="action in item.actions">
											<div ng-if="action.action_type == 'new_window'">
												<a href="{{action.url}}" target="_blank" style="color: {{action.text_color}};">
													<span class="remove-link"><i class="fa {{action.fa_icon}}"></i></i> {{action.label}}</span>
												</a>
											</div>
											<div ng-if="action.action_type == 'same_window'">
												<a href="{{action.url}}" style="color: {{action.text_color}};">
													<span class="remove-link"><i class="fa {{action.fa_icon}}"></i></i> {{action.label}}</span>
												</a>
											</div>
											<div ng-if="action.action_type == 'modal'">
												<span modal ng-if="action.action_type == 'modal'" ng-class=""  data-classes=" {{action.class}}" data-custom_id="{{action.id}}"
													  data-title="{{action.label}}"  data-url="{{action.action_params.url}}" style="color: {{action.text_color}}">
													<i class="fa {{action.fa_icon}}"></i> {{action.label}}
												</span>
											</div>
											<div ng-if="action.action_type == 'deeplink'">
												<a href="{{action.action_params.url}}" class="copy-url"></a>
												<span clipboard class="not-copied" text="textToCopy" on-copied="success()" on-error="fail(err)" ng-click="getShareurl($event)"><i class="fa {{action.fa_icon}}"></i></i> {{action.label}}</span>
												<span class="copied-success"><i class="fa fa-check color-green"></i></i> <span class="color-green font-weight-medium"><?= Yii::t('standard', 'Link copied') ?></span> <?= Yii::t('standard', 'to clipboard!') ?></span>
											</div>
											<div ng-if="action.action_type == 'pop'">
												<span ng-class="" ng-click="setPopStatus(key, 1);dismiss(key)"  data-classes=" {{action.class}}" data-custom_id="{{action.id}}"
													  data-title="{{action.label}}"  data-url="{{action.action_params.url}}" style="color: {{action.text_color}}">
													<i class="fa {{action.fa_icon}}"></i> {{action.label}}
												</span>
											</div>
										</li>

									</ul>
									<div class="bottom-part">
										<span ng-click="close($event)">...</span>
									</div>
								</div>
							</div>
						</div>

					</div>


				</div>
				<!-- Clearfix -->
				<div class="clear-both"></div>
				<div class="channel-wrap placeholder-wrap c_two col-md-12" ng-show="showLazyLoad">
					<div class="col placeholder-wrap" ng-repeat="i in [1, 2, 3, 4, 5, 6]">
						<div class="col col-tile">
							<div class="tile-thumb">
								<div class="thumb-img"><i class="fa fa-spinner"></i></div>
							</div>
							<div class="tile-meta">
								<div class="main-metas">
									<span></span>
								</div>
								<a href='#' class="tile-title"></a>
								<div class="author-metas">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Clearfix -->
			<div class="clear-both"></div>
			<div class="load-more" ng-show="showLoadmore" ng-click="infiniteScrollVertical()"><?php echo Yii::t('standard','Load More') ?></div>
		</div>
	</div>
</div>

<script>
	require([mainWidget+'/js/mainWidget.js'], function(){
		require([
			'angular',
			'angularRoute',
			"angularSanitize",
			"angularClipboard",
			'infiniteScroll',
			'controllersWidget',
			'services',
			'directives',
			'app',
		], function () {
			angular.bootstrap(angular.element('#appwidget_<?php echo $this->dashletModel->id; ?>'), ['Channels']);
		});
	});
</script>