<?php
/* @var $this DashletSingleChannel */
?>
<div class="row-fluid">
	<div class="span12">
		<div class="control-group">
			<label class="control-label"><?= Yii::t('app7020','Channel') ?></label>
			<div class="controls">
				<?php $this->widget('SingleSelector', array(
					'inputName' => 'selected_channel',
					'selectedChannel' => $this->getParam('selected_channel'),
					'style' => 'width: 100%;')) ?>
			</div>
		</div>
	</div>
</div>
<br/>
<p><strong><?=Yii::t('standard', '_SHOW')?></strong></p>
<hr>
<div class="row-fluid" style="margin-top: 10px;">
	<div class="span12">
		<?php
			echo Yii::t('dashboard', 'Display') . "&nbsp;&nbsp;";
			echo CHtml::textField('display_number_items', $this->getParam('display_number_items', 25), array('class' => 'span3'));
			echo '&nbsp;&nbsp;' . Yii::t('standard', 'items');
		?>
		<br>
		<span class="setting-description compact">&nbsp;<?= Yii::t('dashboard', 'Leave blank to display all items') ?></span>
	</div>
</div>