<select id="channels_selector" name="<?=$className?>[channels]">
	<?php if(!empty($channelIds)): ?>
		<?php foreach($channelIds as $channelId => $channel) : ?>
			<option value="<?= $channelId ?>" class="selected"><?= $channel['name'] ?></option>
		<?php endforeach; ?>
	<?php endif; ?>
</select>

<script type="text/javascript">
	$(document).ready(function() {
		$("#channels_selector").fcbkcomplete({
			json_url: "./index.php?r=channels/index/autocomplete",
			addontab: true,
			input_min_size: 0,
			width: '98.5%',
			cache: true,
			complete_text: '',
			filter_selected: true,
			maxitems: 999999999999999
		});
	});
</script>