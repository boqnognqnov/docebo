<?php
/* @var $inputName string */
/* @var $selectedChannel integer */
/* @var $channels array */
/* @var $style string */
?>
<?= CHtml::dropDownList($inputName, $selectedChannel, $channels, array('style' => $style)); ?>
