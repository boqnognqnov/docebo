<?php

class DashletChannelsList extends DashletWidget implements IDashletWidget{

	protected static $handler	= 'channels.widgets.DashletChannelsList';

	/**
	 * @see DashletWidget::init()
	 */
	public function init() {
		parent::init();
	}

	/**
	 * Dashlet descriptor
	 *
	 * @return DashletDescriptor Object, describing the dashlet to the outside world
	 */
	public static function descriptor( $params = FALSE ) {
		$sampleImageUrl = Yii::app()->getModule("channels")->getAssetsUrl() . "/images/preview_channel_list.jpg";

		$descriptor = new DashletDescriptor();
		$descriptor->name 					= 'core_dashlet_channels_list';
		$descriptor->handler				= self::$handler;
		$descriptor->title					= Yii::t('standard', 'Channels list');
		$descriptor->description			= Yii::t('dashlets', 'Displays a list of channels with courses and assets');
		$descriptor->sampleImageUrl 		= $sampleImageUrl;
		$descriptor->settingsFieldNames		= array();

		return $descriptor;
	}

	/**
	 * Render dashlet settings, used in Admin Dashlets UI
	 */
	public function renderSettings() {

	}

	/**
	 * Render fronend content.
	 *
	 * NOTE: Don't use Yii's CClientScript to register js/css resources
	 * here since it doesn't work. Use echo '<script src=...</script>' instead
	 */
	public function renderFrontEnd() {
		$channelId	= $_POST['channel'];
		if($channelId != ''){
			$this->render('channelSingleWidget');
		}else{
			$this->render('channelsWidget');
		}
	}

	/**
	 * Run arbitrary code just before the main page (dashboard) is going to be loaded (rendered) IF the dashlet takes part of it
	 *
	 * <strong>Note 1</strong>: this method is called BEFORE rendering of the main page, as an attribute of an instance created BEFORE rendering!
	 * Which means, DURING the rendering of the dashlet, another object instance is created/used, having no relation to the first instance
	 * The first intance is purely created (and dismissed) for the porpose of bootstraping to allow, mainly, preloading client (browser) resources
	 *
	 * <strong>Note 2</strong>: Be aware, if you have 2 dashlets of the same type in the dashboard, bootstrap() will be called 2 times!!!
	 *
	 * <strong>Hint</strong>: Use this to execute Yii::app()->getClientScript()->registerXXXXX(<url>) to load dashlet specific own javascript/css
	 *
	 * @param boolean|array $params
	 */
	public function bootstrap( $params = FALSE ) {
		$cs = Yii::app()->getClientScript();
		$am = Yii::app()->assetManager;
		
		$cs->registerScriptFile('/themes/spt/js/require_js/require.js',CClientScript::POS_END);
		App7020Helpers::registerApp7020Options();
		$assetsPath = $am->publish(Yii::getPathOfAlias('channels.assets'));
		
		$token = Yii::app()->request->csrfToken;
		$cs->registerScript('csrfTokenAngular', 'window.ngCsrfToken = "'.$token.'";');
		$cs->registerScript('user_id', 'window.$user_id = '.Yii::app()->user->id.';');
		$cs->registerScript('app7020AssetPath', 'app7020AssetPath = "'.App7020Helpers::getAssetsUrl().'/";', CClientScript::POS_HEAD);
		$cs->registerScript('themesUrl', 'themesUrl = "'.Yii::app()->theme->baseUrl.'";', CClientScript::POS_HEAD);

	//	$cs->registerScriptFile($assetsPath.'/js/mainWidget.js',CClientScript::POS_READY);
		//var_dump("Gloariq");
		//var_dump(Yii::app()->getModule('channels')->getAssetsUrl());
		$cs->registerScript('mainWidgetPath', 'mainWidget = "'.str_replace("../", "/", Yii::app()->getModule('channels')->getAssetsUrl()).'"', CClientScript::POS_HEAD);
		$cs->registerCssFile($assetsPath.'/css/swiper.min.css');
		$cs->registerCssFile($assetsPath.'/css/style.css');
		$cs->registerCssFile($assetsPath.'/css/styleWidget.css');
	
	}
}