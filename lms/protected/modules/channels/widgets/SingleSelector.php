<?php

/**
 * Channels selector widget to be used inside forms.
 * It renders a dropdown of channels
 *
 */
class SingleSelector extends CWidget {

	/**
	 * @var CActiveForm The form object where the HTML fields will be added
	 */
	public $form;

	/**
	 * @var string the input field name for the channels dropdown
	 */
	public $inputName;

	/**
	 * @var string CSS style to apply
	 */
	public $style;

	/**
	 * @var integer The selected channel ID
	 */
	public $selectedChannel;

	/**
	 * Initialize some variables
	 */
	public function init() {
		parent::init();

		if (!$this->inputName)
			$this->inputName = 'channel';
	}

	/**
	 * Render widget content
	 * @throws CException
	 */
	public function run() {
		$custom_Select = App7020Channels::customProviderSelect();
		$custom_channel_visibility_select = array(
			"idUser" => Yii::app()->user->idst,
			"ignoreVisallChannels" => false,
			"ignoreGodAdminEffect" => !Yii::app()->user->getIsGodadmin(),
			'appendEnabledEffect' => true,
			'ignoreSystemChannels' => true,
			'customSelect' => $custom_Select,
			'return' => 'provider'
		);
		 
		$dataProvider = App7020Channels::extractUserChannels($custom_channel_visibility_select, false);
		
		$channels = array();
		foreach ($dataProvider->data as $data) {
			$channels[$data['idChannel']] = $data['channelName'];
		}
 		// Render the selector view
		$this->render('single', array(
			'channels' => $channels,
			'selectedChannel' => $this->selectedChannel,
			'inputName' => $this->inputName,
			'style' => $this->style
		));
	}

}
