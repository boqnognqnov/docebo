<?php
/**
 * Items selector widget for courses and learning plans
 */
class Selector extends CWidget
{
	/**
	 * @var string The type of asset (App7020ChannelAssets::ASSET_TYPE_COURSES or App7020ChannelAssets::ASSET_TYPE_LERNING_PLANS)
	 */
	public $itemType;

	/**
	 * @var LearningCourse | LearningCoursepath The item to assign to the channels
	 */
	public $item;

	/**
	 * @var CActiveForm The form object where the HTML fields will be added
	 */
	public $form;

	public function init() {
		parent::init();

		// Load some specific assets needed
		if(!Yii::app()->request->isAjaxRequest) {
			$cs = Yii::app()->getClientScript();
			$cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/fcbkcomplete.css');
			$cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/jquery.fcbkcomplete.js');
		}
	}

	public function run() {
		$channelIds = array();

		// Load all channel IDs for the current item
		$channelIds = App7020ChannelAssets::getChannelsByContent(
			$this->itemType == App7020ChannelAssets::ASSET_TYPE_COURSES ? $this->item->idCourse : $this->item->id_path,
			$this->itemType
		);

		// Load translations for the current channel IDs
		$channels = array();
		if(!empty($channelIds))
			$channels = App7020Channels::model()->translation(false, $channelIds);

		$this->render('index', array(
			'channelIds' => $channels,
			'className' => get_class($this->item)
		));
	}

}
