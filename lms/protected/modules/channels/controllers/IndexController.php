<?php

/**
 * Main module controller
 * Class IndexController
 */
class IndexController extends Controller {

	/**
	 * Overriding default layout
	 */
	public function resolveLayout() {
		$this->layout = '/layouts/dashboard';
	}

	/**
	 * Access filters
	 * @return array
	 */
	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}

	/**
	 * Access rules for this controller
	 * @return array
	 */
	public function accessRules() {
		$res = array();

		// Allow access only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);
		return $res;
	}

	/**
	 *  Shows the Dashboard
	 */
	public function actionIndex() {
		$cs = Yii::app()->getClientScript();
		$themeUrl = Yii::app()->theme->baseUrl;
		App7020Helpers::registerApp7020Options();
		$cs->registerCssFile(App7020Helpers::getAssetsUrl() . "/css/app7020.css");
		$cs->registerCssFile($this->module->assetsUrl . "/css/swiper.min.css");
		$cs->registerCssFile($this->module->assetsUrl . "/css/style.css");

		//$cs->registerScriptFile($themeUrl . '/js/require_js/require.js');
		//$cs->registerScriptFile($this->module->assetsUrl . '/js/main.js');
		$this->breadcrumbs = array(Yii::t('course', '_WELCOME'));
		try {
			$params = array();
			$this->render('index', $params);
		} catch (Exception $e) {
			echo "<h4>" . $e->getMessage() . "</h4>";
			Yii::app()->end();
		}
	}

	/**
	 * Returns view HTML for angular JS
	 * @param $viewName
	 */
	public function actionView() {
		$viewName = Yii::app()->request->getParam('view_name');
		try {
			$this->renderPartial('channels.views.' . $viewName);
		} catch (Exception $e) {
			echo "<h4>" . $e->getMessage() . "</h4>";
			Yii::app()->end();
		}
	}

	/**
	 * Sets or updates personals channel description
	 * 
	 * @param $description 
	 */
	public function actionAxSetPersonalDescription() {
		$pChannelDescription = Yii::app()->request->getParam('description', '');
		$idUser = Yii::app()->user->id;
		$result = array("success" => true);
		try {
			$channelSettinsObject = App7020PersonalChannelsSettings::model()->findByAttributes(array('idUser' => $idUser));
			if (!$channelSettinsObject->id) {
				$channelSettinsObject = new App7020PersonalChannelsSettings();
				$channelSettinsObject->idUser = $idUser;
				$channelSettinsObject->description = $pChannelDescription;
				$channelSettinsObject->save();
			} else {
				$channelSettinsObject->description = $pChannelDescription;
				$channelSettinsObject->update();
			}

			$result["message"] = Yii::t('standard', 'The description has been updated successfully');
		} catch (Exception $e) {
			$result["success"] = false;
			$result["message"] = $e->getMessage();
		}

		echo json_encode($result);
	}

	/**
	 *
	 */
	public function actionChannelExperts() {
		$idChannel = Yii::app()->request->getParam('id', false);

		$dataProvider = App7020ChannelExperts::model()->sqlDataProvider(array("elements_per_page" => 5));

		$listViewParams = array(
			'disableFiltersWrapper' => true,
			'disableMassSelection' => true,
			'disableMassActions' => true,
			'listId' => "channelExpertsComboListView",
			'dataProvider' => $dataProvider,
			'noItemViewContainer' => true,
			'listItemView' => 'lms.protected.modules.channels.views.modal._expertListItem',
			'hiddenFields' => array('id' => $idChannel)
		);

		$this->renderPartial('lms.protected.modules.channels.views.modal._experts', array(
			'total' => $dataProvider->getTotalItemCount(),
			'listViewParams' => $listViewParams
				), false, true);
	}

	/**
	 * Gets channels list to assign to courses/LPs.
	 * Used for FCBKComplete
	 */
	public function actionAutocomplete() {
		$filter = Yii::app()->request->getParam('tag', '');
		$command = Yii::app()->db->createCommand(); /* @var $command CDbCommand */
		$command->select('t.id, COALESCE(tr.name, tr_default.name) AS name');
		$command->from(App7020Channels::model()->tableName() . " t");
		$command->where('t.type > 1'); // excluding from the list all channels of "predefined" type
		$command->where('t.enabled = 1'); // excluding from the list all invisible channels
		$command->leftJoin(App7020ChannelTranslation::model()->tableName() . " tr", '(tr.idChannel=t.id) AND (tr.lang = :lang)', array(':lang' => Yii::app()->getLanguage()));
		$command->leftJoin(App7020ChannelTranslation::model()->tableName() . " tr_default", '(tr_default.idChannel=t.id) AND (tr_default.lang = :defaultLang)', array(':defaultLang' => Lang::getBrowserCodeByCode(Settings::get('default_language', 'english'))));
		if ($filter != '') {
			$command->andWhere(
				'(tr.name LIKE :filter) OR (tr.description LIKE :filter) OR
				(tr.id IS NULL AND (tr_default.name LIKE :filter OR tr_default.description LIKE :filter))',
				array(':filter' => '%' . $filter . '%'));
		}

		$rows = $command->queryAll(true);
		$res = array();
		if (!empty($rows)) {
			foreach ($rows as $row) {
				$res[] = array(
					'key' => $row['id'],
					'value' => $row['name']
				);
			}
		}

		$this->sendJSON($res);
	}

}
