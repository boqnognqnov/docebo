/* global angular, document */
/*
 * Expandable texts directive
 */
'use strict';
define(function () {
	function ModalDirective($location) {
		return {
			restrict: 'AE',
			scope: false,
			link: function (scope, element, attrs) {

				// open modal on click
				element.bind("click", function (e) {
					openDialog(false, attrs.title, attrs.url, attrs.customId, attrs.classes, 'GET', attrs.data);
					$("html, body").animate({scrollTop: 0});

					$(document).one('dialog2.ajax-complete', '#app7020-invite-dialog', function () {
						if (angular.element(document.body).hasClass('ng-scope')) {
							angular.element(document.body).injector().invoke(function ($compile, $rootScope) {
								$compile($("#invite_to_watch"))($rootScope);
								$rootScope.$apply();
							});
						} else {
							angular.element('.widget-channels-functions').injector().invoke(function ($compile, $rootScope) {
								$compile($("#invite_to_watch"))($rootScope);
								$rootScope.$apply();
							});
						}
					});
					$(document).on('dialog2.ajax-complete', '#app7020-confirm-delete-modal-dialog', function () {
						if (angular.element(document.body).hasClass('ng-scope')) {
							angular.element(document.body).injector().invoke(function ($compile, $rootScope) {
								$compile($("#confirmDeleteAssetDialog"))($rootScope);
								$rootScope.$apply();
							});
						} else {
							angular.element('.widget-channels-functions').injector().invoke(function ($compile, $rootScope) {
								$compile($("#confirmDeleteAssetDialog"))($rootScope);
								$rootScope.$apply();
							});
						}
					});

				});
			}
		};
	}
	
	$(document).on('dialog2.before-open', "#dialog_content", function () {
		$(this).closest(".modal").css("position","fixed");
	});
	$(document).on('click', '#dialog_content .app7020-channel-experts-dialog .modal-footer #closebutton', function(){
		if($('#dialog_content').length)
		$('#dialog_content').dialog2('close');
	});
	ModalDirective.$inject = ['$location'];
	return ModalDirective;
});

