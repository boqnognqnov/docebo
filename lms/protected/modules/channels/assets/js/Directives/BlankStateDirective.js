/* global angular */
'use strict';
define(function () {
	/**
	 * Blank state directive for channels dashboard class definition
	 * @param {Service} $timeout
	 * @returns {Class}
	 */
	function BlankStateChannels($timeout){
		return {
			templateUrl: 'index.php?r=channels/index/view&view_name=directives.blankState',
			scope: true,
			restrict: 'E',
			link: function(scope, elem, attr){
				scope.$broadcast('hideLazyLoad');//Hiding lazy load
			}
		};
	}
	BlankStateChannels.$inject = ['$timeout'];//Injecting dependencies
	return BlankStateChannels;//Returns Class definition
});