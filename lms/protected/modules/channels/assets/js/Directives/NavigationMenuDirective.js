define(function () {
	function NavigationMenuDirective($location) {
		return {
			restrict: 'E',
			scope: false,
			templateUrl: 'index.php?r=channels/index/view&view_name=directives.navigationMenu',
			link: function (rootScope, iElem, iAttrs) {
				
				/* Add active class on init */
				if(iElem.find('ul.primaryMenu li a[href="#' + $location.path() + '"]').length > 0){
					iElem.find('ul.primaryMenu li a[href="#' + $location.path() + '"]').closest('li').addClass('active');
				}else{
					iElem.find('ul.primaryMenu li').first().addClass('active');
				}			

				/* Init SmoothActiveLine */
				iElem.find('ul.primaryMenu').boostrapSmoothActiveLine({
					height: '5px',
					background: '#0465AC'
				});
				
				/* Toggle classes when click on any link from navigation menu */
				iElem.on('click', 'ul.primaryMenu li', function () {
					iElem.find('li').removeClass('active');
					$(this).addClass('active');
				});
				
//				/* Open Additional menu links */
//				iElem.on('click', '.additionLinksMenu .toggle:not(.in)', function () {
//					$(this).addClass('in rotate');
//					$('body').addClass('backdrop');
//					$(this).closest('.additionLinksMenu').find('> li:not(.toggle)').each(function () {
//						$(this).css({display: 'block'});
//						$(this).stop().animate({
//							opacity: 1,
//							top: $(this).index() * $(this).height() + 10 * $(this).index()
//						}, 500);
//					});
//				});
//				/* Close Additional menu links */
//				iElem.on('click', '.additionLinksMenu .toggle.in', function () {
//					$(this).closest('.additionLinksMenu').find('> li:not(.toggle)').css({opacity: '', top: '', display: ''});
//					$(this).removeClass('in rotate');
//					$('body').removeClass('backdrop');
//				});
				
				iElem.on('mouseenter', '.additionLinksMenu li[data-toggle="tooltip"]', function(){
					var currentTooltip = $(this).parent().find('.tooltip.in');
					currentTooltip.css({
						top: currentTooltip.position().top
					});
				});
			}
		};
	}
	NavigationMenuDirective.$inject = ['$location'];
	return NavigationMenuDirective;
});