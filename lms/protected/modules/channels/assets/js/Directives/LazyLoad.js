/* global angular */
'use strict';
define(function () {
	/**
	 * Lazy loading directive class definition
	 * @param {Service} $timeout
	 * @returns {Class}
	 */
	function LazyLoadChannel($timeout){
		return {
			templateUrl: 'index.php?r=channels/index/view&view_name=directives.lazyload',
			scope: true,
			restrict: 'E',
			link: function(scope, elem, attr){
				scope.type = attr.type;//Binding type of lazyloader to scope
				scope.$on('showLazyLoad', function(){//Event listener to show lazy loading placeholders
					angular.element(elem).show();
				});
				scope.$on('hideLazyLoad', function(){//Event listener to hide lazy loading placeholders
					$timeout(function(){
						angular.element(elem).hide();
					});
					
				});
			}
		};
	}
	LazyLoadChannel.$inject = ['$timeout'];//Injecting dependencies
	return LazyLoadChannel;//Returns Class definition
});