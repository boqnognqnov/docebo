/* global angular */
'use strict';
define(function () {
	/**
	 * Rating read only
	 */
	function RatingStars(){
		return {
			templateUrl: 'index.php?r=channels/index/view&view_name=directives.ratings',
			scope: {
				maxValue: '=',
				ratingValue: '=',
			},
			restrict: 'A',
			link: function(scope, elem, attr){
				if(scope.ratingValue == '0'){
					$('.rating-count').addClass('null-value');
				}
				
				scope.stars = [];
				for (var i = 0; i < scope.maxValue; i++) {
					scope.stars.push({
						filled: i < scope.ratingValue
					});
				}
			}
		};
	}

	return RatingStars;//Returns Class definition
});