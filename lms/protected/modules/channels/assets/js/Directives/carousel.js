/* global angular, channelsDirectory, $user_id */
'use strict';
if (typeof channelsDirectory == 'undefined') {
	var channelsDirectory = channelsDirectoryModule;
}
define([channelsDirectory + '/lib/swiper.js'], function (Swiper) {
	/**
	 * Carousel directive class definition
	 * @param {Service} $timeout
	 * @param {Service} ChannelsData
	 * @returns {Class}
	 */
	function Carousel($timeout, ChannelsData) {
		return {
			templateUrl: 'index.php?r=channels/index/view&view_name=directives.carousel',
			scope: true,
			restrict: 'E',
			link: function (scope, elem, attr) {
				scope.items = JSON.parse(attr.data); //Parsing initial slides
				scope.chunkSize = 12;//Chunk size for slides
				scope.elementId = attr.id;//Element id
				scope.channelID = attr.id.split("-")[1];//Channeld id
				scope.inifiniteScroll = attr.infiniteScroll;//Infinite scroll function definition
				scope.lastItemID = scope.items.length;//Last item used for requesting more by infinite scroll
				scope.disableInfinite = false;//Flag should prevent infinite scroll request
				scope.endpoint = attr.endpoint;//End point passed in attributes
				scope.idUser = $user_id;


				/**
				 * Executes ajax request to parse more items for current carousel
				 * @param {Void} updateCallback
				 * @returns {Void}
				 */
				scope.infiniteScrollHorizontal = function (updateCallback) {
					if (scope.disableInfinite === true) {
						//updateCallback();
						return;
					}
					ChannelsData.getChannelsData({
						endpoint: (scope.endpoint ? scope.endpoint : ChannelsData.ENUM_ENDPOINT.ITEMS),
						id_channel: scope.channelID,
						from: scope.lastItemID,
						id_user: scope.idUser,
						successCallback: function ($result) {
							if ($result.items.length > 0) { //if there any results
								var i = 0;
								while ($result.items[i]) {
									scope.items.push($result.items[i]); //Push recieved items from server into existing stack
									i++;
								}
								if (scope.items.length === $result.items_count) { //If response items_count variable is equal to current items count should prevent next infinite scroll
									scope.disableInfinite = true;
								}
								scope.lastItemID = scope.items.length; //Update items count for future request
								if (!scope.$$phase) { //If scope not currently in loop trigger it
									scope.$digest();
								}
								updateCallback();//Call the passed callback
								scope.$emit('infiniteScroll:horizontal', {channelID: scope.channelID, items: scope.items});//Emit event that infintie scroll was triggered for outside purpose's
							} else { //Otherwsie call the passed callback and disable future infintie scroll
								updateCallback();
								scope.disableInfinite = true;
							}
						}
					});
				};

				scope.setPopStatus = function ($key, $status) {
					scope.items[$key].pop_status = $status;
				};

				scope.isSetPopStatus = function ($key, $status) {
					return scope.items[$key].pop_status === $status;
				};

				scope.isSetDismiss = function ($key, $status) {
					return scope.items[$key].dismiss === $status;
				};
				scope.forDismiss = [];
				scope.dismiss = function ($key) {
					scope.forDismiss.push({
						id: scope.items[$key].id,
						timeout: $timeout(function () {
							ChannelsData.getChannelsData({
								endpoint: (scope.endpoint ? scope.endpoint : ChannelsData.ENUM_ENDPOINT.ASSET_DISMISS),
								id_channel: scope.items[$key].id,
								id_user: scope.idUser,
								from: scope.items[$key].dismiss_from,
								successCallback: function ($result) {
									scope.items[$key].dismiss = 1;
									scope.closePop($key);
								}
							});
							if (scope.items.length === 1) {
								$('#channel-'+scope.channelID).closest('.channel-wrap').remove();
							}
						}, 5000)
					});
				};

				scope.undo = function ($key) {
					scope.forDismiss.forEach(function (item) {
						if (scope.items[$key].id == item.id) {
							$timeout.cancel(item.timeout);
							scope.forDismiss.splice(item.id, 1);
						}
					});
					ChannelsData.getChannelsData({
						endpoint: (scope.endpoint ? scope.endpoint : ChannelsData.ENUM_ENDPOINT.ASSET_UNDO_DISMISS),
						id_channel: scope.items[$key].id,
						id_user: scope.idUser,
						from: scope.items[$key].dismiss_from,
						successCallback: function ($result) {
							scope.items[$key].dismiss = 0;
							scope.setPopStatus($key, 0);
						}
					});
				};

				scope.closePop = function ($key) {
					if (scope.items[$key].dismiss == 0) {
						scope.setPopStatus($key, 0);
					} else {
						scope.items.splice($key, 1);
						//delete scope.items[$key];
					}
				};

				var initiliaze = function () { //Function used to init/reinit swiper carousel on angular $digest done
					$timeout(function () {
						angular.element('[data-toggle="tooltip"]').tooltip(); //Reinitiliazing bootstrap tooltips everytime angular $digest over
						angular.element('.lazyload-swiper').hide(); //Hiding lazyloading placeholders
						//Creating instance of the Swiper for params information check http://idangero.us/swiper/api/#.Vt_YypN95cA
						new Swiper("[data-id='" + scope.elementId + "']", {
							slidesPerView: 'auto',
							nextButton: '.swiper-button-next',
							prevButton: '.swiper-button-prev',
							slidesOffsetAfter: 60,
							//freeMode: true,
							onReachEnd: function ($self) {
								//Executes when swiper reach end
								if (scope.items.length < 12) { //If items less than 12 dont execute infinite carousel this means there are no more items on server
									$self.update(true);
									return;
								}
								if (!scope.disableInfinite) {
									var lazyLoadSwipes = angular.element($self.wrapper[0]).find('.lazyload-swiper'); //Find lazy load placeholders
									lazyLoadSwipes.show();//Show lazy load placeholders on request start
									$self.update(true); //Update swiper so can show newly showed placeholders
									scope.infiniteScrollHorizontal(function () {//Trigger request to the server
										lazyLoadSwipes.hide();//When done hide lazy load placeholders
										$timeout(function () {
											$self.update(true);//Update swiper so can remove unneccessary placeholders
											angular.element('[data-toggle="tooltip"]').tooltip(); //Reinitiliazing bootstrap tooltips everytime angular $digest over
											$self.slideNext(false);
										});
									});
								}
							},
							onSlideChangeEnd: function ($self) {
								// this breaks the swiper in RTL languages
								// $self.update(true);
							}
						});
					});
				};
				//Calling swiper initially
				initiliaze();
				//Calling swiper everytime infiniteScroll is triggered because its not native angular library and not catching dom modifications made by angular
				scope.$on('infiniteScroll:vertical', function () {
					initiliaze();
				});
			}
		};
	}
	Carousel.$inject = ['$timeout', 'ChannelsData'];//Injecting dependencies
	return Carousel;//Returns class definition
});