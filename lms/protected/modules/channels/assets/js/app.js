'use strict';
define(['angular'], function () {
	return angular.module('Channels', [
		
		'infinite-scroll',
		'Channels.Services',
		'Channels.Directives',
		'Channels.Controllers',
		'ngSanitize',
		'angular-clipboard'
	])
			.filter('range', function () {
				return function (input, total) {
					total = parseInt(total);

					for (var i = 0; i < total; i++) {
						input.push(i);
					}
					return input;
				};
			})
			.filter('cut', function () {
				return function (value, wordwise, max, tail) {
					if (!value)
						return '';
					max = parseInt(max, 10);
					if (!max)
						return value;
					if (value.length <= max)
						return value;
					value = value.substr(0, max);
					if (wordwise) {
						var lastspace = value.lastIndexOf(' ');
						if (lastspace !== -1) {
							value = value.substr(0, lastspace);
						}
					}
					return value + (tail || ' …');
				};
			});
});
