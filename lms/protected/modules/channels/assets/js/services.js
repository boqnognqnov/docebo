'use strict';
define(function (require) {
	var service = require('angular').module('Channels.Services',[]);
	service.factory('ChannelsData', require('Services/ChannelsData'));
	service.factory('ChannelsDataSingle', require('Services/ChannelsDataSingle'));
	service.factory('dataFeed', require('app7020Factories/ngDataFeed'));
	return service;
	// @todo
	// По същия начин си добави модели, ако ти трябват
});
