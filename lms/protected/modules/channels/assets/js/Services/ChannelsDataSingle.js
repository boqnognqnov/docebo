/* global angular */

var isRequestRunning = false; //Flag used for checks whether currently there is an running request
var requestMoreItems = true; // True , False to enable , disable lazy load
define(function(){
	/**
	 * Return angular service for parsing channels data from server
	 * @param {$q} $q
	 * @param {$http} $http
	 * @returns {Class}
	 */
	function ChannelsDataSingle($rootScope, $q, $http, channelID){
		var self = {};
		var request = "";
		self.response = "";
		self.channels = [];//Channels object
		self.itemsCount = 0;//Channels object
		self.itemsChunk = 12;//Items per load
		self.requestItems = true;// True , False to enable , disable lazy load

		/**
		 * ENUM with available endpoints
		 */
		self.ENUM_ENDPOINT = {
			ASSET_DISMISS:'asset_dismiss',
			ASSET_UNDO_DISMISS:'asset_undo_dismiss',
		};

		/*
		 * Checks whether can request more items
		 * @returns {requestMoreChannels|Boolean}
		 */
		self.canRequestMoreItems = function(){
			return self.requestItems;
		};
		/**
		 * Executes ajax request against server to fetch channels data from Data feeder
		 * @param {Object} params - object with data feeder params check defaults for reference
		 * @returns {Void}
		 */
		self.getSingleData = function(channelID){

			params = typeof params !== 'undefined' ? params : {};
			var requestParamsSingle = {
				endpoint: 'channel_items',
				params: {
					id_channel: channelID, 
					single_channel: 1,
//					count: self.itemsChunk,
//					from: self.itemsCount
				}
			};
			isRequestRunning = true;
			request = $http.post(
				'/lms/index.php?r=restdata',
				requestParamsSingle
			);
			return request;
		};
		
		self.setItemsCount = function(itemsCount){
			self.itemsCount = itemsCount;
		};

		self.setRequestItems = function(){
			self.requestItems = false;
		};

		/*
		 * Whether the entered endpoint is valid or not
		 * @param {ENUM_ENDPOINT} endpoint
		 * @returns {Boolean}
		 */
		self.validEndpoint = function(endpoint){
			for(var key in self.ENUM_ENDPOINT){
				if(endpoint === self.ENUM_ENDPOINT[key]){
					return true;
				}
			}
			return false;
		};

		/**
		 * Checks and execute callback according to entered endpoint
		 * @param {ENUM_ENDPOINT} endpoint
		 * @param {JSON} $result
		 * @returns {Void}
		 */
		self.requestSuccess = function(endpoint, $result){
			switch(endpoint){
				case self.ENUM_ENDPOINT.CHANNELS:
					self.channelsCallback($result);
				case self.ENUM_ENDPOINT.ITEMS:
					self.itemsCallback($result);
			}
		};


		self.getChannelsData = function(params){
			//if(isRequestRunning === true){
			//	return;
			//}
			params = typeof params !== 'undefined' ? params : {};
			var defaults = {
				endpoint: self.ENUM_ENDPOINT.CHANNELS,
				id_user: null,
				id_channel: 0,
				lang_code: null,
				from: 0,
				count: self.channelsChunk,
				items_per_channel: self.itemsChunk
			};
			if(typeof params.successCallback !== 'function'){
				params.successCallback = function($result){
					console.log($result);
				};
			}
			if(typeof params.errorCallback !== 'function'){
				params.errorCallback = function($result){
					console.log($result);
				};
			}
			angular.merge(defaults, params);
			if(!self.validEndpoint(defaults.endpoint)){
				return;
			}
			if(defaults.endpoint === self.ENUM_ENDPOINT.CHANNELS && requestMoreChannels === false){
				return;
			}
			var requestParams = {
				endpoint: defaults.endpoint,
				params: {
					id_user: defaults.id_user,
					id_channel: defaults.id_channel,
					lang_code: defaults.lang_code,
					from: defaults.from,
					count: defaults.count,
					items_per_channel: defaults.items_per_channel
				}
			};
			var defer = $q.defer();
			isRequestRunning = true;
			$http.post(
				'/lms/index.php?r=restdata',
				requestParams
			)
				.success(function(response){
					isRequestRunning = false;
					defer.resolve(response);
				})
				.error(function(){
					isRequestRunning = false;
					defer.reject("Error getting channel items");
				});
			defer.promise.then(function($result){
				self.requestSuccess(defaults.endpoint, $result);
				params.successCallback($result);
				self.shouldExecuteRequest(defaults.endpoint, $result);
			}, params.errorCallback);
		};
		
		return self;
	}
		ChannelsDataSingle.$inject = ['$rootScope', '$q','$http']; //Injecting dependencies
		return ChannelsDataSingle; //Returns class definition
});