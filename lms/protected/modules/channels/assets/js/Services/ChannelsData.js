/* global angular */

var isRequestRunning = false; //Flag used for checks whether currently there is an running request
var requestMoreChannels = true; //Flag should do request for more channels
var requestMoreItems = true;
define(function(){
	/**
	 * Return angular service for parsing channels data from server
	 * @param {$q} $q
	 * @param {$http} $http
	 * @returns {Class}
	 */
	function ChannelsData($q, $http){
		var self = {};
		
		/**
		 * ENUM with available endpoints
		 */
		self.ENUM_ENDPOINT = {
			CHANNELS: 'channels',
			ITEMS: 'channel_items',
			USER_CHANNELS: "user_channels",
			USER_CHANNEL_ITEMS: "user_channel_items",
			USER_CHANNEL_IN_REVIEW: 'user_channel_in_review',
			USER_CHANNEL_MY_CONTRIBUTION: 'user_channel_my_contributions',
			OTHER_PERSONAL_CHANNEL: 'other_personal_channel',
			ASSET_DISMISS:'asset_dismiss',
			ASSET_UNDO_DISMISS:'asset_undo_dismiss',
		};
		
		/* Set number of channels (used in channels widget) */
		var channelChunkCount = angular.element('.channels-app').attr('data-channels');
		
		self.channels = [];//Channels object
		self.itemsChunk = 12;//Items per load
		
		self.blankstate = []; //Empty state when no channels available per user
							
		if(channelChunkCount){
			self.channelsChunk = channelChunkCount;//Channels per load
		}else{
			self.channelsChunk = 5;//Channels per load
		}
		self.next_from = 0;//From which index/id should start next request on the server
		/*
		 * Checks whether can request more channels
		 * @returns {requestMoreChannels|Boolean}
		 */
		self.canRequestMoreChannels = function(){
			return requestMoreChannels;
		};
		/*
		 * Checks whether can request more items
		 * @returns {requestMoreChannels|Boolean}
		 */
		self.canRequestMoreItems = function(){
			return requestMoreItems;
		};
		
		/*
		 * Whether the entered endpoint is valid or not
		 * @param {ENUM_ENDPOINT} endpoint
		 * @returns {Boolean}
		 */
		self.validEndpoint = function(endpoint){
			for(var key in self.ENUM_ENDPOINT){
				if(endpoint === self.ENUM_ENDPOINT[key]){
					return true;
				}
			}
			return false;
		};
		
		/**
		* 
		* Check if user is on widgets dashboard
		*/
	   var dashboardElem = document.getElementById('mydashboard');
		
		/**
		 * Executes ajax request against server to fetch channels data from Data feeder
		 * @param {Object} params - object with data feeder params check defaults for reference
		 * @returns {Void}
		 */
		self.getChannelsData = function(params){
			if (dashboardElem === null) {
				if(isRequestRunning === true){
					return;
				}
			}
			params = typeof params !== 'undefined' ? params : {};
			var defaults = {
				endpoint: self.ENUM_ENDPOINT.CHANNELS,
				id_user: null,
				id_channel: 0,
				lang_code: null,
				from: 0,
				count: self.channelsChunk,
				items_per_channel: self.itemsChunk
			};
			if(typeof params.successCallback !== 'function'){
				params.successCallback = function($result){
					console.log($result);
				};
			}
			if(typeof params.errorCallback !== 'function'){
				params.errorCallback = function($result){
					console.log($result);
				};
			}
			angular.merge(defaults, params);
			if(!self.validEndpoint(defaults.endpoint)){
				return;
			}
			if(defaults.endpoint === self.ENUM_ENDPOINT.CHANNELS && requestMoreChannels === false){
				return;
			}
			var requestParams = {
				endpoint: defaults.endpoint,
				params: {
					id_user: defaults.id_user,
					id_channel: defaults.id_channel,
					lang_code: defaults.lang_code,
					from: defaults.from,
					count: defaults.count,
					items_per_channel: defaults.items_per_channel
				}
			};
			var defer = $q.defer();
			isRequestRunning = true;
			$http.post(
				'/lms/index.php?r=restdata',
				requestParams
			)
			.success(function(response){
				isRequestRunning = false;
				defer.resolve(response);
			})
			.error(function(){
				isRequestRunning = false;
				defer.reject("Error getting channel items");
			});
			defer.promise.then(function($result){
				self.requestSuccess(defaults.endpoint, $result);
				params.successCallback($result);
				self.shouldExecuteRequest(defaults.endpoint, $result);
				self.getBlankstate(defaults.endpoint, $result);
			}, params.errorCallback);
		};
		/**
		 * Checks and execute callback according to entered endpoint
		 * @param {ENUM_ENDPOINT} endpoint
		 * @param {JSON} $result
		 * @returns {Void}
		 */
		self.requestSuccess = function(endpoint, $result){
			switch(endpoint){
				case self.ENUM_ENDPOINT.CHANNELS:
					self.channelsCallback($result);
				case self.ENUM_ENDPOINT.ITEMS:
					self.itemsCallback($result);
			}
		};
		/**
		 * The callback that is executed for endpoint channels
		 * @param {JSON} $result
		 * @returns {void}
		 */
		self.channelsCallback = function($result){
			self.next_from = $result.next_from;
			var newChannels = $result.channels;
			if(newChannels.length > 0){
				var i = 0;
				while(newChannels[i]){
					var item = newChannels[i];
					if(self.channels.indexOf(item) === -1){
						self.channels.push(item);
					}
					i++;
				}
			}
		};
		/**
		 * The callback that is executed for endpoint channel_items
		 * @param {JSON} $result
		 * @returns {void}
		 */
		self.itemsCallback = function($result){
			self.updateChannelItems($result.id_channel, $result.items);
		};
		/**
		 * Returns channel by id from channel object
		 * @param {Int} channelID
		 * @returns {Boolean}
		 */
		self.getChannelById = function(channelID){
			var channels = self.getChannels();
			var index = self.getChannelIndex(channelID);
			var needed = channels[index];
			if(typeof needed !== 'undefined'){
				return needed;
			}
			return false;
		};
		/**
		 * Checks if there is data in the channels object and returns it otherwise returns false
		 * @returns {Array|self.channels|Boolean}
		 */
		self.getChannels = function(){
			if(self.channels.length > 0){
				return self.channels;
			}
			return false;
		};
		/**
		 * Checks should do more requests for specified endpoint
		 * @param {ENUM_ENDPOINT} endpoint
		 * @param {JSON} response
		 * @returns {Void}
		 */
		self.shouldExecuteRequest = function(endpoint, response){
			switch(endpoint) {
				case self.ENUM_ENDPOINT.CHANNELS:
					if(response.channels.length === 0){
						requestMoreChannels = false;
					}
					break;
				case self.ENUM_ENDPOINT.ITEMS:
					var cItemsCount = 0;
					var idChannel = response.id_channel;
					var itemsCount = response.items_count;
					var channel = self.getChannelById(idChannel);
					if(channel !== false){
						cItemsCount = channel.items.length;
					}
					if(itemsCount <= cItemsCount){
						requestMoreItems = false;
					}
				default:
					return;
			}
		};
		
		 /* Get blank state fields
		 */
		self.getBlankstate = function(endpoint, response){
			if(endpoint === 'channels'){
				if(response.channels.length === 0){
					self.blankstate = response.blank_slate;
					BlankstateFields = self.blankstate;
				}
			}
		};
		
		/* Get blank state fields in controller
		 */
		self.getBlankstatecontroller = function(){
			return self.blankstate;
		};
		
		/**
		 * Updating channel items for keeping the state of channels dashboard till refresh
		 * @param {Int} channelID
		 * @param {Array} items
		 * @returns {Void}
		 */
		self.updateChannelItems = function(channelID, items){
			var index = self.getChannelIndex(channelID);
			if(index !== false){
				if(items.length > 0){
					var i;
					for(i=0;i<items.length;i++){
						self.channels[index].items.push(items[i]);
					}
				}
			}
		};
		/**
		 * Returns channels index from the channels object by its id otherwise returns false
		 * @param {Int} channelID
		 * @returns {Boolean|Int}
		 */
		self.getChannelIndex = function(channelID){
			var channels = self.getChannels();
			if(channels !== false){
				var filtered = channels.filter(function(obj){
					if(parseInt(obj.id) === parseInt(channelID)){
						return true;
					}
				});
				if(filtered.length > 0){
					return channels.indexOf(filtered[0]);
				}
			}
			return false;
		};
		
			/**
		 * Executes ajax request against server to fetch channels data from Data feeder
		 * @param {Object} params - object with data feeder params check defaults for reference
		 * @returns {Void}
		 */
		self.getSingleData = function(channelID, count, from){

			params = typeof params !== 'undefined' ? params : {};
			var requestParamsSingle = {
				endpoint: 'channel_items',
				params: {
					id_channel: channelID, 
					single_channel: 1,
					count: count,
					from: from
				}
			};
			isRequestRunning = true;
			request = $http.post(
				'/lms/index.php?r=restdata',
				requestParamsSingle
			);
			return request;
		};
		self.setItemsCount = function(itemsCount){
			self.itemsCount = itemsCount;
		};
		
		return self;
	};
	ChannelsData.$inject = ['$q','$http']; //Injecting dependencies
	return ChannelsData; //Returns class definition
});
