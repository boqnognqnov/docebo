/* global app7020AssetPath */
var scripts = document.getElementsByTagName("script");
var currentScriptPath = scripts[scripts.length - 1].src;
var currentScriptPathArray = currentScriptPath.split("/");
currentScriptPathArray.pop();
var channelsDirectory = mainWidget+'/js';
require.config({
	waitSeconds: 20,
	baseUrl: channelsDirectory, //app7020Options.assetUrl + '/',
	paths: {
		app7020Controllers: '../../../' + app7020AssetPath + 'js/controllers',
		app7020Modules: '../../../' + app7020AssetPath + 'js/modules',
		app7020Factories: '../../../' + app7020AssetPath + 'js/factory',
		angular: '/themes/spt/js/angularJs/angular',
		angularResource: '/themes/spt/js/angularJs/angular-resource',
		angularRoute: '/themes/spt/js/angularJs/angular-route',
		angularSanitize: '/themes/spt/js/angularJs/angular-sanitize.min',
		infiniteScroll: channelsDirectory + '/lib/ng-infinite-scroll.min',
		angularClipboard: channelsDirectory + '/lib/angular-clipboard',
		'app7020PDeleteAsset': '../../../' + app7020AssetPath + 'js/plugins/app7020PDeleteAsset',
		'modules/notification': '../../../' + app7020AssetPath + 'js/modules/notification'
	},
	shim: {
		'angular': {'exports': 'angular'},
		'angularRoute': {
			'exports': 'angular',
			'deps': ['angular']
		},
		'angularSanitize': {
			'exports': 'ngSanitize',
			'deps': ['angular']
		},
		'infiniteScroll': {
			'exports': 'infinite-scroll',
			'deps': ['angular']
		},
		'angularClipboard': {
			'exports': 'angularClipboard'
		},
	},
	priority: [
		"angular",
		"angularRoute",
		"angularSanitize",
		"infiniteScroll",
		"angularClipboard"
	]
});
