'use strict';
define(function (require) {
	require('angular').module('Channels', [ 'ngOrderObjectBy']);
	var ctrl = require('angular').module('Channels.Controllers', ['Channels.Services', 'ngRoute', 'ngOrderObjectBy']);
	ctrl.controller('DashboardController', require('Controllers/DashboardController'));
	ctrl.controller('ChannelsSingleController', require('Controllers/ChannelsSingleController'));
	ctrl.controller('MyChannelController', require('Controllers/MyChannelController'));
	ctrl.controller('PChannelController', require('Controllers/PChannelController'));
	ctrl.controller('inviteController', require('app7020Controllers/inviteToWatch'));
	ctrl.controller('app7020DeleteAsset', require('app7020Controllers/app7020DeleteAsset'));
	ctrl.config(require('config'));
	ctrl.run(require('run'));
	return ctrl;
	// @todo
	// По същия начин си добави контроли, ако ти трябват
});
