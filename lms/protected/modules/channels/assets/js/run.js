/* global $scope, angular */

define(function(){
	function AngularRun($rootScope, $timeout){
		$rootScope.bodyClass = '';
		$rootScope.navbarClass = '';
		/* Open three dots link menu */
		$rootScope.open = function($event){
			var selector = angular.element($event.currentTarget).parent().parent().find('.dots-menu');
			selector.addClass('open');
		};
		/* Close three dots link menu */
		$rootScope.close = function($event) {
			var selector = angular.element($event.currentTarget).parent().closest('.dots-menu');
			selector.removeClass('open');
		};
		
		/* Deeplink functionality (copy to clipboard) */
		$rootScope.getShareurl = function($event){
			var selector = angular.element($event.currentTarget).parent().find('.copy-url');
			var selectorInitial = angular.element($event.currentTarget);
			var selectorCopied = angular.element($event.currentTarget).parent().find('.copied-success');
			$rootScope.textToCopy = selector.attr('href');
			
			$rootScope.success = function () {
				selectorInitial.hide();
				selectorCopied.show();
				$timeout(function() {
					selectorInitial.show();
					selectorCopied.hide();
				}, 2000);
			};

			$rootScope.fail = function (err) {
				console.error('Error!', err);
			};
		};
		
		$(document).on('click', function (e) {
			if ($(e.target).parents('.col-tile').length === 0) {
				$(e.target).find(".dots-menu").removeClass('open');
			}
		});
		
		$rootScope.$on( "$routeChangeStart", function(event, next, current) {
			switch (next.originalPath) {
				case '/':
					$rootScope.navbarClass = 'dashboard-page';
					break;
				case '/channel/:id':
					$rootScope.navbarClass = 'viewall-page';
					break;
				case '/pchannel/:id':
					$rootScope.navbarClass = 'viewall-page';
					break;
				default: 
					$rootScope.bodyClass = ''; 
					$rootScope.navbarClass = '';
					break;
			}
		});
		$timeout(function(){
			$(document).on('click', function (e) {
				if ($(e.target).parents('.col-tile').length === 0) {
					$(e.target).find(".dots-menu").removeClass('open');
				}
			});
		}, 100);
		
	}
	AngularRun.$inject = ['$rootScope', '$timeout'];
	return AngularRun;
});


