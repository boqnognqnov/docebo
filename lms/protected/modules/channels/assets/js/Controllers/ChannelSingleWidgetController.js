/* global angular */
var shouldRequestMore = true;//Flag should do more request against the server if there are no more items this flag is setted to false
var preventInfinite = true; //Flag to prevent infinite scroll further its true by default so when no loaded items ot not retrigger
define(function () {
	/**
	 * 
	 * @param {Service} $scope
	 * @param {Service} $timeout
	 * @param {Service} ChannelsData
	 * @param {Array} $routeParams
     * @param NativeAPI
	 * @returns {Class}
	 */
	function ChannelsSingleWidgetController($scope, $timeout, ChannelsData, $routeParams, $attrs){
		$scope.channelID = $attrs.channel; //Setting current channel id from query string
		$scope.chunk = $attrs.chunk;
		//$scope.channel = ChannelsData.getChannelById($scope.channelID); //Get channel data by current id from the ChannelsData service
		$scope.showLazyLoad = true;
		$scope.showLoadmore = true;
		$scope.itemsCount = 0;
		
		/* Recalculate tooltip position when needed */
		$scope.tooltipPosition = function(e){
			 $timeout(function () {
				var offset = $(e.target).closest('.col-tile').offset().left - $(e.target).closest('.single-channel-app').offset().left;
				if (offset <= 10) {
					$(e.target).parent().addClass('left-offset');
				}
			});
		};
		/* Reinitialize bootstrap functions */
		$scope.ScriptsInit = function(){
			$timeout(function(){
				$('body').tooltip({
					selector: '[rel=tooltip]'
				});
			});
		};
		
		var request = ChannelsData.getSingleData($scope.channelID, $scope.chunk, $scope.itemsCount);
		request.then(function (response) {
			$scope.channel = response.data.channel;
			$scope.itemsCount = $scope.channel.items.length;
			ChannelsData.setItemsCount($scope.itemsCount);
			$scope.showLazyLoad = false;
			//Hide LoadMore button if items less that the chunk size
			if($scope.channel.items.length < $scope.chunk){
				$scope.showLoadmore = false;
			}
			$scope.ScriptsInit();
		});
		
		$scope.channelItems = [];
		
		$scope.infiniteScrollVertical = function(){
			$scope.showLazyLoad = true;
			var requestNew = ChannelsData.getSingleData($scope.channelID, $scope.chunk, $scope.itemsCount);
			requestNew.then(function (response) {
				response.data.channel.items.forEach(function(item){
					$scope.channel.items.push(item);
				});
				//$scope.channel = response.data.channel;
				$scope.itemsCount = $scope.channel.items.length;
				ChannelsData.setItemsCount($scope.itemsCount);
				$scope.showLazyLoad = false;
				if(response.data.channel.items.length <= 0){
					$scope.showLoadmore = false;
				}
			});
			$scope.ScriptsInit();
		};
		
		//Reload view ChannelsList/SingleChannel
		$scope.reloadViewChannel = function(e){
			var dashletId = $(e.target).data('dashlet');
			$('#mydashboard').dashboard('loadContent', '.dashlet[data-dashlet-id="'+dashletId+'"]', '', function(){});
			$scope.ScriptsInit();
		};
		

	};
	ChannelsSingleWidgetController.$inject = ['$scope', '$timeout', 'ChannelsData', '$routeParams', '$attrs'];//Injecting dependencies
	return ChannelsSingleWidgetController;//Return Class definition
});
