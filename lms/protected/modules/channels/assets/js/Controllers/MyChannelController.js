define(function () {

	function MyChannelController($scope, $q, $http, ChannelsData, $sce, $timeout) {
		$scope.channelMyContribution = [];
		$scope.channelMyInReview = [];
		$scope.channelData = null;
		$scope.contentEditable = false;

		/* My Contributions */
		$scope.getMyChannelData = function () {
			ChannelsData.getChannelsData({
				endpoint: ChannelsData.ENUM_ENDPOINT.USER_CHANNELS,
				id_user: $user_id,
				count: {
					inReviewCount: 5,
					myContributionsCount: 12
				},
				successCallback: function ($result) {
					$scope.channelData = $result.channel;
					$result.channel.items.forEach(function(data){
						if(data.other_fields.status == 10 || data.other_fields.status == 15){
							$scope.channelMyInReview.push(data);
						}
						
						if(data.other_fields.status == 18 || data.other_fields.status == 20){
							$scope.channelMyContribution.push(data);
						}
					});
				}
			});
		};
		
		$scope.showDescription = function(){
			$('#descriptionInput').show();
		};
		$scope.focusDescription = function(){
			$timeout(function() {
				$('#descriptionInput').focus();
			},100);
		};
		
		$scope.updateDescription = function($event){
			var keyCode = $event.which || $event.keyCode;
			var el = $($event.currentTarget);
			if (keyCode === 13 || $event.type == 'blur') {
				$event.preventDefault();
				$scope.contentEditable = false;
				var defer = $q.defer();
				$http.post(
					'/lms/index.php?r=channels/index/axSetPersonalDescription',
					{
						description: el[0].value
					}
				)
				.success(function(response){
					defer.resolve(response);
					$scope.channelData.description = el[0].value;
				})
				.error(function(){
					isRequestRunning = false;
					defer.reject("Error getting channel items");
				});
			}
			
		};
		$scope.switchToEditable = function($event){
			var el = $($event.currentTarget);
			$scope.contentEditable = true;
			$scope.focusDescription();
		}

		$scope.init = function () {
			$scope.getMyChannelData();
		};
		
		$scope.infiniteScrollVertical = function(){
			ChannelsData.getChannelsData({
				endpoint: ChannelsData.ENUM_ENDPOINT.USER_CHANNEL_MY_CONTRIBUTION,
				from: $scope.channelMyContribution.length,
				count: 12,
				id_user: $user_id,
				successCallback: function($response){
					$response.items.forEach(function(item){
						$scope.channelMyContribution.push(item);
					});
				}
			});
			$scope.$emit("infiniteScroll:vertical");
		};
	}
	MyChannelController.$inject = ['$scope', '$q', '$http', 'ChannelsData', '$sce','$timeout'];
	return MyChannelController;
});