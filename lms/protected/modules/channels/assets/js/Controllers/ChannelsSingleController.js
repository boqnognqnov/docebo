/* global angular */
var shouldRequestMore = true;//Flag should do more request against the server if there are no more items this flag is setted to false
var preventInfinite = true; //Flag to prevent infinite scroll further its true by default so when no loaded items ot not retrigger
define(['../../../../' + app7020AssetPath + 'js/modules/appendCSS.js', 'angular'], function (css, angular) {
	new css('../../../../' + app7020AssetPath + '/css/app7020.css');
	/**
	 * 
	 * @param {Service} $scope
	 * @param {Service} $timeout
	 * @param {Service} ChannelsData
	 * @param {Array} $routeParams
	 * @returns {Class}
	 */
	function ChannelsSingleController($scope, $timeout, ChannelsDataSingle, $routeParams) {
		$timeout(function () {
			$scope.$broadcast('hideLazyLoad');
		});
		$scope.channelID = $routeParams.id; //Setting current channel id from query string
		var request = ChannelsDataSingle.getSingleData($routeParams.id);
		request.then(function (response) {
			if (response.data.success) {
				$scope.channel = response.data.channel;
				$scope.itemsCount = $scope.channel.items.length;
				ChannelsDataSingle.setItemsCount($scope.itemsCount);
				$("lazyload").hide();
			}
			else {
				$scope.error = response.data.error;
				$("lazyload").hide();
			}
		});
		setTimeout(function () {
		}, 500);

		if ($scope.channel === false) {//If the result for retrieving channel from cache is false show lazy loading and retrieve new data from the server and removes preventing infinite scroll
			$scope.$broadcast('showLazyLoad');
			request.then(function (response) {
				if (response.data.success) {
					$scope.channel = response.data.channel;
					$scope.itemsCount = $scope.channel.items.length;
					$("lazyload").hide();
				}
				else {
					$scope.error = response.data.error;
					$("lazyload").hide();
				}
			});

		} else {//Otherwise set the count of items and remove preventing infinite scroll		
			preventInfinite = false;

		}
		/**
		 * Loading new elements from the server for the grid
		 * @returns {Void}
		 */
		$scope.infiniteScrollVertical = function () {
			if (!ChannelsDataSingle.canRequestMoreItems()) { //If should prevent infinite scroll or shouldnt do more request stop execution of this method
				$("lazyload").hide();
				return;
			} else {
				$scope.$broadcast('showLazyLoad');//Show placeholders if they hidden
				request.then(function (response) {
					//response.data.channel.items.forEach(function(item){
					//	$scope.channel.items.push(item);
					//});
					if (response.data.success) {
						$scope.itemsCount = $scope.channel.items.length;
						if ($scope.channel.items.length >= response.data.channel.items_count) {
							ChannelsDataSingle.setRequestItems();
						}
						ChannelsDataSingle.setItemsCount($scope.itemsCount);
						$("lazyload").hide();
					}
					else {
						$scope.error = response.data.error;
						$("lazyload").hide();
					}
				});
			}
//			ChannelsDataSingle.getSingleData($routeParams.id).then(function(response){
//				$scope.channel = response.data.channel;
//				$scope.itemsCount = $scope.channel.items.length;
//				ChannelsDataSingle.setItemsCount($scope.itemsCount);
//				$("lazyload").hide();
//			});
		};

		$scope.setPopStatus = function ($key, $status) {
			$scope.channel.items[$key].pop_status = $status;
		};

		$scope.isSetPopStatus = function ($key, $status) {
			return $scope.channel.items[$key].pop_status === $status;
		};

		$scope.isSetDismiss = function ($key, $status) {
			return $scope.channel.items[$key].dismiss === $status;
		};
		$scope.forDismiss = [];
		$scope.dismiss = function ($key) {
			$scope.forDismiss.push({
				id: $scope.channel.items[$key].id,
				timeout: $timeout(function () {
					ChannelsDataSingle.getChannelsData({
						endpoint: ChannelsDataSingle.ENUM_ENDPOINT.ASSET_DISMISS,
						id_channel: $scope.channel.items[$key].id,
						id_user: $scope.idUser,
						from: $scope.channel.items[$key].dismiss_from,
						successCallback: function ($result) {
							$scope.channel.items[$key].dismiss = 1;
							$scope.closePop($key);
						}
					});
				}, 5000)
			});



		};

		$scope.undo = function ($key) {
			$scope.forDismiss.forEach(function (item) {
				if ($scope.channel.items[$key].id == item.id) {
					$timeout.cancel(item.timeout);
					$scope.forDismiss.splice(item.id, 1);
				}
			});
			ChannelsDataSingle.getChannelsData({
				endpoint: ChannelsDataSingle.ENUM_ENDPOINT.ASSET_UNDO_DISMISS,
				id_channel: $scope.channel.items[$key].id,
				id_user: $scope.idUser,
				from: $scope.channel.items[$key].dismiss_from,
				successCallback: function ($result) {
					$scope.channel.items[$key].dismiss = 0;
					$scope.setPopStatus($key, 0);
				}
			});
		};

		$scope.closePop = function ($key) {
			if ($scope.channel.items[$key].dismiss == 0) {
				$scope.setPopStatus($key, 0);
			} else {
				$scope.channel.items.splice($key, 1);
				$scope.itemsCount = $scope.channel.items.length;
				$scope.channel.counters.count = $scope.channel.items.length;
				//delete scope.items[$key];
			}
		}
	}
	;
	ChannelsSingleController.$inject = ['$scope', '$timeout', 'ChannelsDataSingle', '$routeParams'];//Injecting dependencies
	return ChannelsSingleController;//Return Class definition
});
