define(['../../../../' + app7020AssetPath + 'js/modules/appendCSS.js', 'angular'],function (css,angular) {
	new css('../../../../' + app7020AssetPath + '/css/app7020.css');
	new css(themesUrl + '/css/DoceboPager.css');
	/**
	 * 
	 * @param {Service} $scope
	 * @param {Service} $http
	 * @param {Service} ChannelsData
	 * @returns {Class}
	 */
	function DashboardController($scope, $http, ChannelsData){
		$scope.iScrollRunning = false;
		$scope.$broadcast('hideLazyLoad');
		$scope.channels = [];//Channels scope object
		$scope.showLoadmore = true; //applicable for channels widget
		$scope.showLazyLoad = true;
		$scope.showBlankstate = false;
		$scope.BlankState = [];
		var BlankStateCache = [];
		
		$scope.init = function(){//Called on page initialisation
			var channels = ChannelsData.getChannels();//Retrieve channels data from the service
			if(!channels){//If channels data from the service is empty contact the server to get the data
				/*ChannelsData.getChannelsData({
					successCallback: function(){
						$scope.channels = ChannelsData.getChannels();
						$scope.$broadcast('hideLazyLoad');
					}
				});*/
				$scope.infiniteScrollVertical();
				$scope.closeModal();
			}else{//otherwise set channels object to service data
				$scope.channels = channels;
			}
			
			$scope.closeModalOnRedirect();
			
			if(ChannelsData.canRequestMoreChannels()){
				$scope.showLoadmore = true;
			}else{
				$scope.showLoadmore = false;
			}
			
			BlankStateCache = ChannelsData.getBlankstatecontroller();
			//Check the blank state fields
			if(typeof BlankStateCache === 'object' && BlankStateCache.length !== 0){
				$scope.is_superadmin = BlankStateCache.is_superadmin;
				if($scope.is_superadmin){
					$scope.showBlankstate = true;
				}
				$scope.is_coach_share_active = BlankStateCache.is_coach_share_active;
				$scope.is_smb = BlankStateCache.is_smb;
				$scope.manage_channels = BlankStateCache.urls.manage_channels;
				$scope.manage_courses = BlankStateCache.urls.manage_courses;
				$scope.manage_users = BlankStateCache.urls.manage_users;
				$scope.map_experts = BlankStateCache.urls.map_experts;
				$scope.upload_assets = BlankStateCache.urls.upload_assets;
			}
		};
		$scope.infiniteScrollVertical = function(){//Called on infinite scroll
			$scope.showLazyLoad = true;
			if(!$scope.iScrollRunning){//Checks whether can reqeust more channels or not
				$scope.$broadcast('showLazyLoad');
				$scope.iScrollRunning = true;
			}else{
				$scope.$broadcast('hideLazyLoad');
				$scope.showLoadmore = true;
				return;
			}
		
			ChannelsData.getChannelsData({
				from: ChannelsData.next_from,
				successCallback: function(response){
					$scope.channels = ChannelsData.getChannels();//Setting the scope channels
					$scope.$broadcast('hideLazyLoad');//Hiding lazy load
					$scope.iScrollRunning = false;
				//	angular.element('.placeholder-wrap').hide();
					if(response.channels.length > 0){
						$scope.showLoadmore = true;
					}else{
						$scope.showLoadmore = false;
						$scope.BlankState = response.blank_slate;
						
						//Check the blank state fields
						if($scope.BlankState.length !== 0){
							$scope.is_superadmin = $scope.BlankState.is_superadmin;
							if($scope.is_superadmin){
								$scope.showBlankstate = true;
							}
							$scope.is_coach_share_active = $scope.BlankState.is_coach_share_active;
							$scope.is_smb = $scope.BlankState.is_smb;
							$scope.manage_channels = $scope.BlankState.urls.manage_channels;
							$scope.manage_courses = $scope.BlankState.urls.manage_courses;
							$scope.manage_users = $scope.BlankState.urls.manage_users;
							$scope.map_experts = $scope.BlankState.urls.map_experts;
							$scope.upload_assets = $scope.BlankState.urls.upload_assets;
						}
					}
					$scope.showLazyLoad = false;
				}
			});
			$scope.$emit("infiniteScroll:vertical");//Emit infiniteScroll event for outside use purpose's
		};
		
		/**
		 * Close Modal " View Expert" on click on btn "Close"
		 */
		$scope.closeModal = function(){
			$(document).on('click', '.app7020-channel-experts-dialog .modal-footer .close-btn', function(){
				$(this).closest(".modal-body").dialog2("close");
			});
		};
		
		// Close Modal2 when click to "View channel"
		$scope.closeModalOnRedirect = function() {
			$(document).on('click', '#dialog_content.modal-body.opened .customControls a', function(){
				$(this).closest(".modal-body").dialog2("close");
			});
		};
		
		//Reload view ChannelsList/SingleChannel
		$scope.reloadView = function(e){
			var channelId = $(e.target).data('id');
			var dashletId = $(e.target).data('dashlet');
			$('#mydashboard').dashboard('loadContent', '.dashlet[data-dashlet-id="'+dashletId+'"]', {'channel': channelId}, function(){
		});
	};
		
	}
	DashboardController.$inject = ['$scope', '$http', 'ChannelsData'];//Injecting dependencies
	return DashboardController;//Returning class definition
});