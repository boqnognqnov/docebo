'use strict';

define(function (require) {
	var module = require('angular').module('Channels.Directives', []);
	module.directive('carousel', require('Directives/carousel'));
	module.directive('ddTextCollapse', require('Directives/ReadmoreDirective'));
	module.directive('navigationMenu', require('Directives/NavigationMenuDirective'));
	module.directive('modal', require('Directives/ModalDirective'));
	module.directive('lazyload', require('Directives/LazyLoad'));
	module.directive('blankState', require('Directives/BlankStateDirective'));
	return module;
});