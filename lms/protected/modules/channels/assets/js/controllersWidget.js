'use strict';
define(function (require) {
	var ctrl = require('angular').module('Channels.Controllers', ['Channels.Services', 'ngRoute']);
	ctrl.controller('DashboardController', require('Controllers/DashboardController'));
	ctrl.controller('ChannelSingleWidgetController', require('Controllers/ChannelSingleWidgetController'));
	ctrl.controller('inviteController', require('app7020Controllers/inviteToWatch'));
	ctrl.controller('app7020DeleteAsset', require('app7020Controllers/app7020DeleteAsset'));
	ctrl.config(require('config'));
	ctrl.run(require('run'));
	return ctrl;
});
