var channelsDirectoryModule = channelsScriptPath+'/js';
require.config({
	waitSeconds: 0, 
	baseUrl: channelsDirectoryModule, //app7020Options.assetUrl + '/',
	paths: {
		app7020Controllers: '../../../' + app7020AssetPath + 'js/controllers',
		app7020Modules: '../../../' + app7020AssetPath + 'js/modules',
		app7020Factories: '../../../' + app7020AssetPath + 'js/factory',
		ngOrder: '/themes/spt/js/angularJs/ng-order-object-by.js',
		angular: '/themes/spt/js/angularJs/angular',
		angularResource: '/themes/spt/js/angularJs/angular-resource',
		angularRoute: '/themes/spt/js/angularJs/angular-route',
		angularSanitize: '/themes/spt/js/angularJs/angular-sanitize.min',
		infiniteScroll: channelsDirectoryModule + '/lib/ng-infinite-scroll.min',
		angularClipboard: channelsDirectoryModule + '/lib/angular-clipboard',
		'app7020PDeleteAsset': '../../../' + app7020AssetPath + 'js/plugins/app7020PDeleteAsset',
		'modules/notification': '../../../' + app7020AssetPath + 'js/modules/notification'
	},
	shim: {
		'angular': {'exports': 'angular'},
		'angularRoute': {
			'exports': 'angular',
			'deps': ['angular']
		},
		'angularSanitize': {
			'exports': 'ngSanitize',
			'deps': ['angular']
		},
		'infiniteScroll': {
			'exports': 'infinite-scroll',
			'deps': ['angular']
		},
		'angularClipboard': {
			'exports': 'angularClipboard'
		},
		ngOrder: {
			exports: 'ngOrderObjectBy',
			deps: ['angular']
		},
		'app': {
			deps: [
				'angular', 
				'ngOrder',
				'infiniteScroll',
				'angularRoute',
 				'services',
				'directives',
				'controllers',
				'angularClipboard'
			]
		}
	},
	priority: [
		"angular",
		"angularRoute",
		"angularSanitize",
		"infiniteScroll",
		'ngOrder',
		"angularClipboard",
		'services',
		'directives',
		'controllers',
		'app'
	]
});

