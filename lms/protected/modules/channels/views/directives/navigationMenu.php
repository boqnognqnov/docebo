<div class="tab-navigation {{navbarClass}}">
	<ul class="primaryMenu">
		<li>
			<a href="#/"><?php echo Yii::t('standard', 'All channels'); ?></a>
		</li>
		<?php
		if (PluginManager::isPluginActive('Share7020App')) {
			?>
			<li>
				<a href="#/myChannel"><?php echo Yii::t('standard', 'My channel'); ?></a>
			</li>
			<?php
		}
		?>
			<!--For now is hidden this links-->
<!--		<li>
			<a href="#/watchLater"><?php echo Yii::t('standard', 'Watch later'); ?></a>
		</li>
		<li>
			<a href="#/following"><?php echo Yii::t('standard', 'Following'); ?></a>
		</li>-->
	</ul>
	<a href="#/" class="view-back-arrow"><i class="fa fa-angle-left"></i></a>
	<?php
	if (PluginManager::isPluginActive('Share7020App')) {
		?>
		<ul class="additionLinksMenu">
			<?php
			if (PluginManager::isPluginActive('Share7020App')) {
				?>
				<li style="display:block; opacity: 1; right:79px;" class="fa fa-cloud-upload" rel="tooltip" data-original-title="<?php echo Yii::t('standard', 'Upload a Knowledge Asset'); ?>" data-placement="top" data-toggle="tooltip">
					<a href="<?php echo Docebo::createApp7020Url('assets/index'); ?>"></a>
				</li>
				<?php
			}
			?>
			<li style="display:block; opacity: 1;" class="fa-stack askGuruButton" rel="tooltip" data-original-title="<?php echo Yii::t('standard', 'Ask the Experts!'); ?>" data-placement="top" data-toggle="tooltip">
				<a href="<?php echo Docebo::createApp7020Url('askTheExpert/index', array("#" => "/allQuestions/addQuestion")); ?>"></a>
				<i class="fa fa-user fa-stack-1x"></i>
				<i class="fa fa-comment-o fa-stack-1x"></i>
			</li>
	    </ul>
		<?php
	}
	?>
</div>