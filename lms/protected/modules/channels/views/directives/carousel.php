<div class="swiper-wrapper">
	<div class="swiper-slide" ng-repeat="(key,item) in items track by $id(item)">
		<div  class="col col-tile" ng-class="{'first-child' : $first}" ng-mouseenter="showDots = true" ng-mouseleave="showDots = false">
			<div ng-show="isSetPopStatus(key, 1) || isSetPopStatus(key, 2)" class="tile-overlay">
				<div class="dissmissed text-center">
					<div id="ProgressBar">
						<span class="w70"></span>
					</div>
					<div class="text"><?php echo Yii::t('app7020', 'Thank you') ?>!<br /> <?php echo Yii::t('app7020', 'The asset will be dismissed') ?>!</div>
					<div class="undo-action" >
						<span class="undo" ng-click="undo(key)"><?php echo Yii::t('app7020', 'Undo') ?></span>
					</div>
				</div>
			</div>
			<div class="tile-thumb">
				<div class="thumb-img" style="background: url('{{item.image_uri}}') no-repeat; background-size: cover; background-position: center;"><a target="_parent" ng-href="{{item.can_enter || item.type_id === 'knowledge_asset' ? item.uri:''}}"></a></div>
				<div class="overlay-details">
					<a target="_parent" ng-href="{{item.can_enter || item.type_id === 'knowledge_asset' ? item.uri:''}}" class="label-type">{{item.type_label}}</a>
					<div class="label-right-topcorner">
						<a target="_parent" ng-href="{{item.can_enter || item.type_id === 'knowledge_asset' ? item.uri:''}}">
							<div class="courses-count" ng-if="item.other_fields.courses_count">{{item.other_fields.courses_count}} <?= Yii::t('standard', 'Courses') ?></div>
							<div class="label-new" ng-if="item.is_new"><?= Yii::t('standard', 'New') ?></div>
							<div class="label-duration" ng-if="item.other_fields.duration && item.other_fields.duration != '00:00'">{{item.other_fields.duration}}</div>
						</a>
					</div>
					<div class="label-invititedby" ng-if="item.other_fields.invited_by">
						<a target="_parent" ng-href="{{item.can_enter || item.type_id === 'knowledge_asset' ? item.uri:''}}" class="invite-image" data-toggle="tooltip" data-placement="top" title="<?= Yii::t('standard', 'Invited by:') ?> {{item.other_fields.invited_by}}" data-original-title="<?= Yii::t('standard', 'Invited by:') ?> {{item.other_fields.invited_by}}" rel="tooltip" >
							<span ng-if="item.other_fields.invited_by_url" ng-bind-html="item.other_fields.invited_by_url"></span>
						</a>
					</div>
					<div class="label-left-topcorner" ng-if="item.ribbon.fa_icon">
						<a ng-href="" class="label-left-corner" data-toggle="tooltip" data-placement="top" title="{{item.ribbon.message}}" data-original-title="{{item.ribbon.message}}" rel="tooltip" ng-style="{'color': item.ribbon.text_color, 'border-color': item.ribbon.icon_bg_color + ' transparent transparent transparent'}">
							<i class="fa {{item.ribbon.fa_icon}}"></i>	
						</a>
					</div>
				</div>
			</div>
			<div class="tile-meta">
				<div class="main-metas">
					<span ng-if="item.other_fields.views">{{item.other_fields.views}} <?= Yii::t('standard', 'views') ?></span>
					<span ng-if="item.other_fields.last_viewed">{{item.other_fields.last_viewed}}</span>
					<span ng-if="item.other_fields.language">{{item.other_fields.language}}</span>
					<span ng-if="item.other_fields.level">{{item.other_fields.level}}</span>
					<!-- Rating -->
					<span class="tile-ratings" ng-if="item.rating">
						<a href="#" id="test1" class="popover-link">
							<i class="fa fa-star" ng-if="item.rating >= 4"></i>
							<i class="fa fa-star-half-o" ng-if="item.rating >= 2.5 && item.rating <= 3.5"></i>
							<i class="fa fa-star-o" ng-if="item.rating <= 2"></i>
							{{item.rating}}
						</a>
					</span>
				</div>
				<a target="_parent" ng-href="{{item.can_enter || item.type_id === 'knowledge_asset' ? item.uri:''}}" class="tile-title">{{item.title| cut:true:60:' ...'}}</a>
				<div class="course-status" ng-class="{0:'Subscribed', 1:'progress', 2:'Completed'}[item.other_fields.status_code]" ng-if="item.other_fields.status && !item.other_fields.author">
					{{item.other_fields.status}}
				</div>
				<div class="progress-metas" ng-if="item.other_fields.percentage">
					<span class="progress-percent">{{item.other_fields.percentage}}%</span>
					<div class="progress">
						<div class="progress-bar" role="progressbar" aria-valuenow="{{item.other_fields.percentage}}" aria-valuemin="0" aria-valuemax="100" style="width: {{item.other_fields.percentage}}%">

						</div>
					</div>
				</div>
				<div class="author-metas" ng-if="item.other_fields.author">
					<span><?= Yii::t('webapp', 'by') ?></span>
					<a href="{{item.other_fields.personal_channel}}">{{item.other_fields.author}}</a>
				</div>
				<!-- Actions menu -->
				<span class="dots-link" ng-if="item.actions.length > 0" ng-show="showDots" ng-click="open($event)">...</span>
				<div class="clear-both"></div>
				<div class="dots-menu" ng-if="item.actions.length > 0">
					<div class="bg-overlay"></div>
					<div class="link-overlay">
						<div class="dots-menu-content">
							<ul>
								<li ng-repeat="action in item.actions">
									<div ng-if="action.action_type == 'new_window'">
										<a href="{{action.url}}" target="_blank" style="color: {{action.text_color}};">
											<span class="remove-link"><i class="fa {{action.fa_icon}}"></i></i> {{action.label}}</span>
										</a>
									</div>
									<div ng-if="action.action_type == 'same_window'">
										<a href="{{action.url}}" style="color: {{action.text_color}};">
											<span class="remove-link"><i class="fa {{action.fa_icon}}"></i></i> {{action.label}}</span>
										</a>
									</div>
									<div ng-if="action.action_type == 'modal'">
										<span modal ng-if="action.action_type == 'modal'" ng-class=""  data-classes=" {{action.class}}" data-custom_id="{{action.id}}"
											  data-title="{{action.label}}"  data-url="{{action.action_params.url}}" style="color: {{action.text_color}}">
											<i class="fa {{action.fa_icon}}"></i> {{action.label}}
										</span>
									</div>
									<div ng-if="action.action_type == 'deeplink'">
										<a href="{{action.action_params.url}}" class="copy-url"></a>
										<span clipboard class="not-copied" text="textToCopy" on-copied="success()" on-error="fail(err)" ng-click="getShareurl($event)"><i class="fa {{action.fa_icon}}"></i></i> {{action.label}}</span>
										<span class="copied-success"><i class="fa fa-check color-green"></i></i> <span class="color-green font-weight-medium"><?= Yii::t('standard', 'Link copied') ?></span> <?= Yii::t('standard', 'to clipboard!') ?></span>
									</div>
									<div ng-if="action.action_type == 'pop'">
										<span class="dismiss-button" ng-click="setPopStatus(key, 1); dismiss(key)"  data-classes=" {{action.class}}" data-custom_id="{{action.id}}"
											  data-title="{{action.label}}"  data-url="{{action.action_params.url}}" style="color: {{action.text_color}}">
											<i class="fa {{action.fa_icon}}"></i> {{action.label}}
										</span>
									</div>
								</li>

							</ul>
							<div class="bottom-part">
								<span ng-click="close($event)">...</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="swiper-slide lazyload-swiper placeholder-wrap">
		<div class="col col-tile">
			<div class="tile-thumb">
				<div class="thumb-img"><i class="fa fa-spinner"></i></div>
			</div>
			<div class="tile-meta">
				<div class="main-metas">
					<span></span>
				</div>
				<a href='#' class="tile-title"></a>
				<div class="author-metas">
				</div>
			</div>
		</div>
	</div>
</div>
<!-- If we need navigation buttons -->
<div class="swiper-button-prev" ng-show="show"><i class="fa fa-angle-left"></i></div>
<div class="swiper-button-next" ng-show="show"><i class="fa fa-angle-right"></i></div>