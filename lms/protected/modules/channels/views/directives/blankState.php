<!-- Blank state page for channels dashboard -->
<div class="blank-state-channels">
	<div class="welcome-icon">
		<img src="<?php echo Yii::app()->getModule("channels")->getAssetsUrl() . "/images/razzzo.png"; ?>" alt="Welcome to your Docebo" />
	</div>
	<div class="blank-inner-wrap">
		<!-- Main msg content part -->
		<div class="content-part">
			<h2 class="welcome-title"><?php echo Yii::t('standard','Welcome to Docebo!'); ?></h2>
			<div class="heading-text">
				<?php echo Yii::t('standard','Ready to start your learning project? We’re excited as you are! Let’s get started'); ?>
			</div>
			<div class="operation-wrap">
				<div class="operation-column-2">
					<i class="fa fa-users"></i>
					<div class="operation-text">
						<?php echo Yii::t('standard','Create, import and organize users into branches or groups, and enroll them in your courses.'); ?>
					</div>
					<a href="{{manage_users}}" class="operation-button"><?php echo Yii::t('standard','Manage users'); ?></a>
				</div>
				<div class="operation-column-2">
					<i class="fa fa-television"></i>
					<div class="operation-text">
						<?php echo Yii::t('standard','Create channels to aggregate formal and informal content for specific audiences.'); ?>
					</div>
					<a href="{{manage_channels}}" class="operation-button"><?php echo Yii::t('standard','Manage Channels'); ?></a>
				</div>
				<div class="operation-column-2">
					<i class="fa fa-book"></i>
					<div class="operation-text">
						<?php echo Yii::t('standard','Upload your courses and formal training material, such as SCORM, AICC, xAPI, videos or slides.'); ?>
					</div>
					<a href="{{manage_courses}}" class="operation-button"><?php echo Yii::t('standard','Manage Courses'); ?></a>
				</div>
				<div class="operation-column-2" ng-show="is_coach_share_active">
					<span class="map-icon">
						<i class="fa fa-user fa-stack-1x"></i>
						<i class="fa fa-comment-o fa-stack-1x"></i>
					</span>
					<div class="operation-text">
						<?php echo Yii::t("standard","Let your employees ask questions and get answers from your experts. Map experts to specific audiences using Channels."); ?>
					</div>
					<a href="{{map_experts}}" class="operation-button"><?php echo Yii::t('standard','Map your experts'); ?></a>
				</div>
				<div class="operation-column-2" ng-show="is_coach_share_active">
					<i class="fa fa-cloud-upload"></i>
					<div class="operation-text">
						<?php echo Yii::t('standard','Enable your employees to upload and share informal learning assets across your organization. Peer-review, validate and create knowledge capital.'); ?>
					</div>
					<a href="{{upload_assets}}" class="operation-button"><?php echo Yii::t('standard','Upload & Share Assets'); ?></a>
				</div>
				<div class="clear-float"></div>
			</div>
		</div>
		<!-- Bottom msg part -->
		<div class="bottom-part">
			<div class="left-part">
				<div class="blue-heading" ng-show="is_smb"><?php echo Yii::t('standard','Get in touch with our experts'); ?></div>
				<div class="blue-heading" ng-show="!is_smb"><?php echo Yii::t('standard', 'Get a free, personalized demo of Docebo'); ?></div>
				<div ng-show="is_smb"><?php echo Yii::t('standard','Have questions about your Docebo trial? Need help? <strong>Contact Learning and Support at any time during your trial period</strong>.'); ?></div>
				<div ng-show="!is_smb"><?php echo Yii::t('standard', "Whether you’ll use Docebo for training your employees, partners or customers, our team would be happy to <strong>custom-tailor a live demo to suit your elearning needs</strong>. Request an appointment today!"); ?></div>
			</div>
			<div class="right-part">
				<a class="open-dialog contact-us enterprise" href="<?= Docebo::createLmsUrl('site/axHelpDesk', array(
				'only_sales' => '1')); ?>" ng-show="!is_smb" data-dialog-class="helpdesk-modal users-info-dialogs" closeOnOverlayClick="false" closeOnEscape="false" rel="helpdesk-modal"><?php echo Yii::t('standard','SCHEDULE A DEMO'); ?></a>
				<a class="open-dialog contact-us" href="<?= Docebo::createLmsUrl('site/axHelpDesk', array(
				'only_sales' => '1')); ?>" ng-show="is_smb" data-dialog-class="helpdesk-modal users-info-dialogs" closeOnOverlayClick="false" closeOnEscape="false" rel="helpdesk-modal">
					<span><?= Yii::t('standard','Contact us') ?></span><br>
				</a>

			</div>
		</div>
	</div>
</div>

<script>
	$(document).controls();
</script>