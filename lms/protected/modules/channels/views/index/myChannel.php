<?php if (PluginManager::isPluginActive('Share7020App')): ?>
	<!-- Channel main wrapper  -->
	<div ng-init="init()" ng-cloak class="myChannels-wrapper clearfix" infinite-scroll="infiniteScrollVertical()">

		<!-- Personal information of channel -->
		<div class="my-channel-information-wrapper">
			<div class="profile-info">
				<div  class="avatar" ng-bind-html="channelData.other_fields.avatar"></div>
				<p class="name">{{channelData.name}}</p>
				<label for="descriptionInput" ng-hide="contentEditable" class="description" ng-bind="channelData.description" ng-keydown="updateDescription($event)" ng-blur="updateDescription($event)" contenteditable="{{contentEditable}}" ng-click="switchToEditable($event)" spellcheck="false"
				   placeholder="<?php echo Yii::t('standard', 'Click here to add a description of your channel!'); ?>">{{ channelData.description}}</label>
				<input name="description" id="descriptionInput" tabindex="0" ng-show = "contentEditable" class="description" ng-bind="channelData.description" ng-value="channelData.description" ng-keydown="updateDescription($event)" ng-blur="updateDescription($event)" contenteditable="{{contentEditable}}" ng-click="switchToEditable($event)" spellcheck="false"
					   placeholder="<?php echo Yii::t('standard', 'Click here to add a description of your channel!'); ?>" type="text">
			</div>

			<div ng-class="!channelUserContribution.length && !channelMyInReview.length && !channelMyContribution.length && channelData != null ? 'visible' : 'hidden' "  ng-if="!channelUserContribution.length && !channelMyInReview.length && !channelMyContribution.length" class="myChannel-blank text-center ng-cloak">
				<img src="<?php echo Yii::app()->theme->baseUrl.'/images/app7020/my-channel-blank.png' ?>" alt="" width="378" height="214">
				<h2><?php echo Yii::t('app7020', 'Hey it looks empty here!'); ?></h2>
				<h3><?php echo Yii::t('app7020', 'Upload a new asset and help sharing the Knowledge'); ?></h3>
				<?php if (PluginManager::isPluginActive('Share7020App')) { ?>
							<a class="fa fa-cloud-upload" rel="tooltip" data-original-title="<?php echo Yii::t('standard', 'Upload a Knowledge Asset'); ?>" data-placement="left" data-toggle="tooltip" href="<?php echo Docebo::createApp7020Url('assets/index'); ?>"></a>
				<?php } ?>
			</div>

			<div ng-if="channelUserContribution.length || channelMyInReview.length || channelMyContribution.length" class="information-bar-wrapper">
				<div class="left-side">
					<span class="contributions"><strong>{{channelData.items_count}}</strong><?php echo Yii::t('standard', 'Contributions'); ?></span>
				</div>
				<div class="right-side">
					<!--Coming soon for this settings button ;) -->
					<!--<i class="fa fa-sliders"></i>-->
				</div>
			</div>
			<div class="clear-both"></div>
		</div>

		<!-- Channel In Review Carousel -->
		<div class="channel-wrap c_two col-md-12 my-channel-inReview" ng-if="channelMyInReview.length">
			<div class="header-channel col col-12">
				<div class="col col-10-main">
					<div class="col col-2 title-label">
						<span class="label-icon"><i class="fa fa-pencil-square-o"></i></span>
					</div>
					<div class="col col-10 title-details">
						<h3><?php echo Yii::t('standard', 'In Review'); ?></h3>
						<p><?php echo Yii::t('standard', 'Assets you uploaded and that are currently under Experts\' Peer Review'); ?></p>
					</div>
				</div>
			</div>
			<carousel data-minimum-items="5" id="channel-0" data-id="channel-0" data="{{channelMyInReview}}" data-endpoint='user_channel_in_review' class="tiles-wrap swiper-container"></carousel>
		</div>
		<!-- My Contributions Channel items -->
		<div class="channel-wrap c_two col-md-12 my-channel-contributions" ng-if="channelMyContribution.length">
			<div class="header-channel col col-12">
				<div class="col col-10-main">
					<div class="col col-2 title-label">
						<span class="label-icon"><i class="fa fa-check"></i></span>
					</div>
					<div class="col col-10 title-details">
						<h3><?php echo Yii::t('standard', 'My contributions'); ?></h3>
						<p><?php echo Yii::t('standard', 'My published, unpublished and private contributions'); ?></p>
					</div>
				</div>
			</div>
			<div class="items-wrapper">
				<div class="col col-tile" ng-repeat="item in channelMyContribution track by item.id" ng-mouseenter="showDots = true" ng-mouseleave="showDots = false">
					<!-- Item top overflay details and image -->
					<div class="tile-thumb">
						<div class="thumb-img" style="background: url('{{item.image_uri}}') no-repeat; background-size: cover;"><a href='{{item.uri}}'></a></div>
						<div class="overlay-details">
							<a href='{{item.uri}}' class="label-type">{{item.type_label}}</a>
							<div class="label-right-topcorner">
								<a href='{{item.uri}}'>
									<div class="courses-count" ng-if="item.other_fields.courses_count">{{item.other_fields.courses_count}} <?= Yii::t('standard','Courses') ?></div>
									<div class="label-new" ng-if="item.is_new"><?= Yii::t('standard','New') ?></div>
									<div class="label-duration" ng-if="item.other_fields.duration && item.other_fields.duration != '00:00'">{{item.other_fields.duration}}</div>
								</a>
							</div>
							<div class="label-invititedby" ng-if="item.other_fields.invited_by">
								<a href="{{item.uri}}" class="invite-image" data-toggle="tooltip" data-placement="top" title="<?= Yii::t('standard','Invited by:') ?> {{item.other_fields.invited_by}}" data-original-title="<?= Yii::t('standard','Invited by:') ?> {{item.other_fields.invited_by}}" rel="tooltip" >
									<span ng-if="item.other_fields.invited_by_url" ng-bind-html="item.other_fields.invited_by"></span>
								</a>
							</div>
							<div class="label-left-topcorner" ng-if="item.ribbon.fa_icon">
								<a href="{{item.uri}}" class="label-left-corner" data-toggle="tooltip" data-placement="top" title="{{item.ribbon.message}}" data-original-title="{{item.ribbon.message}}" rel="tooltip" style="color: {{item.ribbon.text_color}}; border-color: {{item.ribbon.icon_bg_color}} transparent transparent transparent;">
									<i class="fa {{item.ribbon.fa_icon}}"></i>	
								</a>
							</div>
						</div>
					</div>

					<!-- Item meta details -->
					<div class="tile-meta">
						<div class="main-metas">
							<span ng-if="item.other_fields.views">{{item.other_fields.views}} <?= Yii::t('standard','views') ?></span>
							<span ng-if="item.other_fields.last_viewed">{{item.other_fields.last_viewed}}</span>
							<span ng-if="item.other_fields.language">{{item.other_fields.language}}</span>
							<span ng-if="item.other_fields.level">{{item.other_fields.level}}</span>
							<!-- Rating -->
							<span class="tile-ratings" ng-if="item.rating">
								<a href="#" id="test1" class="popover-link">
									<i class="fa fa-star" ng-if="item.rating >= 4"></i>
									<i class="fa fa-star-half-o" ng-if="item.rating >= 2.5 && item.rating <= 3.5"></i>
									<i class="fa fa-star-o" ng-if="item.rating <= 2"></i>
									{{item.rating}}
								</a>
							</span>
						</div>
						<a href="{{item.uri}}" ng-bind-html="item.title" class="tile-title">{{item.title | cut:true:55:' ...'}}</a>
						<div class="course-status {{item.other_fields.status}}" ng-if="item.other_fields.status && !item.other_fields.author">
							{{item.other_fields.status}}
						</div>
						<div class="progress-metas" ng-if="item.other_fields.percentage">
							<span class="progress-percent">{{item.other_fields.percentage}}%</span>
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="{{item.other_fields.percentage}}" aria-valuemin="0" aria-valuemax="100" style="width: {{item.other_fields.percentage}}%">

								</div>
							</div>
						</div>
						<div class="author-metas" ng-if="item.other_fields.author">
							<span><?= Yii::t('standard','by') ?></span>
							<a href="{{item.other_fields.personal_channel}}">{{item.other_fields.author}}</a>
						</div>

						<!-- Actions menu -->
						<span class="dots-link" ng-if="item.actions.length > 0" ng-show="showDots" ng-click="open($event)">...</span>
						<div class="clear-both"></div>
						<div class="dots-menu" ng-if="item.actions.length > 0">
							<div class="bg-overlay"></div>
							<div class="link-overlay">
								<div class="dots-menu-content">
									<ul>
										<li ng-repeat="action in item.actions">
											<div ng-if="action.action_type == 'new_window'">
												<a href="{{action.url}}" target="_blank" style="color: {{action.text_color}};">
													<span class="remove-link"><i class="fa {{action.fa_icon}}"></i></i> {{action.label}}</span>
												</a>
											</div>
											<div ng-if="action.action_type == 'same_window'">
												<a href="{{action.action_params.url}}" style="color: {{action.text_color}};">
													<span class="remove-link"><i class="fa {{action.fa_icon}}"></i></i> {{action.label}}</span>
												</a>
											</div>
											<div ng-if="action.action_type == 'modal'">
												<span modal ng-if="action.action_type == 'modal'" ng-class=""  data-classes=" {{action.class}}" data-custom_id="{{action.id}}"
													  data-title="{{action.label}}"  data-url="{{action.action_params.url}}" style="color: {{action.text_color}}">
													<i class="fa {{action.fa_icon}}"></i> {{action.label}}
												</span>
											</div>
											<div ng-if="action.action_type == 'deeplink'">
												<a href="{{action.action_params.url}}" class="copy-url"></a>
												<span clipboard class="not-copied" text="textToCopy" on-copied="success()" on-error="fail(err)" ng-click="getShareurl($event)"><i class="fa {{action.fa_icon}}"></i></i> {{action.label}}</span>
												<span class="copied-success"><i class="fa fa-check color-green"></i></i> <span class="color-green font-weight-medium"><?= Yii::t('standard', 'Link copied') ?></span> <?= Yii::t('standard', 'to clipboard!') ?></span>
											</div>
										</li>

									</ul>
									<div class="bottom-part">
										<span ng-click="close($event)">...</span>
									</div>
								</div>
							</div>
						</div>

					</div>
				<div class="clear-both"></div>
			</div>
		</div>
	</div>
	<?php if($this->uniqueid != 'channelsManagement' && Yii::app()->user->isGodadmin && Docebo::isTrialPeriod() && !CoreSettingUser::getSettingByUser(Yii::app()->user->idst,'cs_splash_screen')) {?>
		<script>
			openDialog(null, '', '<?= Docebo::createApp7020Url('assets/showSplashScreen') ?>', 'app7020-splash-screen', 'modal-splash-screen');
		</script>

	<?php  }?>
<?php endif; ?>