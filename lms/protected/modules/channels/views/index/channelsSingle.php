<!-- Channel view all items -->
<div class="channels-wrapper viewall-page" infinite-scroll-immediate-check="true" infinite-scroll="infiniteScrollVertical()" infinite-scroll-listen-for-event="inifniteScroll:vertical">
	<!-- Channel header -->
	<div class="header-channel col col-12">
		<div class="col col-10-main">
			<div class="col col-2 title-label">
				<div ng-if="channel.fa_icon">
					<span class="label-icon" style="color:{{channel.fa_icon_color}};background-color: {{channel.icon_bg_color}}"><i class="fa {{channel.fa_icon}}"></i></span>
				</div>
			</div>
			<div class="col col-10 title-details">
                <h3 ng-bind-html="channel.name">{{channel.name}}</h3>
			</div>
			<div class="col col-10 description-details">
				<p class="desc"  dd-text-collapse dd-text-collapse-max-length="450" dd-text-collapse-text="{{channel.description}}"></p>
			</div>
			<div class="col col-12 channel-details" ng-if="channel.counters">
<!--				<div class="col details-col2">
					<span class="blue-color">{{channel.counters.count}}</span>
					<span>{{channel.counters.text}}</span>
				</div>-->
				<div class="col details-col2" ng-if="channel.other_fields.count_experts">
					<span class="blue-color">{{channel.other_fields.count_experts}}</span>
					<span ng-if="channel.other_fields.count_experts"><?= Yii::t('standard', 'Experts') ?></span>
					<a href="" class="experts view-expert"  modal  data-title="<?= Yii::t('standard', 'View assigned experts') ?>" id="modal_{{channel.id}}" data-url="index.php?r=channels/index/channelExperts&id={{channel.id}}"><?= Yii::t('standard', 'View assigned experts') ?></a>
				</div>

			</div>
		</div>
	</div>
	<!-- Channel items wrapper -->
	<div class="items-wrapper">
		<div ng-if="channel.items.length > 0" class="col col-tile" ng-repeat="(key,item) in channel.items track by $index" ng-mouseenter="showDots = true" ng-mouseleave="showDots = false">
			<div ng-show="isSetPopStatus(key, 1) || isSetPopStatus(key, 2)" class="tile-overlay">
				<div class="dissmissed text-center">
					<div id="ProgressBar">
						<span class="w70"></span>
					</div>
					<div class="text"><?php echo Yii::t('app7020', 'Thank you') ?>!<br /> <?php echo Yii::t('app7020', 'The asset will be dismissed') ?>!</div>
					<div class="undo-action" >
						<span class="undo" ng-click="undo(key)"><?php echo Yii::t('app7020', 'Undo') ?></span>
					</div>
				</div>
			</div>
			<!-- Item top overflay details and image -->
			<div class="tile-thumb">
				<div class="thumb-img" style="background: url('{{item.image_uri}}') no-repeat center; background-size: cover;"><a ng-href="{{item.can_enter || item.type_id === 'knowledge_asset' ? item.uri:''}}"></a></div>
				<div class="overlay-details">
					<a ng-href="{{item.can_enter || item.type_id === 'knowledge_asset' ? item.uri:''}}" class="label-type">{{item.type_label}}</a>
					<div class="label-right-topcorner">
						<a href='{{item.uri}}'>
							<div class="courses-count" ng-if="item.other_fields.courses_count">{{item.other_fields.courses_count}} <?= Yii::t('standard', 'Courses') ?></div>
							<div class="label-new" ng-if="item.is_new"><?= Yii::t('standard', 'New') ?></div>
							<div class="label-duration" ng-if="item.other_fields.duration && item.other_fields.duration != '00:00'">{{item.other_fields.duration}}</div>
						</a>
					</div>
					<div class="label-invititedby" ng-if="item.other_fields.invited_by">
						<a href="" class="invite-image" data-toggle="tooltip" data-placement="top" title="<?= Yii::t('standard', 'Invited by:') ?> {{item.other_fields.invited_by}}" data-original-title="<?= Yii::t('standard', 'Invited by:') ?> {{item.other_fields.invited_by}}" rel="tooltip" >
							<span ng-if="item.other_fields.invited_by_url" ng-bind-html="item.other_fields.invited_by_url"></span>
						</a>
					</div>
					<div class="label-left-topcorner" ng-if="item.ribbon.fa_icon">
						<a href="" class="label-left-corner" data-toggle="tooltip" data-placement="top" title="{{item.ribbon.message}}" data-original-title="{{item.ribbon.message}}" rel="tooltip" style="color: {{item.ribbon.text_color}}; border-color: {{item.ribbon.icon_bg_color}} transparent transparent transparent;">
							<i class="fa {{item.ribbon.fa_icon}}"></i>	
						</a>
					</div>
				</div>
			</div>

			<!-- Item meta details -->
			<div class="tile-meta">
				<div class="main-metas">
					<span ng-if="item.other_fields.views">{{item.other_fields.views}} <?= Yii::t('standard', 'views') ?></span>
					<span ng-if="item.other_fields.last_viewed">{{item.other_fields.last_viewed}}</span>
					<span ng-if="item.other_fields.language">{{item.other_fields.language}}</span>
					<span ng-if="item.other_fields.level">{{item.other_fields.level}}</span>
					<!-- Rating -->
					<span class="tile-ratings" ng-if="item.rating">
						<a href="#" id="test1" class="popover-link">
							<i class="fa fa-star" ng-if="item.rating >= 4"></i>
							<i class="fa fa-star-half-o" ng-if="item.rating >= 2.5 && item.rating <= 3.5"></i>
							<i class="fa fa-star-o" ng-if="item.rating <= 2"></i>
							{{item.rating}}
						</a>
					</span>
				</div>
				<a ng-href="{{item.can_enter || item.type_id === 'knowledge_asset' ? item.uri:''}}" class="tile-title">{{item.title| cut:true:55:' ...'}}</a>
				<div class="course-status {{item.other_fields.status}}" ng-if="item.other_fields.status && !item.other_fields.author">
					{{item.other_fields.status}}
				</div>
				<div class="progress-metas" ng-if="item.other_fields.percentage">
					<span class="progress-percent">{{item.other_fields.percentage}}%</span>
					<div class="progress">
						<div class="progress-bar" role="progressbar" aria-valuenow="{{item.other_fields.percentage}}" aria-valuemin="0" aria-valuemax="100" style="width: {{item.other_fields.percentage}}%">

						</div>
					</div>
				</div>
				<div class="author-metas" ng-if="item.other_fields.author">
					<span><?= Yii::t('standard', 'by') ?></span>
					<a href="{{item.other_fields.personal_channel}}">{{item.other_fields.author}}</a>
				</div>

				<!-- Actions menu -->
				<span class="dots-link" ng-if="item.actions.length > 0" ng-show="showDots" ng-click="open($event)">...</span>
				<div class="clear-both"></div>
				<div class="dots-menu" ng-if="item.actions.length > 0">
					<div class="bg-overlay"></div>
					<div class="link-overlay">
						<div class="dots-menu-content">
							<ul>
								<li ng-repeat="action in item.actions">
									<div ng-if="action.action_type == 'new_window'">
										<a href="{{action.url}}" target="_blank" style="color: {{action.text_color}};">
											<span class="remove-link"><i class="fa {{action.fa_icon}}"></i></i> {{action.label}}</span>
										</a>
									</div>
									<div ng-if="action.action_type == 'same_window'">
										<a href="{{action.url}}" style="color: {{action.text_color}};">
											<span class="remove-link"><i class="fa {{action.fa_icon}}"></i></i> {{action.label}}</span>
										</a>
									</div>
									<div ng-if="action.action_type == 'modal'">
										<span modal ng-if="action.action_type == 'modal'" ng-class=""  data-classes=" {{action.class}}" data-custom_id="{{action.id}}"
											  data-title="{{action.label}}"  data-url="{{action.action_params.url}}" style="color: {{action.text_color}}">
											<i class="fa {{action.fa_icon}}"></i> {{action.label}}
										</span>
									</div>
									<div ng-if="action.action_type == 'deeplink'">
										<a href="{{action.action_params.url}}" class="copy-url"></a>
										<span clipboard class="not-copied" text="textToCopy" on-copied="success()" on-error="fail(err)" ng-click="getShareurl($event)"><i class="fa {{action.fa_icon}}"></i></i> {{action.label}}</span>
										<span class="copied-success"><i class="fa fa-check color-green"></i></i> <span class="color-green font-weight-medium"><?= Yii::t('standard', 'Link copied') ?></span> <?= Yii::t('standard', 'to clipboard!') ?></span>
									</div>
									<div ng-if="action.action_type == 'pop'">
										<span ng-class="" ng-click="setPopStatus(key, 1);dismiss(key)"  data-classes=" {{action.class}}" data-custom_id="{{action.id}}"
											  data-title="{{action.label}}"  data-url="{{action.action_params.url}}" style="color: {{action.text_color}}">
											<i class="fa {{action.fa_icon}}"></i> {{action.label}}
										</span>
									</div>
								</li>

							</ul>
							<div class="bottom-part">
								<span ng-click="close($event)">...</span>
							</div>
						</div>
					</div>
				</div>

			</div>


		</div>
		<div ng-if="error" class="">{{error}}</div>
		<lazyload type="items"></lazyload>
	</div>
</div>