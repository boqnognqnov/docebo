<!-- Channel main wrapper  -->
<div class="channels-wrapper" ng-init="init()" infinite-scroll="infiniteScrollVertical()">
	<div class="channel-wrap c_two col-md-12" ng-repeat="channel in channels track by $index">
		<div class="header-channel col col-12">
			<div class="col-10-main">
				<div class="col-2 title-label">
					<div ng-if="channel.fa_icon">
						<span class="label-icon" ng-style="{'color': channel.fa_icon_color, 'background-color': channel.icon_bg_color}"><i class="fa {{channel.fa_icon}}"></i></span>
					</div>
				</div>
				<div class="title-details">
					<h3>
                        <a href="#/channel/{{channel.id}}" class="view-more" ng-bind="channel.name">
							{{channel.name}}
							<i class="fa fa-angle-right"></i>
						</a>
					</h3>
                    <div class="descriptionchannel" ng-bind-html="channel.description">{{channel.description}} </div>
				</div>
			</div>
			<div class="col col-2 right-align">
				<div class="experts" ng-if="channel.other_fields.count_experts" modal  data-title="<?= Yii::t('standard','View experts') ?>" id="modal_{{channel.id}}" data-url="index.php?r=channels/index/channelExperts&id={{channel.id}}"><i class="fa fa-users"></i> {{channel.other_fields.count_experts}} <?= Yii::t('standard','experts') ?></div>
			</div>
		</div>
		<div ng-if="channel.items" ng-mouseenter="channel.items.length > 5 ? show = true : show = false" ng-mouseleave="show = false">
			<carousel data-id="channel-{{channel.id}}" data="{{channel.items}}" class="tiles-wrap swiper-container"></carousel>
		</div>
	</div>

	<!-- Clearfix -->
	<div class="clear-both"></div>
</div>
<lazyload type="channels"></lazyload>

<!-- Blank state for channels dashboard -->
<blank-state ng-show="showBlankstate"></blank-state>