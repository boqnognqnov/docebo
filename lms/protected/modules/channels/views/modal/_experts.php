<div class="app7020-channel-experts-dialog">
	<div id="modalContainer" class="row-fluid">
		<div class="heading">
			<?php echo Yii::t('standard', '<b>{n} expert</b> is assigned to this channel|<b>{n} experts</b> are assigned to this channel', $total); ?>
		</div>
		<?php $this->widget('common.widgets.ComboListView', $listViewParams); ?>
	</div>
	<div class="modal-footer">
		<input id="closebutton" class="close-btn" type="button" value="<?php echo Yii::t('standard', '_CLOSE'); ?>"/>
	</div>
</div>