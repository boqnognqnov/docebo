<div class="single-item-expert">
	<div class="">
	<?php
	$this->widget('common.widgets.App7020Avatar', array('userId' => $data['idUser']))
	?>
	</div>
	<div class="content"><div class="expertName">
			<?php
			if (!empty($data['firstname']) && !empty($data['lastname'])) {
				echo $data['firstname'] . ' ' . $data['lastname'];
			} else {
				echo substr($data['userid'], 1);
			}
			?></div></div>
    <div class="customControls">
		<?php
		echo CHtml::link(Yii::t('standard', 'View channel'), Docebo::createLmsUrl('channels/index', array("#" => "/pchannel/" . $data['idUser'])));
		?>
    </div>
</div>