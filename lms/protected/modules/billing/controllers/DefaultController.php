<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */

class DefaultController extends BillingBaseController {


	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {

		return array(

			array('deny',
				'expression' => '!BrandingWhiteLabelForm::isMenuVisible()',
				'actions' => array(
					 'plan', 'upgrade'
				),
				'deniedCallback' => array($this, 'accessDeniedCallbackButton'),
				'message' => Yii::t('standard', 'internal error - contact us'),
			),

			array('deny',
				'expression' => 'Yii::app()->controller->erp_installation_id == ""
					|| Yii::app()->controller->erp_key == ""
					|| Yii::app()->controller->erp_secret_key == ""
					|| !BrandingWhiteLabelForm::isMenuVisible()
									',
				'deniedCallback' => array($this, 'accessDeniedCallback'),
				'message' => Yii::t('standard', 'internal error - contact us'),
			),

			array('allow',
				'users' => array('@'),
				'expression' => 'Yii::app()->user->isGodAdmin',
			),

			// Deny by default
			array('deny', // if some permission is wrong -> the script goes here
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
				'message' => Yii::t('course', '_NOENTER'),
			),
		);

	}

	public function accessDeniedCallbackButton() {
		if(Docebo::isPlatformExpired()){
			Yii::app()->user->logout();
			$this->redirect(Yii::app()->createAbsoluteUrl('site/index', array('error' => 6)));
		}

	}


	/**
	 *  Main action, though it does nothing really
	 */
	public function actionIndex() {
		$this->render('index');
	}


	/**
	 * Return content for the Main Menu -> Buy More submenu
	 *
	 */
	public function actionAxBuyMenuContent() {

		if (Docebo::isTrialPeriod()) {
			echo 'Trial LMS';
			Yii::app()->end();
		}

		try {
			$apiParams = array(
				'installation_id' => $this->erp_installation_id,
				'current_installation_only' => true
			);

			if(isset(Yii::app()->request->cookies['reseller_cookie']))
				$apiParams['request_from_partner'] = true;

			$comboInfo = ErpApiClient::apiErpGetComboInfo($apiParams);
			$comboInfo = CJSON::decode($comboInfo);
			if ($comboInfo['success'] == false) {
				throw new CException(Yii::t('standard', 'internal error - contact us') . $comboInfo['msg']);
			}

			$lmsOrderInfo = $comboInfo['lms_order_info'];
			$billingInfo = $comboInfo['billing_info'];
			$billingInfo['data'] = CJSON::decode($billingInfo['data']);
			$customerInfo = $comboInfo['customer_info'];


			$countriesArray = ErpApiClient::getErpCountriesArr();

			//Yii::log('ERP API Call result:', 'debug', __METHOD__);
			//Yii::log(print_r($comboInfo, true), 'debug');

			if (!Docebo::isTrialPeriod()) {
				$installtion_id = $this->erp_installation_id;
				$params = array(
					'upgradeUrl' => Docebo::createLmsUrl('billing/default/upgrade'),
					'editCCardInfoUrl' => Docebo::createLmsUrl('billing/default/upgrade', array('edit_ccard' => 1)),
					'downgradeUrl' => Docebo::createLmsUrl('billing/default/axDowngrade'),
					'cancelUrl' => Docebo::createLmsUrl('billing/default/axCancel', array('installtion_id' => $installtion_id)),
					'lmsOrderInfo' => $lmsOrderInfo,
					'billingInfo' => $billingInfo,
					'customerInfo' => $customerInfo,
					'activeUsers' => Docebo::getActiveUsers(),
					'maxUsers' => Settings::get('max_users', 0),
					'countriesArray' => $countriesArray,
					'invoiceListUrl' => Docebo::createLmsUrl('billing/default/invoiceList'),
					'activeUsersReportUrl' => Docebo::createLmsUrl('billing/default/activeUsersReport'),
				    'currentPlanString' => $this->getCurrentPlanString($comboInfo),
				);
			}

			$html = $this->renderPartial('_buy_menu_content', $params, true);

			echo $html;
			Yii::app()->end();

		} catch (CException $e) {
			echo '<div class="" style="color: red; padding: 10px;">' . $e->getMessage() . '</div>';
			Yii::app()->end();
		}


	}


	/**
	 * Buy Plan (from trial status)
	 *
	 */
	public function actionPlan() {
		if((Settings::get('enable_user_billing', 'on') != 'on' && !isset(Yii::app()->request->cookies['reseller_cookie'])) || Settings::get('demo_platform', 0) == 1)
			$this->redirect(Docebo::createLmsUrl("site/index"), true);
		$this->forward('upgrade');
	}

	/**
	 * Buy Plan (from trial status)
	 *
	 */
	public function actionAxSetReinsertCCDataPopup() {
		$status = Yii::app()->request->getParam("status", "hide");
		if (isset(Yii::app()->session['reinsert-cc-data']) && $status == 'hide') {
			Yii::app()->session['reinsert-cc-data'] = false;
		}

		if (isset(Yii::app()->session['reinsert-cc-data']) && $status == 'unset') {
			unset(Yii::app()->session['reinsert-cc-data']);
		}
	}

	/**
	 * Upgrade plan of an active LMS
	 */
	public function actionUpgrade() {

		$edit_ccard = Yii::app()->request->getParam("edit_ccard", false);

		$envType = Docebo::environmentType();

		$activeUsers = Docebo::getActiveUsers();
		$freeUsers = Settings::get('free_users', 5);
		$time = time();
		$installInfo = $activeUsers . '_' . $freeUsers;

		$hashKey = $this->getOrderModuleHash($this->erp_installation_id, $installInfo, $time);

		if ($hashKey === false) {
			Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE') . ' (invalid hash)');
			$this->redirect(Docebo::createLmsUrl('//site'));
		}

		if ($edit_ccard)
			$orderUrl = $this->getOrderModuleUrl(true);
		else
			$orderUrl = $this->getOrderModuleUrl();

		$orderUrl .= '&id=' . $this->erp_installation_id .
			'&info=' . $installInfo . '&time=' . $time . '&key=' . $hashKey
			. '&user_email=' . Yii::app()->user->email;

		if(isset(Yii::app()->request->cookies['reseller_cookie']->value))
			$orderUrl .= '&reseller_cookie=' . Yii::app()->request->cookies['reseller_cookie']->value;

		// Check if this is to edit Credit Card info only?
		if ($edit_ccard) {
			$orderUrl .= '&upgcci=1';
			$iframeHeight = 610;
			$html = $this->renderPartial('upgrade', array(
				'orderUrl' => $orderUrl,
				'iframeHeight' => $iframeHeight,
				'inDialog' => true,
				'title' => Yii::t('userlimit', '_UPGRADE_CCARD_INFO'),
			), true);
			echo $html;
		} else {
			$this->layout = '//layouts/erp';
			$this->render('upgrade', array(
				'orderUrl' => $orderUrl,
			));
		}

	}


	/**
	 * Downgrade a plan of an active LMS subscription
	 */
	public function actionAxDowngrade() {

		// @TODO get from configs
		$envType = 'saas';

		$activeUsers = CoreUser::getUsersCount();
		$freeUsers = Settings::get('free_users', 5);
		$time = time();
		$installInfo = $activeUsers . '_' . $freeUsers;

		$hashKey = $this->getOrderModuleHash($this->erp_installation_id, $installInfo, $time);

		if ($hashKey === false) {
			$html = $this->renderPartial('../common/_dialog2_error', array('message' => Yii::t('standard', '_OPERATION_FAILURE') . ' (invalid hash)'
			), true);
			echo $html;
			Yii::app()->end();
		}

		$orderUrl = $this->getOrderModuleUrl($envType, 'downgrade');
		$orderUrl .= '&id=' . $this->erp_installation_id .
			'&info=' . $installInfo . '&time=' . $time . '&key=' . $hashKey;

		$html = $this->renderPartial('_downgrade', array(
			'orderUrl' => $orderUrl,
		), true);

		echo $html;

	}


	/**
	 * Terminate subscription FORM dialog and action
	 *
	 */
	public function actionAxCancel() {
		$api_res = CJSON::decode(ErpApiClient::getIsPendingRenewal(array('installation_id' => $this->erp_installation_id)));
		if ($api_res['success'] && $api_res['data'] == true)
			$pending = true;
		else
			$pending = false;

		if (!$pending)
		{

			$confirm_cancel = Yii::app()->request->getParam("confirm_cancel", false);

			if ($confirm_cancel) {
				try {
					$data_params = array(
						'installation_id' => $this->erp_installation_id,
						'status' => 'user_cancelled_sub',
						'guwid' => null,
						'ccard_expire_date' => null,
						'ccard_last_four' => null,
						'new_option' => null, // annulla eventuali richieste di downgrade
					);

					$api_res = ErpApiClient::apiErpSetSaasSubscriptionOptions($data_params);
					ErpApiClient::setBackupSetting('expiring_notif_answer', 'no');
					Settings::save('expiring_notification', '');
					Settings::save('expiring_notification_show', '');
					$html = $this->renderPartial('_cancel_success', array(), true);
					echo $html;

					//log the action
					$currentUser = Yii::app()->user->loadUserModel();
					Yii::log('Docebo subscription terminated:'
						."\n".'- user: '.Yii::app()->user->getRelativeUsername($currentUser->userid).' (#'.$currentUser->idst.')'
						."\n".'- time: '.Yii::app()->localtime->getUTCNow(), CLogger::LEVEL_INFO);

					Yii::app()->end();
				} catch (CException $e) {
					$html = $this->renderPartial('../common/_dialog2_error', array('message' => $e->getMessage()
					), true);
					echo $html;

					//log the action
					$currentUser = Yii::app()->user->loadUserModel();
					Yii::log('Docebo subscription termination attempted, but failed:'
						."\n".'- user: '.Yii::app()->user->getRelativeUsername($currentUser->userid).' (#'.$currentUser->idst.')'
						."\n".'- time: '.Yii::app()->localtime->getUTCNow()
						."\n".'- error message: '.$e->getMessage(), CLogger::LEVEL_INFO);

					Yii::app()->end();
				}

			}
			else
			{
				// Inform ERP about this action through API call
				$api = new ErpApiClient2();
				$result = $api->lmsSaveHistory(array(
					"type" => ErpApiClient2::API_TERMINATE_SUBSCRIPTION_CLICK,
				));
			}

			if(Settings::get('max_users')<50){
				// If canceling a plan of less than 50 users,
				// warn the user additionally that he will not be able
				// to renew at a plan smaller than 50 users
				// in the future. If he likes, he can continue using
				// his current small plan inifinitely without cancelling.
				$html = $this->renderPartial('_cancel_form_less_than_50', array(), true);
			}else{
				$html = $this->renderPartial('_cancel_form', array(), true);
			}
		}
		else
		{
			// Inform ERP about this action through API call
			$api = new ErpApiClient2();
			$result = $api->lmsSaveHistory(array(
				"type" => ErpApiClient2::API_TERMINATE_SUBSCRIPTION_CLICK,
			));

			$html = $this->renderPartial('_cancel_form_unavailable', array(), true);
		}

		echo $html;
		Yii::app()->end();


	}


	/**
	 * Generates Invoice list table (grid)
	 */
	public function actionInvoiceList() {


		try {
			$params = array(
				'installation_id' => $this->erp_installation_id,
				'current_installation_only' => true
			);

			if(isset(Yii::app()->request->cookies['reseller_cookie']))
				$params['request_from_partner'] = true;

			$comboInfo = ErpApiClient::apiErpGetComboInfo($params);
			$comboInfo = CJSON::decode($comboInfo);
			if ($comboInfo['success'] == false) {
				throw new CException(Yii::t('standard', 'internal error - contact us'));
			}


			$lmsOrderInfo = $comboInfo['lms_order_info'];

			$rawData = array();
			$billingInfo = $comboInfo['billing_info'];
			if (isset($billingInfo['data'])) {
				$billingInfo['data'] = CJSON::decode($billingInfo['data']);
				if (isset($billingInfo['data']['records'])) {
					$rawData = $billingInfo['data']['records'];
				}
			}

			$customerInfo = $comboInfo['customer_info'];

			$dataProvider = new CArrayDataProvider($rawData, array(
				'id' => 'invoiceListDataProvider',
				'keyField' => false,
				'pagination' => array('pageSize' => Settings::get('elements_per_page', 10)),
				'sort' => array(
					'attributes' => array(
						'invoice_date',
					),
				),
			));


			$this->render('invoices', array(
				'billingInfo' => $billingInfo,
				'dataProvider' => $dataProvider,
				'rawData' => $rawData,
				'print_analytics' => isset($_REQUEST['success']),
				'analytics_url' => Yii::app()->params['order_success_url'],
			));
		} catch (CException $e) {
			$this->render('invoices', array(
				'errorMessage' => $e->getMessage(),
			));
		}

	}


	/**
	 * Call this, passing appropriate parameters and will get document pdf (invoice, proforma, creditnote)
	 * Internally makes an API call to ERP
	 *
	 */
	public function actionGetInvoice() {

		$type = Yii::app()->request->getParam("type", false);
		$invoice_id = Yii::app()->request->getParam("invoice_id", false);
		$erp_installation_id = $this->erp_installation_id;


		if ($invoice_id > 0 && !empty($type) && $erp_installation_id > 0) {
			$data_params = array(
				'installation_id' => $erp_installation_id,
				'invoice_id' => $invoice_id,
				'type' => $type,
			);

			$api_res = ErpApiClient::apiErpGetInvoice($data_params);

			if (strpos(substr($api_res, 0, 20), 'PDF') !== false) { // pdf ok, send pdf to download
				$mime = 'application/pdf';
				$size = strlen($api_res);
				$name = $type . '.pdf'; // TODO: change this!

				header('Content-Type: ' . $mime);
				header('Content-Disposition: attachment; filename="' . $name . '"');
				header("Content-Length: " . $size);

				echo($api_res);
				flush();
				die();
			}
		}

	}


	/**
	 *
	 */
	public function actionActiveUsersReport() {

		if (!Yii::app()->request->isAjaxRequest) {
			//this will clear invalid records from active users table. Since this function may be quite slow to be
			//performed (due to potential high number of queries involved) it is executed only when opening the page and not
			//in ajax request (e.g. while updating table)
			UserLimit::activeUsersCleanUp();
		}

		$model = new LearningUserlimitLoginLog();

		// Search might coming?
		if ($data = Yii::app()->request->getParam('LearningUserlimitLoginLog', false)) {
			$model->attributes = $data;
		}

		$addPrompt = true;
		$monthSelectorData = $this->_getMonthSelectorData($addPrompt);

		if (!$model->search_period && !$addPrompt) {
			$keys = array_keys($monthSelectorData);
			$model->search_period = $keys[0];
		}

		$this->render('active_users_report', array(
			'model' => $model,
			'monthSelectorData' => $monthSelectorData,
		));

	}

	/**
	 * Marks the LMS as active in the ERP
	 */
	public function actionReactivateLms() {

		$output = ErpApiClient::apiErpSetSaasSubscriptionOptions(array(
			'status' => 'ok',
			'installation_id' => Docebo::getErpInstallationId()
		));

		echo CJSON::encode(array(
			'success'=> $output
		));

		//log the action
		$currentUser = Yii::app()->user->loadUserModel();
		if ($output) {
			Yii::log('Docebo subscription reactivation:'
				. "\n" . '- user: ' . Yii::app()->user->getRelativeUsername($currentUser->userid) . ' (#' . $currentUser->idst . ')'
				. "\n" . '- time: ' . Yii::app()->localtime->getUTCNow(), CLogger::LEVEL_INFO);
		} else {
			Yii::log('Docebo subscription reactivation attempted, but failed:'
				. "\n" . '- user: ' . Yii::app()->user->getRelativeUsername($currentUser->userid) . ' (#' . $currentUser->idst . ')'
				. "\n" . '- time: ' . Yii::app()->localtime->getUTCNow(), CLogger::LEVEL_INFO);
		}
	}

	/**
	 * Render "Invoice" related columns
	 *
	 * @param array $row
	 * @param number $index
	 * @param CDataColumn $column
	 *
	 * @return string
	 */
	protected function _renderInvoiceDocumentLink($row, $index, $column) {

		$url_params = array();
		$type = "";

		switch ($column->id) {
			case 'invoice' :
				if ($row['document_type'] == 'invoice' && $row['invoice_id'] != 727) {
					$type = 'invoice';
				}
				$title = Yii::t('billing', '_DOWNLOAD_INVOICE');
				break;

			case 'creditnote':
				if ($row['document_type'] == 'credit_note') {
					$type = 'creditnote';
				}
				$title = Yii::t('billing', '_DOWNLOAD_CREDIT_NOTE');
				break;

			case 'proforma':
				if ($row['document_type'] == 'proforma') {
					$type = "proforma";
				}
				$title = Yii::t('billing', '_DOWNLOAD_PROFORMA');
				break;

		}

		if ($type == "") {
			return "";
		}

		$url_params['type'] = $type;
		$url_params['invoice_id'] = $row['invoice_id'];

		$url = Docebo::createLmsUrl('billing/default/getInvoice', $url_params);

		$title .= " #" . $row['invoice_num'];

		$html = "<a title='$title' href='$url'><span class='i-sprite is-pdf'></span></a>";

		return $html;


	}


	/**
	 * Return specific array for Period selection dropdown
	 *
	 * @param bool $addPrompt
	 *
	 * @return array
	 */
	protected function _getMonthSelectorData($addPrompt = false) {

		$periods = Billing::getSubscriptionPeriodDates();
		$periods = array_reverse($periods);

		$data = array();

		if ($addPrompt) {
			$data[''] = Yii::t('userlimit', 'Choose period');
		}


		foreach ($periods as $period) {
			$localDateFrom 	= Yii::app()->localtime->toLocalDate($period["from"]);
			$localDateTo 	= Yii::app()->localtime->toLocalDate($period["to"]);
			$key 			= date('Y-m', strtotime($period["from"])) . "|" . $period['from'] . "|" . $period['to'];
			$value 			=  Yii::t('standard', '_FROM') . ': ' . $localDateFrom  . ' ' . Yii::t('standard', '_TO') . ': ' . $localDateTo;
			$data[$key] 	= $value;
		}

		return $data;


	}


	/**
	 * Rendre the EXTRA login column (which shows if some user is above the quota)
	 *
	 * @param array $raw
	 * @param number $index
	 * @param CDbDataColumn $column
	 * @return string
	 */
	protected function _renderExtraLoginColumn($raw, $index, $column) {
		$dates = Billing::getSubscriptionPeriodDates();
		$userIsEA = false;
		$maxUsers = Settings::get('max_users');
		foreach($dates as $date){
			if(Yii::app()->localtime->isGte($raw['first_period_login'], $date['from']) && Yii::app()->localtime->isLte($raw['first_period_login'],  $date['to'])){
				$model = new LearningUserlimitLoginLog();
				$command = Yii::app()->db->createCommand();
				$command->select =  'userIdst';
				$command->andWhere('first_period_login >= "'.$date['from'].'" AND first_period_login <= "'.$date['to'].'"');
				$command->from($model->tableName());
				$command->offset = $maxUsers;
				$command->order('first_period_login ASC');
				$extraAccessRecords = $command->queryAll();
				foreach($extraAccessRecords as $extraAccessRecord){
					if(isset($raw['idst']) && is_array($extraAccessRecord) && in_array($raw['idst'], $extraAccessRecord)){
						$userIsEA = true;
					}
				}
			}
		}

		if (!$userIsEA) {
			$html = "";
		} else {
			$html = "<span class='text-center overbooked'>" . Yii::t('userlimit', 'Extra Access') . "</span>";
		}

		return $html;
	}

	public function _renderExtraLoginColumnExport($raw) {
		$dates = Billing::getSubscriptionPeriodDates();
		$userIsEA = false;
		$maxUsers = Settings::get('max_users');
		foreach($dates as $date){
			if(Yii::app()->localtime->isGte($raw['first_period_login'], $date['from']) && Yii::app()->localtime->isLte($raw['first_period_login'],  $date['to'])){
				$model = new LearningUserlimitLoginLog();
				$command = Yii::app()->db->createCommand();
				$command->select =  'userIdst';
				$command->andWhere('first_period_login >= "'.$date['from'].'" AND first_period_login <= "'.$date['to'].'"');
				$command->from($model->tableName());
				$command->offset = $maxUsers;
				$command->order('first_period_login ASC');
				$extraAccessRecords = $command->queryAll();
				foreach($extraAccessRecords as $extraAccessRecord){
					if(isset($raw['idst']) && is_array($extraAccessRecord) && in_array($raw['idst'], $extraAccessRecord)){
						$userIsEA = true;
					}
				}
			}
		}

		return $userIsEA;
	}


	/**
	 * Render Username column, basically returning the "clean" one
	 *
	 * @param array $raw
	 * @param index $index
	 */
	protected function _renderUserId($raw, $index) {
		$model = CoreUser::model()->findByPk($raw['idst']);
		if (!$model) {
			$model = CoreDeletedUser::model()->findByAttributes(array('idst' => $raw['idst']));
			if ($model) {
				return '(' . Yii::t('standard', '_USER_STATUS_CANCELLED') . ')';
			}
		} else {
			return $model->getClearUserId();
		}
		return '-';
	}

	protected function _renderMultidomainBranchs($raw, $index){
		$query = Yii::app()->db->createCommand();
		$query->select("GROUP_CONCAT(DISTINCT translation SEPARATOR ', ')");
		$query->from('core_multidomain cm');
		$query->leftJoin('core_org_chart_tree coct', 'coct.idOrg = cm.org_chart');
		$query->leftJoin('core_group_members group_member', 'coct.idst_oc = group_member.idst OR coct.idst_ocd = group_member.idst');
		$query->leftJoin('core_org_chart coc', 'coc.id_dir = coct.idOrg AND lang_code = :lang');
		$query->where('group_member.idstMember = :user');
		if($raw['multidomain'])
			$query->andWhere('coct.idOrg IN (' .$raw['multidomain'] . ')');

		return $query->queryScalar(array(':user'=> $raw['idst'], ':lang' => Lang::getCodeByBrowserCode(Yii::app()->getLanguage())));
	}

	public function actionaxProgressiveExportActiveUsersReport()
	{
		$response = new AjaxResult();
		// Number of rows to extract per chunk; make it as high as possible
		$defaultLimit = Yii::app()->params['report_export_chunk_size'];
		$idReport 	= Yii::app()->request->getParam('idReport', false);
		$limit 		= Yii::app()->request->getParam('limit', $defaultLimit);
		$offset 	= Yii::app()->request->getParam('offset', 0);
		$start 		= Yii::app()->request->getParam('start', false);
		$fileName	= Yii::app()->request->getParam('fileName', false);
		$totalNumber= Yii::app()->request->getParam('totalNumber', false);
		$download	= Yii::app()->request->getParam('download', false);
		$downloadGo	= Yii::app()->request->getParam('downloadGo', false);
		$exportType	= Yii::app()->request->getParam('exportType', LearningReportFilter::EXPORT_TYPE_CSV);
		$phase		= Yii::app()->request->getParam('phase', 'start');
		$customFilter = Yii::app()->request->getParam('model', array());

		// DOWNLOAD button clicked in the dialog. This is just a signal to close the dialog
		if ($download) {
			echo '<a class="auto-close"></a>';
			Yii::app()->end();
		}

		// Real DOWNLOAD action: a result of JS redirection from the dialog
		if ($downloadGo) {
			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_UPLOADS);
			$storage->sendFile($fileName);
			Yii::app()->end();
		}


		try {
			$reportData		= $this->getReportDataProviderInfo($customFilter);
			$dataProvider 	= $reportData['dataProvider'];
			$columns 		= $reportData['gridColumns'];
			$reportTitle	= $reportData['reportTitle'];

			$csvFileName = Sanitize::fileName($reportTitle . ".csv", true);
			$csvFileName = $fileName ? $fileName : $csvFileName;
			$csvFilePath = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR . $csvFileName;

			$nextOffset = $offset;

			if ($phase == 'start' || $start) {
				$fh = fopen($csvFilePath, 'w');
				$headers = array();
				$values = array();

				// HEADERS
				foreach ($columns as $column) {
					if (isset($column['header'])) {
						$headers[] = $column['header'];
					}
					else if (isset($column['name'])) {
						$headers[] = $column['name'];
					}
				}

				// Write a HEADER line
				CSVParser::filePutCsv($fh, $headers);
			}
			else if ($phase == 'extract') {
				$formatCellsAsText = false;
				if($exportType != LearningReportFilter::EXPORT_TYPE_CSV) {
					if(($exportType == LearningReportFilter::EXPORT_TYPE_XLS) && ($totalNumber > Gnumeric::MAX_ROWS_XLS)) {
						$formatCellsAsText = false;
					} else {
						$gnumeric = new Gnumeric();
						$formatCellsAsText = $gnumeric->checkGnumeric();
					}
				}

				$fh = fopen($csvFilePath, 'a');
				$nextOffset = $offset + $limit;
				$sql = $dataProvider->sql;
				$sql = $sql . " LIMIT $offset,$limit";
				$dataReader = Yii::app()->db->createCommand($sql)->query($dataProvider->params);

				$value = null; // reused
				foreach ($dataReader as $data)
				{

                    $data['branches'] = CoreOrgChartTree::getOcGroupsPaths($data["branches"]);
					$data['client'] = $this->_renderMultidomainBranchs($data, null);
					if(!PluginManager::isPluginActive('MultidomainApp'))
						unset($data['client']);

					$data['first_period_login'] = Yii::app()->localtime->toLocalDateTime($data['first_period_login']);
					$data['lastenter'] = Yii::app()->localtime->toLocalDateTime($data['lastenter']);

					if(!$this->_renderExtraLoginColumnExport($data)) {
						$data['extra_access'] = "";
					} else {
						$data['extra_access'] = Yii::t('userlimit', 'Extra Access');
					}
					unset($data['idst']);
					unset($data['id']);
					if(($exportType == LearningReportFilter::EXPORT_TYPE_XLS) && is_array($data)){
						foreach($data as $key => $val){
							$data[$key] = "'".$val;
						}
						CSVParser::filePutCsv($fh, $data);
					}else{
						CSVParser::filePutCsv($fh, $data);
					}

				}

			}
			else if ($phase == 'convert') {
				$gnumeric = new Gnumeric();
				$in = $csvFilePath;
				$pathInfo = pathinfo($in);
				$extension = $exportType;
				$fileName = $pathInfo['filename'] . "." . $extension;

				$out =  $pathInfo['dirname'] . DIRECTORY_SEPARATOR . $fileName;

				// Use gnumeric to convert the file
				$format = false;

				// Note: this is almost useless, because Gnumeric recognize the output extension and uses correct format
				switch ($exportType) {
					case LearningReportFilter::EXPORT_TYPE_CSV:
						$format = Gnumeric::FORMAT_CSV;
						break;
					case LearningReportFilter::EXPORT_TYPE_XLS;
						$format = Gnumeric::FORMAT_XLS;
						break;
				}

				// Do the conversion and get result (true, false)
				$result = $gnumeric->convertTo($in, $out, $format);

				if (!$result) {
					throw new CException($gnumeric->getError());
				}

				// Clean up the INPUT file (csv)
				FileHelper::removeFile($in);

			}

			// Close file if we've got a valid file handler
			if ($fh) {
				fclose($fh);
			}

			// We count records the first time we've got a request ('start' phase) and pass the counter to next call to save some work
			if (!$totalNumber) {
				$totalNumber = $dataProvider->getTotalItemCount();
			}

			// Calculate the extraction offset for the NEXT (!) request which we show NOW!
			// i.e. in current render we show what is GOING TO BE done in the next cycle
			$exportingNumber = $nextOffset + $limit;
			$exportingNumber = min($exportingNumber, $totalNumber);

			// Also, in percentage ...
			$exportingPercent = 0;
			if ($totalNumber > 0) {
				$exportingPercent =  $exportingNumber / $totalNumber * 100;
			}


			// 'Phase' control
			switch ($phase) {
				case 'start':  		// after 'start', we begin extraction
					$nextPhase = 'extract';
					break;
				case 'extract':		// we continue extracttion phase until end-of-records; then switch to 'convert'/'download'
					$nextPhase = $phase;
					if ($nextOffset > $totalNumber) {
						if (($exportType == LearningReportFilter::EXPORT_TYPE_XLS) && ($totalNumber > Gnumeric::MAX_ROWS_XLS)) {
							$exportType = LearningReportFilter::EXPORT_TYPE_CSV;
							$nextPhase = 'download';
							$forceCsvMaxRows = true;
						}
						else {
							// Check if we have healthy Gnumeric installation
							$gnumeric = new Gnumeric();
							$check = $gnumeric->checkGnumeric();
							if (!$check) {
								$forceCsvMissingGnumeric = true;
								$exportType = LearningReportFilter::EXPORT_TYPE_CSV;
								$nextPhase = 'download';
							}
							else {
								$nextPhase = $exportType == LearningReportFilter::EXPORT_TYPE_CSV ?  'download' : 'convert';
							}
						}
					}
					break;
				case 'convert':		// after conversion we are going to 'download' the final file
					$nextPhase = 'download';
					break;
			}


			// We completely stop calling ourselves when we reach the "download" phase. Tell this to the ProgressController JS
			$stop = ($nextPhase == 'download') || ($totalNumber <= 0);

			// Render the dialog to show what is going ot be done on NEXT CALL (if any)
			// or to offer final actions, like "Doanload"
			$html = $this->renderPartial('_progressive_exporter', array(
				'reportTitle' 		=> $reportTitle,
				'totalNumber'		=> $totalNumber,
				'exportingNumber'	=> $exportingNumber,
				'exportingPercent'	=> $exportingPercent,
				'fileName'			=> $fileName,
				'nextPhase'			=> $nextPhase,
				'exportType'		=> $exportType,
				'forceCsvMaxRows'	=> $forceCsvMaxRows,
				'forceCsvMissingGnumeric' => $forceCsvMissingGnumeric,
			),true,true);


			// These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stoppping when $stop == true)
			$data = array(
				'offset'		=> $nextOffset,
				'stop'			=> $stop,
				'fileName'		=> $csvFileName,
				'totalNumber'	=> $totalNumber,
				'exportType'	=> $exportType,
				'phase'			=> $nextPhase,
				'custom_filter' => $customFilter,
			);
			// Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
			$response->setStatus(true);
			$response->setHtml($html)->setData($data)->toJSON();
			Yii::app()->end();
		}
		catch (CException $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			$response->setStatus(false)->setMessage($e->getMessage());
			$response->toJSON();
			Yii::app()->end();
		}
	}

	private function getReportDataProviderInfo($customFilter)
	{
		$columns = array();
		$model = new LearningUserlimitLoginLog();
		$model->attributes = $customFilter;
		$dataProvider = $model->reportSqlDataProvider();

		$columns[] = array('header' => Yii::t('standard', 'branches'),);
		if(PluginManager::isPluginActive('MultidomainApp'))
			$columns[] = array('header' =>  Yii::t('multidomain', 'Client'));

		$columns = array_merge($columns, array(
			array('header' => Yii::t('standard', '_USERNAME'),),
			array('header' => Yii::t('standard', '_FIRSTNAME'),),
			array('header' => Yii::t('standard', '_LASTNAME'),),
			array('header' => Yii::t('userlimit', 'First Access'),),
			array('header' => Yii::t('userlimit', 'Last login'),),
			array('name' => Yii::t('userlimit', 'Extra Access'),),
		));

		return array(
			'dataProvider' 	=> $dataProvider,
			'gridColumns'	=> $columns,
			'reportTitle'	=> Yii::t('userlimit', 'Active users report'),
		);
	}
	
	/**
	 * Build "Current plan" string, based on LMS/ERP status
	 *
	 * @return string
	 */
	public function getCurrentPlanString($erp_combo) {
	    
	    if ($erp_combo['lms_order_info']['plan_info']['pricing_version'] == 2) {
	        $isBundledPlan = true;
	    }
	    
	    $result = "";
	
	    if ($isBundledPlan) {
	        $planName = $erp_combo['lms_order_info']['plan_info']['option_name'];
	        $result = $planName . " (" . $erp_combo['lms_order_info']['plan_info']['default_amount'] . ")";
	    }
	    else {
	        $result = $erp_combo['lms_order_info']['plan_info']['default_amount'] . " " . Yii::t("standard", "Users");
	    }
	    return $result;
	}

}