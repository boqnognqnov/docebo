var BillingModule = function(options) {
	// Default settings
	this.settings = {
	};
	this.init(options);

};


BillingModule.prototype = {

		init: function(options) {
			this.settings = $.extend({}, this.settings, options);
		},
		
		addListeners: function() {
			
			// Submit search on ENTER
			$("#active-users-search-form input[type='text']").keypress(function(event) {
			    if (event.which == 13) {
			        event.preventDefault();
			        $("#active-users-search-form").submit();
			    }
			});
			
			// Listen for MONTH select change
			$("#search_period").on('change', function() {
				//if ($(this).val() == '') return false;
				$("#active-users-search-form").submit();
			});
			
			
			// Listen for Grid Search FORM submition
			$("#active-users-search-form").submit(function(){
				var data = $(this).serialize();
				$.fn.yiiGridView.update('active-users-report-grid', {
					data: data
				});
				return false;
			});
			
			
			
			
		}		
		
};


var Billing = null;

$(function () {
	$(function(){
		Billing.addListeners();
	});
});