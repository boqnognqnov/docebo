<style>
<!--

	.modal-header h3 {
		padding-left: 0px;
	}

-->
</style>


<h1><?= Yii::t('userlimit', '_CANCEL_SUBSCRIPTION') ?></h1>


<?php
	$form = $this->beginWidget('CActiveForm', array(
		'method' => 'POST',
		'id' => 'cancel-subscription-form',
		'enableAjaxValidation' => false,
		'htmlOptions'=>array(
				'class' => 'ajax form-forizontal',
		),
	));

?>


<div>
<?= Yii::t('userlimit', '_ARE_YOU_SURE_CANCEL_SUB')  ?>
</div>
<br>
<div class="clearfix"></div>

<div class="row-fluid">
	<div class="span12">
	<label class="checkbox">
		<?php echo CHtml::checkBox('confirm_cancel', false, array()); ?>&nbsp;
		<?php echo Yii::t('userlimit','Yes, I confirm I want to terminate my subscription'); ?>
	</label>
	</div>
</div>



<div class="form-actions">
	<?= CHtml::submitButton(Yii::t('userlimit', '_CANCEL_SUBSCRIPTION'), array('class' => 'btn-docebo big red disabled btn-cancel-submit')); ?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn-docebo big black close-dialog')); ?>
</div>

<?php $this->endWidget(); ?>



<script type="text/javascript">
<!--

$(function(){

	// Clicks on disabled SUBMIT buttons must be ignored
	$(document).on('click', '.disabled', function(){return false;});
	
	$('input[name="confirm_cancel"]').on('change', function() {
		$(this).prop("checked") ? $('.btn-cancel-submit').removeClass('disabled') : $('.btn-cancel-submit').addClass('disabled');
	});
});


//-->
</script>