<style>
<!--


-->
</style>



<?php $this->breadcrumbs = array(
	Yii::t('userlimit', 'Active users report'),
); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'active-users-search-form',
	'htmlOptions' => array(
		'class' => 'form-inline',
		'data-grid' => '#active-users-grid'
	),
)); ?>

<div class="docebo-form active-users-report">
	<div class="control-container">
			<div class="row-fluid">
				<div class="span7 control-group">
					<?= $form->dropDownList($model, 'search_period', $monthSelectorData,
								array('class' => 'span6', 'id' => 'search_period')) ?>

					<div id="export_active_users_button">
						<?php
							$actions = array(
								'csv' => Yii::t('standard', '_EXPORT_CSV'),
								'xls' => Yii::t('standard', '_EXPORT_XLS'),
							);

							echo CHtml::dropDownList('export_active_users', false, $actions, array(
									'id' => 'export-active-users',
									'empty' => Yii::t('standard', 'Select action')
								)
							);
						?>
					</div>
				</div>
        		<div class="span5 text-right control-group">
        			<div class="ui-search">
            			<?php echo $form->textField($model, 'search_input', array(
							'id' => 'search-transactions-text',
							'placeholder' => Yii::t('standard', '_SEARCH').'...',
            				'class' => 'search-input search-txt input-append'	
						)); ?>
            			<button type="submit" class="search-btn"><i class="icon-search"></i></button>
            		</div>
           		</div>
           		
           	</div>
	</div>
</div>


<?php

$this->widget('DoceboCGridView', array(
	'id' => 'active-users-report-grid',
	'htmlOptions' => array(''),
	'afterAjaxUpdate'=>'function(){$(document).controls(); }',
	'template' => '{pager}{items}{summary}{pager}',
	'summaryText' => Yii::t('standard', '_TOTAL'),
	'pager' => array(
		'class' => 'DoceboCLinkPager',
		'maxButtonCount' => 8,
	),
	'pagerCssClass' => 'pager',
	'dataProvider' => $model->reportSqlDataProvider(20),
	'columns' => array(
		array(
			'header' => Yii::t('standard', 'branches'),
			'value' => 'CoreOrgChartTree::getOcGroupsPaths($data["branches"])',
		),
		array(
			'header' =>  Yii::t('multidomain', 'Client'),
			'value' => array($this, '_renderMultidomainBranchs'),
			'visible' => PluginManager::isPluginActive('MultidomainApp')
		),
		array(
			'name' => 'userid',
			'header' => Yii::t('standard', '_USERNAME'),
			'value' => array($this, '_renderUserId'),
			'type' => 'raw',
		),
		array(
			'name' => 'firstname',
			'header' => Yii::t('standard', '_FIRSTNAME'),
			'value' => '$data[firstname]',
			'type' => 'raw',
		),
		array(
			'name' => 'lastname',
			'header' => Yii::t('standard', '_LASTNAME'),
			'value' => '$data[lastname]',
			'type' => 'raw',
		),
		array(
			'name' => 'first_period_login',
			'header' => Yii::t('userlimit', 'First Access'),
			'value' => 'Yii::app()->localtime->toLocalDateTime($data[first_period_login])',
			'type' => 'raw',
		),
		array(
			'name' => 'lastenter',
			'header' => Yii::t('userlimit', 'Last login'),
			'value' => 'Yii::app()->localtime->toLocalDateTime($data[lastenter])',
			'type' => 'raw',
		),
		array(
			'name' => Yii::t('userlimit', 'Extra Access'),
			'value' => array($this, '_renderExtraLoginColumn'),
			'type' => 'raw',
		),
	),
));


?>



<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<?php
	$progressUrl = Docebo::createAbsoluteLmsUrl('progress/run', array(
		'type'		=> Progress::JOBTYPE_EXPORT_ACTIVE_USERS_REPORT,
		'idReport' 	=> $_REQUEST['id'],
	));
?>

<script type="text/javascript">
	$(function(){
		$('#export-active-users').on('change', function(){
			if ($(this).val() == '')
				return true;

			// If plugins added any custom filters/inputs
			var exportData = $('#active-users-search-form').serialize();

			var url = '<?= $progressUrl ?>' + '&exportType=' + $(this).val();
			$('<div/>').dialog2({
				title: '',
				content: url,
				ajaxType: 'post',
				data: exportData,
				id: "progressive-export-active-users-report"
			});
			return true;
		});

		//Add additional data to the importing, such as should the record be updated or not
		$(document).on('dialog2.closed', '#progressive-export-active-users-report', function(e, xhr, settings) {
			$('#export-active-users').val('0');
		});
	});
</script>
