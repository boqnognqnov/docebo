<style>

b{
	font-weight: 600;
}
.warning-canceling .red-text{
	color: #d50304;
}
.red-bordered{
	border: 2px solid #d50304;
	padding: 10px;
}
.red-bordered p{
	margin: 0;
}
.warning-exclamation-icon{
	margin-bottom: 10px;
}

</style>


<h1><?= Yii::t('standard', '_WARNING') ?></h1>

<div class="warning-canceling">

	<div class="text-center">
		<div class="warning-exclamation-icon i-sprite is-solid-exclam red large"></div>
	</div>

	<div class="red-bordered text-center">
		<p class="nomargin"><?=Yii::t('userlimit', 'You are canceling your actual <b>{current_users} active users plan.</b>', array('{current_users}'=>Settings::get('max_users')))?></p>

		<p class="nomargin"><?=Yii::t('userlimit', "<b class='red-text'>In case of reactivation, this plan won't be available</b> anymore, because now plans start from <b>50 active users.</b>")?></p>
	</div>

	<p>&nbsp;</p>

	<p class="nomargin"><?=Yii::t('userlimit', "Please note that, by cancelling your plan, your data's retention won't be guaranteed. If you decide to cancel this plan, you will be able to subscribe ONLY to plans starting from 50 active users.");?></b></p>
</div>

<p>&nbsp;</p>

<?php
	$form = $this->beginWidget('CActiveForm', array(
		'method' => 'POST',
		'id' => 'cancel-subscription-form',
		'enableAjaxValidation' => false,
		'htmlOptions'=>array(
				'class' => 'ajax form-forizontal',
		),
	));

?>

<div class="row-fluid">
	<div class="span12">
	<label class="checkbox">
		<?php echo CHtml::checkBox('confirm_cancel', false, array()); ?>&nbsp;
		<?php echo Yii::t("userlimit", "Yes, I'm sure and I want to <b>cancel my {current_users} active users plan subscription.</b>", array('{current_users}'=>Settings::get('max_users'))); ?>
	</label>
	</div>
</div>

<p>&nbsp;</p>
<div class="form-actions-old-dont-attach">
	<div class="pull-left">
		<?=Yii::t('userlimit', 'Need more info?')?> <a class="ajax blue-text" href="<?=Docebo::createLmsUrl('site/axHelpDesk')?>" rel="helpdesk-modal" data-dialog-class="helpdesk-modal"><?=Yii::t('standard', 'Contact us')?></a>!
	</div>
	<div class="pull-right">
		<?= CHtml::submitButton(Yii::t('standard', '_CONFIRM'), array('class' => 'btn-docebo big green disabled btn-cancel-submit')); ?>
		<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn-docebo big black close-dialog')); ?>
	</div>
	<div class="clearfix"></div>
</div>

<?php $this->endWidget(); ?>



<script type="text/javascript">
<!--

$(function(){

	// Clicks on disabled SUBMIT buttons must be ignored
	$(document).on('click', '.disabled', function(){return false;});
	
	$('input[name="confirm_cancel"]').on('change', function() {
		$(this).prop("checked") ? $('.btn-cancel-submit').removeClass('disabled') : $('.btn-cancel-submit').addClass('disabled');
	});

	$(document).on('click', '.close-dialog', function(){
		$(this).closest('.modal-body').dialog2('close');
	});
});


//-->
</script>