<style>
	.editCCInfo-dialog {
		top: 150px;
	}

	.editCCInfo-dialog .modal-body {
		padding: 0px;
		max-height: none !important;
		/*line-height: 1px;*/
		height: 610px;
		overflow-y: hidden;
	}
	.editCCInfo-dialog .modal-footer{
		display: none !important;
	}
	
	
	.modal-body#edit-creditcard-modal {
	   min-height: 615px;
	}

	iframe.erp {
        height: <?= !empty($iframeHeight) ? ($iframeHeight.'px') : 'calc(100vh - 120px)' ?>;
    }
	
</style>
<?php if ($inDialog): ?>
	<h1><?= $title ?></h1>
<?php else : ?>
<?php $this->breadcrumbs = array(
	Yii::t('order', 'Get more users'),
); ?>
<?php  endif; ?>

<iframe id="erp-iframe" class="erp" src="<?php echo $orderUrl; ?>" width="100%" frameborder="0"></iframe>

<script type="text/javascript">
    $(document).ready(function() {
        var erpIframeWindow = $('#erp-iframe')[0].contentWindow;
        window.addEventListener("message", receiveMessage, false);
        function receiveMessage(event) {
            if (event.source == erpIframeWindow) {
                console.log("Detected message from ERP:", event);
                var eventData = event.data;
                if (eventData) {
                    var action = eventData.message;
                    var payload = eventData.payload ? eventData.payload : {};
                    switch (action) {
                        case 'erp.showOverlay':
                        <?php if (Yii::app()->legacyWrapper->hydraFrontendEnabled()) { ?>
						    <?= Yii::app()->legacyWrapper->toggleStripeOverlay(true, false) ?>
						<?php } else { ?>
                            $("#header").css('z-index', '-2');
                            $("#navigation").css('z-index', '-2').css('position', 'relative');
                            $(".menu").hide();
                            $(".stripe-backdrop").show();
						<?php } ?>
                            break;
                        case 'erp.hideOverlay':
						<?php if (Yii::app()->legacyWrapper->hydraFrontendEnabled()) { ?>
						    <?= Yii::app()->legacyWrapper->toggleStripeOverlay(false, false) ?>
						<?php } else { ?>
                            $(".menu").hide();
                            $("#header").css('z-index', 'auto');
                            $("#navigation").css('z-index', 'auto').css('position', 'static');
                            $(".stripe-backdrop").hide();
						<?php } ?>
                            break;
                        case 'erp.reloadBilling':
						<?php if (Yii::app()->legacyWrapper->hydraFrontendEnabled()) { ?>
						    <?= Yii::app()->legacyWrapper->reloadBillingInfo(false) ?>
						<?php } ?>
                            break;
                        case 'erp.contactUs':
						<?php if (Yii::app()->legacyWrapper->hydraFrontendEnabled()) { ?>
                            eventData.state = 'sales';
						    <?= Yii::app()->legacyWrapper->openCommunicationCenter(false) ?>
						<?php } else { ?>
                            $(document).trigger("communication-center.open", "sales");
						<?php } ?>
                            break;
                        case 'erp.goToPage':
                            var page = payload.destination;
                            switch(page) {
                                case 'dashboard':
								<?php if (Yii::app()->legacyWrapper->hydraFrontendEnabled()) { ?>
								    <?= Yii::app()->legacyWrapper->setHydraRoute('/learn/home', false) ?>
								<?php } else { ?>
                                    window.location.href=window.location.protocol+"//"+window.location.hostname+"/lms/index.php?r=site/index";
								<?php } ?>
                                    break;
                                case 'invoices':
                                    window.location.href=window.location.protocol+"//"+window.location.hostname+"/lms/index.php?r=billing/default/invoiceList";
                                    break;
                            }
                            break;
                    }
                }
            }
        }
    });
</script>