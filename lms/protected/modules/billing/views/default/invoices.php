<?php $this->breadcrumbs = array(
    Yii::t('billing', '_BILLING_INFORMATION'),
); ?>


<?php if ($errorMessage) : ?>
	<div class="alert alert-error"><?= $errorMessage ?></div>
<?php else : ?>



<?php
// Grid View using Array Data provider
$this->widget('DoceboCGridView', array(
		'id' => 'invoice-list-grid',
		'htmlOptions' => array(),
		'template' => '{pager}{items}{pager}',
		'dataProvider' => $dataProvider,
		'columns' => array(
			array(
				'name' => Yii::t('billing','_INVOICE_NUMBER'),
				'value' => '$data[invoice_num]',
				'type' => 'raw',
			),
			array(
				'name' => Yii::t('standard','_DATE'),
				'value' => 'date("d/m/Y", strtotime($data[invoice_date]))',
				'type' => 'raw',
			),
			array(
				'name' => Yii::t('billing','_AMOUNT'),
				'value' => '$data[amount_vat]',
				'type' => 'raw',
			),
			array(
				'name' => Yii::t('billing','_PAID'),
				'value' => '$data[is_paid] ? "<span class=\"i-sprite is-circle-check green\"></span>" : "<span class=\"i-sprite is-circle-check grey\"></span>"',
				'type' => 'raw',
			),

			array(
				'name' 	=> Yii::t('billing','_PROFORMA'),
				'id'	=> 'proforma',
				'value' => array($this, '_renderInvoiceDocumentLink'),
				'type' 	=> 'raw',
			),
			array(
				'name' 	=> Yii::t('billing','_CREDIT_NOTE'),
				'id'	=> 'creditnote',
				'value' => array($this, '_renderInvoiceDocumentLink'),
				'type' 	=> 'raw',
			),
			array(
				'name' 	=> Yii::t('billing','_INVOICE'),
				'id'	=> 'invoice',
				'value' => array($this, '_renderInvoiceDocumentLink'),
				'type' 	=> 'raw',
			),

		),
));


?>


<?php endif; ?>

<?php if ($print_analytics) : ?>

	<iframe src="<?php echo $analytics_url; ?>" height="0" width="0" frameborder="0" style="width:0px;height:0px;border:0 none;"></iframe>

<?php endif; ?>