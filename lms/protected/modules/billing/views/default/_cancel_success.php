<style>
<!--

	.modal-header h3 {
		padding-left: 0px;
	}

-->
</style>

<h1><?= Yii::t('userlimit','Subscription terminated') ?></h1>

<div>
	<?= Yii::t('userlimit','Subscription has been terminated successfully') ?>
</div>

<!--
This is consumed by the 'dialog2.closed' event handler in the parent view.
Reloads the page after the current dialog is closed voluntarely by the user

-->
<div class="reload-after-close"></div>

<div class="form-actions">
	<?= CHtml::button(Yii::t('standard', '_CLOSE'), array('class' => 'btn-docebo big black close-dialog')); ?>
</div>
