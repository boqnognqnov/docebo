<div class="second-menu-spacer">
	<h2><?= $activeUsers ?> / <span><?= $maxUsers ?></span></h2>
	<p class="active-users"><?= Yii::t('order', 'Active users') ?>
		<i 	id="active-users"
			class="icon-info-sign"
			data-title="<?= strip_tags(Yii::t('order', '<b>Active users: </b> Unique Users that sign in the platform within 30 days')); ?>"
			data-placement="bottom"
			data-trigger="hover focus click"></i>
	</p>

	<a href="<?= $activeUsersReportUrl ?>" class="btn-docebo grey full"><?= Yii::t('userlimit', 'Active users report') ?></a>

	<br />

	<? if($lmsOrderInfo['payment_status'] == 'user_cancelled_sub'): // LMS cancelled but can be reactivated ?>
		<div class="pending-cancellation">
			<h4 class="pending-cancellation-header"><img src="<?=Yii::app()->theme->baseUrl . '/images/trash-red.png'?>"><?=Yii::t('userlimit', 'Subscription terminated')?></h4>
			<br/>
			<p>
				<?=Yii::t('userlimit', 'Subscription has been terminated successfully')?>
				<?/*=Yii::t('billing', 'Your {users} users plan will be definitely cancelled on {date} (End of billing period). Before this date, you are still able to reactivate your plan.', array(
					'{users}'=>Settings::get('max_users'),
					'{date}'=>'<b>'.date('d-m-Y', strtotime($lmsOrderInfo['payment_expire_date'])).'</b>',
				))*/?>
			</p>
			<a href="#" id="reactivate-lms" class="btn-docebo green full text-center"><?=Yii::t('standard', '_REACTIVATE')?></a>
		</div>
		<br/>
	<? else: // LMS subscription fully active (not cancelled) ?>
		<p class="">
			<div class="menu_active_plan"><strong><?= Yii::t('userlimit', '_YOUR_PLAN') ?></strong><span><?= $currentPlanString  ?></span></div>
			<?php if ($lmsOrderInfo['new_plan_info']) : ?>
				<div class="menu_new_plan">
					<strong><?= Yii::t('userlimit', '_YOUR_NEXT_PLAN') ?><br><i><?= Yii::t('standard', '_FROM') ?> <?=date("Y-m-d", (strtotime($lmsOrderInfo['payment_expire_date'])) + 86400)?></i></strong>
					<span><?= $lmsOrderInfo['new_plan_info']['default_amount'] ?></span>
				</div>
			<?php endif; ?>
		</p>
		<?php if (Billing::isWireTransfer()) : ?>
			<a href="javascript:;" onclick="contactUs();" class="btn-docebo blue full" id="change-plan"><span class="i-sprite is-settings white"></span> <?= Yii::t('userlimit', 'Change plan') ?></a>
		<?php else : ?>
			<a href="<?= $upgradeUrl ?>" class="btn-docebo blue full" id="change-plan"><span class="i-sprite is-settings white"></span> <?= Yii::t('userlimit', 'Change plan') ?></a>
		<?php endif; ?>
		<br />
		<!-- <a href="<?= $downgradeUrl ?>" class="open-dialog btn-docebo black full" rel="downgrade-modal"><i class="ico-gmu downgrade"></i> <?= Yii::t('userlimit', '_DOWNGRADE_SUBSCRIPTION') ?></a>
		<br /> -->
		<a href="<?= $cancelUrl ?>" class="open-dialog btn-docebo red full" data-dialog-class="downgrade-dialog" rel="cancel-modal"><i class="ico-gmu terminate"></i> <?= Yii::t('userlimit', '_CANCEL_SUBSCRIPTION') ?></a>
		<br />
	<? endif; ?>


	<h3><i class="ico-gmu your-plan"></i> <?= Yii::t('billing', '_ACTIVE_SUBSCRIPTION') ?></h3>
	<p>
		<strong><?= Yii::t('billing', '_PAYMENT_CYCLE') ?>:</strong> <?= Yii::t('userlimit', '_CYCLE_TYPE_' . strtoupper($lmsOrderInfo['payment_cycle'])) ?><br/>
		<?php $dateText = $lmsOrderInfo['subscription_status'] != 'user_cancelled_sub' ? Yii::t('billing', '_NEXT_DUE_DATE') : Yii::t('standard', '_EXPIRATION_DATE') ; ?>
		<strong><?= $dateText ?>:</strong> <?= date('d/m/Y', strtotime($lmsOrderInfo['payment_expire_date'])) ?><br/>


		<strong><?= Yii::t('billing', '_PAYMENT_METHOD') ?>:</strong> <?= Docebo::$PAYMENT_METHOD[$lmsOrderInfo['payment_method']] ?>
		<?php
			if ($lmsOrderInfo['payment_method'] == Docebo::PAYMENT_METHOD_CREDIT_CARD) {
				echo "<br>";
				echo $lmsOrderInfo['ccard_institute'] . " ****-****-****-" . $lmsOrderInfo['ccard_last_four'];
			}
		?>


	</p>

	<?php if($lmsOrderInfo['payment_method'] == Docebo::PAYMENT_METHOD_CREDIT_CARD) : ?>
		<a 	href="<?= $editCCardInfoUrl ?>"
			class="open-dialog btn-docebo grey full"
			rel="edit-creditcard-modal"><?= Yii::t('userlimit', '_UPGRADE_CCARD_INFO') ?></a>
		<br />
	<?php endif; ?>

	<h3><i class="ico-gmu billing"></i> <?= Yii::t('billing', '_BILLING_INFORMATION') ?></h3>
	<p>
		<?= $customerInfo['info']['company_name'] ?><br/>
		<?= $customerInfo['info']['address'] ?><br/>
		<?= $customerInfo['info']['city'] ?>, <?= $customerInfo['info']['state_province'] ?><br/>
		<?= $countriesArray[$customerInfo['info']['country']] ?><br/>
		<?= $customerInfo['info']['vat'] ?>
	</p>
	<a href="<?= $invoiceListUrl ?>" class="btn-docebo grey full">
		<?= Yii::t('billing', 'View invoices') . ' (' . $billingInfo['data']['totalRecords'] . ')'; ?>
	</a>
</div>


<div id="orders-list"></div>



<script type="text/javascript">
<!--

	$(document).ready(function(){
		$('#active-users').tooltip();
		$('#get-more-user').controls();
		$(document).on('dialog2.closed', '#cancel-modal', function(){
			if($(this).find('.reload-after-close').length > 0){
				window.location.reload();
			}
		});
	});


//-->
</script>