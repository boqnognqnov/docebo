<?php
/**
 *
 * Module implementing "Buy LMS Users, Upgrade, Downgrade" and other LMS billing/business logic and MVC
 *
 */
class BillingBaseController extends Controller {

	protected $erp_installation_id = '';
	protected $erp_key = '';
	protected $erp_secret_key = '';
	protected $install_from = '';


	/**
	 * (non-PHPdoc)
	 * @see Controller::init()
	 */
	public function init() {

		parent::init();

		// Set these and make them available to all descendant controllers
		// Missing any of the "erp_*" is a quaranteed failure to do any serious business,
		// maybe it is good idea to check and redirect
		$this->erp_installation_id = Settings::getCfg('erp_installation_id');
		$this->erp_key = Settings::getCfg('erp_key');
		$this->erp_secret_key = Settings::getCfg('erp_secret_key');
		$this->install_from = Settings::getCfg('install_from');


	}

	/**
	 * (non-PHPdoc)
	 * @see Controller::resolveLayout()
	 */
	public function resolveLayout()
	{
		$this->layout = '/layouts/base';
	}



	/**
	 * (non-PHPdoc)
	 * @see CController::beforeAction()
	 */
	public function beforeAction($action)
	{
		$this->registerResources();
		return parent::beforeAction($action);
	}



	/**
	 *
	 * Register/publish controller specific scripts/css/resources/assets...
	 *
	 */
	public function registerResources() {
		if (!Yii::app()->request->isAjaxRequest) {

			$assetsUrl = $this->module->assetsUrl;
			$cs = Yii::app()->getClientScript();

			// Register js
			$cs->registerScriptFile($assetsUrl . '/js/billing.js');

			// Register css
			$cs->registerCssFile($assetsUrl . '/css/billing.css');



			// Register JS script(s)
			$options = array();
			$options = CJavaScript::encode($options);
			$script = "Billing = new BillingModule($options);";
			$cs->registerScript("orderJs1", $script , CClientScript::POS_HEAD);


		}
	}



	/**
	 *
	 * @param boolean $changeCC
	 * @return string
	 */
	protected function getOrderModuleUrl($changeCC = false)
	{
		if ($changeCC)
			return rtrim(Yii::app()->params['erp_order_url'],'/') . '/en/billing/billing/change-credit-card' . '?lang='.Yii::app()->getLanguage();
		else
			return rtrim(Yii::app()->params['erp_order_url'],'/') . '/en/billing/billing' . '?lang='.Yii::app()->getLanguage();
	}


	/**
	 * Return sha1() key of the information being sent to ERP Order IFRAME
	 *
	 * @param unknown $installation_id
	 * @param unknown $install_info
	 * @param unknown $time
	 * @throws CException
	 * @return string
	 */
	protected function getOrderModuleHash($installation_id, $install_info, $time) {
		
		$domain_name =preg_replace('/^www\\./', '', strtolower($_SERVER['HTTP_HOST']));
		
		$erp_secret_key = Settings::getCfg('erp_secret_key', false);


		if (empty($domain_name) || !$erp_secret_key) {
			return false;
		}

		$info_arr = explode('_', $install_info);
		$actv_users = (int)$info_arr[0];
		$free_users = (int)$info_arr[1];

		$key = sha1(
				$domain_name.'|'.
				$installation_id.'|'.
				$actv_users.'|'.
				$free_users.'|'.
				$time.'|'.
				$erp_secret_key
		);

		return $key;


	}



}