<?php
class Billing extends CApplicationComponent {

	public static function getPaymentMethod()
	{
		$method = Settings::get('payment_method', null);
		if ($method == null)
		{
			$installParams = CJSON::decode(ErpApiClient::apiErpGetSaasOrderInfo(array('installation_id' => Docebo::getErpInstallationId())));
			if (isset($installParams['payment_method']))
			{
				$method = $installParams['payment_method'];
				Settings::save('payment_method', $method);
			}
		}
		return $method;
	}

	public static function isWireTransfer()
	{
		$method = self::getPaymentMethod();
		return ($method == 'wire_transfer') ? true : false;
	}

	/**
	 * Return specific array with subscriptions periods in a array
	 * ordered from the first subscription period up (ascending).
	 * Period dates include hh:mm:ss !
	 *
	 * @return array
	 */
	public static function getSubscriptionPeriodDates() {
		$data = array();
		$subscription_begin_date = Settings::get('subscription_begin_date');
		if ($subscription_begin_date && strtotime($subscription_begin_date)) {
			$tmp_end_date = $subscription_begin_date;
			$timeNow = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'));
			$first = true;
			while(strtotime($tmp_end_date) < $timeNow) {
				$period_date_from = $tmp_end_date;
				
				// Use DateTime to add 1 month (and set the new beginning) then substract 1 day for the end of current period
				$dateTmp = new DateTime($tmp_end_date);
				$tmp_end_date 	= $dateTmp->add(new DateInterval('P1M'))->format('Y-m-d');  // add 1 month
				$period_date_to = $dateTmp->sub(new DateInterval('P1D'))->format('Y-m-d');  // substract 1 day
				
				// Fix hours: if this is NOT the first period, use "begin of day hour" 
				if (!$first) {
					$period_date_from = $period_date_from . " 00:00:00";
				}
				
				// Use "end of day hour" for the end o fthe period
				$period_date_to = $period_date_to . " 23:59:59";
				$data[] = array(
					'from'=>$period_date_from,
					'to'=>$period_date_to,
				);
				$first = false;
			}

		}
		return $data;
	}
}