<?php

interface IPlayerBlockContent {

	/**
	 * 
	 */
	public function myAttributeLabels();
	
	/**
	 * 
	 */
	public function myRules();
	
	/**
	 * 
	 */
	public function getContent();
	
	/**
	 * Returns block title; if empty, Block system title will be used (from Database)
	 */
	public function getTitle();
	

	/**
	 * Returns a raw extra HTML code to be put in block header
	 */
	public function getExtraHeaderHtml();
	
	/**
	 *
	 */
	
	/**
	 * Returns an array of HTML, each put inside &lt;li&gt;&lt;/li&gt; in Block 'system' menu
	 */
	public function getExtraMenuActions();
	
	
	
}
