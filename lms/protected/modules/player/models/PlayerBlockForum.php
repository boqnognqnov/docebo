<?php
/**
 * Behavior attached to PlayerBaseblock model.
 * Provides methods to deliver its own content.
 *
 * Name pattern for block content behaviors:  PlayerBlock<Contenttype>
 * where <Contenttype> is one of UC-first-ed TYPE_* from PlayerBaseBlock model
 * (TYPE_COURSE_INFO, TYPE_COMMENTS, ... etc)
 *
 * @author Plamen Petkov
 *
 */
class PlayerBlockForum extends CActiveRecordBehavior  implements IPlayerBlockContent {

	/**
	 * Construct this behavior.
	 *
	 * @param string $params  (JSON string, kept in main block table)
	 */
	public function __construct($params = false) {

	}

	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::myAttributeLabels()
	 */
	public function myAttributeLabels() {
		return array(
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::myRules()
	 */
	public function myRules() {
		return array(
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getContent()
	 */
	public function getContent() {

		$criteria = new CDbCriteria();
		$criteria->addCondition('idCourse=:course_id');
		$criteria->params[':course_id'] = $this->owner->course_id;

		if(!Yii::app()->controller->userCanAdminCourse && !(LearningCourseuser::userLevel(Yii::app()->user->id,$this->owner->course_id) == LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR))
		{
			$block_model = PlayerBaseblock::model()->findByAttributes(array('course_id' => $this->owner->course_id, 'content_type' => 'comments'));
			if($block_model)
			{
				if(isset($params['forum_id']))
				{
					$params = CJSON::decode($block_model->params);
					$criteria->addCondition('idForum <> :id_forum');
					$criteria->params[':id_forum'] = $params['forum_id'];
				}
			}
		}

		$criteria->order = "sequence ASC";

		$forums = LearningForum::model()->findAll($criteria);

		return Yii::app()->controller->renderPartial('player.views.block.'.$this->owner->content_type.'.index', array(
			'blockModel' => $this->owner,
			'forums' => $forums
		), true);
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getTitle()
	 */
	public function getTitle() {
		return Yii::t('course', 'Forums');
	}


	/**
	 *
	 */
	public function destringifyParams() {
		$params = CJSON::decode($this->owner->params);
	}


	/**
	 * (non-PHPdoc)
	 * @see CActiveRecordBehavior::beforeSave()
	 */
	public function beforeSave($event)  {
		$this->owner->params = CJSON::encode(get_object_vars($this));
		return true;
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getExtraHeaderHtml()
	 */
	public function getExtraHeaderHtml() {
		return Yii::app()->controller->renderPartial('player.views.block.forum._extra_header_html', array(
				'blockModel' => $this->owner,
		), true);
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getExtraMenuActions()
	 */
	public function getExtraMenuActions() {

        $settingsUrl = Yii::app()->controller->createUrl('//forum/default/axRatingSettings', array('block_id'=>$this->owner->id, 'course_id'=>$this->owner->course_id));

        $result = array('<a href="'.$settingsUrl.'" class="open-dialog p-hover" data-dialog-class="player-forum-rating-settings-page"><i class="i-sprite is-gearpair"></i> '.Yii::t('standard', 'Settings').'</a>');

		return $result;
	}


}


