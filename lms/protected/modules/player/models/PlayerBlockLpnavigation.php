<?php
/**
 * 
 *
 */
class PlayerBlockLpnavigation extends CActiveRecordBehavior implements IPlayerBlockContent
{

    public function __construct($params = false) {

    }

    /**
     *
     */
    public function myAttributeLabels()
    {
        return array();
    }

    /**
     *
     */
    public function myRules()
    {
        return array(
        );
    }

    /**
     *
     */
    public function getContent()
    {
        $course = Yii::app()->player->course;
        $isPartOfLp = false;
        $contentType = $this->owner->content_type;
        $userId = Yii::app()->user->idst;
        $columnsSpan = $this->owner->columns_span;
        //Enrolled in Learning Plan?
        $enrolled = false;
        //Will contains the message if the user is not enrolled in any Learning Plan or we cannot recognize from witch Learning Plan he comes
        $msg = null;
        //Course that are in the Learning Plan
        $courses = array();
        //LearningCourseUser for each course - array index is id of the course!!!
        $courseUser = array();
        //Contains data for the chart (percentage, total, etc.)
        $pathData = array();
        //LearningCoursepath data
        $endResult = null;
        //Learning Plans courses details
        $pathCourses = array();

        $isPartOfLp = LearningCoursepathCourses::isCourseAssignedToLearningPaths($course->idCourse);
        if($isPartOfLp){
            $lpEnrollments = LearningCoursepath::checkEnrollment($userId, $course->idCourse, true);
            if(!empty($lpEnrollments)){
                $enrolled = true;

                $countLp = count($lpEnrollments);

                $idPath = null;

                /**
                 *  If we found just one course - this will be the choose one!
                 *
                 * Else if more than one course checks if we came from some Learning Plan page( check is made in PlayerBaseController )
                 */
                if($countLp == 1){
                    // It was like $lpEnrollments[1], which is weird; index of the first element was unknown!
                    // So we need just the FIRST (and only) element!
                    $first = reset($lpEnrollments);
                    $idPath = $first['id_path'];
                } else {
                    if (empty(Yii::app()->player->coursePath)) {
                        $enrolled = false;
                        $msg = Yii::t('curricula_management', 'You have multiple active learning plans containing this course.');
                    }
                    else{
                        $idPath = Yii::app()->player->coursePath->id_path;
                    }
                }

                if($enrolled){
                    $criteria = new CDbCriteria();
                    $criteria->with['coreUsers'] = array('joinType' => 'INNER JOIN');
                    $criteria->with['certificate'] = array('joinType' => 'LEFT JOIN');
                    $criteria->addCondition('coreUsers_coreUsers.idUser = :idUser');
                    $criteria->addCondition('t.id_path = :id_path');
                    $criteria->params[':idUser']   = $userId;
                    $criteria->params[':id_path'] = $idPath;
                    $criteria->order = 't.path_name ASC';
                    $criteria->group = 't.id_path';

                    $endResult = LearningCoursepath::model()->with('coursesCount')->find($criteria);
                    $pathData = $endResult->getCoursepathPercentage();
                    $pathCourses = $endResult->getCoursepathCourseDetails();

                    /**
                     * Build arrays of data - course, enrollments (with id of his course)
                     */
                    foreach($pathCourses as $key => $course){

                        $courses[] = $course->learningCourse;

                        $_courseUser = LearningCourseuser::model()->findByAttributes(array(
                            'idUser' => $userId,
                            'idCourse' => $course->learningCourse->idCourse
                        ));

                        if($_courseUser instanceof LearningCourseuser){
                            $courseUser[$course->learningCourse->idCourse] = $_courseUser;
                        }
                    }
                }
            } else{
                $msg = Yii::t('curricula_management', 'You are not enrolled to any Learning Plans containing this course!');
            }
        } else{
            $msg = Yii::t('curricula_management', 'You course does not belong to any Learning Plan');
        }

        return Yii::app()->controller->renderPartial('player.views.block.' . $contentType . '.index', array(
            'courses' => $courses,
            'courseUser' => $courseUser,
            'pathData' => $pathData,
            'pathCourses' => $pathCourses,
            'data' => $endResult,
            'size' => ($columnsSpan == 12 ? 'big' : 'small'),
            'enrolled' => $enrolled,
            'msg' => $msg
        ));
    }

    /**
     * Returns block title; if empty, Block system title will be used (from Database)
     */
    public function getTitle()
    {
        return Yii::t("curricula_management", 'Learning Plan navigation');
    }

    /**
     * Returns a raw extra HTML code to be put in block header
     */
    public function getExtraHeaderHtml()
    {
        // TODO: Implement getExtraHeaderHtml() method.
    }

    /**
     * Returns an array of HTML, each put inside &lt;li&gt;&lt;/li&gt; in Block 'system' menu
     */
    public function getExtraMenuActions()
    {
        // TODO: Implement getExtraMenuActions() method.
    }

    public function destringifyParams() {
        $params = CJSON::decode($this->owner->params);
    }

    public function beforeSave($event)  {
        $this->owner->params = CJSON::encode(get_object_vars($this));
        return true;
    }
}