<?php
/**
 * Behavior attached to PlayerBaseblock model.
 * Provides methods to deliver its own content.
 *
 * Name pattern for block content behaviors:  PlayerBlock<Contenttype>
 * where <Contenttype> is one of UC-first-ed TYPE_* from PlayerBaseBlock model
 * (TYPE_COURSE_INFO, TYPE_COMMENTS, ... etc)
 *
 * @author Plamen Petkov
 *
 */
class PlayerBlockIframe extends CActiveRecordBehavior  implements IPlayerBlockContent {

    /**
	 * Construct this behavior.
	 *
	 * @param string $params  (JSON string, kept in main block table)
	 */
	public function __construct($params = false) {

	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::myAttributeLabels()
	 */
	public function myAttributeLabels() {
		return array(
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::myRules()
	 */
	public function myRules() {
		return array(
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getContent()
	 */
	public function getContent() {

		$blockModel   = $this->owner;
		$course_id    = $blockModel->course_id;
		$user_id      = Yii::app()->user->id;

		$courseUserModel = LearningCourseuser::model()->findByAttributes(array("idUser" => $user_id, "idCourse" => $course_id));

		$level = $courseUserModel->level;
		$allowAdminOperations = false; //initialize with default value
		if ((Yii::app()->user->isGodAdmin)) {
			$allowAdminOperations = true;
		} elseif (Yii::app()->user->getIsPu()) {
			// Is this a power user and the course was assigned to him?
			$pUserCourseAssigned = CoreUserPuCourse::model()->findByAttributes(array(
				'puser_id' => Yii::app()->user->id,
				'course_id' => $course_id
			));
			$allowAdminOperations = (!empty($pUserCourseAssigned) && Yii::app()->user->checkAccess('/lms/admin/course/mod'));
		} else {
			$allowAdminOperations = ($level > LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT && $level != LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR);
		}

		return Yii::app()->controller->renderPartial('player.views.block.'.$this->owner->content_type.'.index', array(
			'blockModel' => $this->owner,
			"allowAdminOperations" => $allowAdminOperations,
		), true);
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getTitle()
	 */
	public function getTitle() {
		return Yii::t("player", 'External content');
	}


	/**
	 *
	 */
	public function destringifyParams() {
		$params = CJSON::decode($this->owner->params);
	}


	/**
	 * (non-PHPdoc)
	 * @see CActiveRecordBehavior::beforeSave()
	 */
	public function beforeSave($event)  {
		if($this->owner->scenario !== 'WidgetParamsSaved'){
			$this->owner->params = CJSON::encode(get_object_vars($this));
		}
		return true;
	}

	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getExtraHeaderHtml()
	 */
	public function getExtraHeaderHtml($userLevel = false, $userCanAdminCourse = false) {
	    return '';
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getExtraMenuActions()
	 */
	public function getExtraMenuActions() {
		
		$settingsUrl = Yii::app()->controller->createUrl('/player/block/axIframeSettings', array(
		    'block_id'=>$this->owner->id, 
		    'course_id'=>$this->owner->course_id,
		));
		$result = array('<a href="'.$settingsUrl.'" class="open-dialog p-hover" rel="widget-iframe-settings-'.$this->owner->id.'"><i class="i-sprite is-gearpair"></i> '.Yii::t('standard', 'Settings').'</a>');
		return $result;
	}
	
	
	public function getIframeUrl($url, $courseId, $oauthClient, $secretSalt) {

		if($url && $courseId && $oauthClient && $secretSalt) {

			$courseCode = LearningCourse::model()->findByAttributes(array('idCourse' => $courseId))->code;

			// Parse current iframe url
			$url_parts = parse_url($url);
			parse_str($url_parts['query'], $params);
				
			$paramsToHash = array(
				'course_id' => $courseId,
				'course_code' => $courseCode,
				'user_id' => urlencode(Yii::app()->user->getId()),
				'username' => urlencode(Yii::app()->user->getUsername()),
				'auth_code' => $this->getOAuthAuthorizationCode($oauthClient),
			);
			
			$params = array_merge($params, $paramsToHash);

			// Calculate the sha1 hash of the previous params + the secret
			$paramsToHash = array_merge($paramsToHash, array($secretSalt));
			$params['hash'] = hash("sha256", implode(",", $paramsToHash));

			$fragment = !empty($url_parts["fragment"]) ? "#" . $url_parts["fragment"] : "";

			// this will url_encode all values
			$url_parts['query'] = http_build_query($params);
			
			$url = $url_parts['scheme'] . '://' . $url_parts['host'] . $url_parts['path'] . '?' . $url_parts['query']  . $fragment;
			
		} else {
			$url = "";
		}

		return $url;
	}

	private function getOAuthAuthorizationCode($clientId)
	{
		$authCode = "";
		$server = DoceboOauth2Server::getInstance();
		if ($server) {
			$server->init();
			$authCode = $server->generateAuthorizationCode(Yii::app()->user->id, $clientId, null, 'api', 30);
		}
		return $authCode;
	}

}


