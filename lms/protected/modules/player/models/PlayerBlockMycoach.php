<?php
/**
 * Behavior attached to PlayerBaseblock model.
 * Provides methods to deliver its own content.
 *
 * Name pattern for block content behaviors:  PlayerBlock<Contenttype>
 * where <Contenttype> is one of UC-first-ed TYPE_* from PlayerBaseBlock model
 * (TYPE_COURSE_INFO, TYPE_COMMENTS, ... etc)
 *
 * @author Georgi Stefkoff
 *
 */

class PlayerBlockMycoach extends CActiveRecordBehavior implements IPlayerBlockContent{
    
    public $email_contact = false;
    public $time_compared = false;
    public $addition_field = false;
    public $msg_system = false;
    public $webinar_system = false;
    private $idCoachingSession = false;

    public function __construct($params = false) {

    }
    
    public function getContent() {
        $blockModel = $this->owner;
        $isUserInSession = false;
        $contentType = $this->owner->content_type;
        $isUserCoach = false;
        $user_idst = Yii::app()->user->idst;
        $courseId = $blockModel->course_id;
        $now = new DateTime();

        $result = $this->getCoachingSession();

        $coachSession = LearningCourseCoachingSession::model()->find('idSession = :session', array(':session' => $result['idSession']));
                  
        
        if(!empty($coachSession) || $coachSession['idCoach'] == $user_idst){
            $courseUser = LearningCourseuser::model()->find('idCourse = :course AND idUser = :user', array(
                ':course' => $courseId,
                ':user' => $user_idst
            ));            
            $isUserInSession = true;
            $userLevel = $courseUser->level;
            
            if($userLevel == 8){
                $isUserCoach = true;
            }                                  
            
            $userModel = CoreUser::model()->find('idst = :user', array(
                ':user' => $user_idst
            ));
            
            $coachModel = CoreUser::model()->find('idst = :coach', array(
                ':coach' => $coachSession['idCoach']
            ));            
            
            if(!empty($this->addition_field)){
                $command = Yii::app()->db->createCommand();
                $command->select('f.translation as translation, fu.user_entry as user_entry, fs.translation as tr, f.type_field as type');
                $command->from('core_field f');
                $command->where('f.id_common IN (' . implode(',', $this->addition_field) .') AND f.lang_code = :lang');
                $command->join('core_field_userentry fu', 'fu.id_common = f.id_common AND fu.id_user = :user');
                $command->leftJoin("core_field_son fs", "f.type_field = 'dropdown' AND fs.idField = f.id_common AND fs.lang_code = :lang");
                $command->group('fu.user_entry');
                $params = array(
                    ':lang' => Lang::getCodeByBrowserCode(Yii::app()->getLanguage()),
                    ':user' => $coachSession['idCoach']
                );
                $additionalField = $command->queryAll(true, $params);                        
            }
                        
            $date = new DateTime();
            $learnerTimezone = Yii::app()->localtime->getTimezoneFromSettings($user_idst);                        
            $date->setTimezone(new DateTimeZone($learnerTimezone));            
            $learnerTime = Yii::app()->localtime->toLocalTime($date->format('H:i'));
            
            $coachTimezone = Yii::app()->localtime->getTimezoneFromSettings($coachModel->idst);            
            $date->setTimezone(new DateTimeZone($coachTimezone));            
            $coachTime = Yii::app()->localtime->toLocalTime($date->format('H:i'));
                       
            $sessionStartDate = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($coachSession['datetime_start']));
            $sessionEndDate = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($coachSession['datetime_end']));
            $sessionDaysLeft = Yii::app()->localtime->countDayDiff($coachSession['datetime_end'], Yii::app()->localtime->UTCNow);
            $isSessionExpired = (Yii::app()->localtime->isGt(Yii::app()->localtime->UTCNow, $coachSession['datetime_end']));
            
            $assignedUsers = $coachSession->countAssignedUsers();                                                            
            
            return Yii::app()->controller->renderPartial('player.views.block.' . $contentType . '.index', array(
                'blockModel' => $blockModel,
                'additionalFields' => $additionalField,
                'coachSession' => $coachSession,
                'coachModel' => $coachModel,
                'daysLeft' => $sessionDaysLeft,
                'learnerTime' => $learnerTime,
                'userModel' => $userModel,
                'isUserCoach' => $isUserCoach,
                'sessionStartDate' => $sessionStartDate,
                'sessionEndDate' => $sessionEndDate,
                'assignedUsers' => $assignedUsers,                
                'coachTime' => $coachTime,
                'isSessionExpired' => $isSessionExpired,
                'idSession' => Yii::app()->request->getParam('id_session', false)
            ), true);
        }
                               
        return Yii::app()->controller->renderPartial('player.views.block.' . $contentType . '.not_entolled_user', array(
            'blockModel' => $blockModel,
            'idSession' => Yii::app()->request->getParam('id_session', false)
        ), true);
    }

    public function getExtraHeaderHtml() {
        $result = $this->getCoachingSession();
        $coachingSession = LearningCourseCoachingSession::model()->findByPk($result['idSession']);
        if(($coachingSession->idCoach == Yii::app()->user->id) && $this->webinar_system)
            return '<a
                href="/lms/index.php?r=player/block/axCreateCoachingWebinarSession&course_id='.$this->owner->course_id.'&coaching_session_id='.$result['idSession'].'"
                class="open-dialog block-videoconf-add-room"
                data-dialog-class="modal-create-webinar-session coaching-webinar"
                rel="dialog-handle-room"
                data-block-id="<?= $blockModel->id ?>"
                title="'.Yii::t('coaching', 'New live session').'">
                <span class="p-sprite add-block-item"></span>
            </a>';
        else
            return '';
    }

    public function getExtraMenuActions() {
        return '';
    }

    public function getTitle() {        
        return Yii::t('coaching', 'My Coach');
    }

    public function myAttributeLabels() {
        return array();
    }

    public function myRules() {
        return array(
            array('email_contact, time_compared, msg_system, webinar_system', 'numerical'),
            array('addition_field', 'safe')
        );
    }
    
    public function beforeSave($event) {
        $this->owner->params = CJSON::encode(get_object_vars($this));
    }
    
    /**
	 * Convert loaded block parameters to THIS object's public attributes (the bevhavior object, not base model!)
	 */
	public function destringifyParams() {        
		$params = CJSON::decode($this->owner->params);        
		if (is_array($params) && count($params) > 0) {
			foreach($params as $varName => $varValue ){
				$this->$varName = $varValue;
			}
		}
	}

    public function getCoachingSession(){
        $blockModel = $this->owner;
        $isUserInSession = false;
        $contentType = $this->owner->content_type;
        $isUserCoach = false;
        $user_idst = Yii::app()->user->idst;
        $courseId = $blockModel->course_id;
        $now = new DateTime();


        $params = array(
            ':now' => $now->format('Y-m-d'),
            ':course' => $courseId,
            ':user' => $user_idst
        );
        //Select the active session in the moment, or the Next available session or the last active before todoy
        //If there is a better solution, please fix it
        $result = null;
        $command = Yii::app()->db->createCommand();
        $command->select('s.idSession')
            ->from('learning_course_coaching_session s')
            ->leftJoin('learning_course_coaching_session_user su', '(su.idSession = s.idSession)')
            ->where('(s.datetime_start <= :now AND s.datetime_end >= :now) AND (s.idCourse = :course AND (s.idCoach = :user OR su.idUser = :user))');
        $result1 = $command->queryRow(true, $params);
        if(empty($result1)){
            $command1 = Yii::app()->db->createCommand();
            $command1->select('s.idSession')
                ->from('learning_course_coaching_session s')
                ->leftJoin('learning_course_coaching_session_user su', '(su.idSession = s.idSession)')
                ->where('s.datetime_start > :now AND s.idCourse = :course AND (s.idCoach = :user OR su.idUser = :user) ORDER BY s.datetime_start ASC LIMIT 1');
            $result2 = $command1->queryRow(true, $params);
            if(empty($result2)){
                $command2 = Yii::app()->db->createCommand();
                $command2->select('s.idSession')
                    ->from('learning_course_coaching_session s')
                    ->leftJoin('learning_course_coaching_session_user su', '(su.idSession = s.idSession)')
                    ->where('s.datetime_end < :now AND s.idCourse = :course AND (s.idCoach = :user OR su.idUser = :user) ORDER BY s.datetime_end DESC LIMIT 1');
                $result3 = $command2->queryRow(true, $params);
                if(!empty($result3)){
                    $result = $result3;
                }
            } else{
                $result = $result2;
            }
        } else{
            $result = $result1;
        }
        return $result;
    }
}
