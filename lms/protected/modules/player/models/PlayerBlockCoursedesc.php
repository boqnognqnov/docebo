<?php
/**
 * Behavior attached to PlayerBaseblock model.
 * Provides methods to deliver its own content.
 *
 * Name pattern for block content behaviors:  PlayerBlock<Contenttype>
 * where <Contenttype> is one of UC-first-ed TYPE_* from PlayerBaseBlock model
 * (TYPE_COURSE_INFO, TYPE_COMMENTS, ... etc)
 *
 * @author Plamen Petkov
 *
 */
class PlayerBlockCoursedesc extends CActiveRecordBehavior implements IPlayerBlockContent  {

    
    public $show_desc = true;
    public $additional_fields = null;
    
	/**
	 * Construct this behavior.
	 *
	 * @param string $params  (JSON string, kept in main block table)
	 */
	public function __construct($params = false) {

	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::myAttributeLabels()
	 */
	public function myAttributeLabels() {
		return array(
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::myRules()
	 */
	public function myRules() {
		return array(
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getContent()
	 */
	public function getContent() {
		$contentType = $this->owner->content_type;		
		return Yii::app()->controller->renderPartial('player.views.block.'.$contentType.'.index', array(
			'blockModel' => $this->owner,
			'courseModel' => LearningCourse::model()->findByPk($this->owner->course_id),
			'widget' => $this
		), true);
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getTitle()
	 */
	public function getTitle() {
		return Yii::t('certificate', '_COURSE_DESCRIPTION');
	}


	/**
	 *
	 */
	public function destringifyParams() {
		$params = CJSON::decode($this->owner->params);
        if (is_array($params) && count($params) > 0) {
			foreach($params as $varName => $varValue ){
				$this->$varName = $varValue;
			}
		}
	}


	/**
	 * (non-PHPdoc)
	 * @see CActiveRecordBehavior::beforeSave()
	 */
	public function beforeSave($event)  {
		$this->owner->params = CJSON::encode(get_object_vars($this));
		return true;
	}

	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getExtraHeaderHtml()
	 */
	public function getExtraHeaderHtml() {
		return '';
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getExtraMenuActions()
	 */
	public function getExtraMenuActions() {
		$settingsUrl = Docebo::createAdminUrl('additionalCourseFields/axCourseDescWidgetSettings', array('block_id'=>$this->owner->id, 'course_id'=>$this->owner->course_id));
        $result = array(
			'<a href="'.$settingsUrl.'" class="open-dialog p-hover" data-dialog-class="course-desc-settings" rel="update-block-dialog-' . $this->owner->id .'"><i class="i-sprite is-gearpair"></i> '.Yii::t('standard', 'Settings').'</a>');
		return $result;
	}

	/**
	 * Determines the set of field name/values to display in this widget
	 * @return array
	 */
	public function getAdditionalFieldsToDisplay() {
		$additionalFields = array();
		$params = CJSON::decode($this->owner->params);
		if(is_array($params) && isset($params['additional_fields'])) {
			$fields = $params['additional_fields'];
			foreach ($fields as $index => $field) {
				$fieldModel = LearningCourseField::model()->findByPk($field); /** @var $fieldModel LearningCourseField */
				if (!$fieldModel || ($fieldModel->invisible_to_user == 0))
					continue;

				$tempValue = Yii::app()->getDb()->createCommand()
					->select('field_' . $field)
					->from(LearningCourseFieldValue::model()->tableName() . ' v')
					->where('v.id_course=:id_course', array(':id_course' => $this->owner->course_id))
					->queryScalar();
				if($tempValue) {
					$fieldModel->course = LearningCourse::model()->findByPk($this->owner->course_id);
					$className = 'CourseField' . ucfirst($fieldModel->type);
					$fieldValue = $className::renderFieldValue($tempValue, $fieldModel, 'course_description');
					$in_column = method_exists($className, "shouldDisplayInColumn") ? $className::shouldDisplayInColumn('course_description') : true;
					if (empty($fieldValue) || $fieldValue === false || $fieldValue === '')
						continue;

					$additionalFields[$fieldModel->getTranslation()] = array('value' => $fieldValue, 'in_column' => $in_column);
				}
			}
		}

		return $additionalFields;
	}
}


