<?php
/**
 * Behavior attached to PlayerBaseblock model.
 * Provides methods to deliver its own content.
 *
 * Name pattern for block content behaviors:  PlayerBlock<Contenttype>
 * where <Contenttype> is one of UC-first-ed TYPE_* from PlayerBaseBlock model
 * (TYPE_COURSE_INFO, TYPE_COMMENTS, ... etc)
 *
 * @author Plamen Petkov
 *
 */
class PlayerBlockComments extends CActiveRecordBehavior  implements IPlayerBlockContent {

	public $forum_id = false;

	/**
	 * Construct this behavior.
	 *
	 * @param string $params  (JSON string, kept in main block table)
	 */
	public function __construct($params = false) {

	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::myAttributeLabels()
	 */
	public function myAttributeLabels() {
		return array(
				'forum_id' => 'Forum',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::myRules()
	 */
	public function myRules() {
		return array(
			array('forum_id', 'required'),
			array('forum_id', 'numerical', 'integerOnly'=>true),
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getContent()
	 */
	public function getContent($options = array()) {

		$blockModel = $this->owner;
		$forum_id = $blockModel->forum_id;

		$discussionsOffset = isset($options["discussions_offset"]) ? $options["discussions_offset"] : 0;
		$discussionsLimit  = isset($options["discussions_limit" ]) ? $options["discussions_limit"] :
								Yii::app()->params["player"]["comments_block_discussions"];

		$commentsOffset = isset($options["comments_offset"]) ? $options["comments_offset"] : 0;
		$commentsLimit  = isset($options["comments_limit" ]) ? $options["comments_limit"] :
								Yii::app()->params["player"]["comments_block_comments"];

		$threadId = isset($options["thread_id"]) ? $options["thread_id"] : false;
		$threadId = (int) $threadId;


		// Modify loaded html according to caller's request
		$showViewAll = isset($options["show_viewall"]) ? $options["show_viewall"] : true;
		$showAllComments = isset($options["show_all_comments"]) ? $options["show_all_comments"] : false;
		$showCreateDiscussion = isset($options["show_create_discussion"]) ? $options["show_create_discussion"] : true;
		$showLoadMore = isset($options["show_load_more"]) ? $options["show_load_more"] : true;
		$InitBlocksJs = isset($options["init_blocks_js"]) ? $options["init_blocks_js"] : true;

		$forumModel 		= LearningForum::model()->findByPk($blockModel->forum_id);

		$criteria = new CDbCriteria();
		$criteria->addCondition("idForum=:forum_id");
		$criteria->params = array(":forum_id" => $forum_id);

		if ($threadId > 0) {
			$criteria->addCondition("idThread = :id_thread");
			$criteria->params[':id_thread'] = $threadId;
			$countTotalDiscussions = LearningForumthread::model()->count($criteria);
		}
		else {
			$countTotalDiscussions = LearningForumthread::model()->count($criteria);
			$criteria->offset = $discussionsOffset;
			$criteria->limit  = $discussionsLimit;
		}

		$criteria->order = "posted DESC";

		$discussions 		= LearningForumthread::model()->findAll($criteria);

		$currentUserModel  = CoreUser::model()->findByPk(Yii::app()->user->id);

		return Yii::app()->controller->renderPartial('player.views.block.comments.index', array(
			'blockModel' 			=> $this->owner,
			'forumModel' 			=> $forumModel,
			'discussions'	 		=> $discussions,
			'currentUserModel'		=> $currentUserModel,
			'commentsOffset'		=> $commentsOffset,
			'commentsLimit'			=> $commentsLimit,
			'discussionsOffset' 	=> $discussionsOffset,
			'discussionsLimit'  	=> $discussionsLimit,
			'showViewAll'			=> $showViewAll,
			'showAllComments'		=> $showAllComments,
			'showCreateDiscussion'  => $showCreateDiscussion,
			'showLoadMore'			=> $showLoadMore,
			'InitBlocksJs'			=> $InitBlocksJs,
			'countTotalDiscussions'	=> $countTotalDiscussions,
		), true);

	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getTitle()
	 */
	public function getTitle() {
		return Yii::t('standard', '_COMMENTS');
	}


	/**
	 * Convert loaded block parameters to THIS object's public attributes (the bevhavior object, not base model!)
	 */
	public function destringifyParams() {
		$params = CJSON::decode($this->owner->params);
		if (is_array($params) && count($params) > 0) {
			foreach($params as $varName => $varValue ){
				$this->$varName = $varValue;
			}
		}
	}


	/**
	 * (non-PHPdoc)
	 * @see CActiveRecordBehavior::beforeSave()
	 */
	public function beforeSave($event)  {
		$this->owner->params = CJSON::encode(get_object_vars($this));
		return true;
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getExtraHeaderHtml()
	 */
	public function getExtraHeaderHtml() {
		return '';
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getExtraMenuActions()
	 */
	public function getExtraMenuActions() {
		return '';
	}


}


