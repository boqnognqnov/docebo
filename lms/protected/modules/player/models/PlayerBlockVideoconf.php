<?php
/**
 * Behavior attached to PlayerBaseblock model.
 * Provides methods to deliver its own content.
 *
 * Name pattern for block content behaviors:  PlayerBlock<Contenttype>
 * where <Contenttype> is one of UC-first-ed TYPE_* from PlayerBaseBlock model
 * (TYPE_COURSE_INFO, TYPE_COMMENTS, ... etc)
 *
 * @author Plamen Petkov
 *
 */
class PlayerBlockVideoconf extends CActiveRecordBehavior  implements IPlayerBlockContent {

	/**
	 * Construct this behavior.
	 *
	 * @param string $params  (JSON string, kept in main block table)
	 */
	public function __construct($params = false) {

	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::myAttributeLabels()
	 */
	public function myAttributeLabels() {
		return array(
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::myRules()
	 */
	public function myRules() {
		return array(
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getContent()
	 */
	public function getContent() {

		$blockModel = $this->owner;

	    // get the Conf. Systems App activated and enabled
	    $availableConfSystems = $this->getConferenceSystemsInfo();

	    // Render warning if no single Conferencing app is availble.
	    $numConfSystems = count($availableConfSystems);
	    if( $numConfSystems <= 0) {
 	    	return Yii::app()->controller->renderPartial('player.views.block.videoconf._no_conf_app_available',array(), true);
	    }

	    $course_id = $blockModel->course_id;
	    $user_id = Yii::app()->user->id;

	    $courseUserModel = LearningCourseuser::model()->findByAttributes(array("idUser" => $user_id, "idCourse" => $course_id));
	    $courseModel = LearningCourse::model()->findByPk($course_id);
	    $userModel = CoreUser::model()->findByPk($user_id);


	    // Some actions are restricted to Course admin only
	    $level = $courseUserModel->level;
		if(Yii::app()->user->getIsPu()) {
			// Is this a power user and the course was assigned to him?
			$pUserCourseAssigned = CoreUserPuCourse::model()->findByAttributes(array(
				'puser_id' => Yii::app()->user->id,
				'course_id' => $course_id
			));
		} else
			$pUserCourseAssigned = null;

	    $allowAdminOperations = Yii::app()->user->getIsGodadmin() ||
			(Yii::app()->user->getIsPu() && $pUserCourseAssigned && Yii::app()->user->checkAccess('/lms/admin/course/mod')) ||
			($level > LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);

	    // get Auth manager instance
	    $authManager = Yii::app()->authManager;

	    // Get Active and History Rooms data providers
	    $dataProviderOnair = WebinarSession::model()->getRoomsListDataProvider($course_id, ConferenceRoom::STATUS_ONAIR, true);
	    $dataProviderScheduled = WebinarSession::model()->getRoomsListDataProvider($course_id, ConferenceRoom::STATUS_SCHEDULED, true);
	    $dataProviderHistory = WebinarSession::model()->getRoomsListDataProvider($course_id, ConferenceRoom::STATUS_HISTORY);

		$courseUser = LearningCourseuser::model()->findByAttributes(array(
			'idUser' => Yii::app()->user->id,
			'idCourse' => $course_id
		));

		return Yii::app()->controller->renderPartial('player.views.block.'.$this->owner->content_type.'.index', array(
			'blockModel' => $blockModel,
			'availableConfSystems' => $availableConfSystems,
			"userModel" => $userModel,
			"courseModel" => $courseModel,
			"courseUserModel" => $courseUserModel,
			"allowAdminOperations" => $allowAdminOperations,
			"authManager" => $authManager,
			"dataProviderOnair" => $dataProviderOnair,
			"dataProviderScheduled" => $dataProviderScheduled,
			"dataProviderHistory" => $dataProviderHistory,
			'hideOnAirTab' => (0 == $dataProviderOnair->getItemCount()),
			'courseUser' => $courseUser
		), true , true);
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getTitle()
	 */
	public function getTitle() {
		return Yii::t('player', 'Video Conferences');
	}


	/**
	 *
	 */
	public function destringifyParams() {
		$params = CJSON::decode($this->owner->params);
	}


	/**
	 * (non-PHPdoc)
	 * @see CActiveRecordBehavior::beforeSave()
	 */
	public function beforeSave($event)  {
		$this->owner->params = CJSON::encode(get_object_vars($this));
		return true;
	}


	/**
	 * Call conference manager and get information about AVAILABLE conferencing systems (i.e. activated && enabled)
	 *
	 * @return array
	 */
	private function getConferenceSystemsInfo() {
		$result = WebinarTool::getAllTools();
		return $result;
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getExtraHeaderHtml()
	 */
	public function getExtraHeaderHtml() {
		$videconfs = WebinarTool::getAllTools();
		if (count($videconfs) <= 0)
			return '';
		return Yii::app()->controller->renderPartial('player.views.block.'.$this->owner->content_type.'._extra_header_html', array(
			'blockModel' => $this->owner,
		), true);
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getExtraMenuActions()
	 */
	public function getExtraMenuActions() {
		return '';
	}


}


