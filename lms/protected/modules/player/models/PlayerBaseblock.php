<?php

/**
 * This is the model class for table "player_courseblock".
 *
 * The followings are the available columns in table 'player_courseblock':
 * @property integer $id
 * @property integer $course_id
 * @property string $content_type
 * @property integer $columns_span
 * @property integer $position
 * @property string $params
 * @property integer $canNormalUserUploadFiles
 * @property integer $fileLimit
 * @property integer $permissionType
 * @property integer $permissionLevels
 *
 *
 * The followings are the available model relations:
 * @property LearningCourse $course
 */
class PlayerBaseblock extends CActiveRecord
{
	// Block types
	const TYPE_COURSE_INFO 		= 'courseinfo';
	const TYPE_COMMENTS			= 'comments';
	const TYPE_COURSEDOCS		= 'coursedocs';
	const TYPE_COURSETEACHER	= 'courseteacher';
	const TYPE_NEWSBLOG			= 'newsblog';
	const TYPE_VIDEOCONF		= 'videoconf';
	const TYPE_FORUM			= 'forum';
	const TYPE_COURSE_DESCRIPTION = 'coursedesc';
	const TYPE_BLOG             = 'blog';
	const TYPE_MY_INSTRUCTOR 	= 'myinstructor';
    const TYPE_MY_COACH         = 'mycoach';
	const TYPE_LP_NAVIGATION	= 'lpnavigation';
	const TYPE_IFRAME           = 'iframe';
	const TYPE_QUESTIONS           = 'questions';

	//classroom specific blocks
	const TYPE_SESSION_INFO = 'sessioninfo';


	const TYPE_DEFAULT			= 'courseinfo';

	public $behaviorsRules = array();
	public $behaviorsLabels = array();

	//CourseDocs Type
	public $canNormalUserUploadFiles;
	public $fileLimit;
	public $permissionType;
	public $permissionLevels;
	
	
	// IFRAME type specific
	public $iframeUrl       = "";
	public $iframeHeight    = 0;
	public $secretSalt      = "";
	public $oauthClient     = "";
	public $repeatSecretSalt = "";
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PlayerBaseblock the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'player_baseblock';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		$rules =  array(
			array('course_id, content_type, columns_span, position', 'required'),
			array('course_id, columns_span, position, fileLimit', 'numerical', 'integerOnly'=>true),
			array('content_type', 'length', 'max'=>64),
			array('fileLimit', 'required', 'on'=>'normalUserCanUploadFiles'),
			array('params, canNormalUserUploadFiles, permissionLevels, permissionType', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, course_id, content_type, columns_span, position, params', 'safe', 'on'=>'search'),
			array('permissionType', 'validateEnrollmentPermission'),
		);

		// Include also collected rules from the behavior (if any)
		$rules = CMap::mergeArray($rules, $this->behaviorsRules);

		return $rules;


	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'course' => array(self::BELONGS_TO, 'LearningCourse', 'course_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		$labels = array(
			'id' => 'ID',
			'course_id' => 'Course Id',
			'content_type' => Yii::t('player', 'Block Content Type'),
			'columns_span' => Yii::t('standard', '_WIDTH'),
			'position' => 'Position',
			'params' => 'Parameters',
		);

		// Include also collected rules from the behavior (if any)
		$labels = CMap::mergeArray($labels, $this->behaviorsLabels);


// 		// Merge with attached behavior(s)' attribute labels, if they provide a method
// 		foreach (self::getTypes() as $type => $name) {
// 			$behavior = $this->asa($type);
// 			if ($behavior != null) {
// 				if (method_exists($behavior, 'myAttributeLabels')) {
// 					$labels = CMap::mergeArray($labels, $behavior->myAttributeLabels());
// 				}
// 			}
// 		}

		return $labels;

	}

	/**
	 * Permission validation
	 * @return boolean
	 */
	public function validateEnrollmentPermission(){
		if($this->permissionType == LearningCourseFile::FILE_PERMISSION_SELECTIBLE){
			if(empty($this->permissionLevels)){
				$this->addError('permissionLevels', Yii::t('player','Select enrollment levels'));
			}
		}
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('course_id',$this->course_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	/**
	 * Returns an array of <blocktype> => <Block Name>
	 *
	 * @return array
	 */
	public static function getTypes($currentId = '') {

		$typesArray = array(
			self::TYPE_COURSE_INFO 		=> Yii::t("menu_course", "_INFCOURSE"),
			self::TYPE_COURSE_DESCRIPTION => Yii::t('certificate', '_COURSE_DESCRIPTION'),
			self::TYPE_COMMENTS 		=> Yii::t("standard", "_COMMENTS"),
			self::TYPE_COURSEDOCS 		=> Yii::t("player", "File repository area"),
			self::TYPE_FORUM 			=> Yii::t("standard", "_FORUM"),
			self::TYPE_MY_INSTRUCTOR 	=> Yii::t("standard", 'Teacher(s)'),//Yii::t('classroom', 'My instructor(s)'),
			self::TYPE_IFRAME           => Yii::t("standard", "IFRAME"),
            
		);
        
        if(Yii::app()->player->course->enable_coaching && PluginManager::isPluginActive('CoachingApp')){
            $typesArray[self::TYPE_MY_COACH] = Yii::t('coaching', 'My Coach');
        }

		if(PluginManager::isPluginActive('CurriculaApp')){
			$typesArray[self::TYPE_LP_NAVIGATION] = Yii::t("curricula_management", 'Learning Plan navigation');
		}

		if (Yii::app()->player->course->course_type !== LearningCourse::TYPE_WEBINAR)
			$typesArray[self::TYPE_VIDEOCONF] = Yii::t("standard", "_VIDEOCONFERENCE");

        if (PluginManager::isPluginActive('MyBlogApp')) {
            $criteria = new CDbCriteria();
            $criteria->condition = 'course_id = :course AND content_type = :type';
			if (isset($currentId) && $currentId != ""){
				$criteria->condition .= ' AND id !=:id';
				$criteria->params[':id'] = $currentId;
			}
            $criteria->params[':course'] = Yii::app()->player->course->idCourse;
            $criteria->params[':type'] = self::TYPE_BLOG;

            // If blog widget is enabled for this course don't allow to the user to select it again
            if(PlayerBaseblock::model()->exists($criteria) === false)
                $typesArray[self::TYPE_BLOG] = Yii::t("myblog", "Blog");
        }

		//some blocks specific for classroom courses
		if (PluginManager::isPluginActive('ClassroomApp') && (Yii::app()->player->course->course_type == LearningCourse::TYPE_CLASSROOM)) {
			$typesArray[self::TYPE_SESSION_INFO] = Yii::t('classroom', 'Session info');
		}

		if (PluginManager::isPluginActive('Share7020App') && (Yii::app()->player->course->course_type !== LearningCourse::TYPE_WEBINAR) && (Yii::app()->player->course->course_type !== LearningCourse::TYPE_CLASSROOM)) {
			$typesArray[self::TYPE_QUESTIONS] = Yii::t('app7020', 'Questions & Answers');
		}


		return $typesArray;
	}


	/**
	 * Attach a behavior according to block content type, if it exists.
	 * Different behaviors will deliver their own content
	 *
	 * @return PlayerBaseblock
	 */
	public function attachContentBehavior() {

		$contentType =  $this->content_type;

		// No content type in model? Get out!
		if (empty($contentType)) {
			return false;
		}

		$behaviorClassName = 'PlayerBlock' . ucfirst($contentType);
		//$behaviorFileName = Yii::getPathOfAlias('player') . '/models/' . $behaviorClassName . '.php';

		if (!class_exists($behaviorClassName)) {
			return false;
		}

		$behavior = new $behaviorClassName();
		$this->attachBehavior($contentType, new $behaviorClassName($this->params));

		// Collect behaviors rules in class property
		if ($behavior != null) {
			if (method_exists($behavior, 'myRules')) {
				$this->behaviorsRules = $this->myRules();
			}
			if (method_exists($behavior, 'myAttributeLabels')) {
				$this->behaviorsLabels = $this->myAttributeLabels();
			}
			if (method_exists($behavior, 'destringifyParams')) {
				$this->destringifyParams();
			}

		}


		return $this;

	}

	/**
	 * Check the block type and do some modifications related to it.
	 * @return bool
	 */
	public function beforeValidate()
	{
		switch($this->content_type)
		{
			case self::TYPE_COMMENTS: //create init forum in case of empty forum_id
				if (!$this->forum_id)
				{
					$forum = LearningForum::createCommentsForum($this->course_id);
					if ($forum)
						$this->forum_id = $forum->primaryKey;
				}
				break;

			case self::TYPE_SESSION_INFO:
			case self::TYPE_COURSE_INFO:  // FORCE course info widgets to FILL size (span12)
				$this->columns_span = 12;
				break;	
				
		}
		return parent::beforeValidate();
	}

	public function getCanUserUploadFiles($canEdinOrDelete = false)
	{
		$params = json_decode($this->params, true);
		//This is only to decide if user can edin or delete his own files
		if($canEdinOrDelete)
			return $params['canNormalUserUploadFiles'] == '1';

		if($params['canNormalUserUploadFiles'] != '1')
			return false;

		$userUploadedFiles = Yii::app()->db->createCommand("SELECT COUNT(1) FROM learning_course_file WHERE id_course = :course AND idUser = :user")->queryScalar(array(
			':course' => $this->course_id,
			':user' => Yii::app()->user->id
		));


		if( (int)$params['fileLimit'] != 0 && ((int)$userUploadedFiles >= (int)$params['fileLimit']) )
			return false;

		return true;
	}

	/**
	 * Checks if specified block widget is active for the course
	 *
	 * @param $idCourse ID of LMS Course
	 * @param $block Block content_type
	 * @return bool Return true if one or more widget of this type is active for the course
	 */
	public static function isBlockEnabled($idCourse, $block){
		$bockModel = self::model()->findByAttributes(array(
			'course_id' => $idCourse,
			'content_type' => $block
		));

		if($bockModel instanceof PlayerBaseblock){
			return true;
		}

		return false;
	}
}
