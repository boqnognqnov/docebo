<?php
/**
 * Behavior attached to PlayerBaseblock model.
 * Provides methods to deliver its own content.
 *
 * Name pattern for block content behaviors:  PlayerBlock<Contenttype>
 * where <Contenttype> is one of UC-first-ed TYPE_* from PlayerBaseBlock model
 * (TYPE_COURSE_INFO, TYPE_COMMENTS, ... etc)
 *
 * @author Plamen Petkov
 *
 */
class PlayerBlockCoursedocs extends CActiveRecordBehavior  implements IPlayerBlockContent {

	/**
	 * Construct this behavior.
	 *
	 * @param string $params  (JSON string, kept in main block table)
	 */
	public function __construct($params = false) {

	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::myAttributeLabels()
	 */
	public function myAttributeLabels() {
		return array(
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::myRules()
	 */
	public function myRules() {
		return array(
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getContent()
	 */
	public function getContent() {

		$blockModel = $this->owner;
		$page = Yii::app()->request->getParam('page', 1);
		$parentId = Yii::app()->request->getParam('parent_id', null);
		$course_id = $blockModel->course_id;
		$user_id = Yii::app()->user->id;

		$courseUserModel = LearningCourseuser::model()->findByAttributes(array("idUser" => $user_id, "idCourse" => $course_id));
		$parent = LearningCourseFile::model()->findByPk($parentId);
		if($parent === null)
			$parent = LearningCourseFile::model()->findByAttributes(array(
				'path' => 'root',
				'id_course' => $course_id
			));

		// Some actions are restricted to Course admin only
		// If one of the following conditions is true the
		// user has access to administer the course:
		// 1. Is Goadmin
		// 2. Is Instructor
		// 3. Is Power User, has this course assigned to him and has "course edit" permissions
		$level = $courseUserModel->level;
		$allowAdminOperations = false; //initialize with default value
		if ((Yii::app()->user->isGodAdmin)) {
			$allowAdminOperations = true;
		} elseif (Yii::app()->user->getIsPu()) {
			// Is this a power user and the course was assigned to him?
			$pUserCourseAssigned = CoreUserPuCourse::model()->findByAttributes(array(
				'puser_id' => Yii::app()->user->id,
				'course_id' => $course_id
			));
			$allowAdminOperations = (!empty($pUserCourseAssigned) && Yii::app()->user->checkAccess('/lms/admin/course/mod'));
		} else {
			$allowAdminOperations = ($level > LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT && $level != LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR);
		}

		$dataProvider = LearningCourseFile::model()->getCoursedocsListDataProvider($course_id, Yii::app()->user->id, $parent);

		return Yii::app()->controller->renderPartial('player.views.block.'.$this->owner->content_type.'.index', array(
			'blockModel' => $this->owner,
			'dataProvider' => $dataProvider,
			"allowAdminOperations" => $allowAdminOperations,
			'parent' => $parent,
			'page' => $page
		), true);
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getTitle()
	 */
	public function getTitle() {
		return Yii::t("player", 'File repository area');
	}


	/**
	 *
	 */
	public function destringifyParams() {
		$params = CJSON::decode($this->owner->params);
	}


	/**
	 * (non-PHPdoc)
	 * @see CActiveRecordBehavior::beforeSave()
	 */
	public function beforeSave($event)  {
		if($this->owner->scenario !== 'WidgetParamsSaved'){
			$this->owner->params = CJSON::encode(get_object_vars($this));
		}
		return true;
	}

	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getExtraHeaderHtml()
	 */
	public function getExtraHeaderHtml($userLevel = false, $userCanAdminCourse = false) {
		return Yii::app()->controller->renderPartial('player.views.block.' . $this->owner->content_type . '._extra_header_html', array(
			'blockModel' => $this->owner,
			'userLevel' => $userLevel,
			'userCanAdminCourse' => $userCanAdminCourse
		), true);
	}


	/**
	 * (non-PHPdoc)
	 * @see IPlayerBlockContent::getExtraMenuActions()
	 */
	public function getExtraMenuActions() {
		
		$settingsUrl = Yii::app()->controller->createUrl('/player/block/axCourseDocsSettings', array('block_id'=>$this->owner->id, 'course_id'=>$this->owner->course_id));
		$result = array('<a href="'.$settingsUrl.'" class="open-dialog p-hover"><i class="i-sprite is-gearpair"></i> '.Yii::t('standard', 'Settings').'</a>');
		return $result;
	}

}


