    <?php
/**
 * Training Resources
 */
class TrainingController extends PlayerBaseController
{

	// Do user needs to select a coaching session first before starting the course or ask for coaching session
	public $needsCoachingSession = false;

	/**
	 * userNeedsCoachingSession - check if the user needs extra processing because of the enabled coaching
	 * set a coaching session status
	 * @return bool
	 */
	protected function userNeedsCoachingSession()
	{
		// Check if user is not admin or PU before proceed to next steps - avoid
		if ((!(Yii::app()->user->getIsGodadmin() || Yii::app()->user->getIsPu())) && (!Yii::app()->user->getIsGuest())) {

			// If there is no Course id set try to determine and set it
			$idCourse = Yii::app()->request->getParam('course_id', false);
			$idCourse = !empty($idCourse) ? $idCourse : Yii::app()->request->getParam('idCourse', false);

			// Ensure the courseModel exists first
			if ($this->courseModel == null && $idCourse) {
				// Set the course id if not already set
				$this->course_id = $idCourse;

				// Create Course model
				$this->courseModel = LearningCourse::model()->findByPk($this->course_id);
			}

			// Get Learning_Courseuser model
			$learningCourseUser = LearningCourseuser::model()->findByAttributes(array('idCourse' => $this->course_id, 'idUser' => Yii::app()->user->id));

			// Is there a corresponding course user model
			if (!empty($learningCourseUser)) {
				// We need to proceed with coaching session check only if the user level is student !!!
				if ($learningCourseUser->level != LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT) {
					// Avoid extra coaching processing for users, that are not students
					$this->coachingSessionStatus = LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_PROCEED;
					return false;
				}
				//$learningCourseUser = current($learningCourseUser);
			}

			// First check is coaching enabled for this course
			if ($this->courseModel->enable_coaching) {
				// If it is submitted already choosen by user session
				if (!empty($_REQUEST['confirm_button']) && !empty($_REQUEST['sessionChoose']) && $_REQUEST['coaching_status'] <> LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_NEW) {
					// Create a new user coaching session
					$newUserCoachingSession = new LearningCourseCoachingSessionUser();
					$newUserCoachingSession->idSession = (int) $_REQUEST['sessionChoose'];
					$newUserCoachingSession->idUser = (int) Yii::app()->user->id;
					$newUserCoachingSession->save();

					// A new coaching session for this user was added and we need to display coresponding message
					$this->coachingSessionStatus = LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_NEW;
					return false;
				}


				// Check is there any active coaching session for this user
				$userActiveCoachingSessions = LearningCourseCoachingSessionUser::model()->getUserActiveCoachingSessions(Yii::app()->user->id, $this->course_id);

				// If there is already assigned coaching session we don't need to proceed with coaching stuff,
				// so proceed to the course player
				if (count($userActiveCoachingSessions) > 0) {
					// There is an active user coaching session and we can proceed running the course
					$this->coachingSessionStatus = LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_PROCEED;
					return false;
				} else {
					// Try to get any user course coaching sessions, that are expired or not started
					$userAssignedSessions = LearningCourseCoachingSession::model()->userAssignedSessions(Yii::app()->user->id, true, $this->course_id);


					//IF COACH SESSION IS NOT ISSIGNED BUT USER HAS PERMISSION TO ACCESS THE COURSE
					if ($this->courseModel->coaching_option_access_without_assign_coach == 1) {
						$this->coachingSessionStatus = LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_ASK_BUT_ACCESSED;
						return false;
					}


					//Check if there are user coaching sessions
					if (count($userAssignedSessions) > 0) {

						$sessionExpired = false; // indicator for expired sessions
						$sessionInFuture = false; // indicator for not started yet sessions

						// Get the user local date now(today):
						$date_today = Yii::app()->localtime->toLocalDateTime(Yii::app()->localtime->getLocalNow());

						// Process every user coaching sessions in other to get correct coaching session status
						foreach ($userAssignedSessions as $userAssignedSession){
							// Create temp start and end date objects to correct dates comparing
							$temp_start_date = Yii::app()->localtime->toLocalDateTime($userAssignedSession->datetime_start);

							$temp_end_date = Yii::app()->localtime->toLocalDateTime($userAssignedSession->datetime_end);

							// Is the session expired
							if (Yii::app()->localtime->isLt($temp_end_date, $date_today)) {
								// Session is Expired
								$sessionExpired = true;
							}

							// Is the session not started yet
							if (Yii::app()->localtime->isGt($temp_start_date, $date_today)) {
								// Coaching session is not started yet !!!
								// We need to check if there is a course setting to allow user to run course if the coaching session is not started
								if ($this->courseModel->coaching_option_allow_start_course != 1) {
									$sessionInFuture = true; // The coaching session is not started and it is not allowed user to start the course !!!
								}
								else {
									// It is allowed user to start the course, although the coaching session is not started yet
									$this->coachingSessionStatus = LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_PROCEED;
									return false;
								}
							}
						} // End of user coaching sessions loop

						// Are there expired user coaching sessions
						if ($sessionExpired && !$sessionInFuture) {
							// There is a coaching session, that expired and there are coaching session scheduled for future per this user
							// NOTE: User will be able to start the the course, although his/her coaching session is expired
							$this->coachingSessionStatus = LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_PROCEED;
							return false;
						}

						// Are there any future coaching sessions
						if ($sessionInFuture) {
							// There is a coaching session, that is scheduled for future per this user
							$this->coachingSessionStatus = LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_HAVE_SESSION_NOT_STARTED;
							// NOTE: User won't be able to run the course yet, as it is not set in course settings 'coaching_option_allow_start_course'
							// also we neglect any other expired coaching sessions as there is a one scheduled to start soon!!!

							return false;
						}
					} // End of check for (all) user coaching sessions


					// If we are here the coaching is enabled for this course and user doesn't have any past/future coaching sessions assigned so far

					// Different cases depending the course learner assignment setting (Course Advanced Settings) !!!
					switch ($this->courseModel->learner_assignment) {
						case LearningCourse::COACHING_LEARNER_ASSIGNMENT_AUTOMATICALLY_MAXIMIZED:
						case LearningCourse::COACHING_LEARNER_ASSIGNMENT_AUTOMATICALLY_BALANCED:
							// First determine which auto coaching session assign is set to be used
							$AutoAssignMethod = ($this->courseModel->learner_assignment == LearningCourse::COACHING_LEARNER_ASSIGNMENT_AUTOMATICALLY_MAXIMIZED) ?
								LearningCourseCoachingSession::AUTO_ASSIGN_MAXIMIZED : LearningCourseCoachingSession::AUTO_ASSIGN_BALANCED;

							// Try to assign user to a coaching session
							LearningCourseCoachingSession::autoAssign($this->course_id, Yii::app()->user->id, $AutoAssignMethod);

							// Check if user was really assigned to a coaching session, before proceeding to the course
							$newUserCoachingSession = LearningCourseCoachingSession::userAssignedSessions(Yii::app()->user->id, true, $this->course_id);

							// User is a assigned to a new coaching session
							if (count($newUserCoachingSession) > 0) {
								// Check is the session started
								$currentUserCoachingSession = current($newUserCoachingSession);

								// Create start date object to do correct dates comparing
								$currentUserCoachingSessionStartDate = Yii::app()->localtime->toLocalDate($currentUserCoachingSession->datetime_start);
								$currentUserCoachingSessionStartDate = DateTime::createFromFormat(Yii::app()->localtime->getPHPLocalDateFormat('short'), $currentUserCoachingSessionStartDate);

								// Get the user local date now(today):
								$date_today = Yii::app()->localtime->toLocalDate();
								$date_today = DateTime::createFromFormat(Yii::app()->localtime->getPHPLocalDateFormat('short'), $date_today);

								if (($currentUserCoachingSessionStartDate > $date_today) && ($this->courseModel->coaching_option_allow_start_course != 1)) {
									// The assigned course coaching session is not started yet, so display the corresponding message
									// Note, that also it is not allowed user to start a course if his/her coaching session is scheduled in future !!!
									$this->coachingSessionStatus = LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_HAVE_SESSION_NOT_STARTED;
								} else {
									// The assigned course coaching session is started, so proceed
									$this->coachingSessionStatus = LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_PROCEED;
								}


							} else {
								// For some reason user was not assigned to a coaching session - maybe due a lack of coaching sessions with free seats
								// User needs to ask admin for coaching session !!!
								$this->coachingSessionStatus = LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_ASK;
							}

							return false;
							break;

						case LearningCourse::COACHING_LEARNER_ASSIGNMENT_ADMIN_SELECTED:
							// In this case user needs to ask admin to assign a new coaching session to him/her as only admin can do it !!!
							$this->coachingSessionStatus = LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_ASK;
							return false;
							break;

						case LearningCourse::COACHING_LEARNER_ASSIGNMENT_LEARNER_SELECTED:
							// Check is there any coaching sessions with free seats to offer the user
							$availableCoachingSessions = LearningCourse::getAvailableCoachingSessions($this->course_id);

							// If there are available coaching sessions to from
							if (count($availableCoachingSessions) > 0) {
								$this->needsCoachingSession = true;
								$this->coachingSessionStatus = LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_SELECT;
								return true;
							}

							// User needs to ask admin for coaching session !!!
							$this->coachingSessionStatus = LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_ASK;
							return false;

							break;

						default:
							// User needs to ask admin for coaching session !!!
							$this->coachingSessionStatus = LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_ASK;
							return false;
							break;
					} // End of switch for determining how the user should be assigned to a coaching session
				} // End of the case, when there are no active coaching sessions assign to the user
			} // End of the case when course coaching is enabled
		} // End of the case when a regular user starts the course

		// By default proceed and start the course
		$this->coachingSessionStatus = LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_PROCEED;
		return false;
	}


	/**
	 * overwrite resolveLayout method to use module based layout
	 */
	public function resolveLayout() {
		if ($this->coachingSessionStatus != null) {
			// TODO: Fixme temporary disable this layout till it is modified to suit the coaching mockups
			//$this->layout = '/layouts/coaching';
			parent::resolveLayout();
		} else {
			parent::resolveLayout();
		}
	}


	public function init() {

		// create root element if not exists for LearningCourseFile
		$courseId = Yii::app()->request->getParam('course_id', false);

		if ($courseId !== false) {
			$isRootElementExists = Yii::app()->db->createCommand()->select('id_file')
				->from(LearningCourseFile::model()
				->tableName())
				->where('id_course=:id', array(':id' => $courseId))
				->queryScalar();

			if (!$isRootElementExists) {
				Yii::app()->db->createCommand()->insert(LearningCourseFile::model()->tableName(), array(
					'id_course' => $courseId,
					'path' => 'root',
					'title' => 'root_' . $courseId,
					'position' => 0,
					'original_filename' => 'root',
					'idParent' => 0,
					'item_type' => 10,
					'iLeft' => 1,
					'iRight' => 2,
					'lev' => 1,
				));
			}
		}

		// Check coaching stuff
		$this->needsCoachingSession = $this->userNeedsCoachingSession();
		$r = Yii::app()->request->getParam('r');
		if ($this->needsCoachingSession || ($r == 'player/training/userRequestedNewCoachingSession')) {
			$this->registerResources = false;
		}

		parent::init();
	}


	/**
	 * (non-PHPdoc)
	 * @see PlayerBaseController::beforeAction()
	 */
	public function beforeAction($action) {
		$this->breadcrumbs = array();

        PlayerBaseController::breadcrumbUserSubscription($this);

		if (isset($_GET['ref_src']) && $_GET['ref_src'] == 'admin') {
			$this->breadcrumbs[Yii::t('menu', 'Admin')] = Docebo::createAppUrl('admin:dashboard/index');
			$this->breadcrumbs[Yii::t('standard', '_COURSES')] = Docebo::createAppUrl('admin:courseManagement/index');
		} else {
			//redirect to learning plan page on course complete
			$redirect = Settings::get('curricula_redirect_to_learning_plan', 'off', true);
			if ($redirect === 'on' && $this->pathId && !Yii::app()->request->isAjaxRequest) {
				$sessionCourseStatus = Yii::app()->session['courseStatus_' . $this->course_id];
				Yii::app()->session['courseStatus_' . $this->course_id] = $this->courseStatus;

				if (isset($sessionCourseStatus) && $this->courseStatus == LearningCourseuser::$COURSE_USER_END && $sessionCourseStatus != $this->courseStatus) {
					$redirectUrl = Docebo::createLmsUrl('curricula/show', array('id_path' => $this->pathId));
					$this->redirect($redirectUrl, true);
				}
			}
			// Generate breadcrumbs if Learning Plan Navigator widget is active for this course
			if($this->pathId){
				$pathModel = Yii::app()->player->coursePath;
				/**
				 * @var $pathModel LearningCoursepath
				 */

				if($pathModel){
					$this->breadcrumbs[Yii::t('standard', '_COURSEPATH')] = Docebo::createLmsUrl('curricula/show');
					$this->breadcrumbs[$pathModel->path_name] = Docebo::createLmsUrl('curricula/show', array(
						'id_path' => $pathModel->id_path
					));
				}
			} else{
				$this->breadcrumbs[Yii::t('course', '_WELCOME')] = Docebo::createLmsUrl('site/index');
				$this->breadcrumbs[Yii::t('menu_over', '_MYCOURSES')] = Docebo::createLmsUrl('site/index', array('opt' => 'fullmycourses'));
			}
		}

		return parent::beforeAction($action);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
				'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {

		return array(

			// Admins and Teachers can do all
			array('allow',
				'users'=>array('@'),
				'expression' => 'Yii::app()->controller->userCanAdminCourse',
			),

			array('allow',
				'users'=>array('@'),
				'actions'=>array('axGetLoCountersHtml'),
				'expression' => "(Yii::app()->user->getIsPu() && Yii::app()->user->checkAccess('/lms/admin/course/view'))",
			),

			array('allow',
				'users'=>array('@'),
				'actions'=>array('webinarSession'),
				'expression' => "(Yii::app()->user->getIsPu() && (Yii::app()->user->checkAccess('/lms/admin/webinarsessions/add') || Yii::app()->user->checkAccess('/lms/admin/webinarsessions/mod') || Yii::app()->user->checkAccess('/lms/admin/webinarsessions/view') || Yii::app()->user->checkAccess('/lms/admin/webinarsessions/assign')))",
			),

			// For Power Users who have the course assigned
			// AND have permissions to VIEW Classroom Sessions
			array('allow',
				'users'=>array('@'),
				'actions'=>array('session'),
				'expression' => "(Yii::app()->user->getIsPu() && Yii::app()->user->checkAccess('/lms/admin/classroomsessions/view'))",
			),

			// Allow some non harmful actions for Power Users to avoid errors
			array('allow',
				'users'=>array('@'),
				'actions'=>array('axGetLoCountersHtml'),
				'expression' => "(Yii::app()->user->getIsPu() && Yii::app()->user->checkAccess('enrollment/update'))",
			),

			// deny dangerous operations for those subscribers
			array('deny',
				'users'=>array('@'),
				'actions'=>array(
					'axDeleteLo',
					'deleteImage',
					'axCreateFolder',
					'axReorderLo',
					'moveLoToFolder',
					'axDeleteSubtitle',
				),
				'expression' => 'LearningCourseuser::isSubscribed($user->id, ' . $this->course_id . ')',
			),

			array('allow',
				'users'=>array('@'),
				//'actions'=>array('index', 'downloadCertificate'),  //@TODO Fix this list when we are done
				'expression' => 'LearningCourseuser::isSubscribed($user->id, ' . $this->course_id . ')',
			),

			array('allow',
				'users'=>array('@'),
				'actions'=>array('index', 'AxGetCourseLearningObjectsTree', 'axGetQuickNavHtml', 'axGetCommonLaunchpad', 'axReloadSocialRatingPanel', 'AxGetCourseStatus', 'axTrackCourseAccess', 'AxGetPlayerNavigator'),
				'expression' => "(Yii::app()->user->getIsPu() && CoreUserPuCourse::isCourseAssignedToPU(Yii::app()->user->id, '" . $this->course_id . "') && Yii::app()->user->checkAccess('/lms/admin/course/view'))",
			),

			// Deny by default
			array('deny', // if some permission is wrong -> the script goes here
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
				'message' => Yii::t('course', '_NOENTER'),
			),

		);
	}


	/**
	 * Common function to call when we are trying to enter a course of incorrect type (elearning, classroom)
	 * Prepare an error message and redirect
	 */
	protected function invalidCourseType() {
		Yii::app()->user->setFlash('error', Yii::t('course', 'Invalid course type'));
		$redirectUrl = $this->createUrl('site/index', array('opt' => 'mycourses'));
		$this->redirect($redirectUrl, true);
	}

    /**
     * If the Classroom app is enabled, allow the user to enroll to a course session.
     * Redirects to the session enroll/select screen if multiple sessions are found in each case
     */
    protected function _showSessionLearnerView() {
		$params = $this->helperBuildParamData();
        // multiple course sessions found? redirect to the select session screen
        /* @var $courseSessions CActiveDataProvider */
        $courseSessions = LtCourseuserSession::model()->sessionSelectDataProvider(Yii::app()->user->id, $this->course_id);
        if ($courseSessions->getTotalItemCount() == 1) {
            $sessions = $courseSessions->getData();
            $session = $sessions[0];
			$params['session_id'] =  $session->id_session;
            $this->redirect(Docebo::createAppUrl('lms:player/training/session', $params));
        }
        else if($courseSessions->getTotalItemCount() > 1)
            $this->redirect(Docebo::createAppUrl('lms:ClassroomApp/Session/select', $params));

        // no user sessions. y u not enroll me?
        $availableSessions = LtCourseSession::model()->findAllByAttributes(array('course_id' => $this->course_id));
        if (count($availableSessions) > 0)
            $this->redirect(Docebo::createAppUrl('lms:ClassroomApp/Session/enroll', $params));

        // no sessions available -> handled in ClassroomApp/Session/select
        $this->redirect(Docebo::createAppUrl('lms:ClassroomApp/Session/select', $params));
    }

    /**
     * Show the session and learning page to instructors (and superadmins)
     */
    protected function _showSessionInstructorView() {
		$params = $this->helperBuildParamData();
        $this->redirect(Docebo::createAppUrl('lms:player/training/session', $params));
    }


	/**
	 * If the Classroom app is enabled, allow the user to enroll to a course session.
	 * Redirects to the session enroll/select screen if multiple sessions are found in each case
	 */
	protected function _showWebinarSessionLearnerView() {
		$params = $this->helperBuildParamData();
		// multiple course sessions found? redirect to the select session screen
		/* @var $courseSessions CActiveDataProvider */
		$courseSessions = WebinarSessionUser::model()->sessionSelectDataProvider(Yii::app()->user->id, $this->course_id);
		if ($courseSessions->getTotalItemCount() == 1) {
			$sessions = $courseSessions->getData();
			$session = $sessions[0];
			$params['session_id'] = $session->id_session;
			$this->redirect(Docebo::createAppUrl('lms:player/training/webinarSession', $params));
		}
		else if($courseSessions->getTotalItemCount() > 1) {
			$this->redirect(Docebo::createAppUrl('lms:webinar/session/select', $params));
		}

		// no user sessions. y u not enroll me?
		$availableSessions = WebinarSession::model()->findAllByAttributes(array('course_id' => $this->course_id));
		if (count($availableSessions) > 0)
			$this->redirect(Docebo::createAppUrl('lms:webinar/session/enroll', $params));

		// no sessions available -> handled in ClassroomApp/Session/select
		$this->redirect(Docebo::createAppUrl('lms:webinar/session/select', $params));
	}

	/**
	 * Show the session and learning page to instructors (and superadmins)
	 */
	protected function _showWebinarSessionInstructorView() {
		$params = $this->helperBuildParamData();
		$this->redirect(Docebo::createAppUrl('lms:player/training/webinarSession', $params));
	}

	public function actionGetSlidersContent(){

		$loId = Yii::app()->getRequest()->getParam('loId');
		$idCourse = Yii::app()->getRequest()->getParam('course_id');

		$loModel = LearningOrganization::model()->findByPk($loId);
		$courseModel = LearningCourse::model()->findByPk($idCourse);

		if(!$courseModel){
			throw new CException('Invalid course ID');
		}

		// default crop size
		$cropSize = array(
			'width' => 300,
			'height' => 300,
		);
		Yii::app()->event->raise('OverrideCourseImageCropSize', new DEvent($this, array(
			'cropSize' => &$cropSize
		)));



		list($count, $defaultImages, $userImages, $selected) = LearningOrganization::prepareThumbnails($loId, 8);

		$uniqueId = substr(md5(uniqid(rand(), true)), 0, 10);

		$html = $this->renderPartial('_sliders', array(
			'model' => $loModel,
			'defaultImages' => $defaultImages,
			'userImages' => $userImages,
			'count' => $count,
			//'selectedTab' => $selected,
			'selectedTab' => 'user',
			'uniqueId' => $uniqueId,
			'cropSize' => $cropSize,
			'idCourse'=> $courseModel->idCourse,
		), true);

		echo CJSON::encode(array(
			'content'=>$html
		));
	}

	/**
	 * Ajax action that handles custom image uploaded deletion icon click
	 */
	public function actionDeleteImage(){
		$imageId = Yii::app()->getRequest()->getParam('image_id');

		$success = false;

		if ((int) $imageId > 0) {
			$asset = CoreAsset::model()->findByPk($imageId);
			if ($asset) {
				$success = $asset->delete();
			}

			$ajaxResult = new AjaxResult();
			$data = array();
			$ajaxResult->setStatus($success)->setData($data)->toJSON();
			Yii::app()->end();
		}else{
			throw new CException('Invalid image ID');
		}
	}


	/**
	 * Entry point
	 */
	public function actionIndex() {


		// Check the coaching session status and if need do extra coaching session processing
		if ($this->coachingSessionStatus <> LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_PROCEED
			&& $this->coachingSessionStatus <> LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_ASK_BUT_ACCESSED ) {
			$this->processCoachingStatuses();
		}

		// Let custom plugins add their own custom course types
		$customCourseDescriptors = array();
		Yii::app()->event->raise('CollectCustomCourseTypes', new DEvent($this, array('types' => &$customCourseDescriptors)));
		if(isset($customCourseDescriptors[Yii::app()->player->course->course_type]['player_course_url']))
			$this->redirect($customCourseDescriptors[Yii::app()->player->course->course_type]['player_course_url'].'&course_id='.$this->course_id);

        /* @var $cs CClientScript */
        $cs = Yii::app()->getClientScript();

		$adminJs = Yii::app()->assetManager->publish(Yii::getPathOfAlias('admin.js'));
		$playerAssets = $this->getPlayerAssetsUrl();

		// We need some helper methods located in these files in the
		// Player List View layout LO add/edit UI: Additional info tab, custom thumbnail uploading logic
		$cs->registerScriptFile($adminJs.'/scriptTur.js');
		$cs->registerScriptFile($adminJs.'/scriptModal.js');
		$cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/Jcrop/jquery.Jcrop.min.js');
		$cs->registerCssFile(Yii::app()->theme->baseUrl.'/js/Jcrop/jquery.Jcrop.min.css');
		//$cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/admin.css');
		$cs->registerScriptFile($adminJs . '/script.js');
		Yii::app()->getModule('centralrepo')->registerResources(true);
		$cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/jwizard.js');

		if (Yii::getPathOfAlias('jplayer') === false)
			Yii::setPathOfAlias('jplayer', Yii::app()->basePath.'/../../common/extensions/jplayer');

		$assetsPath = Yii::getPathOfAlias('jplayer.assets');
		$assetsUrl = Yii::app()->assetManager->publish($assetsPath);
		$cs->registerCssFile($assetsUrl . "/skin/course_videoplayer/jplayer.css");

        $themeUrl = Yii::app()->theme->baseUrl;

        // FCBK
        $cs->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
        $cs->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');

		if(LearningCourse::getPlayerLayout($this->course_id)==LearningCourse::$PLAYER_LAYOUT_NAVIGATOR || LearningCourse::getPlayerLayout($this->course_id)==LearningCourse::$PLAYER_LAYOUT_LISTVIEW){
			$cs->registerScriptFile($playerAssets.'/js/player_navigator.js');
			$cs->registerCssFile($playerAssets.'/css/player_navigator.css');
		}
		$userLevel = $this->userIsSubscribed ? LearningCourseuser::userLevel(Yii::app()->user->id, $this->course_id) : false;

		if (PluginManager::isPluginActive('ClassroomApp') && (Yii::app()->player->course->course_type == LearningCourse::TYPE_CLASSROOM)) {
			// Users with level less than instructor will see the "learner view"
			if (!$this->userCanAdminCourse)
				$this->_showSessionLearnerView();
			else
				$this->_showSessionInstructorView();
		}

		if (Yii::app()->player->course->course_type == LearningCourse::TYPE_WEBINAR) {
			// Users with level less than instructor will see the "learner view"
			if (!$this->userCanAdminCourse)
				$this->_showWebinarSessionLearnerView();
			else
				$this->_showWebinarSessionInstructorView();
		}

        if (PluginManager::isPluginActive('MyBlogApp')) {
            // Register myblog.css
            $cs->registerCssFile(Yii::app()->baseUrl.'/../plugins/MyBlogApp/assets/css/myblog.css');
        }

		// show overlay hints for admin
		$showHints = array();
		if (Yii::app()->user->getIsGodadmin() || Yii::app()->user->getIsAdmin()) {
			// if no LOs exist for this course, show a hint
			$countLOs = LearningObjectsManager::getCourseLearningObjectsCount($this->course_id);
			if ( 0 == $countLOs ) {
				$showHints[] = 'bootstroAddTrainingResources';
				$showHints[] = 'bootstroAddBlockLauncher';
			}
			// if no course blocks exist, show a hint
			/*$countBlocks = PlayerBaseblock::model()->countByAttributes(array('course_id'=>$this->course_id));
			if (! $countBlocks) {
			}*/
			// when the first LO is uploaded, show this hint
			$showHints[] = 'bootstroManageCourse';
		}
		else if (Yii::app()->user->getIsUser()) {
			// show hints the first time a user enteres a course
			$isFirstAccess = LearningCourseuser::isFirstAccess(Yii::app()->user->id, $this->course_id);

			if($isFirstAccess){
				Yii::app()->event->raise('UserFirstLoginToCourse', new DEvent($this, array(
					'course' => $this->courseModel // LearningCourse object
				)));

				$showHints[] = 'bootstroCourseStructure';
				$showHints[] = 'bootstroLaunchpad';
			}

		}

		if ($this->userIsSubscribed) {
			LearningCourseuser::updateCourseUserAccess(Yii::app()->user->id, $this->course_id);
		}
        $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/clipboard.min.js');
		$disableAddLOButton = false;
		// extra html/js added by plugins
		$html = '';
		$params = array(
			'course_id' => $this->course_id,      // set in BaseController
			'courseModel' => $this->courseModel,  // set in BaseController
			'showHints' => implode(',', $showHints),
			'userLevel' => ($userLevel),
			'disableAddLOButton' => &$disableAddLOButton,
			'html' => &$html,
			'lastPopupPlayed' => Yii::app()->request->getParam('last_popup_played', false),
			'deepLink'          =>  ($this->courseModel->deep_link)? $this->courseModel->getDeepLink() : '',
		);

		Yii::app()->event->raise('BeforeCoursePlayerViewRender', new DEvent($this, $params));

		$this->render('index', $params);
	}


	/**
	 * Main sessions view
	 */
	public function actionSession() {
        $cs = Yii::app()->getClientScript();
        $themeUrl = Yii::app()->theme->baseUrl;

        $cs->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
        $cs->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');
        $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/clipboard.min.js');

		if (!PluginManager::isPluginActive('ClassroomApp') || (Yii::app()->player->course->course_type !== LearningCourse::TYPE_CLASSROOM))
			throw new CHttpException("Action only available for classroom courses");

		$userLevel = $this->userIsSubscribed ? LearningCourseuser::userLevel(Yii::app()->user->id, $this->course_id) : false;
		$courseSession = null;
		$sessionId = null;

		if ($this->userIsSubscribed)
			LearningCourseuser::updateCourseUserAccess(Yii::app()->user->id, $this->course_id);

		$puWithSessionPermission = Yii::app()->user->getIsPu() &&
			(Yii::app()->user->checkAccess('/lms/admin/classroomsessions/view') ||
			Yii::app()->user->checkAccess('/lms/admin/classroomsessions/add') ||
			Yii::app()->user->checkAccess('/lms/admin/classroomsessions/mod')) &&
			CoreUserPuCourse::isCourseAssignedToPU(Yii::app()->user->id, $this->course_id)
		;

		$sessionId = Yii::app()->request->getParam('session_id');
		if ($sessionId) { // Digging into a specific session
			$courseSession = LtCourseSession::model()->findByAttributes(array('id_session' => $sessionId));
			if (!$courseSession)
				throw new CHttpException("Invalid session id");

			// save the session name in the current session, to be used in the CourseHeader view
			Yii::app()->session['course_header_session_'.time()] = $courseSession->name;

			// If multiple sessions sessions are available for the course, go to the 'select session' page when
			// clicking on the course name in the breadcrumbs
			$enrolledSessions = LtCourseuserSession::model()->sessionSelectDataProvider(Yii::app()->user->id, $this->course_id);
			if ($enrolledSessions->getTotalItemCount() > 1) {
				$this->breadcrumbs[Docebo::ellipsis($this->courseModel->name, 60, '...')] = Docebo::createAppUrl('lms:ClassroomApp/session/select', array('course_id' => $this->course_id));
				$this->breadcrumbs[] = Docebo::ellipsis($courseSession->name, 60, '...');
			} else {
				$this->breadcrumbs[] = Docebo::ellipsis($this->courseModel->name, 60, '...');
				$this->breadcrumbs[] = Docebo::ellipsis($courseSession->name, 60, '...');
			}
		} else {
			if (!$this->userCanAdminCourse && !$puWithSessionPermission)
				$this->_showSessionLearnerView();
			else {
				$this->breadcrumbs[] = Docebo::ellipsis($this->courseModel->name, 60, '...');
				$this->breadcrumbs[] = Yii::t('classroom', 'Sessions and Enrollments');
			}
		}

		//Additional check for the PU to understand in which session is subscribed as an instructor
		if(Yii::app()->user->getIsPu())
		{
			$tmp = array();
			$tmp2 = array();
			if(!is_null(Yii::app()->session['PUIsInstructorForCourse']))
				$tmp = Yii::app()->session['PUIsInstructorForCourse'];
			if(!is_null(Yii::app()->session['PUIsCourseAssociated']))
				$tmp2 = Yii::app()->session['PUIsCourseAssociated'];

			$subscribed_sessions =	Yii::app()->db->createCommand()->select('cus.id_session')
									->from(LtCourseSession::model()->tableName().' cs')
									->join(LtCourseuserSession::model()->tableName().' cus', 'cs.id_session = cus.id_session')
									->where('cs.course_id = :id_course AND cus.id_user = :id_user', array(':id_course' => $this->course_id, ':id_user' => Yii::app()->user->id))
									->queryColumn();
			foreach($subscribed_sessions as $id_session)
				$tmp[$this->course_id][$id_session] = $id_session;

			$tmp2[$this->course_id] = CoreUserPuCourse::model()->countByAttributes(array('puser_id' => Yii::app()->user->id, 'course_id' => $this->course_id)) ? true : false;

			Yii::app()->session['PUIsInstructorForCourse'] = $tmp;
			Yii::app()->session['PUIsCourseAssociated'] = $tmp2;

			unset($tmp);
			unset($tmp2);
			unset($subscribed_sessions);
		}

		$this->render('session', array(
			'courseModel' => $this->courseModel, // set in BaseController
			'session_id' => $sessionId,
			'courseSessionModel' => $courseSession,
            'deepLink'          =>  ($this->courseModel->deep_link)? $this->courseModel->getDeepLink() : '',
            		));
	}

	/**
	 * Download certificate file for this course & user
	 */
	public function actionDownloadCertificate() {
		$model = LearningCertificateAssign::loadByUserCourse(Yii::app()->user->id, $this->course_id);
		if ($model) {
			$model->downloadCertificate();
		}
	}


	/**
	 * Render and echo back Dialog2
	 */
	public function actionAxLoUploadDialog() {
		$loType = Yii::app()->request->getParam("lo_type", false);
		$folderKey = Yii::app()->request->getParam("folder_key", false);

		$requestTitle 		= false;
		$requestDescription = false;
		$videoSubtitles = array(); // array to hold video subtitles if LO is a video and there are any subtitles

		switch ($loType) {
			case LearningOrganization::OBJECT_TYPE_SCORMORG:
			case LearningOrganization::OBJECT_TYPE_TINCAN:
			case LearningOrganization::OBJECT_TYPE_AICC:
			case LearningOrganization::OBJECT_TYPE_ELUCIDAT:
				break;
			default:
				$requestTitle = true;
				$requestDescription = true;
				break;
		}

		// if we're editing, get the LO instance
		$loModel = LearningOrganization::model()->findByPk(Yii::app()->request->getParam("id_org"));

		$resource = null;
		if ($loModel) {
			switch ($loType) {
				case LearningOrganization::OBJECT_TYPE_VIDEO:
					$resVideo = LearningVideo::model()->findByPk($loModel->idResource);
					$resource = $resVideo->attributes;
					$resource['id_org'] = $loModel->idOrg;
					$videoFormats = CJSON::decode($resVideo->video_formats);
					$videoNameArr = array_values($videoFormats);
					$resource['filename'] = $videoNameArr[0];

					$videoSubtitles = LearningVideoSubtitles::model()->findAllByAttributes(array('id_video' => $loModel->idResource));
					break;
				case LearningOrganization::OBJECT_TYPE_FILE:
					$resFile = LearningMaterialsLesson::model()->findByPk($loModel->idResource);
					$resource = $resFile->attributes;
					$resource['id_org'] = $loModel->idOrg;
					$resource['filename'] = empty($resFile->original_filename) ? $resFile->path : $resFile->original_filename;
					break;
				case LearningOrganization::OBJECT_TYPE_SCORMORG:
					$resource['id_org'] = $loModel->idOrg;
					$resource['filename'] = $loModel->scormOrganization->scormPackage->path;
					break;
				case LearningOrganization::OBJECT_TYPE_TINCAN:
					$resource['id_org'] = $loModel->idOrg;
					$resource['filename'] = $loModel->tincanMainActivity->ap->path;
					break;
				case LearningOrganization::OBJECT_TYPE_AICC:
					$resource['id_org'] = $loModel->idOrg;
					$aiccPackage = LearningAiccPackage::model()->findByPk($loModel->idResource);
					$resource['filename'] = $aiccPackage->path;
					break;

				default:
					break;
			}
		}

		// Tab-based view will be used always, as per UX designer recommendations
		$view = '_upload_detailed';
		$isListViewLayout = false;
		if($this->course_id)
			$isListViewLayout = LearningCourse::getPlayerLayout($this->course_id)==LearningCourse::$PLAYER_LAYOUT_LISTVIEW;

		// List of all active languages
		$activeLanguagesList = CoreLangLanguage::getActiveLanguages();

		// Load bootstro styles (used in the new/edit video LO modal)
		/* @var CClientScript $cs */
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/js/bootstro/bootstro.css');
		$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/bootstro/bootstro.js');

		$showVideoSubsHints = (count($videoSubtitles) == 0) ? true : false;


		$html = $this->renderPartial($view, array(
			'uploadUrl' 			=> $this->createUrl("axLoUpload"),
			'assetsBaseUrl' 		=> $this->module->assetsUrl,
			'lo_type' 				=> $loType,
			'folder_key' 			=> $folderKey,
			'courseModel' 			=> $this->courseModel,
			'requestTitle' 			=> $requestTitle,
			'requestDescription' 	=> $requestDescription,
			'resource' 				=> $resource,
			'loModel' 				=> $loModel,
			'isListViewLayout'		=> $isListViewLayout,
			'resVideo'              => $resVideo,
			'activeLanguagesList'   => $activeLanguagesList,
			'showHints'             => $showVideoSubsHints,
			'videoSubtitles'        => $videoSubtitles,
		), true);

		$response = new AjaxResult();
		$response->setHtml($html)->setStatus(true);

		echo $response->toJSON();

	}


	/**
	 * Do the actual upload
	 */
	public function actionAxUpload() {

		// HTTP headers for no cache etc
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		$targetDir = Docebo::getUploadTmpPath();
		$cleanupTargetDir = true; // Remove old files
		$maxFileAge = 5 * 3600; // Temp file age in seconds

		// 5 minutes execution time
		@set_time_limit(5 * 60);

		// Uncomment this one to fake upload time
		//usleep(5000);


		// Get parameters
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
		$fileName = isset($_REQUEST["name"]) ? basename($_REQUEST["name"]) : '';
		$deliverableUpload =  Yii::app()->request->getParam("deliverableUpload", false);
		$isCourseDocUpload =  Yii::app()->request->getParam("isCourseDocUpload", false);

		// Clean the fileName for security reasons
		// This generates a bug, since space is replaced by '_' and
		// $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

		// Make sure the fileName is unique but only if chunking is disabled
		if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName) && !$deliverableUpload) {
			$ext = strrpos($fileName, '.');
			$fileName_a = substr($fileName, 0, $ext);
			$fileName_b = substr($fileName, $ext);
			$count = 1;
			while(file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b)) {
				$count++;
			}
			$fileName = $fileName_a . '_' . $count . $fileName_b;
		}

		$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

		// Remove old temp files
		if ($cleanupTargetDir) {
			if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
				while (($file = readdir($dir)) !== false) {
					$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

					// Remove temp file if it is older than the max age and is not the current file
					if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
						@unlink($tmpfilePath);
					}
				}
				closedir($dir);
			} else {
				die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory: ' . $targetDir . '."}, "id" : "id"}');
			}
		}


		// Look for the content type header
		if (isset($_SERVER["HTTP_CONTENT_TYPE"])) {
			$contentType = $_SERVER["HTTP_CONTENT_TYPE"];
		}

		if (isset($_SERVER["CONTENT_TYPE"])) {
			$contentType = $_SERVER["CONTENT_TYPE"];
		}


		// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
		if (strpos($contentType, "multipart") !== false) {
			if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
				// Open temp file
				$out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
				if ($out) {
					// Read binary input stream and append it to temp file
					$in = @fopen($_FILES['file']['tmp_name'], "rb");

					if ($in) {
						while($buff = fread($in, 4096)) {
							fwrite($out, $buff);
						}
					} else {
						die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
					}
					@fclose($in);
					@fclose($out);
					@unlink($_FILES['file']['tmp_name']);
				} else {
					die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
				}
			} else {
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
			}
		} else {
			// Open temp file
			$out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
			if ($out) {
				// Read binary input stream and append it to temp file
				$in = @fopen("php://input", "rb");

				if ($in) {
					while($buff = fread($in, 4096)) {
						fwrite($out, $buff);
					}
				} else {
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
				}

				@fclose($in);
				@fclose($out);
			} else {
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
		}
		}

		// Check if file has been uploaded
		if (!$chunks || $chunk == $chunks - 1) {
			// Strip the temp .part suffix off
			rename("{$filePath}.part", $filePath);

			if($isCourseDocUpload) {
				// Mime type file check
				$mimeTypes = FileTypeValidator::getMimeWhiteListArray(FileTypeValidator::TYPE_ITEM);
				if (!empty($mimeTypes)) {
					$fileHelper = new CFileHelper();
					$fileMimeType = $fileHelper->getMimeType($filePath);
					if(!$fileMimeType || !in_array($fileMimeType, $mimeTypes)) {
						@unlink($filePath);
						die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Detected file type "' . $fileMimeType . '" not allowed"}, "id" : "id", "extra" : "' . $extraParam . '"}');
					}
				}
			}
		}

		die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
	}

	public function actionAxGetCourseStatus(){
		$data = array('courseStatus' => $this->courseStatus);

		$response  = new AjaxResult();
		$response->setData($data)->setStatus(true);
		$response->toJSON();

		Yii::app()->end(0, true);
	}

	/**
	 * (non-PHPdoc)
	 * @see PlayerBaseController::registerResources()
	 */
	public function registerResources() {

		parent::registerResources();

		$cs = Yii::app()->getClientScript();
		$assetsUrl = $this->module->assetsUrl;


		$isClassroom = (Yii::app()->player->course->course_type == LearningCourse::TYPE_CLASSROOM);
		$isWebinar = (Yii::app()->player->course->course_type == LearningCourse::TYPE_WEBINAR);
		$isMobile = Yii::app()->player->course->isMobile();

		if (!Yii::app()->request->isAjaxRequest) {
		    // Maybe we got a request to play directly some LO?
		    // It will become the "first LO to play" when Arena is opened!
		    $forcePlayLoKey  = Yii::app()->request->getParam('lo_key', false);

			/**
			 * NOTE please, the order and placement (head, end, ready,...) of scripts DO MATTER!!!
			 */

			// Load/Register all JS/CSS
			/* @var CClientScript $cs */

			// Dynatree
			$cs->registerScriptFile($assetsUrl . '/js/jquery.dynatree.js');
			$cs->registerCssFile($assetsUrl . '/css/ui.dynatree-modified.css');

			// BlockUI
			$cs->registerScriptFile($assetsUrl . '/js/jquery.blockUI.js');

			// A nice knob plugin
			$cs->registerScriptFile($assetsUrl . '/js/excanvas.js');
			$cs->registerScriptFile($assetsUrl . '/js/jquery.knob.js');

			// Text expander: cuts a string and adds "read more"
			//$cs->registerScriptFile($assetsUrl . '/js/jquery.expander.min.js');
			$cs->registerScriptFile($assetsUrl . '/js/jquery.expander.js');

			// Used by TEST edit/create
			$cs->registerCssFile($assetsUrl.'/css/jquery.dataTables.css');

			// Needed in Videoconf dialogs
			DatePickerHelper::registerAssets(Yii::app()->getLanguage(), CClientScript::POS_HEAD);


			//var_dump(Yii::app()->plupload->getAssetsUrl());

			// Register Player related scripts
			$onLoadArenaMode = Yii::app()->request->getParam("mode", "edit_course");

			// NOTE: next loaded scripts breaks the coaching session datePicker filtering!!!

			// In case of classroom, we don't need Arena stuff
			if (!$isClassroom && !$isWebinar) { $cs->registerScriptFile($assetsUrl . '/js/training.js'); }

			// My Coach widget jquery plugin
			$cs->registerScriptFile($assetsUrl .'/js/jquery.myCoachChat.js');

			// Mobile courses does not allow Blocks/Widgets
			if (!$isMobile) { $cs->registerScriptFile($assetsUrl . '/js/blocks.js'); }

			if (!$isClassroom && !$isWebinar) { $cs->registerScriptFile($assetsUrl . '/js/arena.js'); }

			// Thoug we need this FileUploaderClass, used by blocks/widgets, for example
			$cs->registerScriptFile($assetsUrl . '/js/fileuploader.js');


			// Available Seats (if course if MP)
			$availableSeats = $this->courseModel->getAvailableSeats();

			// We need to know the currently configured File Storage list of allowed Video Formats
			// We create a dummy storage object and retrieve an array of allowed extensions.
			// (see below the Arena init() )
			$dummyStorage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_VIDEO);
			$whitelistArray = $dummyStorage->getVideoWhitelistArray();


			//check if we are displaying a page specific for classroom course, with session id specification
			$blockUrlParameters = array();
			if (PluginManager::isPluginActive('ClassroomApp') && (Yii::app()->player->course->course_type == LearningCourse::TYPE_CLASSROOM)) {
				$idSession = Yii::app()->request->getParam('session_id', 0);
				if ((int)$idSession > 0) { $blockUrlParameters['id_session'] = $idSession; }
			}

			$redirectUrl = '';
			if($this->pathId){
				$blockUrlParameters = $this->helperBuildParamData($blockUrlParameters);
				$redirect = Settings::get('curricula_redirect_to_learning_plan', 'off', true);

				if ($redirect === 'on') {
					$redirectUrl = Docebo::createLmsUrl('curricula/show', array(
						'id_path' => $this->pathId
					));
				}
			}

			unset(Yii::app()->session['courseStatus_' . $this->course_id]);
			Yii::app()->session['courseStatus_' . $this->course_id] = $this->courseStatus;

			// This is to register TinyMce in the main page
			Yii::app()->tinymce->init();

			// Create/Init Player related JS objects
			Yii::app()->clientScript->registerScript('playerAllJs', '

					'.((!$isClassroom && !$isWebinar) ? '

					var Launcher = new LoLauncher({});

					Arena.uploader = new LoUploader();
					Arena.htmledit = new LoHtmlEdit({});
					Arena.googledrive = new LoGoogleDriveEdit({});
					Arena.elucidat = new LoElucidatEdit({});
					Arena.lti = new LoLtiEdit({});
					Arena.deliverable = new LoDeliverable({});
					Arena.test = new LoTest({});
					Arena.poll = new LoPoll({});
					Arena.organizer = new LoOrganizer({});
					Arena.centralRepo = new LoCentralRepoEdit({});


					Launcher.init({course_id: "' . (int) $this->course_id . '"});

					Arena.htmledit.init({editHtmlPageUrl: "' . Yii::app()->createUrl('htmlpage/default/axEditHtmlPage') . '"});
					Arena.deliverable.init({editDeliverableUrl: "' . Yii::app()->createUrl('deliverable/default/axEditDeliverable') . '"});
					Arena.lti.init({editLtiUrl: "' . Yii::app()->createUrl('//LtiApp/LtiApp/axEditLTI') . '"});
					Arena.elucidat.init({editPageUrl: "' . Yii::app()->createUrl('//ElucidatApp/ElucidatApp/axEditElucidat') . '"});
					Arena.googledrive.init({editGoogleDriveUrl: "' . Yii::app()->createUrl('//GoogleDriveApp/GoogleDriveApp/axEditGoogleDrive') . '"});

					' : '').

					// Mobile courses does not allow Blocks/Widgets
					(!$isMobile ?
					'
					Blocks.init({
						course_id: "' . $this->course_id . '",
						'.(isset($idSession) && (int)$idSession > 0 ? 'id_session: '.(int)$idSession.', ' : '').'
						blocksGridUrl: "' . $this->createUrl('block/axLoadBlocksGrid', $blockUrlParameters) . '",
						updateBlocksOrderUrl: "' . $this->createUrl('block/axUpdateOrder', $blockUrlParameters) . '",
						updateCoursedocsOrderUrl: "' . $this->createUrl('block/axUpdateCoursedocsOrder', $blockUrlParameters) . '",
						loadOneBlockUrl: "' . $this->createUrl('block/axLoadOneBlock', $blockUrlParameters) . '",
						loadOneBlockCompletelyUrl: "' . $this->createUrl('block/axLoadOneBlockCompletely') . '"
					});
					' : '').


					((!$isClassroom && !$isWebinar) ? '
					Arena.init({
						course_id: "' . $this->course_id . '",
						courseInfo: {
							idCourse: '.(int)$this->course_id .',
							code: '.CJSON::encode($this->courseModel->code).',
							name: '.CJSON::encode($this->courseModel->name).',
							mpidCourse: '.CJSON::encode($this->courseModel->mpidCourse).',
							isMpCourse: '.CJSON::encode($this->courseModel->mpidCourse > 0).',
							availableSeats: '.CJSON::encode($availableSeats).',
							userHasSeat: '.CJSON::encode(LearningLogMarketplaceCourseLogin::userHasSeat($this->course_id, Yii::app()->user->id)).',
							mpSeatsUnlimited: '.CJSON::encode($this->courseModel->mpSeatsUnlimited()). ',
							status: ' . CJSON::encode($this->courseStatus) . '
						},
						getCourseLearningObjectsTreeUrl: "' . $this->createUrl('axGetCourseLearningObjectsTree') . '",
						createFolderDialogUrl: "' . $this->createUrl('axCreateFolder', array('course_id' => $this->course_id)) . '",
						uploadDialogUrl: "' . $this->createUrl('axLoUploadDialog') . '",
						uploadFilesUrl: "'.$this->createUrl('axUpload').'",
						saveUploadedCoursedocUrl: "'.$this->createUrl('block/axSaveCoursedoc').'",
						openInCentralRepositoryUrl: "'.(Yii::app()->user->getIsGodadmin() ? Yii::app()->createAbsoluteUrl("centralrepo/centralRepo/index") : "").'",
						assetsBaseUrl: "' . $assetsUrl . '",
						pluploadAssetsUrl: 	"' . Yii::app()->plupload->getAssetsUrl() . '",
						csrfToken: "'.Yii::app()->request->csrfToken.'",
						deleteLoUrl: "'.$this->createUrl('axDeleteLo', array('course_id'=>$this->course_id)).'",
						sortLearningObjectUrl: "' . $this->createUrl('axReorderLo') .'",
						moveInFolderLearningObjectUrl: "' . $this->createUrl('axMoveLoToFolder'). '",
						userCanAdminCourse: ' . ($this->userCanAdminCourse ? 'true' : 'false') . ',
						axEditLoPrerequisitesUrl: "' . $this->createUrl('axEditLoPrerequisites') . '",
						onLoadArenaMode: "' . $onLoadArenaMode . '",
						createTestUrl: "' . $this->createUrl('//test/default/axCreateTest') . '",
						createPollUrl: "' . $this->createUrl('//poll/default/axCreatePoll') . '",
						videoExtensionsWhitelist: "'. implode(',', $whitelistArray) .'",
						isMobileCourse: '. ($isMobile ? 'true' : 'false')  .',
						reloadSocialRatingUrl: "'.$this->createUrl('axReloadSocialRatingPanel', array('courseId' => $this->course_id)).'",
					    forcePlayLoKey: "' . $forcePlayLoKey . '",
					    checkStatusUrl: "' . $this->createUrl('AxGetCourseStatus') . '",
					    redirectUrl: "' . $redirectUrl . '",
					});

					' : '').'

			', CClientScript::POS_END);


			if (!$isClassroom && !$isWebinar) {
				// Bind listeners in Document Ready
				Yii::app()->clientScript->registerScript('js2013_1', '
						Arena.uploader.globalListeners();
						Arena.htmledit.globalListeners();
						Arena.elucidat.globalListeners();
						Arena.lti.globalListeners();
						Arena.googledrive.globalListeners();
						Arena.deliverable.globalListeners();
						Arena.test.globalListeners();
						Arena.poll.globalListeners();
						Arena.organizer.globalListeners();
						Arena.centralRepo.globalListeners();
				', CClientScript::POS_READY);
			}

			// Register FancyBox (used in launcher.js)
			Yii::import('common.extensions.fancybox.EFancyBox');
			$scoBox = new EFancyBox();
			$scoBox->init();
		}
	}

	/**
	 * Show a dialog : Create new folder. Plus: Handle the creation process.
	 */
	public function actionAxCreateFolder() {
		$response = new AjaxResult();

		$folder_name = Yii::app()->request->getParam("folder_name", false);
		$orgId = (int) Yii::app()->request->getParam("org_id", -1);
		$parentId = (int) Yii::app()->request->getParam("parent_id", -1);
		$create_folder_button = Yii::app()->request->getParam("create_folder_button", false);

		$folderObj = LearningOrganization::model()->findByPk($orgId);

		// If Submit button is clicked
		if (isset($_POST['create_folder_button'])) {
			if (!empty($folder_name) && ($parentId >= 0)) {
				$loManager = new LearningObjectsManager();

				if ($folderObj) {
					// edit operation
					$success = $loManager->updateFolderTitle($folderObj->idOrg, $folder_name);
				} else {
					// create operation
					$loManager->setIdCourse($this->course_id);
					$success = $loManager->addFolder($folder_name, $parentId);
				}

				$response->setStatus($success)->toJSON();
				Yii::app()->end();
			} else {
				$response
					->setMessage(Yii::t('standard','Please fill all required fields'))
					->setStatus(false)->toJSON();
				Yii::app()->end();
			}
		}
		// Check Parent Id
		if ($parentId >= 0) {
			$loModel = LearningOrganization::model()->findByPk($parentId);
			$html = $this->renderPartial("_create_folder_dialog", array(
				'parent_id' => $parentId,
				'loModel' => $loModel,
				'folderObj' => $folderObj
			), true, true);
			$response->setStatus(true)->setHtml($html)->toJSON();
			Yii::app()->end();
		} else {
			$response
				->setMessage(Yii::t('player','Please select a parent folder!'))
				->setStatus(false)->toJSON();
			Yii::app()->end();
		}
	}


	/**
	 *	Loads LO tree for the current Player's Course Id and sends back a JSON result for Dynatree
	 *
	 */
	public function actionAxGetCourseLearningObjectsTree() {
		$response     = new AjaxResult();
		$treeType     = Yii::app()->request->getParam('tree_type', 'play,management');
		$forcePlayLoKey  = Yii::app()->request->getParam('lo_key', false);
		$lastPopupPlayed = Yii::app()->request->getParam('last_popup_played', false);
		$lastPlayedLo = Yii::app()->request->getParam('lastPlayedLo', false);

		if($forcePlayLoKey === 'false')
			$forcePlayLoKey = false;
		elseif($forcePlayLoKey === 'true')
			$forcePlayLoKey = true;

		try {
			$requestTypes = explode(',', $treeType);
			$data = array();
			$data['player_layout'] = LearningCourse::getPlayerLayout($this->course_id);
			$data['fistToPlayCheck'] = false;
			foreach ($requestTypes as $type) {
				switch ($type) {
					case 'play':
					{
						$treeParams               = array('userInfo' => true, 'scormChapters' => true, 'showHiddenObjects' => false, 'aiccChapters' => true);
						$data[$type]              = LearningObjectsManager::getCourseLearningObjectsTree($this->course_id, $treeParams);
						if(!$forcePlayLoKey && $lastPlayedLo)
							$forcePlayLoKey = $lastPlayedLo;
						$data['firstToPlay']      = LearningObjectsManager::getFirstObjectToPlay($this->course_id, false, $forcePlayLoKey);
						$data['firstToPlayButton'] = false;

						if($data['firstToPlay']['status'] == LearningCommontrack::STATUS_COMPLETED)
						{
							$data['firstToPlayButton'] = $data['firstToPlay'];
							if(!$lastPlayedLo)
								$data['firstToPlay'] = false;
						}

						$data['resume_autoplay']  = LearningCourse::getResumeAutoplayEnabled($this->course_id);
						$data['firstToPlayButton'] = false;
						// With the following we are trying to prevent the new window opening every time the player page is reloaded for LOs
						// in "external page" mode when autoplay is enabled.
						if ($lastPopupPlayed !== false && $data['firstToPlay'] !== false && $data['firstToPlay']['idOrganization'] == $lastPopupPlayed) {
							$data['firstToPlayButton'] = $data['firstToPlay'];
							$data['resume_autoplay'] = false;
						} else {
							$sessionPrefix = 'course_session.'.$this->course_id.'.';
							if ($data['firstToPlay'] !== false && Yii::app()->session[$sessionPrefix.'last_played_lo'] == $data['firstToPlay']['idOrganization'] &&  (LearningCourse::getResumeAutoplayEnabled($this->course_id))) {
								if ($data['type'] == 'sco' && Yii::app()->session[$sessionPrefix.'last_played_lo_sco'] == $data['idScormItem']) {
									$data['firstToPlayButton'] = $data['firstToPlay'];
									$data['resume_autoplay'] = false;
								} elseif ($data['type'] != 'sco') {
									$data['firstToPlayButton'] = $data['firstToPlay'];
									$data['resume_autoplay'] = false;
								}
							}
						}
						$stats                    = LearningObjectsManager::getCourseLearningObjectsStats($this->course_id, Yii::app()->user->id, true);
						$data['countCompleted']   = $stats[LearningCommontrack::STATUS_COMPLETED];
						$data['countTotal']       = $stats['total'];
					} break;
					case 'management':
					{
						$treeParams = array('userInfo' => false, 'scormChapters' => false, 'showHiddenObjects' => true, 'aiccChapters' => false);
						$data[$type] = LearningObjectsManager::getCourseLearningObjectsTree($this->course_id, $treeParams);
					} break;
				}
			}
			$response->setData($data)->setStatus(true);
		} catch(CException $e) {
			$response->setMessage($e->getMessage())->setStatus(false);
		}
		$response->toJSON();
		Yii::app()->end();
	}


	/**
	 * Change sequence parameter of learning objects/folder of the same level
	 */
	public function actionAxReorderLo() {
		$idObject = Yii::app()->request->getParam('id_object', 0);
		//$newPosition = (int)Yii::app()->request->getParam('new_position', -1);
		$nodeBefore = Yii::app()->request->getParam('before', NULL);
		$nodeAfter = Yii::app()->request->getParam('after', NULL);

		$trans = Yii::app()->db->beginTransaction();
		$response = new AjaxResult();

		try {

			//validate input
			if (empty($nodeBefore) && empty($nodeAfter)) {
				throw new CException( Yii::t('error', 'Invalid input data') );
			}

			$object = LearningOrganization::model()->findByPk($idObject);
			if (!$object) {
				throw new CException( Yii::t('error', 'Object ID is invalid.') );
			}

			//do the moving operation
			if (!empty($nodeBefore)) {
				$targetNode = LearningOrganization::model()->findByPk($nodeBefore);
				if (!$targetNode) {
					throw new CException( Yii::t('error', 'Invalid target node ID') . ': ' . $nodeBefore);
				}
				$rs = $object->moveAfter($targetNode);
			} else {
				$targetNode = LearningOrganization::model()->findByPk($nodeAfter);
				if (!$targetNode) {
					throw new CException( Yii::t('error', 'Invalid target node ID') . ': ' . $nodeAfter);
				}
				$rs = $object->moveBefore($targetNode);
			}

			if (!$rs) {
				throw new CException( Yii::t('error', 'Error while moving node') );
			}

			$trans->commit();
			$response->setData(array(
				'play' => LearningObjectsManager::getCourseLearningObjectsTree($this->course_id, array('userInfo' => true, 'scormChapters' => true, 'aiccChapters' => true)),
				'management' => LearningObjectsManager::getCourseLearningObjectsTree($this->course_id, array('userInfo' => false, 'scormChapters' => false, 'aiccChapters' => false)),
				'firstToPlay' => $data['firstToPlay'] = LearningObjectsManager::getFirstObjectToPlay($this->course_id),
			));
			$response->setStatus(true);

		} catch (CException $e) {

			$trans->rollback();
			$response->setMessage($e->getMessage())->setStatus(false);
		}

		echo $response->toJSON();
	}


	/**
	 * Move a learning object into a new tree folder
	 */
	public function actionAxMoveLoToFolder() {
		$idFolder = Yii::app()->request->getParam('id_folder', 0);
		$idObject = Yii::app()->request->getParam('id_object', 0);

		$trans = Yii::app()->db->beginTransaction();
		$response = new AjaxResult();

		try {
			$objectNode = LearningOrganization::model()->findByPk($idObject);
			if (!$objectNode) {
				throw new CException( Yii::t('error', 'Invalid Object ID') . ': ' . $idObject );
			}

			if ($idFolder == 0) {
				$folderNode = LearningObjectsManager::getRootNode($this->course_id);
			} else {
				$folderNode = LearningOrganization::model()->findByPk($idFolder);
			}
			if (!$folderNode) {
				throw new CException( Yii::t('error', 'Invalid Folder ID') . ': ' . $idFolder);
			}

			if (!$objectNode->moveAsLast($folderNode)) {
				throw new CException( Yii::t('standard', '_OPERATION_FAILURE') );
			}

			$trans->commit();
			$response->setData(array(
				'play' => LearningObjectsManager::getCourseLearningObjectsTree($this->course_id, array('userInfo' => true, 'scormChapters' => true, 'aiccChapters' => true)),
				'management' => LearningObjectsManager::getCourseLearningObjectsTree($this->course_id, array('userInfo' => false, 'scormChapters' => false, 'aiccChapters' => false)),
				'firstToPlay' => LearningObjectsManager::getFirstObjectToPlay($this->course_id),
			));
			$response->setStatus(true);

		} catch (CException $e) {

			$trans->rollback();
			$response->setMessage($e->getMessage())->setStatus(false);
		}

		echo $response->toJSON();
	}


	/**
	 * Delete a learning object or a Folder of learning objects (*including* its content)
	 *
	 */
	public function actionAxDeleteLo() {
		$idObject = Yii::app()->request->getParam('id_object', 0);
		$confirmDelete = Yii::app()->request->getParam('confirm_delete_lo', false);
		$setCourseAsCompleted = Yii::app()->request->getParam('set_course_as_completed', false);

		// Bad model means 'get out'
		$loModel = LearningOrganization::model()->findByPk($idObject);
		if (!$loModel) {
			echo Yii::t('standard', '_OPERATION_FAILURE');
			Yii::app()->end();
		}

		// Is this a post request
		if (Yii::app()->request->isPostRequest && $confirmDelete) {
			try {
				// allow custom plugin to delete custom info
				//Yii::app()->event->raise('DeleteTestInfoFromCustomPlugin', new DEvent($this, array('testId' => $loModel->idResource)));
				$idOrg = $loModel->idOrg;
				$idOrgCourse = $this->courseModel->final_object;
				$idInitialOrgCourse = $this->courseModel->initial_object;
				$resourceDeleteSuccess = LearningObjectsManager::deleteLearningObject($idObject, array('throwExceptions' => true));

				Yii::app()->event->raise('AfterDeleteLo', new DEvent($this, array('courseModel' => $this->courseModel)));
				if ( ! $resourceDeleteSuccess) {
					throw new CException( Yii::t('standard', '_OPERATION_FAILURE' ) );
				}

				if($this->courseModel->final_score_mode == LearningCourse::FINAL_SCORE_TYPE_KEY_OBJECT && $idOrg == $idOrgCourse)
				{
					LearningCourseuser::model()->updateAll(array('score_given' => NULL), 'idCourse = '.$this->courseModel->idCourse);
					LearningCourse::model()->updateByPk($this->courseModel->idCourse, array('final_score_mode' => NULL));
				}
				if($this->courseModel->initial_score_mode == LearningCourse::INITIAL_SCORE_TYPE_KEY_OBJECT && $idOrg == $idInitialOrgCourse)
				{
					LearningCourseuser::model()->updateAll(array('initial_score_given' => NULL), 'idCourse = '.$this->courseModel->idCourse);
					LearningCourse::model()->updateByPk($this->courseModel->idCourse, array('initial_score_mode' => NULL));
				}

				//	If the super admin deletes a LO after a user has already started an E-learning course and
				//	if the user has completed all other LOs, but not the one that is deleted,
				//	then mark the course as completed for that user
				if($setCourseAsCompleted && $this->courseModel->course_type == LearningCourse::TYPE_ELEARNING) {
					$this->changeStatusToCompleted($this->courseModel->idCourse, $idOrg);
				} else {
					Log::_('DELETION: Object with idOrg '.$idOrg.' from course '.$this->courseModel->idCourse.' deleted by user '.Yii::app()->user->id, CLogger::LEVEL_WARNING);
					echo "<a class='auto-close success'></a>";
					Yii::app()->end();
				}

			} catch (CException $e) {

				Yii::log($e->getMessage(), 'error');
				Yii::log($e->getTraceAsString(), 'error');
				echo $e->getMessage();
				Yii::app()->end();
			}
		}

		$this->renderPartial('_delete_lo', array(
				'loModel' => $loModel,
				'courseType' => $this->courseModel->course_type,
				'hasFinalLearningObject' => $this->courseModel->hasEndObjectLo()
		));
	}

	/**
	 * Set the course as "completed" if the current status is "in progress"
	 * and the user has completed all LOs
	 * @param $idCourse
	 * @param $idOrg
	 */
	protected function changeStatusToCompleted($idCourse, $idOrg) {

		$course = new LearningCourse();
		$course->idCourse = $idCourse;
		$hasFinalLearningObject = $course->hasEndObjectLo();
		// change the status only if there aren't any final LOs (in order to complete the course, the user has to complete all LO)
		if(!$hasFinalLearningObject) {
			$loCount = Yii::app()->db->createCommand()
				->select('COUNT(idOrg)')
				->from(LearningOrganization::model()->tableName())
				->where('objectType <> "" AND idCourse = :idCourse', array(':idCourse' => $idCourse))
				->queryScalar();

			$users = Yii::app()->db->createCommand()
				->select('ct.idUser, COUNT(ct.idReference) AS cnt')
				->from(LearningCommontrack::model()->tableName().' ct')
				->join(LearningOrganization::model()->tableName().' lo', 'lo.idOrg = ct.idReference AND lo.idCourse = :idCourse', array(':idCourse' => $idCourse))
				->join(LearningCourseuser::model()->tableName().' cu', 'cu.idCourse = lo.idCourse AND cu.idUser = ct.idUser')
				->where('lo.idCourse = :idCourse AND ct.status = :loCompleted AND cu.status = :courseInProgress', array(
					':loCompleted' => LearningCommontrack::STATUS_COMPLETED,
					':courseInProgress' => LearningCourseuser::$COURSE_USER_BEGIN
				))
				->group('ct.idUser')
				->having('cnt = :loCount', array(':loCount' => $loCount))
				->queryColumn();
		}

		$html = '';
		if(!$hasFinalLearningObject && $loCount > 0 && count($users) > 0) {
			Yii::log('Course status changed to completed for users: '.implode($users).' after deleting LO (idOrg = '.$idOrg.')', 'debug');
			// change the status progressive
			$html = $this->renderPartial('admin.protected.views.courseManagement/_mass_enrollment_status_change_last_step', array(
				'newStatus' => LearningCourseuser::$COURSE_USER_END,
				'users' => $users,
				'currentCourseId' => $idCourse,
				'afterUpdateCallback' => false
			), true);
		}

		Log::_('DELETION: Object with idOrg '.$idOrg.' from course '.$idCourse.' deleted by user '.Yii::app()->user->id, CLogger::LEVEL_WARNING);

		echo $html."<a class='auto-close success'></a>";
		Yii::app()->end();
	}

	/**
	 * FOR TESTS ONLY
	 */
	public function actionTest() {

	}

	public function actionAxUpdateSelectedFolder() {
		if(isset($_POST['currentFolder']) && $_POST['currentFolder']) {
			Yii::app()->session['currentFolder.'.$this->course_id] = $_POST['currentFolder'];
		}
	}

	/**
	 * development function: this will update the learning_organization table with proper iLeft/iRight values and nodes
	 * WARNING: all subfolders will be "flattened" at root level
	 */
	public function actionLoTreeUpdate() {

		$db = Yii::app()->db;
		$trans = $db->beginTransaction();

		try{

			//delete all roots
			$rs = $db->createCommand("DELETE FROM learning_organization WHERE idCourse = 0 OR iLeft = 1")->execute();
			if (!$rs) {
				throw new CException("error while clearing previous roots");
			}

			//get all existing courses and cycle for every course
			$courses = $db->createCommand("SELECT * FROM learning_course")->query();
			foreach ($courses as $course) {

				//get course LOs
				$orgs = $db->createCommand("SELECT * FROM learning_organization WHERE idCourse = ".$course['idCourse']." ORDER BY iLeft, title")->query();
				$i = 0;
				//update left/right for every LO
				foreach ($orgs as $org) {
					$rs = $db->createCommand("UPDATE learning_organization SET "
						." iLeft = ".(($i+1)*2).", "
						." iRight = ".(($i+1)*2+1).", "
						." lev = 2 "
						." WHERE idOrg = ".$org['idOrg'])->execute();
					if (!$rs) {
						throw new CException("error while updating left/right (idcourse=" . $course['idCourse'] . ";i=" . $i . ")");
					}

					$i++;
				}

				//create course LOs root
				$rs = $db->createCommand("INSERT INTO `learning_organization` (`idParent`, `lev`, `iLeft`, `iRight`, `title`, `idCourse`, `visible`) "
					." VALUES ('0', '1', '1', '".(($i+1)*2)."', '".LearningOrganization::ROOT_NODE_PREFIX.$course['idCourse']."', '".$course['idCourse']."', '0')")->execute();
				if (!$rs) {
					throw new CException("error while creating course LO root for course: " . $course['idCourse']);
				}

			}

			$trans->commit();
			echo "LO Tree successfully updated !!";

		} catch(CException $e) {
			$trans->rollback();
			echo "<b>ERROR:</b>&nbsp;".$e->getMessage();
		}
	}


	/**
	 * AJAX called method, dedicated to "track course level access done by a subscriber"
	 *
	 */
	public function actionAxTrackCourseAccess() {
		$response = new AjaxResult();
		try {
			$sessionPrefix = 'course_session.'.$this->course_id.'.';
			if(isset($_POST['lo_data']) && !is_null($_POST['lo_data']) && isset($_POST['lo_data']['idOrganization']))
			{
				Yii::app()->session['last_played_lo'] = $_POST['lo_data']['idOrganization'];
				Yii::app()->session[$sessionPrefix.'last_played_lo'] = $_POST['lo_data']['idOrganization'];
				if ($_POST['lo_data']['type'] == 'sco')
				{
					Yii::app()->session['last_played_lo_sco'] = $_POST['lo_data']['idScormItem'];
					Yii::app()->session[$sessionPrefix.'last_played_lo_sco'] = $_POST['lo_data']['idScormItem'];
				}
				else
				{
					Yii::app()->session['last_played_lo_sco'] = false;
					Yii::app()->session[$sessionPrefix.'last_played_lo_sco'] = false;
				}
			}
			// If the course is a Marketplace course ... and
			// If user still does NOT have a seat ...  and
			// If there are still available seats ...
			// Take seat for the current user
			if($course && ($course->mpidCourse > 0)) {
				if( (! LearningLogMarketplaceCourseLogin::userHasSeat($this->course_id, Yii::app()->user->id)) && ($course->getAvailableSeats() > 0) )
				{
					LearningLogMarketplaceCourseLogin::takeASeat($this->course_id, Yii::app()->user->id);
				}
			}

			// Update subscriber's access
			if ($this->userIsSubscribed) {
				LearningCourseuser::updateCourseUserAccess(Yii::app()->user->id, $this->course_id);
			}
			else {
				// Auto-subscribe all godAdmins as Instructors
				if ( Yii::app()->user->getIsGodadmin() || Yii::app()->user->getIsAdmin()){
					$level = (Yii::app()->user->getIsGodadmin() || (Yii::app()->user->getIsAdmin() && Yii::app()->user->checkAccess('/lms/admin/course/mod'))) ? LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR : LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT;
					$model = new LearningCourseuser();
					if(Yii::app()->user->getIsAdmin())
						$model->scenario = 'self-subscribe';
					$model->subscribeUser(Yii::app()->user->id, Yii::app()->user->id, $this->course_id, 0, $level);
					LearningCourseuser::updateCourseUserAccess(Yii::app()->user->id, $this->course_id);
				}
			}

			$response->setData("")->setStatus(true)->toJSON();

		} catch (CException $e) {
			Yii::log('actionAxTrackCourseAccess() error: '.$e->getMessage(), CLogger::LEVEL_ERROR);
			$response->setMessage($e->getMessage())->setStatus(false)->toJSON();
		}
	}


	/**
	 * Returns  HTML representing a Common Launchpad to launch any type of playable objects
	 * i.e. a background image and a big PLAY button, as well as Quick navigation band at bottom
	 *
	 * @throws CException
	 */
	public function actionAxGetCommonLaunchpad() {

		// Get LO object data
		$lo_data = Yii::app()->request->getParam('lo_data', false);
		$response = new AjaxResult();

		try {

			// get launchpad bg image
			$courseModel = $this->courseModel ? $this->courseModel : LearningCourse::model()->findByPk($this->course_id);
			$playerBgImage = $courseModel->getPlayerBgImage();

			Yii::app()->event->raise('BeforeLaunchpadDisplay', new DEvent($this, array(
				'course' => $courseModel,
				'user' => Yii::app()->user->loadUserModel(),
			)));

			// Render HTML, return it, process output!!
			$html = $this->renderPartial('_common_launchpad', array(
				'lo_data' => $lo_data,
				'player_bg' => $playerBgImage
			),true,true);

			$data = array(
				'html' => $html,
			);

			// Send response
			$response->setStatus(true)->setData($data)->toJSON();
			Yii::app()->end();

		} catch (CException $e) {
			$response->setMessage($e->getMessage())->setStatus(false)->toJSON();
		}

	}

	public function actionEditTincan(){

		if(!$this->userCanAdminCourse){
			// Only course managers can edit a Tincan
			throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
		}

		$loType = Yii::app()->request->getParam("lo_type", false);

		$loModel = LearningOrganization::model()->findByPk(Yii::app()->request->getParam("org_id"));

		$response = new AjaxResult();

		if($loModel){

			// WATCH OUT!!! $_POST is set even when the form is initially loaded!
			// So don't just assume that the form is submitted based on that fact,
			// actually check if $_POST contains what we need (e.g. title of the LO)

			if(isset($_POST['file-title'])){
				$title = $_POST['file-title'];
				$desc = $_POST['description'];

				$loModel->title = $title;

				// Save the updated LO
				if (!$loModel->saveNode()) {
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				}

				$response->setStatus(true)->toJSON();
				Yii::app()->end();
			}

			$resource = $loModel->attributes;
			$resource['id_org'] = $loModel->idOrg;
		}else{
			// LO doesn't exist
			throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
		}

		$html = $this->renderPartial('_edit_tincan', array(
			'resource' => isset($resource) ? $resource : null,
		), true);

		$response->setStatus(true)->setHtml($html)->toJSON();
		Yii::app()->end();
	}


	/**
	 * Show Prerequisites Dialog
	 *
	 */
	public function actionAxEditLoPrerequisites() {


		// Get incoming data, if any
		$id_object = Yii::app()->request->getParam("id_object", false);

		try {

			// No LO Id means: ERROR
			if (!$id_object) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				Yii::app()->end();
			}

			// Get the LO
			$loModel = LearningOrganization::model()->findByPk($id_object);

			// LearninOrganization model not found???  Error!!
			if (!$loModel) {
				throw new CException("Learning Object model not found (ID:{$id_object})"); // don't translate please
				Yii::app()->end();
			}

			// SAVING data requested? (submit button)
			if (isset($_REQUEST['confirm_save_prerequisites'])) {
				// Get selected LO idOrgs from incoming request
				$prerequisitesString = Yii::app()->request->getParam("selected_prereq", false);
				$loModel->prerequisites = $prerequisitesString;
				if (!$loModel->saveNode()) {
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				}

				echo '<a class="auto-close success"></a>';
				Yii::app()->end();
			}

			// Otherwise... show the View and do the real work
			$prerequisitesString = trim($loModel->prerequisites);
			$prerequisitesArray = explode(",", $prerequisitesString);

			// Load the LO Tree:
			$treeParams = array(
				'userInfo' => true,
				'scormChapters' => false,
				'excludeEmptyFolders' => true,
				'excludeObjects' => array($loModel->idOrg)
			);
			$loTreeData = LearningObjectsManager::getCourseLearningObjectsTree($loModel->idCourse, $treeParams);

			// RENDER View and echo back to Dialog2
			$html = $this->renderPartial("_edit_prerequisites", array(
					'loModel' => $loModel,
					'prerequisitesArray' => $prerequisitesArray,
					'loTreeData' => $loTreeData,
			), true, true);

			echo $html;
			Yii::app()->end();
		} catch (CException $e) {
			$html = $this->renderPartial("_edit_prerequisites_error", array(
					'error' => $e->getMessage(),
			), true, true);
			echo $html;
			Yii::app()->end();
		}


	}


	/**
	 * Edit Learning object properties
	 */
	public function actionAxConfigureLo() {

		$idOrg = (int) Yii::app()->request->getParam('idOrg', false);
		$showNumberOfViewsOption = true;

		try {
			$loModel = LearningOrganization::model()->findByPk($idOrg);
			if (!$loModel) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}


			// Set some default values, if not set in model
			$loModel->setDefaultValues();

			// Get LO type, just in case
			$loType = $loModel->objectType;

			// If the type of the LO is Survey(Poll) we're hiding the Number Of Views Option
			if($loType == LearningOrganization::OBJECT_TYPE_POLL){
				$showNumberOfViewsOption = false;
			}

			// SAVING data requested? (submit button)
			if (isset($_REQUEST['LearningOrganization'])) {

				$loModel->attributes = $_REQUEST['LearningOrganization'];

				switch($loType){
					// Tests and surveys can't be published unless
					// they have at least one valid question in them

					case LearningOrganization::OBJECT_TYPE_TEST:
						$hasQuestions = LearningTestQuestRel::model()->findByAttributes(array('id_test'=>$loModel->idResource));
						if(!$hasQuestions && $loModel->visible){
							// You are trying to set a Test LO as visible, but it doesn't have any questions
							throw new CException(Yii::t('player', 'Can not publish this learning object until it has at least one question'));
						}
						break;
					case LearningOrganization::OBJECT_TYPE_POLL:
						$hasQuestions = LearningPollquest::model()->findByAttributes(array('id_poll'=>$loModel->idResource));
						if(!$hasQuestions && $loModel->visible){
							// You are trying to set a Test LO as visible, but it doesn't have any questions
							throw new CException(Yii::t('player', 'Can not publish this learning object until it has at least one question'));
						}
						break;
				}

				if ($loModel->validate()) {
					if (!$loModel->saveNode()) {
						throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
						Yii::app()->end();
					}
					echo "<a class='auto-close success'></a>";
					Yii::app()->end();
				}

			}


			$html = $this->renderPartial("_configure_lo", array(
					'loModel' => $loModel,
					'showNumberOfViewsOption' => $showNumberOfViewsOption
			), true, true);


			echo $html;
			Yii::app()->end();

		} catch (CException $e) {

			$html = $this->renderPartial("_configure_lo", array(
					'error' => true,
					'errorMessage' => $e->getMessage(),
					'showNumberOfViewsOption' => $showNumberOfViewsOption
			), true, true);
			echo $html;
			Yii::app()->end();
		}

	}


	/**
	 * Return Quick Navigation HTML
	 */
	public function actionAxGetQuickNavHtml() {
		$course_id = (int) Yii::app()->request->getParam('course_id', false);

		if (!$course_id) {
			echo 'Invalid course Id';
			Yii::app()->end();
		}

		$this->renderPartial('_quick_navigation_band', array(
			'course_id'=>$course_id,
		), false, true);

	}
	public function actionAxGetPlayerNavigator(){
		$idCourse = $this->course_id;
		$idCurrentKey = isset($_REQUEST['current_key']) ?  $_REQUEST['current_key'] : null;
		$state = isset($_REQUEST['state']) ? $_REQUEST['state'] : false;

		if(!$idCurrentKey && !$state){
			Docebo::fail('No current LO id set for PlayerNavigator', __CLASS__);
		}

		$this->renderPartial('_player_navigator', array(
			'courseModel'=>$this->courseModel,
			'currentKey'=>$idCurrentKey,
			'state' => $state,
		), false, true);
	}


	/**
	 * Get general LO stats (completed/total)
	 */
	public function actionAxGetLOStats() {
		$response = new AjaxResult();
		$stats = LearningObjectsManager::getCourseLearningObjectsStats($this->course_id);
		$data['countCompleted'] = $stats[LearningCommontrack::STATUS_COMPLETED];
		$data['countTotal'] = $stats['total'];
		$response->setData($data)->setStatus(true);
		$response->toJSON();
		Yii::app()->end();
	}


	/**
	 * Returns HTML ready to be shown in Main Menu LO Counters point  - general LO stats (completed/total)
	 */
	public function actionAxGetLoCountersHtml() {
		$response = new AjaxResult();
		$enable_legacy_menu = Settings::get('enable_legacy_menu', false);
		$stats = LearningObjectsManager::getCourseLearningObjectsStats($this->course_id, Yii::app()->user->id, true);
		if ($enable_legacy_menu === 'on') {
			$html = '<strong id="menu-play-lo-stats-completed">' . $stats[LearningCommontrack::STATUS_COMPLETED]
					. '</strong> / <span id="menu-play-lo-stats-total">' . $stats['total']
					. '</span>';
		} else {
			$html = '<strong id="menu-play-lo-stats-completed">' . $stats[LearningCommontrack::STATUS_COMPLETED]
					. '</strong>&#47;<span id="menu-play-lo-stats-total">' . $stats['total']
					. '</span>';
		}
		echo $html;
		Yii::app()->end();
	}

	/**
	 * Called by JS code to check LO Video conversion status which is kept in "visible" field:
	 * 	-1   :  in_progress
	 *  -2	 :  error (set by the callback received from Amazon Elastic Transcoder
	 *   0,1 :  converted
	 *
	 *
	 * @param number $idOrg  LO Id
	 */
	public function actionAxGetVideoConversionStatus($idOrg) {
		$response = new AjaxResult();

		// No IdOrg means we are doing something wrong. Return 'error' status
		if (! (int) $idOrg) {
			$status = 'error';
		}
		else {
			$loModel = LearningOrganization::model()->findByPk($idOrg);

			// No model found, or Amazon called back with 'error' status, or object is NOT a video??
			if (!$loModel || ($loModel->visible == -2) || ($loModel->objectType != LearningOrganization::OBJECT_TYPE_VIDEO)) {
				$status = 'error';
			}
			else {
				if (in_array($loModel->visible, array(0,1))) {
					$status = 'converted';
				}
				else if ($loModel->visible == -1) {
					$status = 'in_progress';
				}
			}
		}

		// Send back JSON
		$data = array('status'	=> $status);
		$response->setStatus(true)->setData($data)->toJSON();
		Yii::app()->end();
	}

	/**
	 * Chekck VIDEO LOs for their progress status, e.g. during transcoding.
	 *
	 * Additionally, it will set the LO status to error (-2) if it is tool old.
	 *
	 */
	public function actionAxCheckVideoStatuses() {
		$idOrgs = Yii::app()->request->getParam('idOrgs', array());
		$response = new AjaxResult();
		$statuses = array();
		$data = array('statuses'	=> $statuses);

		if (is_array($idOrgs)) {

			foreach ($idOrgs as $idOrg) {

				$loModel = LearningOrganization::model()->findByPk($idOrg);

				// Do this FIRST!!!
				if ($loModel) {
					// Set to ERROR if in PROGRESS and too old
					$loModel->setOldInProgressVideosToError();
				}

				// No model found, or Amazon called back with 'error' status, or object is NOT a video??
				if (!$loModel || ($loModel->visible == -2) || ($loModel->objectType != LearningOrganization::OBJECT_TYPE_VIDEO)) {
					$status = 'error';
				}
				else {
					if (in_array($loModel->visible, array(0,1))) {
						$status = 'converted';
					}
					else if ($loModel->visible == -1) {
						$status = 'in_progress';
					}
				}
				$statuses[] = array(
					"idOrg"		 => $idOrg,
					"status" 	 => $status,
					"visible" 	 => $loModel->visible,
				);
			}
			$data = array(
				'statuses' => $statuses,
			);
		}

		$response->setStatus(true)->setData($data)->toJSON();
		Yii::app()->end();
	}


	public function actionLoading() {

		$this->renderPartial('loading', array(

		));
	}

	public function actionAxReloadSocialRatingPanel()
	{
		$courseId = Yii::app()->getRequest()->getParam('courseId');
		$courseModel = LearningCourse::model()->findByPk($courseId);
		if ($courseModel)
		{
			$cs = Yii::app()->clientScript;
			$cs->scriptMap['jquery.textPlaceholder.js'] = false;
			$cs->scriptMap['gamification.css'] = false;
            $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/clipboard.min.js');

			echo $this->renderPartial('_social_rating', array('courseModel' => $courseModel,'deepLink'=>(($this->courseModel->deep_link)? $this->courseModel->getDeepLink() : '')), true, true);
			exit;
		}
	}

	/**
	 * Main webinar sessions view
	 */
	public function actionWebinarSession() {
		$cs = Yii::app()->getClientScript();

        $themeUrl = Yii::app()->theme->baseUrl;

        $cs->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
        $cs->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');

		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/classroom-attendance-sheet.css');
		$adminPath = Yii::app()->getRequest()->getBaseUrl() . '/../admin';
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/admin.css');
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/webinar.css');

		// Bootcamp menu
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/docebo_bootcamp.css');

		// Load the datepicker (used in the new/edit session modal)
		DatePickerHelper::registerAssets();

		// Load bootstro styles (used in the new/edit session modal)
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/js/bootstro/bootstro.css');
        $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/clipboard.min.js');

        $webinarAssetsUrl 		 = Yii::app()->assetManager->publish(Yii::getPathOfAlias("webinar.assets"));
        $webinarWidgetsAssetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias("webinar.widgets.assets"));
        Yii::app()->clientScript->registerScriptFile($webinarAssetsUrl."/js/sessionForm.js");
        Yii::app()->clientScript->registerScriptFile($webinarWidgetsAssetsUrl."/sessionInfo.js");
        Yii::app()->clientScript->registerCssFile($webinarAssetsUrl."/css/sessionForm.css");

		$courseSession = null;
		$sessionId = null;

		if ($this->userIsSubscribed)
			LearningCourseuser::updateCourseUserAccess(Yii::app()->user->id, $this->course_id);

		$puWithSessionPermission = Yii::app()->user->getIsPu() &&
			(Yii::app()->user->checkAccess('/lms/admin/webinarsessions/view') ||
			Yii::app()->user->checkAccess('/lms/admin/webinarsessions/add') ||
			Yii::app()->user->checkAccess('/lms/admin/webinarsessions/mod')) &&
			CoreUserPuCourse::isCourseAssignedToPU(Yii::app()->user->id, $this->course_id);

		$sessionId = Yii::app()->request->getParam('session_id');
		if ($sessionId) { // Digging into a specific session
			$courseSession = WebinarSession::model()->findByAttributes(array('id_session' => $sessionId));
			if (!$courseSession)
				throw new CHttpException("Invalid session id");

			// save the session name in the current session, to be used in the CourseHeader view
			Yii::app()->session['course_header_session_'.time()] = $courseSession->name;

			// If multiple sessions sessions are available for the course, go to the 'select session' page when
			// clicking on the course name in the breadcrumbs
			$enrolledSessions = WebinarSessionUser::model()->sessionSelectDataProvider(Yii::app()->user->id, $this->course_id);
			if ($enrolledSessions->getTotalItemCount() > 1) {
				$this->breadcrumbs[Docebo::ellipsis($this->courseModel->name, 60, '...')] = Docebo::createAppUrl('lms:webinar/session/select', array('course_id' => $this->course_id));
				$this->breadcrumbs[] = Docebo::ellipsis($courseSession->name, 60, '...');
			} else {
				$this->breadcrumbs[] = Docebo::ellipsis($this->courseModel->name, 60, '...');
				$this->breadcrumbs[] = Docebo::ellipsis($courseSession->name, 60, '...');
			}
		} else {
			if (!$puWithSessionPermission && !$this->userCanAdminCourse)
				$this->_showWebinarSessionLearnerView();
			else {
				$this->breadcrumbs[] = Docebo::ellipsis($this->courseModel->name, 60, '...');
				$this->breadcrumbs[] = Yii::t('classroom', 'Sessions and Enrollments');
			}
		}

		if(Yii::app()->user->getIsPu() && $this->userCanAdminCourse)
		{
			$tmp = array();
			$tmp2 = array();
			if(!is_null(Yii::app()->session['PUIsInstructorForCourse']))
				$tmp = Yii::app()->session['PUIsInstructorForCourse'];
			if(!is_null(Yii::app()->session['PUIsCourseAssociated']))
				$tmp2 = Yii::app()->session['PUIsCourseAssociated'];

			$subscribed_sessions =	Yii::app()->db->createCommand()->select('wsu.id_session')
									->from(WebinarSession::model()->tableName().' ws')
									->join(WebinarSessionUser::model()->tableName().' wsu', 'ws.id_session = wsu.id_session')
									->where('ws.course_id = :id_course AND wsu.id_user = :id_user', array(':id_course' => $this->course_id, ':id_user' => Yii::app()->user->id))
									->queryColumn();
			foreach($subscribed_sessions as $id_session)
				$tmp[$this->course_id][$id_session] = $id_session;

			$tmp2[$this->course_id] = CoreUserPuCourse::model()->countByAttributes(array('puser_id' => Yii::app()->user->id, 'course_id' => $this->course_id)) ? true : false;

			Yii::app()->session['PUIsInstructorForCourse'] = $tmp;
			Yii::app()->session['PUIsCourseAssociated'] = $tmp2;

			unset($tmp);
			unset($subscribed_sessions);
		}
		elseif(Yii::app()->user->getIsPu() && !$this->userCanAdminCourse)
		{
			unset(Yii::app()->session['PUIsInstructorForCourse']);
			unset(Yii::app()->session['PUIsCourseAssociated']);
		}

		$this->render('webinarSession', array(
			'courseModel' => $this->courseModel, // set in BaseController
			'session_id' => $sessionId,
			'courseSessionModel' => $courseSession,
            'deepLink'          =>  ($this->courseModel->deep_link)? $this->courseModel->getDeepLink() : '',
		));
	}


	public function processCoachingStatuses() {
		// Double check to ensure we are here on purpose

		if ($this->coachingSessionStatus <> LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_PROCEED) {

			switch ($this->coachingSessionStatus) {
				case LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_SELECT:
					$this->registerSelectCoachingSessionResources();
					$userLevel = $this->userIsSubscribed ? LearningCourseuser::userLevel(Yii::app()->user->id, $this->course_id) : false;
					$learningCourseCoachingSession = LearningCourseCoachingSession::model()->findByAttributes(array('idCourse' => $this->course_id));

					$paramsCoaching = array(
						'course_id'                     => $this->course_id,      // set in BaseController
						'courseModel'                   => $this->courseModel,  // set in BaseController
						'userLevel'                     => ($userLevel),
						'learningCourseCoachingSession' => $learningCourseCoachingSession,
						'coachingSessionStatus'         => $this->coachingSessionStatus
					);

					$adminJsAssetsUrl = $this->getAdminJsAssetsUrl();
					Yii::app()->clientScript->registerScriptFile($adminJsAssetsUrl . '/coaching.js');

					//DatePickerHelper::registerAssets();

					$this->render('coaching_select_session', $paramsCoaching);
					Yii::app()->end();
					break;

				case LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_NEW:
					$this->registerSelectCoachingSessionResources();
					$message = '<i class="fa fa-check fa-2x"></i><br />';
					$message .= Yii::t('course', 'Your coaching session was successfully added');
					$messageClass = 'green-text success text-center center';

					$this->render('index_coaching',
						array(
							'course_id'             => $this->course_id,      // set in BaseController
							'message'               => $message,
							'messageClass'          => $messageClass,
							'coachingSessionStatus' => $this->coachingSessionStatus
						)
					);

					Yii::app()->end();
					break;

				case LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_HAVE_SESSION_NOT_STARTED:
					$this->registerSelectCoachingSessionResources();
					// Get user course coaching sessions
					$userCourseCoachingSessions = LearningCourseCoachingSession::userAssignedSessions(Yii::app()->user->id, true, $this->course_id);

					$message = '<i class="fa fa-clock-o fa-2x"></i><br /><br />';
					$message .= Yii::t('course', '<b>Your coaching session starts on ');

					// Ensure we have the session
					if (count($userCourseCoachingSessions) > 0) {
						$userSession = end($userCourseCoachingSessions);
						// Prepare dates in local date format
						$coachingDateStart = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($userSession->datetime_start));
						$coachingDateEnd = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($userSession->datetime_end));
						$message .= $coachingDateStart;
						$message .= Yii::t('course', ' and ends on ');

						$message .= $coachingDateEnd;
						$message .= '</b><br /><span style="font-size: 14px!important;; font-weight: normal!important;">';
						$message .= Yii::t('coaching', 'You cannot access the course before it. Please come back during your coaching session period');
						$message .= '</span>';
					} else{
						throw new CException('Something went wrong.You are nnot supposed to be here');
						$redirectUrl = Docebo::createLmsUrl('site/index', array('opt' => 'mycourses'));
						$this->redirect($redirectUrl, true);
					}


					$messageClass = 'warning text-center center';
					$actionForm = Docebo::createAbsoluteUrl('site/index', array('opt' => 'mycourses'));

					$this->render('index_coaching',
						array(
							'course_id'             => $this->course_id,      // set in BaseController
							'actionForm'            => $actionForm,
							'message'               => $message,
							'messageClass'          => $messageClass,
							'coachingSessionStatus' => $this->coachingSessionStatus
						)
					);

					Yii::app()->end();
					break;

				case LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_ASK:
					$this->registerSelectCoachingSessionResources();

					$title = Yii::t('course', 'Select your Coach');

					$message = '<br /><br /><b>'. Yii::t('course', 'All coaching sessions are full for this course') . '</b><br /><br />';
					$actionProcessForm = Docebo::createLmsUrl('player/training/userRequestedNewCoachingSession&course_id=' . $this->course_id . '&user_id=' . (Yii::app()->user->id));

					// If Admin should manually assign coaching session to the user
					if ($this->courseModel->learner_assignment == LearningCourse::COACHING_LEARNER_ASSIGNMENT_ADMIN_SELECTED) {
						$message = '<br /><br /><b>'. Yii::t('course', "Admin didn't assigned you a coaching session") .'</b><br /><br />';
						$actionProcessForm = Docebo::createLmsUrl('player/training/userRequestedNewCoachingSession&course_id=' . $this->course_id . '&user_id=' . (Yii::app()->user->id) . '&askadmin=1');
					}

					$message .= '<span style="font-size: 14px!important; font-weight: normal!important;">' . Yii::t('course', 'Please contact your administrator') . '</span>';

					$messageClass = 'warning text-center center';
					$actionForm = Docebo::createAbsoluteUrl('site/index', array('opt' => 'mycourses'));


					$this->render('index_coaching',
						array(
							'course_id'             => $this->course_id,      // set in BaseController
							'actionForm'            => $actionForm,
							'title'                 => $title,
							'message'               => $message,
							'messageClass'          => $messageClass,
							'coachingSessionStatus' => $this->coachingSessionStatus,
							'actionProcessForm'     => $actionProcessForm,
							'hideButton'            => false
						)
					);

					Yii::app()->end();
					break;

				default:
					break;
			}

			$this->render('index_coaching', array());
			Yii::app()->end();
		}
	}

	public function registerSelectCoachingSessionResources() {
		parent::registerResources();


		$cs = Yii::app()->getClientScript();
		$assetsUrl = $this->module->assetsUrl;

		if (!Yii::app()->request->isAjaxRequest) {

			/**
			 * NOTE please, the order and placement (head, end, ready,...) of scripts DO MATTER!!!
			 */

			// Load/Register all JS/CSS
			/* @var CClientScript $cs */

			// Text expander: cuts a string and adds "read more"
			$cs->registerScriptFile($assetsUrl . '/js/jquery.expander.js');

			// Used by TEST edit/create
			$cs->registerCssFile($assetsUrl . '/css/jquery.dataTables.css');

			// Needed in Videoconf dialogs
			DatePickerHelper::registerAssets(Yii::app()->getLanguage(), CClientScript::POS_HEAD);
		}
	}

	/*
	 * actionUserRequestedNewCoachingSession() - display confirmation to user and raise request coaching session events
	 */
	public function actionUserRequestedNewCoachingSession()
	{
		$this->registerSelectCoachingSessionResources();
		$message = '<i class="fa fa-check fa-2x"></i><br />';
		$message .= Yii::t('course', 'Your request has been sent to the admin');
		$messageClass = 'green-text success text-center center';


		$actionForm = Docebo::createLmsUrl('site/index', array('opt' => 'mycourses'));

		$this->render('index_coaching',
			array(
				'course_id'             => $this->course_id,      // set in BaseController
				'message'               => $message,
				'messageClass'          => $messageClass,
				'coachingSessionStatus' => $this->coachingSessionStatus,
				'hideButton'            => true,
				'actionForm'            => $actionForm
			)
		);

		// If there are not already expired user coaching sessions and he/she asks for the first coaching session
		if (empty($_REQUEST['renew'])) {
			// Event raising for user requested a coaching session
			Yii::app()->event->raise('FirstCoachingSessionRequest', new DEvent($this, array(
				'course' => $this->courseModel,
				'idUser' => Yii::app()->user->id
			)));
		} else {
			// User has already expired coaching sessions and he/she wants a new coaching session
			// Event raising for user requested a new coaching session
			Yii::app()->event->raise('AnotherCoachingSessionRequest', new DEvent($this, array(
				'course' => $this->courseModel,
				'idUser' => Yii::app()->user->id
			)));
		}

		Yii::app()->end();
	}

	public function actionAxAddSubtitle() {
		Yii::app()->end();
	}

	public function actionAxSubtitleToggleFallback() {
		$id = Yii::app()->request->getParam('subId', false);
		$idVideo = Yii::app()->request->getParam('idVideo', false);
		$model = false;

		// Get the subtitle model if there is an id of subtitle passed
		if ($id) {
			$model = LearningVideoSubtitles::model()->findByPk($id);
		}

		// If there is an id of a video passed do fallback for all of its subtitles
		if ($idVideo) {
			LearningVideoSubtitles::model()->updateAll(array('fallback'=>0),'id_video=:id_video',array(':id_video'=>$idVideo));
		}

		if ($model) {
			$model->fallback = $model->fallback == 1 ? 0 : 1;
			$model->save();

			$result['success'] = true;
		} else {
			$result['success'] = false;
		}

		$this->sendJSON($result);
	}

	public function actionAxDeleteSubtitle() {
		$idSub  = Yii::app()->request->getParam('id', false);
		$confirm = Yii::app()->request->getParam('confirm_button', false);
		$agreed = Yii::app()->request->getParam('agree_deletion', false);
		$transaction = null;

		try {
			$subModel = false;
			// Check if there is passed id of a subtitle db table
			if (intval($idSub)) {
				// Load subtitle model
				$subModel = LearningVideoSubtitles::model()->findByPk($idSub);
				if (!$subModel)
					throw new Exception('Invalid subtitle');
			}

			if($confirm && $agreed) {
				// If it is a temporary, not yet saved subtitle file to be deleted
				if (preg_match('#[a-z]#i', $idSub)) {
					// Get the actual filepath
					$tmpFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $idSub;

					// Clean up
					FileHelper::removeFile($tmpFilePath);

					// close the dialog
					echo '<a class="auto-close"></a>';
					Yii::app()->end();
				}

				if (Yii::app()->db->getCurrentTransaction() === NULL)
					$transaction = Yii::app()->db->beginTransaction();

				// Delete the asset first
				if ($subModel->asset_id) {
					CoreAsset::model()->deleteByPk($subModel->asset_id);
				}

				// Delete the subtitle
				if(!$subModel->delete())
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));

				if ($transaction)
					$transaction->commit();

				echo '<a class="auto-close"></a>';
				Yii::app()->end();

			} else {
				$this->renderPartial('_delete_subtitle', array(
					'model' => $subModel,
				));
			}
		}
		catch (Exception $e) {
			if ($transaction)
				$transaction->rollback();

			$this->renderPartial('admin.protected.views.common/_dialog2_error', array(
				'message' 	=> $e->getMessage(),
				'type'		=> 'error',
			));
			Yii::app()->end();
		}
	}


	public function actionGetThumbnails(){

		$assets = new CoreAsset();
		$rq = Yii::app()->request;
		$pageId = $rq->getParam('pageId', 0);
		$totalCount = $rq->getParam('totalCount', false);
		$id_object = Yii::app()->request->getParam("id_object", false);

		if(!$id_object){
			$id_object = Yii::app()->request->getParam("loId", false);
		}

		$selectedImageId = $rq->getParam('selectedImageId', null);
		$assets = new CoreAsset();
		$userImages = array();

		$offset = $pageId*10;
		$maxPages =  ceil($totalCount/10) - 1;
		if($offset < 0){
			$offset = $maxPages*10;
			$pageId =  $maxPages;
		}elseif($pageId > $maxPages){
			$offset = 0;
			$pageId = 0;
		}
		if($id_object){
			$learningOrganization = LearningOrganization::model()->findByPk($id_object);
			if($learningOrganization){
				if($learningOrganization->resource && !$selectedImageId){
					$selectedImageId = $learningOrganization->resource;
				}
				$userImages  = $assets->urlList(CoreAsset::TYPE_LO_IMAGE, CoreAsset::SCOPE_PRIVATE, false, CoreAsset::VARIANT_SMALL);
				foreach ($userImages as $key => $value) {
					if ($key == $learningOrganization->resource) {
						$this->reorderArray($userImages, $key, $value);
						break;
					}
				}
				$userImages = array_chunk($userImages, 10, true);
			}
			if(isset($userImages[$pageId])){
				$userImages = $userImages[$pageId];
			}elseif(isset($userImages[$pageId-1])){
				$userImages = $userImages[$pageId-1];
				$pageId = $pageId-1;
			}elseif(isset($userImages[0])){
				$userImages = $userImages[0];
				$pageId = 0;
			}

		}else{
			$learningOrganization = new LearningOrganization();
			$userImages = $assets->urlList(CoreAsset::TYPE_LO_IMAGE, CoreAsset::SCOPE_PRIVATE, false, CoreAsset::VARIANT_SMALL, $offset , 10);

		}

		if(!$userImages){
			$pageId = 0;
			if($pageId < 0){
				$pageId = $maxPages-1;
			}
			$offset = $pageId*10;
			$userImages = $assets->urlList(CoreAsset::TYPE_COURSELOGO, CoreAsset::SCOPE_PRIVATE, false, CoreAsset::VARIANT_SMALL, $offset , 10);
		}

		$html = $this->renderPartial('_carouselThumbnails', array(
			'model' => $learningOrganization,
			'userImages' => $userImages,
			'selectedImageId' => $selectedImageId,

		),true,true);
		$this->sendJSON(array(
			'html' => $html,
			'currentPage' => $pageId,
		));

		Yii::app()->end();

	}

	protected function reorderArray(&$array, $key, $value) {
		unset($array[$key]);
		$array = array($key => $value) + $array;
	}

}
