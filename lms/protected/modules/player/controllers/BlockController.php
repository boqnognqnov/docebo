<?php

class BlockController extends PlayerBaseController {

	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {

		// Merge with parent rules, if any
		return CMap::mergeArray(parent::accessRules(), array(

			array('allow',
				'users' => array('@'),
				'expression' => 'Yii::app()->user->isGodAdmin',
			),

			array('allow',
				'users' => array('@'),
				'actions' => array('axUpdate', 'axUpdateOrder', 'axRemove', 'axLoadBlocksGrid', 'axLoadOneBlock', 'axLoadOneBlockCompletely', 'axLoadCoachingChatUsers', 'axLoadCoachingChatMessage', 'axLoadExpiredCoachingWebinarSessions', 'axChangeWebinarSessionStatus', 'axLoadCoachingWebinarSessionGuests','axCoachingSessionUsersAutocomplete', 'axLoadActiveLiveSessions', 'createNewFolder', 'axGetMessage', 'axDeleteMessage', 'axCheckDeletedCoachingChatMessage', 'axCheckEditedCoachingChatMessage', 'axEditCoachingMessage'),
				'expression' => 'Yii::app()->controller->userCanAdminCourse',
			),

			// Actions allowed to Power Users with this course assigned
			// that have Session view/mod permissions
			array('allow',
				'users' => array('@'),
				'actions' => array('axLoadBlocksGrid', 'axLoadOneBlock', 'axLoadOneBlockCompletely', 'axLoadCoachingChatUsers',
					'axLoadCoachingChatMessage', 'axLoadExpiredCoachingWebinarSessions', 'axChangeWebinarSessionStatus',
					'axLoadCoachingWebinarSessionGuests','axCoachingSessionUsersAutocomplete', 'axLoadActiveLiveSessions', 'axGetMessage', 'axDeleteMessage', 'axCheckDeletedCoachingChatMessage', 'axCheckEditedCoachingChatMessage', 'axEditCoachingMessage'),
				'expression' => "Yii::app()->user->getIsPu() && (Yii::app()->user->checkAccess('/lms/admin/classroomsessions/view') || Yii::app()->user->checkAccess('/lms/admin/classroomsessions/add')
				|| Yii::app()->user->checkAccess('/lms/admin/classroomsessions/mod'))",
			),

			array('allow',
				'users' => array('@'),
				'actions' => array('index', 'axLoadBlocksGrid', 'axLoadOneBlock', 'axLoadOneBlockCompletely', 'axManageFileLink', 'axManageContentRavenLink',
					'axGetForumDiscussionsHtml', 'axSaveCoursedoc', 'axDeleteCoursedoc', 'axUpdateCoursedocsOrder',
					'axUpdateCoursedoc', 'axCourseDocsSettings', 'axLoadCoachingWebinarSessionGuests', 'axLoadCoachingChatUsers', 'axLoadCoachingChatMessage',
					'axLoadExpiredCoachingWebinarSessions', 'axChangeWebinarSessionStatus','axCreateCoachingWebinarSession','axDeleteCoachingWebinarSession',
					'axCoachingSessionUsersAutocomplete', 'axLoadActiveLiveSessions', 'createNewFolder', 'axGetMessage', 'axDeleteMessage', 'axCheckDeletedCoachingChatMessage', 'axCheckEditedCoachingChatMessage', 'axEditCoachingMessage'),
					'expression' => 'LearningCourseuser::isSubscribed($user->id, ' . $this->course_id . ') || Yii::app()->controller->userCanAdminCourse',

			),

			array('allow',
					'users' => array('@'),
					'actions' => array('downloadCoursedoc', 'axLoadBlocksGrid', 'axLoadOneBlock'),
					'expression' => 'LearningCourseuser::isSubscribed($user->id, ' . $this->course_id . ') || Yii::app()->controller->userCanAdminCourse || Yii::app()->user->checkAccess("/lms/admin/course/view")',
			),

			array('allow',
				'users'=>array('@'),
				'actions' => array('downloadCertificate'),
				'expression' => 'LearningCourseuser::isSubscribed($user->id, ' . $this->course_id . ')',
			),

			array('deny', // if some permission is wrong -> the script goes here
					'users' => array('*'),
			'deniedCallback' => array($this , 'accessDeniedCallback'),
			'message' => Yii::t('course', '_NOENTER'),
			),
		));

	}


	/**
	 * Entry point. Unconditionally redirects to Training resources
	 *
	 */
	public function actionIndex() {
		$this->redirect($this->createUrl('training/index', array('course_id' => $this->course_id)), true);
	}


	/**
	 * AJAX called action to build the whole blocks grid
	 *
	 * Accepts course ID, loads its blocks and send back a JSON response
	 */
	public function actionAxLoadBlocksGrid() {

		$course_id = Yii::app()->request->getParam("course_id", false);                

		//some block types may be not showable in certain contexts
		// TODO: think at a better implementation for this, instead of having it hardcoded here
		$allowedBlockTypes = array(
			PlayerBaseblock::TYPE_COMMENTS,
			PlayerBaseblock::TYPE_COURSEDOCS,
			PlayerBaseblock::TYPE_COURSE_INFO,
			PlayerBaseblock::TYPE_COURSETEACHER,
			PlayerBaseblock::TYPE_FORUM,
			PlayerBaseblock::TYPE_NEWSBLOG,
			PlayerBaseblock::TYPE_VIDEOCONF,
			PlayerBaseblock::TYPE_COURSE_DESCRIPTION, 
		    PlayerBaseblock::TYPE_IFRAME,

			//$ulevel = LearningCourseuser::userLevel(Yii::app()->user->id, $this->course_id);
			//if ($ulevel < LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR)
			PlayerBaseblock::TYPE_MY_INSTRUCTOR
		);

		$learningCourse = LearningCourse::model()->findByPk($course_id);

		if($learningCourse->enable_coaching && PluginManager::isPluginActive('CoachingApp')){
			$allowedBlockTypes[] = PlayerBaseblock::TYPE_MY_COACH;
		}

		if(PluginManager::isPluginActive('CurriculaApp')){
			$allowedBlockTypes[] = PlayerBaseblock::TYPE_LP_NAVIGATION;
		}

		if (PluginManager::isPluginActive('MyBlogApp')) {
			$allowedBlockTypes[] = PlayerBaseblock::TYPE_BLOG;
		}

		if (PluginManager::isPluginActive('Share7020App') && (Yii::app()->player->course->course_type != LearningCourse::TYPE_CLASSROOM) && (Yii::app()->player->course->course_type != LearningCourse::TYPE_WEBINAR)) {
			$allowedBlockTypes[] = PlayerBaseblock::TYPE_QUESTIONS;
		}

		//classroom check
		if (PluginManager::isPluginActive('ClassroomApp') && (Yii::app()->player->course->course_type == LearningCourse::TYPE_CLASSROOM)) {
			$idSession = Yii::app()->request->getParam("id_session", false);

			//blocks specific for classroom courses (and some does appear only if specific conditions are satisfied)
			//if (!empty($idSession)) {
				$allowedBlockTypes[] = PlayerBaseblock::TYPE_SESSION_INFO;
			//}
		}

		$response = new AjaxResult();

		// Validate Course ID
		if (!$course_id) {
			$response->setStatus(false)->setMessage(Yii::t('standard', '_OPERATION_FAILURE'))->toJSON();
			Yii::app()->end();
		}

		// Get all blocks for THIS course and THIS user
		$criteria = new CDbCriteria();

		// Course
		$criteria->addCondition("course_id=:course_id");
		$criteria->params[":course_id"] = $course_id;

		//block type filter
		$criteria->addInCondition('content_type', $allowedBlockTypes);

		$criteria->order = "position ASC, id ASC";
		$blocks = PlayerBaseblock::model()->findAll($criteria);

		$idSession = !empty($idSession) ? $idSession : false;

		// Render the grid
		$html = $this->renderPartial('_blocks_grid', array(
			'blocks' => $blocks,
			'idSession' => $idSession
		), true);

		$response->setStatus(true)->setHtml($html)->toJSON();
		Yii::app()->end();
	}


	/**
	 * Show dialog to add new block or edit old one
	 *
	 */
	public function actionAxUpdate() {
		$course_id = $this->course_id; // Set in Base controller
		$block_id = Yii::app()->request->getParam("block_id", false);

		// Any incoming block id or not?
		if (!$block_id) {
			$model = new PlayerBaseblock();
			$model->position = 9999;
			$model->course_id = $course_id;
		} else {
			$model = PlayerBaseblock::model()->find("id=:id", array(":id" => $block_id));
		}

		if (isset($_POST['PlayerBaseblock'])) {
			// This is important: First set content type, then attach behavior, then apply $model->attributes = xxxx
			$model->content_type = $_POST['PlayerBaseblock']['content_type'];
			$model->attachContentBehavior();
			$model->attributes = $_POST['PlayerBaseblock'];
			if ($model->canNormalUserUploadFiles == 1) {
				$postParams = array(
					'canNormalUserUploadFiles' => $_POST['PlayerBaseblock']['canNormalUserUploadFiles'],
					'fileLimit' => $_POST['PlayerBaseblock']['fileLimit'],
				);
				$model->scenario = 'WidgetParamsSaved';
				$model->params = json_encode($postParams);
			}
			if($_POST['PlayerBaseblock']['iframeUrl'] && $_POST['PlayerBaseblock']['oauthClient'] && $_POST['PlayerBaseblock']['secretSalt']) {
				$postParams = array(
					'iframeUrl' => $_POST['PlayerBaseblock']['iframeUrl'],
					'iframeHeight' => $_POST['PlayerBaseblock']['iframeHeight'],
					'oauthClient' => $_POST['PlayerBaseblock']['oauthClient'],
					'secretSalt' => $_POST['PlayerBaseblock']['secretSalt']
				);
				$model->scenario = 'WidgetParamsSaved';
				$model->params = json_encode($postParams);
			}
			if ($model->save()) {
				// Send "auto close" markup; Dialog2 knows what to do with this
				echo '<a class="auto-close success" data-block-id="' . $block_id . '"></a>';
				Yii::app()->end();
			}
		} else {
			$model->attachContentBehavior();
		}

		//retrieve all types
		$blockTypes = PlayerBaseblock::getTypes($model->id);

		//render dialog
		$this->renderPartial("_form", array(
			'model' => $model,
			'blockTypes' => $blockTypes,
		));

		Yii::app()->end();
	}


	/**
	 * Remove a block from the course layout
	 *
	 */
	public function actionAxRemove() {
		$block_id = (int)Yii::app()->request->getParam("block_id", false);

		$errorMessage = "";

		if (!$block_id) {
			$errorMessage = Yii::t("standard", "_OPERATION_FAILURE");
		} else {
			$nDeleted = PlayerBaseblock::model()->deleteByPk($block_id);
			if ($nDeleted <= 0) {
				$errorMessage = Yii::t("standard", "_OPERATION_FAILURE");
			} else {
				echo '<a class="auto-close success"></a>';
				Yii::app()->end();
			}
		}

		echo "<h1>Error</h1>$errorMessage";
		Yii::app()->end();
	}


	/**
	 * AJAX called to update blocks ordering (position) inside the course/user
	 *
	 */
	public function actionAxUpdateOrder() {
		$course_id = Yii::app()->request->getParam("course_id", false);
		$sorted = Yii::app()->request->getParam("sorted", false); // ARRAY!

		// Enumerate all blocks (sorted) and update DB table
		foreach($sorted as $key => $element) {
			$tmp = preg_split("/_/", $element);
			$position = $key;
			$blockId = $tmp[1];
			$condition = "course_id=:course_id AND id=:id";
			$params = array(":course_id" => $course_id, ":id" => $blockId);
			PlayerBaseblock::model()->updateAll(array("position" => $position), $condition, $params);
		}

		$response = array(
			"success" => true,
			"html" => "",
			"message" => "",
		);

		$response = new AjaxResult();
		$response->setStatus(true)->setHtml("")->setMessage("")->toJSON();
		Yii::app()->end();
	}


	/**
	 * Loads the content of a given block (according to its type, using behaviours) and
	 * returns HTML to be shown
	 *
	 */
	public function actionAxLoadOneBlock() {

		$course_id = $this->course_id; // from base controller
		$block_id = (int)Yii::app()->request->getParam("block_id", false);
		$options = Yii::app()->request->getParam("options", array());

		$blockModel = PlayerBaseblock::model()->findByPk($block_id);
		$courseModel = $this->courseModel;

		// Try to attach Content Type behavior. Content type is taken from the model itself!
		$attached = $blockModel->attachContentBehavior();

		$html = $this->renderPartial('widget_content', array(
			'blockModel' => $blockModel,
			'attached' => $attached,
			'options' => $options
		), true, true);

		if (isset($_GET['htmlOnly'])) {
			echo $html;
		} else {
			$response = new AjaxResult();
			$title = '';  // Not used at the moment by the caller, but leave it here please.
			$response->setStatus(true)->setData(array('title' => $title))->setHtml($html)->toJSON();
		}
		Yii::app()->end();
	}

	public function actionAxLoadOneBlockCompletely() {

		$block_id = (int)Yii::app()->request->getParam("block_id", false);
		$blockModel = PlayerBaseblock::model()->findByPk($block_id);

		// Try to attach Content Type behavior. Content type is taken from the model itself!
		$attached = $blockModel->attachContentBehavior();

		// Ok ?
		if ($attached) {
			$html = $this->renderPartial('_single_block', array('blockModel' => $blockModel), true);
		} // .. Or maybe we don't have it defined yet??
		else {
			$html = Yii::t("standard", "_OPERATION_FAILURE") . $blockModel->content_type;
		}

		if (isset($_GET['htmlOnly'])) {
			echo $html;
		} else {
			$response = new AjaxResult();
			$title = '';  // Not used at the moment by the caller, but leave it here please.
			$response->setStatus(true)->setData(array('title' => $title))->setHtml($html)->toJSON();
		}
		Yii::app()->end();
	}


	/**
	 *
	 */
	public function actionAxGetForumDiscussionsHtml() {

		$response = new AjaxResult();

		// We use Block Id to detect the forum and other info
		$block_id = (int)Yii::app()->request->getParam("block_id", 0);

		// Get block model and attach it's behaviour
		$blockModel = PlayerBaseblock::model()->findByPk($block_id);

		// Here we are interested in "comments" block only;
		// So check the type and get out if it is not (should NOT happen though!)
		if ($blockModel->content_type != PlayerBaseblock::TYPE_COMMENTS) {
			$response->setStatus(false)->setMessage('This is not a comments block!')->toJSON();
			Yii::app()->end();
		}
		$attached = $blockModel->attachContentBehavior();

		$options = array(
			'discussions_offset' => (int)Yii::app()->request->getParam("doffset", 0),
			'discussions_limit' => (int)Yii::app()->request->getParam("dlimit", Yii::app()->params["player"]["comments_block_discussions"]),
			'comments_offset' => (int)Yii::app()->request->getParam("coffset", 0),
			'comments_limit' => (int)Yii::app()->request->getParam("climit", Yii::app()->params["player"]["comments_block_comments"]),
			'thread_id' => (int)Yii::app()->request->getParam("thread_id", false),
			'show_viewall' => Yii::app()->request->getParam("show_viewall", true),
			'show_all_comments' => Yii::app()->request->getParam("show_all_comments", false),
			'show_create_discussion' => Yii::app()->request->getParam("show_create_discussion", true),
			'show_load_more' => Yii::app()->request->getParam("show_load_more", true),
			'init_blocks_js' => Yii::app()->request->getParam("init_blocks_js", true),
		);


		// Get total forum discussions
		$criteria = new CDbCriteria();
		$criteria->addCondition("idForum=:forum_id");
		$criteria->params = array(":forum_id" => $blockModel->forum_id);
		$countTotalDiscussions = LearningForumthread::model()->count($criteria);


		$html = $blockModel->getContent($options);

		$data = array("html" => $html, "countTotalDiscussions" => $countTotalDiscussions);

		$response = new AjaxResult();
		$response->setStatus(true)->setData($data)->toJSON();
		Yii::app()->end();
	}



	/**
	 * Does the real phisical saving of the Course document and returns the name of the file
	 *
	 * @param object $fileModel
	 * @param array $file
	 * @throws CException
	 * @return string
	 */
	protected function updateCoursedocFile($fileModel, $file) {

		//we need $file to be a regular file descriptor
		if (!is_array($file) || !isset($file['name'])) return false;

		// Check if uploaded file exists
		$uploadTmpFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $file['name'];
		if (!is_file($uploadTmpFile)) {
			throw new CException( Yii::t('error', 'File "{name}" does not exist.', array('{name}'=> $file['name'])) );
		}

		// Rename uploaded file
		$extension = strtolower(CFileHelper::getExtension($file['name']));
		$newFileName = uniqid() . '_' . $file['name'];
		$newFileName = md5($newFileName) . '.' . $extension;
		$newTmpFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $newFileName;
		if (!@rename($uploadTmpFile, $newTmpFile)) {
			throw new CException( Yii::t('error', 'Error renaming uploaded file.') );
		}

		// Move uploaded file to final storage for later usage
		$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_COURSEDOCS);
		if (!$storageManager->store($newTmpFile)) {
			throw new CException( Yii::t('error', 'Error while saving file to final storage.') );
		}

		// Delete tmp file
		FileHelper::removeFile($newTmpFile);

		return $newFileName;

	}


	/**
	 * Upload new or update old course document
	 */
	public function actionAxUpdateCoursedoc() {

		$courseId = Yii::app()->request->getParam("course_id", null);
		$fileId = Yii::app()->request->getParam("file_id", null);
		$blockId = Yii::app()->request->getParam("block_id", null);
		$listOfFolders = $this->getListOfFolders($courseId);
		$root = LearningCourseFile::model()->findByPk(Yii::app()->request->getParam('parentFolderId',null));
		$file = Yii::app()->request->getParam("file", null); // uploaded file

		// Only passed if the dialog is opened from inside "Learner View"
		$idSession = Yii::app()->getRequest()->getParam('session_id');

		// Use the physically uploaded filename, put by PLUpload in the "target_name" element
		// This is tru when "unique_names" option is used for plupload JavaScript object
		if (is_array($file) && isset($file['name'])) {
			$originalFileName = urldecode($file['name']);
			if (isset($file['target_name'])) {
				$file['name'] = $file['target_name'];
			}
		}

		$transaction = Yii::app()->db->beginTransaction();
		try {

			if (!$courseId && !$fileId) {
				throw new CException( Yii::t('error', 'Course or file id is required') );
			}

			$fileModel = LearningCourseFile::model()->findByPk($fileId);

			if (!$fileModel) {
				$fileModel = new LearningCourseFile();
				$fileModel->id_course = $courseId;
				$fileModel->idUser = Yii::app()->user->id;
				$fileModel->idParent = Yii::app()->request->getParam('parentFolderId');
				$fileModel->item_type = $fileModel::FILE;
				$fileModel->file_source = LearningCourseFile::FILE_SOURCE_UPLOAD;
			}else{
				$courseId = $fileModel->id_course;
			}

			$courseModel = LearningCourse::model()->findByPk($courseId);

			if ($_REQUEST['LearningCourseFile'] && !empty($_POST['confirm_save'])) {

				$fileModel->attributes = $_REQUEST['LearningCourseFile'];
				$fileModel->title = Yii::app()->htmlpurifier->purify($fileModel->title);
				$modelValidated = $fileModel->validate();
				if ($modelValidated) {

					$ok = true;
					if ($fileModel->isNewRecord) {
						if (!$file) {
							$fileModel->addError('', Yii::t('error', 'Select a file to upload!'));
							$ok = false;
						}
						else {
							$fileName = $this->updateCoursedocFile($fileModel, $file);
							if (!$fileName) {
								$fileModel->addError('', Yii::t('standard', '_OPERATION_FAILURE'));
								$ok = false;
							}
							else {
								$ext = pathinfo($fileName, PATHINFO_EXTENSION);
								if (!in_array($ext, FileTypeValidator::getWhiteListArray(FileTypeValidator::TYPE_ITEM))) {
									$fileModel->addError('', Yii::t('standard', 'File type/extension not allowed'));
									$ok = false;
								}
								else {
									$fileModel->path = $fileName;
									$fileModel->original_filename = $originalFileName;
									$fileModel->created = Yii::app()->localtime->toLocalDateTime();
								}
							}
						}
					}
					else {
						if ($file) {
							$fileName = $this->updateCoursedocFile($fileModel, $file);
							if (!$fileName) {
								$fileModel->addError('', Yii::t('standard', '_OPERATION_FAILURE'));
								$ok = false;
							}
							else {
								$fileModel->path = $fileName;
								$fileModel->original_filename = $originalFileName;
							}
						}
					}


					if ($ok) {
						$fileModel->file_permission = LearningCourseFile::setPermissions($fileModel->permissionLevels, $fileModel->permissionType);

						if ($fileModel->isNewRecord) {
							$lastPosition = LearningCourseFile::model()->findByAttributes(array('id_course' => $fileModel->id_course), array('order' => 'position DESC'));
							$fileModel->position = ($lastPosition) ? intval($lastPosition->position) + 1 : 0;

							$fileModel->appendTo($root);
						} else {
							$fileModel->idParent = $root->id_file;
							$fileModel->moveAsFirst($root);
							$fileModel->saveNode(false);
						}

						// Update file visibility (allowed to be viewed in specific sessions
						// only or in all sessions in the course)
						$sessionsInCourse = array();
						switch($courseModel->course_type){
							case LearningCourse::TYPE_CLASSROOM:
								$sessionsInCourse = Yii::app()->getDb()->createCommand()
									->select('id_session')
									->from('lt_course_session')
									->where('course_id=:idCourse', array(':idCourse'=>$courseId))
									->queryColumn();
								break;
							case LearningCourse::TYPE_WEBINAR:
								$sessionsInCourse = Yii::app()->getDb()->createCommand()
									->select('id_session')
									->from('webinar_session')
									->where('course_id=:idCourse', array(':idCourse'=>$courseId))
									->queryColumn();
								break;
						}

						// Delete old visibilities first
						LearningCourseFileVisibility::model()->deleteAllByAttributes(array(
							'id_session'=>$sessionsInCourse,
							'id_file'=>$fileId,
						));

						switch($fileModel->viewable_by){
							case LearningCourseFile::VIEWABLE_IN_ANY_SESSION:
								// Nothing to do here
								break;
							case LearningCourseFile::VIEWABLE_IN_SELECTED_SESSIONS_ONLY:
								$toInsert = array();
								$selectedItems = array_filter(explode(',', $_POST['selectedItemsString']));
								foreach ($selectedItems as $idSessions) {
									$toInsert[] = array(
											'id_session' => $idSessions,
											'id_file' => $fileModel->id_file,
											'session_type' => $courseModel->course_type,
									);
								}

								if(!empty($toInsert)) {
									Yii::app()->getDb()->getCommandBuilder()
										->createMultipleInsertCommand('learning_course_file_visibility', $toInsert)
										->execute();
								}
								break;
						}
						$transaction->commit();
						$this->reloadWidgetAtCurrentFolder(true);
					}
				}
			}

			if (isset($_POST['confirmSaveFolder']) && !empty($_POST['confirmSaveFolder'])) {

				$fileModel->idParent = $root->id_file;
				$fileModel->title = Yii::app()->htmlpurifier->purify($_POST['folderTitle']);

				if($fileModel->validate('title')) {
					$fileModel->moveAsFirst($root);
					$fileModel->saveNode(false);
					// DB Commit
					$transaction->commit();
					$this->reloadWidgetAtCurrentFolder();
				}
			}

			if(!empty($_POST['confirm_save']) && (isset($modelValidated) && $modelValidated) && empty($fileModel->getErrors())){
				$this->reloadWidgetAtCurrentFolder();
			}
			if($fileModel->item_type==$fileModel::FOLDER){

				// folder edit
				$this->renderPartial("coursedocs/_update_course_folder", array(
					'fileModel' => $fileModel,
					'blockId' => $blockId,
					'listOfFolders' => $listOfFolders,
				), false, true);
			}else {
				// file edit

				if($idSession) {
					// We are in learner view. Some additional CSS is needed
					$cs = Yii::app()->getClientScript();
					$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/admin.css');
				}
				$selectedSessionIds = array();
				if ($fileModel->viewable_by == LearningCourseFile::VIEWABLE_IN_SELECTED_SESSIONS_ONLY) {
					$selectedSessionIds = Yii::app()->db->createCommand()
							->select('id_session')
							->from(LearningCourseFileVisibility::model()->tableName())
							->where('id_file=:id', array(':id' => $fileModel->id_file))->queryColumn();
				}

				if($idSession !== null)
					$selectedSessionIds[] = $idSession;

				if(!empty($_POST['selectedItemsString'])) {
					$selectedItemsFromPost = array_filter(explode(',', $_POST['selectedItemsString']));
					foreach ($selectedItemsFromPost as $id) {
						$selectedSessionIds[] = $id;
					}
					$selectedSessionIds = array_unique($selectedSessionIds);
				}

				// Permissions set
				if ($fileModel->isNewRecord) {
					$blockModel = PlayerBaseblock::model()->findByPk($blockId);
					// Check if can't find such widget
					if ($blockModel === null) {
						echo Yii::t('standard', '_OPERATION_FAILURE');
						Yii::app()->end();
					}

					//Load the parametters from the model
					$params = json_decode($blockModel->params, true);
					$fileModel->permissionType = (isset($params['permissionType'])) ? $params['permissionType'] : 0;
					$fileModel->permissionLevels = (isset($params['permissionLevels']) ? $params['permissionLevels'] : array());
				}
				else {
					$fileModel->permissionType = (LearningCourseFile::checkPermission($fileModel->file_permission, LearningCourseFile::FILE_PERMISSION_SELECTIBLE)) ? 1 : 0;
					$fileModel->permissionLevels = LearningCourseFile::getSelectedPermission($fileModel->file_permission);
				}
				// Permissions set

				$this->renderPartial("coursedocs/_update_coursedoc", array(
						'fileModel' => $fileModel,
						'blockId' => $blockId,
						'listOfFolders' => $listOfFolders,
						'idCourse' => $courseId,
						'idSession' => $idSession,
						'courseModel' => $courseModel,
						'selectedSessionIds' => $selectedSessionIds,
				), false, true);
			}

			// DB Commit
			if($transaction->getActive())
				$transaction->commit();
			Yii::app()->end();

		} catch (Exception $e) {
				$transaction->rollback();
				$html = '<div class="alert">'. $e->getMessage() .'</div>';
				echo $html;
			Yii::app()->end();
		}
	}

	public function actionAxManageFileLink() {
		$courseId = Yii::app()->request->getParam("course_id", null);
		$fileId = Yii::app()->request->getParam("file_id", null);
		$blockId = Yii::app()->request->getParam("block_id", null);
		$listOfFolders = $this->getListOfFolders($courseId);
		$root = LearningCourseFile::model()->findByPk(Yii::app()->request->getParam('parentFolderId',null));

		$transaction = Yii::app()->db->beginTransaction();
		try {
			if (!$courseId && !$fileId) {
				throw new CException( Yii::t('error', 'Course or file id is required') );
			}

			$fileModel = LearningCourseFile::model()->findByPk($fileId);

			if (!$fileModel) {
				$fileModel = new LearningCourseFile("file_link");
				$fileModel->id_course = $courseId;
				$fileModel->idUser = Yii::app()->user->id;
				$fileModel->idParent = Yii::app()->request->getParam('parentFolderId');
				$fileModel->item_type = $fileModel::FILE;
				$fileModel->file_source = LearningCourseFile::FILE_SOURCE_LINK;
			} else {
				$fileModel->setScenario("file_link");
				$fileModel->file_link = $fileModel->original_filename;
			}

			if ($_REQUEST['LearningCourseFile']) {

				$fileModel->attributes = $_REQUEST['LearningCourseFile'];
				$fileModel->title = Yii::app()->htmlpurifier->purify($fileModel->title);

				if ($fileModel->validate()) {

					$fileUrl = $fileModel->file_link;
					if(strpos($fileUrl, 'http') !== 0){
						$fileUrl = 'http://' . $fileUrl;
					}
					$fileModel->path = $fileUrl;
					$fileModel->original_filename = $fileUrl;

					if ($fileModel->isNewRecord) {
						$fileModel->created = Yii::app()->localtime->toLocalDateTime();
						$lastPosition = LearningCourseFile::model()->findByAttributes(array('id_course' => $fileModel->id_course), array('order' => 'position DESC'));
						$fileModel->position = ($lastPosition) ? intval($lastPosition->position) + 1 : 0;

						$fileModel->appendTo($root);
					} else {
						$fileModel->idParent = $root->id_file;
						$fileModel->moveAsFirst($root);
						$fileModel->saveNode(false);
					}

				}
			}

			if (isset($_POST['confirm_save']) && !empty($_POST['confirm_save'])) {
				if( $fileModel->validate()) {
					$fileModel->idParent = $root->id_file;
					$fileModel->moveAsFirst($root);
					$fileModel->saveNode(false);
					// DB Commit
					$transaction->commit();
					$this->reloadWidgetAtCurrentFolder();
					Yii::app()->end();
				}
			}

			// file edit
			$html = $this->renderPartial("coursedocs/_manage_file_link", array(
				'fileModel' => $fileModel,
				'blockId' => $blockId,
				'listOfFolders' => $listOfFolders,
			), true, true);

			echo $html;

			// DB Commit
			$transaction->commit();
			Yii::app()->end();

		} catch (Exception $e) {
			$transaction->rollback();
			$html = '<div class="alert">'. $e->getMessage() .'</div>';
			echo $html;
			Yii::app()->end();
		}
	}

	public function actionAxManageContentRavenLink() {
		$courseId = Yii::app()->request->getParam("course_id", null);
		$fileId = Yii::app()->request->getParam("file_id", null);
		$blockId = Yii::app()->request->getParam("block_id", null);
		$listOfFolders = $this->getListOfFolders($courseId);
		$root = LearningCourseFile::model()->findByPk(Yii::app()->request->getParam('parentFolderId',null));

		$transaction = Yii::app()->db->beginTransaction();
		try {
			if (!$courseId && !$fileId) {
				throw new CException( Yii::t('error', 'Course or file id is required') );
			}

			$fileModel = LearningCourseFile::model()->findByPk($fileId);

			if (!$fileModel) {
				$fileModel = new LearningCourseFile("content_raven_link");
				$fileModel->id_course = $courseId;
				$fileModel->idUser = Yii::app()->user->id;
				$fileModel->idParent = Yii::app()->request->getParam('parentFolderId');
				$fileModel->item_type = $fileModel::FILE;
				$fileModel->file_source = LearningCourseFile::FILE_SOURCE_CONTENT_RAVEN;
			} else {
				$fileModel->setScenario("content_raven_link");
				$settings = (array) CJSON::decode($fileModel->settings, 1);
				$fileModel->content_id = isset($settings['content_id']) ? $settings['content_id'] : "";
				$fileModel->permission_id = isset($settings['permission_id']) ? $settings['permission_id'] : "";
			}

			if ($_REQUEST['LearningCourseFile']) {

				$fileModel->attributes = $_REQUEST['LearningCourseFile'];
				$fileModel->title = Yii::app()->htmlpurifier->purify($fileModel->title);
				$fileModel->content_id = Yii::app()->htmlpurifier->purify($fileModel->content_id);
				$fileModel->permission_id = Yii::app()->htmlpurifier->purify($fileModel->permission_id);

				if ($fileModel->validate()) {

					$settings = array(
						'content_id' => $fileModel->content_id,
						'permission_id' => $fileModel->permission_id
					);
					$fileModel->settings = CJSON::encode($settings);

					if ($fileModel->isNewRecord) {
						$fileModel->created = Yii::app()->localtime->toLocalDateTime();
						$lastPosition = LearningCourseFile::model()->findByAttributes(array('id_course' => $fileModel->id_course), array('order' => 'position DESC'));
						$fileModel->position = ($lastPosition) ? intval($lastPosition->position) + 1 : 0;

						$fileModel->appendTo($root);
					} else {
						$fileModel->idParent = $root->id_file;
						$fileModel->moveAsFirst($root);
						$fileModel->saveNode(false);
					}

				}
			}

			if (isset($_POST['confirm_save']) && !empty($_POST['confirm_save'])) {
				if( $fileModel->validate()) {
					$fileModel->idParent = $root->id_file;
					$fileModel->moveAsFirst($root);
					$fileModel->saveNode(false);
					// DB Commit
					$transaction->commit();
					$this->reloadWidgetAtCurrentFolder();
					Yii::app()->end();
				}
			}

			// file edit
			$html = $this->renderPartial("coursedocs/_manage_content_raven_link", array(
				'fileModel' => $fileModel,
				'blockId' => $blockId,
				'listOfFolders' => $listOfFolders,
			), true, true);

			echo $html;

			// DB Commit
			$transaction->commit();
			Yii::app()->end();

		} catch (Exception $e) {
			$transaction->rollback();
			$html = '<div class="alert">'. $e->getMessage() .'</div>';
			echo $html;
			Yii::app()->end();
		}
	}
	/**
	 * Deletes a course document along with it's associated content
	 */
	public function actionAxDeleteCoursedoc() {
		$idFile = Yii::app()->request->getParam('id_file');
		$blockId = Yii::app()->request->getParam('block_id');
		$confirmDelete = Yii::app()->request->getParam('confirm_delete', false);
		$confirmCheckBox = Yii::app()->request->getParam('confirm', null);
		$flagForDelete = Yii::app()->request->getParam('totalDelete', 0);
		$listOfFolders = $this->getListOfFolders($this->course_id);
		$itemForDelete = LearningCourseFile::model()->findByPk($idFile);
		if (!$itemForDelete) {
			echo Yii::t('standard', '_OPERATION_FAILURE');
			Yii::app()->end();
		}

		// Is this a post request
		$isDeleteComplete = false;
		if (Yii::app()->request->isPostRequest && $confirmDelete) {
			try {
				$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_COURSEDOCS);
				if ($confirmCheckBox == 1 || Yii::app()->request->getParam('confirm_delete') == 'Confirm') {
					if ($flagForDelete == 1) {

						// full deletion
						$itemForDelete->deleteNode();
					} elseif ($flagForDelete == 2) {

						// delete folder and move it's content
						$targetFolderId = Yii::app()->request->getParam('newTargetFolder', null);
						if (!$targetFolderId) {
							throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
						}
						$targetFolderModel = LearningCourseFile::model()->findByPk($targetFolderId);
						$childs = $itemForDelete->children()->findAll();
						foreach($childs as $child){
							$child->idParent = $targetFolderId;
							$child->moveAsFirst($targetFolderModel);
							$child->saveNode(false);
						}
						$itemForDelete->deleteNode();
					} elseif ($flagForDelete === 0) {
						$itemForDelete->deleteNode();
					}else{
						throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
					}
					$this->reloadWidgetAtCurrentFolder();
				} else {
					if (!$storageManager->remove($itemForDelete->path)) {
						throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
					}
					if (!$itemForDelete->deleteNode()) {
						throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
					}
				}

				$isDeleteComplete = true;

			} catch (CException $e) {

				echo $e->getMessage();
				Yii::app()->end();
			}
		}

		$this->renderPartial('player.views.block.coursedocs._delete_coursedoc', array(
			'coursedoc' => $itemForDelete,
			'isDeleteComplete' => $isDeleteComplete,
			'blockId' => $blockId,
			'listOfFolders' => $listOfFolders
		));
	}


	/**
	 * AJAX called to update course docs ordering (position) inside the coursedocs block widget
	 *
	 */
	public function actionAxUpdateCoursedocsOrder() {
		$course_id = $this->course_id; // Set in Base controller

		$response = new AjaxResult();

		try {
			if (!empty($_POST['sorted'])) {
				$arrSortedIds = $_POST['sorted'];
				// we're displaying the latest coursedocs first (descending ORDER by position
				// so we need to reverse the order of the 'sorted' array
				$arrSortedIds = array_values($arrSortedIds);
				$arrSortedIds = array_filter($arrSortedIds);

				// retrieve all coursedocs for this block from the db
				$coursedocs = LearningCourseFile::model()->findAllByAttributes(array('id_course' => $course_id), array('order' => 'position DESC'));
				$arrCoursedocsById = array();
				if (!empty($coursedocs)) {
					// group coursedocs by id
					foreach($coursedocs as $coursedoc) {
						$arrCoursedocsById[$coursedoc->id_file] = $coursedoc;
					}
					// then set the new position for each one
					foreach($arrSortedIds as $position => $id) {
						$coursedoc = $arrCoursedocsById[$id];
						$coursedoc->position = $position;
						$coursedoc->saveNode();
					}
				}
			}
		} catch (CException $ex) {
			$response->setStatus(false)->setMessage($ex->getMessage())->toJSON();
			Yii::app()->end();
		}

		$response->setStatus(true)->toJSON();
		Yii::app()->end();
	}


	public function actionDownloadCoursedoc() {
		$courseId = Yii::app()->request->getParam('course_id');
		$fileId = Yii::app()->request->getParam('id_file');

		$coursedoc = LearningCourseFile::model()->findByAttributes(array(
			'id_course' => $courseId,
			'id_file' => $fileId
		));

		if (!$coursedoc) {
			throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
		}

		$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_COURSEDOCS);
		$coursedoc->original_filename = Sanitize::fileName($coursedoc->original_filename,true);

        //it is a new Learning Course File download
        Yii::app()->event->raise('CourseFileDownloaded', new DEvent($this, array(
            'courseFile' => $coursedoc
        )));

		$storageManager->sendFile($coursedoc->path, $coursedoc->original_filename);
	}


	/**
	 * Get Content type specific Settings HTML for Update form.
	 * Called from inside main Update View (_form)
	 *
	 * @param string $content_type
	 * @param object $form CActiveForm
	 * @return string
	 */
	public function renderContentTypeParameters($blockModel, $content_type, $form) {

		// Check if view exists
		$viewFile = realpath(Yii::getPathOfAlias("player.views.block.${content_type}")) . '/_form.php';
		if (is_file($viewFile)) {
			$html = $this->renderPartial('player.views.block.' . $content_type . '._form', array(
				'blockModel' => $blockModel,
				'form' => $form,
			), true);
		} else {
			$html = "";

		}

		return $html;
	}


	/**
	 * (non-PHPdoc)
	 * @see PlayerBaseController::registerResources()
	 */
	public function registerResources() {
		parent::registerResources();

		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->getModule('curricula')->getAssetsUrl() . "/css/main.css");
		$cs->registerScriptFile(Yii::app()->getModule('curricula')->getAssetsUrl() . "/js/main.js");

		if (!Yii::app()->request->isAjaxRequest) {
			// ---
		}

	}

	public function actionAxCourseDocsSettings()
	{
		$courseId = Yii::app()->request->getParam("course_id", null);
		$blockId = Yii::app()->request->getParam("block_id", null);
		$model = PlayerBaseblock::model()->findByPk($blockId);
		// Check if can't find such widget
		if($model === null)
		{
			echo Yii::t('standard', '_OPERATION_FAILURE');
			Yii::app()->end();
		}

		if(isset($_POST['PlayerBaseblock']))
		{
			// Setting the model attributes to apply condition for fileLimit to be number only
			$model->attributes = $_POST['PlayerBaseblock'];

			if($model->canNormalUserUploadFiles)
				$model->scenario = 'normalUserCanUploadFiles';

			$model->params = json_encode($_POST['PlayerBaseblock']);
			// If model save(validate), leave
			if($model->save())
			{
				echo '<a class="auto-close"></a>';
				Yii::app()->end();
			}
		}

		//Load the parametters from the model
		$params = json_decode($model->params,true);
		$model->canNormalUserUploadFiles = isset($params['canNormalUserUploadFiles']) ? $params['canNormalUserUploadFiles'] : false;
		$model->fileLimit = isset($params['fileLimit']) ? $params['fileLimit'] : false;
		$model->permissionType = (isset($params['permissionType'])) ? $params['permissionType'] : 0;
		$model->permissionLevels = (isset($params['permissionLevels']) ? $params['permissionLevels'] : array());
		$html = $this->renderPartial("coursedocs/_settings", array(
			'model' => $model,
		), true, true);

		echo $html;
	}

	/**
	 * actionAxLoadCoachingChatUsers() - renders users of a given coaching session
	 */
	public function actionAxLoadCoachingChatUsers() {

		$idSession = Yii::app()->request->getParam("idSession", null);
		$searchCriteria = Yii::app()->request->getParam("search", false);


		// get all coaching session learners

		$criteria = new CDbCriteria();
		$criteria->addCondition('idSession = :idSession');
		$criteria->params[':idSession'] = $idSession;

		if ($searchCriteria) {
			$searchCriteria = preg_replace('/([^a-z0-9\-\_\.]+)/i', '', $searchCriteria);
			$criteria->with = array('user');
			$criteria->addCondition('CONCAT(user.firstname, " ", user.lastname) like :search OR user.userid LIKE :search');
			$criteria->params[':search'] = '%'.$searchCriteria.'%';
		}

		$sessionUsers = LearningCourseCoachingSessionUser::model()->findAll($criteria);

		$this->renderPartial("lms.protected.modules.player.views.block.mycoach._listChatUsers", array(
			'sessionUsers' => $sessionUsers
		), false, true);

		Yii::app()->end();
	}

	/**
	 * renders coaching chat messages per user/coaching session
	 *  also saves sent message
	 */
	public function actionAxLoadCoachingChatMessage() {
		// Id of a session
		$idSession = Yii::app()->request->getParam("idSession", null);

		// Id of a learner (CoreUser->idst)
		$idLearner = Yii::app()->request->getParam("idLearner", null);

		// Id of a coach (CoreUser->idst)
		$idCoach = Yii::app()->request->getParam("coach", false);

		// Id of the current course
		$idCourse = Yii::app()->request->getParam("course_id", null);

		// Sent message body
		$sentMessage = Yii::app()->request->getParam("write_message", false);

		// Message sent by user id
		$sentBy = Yii::app()->request->getParam("sent_by", false);

		//loadMessagesOnly
		$loadNewMessagesOnly = Yii::app()->request->getParam("loadMessagesOnly", false);                                

		// Try to find id of a coach, that is missing if there is a session passed
		if(!$idCoach && $idSession) {
			// try to get current coaching session by id
			$coachingSession = LearningCourseCoachingSession::model()->findByPk($idSession);

			// set the id of a coach if the coaching session is found above
			$idCoach = !empty($coachingSession->idCoach) ? $coachingSession->idCoach : $idCoach;
		}

		// If there is a posted message - try save them to the db - starts here
		if (($sentMessage && $sentMessage)) {
			try {
				// Create new coaching session message model
				$newMessage = new LearningCourseCoachingMessages();

				// Set session id
				$newMessage->idSession = (int)$idSession;

				// Set coach id
				$newMessage->idCoach = (int)$idCoach;

				// Sett learner id
				$newMessage->idLearner = (int)$idLearner;

				// Purify the input message
				$parser = new CHtmlPurifier(); //create instance of CHtmlPurifier
				$newMessage->message = $sentMessage = $parser->purify($sentMessage);
				$newMessage->status = 0;

				// Determine who sent the message first
				if ($sentBy == $idCoach) {
					// sent by coach
					$newMessage->sent_by_coach = 1;

					// Get the course model
					$courseModel = LearningCourse::model()->findByPk($idCourse);
					Yii::app()->event->raise('CoachMessagedLearner', new DEvent($this, array(
							'idCourse' => $idCourse,
							'message' => $newMessage->message,
							'idLearner' => (int)$idLearner,
							'idCoach' => (int)$idCoach,
							'idSession' => (int)$idSession,
							'idUser' => (int)$idLearner,
							'course' => &$courseModel
					)));
				} else {
					// else sent by learner

					// Get the course model
					$courseModel = LearningCourse::model()->findByPk($idCourse);

					// Raise event for sent by learner messages
					Yii::app()->event->raise('SentMessageToCoachByLearner', new DEvent($this, array(
						'idCourse' => $idCourse,
						'message' => $newMessage->message,
						'idLearner' => (int)$idLearner,
						'idCoach' => (int)$idCoach,
						'idSession' => (int)$idSession,
						'idUser' => Yii::app()->user->id,
						'course' => &$courseModel
					)));
				}


				// Save the message
				$newMessage->save();
			} catch (Exception $e) {
				Yii::log($e->getTraceAsString(), CLogger::LEVEL_ERROR);
			}
		} // If there is a posted message - try save them to the db - ends here
                
		// Get coaching session messages per session/learner enough criteria to filter messages records
		if (!$loadNewMessagesOnly) {
			$coachingMessages = LearningCourseCoachingMessages::model()->findAllByAttributes(array('idSession' => $idSession, 'idLearner' => $idLearner));
		} else {
			$sent_by_coach = (Yii::app()->user->idst == $idCoach) ? 0 : 1;

			$coachingMessages = LearningCourseCoachingMessages::model()->findAllByAttributes(
				array(
					'idSession'     => $idSession,
					'idLearner'     => $idLearner,
					'status'        => 0,
					'sent_by_coach' => $sent_by_coach
				)
			);
		}

		// Mark message as read starts here
		foreach($coachingMessages as $coachingMessageKey =>  $coachingMessage) {
			// If current user is a coach and the message is marked as unread(status=0)
			if ($coachingMessage->idCoach == Yii::app()->user->id && $coachingMessage->sent_by_coach == 0 && $coachingMessage->status == 0) {
				// Get current message model
				$currentCoachingMessage = LearningCourseCoachingMessages::model()->findByPk($coachingMessage->idMessage);

				// Set status to read
				$currentCoachingMessage->status = 1;
				$currentCoachingMessage->save(); // Save and apply new message status


			} elseif (($coachingMessage->idLearner == Yii::app()->user->id) && $coachingMessage->sent_by_coach == 1 && $coachingMessage->status == 0) {
				// if user is a learner and he/she this message is not marked as read
				// Get current message model
				$currentCoachingMessage = LearningCourseCoachingMessages::model()->findByPk($coachingMessage->idMessage);

				// Set status to read
				$currentCoachingMessage->status = 1;
				$currentCoachingMessage->save(); // Save and apply new message status
			}
		} // Mark message as read ends here

		// Get Coach model
		$coach = CoreUser::model()->findByPk($idCoach);
                                                
                
                $now = new DateTime();
                $sessionModel = LearningCourseCoachingSession::model()->findByPk($idSession);
                $isSessionExpired = (Yii::app()->localtime->isGt($now->format('Y-m-d'), $sessionModel->datetime_end));                                     

		// Get Learner model
		$learner = CoreUser::model()->findByPk($idLearner);

		$renderView = "lms.protected.modules.player.views.block.mycoach._listChatMessages";

		if ($loadNewMessagesOnly) { // if load new messages set
			$renderView = "lms.protected.modules.player.views.block.mycoach._listNewChatMessages";
		}

		// Render the view with messages
		$this->renderPartial($renderView, array(
			'coachingMessages'  => $coachingMessages,
			'idCourse'          => $idCourse,
			'idSession'         => $idSession,
			'idLearner'         => $idLearner,
			'idCoach'           => $idCoach,
			'coach'             => $coach,
			'learner'           => $learner,
			'loadMessagesOnly'  => $loadNewMessagesOnly,
                    'isSessionExpired'      => $isSessionExpired
		), false, true);

		Yii::app()->end();
	}

	/*
	 * Render the expired coaching webinar sessions
	 * Used in the My Coach course widget - History tab(past webinar sessions)
	 */
	public function actionAxLoadExpiredCoachingWebinarSessions() {
		// Id of the course - required in the Player area !!!
		$idCourse = Yii::app()->request->getParam("course_id", null);

		// Id of a coaching session
		$idCoachingSession = Yii::app()->request->getParam("idCoachingSession", null);

		// Get filter by date value if set
		$filterByDate = Yii::app()->request->getParam("filterByDate", false);

		// webinar sessions array default value - no sessions
		$webinarSessions = array();

		// Get the coaching session
		$coachSession = LearningCourseCoachingSession::model()->findByPk($idCoachingSession);

		// If there is a coaching session
		if ($coachSession) {
			// Get (expired/past) webinar sessions
			if ($filterByDate) {
				$filterByDate = Yii::app()->localtime->toUTC(Yii::app()->localtime->fromLocalDate($filterByDate).' 23:59:59');

				// Get the date today
				$todayDate = Yii::app()->localtime->toUTC(Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->getLocalNow()).' 00:00:00');

				// If for some reason date in future is passed, filter by current date !!!
				if (Yii::app()->localtime->isGt($filterByDate, $todayDate)) {
					$filterByDate = $todayDate;
				}

				// Here we need to filter by date passed
				$webinarSessions = $coachSession->getPastWebinarSessions($filterByDate);
			} else {
				$webinarSessions = $coachSession->getPastWebinarSessions();
			}

			// Get the date today
			$todayDate = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->getLocalNow());

			// Get the coaching session end date
			$coachingSessionEndDate = Yii::app()->localtime->stripTimeFromLocalDateTime(
				Yii::app()->localtime->toLocalDateTime($coachSession->datetime_end));

			// Determine is the coaching session expired or not
			if (Yii::app()->localtime->isGt($todayDate, $coachingSessionEndDate)) {
				$isSessionExpired = true;
			} else {
				$isSessionExpired = false;
			}

			// Determine is the user a coach or not
			$isUserCoach = (Yii::app()->user->id == $coachSession->idCoach) ? true : false;
		} else {
			// Set default values to the view
			$isSessionExpired   = true;
			$isUserCoach        = false;
		}

		// Render the view with expired coaching webinar sessions
		$this->renderPartial('lms.protected.modules.player.views.block.mycoach._listExpiredCoachingWebinarSessions', array(
			'webinarSessions'   => $webinarSessions,
			'idCourse'          => $idCourse,
			'idCoachingSession' => $idCoachingSession,
			'isSessionExpired'  => $isSessionExpired,
			'isUserCoach'       => $isUserCoach
		), false, true);

		Yii::app()->end();
	}


	/**
	 * actionAxChangeWebinarSessionStatus() - sets coaching webinar status per user
	 */
	public function actionAxChangeWebinarSessionStatus() {
		// Id of the course - required in the Player area !!!
		$idCourse = Yii::app()->request->getParam("course_id", null);

		// Id of a coaching webinar session
		$idCoachingWebinarSession = Yii::app()->request->getParam("idSession", null);

		// Get id of the learner
		$idLearner = Yii::app()->request->getParam("idLearner", null);

		// Get status value from the request
		$newStatus = Yii::app()->request->getParam("setStatus", false);

		// Html output is not needed by now
		$html = '';

		// Returning data is not needed by now
		$data = array();

		// Operation success indicator
		$operationSuccess = false;

		if (!empty($idCoachingWebinarSession) && !empty($idLearner) && !empty($newStatus)) {
			$coachingWebinarSessionUser = LearningCourseCoachingWebinarSessionUser::model()->findByAttributes(
				array('id_session' => $idCoachingWebinarSession, 'id_user' => $idLearner));

			if (empty($coachingWebinarSessionUser)) {
				$coachingWebinarSessionUser = new LearningCourseCoachingWebinarSessionUser();
				$coachingWebinarSessionUser->id_session = $idCoachingWebinarSession;
				$coachingWebinarSessionUser->id_user = $idLearner;
			}

			// If there is a model try to set the status and save it
			if (!empty($coachingWebinarSessionUser)) {
				$coachingWebinarSessionUser->status = (int) $newStatus;
				$coachingWebinarSessionUser->save();
				$operationSuccess = true;
			}
		}

		// Generate and return the response
		$response = new AjaxResult();
		$response->setStatus($operationSuccess);
		$response->setHtml($html)->setData($data)->toJSON();

		Yii::app()->end();
	}

    /**
     * Open dialog for new webinar session
     */
    public function actionAxCreateCoachingWebinarSession(){
        try {
            // Check that course model has been saved
            $idCourse = Yii::app()->request->getParam("course_id", null);
            $confirm = Yii::app()->request->getParam('confirm', false);
            $webinarSessionId = Yii::app()->request->getParam('webinar_session_id', false);
            $webinarTool = ($_POST['WebinarSession'])? $_POST['WebinarSession'] : null;
            if (!Yii::app()->request->getParam('coaching_session_id', false) && !$webinarSessionId)
                throw new Exception("Invalid coaching session id provided.", 1);

            $model = ($webinarSessionId)? LearningCourseCoachingWebinarSession::model()->findByPk($webinarSessionId) : new LearningCourseCoachingWebinarSession();

	        // Prepare list of users already assigned to the live coaching session
	        $preselectedWebinarUsers = array();
	        if ($webinarSessionId) {
                $liveSessionUsers = LearningCourseCoachingWebinarSessionUser::model()->findAllByAttributes(array('id_session' => $model->id_session));

                foreach($liveSessionUsers as $liveSessionUser) {
	                $preselectedWebinarUsers[$liveSessionUser->idUser->idst] = $liveSessionUser->idUser->getClearUserId();
                }
	        }

            if ($webinarSessionId){
                $webinarTool = array('id_tool_account' => $model->id_tool_account);
            }

            if ($confirm) {

				// Get account tool ID from selection if $webinarTool is null
				if(!$webinarTool && isset($_POST['webinar_id_tool_account'])) {
					$webinarTool = array('id_tool_account' => $_POST['webinar_id_tool_account']);
				}

                if (($_POST['LearningCourseCoachingWebinarSession']['tool'] != 'custom' && !$webinarTool) || !$_POST['LearningCourseCoachingWebinarSession']) {
                    throw new CException(Yii::t('coaching', 'Cannot create a session without the needed data!'));
                }
                if ($webinarSessionId){
                    $attributes = array('name' => $model->name, 'tool' => $model->tool, 'date' => $model->date, 'start' => $model->start, 'duration' => $model->duration);
                    //if custom add 'custom_url'
                    if ($attributes['tool'] == 'custom') {
                        $attributes['custom_url'] = $model->custom_url;
                    } else {
                        $attributes['id_tool_account'] = $model->id_tool_account;
                    }
                    $attributes = ($_POST['LearningCourseCoachingWebinarSession']['tool'] != 'custom') ? array_merge($attributes,$webinarTool, $_POST['LearningCourseCoachingWebinarSession']) : array_merge($attributes,$_POST['LearningCourseCoachingWebinarSession']);
                }else {
                    $attributes = ($_POST['LearningCourseCoachingWebinarSession']['tool'] != 'custom') ? array_merge($webinarTool, $_POST['LearningCourseCoachingWebinarSession']) : $_POST['LearningCourseCoachingWebinarSession'];
                }
                $attributes = array_merge($attributes, array('max_enroll' => 100));
                if (!$attributes || !$attributes['name'] || !$attributes['tool'] || !$attributes['date'] || !$attributes['start'] || !$attributes['duration'] || (!$webinarSessionId && !(Yii::app()->request->getParam('coaching_session_id', false))) || !(Yii::app()->request->getParam('chooseInvite', false))) {
                    throw new CException(Yii::t('coaching', 'Cannot create a session without the needed data!'));
                }
                if ($attributes['tool'] == 'custom') {
                    if (!$attributes['custom_url']) {
                        throw new CException(Yii::t('coaching', 'Cannot create a session without the needed data!'));
                    }
                } else {
                    if (!$attributes['id_tool_account']) {
                        throw new CException(Yii::t('coaching', 'Cannot create a session without the needed data!'));
                    }
                }

                $invitationMode = Yii::app()->request->getParam('chooseInvite', false);
                $idCoachingSession = ($webinarSessionId)? $model->id_coaching_session : $_POST['LearningCourseCoachingWebinarSession']['id_coaching_session'];
                if($invitationMode) {
                    if ($invitationMode == 2)
                        $users = Yii::app()->request->getParam('choose_users', false);
                    else {
                        $users = LearningCourseCoachingSession::model()->findByPk($idCoachingSession)->listOfAssignedUsers();
                    }
                }

                if(!$users || empty($users)){
                    throw new CException(Yii::t('coaching', 'No users to be invited!'));
                }

                $model->attributes = $attributes;
                if(!$webinarSessionId) {
                    $model->id_coaching_session = Yii::app()->request->getParam('coaching_session_id', false);
                }
                else{
                    $model->id_coaching_session = $idCoachingSession;
                }
                if(!$attributes['custom_url'])
                    $model->custom_url = null;

                if ($model->validate()) {
                    $model->configureDates();
                    $model->save();
                    if(!$webinarSessionId)
                        $model->assignUsers($users);
                    else{
                        $alreadyAssigned = $model->getListOfAssignedUsers();
                        $toRemove = array_diff($alreadyAssigned,$users);
                        if(count($toRemove))
                            $model->unassignUsers($toRemove);
                        $toAdd = array_diff($users,$alreadyAssigned);
                        if(count($toAdd))
                            $model->assignUsers($toAdd);
                    }
                    echo '<a class="auto-close successfullCreation"></a>';
                    Yii::app()->end();
                } else
                    throw new CException(CHtml::errorSummary($model));
            }
            else{
                $cs = Yii::app()->getClientScript();
                $themeUrl = Yii::app()->theme->baseUrl;
                $cs->registerCssFile($themeUrl . '/css/webinar.css');
                // FCBK
                $cs->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
                $cs->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');

	            // Ensure there is a coaching session id to determine the max users count
	            if (empty($idCoachingSession)) {
		            $idCoachingSession = ($webinarSessionId)? $model->id_coaching_session : false;
	            }

	            $coachingSessionUsers = ($idCoachingSession) ?
		            LearningCourseCoachingSession::model()->findByPk($idCoachingSession)->listOfAssignedUsers() :
	                false;

	            // Max items shown in FcBkAutocomplete users selector !!!
	            $maxitems = ($coachingSessionUsers) ? count($coachingSessionUsers) :
		            LearningCourseuser::getCountEnrolledByLevel($idCourse, LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);

                $this->renderPartial('lms.protected.modules.player.views.block.mycoach.webinarSession', array(
                    'model'                     => $model,
                    'coaching_session_id'       => Yii::app()->request->getParam('coaching_session_id', false),
                    'idCourse'                  => $idCourse,
                    'preselectedWebinarUsers'   => $preselectedWebinarUsers,
	                'maxitems'                  => $maxitems
                ),false,true);

                Yii::app()->end();
            }
        } catch (Exception $e) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
            $this->renderPartial('admin.protected.views.common/_dialog2_error', array(
                'message' => $e->getMessage(),
                'type' => 'error',
            ));
            Yii::app()->end();
        }
    }
    public function actionAxDeleteCoachingWebinarSession(){
        $idCourse = Yii::app()->request->getParam("course_id", null);
        $webinarSessionId = Yii::app()->request->getParam('webinar_session_id', false);
        $confirm = Yii::app()->request->getParam('confirm_button', false);
        try{
            $model = LearningCourseCoachingWebinarSession::model()->findByPk($webinarSessionId);

            if($confirm){
                $model->delete();
                echo '<a class="auto-close successfullDeletion"></a>';
                Yii::app()->end();
            }else {
                $this->renderPartial('lms.protected.modules.player.views.block.mycoach._deleteWebinarSession', array(
                    'coaching_session_id' => Yii::app()->request->getParam('coaching_session_id', false),
                    'idCourse' => $idCourse,
                    'webinar_session_id' => $webinarSessionId,
                    'session_name' => $model->name,
                    'starting_time' => $model->date
                ), false, true);
                Yii::app()->end();
            }
        } catch (Exception $e) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
            $this->renderPartial('admin.protected.views.common/_dialog2_error', array(
                'message' => $e->getMessage(),
                'type' => 'error',
            ));
            Yii::app()->end();
        }
    }

    public function actionAxCoachingSessionUsersAutocomplete() {
        // Get search tag
        $query = Yii::app()->request->getParam('tag', false);
        //$idCourse = Yii::app()->request->getParam('course_id', false);
        $idCoachingSession = Yii::app()->request->getParam('idCoachingSession', false);
        $users = Yii::app()->db->createCommand()
            ->select('cu.*')
            ->from('learning_course_coaching_session_user csu')
            ->join('core_user cu', 'cu.idst=csu.idUser')
            ->where('(cu.userid LIKE :match OR cu.firstname LIKE :match2  OR cu.lastname LIKE :match3)
                AND csu.idSession=:idCoachingSession',
                array(
                    ':match' => "/%$query%",
                    ':match2' => "%$query%",
                    ':match3' => "%$query%",
                    ':idCoachingSession' => (int) $idCoachingSession
                ))
            ->queryAll();

        $usersList = array_values(CHtml::listData($users, 'idst', 'idst'));
        $uList = array();


        if (!empty($usersList) && count($usersList)) {
            foreach ($usersList as $key => $value) {
                $currentUserModel = CoreUser::model()->findByPk($value);
                if($currentUserModel) {
                    $uList[] = array('key' => $value, 'value' => $currentUserModel->getUserNameFormatted());
                }
            }
        }

        // Ensure, that we are returning an array !!!
        $usersList = (!empty($usersList) && is_array($usersList)) ? $usersList : array();

        // send the Power Users List
        $this->sendJSON($uList);
    }


	/*
	* Render the coaching webinar sessions members
	* Used in the My Coach course widget - Webinar sessions tab/History tabe
	*/
	public function actionAxLoadCoachingWebinarSessionGuests() {
		// Id of the course - required in the Player area !!!
		$idCourse = Yii::app()->request->getParam("course_id", null);

		// Id of a coaching session
		$idCoachingWebinarSession = Yii::app()->request->getParam("idSession", null);

		$learningCourseCoachingWebinarSession = LearningCourseCoachingWebinarSession::model()->findByAttributes(array('id_session' => $idCoachingWebinarSession));

		$guests = LearningCourseCoachingWebinarSessionUser::model()->findAllByAttributes(array('id_session' => $idCoachingWebinarSession));

		$countByStatus = array(
			LearningCourseCoachingWebinarSessionUser::STATUS_INVITED    => 0,
			LearningCourseCoachingWebinarSessionUser::STATUS_ACCEPTED   => 0,
			LearningCourseCoachingWebinarSessionUser::STATUS_MAYBE      => 0,
			LearningCourseCoachingWebinarSessionUser::STATUS_DECLINED   => 0,
			'all'                                                       => 0
		);

		// Check is there webinar members to be displayed
		if (is_array($guests) && count($guests)) {
			foreach ($guests as $guest) {
				$countByStatus[$guest->status]++;
				$countByStatus['all']++;
			}
		}

		$coachingSessionLearnersCount = Yii::app()->db->createCommand()
			->select('COUNT(idUser)')
			->from('learning_course_coaching_session_user')
			->where('idSession = :idSession', array(':idSession' => $learningCourseCoachingWebinarSession->id_coaching_session))
			->queryScalar();

		// Render the view with coaching webinar session guests
		$this->renderPartial('lms.protected.modules.player.views.block.mycoach._listCoachingWebinarSessionGuests', array(
			'idCourse'                          => $idCourse,
			'guests'                            => $guests,
			'countByStatus'                     => $countByStatus,
			'coachingSessionLearnersCount'      => $coachingSessionLearnersCount
		), false, true);

		Yii::app()->end();
	}
    /**
    * actionAxLoadActiveSessionOptions() - returns html for the options box
    */
    public function actionAxLoadActiveSessionOptions(){
        $this->renderPartial('lms.protected.modules.player.views.block.mycoach._webinarSessionOptions',array(),false,true);
        Yii::app()->end();
    }

	/**
	 * actionAxLoadActiveLiveSessions() - renders active coaching live (webinar) sessions
	 */
	public function actionAxLoadActiveLiveSessions() {

		$idCourse = Yii::app()->request->getParam("course_id", null);
		$idSession = Yii::app()->request->getParam("idSession", null);
		$currentUniqueId = Yii::app()->request->getParam("currentUniqueId", '');
		$idUser = Yii::app()->request->getParam("idUser", Yii::app()->user->idst);

		$coachUser = Yii::app()->getDb()->createCommand()
			->selectDistinct('idCoach')
			->from(LearningCourseCoachingSession::model()->tableName())
			->where('idCourse = :idCourse', array(':idCourse' => $idCourse))
			->andWhere('idSession = :idSession', array(':idSession' => $idSession))
			->andWhere('idCoach = :idUser', array(':idUser' => $idUser))
			->queryColumn();

		$isUserCoach = count($coachUser) > 0 ? true : false;

		// Get the coaching session
		$coachSession = LearningCourseCoachingSession::model()->findByPk($idSession);

		if ($isUserCoach) {
			$webinarSessions = $coachSession->getFutureAndPresentWebinarSessions();
		} else {
			$webinarSessions = $coachSession->getFutureAndPresentWebinarSessions($idUser);
		}

		$this->renderPartial("lms.protected.modules.player.views.block.mycoach._listActiveLiveSessions", array(
			'webinarSessions'   => $webinarSessions,
			'isUserCoach'       => $isUserCoach,
			'idCourse'          => $idCourse,
			'idUser'            => $idUser,
			'currentUniqueId'   => $currentUniqueId
		), false, true);

		Yii::app()->end();
	}

	public function actionCreateNewFolder()
	{
		$update = false;
		$folder = new LearningCourseFile();
		if (isset($_POST['confirm_save']) && !empty($_POST['confirm_save'])) {
			$update = true;

			// create folder and append it to chosen one
			$rootId = Yii::app()->request->getParam('parentFolderId');
			$root = LearningCourseFile::model()->findByPk($rootId);

			$folderName = Yii::app()->request->getParam('folderName');
			$folderName = Yii::app()->htmlpurifier->purify($folderName);

			$folder->title = $folderName;
				$folder->item_type = $folder::FOLDER;
				$folder->path = $folder->types[$folder::FOLDER];

			if ($root !== null) {

				if($folder->validate('title')){
				$folder->appendTo($root);
				$this->reloadWidgetAtCurrentFolder();
			} else {
				$update = false;
					$folder->clearErrors();
					$folder->addError('title', Yii::t('yii','{attribute} cannot be blank.', array( '{attribute}'=> Yii::t('storage', '_ORGFOLDERNAME') )) );
			}

			} else {
				$update = false;
				$folder->addError('title', Yii::t('standard', '_OPERATION_FAILURE'));
		}
		}
		$blockId = intval(Yii::app()->request->getParam('block_id', null));
		$courseId = intval(Yii::app()->request->getParam('course_id', null));
		$listOfFolders = $this->getListOfFolders($courseId);
		$this->renderPartial('coursedocs/_create_folder', array(
			'listOfFolders' => $listOfFolders,
			'update' => $update,
			'courseId' => $courseId,
			'blockId' => $blockId,
			'folder' => $folder));
	}

	private function getListOfFolders($courseId)
	{
		$data = array();
		$folders = Yii::app()->db->createCommand()
			->select('id_file, title, path, lev')
			->from(LearningCourseFile::model()->tableName())
			->where('id_course=:id', array(':id' => $courseId))
			->andWhere('item_type=:type', array(':type' => LearningCourseFile::FOLDER))
			->order('iLeft ASC')
			->queryAll();
		$prefix = '';
		foreach ($folders as $folder){
			if ($folder['path'] == 'root' && $folder['idParent'] == 0) {
				$data[$folder['id_file']] = Yii::t('course', 'Root (All files and folders)');
			} else {
				for ($i = 1; $i < $folder['lev']; $i++) {
					$prefix .= '-';
				}
				$data[$folder['id_file']] = $prefix . $folder['title'];
				$prefix = '';
			}
			$previousFolder = $folder;
		}

		return $data;
	}	
        
        /**
         * 
         * 
         * @return type
         */
        public function actionAxGetMessage(){
            $request = Yii::app()->request;
            if($request->isAjaxRequest){
                $messageId = $request->getParam('idMessage', false);
                
                if(!$messageId){
                    return;
                }
                                                
                $message = LearningCourseCoachingMessages::model()->findByPk($messageId);                                
                
                if($message){
                    echo $message->message;
                }                
            }
        }
        
        /**
         * Delet message from the DB
         * 
         * @return type
         */
        public function actionAxDeleteMessage(){
            $request = Yii::app()->request;
            if($request->isAjaxRequest){
                $messageId = $request->getParam('idMessage', false);                                
                
                if(!$messageId){
                    return;
                }
                                                
                $message = LearningCourseCoachingMessages::model()->findByPk($messageId);                                                                
                
                if($message){
                    echo $message->delete();
                }                            
            }
        }
        
        /**
         * Get all messages and give to the JS to check if some is deleted
         * 
         * @return type
         */
        public function actionAxCheckDeletedCoachingChatMessage(){
            $request = Yii::app()->request;
            if($request->isAjaxRequest){
                $sessionId = $request->getParam('idSession', false);
                $courseId = $request->getParam('course_id', false);
                $learnerId = $request->getParam('idLearner', false);
                if(!$sessionId || !$courseId || !$learnerId){
                    return;
                }
                                                
                $message = LearningCourseCoachingMessages::model()->findAllByAttributes(array(
                    'idSession' => $sessionId,
                    'idLearner' => $learnerId
                ));                                
                
                $messageIds = array();
                                
                foreach ($message as $id => $message){
                    /**
                     * @var $message LearningCourseCoachingMessages
                     */
                    $messageIds[] = $message->idMessage;                    
                }
                
                $result = array(
                    'ids' => $messageIds
                );
                
                if(!empty($messageIds)){
                    echo CJSON::encode($result);
                }                            
            }
        }
        
        /**
         * Get only messages with date_edit and give to the JS
         * 
         * @return type
         */
        public function actionAxCheckEditedCoachingChatMessage(){
            $request = Yii::app()->request;
            if($request->isAjaxRequest){
                $sessionId = $request->getParam('idSession', false);
                $courseId = $request->getParam('course_id', false);
                $learnerId = $request->getParam('idLearner', false);
                if(!$sessionId || !$courseId || !$learnerId){
                    return;
                }
                
                $criteria = new CDbCriteria();
                $criteria->condition = "idSession = :session AND idLearner = :learner AND date_edited <> '0000-00-00 00:00:00'";
                $criteria->params[':session'] = $sessionId;
                $criteria->params[':learner'] = $learnerId;
                                                
                $messages = LearningCourseCoachingMessages::model()->findAll($criteria);                                
                
                
                $editedMessages = array();
                                
                foreach ($messages as $id => $message){
                    /**
                     * @var $message LearningCourseCoachingMessages
                     */
                    $editedMessages[$message->idMessage] = $message->message;
                }
                
                $result = array(
                    'ids' => $editedMessages
                );
                
                if(!empty($editedMessages)){
                    echo CJSON::encode($result);
                }                            
            }
        }
        
        public function actionAxEditCoachingMessage(){
            $request = Yii::app()->request;
            
            if($request->isAjaxRequest){
                $messageId = $request->getParam('id', false);
                
                if($messageId){
                    
                    $messageModel = LearningCourseCoachingMessages::model()->findByPk($messageId);
                    
                    if($_POST['new_message']){
                        $parser = new CHtmlPurifier(); //create instance of CHtmlPurifier
                        $messageModel->message = $parser->purify($_POST['new_message']);
                        $messageModel->date_edited = Yii::app()->localtime->getUTCNow();
                        
                        $messageModel->save();
                        
                        echo "<a class='auto-close'></a>";
                        Yii::app()->end();
                    }
                    
                    $this->renderPartial('mycoach/_edit_message', array(
                        'messageModel' => $messageModel
                    ));
                }
            }
        }

		public function actionDownloadCertificate() {
			$model = LearningCertificateAssign::loadByUserCourse(Yii::app()->user->id, $this->course_id);
			if ($model) {
				$model->downloadCertificate();
			}
		}

		private function reloadWidgetAtCurrentFolder($continueExecution = false){?>
			<script>
				var blockId = '<?=Yii::app()->request->getParam("block_id")?>';
				var parentId = $('#currentFolder').data('current-id');
				$('#blockid_' + blockId).off('coursedoc-upload-complete').loadOneBlockContentCompletely({parentId: parentId});
			</script>
			<?php
			if($continueExecution === false) {
				echo '<a class="auto-close"></a>';
				Yii::app()->end();
			}
		}
		
		
		public function actionAxIframeSettings()
		{
		    $courseId = Yii::app()->request->getParam("course_id", null);
		    $blockId = Yii::app()->request->getParam("block_id", null);
		    $model = PlayerBaseblock::model()->findByPk($blockId);
		    // Check if can't find such widget
		    if($model === null)
		    {
		        echo Yii::t('standard', '_OPERATION_FAILURE');
		        Yii::app()->end();
		    }
			$newParams = array();
		    if(isset($_POST['PlayerBaseblock']))
		    {
				$newParams = $_POST['PlayerBaseblock'];

				if(!$_POST['PlayerBaseblock']['iframeUrl']) {
					$model->addError('iframeUrl', Yii::t('player', 'The iframe url cannot be empty'));
				}

				if(!$_POST['PlayerBaseblock']['oauthClient']) {
					$model->addError('oauthClient', 'The OAuth Client cannot be empty');
				}

				if(!$_POST['PlayerBaseblock']['secretSalt']) {
					$model->addError('secretSalt', Yii::t('player', 'The salt cannot be empty'));
				}

				if(!$_POST['PlayerBaseblock']['repeatSecretSalt']) {
					$model->addError('repeatSecretSalt', Yii::t('player', 'The repeated salt cannot be empty'));
				}

				if($_POST['PlayerBaseblock']['repeatSecretSalt'] != $_POST['PlayerBaseblock']['secretSalt']) {
					$model->addError('repeatSecretSalt', Yii::t('thomsonreuters', 'The repeated salt is wrong'));
				}

				$params = $_POST['PlayerBaseblock'];

				unset($params['repeatSecretSalt']);

				if(!$model->hasErrors() && $model->validate()) {
					$model->scenario = 'WidgetParamsSaved';
					$model->params = json_encode($params);
					// If model save(validate), leave
					if ($model->save()) {
						echo '<a class="auto-close success"></a>';
						Yii::app()->end();
					}
				}
		    }

			$clientsList = array();
			foreach(OauthClients::model()->findAllByAttributes(array('enabled' => 1, 'hidden' => 0)) as $client) {
				$clientsList[$client['client_id']] = $client['app_name'];
			}

		    $html = $this->renderPartial("iframe/_settings", array(
		        'model' => $model,
				'clientsList' => $clientsList,
				'newParams' => $newParams
		    ), true, true);
		
		    echo $html;
		}
		
		
}
