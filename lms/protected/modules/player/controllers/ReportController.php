<?php

class ReportController extends PlayerBaseController
{

	public function beforeAction($action)
	{
        $this->breadcrumbs = array();

        PlayerBaseController::breadcrumbUserSubscription($this);
        if (isset($_GET['ref_src']) && $_GET['ref_src'] == 'admin') {
            $this->breadcrumbs[Yii::t('menu', 'Admin')] = Docebo::createAppUrl('admin:dashboard/index');
            $this->breadcrumbs[Yii::t('standard', '_COURSES')] = Docebo::createAppUrl('admin:courseManagement/index');
        } else {
            $this->breadcrumbs[Yii::t('menu_over', '_MYCOURSES')] = Docebo::createLmsUrl('site/index', array('opt' => 'fullmycourses'));
        }
		return parent::beforeAction($action);
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules()
	{
		return array(

			array('allow',
				'users' => array('@'),
					'expression' => 'Yii::app()->user->isGodAdmin || Yii::app()->controller->userCanAdminCourse ||
                        LearningCourseuser::userLevel($user->id, ' . $this->course_id . ') == LearningCourseuser::$USER_SUBSCR_LEVEL_ADMIN
                        || (Yii::app()->user->getIsPu() && CoreUserPuCourse::isCourseAssignedToPU(Yii::app()->user->id, ' . $this->course_id . '))',
			),

			array('deny', // if some permission is wrong -> the script goes here
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
				'message' => Yii::t('course', '_NOENTER'),
			),

		);
	}

	public function actionIndex()
	{
		JsTrans::addCategories('report');

		$courseModel = $this->courseModel;
		$learningOrganization = new LearningOrganization();
		$usersModel = new CoreUser('search');

		$inAjaxGridUpdate = Yii::app()->request->getParam('ajax', false);

		if (Yii::app()->user->getIsPu()) {
			$usersModel->powerUserManager = Yii::app()->user->id;
		}

		$usersModel->unsetAttributes();
		if(isset($_REQUEST['CoreUser'])) {
			$usersModel->attributes = $_REQUEST['CoreUser'];
		}

		// Calculate this data ONLY if this is NOT an Ajax Yii Grid update OR the updated grid is the OBJECTS GRID
		$objectsArray = array();
		if ( ($inAjaxGridUpdate == false) || ($inAjaxGridUpdate == 'player-objects') ) {
			$objectsArray = $learningOrganization->getLearningArray($this->course_id);
			if(isset($_GET['objects_search']) && $_GET['objects_search'] != '')
				$objectsArray = ReportHelper::filter($objectsArray, $_GET['objects_search']);
		}

		//deliverables LOs management
		$deliverablesModel = new LearningDeliverableObject('search');
		$deliverablesModel->unsetAttributes();
		if (isset($_GET['LearningDeliverableObject'])) {
			$deliverablesModel->attributes = $_GET['LearningDeliverableObject'];
		}
		$deliverablesModel->courseId = $courseModel->primaryKey;

		// Display or not select coaching session dropDown - default no
		$showCoachingSessionSelect = false;
		$coachingSessionDropDownHtml = '';
		if ($courseModel->enable_coaching && PluginManager::isPluginActive('CoachingApp')) { // Is the coaching enabled for this course
			$idCoach = Yii::app()->user->id;

			$coachSessionModels = $courseModel->getCoachSessions($idCoach, true);
			if(count($coachSessionModels) > 0) {
				$coachSessions[''] = Yii::t('standard', '_ALL');
				foreach ($coachSessionModels as $coachingSession) {
					// Prepare date start
					$coachingSessionDateStart = $coachingSession->datetime_start;
					$coachingSessionDateStart = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($coachingSessionDateStart));

					// Prepare date end
					$coachingSessionEndDate = $coachingSession->datetime_end;
					$coachingSessionEndDate = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($coachingSessionEndDate));

					// Set the coaching session details to be displayed in the dropDown
					$coachSessions[$coachingSession->idSession] = $coachingSessionDateStart . ' - ' . $coachingSessionEndDate;
				}

				// Set the id of the coaching session if it is passed to the filter form and should be used by corresponding DataProvider()
				$deliverablesModel->idCoachingSession = Yii::app()->request->getParam('coaching_session_id', false);

				// Show select(filter by) coaching session dropDown
				$showCoachingSessionSelect = true;
			}

			// Check is it an admin
			if (Yii::app()->user->getIsGodadmin()) {
				// Get all coaching sessions per this course and display them to the admin
				$coachingSessions = $courseModel->getAllCoachingSessions($courseModel->idCourse);
				if(count($coachingSessions) > 0) {
					$coachSessions[''] = Yii::t('standard', '_ALL');
					foreach ($coachingSessions as $coachingSessionId) {
						// Get coaching session model
						$coachingSession = LearningCourseCoachingSession::model()->findByPk($coachingSessionId);

						// Get coach model
						$coach = CoreUser::model()->findByPk($coachingSession->idCoach);

						// Prepare date start
						$coachingSessionDateStart = $coachingSession->datetime_start;
						$coachingSessionDateStart = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($coachingSessionDateStart));

						// Prepare date end
						$coachingSessionEndDate = $coachingSession->datetime_end;
						$coachingSessionEndDate = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($coachingSessionEndDate));

						// Set the coaching session details to be displayed in the dropDown
						$coachSessions[$coachingSession->idSession] = ($coach->getUserNameFormatted()) . ': ' .$coachingSessionDateStart . ' - ' . $coachingSessionEndDate;
					}

					// Set the id of the coaching session if it is passed to the filter form and should be used by corresponding DataProvider()
					$deliverablesModel->idCoachingSession = Yii::app()->request->getParam('coaching_session_id', false);

					// Show select(filter by) coaching session dropDown
					$showCoachingSessionSelect = true;
				}
			}
		} // End if course coaching is enabled !!!

		// Overall course statistics
		// Do not calculate in case we are in AJAX Yii Grid update
		if ($inAjaxGridUpdate == false) {
			$stats = $this->courseModel->getCourseStats();
		}

		$this->render('index', array(
			'courseModel' => $courseModel,
			'usersModel' => $usersModel,
			'deliverablesModel' => $deliverablesModel,
			'lmsLogins' => LearningTracksession::getLmsLogins($this->course_id, 14),
			'learningOrganization' => $learningOrganization,
			'stats' => $stats,
			'objectsDataProvider' => new CArrayDataProvider($objectsArray, array(
				'pagination' =>array(
					'pageSize'=>20,
					),
				)),
			'inAjaxGridUpdate' => $inAjaxGridUpdate,
			'showCoachingSessionSelect' => $showCoachingSessionSelect,
			'coachSessions' => $coachSessions
		));
	}



	public function actionByUser($user_id)
	{
		JsTrans::addCategories( array( 'report', 'standard') );

		Yii::app()->clientScript->registerScriptFile(Yii::app()->bootstrap->getAssetsUrl() . '/js/bootstrap-editable.js');
		Yii::app()->clientScript->registerCssFile(Yii::app()->bootstrap->getAssetsUrl() . '/css/bootstrap-editable.css');
		Yii::app()->clientScript->registerScriptFile($this->module->assetsUrl . '/js/report-custom-editable.js');
		Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getModule('xapi')->getAssetsUrl() . '/js/xapi.js');

		$courseModel = $this->courseModel;
		$learningOrganization = new LearningOrganization();
		$courseUser = LearningCourseuser::model()->with('user')->findByAttributes(array(
			'idCourse' => $this->course_id,
			'idUser' => $user_id,
		));

		$criteria = new CDbCriteria();
		$criteria->with['learningTracksession'] = array('on' => 'learningTracksession.idCourse = :idCourse');
		$criteria->params[':idCourse'] = $this->course_id;
		$criteria->select = '*, SUM(UNIX_TIMESTAMP(learningTracksession.lastTime) - UNIX_TIMESTAMP(learningTracksession.enterTime)) AS sessionTime';
		$criteria->group = 't.idst';
		$criteria->together = true;
		$criteria->compare('t.idst', $user_id);
		$userModel = CoreUser::model()->find($criteria);

		$timeline = LearningOrganization::getTimelineData($this->course_id, $user_id);

		$objectsArray = $learningOrganization->getLearningArray($this->course_id, $user_id, true);

		if(isset($_GET['objects_search']) && $_GET['objects_search'] != '')
			$objectsArray = ReportHelper::filter($objectsArray, $_GET['objects_search']);

		$this->render('by_user', array(
			'activityTimeline' => $timeline,
			'courseModel' => $courseModel,
			'courseUser' => $courseUser,
			'userModel' => $userModel,
			'lmsLogins' => LearningTracksession::getLmsLogins($this->course_id, 14, $user_id),
			'learningOrganization' => $learningOrganization,
			'objectsDataProvider' => new CArrayDataProvider($objectsArray,array(
				'pagination'=>array(
					'pageSize'=>20,
				),
			))
		));
	}

	/**
	 * Export reports user-table to the given format
	 */
	public function actionExport()
	{
		$user = new CoreUser('search');
		$user->unsetAttributes();
		if(isset($_GET['CoreUser']))
			$user->attributes=$_GET['CoreUser'];

		$columns = array(
			Yii::t('standard', '_USERNAME'),
            Yii::t('report', '_DATE_INSCR'),
			Yii::t('standard', '_DATE_FIRST_ACCESS'),
			Yii::t('report', '_DATE_COURSE_COMPLETED'),
			Yii::t('standard', '_DATE_LAST_ACCESS'),
			Yii::t('standard', '_PROGRESS'),
			Yii::t('course', '_PARTIAL_TIME'),
			Yii::t('standard', '_STATUS'),
			Yii::t('standard', '_SCORE'),
		);
		$excelData = array();

		$criteria = $user->searchCriteria($this->course_id, true);

		$rows = $user->findAll($criteria);

		foreach ($rows as $i => $row)
		{
			$excelData[$i][] = $row->clearUserId;
			$excelData[$i][] = $row->learningCourse->date_inscr ? $row->learningCourse->date_inscr : "-";
			$excelData[$i][] = $row->learningCourse->date_first_access ? $row->learningCourse->date_first_access : "-";
			$excelData[$i][] = $row->learningCourse->date_complete ? $row->learningCourse->date_complete : "-";
			$excelData[$i][] = $row->learningCourse->date_last_access ? $row->learningCourse->date_last_access : "-";
			$excelData[$i][] = LearningOrganization::getProgressByUserAndCourse($this->course_id, $row->idst)."%";
			$excelData[$i][] = $row->sessionTime ? Yii::app()->localtime->hoursAndMinutes($row->sessionTime) : "-";
			$excelData[$i][] = $row->learningCourse->statusLabel;
			$excelData[$i][] = LearningCourseuser::getLastScoreByUserAndCourse($this->course_id, $row->idst);
		}

		Yii::import('common.extensions.array2excel.Array2Excel');
		$xls = new Array2Excel($excelData, $columns);
		if (array_key_exists($_GET['exportType'],  LearningCourseuser::exportTypes2()))
			$xls->exportType = $_GET['exportType'];

		$xls->filename = Yii::t('stats', 'Course report');
		$xls->run();
	}


	/**
	 * See $this->actionExport()
	 * This is a more optimized version with smaller queries,
	 * but not needed for now since the original bug has disappeared
	 */
	public function actionExportNewTest()
	{

		$user = new CoreUser('search');
		$user->unsetAttributes();
		if(isset($_GET['CoreUser']))
			$user->attributes=$_GET['CoreUser'];

		$columns = array(
			Yii::t('standard', '_USERNAME'),
            Yii::t('report', '_DATE_INSCR'),
			Yii::t('standard', '_DATE_FIRST_ACCESS'),
			Yii::t('report', '_DATE_COURSE_COMPLETED'),
			Yii::t('standard', '_DATE_LAST_ACCESS'),
			Yii::t('standard', '_PROGRESS'),
			Yii::t('course', '_PARTIAL_TIME'),
			Yii::t('standard', '_STATUS'),
			Yii::t('standard', '_SCORE'),
		);
		$excelData = array();

		$criteria = $user->searchCriteria($this->course_id, true);

		$rows = $user->findAll($criteria);

		/*
		$c = new CDbCriteria();
		$c->addCondition('idCourse=:idCourse');
		$c->params[':idCourse'] = $this->course_id;
		$c->select = 'idOrg, idResource';

		// Get all LOs in this course

		// More specifically:
		// - their idOrg's to get the learning_commontrack rows and
		// - their idReference's to get the learning_scorm_items rows
		$orgs = LearningOrganization::model()->findAll($c);
		$orgIds = array(); // learning_organization.idOrg
		$refIds = array(); // learning_organization.idReference
		if($orgs){
			foreach($orgs as $org){
				$orgIds[] = $org->idOrg;
				$refIds[] = $org->idResource;
			}
		}

		// Count how much SCOs we have in the WHOLE course
		$c = new CDbCriteria();
		$c->addInCondition('idscorm_organization', $refIds);
		$scormItemsInThisCourse = LearningScormItems::model()->count($c);

		// Will contain an array of arrays in the end in this format:
		// 12301=>array('total'=>5, 'completed'=>3)
		// 12302=>array('total'=>0, 'completed'=>0)
		// if user with ID 12301 has started 5 tracks and completed 3 of them, etc
		$finalStats = array();

		// Get all student IDs in this course
		$c = new CDbCriteria();
		$c->addCondition('idCourse=:idCourse');
		$c->params[':idCourse'] = $this->course_id;
		$c->select = 'idUser';
		$c->group = 'idUser';
		$c->addCondition('level=:level');
		$c->params[':level'] = LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT;
		$students = LearningCourseuser::model()->findAll($c);

		foreach($students as $student){
			$finalStats[$student->idUser] = array('total'=>(count($orgIds)+$scormItemsInThisCourse), 'completed'=>0);
		}

		// See how much tracks were completed and group them by users
		$c = new CDbCriteria();
		$c->addInCondition('idReference', $orgIds);
		$c->select = 'idUser, status';
		$tracks = LearningCommontrack::model()->findAll($c);

		// Get and count all commontrack rows - total
		// and completed - and organize them per user
		if($tracks){
			foreach($tracks as $track){
				if(isset($finalStats[$track->idUser])){
					if($track->status == LearningCommontrack::STATUS_COMPLETED){
						$finalStats[$track->idUser]['completed']++;
					}
				}else{
					// This user isn't subscribed to the course.
					// Do we have a stranded/leftover row here?
				}
			}
		}

		// Get per-user stats for individual SCOs from SCORMs
		$c = new CDbCriteria();
		$c->addInCondition('idReference', $orgIds);
		$c->select = 'idUser, status';
		$scormItems = LearningScormItemsTrack::model()->findAll($c);
		if($scormItems){
			foreach($scormItems as $scormItem){
				if(isset($finalStats[$scormItem->idUser])){
					if($scormItem->status == LearningScormItemsTrack::STATUS_COMPLETED){
						$finalStats[$scormItem->idUser]['completed']++;
					}
				}else{
					// This user isn't subscribed to the course.
					// Do we have a stranded/leftover row here?
				}
			}
		}*/

		foreach ($rows as $i => $row)
		{
			/*$completed = $finalStats[$row->idst]['completed'];
			$totalItems = $finalStats[$row->idst]['total'];
			$percent = round(($completed * 100) / $totalItems);*/
			$excelData[$i][] = $row->clearUserId;
			$excelData[$i][] = $row->learningCourse->date_inscr ? $row->learningCourse->date_inscr : "-";
			$excelData[$i][] = $row->learningCourse->date_first_access ? $row->learningCourse->date_first_access : "-";
			$excelData[$i][] = $row->learningCourse->date_complete ? $row->learningCourse->date_complete : "-";
			$excelData[$i][] = $row->learningCourse->date_last_access ? $row->learningCourse->date_last_access : "-";
            $excelData[$i][] = LearningOrganization::getProgressByUserAndCourse($this->course_id, $row->idst)."%";
            $excelData[$i][] = $row->sessionTime ? Yii::app()->localtime->hoursAndMinutes($row->sessionTime) : "-";
            $excelData[$i][] = $row->learningCourse->statusLabel;
            $excelData[$i][] = LearningCourseuser::getLastScoreByUserAndCourse($this->course_id, $row->idst);
		}

		Yii::import('common.extensions.array2excel.Array2Excel');
		$xls = new Array2Excel($excelData, $columns);
		if (array_key_exists($_GET['exportType'],  LearningCourseuser::exportTypes2()))
			$xls->exportType = $_GET['exportType'];

		$xls->filename = Yii::t('stats', 'Course report');
		$xls->run();
	}

	public function actionExportObjects()
	{
		$learning = new LearningOrganization();
		$userId = (isset($_GET['user_id']) && $_GET['user_id'] != 'undefined'/* Some JS error causes 'undefined' */)
					? $_GET['user_id']
					: null;

		if ($userId)
		{
			$columns = array(
                Yii::t('standard', '_TITLE'),
				Yii::t('standard', '_STATUS'),
				Yii::t('player', 'Average time'),
				Yii::t('standard', '_AVERANGE'),
			);
		}
		else
		{
			$columns = array(
                Yii::t('standard', '_TITLE'),
				Yii::t('standard', '_COMPLETED'),
				Yii::t('standard', '_USER_STATUS_BEGIN'),
                Yii::t('report', '_MUSTBEGIN'),
				Yii::t('player', 'Average time'),
				Yii::t('standard', '_AVERANGE'),
			);
		}
		$excelData = array();
		$rows = $learning->getLearningArray($this->course_id, $userId);

		if(isset($_GET['objects_search']) && $_GET['objects_search'] != '')
			$rows = ReportHelper::filter($rows, $_GET['objects_search']);

		foreach ($rows as $i => $row)
		{
			$excelData[$i][] = $row->title;
			if ($userId)
			{
				if ($row->itemsInitState)
					$excelData[$i][] = Yii::t('standard', '_NOT_STARTED');
				elseif ($row->itemsCompleted)
					$excelData[$i][] = Yii::t('standard', '_COMPLETED');
				else
					$excelData[$i][] = Yii::t('standard', '_NOT_STARTED');
			}
			else
			{
				$excelData[$i][] = $row->itemsCompleted;
				$excelData[$i][] = $row->itemsAttempted;
				$excelData[$i][] = $row->itemsInitState;
			}
			$excelData[$i][] = Yii::app()->localtime->hoursAndMinutes($row->averageTime());

			switch ($row->objectType)
			{
				case '': // A SCORM SCO
					$scores = $row->scores;
					$score = isset($scores[0]) ? $scores[0] : false;
					$maxScore = isset($score['max']) && $score['max'] ? $score['max'] : 100;
					if(isset($score['raw']) && $score['raw']>0){
						//$excelData[$i][] = number_format($score['raw'],2) . '/' . number_format($maxScore,2);
						$excelData[$i][] = number_format($score['raw'],2);
					}
					break;
				case LearningOrganization::OBJECT_TYPE_SCORMORG:
					// SCORM packages don't have score for the whole package,
					// only for individual SCOs
					break;
				case LearningOrganization::OBJECT_TYPE_TEST:
				case LearningOrganization::OBJECT_TYPE_TINCAN:
				case LearningOrganization::OBJECT_TYPE_ELUCIDAT:
				default:
					$excelData[$i][] = number_format($row->averageScore(),2);
		}
		}

		Yii::import('common.extensions.array2excel.Array2Excel');
		$xls = new Array2Excel($excelData, $columns);
		if (array_key_exists($_GET['exportType'],  LearningCourseuser::exportTypes2()))
			$xls->exportType = $_GET['exportType'];

		$xls->filename = Yii::t('standard', '_REPORT');
		$xls->run();
	}

	public function actionTestUsers($organizationId)
	{
		$organizationModel = LearningOrganization::model()->findByPk($organizationId);
		$criteria = new CDbCriteria();
		if($organizationModel && $organizationModel->repositoryObject && $organizationModel->repositoryObject->shared_tracking) {
			// Collect all course placeholder for the passed object
			$organizationIds = $organizationModel->objectVersion->getCoursePlaceholdersForThisObject();
			$criteria->addCondition("idReference IN (".implode(",", $organizationIds).")");
		} else {
			$criteria->addCondition('idReference = :idReference');
			$criteria->params['idReference'] = $organizationId;
		}

		$testTrack = LearningTesttrack::model()->find($criteria);

		// Override (load) by much easily loaded list of question. The "quests" relation is heavy
		// Just in case, because there is event(s) expecting $testTrack (in the _test_users view)
		// and I don't know if it will need ->quests, for some reason [plamen]
		if(!empty($testTrack)) {
			$testTrack->quests = $testTrack->test->questionsOnly;

		$usersModel = new CoreUser('search');
		$usersModel->unsetAttributes();
		if(isset($_GET['CoreUser']['search_input']) && !empty($_GET['CoreUser']['search_input']))
		{
			$usersModel->attributes=$_GET['CoreUser'];
		}
		if(isset($_GET['test_questions']) && !empty($_GET['test_questions']))
		{
			$test_questions = Yii::app()->request->getQuery("test_questions", array());
		}
		else
			$test_questions = is_array($testTrack->test->questionsOnly) ? CHtml::listData($testTrack->test->questionsOnly, 'idQuest', 'idQuest') : array();

		$html = $this->renderPartial("_test_users", array(
			'usersModel' => $usersModel,
			'organizationId' => $organizationId,
			'testTrack' => $testTrack,
			'test_questions' => $test_questions,
			'organizationModel' => $organizationModel,
		), true, true);
		if (isset($_GET['ajax'])) //grid view pager or similar
			echo $html;
		else
		{
			$html = mb_convert_encoding($html, 'UTF-8', 'auto');
			$response = new AjaxResult();
			$response->setHtml($html)->setStatus(true)->toJSON();
		}
                } else {
                    $response = new AjaxResult();
                    $html = "<p>No answers to be shown</p>";
                    $response->setHtml($html)->setStatus(true)->toJSON();
                }
	}


	public function actionExportTestUsers($organizationId) {
		$organizationModel = LearningOrganization::model()->findByPk($organizationId);
		$criteria = new CDbCriteria();
		if($organizationModel && $organizationModel->repositoryObject && $organizationModel->repositoryObject->shared_tracking) {
			// Collect all course placeholder for the passed object
			$organizationIds = $organizationModel->objectVersion->getCoursePlaceholdersForThisObject();
			$criteria->addCondition("idReference IN (".implode(",", $organizationIds).")");
		} else {
			$criteria->addCondition('idReference = :idReference');
			$criteria->params['idReference'] = $organizationId;
		}

		$testTrack = LearningTesttrack::model()->with(array('quests'))->find($criteria);

		$user = new CoreUser('search');
		$user->unsetAttributes();
		if (isset($_GET['CoreUser']['search_input']) && !empty($_GET['CoreUser']['search_input'])) {
			$user->attributes = $_GET['CoreUser'];
			$test_questions = Yii::app()->request->getQuery("test_questions", array());
		} else {
			$test_questions = is_array($testTrack->quests) ? CHtml::listData($testTrack->quests, 'idQuest', 'idQuest') : array();
		}

		$columns = array(
			Yii::t('standard', '_USERNAME'),
			Yii::t('standard', '_FIRSTNAME'),
			Yii::t('standard', '_LASTNAME'),
			Yii::t('report', '_DATE_COURSE_COMPLETED'),
			Yii::t('standard', '_SCORE'),
		);
		if ($testTrack && $testTrack->quests) {
			foreach($testTrack->quests as $q) {
				if (in_array($q->idQuest, $test_questions)) {
					$columns[] = strip_tags($q->title_quest);
				}
			}
		}

		$excelData = array();
		$rows = $user->findAll($user->searchTestUsersCriteria($this->course_id, $organizationId));

		foreach($rows as $i => $row) {

			$excelData[$i][] = $row->clearUserId;
			$excelData[$i][] = $row->firstname;
			$excelData[$i][] = $row->lastname;
			$excelData[$i][] = $row->learningTesttrack->date_end_attempt;
			$excelData[$i][] = $row->learningTesttrack->getScoresPair();

			foreach($testTrack->quests as $q) {

				if (in_array($q->idQuest, $test_questions)) {

					$excelData[$i][] = $this->widget("TestReportsGridFormatter", array(
						'userId' => $row->clearUserId,
						'questId' => $q->idQuest,
						'trackId' => $row->learningTesttrack->idTrack,
						'html' => false
					), true);
				}
			}
		}

		Yii::import('common.extensions.array2excel.Array2Excel');
		$xls = new Array2Excel($excelData, $columns);
		if (array_key_exists($_GET['exportType'], LearningCourseuser::exportTypes2())) {
			$xls->exportType = $_GET['exportType'];
		}

		$xls->filename = Yii::t('standard', '_REPORT');
		$xls->run();
	}

	public function actionTestByUser($organizationId, $userId)
	{
		// needed for a correct display
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile($this->getPlayerAssetsUrl().'/css/testpoll.css');

		$organization = LearningOrganization::model()->findByPk($organizationId);
		if (!$organization || $organization->objectType != LearningOrganization::OBJECT_TYPE_TEST) {
			throw new CException('Invalid test');
		}

		$organization = $organization->getMasterForCentralLo($userId);
		$organizationId = $organization->idOrg;

		$test = LearningTest::model()->findByPk($organization->idResource);
		$testTrack = LearningTesttrack::model()->with(array('questsWithTitles'))->findByAttributes(array(
			'idReference' => $organizationId,
			'idUser' => $userId
		));
		if($testTrack) {
			$commonTrack = LearningCommontrack::model()->findByPk(array(
				'idTrack' => $testTrack->idTrack,
				'objectType' => LearningOrganization::OBJECT_TYPE_TEST,
				'idReference' => $organizationId
			));
		} else {
			$commonTrack = new LearningCommontrack();
		}

		$pUserRights = Yii::app()->user->checkPURights($organization->idCourse, $userId);
		if(Yii::app()->user->getIsGodadmin() //godadmin
			|| (LearningCourseuser::isUserLevel(Yii::app()->user->id, $organization->idCourse, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR) && LearningCourse::isUserAssignedToInstructorSessions($organization->idCourse, $userId))
			|| ($pUserRights->all && Yii::app()->user->checkAccess('/lms/admin/report/mod')) //PU with "mod" permissions, assigned course and user
		) {
			$isTeacher = true;
		} else {
			$isTeacher = false;
		}

		$this->renderPartial('_test_by_user', array(
			'organization' => $organization,
			'userId' => $userId,
			'testTrack' => $testTrack,
			'commonTrack' => $commonTrack,
			'questionsLang' => LearningTestquest::getQuestionTypesTranslationsList(),
			'test' => $test,
			'isTeacher' => $isTeacher
		), false, true);
	}

	public function actionPollByUser($organizationId, $userId)
	{
		// needed for a correct display
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile($this->getPlayerAssetsUrl().'/css/testpoll.css');

		$organization = LearningOrganization::model()->findByPk($organizationId);
		if (!$organization || $organization->objectType != LearningOrganization::OBJECT_TYPE_POLL) {
			throw new CException('Invalid poll');
		}

		$organization = $organization->getMasterForCentralLo($userId);
		$organizationId = $organization->idOrg;

		$poll = LearningPoll::model()->findByPk($organization->idResource);
		$pollTrack = LearningPolltrack::model()->findByAttributes(array(
			'id_reference' => $organizationId,
			'id_user' => $userId
		));
		$this->renderPartial('_poll_by_user', array(
			'userId' => $userId,
			'poll' => $poll,
			'pollTrack' => $pollTrack,
			'questionsLang' => LearningPollquest::getPollTypesTranslationsList(),
			'isTeacher' => true
		), false, true);
	}

	public function actionEditTestScore($trackId, $questId)
	{
		$res = new stdClass();
		$res->status = 'error';
		$value = Yii::app()->request->getPost('value', null);
		$model = LearningTesttrackAnswer::model()->findByAttributes(array(
			'idTrack' => $trackId,
			'idQuest' => $questId
		));
		$response = new AjaxResult();
		if ($model)
		{
			try {
				//handle input value
				//NOTE: at the moment decimal separator is not managed by international standards.
				//We are assuming the dot "." as decimal separator, but commas "," are accepted and managed too!
				//TODO: This must be changed in the future and i18n standards applied for number formats (same thing applies to _test_by_user.php view file)
				$value = str_replace(",", ".", $value); //do the same thing as in the client side

				$maxScore = Yii::app()->db->createCommand()
					->select('score_correct')
					->from(LearningTestquestanswer::model()->tableName())
					->where('idQuest = :idQuest', array(':idQuest' => $model->idQuest))
					->queryScalar();

				if($maxScore && $value > $maxScore) {
					throw new CException(Yii::t('error', 'Invalid score: maximum possible score is {maxscore}', array('{maxscore}'=>$maxScore)));
				}
				$model->score_assigned = (double)$value;
				$model->scenario = LearningTesttrackAnswer::SCENARIO_EVALUATION;
				if($model->save(false)){
					if(PluginManager::isPluginActive('NotificationApp')) {
						$event = new DEvent($this, array(
							'course' => $this->getIdCourse(),
							'user' => $model->testtrack->idUser,
							'idObject' => $model->idTrack,
							'idObjectType' => InstructorEvaluatedAssignment::TYPE_FREE_TEXT_ANSWER_TRACK
						));
						Yii::app()->event->raise('InstructorEvaluatedAssignment', $event);
					}
				}

				if(isset($model->testtrack) && $model->testtrack){ // LearningTesttrack model

					// Recalculate the score earned for the whole Test LO
					// based on the scores for each question

					$trackModel = $model->testtrack;

					if(isset($trackModel->answers)){
						$newScore = 0;
						foreach($trackModel->answers as $answer){
							if($answer->score_assigned)
								$newScore += $answer->score_assigned;
						}

						//calculate the score
						if($trackModel->test->point_type == LearningTest::POINT_TYPE_PERCENT) {
							$totalMaxScore = $trackModel->test->getMaxScore($trackModel->idUser, true);
							if (!($totalMaxScore >0)) {
								$newScore = 0;
							}
							else {
								$newScore = round(100 * $newScore / $totalMaxScore);
							}
						}
						$trackModel->score = $newScore;

						//change status
						if ($newScore < $trackModel->test->point_required) {
							$trackModel->score_status = LearningTesttrack::STATUS_NOT_PASSED;
						} else {
							$trackModel->score_status = LearningTesttrack::STATUS_VALID;
						}

						$trackModel->save();
					}

				}
			} catch(CException $e) {
				Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
				$response->setStatus(FALSE)->setMessage($e->getMessage());
			}
		}
		$response->toJSON();
	}

	public function actionScoUsers($objectId)
	{
		$scormItem = LearningScormItems::model()->findByPk($objectId);
		$usersModel = new CoreUser('search');
		$usersModel->unsetAttributes();
		if(isset($_GET['CoreUser']))
			$usersModel->attributes=$_GET['CoreUser'];

		$html = $this->renderPartial("_sco_users", array(
			'usersModel' => $usersModel,
			'scormItemId' => $objectId,
			'scormItem' => $scormItem,
		), true, true);
		if (isset($_GET['ajax'])) //grid view pager or similar
			echo $html;
		else
		{
			$response = new AjaxResult();
			$response->setHtml($html)->setStatus(true)->toJSON();
		}
	}

	public function actionAxAnswersBreakdown()
	{
		$objectId = Yii::app()->request->getParam('objectId');
		$scormItem = LearningScormItems::model()->findByPk($objectId);

		$qa = $scormItem->collectQuestionsAndAnswers();

		$this->renderPartial('_answers_breakdown', array(
			'scormItem' => $scormItem,
			'qa' => $qa
		));
	}

	public function actionScoQAXmlExport($objectId)
	{
		$objectId = Yii::app()->request->getParam('objectId');
		$exportType = Yii::app()->request->getParam('exportType');

		$scormItem = LearningScormItems::model()->findByPk($objectId);
		$qa = $scormItem->collectQuestionsAndAnswers();

		$_questionsLang = LearningTestquest::getQuestionTypesTranslationsList();
		$_allowedTypes = LearningScormItems::getAllowedInteractionTypes();

		$columns = array(
			Yii::t('report', 'Type'),
			Yii::t('report', 'Question'),
			Yii::t('report', 'Answers'),
			Yii::t('report', 'Is correct'),
			Yii::t('report', 'Percent'),
			Yii::t('report', 'Total'),
		);
		$excelData = array();

		foreach ($qa as $qKey => $question) {
			$type = $question['type'];
			$typeLabel = isset($_allowedTypes[$type]) ? $_allowedTypes[$type] : (
				isset($_questionsLang[$type]) ? $_questionsLang[$type] : $type
			);

			if (count($question['correct_responses'])==1 && is_array($question['correct_responses'][0])) {
				$correctResponses = $question['correct_responses'][0];
			} else {
				if(is_array($question['correct_responses'])){
					$correctResponses =  $question['correct_responses'];
				}else{
					$correctResponses =  array();
				}

			}

			$ans = '';
			$isCorrect = false;
			$response = array();
			if(isset($question['responses']) ){
				foreach ($question['responses'] as $k => $v) {
					$ans = (!empty($v['description']) ? $v['description'] : $k);
					$response = $v;
					$isCorrect = in_array($k, $correctResponses);
					break;
				}
			}

			if ($type==='true-false') {
				if (count($question['responses'])==1) {
					if ($ans === 't') {
						$question['responses']['f'] = array(
							'counter' => 0,
							'description' => 'f'
						);
					} else {
						$question['responses']['t'] = array(
							'counter' => 0,
							'description' => 't'
						);
					}
				}
				switch ($ans) {
					case 't': $ans = Yii::t('standard', '_TRUE'); break;
					case 'f': $ans = Yii::t('standard', '_FALSE'); break;
					default: break;
				}
			}

			$counter = intval($response['counter']);
			$totalResponses = intval($question['total_responses']);
			$percentage = $totalResponses ? floatval($counter / $totalResponses) * 100 : 0;
			$description = (!empty($question['description']) ? $question['description'] : $qKey);

			// add a break row first
			$excelData[] = array('', '', '', '', '', '');

			$excelData[] = array(
				$typeLabel,
				$description,
				$ans,
				$isCorrect ? Yii::t('standard', 'Yes') : Yii::t('standard', 'No'),
				number_format($percentage, 2),
				$counter
			);

			if (count($question['responses']) > 1) {
				$i=0;
				foreach ($question['responses'] as $rKey => $response) {
					$i++;
					if ($i==1) continue;

					$ans = (!empty($response['description']) ? $response['description'] : $rKey);
					$isCorrect = in_array($rKey, $correctResponses);

					if ($type==='true-false') {
						switch ($ans) {
							case 't': $ans = Yii::t('standard', 'True'); break;
							case 'f': $ans = Yii::t('standard', 'False'); break;
							default: break;
						}
					}

					$counter = intval($response['counter']);
					$percentage = $totalResponses ? floatval($counter / $totalResponses) * 100 : 0;

					$excelData[] = array(
						'',
						'',
						$ans,
						$isCorrect ? Yii::t('standard', '_YES') : Yii::t('standard', '_NO'),
						number_format($percentage, 2),
						$counter
					);
				}
			}
		}

		Yii::import('common.extensions.array2excel.Array2Excel');
		$xls = new Array2Excel($excelData, $columns);
		if (array_key_exists($exportType,  LearningCourseuser::exportTypes2()))
			$xls->exportType = $_GET['exportType'];

		$xls->filename = Yii::t('standard', '_REPORT');
		$xls->run();
	}

	public function actionExportScoUsers($objectId)
	{
		$user = new CoreUser('search');
		$user->unsetAttributes();
		if(isset($_GET['CoreUser']))
			$user->attributes=$_GET['CoreUser'];

		$columns = array(
            Yii::t('standard', '_USERNAME'),
			Yii::t('standard', '_FIRSTNAME'),
			Yii::t('standard', '_LASTNAME'),
			Yii::t('standard', '_STATUS'),
		);
		$excelData = array();

		$rows = $user->findAll($user->searchScormUsersCriteria($this->course_id, $objectId));
		foreach ($rows as $i => $row)
		{
			$excelData[$i][] = $row->clearUserId;
			$excelData[$i][] = $row->firstname;
			$excelData[$i][] = $row->lastname;
			$excelData[$i][] = $row->learningScormItemsTrack->status ? $row->learningScormItemsTrack->statusLabel : LearningScormItemsTrack::getStatusValue(LearningScormItemsTrack::STATUS_NOT_ATTEMPTED);
		}

		Yii::import('common.extensions.array2excel.Array2Excel');
		$xls = new Array2Excel($excelData, $columns);
		if (array_key_exists($_GET['exportType'],  LearningCourseuser::exportTypes2()))
			$xls->exportType = $_GET['exportType'];

		$xls->filename = Yii::t('standard', '_REPORT');
		$xls->run();
	}

	public function actionScoXmlReport($user_id, $objectId)
	{
		// needed for a correct display
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile($this->getPlayerAssetsUrl().'/css/testpoll.css');
		$cs->scriptMap['jquery.yiigridview.js'] = false;

		$trackingModel = LearningScormTracking::model()->findByAttributes(array(
			'idUser' => $user_id,
			'idscorm_item' => $objectId
		));
		$scormItemsTrackModel = LearningScormItemsTrack::model()->findByAttributes(array(
			'idUser' => $user_id,
			'idscorm_item' => $objectId
		));

		$interactions = $trackingModel->getLearnerInteractions(array($user_id));

		if(isset($_GET['LearningScormTracking']))
			$trackingModel->attributes=$_GET['LearningScormTracking'];

		$this->renderPartial('_sco_xml_report', array(
			'userId' => $user_id,
			'objectId' => $objectId,
			'trackingModel' => $trackingModel,
			'scormItemsTrackModel' => $scormItemsTrackModel,
			'interactions' => $interactions,
		), false, true);
	}

	public function actionScoXmlExport($user_id, $objectId)
	{
		$trackingModel = LearningScormTracking::model()->findByAttributes(array(
			'idUser' => $user_id,
			'idscorm_item' => $objectId
		));

		if(isset($_GET['LearningScormTracking']))
			$trackingModel->attributes=$_GET['LearningScormTracking'];

		$columns = array(
			'CMI',
            Yii::t('standard', '_TITLE'),
		);
		$excelData = array();

		//$rows = $trackingModel->getArrayFromXml();
		$trackingDataprovider = $trackingModel->getTrackingArray();
		$rows = $trackingDataprovider->getData();

		foreach ($rows as $i => $row)
		{
			$excelData[$i][] = $row['cmi'];
			$excelData[$i][] = $row['title'];
		}

		Yii::import('common.extensions.array2excel.Array2Excel');
		$xls = new Array2Excel($excelData, $columns);
		if (array_key_exists($_GET['exportType'],  LearningCourseuser::exportTypes2()))
			$xls->exportType = $_GET['exportType'];

		$xls->filename = 'CMI';
		$xls->run();
	}

	public function actionTinCan($user_id, $objectId)
	{
		$activity = LearningTcActivity::model()->findByPk($objectId);
		$activityProvider = $activity->ap;
		$user = CoreUser::model()->findByPk($user_id);
		$auth = LearningTcAp::buildBasicAuth($user);

        $actor = $activityProvider->buildActor($user);

		$ajax = new AjaxResult();
		$ajax->setHtml(
			$this->renderPartial('_tin_can', array(
				'activity' => $activity,
			    'ap' => $activity->ap,
				'user' => $user,
				'auth' => $auth,
				'actor' => $actor,
			    'lo' => $activity->ap->learningOrganization(),
				'registration' => $activity->ap->registration,
				'lrs_endpoint' => Docebo::getRootBaseUrl(true).'/tcapi/',
			), true, true)
		);
		$ajax->setStatus(true)->toJSON();
	}

	public function actionCommonUsers($organizationId)
	{
		$organizationModel = LearningOrganization::model()->findByPk($organizationId);
		$usersModel = new CoreUser('search');
		$usersModel->unsetAttributes();
		if(isset($_GET['CoreUser']))
			$usersModel->attributes=$_GET['CoreUser'];

		$html = $this->renderPartial("_common_users", array(
			'usersModel' => $usersModel,
			'organizationId' => $organizationId,
			'organizationModel' => $organizationModel,
		), true, true);
		if (isset($_GET['ajax'])) //grid view pager or similar
			echo $html;
		else
		{
			$response = new AjaxResult();
			$response->setHtml($html)->setStatus(true)->toJSON();
		}
	}

	public function actionDeliverableResult($organizationId)
	{
        $organizationModel = LearningOrganization::model()->findByPk($organizationId);
        $usersModel = new CoreUser('search');
        $usersModel->unsetAttributes();
        if(isset($_GET['CoreUser']))
            $usersModel->attributes=$_GET['CoreUser'];

        $html = $this->renderPartial("_deliverable_results", array(
            'usersModel' => $usersModel,
            'organizationId' => $organizationId,
            'organizationModel' => $organizationModel,
        ), true, true);
        if (isset($_GET['ajax'])) //grid view pager or similar
            echo $html;
        else
        {
            $response = new AjaxResult();
            $response->setHtml($html)->setStatus(true)->toJSON();
        }
	}

    public function actionDeliverableDetailsByUser()
    {
        $courseId = Yii::app()->request->getParam('course_id');
        $userId = Yii::app()->request->getParam('user_id');
        $organizationId = Yii::app()->request->getParam('organization_id');

        $organizationModel = LearningOrganization::model()->with(array('learningCommontrack'=>array(
	        'alias'=>'lc',
	        'condition'=>'lc.idUser = :idUser ',
	        'params'=> array(':idUser'=>$userId)
        )))->findByPk($organizationId);

        $criteria = new CDbCriteria();
        $criteria->with = array(
            'deliverableObjects' => array(
                'alias' => 'd',
                'condition' => 'd.id_user = :id_user',
                'params' => array(':id_user'=>$userId),
                'order' => 'created ASC'
            )
        );
        $criteria->addCondition('t.id_deliverable = :id_deliverable');
        $criteria->params[':id_deliverable'] = $organizationModel->idResource;
        $deliverable = LearningDeliverable::model()->find($criteria);


        $this->renderPartial('_deliverable_details_by_user', array(
            'deliverable' => $deliverable,
            'organizationModel' => $organizationModel
        ));
    }

	public function actionExportCommonUsers($organizationId)
	{
		$user = new CoreUser('search');
		$user->unsetAttributes();
		if(isset($_GET['CoreUser']))
			$user->attributes=$_GET['CoreUser'];

		$columns = array(
            Yii::t('standard', '_USERNAME'),
			Yii::t('standard', '_FIRSTNAME'),
			Yii::t('standard', '_LASTNAME'),
			Yii::t('report', '_LO_COL_FIRSTATT'),
			Yii::t('report', '_LO_COL_LASTATT'),
			Yii::t('player', 'First complete'),
			Yii::t('report', '_DATE_COURSE_COMPLETED'),
			Yii::t('standard', '_STATUS'),
		);
		$excelData = array();

		$rows = $user->findAll($user->searchCommonUsersCriteria($this->course_id, $organizationId));
		foreach ($rows as $i => $row)
		{
			$excelData[$i][] = $row->clearUserId;
			$excelData[$i][] = $row->firstname;
			$excelData[$i][] = $row->lastname;
			$excelData[$i][] = $row->learningCommontrack->firstAttempt ? $row->learningCommontrack->firstAttempt : "-";
			$excelData[$i][] = $row->learningCommontrack->dateAttempt ? $row->learningCommontrack->dateAttempt : "-";
			$excelData[$i][] = $row->learningCommontrack->first_complete ? $row->learningCommontrack->first_complete : "-";
			$excelData[$i][] = $row->learningCommontrack->last_complete ? $row->learningCommontrack->last_complete : "-";
			$excelData[$i][] = $row->learningCommontrack->getStatusLabel();
		}

		Yii::import('common.extensions.array2excel.Array2Excel');
		$xls = new Array2Excel($excelData, $columns);
		if (array_key_exists($_GET['exportType'],  LearningCourseuser::exportTypes2()))
			$xls->exportType = $_GET['exportType'];

		$xls->filename = Yii::t('stats', 'Course report');
		$xls->run();
	}

	public function actionPollResult($objectId, $organizationId)
	{
		$poll = LearningPoll::model()->with('questions')->findByPk($objectId);

		// Check if this is a central LO object with shared tracking
		$organization = LearningOrganization::model()->findByPk($organizationId);

        $query = Yii::app()->db->createCommand()
            ->select('id_track')
            ->from(LearningPolltrack::model()->tableName());

		if($organization && $organization->repositoryObject && $organization->repositoryObject->shared_tracking) {
			// Collect all course placeholder for the passed object
			$organizationIds = $organization->objectVersion->getCoursePlaceholdersForThisObject();
            $query->where(array('in', 'id_reference', $organizationIds));
		} else {
            $query->where('id_reference = :idReference', array('idReference' => $organizationId));
		}

		$pUserRights = Yii::app()->user->checkPURights($this->course_id);
		//PU not instructor
		if($pUserRights->isPu && !$pUserRights->isInstructor){
            $query->andWhere(array('in', 'id_user', CoreUserPU::getList()));
		}

		//PU instructor OR user instructor
		if(!Yii::app()->user->getIsGodadmin() && LearningCourseuser::isUserLevel(Yii::app()->user->id, $this->course_id, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR)) {
			$instructorUsers = LearningCourse::getInstructorUsers($this->course_id);
			if(!empty($instructorUsers))
                $query->andWhere(array('in', 'id_user', $instructorUsers));
		}

        $pollTrack = $query->queryColumn();

		$this->renderPartial('_poll_result', array(
			'poll' => $poll,
			'organizationId' => $organizationId,
			'pollTrack' => $pollTrack,
			'pollTranslation' => LearningPollquest::getPollTypesTranslationsList(),
		), false, true);
	}

	public function actionPollResultExport($objectId, $organizationId)
	{
		$poll = LearningPoll::model()->with('questions')->findByPk($objectId);

        $query = Yii::app()->db->createCommand()
            ->select('id_track')
            ->from(LearningPolltrack::model()->tableName());

		// Check if this is a central LO object with shared tracking
		$organization = LearningOrganization::model()->findByPk($organizationId);
        if($organization && $organization->repositoryObject && $organization->repositoryObject->shared_tracking) {
            // Collect all course placeholder for the passed object
            $organizationIds = $organization->objectVersion->getCoursePlaceholdersForThisObject();
            $query->where(array('in', 'id_reference', $organizationIds));
        } else {
            $query->where('id_reference = :idReference', array('idReference' => $organizationId));
        }

        $pUserRights = Yii::app()->user->checkPURights($this->course_id);
        //PU not instructor
        if($pUserRights->isPu && !$pUserRights->isInstructor){
            $query->andWhere(array('in', 'id_user', CoreUserPU::getList()));
        }

        //PU instructor OR user instructor
        if(!Yii::app()->user->getIsGodadmin() && LearningCourseuser::isUserLevel(Yii::app()->user->id, $this->course_id, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR)) {
            $instructorUsers = LearningCourse::getInstructorUsers($this->course_id);
            if(!empty($instructorUsers))
                $query->andWhere(array('in', 'id_user', $instructorUsers));
        }

        $pollTrack = $query->queryColumn();

        $csvName = $poll->title . "_" . date('Y_m_d_H_i');
        $csvFileName = Sanitize::fileName($csvName . ".csv", true);
        $csvFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $csvFileName;
        $fh = fopen($csvFilePath, 'w');

		$i = 0;
		$questions = $poll->questions;
		if (is_array($questions) && !empty($questions)) {

			//make sure that questions are ordered by sequence; TODO: can it be done at model level with relations?
			usort($questions, function($a, $b) {
				if ($a->sequence == $b->sequence) { return 0; }
				return ($a->sequence < $b->sequence) ? -1 : 1;
			});

			//prepare exporting data
			foreach ($questions as $question) {
                $excelData = array();
                $userAnswers = Yii::app()->db->createCommand()
                        ->select('id_answer, more_info')
                        ->from(LearningPolltrackAnswer::model()->tableName())
                        ->where(array('in', 'id_track', $pollTrack))
                        ->andWhere('id_quest = :id_quest', array(':id_quest' => $question->id_quest))
                        ->queryAll();

                $answers = Yii::app()->db->createCommand()
                    ->select('id_answer, answer')
                    ->from(LearningPollquestanswer::model()->tableName())
                    ->where('id_quest = :questId',array(':questId' => $question->id_quest))
                    ->order('sequence ASC')
                    ->queryAll();

                switch ($question->type_quest) {
                    case LearningPollquest::POLL_TYPE_CHOICE:
                    case LearningPollquest::POLL_TYPE_CHOICE_MULTIPLE:
                    case LearningPollquest::POLL_TYPE_INLINE_CHOICE:
                        $i++;
                        $excelData[$i][] = Yii::t('poll', '_QUEST');
                        $excelData[$i][] = html_entity_decode(strip_tags($question->title_quest)); //removeing unwanted html tags

                        $answer_count = array();
                        foreach ($userAnswers as $uAnswer) {
                            if (isset($answer_count[$uAnswer['id_answer']])) {
                                $answer_count[$uAnswer['id_answer']]++;
                            } else {
                                $answer_count[$uAnswer['id_answer']] = 1;
                            }
                        }

                        foreach ($answers as $answer) {
                            $i++;
                            $excelData[$i][] = html_entity_decode(strip_tags($answer['answer']));
                            $excelData[$i][] = ( isset($answer_count[$answer['id_answer']])
                                ? $answer_count[$answer['id_answer']]
                                : 0 );
                        }
                    break;
                    case LearningPollquest::POLL_TYPE_TEXT:
                        $i++;

                        $excelData[$i][] = Yii::t('poll', '_QUEST');
                        // removing unwante <p>
                        $excelData[$i][] = html_entity_decode(strip_tags($question->title_quest));
                        foreach ($userAnswers as $uAnswer) {
                            $i++;
                            if ($uAnswer['more_info']) {
                                $excelData[$i][] = ''; //leave an empty space at the first column
                                $excelData[$i][] = html_entity_decode(strip_tags($uAnswer['more_info']));
                            }
                        }
                    break;

                    case LearningPollquest::POLL_TYPE_TEACHER_EVAL:
                    case LearningPollquest::POLL_TYPE_COURSE_EVAL:
                        // we doesn't have this two question type anymore but i'm still able to export old results
                        $i++;

                        $excelData[$i][] = Yii::t('poll', '_QUEST');
                        // removing unwante <p>
                        $excelData[$i][] = html_entity_decode(strip_tags($question->title_quest));
                        $user_votes = array(
                            '1' => 0,
                            '2' => 0,
                            '3' => 0,
                            '4' => 0,
                            '5' => 0,
                            '6' => 0,
                            '7' => 0,
                            '8' => 0,
                            '9' => 0,
                            '10' => 0
                        );
                        foreach ($userAnswers as $uAnswer) {

                            if (isset($user_votes[$uAnswer['more_info']])) $user_votes[$uAnswer['more_info']]++;
                            else $user_votes[$uAnswer['more_info']]=1;
                        }
                        foreach ($user_votes as $answer => $count) {
                            $i++;
                            $excelData[$i][] = html_entity_decode(strip_tags($answer));
                            $excelData[$i][] = $count;
                        }

                    break;

                    case LearningPollquest::POLL_TYPE_LIKERT_SCALE:
                        // Prepare some data
                        $i++;
                        $questionManager = PollQuestion::getQuestionManager($question->type_quest);
                        $scales = $questionManager->getPollScales($poll->id_poll);

                        // First ROW
                        $excelData[$i][] = Yii::t('poll', '_QUEST');
                        $excelData[$i][] = html_entity_decode(strip_tags($question->title_quest));

                        // Second ROW
                        $i++;
                        $excelData[$i][] = '';
                        foreach($scales as $scale){
                            $excelData[$i][] = $scale->title;
                        }

                        // Prepare the questions data
                        $result = array();
                        foreach ($answers as $answer) {
                            if (!isset($result[$answer['id_answer']])) {
                                $result[$answer['id_answer']] = array();
                            }
                        }
                        foreach ($userAnswers as $uAnswer) {
                            if (isset($result[$uAnswer['id_answer']])){
                                $result[$uAnswer['id_answer']][(string)$uAnswer['more_info']]++;
                            }
                        }

                        // Questions Rows
                        foreach($answers as $answer){
                            $i++;
                            $excelData[$i][] = html_entity_decode(strip_tags($answer['answer']));
                            foreach($scales as $scale){
                                $excelData[$i][] = isset($result[$answer['id_answer']][$scale->id]) ? $result[$answer['id_answer']][$scale->id] : 0;
                            }
                        }

                    break;
                }
			    $i++;

                //save after every question and clean the array, so it doesn't hit the memory limit
                foreach ($excelData as $d) {
                    CSVParser::filePutCsv($fh, $d);
                }
		    }
		}

        $pathInfo = pathinfo($csvFilePath);
        $convertedFile = false;
        $gnumeric = new Gnumeric();
        if ($gnumeric->checkGnumeric()) {
            $exportType = Docebo::normalizeExportType($_GET['exportType']);
            $extension = Docebo::exportExtension($exportType);
            $convertedFile = $pathInfo['dirname'] . DIRECTORY_SEPARATOR . Yii::t('storage', '_LONAME_poll').'.' . $extension;
            $result = $gnumeric->convertTo($csvFilePath, $convertedFile, $extension);
            if (!$result) {
                fclose($fh);
                FileHelper::removeFile($csvFilePath);
                throw new CException($gnumeric->getError());
            }
        }

        if($convertedFile) {
            header('Content-disposition: attachment; filename="'.basename($convertedFile).'"');
            readfile($convertedFile);
            FileHelper::removeFile($convertedFile);
        }
        // Clean up the INPUT file (csv)
        fclose($fh);
        FileHelper::removeFile($csvFilePath);
	}

	public function actionSearchCourseUsers($query, $course_id)
	{
		$result = array();
		$model = new CoreUser('search');
		$model->search_input = $query;
		$criteria = $model->searchCriteria($course_id);
		$users = $model->findAll($criteria);
		foreach ($users as $i => $user)
		{
			$result[$i]['label'] = $user->fullName;
			$result[$i]['id'] = $user->idst;
		}
		header('Content-type: application/json');
		echo CJSON::encode($result);
	}

	/**
	 * (non-PHPdoc)
	 * @see PlayerBaseController::registerResources()
	 */
	public function registerResources() {

		parent::registerResources();


		if (!Yii::app()->request->isAjaxRequest) {
			Yii::app()->clientScript->registerScriptFile($this->module->assetsUrl . '/js/report.js');
			Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jqueryRotate.js');
			Yii::app()->clientScript->registerScriptFile($this->module->assetsUrl . '/js/timeline-min.js');
			Yii::app()->clientScript->registerCssFile($this->module->assetsUrl . '/css/timeline.css');
			Yii::app()->clientScript->registerScriptFile($this->module->assetsUrl . '/js/excanvas.js');
			Yii::app()->clientScript->registerScriptFile($this->module->assetsUrl . '/js/jquery.knob.js');
			Yii::app()->clientScript->registerCssFile($this->module->assetsUrl . '/css/fancybox-to-dialog-ui.css');
			Yii::import('common.extensions.fancybox.EFancyBox');
			$scoBox = new EFancyBox();
			$scoBox->init();

			$cs = Yii::app()->getClientScript();
			$themeUrl = Yii::app()->theme->baseUrl;

			// FCBK
			$cs->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
			$cs->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');
		}

	}



	/**
	 * In the course report grid, it allows to change the user status for a LO or scorm chapter
	 * @throws CException
	 */
	public function actionAxChangeLOUserStatus() {

		$rq = Yii::app()->request;
		$idObject = (int)$rq->getParam('object_id', 0);
		$idUser = (int)$rq->getParam('user_id', 0);
		$newStatus = $rq->getParam('value', false);
		$idScormItem = $rq->getParam('id_scorm_item', false);  // Also represents AICC Item!!!!

		$response = new AjaxResult();
		$transaction = Yii::app()->db->beginTransaction();

		$dataUpdates = array();
		$pUserRights = Yii::app()->user->checkPURights(Yii::app()->request->getParam('course_id', 0));
        if(!$this->getReportUserIsEnrolledInInstructorsSession() && $pUserRights->isInstructor && !$pUserRights->all && !Yii::app()->user->getIsGodAdmin()) {
            $response->setStatus(FALSE)->setMessage(Yii::t('error', "Permission denied. You cannot change the {param} of this user.", array('{param}' => Yii::t('report', 'status'))));
            $response->toJSON();
            exit;
        }

		try {

			$userModel = CoreUser::model()->findByPk($idUser);
			if (!$userModel) { throw new CException(Yii::t('error', 'Invalid specified user')); }

			/* @var $object LearningOrganization */
			$object = LearningOrganization::model()->findByPk($idObject);
			if (!$object) { throw new CException(Yii::t('error', 'Invalid specified learning object')); }

			// Check if this is a central LO object with shared tracking
			// and update the LO id to point to the master
			$object = $object->getMasterForCentralLo($idUser);
			$idObject = $object->idOrg;

			// SCORM Item (real SCORM item, not AICC, see else if)
			if ($object->objectType == LearningOrganization::OBJECT_TYPE_SCORMORG) {

				// We are going to edit a scorm chapter instead of a "true" LO
				if ($idScormItem > 0){

					// Check the SCO item
					$item = LearningScormItems::model()->findByPk($idScormItem);
					if (!$item) { throw new CException(Yii::t('error', 'Invalid specified scorm item')); }

					// Get SCO Item tracking record (learning_scorm_items_track)
					$track = LearningScormItemsTrack::model()->findByAttributes(array(
						'idUser' => $idUser,
						'idReference' => $idObject,
						'idscorm_item' => $idScormItem
					));

					if (empty($track)) {
						throw new CException(Yii::t('error', 'Invalid scorm item track data. Please play the object at least once!'));
					}

					//make sure that sco tracking is properly initialized
					LearningScormTracking::setupScormItemsTrack($idUser, $item->idscorm_organization, $idObject);

					switch ($newStatus) {
						case LearningCommontrack::STATUS_AB_INITIO: { $itemStatus = LearningScormItemsTrack::STATUS_NOT_ATTEMPTED; } break;
						case LearningCommontrack::STATUS_ATTEMPTED: { $itemStatus = LearningScormItemsTrack::STATUS_INCOMPLETE; } break;
						case LearningCommontrack::STATUS_COMPLETED: { $itemStatus = LearningScormItemsTrack::STATUS_COMPLETED; } break;
						case LearningCommontrack::STATUS_FAILED: { $itemStatus = LearningScormItemsTrack::STATUS_FAILED; } break;
						default: {
							throw new CException(Yii::t('error', 'Invalid specified status'));
						} break;
					}
					$track->updateStatus($itemStatus);

				} else {
					// We are updating a FULL Scorm package

					//check if the new status must be applied to scorm items
					if (!is_array($newStatus) || !isset($newStatus['status'])) {
						throw new CException(Yii::t('error', 'Invalid specified status'));
					}
					$inherit = (isset($newStatus['inherit']) ? ((int)$newStatus['inherit'] > 0) : false);
					$newStatus = $newStatus['status'];

					//retrive tracking info records
					$track = LearningCommontrack::model()->findByAttributes(array('idReference' => $idObject, 'idUser' => $idUser));
					$scormTracks = LearningScormTracking::model()->findAllByAttributes(array('idReference' => $idObject, 'idUser' => $idUser));

					//check if the object has already been played (and then some tracking data exists, otherwise we are unable to apply the new status)
					if (empty($scormTracks)) {
						throw new CException(Yii::t('error', 'Invalid scorm item track data. Please play the object at least once!'));
					}

					//initialize common tracking, if needed
					if (!$track) { CommonTracker::track($idObject, $idUser, $newStatus); }

					//update tracking status through Commontrack model function
					if ($track && $track->status != $newStatus) {
						$track->status = $newStatus;
						$rs = $track->save();
						if (!$rs) { throw new CException(Yii::t('error', 'Error while updating tracking status')); }
					}

					//do we have to update scorm items status too?
					if ($inherit) {
						switch ($newStatus) {
							case LearningCommontrack::STATUS_AB_INITIO: { $itemStatus = LearningScormItemsTrack::STATUS_NOT_ATTEMPTED; } break;
							case LearningCommontrack::STATUS_ATTEMPTED: { $itemStatus = LearningScormItemsTrack::STATUS_INCOMPLETE; } break;
							case LearningCommontrack::STATUS_COMPLETED: { $itemStatus = LearningScormItemsTrack::STATUS_COMPLETED; } break;
							case LearningCommontrack::STATUS_FAILED: { $itemStatus = LearningScormItemsTrack::STATUS_FAILED; } break;
							default: {
							throw new CException(Yii::t('error', 'Invalid specified status'));
							} break;
						}

						//retrieve all scorm items track records
						$items = LearningScormItemsTrack::model()->findAllByAttributes(array('idReference' => $idObject, 'idUser' => $idUser));
						$isCompleted = ($itemStatus == LearningScormItemsTrack::STATUS_COMPLETED || $itemStatus == LearningScormItemsTrack::STATUS_PASSED);
						if (!empty($items)) {
							//now set new status to each item
							foreach ($items as $item) {
								/* @var $item LearningScormItemsTrack */
								$item->status = $itemStatus;
								$item->nChildCompleted = ($isCompleted ? $item->nChild : 0);
								$item->nDescendantCompleted = ($isCompleted ? $item->nDescendant : 0);
								$rs = $item->save();
								if (!$rs) { throw new CException(Yii::t('error', 'Error while updating tracking status')); }
								//"synchronize" status in learning_scorm_tracking table
								$item->setScormTrackingStatus();
							}
						}
					}
				}

			}
			// AICC item (AU or Objective)
			else if ($object->objectType == LearningOrganization::OBJECT_TYPE_AICC) {

				$analyzeStatus = $newStatus;
				if (is_array($analyzeStatus)) {
					$newStatus = false;
					if (isset($analyzeStatus['inherit'])) {
						$inherit = (int) $analyzeStatus['inherit'] > 0;
					}
					if (isset($analyzeStatus['status'])) {
						$newStatus = $analyzeStatus['status'];
					}
				}

				switch ($newStatus) {
					case LearningCommontrack::STATUS_AB_INITIO: { $itemStatus = LearningAiccItemTrack::LESSON_STATUS_NOT_ATTEMPTED; } break;
					case LearningCommontrack::STATUS_ATTEMPTED: { $itemStatus = LearningAiccItemTrack::LESSON_STATUS_INCOMPLETE; } break;
					case LearningCommontrack::STATUS_COMPLETED: { $itemStatus = LearningAiccItemTrack::LESSON_STATUS_COMPLETED; } break;
					case LearningCommontrack::STATUS_FAILED: 	{ $itemStatus = LearningAiccItemTrack::LESSON_STATUS_FAILED; } break;
					default: {
						throw new CException(Yii::t('error', "Invalid specified status ($newStatus)"));
					} break;
				}

				// Specific AU (playable unit) status change requested?
				if ($idScormItem > 0){

					// Check the item
					$item = LearningAiccItem::model()->findByPk($idScormItem);
					if (!$item) { throw new CException(Yii::t('error', 'Invalid AICC Item')); }

					$track = LearningAiccItemTrack::getTrack($idUser, $idScormItem);
					if (!$track) { throw new CException(Yii::t('error', 'Invalid AICC Item track')); }

					$oldStatus = $track->lesson_status;
					$track->lesson_status = $itemStatus;

					$rs = $track->save();
					if (!$rs) { throw new CException(Yii::t('error', 'Error while updating tracking status')); }

					if ( ($oldStatus != $itemStatus) &&  ($itemStatus == LearningAiccItemTrack::LESSON_STATUS_COMPLETED)) {
						$rs = $track->propagateCompletion();
					}
					else {
						// Propagate (forcefully) status to Package tracking and to Common tracking, overriding even COMPLETE status
						$rs = $track->propagateNonComplete($newStatus, true);
					}

				}
				// AICC package as whole selected (LO level)
				// Change all descendants status if "inherit" is requested!
				else {
					$object = LearningOrganization::model()->findByPk($idObject);
					$package = $object->aiccPackage;
					$masterItem = $package->masterItem;
					$packageTrack = $package->getTrackForUser($idUser);

					$packageTrack->status = $newStatus;
					$rs = $packageTrack->save();
					if (!$rs) { throw new CException(Yii::t('error', 'Error while updating package tracking status')); }

					CommonTracker::track($package->idReference, $idUser, $newStatus);

					//do we have to update items status too?
					if ($inherit) {
						foreach ($package->items as $item) {
							$isCompleted = ($itemStatus == LearningAiccItemTrack::LESSON_STATUS_COMPLETED || $itemStatus == LearningAiccItemTrack::LESSON_STATUS_PASSED);
							$track 							= $item->getTrackForUser($idUser);
							$track->nChildCompleted 		= ($isCompleted ? $item->nChild : 0);
							$track->nDescendantCompleted 	= ($isCompleted ? $item->nDescendant : 0);
							$track->lesson_status = $itemStatus;
							$rs = $track->save();
							if (!$rs) { throw new CException(Yii::t('error', 'Error while updating tracking status')); }
						}
					}




				}

			} else {

				$track = LearningCommontrack::model()->findByAttributes(array('idReference' => $idObject, 'idUser' => $idUser));
				if (!$track) {
					//commontrack record does not exists yet, user has not started the LO, so initialize the tracking
					if ($newStatus != LearningCommontrack::STATUS_AB_INITIO) {

						switch($object->objectType){
							case LearningOrganization::OBJECT_TYPE_POLL:
								$specific = new LearningPolltrack();
								$specific->id_user = $idUser;
								$specific->id_reference = $idObject;
								$specific->id_poll = $object->idResource;
								$specific->status = $newStatus;
								if (!$specific->save()) { throw new CException(Yii::t('error', 'Error while saving file tracking data')); }
							break;

							case LearningOrganization::OBJECT_TYPE_AUTHORING:
								$specific = new LearningConversion();
								$specific->authoring_id = $object->idResource;
								$specific->user_id = $idUser;
								$specific->completion_status = $newStatus;
								if (!$specific->save()) { throw new CException(Yii::t('error', 'Error while saving authoring tracking data')); }
								break;

							case LearningOrganization::OBJECT_TYPE_ITEM:
							case LearningOrganization::OBJECT_TYPE_FILE:
								$specific = new LearningMaterialsTrack();
								$specific->idReference = $idObject;
								$specific->idUser = $idUser;
								$specific->idResource = $object->idResource;
								$specific->objectType = LearningOrganization::OBJECT_TYPE_FILE;
								$specific->setStatusPropertyName('status');
								$specific->status = $newStatus;
								if (!$specific->save()) { throw new CException(Yii::t('error', 'Error while saving file tracking data')); }
								break;

							case LearningOrganization::OBJECT_TYPE_VIDEO:
								$specific = new LearningVideoTrack();
								$specific->idReference = $idObject;
								$specific->idUser = $idUser;
								$specific->idResource = $object->idResource;
								$specific->accesses = 0;
								$specific->total_time = 0;
								$specific->status = $newStatus;

								//LearningVideoTrack creates a row in commonTrack automatically via behaviour CommontrackBehavior
								if (!$specific->save(false)) { throw new CException(Yii::t('error', 'Error while saving video tracking data')); }

								break;
							case LearningOrganization::OBJECT_TYPE_TEST:
								$specific = new LearningTesttrack();
								$specific->idReference = $idObject;
								$specific->idUser = $idUser;
								$specific->idTest = $object->idResource;
								$specific->score_status = $newStatus;
								if (!$specific->save(false)) { throw new CException(Yii::t('error', 'Error while saving test tracking data')); }
								break;

							case LearningOrganization::OBJECT_TYPE_TINCAN:
							case LearningOrganization::OBJECT_TYPE_ELUCIDAT:
								$specific = new LearningTcTrack();
								$specific->idReference = $idObject;
								$specific->idResource = $object->idResource;
								$specific->idUser = $idUser;
								if (!$specific->save(false)) { throw new CException(Yii::t('error', 'Error while saving tracking data')); }
								CommonTracker::track($idObject, $idUser, $newStatus,  array('preventStatusDowngrade' => true));
								break;

							case LearningOrganization::OBJECT_TYPE_HTMLPAGE:
								$specific = new LearningMaterialsTrack();
								$specific->idReference = $idObject;
								$specific->idUser = $idUser;
								$specific->idResource = $object->idResource;
								$specific->objectType = LearningOrganization::OBJECT_TYPE_HTMLPAGE;
								$specific->setStatusPropertyName('status');
								$specific->status = $newStatus;
								if (!$specific->save()) { throw new CException(Yii::t('error', 'Error while saving file tracking data')); }
							break;
						}

					}
				}
				else
				{
					// update tracking status through Commontrack model function
					if ($track && $track->status != $newStatus) {
						$rs = $track->updateStatus($newStatus);

						/* We have moved this functionality of resetting the attempts
						in a separate grid column/button
						switch($object->objectType){
							case LearningOrganization::OBJECT_TYPE_TEST:

								// The admin has changed the user's status in a test to "not started"
								// so also restart the number of attempts to 0, so the user
								// can reattempt the test

								$testTrack = LearningTesttrack::model()->findByPk($track->idTrack);

								if($testTrack && $newStatus==LearningCommontrack::STATUS_AB_INITIO){
									$testTrack->number_of_attempt = 0;
									$testTrack->save();
								}
								break;
						}*/

						if (!$rs) { throw new CException(Yii::t('error', 'Error while updating tracking status')); }
                        else{
                            // Raise event right after the Instructor update track status
                            Yii::app()->event->raise('afterTrackStatusChange', new DEvent($this, array(
                                'track' => $track, // AR relation
                            )));
                        }
					}
				}
			}

			//finish operations
			$transaction->commit();

			//post actions
			if ($object->objectType == LearningOrganization::OBJECT_TYPE_SCORMORG) {
				//if ($idScormItem > 0) {
					$orgTrack = LearningScormTracking::model()->findByAttributes(array('idReference' => $idObject, 'idUser' => $idUser));
					if ($orgTrack) {
						switch ($orgTrack->lesson_status) {
							case LearningScormTracking::STATUS_COMPLETED: { $orgStatus = LearningCommontrack::STATUS_COMPLETED; } break;
							case LearningScormTracking::STATUS_FAILED: { $orgStatus = LearningCommontrack::STATUS_FAILED; } break;
							case LearningScormTracking::STATUS_INCOMPLETE: { $orgStatus = LearningCommontrack::STATUS_ATTEMPTED; } break;
							case LearningScormTracking::STATUS_NOT_ATTEMPTED: { $orgStatus = LearningCommontrack::STATUS_AB_INITIO; } break;
							case LearningScormTracking::STATUS_PASSED: { $orgStatus = LearningCommontrack::STATUS_COMPLETED; } break;
							default: { $orgStatus = false; } break;
						}
						if (isset($orgStatus)) { $dataUpdates[] = array('id' => $idObject, 'newStatus' => $orgStatus); }
					} else {
						//if specific scorm tracking info is not present, then fallback to common tracking
						$commonTrack = LearningCommontrack::model()->findByAttributes(array('idReference' => $idObject, 'idUser' => $idUser));
						if ($commonTrack) { $dataUpdates[] = array('id' => $idObject, 'newStatus' => $commonTrack->status); }
					}
				//} else {
					$items = LearningScormItemsTrack::model()->findAllByAttributes(array('idReference' => $idObject, 'idUser' => $idUser));
					if (!empty($items)) {
						foreach ($items as $item) {
							if ($item->idscorm_item > 0) {
								switch ($item->status) {
									case LearningScormItemsTrack::STATUS_COMPLETED: { $itemStatus = LearningCommontrack::STATUS_COMPLETED; } break;
									case LearningScormItemsTrack::STATUS_FAILED: { $itemStatus = LearningCommontrack::STATUS_FAILED; } break;
									case LearningScormItemsTrack::STATUS_INCOMPLETE: { $itemStatus = LearningCommontrack::STATUS_ATTEMPTED; } break;
									case LearningScormItemsTrack::STATUS_NOT_ATTEMPTED: { $itemStatus = LearningCommontrack::STATUS_AB_INITIO; } break;
									case LearningScormItemsTrack::STATUS_PASSED: { $itemStatus = LearningCommontrack::STATUS_COMPLETED; } break;
									default: { $itemStatus = false; } break;
								}
								if ($itemStatus) {
									$dataUpdates[] = array('id' => $idObject, 'id_scorm_item' => $item->idscorm_item, 'newStatus' => $itemStatus);
								}
							}
						}
					}
				//}
			}

			//return data to client
			$response->setStatus(TRUE)->setData(array('updates' => $dataUpdates));

		} catch(CException $e) {

			$transaction->rollback();
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			$response->setStatus(FALSE)->setMessage($e->getMessage());
		}

		$response->toJSON();
	}

	/**
	 * Displays a dialog (and does the actual deleting on confirm button click)
	 * for clearing all user's progress in a specific LO. Essentially, a reset for
	 * the user's progress
	 *
	 * @throws CException
	 */
	public function actionAxResetLoStats(){
		$rq = Yii::app()->request;
		$idObject = (int)$rq->getParam('object_id', 0);
		$idUser = (int)$rq->getParam('user_id', 0);
		$idCourse = (int)$rq->getParam('course_id',0);

		$confirmClear = (int)$rq->getParam('confirm',0);
		$pUserRights = Yii::app()->user->checkPURights($idCourse);
        if(!$this->getReportUserIsEnrolledInInstructorsSession() && !$pUserRights->isInstructor && !Yii::app()->user->getIsGodAdmin()) {
            echo "<h1>".Yii::t('error', 'Error')."</h1>" . "<div class='alert alert-danger'>" . Yii::t('error', "Permission denied. You cannot change the {param} of this user.", array('{param}' => Yii::t('report', 'stats'))) . "</div>";
            Yii::app()->end();
        }

		$userModel = CoreUser::model()->findByPk($idUser);
		if (!$userModel) { throw new CException(Yii::t('error', 'Invalid specified user')); }

		$object = LearningOrganization::model()->findByPk($idObject);
		if (!$object) { throw new CException(Yii::t('error', 'Invalid specified learning object')); }

		// Check if this is a central LO object with shared tracking
		// and update the LO id to point to the master
		$object = $object->getMasterForCentralLo($idUser);
		$idObject = $object->idOrg;
		// Calculate if the resetting of the status will harm anyone and if so show it to the Admin
		$willHarm = $object->getUserLoCourseParticipation($idUser, $idCourse);

		if($confirmClear){
			LearningCommontrack::resetTracking($idObject, $idCourse,$idUser);
			// Finally, delete the common track row itself
			//for deliverable remove only evaluation
			if($object->objectType != LearningOrganization::OBJECT_TYPE_DELIVERABLE)
				LearningCommontrack::model()->deleteAllByAttributes(array('idReference' => $idObject, 'idUser' => $idUser));

            // Just informing the Plugins that we just reset User - LO status
            $event = new DEvent($this, array('model' => $object, 'id_user' => $idUser, 'id_course' => $idCourse, 'user' => $userModel));
            Yii::app()->event->raise('ResetUserLoStatus', $event);

			echo '<a class="auto-close refresh"></a>';
			Yii::app()->end();
		}

		$this->renderPartial('_reset_user_lo_stats', array(
			'user_id'=>$idUser,
			'course_id'=>$idCourse,
			'object_id'=>$idObject,
			'willHarm' => $willHarm
		));
	}

	/**
	 * In the course report gris, allows to change an user's score (for LOs with score, such as SCORM, TEST, TINCAN)
	 * @throws CException
	 */
	public function actionAxChangeLOUserScore() {

		$rq = Yii::app()->request;
		$idObject = (int)$rq->getParam('object_id', 0);
		$idUser = (int)$rq->getParam('user_id', 0);
		$newScore = (double)$rq->getParam('value', false);
		$idScormItem = $rq->getParam('id_scorm_item', false);

		$response = new AjaxResult();
		$pUserRights = Yii::app()->user->checkPURights(Yii::app()->request->getParam('course_id', 0));
        if(!$this->getReportUserIsEnrolledInInstructorsSession() && $pUserRights->isInstructor && !$pUserRights->all && !Yii::app()->user->getIsGodAdmin()) {
            $response->setStatus(FALSE)->setMessage(Yii::t('error', "Permission denied. You cannot change the {param} of this user.", array('{param}' => Yii::t('report', 'scores'))));
            $response->toJSON();
            exit;
        }

		$transaction = Yii::app()->db->beginTransaction();

		$dataUpdates = array();

		try {

			$userModel = CoreUser::model()->findByPk($idUser);
			if (!$userModel) { throw new CException(Yii::t('error', 'Invalid specified user')); }

			$object = LearningOrganization::model()->findByPk($idObject);
			if (!$object) { throw new CException(Yii::t('error', 'Invalid specified learning object')); }

			// Check if this is a central LO object with shared tracking
			// and update the LO id to point to the master
			$object = $object->getMasterForCentralLo($idUser);
			$idObject = $object->idOrg;

			//set score of a LO
			switch ($object->objectType) {
				case LearningOrganization::OBJECT_TYPE_TEST: {

					$test = LearningTest::model()->findByPk($object->idResource);
					$track = LearningTesttrack::model()->findByAttributes(array('idReference' => $idObject, 'idUser' => $idUser));
					if (empty($track)) {
						//no track data is present yet, so create it
						$track = new LearningTesttrack();
						$track->idTest = $object->idResource;
						$track->idUser = $idUser;
						$track->idReference = $idObject;
						$track->date_attempt = Yii::app()->localtime->toLocalDateTime();
						$track->date_end_attempt = Yii::app()->localtime->toLocalDateTime();
						$track->last_page_seen = 0;
						$track->number_of_save = 0;
						$track->number_of_attempt = 0;
						$track->score_status = LearningTesttrack::STATUS_NOT_COMPLETE;
						if (!$track->save()) { throw new CException(Yii::t('error', 'Error while saving tracking info')); }
					}
					//$track->score = $newScore;
					$track->bonus_score = $newScore - $track->score;

					$maxScore = $test->getMaxScore($idUser);
					if ($newScore > $maxScore) {
						throw new CException(Yii::t('error', 'Invalid score: maximum possible score is {maxscore}', array('{maxscore}'=>$maxScore)));
					}
					if ($newScore < $test->point_required) {
						$track->score_status = LearningTesttrack::STATUS_NOT_PASSED;
						$dataUpdates[] = array('id' => $object->getPrimaryKey(), 'newStatus' => LearningCommontrack::STATUS_FAILED);
					} else {
						$track->score_status = LearningTesttrack::STATUS_VALID;
						$dataUpdates[] = array('id' => $object->getPrimaryKey(), 'newStatus' => LearningCommontrack::STATUS_COMPLETED);
					}

					if (!$track->save()) { throw new CException(Yii::t('error', 'Error while updating {object_type} score', array('{object_type}' => LearningOrganization::OBJECT_TYPE_TEST))); }

				} break;

				case LearningOrganization::OBJECT_TYPE_TINCAN:
				case LearningOrganization::OBJECT_TYPE_ELUCIDAT:
				    {

					//for tincan object, the score is stored in learning_commontrack directly
					$track = LearningCommontrack::model()->findByAttributes(array('idReference' => $idObject, 'idUser' => $idUser));
					if (!$track) {
						//commontrack record does not exists yet, user has not started the LO, so initialize the tracking
						$track = new LearningCommontrack();
						$track->idReference = $idObject;
						$track->idUser = $idUser;
						$track->objectType = $object->objectType;
						$track->dateAttempt = Yii::app()->localtime->toLocalDateTime();
						$track->status = LearningCommontrack::STATUS_ATTEMPTED;
						$track->firstAttempt = Yii::app()->localtime->toLocalDateTime();
						$track->first_complete = NULL;
						$track->last_complete = NULL;
						$track->first_score = 0;
						//$track->score = $newScore;
						$track->score_max = NULL;
						$track->total_time = NULL;
					}
					$track->score = $newScore;

					//update tracking status through Commontrack model function
					if ($track) {
						$rs = $track->save();
						if (!$rs) { throw new CException(Yii::t('error', 'Error while updating {object_type} score', array('{object_type}' => LearningOrganization::OBJECT_TYPE_TINCAN))); }
					}

				} break;

				case LearningOrganization::OBJECT_TYPE_SCORMORG: {

					$track = LearningScormTracking::model()->findByAttributes(array('idReference' => $idObject, 'idUser' => $idUser));
					if (!$track) {
						//scorm tracking record does not exists, so create it
						$track = new LearningScormTracking();
						$track->idUser = $idUser;
						$track->idReference = $idObject;
						$track->idscorm_item = 0; //??!?
						$track->user_name = LearningScormTracking::formatUserName($userModel->firstname, $userModel->lastname);
						$track->lesson_location = NULL; //??!?
						$track->credit = NULL;//'credit'; //??!?
						$track->lesson_status = LearningScormTracking::STATUS_NOT_ATTEMPTED;
						$track->entry = 'ab-initio';
						//$track->score_raw = $newScore;
						$track->score_max = NULL;
						$track->score_min = NULL;
						$track->total_time = LearningScormTracking::formatTotalTime(0);
						$track->lesson_mode = NULL;//'normal'; //??!?
						$track->exit = NULL; //??!?
						$track->session_time = NULL;
						$track->suspend_data = NULL;
						$track->launch_data = NULL;
						$track->comments = NULL;
						$track->comments_from_lms = NULL;
						$track->xmldata = NULL;
						$track->first_access = NULL;
						$track->last_access = NULL;
					}
					$track->score_raw = $newScore;

					//update tracking status
					if ($track) {
						$rs = $track->save();
						if (!$rs) { throw new CException(Yii::t('error', 'Error while updating {object_type} score', array('{object_type}' => LearningOrganization::OBJECT_TYPE_SCORMORG))); }
					}

				} break;

				case LearningOrganization::OBJECT_TYPE_DELIVERABLE: {
					//for deliverable object, the score is stored in learning_deliverable_object
					$deliverableObject = LearningDeliverableObject::model()->findByAttributes(array('id_deliverable' => $object->idResource, 'id_user' => $idUser, 'evaluation_status' => LearningDeliverableObject::STATUS_ACCEPTED));
					if (!$deliverableObject) {
						throw new CException(Yii::t('error', 'Error: no valid deliverable found'));
					}
					$deliverableObject->evaluation_score = $newScore;

					if ($deliverableObject) {
						$rs = $deliverableObject->save();
						if (!$rs) { throw new CException(Yii::t('error', 'Error while updating {object_type} score', array('{object_type}' => LearningOrganization::OBJECT_TYPE_DELIVERABLE))); }
					}

				} break;

				case LearningOrganization::OBJECT_TYPE_AICC: {
					$track = LearningCommontrack::model()->findByAttributes(array('idReference' => $idObject, 'idUser' => $idUser));
					if (!$track) {
						$track = new LearningCommontrack();
						$track->idReference = $idObject;
						$track->idUser = $idUser;
						$track->objectType = $object->objectType;
						$track->dateAttempt = Yii::app()->localtime->toLocalDateTime();
						$track->status = LearningCommontrack::STATUS_ATTEMPTED;
						$track->firstAttempt = Yii::app()->localtime->toLocalDateTime();
						$track->first_complete = NULL;
						$track->last_complete = NULL;
						$track->first_score = 0;
						$track->score_max = NULL;
						$track->total_time = NULL;
					}
					$track->score = $newScore;
					if ($track) {
						$rs = $track->save();
						if (!$rs) { throw new CException(Yii::t('error', 'Error while updating {object_type} score', array('{object_type}' => LearningOrganization::OBJECT_TYPE_AICC))); }
					}
				} break;


				default: {
					throw new CException( Yii::t('error', 'Error: type "{object_type}" has no score', array('{object_type}'=>$object->objectType)) );
				} break;
			}


			$transaction->commit();
			$response->setStatus(TRUE)->setData(array('updates' => $dataUpdates));

		} catch(CException $e) {

			$transaction->rollback();
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			$response->setStatus(FALSE)->setMessage($e->getMessage());
		}

		$response->toJSON();
	}

    protected function reportGridRenderCommontrackStatus($data, $index) {
        $css = '';
        switch ($data->status) {
            case LearningCommontrack::STATUS_COMPLETED:
                $css = 'green';
                break;
            case LearningCommontrack::STATUS_FAILED:
                $css = 'red';
                break;
            default:
                $css = 'grey';
                break;
        }
        $html = '<span class="i-sprite is-circle-check '.$css.'"></span>';
        return $html;
    }

    protected function reportGridRenderDeliverableStatus($data, $index) {
        $css = '';
        switch ($data->evaluation_status) {
            case LearningDeliverableObject::STATUS_ACCEPTED:
                $css = 'green';
                break;
            case LearningDeliverableObject::STATUS_REJECTED:
                $css = 'red';
                break;
            default:
                $css = 'grey';
                break;
        }
        $html = '<span class="i-sprite is-circle-check '.$css.'"></span>';
        return $html;
    }

    protected function reportGridRenderDeliverableOptions($data, $index) {
        $evaluateUrl = Yii::app()->createUrl('deliverable/default/axEvaluate', array('id'=>$data->idObject));
		$pUserRights = Yii::app()->user->checkPURights($this->course_id, $data->id_user);

        if ($data->evaluation_status == LearningDeliverableObject::STATUS_PENDING) {
			if(Yii::app()->user->getIsGodadmin() //godadmin
				|| (LearningCourseuser::isUserLevel(Yii::app()->user->id, $this->course_id, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR) && LearningCourse::isUserAssignedToInstructorSessions($this->course_id, $data->id_user)) //instructor
				|| ($pUserRights->all && Yii::app()->user->checkAccess('/lms/admin/report/mod')) //PU with "mod" permissions assigned course and user
			) {
            	$html = '<a href="'. $evaluateUrl .'" class="open-dialog custom-grid-link" data-dialog-class="modal-evaluate-deliverable">'.Yii::t('classroom', 'Evaluate').'</a>';
			} else {
				$html = '';
			}
        }
        else {
            $html = '<a href="' . $evaluateUrl . '" class="open-dialog" data-dialog-class="modal-evaluate-deliverable"><span class="i-sprite is-search"></span></a>';
        }
        return $html;
    }

	/**
	 * @param $data
	 * @param $index
	 * @return string
	 */
	protected function reportGridRenderLoAddedAfterCourseCompletion($data, $index) {
		// course is completed
		if($data['status'] == LearningCourseuser::$COURSE_USER_END) {
			// find all LO added after the course completion
			$LOs = Yii::app()->db->createCommand()
				->select('COUNT(idOrg)')
				->from(LearningOrganization::model()->tableName())
				->where('dateInsert > :completionDate AND idCourse = :idCourse AND objectType <> ""', array(
					':completionDate' => $data['date_complete'],
					':idCourse' => $this->course_id,
				))
				->queryScalar();
			if($LOs) {
				return CHtml::link(
					'<i style="font-size: 18px;" class="fa fa-eye" rel="tooltip" data-toggle="tooltip" title="'.Yii::t('standard', '_MORE_INFO').'"></i>',
					array("loAddedAfterCourseCompletion", "course_id"=>$data["idCourse"], "user_id"=>$data["idUser"]),
					array('class' => 'open-dialog')
				);
			}
		}
		return '';
	}

	/**
	 * @param $course_id
	 * @param $user_id
	 */
	public function actionLoAddedAfterCourseCompletion($course_id, $user_id) {
		$enrollment = LearningCourseuser::model()->findByAttributes(array('idUser' => $user_id, 'idCourse' => $course_id));

		if(!$enrollment || $enrollment->status != LearningCourseuser::$COURSE_USER_END) {
			echo Yii::t('standard', '_OPERATION_FAILURE');
			Yii::app()->end();
		}

		$command = Yii::app()->db->createCommand()
			->select('COUNT(idOrg)')
			->from(LearningOrganization::model()->tableName())
			->where('dateInsert > :completionDate AND idCourse = :idCourse AND objectType <> ""', array(
				':completionDate' => Yii::app()->localtime->fromLocalDateTime($enrollment->date_complete),
				':idCourse' => $this->course_id,
			))
			->order('dateInsert DESC');

		$commandData = clone $command;
		$commandData->select('title, dateInsert');
		$numRecords = $command->queryScalar();

		$config = array(
			'totalItemCount'	=>	$numRecords,
			'pagination' 		=> false,
		);

		$dataProvider = new CSqlDataProvider($commandData, $config);

		echo $this->renderPartial('_lo_added_after_course_completion', array(
			'dataProvider' => $dataProvider
		));
	}

	/**
	 * Extract -> Convert -> Download  Course Users/Objects STATS report using ProgressController based dialog(s)
	 *
	 * Called by ProgressController workflow
	 *
	 * @throws CException
	 */
	public function actionProgressiveExporterCourseStats()
	{

		$response = new AjaxResult();

		// Number of rows to extract per chunk; make it as high as possible without ruining the memory
		$defaultLimit = Yii::app()->params['report_export_chunk_size'];
		$defaultLimit = 10;

		// Collect all possible/expected request data

		// If it is SCO-Users export..
		$idScormItem = Yii::app()->request->getParam('idScormItem', false);

		// If it is TEST-Users export..
		$idTest = Yii::app()->request->getParam('idTest', false);

		// If it is "common" LO-Users export..  (just the rest of the LO types)
		$idOrg = Yii::app()->request->getParam('idOrg', false);

		$idUser = Yii::app()->request->getParam('user_id', false);
		$idCourse = Yii::app()->request->getParam('course_id', false);
		$searchInput = Yii::app()->request->getParam('search_input', false);
		$limit = Yii::app()->request->getParam('limit', $defaultLimit);
		$start = Yii::app()->request->getParam('start', false);
		$fileName = basename(Yii::app()->request->getParam('fileName', false));
		$totalNumber = Yii::app()->request->getParam('totalNumber', false);
		$download = Yii::app()->request->getParam('download', false);
		$downloadGo = Yii::app()->request->getParam('downloadGo', false);
		$exportType = Yii::app()->request->getParam('exportType', LearningReportFilter::EXPORT_TYPE_CSV);
		$phase = Yii::app()->request->getParam('phase', 'start');
		$page = Yii::app()->request->getParam('page', 0);
		$type = Yii::app()->request->getParam('type', 0);
		$quests = Yii::app()->request->getParam('quests', '');


		// DOWNLOAD button clicked in the dialog. This is just a signal to close the dialog
		if ($download) {
			echo '<a class="auto-close"></a>';
			Yii::app()->end();
		}

		// Real DOWNLOAD action: a result of JS redirection from the dialog
		if ($downloadGo) {
			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_UPLOADS);
			$storage->sendFile($fileName);
			Yii::app()->end();
		}

		// Since we may receive different values for export type (like 'xls', 'Excel5',..), we need to normalize it...
		$exportType = Docebo::normalizeExportType($exportType);

		try {

			$courseModel = LearningCourse::model()->findByPk($idCourse);

			if (!$courseModel) {
				throw new CException('Invalid course');
			}

			// Different report types require different data providers, columns etc.
			switch ($type) {
				case Progress::JOBTYPE_COURSE_SINGLE_OBJECT_STATS:
					$dataInfo = $this->getCourseSingleLoDataInfo($idCourse, $idOrg, $defaultLimit, $page, $idScormItem, $searchInput, $quests);
					$headers = $dataInfo['headers'];
					$dataProvider = $dataInfo['dataProvider'];
					$reportTitle = 'Course_Single_Lo_Stats_C' . $idCourse
						. ($idOrg ? '_LO' . $idOrg : '')
						. ($idScormItem ? '_SCO' . $idScormItem : '');
					break;

				case Progress::JOBTYPE_COURSE_USERS_REPORT:
					$columns = $this->columnsCourseReportUsersStats($idCourse, true);
					$dataInfo = $this->getCourseUsersStatsDataInfo($idCourse, $columns, $defaultLimit, $page, $searchInput);
					$headers = $dataInfo['headers'];
					$dataProvider = $dataInfo['dataProvider'];
					$reportTitle = 'Course_Users_Stats_C' . $idCourse;
					break;

				case Progress::JOBTYPE_COURSE_OBJECTS_REPORT:
					$dataInfo = $this->getCourseLoStatsDataInfo($idCourse, $idUser, $defaultLimit, $page, $searchInput);
					$headers = $dataInfo['headers'];
					$dataProvider = $dataInfo['dataProvider'];
					$reportTitle = 'Course_Lo_Stats_C' . $idCourse;
					break;

				default:
					throw new CException('Invalid report type, can not get data provider');
					break;

			}


			// Add time to report title
			$reportTitle = $reportTitle . "_" . date('Y_m_d_H_i');

			// Sanitize file names
			$csvFileName = Sanitize::fileName($reportTitle . ".csv", true);
			$csvFileName = $fileName ? $fileName : $csvFileName;

			// CSV file is extracted into Uploads folder
			$csvFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $csvFileName;

			// Phases are "start" => "extract" [ => "convert"]  => "download"
			if ($phase == 'start' || $start) {
				$fh = fopen($csvFilePath, 'w');
				// Write a HEADER line
				CSVParser::filePutCsv($fh, $headers);
			} else if ($phase == 'extract') {
				$fh = fopen($csvFilePath, 'a');
				/* @var $dataProvider CActiveDataProvider */
				$pageData = $dataProvider->getData();

				$columns = $this->columnsCourseReportUsersStats($idCourse, true);

				foreach ($pageData as $data) {

					switch ($type) {
						case Progress::JOBTYPE_COURSE_SINGLE_OBJECT_STATS:
							$gnumeric = new Gnumeric();
							$exportAsXls = ($exportType == Docebo::EXPORT_EXT_XLS && $totalNumber <= Gnumeric::MAX_ROWS_XLS && $gnumeric->checkGnumeric())? true : false;
							$values = $this->getCourseSingleLoOneExportRow($data, $idOrg, $idScormItem, $exportAsXls, $quests);
							break;

						case Progress::JOBTYPE_COURSE_USERS_REPORT:
							$values = $this->getCourseUsersStatsOneExportRow($data, $idCourse, $columns);
							break;

						case Progress::JOBTYPE_COURSE_OBJECTS_REPORT:
							$values = $this->getCourseLoStatsOneExportRow($data, $idCourse, $idUser);
							break;

						// @TODO  Single Object -> TEST
						// @TODO  Single Object -> SCO Item

						default:
							throw new CException('Invalid report type (2)');
							break;
					}

					if($exportType == Docebo::EXPORT_EXT_XLS){
						foreach($values as $key=>$val){
							$values[$key] = "'".$val;
						}
					}

					CSVParser::filePutCsv($fh, $values);
				}
				// Set page number for the next call
				$nextPage = $page + 1;

			} else if ($phase == 'convert') {
				$gnumeric = new Gnumeric();
				$in = $csvFilePath;
				$pathInfo = pathinfo($in);

				$exportType = Docebo::normalizeExportType($exportType);
				$extension = Docebo::exportExtension($exportType);

				$fileName = $pathInfo['filename'] . "." . $extension;

				$out = $pathInfo['dirname'] . DIRECTORY_SEPARATOR . $fileName;

				// Use gnumeric to convert the file
				$format = false;

				// Note: this is almost useless, because Gnumeric recognize the output extension and uses correct format
				switch ($exportType) {
					case Docebo::EXPORT_TYPE_CSV:
						$format = Gnumeric::FORMAT_CSV;
						break;
					case Docebo::EXPORT_TYPE_XLS:
						$format = Gnumeric::FORMAT_XLS;
						break;
				}

				// Do the conversion and get result (true, false)
				$result = $gnumeric->convertTo($in, $out, $format);

				if (!$result) {
					throw new CException($gnumeric->getError());
				}

				// Clean up the INPUT file (csv)
				FileHelper::removeFile($in);

			}

			// Close file if we've got a valid file handler
			if ($fh) {
				fclose($fh);
			}

			// We count records the first time we've got a request ('start' phase) and pass the counter to next call to save some work
			if (!$totalNumber) {
				$totalNumber = $dataProvider->totalItemCount;
			}
			$totalPageNumbers = ceil($totalNumber / $defaultLimit);

			// Calculate the extraction NUMBER for the NEXT (!) request which we show NOW!
			// i.e. in current render we show what is GOING TO BE done in the next cycle
			$exportingNumber = $defaultLimit * ($page + 1);
			$exportingNumber = min($exportingNumber, $totalNumber);

			// Also, in percentage ...
			$exportingPercent = 0;
			if ($totalNumber > 0) {
				$exportingPercent = $exportingNumber / $totalNumber * 100;
			}


			// 'Phase' control
			switch ($phase) {
				case 'start':      // after 'start', we begin extraction
					$nextPhase = 'extract';
					break;
				case 'extract':  // we continue extracttion phase until end-of-records; then switch to 'convert'/'download'
					$nextPhase = $phase;
					if ($nextPage >= $totalPageNumbers) {
						if (($exportType == Docebo::EXPORT_TYPE_XLS) && ($totalNumber > Gnumeric::MAX_ROWS_XLS)) {
							$exportType = Docebo::EXPORT_TYPE_CSV;
							$nextPhase = 'download';
							$forceCsvMaxRows = true;
						} else {
							// Check if we have healthy Gnumeric installation
							$gnumeric = new Gnumeric();
							$check = $gnumeric->checkGnumeric();
							if (!$check) {
								$forceCsvMissingGnumeric = true;
								$exportType = Docebo::EXPORT_TYPE_CSV;
								$nextPhase = 'download';
							} else {
								$nextPhase = $exportType == Docebo::EXPORT_TYPE_CSV ? 'download' : 'convert';
							}
						}
					}
					break;

				case 'convert':    // after conversion we are going to 'download' the final file
					$nextPhase = 'download';
					break;

			}


			// We completely stop calling ourselves when we reach the "download" phase. Tell this to the ProgressController JS
			$stop = ($nextPhase == 'download') || ($totalNumber <= 0);

			// Render the dialog to show what is going ot be done on NEXT CALL (if any)
			// or to offer final actions, like "Doanload"
			$html = $this->renderPartial('_progressive_exporter', array(
				'reportTitle' => $reportTitle,
				'totalNumber' => $totalNumber,
				'exportingNumber' => $exportingNumber,
				'exportingPercent' => $exportingPercent,
				'fileName' => $fileName,
				'nextPhase' => $nextPhase,
				'exportType' => $exportType,
				'forceCsvMaxRows' => $forceCsvMaxRows,
				'forceCsvMissingGnumeric' => $forceCsvMissingGnumeric,
				'course_id' => $idCourse,
			), true);


			// These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stoppping when $stop == true)
			$data = array(
				'page' => $nextPage,
				'stop' => $stop,
				'fileName' => $csvFileName,
				'totalNumber' => $totalNumber,
				'exportType' => $exportType,
				'phase' => $nextPhase,
				'course_id' => $idCourse,
				'search_input' => $searchInput,
				'idOrg' => $idOrg,
				'idScormItem' => $idScormItem,
				'user_id' => $idUser,
			);

			// Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
			$response->setStatus(true);
			$response->setHtml($html)->setData($data)->toJSON();
			Yii::app()->end();
		} catch (CException $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			$response->setStatus(false)->setMessage($e->getMessage());
			$response->toJSON();
			Yii::app()->end();
		}

	}



    /**
     * Course -> USERS STATS report info
     *
     * @param number $idCourse
     * @param array $columns Columns definition array, the one used for the grid as well
     * @param number $pageSize Chunck size
     * @param number $currentPage Current page under processing
     * @param string $search Search string, if any
     *
     * @return multitype:CSqlDataProvider multitype:unknown
     */
    private function getCourseUsersStatsDataInfo($idCourse, $columns, $pageSize=false, $currentPage=0, $search=null) {

    	$pagination = array();
    	if ((int) $pageSize > 0) {
    		$pagination['pageSize'] 	= (int) $pageSize;
    		$pagination['currentPage'] 	= (int) $currentPage;
    	}
    	else {
    		$pagination = false;
    	}

    	$dataProvider = $this->sqlDataProviderCourseReportUsersStats($idCourse, $search, true);
    	$dataProvider->pagination = $pagination;

    	$headers = array();
    	foreach ($columns as $columns) {
    		$headers[] = $columns["name"];
    	}

    	// Return structure
    	$result = array(
    			'dataProvider' 	=> $dataProvider,
    			'headers'		=> $headers,
    	);

    	return $result;

    }

    /**
     * Course -> USERS STATS report ONE EXPORT ROW
     *
     * @param object $data AR Dataprovider data item
     * @param number $idCourse
     * @return array
     */
    private function getCourseUsersStatsOneExportRow($data, $idCourse, $columns) {
    	$values = array();
			$tmpValue = '';
    	foreach ($columns as $column) {

		    if (is_callable($column["value"]))
			    $tmpValue = call_user_func($column["value"], $data, 0);
		    else
			    eval('$tmpValue=' . $column["value"] . ';');
		    $values[] = $tmpValue;
    	}
    	return $values;
    }





    /**
     * Course -> Object Stats info
     *
     * @param number $idCourse
     * @param string $idUser
     * @param string $pageSize
     * @param number $currentPage
     * @param string $search
     * @return array
     */
    private function getCourseLoStatsDataInfo($idCourse, $idUser=false, $pageSize=false, $currentPage=0, $search=null) {

    	// BIG NOTE: At the moment we use Array Data provider, but this is bad, very bad.. In case of MANY users subscribed to the course
    	// Consider using SQL data provider, which means.. change the whole Course -> Reports -> LO Stats

    	if ($idUser) {
    		$headers = array(
    				Yii::t('standard', '_TITLE'),
    				Yii::t('standard', '_STATUS'),
    				Yii::t('player', 'Time'),
    				Yii::t('standard', '_AVERANGE'),
    		);
    	}
    	else {
    		$headers = array(
    				Yii::t('standard', '_TITLE'),
    				Yii::t('standard', '_COMPLETED'),
    				Yii::t('standard', '_USER_STATUS_BEGIN'),
    				Yii::t('report', '_MUSTBEGIN'),
    				Yii::t('player', 'Average time'),
    				Yii::t('standard', '_AVERANGE'),
    		);
    	}

    	$learning = new LearningOrganization();
    	$learningArray = $learning->getLearningArray($idCourse, $idUser);

    	// Pagination, if any
    	$pagination = array();
    	if ((int) $pageSize > 0) {
    		$pagination['pageSize'] 	= (int) $pageSize;
    		$pagination['currentPage'] 	= (int) $currentPage;
    	}
    	else {
    		$pagination = false;
    	}

    	$config =  array(
    			'pagination' => $pagination,
    	);

    	$dataProvider = new CArrayDataProvider($learningArray, $config);

    	// Return structure
    	$result = array(
    			'dataProvider' 	=> $dataProvider,
    			'headers'		=> $headers,
    	);

    	return $result;

    }


    /**
     * Course -> OBJECTS STATS report ONE EXPORT ROW
     *
     * @param object $data of class ReportHelper
     * @param number $idCourse
     * @param string $idUser Optional
     * @return string
     */
    private function getCourseLoStatsOneExportRow($data, $idCourse, $idUser=FALSE) {

    	$values[] = $data->title;

    	if ($idUser) {
    		if ($data->itemsCompleted)
    			$values[] = Yii::t('standard', '_COMPLETED');
    		elseif ($data->itemsAttempted)
    			$values[] = Yii::t('standard', '_USER_STATUS_BEGIN');
    		elseif ($data->itemsInitState)
    			$values[] = Yii::t('report', '_MUSTBEGIN');
    		elseif ($data->itemsFailed)
    			$values[] = Yii::t('test', '_TEST_FAILED');
    		elseif ($data->itemsPassed)
    			$values[] = Yii::t('coursereport', '_PASSED');
    		else
    			$values[] = Yii::t('standard', '_NOT_STARTED');
    	}
    	else {
    		$values[] = $data->itemsCompleted;
    		$values[] = $data->itemsAttempted;
    		$values[] = $data->itemsInitState;
    	}
    	$values[] = Yii::app()->localtime->hoursAndMinutes($data->averageTime());

    	switch ($data->objectType) {
    		case '': // A SCORM SCO
    			$scores = $data->scores;
    			$score = isset($scores[0]) ? $scores[0] : false;
    			$maxScore = isset($score['max']) && $score['max'] ? $score['max'] : 100;
    			if(isset($score['raw']) && $score['raw']>0){
    				$values[] = number_format($score['raw'],2);
    			}
    			break;
    		case LearningOrganization::OBJECT_TYPE_SCORMORG:
    			// SCORM packages don't have score for the whole package,
    			// only for individual SCOs
    			break;
    		case LearningOrganization::OBJECT_TYPE_TEST:
    		case LearningOrganization::OBJECT_TYPE_TINCAN:
    		case LearningOrganization::OBJECT_TYPE_ELUCIDAT:
    		default:
    			$values[] = number_format($data->averageScore(),2);
    			break;
    	}

    	return $values;
    }



    /**
     * Course -> SINGLE OBJECT STATS report info
     *
     * @param number $idCourse
     * @param number $idOrg
     * @param string $pageSize
     * @param number $currentPage
     * @param string $search
     *
     * @return array TWO elements array: data provider and export column headers
     */
    private function getCourseSingleLoDataInfo($idCourse, $idOrg, $pageSize=false, $currentPage=0, $idScormItem=false, $search=null, $quests = '') {

    	$pagination = array();
    	if ((int) $pageSize > 0) {
    		$pagination['pageSize'] 	= (int) $pageSize;
    		$pagination['currentPage'] 	= (int) $currentPage;
    	}
    	else {
    		$pagination = false;
    	}

    	$user = new CoreUser('search');
    	$user->unsetAttributes();
    	$user->search_input = $search;

    	// "Common", normal LO types (HTML, Vide, File, SCORM, TEST...)
    	if ($idOrg) {

    		// Check if this is a TEST LO
    		$isTest = false;
    		if ($idOrg) {
    			$loModel = LearningOrganization::model()->findByPk($idOrg);
    			$isTest = $loModel->objectType == LearningOrganization::OBJECT_TYPE_TEST;
    		}

    		// NOT a TEST LO
    		if (!$isTest) {
		    	$criteria = $user->searchCommonUsersCriteria($idCourse, $idOrg);
		    	$config =  array(
		    			'criteria' => $criteria,
		    			'pagination' => $pagination,
		    	);
		    	$headers = array(
		    			Yii::t('standard', '_USERNAME'),
		    			Yii::t('standard', '_FIRSTNAME'),
		    			Yii::t('standard', '_LASTNAME'),
		    			Yii::t('report', '_LO_COL_FIRSTATT'),
		    			Yii::t('report', '_LO_COL_LASTATT'),
		    			Yii::t('player', 'First complete'),
		    			Yii::t('report', '_DATE_COURSE_COMPLETED'),
		    			Yii::t('standard', '_STATUS'),
		    	);
    		}
    		// TEST LO
    		else {
				$addCondition = '';
				if($quests !== '')
					$addCondition = ' AND quests.idQuest IN ('.$quests.')';

				if($loModel->repositoryObject && $loModel->repositoryObject->shared_tracking) {
					// Collect all course placeholder for the passed object
					$organizationIds = $loModel->objectVersion->getCoursePlaceholdersForThisObject();
				} else
					$organizationIds = array($idOrg);

				/* @var $command CDbCommand */
				$command = Yii::app()->db->createCommand("SELECT quests.idQuest, quests.id_test, quests.title_quest,t.score_status
															FROM ".LearningTesttrack::model()->tableName()." t
															LEFT JOIN (
															 SELECT  t1.*, t2.id_test, t2.sequence
															 FROM ".LearningTestquest::model()->tableName()." t1
															 JOIN ".LearningTestQuestRel::model()->tableName()." t2 ON (t1.idQuest = t2.id_question AND t2.id_test = :id_test)
															) quests ON (quests.id_test = t.idTest AND quests.type_quest != :quest_type_break_page AND quests.type_quest != :quest_type_title)
															JOIN (
															 SELECT MIN(idTrack) minIdTrack
															 FROM ".LearningTesttrack::model()->tableName()."
															 WHERE idReference IN (".implode(",", $organizationIds).")
															) m ON m.minIdTrack = t.idTrack
															WHERE t.idReference IN (".implode(",", $organizationIds).") ".$addCondition."
															ORDER BY quests.sequence ASC");

				$result = $command->queryAll(true, array(
					':id_test' => $loModel->idResource,
					':quest_type_break_page' => LearningTestquest::QUESTION_TYPE_BREAK_PAGE,
					':quest_type_title' => LearningTestquest::QUESTION_TYPE_TITLE
				));

				$headers = array(
					Yii::t('standard', '_USERNAME'),
					Yii::t('standard', '_FIRSTNAME'),
					Yii::t('standard', '_LASTNAME'),
					Yii::t('report', '_DATE_COURSE_COMPLETED'),
					Yii::t('standard', '_SCORE'),
					Yii::t('standard', '_STATUS'),
				);
				Yii::app()->event->raise('onGetCourseSingleLoDataInfoHeaders', new DEvent($this, array(
					'headers'=>&$headers,
				)));
				if (!empty($result)) {
					foreach ($result as $row) {
						$headers[] = TestReportsGridFormatter::formatTextForExport($row['title_quest']);
						$headers[] = TestReportsGridFormatter::formatTextForExport(Yii::t('report', 'answer is correct'));
					}
				}

    			$criteria = $user->searchTestUsersCriteria($idCourse, $idOrg);

				Yii::app()->event->raise('onGetCourseSingleLoDataInfoSingleCriteriaForTest', new DEvent($this, array(
					'criteria'=>&$criteria
				)));

    			$config =  array(
    					'criteria' => $criteria,
    					'pagination' => $pagination,
    			);
    		}

    	}
    	// SCORM SCO Items export
    	else if ($idScormItem) {
    		$criteria = $user->searchScormUsersCriteria($idCourse, $idScormItem);
    		$config =  array(
    				'criteria' => $criteria,
    				'pagination' => $pagination,
    		);
    		$headers = array(
    			Yii::t('standard', '_USERNAME'),
    			Yii::t('standard', '_FIRSTNAME'),
    			Yii::t('standard', '_LASTNAME'),
    			Yii::t('standard', '_STATUS'),
				Yii::t('standard', '_SCORE'),
    		);
    	}

    	$dataProvider = new CActiveDataProvider('CoreUser', $config);

    	// Return structure
    	$result = array(
    			'dataProvider' 	=> $dataProvider,
    			'headers'		=> $headers,
    	);

    	return $result;

    }


	/**
	 * Course -> SINGLE OBJECT STATS report ONE EXPORT ROW
	 *
	 * @param object $data AR Dataprovider data item
	 * @param bool $idOrg
	 * @param bool $idScormItem
	 * @param bool $exportAsXls
	 * @internal param number $idCourse
	 * @return array
	 */

	private function getCourseSingleLoOneExportRow($data, $idOrg=false, $idScormItem=false, $exportAsXls=false, $quests = '') {

		$values = array();

		if ($idOrg) {

			// Check if this is a TEST LO
			$isTest = false;
			if ($idOrg) {
				$loModel = LearningOrganization::model()->findByPk($idOrg);
				$isTest = $loModel->objectType == LearningOrganization::OBJECT_TYPE_TEST;
			}

			// NOT TEST LO
			if (!$isTest) {
				$values[] = $data->clearUserId;
				$values[] = $data->firstname;
				$values[] = $data->lastname;
				$values[] = $data->learningCommontrack->firstAttempt ? $data->learningCommontrack->firstAttempt : "-";
				$values[] = $data->learningCommontrack->dateAttempt ? $data->learningCommontrack->dateAttempt : "-";
				$values[] = $data->learningCommontrack->first_complete ? $data->learningCommontrack->first_complete : "-";
				$values[] = $data->learningCommontrack->last_complete ? $data->learningCommontrack->last_complete : "-";
				$values[] = $data->learningCommontrack->getStatusLabel();
			}
			// TEST LO
			else {
				$values[] = $data->clearUserId;
				$values[] = $data->firstname;
				$values[] = $data->lastname;
				$values[] = ($exportAsXls ? "'" : '').$data->learningTesttrack->date_end_attempt;
				$values[] = ($exportAsXls ? "'" : '').$data->learningTesttrack->getScoresPair();
				switch ($data->learningTesttrack->score_status) {
					case LearningTesttrack::STATUS_CHECKED :
						$values[] = Yii::t('coursereport', 'CHECKED');
						break;
					case LearningTesttrack::STATUS_DOING :
						$values[] = Yii::t('coursereport', 'DOING');
						break;
					case LearningTesttrack::STATUS_COMPLETED :
						$values[] = Yii::t('standard', '_COMPLETED');
						break;
					case LearningTesttrack::STATUS_FAILED :
						$values[] = Yii::t('standard', 'failed');
						break;
					case LearningTesttrack::STATUS_NOT_PASSED :
						$values[] = Yii::t('coursereport', '_NOT_PASSED');
						break;
					case LearningTesttrack::STATUS_NOT_CHECKED :
						$values[] = Yii::t('coursereport', '_NOT_CHECKED');
						break;
					case LearningTesttrack::STATUS_NOT_COMPLETE :
						$values[] = Yii::t('coursereport', 'NOT COMPLETE');
						break;
					case LearningTesttrack::STATUS_PASSED :
						$values[] = Yii::t('coursereport', '_PASSED');
						break;
					case LearningTesttrack::STATUS_VALID :
						$values[] = Yii::t('coursereport', 'VALID');
						break;
				}

				$command = Yii::app()->db->createCommand()
					->select('tq.idQuest, t.idTest, tta.idAnswer,tq.type_quest, tta.more_info, tqa.answer, tqa.is_correct')
					->from('learning_testtrack t')
					->join(
						'learning_test_quest_rel qr',
						'qr.id_test = t.idTest')
					->join('learning_testquest tq',
						'qr.id_question = tq.idQuest AND tq.type_quest != :quest_type_break_page AND tq.type_quest != :quest_type_title' . (($quests !== '') ? " AND tq.idQuest IN (" . $quests . ')' : ''),
						array(
							':quest_type_break_page' => LearningTestquest::QUESTION_TYPE_BREAK_PAGE,
							':quest_type_title' => LearningTestquest::QUESTION_TYPE_TITLE
						)
					)
					->leftJoin('learning_testtrack_answer tta',
						'tta.idTrack = t.idTrack AND tq.idQuest = tta.idQuest AND t.idTrack = :idTrack AND (tta.user_answer = 1 OR tq.type_quest = "'.LearningTestquest::QUESTION_TYPE_ASSOCIATE.'")',
						array(':idTrack' => $data->learningTesttrack->idTrack)
					)
					->leftJoin('learning_testquestanswer tqa',
						'tqa.idAnswer = tta.idAnswer AND tqa.idQuest = tta.idQuest')
					->where('t.idTrack = :idTrack', array(':idTrack' => $data->learningTesttrack->idTrack))
					->group('tq.idQuest')
					->order('qr.sequence ASC');

				// get user answers
				$result = $command->queryAll();

				if(!empty($result)) {
					$idTrack = $data->learningTesttrack->idTrack;
					$answeredQuestions = $data->learningTesttrack->getAnsweredQuestions();
					$answeredQuestionsIds = array();
					foreach($answeredQuestions as $question) {
						$answeredQuestionsIds[] = $question['idQuest'];
					}
					foreach ($result as $row) {
						$idQuest = $row['idQuest'];
						//leave empty if the question is not included in the test
						if(!in_array($idQuest, $answeredQuestionsIds)) {
							$values[] = ''; //user answer
							$values[] = ''; //is correct
						} else {
							$value = TestReportsGridFormatter::formatAnswerText($row['type_quest'], $row['more_info'], Docebo::nbspToSpace($row['answer']), (int)$row['is_correct'], false);
							if ($row['type_quest'] == LearningTestquest::QUESTION_TYPE_CHOICE_MULTIPLE || $row['type_quest'] == LearningTestquest::QUESTION_TYPE_ASSOCIATE) {
								$values[] = $this->widget("TestReportsGridFormatter", array(
									'userId' => $data->clearUserId,
									'questId' => $idQuest,
									'trackId' => $data->learningTesttrack->idTrack,
									'html' => false,
									'multipleAnswersDelimiter' => "\r\n"
								), true);

								if ($row['type_quest'] == LearningTestquest::QUESTION_TYPE_ASSOCIATE) {
									$answers = Yii::app()->db->createCommand()
										->select('tta.idAnswer, tta.more_info, tqa.is_correct')
										->from(LearningTesttrackAnswer::model()->tableName().' tta')
										->join(LearningTestquestanswer::model()->tableName().' tqa', 'tta.idQuest = tqa.idQuest AND tta.idAnswer = tqa.idAnswer')
										->where('tta.idTrack = :idTrack AND tqa.idQuest = :idQuest', array(
											':idTrack' => $idTrack,
											':idQuest' => $idQuest
										))
										->queryAll();
									$isCorrect = true;
									foreach($answers as $answer)
										if($answer['more_info'] != $answer['is_correct']) {
											$isCorrect = false;
											break;
										}
									$values[] = $isCorrect === true ? Yii::t('standard', '_YES') : Yii::t('standard', '_NO');
								} else {
									$isCorrect = $this->checkForCorrectAnswersMultipleChoise($idTrack, $idQuest);
									$values[] = $isCorrect === true ? Yii::t('standard', '_YES') : Yii::t('standard', '_NO');
								}
							} elseif($row['type_quest'] == LearningTestquest::QUESTION_TYPE_FITB) {
								if($row['more_info']){
									$values[] = TestReportsGridFormatter::prepareFITBTestQuestionForExport($idTrack, $idQuest);
								} elseif (empty($row['more_info'])){
									$values[] = '';
								}
								// Is the Question is Correctly answered by the user
								$values[] = TestReportsGridFormatter::prepareFITBTestQuestionForExportAsCorrectColumn($idTrack, $idQuest);
								$questions[$idQuest] = $idQuest;
							} else {

								if($row['type_quest'] == LearningTestquest::QUESTION_TYPE_TEXT_ENTRY){
									$isCorrect = (strcmp(trim(strtolower(strip_tags($row['answer']))), trim(strtolower(strip_tags($row['more_info']))))  == 0) ? true : false;
								}else{
									$isCorrect = (int)$row['is_correct'];
								}

								$values[] = $value;
								$values[] = $isCorrect == 1 ? Yii::t('standard', '_YES') : Yii::t('standard', '_NO');
							}
						}
					}
				}

				Yii::app()->event->raise('onGetCourseSingleLOExportForTestColumnValues', new DEvent($this, array(
					'values'=>&$values,
					'data'=>$data, // Current data row (CoreUser model)
				)));
    		}
    	}
		else if ($idScormItem) {
			$values[] = $data->clearUserId;
			$values[] = $data->firstname;
			$values[] = $data->lastname;
			$values[] = $data->learningScormItemsTrack->status ? $data->learningScormItemsTrack->statusLabel : LearningScormItemsTrack::getStatusValue(LearningScormItemsTrack::STATUS_NOT_ATTEMPTED);
			$values[] = $data->learningScormTracking->score_raw && $data->learningScormTracking->score_raw !== '' ? $data->learningScormItemsTrack->renderScore("") : "";
		}
		return $values;
	}

	private function checkForCorrectAnswersMultipleChoise($idTrack, $idQuest){
		$q = "SELECT COUNT(*)
			FROM `learning_testtrack` `t`
			JOIN `learning_test_quest_rel` `qr` ON qr.id_test = t.idTest
			JOIN `learning_testquest` `tq` ON qr.id_question = tq.idQuest AND tq.type_quest != 'break_page' AND tq.type_quest != 'title'
			LEFT JOIN `learning_testtrack_answer` `tta` ON tta.idTrack = t.idTrack AND tq.idQuest = tta.idQuest AND t.idTrack = $idTrack
			LEFT JOIN `learning_testquestanswer` `tqa` ON tqa.idAnswer = tta.idAnswer AND tqa.idQuest = tta.idQuest
			WHERE t.idTrack = $idTrack AND tta.user_answer=1
			AND tq.`idQuest`=$idQuest
			AND tqa.`is_correct`=0
			ORDER BY `qr`.`sequence` ASC";
		$countOfWrongUserAnswers = Yii::app()->db->createCommand($q)->queryScalar();
		$q = "SELECT COUNT(*)
			FROM `learning_testtrack` `t`
			JOIN `learning_test_quest_rel` `qr` ON qr.id_test = t.idTest
			JOIN `learning_testquest` `tq` ON qr.id_question = tq.idQuest AND tq.type_quest != 'break_page' AND tq.type_quest != 'title'
			LEFT JOIN `learning_testtrack_answer` `tta` ON tta.idTrack = t.idTrack AND tq.idQuest = tta.idQuest AND t.idTrack = $idTrack
			LEFT JOIN `learning_testquestanswer` `tqa` ON tqa.idAnswer = tta.idAnswer AND tqa.idQuest = tta.idQuest
			WHERE t.idTrack = $idTrack AND tta.user_answer=1
			AND tq.`idQuest`=$idQuest
			AND tqa.`is_correct`=1
			ORDER BY `qr`.`sequence` ASC";
		$countOfCorrectUserAnswers = Yii::app()->db->createCommand($q)->queryScalar();
		$q = "SELECT COUNT(*) FROM
			`learning_testquest` tq
			JOIN `learning_testquestanswer` tqa
			ON tq.`idQuest`=tqa.`idQuest`
			WHERE tq.`idQuest`=$idQuest
			AND tqa.`is_correct`=1";
		$countOfTotalCorrectAnswers = Yii::app()->db->createCommand($q)->queryScalar();
		if($countOfCorrectUserAnswers == $countOfTotalCorrectAnswers && $countOfWrongUserAnswers == 0) {
			return true;
		}
		return false;
	}

    /**
     * Return SQL data provider for Course -> Reports -> USERS STATS, for a given course
     *
     * @return CSqlDataProvider
     */
    protected function sqlDataProviderCourseReportUsersStats($idCourse, $search=false, $withTracksession=false) {
		$event = new DEvent($this, array('idCourse' => $idCourse, 'search' => $search, 'withTracksession' => $withTracksession));
		Yii::app()->event->raise('BeforeSqlDataProviderCourseReportUsersStats', $event);
		if(!$event->shouldPerformAsDefault() && $event->return_value['dataProvider'])
			return $event->return_value['dataProvider'];

    	// Start building SQL parameters array
    	$params = array();
    	$params[':idCourse'] = $idCourse;


    	/************* BASE *******************/
         $pUserRights = Yii::app()->user->checkPURights($idCourse);

    	$commandBase = Yii::app()->db->createCommand()
    		->from('learning_courseuser enrollment')
    		->join('core_user user', 'user.idst=enrollment.idUser')
    		->where('enrollment.idCourse=:idCourse');


    	// Power User filtering
    	if ($pUserRights->isPu && !$pUserRights->isInstructor) {
    		$commandBase->join(CoreUserPU::model()->tableName()." pu", "pu.puser_id = " . Yii::app()->user->id . " AND enrollment.idUser = pu.user_id");
    		$commandBase->join(CoreUserPuCourse::model()->tableName()." puc", "puc.puser_id = "  . Yii::app()->user->id .  " AND puc.course_id = :idCourse");
    	}

		//PU instructor OR user instructor
		if(!Yii::app()->user->getIsGodadmin() && LearningCourseuser::isUserLevel(Yii::app()->user->id, $idCourse, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR)) {
			$instructorUsers = LearningCourse::getInstructorUsers($idCourse);
			if(!empty($instructorUsers))
				$commandBase->andWhere('enrollment.idUser IN ('.implode(',', $instructorUsers).')');
		}

    	// Search for some text ?
    	if (is_string($search)) {
    		$commandBase->andWhere('CONCAT(user.firstname, user.lastname, user.email, user.userid) LIKE :search');
    		$params[':search'] = '%' . $search . '%';
    	}

    	// Also collect learning tracksession statistics for every user
    	if ($withTracksession) {
    		$commandBase->leftJoin(
    				'(
				 SELECT SUM(UNIX_TIMESTAMP(lts.lastTime) - UNIX_TIMESTAMP(lts.enterTime)) AS sessionTime, lts.idUser as idUserInner
				 FROM learning_tracksession lts
				 WHERE (lts.idCourse=:idCourse) AND lts.lastTime != "0000-00-00 00:00:00" AND lts.lastTime IS NOT NULL AND lts.enterTime != "0000-00-00 00:00:00" AND lts.enterTime IS NOT NULL
				 GROUP BY lts.idUser
				 ) trackSession',
    				'trackSession.idUserInner=enrollment.idUser'
    		);
    	}


    	/************* COUNTER *******************/

    	$commandCounter = clone $commandBase;
    	$commandCounter->params = $params;
    	$commandCounter->select('count(*) as c');
    	$numRecords = $commandCounter->queryScalar();


    	/************* DATA *******************/

    	$commandData = clone $commandBase;
    	$commandData->params = $params;
    	$commandData->select('*');


    	/************* DATA PROVIDER *******************/

    	// Data provider config
    	$config = array(
    			'params' => $params,
    			'totalItemCount'=>$numRecords,
    			'pagination' => array(
    					'pageSize' => Settings::get('elements_per_page', 10)
    			),
				'sort' => array(
					'attributes'=>array(
						'userid',
						'date_inscr',
						'date_first_access',
						'date_complete',
						'date_last_access',
						'sessionTime',
						'status'
					)
				)
    	);

    	// Create data provider and return to caller
    	$dataProvider = new CSqlDataProvider($commandData->getText(), $config);

    	return $dataProvider;

    }



    /**
     * Build Report Columns array for:  Course -> Reports -> USERS STATS, for a given course
     *
     * @param integer $idCourse
     * @param boolean $textOnly
     */
	protected function columnsCourseReportUsersStats($idCourse, $textOnly=false) {
		$event = new DEvent($this, array('idCourse' => $idCourse, 'textOnly' => $textOnly));
		Yii::app()->event->raise('BeforeColumnsCourseReportUsersStats', $event);
		if(!$event->shouldPerformAsDefault() && is_array($event->return_value['columns']))
			return $event->return_value['columns'];
		$course = LearningCourse::model()->findByPk($idCourse);

		$columns = array (
			array (
				'name' => 'userid',
				'header' => Yii::t ( 'standard', '_USERNAME' ),
				'value' =>
					$textOnly ?
						'trim($data["userid"],"/")'
						:
						'CHtml::link(trim($data["userid"],"/"), array("byUser", "course_id"=>$data["idCourse"], "user_id"=>$data["idst"]), array("class"=>"grid-link"))',
				'type' => 'raw'
			),
			array (
				'name' => 'date_inscr',
				'header' => Yii::t ( 'report', '_DATE_INSCR' ),
				'value' => '$data["date_inscr"] ? Yii::app()->localtime->toLocalDateTime($data["date_inscr"]) : "-"',
				'htmlOptions' => array (
					'class' => 'text-center'
				),
				'headerHtmlOptions' => array (
					'class' => 'text-center'
				)
			),
			array (
				'name' => 'date_first_access',
				'header' => Yii::t ( 'standard', '_DATE_FIRST_ACCESS' ),
				'value' => '$data["date_first_access"] ? Yii::app()->localtime->toLocalDateTime($data["date_first_access"]) : "-"',
				'htmlOptions' => array (
					'class' => 'text-center'
				),
				'headerHtmlOptions' => array (
					'class' => 'text-center'
				)
			),
			array (
				'name' => 'date_complete',
				'header' => Yii::t ( 'report', '_DATE_COURSE_COMPLETED' ),
				'value' => '$data["date_complete"] ? Yii::app()->localtime->toLocalDateTime($data["date_complete"]) : "-"',
				'htmlOptions' => array (
					'class' => 'text-center'
				),
				'headerHtmlOptions' => array (
					'class' => 'text-center'
				)
			),
			array (
				'name' => 'date_last_access',
				'header' => Yii::t ( 'standard', '_DATE_LAST_ACCESS' ),
				'value' => '$data["date_last_access"] ? Yii::app()->localtime->toLocalDateTime($data["date_last_access"]) : "-"',
				'htmlOptions' => array (
					'class' => 'text-center'
					),
				'headerHtmlOptions' => array (
					'class' => 'text-center'
				)
			),
			array (
				'name' => Yii::t ( 'standard', '_PROGRESS' ),
				'value' => 'LearningOrganization::getProgressByUserAndCourse(' . $idCourse . ', $data["idst"])."%"',
				'htmlOptions' => array (
					'class' => 'text-center'
				),
				'headerHtmlOptions' => array (
					'class' => 'text-center'
				)
			));
		if(!$textOnly && $course->course_type == LearningCourse::TYPE_ELEARNING && !$course->hasEndObjectLo()) {
			$columns[] = array (
				'name' => '',
				'value' => array($this, 'reportGridRenderLoAddedAfterCourseCompletion'),
				'type' => 'raw',
				'htmlOptions' => array (
					'class' => 'text-center'
				),
				'headerHtmlOptions' => array (
					'class' => 'text-center'
				)
			);
		}
		$columns[] = array (
				'name' => 'sessionTime',
				'header' => Yii::t ( 'course', '_PARTIAL_TIME' ),
				'value' => '$data["sessionTime"] ? Yii::app()->localtime->hoursAndMinutes($data["sessionTime"]) : "-"',
				'htmlOptions' => array (
					'class' => 'text-center'
				),
				'headerHtmlOptions' => array (
					'class' => 'text-center'
				)
			);
		$columns[] = array (
				'name' => 'status',
				'header' => Yii::t ( 'standard', '_STATUS' ),
				'value' => 'LearningCourseuser::getStatusLabelColored($data["status"], ' . ($textOnly ? 'true' : 'false') . ')',
				'type' => 'raw',
				'htmlOptions' => array (
					'class' => 'text-center'
				),
				'headerHtmlOptions' => array (
					'class' => 'text-center'
				)
			);
		$columns[] = array (
				'name' => Yii::t ( 'standard', '_SCORE' ),
				'value' => 'LearningCourseuser::getLastScoreByUserAndCourse(' . $idCourse . ', $data["idst"])',
				'htmlOptions' => array (
					'class' => 'text-center'
				),
				'headerHtmlOptions' => array (
					'class' => 'text-center'
				)
			);

		return $columns;

	}

    /**
     * Checks if the current user (Instructor) is an Admin to the session user in webinar or classroom.
     * @return boolean
     * TRUE = The current Instructor is an admin to the user given in the current session.
     * FALSE = The current Instructor is not an admmin to the user given in the current session.
     */
    public function getReportUserIsEnrolledInInstructorsSession() {
        // getting the course_id the instructor is at and the user's id
        $request = Yii::app()->request;
        $course_id = $request->getParam('course_id', 0);
        $user_id = $request->getParam('user_id', 0);

        // getting the type of the course
        $cmd = Yii::app()->db->createCommand()
                ->select('course_type')
                ->from('learning_course as c')
                ->where('c.idCourse = :idCourse')
                ->bindParam(':idCourse', $course_id);

        $courseType = $cmd->queryRow();
        $courseType = $courseType['course_type'];

        switch($courseType) {
            case 'webinar':
                $table = 'webinar_session';
                $table_user = 'webinar_session_user';
                break;
            case 'classroom':
                $table = 'lt_course_session';
                $table_user = 'lt_courseuser_session';
                break;
            default:
                return true;
        }

        // getting the sessions and users of the sessions of the current course
        $cmd = Yii::app()->db->createCommand()
                ->select('wsu.id_session, wsu.id_user')
                ->from("{$table} AS ws")
                ->leftJoin("{$table_user} AS wsu", 'wsu.id_session = ws.id_session')
                ->where('ws.course_id = :course_id');

        $cmdParams = array(
            ':course_id' => $course_id
        );

        $sessions_users = $cmd->queryAll(true, $cmdParams);
        $session_users = array();

        // Creating multidimensional array as the key will be the id_session and the values are the users ids.
        foreach($sessions_users as $row) {
            if(!isset($session_users[$row['id_session']])) {
                $session_users[$row['id_session']] = array();
            }
            $session_users[$row['id_session']][] = $row['id_user'];
            $session_users[$row['id_session']] = array_unique($session_users[$row['id_session']]);
        }

        // checking if the current user from the report by_user is in session with the current instructor evaluating
        foreach($session_users as $key => $session) {
            // looping thru all the sessions
            if(in_array(Yii::app()->user->getId(), $session) && in_array($user_id, $session)) {
                return true;
            }
        }
        // else the instructor has no right to edit the user.
        return false;
    }

    /**
     * Update/Edit User->LO failed attempts (taken and allowed).
     * Called from Course -> Reports -> User LO objects, ATTEMPTS column
     * 
     * @throws CException
     */
    public function actionAxEditAttempts(){
        
        $rq             = Yii::app()->request;
        $idObject       = (int)$rq->getParam('object_id', 0);
        $idUser         = (int)$rq->getParam('user_id', 0);
        $newValue       = $rq->getParam('value', 0);
        $attemptsType   = $rq->getParam('attempts_type', 0);
        
        $response = new AjaxResult();

        // Reject NON-integers or negative values
        if ((strval(intval($newValue)) !== strval($newValue)) || (intval($newValue) < 0)) {
            $response->setStatus(false)->setMessage(Yii::t('standard', 'Bad value'))->setData($data)->toJSON();
            Yii::app()->end();
        }
        $newValue = intval($newValue);
        
        
        $transaction = Yii::app()->db->beginTransaction();

        // Permissions check
        $pUserRights = Yii::app()->user->checkPURights(Yii::app()->request->getParam('course_id', 0));
        if(!$this->getReportUserIsEnrolledInInstructorsSession() && $pUserRights->isInstructor && !$pUserRights->all && !Yii::app()->user->getIsGodAdmin()) {
            $response->setStatus(FALSE)->setMessage(Yii::t('error', "Permission denied. You cannot change the {param} of this user.", array('{param}' => Yii::t('report', 'attempts'))));
            $response->toJSON();
            Yii::app()->end();
        }
        
        try {
            
            $userModel = CoreUser::model()->findByPk($idUser);
            if (!$userModel) { throw new CException(Yii::t('error', 'Invalid specified user')); }
        
            /* @var $object LearningOrganization */
            $object = LearningOrganization::model()->findByPk($idObject);
            if (!$object) { throw new CException(Yii::t('error', 'Invalid specified learning object')); }
        
            // Check if this is a central LO object with shared tracking
            // and update the LO id to point to the master
            $object = $object->getMasterForCentralLo($idUser);
            $idObject = $object->idOrg;
        
            // Retrive tracking info records
            $track = LearningCommontrack::model()->findByAttributes(array('idReference' => $idObject, 'idUser' => $idUser));

            if (!$track) { 
                throw new CException(Yii::t('error', 'Error while updating tracking status'));
            }
        
            switch ($attemptsType) {
                case 'taken':
                    $track->failed_attempts_taken = $newValue;
                    break;
                case 'allowed':
                    $track->failed_attempts_allowed = $newValue;
                    break;
                    
                default:
                    throw new CException(Yii::t('error', 'Invalid attempts type'));
                    break;
            }
            
            $rs = $track->save();
            if (!$rs) { throw new CException(Yii::t('error', 'Error while updating tracking status')); }
        
            $transaction->commit();
            
            $data = array();
            $response->setStatus(true)->setData($data)->toJSON();
            
            Yii::app()->end();
        
        } catch(CException $e) {
            $transaction->rollback();
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
            $response->setStatus(FALSE)->setMessage($e->getMessage());
        }
        
        $response->toJSON();
        
        
    }
    

}