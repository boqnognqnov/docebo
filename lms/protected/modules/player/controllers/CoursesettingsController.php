<?php

class CoursesettingsController extends PlayerBaseController
{

	public function beforeAction($action)
	{
        $this->breadcrumbs = array();

        PlayerBaseController::breadcrumbUserSubscription($this);
        if (isset($_GET['ref_src']) && $_GET['ref_src'] == 'admin') {
            $this->breadcrumbs[Yii::t('menu', 'Admin')] = Docebo::createAppUrl('admin:dashboard/index');
            $this->breadcrumbs[Yii::t('standard', '_COURSES')] = Docebo::createAppUrl('admin:courseManagement/index');
        } else {
            $this->breadcrumbs[Yii::t('menu_over', '_MYCOURSES')] = Docebo::createLmsUrl('site/index', array('opt' => 'fullmycourses'));
        }
        
        
        Yii::app()->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery-ui-i18n.js');

		return parent::beforeAction($action);
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules()
	{
		return array(

			array('allow',
					'users'=>array('@'),
					'expression' => 'Yii::app()->user->isGodAdmin || Yii::app()->controller->userCanAdminCourse ||
								LearningCourseuser::userLevel($user->id, ' . $this->course_id . ') == LearningCourseuser::$USER_SUBSCR_LEVEL_ADMIN',
			),

			array('deny', // if some permission is wrong -> the script goes here
					'users' => array('*'),
					'deniedCallback' => array($this, 'accessDeniedCallback'),
					'message' => Yii::t('course', '_NOENTER'),
			),
		);
	}

	/**
	 * General tab
	 */
	public function actionIndex()
	{
		Yii::app()->tinymce->init();
		DatePickerHelper::registerAssets();

		$courseModel = LearningCourse::model()->findByPk($this->course_id);
		$courseModel->scenario = 'course_setting';

		$isInstructor = false;
		$event = new DEvent($this, array('courseId' => $this->course_id));
		Yii::app()->event->raise('CheckIfUserIsInstructor', $event);
		if (!$event->shouldPerformAsDefault()) {
			$isInstructor = true;
		}

		if(!$isInstructor && Yii::app()->user->getIsPu() && !CoreUserPuCourse::isCourseAssignedToPU(Yii::app()->user->id, $this->course_id)){

			// This course is not assigned to the PU
			throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
		}

		if (isset($_POST['LearningCourse'])) {

			// check if the LO fields are empty and the selected mode requires to be not empty - do not allow to continue and return with an error...
			if (($_POST['LearningCourse']['initial_score_mode'] == LearningCourse::INITIAL_SCORE_TYPE_KEY_OBJECT && !isset($_POST['LearningCourse']['initial_object']))
				|| ($_POST['LearningCourse']['final_score_mode'] == LearningCourse::FINAL_SCORE_TYPE_KEY_OBJECT && !isset($_POST['LearningCourse']['final_object']))){

				Yii::app()->user->setFlash('error', Yii::t('course','You must select one training material from the list'));
				$this->refresh(true, (isset($_POST['current_tab']) ? '#' . $_POST['current_tab'] : ''));
			}
			if (PluginManager::isPluginActive('PowerUserApp')) {
				//read category id before assigning new attributes
				$oldIdCategory = $courseModel->idCategory;
			}

			$price = $_POST['LearningCourse']['prize'];

			if(isset($price)){
				
				$price = str_replace(',', '.', $price);
				$intPrice = floatval($price);

				if($intPrice > 0){
					$_POST['LearningCourse']['prize'] = $intPrice;
				}else{
					$_POST['LearningCourse']['prize'] = '';
				}
			}

			if(!isset($_POST['enable_soft_deadline_option'])) {
				$_POST['LearningCourse']['soft_deadline'] = null;
			}

			if(!isset($_POST['enable_has_overview']))
				$_POST['LearningCourse']['has_overview'] = null;

			if(!isset($_POST['enable_show_toc']))
				$_POST['LearningCourse']['show_toc'] = null;

			$newMaxNumSubscribe = $_POST['LearningCourse']['max_num_subscribe'];
			$oldMaxNumSubscribe=$courseModel->max_num_subscribe;

			$courseModel->setAttributes($_POST['LearningCourse']);

			if ($courseModel->validate()) {

				if (isset($_POST['LearningCourse']['additional'])) {

					$fieldModel = LearningCourseFieldValue::model()->findByPk($courseModel->idCourse);

					/**
					 * @var $fieldModel LearningCourseFieldValue					 
					*/
					if(!$fieldModel){

						$fieldModel = new LearningCourseFieldValue();

						$fieldModel->id_course = $courseModel->idCourse;

						$fieldModel->save();
					}

					$fields = $_POST['LearningCourse']['additional'];

					//Event for iFrame course field
					Yii::app()->event->raise('BeforeSaveCourseAdditionalFields', new DEvent($this, array('fields' => &$fields, 'courseModel' => $courseModel)));									
					
					foreach($fields as $id => $field){											
						
						$fieldValue = $field['value'];						
						switch ($field['type']) {
							case 'date':
								$fieldValue = ($fieldValue) ? Yii::app()->localtime->fromLocalDate($fieldValue) : null;								
								break;
						}
						$sql = "UPDATE " . LearningCourseFieldValue::model()->tableName() . " SET field_" . $id . " = :data WHERE id_course = :course";
						$parameters = array(
								':data' => $fieldValue,
								':course' => $courseModel->idCourse
						);
						Yii::app()->db->createCommand($sql)->execute($parameters);
					}
				}

				//manage custom sharing options
				if ($_POST['LearningCourse']['social_sharing_custom'] == 1) {
					$sharing = array();
					$sharing['course_sharing_permission'] = $courseModel->course_sharing_permission;
					$sharing['course_user_share_permission'] = $courseModel->course_user_share_permission;
					$sharing['course_share_facebook'] = $courseModel->course_share_facebook;
					$sharing['course_share_twitter'] = $courseModel->course_share_twitter;
					$sharing['course_share_linkedin'] = $courseModel->course_share_linkedin;
					$sharing['course_share_google'] = $courseModel->course_share_google;
					$courseModel->social_sharing_settings = CJSON::encode($sharing);
				} else {
					$courseModel->social_sharing_settings = NULL;
				}

				//manage custom rating options
				if ($_POST['LearningCourse']['social_rating_custom'] == 1) {
					$rating = array();
					$rating['course_rating_permission'] = $courseModel->course_rating_permission;
					$courseModel->social_rating_settings = CJSON::encode($rating);
				} else {
					$courseModel->social_rating_settings = NULL;
				}

				if ($courseModel->inherit_quota) {
					$courseModel->course_quota = LearningCourse::COURSE_QUOTA_INHERIT;
				}

				if ($courseModel->random_autoregistration_code) {
					$courseModel->autoregistration_code = $courseModel->generateAutoRegCode();
				}

				LearningLabelCourse::saveLabelsToCourse($courseModel->labels, $courseModel->primaryKey);
				$courseModel->level_show_user = Mask::createMask($courseModel->userLevels);
				$courseModel->userStatusOp = Mask::createMask($courseModel->userStatuses);

				// This should not be called here (or anywhere for that matter), as it deals with uploaded logo, which is not
				// a subject of this UI
				// $courseModel->manageLogo();

				$courseModel->manageCourseDemo();

				if (!isset($_POST['custom_player_layout_enabled'])) {
					$courseModel->player_layout = null;
				}

				if (!isset($_POST['enable_course_bg_image'])) {
					$courseModel->player_bg_aspect = null;
					$courseModel->player_bg = null;
				} else {
					if (!empty($_POST['LearningCourse']['player_bg_id'])) {
						$courseModel->player_bg = (int)$_POST['LearningCourse']['player_bg_id'];
					}
					if (!empty($_POST['LearningCourse[player_bg_aspect]'])) {
						$courseModel->player_bg_aspect = $_POST['LearningCourse[player_bg_aspect]'];
					}
				}
//				return CVarDumper::dump($courseModel,10,true);

				$courseModel->save(false);


				// if admin is increases the max number of subscribe users, automatically enroll users from waiting list
				if ($oldMaxNumSubscribe < $newMaxNumSubscribe && $courseModel->selling == 0 && $courseModel->course_type == LearningCourse::TYPE_ELEARNING) {
					$courseModel->automaticallyEnroll(LearningCourse::TYPE_ELEARNING, null, $newMaxNumSubscribe - $oldMaxNumSubscribe);
				}

				// If we are passed channels, lets assign this course to them
				if(!empty($_POST['LearningCourse']['channels']) && is_array($_POST['LearningCourse']['channels'])) {
					// Remove all channel assignments for this course
					App7020ChannelAssets::model()->deleteAllByAttributes(array('idAsset' => $courseModel->idCourse, 'asset_type' => App7020ChannelAssets::ASSET_TYPE_COURSES));
					App7020ChannelAssets::massAppendChannelToAnAsset($_POST['LearningCourse']['channels'], $courseModel->idCourse, App7020ChannelAssets::ASSET_TYPE_COURSES);
				} else {
					App7020ChannelAssets::model()->deleteAllByAttributes(array('idAsset' => $courseModel->idCourse, 'asset_type' => App7020ChannelAssets::ASSET_TYPE_COURSES));
				}


				if (PluginManager::isPluginActive('PowerUserApp')) {
					if ($oldIdCategory != $courseModel->idCategory) {
						//we need to remove old course reference in old category
						CoreUserPuCourse::model()->updateCourseCategory($oldIdCategory, $this->course_id);
						LearningCourseCategory::flushTreeCache();
					}
				}

				//update all enrollments expire dates where users already started this course
				if (isset($_POST['update_enrollments_expire_dates']) && $_POST['update_enrollments_expire_dates']) {
					if (intval($courseModel->valid_time) > 0) {
						//check for selected option in Days of validity 'after first course access' of 'after being enrolled in the course'
						$field = ($courseModel->valid_time_type == LearningCourse::VALID_TIME_TYPE_FIRST_COURSE_ACCESS) ? 'date_first_access' : 'date_inscr';
						LearningCourseuser::model()->updateAll(
							array('date_expire_validity' => new CDbExpression('ADDDATE(' . $field . ', ' . intval($courseModel->valid_time) . ')'),
								  'date_begin_validity' => new CDbExpression($field)),
							'idCourse = :idCourse',
							array(":idCourse" => $courseModel->idCourse)
						);
					} else {
						LearningCourseuser::model()->updateAll(
							array('date_expire_validity' => null, 'date_begin_validity' => null),
							'idCourse = :idCourse',
							array(":idCourse" => $courseModel->idCourse)
						);
					}
				}

				// save assigned certificate
				LearningCertificate::assignToCourse($courseModel->assigned_certificate, $courseModel->primaryKey);

				if (PluginManager::isPluginActive("CertificationApp")) {
					// Save Certification association (retraining)
					CertificationItem::assignItemToCertification(CertificationItem::TYPE_COURSE, $courseModel->primaryKey, $courseModel->certification);
				}

				$event =
					new DEvent(
						$this,
						array(
							'currentTab' => $_POST['current_tab'],
							'enabledCustomSettings' => intval($_POST['enabledCustomSettings']),
							'course' => $courseModel,
						)
					);
				Yii::app()->event->raise('SaveCustomSettingsFields', $event);

				$event = new DEvent($this, array('idCourse' => $courseModel->idCourse));
				Yii::app()->event->raise('SaveChoiseIsCodesAreRequired', $event);

				// Event raised for YnY app to hook on to
				Yii::app()->event->raise('CourseSettingsCustomSave', new DEvent($this, array('courseModel' => $courseModel, 'data' => $_POST)));

				// we have change in course equivalencies, so apply it
				if(isset($_POST['equivalent-courses-data'])){
					$this->manageEquivalencyChanges($courseModel);
				}

				$this->refresh(true, (isset($_POST['current_tab']) ? '#' . $_POST['current_tab'] : ''));
			}else{
                $this->refresh(true, (isset($_POST['current_tab']) ? '#' . $_POST['current_tab'] : ''));
            }
		} else {
			if ($courseModel->course_quota == LearningCourse::COURSE_QUOTA_INHERIT) {
				$courseModel->course_quota = '';
				$courseModel->inherit_quota = true;
			}
			if ($courseModel->hour_begin == '-1')
				$courseModel->hour_begin = '';

			if ($courseModel->hour_end == '-1')
				$courseModel->hour_end = '';

			$courseModel->labels = CHtml::listData($courseModel->learningLabels, 'id_common_label', 'id_common_label');
			$courseModel->userLevels = Mask::matchMask($courseModel->level_show_user, array_keys(LearningCourseuser::getLevelsArray()));
			$courseModel->userStatuses = Mask::matchMask($courseModel->userStatusOp, array_keys(LearningCourseuser::getStatusesArray()));
		}

		$certificates = array();
		$criteria = new CDbCriteria();
		$criteria->select = 'id_certificate, name';
		$all_certificates = LearningCertificate::model()->findAll($criteria);
		$certificates[0] = Yii::t('standard', '_NONE');
		foreach ($all_certificates as $key => $certificate) {
			$certificates[$certificate->id_certificate] = $certificate->name;
		}

		$cs = Yii::app()->clientScript;
		$am = Yii::app()->assetManager;
		$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.blockUI.js');
		$cs->registerScriptFile($am->publish(Yii::getPathOfAlias('admin.js')).'/coaching.js');
		$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/clipboard.min.js');
		
		
		
		// Accreditation IFRAME custom field JS helper (TR)
		$cs->registerScriptFile(Yii::app()->theme->baseUrl . "/js/accreditation_iframe.js", CClientScript::POS_BEGIN);
		

		$deepLink = $courseModel->getDeepLink();

		// Calculate visibility for Publish/Unpublish toggle
		$pUserRights = Yii::app()->user->checkPURights($courseModel->idCourse);
		$admin_has_course = false;
		if (Yii::app()->user->isAdmin) {
			$hasCourse = CoreUserPuCourse::model()->findByAttributes(array(
				'puser_id' => Yii::app()->user->id,
				'course_id' => $courseModel->idCourse
			));
			$admin_has_course = !empty($hasCourse);
		}
		$canTogglePublishStatus = (Yii::app()->user->isGodadmin || $pUserRights->all || ($admin_has_course && Yii::app()->user->checkAccess('/lms/admin/course/mod')));

		// delete session variable, if this is normal page load - we don't need the temp data for selected,
		// but not confirmed course equivalencies on page refresh of first visit
		if(!Yii::app()->request->isAjaxRequest) {
			$_SESSION['selectedEquivalentCoursesIds'] = array();
			$_SESSION['selectedEquivalentCoursesIdsForDeletion'] = array();
		}

		$this->render('index', array(
			'canTogglePublishStatus' => $canTogglePublishStatus,
			'courseModel' => $courseModel,
			'certificates' => $certificates,
			'deepLink'     => $deepLink,
			'showLabelsSelector' => PluginManager::isPluginActive('LabelApp'),
			'showChannelsSelector' => true,
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see PlayerBaseController::registerResources()
	 */
	public function registerResources() {
		parent::registerResources();

		if (!Yii::app()->request->isAjaxRequest) {
			Yii::app()->clientScript->registerScriptFile($this->module->assetsUrl . '/js/coursesettings.js');
			Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/chosen.jquery.min.js');
			Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.timepicker.js');
			Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/chosen.css');
			Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/jquery.timepicker.css');
			Yii::app()->clientScript->registerCoreScript('jquery.ui');

			$cs = Yii::app()->getClientScript();
			$themeUrl = Yii::app()->theme->baseUrl;

			// FCBK
			$cs->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
			$cs->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');
		}

	}

	
	
	/**
	 * Show modal dialog to upload/change Per Course background image 
	 */
	public function actionAxPlayerBg() {
		
		// SAVE button
		$confirm_save = Yii::app()->request->getParam("confirm_save", false);
		
		// Delete requested
		$delete_image = Yii::app()->request->getParam("delete_image", false);
		
		
		$transaction = null;
		if (Yii::app()->db->getCurrentTransaction() === NULL) { $transaction = Yii::app()->db->beginTransaction(); }
		
		// Wrap everything in try/catch and provide DB transaction commit/rollback
		try {
			
			$courseModel = LearningCourse::model()->findByPk($this->course_id);
			
			// No course model ? Impossible, but still happens :-)
			if (!$courseModel) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}
			
			$courseModel->scenario = 'course_setting';
				
			// If user selected an image to upload, automatic AJAX form submition is executed
			// Lets get the file and save it. No cropping functionality.
			// Also, it must be NOT due 'Save Changes'!
			if (!empty($_FILES["image_manage_file"]) && !$confirm_save && !$delete_image) {
				
				// Get hthe uploaded file
				$uploadedFile 	= CUploadedFile::getInstanceByName("image_manage_file");
				$filePath      	= $uploadedFile->getTempName();
				$tmpFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $uploadedFile->getName();
				copy($filePath, $tmpFilePath);
				
				$asset = new CoreAsset();
				$asset->sourceFile = $tmpFilePath;
				$asset->type = CoreAsset::TYPE_PLAYER_BACKGROUND;
				
				
				// Validate!! && Save
				$ok = $asset->save();
				if (!$ok) {
				    Yii::log(print_r($asset->getErrors(),true), 'error');
					throw new CException(Yii::t("standard", '_OPERATION_FAILURE'));
				}
				
				// Commit
				if ($transaction) $transaction->commit();
				
				$ajaxResult = new AjaxResult();
				
				$data = array(
						'image_id' => $asset->id,
						'image_url' => $asset->getUrl(CoreAsset::VARIANT_SMALL),
				);
				
				$ajaxResult->setStatus(true)->setData($data)->toJSON();

				// Clean up
				FileHelper::removeFile($tmpFilePath);
				
				Yii::app()->end();
			}

			
			// DELETE AJAX call
			if ($delete_image) {
				$imageId = Yii::app()->request->getParam("image_id", false);
				$success = false;
				if ((int) $imageId > 0) {
					$asset = CoreAsset::model()->findByPk($imageId);
					if ($asset) {
						$success = $asset->delete();
					}
				}
				$ajaxResult = new AjaxResult();
				$data = array();
				$ajaxResult->setStatus($success)->setData($data)->toJSON();
				if ($transaction) $transaction->commit();
				Yii::app()->end();
			}

			
			// SAVE CHANGES button is clicked in Player background selection dialog
			if ($confirm_save) {
				$selectedImageId = Yii::app()->request->getParam("image_id_radio", false);
				if ($selectedImageId) {
					$assetModel = CoreAsset::model()->findByPk($selectedImageId);
					$src = $assetModel->getUrl(CoreAsset::VARIANT_SMALL);
					$ajaxResult = new AjaxResult();
					$data = array(
							'image_id' => $selectedImageId,
							'image_url' => $src,
					);
					$ajaxResult->setStatus(true)->setData($data)->toJSON();
				}
				Yii::app()->end();
			}
			
			
			if ($transaction) $transaction->commit();
			
		}
		catch (CException $e) {
			// Clean up
			FileHelper::removeFile($tmpFilePath);
			// Log error
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			// Rollback 
			if ($transaction) $transaction->rollback();
			
			// Send result to caller
			$ajaxResult = new AjaxResult();
			$ajaxResult->setStatus(false)->setMessage($e->getMessage())->toJSON();
			
			Yii::app()->end();
		}
		
		// We end up here on first load 
		$chunkSize = 6;

		// Get list of URLs of SHARED and USER player backgrounds
		$assets = new CoreAsset();
		$systemImages   = $assets->urlList(CoreAsset::TYPE_PLAYER_BACKGROUND, CoreAsset::SCOPE_SHARED, 6, CoreAsset::VARIANT_SMALL);
		$userImages   	= $assets->urlList(CoreAsset::TYPE_PLAYER_BACKGROUND, CoreAsset::SCOPE_PRIVATE, 6, CoreAsset::VARIANT_SMALL);
		
		$html = $this->renderPartial('_modal_player_bg', array(
				'courseModel'	=> $courseModel,
				'systemImages' 	=> $systemImages,
				'userImages'	=> $userImages,
				'chunkSize'		=> $chunkSize
		),true,true);
		
		// Echo HTML to Dialog2
		echo $html;

	}

	public function actionDelCourseDemoMaterial(){

		$result = array('success'=>false);

		if($this->courseModel->course_demo){
			$demoFileName = $this->courseModel->course_demo;
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_COURSEDOCS);
			if($storageManager->remove($demoFileName)){
				$this->courseModel->course_demo = null;
				$this->courseModel->save();
				$result['success'] = true;
			}
		}

		echo CJSON::encode($result);
	}

	/**
	 * Determines the set of field name/values to display in the settings page
	 * @param $courseModel LearningCourse
	 * @return array
	 */
	public function getAdditionalFieldsToDisplay($courseModel) {
		$criteria = new CDbCriteria();
		$criteria->order = 'sequence ASC';
		$additionalFields = LearningCourseField::model()->findAll($criteria);
		$courseFields = array();
		foreach ($additionalFields as $field) {
			$fieldClass = 'CourseField' . ucfirst($field->type);
			$courseField = $fieldClass::model()->findByPk($field->id_field);
			$tempValue = Yii::app()->getDb()->createCommand()
				->select('field_'.$field->id_field)
				->from(LearningCourseFieldValue::model()->tableName().' v')
				->where('v.id_course=:id_course', array(':id_course' => $courseModel->idCourse))
				->queryScalar();
			$courseField->courseEntry = $tempValue;
			$courseField->course = $courseModel;
			$courseFields[] = $courseField;
		}

		return $courseFields;
	}

	public function actionFcbkAutocomplete(){
		if(Yii::app()->request->isAjaxRequest) {
			$searchInput = Yii::app()->request->getParam('tag', null);
			if (!$searchInput) {
				return;
			}

			$courseModel = LearningCourse::model()->findByPk((int)Yii::app()->request->getParam('course_id'));
			if (!$courseModel) {
				return;
			}

			$command = Yii::app()->db->createCommand()
				->select('idCourse, name, code')
				->from(LearningCourse::model()->tableName())
					->where('`name` LIKE :search OR `code` LIKE :search');
			$command->params = array(':search'=>"%$searchInput%");
			$resultRaw = $command->queryAll();
			$alreadyEquivalentCoursesIds = $courseModel->getEquivalentCoursesIds();

			// add current course as an exclusion
			$alreadyEquivalentCoursesIds[] = $courseModel->idCourse;

			$alreadyEquivalentCoursesIds = array_diff($alreadyEquivalentCoursesIds, $_SESSION['selectedEquivalentCoursesIdsForDeletion']);
			$result = [];
			foreach ($resultRaw as $item) {
				if (!in_array($item['idCourse'], $alreadyEquivalentCoursesIds))
					$result[] = array(
						'key' => $item['idCourse'],
						'value' => ((strpos($item['name'],$searchInput) !== false)?$item['name'] : $item['code']),
					);
			}
			echo CJSON::encode($result);
		}
	}

	public function actionAddCourseEquivalents(){
		$selectedCoursesIds = $_POST['selectedCoursesIds'];
		$_SESSION['selectedEquivalentCoursesIds'] = array_merge($selectedCoursesIds, $_SESSION['selectedEquivalentCoursesIds']);
		echo CJSON::encode($_SESSION['selectedEquivalentCoursesIds']);
	}

	public function actionDeleteCourseEquivalent(){
		if(Yii::app()->request->isAjaxRequest) {
			$courseId = (int)$_POST['courseRemoveId'];
			if (!empty($courseId)) {
				$markForDeletion = true;
				if(isset($_SESSION['selectedEquivalentCoursesIds']) && !empty($_SESSION['selectedEquivalentCoursesIds'])) {
					$foundIndex = array_search($courseId, $_SESSION['selectedEquivalentCoursesIds']);
					if ($foundIndex !== false) {
						unset($_SESSION['selectedEquivalentCoursesIds'][$foundIndex]);
						$markForDeletion = false;
					}
				}

				if($markForDeletion){
					$_SESSION['selectedEquivalentCoursesIdsForDeletion'][] = $courseId;
				}
			}
		}
	}

	private function manageEquivalencyChanges(LearningCourse $courseModel){
		// array of new selection - keys are courses ids, values are flags for bidirectional
		$newEquivalencies = json_decode($_POST['equivalent-courses-data'], true);
		$currentlyEquivalentCoursesIds = $courseModel->getEquivalentCoursesIds();
		$coursesToBeDeleted = array_diff($currentlyEquivalentCoursesIds, array_keys($newEquivalencies));

		// delete equivalencies, that are not present already
		$command = Yii::app()->db->createCommand();
		$deleteCondition = 'source_course_id=:source';
		$deleteConditionCorespondentRelation = 'target_course_id=:source';
		if(!empty($coursesToBeDeleted)) {
			$deleteCondition .= ' AND target_course_id IN (' . implode(',', $coursesToBeDeleted) . ')';
			$deleteConditionCorespondentRelation .= ' AND source_course_id IN (' . implode(',', $coursesToBeDeleted) . ')';
			$command->delete(EquivalentCourse::model()->tableName(), $deleteCondition, array(':source' => $courseModel->idCourse));

			// delete also corespondent relation
			$command->delete(EquivalentCourse::model()->tableName(), $deleteConditionCorespondentRelation,
					array(':source' => $courseModel->idCourse));
		}



		$currentlyEquivalentCoursesIds = $courseModel->getEquivalentCoursesIds();
		foreach ($newEquivalencies as $courseId => $bidirectional) {
			if(!in_array($courseId, $currentlyEquivalentCoursesIds)) {
				// save a new equivalency
				Yii::app()->db->createCommand()
					->insert(EquivalentCourse::model()->tableName(), array(
						'source_course_id' => $courseModel->idCourse,
						'target_course_id' => $courseId,
						'bidirectional' => $bidirectional
					));
				if ($bidirectional == 1) {
					// create reverse relation
					Yii::app()->db->createCommand()
						->insert(EquivalentCourse::model()->tableName(), array(
							'source_course_id' => $courseId,
							'target_course_id' => $courseModel->idCourse,
							'bidirectional' => $bidirectional
						));
				}
			} else {
				// update current entry
				Yii::app()->db->createCommand()
					->update(EquivalentCourse::model()->tableName(),
						array('bidirectional' => $bidirectional),
						'source_course_id=:sourceCourse AND target_course_id=:target',
						array(
							':sourceCourse' => $courseModel->idCourse,
							':target' => $courseId
						)
					);
				if($bidirectional == 1){
					$isRelationExists = Yii::app()->db->createCommand()
						->select('source_course_id')->from(EquivalentCourse::model()->tableName())
						->where('source_course_id=:source AND target_course_id=:target',
							array(
								':source'=>$courseId,
								':target'=>$courseModel->idCourse
							))->queryScalar();
					if(!$isRelationExists)
						// create reverse relation
						Yii::app()->db->createCommand()
							->insert(EquivalentCourse::model()->tableName(), array(
								'source_course_id' => $courseId,
								'target_course_id' => $courseModel->idCourse,
								'bidirectional' => $bidirectional
							));
				}else{
					// delete reverse relation
					Yii::app()->db->createCommand()
						->delete(EquivalentCourse::model()->tableName(),
							'source_course_id=:source AND target_course_id=:target',
							array(':source' => $courseId, ':target' => $courseModel->idCourse));
				}
			}
		}
		EquivalentCourse::synchronizeEquivalents();
	}
}
