/**
 * My Coach widget jquery plugin
 */
(function($) {
    /* Holds an array of IDs of My Coach Widgets */
    var myCoachWidgets = [];

    /* Hold pluging options in this closure variable */
    var settings = {};

    /* Enable disable log() output to console */
    var debug = true;

    /* Unique id of the tab/chat widget */
    var uniqueId = '';

    // Prepare search criteria for chat users
    var searchCriteria = '';

    // This variable store the refresh messages timeout in order to be cleared lately
    var refreshMessagesTimeout = [];

    // This variable store the refresh users timeout in order to be cleared lately
    var refreshUsersTimeout;

    /**
     * A nice logging/debugging function
     */
    var log = function() {
        if (!debug) return;
        if (window.console && window.console.log) {
            var args = Array.prototype.slice.call(arguments);
            if(args){
                $.each(args, function(index, ev){
                    window.console.log(ev);
                });
            }
        }
    };

    // Define plugin methods, that could be called outside !
    var methods = {
        /**
         * init() is a special methods and is called if no method is specified, e.g. $('.widget').myCoachChat({}) will call init().
         * It must be called always first!!!!!! Without calling it first, the whole plugin won't work!!!!
         */
        init: function(options) {

            settings = $.extend({}, settings, options);

            if (typeof settings.debug !== "undefined")
                debug = settings.debug;

            // current tab id
            var currentTabId = $(this).attr('id');

            var currentUniqueId = currentTabId;
            currentUniqueId = currentUniqueId.replace("tabChat", "");

            // Get unique id by data-id attr of the tabChat elem
            var uniqueId = $(this).data('id');

            // Prepare search criteria
            if ($("#filter_users"+uniqueId).length > 0) {
                searchCriteria = $("#filter_users"+uniqueId).val();
            }

            var loadUsersListOptions = {'uniqueId': uniqueId, 'searchCriteria': searchCriteria, 'showLoader': true};

            loadUsersListOptions = $.extend({}, loadUsersListOptions, settings);

            if(loadUsersListOptions.typeChat == 'coach') {
                // Process chat preparation for the coach
                // Load basic event listeners for search and back icons
                methods.addBasicListeners(loadUsersListOptions);

                // Load users list
                methods.loadUsersList(loadUsersListOptions);
            } else {
                // Load messages list for the learner
                methods.loadMessagesList(loadUsersListOptions);
            }

            return this;
        },

        // Load list with users in the chat widget
        loadUsersList: function(options) {

            if (options.showLoader) {
                // Display loading gif
                $("#tabChat" + options.uniqueId + " .learners-list").prepend('<div class="chat-loading"><i class="fa fa-spinner fa-pulse fa-lg"></i></div>');
            }

            $("#tabChat"+options.uniqueId+" .learners-list").load(
                HTTP_HOST + 'index.php?r=player/block/axLoadCoachingChatUsers&idSession='+options.idSession+'&course_id='+options.idCourse+'&search='+options.searchCriteria+'&chatType=coach',

            function (response, status, xhr) {
                if (status == "error") {
                    var msg = "Sorry but there was an error: ";
                    $("#error").html(msg + xhr.status + " " + xhr.statusText);
                }

                if (status == "success") {

                    if($('#tabChat'+options.uniqueId).length > 0) {

                        // If there are any learners shown in the list
                        if ($(this).find('.learners-list-item').length > 0) {
                            // start adding event listeners per user
                            // Go through different learners
                            $(this).find('.learners-list-item').each(function(index, elem) {
                                $('#' + elem.id).on('click', function(){
                                    // Display loading gif
                                    $("#tabChat" + options.uniqueId + " .learners-list").prepend('<div class="chat-loading"><i class="fa fa-spinner fa-pulse fa-lg"></i></div>');

                                    // Hide(fade out) users list
                                    $("#tabChat" + options.uniqueId + " .learners-list > .learners-list-item").hide();

                                    // Current Learner id
                                    var currentLearner = elem.id;
                                    currentLearner = currentLearner.replace("learners-list-item-", "");

                                    // Load messages
                                    $("#tabChat" + options.uniqueId + " .learners-list").load(
                                        HTTP_HOST + 'index.php?r=player/block/axLoadCoachingChatMessage&idSession='+options.idSession+'&course_id='+options.idCourse+'&idLearner='+currentLearner,
                                        function( response, status, xhr ) {
                                            if (status == "error") {
                                                var msg = "Sorry but there was an error: ";
                                                $("#error").html(msg + xhr.status + " " + xhr.statusText);
                                            }

                                            if (status == "success") {
                                                // Scroll down to the message list bottom
                                                $("#tabChat" + options.uniqueId + " .messages-container").animate({
                                                    scrollTop: $("#tabChat" + options.uniqueId + " #messagesBottom").position().top
                                                }, 1000);

                                                // Hide search box
                                                $("#tabChat" + options.uniqueId + " #search-box" + options.uniqueId).hide();

                                                // Show the back icon
                                                $("#tabChat" + options.uniqueId + " #back-icon" + options.uniqueId).show();

                                                // Make sure it is displayed the send message button
                                                if ($("#tabChat" + options.uniqueId + " .write-message-button").length >0) {
                                                    $("#tabChat" + options.uniqueId + " .write-message-button").on('click', function() {
                                                        options.learner = currentLearner;
                                                        methods.doMessageSend(options);
                                                    });
                                                }

                                                clearInterval(refreshMessagesTimeout[options.uniqueId]);
                                                clearInterval(refreshUsersTimeout);
                                                
                                                methods.addMessageControlsListeners(options);
                                                
                                                refreshMessagesTimeout[options.uniqueId] = setInterval(function() {
                                                    localStorage.clear();
                                                    options.learner = currentLearner;
                                                    methods.refreshMessagesList(options);
                                                }, 60000);                                                
                                            }
                                        });
                                    });
                                });
                            }
                        }

                        clearInterval(refreshMessagesTimeout[options.uniqueId]);
                        clearInterval(refreshUsersTimeout);
                        refreshUsersTimeout = setInterval(function() {
                            localStorage.clear();
                            options.showLoader = false;
                            methods.loadUsersList(options);
                        }, 300000);

                    }
                }
            );
        },
        
        /**
         * Registering the event for the massages controls (edit, delete)
         * 
         * @param {type} options
         * @returns {undefined}
         */
        addMessageControlsListeners: function(options){    
            
            //Catch the event for hove on the control items (edit, delete) to hover the button (the 3 dots)
            $(".message-controls-items").hover(function(){                
                $(this).prev().css({'color': 'black', 'background-color': 'white', 'box-shadow': '0px 0px 3px #888888'});
            }, function(){
                $(this).prev().css({'color': '#CACACA', 'background-color': 'transparent', 'box-shadow': 'none'});
            });
            
            /**
             * Event for Edit
             */
//            $(".edit-message").on('click', function(){                                                                                
//                var messageId = $(this).data('id-message');                                
//                
//            	var request = $.ajax({
//                    url: HTTP_HOST + 'index.php?r=player/block/axGetMessage&idMessage='+messageId +'&course_id='+options.idCourse,
//                    cache: false,
//                    method: "GET"
//                });
//
//                request.done(function( response ) {
//                    $('#write-message-textarea').val(response);
//                    options.edit = messageId;
//                });
//            });
            
            /**
             * Event for Delete
             */
            $(".delete-message").on('click', function(){
                
                var messageId = $(this).data('id-message');
                var that = this;
            	var request = $.ajax({
                    url: HTTP_HOST + 'index.php?r=player/block/axDeleteMessage&idMessage='+messageId +'&course_id='+options.idCourse,
                    cache: false,
                    method: "GET"
                    //data: { uniqueId : options.uniqueId },
                    //dataType: "html"
                });

                request.done(function( response ) {              
                    if(response === '1'){                        
                        $(that).closest('.message-container').remove();
                    }                    
                });
            });
        },

        // Load a list of messages in the chat widget for the learner
        loadMessagesList: function(options) {
            // Display loader animation
            $("#tabChat" + options.uniqueId + " .learners-list").prepend('<div class="chat-loading"><i class="fa fa-spinner fa-pulse fa-lg"></i></div>');

            // Load messages
            $("#tabChat" + options.uniqueId + " .learners-list").load(
                HTTP_HOST + "index.php?r=player/block/axLoadCoachingChatMessage&idSession="+options.idSession+"&course_id="+options.idCourse+"&idLearner="+options.learner,
                function( response, status, xhr ) {
                    if (status == "error") {
                        var msg = "Sorry but there was an error: ";
                        $("#error").html(msg + xhr.status + " " + xhr.statusText);
                    }

                    if (status == "success") {
                        // Scroll down to the message list bottom
                        $("#tabChat" + options.uniqueId + " .messages-container").animate({
                            scrollTop: $("#tabChat" + options.uniqueId + " #messagesBottom").position().top
                        }, 1000);

                        // Make sure it is displayed the send message button
                        if ($("#tabChat" + options.uniqueId + " .write-message-button").length >0) {
                            $("#tabChat" + options.uniqueId + " .write-message-button").on('click', function() {                                
                                methods.doMessageSend(options);
                            });
                        }

                        /* fixme uncomment when ready with the logic */

                        clearInterval(refreshMessagesTimeout[options.uniqueId]);
                        methods.addMessageControlsListeners(options);
                        clearInterval(refreshUsersTimeout);
                        refreshMessagesTimeout[options.uniqueId] = setInterval(function() {
                            localStorage.clear();
                            methods.refreshMessagesList(options);
                        }, 60000);

                    }
                });
        },

        doMessageSend: function(options) {
            $("#tabChat" + options.uniqueId + " #messagesBottom").html('<div class="chat-loading"><i class="fa fa-spinner fa-pulse fa-lg"></i></div>');
            // scroll to the bottom
            $("#tabChat" + options.uniqueId + " .messages-container").animate({
                scrollTop: $("#tabChat" + options.uniqueId + " #messagesBottom").position().top
            }, 500);

            // Serialize form data
            var dataMessageForm = $("#tabChat" + options.uniqueId + " #send-coach-chat-message-form").serialize();

            // current tab id
            var currentTabId = $("#tabChat" + options.uniqueId + " #send-coach-chat-message-form" ).parents( "div.tabChat" ).attr('id');

            var currentUniqueId = currentTabId;
            currentUniqueId = currentUniqueId.replace("tabChat", "");

            // Current coaching session id
            var idSession = options.idSession;

            // Current course id
            var idCourse = options.idCourse;                        

            $("#" + currentTabId + " .learners-list").load(                    
                HTTP_HOST + 'index.php?r=player/block/axLoadCoachingChatMessage&idSession='+idSession+'&course_id='+idCourse+'&idLearner='+options.learner+'&'+dataMessageForm,
                function( response, status, xhr ) {
                    if (status == "error") {
                        var msg = "Sorry but there was an error: ";
                        $("#error").html(msg + xhr.status + " " + xhr.statusText);
                    }

                    if (status == "success") {                        
                        
                        $("#tabChat" + options.uniqueId + " .write-message-button").on('click', function() {
                            methods.doMessageSend(options);
                        });

                        // scroll to the bottom
                        $("#" + currentTabId + " .messages-container").animate({
                            scrollTop: $("#" + currentTabId + " #messagesBottom").position().top
                        }, 1000);

                        /* fixme uncomment when ready with the logic */

                        clearInterval(refreshMessagesTimeout[options.uniqueId]);
                        
                        methods.addMessageControlsListeners(options);
                        clearInterval(refreshUsersTimeout);
                        refreshMessagesTimeout[options.uniqueId] = setInterval(function() {
                            localStorage.clear();
                            methods.refreshMessagesList(options);
                        }, 60000);

                        /* */
                    }
                });
        },

        addBasicListeners: function(options) {
            // On back icon click - fixme test and adjust !!!
            $("#back-icon"+options.uniqueId).on('click', function() {
                // Display loading gif
                $("#"+options.uniqueId+" .learners-list").prepend('<div class="chat-loading"><i class="fa fa-spinner fa-pulse fa-lg"></i></div>');

                // Hide(fade out) users list
                $("#"+options.uniqueId+" .learners-list > .message-list-item").hide();

                // Hide no messages text if it is shown
                $("#"+options.uniqueId+" .no-messages").fadeOut(1000);

                // Hide back icon
                $("#back-icon"+options.uniqueId).hide();

                // Show search box
                $("#search-box"+options.uniqueId).show();

                methods.loadUsersList(options);
            });

            // On search icon click
            $("#search-icon"+options.uniqueId).on('click', function() {
                // Get the search criteria
                if ($("#filter_users"+options.uniqueId).length > 0) {
                    searchCriteria = $("#filter_users"+options.uniqueId).val();
                    options.searchCriteria = searchCriteria;
                }

                methods.loadUsersList(options);
            });

            // On reset search criteria icon clicked
            $("#clear-search"+options.uniqueId).on('click', function() {
                // Reset the search criteria
                if ($("#filter_users"+options.uniqueId).length > 0) {
                    $("#filter_users"+options.uniqueId).val('');
                    options.searchCriteria = '';
                }

                // Reload the users list
                methods.loadUsersList(options);
            });

            // process enter key pressed in search users (filter) box
            $("#filter_users"+options.uniqueId).bind("enterKey",function(e){
                // Get the search criteria
                if ($("#filter_users"+options.uniqueId).length > 0) {
                    searchCriteria = $("#filter_users"+options.uniqueId).val();
                    options.searchCriteria = searchCriteria;
                }

                methods.loadUsersList(options);
            });

            $("#filter_users"+options.uniqueId).keyup(function(e){
                if(e.keyCode == 13)
                {
                    // trigger custom event enter key pressed
                    $(this).trigger("enterKey");
                }
            });
        },
        
        /**
         * Refreshing all messages, to be sure that if the message is edited in the DB, it will be update here
         * 
         * @param {type} options
         * @returns {undefined}
         */
        refreshEditedMessages: function(options){
            if ($("#tabChat" + options.uniqueId + " .messages-container").length >0) {
                // current tab id
                var currentTabId = $("#tabChat" + options.uniqueId + " #send-coach-chat-message-form" ).parents( "div.tabChat" ).attr('id');

                // Current coaching session id
                var idSession = options.idSession;

                // Current course id
                var idCourse = options.idCourse;

                // Do the request to check is there any new seats and add new messages to the messages queue/list
                var request = $.ajax({
                    url: HTTP_HOST + 'index.php?r=player/block/axCheckEditedCoachingChatMessage&idSession='+idSession+'&course_id='+idCourse+'&idLearner='+options.learner+'&loadMessagesOnly=1',
                    cache: false,
                    method: "GET"
                });
                
                var that = this;

                request.done(function( response ) {       
                    
                    if(!response){
                        return false;
                    }
                    
                    var data = JSON.parse(response);                                       
                    
                    var idsLength = data.ids.length;
                    
                    if(idsLength === 0){
                        return;
                    }
                    
                    $.each(data.ids, function(key, value){
                        if($(".message-container-" + options.learner + ' #message_' + key).length > 0){
                            $(".message-container-" + options.learner + ' #message_' + key).find('.message-text').text(value);                            
                        }
                    });
                });

            } else {
                clearInterval(refreshMessagesTimeout[options.uniqueId]);
            }
        },
        
        /**
         * 
         * Refresh the messages if message is deleted from the DB
         * 
         * @param {type} options
         * @returns {undefined}
         */
        refreshDeleteMessages: function(options){
            if ($("#tabChat" + options.uniqueId + " .messages-container").length >0) {
                // current tab id
                var currentTabId = $("#tabChat" + options.uniqueId + " #send-coach-chat-message-form" ).parents( "div.tabChat" ).attr('id');

                // Current coaching session id
                var idSession = options.idSession;

                // Current course id
                var idCourse = options.idCourse;

                // Do the request to check is there any new seats and add new messages to the messages queue/list
                var request = $.ajax({
                    url: HTTP_HOST + 'index.php?r=player/block/axCheckDeletedCoachingChatMessage&idSession='+idSession+'&course_id='+idCourse+'&idLearner='+options.learner+'&loadMessagesOnly=1',
                    cache: false,
                    method: "GET"
                });

                request.done(function( response ) {                                            
                    
                    if(!response){                        
                        return false;
                    }
                    
                    var data = JSON.parse(response);                                                                                                   
                    
                    $(".message-container-" + options.learner + " .message-container").each(function(key, value){                        
                        var id = $(this).attr('id');
                        if(!id)
                            return false;
                        var ids = id.split('_');
                        var idMessage = ids[1];                                                    
                        
                        if(data.ids.indexOf(idMessage) < 0){
                            $(this).remove();
                        }
                    });
                });

            } else {
                clearInterval(refreshMessagesTimeout[options.uniqueId]);
            }
        },

        // Load a list of messages in the chat widget for the learner -- only the new messages
        refreshMessagesList: function(options) {                        
            //alert('yes');
            // Make sure it is displayed the messages container(in fact the messages list is displayed) !!!
            if ($("#tabChat" + options.uniqueId + " .messages-container").length >0) {
                // current tab id
                var currentTabId = $("#tabChat" + options.uniqueId + " #send-coach-chat-message-form" ).parents( "div.tabChat" ).attr('id');

                // Current coaching session id
                var idSession = options.idSession;

                // Current course id
                var idCourse = options.idCourse;

                // Do the request to check is there any new seats and add new messages to the messages queue/list
                var request = $.ajax({
                    url: HTTP_HOST + 'index.php?r=player/block/axLoadCoachingChatMessage&idSession='+idSession+'&course_id='+idCourse+'&idLearner='+options.learner+'&loadMessagesOnly=1',
                    cache: false,
                    method: "GET"
                    //data: { uniqueId : options.uniqueId },
                    //dataType: "html"
                });

                request.done(function( response ) {  
                    
                    //Edit and Delete checking
                    methods.refreshDeleteMessages(options);
                    methods.refreshEditedMessages(options);
                    if ($(response).find('.message-text').length > 0) {                                                                                                
                        $(".message-container-" + options.learner + " #messagesBottom").before(response);

                         // FIXME - Temporary disable scroll to last message as it doesn't works ok !!!
                         //$("#" + currentTabId + " .messages-container").animate({
                         //   scrollTop: $("#" + currentTabId + " #messagesBottom").offset().top
                         //}, 500);

                    }
                });

            } else {
                clearInterval(refreshMessagesTimeout[options.uniqueId]);
            }
        }
    };

    // pass options to the jquery plugin or get corresponding method passed
    $.fn.myCoachChat = function(methodOrOptions) {
        if ( methods[methodOrOptions] ) {
            return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
            // Default to "init"
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  methodOrOptions + ' does not exist on jQuery.myCoachChat' );
        }
    };

})(jQuery);