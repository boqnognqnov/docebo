var Report = {

	course_id: null,
	reportGridId: null,
	deliverableGridId: null,
	objectsGridId: null,
	exportUsersUrl: null,
	exportObjectsUrl: null,
	reportScoUsersGridId: null,
	exportScoUsersUrl: null,

	init: function(options) {
		this.course_id = options.course_id;
		this.reportGridId =  options.reportGridId;
		this.deliverableGridId =  options.deliverableGridId;
		this.objectsGridId =  options.objectsGridId;
		this.exportUsersUrl =  options.exportUsersUrl;
		this.exportObjectsUrl =  options.exportObjectsUrl;
		this.progressUsersExportUrl = options.progressUsersExportUrl;
		this.progressObjectsExportUrl = options.progressObjectsExportUrl;
		this.progressSingleObjectExportUrl = options.progressSingleObjectExportUrl;

	},

	exportReport: function(obj, formId, url)
	{
		var form = $("#"+formId);
		var searchData = form.find("input[type='hidden'], :input:not(:hidden)").serialize();
		var exportType = $(obj).prev(".exportType").val();
		window.location = url+"&"+searchData + "&exportType=" + exportType;
	},

	search: function(gridId, formId)
	{
		$.fn.yiiGridView.update(gridId, {
			data: $("#"+formId).find("input[type='hidden'], :input:not(:hidden), .test-report-column input").serialize()+"&test_questions%5B%5D=0"
		});
	},

	searchUsers: function()
	{
		$.fn.yiiGridView.update(Report.reportGridId, {
			data: $("#reportSearchUsersForm").find("input[type='hidden'], :input:not(:hidden)").serialize()
		});
	},

	searchDeliverables: function()
	{
		$.fn.yiiGridView.update(Report.deliverableGridId, {
			data: $("#reportSearchDeliverablesForm").find("input[type='hidden'], :input:not(:hidden)").serialize()
		});
	},

	exportUsers: function()
	{
		var searchData = $("#reportSearchUsersForm").find("input[type='hidden'], :input:not(:hidden)").serialize();
		var exportType = $("#exportType").val();
		window.location = this.exportUsersUrl+"&"+searchData + "&exportType=" + exportType;
	},

	searchObjects: function()
	{
		$.fn.yiiGridView.update(Report.objectsGridId, {
			data: $("#reportSearchObjectsForm").find("input[type='hidden'], :input:not(:hidden)").serialize()
		});
	},

	exportObjects: function(user_id)
	{
		var url = '';
		if (user_id != 'undefined')
			url = '&user_id='+user_id;
		var searchData = $("#reportSearchObjectsForm").find("input[type='hidden'], :input:not(:hidden)").serialize();
		var exportType = $("#exportObjectsType").val();
		window.location = this.exportObjectsUrl+"&"+searchData + "&exportType=" + exportType + url;
	},

	searchScoUsers: function()
	{
		$.fn.yiiGridView.update(Report.reportScoUsersGridId, {
			data: $("#reportSearchScoUsersForm").find("input[type='hidden'], :input:not(:hidden)").serialize()
		});
	},

	exportScoUsers: function()
	{
		var searchData = $("#reportSearchScoUsersForm").find("input[type='hidden'], :input:not(:hidden)").serialize();
		var exportType = $("#exportScoType").val();
		window.location = this.exportScoUsersUrl+"&"+searchData + "&exportType=" + exportType;
	},

	changeGrid: function(obj, id)
	{
		$('.report-nav-btn').removeClass('active');
		$('.report-nav-arrow').removeClass('active');
		$('.report-grid-box').hide();

		$(obj).addClass('active');
		$(obj).find('.report-nav-arrow').addClass('active');
		$('#'+id).show();

		setTimeout(function(){
			$(document).controls();
		}, 600);
	},

	showStatusPopover: function(id)
	{
		$('#'+id).popover();
	},

	changeUserSelection: function(url, id)
	{
		window.location = url+"&user_id="+id;
	},

	showInfoBox: function(idToHide, url)
	{
		$("#report-info-box").show();
		$("#"+idToHide).hide();
		$("#report-info-box-content").html('');
		$(".report-ajax-loader").show();
		$.ajax({
			url: url,
			error: function (data) {
				bootbox.alert("Ajax error");
				$('.report-ajax-title').css('margin-left', (parseInt($("#report-info-box .docebo-back").outerWidth()) + 20)+'px');
			},
			success: function (data) {
				data = JSON.parse(data);
				if (data.success == true) {
					$(".report-ajax-loader").hide();
					$("#report-info-box-content").html(data.html);
					$("#report-test-column-selector").css('width', $("#player-reports-test-users").width()-15);
					jQuery("#report-test-column-selector").click(function(e){
						e.stopPropagation();
					});
					$('.report-ajax-title').css('margin-left', (parseInt($("#report-info-box .docebo-back").outerWidth()) + 20) + 'px');
					$(document).controls();
				}
			}
		});
	},

	hideInfoBox: function(idToShow)
	{
		$("#report-info-box").hide();
		$("#"+idToShow).show();
	},

	showScoXml: function(url)
	{
		$.fancybox.open([{
			type: 'wrap',
			href : url
		}], {
			padding : 0,
			margin: 30,
			width: '60%',
			openEffect: 'elastic',
			closeEffect: 'fade'
		});
	},

	triggerInlineEdit: function(e, questId)
	{
		e = $.event.fix(e); // IE fix
		e.stopPropagation();
		$('#score-edit-'+questId).trigger('click');
	},

	triggerLOStatusInlineEdit: function(e, id)
	{
		e = $.event.fix(e); // IE fix
		e.stopPropagation();
		$("#lo_status_"+id).trigger('click');
	},

	changeLOStatus: function(id, status)
	{
		var element = $('#btn_'+id);
		element.attr('class', '');
		switch (status)
		{
			case 'completed':
				element.addClass('i-sprite is-circle-check green lo-status-edit-btn');
				break;
			case 'ab-initio':
				element.addClass('i-sprite is-play grey lo-status-edit-btn');
				break;
			case 'attempted':
				element.addClass('i-sprite is-play orange lo-status-edit-btn');
				break;
			case 'failed':
				element.addClass('i-sprite is-play red lo-status-edit-btn');
				break;
		}
	},

	/**
	 * If updates parameters are not available, use the original boostrap.editable value
	 * @param updates
	 * @param id
	 * @param value
	 */
	updateLORow: function(type, updates, id, value)
	{
		if (type == 'status')
			Report.changeLOStatus(id, value);
		if (typeof updates !== "undefined" && updates.length > 0)
		{
			for (var i = 0; i < updates.length; i++)
			{
				if (updates[i].newStatus)
				{
					var id = updates[i].id;
					if (updates[i].id_scorm_item)
						id = id + updates[i].id_scorm_item;
					Report.changeLOStatus("lo_status_"+id, updates[i].newStatus);
				}
			}
		}
	},

	participationCheck: function(obj, params, deferrer) {
		var _this = $(obj);
		var status = _this.hasClass("lo-status-editable");

		var option = 'score';
		if (status) {
			option = 'status';
		}
		else if (_this.hasClass("lo-attempts-editable")) {
			option = 'attempts';
		}

		// The LO is participating in other courses as well
		var participate = _this.data("participate");

		// If so then let's
		if(participate) {
			_this.editable('option', 'onblur', 'ignore');
			var content = _this.data("change-value-confirm");
			var options = { header: Yii.t('standard', '_EDIT_SCORE'), classes: "change-score", headerCloseButton: false };
			if (status) {
				options = { header: Yii.t('standard', 'Change status'), classes: "change-status", headerCloseButton: false };
			}

			var buttons = [
				{
					label: Yii.t('standard', '_CONFIRM'),
					'class': 'btn-docebo big green',
					callback: function() {
						// If the user did Confirm the Checkbox then we are good to go .... change the status
						var confirm = $('div.bootbox.modal').find("input:checked");
						if (confirm.length) {

							$.ajax({
								type: "POST",
								url: _this.data("url"),
								data: params,
								success: function(res) {
									_this.editable('option', 'onblur', 'cancel');
									res = JSON.parse(res);
									if (res.success == false) {
										return deferrer.reject( res.message );
									} else {
										Report.updateLORow(option, res.data.updates, params.name, params.value);
										deferrer.resolve();
									}
								}
							});

						} else {
							// You can't confirm without Accepting the checkbox
							return false;
						}
					}
				}, {
					label: Yii.t('standard', '_CANCEL'),
					'class': 'btn-docebo big black',
					callback: function() {
						$(obj).siblings(".popover").hide();
						_this.editable('option', 'onblur', 'cancel');
					}
				}
			];

			bootbox.dialog(content, buttons, options)

		} else {
			$.ajax({
				type: "POST",
				url: _this.data("url"),
				data: params,
				success: function(res) {
					res = JSON.parse(res);
					if (res.success == false) {
						return deferrer.reject( res.message );
					} else {
						Report.updateLORow(option, res.data.updates, params.name, params.value);
						deferrer.resolve();
					}
				}
			})
		}
	},

	/**
	 * General method to prepare "editable" elements
	 */
	prepareEditableType: function(typeClass, validateMessage) {
		
		var self = this;
		var myTypeClass = '.' + typeClass;
		
		if ($(myTypeClass).length > 0)
		{
			$(myTypeClass).editable({
				validate: function(value) {
					value = value.trim();
					if (value == '')
					{
						return validateMessage;
					}

				},
				url: function(params) {

					var def = new $.Deferred();

					self.participationCheck(this, params, def);

					return def.promise();
				}
			});
		}
		
	},
	
	prepareEditable: function()
	{
		var self = this;
		if ($('.lo-status-editable').length > 0)
		{
			$('.lo-status-editable').editable({
				validate: function(value) {
					if (value == '')
					{
						return Yii.t("player", "Please select status");
					}

				},
				url: function(params) {

					var def = new $.Deferred();

					self.participationCheck(this, params, def);

					return def.promise();
				}
			});
		}

		if ($('.lo-score-editable').length > 0)
		{
			$('.lo-score-editable').editable({
				validate: function(value) {
					//if (value == '' || !(value % 1 === 0))
					if (value == '')
					{
						return Yii.t("player", "Please add valid score");
					}
				},
				url: function(params) {

					var def = new $.Deferred();

					self.participationCheck(this, params, def);

					return def.promise();
				},
				//onblur: "ignore"
			});

			//$(document).on("click", function(e){
			//	var element = $('div.bootbox.modal.change-status');
			//	if ( element.length ) {
			//		e.preventDefault();
			//	}
			//});

		}

		$('.lo-score-editable, .lo-status-editable, .lo-attempts-editable').on('shown', function() {
			if ($('.editable-submit').length){
				$('.editable-submit').addClass('btn-docebo green big')
			}

			if ($('.editable-cancel').length)
				$('.editable-cancel').addClass('btn-docebo big');
		});
	},

	/**
	 * Open Dialog and run "progressive" exporter for Course Users stats
	 */
	runProgressExportUsers: function()
	{
		var searchData = $("#reportSearchUsersForm").find("input[type='hidden'], :input:not(:hidden)").serialize();
		var exportType = $("#exportType").val();

		var url = this.progressUsersExportUrl + '&exportType='+exportType  + '&' + searchData;

		$('<div/>').dialog2({
			title: Yii.t('report', 'Exporting') + '...',
			content: url,
			id: "progressive-report-export-dialog"
		});
		return true;

	},

	/**
	 * Open Dialog and run "progressive" exporter for Course OBJECTS stats
	 */
	runProgressExportObjects: function(user_id)
	{
		var userParam = '';
		if (typeof user_id != "undefined")
			userParam = '&user_id='+user_id;

		var searchData = $("#reportSearchObjectsForm").find("input[type='hidden'], :input:not(:hidden)").serialize();
		var exportType = $("#exportObjectsType").val();

		var url = this.progressObjectsExportUrl + '&exportType='+exportType  + '&' + searchData + userParam;

		$('<div/>').dialog2({
			title: Yii.t('report', 'Exporting') + '...',
			content: url,
			id: "progressive-report-export-dialog"
		});
		return true;

	},

	/**
	 * Open Dialog and run "progressive" exporter for Course SINGLE OBJECT stats (one object -> many users)
	 */
	runProgressExportSingleObject: function(idOrg, idScormItem)
	{
		var searchData = '';

		// This method is a variant: it could be used for ANY LO or for SCO Items (SCORM sub items)
		idOrg		 	= (idOrg == null 		|| typeof idOrg == "undefined") 		? false : idOrg;
		idScormItem 	= (idScormItem == null 	|| typeof idScormItem == "undefined") 	? false : idScormItem;

		// Get the export type
		if (idOrg) {
			var exportType = $('select.exportType').val();
		}
		else if (idScormItem) {
			var exportType = $('select#exportScoType').val();
		}

		var quests = '';

		if($('#report-test-column-selector'))
		{
			var first = 1;
			quests = '';
			var questions = $('#report-test-column-selector input');

			for(var i = 0; i < questions.length; i++)
			{
				if(questions[i].value > 0 && questions[i].checked == 1)
				{
					if(first == 1)
						first = 0;
					else
						quests = quests + ',';
					quests = quests + questions[i].value;
				}
			}
		}

		// Build URL; mind the variants
		var url = this.progressSingleObjectExportUrl + '&exportType=' + exportType
			+ (idOrg ? ('&idOrg=' + idOrg) : '')
			+ (idScormItem ? ('&idScormItem=' + idScormItem) : '')
			+ (searchData != '' ? '&' + searchData : '');


		$('<div/>').dialog2({
			title: Yii.t('report', 'Exporting') + '...',
			content: url,
			id: "progressive-report-export-dialog",
			ajaxType: 'POST',
			data: {quests: quests}
		});

		return true;

	}
}


$(function(){
	var gauge1 = $('#gauge-courseCompletion img');
	var gauge1Angle = 0 - parseInt(gauge1.attr('data-value')) *  1.8;

	var gauge2 = $('#gauge-overOneMonthCompletion img');
	var gauge2Angle = 0 - parseInt(gauge2.attr('data-value')) *  1.8;

	setTimeout(function(){
		gauge1.rotate(gauge1Angle);
		gauge2.rotate(gauge2Angle);
	}, 500);

	$(".reports-fancybox").fancybox({
		'zoomSpeedIn': 300,
		'zoomSpeedOut': 300,
		'overlayShow': true,
		//'scrolling': 'yes',
		'afterShow': function() {},
		'afterClose':function (opt) {
			if(opt.element){
				var elem = $(opt.element);
				if(elem && elem.hasClass('report-grid-link')){
					if (!elem.data('no-refresh')) {
						window.location.reload();
					}
				}
			}
		}
	});

	Report.prepareEditable();
	
	Report.prepareEditableType('lo-attempts-editable', Yii.t('player', 'Please provide correct number'));
	
});