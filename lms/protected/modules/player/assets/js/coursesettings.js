
var Coursesettings = {

    dateFormat: 'yy-mm-dd',
	datePickerStartDate: null,
	datePickerEndDate: null,
	datePickerSubscriptionStartDate: null,
	datePickerSubscriptionEndDate: null,

	init: function() {
		$(window).bind('hashchange', function() {
			Coursesettings.switchTab( location.hash.substr(1) );
		}).trigger('hashchange');
		if (location.hash == "") {
			location.hash = '#' + $('#coursesettings-menu a:first').data('target');
		}

		// listen for subscription mode (option 3 use extra cascading fields)
		if ($("input[type='radio'][name='LearningCourse[can_subscribe]']:checked").val() != '2') {
			$('#can_subscribe_period_selection').hide();
		}
		$("input[type='radio'][name='LearningCourse[can_subscribe]']").parent().on('change', this.subscriptionMethodChange);

		// listen for autogenerate button
		$('#self_generate_code').on('click', this.generateCode);

		// listen for generate preview
		$('#certificate_preview').on('click', function(e) {
			e.preventDefault();
			// read the id of the selected certitificate and use with the url
			var id_certificate = $('#LearningCourse_assigned_certificate').val();
			window.location = $(this).data('url') + id_certificate;
		});

		// Manage click on Publish/Unpublish switch
		$("#course-publish-mode-switcher").on('click', this.handlePublishSwitch);

        $(document).ready(function(){
            Coursesettings.initForm();
        });
	},

	handlePublishSwitch: function(e) {
		e.preventDefault();
		var currentStatus = $('#publish-switch input[name="publish-status"]:checked').val();
		var targetStatus = (currentStatus == 'published') ? 'unpublished' : 'published';

		// Call ajax to change status
		$.ajax({
			url: Player.changeCourseStatusUrl,
			data: {status: targetStatus},
			error: function (data) {
				Docebo.Feedback.show('error', "Ajax error");
			},
			success: function (data) {
				data = JSON.parse(data);
				if (data.success == true) {
					// in this case the central switch was clicked we have to invert the current selection
					// by checking the other radio and switching
					switch (targetStatus) {
						case 'published':
							$('#course-mode-unpublish').prop('checked', false);
							$('#course-mode-publish').prop('checked', true);
							$('#course-publish-mode-switcher').addClass('active');
							$("#player-course-status").removeClass("published not-published").addClass("published");
							break;
						case 'unpublished':
						default:
							$('#course-mode-publish').prop('checked', false);
							$('#course-mode-unpublish').prop('checked', true);
							$('#course-publish-mode-switcher').removeClass('active');
							$("#player-course-status").removeClass("published not-published").addClass("not-published");
							break;
					}

					// Update the publish status message
					$("#player-course-status").html(data.data.new_status);
					var publishStatusHtml = data.data.publish_status_html;
					if(publishStatusHtml)
						$('#publish-status').html(publishStatusHtml);
				} else
					Docebo.Feedback.show('error', "Ajax error");
			}
		});
	},

    initForm: function() {
		var settings = this;
		var datepickerSettings = {
			format: Coursesettings.dateFormat,
			autoclose: true,
			language: yii.language
		};
		this.datePickerStartDate = $('#date-begin-calendar').bdatepicker(datepickerSettings).on('show', function(ev){
			settings.datePickerEndDate.hide();
		}).data('datepicker');

		this.datePickerEndDate = $('#date-end-calendar').bdatepicker(datepickerSettings).on('show', function(ev){
			settings.datePickerStartDate.hide();
		}).data('datepicker');

		this.datePickerSubscriptionStartDate = $('#subscr-date-from').bdatepicker(datepickerSettings).on('show', function(ev){
			settings.datePickerSubscriptionEndDate.hide();
		}).data('datepicker');

		this.datePickerSubscriptionEndDate = $('#subscr-date-to').bdatepicker(datepickerSettings).on('show', function(ev){
			settings.datePickerSubscriptionStartDate.hide();
		}).data('datepicker');

        $('.timepicker_15').timepicker({ 'timeFormat': 'H:i', 'step' : 15 });
    },

	generateCode: function(e) {
		e.preventDefault();
		// generate code
		var generated_code = '',
			pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		for (var i = 0; i < 11; i++) {
			generated_code += pool[Math.round(Math.random() * (pool.length - 1))];
		}

		// assign code to the field
		$('#LearningCourse_autoregistration_code').val(generated_code);
	},

	subscriptionMethodChange: function(e) {
		if ($(this).find("input[type='radio'][name='LearningCourse[can_subscribe]']").val() == '2') {
			$('#can_subscribe_period_selection').show();
		} else {
			$('#can_subscribe_period_selection').hide();
		}
	},

	hideAll: function() {
		$('.settings-tab').hide();
	},

	switchTab: function(id) {
		$('.settings-tab').hide();
		$('#'+id).show();

		$('#current_tab').val(id);

		var all_links = $('#coursesettings-menu li a');
		all_links.removeClass("active");
		all_links.find("span").removeClass("white");

		$('a[data-target="' + id + '"]').addClass("active");
		$('a[data-target="' + id + '"] span').addClass("white");
	}

}
