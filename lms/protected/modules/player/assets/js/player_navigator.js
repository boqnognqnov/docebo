var PlayerNavigator = {
	settings: {},
	ajaxUrl: null,
	currentKey: null,
	ended: false,
	state: 'initial',
	init: function(options){
		var defaultOptions = {
			selector: '.player-navigator'
		};
		this.settings = $.extend(defaultOptions, this.settings, options);

		if(options.ajaxUrl){
			this.ajaxUrl = options.ajaxUrl;
		}

		$(this.settings.selector).on('click', '.next, .prev', function(){
			var parentHolder = $(this).parent('.player-navigator-holder');
			var loKey = $(this).data('key');
			if($.type(parentHolder.attr('data-disablerq')) == 'undefined'){
				parentHolder.attr('data-disablerq', true);
				Arena.playByLoKey(loKey);
			}
		});
	},
	setCurrentKey: function(key){
		this.currentKey = key;
	},
	setState: function(newState){
		this.state = newState;
	},
	setEnded: function(endedOrNotBool){
		if(endedOrNotBool){
			this.state = 'ended';
		}
	},
	reload: function(){
		if(this.currentKey===null && this.state!=='ended'){
			PlayerNavigator.disable();
		}else {
			var url = this.ajaxUrl + '&current_key=' + this.currentKey + '&state='+this.state;
			$(this.settings.selector).load(url, function(data){
				PlayerNavigator.afterReload(data);
			});
		}
	},
	afterReload: function(data){
		// after data recieved
		this.enable();
	},
	disable: function(){
		$(this.settings.selector).hide();
	},
	enable: function(){
		$(this.settings.selector).show();
	}
};