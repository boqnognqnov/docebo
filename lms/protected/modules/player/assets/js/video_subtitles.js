var SubtitlesManagementClass  = SubtitlesManagementClass || false;

(function($) {
	
	SubtitlesManagementClass = function(options) {
		
		this.defaultOptions = {
			debug:	true
		};
		
		if (typeof options === "undefined" ) {
			var options = {};
		}
		
		this.init(options);

	}

	
	/**
	 * Prototype: SubtitlesManagementClass
	 */
	SubtitlesManagementClass.prototype = {

		pl_uploader: null,

		/**
		 * A nice logging/debugging function
		 */
	    log : function() {
	    	if (!this.options.debug) return;
	    	if (window.console && window.console.log) {
	    		var args = Array.prototype.slice.call(arguments);
	    		if(args){
	    			$.each(args, function(index, ev){
	    				window.console.log(ev);
	    			});
	    		}
	        }
	    },
			
			
		/**
		 * Initialize class object
		 */
		init: function(options) {

			this.options = $.extend({}, this.defaultOptions, options);
			this.log('Initialize SubtitlesManagement ');
			
			this.initializePluploader();

			this.hideFallbackDeleteBtn();

			this.toggleFallbackHandlers();

		},

		hideFallbackDeleteBtn: function() {
			// 1. Remove all hidden delete buttons
			$('.subtitles-list .fa-check-circle-o').each(function( index ) {
				var currentElemContainer = $(this).closest('.subtitles-list-row');
				var currentElem = currentElemContainer.find('.delete-sub-link');

				if (currentElem.hasClass('hidden')) {
					currentElem.removeClass('hidden');
				}
			});

			// 2. Hide only fallback subtitle
			var currentFallbackContainer = $('.fa-check-circle-o.green-text').closest('.subtitles-list-row');
			var currentFallback = currentFallbackContainer.find('.delete-sub-link');
			if (!(currentFallback.hasClass('hidden'))) {
				currentFallback.addClass('hidden');
			}
		},

		/**
		 * Attach toggle Fallback event handler
 		 */
		toggleFallbackHandlers: function() {
			var that = this;
			$('.subtitles-list .fa-check-circle-o').off();
			$('.subtitles-list .fa-check-circle-o').on('click', function() {
				// Get the current subtitle row
				var selectedElement = $(this).closest('.subtitles-list-row');
				var currentCheckboxId = $(this).data('subId');

				$('input[name="fallbackSub"]').val(currentCheckboxId);
				// Make all checkboxes grey
				$('.subtitles-list .fa-check-circle-o').each(function( index ) {
					if ($(this).data('subId') !== currentCheckboxId) {
						if ($(this).hasClass('green-text')) {
							$(this).removeClass('green-text');
						}
					}
				});

				if ($('i[data-sub-id='+$(this).data('subId')+']').hasClass('green-text')) {
					$(this).removeClass('green-text');
					$('#fallbackSub').val(''); // reset the default selected fallback subtitle !!!
				} else {
					$(this).addClass('green-text');
				}
				
				// Do ajax request to do actual deletion
				$.ajax({
					method: "POST",
					url: that.options.pluploaderOptions.baseFallbackSetUrl,
					data: {
						langCode:	selectedElement.data('filterValue'),
						subId: 		$(this).data('subId')
					}
				})
				.done(function( msg ) {
					//this.log( "Data Saved: " + msg );
				});

				that.hideFallbackDeleteBtn();
			});
		},

		/**
		 * 
		 */
		initializePluploader: function() {
			// Create pluploader object
			this.pl_uploader = new plupload.Uploader(this.options.pluploaderOptions);
			
			if (typeof this.pl_uploader === "undefined" || this.pl_uploader == null) {
				this.log('PLUploader not properly initialized or created!');
				return false;
			}

			this.pl_uploader.bind('Init', $.proxy(this.Init, this));

			this.pl_uploader.init();

			this.pl_uploader.bind('BeforeUpload', $.proxy(this.BeforeUpload, this));
			this.pl_uploader.bind('FilesAdded', $.proxy(this.FilesAdded, this));
			this.pl_uploader.bind('UploadProgress', $.proxy(this.UploadProgress, this));
			this.pl_uploader.bind('Error', $.proxy(this.Error, this));
			this.pl_uploader.bind('UploadComplete', $.proxy(this.UploadComplete, this));
			this.pl_uploader.bind('FileUploaded', $.proxy(this.FileUploaded, this));

		},
			
		Init: function(up,params) {
			//this.log('Init.');
		},

		BeforeUpload: function(up,file) {
			//this.log('BeforeUpload.');
		},

		FilesAdded: function(up,file) {
			// This is in order to prevent the IE bug with upload of empty files, which crashes the browser
			if (file[0].size <= 0) {
				Docebo.Feedback.show('error', (Yii.t('player', 'Empty file not allowed')), true, true);
				up.removeFile(up.getFile(file[0].id));
				return false;
			}

			var currentValue = $('#LearningVideoSubtitles_lang_code').val();
			var currentText = $('#LearningVideoSubtitles_lang_code option[value="'+currentValue+'"]').text();

			//up.settings.url = up.settings.url + '&lang=' + currentValue;
			up.settings.url = this.options.pluploaderOptions.url + '&extra=' + currentValue;
			up.settings.multipart_params.lang = currentValue;

			// Show header row if it is not shown
			if ($('.subtitles-list-titles').hasClass('hidden')) {
				$('.subtitles-list-titles').removeClass('hidden');
			}

			var newHtml = '<div class="row-fluid subtitles-list-row" id="subtitles-list-row-'+currentValue+'" data-filter-value="'+currentValue+'"  data-filter-text="'+currentText+'" data-real-id="'+currentValue+'">';
			newHtml += '<div class="span3 subtitle-language-text">'+ (currentValue.replace('_', ' ')) + '</div>';
			newHtml += '<div class="span5 subtitle-filename"><i class="fa fa-spinner fa-pulse"></i></div>';
			newHtml += '<div class="span3 subtitle-set-fallback">&nbsp;</div>';
			newHtml += '<div class="span1 subtitle-delete"></div>';
			newHtml += '</div>';
			$('.row-fluid.subtitles-list').append(newHtml);
			$('#LearningVideoSubtitles_lang_code option[value="'+currentValue+'"]').remove();

			up.lang_code = currentValue;

			// --
			up.start();

			//this.log('FilesAdded.');
		},

		UploadProgress: function(up,file) {
			//this.log('Upload Progress.');
		},

		Error: function(up,error) {
			this.log('Error.');
		},

		FileUploaded: function(up,file, info) {
			var infoObj = jQuery.parseJSON(info.response);
			var langCode = '';

			if (infoObj !== null && typeof infoObj === 'object') {
				if (infoObj.hasOwnProperty('extra')) {
					var langCode = infoObj.extra;
				}
			}

			$('#pluploader-container').append('<input type="hidden" value="'+file.name+'" name="subtitles['+langCode+'][original_filename]" id="original_filename_'+langCode+'" />');
			$('#pluploader-container').append('<input type="hidden" value="'+file.target_name+'" name="subtitles['+langCode+'][target_filename]" id="target_filename_'+langCode+'" />');
			$("#subtitles-list-row-"+langCode+" .subtitle-filename").html(file.name);
			$("#subtitles-list-row-"+langCode+" .subtitle-set-fallback").html('<i data-sub-id="'+langCode+'" class="fa fa-check-circle-o fa-lg"></i>');

			$("#subtitles-list-row-"+langCode+" .subtitle-delete").html('<a href="' + this.options.pluploaderOptions.baseDeleteUrl + '&id='+file.target_name+'&langcode='+langCode+'" ' +
				'class="open-dialog delete-sub-link" data-dialog-id="delete-sub-dialog" data-dialog-class="delete-item-modal sub-delete-modal"' +
				' closeOnEscape="true" closeOnOverlayClick="true" data-dialog-title="' + Yii.t('player', 'Delete Subtitle') + '">' +
				'<i class="fa fa-times fa-lg red-text"></i>' +
				'</a>');

			// Increase the number of already added subtitles
			var addedSubsCount = $('.added-subs-count').html();
			addedSubsCount++;
			$('.added-subs-count').html(addedSubsCount);

			this.toggleFallbackHandlers();

			// If it is a first subtitle added for the video make it a fallback subtitle
			if ($('.subtitles-list-row').length == 1) {
				$("#subtitles-list-row-"+langCode+" .fa-check-circle-o").trigger('click');
			}


			$(document).controls();

			//Docebo.log('File Uploaded: ');
			//Docebo.log(file);
		},

		UploadComplete: function(up,files) {
			//Docebo.log('Completed');
		}
		
			
	}
	
})(jQuery);