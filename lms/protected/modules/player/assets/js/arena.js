/**********************************************************************************************
 * LoUploader
 *
 *
 * Serves the LO creation process when File must be uploaded: Scorm, Tincan, File, Video, ...
 *
 */
var LoUploader = function () {
	//this.globalListeners();
};

LoUploader.prototype = {
	uploadFilters: [],
	hide: function () {
		$('#player-arena-uploader-panel').hide();
	},
	show: function (oSettings) {
		Arena.hideAllArenaContent();
		var $panel = $('#player-arena-uploader-panel');
		$panel.show();
		if (oSettings && oSettings.title) {
			$panel.find('.header').html(oSettings.title);
		}
	},
	lo_type: null,
	parent_id: null,
	/**
	 * Ask plUploader to re-render; used to fight IE super browser :-)
	 */
	refresh: function () {
		this.pluploader.refresh();
	},
	
	/**
	 * Update UI to respect 'use_xapi_content_url' checkbox status 
	 */
	showHideXapiFromUrl: function() {
		if (this.lo_type === "tincan") {
			if ($('[name=use_xapi_content_url]').is(':checked')) {
				$('#player-arena-uploader-panel #xapi-content-from-url-settings').show();
				$('#player-arena-uploader-panel #new-tincan-title').closest('.control-group').hide();
				$('#player-arena-uploader-panel #upload-file').hide();
			}
			else {
				$('#player-arena-uploader-panel #xapi-content-from-url-settings').hide();
				$('#player-arena-uploader-panel #new-tincan-title').closest('.control-group').show();
				$('#player-arena-uploader-panel #upload-file').show();
			}
		}
	},
	
	// Started after _upload partial view is loaded
	run: function () {

		this.showHideXapiFromUrl();

		// Create uploader instance
		var pl_uploader = new plupload.Uploader({
			unique_names: true, // If true, you will find the PHYSICAL file name in  $_REQUEST['file']['target_name'], otherwise: $_REQUEST['file']['name']
			chunk_size: '1mb',
			multi_selection: false,
			runtimes: 'html5,flash',
			browse_button: 'pickfiles',
			container: 'player-pl-uploader',
			drop_element: 'player-pl-uploader-dropzone',
			max_file_size: '1024mb',
			url: Arena.uploadFilesUrl,
			multipart: true,
			multipart_params: {
				course_id: Arena.course_id,
				YII_CSRF_TOKEN: Arena.csrfToken
			},
			flash_swf_url: Arena.pluploadAssetsUrl + '/plupload.flash.swf',
			filters: Arena.uploader.uploadFilters
		});

		this.pluploader = pl_uploader;

		// Init event hander
		pl_uploader.bind('Init', function (up, params) {

		});

		// Run uploader
		pl_uploader.init();


		/**
		 * Check the upload feedback (info)
		 * Normally "info" is:  Object {response: "{"jsonrpc" : "2.0", "result" : null, "id" : "id"}", status: 200}
		 * In case of some errors, it is changing to: Object {response: "", status: 0}
		 * We analyze this to decide if upload is going well or not
		 */
		var checkInfo = function (up, file, info) {
			if (info.response === "" || info.status === 0 || info.status >= 400) {
				up.trigger('Error', {
					code: plupload.HTTP_ERROR,
					message: Yii.t('standard', 'Error occured during file upload, please check your connection and try again.'),
					file: file,
					status: info.status
				})
				$('#player-uploaded-cancel-upload').click();
				return false;
			}
			return true;
		};


		pl_uploader.bind('ChunkUploaded', function (up, file, info) {
			checkInfo(up, file, info);
		});


		pl_uploader.bind('FileUploaded', function (up, file, info) {
			checkInfo(up, file, info);
		});


		// Just before uploading file
		pl_uploader.bind('BeforeUpload', function (up, file) {
			$('#player-save-uploaded-lo').addClass("disabled");

			$('.upload-button').hide();
			$('.upload-progress').show();
			$('.player-uploaded-percent').show();
			bootstro.stop('bootstroAddBlockLauncher');
		});


		// Handle 'files added' event
		pl_uploader.bind('FilesAdded', function (up, files) {
			// in edit mode, mark the file as a new upload
			$('#player-arena-uploader').find('#file-new-upload').val(1);

			this.files = [files[0]];
			$("#player-uploaded-filename").html(files[0].name);
			$("#player-uploaded-filesize").html(plupload.formatSize(files[0].size));
			pl_uploader.start();
		});

		// Upload progress
		pl_uploader.bind('UploadProgress', function (up, file) {
			$('#uploader-progress-bar').css('width', file.percent + '%');
			$('#player-uploaded-percent-gauge').html('&nbsp;' + file.percent + '%');
		});

		// On error
		pl_uploader.bind('Error', function (up, error) {
			$('#player-save-uploaded-lo').removeClass("disabled");
			Docebo.Feedback.show('error', error.message, true, false);
		});

		// On Upload complete
		pl_uploader.bind('UploadComplete', function (up, files) {
			$('.docebo-progress')
					.addClass("progress-success")
					.removeClass("progress-striped");
			$('#player-save-uploaded-lo').removeClass("disabled");
			$('#player-uploaded-cancel-upload').hide();
		});

		// cancel upload cancel
		$('#player-uploaded-cancel-upload').on('click', function (e) {
			e.preventDefault();

			// Inline Upload area may have Tinymce attached to 'file-description' which must be destroyed
			// TinyMce.removeEditorById('file-description');

			$('#player-save-uploaded-lo').removeClass("disabled");
			if (pl_uploader) {
				pl_uploader.stop();
				pl_uploader.splice();
			}
			$('.upload-button').show();
			$('.upload-progress').hide();
		});

		// ON CANCEL Upload Button
		$('#player-cancel-uploaded-lo').on('click', function (e) {
			e.preventDefault();

			// Inline Upload area may have Tinymce attached to 'file-description' which must be destroyed
			TinyMce.removeEditorById('file-description');

			// Destroy what we have done
			if (pl_uploader) {
				pl_uploader.stop();
				pl_uploader.destroy();
			}
			$("#player-arena-uploader").html("");
			Arena.setEditCourseMode();
			return false;
		});


		// On SAVE BUTTON click (actual saving of the LO), ajax-submit the form
		$('#upload-lo-form').submit(function () {

			// Button may still be disabled (upload not finished??)
			if ($(this).hasClass('disabled')) {
				return false;
			}

			var $playerArenaUploader = $('#player-arena-uploader');
			var idOrg = parseInt($playerArenaUploader.find('#file-id-org').val());
			var isNewUpload = parseInt($playerArenaUploader.find('#file-new-upload').val());
			var theFile = false;
			var videoSource  = $playerArenaUploader.find('input[name=video-source]:checked').val();
			var externalXapi = $playerArenaUploader.find('#use-xapi-content-url').is(':checked');

			if ((!isNaN(idOrg) && !isNewUpload) || (videoSource == 1) || externalXapi) {
				// do not check if a new file was uploaded
			} else {
				if (pl_uploader.files.length <= 0) {
					Docebo.Feedback.show('error', Yii.t('error', 'Select a file to upload!'));
					return false;
				}
				theFile = pl_uploader.files[0];

				// Make some checks
				if (theFile.percent < 100) {
					Docebo.Feedback.show('error', 'Failed to upload 100% of the file!');
					return false;
				}
			}

			var title = "", $title = $playerArenaUploader.find('#file-title, #new-scorm-title, #new-tincan-title, #new-aicc-title');
			if ($title.length > 0) {
				title = $title.val();
				if (title.length <= 0) {
					Docebo.Feedback.show('error', 'Enter a title!');
					return false;
				}
			}
			
			if (externalXapi) {
				if ( ($('#xapi_content_url_title').val().length <= 0) || 
					 ($('#xapi_content_url_description').val().length <= 0) || 
					 (!$('#xapi_content_url').val())
				   ) {
					Docebo.Feedback.show('error', Yii.t("standard", "TinCan URL, title and description are all required"));
					return false;
				}
			}

			var formHasDescriptionNamedInput = $(this).find(':input[name="description"]').length;

			if (!formHasDescriptionNamedInput) {
				var description = "";
				var $description = $playerArenaUploader.find('textarea[id^="file-description"]');
				if ($description.length > 0) {
					description = $description.val();
				}
			}
			
			var videoUrl = "";
			if(videoSource == 1){
				var $videoUrl = $playerArenaUploader.find('input[name=video_url]').val();
				if($videoUrl.length > 0){
					if(!validateVideoUrl($videoUrl)){
						Docebo.Feedback.show('error', Yii.t('standard', 'The URL is not valid'));
						return false;
					}
					videoUrl = $videoUrl;
				} else{
					Docebo.Feedback.show('error', 'Enter a training matirial URL');
					return false;
				}
			}

			// URI Encode text data (because of IE!)
			// Do NOT forget to URL Decose PHP side
			theFile.name = encodeURIComponent(theFile.name);

			var dataObject = {
				course_id: Arena.course_id,
				parent_id: Arena.uploader.parent_id,
				file: theFile,
				title: title,
				videoSource: videoSource,
				videoUrl: videoUrl
			};

			if (!formHasDescriptionNamedInput) {
				dataObject.description = description;
			}
			
			
			var formOptions = {
				dataType: 'json',
				data: dataObject,
				beforeSubmit: function (a, form, o) {
					// No need to show 'Upload...' message for small files
					if (theFile.size > 30000) {
						$('#player-arena-uploader').slideUp();
						$('#upload-stuff-message').slideDown();
					}
					return true;
				},
				beforeSend: function (x) {
					if (x && x.overrideMimeType) {
						x.overrideMimeType('application/x-www-form-urlencoded; charset=UTF-8');
					}
					$('#upload-lo-form input[type=submit]').attr('disabled','disabled').addClass("disabled");
					return true;
				},
				success: function (res) {
					// Destroy TinyMce editor and switch to Admin view
					TinyMce.removeEditorById('file-description');
					Arena.setEditCourseMode();
					if (res.success) {
						$('#upload-stuff-message').slideUp();
						Docebo.Feedback.show('success', res.data.message);
						Arena.learningObjects.ajaxUpdate('all', function () {
							$(document).trigger('lo-upload-complete');
						});
					}
					else {
						$('#upload-stuff-message').slideUp();
						$('#player-arena-uploader').slideDown();
						Docebo.Feedback.show('error', res.message);
					}
					$('#upload-lo-form input[type=submit]').removeAttr('disabled').removeClass("disabled");
				}
			};

			$('#upload-lo-form').ajaxSubmit(formOptions);


			return false;

		});

	},
	globalListeners: function () {
		
		var that = this;

		$('#player-arena-container').on('change', 'input[name=video-source]', function (){
			if($(this).val() == 1){
				$('#upload-file').addClass('hidden');
				$('#video-seekable').addClass('hidden');
				$('#video-subtitles').addClass('hidden');
				$('#video-url').removeClass('hidden');
			} else{
				$('#video-url').addClass('hidden');
				$('#upload-file').removeClass('hidden');
				$('#video-seekable').removeClass('hidden');
				$('#video-subtitles').removeClass('hidden');
			}
		});

		$('#player-arena-container').on('keyup change paste', 'input[name=video_url]', function(){
			setTimeout(function(){
				var element = $('#player-arena-container input[name=video_url]');
				var value = element.val();
				var infoBox = element.parent().find('.url-valudator-info');

				if(validateVideoUrl(value)){
					infoBox.html('<i class="fa fa-check-circle-o fa-2x" aria-hidden="true"></i>');
					element.val(value.trim());
				} else{
					infoBox.html('<i rel="tooltip" data-original-title="' + Yii.t('standard', 'The URL is not valid') + '" class="fa fa-times fa-2x" aria-hidden="true"></i>');
				}

				infoBox.find('i').tooltip('show');
			}, 100);
		});

		// Bind LO Upload clicks
		$('#player-arena-container').on('click', '.lo-upload', function (e) {
			e.preventDefault();

			var $this = $(this);			
			var lo_type = $this.data('lo-type');

			switch (lo_type) {
				case 'video':
					Arena.uploader.uploadFilters = [
						//{title: "Video files", extensions: "mp4,ogv,webm,flv,avi"}
						{title: "Video files", extensions: Arena.videoExtensionsWhitelist}
					];
					break;
				case 'tincan':
					Arena.uploader.uploadFilters = [
						{title: "Zip files", extensions: "zip"}
					];
					break;
				case 'scormorg':
				case 'aicc':
					Arena.uploader.uploadFilters = [
						{title: "Zip files", extensions: "zip"}
					];
					break;
				case 'deliverable':
					Arena.uploader.uploadFilters = [
						{title: "Video files", extensions: "mp4,ogv,webm,flv"}
					];
					break;
				default:
					Arena.uploader.uploadFilters = [];
					break;
			}

			var folder_key = Arena.learningObjects.management.getActiveFolderKey();
			if (folder_key === false) {
				Docebo.Feedback.show('error', 'Please select a folder');
				return false;
			}

			Arena.hideAllArenaContent();
			var uploaderSettings = {title: $this.data('title')};
			Arena.uploader.show(uploaderSettings);

			var nodeElement = $this.parent().parent().parent().parent().parent();
			var versionContext = nodeElement.find('.version-info');

			var url = Arena.uploadDialogUrl; 

			//Check here if we need to edit a version of a central Repo Object!!!
			// If so - change the URL
			var runUploader = true;
			if(versionContext.length > 0){
				url = $this.attr('href');
				runUploader = false;
			}

			$.ajax({
				url: url,
				type: 'post',
				dataType: 'json',
				data: {
					course_id: Arena.course_id,
					lo_type: lo_type,
					folder_key: folder_key,
					id_org: $(this).data('id-object') // only for editing
				},
				beforeSend: function () {

				}
			}).done(function (res) {
				if (res.success) {
					Arena.uploader.lo_type = lo_type;
					Arena.uploader.parent_id = folder_key;
					//$('#player-arena-uploader').html(res.html);
					$('#player-lo-edit-inline-panel').html(res.html).show();
					//initialize the tags and topics (fancytree and bloodhound) controls

					// PlUploader is good to be run AFTER HTML is rendered!!
					if(runUploader){
						Arena.uploader.run();
					}			
					//Arena.uploader.pluploader.refresh();
				}
			}).always(function () {
				Docebo.scrollTo('#arena-board-top');
			}).fail(function (jqXHR, textStatus, errorThrown) {
				//...
			});

		});
		
		
		$('#player-arena-container').on('change', '#use-xapi-content-url', function(){
			that.showHideXapiFromUrl();
		});

	}


};


/**********************************************************************************************
 * LTI Resources
 *
 *
 * Show Create/Edit LTI resource dialog: title and editable area. Handles the submission process.
 *
 */
var LoLtiEdit = function (options) {
	this.init(options);
	//this.globalListeners();
};


LoLtiEdit.prototype = {
	init: function (options) {
		this.editLtiUrl = options.editLtiUrl;
	},
	show: function (idResource) {

		Arena.hideAllArenaContent();
		var inlineEditPanel = $('#player-lo-edit-inline-panel');
		$.post(this.editLtiUrl, {idResource: idResource, course_id: Arena.course_id}, function (response) {
			if (response.success == true) {
				//panel.show().find('.content').html(response.html);
				inlineEditPanel.html(response.html).show();
				TinyMce.attach('#player-arena-lti-editor-textarea', 'standard_embed', {height: 300});
				Arena.lti.formListeners();
				Docebo.scrollTo('#arena-board-top');
			}
		}, 'json');
	},
	formListeners: function () {
		// ON Google Drive URL entry
		var element = $('#player-arena-uploader input#LearningLTI_url');
		$(element).on('focus', function(){
			if(element.val() == 'https://'){
				element.val('');
			}
		}).on('keypress paste', function(){
			setTimeout(function(){
				var value = element.val();
				var ltiRegex = /^(https:\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
				var ltiMatch = value.match(ltiRegex);
				var infoBox = element.parent().find('.url-valudator-info');

				if(ltiMatch){
					infoBox.html('<i class="fa fa-check-circle-o fa-2x" aria-hidden="true"></i>');
					element.val(value.trim());
				} else {
					infoBox.html('<i rel="tooltip" data-original-title="' + Yii.t('standard', 'The URL is not valid') + '" data-placement="bottom" class="fa fa-times fa-2x" aria-hidden="true"></i>');
				}

				infoBox.find('i').tooltip('show');
			}, 100);
		}).on('blur', function(){
			if(element.val() == ''){
				element.val('https://');
			}
		});

		// ON Cancel button click
		$("#cancel_save_lti").on("click", function (e) {
			e.preventDefault();
			TinyMce.removeEditorById('player-arena-lti-editor-textarea');
			Arena.setEditCourseMode();
		});

		// AJAX-Submit the form (jQuery forms is already loaded)
		$('#lti-editor-form').submit(function () {
			var options = {
				data: {
					//course_id: Arena.course_id,
					//parent_id: Arena.learningObjects.management.getActiveFolderKey()
				},
				dataType: 'json',
				success: function (res) {
					if (res.success == true) {
						Docebo.Feedback.show('success', res.data.message);
						Arena.learningObjects.ajaxUpdate('all', function () {
							$(document).trigger('lo-upload-complete');
						});
						Arena.setEditCourseMode();
						TinyMce.removeEditorById('player-arena-lti-editor-textarea');
					} else {
						Docebo.Feedback.show('error', res.message);
					}
				}
			};

			$(this).ajaxSubmit(options);
			return false;
		});
	},
	globalListeners: function () {
		$('#player-manage-add-lo-button').on('click', '.lo-create-lti', function (e) {
			e.preventDefault();
			Arena.lti.show();
		});
	}
};


/**********************************************************************************************
 * HtmlPage
 *
 *
 * Show Create/Edit HTML Page dialog: title and editable area. Handles the submition process.
 *
 */
var LoHtmlEdit = function (options) {
	this.init(options);
	//this.globalListeners();
};


LoHtmlEdit.prototype = {
	init: function (options) {
		this.editHtmlPageUrl = options.editHtmlPageUrl;
	},
	show: function (idResource) {

		Arena.hideAllArenaContent();
		//var panel = $('#player-arena-htmlpage-editor-panel');
		var inlineEditPanel = $('#player-lo-edit-inline-panel');
		//xxxx
		$.post(this.editHtmlPageUrl, {idResource: idResource, course_id: Arena.course_id}, function (response) {
			if (response.success == true) {
				//panel.show().find('.content').html(response.html);
				inlineEditPanel.html(response.html).show();
				TinyMce.attach('#player-arena-htmlpage-editor-textarea', 'standard_embed', {height: 300});
				Arena.htmledit.formListeners();
				Docebo.scrollTo('#arena-board-top');
			}
		}, 'json');
	},
	formListeners: function () {
		// ON Cancel button click
		$("#cancel_save_htmlpage").on("click", function (e) {
			e.preventDefault();
			TinyMce.removeEditorById('player-arena-htmlpage-editor-textarea');
			Arena.setEditCourseMode();
		});

		// AJAX-Submit the form (jQuery forms is already loaded)
		$('#htmlpage-editor-form').submit(function () {
			var options = {
				data: {
					course_id: Arena.course_id,
					parent_id: Arena.learningObjects.management.getActiveFolderKey()
				},
				dataType: 'json',
				success: function (res) {
					if (res.success == true) {
						Docebo.Feedback.show('success', res.data.message);
						Arena.learningObjects.ajaxUpdate('all', function () {
							$(document).trigger('lo-upload-complete');
						});
						Arena.setEditCourseMode();
						TinyMce.removeEditorById('player-arena-htmlpage-editor-textarea');
						//$(document).trigger('lo-upload-complete');
					} else {
						Docebo.Feedback.show('error', res.message);
					}
					$('#htmlpage-editor-form input[type=submit]').removeAttr('disabled').removeClass("disabled");
				},
				beforeSubmit: function (arr, $form, options) {
					$('#htmlpage-editor-form input[type=submit]').attr('disabled','disabled').addClass("disabled");
					return true;
				}
			};

			$(this).ajaxSubmit(options);
			return false;
		});
	},
	globalListeners: function () {
		$('#player-manage-add-lo-button').on('click', '.lo-create-htmlpage', function (e) {
			e.preventDefault();
			Arena.htmledit.show();
		});
	}



};


/**********************************************************************************************
 * Google Drive
 *
 *
 * Show Create/Edit Google Drive Page dialog: title and editable area. Handles the submission process.
 *
 */
var LoGoogleDriveEdit = function (options) {
	this.init(options);
	//this.globalListeners();
};


LoGoogleDriveEdit.prototype = {
	init: function (options) {
		this.editGoogleDriveUrl = options.editGoogleDriveUrl;
	},
	show: function (idResource) {

		Arena.hideAllArenaContent();
		//var panel = $('#player-arena-googledrive-editor-panel');
		var inlineEditPanel = $('#player-lo-edit-inline-panel');
		//xxxx
		$.post(this.editGoogleDriveUrl, {idResource: idResource, course_id: Arena.course_id}, function (response) {
			if (response.success == true) {
				//panel.show().find('.content').html(response.html);
				inlineEditPanel.html(response.html).show();
				TinyMce.attach('#player-arena-googledrive-editor-textarea', 'standard_embed', {height: 300});
				Arena.googledrive.formListeners();
				Docebo.scrollTo('#arena-board-top');
			}
		}, 'json');
	},
	formListeners: function () {
		// ON Google Drive URL entry
		$('#player-arena-uploader').on('keypress paste', 'input#LearningGoogledoc_url', function() {
			setTimeout(function () {
				var element = $('#player-arena-uploader input#LearningGoogledoc_url');
				var value = element.val();
				var googleDriveRegex = /^(?:https?:\/\/docs.google.com\/)(a\/.{3,}\/)?(?:document|spreadsheets|presentation|drawings)(?:\/d\/)(.+)(\/pub|\/pubhtml|\/embed|\/preview|\/htmlembed)/;
				var googleDriveMatch = value.match(googleDriveRegex);
				var infoBox = element.parent().find('.url-valudator-info');

				if (googleDriveMatch) {
					infoBox.html('<i class="fa fa-check-circle-o fa-2x" aria-hidden="true"></i>');
					element.val(value.trim());
				} else {
					infoBox.html('<i rel="tooltip" data-original-title="' + Yii.t('standard', 'The URL is not valid') + '" data-placement="bottom" class="fa fa-times fa-2x" aria-hidden="true"></i>');
				}

				infoBox.find('i').tooltip('show');
			}, 100);
		});
		// ON Cancel button click
		$("#cancel_save_googledrive").on("click", function (e) {
			e.preventDefault();
			TinyMce.removeEditorById('player-arena-googledrive-editor-textarea');
			Arena.setEditCourseMode();
		});

		// AJAX-Submit the form (jQuery forms is already loaded)
		$('#googledrive-editor-form').submit(function () {
			var options = {
				data: {
					//course_id: Arena.course_id,
					//parent_id: Arena.learningObjects.management.getActiveFolderKey()
				},
				dataType: 'json',
				success: function (res) {
					if (res.success == true) {
						Docebo.Feedback.show('success', res.data.message);
						Arena.learningObjects.ajaxUpdate('all', function () {
							$(document).trigger('lo-upload-complete');
						});
						Arena.setEditCourseMode();
						TinyMce.removeEditorById('player-arena-googledrive-editor-textarea');
						//$(document).trigger('lo-upload-complete');
					} else {
						Docebo.Feedback.show('error', res.message);
					}
				}
			};

			$(this).ajaxSubmit(options);
			return false;
		});
	},
	globalListeners: function () {
		$('#player-manage-add-lo-button').on('click', '.lo-create-googledrive', function (e) {
			e.preventDefault();
			Arena.googledrive.show();
		});
	}
};

/*********************************************************************************************\
 * Central Repository
 *
 * Show Create/Edit Central Repository dialog.
 */

var LoCentralRepoEdit = function(options){
	if(typeof options !== "undefined"){
		this.init(options);
	}
};

LoCentralRepoEdit.prototype = {
	init: function(options){
		this.editUrl = options.editUrl;
	},

	formListeners: function(){
		var $this = this;

		$(document).off('change', 'form#edit-shared-lo-form #enable-custom-view').on('change', 'form#edit-shared-lo-form #enable-custom-view', function(e){
			if($(this).is(':checked')){
				$(this).parent().parent().parent().find('.overlay').css('display', 'none');
			} else{
				$(this).parent().parent().parent().find('.overlay').css('display', 'block');
			}
		});

		$(document).off('click', '.player-central-lo-shared-settings #player-cancel-uploaded-lo').on('click', '.player-central-lo-shared-settings #player-cancel-uploaded-lo', function(){
			$this.resetPanel();
		});

		$(document).off('submit', 'form#edit-shared-lo-form').on('submit', 'form#edit-shared-lo-form', function(e){
			e.preventDefault();
			var options = {
				dataType: 'json',
				success: function (res) {
					if (res.success == true) {
						if (res.data.showConfirmationDialog == true) {
							var dialogId = 'version-change-lo-dialog';
							$('<div/>').dialog2({
								autoOpen: true, // We will open the dialog later
								id: dialogId,
								title: Yii.t('standard', 'Version change'),
								modalClass: 'version-change-modal'
							});
							$('#' + dialogId).html(res.html);
						} else {
							Docebo.Feedback.show('success', res.message);
							Arena.learningObjects.ajaxUpdate('all', function () {
								$(document).trigger('lo-upload-complete');
							});
							Arena.setEditCourseMode();
						}
					}
					else
						Docebo.Feedback.show('error', res.message);
				}
			};

			$(this).ajaxSubmit(options);
			return false;
		});

		$(document).off('click', '.version-change-modal form#save-shared-lo-form input[name="confirm-button"]').on('click', '.version-change-modal form#save-shared-lo-form input[name="confirm-button"]', function(e){
			e.preventDefault();
			var options = {
				dataType: 'json',
				success: function (res) {
					$('#version-change-lo-dialog').dialog2('close');
					Docebo.Feedback.show('success', Yii.t('standard', '_OPERATION_SUCCESSFUL'));
					$this.resetPanel();
					if (res.success == true) {
						Docebo.Feedback.show('success', res.message);
						Arena.learningObjects.ajaxUpdate('all', function () {
							$(document).trigger('lo-upload-complete');
						});
					} else
						Docebo.Feedback.show('error', res.message);
				}
			};

			$('.version-change-modal form#save-shared-lo-form').ajaxSubmit(options);
			return false;
		});

		$(document).off('click', '.version-change-modal #player-cancel-save-version').on('click', '.version-change-modal #player-cancel-save-version', function(e){
			$('#version-change-lo-dialog').dialog2('close');
			$this.resetPanel();
		});
	},

	resetPanel: function(){
		$("#player-arena-uploader").html("");
		Arena.setEditCourseMode();
	},

	globalListeners: function(){
		var $this = this;

		var jwizard = new jwizardClass({
			dialogId		: 'push-to-course-dialog',
			dispatcherUrl	: Docebo.createAbsoluteAdminUrl('centralrepo/CentralRepoPushLOToCourse/wizard', []),
			alwaysPostGlobal: true,
			debug			: false
		});

		$(document).on('dialog2.closed', '#push-lo-to-single-course', function(){
			Arena.learningObjects.ajaxUpdate('all', function () {
				$(document).trigger('lo-upload-complete');
			});
		});
	}
};



/**********************************************************************************************
 * Elucidat
 *
 *
 * Show Create/Edit Elucidat dialog: title and editable area. Handles the submition process.
 *
 */
var LoElucidatEdit = function (options) {
	this.init(options);
	//this.globalListeners();
};


LoElucidatEdit.prototype = {
	init: function (options) {
		this.editPageUrl = options.editPageUrl;
	},
	show: function (idResource, step) {

		Arena.hideAllArenaContent();
		var that = this;
		var inlineEditPanel = $('#player-lo-edit-inline-panel');

		// Show loader, because Elucidat must do some background work before show some html (API calls etc.)
		inlineEditPanel.html(Arena.ajaxLoader).show();
		$('.ajaxloader').show();

		var url = this.editPageUrl;

		url = url + "&step=" + step;

		$.post(url, {idResource: idResource, course_id: Arena.course_id}, function (response) {
			if (response.success == true) {
				inlineEditPanel.html(response.html).show();
				if (step == 2)
					TinyMce.attach('#player-arena-elucidat-editor-textarea', 'standard_embed', {height: 100});
				that.formListeners();
				Docebo.scrollTo('#arena-board-top');
			}
			else {
				inlineEditPanel.html("");
			}
		}, 'json');

	},
	formListeners: function () {
		var that = this;

		// ON Cancel button click
		$("#cancel_save_elucidat").on("click", function (e) {
			e.preventDefault();
			TinyMce.removeEditorById('player-arena-elucidat-editor-textarea');
			Arena.setEditCourseMode();
		});

		//$(document).one('click', '.lo-select-elucidat', function(){
		//	that.show(null, 2); // go to second step, to select project/release
		//});
		$('.lo-select-elucidat').on('click', function () {
			that.show(null, 2); // go to second step, to select project/release
		});
		// AJAX-Submit the form (jQuery forms is already loaded)
		$('#elucidat-editor-form').submit(function () {
			var options = {
				data: {
					course_id: Arena.course_id,
					parent_id: Arena.learningObjects.management.getActiveFolderKey()
				},
				dataType: 'json',
				success: function (res) {
					if (res.success == true) {
						Docebo.Feedback.show('success', res.data.message);
						Arena.learningObjects.ajaxUpdate('all', function () {
							$(document).trigger('lo-upload-complete');
						});
						Arena.setEditCourseMode();
						TinyMce.removeEditorById('player-arena-elucidat-editor-textarea');
					} else {
						Docebo.Feedback.show('error', res.message);
					}
				}
			};

			$(this).ajaxSubmit(options);
			return false;
		});


	},
	globalListeners: function () {
		var that = this;
		$('#player-manage-add-lo-button').on('click', '.lo-create-elucidat', function (e) {
			e.preventDefault();
			that.show(null, 1);
		});
	}



};




/**********************************************************************************************
 * Deliverable
 *
 *
 * Show Create/Edit Deliverable dialog: title and editable area. Handles the submition process.
 *
 */
var LoDeliverable = function (options) {
	this.init(options);
	//this.globalListeners();
};


LoDeliverable.prototype = {
	init: function (options) {
		this.editDeliverableUrl = options.editDeliverableUrl;
	},
	show: function (idResource) {

		Arena.hideAllArenaContent();
		//var panel = $('#player-arena-htmlpage-editor-panel');
		var inlineEditPanel = $('#player-lo-edit-inline-panel');
		$.post(this.editDeliverableUrl, {
			idResource: idResource,
			course_id: Arena.course_id
		}, function (response) {
			if (response.success == true) {
				//panel.show().find('.content').html(response.html);
				inlineEditPanel.html(response.html).show();
				TinyMce.attach('#player-arena-deliverable-editor-textarea', 'standard_embed', {height: 300});
				Arena.deliverable.formListeners();
				Docebo.scrollTo('#arena-board-top');

			}
		}, 'json');
	},
	formListeners: function () {
		// ON Cancel button click
		$("#cancel_save_deliverable").on("click", function (e) {
			e.preventDefault();
			TinyMce.removeEditorById('player-arena-deliverable-editor-textarea');
			Arena.setEditCourseMode();
		});

		// AJAX-Submit the form (jQuery forms is already loaded)
		$('#deliverable-editor-form').submit(function () {
			var options = {
				data: {
					course_id: Arena.course_id,
					parent_id: Arena.learningObjects.management.getActiveFolderKey()
				},
				dataType: 'json',
				success: function (res) {
					if (res.success == true) {
						Docebo.Feedback.show('success', res.data.message);
						Arena.learningObjects.ajaxUpdate('all', function () {
							$(document).trigger('lo-upload-complete');
						});
						Arena.setEditCourseMode();
						TinyMce.removeEditorById('player-arena-deliverable-editor-textarea');
						//$(document).trigger('lo-upload-complete');
					} else {
						Docebo.Feedback.show('error', res.message);
					}
					$('#deliverable-editor-form input[type=submit]').removeAttr('disabled').removeClass("disabled");
				},
				beforeSubmit: function (arr, $form, options) {
					$('#deliverable-editor-form input[type=submit]').attr('disabled','disabled').addClass("disabled");
					return true;
				}
			};

			$(this).ajaxSubmit(options);
			return false;
		});
	},
	globalListeners: function () {
		$('#player-manage-add-lo-button').on('click', '.lo-create-deliverable', function (e) {
			e.preventDefault();
			Arena.deliverable.show();
		});
	}



};


/**********************************************************************************************
 * TEST
 *
 *
 *  Create TEST Dialog and submission. NO EDIT!
 *
 */
var LoTest = function (options) {
	this.init(options);
	//this.globalListeners();
};


LoTest.prototype = {
	init: function (options) {
	},
	hide: function () {
		$('.player-arena-test-create-panel').hide();
	},
	show: function () {
		that = this;
		var inlineEditPanel = $('#player-lo-edit-inline-panel');
		$.post(Arena.createTestUrl, {}, function (response) {
			if (response.success == true) {
				inlineEditPanel.html(response.html).show();

				TinyMce.attach('#player-arena-test-editor-textarea', {height: 300});
				that.formListeners();
				Docebo.scrollTo('#arena-board-top');
			}
		}, 'json');
	},
	formListeners: function () {
		// ON Cancel button click
		$("#cancel_save_test").on("click", function (e) {
			e.preventDefault();
			TinyMce.removeEditorById('player-arena-test-editor-textarea');
			Arena.setEditCourseMode();
		});

		// AJAX-Submit the form (jQuery forms is already loaded)
		$('#test-create-form').submit(function () {
			var options = {
				data: {
					course_id: Arena.course_id,
					parent_id: Arena.learningObjects.management.getActiveFolderKey()
				},
				dataType: 'json',
				success: function (res) {
					if (res.success == true) {
						Docebo.Feedback.show('success', res.data.message);
						Arena.learningObjects.ajaxUpdate();
						TinyMce.removeEditorById('player-arena-test-editor-textarea');
						redirectUrl = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=test/default/edit&id_object=' + res.data.organizationId + '&course_id=' + Arena.course_id;
						window.location.href = redirectUrl;
					} else {
						$('#test-create-form input[type=submit]').removeAttr('disabled').removeClass("disabled");
					}
				},
				beforeSubmit: function (arr, $form, options) {
					$('#test-create-form input[type=submit]').attr('disabled','disabled').addClass("disabled");
					return true;
				}
			};
			$(this).ajaxSubmit(options);
			return false;
		});
	},
	globalListeners: function () {
		// On "CREATE TEST"
		$('#player-manage-add-lo-button').on('click', '.lo-create-test', function (e) {
			e.preventDefault();
			Arena.hideAllArenaContent();
			Arena.test.show();
		});
	}

};


/**********************************************************************************************
 * POLL
 *
 *
 * Create dialog and submisison. No EDIT!
 *
 */
var LoPoll = function (options) {
	this.init(options);
	//this.globalListeners();
};


LoPoll.prototype = {
	init: function (options) {
	},
	hide: function () {
		$('.player-arena-poll-create-panel').hide();
	},
	show: function () {
		that = this;
		var inlineEditPanel = $('#player-lo-edit-inline-panel');
		$.post(Arena.createPollUrl, {}, function (response) {
			if (response.success == true) {
				inlineEditPanel.html(response.html).show();
				TinyMce.attach('#player-arena-poll-editor-textarea', {height: 300});
				that.formListeners();
				Docebo.scrollTo('#arena-board-top');
			}
		}, 'json');
	},
	formListeners: function () {
		// ON Cancel button click
		$("#cancel_save_poll").on("click", function (e) {
			e.preventDefault();
			Arena.setEditCourseMode();
		});

		// AJAX-Submit the form (jQuery forms is already loaded)
		$('#poll-create-form').submit(function () {
			var options = {
				data: {
					course_id: Arena.course_id,
					parent_id: Arena.learningObjects.management.getActiveFolderKey()
				},
				dataType: 'json',
				success: function (res) {
					if (res.success == true) {
						Docebo.Feedback.show('success', res.data.message);
						Arena.learningObjects.ajaxUpdate(function () {
							$(document).trigger('lo-upload-complete');
						});

						redirectUrl = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=poll/default/edit&id_object=' + res.data.organizationId + '&course_id=' + Arena.course_id;
						window.location.href = redirectUrl;
					} else {
						$('#poll-create-form input[type=submit]').removeAttr('disabled').removeClass("disabled");
					}
				},
				beforeSubmit: function (arr, $form, options) {
					$('#poll-create-form input[type=submit]').attr('disabled','disabled').addClass("disabled");
					return true;
				}
			};
			$(this).ajaxSubmit(options);
			return false;
		});
	},
	globalListeners: function () {
		// On CREATE POLL
		$('#player-manage-add-lo-button').on('click', '.lo-create-poll', function (e) {
			e.preventDefault();
			Arena.hideAllArenaContent();
			Arena.poll.show();
		});

	}


};


/**********************************************************************************************
 * Organizer
 *
 *
 * Create NEW folder dialog.
 *
 */
var LoOrganizer = function (options) {
	this.init(options);
	//this.globalListeners();
};


LoOrganizer.prototype = {
	init: function () {

	},
	show: function (orgId) {
		var folder_key = Arena.learningObjects.management.getActiveFolderKey();
		if (folder_key === false) {
			Docebo.Feedback.show('error', 'Please select a folder');
			return false;
		}

		var url = Arena.createFolderDialogUrl + '&parent_id=' + folder_key;
		if (orgId) {
			url += '&org_id=' + orgId;
		}
		var inlineEditPanel = $('#player-lo-edit-inline-panel');

		$.post(url, {}, function (response) {
			if (response.success == true) {
				Arena.hideAllArenaContent();
				inlineEditPanel.html(response.html).show();
				Arena.organizer.formListeners();
			} else {
				Docebo.Feedback.show('error', response.message);
			}
		}, 'json');
	},
	formListeners: function () {

		$('#cancel_save_newfolder').click(function (e) {
			e.preventDefault();
			Arena.setEditCourseMode();
		});

		$('#create-newfolder-form').ajaxForm({
			dataType: 'json',
			success: function (result) {
				if (result.success == true) {
					Arena.learningObjects.ajaxUpdate();
					Arena.setEditCourseMode();
				} else {
					Docebo.Feedback.show('error', result.message);
				}
				$('#create-newfolder-form input[type=submit]').removeAttr('disabled').removeClass("disabled");
			},
			beforeSubmit: function (arr, $form, options) {
				$('#create-newfolder-form input[type=submit]').attr('disabled','disabled').addClass("disabled");
				return true;
			}
		});
	},
	globalListeners: function () {
		// On CREATE FOLDER
		$('#create-folder').on('click', function (e) {
			e.preventDefault();
			Arena.organizer.show();
		});

		$('#player-arena-container').on('click', '.lo-edit-folder', function (e) {
			var incomingFolderIdObject = $(this).data('id-object');
			e.preventDefault();
			Arena.organizer.show(incomingFolderIdObject);
		});

	}


};


/**********************************************************************************************
 *  Launcher
 *
 *
 *  All the LO launching business is done here - dispatching, opening lightboxes,
 *  opening new windows, redirecting and so.
 *
 *
 */
var LoLauncher = function (options) {
	this.init(options);
};

var isTablet = function() {

	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		var w = $(window).width();
		var h = $(window).height();
		var device_width = Math.max(w,h);

		if (device_width > 767) {
			return true;
		}
	}
	return false;
};

var isSmartphone = function() {

	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		var w = $(window).width();
		var h = $(window).height();
		var device_width = Math.max(w,h);

		if (device_width <= 767) {
			return true;
		}
	}
	return false;
};

LoLauncher.prototype = {

	init: function (options) {

	},
	isTablet: function() { return isTablet(); },
	isSmathphone: function() { return isSmartphone(); },

	// Choose (dispatch) appropriate LO launcher (per type) and run it
	dispatch: function (loData, callback) {
		//Fake previous url for "overriding" the browser's back button
		var url = window.location.href;
		var index = url.indexOf("?");
		var parameters = url.substring(index);
		if (parameters.indexOf("#training") < 0) {
			History.pushState({state: 1, rand: Math.random()}, null, parameters + "#training");
			$(window).on('hashchange', function () {
				//$(window).off('hashchange');
				location.reload();
				return false;
			});
		}
		// code needed for the fancybox
		$.fancybox.helpers.doceboTitle = {
			beforeShow: function (opts) {
				var headerWidget = opts.headerWidget;
				var F = $.fancybox,
						title = $('<div class="fancybox-docebo-title"><span>' + opts.title + '</span>'+headerWidget+'</div>');
				title.appendTo(F.skin);
				if(headerWidget != "undefined" && headerWidget != ''){
					if($('.courseLoHeaderApp7020 .triggerOpenSidebar').length > 0){
						$('.courseLoHeaderApp7020 .triggerOpenSidebar').attr("id-lo", opts.loKey);
					}
				}
			}
		};

		// No folders, no locked LOs!
		if (loData.isFolder || loData.locked || (loData.type == 'scormorg')) {
			return false;
		}

		// Check if the course we are running
		//   1. Is a Marketplace course
		//   2. Has ZERO available seats
		//   3. And user has NO seats taken before
		//   If all match: show feedback message and return
		if (Arena.courseInfo.isMpCourse && !Arena.courseInfo.mpSeatsUnlimited) {
			if ((Arena.courseInfo.availableSeats == 0) && (!Arena.courseInfo.userHasSeat)) {
				Docebo.Feedback.show('error', Yii.t('standard', 'No seats available'));
				return false;
			}
		}
        var isDone = false;
		var that = this;
		$(document)
				.unbind('continue-lo-play')
				.on('continue-lo-play', function () {

					// We are launching a LO! So, first, we have to track the "course access"
					$.ajax({
						url: Docebo.lmsAbsoluteBaseUrl + '/index.php?r=//player/training/axTrackCourseAccess',
						type: 'post',
						dataType: 'json',
						data: {
						course_id: Arena.course_id,
						lo_data: loData ? loData : null
						},
						beforeSend: function () {
						}
					}).done(function (res) {
                    isDone = true;
					// Change the Learner/Admin View switch
					Arena.setSwitchToLearnerView();
					Arena.lastPlayedLo = loData.key;
					switch (loData.type) {
						case 'authoring':
							that.authoring(loData);
							break;

						case 'htmlpage':
							that.htmlpage(loData);
							break;

						case 'deliverable':
						case 'video':
						case 'file':
						case 'poll':
						case 'test':
							that.launchpad(loData);
							break;

						case 'sco':
							that.scormSco(loData);
							break;

						case 'tincan':
							that.tincan(loData);
							break;

						case 'aicc':
							that.aicc(loData);
							break;

						case 'lti':
							that.lti(loData);
							break;

						case 'elucidat':
							that.elucidat(loData);
							break;

						case 'googledrive':
							that.googledrive(loData);
							break;

						default:
							Docebo.Feedback.show('error', loData.type + " launcher is coming soon....");
							break;
					}
				});

                if(isTablet()&& !isDone){
                    Arena.setSwitchToLearnerView();
                    switch (loData.type) {
                        case 'sco':
                            that.scormSco(loData);
                            break;

                        case 'tincan':
                            that.tincan(loData);
                            break;

                        case 'aicc':
                            that.aicc(loData);
                            break;

                        case 'elucidat':
                            that.elucidat(loData);
                            break;

                        case 'authoring':
                            that.authoring(loData);
                            break;
                    }
                }

					if ($.isFunction(callback)) {
						callback.call(this);
					}
				});

		var cancelPlay = false;

		$(document)
				.unbind('cancel-lo-play')
				.on("cancel-lo-play", function () {
					cancelPlay = true;
				});

		// let plugins perform an action before playing a LO
		$(document).trigger('before-lo-play');

		if (!cancelPlay) {
			$(document).trigger('continue-lo-play');
		}

		return true;
	},
	// Load or Run/Show a so called 'common launch pad': some image plus a big PLAY button
	commonLaunchpad: {
		load: function (loData, doShow) {

			// !!! Compose the Launchpad URL ( <module>/default/axLaunchpad) !!!
			//var url = 'lms/?r=player/training/axGetCommonLaunchpad';
			var url = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=player/training/axGetCommonLaunchpad';

			// Load the launchpad
			$.ajax({
				url: url,
				type: 'post',
				dataType: 'json',
				data: {
					course_id: Arena.course_id, // We need course_id for ALL requests
					lo_data: loData ? loData : null
				},
				beforeSend: function () {

					if (Arena.currentView === 'play') {
						$('#player-arena-launchpad').hide();
						if (Arena.playerLayout !== 'listview') {
							Arena.setLaunchpadMode();
						} else if (Arena.playerLayout === 'listview') {
							Launcher.resetToListView();
						}
					}

				}
			})
					.done(function (res) {
						if (res.success == true) {

							$('#player-arena-launchpad').html(res.data.html);
							if (loData) {
								$("#common-launchpad-play-object").on("click", function () {
									Launcher.dispatch(loData);
								});
							}

							// Well, we loaded it, but should we show also?
							if (doShow && Arena.playerLayout !== 'listview') {
								Arena.setLaunchpadMode();
							} else if (Arena.playerLayout === 'listview') {
								Launcher.resetToListView();
							}

							// This is so LMS plugins can also detect when the launchpad was
							// updated without having to touch core JS code
							$('#player-arena-launchpad').trigger('launchpadUpdated');

						}
						else {
							Docebo.Feedback.show('error', res.message);
						}
					})
					.always(function () {
						//Docebo.activateAjaxFilter();
					})
					.fail(function (jqXHR, textStatus, errorThrown) {
					});

		},
		run: function (data) {
			Arena.setLaunchpadMode();
		}
	},

	/**
	 * Show LTI's
	 */
	lti: function(loData){
		var launchType = loData.lti_target;

		var launchLink = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=LtiApp/LtiApp/show&id_object=' + loData.key + '&course_id=' + Arena.course_id;

		switch(launchType){
			case 'iframe':
			case 'window':
				// Change to launchpad view
				Arena.setLaunchpadMode();
				this.inline(launchLink, loData);
				break;
			case 'lightbox':
				this.lightbox(launchLink, loData);
				break;
			case 'fullscreen':
				window.location.href = launchLink;
				break;
			default:
			// nothing to do yet in this case
		}
		Arena.disableQuickNav();
		Arena.learningObjects.ajaxUpdate();
	},

	/**
	 * Show HTML Page learning object
	 */
	htmlpage: function (loData) {
		// Get the launch type (LO specific)
		// Possible open mode parameters are:
		// loData.params.openmode, .desktop_openmode, .tablet_openmode, .smartphone_openmode
		// Use them on your discretion

		var launchType = loData.params.openmode ? loData.params.openmode : 'inline';

		// However, Force INLINE for now; Initially, HTML page is required to be inline only, but.. we may change this
		launchType = 'inline';

		var launchLink = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=htmlpage/default/content&id_object=' + loData.key + '&course_id=' + Arena.course_id;

		var keepAliveUrl = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=htmlpage/default/keepAlive&course_id=' + Arena.course_id;
		setTimeout("Launcher.keepAlive('"+keepAliveUrl+"')", 5*60*1000);

		switch (launchType) {

			case 'inline' :
				// Change to launchpad view
				Arena.setLaunchpadMode();
				this.inline(launchLink, loData);
				break;

			case 'lightbox':
				this.lightbox(launchLink, loData);
				break;

			case 'redirect':
				window.location.href = launchLink;
				break;

			case 'popup':
			default:
				// IE is picky about the window name argument.
				// It doesn't like spaces, dashes, or other punctuation.
				var windowName = loData.title.replace(/\s/g, '_');
				try {
					window.open(launchLink, windowName);
				}
				catch (e) {
					window.open(launchLink, '_blank');
				}
				break;

		}
            Arena.disableQuickNav();
            Arena.learningObjects.ajaxUpdate();

    },
	/**
	 * Show Google Drive documents
	 */
	googledrive: function(loData){
		// By default, this will be 'inline', can be extended later on to 'lightbox', 'redirect' or 'popup'
		var launchType = 'inline';

		var launchLink = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=GoogleDriveApp/GoogleDriveApp/show&id_object=' + loData.key + '&course_id=' + Arena.course_id;

		switch(launchType){
			case 'inline' :
				// Change to launchpad view
				Arena.setLaunchpadMode();
				this.inline(launchLink, loData);
				break;
			default:
				// nothing to do yet in this case
		}
		Arena.disableQuickNav();
		Arena.learningObjects.ajaxUpdate();
	},
	// Run LO using Launchpad (inline): file, html, ...
	launchpad: function (data) {

		Arena.setLaunchpadMode();

		// !!! Compose the Launchpad URL ( <module>/default/axLaunchpad) !!!
		var url = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=' + data.type + '/default/axLaunchpad';

		// Load the launchpad
		$.ajax({
			url: url,
			type: 'post',
			dataType: 'json',
			data: {
				course_id: Arena.course_id, // We need course_id for ALL requests
				id_object: data.key
			},
			beforeSend: function () {
			}
		})
				.done(function (res) {
					if (res.success == true) {
						// We expect res.data.html OR res.html
						// res.html will be subject to global .ajaxFilter!!
						// res.data.html - not. You decide which one to use

						if (Arena.playerLayout !== 'listview') {
							Arena.setLaunchpadMode();
						} else if (Arena.playerLayout === 'listview') {
							//Launcher.resetToListView();
						}

						if (res.data != null && typeof (res.data.html) !== 'undefined') {
							$('#player-arena-launchpad').html(res.data.html);
						}
						else {
							$('#player-arena-launchpad').html(res.html);
						}


						Arena.disableQuickNav();
						Arena.learningObjects.ajaxUpdate();

						if (Arena.playerLayout === 'navigator' || Arena.playerLayout==='listview') {
							PlayerNavigator.setState('playing');
							PlayerNavigator.setCurrentKey(data.key);
							PlayerNavigator.reload();
						}

						if ($('.courseLoHeaderApp7020 .triggerOpenSidebar').length > 0) {
							$('.courseLoHeaderApp7020 .triggerOpenSidebar').attr("id-lo", data.key);
						}

					}
					else {
						Docebo.Feedback.show('error', res.message);
					}
				})
				.always(function () {
					Docebo.activateAjaxFilter();
				})
				.fail(function (jqXHR, textStatus, errorThrown) {
				});

	},
	// SCO launcher
	scormSco: function (loData) {

		// This should be LO configuration
		var launchType = loData.params.openmode ? loData.params.openmode : 'lightbox';

		// loData.params.openmode, .desktop_openmode, .tablet_openmode, .smartphone_openmode
		if (this.isTablet()) {

			launchType = loData.params.tablet_openmode ? loData.params.tablet_openmode : 'fullscreen';
		} else if (this.isSmathphone()) {

			launchType = loData.params.smartphone_openmode ? loData.params.smartphone_openmode : 'fullscreen';
		} else {

			launchType = loData.params.desktop_openmode ? loData.params.desktop_openmode : launchType;
		}


		var that = this; // i.e. Launcher object
		var async = true;

		if (launchType == 'popup') {
			async = false;

			// let's preopen the windows or the popup blocker will it

			// IE is picky about the window name argument.
			// It doesn't like spaces, dashes, or other punctuation.
			var windowName = loData.title.replace(/\s/g, '_'),
					popup = false,
					loading_url = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=player/training/loading&course_id=' + Arena.course_id;
			try {
				popup = window.open(loading_url, windowName);
			} catch (e) {
				popup = window.open(loading_url, '_blank');
			}

			windowCheck(popup, loData);
		}

		// Get launching information and continue
		$.ajax({
			async: async,
			url: Docebo.lmsAbsoluteBaseUrl + '/index.php?r=scormorg/default/axGetLaunchInfo',
			type: 'post',
			dataType: 'json',
			data: {
				course_id: Arena.course_id, // We need course_id for ALL requests
				data_key: loData.key,
				id_org: loData.idOrganization,
				id_resource: loData.idResource,
				id_scorm_item: loData.idScormItem,
				chapter: loData.identifier,
				launch_type: launchType
			}
		}).done(function (res) {
			if (res.success == true) {
				var data = res.data;
				var launchLink = data.launch_url
						+ "?host=" + encodeURIComponent(data.host)
						+ "&id_user=" + encodeURIComponent(data.id_user)
						+ "&id_reference=" + encodeURIComponent(data.id_reference)
						+ "&scorm_version=" + encodeURIComponent(data.scorm_version)
						+ "&id_resource=" + encodeURIComponent(data.id_resource)
						+ "&id_item=" + encodeURIComponent(data.id_item)
						+ "&idscorm_organization=" + encodeURIComponent(data.idscorm_organization)
						+ "&id_package=" + encodeURIComponent(data.id_package)
						+ "&id_course=" + encodeURIComponent(Arena.course_id)
						+ "&launch_type=" + encodeURIComponent(launchType);
						// + "&auth_code=" + encodeURIComponent(data.auth);
				if (data.debug)
					launchLink += "&debug=1";

				//Launcher.commonLaunchpad.load(loData);
				if (Arena.playerLayout === 'listview') {
					// Course player layout is List View

					// Manually trigger hiding the left menu objects tree if opened
					if ($('#player-lonav').attr("data-opened") == "true") {
						Arena.hideLoNavigation();
					} else {
						Arena.showLoNavigation();
					}
				} else {
					// Play button view
					Arena.toggleLoNavigation();
				}

				switch (launchType) {

					case 'inline' :
						that.inline(launchLink, loData);
						break;

					case 'lightbox':
						that.lightbox(launchLink, loData);
						break;

					case 'redirect':
						window.location.href = launchLink;
						break;

					case 'fullscreen':
						window.location.href = launchLink;
						break;

					case 'popup':
					default:
						if (popup) {
							if (loData.width && loData.height) {
								try {
									popup.resizeTo(loData.width, loData.height);
								} catch (e) {
								}
							}
							$('#player-arena-launchpad').html('').hide();
							popup.location = launchLink;
						}
						break;
				} // end of launch
			}
			else {
				Docebo.Feedback.show('error', res.message);
			}
		});
	},
	// AICC Item (AU) launcher
	// Very similar to SCO launcher, copy & paste & modify
	aicc: function (loData) {

		// This should be LO configuration
		var launchType = loData.params.openmode ? loData.params.openmode : 'lightbox';

		// loData.params.openmode, .desktop_openmode, .tablet_openmode, .smartphone_openmode
		if (this.isTablet()) {
			launchType = loData.params.tablet_openmode ? loData.params.tablet_openmode : 'fullscreen';
		} else if (this.isSmathphone()) {
			launchType = loData.params.smartphone_openmode ? loData.params.smartphone_openmode : 'fullscreen';
		} else {
			launchType = loData.params.desktop_openmode ? loData.params.desktop_openmode : launchType;
		}

		var that = this; // i.e. Launcher object
		var async = true;

		if (launchType == 'popup') {
			async = false;

			// let's preopen the windows or the popup blocker will it

			// IE is picky about the window name argument.
			// It doesn't like spaces, dashes, or other punctuation.
			var windowName = loData.title.replace(/\s/g, '_'),
					popup = false,
					loading_url = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=player/training/loading&course_id=' + Arena.course_id;
			try {
				popup = window.open(loading_url, windowName);
			} catch (e) {
				popup = window.open(loading_url, '_blank');
			}

			windowCheck(popup, loData);
		}

		// Get launching information and continue
		$.ajax({
			async: async,
			url: Docebo.lmsAbsoluteBaseUrl + '/index.php?r=aicc/default/axGetLaunchInfo',
			type: 'post',
			dataType: 'json',
			data: {
				course_id: Arena.course_id, // We need course_id for ALL requests
				data_key: loData.key,
				id_org: loData.idOrganization,
				id_aicc_item: loData.idAiccItem,
				chapter: loData.aiccSystemId,
				launch_type: launchType
			}
		}).done(function (res) {

			if (res.success == true) {
				var data = res.data;
				var launchLink = res.data.launch_url;

				if (data && data.debug)
					launchLink += "&debug=1";

				//Launcher.commonLaunchpad.load(loData);
				if (Arena.playerLayout === 'listview') {
					// Course player layout is List View

					// Manually trigger hiding the left menu objects tree if opened
					if ($('#player-lonav').attr("data-opened") == "true") {
						Arena.hideLoNavigation();
					} else {
						Arena.showLoNavigation();
					}
				} else {
					// Play button view
					Arena.toggleLoNavigation();
				}

				switch (launchType) {

					case 'inline' :
						that.inline(launchLink, loData);
						break;

					case 'lightbox':
						that.lightbox(launchLink, loData);
						break;

					case 'redirect':
						window.location.href = launchLink;
						break;

					case 'fullscreen':
						window.location.href = launchLink;
						break;

					case 'popup':
					default:
						if (popup) {
							if (loData.width && loData.height) {
								try {
									popup.resizeTo(loData.width, loData.height);
								} catch (e) {
								}
							}
							$('#player-arena-launchpad').html('').hide();
							popup.location = launchLink;
						}
						break;
				} // end of launch
			}
			else {
				Docebo.Feedback.show('error', res.message);
			}
		});

	},
	// TINCAN Launcher
	tincan: function (loData) {

		// This should be LO configuration
		var launchType = loData.params.openmode ? loData.params.openmode : 'lightbox';
		var that = this; // i.e. Launcher object
        // IE is picky about the window name argument.
        // It doesn't like spaces, dashes, or other punctuation.
        var windowName = loData.title.replace(/\s/g, '_');
        var popup = false;
        var asynch = true;
        if(launchType == 'popup'){
            asynch = false;
            var loading_url = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=player/training/loading&course_id=' + Arena.course_id;
            try {
                popup = window.open(loading_url, windowName);
            } catch (e) {
                popup = window.open(loading_url, '_blank');
            }

            windowCheck(popup, loData);

        }

		// Get launching information and continue
		$.ajax({
            async:asynch,
			url: Docebo.lmsAbsoluteBaseUrl + '/index.php?r=tincan/default/axGetLaunchInfo',
			type: 'post',
			dataType: 'json',
			data: {
				course_id: Arena.course_id, // We need course_id for ALL requests
				id_object: loData.key
			},
			beforeSend: function () {
			}
		}).done(function (res) {
			if (res.success == true) {
				var keepAliveUrl = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=tincan/default/keepAlive&course_id=' + Arena.course_id;
				setTimeout("Launcher.keepAlive('"+keepAliveUrl+"')", 5*60*1000);

				var data = res.data;

				// Presense of a query string is determined server-side; see the controller/action
				var glue = data.hasQueryString ? "&" : "?";

				var launchLink = data.launch + glue + "endpoint=" + encodeURIComponent(data.endpoint)
						+ "&auth=" + encodeURIComponent(data.auth)
						+ "&actor=" + encodeURIComponent(data.actor)
						+ "&registration=" + encodeURIComponent(data.registration)
						+ "&activity_id=" + encodeURIComponent(data.activityId)
						+ "&Accept-Language=" + data.acceptLanguage
						+ "&content_token=" + data.content_token;
				
				if (typeof data.xapi_auth_code !== "undefined") {
					launchLink = launchLink + "&xapi_auth_code=" + data.xapi_auth_code;
				}

				// Add list of "extended parameters", if any
				if (typeof data.extended_params !== "undefined") {
					var eps = [];
					for(var propertyName in data.extended_params) {
						eps.push(propertyName + '=' + data.extended_params[propertyName]);
					}
					var epsUri = eps.join('&');
					if (epsUri.length > 0) {
						launchLink = launchLink + "&" + epsUri;
					}
				}

				if (Arena.playerLayout === 'listview') {
					// Course player layout is List View

					// Manually trigger hiding the left menu objects tree if opened
					if ($('#player-lonav').attr("data-opened") == "true") {
						Arena.hideLoNavigation();
					} else {
						Arena.showLoNavigation();
					}
				} else {
					// Play button view
					Arena.toggleLoNavigation();
				}

				switch (launchType) {

					case 'inline' :
						that.inline(launchLink, loData);
						break;

					case 'lightbox':
						that.lightbox(launchLink, loData);
						break;

					case 'redirect':
						window.location.href = launchLink;
						break;

					case 'popup':
					default:
                        if(popup){
                            popup.location = launchLink;

                        }else{
                            try {
                                newWindow = window.open(launchLink, windowName);
                            } catch (e) {
                                newWindow = window.open(launchLink, '_blank');
                            }
                            windowCheck(newWindow, loData);
                        }
						$('#player-arena-launchpad').html('').hide();
						break;
				} // end of launch
			}
			else {
				Docebo.Feedback.show('error', res.message);
			}
		});

	},
	// Elucidat Launcher - it is based on TinCan
	elucidat: function (loData) {
		// This should be LO configuration
		var launchType = loData.params.openmode ? loData.params.openmode : 'lightbox';

		// loData.params.openmode, .desktop_openmode, .tablet_openmode, .smartphone_openmode
		if (this.isTablet()) {

			launchType = loData.params.tablet_openmode ? loData.params.tablet_openmode : 'fullscreen';
		} else if (this.isSmathphone()) {

			launchType = loData.params.smartphone_openmode ? loData.params.smartphone_openmode : 'fullscreen';
		} else {

			launchType = loData.params.desktop_openmode ? loData.params.desktop_openmode : launchType;
		}

		var that = this; // i.e. Launcher object
		var async = true;

		if (launchType == 'popup') {
			async = false;

			// let's preopen the windows or the popup blocker will it

			// IE is picky about the window name argument.
			// It doesn't like spaces, dashes, or other punctuation.
			var windowName = loData.title.replace(/\s/g, '_'),
					popup = false,
					loading_url = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=player/training/loading&course_id=' + Arena.course_id;
			try {
				popup = window.open(loading_url, windowName);
			} catch (e) {
				popup = window.open(loading_url, '_blank');
			}

			windowCheck(popup, loData);
		}

		// Get launching information and continue
		$.ajax({
            async:async,
			url: Docebo.lmsAbsoluteBaseUrl + '/index.php?r=ElucidatApp/ElucidatApp/axGetLaunchInfo',
			type: 'post',
			dataType: 'json',
			data: {
				course_id: Arena.course_id, // We need course_id for ALL requests
				id_object: loData.key
			},
			beforeSend: function () {
			}
		}).done(function (res) {
			if (res.success == true) {
				var data = res.data;

				// Presense of a query string is determined server-side; see the controller/action
				var glue = data.hasQueryString ? "&" : "?";

				var launchLink = data.launch;

				if (Arena.playerLayout === 'listview') {
					// Course player layout is List View

					// Manually trigger hiding the left menu objects tree if opened
					if ($('#player-lonav').attr("data-opened") == "true") {
						Arena.hideLoNavigation();
					} else {
						Arena.showLoNavigation();
					}
				} else {
					// Play button view
					Arena.toggleLoNavigation();
				}
				switch (launchType) {

					case 'inline' :
						that.inline(launchLink, loData);
						break;

					case 'lightbox':
						that.lightbox(launchLink, loData);
						break;

					case 'redirect':
						window.location.href = launchLink;
						break;

					case 'fullscreen':
						window.location.href = launchLink;
						break;

					case 'popup':
					default:
						// IE is picky about the window name argument.
						// It doesn't like spaces, dashes, or other punctuation.
						var windowName = loData.title.replace(/\s/g, '_');
						try {
							newWindow = window.open(launchLink, windowName);
						} catch (e) {
							newWindow = window.open(launchLink, '_blank');
						}
						windowCheck(newWindow, loData);
						$('#player-arena-launchpad').html('').hide();
						break;
				} // end of launch
			}
			else {
				Docebo.Feedback.show('error', res.message);
			}
		});

	},
	authoring: function (loData) {

		//var launchLink = Docebo.rootAbsoluteBaseUrl+'/authoring/index.php?r=main/play&id=' + loData.idResource + '&id_course=' + Arena.course_id;
		var launchLink = Docebo.rootAbsoluteBaseUrl + '/authoring/index.php?r=main/play&id=' + loData.idResource + '&id_object=' + loData.idOrganization + '&id_course=' + Arena.course_id;

		var launchLinkStripped = launchLink + '&stripped=1';
		var launchType = loData.params.openmode ? loData.params.openmode : 'redirect';

		var keepAliveUrl = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=authoring/default/keepAlive&course_id=' + Arena.course_id;
		setTimeout("Launcher.keepAlive('"+keepAliveUrl+"')", 5*60*1000);

		// Force Authoring launchtype to 'redirect; (?)
		//launchType = 'redirect';

		if (Arena.playerLayout === 'listview') {
			// Course player layout is List View

			// Manually trigger hiding the left menu objects tree if opened
			if ($('#player-lonav').attr("data-opened") == "true") {
				Arena.hideLoNavigation();
			} else {
				Arena.showLoNavigation();
			}
		} else {
			// Play button view
			Arena.toggleLoNavigation();
		}

		switch (launchType) {

			case 'inline' :
				this.inline(launchLinkStripped, loData);
				break;

			case 'lightbox':
				this.lightbox(launchLinkStripped, loData);
				break;

			case 'redirect':
				window.location.href = launchLink;
				break;

			case 'popup':
			default:
				// IE is picky about the window name argument.
				// It doesn't like spaces, dashes, or other punctuation.
				var windowName = loData.title.replace(/\s/g, '_');
				var newWindow = new Object();
				newWindow.closed = false;
				try {
					newWindow = window.open(launchLink, windowName);
				} catch (e) {
					newWindow = window.open(launchLink, '_blank');
				}
				$('#player-arena-launchpad').html('').hide();

					windowCheck(newWindow, loData);

				break;

		} // end of launch

	},
	lightbox: function (url, loData) {
		$.fancybox.open([{
				type: 'iframe',
				href: url
			}], {
			padding: 0,
			margin: (/iPad/.test(navigator.userAgent) ? [30, 0, 0, 0] : [33, 10, 5, 10]),
			tpl: {
				closeBtn: '<a title="' + Yii.t('standard', '_CLOSE') + '" class="fancybox-docebo-close" href="javascript:;">'
						+ Yii.t('standard', '_CLOSE') + '</a>'
			},
			autoSize: false,
			width: loData.width && loData.width > 100 ? loData.width + 'px' : '100%',
			height: loData.height && loData.height > 100 ? loData.height + 'px' : '100%',
			openEffect: 'elastic',
			closeEffect: 'fade',
			helpers: {
				overlay: {closeClick: false},
				doceboTitle: {
					title: (loData.type == 'sco' && loData.scormTitle !== 'undefined') ? loData.scormTitle : loData.title,
					headerWidget: (typeof loData.headerWidget !== "undefined") ? loData.headerWidget : '',
					loKey: loData.key
				}
			},
			beforeLoad: function () {
				if (loData.type == 'sco')
					loadCheck();
			},
			afterLoad: function () {
				Arena.learningObjects.ajaxUpdate();
				$('#player-arena-launchpad').html('').hide();
                $(window).resize(function() {
                    $.fancybox.update();
                });
			},
			beforeClose: function () {
				if(Arena.playerLayout==='listview'){
					$('.player-navigator').hide();
				}
				$(this.inner).find("iframe").attr('src', '#');
				$.fancybox.showLoading();
			},
			afterClose: function () {
				$.fancybox.hideLoading();
				Launcher.showCommonLaunchpadFirstToPlay();
				Arena.showQuickNav();
			}
		});

	},
	inline: function (url, loData) {
		var headerWidget;
		var title = (loData.type == 'sco' && loData.scormTitle !== 'undefined') ? loData.scormTitle : loData.title;
		if (typeof loData.headerWidget != "undefined") {
			headerWidget = loData.headerWidget;
		} else {
			headerWidget = '';
		}
		$('#player-arena-launchpad').show();
		$('#player-arena-launchpad').html(
				'<div class="row-fluid player-launchpad-header">' +
				'<h2>' + title + '</h2>' +
				headerWidget +
				'<a id="inline-player-close" class="player-close" href="#">' + Yii.t('standard', '_CLOSE') + '</a>' +
				'</div>' +
				'<div class="player-launchpad"></div>'
				);
		if (headerWidget != '') {
			if ($('.courseLoHeaderApp7020 .triggerOpenSidebar').length > 0) {
				$('.courseLoHeaderApp7020 .triggerOpenSidebar').attr("id-lo", loData.key);
			}
		}
		var iframe = $('<iframe />', {
			id: 'inline-' + loData.idOrganization,
			width: '100%',
            allowfullscreen: 'allowfullscreen',
			height: (loData.height ? loData.height + 'px' : '680px'),
			'class': 'player-arena-inline ' + loData.type
		});
		iframe.appendTo('#player-arena-launchpad div.player-launchpad');

		if (Arena.playerLayout === 'listview') {
			$('#player-quicknavigation').hide();
		}

		if (Arena.playerLayout === 'navigator' || Arena.playerLayout==='listview') {
			iframe.on('load', function(){
				if(loData.type != 'sco' || (loData.params && loData.params.desktop_openmode && loData.params.desktop_openmode == 'inline')) {
					PlayerNavigator.init({ajaxUrl: Docebo.lmsAbsoluteBaseUrl + '/index.php?r=player/training/axGetPlayerNavigator&course_id=' + Arena.course_id});
					PlayerNavigator.setCurrentKey(loData.key);
					PlayerNavigator.setState('playing');
					PlayerNavigator.reload();
					Arena.learningObjects.ajaxUpdate(null, null, false);
				}
			});
		}
		iframe.attr('src', url);

	},
	/**
	 * Only applies to courses with Player Layout of "List View"
	 */
	resetToListView: function () {
		$('#player-arena-launchpad').html('').hide();
		Arena.learningObjects.ajaxUpdate(null, Arena.ajaxUpdateSuccessCallback, false);
	},
	keepAlive: function (keepAliveUrl) {
		$.ajax({
			url: keepAliveUrl,
			dataType: 'json'
		});
		setTimeout("Launcher.keepAlive('"+keepAliveUrl+"')", 5*60*1000);
	},
	/**
	 * Show the common lauchpad with 'first to play' inside
	 */
	showCommonLaunchpadFirstToPlay: function () {
		Docebo.scrollTo('#arena-board-top');

		// Initially reset the player to the blue Play icon while
		// the updated LOs list loads with AJAX
		Launcher.commonLaunchpad.load(Arena.firstLoToPlay, true);
		// Make the AJAX call to reload the LOs list at the bottom of the player.
		// Needed so the user's status for those LOs is updated visually, since this
		// function is called in some occasions after a LO is played and closed (e.g. a SCO
		// is played Inline and closed via "Close" at top right without finishing)
		Arena.learningObjects.ajaxUpdate('', function (newTreeData) {
			Launcher.commonLaunchpad.load(Arena.firstLoToPlay, true);
			if (Arena.playerLayout === 'navigator' || Arena.playerLayout==='listview') {
				if (Arena.firstLoToPlay) {
					PlayerNavigator.setState('initial');
					PlayerNavigator.setCurrentKey(Arena.firstLoToPlay.key);
					PlayerNavigator.reload();
				}
			}
		});
	}


};


/**
 * Show Feedback div at the top of the caller (prepend);
 * @TODO  Styling please... should "stick" just before the header or something...
 */
(function ($) {
	$.fn.feedback = function (type, message, showdismiss) {
		var $this = $(this);
		var html = '', typeClass = '', extraHtml = '';
		var $feedbackDiv = $('<div class="element-feedback" id="' + $this.attr('id') + '-feedback"></div>');

		// Remove previos feedbacks, if any
		$this.find('#' + $this.attr('id') + '-feedback').remove();

		switch (type) {
			case 'success':
				typeClass = 'alert-success';
				extraHtml = '<span class="i-sprite is-check green"></span> ';
				break;
			case 'error':
				typeClass = 'alert-error';
				extraHtml = '<span class="i-sprite is-remove red"></span> ';
				break;
			default:
				break;
		}

		style = 'opacity: 0.5; filter: alpha(opacity = 50);';
		html += '<div class="alert ' + typeClass + ' in alert-block fade">' +
				(showdismiss ? '<button type="button" class="close" data-dismiss="alert">&times;</button>' : '') +
				extraHtml +
				message +
				'</div>';

		$feedbackDiv.remove();
		$feedbackDiv.html(html);
		$this.prepend($feedbackDiv);

	};
}(jQuery));


function getLoLaunchType(loData) {
	var launchType = false;
	switch (loData.type) {
		case 'authoring':
			launchType = loData.params.openmode ? loData.params.openmode : 'redirect'
			break;
		case 'htmlpage':
			launchType = 'inline';
			break;
		case 'tincan':
			launchType = loData.params.openmode ? loData.params.openmode : 'lightbox';
			break;
		case 'aicc':
		case 'sco':
		case 'scormorg':
			// This should be LO configuration
			launchType = loData.params.openmode ? loData.params.openmode : 'lightbox';

			// loData.params.openmode, .desktop_openmode, .tablet_openmode, .smartphone_openmode
			if (isTablet()) {
				launchType = loData.params.tablet_openmode ? loData.params.tablet_openmode : 'fullscreen';
			} else if (isSmartphone()) {
				launchType = loData.params.smartphone_openmode ? loData.params.smartphone_openmode : 'fullscreen';
			} else {
				launchType = loData.params.desktop_openmode ? loData.params.desktop_openmode : launchType;
			}
			break;
		default:
			launchType = loData.params.openmode ? loData.params.openmode : 'inline';
			break;
	}
	return launchType;
}

function windowCheck(newWindow, loData)
{
	if (newWindow && newWindow.closed) {
		if (getLoLaunchType(loData) == 'popup') {
			var newHref = location.href;
			var index = newHref.indexOf('last_popup_played', 0);
			if (index > 0) {
				newHref.replace(/last_popup_played=[\d]+/, 'last_popup_played=' + loData.idOrganization);
			} else {
				newHref += '&last_popup_played=' + loData.idOrganization; //we are telling the
			}
			if(location.href == newHref)
				location.reload(true);
			else
				location.href = newHref;
		} else {
			location.reload(true);
		}
	} else {
		setTimeout(function () {
			windowCheck(newWindow, loData);
		}, 500);
	}
}
//sometimes on Chrome the "load" event is not triggered although the scorm is loaded.
function loadCheck() {
	if ($("[id*=fancybox-frame]").length) {
		//if the iframe is there, but the load event is not triggered, trigger it manually
		if ($('#fancybox-loading').length)
			$("[id*=fancybox-frame]").trigger("load");
	} else {
		setTimeout(function () {
			loadCheck();
		}, 5000);
	}
}