(function ($) {

	var ScormSelect = function (options) {
		this.init('scormselect', options, ScormSelect.defaults);
	};

	$.fn.editableutils.inherit(ScormSelect, $.fn.editabletypes.list);

	$.extend(ScormSelect.prototype, {
		renderList: function () {
			if (!$.isArray(this.sourceData)) {
				return;
			}

			var selectInput = this.$input.filter('select');
			for (var i = 0; i < this.sourceData.length; i++) {
				selectInput.append($('<option>', {value: this.sourceData[i].value}).text(this.sourceData[i].text));
			}

			//enter submit
			this.$input.on('keydown.editable', function (e) {
				if (e.which === 13) {
					$(this).closest('form').submit();
				}
			});
		},

		value2htmlFinal: function (value, element) {
			var text = '', item = this.itemByVal(value);
			if (item) {
				text = item.text;
			}
			ScormSelect.superclass.constructor.superclass.value2html(text, element);
		},

		input2value: function () {
			return {
				status: this.$input.filter('select').val(),
				inherit: (this.$input.parent().find('input[name="inherit"]').prop('checked') ? '1' : '0')
			};
		},

		autosubmit: function () {
			this.$input.off('keydown.editable').on('change.editable', function () {
				$(this).closest('form').submit();
			});
		}
	});


	ScormSelect.defaults = $.extend({}, $.fn.editabletypes.list.defaults, {
		tpl: '<select></select>'
			+ '<div><label class="scorm-status-assign-label">'
			+ '<input type="checkbox" name="inherit" value="1"><span>' + (Yii ? Yii.t('standard', '_INHERIT') : '') + '</span>'
			+ '</label></div>'
	});

	$.fn.editabletypes.scormselect = ScormSelect;

}(window.jQuery));