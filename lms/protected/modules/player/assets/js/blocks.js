/**
 * Blocks Object
 */

/**
 * Class Used to handle Comments block instances operations.
 * More than one Comments blocks can exist! Every Comments block create a
 * new instance of this class, identified by class->id attribute.
 *
 */
var CommentsBlock = function (id, discussionsOffset, discussionsLimit) {
	this.id = id;
	this.discussionsOffset = discussionsOffset;
	this.discussionsLimit = discussionsLimit;
	this.course_id = Blocks.course_id;
	this.block_selector = "#blockid_"+this.id;

	var me = this;


	/**
	 * Toggle Show/Hide Comments element
	 */
	$(me.block_selector).on("click", "a.do-comment", function (e) {
		$(this).next().slideToggle(function(){Blocks.equalizeNeighborsHeights();}); // next is the comments-container
	});


	/**
	 * LOAD MORE.. [discussions]
	 *
	 */
	$(me.block_selector).on("click", '#block-load-more-discussions-block-id-'+me.id, function (e) {
		e.preventDefault();

		// New Discussions offset and Limit
		var doffset = parseInt(me.discussionsOffset)+parseInt(me.discussionsLimit);
		var dlimit = parseInt(me.discussionsLimit);

		// Request loading of next (offset,limit) discussions OR a particular Doscussion
		var data = {
			course_id: Blocks.course_id,
			block_id: me.id,
			doffset: doffset,
			dlimit: dlimit,
			show_viewall: 1,
			show_all_comments: 0,
			show_create_discussion: 0,
			show_load_more: 0,
			init_blocks_js: 0
		};

		var url = Docebo.lmsAbsoluteBaseUrl+'/index.php?r=player/block/axGetForumDiscussionsHtml&course_id='+me.course_id;

		$.ajax({
			url: url,
			type: 'post',
			dataType: 'json',
			data: data,
			beforeSend: function () {}
		}).done(function (res) {
			if (res.success == true) {
				var countTotalDiscussions = res.data.countTotalDiscussions;

				// Save new offset/limit status as current
				me.discussionsOffset = doffset;
				me.discussionsLimit = dlimit;

				if ((doffset+dlimit) >= (countTotalDiscussions)) {
					$("#blockid_"+me.id+" .load-more").hide();
				}

				// Inject newly loaded html into dedicated element
				$("#block-more-discussions-loaded-block-id-"+me.id).append(res.data.html);

				me.attachExpander();

			} else {}
		});

		return false;

	});

	/**
	 * VIEW ALL COMMENTS
	 *
	 */
	$(me.block_selector).on("click", '[id^="block-view-all-comments-link-block-id-'+me.id+'"]', function (e) {

		e.preventDefault();

		// Discussion Id
		var threadId = parseInt($(this).data('discussion-id'));

		// Request a particular discussion HTML
		// POST data
		var data = {
			course_id: Blocks.course_id,
			block_id: me.id,
			thread_id: threadId,
			show_all_comments: 1,
			show_viewall: 0,
			show_create_discussion: 0,
			show_load_more: 0,
			init_blocks_js: 0
		};

		var url = Docebo.lmsAbsoluteBaseUrl+'/index.php?r=player/block/axGetForumDiscussionsHtml&course_id='+me.course_id;

		$.ajax({
			url: url,
			type: 'post',
			dataType: 'json',
			data: data,
			beforeSend: function () {
			}
		}).done(function (res) {
			if (res.success == true) {

				// Open a Fancybox, using this content
				var content = "<div class='player-block-content-inner'>"+res.data.html+"</div>";
				$.fancybox.open([
					{
						content: content
					}
				], {
					padding: 0,
					margin: 30,
					maxWidth: 800,
					minWidth: 800
					//afterShow: function(){me.attachExpander()}
				});

			} else {}
		});

		return false;

	});



	/**
	 * ADD COMMENT
	 */
	$(document).off("submit", '[id^="block-comments-add-comment-'+me.id+'"]')
		.on("submit", '[id^="block-comments-add-comment-'+me.id+'"]', function () {
			var $form = $(this);

			if ($($form).find("textarea").val() == '') {
				return false;
			}

			var options = {
				url: Docebo.lmsAbsoluteBaseUrl+'/index.php?r=forum/default/axAddCommentFromBlock',
				type: 'post',
				data: {
				},
				dataType: 'json',
				success: function (res) {
					if (res.success == true) {
						// Append new comment at the end
						var selector = "#block-comments-comments-"+me.id+"-"+res.data.thread_id+" .block-comments-comments-container-inner";
						$(selector).append(res.data.html);
						// Clear comment textarea
						$($form).find('[name="comment_text"]').val('');
						$('.discussion-id-' + res.data.thread_id).find('.comments-count .count-number').text(res.data.commentsCount);
						if (res.data.commentsCount <= res.data.commentsLimit) {
							$('.discussion-id-' + res.data.thread_id).find('[id^="block-view-all-comments-link-block-id-"]').hide();
						}
					} else {
						Docebo.Feedback.show('error', res.message);
					}

				}
			};

			$(this).ajaxSubmit(options);
			return false;

		});


	/**
	 * REMOVE COMMENT
	 */
	$(document).on("click", ".remove-forum-message", function () {
		var messageId = $(this).data("message-id");
		var url = Docebo.lmsAbsoluteBaseUrl+'/index.php?r=forum/default/axDeleteMessageFromBlock';
		$.ajax({
			url: url,
			type: 'post',
			dataType: 'json',
			data: {
				course_id: me.course_id,
				message_id: messageId
			},
			beforeSend: function () {
			}
		}).done(function (res) {
			if (res.success == true) {
				var $target = $(".block-comments-comment-id-"+parseInt(messageId));
				$target.fadeOut('150', function () {
					$target.remove();
				});
				$('.discussion-id-' + res.data.thread_id).find('.comments-count .count-number').text(res.data.commentsCount);
				if (res.data.commentsCount <= res.data.commentsLimit) {
					$('.discussion-id-' + res.data.thread_id).find('[id^="block-view-all-comments-link-block-id-"]').hide();
				}
			}
			else {
				Docebo.Feedback.show('error', res.message);
			}
		});


		return false;
	});


	$(document).on("click", ".remove-comment-thread", function () {
		var threadId = $(this).data("thread-id");
		var url = Docebo.lmsAbsoluteBaseUrl+'/index.php?r=forum/default/axDeleteComment';
		$.ajax({
			url: url,
			type: 'post',
			dataType: 'json',
			data: {
				course_id: me.course_id,
				thread_id: threadId
			},
			beforeSend: function () {
			}
		}).done(function (res) {
			if (res.success == true) {
				var $target = $(".discussion-id-"+parseInt(threadId));
				$target.fadeOut('150', function () {
					$target.remove();
				});
			}
			else {
				Docebo.Feedback.show('error', res.message);
			}
		});


		return false;
	});

	/**
	 * CREATE DISCUSSION
	 */
	$(me.block_selector).on("submit", '[id^="block-comments-add-discussion-'+me.id+'"]', function () {
		var $form = $(this);

		// If input is empty, get out
		if ($form.find('[name="opening_message_text"]').val() == '') {
			return false;
		}

		var options = {
			//url: Docebo.lmsAbsoluteBaseUrl+'/index.php?r=forum/default/axAddDiscussionFromBlock',
			type: 'post',
			data: {
			},
			dataType: 'json',
			success: function (res) {
				if (res.success == true) {
					$("#block-comments-discussions-"+me.id).prepend(res.data.html);
					$form.find('[name="opening_message_text"]').val('');
				} else {
					Docebo.Feedback.show('error', res.message);
				}
			}
		};

		$(this).ajaxSubmit(options);
		return false;

	});




	// Called once, upon initialization/creation of the object
	me.attachExpander();


};

/**
 *
 */
CommentsBlock.prototype = {

	attachExpander: function () {
		/**
		 * Cut the string and put "read more"/"read less" for all ".expandable"s
		 */
		$(".player-comments-block-discussion .message-expandable").expander({
			slicePoint: 160,
			expandEffect: 'show',
			expandSpeed: 0,
			collapseEffect: 'hide',
			collapseSpeed: 0,
			expandText: "<span style='white-space: nowrap;'>read more</span>",
			userCollapseText: "<span style='white-space: nowrap;'><i class='icon-chevron-up'></i></span>",
			afterExpand: function () {
				// Make it inline... because it is inline-block and makes it like it has <br> (new line)... weird...
				$(this).find(".details").css("display", "inline");
				Blocks.equalizeNeighborsHeights();
			}
		});

	}


};

/**
 *
 */
var Blocks = {

	// Used at block grid level
	ajaxLoader: '<div class="ajaxloader" style="display: none;"></div>',
	// Per-block ajax loading GIF
	blockAjaxLoader: '<div class="blockAjaxloader" id="ajax-loader-%ID%"></div>',

	// Initialize Blcoks object
	init: function (options) {
		this.course_id = options.course_id;
		this.id_session = (options.id_session || null); //this will be filled only if classroom app is active
		this.blocksGridUrl = options.blocksGridUrl;
		this.updateBlocksOrderUrl = options.updateBlocksOrderUrl;
		this.updateCoursedocsOrderUrl = options.updateCoursedocsOrderUrl;
		this.loadOneBlockUrl = options.loadOneBlockUrl;
		this.loadOneBlockCompletelyUrl = options.loadOneBlockCompletelyUrl;
	},


	// Ajax call to load the whole blocks grid
	loadGrid: function (option) {
		$.ajax({
			context: this,  // IMPORTANT!
			url: this.blocksGridUrl,
			type: 'post',
			dataType: 'json',
			data: {course_id: this.course_id, option: option},
			beforeSend: function () {
				$('#player-blocks-grid').html(Blocks.ajaxLoader);
				$('.ajaxloader').show();
			}
		}).done(function (res) {
					if (res.success == true) {
						$('#player-blocks-grid').html(res.html);
						this.loadAllBlocksContent();
						this.sortize();
						$('#player-blocks-grid').controls();  // jQuery-controls plugin
					}
					else {
						$('#player-blocks-grid').html(res.message);
					}
				})
				.always(function () {
					$('.ajaxloader').hide();
				});
	},

	// Make all player blocks elements Sortable/Draggable
	sortize: function () {

		// Add some classes to all blocks
		$(".player-block").
		addClass("ui-widget ui-widget-content ui-helper-clearfix").
		find(".player-block-header").
		addClass("ui-widget-header");

		// Make the UL sortable/draggable
		// Bind UPDATE block order to DROP event
		$('#blocks-sortable').sortable({
			items: ".player-block",
			handle: ".player-block-header .drag-handle",
			opacity: 0.5,
			cursorAt: {
				right:50,
				top: 20,
			},
			placeholder: 'player-block player-block-placeholder span6',
			stop: function () {
				var sorted = $(this).sortable("toArray");
				var data = {
					course_id: Blocks.course_id,
					sorted: sorted
				};

				$.ajax({
					url: Blocks.updateBlocksOrderUrl,
					type: 'post',
					dataType: 'json',
					data: data
				});
			}
		});

	},


	// Load ONE block content
	// Usage: $('.some-player-block').loadOneBlockContent(opions)
	loadOneBlockContent: function (options) {

		$.ajax({
					url: Blocks.loadOneBlockUrl,
					type: 'post',
					dataType: 'json',
					data: {
						options: options,
						course_id: Blocks.course_id,
						block_id: $(this).data('block-id')
					},
					context: $(this),  /// VEERY important
					beforeSend: function () {
						// Insert ajax-loader markup and show
						var blockId = $(this).data('block-id');
						$(this).find('.player-block-content-inner').html(Blocks.blockAjaxLoader.replace('%ID%', $(this).data('block-id')));
						$('#ajax-loader-' + blockId).show();
					}
				})
				.done(function (res) {
					if (res.success == true) {
						$(this).find('.player-block-content-inner').html(res.html);
						$(this).controls();
						$(this).trigger('block-refreshed', options);
					} else {
					}
					setTimeout(function(){Blocks.equalizeNeighborsHeights();}, 2000);
				});

	},

	// Load ONE block content
	// Usage: $('.some-player-block').loadOneBlockContent(opions)
	loadOneBlockContentCompletely: function (options) {

		$.ajax({
					url: Blocks.loadOneBlockCompletelyUrl,
					type: 'post',
					dataType: 'json',
					data: {
						options: options,
						course_id: Blocks.course_id,
						block_id: $(this).data('block-id')
					},
					context: $(this),  /// VEERY important
					beforeSend: function () {
						// Insert ajax-loader markup and show
						var blockId = $(this).data('block-id');
						$(this).find('.player-block-content-inner').html(Blocks.blockAjaxLoader.replace('%ID%', $(this).data('block-id')));
						$('#ajax-loader-' + blockId).show();
					}
				})
				.done(function (res) {
					if (res.success == true) {
						var block_id = $(this).data('block-id');
						$('#blockid_' + block_id).replaceWith(res.html);
						$('#blockid_' + block_id).off('coursedoc-upload-complete').loadOneBlockContent(options);
					} else {
					}
				});

	},


	// Enumerate block elements inside the Blocks Grid and load its content
	loadAllBlocksContent: function () {

		//Docebo.log('Loading all blocks content');

		$('#player-blocks-grid').ajaxStop(function () {
			Blocks.equalizeNeighborsHeights();
		});

		$('#player-blocks-grid').find('.player-block').each(function () {
			$(this).loadOneBlockContent();
		});

	},


	/**
	 * Enumerate all blocks (content already loaded!!!!!!!!), checks those having equal TOP position (implies they are in a row)
	 * and makes their heights equal to the max height among all of them (i.e. in the same row).
	 *
	 *  Called on ajaxStop() and on-demand by various event handlers here changing the content of blocks.
	 *
	 *  @todo Avoid using ajaxStop(), instead find a way to use Ajax Queues (plamen)
	 *
	 */
	equalizeNeighborsHeights: function () {

		// First, make all blocks 'auto' so they can accomodate their content
		$('#player-blocks-grid').find('.player-block').each(function () {
			$(this).css({height: 'auto'});
		});

		// Now walkthrough aganin and equalize
		$('#player-blocks-grid').find('.player-block').each(function () {
			var $block = $(this);
			var maxHeight = 0;

			$(this).siblings().each(function () {
				if ($block.position().top == $(this).position().top) {
					maxHeight = Math.max($block.height(), $(this).height());
					$block.height(maxHeight);
					$(this).height(maxHeight);
				}
			});

		});
	},


	/**
	 * Remove a forum. Forum ID is in the HREF of the element
	 * @param e Event
	 * @returns {Boolean}
	 */
	removeForum: function (e) {
		var elem = $(e.currentTarget);
		var url = elem.attr('href');
		var blockId = elem.data('block-id');

		// Show Prompt
		var dialogMessage = Yii.t('forum', 'Delete this forum and all its content (discussions and messages)? <br><br>' +
				'<span class="i-sprite is-solid-exclam red"></span> <strong>Warning! This operation can not be undone!</strong>');

		// Show prompt dialog and handle answer
		bootbox.dialog(dialogMessage,
				[
					{
						'label': Yii.t('standard', '_DEL'),
						'class': 'btn-docebo big red',
						'callback': function () {

							$.post(url, {}, function (res) {
								if (res.success == true) {
									$('#blockid_' + blockId).loadOneBlockContent();
									if (res.data && res.data != 0)
										$('#blockid_' + res.data).remove();
								}
								else {
									Docebo.Feedback.show('error', res.message);
								}
							}, 'json');
						}
					},
					{
						'label': Yii.t('standard', '_CANCEL'),
						'class': 'btn-docebo big black',
						'callback': function () {

						}
					}

				],
				{
					header: Yii.t('forum', 'Delete forum')
				}
		);

		return false;
	}
};

var JoinButtonTimer = function(options){

	this.target_date = false;
	this.joinButtonOffsetSeconds = false;
	this.startTimeStamp = false;
	this.endTimeStart = false;
	this.now = false;
	this.buttonElement = false;
	this.endButtonElement = false;
	this.task = false;

	if(typeof options === "undefined"){
		var options = {};
	} else{
		this.target_date = options.target_date;
		this.joinButtonOffsetSeconds = options.joinButtonOffsetSeconds;
		this.startTimeStamp= options.startTimeStamp;
		this.endTimeStart= options.endTimeStart;
		this.now = options.now || false;
		this.buttonElement = options.buttonElement;
		this.endButtonElement = options.endButtonElement;
		this.task = options.task || 'hide';
	}

	var joinButtonOffsetSeconds = this.joinButtonOffsetSeconds;
	var time_to_begin = this.startTimeStamp - joinButtonOffsetSeconds - (this.now);
	var time_to_end = parseInt(this.endTimeStart) - parseInt(this.now);
	var that = this;


	if(this.task == 'show') {
		time_to_begin = 0;
	}

	var timerId = setInterval(function () {
		time_to_end -= 1;
		time_to_begin -= 1;

		if (time_to_begin > 0) {
			$("." + that.buttonElement).hide();
		}
		else
			$("." + that.buttonElement).show();


		if (time_to_begin <= 0 && time_to_end <= 0 ){
			$("." + that.buttonElement).hide();
			clearInterval(timerId);
		}
	}, 1000);
};

/** DOM READY **/
$(function () {

	// extend jQuery a bit so we can chain
	$.fn.loadOneBlockContent = Blocks.loadOneBlockContent;
	$.fn.loadOneBlockContentCompletely = Blocks.loadOneBlockContentCompletely;

	// Load Blocks Grid on page load & ready
	Blocks.loadGrid();


	// FORUM BLOCK: Delete Forum icon in Forum Block
	$(document).on("click", '.remove-forum-operation', $.proxy(Blocks.removeForum, this));


	// COURSEDOCS Block: Very simple Form validation; we don't want to upload 200 MB , submit the form and find out that we forgot to enter title.
	$('#upload-x-form').on('submit', function(e){
		// Empty title ?
		if ($('#LearningCourseFile_title').val() == '') {
			$('[id^="update-coursedoc-modal-"]').feedback('error', 'Enter title', true);
			return false;
		}
	});



	/** Dialog 2 Close listeners **/


	// BLOCK management dialogs
	$(document).on('dialog2.content-update', '[id^="remove-block-dialog-"], #add-block-dialog, [id^="update-block-dialog-"]', function () {
		if ($(this).find("a.auto-close.success").length > 0) {

			// If we just update a block, re-load only that block, ptherwise, re-load all
			// Note: changing size requires manual reload
			if ($(this).attr('id').indexOf('update-block-dialog') >= 0) {
				Blocks.loadGrid();
				//var blockId = $(this).attr('id').split('update-block-dialog-').pop();
				//$('#blockid_' + blockId).loadOneBlockContent();
			}
			else {
				Blocks.loadGrid();
			}
		}
	});


	// FORUM Block operations
	$(document).on('dialog2.content-update', '[id^="create-forum-modal-"], [id^="edit-forum-modal-"]', function (e) {
		var blockId = $(this).attr('id').split('forum-modal-').pop();
		TinyMce.attach('#forum-description', 'simple', {height: '150px'});
		if ($(this).find("a.auto-close.success").length > 0) {
			$('#blockid_' + blockId).loadOneBlockContent();
		}
	});


	// On closing the dialog we must destroy TinyMce editors;
	$(document).on('dialog2.closed', '[id^="create-forum-modal-"], [id^="edit-forum-modal-"]', function (e) {
		//TinyMce.destroy('textarea');
		TinyMce.removeEditorById('forum-description');
	});



	// COURSEDOCS Block operations
	$(document).on('dialog2.content-update', '[id^="update-coursedoc-modal-"]', function () {
		var blockId = $(this).attr('id').split('update-coursedoc-modal-').pop();
		if ($(this).find("a.auto-close.success").length > 0) {
			$('#blockid_' + blockId).loadOneBlockContentCompletely();
		}
	});





});