/**
 * Reusable File Uploader class. Uploading ONLY 1 file!
 *
 * Must be used in combination with Form, having specific HTML structure,
 * and form fields havind specific classes/ids/ etc.
 *
 * NOTE!: This class does NOT handle form submission, but only uploading!
 * Uploaded file information can be accessed like this:
 *
 * file = FileUploaderClass.pl_uploader.files[0];
 * Returning array (see PLUpload website):
 *
 * file[id] : p180bh2i4ab3d3hhoqtj9o1ah5
 * file[name] : filename.ext
 * file[size] : 2113359
 * file[loaded] : 2113359
 * file[percent] : 100
 * file[status] : 5
 *
 * This info should be used in on-submit/beforSend events/callbacks to add "file" to POST data.
 *
 * See /player/views/block/coursedocs/_update_coursedoc.php

  Basically:

  HTML:
  -----
  <form>
  	<div id='container'>
  		<div id='dropzone'>
  			... button
  			... progress bars
  			... fields
  		</div>
  	</div>

  	<input class='upload-save' type='submit'>
  	<input class='upload-cancel'>
  </form>


  Javascript:
  -----------
  CoursedocUploader = new FileUploaderClass();
	CoursedocUploader.init({
		chunk_size: '2mb',
		multi_selection: false,
		runtimes: 'html5,flash',
		browse_button: 'pickfiles',
		container: 'container',
		drop_element: 'dropzone',
		max_file_size: '4mb',
		url: /url/to/pl-uploader-upload/code,
		multipart: true,
		multipart_params: {
			YII_CSRF_TOKEN: 'token'
		},
		flash_swf_url: '/plupload/plupload.flash.swf',
		filters: []
	});
	CoursedocUploader.run();


  	//And here follows JS Code to handle form submission
  	......


 *
 */
var FileUploaderClass = function(options) {

	this.pl_uploader = null;

	// Default PL Uploader settings
	this.settings = {
		chunk_size: '1mb',
		multi_selection: false,
		runtimes: 'html5,flash',
		browse_button: 'pickfiles',
		container: 'uploader-container',
		drop_element: 'uploader-dropzone',
		max_file_size: '4mb',
		url: '',
		multipart: true,
		multipart_params: {},
		flash_swf_url: '',
		filters: [],
		onBeforeUpload:   function() {},
		onUploadComplete: function() {},
		onUploadCancel:   function() {},
		onFilesAdded:     function(up, files) {
            var $container = $('#' + up.settings.container);
            up.files = [files[0]];
            $container.find("#uploaded-filename").html(files[0].name);
            $container.find("#uploaded-filesize").html( plupload.formatSize(files[0].size) );
            up.start();
        }
	};
	this.init(options);

};

FileUploaderClass.prototype = {

	init: function(options) {
		//Docebo.log('Initialize File Uploader');
		// Apply incoming PL Uploader settings
		this.settings = $.extend({}, this.settings, options);
		this.pl_uploader = new plupload.Uploader(this.settings);
		this.onBeforeUpload = this.settings.onBeforeUpload;
		this.onUploadComplete = this.settings.onUploadComplete;
		this.onUploadCancel = this.settings.onUploadCancel;
		this.onFilesAdded = this.settings.onFilesAdded;
	},

	run: function() {

		//Docebo.log('Run File Uploader');
		//Docebo.log('File Uploader Settings object: ', this.settings);

		var that = this; // FileUploader
		var pl_uploader = this.pl_uploader;
		var $container = $('#' + pl_uploader.settings.container);

		if (!pl_uploader) {
			return false;
		}

		// Init event hander
		pl_uploader.bind('Init', function(up, params){
		});

		// Run uploader
		pl_uploader.init();

		// Just before uploading file
		pl_uploader.bind('BeforeUpload', function(up, file) {
			$container.find('.upload-button').hide();
			$container.find('.upload-progress').show();
			$container.find('.uploaded-percent').show();
			$('.upload-save').addClass("disabled");
			that.onBeforeUpload();
		});

		// Handle 'files added' event
        pl_uploader.bind('FilesAdded', this.onFilesAdded);

		// Upload progress
		pl_uploader.bind('UploadProgress', function(up, file) {
			$container.find('#uploader-progress-bar').css('width', file.percent + '%');
			$container.find('#uploaded-percent-gauge').html('&nbsp;' + file.percent + '%');
		});

		// On error
		pl_uploader.bind('Error', function(up, error) {
			$('.upload-save').removeClass("disabled");
			$('[id^="update-coursedoc-modal-"]').feedback('error',Yii.t('standard', '_OPERATION_FAILURE'), true);
			//Docebo.Feedback.show('error', 'Failed to upload a file: ' + error.message + ' (' + error.status + ')');
		});

		// On Upload complete
		pl_uploader.bind('UploadComplete', function(up, files){
			$container.find('.docebo-progress')
				.addClass("progress-success")
				.removeClass("progress-striped");
			$container.find('#uploaded-cancel-upload').hide();
			that.onUploadComplete(files, pl_uploader.settings.form_selector);

			that.addFileFormInfo(files, pl_uploader.settings.form_selector);

			$('.upload-save').removeClass("disabled");
		});

		// cancel upload cancel
		$container.find('#uploaded-cancel-upload').on('click', function(e){
			e.preventDefault();
			$('.upload-save').removeClass("disabled");
			if (that.pl_uploader) {
				that.pl_uploader.stop();
				that.pl_uploader.splice();
			}
			$container.find('.upload-button').show();
			$container.find('.upload-progress').hide();
			that.onUploadCancel();
			return false;
		});

		// ON CANCEL Upload Button
		$('.upload-cancel').on('click', function(e) {
			e.preventDefault();
			if (that.pl_uploader) {
				that.pl_uploader.stop();
				that.pl_uploader.destroy();
				that.onUploadCancel();
			}
			return false;
		});

	},


	/**
	 * Add 'file[]' hidden fields to uploading form
	 *
	 * @param files Uploaded file(s) array
	 * @param formSelector Where to add those hidden fields
	 * @returns {Boolean}
	 */
	addFileFormInfo: function(files, formSelector) {
		var html = '';
		if (files.length <= 0)
			return;

		for (var key in files[0])
			html += '<input type="hidden" name="file['+key+']" value="'+files[0][key]+'">\n';

		// Remove previiosly added file[]
		$(formSelector).find('input[name^="file["]').remove();

		$(formSelector).prepend(html);

		return true;
	}

};

