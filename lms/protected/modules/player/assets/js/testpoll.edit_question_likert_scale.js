var AnswersManager = {

    inputCounter: 1,
    oldQuestionRowHtml: null,
    isQuestionEditModeActive: false,

    scaleCounter: 1,
    oldScaleRowHtml: null,
    isScaleEditModeActive: false,

    // #############################################################################################################
    // Questions Management
    showQuestionTable: function(){
        $('div.l-testpoll div.likertScaleContainer table#questions-table tr#no-question-holder').hide();
        //$('div.l-testpoll div.likertScaleContainer').siblings('div.spacer').hide().siblings('div.form-actions').show();
    },

    hideQuestionTable: function(){
        $('div.l-testpoll div.likertScaleContainer table#questions-table tr#no-question-holder').show();
        //$('div.l-testpoll div.likertScaleContainer').siblings('div.spacer').show().siblings('div.form-actions').hide();
    },

    questionRowTemplate: function(index, value, inputs){
        return '<td>' + value + '</td>'
            + '<td class="img-cell"><span class="p-sprite drag-small-black"></span>' + inputs + '</td>'
            + '<td class="img-cell"><a href="javascript:void(0)" id="update-question-' + index + '"><span class="objlist-sprite p-sprite edit-black"></span></a></td>'
            + '<td class="img-cell"><a href="javascript:void(0)" id="delete-question-' + index + '"><span class="objlist-sprite p-sprite cross-small-red"></span></a></td>';
    },

    addQuestionTableRow: function(values) {
        // Show the table if this is the first question
        // If the getNumQuestions() return 0 then there is still no questions and we must show the table
        if(AnswersManager.getNumQuestions() == 0){
            AnswersManager.showQuestionTable();
        }

        var preName = 'questions['+AnswersManager.inputCounter+']',
            preId = 'questions-'+AnswersManager.inputCounter+'-';

        var inputs = '<input type="hidden" name="'+preName+'[sequence]" id="'+preId+'sequence" />'
            + '<input type="hidden" name="'+preName+'[question]" id="'+preId+'question" />';

        if (values.idQuestion) {
            inputs += '<input type="hidden" name="'+preName+'[id_question]" id="'+preId+'id_question" />';
        }

        var rowTemplate = AnswersManager.questionRowTemplate(AnswersManager.inputCounter, values.title, inputs);
        var html = '<tr id="question-table-input-row-'+ AnswersManager.inputCounter +'">' + rowTemplate + '</tr>';
        $('#questions-tbody').append(html);
        $('#'+preId+'question').val(values.title);
        if (values.idQuestion) { $('#'+preId+'id_question').val(values.idQuestion); }

        AnswersManager.inputCounter++;
        AnswersManager.updateQuestionIndexes();
    },

    deactivateQuestionEditMode: function() {
        var index = AnswersManager.oldQuestionRowHtml.id;
        var data = AnswersManager.oldQuestionRowHtml.data;
        $('tr#question-table-input-row-' + index).html(data);
        AnswersManager.isQuestionEditModeActive = false;
        AnswersManager.oldQuestionRowHtml = null;
    },

    deleteQuestionTableRow:  function(index) {
        $('#question-table-input-row-'+index).remove();
        if(AnswersManager.getNumQuestions() == 0){
            AnswersManager.hideQuestionTable();
        }
        // Update the indexes of the questions
        AnswersManager.updateQuestionIndexes();
    },

    updateQuestionIndexes: function(){
        $('table#questions-table tbody tr').each(function(i,e){
            var counter = i+1;
            $(e).find('input[id*="sequence"]').val(counter)
        });
    },

    getNumQuestions: function() { return $('#questions-tbody > tr[id*="question-table-input"]').length;},
    checkNumQuestions: function() { return (AnswersManager.getNumQuestions() >= 1);},
    //##################################################################################################################

    //##################################################################################################################
    // Scale management methods
    showScaleTable: function(){
        $('div.l-testpoll div.likertScaleContainer table#scales-table tr#no-scales-holder').hide();
    },

    hideScaleTable: function(){
        $('div.l-testpoll div.likertScaleContainer table#scales-table tr#no-scales-holder').show();
    },

    scaleRowTemplate: function(index, value, inputs){
        return '<td>' + index + '</td>'
            + '<td>' + value + '</td>'
            + '<td class="img-cell"><span class="p-sprite drag-small-black"></span>' + inputs + '</td>'
            + '<td class="img-cell"><a href="javascript:void(0)" id="update-scale-' + index + '"><span class="objlist-sprite p-sprite edit-black"></span></a></td>'
            + '<td class="img-cell"><a href="javascript:void(0)" id="delete-scale-' + index + '"><span class="objlist-sprite p-sprite cross-small-red"></span></a></td>';
    },

    addScaleTableRow: function(values) {
        // Show the table if this is the first scale
        // If the getNumScales() return 0 then there is still no scales and we must show the table
        if(AnswersManager.getNumScales() == 0){
            AnswersManager.showScaleTable();
        }

        var preName = 'scales['+AnswersManager.scaleCounter+']',
            preId = 'scales-'+AnswersManager.scaleCounter+'-';

        var inputs = '<input type="hidden" name="'+preName+'[sequence]" id="'+preId+'sequence" />'
            + '<input type="hidden" name="'+preName+'[scale]" id="'+preId+'scale" />';

        if (values.idScale) {
            inputs += '<input type="hidden" name="'+preName+'[id_scale]" id="'+preId+'id_scale" />';
        }

        var rowTemplate = AnswersManager.scaleRowTemplate(AnswersManager.scaleCounter, values.title, inputs);
        var html = '<tr id="scale-table-input-row-'+ AnswersManager.scaleCounter +'">' + rowTemplate + '</tr>';
        $('#scales-tbody').append(html);
        $('#'+preId+'scale').val(values.title);
        if (values.idScale) { $('#'+preId+'id_scale').val(values.idScale); }

        AnswersManager.scaleCounter++;
        AnswersManager.updateScaleIndexes();
    },

    deactivateScaleEditMode: function() {
        var index = AnswersManager.oldScaleRowHtml.id;
        var data = AnswersManager.oldScaleRowHtml.data;
        $('tr#scale-table-input-row-' + index).html(data);
        AnswersManager.isScaleEditModeActive = false;
        AnswersManager.oldScaleRowHtml = null;
    },

    deleteScaleTableRow: function(index) {
        $('#scale-table-input-row-'+index).remove();
        if(AnswersManager.getNumScales() == 0){
            AnswersManager.hideScaleTable();
        }
        // Upda te the scale indexes if a scale has been deleted
        AnswersManager.updateScaleIndexes();
    },

    updateScaleIndexes: function(){
        $('table#scales-table tbody tr[id*="scale-table-input"]').each(function(i,e){
            var counter = i+1;
            $(e).find('td:first-child').html(counter);
            $(e).find('input[id*="sequence"]').val(counter)
        });
    },

    getNumScales: function() { return $('#scales-tbody > tr[id*="scale-table-input"]').length; },
    checkNumScales: function() { return (AnswersManager.getNumScales() >= 1);},
    //##################################################################################################################

    questionListeners: function(){
        // Questions Management Listeners
        // Add new question
        $(document).on('click', 'a.add-question-button', function(){
            var input = $(this).closest('.headerContainer').find('input#addQuestionInput');
            var val = input.val().replace(/\s+/g, '');
            if (!val) {
                bootbox.dialog( Yii.t('poll', 'Please add question') , [{
                        label: Yii.t('standard', '_CANCEL'),
                        'class': 'btn-docebo big black',
                        callback: function() { }
                    }],
                    { header: Yii.t('standard', '_WARNING') }
                );
            }else{
                var inputs = { title: input.val() };
                AnswersManager.addQuestionTableRow(inputs);
                input.val('');
            }
        });

        // Add new question with enter key
        $(document).on('keydown', 'input#addQuestionInput',function(e) {
            if (e.keyCode == 13){
                e.stopImmediatePropagation();
                e.preventDefault();
                return false;
            }
        });

        $(document).on('keydown', 'input#questionEditInput', function(e){
            if (e.keyCode == 13){
                e.stopImmediatePropagation();
                e.preventDefault();
                return false;
            }
            if(e.keyCode == 27){
                AnswersManager.deactivateQuestionEditMode();
            }
        });

        $(document).on('keyup', 'input#questionEditInput', function(e){
            // Get the input current value
            var val = $(this).val().replace(/\s+/g, '');
            // If the value does not have any text, then DISABLE the add button
            if(!val){
                $(this).siblings('input').prop('disabled', true).addClass('disabled');
            }else{
                $(this).siblings('input').prop('disabled', false).removeClass('disabled');
            }
        });

        // Activate Edit Mode
        $(document).on('click', '#questions-tbody a[id*="update-question"]', function(){
            // Check if we are already in edit mode
            if(AnswersManager.isQuestionEditModeActive){
                AnswersManager.deactivateQuestionEditMode();
            }
            if(AnswersManager.isScaleEditModeActive){
                AnswersManager.deactivateScaleEditMode();
            }

            var index = $(this).attr('id').replace(/\D+/g, '');
            var preName = 'questions['+index+']',
                preId = 'questions-'+index+'-';
            var element = $("tr#question-table-input-row-" + index);
            AnswersManager.oldQuestionRowHtml = {id: index, data: element.html()};
            var oldValue = element.find('input#questions-'+index+'-question').val();
            var questionId = element.find('input#questions-'+index+'-id_question').val();

            var newHtml = "<td colspan='4' id='rowQuestionEditMode'>" +
                "<input id='questionEditInput' class='questionEditInput' type='text' value='"+oldValue+"'>" +
                "<input type='hidden' name='" + preName + "[id_question]' id='" + preId + "id_question' value='" + questionId + "' />" +
                "<input type='button' name='save' class='span2 btn-docebo green big' value='" + Yii.t('admin', 'Save') + "'>" +
                "</td>";
            element.html(newHtml);
            AnswersManager.isQuestionEditModeActive = true;
        });

        // When the user click on SAVE button in the EDIT MODE
        $(document).on('click', 'td#rowQuestionEditMode > input[type="button"]',function(e) {
            var newValue = $(this).closest('td#rowQuestionEditMode').find('input[type="text"]').val();

            // Check if the value is ok to be added
            if(!newValue.replace(/\s+/g, '')){ return false; }

            var index = $(this).closest('tr').attr('id').replace(/\D+/g, '');
            var preName = 'questions['+index+']',
                preId = 'questions-'+index+'-';
            var inputs = '<input type="hidden" name="'+preName+'[sequence]" id="'+preId+'sequence" />'
            + '<input type="hidden" name="'+preName+'[id_question]" id="'+preId+'id_question" />'
                + '<input type="hidden" name="'+preName+'[question]" id="'+preId+'question" />';

            // Build the new html
            var newHtml = AnswersManager.questionRowTemplate(index, newValue, inputs);
            // Apply the new html
            $(this).closest('tr').html(newHtml);
            $('#'+preId+'question').val(newValue);
            var questionId = $(this).closest('td#rowQuestionEditMode').find('input#questions-'+index+'-id_question').val();
            $('#'+preId+'id_question').val(questionId);
            AnswersManager.isQuestionEditModeActive = false;
            AnswersManager.updateQuestionIndexes();
        });

        // Delete Question ROW
        $(document).on('click', '#questions-tbody a[id*="delete-question"]', function(){
            var index = $(this).attr('id').replace(/\D+/g, '');
            bootbox.dialog(Yii.t('standard', '_AREYOUSURE'), [{
                    label: Yii.t('standard', '_CONFIRM'),
                    'class': 'btn-docebo big green',
                    callback: function() { AnswersManager.deleteQuestionTableRow(index); }
                }, {
                    label: Yii.t('standard', '_CANCEL'),
                    'class': 'btn-docebo big black',
                    callback: function() { }
                }],
                { header: Yii.t('standard', '_DEL'), classes: 'delete-question' }
            );
        });

        $('#questions-tbody').sortable({
            handle: '.drag-small-black',
            helper: function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            },
            update: function(e, ui) {
                AnswersManager.updateQuestionIndexes();
            }
        });
    },

    scaleListeners: function(){
        // Add new Scale
        $(document).on('click', 'a.add-scale-button', function(){
            var input = $(this).closest('.headerContainer').find('input#addScaleInput');
            var val = input.val().replace(/\s+/g, '');
            if (!val) {
                bootbox.dialog( Yii.t('poll', 'Please add scale') , [{
                        label: Yii.t('standard', '_CANCEL'),
                        'class': 'btn-docebo big black',
                        callback: function() { }
                    }],
                    { header: Yii.t('standard', '_WARNING') }
                );
            }else{
                var inputs = { title: input.val() };
                AnswersManager.addScaleTableRow(inputs);
                input.val('');
            }
        });

        // Add new Scale with enter key
        $(document).on('keydown', 'input#addScaleInput',function(e) {
            if (e.keyCode == 13){
                e.stopImmediatePropagation();
                e.preventDefault();
                return false;
            }
        });

        $(document).on('keydown', 'input#scaleEditInput', function(e){
            if (e.keyCode == 13){
                e.stopImmediatePropagation();
                e.preventDefault();
                return false;
            }
            if(e.keyCode == 27){
                AnswersManager.deactivateScaleEditMode();
            }
        });

        $(document).on('keyup', 'input#scaleEditInput', function(e){
            // Get the input current value
            var val = $(this).val().replace(/\s+/g, '');
            // If the value does not have any text, then DISABLE the add button
            if(!val){
                $(this).siblings('input').prop('disabled', true).addClass('disabled');
            }else{
                $(this).siblings('input').prop('disabled', false).removeClass('disabled');
            }
        });

        // Activate Edit Mode
        $(document).on('click', '#scales-tbody a[id*="update-scale"]', function(){
            // Check if we are already in edit mode
            if(AnswersManager.isScaleEditModeActive){
                AnswersManager.deactivateScaleEditMode();
            }
            if(AnswersManager.isQuestionEditModeActive){
                AnswersManager.deactivateQuestionEditMode();
            }

            var index = $(this).attr('id').replace(/\D+/g, '');
            var element = $("tr#scale-table-input-row-" + index);
            AnswersManager.oldScaleRowHtml = {id: index, data: element.html()};
            var oldValue = element.find('input#scales-'+index+'-scale').val();
            var scaleId = element.find('input#scales-'+index+'-id_scale').val();

            var preName = 'scales['+index+']',
                preId = 'scales-'+index+'-';

            var newHtml = "<td colspan='5' id='rowScaleEditMode'>" +
                "<input id='scaleEditInput' class='scaleEditInput' type='text' value='"+oldValue+"'>" +
                '<input type="hidden" name="'+preName+'[id_scale]" id="'+preId+'id_scale" value="' + scaleId + '" />' +
                "<input type='button' name='save' class='span2 btn-docebo green big' value='" + Yii.t('admin', 'Save') + "'>" +
                "</td>";
            element.html(newHtml);
            AnswersManager.isScaleEditModeActive = true;
        });

        // When the user click on SAVE button in the EDIT MODE
        $(document).on('click', 'td#rowScaleEditMode > input[type="button"]',function(e) {
            var newValue = $(this).closest('td#rowScaleEditMode').find('input[type="text"]').val();
            var index = $(this).closest('tr').attr('id').replace(/\D+/g, '');

            if(!newValue.replace(/\s+/g, '')) { return false };

            var preName = 'scales['+index+']',
                preId = 'scales-'+index+'-';
            var inputs = '<input type="hidden" name="'+preName+'[sequence]" id="'+preId+'sequence" />'
                + '<input type="hidden" name="'+preName+'[id_scale]" id="'+preId+'id_scale" />'
                + '<input type="hidden" name="'+preName+'[scale]" id="'+preId+'scale" />';

            // Build the new html
            var newHtml = AnswersManager.scaleRowTemplate(index, newValue, inputs);
            // Apply the new html
            $(this).closest('tr').html(newHtml);
            $('#'+preId+'scale').val(newValue);
            var scaleId = $(this).closest('td#rowScaleEditMode').find('input#scales-'+index+'-id_scale').val();
            $('#'+preId+'id_scale').val(scaleId);
            AnswersManager.isScaleEditModeActive = false;
            AnswersManager.updateScaleIndexes();
        });

        // Delete Scale ROW
        $(document).on('click', '#scales-tbody a[id*="delete-scale"]', function(){
            var index = $(this).attr('id').replace(/\D+/g, '');
            var message = $('#deleteScaleMessage').html();
            bootbox.dialog(message, [{
                    label: Yii.t('standard', '_CONFIRM'),
                    'class': 'btn-docebo big green',
                    callback: function() { AnswersManager.deleteScaleTableRow(index); }
                }, {
                    label: Yii.t('standard', '_CANCEL'),
                    'class': 'btn-docebo big black',
                    callback: function() { }
                }],
                { header: Yii.t('standard', '_DEL'), classes: 'delete-scale' }
            );
        });

        // If the user click outside of the TinyMCE and this click is not for adding code inside the TinyMCE then remove the flag
        $(document).on('click', function(e){
            if(AnswersManager.isQuestionEditModeActive || AnswersManager.isScaleEditModeActive){
                if(AnswersManager.isQuestionEditModeActive){
                    if(e.target.className == 'objlist-sprite p-sprite edit-black' || e.target.className == 'questionEditInput'){
                        return true;
                    }else{
                        AnswersManager.deactivateQuestionEditMode();
                    }
                } else if (AnswersManager.isScaleEditModeActive){
                    if(e.target.className == 'objlist-sprite p-sprite edit-black' || e.target.className == 'scaleEditInput'){
                        return true;
                    }else{
                        AnswersManager.deactivateScaleEditMode();
                    }
                }
            }
            return true;
        });

        $(document).on('mousedown', 'td.img-cell span.drag-small-black', function(e){
            if(AnswersManager.isQuestionEditModeActive || AnswersManager.isScaleEditModeActive){
                if(AnswersManager.isQuestionEditModeActive) {
                    AnswersManager.deactivateQuestionEditMode();
                }

                if (AnswersManager.isScaleEditModeActive) {
                    AnswersManager.deactivateScaleEditMode();
                }
            }
        });

        $('#scales-tbody').sortable({
            handle: '.drag-small-black',
            helper: function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            },
            update: function(e, ui) {
                AnswersManager.updateScaleIndexes();
            }
        });
    },

    init: function(options) {

        // #############################################################################################################
        if (Validation) {
            Validation.addValidator(AnswersManager.checkNumQuestions, Yii.t('test', 'Invalid question number'), 1);
        }

        if (options.questions) {
            var Questions = $.type(options.questions) == 'array' ? options.questions : [];
            for (var i=0; i<Questions.length; i++) {
                AnswersManager.addQuestionTableRow(Questions[i]);
            }
        }

        if (Validation) {
            Validation.addValidator(AnswersManager.checkNumScales, Yii.t('test', 'Invalid scale number'), 1);
        }

        if (options.scales) {
            var Scales = $.type(options.scales) == 'array' ? options.scales : [];
            for (var i=0; i<Scales.length; i++) {
                AnswersManager.addScaleTableRow(Scales[i]);
            }
        }

        // Add listeners to both grids
        this.questionListeners();
        this.scaleListeners();
        // #############################################################################################################
    }

};