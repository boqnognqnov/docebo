/**
 * Document ready function for main player module js
 */

var CoursePlayer = function() {

	// General method for ALL runing Dialog2s, avoid using code, specific for a given dialog.
	// All! Dialog2 dialogs will be closed by sending them <a class="auto-close"></a> as a content
	$(document).on("dialog2.content-update", ".modal", function () {
		if ($(this).find("a.auto-close").length > 0) {
			try {
                $(this).find(".modal-body").dialog2("close");
            } catch(e) {}
		}
	});
};
CoursePlayer.prototype = {

	/**
	 * internally used, holding current options
	 */
	_options: {},

	/**
	 * Init can be called many times to add more options (converted in object attributes)
	 */
	init: function (options) {
		this.setOptions(options);
	},


	setOptions: function (options) {
		// Deep Extend _options to allow consequitive call to add more options.
		this._options = $.extend(true, this._options, options);
		// Create object attribute for every _options[k]
		for(var k in this._options) {
			this[k] = this._options[k];
		}
	},


	/**
	 * Update the course header witht the new status
	 * @deprecated
	 */
	updateCourseHeader: function () {

		$.ajax({
			url: this.updateCourseHeaderUrl,
            data: {
                targetMode: $('#player-manage-mode-selector input[name=target-mode]:checked').val()
            },
			error: function (data) {
				Docebo.Feedback.show('error', "Ajax error");
			},
			success: function (data) {
				data = JSON.parse(data);
				if (data.success == true) {
					$("#player-courseheader-container").replaceWith(data.html);
				}
			}
		});
	},

	/**
	 * Logging method
	 */
	log: function () {
		if (window.console && window.console.log) {
			window.console.log.apply(window.console, arguments);
		}
	}

};

var Player = false;

$(function () {
	Player = new CoursePlayer();

	$('#player-blocks-container').on("click", ".nav.nav-tabs > li > a", function (e) {
		setTimeout(function(){Blocks.equalizeNeighborsHeights();}, 10);
	});
});