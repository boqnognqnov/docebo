/**
 * Arena Object
 * ------------
 * This regulate the behaviour of the arena play area and edit area and the lonav on the left
 **/
var Arena = {
	quickNavEnabled: true,
	firstLoToPlay: null,
	currentView: 'play', // play|management
	overallFadeSpeed: 300,
	hideMenuPosition: '-402px',
	ajaxLoader: '<div class="ajaxloader" style="display: none;"></div>',
	course_id: '',
	createFolderDialogUrl: '',
	uploadDialogUrl: '',
	uploadFilesUrl: '',
	saveUploadedCoursedocUrl: '',
	assetsBaseUrl: '',
	csrfToken: '',
	getCourseLearningObjectsTreeUrl: '',
	deleteLoUrl: '',
	sortLearningObjectUrl: '',
	moveInFolderLearningObjectUrl: '',
	userCanAdminCourse: false,
	openInCentralRepositoryUrl: '',
	reloadSocialRatingUrl: '',
	courseInfo: {
		idCourse: 0,
		code: '',
		name: ''
	},
	onLoadArenaMode: 'view_course',
	createTestUrl: '',
	createPollUrl: '',
	videoExtensionsWhitelist: '',
	videoConversionInProgressHtml: '<a rel="tooltip" title="' + Yii.t('authoring', 'CONVERSION_IN_PROGRESS') + '"><span class="video-converting-animation"></span></a>',
	videoConversionErrorHtml: '<a title="'+ Yii.t('authoring', 'Error_while_converting_file') +'"><span class="lo-check i-sprite objlist-sprite is-circle-check red" ></span></a>',
	videoLoGreenHtml : '<span class="lo-check p-sprite objlist-sprite check-green"></span>',
	videoStatusPollJobs: [],
	videoLoNodesToCheck: [],
	playerLayout: 'play_button', // LearningCourse::$PLAYER_LAYOUT_PLAY_BUTTON or LearningCourse::$PLAYER_LAYOUT_LISTVIEW
	resumeAutoplay: false,
	currentFolder: false,
	redirectUrl: '',
	toReload: false,
	lastPlayedLo: null,

	/**
	 * Initialize the environment
	 * @param options
	 */
	init: function (options) {
		this.course_id = options.course_id;
		this.createFolderDialogUrl = options.createFolderDialogUrl;
		this.uploadDialogUrl = options.uploadDialogUrl;
		this.uploadFilesUrl = options.uploadFilesUrl;
		this.openInCentralRepositoryUrl = options.openInCentralRepositoryUrl;
		this.saveUploadedCoursedocUrl = options.saveUploadedCoursedocUrl;
		this.assetsBaseUrl = options.assetsBaseUrl;
		this.pluploadAssetsUrl = options.pluploadAssetsUrl;
		this.csrfToken = options.csrfToken;
		this.getCourseLearningObjectsTreeUrl = options.getCourseLearningObjectsTreeUrl;
		this.deleteLoUrl = options.deleteLoUrl;
		this.sortLearningObjectUrl = options.sortLearningObjectUrl;
		this.moveInFolderLearningObjectUrl = options.moveInFolderLearningObjectUrl;
		this.userCanAdminCourse = options.userCanAdminCourse;
		this.courseInfo = $.extend({}, this.courseInfo, options.courseInfo);
		this.onLoadArenaMode = options.onLoadArenaMode;
		this.axEditLoPrerequisitesUrl = options.axEditLoPrerequisitesUrl;
		this.createTestUrl = options.createTestUrl;
		this.createPollUrl = options.createPollUrl;
		this.videoExtensionsWhitelist = options.videoExtensionsWhitelist;
		this.reloadSocialRatingUrl = options.reloadSocialRatingUrl;
		this.forcePlayLoKey = 	options.forcePlayLoKey;  // a LO key (123 or 123:456 for SCO) to go (not play, just load in launchpad) on FIRST LO tree load; later - NOPE
		this.checkStatusUrl = options.checkStatusUrl;
		this.redirectUrl = options.redirectUrl;
		// bootbox.animate(false);
	},

	/**
	 * Hide the main arean container
	 */
	hideAllArenaContent: function () {
		$("#player-arena-content > div").hide();
	},


	/**
	 * Show LO management panel
	 */
	showLoManagement: function () {
		Arena.hideAllArenaContent();
		$('#player-lo-manage-panel').show();
	},

	/**
	 * Show LO Arena Content & Launchpad
	 */
	showLoLaunchPad: function () {
		Arena.hideAllArenaContent();
		$('#player-arena-launchpad').show();
		//Docebo.scrollTo('#arena-board-top');
	},

	/**
	 * Show LO Navigation area
	 */
	showLoNavigation: function () {
		Arena.hideAllArenaContent();


		Arena.showLoLaunchPad();

		//$('#player-lonav').removeClass('close');
		//$('#player-lonav').addClass('open');
		//$('#player-lonav').css('left', $("#menu").width()+1);
		//$('#player-lonav').attr("data-opened", true);
	},

	/**
	 * Hide LO Navigation area
	 */
	hideLoNavigation: function () {
		$('#player-lonav').removeClass('open');
		$('#player-lonav').addClass('close');
		//$('#player-lonav').css('left', this.hideMenuPosition);
		//$('#player-lonav').attr("data-opened", false);
	},


	showQuickNav: function () {
		if (Arena.playerLayout === 'listview') {
			if ($('#player-arena-launchpad').html() == '' || $('#player-arena-launchpad').css('display') == 'none')
				$('#player-quicknavigation').show();
			else
				$('#player-quicknavigation').hide();
		} else
			$("#player-quicknavigation").show();

		$("#player-quicknavigation").unblock();
		Arena.enableQuickNav();
	},


	hideQuickNav: function() {
		// Quick navigation is always shown (in both play and LO management mode)
		// except when the course has a player layout of "List View"
		// in which case the Quick Navigation is not the black stripe
		// but the new LO listing layout, so in this case it makes
		// sense to hide it in LO management mode
		// In all other cases we should not hide it
		if($("#player-quicknavigation").hasClass('listview')){
			$("#player-quicknavigation").hide();
		}else{
			$("#player-quicknavigation").show();
		}
	},


	blockQuickNav: function() {
		return; // decided to have Quicknav always shown
		$("div#player-quicknavigation").block({
			message: "",
			timeout: 0,
			overlayCSS:  {
				backgroundColor: 	'#CCC',
				opacity:         	0.6,
				cursor:          	'not-allowed'
			}
		});
	},


	enableQuickNav: function() {
		return true; // decided to have Quicknav always shown
		Arena.quickNavEnabled = true;
	},

	disableQuickNav: function() {
		return true; // decided to have Quicknav always shown
		Arena.quickNavEnabled = false;
	},

	reloadQuickNavs: function(){


        if(Arena.playerLayout ){
            if(Arena.playerLayout !== 'listview'){
                // Throttle this method as being resource and time expensive; do not allow to be run again in less than 5 secs
                if (!Throttle.canRun('reloadQuickNavs', 5000)) return;
            }
        }else{
            if (!Throttle.canRun('reloadQuickNavs', 5000)) return;
        }


		
		
		var quicknavUrl = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=player/training/axGetQuickNavHtml&course_id=' + Arena.course_id;
		$(".player-quicknav-wrapper").load(quicknavUrl, function(){
			if (!Arena.quickNavEnabled) {
				Arena.blockQuickNav();
			}

			// Activate carousel item containing the First-to-lay object
			if (Arena.firstLoToPlay) {
				var carouselIndex = parseInt($('[id="player-quicknav-object-' + Arena.firstLoToPlay.key + '"]').data('carousel-index'));
				$('#player-quicknav-carousel').carousel(carouselIndex);
				// This is NON-sliding variant!
				//$('#player-quicknav-carousel .item').removeClass('active');
				//$('#player-quicknav-object-' + Arena.firstLoToPlay.key).closest('.item').addClass('active');
			}

			// Since this is a delayed AJAX and we may have already
			// switched to the LO management (admin) view before
			// the result of the AJAX is received, we have to check
			// if we should show this LO nav block here first
			if(Arena.currentView==='play'){
				Arena.showQuickNav();

				switch(Arena.playerLayout){
					case 'listview':
						if($('#player-arena-launchpad').html() == '' || $('#player-arena-launchpad').css('display') == 'none')
							$('#player-quicknavigation').show();
						else
							$('#player-quicknavigation').hide();
						break;
					case 'navigator':
						break;
				}
			}else{
				if(Arena.playerLayout==='listview'){
					// The LO quicknav has two states/layouts depending on the
					// following order:
					// 1. Course specific setting set in the course's Settings page
					// 2. LMS wide setting if the above is not set (Advanced Settings)
					// The two available layouts are "Play Button" and "List View"
					// where Play Button will load the "black stripe" carousel below
					// the course and the List View layout will reuse the same widget
					// but will load a vertical List View (5.4 type) list of LOs.
					// The Play Button's black stripe is always visible, whereas
					// the List View's vertical LO list is visible only in Play mode
					// (not visible in LO management)
					$('#player-quicknavigation').hide();
				}else{
					// All other cases the black stripe is always visible
					// (both LO management and play LO area)
					Arena.showQuickNav();
				}
			}

		});
	},

	playByLoKey: function(loKey){
		loKey = ""+loKey;

		var loNode = $('.player-lonav-tree').dynatree("getTree").getNodeByKey(loKey);
		$('.player-lonav-tree').dynatree("getTree").activateKey(loKey);

		var scroll_top = parseInt($(window).scrollTop());
		var scroll_left = parseInt($(window).scrollLeft());

		// Directly PLAY the LO
		Launcher.dispatch(loNode.data, function(){
			$(window).scrollTop(scroll_top);
			$(window).scrollLeft(scroll_left);
			if(Arena.playerLayout==='navigator' || Arena.playerLayout==='listview'){
				PlayerNavigator.setState('playing');
				PlayerNavigator.setCurrentKey(loNode.data.key);
				PlayerNavigator.reload();
			}
		});
	},


	/**
	 * Toggle LO Navigation area
	 */
	toggleLoNavigation: function () {
		if ($('#player-lonav').attr("data-opened") == "true") {
			this.hideLoNavigation();
		} else {
			this.showLoNavigation();
			this.setViewCourseMode();
		}
	},

	/**
	 * Switch the interface back to the arena edit mode and close the navigation area
	 */
	setEditCourseMode: function () {
		$(".player-lo-header").show();

		this.setSwitchToAdminView();
		this.hideLoNavigation();
		this.showLoManagement();

		if(Arena.playerLayout==='listview'){
			$('#player-quicknavigation').hide();
		}else {
			$('#player-quicknavigation').show();
		}
		$('#popup_bootstroAddBlockLauncher').show();

		var notice = $("#course-expired-softdeadline-warning");
		if(notice)
			notice.hide();
		$(document).trigger('adminViewActivated');
	},

	/**
	 * Switch the interface to the arena launchpad
	 */
	setViewCourseMode: function () {
		$(".player-lo-header").hide();

		this.setSwitchToLearnerView();


		if(Arena.playerLayout==='listview'){
			Arena.hideAllArenaContent();
			Launcher.resetToListView();
		}else{
			Arena.setLaunchpadMode();
		}


		this.showQuickNav();
		$('#popup_bootstroAddBlockLauncher').hide();

		var notice = $("#course-expired-softdeadline-warning");
		if(notice)
			notice.show();
	},

	/**
	 * Change the radio switcher to LEARNER position
	 */
	setSwitchToLearnerView: function() {
		$('#player-manage-mode-selector label').removeClass('active').eq(0).addClass('active');
		$('#player-manage-mode-selector-switcher').removeClass('active');
		Arena.currentView = 'play';
	},

	/**
	 * Change the radio switcher to ADMIN VIEW position
	 */
	setSwitchToAdminView: function() {
		$('#player-manage-mode-selector label').removeClass('active').eq(1).addClass('active');
		$('#player-manage-mode-selector-switcher').addClass('active');
		Arena.currentView = 'management';
	},



	/**
	 * Close opened items and display the launchpad only
	 */
	setLaunchpadMode: function () {
		this.hideLoNavigation();
		this.hideAllArenaContent();

		this.showLoLaunchPad();

	},

	hoverAndHintListeners: function () {
		$('#player-manage-add-lo-button .dropdown-menu .add-lo-type-description').hover(function () {
			$("#add-lo-type-hint").html('<span class="pull-left hint-ico"><span class="'+$(this).data("hint-icon-class")+'"></span></span> '+$(this).data("hint-text"));
		}, function () {
			$("#add-lo-type-hint").html('<span class="span12 text-left">'+$("#add-lo-type-hint").data("default-text")+'</span>');
		});
	}

};
// Arena

// Learning Objects Tree (for Play)
Arena.learningObjects = {
	_loToSprite: {
		'authoring':    'is-converter',
		'item':         'is-file',
        'file':         'is-file',
        'htmlpage':     'is-htmlpage',
		'deliverable':  'is-deliverable',
		'poll':         'is-poll',
		'test':         'is-test',
		'video':        'is-video',
		'scorm':        'is-scorm',
		'tincan':       'is-tincan',
		'scormorg':     'is-zip',
		'aicc':         'fa fa-archive',
        'elucidat':     'is-elucidat',
		'googledrive':  'is-googledrive',
		'lti':          'is-lti'
	},

	options: {
		contentViewer: {},
		management: {}
	},

	// Call management and viewer constructors
	init: function (options) {
		this.options = $.extend({}, this.options, options);
		this.contentViewer.init(this.options.contentViewer);
		this.management.init(this.options.management);
		$('.dynatree-container').parent().addClass('dynatree-container-parent');
	},


	reloadTrees: function () {
		this.contentViewer.reloadTree();
		this.management.reloadTree();
		this.expandAll();
	},

	expandAll: function () {
		this.contentViewer.expandAll();
		this.management.expandAll();
	},
	getSpriteForObject: function (/* String */object) {
		return (this._loToSprite[object] ? this._loToSprite[object] : '');
	},

	reloadSocialRatingPanel: function(data)
	{
		if (data.countTotal == data.countCompleted)
		{
			if ($("#social_rating_course").length > 0)
			{
				$.ajax({
					url: Arena.reloadSocialRatingUrl,
					type: 'get'
				}).done(function (data) {
					$("#social_rating_course").replaceWith(data);
				});
			}
		}
	},

	// Do an AJAX call to refresh Dynatrees
	ajaxUpdate: function (treeType, successCallback, canRunCheck) {

        if(typeof canRunCheck !== 'undefined'){
            canRunCheck = canRunCheck
        }else{
            canRunCheck = true;
        }

        if(canRunCheck){
       		if (!Throttle.canRun('arenaAjaxUpdate', 5000)) return;
        }

		//validate treeType parameter
		var requestType = [];
		switch (treeType) {
			case 'play':
			case 'management':
			{
				requestType.push(treeType);
			}
				break;
			default:
			{ //unspecified or invalid type: ask for both trees
				requestType.push('play');
				requestType.push('management');
			}
				break;
		}
		//do ajax request

		var lastPopupPlayed = false, lpp_el = $('#last_popup_played-input');
		var CourseLearningObjectsTreeUrl = Arena.getCourseLearningObjectsTreeUrl;
		if (lpp_el.length > 0) {
			lastPopupPlayed = lpp_el.val();
			CourseLearningObjectsTreeUrl += '&last_popup_played=' + lastPopupPlayed;
		}

		Arena.firstLoToPlay = false;

		$.ajax({
			url: CourseLearningObjectsTreeUrl,
			type: 'post',
			dataType: 'JSON',
			data: {
				course_id: Arena.course_id,
				tree_type: requestType.join(','),
				lastPlayedLo: Arena.lastPlayedLo,
				lo_key: Arena.forcePlayLoKey // This is set ONCE upon main Player page is loaded (See Training controller) Sets which LO to load upon tree update
			}
		}).done(function (res) {
				// Once LO tree is loaded, always reset this option; otherwise Player will load always the same object (in the launchpad)
				Arena.forcePlayLoKey = false;

				if (res.success) {
					if (res.data) {
						//var redirectUrl = res.data.redirectUrl;
						if (res.data.play) {
							Arena.playerLayout = res.data.player_layout;

							// Some widgets are sensitive to training materials (status, etc.). Load ONLY them
							$('.player-block-courseinfo').each(function () {
								$(this).loadOneBlockContent();
							});
							Arena.learningObjects.contentViewer.setTree(res.data.play);
							Arena.firstLoToPlay = res.data.firstToPlay;
							if (!(Arena.firstLoToPlay == false)) {
								// Navigate/Activate the first LO to play. All parents will be expanded as necessary
								$('.player-lonav-tree').each(function () {
									$(this).dynatree("getTree").activateKey(Arena.firstLoToPlay.key);
								});
							}


							Arena.resumeAutoplay = res.data.resume_autoplay;
						}
						if (res.data.management) {
							Arena.learningObjects.management.setTree(res.data.management);
						}

						$(document).trigger("lo_list_refresh");

						// Update LO Counters/Stats in Mainmenu
						if (typeof theMainMenu !== 'undefined' && theMainMenu) {
							theMainMenu.updateLoStats();
						}


						// Reload  Quicknavs to reflect changes in tree
						Arena.reloadQuickNavs();

						// Call Success callback, if any
						if ($.isFunction(successCallback)) {
							successCallback(res.data);
						}

						Arena.learningObjects.reloadSocialRatingPanel(res.data);

					}
				}
			}).always(function () {

			}).fail(function (jqXHR, textStatus, errorThrown) {

			});
	}
};

//
Arena.learningObjects.management = {
	currentZIndex: 1000,
	tree: [],
	hasFoldersInRoot: function () {
		var hasFolders = false;
		var tree = Arena.learningObjects.management.tree;
		for(var child in tree) {
			if (tree[child].isFolder) {
				hasFolders = true;
			}
		}
		return hasFolders;
	},

	hasSubfolders: function (elements, cache) {
		// Cache the information in the node for a little performance boost
		if (typeof elements.hasSubfolders == 'undefined' || cache !== true) {
			elements.hasSubfolders = false;
			for(key in elements) {
				if (elements[key].isFolder) {
					elements.hasSubfolders = true;
				}
			}

		}

		return elements.hasSubfolders;
	},
	options: {
		// Called when the node is dropped
		onNodeMoved: function (ui, item, dropPosition, siblings, type) {
		}
	},

	init: function (options) {
		var me = this;
		this.options = $.extend({}, this.options, options);
		if (this.hasFoldersInRoot()) {
			$('#player-arena-overall-folders').dynatree({
				selectMode: 1,
				onPostInit: function (isReloading, isError) {
				},
				onCreate: function (node, nodeSpan) {
					var $nodeSpan = $(nodeSpan);
					var bgSpan = $('<span class="background-loitem"></span>');
					if (node.data.isFolder) {
						bgSpan.addClass('folder');
						$nodeSpan.before(bgSpan);
						$nodeSpan.siblings().addBack().hover(function () {
							bgSpan.addClass('hover');
							$(this).addClass('hover');
						}, function () {
							bgSpan.removeClass('hover');
							$(this).removeClass('hover');
						});

						$nodeSpan.parent().attr('data-object-id', node.data.key);

					} else {
						$nodeSpan.parent().css("display", "none");
					}

					$nodeSpan.parent().droppable({

						tolerance: "pointer",
						greedy: true,
						over: function (event, ui) {
							$(ui.helper).attr('data-drop-type', 'folder');
						},
						out: function (event, ui) {
							$(ui.helper).attr('data-drop-type', 'sort');
						},
						drop: function (event, ui) {
							var folderId = $(this).attr('data-object-id');
							me.options.onNodeMoved(ui, $(ui.helper).attr('data-object-id'), folderId, [], $(ui.helper).attr('data-drop-type'));
							return false; // Prevent propagation to parent
							//folder
						}
					});


				},

				onRender: function (node, nodeSpan) {
					var $nodeSpan = $(nodeSpan);
					if (node.data.isFolder) {
						var cssClass = '';
						// Use the first row for check if the content in right
						// panel is shown
						// var opened = ($nodeSpan.hasClass('dynatree-expanded')
						// || $nodeSpan.hasClass('dynatree-active')) &&
						// Arena.learningObjects.hasSubfolders(node.data.children,
						// true);
						var opened = $nodeSpan.hasClass('dynatree-expanded');
						cssClass = opened ? "is-folder opened" : "is-folder closed";
						if (node.data.sort == '1') {
							cssClass += ' is-root';
						}
						else if (Arena.learningObjects.management.hasSubfolders(node.data.children, true)) {
							cssClass += " has-children";
						}
						$nodeSpan.find('span.dynatree-icon').addClass('p-sprite '+cssClass).removeClass('dynatree-icon');
						$nodeSpan.parent().addClass('droppable-folder');
						if (node.data.sort == '1') { // Root node
							$nodeSpan.children('.dynatree-title')
								.html('<div class="course-name">'+Arena.courseInfo.name+'</div>')
								.addClass("is-root");
						}
					}
					$(document).trigger("lo_list_refresh_hook");
				},

				onClick: function (node, event) {
					me.showFolder(node.data.children, node);
				},

				onActivate: function (node) {

				},

				children: Arena.learningObjects.management.tree
			});
		} else {
			// Just make this element a Dynatree, having empty tree (is this
			// ok?)
			$('#player-arena-overall-folders').dynatree({
				children: []
			});
			this.showFolder(Arena.learningObjects.management.tree);
		}

	},

	reloadTree: function () {
		this.init(this.options);
		$('#player-arena-overall-folders').dynatree("getTree").reload(function(){
			// Traverse all nodes. If node is Video and in in_progress status, add it to the array of videos to check
			// This is to allow check process to start on ALL videos, including in folders
			// Otherwise, the rpocess will start ONLY when folder is opened
			// (implemented in onCreate method)
			this.getRoot().visit(function (node) {
				if (node.data.type == 'video' && node.data.is_video_transcoding === 1) {
					Arena.videoLoNodesToCheck[node.data.key] = {key: node.data.key, 
						node: node, 
						counter: 0, 
						is_video_transcoding: 1, 
						is_video_transcoding_error: 0,
						resolved: 0
					};
				}
			});
		});
		this.expandAll();
	},

	setTree: function (tree) {
		this.tree = tree;
		this.reloadTree();
	},



	/**
	 * A function to start 'polling' the server every 'interval' miliseconds (for a 'maxCounter' numbers of retry)
	 * to get the a VIDEO LO conversion status from the Database.
	 *
	 * The status in database is set by a callback controller/action, called by Amazon SNS (notification service), so
	 * we do not have to call Amazon here, we just call out controller to get info.
	 *
	 *
	 *  @param node Dynatree node which holds LO information
	 *  @param nodeSpan Dynatree nodeSpan, holdeing DOM element for the given LO
	 *
	 */
	pollVideoConversionStatus: function(node, nodeSpan) {

		var interval = 5000;
		var maxCounter = 60;
		
		// In case the polling job for this node is already started, just get out
		if ($.inArray(node.data.key, Arena.videoStatusPollJobs) > -1) return;

		// We are interested in VIDEO LOs only
		if (node.data.type != 'video') return;

		// Stack the node key in the pool of running jobs for later checks
		Arena.videoStatusPollJobs.push(node.data.key);

		var counter = 0;
		(function refreshVideoStatus(node, nodeSpan){
			counter++;
			var url = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=player/training/axGetVideoConversionStatus&idOrg=' + node.data.idOrganization + '&course_id=' + Arena.course_id;
			$.getJSON(url, function(res){

				if (res.success) {
					
					var removeKeyFromJobs = false;
					
					if ((res.data.status == 'in_progress') && (counter > maxCounter)) {
						$(nodeSpan).find('.player-postfix .video-conversion-in-progress').replaceWith(Arena.videoConversionErrorHtml);
						removeKeyFromJobs = true;
					}
					if (res.data.status == 'in_progress') {
						setTimeout(function() {refreshVideoStatus(node, nodeSpan);},interval);
					}
					else if (res.data.status == 'error') {
						$(nodeSpan).find('.player-postfix .video-conversion-in-progress').replaceWith(Arena.videoConversionErrorHtml);
						removeKeyFromJobs = true;
						Arena.learningObjects.ajaxUpdate();
					}
					else if (res.data.status == 'converted') {
						$(nodeSpan).find('.player-postfix .video-conversion-in-progress').replaceWith(Arena.videoLoGreenHtml);
						removeKeyFromJobs = true;
						Arena.learningObjects.ajaxUpdate();
					}

					// Remove Node key from running jobs: it is done.
					if (removeKeyFromJobs) {
						Arena.videoStatusPollJobs.splice( $.inArray(node.data.key, Arena.videoStatusPollJobs), 1 );
					}
					
				}
			});
		})(node, nodeSpan);


	},




	showFolder: function (data, parentNode) {

		var me = this;
		var updateZIndex = function () {
			$('.player-arena-buttongroup').reverse().each(function (index, element) {
				$(this).css("z-index", index+2);
			});
		};

		$('#player-arena-overall-objlist').dynatree({
			clickFolderMode: 1,
			selectMode: 1,

			onPostInit: function (isReloading, isError) {
				currentIndex = 0;
				updateZIndex();
				
				$('#player-arena-overall-objlist .droppable-folder').droppable({
					hoverClass: 'droppable-hovered',
					//greedy: true,
                    accept: 'li',
                    tolerance: 'pointer',
					over: function (event, ui) {
                    },
					out: function (event, ui) {
						$(ui.helper).attr('data-drop-type', 'sort');
					},
					drop: function (event, ui) {
                        // The new requirement then is to completely disable DROPPING items into folders in the RIGHT content panel ...
                        return false

                        $(ui.helper).attr('data-drop-type', 'folder');
						var folderId = $(this).attr('data-object-id');

						me.options.onNodeMoved(ui, $(ui.helper).attr('data-object-id'), folderId, [], $(ui.helper).attr('data-drop-type'));

						return false; // Prevent propagation to parent
					}
				});
			},

			onExpand: function (flag, node) {
				updateZIndex();
			},

			onCreate: function (node, nodeSpan) {

				var idOrg = parseInt(node.data.idOrganization);			
				var $nodeSpan = $(nodeSpan);
				var buttons = '';

				// Data attribute to set
				$nodeSpan.parent().attr('data-object-id', node.data.key);
				$nodeSpan.parent().attr('id', node.data.key);

				// remove unused node and setup classes
				$nodeSpan.find('span.dynatree-icon').remove();
				$nodeSpan.parent().addClass('player-arena-li');
				if (node.data.isFolder) $nodeSpan.parent().addClass('player-arena-tree-folder');

				// prefix zone ------------------
				var prefix = $('<span class="player-prefix"></span>');
				$nodeSpan.prepend(prefix);	

				// Drag Handler
				prefix.append( $('<span class="lo-drag p-sprite objlist-sprite drag-small-black"></span>') );
				$nodeSpan.find('.lo-drag').on("click", function () { return false; });

				// Type Icon
				var cssClass = '';
				if (!node.data.isFolder) {
					cssClass = 'object-icon i-sprite objlist-sprite ' + Arena.learningObjects.getSpriteForObject(node.data.type);
				} else {
					cssClass = "p-sprite folder-grey";
					$nodeSpan.parent().addClass('droppable-folder');
				}
				prefix.append( $('<span class="' + cssClass + '"></span>') );

				// postfix zone ---------------------
				var postfix = $('<span class="player-postfix"></span>');
				$nodeSpan.prepend(postfix);

				// show a lock icon if some prereq is setted up
				postfix.append( $('<span class="lo-locked p-sprite objlist-sprite lock-small-black" '
					+(node.data.prerequisites === '' ? 'style="visibility:hidden"' : '')+'></span>') );

				// If video has been already checked by AJAX calls, get its new UI-ONLY status!
				if (Arena.videoLoNodesToCheck[node.data.key]) {
					node.data.is_video_transcoding       = Arena.videoLoNodesToCheck[node.data.key].is_video_transcoding;
					node.data.is_video_transcoding_error = Arena.videoLoNodesToCheck[node.data.key].is_video_transcoding_error;
					node.data.is_visible 				 = Arena.videoLoNodesToCheck[node.data.key].visible;
				}

				// Show animation icon if video is under conversion
				if (parseInt(node.data.is_video_transcoding) == 1) {
					postfix.append($('<span class="video-conversion-in-progress">' + Arena.videoConversionInProgressHtml + '</span>'));
					Arena.videoLoNodesToCheck[node.data.key] = $.extend({}, Arena.videoLoNodesToCheck[node.data.key], {nodeSpan: nodeSpan});
				}
				// Show "error" icon if we get some error during conversion
				else if (parseInt(node.data.is_video_transcoding_error) == 1) {
					postfix.append($(Arena.videoConversionErrorHtml));
				}
				// Otherwise, show normal Green, "available" icon
				else {
					postfix.append( $('<span class="lo-check p-sprite objlist-sprite '
							+(parseInt(node.data.is_visible) > 0? 'check-green' : 'check-grey')+ '"></span>') );
				}

				// dropdown menu actions
				var actions = '';

				// CONFIGURE
				if ( (!node.data.isFolder) && (parseInt(node.data.is_video_transcoding) == 0) && (parseInt(node.data.is_video_transcoding_error) == 0)) {
					var configUrl = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=player/training/axConfigureLo&idOrg=' + idOrg + '&course_id=' + Arena.course_id;
					actions +=  '<li><a href="' + configUrl +
								'" class="open-dialog p-hover" data-dialog-class="modal-configure-lo" rel="modal-configure-lo-' + idOrg +
								'" role="menuitem">' +
								'<span class="objlist-sprite i-sprite is-gearpair"></span>'+
								'<span class="menu-label"> '+ Yii.t('standard', 'Settings') + '</span> ' +
								'</a></li>';
				}

				// COPY
				//actions += '<li><a href="#" role="menuitem"><span class="objlist-sprite i-sprite is-clone"></span><span class="menu-label"> Copy</span></a></li>';

				// PREREQUISITES
				if (!node.data.isFolder) {
					var prereqUrl = Arena.axEditLoPrerequisitesUrl+'&id_object=' + idOrg + '&course_id=' + Arena.course_id;
					actions += '<li><a href="' + prereqUrl +
						'" class="open-dialog p-hover" rel="modal-edit-prerequisites-'+idOrg+
						'" role="menuitem" data-dialog-class="modal-edit-prerequisites">'+
						'<span class="objlist-sprite i-sprite is-link"></span>'+
						'<span class="menu-label"> '+Yii.t('standard', '_PREREQUISITES')+'</span></a></li>';
				}

				actions += '<li class="lo-menu"><hr/></li>';

				// place an evaluation link for the deliverabels
				if (node.data.isEvaluationAllowed && (node.data.type == 'deliverable' || node.data.type == 'test' || node.data.type == 'poll')) {

					actions += '<li><a href="index.php?r=player/report&course_id=' + Arena.course_id+'" class="lo-evaluate p-hover" data-id-object="'+idOrg+'">'
						+'<span class="objlist-sprite i-sprite is-check"></span>'
						+'<span class="menu-label"> '+Yii.t('classroom', 'Evaluation')+'</span></a></li>'
						+ '<li class="lo-menu"><hr/></li>';
				}

				// EDIT FOLDER
				if (node.data.isFolder) {
                    actions += '<li><a href="#" role="menuitem" '
					+'class="lo-edit-folder p-hover" data-id-object="'+idOrg+'">'
					+'<span class="objlist-sprite i-sprite is-edit"></span>'
					+'<span class="menu-label"> '+Yii.t('standard', '_MOD')+'</span></a></li>';
				}

				var editUrl = false;

				if(typeof node.data.id_object !== "undefined" && node.data.id_object !== null){
					editUrl = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=centralrepo/centralLo/editShared&id_course' + Arena.course_id + '&id_org=' + idOrg;
				}

				// EDIT
				if (!node.data.isFolder) {
					if(node.data.id_object !== null && typeof node.data.id_object !== "undefined" && $.inArray(node.data.type, ['scormorg', 'aicc', 'tincan', 'elucidat', 'file', 'video']) === -1){
						//do not show edit button if the repoObject has no versioning!!!!
					} else{
						switch (node.data.type) {
							case 'test':
							case 'poll':
								var editHref = (editUrl !== false) ? editUrl : Docebo.lmsAbsoluteBaseUrl + '/index.php?r=' + node.data.type + '/default/edit&id_object=' + idOrg + '&course_id=' + Arena.course_id;
								actions += '<li><a href="' + editHref + '" '
									+'role="menuitem" class="lo-edit p-hover" '
									+'data-lo-type="' + node.data.type + '" data-id-object="' + idOrg + '">'
									+'<span class="objlist-sprite i-sprite is-edit"></span>'
									+'<span class="menu-label"> '+Yii.t('standard', '_MOD')+'</span></a></li>';
								break;
							case 'video':
							case 'file':
							case 'item':
							case 'aicc':	
							case 'scormorg':
								actions += '<li><a href="'+ ((editUrl !== false) ? editUrl : Arena.uploadDialogUrl) +'" role="menuitem" '
									+'class="lo-upload p-hov	er" data-lo-type="'+node.data.type+'" data-id-object="'+idOrg+'">'
									+'<span class="objlist-sprite i-sprite is-edit"></span>'
									+'<span class="menu-label"> '+Yii.t('standard', '_MOD')+'</span></a></li>';
								break;
							case 'tincan':
								actions += '<li><a href="'+ ((editUrl !== false) ? editUrl : Arena.uploadDialogUrl) +'" role="menuitem" '
									+'class="lo-upload p-hover" data-lo-type="'+node.data.type+'" data-id-object="'+idOrg+'">'
									+'<span class="objlist-sprite i-sprite is-edit"></span>'
									+'<span class="menu-label"> '+Yii.t('standard', '_MOD')+'</span></a></li>';
								break;
								
							case 'elucidat':
								var axEditUrl = (editUrl !== false) ? editUrl : Docebo.lmsAbsoluteBaseUrl + '/index.php?r=ElucidatApp/ElucidatApp/axEditElucidat&id_object=' + idOrg + '&course_id=' + Arena.course_id;
								actions += '<li><a href="' + axEditUrl + '" '
									+'role="menuitem" class="lo-edit p-hover" '
									+'data-lo-type="'+node.data.type+'" data-id-object="' + idOrg +'">'
									+'<span class="objlist-sprite i-sprite is-edit"></span>'
									+'<span class="menu-label"> '+Yii.t('standard', '_MOD')+'</span></a></li>';
								break;
								
							case 'authoring':
								if (parseInt(node.data.authoring_copy_of) <= 0) {
									var idResource = parseInt(node.data.idResource);
									var editHref = (editUrl !== false) ? editUrl : Docebo.rootAbsoluteBaseUrl + '/authoring/index.php?r=main/create&id=' + idResource + '&id_course=' + Arena.course_id + '&editSlideFile='+true;
									actions += '<li><a href="' + editHref + '" '
										+'role="menuitem" class="lo-edit p-hover" '
										+'data-lo-type="' + node.data.type + '" data-id-object="' + idOrg + '">'
										+'<span class="objlist-sprite i-sprite is-edit"></span>'
										+'<span class="menu-label"> '+Yii.t('standard', '_MOD')+'</span></a></li>';
								}
								break;
							case 'deliverable':
							default:
								var axEditUrl = (editUrl !== false) ? editUrl : Docebo.lmsAbsoluteBaseUrl + '/index.php?r=' + node.data.type + '/default/axEditLo&id_object=' + idOrg + '&course_id=' + Arena.course_id;
								actions += '<li><a href="' + axEditUrl + '" '
									+'role="menuitem" class="lo-edit p-hover" '
									+'data-lo-type="'+node.data.type+'" data-id-object="' + idOrg +'">'
									+'<span class="objlist-sprite i-sprite is-edit"></span>'
									+'<span class="menu-label"> '+Yii.t('standard', '_MOD')+'</span></a></li>';
								break;
						} // end switch
					}
				}

				// Central LO push available for non placeholder objects
				if(Arena.openInCentralRepositoryUrl != '' && (node.data.type !== 'deliverable') && !node.data.isFolder && (typeof node.data.id_object == "undefined" || node.data.id_object == null)) {
					var pushLoUrl = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=centralrepo/centralRepo/pushFromLocalCourse&id_org=' + idOrg;
					var pushLoLabel = Yii.t('standard', 'Push object to Central Repository');
					actions += '<li><a href="' + pushLoUrl + '" rel="push-lo-modal-' + idOrg + '" class="open-dialog p-hover">' +
						'<span style="margin-right: 7px;"><i class="fa fa-hdd-o"></i></span>' +
						'<span class="menu-label"> '+pushLoLabel+'</span></a></li>';
				}

				// Delete Lo item (real LO or a Folder): launches a Dialog2.
				var deleteLoUrl = Arena.deleteLoUrl + '&id_object=' + idOrg + '&course_id=' + Arena.course_id;
				var deleteLabel = (typeof node.data.id_object !== "undefined" && node.data.id_object !== null) ? Yii.t('standard', 'Unlink') : Yii.t('standard', '_DEL');
				actions += '<li><a href="' + deleteLoUrl +
							'" rel="delete-single-lo-modal-' + idOrg +
							'" role="menuitem" class="open-dialog p-hover menuitem-delete-lo">' +
							'<span class="i-sprite is-remove red"></span>' +
							'<span class="menu-label"> '+deleteLabel+'</span></a></li>';


				// now we can display the menu of the object
				postfix.append( $('<div class="btn-group">' +
					'<a class="btn dropdown-toggle p-hover" data-toggle="dropdown" href="#"><i class="p-sprite ico-menu"></i><span class="caret"></span></a>' +
					'<ul class="dropdown-menu pull-right player-lo-actions" role="menu" aria-labelledby="dropdownMenu">' +
						actions +
					'</ul>' +
					'</div>') );

                // some plugins need to disable the reorder icon and the dropdown
                $('body').trigger('onDynatreeNodeCreate', [ $nodeSpan ]);

			},

			onRender: function (node, nodeSpan) {
				var versionName = node.data.versionName;
				var idObject = parseInt(node.data.id_object);			
				var keepUpdated = node.data.keep_updated;
				var versionData = '';
				var checkForTooltips = false;

				$(nodeSpan).children('.dynatree-title').text(node.data.title_shortened);

				var nodeTitleElement = $(nodeSpan).find('.player-postfix');

				//handle central repository info
				if(typeof versionName !== "undefined" && idObject > 0){
					if(!keepUpdated || keepUpdated != 0){
						versionData = Yii.t('standard', 'Always keep updated to the latest version');
					} else{
						var versionCreateDate = node.data.version_create_date;
						if(!node.data.showVersioning){
							versionData = versionCreateDate;
						} else{
							versionData = versionName + ' - ' + versionCreateDate;
						}
					}
					nodeTitleElement.prepend($('<a href="' + Arena.openInCentralRepositoryUrl + '" class="postfix-fa-icon"><span class="version-info"><i rel="tooltip" title="' + versionData + '" class="fa fa-hdd-o"></i></span></a>'));
					checkForTooltips = true;
					if(Arena.openInCentralRepositoryUrl){
						nodeTitleElement.find('.version-info').on('click', function(){
							window.location.href = Arena.openInCentralRepositoryUrl;
						});
					}
				}

				//handle C.S.P. info
				var csp = node.data.csp_info;
				if (csp) {
					var csp_icon = '<span class="csp-info postfix-fa-icon"><i rel="tooltip" title="' + csp.name + '" class="fa fa-shopping-cart"></i></span>';
					nodeTitleElement.prepend($(csp_icon));
					checkForTooltips = true;
				}

				//handle tooltips if needed
				if (checkForTooltips) {
					nodeTitleElement.find('i').tooltip();
				}
			},

			children: data

		});

		$('#player-arena-overall-objlist').dynatree("getTree").reload();

		$('#player-arena-overall-objlist').children('ul').sortable({
			appendTo: "body",
			handle: ".lo-drag",
			helper: "clone",
			zIndex: 9999,
			revert: true,
			connectWith: ".player-arena-tree-folder",
			placeholder: "player-arena-overall-placeholder",
			start: function (event, ui) {
				ui.placeholder.height(ui.item.siblings("li").height());
				$(ui.helper).addClass("player-drag-proxy");
				$(ui.helper).attr('data-drop-type', 'sort');
				$(ui.helper).css('position', 'absolute');
			},
			beforeStop: function (event, ui) {

				updateZIndex();
				if ($(ui.helper).attr('data-drop-type') === 'sort') {

					var elements = $(this).sortable("toArray").filter(function (v) {
						return v !== '';
					});
					var elementIndex = $(ui.item).index();
					var siblings = [ elementIndex > 0 ? elements[elementIndex-1] : null, elementIndex < elements.length-1 ? elements[elementIndex+1] : null ];

					me.options.onNodeMoved(ui, $(ui.item).attr('data-object-id'), elementIndex, siblings, "sort");
				}

			},
			received: function (event, ui) {
				$(ui.helper).attr('data-drop-type', 'sort');
			}

		}).disableSelection();

		// Dialog2 related: handle "open-dialog" markup
		$('#player-arena-overall-objlist').controls();
	},

	blockTrees: function () {
		$('#player-arena-overall-objlist').block({
			message: null,
			css: {
				border: '3px solid #a00'
			}
		});
		$('#player-arena-overall-folders').block({
			message: null,
			css: {
				border: '3px solid #a00'
			}
		});
	},

	unblockTrees: function () {
		$('#player-arena-overall-objlist').unblock();
		$('#player-arena-overall-folders').unblock();
	},

	expandAll: function () {
		$('#player-arena-overall-folders').dynatree("getRoot").visit(function (node) {
			node.expand(true);
		});
		$('#player-arena-overall-objlist').dynatree("getRoot").visit(function (node) {
			node.expand(true);
		});
		if (Arena.learningObjects.management.tree.length > 0 && Arena.learningObjects.management.tree[0].children !== undefined) {
			this.showFolder(Arena.learningObjects.management.tree[0].children);
		}

	},

	getActiveFolderKey: function (strict) {
		activeDynaTreeNode = $('#player-arena-overall-folders').dynatree('getActiveNode');
		if (!activeDynaTreeNode) {
			return 0; // root
		} else {
			//the 'strict' parameter will exclude fake scormorg folders
			if (!activeDynaTreeNode.data.isFolder || (strict && activeDynaTreeNode.data.type != '')) {
				return false;
			}
			return activeDynaTreeNode.data.key;
		}
	}
};

Arena.learningObjects.contentViewer = {
	tree: [],
	options: {
		onChapterSelected: function (chapterData) {
		}
	},

	init: function (options) {
		this.options = $.extend({}, this.options, options);
		var me = this;

		$('.player-lonav-tree').each(function(){
			$(this).dynatree({
				selectMode: 1,

				onCreate: function (node, nodeSpan) {
					var $nodeSpan = $(nodeSpan);
					var bgSpan = $('<span class="background-loitem"></span>');




					// EXPERIMENT XXXXXXXXXXXXX (Hoisting Single SCO object)
					/*
					 var hoistScoObject = false;
					 if (node.data.type == 'scormorg' && node.data.isFolder && node.hasChildren() && hoistScoObject) {

					 // Count SCO children
					 var scoCount = 0;
					 node.visit(function(subNode){
					 if (subNode.data.type == 'sco') {
					 scoCount++;
					 }
					 });

					 // Only ONE ???
					 if (scoCount == 1) {
					 // Visit the SCO and move it before the SCORMORG node, then just hide that node
					 node.visit(function(subNode){
					 if (subNode.data.type == 'sco') {
					 subNode.move(node, 'before');
					 // Also change the rendered (!! we are in onRender method) title
					 $(subNode.span).find('.dynatree-title').html(node.data.title);

					 }
					 // Hide everything else that is a child of a SINGLE SCO SCORMORG!! (could be a folder, for example)
					 else {
					 $(subNode.span).hide();
					 }
					 });
					 // Hide the SCORM ORG node at the end.
					 $nodeSpan.hide();
					 $nodeSpan.remove();
					 }
					 }
					 */
					// EXPERIMENT XXXXXXXXXXXXX (Hoisting Single SCO object)




					if (node.data.isFolder || (node.data.type == 'sco' && node.hasChildren() && !node.data.idResource)) {

						bgSpan.addClass('folder');

						// Status bar on the leftmost side
						var status = me.calculateFolderStatus(node);
						var statusBlock = $('<span class="player-lonav-lostate '+ (status ? status : 'none') +'"></span>');

						//var statusBlock = $('<span class="player-lonav-lostate '+(node.data.status ? node.data.status : 'none')+'"></span>');
						$nodeSpan.before(statusBlock);
					}
					$nodeSpan.before(bgSpan);

					$nodeSpan.siblings().addBack().hover(function () {
						bgSpan.addClass('hover');
						$(this).addClass('hover');
						return false;
					}, function () {
						bgSpan.removeClass('hover');
						$(this).removeClass('hover');
						return false;
					});

					if (node.data.locked) {
					}


				},

				onRender: function (node, nodeSpan) {
					var $nodeSpan = $(nodeSpan);
					var cssClass = '';









					if (!node.data.isFolder && !(node.data.type == 'sco' && node.hasChildren() && !node.data.idResource)) {

						// Always try to expand AICC AU items, they MAY have some children
						// AICC AUs (assignable units) are Playable && Can have children (go figure)
						if ( (node.data.type == 'aicc') && ((node.data.aiccItemType == 'au') || (node.data.aiccItemType == 'block')) ) {
							// @TODO Decide what to do with AU's : expand or not ? (they CAN have children)
							//node.expand();
						}
						
						if (node.data.locked) {
							cssClass = "grey is-lock";
						} else {
							switch (node.data.status) {
								case 'completed':
							case 'passed':
									cssClass = 'is-circle-check green';
									break;
								case 'attempted':
									cssClass = 'is-play orange';
									break;
								case 'failed':
									cssClass = 'is-play red';
									break;
								default:
									cssClass = "grey is-play";
									break;
							}
						}

					} else {
						var opened = ($nodeSpan.hasClass('dynatree-expanded'));
						if (node.data.locked) {
							cssClass = opened ? "is-lock grey opened" : "is-lock grey closed ";
						}
						else {
							cssClass = opened ? "is-folder opened" : "is-folder closed ";
						}
						if (node.hasChildren()) {
							cssClass += ' has-children';
						}
					}


					//
					if (node.data.locked) {
						$nodeSpan.addClass("node-locked");
					}


					if (cssClass === '') {
						$nodeSpan.find('span.dynatree-icon').css("visibility", "hidden");
					}

					if (node.data.isFolder) {
						if (node.data.locked) {
							$nodeSpan.find('span.dynatree-icon').addClass('i-sprite '+cssClass).removeClass('dynatree-icon');
						}
						else {
							// AICC "Objectives" are very special.
							// Bascially Objectives CAN be folders and are treated as "folders" BY DEFAULT in the sense of LO objects tree
							// However, they can have "status" (their own status as part of AICC lessons, NOT reflected by their children)
							// So, in case the objective is NOT a REAL FOLDER (no chilren), we treat it as an item with its status.
							// For such Objectives, we use the "check" icon (fa-check)
							if ((node.data.aiccItemType === 'objective') && (!node.hasChildren())) {
								var objectiveCssClass;
								switch (node.data.status) {
									case 'completed':
									case 'passed':
										objectiveCssClass = 'fg-color-green';
										break;
									case 'attempted':
										objectiveCssClass = 'fg-color-orange';
										break;
									case 'failed':
										objectiveCssClass = 'fg-color-red';
										break;
									default:
										objectiveCssClass = "fg-color-grey";
										break;
								}
								$nodeSpan.find('span.dynatree-icon').addClass('fa fa-check ' + cssClass + ' ' + objectiveCssClass).removeClass('dynatree-icon');
								$nodeSpan.addClass('aicc-objective');
								$nodeSpan.removeClass('dynatree-folder');
							}
							else {
								$nodeSpan.find('span.dynatree-icon').addClass('p-sprite ' + cssClass).removeClass('dynatree-icon');
							}
						}
					}
					else {
						$nodeSpan.find('span.dynatree-icon').addClass('i-sprite '+cssClass).removeClass('dynatree-icon');
					}
					if (node.data.sort == '1') { // Root node
						$nodeSpan.find('.is-folder').addClass('is-root');
						$nodeSpan.children('.dynatree-title').text(Arena.courseInfo.name).addClass('is-root');
					}

				},

				onClick: function (node, event) {
					if (!node.data.locked)
						me.options.onChapterSelected(node.data);
				},

				onActivate: function (node) {
				},

				onQueryExpand: function(flag,node) {
					if (node.data.locked) return false;
					return true;
				},


				children: Arena.learningObjects.contentViewer.tree
			});
		});
	},
	/**
	 * Count completed/attempted LOs inside a branch and return folder-level 'status'
	 */
	calculateFolderStatus: function(node) {
		var countCompleted = 0;
		var countAttempted = 0;
		var countTotal = 0;
		var status = null;

		// We are interested in folders only
		if (!node.data.isFolder)
			return null;

		// Traverse all nodes in this branch
		node.visit(function (node) {
			if (!node.data.isFolder) {
				countTotal++;
				if (node.data.status == 'completed' || node.data.status == 'passed') countCompleted++;
				if (node.data.status == 'attempted') countAttempted++;
			}
		});

		// No LO inside?
		if (countTotal <= 0)
			return null;

		// Check....

		// ALL are COMPLETED
		if (countCompleted == countTotal) {
			status = 'completed';
		}
		// We have some Attempted OR we have SOME completed BUT NOT ALL
		else if ( (countAttempted > 0) || ((countCompleted > 0) && (countCompleted < countTotal)) ) {
			status = 'attempted';
		}
		// No status
		else {
			status = null;
		}

		return status;


	},
	setTree: function (tree) {
		this.tree = tree;
		this.reloadTree();
	},
	reloadTree: function () {
		$('.player-lonav-tree').each(function(){
			$(this).dynatree({
				children: Arena.learningObjects.contentViewer.tree
			});
			$(this).dynatree("getTree").reload();
		});
		this.expandRootOnly();

	},
	expandAll: function () {
		$('.player-lonav-tree').dynatree("getRoot").visit(function (node) {
			node.expand(true);
		});
	},
	expandRootOnly: function () {
		$('.player-lonav-tree').each(function(){
			$(this).dynatree("getRoot").visit(function (node) {
				node.expand(true);
				return false;
			});
		});
	},
	updateNodeProperties: function (node, properties) {
		if (properties.status) {
			node.data.status = properties.status;
			var el = $('#lostatus-indicator-'+node.data.key);
			el.removeClass();
			el.addClass('player-lonav-lostate '+properties.status);
			//to do: update ancestors status
		}
	}
};


// Used as callback on each update of LO list in either
// Learner or Training Material view
Arena.ajaxUpdateSuccessCallback = function (data) {

	if (data.play) {

		if(Arena.playerLayout==='listview'){ // LearningCourse::$PLAYER_LAYOUT_LISTVIEW
			// Load player layout with LOs
			// listed vertically as a list,
			// similar to how 5.4 worked

			if (Arena.resumeAutoplay && Arena.firstLoToPlay && !Arena.userCanAdminCourse)
			{
				if (!Arena.triggeredLaunchpadUpdatedFromListView)
				{
					$.ajax({
						url: Docebo.lmsAbsoluteBaseUrl + '/index.php?r=//player/training/axTrackCourseAccess',
						type: 'post',
						dataType: 'json',
						data: {
							course_id: Arena.course_id,
							lo_data: Arena.firstLoToPlay
						},
						beforeSend: function () {
						}
					}).done(function (res) {
						$('#player-arena-launchpad').trigger('launchpadUpdated');
					});
                }
            }
			else
			{
				$('#player-arena-launchpad').html('').hide();
			}
            
		} else if(Arena.playerLayout==='play_button' || Arena.playerLayout==='navigator' || Arena.playerLayout==='navigator'){ // LearningCourse::$PLAYER_LAYOUT_PLAY_BUTTON
			// Load regular player layout with the
			// big image and the triangle Play button
			Arena.firstLoToPlay = data.firstToPlay || data.firstToPlayButton;;
			if (!(Arena.firstLoToPlay == false) && $('.player-lonav-tree').length > 0) {
				$('.player-lonav-tree').dynatree("getTree").activateKey(Arena.firstLoToPlay.key);
			}
			// Always load the common launchpad. It will analyze and detect IF nothing is to be played next
			Launcher.commonLaunchpad.load(Arena.firstLoToPlay);

			Arena.reloadQuickNavs();

			switch(Arena.playerLayout){
				case 'navigator':
				case 'listview':
					if((Arena.firstLoToPlay!==false) && (typeof Arena.firstLoToPlay !== "undefined")) {
						PlayerNavigator.setCurrentKey(Arena.firstLoToPlay.key);
					}else {
						PlayerNavigator.setEnded(true);
					}
					PlayerNavigator.reload();
					break;
			}
		}
	}
};

// Callback that is only fired on initial page load
Arena.ajaxUpdateSuccessCallbackOneTime = function(data){
	switch(Arena.playerLayout){
		case 'navigator':
		case 'listview':
			PlayerNavigator.init({ajaxUrl: Docebo.lmsAbsoluteBaseUrl + '/index.php?r=player/training/axGetPlayerNavigator&course_id=' + Arena.course_id});
			break;
	}

	if(Arena.currentView==='play'){
        // For uknown reason we need a duplicate of the this functionality in the ajaxUpdateSuccessCalback function that will activate the Autoplay for listview
		if(Arena.resumeAutoplay && Arena.firstLoToPlay && !Arena.triggeredLaunchpadUpdatedFromListView){
			Docebo.log('Resume autoplay enabled. Waiting for launchpad to load to play the first available LO...');
			$(document).on('launchpadUpdated', '#player-arena-launchpad', {}, function() {
				//check if we have already started an external page for current LO
				var playable = true, lpp_el = $('#last_popup_played-input');
				if (lpp_el.length > 0) {
					playable = (Arena.firstLoToPlay.key != lpp_el.val());
				}
				if(typeof Arena.firstLoToPlay.key === 'undefined')
					playable = false;
				//do stuff
				if (playable) {
				Docebo.log('Resuming playback of: '+Arena.firstLoToPlay.title+' ['+Arena.firstLoToPlay.key+']');
				Launcher.dispatch(Arena.firstLoToPlay);
				}
			});
		}
	}

	Arena.ajaxUpdateSuccessCallback(data);
}



// Helper function
jQuery.fn.reverse = function () {
	return this.pushStack(this.get().reverse(), arguments);
};


/** DOM READY * */
$(function () {

	// Initialize LO List
	Arena.learningObjects.init({

		contentViewer: {
			onChapterSelected: function (loData) {
				var success = Launcher.dispatch(loData);
			}
		},

		management: {
			onNodeMoved: function (ui, item, dropPosition, siblings, type) {
				switch (type) {
					case 'folder':
						Arena.learningObjects.management.blockTrees();
						var params = {
							'course_id': Arena.course_id,
							'id_object': item,
							'id_folder': dropPosition
						};
						$.ajax({
							url: Arena.moveInFolderLearningObjectUrl,
							type: 'post',
							dataType: 'JSON',
							data: params,
							beforeSend: function () {

							}
						}).done(function (res) {
								if (res.success) {
									if (res.data) {
										if (res.data.play) {
											Arena.learningObjects.contentViewer.setTree(res.data.play);
										}
										if (res.data.management) {
											Arena.learningObjects.management.setTree(res.data.management);
										}

										// Update the black ribbons (training material and learer view)
										Arena.reloadQuickNavs();
									}
								} else {
									$('#player-arena-overall-objlist').children('ul').sortable("cancel");
								}
							}).always(function () {
								Arena.learningObjects.management.unblockTrees();
							}).fail(function (jqXHR, textStatus, errorThrown) {
								Arena.learningObjects.management.unblockTrees();
							});
						break;
					case 'sort':
						// Here the ajax call
						Arena.learningObjects.management.blockTrees();
						var params = {
							'course_id': Arena.course_id,
							'id_object': item,
							'new_position': dropPosition
						};
						if (siblings[0]) {
							params.before = siblings[0];
						}
						if (siblings[1]) {
							params.after = siblings[1];
						}

						$.ajax({
							url: Arena.sortLearningObjectUrl,
							type: 'post',
							dataType: 'JSON',
							data: params,
							beforeSend: function () {

							}
						}).done(function (res) {
								if (res.success) {
									if (res.data) {
										if (res.data.play) {
											Arena.learningObjects.contentViewer.setTree(res.data.play);
										}
										if (res.data.management) {
											Arena.learningObjects.management.setTree(res.data.management);
										}

										// Update the black ribbons (training material and learer view)
										Arena.reloadQuickNavs();
									}
								} else {
									$('#player-arena-overall-objlist').children('ul').sortable("cancel");
								}
							}).always(function () {
								Arena.learningObjects.management.unblockTrees();
							}).fail(function (jqXHR, textStatus, errorThrown) {
								Arena.learningObjects.management.unblockTrees();
							});
				}

			}

		}
	});

	// Load Learning Objects Tree into Dynatrees
	Arena.learningObjects.ajaxUpdate(null, Arena.ajaxUpdateSuccessCallbackOneTime);

	// Set initial arena mode (edit/view)
	if (Arena.onLoadArenaMode == 'edit_course' && !Arena.userCanAdminCourse) {
		Arena.onLoadArenaMode = 'view_course';
	}
	if (Arena.onLoadArenaMode == 'edit_course') {
		Arena.setEditCourseMode();
	} else {
		Arena.setViewCourseMode();
	}
	Arena.hideLoNavigation();

	// **************** LISTENERS

	$(document).controls();
	Arena.hoverAndHintListeners();

	//redirect to Learnign Plan page on course complete
	$(document).on("click", '#fancybox-docebo-close, .fancybox-docebo-close, #inline-player-close, #player-inline-close', function () {
		$.ajax({
			url: Arena.checkStatusUrl,
			type: 'get',
			dataType: 'JSON'
		}).done(function (res) {
			if (res.success) {
				if (res.data) {
					var courseStatus = res.data.courseStatus;
					if (courseStatus == 2 && Arena.courseInfo.status != courseStatus && typeof Arena.redirectUrl != 'undefined' && Arena.redirectUrl != '') {
						window.top.location = Arena.redirectUrl;
					}else{
						window.location.reload();
					}
				}
			}
		})
	});

	// Listener for clicks on EDIT LO Dropdown menu item
	$('#player-lo-manage-panel').on('click', '.lo-edit', function (e) {
		e.preventDefault();

		var lo_type = $(this).data('lo-type');

		if (lo_type == 'test' || lo_type == 'poll' || lo_type == 'authoring') {
			window.location.href = $(this).attr('href');
		}
		else if (lo_type == 'file' || lo_type == 'item') {
			$('<div/>').dialog2({
				content: $(this).attr('href'),
				id: 'edit-'+lo_type+'-modal'
			});

			// Catch the auto-close.success from Create Folder Dialog2 and reload Dynatree
			$(document).on("dialog2.content-update", '#edit-'+lo_type+'-modal', function () {
				if ($(this).find("a.auto-close.success").length > 0) {
					Arena.learningObjects.ajaxUpdate();
				}
			});
		}
		else if (lo_type == 'htmlpage') {
			Arena.htmledit.show($(this).data('id-object'));
		}
		else if (lo_type == 'deliverable') {
			Arena.deliverable.show($(this).data('id-object'));
		}
		else if (lo_type == 'video') {
			Arena.uploader.show($(this).data('id-object'));
		}
		else if (lo_type == 'lti') {
			Arena.lti.show($(this).data('id-object'));
		}
		else if (lo_type == 'elucidat') {
			Arena.elucidat.show($(this).data('id-object'), 2);
		}
		else if (lo_type == 'googledrive'){
			Arena.googledrive.show($(this).data('id-object'));
		}
		
		else {
			Docebo.Feedback.show('error', 'Coming soon...');
		}
	});


	// ON Clicking EDIT/VIEW Course button for Admins, in Arena (radios)
	var $playerModeSelectorButtons = $("#player-manage-mode-selector label");
	$playerModeSelectorButtons.on('click', function (e) {
		var targetMode = $('#player-manage-mode-selector input[name=target-mode]:checked').val();
		var label = $(e.currentTarget);

		if (label.attr('for') == 'target-mode-view') targetMode = 'view_course';
		else targetMode = 'edit_course';

		// Toggle target mode for next click(s)
		switch (targetMode) {
			case 'edit_course':
				$('#target-mode-view').prop('checked', false);
				$('#target-mode-edit').prop('checked', true);
				$('#equivalenceCourses').hide();
				Arena.setEditCourseMode();
				break;
			case 'view_course':
			default:
				$('#target-mode-edit').prop('checked', false);
				$('#target-mode-view').prop('checked', true);
				$('#equivalenceCourses').show();
				Arena.setViewCourseMode();
				break;
		}
	});
	// manage click on central switch
	var $playerModeSelectorButtons = $("#player-manage-mode-selector-switcher");
	$playerModeSelectorButtons.on('click', function (e) {
		var targetMode;
		e.preventDefault();
		var currentMode = $('#player-manage-mode-selector input[name=target-mode]:checked').val();
		if (currentMode == 'view_course') targetMode = 'edit_course';
		else targetMode = 'view_course';
		// in this case the central switch was clicked we have to invert the current selection
		// by checking the other radio and switching

		switch (targetMode) {
			case 'edit_course':
				$('#target-mode-view').prop('checked', false);
				$('#target-mode-edit').prop('checked', true);
				Arena.setEditCourseMode();
				break;
			case 'view_course':
			default:
				$('#target-mode-edit').prop('checked', false);
				$('#target-mode-view').prop('checked', true);
				Arena.setViewCourseMode();
				break;
		}
	});


	/**
	 * Listen for click on ".lo-evaluate" LO dropdown menu items (Deliverable, Test,...)
	 * and redirects to the HREF of the target
	 */
	$('#player-lo-manage-panel').on('click', '.lo-evaluate', function(e){
		e.preventDefault(); 
		var url=$(this).attr('href');
		window.location.href = url;
	});
	
	
	// Toggle Play List navigation
	$("#player-lonav-button").click(function (e) {
		//e.preventDefault();
		//Arena.toggleLoNavigation();
	});

	// Re-load LO Trees on success-closing of some Dialog2s
	$(document).on("dialog2.content-update", '[id^="modal-edit-prerequisites-"], [id^="modal-configure-lo-"]', function () {
		if ($(this).find("a.auto-close.success").length > 0) {
			Arena.learningObjects.ajaxUpdate();
		}
	});

	$('#player-manage-add-lo-button').on('click', '.lo-authoring', function(e){
		e.preventDefault();
		var url = Docebo.rootAbsoluteBaseUrl + '/authoring/index.php?r=main/create'
		+'&id_course=' + Arena.course_id
		+'&id_parent=' + Arena.learningObjects.management.getActiveFolderKey(true);
		window.location.href = url;
	});


	// Catch the auto-close.success from Delete LO Dialog2 and reload Dynatree
	$(document).on("dialog2.content-update", '[id^="delete-single-lo-modal-"]', function () {
		if ($(this).find("a.auto-close.success").length > 0) {
			Arena.learningObjects.ajaxUpdate();
		}
	});

	// Catch the auto-close.success from Push LO Dialog2 and reload Dynatree
	$(document).on("dialog2.content-update", '[id^="push-lo-modal-"]', function () {
		if ($(this).find("a.auto-close.success").length > 0) {
			Arena.learningObjects.ajaxUpdate();
		}
	});


	$(document).on('lo-upload-complete', function () {
		Launcher.commonLaunchpad.load(Arena.firstLoToPlay, false);
	});

	// close listener for launchpad
	$(document).on('click', '#player-inline-close', function (e) {
		e.preventDefault();
		if(Arena.playerLayout==='listview'){
			$('.player-navigator').hide();
		}
        $(window).off('hashchange');
        History.back();
		switch(Arena.playerLayout){
			case 'listview':
				Launcher.resetToListView();
				break;
			case 'navigator':
				Launcher.showCommonLaunchpadFirstToPlay();
				if(!(Arena.firstLoToPlay===false)){
					PlayerNavigator.setState('initial');
					PlayerNavigator.setCurrentKey(Arena.firstLoToPlay.key);
					PlayerNavigator.reload();
				}else{
					PlayerNavigator.setEnded(true);
					PlayerNavigator.reload();
				}
				break;
			default:
				Launcher.showCommonLaunchpadFirstToPlay();
		}

	});

	
	
	/**
	 * Start video Conversion/Transcoding status checker.
	 * 
	 * Note: setTimeout and recursion inside!
	 * 
	 * This is improved version of the previos VideoStatus checker in that it now makes ONE ajax call for ALL LOs.
	 * 
	 */
	(function videoStatusChecker(){
		var maxCounter = 60;
		var url = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=player/training/AxCheckVideoStatuses';
		var sumCounter = 0;
		var interval = 7000; // Initial timeout for AJAX calls
		
		// Collect idOrgs to check
		var ids = [];
		// !Arena.videoLoNodesToCheck is GLOBAL
		Arena.videoLoNodesToCheck.forEach(function(item){
			// Check only items having counter less than maxCounter and not yet "resolved"
			if (item.counter < maxCounter && !item.resolved) {
				ids.push(item.key);
				// Accumulate sum of all counters
				sumCounter = sumCounter + item.counter;	
			}
		});
		
		// If there is something to check, make call, get statuses, etc.
		if (ids.length > 0) {
			
			// Apply addaptive timeout: when average counter reaches half of the max counter, double the timeout 
			var avgCounter = sumCounter / ids.length;  // float is ok
			if (avgCounter > (maxCounter/2)) {  // float is ok
				interval = Math.round( interval * 2 );
			}
			
			data = {
				idOrgs: ids,
				course_id: Arena.course_id
			};
			$.ajax({
				url: url,
				type: "post",
				dataType: 'json',
				data : data
			})
			.done(function(res){
				if (res.success) {
					// Got statuses?
					if (res.data.statuses.length > 0) {
						
						res.data.statuses.forEach(function(item){
							// Increase counter for this idOrg
							Arena.videoLoNodesToCheck[item.idOrg].counter++;
							
							// Analyze the result of the check
							if ((item.status == 'in_progress') && (Arena.videoLoNodesToCheck[item.idOrg].counter >= maxCounter)) {
								Arena.videoLoNodesToCheck[item.idOrg].is_video_transcoding_error=1;
								Arena.videoLoNodesToCheck[item.idOrg].resolved = 1;
								if (typeof Arena.videoLoNodesToCheck[item.idOrg].nodeSpan !== "undefined")
									$(Arena.videoLoNodesToCheck[item.idOrg].nodeSpan).find('.player-postfix .video-conversion-in-progress').replaceWith(Arena.videoConversionErrorHtml);
							}
							else if (item.status == 'converted') {
								Arena.videoLoNodesToCheck[item.idOrg].is_video_transcoding_error=0;
								Arena.videoLoNodesToCheck[item.idOrg].resolved = 1;
								if (typeof Arena.videoLoNodesToCheck[item.idOrg].nodeSpan !== "undefined")
									$(Arena.videoLoNodesToCheck[item.idOrg].nodeSpan).find('.player-postfix .video-conversion-in-progress').replaceWith(Arena.videoLoGreenHtml);
							}
							else if (item.status == 'error') {
								Arena.videoLoNodesToCheck[item.idOrg].is_video_transcoding_error=1;
								Arena.videoLoNodesToCheck[item.idOrg].resolved = 1;
								if (typeof Arena.videoLoNodesToCheck[item.idOrg].nodeSpan !== "undefined")
									$(Arena.videoLoNodesToCheck[item.idOrg].nodeSpan).find('.player-postfix .video-conversion-in-progress').replaceWith(Arena.videoConversionErrorHtml);
							}
							
							
							if (Arena.videoLoNodesToCheck[item.idOrg].resolved) {
								Arena.videoLoNodesToCheck[item.idOrg].is_video_transcoding=0;
								Arena.videoLoNodesToCheck[item.idOrg].visible = item.visible;  
							}
							
						});
					}
				}
			});
		}
		
		// Keep calling this every N miliseconds, 
		setTimeout(function() {videoStatusChecker();},interval);
		
	})();

	
});
