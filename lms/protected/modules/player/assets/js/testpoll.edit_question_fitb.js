var AnswersManager = {

	focusedElement: false,

	init: function(options) {

		var numbersPattern = options.numberPattern || /\d+\.?\d*/;
		var answerHtmlEditor = (options.answerHtmlEditor || false);
		var singleCorrectAnswer = (options.singleCorrectAnswer || false);
		var modality = options.modality;

		switch (modality.toLowerCase()) {
			case 'full': modality = 'full'; break;
			default: { modality = 'simple'; } break;
		}

		var setAllIncorrect = function() {
			$('#answers-tbody > tr').each(function() {
				$('td:eq(1)', this).html(getCorrectnessText(false));
				var index = $(this).attr('id').replace('answers-table-input-row-', '');
				$('#answers-'+index+'-correct').val('0');
			});
		};

		var generalCounter = 0;

		var getCorrectnessText = function(isCorrect) {
			return '<span class="'+(isCorrect ? 'answer-correct' : 'answer-incorrect')+'">'
				+(isCorrect ? Yii.t('test', '_TEST_CORRECT') : Yii.t('test', '_TEST_INCORRECT'))
				+'</span>';
		};

		var addTableRow = function(values) {
			addAnswersHeader();

			var inputCounter = values.sequence;
			var preName = 'answers['+inputCounter+']',
				preId = 'answer-'+inputCounter+'-',
				col1 = '[answ ' + inputCounter +']';

			var inputs = '<input type="hidden" name="'+preName+'[answers]" id="'+preId+'answers" />'
				+ '<input type="hidden" name="'+preName+'[sequence]" id="'+preId+'sequence" />'
				+ '<input type="hidden" name="'+preName+'[case_sensitive]" id="'+preId+'case_sensitive" />'
				+ '<input type="hidden" name="'+preName+'[score_correct]" id="'+preId+'score_correct" />'
				+ '<input type="hidden" name="'+preName+'[score_incorrect]" id="'+preId+'score_incorrect" />'
				+ '<input type="hidden" name="'+preName+'[comment]" id="'+preId+'comment" />';
			if (values.id_answer) {
				inputs += '<input type="hidden" name="'+preName+'[id_answer]" id="'+preId+'id_answer" />';
			}

			var html = '<tr id="answers-table-input-row-'+inputCounter+'">';
			html += '<td colspan="1" class="rowIndex" ><p>' + col1 + '</p></td>';
			html += '<td colspan="1" >' + values.answers.length + " " + Yii.t('test', 'choices') +'</td>';
			var pre1, value1, pre2, value2;
			if (values.scoreCorrect != 0) {
				pre1 = '+&nbsp;';
				value1 = parseFloat(values.scoreCorrect);
			} else {
				pre1 = '';
				value1 = '0';
			}
			if (values.scoreIncorrect != 0) {
				pre2 = '-&nbsp;';
				value2 = parseFloat(values.scoreIncorrect);
			} else {
				pre2 = '';
				value2 = '0';
			}
			html += '<td colspan="1" >'+pre1+value1+'</td>' + '<td colspan="1" >'+pre2+value2+'</td>';
			// Add here the hidden inputs
			html += '<td colspan="2" >&nbsp;&nbsp;&nbsp; ' + inputs + '</td>';
			// Add the table options on the right side
			html += '<td class="img-cell">'+'<a href="#" id="copy-answer-'+inputCounter+'"><span class="objlist-sprite p-sprite make-copy"></span></a>'+'</td>'
				+ '<td class="img-cell">'+'<a href="#" id="update-answer-'+inputCounter+'"><span class="objlist-sprite p-sprite edit-black"></span></a>'+'</td>'
				+'<td class="img-cell">'+'<a href="#" id="delete-answer-'+inputCounter+'"><span class="objlist-sprite p-sprite cross-small-red"></span></a>'+'</td>'
				+'</tr>';

			// Let's add the complete row to the table
			$('#answers-tbody').append(html);

			// We added the hidden input's let's add them the appropriate values
			$('#'+preId+'answers').val(values.answers);
			$('#'+preId+'sequence').val(inputCounter);
			$('#'+preId+'case_sensitive').val(values.case_sensitive == '1' ? '1' : '0');
			$('#'+preId+'score_correct').val(values.scoreCorrect);
			$('#'+preId+'score_incorrect').val(values.scoreIncorrect);
			$('#'+preId+'comment').val(values.comment);
			if (values.id_answer) { $('#'+preId+'id_answer').val(values.id_answer); }

			// Attache the COPY button functionality
			$('#copy-answer-'+inputCounter).on("click", function(e) {
				e.preventDefault();
				var index = $(this).attr('id').replace('copy-answer-', ''),
				pre = 'answer-'+index+'-';
				var values = {
					sequence		: parseInt(generalCounter) + 1,
					answers			: $('#'+pre+'answers').val().split(","),
					case_sensitive 	: ($('#'+pre+'case_sensitive').val() > 0),
					scoreCorrect 	: $('#'+pre+'score_correct').val(),
					scoreIncorrect 	: $('#'+pre+'score_incorrect').val(),
					comment 		: $('#'+pre+'comment').val()
				};
				addTableRow(values);
				generalCounter++;
			});

			// Attache the UPDATE button functionality
			$('#update-answer-'+inputCounter).on("click", function(e) {
				e.preventDefault();
				var index = $(this).attr('id').replace('update-answer-', ''),
					pre = 'answer-'+index+'-';

				var values = {
					answer			: $('#'+pre+'answers').val(),
					case_sensitive 	: ($('#'+pre+'case_sensitive').val() > 0),
					scoreCorrect 	: $('#'+pre+'score_correct').val(),
					scoreIncorrect 	: $('#'+pre+'score_incorrect').val(),
					comment 		: $('#'+pre+'comment').val()
				};

				activateEditMode(index, values);

				var top = $('#form-box-container').offset().top;
				$('body').animate({scrollTop: top}, 'slow');
			});

			// Attache the DELETE button functionality
			$('#delete-answer-'+inputCounter).on("click", function(e) {
				e.preventDefault();
				var index = $(this).attr('id').replace('delete-answer-', '');

				bootbox.dialog(Yii.t('standard', '_AREYOUSURE'), [{
						label: Yii.t('standard', '_CONFIRM'),
						'class': 'btn-docebo big green',
						callback: function() { deleteTableRow(index); }
					}, {
						label: Yii.t('standard', '_CANCEL'),
						'class': 'btn-docebo big black',
						callback: function() { }
					}],
					{ header: Yii.t('standard', '_DEL') }
				);
			});

			setAnswersVisibility();
		};

		var deleteTableRow = function(index) {
			$('#answers-table-input-row-'+index).remove();
			// When deleting row delete its code from the Question title
			var curentContent = $('#question-text').tinymce().getContent();
			var newContent = curentContent.split("[answ " + index + "]").join("");
			$('#question-text').tinymce().setContent(newContent);

			setAnswersVisibility();
		};

		var setAnswersVisibility = function() {
			if (getNumAnswers() > 0) {
				$('#no-answers-placeholder').css('display', 'none');
				$('#answers-table-container').css('display', 'block');
			} else {
				$('table#answers-table>thead').remove();
				$('#no-answers-placeholder').css('display', 'block');
				$('#answers-table-container').css('display', 'none');
			}
		};

		var clearForm = function() {
			$("select#new_correct_answers").trigger("removeAllItems");
			$('input.maininput').val('');
			// Uncheck case sensitive checkbox
			$('input.case_sensitive').attr('checked', false);
			$('span.case_sensitive').removeClass("checked");

			$('#new-answer-score_correct').val('0');
			$('#new-answer-score_incorrect').val('0');
			$('#new-answer-comment').val('');
		};

		var isEditing = false, wasMaskHidden = true, selectedRow = false;

		var selectEditRow = function(index) {
			var tr = $('#answer-'+index+'-answers').parent().parent();
			if (tr) {
				selectedRow = tr;
				tr.addClass('selected-table-row');
			}
		};

		var unselectEditRow = function() {
			if (selectedRow) { selectedRow.removeClass('selected-table-row'); }
			selectedRow = false;
		};

		var activateEditMode = function(index, values) {
			var answers = values.answer.split(',');
			$("select#new_correct_answers").trigger("removeAllItems");
			for (var i = 0; i < answers.length; i++){
				$("select#new_correct_answers").trigger("addItem", [{"title": answers[i], "value": answers[i]}]);
			}

			if(values.case_sensitive){
				$('input.case_sensitive').attr('checked', true);
				$('span.case_sensitive').addClass("checked");
			}else{
				$('input.case_sensitive').attr('checked', false);
				$('span.case_sensitive').removeClass("checked");
			}

			$('#new-answer-score_correct').val(values.scoreCorrect);
			$('#new-answer-score_incorrect').val(values.scoreIncorrect);
			$('#new-answer-comment').val(values.comment);

			$('#add-answer-buttons').css('display', 'none');
			$('#mod-answer-buttons').css('display', 'block');
			isEditing = index;

			$('#add-answer-block-header').css('display', 'none');
			$('#edit-answer-block-header').css('display', 'block');
			//wasMaskHidden = AddMask.hidden();
			AddMask.show();

			unselectEditRow();
			selectEditRow(index);
		};

		var deactivateEditMode = function() {
			$('#add-answer-buttons').css('display', 'block');
			$('#mod-answer-buttons').css('display', 'none');
			isEditing = false;
			clearForm();

			$('#add-answer-block-header').css('display', 'block');
			$('#edit-answer-block-header').css('display', 'none');
			AddMask.hide();//if (wasMaskHidden) { AddMask.hide(); } else { AddMask.show(); }
			unselectEditRow();
		};

		var validateInput = function(values) {
			var output = {valid: true, message: ""};
			if (!values.answers) {
				output.valid = false;
				output.message = Yii.t('test', '_EMPTY_ANSWER');
			} else {
				if (modality == 'full') {
					if (!numbersPattern.test(values.scoreCorrect)) { output.valid = false; output.message = Yii.t('standard', '_OPERATION_FAILURE'); }
					else if (!numbersPattern.test(values.scoreIncorrect)) { output.valid = false; output.message = Yii.t('standard', '_OPERATION_FAILURE'); }
				}
			}
			return output;
		};


		$('#add-answer-button').on("click", function(e) {
			var input = {
				sequence: parseInt(generalCounter) + 1,
				answers: $('#new_correct_answers').val(),
				case_sensitive: $('#answers_are_case_sensitive').is(':checked'),
				scoreCorrect: $('#new-answer-score_correct').val(),
				scoreIncorrect: $('#new-answer-score_incorrect').val(),
				comment: $('#new-answer-comment').val()
			}, result = validateInput(input);
			if (result.valid) {
				addTableRow(input);
				generalCounter++;
				clearForm();
			} else {
				/*
				bootbox.alert(result.message);
				*/
				bootbox.dialog(result.message, [{
						label: Yii.t('standard', '_CANCEL'),
						'class': 'btn-docebo big black',
						callback: function() { }
					}],
					{ header: Yii.t('standard', '_WARNING') }
				);
			}
		});

		$('#mod-answer-button').on("click", function(e) {
			if (!isEditing) return;
			var pre = 'new-answer-', $tr = $('#answers-table-input-row-'+isEditing);
			var input = {
				answers: $('#new_correct_answers').val(),
				case_sensitive: $('#answers_are_case_sensitive').is(':checked'),
				scoreCorrect: $('#new-answer-score_correct').val(),
				scoreIncorrect: $('#new-answer-score_incorrect').val() ,
				comment: $('#new-answer-comment').val()
			}
			var result = validateInput(input);
			if (!result.valid) {
				/*
				bootbox.alert(result.message);
				*/
				bootbox.dialog(result.message, [{
						label: Yii.t('standard', '_CANCEL'),
						'class': 'btn-docebo big black',
						callback: function() { }
					}],
					{ header: Yii.t('standard', '_WARNING') }
				);
				return;
			}

			// Let's update the GRID values
			$tr.find('td:eq(1)').html(input.answers.length + " " + Yii.t('test', 'choices'));
			// Let's manage the Scores
			var pre1, value1, pre2, value2;
			if (input.scoreCorrect != 0) {
				pre1 = '+&nbsp;';
				value1 = parseFloat(input.scoreCorrect);
			} else {
				pre1 = '';
				value1 = '0';
			}
			if (input.scoreIncorrect != 0) {
				pre2 = '-&nbsp;';
				value2 = parseFloat(input.scoreIncorrect);
			} else {
				pre2 = '';
				value2 = '0';
			}
			$tr.find('td:eq(2)').html(pre1 + value1);
			$tr.find('td:eq(3)').html(pre2 + value2);

			// Let's take care for the hidden inputs
			var preId = 'answer-'+isEditing+'-';
			$('#'+preId+'answers').val(input.answers);
			$('#'+preId+'case_sensitive').val(input.case_sensitive ? '1' : '0');
			$('#'+preId+'score_correct').val(input.scoreCorrect);
			$('#'+preId+'score_incorrect').val(input.scoreIncorrect);
			$('#'+preId+'comment').val(input.comment);

			var top = $tr.offset().top;
			$('body').animate({scrollTop: top}, 'slow');

			deactivateEditMode();
		});

		$('#undo-edit-answer-button').on("click", function(e) {
			deactivateEditMode();
		});


		var AddMask = {
			id: 'form-box-container',
			headerId: 'add-answer-link-container',
			hide: function() {
				$('#'+this.id).css('display', 'none');
				$('#'+this.headerId).css('display', 'block');
			},
			show: function() {
				$('#'+this.id).css('display', 'block');
				$('#'+this.headerId).css('display', 'none');
			},
			hidden: function() { return $('#'+this.id).css('display') == 'none'; },
			showed: function() { return $('#'+this.id).css('display') == 'block'; },
			toggle: function() {
				if (this.hidden()) {
					this.show();
				} else {
					this.hide();
				}
			},
			init: function() {
				this.hide();
				var oScope = this;
				$('#toggle-add-answer').on('click', function(e) {
					e.preventDefault();
					oScope.toggle();
				});
				$('#undo-add-answer-button').on('click', function(e) {
					oScope.hide();
				});
			}
		};

		AddMask.init();

		var getNumAnswers = function() { return $('#answers-tbody > tr').length; }
		var checkNumAnswers = function() {
			return (getNumAnswers() != 0);
		};

		var checkShortCodes = function(){
			//We need to check if the Question title contains shortcodes
			var content = $('#question-text').tinymce().getContent();
			var reg = /\[(.+?)]/g;
			var result = content.match(reg);
			// If does then the number of the codes chould be equal to the number of the answers (theoretically)
			if(result){
				for(var i=0;i<result.length;){
					if(!$('tbody#answers-tbody td.rowIndex > p:contains('+result[i]+')').length)
						return false;
					i++;
				}
			}
			return true;
		};

		var checkShortCodesAndAnswers = function(){
			var content = $('#question-text').tinymce().getContent();
			// Check if all rows exists in the question title
			var rows = $('tbody#answers-tbody tr').length;
			if(rows){
				var rowsContens = [];
				$('tbody#answers-tbody tr td:first-child p').each(function(){
					rowsContens.push($(this).html());
				});
				for(var i=0;i<rowsContens.length;){
					var result = content.indexOf(rowsContens[i]);
					var notExist = result < 0;
					if(notExist){
						return false;
					}
					i++;
				}
			}
			return true;
		};

		var addAnswersHeader = function(){
			if($('table#answers-table tr.answers-header').length) return;
			var thead = '<thead><tr class="answers-header">';
			thead += '<th>' + Yii.t('test', 'Schortcode').toUpperCase() + '</th>';
			thead += '<th>' + Yii.t('test', 'Possible Answers').toUpperCase()  + '</th>';
			thead += '<th>' + Yii.t('test', '_TEST_IFCORRECT').toUpperCase()  + '</th>';
			thead += '<th>' + Yii.t('test', '_TEST_IFINCORRECT').toUpperCase()  + '</th>';
			thead += '</tr></<thead>';
			$('table#answers-table').prepend(thead);
		};

		// If the user click outside of the TinyMCE and this click is not for adding code inside the TinyMCE then remove the flag
		$(document).on('click', function(e){
			if(!$(e.target).parent().hasClass('rowIndex'))
				AnswersManager.focusedElement = false;

			return true;
		});
		// If the user click on
		$(document).on('click', 'table tbody td.rowIndex p', function(){
			if(AnswersManager.focusedElement == false)
				return;

        	var text = $(this).html();
			$('#question-text').tinymce().execCommand( 'mceInsertContent', false, text);
		});

		if (Validation) {
			// Check if the answer list is empty
			Validation.addValidator(checkNumAnswers, Yii.t('test', 'You should provide at least one answer for this type of question'), 1);
			// Check if everything is ok with the Shortcodes
			Validation.addValidator(checkShortCodes, Yii.t('test', 'You should provide answers for all the shortcodes used in your question title'), 1);
			// Check if the shortcodes are matching
			Validation.addValidator(checkShortCodesAndAnswers, Yii.t('test', 'You should provide shortcodes in the title for all answers you have in the list'), 1);
		}

		if (options.answers) {
			var Answers = $.type(options.answers) == 'array' ? options.answers : [];
			for (var i=0; i<Answers.length; i++) {
				addTableRow(Answers[i]);
				generalCounter = Answers[i].sequence;
			}
		}


	}

};