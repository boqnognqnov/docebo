<div id="display_options" class="settings-tab hidden-tab">
	<div class="coursesettings-grey-box" style="margin-top: 10px;">
		<div class="span4">
			<div class="settings-left-title"><?= Yii::t("course", "_WHERE_SHOW_COURSE") ?></div>
		</div>
		<div class="span8">
			<div class="settings-subscr-form">
				<? echo $form->radioButtonList($courseModel, 'show_rules', LearningCourse::displayRulesList()) ?>
			</div>
		</div>
	</div>
	<div class="coursesettings-white-box">
		<div class="span4">
			<div class="settings-left-title"><?= Yii::t("course", "_WHAT_SHOW") ?></div>
		</div>
		<div class="span8">
			<div class="settings-subscr-form">
				<?php echo $form->checkBox($courseModel, 'show_progress') ?>
				<?php echo $form->labelEx($courseModel, 'show_progress'); ?>

				<?php echo $form->checkBox($courseModel, 'show_time') ?>
				<?php echo $form->labelEx($courseModel, 'show_time'); ?>

				<?php echo $form->checkBox($courseModel, 'show_extra_info') ?>
				<?php echo $form->labelEx($courseModel, 'show_extra_info'); ?>
			</div>
		</div>
	</div>

	<div class="coursesettings-grey-box" style="margin-top: 10px;">
		<div class="span4">
			<div class="settings-left-title"><?= Yii::t("course", "_SHOW_USER_OF_LEVEL") ?></div>
		</div>
		<div class="span8">
			<div class="settings-subscr-form">
				<?php echo $form->checkBoxList($courseModel, 'userLevels', LearningCourseuser::getLevelsArray()) ?>
				<?php echo $form->labelEx($courseModel, 'userLevels'); ?>
			</div>
		</div>
	</div>
	<div class="coursesettings-white-box">
		<div class="span4">
			<div class="settings-left-title"><?= Yii::t("course", "_COURSE_STATUS_CANNOT_ENTER") ?></div>
		</div>
		<div class="span8">
			<div class="settings-subscr-form">
				<?php echo $form->checkBoxList($courseModel, 'userStatuses', LearningCourseuser::getStatusesArray()) ?>
				<?php echo $form->labelEx($courseModel, 'userStatuses'); ?>
			</div>
		</div>
	</div>
</div>