<div id="social-rating-tab" class="settings-tab hidden-tab player-settings-form">

	<div class="coursesettings-grey-box">
		<div class="control-container odd">
			<div class="control-group">
				<span class="control-label">
					<?php echo Yii::t('social_rating', 'Social sharing'); ?>
					<p class="setting-description"><?php echo Yii::t('social_rating', 'Enable users to share a course on the major social networks (this will be applied as default for all courses, you can specify a custom setting in the course advanced settings)');?></p>
				</span>
				<div class="controls">
					<label class="checkbox">
						<?php echo $form->checkBox($courseModel, 'social_sharing_custom', array('id' => 'social_sharing_custom')); ?> <?php echo Yii::t('social_rating', 'Enable custom settings for this course'); ?>
					</label>
					<br>
					<div class="sharing-layout-settings-wrapper">
						<label class="radio">
							<?php echo $form->radioButton($courseModel, 'course_sharing_permission', array('value' => AdvancedSettingsSocialRatingForm::COURSE_SHARE_DISABLED, 'uncheckValue' => NULL)); ?>
							<?php echo Yii::t('social_rating', 'Disable courses social sharing'); ?>
						</label>

						<label class="radio">
							<?php echo $form->radioButton($courseModel, 'course_sharing_permission', array('value' => AdvancedSettingsSocialRatingForm::COURSE_SHARE_IF_ENROLLED, 'uncheckValue' => NULL)); ?>
							<?php echo Yii::t('social_rating', 'Users can share a course on social networks anytime, if enrolled'); ?>
						</label>
						<label class="radio">
							<?php echo $form->radioButton($courseModel, 'course_sharing_permission', array('value' => AdvancedSettingsSocialRatingForm::COURSE_SHARE_IF_COMPLETED, 'uncheckValue' => NULL)); ?>
							<?php echo Yii::t('social_rating', 'Users can share a course on social networks only if they completed the course'); ?>
						</label>
						<br/>
						<label class="checkbox">
							<?php echo $form->checkbox($courseModel, 'course_user_share_permission', array('uncheckValue' => 'off', 'value' => 'on')); ?>
							<?php echo Yii::t('social_rating', 'If the course is completed, user can also share his score') ?>
						</label>
						<br>
						<div class="row-fluid">
							<div class="span12 no-margin">
								<span><?php echo Yii::t('social_rating', 'Share on:');?></span>
							</div>
							<div class="no-margin span3">
								<?php echo $form->checkbox($courseModel, 'course_share_facebook', array('uncheckValue' => 'off', 'value' => 'on', 'id' => 'course_share_facebook')); ?>
								<?php echo CHtml::label(Yii::t('app', 'facebook'), 'course_share_facebook', array('class' => 'facebook-icon social-label'));?>
							</div>
							<div class="no-margin span3">
								<?php echo $form->checkbox($courseModel, 'course_share_twitter', array('uncheckValue' => 'off', 'value' => 'on', 'id' => 'course_share_twitter')); ?>
								<?php echo CHtml::label(Yii::t('app', 'twitter'), 'course_share_twitter', array('class' => 'twitter-icon social-label'));?>
							</div>
							<div class="no-margin span3">
								<?php echo $form->checkbox($courseModel, 'course_share_linkedin', array('uncheckValue' => 'off', 'value' => 'on', 'id' => 'course_share_linkedin')); ?>
								<?php echo CHtml::label(Yii::t('app', 'linkedin'), 'course_share_linkedin', array('class' => 'linkedin-icon social-label'));?>
							</div>
							<div class="no-margin span3">
								<?php echo $form->checkbox($courseModel, 'course_share_google', array('uncheckValue' => 'off', 'value' => 'on', 'id' => 'course_share_google')); ?>
								<?php echo CHtml::label(Yii::t('standard', '_GOOGLE'), 'course_share_google', array('class' => 'google-icon social-label'));?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="control-container">
			<div class="control-group">
				<span class="control-label">
					<?php echo Yii::t('social_rating', 'Rating'); ?>
					<p class="setting-description"><?php echo Yii::t('social_rating', 'Enable users to rate a course (this will be applied as default for all courses, you can specify a custom setting in the course advanced settings)');?></p>
				</span>

				<div class="controls">
					<label class="checkbox">
						<?php echo $form->checkBox($courseModel, 'social_rating_custom', array('id' => 'social_rating_custom')); ?> <?php echo Yii::t('social_rating', 'Enable custom settings for this course'); ?>
					</label>
					<br>
					<div class="rating-layout-settings-wrapper">
						<label class="radio">
							<?php echo $form->radioButton($courseModel, 'course_rating_permission', array('value' => AdvancedSettingsSocialRatingForm::COURSE_RATING_DISABLED, 'uncheckValue' => NULL)); ?>
							<?php echo Yii::t('social_rating', 'Disable course rating'); ?>
						</label>
						<label class="radio">
							<?php echo $form->radioButton($courseModel, 'course_rating_permission', array('value' => AdvancedSettingsSocialRatingForm::COURSE_RATING_ENABLED, 'uncheckValue' => NULL)); ?>
							<?php echo Yii::t('social_rating', 'Every user can rate a course, even if they\'re not enrolled'); ?>
						</label>
						<label class="radio">
							<?php echo $form->radioButton($courseModel, 'course_rating_permission', array('value' => AdvancedSettingsSocialRatingForm::COURSE_RATING_IF_ENROLLED, 'uncheckValue' => NULL)); ?>
							<?php echo Yii::t('social_rating', 'Users can rate a course, only if enrolled'); ?>
						</label>
						<label class="radio">
							<?php echo $form->radioButton($courseModel, 'course_rating_permission', array('value' => AdvancedSettingsSocialRatingForm::COURSE_RATING_IF_COMPLETED, 'uncheckValue' => NULL)); ?>
							<?php echo Yii::t('social_rating', 'Users can rate a course only if they completed the course'); ?>
						</label>
					</div>
				</div>
			</div>
		</div>
        <div class="control-container odd">
            <div class="control-group">
				<span class="control-label">
					<?php echo Yii::t('social_rating', 'Deep link'); ?>
                    <p class="setting-description"><?php echo Yii::t('social_rating', 'Generate a URL to share this course directly from the course page. Automatic enroll will be performed for the user that will open the URL without being enrolled.');?></p>
				</span>
                <div class="controls">
                    <label class="checkbox">
                        <?php echo $form->checkBox($courseModel, 'deep_link', array('id' => 'deep_link')); ?> <?php echo Yii::t('social_rating', 'Enable deep link for this course'); ?>
                    </label>
                    <br>
                    <div class="">
                        <label class="radio">
                            <div class="controls deep_link_demo" style="margin: 5px 0px 0px 8px; display: block">
                                <div class="input-group">
                                    <input style="width: 83%" id="copy-example" type="text" class="form-control" readonly="readonly" value="<?= $deepLink; ?>"/>
                                    <input rel="" data-original-title="<?=Yii::t('course','Copied to Clipboard!');?>" id="copy_button" data-clipboard-target="#copy-example" style="background-color: black !important; margin-left: -4px; padding-top: 7px;" class="btn btn-docebo black big" type="button" value="<?php echo Yii::t('coursepath', 'Copy'); ?>"/>
                                </div>
                            </div>
                        </label>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>

<script>
	$(function(){
		$('#social_sharing_custom').on('change', function(){
			if($(this).is(':checked')){
				$('.sharing-layout-settings-wrapper').css('opacity', '1');

			} else {
				$('.sharing-layout-settings-wrapper').css('opacity', '0.5');
			}
		}).trigger('change');

		$('#social_rating_custom').on('change', function(){
			if($(this).is(':checked')){
				$('.rating-layout-settings-wrapper').css('opacity', '1');

			} else {
				$('.rating-layout-settings-wrapper').css('opacity', '0.5');
			}
		}).trigger('change');

        $('#deep_link').on('change', function(){
            if($(this).is(':checked')){
                $('.deep_link_demo').css('display', 'block');
            } else {
                $('.deep_link_demo').css('display', 'none');
            }
        }).trigger('change');


        var clipboard = new Clipboard('#copy_button.btn');
        var copy_button = $('#copy_button');
        var options = {
            'trigger' : 'click'
        };

        clipboard.on('success', function(e) {
            copy_button.tooltip(options);
            copy_button.tooltip('show');
            $(document).off('mouseleave','#copy_button');
            copy_button.on('mouseleave', function() {
                copy_button.tooltip('hide');
            });
            e.clearSelection();
        });
	});
</script>
