<div id="special_options" class="settings-tab hidden-tab player-settings-form">
	<div class="coursesettings-grey-box" style="margin-top: 10px;">

		<?php echo $form->labelEx($courseModel, 'min_num_subscribe'); ?>
		<?php echo $form->textField($courseModel, 'min_num_subscribe'); ?>
		<div class="clearfix"></div>
		<br>

		<?php echo $form->labelEx($courseModel, 'max_num_subscribe'); ?>
		<?php echo $form->textField($courseModel, 'max_num_subscribe'); ?>
		<div class="clearfix"></div>
		<br>

		<?php echo $form->labelEx($courseModel, 'allow_overbooking'); ?>
		<?php echo $form->checkBox($courseModel, 'allow_overbooking'); ?>
		<div class="clearfix"></div>
		<br>

		<?php echo $form->labelEx($courseModel, 'course_quota'); ?>
		<?php echo $form->textField($courseModel, 'course_quota'); ?>
		<div class="clearfix"></div>
		<br>

		<?php echo $form->labelEx($courseModel, 'inherit_quota'); ?>
		<?php echo $form->checkBox($courseModel, 'inherit_quota'); ?>
		<div class="clearfix"></div>
		<br>
	</div>
</div>