<div id="catalog_options" class="settings-tab hidden-tab player-settings-form">
	<div class="coursesettings-grey-box">
		<div class="control-container odd">
			<div class="control-group">
				<span class="control-label"><?= Yii::t("course", "_WHERE_SHOW_COURSE") ?></span>
				<div class="controls">
					<label class="radio">
						<?php echo $form->radioButton($courseModel, 'show_rules', array('value' => $courseModel::DISPLAY_EVERYONE_HOMEPAGE, 'uncheckValue' => NULL)); ?>
						<?php echo Yii::t('course', '_SC_EVERYWHERE'); ?>
					</label>

					<label class="radio">
						<?php echo $form->radioButton($courseModel, 'show_rules', array('value' => $courseModel::DISPLAY_ONLY_LOGGED_USER, 'uncheckValue' => NULL)); ?>
						<?php echo Yii::t('course', '_SC_ONLY_IN'); ?>
					</label>
					<label class="radio">
						<?php echo $form->radioButton($courseModel, 'show_rules', array('value' => $courseModel::DISPLAY_ONLY_SUBSCRIBED_USERS, 'uncheckValue' => NULL)); ?>
						<?php echo Yii::t('course', '_SC_ONLYINSC_USER'); ?>
					</label>
				</div>
			</div>
		</div>
		<div class="control-container">
			<div class="control-group">
				<?php echo $form->labelEx($courseModel, 'max_num_subscribe', array('class' => 'control-label')); ?>
				<div class="controls">
					<?php echo $form->textField($courseModel, 'max_num_subscribe'); ?>

					&nbsp;&nbsp;&nbsp;&nbsp;<span style="color: #ff7904" id="max_num_subscribe_alert_id"></span>
				</div>

			</div>
		</div>
		<div class="control-container odd">
			<div class="control-group">
				<div>
					<? $this->widget('common.widgets.warningStrip.WarningStrip', array(
						'id'=>'waitlist-not-available-on-sale',
						'message'=>Yii::t('course', 'Automatic waitlist management is not available for courses on sale.'),
					)) ?>
				</div>
<!--				<div>-->
<!--					--><?// $this->widget('common.widgets.warningStrip.WarningStrip', array(
//						'id'=>'allow-self-unenrollment',
//						'message'=>Yii::t('course', 'You must allow learners self-unenrollment in order to enable this option. Find the self-unenrollment option into the Details tab'),
//					)) ?>
<!--				</div>-->
				<span class="control-label"><?php echo Yii::t('course', '_ALLOW_OVERBOOKING'); ?></span>
				<div class="controls">
					<label class="checkbox">
						<?php echo $form->checkBox($courseModel, 'allow_overbooking'); ?>
						<?php echo Yii::t('course', 'If this option is enabled users that enroll from the catalog when the max enrolled limit is reached are placed into a waiting list.'); ?>
					</label>
					<label class="checkbox">
						<?php echo $form->checkBox($courseModel, 'allow_automatically_enroll'); ?>
						<?php echo Yii::t('course', 'Automatically enroll one waitlisted learner when another learner is unenrolled.'); ?>
						<p class="help-block"><?php echo Yii::t('course', 'The system will select waitlested learners based on the enrollment date'); ?></p>
					</label>
				</div>
			</div>
		</div>
		<div class="control-container">
			<div class="control-group">
				<?php echo $form->labelEx($courseModel, 'course_demo', array('class' => 'control-label')); ?>
				<div class="controls">
					<?php echo $form->fileField($courseModel, 'course_demo'); ?>
					<p class="help-block"><?php echo Yii::t('course', 'This file will be available for the users inside the course details in the catalog (both internal and public).'); ?></p>
                    <?php
                        if ($courseModel->course_demo) :
                            $demoFileName = $courseModel->course_demo;
                            $storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_COURSEDOCS);
                            $previewActionUrl = $storageManager->fileUrl($demoFileName);
                    ?>
                    <p>
	                    <a href="<?php echo $previewActionUrl ?>" target="_blank"><?php echo $courseModel->course_demo ?></a>
	                    <a class="remove-demo-material i-sprite is-remove red" href="<?=Docebo::createLmsUrl('/player/coursesettings/delCourseDemoMaterial', array('course_id'=>$courseModel->idCourse))?>"></a>
                    </p>
                    <?php endif; ?>
				</div>
			</div>
		</div>
		<div class="control-container odd">
			<div class="control-group">
				<span class="control-label"><?= Yii::t("catalogue", "_COURSE_SUBSCRIPTION") ?></span>
				<div class="controls">
					<label class="radio">
						<?php echo $form->radioButton($courseModel, 'can_subscribe', array('value' => $courseModel::COURSE_SUBSCRIPTION_CLOSED, 'uncheckValue' => NULL)); ?>
						<?php echo Yii::t('course', '_SUBSCRIPTION_CLOSED'); ?>
					</label>

					<label class="radio">
						<?php echo $form->radioButton($courseModel, 'can_subscribe', array('value' => $courseModel::COURSE_SUBSCRIPTION_OPEN, 'uncheckValue' => NULL)); ?>
						<?php echo Yii::t('course', '_SUBSCRIPTION_OPEN'); ?>
					</label>
					<label class="radio">
						<?php echo $form->radioButton($courseModel, 'can_subscribe', array('value' => $courseModel::COURSE_SUBSCRIPTION_BETWEEN_DATES, 'uncheckValue' => NULL)); ?>
						<?php echo Yii::t('course', '_SUBSCRIPTION_IN_PERIOD'); ?>
					</label>
					<div id="can_subscribe_period_selection" class="form-inline form-suboption">
						<label class="settings-date-label"><?php echo Yii::t('standard', '_FROM'); ?></label>&nbsp;
						<?php echo $form->textField($courseModel, 'sub_start_date', array(
							'value' => Yii::app()->localtime->stripTimeFromLocalDateTime($courseModel->sub_start_date),
							'class' => 'datepicker settings-date-input',
							'id' => 'subscr-date-from'
						))?>
						<i class="p-sprite calendar-black-small settings-date-icon" id="subscr-from-icon"></i>

						<label class="settings-date-label" style="margin-left: 20px;"><?php echo Yii::t('standard', '_TO'); ?></label>&nbsp;
						<?php echo $form->textField($courseModel, 'sub_end_date', array(
							'value' => Yii::app()->localtime->stripTimeFromLocalDateTime($courseModel->sub_end_date),
							'class' => 'datepicker settings-date-input',
							'id' => 'subscr-date-to'
						))?>
						<i class="p-sprite calendar-black-small settings-date-icon" id="subscr-to-icon"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="control-container">
			<div class="control-group">
				<span class="control-label"><?= Yii::t("course", "_COURSE_SUBSRIBE") ?></span>
				<div class="controls">
					<label class="radio">
						<?php echo $form->radioButton($courseModel, 'subscribe_method', array('value' => $courseModel::SUBSMETHOD_ADMIN, 'uncheckValue' => NULL)); ?>
						<?php echo Yii::t('course', '_COURSE_S_GODADMIN'); ?>
					</label>

					<label class="radio">
						<?php echo $form->radioButton($courseModel, 'subscribe_method', array('value' => $courseModel::SUBSMETHOD_MODERATED, 'uncheckValue' => NULL)); ?>
						<?php echo Yii::t('course', '_COURSE_S_MODERATE'); ?>
					</label>
					<label class="radio">
						<?php echo $form->radioButton($courseModel, 'subscribe_method', array('value' => $courseModel::SUBSMETHOD_FREE, 'uncheckValue' => NULL)); ?>
						<?php echo Yii::t('standard', '_COURSE_S_FREE'); ?>
					</label>
				</div>
			</div>
		</div>
		
		<?php if (PluginManager::isPluginActive('SubscriptionCodesApp')) : ?>
			<div class="control-container odd">
				<div class="control-group">
					<?php echo $form->labelEx($courseModel, 'autoregistration_code', array('class' => 'control-label')); ?>
					<div class="controls">
						<?php echo $form->textField($courseModel, 'autoregistration_code'); ?>
						&nbsp;<a id="self_generate_code" href="#" class="btn-docebo black big" title="<?php echo Yii::t('course', '_RANDOM_COURSE_AUTOREGISTRATION_CODE'); ?>">
							<i class="icon-retweet icon-white"></i>
						</a>
					</div>
				</div>
                <input type="hidden" name="old_max_subscriptions" value=<?= $courseModel->max_num_subscribe ?>>



            </div>
		<?php endif;?>
        <label id="max_subscriptions_increase_text"
               hidden><?php echo Yii::t('course', "If you increase subscription quota you'll auto enroll the waiting users") ?> </label>
        <input type="hidden" name="auto_enroll_boolean" value=<?= $courseModel->allow_automatically_enroll ?>>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function () {
		if ($("input[name=auto_enroll_boolean]").val() == 1) {
			$("input[name='LearningCourse[max_num_subscribe]']").keyup(function () {
				var oldEnrollmentsCounter = $("input[name=old_max_subscriptions]").val();
				var newEnrollmentsCounter = $("input[name='LearningCourse[max_num_subscribe]']").val();

				if ($.isNumeric(newEnrollmentsCounter)) {
					if (oldEnrollmentsCounter < newEnrollmentsCounter) {
						$("#max_num_subscribe_alert_id").text($("#max_subscriptions_increase_text").text());
					} else if (oldEnrollmentsCounter >= newEnrollmentsCounter) {
						$("#max_num_subscribe_alert_id").text('');
					}
				}
			});
		}
	});

//	$courseModel, 'max_num_subscribe'
	$(function(){
		$('.remove-demo-material').click(function(e){
			e.preventDefault();
			var url = $(this).attr('href');
			var t = $(this);

			$.ajax({
				'url': url,
				'type': 'GET',
				'dataType': 'json',
				'success': function(data){
					if(data.success){
						t.parent().remove();
					}
				}
			});
		});
	});
	$(document).on("ready", function(){
		var waitlistNotAvailableOnSale = $('#waitlist-not-available-on-sale');
		var attentionWaitlistNotAvailableOnSale = $('#attention-waitlist-not-available-on-sale');

		var allowAutomaticallyEnroll = $('#LearningCourse_allow_automatically_enroll');
		var learningCourseSelling  = $('#LearningCourse_selling');

		if (learningCourseSelling.is(':checked')){
			waitlistNotAvailableOnSale.show();
			disable(allowAutomaticallyEnroll);
		}

		$(document).on('change', '#LearningCourse_selling', function(){
			waitlistNotAvailableOnSale.toggle();
			if($(this).is(':checked')) {
				attentionWaitlistNotAvailableOnSale.show();
				disable(allowAutomaticallyEnroll);
			}
			else {
				attentionWaitlistNotAvailableOnSale.hide();
				enable(allowAutomaticallyEnroll);
			}
		})
	})

	function enable(obj){
		obj.attr('disabled', false).trigger('refresh');
		obj.closest("label").removeClass("disabled");
	}

	function disable(obj){
		obj.attr('disabled', true).prop("checked", false).trigger('refresh');
		obj.closest("label").addClass("disabled");
		//obj.prop("checked", false);
	}

</script>