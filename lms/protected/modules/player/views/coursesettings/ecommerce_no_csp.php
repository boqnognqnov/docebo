<div class="no-csp-notification-box">
	<div class="warning-message-yellow">
		<i class="fa fa-info-circle fa-2x"></i>
		<?= Yii::t('marketplace', 'You cannot sell courses containing training materials from content service providers') ?>
	</div>
</div>
