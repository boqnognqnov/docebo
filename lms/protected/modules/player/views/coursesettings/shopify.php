
<div id="shopify-tab" class="settings-tab hidden-tab player-settings-form">

	<div class="coursesettings-grey-box">
		<div class="control-container odd">
			<div class="control-group">
				<span class="control-label"><?php echo Yii::t('course', '_COURSE_SELL'); ?></span>
				<div class="controls">
					<label class="checkbox">
						<?php echo $form->checkBox($courseModel, 'selling'); ?> <?php echo Yii::t('course', 'Put the course on the shelf'); ?>
					</label>
					<p class="help-block"><?php echo Yii::t('ecommerce', 'If this option is enabled the course will be available for purchase in the catalog by the users.'); ?></p>
				</div>
			</div>
		</div>
		<div class="control-container">
			<div class="control-group">
				<?php
				//echo $form->labelEx($courseModel, 'prize', array('class'=>'control-label'));
				$_currencyCode = Settings::get('currency_code', '');
				echo CHtml::label(Yii::t('transaction', '_PRICE').(!empty($_currencyCode) ? '&nbsp('.$_currencyCode.')' : ''), 'LearningCourse_prize', array('class'=>'control-label'));
				?>
				<div class="controls">
					<div class="input-append input-container">
						<?php echo $form->textField($courseModel, 'prize', array('readonly' => ($courseModel->selling ? 'readonly' : ''))); ?>
						<!--<span class="add-on"><?php echo Settings::get('currency_symbol', '-');?></span>-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
