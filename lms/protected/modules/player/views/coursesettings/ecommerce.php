
<div id="ecommerce-tab" class="settings-tab hidden-tab player-settings-form">

	<div class="coursesettings-grey-box">
		<? if ($courseModel->mpidCourse) : ?>
			<div class="control-container">
				<div class="control-group">
					<?=Yii::t('ecommerce', 'You are not allowed to resell Courses purchased from Docebo\'s Marketplace.')?>
				</div>
			</div>
		<? else : ?>
		<div class="control-container odd">
			<div class="control-group">
				<div>
					<? $this->widget('common.widgets.warningStrip.WarningStrip', array(
						'id'=>'attention-waitlist-not-available-on-sale',
						'message'=>Yii::t('course', 'By putting this course on sale, the automatic waitlist management option will be disabled. Administrators must manually accept or reject learner enrollment requests. (Please, refer to the Waiting List option into the Catalog options tab.)'),
					)) ?>
				</div>
				<span class="control-label"><?php echo Yii::t('course', '_COURSE_SELL'); ?></span>
				<div class="controls">
					<label class="checkbox">
						<?php echo $form->checkBox($courseModel, 'selling'); ?> <?php echo Yii::t('course', 'Put the course on the shelf'); ?>
					</label>
					<p class="help-block"><?php echo Yii::t('ecommerce', 'If this option is enabled the course will be available for purchase in the catalog by the users.'); ?></p>
				</div>
			</div>
		</div>
		<div class="control-container">
			<div class="control-group">
				<?php echo $form->labelEx($courseModel, 'prize', array('class'=>'control-label')); ?>
				<div class="controls">
					<div class="input-append input-container">
						<?php echo $form->textField($courseModel, 'prize'); ?>
						<span class="add-on"><?php echo Settings::get('currency_symbol', '-');?></span>
					</div>
				</div>
			</div>
		</div>

        <?php
            // Raise event in the course ecommerce settings page so some plugins can add more attributes
            $event = new DEvent($this, array('course' => $courseModel));
            Yii::app()->event->raise('renderCourseConstInCredits', $event);
            if($event->return_value){
                echo $event->return_value;
            }
        ?>

		<? endif; ?>
	</div>
</div>
