<?php
/* @var $courseModel LearningCourse */ ?>
<div id="additional_fields" class="settings-tab hidden-tab player-settings-form">
    <div class="coursesettings-grey-box">
            <?php
                $courseFields = $this->getAdditionalFieldsToDisplay($courseModel);
                foreach($courseFields as $index => $courseField):?>
			        <div class="control-container <?= ($index % 2 === 0) ? "odd" : "even" ?>">
			            <div class="control-group">
			                <?= CHtml::label($courseField->translation, '', array('class' => 'control-label')) ?>
			                <div class="controls"><?= $courseField->render(); ?></div>
			            </div>
			        </div>                                      
        	<?php endforeach; ?>
    </div>
</div>

<script>
    $(function(){
        $(".settings-date-icon").on('click', function (ev) {
            $(this).prev('input').focus();
        });
    });
</script>