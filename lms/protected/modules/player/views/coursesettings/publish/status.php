<?php /* @var $courseModel LearningCourse */ ?>
<?php if($courseModel->status == LearningCourse::$COURSE_STATUS_EFFECTIVE):?>
	<span class="published-by"><?=Yii::t('player', "Published by") ?>:</span>
<?php else: ?>
	<span class="unpublished-by"><?=Yii::t('player', "Unpublished by") ?>:</span>
<?php endif; ?>
<span class="author"><?= $courseModel->publisher->getFullName() ?></span>&nbsp;<?=Yii::t('standard', 'on') ?>&nbsp;<span class="publish-date"><?= $courseModel->publish_date ?></span>