<style>
	#coursesettings-menu{
		margin: 0;
	}
</style>
<ul id="coursesettings-menu">
	<!-- <li>
		<a href="#general" data-target="general" class="p-hover settings-general">
			<span class="i-sprite is-gear"></span> <?=Yii::t('player', 'General')?>
		</a>
	</li> -->
	<?php
	// Is this a power user and the course was assigned to him?
	$pUserCourseAssigned = CoreUserPuCourse::model()->findByAttributes(array(
		'puser_id' => Yii::app()->user->id,
		'course_id' => $this->course_id
	));
	$puHaveAccessToCourseSettings = (Yii::app()->user->getIsPu() && Yii::app()->user->checkAccess('/lms/admin/course/mod') && $pUserCourseAssigned);
	if(Yii::app()->user->getIsGodadmin() || $puHaveAccessToCourseSettings){
	?>
	<li>
		<a href="#details" data-target="details" class="p-hover settings-details">
			<div class="icon">
				<span class="i-sprite is-list"></span>
			</div>
			<?=Yii::t('standard', '_DETAILS')?>
		</a>
	</li>
	<?php if (PluginManager::isPluginActive('CoursecatalogApp')) : ?>
	<li>
		<a href="#catalog_options" data-target="catalog_options" class="p-hover settings-displaymode">
			<div class="icon"><span class="i-sprite is-settings"></span></div> <?=Yii::t('player', 'Catalog options')?>
		</a>
	</li>
	<!-- <li>
		<a href="#subscription" data-target="subscription" class="p-hover settings-subscription">
			<span class="i-sprite is-check"></span> <?=Yii::t('catalogue', '_COURSE_SUBSCRIPTION')?>
		</a>
	</li> -->
	<?php endif; ?>
	<?php if (PluginManager::isPluginActive('EcommerceApp')) : ?>
		<li>
			<a href="#ecommerce-tab" data-target="ecommerce-tab" class="p-hover settings-ecommerce">
				<div class="icon"><span class="i-sprite is-cart"></span></div> <?=Yii::t('app', 'ecommerce')?>
			</a>
		</li>
	<?php endif; ?>
	<?php if (PluginManager::isPluginActive('ShopifyApp')) : ?>
		<li>
			<a href="#shopify-tab" data-target="shopify-tab" class="p-hover settings-ecommerce">
				<div class="icon"><span class="admin-ico shopify-icon-settings"></span></div> <?=Yii::t('app', 'Shopify')?>
			</a>
		</li>
	<?php endif; ?>
    <?php if($courseModel->course_type != LearningCourse::TYPE_CLASSROOM && $courseModel->course_type != LearningCourse::TYPE_WEBINAR): ?>
	<li>
		<a href="#time_options" data-target="time_options" class="p-hover settings-time">
			<div class="icon"><span class="i-sprite is-timer"></span></div> <?=Yii::t('course', '_COURSE_TIME_OPTION')?>
		</a>
	</li>
    <?php endif; ?>
	<li>
		<a href="#certificates" data-target="certificates" class="p-hover settings-certificates">
			<div class="icon"><span class="i-sprite is-cert"></span></div> <?=Yii::t('menu', '_CERTIFICATE')?>
		</a>
	</li>
    <?php if($courseModel->course_type != LearningCourse::TYPE_CLASSROOM && $courseModel->course_type != LearningCourse::TYPE_WEBINAR): ?>
	<li>
		<a href="#player-bg" data-target="player-bg" class="p-hover settings-playerbg">
			<div class="icon"><span class="i-sprite is-courseplayer"></span></div> <?=Yii::t('templatemanager', 'Course Player')?>
		</a>
	</li>
    <?php endif; ?>
	<li>
		<a href="#social-rating-tab" data-target="social-rating-tab" class="p-hover">
			<div class="icon"><span class="social-rating-menu-icon"></span></div> <?=Yii::t('social_rating', 'Social & Rating')?>
		</a>
	</li>
    
    <?php if (PluginManager::isPluginActive('CertificationApp')) : ?>
	<li>
		<a href="#retraining" data-target="retraining" class="p-hover settings-retraining">
			<div class="icon"><span class="docebo-sprite ico-cert small"></span></div> <?=Yii::t('certification', 'Retraining')?>
		</a>
	</li>
    <?php endif; ?>
	<?php if(($courseModel->course_type == LearningCourse::TYPE_ELEARNING) && PluginManager::isPluginActive('CoachingApp')): ?>
	<li>
		<a href="#coaching" data-target="coaching" class="p-hover">
			<div class="icon"><i class="coaching-menu-icon fa fa-comments-o fa-lg"></i></div> <?=Yii::t('course', 'Coaching')?>
		</a>
	</li>
	<?php endif; ?>
	<li>
		<a href="#final_score" data-target="final_score" class="p-hover">
			<div class="icon"><i class="fa fa-flag-checkered fa-lg"></i></div> <?=Yii::t('standard', '_SCORE')?>
		</a>
	</li>
    <?php if ((Yii::app()->user->getIsGodadmin() || Yii::app()->user->getIsAdmin()) && LearningCourseField::hasAnyField()): ?>
        <li>
            <a href="#additional_fields" data-target="additional_fields" class="p-hover settings-details">
                <div class="icon"><i class="i-sprite is-list"></i></div>

                <?=Yii::t('menu', '_FIELD_MANAGER')?>
            </a>
        </li>
    <?php endif;?>
		<li>
			<a href="#equivalent_courses" data-target="equivalent_courses" class="p-hover settings-details">
				<div class="icon" style="position:relative;">
					<i class="fa fa-exchange" style="font-size: 1.5em"></i>
				</div>

				<?=Yii::t('menu', 'Equivalencies')?>
			</a>
		</li>
	<?php }
	$event = new DEvent($this, array('puHaveAccessToCourseSettings' => $puHaveAccessToCourseSettings));
	Yii::app()->event->raise('ShowTabForCustomReportFields', $event);
	if(!$event->shouldPerformAsDefault()){
		echo $event->return_value;
	}

	// Event raised for YnY app to hook on to
	Yii::app()->event->raise('CourseSettingsCustomTabMenu', new DEvent($this, array('courseModel' => $courseModel)));
	?>
</ul>