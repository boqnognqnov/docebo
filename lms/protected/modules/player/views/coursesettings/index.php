<?php
//in some cases PUs may have management permissions for courses, but not the view permission (weird, but possible)
$isPUWithNoCourseViewPermission = false;
if (Yii::app()->user->isAdmin && !Yii::app()->user->checkAccess('/lms/admin/course/view')) {
	$isPUWithNoCourseViewPermission = true;
}
//we already have the base training breadcrumb added, so we need to add one in addition
if (!$isPUWithNoCourseViewPermission) {
	$this->breadcrumbs[] = array(
		'label' => Docebo::ellipsis(Yii::t('player', $courseModel->name), 60, '...'),
		'url' => Docebo::createLmsUrl('player', PlayerBaseController::ref_src(array('course_id' => $courseModel->idCourse)))
	);
} else {
	$this->breadcrumbs[] = Docebo::ellipsis(Yii::t('player', $courseName), 60, '...');
}
$this->breadcrumbs[] = Yii::t('standard', 'Settings');
if(Yii::app()->user->hasFlash('error')) { ?>
	<div class="row-fluid">
		<div class="span12">
			<div class='alert alert-error alert-compact text-left'>
				<?= Yii::app()->user->getFlash('error'); ?>
			</div>
		</div>
	</div>
<?php } ?>

<?php $this->renderPartial('publish_widget', array('courseModel' => $courseModel, 'canTogglePublishStatus' => $canTogglePublishStatus)) ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'learning-course-form',
	'enableAjaxValidation' => false,
	'htmlOptions'=>array(
		'enctype'=>'multipart/form-data',
		'class' => 'form-horizontal docebo-form',
	),
)); ?>
<?php // this is for the current tab
echo CHtml::hiddenField('current_tab') ?>
<div class="coursesetting-wrapper">
	<div class="coursesetting-sidebar">
		<?= $this->renderPartial('_menu', array('courseModel' => $courseModel)) ?>
	</div>
	<div class="coursesetting-main">
		<div class="coursesetting-content">
			<?php echo $form->errorSummary($courseModel); ?>
			<?php $this->renderPartial('details', array('courseModel' => $courseModel, 'form' => $form, 'showLabelsSelector' => $showLabelsSelector, 'showChannelsSelector' => $showChannelsSelector)) ?>
			<?php
				if (PluginManager::isPluginActive('EcommerceApp')){
					if (LearningCourse::hasCspObjects($courseModel->idCourse)) {
						$this->renderPartial('ecommerce_no_csp', array('courseModel' => $courseModel, 'form' => $form));
					} else {
						$this->renderPartial('ecommerce', array('courseModel' => $courseModel, 'form' => $form));
					}
				}
			?>
			<?php
				if (PluginManager::isPluginActive('ShopifyApp')){
					if (LearningCourse::hasCspObjects($courseModel->idCourse)) {
						$this->renderPartial('ecommerce_no_csp', array('courseModel' => $courseModel, 'form' => $form));
					} else {
						$this->renderPartial('shopify', array('courseModel' => $courseModel, 'form' => $form));
					}
				}
			?>
			<?php $this->renderPartial('catalog', array('courseModel' => $courseModel, 'form' => $form)) ?>
            <?php if($courseModel->course_type != LearningCourse::TYPE_CLASSROOM && $courseModel->course_type != LearningCourse::TYPE_WEBINAR): ?>
			<?php $this->renderPartial('time_options', array('courseModel' => $courseModel, 'form' => $form)) ?>
            <?php endif; ?>
			<?php $this->renderPartial('certificates', array('courseModel' => $courseModel, 'certificates' => $certificates, 'form' => $form)) ?>
            <?php if($courseModel->course_type != LearningCourse::TYPE_CLASSROOM && $courseModel->course_type != LearningCourse::TYPE_WEBINAR): ?>
			<?php $this->renderPartial('player_bg', array('courseModel' => $courseModel, 'form' => $form)) ?>
            <?php endif; ?>
			<?php $this->renderPartial('social_rating_settings', array('courseModel' => $courseModel, 'form' => $form, 'deepLink'=>$deepLink)) ?>
            
            <?php if (PluginManager::isPluginActive('CertificationApp')) : ?>
            	<?php $this->renderPartial('_retraining', array('courseModel' => $courseModel, 'form' => $form)); ?>
            <?php endif; ?>
			<?php if(($courseModel->course_type == LearningCourse::TYPE_ELEARNING) && PluginManager::isPluginActive('CoachingApp')): ?>
			<?php $this->renderPartial('_coaching', array('courseModel' => $courseModel, 'form' => $form)); ?>
			<?php endif; ?>
			<?php $this->renderPartial('_final_score', array('courseModel' => $courseModel, 'form' => $form)); ?>
			<?php $this->renderPartial('_equivalent_courses', array('courseModel' => $courseModel, 'form' => $form)); ?>

			<?php
			$event = new DEvent($this, array());
			Yii::app()->event->raise('ShowAdditionalTabWithSettings', $event);

			// Event raised for YnY app to hook on to
			Yii::app()->event->raise('CourseSettingsCustomTabContent', new DEvent($this, array('form' => $form, 'courseModel' => $courseModel)));

			if (!$event->shouldPerformAsDefault() && $event->return_value == 'renderCustomSettingsFields') {
				$this->widget('GeApp.widgets.RenderHelperWidget', array('msg' => '', 'renderPartial' => true));
			}
			?>
            <?php (LearningCourseField::hasAnyField()) ? $this->renderPartial('additional_fields', array(
                'courseModel' =>  $courseModel
            )): null ?>
		</div>
	</div>
	<div class="coursesetting-button text-right">
		<div class="cs-button-inner">
			<?php echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class' => 'btn-docebo big green')); ?>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>
<script type="text/javascript">

var oImageManagePanel = false;


$(function () {
    Coursesettings.dateFormat = '<?= Yii::app()->localtime->dateStringToBDatepickerFormat() ?>';
	Coursesettings.init();
	$('input').styler();

})
</script>