<div id="certificates" class="settings-tab hidden-tab">
	<div class="coursesettings-grey-box">
		<div class="control-container odd">
			<div class="control-group">
				<span class="control-label"><?= Yii::t("player", "Choose certificate's template") ?></span>
				<div class="controls">
					<?php
					if (isset($courseModel->certificate)) $assigned_certificate = $courseModel->certificate->id_certificate;
					else $assigned_certificate = 0;

					echo Chtml::dropDownList('LearningCourse[assigned_certificate]', $assigned_certificate, $certificates, array(
						'id' => 'LearningCourse_assigned_certificate'
					)); ?>
					&nbsp;
					
					<a class="btn-docebo grey big preview-cert"><i class="icon-search icon-white"></i> <?= Yii::t("standard", "_PREVIEW") ?></a>
					
					<!-- 
					<a id="certificate_preview" href="../doceboLms/index.php?modname=mycertificate&op=preview_cert&id_course=<?php echo $courseModel->idCourse; ?>&id_certificate=<?php echo $assigned_certificate; ?>"
					   data-url="../doceboLms/index.php?modname=mycertificate&op=preview_cert&id_course=<?php echo $courseModel->idCourse; ?>&id_certificate="
					   class="btn-docebo grey big"><i class="icon-search"></i> <?= Yii::t("standard", "_PREVIEW") ?></a>
					 -->   
					   
					<p class="help-block"><?= Yii::t("player", "The selected certificate will be available to the user after the course completion"); ?></p>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
<!--
var CERT_URL = '<?= Docebo::createAdminUrl('certificateManagement/downloadTemplate', array(id => '')) ?>';
	$(function(){

		// Download Certificate PDF by calling admin action
		$('.preview-cert').on('click', function(){
			var certId = $('#LearningCourse_assigned_certificate').val();
			if (certId > 0) {
				var url = CERT_URL + parseInt(certId);
				window.location = url;
			}
			return false;
		});
		
	});

//-->
</script>