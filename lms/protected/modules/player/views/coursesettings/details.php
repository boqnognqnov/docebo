<div id="details" class="settings-tab hidden-tab player-settings-form">
	<div class="coursesettings-grey-box">
		<div class="control-container odd">
			<div class="control-group">
				<?php echo $form->labelEx($courseModel, 'idCategory', array('class'=>'control-label')); ?>
				<div class="controls">
					<?php echo $form->dropDownList($courseModel, 'idCategory', LearningCourseCategory::getCategoriesList(), array('encode'=>false, 'prompt'=>'')); ?>
				</div>
			</div>
		</div>
		<div class="control-container">
			<div class="control-group">
				<?php echo $form->labelEx($courseModel, 'lang_code', array('class'=>'control-label')); ?>
				<div class="controls">
					<?php echo $form->dropDownList($courseModel, 'lang_code', CHtml::listData(Lang::getLanguages(), 'code', 'description')); ?>
				</div>
			</div>
		</div>
		<? $index = 1; ?>
		<?php if ($showLabelsSelector) : ?>
		<div class="control-container <?=$index%2 ? 'odd' : ''; $index++;?>">
			<div class="control-group">
				<?php echo $form->labelEx($courseModel, 'labels', array('class'=>'control-label')); ?>
				<div class="controls">
					<?php echo $form->dropDownList($courseModel, 'labels', CHtml::listData(LearningLabel::getLabelsByLang(Lang::getCodeByBrowserCode(Yii::app()->getLanguage())), 'id_common_label', 'title'), array('prompt'=>Yii::t('player', 'Select label'))); ?>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<?php if ($showChannelsSelector) : ?>
			<div class="control-container <?=$index%2 ? 'odd' : ''; $index++;?>">
				<div class="control-group">
					<?php echo $form->labelEx($courseModel, 'channels', array('class'=>'control-label')); ?>
					<div class="controls">
						<?php $this->widget('lms.protected.modules.channels.widgets.Selector', array('itemType' => App7020ChannelAssets::ASSET_TYPE_COURSES, 'item' => $courseModel, 'form' => $form)); ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<div class="control-container  <?=$index%2 ? 'odd' : ''; $index++;?>">
			<div class="control-group">
				<?php echo $form->labelEx($courseModel, 'credits', array('class'=>'control-label')); ?>
				<div class="controls">
					<?php echo $form->textField($courseModel, 'credits'); ?>
				</div>
			</div>
		</div>

        <?php if($courseModel->course_type != LearningCourse::TYPE_CLASSROOM && $courseModel->course_type != LearningCourse::TYPE_WEBINAR): ?>
		<div class="control-container <?=$index%2 ? 'odd' : ''; $index++;?>">
			<div class="control-group">
				<span class="control-label"><?= Yii::t("standard", "Navigation rules") ?></span>
				<div class="controls">
					<label class="radio">
						<?php echo $form->radioButton($courseModel, 'prerequisites_policy', array('value' => $courseModel::NAVRULE_FREE, 'uncheckValue' => NULL)); ?>
						<?php echo Yii::t('organization', 'Free learning path'); ?>
					</label>

					<label class="radio">
						<?php echo $form->radioButton($courseModel, 'prerequisites_policy', array('value' => $courseModel::NAVRULE_SEQUENTAL, 'uncheckValue' => NULL)); ?>
						<?php echo Yii::t('organization', 'Sequential learning path'); ?>
					</label>
					<label class="radio">
						<?php echo $form->radioButton($courseModel, 'prerequisites_policy', array('value' => $courseModel::NAVRULE_FINALTEST, 'uncheckValue' => NULL)); ?>
						<?php echo Yii::t('organization', 'Conditional learning path, final test'); ?>
					</label>

					<label class="radio">
						<?php echo $form->radioButton($courseModel, 'prerequisites_policy', array('value' => $courseModel::NAVRULE_PRETEST, 'uncheckValue' => NULL)); ?>
						<?php echo Yii::t('organization', 'Conditional learning path, pre-test'); ?>
					</label>
					
				</div>
			</div>
		</div>
        <?php endif; ?>

		<div class="control-container <?=$index%2 ? 'odd' : ''; $index++;?>">
			<div class="control-group">
				<?php echo $form->labelEx($courseModel, 'enable_self_unenrollment', array('class'=>'control-label')); ?>
				<div class="controls">
					<label class="checkbox">
						<?php echo $form->checkBox($courseModel, 'enable_self_unenrollment'); ?>
						<?php echo Yii::t('course', 'Allow self-unenrollment from this course'); ?>
					</label>
					<?php if($courseModel->course_type == LearningCourse::TYPE_CLASSROOM || $courseModel->course_type == LearningCourse::TYPE_WEBINAR){ ?>
						<label class="checkbox">
							<?php echo $form->checkBox($courseModel, 'enable_session_self_unenrollment'); ?>
							<?php echo Yii::t('course', 'Allow self-unenrollment from this session'); ?>
						</label>
					<?php } ?>

					<label class="checkbox">
						<?php echo $form->checkBox($courseModel, 'enable_unenrollment_on_course_completion'); ?>
						<?php echo Yii::t('course', 'Allow self-unenrollment also if the course has been completed'); ?>
					</label>

				</div>
			</div>
		</div>
		
		
		<div class="control-container  <?=$index%2 ? 'odd' : ''; $index++;?>">
			<div class="control-group">
				<?php echo $form->labelEx($courseModel, 'lo_max_attempts', array('class'=>'control-label')); ?>
				<div class="controls">
					<?php echo $form->textField($courseModel, 'lo_max_attempts'); ?>
					<p class="help-block"><?= Yii::t('course', 'Leave empty or set to 0 for unlimited failing attempts allowed for this course training materials') ?></p>
				</div>
			</div>
		</div>
		
		
		<?php
		$event = new DEvent($this, array('index' => $index, 'form' => $form, 'courseModel' => $courseModel));
		Yii::app()->event->raise('SetAdditionalOptionForCodes', $event);

		$html = '';
		// Event raised for YnY app to hook on to
		Yii::app()->event->raise('CourseDetailsAfterRender', new DEvent($this, array(
			'html' => &$html,
			'course' => $courseModel,
			'form' => $form
 		)));
		echo $html;
		?>
	</div>
</div>
