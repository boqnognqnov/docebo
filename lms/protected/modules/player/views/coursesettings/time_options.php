<?php
/* @var $courseModel LearningCourse */
//this little patch will prevent the printing of the "0000-00-00" value in input fields
if ($courseModel->date_begin == '0000-00-00') { $courseModel->date_begin = NULL; }
if ($courseModel->date_end == '0000-00-00') { $courseModel->date_end = NULL; }
?>
<div id="time_options" class="settings-tab hidden-tab player-settings-form">
	<div class="coursesettings-grey-box">
		<div class="control-container odd">
			<div class="control-group">
				<?php echo $form->labelEx($courseModel, 'date_begin', array('class' => 'control-label')); ?>
				<div class="controls">
					<?php echo $form->textField($courseModel, 'date_begin', array('class'=>'datepicker settings-date-input', 'id'=>'date-begin-calendar')); ?>
					<i class="p-sprite calendar-black-small settings-date-icon" id="date-begin-icon"></i>
					&nbsp;00:00:00 UTC
				</div>
			</div>
		</div>
		<div class="control-container">
			<div class="control-group">
				<?php echo $form->labelEx($courseModel, 'date_end', array('class' => 'control-label')); ?>
				<div class="controls">
					<?php echo $form->textField($courseModel, 'date_end', array('class'=>'datepicker settings-date-input', 'id'=>'date-end-calendar')); ?>
					<i class="p-sprite calendar-black-small settings-date-icon" id="date-end-icon"></i>
					&nbsp;23:59:59 UTC
				</div>
			</div>
		</div>
        <div class="control-container odd">
            <div class="control-group">
                <?php echo $form->labelEx($courseModel, 'soft_deadline', array('class' => 'control-label')); ?>
                <div class="controls">
                    <label class="checkbox">
                        <input type="checkbox" name="enable_soft_deadline_option" id="enable_soft_deadline_option" value="1" <?php if(!is_null($courseModel->soft_deadline)):?>checked="checked"<?php endif;?>>
                        <?php echo Yii::t('course', 'Enable custom deadline settings for this course'); ?>
                    </label>
                    <label id="soft_deadline_checkBox_wrapper" class="checkbox" <?php if(is_null($courseModel->soft_deadline)):?>style="opacity: 0.5;;"<?php endif;?>>
                        <?php echo $form->checkbox($courseModel, 'soft_deadline', array(
								'disabled' => is_null($courseModel->soft_deadline) ? "disabled" : "",
								'uncheckValue' => 0,
								'value' => 1,
								'checked' => (is_null($courseModel->soft_deadline) && $courseModel->getSoftDeadlineEnabled()) || ($courseModel->soft_deadline == LearningCourse::SOFT_DEADLINE_ON)
							)
						);
						?>
                        <?php echo Yii::t('course', 'Allow users to enter an E-Learning course even if the enrollment validity or course end date is expired'); ?>
                    </label>
                </div>
            </div>
        </div>
		<div class="control-container">
			<div class="control-group">
				<?php echo $form->labelEx($courseModel, 'valid_time', array('class' => 'control-label')); ?>
				<div class="controls">
					<?php echo $form->textField($courseModel, 'valid_time'); ?>
					<p class="help-block"><?php echo Yii::t('course', 'This setting allows users to access this course only for a specific number of days (days of validity)'); ?></p>
					<div class="row-fluid timeRadioRow">
						<div class="span4">
							<label>
								<?= $form->radioButton($courseModel, 'valid_time_type', array('value'=>LearningCourse::VALID_TIME_TYPE_FIRST_COURSE_ACCESS, 'uncheckValue'=>NULL))?>
								<?=Yii::t('course', 'after first course access')?>
							</label>
						</div>
						<div class="span8">
							<label style="margin-left:0">
								<?= $form->radioButton($courseModel, 'valid_time_type', array('value'=>LearningCourse::VALID_TIME_TYPE_BEING_ENROLLED, 'uncheckValue'=>NULL))?>
								<?=Yii::t('course', 'after being enrolled in the course')?>
							</label>
						</div>
					</div>
					<label class="checkbox">
						<input type="checkbox" name="update_enrollments_expire_dates" id="update_enrollments_expire_dates" value="1">
						<span id="alreadyStartedLabel"></span>
					</label>
				</div>
			</div>
		</div>
		<div class="control-container odd">
			<div class="control-group">
				<?php echo $form->labelEx($courseModel, 'mediumTime', array('class' => 'control-label')); ?>
				<div class="controls">
					<div class="course-time-control">
						<div class="course-time-input">
							<?= $form->textField($courseModel, 'durationHours', array('maxlength'=>3)) ?>
							<label for="LearningCourse_durationHours"><?= Yii::t('standard','_HOURS') ?></label>
						</div>
						<div class="course-time-input">
							<?= $form->textField($courseModel, 'durationMinutes', array('maxlength'=>2)) ?>
							<label for="LearningCourse_durationMinutes"><?= Yii::t('standard','_MINUTES') ?></label>
						</div>
						<div class="course-time-input">
							<?= $form->textField($courseModel, 'durationSeconds', array('maxlength'=>2)) ?>
							<label for="LearningCourse_durationSeconds"><?= Yii::t('standard','_SECONDS') ?></label>
						</div>
					</div>
					<p class="help-block"><?php echo Yii::t('course', 'This is the time extimated for completing the course, will be used in the delay analysis to find overdue trainer.'); ?></p>
				</div>
			</div>
		</div>
	</div>
</div>

<style type="text/css">
	.course-time-control {}
	.course-time-control .course-time-input {
		display: inline-block;
		margin-right: 15px;
	}
	.course-time-control .course-time-input input {
		width: 30px;
		text-align: right;
	}
	.course-time-control .course-time-input label {
		display: inline;
		vertical-align: middle;
	}

	.timeRadioRow {
		margin: 15px 0px 10px 0px;
	}
	.timeRadioRow label, .timeRadioRow label span {
		margin-left: 0;
	}


</style>

<script type="text/javascript">
	$(function(){
		// Enforce course end > course begin date
		var dateBeginField = $('#date-begin-calendar');
		var dateEndField = $('#date-end-calendar');

		var doEnforce = function(){
			var begin = Coursesettings.datePickerStartDate.getDate();
			var end = Coursesettings.datePickerEndDate.getDate();

			if(dateEndField.val()==='0000-00-00' || dateEndField.val() == "" || dateBeginField.val()==='0000-00-00' || dateBeginField.val() == ""){
				// clear errors, one of both ends is infinity
				changeSubmitButtonState(true);
			}else{
				// Do validation:
				if(end < begin){
					if(end != null){
						Docebo.Feedback.show('error', "<?=Yii::t('standard', 'Course end date must be after start date')?>");
						changeSubmitButtonState(false);
					}
				}else{
					changeSubmitButtonState(true);
				}
			}
		};
		var changeSubmitButtonState = function(enable){

			var submitBtn = $('.coursesetting-button input[type=submit]', '#learning-course-form');

			if(enable){
				submitBtn.removeClass('disabled');
				submitBtn.prop('disabled', false);
			}else{
				submitBtn.addClass('disabled');
				submitBtn.prop('disabled', true);
			}
		};

		dateBeginField.change(function(){
			doEnforce();
		});
		dateEndField.change(function(){
			doEnforce();
		});

        $('#enable_soft_deadline_option').on('change',function(){
            if($(this).is(':checked')){
                $('#soft_deadline_checkBox_wrapper').css('opacity', '1');
                $('#LearningCourse_soft_deadline').prop('disabled', false).trigger('refresh');
            }
            else {
                $('#soft_deadline_checkBox_wrapper').css('opacity', '0.5');
				<?php if(is_null($courseModel->soft_deadline) && $courseModel->getSoftDeadlineEnabled()): ?>
				$('#LearningCourse_soft_deadline').attr('checked', true).prop('disabled', 'disabled').trigger('refresh');
				<?php else: ?>
				$('#LearningCourse_soft_deadline').attr('checked', false).prop('disabled', 'disabled').trigger('refresh');
				<?php endif; ?>
            }
        });

		$(document).on('change', '[name*=valid_time_type]', function(){
			changeEnrollmentsExpireDatesLabel();
		});

		var changeEnrollmentsExpireDatesLabel = function(){
			var label = "<?php echo Yii::t('course', 'Also update all enrollments expiration dates where users already started this course'); ?>";

			if ($('[name*=valid_time_type]:checked').val() == 1){
				label = "<?php echo Yii::t('course', 'Also update all enrollments expiration dates for users already enrolled in this course'); ?>";
			}

			$("#alreadyStartedLabel").text(label);
		}

		changeEnrollmentsExpireDatesLabel();
	})
</script>