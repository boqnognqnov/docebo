<?php
	$dp = Certification::model()->sqlDataProvider(true);
	$data = $dp->getData();

	$list = array();
	foreach ($data as $cert) {
		$list[$cert['id_cert']] = $cert['title'];
	}

	$idCert = CertificationItem::itemCertification(CertificationItem::TYPE_COURSE, $courseModel->idCourse);

?>
<div id="retraining" class="settings-tab hidden-tab">
	<div class="coursesettings-grey-box">
		<div class="control-container odd">
			<div class="control-group">
				<span class="control-label"><?= Yii::t("certification", "Select certification") ?></span>
				<div class="controls">
					<?php
						echo Chtml::dropDownList('LearningCourse[certification]', $idCert, $list, array(
							'id' => 'LearningCourse_certification',
							'prompt' => Yii::t('certification','Select certification'),
						));
					?>
					&nbsp;


					<p class="help-block"><?= Yii::t("certification", "Select the certification to associate with this course. Student will obtain the above certification once this course is completed."); ?></p>
				</div>
			</div>
		</div>
	</div>
</div>

