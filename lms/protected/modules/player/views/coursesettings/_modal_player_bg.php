<h1><?= Yii::t('setup', 'Upload your background') ?></h1>


<?php
	// NON AJAX FOR !!!
	$form = $this->beginWidget('CActiveForm', array(
		'method' => 'post',
		'htmlOptions' => array(
			'class' => 'form-inline',
			'id' => 'course-player-bg-form',
			'enctype'=>"multipart/form-data",
		)
	));
?>

<?php 
	// I am preparing this to be a widget.. so I need this block here for all wanna-be-widget-properties values
	$radioName = 'image_id_radio'; 
	$systemImageCheckedId = $courseModel->player_bg;
	$userImageCheckedId = $courseModel->player_bg;
	$formId = 'course-player-bg-form';
	$systemImages = $systemImages;
	$userImages = $userImages;
	$chunkSize = $chunkSize;
	$modalId = 'modal-player-bg';
?>




<div class="image-manage-panel">

	<div class="row-fluid">
	
		<!-- TABS (LIST) -->
		<div class="span3">
			<ul class="nav nav-list">
				<li class="active">
					<a href="#system-images" data-toggle="tab">
						<?php echo Yii::t('templatemanager', 'Ours images collection'); ?> (<span id="system-images-count"></span>)
					</a>
				</li>
				<li>
					<a href="#user-images" data-toggle="tab">
						<?php echo Yii::t('templatemanager', 'Your images collection'); ?> (<span id="user-images-count"></span>)
					</a>
				</li>
			</ul>
		</div>
		
		
		
		<!-- TABS CONTENT (panes)  -->
		<div class="span9 tab-content">
		
			<!-- TAB PANE 1: System/Default images  -->
			<div class="tab-pane active" id="system-images">

			
				<div class="row-fluid">
					<div class="span12 pane-header">
						<div>
							<?php echo Yii::t('branding', 'Select image'); ?>
						</div>
					</div>
				</div>
			
				<div class="row-fluid">
					<div class="span12 text-center">
					
						<?php 
							$checkedId = $systemImageCheckedId;
						?>
					
						
						
							<div id="system-images-slider" class="carousel slide">
								<div class="carousel-inner">
									<?php foreach ($systemImages as $chunkIndex => $chunk) : ?>
										<div class="item<?php echo ($chunkIndex == 0) ? ' active' : ''; ?>">
											<?php foreach ($chunk as $imageId => $imageUrl) : ?>
												<div class="sub-item <?= ($imageId == $checkedId) ? 'checked' : '' ?>">
													<?= CHtml::image($imageUrl) ?>
													<input type="radio" name="<?= $radioName ?>" value="<?= $imageId ?>" <?= $imageId == $checkedId ? 'checked="checked"' : '' ?> />
													<span class="checkmark"><span class="i-sprite is-check green large"></span></span>
												</div>
											<?php endforeach; ?>
										</div>
									<?php endforeach; ?>
								</div>
								<?php if (count($systemImages) > 0) : ?>
									<a class="carousel-control left" href="#system-images-slider" data-slide="prev">&lsaquo;</a>
									<a class="carousel-control right" href="#system-images-slider" data-slide="next">&rsaquo;</a>
								<?php endif; ?>
							</div>
						
						
					
					</div>
				</div>
			
			</div>
			
			
			<!-- TAB PANE 2: User uploaded images  -->
			<div class="tab-pane" id="user-images">
			
				<div class="row-fluid">
					<div class="span12 pane-header">
						<div>
							<?php echo Yii::t('branding', '_UPLOAD_BACKGROUND_BTN'); ?>
						</div>
						<div>
							<?php echo CHtml::fileField('image_manage_file', '', array('id' => 'image-manage-file')); ?>
						</div>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12 text-center">
					
						<?php 
							$checkedId = $userImageCheckedId;
						?>
					
						<div class="clearfix"></div>	
					
							<div id="user-images-slider" class="carousel slide">
								<!-- Carousel items -->
								<div class="carousel-inner">
									<?php foreach ($userImages as $chunkIndex => $chunk) : ?>
										<div class="item<?php echo ($chunkIndex == 0) ? ' active' : ''; ?>">
											<?php foreach ($chunk as $imageId => $imageUrl) : ?>
												<div class="sub-item <?= ($imageId == $checkedId) ? 'checked' : '' ?>">
													<?= CHtml::image($imageUrl) ?>
													<input type="radio" name="<?= $radioName ?>" value="<?= $imageId ?>" <?= $imageId == $checkedId ? 'checked="checked"' : '' ?> />
													<span class="checkmark"><span class="i-sprite is-check green large"></span></span>
													<span class="deleteicon"><span class="i-sprite is-remove red"></span></span>
												</div>
											<?php endforeach; ?>
										</div>
									<?php endforeach; ?>
								</div>
								<!-- Carousel nav -->
								<?php
									$showControls = false;
									$tmpStyle = '';
									if (count($userImages) > 0) {
										$showControls = true;
									}
									if (!$showControls) {
										$tmpStyle = ' style="display:none;" ';
									}
		
								?>
									<a <?= $tmpStyle ?> class="carousel-control left" href="#user-images-slider" data-slide="prev">&lsaquo;</a>
									<a <?= $tmpStyle ?> class="carousel-control right" href="#user-images-slider" data-slide="next">&rsaquo;</a>
							</div>
						
						
						
						
					</div>
				</div>
		
			</div>
		
		</div>
	
	</div>

</div>	


<!-- For Dialog2 - they go in footer -->
<div class="form-actions">
	<!-- No SUBMIT BUTTON here! We handle 'clicks' and do AJAX form submition -->
	<?= CHtml::button(Yii::t('templatemanager', 'Use image'), array('class' 	=> 'confirm-save btn-docebo green big')); ?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' 	=> 'btn-docebo black big close-dialog')); ?>
</div>


<?php $this->endWidget(); ?>


					
<script type="text/javascript">


/**************************************************************************/
var ImageManagePanel = function(options) {
	this.settings = $.extend({}, this.settings, options);
}

ImageManagePanel.prototype = {

	/**
	 * Add event listeners
	 *
	 */
	addListeners: function() {

		// Provide an access to parent object (the ImageManagerPanel)
		var me = this;

		/**
		 * Listen to clicks on an images (carousel sub-items)
		 */
		$(document).on('click', '.image-manage-panel .item > div',function() {
			$('.image-manage-panel .item > div').removeClass('checked');
			$('.image-manage-panel .item > div input[type="radio"]').prop('checked', false);
			$(this).addClass('checked');
			$(this).find('input[type="radio"]').prop('checked', true);
		});

		/**
		 * Handle new image upload
		 */
		$(document).on('change', '#image-manage-file',function(e) {
		    var options = {
		      type        : 'post', 
		      dataType    : 'json',
		      data: {
		      }, 
		      beforeSubmit: function(){},
		      success     : function (res) {
			      if (res && res.success == true) {
			      	  me.liveAddImage('user-images', res.data);
			      }
		      }  
		    };
		    e.stopPropagation();
		    $('#' + me.settings.formId).ajaxForm(options).submit();

		    return false;
		});


		/**
		 * Final Form submition (returning image selection)
		 *
		 * Note:
		 * Selector MUST be 'a.confirm-save' and not just '.confirm-save' (we have an 'out of view' <input> having the same class! (Dialog2 form actions))
		 */
		 $(document).on('click', 'a.confirm-save', function(e){
		    var options = {
		      type        : 'post', 
		      dataType    : 'json',
		      data: {
			      confirm_save: true
		      }, 
		      success     : function (res) {
			    if (res && (res.success == true)) {
					me.settings.onSuccessCallback(res.data);
			    }  
			    // Close the srrounding Dialog!
			    $('#' + me.settings.modalId).dialog2('close');  
		      }
		  	};
		    e.stopPropagation();
		    $('#' + me.settings.formId).ajaxForm(options).submit();
			return false;
		});

			
		/**
		 * DELETE
		 */
		$(document).on('click', '#user-images-slider .deleteicon', function(e) {

			var imageId = $(this).closest('.sub-item').find('input[type="radio"]').val();

		    var options = {
				type        : 'post', 
				dataType    : 'json',
				data: {
					delete_image	: true,
					image_id		: imageId
				}, 
				success     : function (res) {
					if (res && (res.success == true)) {
						$('.image-manage-panel').find('input[type="radio"][value="'+ imageId +'"]').closest('.sub-item').remove();
						$('#user-images-count').text($('#user-images-slider .sub-item').length);
						me.controls();
					}  
				}
			};
			
		    e.stopPropagation();
		    
		    $('#' + me.settings.formId).ajaxForm(options).submit();
			return false;
			
		});
		
	},


	/**
	 * Upon uploading a new image we have to add it to the slider, LIVE
	 */
	liveAddImage: function(tabPaneId, data) {

		// Provide an access to parent object
		var me = this;

		// Uncheck all previosuly checked images
		$('.image-manage-panel .carousel .sub-item').removeClass('checked');

		// New sub-item, Both RADIO && IMAGE CHECKED!!!
		var newSubItem = '' +
			'<div class="sub-item checked">' +
			'  <img src="'+ data.image_url + '"> ' +
			'  <input type="radio" checked="checked" name="' + this.settings.radioName + '" value="' + data.image_id +'">' +
			'  <span class="checkmark"> ' +
			'    <span class="i-sprite is-check green large"></span> ' +
			'  </span> ' + 
			'  <span class="deleteicon"><span class="i-sprite is-remove red"></span></span> ' +
			'</div>' +  
		'';

		// New carousel Item (chucnk)
		var newItem = '' +
			'<div class="item">' +
			' ' + newSubItem + ''
			'</div>' +  
		'';		
		
		// Find a carousel item (chunk) having less than 'chunckSize' sub-items
		// and add new image into it. Otherwise, create new item (chunk)
		var found = false;

		// De-check all radios, just in case
		$('#' + tabPaneId + ' .carousel .item').find('input[type="radio"]').prop('checked', false);
		
		$('#' + tabPaneId + ' .carousel .item').each(function(){
			var $item = $(this);
			if ($item.find('.sub-item').length < me.settings.chunkSize) {
				$item.append(newSubItem);
				found = true;
				return false; // break each(), not returning from function!
			}
		});

		// If we did not find a proper chunk to insert the new image, just add a whole new chunck
		if (!found) {
			$('#' + tabPaneId + ' .carousel-inner').append(newItem);
		}
		
	    // Update HTML/etc.
	    this.controls();
		
		return true;
		
	},	
	
	
	/**
	 *  Do controls styling and post-render stuff: counting, selecting, activating, etc.
	 *
	 *	This one must be called after HTML is rendered/changed, every time.
	 */
	controls: function() {
		// Broswe button styling (we should find some other way, not useing styler)
		$('input[name="image_manage_file"]').styler({browseText:Yii.t('organization', 'Upload File')});

		// Count sub-items (images) and show count information in tab selector
		$('#system-images-count').text($('#system-images-slider .sub-item').length);
		$('#user-images-count').text($('#user-images-slider .sub-item').length);

		
		// Find "checked" sub-item (image) and make its chunk (the carousel item) "active"; stay inside its slider!
		var $subItemChecked = $('.image-manage-panel .sub-item.checked');
		if ($subItemChecked.length > 0) {
			// "Deactivate" all chuncks (carousel items) && Activate "checked" sub-item's chunk  
			$subItemChecked.closest('.carousel').find('.item').removeClass('active');
			$subItemChecked.parent().addClass('active');

			// Deactivate all tab panes, activate the one where we have checked sub-item
			$('.image-manage-panel .tab-pane').removeClass('active');
			$subItemChecked.closest('.tab-pane').addClass('active');

			// Deactivate all tabs (the left menu) && Activate the one that corresponds to the activated pane
			var activePaneId = $('.image-manage-panel .tab-pane.active').prop('id');
			$('.image-manage-panel .nav-list > li').removeClass('active');
			$('.image-manage-panel .nav-list > li a[href="#'+activePaneId+'"]').closest('li').addClass('active');
		}
		
		if ($('#user-images-slider .sub-item').length > 0) {
			$('#user-images-slider .carousel-control.right').show();
			$('#user-images-slider .carousel-control.left').show();
		}
		else {
			$('#user-images-slider .carousel-control.right').hide();
			$('#user-images-slider .carousel-control.left').hide();
		}	
		
		
	},	
	
}
/**************************************************************************/

// Used as callback in SAVE ajax call success
function onSuccessCallback(data) {
	var imageId = data.image_id;
	var imageUrl = data.image_url;
  	$('#player-bg .current img').attr('src',imageUrl);
  	$('#player-bg input[name*="player_bg_id"]').val(imageId);
}


// Loading the modal for the first time? Create the JS object
// Keep this inside the IF() otherwise you will get multiple oImageManagePanel and multiple listeners 
if (oImageManagePanel == false) {
	
	var oImageManagePanel = new ImageManagePanel({
		formId: '<?= $formId ?>',
		chunkSize: '<?= $chunkSize ?>',
		radioName: '<?= $radioName ?>',
		modalId	 : '<?= $modalId ?>',
		onSuccessCallback: onSuccessCallback,
	});

	// Add listeners 
	oImageManagePanel.addListeners();
}

oImageManagePanel.controls();




</script>					