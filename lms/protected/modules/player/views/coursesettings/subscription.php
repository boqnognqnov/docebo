<div id="subscription" class="settings-tab hidden-tab">
	<div class="coursesettings-grey-box">

		<div class="control-container odd">
			<div class="control-group">
				<span class="control-label"><?= Yii::t("catalogue", "_COURSE_SUBSCRIPTION") ?></span>
				<div class="controls">
					<label class="radio">
						<?php echo $form->radioButton($courseModel, 'can_subscribe', array('value' => $courseModel::COURSE_SUBSCRIPTION_CLOSED, 'uncheckValue' => NULL)); ?>
						<?php echo Yii::t('course', '_SUBSCRIPTION_CLOSED'); ?>
					</label>

					<label class="radio">
						<?php echo $form->radioButton($courseModel, 'can_subscribe', array('value' => $courseModel::COURSE_SUBSCRIPTION_OPEN, 'uncheckValue' => NULL)); ?>
						<?php echo Yii::t('course', '_SUBSCRIPTION_OPEN'); ?>
					</label>
					<label class="radio">
						<?php echo $form->radioButton($courseModel, 'can_subscribe', array('value' => $courseModel::COURSE_SUBSCRIPTION_BETWEEN_DATES, 'uncheckValue' => NULL)); ?>
						<?php echo Yii::t('course', '_SUBSCRIPTION_IN_PERIOD'); ?>
					</label>
					<div id="can_subscribe_period_selection" class="form-inline form-suboption">
						<label class="settings-date-label"><?php echo Yii::t('standard', '_FROM'); ?></label>&nbsp;
						<?php echo $form->textField($courseModel, 'sub_start_date', array('class' => 'datepicker settings-date-input', 'id' => 'subscr-date-from'))?>
						<i class="p-sprite calendar-black-small settings-date-icon" id="subscr-from-icon"></i>

						<label class="settings-date-label" style="margin-left: 20px;"><?php echo Yii::t('standard', '_TO'); ?></label>&nbsp;
						<?php echo $form->textField($courseModel, 'sub_end_date', array('class' => 'datepicker settings-date-input', 'id' => 'subscr-date-to'))?>
						<i class="p-sprite calendar-black-small settings-date-icon" id="subscr-to-icon"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="control-container">
			<div class="control-group">
				<span class="control-label"><?= Yii::t("course", "_COURSE_SUBSRIBE") ?></span>
				<div class="controls">
					<label class="radio">
						<?php echo $form->radioButton($courseModel, 'subscribe_method', array('value' => $courseModel::SUBSMETHOD_ADMIN, 'uncheckValue' => NULL)); ?>
						<?php echo Yii::t('course', '_COURSE_S_GODADMIN'); ?>
					</label>

					<label class="radio">
						<?php echo $form->radioButton($courseModel, 'subscribe_method', array('value' => $courseModel::SUBSMETHOD_MODERATED, 'uncheckValue' => NULL)); ?>
						<?php echo Yii::t('course', '_COURSE_S_MODERATE'); ?>
					</label>
					<label class="radio">
						<?php echo $form->radioButton($courseModel, 'subscribe_method', array('value' => $courseModel::SUBSMETHOD_FREE, 'uncheckValue' => NULL)); ?>
						<?php echo Yii::t('standard', '_COURSE_S_FREE'); ?>
					</label>
				</div>
			</div>
		</div>
		<div class="control-container odd">
			<div class="control-group">
				<?php echo $form->labelEx($courseModel, 'autoregistration_code', array('class' => 'control-label')); ?>
				<div class="controls">
					<?php echo $form->textField($courseModel, 'autoregistration_code'); ?>
					&nbsp;<a id="self_generate_code" href="#" class="btn-docebo black big" title="<?php echo Yii::t('course', '_RANDOM_COURSE_AUTOREGISTRATION_CODE'); ?>">
						<i class="icon-retweet icon-white"></i>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>