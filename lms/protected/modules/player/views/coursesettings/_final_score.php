<?
/* @var $courseModel LearningCourse */

$criteria = new CDbCriteria();
$criteria->compare( 'objectType', array(
	// Those are the only LO types that can have a score
	LearningOrganization::OBJECT_TYPE_AICC,
	LearningOrganization::OBJECT_TYPE_SCORMORG,
	LearningOrganization::OBJECT_TYPE_TEST,
	LearningOrganization::OBJECT_TYPE_TINCAN,
	LearningOrganization::OBJECT_TYPE_DELIVERABLE,
    LearningOrganization::OBJECT_TYPE_ELUCIDAT,
) );
$criteria->compare( 'idCourse', $courseModel->idCourse );
$criteria->select = 'idOrg, objectType, title, idCourse';
$courseLos        = CHtml::listData( LearningOrganization::model()->findAll( $criteria ), 'idOrg', 'title' );
?>

<div id="final_score" class="settings-tab hidden-tab player-settings-form">

	<div class="coursesettings-grey-box">

		<div class="control-container odd">
			<div class="control-group">
					<span class="control-label">
						<?php echo $form->labelEx( $courseModel, 'initial_score_mode', array( 'class' => 'control-label' ) ); ?>
					</span>

				<div class="controls">
					<? foreach ( LearningCourse::getInitialScoreModesList() as $key => $label ): ?>
						<label class="radio">
							<?php echo $form->radioButton( $courseModel, 'initial_score_mode', array(
								'value'        => $key,
								'uncheckValue' => NULL
							) ); ?>
							<?= $label ?>
						</label>
						<? if ( $key == LearningCourse::INITIAL_SCORE_TYPE_KEY_OBJECT ): ?>
							<?= $form->dropDownList( $courseModel, 'initial_object', $courseLos, array(
								'value'        => $courseModel ? $courseModel->initial_object : NULL,
								'uncheckValue' => NULL,
								'style'        => 'margin-left: 28px;' . ($courseModel->initial_score_mode==LearningCourse::INITIAL_SCORE_TYPE_KEY_OBJECT ? '' : 'display:none'),
							) ) ?>
						<? endif; ?>
					<? endforeach ?>
					<br/>
					<br/>
					<div>
						<? $this->widget('common.widgets.warningStrip.WarningStrip', array(
							'id'=>'initial-score-reset-warning',
							'message'=>Yii::t('course', 'By changing the score calculation formula, all existing scores will be recalculated. Are you sure you want to proceed?'),
						)) ?>
					</div>
				</div>
			</div>
		</div>

	</div>
	<br />
	<div class="coursesettings-grey-box">

		<div class="control-container odd">
			<div class="control-group">
					<span class="control-label">
						<?php echo $form->labelEx( $courseModel, 'final_score_mode', array( 'class' => 'control-label' ) ); ?>
					</span>

				<div class="controls">
					<? foreach ( LearningCourse::getFinalScoreModesList() as $key => $label ): ?>
						<label class="radio">
							<?php echo $form->radioButton( $courseModel, 'final_score_mode', array(
								'value'        => $key,
								'uncheckValue' => NULL
							) ); ?>
							<?= $label ?>
						</label>
						<? if ( $key == LearningCourse::FINAL_SCORE_TYPE_KEY_OBJECT ): ?>
							<?= $form->dropDownList( $courseModel, 'final_object', $courseLos, array(
								'value'        => $courseModel ? $courseModel->final_object : NULL,
								'uncheckValue' => NULL,
								'style'        => 'margin-left: 28px;' . ($courseModel->final_score_mode==LearningCourse::FINAL_SCORE_TYPE_KEY_OBJECT ? '' : 'display:none'),
							) ) ?>
						<? endif; ?>
					<? endforeach ?>

					<div>
						<? $this->widget('common.widgets.warningStrip.WarningStrip', array(
							'id'=>'same-units-warning',
							'message'=>Yii::t('course', 'This final score calculation mode works accurately only if all learning objects inside the course use the same units - points or percent'),
						)) ?>
					</div>

					<br/>

					<div>
						<? $this->widget('common.widgets.warningStrip.WarningStrip', array(
							'id'=>'final-score-reset-warning',
							'message'=>Yii::t('course', 'By changing the score calculation formula, all existing scores will be recalculated. Are you sure you want to proceed?'),
						)) ?>
					</div>

				</div>
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
	$(function(){
		var warningRecalcNeeded = $('.final-score-reset-warning');

		var warningUnitsMissmatch = $('.same-units-warning');

		var warningRecalcNeededInitial = $('.initial-score-reset-warning');

		var warningUnitsMissmatchInitial = $('.same-initial-units-warning');

		warningRecalcNeeded.hide();
		warningUnitsMissmatch.hide();

		warningRecalcNeededInitial.hide();
		warningUnitsMissmatchInitial.hide();

		var finalScoreModeUnchanged = $('input#LearningCourse_final_score_mode:checked').val();
		var initialScoreModeUnchanged = $('input#LearningCourse_initial_score_mode:checked').val();

		$(document).on('change', 'input#LearningCourse_final_score_mode', function(){
			if($(this).val() !== finalScoreModeUnchanged)
				warningRecalcNeeded.show();
			else
				warningRecalcNeeded.hide();

			if($(this).val()==='<?=LearningCourse::FINAL_SCORE_TYPE_KEY_OBJECT?>'){
				$('select#LearningCourse_final_object').show();
			}else{
				$('select#LearningCourse_final_object').hide();
			}

			if($(this).val()==='<?=LearningCourse::FINAL_SCORE_TYPE_AVG_ALL_COMPLETED?>' || $(this).val()==='<?=LearningCourse::FINAL_SCORE_TYPE_SUM_ALL_COMPLETED?>'){
				warningUnitsMissmatch.show();
			}else{
				warningUnitsMissmatch.hide();
			}
		});

		$(document).on('change', 'select#LearningCourse_final_object', function(){
			warningRecalcNeeded.show();
		});

		$(document).on('change', 'input#LearningCourse_initial_score_mode', function(){
			if($(this).val() !== initialScoreModeUnchanged)
				warningRecalcNeededInitial.show();
			else
				warningRecalcNeededInitial.hide();

			if($(this).val()==='<?=LearningCourse::INITIAL_SCORE_TYPE_KEY_OBJECT?>'){
				$('select#LearningCourse_initial_object').show();
			}else{
				$('select#LearningCourse_initial_object').hide();
			}

			if($(this).val()==='<?=LearningCourse::FINAL_SCORE_TYPE_AVG_ALL_COMPLETED?>' || $(this).val()==='<?=LearningCourse::FINAL_SCORE_TYPE_SUM_ALL_COMPLETED?>'){
				warningUnitsMissmatchInitial.show();
			}else{
				warningUnitsMissmatchInitial.hide();
			}
		});

		$(document).on('change', 'select#LearningCourse_initial_object', function(){
			warningRecalcNeededInitial.show();
		});
	})
</script>
<style>
    #same-units-warning {
        height: 72px;
    }
</style>