<?php /* @var $courseModel LearningCourse */ ?>
<?php /* @var $canTogglePublishStatus boolean  */ ?>
<?php if($courseModel->course_type == LearningCourse::TYPE_ELEARNING): ?>
<div id="course-publisher">
	<div id="course-publisher-inner" class="row-fluid">
		<?php if($canTogglePublishStatus ): ?>
		<div class="span4" id="publish-switch">
			<label for="course-mode-unpublish">
				<input type="radio" id="course-mode-unpublish" name="publish-status" value="unpublished" <?php if($courseModel->status != LearningCourse::$COURSE_STATUS_EFFECTIVE): ?>checked="checked"<?php endif; ?>/>
				<span><?php echo Yii::t('standard', 'Unpublished'); ?></span>
			</label>
			<a href="#" id="course-publish-mode-switcher" class="p-sprite switcher <?php if($courseModel->status == LearningCourse::$COURSE_STATUS_EFFECTIVE): ?>active<?php endif; ?>"></a>
			<label class="active" for="course-mode-publish">
				<input type="radio" id="course-mode-publish" name="publish-status" value="published" <?php if($courseModel->status == LearningCourse::$COURSE_STATUS_EFFECTIVE): ?>checked="checked"<?php endif; ?>/>
				<span><?php echo Yii::t('standard', 'Published'); ?></span>
			</label>
		</div>
		<?php endif; ?>

		<div class="span8" id="publish-status">
			<?php if($courseModel->publisher): ?>
				<?php $this->renderPartial('publish/status', array('courseModel' => $courseModel)); ?>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php endif; ?>

