<? /* @var $courseModel LearningCourse */ ?>
<div id="player-bg" class="settings-tab hidden-tab">
	<div class="coursesettings-grey-box">
		<div class="control-container odd">
			<div class="control-group">
				<span class="control-label">
					<?= Yii::t('standard', 'Course autoplay') ?>
				</span>
				<div class="controls">
					<p>
						<label for="resume_autoplay_override">
							<?=CHtml::checkBox('resume_autoplay_override', $courseModel->resume_autoplay!=LearningCourse::RESUME_AUTOPLAY_UNSET)?>
							<?=Yii::t('course', 'Enable custom autoplay for this course')?>
						</label>
					</p>

					<p class="resume_autoplay_override_block">
						<label for="resume_autoplay_enabled">
							<?=CHtml::checkBox('resume_autoplay_enabled', $courseModel->resume_autoplay==LearningCourse::RESUME_AUTOPLAY_ON)?>
							<?=Yii::t('course', 'Directly play first item (or resume the last played object) when entering a course')?>
						</label>
					</p>

					<?=CHtml::activeHiddenField($courseModel, 'resume_autoplay', array(
//						'value'=> (empty($courseModel->resume_autoplay) ? LearningCourse::RESUME_AUTOPLAY_UNSET : $courseModel->resume_autoplay)
					));?>

				</div>
			</div>
		</div>

		<div class="control-container even background-image-for-course">
			<div class="control-group">
				<span class="control-label">
					<?= Yii::t('certificate', '_BACK_IMAGE') ?>
				</span>
				<div class="controls">

					<p>
						<label for="enable_course_bg_image">
							<?=CHtml::checkBox('enable_course_bg_image', $courseModel->player_bg || $courseModel->player_bg_aspect)?>
							<?=Yii::t('course', 'Enable custom background image settings for this course') ?>
						</label>
					</p>

					<br/>

					<input type="hidden" name="<?= get_class($courseModel) . '[player_bg_id]' ?>" id="<?= get_class($courseModel) . '_player_bg_id' ?>" value="<?= $courseModel->player_bg ?>" />

					<div class="course-bg-image-settings-wrapper">
	                    <p><?= Yii::t('templatemanager', 'Current background image') ?></p>
	                    <div class="row-fluid">
	                        <div class="span4">
	                            <div class="current">
	                                <?= $courseModel->renderPlayerBg(CoreAsset::VARIANT_SMALL, true) ?>
	                            </div>
	                        </div>
	                        <div class="span8">
	                            <?= $form->labelEx($courseModel, 'player_bg_aspect', array(
	                                'label' => $form->radioButton($courseModel, 'player_bg_aspect', array('id'=>'player_bg_aspect_opt_a', 'value'=>'fill', 'uncheckValue'=>NULL)) . ' ' . Yii::t('branding', '_HOME_IMG_POSITION_FILL', array('{w}'=>1100, '{h}'=>410)),
	                                'for' => 'player_bg_aspect_opt_a',
	                                'class' => 'radio'
	                            )) ?>
	                            <br>
	                            <?= $form->labelEx($courseModel, 'player_bg_aspect', array(
	                                'label' => $form->radioButton($courseModel, 'player_bg_aspect', array('id'=>'player_bg_aspect_opt_b', 'value'=>'tile', 'uncheckValue'=>NULL)) . ' ' . Yii::t('branding', '_HOME_IMG_POSITION_TILE'),
	                                'for' => 'player_bg_aspect_opt_b',
	                                'class' => 'radio'
	                            )) ?>
	                        </div>
	                    </div>

	                    <br>
	                    <a 	class="open-dialog btn btn-docebo green big"
	                        rel="modal-player-bg"
	                        data-dialog-class="modal-player-bg"
	                        href="<?= Docebo::createLmsUrl('player/coursesettings/axPlayerBg', array('course_id' => $this->course_id)) ?>">
	                        <?= Yii::t('setup', 'Upload your background') ?>
	                    </a>
	                    <?php //echo $form->fileField($courseModel, 'player_bg'); ?>

					</div>

				</div>
			</div>
		</div>
	</div>

    <div class="control-container odd legacy-player-options">
        <div class="control-group">
				<span class="control-label">
					<?= Yii::t('standard', 'Training Material View') ?>
                    <p class="description"><?php echo Yii::t('branding', "Choose the layout to to display your course's training material"); ?></p>
				</span>
            <div class="controls">
                <p>
                    <label for="custom_player_layout_enabled">
						<?=CHtml::checkBox('custom_player_layout_enabled', $courseModel->player_layout)?>
						<?=Yii::t('course', 'Enable custom training material view for this course')?>
                    </label>
                </p>

                <br/>

                <div class="row-fluid player-layout-settings-wrapper">
                    <div class="span4">
                        <label style="margin-left:0">
							<?=$form->radioButton($courseModel, 'player_layout', array('value'=>LearningCourse::$PLAYER_LAYOUT_PLAY_BUTTON, 'uncheckValue'=>NULL))?>
							<?=Yii::t('branding', 'Player view (browser mode)')?>
                        </label>

                        <br/><br/>

                        <div class="player-layout-preview player"></div>
                    </div>
                    <div class="span4">
                        <label style="margin-left:0">
							<?=$form->radioButton($courseModel, 'player_layout', array('value'=>LearningCourse::$PLAYER_LAYOUT_NAVIGATOR, 'uncheckValue'=>NULL))?>
							<?=Yii::t('branding', 'Player view (prev-next mode)')?>
                        </label>

                        <br/><br/>

                        <div class="player-layout-preview navigator"></div>
                    </div>
                    <div class="span4">
                        <label style="margin-left:0">
							<?=$form->radioButton($courseModel, 'player_layout', array('value'=>LearningCourse::$PLAYER_LAYOUT_LISTVIEW, 'uncheckValue'=>NULL))?>
							<?=Yii::t('branding', 'List view')?>
                        </label>

                        <br/><br/>

                        <div class="player-layout-preview listview"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="control-container odd hydra-player-options">
        <div class="control-group">
            <span class="control-label">
                <?= Yii::t('branding', 'Course starting view') ?>
                <p class="description"><?php echo Yii::t('branding', "Decide whether users must directly enter a course or land in a course pre-page before playing the course."); ?></p>
            </span>
            <div class="controls">
                <p>
                    <label for="enable_has_overview">
						<?=CHtml::checkBox('enable_has_overview', !is_null($courseModel->has_overview))?>
						<?=Yii::t('course', 'Enable custom course starting view for this course') ?>
                    </label>
                </p>
                <br/>
                <div id="has_overview_options">
					<?= $form->labelEx($courseModel, 'has_overview', array (
						'label' => $form->radioButton($courseModel, 'has_overview', array('id'=>'has_overview_no', 'value'=>'0', 'uncheckValue'=>NULL)) .
							' ' . Yii::t('branding', 'Directly land on the course player page (one-page view)') .
							'<p class="help-block">'.Yii::t('branding', 'Course widgets will be displayed underneath the course player.').'</p>',
						'for' => 'has_overview_no',
						'class' => 'radio'
					)) ?>
                    <br>
					<?= $form->labelEx($courseModel, 'has_overview', array (
						'label' => $form->radioButton($courseModel, 'has_overview', array('id'=>'has_overview_yes', 'value'=>'1', 'uncheckValue'=>NULL)) . ' ' . Yii::t('branding', 'Course pre-page').
							'<p class="help-block">'.Yii::t('branding', 'Course widgets will be displayed in the pre-page only, so that users will focus on the course content only while playing it.').'</p>',
						'for' => 'has_overview_yes',
						'class' => 'radio'
					)) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="control-container even hydra-player-options">
        <div class="control-group">
			<span class="control-label">
				<?= Yii::t('branding', 'Table of contents default visibility') ?>
                <p class="description"><?php echo Yii::t('branding', "ToC can always be displayed or hidden directly by the user while playing a course."); ?></p>
			</span>
            <div class="controls">
                <p>
                    <label for="enable_show_toc">
						<?=CHtml::checkBox('enable_show_toc', !is_null($courseModel->show_toc))?>
						<?=Yii::t('course', 'Enable custom table of content visibility for this course') ?>
                    </label>
                </p>
                <br/>
                <div id="show_toc_options">
					<?= $form->labelEx($courseModel, 'show_toc', array(
						'label' => $form->radioButton($courseModel, 'show_toc', array('id'=>'show_toc_no', 'value'=>'0', 'uncheckValue'=>NULL)) .
							'<span class="hide-toc-image"></span>'. Yii::t('branding', 'Hide the table of contents when user enters the course'),
						'for' => 'show_toc_no',
						'class' => 'radio'
					)) ?>
                    <br>
					<?= $form->labelEx($courseModel, 'show_toc', array(
						'label' => $form->radioButton($courseModel, 'show_toc', array('id'=>'show_toc_yes', 'value'=>'1', 'uncheckValue'=>NULL)) .
                            '<span class="show-toc-image"></span>'.Yii::t('branding', 'Show the table of contents when user enters the course'),
						'for' => 'show_toc_yes',
						'class' => 'radio'
					)) ?>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
<!--
	function autoplayOverride(){
		if($('#resume_autoplay_override').is(':checked')){
			$('.resume_autoplay_override_block').css('opacity', '');
			$('#resume_autoplay_enabled').prop('disabled', false).trigger('refresh');
		}else{
			$('.resume_autoplay_override_block').css('opacity', '0.5');
			$('#resume_autoplay_enabled').prop('disabled', true).trigger('refresh');
		}
	}
	$(document).ready(function(){
		$('input').styler();

		//initial check for the override option checkboxes

		autoplayOverride();

		// Hide the "Background image" setting if the player type
		// for this course is List View
		$('input[name="LearningCourse[player_layout]"]').change(function(){
			if($(this).val() === '<?=LearningCourse::$PLAYER_LAYOUT_LISTVIEW?>')
				$('.background-image-for-course').hide();
			else
				$('.background-image-for-course').show();

			$('#enable_course_bg_image').trigger('change');
		});

		if($('input[name="LearningCourse[player_layout]"]:checked').val()==='<?=LearningCourse::$PLAYER_LAYOUT_LISTVIEW?>'){
			$('.background-image-for-course').hide();
			$('#enable_course_bg_image').trigger('change');
		}

		$('#custom_player_layout_enabled').on('change', function(){
			if($(this).is(':checked')){
				$('.player-layout-settings-wrapper').css('opacity', '1');
				var hasChecked = $('input[name="LearningCourse[player_layout]"]:checked').length > 0;
				if(hasChecked) {
					// There is already a selected radio
					// Don't modify that selection programmatically
					return;
				} else {
					// The user has not selected any option for player layout
					// so select the platform default by default
					$('#player_layout_opt_a').attr('checked', true).trigger('refresh');
					$('.background-image-for-course').show();
				}
			} else {
				$('.player-layout-settings-wrapper').css('opacity', '0.5');
				$('input[name="LearningCourse[player_layout]"]').attr('checked', false).trigger('refresh');
			}
		}).trigger('change');

		$('#enable_course_bg_image').change(function(){
			var el = $('#<?= get_class($courseModel) . '_player_bg_id'?>');

			if($(this).is(':checked')){
				if(el.val()==='' && el.data('lastSelection')) {
					el.val(el.data('lastSelection'));
					el.data('lastSelection', '');
				}

				var hasChecked = $('input[name="LearningCourse[player_bg_aspect]"]:checked').length > 0;
				if(hasChecked) {
					// There is already a selected radio
					// Don't modify that selection programmatically
					return;
				} else {
					// The user has not selected any option for player bg aspect
					// so select the platform default by default
					$('#player_bg_aspect_opt_a').attr('checked', true).trigger('refresh');
				}

				$('.course-bg-image-settings-wrapper').css('opacity', '1');
			}else{
				el.data('lastSelection', el.val());
				el.val('');
				$('.course-bg-image-settings-wrapper').css('opacity', '0.5');
				$('input[name="LearningCourse[player_bg_aspect]"]').attr('checked', false).trigger('refresh');
			}
		}).trigger('change');

        $('#enable_has_overview').change(function(){
            if($(this).is(':checked')) {
                $("#has_overview_options").css('opacity', '1');
                $('#has_overview_options input[type="radio"]').prop("disabled", false).trigger('refresh');
            } else {
                $("#has_overview_options").css('opacity', '0.5');
                $('#has_overview_options input[type="radio"]').prop("disabled", true).trigger('refresh');
            }
        }).trigger('change');

        $('#enable_show_toc').change(function(){
            if($(this).is(':checked')) {
                $("#show_toc_options").css('opacity', '1');
                $('#show_toc_options input[type="radio"]').prop("disabled", false).trigger('refresh');
            } else {
                $("#show_toc_options").css('opacity', '0.5');
                $('#show_toc_options input[type="radio"]').prop("disabled", true).trigger('refresh');
            }
        }).trigger('change');

		$('#resume_autoplay_override, #resume_autoplay_enabled').change(function(){
			autoplayOverride();

			var enableAutoplay = $('#resume_autoplay_override');
			var resumeAutoplay = $('#resume_autoplay_enabled');
			var hiddenField = $('#LearningCourse_resume_autoplay');

			if(enableAutoplay.is(':checked') && resumeAutoplay.is(':checked')) {
				hiddenField.val(<?=LearningCourse::RESUME_AUTOPLAY_ON?>);
			} else if(enableAutoplay.is(':checked') && !resumeAutoplay.is(':checked')) {
				hiddenField.val(<?=LearningCourse::RESUME_AUTOPLAY_OFF?>);
			} else {
				hiddenField.val(<?=LearningCourse::RESUME_AUTOPLAY_UNSET?>);
			}
		});

	});

	
//-->
</script>