<div id="coaching" class="settings-tab hidden-tab player-settings-form">
	<div class="coursesettings-grey-box">
		<div class="control-container odd">
			<div class="control-group">
				<?php echo $form->labelEx($courseModel, 'enable_coaching', array('class'=>'control-label')); ?>
				<div class="controls">
					<label class="checkbox">
						<?php echo $form->checkBox($courseModel, 'enable_coaching'); ?>
						<?php echo Yii::t('coaching', 'Enable coaching for this course'); ?>
					</label>
				</div>
			</div>
		</div>

		<div class="blockable"> <!-- blockable -->
			<div class="control-container">
				<div class="control-group">
					<span class="control-label"><?= Yii::t("coaching", "Learners Assignment") ?></span>
					<div class="controls">
                        <div class="row-fluid">
                            <div class="span12">
                                <label class="radio">
                                    <?php echo $form->radioButton($courseModel, 'learner_assignment', array('value' => $courseModel::COACHING_LEARNER_ASSIGNMENT_AUTOMATICALLY_MAXIMIZED, 'uncheckValue' => NULL)); ?>
                                    <?= Yii::t('coaching', "Automatically by maximizing by session's usage")?>
                                </label>
                                <div class="setting-description styler indented"><?= Yii::t('coaching', "Learner will be automatically assigned to a coaching session (fill a session before starting with the next one)"); ?></div>
                            </div>
                        </div>						

                        <div class="row-fluid">
                            <div class="span12">
                                <label class="radio">
                                    <?php echo $form->radioButton($courseModel, 'learner_assignment', array('value' => $courseModel::COACHING_LEARNER_ASSIGNMENT_AUTOMATICALLY_BALANCED, 'uncheckValue' => NULL)); ?>
                                    <?php echo Yii::t('coaching', "Automatically by balancing coaching assignments")?>
                                </label>
                                <div class="setting-description styler indented"><?= Yii::t('coaching', "Learner will be automatically assigned to a coaching session (balance users among sessions)"); ?></div>
                            </div>
                        </div>                        						
                        <div class="row-fluid">
                            <div class="span12">
                                <label class="radio">
                                    <?php echo $form->radioButton($courseModel, 'learner_assignment', array('value' => $courseModel::COACHING_LEARNER_ASSIGNMENT_LEARNER_SELECTED, 'uncheckValue' => NULL)); ?>
                                    <?php echo Yii::t('coaching', "Learners must select their preferred coaching session")?>
                                </label>
                                <div class="setting-description styler indented"><?= Yii::t('coaching', "Learners must select a coaching session at first course access"); ?></div>
                            </div>
                        </div>						

                        <div class="row-fluid">
                            <div class="span12">
                                <label class="radio" style="display: block;">
                                    <?php echo $form->radioButton($courseModel, 'learner_assignment', array('value' => $courseModel::COACHING_LEARNER_ASSIGNMENT_ADMIN_SELECTED, 'uncheckValue' => NULL)); ?>
                                    <?php echo Yii::t('coaching', "Manually by the admins"); ?>
                                </label>
                                <div class="setting-description styler indented"><?= Yii::t('coaching', "Admins will manually assign learners to coaching sessions"); ?></div>
                            </div>
                        </div>
                        
                        
                        

					</div>
				</div>
			</div>

			<div class="control-container odd">
				<div class="control-group">
					<span class="control-label">
						<?= Yii::t('admin', 'Options') ?>
					</span>
					<div class="controls">						
                        <div class="row-fluid">
                            <div class="span12">
                                <label for="coaching_option_allow_start_course" class="checkbox">
                                    <?=CHtml::checkBox('coaching_option_allow_start_course', $courseModel->coaching_option_allow_start_course)?>
                                    <?=Yii::t('coaching', 'Allow learners to start the course even if the coaching session is scheduled for the future')?>
                                </label>
                            </div>
                        </div>


                        <div class="row-fluid">
                            <div class="span12">
                                <label for="coaching_option_allow_request_session_change" class="checkbox">
                                    <?=CHtml::checkBox('coaching_option_allow_request_session_change', $courseModel->coaching_option_allow_request_session_change)?>
                                    <?=Yii::t('coaching', 'Allow learners to request a coaching session change')?>
                                </label>
                                <div class="setting-description styler indented"><?=Yii::t('coaching', 'Learners will be able to contact the administrator in order to change their assigned coaching session while taking the course')?></div>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span12">
                                <label for="coaching_option_allow_request_session_change" class="checkbox">

                                    <?=CHtml::checkBox('coaching_option_access_without_assign_coach', $courseModel->coaching_option_access_without_assign_coach)?>

                                    <?=Yii::t('coaching', 'Allow users to enter the course even if they do not have an assigned coaching session')?>
                                </label>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div> <!-- blockable -->

	</div>
</div>

<script type="text/javascript">
	(function($) {
		CoachingManager = new CoachingManagerClass();
	})(jQuery)
</script>
