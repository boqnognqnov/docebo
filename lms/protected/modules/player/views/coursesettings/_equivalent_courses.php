<style>
    #equivalent_courses .controls a {
        float: right;
    }

    #equivalent_courses div.controls > input[type="text"], #equivalent_courses ul.holder {
        display: inline-block;
    }
    #equivalent-courses-grid table.items, #equivalent-courses-grid, #equivalent-courses-grid .gridItemsContainer{
        background-color: #f1f3f2;
    }
    #equivalent-courses-grid .gridItemsContainer{
        padding: 20px;
        margin-top: 15px;
    }
    .deleteEquivalent{
        background-image: url(<?=Yii::app()->theme->baseUrl.'/images/icons_elements.png'?>);
        background-position: -82px -227px;
        width: 21px;
        height: 21px;
        display: inline-block;
    }
    #equivalent-courses-grid .gridItemsContainer table.items th, #equivalent-courses-grid .gridItemsContainer table.items td{
        text-align: center;
    }
</style>

<div id="equivalent_courses" class="settings-tab hidden-tab player-settings-form">
    <div class="coursesettings-grey-box">
        <div class="control-container odd">
            <div class="control-group">
            <span class="control-label">
                <?= Yii::t('course', 'Course equivalencies') ?>
                <p class="setting-description">
                    <?= Yii::t('course',
                        'Select one or more courses to be considered as "equivalent" to the current course') ?>
                </p>
            </span>
                <div class="controls">
                    <?= CHtml::textField('equivalencies', '') ?>
                    <?= CHtml::link(Yii::t('course', 'Add course'),
                        Docebo::createAbsoluteUrl('player/coursesettings/addCourseEquivalents',
                            array('course_id' => $courseModel->idCourse)),
                        array('class' => 'btn-docebo big green', 'id'=>'addCoursesLink')); ?>
                </div>
            </div>

            <!--    Grid-->
            <div>
                <?php
                $dataProvider = EquivalentCourse::dataProvider($courseModel);

                // array of idCourse, code, name, type, bidirectional
                $equivalenceData = $dataProvider->rawData;

                // holds just id_course as key and if it is bidirectional as a value /0, 1/ - will need in JS
                $jsData = [];
                foreach ($equivalenceData as $item) {
                    $jsData[$item['idCourse']] = (int)$item['bidirectional'];
                }

                $this->widget('DoceboCGridView', array(
                    'id' => 'equivalent-courses-grid',
                    'htmlOptions' => array('class' => 'grid-view clearfix'),
                    'dataProvider' => $dataProvider,
                    'columns' => array(
                        array(
                            'name' => Yii::t('standard', '_CODE'),
                            'value' => '$data["code"]'
                        ),
                        array(
                            'name' => Yii::t('standard', '_COURSE_NAME'),
                            'value' => function($data){
                                echo CHtml::link($data['name'], Docebo::createAbsoluteUrl('player/coursesettings',
                                    array('course_id'=>$data['idCourse'],'ref_src'=>'admin')).'#equivalent_courses',array('style'=>'text-decoration: underline'));
                            }
                        ),
                        array(
                            'name' => Yii::t('standard', '_TYPE'),
                            'value' => '$data["type"]'
                        ),
                        array(
                            'name' => Yii::t('course', 'Bidirectional'),
                            'value' => function ($data) {
                                echo CHtml::checkBox('bidirectional[' . $data['idCourse'] . ']',
                                    ($data['bidirectional'] == 1),
                                    array(
                                        'uncheckValue' => 0,
                                        'class' => 'bidirectionalValue',
                                        'data-id' => $data['idCourse']
                                    ));
                            }
                        ),
                        array(
                            'name' => '',
                            'value' => function ($data) {?>
                                <i class="deleteEquivalent" style="cursor: pointer"
                                   data-id="<?= $data['idCourse'] ?>"></i>
                            <?php }
                        )
                    ),
                    'template' => '{items}{summary}{pager}',
                    'summaryText' => Yii::t('standard', '_TOTAL'),
                    'afterAjaxUpdate' => 'function(){
                        $("#equivalent_courses input").styler();
                        updateHiddenField();
                        $.each(jsData, function (i, e) {
                            if (e == 1 && !$(\'input[name="bidirectional[\' + i + \']"]\').is(\':checked\')
                                || e == 0 && $(\'input[name="bidirectional[\' + i + \']"]\').is(\':checked\')) {
                                $(\'input[name="bidirectional[\' + i + \']"]\').next(\'span\').trigger(\'click\');
                            }
                        })
                    }'
                ));
                ?>
                <input type="hidden" id="equivalent-courses-data" name="equivalent-courses-data"/>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    'use strict';

    var jsData = <?=json_encode((object)$jsData)?>;

    function updateHiddenField(){
        $('input#equivalent-courses-data').val(JSON.stringify(jsData));
    };

    $(function () {
        updateHiddenField();
        $('#equivalencies').fcbkcomplete({
            width: "68%",
            maxitems: 100,
            json_url: "<?= Docebo::createAbsoluteUrl('player/coursesettings/fcbkAutocomplete', array(
                'course_id' => $courseModel->idCourse
            ))?>",
            filter_selected: true,
            cache: false,
            complete_text: '',
            placeholder: '<?=Yii::t('standard', 'Type here...')?>'
        });

        // add courses to the grid
        $(document).on('click', '#equivalent_courses a#addCoursesLink', function (e) {
            e.preventDefault();
            e.stopPropagation();

            // get selected courses and save them in jsData
            var selectedCourses = $('#equivalencies option');
            var selectedCoursesIds = [];
            $.each(selectedCourses, function (i, e) {
                var courseId = $(e).val();
                selectedCoursesIds.push(courseId);
                jsData[courseId] = 0;
            });

            $.ajax({
                type: 'POST',
                url: $(this).prop('href'),
                data: {selectedCoursesIds: selectedCoursesIds}
            }).success(function (data) {
                data = JSON.parse(data);

                $('#equivalent_courses ul.holder li.bit-box').remove();
                $('#equivalent-courses-grid').yiiGridView('update');
                setTimeout(function () {
                    // clear bidirectional flags, for newly selected course relations
                    for (var i = 0; i < data.length; i++) {
                        if ($('input[name="bidirectional[' + data[i] + ']"]').is(':checked'))
                            $('input[name="bidirectional[' + data[i] + ']"]').next('span').trigger('click')
                    }
                }, 3000);
            });
        });

        //delete course from the grid
        $(document).on('click', '#equivalent_courses .deleteEquivalent', function () {
            var courseId = $(this).data('id');
            $.ajax({
                type:'POST',
                url: '<?=Docebo::createAbsoluteUrl('player/coursesettings/deleteCourseEquivalent',
                    array(
                    'course_id' => $courseModel->idCourse,
                    ))?>',
                data: {courseRemoveId: courseId}
            }).success(function(data){
                $('#equivalencies option[value="'+courseId+'"]').remove();
                delete jsData[courseId];
                $('#equivalent-courses-grid').yiiGridView('update');
            });
        });

        //change direction of the current relation
        $(document).on('change', '.bidirectionalValue', function () {
            var isChecked = $(this).is(':checked');
            var courseId = $(this).data('id');
            if (isChecked) {
                jsData[courseId] = 1;
            } else {
                jsData[courseId] = 0;
            }
            updateHiddenField();
        });
    });
</script>