<?php
/* @var $this ReportController */
/* @var $deliverable LearningDeliverable */
/* @var $organizationModel LearningOrganization */

?>

<style type="text/css">
    .modal-deliverable-user-history {}
</style>

<h1><?= $deliverable->title ?></h1>

<div class="modal-content">
    <div class="row-fluid docebo-form">
        <div class="span6 control-container odd">
            <div class="split-container control-group" style="height: 28px;padding: 17px 20px;">
                <span class="status-icon">
	                <?php if($organizationModel->learningCommontrack->objectType == LearningOrganization::OBJECT_TYPE_DELIVERABLE && !$organizationModel->learningCommontrack->getEvaluatedDeliverableObjectByUser()) :?>
		                <span class="i-sprite is-circle-check large orange"></span>
	                 <?php else: ?>
		                 <?php if ($organizationModel->learningCommontrack->status == LearningCommontrack::STATUS_COMPLETED): ?>
			                <span class="i-sprite is-circle-check large green"></span>
		                <?php elseif ($organizationModel->learningCommontrack->status == LearningCommontrack::STATUS_FAILED): ?>
			                <span class="i-sprite is-remove large red"></span>
		                <?php else: ?>
			                <span class="i-sprite is-circle-check large grey"></span>
		                <?php endif; ?>
	                <?php endif; ?>

                </span>

                <span class="status-label"><?= $organizationModel->learningCommontrack->getStatusLabel() ?></span>
            </div>
        </div>
        <div class="span6 control-container odd">
            <div class="split-container control-group" style="padding: 8px 20px;">
                <div class="score-label" style="font-size: 2em;font-weight: bold;line-height: 1em;">
                    <?= $organizationModel->learningCommontrack->renderScore() ?>
                </div>
                <label style="margin: 0;"><?= Yii::t('standard', '_SCORE') ?></label>
            </div>
        </div>
    </div>

    <?php
    $this->widget('DoceboCGridView', array(
        'id' => 'player-reports-deliverables-grid',
        'dataProvider' => new CArrayDataProvider($deliverable->deliverableObjects, array(
            'id' => 'deliverable-objects',
            'keyField' => 'idObject'
        )),
        'columns' => array(
            array(
                'name' => Yii::t('deliverable', 'Video name'),
                'value' => 'CHtml::encode($data->name)',
                'type' => 'raw',
            ),
            array(
                'name' => Yii::t('deliverable', 'Submitted on'),
                'value' => '$data->created',
                'type' => 'raw',
                'htmlOptions' => array('class' => 'text-center'),
                'headerHtmlOptions' => array('class' => 'text-center'),
            ),
            array(
                'name' => Yii::t('standard', '_STATUS'),
                'value' => '$data->renderStatusIcon()',
                'type' => 'raw',
                'htmlOptions' => array('class' => 'text-center'),
                'headerHtmlOptions' => array('class' => 'text-center'),
            ),
            array(
                'name' => Yii::t('standard', '_SCORE'),
                'value' => '$data->renderScore()',
                'type' => 'raw',
                'htmlOptions' => array('class' => 'text-center'),
                'headerHtmlOptions' => array('class' => 'text-center'),
            ),
            array(
                'name' => Yii::t('deliverable', 'View/Evaluate'),
                'value' => array($this, 'reportGridRenderDeliverableOptions'),
                'type' => 'raw',
                'htmlOptions' => array('class' => 'text-center'),
                'headerHtmlOptions' => array('class' => 'text-center'),
            ),
        ),
    )); ?>
</div>

<script type="text/javascript">
    $(document).delegate('.modal-deliverable-user-history', 'dialog2.content-update', function() {
        var e = $(this);

        e.find('.modal-body a').click(function() {
            e.find('.modal-body').dialog2("close");
        });

        $(document).controls();
    });
</script>