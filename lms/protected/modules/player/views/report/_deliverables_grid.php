<?php
/* @var $this ReportController */
?>
<div class="row-fluid">
    <div class="player-grey-box">
        <div id="enrollmentAvailableSearch">
            <form id="reportSearchDeliverablesForm" class="player-search-form" action="<?=$this->createUrl('index', array('course_id'=>$this->course_id))?>" onsubmit="Report.searchDeliverables();return false;">
                <?=CHtml::button("searchSubmit", array('value'=>'', 'onclick'=>'Report.searchDeliverables()'));?>
                <?=CHtml::activeTextField($deliverablesModel, 'search_input');?>

                <div class="player-deliverables-filter">
					<?php if($showCoachingSessionSelect): ?>
						<div class="coaching-session pull-left"><div><?=Yii::t('course', 'Coaching Session') ?>:</div><?php echo CHtml::dropDownList('coaching_session_id', '', $coachSessions); ?> </div>
					<?php endif; ?>
                    <label class="radio" for=""><input type="radio" name="<?= CHtml::activeName(new LearningDeliverableObject(), 'search_status') ?>" value="evaluated" id="deliverable_status_evaluated" /> <?= Yii::t('classroom', 'Already evaluated') ?></label>
                    <label class="radio" for=""><input type="radio" name="<?= CHtml::activeName(new LearningDeliverableObject(), 'search_status') ?>" id="deliverable_status_pending" value="pending" /> <?= Yii::t('classroom', 'Waiting for evaluation') ?></label>
                    <label class="radio" for=""><input type="radio" name="<?= CHtml::activeName(new LearningDeliverableObject(), 'search_status') ?>" id="deliverable_status_all" value="all" checked /> <?= Yii::t('standard', '_ALL') ?></label>
                </div>

            </form>
        </div>
    </div>
</div>
<br>
<?php
$this->widget('DoceboCGridView', array(
	'id' => 'player-reports-deliverables-grid',
	'dataProvider' => $deliverablesModel->dataProvider(),
	'columns' => array(
		array(
			'name' => Yii::t('standard', '_USERNAME'),
			'value' => 'CHtml::link($data->user->getClearUserId(), array("byUser", "course_id"=>$_GET["course_id"], "user_id"=>$data->user->primaryKey), array("class"=>"grid-link"))',
			'type' => 'raw',
		),
		array(
			'name' => Yii::t('standard', '_FIRSTNAME'),
			'value' => 'CHtml::link($data->user->firstname, array(), array())',
			'type' => 'raw',
		),
		array(
			'name' => Yii::t('standard', '_LASTNAME'),
			'value' => 'CHtml::link($data->user->lastname, array(), array())',
			'type' => 'raw',
		),
		array(
			'name' => Yii::t('deliverable', '_LONAME_deliverable'),
			'value' => '$data->deliverable->title',
			'type' => 'raw',
		),
		array(
			'name' => Yii::t('deliverable', 'Submitted on'),
			'value' => '$data->created',
			'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'text-center'
            ),
            'headerHtmlOptions' => array(
                'class' => 'text-center',
            ),
		),
		array(
			'name' => Yii::t('standard', '_STATUS'),
			'value' => array($this, 'reportGridRenderDeliverableStatus'),
			'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'text-center'
            ),
            'headerHtmlOptions' => array(
                'class' => 'text-center',
            ),
		),
		array(
			'name' => Yii::t('standard', '_SCORE'),
			'value' => '$data->getScore()',
			'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'text-center'
            ),
            'headerHtmlOptions' => array(
                'class' => 'text-center',
            ),
		),
        array(
            'name' => Yii::t('deliverable', 'View/Evaluate'),
            'value' => array($this, 'reportGridRenderDeliverableOptions'),
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'text-center'
            ),
            'headerHtmlOptions' => array(
                'class' => 'text-center',
            ),
        ),
	),
    'afterAjaxUpdate' => 'function(id, data){
        $(\'a[rel="tooltip"]\').tooltip();
        $(document).controls();
    }'
)); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('.player-deliverables-filter input').styler();


		$(document).on('change', 'select[name="coaching_session_id"]', function(){
			Report.searchDeliverables();
		});     
	
        
    });
</script>