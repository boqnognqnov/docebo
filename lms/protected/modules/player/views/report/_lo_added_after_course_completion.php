<style>
	.modal {
		width: 700px;
		margin-left: -350px;
	}
</style>
<h1><?=Yii::t('stats', 'Learning Objects added after the user completed the course:')?></h1>
<?php $this->widget('DoceboCGridView', array(
	'id' => 'lo-added-after-course-completion-grid',
	'htmlOptions' => array('class' => 'grid-view clearfix'),
	'dataProvider' => $dataProvider,
	'template' => '{items}{pager}',
	'pager' => array(
		'class' => 'DoceboCLinkPager',
		'maxButtonCount' => 8,
	),
	'columns' => array(
		array(
			'header' => Yii::t('standard', 'Learning Object'),
			'value' => '$data["title"]',
		),

		array(
			'header' => Yii::t('stats', 'Added:'),
			'value' => '$data["dateInsert"] ? Yii::app()->localtime->toLocalDateTime($data["dateInsert"]) : "-"',
		),
	),
)); ?>

<div class="form-actions">
	<?= CHtml::button(Yii::t('standard', '_CLOSE'), array('class' => 'btn btn-docebo black big close-dialog')); ?>
</div>