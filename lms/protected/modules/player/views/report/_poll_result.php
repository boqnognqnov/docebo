<div class="row-fluid no-space" style="width: 950px;">
	<div class="player-grey-box">
		<div class="span6" id="report-export">
			<label><?=Yii::t('standard', 'Export as')?></label>
			<?=CHtml::dropDownList('export_report', '',  LearningCourseuser::exportTypes2(), array('class'=>'exportType'))?>
			<a href="javascript:;" onclick="Report.exportReport(this, '', '<?=$this->createUrl('pollResultExport', array('course_id' => $this->course_id, 'organizationId' => $organizationId, 'objectId' => $poll->id_poll))?>')" class="p-sprite download-transparent-black"></a>
		</div>
		<div class="poll-participants-count">
			<strong><?=Yii::t('standard', '_ATTENDANCE')?>:</strong> <?=count($pollTrack)?>
		</div>
	</div>
	<? foreach ($poll->questionsSequence as $i => $question) : ?>
		<? if (
            $question->type_quest != LearningPollquest::POLL_TYPE_BREAK_PAGE
            && in_array($question->type_quest, LearningPollquest::getPollTypesList()) /* check is widget for this type exists */
            ) : ?>
			<? if ($question->type_quest == LearningPollquest::POLL_TYPE_TITLE) : ?>
				<div class="span12 test-details-type-title"><span><?=$question->title_quest?></span></div>
			<? else : ?>
				<div class="span12 test-details-<?=$i%2 ? 'grey' : 'white'?>-box">
					<div class="span2"><?=isset($pollTranslation[$question->type_quest]) ? $pollTranslation[$question->type_quest] : ''?></div>
					<div class="span10">
						<strong class="test-details-question-title"><?=$i+1?>) <?=$question->title_quest?></strong><br><br>
						<?
						$this->widget('player.components.widgets.PollResultFormatter', array(
							'question' => $question,
							'questId' => $question->id_quest,
							'questionType' => $question->type_quest,
							'pollTrackIds' => $pollTrack,
							'bgColor' => $i%2 ? 'f1f3f2' : 'f9f9f9',
						));
						?>
					</div>
				</div>
			<? endif; ?>
		<? endif; ?>
	<? endforeach ?>

</div>