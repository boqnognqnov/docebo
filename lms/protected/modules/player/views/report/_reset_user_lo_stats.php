<?
/* @var $this ReportController */
?>

<h1><?=Yii::t('standard', '_RESET')?></h1>

<div class="form">

	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'core-enroll-rule-form',
		'htmlOptions' => array(
			'class' => 'ajax',
		)
	)); /* @var $form CActiveForm */ ?>


	<?php
		// If Resetting the status will harm to the users them let's tell that to the Admin
		if($willHarm) {
			$this->widget('common.widgets.warningStrip.WarningStrip', array (
				'message'=> Yii::t('standard', "This change will also affect <strong>{n} other course(s)</strong> where the selected user is enrolled in.", array('{n}' => $willHarm) ),
				'type'=>'warning'
			));
		}
	?>

	<?=CHtml::hiddenField('object_id', $object_id)?>
	<?=CHtml::hiddenField('user_id', $user_id)?>
	<?=CHtml::hiddenField('course_id', $course_id)?>

	<div class="clearfix">
		<?=CHtml::checkbox('confirm', false, array(
			'class'=>'pull-left'
		))?>
		<?=CHtml::label(Yii::t('standard','Yes, I confirm I want to proceed'), 'confirm')?>
	</div>

	<div class="form-actions">
		<?php echo CHtml::submitButton(Yii::t('standard','_CONFIRM'), array('class' => 'btn btn-docebo green big')); ?>
		<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-docebo black big close-dialog')); ?>
	</div>

	<? $this->endWidget(); ?>

</div><!--/.form-->