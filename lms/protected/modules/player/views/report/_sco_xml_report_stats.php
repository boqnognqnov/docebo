<?php
/** @var $this ReportController */
?>

<div class="row-fluid">
	<div class="player-grey-box">
		<div class="span6" id="report-export">
			<label><?=Yii::t('standard', 'Export as')?></label>
			<?=CHtml::dropDownList('export_report', '',  LearningCourseuser::exportTypes2(), array('class'=>'exportType'))?>
			<a href="javascript:;" onclick="Report.exportReport(this, 'reportSearchScoXml', '<?=$this->createUrl('scoXmlExport', array('course_id'=>$this->course_id, 'objectId'=>$trackingModel->idscorm_item, 'user_id'=>$_GET['user_id']))?>')" class="p-sprite download-transparent-black"></a>
		</div>
		<div id="enrollmentAvailableSearch">
			<form id="reportSearchScoXml" class="player-search-form" onsubmit="Report.search('sco-xml-report', 'reportSearchScoXml');return false;">
				<?=CHtml::button("searchSubmit", array('value'=>'', 'onclick'=>'Report.search("sco-xml-report", "reportSearchScoXml")'));?>
				<?=CHtml::activeTextField($trackingModel, 'search_input');?>
			</form>
		</div>
	</div>
</div>
<br>
<div class="row-fluid">
	<div class="span3">
		<div class="player-grey-box">
			<?
			if ($scormItemsTrackModel->status == LearningScormItemsTrack::STATUS_COMPLETED || $scormItemsTrackModel->status == LearningScormItemsTrack::STATUS_PASSED)
				$icon = 'circle-check-large-green';
			else if ($scormItemsTrackModel->status == LearningScormItemsTrack::STATUS_INCOMPLETE)
				$icon = 'circle-check-large-orange';
			else if($scormItemsTrackModel->status == LearningScormItemsTrack::STATUS_FAILED)
				$icon = 'lo-failed-icon-big';
			else
				$icon = 'circle-check-large-grey';
			?>
			<div class="row-fluid">
				<div class="span4 text-center">
					<div class="p-sprite <?=$icon?> sco-xml-icon"></div>
				</div>
				<div class="span8">
					<div class="sco-xml-infobox">
						<div class="sco-xml-title-big"><?=ucfirst($scormItemsTrackModel->status)?></div>
						<div class="sco-xml-title-small"><?=Yii::t('standard', '_STATUS')?></div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="span3">
		<div class="player-grey-box">
			<div class="row-fluid">
				<div class="span4 text-center">
					<div class="p-sprite clock-large-black sco-xml-icon"></div>
				</div>
				<div class="span8">
					<div class="sco-xml-infobox">
						<div class="sco-xml-title-big text-colored"><?=Yii::app()->localtime->hoursAndMinutes($trackingModel->totalTimeInSeconds)?></div>
						<div class="sco-xml-title-small"><?=Yii::t('standard', '_TOTAL_TIME')?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="span3">
		<div class="player-grey-box">
			<div class="row-fluid">
				<div class="span4 text-center">
					<div class="p-sprite check-solid-large-black sco-xml-icon"></div>
				</div>
				<div class="span8">
					<div class="sco-xml-infobox">
						<h1 class="sco-xml-title-big text-colored" style="display: block !important;"><?= number_format($trackingModel->score_raw,2) ?>/<?= number_format($trackingModel->score_max,2) ?></h1>
						<div class="sco-xml-title-small"><?=Yii::t('standard', '_SCORE')?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="span3">
		<div class="player-grey-box">
			<div class="row-fluid">
				<div class="span4 text-center">
					<div class="p-sprite clock-large-black sco-xml-icon"></div>
				</div>
				<div class="span8">
					<div class="sco-xml-infobox">
						<div class="sco-xml-title-big text-colored"><?php
							if($trackingModel->last_access) {
								$utcLastScoAccess = Yii::app()->localtime->fromLocalDateTime($trackingModel->last_access);
								$utcLastScoAccessTimestamp = strtotime($utcLastScoAccess) * 1000;
								echo $utcLastScoAccessTimestamp;
							}
						?></div>
						<div class="sco-xml-title-small"><?=Yii::t('standard', 'SCO Timestamp')?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php

$this->widget('DoceboCGridView', array(
	'id' => 'sco-xml-report',
	'dataProvider' => $trackingModel->getTrackingArray(),
	'columns' => array(
		'cmi',
		'title',
	),
)); ?>
</body>