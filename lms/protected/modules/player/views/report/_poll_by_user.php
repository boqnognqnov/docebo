<? if ($pollTrack) : ?>
<div class="l-testpoll review-dialog review-answers">
	<div class="row-fluid">
		<div class="span2">
			<a href="javascript:;" class="test-details-print" onclick="window.print()" style="display: none;">
				<?=CHtml::image($this->module->assetsUrl.'/images/print.png')?>
			</a>&nbsp;
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="block-hr"></div>
	<!-- answers -->
	<div class="row-fluid"><?php

		$answeredQuestions = $pollTrack->getAnsweredQuestions();

		foreach ($answeredQuestions as $i => $question) : ?>
			<?php if ($question['type_quest'] == LearningPollquest::POLL_TYPE_TITLE) : ?>
				<div class="span12 test-details-type-title" style="margin-left: 0px;"><span><?=$question['title_quest']?></span></div>
			<?php else : ?>
				<div class="block <?= (!($i%2)) ? 'odd' : '' ?>">
					<div class="row-fluid">
						<div class="span2"><?=isset($questionsLang[$question['type_quest']]) ? $questionsLang[$question['type_quest']] : '-'?></div>
						<div class="span10">
							<div class="test-details-question-title">
								<span class="pull-left"><?=$i+1?>)&nbsp;</span> <?=$question['title_quest']?>
							</div>
							<?php
							$this->widget('player.components.widgets.PollReportsAnswerFormatter', array(
								'questId' => $question['id_quest'],
								'trackId' => $pollTrack->getPrimaryKey(),
								'userId' => $userId,
								'questionType' => $question['type_quest']
							));
							?>
						</div>
					</div>
					<div style="clear:both"><!-- this is needed by IE8 --></div>
				</div>
				<div class="block-hr"></div>
			<? endif; ?>
		<? endforeach ?>
	</div>
	<? else : ?>
		<div style="margin: 50px;"><?=Yii::t('standard', '_NO_CONTENT')?></div>
	<? endif; ?>
	<div class="form-actions">
		<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CLOSE'); ?>"/>
	</div>
</div>
<script>
	(function() {
		$(document).on('click', '.fancybox-inner .close-dialog', function(){
			$.fancybox.close();
			return false;
		});
	})();
</script>