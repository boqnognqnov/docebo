<?php
/** @var $this ReportController */
/** @var $interactions array */

$_index = 0;
$_questionsLang = LearningTestquest::getQuestionTypesTranslationsList();
$_allowedTypes = LearningScormItems::getAllowedInteractionTypes();

foreach ($interactions as $interaction) :

	if (!in_array($interaction['type'], array('true-false', 'choice', 'fill-in', 'long-fill-in', 'matching', 'numeric', 'sequencing', 'other')))
		continue;

	$_index++;

	$type = $interaction['type'];
	$typeLabel = isset($_allowedTypes[$type]) ? $_allowedTypes[$type] : (
					isset($_questionsLang[$type]) ? $_questionsLang[$type] : $type
				);
	?>

	<?php if (!($_index%2)) : ?>
		<div class="block-hr"></div>
	<?php endif; ?>

	<div class="block <?= (!($_index%2)) ? 'odd' : '' ?>">

		<div class="row-fluid">
			<div class="span2">
				<?= $typeLabel
				?>
			</div>
			<div class="span10">
				<div class="test-details-question-title">
					<span class="pull-left"><?=$_index.')'?></span>&nbsp;<?= (!empty($interaction['description']) ? $interaction['description'] : $interaction['id']) ?>
				</div>
				<?php
				$this->widget('player.components.widgets.ReportsInteractionFormatter', array(
					'interaction' => $interaction,
				));
				?>
			</div>
		</div>
	</div>

	<?php if (!($_index%2)) : ?>
		<div class="block-hr"></div>
	<?php endif; ?>

<?php endforeach; ?>

