<?php
/** @var $this ReportController */
/** @var $userId string */
/** @var $objectId string */
/** @var $trackingModel LearningScormTracking */
/** @var $scormItemsTrackModel LearningScormItemsTrack */
/** @var $interactions array */
?>

<h1><?= $scormItemsTrackModel->scormItem ? $scormItemsTrackModel->scormItem->title : 'Untitled' ?></h1>

<div class="report-user-sco-stats">
<?php if (count($interactions) > 0) : ?>

	<div class="row-fluid sco-xml-report-header">
		<div class="span3">
			<div class="player-stats-counter-big"><?= $trackingModel->user->getClearUserId() ?></div>
			<div class="player-stats-label-box"><?= Yii::t('standard', '_USERNAME') ?></div>
		</div>
		<div class="span3">
			<div class="player-stats-counter-big"><?= $trackingModel->user->getFullName() ?></div>
			<div class="player-stats-label-box"><?= Yii::t('standard', '_NAME') ?></div>
		</div>
		<div class="span2 stat-last-attempt">
			<div class="player-stats-counter-big"><?= $trackingModel->renderLastAccess() ?></div>
			<div class="player-stats-label-box"><?= Yii::t('report', '_LO_COL_LASTATT') ?></div>
		</div>
		<div class="span2">
			<div class="player-stats-counter-big"><?= LearningScormItemsTrack::getStatusValue($scormItemsTrackModel->status) ?></div>
			<div class="player-stats-label-box"><?= Yii::t('standard', '_STATUS') ?></div>
		</div>
		<div class="span2">
			<div class="player-stats-counter-big"><?= $trackingModel->renderScore() ?></div>
			<div class="player-stats-label-box"><?= Yii::t('standard', '_SCORE') ?></div>
		</div>
	</div>

	<div class="docebo-modal-tabs clearfix">
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#" data-toggle="tab" data-target="#sco-xml-report-tab1">
					<span class="i-sprite is-search"></span>
					<span class="i-sprite is-search white active"></span>
					<?= Yii::t('report', 'Attempt details') ?>
				</a>
			</li>
			<li>
				<a href="#" data-toggle="tab" data-target="#sco-xml-report-tab2">
					<span class="i-sprite is-stats"></span>
					<span class="i-sprite is-stats white active"></span>
					<?= Yii::t('report', 'Advanced stats') ?>
				</a>
			</li>
		</ul>

		<div class="tab-content">

			<div class="tab-pane active" id="sco-xml-report-tab1">
				<div class="sco-interactions l-testpoll">
					<div class="row-fluid">
						<?php $this->renderPartial('_sco_xml_report_activity', array(
							'interactions' => $interactions,
						)); ?>
					</div>
				</div>
			</div>

			<div class="tab-pane" id="sco-xml-report-tab2">
				<?php $this->renderPartial('_sco_xml_report_stats', array(
					'userId' => $user_id,
					'objectId' => $objectId,
					'trackingModel' => $trackingModel,
					'scormItemsTrackModel' => $scormItemsTrackModel
				)); ?>
			</div>

		</div>

	</div>

<?php else: ?>

	<?php $this->renderPartial('_sco_xml_report_stats', array(
		'userId' => $user_id,
		'objectId' => $objectId,
		'trackingModel' => $trackingModel,
		'scormItemsTrackModel' => $scormItemsTrackModel
	)); ?>

<?php endif; ?>
</div>

<div class="form-actions">
	<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CLOSE'); ?>"/>
</div>
<script type="text/javascript">

	(function() {

		$(document).on('click', '.fancybox-inner .close-dialog', function(){
			$.fancybox.close();
			return false;
		});

		$(document).delegate(".modal-report-user-sco-stats", "dialog2.content-update", function() {
			var e = $(this);
			var $modal = e.find('.modal-body');

			var autoclose = e.find("a.auto-close");

			if (autoclose.length > 0) {
				e.find('.modal-body').dialog2("close");

				var href = autoclose.attr('href');
				if (href) {
					window.location.href = href;
				}
			}
			else {
			}
		});
	})();
</script>