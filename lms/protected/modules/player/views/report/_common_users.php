<h5 class="report-ajax-title"><span><?=LearningOrganization::objectTypeValue($organizationModel->objectType)?></span> - <?=$organizationModel->title?></h5>
<div class="row-fluid">
	<div class="player-grey-box">
		<div class="span6" id="report-export">
			<label><?=Yii::t('standard', 'Export as')?></label>
			<?=CHtml::dropDownList('export_report', '',  LearningCourseuser::exportTypes2(), array('class'=>'exportType'))?>
			<a href="javascript:;" onclick="Report.runProgressExportSingleObject(<?= (int) $organizationId ?>)" class="p-sprite download-transparent-black"></a>
		</div>
		<div id="enrollmentAvailableSearch">
			<form id="reportSearchCommonUsers" class="player-search-form" onsubmit="Report.search('player-reports-common-users', 'reportSearchCommonUsers');return false;">
				<?=CHtml::button("searchSubmit", array('value'=>'', 'onclick'=>'Report.search("player-reports-common-users", "reportSearchCommonUsers")'));?>
				<?=CHtml::activeTextField($usersModel, 'search_input');?>
			</form>
		</div>
	</div>
</div>
<br>
<?php
Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;
$this->widget('DoceboCGridView', array(
	'id' => 'player-reports-common-users',
	'dataProvider' => $usersModel->searchUsersByCommonTrack($this->course_id, $organizationId),
	'columns' => array(
		array(
			'name' => Yii::t('standard', '_USERNAME'),
			'value' => '$data->clearUserId',
			'type' => 'raw',
		),
		'firstname',
		'lastname',
		array(
			'name' => Yii::t('report', '_LO_COL_FIRSTATT'),
			'value' => '$data->learningCommontrack->firstAttempt ? $data->learningCommontrack->firstAttempt : "-"',
			'type' => 'raw',
		),
		array(
			'name' => Yii::t('report', '_LO_COL_LASTATT'),
			'value' => '$data->learningCommontrack->dateAttempt ? $data->learningCommontrack->dateAttempt : "-"',
			'type' => 'raw',
		),
		array(
			'name' => Yii::t('player', 'First complete'),
			'value' => '$data->learningCommontrack->first_complete ? $data->learningCommontrack->first_complete : "-"',
			'type' => 'raw',
		),
		array(
			'name' => Yii::t('report', '_DATE_COURSE_COMPLETED'),
			'value' => '$data->learningCommontrack->last_complete ? $data->learningCommontrack->last_complete : "-"',
			'type' => 'raw',
		),
		array(
			'name' => Yii::t('standard', 'Version'),
            'value' => function($data) {
                if(in_array($data->learningCommontrack->objectType, LearningRepositoryObject::$objects_with_versioning)) return $data->learningCommontrack->objectVersion->version_name;
            },
			'type' => 'raw'
		),
		array(
			'name' => Yii::t('standard', '_STATUS'),
			'value' => '$data->learningCommontrack->getStatusLabel()',
			'type' => 'raw',
		),
	),
)); ?>
