<? if (!isset($isPlaying)) { $isPlaying = false; } ?>
<? if ($testTrack) : ?>

	<?php if (!$isPlaying) {
		echo '<div class="l-testpoll review-dialog review-answers">';
	} ?>

<div class="row-fluid">
	<div class="span2">
		<a href="javascript:;" class="test-details-print" onclick="window.print()" style="display: none;">
			<?=CHtml::image($this->module->assetsUrl.'/images/print.png')?>
		</a>&nbsp;
	</div>
	<div class="span10 text-right">
		<div class="pull-right">
		</div>
		<p class="review-header"><?php

			echo '<span class="review-icon">'
			.( $testTrack->score_status == LearningTesttrack::STATUS_NOT_PASSED
				? '<span class="p-sprite remove-small-red"></span>'
				: '<span class="p-sprite check-solid-small-green"></span>' )
			.'</span>&nbsp;';

			echo Yii::t('standard', '_FINAL_SCORE') . ':&nbsp;';

			//check if we are showing points or percentage
			switch ($test->point_type) {
				case LearningTest::POINT_TYPE_NORMAL: {
					$userScore = number_format($testTrack->getCurrentScore(),2);
					echo '<b class="' . ($testTrack->score_status == LearningTesttrack::STATUS_NOT_PASSED ? 'score-failed' : 'score-passed' ) . '">'
						// . number_format($testTrack->getCurrentScore(),2) . ($test->order_type < 2 ? '/'. number_format($test->getMaxScore(),2) : '' ).'</b>';
						. $userScore . '</b>';
				} break;
				case LearningTest::POINT_TYPE_PERCENT: {
					$_percent = $testTrack->getCurrentScore();
					$_percent = ($_percent > 0)? $_percent : 0;
					$userScore = round($_percent, 1, PHP_ROUND_HALF_EVEN);
					echo '<b class="' . ($testTrack->score_status == LearningTesttrack::STATUS_NOT_PASSED ? 'score-failed' : 'score-passed' ) . '">'
						. $userScore . ($commonTrack->score_max == 100 ? '</b> <span class="review-percentage">%</span>' : '');
				} break;
			}
			?>
		</p>
		<?php
		$manualScore = $testTrack->getMaxManualScore();
		if ($manualScore > 0 && $testTrack->score_status != LearningTesttrack::STATUS_VALID) {
			echo '<p class="review-manual-score">'
				. Yii::t('test', '_TEST_MANUAL_SCORE')
				. ' <b>' . $manualScore . '</b> ' . (($test->point_type == LearningTest::POINT_TYPE_NORMAL || $commonTrack->score_max != 100) ? Yii::t('test', '_TEST_SCORES') : ' <span class="review-percentage">%</span>')
				. '</p>';
		} ?>
	</div>
	<div class="clearfix"></div>
</div>
<div class="block-hr"></div>
<!-- answers -->
<div class="row-fluid"><?php

	//TODO: this is a fix, write it in a better way
	$answeredQuestions = $testTrack->getAnsweredQuestions();
	$allQuestions = array();
	foreach ($testTrack->questsWithTitles as $i => $question) {
		$allQuestions[$question->getPrimaryKey()] = $question;
	}

	//TODO: test partially done should be managed in a different way
	foreach ($answeredQuestions as $i => $answeredQuestion) :

		$question = $allQuestions[$answeredQuestion['idQuest']];
		if(!$question)
			continue;

		$questionPlayer = CQuestionComponent::getQuestionManager($question->type_quest);

		$userAnswer = LearningTesttrackAnswer::model()->findAllByAttributes(array(
			'idTrack' => $testTrack->idTrack,
			'idQuest' => $question->idQuest,
		));


		if ($question->type_quest == LearningTestquest::QUESTION_TYPE_TITLE) : ?>
			<div class="span12 test-details-type-title"><span><?=$question->title_quest?></span></div>
		<?php else : ?>
			<div class="block <?= (!($i%2)) ? 'odd' : '' ?>">
				<div class="row-fluid">
					<div class="span2"><?=isset($questionsLang[$question->type_quest]) ? $questionsLang[$question->type_quest] : '-'?></div>
					<div class="span10">
						<div class="test-details-question-title">
							<span class="pull-left"><?=$i+1?>)&nbsp;</span> <?= Docebo::nbspToSpace($questionPlayer->applyTransformation('review', $question->title_quest, array(
								'idQuestion' => $question->idQuest,
								'idTrack' => $testTrack->idTrack,
								'questionManager' => $questionPlayer
							)));?>
						</div>
						<?php
						if ($isPlaying) {
							$_showCorrectAnswers = $test->correctAnswersVisibleOnReview($userId);
							$_showAnswers = $test->answersVisibleOnReview($userId);
						} else {
							$_showCorrectAnswers = true;
							$_showAnswers = true;
						}
						$this->widget('player.components.widgets.TestReportsAnswerFormatter', array(
							'questionType' => $question->type_quest,
							'questId' => $question->idQuest,
							'trackId' => $testTrack->idTrack,
							'testId' => $testTrack->idTest,
							'userId' => $userId,
							'isTeacher' => (isset($isTeacher) ? (bool)$isTeacher : false),
							'showCorrectAnswers' => $_showCorrectAnswers,
							'showAnswers' => $_showAnswers
						));
						?>
					</div>
				</div>
				<div style="clear:both"><!-- this is needed by IE8 --></div>
			</div>
			<div class="block-hr"></div>
		<? endif; ?>
	<? endforeach ?>
</div>
<? else : ?>
	<div style="margin: 50px;"><?=Yii::t('standard', '_NO_CONTENT')?></div>
<? endif; ?>

<?php if (!$isPlaying) {
	echo '</div>';
} ?>

<script>
	$(document).ready(function() {
		try {
			$('.score-editable').editable({
				validate: function(value) {
					//NOTE: at the moment decimal separator is not managed by international standards.
					//We are assuming the dot "." as decimal separator, but commas "," are accepted and managed too!
					//TODO: This must be changed in the future and i18n standards applied for number formats (same thing applies to actionEditTestScore() in ReportController.php)
					if (isNaN(parseFloat(value.replace(",", ".")))) {
						return <?= CJSON::encode(Yii::t('player', 'The value must be a number')) ?>;
					}
				},
				success: function(res, value)
				{
					res = JSON.parse(res);
					if (res.success == false)
						return res.message;
				}
			});
		} catch(e) {}
	});
</script>
<style>
	@media print {
		.container, .menu {
					 display:none;
				 }
		.docebo {
			width: 950px;
		}
		.fancybox-inner {
			display:block;
		}
	}
</style>