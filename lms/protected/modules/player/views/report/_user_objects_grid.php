<div class="row-fluid">
	<div class="player-grey-box" style="margin:0;">
		<div class="span6" id="report-export">
			<label><?=Yii::t('standard', 'Export as')?></label>
			<?=CHtml::dropDownList('export_report', '',  LearningCourseuser::exportTypes2(), array('id'=>'exportObjectsType'))?>
			<a href="javascript:;" onclick="Report.runProgressExportObjects(<?=$_GET['user_id']?>)" class="p-sprite download-transparent-black"></a>
		</div>
		<div id="enrollmentAvailableSearch">
			<form id="reportSearchObjectsForm" class="player-search-form" action="<?=$this->createUrl('index', array('course_id'=>$this->course_id))?>" onsubmit="Report.searchObjects();return false;">
				<?=CHtml::button("searchSubmit", array('value'=>'', 'onclick'=>'Report.searchObjects()'));?>
				<?=CHtml::textField('objects_search');?>
			</form>
		</div>
	</div>
</div>
<br>
<?php

$columns = array(
	array(
		'header' => Yii::t('standard', '_TITLE'),
		'value' => '$this->grid->controller->widget("ReportsGridFormatter", array("stats"=>$data, "column"=>"titleByUser"), true);',
		'type' => 'raw',
	),
	array(
		'header' => Yii::t('report', '_LO_COL_FIRSTATT'),
		'value' => '$data->objectType ? LearningCommontrack::getDateByParameters("firstAttempt", $data->organizationId, '.$_GET['user_id'].') : ""',
		'type' => 'raw',
        'htmlOptions' => array('class' => 'text-center'),
        'headerHtmlOptions' => array('class' => 'text-center'),
	),
	array(
		'header' => Yii::t('report', '_LO_COL_LASTATT'),
		'value' => '$data->objectType ? LearningCommontrack::getDateByParameters("dateAttempt", $data->organizationId, '.$_GET['user_id'].') : ""',
		'type' => 'raw',
        'htmlOptions' => array('class' => 'text-center'),
        'headerHtmlOptions' => array('class' => 'text-center'),
	),
	array(
		'header' => Yii::t('player', 'First complete'),
		'value' => '$data->objectType ? LearningCommontrack::getDateByParameters("first_complete", $data->organizationId, '.$_GET['user_id'].') : ""',
		'type' => 'raw',
        'htmlOptions' => array('class' => 'text-center'),
        'headerHtmlOptions' => array('class' => 'text-center'),
	),
	array(
		'header' => Yii::t('report', '_DATE_COURSE_COMPLETED'),
		'value' => '$data->objectType ? LearningCommontrack::getDateByParameters("last_complete", $data->organizationId, '.$_GET['user_id'].') : ""',
		'type' => 'raw',
        'htmlOptions' => array('class' => 'text-center'),
        'headerHtmlOptions' => array('class' => 'text-center'),
	),
	array(
		'header' => Yii::t('standard', 'Ver'),
        'value' => function($data){
            if(in_array($data->objectType, LearningRepositoryObject::$objects_with_versioning)) {
                return $data->versionName;
            }
            return '';
        },
		'type' => 'raw',
		'htmlOptions' => array(
			'class' => 'text-center'
		),
		'headerHtmlOptions' => array(
			'class' => 'text-center'
		)
	),
	array(
		'header' => Yii::t('standard', '_STATUS'),
		'value' => '$this->grid->controller->widget("ReportsGridFormatter", array("stats"=>$data, "column"=>"userStatus"), true);',
		'type' => 'raw',
        'htmlOptions' => array('class' => 'text-center'),
        'headerHtmlOptions' => array('class' => 'text-center'),
	),
	array(
		'header' => Yii::t('standard', '_RESET'),
		'value' => '$this->grid->controller->widget("ReportsGridFormatter", array("stats"=>$data, "column"=>"userLoStatusReset", "userId"=>'.$_GET['user_id'].'), true);',
		'type' => 'raw',
		'htmlOptions' => array('class' => 'text-center'),
		'headerHtmlOptions' => array('class' => 'text-center'),
	),
    // array(
    // 'header' => Yii::t('report', '_TOTAL_SESSION'),
    // 'value' => '$this->grid->controller->widget("ReportsGridFormatter", array("stats"=>$data, "column"=>"avgTime"), true);',
    // 'type' => 'raw',
    //          'htmlOptions' => array('class' => 'text-center'),
    //          'headerHtmlOptions' => array('class' => 'text-center'),
    // ),
	array(
		'header' => Yii::t('standard', '_SCORE'),
		'value' => '$this->grid->controller->widget("ReportsGridFormatter", array("stats"=>$data, "column"=>"avgScoreByUser"), true);',
		'type' => 'raw',
        'htmlOptions' => array('class' => 'text-center'),
        'headerHtmlOptions' => array('class' => 'text-center'),
	),
);

if ((int) $courseModel->lo_max_attempts > 0) {
    $columns[] = array(
        'header' => Yii::t('standard', 'Attempts'),
        'value' => '$this->grid->controller->widget("ReportsGridFormatter", array("stats"=>$data, "column"=>"attempts"), true);',
        'type' => 'raw',
        'htmlOptions' => array('class' => 'text-center'),
        'headerHtmlOptions' => array('class' => 'text-center'),
    );
}

$this->widget('DoceboCGridView', array(
	'id' => 'player-objects',
	'dataProvider' => $objectsDataProvider,
	'rowCssClassExpression'=>'$data->objectType	 ? "odd parent" : "odd"',
	'afterAjaxUpdate' => 'function(){$(document).controls();Report.prepareEditable();}',
	'template' => '{items}{pager}',
	'columns' => $columns,
)); 
            

?>

<script type="text/javascript">
	$(function(){
		$(document).on("dialog2.content-update", "[id^='dialog_reset_user_lo_stats']", function () {
			// Watch for auto-close in the content of the dialog and close it
			if ($(this).find("a.auto-close").length > 0) {

				if($(this).find("a.auto-close").hasClass('refresh'))
				// Also reload the page
				window.location.reload();

			}

			$('input', this).styler();
		});
	});
</script>