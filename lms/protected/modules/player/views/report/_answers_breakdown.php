<?php
/** @var $this ReportController */
/** @var $scormItem LearningScormItems */
/** @var $qa [] */
?>

<style type="text/css">
	.modal-report-answers-breakdown {
		width: 960px;
		margin-left: -490px;
	}
	.modal-report-answers-breakdown .qa-container {
		background: #f1f3f2;
		border-top: 1px solid #e6e6e6;
		border-bottom: 1px solid #e6e6e6;
		-moz-box-shadow: inset 0 1px 0px 0px #fff, inset 0 -1px 0px 0px #fff;
		-webkit-box-shadow: inset 0 1px 0px 0px #fff, inset 0 -1px 0px 0px #fff;
		box-shadow: inset 0 1px 0px 0px #fff, inset 0 -1px 0px 0px #fff;
		padding: 20px 0 10px 40px;
		margin: 10px 0 0;
	}
	.modal-report-answers-breakdown .qa-container > div {
		margin-bottom: 10px;
	}
	.modal-report-answers-breakdown .qa-container > div:last-child {
		margin-bottom: 0;
	}
	.modal-report-answers-breakdown .qa-container > div label {
		display: none;
	}
	.modal-report-answers-breakdown .qa-container > div:first-child label {
		display: block;
		position: absolute;
		top: -50px; left: 50%;
		width: 80px;
		margin-left: -40px;
		text-align: center;
		font-weight: bold;
		text-transform: uppercase;
	}
	.modal-report-answers-breakdown .qa-container > div > div {
		position: relative;
		line-height: 15px;
	}
	.modal-report-answers-breakdown .qa-container .p-sprite {
		position: absolute;
		top: 0; left: -30px;
	}
	.modal-report-answers-breakdown .qa-progress-bar {
		border: 1px solid #e4e6e5;
		height: 11px;
		width: 210px;
		overflow: hidden;
		background: #FFF;
	}
	.modal-report-answers-breakdown .qa-progress-bar > span {
		background: #5fbf5f;
		height: 11px;
		float: left;
	}
	.modal-report-answers-breakdown .qa-total-responses {
		text-align: right;
		margin: 5px 25px 20px 0;
	}
	.modal-report-answers-breakdown .qa-stats-title {}
	.modal-report-answers-breakdown .qa-stats-value {
		font-weight: bold;
		font-size: 18px;
	}
	.modal-report-answers-breakdown .question-title {
		white-space: nowrap;
		margin-bottom: 30px;
	}
</style>

<h1><?= $scormItem->scorm_org->title ?></h1>
<?php $maxScore = LearningCommontrack::getMaxScoreByScorm($scormItem->idscorm_organization);?>

	<br/>
<div class="row-fluid">
	<div class="span3">
		<div class="">
			<div class="qa-stats-value">
				<?=LearningCommontrack::countAttemptsByScorm($scormItem->idscorm_organization);?>
			</div>
			<div class="qa-stats-title"><?=Yii::t('organization', '_ATTEMPTS')?></div>
		</div>
	</div>
	<div class="span3">
		<div class="">
			<div class="qa-stats-value">
				<?=LearningCommontrack::countByStatusAndScorm($scormItem->idscorm_organization, LearningCommontrack::STATUS_COMPLETED)
				.'/'.
				LearningCommontrack::countByStatusAndScorm($scormItem->idscorm_organization, LearningCommontrack::STATUS_FAILED)?></div>
			<div class="qa-stats-title"><?=Yii::t('standard', 'passed').'/'.Yii::t('standard', 'failed')?></div>
		</div>
	</div>
	<div class="span2">
		<div class="">
			<div class="qa-stats-value">
				<?=LearningCommontrack::getHighestScoreByScorm($scormItem->idscorm_organization).'/'.$maxScore?>
			</div>
			<div class="qa-stats-title"><?=Yii::t('standard', '_MAX_SCORE');?></div>
		</div>
	</div>
	<div class="span2">
		<div class="">
			<div class="qa-stats-value">
				<?=LearningCommontrack::getLowestScoreByScorm($scormItem->idscorm_organization).'/'.$maxScore;?>
			</div>
			<div class="qa-stats-title"><?=Yii::t('standard', '_MIN_SCORE');?></div>
		</div>
	</div>
	<div class="span2">
		<div class="">
			<div class="qa-stats-value">
				<?=LearningCommontrack::getAverageScoreByScorm($scormItem->idscorm_organization).'/'.$maxScore;?>
			</div>
			<div class="qa-stats-title"><?=Yii::t('course', '_MEDIUM')?></div>
		</div>
	</div>
</div>

	<br/>

<div class="row-fluid">
	<div class="player-grey-box">
		<div class="span6" id="report-export" style="padding-bottom: 10px;">
			<label><?=Yii::t('standard', 'Export as')?></label>
			<?=CHtml::dropDownList('export_report', '',  LearningCourseuser::exportTypes2(), array('class'=>'exportType'))?>
			<a href="javascript:;" onclick="Report.exportReport(this, 'reportSearchScoQAXml', '<?=$this->createUrl('scoQAXmlExport', array('objectId'=>$scormItem->idscorm_item))?>')" class="p-sprite download-transparent-black"></a>
		</div>
		<div class="span6">
			<form id="reportSearchScoQAXml" class="player-search-form"></form>
		</div>
	</div>
</div>

<?php if (!empty($qa)) :
	$_index = 0;
	$_questionsLang = LearningTestquest::getQuestionTypesTranslationsList();
	$_allowedTypes = LearningScormItems::getAllowedInteractionTypes();
	?>
	<div class="answers-breakdown">
	<?php
	foreach ($qa as $k => $question) :

		$type = $question['type'];
		if (!in_array($type, array('true-false', 'choice', 'matching', 'fill-in', 'sequencing', 'other')))
			continue;

		$_index++;
		$typeLabel = isset($_allowedTypes[$type]) ? $_allowedTypes[$type] : (
			isset($_questionsLang[$type]) ? $_questionsLang[$type] : $type
		);

		?>

		<div class="row-fluid">
			<div class="qa-type"><?= $typeLabel ?></div>
			<div class="question-title"><strong><?= $_index.') '. (!empty($question['description']) ? $question['description'] : $k) ?></strong></div>
			<?php
				$this->widget('player.components.widgets.ReportsQAFormatter', array(
					'question' => $question,
				));
			?>
			<div class="qa-total-responses">
				<?= Yii::t('report', 'Total responses: {count}', array('{count}' => $question['total_responses'])) ?>
			</div>
		</div>

		<?php

	endforeach; ?>
	</div>
<?php endif; ?>