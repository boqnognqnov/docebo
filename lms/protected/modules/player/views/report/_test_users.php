<h5 class="report-ajax-title"><span><?=LearningOrganization::objectTypeValue($organizationModel->objectType)?></span> - <?=$organizationModel->title?></h5>
<div class="row-fluid">
	<div class="player-grey-box">
		<div class="span6" id="report-export">
			<label><?=Yii::t('standard', 'Export as')?></label>
			<?=CHtml::dropDownList('export_report', '',  LearningCourseuser::exportTypes2(), array('class'=>'exportType'))?>
			<!-- 
			<a href="javascript:;" onclick="Report.exportReport(this, 'reportSearchTestUsersForm', '<?=$this->createUrl('exportTestUsers', array('course_id' => $this->course_id, 'organizationId' => $organizationId))?>')" class="p-sprite download-transparent-black"></a>
			 -->
			<a href="javascript:;" onclick="Report.runProgressExportSingleObject(<?= (int) $organizationId ?>)" class="p-sprite download-transparent-black"></a>
			
		</div>
		<div id="enrollmentAvailableSearch">
			<form id="reportSearchTestUsersForm" class="player-search-form" action="<?=$this->createUrl('index', array('course_id'=>$this->course_id))?>" onsubmit="Report.search('player-reports-test-users', 'reportSearchTestUsersForm');return false;">
				<div class="pull-right report-test-select-columns">
					<ul class="nav nav-pills" style="margin-bottom: 0px; width: 100%;">
						<li class="dropdown">
							<div class="i-sprite is-table" data-toggle="dropdown">&nbsp;</div>
							<ul class="dropdown-menu pull-right" id="report-test-column-selector" style="margin-right: -5px;">
								<li class="test-report-column-title"><?=Yii::t('player', 'Click to select questions to display.');?></li>
								<? if ($testTrack->test->questionsOnly) : ?>
									<? foreach ($testTrack->test->questionsOnly as $i => $q) : ?>
									<li class="test-report-column"><input type="checkbox" checked onclick="Report.search('player-reports-test-users', 'reportSearchTestUsersForm')" name="test_questions[]" value="<?=$q->idQuest?>" id="t_q_<?=$q->idQuest?>" /><label for="t_q_<?=$q->idQuest?>"><?=($i+1).") ".$q->title_quest?></label></li>
									<? endforeach; ?>
								<? endif; ?>
							</ul>
						</li>
					</ul>
				</div>
				<?=CHtml::button("searchSubmit", array('value'=>'', 'onclick'=>'Report.search("player-reports-test-users", "reportSearchTestUsersForm")'));?>
				<?=CHtml::activeTextField($usersModel, 'search_input');?>
			</form>
		</div>
	</div>
</div>
<br>
<?php
Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;
$columns = array(
	array(
		'name' => Yii::t('standard', '_USERNAME'),
		'value' => '$data->clearUserId',
		'type' => 'raw',
	),
	'firstname',
	'lastname',
	array(
		'name' => Yii::t('report', '_DATE_COURSE_COMPLETED'),
		'value' => '$data->learningTesttrack->date_end_attempt',
		'type' => 'raw',
	),
	array(
		'name' => Yii::t('standard', '_SCORE'),
		'value' => '$data->learningTesttrack->getScoresPair()',
		'type' => 'raw',
	),
);
if ($testTrack && $testTrack->test->questionsOnly)
{
	foreach ($testTrack->test->questionsOnly as $i => $q)
	{
		if (in_array($q->idQuest, $test_questions))
		{
			$columns[] = array(
				'value' => '$this->grid->controller->widget("TestReportsGridFormatter", array("userId"=>$data, "questId"=>'.$q->idQuest.', "trackId"=>$data->learningTesttrack->idTrack), true);',
				'type' => 'raw',
				'header' => $q->getTitleQuestTruncated(),
			);
		}
	}
}


Yii::app()->event->raise('onGetColumnsUsersCompletedTestsReport', new DEvent($this, array(
	'columns'=>&$columns,
	'testTrack'=>($testTrack ? $testTrack : null),
	'test_questions'=>$test_questions,
)));


$this->widget('DoceboCGridView', array(
	'id' => 'player-reports-test-users',
	'dataProvider' => $usersModel->searchUsersByCourseAndTest($this->course_id, $organizationId),
	'columns' => $columns,
)); ?>
<style type="text/css">
	#player-reports-test-users tr {
		vertical-align: top;
	}
</style>