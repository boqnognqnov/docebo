<?php
$this->widget('common.extensions.jqplot.JqplotGraphWidget', array(
	'pluginScriptFile'=>array(
		'jqplot.dateAxisRenderer.js',
		'jqplot.barRenderer.js',
		'jqplot.categoryAxisRenderer.js',
		'jqplot.highlighter.js')
));
//we already have the base training breadcrumb added, so we need to add one in addition
//the course breadcrumb need some additional checks for permissions
$useCourseUrl = true;
if (Yii::app()->user->getIsPu()) {
	if (!Yii::app()->user->checkAccess('/lms/admin/course/view')) {
		$enrollment = LearningCourseuser::model()->findByAttributes(array(
			'idUser' => Yii::app()->user->id,
			'idCourse' => $courseModel->idCourse
		));
		if (empty($enrollment)) {
			$useCourseUrl = false;
		}
	}
}
if ($useCourseUrl) {
	$this->breadcrumbs[] = array(
		'label' => Docebo::ellipsis(Yii::t('player', $courseModel->name), 60, '...'),
		'url' => Docebo::createLmsUrl('player', array('course_id' => $courseModel->idCourse))
	);
} else {
	$this->breadcrumbs[] = Docebo::ellipsis(Yii::t('player', $courseName), 60, '...');
}
$this->breadcrumbs[] = Yii::t('standard', '_REPORTS');
?>

<div class="row-fluid">
	<h4 class="title-dark"><?=Yii::t('stats', 'Course report')?></h4>
	<div class="row-fluid no-space">
		<div class="span4">
			<br>
			<div class="span6 report-knob-text">
				<br>
				<?=Yii::t('stats', 'Users that finished the course')?>
			</div>
			<div class="span1"></div>
			<div class="span4">
				<input class="player-report-enrolled" value="<?=$stats->completed_percentage?>" />
			</div>
		</div>
		<div class="span8">
			<div class="span4">
				<div class="span4 player-stats-icon-box">
					<div class="p-sprite user-check-black player-stats-icon-big"></div>
				</div>
				<div class="span8">
					<div class="span12 player-stats-counter">
						<?=$stats->enrolled?>
					</div>
					<div class="span12 player-stats-label-box">
						<?=Yii::t('standard', 'Enrolled users')?>
					</div>
				</div>
			</div>
			<div class="span4">
				<div class="span4 player-stats-icon-box">
					<div class="p-sprite calendar-black player-stats-icon-big"></div>
				</div>
				<div class="span8">
					<div class="span12 player-stats-counter">
						<?=$stats->days_since_lunch?>
					</div>
					<div class="span12 player-stats-label-box">
						<?=Yii::t('stats', 'Days of activity')?>
					</div>
				</div>
			</div>
			<div class="span4">
				<div class="span4 player-stats-icon-box">
					<div class="p-sprite books-black player-stats-icon-big"></div>
				</div>
				<div class="span8">
					<div class="span12 player-stats-counter">
						<?=$stats->resources?>
					</div>
					<div class="span12 player-stats-label-box">
						<?=Yii::t('standard', 'Training materials')?>
					</div>
				</div>
			</div>
			<div class="span4">
				<div class="span4 player-stats-icon-box">
					<div class="p-sprite arrow-circle-big-black player-stats-icon-big"></div>
				</div>
				<div class="span8">
					<div class="span12 player-stats-counter">
						<?=$stats->to_begin?>
					</div>
					<div class="span12 player-stats-label-box">
						<?=Yii::t('report', '_MUSTBEGIN')?>
					</div>
				</div>
			</div>
			<div class="span4">
				<div class="span4 player-stats-icon-box">
					<div class="p-sprite clock-circle-big player-stats-icon-big"></div>
				</div>
				<div class="span8">
					<div class="span12 player-stats-counter">
						<?=$stats->in_progress?>
					</div>
					<div class="span12 player-stats-label-box">
						<?=Yii::t('standard', '_USER_STATUS_BEGIN')?>
					</div>
				</div>
			</div>
			<div class="span4">
				<div class="span4 player-stats-icon-box">
					<div class="p-sprite flag-black player-stats-icon-big"></div>
				</div>
				<div class="span8">
					<div class="span12 player-stats-counter">
						<?=$stats->completed?>
					</div>
					<div class="span12 player-stats-label-box">
						<?=Yii::t('standard', '_COMPLETED')?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
$event = new DEvent($this, array('controller' => $this, 'courseModel' => $courseModel, 'usersModel' => $usersModel));
Yii::app()->event->raise('OnCourseReportStatisticsRender', $event);
?>

<div class="row-fluid">
	<h4 class="title-dark"><?=Yii::t('stats', 'Course access statistics')?></h4>
	<?php
	// Raise an event which may substitute the default graph with a custom one
	$CustomGraphEvent = new DEvent($this, array(
		'courseModel' => $courseModel,
		'usersModel' => $usersModel
	));
	Yii::app()->event->raise('OnCourseReportCustomGraphCheck', $CustomGraphEvent);

	if($CustomGraphEvent->return_value['overrideDefault'] == true){
		echo $CustomGraphEvent->return_value['html'];
	} else {
	?>
	<div id="statistics_general_lms_logins" style="height: 250px;"></div>
	<?php } ?>
</div>
<br>
<div id="report-grids-box">
	<div id="report-navigation">
		<div class="report-nav-btn active" onclick="Report.changeGrid(this, 'report-users-grid')">
			<?=Yii::t('standard', 'User statistics')?>
			<div class="report-nav-arrow active"></div>
		</div>
		<div class="report-nav-btn" onclick="Report.changeGrid(this, 'report-objects-grid')">
			<?=Yii::t('menu_course', '_STATCOURSE')?>
			<div class="report-nav-arrow"></div>
		</div>
		<?php  if(!Yii::app()->user->isPu || (Yii::app()->user->isPu && Yii::app()->user->checkAccess("/lms/admin/course/evaluation")) || ($courseModel && LearningCourseuser::isUserLevel(Yii::app()->user->id, $courseModel->idCourse, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR) && LearningCourse::isUserAssignedToInstructorSessions($courseModel->idCourse, Yii::app()->user->id))){?>
		<div class="report-nav-btn" onclick="Report.changeGrid(this, 'report-deliverables-grid')">
			<?=Yii::t('deliverable', 'Student deliverables')?>
			<div class="report-nav-arrow"></div>
		</div>
		<?php } ?>
		<?php Yii::app()->event->raise('AfterCourseReportTabsRender', new DEvent($this, array())); ?>
	</div>

	<div id="report-users-grid" class="report-grid-box">
		<?php
			if ( ($inAjaxGridUpdate == false) || ($inAjaxGridUpdate == 'player-reports') ) {
				$this->renderPartial('_users_grid', array(
					'usersModel' => $usersModel,
					'courseModel' => $courseModel,
				));
			}
		?>
	</div>
	<div id="report-objects-grid" class="report-grid-box" style="display: none;">
		<?php
			if ( ($inAjaxGridUpdate == false) || ($inAjaxGridUpdate == 'player-objects') ) {
		 		$this->renderPartial('_objects_grid', array(
					'learningOrganization' => $learningOrganization,
					'objectsDataProvider' => $objectsDataProvider,
					'usersModel' => $usersModel,
					'courseModel' => $courseModel,
				));
			}
		?>
	</div>
	<div id="report-deliverables-grid" class="report-grid-box" style="display: none;">
		<?php 
			if ( ($inAjaxGridUpdate == false) || ($inAjaxGridUpdate == 'player-reports-deliverables-grid') ) {
				$this->renderPartial('_deliverables_grid', array(
            		'deliverablesModel'             => $deliverablesModel,
					'showCoachingSessionSelect'		=> $showCoachingSessionSelect,
					'coachSessions'         		=> $coachSessions
				));
			}
		?>
	</div>
	<?php Yii::app()->event->raise('AfterCourseReportTabsContentRender', new DEvent($this, array(
		'inAjaxGridUpdate' => $inAjaxGridUpdate,
		'usersModel' => $usersModel,
		'courseModel' => $courseModel,
	))); ?>
</div>
<div id="report-info-box" style="display: none;">
	<a class="docebo-back" href="javascript:;" onclick="Report.hideInfoBox('report-grids-box')">
		<span><?=Yii::t('standard', '_BACK')?></span>
	</a>
	<br><br>
	
	<div class="report-ajax-loader"></div>
	<div id="report-info-box-content"></div>
</div>

<script>

	Report.init({
		course_id: <?=$this->course_id?>,
		reportGridId: 'player-reports',
        deliverableGridId: 'player-reports-deliverables-grid',
		objectsGridId: 'player-objects',
		exportUsersUrl: '<?=$this->createUrl('export', array('course_id'=>$this->course_id))?>',
		exportObjectsUrl: '<?=$this->createUrl('exportObjects', array('course_id'=>$this->course_id))?>',
		progressUsersExportUrl			: '<?= Docebo::createAbsoluteLmsUrl('progress/run', array('type' => Progress::JOBTYPE_COURSE_USERS_REPORT, 			'course_id'=>$this->course_id)) ?>',
		progressObjectsExportUrl		: '<?= Docebo::createAbsoluteLmsUrl('progress/run', array('type' => Progress::JOBTYPE_COURSE_OBJECTS_REPORT, 		'course_id'=>$this->course_id)) ?>',
		progressSingleObjectExportUrl	: '<?= Docebo::createAbsoluteLmsUrl('progress/run', array('type' => Progress::JOBTYPE_COURSE_SINGLE_OBJECT_STATS, 	'course_id'=>$this->course_id)) ?>'
	});

	$(function(){

        Docebo.init({ajaxResponseFilterEnabled:false});

		$(".player-report-enrolled").knob({
			'min':0,
			'max':100,
			'readOnly': true,
			//'displayInput': false,
			'fgColor': '#6cc267',
			'bgColor': '#cccccc',
			'thickness':.25,
			'width': 80,
			'height':80,
			'draw' : function () {
				var displayValue = Math.round(this.cv);
				$(this.i).val(displayValue + '%')
				//$(this.i).val(this.cv + '%')
			}
		});

		<?php
		$_accessValues = array_values($lmsLogins);
		$_accessDates = array_keys($lmsLogins);
		foreach ($_accessDates as &$_date) {
			$_date = "'". date('m/d/y', strtotime($_date)) ."'";
		}
		?>
		var s1 = [<?= implode(', ', $_accessValues); ?>];
		var ticks = [<?= implode(',', $_accessDates); ?>];
		var line1 = [];
		for (var i = 0; i < s1.length; i++)
			line1.push([ticks[i], s1[i]]);
		$.jqplot.config.enablePlugins = true;

		var plot1 = $.jqplot('statistics_general_lms_logins', [line1], {
			gridPadding:{right:35},
			axes:{
				xaxis:{
					renderer:$.jqplot.DateAxisRenderer,
					tickOptions:{formatString:'%b %#d'}
				}
			},
			series:[{lineWidth:4, markerOptions:{style:'filledCircle'}}],
			grid: {
				background: '#f5f5f5',
				shadow: false,
				borderColor: '#CCC',
				borderWidth: 1,
				gridLineColor: '#fff'
			},
			seriesDefaults: {
				color: '#0465ac'
			},
			highlighter: {
				lineWidthAdjust: 2.5,   // pixels to add to the size line stroking the data point marker
				// when showing highlight.  Only affects non filled data point markers.
				sizeAdjust: 5,          // pixels to add to the size of filled markers when drawing highlight.
				showTooltip: true,      // show a tooltip with data point values.
				tooltipLocation: 'n',  // location of tooltip: n, ne, e, se, s, sw, w, nw.
				fadeTooltip: false,      // use fade effect to show/hide tooltip.
				tooltipFadeSpeed: "fast",// slow, def, fast, or a number of milliseconds.
				tooltipOffset: 2,       // pixel offset of tooltip from the highlight.
				tooltipAxes: 'y',    // which axis values to display in the tooltip, x, y or both.
				tooltipSeparator: ', ',  // separator between values in the tooltip.
				useAxesFormatters: false, // use the same format string and formatters as used in the axes to
				// display values in the tooltip.
				tooltipFormatString: '%.0f' // sprintf format string for the tooltip.  only used if
				// useAxesFormatters is false.  Will use sprintf formatter with
				// this string, not the axes formatters.
			}

		});

		// make it scalable with the browser window
		$(window).resize(function (event, ui) {
			plot1.replot( { resetAxes: true } );
		});
	});

</script>