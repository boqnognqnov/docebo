<h5 class="report-ajax-title"><span><?=LearningOrganization::objectTypeValue($organizationModel->objectType)?></span> - <?=$organizationModel->title?></h5>
<div class="row-fluid">
	<div class="player-grey-box">
		<div class="span6" id="report-export">
			<label><?=Yii::t('standard', 'Export as')?></label>
			<?=CHtml::dropDownList('export_report', '', LearningCourseuser::exportTypes(), array('class'=>'exportType'))?>
			<a href="javascript:;" onclick="Report.exportReport(this, 'reportSearchTestUsersForm', '<?=$this->createUrl('exportCommonUsers', array('course_id' => $this->course_id, 'organizationId' => $organizationId))?>')" class="p-sprite download-transparent-black"></a>
		</div>
		<div id="enrollmentAvailableSearch">
			<form id="reportSearchCommonUsers" class="player-search-form" onsubmit="Report.search('player-reports-common-users', 'reportSearchCommonUsers');return false;">
				<?=CHtml::button("searchSubmit", array('value'=>'', 'onclick'=>'Report.search("player-reports-common-users", "reportSearchCommonUsers")'));?>
				<?=CHtml::activeTextField($usersModel, 'search_input');?>
			</form>
		</div>
	</div>
</div>
<br>
<?php
Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;
$this->widget('DoceboCGridView', array(
    'id' => 'player-reports-deliverables-grid-results',
    'dataProvider' => $usersModel->searchUsersByDeliverableObject($this->course_id, $organizationModel),
    'columns' => array(
        array(
            'name' => Yii::t('standard', '_USERNAME'),
            'value' => '$data->getClearUserId()',
            'type' => 'raw',
        ),
        array(
            'name' => Yii::t('standard', '_FIRSTNAME'),
            'value' => 'CHtml::link($data->firstname, array(), array())',
            'type' => 'raw',
        ),
        array(
            'name' => Yii::t('standard', '_LASTNAME'),
            'value' => 'CHtml::link($data->lastname, array(), array())',
            'type' => 'raw',
        ),
        array(
            'name' => Yii::t('deliverable', 'Submitted on'),
            'value' => '($data->learningCommontrack->getLastCreatedDeliverableObject() && $data->learningCommontrack->getLastCreatedDeliverableObject()->created ) ? $data->learningCommontrack->getLastCreatedDeliverableObject()->created : "-"',
            'type' => 'raw',
            'htmlOptions' => array('class' => 'text-center'),
            'headerHtmlOptions' => array('class' => 'text-center'),
        ),
        array(
            'name' => Yii::t('standard', '_STATUS'),
            'value' => '$data->learningCommontrack->renderStatusIcon()',
            'type' => 'raw',
            'htmlOptions' => array('class' => 'text-center'),
            'headerHtmlOptions' => array('class' => 'text-center'),
        ),
        array(
            'name' => Yii::t('standard', '_SCORE'),
            'value' => '$data->learningCommontrack->renderScore()',
            'type' => 'raw',
            'htmlOptions' => array('class' => 'text-center'),
            'headerHtmlOptions' => array('class' => 'text-center'),
        ),
        array(
            'name' => Yii::t('standard', '_DETAILS'),
            'value' => 'CHtml::link(CHtml::tag("span", array("class"=>"i-sprite is-list")), array("deliverableDetailsByUser", "organization_id"=>$data->learningCommontrack->idReference, "course_id"=>$_GET["course_id"], "user_id"=>$data->primaryKey), array("class"=>"grid-link open-dialog", "data-dialog-class"=>"modal-deliverable-user-history"))',
            'type' => 'raw',
            'htmlOptions' => array('class' => 'text-center'),
            'headerHtmlOptions' => array('class' => 'text-center'),
        ),
    ),
    'afterAjaxUpdate' => 'function(id, data){
        $(\'a[rel="tooltip"]\').tooltip();
        $(document).controls();
    }'
)); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $(document).controls();
    });
</script>