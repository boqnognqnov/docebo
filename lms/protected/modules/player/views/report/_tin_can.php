<?php
    $title = empty($lo->title) ? $activity->name : $lo->title;
    $descr = empty($lo->short_description) ? $activity->description : $lo->short_description; 
?>

<div class="statistics-widget statistics-widget-xapi-statements">
    <h4><span style="color: #323232;"><?= $title ?></span></h4>
    <div><span style="color: #323232;"><?= $descr ?></span></div>
    
    <br/>
    
    <div id="loading">
    	<p><?= Yii::t('standard', '_LOADING'); ?></p>
    
    	<div class="progress progress-striped active">
    		<div class="bar" style="width: 100%;"></div>
    	</div>
    </div>
    <div id="theStatements">
    
    </div>
    <div class="text-right">
    	<a id="more" href="#">More</a>
    </div>
    
    <div id="bottom-marker"></div>

</div>

<script type="text/javascript">
/*<![CDATA[*/
           
	var includeRawData = false;
           
	var apActivityNames = [];
	var apActivityDescriptions = [];

    function addHeaders(xhr) {
    	// Add Basic Auth Header
    	xhr.setRequestHeader("Authorization", "<?=$auth?>");
    	// Send the Version header
   		xhr.setRequestHeader("X-Experience-API-Version", "<?= Xapi::VERSION_MAX ?>");
	};

    $(document).ready(function () {

        var filterObject = {};
        var limit = 50;
		var apVersion = <?= $ap->xapi_version ? json_encode($ap->xapi_version) : "null" ?>;
		var actor = '<?= json_encode($actor) ?>';

       	XAPIHelper = new XAPI.Helper({language: <?= json_encode(Yii::app()->getLanguage()) ?>});
		filterObject = {
			"limit":limit,
            "registration":'<?=$registration?>',
            "internal" : 1,
            "apVersion" : apVersion
		};
    		
    	if (apVersion == "0.9") {
        	filterObject.actor = actor;
			filterObject.context = true;
        }
    		
    	else {
        	filterObject.agent = actor;
    	}
    	
    	// TinCan API CALL
    	$.ajax({
    		type:"GET",
    		beforeSend:addHeaders,
    		url:"<?=$lrs_endpoint?>statements",
    		data: filterObject
    	})
    		.done(function (msg) {
   				var data = jQuery.parseJSON(msg);
   				var html = XAPIHelper.renderStatements(data.statements, includeRawData);
   				$("#theStatements").append(html);
    
   				// Handle moreStatementsUrl
   				if(!data.more) {
   					$("#more").hide();
   				} else {
   					$('#more').attr("href", data.more);
   				}
    		})
    		.always(function(){
				$("#loading").hide();        		
        	});
    
    	// More button handler
    	$(".statistics-widget-xapi-statements #more").on("click", function (e) {
    		e.preventDefault();
    		var more_without_slash = $(this).attr("href").substr($(this).attr("href").indexOf('/') + 1);

    		$("#loading").show();

    		
    		$.ajax({
    			type:"GET",
    			beforeSend:addHeaders,
    			url:"<?=$lrs_endpoint?>" + more_without_slash
    		})
    			.done(function (msg) {
   					var data = jQuery.parseJSON(msg);
   					var html = XAPIHelper.renderStatements(data.statements, includeRawData);
   					$("#theStatements").append(html);
   
   					// Handle moreStatementsUrl
   					if(!data.more) {
   						$("#more").hide();
   					} else {
   						$('#more').attr("href", data.more);
   					}
    			})
    			.always(function(){
    				$("#loading").hide();
        		});
    
    	});
    	 
    
    });

	$('#back').live('click', function () {
		var $this = $(this);
		$('#tab-statistics .lrs-statistics').hide();
		$('#tab-statistics .detailed-statistics')
			.html('<div class="align-center"><img src="../templates/standard/images/standard/loadbar.gif" /></div>')
			.show()
			.load($this.attr('href'));
		return false;
	});

/*]]>*/
</script>
