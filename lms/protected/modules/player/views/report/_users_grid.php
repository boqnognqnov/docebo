<div class="row-fluid">
	<div class="player-grey-box">
		<div class="span6" id="report-export">
			<label><?=Yii::t('standard', 'Export as')?></label>
			<?=CHtml::dropDownList('export_report', '',  LearningCourseuser::exportTypes2(), array('id'=>'exportType'))?>
			<a href="javascript:;" onclick="Report.runProgressExportUsers()" class="p-sprite download-transparent-black"></a>
		</div>
		<div id="enrollmentAvailableSearch">
			<form id="reportSearchUsersForm" class="player-search-form" action="<?=$this->createUrl('index', array('course_id'=>$this->course_id))?>" onsubmit="Report.searchUsers();return false;">
				<?=CHtml::button("searchSubmit", array('value'=>'', 'onclick'=>'Report.searchUsers()'));?>
				<?=CHtml::activeTextField($usersModel, 'search_input');?>
			</form>
		</div>
	</div>
</div>
<br>
<?php
	$event = new DEvent($this, array('controller' => $this, 'courseModel' => $courseModel, 'usersModel' => $usersModel));
	Yii::app()->event->raise('OnCourseReportUserGridRender', $event);
	if($event->shouldPerformAsDefault()) {
		$this->widget('DoceboCGridView', array(
			'id' => 'player-reports',
			'dataProvider' => $this->sqlDataProviderCourseReportUsersStats($courseModel->idCourse, $usersModel->search_input, true),
			'columns' => $this->columnsCourseReportUsersStats($courseModel->idCourse),
			'afterAjaxUpdate'=>'function(){
				$(\'[data-toggle="tooltip"]\').tooltip();
				$(document).controls();
			}',
		));
	}
?>