<?php
$this->widget('common.extensions.jqplot.JqplotGraphWidget', array(
	'pluginScriptFile'=>array(
		'jqplot.dateAxisRenderer.js',
		'jqplot.barRenderer.js',
		'jqplot.categoryAxisRenderer.js',
		'jqplot.highlighter.js')
));
//we already have the base training breadcrumb added, so we need to add one in addition
//the course breadcrumb need some additional checks for permissions
$useCourseUrl = true;
if (Yii::app()->user->getIsPu()) {
	if (!Yii::app()->user->checkAccess('/lms/admin/course/view')) {
		$enrollment = LearningCourseuser::model()->findByAttributes(array(
			'idUser' => Yii::app()->user->id,
			'idCourse' => $courseModel->idCourse
		));
		if (empty($enrollment)) {
			$useCourseUrl = false;
		}
	}
}
if ($useCourseUrl) {
	$this->breadcrumbs[Docebo::ellipsis(Yii::t('player', $courseModel->name), 60, '...')] = Docebo::createLmsUrl('player', array('course_id' => $courseModel->idCourse));
} else {
	$this->breadcrumbs[] = Docebo::ellipsis(Yii::t('player', $courseModel->name), 60, '...');
}
$this->breadcrumbs[] = Yii::t('standard', '_REPORTS');
?>

<div class="row-fluid no-space">
	<div class="span12">
		<a class="docebo-back" href="<?=$this->createUrl('/player/report', array('course_id' => $_GET['course_id']))?>">
			<span><?=Yii::t('standard', '_BACK')?></span>
		</a>
		<span class="title-dark"><?=Yii::t('stats', 'User report for') . ' ' . $courseUser->user->clearUserid;?></span>
	</div>
	<div class="span12 player-grey-box">
		<form class="user-report-search" onsubmit="redirectToUser(); return false;">
			<label class=""><?=Yii::t('stats', 'User report for')?></label>
			<?=CHtml::textField('user_names', '', array('id'=>'user_names', 'autocomplete'=>'off'));?>
			<?=CHtml::button(Yii::t('certificate', '_GENERATE'), array('class'=>'user-report-button', 'onclick' => 'redirectToUser()'))?>
			<?=CHtml::hiddenField('selected_user_id', '', array('id'=>'selected_user_id'));?>
		</form>
	</div>
	<h4 class="title-dark"><?=Yii::t('standard', '_DETAILS')?></h4>
	<div class="span12 user-report-course-stats">
		<div class="span6">
			<div class="span7 report-knob-box">
				<img src="<?=$courseUser->user->getAvatarImage(true)?>" class="user-report-avatar" />
				<div class="user-report-userinfo">
					<div class="user-report-username"><?=$courseUser->user->fullName?></div>
					<div class="user-report-email"><?=$courseUser->user->clearUserid?></div>
				</div>
			</div>

			<div class="span5 report-knob-box">
				<div  class="span5 report-knob-text">
					<br>
					<?=Yii::t('standard', '_PROGRESS')?>
				</div>
				<div class="span1"></div>
				<div class="span5">
					<input type="text" class="player-report-progress" value="<?=LearningOrganization::getProgressByUserAndCourse($courseModel->idCourse, $courseUser->idUser)?>" />
				</div>
			</div>
		</div>
		<div class="span6">
			<div class="span6">
				<div class="span4 player-stats-icon-box">
					<div class="p-sprite user-check-black player-stats-icon-big"></div>
				</div>
				<div class="span8">
					<div class="span12 player-stats-counter-big">
						<?=$courseUser->date_inscr?>
					</div>
					<div class="span12 player-stats-label-box">
						<?=Yii::t('report', '_DATE_INSCR')?>
					</div>
				</div>
			</div>
			<div class="span6">
				<div class="span4 player-stats-icon-box">
					<div class="i-sprite large is-calendar-date player-stats-icon-big"></div>
				</div>
				<div class="span8">
					<div class="span12 player-stats-counter-big">
						<?=$courseUser->date_last_access ? $courseUser->date_last_access : '-'?>
					</div>
					<div class="span12 player-stats-label-box">
						<?=Yii::t('standard', '_DATE_LAST_ACCESS')?>
					</div>
				</div>
			</div>
			<div class="span6">
				<div class="span4 player-stats-icon-box">
					<div class="p-sprite books-black player-stats-icon-big"></div>
				</div>
				<div class="span8">
					<div class="span12 player-stats-counter-big">
						<?php
							$data = LearningOrganization::getCompletedAndTotalLOsCount($courseModel->idCourse, $courseUser->idUser);
							if($data['totalItems'])
								echo $data['completed'].' / '.$data['totalItems'];
							else
								echo '0 / 0';
						?>
					</div>
					<div class="span12 player-stats-label-box">
						<?=Yii::t('stats', '_PROGRESS')?>
					</div>
				</div>
			</div>
			<div class="span6">
				<div class="span4 player-stats-icon-box">
					<div class="i-sprite large is-timer player-stats-icon-big"></div>
				</div>
				<div class="span8">
					<div class="span12 player-stats-counter-big">
						<?=Yii::app()->localtime->hoursAndMinutes($userModel->sessionTime)?>
					</div>
					<div class="span12 player-stats-label-box">
						<?=Yii::t('course', '_PARTIAL_TIME')?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="span12">
		<h4 class="title-dark"><?=Yii::t('stats', 'Course access statistics')?></h4>
		<div id="statistics_general_lms_logins" style="height: 250px;"></div>
	</div>
</div>

<div class="row-fluid">
	<div class="span8 timeline-title">
		<h4 class="title-dark"><?=$courseUser->user->fullName?> <?=Yii::t('stats', 'timeline')?></h4>
	</div>
	<div class="span4">
		<div id="timeline-custom-navigation"></div>
	</div>
</div>

<div class="row-fluid">
	<div id="activity-timeline"></div>
</div><br>

<div class="row-fluid info-box-header">
	<h4  class="title-dark" style="float: left;"><?=Yii::t('standard', '_PROGRESS')?></h4>
</div>

<div id="report-bu-user-grid" class="report-grid-box">
	<?=$this->renderPartial('_user_objects_grid', array(
		'learningOrganization' => $learningOrganization,
		'objectsDataProvider' => $objectsDataProvider,
		'courseModel' => $courseModel,
	))?>
</div>
<div id="report-info-box" style="display: none;">
	<br>
	<div class="row-fluid info-box-header">
		<a class="docebo-back" href="javascript:;" onclick="Report.hideInfoBox('report-bu-user-grid')"><?=Yii::t('standard', '_BACK')?></a>
	</div>
	<br>
	<div class="report-ajax-loader"></div>
	<div id="report-info-box-content"></div>
</div>


<style>
	div#dialog_reset_user_lo_stats div#warning-strip,
	div.bootbox.modal.change-score div#warning-strip,
	div.bootbox.modal.change-status div#warning-strip {
		margin-bottom: 20px;
	}

	div.bootbox.modal.change-status div.modal-body label,
	div.bootbox.modal.change-score div.modal-body label {
		margin-left: 25px;
	}

	a.lo-editable.lo-status-editable + div.popover.top > div.arrow {
		left: 60%;
	}
</style>

<script>
	function addHeaders(xhr) {
		// Add Basic Auth Header
		xhr.setRequestHeader("Authorization", "<?=$auth?>");
	}
	Report.init({
		course_id: <?=$this->course_id?>,
		objectsGridId: 'player-objects',
		exportObjectsUrl: '<?=$this->createUrl('exportObjects', array('course_id'=>$this->course_id))?>',
		progressObjectsExportUrl: '<?= Docebo::createAbsoluteLmsUrl('progress/run', array('type' => Progress::JOBTYPE_COURSE_OBJECTS_REPORT, 'course_id'=>$this->course_id)) ?>'
	});

	$(function(){
		var timeline;
		var data = [];

		<?php if (!empty($activityTimeline['data'])) : ?>
		<?php foreach($activityTimeline['data'] as $_date) : ?>
		<?php $_start = strtotime($_date['start']); ?>
		<?php $_end = (!empty($_date['end'])) ? strtotime($_date['end']) : null; ?>
		data.push({
			'start':new Date(<?= date('Y', $_start) ?>, <?= date('n', $_start)-1; ?>, <?= date('d', $_start) ?>, <?= date('H', $_start) ?>, <?= date('i', $_start) ?>, <?= date('s', $_start) ?>),
			<?php if ($_end) : ?>
			'end':new Date(<?= date('Y', $_end) ?>, <?= date('n', $_end)-1; ?>, <?= date('d', $_end) ?>, <?= date('H', $_end) ?>, <?= date('i', $_end) ?>, <?= date('s', $_end) ?>),
			<?php endif; ?>
			<?php if ($_date['isKeyEvent'] == true) : ?>
			'content':'<div class="key-event"><?= CHtml::encode($_date['content']) ?></div>'
			<?php else : ?>
			'content':'<div><?= CHtml::encode($_date['content']) ?></div>'
			<?php endif; ?>
		});
		<?php endforeach; ?>
		<?php endif; ?>
		<?
		if (isset($activityTimeline['data']) && count($activityTimeline['data']))
		{
			$lastAccess = $activityTimeline['data'][count($activityTimeline['data'])-1]['start'];
			$lastAccess = strtotime($lastAccess) * 1000 + (1000 * 60 * 60 * 12);
		}
		else
		{
			$lastAccess = time() + (1000 * 60 * 60 * 12);
		}
		?>
		// specify options
		var end = <?=$lastAccess?>; //in milliseconds, shifted with 10 hours for more good UI
		var start = end - (14 * 60 * 60 * 24 * 1000); // in milliseconds, 14 days ago
		var options = {
			/*"width":"100%",*/
			"start" : start,
			"end" : end,
			"showNavigation": true,
			"height":"300px",
			"style":"box",
			<?php $_minDate = (!empty($activityTimeline['minDate'])) ? strtotime($activityTimeline['minDate']) - (3600 * 24 * 31) : null; ?>
			<?php $_maxDate = (!empty($activityTimeline['maxDate'])) ? strtotime($activityTimeline['maxDate']) + (3600 * 24 * 31) : null; ?>
			<?php if ($_minDate) : ?>
			// lower limit of visible range
			"min":new Date(<?= date('Y', $_minDate) ?>, <?= date('n', $_minDate)-1 ?>, <?= date('d', $_minDate) ?>, <?= date('H', $_minDate) ?>, <?= date('i', $_minDate) ?>, <?= date('s', $_minDate) ?>),
			<?php endif; ?>
			<?php if ($_maxDate) : ?>
			// upper limit of visible range
			"max":new Date(<?= date('Y', $_maxDate) ?>, <?= date('n', $_maxDate)+1 ?>, <?= date('d', $_maxDate) ?>, <?= date('H', $_maxDate) ?>, <?= date('i', $_maxDate) ?>, <?= date('s', $_maxDate) ?>)
			<?php endif; ?>
			//"intervalMin": 1000 * 60 * 60 * 24,          // one day in milliseconds
			//"intervalMax": 1000 * 60 * 60 * 24 * 31 * 10  // about three months in milliseconds
		};

		// Redefine MouseWheel event to zoom - effectively disabling it
		links.Timeline.prototype.onMouseWheel = function(event) {
			return;
		}

		// Instantiate our timeline object.
		timeline = new links.Timeline(document.getElementById('activity-timeline'));

		// Draw our timeline with the created data and options
		timeline.draw(data, options);


		// CUSTOM Navigation: Move the navigation outside timeline frame
		// See also CSS
		$('.timeline-navigation').prependTo('#timeline-custom-navigation');



		$(".player-report-progress").knob({
			'min':0,
			'max':100,
			'readOnly': true,
			//'displayInput': false,
			'fgColor': '#6cc267',
			'bgColor': '#cccccc',
			'thickness':.25,
			'width': 80,
			'height':80,
			'draw' : function () {
				$(this.i).val(this.cv + '%')
			}
		});

		<?php
		$_accessValues = array_values($lmsLogins);
		$_accessDates = array_keys($lmsLogins);
		foreach ($_accessDates as &$_date) {
			$_date = "'". date('m/d/y', strtotime($_date)) ."'";
		}
		?>
		var s1 = [<?= implode(', ', $_accessValues); ?>];
		var ticks = [<?= implode(',', $_accessDates); ?>];
		var line1 = [];
		for (var i = 0; i < s1.length; i++)
			line1.push([ticks[i], s1[i]]);
		$.jqplot.config.enablePlugins = true;
		var plot1 = $.jqplot('statistics_general_lms_logins', [line1], {
			gridPadding:{right:35},
			axes:{
				xaxis:{
					renderer:$.jqplot.DateAxisRenderer,
					tickOptions:{formatString:'%b %#d'}
				}
			},
			series:[{lineWidth:4, markerOptions:{style:'filledCircle'}}],
			grid: {
				background: '#f5f5f5',
				shadow: false,
				borderColor: '#CCC',
				borderWidth: 1,
				gridLineColor: '#fff'
			},
			seriesDefaults: {
				color: '#0465ac'
			},
			highlighter: {
				lineWidthAdjust: 2.5,   // pixels to add to the size line stroking the data point marker
				// when showing highlight.  Only affects non filled data point markers.
				sizeAdjust: 5,          // pixels to add to the size of filled markers when drawing highlight.
				showTooltip: true,      // show a tooltip with data point values.
				tooltipLocation: 'n',  // location of tooltip: n, ne, e, se, s, sw, w, nw.
				fadeTooltip: false,      // use fade effect to show/hide tooltip.
				tooltipFadeSpeed: "fast",// slow, def, fast, or a number of milliseconds.
				tooltipOffset: 2,       // pixel offset of tooltip from the highlight.
				tooltipAxes: 'y',    // which axis values to display in the tooltip, x, y or both.
				tooltipSeparator: ', ',  // separator between values in the tooltip.
				useAxesFormatters: false, // use the same format string and formatters as used in the axes to
				// display values in the tooltip.
				tooltipFormatString: '%.0f' // sprintf format string for the tooltip.  only used if
				// useAxesFormatters is false.  Will use sprintf formatter with
				// this string, not the axes formatters.
			}

		});
		// make it scalable with the browser window
		$(window).resize(function (event, ui) {
			plot1.replot( { resetAxes: true } );
		});

		var searchUrl = '<?=$this->createUrl('/player/report/searchCourseUsers')?>';
		$( "#user_names" ).autocomplete({
			source: function( request, response ) {
				$.ajax({
					url: searchUrl,
					dataType: "json",
					data: {
						query: request.term
					},
					success: function( data ) {
						response( $.map( data, function( item ) {
							return {
								label: item.label,
								value: item.id
							}
						}));
					}
				});
			},
			minLength: 2,
			select: function( event, ui ) {
				if(ui.item)
				{
					$('#user_names').val(ui.item.label);
					$('#selected_user_id').val(ui.item.value);
					return false; // Prevent the widget from inserting the value.
				}
				else
					$('#selected_user_id').val('');
			}
		});
	});

	function redirectToUser()
	{
		var redirectUrl = '<?=$this->createUrl('byUser', array('course_id'=>$this->course_id))?>';
		var id = $('#selected_user_id').val();
		if (id)
			Report.changeUserSelection(redirectUrl, id);
		else
			bootbox.alert("<?=Yii::t('register', '_INEXISTENT_USER')?>");
	}

</script>