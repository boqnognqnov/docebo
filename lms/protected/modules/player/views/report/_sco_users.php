<h5 class="report-ajax-title"><span><?=Yii::t('storage', '_LONAME_scormorg')?> - <?=$scormItem->scorm_org->title?></span> - <?=$scormItem->title?></h5>
<div class="row-fluid">
	<div class="player-grey-box">
		<div class="span6" id="report-export">
			<label><?=Yii::t('standard', 'Export as')?></label>
			<?=CHtml::dropDownList('export_report', '',  LearningCourseuser::exportTypes2(), array('id'=>'exportScoType'))?>
			<a href="javascript:;" onclick="Report.runProgressExportSingleObject(null, <?= (int) $scormItemId ?>)" class="p-sprite download-transparent-black"></a>
		</div>
		<div id="enrollmentAvailableSearch">
			<form id="reportSearchScoUsersForm" class="player-search-form" action="<?=$this->createUrl('index', array('course_id'=>$this->course_id))?>" onsubmit="Report.searchScoUsers();return false;">
				<?=CHtml::button("searchSubmit", array('value'=>'', 'onclick'=>'Report.searchScoUsers()'));?>
				<?=CHtml::activeTextField($usersModel, 'search_input');?>
			</form>
		</div>
	</div>
</div>
<br>
<a href="<?= $this->createUrl('axAnswersBreakdown', array('objectId'=>$scormItemId)) ?>" class="btn btn-docebo big black pull-right open-dialog" data-dialog-class="modal-report-answers-breakdown"><?= Yii::t('report', 'Answers breakdown') ?></a>
<?php
Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;
$this->widget('DoceboCGridView', array(
	'id' => 'player-reports-sco-users',
	'dataProvider' => $usersModel->searchUsersByCourseAndScorm($this->course_id, $scormItemId),
	'afterAjaxUpdate' => 'function(id, data){
			 	$(document).controls();

			}',
	'columns' => array(
		array(
			'name' => Yii::t('standard', '_USERNAME'),
			'value' => '$data->learningScormItemsTrack ? "<a title=\"".$data->learningScormItemsTrack->scormItem->title."\" class=\"open-dialog\" data-dialog-class=\"modal-report-user-sco-stats\" href=\"".$data->learningScormItemsTrack->renderScoXmlReportUrl()."\">".$data->clearUserId."</a>" : $data->clearUserId',
			'type' => 'raw',
		),
		'firstname',
		'lastname',
		array(
			'name' => Yii::t('standard', '_STATUS'),
			'value' => '$data->learningScormItemsTrack->status ? $data->learningScormItemsTrack->statusLabel : LearningScormItemsTrack::getStatusValue(LearningScormItemsTrack::STATUS_NOT_ATTEMPTED)',
			'type' => 'raw',
		),
		array(
			'name' => Yii::t('standard', '_SCORE'),
			'value' => '$data->learningScormItemsTrack ? $data->learningScormItemsTrack->renderScore() : "-"',
			'type' => 'raw',
		),
		array(
			'value' => '$data->learningScormItemsTrack ? "<a class=\"open-dialog\" data-dialog-class=\"modal-report-user-sco-stats\" href=\"".$data->learningScormItemsTrack->renderScoXmlReportUrl()."\"><span class=\"docebo-sprite ico-magnifier\"></span></a>" : ""',
			'type' => 'raw',
		)
	),
)); ?>

<script type="text/javascript">
	Report.reportScoUsersGridId = 'player-reports-sco-users';
	Report.exportScoUsersUrl = '<?=$this->createUrl('exportScoUsers', array('course_id' => $this->course_id, 'objectId' => $scormItemId))?>';
</script>