<div class="row-fluid">
	<div class="player-grey-box">
		<div class="span6" id="report-export">
			<label><?=Yii::t('standard', 'Export as')?></label>
			<?=CHtml::dropDownList('export_report', '',  LearningCourseuser::exportTypes2(), array('id'=>'exportObjectsType'))?>
			<a href="javascript:;" onclick="Report.runProgressExportObjects()" class="p-sprite download-transparent-black"></a>
		</div>
		<div id="enrollmentAvailableSearch">
			<form id="reportSearchObjectsForm" class="player-search-form" action="<?=$this->createUrl('index', array('course_id'=>$this->course_id))?>" onsubmit="Report.searchObjects();return false;">
				<?=CHtml::button("searchSubmit", array('value'=>'', 'onclick'=>'Report.searchObjects()'));?>
				<?=CHtml::textField('objects_search');?>
			</form>
		</div>
	</div>
</div>
<br>
<?php
$this->widget('DoceboCGridView', array(
	'id' => 'player-objects',
	'dataProvider' => $objectsDataProvider,
	'rowCssClassExpression'=>'$data->objectType	 ? "odd parent" : "odd"',
	'columns' => array(
		array(
			'header' => Yii::t('standard', '_TITLE'),
			'value' => '$this->grid->controller->widget("ReportsGridFormatter", array("stats"=>$data, "column"=>"title"), true);',
			'type' => 'raw',
		),
		array(
			'header' => Yii::t('standard', '_STATUS'),
			'value' => '$this->grid->controller->widget("ReportsGridFormatter", array("stats"=>$data, "column"=>"status"), true);',
			'type' => 'raw',
		),
// 		array(
// 			'header' => Yii::t('player', 'Average time'),
// 			'value' => '$this->grid->controller->widget("ReportsGridFormatter", array("stats"=>$data, "column"=>"avgTime"), true);',
// 			'type' => 'raw',
// 		),
		array(
			'header' => Yii::t('standard', '_AVERANGE'),
			'value' => '$this->grid->controller->widget("ReportsGridFormatter", array("stats"=>$data, "column"=>"avgScore"), true);',
			'type' => 'raw',
		),
	),
)); ?>