<?php /* @var $this Controller */

$this->beginContent('//layouts/lms');

if (Yii::app()->controller->id != 'report')
{
	$this->widget('player.components.widgets.CourseHeader', array(
		'course_id' => Yii::app()->request->getQuery('course_id'),
		'session_id' => Yii::app()->request->getQuery('session_id'),
        'userCanAdminCourse' => $this->userCanAdminCourse
	));
}
//echo '<div class="clearfix"></div>';

echo $content;

$this->endContent();