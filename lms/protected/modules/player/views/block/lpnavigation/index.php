<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 26.10.2015 �.
 * Time: 17:27
 *
 * @var $isPartOfLp boolean
 * @var $courseModel LearningCourse
 */
?>
<hr>
<div class="curricula-navigator-widget">
<?php
if($enrolled):
?>
    <div class="row-fluid">
        <div class="span<?=$size === 'big' ? '1' : '2'?>">
            <div class="curricula-progress">
                <div id="player-course-percentgraph">
                    <input class="player-course-graph-<?= $data->id_path ?>" value="<?php echo $pathData['percentage'] ?>" style="display: none;">
                    <div id="player-course-percentage">
                        <?php echo $pathData['percentage'] ?><span>%</span>
                    </div>
                </div>
                <script type="text/javascript">
                    $(function() {
                        $(".player-course-graph-<?= $data->id_path ?>").knob({
                            'min': 0,
                            'max': 100,
                            'readOnly': true,
                            'fgColor': '<?= ($pathData['percentage'] == 0 ? '#E4E6E5' : '#53AB53'); ?>',
                            'bgColor': '<?= ($pathData['percentage'] == 100 ? '#53AB53' : '#E4E6E5'); ?>',
                            'width': 66,
                            'height': 66,
                            'thickness': .25,
                            'value': 0
                        });
                    });
                </script>
            </div>
        </div>
        <div class="span<?=$size === 'big' ? '11' : '10'?> curricula-widget-info">
            <div class="row-fluid">
                <p class="lead"><?=$data->path_name?></p>
            </div>
            <div class="row-fluid">
                <span class="curricula-widget-progress-info"><?= Yii::t('curricula_management', 'You have completed <span class="green-text">{done}</span>/<b>{all}</b> courses', array(
                    '{done}' => $pathData['done'],
                    '{all}' => $pathData['total']
                ))?></span>
            </div>
        </div>
    </div>
    <br>
    <div class="row-fluid">
        <div class="curricula-details">
            <?php if ($courses) : ?>
                <?php foreach($courses as $id => $course): ?>
                    <div class="dropdown-row drop">
                        <div class="dropdown-context">
                            <?php
                            $courseType = '';
                            switch ($course->course_type) {
                                case LearningCourse::TYPE_ELEARNING:
                                    $courseType = 'E-Learning';
                                    break;
                                case LearningCourse::TYPE_CLASSROOM:
                                    $courseType = 'Classroom';
                                    break;
                                case LearningCourse::TYPE_MOBILE:
                                    $courseType = 'Mobile';
                                    break;
                                default:
                                    $courseType = ucfirst($course->course_type);
                                    break;
                            }
                            $isFailed = false;
                            $catchup = null;
                            $catchupEnrollment = null;
                            $isFailed = $course->isCourseFailedByUser(Yii::app()->user->id);

                            $enrollment = $courseUser[$course->idCourse];
                            $canHaveCatchup = $course->isCatchupType();
                            if($enrollment)
                                $canEnterCourse = $enrollment->canEnterCourse();
                            else
                                $canEnterCourse = array('can' => false);
                            $locked = $pathCourses[$id]->isLocked() || !$canEnterCourse['can'];

                            if ($canHaveCatchup) {
                                $catchup = LearningCourseCatchUp::loadAssignedCatchup($data->id_path, $course->idCourse);
                                if ($catchup) {
                                    $catchupEnrollment = LearningCourseuser::model()->findByAttributes(array('idCourse' => $catchup->id_catch_up, 'idUser' => Yii::app()->user->id));
                                    if(!$catchupEnrollment)
                                        LearningCourseuser::subscribeUsersToCourses(array(Yii::app()->user->id), array($catchup->id_catch_up));
                                }
                            }

                            $started = false; // The person is not started the course
                            if ($enrollment->date_first_access != NULL ) // If the person access the course date_first_access is not empty
                                $started = true; // The person has played the course once
                            ?>
                            <?php
                            // Only show course price and selection checkbox if the LP is for sale and the user has not purchased this course already
                            if($data->is_selling && (!$enrollment || $enrollment->status==LearningCourseuser::$COURSE_USER_WAITING_LIST)):
                                $hasCoursesToBuy = true;
                                if($data->purchase_type == LearningCoursepath::PURCHASE_TYPE_SINGLE_COURSES_IN_SEQUENCE) { ?>
                                    <div class="curricula-course-list-buy">
                                        <?=CHtml::checkBox('', false, array(
                                            'class'=>'course-check-to-buy sequence-'.$course->sequence,
                                            'data-idCourse'=>$course->idCourse,
                                            'data-price'=>$pathCourses[$id]->getCoursePrice(),
                                            'data-sequence'=>$pathCourses[$id]->sequence,
                                            'data-idPath'=>$data->id_path
                                        ))?>
                                    </div>
                                <?php } else { ?>
                                    <div class="curricula-course-list-icon">
                                        <i class="i-sprite is-lock black" title="<?= CHtml::encode(Yii::t('coursepath', 'Users must buy the full learning plan')) ?>" rel="tooltip"></i>
                                    </div>
                                <?php } ?>
                            <?php else: ?>
                                <div class="curricula-course-list-icon">
                                    <?php if ($canHaveCatchup && ($enrollment->status != LearningCourseuser::$COURSE_USER_END) && $isFailed) : ?>
                                        <?php
                                        if(!$locked)
                                            echo '<a href="'.Yii::app()->createUrl('player', array('course_id' => $course->idCourse, 'coming_from' => 'lp', 'id_plan' => $data->id_path)).'">';
                                        ?>
                                        <div class="p-sprite circle-arrow-small-red"></div>
                                        <?php
                                        if(!$locked)
                                            echo '</a>';
                                        ?>
                                    <?php else : ?>
                                        <?php if ($enrollment && $enrollment->status == LearningCourseuser::$COURSE_USER_END) : ?>
                                            <?php
                                            if(!$locked)
                                                echo '<a href="'.Yii::app()->createUrl('player', array('course_id' => $course->idCourse, 'coming_from' => 'lp', 'id_plan' => $data->id_path)).'">';
                                            ?>
                                            <div class="p-sprite circle-check-small-green"></div>
                                            <?php
                                            if(!$locked)
                                                echo '</a>';
                                            ?>
                                        <?php elseif($locked): ?>
                                            <i class="i-sprite is-lock black" title="<?= CHtml::encode($pathCourses[$id]->getPrerequisiteMessage($enrollment)) ?>" rel="tooltip" data-html="true"></i>
                                            <!-- If the learner has not started the course, it's not locked for him and he is enrolled (grey arrow) -->
                                        <?php elseif(!$started && !$locked && $enrollment):?>
                                            <a href="<?= Yii::app()->createUrl('player', array('course_id' => $course->idCourse, 'coming_from' => 'lp', 'id_plan' => $data->id_path)); ?>">
                                                <i class="i-sprite is-play grey"></i>
                                            </a>
                                        <?php else: ?>
                                            <?php
                                            if(!$locked && $enrollment)
                                                echo '<a href="'.Yii::app()->createUrl('player', array('course_id' => $course->idCourse, 'coming_from' => 'lp', 'id_plan' => $data->id_path)).'">';
                                            ?>
                                            <div class="p-sprite circle-arrow-small-orange"></div>
                                            <?php
                                            if(!$locked && $enrollment)
                                                echo '</a>';
                                            ?>
                                        <?php endif; ?>
                                    <?php endif; ?>

                                </div>
                            <?php endif; ?>
                            <div class="curricula-course-thumbnail">
                                <?= CHtml::image($course->getCourseLogoUrl(CoreAsset::VARIANT_SMALL, true), $course->name, array(
                                    'width' => 35,
                                    'height' => 35
                                )) ?>
                            </div>
                            <div class="curricula-course-list-title">
                                <?php if ($locked && !$isFailed): ?>
                                    <?php echo $course->name ?>
                                <?php else: ?>
                                    <a href="<?php echo Yii::app()->createUrl('player', array('course_id' => $course->idCourse, 'coming_from' => 'lp', 'id_plan' => $data->id_path)) ?>"><?php echo $course->name ?></a>
                                <?php endif; ?>
                            </div>
                            <div class="curricula-course-list-type">
                                <?php echo $courseType ?>
                            </div>
                        </div>
                        <?php
                        if ( $data->catchup_enabled && $catchup && $canHaveCatchup ){
                            $this->renderPartial('lms.protected.modules.curricula.views.show._catchupRow', array('catchup' => $catchup, 'catchupEnrollment' => $catchupEnrollment, 'pathModel' => $data, 'courseModel' => $course, 'isFailed' => $isFailed))
                            ?>
                            <style type="text/css">
                                .catch-up-warning{
                                    display: inline-block;
                                    background-color: #FFE59A;
                                    width: 100%;
                                }
                                .catch-up-warning .pull-left:nth-child(1){
                                    display: inline-block;
                                    margin: 15px 17px;
                                }
                                .catch-up-warning .pull-left i{
                                    font-size: 26px;
                                }
                                .catch-up-warning .pull-left:nth-child(2){
                                    display: inline-block;
                                    max-width: 430px;
                                    font-size: 13px;
                                    margin-top: 5px;
                                    margin-bottom: 5px;
                                    line-height: 16px;
                                }
                                .catch-up-course-to-play{
                                    background-color: #F1F3F2;
                                    display: inline-block;
                                    width: 100%;
                                }
                                .catch-up-course-to-play .pull-left .fa-life-buoy{
                                    margin: 20px;/* 20px;*/
                                }
                                .catch-up-course-to-play .pull-left.catchup-course-logo{
                                    width: 35px;
                                    height: 35px;
                                    margin-top: 10px;
                                    margin-bottom: 10px;
                                }
                                .catch-up-course-to-play .pull-left.catchup-course-info{
                                    margin-left: 15px;
                                    line-height: 16px;
                                    margin-top: 9px;
                                    font-size: 13px;
                                }
                                .catch-up-course-to-play .pull-right.catchup-course-play-icon .curricula-course-list-icon{
                                    height: 30px;
                                    width: 40px;
                                    margin-right: 10px;
                                    text-align: center;
                                    margin-top: 15px;
                                }
                            </style>
                        <?php } ?>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
            </div>
    </div>
    <br/>
<script type="text/javascript">
    $(function(){
        $('.curricula-navigator-widget .dropdown-row.header').on('click', function(event){
            dropdownClick($(this));
            return false;
        });
    });
</script>
<?php
    else:
        echo "<strong>" . $msg . "</strong><br><br>";
    endif;
?>
    <div class="row-fluid">
        <small><a class="pull-right" href="<?= Docebo::createAbsoluteUrl('curricula/show')?>">View Learning Plans list</a></small>
    </div>
</div>
