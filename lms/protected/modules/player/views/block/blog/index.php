<?php
/* @var $blockModel */
/* @var $idCourse int */
/* @var $model MyblogPost */
?>
<div class="player-blog-block">
    <div class="create-post-container">
        <a class="btn-docebo green big open-dialog"
           data-dialog-class="modal-edit-blogpost"
           closeOnOverlayClick="false"
           closeOnEscape="false" href="<?= Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/axEdit', array('idCourse' => $idCourse)) ?>"><?= Yii::t('myblog', 'New Blog Post') ?></a>
    </div>

    <?php
    $this->widget('BlogPostList', array(
        'idCourse'=> $idCourse,
        'isAdminMode' => false
    ));
    ?>

</div>
