<?php
/**
 * Created by PhpStorm.
 * User: Asen Nikolov
 * Date: 14-Aug-15
 * Time: 11:58 AM
 */
if ($attached) {
	echo $blockModel->getContent($options); // call behavior's method
} // .. Or maybe we don't have it defined yet??
else {
	echo Yii::t("standard", "_OPERATION_FAILURE") . $blockModel->content_type;
}