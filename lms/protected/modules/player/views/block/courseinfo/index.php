<?php

// Is there a certificate attached to this COURSE (!) ?
$courseCertificate = LearningCertificateCourse::model()->findByAttributes(array(
	'id_course' => $this->course_id,
));


$courseUser = LearningCourseuser::model()->findByAttributes(array("idUser" => Yii::app()->user->id, "idCourse" => $this->course_id));

// Generate if not yet generated a possible certificate for this USER+COURSE
$userCertificate = LearningCertificateAssign::loadByUserCourse(Yii::app()->user->id, $this->course_id);

$learningObjects = LearningOrganization::getCompletedAndTotalLOsCount($this->course_id, Yii::app()->user->id);
$completedPercentage = LearningOrganization::getProgressByUserAndCourse($this->course_id, Yii::app()->user->id);

$totalSpentTime = LearningTracksession::getTotalTime($this->course_id, Yii::app()->user->id);
$totalSpentHours = floor($totalSpentTime / 3600);
$totalSpentMinutes = round(($totalSpentTime - (3600*$totalSpentHours)) / 60);
$totalViews = LearningTracksession::getTotalViews($this->course_id, Yii::app()->user->id);

//An event that returns a link to download the branch certificate for the user
$branchCertificate = Yii::app()->event->raise('DisplayBranchCertificate', new DEvent($this, array(
	'userId' => Yii::app()->user->id,
	'courseId' => $this->course_id,
	'blockModel' => $blockModel
)));

?>
<div class="row-fluid">
	<div class="span4" id="player-courseinfo-user">
		<?php if ($courseUser) : ?>
			<?php echo Yii::app()->user->getAvatar('pull-left avatar'); ?>
			<h4><?=Yii::app()->user->getDisplayName()?></h4>
			<p><?=Yii::t('report', '_DATE_INSCR')?>:<?=$courseUser->date_inscr?></p>
			<p><?=Yii::t('standard', '_DATE_LAST_ACCESS')?>: <?=$courseUser->date_last_access ? $courseUser->date_last_access : "-"?></p>
		<?php endif; ?>
	</div>
	<div class="span4">
		<div class="row-fluid">
			<?php if(is_array($branchCertificate) && isset($branchCertificate['html'])):?>
				<?= $branchCertificate['html']; ?>
			<?php elseif($courseCertificate):?>
				<div class="pull-left" id="player-certificate-info">
					<i class="p-sprite certificate" id="player-certificate-medal"></i>
				</div>
				<h4><?php echo Yii::t('player', 'Certificate');?></h4>
				<?php if ($userCertificate) : ?>
					<a id="dl-cert-<?=$blockModel->id?>" class="dl-certificate" href="<?=$this->createUrl('/player/block/downloadCertificate', array('course_id' => $this->course_id))?>" target="_blank">
						<?=Yii::t('certificate', '_TAKE_A_COPY')?> <i class="p-sprite download-transparent-black"></i>
					</a>
				<?php else : ?>
						<span><?= Yii::t('player', 'The selected certificate will be available to the user after the course completion') ?></span>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
<!--
	<div class="span4">
		<div class="row-fluid">
			<div class="span3">
				<div class="p-sprite flag-black" id="player-completed-flag"></div>
			</div>
			<div class="span9" id="player-completed-info">
				Must be completed by <?=$courseUser->date_expire_validity?>
			</div>
		</div>
	</div>
-->
	<div class="span3" id="player-timinings">
		<h4><?php echo Yii::t('course', 'My Activities'); ?></h4>
		<p>
			<i class="p-sprite little-clock"></i> <?=$totalSpentHours?> <?=Yii::t('standard', '_HOURS')?> <?=$totalSpentMinutes?> <?=Yii::t('standard', '_MINUTES')?>
		</p>
	</div>

	<div class="span1">
		<div id="player-course-percentgraph">
			<input id="graph-<?=$blockModel->id?>" class="player-course-graph" data-bgcolor="#EEEEEE" data-fgcolor="#5FBF5F" value="<?=$learningObjects['completed']?>" style="display: none;" />
			<div id="player-course-percentage">
				<?=$completedPercentage?><span>%</span>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(function(){
	//$(".player-course-graph")
	$('#graph-<?=$blockModel->id?>').knob({
		'min':0,
		'max':<?=$learningObjects['totalItems']?>,
		'readOnly': true,
// 		'fgColor': '#53AB53',
// 		'bgColor': '#666777',
		'width': 66,
		'height': 66,
		'thickness':.25,
		'value': 0
	});


});
</script>