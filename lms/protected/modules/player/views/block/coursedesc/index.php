<?php
/** @var $widget PlayerBlockCoursedesc */
$params = CJSON::decode($blockModel->params);
$showDesc = $params['show_desc'];
$additionalFields = $widget->getAdditionalFieldsToDisplay();

?>
<div class="course-description">
	
    <?php if($showDesc !== null || empty($params)): ?>
	<p>
		<?php
			$description =  Yii::app()->htmlpurifier->purify($courseModel->description);
			echo htmlspecialchars_decode(htmlentities($description, ENT_NOQUOTES, 'UTF-8'), ENT_NOQUOTES);
		?>
	</p>
    <?php endif; ?>

    <?php if(!empty($additionalFields)):?>
    <hr>
    <?php foreach($additionalFields as $fieldName => $fieldItem): ?>
    <?php $fieldValue = $fieldItem['value']; ?>
    <?php $inColumn = $fieldItem['in_column']; ?>
    <div class="row-fluid course-field-row">
        <?php if($inColumn):?>
        <div class="span4">
            <b><?= $fieldName ?>:</b>
        </div>
        <div class="span8">
            <p><?=$fieldValue?></p>
        </div>
        <?php else: ?>
        <div><b><?= $fieldName ?></b></div>
        <div style="margin-bottom: 20px;"><?= $fieldValue ?></div>
        <?php endif; ?>
    </div>
    <?php endforeach;?>
    <hr>
    <?php endif; ?>
</div>