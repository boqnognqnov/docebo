<?php

//initialize instructors list
$instructors = array();

//check user access madality
if (Yii::app()->user->getIsGodAdmin()) {
	//godadmin
	$showSpecial = true;
} elseif (Yii::app()->user->getIsAdmin()) {
	//power user
	// Is this course assigned to our power user
	$pUserCourseAssigned = CoreUserPuCourse::model()->findByAttributes(array(
		'puser_id' => Yii::app()->user->id,
		'course_id' => $blockModel->course_id
	));
	$showSpecial = ($pUserCourseAssigned && Yii::app()->user->checkAccess('/lms/admin/course/mod'));
} else {
	//normal users
	$showSpecial = false;
}

//check if we are into a classroom session or a generic course
if (!empty($idSession)) {
	
	if ($showSpecial) {
		
		//some allowed admins may enter in the course also if they are not enrolled, a different method is needed here
		$instructors = LtCourseuserSession::findSessionInstructors($idSession);
		
	} else {
	
		//search for instructor(s) in specified session
		$courseUserSession = LtCourseuserSession::model()->findByAttributes(array(
			'id_user' => Yii::app()->user->id,
			'id_session' => $idSession
		));
		if ($courseUserSession) {
			$instructors = $courseUserSession->findInstructors();
		} else {
			//...
		}
	}
	
} else if ($showSpecial) {
		
	//some allowed admins may enter in the course also if they are not enrolled, a different method is needed here
	$instructors = LearningCourseuser::findCourseInstructors($this->course_id);

} else {

	//generic search of instructor(s) for entire course
	$courseUser = LearningCourseuser::model()->findByAttributes(array(
		'idUser' => Yii::app()->user->id,
		'idCourse' => $this->course_id
	));
	if ($courseUser) {
		$instructors = $courseUser->findInstructors();
	} else {
		//...
	}
}


if (!empty($instructors)) :
?>
<div class="myinstructor">
	
	<table>
		<tbody>
			<?php 
				foreach ($instructors as $instructor): 
					if ($instructor->getPrimaryKey() != Yii::app()->user->id):
			?>
			<tr>
				
				<td class="photo">
					<?= $instructor->getAvatarImage() ?>
				</td>

				<td class="name">
					<span><?= $instructor->getFullName() ?></span>
				</td>
				
				<td class="email">
					<?php if (!empty($instructor->email)): ?>
					<a class="i-sprite is-env-closed blue" href="mailto:<?= $instructor->email ?>"></a>
					<?php else: ?>
					&nbsp;
					<?php endif; ?>
				</td>
				
			</tr>
			<?php 
					endif;
				endforeach; 
			?>
		</tbody>
	</table>
	
</div>
<?php
else:
	echo '('.Yii::t('classroom', 'No instructors').')';
endif;
?>