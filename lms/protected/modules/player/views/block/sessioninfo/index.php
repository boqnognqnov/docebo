<?php

$courseUser = LearningCourseuser::model()->findByAttributes(array(
	"idUser" => Yii::app()->user->id, 
	"idCourse" => $this->course_id
));


$courseUserSession = LtCourseuserSession::model()->findByAttributes(array(
	"id_user" => Yii::app()->user->id, 
	"id_session" => $idSession
));


$userCertificate = LearningCertificateAssign::loadByUserCourse(Yii::app()->user->id, $this->course_id);

/*
$learningOrg =  new LearningOrganization();
$totalCount = count($learningOrg->byCourse($this->course_id)->findAll());
$completedCount = count($learningOrg->byCourse($this->course_id)->completedByUser(Yii::app()->user->id)->findAll());
if ($totalCount > 0)
	$completedPercentage = round($completedCount/$totalCount*100);
else
	$completedPercentage = 0;

$totalSpentTime = LearningTracksession::getTotalTime($this->course_id, Yii::app()->user->id);
$totalSpentHours = floor($totalSpentTime / 3600);
$totalSpentMinutes = round(($totalSpentTime - (3600*$totalSpentHours)) / 60);
$totalViews = LearningTracksession::getTotalViews($this->course_id, Yii::app()->user->id);
*/

$totalSpentHours = 0;
$totalSpentMinutes = 0;

$completedCount = 0;
$completedPercentage = 0;

//$courseUserSession->total_hours

$sessionModel = LtCourseSession::model()->findByPk($idSession);
$dates = $sessionModel->getDates();
$totalSeconds = 0;
$totalSpentSeconds = 0;
foreach ($dates as $date) {
	$count = $date->countSeconds();
	$totalSeconds += $count;
	if ($date->hasAttended(Yii::app()->user->id)) {
		$totalSpentSeconds += $count;
	}
}

$totalSpentTime = $totalSpentSeconds;
$totalSpentHours = floor($totalSpentTime / 3600);
$totalSpentMinutes = round(($totalSpentTime - (3600*$totalSpentHours)) / 60);

// TODO: "completed" refers to hours or days?
$totalHours = ($totalSeconds / 3600);
$completedCount = ($totalSpentSeconds / 3600);
$completedPercentage = (round(($totalSpentSeconds / $totalSeconds) * 100));


?>
<div class="row-fluid">
	<div class="span4" id="player-courseinfo-user">
		<?php if ($courseUser) : ?>
			<?php echo Yii::app()->user->getAvatar('pull-left avatar'); ?>
			<h4><?=Yii::app()->user->getUsername()?></h4>
			<p><?=Yii::t('report', '_DATE_INSCR')?>:<?=$courseUserSession->date_subscribed?></p>
			<p><?=Yii::t('standard', '_DATE_LAST_ACCESS')?>: <?=$courseUser->date_last_access ? $courseUser->date_last_access : "-"?></p>
		<?php endif; ?>
	</div>
	<div class="span4">
		<div class="row-fluid">
			<div class="pull-left" id="player-certificate-info">
				<i class="p-sprite certificate" id="player-certificate-medal"></i>
			</div>
			<h4><?php echo Yii::t('player', 'Certificate');?></h4>
			<?php if ($userCertificate) : ?>
				<a id="dl-cert-<?= $blockModel->id ?>" class="dl-certificate" href="<?=$this->createUrl('/player/block/downloadCertificate', array('course_id' => $this->course_id))?>" target="_blank">
					<?=Yii::t('certificate', '_TAKE_A_COPY')?> <i class="p-sprite download-transparent-black"></i>
				</a>
			<?php else : ?>
				<span><?= Yii::t('player', 'The selected certificate will be available to the user after the course completion') ?></span>
			<?php endif; ?>
		</div>
	</div>

	<div class="span3" id="player-timinings">
		<h4><?php echo Yii::t('course', 'Your Activities'); ?></h4>
		<p>
			<i class="p-sprite little-clock"></i> <?=$totalSpentHours?> <?=Yii::t('standard', '_HOURS')?> <?=$totalSpentMinutes?> <?=Yii::t('standard', '_MINUTES')?>
		</p>
	</div>

	<div class="span1">
		<div id="player-course-percentgraph">
			<input id="graph-<?= $blockModel->id ?>" class="player-course-graph" data-bgcolor="#EEEEEE" data-fgcolor="#5FBF5F" value="<?=$completedCount?>" style="display: none;" />
			<div id="player-course-percentage">
				<?=$completedPercentage?><span>%</span>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(function(){

	$('#graph-<?= $blockModel->id ?>').knob({
		'min':0,
		'max':<?=$totalHours?>,
		'readOnly': true,
// 		'fgColor': '#53AB53',
// 		'bgColor': '#666777',
		'width': 66,
		'height': 66,
		'thickness':.25,
		'value': 0
	});


});
</script>