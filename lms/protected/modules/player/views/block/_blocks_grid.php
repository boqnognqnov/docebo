<div id="blocks-sortable">
	<div class="row-fluid">
		<ul class="thumbnails fluid comfort">
			<?php
			foreach ($blocks as $blockModel) {
				$this->widget('player.components.widgets.PlayerBlock', array(
					'blockModel' => $blockModel,
					'idSession' => (isset($idSession)? $idSession : false)
				));
			}
			?>
		</ul>
	</div>
</div>

