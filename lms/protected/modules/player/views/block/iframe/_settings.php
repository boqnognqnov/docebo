<?php /* @var $model PlayerBlockIframe */ ?>

<h1><?= Yii::t('player', 'Edit Iframe Widget Settings') ?></h1>

<?php $form = $this->beginWidget('CActiveForm', array(
	'action' => $this->createUrl('//player/block/AxIframeSettings', array('course_id' => $model->course_id ,'block_id' => $model->id)),
	'method' => 'post',
	'htmlOptions' => array(
		'id'      => 'iframe-widget-' . $model->id,
		'class'   => 'ajax form-horizontal',
	),
)); ?>

<?php
$params = json_decode($model->params, true);

if(!empty($newParams['iframeUrl'])) {
	$iframeUrlVal = $newParams['iframeUrl'];
} elseif($params['iframeUrl']) {
	$iframeUrlVal = $params['iframeUrl'];
} else {
	$iframeUrlVal = "";
}

if($newParams['iframeHeight'] > 0){
	$iframeHeightVal = $newParams['iframeHeight'];
} elseif($params['iframeHeight'] > 0) {
	$iframeHeightVal = $params['iframeHeight'];
} else {
	$iframeHeightVal = 0;
}

if(!empty($newParams['oauthClient'])){
	$oauthClientVal = $newParams['oauthClient'];
} elseif($params['oauthClient']) {
	$oauthClientVal = $params['oauthClient'];
} else {
	$oauthClientVal = "";
}

if(!empty($newParams['secretSalt'])) {
	$secretSaltVal = $newParams['secretSalt'];
} elseif($params['secretSalt']) {
	$secretSaltVal = $params['secretSalt'];
} else {
	$secretSaltVal = "";
}

if(!empty($newParams['repeatSecretSalt'])) {
	$repeatSecretSaltVal = $newParams['repeatSecretSalt'];
} elseif($params['secretSalt']) {
	$repeatSecretSaltVal = $params['secretSalt'];
} else {
	$repeatSecretSaltVal = "";
}

?>
<div id="iframe-widget-container">
	
	<div class="control-group">
		<?php echo CHtml::label(Yii::t('player', 'IFRAME URL'), 'iframe-url', array(
			'class' => 'control-label'
		)); ?>
		<div class="controls">
			<?php
                echo $form->textField($model, 'iframeUrl', array(
					'class' => 'input-block-level',
					'id' => 'iframe-url',
					'value' => $iframeUrlVal
				));
			?>
			<?php echo $form->error($model, 'iframeUrl'); ?>
		</div>

	</div>

	<div class="control-group">
		<?php echo CHtml::label(Yii::t('player', 'Height (px)'), 'iframe-height', array(
			'class' => 'control-label'
		)); ?>
		<div class="controls">
			<?php
				echo $form->textField($model, 'iframeHeight', array(
					'class' => 'input-block-level',
					'id' => 'iframe-height',
					'value' => $iframeHeightVal
				));
			?>
			<?php echo $form->error($model, 'iframeHeight'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo CHtml::label(Yii::t('player', 'OAuth Client'), 'oauth-client', array(
			'class' => 'control-label'
		)); ?>
		<div class="controls">
			<?php
				echo $form->dropDownList($model, 'oauthClient', $clientsList, array(
					'options' => array($oauthClientVal => array('selected' => true))
				));
			?>
			<?php echo $form->error($model, 'oauthClient'); ?>
		</div>
	</div>
	
	<div class="control-group">
		<?php echo CHtml::label(Yii::t('player', 'Secret salt'), 'secret-salt', array(
			'class' => 'control-label'
		)); ?>
		<div class="controls">
			<?php
                echo $form->passwordField($model, 'secretSalt', array(
					'class' => 'input-block-level',
					'id' => 'secret-salt',
					'value' => $secretSaltVal
				));
			?>
			<?php echo $form->error($model, 'secretSalt'); ?>
		</div>

	</div>

	<div class="control-group">
		<?php echo CHtml::label(Yii::t('player', 'Repeat Secret salt'), 'repeat-secret-salt', array(
			'class' => 'control-label'
		)); ?>
		<div class="controls">
			<?php
				echo $form->passwordField($model, 'repeatSecretSalt', array(
					'class' => 'input-block-level',
					'id' => 'repeat-secret-salt',
					'value' => $repeatSecretSaltVal
				));
			?>
			<?php echo $form->error($model, 'repeatSecretSalt'); ?>
		</div>

	</div>

</div>

<!-- Dialog2 will move these in Dialog footer  -->
<div class="form-actions">
	<input class="btn-docebo green big upload-save" type="submit" name="confirm_save" value="<?php echo Yii::t('standard', '_SAVE'); ?>">
	<input class="btn-docebo black big close-dialog upload-cancel" type="button" value="<?= Yii::t('standard', '_CANCEL'); ?>">
</div>


<?php $this->endWidget(); ?>



<script type="text/javascript">
	(function(){

		var blockId = <?= json_encode($model->id) ?>;
		var formId = 'iframe-widget-' + blockId;

		console.log('Settings JS loaded');
		console.log('FormID = ' + formId);
		
		$(function(){
			console.log('Settings DOCUMENT READY');
		});
		
	})(jQuery);

</script>