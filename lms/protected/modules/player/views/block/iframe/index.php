<?php /* @var $blockModel PlayerBlockIframe */ ?>

<?php
    // All the settings should be in params
    $params = json_decode($blockModel->params, true);
    $url = $blockModel->getIframeUrl($params['iframeUrl'], $blockModel->course_id, $params['oauthClient'], $params['secretSalt']);
    $id = $blockModel->id;
    $iframeId = "iframe-widget-iframe-" . $id;

if(!empty($url)) {

?>

<iframe id="<?= $iframeId ?>" width="100%" src="<?= $url ?>"></iframe>

<?php } ?>

<script type="text/javascript">
	(function(){

		var blockId = <?= json_encode($blockModel->id) ?>;
		var blockRefreshId = '#blockid_<?= $blockModel->id ?>';
		var blockHeight = <?= json_encode($params['iframeHeight']) ?>;

		$('#<?= $iframeId ?>').css({height: (blockHeight > 0 ? blockHeight : 600)  + 'px'});
		
		console.log('CONTENT JS loaded');
		console.log('blockId = ' + blockId);
		
		$(function(){
			console.log('Content DOCUMENT READY');
		});

		// On Document Ready...
		$(function(){

			// Listen for any postMessage
			$(window).off("onmessage message").on("onmessage message", function(e){
				var iframeWindow = document.getElementById('<?= $iframeId ?>').contentWindow;
				var $iframe = $('#<?= $iframeId ?>');
				var event = e.originalEvent;
				// Ensure sending IFRAME is the one know very well
				if((iframeWindow === event.source) && event.data) {
					if (typeof event.data.height !== "undefined") {
						$iframe.height(blockHeight > 0 ? blockHeight : event.data.height);
					}
				}
			});
		});

		// Refresh block after save settings
		$(document).on('dialog2.content-update', '[id^="widget-iframe-settings-'+blockId+'"]', function(){
			if ($(this).find("a.auto-close.success").length > 0) {
				$(blockRefreshId).loadOneBlockContent();
			}
		});
		
	})(jQuery);

</script>