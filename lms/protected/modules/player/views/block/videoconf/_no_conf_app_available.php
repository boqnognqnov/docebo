<?php if (Yii::app()->user->isGodAdmin) : ?>
    <div class="row-fluid">
        <div class="span3">
            <a class="p-sprite apps-tile" href="<?= Yii::app()->createUrl('app/index') ?>#web_conferencing"></a>
        </div>
        <div class="span9">
        	<p>
            	<?= Yii::t('conference', 'To add conferencing functionality you must activate and enable one of the Conferencing Apps available at our App marketplace.'); ?>
            </p>
            <h4>
           		<a class="nounderline" href="<?= Yii::app()->createUrl('app/index') ?>#web_conferencing">
	            	<?= Yii::t('templatemanager', 'Get this App', array('{color_start}'=>'<span class="text-colored-orange">', '{color_end}'=>'</span>')) ?>
            	</a>
            </h4>
        </div>
    </div>

<?php else : ?>
    <div class="row-fluid">
        <div class="span12">
            <?= Yii::t('conference', 'Conferencing functionality is not enabled. Please ask LMS administrator to activate an App.'); ?>
        </div>
    </div>
<?php endif; ?>
