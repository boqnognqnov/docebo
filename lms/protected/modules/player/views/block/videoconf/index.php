<?php
/* @var $dataProviderOnair CActiveDataProvider */
/* @var $dataProviderScheduled CActiveDataProvider */
/* @var $dataProviderHistory CActiveDataProvider */
/* @var $hideOnAirTab bool */
?>
<div class="player-videoconf-block">

    <div class="tabbable"> <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
			<?php if ( ! $hideOnAirTab ) : ?>
            <li class="active"><a href="#player-videoconf-section-onair" data-toggle="tab"><span class="i-sprite is-onair large"></span></a></li>
			<?php endif; ?>
            <li class="<?= ($hideOnAirTab ? 'active' : '') ?>"><a href="#player-videoconf-section-scheduled" data-toggle="tab"><span class="i-sprite is-calendar-date large"></span></a></li>
            <li><a href="#player-videoconf-section-history" data-toggle="tab"><span class="i-sprite is-clock large"></span></a></li>
        </ul>
        <div class="tab-content">
			<?php if ( ! $hideOnAirTab ) : ?>
            <div class="tab-pane active" id="player-videoconf-section-onair">
				<div class="title important"><?= Yii::t('conference', 'On air now') ?></div>

				<?php if ($dataProviderOnair->getTotalItemCount() > 0) : ?>
					<?php
					$this->widget('zii.widgets.CListView', array(
						'dataProvider'=>$dataProviderOnair,
						'template'=>"{sorter}\n{items}",
						'itemsTagName'=>'ul',
						'itemView'=>'player.views.block.videoconf._one_onair',
						'viewData' => array('allowAdminOperations' => $allowAdminOperations, 'courseUser' => $courseUser),
						'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
						'id' => 'videoconf-onair-list'
					));
					?>
				<?php endif; ?>
			</div>
			<?php endif; ?>

			<div class="tab-pane <?= ($hideOnAirTab ? 'active' : '') ?>" id="player-videoconf-section-scheduled">
				<div class="title"><?= Yii::t('conference', 'Scheduled') ?></div>
				<?php if ($dataProviderScheduled->getTotalItemCount() > 0) : ?>
					<div class="player-videoconf-section">
						<?php
						$this->widget('zii.widgets.CListView', array(
							'dataProvider'=>$dataProviderScheduled,
							'template'=>"{sorter}\n{items}",
							'itemsTagName'=>'ul',
							'itemView'=>'player.views.block.videoconf._one_scheduled',
							'viewData' => array('allowAdminOperations' => $allowAdminOperations, 'courseUser' => $courseUser),
							'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
							'id' => 'videoconf-scheduled-list'
						));
						?>
					</div>
				<?php endif; ?>
            </div>

            <div class="tab-pane" id="player-videoconf-section-history">
				<div class="title"><?= Yii::t('standard', '_HISTORY') ?></div>

                <div class="player-videoconf-section player-videoconf-section-history">
                    <div class="datepicker-container">
					<?php
						$this->widget('bootstrap.widgets.TbDatepicker', array(
							'label' => 'Search by date',
							'htmlOptions' => array(
								'id' => 'player-videoconf-history-datepicker',
								'name' => 'videoconf_history_date_filter'
							)
						));
					?>
                        <script type="text/javascript">
							$('#player-videoconf-history-datepicker').change(function(){
								var dateFilter = $(this).serialize();
                                $.fn.yiiListView.update(
									'videoconf-history-list',
									{
										data: dateFilter,
										url: '<?= $this->createUrl('axLoadOneBlock', array(
											'course_id'=>$courseModel->idCourse,
											'block_id'=>$blockModel->id,
											'htmlOnly'=>'videoconf-history-list'
										)) ?>'
									}
                                );
							});
                        </script>
                    </div>

					<?php
					$this->widget('zii.widgets.CListView', array(
						'dataProvider'=>$dataProviderHistory,
						'template'=>"{sorter}\n{items}\n{pager}",
						'pager'=>array(
							'class' => 'DoceboCLinkPager',
							'extraUrlParams' => array(
								'videoconf_history_date_filter' => Yii::app()->request->getParam('videoconf_history_date_filter'),
								'block_id' => $blockModel->id,
								'for' => 'history'
							),
							'htmlOptions' => array('id' => 'videoconference-history-pager'),
							'id' => 'videoconference-history-pager'
						),
						'ajaxUpdate' => 'videoconf-history-list-all-items',
						'itemsTagName'=>'ul',
						'itemView'=>'player.views.block.videoconf._one_history',
						'viewData' => array('allowAdminOperations' => $allowAdminOperations),
						'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
						'id' => 'videoconf-history-list',
						'ajaxVar' => 'htmlOnly',
					));
					?>
                </div>

            </div>
        </div>
    </div>

</div>




<!-- --- JS ------------------------------------------------------------------------------------------------------ -->

<script type="text/javascript">

/*<![CDATA[*/

var BLOCKID = '#blockid_<?= $blockModel->id ?>';

// Catch the auto-close and refresh the block
$(document).on("dialog2.content-update", '[id^="dialog-handle-room"]', function() {
	var e = $(this);

	if ($(this).find("a.auto-close").length > 0) {
		// Reload MYSELF (the block); that will update everything inside
		$(BLOCKID).loadOneBlockContent();
	}

	var autoCloseAndRedir = e.find("a.auto-close-and-redirect");
	if (autoCloseAndRedir.length > 0) {
		e.dialog2("close");
		var href = autoCloseAndRedir.attr('href');
		if (href) {
			window.location.replace(href);
		}
		return;
	}


});

function dialogError(message) {
}

/*]]>*/
</script>



