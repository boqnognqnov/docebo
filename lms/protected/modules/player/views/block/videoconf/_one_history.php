<?php
	$moderator = $allowAdminOperations ? "'true'" : "'false'"; 
	$deleteRoomUrl = Yii::app()->createUrl("videoconference/axDeleteRoom", array("room_id" => $data["id_session"], "op" => "delete"));
	$editRommUrl = Yii::app()->createUrl("videoconference/axHandleRoom", array("room_id" => $data["id_session"],"op"=>"update"));
?>
<li class="player-videoconf-row">

	<div class="row-fluid">
		<div class="span10">
			<?php echo $data["name"]; ?>
			<br>
			<span class="i-sprite is-clock"></span> <strong><?= Yii::app()->localtime->toLocalDateTime($data['date_begin']);?></strong>
		</div>
		<div class="span2 text-right">
			<?php if ($allowAdminOperations) : ?>
				<div class="dropdown-div">
					<div class="dropdown-toggle lo-menu p-sprite menu-rounded-black" role="button" data-toggle="dropdown" data-target="#"  data-container="body"></div>
					<ul class="dropdown-menu  pull-right" role="menu">
						<li>
							<a  href="<?= $editRommUrl ?>" class="open-dialog" rel="dialog-handle-room-<?= $data["id_session"] ?>" data-dialog-class="videoconf-room-dialog" role="menuitem"><span class="i-sprite is-edit"></span> <?= Yii::t('conference', 'Edit room') ?></a>
						</li>
						<li>
							<a href="<?= $deleteRoomUrl ?>" class="open-dialog" rel="dialog-handle-room-<?= $data["id_session"] ?>" data-dialog-class="edit-block-modal" role="menuitem"><span class="i-sprite is-remove red"></span> <?= Yii::t('conference', 'Delete Web Conferencing session') ?></a>
						</li>
					</ul>
				</div>
			<?php endif; ?>
		</div>
	</div>


</li>
