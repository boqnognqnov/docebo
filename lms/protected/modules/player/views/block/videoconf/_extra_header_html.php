<?php
	$url = Yii::app()->controller->createUrl('//videoconference/axHandleRoom', array('course_id' => $blockModel->course_id, 'op' => 'create'));
?>
<a  
	href="<?= $url ?>"
	class="open-dialog block-videoconf-add-room"
	data-dialog-class="videoconf-room-dialog"
	rel="dialog-handle-room"
	data-block-id="<?= $blockModel->id ?>"
	title="<?= Yii::t('conference', 'Add room') ?>">
	<span class="p-sprite add-block-item"></span>
</a>