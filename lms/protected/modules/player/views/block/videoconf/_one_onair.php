<?php
/*
 * @var $data WebinarSession
 *
 */
	$deleteRoomUrl = Yii::app()->createUrl("videoconference/axDeleteRoom", array("room_id" => $data["id_session"], "op" => "delete"));
	$editRoomUrl = Yii::app()->createUrl("videoconference/axHandleRoom", array("room_id" => $data["id_session"],"op"=>"update"));
	$joinMeetingUrl = Yii::app()->createUrl("videoconference/joinMeeting", array("room_id" => $data["id_session"], 'day' => $data['day']));

	$joinButtonSeconds = WebinarSessionDate::getJoinInAdvanceButtonTimeoutMinutes(Yii::app()->user->getIdst(), $data['id_session'], $data['day']);
	$user = CoreUser::model()->findByPk(Yii::app()->user->getIdst());

	$userValidation = WebinarSession::model()->validateWebinarUser($user, $data['tool']);
	$hideJoinButton = !is_array($userValidation) || !$userValidation['valid'];

	$timestampNow = time();
?>
<li class="player-videoconf-row">

	<div class="row-fluid">
		<div class="span1">
			<span class="i-sprite is-couple"></span>
		</div>

		<!--<div class="span1">
			<strong><?/*= '';//$data['count_participants'] */?></strong>
		</div>-->

		<?php if ($hideJoinButton): ?>
			<div class="span9">
				<?php echo $data['name']; ?>
				<div class="warning-message-yellow">
					<i class="fa fa-2x fa-exclamation-circle">&nbsp;</i>
						<span>
							<?= $userValidation['message']; ?>
						</span>
				</div>
			</div>
		<?php else: ?>
				<div class="span9">
					<?php echo $data['name']; ?>
				</div>
			<div class="span1">
				<div class="webinar-timer-box-<?=$data["id_session"] . '_' . $timestampNow ?>">
					<a class="btn-docebo green big pull-right" target="_blank" href="<?= $joinMeetingUrl ?>"><?= Yii::t('conference', 'Join') ?></a>
				</div>
			</div>
		<?php endif; ?>

		<div class="span1 text-right">
			<?php if ($allowAdminOperations) : ?>
				<div class="dropdown-div">
					<div class="dropdown-toggle lo-menu p-sprite menu-rounded-black" role="button" data-toggle="dropdown" data-target="#"  data-container="body"></div>
					<ul class="dropdown-menu pull-right" role="menu">
						<li>
							<a  href="<?= $editRoomUrl ?>" class="open-dialog" rel="dialog-handle-room-<?= $data["id_session"] ?>" data-dialog-class="videoconf-room-dialog" role="menuitem"><span class="i-sprite is-edit"></span> <?= Yii::t('conference', 'Edit room') ?></a>
						</li>
						<li>
							<a href="<?= $deleteRoomUrl ?>" class="open-dialog" rel="dialog-handle-room-<?= $data["id_session"] ?>" data-dialog-class="edit-block-modal" role="menuitem"><span class="i-sprite is-remove red"></span> <?= Yii::t('conference', 'Delete Web Conferencing session') ?></a>
						</li>
					</ul>
				</div>
			<?php endif; ?>
		</div>
	</div>

	<?php if(!$hideJoinButton): ?>

	<script type="application/javascript">
		$(document).ready(function () {
			// set the date we're counting down to
			<?php
                $startTimestamp = strtotime($data['date_begin']);
                $endTimestamp = strtotime($data['date_end']);
			?>

			var options<?=$data["id_session"] . '_' . $timestampNow ?> = {
				target_date: <?= $endTimestamp ?>,
				joinButtonOffsetSeconds: <?= ($joinButtonSeconds*60) ?>,
				startTimeStamp: <?= $startTimestamp ?>,
				endTimeStart: <?= $endTimestamp ?>,
				now: '<?= strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s')) ?>',
				buttonElement: 'webinar-timer-box-<?=$data["id_session"] . '_' . $timestampNow ?>',
				endButtonElement: 'webinar-timer-box-end-<?=$data["id_session"] . '_' . $timestampNow ?>',
				task: 'show'
			};
			var counter<?=$data["id_session"] . '_' . $timestampNow ?> = new JoinButtonTimer(options<?=$data["id_session"] . '_' . $timestampNow ?>);
		});
	</script>
	<?php endif; ?>
</li>
