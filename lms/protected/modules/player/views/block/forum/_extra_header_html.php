<?php
	$url = Yii::app()->controller->createUrl('//forum/default/axEditForumPopup', array()); 
?>
<a  
	href="<?= $url ?>"
	class="open-dialog"
	rel="create-forum-modal-<?= $blockModel->id ?>"
	removeOnClose="true" 
	data-block-id="<?= $blockModel->id ?>">
	<span class="p-sprite add-block-item"></span>
</a>