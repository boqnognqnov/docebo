<?php
/* @var $forums LearningForum[] */
?>
<div class="player-forum-block">
	<table class="table table-hover">
		<thead>
			<tr>
				<th><?php echo Yii::t('standard', '_FORUM'); ?></th>
				<th class="text-center close-column"><?php echo Yii::t('forum', '_NUMTHREAD'); ?></th>
				<th class="text-center close-column"><?php echo Yii::t('forum', '_NUMPOST'); ?></th>
				<th class="text-right"></th>
			</tr>
		</thead>
		<tbody>
		<?php if (!empty($forums)): ?>
			<?php foreach($forums as $forum) :
				$forumUrl = $this->createUrl('//forum/thread', array('forum_id' => $forum->idForum));
				$editUrl 	= Yii::app()->controller->createUrl('//forum/default/axEditForumPopup', array('forum_id' => $forum->idForum));
				$removeUrl 	= Yii::app()->controller->createUrl("//forum/default/axRemoveForum", array("forum_id" => $forum->idForum));
				?>
				<tr class="forum-line">
					<td>
						<a href="<?= $forumUrl ?>"><span class="p-sprite chat-icon"></span>&nbsp;&nbsp;<span class="forum-name"><?= $forum->title ?></span></a>
					</td>
					<td class="text-center close-column hide-on-hover"><?= $forum->numThreads ?></td>
					<td class="text-center close-column hide-on-hover"><?= $forum->getNumOfReplies() ?></td>

					<td class="text-right close-column">
						<?php if ($this->userCanAdminCourse || LearningCourseuser::userLevel(Yii::app()->user->id, $this->course_id)==LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR) : ?>
						<div class="action">
							<a href="<?= $editUrl ?>"
								class="open-dialog"
								rel="edit-forum-modal-<?= $blockModel->id ?>"
								removeOnClose="true"
								data-block-id="<?= $blockModel->id ?>"><span class="i-sprite is-edit"></span></a>
							<a 	href="<?= $removeUrl ?>"
								class="remove-forum-operation"
								data-forum-id="<?= $forum->idForum ?>"
								data-block-id="<?= $blockModel->id ?>"><span class="i-sprite is-remove red"></span></a>
						</div>
						<?php endif;?>
					</td>
				</tr>
			<?php endforeach; ?>
		<?php endif; ?>
		</tbody>
	</table>
</div>
