<?php
/**
 * Created by PhpStorm.
 * User: NikVas
 * Date: 24-Jul-15
 * Time: 18:02
 */
 /* @var $form CActiveForm */
		$form = $this->beginWidget('CActiveForm', array(
            'id' => 'session_form',
            'htmlOptions' => array('class' => 'ajax'),
        ));

$coachingSession = ($coaching_session_id)? LearningCourseCoachingSession::model()->findByPk($coaching_session_id) : LearningCourseCoachingSession::model()->findByPk($model->id_coaching_session);
echo $form->hiddenField($model, 'id_coaching_session',array('value'=>$coaching_session_id));
?>
    <div class="tab-pane active session-form-details-tab-content" id="sessionForm-details">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="clearfix">
                            <?php
                            echo $form->labelEx($model, 'name');
                            echo $form->textField($model, 'name', array('class' => 'session-name'));
                            echo $form->error($model, 'name');
                            ?>
                        </div>
                    </div>
                </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="clearfix">
                    <?php echo $form->labelEx($model, 'tool');
                    if(!$model->id_session)
                        echo $form->dropDownList($model, 'tool', WebinarSession::toolList(), array('class' => 'webinar-dropdown-full', 'id' => 'toolDropdown', 'prompt' => Yii::t('webinar', 'Select webinar tool')));
                    else {
                        echo $form->dropDownList($model, 'tool', WebinarSession::toolList(), array('disabled' => 'disabled', 'class' => 'webinar-dropdown-full', 'id' => 'toolDropdown', 'prompt' => Yii::t('webinar', 'Select webinar tool')));
                        echo $form->hiddenField($model, 'tool');
                    }
                    echo $form->error($model, 'tool');
                    ?>
                </div>
            </div>

        </div>
        <div class="row-fluid" id="tool_selection" style="display: <?=($model->tool && $model->tool != WebinarSession::TOOL_CUSTOM) ? 'block' : 'none'?>;">
            <div class="span12">
                <div class="clearfix tool-account-box">
                    <?php
                    $this->renderPartial('lms.protected.modules.webinar.views.session._tool_accounts', array('model' => $model));
                    ?>
                </div>
            </div>
        </div>
        <div class="row-fluid" id="tool_custom" style="display: <?=($model->tool && $model->tool == WebinarSession::TOOL_CUSTOM) ? 'block' : 'none'?>;">
            <div class="span12">
                <div class="clearfix">
                    <?php
                    echo $form->labelEx($model, 'custom_url');
                    echo $form->textField($model, 'custom_url', array('class' => 'session-name'));
                    echo $form->error($model, 'custom_url');
                    ?>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span5">
                <?php echo $form->labelEx($model, 'date') ?>
                <?php echo $form->textField($model, 'date', array('class' => 'datepicker edit-day-input webinar-date-input', 'id' => 'day-calendar', 'readonly' => 'readonly')); ?><i class="p-sprite calendar-black-small date-icon"></i>
                <?php echo $form->error($model, 'date'); ?>
            </div>
            <div class="span4">
                <?php echo $form->labelEx($model, 'start') ?>
                <?php echo $form->dropDownList($model, 'start', WebinarSession::arrayForHours(), array('class' => 'webinar-start-input', 'prompt' => '')); ?>
                <?php echo $form->error($model, 'start'); ?>
            </div>
            <div id="coaching-webinar-duration">
                <?php echo $form->labelEx($model, 'duration') ?>
                <?php echo $form->dropDownList($model, 'duration', WebinarSession::arrayForDuration(), array('class' => 'webinar-duration-input', 'prompt' => '')); ?> hh:mm
                <?php echo $form->error($model, 'duration'); ?>
            </div>
        </div>
        <div class="row-fluid"><?=Yii::t('coaching','Invite');?></div>
        <div class="row-fluid">
            <div class="invitation-radio-btn">
                <?php echo CHtml::radioButton("chooseInvite", true, array(
                    'id'    =>  'invite-all-assigned-users',
                    'value' => 1,
                )); ?>
                <?php echo CHtml::label(Yii::t('coaching','All assigned users ({count})',array('{count}'=>$coachingSession->countAssignedUsers())), 'invite-all-assigned-users',array(
                    'class'=>'inline-label'
                )); ?>
            </div>
            <div class="invitation-radio-btn">
                <?php echo CHtml::radioButton("chooseInvite", false, array(
                    'id'    =>  'select-users-to-invite',
                    'value' => 2
                )); ?>
                <?php echo CHtml::label(Yii::t('coaching','Select users to invite'), 'select-users-to-invite',array(
                    'class'=>'inline-label'
                )); ?>
            </div>
        </div>
        <div id="users-multiselect-container" class="row-fluid" style="display: none;">
            <div style="display: inline-block;">
                <?php
//                echo CHtml::dropDownList('choose_users', '', array(), array(
//                    'id' => 'choose_users',
//                    'empty' => '---',
//                    'multiple' => 'multiple',
//                    'class' => 'fbkcomplete'
//                ));
                ?>
                <select id="choose_users" name="choose_users" multipl="multiple" class="fbkcomplete" empty="---">
                    <?php
                        // show previouslu assigned to the live coaching session users
                        if (count($preselectedWebinarUsers) > 0) {
                            foreach ($preselectedWebinarUsers as $idUser => $userNames) {
                                echo '<option value="'.$idUser.'" class="selected" selected="selected">'.$userNames.'</option>';
                            }
                        }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <div class="form-actions">
        <?php echo CHtml::submitButton(Yii::t('standard','_CONFIRM'), array('name'  => 'confirm','class' => 'btn btn-submit webinar-form-submit')); ?>
        <?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
    </div>
<?php $this->endWidget(); ?>
<script type="text/javascript">
    (function ($) {
        $(function () {
            $('#choose_users').fcbkcomplete(
                {
                    json_url: "<?=Docebo::createAbsoluteLmsUrl("//player/block/axCoachingSessionUsersAutocomplete&course_id=".$idCourse."&idCoachingSession=".$coachingSession->idSession) ?>",
                    complete_text: '<?=Yii::t('coaching', 'Type username/name here...')?>',
                    cache: true,
                    filter_selected: true,
                    height:4,
                    width: 556,
                    maxitems: <?=$maxitems ?>
                }
            );

        });
    })(jQuery);
</script>
<script type="text/javascript">
    /* ===  Onload event handler  === */
    $(function () {
        $(document).on('change','.coaching-webinar input[name="chooseInvite"]',function(){
            if($(this).attr('id')=='select-users-to-invite')
                $('#users-multiselect-container').show();
            else
                $('#users-multiselect-container').hide();

        });
        $(document).controls();
//       
        $('.coaching-webinar input').styler();
        $('.coaching-webinar .datepicker').bdatepicker({
            format: '<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')); ?>',//'yyyy-mm-dd',
            language: yii.language,
            autoclose: true,
            startDate:"<?= Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($coachingSession->datetime_start));?>",
            endDate:"<?= Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($coachingSession->datetime_end));?>"
        });
        $(".coaching-webinar .date-icon").on('click', function (ev) {
            $(this).prev('input').bdatepicker().data('datepicker').show();
        });
        $('.edit-session-tabs > ul.nav.nav-tabs li').bind('click', function (e) {
            e.preventDefault();

            // hide all tabs
            $(this).closest('div').find('.tab-content .tab-pane').hide();
            $(this).parent().find('li').removeClass('active');
            $(e.currentTarget).addClass('active');

            // Show the right tab and hide the other tab
            switch ($(e.target).attr('data-target')) {
                case '#sessionForm-details':
                    // Show session details form and hide dates form
                    $('.coaching-webinar #sessionForm-details').show();

                    break;

                case '#sessionForm-evaluation':
                    $('.coaching-webinar #sessionForm-evaluation').show();

                    break;
            }
        }).eq(0).find('a').click();

        $(".coaching-webinar #toolDropdown").change(function(){
            if ($(this).val() == '<?=WebinarSession::TOOL_CUSTOM?>')
            {
                $(".coaching-webinar #tool_custom").show();
                $(".coaching-webinar #tool_selection").hide();
            }
            else
            {
                loadToolAccounts($(this).val());
                $(".coaching-webinar #tool_custom").hide();
                $(".coaching-webinar #tool_selection").show();
            }
        });
    });

    function loadToolAccounts(tool)
    {
        $.ajax({
            url: "<?=Docebo::createLmsUrl('webinar/session/getAccountsByTool')?>&tool="+tool,
            context: document.body
        }).done(function(data) {
            $('.coaching-webinar .tool-account-box').html(data);
        });
    }
</script>
<style>

    .btn:hover{
        transition: none;
        color: #ffffff;
    }
    a.btn{
        color: #ffffff;
        font-size: 14px;
        text-transform: uppercase;
        border: none;
        border-radius: 0;
        padding: 7px 10px;
        min-width: 98px;
        width: auto;
        text-align: center;
        box-shadow: none;
        font-weight: 600;
        line-height: normal;
    }
    a.btn,
    a.btn.disabled {
        min-width: 80px;
    }
    .modal-footer a.btn {
        outline: none;
    }
    .btn-submit{
        background: #53ab53;
        text-shadow: 1px 1px #306430;
    }
    .btn-submit.disabled,.btn-cancel{
        color: #ffffff;
        font-size: 14px;
        text-transform: uppercase;
        border: none;
        border-radius: 0;
        padding: 7px 10px;
        min-width: 98px;
        width: auto;
        text-align: center;
        box-shadow: none;
        font-weight: 600;
        line-height: normal;
    }
    .modal-footer .btn-submit.disabled {
        opacity: 0.5;
        filter: progid:DXImageTransform.Microsoft.Alpha(opacity=50); /* IE 5.5+*/
        -moz-opacity: 0.5; /* Mozilla 1.6- */
        -khtml-opacity: 0.5; /* Konqueror 3.1, Safari 1.1 */
    }
    .btn-cancel,
    .btn-cancel:hover,
    .btn-cancel:active,
    .btn-cancel:focus{
        background: #333333;
        text-shadow: 1px 1px #000000;
    }
    .modal-body input[type="text"]{
        height: 31px;
        line-height: 31px;
        border-left: 1px solid #999999;
        border-top: 1px solid #999999;
        border-bottom: 1px solid #e5e5e5;
        border-right: 1px solid #e5e5e5;
        background: #f9f9f9;
        padding: 0 8px;
        color: #333333;
    }
    select {
        outline: none !important;
        border-radius: 0;
        margin: 0;
        border-radius: 0;
        padding: 4px;
        box-shadow: none;
    }
    .modal-body select, .modal-body textarea {
        padding: 0 8px;
        color: #333333;
        width: 280px;
    }
    .modal-body select {
        padding: 0;
        width: 298px;
    }
    select{
        /*     width: 205px; */
        height: 31px;
        line-height: 31px;
        border-left: 1px solid #999999;
        border-top: 1px solid #999999;
        border-bottom: 1px solid #e5e5e5;
        border-right: 1px solid #e5e5e5;
        background: #f9f9f9;
    }
    .p-sprite.calendar-black-small.date-icon{
        vertical-align:middle;
        margin-top:-10px;
    }
    #coaching-webinar-duration{
        display:inline-block;float:right;
    }
    .inline-label,.invitation-radio-btn{
        display:inline-block;
    }
    .invitation-radio-btn label{
        margin-left:15px;
        margin-right:15px;
    }
    .bit-box{
        padding: 6px 25px 6px 10px !important;
        background: #EFF2F7 !important;
        border-color: rgb(234, 234, 234) !important;
    }
    .modal-body.opened#dialog-handle-room{
        max-height:none;
        overflow: visible;
    }
</style>