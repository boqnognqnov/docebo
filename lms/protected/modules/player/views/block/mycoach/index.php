<?php

/* 
 * @author Georgi Stefkoff
 */
?>


<div class="mycoach-block-container">
	<div class="mycoach-block row-fluid">
	    <div class="span2">
	        <?php echo $coachModel->getAvatarImage(); ?>
	    </div>
	    <div class="span10">
	        <div class="row-fluid">
	            <p class="blue-text lead"><?=$coachModel->getFullName(); ?> <?php if($isUserCoach === false && $blockModel->email_contact != 0){ echo "<a href='mailto:{$coachModel->email}' title='{$coachModel->email}' target='_blank'><span class='mail'></span></a>"; } ?></p>
	        </div>
	        <?php
	            if(!$isSessionExpired):
	        ?>
	        <div class="row-fluid">
	            <div class="span1"><i class="fa fa-calendar"></i></div>
	            <div class="span11">
	                <p><?=Yii::t('coaching', 'My session');?>: <?=Yii::t('standard', '_FROM');?>: <?=$sessionStartDate; ?> <?=Yii::t('standard', '_TO');?> <?=$sessionEndDate; ?> <b class="green-text">(<?=$daysLeft?> <?=Yii::t('standard', 'Days left');?>)</b></p>
	            </div>
	        </div>
	            <?php
	                if($isUserCoach === true){
	                ?>
	        <div class="row-fluid">
	            <div class="span1"><span class="assigned-user"></span></div>
	            <div class="span11">
	                <?=Yii::t('standard', 'Assigned users');?>: <b><?=$assignedUsers; ?></b> &nbsp; <?=Yii::t('coaching', 'Max users');?>: <b><?= $coachSession->max_users ?></b>
	            </div>
	        </div>

	                <?php } else{ ?>
	    <hr>
	    <div class="row-fluid">
	            <div class="span5">
	                <?php
	                if(!empty($additionalFields)):
	                    foreach ($additionalFields as $fiedId => $field){
	                ?>
	                <span class="row-fluid">
	                    <span class="blue-text"><?= $field['translation']; ?>:</span>
	                    <?php if($field['type'] == 'yesno'){
	                    echo "<span>";
	                            switch ($field['user_entry']){
	                                case 1:
	                                    echo Yii::t('standard', '_YES');
	                                    break;
	                                case 2:
	                                    echo Yii::t('standard', '_NO');
	                                    break;
	                            }
	                    echo "</span>";
	                    } else {
	                        ?>
	                    <?php if(!empty($field['tr'])){ ?>
	                    <span><?= $field['tr']; ?></span>
	                    <?php } else { ?>
	                    <span><?= $field['user_entry']; ?></span>
	                    <?php } ?>
	                </span>
	                <?php
	                    }
	                    }
	                    endif;
	                ?>
	            </div>
	        <?php if($blockModel->time_compared != 0): ?>
	        <div class="span1">
	            <span class="clock"></span>
	        </div>
	            <div class="span6">
	                <div class="span1">

	                </div>
	                <div class="span11">
	                    <div class="row-fluid">
	                        <?=Yii::t('coaching', 'Your time');?>: <?= $learnerTime; ?>
	                    </div>
	                    <div class="row-fluid">
	                        <?=Yii::t('coaching', "Your coach's time");?>: <?= $coachTime; ?>
	                    </div>
	                </div>
	                <?php endif; ?>
	            </div>
	        </div>
	            <?php } ?>
	    <?php else: ?>
	        <div class="row-fluid">
	            <div class="span1"><i class="fa fa-calendar"></i></div>
	            <div class="span11">
	                <p class="small"><?=Yii::t('coaching', 'Your coaching session');?> (<?=Yii::t('standard', '_FROM');?> <?=$sessionStartDate; ?> <?=Yii::t('standard', '_TO');?> <?=$sessionEndDate; ?>) <b class="error"><?=Yii::t('coaching', 'has expired');?></b></p>
	            </div>
	        </div>
	    <?php if(!$isUserCoach): ?>
	    <div class="row-fluid">
	        <a class="btn btn-docebo blue big" href="<?=Docebo::createLmsUrl('player/training/userRequestedNewCoachingSession&course_id=' . $blockModel->course_id . '&user_id=' . (Yii::app()->user->id) . '&renew=1');?>"><?=Yii::t('coaching', 'Request another coach');?></a>
	    </div>
	    <?php endif; ?>
	    <?php endif; ?>
	    </div>

	</div>

	<div class="clearfix">&nbsp;</div>


	<?php if($blockModel->msg_system != 0 || $blockModel->webinar_system != 0): ?>
	<?php $currentUniqueId = uniqid(); ?>
    <div class="tabbable<?php if($blockModel->msg_system != 0 && !$isSessionExpired && !$isUserCoach) { echo " mycoach-block-container"; } ?>">
	    <ul class="nav nav-tabs" data-tabs="tabs">
            <?php if($blockModel->msg_system != 0):?>
		    <li class="active">
			    <a href="#tabChat<?=$currentUniqueId; ?>" class="nav-tabs-link" data-toggle="tab">
                        <i class="fa fa-envelope-o fa-2x"></i>
			    </a>
		    </li>
            <?php endif; ?>
            <?php if($blockModel->webinar_system != 0):?>
                <li class="<?=($blockModel->msg_system != 0)? '': 'active';?>">
                    <a href="#tabWeb<?=$currentUniqueId; ?>" class="nav-tabs-link tabWeb" data-toggle="tab">
                        <i class="webinar-icon-big"></i>
                    </a>
                </li>
                <li class="">
                    <a href="#tabHistory<?=$currentUniqueId; ?>" class="nav-tabs-link" data-toggle="tab">
                        <i class="fa fa-clock-o fa-2x"></i>
                    </a>
                </li>
            <?php endif; ?>
	    </ul>
	    <div class="tab-content">
            <?php if($blockModel->msg_system != 0):?>
		    <div class="tab-pane active tabChat" id="tabChat<?=$currentUniqueId; ?>" data-id="<?=$currentUniqueId; ?>">
			    <div class="search-box-container">
				    <?php if($isUserCoach): ?>
					    <div id="search-box<?=$currentUniqueId; ?>" class="pull-left search-input-wrapper span11" style="margin-left: 10px; width: 95%;">
					    <?= CHtml::textField('filter', '',
						    array(
							    'id' => 'filter_users' . $currentUniqueId,
							    'class' => 'filter_users',
							    'placeholder' => Yii::t('coaching', 'Search User')
						    )) ?>
					    <button id="clear-search<?=$currentUniqueId ?>" type="button" class="close clear-search">&times;</button>
					    <span id="search-icon<?=$currentUniqueId ?>" class="search-icon" style="display: block;"></span>
				    </div>

				    <div id="back-icon<?=$currentUniqueId; ?>" class="back-icon-container pull-left span1">
					    <div class="back-icon"><i class="fa fa-angle-left fa-2x"></i></div>
				    </div>

				    <?php endif; ?>
				    <div class="clearfix"></div>
			    </div>


			    <div class="learners-list container">
				    <!-- div class="menu-loading"></div -->
			    </div>
		    </div>
            <?php endif; ?>
            <?php if($blockModel->webinar_system != 0):?>
            <div class="tab-pane <?=($blockModel->msg_system != 0)? '': 'active';?>" id="tabWeb<?=$currentUniqueId; ?>" data-id="<?=$currentUniqueId; ?>">
                <div class="search-box-container" style="position: relative;">
                    <span style="position: absolute; bottom: 10px; color:#0465AC;"><strong><?= strtoupper(Yii::t('coaching','Meet the coach!'));?></strong></span>
                </div>
                    <div class="learners-list container active-webinars-list" style="padding:10px 0px 24px 10px;text-align:left;<?php  if($isSessionExpired){ echo 'border:0;';}?>">
                            <!-- div class="menu-loading"></div -->
                        <?php  if(!$isSessionExpired){
                            if ($isUserCoach) {
                                $webinarSessions = $coachSession->getFutureAndPresentWebinarSessions();
                            } else {
                                $webinarSessions = $coachSession->getFutureAndPresentWebinarSessions(Yii::app()->user->id);
                            }

                        if(count($webinarSessions)) {
                            if ( !filter_var(Yii::app()->user->email, FILTER_VALIDATE_EMAIL)) { // user with invalid email
                                $this->widget('common.widgets.warningStrip.WarningStrip', array(
                                    'message' => Yii::t('webinar', 'Your profile seems not to have a valid email setup'),
                                    'type' => 'warning',
                                ));
                            }
                            foreach ($webinarSessions as $webinarSession) {
                                ?>
                                <div class="active-webinar-session-row" style="border-bottom: 1px solid #E4E6E5;padding:10px;">
                                    <div class="webinarSessionInfo" style="display:inline-block;">
                                        <div><strong><?= $webinarSession->name; ?></strong></div>
                                        <div><i class="fa fa-clock-o fa-1x"></i> <span class="coach-webinar-time"><?php
	                                        $todayDate = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->getLocalNow());
											$webinarSessionStartDate = Yii::app()->localtime->stripTimeFromLocalDateTime($webinarSession->date_begin);
	                                        if ($todayDate != $webinarSessionStartDate) {
		                                        echo $webinarSession->date_begin;
	                                        } else {
		                                        echo '<strong> ' . Yii::t('calendar', '_TODAY') . '</strong> '.  Yii::app()->localtime->toLocalTime($webinarSession->date_begin, LocalTime::SHORT);
	                                        }

	                                        ?></span></div>
                                    </div>
                                    <div class="webinarSessionActions <?=(!(Yii::app()->localtime->isLte($webinarSession->date_begin, $nowTime) && Yii::app()->localtime->isGte($webinarSession->date_end, $nowTime)) && !$isUserCoach)? "has-user-status-btns" : "" ;?> <?= (Yii::app()->localtime->isLte($webinarSession->date_begin, $nowTime) && Yii::app()->localtime->isGte($webinarSession->date_end, $nowTime))?"withBtn":""?>" style="float:right;display:inline-block;">
                                        <?php

                                            $nowTime = Yii::app()->localtime->getLocalNow();
                                            // Display the button only if the webinar session is started and is not finished yet
                                            if (Yii::app()->localtime->isLte($webinarSession->date_begin, $nowTime) && Yii::app()->localtime->isGte($webinarSession->date_end, $nowTime)) {
		                                        if($isUserCoach) {
			                                        // if it is a coach
		                                            echo CHtml::button(Yii::t('standard', '_START'),
			                                                array(
				                                                'class' => 'confirm-save btn btn-docebo green big coaching-webinar-btn coaching-webinar-start-btn',
				                                                'data-url' => $webinarSession->getWebinarUrl(Yii::app()->user->id) //TODO FIXME:
			                                                )
		                                            );

			                                        $countWebinarSessionUsers = $webinarSession->countWebinarSessionUsers();
										?>
	                                        <div class="webinar-session-users-count-container">
		                                        <a class="open-dialog"
		                                           data-dialog-class="wide"
		                                           data-dialog-id="coaching-webinar-session-guests-dialog"
		                                           data-dialog-title= "<?= Yii::t('coaching', 'Guests') ?>"
		                                           href="<?=Docebo::createLmsUrl('player/block/axLoadCoachingWebinarSessionGuests&course_id=' . $blockModel->course_id . '&idSession='.$webinarSession->id_session);?>">
			                                        <div class="coach-icon-holder text-center">
				                                        <i class="fa fa-user fa-lg"></i>
				                                        <div class="icon-users-number <?=($countWebinarSessionUsers == 0) ? 'hide' : '';  ?>"><?=$countWebinarSessionUsers; ?></div>
			                                        </div>
		                                        </a>
	                                        </div>
                                            <div class="webinar-session-edit-container options-button">
                                                <a href="javascript:">
                                                    <div class="coach-icon-holder text-center">
                                                        <i class="fa fa-ellipsis-v fa-lg"></i>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="popover-options align-right">
                                                    <ul>
                                                        <li>
                                                            <a
                                                                href="/lms/index.php?r=player/block/axCreateCoachingWebinarSession&course_id=<?= $blockModel->course_id?>&webinar_session_id=<?=$webinarSession->id_session?>"
                                                                class="open-dialog block-videoconf-add-room"
                                                                data-dialog-class="modal-create-webinar-session coaching-webinar"
                                                                rel="dialog-handle-room"
                                                                data-block-id="<?= $blockModel->id ?>"
                                                                title="<?=Yii::t('coaching', 'Edit live session')?>">
                                                                <span class="i-sprite is-edit"></span> <?= Yii::t('standard','_MOD');?>
                                                            </a>
                                                        <li>
                                                            <a
                                                                href="/lms/index.php?r=player/block/axDeleteCoachingWebinarSession&course_id=<?= $blockModel->course_id?>&webinar_session_id=<?=$webinarSession->id_session?>"
                                                                class="open-dialog block-videoconf-add-room"
                                                                data-dialog-class="modal-delete-webinar-session coaching-webinar"
                                                                rel="dialog-handle-room"
                                                                data-block-id="<?= $blockModel->id ?>"
                                                                title="<?=Yii::t('coaching', 'Delete webinar session')?>">
                                                                <span class="i-sprite is-remove red"></span> <?= Yii::t('standard','_DEL');?>
                                                            </a>
                                                        </li>
                                                    </ul>
                                            </div>

                                        <?php
		                                        } else {
                                                    // It is a learner
                                                    if ( filter_var(Yii::app()->user->email, FILTER_VALIDATE_EMAIL) || (isset($webinarSession->tool) && !in_array($webinarSession->tool, array('gotowebinar', 'gototraining', 'gotomeeting')))) {
                                                        echo CHtml::button(Yii::t('conference', 'Join'),
                                                            array(
                                                                'class' => 'confirm-save btn btn-docebo green big coaching-webinar-btn coaching-webinar-start-btn',
                                                                'data-url' => $webinarSession->getWebinarUrl(Yii::app()->user->id) //TODO FIXME: remove after tests
                                                            )
                                                        );
                                                    } else {
                                                        echo CHtml::button(Yii::t('conference', 'Join'),
                                                            array(
                                                                'class' => 'disabled btn btn-docebo green big coaching-webinar-btn coaching-webinar-start-btn',
                                                                'data-url' => 'javascript:void(0);'
                                                            )
                                                        );
                                                    }
		                                        }
                                            } elseif ($isUserCoach) {
	                                            $countWebinarSessionUsers = $webinarSession->countWebinarSessionUsers();
										?>
	                                            <div class="webinar-session-users-count-container">
		                                            <a class="open-dialog"
		                                               data-dialog-class="wide"
		                                               data-dialog-id="coaching-webinar-session-guests-dialog"
		                                               data-dialog-title= "<?= Yii::t('coaching', 'Guests') ?>"
		                                               href="<?=Docebo::createLmsUrl('player/block/axLoadCoachingWebinarSessionGuests&course_id=' . $blockModel->course_id . '&idSession='.$webinarSession->id_session);?>">

			                                            <div class="coach-icon-holder text-center">
				                                            <i class="fa fa-user fa-lg"></i>
				                                            <div class="icon-users-number <?=($countWebinarSessionUsers == 0) ? 'hide' : '';  ?>"><?=$countWebinarSessionUsers;  ?></div>
			                                            </div>

		                                            </a>
	                                            </div>
                                                <div class="webinar-session-edit-container options-button">
                                                    <a href="javascript:">
                                                        <div class="coach-icon-holder text-center">
                                                            <i class="fa fa-ellipsis-v fa-lg"></i>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="popover-options align-right">
                                                    <ul>
                                                        <li>
                                                            <a
                                                                href="/lms/index.php?r=player/block/axCreateCoachingWebinarSession&course_id=<?= $blockModel->course_id?>&webinar_session_id=<?=$webinarSession->id_session?>"
                                                                class="open-dialog block-videoconf-add-room"
                                                                data-dialog-class="modal-create-webinar-session coaching-webinar"
                                                                rel="dialog-handle-room"
                                                                data-block-id="<?= $blockModel->id ?>"
                                                                title="<?=Yii::t('coaching', 'Edit live session')?>">
                                                                <span class="i-sprite is-edit"></span> <?= Yii::t('standard','_MOD');?>
                                                            </a>
                                                        <li>
                                                            <a
                                                                href="/lms/index.php?r=player/block/axDeleteCoachingWebinarSession&course_id=<?= $blockModel->course_id?>&webinar_session_id=<?=$webinarSession->id_session?>"
                                                                class="open-dialog block-videoconf-add-room"
                                                                data-dialog-class="modal-delete-webinar-session coaching-webinar"
                                                                rel="dialog-handle-room"
                                                                data-block-id="<?= $blockModel->id ?>"
                                                                title="<?=Yii::t('coaching', 'Delete webinar session')?>">
                                                                <span class="i-sprite is-remove red"></span> <?= Yii::t('standard','_DEL');?>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>

                                        <?php

                                            } elseif(!$isUserCoach) {
	                                            // If user is a leaner show choose status buttons

	                                            // Get the webinar session user data
	                                            $webinarSessionUser = LearningCourseCoachingWebinarSessionUser::model()->findByAttributes(array('id_user' => Yii::app()->user->id, 'id_session' => $webinarSession->id_session));
	                                    ?>

	                                            <div class="change-enrollment-status-btn-group btn-group">
		                                            <button id="going<?=$webinarSession->id_session ?>" data-userid="<?=Yii::app()->user->id ?>" data-sessionid="<?=$webinarSession->id_session ?>" data-value="<?=LearningCourseCoachingWebinarSessionUser::STATUS_ACCEPTED ;?>"
		                                                     class="btn-going select-status confirm-save btn btn-docebo green big
		                                                <?=(!empty($webinarSessionUser->id_session) && $webinarSessionUser->status == LearningCourseCoachingWebinarSessionUser::STATUS_ACCEPTED) ?  'coaching-webinar-green-btn' : 'coaching-webinar-gray-btn coaching-webinar-inactive-green-btn';?>">
			                                            <?= Yii::t('coaching', 'Going'); ?>
		                                            </button>
		                                            <button id="maybe<?=$webinarSession->id_session ?>" data-userid="<?=Yii::app()->user->id ?>" data-sessionid="<?=$webinarSession->id_session ?>" data-value="<?=LearningCourseCoachingWebinarSessionUser::STATUS_MAYBE ;?>"
		                                                    class="btn-maybe select-status confirm-save btn btn-docebo green big coaching-webinar-gray-btn
		                                                <?=(!empty($webinarSessionUser->id_session) && $webinarSessionUser->status == LearningCourseCoachingWebinarSessionUser::STATUS_MAYBE) ?  'coaching-webinar-orange-btn' : 'coaching-webinar-inactive-orange-btn';?>">
			                                            <?= Yii::t('coaching', 'Maybe'); ?>
		                                            </button>
		                                            <button id="decline<?=$webinarSession->id_session ?>" data-userid="<?=Yii::app()->user->id ?>" data-sessionid="<?=$webinarSession->id_session ?>" data-value="<?=LearningCourseCoachingWebinarSessionUser::STATUS_DECLINED ;?>"
		                                                    class="btn-decline select-status confirm-save btn btn-docebo green big coaching-webinar-gray-btn
		                                                <?=(!empty($webinarSessionUser->id_session) && $webinarSessionUser->status == LearningCourseCoachingWebinarSessionUser::STATUS_DECLINED) ?  'coaching-webinar-dark-gray-btn' : 'coaching-webinar-inactive-dark-gray-btn';?>">
			                                            <?= Yii::t('coaching', 'Decline'); ?>
		                                            </button>
	                                            </div>

	                                    <?php

                                            }
                                        ?>

                                    </div>
                                </div>
                            <?php }} else { ?>
                            <div class="row-fluid">
                                <div class="span1"><i class="fa fa-calendar"></i></div>
                                <div class="span11">
                                    <p class="small" style="text-align: left;font-size: 14px;"><?=Yii::t('coaching', 'This coaching session doesn\'t have webinar sessions yet.');?>  </p>
                                </div>
                            </div>
                        <?php }} ?>
                </div>
            </div>
            <div class="tab-pane" id="tabHistory<?=$currentUniqueId; ?>" data-id="<?=$currentUniqueId; ?>">
                <div class="search-box-container" style="position: relative;">
                    <span style="position: absolute; bottom: 10px; color:#0465AC;"><strong><?= strtoupper(Yii::t('standard','_HISTORY'));?></strong></span>
                </div>

	            <div class="search-box-container row-fluid" style="position: relative;  width: auto;">
		            <div>
			            <?php
			            $this->widget('common.widgets.DatePicker', array(
				            'id'          => 'webinar-date-start-container',
				            'label'       => CHtml::label(Yii::t('coaching', 'Search by date'),
				            'start_date',array('id'=>'webinar-start-date-label')),
				            'pickerEndDate'     => Yii::app()->localtime->toLocalDate(date('Y-m-d',strtotime("-1 days"))),
				            'htmlOptions' => array(
					            'id'                    => 'webinar-start-date',
					            'class'                 => 'datepicker coaching-session-date',
					            'data-date-format'      => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')),
					            'data-date-language'    => Yii::app()->getLanguage(),
					            'bootstrap-datepicker'  => '',
//					            'data-date-end-date'    => "0d"
				            ),
			            ));
			            ?>
		            </div>
	            </div>

                <div class="past-webinars-list container" id="past-webinars-list<?=$currentUniqueId; ?>">
                    <!-- div class="menu-loading"></div -->
                </div>
            </div>
			<?php endif; ?>
	    </div>
    </div>
	<?php endif; ?>

</div>
<script type="text/javascript">
	(function( $ ) {
		<?php
		// Display users assigned to this coaching session list here

		// If messaging is enabled
		if ($blockModel->msg_system != 0) {
		// Check if user is a coach
			if(/*!$isSessionExpired  &&*/ $isUserCoach) {
				// If session is not expired
//				if(!$isSessionExpired) {
		?>

		// Try init the myCoachChat jquery plugin as coach
		 if($('#tabChat<?=$currentUniqueId; ?>').length > 0) {
		    $('#tabChat<?=$currentUniqueId; ?>').myCoachChat({
			    'idCourse': <?=$blockModel->course_id ?>,
			    'idSession': <?=$coachSession->idSession ?>,
			    'typeChat': 'coach'
		    });
		 }

		<?php
					// end coach chat processing
//				}
			} else {
				// proceed with learner logic
		?>

		// Try init the myCoachChat jquery plugin as learner
		if($('#tabChat<?=$currentUniqueId; ?>').length > 0) {
			$('#tabChat<?=$currentUniqueId; ?>').myCoachChat({
				'idCourse': <?=$blockModel->course_id ?>,
				'idSession': <?=$coachSession->idSession ?>,
				'learner': <?=Yii::app()->user->id ?>,
				'typeChat': 'learner'
			});
		}

		<?php
			}
		}

		if(!$isSessionExpired) {
?>
		var refreshActiveLiveSessions; // refresh active live (webinar) sessions list interval

		// load past webinars
		function loadPastWebinars() {
			// Display loading gif
			if ($('#past-webinars-list<?=$currentUniqueId; ?>  .chat-loading').length == 0) {
				$("#tabHistory<?=$currentUniqueId; ?> .past-webinars-list").prepend('<div class="chat-loading"><i class="fa fa-spinner fa-pulse fa-lg"></i></div>');
			}

			// set default value for filter by date
			var filterByDate = '';
			if ($("#tabHistory<?=$currentUniqueId; ?> #webinar-start-date").length > 0) {
				filterByDate = $("#tabHistory<?=$currentUniqueId; ?> #webinar-start-date").val();
			}

			// Load past coaching webinar sessions
			$("#tabHistory<?=$currentUniqueId; ?> .past-webinars-list").load(
				HTTP_HOST + 'index.php?r=player/block/axLoadExpiredCoachingWebinarSessions&idCoachingSession=<?=$coachSession->idSession ?>&course_id=<?=$blockModel->course_id ?>&filterByDate='+filterByDate,
				function( response, status, xhr ) {
					if (status == "error") {
						var msg = "Sorry but there was an error: ";
						$("#error").html(msg + xhr.status + " " + xhr.statusText);
					}

					if (status == "success") {
						// Make sure it is displayed the search by date datepicker
					if ($("#tabHistory<?=$currentUniqueId; ?> #webinar-start-date").length >0) {
						$("#tabHistory<?=$currentUniqueId; ?> #webinar-start-date").off('change');
						$("#tabHistory<?=$currentUniqueId; ?> #webinar-start-date").on('change', function() {
							loadPastWebinars();
						});
					}
					}
				});
		}

		// Load the past webinars in the corresponding container
		loadPastWebinars();

		// Set the TabWeb (Live Sessions Tab) event listeners
		function setTabWebListeners() {
			// Do coaching webinar session status change
			$('#tabWeb<?=$currentUniqueId; ?> .select-status').on('click', function() {
				// Data to be passed to ajax
				var options = $(this).data();
				//console.log(options);

				// Reset group buttons
				$('#tabWeb<?=$currentUniqueId; ?> #going'+$(this).data('sessionid')).attr('class', 'select-status confirm-save btn btn-docebo green big coaching-webinar-gray-btn coaching-webinar-inactive-green-btn');
				$('#tabWeb<?=$currentUniqueId; ?> #maybe'+$(this).data('sessionid')).attr('class', 'select-status confirm-save btn btn-docebo green big coaching-webinar-gray-btn coaching-webinar-inactive-orange-btn');
				$('#tabWeb<?=$currentUniqueId; ?> #decline'+$(this).data('sessionid')).attr('class', 'select-status confirm-save btn btn-docebo green big coaching-webinar-gray-btn coaching-webinar-inactive-dark-gray-btn');

				// Show chosen(clicked) button
				var classes = $('#tabWeb<?=$currentUniqueId; ?> #'+$(this).attr('id')).attr('class');
				classes = classes.replace('-inactive', '')
				classes = classes.replace('coaching-webinar-gray-btn', '')
				$('#tabWeb<?=$currentUniqueId; ?> #'+$(this).attr('id')).attr('class', classes);

				// current tab id
				var currentTabId = '#tabWeb<?=$currentUniqueId; ?>';

				// Current coaching session id
				var idSession = options.sessionid;

				// Current course id
				var idCourse = <?=$blockModel->course_id; ?>;

				// Id of the current user
				var idUser = options.userid;

				var setStatus = options.value;

				// TODO - add action for changing the user webinar session status !!!
				var request = $.ajax({
					url: HTTP_HOST + 'index.php?r=player/block/axChangeWebinarSessionStatus&idSession='+idSession+'&course_id='+idCourse+'&idLearner='+idUser+'&setStatus='+setStatus,
					cache: false,
					method: "POST"
				});

				request.done(function( response ) {
					// todo fixme - decide what should be done here
				});

			});

			// Do redirect to coaching webinar url on button Start or Join click(-ed)
			$('#tabWeb<?=$currentUniqueId; ?> .coaching-webinar-start-btn').on('click', function() {
				window.location = $(this).data('url');
			});
		}

		<?php if(count($webinarSessions)): ?>
		// Clear the refresh Tab Web interval
		clearInterval(refreshActiveLiveSessions);
		// Set the refresh interval of the Tab Web here
		refreshActiveLiveSessions = setInterval(function() {
			localStorage.clear();

			// Load active coaching live (webinar) sessions
			$("#tabWeb<?=$currentUniqueId; ?> .active-webinars-list").load(
				HTTP_HOST + 'index.php?r=player/block/axLoadActiveLiveSessions&idSession=<?=$coachSession->idSession ?>&course_id=<?=$blockModel->course_id ?>&idUser=<?=Yii::app()->user->id; ?>&currentUniqueId=<?=$currentUniqueId; ?>',
				function( response, status, xhr ) {
					if (status == "error") {
						var msg = "Sorry but there was an error: ";
						$("#error").html(msg + xhr.status + " " + xhr.statusText);
					}

					if (status == "success") {
						setTabWebListeners();
						$(document).controls();
					}
				});
		}, 60000);
		<?php endif; ?>

		setTabWebListeners();

<?php
	 }

		?>

		$(document).controls();
        $(document).on('dialog2.content-update', '.coaching-webinar', function () {
            if ($(this).find("a.auto-close.successfullCreation").length > 0 || $(this).find("a.auto-close.successfullDeletion").length > 0) {
                //$(this).dialog2("close");
                $('.player-block-mycoach').loadOneBlockContent();
            }
        });
        $('.options-button').hover(function(){
            $(this).css({
                'background-color':'#FFF',
                'border': '1px solid #C5C5C5',
                'margin-left' : '2px',
                'margin-right' : '-1px',
                'margin-top' : '-1px'
            });
        },function(){
            $(this).css({
                'background-color':'transparent',
                'border-width': '0px',
                'margin-left' : '3px',
                'margin-right' : '0px',
                'margin-top' : '10px'
            });
        });
        $('.popover-options').hover(function(){
//            alert('In');
            $(this).prev('.options-button').css({
                'background-color':'#FFF',
                'border': '1px solid #C5C5C5',
                'margin-left' : '2px',
                'margin-right' : '-1px',
                'margin-top' : '-1px'
            });
            $(this).prev('.options-button').find('i').css('color','#000000');
        }, function(){
//            alert('Out');
            $(this).prev('.options-button').css({
                'background-color':'transparent',
                'border-width': '0px',
                'margin-left' : '3px',
                'margin-right' : '0px',
                'margin-top' : '10px'
            });
            $(this).prev('.options-button').find('i').css('color','#C5C5C5');
        });

	}( jQuery ));
</script>
