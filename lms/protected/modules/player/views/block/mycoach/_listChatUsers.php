<?php

if (!empty($sessionUsers) && is_array($sessionUsers) && count($sessionUsers) >0) {
	foreach ($sessionUsers as $key => $sessionUser) {
		// Get user last message
		$lastMessage = LearningCourseCoachingMessages::model()->getUserLastMessage($sessionUser->idUser, $sessionUser->idSession);

		// Get unread messages count
		$unreadMessages = LearningCourseCoachingMessages::model()->getUserUnreadMessages($sessionUser->idUser, $sessionUser->idSession);

		// Show user in chat
		if($key == 0) {
?>
<div id="learners-list-item-<?=$sessionUser->user->idst; ?>" class="learners-list-item first row-fluid">
<?php
		} else {
?>
<div id="learners-list-item-<?=$sessionUser->user->idst; ?>" class="learners-list-item row-fluid">
<?php
		}
?>
	<div class="span1 learner-avatar">
<?php
		echo $sessionUser->user->getAvatarImage(false, 'chat-avatar-image');
?>

	</div>
	<div class="span7 learner-names-column">
		<span class="learner-names">
<?php
		echo $sessionUser->user->getFullName();
?>
		</span>
	</div>
	<div class="span2 learner-last-message">

<?php
		// Fixme todo date diff
		if(is_array($lastMessage) && count($lastMessage) > 0 && !empty($lastMessage['date_added'])) {
			echo Yii::t('coaching', 'Last Message');
?>
		<br />
<?php
			// Get the last message date in local user time !!!
			$showDate = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($lastMessage['date_added']));
			$todayDate = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->getLocalNow());

			if ($showDate == $todayDate) {
				// If it is today show time
				//$showDate = Yii::app()->localtime->toLocalTime($lastMessage['date_added'], LocalTime::SHORT);
				$showDate = Yii::app()->localtime->toLocalTime(Yii::app()->localtime->toUserLocalDatetime(Yii::app()->user->id, $lastMessage['date_added']), LocalTime::SHORT);
			}

			// Show the last message date or time
			echo $showDate;
		} else {
			// If there is no prev messages show dash
			//echo '      &mdash;';
		}
?>
      <br />
	</div>

	<div class="span1 learner-num-messages">
<?php
		if ($unreadMessages != 0) {
?>

		<div class="coach-icon-holder">
			<i class="fa fa-envelope-o fa-lg"></i>
  			<div class="icon-number"><?=$unreadMessages; ?></div>
		</div>

<?php
		} else {
?>
		<i class="fa fa-envelope-o fa-lg"></i>
<?php
		}
?>
	</div>

	<div class="span1 pull-right learner-arrow-column">
		<i class="fa fa-angle-right fa-lg learner-arrow"></i>
	</div>
</div>
<?php
	}
} else {
	// No users to display
?>
<div class="no-users">
	<span class="no-users-span">
<?=Yii::t('coaching', 'There are no coaching session users to be displayed here'); ?>
	</span>
</div>
<?php
}
?>
