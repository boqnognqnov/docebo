<?php
if(count($webinarSessions)) {
    if ( !filter_var(Yii::app()->user->email, FILTER_VALIDATE_EMAIL)) { // user with invalid email
        $this->widget('common.widgets.warningStrip.WarningStrip', array(
            'message' => Yii::t('webinar', 'Your profile seems not to have a valid email setup'),
            'type' => 'warning',
        ));
    }
foreach ($webinarSessions as $webinarSession) {
?>
	<div class="active-webinar-session-row" style="border-bottom: 1px solid #E4E6E5;padding:10px;">
		<div class="webinarSessionInfo" style="display:inline-block;">
			<div><strong><?= $webinarSession->name; ?></strong></div>
			<div><i class="fa fa-clock-o fa-1x"></i> <span class="coach-webinar-time"><?php
					$todayDate = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->getLocalNow());
					$webinarSessionStartDate = Yii::app()->localtime->stripTimeFromLocalDateTime($webinarSession->date_begin);
					if ($todayDate != $webinarSessionStartDate) {
						echo $webinarSession->date_begin;
					} else {
						echo '<strong> ' . Yii::t('calendar', '_TODAY') . '</strong> '.  Yii::app()->localtime->toLocalTime($webinarSession->date_begin, LocalTime::SHORT);
					}

					?></span></div>
		</div>
		<div class="webinarSessionActions <?=(!(Yii::app()->localtime->isLte($webinarSession->date_begin, $nowTime) && Yii::app()->localtime->isGte($webinarSession->date_end, $nowTime)) && !$isUserCoach)? "has-user-status-btns" : "" ;?> <?= (Yii::app()->localtime->isLte($webinarSession->date_begin, $nowTime) && Yii::app()->localtime->isGte($webinarSession->date_end, $nowTime))?"withBtn":""?>" style="float:right;display:inline-block;">
			<?php

			$nowTime = Yii::app()->localtime->getLocalNow();
			// Display the button only if the webinar session is started and is not finished yet
			if (Yii::app()->localtime->isLte($webinarSession->date_begin, $nowTime) && Yii::app()->localtime->isGte($webinarSession->date_end, $nowTime)) {
				if($isUserCoach) {
					// if it is a coach
					echo CHtml::button(Yii::t('standard', '_START'),
						array(
							'class' => 'confirm-save btn btn-docebo green big coaching-webinar-btn coaching-webinar-start-btn',
							'data-url' => $webinarSession->getWebinarUrl($idUser)
						)
					);

					$countWebinarSessionUsers = $webinarSession->countWebinarSessionUsers();
					?>
					<div class="webinar-session-users-count-container">
						<a class="open-dialog"
						   data-dialog-class="wide"
						   data-dialog-id="coaching-webinar-session-guests-dialog"
						   data-dialog-title= "<?= Yii::t('coaching', 'Guests') ?>"
						   href="<?=Docebo::createLmsUrl('player/block/axLoadCoachingWebinarSessionGuests&course_id=' . $idCourse . '&idSession='.$webinarSession->id_session);?>">
							<div class="coach-icon-holder text-center">
								<i class="fa fa-user fa-lg"></i>
								<div class="icon-users-number <?=($countWebinarSessionUsers == 0) ? 'hide' : '';  ?>"><?=$countWebinarSessionUsers; ?></div>
							</div>
						</a>
					</div>
                    <div class="webinar-session-edit-container options-button">
                        <a href="javascript:">
                            <div class="coach-icon-holder text-center">
                                <i class="fa fa-ellipsis-v fa-lg"></i>
                            </div>
                        </a>
                    </div>
                    <div class="popover-options">
                        <ul>
                            <li>
                                <a
                                    href="/lms/index.php?r=player/block/axCreateCoachingWebinarSession&course_id=<?= $idCourse?>&webinar_session_id=<?=$webinarSession->id_session?>"
                                    class="open-dialog block-videoconf-add-room"
                                    data-dialog-class="modal-create-webinar-session coaching-webinar"
                                    rel="dialog-handle-room"
                                    data-block-id="<?= $blockModel->id ?>"
                                    title="<?=Yii::t('coaching', 'Edit live session')?>">
                                    <span class="i-sprite is-edit"></span> <?= Yii::t('standard','_MOD');?>
                                </a>
                            <li>
                                <a
                                    href="/lms/index.php?r=player/block/axDeleteCoachingWebinarSession&course_id=<?= $idCourse?>&webinar_session_id=<?=$webinarSession->id_session?>"
                                    class="open-dialog block-videoconf-add-room"
                                    data-dialog-class="modal-delete-webinar-session coaching-webinar"
                                    rel="dialog-handle-room"
                                    data-block-id="<?= $blockModel->id ?>"
                                    title="<?=Yii::t('coaching', 'Delete webinar session')?>">
                                    <span class="i-sprite is-remove red"></span> <?= Yii::t('standard','_DEL');?>
                                </a>
                            </li>
                        </ul>
                    </div>
					<?php
				} else {
					// It is a learner
                    if ( filter_var(Yii::app()->user->email, FILTER_VALIDATE_EMAIL)  || (isset($webinarSession->tool) && !in_array($webinarSession->tool, array('gotowebinar', 'gototraining', 'gotomeeting')))) {
                        echo CHtml::button(Yii::t('conference', 'Join'),
                            array(
                                'class' => 'confirm-save btn btn-docebo green big coaching-webinar-btn coaching-webinar-start-btn',
                                'data-url' => $webinarSession->getWebinarUrl($idUser)
                            )
                        );
                    } else {
                        echo CHtml::button(Yii::t('conference', 'Join'),
                            array(
                                'class' => 'disabled btn btn-docebo green big coaching-webinar-btn coaching-webinar-start-btn',
                                'data-url' => 'javascript:void(0);'
                            )
                        );
                    }
				}
			} elseif ($isUserCoach) {
				$countWebinarSessionUsers = $webinarSession->countWebinarSessionUsers();
				?>
				<div class="webinar-session-users-count-container">
					<a class="open-dialog"
					   data-dialog-class="wide"
					   data-dialog-id="coaching-webinar-session-guests-dialog"
					   data-dialog-title= "<?= Yii::t("coaching", "Guests") ?>"
					   href="<?=Docebo::createLmsUrl("player/block/axLoadCoachingWebinarSessionGuests&course_id=" . $idCourse . "&idSession=".$webinarSession->id_session);?>">
						<div class="coach-icon-holder text-center">
							<i class="fa fa-user fa-lg"></i>
							<div class="icon-users-number <?=($countWebinarSessionUsers == 0) ? "hide" : "";  ?>"><?=$countWebinarSessionUsers;  ?></div>
						</div>
					</a>
				</div>
                <div class="webinar-session-edit-container options-button">
                    <a href="javascript:">
                        <div class="coach-icon-holder text-center">
                            <i class="fa fa-ellipsis-v fa-lg"></i>
                        </div>
                    </a>
                </div>
                <div class="popover-options">
                    <ul>
                        <li>
                            <a
                                href="/lms/index.php?r=player/block/axCreateCoachingWebinarSession&course_id=<?= $idCourse?>&webinar_session_id=<?=$webinarSession->id_session?>"
                                class="open-dialog block-videoconf-add-room"
                                data-dialog-class="modal-create-webinar-session coaching-webinar"
                                rel="dialog-handle-room"
                                data-block-id="<?= $blockModel->id ?>"
                                title="<?=Yii::t('coaching', 'Edit live session')?>">
                                <span class="i-sprite is-edit"></span> <?= Yii::t('standard','_MOD');?>
                            </a>
                        <li>
                            <a
                                href="/lms/index.php?r=player/block/axDeleteCoachingWebinarSession&course_id=<?= $idCourse?>&webinar_session_id=<?=$webinarSession->id_session?>"
                                class="open-dialog block-videoconf-add-room"
                                data-dialog-class="modal-delete-webinar-session coaching-webinar"
                                rel="dialog-handle-room"
                                data-block-id="<?= $blockModel->id ?>"
                                title="<?=Yii::t('coaching', 'Delete webinar session')?>">
                                <span class="i-sprite is-remove red"></span> <?= Yii::t('standard','_DEL');?>
                            </a>
                        </li>
                    </ul>
                </div>
				<?php

			} elseif(!$isUserCoach) {
				// If user is a leaner show choose status buttons

				// Get the webinar session user data
				$webinarSessionUser = LearningCourseCoachingWebinarSessionUser::model()->findByAttributes(array('id_user' => $idUser, 'id_session' => $webinarSession->id_session));
				?>

				<div class="change-enrollment-status-btn-group btn-group">
					<button id="going<?=$webinarSession->id_session ?>" data-userid="<?=$idUser ?>" data-sessionid="<?=$webinarSession->id_session ?>" data-value="<?=LearningCourseCoachingWebinarSessionUser::STATUS_ACCEPTED ;?>"
					        class="btn-going select-status confirm-save btn btn-docebo green big
		                                                <?=(!empty($webinarSessionUser->id_session) && $webinarSessionUser->status == LearningCourseCoachingWebinarSessionUser::STATUS_ACCEPTED) ?  'coaching-webinar-green-btn' : 'coaching-webinar-gray-btn coaching-webinar-inactive-green-btn';?>">
						<?= Yii::t('coaching', 'Going'); ?>
					</button>
					<button id="maybe<?=$webinarSession->id_session ?>" data-userid="<?=$idUser ?>" data-sessionid="<?=$webinarSession->id_session ?>" data-value="<?=LearningCourseCoachingWebinarSessionUser::STATUS_MAYBE ;?>"
					        class="btn-maybe select-status confirm-save btn btn-docebo green big coaching-webinar-gray-btn
		                                                <?=(!empty($webinarSessionUser->id_session) && $webinarSessionUser->status == LearningCourseCoachingWebinarSessionUser::STATUS_MAYBE) ?  'coaching-webinar-orange-btn' : 'coaching-webinar-inactive-orange-btn';?>">
						<?= Yii::t('coaching', 'Maybe'); ?>
					</button>
					<button id="decline<?=$webinarSession->id_session ?>" data-userid="<?=$idUser ?>" data-sessionid="<?=$webinarSession->id_session ?>" data-value="<?=LearningCourseCoachingWebinarSessionUser::STATUS_DECLINED ;?>"
					        class="btn-decline select-status confirm-save btn btn-docebo green big coaching-webinar-gray-btn
		                                                <?=(!empty($webinarSessionUser->id_session) && $webinarSessionUser->status == LearningCourseCoachingWebinarSessionUser::STATUS_DECLINED) ?  'coaching-webinar-dark-gray-btn' : 'coaching-webinar-inactive-dark-gray-btn';?>">
						<?= Yii::t('coaching', 'Decline'); ?>
					</button>
				</div>

				<?php

			}
			?>

		</div>
	</div>
<?php
	}} else{
?>
	<div class="row-fluid">
		<div class="span1"><i class="fa fa-calendar"></i></div>
		<div class="span11">
			<p class="small" style="text-align: left;font-size: 14px;"><?=Yii::t('coaching', 'This coaching session doesn\'t have webinar sessions yet.');?>  </p>
		</div>
	</div>
<?php
	}
?>

<script type="text/javascript">
	(function( $ ) {

		// Do coaching webinar session status change
		$('#tabWeb<?=$currentUniqueId; ?> .select-status').on('click', function() {
			// Data to be passed to ajax
			var options = $(this).data();
			//console.log(options);

			// Reset group buttons
			$('#tabWeb<?=$currentUniqueId; ?> #going'+$(this).data('sessionid')).attr('class', 'select-status confirm-save btn btn-docebo green big coaching-webinar-gray-btn coaching-webinar-inactive-green-btn');
			$('#tabWeb<?=$currentUniqueId; ?> #maybe'+$(this).data('sessionid')).attr('class', 'select-status confirm-save btn btn-docebo green big coaching-webinar-gray-btn coaching-webinar-inactive-orange-btn');
			$('#tabWeb<?=$currentUniqueId; ?> #decline'+$(this).data('sessionid')).attr('class', 'select-status confirm-save btn btn-docebo green big coaching-webinar-gray-btn coaching-webinar-inactive-dark-gray-btn');

			// Show chosen(clicked) button
			var classes = $('#tabWeb<?=$currentUniqueId; ?> #'+$(this).attr('id')).attr('class');
			classes = classes.replace('-inactive', '')
			classes = classes.replace('coaching-webinar-gray-btn', '')
			$('#tabWeb<?=$currentUniqueId; ?> #'+$(this).attr('id')).attr('class', classes);

			//$('#tabWeb<?=$currentUniqueId; ?> #'+$(this).attr('id')).attr('style', 'border: 1px solid red;');
			// current tab id
			var currentTabId = '#tabWeb<?=$currentUniqueId; ?>';

			// Current coaching session id
			var idSession = options.sessionid;

			// Current course id
			var idCourse = <?=$idCourse; ?>;

			// Id of the current user
			var idUser = options.userid;

			var setStatus = options.value;

			// Change the user webinar session status !!!
			var request = $.ajax({
				url: HTTP_HOST + 'index.php?r=player/block/axChangeWebinarSessionStatus&idSession='+idSession+'&course_id='+idCourse+'&idLearner='+idUser+'&setStatus='+setStatus,
				cache: false,
				method: "POST"
			});

			request.done(function( response ) {
				// todo fixme - decide what should be done here
			});

		});

		// Do redirect to coaching webinar url on button Start or Join click(-ed)
		$('#tabWeb<?=$currentUniqueId; ?> .coaching-webinar-start-btn').on('click', function() {
			window.location = $(this).data('url');
		});

        $('.options-button').hover(function(){
            $(this).css({
                'background-color':'#FFF',
                'border': '1px solid #C5C5C5',
                'margin-left' : '2px',
                'margin-right' : '-1px',
                'margin-top' : '-1px'
            });
        },function(){
            $(this).css({
                'background-color':'transparent',
                'border-width': '0px',
                'margin-left' : '3px',
                'margin-right' : '0px',
                'margin-top' : '10px'
            });
        });
        $('.popover-options').hover(function(){
//            alert('In');
            $(this).prev('.options-button').css({
                'background-color':'#FFF',
                'border': '1px solid #C5C5C5',
                'margin-left' : '2px',
                'margin-right' : '-1px',
                'margin-top' : '-1px'
            });
            $(this).prev('.options-button').find('i').css('color','#000000');
        }, function(){
//            alert('Out');
            $(this).prev('.options-button').css({
                'background-color':'transparent',
                'border-width': '0px',
                'margin-left' : '3px',
                'margin-right' : '0px',
                'margin-top' : '10px'
            });
            $(this).prev('.options-button').find('i').css('color','#C5C5C5');
        });

	}( jQuery ));
</script>

