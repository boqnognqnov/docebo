<?php
/**
 * @var $blockModel
 * @var $form
 * 
 * @author Georgi Stefkoff
 */
?>
<div id="my-control-coach" class="control-group">
    <?=CHtml::label(Yii::t('standard', '_SHOW'), 'email_contact', array(
        'class' => 'control-label'
    ));?>
    <div class="controls checkbox inline-label">
        <?php
            echo $form->checkBox($blockModel, 'email_contact');
            echo Yii::t('coaching', 'Contact email');
        ?>
    </div>    
    <div class="controls checkbox inline-label">
        <?php
            echo $form->checkBox($blockModel, 'time_compared');
            echo Yii::t('coaching', "Coach's time compared with learner's time");
        ?>
    </div>
    <br>
    <div class="controls">
        <?=CHtml::label(Yii::t('coaching', "Coach's additional fields"), '', array(
    ));?>
    </div>
    <div class="controls inline-label">                
        <select name="PlayerBaseblock[addition_field][]" id="addition_fields" multiple="multiple">
            <?php
            if(!empty($blockModel->addition_field)):
                foreach ($blockModel->addition_field as $idField => $field):
                ?>
                <option class="selected" value="<?=$field;?>"><?=  CoreUserField::getTranslationById($field);?></option>
                <?php
                endforeach;
            endif;
            ?>
        </select>
    </div>
</div>

<div class="control-group">
    <?php
        echo CHtml::label(Yii::t('standard', 'Enable'), '', array(
                'class' => 'control-label top'
            ));
        ?>
    <div class="controls checkbox inline-label">
        <?php
            echo $form->checkBox($blockModel, 'msg_system');
            echo Yii::t('coaching', 'Learner-coach messages system');
        ?>
    </div>
    <div class="controls checkbox inline-label">
        <?php
        echo $form->checkBox($blockModel, 'webinar_system');
        echo Yii::t('coaching', "Coach's live session" );
        ?>
    </div>
</div>

<script type="text/javascript">
    $("#my-control-coach input, input#PlayerBaseblock_msg_system,input#PlayerBaseblock_webinar_system").styler();
    $('select#addition_fields').fcbkcomplete({
            json_url: '<?=Docebo::createAbsoluteLmsUrl('//mydashboard/DashletWidgets/AxAdditionalFieldsAutocomplete')?>',
            width: '98%',
            complete_text: '',
            oncreate: function(){
                //This is because, .form-horizontal is making select -> display: inline-block
                $('select#addition_fields').css('display', 'none');
            }
    });
</script>
