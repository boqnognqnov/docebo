<?php
/**
 * 
 * @author Georgi Stefkoff
 */
?>
<div class="error">
    <?=Yii::t('coaching', 'You are not assigned to any session!');?>
</div>
