<?php
// If we need to load just messages list without their container and write message box
if ($loadMessagesOnly === false) { ?>
<div class="messages-container message-container-<?=$idLearner?>">
<?php
	}

if (!empty($coachingMessages) && is_array($coachingMessages) && count($coachingMessages) >0) {
	foreach ($coachingMessages as $key => $messageItem) {	            

		$sentByCoach = ($messageItem->sent_by_coach) ? true : false;

		$avatarClass = 'span2 avatar-container'; // a class to be added to avatar
		$containerClass = '';

		// Show message in chat
?>
	<div class="row-fluid message-container" id="message_<?=$messageItem->idMessage?>">
<?php
		if (($idCoach == Yii::app()->user->idst && $sentByCoach) || ($idLearner == Yii::app()->user->idst && !$sentByCoach)) { // if current user is coach
			$avatarClass .= ' pull-right';
			$containerClass = 'pull-left';
?>

      <div id="message-list-item-<?=$key; ?>" class="message-list-item message-left span10">
<?php
		} else { // if current user is learner
			$avatarClass .= ' pull-left';
			$containerClass = 'pull-right';
?>
      <div id="message-list-item-<?=$key; ?>" class="message-list-item message-right span10 <?=$containerClass; ?>">
<?php
		}
?>
			<div class="row-fluid">
<?php

		if (($idCoach == Yii::app()->user->idst && $sentByCoach) || ($idLearner == Yii::app()->user->idst && !$sentByCoach)) { // if current user is coach or learner ?>

				<div class="span6 pull-right sent-by sent-by-right">
			<?php
			echo Yii::t('coaching', 'Me');
			$messageClass = 'message-text-right';
		} else {
?>
				<div class="span6 pull-left sent-by sent-by-left">
<?php
			if ($idCoach == Yii::app()->user->idst) {
				echo $learner->getFullName();
			} else {
				echo $coach->getFullName();
			}

			$messageClass = 'message-text-left';
		}
?>
				</div>
<?php
		if (($idCoach == Yii::app()->user->idst && $sentByCoach) || ($idLearner == Yii::app()->user->idst && !$sentByCoach)) { // if current user is coach
?>
				<div class="span6 pull-left date-sent date-sent-left">
<?php
		} else {
?>
				<div class="span6 pull-right date-sent date-sent-right">
<?php
		}

		// Get the last message date in local user time !!!
		$showDate = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($messageItem['date_added']));
		$todayDate = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->getLocalNow());

		if ($showDate == $todayDate) {
			// If it is today show time
			//$showDate = Yii::app()->localtime->toLocalTime(Yii::app()->localtime->toLocalDateTime($messageItem['date_added']), LocalTime::SHORT);
			$showDate = Yii::app()->localtime->toLocalTime(Yii::app()->localtime->toUserLocalDatetime(Yii::app()->user->id, $messageItem['date_added']), LocalTime::SHORT);
			//$showDate = Yii::app()->localtime->toUserLocalDatetime(Yii::app()->user->id, $messageItem['date_added']);
			//$showDate = Yii::app()->localtime->toLocalDatetime($messageItem['date_added']);
		}

		// Show the last message date or time
		echo $showDate;
?>
				</div>

				<div class="row-fluid">
                                    <?php if(($idCoach == Yii::app()->user->idst && $sentByCoach) || ($idLearner == Yii::app()->user->idst && !$sentByCoach)): ?>
					<div class="span1" style="min-height: 40px;max-height: 40px;">
						<div class="message-controls">
							<a href="javascript:" class="message-controls-link row-fluid"><i class="fa fa-ellipsis-v fa-lg"></i></a>
							<div class="message-controls-items row-fluid">
								<ul>
                                                                    <li><a href="<?=Docebo::createAbsoluteUrl('player/block/axEditCoachingMessage', array('id' => $messageItem->idMessage, 'course_id' => $idCourse))?>" class="edit-message open-dialog" data-dialog-class="edit-coaching-message" data-id-message="<?=$messageItem->idMessage?>"><i class="fa fa-pencil-square-o"></i>&nbsp;&nbsp;<?= Yii::t('standard', '_MOD') ?></a></li>
									<li><a href="javascript:" class="delete-message" data-id-message="<?=$messageItem->idMessage?>"><i class="fa fa-times remove-massage"></i>&nbsp;&nbsp;<?= Yii::t('standard', '_DEL') ?></a></li>
								</ul>
							</div>
						</div>
					</div>
                                    <?php endif; ?>
					<div class="span11 message-text <?=$messageClass; ?>">                                            
<?php
		echo $messageItem->message;
?>
					</div>
				</div>
			</div>

		</div>

		<div class="<?=$avatarClass; ?>">
<?php
		// Display avatar
		if ($sentByCoach) {
			echo $coach->getAvatarImage(false, 'chat-avatar-image');
		} else {
			echo $learner->getAvatarImage(false, 'chat-avatar-image');
		}
?>
		</div>
	</div>
<?php
	}

} else {
	// No messages to display
?>
		<div class="no-messages">
			<span class="no-messages-span">
<?php
	echo Yii::t('coaching', 'There are no messages to be displayed here');
?>
			</span>
		</div>
<?php
}
?>
		<div id="messagesBottom"></div>
<?php
// If we need to load just messages list without their container and write message box
if ($loadMessagesOnly === false) { /* end of the messages container */ ?>
	</div>

	<div class="write-message-container">
            <?php if(!$isSessionExpired) : ?>
<?php
	// Start the Form
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'send-coach-chat-message-form',
		'htmlOptions' => array(
			'class' => 'form ajax',
			//'data-grid-items' => 'true',
			'method' => 'POST'
		)
	));
?>
	<div class="write-message row-fluid">
		<div class="write-message-inner span12">

			<div class="textarea-container row-fluid">
				<div class="span10">
<?php
	echo CHtml::textArea('write_message', '', array(
							'id' => 'write-message-textarea',
							'class' => 'write-message-textarea pull-right',
							'placeholder' => Yii::t('coaching', 'Write here...')
						));

	// Coach id hidden field
	echo CHtml::hiddenField('coach', $idCoach);

	echo CHtml::hiddenField('sent_by', Yii::app()->user->id);
?>
				</div>
				<div class="send-message-button-container span2">

					<div class="write-message-button pull-left"><i class="fa fa-check fa-lg apply-icon"></i></div>
				</div>
			</div>

		</div>
	</div>
<?php
	// end the form
	$this->endWidget();        
?>
            <?php 
            else:
                ?>
            <p class="no-send-messages"><?= Yii::t('coaching', 'Your session is expired and you cannot send new messages') ?></p>
            <?php
            
            endif; ?>
</div>          
<?php } /* end of write message area */ ?>
          <script type="text/javascript">
              
              $(document).delegate('.modal.edit-coaching-message', 'dialog2.opened', function(){                  
                  $(this).css({'position': 'fixed'});
              });
              
              $(document).delegate('.modal.edit-coaching-message', 'dialog2.closed', function(){
                  var request = $.ajax({
                    url: HTTP_HOST + 'index.php?r=player/block/axCheckEditedCoachingChatMessage&idSession='+<?= $idSession ?>+'&course_id='+<?= $idCourse?>+'&idLearner='+ <?= $idLearner ?> +'&loadMessagesOnly=1',
                    cache: false,
                    method: "GET"
                });
                
                var that = this;

                request.done(function( response ) {       
                    
                    if(!response){
                        return false;
                    }
                    
                    var data = JSON.parse(response);                                       
                    
                    var idsLength = data.ids.length;
                    
                    if(idsLength === 0){
                        return;
                    }
                    
                    $.each(data.ids, function(key, value){
                        if($(".message-container-" + <?= $idLearner ?> + ' #message_' + key).length > 0){
                            $(".message-container-" + <?= $idLearner ?> + ' #message_' + key).find('.message-text').text(value);                            
                        }
                    });
                    });
              });
              $(document).controls();
          </script>