<?php
/**
 * Created by PhpStorm.
 * User: NikVas
 * Date: 27-Aug-15
 * Time: 09:51
 */
?>
<?= CHtml::beginForm('', 'POST', array('class'	=> 'ajax')); ?>
<?= CHtml::hiddenField('webinar_session_id', $webinar_session_id); ?>
<br />
<?= Yii::t('coaching', 'Are you sure you want to delete the live coaching session <strong>{name}</strong> (from <strong>{from}</strong>)', array(
    '{name}' 	=> $session_name,
    '{from}'	=> $starting_time,
));?>

<br />
<br />
<div id="proceed-check" class="coaching-mode-radio-btn" >
    <?php echo CHtml::checkBox("agree_deletion", false, array(
        'id'    =>  'proceed-check-btn',
        'class' =>  'coaching-delete-check',
    )); ?>
    <?php echo CHtml::label(Yii::t('standard','Yes, I confirm I want to proceed'), 'proceed-check-btn',array('class'=>'coaching-inline-label','style'=>'display:inline-block;')); ?>
</div>

<!-- For Dialog2 - they go in footer -->
<div class="form-actions">
    <?= CHtml::submitButton(Yii::t('standard', '_CONFIRM'), array(
            'name'  => 'confirm_button',
            'class' => 'coaching-session-delete-confirm btn-docebo green big disabled')
    ); ?>
    <?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' 	=> 'btn-docebo black big close-dialog')); ?>
</div>
<?php echo CHtml::endForm(); ?>

<script type="text/javascript">
    $(function(){
        $('.coaching-delete-radio,.coaching-delete-check').styler();
        $('.coaching-delete-modal .coaching-delete-radio').on('change',function() {
            var selectedValue = $(this).val();
            if (selectedValue == 'merge') {
                $('#proceed-check-btn').removeAttr('checked');
                $('#proceed-check').hide();
                $('.coaching-session-delete-confirm').removeClass('disabled');
                $('.coaching-session-delete-confirm').text("<?php echo Yii::t('standard', '_NEXT') ?>");
            } else {
                $('#proceed-check').show();
                $('.coaching-session-delete-confirm').addClass('disabled');
                $('.btn.btn-docebo.green.big').text("<?php echo Yii::t('standard', '_CONFIRM') ?>");
            }
        });

        $('#proceed-check-btn').on('change', function() {
            if($(this).is(':checked'))
                $('.coaching-session-delete-confirm').removeClass('disabled');
            else
                $('.coaching-session-delete-confirm').addClass('disabled');
        });

        $('.coaching-session-delete-confirm').on('click', function() {
            if($(this).hasClass('disabled'))
                return false;
        });
    });
</script>