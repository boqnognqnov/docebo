<?php

/**
 * @var $messageModel LearningCourseCoachingMessages
 */
/* @var $form CActiveForm */
?>

<h1><?=Yii::t('coaching', 'Edit message') ?></h1>

<?php
    echo CHtml::beginForm('', 'post', array(
        'class' => 'ajax'
    ));
?>
<div class="row-fluid">
    <div class="span4">
        <?= CHtml::label(Yii::t('coaching', 'Edit your message'), 'new_message') ?>
    </div>
    <div class="span8">
        <?=CHtml::textArea('new_message', $messageModel->message, array(
            'style' => '    width: 93%; min-height: 40px; resize: none'
        )) ?>
        <?=Chtml::hiddenField('id', $messageModel->idMessage) ?>
    </div>
</div>

<div class="form-actions">
    <input type="submit" name="edit" class="btn btn-docebo green big" value="<?php echo Yii::t('standard', '_SAVE'); ?>" />
    <input type="button" name="cancel" class="btn btn-docebo black big close-dialog" value="<?php echo Yii::t('standard', '_CANCEL'); ?>" />
</div>

<?php echo CHtml::endForm() ?>
