<?php
// If the coaching session is not expired
if(!$isSessionExpired){
	if(count($webinarSessions)) {
		foreach ($webinarSessions as $webinarSession) {
			?>
			<div class="past-webinar-session-row" style="border-bottom: 1px solid #E4E6E5;padding:10px; margin-left: -10px;  margin-right: -10px;">
				<div class="webinarSessionInfo" style="display:inline-block;">
					<div><strong><?= $webinarSession->name; ?></strong></div>
					<div><i class="fa fa-clock-o fa-1x"></i> <span class="coach-webinar-time"><?php
							$todayDate = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->getLocalNow());
							$webinarSessionStartDate = Yii::app()->localtime->stripTimeFromLocalDateTime($webinarSession->date_begin);
							if ($todayDate != $webinarSessionStartDate) {
								echo $webinarSession->date_begin;
							} else {
								echo '<strong> ' . Yii::t('calendar', '_TODAY') . '</strong> '.  Yii::app()->localtime->toLocalTime($webinarSession->date_begin, LocalTime::SHORT);
							}

							?></span></div>
				</div>
				<div class="webinarSessionActions" style="float:right;display:inline-block;">
					<?php

					$nowTime = Yii::app()->localtime->getLocalNow();
					// Display the button only if the webinar session is started and is not finished yet
					if (Yii::app()->localtime->isLte($webinarSession->date_begin, $nowTime) && Yii::app()->localtime->isGte($webinarSession->date_end, $nowTime)) {
						if($isUserCoach) {
							// if it is a coach
							echo CHtml::button(Yii::t('standard', '_START'),
								array('class' => 'confirm-save btn btn-docebo green big coaching-webinar-btn')
							);

							$countWebinarSessionUsers = $webinarSession->countWebinarSessionUsers();
							?>
							<div class="webinar-session-users-count-container">
								<div class="coach-icon-holder ">
									<i class="fa fa-user fa-lg"></i>
									<div class="icon-users-number <?=($countWebinarSessionUsers == 0) ? 'hide' : '';  ?>"><?=$countWebinarSessionUsers; ?></div>
								</div>
							</div>
                            <div class="webinar-session-edit-container options-button">
								<a
									href="/lms/index.php?r=player/block/axDeleteCoachingWebinarSession&course_id=<?= $idCourse?>&webinar_session_id=<?=$webinarSession->id_session?>"
									class="open-dialog block-videoconf-add-room"
									data-dialog-class="modal-delete-webinar-session coaching-webinar"
									rel="dialog-handle-room"
									data-block-id="<?= $blockModel->id ?>"
									title="<?=Yii::t('coaching', 'Delete webinar session')?>">
                                    <div class="coach-icon-holder text-center">
                                        <i class="i-sprite is-remove red"></i>
                                    </div>
                                </a>
                            </div>
							<?php
						} else {
							// It is a learner
							echo CHtml::button(Yii::t('conference', 'Join'),
								array('class' => 'confirm-save btn btn-docebo green big coaching-webinar-btn')
							);
						}
					} elseif ($isUserCoach) {
						$countWebinarSessionUsers = $webinarSession->countWebinarSessionUsers();
						?>
						<div class="webinar-session-users-count-container past-session">
							<div class="coach-icon-holder ">
								<i class="fa fa-user fa-lg"></i>
								<div class="icon-users-number <?=($countWebinarSessionUsers == 0) ? 'hide' : '';  ?>"><?=$countWebinarSessionUsers;  ?></div>
							</div>
						</div>
                        <div class="webinar-session-edit-container options-button">
							<a
								href="/lms/index.php?r=player/block/axDeleteCoachingWebinarSession&course_id=<?= $idCourse?>&webinar_session_id=<?=$webinarSession->id_session?>"
								class="open-dialog block-videoconf-add-room"
								data-dialog-class="modal-delete-webinar-session coaching-webinar"
								rel="dialog-handle-room"
								data-block-id="<?= $blockModel->id ?>"
								title="<?=Yii::t('coaching', 'Delete webinar session')?>">
                                <div class="coach-icon-holder text-center">
                                    <i class="i-sprite is-remove red"></i>
                                </div>
                            </a>
                        </div>
						<?php

					}
					?>

				</div>
			</div>
		<?php
		}
	} else {
		?>
		<div class="row-fluid">
			<div class="span1"><i class="fa fa-calendar"></i></div>
			<div class="span11">
				<p class="small" style="text-align: left;font-size: 14px;"><?=Yii::t('coaching', 'This coaching session doesn\'t have webinar sessions yet.');?>  </p>
			</div>
		</div>
	<?php
	}
} else {
	?>
	<div class="row-fluid">
		<div class="span1"><i class="fa fa-calendar"></i></div>
		<div class="span11">
			<p class="small" style="text-align: left;font-size: 14px;"><?=Yii::t('coaching', 'Your coaching session');?> (<?=Yii::t('standard', '_FROM');?> <?=$sessionStartDate; ?> <?=Yii::t('standard', '_TO');?> <?=$sessionEndDate; ?>) <b class="error"><?=Yii::t('coaching', 'has expired');?></b></p>
		</div>
	</div>
<?php }; ?>

<script type="text/javascript">

	$(document).controls();
</script>
