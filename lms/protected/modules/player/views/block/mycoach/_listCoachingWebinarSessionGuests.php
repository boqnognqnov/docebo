<div class="content">
	<div class="row-fluid">
		<div class="span12">
		<div class="invited-guests">
				<?=Yii::t('coaching', 'Invited guests') . ": <strong>" . $countByStatus['all'] . '/' . $coachingSessionLearnersCount . '</strong>'; ?>
			</div>
		</div>
	</div>
	<div class="filters filters_course_management">

		<div class="row-fluid filter-by-coaching-session-status">

		<div class="span6">
		<?php
		echo CHtml::radioButton('filter_guests',true ,array('id' => 'filter_guests_0', 'value' => 'statusAll'));
		echo CHtml::label(Yii::t('coaching', 'All Guests') . ' (' . $countByStatus['all'] . ')', 'filter_guests_0', array('id' => 'filter_guests_0_label'));
		?>
		</div>
			<div class="span6">
				<div class="row-fluid">
					<div class="span4">
				<?php
				echo CHtml::radioButton('filter_guests',false ,array('id' => 'filter_guests_1', 'value' => 'status1'));
				echo CHtml::label(Yii::t('coaching', 'Going') . ' (' . $countByStatus[1] . ')', 'filter_guests_1', array('id' => 'filter_guests_1_label'));
				?>
					</div>
					<div class="span4">
				<?php
				echo CHtml::radioButton('filter_guests',false ,array('id' => 'filter_guests_2', 'value' => 'status2'));
				echo CHtml::label(Yii::t('coaching', 'Maybe') . ' (' . $countByStatus[2] . ')', 'filter_guests_2', array('id' => 'filter_guests_2_label'));
				?>
					</div>
					<div class="span4">
				<?php
				echo CHtml::radioButton('filter_guests',false ,array('id' => 'filter_guests_3', 'value' => 'status3'));
				echo CHtml::label(Yii::t('coaching', 'Declined') . ' (' . $countByStatus[3] .')', 'filter_guests_3', array('id' => 'filter_guests_3_label'));
				?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="guests-container bottom-section border-bottom-section clearfix">

	<?php
	$statuses = array(
		LearningCourseCoachingWebinarSessionUser::STATUS_INVITED => '<span style="font-weight: bold; color: #000;">'. Yii::t('coaching', 'Invited') .'</span>',
		LearningCourseCoachingWebinarSessionUser::STATUS_ACCEPTED => '<span style="font-weight: bold; color: #5EBE5D;">'. Yii::t('coaching', 'Going') .'</span>',
		LearningCourseCoachingWebinarSessionUser::STATUS_MAYBE => '<span style="font-weight: bold; color: #E58024;">'. Yii::t('coaching', 'Maybe') .'</span>',
		LearningCourseCoachingWebinarSessionUser::STATUS_DECLINED =>'<span style="font-weight: bold; color: #B2B2B2;">'.  Yii::t('coaching', 'Declined') .'</span>'
	);

	// Check is there webinar members to be displayed
	if (is_array($guests) && count($guests)) {
		foreach ($guests as $guest) {
			echo '<div class="member-by-status-container status'.$guest->status.'">';

			echo $guest->idUser->getAvatarImage(false, 'coaching-webinar-avatar-image', 'margin: 10px; float: left;');

			echo '<div style="float: left; margin-top: 10px;">';
			$names = $guest->idUser->getFullName();
			if ($guest->status == LearningCourseCoachingWebinarSessionUser::STATUS_DECLINED) {
				echo '<span style="color: #B2B2B2;">';
			}
			if ($names == '') {
				echo $names .= ' (' . $guest->idUser->getUserNameFormatted() . ') <br />';
			} else {
				echo $names . ' <br />';
			}

			if ($guest->status == LearningCourseCoachingWebinarSessionUser::STATUS_DECLINED) {
				echo '</span>';
			}
			echo $statuses[$guest->status];


			echo '</div>';

			echo '</div>';

		}
	} else {
		echo '<div style="border: 1px solid #e5e5e5; background-color: #07cdde;">';
		// There are currently no guests invited to this coaching webinar
		echo Yii::t('coaching', 'There are no coaching session members invited to this webinar session');
		echo '</div>';
	}

	?>

	</div>

	<div style="row-fluid">
		<div class="span12 form-actions" style="display: ; border: 0; background: #fff;">
			<?= CHtml::button(Yii::t('standard', '_CLOSE'), array(
				'class' => 'btn btn-cancel close-dialog'
			)) ?>

		</div>
	</div>
</div>

<script type="text/javascript">
	(function ($) {
		$(function () {
			// Hide all guests
			function hideAll() {
				$('.status0,.status1,.status2,.status3').hide();
			}

			// Show all guests
			function showAll() {
				$('.status0,.status1,.status2,.status3').show();
			}

			// Apply styler on the radio buttons
			$('#filter_guests_0,#filter_guests_1,#filter_guests_2,#filter_guests_3').styler();

			// Handle event click on the styler spans
			$('#filter_guests_0-styler').on('click', function() { showAll(); });
			$('#filter_guests_1-styler').on('click', function() { hideAll(); $('.status1').show(); });
			$('#filter_guests_2-styler').on('click', function() { hideAll(); $('.status2').show(); });
			$('#filter_guests_3-styler').on('click', function() { hideAll(); $('.status3').show(); });

			// Handle event click on the radio btns labels
			$('#filter_guests_0_label').on('click', function() { showAll(); });
			$('#filter_guests_1_label').on('click', function() { hideAll(); $('.status1').show(); });
			$('#filter_guests_2_label').on('click', function() { hideAll(); $('.status2').show(); });
			$('#filter_guests_3_label').on('click', function() { hideAll(); $('.status3').show(); });
		});

	})(jQuery);
</script>
