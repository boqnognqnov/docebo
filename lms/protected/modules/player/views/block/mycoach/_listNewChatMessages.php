<?php

$addNewMessages = '';

if (!empty($coachingMessages) && is_array($coachingMessages) && count($coachingMessages) >0) {
	foreach ($coachingMessages as $key => $messageItem) {

		$sentByCoach = ($messageItem->sent_by_coach) ? true : false;

		$avatarClass = 'span2 avatar-container'; // a class to be added to avatar
		$containerClass = '';

		// Show message in chat
		$addNewMessages .= '<div class="row-fluid message-container" id="message_' . $messageItem->idMessage . '">';

		if (($idCoach == Yii::app()->user->idst && $sentByCoach) || ($idLearner == Yii::app()->user->idst && !$sentByCoach)) { // if current user is coach
			$avatarClass .= ' pull-right';
			$containerClass = 'pull-left';


			$addNewMessages .= '<div id="message-list-item-'.$key.'" class="message-list-item message-left span10">';

		} else { // if current user is learner
			$avatarClass .= ' pull-left';
			$containerClass = 'pull-right';
			$addNewMessages .= '<div id="message-list-item-'.$key.'" class="message-list-item message-right span10 '.$containerClass.'">';
		}

		$addNewMessages .= '<div class="row-fluid">';


		if (($idCoach == Yii::app()->user->idst && $sentByCoach) || ($idLearner == Yii::app()->user->idst && !$sentByCoach)) {
			// if current user is coach or learner
			$addNewMessages .= '<div class="span6 pull-right sent-by sent-by-right">';

			$addNewMessages .= Yii::t('coaching', 'Me');
			$messageClass = 'message-text-right';
		} else {

			$addNewMessages .= '<div class="span6 pull-left sent-by sent-by-left">';

			if ($idCoach == Yii::app()->user->idst) {
				$addNewMessages .= $learner->getFullName();
			} else {
				$addNewMessages .= $coach->getFullName();
			}

			$messageClass = 'message-text-left';
		}

		$addNewMessages .= '</div>';

		if (($idCoach == Yii::app()->user->idst && $sentByCoach) || ($idLearner == Yii::app()->user->idst && !$sentByCoach)) { // if current user is coach
			$addNewMessages .= '<div class="span6 pull-left date-sent date-sent-left">';
		} else {
			$addNewMessages .= '<div class="span6 pull-right date-sent date-sent-right">';
		}

		// Get the last message date in local user time !!!
		$showDate = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($messageItem['date_added']));
		$todayDate = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->getLocalNow());

		if ($showDate == $todayDate) {
			// If it is today show time
			//$showDate = Yii::app()->localtime->toLocalTime(Yii::app()->localtime->toLocalDateTime($messageItem['date_added']), LocalTime::SHORT);
			$showDate = Yii::app()->localtime->toLocalTime(Yii::app()->localtime->toUserLocalDatetime(Yii::app()->user->id, $messageItem['date_added']), LocalTime::SHORT);
			//$showDate = Yii::app()->localtime->toUserLocalDatetime(Yii::app()->user->id, $messageItem['date_added']);
			//$showDate = Yii::app()->localtime->toLocalDatetime($messageItem['date_added']);
		}

		// Show the last message date or time
		$addNewMessages .= $showDate;

		$addNewMessages .= '</div>';

		$addNewMessages .= '<div class="row-fluid">';
		$addNewMessages .= '<div class="span12 message-text '.$messageClass.'">';

		$addNewMessages .= $messageItem->message;

		$addNewMessages .= '</div>';
		$addNewMessages .= '</div>';
		$addNewMessages .= '</div>';

		$addNewMessages .= '</div>';

		$addNewMessages .= '<div class="'.$avatarClass.'">';

		// Display avatar
		if ($sentByCoach) {
			$addNewMessages .= $coach->getAvatarImage(false, 'chat-avatar-image');
		} else {
			$addNewMessages .= $learner->getAvatarImage(false, 'chat-avatar-image');
		}

		$addNewMessages .= '</div>';
		$addNewMessages .= '</div>';
	}

	//$addNewMessages .= '<div id="messagesBottom"></div>';
}

// if there are a new messages to br displayed proceed with appending them to the list of messages !!!
if ($addNewMessages <> '') {
	echo $addNewMessages;
} else {
	echo 'no-new-messages';
}

?>

