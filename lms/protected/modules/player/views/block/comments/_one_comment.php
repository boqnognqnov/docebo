<?php
	$posterUserModel = CoreUser::model()->findByPk($message->author);
	if ($posterUserModel) {
		$avatar =  $posterUserModel->getAvatarImage();
		$userName = $posterUserModel->firstname . ' ' . $posterUserModel->lastname;
		$ownMessage = Yii::app()->user->id == $posterUserModel->idst;
	} else {
		// user doesn't exists maybe it has been deleted #60066424
		$avatar = CoreUser::getAnonymousAvatarImage();
		$userName = Yii::t('event_manager', '_EVENT_CLASS_UserDel');
		$ownMessage = false;
	}
    $utcPostedTime = Yii::app()->localtime->fromLocalDateTime($message->posted);
	$isToday = (date('Ymd', strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'))) === date('Ymd', strtotime($utcPostedTime)));
	$postedDateString = $isToday
        ? Yii::t('calendar', '_TODAY') . ' ' . Yii::app()->localtime->toLocalDateTime($utcPostedTime, null, null, 'H:i')
        : $message->posted;
?>

<!-- PLEASE LEAVE block-comments-comment-id-..... as CLASS!!! Ask plamen why (Hint: fancybox delete comment...) -->
<div class="player-comments-block-comment block-comments-comment-id-<?= $message->idMessage ?>">
	<div class="row-fluid">

		<div class="span1">
			<?= $avatar; ?>
		</div>

		<div class="span11">
			<small class="pull-right"><?= $postedDateString ?></small>

			<div class="author text-colored"><?= $userName ?></div>

			<?php if (Yii::app()->user->isGodAdmin || $ownMessage) : ?>
				<a href="#" class="pull-right remove-forum-message" data-message-id="<?= $message->idMessage ?>">
					<span class="p-sprite remove-micro-grey"></span>
				</a>
			<?php endif; ?>

			<div class="message-expandable">
				<?
				$text = $message->textof;
				$text = preg_replace('/\[QUOTE=([^\]]*);\]/ui', '<fieldset class="forum-quote"><legend class="forum-quote-author">$1</legend>', $text);
				if (stripos($text,'[/quote]') !== false)
					$text = str_ireplace('[/quote]', '</fieldset>', $text);

				echo $text;
				?>
			</div>
		</div>

	</div>
</div>