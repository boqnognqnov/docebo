<?php
	$commentsCount = count($messages)-1;
	$writeCommentPlaceholder = ($commentsCount <= 0) ? Yii::t('forum', 'Write here your comment') : Yii::t('forum', 'Write here your comment');
	$allComments = array_slice($messages, 1, count($messages)-1, true);

	if ($showAllComments) {
		$lastNComments = $allComments;
	}
	else {
		$n = (count($messages) > $commentsLimit) ? $commentsLimit : $commentsCount;
		$lastNComments = array_slice($messages, -$n, $n, true);
	}

	$discussionId = null;
	if ($commentsCount > 0) {
		$discussion = $messages[0];
		$discussionId = $discussion->idThread;
	}

?>

<!-- Comments row & container listing all comments (i.e. all messages except first one) -->
<div class="block-comments-comments-container" id="block-comments-comments-<?= $blockModel->id ?>-<?= $thread->idThread ?>">
	<div class="row-fluid">
		<div class="span12">

			<div class="block-comments-comments-container-inner">

			<?php
				foreach ($lastNComments as $message) {
					Yii::app()->controller->renderPartial('player.views.block.comments._one_comment', array(
						'message' => $message,
					));
				}
			?>

			</div>

			<!-- ADD YOUR COMMENT to THIS THREAD -->
			<form action="#" id="block-comments-add-comment-<?= $blockModel->id ?>">
				<input type="hidden" name="thread_id" value="<?= $thread->idThread ?>">
				<input type="hidden" name="user_id" value="<?= Yii::app()->user->id ?>">
				<input type="hidden" name="course_id" value="<?= Yii::app()->player->course->idCourse ?>">
				<input type="hidden" name="title" value="">


				<div  class="player-comments-block-add-comment">
					<div class="row-fluid">
						<div class="span1">
							<?php echo Yii::app()->user->getAvatar(); ?>
						</div>
						<div class="span11">
							<!--
							<input type="text" name="comment_text" class="input-block-level" placeholder="<?= $writeCommentPlaceholder ?>">
							 -->
							<textarea name="comment_text" class="input-block-level" placeholder="<?= $writeCommentPlaceholder ?>"></textarea>
							<input type="submit" name="confirm_comment" value="<?= Yii::t('standard', '_PUBLISH') ?>" class="btn-docebo green big pull-right">
						</div>
					</div>
				</div>


			</form>



		</div>
	</div>
</div>


<script type="text/javascript">
<!--


//-->
</script>