<?php
	// All messages except first one are COMMENTS (in our terms)
	$commentsCount = count($messages)-1;

	// Discussion creator
	$posterUserModel = CoreUser::model()->findByPk($messages[0]->author);
	if ($posterUserModel) {
		$avatar =  $posterUserModel->getAvatarImage();
		$userName = $posterUserModel->firstname . ' ' . $posterUserModel->lastname;
	} else {
		// user doesn't exists maybe it has been deleted #60066424
		$avatar = CoreUser::getAnonymousAvatarImage();
		$userName = Yii::t('event_manager', '_EVENT_CLASS_UserDel');
	}

	// Time of the discussion creation
    $utcPostedTime = Yii::app()->localtime->fromLocalDateTime($thread->posted);
    $isToday = (date('Ymd', strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'))) === date('Ymd', strtotime($utcPostedTime)));

	// ..for display purposes
	$postedDateString = $isToday
        ? Yii::t('calendar', '_TODAY') . ' ' . Yii::app()->localtime->toLocalDateTime($utcPostedTime, null, null, 'H:i')
        : $thread->posted;

	$openingMessage = $messages[0];
	$lastNComments = $messages;


?>

<!-- Single Discussion row  -->

<div class="player-comments-block-discussion discussion-id-<?= $thread->idThread ?>">

	<div class="row-fluid">

		<!-- Discussion poster avatar -->
		<div class="span1">
			<?= $avatar; ?>
		</div>

		<div class="span11">
			<small class="pull-right"><?= $postedDateString ?></small>

			<!-- Discussion Poster info -->
			<div class="author text-colored"><?= $userName ?></div>
			<?php if (Yii::app()->user->isGodAdmin) : ?>
				<a href="#" class="pull-right remove-comment-thread" data-thread-id="<?= $thread->idThread ?>">
					<span class="p-sprite remove-micro-grey"></span>
				</a>
			<?php endif; ?>
			<!-- Discussion First Post -->
			<div class="message-expandable">
				<?= nl2br($openingMessage->textof) ?>
			</div>

			<a href="javascript:;" class="do-comment">
				<span class="comments-count text-colored">
					<?= Yii::t('standard', '_COMMENTS') . ' (<span class="count-number">' . $commentsCount . '</span>)' ?>
				</span>
			</a>

			<?php
				$commentsStyle = "";
				if ($commentsCount <= 0) {
					$commentsStyle = "display: none;";
				}
			?>

			<div style="<?= $commentsStyle ?>" class="comments-container">

				<?php if ($showViewAll && ($commentsCount > 0) && ($commentsCount > $commentsLimit )) : ?>
				<div class="text-right view-all">
					<a href="#"

						id="block-view-all-comments-link-block-id-<?= $blockModel->id ?>"
						data-block-id="<?= $blockModel->id ?>"
						data-discussion-id="<?= $thread->idThread ?>">
						<span class="comments-count text-colored">
							<?php
								echo Yii::t('standard', 'Load More') . ' (<span class="count-number">' . $commentsCount . '</span>)';
							?>
						</span>

					</a>
				</div>
				<?php endif; ?>


				<!-- Comments (the rest of messages) -->
				<?php
					Yii::app()->controller->renderPartial('player.views.block.comments._comments', array(
						'messages' => $messages,
						'currentUserName' => $currentUserName,
						'commentsOffset' => $commentsOffset,
						'commentsLimit' => $commentsLimit,
						'showAllComments' => $showAllComments,
						'thread' => $thread,
						'blockModel' => $blockModel,
					));
				?>
			</div>

		</div>

	</div>

</div>



<script type="text/javascript">
<!--




//-->
</script>
