<?php 
/**
 * @var $blockModel
 * @var $form
 */

	// List only forums tied to the course of this block
	$criteria = new CDbCriteria();
	$criteria->addCondition('idCourse=:course_id');
	$criteria->params[':course_id'] = $blockModel->course_id;
	
	$forums = LearningForum::model()->findAll($criteria);
	if (count($forums))
	{
		$listData = CHtml::listData($forums, "idForum", function($m) {
			return CHtml::encode($m->title);
		});
		echo CHtml::activeLabel($blockModel, "forum_id");
		echo $form->dropDownList($blockModel, "forum_id", $listData, array("class" => "input-block-level"));
	}

?>
