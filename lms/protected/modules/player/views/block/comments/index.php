<?php
	$countDiscussions = count($discussions);
	$currentUserName = $currentUserModel->firstname . ' ' . $currentUserModel->lastname;
	$writeDiscussionPlaceholder = (count($discussions) <= 0) ?  Yii::t('forum', 'Write here your comment') : Yii::t('forum', 'Write here your comment');
	$addDiscussionUrl = Yii::app()->createUrl("//forum/default/axAddDiscussionFromBlock", array());
?>

<div class="player-comments-block">

	<?php if ($showCreateDiscussion) : ?>
	<!-- Create Discussion Interface. Always on top -->


	<form action="<?= $addDiscussionUrl	?>" id="block-comments-add-discussion-<?= $blockModel->id ?>">
		<input type="hidden" name="block_id" value="<?= $blockModel->id ?>">
		<input type="hidden" name="forum_id" value="<?= $forumModel->idForum ?>">
		<input type="hidden" name="user_id" value="<?= Yii::app()->user->id ?>">
		<input type="hidden" name="course_id" value="<?= Yii::app()->player->course->idCourse ?>">
		<input type="hidden" name="title" value="">


		<div class="player-comments-block-add-comment">
			<div class="row-fluid">
				<div class="span1">
					<?php echo Yii::app()->user->getAvatar(); ?>
				</div>
				<div class="span11">
					<!--
					<input type="text" name="opening_message_text" class="input-block-level" placeholder="<?= $writeDiscussionPlaceholder ?>">
					 -->
					<textarea name="opening_message_text" class="input-block-level comment-input-field" placeholder="<?= $writeDiscussionPlaceholder ?>"></textarea>

					<input type="submit" name="confirm_opening_message" value="<?= Yii::t('standard', '_PUBLISH') ?>" class="btn-docebo green big pull-right">
				</div>
			</div>
		</div>
	</form>

	<?php endif; ?>


	<!-- Discussions; List all topics from the forum; should be in DESC order  -->
	<div id="block-comments-discussions-<?= $blockModel->id ?>">
		<?php
			// Enumerate discussions
			foreach ($discussions as $thread) {
				// All messages in the discussion
				$messages = $thread->messages;

				Yii::app()->controller->renderPartial('player.views.block.comments._one_discussion', array(
					'messages' => $messages,
					'commentsOffset' => $commentsOffset,
					'commentsLimit' => $commentsLimit,
					'thread' => $thread,
					'blockModel' => $blockModel,
					'currentUserName' => $currentUserName,
					'showViewAll' => $showViewAll,
					'showAllComments' => $showAllComments,
				));
			}

		?>
	</div>

	<!-- TEST LOAD MORE -->
	<?php if ($showLoadMore && ($countTotalDiscussions > $countDiscussions)) : ?>
		<div id="block-more-discussions-loaded-block-id-<?= $blockModel->id ?>"></div>
		<div class="load-more text-center">
			<a href="#" data-block-id="<?= $blockModel->id ?>" id="block-load-more-discussions-block-id-<?= $blockModel->id ?>"><?= Yii::t('standard', 'Load More') . '&hellip;' ?></a>
		</div>
	<?php endif; ?>

</div>

<script type="text/javascript">
<!--

	// Keep track of loaded Comments Blocks
	if (!$.loadedCommentBlocks) {
	    $.loadedCommentBlocks = {};
	}

	$(function(){

		// IE placeholder fix!
		$('.comment-input-field').val('');

		// CommentBlock object is created on-demand, because some operations MUST NOT create new one
		<?php if ($InitBlocksJs) : ?>
				oCommentsBlock = new CommentsBlock('<?= $blockModel->id ?>', '<?= $discussionsOffset ?>', '<?= $discussionsLimit ?>');
				$.loadedCommentBlocks[<?= $blockModel->id ?>] = oCommentsBlock;
		<?php endif; ?>
		$.loadedCommentBlocks[<?= $blockModel->id ?>].attachExpander();

	});
//	-->
</script>
