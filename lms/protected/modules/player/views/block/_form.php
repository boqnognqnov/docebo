<?php
/* @var $form CActiveForm */
/* @var $model PlayerBaseblock */
/* @var $blockTypes array */
?>
<!-- <h1> is 'consumed' by Dialog2 as Dialog title -->
<h1><?= Yii::t('standard', '_ADD') ?>/<?= Yii::t('standard', '_MOD') ?></h1>

<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'block-form',
	'enableAjaxValidation' => false,
	'htmlOptions' => array(
		'class' => 'form-horizontal ajax' // <----- 'ajax' makes the form ajax submitted by Dialog2
	)
));
$params = json_decode($model->params, true);
?>
<input type="hidden" value="<?=$params['canNormalUserUploadFiles']?>" name="PlayerBaseblock[canNormalUserUploadFiles]" />
<input type="hidden" value="<?=$params['fileLimit']?>" name="PlayerBaseblock[fileLimit]" />
<input type="hidden" value="<?=$params['iframeUrl']?>" name="PlayerBaseblock[iframeUrl]" />
<input type="hidden" value="<?=$params['iframeHeight']?>" name="PlayerBaseblock[iframeHeight]" />
<input type="hidden" value="<?=$params['oauthClient']?>" name="PlayerBaseblock[oauthClient]" />
<input type="hidden" value="<?=$params['secretSalt']?>" name="PlayerBaseblock[secretSalt]" />

<?php echo $form->errorSummary($model); ?>

<div class="control-group">
	<?= $form->labelEx($model, 'content_type', array('class' => 'control-label')); ?>
	<div class="controls">
		<?= $form->dropDownList($model, 'content_type', $blockTypes, array('class' => 'input-block-level', 'id' => 'content-type')); ?>
	</div>
</div>

<!-- SETTING Column: tricky part -->
<!-- @TODO Some optimization would be nice -->
<div class="">
	<?php foreach($blockTypes as $type => $name) : ?>
		<div id="content-settings-<?= $type ?>" style="display: none;">
			<?php
			if ($type != $model->content_type) {
				$tmpModel = new PlayerBaseblock();
				$tmpModel->content_type = $type;
				$tmpModel->attachContentBehavior();
				$tmpModel->course_id = $model->course_id;
			} else {
				$tmpModel = $model;
			}                        
			echo $this->renderContentTypeParameters($tmpModel, $type, $form);
			?>
		</div>
	<?php endforeach; ?>
</div>

<div class="control-group">
	<?= $form->labelEx($model, 'columns_span', array('class' => 'control-label')); ?>
	<div class="controls inline-label">
		<?php
		// set default
		if (empty($model->columns_span)) {
			$model->columns_span = 6;
		}
		echo CHtml::activeRadioButtonList(
			$model, 'columns_span',
			array(
				'6' => '<span class="p-sprite inline-block half-size"></span>  ' . Yii::t('player', 'Half size'),
				'12' => '<span class="p-sprite inline-block full-size"></span>  ' . Yii::t('player', 'Full size')
			),
			array(
				'separator' => '<br><br>',
			)
		);
		?>
	</div>
</div>



<!-- Fake Dialog2 form! Generates buttons in the footer. Class "form-actions" is the key! -->
<div class="form-actions">
	<input class="btn-docebo green big" id="block-edit-save-button" type="submit"
		   value="<?php echo Yii::t('standard', '_SAVE'); ?>">&nbsp;&nbsp;
	<input class="btn-docebo black big close-dialog" type="button" value="<?php echo Yii::t('standard', '_CANCEL'); ?>">
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(function () {

		// On load show content type specific settings pane
		$('[id^="content-settings-"]').hide();
		$('#content-settings-'+$('#content-type').val()).show();

		// Change settings pane on chaning the content type
		$('#content-type').on('change', function () {
			$('[id^="content-settings-"]').hide();
			$('#content-settings-'+$(this).val()).show();
		});

	});
</script>
