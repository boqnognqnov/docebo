<?php
$fileUploadUrl = Yii::app()->controller->createUrl('//player/block/axUpdateCoursedoc', array('course_id' => $blockModel->course_id, 'block_id' => $blockModel->id));
$newFolderUrl=Yii::app()->controller->createUrl('//player/block/createNewFolder', array('course_id' => $blockModel->course_id, 'block_id' => $blockModel->id));
$newLinkUrl = Yii::app()->controller->createUrl('//player/block/axManageFileLink', array('course_id' => $blockModel->course_id, 'block_id' => $blockModel->id));
?>
<div class="btn-group">
	<a
		href="#"
		class="block-coursedocs-update-file dropdown-toggle"
		rel="update-coursedoc-modal-<?= $blockModel->id ?>"
		data-block-id="<?= $blockModel->id ?>"
		data-toggle="dropdown">

		<span class="p-sprite add-block-item"></span>
	</a>
	<ul class="dropdown-menu pull-right player-lo-actions">
		<?php
		if ($userLevel == LearningCourseuser::$USER_SUBSCR_LEVEL_ADMIN
			|| $userLevel == LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR
			|| $userCanAdminCourse){
		?>
		<li>
			<a href="<?=$newFolderUrl?>" class="p-hover open-dialog">
				<i class="fa fa-folder"></i>
				<span class="menu-label"><?= Yii::t('standard', 'Create folder') ?></span>
			</a>
		</li>
		<?php } ?>
		<li>
			<a href="<?=$fileUploadUrl?>" class="p-hover open-dialog" id="fileUpload">
				<i class="fa fa-cloud-upload"></i>
				<span class="menu-label"><?= Yii::t('test', '_QUEST_UPLOAD') ?></span>
			</a>
		</li>
		<li>
			<a href="<?=$newLinkUrl?>" class="p-hover open-dialog" id="fileUpload">
				<i class="fa fa-link"></i>
				<span class="menu-label"><?= Yii::t('standard', 'Link URL') ?></span>
			</a>
		</li>
		<?php if ( PluginManager::isPluginActive('ContentRavenApp') ) : ?>
			<?php $contentRavenFile = Yii::app()->controller->createUrl('//player/block/axManageContentRavenLink', array('course_id' => $blockModel->course_id, 'block_id' => $blockModel->id)) ?>
			<li>
				<a href="<?=$contentRavenFile?>" class="p-hover open-dialog" id="linkContentRaven">
					<i class="fa fa-link" ></i>
					<span class="menu-label"><?= Yii::t('app', 'Link Content Raven Item') ?></span>
				</a>
			</li>
		<?php endif; ?>
	</ul>
</div>