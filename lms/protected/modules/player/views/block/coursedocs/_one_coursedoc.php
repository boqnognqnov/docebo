<?php /* @var $data LearningCourseFile */ ?>
<?php /* @var $allowAdminOperations bool */ ?>
<?php /* @var $blockModel CActiveRecordBehavior */ ?>
<style>
	td.close-column a{
		text-align: right;
	}
</style>
<tr id="<?= $data->id_file ?>">

	<?php
		$_iconClass = 'is-file';
		$_ext = strtolower(CFileHelper::getExtension($data->path));
		switch($_ext) {
			case 'pdf': 	$_iconClass = 'is-pdf'; break;

			case 'jpg': 	$_iconClass = 'is-image'; break;
			case 'jpeg': 	$_iconClass = 'is-image'; break;
			case 'gif': 	$_iconClass = 'is-image'; break;
			case 'png': 	$_iconClass = 'is-image'; break;

			case 'doc': 	$_iconClass = 'is-word'; break;
			case 'docx': 	$_iconClass = 'is-word'; break;

			case 'xsl': 	$_iconClass = 'is-xsl'; break;
			case 'xml': 	$_iconClass = 'is-xsl'; break;

			case 'zip': 	$_iconClass = 'is-zip'; break;

			default:		$_iconClass = 'is-file'; break;
		}

		// Sinve we are supporting more file sources, let's define what it will be the file source for this file
		$dlUrl = "";
		switch($data->file_source){
			case LearningCourseFile::FILE_SOURCE_LINK: {
				$dlUrl = $data->path;
			} break;
			case LearningCourseFile::FILE_SOURCE_CONTENT_RAVEN: {
				$dlUrl = $data->getContentRavenUrl();
			} break;
			default: {
				$dlUrl = $this->createUrl('//player/block/downloadCoursedoc', array('course_id'=>$blockModel->course_id, 'id_file'=>$data->id_file));
			}
		}

		$class = '';
		if ($data->path == $data->types[$data::FOLDER]) {
			$_iconClass = 'is-folder-closed blue';
			$dlUrl = '#';
			$class = 'folderLink';
		}
	?>

	<td class="close-column">
		<a class="coursedoc-title <?=$class?>" href="<?= $dlUrl ?>" target="_blank" data-parent-id="<?= $data->id_file ?>" data-block-id="<?= $blockModel->id?>">
			<?php
				// Since we are supporting uploading of link and Content raven links we must add additional icons
				if($data->file_source == LearningCourseFile::FILE_SOURCE_LINK || $data->file_source == LearningCourseFile::FILE_SOURCE_CONTENT_RAVEN) { ?>
					<i class="fa fa-link"></i>
			<?php } elseif($data->path == $data->types[$data::FOLDER]) { ?>
					<i class="fa fa-folder folder-item" ></i>
			<?php } else { ?>
				<span class="i-sprite <?= $_iconClass ?> large"></span>
			<?php } ?>
		</a>
	</td>
	<td class="coursedoc-info">
		<a class="coursedoc-title <?=$class?>" href="<?= $dlUrl ?>" target="_blank" data-parent-id="<?= $data->id_file ?>" data-block-id="<?= $blockModel->id?>">
			<?= $data->title ?>
		</a>
		<?php
		if($data->path != $data->types[$data::FOLDER]){
		?>
		<div class="coursedoc-meta"><?= Yii::t('stats', 'Active by {user} on {since}', array('{user}' => $data->getUsername(), '{since}' => $data->created ))?></div>
		<?php } ?>
	</td>
	<td class="text-right close-column">
		<?php
		if ($data->path == $data->types[$data::FOLDER]) { ?>
			<div class="folderArrow">
				<i class="i-sprite small is-arrow-right"></i>
			</div>

		<?php } ?>
		<div class="action">
			<?php
				/*
				 * Allow to the normal user to edin or delete his own files but not to move them
				 */
				$canUserEdinHisOnw = $blockModel->getCanUserUploadFiles(true) && (Yii::app()->user->id == $data->idUser);
				if ( $allowAdminOperations || $canUserEdinHisOnw) : ?>
				<?php
					// NOW we are supporting uploading different file SOURCE
					$editUrl = "";
					switch($data->file_source) {
						case LearningCourseFile::FILE_SOURCE_LINK: {
							$editUrl = Yii::app()->createUrl('//player/block/axManageFileLink', array('course_id'=>$blockModel->course_id, 'file_id' => $data->id_file, 'block_id'=>$blockModel->id));
						} break;

						case LearningCourseFile::FILE_SOURCE_CONTENT_RAVEN: {
							$editUrl = Yii::app()->createUrl('//player/block/axManageContentRavenLink', array('course_id'=>$blockModel->course_id, 'file_id' => $data->id_file, 'block_id'=>$blockModel->id));
						} break;

						default: {
							$editUrl = Yii::app()->createUrl('//player/block/axUpdateCoursedoc', array('course_id'=>$blockModel->course_id, 'file_id' => $data->id_file, 'block_id'=>$blockModel->id));
						}
					}
					$deleteUrl 	= Yii::app()->createUrl('//player/block/axDeleteCoursedoc', array('course_id'=>$blockModel->course_id, 'id_file'=>$data->id_file, 'block_id'=>$blockModel->id));
				?>
					<?php if($allowAdminOperations) : ?>
						<a href="javascript:;"><span class="p-sprite drag controll-icon drag-handle"></span></a>
					<?php endif;?>

					<a href="<?= $editUrl 	?>" class="inline-block open-dialog" rel="update-coursedoc-modal-<?= $blockModel->id ?>"><span class="p-sprite edit-black"></span><span class="menu-label"></span></a>
					<a href="<?= $deleteUrl ?>" class="inline-block open-dialog" data-coursedoc-id="<?= $data->id_file ?>" rel="dialog-delete-coursedoc" data-dialog-class="edit-block-modal">
						<span class="p-sprite cross-small-red"></span><span class="menu-label"></span>
					</a>

			<?php endif; ?>
		</div>
	</td>

</tr>
<script>
	$(document).controls();
</script>