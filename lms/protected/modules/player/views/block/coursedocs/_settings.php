<style>
	span.jq-checkbox{
		background-position: -143px -195px;
	}

	span.jq-checkbox.checked{
		background-position: -189px -195px;
	}
</style>
<h1><?= Yii::t('player', 'Edit Learning Object Properties') ?></h1>
<?php

// NOTE:  <input type='hidden' name='file[]' value='xxx'> will be added to the form silently by File Uploader JS!!
$form = $this->beginWidget('CActiveForm', array(
	'action' => $this->createUrl('//player/block/AxCourseDocsSettings', array('course_id' => $model->course_id ,'block_id' => $model->id)),
	'method' => 'post',
	'htmlOptions' => array(
		'id' => 'upload-x-form',
		'class' => 'ajax form-horizontal',
	),
));
?>
<div id="coursedoc-uploader-container">
	<?= CHtml::errorSummary($model) ?>

	<div class="control-group">
		<?php echo CHtml::label(Yii::t('player', 'Allow normal User to upload files'), 'limit', array(
			'class' => 'control-label'
		)); ?>
		<div class="controls">
			<?php
			echo $form->checkBox($model, 'canNormalUserUploadFiles', array('class' => 'input-block-level', 'id'=>'allowNormalUserToUploadFiles'));
			?>
		</div>

	</div>

	<div class="control-group" id="permissionTypes">
		<?php echo CHtml::label(Yii::t('player', 'File Visibility'), 'PlayerBaseblock[permissionType]', array(
			'class' => 'control-label'
		)); ?>
		<div class="controls">
			<?=  CHtml::radioButtonList('PlayerBaseblock[permissionType]',
					$model->permissionType,
					LearningCourseFile::permissionTypesLabels(),
					array('labelOptions' => array('style' => 'display: inline-block;'))
				 )
			?>
			<div class="control-group" id="permissionGroups">
				<div class="controls">
					<?php
						echo $form->checkboxList($model, 'permissionLevels', LearningCourseFile::permissionLabels(), array(
							'template' => "<div><span>{input} &nbsp; &nbsp;</span><span>{label}</span></div>",
							'container' => '',
							'separator' => '',
							'labelOptions' => array('style' => 'display: inline-block;')
						));
					?>
				</div>
			</div>
		</div>
	</div>

	<div class="control-group">
		<?php echo CHtml::label(Yii::t('player', 'File Limit'), 'limit', array(
			'class' => 'control-label'
		)); ?>
		<div class="controls">
			<?php
			echo $form->textField($model, 'fileLimit', array('class' => 'input-block-level', 'id' => 'uploadFileLimit'));
			?>
		</div>

	</div>

</div>
<!-- /end upload -->

<!-- Dialog2 will move these in Dialog footer  -->
<div class="form-actions">
	<input class="btn-docebo green big upload-save" type="submit" name="confirm_save" value="<?php echo Yii::t('standard', '_SAVE'); ?>">
	<input class="btn-docebo black big close-dialog upload-cancel" type="button" value="<?= Yii::t('standard', '_CANCEL'); ?>">
</div>

<?php $this->endWidget(); ?>

<script>
	$(function(){

		var active = $('#allowNormalUserToUploadFiles');
		if(active.attr('checked') != 'checked')
			$("#uploadFileLimit").attr('readonly', true);

		$('#allowNormalUserToUploadFiles').change(function(){
			if($(this).attr('checked') == 'checked')
				$("#uploadFileLimit").attr('readonly', false);
			else
				$("#uploadFileLimit").attr('readonly', true);
		})
		$('input').styler();
		$('div#social_rating_course_left .jq-radio').remove();


		if ($("input[name*=permissionType]:checked").val() == 1){
			$("#permissionGroups").show();
		} else {
			$("input[name*=permissionLevels]").each(function(){
				if ($(this).is(':checked'))	{
					$(this).trigger('click');
					$(this).prop('checked', false);
				}
			});
		}

		$("input[name*=permissionType]").on("change", function(){
			if ($("input[name*=permissionType]:checked").val() == 1){
				$("#permissionGroups").show();
			}
			else {
				$("input[name*=permissionLevels]").each(function(){
					if ($(this).is(':checked'))	{
						$(this).trigger('click');
						$(this).prop('checked', false);
					}
				});
				$("#permissionGroups").hide();
			}
		})
	})

</script>