<?php /* @var $dataProvider CActiveDataProvider */ ?>
<?php /* @var $allowAdminOperations bool */ ?>
<?php /* @var $blockModel CActiveRecordBehavior */ ?>
<?php /* @var $page integer */ ?>
<style>
	.yiiPager li a{
		padding: 5px;
	}
	.pager li>a{
		background: none !important;
	}
	.selected{
		border: 1px solid #e0e0e0;
		background-color: #ddd;
	}

	.coursedocs-list tbody tr > td {
		position: static !important;

	}
</style>
<div class="player-coursedocs-block" id="ajaxUpdate">

	<?php
	$pageSize = Settings::get('elements_per_page');
	$coursedocs = $dataProvider->getData();
	if (isset($page)) {
		$coursedocsSliced = array_slice($coursedocs, ($page - 1) * $pageSize, $pageSize);
	}
	?>

	<table class="table table-hover coursedocs-list">
		<tbody>
		<tr>
			<?php
			echo $this->renderPartial('player.views.block.coursedocs._parent_folder', array(
				'blockModel' => $blockModel,
				'parent' => $parent,
			), true);
			?>
		</tr>
		<?php
		if (empty($coursedocs)) {
			echo Yii::t('standard', '_NO_CONTENT');
		} else {
			foreach ($coursedocsSliced as $coursedoc) {
				echo $this->renderPartial('player.views.block.coursedocs._one_coursedoc', array(
					'data' => $coursedoc,
					'allowAdminOperations' => $allowAdminOperations,
					'blockModel' => $blockModel,
					'parent' => $parent
				), true);
			}
		}
		?>
		</tbody>
	</table>
	<?php
	$pages = new CPagination(count($coursedocs));
	$pages->pageSize = $pageSize;
	$this->widget('DoceboCLinkPager', array(
		'pages' => $pages,
		'htmlOptions' => array('id' => 'coursedocs-pager')
	));
	?>

</div>

<script type="text/javascript">

	var oCoursedocs = {
		block: null,
		makeSortable: function() {
			// Make the table rows sortable/draggable
			oCoursedocs.block.find('.coursedocs-list tbody')
				.sortable({
					handle: ".drag-handle",
					stop: function() {
						var sorted = $(this).sortable("toArray");
						data = {
							course_id : Blocks.course_id,
							sorted: sorted
						};

						$.ajax({
							url: Blocks.updateCoursedocsOrderUrl,
							type: 'post',
							dataType: 'json',
							data: data
						});
					}
				})
				.disableSelection();
		},
		init: function() {
			oCoursedocs.block = $('#blockid_<?= $blockModel->id ?>');

			oCoursedocs.makeSortable();
		}
	};
	$('.pageLink').off('click');

	$(function(){
		oCoursedocs.init();

		$(function(){
			$('body').off('click', '.folderLink');
			$('body').on('click', '.folderLink', function (e, parentId, blockId) {
				e.preventDefault();
				e.stopPropagation();
				var options = [];
				var block = $(this).data('block-id');
				var parent_id = $(this).data('parent-id');

				var block_id = block;
				if (parentId != undefined && blockId != undefined) {
					parent_id = parentId;
					block_id = blockId;
				}

				var player_block_grid = $($(this).parents().find("#player-blocks-container")).attr('data-parent-id', parent_id);

				$.ajax({
					url: Blocks.loadOneBlockUrl,
					type: 'post',
					dataType: 'json',
					data: {
						options: options,
						course_id: Blocks.course_id,
						parent_id: parent_id,
						block_id: block_id,
					},
					context: $(this),  /// VEERY important
					beforeSend: function () {
						// Insert ajax-loader markup and show
						var blockId = $(this).data('block-id');
						$(this).closest('.player-block-content-inner').html(Blocks.blockAjaxLoader.replace('%ID%', $(this).data('block-id')));
						$('#ajax-loader-' + blockId).show();
					}
				})
					.done(function (res) {
						if (res.success == true) {
							$('#ajax-loader-' + block).closest('.player-block-content-inner').html(res.html);
						}
					});
			});
		}());

		$('body').off('click', '.player-coursedocs-block ul.yiiPager>li>a');
		$('body').on('click', '.player-coursedocs-block ul.yiiPager>li>a', function (e) {
			e.preventDefault();
			e.stopPropagation();
			var blockId = $(this).closest($('li.player-block.player-block-coursedocs')).data('block-id');
			var parent_id = $($(this).parents().find("#player-blocks-container")).attr('data-parent-id');
			var url = '' + this.href;
			$.ajax({
				url: url,
				type: "GET",
				dataType: 'json',
				data: {
					block_id : blockId,
					parent_id: parent_id,
				},
				success: function (data) {
					var result = data.html;
					var parent = $('div.player-coursedocs-block');
					parent.html(result);
				}
			});
		});
	}());

	$(document).on('click', '.player-block-coursedocs ul.yiiPager>li>a', function(e){
		e.preventDefault();
		var blockId = $(this).closest($('li.player-block.player-block-coursedocs')).data('block-id');
		var url = '' + this.href + '&block_id=' + blockId;

		$.ajax({
			url: url,
			type: "GET",
			success: function (data) {
				var result = JSON.parse(data).html;
				var parent = $('div.player-coursedocs-block');
				parent.html(result);
				$(document).controls();
			}
		});
	});

	$(document).on('block-refreshed', '#blockid_' + '<?=Yii::app()->request->getParam("block_id")?>', function () {
		if (arguments[1] && arguments[1].parentId)
			var parentId = arguments[1].parentId;

		if (parentId != undefined) {
			var blockId = '<?=Yii::app()->request->getParam("block_id")?>';
			$('.folderLink').trigger('click', [parentId, blockId]);
		}
	});

	$(function(){
		$(document).off('click', '.folderLink').on('click', '.folderLink', function (e, parentId, blockId) {
			e.preventDefault();
			e.stopPropagation();
			var options = [];
			var block = $(this).data('block-id');
			var parent_id = $(this).data('parent-id');

			var block_id = block;
			if (parentId != undefined && blockId != undefined) {
				parent_id = parentId;
				block_id = blockId;
			}

			var player_block_grid = $($(this).parents().find("#player-blocks-container")).attr('data-parent-id', parent_id);

			$.ajax({
				url: Blocks.loadOneBlockUrl,
				type: 'post',
				dataType: 'json',
				data: {
					options: options,
					course_id: Blocks.course_id,
					parent_id: parent_id,
					block_id: block_id,
				},
				context: $(this),  /// VEERY important
				beforeSend: function () {
					// Insert ajax-loader markup and show
					var blockId = $(this).data('block-id');
					$(this).closest('.player-block-content-inner').html(Blocks.blockAjaxLoader.replace('%ID%', $(this).data('block-id')));
					$('#ajax-loader-' + blockId).show();
				}
			})
				.done(function (res) {
					if (res.success == true) {
						$('#ajax-loader-' + block).closest('.player-block-content-inner').html(res.html);
					}
				});
		});

		$(document).off('click', '.player-coursedocs-block ul.yiiPager>li>a').on('click', '.player-coursedocs-block ul.yiiPager>li>a', function (e) {
			e.preventDefault();
			e.stopPropagation();
			var blockId = $(this).closest($('li.player-block.player-block-coursedocs')).data('block-id');
			var parent_id = $($(this).parents().find("#player-blocks-container")).attr('data-parent-id');
			var url = '' + this.href;
			$.ajax({
				url: url,
				type: "GET",
				dataType: 'json',
				data: {
					block_id : blockId,
					parent_id: parent_id,
				},
				success: function (data) {
					var result = data.html;
					var parent = $('div.player-coursedocs-block');
					parent.html(result);
				}
			});
		});
	});
</script>