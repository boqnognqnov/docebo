<style>
    .modal-body {
        overflow: visible;
    }

    div#coursedoc-uploader-container > div.row-fluid {
        margin-bottom: 10px;
    }

    div#coursedoc-uploader-container select {
        border-radius: 0px;
    }
</style>
<h1><?= Yii::t('standard', 'Link URL') ?></h1>
<?php

// NOTE:  <input type='hidden' name='file[]' value='xxx'> will be added to the form silently by File Uploader JS!!
$form = $this->beginWidget('CActiveForm', array(
    'action' => $this->createUrl('//player/block/axManageFileLink', Array('course_id' => $fileModel->id_course, 'file_id' => $fileModel->id_file)),
    'method' => 'post',
    'htmlOptions' => array(
        'id' => 'upload-x-form',
        'class' => 'ajax form-horizontal',
    ),
));
?>
<div id="coursedoc-uploader-container">
    <?= CHtml::errorSummary($fileModel) ?>

        <div class="row-fluid">
            <?= CHtml::label(Yii::t('standard', '_TITLE'), 'file-title'); ?>
            <?= $form->textField($fileModel, 'title', array('class' => 'input-block-level')); ?>
        </div>

        <div class="row-fluid">
            <?= CHtml::label(Yii::t('standard', '_URL'), 'file_link'); ?>
            <?= $form->textField($fileModel, 'file_link', array('class' => 'input-block-level')); ?>
        </div>

        <div class="row-fluid">
            <?= CHtml::label(Yii::t('standard', 'Select folder'), 'select-folder'); ?>
            <?= CHtml::dropDownList('parentFolderId', '', $listOfFolders, array('class' => 'input-block-level', 'id' => 'select-folder')) ?>
        </div>

</div>
<!-- /end upload -->

<!-- Dialog2 will move these in Dialog footer  -->
<div class="form-actions">
    <input class="btn-docebo green big upload-save" type="submit" name="confirm_save" value="<?php echo Yii::t('standard', '_SAVE'); ?>">
    <input class="btn-docebo black big close-dialog upload-cancel" type="button" value="<?= Yii::t('standard', '_CANCEL'); ?>">
</div>

<?php echo CHtml::hiddenField('block_id', $blockId); ?>
<?php echo $form->hiddenField($fileModel, 'id_course'); ?>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    <!--
    var currentFolderId = $('#currentFolder').data('current-id');
    $('#select-folder option[value="' + currentFolderId + '"]').prop('selected', true);
    //-->
</script>