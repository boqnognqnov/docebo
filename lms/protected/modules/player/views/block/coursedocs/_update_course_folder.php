<?php
/**
 * Created by PhpStorm.
 * User: Asen Nikolov
 * Date: 14-Aug-15
 * Time: 6:02 PM
 * @var $fileModel LearningCourseFile
 */ ?>

<style>

	.modal-body {
		overflow: visible;
	}
	form label.control-label{
		width: 20% !important;
	}
	form div.controls{
		margin-left: 20% !important;
	}
	.controls select{
		width: 100% !important;
	}
	.controls input{
		width: 96.5% !important;
	}

</style>
<h1><?= Yii::t('standard', '_MOD') ?></h1>
<?= CHtml::beginForm($this->createUrl('//player/block/axUpdateCoursedoc', Array('course_id' => $fileModel->id_course, 'file_id' => $fileModel->id_file)), 'post', array('class' => 'ajax form-horizontal')) ?>
<input type="hidden" id="folderId" value="<?= $fileModel->id_file ?>"/>
<input type="hidden" id="parent-folder-Id" value="<?= $fileModel->idParent ?>"/>
<?php echo CHtml::hiddenField('block_id', $blockId); ?>
<?= CHtml::errorSummary($fileModel) ?>
<div class="control-group">
	<?= CHtml::label(Yii::t('standard', '_TITLE'), 'folder-title', array(
		'class' => 'control-label'
	)); ?>
	<div class="controls">
		<input type="text" value="<?= $fileModel->title ?>" name="folderTitle" id="folder-title">
	</div>
</div>
<div class="control-group">
	<?= CHtml::label(Yii::t('standard', 'Select folder'), 'parentFolderId', array(
		'class' => 'control-label'
	)); ?>
	<div class="controls">
		<?= CHtml::dropDownList('parentFolderId', '', $listOfFolders) ?>
	</div>
</div>

<!-- Dialog2 will move these in Dialog footer  -->
<div class="form-actions">
	<input class="btn-docebo green big upload-save" type="submit" name="confirmSaveFolder"
		   value="<?php echo Yii::t('standard', '_SAVE'); ?>">
	<input class="btn-docebo black big close-dialog upload-cancel" type="button"
		   value="<?= Yii::t('standard', '_CANCEL'); ?>">
</div>

<?= CHtml::endForm() ?>

<script>
	var folderTitle = $('#folderId').val();
	var parentFolderId = $('#parent-folder-Id').val();
	$('#parentFolderId option[value="' + folderTitle + '"]').remove();
	$('#parentFolderId option[value="' + parentFolderId + '"]').prop('selected', true);;
</script>