<style>
.modal-body {
	overflow: visible;
	max-height:none;
}
</style>

<div id="coursedoc-uploader-container">
	<?= CHtml::errorSummary($fileModel) ?>
	<div class="control-group">
		<?php echo CHtml::label(Yii::t('authoring', 'Select files'), 'pickfiles', array(
			'class' => 'control-label'
		)); ?>
		<div class="controls">

			<div id="x-pl-uploader-dropzone">
				<div id="x-pl-uploader">

					<div class="upload-button">
						<a class="btn-docebo green big" id="pickfiles3" href="javascript:;">
							<?php echo Yii::t('organization', 'Upload File'); ?>
						</a>
						<?php if ($fileModel->id_file) :?>
							&nbsp;&nbsp;<span class="uploaded-fileinfo previous-uploaded-file">
								<?php
									$currentFileName = empty($fileModel->original_filename) ?  $fileModel->path :  $fileModel->original_filename;
								?>
								<?php echo Yii::t('standard', '_CURRENT_FILE') . ': <span class="uploaded-filename">'. $currentFileName . '</span>'; ?>
							</span>
						<?php endif; ?>
					</div>
					<div class="upload-progress">
						<div class="uploaded-fileinfo">
							<?php echo Yii::t('authoring', 'Uploading') . ' : <span class="uploaded-filename" id="uploaded-filename"></span><span id="uploaded-filesize"></span>'; ?>
						</div>
						<div class="row-fluid">
							<div class="span10">
								<div class="docebo-progress progress progress-striped active">
									<div id="uploader-progress-bar" class="bar full-height" style="width: <?= ($fileModel->id_file ? '100' : '0') ?>%;"></div>
								</div>
							</div>
							<div class="span2 uploaded-percent">
								<span id="uploaded-percent-gauge"><?= ($fileModel->id_file ? '100%' : '') ?></span>
								<a href="#" id="uploaded-cancel-upload"><i class="icon-remove"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="control-group">
		<?php echo CHtml::label(Yii::t('standard', '_TITLE'), 'file-title', array(
			'class' => 'control-label'
		)); ?>
		<div class="controls">
			<?php
				echo $form->textField($fileModel, 'title', array('class' => 'input-block-level'));
			?>
		</div>
	</div>

	<div class="control-group">
		<?php echo CHtml::label(Yii::t('standard', 'Select folder'), 'select-folder', array(
			'class' => 'control-label'
		)); ?>
		<div class="controls">
			<?= CHtml::dropDownList('parentFolderId', '', $listOfFolders, array('class' => 'input-block-level', 'id' => 'select-folder')) ?>
		</div>
	</div>

	<div class="control-group" id="permissionTypes">
		<?php echo CHtml::label(Yii::t('player', 'File Visibility'), 'LearningCourseFile[permissionType]', array(
			'class' => 'control-label'
		)); ?>
		<div class="controls">
			<?=  CHtml::radioButtonList('LearningCourseFile[permissionType]',
					$fileModel->permissionType,
					LearningCourseFile::permissionTypesLabels(),
					array('labelOptions' => array('style' => 'display: inline-block;'))
				)
			?>
			<div class="control-group" id="permissionGroups">
				<div class="controls">
					<?php
						echo $form->checkboxList($fileModel, 'permissionLevels', LearningCourseFile::permissionLabels(), array(
							'template' 	=> "<div><span>{input} &nbsp; &nbsp;</span><span>{label}</span></div>",
							'container' => '',
							'separator' => '',
							'labelOptions' => array('style' => 'display: inline;')
						));
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /end upload -->

<!-- Dialog2 will move these in Dialog footer  -->
<div class="form-actions">
	<input class="btn-docebo green big upload-save" type="submit" name="confirm_save" value="<?php echo Yii::t('standard', '_SAVE'); ?>">
	<input class="btn-docebo black big close-dialog upload-cancel" type="button" value="<?= Yii::t('standard', '_CANCEL'); ?>">
</div>

<?php echo CHtml::hiddenField('block_id', $blockId); ?>
<?php echo $form->hiddenField($fileModel, 'id_course'); ?>

<script type="text/javascript">
<!--

$(function () {

	// Create new Uploader object
	CoursedocUploader = new FileUploaderClass();

	// PL Uploader options
	CoursedocUploader.init({
		unique_names: true,
		browse_button: 'pickfiles3',
		container: 'coursedoc-uploader-container',
		drop_element: 'x-pl-uploader-dropzone',
		max_file_size: '400mb',  // ???????????? (@Fabio??)
		url: '<?= Docebo::createAbsoluteLmsUrl('player/training/axUpload') ?>',
		multipart: true,
		multipart_params: {
			course_id: '<?= (int) $fileModel->id_course ?>',
			YII_CSRF_TOKEN: '<?= Yii::app()->request->csrfToken ?>',
			isCourseDocUpload: true
		},
		flash_swf_url: '<?=Yii::app()->plupload->getAssetsUrl()?>/plupload.flash.swf',

		// -- custom settings; not PL Uploader specific; accessible inside uploaded code as pl_upload.settings.xyz
		form_selector: '#upload-x-form',
		filters: [{title: "Course documents", extensions: '<?= implode(',', FileTypeValidator::getWhiteListArray(FileTypeValidator::TYPE_ITEM)) ?>'}]


	});

	// Run uploader
	CoursedocUploader.run();

//	$('input').styler();
	if ($("input[name*=permissionType]:checked").val() == 1){
		$("#permissionGroups").show();
	} else {
		$("input[name*=permissionLevels]").each(function(){
			if ($(this).is(':checked'))	{
				$(this).trigger('click');
				$(this).prop('checked', false);
			}
		});
	}

	$("input[name*=permissionType]").on("change", function(){
		if ($("input[name*=permissionType]:checked").val() == 1){
			$("#permissionGroups").show();
		}
		else {
			$("input[name*=permissionLevels]").each(function(){
				if ($(this).is(':checked'))	{
					$(this).trigger('click');
					$(this).prop('checked', false);
				}
			});
			$("#permissionGroups").hide();
		}
	})

});
	var currentFolderId = $('#currentFolder').data('current-id');
	$('#select-folder option[value="' + currentFolderId + '"]').prop('selected', true);

//-->
</script>