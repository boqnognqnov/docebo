<?php /* @var $coursedoc LearningCourseFile */ ?>
<?php /* @var $isDeleteComplete bool */ ?>
<?php /* @var $blockId int */ ?>
<style>
	#deleteForm label {
		display: inline-block;
		word-break: break-all;
	}
	#deleteForm select{
		width: 100%;
	}
	#deleteForm .span1{
		text-align: center;
		user-select: none;
	}
</style>
<?php
if ($coursedoc->item_type == $coursedoc::FOLDER) { ?>
	<h1><?= Yii::t('standard', 'Delete folder') ?></h1>
<?php } else { ?>
	<h1><?= Yii::t('standard', '_DEL') ?></h1>
<?php } ?>
<?php if ($isDeleteComplete === true) : ?>
	<a class="auto-close success"></a>
<?php else: ?>

	<?php
		$form = $this->beginWidget('CActiveForm', array(
			'method' => 'post',
			'htmlOptions' => array(
				'class' => 'ajax form-horizontal',
				'id' => 'deleteForm'
			)
		));
	$class = 'disabled';
	if($coursedoc->item_type == $coursedoc::FOLDER) { ?>
		<div class="row-fluid">
			<div class="span1">
				<input type="radio" name="totalDelete" value="1" id="totalDelete" checked/>
			</div>
			<div class="span11"><?php
				echo '<label for="totalDelete">' . Yii::t('standard', '_DEL') . ' "' . $coursedoc->title . '" <strong>' . Yii::t('course', 'with all it\'s content') . '</strong></label>'; ?>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1">
				<input type="radio" name="totalDelete" value="2" id="partialDelete"/>
			</div>
			<div class="span11">
				<?php
				echo '<label for="partialDelete">' . Yii::t('standard', '_DEL') . ' "' . $coursedoc->title . '" <strong>' . Yii::t('course', 'and move it\'s content to another folder') . '</strong></label>'; ?>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>
			<div class="span11">
				<?= CHtml::dropDownList('newTargetFolder', '', $listOfFolders, array('id' => 'newTargetFolder','disabled' => true)) ?>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1">
				<input type="checkbox" name="confirm" id="confirmDelete" value="1"/>
			</div>
			<div class="span11">
				<label for="confirmDelete">
					<?= Yii::t('standard', 'Yes, I confirm I want to proceed') ?>
				</label>
			</div>
		</div>
	<?php } else {
		$class = '';
		echo '<div>' . Yii::t('standard', '_DEL') . ' <strong>"' . $coursedoc->title . '"</strong></div>';
	}
	?>

	<div class="form-actions">
		<input class="btn-docebo green big submitDelete <?=$class?>" type="submit" name="confirm_delete" value="<?= Yii::t('standard', '_CONFIRM') ?>"/>
		<input class="btn-docebo black big close-dialog" type="reset" value="<?= Yii::t('standard', '_CLOSE') ?>"/>
	</div>

	<?php $this->endWidget(); ?>
<?php endif; ?>

<script>
	// Remove previously .on bind, if this view is loaded more than one times
	$('#confirmDelete').off('change');
	$('input[type="radio"]').off('change');
	$('.submitDelete').off('click');

	$('#deleteForm input, select').styler();

	$('input[type="radio"]').change(function () {
		$('#newTargetFolder').prop('disabled', !$('#partialDelete').is(':checked'));
	});
	$('#confirmDelete').on('change', function () {
		if ($(this).is(':checked')) {
			$('.submitDelete').removeClass('disabled');
		} else {
			$('.submitDelete').addClass('disabled');
		}
	});
	$(document).on('click', 'input.submitDelete', function (e) {
		if ($(this).hasClass('disabled')) {
			e.stopPropagation();
			e.preventDefault();
		}
	});
	$('#newTargetFolder option[value="<?=$coursedoc->id_file?>"]').remove();

</script>