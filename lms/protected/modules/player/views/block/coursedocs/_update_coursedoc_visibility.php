<? /* @var $fileModel LearningCourseFile */ ?>
<? /* @var $form CActiveForm */ ?>
<? /* @var $courseModel LearningCourse */ ?>
<? if(!$fileModel->viewable_by) {
	$fileModel->viewable_by = LearningCourseFile::VIEWABLE_IN_ANY_SESSION;
}

$courseUserModel = LearningCourseuser::model()->findByAttributes(array(
	'idCourse' => $idCourse,
	'idUser' => Yii::app()->user->getIdst(),
));

$isInstructorOfCourse = !Yii::app()->user->getIsGodadmin() && !Yii::app()->user->getIsPu() && $courseUserModel->level == LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR;
if($isInstructorOfCourse) {
	$fileModel->viewable_by = LearningCourseFile::VIEWABLE_IN_SELECTED_SESSIONS_ONLY;
}

$canSetToAll = false;
if(Yii::app()->user->getIsGodadmin()){
	$canSetToAll = true;
} elseif(Yii::app()->user->getIsPu() && CoreUserPuCourse::isCourseAssignedToPU(Yii::app()->user->getIdst(), $idCourse)) {
	$canSetToAll = true;
} elseif($courseUserModel->level && $courseUserModel->level != LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT){
	$canSetToAll = true;
}

?>
	<div class="grey-wrapper ilt-session-file-download-visibility">
		<? if($canSetToAll) { ?>
			<span>
			<label style="display:inline">
				<?= $form->radioButton($fileModel, 'viewable_by', array(
					'value' => LearningCourseFile::VIEWABLE_IN_ANY_SESSION,
					'uncheckValue' => null,
					'checked' => ($fileModel->viewable_by == LearningCourseFile::VIEWABLE_IN_ANY_SESSION ? 'checked' : '')
				)); ?>
				<?= Yii::t('course_file', 'All sessions') ?>
			</label>
			</span>
		<? } ?>

		<span>
			<label style="display:inline">
				<?= $form->radioButton($fileModel, 'viewable_by', array(
					'value' => LearningCourseFile::VIEWABLE_IN_SELECTED_SESSIONS_ONLY,
					'uncheckValue' => null,
					'checked' => (($fileModel->viewable_by == LearningCourseFile::VIEWABLE_IN_SELECTED_SESSIONS_ONLY || $isInstructorOfCourse || $courseUserModel->level == LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR) ? 'checked' : '')
				)); ?>
				<?= Yii::t('course_file', 'Select sessions') ?>
			</label>
		</span>
		<br/>
	</div>

<?
$preselectedSessionIds = isset($selectedSessionIds) ? $selectedSessionIds : array();
if(!$fileModel->getIsNewRecord() && empty($preselectedSessionIds)) {
	// Retrieve existing sessions for which this file was given visibility for
	switch ($courseModel->course_type) {
		case LearningCourse::TYPE_CLASSROOM:
			$preselectedSessionIds = Yii::app()->getDb()->createCommand()
				->selectDistinct('s.id_session')
				->from('lt_course_session s')
				->join('learning_course_file_visibility lfv', 'lfv.id_session=s.id_session AND lfv.id_file=:idFile AND lfv.session_type=:course_type', array(
					':idFile' => $fileModel->id_file,
					':course_type' => $courseModel->course_type,
				))
				->where('s.course_id=:idCourse', array(':idCourse' => $idCourse))
				->queryColumn();
			break;
		case LearningCourse::TYPE_WEBINAR:
			$preselectedSessionIds = Yii::app()->getDb()->createCommand()
				->selectDistinct('s.id_session')
				->from('webinar_session s')
				->join('learning_course_file_visibility lfv', 'lfv.id_session=s.id_session AND lfv.id_file=:idFile AND lfv.session_type=:course_type', array(
					':idFile' => $fileModel->id_file,
					':course_type' => $courseModel->course_type,
				))
				->where('s.course_id=:idCourse', array(':idCourse' => $idCourse))
				->queryColumn();
			break;
	}
}

if($idSession){
	$preselectedSessionIds[] = $idSession;
}

$dataProvider = false;
switch ($courseModel->course_type) {
	case LearningCourse::TYPE_CLASSROOM:
		$dataProvider = LtCourseSession::model()->dataProvider($idCourse);
		break;
	case LearningCourse::TYPE_WEBINAR:
		$dataProvider = WebinarSession::model()->dataProvider($idCourse);
		break;
}

$columns = array(
	array(
		'header' => '', //'<input type="checkbox" class="select-on-check-all" />',
		'type' => 'raw',
		'value' => function($data) use ($preselectedSessionIds) {
			$isChecked = in_array($data->id_session, $preselectedSessionIds);
			return CHtml::checkBox("selectBox", $isChecked, array("value" => $data->id_session, "class" => "select-on-check selectItem", 'id' => intval(microtime(1))));
		},
	),
	array(
		'header' => Yii::t('standard', '_NAME'),
		'value' => '$data->name',
		'type' => 'raw',
	),
	array(
		'type' => 'raw',
		'value' => function($data) {
			$dateBegin = Yii::app()->localtime->toUTC($data->date_begin);
			$dateEnd = Yii::app()->localtime->toUTC($data->date_end);
			return Yii::app()->localtime->toLocalDate($dateBegin) . '&nbsp;&nbsp; | &nbsp;&nbsp;' . Yii::app()->localtime->toLocalDate($dateEnd);
		},
		'header' => Yii::t('standard', '_DATE')
	),
);

// Webinars don't have location column
if($courseModel->course_type == LearningCourse::TYPE_CLASSROOM) {
	$columns[] = array(
		'type' => 'raw',
		'value' => '$data->renderLocations();',
		'header' => Yii::t('classroom', 'Location')
	);
}

$this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'course-file-visibility-in-session',
		'htmlOptions' => array('class' => 'grid-view clearfix'),
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
		'dataProvider' => $dataProvider,
		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'pager' => array(
				'class' => 'DoceboCLinkPager',
				'maxButtonCount' => 8,
		),
		'cssFile' => false,
		'columns' => $columns,
)); ?>

<script>
	function updateSelection(){
		var selection = $('#selectedItemsString').val().split(',');

		if (selection.length > 0) {
			$.each($('.modal-body input.select-on-check.selectItem'), function(i, e){
				e = $(e);
				if(selection.indexOf(e.val()) != -1){
					e.next('span.jq-checkbox').addClass('checked');
				}else{
					e.next('span.jq-checkbox').removeClass('checked');
				}
			});
		}
	}
	$(document).ajaxComplete(function () {
		if ($('.modal .modal-body.opened').length > 0) {
			$('.modal .modal-body.opened input').styler();
			updateSelection();
		}
	});
</script>
