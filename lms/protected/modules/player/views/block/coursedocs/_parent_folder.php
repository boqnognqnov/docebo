<?php /* @var $data LearningCourseFile */ ?>
<?php /* @var $allowAdminOperations bool */ ?>
<?php /* @var $blockModel CActiveRecordBehavior */ ?>
<style>
	.coursedoc-info{
		word-break: break-all;
		padding-top: 12px;
		padding-left: 8px;
	}

	a.folderLink {
		vertical-align: super;
	}
</style>
<?php
	if($parent->path == 'root'){
//		echo '<hr id="currentFolder" data-current-id="'.$parent->id_file.'">'.Yii::t('course', 'All files and folders');
	}else{

		$_iconClass = 'is-folder-opened blue';
		$parentUrl = '';
		$class = 'folderLink';
		?>
		<td class="close-column">
			<span class="coursedoc-title">
				<a href="#" class="<?=$class?>" data-parent-id="<?= $parent->idParent ?>" data-block-id="<?= $blockModel->id?>">
					<span class="i-sprite small is-arrow-left"></span>
				</a>
				<i class="fa fa-folder-open folder-item" ></i>
			</span>
		</td>
		<td class="coursedoc-info">
			<span class="coursedoc-title" id="currentFolder" data-current-id="<?=$parent->id_file?>" data-parent-id="<?= $data->id_file ?>" data-block-id="<?= $blockModel->id?>">
				<?= $parent->title ?>
			</span>
<!--			<div class="coursedoc-meta">--><?//= Yii::t('stats', 'Active by {user} on {since}', array('{user}' => $data->getUsername(), '{since}' => $data->created ))?><!--</div>-->
				</td>
		<td class="text-right close-column">
				<i class="i-sprite is-check"></i>
			<div class="action">
				<?php
				/*
				 * Allow to the normal user to edin or delete his own files but not to move them
				 */
				$canUserEdinHisOnw = $blockModel->getCanUserUploadFiles(true) && (Yii::app()->user->id == $data->idUser);
				if ( $allowAdminOperations || $canUserEdinHisOnw) : ?>
					<?php
					$editUrl 	= Yii::app()->createUrl('//player/block/axUpdateCoursedoc', array('course_id'=>$blockModel->course_id, 'file_id' => $data->id_file, 'block_id'=>$blockModel->id));
					$deleteUrl 	= Yii::app()->createUrl('//player/block/axDeleteCoursedoc', array('course_id'=>$blockModel->course_id, 'id_file'=>$data->id_file, 'block_id'=>$blockModel->id));
					?>
					<?php if($allowAdminOperations) : ?>
						<a href="javascript:;"><span class="p-sprite drag controll-icon drag-handle"></span></a>
					<?php endif;?>

					<a href="<?= $editUrl 	?>" class="inline-block open-dialog" rel="update-coursedoc-modal-<?= $blockModel->id ?>"><span class="p-sprite edit-black"></span><span class="menu-label"></span></a>
					<a href="<?= $deleteUrl ?>" class="inline-block open-dialog" data-coursedoc-id="<?= $data->id_file ?>" rel="dialog-delete-coursedoc" data-dialog-class="edit-block-modal">
						<span class="p-sprite cross-small-red"></span><span class="menu-label"></span>
					</a>

				<?php endif; ?>
			</div>
		</td>
	<?php } ?>
