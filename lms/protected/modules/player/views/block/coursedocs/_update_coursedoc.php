<?
/* @var $this BlockController */
/* @var $courseModel LearningCourse */
?>

<h1><?= Yii::t('course', '_DOCUMENT_UPLOAD') ?></h1>

<?php

// NOTE:  <input type='hidden' name='file[]' value='xxx'> will be added to the form silently by File Uploader JS!!
$form = $this->beginWidget('CActiveForm', array(
	'action' => $this->createUrl('//player/block/axUpdateCoursedoc', Array(
		'course_id' => $fileModel->id_course,
		'file_id' => $fileModel->id_file
	)),
	'method' => 'post',
	'htmlOptions' => array(
		'id' => 'upload-x-form',
		'class' => 'ajax file-upload-session-form',
	),
));
?>
<input type="hidden" name="selectedItemsString" id="selectedItemsString" value="<?=implode(',', $selectedSessionIds);?>" />
<?= CHtml::errorSummary($fileModel) ?>
<?php
$tabs = array(
		1 => array(
				'label' => Yii::t('standard', 'File upload'),
				'content' => $this->renderPartial("coursedocs/_update_coursedoc_upload", array(
						'fileModel' => $fileModel,
						'blockId' => $blockId,
						'listOfFolders' => $listOfFolders,
						'form' => $form,
				), TRUE, TRUE),
		),

);
if ($courseModel->course_type == LearningCourse::TYPE_CLASSROOM || $courseModel->course_type == LearningCourse::TYPE_WEBINAR) {
	$tabs[2] = array(
			'label' => Yii::t('standard', 'File visibility'),
			'content' => $this->renderPartial('coursedocs/_update_coursedoc_visibility', array(
					'fileModel' => $fileModel,
					'blockId' => $blockId,
					'listOfFolders' => $listOfFolders,
					'form' => $form,
					'idCourse' => $idCourse,
					'idSession' => $idSession,
					'courseModel' => $courseModel,
					'selectedSessionIds' => $selectedSessionIds,
			), true, TRUE),
	);

	$this->widget('common.extensions.tabbed.TabbedWidget', array(
			'active_tab' => $fileModel->hasErrors('viewable_by') ? 1 : 0,
			'tabs' => $tabs
	));
} else {
	echo $tabs[1]['content'];
}
?>

<div class="clearfix"></div>

<? $this->endWidget(); ?>
<script>

	$(function () {
		$('.modal input').styler();

		$(document).off('change', '.modal-body input.select-on-check.selectItem').on('change', '.modal-body input.select-on-check.selectItem', function () {
			var that = $(this);
			var currentValue = that.val();
			var currentSelectedList = $('#selectedItemsString').val().split(',');
			if (that.is(':checked')) {
				$.each(currentSelectedList, function (i, e) {
					if (currentSelectedList.indexOf(currentValue) == -1) {
						currentSelectedList.push(currentValue);
					}
				})
			} else {
				$.each(currentSelectedList, function (i, e) {
					if (currentSelectedList.indexOf(currentValue) != -1) {
						currentSelectedList.splice(currentSelectedList.indexOf(currentValue), 1);
					}
				})
			}
			$('#selectedItemsString').val(currentSelectedList.join(','));
		});

		var showOrHideGrid = function () {
			var val = $('input[name="LearningCourseFile[viewable_by]"]:checked').val();

			switch (val) {
				case '<?=LearningCourseFile::VIEWABLE_IN_ANY_SESSION?>':
					$('#course-file-visibility-in-session').hide();
					break;
				case '<?=LearningCourseFile::VIEWABLE_IN_SELECTED_SESSIONS_ONLY?>':
				default:
					$('#course-file-visibility-in-session').show();
					break;
			}
		};

		// Create new Uploader object
		CoursedocUploader = new FileUploaderClass();

		// PL Uploader options
		CoursedocUploader.init({
			unique_names: true,
			browse_button: 'pickfiles',
			container: 'coursedoc-uploader-container',
			drop_element: 'x-pl-uploader-dropzone',
			max_file_size: '400mb',  // ???????????? (@Fabio??)
			url: '<?= Docebo::createAbsoluteLmsUrl('player/training/axUpload') ?>',
			multipart: true,
			multipart_params: {
				course_id: '<?= (int) $fileModel->id_course ?>',
				YII_CSRF_TOKEN: '<?= Yii::app()->request->csrfToken ?>',
				isCourseDocUpload: true
			},
			flash_swf_url: '<?=Yii::app()->plupload->getAssetsUrl()?>/plupload.flash.swf',

			// -- custom settings; not PL Uploader specific; accessible inside uploaded code as pl_upload.settings.xyz
			form_selector: '#upload-x-form',
			filters: [{
				title: Yii.t("player", "File repository area"),
				extensions: '<?= implode(',', FileTypeValidator::getWhiteListArray(FileTypeValidator::TYPE_ITEM)) ?>'
			}]
		});

		$('input[name="LearningCourseFile[viewable_by]"]').change(function () {
			showOrHideGrid();
		});

		showOrHideGrid();
	});
</script>
