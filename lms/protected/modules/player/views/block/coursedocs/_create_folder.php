<style>
	<!--

	.modal-body {
		overflow: visible;
	}

	.pull-right.controls input, .pull-right.controls select {
		margin-left: 1%;
		width: 80%;
	}

	label {
		text-align: right;
	}

	.pull-right {
		width: 95%;
		margin-top: 2%;
	}

	-->
</style>
<?php
if ($update === true) { ?>
	<a class="auto-close success"></a>
<?php }else{
$newFolderUrl = Yii::app()->controller->createUrl('//player/block/createNewFolder');
?>


<h1><?= Yii::t('standard', 'Create folder') ?></h1>
<?= CHtml::beginForm($newFolderUrl, 'POST', array('class' => 'ajax form-horizontal', 'id' => 'createFolderForm')) ?>
	<?= CHtml::errorSummary($folder) ?>
	<input type="hidden" value="<?=$blockId?>" name="block_id">
<div class="pull-right controls">
	<label>
		<?= Yii::t('storage', '_ORGFOLDERNAME') ?>
		<input type="text" name="folderName" class="input-block-level"/>
	</label>
</div>
<div class="clearfix"></div>
<div class="pull-right controls">
	<label><?= Yii::t('player', 'Parent folder'); ?><?= CHtml::dropDownList('parentFolderId', '', $listOfFolders, array('class' => 'input-block-level')) ?></label>
</div>
<div class="clearfix"></div>


<!-- Dialog2 will move these in Dialog footer  -->
<div class="form-actions">
	<input class="btn-docebo green big upload-save" type="submit" name="confirm_save"
		   value="<?php echo Yii::t('standard', '_SAVE'); ?>">
	<input class="btn-docebo black big close-dialog upload-cancel" type="button"
		   value="<?= Yii::t('standard', '_CANCEL'); ?>">
</div>
<?= CHtml::endForm() ?>
<?php } ?>

<script>
		$('#createFolderForm input,select').styler();
		var currentFolderId = $('#currentFolder').data('current-id');
		$('#parentFolderId option[value="' + currentFolderId + '"]').prop('selected', true);

</script>