<?php
	$form = $this->beginWidget('CActiveForm', array(
			'action' => $this->createUrl('/' . $lo_type . '/default/axSaveLo'),
			'method' => 'post',
			'htmlOptions' => array(
				'id' => 'upload-lo-form',
 				'class' => 'form-horizontal',
			),
	));

	$uploadingMessage = Yii::t('standard', 'This operation can take up to 2 minutes depending on the size of your file.');
	$uploadingImageFileName = 'cloud_upload.jpg';
	if ($lo_type == LearningOrganization::OBJECT_TYPE_VIDEO) {
		$dummyStorage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_VIDEO);
		if ($dummyStorage->transcodeVideo) {
			$uploadingMessage 	= Yii::t('standard', 'Your video is being converted into web and mobile formats. You will see an icon until the conversion process is complete.');
			$uploadingImageFileName = 'converting_video.jpg';
		}
	}


?>
<div id="player-arena-uploader-panel" class="player-arena-editor-panel">
	<div class="header">
		<?php echo Yii::t('organization', 'Upload File'); ?>
	</div>

	<div id="upload-stuff-message" class="" style="display: none ;">
		<div class="uploading-message">
			<div>
				<h4><?= Yii::t('standard', 'Uploading your stuffs to the clouds') ?></h4>
				<p>
					<?= $uploadingMessage ?>
				</p>
			</div>
		</div>
		<div class="uploading-image">
			<?php echo CHtml::image(Yii::app()->theme->baseUrl . "/images/" . $uploadingImageFileName); ?>
		</div>
	</div>

	<div class="content" id="player-arena-uploader">
		<div>
			<div id="player-uploader-wrapper" class="player-arena-uploader">

				<div class="control-group">
					<?php echo CHtml::label(Yii::t('authoring', 'Select files'), 'pickfiles', array(
						'class' => 'control-label'
					)); ?>
					<div class="controls">

						<div id="player-pl-uploader-dropzone">
							<div id="player-pl-uploader">

								<div class="upload-button">
									<a class="btn-docebo green big" id="pickfiles" href="javascript:;">
										<?php echo Yii::t('organization', 'Upload File'); ?>
									</a>
									<?php if ($resource) :?>
										&nbsp;&nbsp;<span class="uploaded-fileinfo previous-uploaded-file">
											<?php echo Yii::t('standard', '_CURRENT_FILE') . ': <span class="uploaded-filename">'
												. Docebo::cleanFilename($resource['filename']). '</span>'; ?>
										</span>
									<?php endif; ?>
								</div>
								<div class="upload-progress">
									<div class="uploaded-fileinfo">
										<?php echo Yii::t('standard', 'Uploading') . ': <span class="uploaded-filename" id="uploaded-filename"></span><span id="uploaded-filesize"></span>'; ?>
									</div>
									<div class="row-fluid">
										<div class="span11">
											<div class="docebo-progress progress progress-striped active">
												<div id="uploader-progress-bar" class="bar full-height" style="width: <?= ($resource ? '100' : '0') ?>%;"></div>
											</div>
										</div>
										<div class="span1 player-uploaded-percent">
											<span id="player-uploaded-percent-gauge"><?= ($resource ? '100%' : '') ?></span>
											<a href="#" id="player-uploaded-cancel-upload"><i class="icon-remove"></i></a>
										</div>
									</div>
								</div>

								<?php
								// when editing a video/file, id_org will be set
								echo CHtml::hiddenField('id_org', $resource ? $resource['id_org'] : '', array('id' => 'file-id-org'));
								// the following hidden input makes sense when editing a video/file
								// will be set to '1' if the user has uploaded a new file, thus overwriting the old file
								echo CHtml::hiddenField('new_upload', 0, array('id' => 'file-new-upload'));
								?>
							</div>
						</div>

					</div>
				</div>
				<!-- /end upload -->

				<?php if ($requestTitle): ?>
					<div class="control-group">
						<?php echo CHtml::label(Yii::t('standard', '_TITLE'), 'file-title', array(
							'class' => 'control-label'
						)); ?>
						<div class="controls">
							<?php echo CHtml::textField('file-title', ($resource ? $resource['title'] : ''), array('id' => 'file-title', 'class' => 'input-block-level')); ?>
						</div>
					</div>
				<?php endif; ?>


				<?php if ($lo_type == LearningOrganization::OBJECT_TYPE_SCORMORG && $resource && $loModel): ?>
					<div class="control-group">
						<?php echo CHtml::label(Yii::t('standard', '_TITLE'), 'new-scorm-title', array(
							'class' => 'control-label'
						)); ?>
						<div class="controls">
							<?php echo CHtml::textField('new-scorm-title', $loModel->title, array('id' => 'new-scorm-title', 'class' => 'input-block-level')); ?>
						</div>
					</div>
				<?php endif; ?>
				
				<?php if ($lo_type == LearningOrganization::OBJECT_TYPE_AICC && $resource && $loModel): ?>
					<div class="control-group">
						<?php echo CHtml::label(Yii::t('standard', '_TITLE'), 'new-aicc-title', array(
							'class' => 'control-label'
						)); ?>
						<div class="controls">
							<?php echo CHtml::textField('new-aicc-title', $loModel->title, array('id' => 'new-aicc-title', 'class' => 'input-block-level')); ?>
						</div>
					</div>
				<?php endif; ?>
				

				<?php if ($requestDescription): ?>	
					<div class="control-group">
						<?php echo CHtml::label(Yii::t('standard', '_DESCRIPTION'), 'file-description', array(
							'class' => 'control-label'
						)); ?>
						<div class="controls">
							<!-- <textarea id="file-description<?= ($resource ? $resource['id_org'] : '') ?>" -->
							<textarea id="file-description"
								name="description"
								class="input-block-level squared"><?= ($resource ? $resource['description'] : '') ?></textarea>
						</div>
					</div>
				<?php endif; ?>

				<?php if ($lo_type == LearningOrganization::OBJECT_TYPE_SCORMORG) : ?>
					<?php
						$options['desktop_openmode'] 	= $loModel->desktop_openmode;
						$options['tablet_openmode'] 	= $loModel->tablet_openmode;
						$options['smartphone_openmode'] = $loModel->smartphone_openmode;
						
						$this->renderPartial('_upload_options_scorm', array(
							'options' 		=> $options,
							'courseModel' 	=> $courseModel,
						));
					?>	
				<?php elseif($lo_type == LearningOrganization::OBJECT_TYPE_AICC) : ?>
					<?php 
						// Same options, different model
						$options['desktop_openmode'] 	= $loModel->desktop_openmode;
						$options['tablet_openmode'] 	= $loModel->tablet_openmode;
						$options['smartphone_openmode'] = $loModel->smartphone_openmode;

						$this->renderPartial('_upload_options_aicc', array(
							'options' 		=> $options,
							'courseModel' 	=> $courseModel,
						));
					?>
				<?php endif; ?>

				<br>

				<div class="text-right">

					<input type="submit" id="player-save-uploaded-lo"
						class="btn-docebo green big" value="<?php echo Yii::t('standard', '_SAVE'); ?>">&nbsp;&nbsp;
					<a id="player-cancel-uploaded-lo" href="#"
					   class="btn-docebo black big"><?php echo Yii::t('standard', '_CANCEL'); ?></a>
					   
				</div>

			</div>

		</div>

	</div>


</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">

	$(function () {
		//TinyMce.attach('textarea#file-description<?= ($resource ? $resource['id_org'] : '') ?>', {height: 200});
		TinyMce.attach('#file-description', {height: 200});
		//Arena.uploader.run();

		// Styler, I hate it, but....
		$('.scorm-upload-options input[type="radio"]').styler();
		
	});

</script>


