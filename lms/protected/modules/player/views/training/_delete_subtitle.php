<?php
/* @var $model LearningVideoSubtitles */

$idSub = (is_object($model) && property_exists($model, 'id')) ? $model->id : !empty($_REQUEST['id']) ? $_REQUEST['id'] : '';
$langCode = Yii::app()->request->getParam('langcode', '');
?>

<?= CHtml::beginForm('', 'POST', array('class'	=> 'ajax')); ?>
<?= CHtml::hiddenField('id', $idSub) ?>
<?= CHtml::hiddenField('langCode', $langCode) ?>

<div id="proceed-check" class="coaching-mode-radio-btn">
	<?php echo CHtml::checkBox("agree_deletion", false, array(
		'id'    =>  'proceed-check-btn',
		'class' =>  'delete-check ',
	)); ?>
	<?php echo CHtml::label(Yii::t('standard','Yes, I confirm I want to proceed'), 'proceed-check-btn',array('class'=>'coaching-inline-label')); ?>
</div>

<!-- For Dialog2 - they go in footer -->
<div class="form-actions">
	<?= CHtml::submitButton(Yii::t('standard', '_CONFIRM'), array(
			'name'  => 'confirm_button',
			'class' => 'sub-delete-confirm btn-docebo green big disabled' )
	); ?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' 	=> 'btn-docebo black big close-dialog')); ?>
</div>
<?php echo CHtml::endForm(); ?>

<script type="text/javascript">
	$(function(){
		$('.delete-item-modal .delete-check').styler();

		$('#proceed-check-btn').on('change', function() {
			if($(this).is(':checked'))
				$('.sub-delete-confirm').removeClass('disabled');
			else
				$('.sub-delete-confirm').addClass('disabled');
		});

		$('.sub-delete-confirm').on('click', function() {
			// Get the current subtitle row
			var selectedElement = $('#subtitles-list-row-<?=$langCode; ?>');

			// Add language to the list of languages
			$('#LearningVideoSubtitles_lang_code').append($('<option>', {
				value: selectedElement.data('filterValue'),
				text: selectedElement.data('filterText')
			}));

			// Sort languages in the dropdown
			var sel = $('#LearningVideoSubtitles_lang_code');
			var opts_list = sel.find('option').clone();
			opts_list.sort(function(a, b) { return $(a).text() > $(b).text() ? 1 : -1; });
			sel.html('').append(opts_list);
			// Select first option
			$('select#LearningVideoSubtitles_lang_code option:first-child').attr("selected", "selected");

			// Remove current subtitle row
			$('#' + selectedElement.attr('id')).remove();

			// Decrease the count of already added subtitles
			var addedSubsCount = $('.added-subs-count').html();
			addedSubsCount--;
			$('.added-subs-count').html(addedSubsCount);

			var currentFallback = $('input[name="fallbackSub"]').val();
			if (currentFallback == selectedElement.data('realId')) {
				$('input[name="fallbackSub"]').val('');
			}
		});
	});
</script>