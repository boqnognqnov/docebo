<?php
/** @var $disableAddLOButton bool */
/** @var $this TrainingController */
Yii::app()->clientScript->registerCoreScript('history');
?>
<div id="player-arena-container">
	<!-- ADD LO BUTTON -->
	<?php
	$deadline = $courseModel->getDeadlineForUser(Yii::app()->user->id);
	if ($deadline && $deadline != '0000-00-00' && $deadline != '0000-00-00 00:00:00' && Yii::app()->localtime->fromLocalDateTime($deadline) < Yii::app()->localtime->getLocalNow()): ?>
		<div class="row-fluid" id="course-expired-softdeadline-warning">
			<div id="exclamation-mark"><i class="fa fa-exclamation-circle"></i></div>
			<div
				id="warning-text"><?php
				echo ($courseModel->getSoftDeadlineEnabled())
					? Yii::t('course', '<strong>Hey, it looks like you are late!</strong> This course should have been completed by <strong>{end_date}</strong>. Thanks for taking the time to complete it now!', array(
						'{end_date}' => Yii::app()->localtime->stripTimeFromLocalDateTime($deadline)
					))
					: Yii::t('course', '<strong>Hey, it looks like you are late!</strong> This course should have been completed by <strong>{end_date}</strong>.', array(
						'{end_date}' => Yii::app()->localtime->stripTimeFromLocalDateTime($deadline)
					)
				);
				?></div>
		</div>
	<?php endif; ?>
	<a id="arena-board-top" href="#arena-board-top"></a>

	<div class="row-fluid" data-bootstro-id="bootstroLaunchpad" id="player-arena-content">

		<!-- INLINE Edit pane -->
		<div id="player-lo-edit-inline-panel"></div>

		<!-- LAUNCHPAD  -->
		<div id="player-arena-launchpad"></div>

		<div class="clearfix"></div>

		<!-- LO MANAGER -->
		<div id="player-lo-manage-panel" class="player-arena-board" style="display: none;">
			<div class="row-fluid player-lo-manage-header">
				<div class="span4">
					<span class="lom-title"><?php echo Yii::t('organization', 'Folders'); ?></span>
				</div>
				<div class="span8">
					<?php if ($disableAddLOButton!==true) : ?>
					<span class="lom-title"><?php echo Yii::t('standard', 'Training materials'); ?></span>
					<?= $this->renderPartial("_add_lo_button", array(
						'courseModel' => $courseModel,
					)) ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="player-arena-overall-container">
				<div class="row-fluid">
					<div class="span4">
						<div id="player-arena-overall-folders" class="player-arena-overall-column">
							No folders
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="span8">
						<div id="player-arena-overall-objlist" class="player-arena-overall-column player-arena-overall-objlist"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- /LO MANAGER -->
	</div>

	<div class="player-navigator" style="display: none;"></div>

	<div class="player-quicknav-wrapper" id="player-quicknavigation"></div>

	<?php $this->widget('OverlayHints'); ?>
</div>

