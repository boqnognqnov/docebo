<?php
$evaluation = LtCourseuserSession::model()->findByPk(array(
	'id_user' => Yii::app()->user->id,
	'id_session' => $idSession
));

// TODO: check if $evaluation is a record and has valid evaluation content

if ($evaluation && $evaluation->evaluation_status !== null):

?>
<div>

	<h2><?= Yii::t('classroom', 'Personal performance evaluation'); ?></h2>

	<div class="border-container evaluation-info-container">

		<!-- evaluation status -->
		<div class="evaluation-status-score">

			<div>
				<span class="evaluation-score <?php echo ($evaluation->evaluation_status > 0 ? 'good' : 'bad'); ?>">
					<?php echo ($evaluation->evaluation_status > 0 ? Yii::t('coursereport', '_PASSED') : Yii::t('coursereport', '_NOT_PASSED')); ?>
				</span>
				&nbsp;&nbsp;
				<span class="classroom-sprite <?php echo ($evaluation->evaluation_status > 0 ? 'approved-large' : 'unapproved-large'); ?>"></span>
			</div>

			<br />
			
			<div class="">
				<?= Yii::t('standard', '_SCORE') ?>
				&nbsp;&nbsp;
				<span class="evaluation-score <?= ($evaluation->evaluation_status > 0 ? 'good' : 'bad') ?>"><?= $evaluation->evaluation_score ?></span>
				/
				<?= $sessionModel->score_base ?>
			</div>
			
		</div>


		<!-- evaluator info -->
		<div class="evaluator-info">

			<div>
				<span class="date"><?= $evaluation->evaluation_date ?></span>
			</div>

			<div>
				<?= Yii::t('menu_over', 'Teacher') ?>:
				<br />
				<?php
					if ($evaluation->evaluator) {
						echo $evaluation->evaluator->getAvatarImage();
						echo $evaluation->evaluator->getFullName();
					}
				?>
			</div>
			
		</div>


		<!-- evaluation data -->
		<div class="evaluation-text-file">
			<h3><?= Yii::t('classroom', 'Student performance evaluation') ?></h3>
			<p>
				<?= (!empty($evaluation->evaluation_text) 
					? $evaluation->evaluation_text 
					: '<span class="empty-evaluation">('.Yii::t('classroom', 'No evaluation text provided').')</span>') 
				?>
			</p>
			
			<?php if (!empty($evaluation->evaluation_file)): ?>
			
			<div class="separator"></div>
			
			<div class="">
				<?php
					$_downloadUrl = Docebo::createLmsUrl('ClassroomApp/instructor/downloadEvaluationFile', array(
						'id_user' => Yii::app()->user->id,
						'id_session' => $idSession,
						'course_id' => $courseModel->getPrimaryKey()
					));
				?>
				<span class="classroom-sprite dl-file"></span>
				<a href="<?= $_downloadUrl ?>" class="evaluation-file-download"><?= $evaluation->getOriginalFileName() ?></a>
			</div>
			
			<?php endif; ?>
			
		</div>


		<div class="clearfix"></div>

	</div>

</div>
<?php

endif;

?>