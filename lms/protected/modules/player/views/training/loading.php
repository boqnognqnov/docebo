<!doctype html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title><?php echo Yii::t('standard', '_LOADING'); ?></title>

	<style>
		html, body{
			height:100%;width:100%;margin:0;padding:0;overflow:hidden;
		}
		.scorm-loading{
			position: absolute;
			height: 170px;
			width: 170px;
			margin: -85px 0 0 -85px;
			left:50%;
			top:50%;
		}
	</style>
</head>
<body>
	<div class="scorm-loading">
		<?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/loading_big.gif', Yii::t('standard', '_LOADING')); ?>
	</div>
</body>
</html>