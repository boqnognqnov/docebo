<div class="courseEnroll-tabs">
	<ul class="nav nav-tabs">
		<li class="<?php echo $selectedTab == 'default' ? 'active' : ''; ?>">
			<a data-toggle="tab" href="#loThumbShared">
				<?php echo Yii::t('course', 'Choose a thumbnail for your course'); ?> (<?php echo $count['default']; ?>)
			</a>
		</li>
		<li class="<?php echo $selectedTab == 'user' ? 'active' : ''; ?>">
			<a data-toggle="tab" href="#loThumbPrivate">
				<?php echo Yii::t('course', 'Upload Your Own Thumbnail'); ?> (<span id="user-images-count"><?php echo $count['user']; ?></span>)
			</a>
		</li>
	</ul>

	<div class="select-user-form-wrapper">
		<div class="upload-form-wrapper" style="display: <?php echo $selectedTab == 'user' ? 'block' : 'none';?>">
			<div class="upload-form">
				<div class="fileUploadContent">
					<label for="upload-btn"><?php echo Yii::t('branding', '_UPLOAD_BACKGROUND_BTN'); ?></label>
					<?php echo CHtml::fileField('LearningOrganization[attachments]', '', array(
						'class' => 'ajaxCropNoParentForm custom-image',
						'data-class' => 'LearningOrganization',
						'data-width' => $cropSize['width'],
						'data-height' => $cropSize['height'],
						'data-image-type' => CoreAsset::TYPE_COURSELOGO,
						'data-callback' => 'updateSliderContent',
						'data-url' => Docebo::createAdminUrl('crop', array(
							'imageType' => CoreAsset::TYPE_LO_IMAGE,
							'class'=>'LearningOrganization',
							'width'=>$cropSize['width'],
							'height'=>$cropSize['height'],
							'cropCallback' => 'updateSliderCarousel', // see $.fn.updateSliderCarousel
						)),
					)); ?>
				</div>
			</div>
		</div>
		<div class="tab-content">
			<div id="loThumbShared" class="courseEnroll-page-users tab-pane <?php echo $selectedTab == 'default' ? 'active' : ''; ?>">
				<div id="defaultSlider_<?php echo $uniqueId; ?>" class="carousel slide thumbnails-carousel">
					<!-- Carousel items -->
					<div class="carousel-inner">
						<?php if (!empty($defaultImages)) { ?>
							<?php foreach ($defaultImages as $key => $defaultThumbnail) { ?>
								<div class="item<?php echo ($key == 0) ? ' active' : ''; ?>">
									<?php foreach ($defaultThumbnail as $key => $imageUrl) {
										$html = CHtml::image($imageUrl);
										$html .= '<input type="radio" name="thumb" value="' . $key . '" '. ($model && $key == $model->resource ? 'checked="checked"' : '') .' />';
										echo '<div class="sub-item ' . ($model && $key == $model->resource ? 'checked' : '') .'">' . $html . '</div>';
									}
									?>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
					<!-- Carousel nav -->
					<a class="carousel-control left" href="#defaultSlider_<?php echo $uniqueId; ?>" data-slide="prev">&lsaquo;</a>
					<a class="carousel-control right" href="#defaultSlider_<?php echo $uniqueId; ?>" data-slide="next">&rsaquo;</a>
				</div>
			</div>
			<div id="loThumbPrivate" class="courseEnroll-page-groups tab-pane <?php echo $selectedTab == 'user' ? 'active' : ''; ?>">
				<div id="userSlider_<?php echo $uniqueId; ?>" class="carousel slide thumbnails-carousel">
					<!-- Carousel items -->
					<div class="carousel-inner">
						<?php if (!empty($userImages)) { ?>
							<?php foreach ($userImages as $key => $userThumbnail) { ?>
								<div class="item<?php echo ($key == 0) ? ' active' : ''; ?>">
									<?php foreach ($userThumbnail as $key => $imageUrl) {
										$html = CHtml::image($imageUrl);
										$html .= '<input type="radio" name="thumb" value="' . $key . '" '. ($model && $key == $model->resource ? 'checked="checked"' : '') .' />';
										echo '<div class="sub-item ' . ($model && $key == $model->resource ? 'checked' : '') .'">' . $html;
										echo '<span class="deleteicon"><span class="i-sprite is-remove red"></span></span>';
										echo '</div>';
									}
									?>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
					<!-- Carousel nav -->
					<a class="carousel-control left" href="#userSlider_<?php echo $uniqueId; ?>" data-slide="prev">&lsaquo;</a>
					<a class="carousel-control right" href="#userSlider_<?php echo $uniqueId; ?>" data-slide="next">&rsaquo;</a>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$(function(){
		$('.nav.nav-tabs li a').on('click', function (e) {
			if($(this).attr('href')==='#loThumbPrivate')
				$('.upload-form-wrapper').show();
			else
				$('.upload-form-wrapper').hide();
		});

		$('.loSlider .sub-item > img').on('click', function(){

			// Remove "selected" state from all items in shared and private carousels
			$('.loSlider .sub-item').removeClass('checked');

			// And then check the current image as active
			$(this).closest('.sub-item').addClass('checked').find('input[type=radio]').click();

		});
	})

	function updateControls(nav) {
		if (nav == 'show' ) {
			$('[id*="userSlider"] .carousel-control.right').show();
			$('[id*="userSlider"] .carousel-control.left').show();
		}
		else if (nav == 'hide' ) {
			$('[id*="userSlider"] .carousel-control.right').hide();
			$('[id*="userSlider"] .carousel-control.left').hide();
		}
	}
	var navStatus = $('[id*="userSlider"] .sub-item').length > 0 ? 'show' : 'hide';
	updateControls(navStatus);



	/**
	 * DELETE User image
	 */

	$(document).off('click', '[id*="userSlider"] .deleteicon').on('click', '[id*="userSlider"] .deleteicon', function(e) {

		var imageId = $(this).closest('.sub-item').find('input[type="radio"]').val();
		var form = $(this).closest('form');

		var options = {
			type        : 'post',
			dataType    : 'json',
			data: {
				delete_image	: true,
				image_id		: imageId
			},
			success     : function (res) {
				if (res && (res.success == true)) {
					$('[id*="userSlider"]').find('input[type="radio"][value="'+ imageId +'"]').closest('.sub-item').remove();
					$('#user-images-count').text($('[id*="userSlider"] .sub-item').length);
					var navStatus = $('[id*="userSlider"] .sub-item').length > 0 ? 'show' : 'hide';
					updateControls(navStatus);
				}
			}
		};

		e.stopPropagation();
		form.ajaxForm(options).submit();
		return false;

	});

</script>
