<?php /* @var $folderObj LearningOrganization */ ?>

<!-- New Folder Editor Panel -->
<div id="player-arena-newfolder-editor-panel" class="player-arena-editor-panel">
	<div class="header"><?= Yii::t('standard', '_NEW_FOLDER') ?></div>
	<div class="content">
		<?php

		if (Yii::app()->user->hasFlash('error')) {
			echo '<div class="alert">' . Yii::app()->user->getFlash('error') . '</div>';
		}

		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'create-newfolder-form',
			'method' => 'post',
			'htmlOptions' => array(
				'class' => 'form-horizontal'
			)
		));

		if ($folderObj) {
			echo CHtml::hiddenField('org_id', $folderObj->idOrg);
		}

		echo CHtml::hiddenField('course_id', $this->course_id);
		echo CHtml::hiddenField('parent_id', $parent_id);
		?>
		<div class="control-group">
			<?php echo CHtml::label(Yii::t('storage', '_ORGFOLDERNAME'), 'folder-name', array(
				'class' => 'control-label'
			)); ?>
			<div class="controls">
				<?php echo CHtml::textField('folder_name', ($folderObj ? $folderObj->title : ''), array('id' => 'folder-name', 'class' => 'input-block-level')); ?>
			</div>
		</div>

		<div class="text-right">
			<input type="submit" name="create_folder_button" id="confirm_save_newfolder"
				   class="btn-docebo green big" value="<?php echo Yii::t('standard', '_SAVE'); ?>">&nbsp;&nbsp;
			<a id="cancel_save_newfolder" class="btn-docebo black big"><?php echo Yii::t('standard', '_CANCEL'); ?></a>
		</div>

		<?php $this->endWidget(); ?>

	</div>
</div>