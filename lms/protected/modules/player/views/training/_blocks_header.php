<?php
	$_urlParams = array("course_id" => $courseModel->idCourse);
	if (isset($courseSessionModel)) { $_urlParams['id_session'] = $courseSessionModel->getPrimaryKey(); }
	$addBlockUrl = $this->createUrl("block/axUpdate", $_urlParams);
?>
<div class="row-fluid player-blocks-nav-header">
    <div class="span12 player-blocks-nav-header-button text-right">
    <?php if ($this->userCanAdminCourse && !isset($courseSessionModel)) :?>
		<a 
			href="<?= $addBlockUrl ?>" 
			id="add-block-launcher" 
			rel="add-block-dialog" 
			data-dialog-class="edit-block-modal" 
			class="open-dialog btn-docebo green big"
			data-bootstro-id="bootstroAddBlockLauncher">
			<span><?= Yii::t('player', 'Add Course Blocks') ?></span></a>
    <?php endif; ?>
	</div>
</div>