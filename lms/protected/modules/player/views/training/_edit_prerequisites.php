<style>
	<!--

	-->
</style>


<h1><?= Yii::t('player', 'Set Learning object Prerequisites') ?></h1>
<h2><?= $loModel->title ?>
	<hr>
</h2>

<!--
Object ID: <?= $loModel->idOrg ?>
-->

<form action="<?= $this->createUrl("axEditLoPrerequisites") ?>" class="ajax" id="edit-prerequisites-form" method="post">
	<input type="hidden" name="id_object" id="id-object" value="<?= $loModel->idOrg ?>">
	<input type="hidden" name="selected_prereq" id="selected-prereq" value="">

	<div id="prereq-container" class="dynatree-container-parent">
		<!-- Here goes the LO Tree -->
		<div id="prereq-tree"></div>
	</div>

	<div class="form-actions">
		<input class="btn-docebo green big" id="edit-prerequisites-save-button" type="submit" name="confirm_save_prerequisites" value="<?php echo Yii::t('standard', '_SAVE'); ?>">
		<input class="btn-docebo black big close-dialog" type="button" value="<?php echo Yii::t('standard', '_CANCEL'); ?>">
	</div>
</form>

<script type="text/javascript">
	<!--

	// Encode All Lo Tree Data in a JS object
	var treeData = <?= CJSON::encode($loTreeData) ?>;

	// LO Id that should be NOT assigned a checkbox
	var targetIdOrg = parseInt(<?= $loModel->idOrg ?>);

	var dialogError = function (message) {
		bootbox.alert(message);
	};

	var currentPrereq = <?= CJSON::encode($prerequisitesArray) ?>;

	jQuery(function($){
		$('#prereq-tree').on('click', '.dynatree-title', function(){
			// LO title/name click should mark its checkbox (should work like a label)
			$(this).parent().find('.dynatree-checkbox').click();
		});
	});

	// Attach tree data
	$("#prereq-tree").dynatree({

		children: treeData[0].children,

		selectMode: 2,

		checkbox: true,

		onCreate: function (node, nodeSpan) {
			var $nodeSpan = $(nodeSpan);
		},

		onRender: function (node, nodeSpan) {
			var $nodeSpan = $(nodeSpan);
			var cssClass = '';
			if (node.data.isFolder) {
				var opened = $nodeSpan.hasClass('dynatree-expanded');
				cssClass = opened ? "is-folder opened " : "is-folder closed ";
				if (node.hasChildren()) cssClass += " has-children";
				$nodeSpan.find('span.dynatree-icon').addClass('p-sprite '+cssClass).removeClass('dynatree-icon');
				node.data.hideCheckbox = true;
			}
			else {
				cssClass = Arena.learningObjects
					? Arena.learningObjects.getSpriteForObject(node.data.type)
					: Arena.learningObjectsManagement
						? Arena.learningObjectsManagement.getSpriteForObject(node.data.type)
						: '';
				
				// Swap icon and checkbox spans
				$nodeSpan.find('span.dynatree-icon').after($nodeSpan.find('span.dynatree-checkbox'));
				
				$nodeSpan.find('span.dynatree-icon').addClass('i-sprite grey '+cssClass).removeClass('dynatree-icon');
			}

		}

	});

	//Select CURRENT prerequisites
	$("#prereq-tree").dynatree('getRoot').visit(function (node) {
		if (!node.data.isFolder && $.inArray(""+node.data.idOrganization, currentPrereq) >= 0) {
			node.select(true);
		}
	});

	$("#prereq-tree").dynatree("getRoot").visit(function (node) {
		node.expand(true);
	});

	// On SAVE button click, make a list of selected LOs and put it into the hidden field
	$("#edit-prerequisites-save-button").on("click", function () {
		// Get selected NODES (ignore folders!!!!)
		var selectedNodes = $("#prereq-tree").dynatree("getSelectedNodes");
		var selectedKeys = $.map(selectedNodes, function (node) {
			if (!node.data.isFolder) {
				return node.data.idOrganization;
			}
		});

		var prereqList = selectedKeys.join(',');
		$("#selected-prereq").val(prereqList);
		return true;

	});

	//-->
</script>