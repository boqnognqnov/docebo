<?php /* @var $loModel LearningOrganization */ ?>

<?php if ($error) : ?>
	<h1><?=Yii::t('standard', '_OPERATION_FAILURE')?></h1>

	<div class="alert"><?= $errorMessage ?></div>

	<div class="form-actions">
		<input class="btn-docebo black big close-dialog" type="button" value="<?= Yii::t('standard', '_CLOSE'); ?>">
	</div>

<?php return; endif;  ?>



<style>
<!--
	.modal-configure-lo{
		width: 700px;
		margin-left: -350px;
	}
	.modal-configure-lo .option-deprecated{
		font-style: italic;
	}
	.modal-configure-lo a, .modal-configure-lo a:hover{
		color: #08c;
	}
	.modal-configure-lo .date-interval-label{
		padding-top: 5px;
		float: left;
		margin-right: 10px;
	}
    #publish-from, #publish-to{
	    width: 80%;
    }
-->
</style>


<h1><?= Yii::t('player', 'Edit Learning Object Properties'); ?></h1>
<h4><?= $loModel->title ?>
	<hr>
</h4>

<?php

	$radioTemplate = '<label class="radio">{input}{label}</label>';

/** @var CActiveForm $form */
	$form = $this->beginWidget('CActiveForm', array(
		'action' => $this->createUrl("axConfigureLo", array("idOrg" => $loModel->idOrg)),
		'method' => 'POST',
		'id' => 'configure-lo-form',
		'enableAjaxValidation' => false,
		'htmlOptions'=>array(
			'class' => 'ajax form-horizontal docebo-form light',
		),
	));
?>


	<?php if ($loModel->hasErrors()) : ?>
		<div class="alert">
			<?php
				echo $form->errorSummary($loModel);
			?>
		</div>
	<?php endif; ?>

	<?php //echo $form->hiddenField($loModel, 'idOrg'); ?>

	<div class="configure-lo-container">

		<!-- COMMON PART, the same for all LOs -->


		<!-- visible ? (Hidden) -->
		<div class="control-group">
			<label class="control-label" for="LearningOrganization[visible]"><?= Yii::t('standard', '_HIDDEN'); ?></label>
			<div class="controls">
				<?=$form->radioButton($loModel, 'visible', array(
					'value'=>'1', 'uncheckValue'=>null
				))?>
				<?=Yii::t('standard', '_NO')?>

				<?=$form->radioButton($loModel, 'visible', array(
					'value'=>'0', 'uncheckValue'=>null
				))?>
				<?=Yii::t('standard', '_YES')?>

				<div class="clearfix"></div>

			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="LearningOrganization[publish_from]"><?= Yii::t('organization', 'Publish'); ?></label>
			<div class="controls">
				<div class="row-fluid">
					<div class="span6">
						<div class="date-interval-label">
							<?=ucfirst(Yii::t('standard', '_FROM'))?>
						</div>
						<div>
							<div class="input-append date" id="publish-from" data-date-format="<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>" data-date-language="<?= Yii::app()->getLanguage() ?>">
								<?php
								echo $form->textField($loModel, 'publish_from', array(
									'value' => Yii::app()->localtime->stripTimeFromLocalDateTime($loModel->publish_from),
									'class'=>'span9',
									'style'=>'padding:0'
								));
								?>
								<span class="add-on"><span class="i-sprite is-calendar-date"></span></span>
							</div>
						</div>
					</div>


					<div class="span6">
						<div class="date-interval-label">
							<?=ucfirst(Yii::t('standard', '_TO'))?>
						</div>
						<div class="input-append date" id="publish-to" data-date-format="<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>" data-date-language="<?= Yii::app()->getLanguage() ?>" data-date-language="<?= Yii::app()->getLanguage() ?>">
							<?php
							echo $form->textField($loModel, 'publish_to', array(
								'value' => Yii::app()->localtime->stripTimeFromLocalDateTime($loModel->publish_to),
								'class'=>'span9'
							));
							?>
							<span class="add-on"><span class="i-sprite is-calendar-date"></span></span>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Conditional visibility? (Number of views..) -->
		<?php if($showNumberOfViewsOption) : ?>
		<div class="control-group">
			<label class="control-label" for="LearningOrganization[self_prerequisite]"><?= Yii::t('standard', '_PLAY_CHANCE'); ?></label>
			<div class="controls">
				<? foreach(array(
					'*' 		 => Yii::t('standard', '_UNLIMITED'),
					'incomplete' => Yii::t('standard', '_UNTIL_COMPLETED'),
					'NULL' 		 => Yii::t('standard', '_ONLY_ONCE')
				) as $val=>$label): ?>
					<label>
						<?=$form->radioButton($loModel, 'self_prerequisite', array(
							'value'=>$val,
							'uncheckValue'=>null
						))?> <?=$label?> &nbsp;&nbsp;
					</label>
				<? endforeach; ?>
			</div>
		</div>
		<?php endif; ?>


		<!-- Terminator ? -->
		<div class="control-group">
			<label class="control-label" for="LearningOrganization[isTerminator]"><?= Yii::t('organization', '_ORGISTERMINATOR'); ?></label>
			<div class="controls">
				<? foreach(array(
					'0' => Yii::t('standard', '_NO'),
					'1' => Yii::t('standard', '_YES')
				) as $val=>$label): ?>
					<label>
						<?=$form->radioButton($loModel, 'isTerminator', array(
							'value'=>$val,
							'uncheckValue'=>null
						))?> <?=$label?> &nbsp;&nbsp;
					</label>
				<? endforeach; ?>

				<div class="clearfix"></div>

				<p class="muted">
					<?=Yii::t('organization', 'By activating the end object marker, completing this object will mark the entire course as completed')?>
				</p>
			</div>
		</div>

		<!-- Bookmark (milestone) ? -->
		<div class="control-group">
			<label class="control-label" for="LearningOrganization[milestone]"><?= Yii::t('organization', '_ORGMILESTONE'); ?></label>
			<div class="controls">
				<p class="option-deprecated">
					<?=Yii::t('organization', 'This option has been moved to the <a href="{link}">course settings</a>', array(
						'{link}'=>Docebo::createLmsUrl('player/coursesettings', array('course_id'=>$loModel->idCourse))
					))?>
				</p>
			</div>
		</div>




		<!-- Conditional parameters: LO Type dependent -->

		<!-- Mobile ?? -->
		<?php /* if (in_array($loModel->objectType, LearningOrganization::$HAVE_MOBILE_PARAMETER )) : ?>

			<div class="control-group">
				<label class="control-label" for="LearningOrganization[mobile]"><?= Yii::t('standard', 'Mobile') ?></label>
				<div class="controls">
					<?php
						echo  $form->radioButtonList($loModel, 'mobile',
							array(
								'0' => Yii::t('standard', '_NO'),
								'1' => Yii::t('standard', '_YES')
							),
							array('template' => $radioTemplate,'separator' => '')
						);
					?>
				</div>
			</div>

		<?php endif;*/ ?>

		<!-- Autoplay ?? -->
		<?php if (in_array($loModel->objectType, LearningOrganization::$HAVE_AUTOPLAY_PARAMETER )) : ?>

			<div class="control-group">
				<label class="control-label" for="LearningOrganization[autoplay]"><?= Yii::t('organization', '_AUTOPLAY'); ?></label>
				<div class="controls">
					<? foreach(array(
						LearningOrganization::AUTOPLAY_NO => Yii::t('standard', '_NO'),
						LearningOrganization::AUTOPLAY_YES => Yii::t('standard', '_YES')
					) as $val=>$label): ?>
						<label>
							<?=$form->radioButton($loModel, 'autoplay', array(
								'value'=>$val,
								'uncheckValue'=>null
							))?> <?=$label?> &nbsp;&nbsp;
						</label>
					<? endforeach; ?>
				</div>
			</div>

		<?php endif;?>

		<!-- Width ?? -->
		<?php if (in_array($loModel->objectType, LearningOrganization::$HAVE_WIDTH_PARAMETER ) || in_array($loModel->objectType, LearningOrganization::$HAVE_HEIGHT_PARAMETER)) : ?>

			<div class="control-group">
				<label class="control-label" for="LearningOrganization[width]"><?= Yii::t('standard', 'Dimensions'); ?></label>
				<div class="controls">
					<div class="row-fluid">
						<? if(in_array($loModel->objectType, LearningOrganization::$HAVE_WIDTH_PARAMETER )): ?>
							<div class="span6">
								<div class="pull-left date-interval-label">
									<?= Yii::t('standard', '_WIDTH'); ?> px
								</div>
								<div class="pull-left">
									<?php echo $form->textField($loModel, 'width', array(
										'style'=>'width: auto; max-width: 100px;'
									)); ?>
								</div>
								<!--<div class="pull-left date-interval-label">
									&nbsp;px
								</div>-->
							</div>
						<? endif; ?>
						<? if(in_array($loModel->objectType, LearningOrganization::$HAVE_HEIGHT_PARAMETER )): ?>
							<div class="span6">
								<div class="pull-left date-interval-label">
									<?= Yii::t('standard', '_HEIGHT'); ?> px
								</div>
								<div class="pull-left">
									<?php echo $form->textField($loModel, 'height', array(
										'style'=>'width: auto; max-width: 100px;'
									)); ?>
								</div>
								<!--<div class="pull-left date-interval-label">
									&nbsp;px
								</div>-->
							</div>
						<? endif; ?>
					</div>

				</div>
			</div>

		<?php endif;?>

		<!-- OPEN MODE (inline, popup, lightbox) -->
		<?php if (in_array($loModel->objectType, LearningOrganization::$HAVE_OPENMODE_PARAMETER )) : ?>
			<div class="control-group">
				<label class="control-label" for="LearningOrganization[openmode]"><?= Yii::t('player', 'Opening mode'); ?></label>
				<div class="controls">
					<?
					$radioButtons = array(
						LearningOrganization::OPENMODE_INLINE => Yii::t('player', 'Inline'),
						LearningOrganization::OPENMODE_LIGHTBOX => Yii::t('player', 'Lightbox')
					);
					if (!Yii::app()->legacyWrapper->hydraFrontendEnabled())
						$radioButtons[LearningOrganization::OPENMODE_POPUP] = Yii::t('player', 'New window');
					?>
					<? foreach($radioButtons as $val=>$label): ?>
						<label>
							<?=$form->radioButton($loModel, 'openmode', array(
								'value'=>$val,
								'uncheckValue'=>null
							))?> <?=$label?> &nbsp;&nbsp;
						</label>
					<? endforeach; ?>
				</div>
			</div>
		<?php endif;?>



	</div>




	<!-- Save/Cancel buttons -->
	<div class="form-actions">
		<input class="btn-docebo green big" id="configure-lo-save-button" type="submit" name="confirm_save_configure_lo" value="<?php echo Yii::t('standard', '_SAVE'); ?>">
		<input class="btn-docebo black big close-dialog" type="button" value="<?= Yii::t('standard', '_CANCEL'); ?>">
	</div>



<?php $this->endWidget(); ?>



<script type="text/javascript">
<!--

$(function(){
	$("#publish-from").bdatepicker({
		language: '<?= Yii::app()->getLanguage() ?>'
	});

	$("#publish-to").bdatepicker({
		language: '<?= Yii::app()->getLanguage() ?>'
	});

	$('input,select', '#configure-lo-form').styler();
});

//-->
</script>
