<?php
if(PluginManager::isPluginActive('Share7020App')){
	// ****** GURUS CIRCLES ****** //
	$gurusArray = array_fill(0, 8, array(
		'name' => 'Admin',
		'linkSource' => 'javascript:;',
		'status' => 'online',
		'imgSource' => 'https://placeholdit.imgix.net/~text?txtsize=14&txt=40%C3%9740&w=40&h=40')
	);
	$listParams = array(
		'arrayData' => $gurusArray,
		'maxVisibleItems' => 3,
		'containerClass' => 'topVideosGuruList'
	);
	?>
	<div id="<?= $containerId ?>">
		<div class="row-fluid">
			<div class="hintSection span5">
				<div class="app7020-hint app7020-course-player-hint">
					<h4>
						<?= Yii::t('app7020', 'Knowledge Gurus <span>are here</span>'); ?>
					</h4>
					<h4>
						<?= Yii::t('app7020', '<span>to</span> answer your questions!'); ?>
					</h4>
				</div>
			</div>
			<div class="gurusSection span3">
				<?php $this->widget('common.widgets.GuruCirclesList', $listParams); ?>
			</div>
			<div class="dialogSwitcherSection span4">
				<?= CHtml::link(Yii::t('app7020', 'Ask the expert!'), 'javascript:;', array('class' => 'app7020-button')) ?>
			</div>
		</div>
	</div>
<?php
}
?>