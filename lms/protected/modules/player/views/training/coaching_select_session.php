<div id="player-blocks-container" style="margin: 0!important; padding: 20px!important;">
	<div id="user-coaching-session-content">
		<!-- There will be listed all coaching sessions, that user should be able to pick from -->

		<?php
		// First try to get start date from tbe request if it is set
		$date_start = !empty($_REQUEST['date-start']) ? $_REQUEST['date-start'] : null;
		// Convert start date to UTC
		if ($date_start != null) {
			// Get the start date from the request
			//$date_start = Yii::app()->localtime->fromLocalDate($date_start);
			$date_start = Yii::app()->localtime->toUTC(Yii::app()->localtime->fromLocalDate($date_start).' 00:00:00');
		} else {
			// Set by default current date
			//$date_start = Yii::app()->localtime->getUTCNow();
			$date_start = Yii::app()->localtime->toUTC(Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->getLocalNow()).' 00:00:00');
		}

		// Second try to get the end date from the request
		$date_end = !empty($_REQUEST['date-end']) ? $_REQUEST['date-end'] : null;
		// convert the end date to UTC
		if ($date_end != null) {
			// Convert the end date from request
			//$date_end = Yii::app()->localtime->fromLocalDate($date_end);
			$date_end = Yii::app()->localtime->toUTC(Yii::app()->localtime->fromLocalDate($date_end).' 00:00:00');
		} else {
			// Set default end date to after 2 months
			//$date_end = Yii::app()->localtime->getUTCNow();
			$date_end = Yii::app()->localtime->toUTC(Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->getLocalNow()).' 23:59:59');
			$date_end = Yii::app()->localtime->addInterval($date_end, 'P2M');
			$date_end = Yii::app()->localtime->toUTC($date_end);
		}

		// Prepare dates for setting the DatePicker values - to local date !!!
		//$date_start_local = Yii::app()->localtime->toLocalDate($date_start);
		//$date_end_local = Yii::app()->localtime->toLocalDate($date_end);
		$date_start_local = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($date_start));
		$date_end_local = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($date_end));

		// Start rendering the select coaching session gridview
		?>
			<h3 class="title-bold"><span><?=Yii::t('course', 'Select your Coach') ?></span></h3>
			<?php
			// Begin FORM
			echo CHtml::beginForm('', 'POST', array(
				'id' 	=> 'coaching-user-select-session-form',
				'name' 	=> 'coaching-user-select-session-form',
				'class' => 'ajax',
			));

			// Default selected radio button value - if the user selected already a session and grid update is done
			$selectedCoachingSession = !empty($_REQUEST['sessionChoose']) ? (int) $_REQUEST['sessionChoose'] : null;
			?>

			<div class="main-section user-coaching-session-management">

				<div class="filters-wrapper">
					<div class="filters clearfix">
						<div class="input-wrapper">
							<?php
								echo Yii::t('standard', '_FILTER') . ': ' . '&nbsp;&nbsp;';

								// Start Date datePicker widget
								$this->widget(
									'common.widgets.DatePicker', array(
										'id' => 'date-start-container',
										'label' => Yii::t('report', 'Start date'),
										'fieldName' => 'date-start',
										'stackLabel' => false,
										'value' => $date_start_local,
										'htmlOptions' => array(
											'id' => 'date-start',
											'class' => 'bdatepicker_coaching',
											'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short'))
										),
										'datePickerOptions' => array(
											//"orientation" => "bottom left"
										)
									)
								);

								// End Date datePicker widget
								$this->widget(
									'common.widgets.DatePicker', array(
										'id' => 'date-end-container',
										'label' => Yii::t('report', 'End date'),
										'fieldName' => 'date-end',
										'stackLabel' => false,
										'value' => $date_end_local,
										'htmlOptions' => array(
											'id' => 'date-end',
											'class' => 'bdatepicker_coaching',
											'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short'))
										),
										'datePickerOptions' => array(
											//"orientation" => "bottom left"
										)
									)
								);
							?>
						</div>
					</div>
				</div>
			</div>

			<?php

			// Set a filter and adjust data to filter by
			$filterObject = new stdClass();
			$filterObject->ignoreEnded = true;
			$filterObject->ignoreFull = true;
			$filterObject->fromDate = $date_start;
			$filterObject->toDate = $date_end;

			// Begin Coaching Sessions gridView
			$this->widget('DoceboCGridView', array(
				'id' => 'course-coaching-sessions-grid',
				'ajaxType' => 'post',
				'afterAjaxUpdate' => 'function(id, data){
					$("#course-coaching-sessions-grid input").styler();
					 $(document).controls();
			    }',
				'htmlOptions' => array('class' => 'grid-view clearfix'),
				'template' => '{items}',
				'dataProvider' => LearningCourse::coachingSessionsSqlDataProvider($course_id, $filterObject),

				'columns' => array(
					array(
						'id' 	=> 'coaching-sessions-grid-radiobuttons',
						'type' => 'raw',
						'htmlOptions' => array('class' => 'coaching-session-column-radio'),
						'value' => function($data) use ($selectedCoachingSession) {
							$checkedSessionRadioBtn = ($data["idSession"] == $selectedCoachingSession) ? true : false;
							echo CHtml::radioButton("sessionChoose", $checkedSessionRadioBtn, array('value'=>$data["idSession"]));
						},
					),
					array(
						'name' => 'username',
						'header' => Yii::t('standard', '_USERNAME'),
						'htmlOptions' => array(
							'class' => 'center-aligned'
						),
						'headerHtmlOptions' => array(
							'class' => 'center-aligned'
						),
						'type' => 'raw',
						'value' => 'CHtml::encode(str_replace("/", "", $data["userid"]))'
					),
					array(
						'name' => 'firstname',
						'header' => Yii::t('standard', '_FIRSTNAME'),
						'htmlOptions' => array(
							'class' => 'center-aligned'
						),
						'headerHtmlOptions' => array(
							'class' => 'center-aligned'
						),
						'type' => 'raw',
						'value' => 'CHtml::encode($data["firstname"])'
					),
					array(
						'name' => 'lastname',
						'header' => Yii::t('standard', '_LASTNAME'),
						'htmlOptions' => array(
							'class' => 'center-aligned'
						),
						'headerHtmlOptions' => array(
							'class' => 'center-aligned'
						),
						'type' => 'raw',
						'value' => 'CHtml::encode($data["lastname"])'
					),
					array(
						'name' => 'session_dates',
						'header' => Yii::t('coaching', 'Session dates'),
						'htmlOptions' => array(
							'class' => 'center-aligned'
						),
						'headerHtmlOptions' => array(
							'class' => 'center-aligned'
						),
						'type' => 'raw',
						'value' => 'Yii::t("standard", "_FROM") . " " . Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($data["datetime_start"])) . " " . Yii::t("standard", "_TO") . "  " . Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($data["datetime_end"]))',
					),
					array(
						'header' => (Yii::t("standard", "AVAILABILITY")),
						'htmlOptions' => array(
							'class' => 'center-aligned coaching-session-availability'
						),
						'headerHtmlOptions' => array(
							'class' => 'center-aligned'
						),
						'type' => 'raw',
						'value' => array( LearningCourseCoachingSession::model(), 'renderAvailabilityColumn'),
					)
				),
			));

			?>

			<div class="form-actions" style="background-color: #fff; text-align: right; margin-bottom: 20px;">
				<?= CHtml::hiddenField('coaching_status', $coachingSessionStatus); ?>
				<?= CHtml::submitButton(Yii::t('standard', '_CONFIRM'), 		array('name'  => 'confirm_button', 'class' => 'btn btn-docebo green big')); ?>
			</div>

			<?php echo CHtml::endForm(); ?>
		<div class="clearfix"></div>
	</div>
</div>

<script type="text/javascript">
	(function ($) {

		// Apply styler to the radio inputs
		$("#course-coaching-sessions-grid input[type='radio']").styler();

		// On changing the filter start date do update of the grid + post form data
		$("input#date-start").on('change', function(){
			// Prepare the form data to be sent with gridView update
			var options = {
				data: $('#coaching-user-select-session-form').serialize()
			};

			// Do the yiiGridView update here
			$.fn.yiiGridView.update("course-coaching-sessions-grid", options);
		});

		// On changing the filter end date do update of the grid + post form data
		$("input#date-end").on('change', function(){
			// Prepare the form data to be sent with gridView update
			var options = {
				data: $('#coaching-user-select-session-form').serialize()
			};

			// Do the yiiGridView update here
			$.fn.yiiGridView.update("course-coaching-sessions-grid", options);
		});

		$(document).controls();
	})(jQuery);
</script>