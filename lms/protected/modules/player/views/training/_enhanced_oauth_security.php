<?php
    $oAuthClientsList = array();
    foreach (OauthClients::sqlDataProvider()->data as $client) {
        $oAuthClientsList[$client['client_id']] = $client["app_name"];
    }
?>

<?php if ($resource && $loModel): ?>
	<?php
	    $enableOAuth = $loModel->enable_oauth;
	    $oAuthClientId = $loModel->oauth_client;
	?>
<?php endif; ?>

<div class="control-group">
	<?php echo CHtml::label(Yii::t('standard', 'Add OAuth code to launch URL'), 'enable-oauth', array(
		'class' => 'control-label'
	)); ?>
	<div class="controls">
		<?php echo CHtml::checkBox('enable_oauth', $enableOAuth, array('id' => 'enable-oauth', 'class' => 'input-block-level')); ?>
	</div>
</div>

<div class="control-group">
	<?php echo CHtml::label(Yii::t('standard', 'OAuth client'), 'oauth-client', array(
		'class' => 'control-label'
	)); ?>
	<div class="controls">
		<?php echo CHtml::dropDownList('oauth_client', $oAuthClientId, $oAuthClientsList, array('id' => 'oauth-client', 'class' => 'input-block-level')); ?>
	</div>
</div>
