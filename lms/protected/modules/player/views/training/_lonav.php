<?php /** @var $courseModel */ ?>
<!-- Lo Navigation -->
<div id="player-lonav" class="second-menu">
	<div class="second-menu-spacer">
		<div class="player-lonav-content">
			<h2><?= $courseModel->name?></h2>
			<div class="player-lonav-tree"></div>
		</div>
	</div>
</div>