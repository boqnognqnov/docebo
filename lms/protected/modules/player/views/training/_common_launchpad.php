<?php
/* @var $lo_data mixed */
/* @var $player_bg array */

if (!empty($player_bg['src'])) {
	$style = 'style="'
			. 'background:url(' . $player_bg['src'] . ') center center no-repeat;'
			. 'background-position:center;';

	switch ($player_bg['aspect']) {
		case 'fill':
			$style .= 'background-size:cover;';
			break;
		case 'tile':
			$style .= '';
			break;
	}

	$style .= '"';
} else {
	$style = '';
}

if (isset($lo_data['idOrganization']) && (!is_null($lo_data['idOrganization']))) {
	if(PluginManager::isPluginActive('Coach7020App') && LearningOrganization::isAskGuruEnabled($lo_data['idOrganization']) && Settings::get('app7020_ask_button_lo', CoachApp7020Settings::OFF) != CoachApp7020Settings::OFF){
		$this->renderPartial("_app7020_course_player_header", array('containerId' => 'app7020-course-player-header'), false, true);
	}
}
?>
<div class="row-fluid">
	<div class="span12">

		<div class="player-launchpad-content">
			<div class="player-launchpad-empty <?= $player_bg['aspect'] ?>-image"<?= $style ?>>

				<? if (!empty($player_bg['src'])): ?>
				<!--[if lt IE 9]>
					<img class="ie8fix" src="<?php echo $player_bg['src']; ?>" />
					<script type="text/javascript">
						$(function(){
							$(window).on('resize', function(){
								// Center the image in its container
								var fix = $('.ie8fix');
								if(fix.length > 0){
									fix.css('margin-left', (fix.parent().width() - fix.width())/2);
									fix.css('margin-top', (fix.parent().height() - fix.height())/2);
								}
							}).trigger('resize');
						});
					</script>
				<![endif]-->
				<? endif; ?>

<?php if ($lo_data) : ?>
					<a id="common-launchpad-play-object" class="player-launchpad-big-blue-play" data-bootstro-id="bootstroPlayLo">
						<!-- Play -->
					</a>
<?php else : ?>
					<!-- <h2>Congratulations! You completed the course</h2>  -->
				<?php endif; ?>


				<div class="player-launchpad-header">
				<?php if ($lo_data) : ?>
					<h2><?php echo $lo_data['title']; ?></h2>
				<?php endif; ?>
				</div>

			</div>

		</div>

	</div>
</div>

