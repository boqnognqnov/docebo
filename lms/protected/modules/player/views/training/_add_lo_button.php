<?php
/**
 * @var $courseModel LearningCourse
 */

if ($this->userCanAdminCourse) : ?>
	<?php
		$typeClass = "dev-desktop-tablet";
		if ($courseModel->isMobile()) {
			$typeClass = "dev-smartphone";
		} 
	?>
	<div id="player-manage-add-lo-button" class="pull-right" data-bootstro-id="bootstroAddTrainingResources">
		<div class="dropdown">

			<!-- Toggle -->
			<i class="course-device-type devices-sprite <?= $typeClass ?>">&nbsp;</i>
			
			<a href="#" class="btn-docebo green big" data-toggle="dropdown">
				<span class="p-sprite plus-objects-white-green"></span> <?php echo Yii::t('organization', 'ADD TRAINING RESOURCES'); ?> <span class="p-sprite dropdown-objects-white-green"></span>
			</a>

			<ul class="dropdown-menu pull-right">
				<!-- HINT -->
				<li id="add-lo-type-hint" data-default-text="<?php echo Yii::t('organization', 'Select learning object type to add'); ?>">
					<span class="span12 text-left">
						<?php echo Yii::t('organization', 'Select learning object type to add'); ?>
					 </span>
				</li>
				<!-- LO Types -->
				<li>
					<span class="span3 add-lo-operation-type">
						<span><?php echo Yii::t('organization', 'ORGANIZE'); ?></span>
					</span>
					<span class="span9 add-lo-type-description"
						data-hint-icon-class="p-sprite new-folder"
						data-hint-text="<?php echo Yii::t('organization', 'Create new folder at the end of the course path'); ?>">

						<a id="create-folder" class="i-sprite-white p-hover" href="#"><span class="i-sprite is-add-folder"></span> <?php echo Yii::t('standard', '_NEW_FOLDER'); ?></a>
					</span>
				</li>
				<li>
					<span class="span3 add-lo-operation-type">
						<span><?php echo Yii::t('organization', 'UPLOAD'); ?></span>
					</span>
					<span class="span9 add-lo-type-description"
						data-hint-icon-class="p-sprite lo-scorm"
						data-hint-text="<?php echo Yii::t('organization', 'Enable end users to play SCORM content (1.2 and 2004).'); ?>">

						<a class="lo-upload i-sprite-white p-hover" data-lo-type="<?= LearningOrganization::OBJECT_TYPE_SCORMORG ?>" href="#"><span class="i-sprite is-zip"></span> <?php echo Yii::t('storage', '_LONAME_scormorg'); ?></a>
					</span>
				</li>
				<li>
					<span class="span3"></span>
					<span class="span9 add-lo-type-description"
						data-hint-icon-class="fa fa-archive fa-3x"
						data-hint-text="<?= Yii::t('organization', 'Upload your AICC/HAPC package') ?>">

						<a class="lo-upload" data-lo-type="<?= LearningOrganization::OBJECT_TYPE_AICC ?>" href="#"><span class="fa fa-archive"></span>&nbsp;&nbsp;AICC</a>
					</span>
				</li>
				
				<li>
					<span class="span3"></span>
					<span class="span9 add-lo-type-description"
						data-hint-icon-class="p-sprite lo-tin-can"
						data-hint-text="<?= Yii::t('organization', 'Enable your users to play with <strong>TIN CAN - EXPERIENCE API</strong> content.') ?>">

						<a class="lo-upload i-sprite-white p-hover" data-lo-type="<?= LearningOrganization::OBJECT_TYPE_TINCAN ?>" href="#"><span class="i-sprite is-tincan"></span> <?php echo Yii::t('standard', 'Tin Can'); ?></a>
					</span>
				</li>
				<li>
					<span class="span3"></span>
					<span class="span9 add-lo-type-description"
						data-hint-icon-class="p-sprite lo-video"
						data-hint-text="<?= Yii::t('organization', 'Enable your users to watch video resources in their course (MP4) or embed a video from YouTube, Vimeo, Wistia.') ?>">

						<a class="lo-upload i-sprite-white p-hover" data-lo-type="<?= LearningOrganization::OBJECT_TYPE_VIDEO ?>" href="#"><span class="i-sprite is-video"></span> <?php echo Yii::t('organization', 'Video'); ?></a>
					</span>
				</li>
				<li>
					<span class="span3"></span>
					<span class="span9 add-lo-type-description"
						data-hint-icon-class="p-sprite lo-file"
						data-hint-text="<?= Yii::t('organization', 'Enable your users to download any type of File locally.') ?>">

						<a class="lo-upload i-sprite-white p-hover" data-lo-type="<?= LearningOrganization::OBJECT_TYPE_FILE ?>" href="#"><span class="i-sprite is-file"></span> <?php echo Yii::t('standard', '_FILE'); ?></a>
					</span>
				</li>
				
				<?php if (!$courseModel->isMobile()) : ?>
				<li>
					<span class="span3"></span>
					<span class="span9 add-lo-type-description"
						data-hint-icon-class="p-sprite lo-video"
						data-hint-text="<?= Yii::t('deliverable', 'Enable your users to upload deliverables.') ?>">

						<a class="lo-create-deliverable i-sprite-white p-hover" data-lo-type="<?= LearningOrganization::OBJECT_TYPE_DELIVERABLE ?>" href="#">
							<span class="i-sprite is-deliverable"></span> <?php echo Yii::t('deliverable', '_LONAME_deliverable'); ?></a>
					</span>
				</li>
				<?php endif; ?>

				<?php
				// INSERTING POSSIBLE IMPORT TOOLS
				$importTitleShown = false;
				?>
<!--<!--                background-position-x: 817px;-->
<!--    background-position-y: -290px;-->
				<?php if (PluginManager::isPluginActive('ElucidatApp')) : ?>
                    <li>
                        <span class="span3 add-lo-operation-type">
                            <span><?=(!$importTitleShown)?Yii::t('standard', 'Import'):''?></span>
					</span>
						<span class="span9 add-lo-type-description"
                          data-hint-icon-class="p-sprite is-elucidat"
                          data-hint-text="<?= Yii::t('standard', 'Enable your users to play Elucidat course materials.') ?>">

						<a class="lo-create-elucidat i-sprite-white p-hover" data-lo-type="<?= LearningOrganization::OBJECT_TYPE_ELUCIDAT ?>" href="#">
                            <span class="i-sprite is-elucidat"></span> Elucidat
						</a>
					</span>
                    </li>
                <?php
					$importTitleShown = true;
					endif;
				?>

				<?php if (Yii::app()->user->getIsGodadmin()):?>
				<li>
					<span class="span3 add-lo-operation-type">
						<span><?=(!$importTitleShown)?Yii::t('standard', 'Import'):''?></span>
					</span>
					<span
						class="span9 add-lo-type-description"
						data-hint-icon-class="fa fa-hdd-o fa-4x"
						data-hint-text="<?= Yii::t('standard', 'Import training materials from Central Repository') ?>">

						<?php
						echo CHtml::link('<i class="fa fa-hdd-o fa-2x"></i>' . Yii::t('standard', 'Central Repository') . '<span class="pull-right new-label">NEW</span>', /*Docebo::createAdminUrl('centralrepo/centralRepo/pushToCourse')*/Docebo::createAdminUrl('centralrepo/CentralRepoPushLOToCourse/wizard', array(
							'skipCoursesStep' => true,
							'courseId' => $courseModel->idCourse,
							'courseType' => LearningCourse::TYPE_ELEARNING
						)), array(
							'class' => 'push_los_to_courses open-dialog lo-upload-centralrepo',
							'alt' => Yii::t('central_repo', 'Push training materials to courses'),
							'data-dialog-class' => 'lorepo-push-to-courses users-selector',
							'data-dialog-title' =>  Yii::t('course','Push training materials to courses'),
							'data-dialog-id' => 'push-lo-to-single-course',
							'removeOnClose' => 'true',
							'rel' => $wizardId,
							'data-lo-type' => LearningOrganization::OBJECT_TYPE_CENTRALREPO
						));
						?>
					</span>
				</li>
				<?php
					$importTitleShown = true;
					endif;
				?>

				<?php if (PluginManager::isPluginActive('GoogleDriveApp')) : ?>
					<li>
						<span class="span3 add-lo-operation-type">
							<span><?=(!$importTitleShown)?Yii::t('standard', 'Import'):''?></span>
						</span>
						<span class="span9 add-lo-type-description"
							  data-hint-icon-class="p-sprite is-googledrive"
							  data-hint-text="<?= Yii::t('googledrive', 'Enable your users to view Google Drive documents.') ?>">

						<a class="lo-create-googledrive i-sprite-white p-hover" data-lo-type="<?= LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE ?>" href="#">
							<span class="i-sprite is-googledrive"></span> <?=Yii::t('google_drive', 'Google Drive')?><span class="pull-right new-label">NEW</span>
						</a>
					</span>
					</li>
				<?php
					$importTitleShown = true;
					endif;
				?>

				<?php if (PluginManager::isPluginActive('LtiApp')) : ?>
					<li>
						<span class="span3 add-lo-operation-type">
							<span><?=(!$importTitleShown)?Yii::t('standard', 'Import'):''?></span>
						</span>
						<span class="span9 add-lo-type-description"
							  data-hint-icon-class="p-sprite is-lti"
							  data-hint-text="<?= Yii::t('lti', 'Enable your users to view LTI resources.') ?>">

						<a class="lo-create-lti i-sprite-white p-hover" data-lo-type="<?= LearningOrganization::OBJECT_TYPE_LTI ?>" href="#">
							<span class="i-sprite is-lti"></span> LTI<span class="pull-right new-label">NEW</span>
						</a>
					</span>
					</li>
					<?php
					$importTitleShown = true;
					endif;
				?>

				<?php if (!$courseModel->isMobile()) : ?>
				<li>
					<span class="span3 add-lo-operation-type">
						<span><?= Yii::t('standard', '_CREATE') ?></span>
					</span>
					<span class="span9 add-lo-type-description"
						data-hint-icon-class="p-sprite lo-doc-converter"
						data-hint-text="<?= Yii::t('authoring', 'Convert your Power Point presentations and PDFs documents in a proper Learning Object.') ?>">

						<a class="lo-authoring i-sprite-white p-hover" href="#">
							<span class="i-sprite is-converter"></span> <?php echo Yii::t('authoring', 'Slides converter'); ?>
						</a>
					</span>
				</li>
				<?php endif; ?>
				
				
				<li>
					<span class="span3 add-lo-operation-type">
						<?php if ($courseModel->isMobile()) : ?>
							<span><?= Yii::t('standard', '_CREATE') ?></span>
						<?php endif; ?>
					</span>
					<span class="span9 add-lo-type-description"
						data-hint-icon-class="p-sprite lo-html"
						data-hint-text="<?= Yii::t('organization', 'Create HTML Page using our inline editing tool'); ?>">

						<a class="lo-create-htmlpage i-sprite-white p-hover" data-lo-type='<?= LearningOrganization::OBJECT_TYPE_HTMLPAGE ?>' href="#"><span class="i-sprite is-htmlpage"></span> <?php echo Yii::t('storage', '_LONAME_htmlpage'); ?></a>
					</span>
				</li>
				
				<?php if (!$courseModel->isMobile()) : ?>
				<li>
					<span class="span3"></span>
					<span class="span9 add-lo-type-description"
						data-hint-icon-class="p-sprite lo-poll"
						data-hint-text="<?= Yii::t('organization', 'Create a Poll'); ?>">

						<a class="lo-create-poll i-sprite-white p-hover" data-lo-type='<?= LearningOrganization::OBJECT_TYPE_POLL ?>' href="#"><span class="i-sprite is-poll"></span> <?php echo Yii::t('storage', '_LONAME_poll'); ?></a>
					</span>
				</li>
				<?php endif; ?>
				
				
				<li>
					<span class="span3"></span>
					<span class="span9 add-lo-type-description"
						data-hint-icon-class="p-sprite lo-test"
						data-hint-text="<?= Yii::t('organization', 'Create your own test assessment, using the Docebo built-in assessment creation tool!'); ?>">

						<a class="lo-create-test i-sprite-white p-hover" data-lo-type='<?= LearningOrganization::OBJECT_TYPE_TEST ?>' href="#"><span class="i-sprite is-test"></span> <?php echo Yii::t('storage', '_LONAME_test'); ?></a>
					</span>
				</li>
			</ul>
		</div>
	</div>
<?php endif; ?>