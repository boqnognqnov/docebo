<?php
	/* @var $showHints string */

	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($assetsBaseUrl . '/js/video_subtitles.js');

	$csrfToken = Yii::app()->request->csrfToken;
	$uploadUrl = Docebo::createLmsUrl('site/AxUploadFile', array('for_subtitle' => 1));
	$flashUrl = Yii::app()->plupload->getAssetsUrl() . "/plupload.flash.swf";


	$idVideo = ($loModel instanceof LearningOrganization) ? $loModel->idResource : false;
	echo CHtml::hiddenField('idVideoRes', $idVideo);

	$baseDeleteUrl = Docebo::createLmsUrl('player/training/axDeleteSubtitle', array('course_id' => $_REQUEST['course_id']));
	$baseFallbackSetUrl = Docebo::createLmsUrl('player/training/axSubtitleToggleFallback',
		array(
			'course_id' => $_REQUEST['course_id'],
			'idVideo' => $idVideo,
		)
	);

	$fallbackSubId = "";
	$showLanguages = array();
	$existingLanguages = array();
	foreach ($videoSubtitles as $videoSubtitle) {
		$existingLanguages[$videoSubtitle->lang_code] = $videoSubtitle->lang_code;
		if ($videoSubtitle->fallback == 1) {
			$fallbackSubId = $videoSubtitle->id;
		}
	}

	$showLanguages = array_diff_key($activeLanguagesList, $existingLanguages);
?>

<div class="row-fluid">
	<div class="span3 assign-vtt-file-title"><?= Yii::t('player', 'Assign VTT subtitles file for each available language'); ?></div>
	<div class="span9">
		<div class="row-fluid">
			<div class="languagesList span5">
				<?= Yii::t('player', 'Language') ; ?>
				<br />
				<?php echo $form->dropDownList(LearningVideoSubtitles::model(), 'lang_code', $showLanguages, array('style' => '')); ?>

				<div class="orgChart_languages">
					<div><?php echo Yii::t('admin_lang', '_LANG_ALL').': <strong>' . count($activeLanguagesList).'</strong>'; ?></div>
					<div id="orgChart_assignedCount"><?php echo Yii::t('player', 'Assigned files based on language/s').
							': <strong class="added-subs-count green-text">'.((!empty($videoSubtitles)) ? count(array_filter($videoSubtitles)) : 0).'</strong>'; ?>
					</div>
				</div>
			</div>
			<div class="span3">
				<?= Yii::t('player', 'VTT Subtitles'); ?>
				<br />
				<div id="pluploader-wrapper">
					<div class="pluploader-wrapper-inner">
						<div id="pluploader-container">
							<a class="btn-docebo green big" id="pickfiles2" href="javascript:;" data-bootstro-id="bootstroAddSubtitle">
								<?php echo Yii::t('course_management', 'CHOOSE FILE'); ?>
							</a>
							<?php

							echo CHtml::hiddenField('type', FileTypeValidator::TYPE_SUBTITLES);

							echo CHtml::hiddenField('fallbackSub', $fallbackSubId);
							?>
						</div>
					</div>
				</div>

				<?php
				if ($showHints) {
					?>
				<div class="popover bootstro fade left in" id="popup_bootstroAddSubtitle" style="width: 370px; max-width: 450px; top: 90px; left: auto; right: 0; display: block;">
					<div class="arrow"></div>
					<div class="popover-inner">
						<h3 class="popover-title"></h3>
						<div class="popover-content">
							<div style="text-align: left; margin-left: 80px; padding-top: 10px; font-size: 26px; line-height: 26px; color: #999;">
								<span class="text-colored green" style="font-weight: bold; font-size: 30px; letter-spacing: 1px;"><?= Yii::t('player', 'Start here!'); ?></span><br />
								<?= Yii::t('player', 'Choose a VTT file to<br /> associate for each<br /> available language'); ?>
							</div>

							<div class="bootstro-arrow arrow-e4" style="left: 0; right: auto;"></div>
						</div>
					</div>
				</div>
				<?php
				}
				?>
			</div>
			<div class="span4 assign-vtt-file-notes">
				&nbsp;<br />
				<?= Yii::t('player', 'VTT subtitles files only are accepted. To know more about VTT format, please'); ?> <u><a href="https://en.wikipedia.org/wiki/WebVTT" target="_blank" class="blue-text"><?= Yii::t('player', 'visit this link.'); ?></a></u>
			</div>
		</div>

		<br />
		<br />

		<div class="row-fluid subtitles-list">
			<?php $headerRow = '
			<div class="row-fluid subtitles-list-titles '. ((count($videoSubtitles) == 0) ? "hidden" : "") .'" >
				<div class="span3">'. Yii::t('standard', '_LANGUAGE') . '</div>
				<div class="span5">'. Yii::t('player', 'Subtitles File') . '</div>
				<div class="span3">'. Yii::t('player', 'Fallback track') . '&nbsp;
					<a class="t"
                         style="display: inline-block"
                         data-toggle="tooltip"
                         rel="tooltip"
                         data-placement="top"
                         data-delay=\'{"show":"0", "hide":"3000"}\'
                         title=\'<span class="tooltip-text">'. Yii::t('player', 'The fallback track is used when there are no subtitles available for the user language')
				.'</span>\'
                         data-html="true">
						<i class="fa fa-question-circle"></i>
					</a>
				</div>
				<div class="span1">&nbsp;</div>
			</div>
';

			echo $headerRow;
			?>
			<?php if(count($videoSubtitles) > 0) { ?>
				<?php foreach($videoSubtitles as $videoSubtitle) { ?>
			<div class="row-fluid subtitles-list-row" id="subtitles-list-row-<?= $videoSubtitle->lang_code; ?>"
			     data-filter-value="<?=$videoSubtitle->lang_code; ?>"  data-filter-text="<?=$activeLanguagesList[$videoSubtitle->lang_code]; ?>"
				 data-real-id="<?=$videoSubtitle->id ?>">
				<div class="span3 subtitle-language-text"><?= str_replace('_', ' ', $videoSubtitle->lang_code); ?></div>
				<div class="span5"><?php $currentAsset = CoreAsset::model()->findByPk($videoSubtitle->asset_id); echo $currentAsset->original_filename; ?></div>
				<div class="span3 subtitle-set-fallback"><i data-sub-id="<?= $videoSubtitle->id; ?>" class="fa fa-check-circle-o fa-lg<?= ($videoSubtitle->fallback == 1) ? ' green-text' : ''; ?>"></i></div>
				<div class="span1 subtitle-delete">
					<a href="<?=$baseDeleteUrl; ?>&id=<?=$videoSubtitle->id ?>&langcode=<?= $videoSubtitle->lang_code; ?>"
					   class="open-dialog delete-sub-link" data-dialog-id="delete-sub-dialog" data-dialog-class="delete-item-modal sub-delete-modal"
					   closeOnEscape="true" closeOnOverlayClick="true" data-dialog-title="<?= Yii::t('player', 'Delete Subtitle') ; ?>">
						<i class="fa fa-times fa-lg red-text"></i>
					</a>
				</div>
			</div>
				<?php } ?>
			<?php } ?>
		</div>
	</div>
</div>

<script type="text/javascript">
//<![CDATA[
	$(function(){

		// pluploader specific set of options
		var pluploaderOptions = {
			unique_names		: true,
//			chunk_size			: '1mb',
			multi_selection		: false,
			runtimes			: 'html5,flash',
			browse_button		: 'pickfiles2',
			container			: 'pluploader-container',
			drop_element		: 'pluploader-dropzone',
			max_file_size		: '4mb',
			url					: <?= json_encode($uploadUrl) ?>,
			multipart			: true,
			multipart_params	: {YII_CSRF_TOKEN: <?= json_encode($csrfToken) ?>},
			flash_swf_url 		: <?= json_encode($flashUrl) ?>,
			//allowedExtensions	: 'vtt,srt',
			'baseDeleteUrl'     : '<?= $baseDeleteUrl ?>',
			'baseFallbackSetUrl': '<?= $baseFallbackSetUrl ?>',
		};

		// Overall Substitles JS options
		var options = {
			debug: true,
			pluploaderOptions: pluploaderOptions
		};

		// Create Subtitles JS object
		var SubtitlesManagement = new SubtitlesManagementClass(options);


		// ---

		$('.t').tooltip({'html': 'true', delay: { "show": 0, "hide": 1000}});

		// Hide the Bootstro hint(arrow + text), if there are
		$('a[data-toggle="tab"]').on('shown', function (e) {

			// Because of IE super-brpwsers!!!! (re-render the PLuploader  button.. it was hidden until now.. )
			SubtitlesManagement.pl_uploader.refresh();


			var newTab = e.target; // activated tab
			var newTabAnchor = newTab.getAttribute("href"); // activated tab

			$('#pickfiles2').on('click', function() {
//				if (newTabAnchor == "#lo-video-subtitles") {
					if ($('#popup_bootstroAddSubtitle').length > 0) {
						$('#popup_bootstroAddSubtitle').fadeOut(2000);
					}
//				}
			});
		});

		$(document).controls();

		$(document).on('dialog2.closed', '.modal', function(e) {
		});
	});
//]]>
</script>