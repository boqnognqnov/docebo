<?php
/**
 * @var $lo_type string
 * @var $resVideo LearningVideo
 * @var $loModel LearningOrganization
 */
	$form = $this->beginWidget('CActiveForm', array(
			'action' => $this->createUrl('/' . $lo_type . '/default/axSaveLo'),
			'method' => 'post',
			'htmlOptions' => array(
				'id' => 'upload-lo-form',
 				'class' => 'form-horizontal',
			),
	));

	$uploadingMessage = Yii::t('standard', 'This operation can take up to 2 minutes depending on the size of your file.');
	$uploadingImageFileName = 'cloud_upload.jpg';
	if ($lo_type == LearningOrganization::OBJECT_TYPE_VIDEO) {
		$dummyStorage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_VIDEO);
		if ($dummyStorage->transcodeVideo) {
			$uploadingMessage 	= Yii::t('standard', 'Your video is being converted into web and mobile formats. You will see an icon until the conversion process is complete.');
			$uploadingImageFileName = 'converting_video.jpg';
		}
	}

	$xapi_content_url_id = "";
	$title = "";
	$description = "";
	if ($loModel) {
	    $title = $loModel->title;
	    if ($loModel->use_xapi_content_url) {
	        $tincanActivity = $loModel->tincanMainActivity;
	        $xapi_content_url_id = CoreSettingTincanUrl::getIdByUrl($tincanActivity->launch);
	        $title = $tincanActivity->name;
	        $description = $tincanActivity->description;
	    }
	}


?>
<div id="player-arena-uploader-panel" class="<?php echo Yii::app()->getLanguage(); ?> player-arena-editor-panel">
	<div class="header">
		<?php echo Yii::t('organization', 'Upload File'); ?>
	</div>

	<div id="upload-stuff-message" class="" style="display: none ;">
		<div class="uploading-message">
			<div>
				<h4><?= Yii::t('standard', 'Uploading your stuffs to the clouds') ?></h4>
				<p>
					<?= $uploadingMessage ?>
				</p>
			</div>
		</div>
		<div class="uploading-image">
			<?php echo CHtml::image(Yii::app()->theme->baseUrl . "/images/" . $uploadingImageFileName); ?>
		</div>
	</div>

	<div class="content" id="player-arena-uploader">
		<div>
			<div class="tabbable"> <!-- Only required for left/right tabs -->
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab1" data-toggle="tab"><?=LearningOrganization::objectTypeValue($lo_type)?></a></li>
						<li><a href="#lo-additional-information" data-toggle="tab"><?=Yii::t('myactivities', 'Additional info')?></a></li>
					<?php if ($lo_type == LearningOrganization::OBJECT_TYPE_VIDEO) : ?>
						<li id="video-subtitles" class="<?= $resVideo && $resVideo->type !== LearningVideo::VIDEO_TYPE_UPLOAD ? 'hidden' : '' ?>" ><a href="#lo-video-subtitles" data-toggle="tab"><?=Yii::t('myactivities', 'Subtitles')?></a></li>
					<?php endif; ?>
					<?php if ($lo_type == LearningOrganization::OBJECT_TYPE_TINCAN) : ?>
						<li><a href="#enhanced-oauth-security" data-toggle="tab"><?=Yii::t('myactivities', 'Enhanced Security')?></a></li>
					<?php endif; ?>
					<?php
						// Raise event to let plugins add new tabs
						Yii::app()->event->raise('RenderEditLoTabs', new DEvent($this, array(
							'courseModel' => $courseModel,
							'loModel' => $loModel
						)));
					?>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tab1">
						<div id="player-uploader-wrapper" class="player-arena-uploader">

							<?php if($lo_type === LearningOrganization::OBJECT_TYPE_VIDEO): ?>
								<div class="control-group">
									<?php echo CHtml::label(Yii::t('standard', 'Video source'), 'video-source', array(
										'class' => 'control-label'
									)); ?>
									<div class="controls">
										<?php
											$selected = 0;
											if(isset($resVideo->type) && $resVideo->type !== LearningVideo::VIDEO_TYPE_UPLOAD){
												$selected = 1;
											}
										?>
										<?= CHtml::radioButtonList('video-source', $selected, array(
											0 => Yii::t('standard', 'Upload file'),
											1 => Yii::t('standard', 'Embed video URL').'&nbsp&nbsp<i class="fa fa-youtube fa-2x" style="color: red" aria-hidden="true"></i>&nbsp&nbsp<i class="fa fa-vimeo fa-2x" style="color: #26bbeb" aria-hidden="true"></i>&nbsp&nbsp<img class="wistia-icon" src="' . Yii::app()->theme->baseUrl . '/img/wistia.png' . '">'
										), array(
											'id' => 'video-source',
											'separator' => '',
											'labelOptions' => array(
												'style' => 'display: inline-block;'
											)
										)) ?>
									</div>
								</div>
								<div id="video-url" class="<?= $resVideo && $resVideo->type !== LearningVideo::VIDEO_TYPE_UPLOAD ? '' : 'hidden' ?>">
									<div class="control-group">
										<div class="controls">
											<?= CHtml::textField('video_url', urldecode($resVideo->url), array(
												'placeholder' => 'https://www.youtube.com/watch?v=7Uuwjlu-5S4'
											)) ?>
											<span class="url-valudator-info"></span>
											<p class="muted"><?= Yii::t('standard', 'Paste URL of the video training material') ?></p>
										</div>
									</div>
								</div>
							<?php endif; ?>

							<div id="upload-file" class="<?= (!$resVideo || $resVideo->type === LearningVideo::VIDEO_TYPE_UPLOAD ? '' : 'hidden') ?>">

								<div class="control-group">
									<?php echo CHtml::label(Yii::t('authoring', 'Select files'), 'pickfiles', array(
										'class' => 'control-label',
                                        'style' => 'cursor: auto'
									)); ?>
									<div class="controls">

										<div id="player-pl-uploader-dropzone">
											<div id="player-pl-uploader">

												<div class="upload-button">
													<a class="btn-docebo green big" id="pickfiles" href="javascript:;">
														<?php echo Yii::t('organization', 'Upload File'); ?>
													</a>
													<?php if ($resource) :?>
														&nbsp;&nbsp;
														<span class="uploaded-fileinfo previous-uploaded-file">
														<?php
												            if($resVideo->type === LearningVideo::VIDEO_TYPE_UPLOAD){
                                                                echo Yii::t('standard', '_CURRENT_FILE') . ': <span class="uploaded-filename">'
														              . Docebo::cleanFilename($resource['filename']). '</span>';
												            }
												        ?>
														</span>
													<?php endif; ?>
												</div>
												<div class="upload-progress">
													<div class="uploaded-fileinfo">
														<?php echo Yii::t('standard', 'Uploading') . ': <span class="uploaded-filename" id="uploaded-filename"></span><span id="uploaded-filesize"></span>'; ?>
													</div>
													<div class="row-fluid">
														<div class="span11">
															<div class="docebo-progress progress progress-striped active">
																<div id="uploader-progress-bar" class="bar full-height" style="width: <?= ($resource ? '100' : '0') ?>%;"></div>
															</div>
														</div>
														<div class="span1 player-uploaded-percent">
															<span id="player-uploaded-percent-gauge"><?= ($resource ? '100%' : '') ?></span>
															<a href="#" id="player-uploaded-cancel-upload"><i class="icon-remove"></i></a>
														</div>
													</div>
												</div>

												<?php
												// when editing a video/file, id_org will be set
												echo CHtml::hiddenField('id_org', $resource ? $resource['id_org'] : '', array('id' => 'file-id-org'));
												// the following hidden input makes sense when editing a video/file
												// will be set to '1' if the user has uploaded a new file, thus overwriting the old file
												echo CHtml::hiddenField('new_upload', 0, array('id' => 'file-new-upload'));
												?>
											</div>
										</div>

									</div>
								</div>
							</div>
							<!-- /end upload -->



							<?php if ($requestTitle): ?>
								<div class="control-group">
									<?php echo CHtml::label(Yii::t('standard', '_TITLE'), 'file-title', array(
										'class' => 'control-label'
									)); ?>
									<div class="controls">
										<?php echo CHtml::textField('file-title', ($resource ? $resource['title'] : ''), array('id' => 'file-title', 'class' => 'input-block-level')); ?>
									</div>
								</div>
							<?php endif; ?>

							<?php if ($lo_type == LearningOrganization::OBJECT_TYPE_SCORMORG && $resource && $loModel): ?>
								<div class="control-group">
									<?php echo CHtml::label(Yii::t('standard', '_TITLE'), 'new-scorm-title', array(
										'class' => 'control-label'
									)); ?>
									<div class="controls">
										<?php echo CHtml::textField('new-scorm-title', $loModel->title, array('id' => 'new-scorm-title', 'class' => 'input-block-level')); ?>
									</div>
								</div>
							<?php endif; ?>

							<?php if ($lo_type == LearningOrganization::OBJECT_TYPE_TINCAN && $resource && $loModel): ?>
								<div class="control-group">
									<?php echo CHtml::label(Yii::t('standard', '_TITLE'), 'new-tincan-title', array(
										'class' => 'control-label'
									)); ?>
									<div class="controls">
										<?php echo CHtml::textField('new-tincan-title', $loModel->title, array('id' => 'new-tincan-title', 'class' => 'input-block-level')); ?>
									</div>
								</div>
							<?php endif; ?>

							<?php if ($lo_type == LearningOrganization::OBJECT_TYPE_AICC && $resource && $loModel): ?>
								<div class="control-group">
									<?php echo CHtml::label(Yii::t('standard', '_TITLE'), 'new-aicc-title', array(
										'class' => 'control-label'
									)); ?>
									<div class="controls">
										<?php echo CHtml::textField('new-aicc-title', $loModel->title, array('id' => 'new-aicc-title', 'class' => 'input-block-level')); ?>
									</div>
								</div>
							<?php endif; ?>
							<?php if ($requestDescription): ?>
								<div class="control-group">
									<?php echo CHtml::label(Yii::t('standard', '_DESCRIPTION'), 'file-description', array(
										'class' => 'control-label'
									)); ?>
									<div class="controls">
										<!-- <textarea id="file-description<?= ($resource ? $resource['id_org'] : '') ?>" -->
										<textarea id="file-description"
										          name="description"
										          class="input-block-level squared"><?= ($resource ? $resource['description'] : '') ?></textarea>

									</div>

								</div>
							<?php endif; ?>
                            <?php if ( $lo_type == 'video') { ?>
								<div id="video-seekable" class="<?= $resVideo && $resVideo->type !== LearningVideo::VIDEO_TYPE_UPLOAD ? 'hidden' : '' ?>">
									<div class="control-group">
										<?php echo CHtml::label(Yii::t('course', 'Allow users to move through the video by dragging the playhead'), 'seekable', array(
											'class' => 'control-label'
										)); ?>
										<div class="controls">
											<?= CHtml::radioButtonList('seekable', $resVideo ? $resVideo->seekable : LearningVideo::VIDEO_SEEKABLE_YES, array(
												LearningVideo::VIDEO_SEEKABLE_NO => Yii::t('standard', 'No').'<br />',
												LearningVideo::VIDEO_SEEKABLE_YES => Yii::t('standard', 'Yes').'<br />',
												LearningVideo::VIDEO_SEEKABLE_AFTER_COMPLETE => Yii::t('standard', 'After completion')
											), array(
												'id' => 'seekable',
												'labelOptions' => array(
													'style' => 'display: inline-block;'
												)
											)) ?>
										</div>
									</div>
								</div>
                            <?php } ?>


						    <?php if ($lo_type === LearningOrganization::OBJECT_TYPE_TINCAN) : ?>
                            	<div class="control-group">
									<?php
                                        echo CHtml::label(Yii::t('standard', 'From URL'), 'use-xapi-content-url', array(
                                            'class' => 'control-label'
                            	        ));
                                    ?>
                            		<div class="controls">
                            			<?php echo CHtml::checkBox('use_xapi_content_url', $loModel ? $loModel->use_xapi_content_url : false, array('id' => 'use-xapi-content-url', 'class' => 'input-block-level')); ?>
                            		</div>
                            	</div>

								<div id="xapi-content-from-url-settings">
                                	<div class="control-group">
                                		<?= CHtml::label(Yii::t('standard', 'Select URL'), 'xapi-content-url', array('class' => 'control-label')); ?>
                                		<div class="controls">
                                			<?php echo CHtml::dropDownList('xapi_content_url', $xapi_content_url_id, LearningOrganization::getTinCanPredefinedUrs(), array(
                                			    'class'  => 'span12',
                                			    'prompt' => Yii::t('standard', 'Select URL'),
                                			)); ?>
											<div class="setting-description"><?php echo Yii::t('admin', 'You can configure predefined URLs in Admin &gt; Advanced Settings &gt; E-Learning'); ?></div>
                                		</div>

									</div>
									<div class="control-group">
										<?= CHtml::label(Yii::t('standard', 'Title'), 'xapi-content-url-title', array('class' => 'control-label')); ?>
                                		<div class="controls">
                                			<?php echo CHtml::textField('xapi_content_url_title', $title, array('class' => 'span12')); ?>
                                		</div>
									</div>
									<div class="control-group">
										<?= CHtml::label(Yii::t('standard', 'Description'), 'xapi-content-url-description', array('class' => 'control-label')); ?>
                                		<div class="controls">
                                			<?php echo CHtml::textField('xapi_content_url_description', $description, array('class' => 'span12')); ?>
                                			<div class="setting-description"><?php echo Yii::t('admin', 'This is the generated activity description. See Additional Info tab!'); ?></div>
                                		</div>
									</div>
								</div>
                            <?php endif; ?>


							<?php if ($lo_type == LearningOrganization::OBJECT_TYPE_SCORMORG) : ?>
								<?php
								$options['desktop_openmode'] 	= $loModel->desktop_openmode;
								$options['tablet_openmode'] 	= $loModel->tablet_openmode;
								$options['smartphone_openmode'] = $loModel->smartphone_openmode;

								$this->renderPartial('_upload_options_scorm', array(
									'options' 		=> $options,
									'courseModel' 	=> $courseModel,
								));
								?>
							<?php elseif($lo_type == LearningOrganization::OBJECT_TYPE_AICC) : ?>
								<?php
								// Same options, different model
								$options['desktop_openmode'] 	= $loModel->desktop_openmode;
								$options['tablet_openmode'] 	= $loModel->tablet_openmode;
								$options['smartphone_openmode'] = $loModel->smartphone_openmode;

								$this->renderPartial('_upload_options_aicc', array(
									'options' 		=> $options,
									'courseModel' 	=> $courseModel,
								));
								?>
							<?php endif; ?>

							<br>

						</div>
					</div>
					<div class="tab-pane" id="lo-additional-information">
						<? $this->widget('common.widgets.PlayerLayoutAdditionalInfo', array(
							'loModel'=> $loModel,
							'idCourse' => $courseModel->idCourse,
						)); ?>
					</div>

					<?php if ($lo_type == LearningOrganization::OBJECT_TYPE_TINCAN) : ?>
						<div class="tab-pane" id="enhanced-oauth-security" style="min-height: 300px;">
							<?php
                                $this->renderPartial('_enhanced_oauth_security', array(
                                    'loModel'   => $loModel,
                                    'resource'  => $resource,
                                ), false, true);
                            ?>
						</div>
					<?php endif; ?>

					
					<?php if ($lo_type == LearningOrganization::OBJECT_TYPE_VIDEO) : ?>
						<div class="tab-pane" id="lo-video-subtitles">
							<?php
								$this->renderPartial('_edit_video_subtitles', array(
									'assetsBaseUrl'	        => $assetsBaseUrl,
									'form'                  => $form,
									'activeLanguagesList'   => $activeLanguagesList,
									'showHints'             => $showHints,
									'videoSubtitles'        => $videoSubtitles,
									'loModel'               => $loModel,
								), false, true);
							?>
						</div>
					<?php endif; ?>

					<?php
					// Raise event to let plugins add new tab contents
					Yii::app()->event->raise('RenderEditLoTabsContent', new DEvent($this, array(
						'courseModel' => $courseModel,
						'loModel' => $loModel
					)));
					?>

				</div>
			</div>

			<div class="text-right">

				<input type="submit" id="player-save-uploaded-lo"
				       class="btn-docebo green big" value="<?php echo Yii::t('standard', '_SAVE'); ?>">&nbsp;&nbsp;
				<a id="player-cancel-uploaded-lo" href="#"
				   class="btn-docebo black big"><?php echo Yii::t('standard', '_CANCEL'); ?></a>

			</div>

		</div>

	</div>


</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    var mceHeight = 200;
	$(function () {
        '<?=$lo_type?>' == 'video' ? mceHeight = 100 : mceHeight;
		//TinyMce.attach('textarea#file-description<?= ($resource ? $resource['id_org'] : '') ?>', {height: 200});
		TinyMce.attach('#file-description', {height: mceHeight});
		//Arena.uploader.run();

		// Styler, I hate it, but....
		$('.scorm-upload-options input[type="radio"]').styler();

		$('#lo-additional-information input').styler();
		$('input[name="seekable"]').styler();
		$('input[name=video-source]').styler();
	});

</script>


