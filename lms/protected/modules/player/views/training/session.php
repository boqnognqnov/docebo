<!-- Arena -->
<div id="player-container">
	<?php
	// Event raised for YnY app to hook on to
	Yii::app()->event->raise('BeforeRenderSessionDetails', new DEvent($this, array('courseModel' => $courseModel, 'courseSessionModel' => $courseSessionModel)));

	if ($courseSessionModel) {
		$this->widget('plugin.ClassroomApp.widgets.SessionInfo', array(
			'id' => 'session-info',
			'sessionId' => $session_id,
			'courseModel' => $courseModel
		));
	} else {
		$this->widget('plugin.ClassroomApp.widgets.SessionManagement', array(
			'id' => 'session-management',
			'courseModel' => $courseModel
		));
	} ?>

	<div class="clearfix"></div>

	<!-- Blocks header -->
	<!-- Mobile course: disable widgets -->
	<?php if (!$courseModel->isMobile()) : ?>
		<div id="player-blocks-nav-header-container">
			<?php
			$this->renderPartial('_blocks_header', array(
				'courseModel' => $courseModel,
				'courseSessionModel' => $courseSessionModel
			));
			?>
		</div>
		<div class="clearfix"></div>

		<!-- Blocks -->
		<div id="player-blocks-container">
			<?=$this->renderPartial('_social_rating', array('courseModel' => $courseModel,'deepLink'=>$deepLink))?>
			<!-- This DIV will be loaded by blocks grid -->
			<div id="player-blocks-grid"></div>
		</div>
	<?php endif; ?>


	<div class="clearfix"></div>
</div>