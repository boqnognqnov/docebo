<div id="social_rating_course">
	<?php if ($courseModel->canViewRating()) : ?>
		<div id="social_rating_course_left">
			<?php
			echo CHtml::label(Yii::t('social_rating', 'Rate this course'), '', array('class' => 'social_rate_title pull-left'));
			$this->widget('common.widgets.DoceboStarRating',array(
				'id'=>'course-rating-'.$courseModel->idCourse,
				'name'=>'course_rating_'.$courseModel->idCourse,
				'type' => 'big-star',
				'readOnly' => !$courseModel->canRate(Yii::app()->user->id),
				'value' => $courseModel->getRating(),
				'ratingUrl' => Docebo::createAppUrl('lms:course/axRate'),
				'urlParams' => array(
					'idCourse' => $courseModel->idCourse
				),
				'id_course' => $courseModel->idCourse,
				'id_user' => Yii::app()->user->id
			));
			?>
			<span class="rating-value"><?= ($courseModel->learningCourseRating ? $courseModel->learningCourseRating->rate_average : 0)?></span>
		</div>
	<?php endif; ?>
    <?php if($courseModel->deep_link) {
    ?>
    <i class="deep_link_option fa fa-link" data-original-title="<?=Yii::t('course','Get sharable link');?>" style="cursor: pointer; color:white; font-size:22px;padding-left:8px;padding-top:8px; margin-left:35px; width: 29px; height: 29px; float: right;background-color: black; border-radius:5px;"></i>
    <div class="input-group deep_link_field" style="display:none;float:right;padding-top:2px;height: 30px;">
        <input style="margin-top:0px; width: 300px; height:20px !important;" id="copy-example" type="text" class="form-control" readonly="readonly"
               value="<?= $deepLink; ?>"/>
        <input rel="" data-original-title="<?=Yii::t('course','Copied to Clipboard!');?>" id="copy_button"
               data-clipboard-target="#copy-example"
               style="line-height:16px; margin-top:0px; background-color: black !important; margin-left: -4px; padding-top: 7px; vertical-align: top !important; min-width: 0;"
               class="btn btn-docebo black big" type="button"
               value="<?php echo Yii::t('coursepath', 'Copy'); ?>"/>
    </div>
        <script type="text/javascript">
            $(document).ready(function(){
				if($('#social_rating_course_right').length > 0){
					$('.deep_link_field').css('height', '40px');
				}
                $('.deep_link_option').tooltip();
                $('.deep_link_option').on('click',function(){
                    var linkContainer = $('.deep_link_field');
                    var otherShareLinks = $('.social_share_link, .social_share_title');
                    if(linkContainer.css('display')=='none'){
                        linkContainer.css('display', 'block');
                        otherShareLinks.css('display', 'none');
                    }else{
                        linkContainer.css('display', 'none');
                        otherShareLinks.css('display', 'block');
                    }
                });
                var clipboard = new Clipboard('#copy_button.btn');
                var copy_button = $('#copy_button');
                var options = {
                    'trigger' : 'click'
                };

                clipboard.on('success', function(e) {
                    copy_button.tooltip(options);
                    copy_button.tooltip('show');
                    $(document).off('mouseleave','#copy_button');
                    copy_button.on('mouseleave', function() {
                        copy_button.tooltip('hide');
                    });
                    e.clearSelection();
                });
                $('.deep_link_option, #copy_button').on('shown.bs.tooltip', function () {
                    $('#social_rating_course .popover.right.in').remove();
                });
                $('#social_rating_course_right').css('width','42%');
            });
        </script>
    <?php }?>
	<?php if ($courseModel->canShare(Yii::app()->user->id)) : ?>
		<div id="social_rating_course_right">
            <?php
			if ($courseModel->course_share_google == AdvancedSettingsSocialRatingForm::SETTING_ON)
				echo CHtml::label('', '', array('class' => 'social_google_link social_share_link'));
			if ($courseModel->course_share_linkedin == AdvancedSettingsSocialRatingForm::SETTING_ON)
				echo CHtml::link('', 'javascript:;', array('class' => 'social_linkedin_link social_share_link'));
			if ($courseModel->course_share_twitter == AdvancedSettingsSocialRatingForm::SETTING_ON)
				echo CHtml::label('', '', array('class' => 'social_twitter_link social_share_link'));
			if ($courseModel->course_share_facebook == AdvancedSettingsSocialRatingForm::SETTING_ON)
				echo CHtml::link('', 'javascript:fbShare()', array('class' => 'social_facebook_link social_share_link'));

			$score = ($courseModel->course_user_share_permission == AdvancedSettingsSocialRatingForm::SETTING_ON) ? LearningCourseuser::getLastScoreByUserAndCourse($courseModel->idCourse, Yii::app()->user->id, true) : '';
			$scoreLabel = '';
			if (isset($score['hasScore']) && $score['hasScore'])
				$scoreLabel = Yii::t('social_rating', 'My score').': '.$score['maxScore'].'! ';

			echo CHtml::label(Yii::t('social_rating', 'Share this course').'!', '', array('class' => 'social_share_title pull-right'));
			?>
		</div>

		<script type="text/javascript">

			//escaped, otherwise & symbol is not sent (with symbols after it)
			var courseUrl = encodeURIComponent('<?=Docebo::createAbsoluteLmsUrl('course/details', array('id' => $courseModel->idCourse))?>');
			var courseImage = '';
			var description = '<?= addslashes(preg_replace('~[\r\n]+~', '', strip_tags(nl2br($courseModel->description))));?>';
			var descriptionTwitter = '<?= addslashes((strlen(preg_replace('~[\r\n]+~', '', strip_tags(nl2br($courseModel->description))))>50)  ? (substr(preg_replace('~[\r\n]+~', '', strip_tags(nl2br($courseModel->description))),0 ,50).'...') : (preg_replace('~[\r\n]+~', '', strip_tags(nl2br($courseModel->description)))));?>';
			var descriptionLinkedIn = '<?= addslashes((strlen(preg_replace('~[\r\n]+~', '', strip_tags(nl2br($courseModel->description))))>300)  ? (substr(preg_replace('~[\r\n]+~', '', strip_tags(nl2br($courseModel->description))),0 ,300).'...') : (preg_replace('~[\r\n]+~', '', strip_tags(nl2br($courseModel->description)))));?>';

			function fbShare() {
				var winWidth = 575;
				var winHeight = 400;
				var winTop = (screen.height / 2) - (winHeight / 2);
				var winLeft = (screen.width / 2) - (winWidth / 2);
				window.open('http://www.facebook.com/sharer.php?s=100&p[title]=<?=urlencode ($courseModel->name)?>&p[summary]=<?=$scoreLabel?>'+description+'&p[url]=' + courseUrl + '&p[images][0]=' + courseImage, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
			}

			$(document).ready(function() {
				$('.social_twitter_link').click(function(event) {
					var width  = 575,
						height = 400,
						left   = ($(window).width()  - width)  / 2,
						top    = ($(window).height() - height) / 2,
						url    = 'http://twitter.com/share?text=<?=$scoreLabel.urlencode ($courseModel->name).' - '?>'+descriptionTwitter+'&url='+courseUrl,
						opts   = 'status=1' +
							',width='  + width  +
							',height=' + height +
							',top='    + top    +
							',left='   + left;

					window.open(url, 'twitter', opts);

					return false;
				});

				$('.social_google_link').click(function(event) {
					var width  = 575,
						height = 400,
						left   = ($(window).width()  - width)  / 2,
						top    = ($(window).height() - height) / 2,
						url    = 'https://plus.google.com/share?url='+courseUrl,
						opts   = 'status=1' +
							',width='  + width  +
							',height=' + height +
							',top='    + top    +
							',left='   + left;

					window.open(url, 'google', opts);

					return false;
				});
				$('.social_linkedin_link').click(function(event) {
					var width  = 575,
						height = 400,
						left   = ($(window).width()  - width)  / 2,
						top    = ($(window).height() - height) / 2,
						url    = 'http://www.linkedin.com/shareArticle?mini=true&url='+courseUrl+'&title=<?=urlencode ($courseModel->name)?>&summary=<?=$scoreLabel?>'+descriptionLinkedIn+'&source=docebo.com?url='+courseUrl,
						opts   = 'status=1' +
							',width='  + width  +
							',height=' + height +
							',top='    + top    +
							',left='   + left;

					window.open(url, 'linkedin', opts);

					return false;
				});
			});

		</script>
	<?php endif; ?>
</div>