<?php
/* @var $showHints string */
/* @var $disableAddLOButton bool */

//we already have the base training breadcrumb added, so we need to add one in addition
$this->breadcrumbs[] = Docebo::ellipsis(Yii::t('player', $courseModel->name), 60, '...');
$this->breadcrumbs[] = Yii::t('standard', 'Training materials');
?>
<!-- Arena -->
<div id="player-container">
	<?php
	/*
	$this->renderPartial('_lonav', array(
		'courseModel' => $courseModel,
	));
	*/
	?>

	<?php
	// Render the arena view
		$this->renderPartial('_arena', array(
			'courseModel' => $courseModel,
			'disableAddLOButton' => $disableAddLOButton
		));
	?>
	<div class="clearfix"></div>

	<!-- Blocks header -->
	<!-- Mobile course: disable widgets -->
	<?php if (!$courseModel->isMobile()) : ?>
		<div id="player-blocks-nav-header-container">
			<?php
			$this->renderPartial('_blocks_header', array(
				'courseModel' => $courseModel,
			)); ?>
		</div>
		<div class="clearfix"></div>

		<!-- Blocks -->
		<div id="player-blocks-container">
			<?=$this->renderPartial('_social_rating', array('courseModel' => $courseModel, 'deepLink'=>$deepLink))?>
			<!-- This DIV will be loaded by blocks grid -->
			<div id="player-blocks-grid"></div>
		</div>
	<?php endif;?>
	
	
	<div class="clearfix"></div>
</div>

<?php
	if (!empty($showHints)) {
		$this->widget('OverlayHints', array('hints'=>$showHints));
	}

	// extra html/js created by plugins
	echo $html;

	//this is used to prevent external windows opening for playing LOs when not needed
	if ($lastPopupPlayed !== false) {
		echo CHtml::hiddenField('last_popup_played', $lastPopupPlayed, array('id' => 'last_popup_played-input'));
	}
?>

<?php
	if ($_GET['fullscreen'] == 1) {
		?>
			<script>
				$('.menu').hide();
				$('#header').hide();
				$('#navigation').hide();
				$('#player-courseheader-container').hide();
				$('.social_rate_title ').hide();
				$('#player-blocks-container').hide();
				$('#footer').hide();
				$('#player-blocks-nav-header-container').hide();
				$('.menu-spacer')[0].style.marginLeft = 0;
			</script>
		<?php
	}
?>

