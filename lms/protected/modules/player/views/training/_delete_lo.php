<?php
/* @var $objectId int */
/* @var $loModel LearningOrganization */
?>

<h1><?= $loModel->repositoryObject ? Yii::t('standard', 'Unlink') : Yii::t('standard', '_DEL') ?></h1>

<?php

	if (Yii::app()->user->hasFlash('error')) {
		echo "<div class='alert'>" . Yii::app()->user->getFlash('error') . "</div>";
	}
?>

<?php
	$form = $this->beginWidget('CActiveForm', array(
		'method' => 'post',
		'htmlOptions' => array(
			'class' => 'ajax'
		)
	));

	if (!$loModel->isFolder()) {
		if(!$loModel->repositoryObject)
			echo Yii::t('player', "Are you sure you want to delete:") . " <strong>".$loModel->title."</strong> ?";
		else
			echo Yii::t('player', "Are you sure you want to unlink:") . " <strong>".$loModel->title."</strong> ?";
	}
	else
		echo Yii::t('player', "Are you sure you want to delete the folder <strong>'[folderName]'</strong> and its content?", array('[folderName]' => $loModel->title));

?>
<?php if($courseType == LearningCourse::TYPE_ELEARNING && !$hasFinalLearningObject):?>
	<br />
	<br />
	<div class="clearfix">
		<?php echo CHtml::label(
			CHtml::checkBox('set_course_as_completed', false, array('id' => 'set_course_as_completed')) . ' ' . Yii::t('player', 'Set the course as Completed, for all users who have finished all other LOs.'),
			'set_course_as_completed',
			array('class' => 'checkbox')
		); ?>
	</div>
<?php endif; ?>

<div class="form-actions">
	<input class="btn-docebo green big" type="submit" name="confirm_delete_lo" value="<?= Yii::t('standard', '_YES'); ?>">
	<input class="btn-docebo black big close-dialog" type="reset" value="<?= Yii::t('standard', '_CANCEL'); ?>">
</div>


<?php $this->endWidget(); ?>
<script>
	$(function(){
		$('#set_course_as_completed').styler();
	});
</script>


