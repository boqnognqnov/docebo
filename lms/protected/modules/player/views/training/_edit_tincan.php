<?php
$form = $this->beginWidget('CActiveForm', array(
	'action' => $this->createUrl('editTincan'),
	'method' => 'post',
	'htmlOptions' => array(
		'id' => 'edittincan-form'
	),
));

?>
<div id="player-arena-uploader-panel" class="player-arena-editor-panel">
	<div class="header">
		<?=Yii::t('standard', 'Tin Can')?>
	</div>
	<div class="content" id="player-arena-uploader">
		<div class="form-horizontal">
			<div id="player-uploader-wrapper" class="player-arena-uploader">

					<div class="control-group">
						<?php echo CHtml::label(Yii::t('standard', '_TITLE'), 'file-title', array(
							'class' => 'control-label'
						)); ?>
						<div class="controls">
							<?php echo CHtml::textField('file-title', ($resource ? $resource['title'] : ''), array('id' => 'file-title', 'class' => 'input-block-level')); ?>
						</div>
					</div>
					<div class="control-group">
						<?php echo CHtml::label(Yii::t('standard', '_DESCRIPTION'), 'file-description'.($resource ? $resource['id_org'] : ''), array(
							'class' => 'control-label'
						)); ?>
						<div class="controls">
							<textarea id="file-description<?= ($resource ? $resource['id_org'] : '') ?>"
							          name="description"
							          class="input-block-level squared"><?= ($resource ? $resource['description'] : '') ?></textarea>
						</div>
					</div>

				<br>
				<?=Chtml::hiddenField('org_id', $resource['id_org'])?>

				<div class="text-right">

					<input type="submit" class="btn-docebo green big" value="<?php echo Yii::t('standard', '_SAVE'); ?>">&nbsp;&nbsp;
					<a href="#" class="cancel-edit btn-docebo black big"><?php echo Yii::t('standard', '_CANCEL'); ?></a>
				</div>

			</div>

		</div>

	</div>


</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">

	$(function () {
		TinyMce.attach('textarea#file-description<?= ($resource ? $resource['id_org'] : '') ?>', {height: 200});
	});

</script>


