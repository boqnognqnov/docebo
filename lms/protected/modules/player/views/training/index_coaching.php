<div id="player-blocks-container" style="margin: 0!important; padding: 20px!important;">
	<div id="user-coaching-session-content">
		<?php if (!empty($title)): ?>
		<h3 class="title-bold" style="padding: 20px; border-bottom: 1px solid #E5E5E5 !important;"><span><?=$title ?></span></h3>
		<?php endif; ?>
		<div class="main-section user-coaching-session-management">
			<?php
				$actionForm = !empty($actionForm) ? $actionForm : Docebo::createAbsoluteUrl('player/training/index&course_id='.$course_id);
				$messageClass = !empty($messageClass) ? $messageClass : 'green-text success text-center center';
				$hideButton = isset($hideButton) ? $hideButton : true;

				// Begin FORM
				echo CHtml::beginForm($actionForm, 'POST', array(
					'id' 	=> 'coaching-user-session-created-form',
					'name' 	=> 'coaching-user-session-created-form',
					'class' => 'ajax',
					'action' => $actionForm
				));

				if (!empty($message)) {
					echo "<div style='display: block; margin: 100px auto; font-size: 18px; font-weight: bold' class='".$messageClass."'>$message<br /><br />";
						if (($coachingSessionStatus == LearningCourseCoachingSessionUser::COACHING_SESSION_STATUS_ASK) && !$hideButton) {
							echo Chtml::ajaxButton(
								Yii::t('course', 'REQUEST A COACH TO THE ADMIN'),
								$actionProcessForm,
								array(),
								array('id' => 'requestCoachingSessionBtn', 'class'=> 'btn btn-docebo blue big center')
							);
						}
			?>

			<script type="text/javascript">
				(function ($) {
					$('#requestCoachingSessionBtn').click(function() {
						window.location.href = "<?=$actionProcessForm ?>";
					});
				})(jQuery);
			</script>
			<?php
					echo "</div>";
				}
			?>
			<div class="form-actions" style="background-color: #fff; text-align: right; margin-bottom: 0px; border-top: 1px solid #E5E5E5!important;">
				<?= CHtml::hiddenField('coaching_status', $coachingSessionStatus); ?>

				<?
				if ($coachingSessionStatus == 4) {
					echo CHtml::submitButton(Yii::t('standard', '_BACK'), array('name' => 'confirm_button', 'class' => 'btn btn-docebo green big'));
				} else {
					echo CHtml::submitButton(Yii::t('standard', '_CONTINUE'), array('name' => 'confirm_button', 'class' => 'btn btn-docebo green big'));
				}
				?>
			
			</div>
			<?php echo CHtml::endForm(); ?>
			<div class="clearfix"></div>

		</div>
	</div>
</div>