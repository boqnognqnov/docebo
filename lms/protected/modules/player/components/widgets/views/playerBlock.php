<?php

	$_params = array(
		"block_id" => $this->blockModel->id,
		"course_id" => $this->blockModel->course_id
	);
	if (!empty($idSession)) {
		$_params['id_session'] = (int)$idSession;
	}

	$updateBlockUrl = $this->owner->createUrl("block/axUpdate", $_params);
	$removeBlockUrl = $this->owner->createUrl("block/axRemove", $_params);

?>
<style>
	.table-hover tr td > a{
		white-space: normal;
		word-break: break-all;
	}
	td.text-right.close-column{
		width: 18%;
	}
	li.player-block.player-block-coursedocs{
		height: auto;
	}
</style>
<li data-block-id="<?= $this->blockModel->id ?>" 
	id="blockid_<?= $this->blockModel->id ?>" 
	class="player-block player-block-<?= $this->blockModel->content_type ?> span<?= $this->blockModel->columns_span ?>" 
	data-span-number="<?= $this->blockModel->columns_span ?>">
	
	<div class="player-block-header">
		<div class="row-fluid">
			<div class="span8">
				<h3 class="title"><?= $title ?></h3>
			</div>
			<div class="span4 text-right">

				<?php
					$userLevel = LearningCourseuser::userLevel(Yii::app()->user->id, $this->owner->course_id);
					$rules = $this->owner->userCanAdminCourse || ($userLevel == LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR);
					// If this is CourseDocs and the user is a normal user check if he has permission also to upload files
					if($this->blockModel->content_type == PlayerBaseblock::TYPE_COURSEDOCS && !$this->owner->userCanAdminCourse)
						$rules = $rules || $this->blockModel->getCanUserUploadFiles();

					// If I'm a coach and this is the My Coach widget
					if($this->blockModel->content_type == PlayerBaseblock::TYPE_MY_COACH && ($userLevel == LearningCourseuser::$USER_SUBSCR_LEVEL_COACH))
						$rules = true;

				?>

				<?php if ($rules) : ?>
				<div class="player-block-extra-header-html"><?= $this->blockModel->getExtraHeaderHtml($userLevel, $this->owner->userCanAdminCourse) ?></div>&nbsp;
					<?php if($this->owner->userCanAdminCourse): ?>
						<a href="#">
							<span class="p-sprite move-light drag-handle"></span>
						</a>&nbsp;

						<div class="dropdown-docebo btn-group">
							<a class="btn btn-transparent dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="p-sprite gear-light"></i>
							</a>
							<ul class="dropdown-menu little pull-right player-block-actions" role="menu" aria-labelledby="blockMenu">
								<?= $extraMenuActionsHtml ?>
								<li><a href="<?= $updateBlockUrl ?>" rel="update-block-dialog-<?= $this->blockModel->id ?>" data-dialog-class="edit-block-modal" class="open-dialog p-hover"><i class="i-sprite is-edit"></i> <?= Yii::t("standard", "_MOD")?></a></li>
								<li><a href="<?= $removeBlockUrl ?>" rel="remove-block-dialog-<?= $this->blockModel->id ?>" class="ajax open-dialog p-hover"><i class="i-sprite is-remove red"></i> <?= Yii::t("player", "Remove block")?></a></li>
							</ul>
						</div>
					<?php endif; ?>
				<?php endif; ?>

			</div>
		</div>
	</div>
	<div class="player-block-content">
		<!-- Here goes the Block content, depending on its type -->
		<div class="player-block-content-inner"></div>
	</div>
</li>
