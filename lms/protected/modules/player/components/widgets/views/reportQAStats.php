<?php
/** @var $this ReportsQAFormatter */
/** @var $answers [] */

$question = $this->question;
?>

<div class="qa-container">
	<?php foreach ($answers as $answer) : ?>
	<div class="row-fluid">
		<div class="span5 qa-title"><span class="p-sprite <?= ($answer['is_correct'] ? 'check-solid-small-green' : 'invisible') ?>"></span> <?= $answer['description'] ?></div>
		<div class="span5">
			<div class="qa-progress-bar"><span style="width: <?= $answer['percentage'] ?>%;"></span></div>
		</div>
		<div class="span1 text-center">
			<label class="qa-lbl"><?= Yii::t('report', 'Percent') ?></label>
			<?= $answer['percentage'].'%' ?></div>
		<div class="span1 text-center">
			<label class="qa-lbl"><?= Yii::t('report', 'Total') ?></label>
			<?= $answer['total'] ?></div>
	</div>
	<?php endforeach; ?>
</div>
