<style>
	#equivalenceCourses{
		margin-left: 0;
	}
</style>
<?php
/* @var $this CourseHeader */
/* @var $editMode bool */
$equivalentSourceCoursesIds = $this->course->getEquivalentSourceCoursesIds(true, false, false);//$this->course->getEquivalentCoursesIds(true);
$equivalentSourceCourseNames = '';
if(!empty($equivalentSourceCoursesIds)){
	$equivalentSourceCourseNames = implode(', ', Yii::app()->db->createCommand()
			->select('name')->from(LearningCourse::model()->tableName())
			->where(array('IN', 'idCourse', $equivalentSourceCoursesIds))->queryColumn());
}
$equivalentCoursesIds = $this->course->getEquivalentCoursesIds(true, false, true);
$equivalentCourseNames = '';
if(!empty($equivalentCoursesIds)){
	$equivalentCourseNames = implode(', ', Yii::app()->db->createCommand()
		->select('name')->from(LearningCourse::model()->tableName())
		->where(array('IN', 'idCourse', $equivalentCoursesIds))->queryColumn());
}
$showUnenrollmentButtons = true;
if($course->enable_unenrollment_on_course_completion != 1 && $courseUser->status == LearningCourseuser::$COURSE_USER_END){
	$showUnenrollmentButtons = false;
}

?>
<div id="player-courseheader-container" class="row-fluid">
	<div class="span7">
		<div class="course-header-logo"><?= CHtml::image($this->course->courseLogoUrl, ''); ?></div>
		<div class="course-header-title">
            <h2><span class="lang-sprite lang_<?=$this->course->lang_code?>  lang-sprite-title" ></span><span style="word-break: normal;"><?= $this->course->name ?></span></h2>

			<?php
			// todo: find a better way to add the session name here
			if ('' !== ($sessionName = Yii::app()->session['course_header_session_'.time()])) : ?>
			<h3 class="course-header-session-name"><?= Yii::t('classroom', 'Session'). ': ' . $sessionName ?></h3>
			<?php endif; ?>
		</div>
	</div>
	<?php if(!$editMode && $enableSelfUnsubscribe): ?>
		<?php
		$canPerformUnenrollment = true;
		if (PluginManager::isPluginActive('CurriculaApp')) {
			if (LearningCoursepathCourses::isCourseAssignedToLearningPaths($this->course_id)) {
				$lpInfo = LearningCoursepath::checkEnrollment(Yii::app()->user->id, $this->course_id);
				if (!empty($lpInfo)) {
					$canPerformUnenrollment = false;
				}
			}
		}
		?>
		<?php if($showUnenrollmentButtons && $canPerformUnenrollment){ ?>
		<div class="span5">
			<div class="text-right" id="course-unenrollment-button">
				<?php
					echo CHtml::link(Yii::t('standard', 'Uneroll'), Docebo::createAbsoluteUrl('admin:courseManagement/selfUnenroll', array('idCourse' => $this->course_id)), array(
						'class' => 'open-dialog btn-docebo blue big delete-node delete-action',
						'rel' => 'self-unenroll',
						'alt' => Yii::t('standard', 'Uneroll'),
						'title' => '',
					));
				?>
			</div>
		</div>
	<?php } ?>

	<?php endif; ?>
	<?php if($enableSessionSelfUnsubscribe && $showUnenrollmentButtons) { ?>
	<div class="span5">
			<div class="text-right" id="session-unenrollment-button">
		<?php
		echo CHtml::link(Yii::t('standard', 'Session Unenroll'), Docebo::createAbsoluteUrl('ClassroomApp/enrollment/sessionSelfUnenroll', array(
			'id_session' => $this->courseSession->id_session,
			'idCourse'=> $this->course->idCourse,
		)), array(
			'class' => 'open-dialog btn-docebo blue big delete-node delete-action',
			'rel' => 'self-unenroll',
			'alt' => Yii::t('standard', 'Session Unenroll'),
			'title' => "",
		));
		?>
			</div>
		</div>
 <?php } ?>
	<?php
	$courseUser = LearningCourseuser::model()->findByAttributes(array(
			'idUser'	=>	Yii::app()->user->id,
			'idCourse'	=>	$this->course_id,
	));
	if(!empty($equivalentSourceCourseNames) && $courseUser && $courseUser->status == LearningCourseuser::$COURSE_USER_END && $courseUser->status_propagator){

		?>
		<div id="equivalenceCourses" class="span12">
			<div id="waitlist-not-available-on-sale" class="warning-strip warning waitlist-not-available-on-sale" style="display: block;color: #3A87AD;	background-color: #D9EDF7;">
				<div class="exclamation-mark"><i class="fa fa-exclamation-circle"></i></div>
				<div class="warning-text" style="margin-top:10px !important;padding-top: 0px !important; max-width: 93% !important; margin-left: 0px !important; float: left !important; margin-right: 0px !important;">
					<?=Yii::t("course", "It looks like you're enrolled also in equivalent courses: {courseNames}. This course has been completed as a part of course equivalence.",
							array('{courseNames}' => $equivalentSourceCourseNames)) ?>
				</div>
			</div>
		</div>
	<?php }elseif(!empty($equivalentCourseNames)){
			if (!$courseUser || $courseUser->status != LearningCourseuser::$COURSE_USER_END) {
		 ?>
		<div id="equivalenceCourses" class="span12">
			<div id="waitlist-not-available-on-sale" class="warning-strip warning waitlist-not-available-on-sale" style="display: block;color: #3A87AD;	background-color: #D9EDF7;">
				<div class="exclamation-mark"><i class="fa fa-exclamation-circle"></i></div>
				<div class="warning-text" style="margin-top:10px !important;padding-top: 0px !important; max-width: 93% !important; margin-left: 0px !important; float: left !important; margin-right: 0px !important;">
					<?=Yii::t("course", "It looks like you're enrolled also in equivalent courses: {courseNames}. Upon completing this course, such courses will be automatically marked as completed.",
						array('{courseNames}' => $equivalentCourseNames)) ?>
				</div>
			</div>
		</div>
	<?php }
		} ?>
</div>
<div class="clearfix"></div>
