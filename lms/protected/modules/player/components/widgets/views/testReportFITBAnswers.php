<div class="span12 test-details-question-score" <?= ((isset($isForNotifications) && $isForNotifications)? 'style="border: 1px solid rgb(230,230,230); padding:10px; background-color: white; margin-top: 10px;"' : '' )?>>
	<h6 <?= ((isset($isForNotifications) && $isForNotifications)? 'style="margin-bottom: 5px; font-size: 14px;"' : '')?>>
		<?= Yii::t('standard', '_SCORE') ?>: <?=$this->score?>
	</h6>
	<?php
		$this->widget('DoceboCGridView', array(
			'id' => 'fitb_test_results',
			'htmlOptions' => array('class' => 'grid-view'),
			'dataProvider' => $dataProvider,
			'columns' => $columns,
			'template' => '{items}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'enablePagination' => false,
			'tableInlineStyle' => ((isset($isForNotifications) && $isForNotifications)? 'width: 100%; font-size: 12px; color: #444; border-collapse: collapse; border-spacing: 0; text-align: left;' : ''),
		));
	?>
</div>

<style>
	div.test-details-question-score{
		margin-left: 0px !important;
	}

	div.test-details-question-score>h6{
		margin-top: 5px;
	}
</style>

