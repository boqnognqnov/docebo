<?php /* @var $navObjects array */ ?>
<?php /* @var $courseModel LearningCourse */ ?>

<?php $courseHasSubfolders = false;
foreach($objectsTree as $rootObj){
	if(isset($rootObj['children']) && is_array($rootObj['children'])){
		foreach($rootObj['children'] as $child){
			if($child['type'] === ''){
				// There is at least 1 subfolder in this course's object tree
				// so show the tree
				$courseHasSubfolders = true;
				break;
			}
		}
	}
}

?>
<div class="row-fluid">
	<?php if($courseHasSubfolders): ?>
	<div class="span4">
		<div class="dynatree-container-parent dynatree-listview">
			<?php $renderItem = function($item) use (&$renderItem, &$courseModel){ ?>
				<li data-level='<?=$item['level']?>' data-key='<?=$item['key']?>' data-path='<?=$item['path']?>'>
				<?php

				// @TODO display a vertical rectangle with the folder's color here

				// Display a left spacer depending on the depth level of the current item
	                        // Updated: indentation/spacing is implemented as a padding in the parent <li> element
	                        //
	                        //echo '<div style="width: '.(intval($item['level']) * 15).'px; float: left;">&nbsp;</div>';
				echo '<span class="i-sprite is-folder-closed folder-icon"></span> ';
				echo '<span class="foldername">'.($item['level'] == 1 ? $courseModel->name : $item['title']).'</span>';
				if(isset($item['children']) && !empty($item['children'])){
					// Walk through child folders

					foreach($item['children'] as $key=>$child){
						if($child['type'] != ''){
							// This is not a folder, skip it
							unset($item['children'][$key]);
						}
					}

					if(count($item['children'])){
						// We have some children folders to display, walk through them
						echo '<ul>';
						foreach($item['children'] as $newChild){
							$renderItem($newChild);
						}
						echo '</ul>';
					}
				}
				echo '</li>';
			};
			echo '<ul class="listview-folder-tree dynatree-container">';
			foreach($objectsTree as $treeItem){
				$renderItem($treeItem);
			}
			echo '</ul>';
			?>
		</div>
	</div>

	<?php endif; ?>

	<div class="<?=$courseHasSubfolders ? 'span8' : 'span12' ?>">
		<div class="player-listview">

			<?php foreach($navObjects as $navObject): ?>

				<?php
				$statusIconClass = "";
				$status = $navObject['status'];

				if ($navObject['locked']) {
					$statusIconClass = "p-sprite lock-small-black";
				} else {
					switch ($status) {
						case LearningScormItemsTrack::STATUS_PASSED:
						case LearningCommontrack::STATUS_COMPLETED:
							$statusIconClass = "p-sprite circle-check-small-green";
							break;
						case LearningCommontrack::STATUS_ATTEMPTED:
						case LearningScormItemsTrack::STATUS_INCOMPLETE:
							$statusIconClass = "p-sprite circle-arrow-small-orange";
							break;
						case LearningScormItemsTrack::STATUS_FAILED:
							$statusIconClass = "p-sprite circle-arrow-small-red";
							break;
						case LearningScormItemsTrack::STATUS_NOT_ATTEMPTED:
						default:
							$statusIconClass = "p-sprite arrow-circle-black";
							break;
					}

				}
				?>

				<div class="single-lo"
				     data-lo-path="<?=$navObject['path']?>"
				     id="player-quicknav-object-<?= $navObject['key'] ?>"
				     data-lo-key="<?= $navObject['key'] ?>"
					 data-object='<?=htmlspecialchars(json_encode($navObject), ENT_QUOTES, 'UTF-8')?>'
					>

					<div class="pull-right status-icon play-lo">
						<div class="<?= $statusIconClass ?>"></div>
					</div>

					<div class="pull-left span10" style="margin-left: 5px;">
						<?php if($navObject['resource']): ?>
							<div class="pull-left">
								<img class="lo-thumb play-lo" src="<?=CoreAsset::url($navObject['resource'], CoreAsset::VARIANT_SMALL)?>" />
							</div>
						<?php endif; ?>

						<div class="lo-content play-lo<?=($navObject['short_description'] !== '' && !is_null($navObject['short_description']) ? '' : ' no-description')?>"><?= $navObject['title'] ?></div>
						<?php if($navObject['short_description'] !== '' && !is_null($navObject['short_description'])) : ?>
							<p><?=$navObject['short_description']?></p>
						<?php endif; ?>
					</div>

					<div class="clearfix"></div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>

<script>
	$(function(){

		// Activate also the tree/folders visible in this player layout
		// (since they are added via AJAX, we are calling init() again here
		// to make sure the dynatree() JS picks them up)
		Arena.learningObjects.contentViewer.init();

		<?php $sessionKey = 'currentFolder.'.$courseModel->idCourse; ?>
		<?php if(Yii::app()->session[$sessionKey]):?>
			Arena.currentFolder = <?=Yii::app()->session[$sessionKey]?>;
		<?php endif;?>

		function repositionStatusIcon() {
			$('.single-lo .status-icon').each(function(){
				var elem = $(this);
				var elemH = elem.outerHeight();
				var parentH = elem.parent().height();
				var topMarginForElem = (parentH-elemH)/2;
				elem.css('margin-top', topMarginForElem*0.8);
			});
		}

		repositionStatusIcon();

		$(window).on('resize',function(){
			repositionStatusIcon();
		}).resize();

		$('.listview-folder-tree li').click(function(e){
			e.stopPropagation();

			// Filter the ListView LO list based on the folder that
			// was just clicked

			$('.listview-folder-tree li').removeClass('opened');

			$('.listview-folder-tree .folder-icon').removeClass('is-folder-opened').addClass('is-folder-closed');

			$(this).addClass('opened').find('> .folder-icon').addClass('is-folder-opened');

			//This is removed, because of changing the behavor when you
			// click at some folder in Learner view, to show only the LO that
			// belongs only to that folder.
			//			if($(this).data('level') === 1){
			//				// Root node clicked. It always show all LOs by default
			//				// (even items in descendants)
			//				$('.single-lo').show();
			//				return;
			//			}

			var currentFolderId = $(this).data('key');
			var currentFolderPath = $(this).data('path');

			Arena.currentFolder = ($(this).data('level') === 1)? false : currentFolderId;
			$.ajax({
				url: '<?=Docebo::createLmsUrl('player/training/axUpdateSelectedFolder')?>',
				type: 'post',
				data: {
					course_id: Arena.course_id,
					currentFolder: Arena.currentFolder
				}
			});

			$('.single-lo').each(function(){
				var loPathFull = $(this).data('lo-path');
				var loKey = $(this).data('lo-key');                                                               

				if(loPathFull == currentFolderPath + '.' + loKey){                                    
					// The LO is a direct child of the selected folder
					$(this).show();
				}else{                                
					// The LO is not a direct child of the selected folder
					$(this).hide();
				}
			});
		});



		if(Arena.currentFolder) {
			$('.listview-folder-tree li[data-key="'+Arena.currentFolder+'"]').trigger('click');
		}else{
			//If no folder is selected before. Root Folder is selected!
			$($('.listview-folder-tree li')[0]).trigger('click');
		}

		// LISTEN for clicks on LO title act accordingly
		$('.player-listview').on('click', '.play-lo', function (event) {

			var dataContainer = $(this).closest('[id^="player-quicknav-object-"]');

			if(dataContainer.size()===0)
				return;

			var loKey = dataContainer.data('lo-key');
			loKey = ""+loKey;

			if($('.player-lonav-tree').length > 0) {

				// Stop retrieving the LO node (the LOs data array) from the menu item:
				// var loNode = $('.player-lonav-tree').dynatree("getTree").getNodeByKey(loKey);

				$('.player-lonav-tree').dynatree("getTree").activateKey(loKey);
			}

			//make suro to not display quick navigation while loading and preparing launcher
			var nav = $("#player-quicknavigation");
			if (nav.length > 0) { nav.hide(); }

			var loData = dataContainer.data('object');

			// Directly PLAY the LO
            var success = Launcher.dispatch(loData, function(){
                setTimeout(function(){
                    PlayerNavigator.setState('playing');
                    PlayerNavigator.setCurrentKey(loData.key);
                    PlayerNavigator.reload();
                }, 1000)
            });
			if(success == false && nav.length > 0)
				nav.show();
		});
	});

	var treeWrapper = $('.dynatree-container-parent.dynatree-listview');
	var treeWrapperHeight = treeWrapper.height();
	var localItemStorage = window.localStorage;

	// saving default height of the container
	localItemStorage.setItem('treeWrapperDefaultHeight', treeWrapperHeight);

	var getTreeWrapperDefaultHeight = localItemStorage.getItem('treeWrapperDefaultHeight');

	/*
	 * Namespace height checker in JQuery main object in order to have in a "global" single instance, since this piece of
	 * code can be loaded asynchronously and executed multiple times (while JQuery is loaded a single time in the main page)
	 */
	if (!$.LOsTreeHeightChecker) {
		//If no checker object has been set yet then create it. This will be executed first time only.
		$.LOsTreeHeightChecker = {
			running: false,
			timer: false,
			check: function() {
				if($.type(getTreeWrapperDefaultHeight) != 'undefined' && getTreeWrapperDefaultHeight > 0) {

					var playerListViewWrapper = $('.player-listview');
					var playerListViewWrapperHeight = playerListViewWrapper.height();

					if (getTreeWrapperDefaultHeight < playerListViewWrapperHeight) {
						treeWrapper.height(playerListViewWrapperHeight);
					} else {
						treeWrapper.height(getTreeWrapperDefaultHeight);
					}

				}else{
					// If something goes wrong returning back the old functionality
					var el = $('.dynatree-container-parent.dynatree-listview');
					if (el && el.length > 0) {
						var h1 = el.height();
						var h2 = el.parent().parent().innerHeight();
						if (h1 != h2) {
							el.height(h2);
						}
					}
				}


			},
			run: function() {
				if (this.running) return; //timer is already running, no need to start another one
				this.running = true; //setting this we will ensure to run just one timer, avoiding the wasting of resources
				this.timer = setInterval(this.check, 10); //check will run continuously in short time intervals
			}
		}
	}
	$.LOsTreeHeightChecker.run();

</script>
