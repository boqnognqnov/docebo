<?php /* @var $this CoursedocsUpload */ ?>
<?php /* @var $isVisible bool */ ?>
<?php /* @var $header string */ ?>

<?php

	$browseBtnId = 'coursedocs-fileupload-'. $this->blockModel->id
		. ($this->model ? '-'.$this->model->id_file : '');
	$uploadFormContainerId = 'coursedocs-upload-container-'. $this->blockModel->id
		. ($this->model ? '-'.$this->model->id_file : '');

?>

<div class="upload-form-container hide" id="<?= $uploadFormContainerId ?>">
	<?php if (!empty($this->header)) : ?>
	<h4><?= $this->header ?></h4>
	<?php endif; ?>

	<div class="upload-form">



		<br>
		<div class="text-right">
			<a href="#" class="btn btn-squared btn-squared-green <?= ($this->model ? '' : 'disabled') ?> coursedocs-save-uploaded-file"><?= Yii::t('standard', '_SAVE') ?></a>
			<a href="#" class="btn btn-squared btn-squared-dark coursedocs-cancel-uploaded-file"><?= Yii::t('standard', '_CANCEL') ?></a>
		</div>
	</div>

</div>
