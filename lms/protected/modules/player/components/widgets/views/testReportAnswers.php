<div class="span12 test-details-question-answers">
	<?= $this->userAnswerCnt?>
</div>
<div class="span12 test-details-question-score" <?= ((isset($isForNotifications) && $isForNotifications)? 'style="border: 1px solid rgb(230,230,230); padding:10px; background-color: white; margin-top: 10px;"' : '')?> >
	<h6 <?= ((isset($isForNotifications) && $isForNotifications)? 'style="margin-bottom: 5px; font-size: 14px;"' : '')?>>
		<?php if (($this->questionType == LearningTestquest::QUESTION_TYPE_EXTENDED_TEXT
					|| $this->questionType == LearningTestquest::QUESTION_TYPE_UPLOAD) && !$this->skipLinkInEvaluationBtn) : ?>
			<?php if ($this->isTeacher): ?>
				<?= Yii::t('standard', '_SCORE') ?>: <a href="#" data-type="text" id="score-edit-<?=$this->questId?>" data-pk="1" data-url="<?=Yii::app()->createUrl('player/report/editTestScore', array('trackId' => $this->trackId, 'questId' => $this->questId, 'course_id' => $_GET['course_id']))?>" class="score-editable"><?=$this->score?></a>
					<?=(isset($max_score) ? "($max_score)" : null)?>
					&nbsp;&nbsp;
					<a class="i-sprite is-edit" style="cursor: pointer;" onclick="Report.triggerInlineEdit(event, <?=$this->questId?>)"></a>
			<?php else: ?>
				<?= Yii::t('standard', '_SCORE') ?>: <?=$this->score?>
			<?php endif; ?>
		<?php else : ?>
			<?= Yii::t('standard', '_SCORE') ?>: <?=$this->score?>
		<?php endif; ?>
	</h6>
	<?= $this->correctAnswerCnt?>
</div>

