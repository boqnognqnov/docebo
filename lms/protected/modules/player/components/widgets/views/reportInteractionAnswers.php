<?php
/** @var $this ReportsInteractionFormatter */
/** @var $htmlAnswers string */
/** @var $htmlCorrectAnswers string */
/** @var $score string */
?>
<div class="span12 test-details-question-answers">
	<?= $htmlAnswers ?>
</div>

<?php if (!empty($htmlCorrectAnswers)) : ?>
<div class="span12 test-details-question-score">
	<?= $htmlCorrectAnswers ?>
</div>
<?php endif; ?>

