<? /* @var $this PlayerNavigator */ ?>
<div class="row-fluid player-navigator-holder">

	<!-- Previous, left arrow -->
	<? if ( $prev ): ?>
	<div class="span2 text-center prev" data-key="<?= $prev ? $prev['key'] : NULL ?>">
			<i class="fa <?= $prev['locked'] ? 'fa-lock' : 'fa-chevron-left' ?>"></i> <?= Yii::t( 'standard', '_PREV' ) ?>
	</div>
	<? endif; ?>
	<!-- Middle area -->
	<? if (empty($current)): ?>
		<div class="span8"></div>
	<? else: ?>
		<div class="span4 top-space extra">
			<div class="pull-right">
				<?
				$statusIconClass = "";
				$status          = $current['status'];
				if ( $current['locked'] ) {
					$statusIconClass = "p-sprite lock-large-grey";
				} else {
					switch ( $status ) {
						case LearningScormItemsTrack::STATUS_PASSED:
						case LearningCommontrack::STATUS_COMPLETED:
							$statusIconClass = "p-sprite circle-check-medium-green";
							break;
						case LearningCommontrack::STATUS_ATTEMPTED:
						case LearningScormItemsTrack::STATUS_INCOMPLETE:
							$statusIconClass = "p-sprite circle-play-medium-orange";
							break;
						case LearningScormItemsTrack::STATUS_FAILED:
							$statusIconClass = "p-sprite circle-play-medium-red";
							break;
						case LearningScormItemsTrack::STATUS_NOT_ATTEMPTED:
						default:
							$statusIconClass = "p-sprite circle-play-medium-grey";
							break;
					}
				}
				?>
				<span class="<?= $statusIconClass ?>"></span>
			</div>
			<div class="clearfix"></div>
		</div>

		<div class="span4 top-space">
			<div class="pull-left">
				<span class="now-playing-label"><?= Yii::t( 'standard', 'Now playing' ) ?></span>
				<br/>
				<?= $current['title'] ?>
			</div>
			<div class="clearfix"></div>
		</div>
	<? endif; ?>

	<!-- Next, right arrow -->
	<? if ( $next ): ?>
	<div class="span2 text-center next" data-key="<?= $next ? $next['key'] : NULL ?>">

			<? if ($state=='initial' && !$courseStarted): ?>
				<?= Yii::t( 'standard', '_START' ) ?>
			<? else: ?>
				<?= Yii::t( 'standard', '_NEXT' ) ?>
			<? endif; ?>
			 <i
				class="fa <?= $next['locked'] ? 'fa-lock' : 'fa-chevron-right' ?>"></i>
	</div>
	<? endif; ?>
</div>