<ul class="nav nav-pills player-reports-popover">
	<li class="dropdown hover">
		<div data-toggle="dropdown grid-menu img-options">
			<div class="report-object-status">
				<div class="completed" style="width: <?=$this->completed?>%"></div>
				<div class="attempted" style="width: <?=$this->attempted?>%"></div>
				<div class="init-state" style="width: <?=$this->initState?>%"></div>
			</div>
		</div>
		<ul class="dropdown-menu pull-right cgrid-menu-ul">
			<li>
				<div class="player-stats-icon">
					<div class="p-sprite flag-small-black2"></div>
				</div>
				<div class="player-stats-info">
					<strong><?=Yii::t('standard', '_COMPLETED')?></strong>
					<span><?=$this->stats->itemsCompleted?> <?=Yii::t('standard', '_USERS')?></span>
				</div>
			</li>
			<li>
				<div class="player-stats-icon">
					<div class="p-sprite clock-circle-small"></div>
				</div>
				<div class="player-stats-info">
					<strong><?=Yii::t('standard', '_USER_STATUS_BEGIN')?></strong>
					<span><?=$this->stats->itemsAttempted?> <?=Yii::t('standard', '_USERS')?></span>
				</div>
			</li>
			<li>
				<div class="player-stats-icon">
					<div class="p-sprite circle-play-small"></div>
				</div>
				<div class="player-stats-info">
					<strong><?=Yii::t('report', '_MUSTBEGIN')?></strong>
					<span><?=$this->stats->itemsInitState?> <?=Yii::t('standard', '_USERS')?></span>
				</div>
			</li>
		</ul>
	</li>
</ul>