<div class="row-fluid">

	<!-- QUICK Navigation Carousel -->

	<div id="player-quicknav-carousel" class="carousel slide">

		<div class="carousel-inner">

			<!-- enumerate carousel items -->
			<?php for ($i = 0; $i < $carouselItemsCount; $i++) : ?>
				<div class="item">
					<div class="row-fluid">
						<div class="span2"></div>
						<div class="span8">
							<div class="row-fluid">

								<?php for($j = 0; $j < $objectsPerSlide; $j++) : ?>
									<?php $k = $i*$objectsPerSlide+$j; ?>
									<?php if (isset($navObjects[$k])) {

										$navObject = $navObjects[$k];
										$statusIconClass = "";
										$status = $navObject['status'];

										switch ($status) {
											case LearningScormItemsTrack::STATUS_PASSED:
											case LearningCommontrack::STATUS_COMPLETED:
												$statusIconClass = "p-sprite circle-check-medium-green";
												break;
											case LearningCommontrack::STATUS_ATTEMPTED:
											case LearningScormItemsTrack::STATUS_INCOMPLETE:
												$statusIconClass = "p-sprite circle-play-medium-orange";
												break;
											case LearningScormItemsTrack::STATUS_FAILED:
												$statusIconClass = "p-sprite circle-play-medium-red";
												break;
											case LearningScormItemsTrack::STATUS_NOT_ATTEMPTED:
											default:
												$statusIconClass = "p-sprite circle-play-medium-grey";
												break;
										}

										if ($navObject['locked']) {
											$statusIconClass = "p-sprite lock-large-grey";
										} else {
											switch ($status) {
												case LearningScormItemsTrack::STATUS_PASSED:
												case LearningCommontrack::STATUS_COMPLETED:
													$statusIconClass = "p-sprite circle-check-medium-green";
													break;
												case LearningCommontrack::STATUS_ATTEMPTED:
												case LearningScormItemsTrack::STATUS_INCOMPLETE:
													$statusIconClass = "p-sprite circle-play-medium-orange";
													break;
												case LearningScormItemsTrack::STATUS_FAILED:
													$statusIconClass = "p-sprite circle-play-medium-red";
													break;
												case LearningScormItemsTrack::STATUS_NOT_ATTEMPTED:
												default:
													$statusIconClass = "p-sprite circle-play-medium-grey";
													break;
											}

										}

										?>
										<div class="span4 player-quicknav-object"
											 id="player-quicknav-object-<?= $navObject['key'] ?>"
											 data-lo-key="<?= $navObject['key'] ?>"
											 data-carousel-index="<?= $i ?>"
											 data-object='<?=htmlspecialchars(json_encode($navObject), ENT_QUOTES, 'UTF-8')?>'
											>

											<div class="row-fluid">
												<div class="span3 status-icon"><span
														class="<?= $statusIconClass ?>"></span></div>
												<div class="span9 object-data">
													<span>
														<?= $navObject['title'] ?>
													</span>
												</div>
											</div>

										</div>

									<?php } ?>

								<?php endfor; ?>

							</div>
						</div>
						<div class="span2"></div>
					</div>
				</div>

			<?php endfor; ?>

		</div>

		<!-- Navigation buttons -->
		<a class="carousel-control left" href="#player-quicknav-carousel" data-slide="prev">&lsaquo;</a>
		<a class="carousel-control right" href="#player-quicknav-carousel" data-slide="next">&rsaquo;</a>

	</div>

</div>

<script type="text/javascript">
	<!--

	$(function () {

		// Disable auto-cycling
		$('#player-quicknav-carousel').each(function () {
			$(this).carousel({
				interval: false
			}).on('slid', '', function() {
				var $this = $(this);
				$this.children('.carousel-control').show();
				var items = $this.find('.item');
				if(items.first().hasClass('active')) {
					$this.children('.left.carousel-control').hide();
				} else if(items.last().hasClass('active')) {
					$this.children('.right.carousel-control').hide();
				}
			}).on('load', '', function() {
				var $this = $(this);
				$this.children('.carousel-control').show();
				if($this.find('.carousel-inner .item:first').hasClass('active')) {
					$this.children('.left.carousel-control').hide();
				} else if($this.find('.carousel-inner .item:last').hasClass('active')) {
					$this.children('.right.carousel-control').hide();
				}
			});
		});
		// Make the first CAROUSEL item active (NOT LO, but carousel)
		$('#player-quicknav-carousel .carousel-inner .item').first().addClass('active');

		// if we are on the first or last slide, hide the proper control
		$('#player-quicknav-carousel').children('.carousel-control').show();
		if($('.carousel-inner .item:first').hasClass('active')) {
			$('#player-quicknav-carousel').children('.left.carousel-control').hide();
		}
		if($('.carousel-inner .item:last').hasClass('active')) {
			$('#player-quicknav-carousel').children('.right.carousel-control').hide();
		}

		// LISTEN for clicks on LO title act accordingly
		$('#player-quicknav-carousel').on('click', '[id^="player-quicknav-object-"]', function () {
			var loKey = $(this).data('lo-key');
			Arena.playByLoKey(loKey);
		});


	});

	//-->
</script>