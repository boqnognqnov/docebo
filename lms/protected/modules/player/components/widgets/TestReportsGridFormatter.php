<?php

class TestReportsGridFormatter extends CWidget {

	public $userId;
	public $questId;
	public $trackId;
	public $html = true;
	public $answerTextMaxLen = null;
	public $multipleAnswersDelimiter = '';

	public function init() {

	}


	public static function formatTextForExport($text) {
		//remove tags
		$formattedText = strip_tags($text);
		//remove line breakers: those will break CSV files and XLS exports
		$formattedText = str_replace("\r", "", $formattedText);
		$formattedText = str_replace("\n", "", $formattedText);
		return $formattedText;
	}


	public function run() {
		$result = '';

		$user_answer = 1;
		$sql = 'SELECT type_quest FROM ' . LearningTestquest::model()->tableName() . ' WHERE idQuest = :id';
		$type = Yii::app()->db->createCommand($sql)->queryScalar(array(':id' => $this->questId));
		if($type == LearningTestquest::QUESTION_TYPE_ASSOCIATE)
			$user_answer = 0;

		$sql =
			"select * from " . LearningTesttrackAnswer::model()->tableName() . " t".
			" join " . LearningTestquestanswer::model()->tableName() . " ltqa on ltqa.idAnswer = t.idAnswer " .
			" join " . LearningTestquest::model()->tableName() . " ltq on ltq.idQuest = t.idQuest " .
			"where t.idTrack = :idTrack and t.idQuest = :idQuest and t.user_answer = :answer";
		$results =
			Yii::app()->db->createCommand($sql)->
				queryAll(
					true,
					array(':idTrack' => $this->trackId, ':idQuest' => $this->questId, ':answer' => $user_answer )
				);
		$userAnswers = $results;


		$learningTestQuestAnswersQuery = "select idAnswer, is_correct from learning_testquestanswer where idQuest = :questId order by sequence ASC, idAnswer ASC";
		$sqlList = Yii::app()->db->createCommand($learningTestQuestAnswersQuery)->queryAll(true, array(':questId' => $this->questId));
		$answers = array();
		foreach($sqlList as $item)
			$answers[ $item['idAnswer']] =$item['is_correct'];
		$formattedAnswers = array();
		foreach($userAnswers as $answer) {
			if($answer["type_quest"] == LearningTestquest::QUESTION_TYPE_ASSOCIATE)
				$isCorrect =
					(isset($answers[$answer["idAnswer"]]) &&
					 $answers[$answer["idAnswer"]] == $answer["more_info"])
						? true
						: false;
			else
				$isCorrect =
					(isset($answers[$answer["idAnswer"]]) &&
					 intval($answers[$answer["idAnswer"]])===1)
						? true
						: false;

			if (isset($answer["idQuest"])) {
				$formattedAnswers [] =
					self::formatAnswerText(
						$answer["type_quest"],
						$answer["more_info"],
						Docebo::nbspToSpace($answer["answer"]),
						$isCorrect,
						$this->html,
						$this->answerTextMaxLen,
						$answer
					);
			}
		}

		if(!empty($formattedAnswers))
			$result = implode($this->multipleAnswersDelimiter, $formattedAnswers);

		echo $result;
	}

	/**
	 * @param $questionType
	 * @param $moreInfo
	 * @param $answer
	 * @param $isCorrect
	 * @param bool $html
	 * @param int $answerTextMaxLen
	 * @return string
	 */
	public static function formatAnswerText($questionType, $moreInfo, $answer, $isCorrect, $html = true, $answerTextMaxLen = 0, $answerArr = false) {
		$result = '';

		if ($isCorrect) {
			$icon = '<div class="p-sprite check-solid-mini-green" style="float: left; margin: 3px 3px 0px 0px;"></div>';
		} else {
			$icon = '<div class="p-sprite remove-mini-red" style="float: left; margin: 3px 3px 0px 0px;"></div>';
		}

		switch ($questionType) {
			case LearningTestquest::QUESTION_TYPE_EXTENDED_TEXT: {
				if ($moreInfo) {
					$etAnswerText = $moreInfo;
					if ($html) {
						$result .= '<div style="float: left;">' . $icon . $etAnswerText . "</div><br>";
					} else {
						if ((int) $answerTextMaxLen > 0) {
							$result .= mb_substr(self::formatTextForExport($etAnswerText), 0, (int) $answerTextMaxLen) . "... ";
						}
						else {
							$result .= self::formatTextForExport($etAnswerText);
						}
					}
				}
			};break;
			case LearningTestquest::QUESTION_TYPE_TEXT_ENTRY:
				$answer = trim(strtolower(strip_tags($answer)));
				$is_correct = (strcmp($answer, trim(strtolower(strip_tags($moreInfo))))  == 0)? true : false;

				if($is_correct)
					$icon = '<div class="p-sprite check-solid-mini-green" style="float: left; margin: 3px 3px 0px 0px;"></div>';
				else
					$icon = '<div class="p-sprite remove-mini-red" style="float: left; margin: 3px 3px 0px 0px;"></div>';

				if ($moreInfo)
				{
					$etAnswerText = $moreInfo;
					if ($html) {
						$result .= '<div style="float: left;">' . $icon . $etAnswerText . "</div><br>";
					} else {
						if ((int) $answerTextMaxLen > 0) {
							$result .= mb_substr(self::formatTextForExport($etAnswerText), 0, (int) $answerTextMaxLen) . "... ";
						}
						else {
							$result .= self::formatTextForExport($etAnswerText);
						}
					}
				}
			break;
			case LearningTestquest::QUESTION_TYPE_FITB:{
				// Let's decide if the user answered correct
				$correctAnswer = QuestionFillInTheBlank::isAnswerCorrect($answerArr['idAnswer'],$answerArr['idTrack']);
				$style = $correctAnswer ? 'check-solid-mini-green' : 'remove-mini-red';
				$icon = '<div class="p-sprite ' . $style . '" style="float: left; margin: 3px 3px 0px 0px;"></div>';

				$sequence = Yii::app()->db->createCommand()
					->select('sequence')
					->from(LearningTestquestanswer::model()->tableName())
					->where('idAnswer = :answ')
					->queryScalar(array(':answ' => $answerArr['idAnswer']));

				$title = '[answ '.$sequence.'] -> ' . $moreInfo;
				if ($html) {
					$result .= '<div style="float: left;">' . $icon . $title . "</div><br>";
				} else {
					if ((int) $answerTextMaxLen > 0) {
						$result .= mb_substr(self::formatTextForExport($title), 0, (int) $answerTextMaxLen) . "... ";
					}
					else {
						$result .= self::formatTextForExport($title);
					}
				}
			}
			break;
			case LearningTestquest::QUESTION_TYPE_UPLOAD: {
				if ($moreInfo) {
					$etAnswerText = Docebo::cleanFilename($moreInfo);
					if ($html) {
						$result .= '<div style="float: left;">' . $icon . $etAnswerText . "</div><br>";
					} else {
						if ((int) $answerTextMaxLen > 0) {
							$result .= mb_substr(self::formatTextForExport($etAnswerText), 0, (int) $answerTextMaxLen) . "... ";
						}
						else {
							$result .= self::formatTextForExport($etAnswerText);
						}
					}
				}
			};break;
			case LearningTestquest::QUESTION_TYPE_ASSOCIATE:
				$answer .= ' -> ';
				if($moreInfo && $moreInfo !== '')
				{
					$learningTestQuestAnswerAssociateQuery =
						"select answer from " . LearningTestquestanswerAssociate::model()->tableName() . " where idAnswer = :answerId";
					$sqlList = Yii::app()->db->createCommand($learningTestQuestAnswerAssociateQuery)->queryAll(true, array(':answerId' => $moreInfo));
					foreach($sqlList as $item)
						$answer .= $item['answer'];
				}

				if($html)
				{
					$result .= '<div style="float: left;">' . $icon . $answer . "</div><br>";
				}
				else
				{
					if ((int) $answerTextMaxLen > 0)
						$result .= mb_substr(self::formatTextForExport($answer), 0, (int) $answerTextMaxLen) . "... ";
					else
						$result .= self::formatTextForExport($answer);
				}
			break;
			default: {
			if ($html) {
				$result .= '<div style="float: left;">' . $icon . $answer . "</div><br>";
			} else {
				if ((int) $answerTextMaxLen > 0) {
					$result .= mb_substr(self::formatTextForExport($answer), 0, (int) $answerTextMaxLen) . "... ";
				}
				else {
					$result .= self::formatTextForExport($answer);
				}
			}
			}
		} // end switch

		return $result;
	}

	public static function prepareFITBTestQuestionForExport($idTrack, $idQuest){
		$userAnswers = Yii::app()->db->createCommand()
			->from(LearningTesttrackAnswer::model()->tableName() . ' t')
			->leftJoin(LearningTestquestanswer::model()->tableName() . ' ltqa', 'ltqa.idQuest = t.idQuest AND ltqa.idAnswer = t.idAnswer')
			->where(array('and', 't.idTrack = :track', 't.idQuest = :question'))
			->order('t.idAnswer')
			->queryAll(true, array(':track' => $idTrack, ':question' => $idQuest));
		$result = array();
		foreach($userAnswers as $k => $userAnswer){
			if($userAnswer['more_info'])
				$result[] = '[answ '.$userAnswer['sequence'].'] -> ' . $userAnswer['more_info'];
		}
		return implode(';', $result);
	}

	public static function prepareFITBTestQuestionForExportAsCorrectColumn($idTrack, $idQuest){
		$userAnswers = Yii::app()->db->createCommand()
			->from(LearningTesttrackAnswer::model()->tableName() . ' t')
			->leftJoin(LearningTestquestanswer::model()->tableName() . ' ltqa', 'ltqa.idQuest = t.idQuest AND ltqa.idAnswer = t.idAnswer')
			->where(array('and', 't.idTrack = :track', 't.idQuest = :question'))
			->order('t.idAnswer')
			->queryAll(true, array(':track' => $idTrack, ':question' => $idQuest));
		$result = array();

		foreach($userAnswers as $k => $userAnswer){
			if($userAnswer['more_info']){
				$correctAnswer = QuestionFillInTheBlank::isAnswerCorrect($userAnswer['idAnswer'], $idTrack);
				$result[] = '[answ '.$userAnswer['sequence'].'] -> ' . ($correctAnswer ? Yii::t('standard', '_YES') : Yii::t('standard', '_NO'));
			}
		}

		return !empty($result) ? implode(';', $result) : Yii::t('standard', '_NO');
	}


}