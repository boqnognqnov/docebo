<?php

class PlayerBlock extends CWidget {
    
	public $blockModel = null;
	public $idSession = false; //this will be effective only if classroom courses are enabled

	public function init() {
		parent::init();
	}



	public function run() {

		// Block type dependent extra html for the block header and menu 
		$extraHeaderHtml = "";
		$extraMenuActionsHtml = "";
		$title = $this->blockModel->title;

		// Attach behaviour to this block (behaviour type taken from block model itself)
		$attached = $this->blockModel->attachContentBehavior();

		// If successfully attached, get the extra html
		if ($attached) {
			$extraHeaderHtml = $this->blockModel->getExtraHeaderHtml();
			$extraMenuActionsHtml = $this->getExtraMenuActionsHtml($this->blockModel->getExtraMenuActions());
			$title = $this->blockModel->getTitle();
		}

		$this->render('playerBlock', array(
			'extraHeaderHtml' => $extraHeaderHtml,
			'extraMenuActionsHtml' => $extraMenuActionsHtml,
			'title' => $title,
			'idSession' => $this->idSession
		));
	}




	/**
	 * Different block types may provide list of actions (html) to put in the BLOCK "gear" menu, as extra items.
	 * This function will build the final <li>  HTML (see widget's playerBlock view).
	 * 
	 * @param array $actions
	 * @return string
	 */
	protected function getExtraMenuActionsHtml($actions) {

		//do some checkings
		if (!is_array($actions)) { return ""; }
		if (empty($actions)) { return ""; }

		//create menu options markup
		$html = '';
		foreach ($actions as $action) {
			$html .= '<li>' . $action . '</li>';
		}
		$html .= '<li class="divider"></li>';

		return $html;
	}



}
