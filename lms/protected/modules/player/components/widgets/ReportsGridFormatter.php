<?php

/**
 * Widget to display object status graph
 */
class ReportsGridFormatter extends CWidget
{
	public $stats;
	public $column;
	public $userId;

	//in percentage for status formatter
	public $completed = 0.00;
	public $attempted = 0.00;
	public $initState = 0.00;

	public function init()
	{

	}

	public function run()
	{
		switch($this->column)
		{
			case 'status':
				$this->formatStatus();
				break;
			case 'userStatus':
				$this->formatUserStatus();
				break;
			case 'userLoStatusReset':
				$this->formatUserLoStatusResetButton();
				break;
			case 'title':
				$this->formatTitle();
				break;
			case 'titleByUser':
				$this->formatTitleByUser();
				break;
			case 'avgTime':
				$this->formatAvgTime();
				break;
			case 'avgScore':
				$this->formatAvgScore();
				break;
			case 'avgScoreByUser':
				$this->formatUserAvgScore();
				break;
				
            case 'attempts':
                $this->formatAttempts();
			    break;
				
		}
	}

	private function formatStatus()
	{
		if ($this->stats->totalItems > 0)
		{
			$this->initState = (($this->stats->itemsInitState) * 100) / $this->stats->totalItems;
			$this->attempted = (($this->stats->itemsAttempted + $this->stats->itemsFailed) * 100) / $this->stats->totalItems;
			$this->completed = (($this->stats->itemsCompleted+$this->stats->itemsPassed) * 100) / $this->stats->totalItems;
		}
		$this->render('reportsGridStatus');
	}

	private function formatUserStatus()
	{
		$statusesList = "";
		$value = '';
		foreach ( LearningCommontrack::getStatusLabelsList() as $key => $status)
			if($key != 'passed' && $key != null)
				$statusesList .= "'".$key."':"."'".$status."', ";

		if (
		    $this->stats->objectType == LearningOrganization::OBJECT_TYPE_ELUCIDAT ||
			$this->stats->objectType == LearningOrganization::OBJECT_TYPE_TEST ||
			$this->stats->objectType == LearningOrganization::OBJECT_TYPE_TINCAN ||
			$this->stats->objectType == LearningOrganization::OBJECT_TYPE_SCORMORG ||
			$this->stats->objectType == LearningOrganization::OBJECT_TYPE_AICC ||
			$this->stats->objectType == '' //scorm item
		)
			$statusesList .= "'".LearningScormItemsTrack::STATUS_FAILED."':"."'".Yii::t('standard', 'failed')."', ";

		$statusesList = substr($statusesList, 0, -2);
		if ($this->stats->itemsAttempted)
		{
			$value = LearningCommontrack::STATUS_ATTEMPTED;
			$imgClass = 'i-sprite is-play orange';
		}
		elseif ($this->stats->itemsCompleted || $this->stats->itemsPassed)
		{
			$value = LearningCommontrack::STATUS_COMPLETED;
			$imgClass = 'i-sprite is-circle-check green';
		}
		elseif ($this->stats->itemsFailed)
		{
			$imgClass = 'i-sprite is-play red';
			$value = LearningScormItemsTrack::STATUS_FAILED;
		}
		elseif ($this->stats->itemsInitState)
		{
			$value = LearningCommontrack::STATUS_AB_INITIO;
			$imgClass = 'i-sprite is-play grey';
		}

		if ($this->stats->objectType)
		{
			$editUrl = Yii::app()->createUrl('/player/report/axChangeLOUserStatus', array('user_id'=>$_GET['user_id'], 'course_id' => $_GET['course_id'], 'object_id' => $this->stats->organizationId));
			$rowId = $this->stats->organizationId;
		}
		else //scorm item  OR aicc item; we use the same mechanic and share the same type id: id_scorm_item
		{
			$editUrl = Yii::app()->createUrl('/player/report/axChangeLOUserStatus', array('user_id'=>$_GET['user_id'], 'course_id' => $_GET['course_id'], 'object_id' => $this->stats->organizationId, 'id_scorm_item' => $this->stats->objectId));
			$rowId = $this->stats->organizationId . $this->stats->objectId;
		}


		switch ($this->stats->objectType) {
			case LearningOrganization::OBJECT_TYPE_SCORMORG: 	$dataType = 'scormselect'; break;
			case LearningOrganization::OBJECT_TYPE_AICC: 		$dataType = 'scormselect'; break;
			default: $dataType = 'select'; break;
		}

		echo '<div class="'.$imgClass.($this->stats->objectType != LearningOrganization::OBJECT_TYPE_DELIVERABLE ? ' lo-status-edit-btn' : '').'" id="btn_lo_status_'.$rowId.'" onclick="Report.triggerLOStatusInlineEdit(event, '.$rowId.')"></div>';

		if($this->stats->objectType !== LearningOrganization::OBJECT_TYPE_DELIVERABLE && (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/lms/admin/report/mod'))){

			$confirmContent = $this->getConfirmDialogContent();
			echo CHtml::link('', '#', array(
				'id' => 'lo_status_'.$rowId,
				'class' => 'lo-editable lo-status-editable',
				'data-url' => $editUrl,
				'data-type' => $dataType,
				'data-value' => $value,
				'data-participate' => $this->stats->participateInOtherCourses,
				'data-change-value-confirm' => $confirmContent,
				'data-source' => '{'.$statusesList.'}',
				'data-pk' => '1'
			));
		}
	}

	private function formatUserLoStatusResetButton(){
		/* @var $this->stats ReportHelper */

		if(!$this->userId)
			throw new CException('ReportsGridFormatter needs the "userId" param set for the type "userLoStatusReset"');

		$obj = LearningOrganization::model()->findByAttributes(array(
			'objectType' => $this->stats->objectType,
			'idResource' => $this->stats->objectId,
			'idCourse'   => $_GET['course_id']
		));
		$track = LearningCommontrack::model()->findByAttributes(array(
			'idReference' => $obj->idOrg,
			'idUser'=> $this->userId,
		));

		$resetStatsUrl = Yii::app()->createUrl('/player/report/axResetLoStats', array('user_id'=>$this->userId, 'course_id' => $_GET['course_id'], 'object_id' => $this->stats->organizationId));
		$pUserRights = Yii::app()->user->checkPURights($obj->idCourse, $this->userId);

		if(
			$track && $track->status != LearningCommontrack::STATUS_AB_INITIO &&// Don't show the Reset button for LOs that are still in Not Started status
			(
				Yii::app()->user->getIsGodadmin() //godadmin
				|| (LearningCourseuser::isUserLevel(Yii::app()->user->id, $obj->idCourse, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR) && LearningCourse::isUserAssignedToInstructorSessions($obj->idCourse, $this->userId))
				|| ($pUserRights->all && Yii::app()->user->checkAccess('/lms/admin/course/evaluation')) //PU with "mod" permissions, assigned course and user
			)
		) {
			$showResetButton = true;
		} else {
			$showResetButton = false;
		}

		if($showResetButton)
			echo '<a data-dialog-class="reset-lo-stats" rel="dialog_reset_user_lo_stats" href="'.$resetStatsUrl.'" class="open-dialog reset-lo-stats-action"><div class="i-sprite is-remove red lo-status-edit-btn"></div></a>';

	}

	private function formatTitle()
	{
		if ($this->stats->objectType)
		{
			switch ($this->stats->objectType)
			{
				case LearningOrganization::OBJECT_TYPE_TEST:
					echo '<strong class="player-report-object-title">'.LearningOrganization::objectTypeValue($this->stats->objectType).'</strong> - <strong><a href="javascript:;" onclick="Report.showInfoBox(\'report-grids-box\', \''.Yii::app()->createUrl('player/report/testUsers', array('organizationId'=>$this->stats->organizationId, 'course_id'=>$_GET['course_id'])).'\')" style="text-decoration: underline;">'.$this->stats->title.'</a></strong>';
					break;
				case LearningOrganization::OBJECT_TYPE_VIDEO:
				case LearningOrganization::OBJECT_TYPE_FILE:
				case LearningOrganization::OBJECT_TYPE_HTMLPAGE:
				case LearningOrganization::OBJECT_TYPE_TINCAN:
				case LearningOrganization::OBJECT_TYPE_AUTHORING:
				case LearningOrganization::OBJECT_TYPE_SCORMORG:
				case LearningOrganization::OBJECT_TYPE_ELUCIDAT:
					echo '<strong class="player-report-object-title">'.LearningOrganization::objectTypeValue($this->stats->objectType).'</strong> - <strong><a href="javascript:;" onclick="Report.showInfoBox(\'report-grids-box\', \''.Yii::app()->createUrl('player/report/commonUsers', array('organizationId'=>$this->stats->organizationId, 'course_id'=>$_GET['course_id'])).'\')" style="text-decoration: underline;">'.$this->stats->title.'</a></strong>';
					break;
				case LearningOrganization::OBJECT_TYPE_POLL:
					echo '<strong class="player-report-object-title">'.LearningOrganization::objectTypeValue($this->stats->objectType).'</strong> - <strong><a title="'.$this->stats->title.'" href="'.Yii::app()->createUrl('player/report/pollResult', array('objectId'=>$this->stats->objectId, 'organizationId' => $this->stats->organizationId, 'course_id'=>$_GET['course_id'])).'" class="reports-fancybox fancybox.ajax report-grid-link">'.$this->stats->title.'</a></strong>';
					break;
                case LearningOrganization::OBJECT_TYPE_DELIVERABLE:
                    echo '<strong class="player-report-object-title">'.LearningOrganization::objectTypeValue($this->stats->objectType).'</strong> - <strong><a href="javascript:;" onclick="Report.showInfoBox(\'report-grids-box\', \''.Yii::app()->createUrl('player/report/deliverableResult', array('organizationId'=>$this->stats->organizationId, 'course_id'=>$_GET['course_id'])).'\')" style="text-decoration: underline;">'.$this->stats->title.'</a></strong>';
                    break;
				default:
					echo '<strong class="player-report-object-title">'.LearningOrganization::objectTypeValue($this->stats->objectType).'</strong> - <strong>'.$this->stats->title.'</strong>';
			}
		}
		else
		{
			//this is a scorm item
			//calculate indendation of the sco item
			echo '<table class="report-sco"><tbody><tr>';
			$baseSpacer = '<td class="spacer"></td>';
			$level = abs((!empty($this->stats->scormItemTreeLevel) ? (int)$this->stats->scormItemTreeLevel : 1)); //make sure that level number is at least 1
			for ($i=0; $i<$level; $i++) { echo $baseSpacer; }
			//draw item
			echo '<td class="title">';
			if ($this->stats->is_folder || $this->stats->isAiccItem) {
				echo '<strong>'.$this->stats->title.'</strong>';
			} else {
				echo '<strong><a style="text-decoration: underline" href="javascript:;" onclick="Report.showInfoBox(\'report-grids-box\', \''.Yii::app()->createUrl('player/report/scoUsers', array('objectId'=>$this->stats->objectId, 'course_id'=>$_GET['course_id'])).'\')">'.$this->stats->title.'</a></strong>';
			}
			echo '</td>';
			echo '</tr></tbody></table>';
		}
	}

	private function formatTitleByUser()
	{
		if ($this->stats->objectType) {
			switch ($this->stats->objectType)
			{
				case LearningOrganization::OBJECT_TYPE_TINCAN:
			    case LearningOrganization::OBJECT_TYPE_ELUCIDAT:
					echo '<strong class="player-report-object-title">'.LearningOrganization::objectTypeValue($this->stats->objectType).'</strong> - <strong><a class="report-grid-link" href="javascript:;" onclick="Report.showInfoBox(\'report-bu-user-grid\', \''.Yii::app()->createUrl('player/report/tinCan', array('objectId'=>$this->stats->objectId, 'user_id' => $_GET['user_id'], 'course_id'=>$_GET['course_id'])).'\')">'.$this->stats->title.'</a></strong>';
					break;
				case LearningOrganization::OBJECT_TYPE_TEST:
					echo '<strong class="player-report-object-title">'.LearningOrganization::objectTypeValue($this->stats->objectType).'</strong> - <strong><a title="'.$this->stats->title.'" href="'.Yii::app()->createUrl('player/report/testByUser', array('organizationId'=>$this->stats->organizationId, 'userId' => $_GET['user_id'], 'course_id'=>$_GET['course_id'])).'" class="reports-fancybox fancybox.ajax report-grid-link">'.$this->stats->title.'</a></strong>';
					break;
				case LearningOrganization::OBJECT_TYPE_POLL:
					echo '<strong class="player-report-object-title">'.LearningOrganization::objectTypeValue($this->stats->objectType).'</strong> - <strong>'.$this->stats->title.'</strong>';
					break;
				case LearningOrganization::OBJECT_TYPE_ELUCIDAT:
					echo '<strong class="player-report-object-title">'.LearningOrganization::objectTypeValue($this->stats->objectType).'</strong> - <strong><a class="report-grid-link" href="javascript:;" onclick="Report.showInfoBox(\'report-bu-user-grid\', \''.Yii::app()->createUrl('player/report/elucidat', array('objectId'=>$this->stats->objectId, 'user_id' => $_GET['user_id'], 'course_id'=>$_GET['course_id'])).'\')">'.$this->stats->title.'</a></strong>';
					break;
				default:
					echo '<strong class="player-report-object-title">'.LearningOrganization::objectTypeValue($this->stats->objectType).'</strong> - <strong>'.$this->stats->title.'</strong>';
					break;
			}
		} else
		{
			//check for tracking info presence
			$trackingExists = LearningScormTracking::model()->findByAttributes(array('idUser' => $_GET['user_id'], 'idscorm_item' => $this->stats->objectId));
			//calculate indendation of the sco item
			echo '<table class="report-sco"><tbody><tr>';
			$baseSpacer = '<td class="spacer"></td>';
			$level = abs((!empty($this->stats->scormItemTreeLevel) ? (int)$this->stats->scormItemTreeLevel : 1)); //make sure that level number is at least 1
			for ($i=0; $i<$level; $i++) { echo $baseSpacer; }
			//draw item
			echo '<td class="title">';
			if ($this->stats->is_folder || !$trackingExists) {
				echo '<strong>'.$this->stats->title.'</strong>';
			} else {
				echo '<strong><a  title="'.$this->stats->title.'" class="reports-fancybox fancybox.ajax" style="text-decoration: underline" href="'.Yii::app()->createUrl('player/report/scoXmlReport', array('objectId'=>$this->stats->objectId, 'course_id'=>$_GET['course_id'], 'user_id' => $_GET['user_id'])).'">'.$this->stats->title.'</a></strong>';
			}
			echo '</td>';
			echo '</tr></tbody></table>';
		}
	}

	private function formatAvgTime()
	{
		$average = $this->stats->averageTime();
		echo $average > 0 ? Yii::app()->localtime->hoursAndMinutes($average) : '-'; //format from seconds to hrs/minutes
	}

	private function formatAvgScore()
	{
		switch ($this->stats->objectType)
		{
			case LearningOrganization::OBJECT_TYPE_DELIVERABLE:
			case LearningOrganization::OBJECT_TYPE_TEST:
			case LearningOrganization::OBJECT_TYPE_TINCAN:
			case LearningOrganization::OBJECT_TYPE_ELUCIDAT:
				echo $this->stats->averageScore();
				break;
			case 'aicc': // A AICC
			case '': // A SCORM SCO
				$scores = $this->stats->scores;
				$score = isset($scores[0]) ? $scores[0] : false;
				if(isset($score['raw']) && $score['raw']>0){
					$maxScore = isset($score['max']) && $score['max'] ? $score['max'] : 100;
					//echo (int)$score['raw'] . '/' . $maxScore;
					echo number_format($score['raw'],2);
				}
				break;
			default:
				echo '';
		}
	}

	private function formatUserAvgScore()
	{
		if ($this->stats->objectType)
			$rowId = $this->stats->organizationId;
		else
			$rowId = $this->stats->organizationId.$this->stats->objectId;

		$avgPair = $this->stats->averageScorePair();
		switch ($this->stats->objectType)
		{
			/*case LearningOrganization::OBJECT_TYPE_TEST:
			case LearningOrganization::OBJECT_TYPE_TINCAN:
			case LearningOrganization::OBJECT_TYPE_DELIVERABLE:
				echo "<span>" . number_format($avgPair['raw'],2) . "</span>";
				break;*/
			case LearningOrganization::OBJECT_TYPE_TEST:
			case LearningOrganization::OBJECT_TYPE_TINCAN:
			case LearningOrganization::OBJECT_TYPE_ELUCIDAT:
			case LearningOrganization::OBJECT_TYPE_LTI:
				$avgPair = $this->stats->averageScorePair();

				if ($this->stats->objectType)
					$editUrl = Yii::app()->createUrl('/player/report/axChangeLOUserScore', array('user_id'=>$_GET['user_id'], 'course_id' => $_GET['course_id'], 'object_id' => $this->stats->organizationId));
				else //scorm item
					$editUrl = Yii::app()->createUrl('/player/report/axChangeLOUserScore', array('user_id'=>$_GET['user_id'], 'course_id' => $_GET['course_id'], 'object_id' => $this->stats->organizationId, 'id_scorm_item' => $this->stats->objectId));


				if(Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/lms/admin/course/evaluation')){

					$confirmContent = $this->getConfirmDialogContent();
					echo CHtml::link('', '#', array(
						'id' => 'lo_score_' . $rowId,
						'class' => 'lo-score-editable',
						'data-type' => 'text',
						'data-pk' => '1',
						'data-url' => $editUrl,
						'data-value' => number_format($avgPair['raw'],2),
						'data-participate' => $this->stats->participateInOtherCourses,
						'data-change-value-confirm' => $confirmContent,
					));
				}else{
					echo "<span>" . number_format($avgPair['raw'],2) . "</span>";
				}


				break;
			case LearningOrganization::OBJECT_TYPE_DELIVERABLE:
				$avgPair = $this->stats->averageScorePair();

				if ($this->stats->objectType)
					$editUrl = Yii::app()->createUrl('/player/report/axChangeLOUserScore', array('user_id'=>$_GET['user_id'], 'course_id' => $_GET['course_id'], 'object_id' => $this->stats->organizationId));
				else //scorm item
					$editUrl = Yii::app()->createUrl('/player/report/axChangeLOUserScore', array('user_id'=>$_GET['user_id'], 'course_id' => $_GET['course_id'], 'object_id' => $this->stats->organizationId, 'id_scorm_item' => $this->stats->objectId));

				//if the deliverable object is not marked as passed don't edit it.
				if($this->stats->itemsCompleted > 0 && (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/lms/admin/course/evaluation'))){

					$confirmContent = $this->getConfirmDialogContent();
					echo CHtml::link('', '#', array(
						'id' => 'lo_score_' . $rowId,
						'class' => 'lo-score-editable',
						'data-type' => 'text',
						'data-pk' => '1',
						'data-url' => $editUrl,
						'data-value' => number_format($avgPair['raw'],2),
						'data-participate' => $this->stats->participateInOtherCourses,
						'data-change-value-confirm' => $confirmContent,
					));
				}else{
					echo "<span>" . number_format($avgPair['raw'],2) . "</span>";
				}
				break;


			case LearningOrganization::OBJECT_TYPE_AICC:
			case '': // SCORM item/chapter (SCO)
				$scores = $this->stats->scores;
				$score = isset($scores[0]) ? $scores[0] : false;
				if(isset($score['raw']) && $score['raw']>0){
					$maxScore = isset($score['max']) && $score['max'] ? $score['max'] : 100;
					//echo (int)$score['raw'] . '/' . $maxScore;
					echo number_format($score['raw'],2);
				}
				break;
			default:
				echo '';
		}
	}

	private function getConfirmDialogContent() {
		$confirmContent = '';
		if ($this->stats->participateInOtherCourses) {
			$confirmContent = $this->widget('common.widgets.warningStrip.WarningStrip', array (
				'message'=> Yii::t('standard', "This change will affect <strong>{n} other course(s)</strong> where this user is enrolled in.", array('{n}' => $this->stats->participateInOtherCourses) ),
				'type'=>'warning'
			), true);

			$confirmCheckBox = '<div class="clearfix">';
			$confirmCheckBox .= CHtml::checkbox('confirm', false, array('class'=>'pull-left' ));
			$confirmCheckBox .= CHtml::label(Yii::t('standard','Yes, I confirm I want to proceed'), 'confirm');
			$confirmCheckBox .= '</div> <script> $(function(){$("input").styler();}) </script>';

			$confirmContent .= $confirmCheckBox;
		}

		return $confirmContent;
	}
	
	/**
	 * Format [failed] "ATTEMPTS" column in Course -> Report -> User LO objects,
	 * where we show taken and allowed failed attempts, per LO
	 * 
	 */
	private function formatAttempts() {
	    
	    // NOT all LO types are subject to check/show
	    if (!in_array($this->stats->objectType, LearningOrganization::$OBJECTS_TO_CHECK_MAX_ATTEMPTS)) {
	        echo "";
	        return;
	    }
	    
	    /**
	     * 
	     * @var ReportHelper $stats
	     */
	    $stats = $this->stats;

	    if ($stats->failedAttemptsAllowed === null) {
	        echo "<span>- / -</span>";
	        return;
	    }
	    
	    $takenValue = number_format($stats->failedAttemptsTaken,0);
	    $allowedValue = number_format($stats->failedAttemptsAllowed,0);
	    
	    // LO ID
	    $idOrg = $stats->organizationId;
	    
	    $editTakenUrl      = Yii::app()->createUrl('/player/report/axEditAttempts', array('user_id'=>$_GET['user_id'], 'course_id' => $_GET['course_id'], 'object_id' => $idOrg));
	    $editAllowedUrl    = Yii::app()->createUrl('/player/report/axEditAttempts', array('user_id'=>$_GET['user_id'], 'course_id' => $_GET['course_id'], 'object_id' => $idOrg));
	    

	    // Lets colorize the output
	    // Orange: Only one more failed attempt remains
	    // Red: No more failed attempts are allowed, LO will be locked
	    $wrapperClass = "failed-attempts";
	    if ($allowedValue > 0) {
	       if ($takenValue >= $allowedValue) {
	           $wrapperClass = $wrapperClass . " blocked";
	       }
	       else if (($allowedValue - $takenValue) <= 1) {
	           $wrapperClass = $wrapperClass . " warning";
	       }
	    }
	    
	    echo '<span class="'.$wrapperClass.'">';
	    
	    // Only GodAdmin can do this. For the rest, just show values (taken/allowed)
	    if (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/lms/admin/course/evaluation')) {
	        echo CHtml::link('', '#', array(
	            'id'                           => 'attempts-taken-' . $idOrg,
	            'data-pk'                           => $idOrg,
	            'class'                        => 'lo-attempts-editable' . ' red',
	            'data-type'                    => 'text',
	            'data-url'                     => $editTakenUrl,
	            'data-value'                   => $takenValue,
	            'data-params'                  => json_encode(array('attempts_type' => 'taken')),
	        ));
	        echo " / ";
	        echo CHtml::link('', '#', array(
	            'id'                           => 'attempts-allowed-' . $idOrg,
	            'data-pk'                           => $idOrg,
	            'class'                        => 'lo-attempts-editable',
	            'data-type'                    => 'text',
	            'data-url'                     => $editAllowedUrl,
	            'data-value'                   => $allowedValue,
	            'data-params'                  => json_encode(array('attempts_type' => 'allowed')),
	         ));
	         
	    } else {
	        echo "<span>" . number_format($stats->failedAttemptsTaken ,0) . "</span> / <span>" . number_format($stats->failedAttemptsTaken ,0) . "</span>";
	    }
	    
	    echo '</span>';
	     
	    
	}
	
	
}