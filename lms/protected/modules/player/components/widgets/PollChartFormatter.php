<?php

/**
 * Widget to display object status graph
 */
class PollChartFormatter extends CWidget
{
	public $questionType;
	public $result;
	public $bgColor;
	public $id_poll;

	protected $total = 0;
	protected $colors = array('003d6a', '0465ac', '52a1dc');

	public function init()
	{
		foreacH ($this->result as $res)
		{
			$this->total += $res['count'];
		}
	}

	public function run()
	{
		switch($this->questionType)
		{
			case LearningPollquest::POLL_TYPE_CHOICE:
			case LearningPollquest::POLL_TYPE_CHOICE_MULTIPLE:
			case LearningPollquest::POLL_TYPE_INLINE_CHOICE:
				$this->formatChoice();
				break;
			#case LearningPollquest::POLL_TYPE_COURSE_EVAL:
			#case LearningPollquest::POLL_TYPE_TEACHER_EVAL:
			#	$this->formatEval();
			#	break;
			case LearningPollquest::POLL_TYPE_EVALUATION:
				$this->formatEvaluation();
				break;
			case LearningPollquest::POLL_TYPE_LIKERT_SCALE:
				$this->formatLikerScale();
				break;
		}
	}

	private function formatChoice()
	{
		$cnt = 0;
		foreach ($this->result as $res)
		{
			$percentage = 0;
			if ($this->total > 0)
				$percentage = $res['count'] * 100 / $this->total;

			echo '<span style="float: left; width: 100%;">'.$res['title'].'</span>';
			if ($res['count'])
				echo '<div class="poll-chart-bar" style="width: '.$percentage.'%; background-color: #'.$this->colors[$cnt].';"><div class="poll-chart-bar-count">'.$res['count'].'</div><div class="poll-chart-bar-arrow"></div></div>';
			else
				echo '<div class="poll-chart-bar" style="width: 3px; background-color: #'.$this->colors[$cnt].';"></div>';

			$cnt++;
			if ($cnt == count($this->colors))
				$cnt = 0;
		}
	}

	private function formatEval()
	{
		$cnt = 0;
		foreach ($this->result as $res)
		{
			$percentage = 0;
			if ($this->total > 0)
				$percentage = $res['count'] * 100 / $this->total;

			echo '<div style="width: 100%; float; left;">';
			if ($res['count'])
				echo '<div class="poll-chart-bar" style="width: '.$percentage.'%; background-color: #'.$this->colors[$cnt].';"><div class="poll-chart-bar-inner-title" style="background-color: #'.$this->bgColor.';">'.$res['title'].'</div><div class="poll-chart-bar-count">'.$res['count'].'</div><div class="poll-chart-bar-arrow"></div></div>';
			else
				echo '<div class="poll-chart-bar" style="width: 33px; background-color: #'.$this->colors[$cnt].';"><div class="poll-chart-bar-inner-title" style="background-color: #'.$this->bgColor.';">'.$res['title'].'</div></div>';
			echo '</div>';
			$cnt++;
			if ($cnt == count($this->colors))
				$cnt = 0;
		}
	}

	private function formatEvaluation()
	{
		$cnt = 0;
		foreach ($this->result as $res)
		{
			$percentage = 0;
			if ($this->total > 0)
				$percentage = $res['count'] * 100 / $this->total;

			echo '<div style="width: 100%; float; left;">';
			if ($res['count'])
				echo '<div class="poll-chart-bar" style="width: '.$percentage.'%; background-color: #'.$this->colors[$cnt].';"><div class="poll-chart-bar-inner-title" style="background-color: #'.$this->bgColor.';">'.$res['title'].'</div><div class="poll-chart-bar-count">'.$res['count'].'</div><div class="poll-chart-bar-arrow"></div></div>';
			else
				echo '<div class="poll-chart-bar" style="width: 33px; background-color: #'.$this->colors[$cnt].';"><div class="poll-chart-bar-inner-title" style="background-color: #'.$this->bgColor.';">'.$res['title'].'</div></div>';
			echo '</div>';
			$cnt++;
			if ($cnt == count($this->colors))
				$cnt = 0;
		}
	}

	private function formatLikerScale(){
		$questionManager = PollQuestion::getQuestionManager($this->questionType);
		$likertScales = $questionManager->getPollScales($this->id_poll);
		$results = $this->result;
		// Let's format the data a bit in order to bold the highest values
		$data = array();
		// Since we need to find the highest values in array there can be more than one
		foreach($results as $key => $result){
			if(empty($result['answers'])) continue;
			// Let's first extract all the answers from the current question
			$max = max($result['answers']);
			foreach($result['answers'] as $iKey => $value){
				if($max == $value){
					$data[$key][$iKey] = $value;
				}
			}
		}

		echo '<table class="playLikertScale"><thead><tr><th></th>';
			foreach($likertScales as $scale){
				echo '<th class="scaleCheckboxHeader">' . $scale->title . '</th>';
			}
		echo '</tr></thead>';
		echo '<tbody>';
		foreach ($this->result as $key => $answer){
			echo '<tr class="id-'.$key.'">';
			echo '<td>' . $answer['title'] . '</td>';
			foreach($likertScales as $iKey => $scale){
				$value = 0;
				if(isset($answer['answers'][$scale['id']])){
					// Should we bold it
					if(isset($data[$key][$scale['id']])){
						$value = '<b>' . $answer['answers'][$scale['id']] . '</b>';
					}else{
						$value = $answer['answers'][$scale['id']];
					}
				}
				echo '<td class="scaleCheckBox id-' . $scale['id'] . '">';

				echo $value;
				echo '</td>';
			}
			echo '</tr>';
		}
		echo '</tbody></table>';
	}

}