<?php

/**
 * Widget to display object status graph
 */
class PollReportsAnswerFormatter extends CWidget
{
	public $questId;
	public $trackId;
	public $userId;
	public $questionType;

	protected $answers;
	protected $userAnswers;

	public function init()
	{
		$this->answers = array();
		$this->userAnswers = array();

		$answers = Yii::app()->db->createCommand()
			->select('id_answer, answer')
			->from(LearningPollquestanswer::model()->tableName())
			->where('id_quest = :questId', array(':questId' => $this->questId))
			->order('sequence ASC, id_answer ASC')->queryAll();

		foreach($answers as $answer) {
			$this->answers[$answer['id_answer']] = $answer['answer'];
		}

		$answers = Yii::app()->db->createCommand()
			->select('id_answer, more_info')
			->from(LearningPolltrackAnswer::model()->tableName())
			->where('id_track = :id_track AND id_quest = :id_quest', array(
				':id_track' => $this->trackId,
				':id_quest' => $this->questId
			))
			->order('id_answer ASC')->queryAll();

		foreach($answers as $answer) {
			$this->userAnswers[$answer['id_answer']] = $answer['more_info'];
		}
	}

	public function run()
	{
		switch($this->questionType)
		{
			case LearningPollquest::POLL_TYPE_CHOICE:
				$this->formatChoice();
				break;
			case LearningPollquest::POLL_TYPE_CHOICE_MULTIPLE:
				$this->formatChoice(true);
				break;
			case LearningPollquest::POLL_TYPE_INLINE_CHOICE:
				$this->formatChoice();
				break;
			case LearningPollquest::POLL_TYPE_EVALUATION:
				$this->formatEvaluation();
				break;
			case LearningPollquest::POLL_TYPE_TEXT:
				$this->formatText();
		}
	}

	private function formatChoice($multiple = false)
	{
		$input = $multiple ? 'checkbox' : 'radio';
		$html = '<ul>';

		foreach($this->answers as $key => $answer) {
            $trimmedAnswer = trim(preg_replace('/(^(<p>)([\x00-\x1F\x80-\xFF]|&nbsp;|\s){1,})/','<p>',$answer));
            $trimmedAnswer = preg_replace('/([\x00-\x1F\x80-\xFF]|&nbsp;|\s){1,}(<\/p>)$/','</p>',$trimmedAnswer);
			$checked = (isset($this->userAnswers[$key]));
			$html .= '<li>';
			$html .= '<input type="' . $input . '" ' . ($checked ? "checked" : "") . ' disabled><span>' . $trimmedAnswer . '<span>';
			$html .= '</li>';
		}
		$html .= '</ul>';

		echo $html;
	}

	private function formatEvaluation()
	{
		$html = '<ul>';

		foreach($this->answers as $key => $answer) {
            $trimmedAnswer = trim(preg_replace('/(^(<p>)([\x00-\x1F\x80-\xFF]|&nbsp;|\s){1,})/','<p>',$answer));
            $trimmedAnswer = preg_replace('/([\x00-\x1F\x80-\xFF]|&nbsp;|\s){1,}(<\/p>)$/','</p>',$trimmedAnswer);
			$checked = (isset($this->userAnswers[$key]));
			$html .= '<li>';
			$html .= '<input type="radio" ' . ($checked ? "checked" : "") . ' disabled><span>' . $trimmedAnswer . '<span>';
			$html .= '</li>';
		}
		$html .= '</ul>';

		echo $html;
	}

	private function formatText()
	{
		if(isset($this->userAnswers[0])) {
			echo $this->userAnswers[0];
		} else {
			echo  Yii::t('standard', '_NO_ANSWER');
		}

	}
}