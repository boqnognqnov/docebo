<?php

class CourseHeader extends CWidget
{
	public $course_id;
	public $session_id;
	// set this param when instantiating the widget so that this component be reusable
	public $userCanAdminCourse = false;
	public $targetMode = 'edit_course';
	/**
	 * @var LearningCourse
	 */
	public $course;
	public $courseSession;
	public $webinarSession;
	public $showEditViewCourse = false;
	public $courseUser;
	public $sessionUser;
	public $webinarUser;
	public $isAssignedToThisCourse = false;
	public $isAssignedToThisSession = false;
	public $isAssignedToThisWebinar = false;
	public $showUnenrollmentButton = false;

	public $showSessionUnenrollButton = false;
	public $showWebinarUnenrollButton = false;

	public function init()
	{
		if (!$this->course)
			$this->course = LearningCourse::model()->findByPk($this->course_id);
		else
			$this->course_id = $this->course->getPrimaryKey();

		if(!$this->courseSession) {
			$this->courseSession = LtCourseSession::model()->findByAttributes(array(
				'course_id' => $this->course_id,
				'id_session' => $this->session_id
			));
		}


		if (!$this->webinarSession) {
			$this->webinarSession = WebinarSession::model()->findByAttributes(array(
				'course_id' => $this->course_id,
				'id_session' => $this->session_id
			));
		}

		$this->courseUser = LearningCourseuser::model()->findByAttributes(array('idUser' => Yii::app()->user->id, 'idCourse' => $this->course_id));
		$this->sessionUser = LtCourseuserSession::model()->findByAttributes(array('id_user' => Yii::app()->user->id, 'id_session' => $this->courseSession->id_session));
		$this->webinarUser = WebinarSessionUser::model()->findByAttributes(array('id_user' => Yii::app()->user->id, 'id_session' => $this->webinarSession->id_session));

		$this->isAssignedToThisCourse = isset($this->courseUser)? true : false;
		$this->isAssignedToThisSession = isset($this->sessionUser)? true : false;
		$this->isAssignedToThisWebinar = isset($this->webinarUser)? true : false;

		$this->showEditViewCourse = Yii::app()->controller->module->id == 'player' && Yii::app()->controller->id == 'training';
		$this->showUnenrollmentButton = (Yii::app()->controller->module->id == 'player' && Yii::app()->controller->id == 'training') || (Yii::app()->controller->module->id == 'ClassroomApp' && Yii::app()->controller->id == 'session');

		// We want to show the "self-unenroll session button" only in the player view
		$this->showSessionUnenrollButton = (Yii::app()->controller->module->id == 'player');
		$this->showWebinarUnenrollButton = (Yii::app()->controller->module->id == 'player');
		parent::init();
	}

	public function run()
	{
		/* @var $cs CClientScript */
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/courseHeader.css');

		if (!in_array($this->targetMode, array('view_course', 'edit_course'))) {
			$this->targetMode = 'edit_course';
		}

		if (Yii::app()->player->course->course_type == LearningCourse::TYPE_CLASSROOM) {
			$viewName = 'classroomCourseHeader';
			$editMode = ($this->userCanAdminCourse && $this->showEditViewCourse);
		} else {
			$viewName = 'elearningCourseHeader';
			$editMode = ($this->userCanAdminCourse && $this->showEditViewCourse);
		}
		$enableSelfUnsubscribe = ($this->showUnenrollmentButton && $this->isAssignedToThisCourse && ($this->courseUser->level == LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT || $this->courseUser->level == LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR)&& intval($this->course->enable_self_unenrollment) > 0);
		$enableSessionSelfUnsubscribe = ($this->showSessionUnenrollButton && $this->isAssignedToThisSession && ($this->courseUser->level == LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT || $this->courseUser->level == LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR)&& intval($this->course->enable_session_self_unenrollment) > 0);
		$enableWebinarSelfUnsubscribe = ($this->showWebinarUnenrollButton && $this->isAssignedToThisWebinar && ($this->courseUser->level == LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT || $this->courseUser->level == LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR)&& intval($this->course->enable_session_self_unenrollment) > 0);

		$this->render($viewName, array(
			'editMode' => $editMode,
			'enableSelfUnsubscribe' => $enableSelfUnsubscribe,
			'enableSessionSelfUnsubscribe' => $enableSessionSelfUnsubscribe,
			'enableWebinarSelfUnsubscribe' => $enableWebinarSelfUnsubscribe,
			'course' => $this->course,
			'courseUser' => $this->courseUser
		));
	}

}
