<?php
/**
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */

class PlayerNavigator extends CWidget{

	public $courseModel;
	public $currentKey;
	public $state = 'initial'; // initial|playing|ended

	protected $orgModel;
	private $objectsTree;
	private $objectsFlatLevelArray;

	public function init() {
		parent::init();

		$this->orgModel = LearningOrganization::model()->findByPk($this->currentKey);

		$params = array('userInfo' => 1, 'scormChapters' => 1, 'showHiddenObjects' => false, 'downloadOnScormFoldersOnly' => false, 'aiccChapters' => 1);
		$this->objectsTree = LearningObjectsManager::getCourseLearningObjectsTree($this->courseModel->idCourse, $params);
		$this->objectsFlatLevelArray = LearningObjectsManager::flattenLearningObjectsTree($this->objectsTree);
	}

	public function run() {
		if(empty($this->objectsFlatLevelArray)){
			// No LOs inside the course
			return;
		}

		$currentPlayedObject = array();

		$prevObject = false;
		$nextObject = false;

		$courseStarted = $this->isCourseStarted(); // at least one LO played

		switch($this->state){
			case 'initial':
				if($courseStarted){
					foreach($this->objectsFlatLevelArray as $index=>$objectInfo){

						if($objectInfo['key']==$this->currentKey){
							$currentPlayedObject = $this->objectsFlatLevelArray[$index];

							if(isset($this->objectsFlatLevelArray[$index-1])){
								$prevObject = $this->objectsFlatLevelArray[$index-1];
							}
							if(isset($this->objectsFlatLevelArray[$index+1])){
								$nextObject = $this->objectsFlatLevelArray[$index+1];
							}
							break;
						}
					}
				}else{
					// First landing on the course. No LO was ever played yet

					foreach($this->objectsFlatLevelArray as $index=>$objectInfo){
						// Just set the "next" object to be played to the first
						// available to play and hide the middle area and the
						// left arrow (previous button)
						if($objectInfo['key']==$this->currentKey){
							$nextObject = $this->objectsFlatLevelArray[$index];
							break;
						}
					}
				}

				break;
			case 'playing':
				foreach($this->objectsFlatLevelArray as $index=>$objectInfo){

					if($objectInfo['key']==$this->currentKey){
						$currentPlayedObject = $this->objectsFlatLevelArray[$index];

						if(isset($this->objectsFlatLevelArray[$index-1])){
							$prevObject = $this->objectsFlatLevelArray[$index-1];
						}
						if(isset($this->objectsFlatLevelArray[$index+1])){
							$nextObject = $this->objectsFlatLevelArray[$index+1];
						}
						break;
					}
				}
				break;
			case 'ended':
				// Get the last available LO and set it to the "previous" arrow
				$prevObject = end($this->objectsFlatLevelArray);

				$courseStarted = true;
				break;
		}

		if(!$prevObject && !$nextObject && empty($currentPlayedObject)) return;

		$this->render('player_navigator', array(
			'current'=>$currentPlayedObject,
			'next'=>$nextObject,
			'prev'=>$prevObject,
			'courseStarted'=>$courseStarted,
			'state'=>$this->state,
		));
	}

	protected function isCourseStarted(){
		foreach($this->objectsFlatLevelArray as $index=>$objectInfo) {
			if ( $objectInfo['status'] && $objectInfo['status'] != LearningCommontrack::STATUS_AB_INITIO ) {
				// If at least one LO is played/failed/in progress
				return TRUE;
			}
		}

		return FALSE;
	}

}