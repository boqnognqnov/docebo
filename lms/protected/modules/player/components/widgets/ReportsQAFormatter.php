<?php

class ReportsQAFormatter extends CWidget {

	/** @var array */
	public $question;

	public $type;
	public $correctResponses;
	public $totalResponses;
	public $responses;

	public function init() {
		$this->type = $this->question['type'];
		if (count($this->question['correct_responses'])==1 && is_array($this->question['correct_responses'][0])) {
			$this->correctResponses = $this->question['correct_responses'][0];
		} else {
			$this->correctResponses =  $this->question['correct_responses'];
		}
		$this->totalResponses = $this->question['total_responses'];
		$this->responses = $this->question['responses'];
	}


	public function run() {
		switch ($this->type) {
			case 'choice':
			case 'true-false':
				$this->formatChoice();
				break;
			case 'matching':
				$this->formatAssociate();
				break;
			case 'sequencing':
			case 'fill-in':
			case 'other':
				$this->formatFillIn();
				break;
			default:
				break;
		}
	}

	private function formatChoice() {

		$rows = array();

		foreach($this->responses as $k => $response) {

			$counter = $response['counter'];
			$percentage = ($this->totalResponses > 0) ? floatval($counter / $this->totalResponses) * 100 : 0;

			$description = $response['description'];
			if ($this->type==='true-false') {
				switch ($description) {
					case 't': $description = Yii::t('standard', 'True'); break;
					case 'f': $description = Yii::t('standard', 'False'); break;
					default: break;
				}
			}

			$isCorrect = false;
			if(isset($this->correctResponses) && in_array($k, $this->correctResponses))
				$isCorrect = true;

			$rows[] = array(
				'description' => $description,
				'percentage' => number_format($percentage, 2),
				'total' => $counter,
				'is_correct' => $isCorrect
			);
		}

		$this->render('reportQAStats', array(
			'answers' => $rows
		));
	}

	private function formatAssociate() {
		echo 'todo';
	}

	private function formatFillIn() {
		$rows = array();

		if($this->responses) {
			foreach($this->responses as $k => $response) {

				$counter = $response['counter'];
				$percentage = ($this->totalResponses > 0) ? floatval($counter / $this->totalResponses) * 100 : 0;

				$description = $response['description'];

				$isCorrect = false;
				if((isset($this->correctResponses) && in_array($k, $this->correctResponses)) || ($this->type == 'other' && isset($response['correct']) && $response['correct']))
					$isCorrect = true;

					$rows[] = array(
					'description' => $description,
					'percentage' => number_format($percentage, 2),
					'total' => $counter,
					'is_correct' => $isCorrect
				);
			}

		}

		$this->render('reportQAStats', array(
			'answers' => $rows
		));
	}
}