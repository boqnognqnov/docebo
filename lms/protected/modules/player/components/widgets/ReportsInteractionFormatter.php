<?php

/**
 * Widget to display a single interaction object in the interactions history tab
 * of the user's sco details modal
 */
class ReportsInteractionFormatter extends CWidget {

	/** @var array */
	public $interaction;

	public $type;
	public $correctResponses;
	public $learnerResponse;
	public $result;
	public $responses;
	public $sets;

	public function init() {
		$this->type = $this->interaction['type'];
		if (count($this->interaction['correct_responses'])==1 && is_array($this->interaction['correct_responses'][0])) {
			$this->correctResponses = $this->interaction['correct_responses'][0];
		} else {
			$this->correctResponses =  $this->interaction['correct_responses'];
		}
		$this->learnerResponse = is_array($this->interaction['learner_response']) ? $this->interaction['learner_response'] : array($this->interaction['learner_response']);
		$this->result = $this->interaction['result'];
		$this->responses = isset($this->interaction['responses']) ? $this->interaction['responses'] : null;
		$this->sets = isset($this->interaction['sets']) ? $this->interaction['sets'] : null;
	}


	public function run() {
		switch ($this->interaction['type']) {
			case 'choice':
			case 'true-false':
				$this->formatChoice();
				break;
			case 'fill-in':
			case 'long-fill-in':
				$this->formatTextEntry();
				break;
			case 'matching':
				$this->formatAssociate();
				break;
			case 'numeric':
				$this->formatNumeric();
				break;
			case 'sequencing':
				$this->formatSequencing();
				break;
			case 'other':
				$this->formatOther();
				break;
			default:
				$this->todo();
				break;
		}
	}

	private function todo() {
		echo '<p>Not yet implemented.</p>';
	}

	private function formatChoice() {
		$multiple = (count($this->correctResponses) > 1);
		$input = $multiple ? 'checkbox' : 'radio';

		// both true/false answers should be shown
		if ($this->type === 'true-false') {
			if (count($this->responses)==1) {
				$add = isset($this->responses['t']) ? 'f' : 't';
				$this->responses[$add] = array('description' => $add);
			}
		}

		$htmlAnswers = '<ul>';
		$userAnswers = array();
		foreach($this->learnerResponse as $userAnswer) {
			$userAnswers[$userAnswer] = $userAnswer;
		}

		$allAreCorrect = true; // keep a flag if at least one incorrect answer exists -> and print the correct one under the score for a reference
		foreach($this->responses as $answer => $answerData) {
			//print all available answers
			$isChecked = isset($userAnswers[$answer]);
			$isCorrect = false;
			if(isset($this->correctResponses) && is_array($this->correctResponses) && in_array($answer, $this->correctResponses))
				$isCorrect = true;

			$answerLabel = isset($answerData['description']) ? $answerData['description'] : $answer;
			if ($this->type === 'true-false') {
				switch ($answerLabel) {
					case 't':
						$answerLabel = Yii::t('standard', 'True');
						break;
					case 'f':
						$answerLabel = Yii::t('standard', 'False');
						break;
					default: break;
				}
			}

			$htmlAnswers .= '<li>';
			if ($isChecked) {
				$htmlAnswers .= '<span class="p-sprite ' . ($isCorrect ? "check-solid-small-green" : "remove-small-red") . '"></span>';
			} else {
				$htmlAnswers .= '<span class="' . ($isCorrect ? "test-answer-spare-icon" : "test-answer-spare-icon") . '"></span>';
			}
			$htmlAnswers .= '<input type="' . $input . '" ' . ($isChecked ? "checked" : "") . ' disabled><span>' . $answerLabel . '<span>';
			$htmlAnswers .= '</li>';

			if (($isChecked && !$isCorrect) || (!$isChecked && $isCorrect)) {
				$allAreCorrect &= false;
			}
		}
		$htmlAnswers .= '</ul>';

		//print correct answers if an incorrect one exists
		$htmlCorrectAnswers = '';
		if (!$allAreCorrect) {
			$htmlCorrectAnswers = '<br/><h6>' . Yii::t('player', 'Correct answers:') . '</h6>';
			$htmlCorrectAnswers .= '<ul>';
			foreach($this->responses as $answer => $answerData) {
				$isCorrect = false;
				if(isset($this->correctResponses) && is_array($this->correctResponses) && in_array($answer, $this->correctResponses))
					$isCorrect = true;

				$answerLabel = isset($answerData['description']) ? $answerData['description'] : $answer;
				if ($this->type === 'true-false') {
					switch ($answerLabel) {
						case 't':
							$answerLabel = Yii::t('standard', 'True');
							break;
						case 'f':
							$answerLabel = Yii::t('standard', 'False');
							break;
						default: break;
					}
				}

				$htmlCorrectAnswers .= '<li><input type="' . $input . '" ' . ($isCorrect ? "checked" : "") . ' disabled><span class="correct-answer-label">' . Docebo::nbspToSpace($answerLabel) . '</span></li>';
			}
			$htmlCorrectAnswers . '</ul>';
		}

		$this->render('reportInteractionAnswers', array(
			'htmlAnswers' => $htmlAnswers,
			'htmlCorrectAnswers' => $htmlCorrectAnswers,
			'score' => '-'
		));
	}

	private function formatTextEntry() {
		$htmlAnswers = '<div>';

		$allAreCorrect = true;

		if(isset($this->correctResponses) && is_array($this->correctResponses))
			$correctResponse = $this->correctResponses[0];
		else
			$correctResponse = '';
		// let's try and format the correct response
		$knownSeparatorsReplacement = array('|#|' => ', ');
		if (!empty($knownSeparatorsReplacement)) {
			$search = array_keys($knownSeparatorsReplacement);
			$replace = array_values($knownSeparatorsReplacement);
			$correctResponse = str_replace($search, $replace, $correctResponse);
		}


		$isCorrect = ($this->result === 'correct');

		if (count($this->learnerResponse)) {
			$learnerResponse = strip_tags($this->learnerResponse[0]);

			if (!$isCorrect) $allAreCorrect = false;

			if($isCorrect)
				$htmlAnswers .= '<div class="p-sprite check-solid-small-green"></div>' . $learnerResponse;
			else
				$htmlAnswers .= '<div class="p-sprite cross-small-red"></div>' . $learnerResponse;

		} else {
			$htmlAnswers .= '<div class="p-sprite remove-small-red")"></div>' . Yii::t('standard', '_NO_ANSWER');
		}
		$htmlAnswers .= '</div>';

		//print correct answers if an incorrect one exists
		$htmlCorrectAnswers = '';
		if (!$allAreCorrect) {
			$htmlCorrectAnswers = '<br/><h6>' . Yii::t('player', 'Correct answers:') . '</h6>';
			$htmlCorrectAnswers .= '<ul>';
			$htmlCorrectAnswers .= '<li><span class="correct-answer-label">' . Docebo::nbspToSpace($correctResponse). '</span></li>';
			$htmlCorrectAnswers .= '</ul>';
		}

		$this->render('reportInteractionAnswers', array(
			'htmlAnswers' => $htmlAnswers,
			'htmlCorrectAnswers' => $htmlCorrectAnswers,
			'score' => '-'
		));
	}

	private function formatNumeric() {
		$htmlAnswers = '<div>';

		$allAreCorrect = true;

		if(isset($this->correctResponses) && is_array($this->correctResponses))
			$correctResponse = $this->correctResponses[0];
		else
			$correctResponse = '';


		$isCorrect = ($this->result === 'correct');

		if (count($this->learnerResponse)) {
			$learnerResponse = strip_tags($this->learnerResponse[0]);

			if (!$isCorrect) $allAreCorrect = false;

			if($isCorrect)
				$htmlAnswers .= '<div class="p-sprite check-solid-small-green"></div>' . $learnerResponse;
			else
				$htmlAnswers .= '<div class="p-sprite cross-small-red"></div>' . $learnerResponse;

		} else {
			$htmlAnswers .= '<div class="p-sprite remove-small-red")"></div>' . Yii::t('standard', '_NO_ANSWER');
		}
		$htmlAnswers .= '</div>';

		//print correct answers if an incorrect one exists
		$htmlCorrectAnswers = '';
		if (!$allAreCorrect) {
			$htmlCorrectAnswers = '<br/><h6>' . Yii::t('player', 'Correct answers:') . '</h6>';
			$htmlCorrectAnswers .= '<ul>';
			$htmlCorrectAnswers .= '<li><span class="correct-answer-label">' . Docebo::nbspToSpace($correctResponse) . '</span></li>';
			$htmlCorrectAnswers .= '</ul>';
		}

		$this->render('reportInteractionAnswers', array(
			'htmlAnswers' => $htmlAnswers,
			'htmlCorrectAnswers' => $htmlCorrectAnswers,
			'score' => '-'
		));
	}

	private function formatAssociate() {

		$correctAnswer = array();
		$allAreCorrect = true;

		if(isset($this->correctResponses) && is_array($this->correctResponses))
		{
			foreach ($this->correctResponses as $key => $data)
			{
				$k = ($data[0])? $data[0] : $key;
				$v = $data[1];
				$correctAnswer[$k] = $v;
			}
		}
		else
			$correctAnswer = array();

		$userAnswer = array();
		foreach ($this->learnerResponse as $key => $data) {
			$k = ($data[0])? $data[0] : $key;
			$v = $data[1];
			$userAnswer[$k] = $v;
		}

		$htmlCorrectAnswers = '<br /><h6>' . Yii::t('player', 'Correct answers:') . '</h6><ul>';

		$htmlAnswers = '<ul>';
		if (isset($this->sets['left']) && is_array($this->sets['left'])) {
			foreach($this->sets['left'] as $ansLeft) {
				if (isset($userAnswer[$ansLeft])) {
					$isCorrect = ($userAnswer[$ansLeft] === $correctAnswer[$ansLeft]);
					if (!$isCorrect) $allAreCorrect = false;
					$htmlAnswers .= '<li><div class="p-sprite ' . ($isCorrect ? 'check-solid-small-green' : 'remove-small-red') . '"></div>' . $ansLeft . ' - ' . $userAnswer[$ansLeft] . '</li>';
				} else {
					$htmlAnswers .= '<div class="p-sprite remove-small-red""></div>' . Yii::t('standard', '_NO_ANSWER');
					$allAreCorrect = false;
				}

				$htmlCorrectAnswers .= '<li>' . $ansLeft . ' - ' . $correctAnswer[$ansLeft] . '</li>';
			}
		}

		$htmlCorrectAnswers .= '</ul>';

		$htmlAnswers .= '</ul>';

		$this->render('reportInteractionAnswers', array(
			'htmlAnswers' => $htmlAnswers,
			'htmlCorrectAnswers' => $allAreCorrect? '' : $htmlCorrectAnswers,
			'score' => '-'
		));
	}

	private function formatSequencing() {
		$allAreCorrect = true;

		$htmlAnswers = '<ul>';
		$htmlCorrectAnswers = '<br /><h6>' . Yii::t('player', 'Correct answers:') . '</h6><ul>';

		if(is_array($this->correctResponses))
		{
			foreach($this->correctResponses as $index => $response) {
				if (isset($this->learnerResponse[$index])) {
					$isCorrect = ($this->learnerResponse[$index] === $response);
					if (!$isCorrect) $allAreCorrect = false;
					$htmlAnswers .= '<li><div class="p-sprite ' . ($isCorrect ? 'check-solid-small-green' : 'remove-small-red') . '"></div>' . $this->learnerResponse[$index] . '</li>';
				} else {
					$allAreCorrect = false;
					$htmlAnswers .= '<div class="p-sprite remove-small-red""></div>' . Yii::t('standard', '_NO_ANSWER');
				}
				$htmlCorrectAnswers .= '<li>' . $response . '</li>';
			}
		}

		$htmlAnswers .= '</ul>';
		$htmlCorrectAnswers .= '</ul>';

		$this->render('reportInteractionAnswers', array(
			'htmlAnswers' => $htmlAnswers,
			'htmlCorrectAnswers' => $allAreCorrect? '' : $htmlCorrectAnswers,
			'score' => '-'
		));
	}

	private function formatOther() {
		if(isset($this->correctResponses) && is_array($this->correctResponses))
			$correctResponse = $this->correctResponses[0];
		else
			$correctResponse = '';

		$isCorrect = ($this->result === 'correct');

		$htmlAnswers = '<div>';
		if (count($this->learnerResponse)) {
			$learnerResponse = strip_tags($this->learnerResponse[0]);
			if($isCorrect)
				$htmlAnswers .= '<div class="p-sprite check-solid-small-green"></div>' . $learnerResponse;
			else
				$htmlAnswers .= '<div class="p-sprite cross-small-red"></div>' . $learnerResponse;

		} else {
			$htmlAnswers .= '<div class="p-sprite remove-small-red")"></div>' . Yii::t('standard', '_NO_ANSWER');
		}
		$htmlAnswers .= '</div>';

		$htmlCorrectAnswers = '';
		if (!$isCorrect) {
			$htmlCorrectAnswers = '<br/><h6>' . Yii::t('player', 'Correct answers:') . '</h6>';
			$htmlCorrectAnswers .= '<ul>';
			$htmlCorrectAnswers .= '<li><span class="correct-answer-label">' . Docebo::nbspToSpace($correctResponse). '</span></li>';
			$htmlCorrectAnswers .= '</ul>';
		}

		$this->render('reportInteractionAnswers', array(
			'htmlAnswers' => $htmlAnswers,
			'htmlCorrectAnswers' => $htmlCorrectAnswers,
			'score' => '-'
		));
	}

}