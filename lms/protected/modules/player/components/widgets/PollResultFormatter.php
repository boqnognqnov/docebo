<?php

/**
 * Widget to display object status graph
 */
class PollResultFormatter extends CWidget
{
	/** @var LearningPollquest */
	public $question;
	public $questionType;
	public $questId;
	public $pollTrackIds;
	public $bgColor;

	protected $answers;
	protected $userAnswers;

	public function init()
	{
        $this->userAnswers = Yii::app()->db->createCommand()
            ->select('id_answer, more_info')
            ->from(LearningPolltrackAnswer::model()->tableName())
            ->where(array('in', 'id_track', $this->pollTrackIds))
            ->andWhere('id_quest = :id_quest', array(':id_quest' => $this->questId))
            ->queryAll();

        $this->answers = Yii::app()->db->createCommand()
            ->select('id_answer, answer')
            ->from(LearningPollquestanswer::model()->tableName())
            ->where('id_quest = :questId',array(':questId' => $this->questId))
            ->order('sequence ASC')
            ->queryAll();
	}

	public function run()
	{
		switch($this->questionType)
		{
			case LearningPollquest::POLL_TYPE_CHOICE:
			case LearningPollquest::POLL_TYPE_CHOICE_MULTIPLE:
			case LearningPollquest::POLL_TYPE_INLINE_CHOICE:
				$this->formatChoice();
				break;
#			case LearningPollquest::POLL_TYPE_COURSE_EVAL:
#			case LearningPollquest::POLL_TYPE_TEACHER_EVAL:
#				$this->formatEval();
#				break;
			case LearningPollquest::POLL_TYPE_EVALUATION:
 				$this->formatEvaluation();
 				break;
			case LearningPollquest::POLL_TYPE_TEXT:
				$this->formatText();
				break;
			case LearningPollquest::POLL_TYPE_LIKERT_SCALE:
				$this->formatLikerScale();
				break;
		}
	}

	private function formatChoice()
	{
		$result = array();
		foreach ($this->answers as $answer)
		{
			if (!isset($result[$answer->id_answer]))
                //ADD trim here[this is for the statistics]
                $trimmedAnswer = trim(preg_replace('/(^(<p>)([\x00-\x1F\x80-\xFF]|&nbsp;|\s){1,})/','<p>',$answer['answer']));
                $trimmedAnswer = preg_replace('/([\x00-\x1F\x80-\xFF]|&nbsp;|\s){1,}(<\/p>)$/','</p>',$trimmedAnswer);
				$result[$answer['id_answer']] = array('count' => 0, 'title' => $trimmedAnswer);
		}
		foreach ($this->userAnswers as $uAnswer)
		{
			if (isset($result[$uAnswer['id_answer']]))
				$result[$uAnswer['id_answer']]['count']++;
		}
		$this->widget('PollChartFormatter', array(
			'result' => $result,
			'questionType' => $this->questionType,
			'bgColor' => $this->bgColor,
		));
	}

	private function formatEval()
	{
		$min = 0;
		$max = 0;
		$step = 0;
		$result = array();
		foreach ($this->answers as $key => $answer)
		{
			switch ($key)
			{
				case 0:
					$min = $answer['answer'];
				case 1:
					$max = $answer['answer'];
				case 2:
					$step = $answer['answer'];
			}
		}
		for ($i = (int)$min; $i <= $max; $i += $step)
		{
			$result[$i] = array('count' => 0, 'title' => $i);

		}
		foreach ($this->userAnswers as $uAnswer)
		{
			if (isset($result[$uAnswer['more_info']]))
				$result[$uAnswer['more_info']]['count']++;
		}
		$this->widget('PollChartFormatter', array(
			'result' => $result,
			'questionType' => $this->questionType,
			'bgColor' => $this->bgColor,
		));
	}

	private function formatEvaluation()
	{
		$result = array();

		foreach ($this->answers as $answer)
		{
			$id = intval($answer['id_answer']);
			if (!isset($result[$id])) {
				$result[$id] = array('count' => 0, 'title' => $answer['answer']);
			}
		}
		foreach ($this->userAnswers as $uAnswer)
		{
			if (isset($result[$uAnswer['id_answer']]))
				$result[$uAnswer['id_answer']]['count']++;
		}

		$this->widget('PollChartFormatter', array(
			'result' => $result,
			'questionType' => $this->questionType,
			'bgColor' => $this->bgColor,
		));
	}

	private function formatText()
	{
		$result = array();
		foreach ($this->userAnswers as $key => $uAnswer)
		{
			$result[$key]['id'] = $key;
			$result[$key]['answer'] = $uAnswer['more_info'];
		}
		$dataProvider = new CArrayDataProvider($result, array(
			'pagination'=>array(
				'pageSize'=>10,
			),
		));
		$this->widget('DoceboCListView', array(
			'dataProvider'=>$dataProvider,
			'itemView'=>'pollFormatterText',
			'summaryText' => false,
			'id' => 'poll-question-'.$this->questId,
			'template' => '{items}{pager}',
		));
	}

	private function formatLikerScale(){
		$result = array();
		$id_poll = Yii::app()->request->getParam('objectId', null);
		foreach ($this->answers as $answer)
		{
			$id = $answer['id_answer'];
			if (!isset($result[$id])) {
				$result[$id] = array('count' => 0, 'title' => $answer['answer'], 'answers' => array());
			}
		}
		foreach ($this->userAnswers as $uAnswer)
		{
			if (isset($result[$uAnswer['id_answer']])){
				$result[$uAnswer['id_answer']]['count']++;
				$result[$uAnswer['id_answer']]['answers'][$uAnswer['more_info']]++;
			}
		}

		$this->widget('PollChartFormatter', array(
			'result' => $result,
			'questionType' => $this->questionType,
			'bgColor' => $this->bgColor,
			'id_poll' => $id_poll
		));
	}
}