<?php

class CoursedocsUpload extends CWidget {

	/* @var $blockModel CActiveRecordBehavior */
	public $blockModel = null;
	public $header;

	/**
	 * @var LearningCourseFile
	 */
	public $model;

    public function init() {
        parent::init();
    }
    
    public function run() {
        $this->render('coursedocsUpload', array());
    }
    
}
