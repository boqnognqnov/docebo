<?php

class QuickNavigationBand extends CWidget
{
	public $course_id = false;
	public $objectsTree = false;
	public $objectsFlatList = false;
	public $objectsPerSlide = 3;

	public $playerLayout;
	
	
	
	
	public function init() {
		// No course ID means get out of here
		if ($this->course_id  !== false) {
			$params = array('userInfo' => 1, 'scormChapters' => 1, 'showHiddenObjects' => false, 'downloadOnScormFoldersOnly' => false, 'aiccChapters' => 1);
			$this->objectsTree = LearningObjectsManager::getCourseLearningObjectsTree($this->course_id, $params);
			$this->objectsFlatList = LearningObjectsManager::flattenLearningObjectsTree($this->objectsTree);
			$this->playerLayout = LearningCourse::getPlayerLayout($this->course_id);
		}
		parent::init();
		
	}
	
	
	public function run() {
		
		/*
		 
'level' => '4'
        'key' => '281'
        'idOrganization' => 281
        'title' => 'POLL NEWW'
        'isFolder' => false
        'type' => 'poll'
        'sort' => 14
        'path' => '188.304.306.281'
        'prerequisites' => ''
        'status' => false
        'locked' => false		 
		  
		 */
		
		if (!$this->objectsFlatList) {
			return; 
		}

		// Individual playable items (objects)
		$navObjects = array();
		foreach ($this->objectsFlatList as $object) {
			$navObjects[] = $object;
		}	

		$carouselItemsCount = ceil(count($navObjects)/$this->objectsPerSlide);
		
		// No items at all? get out
		if ($carouselItemsCount <= 0) {
			return;
		}
		

		if($this->playerLayout==LearningCourse::$PLAYER_LAYOUT_LISTVIEW) {
			$this->render( 'quickNavigationBandListview', array(
				'navObjects'         => $navObjects,
				'carouselItemsCount' => $carouselItemsCount,
				'objectsPerSlide'    => $this->objectsPerSlide,
				'objectsTree'        => $this->objectsTree,
				'courseModel'        => LearningCourse::model()->findByPk( $this->course_id )
			) );
		}elseif($this->playerLayout==LearningCourse::$PLAYER_LAYOUT_NAVIGATOR){
			// No ribbon. The navigator replaces the standard navbar
		}elseif($this->playerLayout==LearningCourse::$PLAYER_LAYOUT_PLAY_BUTTON) {
			$this->render('quickNavigationBand', array(
				'navObjects'         => $navObjects,
				'carouselItemsCount' => $carouselItemsCount,
				'objectsPerSlide'    => $this->objectsPerSlide,
			));
		}
	}
	
	
	
	
	
}
