<?php

/**
 * Widget to display object status graph
 */
class TestReportsAnswerFormatter extends CWidget {

	public $isForNotifications;
	public $questionType;
	public $trackId;
	public $questId;
	public $isTeacher;
	public $testId;
	public $userId;
	public $courseId;
	public $showCorrectAnswers;
	public $showAnswers;
	public $skipNewLine;
	public $skipLinkInEvaluationBtn;

	protected $answers;
	protected $userAnswers;
	protected $score = 0;
	protected $userAnswerCnt;
	protected $correctAnswerCnt;
	protected $questionTrack = false;
	protected $spriteStyle = '';
	protected $spareIconStyle = '';
	protected $correctIconStyle = '';
	protected $incorrectIconStyle = '';


	public function init() {
		$this->userAnswers = LearningTesttrackAnswer::model()->findAllByAttributes(array(
			'idTrack' => $this->trackId,
			'idQuest' => $this->questId,
		));
		$this->answers = LearningTestquestanswer::model()->findAll(array(
			'condition' => 'idQuest = :questId',
			'params' => array(':questId' => $this->questId),
			'order' => 'sequence ASC, idAnswer ASC',
		));
		$this->questionTrack = LearningTesttrackQuest::model()->findByAttributes(array(
			'idTrack' => $this->trackId,
			'idQuest' => $this->questId,
		));
		//calculate user score
		foreach($this->userAnswers as $answer) {
			$this->score += $answer->score_assigned;
		}
		if($this->isForNotifications){
			$this->spriteStyle = 'background-image: url(\''. (Docebo::getRootBaseUrl(true) . "/themes/spt/images/player-sprite.png").'\') !important; width:20px; height:20px; vertical-align:middle;display:inline-block;*display:inline;zoom:1;margin-right: 5px;';
			$this->spareIconStyle = 'display: inline-block; width: 20px; height: 20px; margin-right: 5px;';
			$this->correctIconStyle = 'background-position: -623px -96px; width: 20px; height: 20px; margin-top: -5px;';
			$this->incorrectIconStyle = ' background-position: -655px -96px; width: 20px; height: 20px; margin-top: -5px;';
		}
	}


	public function run() {
		switch ($this->questionType) {
			case LearningTestquest::QUESTION_TYPE_CHOICE:
				$this->formatChoice();
				break;
			case LearningTestquest::QUESTION_TYPE_CHOICE_MULTIPLE:
				$this->formatChoice(true);
				break;
			case LearningTestquest::QUESTION_TYPE_ASSOCIATE:
				$this->formatAssociate();
				break;
			case LearningTestquest::QUESTION_TYPE_EXTENDED_TEXT:
				$this->formatExtendedText();
				break;
			case LearningTestquest::QUESTION_TYPE_FITB:
				$this->formatFITB();
				break;
			case LearningTestquest::QUESTION_TYPE_UPLOAD:
				$this->formatUploadAnswer();
				break;
			case LearningTestquest::QUESTION_TYPE_INLINE_CHOICE:
				$this->formatChoice();
				break;
			case LearningTestquest::QUESTION_TYPE_TEXT_ENTRY:
				$this->formatTextEntry();
				break;
			default:
				$this->formatAnswers();
		}
	}


	private function formatChoice($multiple = false) {
		$input = $multiple ? 'checkbox' : 'radio';
		$this->userAnswerCnt = '<ul '.(($this->isForNotifications)? 'style="list-style-type: none;"' : '' ).'>';
		$userAnswers = array();
		foreach($this->userAnswers as $uAnswer) {
			$userAnswers[$uAnswer->idAnswer] = $uAnswer->user_answer;
		} //collect all user answers - answerId => isChecked

		//check for information about shuffled question ordering, if stored
		$reorderAnswers = false;
		if (!empty($this->questionTrack->more_info)) {
			$moreInfo = CJSON::decode($this->questionTrack->more_info);
			if (is_array($moreInfo) && isset($moreInfo['shuffled_answers_order'])) {
				$reorderAnswers = $moreInfo['shuffled_answers_order'];
			}
		}
		//if information is present, then do reordering
		if (!empty($reorderAnswers)) {
			$tmpAnswers = array();
			foreach ($reorderAnswers as $idAnswer) {
				$found = false;
				$i = 0;
				while (!$found && $i < count($this->answers)) {
					if ($this->answers[$i]->idAnswer == $idAnswer) {
						$found = true;
						$tmpAnswers[] = $this->answers[$i];
					}
					$i++;
				}
			}
			$answers = $tmpAnswers;
		} else {
			$answers = $this->answers;
		}

		$allAreCorrect = true; // keep a flag if at least one incorrect answer exists -> and print the correct one under the score for a reference
		foreach($answers as $answer) {
			//print all available answers
			$checked = false;
			$correct = false;
			if (isset($userAnswers[$answer->idAnswer])) {
				//if the answer is checked/answered
				if ($userAnswers[$answer->idAnswer] == 1) {
					$checked = true;
				}
				if ($userAnswers[$answer->idAnswer] == 1 && $answer->is_correct) {
					//check if it's selected and if correct
					$correct = true;
				}
			}
			$this->userAnswerCnt .= '<li '.(($this->isForNotifications)? 'style="display: block !important;width:auto !important;"' : '' ).'>';
			if ($this->showAnswers)
			{
				if ($checked) {
					$this->userAnswerCnt .= '<span class="p-sprite ' . ($correct ? "check-solid-small-green" : "remove-small-red") . '" style="'.($this->spriteStyle).($correct ? $this->correctIconStyle : $this->incorrectIconStyle).'"></span>';
				} else {
					$this->userAnswerCnt .= '<span class="' . ($answer->is_correct ? "test-answer-spare-icon" : "test-answer-spare-icon") . '" style="'.$this->spareIconStyle.'"></span>';
				}
				if ($this->questionType == LearningTestquest::QUESTION_TYPE_CHOICE || LearningTestquest::QUESTION_TYPE_CHOICE_MULTIPLE) {
					//$trimmedAnswer = trim(preg_replace('/(^([\xA0\xC2]|&nbsp;|\s){1,})|(([\xA0\xC2]|&nbsp;|\s){1,}$)/', '', $answer->answer));
					//NOTE: for some reasons above regex breaks UTF-8 encoding for some characters like 'à', 'è' etc.
					$trimmedAnswer = trim($answer->answer);
				} else {
					$trimmedAnswer = $answer->answer;
				}
				$this->userAnswerCnt .= '<input type="' . $input . '" ' . ($checked ? "checked" : "") . ' disabled><span>' . (($trimmedAnswer != '')? DOCEBO::nbspToSpace($trimmedAnswer) : 'error') . '<span>';
				$this->userAnswerCnt .= '</li>';
				if ($checked && trim($answer->comment) != '') {
					// show comment related to the choice of the user
					$this->userAnswerCnt .= '<li class="review-comment" '.(($this->isForNotifications)? 'style="padding-left:26px;display:block !important;width:auto !important;"' : '' ).'>'
						. '<b>' . Yii::t('standard', '_COMMENTS') . ':</b> ' . Docebo::nbspToSpace($answer->comment)
						. '<div class="block-hr"></div>'
						. '</li>';
				}
			}
			if (($checked && !$correct) || (!$checked && $correct) || (!$checked && $answer->is_correct)) {
				$allAreCorrect &= false;
			}
		}
		$this->userAnswerCnt .= '</ul>';

		if ($this->showCorrectAnswers) {
			//print correct answers if an incorrect one exists
			if (!$allAreCorrect) {
				$this->correctAnswerCnt = (($this->skipNewLine)? '' : '<br/>').'<h6 '.(($this->isForNotifications)? 'style="margin-bottom: 5px; font-size: 14px;"' : '' ).'>' . Yii::t('player', 'Correct answers:') . '</h6>';
				$this->correctAnswerCnt .= '<ul '.(($this->isForNotifications)? 'style="list-style-type: none;"' : '' ).'>';
				foreach($answers as $answer) {
					if ($this->questionType == LearningTestquest::QUESTION_TYPE_CHOICE || LearningTestquest::QUESTION_TYPE_CHOICE_MULTIPLE) {
						//$trimmedAnswer = trim(preg_replace('/(^([\xA0\xC2]|&nbsp;|\s){1,})|(([\xA0\xC2]|&nbsp;|\s){1,}$)/', '', $answer->answer));
						//NOTE: for some reasons above regex breaks UTF-8 encoding for some characters like 'à', 'è' etc.
						$trimmedAnswer = trim($answer->answer);
					} else {
						$trimmedAnswer = $answer->answer;
					}
					$this->correctAnswerCnt .= '<li '.(($this->isForNotifications)? 'style="display: block !important;width:auto !important;"' : '' ).'><input type="' . $input . '" ' . ($answer->is_correct ? "checked" : "") . ' disabled><span class="correct-answer-label">' . (($trimmedAnswer != '')? DOCEBO::nbspToSpace($trimmedAnswer) : 'error') . '</span></li>';
				}
				$this->correctAnswerCnt .= '</ul>';
			}
		}
		$params = array();
		if($this->isForNotifications){
			$params['isForNotifications'] = true;
		}
		$this->render('testReportAnswers', $params);
	}

	private function formatTextEntry() {
		$userAnswers = array();
		foreach($this->userAnswers as $uAnswer) {
			$userAnswers[$uAnswer->idAnswer]['selected'] = $uAnswer->user_answer;
			$userAnswers[$uAnswer->idAnswer]['more_info'] = $uAnswer->more_info;
		}
		$allAreCorrect = true;
		$correct_answer = '';
		$this->userAnswerCnt = '<div class="text-entry-question-answer" '.(($this->isForNotifications)? 'style="padding-left:40px;"' : '').'>';
		if (count($userAnswers)) {
			foreach($this->answers as $answer) {
				if (isset($userAnswers[$answer->idAnswer])) {

					$correct_answer = $answer->answer;
					$is_correct = (strcmp(trim(strtolower(strip_tags($answer->answer))), trim(strtolower(strip_tags($userAnswers[$answer->idAnswer]['more_info']))))  == 0)? true : false;
					if (!$is_correct) $allAreCorrect = false;

					if ($this->showAnswers)
					{
						if($is_correct)
							$this->userAnswerCnt .= '<div class="p-sprite check-solid-small-green" style="'.($this->spriteStyle).($this->correctIconStyle).'"></div>'.htmlentities($userAnswers[$answer->idAnswer]['more_info']);
						else
							$this->userAnswerCnt .= '<div class="p-sprite cross-small-red" style="'.($this->spriteStyle).(($this->isForNotifications)?'background-position: -125px -277px;':'').'"></div>'.htmlentities($userAnswers[$answer->idAnswer]['more_info']);
					}
				} else {
					if ($this->showAnswers)
						$this->userAnswerCnt .= '<div class="test-answer-spare-icon" style="'.$this->spareIconStyle.'"></div>' . Yii::t('standard', '_NO_ANSWER');
				}
				if (isset($userAnswers[$answer->idAnswer]) && trim($answer->comment) != '' && $this->showAnswers) {
					// show comment related to the choice of the user
					$this->userAnswerCnt .= '<div class="review-comment" '.(($this->isForNotifications)? 'style="padding-left:26px;"' : '' ).'>'
						. '<b>' . Yii::t('standard', '_COMMENTS') . ':</b> ' . Docebo::nbspToSpace($answer->comment)
						. '<div class="block-hr"></div>'
						. '</div>';
				}
			}
		} else {
			if ($this->showAnswers)
				$this->userAnswerCnt .= '<div class="p-sprite remove-small-red" style="'.($this->spriteStyle).($this->incorrectIconStyle).'"></div>' . Yii::t('standard', '_NO_ANSWER');
		}
		$this->userAnswerCnt .= '</div>';

		if ($this->showCorrectAnswers) {
			//print correct answers if an incorrect one exists
			if (!$allAreCorrect) {
				$this->correctAnswerCnt = '<br/><h6 '.(($this->isForNotifications)? 'style="margin-bottom: 5px; font-size: 14px;"' : '' ).'>' . Yii::t('player', 'Correct answers:') . '</h6>';
				$this->correctAnswerCnt .= '<ul '.(($this->isForNotifications)? 'style="list-style-type: none;"' : '' ).'>';
				$this->correctAnswerCnt .= '<li '.(($this->isForNotifications)? 'style="display: block !important;width:auto !important;"' : '' ).'><span class="correct-answer-label">' . Docebo::nbspToSpace(htmlentities($correct_answer)) . '</span></li>';
				$this->correctAnswerCnt . '</ul>';
			}
		}
		$params = array();
		if($this->isForNotifications){
			$params['isForNotifications'] = true;
		}
		$this->render('testReportAnswers', $params);
	}

	private function formatAnswers() {
		$userAnswers = array();
		foreach($this->userAnswers as $uAnswer) {
			$userAnswers[$uAnswer->idAnswer]['selected'] = $uAnswer->user_answer;
			$userAnswers[$uAnswer->idAnswer]['more_info'] = $uAnswer->more_info;
		}

		$this->userAnswerCnt = '<div>';
		if (count($userAnswers)) {
			foreach($this->answers as $answer) {
				if ($this->showAnswers)
				{
					if (isset($userAnswers[$answer->idAnswer])) {
						$this->userAnswerCnt .= '<div class="p-sprite ' . ($userAnswers[$answer->idAnswer]['selected'] == 1 && $answer->is_correct ? "check-solid-small-green" : "remove-small-red") . '" style="'.($this->spriteStyle).($userAnswers[$answer->idAnswer]['selected'] == 1 && $answer->is_correct ? $this->correctIconStyle : $this->incorrectIconStyle).'"></div>' . $userAnswers[$answer->idAnswer]['more_info'];
					} else {
						$this->userAnswerCnt .= '<div class="p-sprite remove-small-red" style="'.($this->spriteStyle).($this->incorrectIconStyle).'"></div>' . Yii::t('standard', '_NO_ANSWER');
					}
					if (isset($userAnswers[$answer->idAnswer]) && trim($answer->comment) != '') {
						// show comment related to the choice of the user
						$this->userAnswerCnt .= '<div class="review-comment" '.(($this->isForNotifications)? 'style="padding-left:26px;"' : '' ).'>'
							. '<b>' . Yii::t('standard', '_COMMENTS') . ':</b> ' . Docebo::nbspToSpace($answer->comment)
							. '<div class="block-hr"></div>'
							. '</div>';
					}
				}
			}
		} else {
			if ($this->showAnswers)
				$this->userAnswerCnt .= '<div class="p-sprite remove-small-red" style="'.($this->spriteStyle).($this->incorrectIconStyle).'"></div>' . Yii::t('standard', '_NO_ANSWER');
		}
		$this->userAnswerCnt .= '</div>';
		$params = array();
		if($this->isForNotifications){
			$params['isForNotifications'] = true;
		}
		$this->render('testReportAnswers', $params);
	}


	private function formatAssociate() {
		$userAnswers = array();
		foreach($this->userAnswers as $uAnswer) {
			$userAnswers[$uAnswer->idAnswer]['more_info'] = $uAnswer->more_info;
		}

		$answer_correct = 0;
		$correct_answer = array();

		foreach($userAnswers as $id_answer => $answer_done)
		{
			$ca = LearningTestquestanswer::model()->findByPk($id_answer);
			if($ca && $ca->is_correct == $answer_done)
				$answer_correct++;
			$correct_answer[$id_answer] = $ca->is_correct;
		}

		if($this->showCorrectAnswers && $answer_correct < count($userAnswers))
			$this->correctAnswerCnt .= '<br /><h6 '.(($this->isForNotifications)? 'style="margin-bottom: 5px; font-size: 14px;"' : '' ).'>' . Yii::t('player', 'Correct answers:') . '</h6><ul '.(($this->isForNotifications)? 'style="list-style-type: none;"' : '' ).'>';

		//check for information about shuffled question ordering, if stored
		$reorderAnswers = false;
		if (!empty($this->questionTrack->more_info)) {
			$moreInfo = CJSON::decode($this->questionTrack->more_info);
			if (is_array($moreInfo) && isset($moreInfo['shuffled_answers_order'])) {
				$reorderAnswers = $moreInfo['shuffled_answers_order'];
			}
		}
		//if information is present, then do reordering
		if (!empty($reorderAnswers)) {
			$tmpAnswers = array();
			foreach ($reorderAnswers as $idAnswer) {
				$found = false;
				$i = 0;
				while (!$found && $i < count($this->answers)) {
					if ($this->answers[$i]->idAnswer == $idAnswer) {
						$found = true;
						$tmpAnswers[] = $this->answers[$i];
					}
					$i++;
				}
			}
			$answers = $tmpAnswers;
		} else {
			$answers = $this->answers;
		}

		//start printing answers content
		$this->userAnswerCnt = '<ul '.(($this->isForNotifications)? 'style="list-style-type: none;"' : '' ).'>';
		if (count($userAnswers)) {
			foreach($answers as $answer) {
				$associate = LearningTestquestanswerAssociate::model()->findByPk($userAnswers[$answer->idAnswer]['more_info']);
				$answeredTitle = $associate ? $associate->answer : Yii::t('standard', '_NO_ANSWER');
				if ($this->showAnswers)
				{
					if (isset($userAnswers[$answer->idAnswer])) {
						$this->userAnswerCnt .= '<li '.(($this->isForNotifications)? 'style="display: block !important;width:auto !important;"' : '' ).'><div class="p-sprite ' . ($userAnswers[$answer->idAnswer]['more_info'] == $answer->is_correct ? "check-solid-small-green" : "remove-small-red") . '" style="'.($this->spriteStyle).($userAnswers[$answer->idAnswer]['more_info'] == $answer->is_correct ? $this->correctIconStyle : $this->incorrectIconStyle).'"></div>' . $answer->answer . ' - ' . $answeredTitle . '</li>';
					} else {
						$this->userAnswerCnt .= '<div class="p-sprite remove-small-red" style="'.($this->spriteStyle).($this->incorrectIconStyle).'"></div>' . Yii::t('standard', '_NO_ANSWER');
					}
					if (isset($userAnswers[$answer->idAnswer]) && trim($answer->comment) != '') {
						// show comment related to the choice of the user
						$this->userAnswerCnt .= '<li class="review-comment" '.(($this->isForNotifications)? 'style="padding-left:26px;display: block !important;width:auto !important;"' : '' ).'>'
							. '<b>' . Yii::t('standard', '_COMMENTS') . ':</b> ' . Docebo::nbspToSpace($answer->comment)
							. '<div class="block-hr"></div>'
							. '</li>';
					}
				}

				if($this->showCorrectAnswers && $answer_correct < count($userAnswers))
				{
					$associate = LearningTestquestanswerAssociate::model()->findByPk($correct_answer[$answer->idAnswer]);
					$this->correctAnswerCnt .= '<li '.(($this->isForNotifications)? 'style="display: block !important;width:auto !important;"' : '' ).'>' . $answer->answer . ' - ' . $associate->answer . '</li>';
				}
			}
		} else {
			if ($this->showAnswers)
				$this->userAnswerCnt .= '<div class="p-sprite remove-small-red" style="'.($this->spriteStyle).($this->incorrectIconStyle).'"></div>' . Yii::t('standard', '_NO_ANSWER');
		}

		if($this->showCorrectAnswers && $answer_correct < count($userAnswers))
			$this->correctAnswerCnt .= '</ul>';

		$this->userAnswerCnt .= '</ul>';
		$params = array();
		if($this->isForNotifications){
			$params['isForNotifications'] = true;
		}
		$this->render('testReportAnswers', $params);
	}


	private function formatExtendedText() {
		$userAnswer = null;
		if (is_array($this->userAnswers) && count($this->userAnswers) && isset($this->userAnswers[0]->more_info)) {
			$userAnswer = $this->userAnswers[0]->more_info;
		}

		// This type of question has only one answer, so it's okay to hardcode [0]
		$idQuestion = $this->userAnswers[0]->idQuest;

		$c = new CDbCriteria();
		$c->addCondition('idQuest=:quest');
		$c->params[':quest'] = $idQuestion;
		$c->select = 'score_correct';
		$questModel = LearningTestquestanswer::model()->find($c);
		$maxScore = $questModel ? $questModel->score_correct : FALSE;

		$this->userAnswerCnt = '<ul '.(($this->isForNotifications)? 'style="list-style-type: none;"' : '' ).'>';
		if ($this->showAnswers)
		{
			if ($userAnswer) {
				$this->userAnswerCnt .= '<div class="test-answer-spare-icon" style="'.$this->spareIconStyle.'"></div>' . $userAnswer;
			} else {
				$this->userAnswerCnt .= '<div class="test-answer-spare-icon" style="'.$this->spareIconStyle.'"></div>' . Yii::t('standard', '_NO_ANSWER');
			}
		}
		$this->userAnswerCnt .= '</ul>';
		$params = array('max_score' => $maxScore);
		if($this->isForNotifications){
			$params['isForNotifications'] = true;
		}
		$this->render('testReportAnswers', $params);
	}

	private function formatFITB(){

		$data = array();
		$systemsAnswers = $this->answers;
		$userAnswers = $this->userAnswers;
		$tmpUserAnswers = array();
		foreach($userAnswers as $key => $userAnswer){
			$tmpUserAnswers[$userAnswer->idAnswer] = $userAnswer;
		}

		foreach($systemsAnswers as $systemAnswer){
			$inputs = '';
			$comment = '';
			$userAnswer = isset($tmpUserAnswers[$systemAnswer->idAnswer]) ? $tmpUserAnswers[$systemAnswer->idAnswer] : null;
			if($userAnswer !== null && $userAnswer->user_answer){
				$correct = QuestionFillInTheBlank::isAnswerCorrect($systemAnswer->idAnswer, $this->trackId);
				// We need to check if we should check if all the answers of that question are answered CORRECT
				$style = $correct ? 'correct' : 'incorrect';
				$inputs = '<span class="'. $style .'" '.(($this->isForNotifications)? (($correct)?'style="color: #5EBE5D;font-weight: bold;"' : 'style="color:#FF0103;font-weight: bold;"' ) : '' ).'>' . $userAnswer->more_info . '</span>';
			}
			$data[] = array(
				'code' => '[answ '.$systemAnswer->sequence.']',
				'answers' => $inputs,
				'correct' => $systemAnswer->answer,
				'comment' => $systemAnswer->comment
			);
		}
		$dataProvider = new CArrayDataProvider($data, array('pagination' => false));
		if($this->isForNotifications){
			$columns = array(
					array(
							'header' => Yii::t('test', 'Answers'),
							'headerHtmlOptions' => array('style'=>'text-align: left;padding: 9px 15px;text-transform: uppercase;border-bottom: 1px solid #ddd;'),
							'value' => function ($data) {
								echo isset($data['answers']) ? $data['answers'] : '';
							},
							'visible' => $this->showAnswers,
							'htmlOptions' => array('style'=>'text-align: left;padding: 9px 15px;border-bottom: 1px solid #ddd;'),
					),
					array(
							'header' => Yii::t('test', 'Correct answers'),
							'headerHtmlOptions' => array('style'=>'text-align: left;padding: 9px 15px;text-transform: uppercase;border-bottom: 1px solid #ddd;'),
							'value' => function ($data) {
								echo isset($data['correct']) ? str_replace(',', ', ', $data['correct']) : '';
							},
							'visible' => $this->showCorrectAnswers,
							'htmlOptions' => array('style'=>'text-align: left;padding: 9px 15px;border-bottom: 1px solid #ddd;'),

					),
					array(
							'header' => Yii::t('standard', '_COMMENTS'),
							'headerHtmlOptions' => array('style'=>'text-align: left; padding: 9px 15px;text-transform: uppercase;border-bottom: 1px solid #ddd;'),
							'value' => function ($data) {
								echo isset($data['comment']) ? $data['comment'] : '';
							},
							'htmlOptions' => array('style'=>'text-align: left;padding: 9px 15px;border-bottom: 1px solid #ddd;'),
					),
			);
		}else {
			$columns = array(
					array(
							'header' => Yii::t('test', 'Answers'),
							'value' => function ($data) {
								echo isset($data['answers']) ? $data['answers'] : '';
							},
							'visible' => $this->showAnswers
					),
					array(
							'header' => Yii::t('test', 'Correct answers'),
							'value' => function ($data) {
								echo isset($data['correct']) ? str_replace(',', ', ', $data['correct']) : '';
							},
							'visible' => $this->showCorrectAnswers
					),
					array(
							'header' => Yii::t('standard', '_COMMENTS'),
							'value' => function ($data) {
								echo isset($data['comment']) ? $data['comment'] : '';
							}
					),
			);
		}

		$params = array('dataProvider' => $dataProvider, 'columns' => $columns);
		if($this->isForNotifications){
			$params['isForNotifications'] = true;
		}
		$this->render('testReportFITBAnswers', $params);
	}

	private function formatUploadAnswer() {

		if(isset($this->courseId))
			$idCourse = $this->courseId;
		else
			$idCourse = $this->getOwner()->courseModel->idCourse;
		$idTest = $this->testId;
		$idQuestion = $this->questId;
		$idTrack = $this->trackId;

		$c = new CDbCriteria();
		$c->addCondition('idQuest=:quest');
		$c->params[':quest'] = $idQuestion;
		$c->select = 'score_correct';
		$questModel = LearningTestquestanswer::model()->find($c);
		$maxScore = $questModel ? $questModel->score_correct : FALSE;

		$this->userAnswerCnt = '<ul '.(($this->isForNotifications)? 'style="list-style-type: none;"' : '' ).'>';
		if ($this->showAnswers)
		{
			if (isset($this->userAnswers[0]->more_info) && !empty($this->userAnswers[0]->more_info)) {
				$this->userAnswerCnt .= '<div class="test-answer-spare-icon" style="'.$this->spareIconStyle.'"></div>'.(($this->skipLinkInEvaluationBtn)? $this->userAnswers[0]->more_info : '<a href="' . Docebo::createLmsUrl('test/player/downloadUpQuestAnswer', array(
						'course_id' => $idCourse,
						'id_test' => $idTest,
						'idQuestion' => $idQuestion,
						'idTrack' => $idTrack,
						'idUser' => $this->userId,
					)) . '">' . $this->userAnswers[0]->more_info . '</a>');
			} else {
				$this->userAnswerCnt .= '<div class="test-answer-spare-icon" style="'.$this->spareIconStyle.'"></div>' . Yii::t('standard', '_NO_ANSWER');
			}
		}
		$this->userAnswerCnt .= '</ul>';
		$params = array('max_score' => $maxScore);
		if($this->isForNotifications){
			$params['isForNotifications'] = true;
		}
		$this->render('testReportAnswers', $params);
	}

}