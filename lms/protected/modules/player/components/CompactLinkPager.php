<?php
/**
 * This class provide a custom look and feel for CListView's pagination
 * Used in widgets for now
 */
class CompactLinkPager extends CLinkPager
{
	const CSS_DISABLED_PAGE='disabled';

	/**
	 * @var string the CSS class for the hidden page buttons. Defaults to 'disabled'.
	 */
	public $disabledPageCssClass=self::CSS_DISABLED_PAGE;

	/*
	 * @var array of parameters (name=>value) that should be used instead of GET when generating pagination URLs.
	 */
	public $paginationParams = array();

	public function init()
	{
		if($this->nextPageLabel===null)
			$this->nextPageLabel='<i class="icon icon-chevron-right"></i>';
		if($this->prevPageLabel===null)
			$this->prevPageLabel='<i class="icon icon-chevron-left"></i>';
		if($this->firstPageLabel===null)
			$this->firstPageLabel=Yii::t('standard', '_START');
		if($this->lastPageLabel===null)
			$this->lastPageLabel=Yii::t('standard', '_END');
		$this->header='';

		if(!isset($this->htmlOptions['id']))
			$this->htmlOptions['id']=$this->getId();
		if(!isset($this->htmlOptions['class']))
			$this->htmlOptions['class']='compact-pager';

		// setting extra params in the pagination links
		if (!empty($this->paginationParams)) {
			$pages = $this->getPages();
			$pages->params = $this->paginationParams;
			$this->setPages($pages);
		}
	}

	/**
	 * Creates a page button.
	 * You may override this method to customize the page buttons.
	 * @param string $label the text label for the button
	 * @param integer $page the page number
	 * @param string $class the CSS class for the page button.
	 * @param boolean $disabled whether this page button is disabled
	 * @param boolean $selected whether this page button is selected
	 * @return string the generated button
	 */
	protected function createPageButton($label,$page,$class,$disabled,$selected)
	{
		if($disabled || $selected)
			$class.=' '.($disabled ? $this->disabledPageCssClass : $this->selectedPageCssClass);
		return '<li class="'.$class.'">'.CHtml::link($label,$this->createPageUrl($page)).'</li>';
	}

	/**
	 * Creates the page buttons.
	 * @return array a list of page buttons (in HTML code).
	 */
	protected function createPageButtons()
	{
		if(($pageCount=$this->getPageCount())<=1)
			return array();

		list($beginPage,$endPage)=$this->getPageRange();
		$currentPage=$this->getCurrentPage(false); // currentPage is calculated in getPageRange()
		$buttons=array();

		// first page
		$buttons[]=$this->createPageButton($this->firstPageLabel,0,$this->firstPageCssClass,$currentPage<=0,false);

		// prev page
		if(($page=$currentPage-1)<0)
			$page=0;
		$prevPageButton=$this->createPageButton($this->prevPageLabel,$page,$this->previousPageCssClass,$currentPage<=0,false);

		// internal pages
		for($i=$beginPage;$i<=$endPage;++$i)
			$buttons[]=$this->createPageButton($i+1,$i,$this->internalPageCssClass,false,$i==$currentPage);

		// next page
		if(($page=$currentPage+1)>=$pageCount-1)
			$page=$pageCount-1;
		$nextPageButton=$this->createPageButton($this->nextPageLabel,$page,$this->nextPageCssClass,$currentPage>=$pageCount-1,false);

		// last page
		$buttons[]=$this->createPageButton($this->lastPageLabel,$pageCount-1,$this->lastPageCssClass,$currentPage>=$pageCount-1,false);

		$buttons[] = $prevPageButton;
		$buttons[] = $nextPageButton;

		return $buttons;
	}
}
