<?php

class CommontrackBehavior extends CBehavior {

	protected $idUserPropertyName = false;
	protected $idResourcePropertyName = '';
	protected $idOrgPropertyName = '';
	protected $statusPropertyName = '';
	protected $statusConversion = false;
	protected $objectType = '';
    protected $trackParams = array();

	/**
	 * Support field for tables which don't have a status property
	 * (the behavior will attempt to read it when $statusPropertyName field has not been set)
	 * @var string
	 */
	protected $_status = false;

	/**
	 * If set to false, events on commontrack will not be executed
	 * @var boolean
	 */
	protected $disabled = false;

	//get / set

	public function getIdUserPropertyName() {
		return $this->idUserPropertyName;
	}

	public function setIdUserPropertyName($idUserPropertyName) {
		if (!is_string($idUserPropertyName)) { return false; }
		$trim = trim($idUserPropertyName);
		if (!empty($trim)) {
			$this->idUserPropertyName = $trim;
			return true;
		}
		return false;
	}

	public function getIdOrgPropertyName() {
		return $this->idOrgPropertyName;
	}

	public function setIdOrgPropertyName($idOrgPropertyName) {
		if (!is_string($idOrgPropertyName)) { return false; }
		$trim = trim($idOrgPropertyName);
		if (!empty($trim)) {
			$this->idOrgPropertyName = $trim;
			return true;
		}
		return false;
	}

	public function getIdResourcePropertyName() {
		return $this->idResourcePropertyName;
	}

	public function setIdResourcePropertyName($idResourcePropertyName) {
		if (!is_string($idResourcePropertyName)) { return false; }
		$trim = trim($idResourcePropertyName);
		if (!empty($trim)) {
			$this->idResourcePropertyName = $trim;
			return true;
		}
		return false;
	}

	public function getStatusPropertyName() {
		return $this->statusPropertyName;
	}

	public function setStatusPropertyName($statusPropertyName) {
		if (!is_string($statusPropertyName)) { return false; }
		$trim = trim($statusPropertyName);
		if (!empty($trim)) {
			$this->statusPropertyName = $trim;
			return true;
		}
		return false;
	}

	public function getStatusConversion() {
		return $this->statusConversion;
	}

	public function setStatusConversion($statusConversion) {
		if (is_array($statusConversion) && !empty($statusConversion)) {
			$this->statusConversion = $statusConversion;
			return true;
		}
		return false;
	}

	public function getObjectType() {
		return $this->objectType;
	}

	public function setObjectType($objectType) {
		if (!is_string($objectType)) { return false; }
		$trim = trim($objectType);
		if (!empty($trim)) {
			$this->objectType = $trim;
			return true;
		}
		return false;
	}

	public function getStatus() {
		return $this->_status;
	}

	public function setStatus($status) {
		if (is_string($status) && !empty($status)) {
			$this->_status = $status;
			return true;
		}
		return false;
	}

    // allows you to send parameters to the CommonTracker::track() method
    public function setTrackParam($paramName, $paramValue) {
        $this->trackParams[$paramName] = $paramValue;
    }

	//methods
	public function disableCommonTracking() {
		$this->disabled = true;
	}

	public function enableCommonTracking() {
		$this->disabled = false;
	}

	/**
	 * Sometimes we may just need to manually update commontrack status separately from owner's table
	 * @param type $attributes
	 */
	public function updateCommontrack($newStatus = false) {
		$owner = $this->getOwner();

		// Retrieve track table
		$idOrgPropertyName = $this->getIdOrgPropertyName();
		$track = LearningCommontrack::model()->findByPk(array(
			'idTrack' => (int)$owner->getPrimaryKey(),
			'objectType' => $this->objectType,
			'idReference' => $owner->$idOrgPropertyName
		));

		if ($track) {
			if (LearningCommontrack::isValidStatus($newStatus)) { $track->status = $newStatus; }
			if ($newStatus == LearningCommontrack::STATUS_COMPLETED) { $track->last_complete = Yii::app()->localtime->toLocalDateTime(); }
			$track->dateAttempt = Yii::app()->localtime->toLocalDateTime();
			$track->save();
		}
		return true;
	}

	/**
	 * Function to manually update scores, taking care not to overwrite the first score after initialisation
	 * @param float $score
	 * @param float $maxScore
	 * @param int $noScore
	 * @param int $updateStatus
	 * @param int $passMark Must be expressed in a percentage (will later be divided by 100 to compare to incoming result.
	 * @return bool
	 */
	public function updateCommontrackScore($score, $maxScore, $noScore, $updateStatus, $passMark){
		$owner = $this->getOwner();
		$owner->setStatusPropertyName('status');

		// Retrieve track table
		$idOrgPropertyName = $this->getIdOrgPropertyName();
		$track = LearningCommontrack::model()->findByPk(array(
			'idTrack' => (int)$owner->getPrimaryKey(),
			'objectType' => $this->objectType,
			'idReference' => $owner->$idOrgPropertyName
		));

		$oldStatus = $track->status;

		if($track) {
			$firstAttempt = false;
			// Only add first score if score_max hasn't been set yet
			if (empty($track->score_max)){
				if($noScore == 0) {
					$track->first_score = $score;
				}
				$firstAttempt = true;
			}
			if($noScore == 0) {
				$track->score = $score;
				$track->score_max = $maxScore;
				Yii::log('updated score');
			} else {
				Yii::log('did not update score');
			}

			$pct = $score / $maxScore;
			$req = $passMark / 100;
			if($pct < $req){ // Less than the prerequisite pass mark --> failed
				$newStatus = LearningCommontrack::STATUS_FAILED;
			} else { // At least got the pass mark --> passed
				$newStatus = LearningCommontrack::STATUS_COMPLETED;
			}

			if(!$firstAttempt) {
				switch ($updateStatus) {
					case LearningCommontrack::UPDATE_STATUS_ALWAYS:
						$this->status = $newStatus;
						break;
					case LearningCommontrack::UPDATE_STATUS_IMPROVE_ONLY:
						if($oldStatus != LearningCommontrack::STATUS_COMPLETED){
							$this->status = $newStatus;
						} else {
							$this->status = LearningCommontrack::STATUS_COMPLETED;
						}
						break;
					case LearningCommontrack::UPDATE_STATUS_NEVER:
					default:
						$this->status = $oldStatus;
						break;
				}
			} else { // In case of a first attempt, we always want to add a status
				$this->status = $newStatus;
			}
			$track->save();
			return true;
		} else {
			return false;
		}
	}



	/**
	 * Declares events and the corresponding event handler methods.
	 * @return array events (array keys) and the corresponding event handler methods (array values).
	 */
	public function events() {
		return array(
			'onAfterSave' => 'afterSave',
			'onBeforeDelete' => 'beforeDelete'
		);
	}

	/**
	 * This event raises after owner saved.
	 * It saves data in learning_commontrack table.
	 * @param CEvent $event - event instance.
	 */
	public function afterSave($event) {

		$owner = $this->getOwner();

		if (method_exists($owner, 'updateCommontrack')) {
			$this->disabled = !$owner->updateCommontrack();
		}

		if ($this->disabled) { return; }

		$idUserPropertyName = $this->getIdUserPropertyName();
		$idResourcePropertyName = $this->getIdResourcePropertyName();
		$statusPropertyName = $this->getStatusPropertyName();
		$idOrgPropertyName = $this->getIdOrgPropertyName();

		$idTrack = $owner->getPrimaryKey();
		$idUser = ($idUserPropertyName ? $owner->$idUserPropertyName : Yii::app()->user->id);
		$idResource = $owner->$idResourcePropertyName;
		$status = (!empty($statusPropertyName) ? $owner->$statusPropertyName : $this->getStatus());
		$idOrg = $owner->$idOrgPropertyName;

		if (!$status) {
			throw new CException('Status value has not been defined'); //... or use a standard value instead?
		}

		//some objects track their status in a different way than commontrack, so we have to provide a "conversion table"
		if (is_array($this->statusConversion) && !empty($this->statusConversion)) {
			if (isset($this->statusConversion[$status])) {
				$status = $this->statusConversion[$status];
			}
		}

		// Just get out if LO is not valid
		$org = LearningOrganization::model()->findByPk($idOrg);
		if (!$org) {
			Yii::log("Attempt to track object (".$idResource.",".$this->getObjectType().") which is invalid", CLogger::LEVEL_ERROR);
			return;
		}

		// Just get out if User does not exists
		if (!CoreUser::model()->exists('idst=:idst', array(':idst' => $idUser))) {
			Yii::log("Attempt to track activity of non-existing user: " . (int) $idUser, CLogger::LEVEL_ERROR);
			return;
		}
		
		if (!CommonTracker::track($org->idOrg, $idUser, $status, $this->trackParams)) {
			throw new CException('CommonTracker: '.Commontracker::getError());
		}
	}

	/**
	 * This event raises befaore owner deleted.
	 * It saves data in learning_commontrack table.
	 * @param CEvent $event - event instance.
	 */
	public function beforeDelete($event) {

		if ($this->disabled) { return; }

		$owner = $this->getOwner();

		$idUserPropertyName = $this->getIdUserPropertyName();
		$idResourcePropertyName = $this->getIdResourcePropertyName();
		$idOrgPropertyName = $this->getIdOrgPropertyName();

		$idTrack = $owner->getPrimaryKey();
		$idUser = ($idUserPropertyName ? $owner->$idUserPropertyName : Yii::app()->user->id);
		$idResource = $owner->$idResourcePropertyName;
		$idOrg = $owner->$idOrgPropertyName;

		//retrieve idReference
		$object = LearningOrganization::model()->findByPk($idOrg);
		if (!$object) {
			throw new CException('Invalid resource');
		}

		//effective delete operation
		if (!CommonTracker::delete($object->getPrimaryKey(), $idUser)) {
			throw new CException('CommonTracker: '.Commontracker::getError());
		}
	}

}