<?php

class LearningObjectsBehavior extends CBehavior {
	
	protected $idPropertyName = '';
	protected $titlePropertyName = '';
	
	protected $idCourse = 0;
	protected $idParent = 0;
	protected $shortDescription = '';
	protected $idResource = null;
	protected $objectType = false;

	protected $eventsEnabled = true;

	// get / set

	public function getIdPropertyName() {
		return $this->idPropertyName;
	}
	
	public function setIdPropertyName($idPropertyName) {
		if (!is_string($idPropertyName)) { return false; };
		$trim = trim($idPropertyName);
		if (!empty($trim)) {
			$this->idPropertyName = $trim;
		}
		return false;
	}
	
	public function getTitlePropertyName() {
		return $this->titlePropertyName;
	}
	
	public function setTitlePropertyName($titlePropertyName) {
		if (!is_string($titlePropertyName)) { return false; };
		$trim = trim($titlePropertyName);
		if (!empty($trim)) {
			$this->titlePropertyName = $trim;
		}
		return false;
	}
	
	public function getIdParent() {
		return (int)$this->idParent;
	}
	
	public function setIdParent($idParent) {
		if ((int)$idParent > 0) {
			$this->idParent = (int)$idParent;
			return true;
		}
		return false;
	}
	
	public function getIdCourse() {
		return (int)$this->idCourse;
	}
	
	public function setIdCourse($idCourse) {
		if ((int)$idCourse > 0) {
			$this->idCourse = (int)$idCourse;
			return true;
		}
		return false;
	}
	
	public function setObjectType($objectType) {
		if (array_key_exists($objectType, LearningOrganization::objectTypes())) {
			$this->objectType = $objectType;
			return true;
		}
		return false;
	}
	
	public function getObjectType() {
		return $this->objectType;
	}
	

	public function getShortDescription() {
		return $this->shortDescription;
	}
	
	public function setShortDescription($s) {
		if ($s) {
			$this->shortDescription = $s;
			return true;
		}
		return false;
	}
	
	public function getIdResource() {
		return (int)$this->idResource;
	}
	
	public function setIdResource($input) {
		if ((int) $input > 0) {
			$this->idResource = (int) $input;
			return true;
		}
		return false;
	}
		
	
	public function events() {
		return array(
			'onAfterSave' => 'afterSave',
			'onBeforeDelete' => 'beforeDelete'
		);
	}



	public function disableLearningObjectsEvents() {
		$this->eventsEnabled = false;
	}



	public function enableLearningObjectsEvents() {
		$this->eventsEnabled = true;
	}


	/**
	 * @param $e
	 *
	 * @return bool|LearningOrganization
	 * @throws CDbException
	 * @throws CException
	 */
	public function afterSave($e) {

		if (!$this->eventsEnabled) { return true; }

		$owner = $this->getOwner();
		if (!$owner->isNewRecord) { return true; } //for existing records, no additive operations are needed
		
		$idPropertyName = $this->getIdPropertyName();
		$titlePropertyName = $this->getTitlePropertyName();

		//if (!$owner->hasProperty($idPropertyName)) { throw new CException('Invalid ID property "'.$idPropertyName.'".'); }
		//if (!$owner->hasProperty($titlePropertyName)) { throw new CException('Invalid title property "'.$idPropertyName.'".'); }
		
		$idCourse = $this->getIdCourse();
		$idParent = $this->getIdParent();
		
		// Additional LO parameters, if any
		$lo_params = array();
		if ($this->getShortDescription()) {
			$lo_params['short_description'] = $this->getShortDescription(); 
		}
		if ($this->getIdResource()) {
			$lo_params['resource'] = $this->getIdResource();
		}
		
		
		if ($idCourse <= 0) { throw new CException('Invalid course ID: "'.$idCourse.'".'); }

		if (Yii::app()->db->getCurrentTransaction() === NULL) { $transaction = Yii::app()->db->beginTransaction(); }
		// Create an entry in the learning_organization table
		$lom = new LearningObjectsManager($idCourse);
		$rs = $lom->addLearningObject($this->objectType, $owner->$idPropertyName, $owner->$titlePropertyName, $idParent, $lo_params);

		if (!$rs) {
			if (isset($transaction)) { $transaction->rollback(); }
			throw new CException('Error while saving learning organization data');
			//return false;
		} else {
			if (isset($transaction)) { $transaction->commit(); }
			return $rs;
		}
	}



	public function beforeDelete($e) {
		if (!$this->eventsEnabled) { return true; }
		$owner = $this->getOwner();

		$idResource = ($owner instanceof LearningTcAp)? $owner->mainActivity->getPrimaryKey() : $owner->getPrimaryKey();
		$learningObjects = LearningOrganization::model()->findAllByAttributes(array(
			'idResource' => $idResource,
			'objectType' => $this->objectType
		));

		foreach ($learningObjects as $learningObject) {
			if (Yii::app()->db->getCurrentTransaction() === NULL) { $transaction = Yii::app()->db->beginTransaction(); }

			try {
				//security check, should always be true
				if (!$learningObject->isLeaf()) { throw new CException('Invalid node'); }

				//effective node deletion
				$rs = $learningObject->deleteNode();
				if (!$rs) {
					if (isset($transaction)) { $transaction->rollback(); }
					throw new CException('Unable to delete learning organization record');
				}

				if (isset($transaction)) { $transaction->commit(); }
			
			} catch(CException $e) {
				
				if (isset($transaction)) { $transaction->rollback(); }
				throw $e;
			}
		}
		return true;
	}
}