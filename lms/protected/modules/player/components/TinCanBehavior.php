<?php
/**
 * @TODO: THIS Behavior is pointless. Better just use the LeareningTcAp model directly.
 *
 * Manages all the operation related to create/delete operations on tincan objects,
 * such as files management, activity info, learning organization synch
 */
class TinCanBehavior extends LearningObjectsBehavior  {

	/**
	 * The path of the physical file to upload when saving a tincan object
	 * @var bool/string
	 */
	protected $_file = false;



	// get / set

	public function getFile() {
		return $this->_file;
	}

	public function setFile($fileName) {
		if (!$this->getOwner()->isNewRecord) { throw new CException('Can\'t assign a file for an already existing object'); } //...or just ignore it and return?
		if (!is_string($fileName)) { return false; };
		$trim = trim($fileName);
		if (!empty($trim)) {
			$this->_file = $trim;
		}
		return false;
	}



	//methods

	public function events() {
		return array(
			'onBeforeSave' => 'beforeSave',
			'onAfterSave' => 'afterSave',
			'onBeforeDelete' => 'beforeDelete'
		);
	}



	public function beforeSave() {
		$owner = $this->getOwner();
		if (!$owner->isNewRecord) { return true; } //files are already created for this object, don't do anything else
		$fileName = $this->getFile();
		if (empty($fileName)) { return false; }
		// remove extension and replace spaces with underscores
		// resulting dirname will be smth like: 12301_maritime_scorm_1369384664_zip
		// TO DO: use a preg_replace() to replace all unallowed characters
		$owner->path = Yii::app()->user->id . '_' . time() . '_' .  str_replace(array('.', ' '), array('_', '_'), $fileName);
	}



	public function afterSave($e) {

		$owner = $this->getOwner();

		if (!$owner->isNewRecord) { return true; } //files are already created for this object, don't do anything else

		$fileName = $this->getFile();
		if (empty($fileName)) { throw new CException('Invalid file specification'); }
		$cleanUp = false;

		if (Yii::app()->db->getCurrentTransaction() === NULL) { $transaction = Yii::app()->db->beginTransaction(); }

		try {

			// Check if uploaded file exists
			$uploadTmpFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $fileName;
			if (!is_file($uploadTmpFile)) { throw new CException("File '" . $fileName . "' does not exist."); }

			// verify that the file is a zip file
			if ('zip' != strtolower(CFileHelper::getExtension($fileName))) {
				throw new CException("Invalid file format.");
			}


			/***********************/

			// Exttract TinCan package content and store to file storage
			if (!$this->owner->extractTincanPackage($uploadTmpFile, $owner->path)) {
				throw new CException('Error while extracting TinCan package');
			}

			if($owner->scenario !== 'centralRepo') {
				$rs = parent::afterSave($e);
				if (!$rs) {
					throw new CException('Error while saving learning organization data');
				}
			}

			//all went ok: commit DB changes
			if (isset($transaction) && $transaction->getActive()) { $transaction->commit(); }


			/***********************/


		} catch (CException $e) {
			//check transaction
			if (isset($transaction)  && $transaction->getActive()) { $transaction->rollback(); }
			//pass the exception at above level
			throw $e;
		}

		return true;
	}



	public function beforeDelete($e) {

		$owner = $this->getOwner();

		if (Yii::app()->db->getCurrentTransaction() === NULL) { $transaction = Yii::app()->db->beginTransaction(); }

		try {

			//delete organization data
			$rs = parent::beforeDelete($e);
			if (!$rs) {
				throw new CException('Error while deleting organization data');
			}

			//delete activity data
			$rs = LearningTcActivity::model()->deleteAllByAttributes(array('id_tc_ap' => $owner->getPrimaryKey()));
			if (!$rs) {
				throw new CException('Error while deleting activity data');
			}

			if(LearningTcAp::model()->countByAttributes(array('path'=>$owner->path)) <= 1){
				// If the same path is used for more than 1 TinCan package
				// this means that a LO was copied (e.g. using "Duplicate course (with LOs)")
				// and it uses the same tincan package files, so in this case
				// we only remove the DB rows and not the actual files
				// from storage so the other LO that depends on don't break

				//deleted stored files (the folder)
				$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_TINCAN);
				$rs = $storage->removeFolder($owner->path);
				if (!$rs) {
					throw new CException('Error while removing TC directory "'.$owner->path.'"');
				}
			}

			if (isset($transaction)) { $transaction->commit(); }

		} catch (CException $e) {

			//check transaction
			if (isset($transaction)) { $transaction->rollback(); }

			//pass the exception at above level
			throw $e;
		}

		return true;
	}

}