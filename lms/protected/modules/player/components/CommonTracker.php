<?php
/**
 * Component providing methods to do Common Track: tracking all type 
 * of Learning Objects regarding their Common behaviour.
 * 
 * Saves tracking information in CommonTrack model(s)
 * 
 *
 */ 
class CommonTracker extends CComponent {
    

    public static $ATTEMPTS_COUNTING_LO = array(
        LearningOrganization::OBJECT_TYPE_TEST,
        LearningOrganization::OBJECT_TYPE_SCORMORG,
        LearningOrganization::OBJECT_TYPE_TINCAN,
        LearningOrganization::OBJECT_TYPE_AICC,
    );
    
    

	// Set this if error has been found
	protected static $error = false;
	
	/**
	 * 
	 * @param integer $idReference
	 * @param integer $idUser
	 * @param array $params
	 */
	public function __construct() {
		
	}



	//error management functions

	protected static function setError($message) {
		if (empty($message)) {
			self::clearError ();
			return;
		} 
		if (!is_string($message)) { $message = 'Unknown error'; }
		self::$error = $message;
	}

	protected static function clearError() {
		self::$error = false;
	}

	public static function hasError() {
		return (!empty(self::$error));
	}

	public static function getError() {
		return self::$error;
	}



	/**
	 * Track LO by Id, User Id
	 * 
	 * @param integer $idReference the ID of the learning object (from learning_organization table)
	 * @param integer $idUser the ID of the user tracked
	 * @param string $status the track status to set
	 * @param array $otherParams (facultative) other parameters to set in the commontrack record
	 */
	public static function track($idReference, $idUser, $status, $otherParams = array()) {
		$firstObjectCompletion = false;
		self::clearError();
		$tackingDeliverable = false ;
		
		//validate input parameters
		if ($idReference <= 0) { self::setError("Invalid value for learning object ID"); return false; }
		if ($idUser <= 0) { self::setError("Invalid value for user ID"); return false; }
		if (!LearningCommontrack::isValidStatus($status)) { self::setError("Invalid status: ".$status); return false; }

		// retrieve information on the learning object (we need object type and track ID)
		$organization = LearningOrganization::model()->findByPk($idReference);
		if (!$organization) { self::setError("Invalid learning object reference: ".$idReference); return false; }

		/**
		 * We always count and store in common-tracking table the FAILED attempts taken, for all Learning objects.
		 * Used IF and WHEN a lo_max_attempts is set at course level to limit number of fails for LOs.
		 * 
		 * However: SCORM is a bit special (due to very specific implementation).
		 * When Scorm tracking code calls track() it must specify if FAILED attempt must be counted.
		 * @see ScormApi12
		 * @see ScormApi2004
		 */
		$countFailedAttempt = true;
		if ($organization->objectType == LearningOrganization::OBJECT_TYPE_SCORMORG) {
		    $countFailedAttempt = (isset($otherParams['countFailedAttempt'])) ? (bool) $otherParams['countFailedAttempt'] : false;
		}

		// By default, allow Status change
		$doUpdateStatus = true;

		//retrieve track record, if exists
		$track = LearningCommontrack::model()->findByAttributes(array('idUser' => (int)$idUser, 'idReference' => (int)$idReference));
		if (!$track) { //object has not been tracked yet: do it now, insert a new record in learning_commontrack
			$idTrack = false;
			switch ($organization->objectType) { //this shouldn't be used here, but I don't have other ways to retrieve the idTrack value
				
				case LearningOrganization::OBJECT_TYPE_AICC: {
					$package = $organization->aiccPackage;
					$specificTrack = LearningAiccPackageTrack::model()->findByAttributes(array('id_package' => $package->id,'idUser' => $idUser));
					if ($specificTrack) { $idTrack = $specificTrack->id; }
				} break;
				
				case LearningOrganization::OBJECT_TYPE_SCORMORG: {
					$specificTrack = LearningScormItemsTrack::model()->findByAttributes(array('idscorm_item' => NULL,'idUser' => $idUser, 'idReference' => $idReference));
					if ($specificTrack) { $idTrack = $specificTrack->idscorm_item_track; }
				} break;
				case LearningOrganization::OBJECT_TYPE_TEST: {
					$specificTrack = LearningTesttrack::model()->findByAttributes(array('idUser' => $idUser, 'idReference' => $idReference));
					if ($specificTrack) { $idTrack = $specificTrack->idTrack; }
				} break;
				case LearningOrganization::OBJECT_TYPE_POLL: {
					$specificTrack = LearningPolltrack::model()->findByAttributes(array('id_user' => $idUser, 'id_reference' => $idReference));
					if ($specificTrack) { $idTrack = $specificTrack->id_track; }
				} break;
				
				case LearningOrganization::OBJECT_TYPE_VIDEO: {
					$specificTrack = LearningVideoTrack::model()->findByAttributes(array('idUser' => $idUser, 'idReference' => $idReference));
					if ($specificTrack) { $idTrack = $specificTrack->idTrack; }
				} break;
				case LearningOrganization::OBJECT_TYPE_TINCAN: 
			    case LearningOrganization::OBJECT_TYPE_ELUCIDAT:
				    {
					$specificTrack = LearningTcTrack::model()->findByAttributes(array('idUser' => $idUser, 'idReference' => $idReference));
					if ($specificTrack) { $idTrack = $specificTrack->idTrack; }
				} break;
				case LearningOrganization::OBJECT_TYPE_DELIVERABLE:{
					$tackingDeliverable = true;
					$specificTrack = LearningMaterialsTrack::model()->findByAttributes(array('idUser' => $idUser, 'idReference' => $idReference));
					if ($specificTrack) { $idTrack = $specificTrack->idTrack; }
				}break;
				case LearningOrganization::OBJECT_TYPE_LTI:
				case LearningOrganization::OBJECT_TYPE_HTMLPAGE:
				case LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE:
				case LearningOrganization::OBJECT_TYPE_FILE: {
					$specificTrack = LearningMaterialsTrack::model()->findByAttributes(array('idUser' => $idUser, 'idReference' => $idReference));
					if ($specificTrack) { $idTrack = $specificTrack->idTrack; }
				} break;
				case LearningOrganization::OBJECT_TYPE_AUTHORING: {
					$specificTrack = LearningConversion::model()->findByAttributes(array('user_id' => $idUser, 'authoring_id' => $organization->idResource));
					if ($specificTrack) { $idTrack = $specificTrack->track_id; }
				} break;
				default: {
					self::setError('Invalid object type: '.$organization->objectType);
					return false;
				} break;
			}
			if (!$idTrack) { self::setError('Invalid object tracking'); return false; }

			//create the new record
			$track = new LearningCommontrack();
			$track->idUser = (int)$idUser;
			$track->idReference = (int)$idReference;
			$track->idTrack = (int)$idTrack;
			$track->objectType = $organization->objectType;
			$track->firstAttempt = Yii::app()->localtime->toLocalDateTime();
			$track->dateAttempt = Yii::app()->localtime->toLocalDateTime();
			$track->status = $status;

			if($tackingDeliverable){
				$track->first_complete = Yii::app()->localtime->toLocalDateTime();
				$track->last_complete = Yii::app()->localtime->toLocalDateTime();
				$firstObjectCompletion = true;
			}

			if ($status == LearningCommontrack::STATUS_COMPLETED) {
				$track->first_complete = Yii::app()->localtime->toLocalDateTime();
				$track->last_complete = Yii::app()->localtime->toLocalDateTime();
				$firstObjectCompletion = true;
			}
			
			// Record the number of allowed attempts (whataver it may means: failed, passed, etc.),
			// We use the CURRENT lo_max_attempts and it stays forever (or until changed forcefully somewhere)
			// Thus, even if the course lo_max_attempts seting is changed AFTER this tracking event, it stayes the same
			$track->failed_attempts_allowed = $organization->course->lo_max_attempts;
			

		} else {
			if($track->objectType == LearningOrganization::OBJECT_TYPE_DELIVERABLE){
				$track->last_complete = Yii::app()->localtime->toLocalDateTime();
			}
			//update existing common tracking record
			if(Yii::app()->controller->action->id != "axChangeLOUserScore" && Yii::app()->controller->action->id != "editTestScore") {
				$track->dateAttempt = Yii::app()->localtime->toLocalDateTime();
			}
			if ($status == LearningCommontrack::STATUS_COMPLETED) {
				if (!$track->first_complete || ($track->first_complete == '0000-00-00 00:00:00')) {
					$track->first_complete = Yii::app()->localtime->toLocalDateTime();
					$firstObjectCompletion = true;
				}
				$track->last_complete = Yii::app()->localtime->toLocalDateTime();
			}
			
			// But... Prevent Downgrade Logic: if asked to
			$preventDowngrade = isset($otherParams['preventStatusDowngrade']) && ($otherParams['preventStatusDowngrade'] === true);
			if ($preventDowngrade) {
				switch ($track->status) {
					// If current status is COMPLETED|PASSED, we can't change the status (when preventing downgrades)
					case LearningCommontrack::STATUS_COMPLETED:
					case LearningCommontrack::STATUS_PASSED:
						$doUpdateStatus = false;
						break;

					// Allow upgrading from FAILED to COMPLETED|PASSSED only	
					case LearningCommontrack::STATUS_FAILED:
						if (!in_array($status, array(
							LearningCommontrack::STATUS_COMPLETED,
							LearningCommontrack::STATUS_PASSED))) $doUpdateStatus = false;
						break;
					default:
						break;	
				}
			}

			// Are we allowed to change the status
			if ($doUpdateStatus) {
				$track->status = $status;
			}
		}

		if ($countFailedAttempt && ($status == LearningCommontrack::STATUS_FAILED)) {
		    $track->failed_attempts_taken = (int) $track->failed_attempts_taken + 1;
		}
        
		
		// Keep idResource updated (in case a new version is published to this course)
		// and only if we're allowed to update the status in common track
		if ($doUpdateStatus)
			$track->idResource = $organization->idResource;

		// Adjust scores for scored LO types
		switch ($track->objectType) {
			case LearningOrganization::OBJECT_TYPE_AICC: {
				$package = $organization->aiccPackage;
				$lastScoredTrack = $package->getLastItemTrack($idUser, true, true);
				if ($lastScoredTrack) {
					$track->score = (int) $lastScoredTrack->score_raw;
					$track->score_max = ($lastScoredTrack->score_max ? (int) $lastScoredTrack->score_max : 100);
				}
			} break;
			
			case LearningOrganization::OBJECT_TYPE_SCORMORG: {
				// Extract score from learning scorm tracking table, if any
				$criteria = new CDbCriteria();
				$criteria->addCondition('(idReference=:idOrg) AND (idUser=:idUser) AND (score_raw > 0)');
				$criteria->params = array(':idOrg' => $idReference, ':idUser' => $idUser);
				$criteria->order = "idscorm_tracking DESC";
				$scoredItem = LearningScormTracking::model()->find($criteria);
				if ($scoredItem) {
					$track->score = (int) $scoredItem->score_raw;
					$track->score_max = ($scoredItem->score_max ? (int) $scoredItem->score_max : 100);
				}
			} break;
			case LearningOrganization::OBJECT_TYPE_TEST: {
				if (!isset($specificTrack)) { $specificTrack = LearningTesttrack::model()->findByAttributes(array('idUser' => $idUser, 'idReference' => $idReference)); }
				if (!$specificTrack) { throw new CException('Error while tracking test score'); }
				$track->score = $specificTrack->getCurrentScore();
				$test = LearningTest::model()->findByPk($specificTrack->idTest);
				if (!$test) { throw new CException('Error while tracking test score'); }
				$track->score_max = round($test->getMaxScore());
			} break;
			case LearningOrganization::OBJECT_TYPE_DELIVERABLE: {
				// set score
				if (!$specificTrack) {
					$specificTrack = LearningMaterialsTrack::model()->findByAttributes(array('idUser' => $idUser, 'idReference' => $idReference));
				}
				$deliverableObject = LearningDeliverableObject::model()->findByPk($otherParams['deliverableObjectId']);
				$track->score = intval($deliverableObject->evaluation_score);
				$track->score_max = 100;
			} break;
			case LearningOrganization::OBJECT_TYPE_TINCAN:
			case LearningOrganization::OBJECT_TYPE_ELUCIDAT:
			    {
				// At the moment TinCan does common tracking by itself and properly fills scores
			} break;

		}

		//do it !!
		if (!$track->save()) {
			self::setError("Error while saving commontrack for (idUser=".(int)$idUser.",idReference=".(int)$organization->idOrg."); error(s) = ".print_r($track->getErrors()));
			return false;
		}

		if ($firstObjectCompletion) {
			// Raise event to let plugins hook to that
			Yii::app()->event->raise(EventManager::EVENT_STUDENT_COMPLETED_LO, new DEvent(self, array(
				'user' => $track->user,
				'organization' => $track->organization,
				'track'	=> $track,
			)));
		}
		
		return $track;
	}



	/**
	 * Delete common tracking records for specified object (and for a single user, if specified).
	 * No events are fired by model.
	 * @param integer $idReference the ID of the learning object (from learning_organization table)
	 * @param integer $idUser the ID of the user tracked
	 */
	public static function delete($idReference, $idUser = false) {
		$attributes = array('idReference' => (int)$idReference);
		if (!empty($idUser)) { $attributes['idUser'] = (int)$idUser; }
		$rs = LearningCommontrack::model()->deleteAllByAttributes($attributes);
		if ($rs === false) {
			self::setError("Error while deleting commontrack record (idReference=".(int)$idReference.(!empty($idUser) ? ",idUser=".(int)$idUser : "").")");
			return false;
		}
		return $rs;
	}

	/**
	 * Creates slave learning_commontrack rows for users that already took this object version
	 * in other courses (only if shared tracking is enabled)
	 *
	 * @param $objectVersion LearningRepositoryObjectVersion The object version pushed to a course
	 * @param $organization LearningOrganization The course placeholder created
	 * @param $idCourse integer The course where it was pushed
	 */
	public static function createSlaveTracksForPushedObject($objectVersion, $organization, $idCourse) {
		if(!$objectVersion->repositoryLO->shared_tracking)
			return;

		// Search all master tracks for users who already took this object version in other courses
		$masterTracks = Yii::app()->db->createCommand()
			->select("ct.*")
			->from(LearningCommontrack::model()->tableName()." ct")
			->join(LearningCourseuser::model()->tableName()." lcu", "lcu.idUser = ct.idUser AND lcu.idCourse = :idCourse", array(':idCourse' => $idCourse))
			->where("idMasterOrg IS NULL")
			->andWhere("(objectType = :objectType) AND (idResource = :idResource)", array(':idResource' => $objectVersion->id_resource, ':objectType' => $objectVersion->object_type))
			->queryAll();

		// Process all master tracks and create/update slaves
		foreach($masterTracks as $masterTrack) {
			// Check if we don't have already a slave track (e.g. from a previous enrollment)
			$slaveTrack = LearningCommontrack::model()->findByAttributes(array('idUser' => $masterTrack['idUser'], 'idReference' => $organization->idOrg));
			if(!$slaveTrack) {
				$slaveTrack = new LearningCommontrack();
				$slaveTrack->idReference = $organization->idOrg;
				$slaveTrack->idUser = $masterTrack['idUser'];
			}

			$slaveTrack->idTrack = $masterTrack['idTrack'];
			$slaveTrack->objectType = $masterTrack['objectType'];
			$slaveTrack->idMasterOrg = $masterTrack['idReference'];
			$slaveTrack->idResource = $masterTrack['idResource'];
			$slaveTrack->dateAttempt = $masterTrack['dateAttempt'] ? Yii::app()->localtime->toLocalDateTime($masterTrack['dateAttempt']) : null;
			$slaveTrack->status = $masterTrack['status'];
			$slaveTrack->firstAttempt = $masterTrack['firstAttempt'] ? Yii::app()->localtime->toLocalDateTime($masterTrack['firstAttempt']) : null;
			$slaveTrack->first_complete = $masterTrack['first_complete'] ? Yii::app()->localtime->toLocalDateTime($masterTrack['first_complete']) : null;
			$slaveTrack->last_complete = $masterTrack['last_complete'] ? Yii::app()->localtime->toLocalDateTime($masterTrack['last_complete']) : null;
			$slaveTrack->first_score = $masterTrack['first_score'];
			$slaveTrack->score = $masterTrack['score'];
			$slaveTrack->score_max = $masterTrack['score_max'];
			$slaveTrack->total_time = $masterTrack['total_time'];
			$slaveTrack->save();
		}
	}

	/**
	 * This function is invoked in case the admin/instructor requests to changed the version pushed into a central LO object
	 *
	 * @param $oldIdResource integer The idResource of the previously pushed version
	 * @param $organization LearningOrganization The LO where the version switch occured
	 */
	public static function updateTracksAfterVersionSwitch($oldIdResource, $organization) {

		if($organization->idResource == $oldIdResource)
			return;

		$isLoWithSharedTracking = $organization->repositoryObject->shared_tracking;

		$usersInProgressInLo = Yii::app()->db->createCommand()
			->select('*')
			->from(LearningCommontrack::model()->tableName())
			->where("idReference = :idOrg AND objectType = :objectType", array(':idOrg' => $organization->idOrg, ':objectType' => $organization->objectType))
			->andWhere("status IN ('".implode("','", array(LearningCommontrack::STATUS_ATTEMPTED, LearningCommontrack::STATUS_AB_INITIO))."')")
			->queryAll();

		if(!$isLoWithSharedTracking) {
			// === Local tracking LO ===
			foreach ($usersInProgressInLo as $inProgressTrack)
				LearningCommontrack::resetTracking($organization->idOrg, $organization->idCourse, $inProgressTrack['idUser']);
		} else {
			// === Shared tracking LO ===
			foreach ($usersInProgressInLo as $inProgressTrack) {
				$isMasterTrack = !$inProgressTrack['idMasterOrg'];
				$slavesCount = Yii::app()->db->createCommand()
					->select("COUNT(*)")
					->from(LearningCommontrack::model()->tableName())
					->where("idUser = :idUser AND idMasterOrg = :idMasterOrg", array(':idUser' => $inProgressTrack['idUser'], ':idMasterOrg' => $organization->idOrg))
					->queryScalar();

				// Search for a pre-existing track of this user in the new version
				// BUT we need exactly the master TRACK
				$newVersionTrack = Yii::app()->db->createCommand()
					->select("idReference")
					->from(LearningCommontrack::model()->tableName())
					->where(array(
						'AND',
						"idUser = :idUser",
						"idResource = :idResource",
						"objectType = :objectType",
						"idMasterOrg IS NULL"
					), array(
						':idUser' => $inProgressTrack['idUser'],
						':idResource' => $organization->idResource,
						':objectType' => $organization->objectType
					))->queryScalar();

				if ($newVersionTrack) { // Ok... looks like this user has already some progress in the new version. Let's reuse it!
					Yii::app()->db->createCommand()
						->update(LearningCommontrack::model()->tableName(),
							// Which Columns to be updated
							array(
								"idMasterOrg" => $newVersionTrack,
								'idResource'  => $organization->idResource
							),
							"idReference = :idReference AND idUser = :idUser",   // Condition
							array(":idReference" => $inProgressTrack['idReference'], ":idUser" => $inProgressTrack['idUser'])); // Params
				} else {
					// **** We are resetting the track this user in this course and forcing him to restart it ****
					if ($isMasterTrack) {  // We have a master track here
						LearningCommontrack::electNewMasterTrack($inProgressTrack['idReference'], $inProgressTrack['idUser'], $inProgressTrack['objectType']);
						if($slavesCount > 0) {
							// Delete only the common track row (no more needed)
							LearningCommontrack::model()->deleteAllByAttributes(array('idReference' => $inProgressTrack['idReference'], 'idUser' => $inProgressTrack['idUser']));
						} else
							// No slave tracks present ... Reset completely this track and the resource specific ones
							LearningCommontrack::resetTracking($organization->idOrg, $organization->idCourse, $inProgressTrack['idUser']);
					} else
						LearningCommontrack::model()->deleteAllByAttributes(array('idReference' => $inProgressTrack['idReference'], 'idUser' => $inProgressTrack['idUser']));
				}
			}
		}
	}
}