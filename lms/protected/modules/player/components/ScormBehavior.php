<?php
/**
 * Manages all the operation related to create/delete operations on tincan objects,
 * such as files management, activity info, learning organization synch
 *
 * @property boolean $isRemote
 *
 */
class ScormBehavior extends CBehavior {

	const MANIFEST_FILE_NAME = 'imsmanifest.xml';

	/**
	 * The path of the physical file to upload when saving a tincan object
	 * @var bool/string
	 */
	protected $_file = false;

	/**
	 * Holds isRemote setter/getter value: If the SCORM package is a localy available SCORM ZIP or is an already installed playable remote content
	 * @var boolean
	 */
	protected $_isRemoteContent = false;

	/**
	 * this will act as a cache for xml data of manifest file, so that multiple requests won't reload the file
	 * @var SimpleXMLElement/NULL
	 */
	protected $_cacheManifestXml = NULL;

	protected $idCourse = false;

	protected $idParent = false;


	public function getFile() {
		return $this->_file;
	}


	/**
	 * Setter/Getter for object attribute holdeing the type of the content: localy uploaded or remote content,
	 * ready to be played (like Marketplace courses)
	 * @param boolean $value
	 */
	public function setIsRemote($value) {
		$this->_isRemoteContent = $value;
	}
	public function getIsRemote() {
		return $this->_isRemoteContent;
	}


	/**
	 * Set PATH to localy uploaded SCORM ZIP (to be unpacked) or URL to remote SCORM Manifest file (where SCORM is already unpacked)
	 * @param string $fileName  Path or URL
	 * @throws CException
	 * @return boolean
	 */
	public function setFile($fileName) {
		if (!$this->getOwner()->isNewRecord) {
			throw new CException('Can\'t assign a file for an already existing object');
		} //...or just ignore it and return?
		if (!is_string($fileName)) {
			return false;
		};
		$trim = trim($fileName);
		if (!empty($trim)) {
			$this->_file = $trim;
		}
		return false;
	}


	public function getIdParent() {
		return (int)$this->idParent;
	}


	public function setIdParent($idParent) {
		if ((int)$idParent > 0) {
			$this->idParent = (int)$idParent;
			return true;
		}
		return false;
	}


	public function getIdCourse() {
		return (int)$this->idCourse;
	}


	public function setIdCourse($idCourse) {
		if ((int)$idCourse > 0) {
			$this->idCourse = (int)$idCourse;
			return true;
		}
		return false;
	}


	/**
	 * Load the imsmanifest xml file and return a XML object (local or remote)
	 * @return boolean/SimpleXMLElement
	 */
	public function getManifestXml() {

		if (!empty($this->_cacheManifestXml)) {
			return $this->_cacheManifestXml;
		}

		// Read SCORM Manifest:: local or remote (URL)
		if ($this->isRemote) {
			$xml = @simplexml_load_file($this->getFile());
		}
		else {
			$dirName = $this->getOwner()->path;
			if (empty($dirName)) {
				return false;
			}
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_SCORM);
			$storageManager->ignoreEvents = true;
			$fileUrl = $storageManager->fileUrl(self::MANIFEST_FILE_NAME, $dirName);
			$xml = @simplexml_load_file($fileUrl);
		}

		if($xml === false)
			throw new CException(	Yii::t('player', 'Invalid imsmanifest.xml file')
									.'. ' . Yii::t('player', 'Please check if your SCORM package is correct and if the problem persist report the error message to the help desk team'));

		if (!empty($xml)) {
			$this->_cacheManifestXml = $xml;
		}
		return $xml;
	}


	public function events() {
		return array(
			'onBeforeSave' => 'beforeSave',
			'onAfterSave' => 'afterSave',
			'onBeforeDelete' => 'beforeDelete'
		);
	}


	/**
	 * @param $event
	 * @return bool
	 * @throws CException
	 * @throws Exception
	 */
	public function beforeSave($event) {

		$owner = $this->getOwner();

		if (!$owner->isNewRecord) {
			//files are already created for this object, don't do anything else
			return true;
		}

		try {

			// we support also remote (like marketplace) courses, so first we need to identify that case
			if ($this->isRemote) {
				$xml = $this->getManifestXml();
				if (empty($xml)) {
					throw new CException("Invalid manifest format.");
				}
			}
			else {

				//step 1) file uploading

				$fileName = $this->getFile();
				if (empty($fileName)) {
					throw new CException("Invalid package file");
				}

				// Check if uploaded file exists
				$uploadTmpFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $fileName;
				if (!is_file($uploadTmpFile)) {
					throw new CException("File '" . $fileName . "' does not exist.");
				}

				// verify that the file is a zip file
				if ('zip' != strtolower(CFileHelper::getExtension($fileName))) {
					throw new CException("Invalid file format.");
				}

				// remove extension and replace spaces with underscores
				// resulting dirname will be smth like: 12301_maritime_scorm_1369384664_zip
				// TO DO: use a preg_replace() to replace all unallowed characters
				//$unpackDirname = Yii::app()->user->id . '_' . time() . '_' . str_replace(array('.', ' '), array('_', '_'), $fileName);

				// Make the forlder upredictable
				$unpackDirname = Docebo::randomHash();

				$owner->path = $unpackDirname;
				// unpack the zip into the temporary folder
				$unpackPath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $unpackDirname;

				// check if a previous upload exists (the scormorg package directory)
				if (file_exists($unpackPath)) {
					throw new CException("A previous upload exists.");
				}

				// New re-usable method. Lets test it
				$this->owner->extractScormPackage($uploadTmpFile, $unpackDirname);

				//step 2) read data from manifest
				$xml = $this->getManifestXml();
			}

			$owner->idpackage = (string)$xml->attributes()->identifier;

			$organizations = $xml->organizations;
			if (empty($organizations)) {
				throw new CException('Invalid imsmanifest file: no organizations element has been found');
			}
			$defaultOrganization = (string)$organizations->attributes()->default;
			if ($defaultOrganization == '') {
				$children = $organizations->children();
				if (count($children) > 0) {
					$defaultOrganization = (string)$children[0]->attributes()->identifier;
				} else {
					$defaultOrganization = '-resource-';
				}
			}
			$owner->defaultOrg = $defaultOrganization;

			$metadata = $xml->metadata;
			if (empty($metadata)) {
				$owner->scormVersion = '1.2';
			} else {
				$schemaversion = (string)$metadata->schemaversion;
				if (!empty($schemaversion)) {
					if ($schemaversion == '1.2') {
						$owner->scormVersion = '1.2';
					} else {
						$owner->scormVersion = '1.3';
					}
				} else {
					$owner->scormVersion = '1.2';
				}
			}

			//step 3) confirm all
			//if (isset($transaction)) { $transaction->commit(); }

		} catch(CException $e) {

			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);

			//if (isset($transaction)) { $transaction->rollback(); }
			$msg = 'Scorm upload error:<br />
<br />
Platform: '.Yii::app()->createAbsoluteUrl('site/index').'
Uploaded file: '.$uploadTmpFile.'
Error code: '.$e->getCode().'<br />
Error message: '.$e->getMessage().'<br />
Trace: '.$e->getTrace().'<br />
POST: '.nl2br(var_export($_POST, true)).'<br />
GET: '.nl2br(var_export($_GET, true)).'';

			Yii::log($msg, 'mail');

			throw $e;
		}
	}


	/**
	 * Count the number of descendant in the scorm xml organiztion section
	 */
	protected function countDescendants(SimpleXMLElement $item) {
		$output = 0;
		foreach ($item->item as $subItem) {
			//Items that contain children shouldn't contain an "identifierref" attribute.
			if ((string)$subItem->attributes()->identifierref != '' && count($subItem->item) == 0) {
				$output += 1;
			}
			if (count($subItem->item) > 0) {
				$output += $this->countDescendants($subItem);
			}
		}
		return $output;
	}


	/**
	 * Save the items of the scorm organization in db
	 * @param SimpleXMLElement $item
	 * @param $idScormOrganization
	 * @param $idParent
	 * @param $resourcesList
	 * @throws CException
	 */
	protected function saveItems(SimpleXMLElement $item, $idScormOrganization, $idParent, $resourcesList) {

		foreach ($item->item as $subItem) {
			$subAttributes = $subItem->attributes();

			$itemRecord = new LearningScormItems();

			$itemRecord->idscorm_organization = $idScormOrganization;
			$itemRecord->idscorm_parentitem = $idParent;
			$itemRecord->title = $subItem->title; //[0] ?
			$itemRecord->nChild = count($subItem->item);
			$itemRecord->nDescendant = $this->countDescendants($subItem);
			$itemRecord->item_identifier = (string)$subAttributes->identifier;
			$itemRecord->isvisible = (empty($subAttributes->isvisible) || $subAttributes->isvisible == 'true' ? 'true' : 'false');
			$itemRecord->parameters = (string)$subAttributes->parameters;

			$reference = (string)$subAttributes->identifierref;
			$itemRecord->identifierref = $reference;
			$itemRecord->idscorm_resource = (isset($resourcesList[$reference]) ? $resourcesList[$reference] : NULL);

			$adlcpElements = $subItem->children('adlcp', true);
			$itemRecord->adlcp_prerequisites = (string)$adlcpElements->prerequisites;
			$itemRecord->adlcp_maxtimeallowed = (string)$adlcpElements->maxtimeallowed;
			$itemRecord->adlcp_timelimitaction = (string)$adlcpElements->timelimitaction;
			$itemRecord->adlcp_datafromlms = (string)$adlcpElements->datafromlms;
			$itemRecord->adlcp_masteryscore = (string)$adlcpElements->masteryscore;

			$completionThreshold = $adlcpElements->completionthreshold;
			if (!empty($completionThreshold) && $completionThreshold->attributes()->minProgressMeasure) {
				$itemRecord->adlcp_completionthreshold = $completionThreshold->attributes()->minProgressMeasure;
			} else {
				$itemRecord->adlcp_completionthreshold = '';
			}
			//...
			$rs = $itemRecord->save(false);
			if (!$rs) {
				throw new CException('Error while saving SCORM item: ' . $itemRecord->item_identifier);
			}
			if (count($subItem->item) > 0) {
				$this->saveItems($subItem, $idScormOrganization, $itemRecord->getPrimaryKey(), $resourcesList);
			}
		}
	}

	/**
	 * @param $event
	 * @return bool
	 * @throws CException
	 * @throws Exception
	 */
	public function afterSave($event) {
		$owner = $this->getOwner();

		if (!$owner->isNewRecord) {
			//files are already created for this object, don't do anything else
			return true;
		}

		if (!Yii::app()->db->getCurrentTransaction()) {
			$transaction = Yii::app()->db->beginTransaction();
		}

		try {

			$xml = $this->getManifestXml();
			if (!$xml) {
				throw new CException('Invalid manifest');
			}

			$resources = $xml->resources;
			if (empty($resources)) {
				//throw ...
			}

			$resourcesList = array();
			foreach ($resources->children() as $resource) {
				$attributes = $resource->attributes();

				$scormType = $attributes->scormtype;
				if (empty($scormType)) {
					$adlcpAttributes = $resource->attributes('adlcp', true);
					$scormType = (string)$adlcpAttributes->scormtype;
				}

				if((string)$attributes->identifier === '')
					continue;

				$resource = new LearningScormResources();
				$resource->idsco = (string)$attributes->identifier;
				$resource->idscorm_package = $owner->getPrimaryKey();
				$resource->scormtype = $scormType;
				$resource->href = (string)$attributes->href;

				$rs = $resource->save();
				if (!$rs) {
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE') . '. ' . Yii::t('player', 'Please check if your SCORM package is correct and if the problem persist report the error message to the help desk team'));
				}

				$resourcesList[$resource->idsco] = $resource->getPrimaryKey();
			}

			$organizations = $xml->organizations;
			if (empty($organizations)) {
				throw new CException('No organization found');
			}

			foreach ($organizations->children() as $organization) {
				$organizationRecord = new LearningScormOrganizations();
				$organizationRecord->idscorm_package = $owner->getPrimaryKey();
				$organizationRecord->org_identifier = (string)$organization->attributes()->identifier;
				$organizationRecord->title = mb_substr((string)$organization->title, 0 , 100);
				$organizationRecord->nChild = count($organization->item);
				$organizationRecord->nDescendant = $this->countDescendants($organization);
				if($this->owner->getScenario() === 'centralRepo'){
					$organizationRecord->detachBehavior('learningObjects');
				} else{
					$organizationRecord->setIdCourse($this->getIdCourse());
					$organizationRecord->setIdParent($this->getIdParent());
				}

				if (!$organizationRecord->save()) {
					Yii::log('Failed to setup the organization : ' . print_r($organizationRecord->getErrors(), true), 'error');
					throw new CException('Failed to setup the organization');
				}
				//now put in db organization items
				$this->saveItems($organization, $organizationRecord->getPrimaryKey(), NULL, $resourcesList);
			}
			if ($owner->defaultOrg == '-resource-') {
				// TODO: save flat organization with resources
			}

			if (isset($transaction)) {
				$transaction->commit();
			}

		} catch(CException $e) {
			//delete unpacked files
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_SCORM);
			$rs = $storageManager->remove($owner->path);

			if (isset($transaction)) {
				$transaction->rollback();
			}
			throw $e;
		}
	}


	/**
	 * @param $event
	 */
	public function beforeDelete($event) {

		// do nothing here
	}

}
