<?php
/**
 * Component for managing operations on learning_organization table related to learning objects insert/edit/delete
 */
class LearningObjectsManager extends CApplicationComponent
{

	const LO_STORAGE_LOCAL 		= 'local';
	const LO_STORAGE_S3			= 's3';

	const PATH_SEPARATOR = '.';


	/*
	 * some internal cache variables: since some functions may be called multiple times,
	 * avoid waste of computational resources by caching data when possible; cache variables
	 * are static, so they can be accessed by different instances of the class
	 */
	protected static $_cache = array();
	protected static $_cacheEnabled = true;

	/**
	 *
	 * @var int the ID of the course in which we are going to manipulate learning objects
	 */
	protected $idCourse = NULL;


	/**
	 * Set Course Id
	 *
	 * @param int $idCourse
	 */
	public function setIdCourse($idCourse) {
		if ((int)$idCourse > 0) {
			$this->idCourse = (int)$idCourse;
		}
	}

	/**
	 * Get Course Id
	 * @return number
	 */
	public function getIdCourse() {
		return $this->idCourse;
	}



	/**
	 * Constructor
	 * @param integer $idCourse sets the idCourse to use in LOs operations
	 */
	public function __construct($idCourse = 0) {
		$this->setIdCourse($idCourse);
	}



	//cache functions

	public static function enableCache() {
		self::$_cacheEnabled = true;
	}

	public static function disableCache() {
		self::$_cacheEnabled = false;
	}

	public static function isCacheEnabled() {
		return (bool)self::$_cacheEnabled;
	}

	public static function isCacheDisabled() {
		return !self::isCacheEnabled();
	}



	/**
	 * Push data into the $_cache
	 * @param string $key
	 * @param mixed $data
	 */
	protected static function setCache($key, $data) {
		if (!self::isCacheEnabled()) { return true; }
		if (!is_string($key) || empty($key)) { return false; }
		if ($data === NULL) { //assigning a NULL value equates to delete the cached data pointed by $key
			if (isset(self::$_cache[$key])) {
				self::$_cache[$key] = NULL; //this effectively free memory allocation for that variable
				unset(self::$_cache[$key]); //unset array key
			}
		} else {
			self::$_cache[$key] = $data;
		}
		return true;
	}



	/**
	 * Read data from internal $_cache variable
	 * @param string $key
	 * @return mixed
	 */
	protected static function getCache($key) {
		if (!self::isCacheEnabled()) { return NULL; }
		if (!isset(self::$_cache[$key])) { return NULL; }
		return self::$_cache[$key];
	}



	/**
	 * Free all cached data
	 * @return boolean
	 */
	protected static function flushCache() {
		self::$_cache = array();
		return true;
	}


	/**
	 *
	 * @param        $objectType
	 * @param        $idResource
	 * @param string $title object name
	 * @param        $idParent
	 * @param array  $loParams other learning_organization parameters
	 *
	 * @throws CException
	 * @internal param string $object_type object type ('video', 'htmlpage', 'scormorg', etc.)
	 * @internal param int $id_resource object idResource
	 * @internal param int $id_parent folder containing the LO (if NULL or 0, then use root note)
	 * @return boolean|LearningOrganization
	 */
	public function addLearningObject($objectType, $idResource, $title, $idParent, $loParams = array(), $idObject = null) {

		$idCourse = $this->getIdCourse();

		// Create an entry in the learning_organization and related tables

		$parent = null;
		if ($idParent) {
			$parent = LearningOrganization::model()->findByPk($idParent);
		} else {
			$parent = LearningObjectsManager::getRootNode($idCourse);
		}

		if (!$parent) {
			return false;
		}

		//insert new object in database
		$lmodel = new LearningOrganization();

		$lmodel->visible = isset($loParams['visible']) ?  (int) $loParams['visible'] : null;
		$lmodel->title = "".$title;
		$lmodel->objectType = "".$objectType;
		$lmodel->idResource = (int)$idResource;
		$lmodel->idAuthor = Yii::app()->user->id;
		$lmodel->dateInsert = Yii::app()->localtime->toLocalDateTime();
		$lmodel->idCourse = $idCourse;
		$lmodel->resource = isset($loParams['resource']) ?  (int) $loParams['resource'] : null;
		$lmodel->short_description = isset($loParams['short_description']) ? $loParams['short_description'] : null;
		if($idObject){
			$lmodel->id_object = $idObject;
		}
		$lmodel->appendTo($parent);

		$idOrganization = $lmodel->getPrimaryKey();

		if (!$lmodel) return false;
		if (!$idOrganization) return false;

		return $lmodel;
	}



	/**
	 * Update a learning object properties
	 * @param integer $idObject the ID reference of the object
	 * @param array $attributes attributes to update for the object
	 * @return boolean
	 */
	public function updateLearningObjectAttributes($idObject, $attributes) {
		if (!is_array($attributes)) { return false; }
		if (empty($attributes)) { return true; }

		$output = false;
		//TO DO ....

		return $output;
	}



	/**
	 * Delete an object from learning_organization table and related operations. This function can be used either for object and folders (folders + descendants)
	 * @param integer $pk PK of learning_organization
	 * @param array $params some control parameters
	 * @return boolean
	 * @throws CException
	 */
	public static function deleteLearningObject($pk, $params = array()) {

		$object = LearningOrganization::model()->findByPk($pk);
		if (!$object) { return false; }

		//read input parameters
		$preventRootDeleting = (isset($params['preventRootDeleting']) ? (bool)$params['preventRootDeleting'] : true);
		$throwExceptions = (isset($params['throwExceptions']) ? (bool)$params['throwExceptions'] : false);

		//do some preliminary checks
		if ($preventRootDeleting && $object->isRoot()) {
			throw new CException('Root node deleting is not allowed');
		}

		//now start effective deleting operation

		if (Yii::app()->db->getCurrentTransaction() === NULL) {
			$trans = Yii::app()->db->beginTransaction();
		}

		try {

			//find all the descendants and delete objects from specific tables, if any, and delete specific data for everyone
			//since LearningOrganization::deleteNode() uses "deleteAll()" function, no events are fired, so we have to do it manually here (although it's not the best approach)
			$subObjects = ($object->isLeaf() ? array() : $object->descendants()->findAll());
			$subObjects[] = $object; //do the same for the main object too
			$objects_id = array();
				foreach ($subObjects as $subObject) { /* @var $subObject LearningOrganization */
					if(!$subObject->repositoryObject) {
						// Special case for Central LO object -> deleting it means deleting only the placeholder (not the physical resource)
						$modelName = LearningOrganization::objectTypeModel($subObject->objectType);
						if ($modelName && ($modelName != 'LearningAiccPackage')) {
							switch ($modelName) {
								case 'LearningTcAp':
									// TinCan main model is a bit hard to get
									$resource = $subObject->tincanMainActivity->ap;
									break;
								case 'LearningElucidat':
									$resource = LearningElucidat::model()->findByAttributes(array(
										'id_resource' => $subObject->idResource
									));
									break;
								default:
									$resource = $modelName::model()->findByPk($subObject->idResource);
							}

							if (!empty($resource) && !$resource->delete()) {
								//TO DO:
								//- what about rollbacking of object who includes operations such as file deleting, etc. ?
								//- what about objects which automatically handles LearningOrganization delete ? (shouldn't cause problems though, only some eventually unnecessary queries)
								throw new CException('Error while deleting resource (' . $subObject->idResource . ',"' . $subObject->objectType . '")');
							}
						}
					}

					$objects_id[] = $subObject->idOrg;
				}

			//delete the node and its descendants from learning_organization table
			// NOTE: some object types do it already when deleting specific data... just check if it is the case with "isDeletedRecord"
			if (!$object->isDeletedRecord) {
                /**
                 * CLOR - Central Learning Object Repository
                 * NSB - Nested Set Behavior
                 *
                 * First try to delete all LO that are part of CLOR and they are "master" tracks,
                 * because the NSB deletes all child element with deleteAll,
                 * but to be elected new Master track, the process should go thought
                 * beforeDelete of LearningOrganization.
                 *
                 * After we delete the linked LO with CLOR, the NST can safety delete all other elements
                 */
			    $criteria = new CDbCriteria();
                $criteria->join = "JOIN " . LearningRepositoryObject::model()->tableName() . ' lro ON lro.id_object = t.id_object';
                $criteria->join .= " JOIN " . LearningCommontrack::model()->tableName() . ' ct ON ct.idMasterOrg = t.idOrg';
                $criteria->addCondition('t.idCourse = ' . $object->idCourse);
                $criteria->addCondition('lro.shared_tracking = 1');
				$criteria->addInCondition('t.idOrg', $objects_id);

                $repoObjects = LearningOrganization::model()->findAll($criteria);

                if($repoObjects){
                    foreach ($repoObjects as $repoObject){
                        //skip this LO, it will be deleted by deleteNode
                        if($repoObject->idOrg == $object->idOrg) {
                            continue;
                        }

                        /**
                         * @var $repoObject LearningOrganization
                         */
                        $repoObject->delete();
                    }
                }

				if (!$object->deleteNode()) {
					throw new CException('Error while deleting learning object "'.$object->title.'" ('.$object->getPrimaryKey().').');
				}
			}
			// find all prerequisites that contain our deleted idOrg and remove them from database
			$columnsToUpdate = Yii::app()->db->createCommand()
				->select('idOrg, prerequisites')
				->from('learning_organization')
				->where(array('like', 'prerequisites', "%$pk%"))->queryAll();

			foreach ($columnsToUpdate as $key => $column) {
				$values = explode(',', $column['prerequisites']);
				$offset = array_search($pk, $values);
				array_splice($values, $offset, 1);
				$columnsToUpdate[$key]['prerequisites'] = implode(',', $values);
				$command = Yii::app()->db->createCommand()->update(
					'learning_organization',
					array('prerequisites' => $columnsToUpdate[$key]['prerequisites']),
					'idOrg = :id',
					array(':id' => $columnsToUpdate[$key]['idOrg']));
			}

            if (isset($trans)) { $trans->commit(); }

			$output = true;

		} catch (CException $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			if (isset($trans)) { $trans->rollback(); }
			if ($throwExceptions) { throw $e; } //pass exception to above level
			$output = false;
		}

		return $output;
	}

	//some particular cases of learning object deleting
	public static function deleteLearningObjectByIdOrganization($idObject) {
		return self::deleteLearningObject($idObject);
	}

	/**
	 * Return an ordered recordset (by sequence assignment, "path" parameter) of learning objects for a given course
	 * @param integer $idCourse the ID of the course
	 * @return array the list of learning objects (folders included)
	 */
	public static function getCourseLearningObjects($idCourse, $root = false, $showHiddenObjects = true) {

		//validate input
		if ((int)$idCourse <= 0) { return false; }

		$cacheKey = 'courseLearningObjects_'.(int)$idCourse.'_'.($root ? '1_' : '0_').($showHiddenObjects ? 'hidden_included' : 'hidden_excluded');
		$records = self::getCache($cacheKey);
		if ($records === NULL) {

			//Course organization tree may be empty and root node non-existent. If so, this call will create it
			$rootNode = self::getRootNode($idCourse);

			//this is to handle some uncorrect situations
			if ($rootNode->lev != 1) { self::_adjustTreeLevels($idCourse); }

			$statQuery = Yii::app()->db->createCommand()
				->select(array("id_object", "COUNT(id_object) as versions_count"))
				->from(LearningRepositoryObjectVersion::model()->tableName())
				->group("id_object");

			//retrieve all course objects already ordered by iLeft value. A "path" value (built with idOrgs) is provided too
			//TO DO: rewrite query in a more-general way (this one is too mysql-specific)
			$records = Yii::app()->db->createCommand()
				->select("pivot.idOrg, pivot.idParent, pivot.lev, pivot.iLeft, pivot.iRight,
					pivot.title, pivot.description, pivot.short_description, pivot.resource, pivot.objectType, pivot.idResource, pivot.prerequisites,
					pivot.isTerminator, pivot.self_prerequisite, pivot.params_json, pivot.params_json_inherited, pivot.visible, pivot.from_csp, 
					pivot.publish_from, pivot.publish_to,
					pivot.width, pivot.height, lrov.version_name, lrov.create_date, pivot.id_object, pivot.keep_updated, stat.versions_count,
					GROUP_CONCAT(info.idOrg ORDER BY info.iLeft SEPARATOR '.') AS path")
				->from(LearningOrganization::model()->tableName()." pivot")
				->join(LearningOrganization::model()->tableName()." info", "pivot.idCourse = :id_course
					AND info.iLeft <= pivot.iLeft
					AND info.iRight >= pivot.iRight
					".(!$root ? " AND info.lev > 1 " : "").

					(!$showHiddenObjects ?
					" AND pivot.visible = 1
					AND (pivot.publish_from = '0000-00-00 00:00:00' OR pivot.publish_from IS NULL OR pivot.publish_from < '".Yii::app()->localtime->getUTCNow('Y-m-d H:i:s')."')
					AND (pivot.publish_to = '0000-00-00 00:00:00' OR pivot.publish_to IS NULL OR pivot.publish_to > '".Yii::app()->localtime->getUTCNow('Y-m-d H:i:s')."') " : "")

					." AND info.idCourse = :id_course", array(':id_course' => $idCourse))
				->leftJoin(LearningRepositoryObjectVersion::model()->tableName() . ' lrov', 'pivot.objectType = lrov.object_type AND pivot.idResource = lrov.id_resource')
				->leftJoin('(' . $statQuery->text . ') stat', 'pivot.id_object = stat.id_object')
				->group("pivot.idOrg")
				->order("pivot.iLeft ASC")
				->queryAll();

			self::setCache($cacheKey, $records);

		}
		return $records;
	}

	/**
	 * Returns the number of learning objects for a given course (excluding folders)
	 * @param int $idCourse
	 * @param boolean $checkVisibility if set to true, items with "visible" set to 0 will be excluded by counting
	 * @return int
	 */
	public static function getCourseLearningObjectsCount($idCourse, $checkVisibility = false) {
		$count = 0;
		$loArr = self::getCourseLearningObjects($idCourse);
		foreach ($loArr as $lo) {
			if (!$checkVisibility || ($checkVisibility && $lo['visible'] > 0)) {
				// folders have objectType an empty string
				if (!empty($lo['objectType'])) {
					$count++;
				}
			}
		}
		return $count;
	}



	/**
	 * Check if a path is a parent of another path.
	 *
	 * @param string $path  The child path
	 * @param string $parentPath  The "maybe" parent path
	 * @return boolean
	 */
	public static function isParentPath($path, $parentPath)  {
		$parentSegments =  explode(self::PATH_SEPARATOR, $parentPath);
		$pathSegments   =  explode(self::PATH_SEPARATOR, $path);

		$parentCount = count($parentSegments);
		$pathCount = count($pathSegments);

		// Looks like the path is at the same level, so it could not be a child
		if ($pathCount <= $parentCount) {
			return false;
		}

		// All the parent segments must be equal to the respective segmets of the child
		// If even a single segment is different, return false
		for ($i=0; $i < $parentCount; $i++) {
			if ($parentSegments[$i] != $pathSegments[$i]) {
				return false;
			}
		}

		return true;
	}


	/**
	 * Retrieve and cache the list of available Content Services Providers
	 *
	 * @return array|mixed|null
	 */
	public static function getCSPList() {

		//first check if the data is present in the local cache
		$cacheKey = 'LearningObjects_CSP_Info';
		$records = self::getCache($cacheKey);

		//if not then retrieve it from the database and store it in the cache
		if ($records === NULL) {

			//prepare output variable
			$records = array();

			/* @var $db CDbConnection */
			/* @var $cmd CDbCommand */
			$db = Yii::app()->db;
			$cmd = $db->createCommand()
				->select("*")
				->from("lti_providers_info");
			$reader = $cmd->query();
			if ($reader) {
				while ($record = $reader->read()) {
					$records[$record['id']] = $record;
				}
			}

			//store retrieved data in the local cache
			self::setCache($cacheKey, $records);
		}

		return $records;
	}



	/**
	 * Returns a tree-structured array with learning objects of a given course
	 * @param integer $idCourse the ID of the course
	 * @param array $params some options
	 * @return array the tree of LOs
	 */
	public static function getCourseLearningObjectsTree($idCourse, $params = array()) {

		//analyze input parameters
		$showRoot = (is_array($params) && isset($params['showRoot'])) ? (bool)$params['showRoot'] : true;
		$onlyFolders = (is_array($params) && isset($params['onlyFolders'])) ? (bool)$params['onlyFolders'] : false;
		$scormChapters = (is_array($params) && isset($params['scormChapters'])) ? (bool)$params['scormChapters'] : false;
		$userInfo = (is_array($params) && isset($params['userInfo'])) ? (bool)$params['userInfo'] : true;
		$targetUser = (is_array($params) && isset($params['targetUser'])) ? (int)$params['targetUser'] : Yii::app()->user->id;
		$excludeEmptyFolders = (is_array($params) && isset($params['excludeEmptyFolders'])) ? (bool)$params['excludeEmptyFolders'] : false;
		$excludeObjects = (is_array($params) && isset($params['excludeObjects']) && is_array($params['excludeObjects'])) ? $params['excludeObjects'] : array();
		$showHiddenObjects = (is_array($params) && isset($params['showHiddenObjects'])) ? (bool)$params['showHiddenObjects'] : true;
		$aiccChapters = (is_array($params) && isset($params['aiccChapters'])) ? (bool)$params['aiccChapters'] : false;

		//retrieve LOs recordset
		$los = self::getCourseLearningObjects($idCourse, $showRoot, $showHiddenObjects);

		if (!empty($los)) {

			//now transform the recordset in a tree structure
			$output = array();

			if ($userInfo) {

				//retrieve infos about user status and accessibility for learning objects
				$list  = array();  // the list of objects in their [flat] order in the tree
				$types = array();  // idOrg => objectType MAP 
				foreach ($los as $record) {
					if ($record['objectType'] != '') {
						$list[] = (int)$record['idOrg'];
						$types[(int)$record['idOrg']] = $record['objectType'];
					}
				}
				$userStatus = array();
				if (!empty($list)) {
					$userStatus = self::getUserLOsStatus($targetUser, $list);
					$userAccess = self::getUserLOsAccessibility($targetUser, $list);

					// Apply navigation policy
					$courseModel = LearningCourse::model()->findByPk($idCourse);
					if (!empty($courseModel->prerequisites_policy)) {
						// Get list of locked objects (an array of   <integer-id-org> =>  0/1)
						$lockedObjects = self::applyNavigationPolicy($list, $userStatus, $courseModel->prerequisites_policy);
						// Apply locked objects on top of user access so far
						foreach ($userAccess as $tmpIdOrg => $tmpAccess) {
							// If object is locked (value is 0 or NULL, ...), deny access to object
							if (isset($lockedObjects[$tmpIdOrg]) && (int)$lockedObjects[$tmpIdOrg]===0) {
								$userAccess[$tmpIdOrg] = 0;  // deny
							}
						}
					}

					// Apply LO-MAX-FAILED-ATTEMPTS policy, if it iset for the course 
					$userAccess = self::applyMaxAttemptsLocking($targetUser, $list, $types, $userStatus, $userAccess, $courseModel);
					
				}

			} else {
				$userStatus = false;
				$userAccess = false;
			}

			// SCORM data
			$mappedItemIdentifiers = false;
			$scormChaptersList = false;
			if ($scormChapters) {

				//find all scorms in the objects list
				$list = array();
				foreach ($los as $record) {
					if ($record['objectType'] == LearningOrganization::OBJECT_TYPE_SCORMORG) {
						$list[] = (int)$record['idResource'];
					}
				}

				//extract chapters
				if (!empty($list)) {
					if ($userInfo) {
						//user info has been requested, join track information for scorm chapters
						$chaptersData = Yii::app()->db->createCommand()
							->select("si.*, sit.status")
							->from("learning_scorm_items si")
							->leftJoin("learning_scorm_items_track sit", "si.idscorm_item = sit.idscorm_item AND sit.idUser = :iduser", array(
								':iduser' => $targetUser
							))
							->where(array('in', "si.idscorm_organization", $list))
							->order("si.idscorm_item")
							->queryAll();
					} else {
						$criteria = new CDbCriteria();
						$criteria->addInCondition('idscorm_organization', $list)->order("idscorm_item");
						$chaptersData = LearningScormItems::model()->findAll($criteria);
					}
				} else {
					$chaptersData = array(); //if no scorm LOs, then no chapters to extract, assign an empty array manually
				}
				//store chapters in an array variable, grouping them by id organization of the scorm; map item indentifiers too
				$scormChaptersList = array();
				$mappedItemIdentifiers = array();
				foreach ($chaptersData as $row) {
					if (!isset($scormChaptersList[ (int) $row['idscorm_organization']])) {
						$scormChaptersList[(int)$row['idscorm_organization']] = array();
					}
					$scormChaptersList[(int)$row['idscorm_organization']][(int)$row['idscorm_item']] = $row;
					$mappedItemIdentifiers[$row['item_identifier']] = (int)$row['idscorm_item'];
				}
			} 

			// AICC data
			$aiccMappedItemIdentifiers = false;
			$aiccChaptersList = false;
			if ($aiccChapters) {

				//find all AICC in the objects list
				$list = array();
				foreach ($los as $record) {
					if ($record['objectType'] == LearningOrganization::OBJECT_TYPE_AICC) {
						$list[] = (int)$record['idResource'];
					}
				}

				// extract AICC chapters
				if (!empty($list)) {
					if ($userInfo) {
						// user info has been requested, join track information for chapters
						$aiccChaptersData = LearningAiccPackage::model()->itemsData($list, true, $targetUser);
					} else {
						$aiccChaptersData = LearningAiccPackage::model()->itemsData($list);
					}
				} else {
					$aiccChaptersData = array();
				}

				// store chapters in an array variable, grouping them by package ID; map item indentifiers too
				$aiccChaptersList = array();
				$aiccMappedItemIdentifiers = array();
				foreach ($aiccChaptersData as $row) {
					$idPackage = (int) $row['id_package'];
					$idItem = (int)$row['id'];
					if (!isset($aiccChaptersList[$idPackage])) {
						$aiccChaptersList[$idPackage] = array();
					}
					$aiccChaptersList[$idPackage][$idItem] = $row;
					$aiccMappedItemIdentifiers[$row['system_id']] = $idItem;
				}
			}

			//first step: separate different tree levels
			$levels = array();
			foreach ($los as $record) {
				$level = (int)$record['lev'];
				if (!$showRoot) { $level--; } //levels have to start at 1, adjust them if we don't want to show the root node
				if (!isset($levels[$level])) { $levels[$level] = array(); }
				$index = $record['path'].'_'.$record['idOrg']; //it may happen the same-level rows have the same sequence number: use idOrg to make them unique in the array and avoid overriding
				$levels[$level][$index] = $record;
			}

			//now create a tree-structured array
			//use a recursive closure to create a tree-like array, since we start with flat linear data

			/*
			 * this function will navigate folder children in LOs list, checking LO status for the user and deducting a status for the folder
			 */
			$checkFolderStatus = function($parentPath, $level) use (&$checkFolderStatus, &$levels, &$userStatus) {
				if (!is_array($levels) || !isset($levels[$level])) { return false; }
				//prepare output variable
				$output = false;
				//support variables
				$allCompleted = true; //it will be put at false if some non-completed objects are detected (or no objects at all)
				$someCompleted = false;
				$someStarted = false;
				$someAbInitio = false;
				$countObjects = 0;
				//check all the subobjects/subfolders recursively
				foreach ($levels[$level] as $index => $row) {
					list($path, $idOrg) = explode('_', $index);
					//if (($parentPath == '' && $level ==1) || (strpos($path, $parentPath) === 0)) {
					if (($parentPath == '' && $level ==1) || (LearningObjectsManager::isParentPath($path, $parentPath))) {

						$countObjects++;
						//get object/folder status
						if ($row['objectType'] == '') {
							$_status = $checkFolderStatus($path, $level+1);
						} else {
							$_status = (isset($userStatus[(int)$row['idOrg']]) ? $userStatus[(int)$row['idOrg']] : false);
						}
						//elaborate object status and update general folder status
						switch ($_status) {
							case LearningCommontrack::STATUS_COMPLETED : {
								$someCompleted = true;
							} break;
							case LearningCommontrack::STATUS_ATTEMPTED : {
								$someStarted = true;
								if ($row['objectType'] == '') {
									$someCompleted = true; //propagate the "yellow" state to upper level folders
								}
								$allCompleted = false;
							} break;
							case LearningCommontrack::STATUS_AB_INITIO : {
								$someAbInitio = true;
								$allCompleted = false;
							} break;
							default: {
								//no status or subfolder objects detected
								$allCompleted = false;
							} break;
						}
					}
				}
				//elaborate variables and calculate output value
				if ($countObjects > 0) { //otherwise false will be returned
					if ($allCompleted) {
						//all the LOs in the folder has been completed
						$output = LearningCommontrack::STATUS_COMPLETED;
					} elseif ($someCompleted) {
						//some LOs in the folder has been completed, but not all of them
						$output = LearningCommontrack::STATUS_ATTEMPTED;
					} elseif ($someAbInitio || $someStarted) {
						//there are no completed LOs in the folder
						$output = LearningCommontrack::STATUS_AB_INITIO; //or shall we just return false ?
					}
				}
				//return status value
				return $output;
			};

			/*
			 * this function checks if a folder contains some LOs or not (return true if empty, false if some objects are contained)
			 */
			$checkFolderEmptyness = function($parentPath, $level) use (&$checkFolderEmptyness, &$levels) {
				if (!is_array($levels) || !isset($levels[$level])) { return true; } //no objects to check
				//prepare output variable
				$output = true;
				//check all the subobjects/subfolders recursively
				foreach ($levels[$level] as $index => $row) {
					list($path, $idOrg) = explode('_', $index);
					//if (($parentPath == '' && $level ==1) || (strpos($path, $parentPath) === 0)) {
					if (($parentPath == '' && $level ==1) || (LearningObjectsManager::isParentPath($path, $parentPath))) {
						//get object/folder status
						if ($row['objectType'] == '') {
							$output = $checkFolderEmptyness($path, $level+1); //check for subfolders emptyness
						} else {
							$output = false; //a non-folder LO has been detected
						}
						if (!$output) { break; } //exit from foreach cycle, no more checks are needed
					}
				}
				//return response
				return $output;
			};

			$sortAdjust = 0; //this variable help keeping sorting info consistent while manually adding nodes (e.g. scorm chapters, when requested)

			/*
			 * this function will extract scorm items and put them into the tree structure
			 */
			$setScormItems = function(&$organizationInfo, &$parentNode) use (&$setScormItems, &$scormChaptersList, &$mappedItemIdentifiers, &$sortAdjust, $userInfo, $userAccess) {
				$idOrganization = (int)$organizationInfo['idOrg'];
				$idResource = (int)$organizationInfo['idResource'];
				$iLeft = (int)$organizationInfo['iLeft'];
				$path = $organizationInfo['path'];
				if (isset($scormChaptersList[$idResource]) && !empty($scormChaptersList[$idResource])) {

					$parentItem = (isset($parentNode['idScormItem']) ? (int)$parentNode['idScormItem'] : -1);
					$hasChildren = false;

					// Count number of chapters in this SCORM LO
					$countChapters = count($scormChaptersList[$idResource]);

					foreach ($scormChaptersList[$idResource] as $chapter) {
						if (($parentItem <= 0 && empty($chapter['idscorm_parentitem'])) || ($chapter['idscorm_parentitem'] == $parentItem)) {


							if (!$hasChildren) {
								$hasChildren = true;
								if (empty($parentNode['idResource'])) { $parentNode['isFolder'] = true; }
							}

							if (!isset($parentNode['children'])) { $parentNode['children'] = array(); }

							$sortAdjust++;

							// Fix LO parameters in JSON (quick fix)
							if (empty($organizationInfo['params_json'])) {
								$organizationInfo['params_json'] = "{}";
							}
							$params_array = CJSON::decode($organizationInfo['params_json']);
							if (!is_array($params_array) || empty($params_array)) {
								$params_json_inherited = CJSON::decode($organizationInfo['params_json_inherited']);
								if(is_array($params_json_inherited)){
									$params_array = $params_json_inherited;
								} else{
									$params_array = array();
								}
							}

							$shortenedTitle = mb_strlen($chapter['title'], 'UTF-8') > 70 ? mb_substr($chapter['title'], 0, 70 - 3, 'UTF-8').'...' : $chapter['title'];

							$chapterNode = array(
								'level' => $chapter['lev']+1,
								'key' => $idOrganization.':'.$chapter['idscorm_item'],
								'idOrganization' => $idOrganization,
								'idResource' => $idResource,
								'idScormItem' => $chapter['idscorm_item'],
								'title' => $chapter['title'],
								'scormTitle' => $organizationInfo['title'],
								//'title' => $shortenedTitle,
								'title_shortened' => $shortenedTitle,
								'isFolder' => empty($idResource) || ((int) $chapter['nChild'] > 0 || ((int) $chapter['nDescendant'] > 0)),
								'type' => 'sco', //change it ?
								'sort' => $iLeft + $sortAdjust,
								'path' => $path.':'.$chapter['idscorm_item'],
								'prerequisites' => $chapter['adlcp_prerequisites'],
								'maxTimeAllowed' => $chapter['adlcp_maxtimeallowed'],
								'timeLimitAction' => $chapter['adlcp_timelimitaction'],
								'dataFromLms' => $chapter['adlcp_datafromlms'],
								'masteryScore' => $chapter['adlcp_masteryscore'],
								'identifier' => $chapter['item_identifier'],
								'idResource' => $chapter['idscorm_resource'],
								'identifierRef' => $chapter['identifierref'],
								'self_prerequisite' => $organizationInfo['self_prerequisite'],
								'is_visible' => (int) $organizationInfo['visible'],
								'params' => $params_array,
								'width' => $organizationInfo['width'],
								'height' => $organizationInfo['height'],
								'resource'=>$organizationInfo['resource'],
								'short_description'=>$organizationInfo['short_description']
							);
							//This not belong here according to MVC Paradigm, but for now will be here till launchapad is refactored
							if (PluginManager::isPluginActive('Share7020App')) {
								$chapterNode['headerWidget'] = Yii::app()->controller->widget('common.widgets.CourseLoHeaderApp7020', array('loID' => $idOrganization), true);
							}
							if ($userInfo) {
								//convert item track status in standard status
								switch ($chapter['status']) {
									case LearningScormItemsTrack::STATUS_PASSED:
									case LearningScormItemsTrack::STATUS_COMPLETED: { $chapterStatus = LearningCommontrack::STATUS_COMPLETED; } break;
									case LearningScormItemsTrack::STATUS_FAILED:
									case LearningScormItemsTrack::STATUS_INCOMPLETE: { $chapterStatus = LearningCommontrack::STATUS_ATTEMPTED; } break;
									case LearningScormItemsTrack::STATUS_NOT_ATTEMPTED:
									default: { $chapterStatus = LearningCommontrack::STATUS_AB_INITIO; } break;
								}
								//check SCO prerequisites
								$chapterLocked = false;
								if ($chapter['adlcp_prerequisites'] != '') {
									$prerequisites = explode(',', $chapter['adlcp_prerequisites']);
									foreach ($prerequisites as $prerequisite) {
										if (!empty($prerequisite)) {
											if (strpos($prerequisite, '=') !== false) {
												list($prerequisite, $requestedStatus) = explode('=', $prerequisite);
											} else {
												$requestedStatus = false;
											}
											if (isset($mappedItemIdentifiers[$prerequisite])) {
												$idScormItem = $mappedItemIdentifiers[$prerequisite];
												$prerequisiteStatus = (isset($scormChaptersList[$idResource][$idScormItem]) ? $scormChaptersList[$idResource][$idScormItem]['status'] : false);
												if ($prerequisiteStatus !== false) {
													if (empty($requestedStatus)) {
														//check for passed/completed status
														$chapterLocked = ($prerequisiteStatus != LearningScormItemsTrack::STATUS_COMPLETED && $prerequisiteStatus != LearningScormItemsTrack::STATUS_PASSED);
													} else {
														$chapterLocked = ($prerequisiteStatus != $requestedStatus);
													}
												}
												if ($chapterLocked) { break; } //prerequisites are unsatisfied and no more checks are needed so exit from foreach cycle
											}
										}
									}
								}
								if($parentNode['locked'])
									$chapterLocked = true;

								// If user is not granted access to this SCORM as w whole (organization), for any reason (usually prerequisites)
								// force chapter (SCO) locked
								if (!empty($userAccess) && is_array($userAccess) && isset($userAccess[$idOrganization]) && !$userAccess[$idOrganization]) {
									$chapterLocked = true;
								}

								$chapterNode['status'] = $chapterStatus;
								$chapterNode['locked'] = $chapterLocked;
							}
							//check if this scorm item has children
							$setScormItems($organizationInfo, $chapterNode);
							$parentNode['children'][] = $chapterNode;
						}
					}
				}
			};



			// AICC ITEMS
			$setAiccItems = function(&$organizationInfo, &$parentNode, $masterAiicItemSystemId) use (&$setAiccItems, &$aiccChaptersList, &$aiccMappedItemIdentifiers, &$sortAdjust, $userInfo, $userAccess) {
				$idOrganization = (int)$organizationInfo['idOrg'];
				$idResource = (int)$organizationInfo['idResource'];
				$iLeft = (int)$organizationInfo['iLeft'];
				$path = $organizationInfo['path'];

				if (isset($aiccChaptersList[$idResource]) && !empty($aiccChaptersList[$idResource])) {

					$parentSystemId = (isset($parentNode['aiccSystemId']) ? $parentNode['aiccSystemId'] : 'NOPARENTSYSTEMID');
					$hasChildren = false;

					// Count number of chapters in this AIIC LO
					$countChapters = count($aiccChaptersList[$idResource]);

					foreach ($aiccChaptersList[$idResource] as $chapter) {

						if (
								(($parentSystemId == 'NOPARENTSYSTEMID') && ($chapter['parent'] == $masterAiicItemSystemId))
							|| 	($chapter['parent'] === $parentSystemId)
							) {

							if (!$hasChildren) {
								$hasChildren = true;
								if (!$parentNode['hasLaunch']) {
									$parentNode['isFolder'] = true;
								}
							}

							if ($chapter['item_type'] == LearningAiccPackage::ITEM_TYPE_OBJECTIVE) {
								$parentNode['hasAiccObjectives'] = true;
							}


							if (!isset($parentNode['children'])) {
								$parentNode['children'] = array();
							}

							$sortAdjust++;

							// Fix LO parameters in JSON (quick fix)
							if (empty($organizationInfo['params_json'])) {
								$organizationInfo['params_json'] = "{}";
							}

							$params_array = CJSON::decode($organizationInfo['params_json']);
							if (!is_array($params_array) || empty($params_array)) {
								if (empty($organizationInfo['params_json_inherited'])) {
									$organizationInfo['params_json_inherited'] = "{}";
								}

								$params_json_inherited = CJSON::decode($organizationInfo['params_json_inherited']);
								if(is_array($params_json_inherited)){
									$params_array = $params_json_inherited;
								} else{
									$params_array = array();
								}
							}

							$shortenedTitle = mb_strlen($chapter['title'], 'UTF-8') > 70 ? mb_substr($chapter['title'], 0, 70 - 3, 'UTF-8').'...' : $chapter['title'];

							// THIS node is going to be sent to the JS Tree structure
							$chapterNode = array(
								'level' 			=> $parentNode['level'] + 1,
								'key' 				=> $idOrganization.':'.$chapter['id'],
								'idOrganization' 	=> $idOrganization,
								'idAiccItem' 		=> $chapter['id'],
								'aiccItemType'		=> $chapter['item_type'],
								'aiccSystemId'		=> $chapter['system_id'],
								'hasAiccObjectives'	=> false,
								'hasLaunch'			=> !empty($chapter['launch']),
								'title' 			=> $chapter['title'],
								'title_shortened' 	=> $shortenedTitle,
								'isFolder' 			=> empty($chapter['launch']),
								'type' 				=> 'aicc',
								'sort' 				=> $iLeft + $sortAdjust,
								'path' 				=> $path.':'.$chapter['id'],
								'prerequisites' 	=> $chapter['prerequisite'],
								'maxTimeAllowed' 	=> $chapter['max_time_allowed'],
								'timeLimitAction' 	=> $chapter['time_limit_action'],
								'masteryScore' 		=> $chapter['mastery_score'],
								'identifier' 		=> $chapter['system_id'],
								'is_visible' 		=> (int) $organizationInfo['visible'],
								'params' 			=> $params_array,
								'width' 			=> $organizationInfo['width'],
								'height' 			=> $organizationInfo['height'],
								'resource'			=> $organizationInfo['resource'],
								'short_description'	=> $organizationInfo['short_description'],
								'versionName' 		=> $chapter['version_name'],
								'id_object'			=> $chapter['id_object'],
								'version_create_date' => Yii::app()->localtime->toLocalDatetime($chapter['create_date']),
								'keep_updated'		=> $chapter['keep_updated']
							);
							//This not belong here according to MVC Paradigm, but for now will be here till launchapad is refactored
							if (PluginManager::isPluginActive('Share7020App')) {
								$chapterNode['headerWidget'] = Yii::app()->controller->widget('common.widgets.CourseLoHeaderApp7020', array('loID' => $idOrganization), true);
							}
							//
							if ($userInfo) {

								switch ($chapter['status']) {
									case LearningAiccItemTrack::LESSON_STATUS_PASSED:
									case LearningAiccItemTrack::LESSON_STATUS_COMPLETED: { $chapterStatus = LearningCommontrack::STATUS_COMPLETED; } break;
									case LearningAiccItemTrack::LESSON_STATUS_FAILED:
									case LearningAiccItemTrack::LESSON_STATUS_INCOMPLETE: { $chapterStatus = LearningCommontrack::STATUS_ATTEMPTED; } break;
									case LearningAiccItemTrack::LESSON_STATUS_NOT_ATTEMPTED:
									default: { $chapterStatus = LearningCommontrack::STATUS_AB_INITIO; } break;
								}


								$chapterLocked = false;
								/*
								 *
								 * @TODO   AICC:  Resolve internal prerequisites
								 *
								 *
								if ($chapter['prerequisite'] != '') {
									$prerequisites = explode(',', $chapter['prerequisite']);
									foreach ($prerequisites as $prerequisite) {
										if (!empty($prerequisite)) {
											if (strpos($prerequisite, '=') !== false) {
												list($prerequisite, $requestedStatus) = explode('=', $prerequisite);
											} else {
												$requestedStatus = false;
											}
											if (isset($aiccMappedItemIdentifiers[$prerequisite])) {
												$idAiccItem = $aiccMappedItemIdentifiers[$prerequisite];
												$prerequisiteStatus = (isset($aiccChaptersList[$idResource][$idAiccItem]) ? $aiccChaptersList[$idResource][$idAiccItem]['status'] : false);
												if ($prerequisiteStatus !== false) {
													if (empty($requestedStatus)) {
														$chapterLocked = ($prerequisiteStatus != LearningAiccItemTrack::LESSON_STATUS_COMPLETED && $prerequisiteStatus != LearningAiccItemTrack::LESSON_STATUS_PASSED);
													} else {
														$chapterLocked = ($prerequisiteStatus != $requestedStatus);
													}
												}
												if ($chapterLocked) { break; } //prerequisites are unsatisfied and no more checks are needed so exit from foreach cycle
											}
										}
									}
								}
								*/
								if($parentNode['locked'])
									$chapterLocked = true;

								// If user is not granted access to this AICC as whole (organization), for any reason (usually prerequisites)
								// force chapter locked
								if (!empty($userAccess) && is_array($userAccess) && isset($userAccess[$idOrganization]) && !$userAccess[$idOrganization]) {
									$chapterLocked = true;
								}

								$chapterNode['status'] = $chapterStatus;
								$chapterNode['locked'] = $chapterLocked;


							}

							// Check and use if this item has children
							$setAiccItems($organizationInfo, $chapterNode, $masterAiicItemSystemId);

							// Skip this chapter/item/node if it is an OBJECTIVES having NO children
							if ( ($chapterNode['aiccItemType'] !== LearningAiccPackage::ITEM_TYPE_OBJECTIVE) || (count($chapterNode['children']) > 0)) {
								$parentNode['children'][] = $chapterNode;
							}

						}
						else {
							// Skipped chapter
						}
					}
				}
			};


			/*
			 * this function will navigate through LOs list and create a tree structured array of nodes
			 */
			$makeTree = function($level, $parentPath, &$parent) use (&$makeTree, &$checkFolderStatus, &$setScormItems, &$setAiccItems, &$checkFolderEmptyness, &$levels, &$userStatus, &$userAccess, $onlyFolders, $userInfo, $scormChapters, $scormChaptersList, $aiccChapters, $aiccChaptersList, &$sortAdjust, &$excludeObjects, $excludeEmptyFolders, $showHiddenObjects) {
				if (!is_array($levels) || !isset($levels[$level])) { return; } //security check
				//search in flat data for all $parent children, use paths for recognition
				foreach ($levels[$level] as $index => $row) {
					list($path, $idOrg) = explode('_', $index);

					//if (($parentPath == '' && $level ==1) || (strpos($path, $parentPath) === 0)) { //if true, then the node $row is a child of $parent
					if (($parentPath == '' && $level ==1) || (LearningObjectsManager::isParentPath($path, $parentPath))) { //if true, then the node $row is a child of $parent
						if (($onlyFolders && $row['objectType'] == '') || !$onlyFolders) { //folder parameter check
							$isFolder = ($row['objectType'] == '');
							$isScorm = ($row['objectType'] == LearningOrganization::OBJECT_TYPE_SCORMORG);
							$isAicc = ($row['objectType'] == LearningOrganization::OBJECT_TYPE_AICC);
							if ($isFolder || empty($excludeObjects) || (!$isFolder && !in_array($row['idOrg'], $excludeObjects))) { //check for direct exclusion of objects
								if (!$excludeEmptyFolders || !$isFolder || ($excludeEmptyFolders && $isFolder && !$checkFolderEmptyness($row['path'], $level+1))) { //check for folder emptyness
									if ($showHiddenObjects || (!$showHiddenObjects && $row['visible']>0)) {
										//create physical node instance

										// Fix LO parameters in JSON (quick fix)
										if (empty($row['params_json'])) {
											$row['params_json'] = "{}";
										}

										$params_array = CJSON::decode($row['params_json']);
										if (!is_array($params_array) || empty($params_array)) {
											if (empty($row['params_json_inherited'])) {
												$row['params_json_inherited'] = "{}";
											}

											$params_json_inherited = CJSON::decode($row['params_json_inherited']);
											if(is_array($params_json_inherited)){
												$params_array = $params_json_inherited;
											} else{
												$params_array = array();
											}
										}


										$shortenedTitle = mb_strlen($row['title'], 'UTF-8') > 70 ? mb_substr($row['title'], 0, 70 - 3, 'UTF-8').'...' : $row['title'];

										// In case of Authoring LO, we need to check if this LO is a COPY OF another LO
										// and pass that information to training area. If it is a copy, EDIT functionality is disabled (see training.js)
										if ($row['objectType'] == LearningOrganization::OBJECT_TYPE_AUTHORING) {
											$authoringModel = LearningAuthoring::model()->findByPk((int)$row['idResource']);
											if ($authoringModel) {
												$authoringCopyOf = (int) $authoringModel->copy_of;
											}
										}


										//retrieve CSP info for the current LO
										$cspInfo = false; //initialize node property value
										if (!$isFolder) {
											$cspList = self::getCSPList();
											if (is_array(($cspList)) && !empty($row['from_csp'])) {
												if (isset($cspList[$row['from_csp']])) {
													$cspInfo = array(
														'id' => (int)$row['from_csp'],
														'name' => $cspList[$row['from_csp']]['name'],
														'type' => $cspList[$row['from_csp']]['type'],
														'is_active' => ($cspList[$row['from_csp']]['is_active'] > 0)
													);
												}
											}
										}


										// IF: This is a SCORMORG, and if there is only ONE scorm chapter (i.e. SCO)....
										// Convert this node to directly playable SCO/LO object, NO SCORM folder!!
										$singleScoScorm = false;

										if ($isScorm && count($scormChaptersList[(int)$row['idResource']]) == 1) {

											$singleScoScorm = true;

											reset($scormChaptersList[$row['idResource']]);
											$idScormItem = key($scormChaptersList[$row['idResource']]);
											$chapter =  $scormChaptersList[$row['idResource']][$idScormItem];
											$organizationModel = LearningOrganization::model()->findByPk($idOrg);

											$shortenedTitle = mb_strlen($row['title'], 'UTF-8') > 70 ? mb_substr($row['title'], 0, 70 - 3, 'UTF-8').'...' : $row['title'];

											//build node structire
											$node = array(
												'level' => $chapter['lev']+1,
												'key' => $row['idOrg'].':'.$chapter['idscorm_item'],
												'idOrganization' => (int)$row['idOrg'],
												'idResource' => (int)$row['idResource'],
												'idScormItem' => $chapter['idscorm_item'],
												'title' => $row['title'],
												'scormTitle' => $organizationModel->title,
												'title_shortened' => $shortenedTitle,
												'short_description' => $row['short_description'],
												'resource' => $row['resource'],
												'isFolder' => false,
												'type' => 'sco', //change it ?
												'sort' => $row['iLeft'] + $sortAdjust,
												'path' => $row['path'].':'.$chapter['idscorm_item'],
												'prerequisites' => $chapter['adlcp_prerequisites'],
												'maxTimeAllowed' => $chapter['adlcp_maxtimeallowed'],
												'timeLimitAction' => $chapter['adlcp_timelimitaction'],
												'dataFromLms' => $chapter['adlcp_datafromlms'],
												'masteryScore' => $chapter['adlcp_masteryscore'],
												'identifier' => $chapter['item_identifier'],
												'idResource' => $chapter['idscorm_resource'],
												'identifierRef' => $chapter['identifierref'],
												'self_prerequisite' => $row['self_prerequisite'],
												'is_visible' => (int)$row['visible'],
												'params' => $params_array,
												'width' => $row['width'],
												'height' => $row['height'],
												'versionName' => $row['version_name'],
												'id_object'	=> $row['id_object'],
												'version_create_date' => Yii::app()->localtime->toLocalDatetime($row['create_date']),
												'keep_updated'		=> $row['keep_updated'],
												'csp_info' => $cspInfo
											);
											if (PluginManager::isPluginActive('Share7020App')) {
												$node['headerWidget'] = Yii::app()->controller->widget('common.widgets.CourseLoHeaderApp7020', array('loID' => $row['idOrg']), true);
											}
										}
										else {

											/*
											 * If user is PU show him Evaluation option if he's an Instructor,
											 * even he has not this permission
											 * */

											$isEvaluationAllowed = true;

											if(Yii::app()->user->isPu) {
												if(!Yii::app()->user->checkAccess("/lms/admin/course/evaluation")) {

													$courseId = LearningOrganization::model()->findByPk((int)$row['idOrg'])->course->idCourse;
													$pUserRights = Yii::app()->user->checkPURights($courseId, Yii::app()->user->id);

													if(!$pUserRights->isInstructor) {
														$isEvaluationAllowed = false;
													}
												}
											}

											//build node structure
											$node = array(
												'level' => $row['lev'],
												'key' => "".$row['idOrg'],
												'idOrganization' => (int)$row['idOrg'],
												'idResource' => (int)$row['idResource'],
												'title' => $row['title'],
												'description' => $row['description'],
												'short_description' => $row['short_description'],
												'resource' => $row['resource'],
												//'title' => $shortenedTitle,
												'title_shortened' => $shortenedTitle,
												'isFolder' => $isFolder || ($isScorm && ($scormChapters)) || ($isAicc && $aiccChapters),
												'type' => $row['objectType'],
												'isEvaluationAllowed' => $isEvaluationAllowed,
												'sort' => $row['iLeft'] + $sortAdjust,
												'path' => $row['path'],
												'prerequisites' => trim($row['prerequisites']),
												'self_prerequisite' => $row['self_prerequisite'],
												'is_visible' => (int)$row['visible'],
												'params' => $params_array,
												'authoring_copy_of' => $authoringCopyOf,
												'height' => $row['height'],
												'versionName' => $row['version_name'],
												'showVersioning' => ( in_array($row['objectType'], LearningRepositoryObject::$objects_with_versioning) && $row['versions_count'] > 1),
												'id_object'	=> $row['id_object'],
												'version_create_date' => Yii::app()->localtime->toLocalDatetime($row['create_date']),
												'keep_updated'		=> $row['keep_updated'],
												'csp_info' => $cspInfo
											);

											//This not belong here according to MVC Paradigm, but for now will be here till launchapad is refactored
											if (PluginManager::isPluginActive('Share7020App')) {
												$node['headerWidget'] = Yii::app()->controller->widget('common.widgets.CourseLoHeaderApp7020', array('loID' => $row['idOrg']), true);
											}

											// Add target location for LTI objects
											if($row['objectType'] == LearningOrganization::OBJECT_TYPE_LTI){
												$node['lti_target'] = Yii::app()->db->createCommand()
													->select('target')
													->from(LearningLti::model()->tableName())
													->where('id = :id', array(':id' => $row['idResource']))
													->queryScalar();
											}
										}
										// We check for special 'visible' status == -1 for VIDEO LO
										$node['is_video_transcoding'] = 0;
										$node['is_video_transcoding_error'] = 0;
										if ( ($row['objectType'] == LearningOrganization::OBJECT_TYPE_VIDEO) && ($row['visible'] == -1)) {
											$node['is_video_transcoding'] = 1;
										}
										if ( ($row['objectType'] == LearningOrganization::OBJECT_TYPE_VIDEO) && ($row['visible'] == -2)) {
											$node['is_video_transcoding_error'] = 1;
										}


										if ($userInfo) { //check if user info has been requested
											if ($isFolder) {
												$_status = $checkFolderStatus($row['path'], $level+1);
												$_access = true;
											} else {
												$_status = (isset($userStatus[(int)$row['idOrg']]) ? $userStatus[(int)$row['idOrg']] : false);
												$_access = (isset($userAccess[(int)$row['idOrg']]) ? $userAccess[(int)$row['idOrg']] : false);

												if($row['self_prerequisite']){
													// Check if the LO is set to "play only once" or "play until passed/completed"

													switch ($row['self_prerequisite']){
														case LearningOrganization::SELF_PREREQ_ONCE:
															// If LO is only allowed to be played once and is already played

															$criteria = new CDbCriteria();
															$criteria->addCondition('idReference=:idReference');
															$criteria->params[':idReference'] = $row['idOrg'];
															$criteria->addCondition('idUser=:idUser');
															$criteria->params[':idUser'] = Yii::app()->user->id;

															// Make sure the status is different from "not started" so we can assume
															// that the user actually started the LO
															$criteria->addNotInCondition('status', array(LearningCommontrack::STATUS_AB_INITIO));
															if(LearningCommontrack::model()->count($criteria)){
																// The user has played the LO already, so lock it now
																$_access = FALSE;
															}
															break;
														case LearningOrganization::SELF_PREREQ_INCOMPLETE:
															// If the LO can be played infinitely until completed

															$_condition = array(
																'idReference' => $row['idOrg'],
																'idUser' => Yii::app()->user->id,
																'status' => LearningCommontrack::STATUS_COMPLETED
															);
															if(LearningCommontrack::model()->countByAttributes($_condition)){
																$_access = FALSE;
															}
															break;
														default: // Don't lock the LO. It can be played infinitely
													}
												}

											}
											$_locked = !$_access;
											$node['status'] = $_status;
											$node['locked'] = $_locked;
										}
										if ($isFolder) {
											$node['children'] = array();
											//recursively search for children
											$makeTree($level+1, $row['path'], $node);
										} elseif ($scormChapters && $isScorm && !$singleScoScorm) {
											$setScormItems($row, $node);
										}
										elseif ($aiccChapters && $isAicc) {
											// Find master AICC Item system id
											$masterAiicItemSystemId = 'NOPARENTSYSTEMID';
											foreach ($aiccChaptersList[$row['idResource']] as $tmpChapter) {
												if ($tmpChapter['parent'] == '/') {
													$masterAiicItemSystemId = $tmpChapter['system_id'];
												}
											}
											$setAiccItems($row, $node, $masterAiicItemSystemId);
										}
										if ($level == 1) { //we are at first level, no nodes inserted yet
											$parent[] = $node;
										} else { //sublevels, we already have nodes instances and 'children' arrays
											$parent['children'][] = $node;
										}

									} //end "showHiddenNodes" check
								} //end "folder emptyness" check
							} //end "direct objects exclusion" check
						} //end "only folders" check
					}
				}
			};

			//create tree into $output variable
			$makeTree(1, '', $output);
		}

		//return the tree array
		return $output;
	}



	/**
	 * For a given course/user, retrieve the first oject to be played in the player (or some LO with arbitrary ID, if requested)
	 *
	 * Note about "arbitrary key" below: usualy KEY is equal to LO idOrg. But for SCO chapters (children of the LO), key is  123:456.
	 * That's why we must use "key" and Learning Object ID here
	 *
	 * @param int $idCourse
	 * @param int $idUser
	 * @param string $arbitraryKey If set, Return LO with this "key" (but only if it is NOT locked!). If not set, return the FIRST LO available for play
	 */
	public static function getFirstObjectToPlay($idCourse, $idUser = false, $arbitraryKey=false) {
		$treeParams = array('userInfo' => true, 'scormChapters' => true, 'showHiddenObjects' => false, 'aiccChapters' => true);
		if ((int)$idUser > 0) { $treeParams['targetUser'] = (int)$idUser; }
		$tree = self::getCourseLearningObjectsTree($idCourse, $treeParams);
		if (empty($tree)) { return false; }

		$firstNode = null;

		$searcher = function(&$nodes) use (&$searcher, &$firstNode, $arbitraryKey) {
			if (is_array($nodes) && !empty($nodes)) {
				foreach ($nodes as $node) {

				    if (!$node['isFolder'] && !$firstNode) {
				        $firstNode = $node;
				    }

					if ($node['isFolder']) {
						//check folder children
						if (isset($node['children']) && !empty($node['children'])) {
							$check = $searcher($node['children']);
							if (!empty($check)) { return $check; }
						}

					}
					else if ($node['type'] == 'sco') {
						//scorm chapters have to be treated separately
						if (!empty($node['idResource']) && !$node['locked']) {
						    // If we are looking for a particular LO ID
						    if ($arbitraryKey == $node['key']) {
						        return $node;
						    }
						    // Or NOT..? and.. also..
							else if (!$arbitraryKey && $node['status'] != LearningCommontrack::STATUS_COMPLETED) {
								return $node;
							}
						}
						if (isset($node['children']) && !empty($node['children'])) {
							$check = $searcher($node['children']);
							if (!empty($check)) { return $check; }
						}
					}
					else if ($node['type'] == LearningOrganization::OBJECT_TYPE_AICC) {
						if ( ($node['aiccItemType'] == LearningAiccPackage::ITEM_TYPE_AU) && !$node['locked']) {
						    // If we are looking for a particular LO ID
						    if ($arbitraryKey == $node['key']) {
						        return $node;
						    }
						    else if (!$arbitraryKey && $node['status'] != LearningCommontrack::STATUS_COMPLETED) {
								return $node;
							}
						}
						if (isset($node['children']) && !empty($node['children'])) {
							$check = $searcher($node['children']);
							if (!empty($check)) { return $check; }
						}
					}

					// Normal object node, check if it is playable
					else if (!$node['locked'] && ($node['type'] != '') && ($node['type'] != LearningOrganization::OBJECT_TYPE_SCORMORG)) {

					    // If we are looking for a particular LO ID
					    if ($arbitraryKey == $node['key']) {
					        return $node;
					    }
					    else if (!$arbitraryKey && $node['status'] != LearningCommontrack::STATUS_COMPLETED) {

    						// In some occasions the player is reset to the "first LO available to play"
							// (e.g. every time the SCORM player lightbox is closed) and we don't want
							// to return the user to the very first available item in this case, because
							// it may lead to an endless cycle where you can not jump over the completed
							// SCORM because every time you close it you would be taken to the "first playable"
							// item before the SCORM
							$skipThisLoAsFirstToPlay = false;
							switch($node['type']){
								case LearningOrganization::OBJECT_TYPE_DELIVERABLE:
									if ($node['status']==LearningCommontrack::STATUS_ATTEMPTED){
										$skipThisLoAsFirstToPlay = true;
									}
									break;
							}

							if($skipThisLoAsFirstToPlay) continue;

							return $node;
						}
					}
				}
			}
			return false;
		};

		$object = $searcher($tree);

		// If Object wasn't found and it was a search for Arbitrary LO, try again, this time with NO arbitrary LO
		// Because we have to return some object!!
		if (!$object && $arbitraryKey) {
		    $object = self::getFirstObjectToPlay($idCourse, $idUser);
		}

		// If still there is no object, get the first ever available
		if (!$object) {
		    $object = $firstNode;
		}


		return (!$object ? false : $object);
	}



	/**
	 * Count course objects by status for a specified user
	 * @param int $idCourse the ID of the course
	 * @param int $idUser the ID of the user
	 * @param boolean $checkVisibility if set to true, LOs with "visible" field set to 0 will be excluded from counting
	 * @return array statistic by status (statuses as key (plus total), counts as values
	 */
	public static function getCourseLearningObjectsStats($idCourse, $idUser = false, $checkVisibility = false) {
		//collect objects IDs (excluding folders)
		$objects = self::getCourseLearningObjects($idCourse);
		$idList = array();
		if (is_array($objects))
		{
			foreach ($objects as $object)
			{
				$publish_from = true;
				$publish_to = true;

				if($object['publish_from'] !== '' && !is_null($object['publish_from']) && $object['publish_from'] !== '0000-00-00 00:00:00')
					if(strtotime($object['publish_from']) > strtotime(Yii::app()->localtime->getLocalNow('Y-m-d H:i:s')))
						$publish_from = false;

				if($object['publish_to'] !== '' && !is_null($object['publish_to']) && $object['publish_to'] !== '0000-00-00 00:00:00')
					if(strtotime($object['publish_to']) < strtotime(Yii::app()->localtime->getLocalNow('Y-m-d H:i:s')))
						$publish_to = false;

				if (!$checkVisibility || ($checkVisibility && $object['visible'] > 0 && $publish_from && $publish_to))
				{
					if ($object['objectType'] != '')
					{
						$idList[] = (int)$object['idOrg'];
					}
				}
			}
		}

		//calculate status for each collected object and specified user
		if (empty($idUser) || (int)$idUser <= 0) { $idUser = Yii::app()->user->id; }
		$statuses = self::getUserLOsStatus((int)$idUser, $idList);

		//count objects by status
		$output = array(
			LearningCommontrack::STATUS_AB_INITIO => 0,
			LearningCommontrack::STATUS_ATTEMPTED => 0,
			LearningCommontrack::STATUS_COMPLETED => 0,
			'total' => 0
		);
		foreach ($idList as $idObject) {
			if($statuses[$idObject] == LearningCommontrack::STATUS_PASSED)
				$statuses[$idObject] = LearningCommontrack::STATUS_COMPLETED;
			$status = (isset($statuses[$idObject]) ? $statuses[$idObject] : LearningCommontrack::STATUS_AB_INITIO);
			$output[$status]++;
			$output['total']++;
		}

		return $output;
	}



	/**
	 * For every passed learning object (in $list parameter) retrieve info on user tracking
	 * @param integer $idUser the ID of the user
	 * @param array $objects a list of IDs of learning objects (ID from learning_organization)
	 * @return array an array 'idReference' => {commontrack record}
	 */
	public static function getUserLOsInfo($idUser, $objects = array()) {
		$output = array();
		if (!empty($objects) && (int)$idUser > 0) {
			//check cache for every requested object
			$toSearch = array();
			$cachePrefix = 'userLOsInfo.'.(int)$idUser.'.';
			foreach ($objects as $idObject) {
				if ((int)$idObject > 0) {
					$userInfo = self::getCache($cachePrefix.(int)$idObject);
					if ($userInfo === NULL) {
						$toSearch[] = (int)$idObject;
					} else {
						$output[(int)$idObject] = $userInfo;
					}
				}
			}
			//for every non-cached object, search in the DB for it and cache it
            if (!empty($toSearch)) {
				$criteria = new CDbCriteria();
				$criteria->addCondition('idUser = :id_user');
				$criteria->addInCondition('idReference', $toSearch);
				$criteria->params[':id_user'] = (int)$idUser;
				$allValues = LearningCommontrack::model()->findAll($criteria);
				foreach ($allValues as $value) {
					if(!isset($output[(int)$value['idReference']])){
					   $output[(int)$value['idReference']] = $value;
					   self::setCache($cachePrefix.(int)$value['idReference'], $value);
				    }
                }
            }
		}
		return $output;
	}



	/**
	 * For every passed learning object (in $list parameter) retrieve the status of the user
	 * @param integer $idUser the ID of the user
	 * @param array $objects a list of IDs of learning objects (ID from learning_organization)
	 * @return array an array 'idReference' => 'status'
	 */
	public static function getUserLOsStatus($idUser, $objects = array()) {
		$output = array();
		if (!empty($objects) && (int)$idUser > 0) {
			$allValues = self::getUserLOsInfo($idUser, $objects);
			foreach ($allValues as $idReference => $value) {
				$output[(int)$idReference] = $value['status'];
			}
		}
		return $output;
	}



	/**
	 * Navigation policy: PRE-TEST: No LO can be run if FIRST one is not yet completed
	 *
	 * @param array $objectsList  Array of LO Ids of the course, in order of their appearance in the training materials tree
	 * @param array $objectsStatus Array of "<idOrg>" => <LO commontrack status>; i.e. information about LO completion status
	 * @return array Array of  "<idOrg>" => 0   (always zero!!); basically a list of objects TO LOCK
	 */
	protected static function applyPretestNavigationPolicy($objectsList, $objectsStatus) {
		$lockedObjects = array();
		foreach ($objectsList as $index => $idOrg) {
			// While we are enumerating, get the FIRST object status
			if ($index == 0) {
				$firstObjectStatus = isset($objectsStatus[$idOrg]) ? $objectsStatus[$idOrg] : false;
			}
			// If that first object is not completed yet, LOCK ALL THE REST after it
			// Do not lock the first one, of course :-)
			if ($firstObjectStatus != LearningCommontrack::STATUS_COMPLETED && $index != 0) {
				$lockedObjects[$idOrg] = 0;
			}
		}

		return $lockedObjects;
	}

	/**
	 * If Course-level lo_max_attempts is set (max failed), check if an object has reached that value and lock it.
	 * 
	 * If the object is a TEST, check if it has its own max-attempts set and skip the LO at all from this check.
	 *  
	 * @param array $objectsList
	 * @param unknown $objectsStatus
	 * @param unknown $userAccess
	 * @param LearningCourse $courseModel
	 * @return unknown|number
	 */
	
	/**
	 * If Course-level lo_max_attempts is set (max failed), check if an object has reached that value and lock it.
	 * 
	 * @param integer $idUser
	 * @param array $objectsList Array of idOrgs
	 * @param array $objectsTypes Array of idOrg => LO Type 
	 * @param array $objectsStatus Array of idOrg => LO Status (for the user)
	 * @param array $userAccess INPUT array of user access (0|1). We modify and return at the end of this method.
	 * @param LearningCourse $courseModel 
	 * 
	 * @return array Array of access flags (0|1) for the user, 0: locked, 1: not locked
	 */
	protected static function applyMaxAttemptsLocking($idUser, $objectsList, $objectsTypes, $objectsStatus, $userAccess, LearningCourse $courseModel) {


	    // No COURSE LEVEL max-attempts set? Get out!
	    if ((int) $courseModel->lo_max_attempts <= 0) {
	        return $userAccess;
	    }
	    
	    // Get array of LearningCommontrack mpodels for all LOs for this user. Don't worry, it is cached
	    $userLOsInfo = self::getUserLOsInfo($idUser, $objectsList);

	    // Enumerate LOs
	    foreach ($objectsList as $index => $idOrg) {
	        
	        // Skip LO type if it is out of max-attempts-check scope
	        if (!in_array($objectsTypes[$idOrg], LearningOrganization::$OBJECTS_TO_CHECK_MAX_ATTEMPTS)) {
	            continue;
	        }

	        // By default, check
	        $check = true;
	        
	        // Skip checking TEST LOs having their own max-attempts set
	        if ($objectsTypes[$idOrg] === LearningOrganization::OBJECT_TYPE_TEST && ($loModel = LearningOrganization::model()->findByPk($idOrg))) {
                $testModel = LearningTest::model()->findByPk($loModel->idResource);
                if ($testModel && (int) $testModel->max_attempt > 0) {
                    $check = false; 
                }
	        }

	        // If we have to check the max-attempt, check it
	        if ($check) {
	            // Use the previously loaded (and cached) user-per-lo-status array of LearningCommontrack models
	            $trackModel = isset($userLOsInfo[$idOrg]) ? $userLOsInfo[$idOrg] : false;
	            if ($trackModel instanceof LearningCommontrack) {
	                if ($trackModel->allowedFailedAttemptsReached) {
	                    $userAccess[$idOrg] = 0;
	                }
	            }
	        }
	        
	    }
	    
	    return $userAccess;
    }
	
	

	/**
	 * Navigation policy: FINAL TEST:  Last object can NOT be run until ALL previous objects are completed.
	 *
	 * @param array $objectsList  Array of LO Ids of the course, in order of their appearance in the training materials tree
	 * @param array $objectsStatus Array of "<idOrg>" => <LO commontrack status>; i.e. information about LO completion status
	 * @return array Array of  "<idOrg>" => 0   (always zero!!); basically a list of objects TO LOCK
	 */
	protected static function applyFinaltestNavigationPolicy($objectsList, $objectsStatus) {
		for ($i=0; $i < count($objectsList)-1; $i++) {
			$objectStatus = isset($objectsStatus[$objectsList[$i]]) ? $objectsStatus[$objectsList[$i]] : false;

			// We found a not-completed object! Return the idOrg of the LAST object in the list: it must be locked!
			if ($objectStatus != LearningCommontrack::STATUS_COMPLETED) {
				return array(end($objectsList) => 0);
			}
		}
		return array();
	}



	/**
	 * Check PREVIOUS objects completion status, as a group
	 *
	 * @param array $objectsList  Array of LO Ids of the course, in order of their appearance in the training materials tree
	 * @param number Check Objects indexed from 0 (zero) to this number
	 * @param array $objectsStatus Array of "<idOrg>" => <LO commontrack status>; i.e. information about LO completion status
	 * @return boolean
	 */
	protected static function previousObjectsCheck($objectsList, $lastPreviousIndex, $objectsStatus) {
		for ($i=0; $i <= $lastPreviousIndex; $i++) {
			$objectStatus = isset($objectsStatus[$objectsList[$i]]) ? $objectsStatus[$objectsList[$i]] : false;
			if ($objectStatus != LearningCommontrack::STATUS_COMPLETED) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Navigation policy: SEQUENTAL: LO is locked if ANY of the previous objects is not yet completed
	 *
	 * @param array $objectsList  Array of LO Ids of the course, in order of their appearance in the training materials tree
	 * @param array $objectsStatus Array of "<idOrg>" => <LO commontrack status>; i.e. information about LO completion status
	 * @return array Array of  "<idOrg>" => 0   (always zero!!); basically a list of objects TO LOCK
	 */
	protected static function applySequentalNavigationPolicy($objectsList, $objectsStatus) {
		$lockedObjects = array();
		for ($i=0; $i < count($objectsList); $i++) {
			// Start checking from SECOND object; in this policy, the first objects can not be locked
			if ( ($i > 0) ) {
				// Check all previous objects for completion status
				if (!self::previousObjectsCheck($objectsList, $i-1, $objectsStatus)) {
					$lockedObjects[$objectsList[$i]] = 0;
				}
			}
		}
		return $lockedObjects;
	}



	/**
	 * Returns an array of objects that should be LOCKED due to the Navigation policy applied to course objects.
	 * Note please, this is a LOCK-list only! We do NOT return if objects must be unlocked... ONLY if it must be locked!!!
	 *
	 * @param array $objectsList  Array of LO Ids of the course, in order of their appearance in the training materials tree
	 * @param array $objectsStatus Array of "<idOrg>" => <LO commontrack status>; i.e. information about LO completion status
	 * @param number $prerequisites_policy  Type of navigation policy (course table -> prerequisites_policy field)
	 * @return array Array of  "<idOrg>" => 0   (always zero!!); basically a list of objects TO LOCK
	 */
	public static function applyNavigationPolicy($objectsList, $objectsStatus, $prerequisites_policy) {

		// Empty by default, i.e. we do not put lock on anything yet
		$lockedObjects = array();

		// Switch based on learning_course->prerequisites_policy field
		switch ($prerequisites_policy) {
			case LearningCourse::NAVRULE_PRETEST:
				$lockedObjects = self::applyPretestNavigationPolicy($objectsList, $objectsStatus);
				break;
			case LearningCourse::NAVRULE_FINALTEST:
				$lockedObjects = self::applyFinaltestNavigationPolicy($objectsList, $objectsStatus);
				break;
			case LearningCourse::NAVRULE_SEQUENTAL:
				$lockedObjects = self::applySequentalNavigationPolicy($objectsList, $objectsStatus);
				break;

			// Return empty by default
			default:
				return array();
				break;

		}

		// Result is an array of <idOrg> => 0,   if any
		return $lockedObjects;
	}



	/**
	 * For every passed learning object (in $list parameter) check user prerequisites
	 * @param integer $idUser the ID of the user
	 * @param array $objects a list of learning objects IDs (ID from learning_organization table)
	 * @return array an array 'idReference' => 'status'
	 */
	public static function getUserLOsAccessibility($idUser, $objects = array()) {


		$output = array();

		if (!empty($objects) && (int)$idUser > 0) {

			//extract info on learning objects prerequisites
			$prerequisites = array();
			//first chek cached objects, to avoid costly duplicated database extractions
			$toSearch = array();
			$cachePrefix = 'learningObjects.';
			foreach ($objects as $idObject) {
				if ((int)$idObject > 0) {
					$objectInfo = self::getCache($cachePrefix.(int)$idObject);
					if ($objectInfo === NULL) {
						$toSearch[] = (int)$idObject;
					} else {
						$prerequisites[(int)$idObject] = explode(",", $objectInfo['prerequisites']);
					}
				}
			}
			//search not-cached learning objects in DB adn cache them for future use
			if (!empty($toSearch)) {
				$criteria = new CDbCriteria();
				$criteria->addInCondition('idOrg', $toSearch);
				//$criteria->addCondition('idOrg IN ('.implode(',', $toSearch).')');
				$allValues = LearningOrganization::model()->findAll($criteria);
				foreach ($allValues as $value) {
					$prerequisites[(int)$value['idOrg']] = explode(",", $value['prerequisites']);
					self::setCache($cachePrefix.(int)$value['idOrg'], $value);
				}
			}

			//get user status for all prerequisites objects
			$superList = array(); //list of uniques idOrg of all prerequisites objects
			foreach ($prerequisites as $list) {
				//$superList = array_merge($superList, $list);
				foreach ($list as $item) {
					if ($item > 0 && !in_array((int)$item, $superList)) { $superList[] = (int)$item; }
				}
			}
			if (!empty($superList)) {
				$statuses = self::getUserLOsStatus($idUser, $superList);
			}

			//check prerequisites for every object and fill output variable
			foreach ($prerequisites as $idOrg => $list) {
				if (empty($list)) { //no prerequisites, accessibility is granted
					$output[(int)$idOrg] = true;
				} else { //there are some prerequisites to check for this object
					$finish = false;
					$passed = true;
					for ($i=0; $i<count($list) && !$finish; $i++) {
						$idPre = (int)$list[$i];
						if (!empty($idPre)) {
							if (!isset($statuses[$idPre])) {
								$passed = false;
								$finish = true;
							} else {
								if ($statuses[$idPre] != LearningCommontrack::STATUS_COMPLETED && $statuses[$idPre] != LearningCommontrack::STATUS_PASSED) {
									$passed = false;
									$finish = true;
								}
							}
						}
					}
					$output[(int)$idOrg] = $passed;
				}
			}
		}
		return $output;
	}



	/**
	 * Ensure that the folder title does not contain invalid characters or tags
	 * @param string $title the original title
	 * @return string the formatted title
	 */
	public static function formatFolderTitle($title) {
		if (!is_string($title)) { return ""; }
		$title = strip_tags("".$title);
		//TO DO: use a preg_replace ?!?
		return $title;
	}



	/**
	 * Insert a new folder in the learning object tree for a given course
	 * @param string $title the name of the folder
	 * @param integer $idParent the idOrg of the parent folder (if 0 or false, then consider it as root)
	 * @return boolean|LearningOrganization the new inserted AR record
	 */
	public function addFolder($title, $idParent) {

		$idCourse = $this->getIdCourse();

		// Create an entry in the learning_organization

		$parent = null;
		if ($idParent) {
			$parent = LearningOrganization::model()->findByPk($idParent);
		} else {
			$parent = LearningObjectsManager::getRootNode($idCourse);
		}

		if (!$parent) {
			return false;
		}

		$lmodel = new LearningOrganization();

		$lmodel->title = LearningObjectsManager::formatFolderTitle($title);
		$lmodel->objectType = "";
		$lmodel->idResource = 0;
		$lmodel->idAuthor = Yii::app()->user->id;
		$lmodel->dateInsert = Yii::app()->localtime->toLocalDateTime();
		$lmodel->idCourse = $idCourse;

		if ($lmodel->appendTo($parent)) {
			return $lmodel;
		}

		return false;
	}



	/**
	 * Delete a folder object from learning_organization table
	 * @param integer $idFolder
	 * @return boolean
	 * @throws CException
	 */
	public function deleteFolder($idFolder) {

		if (!is_numeric($idFolder) || $idFolder <= 0) { return false; }

		if (Yii::app()->db->getCurrentTransaction() === NULL) {
			$trans = Yii::app()->db->beginTransaction();
		}

		try {
			$folder = LearningOrganization::model()->findByPk($idFolder);
			if ($folder->objectType != '') {
				throw new CException('Object '.$idFolder.' is not a folder!');
			}

			//if (!$folder->deleteNode()) {
			if (!self::deleteLearningObject($folder->gerPrimaryKey())) {
				throw new CException('Error while deleting folder "'.$folder->title.'" ('.$idFolder.').');
			}

			if (isset($trans)) { $trans->commit(); }
			$output = true;

		} catch (CException $e) {

			if (isset($trans)) { $trans->rollback(); }
			$output = false;
		}

		return $output;
	}



	/**
	 * Change the name of a folder
	 * @param integer $idFolder
	 * @param string $newTitle
	 * @return boolean
	 */
	public function updateFolderTitle($idFolder, $newTitle) {

		if (!is_numeric($idFolder) || $idFolder <= 0) { return false; }
		$folder = LearningOrganization::model()->findByPk($idFolder);
		if (!$folder) { return false; }
		if ($folder->objectType != '') { return false; }

		$folder->title = LearningObjectsManager::formatFolderTitle($newTitle);
		$rs = $folder->saveNode();

		return ($rs ? true : false);
	}



	/**
	 * Retrieve a course root node
	 * @param int $idCourse the id of the course of which we want retrieve the root node
	 * @return CActiveRecord the AR of the course root node
	 * @throws CException
	 */
	public static function getRootNode($idCourse) {
		$output = LearningOrganization::model()->getRoot($idCourse);
		if (!$output) {
			//root does not exists, so create it
			$courseRoot = new LearningOrganization();
			$courseRoot->title = LearningOrganization::ROOT_NODE_PREFIX.(int)$idCourse;
			$courseRoot->idCourse = (int)$idCourse;
			$courseRoot->visible = 1;
			$rs = $courseRoot->saveNode(); //it will call internally "makeRoot" method, assigning 'idCourse' as subtree identifier
			if (!$rs) { throw new CException('Error in course organization root node creation'); }
			$output = $courseRoot;
		}
		return $output;
	}



	/**
	 * Debug function: this is called to try to adjust tree level values in real time, when invalid values are detected
	 * @param int $idCourse the id of the course of which we are checknig LO tree
	 * @return void
	 */
	private static function _adjustTreeLevels($idCourse) {
		$root = self::getRootNode($idCourse);
		if ($root && $root->lev != 1) {
			$gap = 1 - $root->lev;
			$command = Yii::app()->db->createCommand("UPDATE ".LearningOrganization::model()->tableName()." "
				." SET lev = lev ".($gap > 0 ? "+" : "-")." ".(abs($gap))." "
				." WHERE idCourse = :id_course");
			$command->params[':id_course'] = $idCourse;
			$rs = $command->query();
		}
	}





	/**
	 * !!!!! NOT FINISHED. NOT USED !!!!!!!!!!!!
	 * Traverse the LO tree and if there is ONLY ONE SCO inside any SCORM LO (folder),
	 * transform the SCO into a regular playable object, removing the SCORMORG as a folder.
	 * That way DCORM is represented by a single Playable Object
	 *
	 * @param array $tree
	 */
	public static function handleSingleScoScorm($tree) {

		$result = array();

		// Walk a node and count SCO items inside it, i.e. playable objects
		$walkNode = function ($item, $key) use (&$walkNode, &$scoCount) {
			if ( isset($item['children']) && is_array($item['children']) ) {
				array_walk($item['children'], $walkNode);
			}
			else {
				if ( isset($item['type']) && ($item['type'] == 'sco') ) {
					$scoCount++;
				}
			}
		};


		// Callback for array_walk.
		$walkCallBack = function ($item, $key) use (&$walkCallBack, &$result, &$walkNode, &$scoCount) {

			// If this item is a SCORMORG, walk down the nodes; It will count SCO's inside
			if ( ($item['type'] == 'scormorg') && (is_array($item) && isset($item['children']) && is_array($item['children']) ) ) {
				$scoCount = 0;
				array_walk($item['children'], $walkNode);

				// If there is ONLY one SCO inside...
				if ($scoCount == 1) {
					// So far so good... but..
					// NOW WHAT???  We have to manipulate the BIG TREE....
					// errm....
				}
			}

			if (is_array($item) && isset($item['children']) && is_array($item['children']) ) {
				$level++;
				array_walk($item['children'], $walkCallBack);
			}


		};

		// Walk the array. array_walk_recursive is NOT usable as it does NOT visit sub-arrays!
		if (count($tree) > 0) {
			array_walk($tree, $walkCallBack);
		}



	}

	/**
	 * Accepts Learning Objects Tree (as returned from LearningObjectsManager::getCourseLearningObjectsTree())
	 * and returns a FLATTENED, linear LIST of playable objects (which are arrays also)
	 *
	 * @param array $loTree
	 * @param array $params
	 * @return array
	 */
	public static function flattenLearningObjectsTree($loTree, $params = array()) {

		$result = array();
		// Callback for array_walk.
		$walkCallBack = function ($item, $key) use (&$walkCallBack, &$result, $params) {

			if (is_array($item) && isset($item['children']) && is_array($item['children']) ) {
				// If this tree item is AICC-AU item (playable), add it to the list
				if (($item['type'] == 'aicc') && ($item['aiccItemType'] == LearningAiccPackage::ITEM_TYPE_AU)) {
					$result[] = $item;
				}
				elseif ((isset($params['getFolders']) && $params['getFolders'] == true) && $item['isFolder'] == 1 && $item['level'] == 2 && $item['type'] != LearningOrganization::OBJECT_TYPE_SCORMORG){
					foreach ( $item['children'] as $key => &$folder ) {
						if ( $folder['type'] == 'aicc')
							unset($item['children'][$key]);
						else
							$folder['isChildren'] = true;
					}
					$result[] = $item;
				}

				array_walk($item['children'], $walkCallBack);
			}
			else {
				// If this LO tree item is NOT AICC || is AICC && is AU (playable)
				if (!isset($item['aiccItemType']) || ($item['aiccItemType'] == LearningAiccPackage::ITEM_TYPE_AU)) {
					$result[] = $item;
				}
			}
		};

		// Walk the array. array_walk_recursive is NOT usable as it does NOT visit sub-arrays!
		if (count($loTree) > 0) {
			array_walk($loTree, $walkCallBack);
		}


		// Result is filled by walk callback.
		return $result;

	}

}
