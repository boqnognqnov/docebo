<?php

class ReportHelper {
	public $id;
	public $title;
	public $is_folder = false;
	public $objectId;
	public $organizationId;
	public $objectType;
	public $totalItems = 0;
	public $itemsInitState = 0;
	public $itemsCompleted = 0;
	public $itemsAttempted = 0;
	public $itemsFailed = 0;
	public $itemsPassed = 0;
	public $times = array();
	public $scores = array();
	public $scormItemTreeLevel = false;
	public $isAiccItem = false;
	public $versionName = '';
	// Since Central Repo a Learning Object can participate in many courses with shared tracking
	public $participateInOtherCourses = 0;
	
	public $failedAttemptsTaken = 0;
	public $failedAttemptsAllowed = 0;


	/**
	 * Calculate average time in seconds
	 * @return int
	 */
	public function averageTime()
	{
		$average = 0;
		$sumTimes = 0;
		foreach ($this->times as $time)
			$sumTimes += $time;

		if ($sumTimes > 0)
			$average = $sumTimes / count($this->times); //avg in seconds

		return $average;
	}

	/**
	 * Calculate average scores
	 * @return string
	 */
	public function averageScore()
	{
		$avgPair = $this->averageScorePair();
		//return $avgPair['raw'].'/'.$avgPair['max'];
		return number_format($avgPair['raw'],2);
	}

	public function averageScorePair()
	{
		$sumMax = 0;
		$sumRaw = 0;
		$avgMax = 0;
		$avgRaw = 0;
		$scoresCnt = count($this->scores);
		if ($scoresCnt)
		{
			foreach ($this->scores as $score)
			{
				$sumMax += $score['max'];
				$sumRaw += $score['raw'];
			}
			$avgMax = $sumMax / $scoresCnt;
			$avgRaw = $sumRaw / $scoresCnt;
		}
		return array('max' => $avgMax, 'raw' => $avgRaw);
	}

	/**
	 * Filter array data for the grid
	 * @param $dataArray
	 * @param $searchText
	 * @return mixed
	 */
	public static function filter(array $dataArray, $searchText)
	{
		foreach ($dataArray as $index => $report)
		{
			if (stripos($report->title, $searchText) === false)
				unset($dataArray[$index]);
		}
		return $dataArray;
	}

	public static function getCompletedPercentage(array $dataArray)
	{
		$completed = 0;
		$total = count($dataArray);
		foreach ($dataArray as $report)
		{
			if ($report->itemsCompleted)
				$completed++;
		}

		if ($total > 0)
			return round(($completed * 100) / $total);
		else
			return 0;
	}

	public static function getCompletedCounter(array $dataArray)
	{
		$completed = 0;
		foreach ($dataArray as $report)
		{
			if ($report->itemsCompleted)
				$completed++;
		}
		return $completed;
	}

	public static function getTimesSum(array $dataArray)
	{
		$totalTime = 0;
		foreach ($dataArray as $report)
		{
			foreach ($report->times as $time)
				$totalTime += $time;
		}
		return $totalTime;
	}
}