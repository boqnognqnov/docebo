<?php
/**
 *
 * Module specific base controller
 *
 * @property $courseModel LearningCourse
 */
class PlayerBaseController extends LoBaseController
{

	// Course ID is the same for all controllers inheriting this one
	public $course_id = null;
	public $userCanAdminCourse = false;
	public $userIsSubscribed = false;
	public $courseModel = null;
	protected $pathId = null;
	public $courseStatus = null;
	
	/**
	 * How current user is affected by coaching mechanic for the current course
	 *  
	 * @var integer
	 */
	public $coachingSessionStatus;

	// if classroom course are enabled, save info on session too, if provided
	protected $_sessionModel = null;

	protected $adminJsAssetsUrl = null;


	//some get / set functions

	public function getSessionModel() {
		return (PluginManager::isPluginActive('ClassroomApp') ? $this->_sessionModel : false);
	}

	public function setSessionModel($value) {
		if (!PluginManager::isPluginActive('ClassroomApp')) return false;
		if (is_object($value) && get_class($value) == 'LtCourseSession') {
			$this->_sessionModel = $value;
			return true;
		}
		return false;
	}



	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
				'accessControl',
		);
	}
	
	
	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		
		// Merge with parent rules, if any
		return CMap::mergeArray(parent::accessRules(), array(
		));
		
	}
	

	/**
	 * (non-PHPdoc)
	 * @see CController::init()
	 */
	public function init()
	{
		
		JsTrans::addCategories(array('player', 'authoring', 'classroom', 'error', 'course_management'));
		
		// Set some globally inheritted controller properties && make some preliminary checks
		$this->course_id = Yii::app()->request->getParam("course_id", FALSE);
		if (!$this->course_id) {
			Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
			$this->redirect(array('/site'),TRUE);
		}


		// Check if course exists
		$this->courseModel = LearningCourse::model()->findByPk($this->course_id);
		if (!$this->courseModel) {
			$this->redirect(array('/site'));
		}

		if (PluginManager::isPluginActive('ClassroomApp') && $this->courseModel->isClassroomCourse()) {
			$idSession = $this->getIdSession();
			if ($idSession) {
				$sessionModel = LtCourseSession::model()->findByAttributes(array('id_session' => $idSession, 'course_id' => $this->course_id));
				if ($sessionModel) { $this->sessionModel = $sessionModel; }
			}
		}


		// Check if user is subscribed and can enter course
		$model = LearningCourseuser::model()->findByAttributes(array("idUser" => Yii::app()->user->id, "idCourse" => $this->course_id));
		if ($model) {
			$levelInCourse = $model->level;
			$this->userIsSubscribed = true;
			$this->courseStatus = $model->status;
			// Check if this user is not an admin and can enter course
			if (Yii::app()->user->getIsUser()) {
				$canEnterCourse = $model->canEnterCourse();
				if (!$canEnterCourse['can']) {

					$message = Yii::t('standard', '_OPERATION_FAILURE');

					if($canEnterCourse['reason'] == 'course_status'){
						$message = Yii::t('standard', "Your status does not allow access at the moment");
					}

					Yii::app()->user->setFlash('error', $message);
					$this->redirect(array('/site'), TRUE);
				}
			}
		} else {
			$levelInCourse = 0;
			$this->userIsSubscribed = false;
			// Only subscribed users can play this course (with the exception of admins)
            Yii::app()->user->loginByAccessToken();
			if (!Yii::app()->user->getIsGodadmin() && !Yii::app()->user->getIsPu()) {
				Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
				$this->redirect(array('/site'), TRUE);
			}
		}

		// Is this a power user and the course was assigned to him?
		$pUserCourseAssigned = CoreUserPuCourse::model()->findByAttributes(array(
			'puser_id' => Yii::app()->user->id,
			'course_id' => $this->course_id
		));

		// If one of the following conditions is true the
		// user has access to administer the course:
		// 1. Is Goadmin
		// 2. Is Instructor
		// 3. Is Power User, has this course assigned to him and has "course edit" permissions
		$this->userCanAdminCourse = ($levelInCourse && $levelInCourse > LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT
									&& $levelInCourse != LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR && $levelInCourse != LearningCourseuser::$USER_SUBSCR_LEVEL_COACH)
									|| Yii::app()->user->isGodAdmin
									|| ($pUserCourseAssigned && Yii::app()->user->checkAccess('/lms/admin/course/mod'));

		// allow plugins to make further checks before the user is allowed to enter a course
		Yii::app()->event->raise('BeforeCoursePlayerMainPageRender', new DEvent($this, array(
			'course' => $this->courseModel
		)));

		if(PluginManager::isPluginActive('CurriculaApp')){
			$commingFrom = Yii::app()->request->getParam('coming_from', false);

			if($commingFrom === 'lp'){
				$pathId = Yii::app()->request->getParam('id_plan', false);

				$pathModel = LearningCoursepath::model()->findByPk($pathId);

				if($pathModel instanceof LearningCoursepath){
					Yii::app()->player->setCoursePath($pathModel);
					$this->pathId = $pathModel->id_path;
				}
			} else{
				$lpEnrollments = LearningCoursepath::checkEnrollment(Yii::app()->user->idst, $this->course_id, true);

				if($lpEnrollments && count($lpEnrollments) == 1){

					$first = reset($lpEnrollments);

					$pathModel = LearningCoursepath::model()->findByPk($first['id_path']);

					if($pathModel instanceof LearningCoursepath){
						Yii::app()->player->setCoursePath($pathModel);
						$this->pathId = $pathModel->id_path;
					}
				}
			}
		}

		// Player App. component is preloaded. Here we just set its attributes
		Yii::app()->player->setCourse($this->courseModel);


		// To allow using OLD LMS code base we need to set course id as session variable
		// @TODO Remove as soon as we start porting the player in Yii!
		$_SESSION['idCourse'] = $this->course_id;
		
		
		// Certification Plugin Related
		if(PluginManager::isPluginActive('CertificationApp')) {
			// If we find a reset token in the request, check if the token is still available (it is for ONE TIME usage)
			// If it is, reset the course+user information (tracking and everything else)
			// Then remove the token from plugin table(s)
			// Also, set the "in_renew" mode to true (1)
			$resetToken = Yii::app()->request->getParam("reset", null);
			if ($resetToken) {
				$certUser = CertificationUser::model()->findByAttributes(array('reset_token' => $resetToken));
				if($certUser) {
					LearningCourseuser::reset($this->course_id, Yii::app()->user->id);
					$certUser->reset_token = null;
					$certUser->in_renew = 1;
					$certUser->save();
				}
			}
		}
		
		
		
		

		return parent::init();
	}


	/**
	 * (non-PHPdoc)
	 * Method invoked before any action
	 * @param CAction $action
	 * @return bool
	 */
	public function beforeAction($action)
	{
		return parent::beforeAction($action);
	}


	/**
	 * overwrite resolveLayout method to use module based layout
	 */
	public function resolveLayout()
	{
        $this->layout = '/layouts/base';
	}


	/**
	 * Reload course header with ajax
	 */
	public function actionAxUpdateCourseHeader()
	{
		$result = new AjaxResult();
		$html = $this->widget('player.components.widgets.CourseHeader', array(
			'course_id' => $this->course_id,
			'userCanAdminCourse' => $this->userCanAdminCourse,
			'targetMode' => Yii::app()->request->getParam('targetMode')
		), true);

		$result->setHtml($html)->setStatus(true)->toJSON();
	}

	/**
	 * Change course status
	 *
	 * @param unknown $status
	 */
	public function actionAxChangeCourseStatus($status)
	{
		$result = new AjaxResult();
		$model = LearningCourse::model()->findByPk($this->course_id);
		if ($status == 'published') {
			$model->status = LearningCourse::$COURSE_STATUS_EFFECTIVE;
			$data = array('new_status' 	=> Yii::t('standard', 'Published'));
		} else {
			$model->status = LearningCourse::$COURSE_STATUS_PREPARATION;
			$data = array('new_status' 	=> Yii::t('standard', 'Unpublished'));
		}

		$model->id_publisher = Yii::app()->user->id;
		$model->publish_date = Yii::app()->localtime->getLocalNow();
		$ok = $model->save(false);

		$result->setStatus($ok);
		$model->refresh();
		$data['publish_status_html'] = $this->renderPartial('player.views.coursesettings.publish.status', array('courseModel' => $model), true);
		$result->setData($data)->toJSON();
	}




	/**
	 * Register module specific assets
	 *
	 * (non-PHPdoc)
	 * @see LoBaseController::registerResources()
	 */
	public function registerResources() {

		parent::registerResources();

		if (!Yii::app()->request->isAjaxRequest) {

			$cs = Yii::app()->getClientScript();

			// Player.setOptions() JS in Document Ready
			$options = array(
				'course_id' => $this->course_id,
				'updateCourseHeaderUrl' => $this->createUrl('axUpdateCourseHeader', array('course_id'=>$this->course_id)),
				'changeCourseStatusUrl' => $this->createUrl('axChangeCourseStatus', array('course_id'=>$this->course_id)),
				'playerModuleBaseUrl'   => $this->createUrl('/player'),
			);
			$options = CJavaScript::encode($options);
			$script = "Player.setOptions($options);";
			$cs->registerScript("player_basecontroller_setoptions", $script , CClientScript::POS_READY);
		}

	}

    public static function ref_src($array) {
        if (isset($_GET['ref_src']) && $_GET['ref_src'])
            $array['ref_src'] = $_GET['ref_src'];

        return $array;
    }

	/**
	 * Publish Admin JS assets and return URL
	 */
	public function getAdminJsAssetsUrl()	{
		if ($this->adminJsAssetsUrl === null) {

			$this->adminJsAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias("admin.js"));
		}

		return $this->adminJsAssetsUrl;
	}


	/**
	 * Helper function to build url params in Training Controller
	 * Additional params for Learning Plan are added if we came from Learning Plan or the
	 * course is part of a Learning Plan that the user is enrolled
	 *
	 * @param array $params Additional params
	 * @return array Url params
	 */
	protected function helperBuildParamData($params = array()){
		$getParams = array(
			'course_id' => $this->course_id
		);

		if($this->pathId){
			$getParams['coming_from'] = 'lp';
			$getParams['id_plan'] = $this->pathId;
		}

		if(!empty($params)){
			$getParams = array_merge($getParams, $params);
		}

		return $getParams;
	}

    /* Editing breadcrumb use according to users' status in a course */
    public static function breadcrumbUserSubscription($model) {
        if($model->userCanAdminCourse && !$model->userIsSubscribed)
            return $_GET['ref_src'] = 'admin';
    }
}