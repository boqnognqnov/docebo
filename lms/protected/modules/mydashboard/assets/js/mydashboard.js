/**
 * jQuery Plugin for building/serving My Dashboard, front end & admin alltogether
 *
 * @param $
 */
(function($) {

	/**
	 * Define constants for different dashboard ROW layouts.
	 * They must replicate the ones defined in PHP::DashboardRow model.
	 */
	var ROW_LAYOUT_1_12 	= 1;   	// 1 x 12
	var ROW_LAYOUT_2_66 	= 2;	// 2 x 6
	var ROW_LAYOUT_3_4 		= 3;	// 3 x 4
	var ROW_LAYOUT_2_8_4 	= 4;	// 8 + 4
	var ROW_LAYOUT_2_4_8 	= 5;	// 4 + 8

	/* Hold pluging options in this closure variable */
	var settings = {};

	/* Enable disable log() output to console */
	var debug = true;

	/* Hold dashboard and dashboard dashlets jQuery Objects */
	var $dashboard;  			// the whole #mydashboard
	var $dashboardDashlets;		// .mydashboard-dashlets

	/**
	 * A nice logging/debugging function
	 */
    var log = function() {
    	if (!debug) return;
    	if (window.console && window.console.log) {
    		var args = Array.prototype.slice.call(arguments);
    		if(args){
    			$.each(args, function(index, ev){
    				window.console.log(ev);
    			});
    		}
        }
    };


    /**
     * Reload the whole ROW HTML (replacing it)
     */
    var reloadRow = function(rowId) {
		// Now render the row again (AJAX) and get the HTML
		var url = settings.dashControllerUrl + "/getRawHtml";
		var data = {
			id: rowId,
			admin: 1
		};
		$.get(url, data, function(html){
			// Replace current row's HTML with the new one
			$dashboardDashlets.find('.dashlets-row[data-row-id='+rowId+']').replaceWith(html);
			// As a final step, we have to Load the content for row's dashlets;
			// We have their IDs from the controller first response
			$dashboard.dashboard('loadContent', '.dashlets-row[data-row-id='+rowId+'] .dashlet');
			// And.. make everything sortable so new row and dashlets to be movable/draggable etc
			sortize();
		})

    };


	/**
	 * ADD Listeners
	 */
	var addAdminDashletsListeneres = function() {


		/**
		 * Toggle/Change DEFAULT layout
		 */
		$(document).on('click', '.toggle-default-layout-link', function(e){
			var url = settings.dashControllerUrl + "/setLayoutAsDefault";
			var data = {
				id : $(e.currentTarget).data('id')
			};
			$.getJSON(url, data, function(response){
				if (response.success) {
					$('#dashboard-layouts-grid').comboGridView('updateGrid');
				}
			});
		});


		/**
		 * When LEVEL column in Layouts grid is clicked, SELECT element is shown and level can be changed
		 */
		// Level DISPLAY text is a link, which when clicked, hides itself and shows the SELECT
		$(document).on('click', '.current-level-value-link', function(){
			$(this).hide().closest('.layout-level-column').find('.layout-level-select').show();
		});

		// We need this to provide a way to update level even it is NOT changed (for the scenario when there is still no selected level)
		$(document).on('blur', '.layout-level-select', function(){
			$(this).change();
		});;
		 //.... and then we watch for the SELECT change, make an AJAX call and change it in the backend
		$(document).on('change', '.layout-level-select', function(){
			var that = this;
			var link = $(this).closest('.layout-level-column').find('.current-level-value-link');
			$(this).hide();
			link.show();

			var data = {
				id:	$(this).closest('.layout-level-column').data('layout-id'),
				level: $(this).val()
			};
			var url = settings.dashControllerUrl + "/changeLayoutLevelFilter";
			$.post(url, data, function(response){
				if (response.success) {
					link.text(response.data.level_label);
					$('#dashboard-layouts-grid').comboGridView('updateGrid');
				}
			}, "json");
		});


		/**
		 * "Clone layout" link clicked
		 */
		$(document).on('click', '.clone-dashboard-layout-link', function(){
			var url = settings.dashControllerUrl + "/cloneLayout";
			var data = {
				id: $(this).data('layout-id')
			};
			$.getJSON(url, data, function(response){
				if (response.success) {
					$('#dashboard-layouts-grid').comboGridView('updateGrid');
				}
			});
		});



		/**
		 * Listen for various Dialog2 dialogs that only needs to be closed, with NO additional action required
		 */
		$(document).on('dialog2.content-update', '[id^="edit-dashboard-layout"],[id^="delete-dashboard-layout"],[id^="dashboard-layout-filter-selector"],[id^="dashboard-layout-puprofiles-selector"]', function() {
			var $aElem = $(this).find("a.auto-close.success");
			if ($aElem.length > 0) {
				$(this).dialog2("close");
				$('#dashboard-layouts-grid').comboGridView('updateGrid');
			}
		})



		/**
		 * Listen for Create/Edit Dashlet Dialog2 content update (the Wizard) and closing with success
		 * This Dialog2 can not be just closed! BEFORE closing we need its content to extract response data in <a> (using markup to send response)
		 */
		$(document).on('dialog2.content-update', '[id^="create-dashlet-wizard"]', function() {
			var $aElem = $(this).find("a.auto-close.success");
			if ($aElem.length > 0) {

				// We get the Cell Id in  a.data-cell-id attribute!!
				var cellId 		= $aElem.data('cell-id');
				var dashletId 	= $aElem.data('dashlet-id');
				$(this).dialog2("close");

				if (cellId) {
					var rowId = $dashboard.find('.dashlets-cell[data-cell-id="'+cellId+'"]').closest('.dashlets-row').data('row-id');
					reloadRow(rowId);
				}

				if (dashletId) {
					$dashboard.dashboard('loadContent', '.dashlet[data-dashlet-id="'+dashletId+'"]');
				}


			}
		});


		/**
		 * When wizard dialog is restored (actually, the form)
		 * 1. scroll to selected Dashlet Type, if any
		 * 2. Enable/disable "NEXT" button
		 */
		$(document).on('jwizard.form-restored', '', function(){
			if ($('.dl-type-box.selected').length > 0) {
				$('.modal-body#create-dashlet-wizard').animate({
					scrollTop: $('.dl-type-box.selected').offset().top - 3 * $('.dl-type-box.selected').height()
				}, 1000);
			}
		});


		/**
		 * Dashlet TYPE clicked (selected) (in the Create dashlet Wizard Step 1)
		 */
		$(document).on('click', '.dl-type-box:not(.disallowed)', function(e){
			// Remove .selected from all type boxes and add it to the currently clicked type box
			$('.dashlet-type-boxes .dl-type-box').removeClass('selected');
			$(this).addClass('selected');
			$(this).closest('form').find('input[name="selected_type"]').val($(this).data('dashlet-handler'));
		});


		/**
		 * Upon changing the "selected_type" value, visualize the selection on UI (in Create dashlet Step 1)
		 */
		$(document).on('change', 'input[name="selected_type"]', function(e){
			$('.dashlet-type-boxes').find('[data-dashlet-handler="'+$(this).val()+'"]').addClass('selected');
		});


		/**
		 * REMOVE DASHLET button clicked
		 */
		$dashboard.on('click', '.remove-dashlet-button', function(e){
			e.preventDefault();
			var dashletId = $(e.target).closest('.dashlet').data('dashlet-id');
			var url = settings.dashControllerUrl + "/deleteDashlet";
			$.getJSON(url, {id: dashletId}, function(data){
				if (data.success) {
					$(e.target).closest('.dashlet').remove();
				}
			});
		});


		/**
		 * OPEN EXISTING DASHLET SETTINGS DIALOG2
		 */
		$dashboard.on('click', '.dashlet-settings-button', function(e){
			e.preventDefault();
			var dashletId = $(e.currentTarget).closest('.dashlet').data('dashlet-id');
			var title = Yii.t('standard', '_MOD');
			var url = settings.dashControllerUrl + "/editDashletSettings&id=" + $(e.currentTarget).closest('.dashlet').data('dashlet-id');
			var data = {
				id: dashletId
			};
			openDialog(false, title, url, 'create-dashlet-wizard', 'create-dashlet-wizard', 'POST', data);
		});


		/**
		 * OPEN ADD NEW DASHLET DIALOG2
		 */
		$dashboard.on('click', '.add-dashlet-button', function(e){
			e.preventDefault();
			var cellId = $(this).closest('.dashlets-cell')	.data("cell-id");
			var title = Yii.t('standard', '_ADD');
			var url = settings.dashControllerUrl + "/dashletWizard";
			var data = {
				cellId: cellId
			};
			openDialog(false, title, url, 'create-dashlet-wizard', 'create-dashlet-wizard', 'POST', data);
		});


		/**
		 * SELECT/Change A Row LAYOUT clicked
		 */
		$dashboard.on('click', '.select-row-layout-button', function(e){
			e.preventDefault();
			var currentLayout = $(this).closest('.row-layout-selector').data('row-layout');
			var layoutClicked = $(e.currentTarget).data('row-layout');
			if (layoutClicked === currentLayout) {
				return;
			}

			// AJAX call to change the Row Layout
			var url = settings.dashControllerUrl + "/changeRowLayout";
			var data = {
				id: $(this).closest('.dashlets-row').data('row-id'),
				rowLayout: layoutClicked
			};

			// Call controller to rearrange the row and then reload the whole row
			$.getJSON(url, data, function(response){
				if (response.success) {
					reloadRow(response.data.id);
				}
			});

		});

		/**
		 * ADD NEW ROW button clicked
		 */
		$dashboard.on('click', '.new-layout-row-button', function(e){
			e.preventDefault();

			// AJAX call to create new row, of default row layout (controller will decide)
			var url = settings.dashControllerUrl + "/createNewRow";
			var data = {
				layout	: settings.layoutId
			};
			$.getJSON(url, data, function(response){
				if (response.success) {
					// Now, load the ROW HTML and insert it
					var url = settings.dashControllerUrl + "/getRawHtml";
					var data = {
						id: response.data.id,
						admin: 1
					};
					$.get(url, data, function(html){

						// If NEW rows goes on TOP:
						//$dashboardDashlets.prepend($(html));

						// IF we want it in the bottom::
						$dashboardDashlets.append($(html));
						$('body').animate({
							scrollTop: $('.dashlets-row[id="row-' + response.data.id + '"]').offset().top
						}, 700);



						sortize();
					})
				}
			});
		});


		/**
		 * CLONE A ROW button clicked
		 */
		$dashboard.on('click', '.clone-row', function(e){
			e.preventDefault();

			// AJAX call to clone a row
			var url = settings.dashControllerUrl + "/cloneRow";
			var data = {
				row: $(this).closest('.dashlets-row').data('row-id')
			};
			$.getJSON(url, data, function(response){
				if (response.success) {
					// Now, load the ROW HTML and insert it
					var url = settings.dashControllerUrl + "/getRawHtml";
					var data = {
						id: response.data.id,
						admin: 1
					};
					$.get(url, data, function(html) {
						// Prepend the HTML of the row in the dashboard
						// This is ONLY the "sceleton", NO dashlets yet loaded
						$dashboardDashlets.prepend($(html));

						// Now call the plugin method to load all the dashlets content of the just created, new row
						$dashboard.dashboard('loadContent', '.dashlets-row[id="row-'+response.data.id+'"] .dashlet');

						// Sortize of course
						sortize();
					})
				}
			});
		});



		/**
		 * REMOVE A ROW button clicked
		 */
		$dashboard.on('click', '.remove-row-button', function(e){
			e.preventDefault();

			// AJAX call to create new row, of default row layout (controller will decide)
			var url = settings.dashControllerUrl + "/deleteRow";
			var data = {
				id	: $(this).closest('.dashlets-row').data('row-id')
			};
			$.getJSON(url, data, function(response){
				if (response.success) {
					$('.dashlets-row[data-row-id='+response.data.id+']').remove();
				}
			});
		});



	}



	/**
	 * Make certain elements from DOM jquery.soartable
	 */
	var sortize = function() {

		/**
		 * ROWS
		 */
		$dashboardDashlets.sortable({
			scroll: true,
			items: '.dashlets-row',
			//handle: '.row-admin-drag-handler',
			handle: '.row-admin-header',
			tolerance: "pointer",
			cursor: "move",
			opacity: 0.8,
			helper: "clone",
			update: function(e, ui) {
				var sorted 		= $(this).sortable("toArray");
				var data = {
					layout		: settings.layoutId,
					reorderType	: "row",
					sorted		: sorted
				};

				$.ajax({
					url: settings.dashControllerUrl + "/reorder",
					type: 'post',
					dataType: 'json',
					data: data,
					success: function() {
					}
				});
				return true;

			},
			remove: function(e, ui) {

			}
		});

		/**
		 * Dashlets INSIDE rows
		 */
		$dashboardDashlets.find('.dashlets-sortable').sortable({
			items: ".dashlet",
			//handle: ".dl-header .drag-handle",
			handle: ".dl-header",
			tolerance: "pointer",
			opacity: 0.8,
			cursor: "move",
			helper: "clone",
			update: function(e, ui) {
				log('Update dashlet..');

				var sorted 		= $(this).sortable("toArray");
				var cellId 		= $(this).closest('.dashlets-cell')	.data("cell-id");
				var data = {
					layout		: settings.layoutId,
					reorderType	: "dashlet",
					cellId		: cellId,
					sorted		: sorted
				};

				$.ajax({
					url: settings.dashControllerUrl + "/reorder",
					type: 'post',
					dataType: 'json',
					data: data,
					success: function() {
					}
				});
				return true;
			},
			remove: function(e, ui) {
			},
			receive: function(e, ui) {
			},
			start: function() {
			},
			stop: function() {
			},
			connectWith: '.dashlets-sortable'
		});
	};


	/**
	 * Perform additional, after-loading processing and automation
	 */
	var postProcessDashletContent = function(dashlet) {
		// Display appropriate Courses Catalog dashlet according to its size and cell width
		$('li[class="dashlet mydashboard.widgets.DashletCatalog"]').each(function() {
			var cellWidth = $(this).parent().parent().data('cell-width');
			var windowWidth =   $(window).width();
			var id = $(this).data('dashlet-id');

			if($('.display-style-switch.dark .fa', $(this)).length > 0) {
				var selectedGridTypeClass = $('.display-style-switch.dark .fa', $(this)).attr('class');

				if (selectedGridTypeClass.indexOf("fa-calendar") >= 0) {
					if(windowWidth > 800 && (cellWidth == 8 || cellWidth ==12)) {
						$('#mini-calendar' + '-' + id).hide();

						$('.calendar-title-container', $(this)).show();
						if(cellWidth == 12) {
							$('.view-switch-container', $(this)).show();
						} else {
							$('.view-switch-container', $(this)).hide();
						}

						$('#calendar' + '-' + id).show();
					} else {
						$('#calendar' + '-' + id).hide();
						$('.calendar-title-container', $(this)).hide();
						$('.view-switch-container', $(this)).hide();
						$('#mini-calendar' + '-' + id).show();
					}

					// Load calendar
					if($('#calendar' + '-' + id).is(':visible') == true) {
						$('#calendar' + '-' + id).fullCalendar('gotoDate', (new Date()));
					}

					if($('#mini-calendar' + '-' + id).is(':visible') == true) {
						$('#mini-calendar' + '-' + id).fullCalendar('gotoDate', (new Date()));
					}
				}
			}
		});

		// Automatically create "knobs" (circular progress indicator
		// with percentage, if the specific text field is found like:
		// <input value="[percent, e.g. 80]" class="auto-knob"/>
		var autoKnobs = $(dashlet).find('.auto-knob');
		if(autoKnobs.length > 0){
			autoKnobs.each(function(){
				log('Creating auto-knob (circular percentage indicator) in dashlet ID '+$(dashlet).data('dashlet-id'));

				var hexColor = '#6cc267';
				var size = 80;

				if($(this).data('color')){
					hexColor = $(this).data('color');
				}

				if($(this).data('size')){
					size = $(this).data('size');
				}

				$(this).knob({
					'min':       0,
					'max':       100,
					'readOnly':  true,
					//'displayInput': false,
					'fgColor':   hexColor,
					'bgColor':   '#cccccc',
					'thickness': .25,
					'width':     size,
					'height':    size,
					'draw':      function () {
						$(this.i).val(this.cv + '%')
					}
				});
			});
		}


	};


	/**
	 * Callable methods in form of
	 * $('#mydashboard').dashboard('<method-name>', <arguments>).
	 *
	 */
	var methods = {

		/**
		 * init() is a special methods and is called if no method is specified, e.g. $('#mydashboard').dashboard({}) will call init().
		 * It must be called always first!!!!!! Without calling it first, the whole plugin won't work!!!!
		 */
		init: function(options) {
			settings = $.extend({}, settings, options);

			if (typeof settings.debug !== "undefined")
				debug = settings.debug;

			$dashboard = this;
			$dashboardDashlets = $dashboard.find('.mydashboard-dashlets');


			// Admin mode
			if (settings.adminDashletsMode) {
				$('body').addClass('mydashboard-admin');
				$dashboard.addClass('admin');
				sortize();
				addAdminDashletsListeneres();
			}

			if (settings.adminLayoutsMode) {
				$('body').addClass('mydashboard-admin');
				addAdminDashletsListeneres();
			}

			if (settings.autoLoadContent) $dashboard.dashboard('loadContent');


			return this;


		},

		/**
		 * Load ALL dashlets content. Called like: $('#mydashboard').dashboard('loadContent');
		 * Optionally a dashlet selector can be passed to load the content of a specific dashlet or dashlets
		 *
		 * Calls the "callback" at the end of AJAX's success callback
		 */
		loadContent: function(selector, params, callback) {

			/**
			 * Traverse all dashlets (or by selector) already loaded in the HTML
			 */
			if (!selector)
				selector = '.dashlet';

			$dashboardDashlets.find(selector).each(function(){
				var dashlet = this;
				var id = $(this).data('dashlet-id');
				var url = settings.dashControllerUrl + "/loadDashletContent&id=" + id;

				$(dashlet).find('.dl-content').children().hide();
				$(dashlet).find('.dl-content').append($('<div class="ajaxloader"></div>').css('position', 'static').show());

				// Make an AJAX call to load the dashlet content and other stuff
				if (typeof params === "undefined") {
					params = {};
				}

				// Add ACTUAL layout id to the request (could be REAL or Virtual)
				params.layout_id = $dashboard.data('layout-id');

				$.post(url, params, function(data, textStatus, jqXHR){

					if (textStatus === 'success' && data) {
						if (data.content) {
							$('<div class="ajaxloader"></div>').remove();

							// Height
							$(dashlet).find('.dl-content').css('height', data.height);

							// Min-Height
							$(dashlet).find('.dl-content').css('min-height', data.minHeight);

							// Content
							$(dashlet).find('.dl-content').html(data.content);

							// Header TEXT
							$(dashlet).find('.dl-header-text').html(data.header);

							// Footer is handled in a special way, extracted from content, moved to .dl-footer
							var footer = $(dashlet).find('.dl-content .dl-footer').remove();
							if (footer) {
								$(dashlet).find('div.dl-footer').replaceWith(footer);
							}
							else {
								$(dashlet).find('div.dl-footer').remove();
							}

						}

						if ((data.handles !== '') && settings.adminDashletsMode) {
							$(dashlet).find('.dl-handles').html(data.handles);
						}


						// Post-loading processing
						postProcessDashletContent(dashlet);

						// Apply some standard things we need, like tooltips, Dialog2 ajaxify, etc.
						$('[rel="tooltip"]', dashlet).tooltip({
							container: 'body'
						});
						$(document).controls();

						// Trigger an event to inform the world that a given dashlet just finished loading
						$(dashlet).trigger('mydashboard.dashlet.content-loaded', {dashletId: id});

					}

					// Callback
					if (typeof callback === 'function') {
						callback(textStatus, dashlet);
					}



				}, 'json');
			});

		},

		/**
		 * Call this when Layout -> User filter TOP panel is ready (the all/selected one)
		 */
		selectorTopPanelReady: function() {

			/* Local function to update UI  */
			var updateUi = function() {
				var elem = $('input[name="ufilter_option"]:checked');
				var modal = elem.closest('.modal');
				modal.css("min-height", "1px");
				modal.find('.modal-body').css("min-height", "1px");
				if (elem.val() == 1)
					$('.main-selector-area').hide();
				else
					$('.main-selector-area').fadeIn();
			}


			/* When Radio is changed */
			$('input[name="ufilter_option"]').on('change', function(){
				$(this).trigger('refresh');
				updateUi();
			});

			$('input[name="ufilter_option"]').trigger('change');

			// Style inputs (but see listeners, wehere we trigger "refresh"!
			$('.selector-top-panel :input[type="radio"]').styler();

			//updateUi();

		},

		/**
		 * Auomating the process of $.sortable initialization for dashlet settings markup (inputs) (Admin Dashlets UI)
		 * Finds .auto-sortable container inside the "selector" and make all .setting being sortable elements.
		 * (Note that if you include the "prevent-sortable" class to the the .setting element,
		 * it will prevent that element from being movable)
		 * On sorting "update" event, finds and sets <input id="field_order"> value to a comma separated list of <input> names just sorted,
		 * providing ordering information to the enclosing form.
		 *
		 * <form>
		 *  	<div class="auto-sortable">
		 *  		<div class="setting prevent-sortable"><input name="A"></div>
		 * 			<div class="setting"><input name="B"></div>
		 * 			<div class="setting"><input name="C"></div>
		 * 			<div class="setting"><input name="D"></div>
		 * 			<div class="setting"><input name="D"></div>
		 * 		</div>
		 * 		<input id="field_order">
		 * </form>
		 *
		 * For reference/hints, see: \lms\protected\modules\mydashboard\widgets\views\dashlet_mykpis_settings.php
		 *
		 */
		sortizeAutoSortable: function(selector) {

			log('Sortable');
			$(selector).find('.auto-sortable').each(function(){
				var t = $(this);
				t.sortable({
					items: '.setting-row:not(.prevent-sortable)',
					cancel: '.prevent-sortable',
					handle: '.setting-handle',
					update: function(e, ui){
						var sorted = $('.setting input', t);
						var arr = [];
						sorted.each(function(){
							arr.push($(this).attr('name'));
						});
						$('#field_order', t).val(arr.join(','));
					}
				});
			});
		}




	};

	/**
	 * Plug it in
	 */
	$.fn.dashboard = function(methodOrOptions) {
        if ( methods[methodOrOptions] ) {
            return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
            // Default to "init"
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  methodOrOptions + ' does not exist on jQuery.dashboard' );
        }
    };


})(jQuery);






/**
 * jQuery Plugin For MyCourses dashlet
 *
 * @param $
 */
(function($) {

	/* Hold pluging options in this closure variable */
	var settings = {};

	/* Enable disable log() output to console */
	var debug = true;

	/* Holds an array of IDs of My Courses Dashlets (.w-mycourses .dashlet) */
	var mycoursesDashlets = [];

	/* Holds current page for individual mycourse/catalog dashlets */
	var currentPage = [];

	/* A plugin-wide variable used as a semaphor for controlling various consequitive operations */
	var semaphor = false;

    /* True means the category tree is initializing */
    var initializingTree = false;

	/**
	 * A nice logging/debugging function
	 */
    var log = function() {
    	if (!debug) return;
    	if (window.console && window.console.log) {
    		var args = Array.prototype.slice.call(arguments);
    		if(args){
    			$.each(args, function(index, ev){
    				window.console.log(ev);
    			});
    		}
        }
    };

	var formData = null;



	/**
	 * Listen for events
	 *
	 * NOTE: If you have to, do first off('<event>') to avoid attaching same event multiple times to dashlets
	 *
	 */
	var addListeneres = function () {
		/** Lsiteners proxies, passing them the Dashlet Object context, "this" */
		$(this).find('.filters-button').off('click').on('click', $.proxy(onFiltersButtonClicked, this));
		$(this).find('.mydashboard-cancel-button').off('click').on('click', $.proxy(onCancelButtonClicked, this));
		$(this).find('.categories-button').off('click').on('click', $.proxy(onCategoriesButtonClicked, this));
		$(this).find('.display-style-switch').off('click').on('click', $.proxy(onDisplayStyleSwitchClicked, this));
		$(this).find('.search-button').off('click').on('click', $.proxy(onSearchButtonClicked, this));
		$(this).find('form.mycourses-filter-form').off('submit').submit($.proxy(onFormSubmit, this));
		$(this).find('.search-field input').off('keydown').on('keydown', $.proxy(onSearchInputKeydown, this));
		$(this).find('form button.clear-search').off('click').on('click', $.proxy(onClearSearchClicked, this));
		$(this).find('form select[name="category"]').off('change').on('change', $.proxy(onCourseCategoryChange, this));
		$(this).find('.dashlet-pager-next').off('click').on('click', $.proxy(onNextPageClicked, this));
		$(this).find('.dashlet-pager-prev').off('click').on('click', $.proxy(onPrevPageClicked, this));
		$(this).find('.back-to-catalogs').off('click').on('click', $.proxy(onBackToCatalogsClicked, this));
		$(this).find('.back-to-labels').off('click').on('click', $.proxy(onBackToLabelsClicked, this));
		$(this).find('div.view-more-box > p').off('click').on('click', $.proxy(onViewMoreClicked, this));
		$(window).on('resize', $.proxy(onPageResize, this)).trigger('resize');
	};

	var submitForm = function(data, dashletId){

		formData = data;

		var dlContent = $(this).find('.dl-content')

		// Preserve the height during loading to avoid screen flickering
		// But first add "reloading" class to dl-content
		dlContent.addClass('reloading');
		var currentContentHeight = dlContent.height();
		dlContent.height(currentContentHeight);

		// Load this very specific dashlet!! (by id)
		// On callback, remove the "reloading" class
		$('#mydashboard').dashboard('loadContent', '.dashlet[data-dashlet-id="'+dashletId+'"]', data, function(){
			dlContent.removeClass('reloading');
		});
	}

	var onCancelButtonClicked = function(e){
		var id = $(this).data('dashlet-id');
		var elem = $(e.currentTarget);
		var form = elem.closest('form');
		var params = $.deparam(form.serialize());

		var cookieName = 'dashlet_display_style_' + id;
		if($('form#login_confirm').length > 0) {
			cookieName += '_public';
		}

		//formDataLength = Object.keys(formData).length;
		if(!jQuery.isEmptyObject(formData)) {
			submitForm(formData, id);
		} else{
			// Is it in calendar mode or not
			var calendarMode = $('.display-style-switch.dark').find('.fa').is('.fa-calendar');

			//if($(this).find('.course-list').is(':visible') == false && !calendarMode){
			if ($.cookie(cookieName) === 'thumb' || $.cookie(cookieName) === 'list') {
				$(this).find('.filters-selector').hide();
				$(this).find('.course-list').show();
				$(this).find('.catalog-left-sidebar').show();
				$(this).find('.dashlet-pager').show();
				//elem.text(Yii.t('billing', 'Apply'));

				$('.filter-header .filters-button').text(Yii.t('dashboard', 'Filters'));
			//} else if(calendarMode && $(this).find('.catalog-calendar-content').is(':visible') == false) {
			} else {
				$(this).find('.filters-selector').hide();
				$(this).find('.catalog-calendar-content').show();

				$('.filter-header .filters-button').text(Yii.t('dashboard', 'Filters'));
			}
		}
	}


    /**
     * Event triggered when window is resized
     * this = dashlet
     */
    var onPageResize = function(e) {
        var $dashlet = $(this);
        var dashletId = $dashlet.data('dashlet-id');
        var $sidebar = $dashlet.find('.catalog-with-sidebar');
        var $categoryButton = $dashlet.find('.categories-button');
        if($sidebar.is(":visible")) {
            if (!$dashlet.find('.catalog-left-sidebar').is(':visible')) {
                $categoryButton.removeClass('selected');
                $categoryButton.find('i.fa').removeClass('fa-inverse');
                $('#current-category-dashlet-'+dashletId+'-filter').prop('disabled', false);
                $('#current-category-dashlet-'+dashletId+'-sidebar').prop('disabled', true);
                var currentCategoryId = $('#current-category-dashlet-'+dashletId+'-sidebar').val();
                $('#current-category-dashlet-'+dashletId+'-filter').val(currentCategoryId);
            } else {
                $categoryButton.addClass('selected');
                $categoryButton.find('i.fa').addClass('fa-inverse');
                $('#current-category-dashlet-'+dashletId+'-filter').prop('disabled', true);
                $('#current-category-dashlet-'+dashletId+'-sidebar').prop('disabled', false);
                var currentCategoryId = $('#current-category-dashlet-'+dashletId+'-filter').val();
                $('#current-category-dashlet-'+dashletId+'-sidebar').val(currentCategoryId);
            }
        }

        // Check if some view more button should be disabled
        $dashlet.find('.view-more-box').each(function(obj){
            // Count how many items are hidden or not rendered
            var invisibleItemsCount = $(this).data('remaining-courses') + $(this).siblings('.course-tile:hidden').length;
            if(invisibleItemsCount > 0)
                $(this).show();
            else
                $(this).hide();
        });
    }


	/**
	 * FILTERS button clicked, to show filters selector
	 * this = dashlet
	 */
	var onFiltersButtonClicked = function (e) {
		var elem = $(e.currentTarget);
		var form = elem.closest('form');
		var id = $(this).data('dashlet-id');

		if ($(this).find('.course-list').is(':visible') == true) {
			$(this).find('.course-list').hide();

			// Show filter by elearning and learning plan item types
			$('input[name="item_type"][value="learningplan"]').parent().show();
			$('input[name="item_type"][value="elearning"]').parent().show();

			$(this).find('.catalog-left-sidebar').hide();
			$(this).find('.dashlet-pager').hide();
			$(this).find('.filters-selector').show();
			// Make all visible filter field not readonly !!!
			$('#mycourses-filter-form-'+id+' input:visible, #mycourses-filter-form-'+id+' select:visible, #mycourses-filter-form-'+id+' textarea:visible').each(function(key, value) {
				$(this).removeAttr('disabled');
			});
			elem.text(Yii.t('billing', 'Apply'));
		}
		else if(($(this).find('#calendar-' + id).is(':visible') == true) || ($(this).find('#mini-calendar-' + id).is(':visible') == true)){
			// If the calendar mode is on proceed here
			$(this).find('.catalog-calendar-content').hide();
			$(this).find('.filters-selector').show();

			// Hide filter by elearning and learning plan item types
			$('input[name="item_type"][value="learningplan"]').parent().hide();
			$('input[name="item_type"][value="elearning"]').parent().hide();

			// Make all visible filter field not readonly !!!
			$('#mycourses-filter-form-'+id+' input:visible, #mycourses-filter-form-'+id+' select:visible, #mycourses-filter-form-'+id+' textarea:visible').each(function(key, value) {
				$(this).removeAttr('disabled');
			});
			elem.text(Yii.t('billing', 'Apply'));
		}
		else {
			/*$(this).find('.course-list').show();*/
			$(this).find('.dashlet-pager').show();
			$(this).find('.filters-selector').hide();
			form.submit();
			elem.text(Yii.t('dashboard', 'Filters'));
		}
	}


	/**
	 * "View more"  button clicked to show more courses in a catalog/label
	 * this = dashlet
	 */
	var onViewMoreClicked = function (e) {
		var $elem = $(e.currentTarget).closest('.view-more-box');
		var $form = $elem.closest('form');
		var catalogId = $elem.data('catalog-id');
		var labelId = $elem.data('label-id');
		if (catalogId && typeof catalogId !== "undefined") {
			var $catalog = $form.find('select[name="catalog"]');
			if ($catalog.length <= 0) { $catalog = $form.find('input[name="catalog"]'); }
			if ($catalog.length > 0) { $catalog.val(catalogId); }
			console.log('catalogId: ' + catalogId);
		}
		else if (labelId && typeof labelId != 'undefined') {
			$form.find('select[name="label_id"]').val(labelId);
			console.log('labelId: ' + labelId);
		}

		if (catalogId || labelId)
			$form.submit();
	}


	/**
	 * "Back to catalogs" button clicked to display all catalogs
	 * this = dashlet
	 */
	var onBackToCatalogsClicked = function (e) {
		var $elem = $(e.currentTarget);
		var $form = $elem.closest('form');
		$form.find('[name="catalog"]').val('');
		$form.submit();
	}


    /**
     * "Back to labels" button clicked to display all labels
     * this = dashlet
     */
    var onBackToLabelsClicked = function(e) {
        var $elem = $(e.currentTarget);
        var $form = $elem.closest('form');
        $form.find('select[name="label_id"]').val('');
        $form.submit();
    }

	/**
	 * CATEGORIES button clicked, to show categories selector
	 * this = dashlet
	 */
    var onCategoriesButtonClicked = function(e) {
    	var elem = $(e.currentTarget);

        // Check if categories selector goes in the sidebar
        // If this is the case, do not show categories selector in overlay
        if(elem.hasClass('selected'))
            return;

		if ($(this).find('.course-list').is(':visible') == true) {
			$(this).find('.course-list').hide();
			$(this).find('.categories-selector').show();
		}
		else {
            $(this).find('.categories-selector').hide();
            $(this).find('.course-list').show();
        }
    }


	/**
	 * DISPLAY STYLE STWICH clicked (list or thumb)
	 * this = dashlet
	 */
	var onDisplayStyleSwitchClicked = function (e) {
		var elem = $(e.currentTarget);
		var id = $(this).data('dashlet-id');
		var currentAction = getUrlRoute(true);
		currentAction = currentAction.replace('/', '_');
		var cookieName = 'dashlet_display_style_' + id;// + '_' + currentAction;

		if($('form#login_confirm').length > 0) {
			cookieName += '_public';
		}

		$('#mycourses-filter-form-'+id+' .display-style-switch').each(function() {
			$(this).removeClass("dark");
		});

		$(elem).addClass("dark");

		var tmp = $(this).find('.course-list');
		var tmpSideMenu = $(this).find('.catalog-left-sidebar');
		var tmpCalendarContent = $(this).find('.catalog-calendar-content');
		var tmpDashletPager = $(this).find('.dashlet-pager');
		$(this).find('.filters-selector').hide();
		$(this).find('.filter-header .filters-button').text(Yii.t('dashboard', 'Filters'));

		var selectedGridTypeClass = $(elem).find('.fa').attr('class');
		var selectedGridType = 'thumb';

		if (selectedGridTypeClass.indexOf("fa-th") >= 0) {
			selectedGridType = 'thumb';
			$.cookie(cookieName, 'thumb', {path: '/'});

			tmpCalendarContent.hide();
			tmp.show();
			tmpSideMenu.show();
			tmpDashletPager.show();

			tmp.addClass('tb');
			elem.closest('.dashlet').find('#social_rating_course.fixed_rating').removeClass('list').addClass('thumb');
			$(elem).find('.fa.view-switch').removeClass('fa-th').addClass('fa-list');
		} else if (selectedGridTypeClass.indexOf("fa-list") >= 0) {
			selectedGridType = 'list';
			$.cookie(cookieName, 'list', {path: '/'});

			tmpCalendarContent.hide();
			tmp.show();
			tmpSideMenu.show();
			tmpDashletPager.show();

			tmp.removeClass('tb');
			elem.closest('.dashlet').find('#social_rating_course.fixed_rating').removeClass('thumb').addClass('list');
			$(elem).find('.fa.view-switch').removeClass('fa-list').addClass('fa-th');
		} else 	if (selectedGridTypeClass.indexOf("fa-calendar") >= 0) {
			selectedGridType = 'calendar';
			$.cookie(cookieName, 'calendar', {path: '/'});

			tmp.hide();
			tmpSideMenu.hide();
			tmpDashletPager.hide();
			tmpCalendarContent.show();

			var cellWidth = $(this).parent().parent().data('cell-width');
			var windowWidth =   $(window).width(); // New width

			if(windowWidth > 800 && (cellWidth == 8 || cellWidth ==12)) {
				$('#mini-calendar' + '-' + id).hide();

				$('.calendar-title-container', $(this)).show();
				if(cellWidth == 12) {
					$('.view-switch-container', $(this)).show();
				} else {
					$('.view-switch-container', $(this)).hide();
				}

				$('#calendar' + '-' + id).show();
			} else {
				$('#calendar' + '-' + id).hide();
				$('.calendar-title-container', $(this)).hide();
				$('.view-switch-container', $(this)).hide();
				$('#mini-calendar' + '-' + id).show();
			}

			// Load calendar
			if($('#calendar' + '-' + id).is(':visible') == true) {
				$('#calendar' + '-' + id).fullCalendar('gotoDate', (new Date()));
			}

			if($('#mini-calendar' + '-' + id).is(':visible') == true) {
				$('#mini-calendar' + '-' + id).fullCalendar('gotoDate', (new Date()));
			}
		}
	}


	/**
	 * DASHLET FILTER FORM  submitted
	 * this = dashlet
	 */
    var onFormSubmit = function(e) {
    	var form = $(e.currentTarget);
		e.preventDefault();

		var id = $(this).data('dashlet-id');

		// If semaphor is raised (true), do NOT reset current page
		if (!semaphor) {
			currentPage[id] = 0;
			form.find('input[name="pnumber"]').val(0);
		}
		semaphor = false;


		var params = $.deparam(form.serialize());

		submitForm(params, id);

		return false;

    };


	/**
	 * SEARCH BUTTON clicked in the dashlet
	 * this = dashlet
	 *
	 * Hide Filters and Display style switch, show Search INPUT field
	 */
    var onSearchButtonClicked = function(e) {
    	var elem = $(e.currentTarget);
    	var form = elem.closest('form');

		var filterHeader = $(this).find('.filter-header');
		if (filterHeader.hasClass('search-activated')) {
			form.submit();
		}
		else {
			filterHeader.addClass('search-activated').find('.search-field').show("slide", { direction: "right" }, 500, function() {
				$(this).find('input[name="search_input"]').focus()
			});
		}
    };


	/**
	 * Catch keydown events in general text search field
	 * this = dashlet
	 */
	var onSearchInputKeydown = function(e) {
		var elem = $(e.currentTarget);
		var code = e.keyCode || e.which;
		if (code == 27) {
    		$(this).find('.search-field').hide();
    		$(this).find('.filter-header').removeClass('search-activated')
		} else if (code === 13) {
			e.preventDefault();
			var form = elem.closest('form');
			form.submit();
		}
		return true;
	};


    /**
     * CLEAR SEARCH small button clicked
	 * this = dashlet
     */
    var onClearSearchClicked = function(e) {
    	var elem = $(e.currentTarget);
    	var form = elem.closest('form');
    	var searchInput = form.find('input[name="search_input"]');

    	if (searchInput.val() !== '') {
    		searchInput.val('');
    		form.submit();
    	}
    	else {
    		$(this).find('.search-field').hide();
    		$(this).find('.filter-header').removeClass('search-activated')
    	}
    }

    /**
     * When a course category is changed in Category Filtering panel
     */
    var onCourseCategoryChange = function(e) {
    	var elem = $(e.currentTarget);
    	var form = elem.closest('form');
    	form.submit();
    }


	/**
	 * NEXT / PREV page clicked
	 *
	 * this = dashlet
	 */
	var onNextPageClicked = function (e) {
		var elem = $(e.currentTarget);
		var id = $(this).data('dashlet-id');
		var form = elem.closest('form');
		currentPage[id]++;
		semaphor = true;
		form.find('input[name="pnumber"]').val(currentPage[id]);
		form.submit();
	}

	var onPrevPageClicked = function (e) {
		var elem = $(e.currentTarget);
		var id = $(this).data('dashlet-id');
		var form = elem.closest('form');
		currentPage[id]--;
		if (currentPage[id] < 0)
			currentPage[id] = 0;
		semaphor = true;
		form.find('input[name="pnumber"]').val(currentPage[id]);
		form.submit();
	}


	/**
	 * Callable methods in form of
	 * $('.dashlet').mycourses('<method-name>', <arguments>).
	 *
	 */
	var methods = {

			/**
			 * init() is a special methods and is called if no method is specified, e.g. $('.dashlet').mycourses({}) will call init().
			 * It must be called always first!!!!!! Without calling it first, the whole plugin won't work!!!!
			 */
			init: function(options) {

				settings = $.extend({}, settings, options);

				if (typeof settings.debug !== "undefined")
					debug = settings.debug;

				// Enumerate all jQuery objects matching the calling selector
				// Check if it is a MyCourses type of dashlet, add listeners etc.
				var returnValue = this.each(function(){
					var dashlet = $(this);
					if ($(this).find('.w-mycourses').length > 0) {

						methods.propagateCellWidth(dashlet);

						addListeneres.call(this);

						// Keep a uniqie list of loaded mycourses dashlets, we may need them later
						var id = $(this).data('dashlet-id');
						if (jQuery.inArray(id, mycoursesDashlets) === -1) {
							mycoursesDashlets.push(id);
							currentPage[id] = 0;
						}

						$(this).find('input[type="radio"]:not(.star-rating-applied)').styler();

					}
				});
				return returnValue;
			},


			/**
			 * Dashlet inherits presentation width from cell. Propagate it!
			 */
			propagateCellWidth: function(dashlet) {
				var cell = $(dashlet).closest('.dashlets-cell');
				var textWidth = $(cell).data('cell-text-width');  // half, full, ...
				$(cell).find('.w-mycourses').addClass(textWidth);
				$(cell).find('.course-list').addClass(textWidth);

				// A cells/dashlets of width "onethird" are FORCED to non-thumbnail!!!!
				if (textWidth === 'onethird') {
					$(cell).find('.course-list').removeClass('tb');
				}
			},

            /**
             * Rendered for category tree nodes
             * @param event
             * @param data
             */
            renderCategoryNode: function(event, data) {
                var node = data.node;
                var $span = $(node.span);

                var $title = $span.find('.fancytree-title');
                if ($title) {
                    var $folder = $title.find('.is-folder');
                    if ($folder.length <= 0) {
                        $title.prepend($('<span class="p-sprite is-folder"></span>'));
                        $folder = $title.find('.is-folder');
                    }
                    if (node.expanded) {
                        $folder.removeClass('closed');
                        $folder.addClass('opened');
                    } else {
                        $folder.removeClass('opened');
                        $folder.addClass('closed');
                    }

                    //var num_courses = node.data.courses + countDescendantsCourses(node);
                    /*var num_courses = node.data.courses_d;
                    var $count = $title.find('.node-courses-count');
                    if ($count.length <= 0) {
                        $title.append($('<span class="node-courses-count"></span>'));
                        $count = $title.find('.node-courses-count');
                    }
                    $count.html('&nbsp;(' + num_courses + ')');*/
                }
            },

            /**
             * Category tree handler that reloads the list of courses when selecting a category
             * @param event
             * @param data
             */
            categoryTreeFocusEvent: function(event, data, hiddenFieldId) {
                event.stopPropagation();
                if(initializingTree) {
                    initializingTree = false;
                    return;
                }

                // Set hidden category id field to the current selected tree node
                var dashletId = $(this)[0];
                var $hiddenCategoryField = $('#'+hiddenFieldId);
                var selectedCategoryId = !data.node.parent.parent ? 'all' : data.node.key;
                $hiddenCategoryField.val(selectedCategoryId);

                // Render the form
                var form = $("#"+dashletId).find('form');
                form.submit();
            },

            /**
             * Select event handler for category tree
             * @param event
             * @param data
             * @returns {boolean}
             */
            categoryTreeSelectEvent: function(event, data) {
                event.stopPropagation();
                return false;
            },

            /**
             * Expands/activates the currently selected node
             * @param event
             * @param data
             * @returns {boolean}
             */
            initCategoryTree: function(event, data, categoryId) {
                event.stopPropagation();
                if(categoryId != 'all') {
                    initializingTree = true;
                    data.tree.activateKey(categoryId);
                }
                return false;
            }

	}


	/**
	 * Plug it in
	 */
	$.fn.mycourses = function(methodOrOptions) {
        if ( methods[methodOrOptions] ) {
            return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
            // Default to "init"
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  methodOrOptions + ' does not exist on jQuery.mycourses' );
        }
    };

})(jQuery);

$(window).resize(function() {
	// This will execute whenever the window is resized
	// Display appropriate Courses Catalog dashlet according to its size and cell width
	$('li[class="dashlet mydashboard.widgets.DashletCatalog"]').each(function() {
		var cellWidth = $(this).parent().parent().data('cell-width');
		var windowWidth =   $(window).width();
		var id = $(this).data('dashlet-id');

		var cellWidth = $("#dashlet-" + id).parent().parent().data('cell-width');
		var windowWidth =   $(window).width();
		var cookieName = "dashlet_display_style_" + id;

		if($('form#login_confirm').length > 0) {
			cookieName += '_public';
		}


		if($('.display-style-switch.dark .fa', $(this)).length > 0 || ($.cookie(cookieName) === 'calendar')) {
			var selectedGridTypeClass = $('.display-style-switch.dark .fa', $(this)).attr('class');

			if($.cookie(cookieName) === 'calendar') {
				selectedGridTypeClass = 'fa fa-calendar';
			}

			if (selectedGridTypeClass.indexOf("fa-calendar") >= 0) {
				if(windowWidth > 800 && (cellWidth == 8 || cellWidth ==12)) {
					$('#mini-calendar' + '-' + id).hide();

					$('.calendar-title-container', $(this)).show();
					if(cellWidth == 12) {
						$('.view-switch-container', $(this)).show();
					} else {
						$('.view-switch-container', $(this)).hide();
					}

					$('#calendar' + '-' + id).show();
				} else {
					$('#calendar' + '-' + id).hide();
					$('.calendar-title-container', $(this)).hide();
					$('.view-switch-container', $(this)).hide();
					$('#mini-calendar' + '-' + id).show();
				}

				if(windowWidth < 1200) {
					$('.view-switch-container', $(this)).hide();
				}

				// Load calendar
				if($('#calendar' + '-' + id).is(':visible') == true) {
					$('#calendar' + '-' + id).fullCalendar('gotoDate', (new Date()));
				}

				if($('#mini-calendar' + '-' + id).is(':visible') == true) {
					$('#mini-calendar' + '-' + id).fullCalendar('gotoDate', (new Date()));
				}
			}
		}
	});
});



