/**
 * Load FCBK in Knowledge Library "sortBy" - in Settings view
 */
var knowledgeLibraryLoadFCBKsortBy = function () {
	var selectElem = $('select[name="sortBy"]');
	selectElem.fcbkcomplete({
		json_url: HTTP_HOST + 'index.php?r=mydashboard/DashletWidgets/AxKnowledgeLibrarySortingbyFieldsAutocomplete',
		addontab: false, // <==buggy
		width: '400px',
		cache: false,
		complete_text: '',
		maxshownitems: 5,
		input_min_size: -1,
		input_name: 'maininput-name-2',
		filter_selected: true
	});
};

/**
 * Load FCBK in Knowledge Library "showAssetsTopics" - in Settings view
 */
var knowledgeLibraryLoadFCBKshowAssetsTopics = function () {
	var selectElem = $('select[name="showAssetsTags"]');
	selectElem.fcbkcomplete({
		json_url: HTTP_HOST + 'index.php?r=mydashboard/DashletWidgets/AxKnowledgeLibraryShowTopicsFieldsAutocomplete',
		addontab: false, // <==buggy
		width: '400px',
		cache: false,
		complete_text: '',
		maxshownitems: 5,
		input_min_size: -1,
		input_name: 'maininput-name-1',
		filter_selected: true
	});
};



$(document).on("dialog2.content-update", ".modal.create-dashlet-wizard", function () {
	if ($('section#knowledge-libraty-settings').length > 0) {
		knowledgeLibraryLoadFCBKshowAssetsTopics();
		knowledgeLibraryLoadFCBKsortBy();
	}
});