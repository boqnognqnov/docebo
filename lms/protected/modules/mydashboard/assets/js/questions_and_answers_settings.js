/**
 * Load FCBK in Questions and Answers in Settings view
 */
var questionsAndAnswersSettingsLoadFCBK = function () {
	var selectElem = $('select[name="selected_sorting_fields"]');
	selectElem.fcbkcomplete({
		json_url: HTTP_HOST + 'index.php?r=mydashboard/DashletWidgets/AxQuestionsAndAnswersFieldsAutocomplete',
		addontab: false, // <==buggy
		width: '98.5%',
		cache: false,
		complete_text: '',
		maxshownitems: 5,
		input_min_size: -1,
		input_name: 'maininput-name',
		filter_selected: true
	});
};




$(document).on("dialog2.content-update", ".modal.create-dashlet-wizard", function () {
	if ($('section#questions-and-answers-settings').length > 0) {
		questionsAndAnswersSettingsLoadFCBK();
	}
});

