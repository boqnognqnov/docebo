$(function () {
	$(document).on('mydashboard.dashlet.content-loaded','.dashlet', function(e, data){
		var containingDashlet = $(this);
		$('.mykpis-block', containingDashlet).each(function(){
			var that = this;
			var ajaxUrl = $(this).data('url');
			$.ajax(ajaxUrl, {
				dataType: "json",
				success: function(data){
					if(data.success){
						$(that).html(data.html);

                        // set the default value
                        var size = 80;

                        // set custom size if passed
                        if($(this).data('size')){
                            size = $(this).data('size');
                        }

						$('input', that).styler();
						$('[rel=tooltip]', that).tooltip()
						$('.auto-knob', that).knob({
							'min':       0,
							'max':       100,
							'readOnly':  true,
							//'displayInput': false,
							'fgColor':   '#6cc267',
							'bgColor':   '#cccccc',
							'thickness': .25,
							'width':     size,
							'height':    size,
							'draw':      function () {
								$(this.i).val(this.cv + '%')
							}
						});
					}else{
						$(that).remove();
					}

					// After each KPI block load, append a horizontal grey
					// separator after each 2nd element
					// (if the current dashlet column is wide, this only applies
					// to KPI dashlets that have a "2 column" variation
					if(!$(containingDashlet).closest('.dashlets-cell').hasClass('span4') && !$(containingDashlet).closest('.dashlets-cell').hasClass('span6')) {
						$('.block-separator', containingDashlet).remove();
						var separate = $('<div class="block-separator clearfix"></div>');
						$('.mykpis-block:even', containingDashlet).before(separate);

						// The above before() will place an unnecessary top border/separator
						// and removing it singularly is easier than creating a complex
						// formular for odd/even
						$('.block-separator', containingDashlet).first().remove();
					}
				}
			});
		});
	});

});