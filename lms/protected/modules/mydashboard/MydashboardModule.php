<?php

class MydashboardModule extends CWebModule
{
	
	private $_assetsUrl;
	
	/**
	 * List of Core Dashlet widget classes
	 * @var unknown
	 */
	static $coreDashletClasses = array(
			//'DashletExample',
			'DashletIframe',
			//'DashletWelcome',
			'DashletSocial',
			'DashletUserprofile',
			'DashletCalendar',
			'DashletMyKPIs',
			'DashletMyCourses',
			'DashletAdminKPIs',
			'DashletHTML',
			'DashletCatalog'
	);
	
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'mydashboard.models.*',
			'mydashboard.components.*',
			'mydashboard.widgets.*',				
		));
		
		// Listen for event for collecting dashlet widget descriptors
		Yii::app()->event->on('CollectMyDashboardDashletWidgets', array($this, 'onCollectMyDashboardDashletWidgets'));
		
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			$this->registerResources();
			
			return true;
		}
		else
			return false;
	}
	

	
	public function registerResources() {
		if (!Yii::app()->request->isAjaxRequest) {
			JsTrans::addCategories('mydashboard');
			$cs = Yii::app()->getClientScript();
			$cs->registerCoreScript('jquery.ui');
			$cs->registerCssFile	($cs->getCoreScriptUrl() . '/jui/css/base/jquery-ui.css');
			$cs->registerScriptFile ($this->getAssetsUrl()   . '/js/mydashboard.js');
		}
	}
	
	
	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('mydashboard.assets'));
		}
		return $this->_assetsUrl;
	}
	
	/**
	 * Listen and response to CollectMyDashboardDashletWidgets event, returning list of core dashlet descriptors
	 * 
	 * @param DEvent $event
	 */
	public function onCollectMyDashboardDashletWidgets(DEvent $event) {
		if (!isset($event->params['descriptors'])) {
			return;	
		}
		foreach (self::$coreDashletClasses as $class) {
			$descriptorObj = $class::descriptor();
			if ($descriptorObj instanceof DashletDescriptor) {
				$event->params['descriptors'][] = $descriptorObj;
			}
		}
	}
	
}
