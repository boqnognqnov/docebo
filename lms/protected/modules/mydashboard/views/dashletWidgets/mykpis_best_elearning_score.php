<style type="text/css">
	.mykpis-course-name-hover{
		background: url('<?=Yii::app()->theme->getAbsoluteBaseUrl()?>/images/icons_elements.png') no-repeat -716px -42px;
	}
</style>

<div class="pull-left mykpis-icon-wrapper">
	<span class="i-sprite is-cup orange large"></span>
</div>

<span class="score-holder pull-left"><span class="green"><?=$courseWithBestScore['score']?></span>/100</span>
<span class="mykpis-course-name-hover" rel="tooltip" title="<?=$courseWithBestScore['name']?>"></span>
<p><?=Yii::t('dashlets', '<b>Best score</b> on <b>E-Learning</b>')?></p>
<div class="clearfix"></div>