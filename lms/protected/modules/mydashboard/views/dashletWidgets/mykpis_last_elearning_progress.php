<? if($percent >= 0): // zero is a valid percent ?>
<div class="pull-left knob-holder">
	<input type="text" class="auto-knob" data-size="60" value="<?= number_format($percent,0) ?>" />
</div>
<? endif; ?>
<a href="<?=Docebo::createLmsUrl('/player', array('course_id'=>$idCourse))?>">
<p class="block-head spaced"><b><?= $name ?></b></p>
</a>
<p><?= Yii::t( 'dashlets', 'Last <b>E-learning</b> course progress' ) ?></p>
<div class="clearfix"></div>