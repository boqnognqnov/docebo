<img class="close" src="<?= Yii::app()->theme->baseUrl; ?>/images/icon-close-black-medium.png"/>
<? foreach($items as $item): ?>
	<p><?
		$ex = explode('_', $item['id']);
		$id_course = $ex[0];
		$id_session = $ex[1];
		$day = $ex[2];
		$id_videoconference = $ex[3];

		$url = Docebo::createLmsUrl('player', array('course_id'=>$id_course));
		if (isset($item['type']) && $item['type'] == "webinar")
			$url = Docebo::createLmsUrl('player/training/webinarSession', array('course_id'=>$id_course,'session_id'=>$id_session));
		switch($item['type']){
		case 'elearning':
			echo Yii::t('course', '_COURSE_TYPE_ELEARNING');
			break;
		case 'classroom':
			echo Yii::t('standard', '_CLASSROOM');
			break;
		case 'webinar':
			echo Yii::t('webinar', 'Webinar');
			break;
		case 'conference':
			echo Yii::t('standard', '_VIDEOCONFERENCE');
			break;
		default:
			echo 'Unknown type';
	}?></p>
	<p><u><b>
		<a href="<?=$url?>"><?=$item['name']?></a>
	</b></u></p>
<? endforeach; ?>
<div class="triangle">
	<img src="<?= Yii::app()->getModule('mycalendar')->getAssetsUrl(); ?>/images/triangle.png" />
</div>