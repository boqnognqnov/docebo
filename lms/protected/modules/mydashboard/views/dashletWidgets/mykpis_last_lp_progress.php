<? if($percent >= 0): // zero is a valid percent ?>
	<div class="pull-left knob-holder">
		<input type="text" class="auto-knob" data-size="60" value="<?= number_format($percent) ?>" />
	</div>
<? endif; ?>

<a href="<?=Docebo::createLmsUrl('curricula/show', array(
    'id_path' => $id_path
))?>">
	<p class="block-head spaced"><b><?=$lpName?></b></p>
</a>
<p><?=Yii::t('dashlets', 'Last <b>Learning Plan</b> progress')?></p>
<div class="clearfix"></div>