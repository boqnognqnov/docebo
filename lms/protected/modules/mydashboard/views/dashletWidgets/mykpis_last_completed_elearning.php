<div class="pull-left mykpis-icon-wrapper">
	<span class="i-sprite is-flag2 large"></span>
</div>

<a href="<?=Docebo::createLmsUrl('/player', array('course_id'=>$idCourse))?>">
<p class="block-head"><b><?=$courseName?></b></p>
</a>
<p><?=Yii::t('dashlets', 'Last completed <b>E-Learning</b> course')?></p>
<div class="clearfix"></div>