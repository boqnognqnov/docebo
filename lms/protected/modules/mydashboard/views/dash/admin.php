<?php
	$this->breadcrumbs = array(
		Yii::t('menu', 'Dashboard layout'),
	);
	
?>

<h3 class="title-bold back-button">
	<span><?= Yii::t('menu', 'Dashboard layout') ?></span>
</h3>
<br>


<?php
	$this->renderPartial('_adminMainActions', array(
		'editDialog' => $editDialog,
	));
?>


<div class="clearfix">
	<?php $this->widget('common.widgets.ComboGridView', $gridParams); ?>
</div>


<script type="text/javascript">
/*<![CDATA[*/

	$(function(){

        $('body').dashboard({
        	dashControllerUrl	: <?= json_encode(Docebo::createLmsUrl('//mydashboard/dash')) ?>,            
            adminLayoutsMode: true
        });      
		
	});           
	
           
	
/*]]>*/
</script>

