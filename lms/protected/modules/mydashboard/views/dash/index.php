<div class="mydashboard <?php echo Yii::app()->getLanguage(); ?>mydash" id="mydashboard" data-layout-id="<?= $layoutModel->id ?>">

	<?php if ($adminDashletsMode) : ?>
		<h3 class="title-bold back-button">
			<?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('//mydashboard/dash/admin')); ?>
			<span><?= Yii::t('dashboard', 'Layout widgets') . ": " . $layoutModel->name ?></span>
		</h3>
		<br>

		<?php
			$this->renderPartial('_adminDashletsMainActions', array(
				'layoutModel' => $layoutModel,
			));
		?>
	<?php endif; ?>

	<?php $msg = Yii::app()->user->getFlash('email_status_change');
	if(!empty($msg)) {
		if ($msg == Yii::t('standard', 'You have confirmed your email!')) $class = 'success';
		elseif ($msg == Yii::t('standard', 'Invalid or expired email verification link!')) $class = 'danger';
		else $class = 'info';
		?>
		<div class="alert alert-<?=$class?>" style="text-align: center">
			<?=$msg;?>
		</div>
	<?php } ?>

	<?=Yii::app()->user->getFlash('email_verification_reminder');?>
	<div id="mydashboard-dashlets" class="mydashboard-dashlets">
		<?php foreach ($layoutModel->rows as $rowModel) : ?>
			<?php
				$this->renderPartial('_row_tpl', array(
					'model' => $rowModel,
					'adminDashletsMode' => $adminDashletsMode,
				));
			?>
		<?php endforeach;?>
	</div>
</div>


<script type="text/javascript">
	/*<![CDATA[*/
	$(function () {

		<?php if ($adminDashletsMode) : ?>
		// Create Wizard object (themes/spt/js/jwizard.js)
		var jwizard = new jwizardClass({
			dialogId: 'create-dashlet-wizard',
			dispatcherUrl: '<?= Docebo::createLmsUrl('//mydashboard/dash/dashletWizard') ?>',
			alwaysPostGlobal: true,
			debug: false
		});
		<?php endif; ?>

		if ($.fn.dashboard) {
			$('#mydashboard').dashboard({
				dashControllerUrl: <?= json_encode(Docebo::createLmsUrl('//mydashboard/dash')) ?>,
				adminDashletsMode: <?= json_encode($adminDashletsMode) ?>,
				layoutId: <?= json_encode($layoutModel->id) ?>,
				autoLoadContent: true,
				debug: true
			});
		}

		// JS hack to change css property - no better way found for this at the moment
		$(document).ajaxComplete(function(){
			var locationStr = $(location).attr('search');
		})
	});
	/*]]>*/
</script>
