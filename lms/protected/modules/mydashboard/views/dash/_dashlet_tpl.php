<?php 
	/* @var $model DashboardDashlet */

	// Normalize Height
	$height = DashboardDashlet::normalizeCssHeight($model['height']);
    /***** Get the dashlet type/handler/ and add it as a class to the li.dashlet elements *****/
    $handlerClass = $model['handler'];
	$headerText = DashletWidget::getCurrentLangTitle($model['id']);
?>

<li class="dashlet <?=$handlerClass?>" data-dashlet-id="<?=(int) $model['id']?>" id="dashlet-<?= (int) $model['id'] ?>">

	<? if($adminDashletsMode || $model['header'] || $headerText): ?>
	<div class="row-fluid">
		<div class="span12 dl-header">
			<?php if ($adminDashletsMode) : ?>
				<div class="span8 dl-header-text"><?= DashletWidget::getCurrentLangTitle($model['id']);/* Yii::t('dashboard', $model['header']) */ ?></div>
				<div class="span4 text-right dl-handles"><!-- AJAX --></div>
			<?php else: ?>
				<div class="span12 dl-header-text"><?= DashletWidget::getCurrentLangTitle($model['id']);/* Yii::t('dashboard', $model['header']) */ ?></div>
			<?php endif; ?>
		</div>
	</div>
	<? endif; ?>
		

	<div class="row-fluid">
		<div class="dl-content" style="height: <?= $height ?>;"></div>
	</div>

	<div class="row-fluid">
		<div class="span12 dl-footer"></div>
	</div>

</li>
