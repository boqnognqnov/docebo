<?php 

	$list = array(DashboardAssign::FILTER_ALL => Yii::t('standard', "_ALL"));
	$list = array_merge($list, CoreGroup::getAdminLevels(true, false, true, true));
	echo CHtml::dropDownList('layout_level_select', '', $list, array(
		'style' => 'display: none'	
	));

	$currentLevel = $data->getLevelFilter();
	$currentLevelDisplay 	= $currentLevel ? $list[$currentLevel] : Yii::t('dashboard', 'Select level');

?>

<div class="layout-level-column" data-layout-id="<?= $data->id ?>">
	<a href="javascript: void(0);" class="current-level-value-link"><?= $currentLevelDisplay ?></a>
	
	<?php
		echo CHtml::dropDownList('', $currentLevel ? $currentLevel : 9999, $list, array(
			'class'		=> 'layout-level-select',	
			'style' 	=> 'display: none',
		));
	?>	
	
</div>
