<?php
	/* @var $this DashController */
	/* @var $data DashboardLayout */

	if ($data->isForPowerUsers()) {
		$infoText = '';
		
		if ($data->puprofiles_filter_option == DashboardLayout::LAYOUT_OPTION_ALL) {
			$infoText = Yii::t('standard', 'All profiles');
		}
		else {
			$count = count($data->getAssignedPuProfiles());
			$infoText = Yii::t('standard', '{count} profile(s)', array('{count}' => $count));
			
			if ($count <= 0) {
				$warn = '<a href="javascript: void(0);" rel="tooltip" title="'. Yii::t('dashboard', 'No power user can see this layout') .'"><i class="i-sprite is-solid-exclam orange"></i></a> ';
			}
				
		}
		
		
		$url = Docebo::createLmsUrl('usersSelector/axOpen', array(
				'type' 	=> UsersSelector::TYPE_DASHBOARD_PU_PROFULES,
				'name'	=> 'What name',
				'idGeneral' => $data->id,
				'noyiixviewjs' => true,
				'tp'=> 'mydashboard.views.dash._puprofiels_filter_top',  // top panel partial! view
		));
		
		echo $warn;
		echo  CHtml::link($infoText, $url, array(
				'class' => 'open-dialog underline',
				'data-html' => true,
				'data-dialog-class' => "dashboard-layout-puprofiles-selector",
				'data-dialog-id' => "dashboard-layout-puprofiles-selector",
				'data-dialog-title' => Yii::t('menu', '_PUBLIC_ADMIN_RULES'),
		));
		
		
	}
	else {
		echo "N/A";
	}
	
	
?>

