<?php
/* @var $this UsersSelectorController */
/* @var $model CoreNotification */
?>
<?php
	$model = DashboardLayout::model()->findByPk($this->idGeneral);
?>

<div class="selector-top-panel">

	<br>
	<div class="row-fluid">
		<div class="span12">
			<?= Yii::t('dashboard','Apply this layout to all Users/Groups/Branches or make a custom selection') ?>
		</div>
	</div>

	<?php if (Yii::app()->user->hasFlash('error')) : ?>
		<div class="row-fluid">
			<div class="span12">
	   			<div class='alert alert-error alert-compact text-left'>
    				<?= Yii::app()->user->getFlash('error'); ?>
    			</div>
    		</div>
    	</div>
		<?php endif; ?>
	<div class="row-fluid">
	
	
	<div class="row-fluid">
	
		<div class="grey-wrapper">
	
			<div class="row-fluid">
				<div class="span12">
					
					<div class="row-fluid">
					
						<div class="span4">
							<label class="radio pull-left">
								<?php
									echo CHtml::radioButton('ufilter_option', $model->ufilter_option == DashboardLayout::LAYOUT_OPTION_ALL, array('value' => DashboardLayout::LAYOUT_OPTION_ALL)); 
									echo Yii::t('standard', 'All users, groups and branches');
								?>
							</label>
						</div>
						<div class="span4">
							<label class="radio pull-left">
								<?php
									echo CHtml::radioButton('ufilter_option', $model->ufilter_option == DashboardLayout::LAYOUT_OPTION_SELECTED, array('value' => DashboardLayout::LAYOUT_OPTION_SELECTED)); 
									echo Yii::t('dashboard', 'Select users, groups and branches');
								?>
							</label>
						</div>
					</div>
					
					
					
				</div>
			</div>
		
		</div>
	
	</div>


</div>

<script type="text/javascript">

	$(function(){
		$('#mydashboard').dashboard('selectorTopPanelReady');
	});
	
</script>