<?php
	/* @var $descriptor DashletDescriptor */
	/* @var $dashletModel DashboardDashlet */
	$sampleImageUrl = $descriptor->sampleImageUrl;

	$currentLang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
	$defaulLang = CoreSetting::get('default_language');

	if (!$sampleImageUrl) {
		$sampleImageUrl = Yii::app()->mydashboard->getModule()->getAssetsUrl() . "/images/dashlet_sample.png"; 
	}


	if ($dashletModel) {
//		$title = $dashletModel->header;
		$height = $dashletModel->height;
	}

	// Get array with headers(titles) per different languages
	$titleArr = DashletWidget::getTitles();
	$title = $titleArr[$currentLang];

	$h1Title = DashletWidget::getCurrentLangTitle();
?>
<h1><?=  ($wizardMode) ? $descriptor->title : $h1Title; /* $descriptor->title */?></h1>

<div class="form">

	<?php 
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'dashlet-settings-form',
			'htmlOptions' => array(
				'class' => 'ajax jwizard-form',
				'name' => 'dashlet-settings-form'
			),
		));
		
		// Wizard related
		echo CHtml::hiddenField('from_step', 'step_settings');
        echo CHtml::hiddenField('cellId', $cellId);

		// Generate all hidden temp header inputs
		if(is_array($titleArr) && count($titleArr) > 0) {
			foreach($titleArr as $key => $row) {
				echo CHtml::hiddenField('headers[' . $key . ']', $row, array('id' => 'header-' . $key));
			}
		}

		// Encode the array to JSON in order to be used in the JS area
		$titleArr = CJSON::encode($titleArr);
	?>


	<div class="dashlet-settings-html">
		<div class="row-fluid">
		
			<div class="span6">
				<div class="row-fluid dashlet-description">
					<div class="span12">
						<?= $descriptor->description?>
					</div>
				</div>
				<div class="row-fluid dashlet-sample-image">
					<div class="span12">
						<?= CHtml::image($sampleImageUrl) ?>
					</div>
				</div>
			</div>
			
			<div class="span6">
			
				<div class="row-fluid">
					<div class="languages">
						<p><?= Yii::t('gamification', 'Assign details for each language'); ?></p>
					</div>
					<div class="lang-wrapper">
						<?= CHtml::dropDownList('lang_code', Lang::getCodeByBrowserCode(Yii::app()->getLanguage()), $langs); ?>
						<div class="stats">
							<p class="available"><?= Yii::t('admin_lang', '_LANG_ALL'); ?>: <span><?= count($langs); ?></span></p>
							<p class="assigned"><?= Yii::t('standard', 'Filled languages'); ?>: <span>0</span></p>
						</div>
					</div>
				</div>
				<div class="row-fluid">
					<div class="control-group">
        				<label class="control-label"><?= Yii::t('standard', '_TITLE') ?></label>
        				<div class="controls">
            				<div class="row-fluid">
                				<?= CHtml::textField('dashlet_instance_title', $title, array('class' => 'span12')) ?>
                				<label class="setting-description compact"><?= Yii::t('dashboard', 'Leave blank to hide the title') ?></label>
            				</div>
        				</div>
    				</div>
				</div>
			
				<?php if ($descriptor->hasHeight) : ?>
				<div class="row-fluid">
					<div class="control-group">
        				<label class="control-label"><?= Yii::t('dashboard', 'Widget height') ?></label>
        				<div class="controls">
            				<div class="row-fluid">
                				<?= CHtml::textField('dashlet_instance_height', $height, array('class' => 'span3')) ?> px
                				<label class="setting-description compact"><?= Yii::t('dashboard', 'Leave blank to use default height') ?></label>
            				</div>
        				</div>
    				</div>
				</div>
				<br/>
				<?php endif; ?>

				<div class="row-fluid">
					<div class="span12">
						<div class="dashlet-specific-settings">
							<?php
								// Ask the dashlet widget to render its settings view
								$this->widget($descriptor->handler, array(
									'dashletModel'		=> $dashletModel,  	
									'settings'			=> true,	
								));
							?>
						</div>
					</div>
				</div>
				
			
			</div>
		</div>
	</div>
	
	
	<!-- For Dialog2 - they go in footer -->
	<div class="form-actions jwizard">
		<?php
			if ($wizardMode) {
	   			echo CHtml::submitButton(Yii::t('standard', '_PREV'), array(
	       			'class'  => 'btn btn-docebo green big jwizard-nav',
	       			'name'	=> 'prev_button', 
	   			));
	   			
	   			echo CHtml::submitButton(Yii::t('standard', '_CONFIRM'), array(
	   				'name'  => 'finish_wizard', 
	   				'class' => 'btn btn-docebo green big jwizard-nav jwizard-finish', 
				)); 
	   			
			} 
			else {
				echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
					'class' => 'btn btn-submit notification-submit',
					'name'	=> 'save_changes_button'	
				));
			}
			?>
			<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
	</div>
	
	
	<?php $this->endWidget(); ?>
	
</div>

<script type="text/javascript">
	var showPopulatedLangs = function() {}

	var getAssignedLangsCount = function() {}

	var titleArr = [];

	$(function(){

		// Array with titles for each languages
		titleArr = <?= $titleArr; ?>;

		/**
		 * Show languages, that are populated
		 */
		showPopulatedLangs = function () {
			$("#lang_code > option").each(function() {
				var optionText = this.text;
				this.text = optionText.replace('* ', '');

				if(titleArr[this.value] != '') {
					this.text = '* ' + this.text;
				}
			});
		}

		// Current language
		var currentLang = $('#lang_code').val();

		// Set title according to the currently selected language
		$('#dashlet_instance_title').val(titleArr[currentLang]);

		$('input').not('.star-rating-applied').styler();
		
		// Sortize ($.sortable) .auto-sortable input.setting's
		$('#mydashboard').dashboard('sortizeAutoSortable', '.dashlet-specific-settings');


		<?php
			// Disable event handling on typing the title
			$disabledForTypes = array('mydashboard.widgets.DashletHTML', 'mydashboard.widgets.DashletIframe');

			if (!(($dashletModel && in_array(strval($dashletModel->handler), $disabledForTypes)) || (!empty($_REQUEST['selected_type']) && in_array(strval($_REQUEST['selected_type']), $disabledForTypes)))) {
		?>

		// On typing the title update the hidden fields and titles array
        // Edit: Previously used event listener on keyup but it wasn't triggering on autocomplete
        $('#dashlet_instance_title').bind('input', function() {
            currentLang = $('#lang_code').val();

            titleArr[currentLang] = $('#dashlet_instance_title').val();
            $('input[name="headers['+currentLang+']"]').val($('#dashlet_instance_title').val());

            var assignedLangsCount = getAssignedLangsCount(titleArr);
            $('div.dashlet-settings-html div.lang-wrapper p.assigned span').html(assignedLangsCount);
        });

		<?php
			}
		?>

		// On language change, set proper content in the title
		$('#lang_code').on('change', function() {
			titleArr[currentLang] = $('#dashlet_instance_title').val();

			currentLang = $('#lang_code').val();
			$('#dashlet_instance_title').val(titleArr[currentLang]);
		});

		getAssignedLangsCount = function(titleArr) {
			var count = 0;
			$.each(titleArr, function(key, value) {
				if(value != '') {
					count++;
				}
			});

			// Mark populated languages with asterisks
			showPopulatedLangs();

			return count;
		}

		var assignedLangsCount = getAssignedLangsCount(titleArr);
		$('div.dashlet-settings-html div.lang-wrapper p.assigned span').html(assignedLangsCount);
	});
</script>