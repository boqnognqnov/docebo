<?php
	$previewUrl =  Docebo::createLmsUrl('site/index', array(
		'preview'	=> 1,
		'id'		=> $layoutModel->id,		
	));
?>
<div class="main-actions clearfix">

	<ul class="clearfix">
		<li>
			<div>
				<a class="new-layout-row-button p-hover" href="javascript:;" alt="<?= Yii::t('dashboard', 'Create new row') ?>">
					<span class="p-sprite two-rows"></span> <?= Yii::t('dashboard', 'New row') ?>
				</a>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>
			<p></p>
		</div>
	</div>
	
	<ul class="clearfix">
		<li>
			<div>
				<a class="p-hover" href="<?= $previewUrl ?>" target="_new" alt="<?= Yii::t('standard', '_PREVIEW') ?>" >
				<span class="p-sprite preview"></span> <?= Yii::t('standard', '_PREVIEW') ?>
				</a>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>
			<p></p>
		</div>
	</div>
	
</div>
