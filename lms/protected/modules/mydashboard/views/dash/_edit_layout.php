<?php 
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'edit-dashboard-layout',
		'htmlOptions' => array(
			'class' => 'ajax',
			'name' => 'edit-dashboard-layout'
		),
	));
?>

	<div class="row-fluid">
		<div class="control-group">
			<label class="control-label"><?= Yii::t('standard', '_NAME') ?></label>
			<div class="controls">
           		<div class="row-fluid">
               		<?= $form->textField($model, 'name', array('class' => 'span12')) ?>
           		</div>
        	</div>
    	</div>
	</div>



	<div class="form-actions">
    	<input class="btn-docebo green big" type="submit" name="confirm_save" value="<?php echo Yii::t('standard', '_SAVE'); ?>" />
    	<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
	</div>


<?php $this->endWidget(); ?>

