<div class="row-fluid dashlets-row" data-row-id="<?= $model->id ?>" id="row-<?= $model->id ?>">
	<?php if ($adminDashletsMode) : ?>
		<div class="row-fluid">
			<div class="span12 row-admin-header">
				<div class="span8 row-admin-layout-selector">
					<?php
						$this->renderPartial('_row_admin_layout_selector', array(
							'rowLayout' => $model->detectRowLayout()
						)); 
					?>
				</div>
				<div class="span4 row-admin-handles text-right">
					<?php
						$this->renderPartial('_row_admin_handles', array(
						)); 
					?>
				</div>
			</div>	
		</div>
	<?php endif; ?>

	<div class="row-fluid">
		<div class="span12 dashlets-row-inner-wrapper">
			<?php foreach ($model->cells as $cellModel) : ?>
				<?php 
					$this->renderPartial('_cell_tpl', array(
						'model' => $cellModel, 
						'adminDashletsMode' => $adminDashletsMode,
					)); 
				?>
			<?php endforeach;?>
		</div>
	</div>
</div>	
