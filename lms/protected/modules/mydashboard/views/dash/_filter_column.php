<?php
/* @var $this DashController */
/* @var $data DashboardLayout */



$infoText = '';

if ($data->ufilter_option == DashboardLayout::LAYOUT_OPTION_ALL) {
	$infoText = Yii::t('standard', 'All users');
}
else if (!$data->hasUserFilter()) {
	$warn = '<a href="javascript: void(0);" rel="tooltip" title="'. Yii::t('dashboard', 'Nobody can see this layout') .'"><i class="i-sprite is-solid-exclam orange"></i></a> ';
	$infoText = Yii::t('standard', '_NONE');
}
else {
	$parts = array();
	$info = $data->getUserFilterInfo();
	
	$nUsers 			= count($info['users']);
	$nGroups 			= count($info['groups']);
	$nBraches 			= count($info['branches']);
	$parts[] = $nUsers 		. " " . Yii::t('standard', '_USERS');
	$parts[] = $nGroups 	. " " . Yii::t('standard', '_GROUPS');
	$parts[] = $nBraches 	. " " . Yii::t('standard', 'branches');
	
	if (count($parts) > 0) {
		$infoText = implode(', ', $parts);
	}
}

$url = Docebo::createLmsUrl('usersSelector/axOpen', array(
		'type' 	=> UsersSelector::TYPE_DASHBOARD_USER_FILTER,
		'name'	=> 'What name',
		'idGeneral' => $data->id,
		'tp'=> 'mydashboard.views.dash.._ufilter_top',  // top panel partial! view
		'noyiixviewjs' => true,
));


echo $warn;
echo  CHtml::link($infoText, $url, array(
	'class' => 'open-dialog underline',
	'data-html' => true,
	'data-dialog-class' => "dashboard-layout-filter-selector",
	'data-dialog-id' => "dashboard-layout-filter-selector",
	'data-dialog-title' => Yii::t('standard', 'Filter'),
));


?>




