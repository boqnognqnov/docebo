<?php
	/* @var $data DashboardLayout */
	$editUrl	= Docebo::createAdminUrl("mydashboard/dash/editLayout", array('id' => $data['id']));
	$deleteUrl 	= Docebo::createAdminUrl("mydashboard/dash/deleteLayout", array('id' => $data['id']));
	$cloneUrl 	= Docebo::createAdminUrl("mydashboard/dash/cloneLayout", array('id' => $data['id']));
	
	$previewUrl =  Docebo::createLmsUrl('site/index', array(
		'preview'	=> 1,
		'id'		=> $data['id'],
	));
	
?>

<span rel="tooltip" title="<?= Yii::t('standard', 'Drag to reorder') ?>" class="layout-row-drag-handle p-sprite move"></span>&nbsp;&nbsp;&nbsp;


<?php 
	echo CHtml::link('<span class="i-sprite is-search"></span>', $previewUrl, array(
		'target'			=> '_new',
		'rel'	=> 'tooltip',
		'title'	=> Yii::t('standard', 'Preview'),		
	));
?>
&nbsp;&nbsp;

<?php if ($data['readonly'] == 0) : ?>
<a 
	href="<?= $editUrl ?>"
	rel="tooltip"
	title="<?= Yii::t('standard', '_MOD') ?>"
	class="open-dialog"
	data-dialog-id 		= "edit-dashboard-layout" 
	data-dialog-class	= "edit-dashboard-layout"
	data-dialog-title	= "<?= Yii::t('standard', '_MOD') ?>"
	><span class="i-sprite is-edit"></span></a>
<?php else: ?>
	<span class="i-sprite is-edit" style="visibility: hidden;"></span>
<?php endif; ?>

&nbsp;&nbsp;
<?php if ($data['readonly'] == 0) : ?>
<?php 
	echo CHtml::link('<span class="i-sprite is-clone"></span>','javascript:void(0);', array(
		'class' 			=> 'clone-dashboard-layout-link',
		'data-layout-id' 	=>	$data['id'],
		'rel'	=> 'tooltip',
		'title'	=> Yii::t('standard', 'Clone'),
	));
?>
<?php else: ?>
	<span class="i-sprite is-clone" style="visibility: hidden;"></span>
<?php endif; ?>

&nbsp;&nbsp;
<?php if ($data['is_fallback'] != 1 && ($data['readonly'] == 0)) : ?>
	<a href="<?php echo $deleteUrl; ?>"
		class="open-dialog"
		data-dialog-id="delete-dashboard-layout"
		data-dialog-class="delete-dashboard-layout"
		data-dialog-title	= "<?= Yii::t('standard', '_DEL') ?>"
		rel="tooltip"
		title="<?= Yii::t('standard', '_DEL') ?>"
		>
		<span class="i-sprite is-remove red"></span>
	</a>
<?php else: ?>
	<span class="i-sprite is-remove red" style="visibility: hidden;"></span>
<?php endif; ?>


