<h1><?= Yii::t('dashboard', 'Create/edit Widget: select type') ?></h1>
<?php /* @var $descriptor DashletDescriptor */ ?>


<div class="form">

	<?php 
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'dashlet-type-form',
			'htmlOptions' => array(
				'class' => 'ajax form-horizontal jwizard-form',
				'name' => 'dashlet-type-form'
			),
		));
		
		// Wizard related
		echo CHtml::hiddenField('from_step', 'step_type');
		
		// Selected type will saved here upon clicks
		echo CHtml::hiddenField('selected_type');
		
		// Dashboard CellID
		echo CHtml::hiddenField('cellId', $cellId);
		
	?>
	
	<div class="row-fluid">
			<ul class="dashlet-type-boxes thumbnails fluid comfort">
				<?php foreach ($descriptors as $descriptor) : ?>
					<?php $disallowDashlet = in_array($descriptor->name, $disallowedDashlets); ?>
					<li class="span4 dl-type-box <?= $disallowDashlet ? "disallowed" : "" ?>" data-dashlet-handler="<?= $descriptor->handler ?>">
						<h4 class="dl-type-title"><?= $descriptor->title ?></h4>
						<p class="dl-type-description"><?= Docebo::ellipsis($descriptor->description, 81); ?></p>
						<div class="selection-mark"><span class="p-sprite check-large-white"></span></div>
					</li>	
				<?php endforeach; ?>
				
				<!-- TEST FOR MANY -->
				<?php for ($i=0; $i < -1; $i++) : ?>
					<?php foreach ($descriptors as $descriptor) : ?>
						<?php $handler = $descriptor->handler . $i; ?>
						<li class="span4 dl-type-box" data-dashlet-handler="<?= $handler ?>">
							<h4 class="dl-type-title"><?= $descriptor->title ?></h4>
							<p class="dl-type-description"><?= Docebo::ellipsis($descriptor->description, 81); ?></p>
							<div class="selection-mark"><span class="p-sprite check-large-white"></span></div>
						</li>	
					<?php endforeach; ?>
				<?php endfor; ?>
				<!-- TEST FOR MANY -->
				
				
				
			</ul>
		<div class="clearfix"></div>
	</div>
	
	
	

	<!-- For Dialog2 - they go in footer -->
	<div class="form-actions jwizard">
		
		<?php
			if ($wizardMode) {
				echo CHtml::submitButton(Yii::t('standard', '_NEXT'), array(
	      			'class'  => 'btn btn-docebo green big jwizard-nav',
	      			'name'	=> 'next_button', 
	   			));
			} 
			else {
				echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
					'class' => 'btn btn-submit notification-submit',
					'name'	=> 'save_single_button'	
				));
			}
			?>
			<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
	</div>
	
	
	<?php $this->endWidget(); ?>
	
</div>	
	
