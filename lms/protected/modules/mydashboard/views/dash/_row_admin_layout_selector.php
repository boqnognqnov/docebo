<div class="row-layout-selector" data-row-layout="<?= $rowLayout ?>">
	<div class="row-layout-hint"><?= Yii::t('dashboard', 'Select row layout') ?></div>
	<a href="javascript:;" class="select-row-layout-button" data-row-layout="<?= DashboardRow::ROW_LAYOUT_1_12 ?>"><span  class="p-sprite layout1 <?= $rowLayout==DashboardRow::ROW_LAYOUT_1_12  	? "selected" : "" ?>"></span></a>
	<a href="javascript:;" class="select-row-layout-button" data-row-layout="<?= DashboardRow::ROW_LAYOUT_2_66 ?>"><span  class="p-sprite layout2 <?= $rowLayout==DashboardRow::ROW_LAYOUT_2_66  	? "selected" : "" ?>"></span></a>
	<a href="javascript:;" class="select-row-layout-button" data-row-layout="<?= DashboardRow::ROW_LAYOUT_3_4 ?>"><span   class="p-sprite layout3 <?= $rowLayout==DashboardRow::ROW_LAYOUT_3_4   	? "selected" : "" ?>"></span></a>
	<a href="javascript:;" class="select-row-layout-button" data-row-layout="<?= DashboardRow::ROW_LAYOUT_2_8_4 ?>"><span class="p-sprite layout4 <?= $rowLayout==DashboardRow::ROW_LAYOUT_2_8_4 	? "selected" : "" ?>"></span></a>
	<a href="javascript:;" class="select-row-layout-button" data-row-layout="<?= DashboardRow::ROW_LAYOUT_2_4_8 ?>"><span class="p-sprite layout5 <?= $rowLayout==DashboardRow::ROW_LAYOUT_2_4_8 	? "selected" : "" ?>"></span></a>
</div>



