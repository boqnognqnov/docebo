<?php
	$url = Docebo::createLmsUrl('//mydashboard/dash/editLayout'); 
?>
<div class="main-actions clearfix">
	<ul class="clearfix">
		<li>
			<div>
				<a href="<?= $url ?>"
				    alt="<?= Yii::t('dashboard', 'New layout') ?>"
					class="open-dialog p-hover"
					data-dialog-id 		= 'edit-dashboard-layout'
					data-dialog-class 	= 'edit-dashboard-layout'
					data-dialog-title	= <?= Yii::t('dashboard', 'Create/edit layout') ?>"
					>
					<span class="p-sprite dashlets-large"></span> <?= Yii::t('dashboard', 'New Widgets Layout') ?>
				</a>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>
			<p></p>
		</div>
	</div>
</div>
