<?php

	// Width of the cell; Dashlet will INHERIT this and it will be propagated by 'mycourses' jQuery plugin upon its initialization
	switch ($model->width) {
		case 8:
			$cellWidth = "twothird";
			break;
		case 6:
			$cellWidth = "half";
			break;
		case 4:
			$cellWidth = "onethird";
			break;
		case 12 :
		default:
			$cellWidth = "full";
			break;
	}
$is_expert = App7020Experts::model()->findByAttributes(array('idUser'=>Yii::app()->user->idst));
$spanWidth = 'span' . $model->width; ?>
<div class="dashlets-cell <?= $spanWidth ?>" data-cell-id="<?= $model->id ?>" data-cell-width="<?= $model->width ?>" data-cell-text-width="<?= $cellWidth ?>">

	<ul class="dashlets-sortable">
		<?php
		foreach ($model->dashlets as $dashlet) : ?>
			<?php
				// Filter out dashlets, having a handler which is NOT available at the moment
				// This is because SOME dashlets might be from disabled [custom] plugins.
				// Disabled plugins will not report their handlers, hence, they will be unavailable at THIS moment
				if ((Yii::app()->user->isGodAdmin) || (Yii::app()->mydashboard->isHandlerAvail($dashlet->handler) && $is_expert) || ($dashlet->handler!="plugin.Share7020App.widgets.DashletKnowledgeGurusActivitySummary" && $dashlet->handler!="plugin.Share7020App.widgets.DashletKnowledgeGurusKPIs" )) {
						$this->renderPartial('_dashlet_tpl', array(
							'model' => $dashlet, 
							'adminDashletsMode' => $adminDashletsMode,
							'cellModel'	=> $model, 
						)); 
				}
			?>
		<?php endforeach;?>
		
	</ul>
	
	<?php if ($adminDashletsMode) : ?>
		<div class="add-dashlet text-center"><a href="javascript:;" class="add-dashlet-button"><span class="i-sprite is-plus-solid green"></span></a></div>
	<?php endif; ?>
	

</div>
