<?php 
Yii::app()->getClientScript()->registerCssFile($this->module->getAssetsUrl() . '/css/mydashboard.css');
Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl  .   '/css/admin.css');
$this->beginContent('//layouts/lms');
echo $content;
$this->endContent();
?>