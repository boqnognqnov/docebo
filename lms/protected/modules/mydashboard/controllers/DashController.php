<?php

class DashController extends Controller  {

	static $KPIs = array(
			'mydashboard.widgets.DashletMyKPIs',
			'mydashboard.widgets.DashletAdminKPIs',
			'plugin.PowerUserApp.widgets.DashletPoweruserKPIs'
	);

	static $Catalog = array(
			'mydashboard.widgets.DashletCatalog',
	);


	const KPIs_MIN_VALID_PARAMS = 2;


	public function resolveLayout()
	{
		$this->layout = '/layouts/front';
	}


	public function filters() {
		return array(
				'accessControl' => 'accessControl',
		);
	}


	public function accessRules() {
		$res = array();

		// Allow following actions to admin only:
		$admin_only_actions = array(
			'autocomplete',
			'adminDashlets',
		 	'reorder',
			'editDashlet',
			'deleteDashlet',
			'admin',
		);

		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
				'allow',
				'users' => array('*'),
				'actions' => array('loadDashletContent'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}


	/**
	 * Create or Edit a dashboard layout
	 */
	public function actionEditLayout() {

		try {
			$id = Yii::app()->request->getParam('id', false);
			$model = DashboardLayout::model()->findByPk($id);

			if (!$model) {
				$n = DashboardLayout::model()->count();
				$model = new DashboardLayout();
				if ($n <= 0) {
					$model->default = 1;
				}
			}

			if (isset($_REQUEST['DashboardLayout'])) {
				$model->name = Yii::app()->htmlpurifier->purify($_REQUEST['DashboardLayout']['name']);
				if ($model->isNewRecord) {
					$assignDefaultLevel = true;
				}
				if ($model->save()) {
					if ($assignDefaultLevel) {
						$assignModel = new DashboardAssign();
						$assignModel->id_layout = $model->id;
						$assignModel->item_type = DashboardAssign::ITEM_TYPE_USERLEVEL;
						$assignModel->id_item = DashboardAssign::FILTER_ALL;
						$assignModel->save();
					}
					echo '<a class="auto-close success"></a>';
					Yii::app()->end();
				}
			}


			$this->renderPartial('_edit_layout', array(
				'model'	=> $model,
			));
			Yii::app()->end();


		}
		catch (Exception $e) {

		}

	}




	/**
	 * Delete a dashboard layout
	 */
	public function actionDeleteLayout() {

		try {
			$id = Yii::app()->request->getParam('id', false);
			$model = DashboardLayout::model()->findByPk($id);

			if (!$model || $model->is_fallback || $model->readonly) {
				throw new Exception('Invalid layout, can not delete');
			}

			if (isset($_REQUEST['confirm'])) {
				if (isset($_REQUEST['agree_delete']) && $_REQUEST['agree_delete'] == 'agree') {
					$model->delete();
				}
				echo '<a class="auto-close success"></a>';
				Yii::app()->end();
			}

			$this->renderPartial('_delete_layout', array(
				'model'	=> $model,
			));
			Yii::app()->end();
		}
		catch (Exception $e) {
			$this->renderPartial('//common/_dialog2_error', array('type' => 'error',  'message' => $e->getMessage()));
			Yii::app()->end();
		}
	}


	public function actionSetLayoutAsDefault() {
		$id = Yii::app()->request->getParam('id', false);

		$response = new AjaxResult();
		$response->setStatus(true);

		$model = DashboardLayout::model()->findByPk($id);
		if (!$model) {
			$response->toJSON();
			Yii::app()->end();
		}

		DashboardLayout::model()->updateAll(array('default' => 0));

		$model->default = 1;
		$model->save();

		$response->toJSON();

	}



	/**
	 * The backend counterpart of the "certifications search autocomplete" (JuiAutocomplete)
	 */
	public function actionAutocomplete() {

		$maxNumber 		= Yii::app()->request->getParam('max_number', 10);
		$term 			= Yii::app()->request->getParam('term', false);

		$model = new DashboardLayout();

		$model->search_input = $term ? $term : null;

		$dataProvider = $model->sqlDataProvider();
		$dataProvider->setPagination(array(
			'pageSize' => $maxNumber,
		));
		$data = $dataProvider->getData();
		$result = array();
		if ($data) {
			foreach ($data as $item) {
				$result[] = array(
					'label' => $item['name'],
					'value' => $item['name'],
				);
			}
		}
		echo CJSON::encode($result);
	}


	/**
	 *  Show the Dashboard (BACKEND, specific dashboard layout management)
	 */
	public function actionAdminDashlets() {

		$this->layout = '/layouts/admin';
		Yii::app()->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jwizard.js');
		Yii::app()->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.cookie.js');
		Yii::app()->getClientScript()->registerCoreScript('bbq');
		Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/css/docebo-star-rating.css');
		JsTrans::addCategories(array('dashboard', 'billing'));


		$idLayout = Yii::app()->request->getParam("id", false);
		$layoutModel = DashboardLayout::model()->findByPk($idLayout);

		// Allow dashlets to execute arbitrary bootstrapping code
		$this->dashletsBootstrap();

		// Get additional course fields data here
		$additionalCourseFieldsModels = LearningCourse::model()->getCourseAdditionalFields(true);

		$params = array(
			'layoutModel'                   => $layoutModel,
			'adminDashletsMode'	            => 1,
			'additionalCourseFieldsModels'  => $additionalCourseFieldsModels
		);
		$this->render('index', $params);


	}


	/**
	 * Show dialog to select dashlet type
	 *
	 * @param boolean $wizardMode
	 */
	public function actionSelectDashletType($cellId, $wizardMode = false) {

		try {

			$descriptors = Yii::app()->mydashboard->getDashletDescriptors();

			// Make a list of disallowed dashlets (by their descrptor->name),
			// i.e. those having $descriptor->disallowMultiInstance = true and already existing in the current layout
			// This will make them filtered out (not shown or not allowed for selection in the forthcoming UI)
			$cellModel = DashboardCell::model()->findByPk($cellId);
			$layoutModel = $cellModel->dashboardRow->layout;
			$disallowedDashlets = $layoutModel->getDisallowedDashlets();

			$html = $this->renderPartial('_select_dashlet_type', array(
				'descriptors' 			=> $descriptors,
				'wizardMode'			=> $wizardMode,
				'cellId'				=> $cellId,
				'disallowedDashlets'	=> $disallowedDashlets,
			), true);

			echo $html;

			Yii::app()->end();
		}
		catch (Exception $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
		}

	}


	/**
	 * Show dialog to edit dashlet settings
	 *
	 * @param boolean $wizardMode
	 */
	public function actionEditDashletSettings($wizardMode = false, $dashletDescriptor = false, $cellId = false) {


		try {
			$dashletModel = false;

			if (!$wizardMode) {

				$dashletId = Yii::app()->request->getParam('id', false);
				$dashletModel = DashboardDashlet::model()->findByPk($dashletId);
				$dashletDescriptor = Yii::app()->mydashboard->getDashletDescriptor($dashletModel->handler);


				$saveChangesButton = Yii::app()->request->getParam('save_changes_button', false);
				if ($saveChangesButton && $dashletModel) {
					$dashletModel->header = '';//$_REQUEST['dashlet_instance_title'];
					$dashletModel->height = $_REQUEST['dashlet_instance_height'];
					$params = array();
					$setParams = 0;
					foreach ($dashletDescriptor->settingsFieldNames as $fieldName) {
						$params[$fieldName] = isset($_REQUEST[$fieldName]) ? $_REQUEST[$fieldName] : false;
						if($params[$fieldName] !== false){
							$setParams++;
						}
					}

					// Process Headers/Titles translations
					$headersDashlet = Yii::app()->request->getParam('headers', array());

					foreach($headersDashlet as $key => $row) {
						$dashboardDashletTranslation = DashboardDashletTranslation::model()->findByAttributes(array('id_dashlet' => $dashletId, 'lang_code' => $key));

						if (!$dashboardDashletTranslation) {
							$dashboardDashletTranslation = new DashboardDashletTranslation();
							$dashboardDashletTranslation->lang_code = $key;
							$dashboardDashletTranslation->id_dashlet = $dashletId;
						}

						$dashboardDashletTranslation->header = strval($row);
						$dashboardDashletTranslation->save();
					}

					if(in_array($dashletModel->handler, self::$Catalog)) {
						// Proceed with extra Catalog params processing
						$cookieName = 'dashlet_display_style_' . $dashletModel->id;
						$displayStyle = Yii::app()->request->getParam('initial_display_style', DashletCatalog::DISPLAY_STYLE_THUMB);
						Yii::app()->request->cookies[$cookieName] = new CHttpCookie($cookieName, $displayStyle);
					}

					if(in_array($dashletModel->handler, self::$KPIs)){
						if(self::KPIs_MIN_VALID_PARAMS <= $setParams){
							$dashletModel->params = json_encode($params);
							$dashletModel->save();
							echo '<a class="auto-close success" data-dashlet-id="' . $dashletModel->id . '"></a>';
							Yii::app()->end();
						}
					} else{
						$dashletModel->params = json_encode($params);
						$dashletModel->save();
						echo '<a class="auto-close success" data-dashlet-id="' . $dashletModel->id . '"></a>';
						Yii::app()->end();
					}

				}
            }

			// Get languages
			$langs = Lang::getLanguages(true);
			$lang_code = Settings::get('default_language', 'english');
			foreach($langs as $key => $lang) {
				if($lang['code'] == $lang_code) {
					$langs[$key]['description'] = $lang['description'] . " (" . Yii::t('standard', '_DEFAULT_LANGUAGE') . ")";
					break;
				}
			}
			$langsArr = array();

			// Prepare array with all available languages
			foreach($langs as $lang){
				$langsArr[$lang['code']] = $lang['description'];
			}

			$html = $this->renderPartial('_edit_dashlet_settings', array(
				'wizardMode'	=> $wizardMode,
				'descriptor'	=> $dashletDescriptor,
				'dashletModel'	=> $dashletModel,
				'cellId'        => $cellId,
				'langs'        => $langsArr
			), true, true);

			echo $html;
			Yii::app()->end();
		}
		catch (Exception $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
		}

	}



	/**
	 * Delete dashlet
	 */
	public function actionDeleteDashlet() {
		$id = Yii::app()->request->getParam("id", false);
		DashboardDashlet::model()->deleteByPk($id);
		$response = new AjaxResult();
		$response->setStatus(true)->toJSON();
	}

	/**
	 *
	 */
    public function actionLoadDashletContent() {

		// Dashlet ID
		$id 			= Yii::app()->request->getParam("id", false);
		// Layout ID (could be REAL or Virtual one, it depends; Negative value means Virtual one)
		$idLayout		= Yii::app()->request->getParam("layout_id", false);

		$dashletModel 	= DashboardDashlet::model()->findByPk($id);

		// If Layout ID hints it is a virtual one, create a new instance of the same type (ID)
		if ($idLayout < 0) {
			$layoutModel = DashboardLayout::getVirtualLayout($idLayout, $dashletModel);
			// Now, get the resulting dashlet (because it will have some attributes changed)
			if ($layoutModel)
				$dashletModel = $layoutModel->rows[0]->cells[0]->dashlets[0];
		}

		// Some users are not allowed to see handles
		$handles = "";
		if (Yii::app()->user->isGodAdmin)
			$handles = $this->renderPartial('_admin_handles', array(), true);

		// Normalize Height
		$height = DashboardDashlet::normalizeCssHeight($dashletModel->height);

		if (!Yii::app()->mydashboard->isHandlerAvail($dashletModel->handler)) {
			$content = "Widget content is not available (disabled plugin?)";
			Yii::log('The handler "'.$dashletModel->handler.'" is not available. Unable to render dashlet content', CLogger::LEVEL_ERROR);
		}
		else {
			$cs = Yii::app()->getClientScript();
			$cs->scriptMap['font-awesome.min.css'] = false;
			// Render this partially, so we can utilize the "postProcess"
			$content = $this->renderPartial('_dashlet_content', array(
				'dashletModel' 			=> $dashletModel,
				'idLayout'				=> $idLayout,
			), true, true);
		}

		// Some dashlets may need the HTML content only to be returned as a response
		// e.g. Grid/List views, when  ajax Update is executed
		if (isset($_REQUEST['dashletHtmlOnly'])) {
			echo $content;
			Yii::app()->end();
		}

	    $dashletHeader = DashletWidget::getCurrentLangTitle();

		$data = array(
				'content' 	=> $content,
				'handles'	=> $handles,
				'header'	=> $dashletHeader,//$dashletModel->header,
				'height'	=> $height,
		);

		echo json_encode($data);

	}


	/**
	 *  Show the Dashboard (frontend)
	 */
	public function actionIndex() {

		$this->breadcrumbs = array(Yii::t('course', '_WELCOME'));

		try {
		    
		    // Experimental - close the session before start loading the dashboard
		    // This will unlock the session (file) and allow other dashboard widgets (dashlets) to start loading immediately
		    // Yii::app()->session->close();
		    
		    
			Yii::app()->getClientScript()->registerCoreScript('bbq');
			Yii::app()->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.cookie.js');
			Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/css/docebo-star-rating.css');

			JsTrans::addCategories(array('dashboard', 'billing'));

			// General ID; the meaning depends on context: dashlet ID, layout ID, ...
			$id 		= Yii::app()->request->getParam('id', false);

			// Check for incoming layout ID; for "preview" parameter (use $id)
			$preview 	= Yii::app()->request->getParam('preview', false);

			// Check if we are asked to display Single Cell Layout, i.e. a layout with only one row, one cell and a dashlet inside ( by dashlet ID, $id)
			$singleCellLayout 		= Yii::app()->request->getParam('sclo', false);

			// A special option may come with the request to change "what to show"
			$option = Yii::app()->request->getParam('opt', false);

			// Show full catalog in a standalone layout (opt=catalog)? Catalog plugin must be active!
			$catalogType = Settings::get('catalog_type', 'catalog');
			$catalogIsVisible = $catalogType == 'mycourses' ? false : true;
			if (PluginManager::isPluginActive('CoursecatalogApp') && $option === 'catalog' && !$singleCellLayout && $catalogIsVisible) {
				$this->breadcrumbs = array();
				$this->breadcrumbs[] = Yii::t('standard', 'Catalog');
				$dashlet = DashletCatalog::getFullCatalogDashlet();
				$singleCellLayout = 'sclo';
				$id = $dashlet->id;
			}
			else if ($option === 'fullmycourses') {
				$this->breadcrumbs = array();
				$this->breadcrumbs[] = Yii::t('menu_over', '_MYCOURSES');
				$dashlet = DashletMyCourses::getFullMyCoursesDashlet();
				$singleCellLayout = 'sclo';
				$id = $dashlet->id;
			}
			else if ($option === 'fullrewardsshop') {
				$this->breadcrumbs = array();
				$this->breadcrumbs[] = Yii::t('gamification', 'Rewards shop');
				$dashlet = DashletRewardShop::getFullRewardsShopDashlet();
				$singleCellLayout = 'sclo';
				$id = $dashlet->id;
			}
				

			if ($preview && Yii::app()->user->getIsGodadmin()) {
				$idLayout = $id;
			}
			else {
				$idLayout = DashboardAssign::resolveUserLayout();
			}

			// If we are requested to show a so called "single cell layout", create a virtual layout and use it
			// It will have only one row, only one cell (FULL width) and only THE dashlet found by incoming ID
			// This is used, for example, to "view all my courses" in a new page
			if ($singleCellLayout && $id) {
				$layoutModel = DashboardLayout::getVirtualLayout(DashboardLayout::VIRTUAL_TYPE_SINGLE_CELL, DashboardDashlet::model()->findByPk($id));
			}
			else {
				$layoutModel = DashboardLayout::model()->findByPk($idLayout);
			}

			if (!$layoutModel) {
				throw new Exception('Could not resolve home page: dashboard broken', 403);
			}

			// Check if the loaded dashlet is the channels one
			if($layoutModel->readonly && ($layoutModel->name == 'Channels layout')) {
				$this->redirect(Docebo::createAppUrl('lms:channels'));
				return;
			}


			// Allow dashlets to execute arbitrary bootstrapping code
			$this->dashletsBootstrap($layoutModel);

			// Get additional course fields data here
			$additionalCourseFieldsModels = LearningCourse::model()->getCourseAdditionalFields(true);

			$params = array(
				'layoutModel'                   => $layoutModel,
				'additionalCourseFieldsModels'  => $additionalCourseFieldsModels
			);

			// TODO FIXME remove after tests
			//$this->layout = 'theme.spt.views.layouts.login';

			$this->render('index', $params);

		}
		catch (Exception $e) {
			echo "<h4>" . $e->getMessage() . "</h4>";
			Yii::app()->end();
		}

	}


	/**
	 * Layouts administration: create, edit, assignments
	 */
	public function actionAdmin() {

		$this->layout = '/layouts/admin';

		// Check 'Fallback/Standard' layout, we MUST have one
		$fallbackLayout = DashboardLayout::model()->findByAttributes(array('is_fallback' => 1));
		if (!$fallbackLayout) {
			$fallbackLayout = DashboardLayout::createFallbackLayout();
		}

		$searchInput = Yii::app()->request->getParam('search_input', false);
		$selectAll = Yii::app()->request->getParam('comboGridViewSelectAll', false);

		$model = new DashboardLayout();
		$model->search_input = $searchInput ? $searchInput : null;

		if ($selectAll) {
			$dataProvider = $model->sqlDataProvider();
			$dataProvider->setPagination(false);
			$keys = $dataProvider->keys;
			foreach ($keys as $index => $key) {
				$keys[$index] = (int) $key;
			}
			echo json_encode($keys);
			Yii::app()->end();
		}

		$gridId = 'dashboard-layouts-grid';

		$dataProvider = $model->sqlDataProvider();
		//$dataProvider->pagination = array('pageSize' => 1);
		$dataProvider->pagination = false;

		$gridParams = array(
				'gridId' 				=> $gridId,
				'dataProvider'			=> $dataProvider,
				'columns'				=> $this->getGridColumns(),
				'autocompleteRoute'		=> '//mydashboard/dash/autocomplete',
				'massiveActions'		=> array(),
				'disableMassSelection' 	=> true,
				'disableMassActions' 	=> true,
				'sortableRows'			=> true,
				'sortableUpdateUrl' 	=> Docebo::createLmsUrl('//mydashboard/dash/reorderLayouts'),
				'dragHandlerSelector' 	=> '.layout-row-drag-handle',
		);

		$params = array(
			'gridParams' 		=> $gridParams,
			'model'				=> $model,
			'editDialog'		=> 'edit-dialog',
		);

		// If this is an Yii Grid Update request (detected by ajax=<grid-ID> in the Request)
		// just render-partial the grid. This hugely improves the speed!!!
		if (isset($_REQUEST['ajax']) && ($_REQUEST['ajax'] == $gridId)) {
			$this->widget('common.widgets.ComboGridView', $gridParams);
			Yii::app()->end();
		}

		
		$url = Yii::app()->theme->baseUrl. '/css/DoceboPager.css';
		Yii::app()->getClientScript()->registerCssFile($url);
		// Register selector resources
		UsersSelector::registerClientScripts();

		$this->render('admin', $params);

	}



	public function actionGetDashletTpl() {

		$id = Yii::app()->request->getParam("id", false);
		$adminDashletsMode = Yii::app()->request->getParam("admin", false);

		$adminDashletsMode = $adminDashletsMode && Yii::app()->user->getIsGodadmin();

		$model = DashboardDashlet::model()->findByPk($id);

		$html = $this->renderPartial('_dashlet_tpl', array(
			'model' 			=> $model,
			'adminDashletsMode' 	=> $adminDashletsMode,
		),true);

		$response = new AjaxResult();
		$response->setStatus(true)->setHtml($html)->toJSON();
		Yii::app()->end();

	}



	/**
	 * Render and ECHO HTML of a single Layout Row
	 *
	 * @param integer $id
	 * @return string
	 */
	public function actionGetRawHtml() {

		$id 		= Yii::app()->request->getParam("id", false);
		$admin		= Yii::app()->request->getParam("admin", false);

		$adminDashletsMode = $admin && Yii::app()->user->getIsGodadmin();
		$model = DashboardRow::model()->findByPk($id);
		$html = $this->renderPartial('_row_tpl', array(
			'model' 			=> $model,
			'adminDashletsMode' => $adminDashletsMode,
		),true);
		echo $html;
	}



	/**
	 * Reorder ROWS and DASHLETS
	 */
	public function actionReorder() {

		$layout 		= Yii::app()->request->getParam("layout", false);
		$reorderType	= Yii::app()->request->getParam("reorderType", false);
		$sorted 		= Yii::app()->request->getParam("sorted", false);
		$rowId 			= Yii::app()->request->getParam("rowId", false);
		$cellId			= Yii::app()->request->getParam("cellId", false);

		if ($reorderType === "row") {
			$result = $this->reorderRows($sorted);
		}
		else if ($reorderType === "dashlet") {
			$result = $this->reorderDashlets($cellId, $sorted);
		}

	}



	/**
	 * Called upon LAYOUTs reordering (by the Combogrid .sortable() update event
	 *
	 */
	public function actionReorderLayouts() {

		$sorted 		= Yii::app()->request->getParam("sorted", false);

		if (!is_array($sorted))
			return;

		foreach ($sorted as $newPosition => $id) {
			$model = DashboardLayout::model()->findByPk($id);
			if (!$model)
				continue;
			$model->position = $newPosition+1;
			$model->save(false);
		}

		$response = new AjaxResult();
		$response->setStatus(true)->toJSON();
	}


	/**
	 * Create a new dashboard row; AJAX called
	 */
	public function actionCreateNewRow() {

		$response = new AjaxResult();
		$response->setStatus(false);

		// Dashboard layout to put the new row in
		$layout = Yii::app()->request->getParam("layout", false);
		if ($layout === false) {
			$response->toJSON();
			Yii::app()->end();
		}

		// And specific row layout requested ?? If not, use the default one
		$rowLayout = Yii::app()->request->getParam("row_layout", false);
		if (!$rowLayout) {
			$rowLayout = DashboardRow::ROW_LAYOUT_DEFAULT;
		}

		// Create the new row; it will create new cells internally!
		$newRowModel = DashboardRow::createNewRow($layout, $rowLayout);
		if (!$newRowModel) {
			$response->setStatus(false)->toJSON();
			Yii::app()->end();
		}

		// Return data back to caller
		$data = array('id' => $newRowModel->id);
		$response->setStatus(true)->setData($data)->toJSON();

		Yii::app()->end();

	}



	/**
	 * Clone (copy) a given row
	 *
	 */
	public function actionCloneRow() {

		$response = new AjaxResult();
		$response->setStatus(false);

		// The row ID we are asked to clone
		$row = Yii::app()->request->getParam("row", false);

		// Clone the row, by ID
		$newRowModel = DashboardRow::cloneRow($row);

		// Return data back to caller
		$data = array('id' => $newRowModel->id);
		$response->setStatus(true)->setData($data)->toJSON();

		Yii::app()->end();

	}



	/**
	 * Delete one dashboard row. It will all delete children cells and associated dashlets instances (by DB relation)
	 */
	public function actionDeleteRow() {

		$id = Yii::app()->request->getParam("id", false);
		$result = DashboardRow::model()->deleteByPk($id);

		$response = new AjaxResult();
		$response->setStatus(false);

		if ($result) {
			$response->setStatus(true);
		}

		// This is usually an async call, return the ID of the deleted row as well, call may need it
		$data = array(
			'id' => $id
		);
		$response->setData($data)->toJSON();


	}


	/**
	 * Change Row Layout
	 */
	public function actionChangeRowLayout() {
		$id = Yii::app()->request->getParam("id", false);
		$rowLayout = Yii::app()->request->getParam("rowLayout", false);

		$response = new AjaxResult();
		$response->setStatus(false);

		if (!$id || !$rowLayout) {
			$response->toJSON();
		}

		$row = DashboardRow::model()->findByPk($id);
		$row->rearrangeRowLayout($rowLayout);


		$response->setStatus(true);

		// Return the same data back to caller
		// Also, we need to return IDs of all ROW's dashlets, so content to be reloaded properly
		$row->refresh();
		$dashletIds = array();
		foreach ($row->cells as $cell) {
			foreach ($cell->dashlets as $dashlet) {
				$dashletIds[] = $dashlet->id;
			}
		}
		$data = array(
			'id'		 	=> $id,
			'dashletIds'	=> $dashletIds,
		);

		$response->setData($data)->toJSON();

	}



	/**
	 * Collects data from UsersSelector for user filter (+groups, charts,..)
	 */
	public function actionUserFilter() {

		$layoutId 		= Yii::app()->request->getParam('idGeneral', false);
		$ufilterOption 	= Yii::app()->request->getParam('ufilter_option', false);

		$model = DashboardLayout::model()->findByPk($layoutId);
		if (!$model)
			return;


		if ($ufilterOption == DashboardLayout::LAYOUT_OPTION_ALL) {
			$users		= array();
			$groups		= array();
			$branches	= array();
		}
		else {
			$users		= UsersSelector::getUsersListOnly($_REQUEST);
			$groups		= UsersSelector::getGroupsListOnly($_REQUEST);
			$branches	= UsersSelector::getBranchesListOnly($_REQUEST);
		}

		$model->assignUsers($users);
		$model->assignGroups($groups);
		$model->assignBranches($branches);

		$model->ufilter_option = $ufilterOption;
		$model->save();

		echo '<a class="auto-close success"></a>';
		Yii::app()->end();
	}


	/**
	 *
	 */
	public function actionChangeLayoutLevelFilter() {
		$response = new AjaxResult();

		$layoutId 	= Yii::app()->request->getParam('id', false);
		$level 		= Yii::app()->request->getParam('level', false);
		$levelLabels = array_merge(array(DashboardAssign::FILTER_ALL => Yii::t('standard', '_ALL')),CoreGroup::getAdminLevels(true, false, true, true));

		$model = DashboardAssign::model()->findByAttributes(array('id_layout' => $layoutId, 'item_type' => DashboardAssign::ITEM_TYPE_USERLEVEL));
		if (!$model) {
			$model = new DashboardAssign();
			$model->id_layout = $layoutId;
			$model->item_type = DashboardAssign::ITEM_TYPE_USERLEVEL;
		}
		$model->id_item = $level;
		$model->save();

		$data = array(
			'level_label'	=> $levelLabels[$level],
		);
		$response->setStatus(true)->setData($data)->toJSON();
	}


	/**
	 * Clone/Copy a layout with all the rows, cells and so on.
	 */
	public function actionCloneLayout() {

		$id = Yii::app()->request->getParam('id', false);
		$model = DashboardLayout::model()->findByPk($id);

		$cloning = $model->cloneMe();

		$response = new AjaxResult();

		$response->setStatus(false);
		if ($cloning) {
			$response->setStatus(true);
		}
		$response->toJSON();
		Yii::app()->end();

	}


	/**
	 * Assign set of power user profiles to a Layout
	 */
	public function actionAssignPuProfiles() {

		$layoutId 		= Yii::app()->request->getParam('idGeneral', false);
		$ufilterOption 	= Yii::app()->request->getParam('ufilter_option', false);

		$model = DashboardLayout::model()->findByPk($layoutId);
		if (!$model)
			return;


		if ($ufilterOption == DashboardLayout::LAYOUT_OPTION_ALL) {
			$puprofiles		= array();
		}
		else {
			$puprofiles		= UsersSelector::getPuProfilesList($_REQUEST);
		}

		$model->assignPuProfiles($puprofiles);

		$model->puprofiles_filter_option = $ufilterOption;
		$model->save();

		echo '<a class="auto-close success"></a>';
		Yii::app()->end();

	}



	/**
	 * Dashlet Wizard related constants
	 * @var string
	 */
	const STEP_SELECT_DASHLET_TYPE 	= 'step_type';
	const STEP_DASHLET_SETTINGS 	= 'step_settings';
	const STEP_INITIAL 				= 'step_initial';
	/**
	 * Create Dashlet Wizard
	 */
	public function actionDashletWizard() {

		$fromStep 					= Yii::app()->request->getParam('from_step', self::STEP_INITIAL);
		$nextButton 				= Yii::app()->request->getParam('next_button', false);
		$prevButton 				= Yii::app()->request->getParam('prev_button', false);
		$finishWizard	 			= Yii::app()->request->getParam('finish_wizard', false);
		$idForm						= Yii::app()->request->getParam('idForm', false);
		$noWizardNav				= Yii::app()->request->getParam('nowiznav', false);

		// Creating dashlet is always in the context of a given dashboard CELL!!!!
		$cellId						= Yii::app()->request->getParam('cellId', false);

		// Get all the wizard data
		$data = $this->collectDashletWizardData($_REQUEST);

		// Also :: Validate incoming data here
		// @TODO


		// FINISH Wizard requested (confirm/save, in this case) ?
		if ($finishWizard) {
			if($this->saveDashlet($data)){
				echo '<a class="auto-close success" data-cell-id="' . $data['cellId'] . '"></a>';
				Yii::app()->end();
			} else{
				$fromStep = self::STEP_SELECT_DASHLET_TYPE;
			}
		}


		// Depending on FROM STEP and PREV/NEXT button, render/execute the next dialog/action
		switch ($fromStep) {

			case self::STEP_SELECT_DASHLET_TYPE:
				// If no type has been selected , go back to the same step
				if (!$data['selected_type']) {
					$this->goDashletWizardStep(self::STEP_SELECT_DASHLET_TYPE, false, $cellId);
				}
				$this->goDashletWizardStep(self::STEP_DASHLET_SETTINGS, $data, $cellId);
				break;

			// DEFAULT
			case self::STEP_INITIAL:
			default:
				$this->goDashletWizardStep(self::STEP_SELECT_DASHLET_TYPE, false, $cellId);
				break;
		}

	}


	/**
	 * Save instance of the dashlet
	 *
	 * @param array $data
	 */
	protected function saveDashlet($data) {
		$model = new DashboardDashlet();
		$model->handler 	= $data['selected_type'];
		$model->cell 		= $data['cellId'];
		$model->height 		= isset($data['dashlet_instance_height']) ? $data['dashlet_instance_height'] : 0;
		$model->header		= '';//$data['dashlet_instance_title'];

		// Save dashlet specific settings (fields)
		$params = array();
		$dashletDescriptor = Yii::app()->mydashboard->getDashletDescriptor($model->handler);

		$setParams = 0;
		foreach ($dashletDescriptor->settingsFieldNames as $fieldName) {
			$params[$fieldName] = isset($data[$fieldName]) ? $data[$fieldName] : false;
			if($params[$fieldName] !== false){
				$setParams++;
			}
		}

		if(in_array($model->handler, self::$KPIs)){
			if(self::KPIs_MIN_VALID_PARAMS > $setParams){
				return false;
			}
		}

		$model->params = json_encode($params);

		$result = $model->save();

		if ($result) {
			// Process Headers/Titles translations
			$headersDashlet = Yii::app()->request->getParam('headers', array());
			foreach($headersDashlet as $key => $row) {
				$dashboardDashletTranslation = DashboardDashletTranslation::model()->findByAttributes(array('id_dashlet' => $model->id, 'lang_code' => $key));

				if (!$dashboardDashletTranslation) {
					$dashboardDashletTranslation = new DashboardDashletTranslation();
					$dashboardDashletTranslation->lang_code = $key;
					$dashboardDashletTranslation->id_dashlet = $model->id;
				}

				$dashboardDashletTranslation->header = $row;
				$dashboardDashletTranslation->save();
			}
		}


		return $result;
	}

	/**
	 * Collect all expected and know wizard data (from all possible steps)
	 *
	 * @param array $request
	 * @return array
	 */
	protected function collectDashletWizardData($request) {

		$data = array();

		// From step 1 (where, for example, we select dashlet type)
		$step1Form = isset($request['Wizard']) ? $request['Wizard']['dashlet-type-form'] : $request['dashlet-type-form'];
		if (isset($step1Form['selected_type']))  $data['selected_type'] = $step1Form['selected_type'];
		if (isset($step1Form['cellId']))  $data['cellId'] = $step1Form['cellId'];

		// From Step 2: Dashlet settings
		$step2Form = isset($request['Wizard']) ? $request['Wizard']['dashlet-settings-form'] : $request['dashlet-settings-form'];


		// Just harvest all form "input"s, including buttons etc., it does NOT harm!
		if ($step2Form) {
			foreach ($step2Form as $formName => $formValue) {
				$data[$formName] = $formValue;
			}
		}

		return $data;

	}


	/**
	 * Dispatch Wizard steps
	 *
	 * @param string $step
	 */
	protected function goDashletWizardStep($step, $data = false, $cellId) {

		switch ($step) {
			case self::STEP_SELECT_DASHLET_TYPE:
				$this->actionSelectDashletType($cellId, true);
				break;
			case self::STEP_DASHLET_SETTINGS:
				if (!$data || !isset($data['selected_type'])) {
					$this->actionSelectDashletType(true);
				}
				else {
					$dashletDescriptor = Yii::app()->mydashboard->getDashletDescriptor($data['selected_type']);
					if (!$dashletDescriptor) {
						$this->actionSelectDashletType(true);
					}
					$this->actionEditDashletSettings(true, $dashletDescriptor, $cellId);
				}
				break;
		}
	}



	/**
	 * Certifications grid columns
	 *
	 * @return array
	 */
	protected function getGridColumns() {

		$columns = array(

				array(
						'name' => 'name',
						'value' => '"<strong>" . $data["name"] . "</strong>"',
						'type' => 'raw',
						'header' => Yii::t('standard', '_NAME'),
						'htmlOptions' => array(
						),
						'headerHtmlOptions' => array(
						),
				),

				array(
						'name' => 'filter',
						'value' => array($this, 'renderFilterColumn'),
						'type' => 'raw',
						'header' => Yii::t('standard', 'Filter'),
						'htmlOptions' => array(
							"class" => "text-center",
						),
						'headerHtmlOptions' => array(
							"class" => "text-center",
						),

				),

				array(
						'name' => 'roles',
						'value' => array($this, 'renderLevelColumn'),
						'type' => 'raw',
						'header' => Yii::t('standard', '_LEVEL'),
						'htmlOptions' => array(
							"class" => "text-center",
						),
						'headerHtmlOptions' => array(
							"class" => "text-center",
							"style" => "width: 150px;"
						),
				),

				array(
						'name' => 'power_user_profiles',
						'value' => array($this, 'renderPuProfilesColumn'),
						'type' => 'raw',
						'header' => Yii::t('menu', '_PUBLIC_ADMIN_RULES'),
						'htmlOptions' => array(
							"class" => "text-center",
						),
						'headerHtmlOptions' => array(
							"class" => "text-center",
						),
				),

				array(
						'name' => 'dashlets',
						'value' => array($this, 'renderDashletsColumn'),
						'type' => 'raw',
						'header' => Yii::t('standard', 'Widgets'),
						'htmlOptions' => array(
							"class" => "text-center",
						),
						'headerHtmlOptions' => array(
							"class" => "text-center",
						),
				),
				array(
						'name' => 'default_layout',
						'value' => array($this, 'renderDefaultColumn'),
						'type' => 'raw',
						'header' => Yii::t('standard', 'Default'),
						'htmlOptions' => array(
							"class" => "text-center",
						),
						'headerHtmlOptions' => array(
							"class" => "text-center",
						),
				),

				array(
						'name' => 'operations',
						'value' => array($this, 'renderLayoutOperationsColumn'),
						'type' => 'raw',
						'header' => '',
						'htmlOptions' => array(
							'class' => 'text-right',
						),
						'headerHtmlOptions' => array(
						),
				),


		);


		return $columns;

	}

	protected function renderFilterColumn($data, $index) {
		$data = DashboardLayout::model()->findByPk($data['id']);
		if ($data->is_fallback) {
			$html = "";
		}
		else {
			$html = $this->renderPartial('_filter_column', array(
				'data' => $data,
			), true);
		}
		return $html;
	}

	protected function renderLevelColumn($data, $index) {
		$data = DashboardLayout::model()->findByPk($data['id']);
		if ($data->is_fallback) {
			$html = "";
		}
		else {
			$html = $this->renderPartial('_level_column', array(
				'data' => $data,
			), true);
		}
		return $html;
	}

	protected function renderDashletsColumn($data, $index) {
		if($data["readonly"] == 1)
			return '<a href="'.Docebo::createAppUrl('app7020:channelsManagement/index').'">'.Yii::t('app7020', 'Manage channels').'</a>';

		$data = DashboardLayout::model()->findByPk($data['id']);
		$url = $this->createUrl('adminDashlets', array(
				'id'	=> $data['id'],
		));

		$count = $data->countLayoutDashlets();

		if ($count <= 0) {
			$warn = '<a href="javascript: void(0);" rel="tooltip" title="'. Yii::t('dashboard', 'Empty dashboard') .'"><i class="i-sprite is-solid-exclam orange"></i></a> ';
		}

		$html = $warn . " " . CHtml::link($count, $url, array('class' => 'underline')) . " " . CHtml::link('<span class="p-sprite dashlets"></span>', $url);
		return $html;
	}

	protected function renderDefaultColumn($data, $index) {

		//if ($data['is_fallback'])
		//	return "";

		$green = $data['default'] == 1 ? "green" : "";
		if ($data['default'] != 1) {
			$html = '<a href="javascript:;" class="toggle-default-layout-link" data-id="'. $data['id'] .'"><span class="p-sprite flag3 '.$green.'"></span></a>';
		}
		else {
			$html = '<a href="javascript: void(0);" rel="tooltip" title="'. Yii::t('dashboard', 'Loaded if no other layout matches the user') .'"><span class="p-sprite flag3 '.$green.'"></span></a>';
		}
		return $html;
	}


	protected function renderLayoutOperationsColumn($data, $index) {
		$html = $this->renderPartial('_layout_operations', array(
			'data' => $data,
		), true);
		return $html;
	}

	protected function renderPuProfilesColumn($data, $index) {
		$data = DashboardLayout::model()->findByPk($data['id']);
		if ($data->is_fallback) {
			$html = "";
		}
		else {
			$html = $this->renderPartial('_pu_profiles_column', array(
				'data'	=> $data,
			), true);
		}
		return $html;
	}


	/**
	 * Reorder Dashboard rows
	 *
	 * @param array $sorted
	 */
	protected static function reorderRows($sorted) {


		if (!is_array($sorted))
			return;

		foreach ($sorted as $newPosition => $cssId) {
			$tmp = explode('-', $cssId);
			if (!isset($tmp[1]))
				continue;
			$itemId = $tmp[1];
			$model = DashboardRow::model()->findByPk($itemId);
			if (!$model)
				continue;
			$model->position = $newPosition+1;
			$model->save(false);
		}

		return;
	}

	/**
	 * Reorder dashlets inside a cell
	 *
	 * @param integer $cellId
	 * @param array $sorted
	 */
	protected static function reorderDashlets($cellId, $sorted) {

		if (!is_array($sorted))
			return;

		foreach ($sorted as $newPosition => $cssId) {
			$tmp = explode('-', $cssId);
			if (!isset($tmp[1]))
				continue;
			$itemId = $tmp[1];
			$model = DashboardDashlet::model()->findByPk($itemId);
			if (!$model)
				continue;
			$model->cell = $cellId;
			$model->position = $newPosition+1;
			$model->save(false);
		}

	}


	/**
	 * Allow every dashlet to execute arbitrary code DURING main dashboard page loading (front and back end).
	 * For example, to "register" JS and CSS files and scripts using Yii registerCss/registerJs methods.
	 *
	 * If a layout is specified, ONLY existing dashlets for that layout will be called.
	 * Otherwise, all dashlets returning a descriptor (collected by Mydashboard App components) will be called to execute their bootstrap() methods.
	 *
	 * @param DashboardLayout $layoutModel
	 */
	protected function dashletsBootstrap($layoutModel=false) {

		$dashletHandlers = array();
		// No layout? Call every dashlet's bootstrap()
		if (!$layoutModel) {
			$dashletHandlers = Yii::app()->mydashboard->getAvailHandlers();
		}
		else {
			foreach ($layoutModel->rows as $rowModel)
				foreach ($rowModel->cells as $cell)
					foreach ($cell->dashlets as $dashlet)
						$dashletHandlers[] = $dashlet->handler;
		}

		// Enumerate collected handlers, create a class and check for method existence (we don't use Interface for this, because it is not strictly required)
		// If method exists, execute it (whatever it does)
		if (count($dashletHandlers > 0)) {
			foreach ($dashletHandlers as $classPath) {
				try {
					$handlerFile = Yii::getPathOfAlias($classPath) . ".php";
					if (!file_exists($handlerFile)) {
						throw new Exception("Unable to load dashlet handler class: PHP file not found: " . $handlerFile);
					}
					Yii::import($classPath);
					$tmp = explode('.', $classPath);
					$className = end($tmp);
					$object = new $className();
					if (method_exists($object, 'bootstrap')) {
						$object->bootstrap();
					}
				}
				catch (Exception $e) {
					Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
				}
			}
		}
	}



}
