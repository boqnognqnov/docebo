<?php


class DashletWidgetsController extends Controller  {

	/**
	 * [Blog Dashlet]
	 */
	public function actionAxAdditionalFieldsAutocomplete(){
		$tag = $_REQUEST['tag'];

		$command = Yii::app()->getDb()->createCommand()
			->select('f.id_field, ft.translation')
			->from(CoreUserField::model()->tableName().' f')
			->join(CoreUserFieldTranslations::model()->tableName().' ft', 'ft.id_field = f.id_field')
			->where('ft.translation LIKE :query', array(
				':query'=>'%'.$tag.'%'
			))
			->andWhere('ft.lang_code=:lang', array(
				':lang'=>Lang::getCodeByBrowserCode(Yii::app()->getLanguage())
			));

		$query = $command->queryAll();

		$res = array();
		foreach($query as $tmp){
			$res[] = array(
				'key'=>$tmp['id_field'],
				'value'=>CHtml::encode($tmp['translation'])
			);
		}
		echo CJSON::encode($res);
	}

	/**
	 * [Social Dashlet]
	 */
	public function actionAxAvailableSocialServicesAutocomplete(){
		$available = DashletSocial::getAvailableServices();

		$query = strtolower($_REQUEST['tag']);

		$res = array();
		foreach($available as $serviceCodename=>$serviceMetaData){
			if(!$query){
				// return all
				$res[] = array(
					'key'=>$serviceCodename,
					'value'=>$serviceMetaData['label']
				);
			}elseif(stripos(strtolower($serviceCodename), $query)!==false || stripos(strtolower($serviceMetaData['label']), $query)!==false){
				$res[] = array(
					'key'=>$serviceCodename,
					'value'=>$serviceMetaData['label']
				);
			}
		}
		echo CJSON::encode($res);
	}

	/**
	 * [Questions & Answers Dashlet - Sort by field autocomplete ]
	 */
	public function actionAxAvailableQuestionsAndAnswersAutocomplete(){
		$available = DashletQuestionsAndAnswers::getAvailableSortingFields();

		$query = strtolower($_REQUEST['tag']);

		$res = array();
		foreach($available as $serviceCodename=>$serviceMetaData){
			if(!$query){
				// return all
				$res[] = array(
					'key'=>$serviceCodename,
					'value'=>$serviceMetaData['label']
				);
			}elseif(stripos(strtolower($serviceCodename), $query)!==false || stripos(strtolower($serviceMetaData['label']), $query)!==false){
				$res[] = array(
					'key'=>$serviceCodename,
					'value'=>$serviceMetaData['label']
				);
			}
		}
		echo CJSON::encode($res);
	}

	/**
	 * [My KPIs Dashlet]
	 *
	 * Renders the HTML for a single block inside the My KPIs dashlet's
	 * frontend. Multiple ajax calls are done to this method with a
	 * different $kpi until the dashlet's frontend is fully populated
	 * 
	 * Unknown KPI triggers Event raising to get back calculated data and view path to be rendered, if any
	 * 
	 */
	public function actionAxGetMyKPIFrontendBlock($kpi = null){
		
		if(!in_array($kpi, array_keys(DashletMyKPIs::getKPIList())))
			return; // This KPI is not available in the platform

		$params = array();

		$success = false;

		// In the following switch, CORE KPIs are looked first. 
		// If not found, in the "default" case, event is raised to collect data from listeners
		switch($kpi){
			
			case 'last_lp_progress':
				$params['lpName'] = null;
				$params['percent'] = -1;

				$lastAccessedLP = Yii::app()->getDb()->createCommand()
					->select('cp.path_name, cp.id_path')
					->from(LearningCoursepath::model()->tableName().' cp')
					->join(LearningCoursepathUser::model()->tableName().' cpu', 'cpu.id_path=cp.id_path AND cpu.idUser=:idUser')
					->join(LearningCoursepathCourses::model()->tableName().' cpc', 'cpc.id_path=cp.id_path')
					->join(LearningCourseuser::model()->tableName().' cu', 'cu.idCourse=cpc.id_item AND cu.idUser=:idUser')
					->order('cu.date_last_access DESC')
					->queryRow(true, array(':idUser'=>Yii::app()->user->getIdst()));

				if(!empty($lastAccessedLP)){
					$params['lpName'] = $lastAccessedLP['path_name'];

					$idPath = $lastAccessedLP['id_path'];
                    $params['id_path'] = $idPath;
					$lpCourseStatuses = Yii::app()->getDb()->createCommand()
					                       ->select('cpc.id_item, cpu.status')
					                       ->from(LearningCoursepathCourses::model()->tableName().' cpc')
					                       ->leftJoin(LearningCourseuser::model()->tableName().' cpu', 'cpc.id_item=cpu.idCourse AND cpu.idUser=:idUser', array(':idUser'=>Yii::app()->user->getIdst()))
					                       ->where('cpc.id_path=:id_path', array(':id_path'=>$idPath))
					                       ->queryAll();

					$total = count($lpCourseStatuses);
					$completed = 0;
					foreach($lpCourseStatuses as $tmp){
						if($tmp['status']==LearningCourseuser::$COURSE_USER_END)
							$completed++;
					}

					$params['percent'] = $total==0 ? 0 : ($completed*100)/$total;

					$success = true;
				}

				break;
			case 'last_elearning_progress':
				$lastAcccessedCourseName = Yii::app()->getDb()->createCommand()
				                              ->select('c.name, c.idCourse')
				                              ->from(LearningCourse::model()->tableName().' c')
				                              ->join(LearningCourseuser::model()->tableName().' cu', 'cu.idCourse=c.idCourse AND cu.idUser=:idUser', array(':idUser'=>Yii::app()->user->getIdst()))
				                              ->where('c.course_type=:elearning', array(':elearning'=>LearningCourse::TYPE_ELEARNING))
				                              ->order('cu.date_last_access DESC')
				                              ->queryRow();

				$params['percent'] = -1;
				$params['name'] = null;
				$params['idCourse'] = null;

				if(!empty($lastAcccessedCourseName)){
					$params['name'] = $lastAcccessedCourseName['name'];

					$params['percent'] = LearningOrganization::getProgressByUserAndCourse($lastAcccessedCourseName['idCourse'], Yii::app()->user->getIdst());

					$params['idCourse'] = $lastAcccessedCourseName['idCourse'];

					$success = true;
				}
				break;
			case 'last_completed_elearning':
				$params['courseName'] = null;
				$params['idCourse']   = null;
				$lastCompletedCourse = Yii::app()->getDb()->createCommand()
					->select('c.name, c.idCourse, cu.date_complete ')
					->from(LearningCourse::model()->tableName().' c')
					->join(LearningCourseuser::model()->tableName().' cu', 'c.idCourse=cu.idCourse AND cu.idUser=:idUser AND cu.status=:completed')
					->where('c.course_type=:elearning')
					->order('cu.date_complete DESC')
					->bindValues(array(
						':idUser'=>Yii::app()->user->getIdst(),
						':completed'=>LearningCourseuser::$COURSE_USER_END,
						':elearning'=>LearningCourse::TYPE_ELEARNING
					))
					->queryRow();
				if(!empty($lastCompletedCourse)) {
					$params['courseName'] = $lastCompletedCourse['name'];
					$params['idCourse']   = $lastCompletedCourse['idCourse'];
					$success = true;
				}
				break;
			case 'last_published_course':
				$params['courseName'] = null;
				$params['idCourse'] = null;

				$query = Yii::app()->getDb()->createCommand()
					->select('c.name, c.idCourse')
					->from(LearningCourse::model()->tableName().' c')
					->where(array('IN', 'c.status', array(
						LearningCourse::$COURSE_STATUS_AVAILABLE,
						LearningCourse::$COURSE_STATUS_EFFECTIVE
					)))
					->order('c.create_date DESC');

				if(Yii::app()->user->getIsPu()){
					$query->join(CoreUserPuCourse::model()->tableName().' pu', 'pu.course_id=c.idCourse AND pu.puser_id=:idUser', array(':idUser'=>Yii::app()->user->getIdst()));
				}

				if(!PluginManager::isPluginActive('ClassroomApp')){
					$query->andWhere('c.course_type <> :classroom', array(':classroom'=>LearningCourse::TYPE_CLASSROOM));
				}

				$query = $query->queryRow();

				if(!empty($query)) {
					$params['courseName'] = $query['name'];
					$params['idCourse'] = $query['idCourse'];
					$success = true;
				}
				break;
			case 'best_elearning_score':
				$params['courseName'] = null;

				$completedCourses = Yii::app()->getDb()->createCommand()
					->select('c.name, c.idCourse')
					->from(LearningCourse::model()->tableName().' c')
					->join(LearningCourseuser::model()->tableName().' cu', 'cu.idCourse=c.idCourse AND cu.idUser=:idUser AND cu.status=:completed', array(':idUser'=>Yii::app()->user->getIdst(), ':completed'=>LearningCourseuser::$COURSE_USER_END))
					->where('c.course_type=:elearning', array(':elearning'=>LearningCourse::TYPE_ELEARNING))
					->queryAll();

				$courseWithBestScore = array(
					'name'=>null,
					'score'=>-1,
				);

				foreach($completedCourses as $completedCourse){
					$newScore = intval(LearningCourseuser::getLastScoreByUserAndCourse($completedCourse['idCourse'], Yii::app()->user->getIdst()));

					if($courseWithBestScore['score']<0){
						// First iteration
						$courseWithBestScore['name'] = $completedCourse['name'];
						$courseWithBestScore['score'] = $newScore;
						$courseWithBestScore['idCourse'] = $completedCourse['idCourse'];
					}

					if($newScore > $courseWithBestScore['score']){
						$courseWithBestScore['name'] = $completedCourse['name'];
						$courseWithBestScore['score'] = $newScore;
						$courseWithBestScore['idCourse'] = $completedCourse['idCourse'];
					}
					$success = true;
				}

				$params['courseWithBestScore'] = $courseWithBestScore;
				
				break;
				
			default:
				// Looks like we didin't find a CORE KPI with this id, lets raise an event and see if someone got some data & view
				$viewPath 	= false;
				$viewParams	= false;
				$viewHtml 	= false;
				Yii::app()->event->raise('CollectCustomKPIData', new DEvent(self, array(
					'kpi'			=> $kpi,
					'viewPath' 		=> &$viewPath,  	// !! by reference
					'viewParams'	=> &$viewParams,	// !! by reference
					'viewHtml'		=> &$viewHtml,		// !! by reference
				)));
				
				// If we get HTML, show it, it has high priority
				if ($viewHtml) {
					$this->sendJSON(array(
						'html'=>$viewHtml,
						'success'=>true,
					));
				}
				else if ($viewPath && $viewParams) {
					$this->sendJSON(array(
						'html'=>$this->renderPartial($viewPath, $viewParams, 1),
						'success'=>true,
					));
				}
				
				// END THE SCRIPT HERE!!!!
				Yii::app()->end();
				break;
				
		}

		// This will be called ONLY for CORE KPIs
		$html = $this->renderPartial('mykpis_'.$kpi, $params, 1);
		$this->sendJSON(array(
			'html'=>$html,
			'success'=>$success,
		));
		Yii::app()->end(); 
	}

	/**
	 * [Calendar Dashlet]
	 */
	public function actionGetEventsForDate(){
		$startOfDayTimestamp = $_REQUEST['start']; // unix timestamp
		$endOfDayTimestamp = $_REQUEST['end']; // unix timestamp
		$date = new DateTime('NOW', new DateTimeZone('UTC'));
		$date->setDate($_REQUEST['year'], $_REQUEST['month']+1, $_REQUEST['date']);
		$date->setTime(0, 0, 0);
		$startOfDayTimestamp = strtotime(Yii::app()->localtime->fromLocalDateTime($date->format('Y-m-d H:i:s')));
		$date->setTime(23,59,59);
		$endOfDayTimestamp = strtotime(Yii::app()->localtime->fromLocalDateTime($date->format('Y-m-d H:i:s')));

		if(!$startOfDayTimestamp || !$endOfDayTimestamp){
			Yii::log('Invalid day interval provided', CLogger::LEVEL_ERROR);
			echo('Invalid day interval provided');
			Yii::app()->end();
		}

		$itemsForThatDay = DashletCalendar::getItemsForCalendar(-1, $startOfDayTimestamp, $endOfDayTimestamp);

		$this->renderPartial('calendar_day_items_tooltip', array(
			'items'=>$itemsForThatDay,
		));
	}
	
	/**
	 * "sorting fields autocomplete" counterpart for My Courses Settings: Sort by
	 */
	public function actionAxMyCoursesSortingFieldsAutocomplete() {
		
		$available = DashletMyCourses::getAvailableSortingFields();
		
		$query = strtolower($_REQUEST['tag']);
		
		$res = array();
		foreach($available as $field => $label){
			if (!$query) {
				// return all
				$res[] = array(
					'key' 	=> $field,
					'value'	=> $label,
				);
			} 
			else if (stripos(strtolower($field), $query)!==false || stripos(strtolower($label), $query)!==false) {
				$res[] = array(
					'key' 	=> $field,
					'value'	=> $label,
				);
			}
		}
		
		echo CJSON::encode($res);
		
	}

	/**
	 * "sorting fields autocomplete" counterpart for Learning Plan Settings: Sort by
	 */
	public function actionAxLPSortingFieldsAutocomplete() {
		$available = DashletLearningPlan::getAvailableSortingFields();
		$query = strtolower($_REQUEST['tag']);
		$res = array();
		foreach($available as $field => $label){
			if (!$query) {
				// return all
				$res[] = array(
					'key' 	=> $field,
					'value'	=> $label,
				);
			}
			else if (stripos(strtolower($field), $query)!==false || stripos(strtolower($label), $query)!==false) {
				$res[] = array(
					'key' 	=> $field,
					'value'	=> $label,
				);
			}
		}
		echo CJSON::encode($res);
	}

	public function actionAxQuestionsAndAnswersFieldsAutocomplete() {

		$available = DashletQuestionsAndAnswers::getAvailableSortingFields();

		$query = strtolower($_REQUEST['tag']);

		$res = array();
		foreach($available as $field => $label){
			if (!$query) {
				// return all
				$res[] = array(
					'key' 	=> $field,
					'value'	=> $label,
				);
			}
			else if (stripos(strtolower($field), $query)!==false || stripos(strtolower($label), $query)!==false) {
				$res[] = array(
					'key' 	=> $field,
					'value'	=> $label,
				);
			}
		}

		echo CJSON::encode($res);

	}

	/**
	 * "sorting fields autocomplete"  for Knowledge Libraty Settings: "Sort by" field
	 */
	public function actionAxKnowledgeLibrarySortingbyFieldsAutocomplete() {

		$available = DashletKnowledgeLibrary::getAvailableSortingFields();

		$query = strtolower($_REQUEST['tag']);

		$res = array();
		foreach($available as $field => $label){
			if (!$query) {
				// return all
				$res[] = array(
					'key' 	=> $field,
					'value'	=> $label,
				);
			}
			else if (stripos(strtolower($field), $query)!==false || stripos(strtolower($label), $query)!==false) {
				$res[] = array(
					'key' 	=> $field,
					'value'	=> $label,
				);
			}
		}

		echo CJSON::encode($res);

	}

	/**
	 * "sorting fields autocomplete"  for Knowledge Libraty Settings: "Show only asstets associated to the following topics" field
	 */
	public function actionAxKnowledgeLibraryShowTopicsFieldsAutocomplete() {
		$tags = App7020Tag::getAllTags();
		$res = array();
		foreach($tags as $tag){
			$res[] = array(
				'key' 	=> $tag['id'],
				'value'	=> $tag['tagText'],
			);
		}
		echo CJSON::encode($res);
	}
	
	/**
	 * "sorting fields autocomplete" counterpart for Catalog dashlet Settings: Sort by
	 */
	public function actionAxCatalogSortingFieldsAutocomplete() {
	
		$available = DashletCatalog::getAvailableSortingFields();
	
		$query = strtolower($_REQUEST['tag']);
	
		$res = array();
		foreach($available as $field => $label){
			if (!$query) {
				// return all
				$res[] = array(
						'key' 	=> $field,
						'value'	=> $label,
				);
			}
			else if (stripos(strtolower($field), $query)!==false || stripos(strtolower($label), $query)!==false) {
				$res[] = array(
						'key' 	=> $field,
						'value'	=> $label,
				);
			}
		}
	
		echo CJSON::encode($res);
	
	}
	
	
	
	
	
	
	
}
