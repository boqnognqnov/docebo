<?php

class DashletWelcome extends DashletWidget implements IDashletWidget {
	
	/**
	 *
	 */
	public static function descriptor($params=false) {

		$sampleImageUrl = Yii::app()->mydashboard->getModule()->getAssetsUrl() . "/images/dashlet_sample.png";
		
		$descriptor = new DashletDescriptor();
		
		$descriptor->name 					= 'core_dashlet_welcome';
		$descriptor->handler				= 'mydashboard.widgets.DashletWelcome';
		$descriptor->title					= 'Dashlet Welcome';
		$descriptor->description			= 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua';
		$descriptor->sampleImageUrl 		= $sampleImageUrl;
		$descriptor->settingsFieldNames		= array();
		
		return $descriptor;
		
	}

	/**
	 * @see IDashletWidget::renderSettings()
	 */
	public function renderSettings() {
		echo "";
	}
	
	/**
	 * @see IDashletWidget::renderFrontEnd()
	 */
	public function renderFrontEnd() {
		if ($this->dashletModel)
			$this->render(strtolower(__CLASS__));
		else
			echo "";
	}

	
	/**
	 * @see IDashletWidget::bootstrap()
	 */
	public function bootstrap($params=false) {
	}
	
	
}