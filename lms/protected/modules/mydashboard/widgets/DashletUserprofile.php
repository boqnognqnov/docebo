<?php

/**
 * Class DashletUserprofile
 *
 * Format for $this->params:
 * array(
 *    'show_profile_pic'=>bool, // Show the profile picture
 *    'show_my_activities_link'=>bool, // Show link to "My activities"
 *    'show_email'=>bool, // Show user's email
 *    'show_role'=>bool, // Show user's role
 *    'additional_fields_arr'=>array, // IDs of additional fields to show
 * )
 */
class DashletUserprofile extends DashletWidget implements IDashletWidget {


	public function renderSettings() {
		$cs = Yii::app()->getClientScript();
		$themeUrl = Yii::app()->theme->baseUrl;

		// FCBK
		$cs->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
		$cs->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');

        // Load CSS
        $tmpUrlCSS = Yii::app()->mydashboard->getModule()->getAssetsUrl();
		Yii::app()->getClientScript()->registerCssFile($tmpUrlCSS . '/css/userprofile.css');

		$additionalFieldsSelected = array();

		$oldAdditionalFields = $this->getParam('additional_fields_arr', array());
		if(!empty($oldAdditionalFields) && is_array($oldAdditionalFields)){
			$oldFieldsMeta = Yii::app()->getDb()->createCommand()
                ->select('f.id_field, ft.translation, f.type')
                ->from(CoreUserField::model()->tableName() . ' f')
                ->join(CoreUserFieldTranslations::model()->tableName().' ft', 'ft.id_field = f.id_field')
                ->where(array('IN', 'f.id_field', $oldAdditionalFields))
                ->andWhere('ft.lang_code = :lang', array(':lang'=>Lang::getCodeByBrowserCode( Yii::app()->getLanguage() )))->queryAll();

			foreach($oldFieldsMeta as $tmp){
				$additionalFieldsSelected[$tmp['id_field']] = $tmp['translation'];
			}
		}

		$this->render('dashletuserprofile_settings', array(
			'additionalFieldsSelected'=>$additionalFieldsSelected,
		));
	}


	public function renderFrontEnd() {

        /** @var CoreUser $userModel */
		$userModel = Yii::app()->user->loadUserModel();

		if(!$userModel) return;

		$showUserProfilePic = isset($this->params['show_profile_pic']) ? (bool) $this->params['show_profile_pic'] : false;
		$showMyActivitiesLink = isset($this->params['show_my_activities_link']) ? (bool) $this->params['show_my_activities_link'] : false;
		$showEmail = isset($this->params['show_email']) ? (bool) $this->params['show_email'] : false;

		// ALways hidden for now
		//$showRole = isset($this->params['show_role']) ? (bool) $this->params['show_role'] : false;
		$showRole = false;

		if(PluginManager::isPluginActive('Share7020App')) {
			$showKGRole = (!empty($this->params['show_knowledge_guru_role'])) ? (bool)$this->params['show_knowledge_guru_role'] : false;
			$showKGTopics = (!empty($this->params['show_knowledge_guru_topics'])) ? (bool)$this->params['show_knowledge_guru_topics'] : false;
		}


		// DEBUG::
		// $showUserProfilePic = $showMyActivitiesLink = $showEmail = $showRole = true;

        $additionalFieldUserEntries = [];
		// Array of field IDs
		$additionalFieldIdsToShow = $this->getParam('additional_fields_arr', array());
		$additionalFieldIdsToShow = !empty($additionalFieldIdsToShow) ? (array) $additionalFieldIdsToShow : array();

        $additionalFields = $userModel->getAdditionalFields(true);

		foreach($additionalFields as $field){
            /** @var CoreUserField $field */
            if(!in_array($field->getFieldId(), $additionalFieldIdsToShow)){
                continue;
            }
			$fieldId = $field->getFieldId();
			$fieldType = $field->getFieldType();
			$fieldLocalizedName = $field->getTranslation();
			$fieldUserValueRaw  = $field->userEntry;

			$userEntry = null;

			switch($fieldType){
				case CoreUserField::TYPE_YESNO:
					$userEntry = FieldYesno::getLabelFromValue($fieldUserValueRaw);
					break;
				case CoreUserField::TYPE_COUNTRY:
					$userEntry = FieldCountry::model()->renderFieldValue($fieldUserValueRaw);
					break;

				case CoreUserField::TYPE_UPLOAD:
					$userEntry = FieldUpload::model()->renderFieldValue($fieldUserValueRaw);
					$userEntry = ($userEntry) ? $userEntry : '"N/A"';
					break;

				case CoreUserField::TYPE_DROPDOWN:
					$model = new FieldDropdown();
					$model->lang_code = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
					$model->id_field = $fieldId;
					$userEntry = $model->renderFieldValue($fieldUserValueRaw);
					break;

				default:
					$userEntry = CoreUserField::renderFieldValueByUserId($fieldId, $userModel->idst);
			}

			$additionalFieldUserEntries[$fieldLocalizedName] = $userEntry;
		}

		$userAvatarImgUrl = $userModel->getAvatarImage(true);

		$roleLabel = Yii::t('admin_directory', '_DIRECTORY_'.Yii::app()->user->getHighestLevel());

		$this->render(strtolower(__CLASS__), array(
			'userModel'=>$userModel,

			// Settings variables
			'showUserProfilePic'=>$showUserProfilePic,
			'showMyActivitiesLink'=>$showMyActivitiesLink,
			'showEmail'=>$showEmail,
			'showRole'=>$showRole,

			// Data variables
			'avatarUrl'=>$userAvatarImgUrl,
			'roleLabel' => $roleLabel,
			'additionalFields'=>$additionalFieldUserEntries,
		    'showKGRole' => $showKGRole,
		    'showKGTopics' => $showKGTopics
		));

	}

	/**
	 * Dashlet descriptor
	 *
	 * @return array of dashlet data describing it to the outside world
	 */
	public static function descriptor( $params = FALSE ) {
		$sampleImageUrl = Yii::app()->mydashboard->getModule()->getAssetsUrl() . "/images/userprofile_sample.jpg";

		$descriptor = new DashletDescriptor();

		$descriptor->name 					= 'core_dashlet_userprofile';
		$descriptor->handler				= 'mydashboard.widgets.DashletUserprofile';
		$descriptor->title					= Yii::t('profile', '_PROFILE');
		$descriptor->description			= Yii::t('dashlets', 'Allows to display the avatar and profile information of the currently logged in user');
		$descriptor->sampleImageUrl 		= $sampleImageUrl;
		$descriptor->settingsFieldNames		= array('show_profile_pic', 'show_my_activities_link', 'show_email', 'show_role', 'additional_fields_arr', 'show_knowledge_guru_topics', 'show_knowledge_guru_role');

		return $descriptor;
	}


	/**
	 * @see IDashletWidget::bootstrap()
	 */
	public function bootstrap($params=false) {
	    $clientScript = Yii::app()->getClientScript();
	    $clientScript->registerCssFile(App7020Helpers::getAssetsUrl() . '/css/app7020.css');
	}

}