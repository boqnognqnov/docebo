<?php
class DashletHTML extends DashletWidget implements IDashletWidget{

	/**
	 * Dashlet descriptor
	 *
	 * @return DashletDescriptor Object, describing the dashlet to the outside world
	 */
	public static function descriptor( $params = FALSE ) {
		$sampleImageUrl = Yii::app()->mydashboard->getModule()->getAssetsUrl() . "/images/html_sample.jpg";

		$descriptor = new DashletDescriptor();

		$descriptor->name 					= 'core_dashlet_html';
		$descriptor->handler				= 'mydashboard.widgets.DashletHTML';
		$descriptor->title					= 'HTML/WYSIWYG';
		$descriptor->description			= Yii::t('dashlets', 'Widget with custom HTML content');
		$descriptor->sampleImageUrl 		= $sampleImageUrl;
		$descriptor->settingsFieldNames		= array('html');
		$descriptor->hasHeight				= 1;

		return $descriptor;
	}

	/**
	 * Render dashlet settings, used in Admin Dashlets UI
	 */
	public function renderSettings() {
		$this->render('html/settings');
	}

	/**
	 * Render fronend content.
	 *
	 * NOTE: Don't use Yii's CClientScript to register js/css resources
	 * here since it doesn't work. Use echo '<script src=...</script>' instead
	 */
	public function renderFrontEnd() {
		$currentLang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
		$defaulLang = CoreSetting::get('default_language');
		$html = $this->getParam('html');

		$this->render('html/index',
				array(
						'currentLang'   => $currentLang,
						'defaulLang'    => $defaulLang,
						'html'          => $html
				)
		);
	}

	/**
	 * Run arbitrary code just before the main page (dashboard) is going to be loaded (rendered) IF the dashlet takes part of it
	 *
	 * <strong>Note 1</strong>: this method is called BEFORE rendering of the main page, as an attribute of an instance created BEFORE rendering!
	 * Which means, DURING the rendering of the dashlet, another object instance is created/used, having no relation to the first instance
	 * The first intance is purely created (and dismissed) for the porpose of bootstraping to allow, mainly, preloading client (browser) resources
	 *
	 * <strong>Note 2</strong>: Be aware, if you have 2 dashlets of the same type in the dashboard, bootstrap() will be called 2 times!!!
	 *
	 * <strong>Hint</strong>: Use this to execute Yii::app()->getClientScript()->registerXXXXX(<url>) to load dashlet specific own javascript/css
	 *
	 * @param string $params
	 */
	public function bootstrap( $params = FALSE ) {
		Yii::app()->tinymce->init();
	}

	/**
	 * get HTML content for the textarea
	 *
	 * @param bool|false $lang
	 * @return mixed
	 */
	public function getHtmlContent($lang = false) {
		if(!$lang) {
			$lang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
		}

		// Get default lang
		$defaultLang = CoreSetting::get('default_language');

		$htmlArr = $this->getParam('html');
		if($defaultLang != $lang && empty($htmlArr[$lang])) {
			return $htmlArr[$defaultLang];
		} else {
			return $htmlArr[$lang];
		}
	}

}