<?php

class DashletIframe extends DashletWidget implements IDashletWidget {

	/**
	 *
	 */
	public static function descriptor($params=false) {
		$sampleImageUrl = Yii::app()->mydashboard->getModule()->getAssetsUrl() . "/images/iframe_sample.jpg";
		
		$descriptor = new DashletDescriptor();
		
		$descriptor->name 					= 'core_dashlet_iframe';
		$descriptor->handler				= 'mydashboard.widgets.DashletIframe';
		$descriptor->title					= 'IFRAME';
		$descriptor->description			= Yii::t('dashlets', 'Widget that displays custom content from an external URL');
		$descriptor->sampleImageUrl 		= $sampleImageUrl;
		$descriptor->settingsFieldNames		= array('iframe_url');
		$descriptor->hasHeight				= 1;
		
		return $descriptor;
		
	}
		
	/**
	 * @see IDashletWidget::renderSettings()
	 */
	public function renderSettings() {
		$currentLang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
		$defaulLang = CoreSetting::get('default_language');

		$this->render(strtolower(__CLASS__ . "_settings"),
				array(
					'currentLang'   => $currentLang,
					'defaulLang'    => $defaulLang
				)
		);
	}
	
	/**
	 * @see IDashletWidget::renderFrontEnd()
	 */
	public function renderFrontEnd() {
		$currentLang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
		$defaulLang = CoreSetting::get('default_language');

		if ($this->dashletModel)
			$this->render(strtolower(__CLASS__),
					array(
							'currentLang'   => $currentLang,
							'defaulLang'    => $defaulLang
					)
			);
		else
			echo "";
	}

	/**
	 * @see IDashletWidget::bootstrap()
	 */
	public function bootstrap($params=false) {
	}
	
	
}