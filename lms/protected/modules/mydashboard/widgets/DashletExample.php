<?php

class DashletExample extends DashletWidget implements IDashletWidget {
	
	
	/**
	 * 
	 */
	public static function descriptor($params=false) {
		$sampleImageUrl = Yii::app()->mydashboard->getModule()->getAssetsUrl() . "/images/dashlet_sample.png";

		$descriptor = new DashletDescriptor();
		
		$descriptor->name 					= 'core_dashlet_example';
		$descriptor->handler				= 'mydashboard.widgets.DashletExample';
		$descriptor->title					= 'Dashlet Example';
		$descriptor->description			= 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua';
		$descriptor->sampleImageUrl 		= $sampleImageUrl;
		$descriptor->settingsFieldNames		= array('example_field_1', 'example_field_2', 'selected_types');
		
		return $descriptor;
	}
	
	
	/**
	 * @see IDashletWidget::renderSettings()
	 */
	public function renderSettings() {
		$this->render(strtolower(__CLASS__ . "_settings"));
	}
	

	/**
	 * @see IDashletWidget::renderFrontEnd()
	 */
	public function renderFrontEnd() {
		if ($this->dashletModel)
			$this->render(strtolower(__CLASS__));
		else 
			echo "";
	}
	
	
	/**
	 * @see IDashletWidget::bootstrap() 
	 */
	public function bootstrap($params=false) {
		// $tmpUrl = Yii::app()->mydashboard->getModule()->getAssetsUrl();
		// Yii::app()->getClientScript()->registerCssFile($tmpUrl . '/css/example.css');
		// Yii::app()->getClientScript()->registerScriptFile($tmpUrl . '/js/example.js');
	}
	
}


