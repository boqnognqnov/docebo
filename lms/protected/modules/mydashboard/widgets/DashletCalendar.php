<?php

class DashletCalendar extends DashletWidget  implements IDashletWidget {

	const VIEW_TYPE_CALENDAR = 'calendar';
	const VIEW_TYPE_LISTVIEW = 'listview';

	// Only used by the "full calendar" view type
	protected $colors = array();

	/**
	 * Dashlet descriptor
	 *
	 * @return DashletDescriptor Object, describing the dashlet to the outside world
	 */
	public static function descriptor( $params = FALSE ) {
		$sampleImageUrl = Yii::app()->mydashboard->getModule()->getAssetsUrl() . "/images/calendar_sample.jpg";

		$descriptor = new DashletDescriptor();

		$descriptor->name 					= 'core_dashlet_calendar';
		$descriptor->handler				= 'mydashboard.widgets.DashletCalendar';
		$descriptor->title					= Yii::t('standard', '_CALENDAR');
		$descriptor->description			= Yii::t('dashlets', 'Shows a calendar where the user can see the list of courses he is enrolled in on a day by day basis');
		$descriptor->sampleImageUrl 		= $sampleImageUrl;
		$descriptor->settingsFieldNames		= array('show_my_calendar_link', 'view_type', 'max_items');
		$descriptor->hasHeight				= 1;

		return $descriptor;
	}

	/**
	 * Render dashlet settings, used in Admin Dashlets UI
	 */
	public function renderSettings() {
		$this->render('dashletcalendar_settings', array(

		));
	}

	/**
	 * Render fronend content
	 */
	public function renderFrontEnd() {
		$viewType = $this->getParam('view_type', self::VIEW_TYPE_CALENDAR);

		$show_my_calendar_link = $this->getParam('show_my_calendar_link', false);

		// Only used when 'view_type' is self::VIEW_TYPE_LISTVIEW
		$max_items = $this->getParam('max_items', 20);

		switch($viewType){
			case self::VIEW_TYPE_LISTVIEW:
				$this->renderListView();
				break;
			default:
				$this->renderFullCalendar();
				break;
		}
	}

	/**
	 * This will render a vertical list of latest "events"
	 * that the user should take action on, e.g. expiring subscription
	 * to courses, courses ending soon, video conferences upcoming, etc
	 *
	 * @throws CException
	 */
	private function renderListView(){
		$maxItems = $this->getParam('max_items', 20);

		$allItems = self::getItemsForCalendar($maxItems);

		$this->render('dashletcalendar_listview', array(
			'show_my_calendar_link' => $this->getParam('show_my_calendar_link', false),
			'max_items' => $this->getParam('max_items', 20),
			'allItems' => $allItems,
		));
	}

	static public function getItemsForCalendar($limit = -1, $startTimestamp = null, $endTimestamp = null){

		// Those are only populated below if their corresponding
		// plugins are active
		$classrooms = array();
		$videoConferences = array();
		$lpCourses = array();

		// An array that will hold the mixture of all
		// elearning, classroom and videoconference items.
		// It will later be ordered by key (which is the timestamp
		// of the item)
		$allItems = array();

		if(PluginManager::isPluginActive('CurriculaApp')) {
			$command = Yii::app()->db->createCommand()
				->from(LearningCoursepath::model()->tableName().' cp')
				->join(LearningCoursepathCourses::model()->tableName().' cpc', 'cpc.id_path = cp.id_path AND cp.days_valid IS NOT NULL')
				->join(LearningCourse::model()->tableName().' c', 'c.idCourse = cpc.id_item')
				->join(LearningCoursepathUser::model()->tableName().' cpu', 'cpu.id_path = cp.id_path')
				->andWhere('c.course_type = :course_type', array(':course_type' => LearningCourse::TYPE_ELEARNING))
				->andWhere('cpu.idUser = :idUser', array(':idUser' => Yii::app()->user->id))
				->andWhere('cpu.date_end_validity IS NOT NULL AND date_end_validity <> "0000-00-00 00:00:00"');

			$allLpCoursesCommand = clone $command;
			$command->select('c.idCourse, c.name, cpu.date_end_validity');
			$allLpCoursesCommand->select('c.idCourse');
			if(!$startTimestamp && !$endTimestamp) {
				$command->andWhere('cpu.date_end_validity > NOW()');
			} else {
				$command->andWhere('cpu.date_end_validity >= FROM_UNIXTIME(:start) AND cpu.date_end_validity <= FROM_UNIXTIME(:end)', array(
					':start' => $startTimestamp,
					':end' => $endTimestamp
				));
			}

			if($limit>0)
				$command->limit($limit);
			$lpCourses = $command->queryAll();
			$lpCoursesIds = $allLpCoursesCommand->queryColumn();
		}

		$command = Yii::app()->db->createCommand();
		$command->select('c.idCourse, c.name, COALESCE(cu.date_expire_validity, c.date_end) AS date, cu.idUser');
		$command->from(LearningCourse::model()->tableName().' as c');
		$command->join(LearningCourseuser::model()->tableName().' as cu', 'c.idCourse = cu.idCourse');
		$command->where('cu.idUser = :id_user');
		$command->params[':id_user'] = Yii::app()->user->id;
		if(PluginManager::isPluginActive('CurriculaApp') && !empty($lpCoursesIds))
			$command->andWhere(array('NOT IN', 'c.idCourse', $lpCoursesIds));
		if(!$startTimestamp && !$endTimestamp) {
			$command->andWhere( '
				(cu.date_expire_validity IS NOT NULL AND cu.date_expire_validity > NOW())
				OR
				(cu.date_expire_validity IS NULL AND c.date_end <> "0000-00-00" AND c.date_end > NOW())
		' );
		}else{
				$command->andWhere('
				(cu.date_expire_validity IS NOT NULL AND cu.date_expire_validity >= FROM_UNIXTIME(:start) AND cu.date_expire_validity <= FROM_UNIXTIME(:end))
				OR
				(cu.date_expire_validity IS NULL AND c.date_end <> "0000-00-00" AND c.date_end >= FROM_UNIXTIME(:start) AND c.date_end <= FROM_UNIXTIME(:end))
				');
			$command->params[':start'] = strtotime(date('Y-m-d', $startTimestamp).' 00:00:00');
			$command->params[':end'] =strtotime( date('Y-m-d', $endTimestamp).' 23:59:59');
		}
		if($limit>0)
			$command->limit($limit);

		$elearningCourses = $command->queryAll();

		if(PluginManager::isPluginActive('ClassroomApp')) {
			$command = Yii::app()->db->createCommand();
			$command->select('cs.course_id, cs.id_session, cs.name, csd.name as ses_name, csd.day, csd.time_begin, csd.time_end,
				CONVERT_TZ( CONCAT_WS(" ", csd.day, csd.time_begin), csd.timezone, "GMT") as dateBegin');
			$command->from(LtCourseSession::model()->tableName().' cs');
			$command->join(LtCourseSessionDate::model()->tableName().' csd', 'cs.id_session = csd.id_session');
			$command->join(LtCourseuserSession::model()->tableName().' cus', 'cs.id_session = cus.id_session');
			$command->where('cus.id_user = :id_user');
			$command->params[':id_user'] = Yii::app()->user->id;
			if(!$startTimestamp && !$endTimestamp){
				$command->andWhere('convert_tz(concat(csd.day, " ", csd.time_begin), csd.timezone, "GMT") > NOW()');
			}else{
				$command->andWhere('convert_tz(concat(csd.day, " ", csd.time_begin), csd.timezone, "GMT") > FROM_UNIXTIME(:start) AND convert_tz(concat(csd.day, " ", csd.time_begin), csd.timezone, "GMT") < FROM_UNIXTIME(:end)');
				$command->params[':start'] = $startTimestamp;
				$command->params[':end'] = $endTimestamp;
			}
			if($limit>0)
				$command->limit($limit);
			$classrooms = $command->queryAll();
		}

		//get webinars
		$webinars = WebinarSession::getWebinarsForPeriod(Yii::app()->session['text_filter'], Yii::app()->user->id, false, true, $startTimestamp, $endTimestamp, $limit);

		if(
			PluginManager::isPluginActive('AdobeConnectApp')
			|| PluginManager::isPluginActive('BigbluebuttonApp')
			|| PluginManager::isPluginActive('DoceboWebconferenceApp')
			|| PluginManager::isPluginActive('GotomeetingApp')
			|| PluginManager::isPluginActive('OnsyncApp')
			|| PluginManager::isPluginActive('TeleskillApp')
			|| PluginManager::isPluginActive('VivochaApp')
			|| PluginManager::isPluginActive('WebexApp')
			|| PluginManager::isPluginActive('SkymeetingApp')){

			foreach($webinars as $webinar){
				if ($webinar['course_type'] != 'webinar') {
					$timestamp = $webinar['dateBegin'];
					$allItems[] = array(
						'id' => $webinar['idCourse'].'_0_0_'.$webinar['id_session'],
						'name' => $webinar['name'],
						'type' => 'conference',
						'timestamp' => strtotime(Yii::app()->localtime->toLocalDateTime($timestamp, LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, 'Y-m-d H:i:s'))
					);
				}
			}
		}

		foreach($elearningCourses as $course){
			$allItems[] = array(
				'id'=>$course['idCourse'],
				'name'=>$course['name'],
				'type'=>'elearning',
				'timestamp'=>strtotime(Yii::app()->localtime->toLocalDateTime($course['date'], LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, 'Y-m-d H:i:s'))
			);
		}

		foreach($lpCourses as $course) {
			$allItems[] = array(
				'id'=>$course['idCourse'],
				'name'=>$course['name'],
				'type'=>'elearning',
				'timestamp'=>strtotime(Yii::app()->localtime->toLocalDateTime($course['date_end_validity'], LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, 'Y-m-d H:i:s'))
			);
		}

		foreach($classrooms as $classroom){
			$timestamp = $classroom['dateBegin'];
			$allItems[] = array(
				'id'=>$classroom['course_id'].'_'.$classroom['id_session'].'_'.str_replace('-', '', $classroom['day']).'_0',
				'name'=>$classroom['name'],
				'type'=>'classroom',
				'timestamp'=>strtotime(Yii::app()->localtime->toLocalDateTime($timestamp, LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, 'Y-m-d H:i:s'))
			);
		}

		foreach($webinars as $webinar){
			if ($webinar['course_type'] == 'webinar') {
				$timestamp = $webinar['dateBegin'];
				$allItems[] = array(
					'id' => $webinar['idCourse'] . '_' . $webinar['id_session'] . '_' . strtotime($webinar['day']) . '_0',
					'name' => $webinar['name'],
					'type' => 'webinar',
					'timestamp' => strtotime(Yii::app()->localtime->toLocalDateTime($timestamp, LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, 'Y-m-d H:i:s'))
				);
			}
		}

		$timestamps = array();
		foreach ($allItems as $key => $item) {
			$timestamps[$key] = $item['timestamp'];
		}
		array_multisort($timestamps, SORT_ASC, $allItems);

		if($limit>0)
			$allItems = array_slice($allItems, 0, $limit, true);

		return $allItems;
	}

	/**
	 * This will render a simplistic calendar widget with the upcoming
	 * "My calendar" events
	 * @throws CException
	 */
	private function renderFullCalendar(){
		// Reset all potential filters used by the ajax calls
		// to retrieve calendar events to their default values
		Yii::app()->session['elearning_filter'] = 1;
		Yii::app()->session['classroom_filter'] = 1;
		Yii::app()->session['videoconference_filter'] = 1;
		Yii::app()->session['text_filter'] = '';

		$this->colors = $colors = Yii::app()->theme->getChartColors();

		$this->render('dashletcalendar_calendar', array(
			'show_my_calendar_link' => $this->getParam('show_my_calendar_link', false),
		));
	}

	/**
	 * @see IDashletWidget::bootstrap()
	 */
	public function bootstrap($params=false) {
		//Load some specific assets needed for the calendar

		$cs        = Yii::app()->getClientScript();
		$assetsUrl = Yii::app()->getModule( 'mycalendar' )->assetsUrl;
		$cs->registerCssFile( Yii::app()->theme->baseUrl . '/js/formstyler/jquery.formstyler.css' );
		$cs->registerCssFile( Yii::app()->theme->baseUrl . '/css/admin.css' );
		// $cs->registerCssFile(Yii::app()->baseUrl.'/../admin/css/admin-temp.css');
		$cs->registerCssFile( $assetsUrl . '/css/fullcalendar.css' );
		//$cs->registerCssFile( $assetsUrl . '/css/docebotheme.css' );
		$cs->registerScriptFile( $assetsUrl . '/js/fullcalendar.js' );
	}

}