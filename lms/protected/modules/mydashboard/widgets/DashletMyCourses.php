<?php

class DashletMyCourses extends DashletWidget implements IDashletWidget {


	/**
	 * Display styles/modes
	 * @var string
	 */
	const DISPLAY_STYLE_THUMB 	    = 'thumb';
	const DISPLAY_STYLE_LIST 	    = 'list';
	const DISPLAY_STYLE_CALENDAR    = 'calendar';

	/**
	 * Enrollment statuses
	 * @var string
	 */
	const STATUS_ALL 			= 'all';
	const STATUS_ACTIVE 		= 'active';
	const STATUS_COMPLETED 		= 'completed';

	/**
	 * Value for "all" labels filtering
	 * @var string
	 */
	const LABEL_FILTER_ALL		= 'all';
	const LABEL_FILTER_NONE		= 'none';

	protected static $handler	= 'mydashboard.widgets.DashletMyCourses';


	/**
	 * Hold filtering coming from the REQUEST
	 * @var mixed
	 */
	public $fltEnrollmentStatus;
	public $fltLabelId;
	public $fltSearch;

	public $filterObject;

	public $groupByLabels;
	public $pageSize;
	public $pageNumber;

	/**
	 * Return list of translated dusplay modes
	 *
	 * @return array
	 */
	public static function getDisplayStyles() {
		return array(
			self::DISPLAY_STYLE_THUMB 	=> Yii::t('dashboard', 'Thumbnails'),
			self::DISPLAY_STYLE_LIST 	=> Yii::t('dashboard', 'List'),
		);
	}


	/**
	 * Return list of translated enrollment statuses
	 *
	 * @return multitype:string Ambigous <string, string, unknown>
	 */
	public static function getEnrollmentStatusesList() {
		return array(
				self::STATUS_ALL 		=> Yii::t('standard', '_ALL_COURSES'),
				self::STATUS_ACTIVE 	=> Yii::t('dashboard', '_ACTIVE_COURSE'),
				self::STATUS_COMPLETED	=> Yii::t('standard', 'Completed courses'),
		);
	}


	/**
	 * Return Labels related to current user
	 * (filtered according to the courses the user is enrolled in)
	 * @return array
	 */
	private function getUserLabelsList($addAll=false) {
		$idUser = Yii::app()->user->id;
		$langCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

		$labels = array();
		if ($addAll) {
			$allLabel = new LearningLabel();
			$allLabel->title = Yii::t('standard', 'All labels');
			$allLabel->id_common_label = self::LABEL_FILTER_ALL;
			$labels[self::LABEL_FILTER_ALL] = $allLabel;
		}

		$userLabels = LearningLabel::model()->getUserLabels($idUser, $langCode);
		if (!empty($userLabels)) {
			foreach($userLabels as $label) {
				if ($label['courseuser_count'] > 0  && $label['id_common_label'] != 0) {
					$labelModel = LearningLabel::model()->findByPk(array('id_common_label' => $label['id_common_label'], 'lang_code' => $langCode));
					$labels[$label['id_common_label']] = $labelModel;
				}
			}
		}

		// Add no label
		$noLabel = new LearningLabel();
		$noLabel->title = Yii::t('label', 'No label');
		$noLabel->id_common_label = self::LABEL_FILTER_NONE;
		$labels[self::LABEL_FILTER_NONE] = $noLabel;

		return $labels;
	}


	/**
	 * Return full list of labels (returned by the My Courses settings page)
	 *
	 * @return array
	 */
	public static function getLabelsList($addAll=false) {
		$labels = array();
		if ($addAll)
			$labels[self::LABEL_FILTER_ALL] = Yii::t('label', 'All labels');

		$langCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
		$labelsModels = LearningLabel::getLabelsByLang($langCode);
		foreach ($labelsModels as $model)
			$labels[$model->id_common_label] = $model->title;

		$labels[self::LABEL_FILTER_NONE] = Yii::t('label', 'No label');
		return $labels;
	}


	/**
	 * Returns the list of courses
	 * @return array
	 */
	public function getListOfCourses() {
		$paginate = (($this->idLayout > 0) && $this->getParam('paginate_dashlet', false)) || $this->idLayout < 0;
		$currentPage = false;
		$pageCount = false;
		$courses = array(self::LABEL_FILTER_NONE => array());
		$pageSize = (!$this->groupByLabels || ($this->fltLabelId !== 'all')) ? $this->pageSize : false; // Only paginate if one label is selected or label grouping is disabled

		
		// Load all courses in the current page (if any)
		$dp = $this->sqlDataProviderCourses($pageSize);
		if ($pageSize) {
			$dp->pagination->setCurrentPage($this->pageNumber);
			$currentPage = $this->pageNumber;
			$pageCount = $dp->pagination->pageCount;
		}
		// Collect course AR models (FIXME: It would be better to use associative arrays instead)
		foreach ($dp->data as $course) {
			$courseModel = LearningCourse::model()->findByPk($course['idCourse']);

			// Group all (non paginated) courses on a per label basis (if needed)
			if($this->groupByLabels)
				$courseLabelId = !empty($courseModel->learningLabels) ? $courseModel->learningLabels[0]->id_common_label : self::LABEL_FILTER_NONE;
			else
				$courseLabelId = self::LABEL_FILTER_NONE;
				// show all items in My Courses page and $this->pageSize items per label in the dashboard
				//if($this->idLayout < 0 || (!isset($courses[$courseLabelId]) || count($courses[$courseLabelId]) < $this->pageSize))
				$courses[$courseLabelId][] = $courseModel;
		}

		return array(
			'currentPage' 	=> $currentPage,
			'pageCount'		=> $pageCount,
			'courses'		=> $courses
		);
	}

	/**
	 * Return SQL data provider for User's courses (enrolled), filtered, etc.
	 *
	 * @return array  Array of LearningCourse models
	 */
	public function sqlDataProviderCourses($pageSize=false) {

	    /*
	    $args = array(
	        'fltEnrollmentStatus'      => $this->fltEnrollmentStatus,
	        'fltLabelId'               => $this->fltLabelId,
	        'remove_plan_courses'      => $this->getParam('remove_plan_courses', false),
	        'fltSearch'                => $this->fltSearch,
	        'filterObject'             => $this->filterObject,
	        'selected_sorting_fields'  => $this->getParam('selected_sorting_fields'),
	    );
	    // USING NEW STATIC METHOD
	    return LearningCourseuser::sqlDataProviderMyCourses(Yii::app()->user->id, $args, $pageSize);
	    */

		// SQL Parameters
		$params = array();

		// BASE
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from('learning_course t');

		// Filter by enrollment status of the current user
		// By default, we get ALL courses user is enrolled to
		$params[':idUser'] = Yii::app()->user->id;
		$conditionBase = "(enrollment.idCourse=t.idCourse) AND (enrollment.idUser=:idUser)";
		$condition = $conditionBase;
		if ($this->fltEnrollmentStatus) {
			switch ($this->fltEnrollmentStatus) {
				case self::STATUS_ACTIVE:
					$condition = $conditionBase . " AND (enrollment.status != :completed)";
					$params[':completed'] = LearningCourseuser::$COURSE_USER_END;
					break;
				case self::STATUS_COMPLETED:
					$condition = $conditionBase . " AND (enrollment.status = :completed)";
					$params[':completed'] = LearningCourseuser::$COURSE_USER_END;
			}
		}
		$commandBase->join('learning_courseuser enrollment', $condition);

		// do a left join with Course Field Value db table
		$commandBase->leftJoin(LearningCourseFieldValue::model()->tableName() ." lcfv", "lcfv.id_course = t.idCourse  ");

		// Filter by a particular LABEL ID
		if ($this->fltLabelId != 'all') {
			if($this->fltLabelId == 'none') {
				$commandBase->leftJoin('learning_label_course ll', 'll.id_course=t.idCourse');
				$commandBase->andWhere('ll.id_common_label IS NULL');
			} else {
				$commandBase->join('learning_label_course ll', '(ll.id_course=t.idCourse) AND (ll.id_common_label=:labelId)');
				$params[':labelId'] = $this->fltLabelId;
			}
		}

		// If we are asked to, select only courses NOT being part of a learning plan (left join + id_path = NULL)
		if ($this->getParam('remove_plan_courses', false)) {
			$user_learning_plan = LearningCoursepathUser::getEnrolledLearningPlanIdsByUser(Yii::app()->user->id);
			if(!empty($user_learning_plan))
			{
				//Reoving the coursepath courses
				$courses =	Yii::app()->db->createCommand()
							->select("id_item")
							->from(LearningCoursepathCourses::model()->tableName())
							->where(array('in', 'id_path', $user_learning_plan))
							->queryColumn();

				if(!empty($courses))
					$commandBase->andWhere('t.idCourse  NOT IN (' .implode(',', $courses).  ')');
			}
		}

		// Search a string in the course name
		if ($this->fltSearch) {
			$commandBase->andWhere('CONCAT(t.name, " ", t.description, " ", t.code) LIKE :search');
			$params[':search'] = '%'.str_replace(' ', '%', $this->fltSearch).'%';
		}

		if(!Yii::app()->user->getIsGodadmin() && !Yii::app()->user->getIsAdmin()){
			$allowedCoursesIds = Docebo::learnerNotAllowedCoursesIds();
			if($allowedCoursesIds && is_array($allowedCoursesIds)){
				$commandBase->andWhere('t.idCourse  NOT IN (' .implode(',', $allowedCoursesIds).  ')');
			}
		}

		// Set filter on course type
		$allowedTypes = array(LearningCourse::TYPE_ELEARNING, LearningCourse::TYPE_WEBINAR);
		if (PluginManager::isPluginActive('ClassroomApp'))
			$allowedTypes[] = LearningCourse::TYPE_CLASSROOM;

		// Let custom plugins add their own custom course types
		$_customTypesList = array();
		Yii::app()->event->raise('CollectCustomCourseTypes', new DEvent($this, array('types' => &$_customTypesList)));
		if(!empty($_customTypesList))
			$allowedTypes = array_merge($allowedTypes, array_keys($_customTypesList));

		// Filter on course types
		$commandBase->andWhere("t.course_type IN ('".implode("','",$allowedTypes)."')");

		// If there is any filter by course fields to be applied
		if(is_array($this->filterObject->courseFields) && count($this->filterObject->courseFields) > 0) {
			foreach($this->filterObject->courseFields AS $filterItemId => $filterItem) {
				if (!empty($filterItem['type'])) {
					switch ($filterItem['type']) {
						case 'dropdown':
							if (!empty($filterItem['value'])) {
								$commandBase->andWhere('lcfv.field_' . $filterItemId . ' =' . addslashes($filterItem['value']));
							}
							break;

						case 'date':
							if (!empty($filterItem['value'])) {
								$commandBase->andWhere('lcfv.field_' . $filterItemId . $filterItem['condition'] . " '" .  Yii::app()->localtime->fromLocalDate($filterItem['value']) . "' ");
							}

							break;

						case 'textfield':
							if (!empty($filterItem['value'])) {
								switch($filterItem['condition']) {
									case 'contains':
										$commandBase->andWhere('lcfv.field_' . $filterItemId . " LIKE '%" . addslashes($filterItem['value']) ."%' ");
										break;

									case 'equal':
										$commandBase->andWhere('lcfv.field_' . $filterItemId . " = '" . addslashes($filterItem['value']) ."' ");
										break;

									case 'not-equal':
										$commandBase->andWhere("COALESCE(lcfv.field_" . $filterItemId . ", '') != '" . addslashes($filterItem['value']) . "' ");
										break;

									default:
										break;
								}
							}

							break;

						case 'textarea':
							if (!empty($filterItem['value'])) {
								switch($filterItem['condition']) {
									case 'contains':
										$commandBase->andWhere('lcfv.field_' . $filterItemId . " LIKE '%" . addslashes($filterItem['value']) ."%' ");
										break;

									case 'not-contains':
										$commandBase->andWhere("COALESCE(lcfv.field_" . $filterItemId . ", '') NOT LIKE '%" . addslashes($filterItem['value']) . "%' ");
										break;

									default:
										break;
								}
							}

							break;
						default:
							// Not an internal field type -> instantiate the custom field handler and let it process the case
							$fieldName = 'CourseField'.ucfirst($filterItem['type']);
							if(method_exists($fieldName, "applyMyCoursesFilter"))
								$fieldName::applyMyCoursesFilter($filterItemId, $filterItem['value'], $commandBase);
					}
				}
			}
		}


		// DATA
		$commandData = clone $commandBase;

		// COUNTER
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(t.idCourse)');
		$numRecords = $commandCounter->queryScalar($params);

		// ORDERING
		$sortingFields = $this->getParam('selected_sorting_fields');
		if (!empty($sortingFields))
			$commandData->order = implode(',', $sortingFields);
		else
			$commandData->order = 'name';

		// DATA-II
		$commandData->select("*");

		if ($pageSize == false) {
			$pagination = false;
		}
		else {
			$pagination = new CPagination();
			// We MUST set item count here!!!! The object will calculate page count for us
			$pagination->itemCount = $numRecords;
			$pagination->pageSize = $pageSize;
		}

		$config = array(
			'totalItemCount'	=> $numRecords,
			'pagination' 		=> $pagination,
			'keyField'			=> 'idCourse',
			'params'			=> $params,
		);

		// Pass dbCommand, not SQL text, to apply parameters binding !!!
		$dataProvider = new CSqlDataProvider($commandData, $config);
		return $dataProvider;
	}


	/**
	 * @see DashletWidget::init()
	 */
	public function init() {
		parent::init();

		// Collect filtering information form Request, if any (in which case, set them to "initial" ones from settings, if any)
		$this->fltEnrollmentStatus 	= Yii::app()->request->getParam('enrollment_status', $this->getParam('initial_status_filter', DashletMyCourses::STATUS_ALL));
		$this->fltSearch			= Yii::app()->request->getParam('search_input', false);
		$this->fltLabelId 			= Yii::app()->request->getParam('label_id', $this->getParam('initial_label_filter', 'all'));
		$this->groupByLabels 		= $this->getParam('aggregate_labels', false) && PluginManager::isPluginActive("LabelApp");

		// Get filter by course fields if set
		$advancedSearchAdditionalFields = Yii::app()->request->getParam('advancedSearch', array());

		// Init the filter object
		$this->filterObject = new stdClass();

		// Intercepting of the incoming filter data to be used inside the sqlDataProviderCourses
		if (!empty($advancedSearchAdditionalFields['additional']) && is_array($advancedSearchAdditionalFields['additional'])
			&& count($advancedSearchAdditionalFields['additional']) > 0) {
			foreach ($advancedSearchAdditionalFields['additional'] as $courseFieldKey => $advancedSearchAdditionalField) {
				if (!empty($advancedSearchAdditionalField)) {
					$this->filterObject->courseFields[$courseFieldKey] = $advancedSearchAdditionalField;
				}
			}
		}

		// Page size and Page number
		if ($this->idLayout < 0)
			$this->pageSize	= Yii::app()->request->getParam('psize', $this->getParam('display_number_items_fullist', false));
		else
			$this->pageSize	= Yii::app()->request->getParam('psize', $this->getParam('display_number_items', false));

		$this->pageNumber = Yii::app()->request->getParam('pnumber', 0);
	}

	/**
	 *
	 */
	public static function descriptor($params=false) {
		$sampleImageUrl = Yii::app()->mydashboard->getModule()->getAssetsUrl() . "/images/mycourses_sample.jpg";

		$descriptor = new DashletDescriptor();

		$descriptor->name 					= 'core_dashlet_mycourses';
		$descriptor->handler				= self::$handler;
		$descriptor->title					= Yii::t('menu_over', '_MYCOURSES');
		$descriptor->description			= Yii::t('dashlets', 'Lists all courses and learning plans the user is enrolled in');
		$descriptor->sampleImageUrl 		= $sampleImageUrl;
		$descriptor->settingsFieldNames		= array(
			'initial_display_style',
			'initial_status_filter',
			'initial_label_filter',
			'show_filters_button',
			'show_display_style_switch',
			'show_search_bar',
			'aggregate_labels',
			'display_number_items',
			'display_number_items_fullist',
			'show_view_all_link',
			'remove_plan_courses',
            'show_course_deadline',
			'selected_sorting_fields',
			'paginate_dashlet',
		);

		return $descriptor;
	}

	/**
	 * @see IDashletWidget::renderSettings()
	 */
	public function renderSettings() {
		$cs = Yii::app()->getClientScript();
		$themeUrl = Yii::app()->theme->baseUrl;

		// FCBK
		$cs->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
		$cs->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');

		$this->render('mycourses/settings');
	}

	/**
	 * @see IDashletWidget::renderFrontEnd()
	 */
	public function renderFrontEnd() {
		if ($this->dashletModel) {

			// Load list of courses (grouped per label, depending on the settings & filters)
			$coursesData = $this->getListOfCourses();

			// Initial display style... however, cookies are overriding
			$displayStyle = $this->getParam('initial_display_style', DashletMyCourses::DISPLAY_STYLE_THUMB);
			$cookieName = 'dashlet_display_style_' . $this->dashletModel->id;
			if(isset(Yii::app()->request->cookies[$cookieName]))
				$displayStyle = Yii::app()->request->cookies[$cookieName];
			$displayStyleSwitchIcon = ($displayStyle == "list") ? "fa-th" : "fa-list";
			$thumbClass =  ($displayStyle == "list") ? "" : "tb";

			// Get some On/Off settings from dashlet instance parameters
			$showViewAllCourses 	= $this->getParam('show_view_all_link', false);
			$showFiltersButton 		= $this->getParam('show_filters_button', true);
			$showSearchBar 			= $this->getParam('show_search_bar', true);
			$showDisplayStyleSwitch = $this->getParam('show_display_style_switch', true);
			$showLabelsSelector 	= PluginManager::isPluginActive("LabelApp");

			// sclo =>  "single cell layout", one of the "virtual laouts types"
			$viewAllCoursesUrl 	= Docebo::createLmsUrl('site/index', array('sclo' => 1, 'id' => $this->dashletModel->id));

			// Get additional course fields data here
			$additionalCourseFieldsModels = LearningCourse::model()->getCourseAdditionalFields(false, true);

			$params = array (
				'displayStyleSwitchIcon' => $displayStyleSwitchIcon,
				'thumbClass' => $thumbClass,
				'hasFilterHeader' => $showFiltersButton || $showDisplayStyleSwitch || $showSearchBar,
				'showFiltersButton' => $showFiltersButton,
				'showSearchBar' => $showSearchBar,
				'showDisplayStyleSwitch' => $showDisplayStyleSwitch,
				'showViewAllCourses' => $showViewAllCourses,
				'showLabelsSelector' => $showLabelsSelector,
				'viewAllCoursesUrl' => $viewAllCoursesUrl,
				// Follow "paginate dashlet" setting for "real layouts" and force paginate in "virtual layouts" (negative IDs)
				'paginate' => (($this->idLayout > 0) && $this->getParam('paginate_dashlet', false)) || $this->idLayout < 0,
				'pageCount' => $coursesData['pageCount'],
				'currentPage' => $coursesData['currentPage'],
				'userLabels' => $this->getUserLabelsList(true),
				'courses' => $coursesData['courses'],
				'additionalCourseFieldsModels' => $additionalCourseFieldsModels
			);

			$this->render('mycourses/index', $params);
		}
		else
			echo "";
	}

	/**
	 * @see IDashletWidget::bootstrap()
	 */
	public function bootstrap($params=false) {
		if (Yii::app()->user->getIsGodadmin()) {
			$assetsUrl = Yii::app()->theme->baseUrl;
			/* @var CClientScript $cs */
			$cs = Yii::app()->getClientScript();
			$cs->registerCssFile($assetsUrl . '/js/bootstro/bootstro.css');
			$cs->registerScriptFile($assetsUrl . '/js/bootstro/bootstro.js', CClientScript::POS_END);
		}
	}

	/**
	 * Return available sorting fields and their labels for Settings UI
	 *
	 * @return array
	 */
	public static function getAvailableSortingFields(){
		return array(
			't.name'						=> Yii::t('standard', '_COURSE_NAME').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			't.name DESC'					=> Yii::t('standard', '_COURSE_NAME').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
			'enrollment.status'				=> Yii::t('standard', '_STATUS').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			'enrollment.status DESC' 		=> Yii::t('standard', '_STATUS').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
			't.create_date'					=> Yii::t('report', '_CREATION_DATE').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			't.create_date DESC' 			=> Yii::t('report', '_CREATION_DATE').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
			'enrollment.date_inscr'			=> Yii::t('report', '_DATE_INSCR').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			'enrollment.date_inscr DESC' 	=> Yii::t('report', '_DATE_INSCR').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
			't.code'						=> Yii::t('standard', '_CODE').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			't.code DESC'					=> Yii::t('standard', '_CODE').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
		);
	}


	/**
	 * Try to find a dashlet having DashletMyCourses handler and NOT being part of any layout, that is our dashlet by definition for FULL MY COURSES
	 *
	 * @return DashboardDashlet
	 */
	public static function getFullMyCoursesDashlet() {
		$model = DashboardDashlet::model()->findByAttributes(array('handler' => self::$handler), 'cell IS NULL');
		// If there is no such, create one on the fly and use it
		if (!$model) {
			$model = DashboardDashlet::createFullMyCoursesDashlet();
		}
		return $model;
	}

	/**
	 * Return default dashlet parameters, if someone needs them
	 *
	 * @return array
	 */
	public static function getDefaultParameters() {
		return array(
			"initial_display_style" 		=> "thumb",
			'initial_status_filter'			=> "all",
			'initial_label_filter'			=> "all",
			'aggregate_labels'				=> PluginManager::isPluginActive('LabelApp'),
			'remove_plan_courses'			=> "0",
            'show_course_deadline'          => "0",
			"show_filters_button"			=> "1",
			"show_display_style_switch"		=> "0",
			"show_search_bar"				=> "1",
			"display_number_items" 			=> "3",
			"display_number_items_fullist" 	=> Settings::get('elements_per_page', false),
			"show_view_all_link"			=> "0",
			"selected_sorting_fields"		=> false,
			"paginate_dashlet"				=> true,
		);
	}




}


