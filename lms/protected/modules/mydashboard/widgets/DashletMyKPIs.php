<?php

class DashletMyKPIs extends DashletWidget implements IDashletWidget {

	protected static $kpiList;
	
	/**
	 * Dashlet descriptor
	 *
	 * @return DashletDescriptor Object, describing the dashlet to the outside world
	 */
	public static function descriptor( $params = FALSE ) {
		$sampleImageUrl = Yii::app()->mydashboard->getModule()->getAssetsUrl() . "/images/mykpi_sample.jpg";

		$descriptor = new DashletDescriptor();

		$descriptor->name               = 'core_dashlet_mykpis';
		$descriptor->handler            = 'mydashboard.widgets.DashletMyKPIs';
		$descriptor->title              = Yii::t('dashlets', 'My KPIs');
		$descriptor->description        = Yii::t('dashlets', 'Shows a set of configurable statistics about the currently logged in user');
		$descriptor->sampleImageUrl     = $sampleImageUrl;
		$descriptor->settingsFieldNames = array_merge(array('field_order'), array_keys(self::getKPIList()));

		return $descriptor;
	}

	/**
	 * Register any assets here (e.g. scripts or styles)
	 */
	public function bootstrap($params=false){
		Yii::app()->event->raise('RegisterClientResources', new DEvent($this));
		Yii::app()->clientScript->registerScriptFile(Yii::app()->getModule('player')->getAssetsUrl() . "/js/jquery.knob.js");
		$cs = Yii::app()->clientScript;
		$am = Yii::app()->assetManager;
		$cs->registerScriptFile($am->publish(Yii::getPathOfAlias('mydashboard.assets')) . '/js/kpi_dashlets_helper.js');
	}

	/**
	 * Render dashlet settings, used in Admin Dashlets UI
	 */
	public function renderSettings() {
		$tmpAvailKPIs = self::getKPIList();

		$orderedSettings = array();

		// If editing a dashlet, restore custom ordering of fields, if set
		$fieldsOrdered = explode(',', $this->getParam('field_order'));
		if(!empty($fieldsOrdered)){
			foreach($fieldsOrdered as $kpi){
				if(in_array($kpi, array_keys($tmpAvailKPIs))){
					$orderedSettings[$kpi] = $tmpAvailKPIs[$kpi];
				}
			}
		}
		// Finally, add all settings not yet added by the ordered adding above.
		// On initial dashlet creation this array will just be populated with
		// the original settings in their original order
		foreach($tmpAvailKPIs as $setting=>$label){
			if(!in_array($setting, array_keys($orderedSettings)))
				$orderedSettings[$setting] = $label;
		}

		$this->render( 'dashlet_mykpis_settings', array(
			'settings'=>$orderedSettings,
		));
	}

	/**
	 * Render fronend content
	 */
	public function renderFrontEnd() {

		$fieldsOrdered = explode(',', $this->getParam('field_order'));

		if(empty($fieldsOrdered)){
			// Get default order
			$fieldsOrdered = array_keys(self::getKPIList());
		}

		// Holds the HTML for each block to be rendered in the dashlet
		// Since blocks may be rendered side by side or in a single column
		// vertically depending on the dashlet cell layout (one half, one third, etc)
		// This is why we are building a reusable array, which can be easily split in
		// chunks before rendering when needed
		$blocks = array();

		foreach($fieldsOrdered as $kpi){

			if(!in_array($kpi, array_keys(self::getKPIList())))
				continue; // This KPI is not available in the platform

			if(!$this->getParam($kpi))
				continue; // KPI not enabled in dashlet configuration screen

			$blocks[] = array(
				'dataUrl'=>Docebo::createLmsUrl('//mydashboard/dashletWidgets/axGetMyKPIFrontendBlock', array('kpi'=>$kpi))
			);
		}

		$this->render('dashletmykpis', array(
			'blocks'=>$blocks,
		));

	}

	/**
	 * Returns list of KPIs 
	 *
	 * @return array
	 */
	public static function getKPIList() {
		
		if (!empty(self::$kpiList)) {
			return self::$kpiList;
		}
		
		$list =  array(
			'last_elearning_progress'            => Yii::t( 'dashlets', 'Last E-Learning course progress' ),
			'last_completed_elearning'           => Yii::t( 'dashlets', 'Last completed E-Learning course' ),
			'last_published_course'              => Yii::t( 'dashlets', 'Last published course' ),
			'best_elearning_score'               => Yii::t( 'dashlets', 'Best score on E-Learning' ),
		);

		$kips = self::collectCustomKPIs();
		if (is_array($kips)) {
			foreach ($kips as $kpi => $label) {
				$list[$kpi] = $label;
			}
		}
		
		self::$kpiList = $list;
		
		return $list;
	}
	
	/**
	 * Collect Custom MyKPIs from whoever listens for the given event  (e.g. a plugin)
	 * The event listener must return and array of id => label 
	 * 
	 * @return array
	 */
	public static function collectCustomKPIs() {
		
		// Raise an event to collect Custom MyKPI's
		$kpis = array();
		Yii::app()->event->raise('CollectCustomMyKPIs', new DEvent(self, array(
			'kpis' => &$kpis
		)));
		return $kpis;
	}
	
}
