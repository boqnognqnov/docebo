<div class="w-calendar calendar-listview">
	<ul>
	<?php foreach($allItems as $item): ?>

		<li class="calendar-event">

			<span class="calendar-type calendar-type-<?=$item['type']?>">
				<? switch($item['type']){
					case 'elearning':
						echo Yii::t('standard', '_ELEARNING');
						break;
					case 'classroom':
						echo Yii::t('standard', '_CLASSROOM');
						break;
					case 'webinar':
					case 'conference':
						echo Yii::t('standard', '_VIDEOCONFERENCE');
						break;
				} ?>
			</span>

			<span class="calendar-date pull-right"><?= date('Y-m-d', $item['timestamp']); ?></span>

			<h3><?= $item['name'] ?></h3>

		</li>

	<?php endforeach; ?>
	</ul>
</div>

<? if($show_my_calendar_link): ?>
	<div class="dl-footer">
		<div class="span6 dl-footer-left text-left"></div>
		<div class="span6 dl-footer-right text-right"><a href="<?=Docebo::createLmsUrl('mycalendar/show', array('sop'=>'unregistercourse'))?>"><?=Yii::t('calendar', 'My Calendar')?></a></div>
		<div class="clearfix"></div>
	</div>
<? endif; ?>