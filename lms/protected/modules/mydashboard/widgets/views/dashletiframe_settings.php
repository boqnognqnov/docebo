<?php

// Get array with html contents per different languages
$iframeArr = $this->getParam('iframe_url');

if(empty($iframeArr) || !is_array($iframeArr)) {
	$iframeArr = array();
}

// Get all active languages
$langs = Lang::getLanguages(true);

// If there are missing translations for some languages
// Disabled since in certain cases the translations are >= the active languages
//if (count($langs) > count($iframeArr)) {
	foreach($langs as $lang) {
		if (!array_key_exists($lang['code'], $iframeArr)) {
			$iframeArr[$lang['code']] = '';
		}
	}
//}

// Generate all hidden temp iframe inputs
if(is_array($iframeArr) && count($iframeArr) > 0) {
	foreach($iframeArr as $key => $row) {
		echo CHtml::hiddenField('iframe_url[' . $key . ']', $row, array('id' => 'iframe-' . $key));
	}
}

// Encode the array to JSON in order to be used in the JS area
$iframeArr = CJSON::encode($iframeArr);

?>

<div class="row-fluid">
	<div class="control-group">
		<label class="control-label" for="iframe_url"><?=Yii::t('dashboard', 'URL of the HTML page to display')?></label>
		<div class="controls">
           	<div class="row-fluid">
               	<?= CHtml::textField('iframe', $this->params['iframe_url'][$currentLang], array('class' => 'span12')) ?>
           	</div>
        </div>
    </div>
</div>

<script>

	var iframeArr = [];

	$(function(){

		// Array with HTML area content for each languages
		iframeArr = <?= $iframeArr; ?>;

		// Current language
		var currentLang = $('#lang_code').val();

		// Text area unique id per this dashlet
		var htmlId = '#iframe';

		// Set textarea according to the currently selected language
		$(htmlId).val(iframeArr[currentLang]);

		// On language change, set proper content in the title and html input/textarea
		$('#lang_code').on('change', function() {
			iframeArr[currentLang] = $(htmlId).val();

			currentLang = $('#lang_code').val();
			$(htmlId).val(iframeArr[currentLang]);
		});


		/**
		 * Show languages, that are populated
		 */
		function showPopulatedLangs2() {
			$("#lang_code > option").each(function(key, value) {
				var optionText = this.text;
				this.text = optionText.replace('* ', '');

				if(titleArr[this.value].replace(/\s+/, '') != '' || (iframeArr[this.value].replace(/\s+/, '') != '')) {
					this.text = '* ' + this.text;
				}
			});
		}

		function getAssignedLangsCount2(titleArr) {
			var count = 0;
			$.each(titleArr, function(key, value) {
				if(value.replace(/\s+/, '') != '' || (iframeArr[key].replace(/\s+/, '') != '')) {
					count++;
				}
			});

			// Mark populated languages with asterisks
			showPopulatedLangs2();

			return count;
		}

		$('#iframe').on('keyup', function() {
			currentLang = $('#lang_code').val();
			iframeArr[currentLang] = $('#iframe').val();
			$('input[name="iframe_url['+currentLang+']"]').val($('#iframe').val());

			var assignedLangsCount = getAssignedLangsCount2(titleArr);
			$('div.dashlet-settings-html div.lang-wrapper p.assigned span').html(assignedLangsCount);
		});

		setTimeout(function() {
			var assignedLangsCount = getAssignedLangsCount2(titleArr);
			$('div.dashlet-settings-html div.lang-wrapper p.assigned span').html(assignedLangsCount);
		}, 1000);

		// Prevent default event handling
		// On typing the title update the hidden fields and titles array
		$('#dashlet_instance_title').off('keyup').on('keyup', function() {
			currentLang = $('#lang_code').val();

			titleArr[currentLang] = $('#dashlet_instance_title').val();
			$('input[name="headers['+currentLang+']"]').val($('#dashlet_instance_title').val());

			var assignedLangsCount = getAssignedLangsCount2(titleArr);
			$('div.dashlet-settings-html div.lang-wrapper p.assigned span').html(assignedLangsCount);
		});

	});

</script>