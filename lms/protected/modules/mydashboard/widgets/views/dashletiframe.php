<?php
	$tmpId =  "tmp-" . time();
?>
<span id="<?= $tmpId ?>"></span>
<?php if ($this->params['iframe_url']) : ?>
	<?php
		$style = "";
		if ((int) $this->dashletModel['height'] > 0) {
			$haveHeight = true; 
			$style  = "height: " . $this->dashletModel['height'] . "px;";  
		}

	$iframeSrcs = $this->params['iframe_url'];
	$iframeSrc = '';
	if(!empty($iframeSrcs[$currentLang])) {
		$iframeSrc = $iframeSrcs[$currentLang];
	} elseif ($currentLang != $defaulLang) {
		$iframeSrc = $iframeSrcs[$defaulLang];
	}

	?>
	<iframe style="width: 100%;<?= $style ?>" src="<?= $iframeSrc ?>"></iframe>
<?php endif; ?>


<!-- Use .dl-footer as a special class. dashboard plugin will put the WHOLE element in the footer zone -->
<div class="dl-footer">
	<div class="span6 dl-footer-left 	text-left"></div>
	<div class="span6 dl-footer-right 	text-right"></div>
</div>

<script type="text/javascript">
	$(function(){
		$('iframe').each(function() {
			var url = $(this).attr("src");
			if(url.indexOf('youtube') > 0){
				if (url.indexOf("?") > 0) {
					$(this).attr({
						"src" : url + "&wmode=transparent",
						"wmode" : "opaque"
					});
				}else {
					$(this).attr({
						"src" : url + "?wmode=transparent",
						"wmode" : "opaque"
					});
				}
			}
		});
	}());


	<?php if ($haveHeight) : ?>
		// In case of IFRAME, the dashlets dl-content must not have a height. It should be AUTO, whicle IFRAME is having the required height
		var selector = '#'+ <?= json_encode($tmpId) ?>;
		$('#'+ <?= json_encode($tmpId) ?>).closest('.dl-content').css('height', '');
	<?php endif; ?>

</script>