<div class="row-fluid">

	<h4>This is an example of a dashlet</h4>
	<br>
	<ol>
		<li>Create a widget in lms/protected/modules/mydashboard/widgets, extending DashletWidget class implementing IDashletWidget interface</li>
		<li>Create its view in the "views" folder</li>
		<li>Edit/change widget and view on your discretion</li>
		<li>Widget is rendered in DashController/actionLoadDashletContent</li>
		<li>The only default parameter passed to the widget is the Dashlet model (object)</li>
		<li>Widget's Protected attribute "params" is an array loaded from dashelt model -> params and is available in the view</li>
	</ol>
</div>


<div class="row-fluid">
	<div class="span4"><strong>Row ID <?= $this->dashletModel->dashboardCell->dashboardRow->id ?></strong></div>
	<div class="span4"><strong>Cell ID <?= $this->dashletModel->dashboardCell->id ?></strong></div>
	<div class="span4"><strong>Position ID <?= $this->dashletModel->position ?></strong></div>
</div>
<div class="row-fluid">
	<div class="span4"><strong>Dashlet ID <?= $this->dashletModel->id ?></strong></div>
	<div class="span4"><strong>Dashlet Height <?= $this->dashletModel->height ?></strong></div>
	<div class="span4"></div>
</div>


<div class="row-fluid">
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
</div>





<!-- IMPORTANT FEATURE -->
<!-- Use .dl-footer as a special class. dashboard plugin will put the WHOLE element in the footer zone -->
<div class="dl-footer">
	<div class="span6 dl-footer-left 	text-left"></div>
	<div class="span6 dl-footer-right 	text-right"><a class="open-dialog">View something</a></div>
</div>


<script type="text/javascript">
/*<![CDATA[*/

    // Example event listener: listen for when THIS dashlet instance is loaded (by ID)
	$(document).on('mydashboard.dashlet.content-loaded','.dashlet[data-dashlet-id="<?= json_encode((int) $this->dashletModel->id) ?>"]', function(e, data){
    }); 

           
/*]]>*/
</script>
           
