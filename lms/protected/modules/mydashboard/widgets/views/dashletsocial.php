<?
/* @var $activeServices array */
/* @var $availableServices array */
/* @var $this DashletSocial */
?>
<style type="text/css">
	.dashlet-share-icon{
		width: 50px;
		float: left;
		padding: 10px 5px 10px 5px;
		margin-right: 10px;
	}
	.dashlet-share-icon .icon{
		width: 36px;
		height: 36px;
		background: url('<?=Yii::app()->mydashboard->getModule()->getAssetsUrl()?>/images/social.png') no-repeat;
	}
	.icon.facebook{
		 background-position: 0 0;
	}
	.icon.twitter{
		background-position: -80px 0;
	}
	.icon.youtube{
		background-position: -159px 0;
	}
	.icon.linkedin{
		background-position: -239px 0;
	}
	.icon.gplus{
		background-position: -318px 0;
	}
	.icon.instagram{
		background-position: -397px 0;
	}
	.icon.pinterest{
		background-position: -476px 0;
	}
</style>

<div class="clearfix"></div>

<? foreach($activeServices as $serviceCodename): ?>
	<div class="dashlet-share-icon">
		<a target="_blank" rel="tooltip" title="<?=isset($servicesMeta[$serviceCodename]['hover_text']) ? $servicesMeta[$serviceCodename]['hover_text'] : null; ?>" href="<?=isset($servicesMeta[$serviceCodename]['url']) ? $servicesMeta[$serviceCodename]['url'] : null;?>">
			<?=$this->getServiceIcon($serviceCodename)?>
		</a>
	</div>
<? endforeach; ?>

<div class="clearfix"></div>

<!-- Use .dl-footer as a special class. dashboard plugin will put the WHOLE element in the footer zone -->
<div class="dl-footer">
	<div class="span6 dl-footer-left 	text-left"></div>
	<div class="span6 dl-footer-right 	text-right"></div>
</div>

