<p><?=Yii::t('social', 'Add the following social networks')?></p>
<div id="selected-services">
	<select name="active_services" multiple="multiple">
		<? if(count($previousActiveServicesToRestore)): ?>
			<? foreach($previousActiveServicesToRestore as $key=>$value): ?>
				<option selected="selected" class="selected" value="<?=$key?>"><?=$value?></option>
			<? endforeach; ?>
		<? endif; ?>
	</select>
</div>

<br/>

<div class="social-services-wrapper">
	<? foreach($availableServices as $serviceCodename=>$serviceMeta): ?>
		<div class="single-service <?=$serviceCodename?>">
			<p>
				<?=Yii::t('social', '{service} URL', array('{service}'=>$serviceMeta['label']))?>
			</p>
			<?=CHtml::textField("services_meta[{$serviceCodename}][url]", (isset($previousServiceMetas[$serviceCodename]['url']) ? $previousServiceMetas[$serviceCodename]['url'] : null), array(
				'placeholder'=>'http://'
			))?>
			<p>
				<?=Yii::t('social', '{service} hover text', array('{service}'=>$serviceMeta['label']))?>
			</p>

			<?=CHtml::textField("services_meta[{$serviceCodename}][hover_text]", (isset($previousServiceMetas[$serviceCodename]['hover_text']) ? $previousServiceMetas[$serviceCodename]['hover_text'] : null))?>
		</div>
	<? endforeach; ?>
</div>

<br/>

<script type="text/javascript">
	$(function(){
		$('input').styler();

		var selectElem = $("select", "#selected-services");

		selectElem.fcbkcomplete({
			json_url: '<?=Docebo::createAbsoluteLmsUrl('//mydashboard/DashletWidgets/axAvailableSocialServicesAutocomplete')?>',
			addontab: false, // <==buggy
			width: '98.5%',
			cache: true,
			complete_text: '',
			maxshownitems: 5,
			input_min_size: -1,
			input_name: 'maininput-name',
			filter_selected: true,
			onselect : function(){
				console.log(arguments);
			}
		});

		$("#selected-services").on('change', 'select', function(){
			// Update the visibility of the "meta" fields (URL, hover text)
			// if an item is added/removed in the typeahead
			updateFields();
		});

		var updateFields = function(){
			var selectedSocialServices = $("#selected-services").find("select option");

			// Remove all services
			$('.social-services-wrapper .single-service').hide();

			selectedSocialServices.each(function(){
				var serviceCodename = $(this).val();
				console.log(serviceCodename);

				$('.social-services-wrapper .single-service.'+serviceCodename).show();
			});
		};

		// do it also on page load
		updateFields();
	});
</script>