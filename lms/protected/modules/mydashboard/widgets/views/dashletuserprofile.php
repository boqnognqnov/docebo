<style type="text/css">
	.profile-block .main-block{
		margin-top: 10px;
		margin-bottom: 10px;
	}
	.profile-block .main-block p{
		margin-bottom: 5px;
	}
	.profile-block .role{
		color: #3a87ad;
	}
	.profile-block .name{
		font-size: 22px;
		font-weight: bold;
	}
	.profile-block .avatar{
		max-width: 40px;
		margin-right: 10px;
		margin-left: 5px;
		margin-top: 2px;
		position: relative;
		border: 1px solid #e4e6e5;
	}
	.profile-block .additional-fields{
		border-top: 1px solid #CCCCCC;
		padding-top: 10px;
		padding-bottom: 10px;
	}
</style>

<div class="profile-block">

	<div class="main-block">

		<? if($showUserProfilePic && $avatarUrl): ?>
			<div class="pull-left">
				<div class="avatar">
					<img src="<?=$avatarUrl?>" class="avatar-img"/>

					<a class="open-dialog" data-dialog-class="modal-change-avatar" href="<?= Docebo::createAppUrl('lms:user/changeAvatar') ?>" title="<?php echo Yii::t('menu_over', '_CHANGE_AVATAR'); ?>">
						<img src="<?=Yii::app()->theme->baseUrl?>/images/standard/icon_edit.png" style="display: none; position: absolute; left: 40%; top: 35%;" class="edit-icon"/>
					</a>
				</div>
			</div>
			<script>
				$(function(){
					$('#dashlet-<?=intval($this->dashletModel->id)?>').find('.avatar').hover(function(){
						$('.avatar-img', this).css('opacity', '0.2');

						$('.edit-icon', this).show();
					}, function(){
						$('.avatar-img', this).css('opacity', 1);

						$('.edit-icon', this).hide();
					});
				});
			</script>
		<? endif; ?>

		<div class="pull-left">

			<p class="name"><?=$userModel->getFullName()?></p>

			<? if($showRole): ?>
				<p class="role"><?=$roleLabel?></p>
			<? endif; ?>
			<? if($showEmail): ?>
				<p><?=$userModel->email?></p>
			<? endif; ?>

		</div>

		<div class="clearfix"></div>
		
		
		

    
	<? if(!empty($additionalFields)): ?>
		<div class="additional-fields">
			<? foreach($additionalFields as $fieldTranslation=>$fieldUserEntryFormatted): ?>
				<div class="row-fluid">
					<div class="span5">
						<?=rtrim(CHtml::encode($fieldTranslation), ':')?>:
					</div>
					<div class="span7">
						<?=$fieldUserEntryFormatted?>
					</div>
				</div>
			<? endforeach; ?>
		</div>
	<? endif; ?>
</div>

<!-- Use .dl-footer as a special class. dashboard plugin will put the WHOLE element in the footer zone -->
<div class="dl-footer">
	<div class="span6 dl-footer-left 	text-left"></div>
	<div class="span6 dl-footer-right 	text-right">
		<?php if(Settings::get('profile_only_pwd', 'on') == 'on'): ?>
			<?php echo CHtml::link(
				'<span></span>'.Yii::t('profile', '_CHANGEPASSWORD'), Docebo::createLmsUrl('user/editProfile'), array(
					'class' => 'change-password',
					'alt' => Yii::t('profile', '_CHANGEPASSWORD'),
					'title' => Yii::t('profile', '_CHANGEPASSWORD'),
					'data-edit' => Yii::app()->legacyWrapper->hydraFrontendEnabled() ? 'my-profile' : 'userprofile'
				)); ?>
		<?php else : ?>
			<?php echo CHtml::link(
				'<span></span>'.Yii::t('standard', '_VIEW_PROFILE'), Docebo::createLmsUrl('user/editProfile'), array(
					'class' => 'change-password',
					'alt' => Yii::t('standard', '_VIEW_PROFILE'),
					'title' => Yii::t('standard', '_VIEW_PROFILE'),
					'data-edit' => Yii::app()->legacyWrapper->hydraFrontendEnabled() ? 'my-profile' : 'userprofile'
				)); ?>
		<?php endif; ?>
		<?php if($showMyActivitiesLink): ?>
			|&nbsp;<a href="<?=Docebo::createLmsUrl('myActivities/index')?>"><?=Yii::t('standard', 'Go to') .' '. Yii::t('course', 'My Activities')?></a>
		<?php endif; ?>
	</div>
	<div class="clearfix"></div>
</div>