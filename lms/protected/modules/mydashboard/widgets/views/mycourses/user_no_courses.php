<div class="mycourses-blank-slate-container">
	<div class="row-fluid">
		<div class="span6">
			<?php
			$overlayHint = null;
			switch($enrollmentStatus){
				case DashletMyCourses::STATUS_ALL:
					$overlayHint = Yii::t('overlay_hints', 'It looks like you have no coures available yet!');
					break;
				case DashletMyCourses::STATUS_ACTIVE:
					$overlayHint = Yii::t('overlay_hints', 'You do not have any courses to complete!');;
					break;
				case DashletMyCourses::STATUS_COMPLETED:
					$overlayHint = Yii::t('overlay_hints', 'You have yet to complete any courses!');
					break;
				default:
					$overlayHint = Yii::t('overlay_hints', 'It looks like you have no coures available yet!');
			}
			?>
			<h2 class="text-left"><?= $overlayHint; ?></h2>
			<br/>
			
			<p>
			<?php if ($showCatalogHint) {
				echo Yii::t('overlay_hints', 'You may want to take a look at your catalog', array('{catalog-link}' => Yii::app()->controller->createUrl('index', array('opt'=>'catalog'))) );
			} else {
				echo Yii::t('overlay_hints', 'Try again later');
			} ?>
			</p>
		</div>
	</div>
</div>