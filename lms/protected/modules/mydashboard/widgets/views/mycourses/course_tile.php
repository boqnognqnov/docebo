<?php
	/* @var $course LearningCourse */

	$playUrl = Docebo::createLmsUrl("/player", array(
		'course_id'	=> $course->idCourse,
	));
	
	$idUser = Yii::app()->user->id;
	$locked = false;
	$waitingApproval = false;
	$canPlay = false;
	$isCompleted = false;
	$isNew	= false; // meaning, NOT yet started, new to the user
	
	// Get the enrollment of current user to this course
	/* @var $enrollment LearningCourseuser */
	$enrollment = LearningCourseuser::model()->findByAttributes(array(
		'idUser'		=> $idUser,
		'idCourse'		=> $course->idCourse,	
	));
	
	// If user is enrolled...
	if ($enrollment) {
		// Completion status
		if ($enrollment->status == LearningCourseuser::$COURSE_USER_END) {
			$isCompleted = true;
		}
		
		if ($enrollment->status == LearningCourseuser::$COURSE_USER_SUBSCRIBED) {
			$isNew = true;
		}
		
		
		// If user CAN ENTER the course (decide locked and canPlay)
		$canEnterCourse = $enrollment->canEnterCourse();
		//$canEnterCourse['can'] = false;
		//$canEnterCourse['reason'] = 'waiting';
		if ( ! $canEnterCourse['can'] ) {
			$locked = true;
		}
		else {
			$canPlay = true;
		}
	}
	
	
	$actionClass = false;
	if ($isCompleted) {
		$actionClass = "completed";
		$actionIconClass = "fa fa-check-circle-o";
		$actionLabel = Yii::t('standard', '_COMPLETED');
	}
	else if ($canPlay) {
		$actionClass = "play";
		$actionIconClass = "fa fa-play-circle-o";
		$actionLabel = Yii::t('standard', '_PLAY');
	}
	else if ($locked) {
		
		switch ($canEnterCourse['reason']) {
			case 'prerequisites':
				$actionClass = "locked";
				$actionIconClass = "fa fa-lock";
				$lockReason = Yii::t('storage', '_LOCKED');
				$lockReasonText = Yii::t('organization', '_ORGLOCKEDTITLE');
				break;
			case 'waiting':
				$actionClass 		= "waiting";
				$actionIconClass 	= "fa fa-clock-o";
				$lockReason = Yii::t('catalogue', '_WAITING_APPROVAL');
				break;
			case 'user_status':
			case 'course_status':
			case 'subscription_expired':
			case 'subscription_not_started':
			case 'course_date':
			default:
				$actionClass = "locked";
				$actionIconClass = "fa fa-lock";
				$lockReason = Yii::t('storage', '_LOCKED');
				if ($canEnterCourse['reason'] == 'user_status') {
					$statusesList = LearningCourseuser::getStatusesArray();
					$lockReasonText = Yii::t('standard', 'Enrollment status').':<br />'.(isset($statusesList[$enrollment->status]) ? strtolower($statusesList[$enrollment->status]) : '');
				}
				if ($canEnterCourse['reason'] == 'course_status') {
					$statusesList = LearningCourse::statusesList();
					$lockReasonText = Yii::t('standard', 'Status').':<br />'.(isset($statusesList[$course->status]) ? strtolower($statusesList[$course->status]) : '');
				}
				if ($canEnterCourse['reason'] == 'subscription_expired') {
					$lockReasonText = /*Yii::t('standard', 'Enrollment status').':<br />'.strtolower(Yii::t('configuration', 'Expired'));*/Yii::t('subscribe', '_DATE_EXPIRE_VALIDITY').': '.(($canEnterCourse['expiring_in'])? $canEnterCourse['expiring_in'] : (($enrollment->date_expire_validity && $enrollment->date_expire_validity!='0000-00-00 00:00:00')? $enrollment->date_expire_validity : $course->date_end));
				}
				if ($canEnterCourse['reason'] == 'subscription_not_started') {
					$lockReasonText = /*Yii::t('standard', 'Enrollment status').':<br />'.strtolower(Yii::t('standard', '_NOT_STARTED')).'<br />'.*/Yii::t('subscribe', '_DATE_BEGIN_VALIDITY').': '.(($enrollment->date_begin_validity && $enrollment->date_begin_validity!='0000-00-00 00:00:00')? $enrollment->date_begin_validity : $course->date_begin);
				}
				if ($canEnterCourse['reason'] == 'course_date') {
					//$lockReasonText = Yii::t('standard', 'Closed');
					$_tmp = array();
					if (!empty($course->date_begin) && $course->date_begin != '0000-00-00') { $_tmp[] = Yii::t('subscribe', '_DATE_BEGIN_VALIDITY').': '.$course->date_begin; }
					if (!empty($course->date_end) && $course->date_end != '0000-00-00') { $_tmp[] = Yii::t('subscribe', '_DATE_EXPIRE_VALIDITY').': '.$course->date_end; }
					if (!empty($_tmp)) { $lockReasonText = implode('<br />', $_tmp); }
				}
				break;
		}
		$actionLabel = $lockReason;
	}
	else {
		$actionClass = "locked";
		$actionIconClass = "fa fa-lock";
		$lockReason = Yii::t('storage', '_LOCKED');
	}
	
	
	// Resolve how and if the course can be played, enrolled, etc. and return data for labels, icons, classes (presentation data)
	$accessData = $course->resolveCourseAccess();


?>

<div class="course-tile">

	<?php 
		if ($accessData['linkTag']) { 
			echo $accessData['linkTag']; 
		}  
	?>

		<div class="course-logo" id="course-logo-<?= $course->idCourse ?>">
			<div class="thumb">
			
				<!-- Course tags: in thumbnail -->
				<?php if (count($accessData['tags']) > 0): ?>
					<div class="course-tags">
						<?php foreach ($accessData['tags'] as $tag) : ?>
							<span class="course-tag <?= $tag['class'] ?>" rel="tooltip" title="<?= $tag['tooltip'] ?>"><?= $tag['label'] ?></span>
						<?php endforeach;?>
					</div>
				<?php endif; ?>
						
				<?= CHtml::image($course->getCourseLogoUrl(CoreAsset::VARIANT_ORIGINAL, true), $course->name, array()) ?>
			</div>

			<?php if (isset($lockReasonText) && !empty($lockReasonText)): ?>
			<p class="course-action-locked-reason"><?= $lockReasonText ?></p>
			<?php endif; ?>

			<p class="course-action <?= $accessData['action']['class'] ?>">

			
				<!-- Course tags: in action label -->
				<?php if (count($accessData['tags']) > 0): ?>
					<span class="course-tags">
						<?php foreach ($accessData['tags'] as $tag) : ?>
							<span class="course-tag <?= $tag['class'] ?>"><?= $tag['label'] ?></span>
						<?php endforeach;?>
					</span>
				<?php endif; ?>
							
				
				<span class="icon">
					<i class="<?= $accessData['action']['icon'] ?>"></i>
				</span>
				
				<span class="action-label"><?= $accessData['action']['label'] ?></span>
			</p>
			
			<h3 class="course-name"><?= $course->name ?></h3>
		</div>
		<div class="clearfix"></div>
	
	<?php 
		if ($accessData['linkTag']) {
			echo "</a>";
		}	
	?>

	<div class="course-info">
		<?= $course->getCourseTypeTranslation() ?>&nbsp;
		<span class="extra-course-info">
		
			<?php if ($course->lang_code) : ?>
				<a rel="tooltip" title="<?= strtoupper(Lang::getBrowserCodeByCode($course->lang_code)) ?>"><i class="fa fa-globe"></i></a>
			<?php endif; ?>
			
			<?php if ($course->mediumTime) : ?>
				<a rel="tooltip" title="<?= $course->formatAverageCourseTime() ?>"><i class="fa fa-clock-o"></i></a>
			<?php endif; ?>
			
			<?php if ($enrollment && $enrollment->level) : ?>
				<?php $level = Yii::t('levels', '_LEVEL_' . $enrollment->level); ?>
				<a rel="tooltip" title="<?= ucfirst($level) ?>"><i class="fa fa-user"></i></a>
			<?php endif; ?>

		</span>
        <?php if ($this->getParam('show_course_deadline', false) && ($deadline = $course->getDeadlineForUser(Yii::app()->user->id))) : ?>
            <span class="deadline">
				<?php echo Yii::t('course','Deadline').': '.Yii::app()->localtime->stripTimeFromLocalDateTime($deadline); ?>
			</span>
        <?php endif; ?>

		<?php if($course->canViewRating()):?>
			<br />
			<div id="social_rating_course" class="fixed_rating <?php echo $thumbClass == 'tb' ? 'thumb' : 'list' ?>">
				<span class="star-type-rating rating-container" data-idcourse="<?php echo $course->idCourse;?>" >
					<?php
					$rating = $course->getRating();
					for($i=0; $i<5; $i++)
						echo '<div role="text" class="star-rating star-rating'. ($rating > $i ? "-on" : "") .'" id="course-rating-58_'.$i.'"><a title="'.$i.'">'.$i.'</a></div>';
					?>
				</span>
				<span class="rating-value fixed_rating"><?= $course->getRating() ?></span>
			</div>
		<?php endif; ?>

	</div>

</div>
