<?php
/* @var $label LearningLabel */
/* @var $this DashletMyCourses */

$titleAndDescription = $label->getTitleAndDescriptionLanguageSafe();
?>
<div class="clearfix"></div>
<div class="w-mycourses-label row-fluid">
	<div class="label-container">
		<div class="label-icon <?= ($this->fltLabelId !== 'all') ? 'back-to-labels' : '' ?>" style="background-color: <?= $label->color ? $label->color : '#666'?>">
			<i class="fa <?= ($this->fltLabelId !== 'all') ? 'fa-chevron-left' : ($label->icon ? $label->icon : 'fa-bars') ?> fa-2x fa-inverse"></i>
		</div>
		<div class="label-title" style="color: <?= $label->color ? $label->color : '#666'?>">
			<?= $titleAndDescription['title'] ?>
		</div>
		<div class="description">
			<?= $titleAndDescription['description'] ? $titleAndDescription['description'] : Yii::t('label', '_ALL_DESCRIPTION') ?>
		</div>
	</div>
</div>
