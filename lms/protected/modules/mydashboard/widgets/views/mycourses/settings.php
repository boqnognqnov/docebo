<style>
<!--
	.mycourses-settings .initial-display-mode-radio label {
		display: inline-block;
	}
	
	
-->
</style>


<?php
	/* @var $this DashletWidget */

	$selectedInitialDisplayStyle	= $this->getParam('initial_display_style', DashletMyCourses::DISPLAY_STYLE_THUMB);
	$selectedInitialStatusFilter	= $this->getParam('initial_status_filter', DashletMyCourses::STATUS_ACTIVE);
	$selectedInitialLableFilter		= $this->getParam('initial_label_filter', 'All');
	
	
	$selectedSortingFields			= $this->getParam('selected_sorting_fields', array());
	$availSortingFields 			= DashletMyCourses::getAvailableSortingFields();
	$fieldsToRestore = array();

	if (is_array($selectedSortingFields)) {
		foreach ($selectedSortingFields as $field) {
			$fieldsToRestore[$field] = $availSortingFields[$field];
		}
	}
	
	
	$displayStyles 	= DashletMyCourses::getDisplayStyles();
	$statusesList	= DashletMyCourses::getEnrollmentStatusesList();
	$labelsList		= DashletMyCourses::getLabelsList(true);

?>

<div class="mycourses-settings">


<div class="row-fluid">
	<div class="span12">
		<label><?= Yii::t('dashboard', '<strong>Start with the following</strong> filters view') ?></label>
	</div>
</div>


<div class="row-fluid">
	<div class="span12">
		<div class="control-group">
			<label class="control-label"><?= Yii::t('dashboard','Display mode') ?></label>
			<?php foreach ($displayStyles as $k => $label) : ?>
				<label class="radio inline">
					<?= CHtml::radioButton('initial_display_style', $selectedInitialDisplayStyle==$k, array(
						'value' => $k
					)) . $label ?>
				</label>
			<?php endforeach;?>
			<br>
			<label class="control-label" style="min-height: 30px;"> &nbsp</label>
			<label class="setting-description compact"><?= Yii::t('dashboard', '\'Thumbnails\' is available for <strong>one or two</strong> columns layout only') ?></label>
			<br>
		</div>
	</div>
</div>



<div class="row-fluid">
	<div class="span12">
		<div class="control-group">
			<label class="control-label"><?= Yii::t('standard','_STATUS') ?></label>
			<div class="controls">
				<?php
					echo CHtml::dropDownList('initial_status_filter', $selectedInitialStatusFilter, $statusesList, array(
					)); 
				?>
			</div>
		</div>
	</div>
</div>


<div class="row-fluid">
	<div class="span12">
		<div class="control-group">
			<label class="control-label"><?= Yii::t('standard','_LABEL') ?></label>
			<div class="controls">
				<?php
					echo CHtml::dropDownList('initial_label_filter', $selectedInitialLableFilter, $labelsList, array(
					)); 
				?>
			</div>
		</div>
	</div>
</div>

<hr>


<div class="row-fluid">
	<div class="span12">
		<label class="checkbox">
			<?php
				$checked = $this->getParam('show_filters_button', true);
				echo CHtml::checkBox('show_filters_button', $checked, array('uncheckValue' => '0'));
				echo Yii::t('dashboard', 'Show <strong>filters</strong> selector');
			?>
		</label>
		<div class="setting-description styler indented"><?= Yii::t('dashboard', 'Allow users to filter courses by type or status') ?></div>
	</div>
</div>
<br>

<div class="row-fluid">
	<div class="span12">
		<label class="checkbox">
			<?php
				$checked = $this->getParam('show_display_style_switch', true);
				echo CHtml::checkBox('show_display_style_switch', $checked, array('uncheckValue' => '0'));
				echo Yii::t('dashboard', 'Show <strong>thumbnails/list</strong> view switch');
			?>
		</label>
		<div class="setting-description styler indented"><?= Yii::t('dashboard', 'Allow users to switch display mode') ?></div>
	</div>
</div>
<br>

<div class="row-fluid">
	<div class="span12">
		<label class="checkbox">
			<?php
				$checked = $this->getParam('show_search_bar', true);
				echo CHtml::checkBox('show_search_bar', $checked, array('uncheckValue' => '0'));
				echo Yii::t('dashboard', 'Show <strong>search</strong> bar');
			?>
		</label>
		<div class="setting-description styler indented"><?= Yii::t('dashboard', 'Allow users to search courses') ?></div>
	</div>
</div>
<br>

<div class="row-fluid">
	<div class="span12">
		<label class="checkbox">
			<?php
				$checked = $this->getParam('aggregate_labels', false);
				echo CHtml::checkBox('aggregate_labels', $checked, array('uncheckValue' => '0'));
				echo Yii::t('dashboard', 'Use <strong>labels</strong> to group courses');
			?>
		</label>
		<div class="setting-description styler indented"><?= Yii::t('dashboard', 'Allow users to group courses by label') ?></div>
	</div>
</div>

<hr>

<div class="row-fluid">
	<div class="span12">
		<p><?=Yii::t('social', 'Sort by')?></p>
		<div id="selected-sorting-fields">
			<select name="selected_sorting_fields" multiple="multiple">
				<? if(count($fieldsToRestore)): ?>
					<? foreach($fieldsToRestore as $key=>$value): ?>
						<option selected="selected" class="selected" value="<?=$key?>"><?=$value?></option>
					<? endforeach; ?>
				<? endif; ?>
			</select>
		</div>
	</div>
</div>
<br>
<hr>




<div class="row-fluid">
	<div class="span12">
			<?php
				$n = $this->getParam('display_number_items', 10); 
				echo Yii::t('dashboard', 'Display') . "&nbsp;&nbsp;";
				echo CHtml::textField('display_number_items', $n, array(
					'class' => 'span3',	
				));
				echo '&nbsp;&nbsp;' . Yii::t('dashboard', 'items in widget');
			?>
			<br>
			<span class="setting-description compact">&nbsp;<?= Yii::t('dashboard', 'Leave blank to display all items') ?></span>
	</div>
</div>	
<br>


<div class="row-fluid">
	<div class="span12">
			<?php
				$n = $this->getParam('display_number_items_fullist', 25); 
				echo Yii::t('dashboard', 'Display') . "&nbsp;&nbsp;";
				echo CHtml::textField('display_number_items_fullist', $n, array(
					'class' => 'span3',	
				));
				echo '&nbsp;&nbsp;' . Yii::t('dashboard', 'items in full list');
			?>
			<br>
			<span class="setting-description compact">&nbsp;<?= Yii::t('dashboard', 'Leave blank to display all items') ?></span>
	</div>
</div>	
<br>


<div class="row-fluid">
	<div class="span12">
		<label class="checkbox">
			<?php
				$checked = $this->getParam('paginate_dashlet', false);
				echo CHtml::checkBox('paginate_dashlet', $checked, array('uncheckValue' => '0'));
				echo Yii::t('dashboard', 'Paginate widget');
			?>
		</label>
	</div>
</div>


<div class="row-fluid">
	<div class="span12">
		<label class="checkbox">
			<?php
				$checked = $this->getParam('show_view_all_link', true);
				echo CHtml::checkBox('show_view_all_link', $checked, array('uncheckValue' => '0'));
				echo Yii::t('dashboard', 'Show "view all" link');
			?>
		</label>
	</div>
</div>


<div class="row-fluid">
	<div class="span12">
		<label class="checkbox">
			<?php
				$checked = $this->getParam('remove_plan_courses', false);
				echo CHtml::checkBox('remove_plan_courses', $checked, array('uncheckValue' => '0'));
				echo Yii::t('dashboard', 'Remove courses that are part of a learning plan');
			?>
		</label>
	</div>
</div>

<div class="row-fluid">
    <div class="span12">
        <label class="checkbox">
            <?php
            $checked = $this->getParam('show_course_deadline', false);
            echo CHtml::checkBox('show_course_deadline', $checked, array('uncheckValue' => '0'));
            echo Yii::t('dashboard', 'Show course deadline (if set)');
            ?>
        </label>
    </div>
</div>




</div>


<script type="text/javascript">
	$('.mycourses-settings').closest('form').addClass('form-horizontal');
	
	$(function(){

		var selectElem = $('select[name="selected_sorting_fields"]');

		selectElem.fcbkcomplete({
			json_url: <?= json_encode(Docebo::createAbsoluteLmsUrl('//mydashboard/DashletWidgets/axMyCoursesSortingFieldsAutocomplete')) ?>,
			addontab: false, // <==buggy
			width: '98.5%',
			cache: false,
			complete_text: '',
			maxshownitems: 10,
			input_min_size: -1,
			input_name: 'maininput-name',
			filter_selected: true,
			onselect : function(){
				//console.log(arguments);
			}
		});
		
	});
</script>

