<?php
/* @var $this DashletMyCourses */
/* @var $label LearningLabel */
/* @var $items LearningCourse[] */
/* @var $thumbClass */

$rowLimit = (($this->fltLabelId !== 'all') || ($this->fltLabelId == $label->id_common_label) || !$this->groupByLabels) ? -1 : 5;
?>
<?php if(($label->id_common_label !== 'all') && $this->groupByLabels): ?>
	<?php $this->render('mycourses/label_header', array('label'	=> $label)); ?>
<?php endif; ?>

<?php $count = count($items); ?>
<?php for ($i=0; $i<$count; $i++) : ?>
	<?php $item = $items[$i]; ?>

	<?php if(($rowLimit > 0) && ($i < $rowLimit) && ($i % $rowLimit == 0)): ?>
		<div class="catalog-row">
	<?php endif; ?>

	<?php if(($rowLimit < 0) || ($i < $rowLimit))
			$this->render('mycourses/course_tile', array('course' => $item, 'thumbClass' => $thumbClass));
	?>

	<?php if(($rowLimit > 0) && ($i < $rowLimit) && (($i == $count-1) || (($i+1) % $rowLimit == 0))): ?>
			<div class="clearfix"></div>
			<div class="view-more-box" data-label-id="<?=$label->id_common_label?>" data-remaining-courses="<?=$count - $i - 1?>">
				<p><?= Yii::t('course', 'View more courses')?></p>
			</div>
		</div>
	<?php endif; ?>

<?php endfor; ?>
