<?php
/* @var $this DashletMyCourses */
/* @var $label LearningLabel */
/* @var $items LearningCourse[] */
/* @var $thumbClass */

// $rowLimit = (($this->fltLabelId !== 'all') || ($this->fltLabelId == $label->id_common_label) || !$this->groupByLabels) ? -1 : 5;
$rowLimit = (($this->fltLabelId !== 'all') || ($this->fltLabelId == $label->id_common_label) || !$this->groupByLabels) ? -1 : (($this->dashletModel->cell == -1) ? (($this->groupByLabels) ? -1 : (($this->pageSize) ? $this->pageSize : -1)) : 5);

// $rowLimit = (($this->pageSize) ? $this->pageSize : -1);
?>
<?php if(($label->id_common_label !== 'all') && $this->groupByLabels): ?>
	<?php $this->render('mycourses/label_header', array('label'	=> $label)); ?>
<?php endif; ?>

<?php $count = count($items);  ?>
<?php for ($i=0; $i<$count; $i++) : ?>
	<?php $item = $items[$i]; ?>

	<?php if(($rowLimit > 0) && ($i < $rowLimit) && ($i % $rowLimit == 0) || $i == 0  ): ?>
		<div class="catalog-row">
	<?php endif; ?>

	<?php if(($rowLimit < 0) || ($i < $rowLimit))
			$this->render('mycourses/course_tile', array('course' => $item, 'thumbClass' => $thumbClass));
	?>
    <?php if($i == $count - 1): ?>
        </div>
    <?php endif;?>	

<?php endfor; ?>
