<?php
/* @var $this DashletMyCourses */
/* @var $courses LearningCourse[] */
/* @var $userLabels */
/* @var $hasFilterHeader */
/* @var $showFiltersButton */
/* @var $showSearchBar */
/* @var $showDisplayStyleSwitch */
/* @var $showViewAllCourses */
/* @var $showLabelsSelector */
/* @var $paginate */
/* @var $pageCount */
/* @var $currentPage */
/* @var $viewAllCoursesUrl */
/* @var $thumbClass */
/* @var $displayStyleSwitchIcon */
?>
<div class="w-mycourses">
	<?php
		echo CHtml::beginForm('', 'POST', array(
			'name' 		=> 'mycourses-filter-form-' . $this->dashletModel->id,
			'class'		=> 'mycourses-filter-form',
			'id'		=> 'mycourses-filter-form-' . $this->dashletModel->id,
		));

		// Page number to display on form submit
		echo CHtml::hiddenField('pnumber',0);
	?>

	<?php if ($hasFilterHeader) : ?>
		<div class="row-fluid filter-header">

			<?php if ($showFiltersButton) : ?>
				<div class="filters-button"><?= Yii::t('dashboard', 'Filters') ?></div>
			<?php endif; ?>

			<?php if ($showSearchBar) : ?>
				<div class="search-button"><i class="fa fa-search fa-lg"></i></div>
				<div class="search-field">
					<button type="button" class="close clear-search">&times;</button>
					<?= CHtml::textField('search_input', $_REQUEST['search_input']) ?>
				</div>
			<?php endif; ?>

			<?php if ($showDisplayStyleSwitch) : ?>
				<div class="display-style-switch"><i class="fa <?= $displayStyleSwitchIcon ?> fa-lg view-switch"></i></div>
			<?php endif; ?>
		</div>
	<?php endif; ?>

	<div class="course-list <?=$thumbClass?> mycourses_<?=Yii::app()->getLanguage()?>"  style="display: none">
		<?php $totalCoursesCount = 0 ?>
		<?php foreach ($userLabels as $labelId => $label) : ?>
			<?php if(!empty($courses[$labelId])): ?>
				<?php $totalCoursesCount += count($courses[$labelId]); ?>
				<?php
					if($this->dashletModel->cell === -1){
						$this->render('mycourses/row_full', array('label' => $label, 'items' => $courses[$labelId], 'thumbClass' => $thumbClass));
					} else{
						$this->render('mycourses/row', array('label' => $label, 'items' => $courses[$labelId], 'thumbClass' => $thumbClass));
					}
					
				?>
			<?php endif; ?>
		<?php endforeach; ?>
		<?php if($totalCoursesCount == 0): ?>
			<?php if (Yii::app()->user->getIsGodadmin())
				$this->render('mycourses/superadmin_nocourses', array('enrollmentStatus' => $this->fltEnrollmentStatus));
			else
				$this->render('mycourses/user_no_courses', array('enrollmentStatus' => $this->fltEnrollmentStatus, 'showCatalogHint' => PluginManager::isPluginActive('CoursecatalogApp')));
			?>
		<?php endif; ?>
	</div>

	<?php if ($hasFilterHeader && $showFiltersButton) : ?>
		<div class="filters-selector">
			<div class="row-fluid my-catalog-filters-row">
				<div class="span6 my-catalog-filters-left">
					<div class="row-fluid">
						<label><strong><?= Yii::t('standard', '_STATUS') ?></strong></label>
						<label class="radio">
							<?= CHtml::radioButton('enrollment_status', $this->fltEnrollmentStatus == DashletMyCourses::STATUS_ALL, array(
								'value'		=> DashletMyCourses::STATUS_ALL,
							)) ?><?= Yii::t('standard', '_ALL_COURSES')  ?>
						</label>
						<label class="radio">
							<?= CHtml::radioButton('enrollment_status', $this->fltEnrollmentStatus == DashletMyCourses::STATUS_ACTIVE, array(
								'value'		=> DashletMyCourses::STATUS_ACTIVE
							)) ?><?= Yii::t('dashboard', '_ACTIVE_COURSE')  ?>
						</label>
						<label class="radio">
							<?= CHtml::radioButton('enrollment_status', $this->fltEnrollmentStatus == DashletMyCourses::STATUS_COMPLETED, array(
								'value'		=> DashletMyCourses::STATUS_COMPLETED,
							)) ?><?= Yii::t('standard', 'Completed courses')  ?>
						</label>
						<?php if($showLabelsSelector): ?>
							<br>
							<label><strong><?= Yii::t('standard', 'Select label') ?></strong></label>
							<?php echo CHtml::dropDownList('label_id', $this->fltLabelId, CHtml::listData($userLabels, 'id_common_label', 'title'));?>
						<?php else : ?>
							<?php echo CHtml::dropDownList('label_id', $this->fltLabelId, CHtml::listData($userLabels, 'id_common_label', 'title'), array('style' => 'display:none;'));?>
						<?php endif; ?>
					</div>
					<hr class="left-right-separator" />
				</div>
				<div class="span6 my-catalog-filters-right">
					<div class="row-fluid">
						<div class="span12">
							<?php
							// Display additional fields filter here
							Yii::app()->controller->renderPartial('lms.protected.modules.mydashboard.widgets.views.courseFields._filters',
								array(
										'additionalCourseFieldsModels'  => $additionalCourseFieldsModels,
										'containerClass'                => '.mydashboard\\.widgets\\.DashletMyCourses',
										'dashletId'                     => $this->dashletModel->id
									),
								false, true);
							?>
						</div>
					</div>
				</div>

			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="row-fluid">
						<div class="span12">
							<!-- Apply button goes here -->
							<button class="btn btn-docebo green big filters-button mydashboard-apply-button"><?=Yii::t('billing', 'Apply') ?></button>
							<button type="reset" class="btn btn-docebo black big filters-button mydashboard-cancel-button"><?=Yii::t('billing', 'Cancel') ?></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php else : ?>
		<?php echo CHtml::dropDownList('label_id', $this->fltLabelId, CHtml::listData($userLabels, 'id_common_label', 'title'), array('style' => 'display:none;'));?>
	<?php endif; ?>


	<?php if ($paginate && $pageCount>1) : ?>
	<div class="row-fluid dashlet-pager">
		<div class="span12">
			<?php if (($currentPage !== false)  && $currentPage < ($pageCount-1) ) :  ?>
				<div class="dashlet-pager-next"><?= Yii::t('test', '_TEST_NEXT_PAGE') ?> <i class="fa fa-chevron-right"></i></div>
			<?php endif; ?>

			<?php if ($currentPage > 0) : ?>
				<div class="dashlet-pager-prev"><i class="fa fa-chevron-left"></i> <?= Yii::t('test', '_TEST_PREV_PAGE') ?></div>
			<?php endif; ?>
		</div>
	</div>
	<?php endif; ?>

	<div class="clearfix"></div>

	<?= CHtml::endForm() ?>
</div>


<!-- Use .dl-footer as a special class. dashboard plugin will put the WHOLE element in the footer zone -->
<div class="dl-footer">
	<div class="span6 dl-footer-left 	text-left"></div>
	<div class="span6 dl-footer-right 	text-right">
		<?php if ($showViewAllCourses) : ?>
			<?php if ($this->idLayout == DashboardLayout::VIRTUAL_TYPE_SINGLE_CELL) : ?>
				<a href="<?= Docebo::createLmsUrl('site/index') ?>"><?= Yii::t('dashboard', 'Back to My Dashboard') ?></a>
			<?php else : ?>
				<a href="<?= $viewAllCoursesUrl ?>"><?= Yii::t('dashboard', 'View all courses') ?></a>
			<?php endif;?>

		<?php endif; ?>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		// Attach "mycourses" jquery plugin to THIS VERY dashlet.
        $('[id="dashlet-<?= json_encode((int) $this->dashletModel->id) ?>"]').mycourses({
       		dashControllerUrl	: <?= json_encode(Docebo::createLmsUrl('//mydashboard/dash')) ?>,
			adminLayoutsMode: false
       	});

		$('#search_input').keypress(function (e) {
			var key = e.which;
			if(key == 13){
				$("#mycourses-filter-form-<?php echo $this->dashletModel->id; ?>").submit();
				return false;
			}
		});

		/**
		 * Workaround for Mozilla flicker effect "DOCEBO-424"
		 *
		 * See class div course-list -> style: display: none
		 */
		$('#dashlet-<?= json_encode((int) $this->dashletModel->id) ?> .course-list').show();
	});
</script>
