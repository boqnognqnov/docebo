<? if ( $this->getParam( 'total_sessions_last_year' ) ): ?>
	<? $this->widget( 'DoceboSimpleBarchart', array(
		'data' => $montlySessions
	) ); ?>
	<br/>
	<p><?= Yii::t( 'dashlets', 'Total number of <b>user</b> sessions (1 year)' ) ?></p>
	<br/>
<? endif; ?>

<div class="row-fluid knob-row">
	<? $i = 0;
	foreach ( $orderedBlocks as $kpi ) {
        /*****- Make the admin KPIs on one row and remove the top-border which was on the second line -****/
        $dashWidth = 0;
        $MyDashboardDashlet = DashboardDashlet::model()->findByPk($this->dashletModel['id']);
        $MyDashboardCell = DashboardCell::model()->findByPk($MyDashboardDashlet->cell);

        if ($MyDashboardCell->width > 0) {
            $dashWidth = $MyDashboardCell->width;
        }

        if ( $i == 2 && ($dashWidth === '6' || $dashWidth === '8')) { // For 1/2 and 1/3 apply BORDER
            echo '</div><div class="row-fluid knob-row top-border">';
            $i = 0;
        }
        else if($dashWidth === '12') { // For FullWidth Widget NO BORDER
            $i = 0;
        } else if ($dashWidth === '4'  && $i == 2) {
            echo '</div><div class="row-fluid knob-row top-border">';
            $i = 0;

        } else if ($dashWidth === '4') {
            echo"
            <script>
                $(function() {
                    $('p.knob-label').css({
                        'vertical-align':'sub',
                        'margin-top':'0px',
                        'text-align':'inherit',
                        'width':'100%',
                        'height':'auto'
                    });
                    $('p.knob-label.initial').css({
                        'margin-top':'8px'
                    });
                });
            </script>";
        }
         $i ++ ;

		switch ( $kpi ) {
			case 'courses_not_started':
				?>
				<div class="span6">
					<div class="pull-left knob-holder">
						<input type="text" class="auto-knob" data-color="#000" data-size="60"
						       value="<?= $totalEnrollmentsCount == 0 ? 0 : intval( $courses_not_started / $totalEnrollmentsCount * 100 ) ?>"/>
					</div>
					<p class="knob-label"><?= Yii::t( 'dashlets', '<b>Not yet started</b> courses' ) ?></p>
				</div>
				<?
				break;
			case 'courses_in_progress':
				?>
				<div class="span6">
					<div class="pull-left knob-holder">
						<input type="text" class="auto-knob" data-color="#E47F27" data-size="60"
						       value="<?= $totalEnrollmentsCount == 0 ? 0 : intval( $courses_in_progress / $totalEnrollmentsCount * 100 ) ?>"/>
					</div>
					<p class="knob-label initial"><?= Yii::t( 'dashlets', '<b>In progress</b> courses' ) ?></p>
				</div>
				<?
				break;
			case 'courses_completed':
				?>
				<div class="span6">
					<div class="pull-left knob-holder">
						<input type="text" class="auto-knob" data-size="60"
						       value="<?= $totalEnrollmentsCount == 0 ? 0 : intval( $courses_completed / $totalEnrollmentsCount * 100 ) ?>"/>
					</div>
					<p class="knob-label initial"><?= Yii::t( 'dashlets', '<b>Completed</b> courses' ) ?></p>
				</div>
				<?
				break;

			default:
				// Unknown CORE Admin KPI, lets raise an event and see if someone returns HTML or view/params to render
				$viewPath 	= false;
				$viewParams	= false;
				$viewHtml 	= false;
				Yii::app()->event->raise('CollectCustomKPIData', new DEvent($this, array(
					'kpi'			=> $kpi,
					'viewPath' 		=> &$viewPath,  	// !! by reference
					'viewParams'	=> &$viewParams,	// !! by reference
					'viewHtml'		=> &$viewHtml,		// !! by reference
				)));
				// If we get HTML, show it, it has highest priority
				if ($viewHtml) {
					echo $viewHtml;
				}
				// Otherwise, check for View info
				else if ($viewPath && $viewParams) {
					$this->renderPartial($viewPath, $viewParams);
				}
				break;
		}
	} ?>
</div>
<div class="clearfix"></div>