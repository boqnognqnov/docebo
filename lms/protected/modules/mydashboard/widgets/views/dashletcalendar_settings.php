<div>
	<?=CHtml::radioButton('view_type', $this->getParam('view_type', DashletCalendar::VIEW_TYPE_CALENDAR)===DashletCalendar::VIEW_TYPE_CALENDAR, array(
		'value'=>DashletCalendar::VIEW_TYPE_CALENDAR,
	))?>
	<?=Yii::t("dashlets", "Calendar view")?>
</div>

<br/>

<div>
	<?=CHtml::radioButton('view_type', $this->getParam('view_type', DashletCalendar::VIEW_TYPE_CALENDAR)===DashletCalendar::VIEW_TYPE_LISTVIEW, array(
		'value'=>DashletCalendar::VIEW_TYPE_LISTVIEW,
	))?>
	<?=Yii::t("dashlets", "List view, with maximum")?>

	<input class="span2" type="text" name="max_items" value="<?=$this->getParam('max_items')?>" />

	<?=Yii::t("standard", "items")?>
</div>

<br/>

<div>
	&nbsp;<?=CHtml::checkBox('show_my_calendar_link', $this->getParam('show_my_activities_link', true))?>
	&nbsp;<?=Yii::t("dashlets", 'Show "View my calendar" link')?>
</div>