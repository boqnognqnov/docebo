<p><strong>Row ID <?= $this->dashletModel->dashboardCell->dashboardRow->id ?></strong></p>
<p><strong>Cell ID <?= $this->dashletModel->dashboardCell->id ?></strong></p>
<p><strong>Position ID <?= $this->dashletModel->position ?></strong></p>
<p><strong>Dashlet ID <?= $this->dashletModel->id ?></strong></p>
<p><strong>Dashlet Height <?= $this->dashletModel->height ?></strong></p>

<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
</p>



<!-- Use .dl-footer as a special class. dashboard plugin will put the WHOLE element in the footer zone -->
<div class="dl-footer">
	<div class="span6 dl-footer-left 	text-left"></div>
	<div class="span6 dl-footer-right 	text-right"><a class="open-dialog">View something</a></div>
</div>


<script type="text/javascript">
/*<![CDATA[*/

    // Example: listen for when THIS dashlet instance is loaded (by ID)
	$(document).on('mydashboard.dashlet.content-loaded','.dashlet[data-dashlet-id="<?= json_encode((int) $this->dashletModel->id) ?>"]', function(e, data){
    }); 

           
/*]]>*/
</script>
           
