<?php
// Generate unique id for the textarea
$uniqueId = md5(microtime(1));

// Get array with html contents per different languages
$htmlArr = $this->getParam('html');

// Ensure it is an array
if(empty($htmlArr) || !is_array($htmlArr)) {
	$htmlArr = array();
}

// Get all active languages
$langs = Lang::getLanguages(true);

// If there are missing translations for some languages
// Disabled since in certain cases the translations are >= the active languages
//if (count($langs) > count($htmlArr)) {
	foreach($langs as $lang) {
		if (!array_key_exists($lang['code'], $htmlArr)) {
			$htmlArr[$lang['code']] = '';
		}
	}
//}

// Generate all hidden temp html inputs
if(is_array($htmlArr) && count($htmlArr) > 0) {
	foreach($htmlArr as $key => $row) {
		echo CHtml::hiddenField('html[' . $key . ']', $row, array('id' => 'html-' . $key));
	}
}

// Encode the array to JSON in order to be used in the JS area
$htmlArr = CJSON::encode($htmlArr);
?>

<p><?=Yii::t('dashlets', 'Your content')?></p>

<div>
	<?=CHtml::textArea('htmlArea', $this->getHtmlContent(), array(
		'id'=>'html-'.$uniqueId
	))?>
</div>

<script>
	var htmlArr = [];

	$(function(){
		TinyMce.attach("#<?='html-'.$uniqueId?>", {height: 250});

		// Array with HTML area content for each languages
		htmlArr = <?= $htmlArr; ?>;

		// Current language
		var currentLang = $('#lang_code').val();

		// Text area unique id per this dashlet
		var htmlId = '#html-<?= $uniqueId; ?>';

		// Set textarea according to the currently selected language
		$(htmlId).val(htmlArr[currentLang]);

		// On language change, set proper content in the title and html input/textarea
		$(document).on('change', '#lang_code', function() {
			if(currentLang != $('#lang_code').val()) {
				htmlArr[currentLang] = $(htmlId).val();
			}

			currentLang = $('#lang_code').val();

			$(htmlId).val(htmlArr[currentLang]);
		});

		/**
		 * Show languages, that are populated
		 */
		function showPopulatedLangs() {
			$("#lang_code > option").each(function(key, value) {
				var optionText = this.text;
				this.text = optionText.replace('* ', '');

				if(titleArr[this.value].replace(/\s+/, '') != '' || (htmlArr[this.value].replace(/\s+/, '') != '' && htmlArr[this.value].replace(/\s+/, '') != '<p></p>')) {
					this.text = '* ' + this.text;
				}
			});
		}

		function getAssignedLangsCount2(titleArr) {
			var count = 0;
			$.each(titleArr, function(key, value) {
				if(value.replace(/\s+/, '') != '' || (htmlArr[key].replace(/\s+/, '') != '' && htmlArr[key].replace(/\s+/, '') != '<p></p>')) {
					count++;
				}
			});

			// Mark populated languages with asterisks
			showPopulatedLangs();

			return count;
		}

		function updateFieldsAndTitle(){
			currentLang = $('#lang_code').val();

			titleArr[currentLang] = $('#dashlet_instance_title').val();
			$('input[name="headers['+currentLang+']"]').val($('#dashlet_instance_title').val());

			var assignedLangsCount = getAssignedLangsCount2(titleArr);
			$('div.dashlet-settings-html div.lang-wrapper p.assigned span').html(assignedLangsCount);
		}

		setTimeout(function(){
			var editor = tinymce.EditorManager.editors.pop();

			// On Editor content change
			editor.on('keyup change setContent', function (e) {
				currentLang = $('#lang_code').val();
				htmlArr[currentLang] = editor.getContent();
				$('#html-' + currentLang).val(htmlArr[currentLang]);

				var assignedLangsCount = getAssignedLangsCount2(titleArr);
				$('div.dashlet-settings-html div.lang-wrapper p.assigned span').html(assignedLangsCount);
			});

			var assignedLangsCount = getAssignedLangsCount2(titleArr);
			$('div.dashlet-settings-html div.lang-wrapper p.assigned span').html(assignedLangsCount);

		}, 3000);

		// Prevent default event handling
		// On typing the title update the hidden fields and titles array
		$('#dashlet_instance_title').off('keyup').on('keyup', function() {
			updateFieldsAndTitle();
		});

		$('#dashlet_instance_title').off('change').on('change', function() {
			updateFieldsAndTitle();
		});

		// NOTE: THIS CODE DOESN'T WORKS ALWAYS AND THE EVENT IS NOT HANDLED SOMETIMES - TODO refactor this later
//		// Tinymce is tricky: to add  listeners to the editor we need to wait until the editor is actually created
//		tinymce.EditorManager.on("AddEditor", function (e) {
//			// We are interested only on "OUR: textarea editor(s), ignore others
//			if (e.editor.id == "html-<?//= $uniqueId ?>//") {
//				var editor = e.editor;
//
//				// On Editor content change
//				editor.on('keyup change', function (e) {
//					currentLang = $('#lang_code').val();
//					htmlArr[currentLang] = editor.getContent();
////					$(htmlId).val(htmlArr[currentLang]);
//					$('#html-' + currentLang).val(htmlArr[currentLang]);
//				});
//
//				$(editor).trigger('change');
//			}
//		});

	});
</script>