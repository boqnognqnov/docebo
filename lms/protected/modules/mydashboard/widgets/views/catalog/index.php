<?php
/* @var $this  	DashletCatalog */
/* @var $course LearningCourse */
/* @var $plan 	LearningCoursepath */
/* @var $hasFilterHeader */
/* @var $showCategoriesSelector */
/* @var $showFiltersButton */
/* @var $showSearchBar */
/* @var $showDisplayStyleSwitch */
/* @var $showViewAllCourses */
/* @var $displayStyleSwitchIcon */
/* @var $thumbClass */
/* @var $catalogItems */
/* @var $viewAllCoursesUrl */
/* @var $currentPage */
/* @var $pageCount */
/* @var $paginate */
/* @var $showTopRatedCoursesBox */
/* @var $showSidebar */
/* @var $topRatedCourses LearningCourse[] */
/* @var $categoriesTree */
?>

<div class="w-mycourses">
	<?php
		echo CHtml::beginForm('', 'POST', array(
			'name' 		=> 'mycourses-filter-form-' . $this->dashletModel->id,
			'class'		=> 'mycourses-filter-form',
			'id'		=> 'mycourses-filter-form-' . $this->dashletModel->id,
		));

		// Page number to display on form submit
		echo CHtml::hiddenField('pnumber',0);
		echo CHtml::hiddenField('content_refresh',1);
	?>

	<?php if ($hasFilterHeader) : ?>
		<div class="row-fluid filter-header">

			<?php if ($showCategoriesSelector) : ?>
				<div class="categories-button"><i class="fa fa-folder-open fa-lg"></i></div>
			<?php endif; ?>

			<?php if ($showFiltersButton) : ?>
				<div class="filters-button"><?= Yii::t('dashboard', 'Filters') ?></div>
			<?php endif; ?>

			<?php if ($showSearchBar) : ?>
				<div class="search-button"><i class="fa fa-search fa-lg"></i></div>
				<div class="search-field">
					<button type="button" class="close clear-search">&times;</button>
					<?= CHtml::textField('search_input', $this->fltSearch) ?>
				</div>
			<?php endif; ?>

			<?php if ($this->idLayout == DashboardLayout::VIRTUAL_TYPE_SINGLE_CELL) { ?>
				<div class="display-style-switch display-style-switch-1">
					<i class="fa fa-calendar fa-lg"></i>
				</div>
				<div class="display-style-switch display-style-switch-2">
					<i class="fa fa-list fa-lg"></i>
				</div>
				<div class="display-style-switch display-style-switch-3 dark">
					<i class="fa fa-th fa-lg"></i>
				</div>
			<?php } ?>

			<?php if ($showDisplayStyleSwitch) : ?>
				<div class="display-style-switch display-style-switch-1">
					<i class="fa fa-calendar fa-lg"></i>
				</div>
				<div class="display-style-switch display-style-switch-2">
					<i class="fa fa-list fa-lg"></i>
				</div>
				<div class="display-style-switch display-style-switch-3 dark">
					<i class="fa fa-th fa-lg"></i>
				</div>
			<?php endif; ?>
		</div>
	<?php endif; ?>

	<div class="row-fluid <?= ($showSidebar) ? 'catalog-with-sidebar' : 'catalog-without-sidebar'?>">
		<?php if($showSidebar): ?>
		<div class="catalog-left-sidebar">
			<?php if($showCategoriesSelector): ?>
				<h3><?= Yii::t('reservation', '_CATEGORY_CAPTION') ?></h3>
				<div id="leftCategoriesTree">
					<?php $this->render("catalog/categories_tree", array(
						'treeId' => 'catalog-categories-tree-'.$this->dashletModel->id,
						'categoriesTree' => $categoriesTree,
						'enableFilter' => $showSidebar,
						'isInSidebar' => true
					)); ?>
				</div>
			<?php endif ?>

			<?php if($showTopRatedCoursesBox): ?>
				<h3><?= Yii::t('course', 'Top {count} rated courses', array('{count}' => count($topRatedCourses))) ?></h3>
				<div id="topRatedCategories">
					<?php $this->render('catalog/top_rated', array('topRatedCourses' => $topRatedCourses)); ?>
				</div>
			<?php endif ?>
		</div>
		<?php endif ?>
		<div class="course-list <?= $thumbClass ?>" style="display: none">
			<?php // $items must contain list of catalog items (courses, learning plans)
				$filteredCatalogItems = false ;
				if($catalogItems) {
					foreach($catalogItems as  $catalogId => $filteredItems){
						if($filteredItems){
							$filteredCatalogItems = true;
						}
					}
				}
			?>
			<?php if (!empty($catalogItems) && $filteredCatalogItems) : ?>
				<?php foreach ($catalogItems as $catalog => $items) : ?>
					<?php $this->render('catalog/row', array('catalog' => $catalog, 'items' => $items, 'thumbClass' => $thumbClass));?>
				<?php endforeach; ?>
			<?php else: ?>
				<?= Yii::t('report', '_NULL_REPORT_RESULT') ?>
			<?php endif; ?>
		</div>
		<div class="catalog-calendar-content" style="<?= !empty($items) ? 'display: none' : 'text-align: center' ?>">
			<?php

			if(!empty($items)){
				$this->render('catalog/calendar', array('items' => $items, 'dashletId' => (json_encode((int) $this->dashletModel->id))));
			} else{
				echo Yii::t('report', '_NULL_REPORT_RESULT');
			}

			?>
		</div>
	</div>

	<?php if ($hasFilterHeader) : ?>

		<?php if ($showCategoriesSelector) : ?>
			<div class="categories-selector">
				<label><strong><?= Yii::t('dashboard', 'Select course category') ?></strong></label>
				<?php $this->render("catalog/categories_tree", array(
					'treeId' => 'catalog-categories-tree-'.$this->dashletModel->id."-sidebar",
					'categoriesTree' => $categoriesTree,
					'enableFilter' => !$showSidebar,
					'isInSidebar' => false
				)); ?>
			</div>
		<?php endif; ?>

		<?php if ($showFiltersButton) : ?>
			<div class="filters-selector <?= $showCategoriesSelector ? 'with-categories-selector' : '' ?> row-fluid_">
				<div class="row-fluid my-catalog-filters-row">
					<div class="span6 my-catalog-filters-left">
						<div class="row-fluid">
							<label><strong><?= Yii::t('standard', '_TYPE') ?></strong></label>
							<?php foreach (DashletCatalog::getCatalogItemTypes(true) as $k => $v) :?>
								<label class="radio">
									<?= CHtml::radioButton('item_type', $this->fltItemType == $k, array(
										'value'		=> $k,
									)) ?><?= $v  ?>
								</label>
							<?php endforeach;?>
						</div>

						<?php if (!(Yii::app()->user->isGuest)) { ?>
						<br />

						<div class="row-fluid">
							<label class="checkbox" for="not_enrolled_courses">
								<?= CHtml::checkBox('not_enrolled_courses', ($this->fltNotEnrolled == 1), array(
									'value' => 1
								)) ?>
								<?= Yii::t('catalog', 'Show only courses where I am not enrolled in')  ?>
							</label>
						</div>
						<?php } ?>

						<hr>

						<div class="row-fluid">
							<label><strong><?= Yii::t('transaction', '_PRICE') ?></strong></label>
							<?php foreach (DashletCatalog::getPricingTypes(true) as $k => $v) :?>
								<label class="radio inline">
									<?= CHtml::radioButton('pricing_type', $this->fltPricingType == $k, array(
										'value'		=> $k,
									)) ?><?= $v  ?>
								</label>
							<?php endforeach;?>
						</div>

						<hr>

						<div class="row-fluid">
							<label><strong><?= Yii::t('dashboard', 'Select catalog') ?></strong></label>
							<?php echo CHtml::dropDownList('catalog', $this->fltCatalog, DashletCatalog::getUserAvailableCatalogs(true), array('style' => 'width: auto;')); ?>
						</div>
						<hr class="left-right-separator" />
					</div>
					<div class="span6 my-catalog-filters-right">
						<div class="row-fluid">
							<div class="span12">
								<?php
									// Display additional fields filter here
									Yii::app()->controller->renderPartial('lms.protected.modules.mydashboard.widgets.views.courseFields._filters',
										array(
											'additionalCourseFieldsModels'  => $additionalCourseFieldsModels,
											'containerClass'                => '.mydashboard\\.widgets\\.DashletCatalog',
											'dashletId'                     => $this->dashletModel->id
										),
										false, true);
								?>
							</div>
						</div>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12">
						<div class="row-fluid">
							<div class="span12">
								<!-- Apply button goes here -->
								<button class="btn btn-docebo green big filters-button mydashboard-apply-button"><?=Yii::t('billing', 'Apply') ?></button>
								<button type="reset" class="btn btn-docebo black big filters-button mydashboard-cancel-button"><?=Yii::t('billing', 'Cancel') ?></button>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php else: ?>
			<input type="hidden" id="catalog" name="catalog" value="<?= $this->fltCatalog ?>" />
			<input type="hidden" id="pricing_type" name="pricing_type" value="all" />
			<input type="hidden" id="item_type" name="item_type" value="all" />
		<?php endif; ?>

	<?php endif; ?>

	<?php if ($paginate && $pageCount>1) : ?>
	<div class="row-fluid dashlet-pager">
		<div class="span12">
			<?php if (($currentPage !== false)  && $currentPage < ($pageCount-1) ) :  ?>
				<div class="dashlet-pager-next"><?= Yii::t('test', '_TEST_NEXT_PAGE') ?> <i class="fa fa-chevron-right"></i></div>
			<?php endif; ?>

			<?php if ($currentPage > 0) : ?>
				<div class="dashlet-pager-prev"><i class="fa fa-chevron-left"></i> <?= Yii::t('test', '_TEST_PREV_PAGE') ?></div>
			<?php endif; ?>
		</div>
	</div>
	<?php endif; ?>

	<div class="clearfix"></div>

	<?= CHtml::endForm() ?>
</div>

<!-- Use .dl-footer as a special class. dashboard plugin will put the WHOLE element in the footer zone -->
<div class="dl-footer" id="footer_<?= json_encode((int) $this->dashletModel->id) ?>">
	<div class="span6 dl-footer-left 	text-left"></div>
	<div class="span6 dl-footer-right 	text-right">
		<?php if ($showViewAllCourses) : ?>
			<?php if ($this->idLayout == DashboardLayout::VIRTUAL_TYPE_SINGLE_CELL) : ?>
				<a href="<?= Docebo::createLmsUrl('site/index') ?>"><?= Yii::t('dashboard', 'Back to My Dashboard') ?></a>
			<?php else : ?>
				<a href="<?= $viewAllCoursesUrl ?>"><?= Yii::t('dashboard', 'View full catalog') ?></a>
			<?php endif;?>

		<?php endif; ?>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$('input[type="checkbox"][name="not_enrolled_courses"]').styler();

		// Attach "mycourses" jquery plugin to THIS VERY dashlet.
        $('[id="dashlet-<?= json_encode((int) $this->dashletModel->id) ?>"]').mycourses({
       		dashControllerUrl	: <?= json_encode(Docebo::createLmsUrl('//mydashboard/dash')) ?>,
			adminLayoutsMode: false
       });

		var id = <?= json_encode((int) $this->dashletModel->id) ?>;//$(this).data('dashlet-id');
		var currentAction = getUrlRoute(true);
		currentAction = currentAction.replace('/', '_');
		var cookieName = 'dashlet_display_style_' + id;// + '_' + currentAction;
<?php if(Yii::app()->user->isGuest) { ?>
			cookieName += '_public';
<?php } ?>

		if ($.cookie(cookieName) !== 'calendar') {
			/**
			 * Workaround for Mozilla flicker effect "DOCEBO-424"
			 *
			 * See class div course-list -> style: display: none
			 */
			$('#dashlet-<?= json_encode((int) $this->dashletModel->id) ?> .display-style-switch').removeClass('dark');
			if ($.cookie(cookieName) === 'list') {
				$('#dashlet-<?= json_encode((int) $this->dashletModel->id) ?> .display-style-switch .fa-list').parent().addClass('dark');
			} else {
				$('#dashlet-<?= json_encode((int) $this->dashletModel->id) ?> .display-style-switch .fa-th').parent().addClass('dark');
			}
			$('#dashlet-<?= json_encode((int) $this->dashletModel->id) ?> .catalog-calendar-content').hide();
			$('#dashlet-<?= json_encode((int) $this->dashletModel->id) ?> .course-list').show();
		} else {
			$('#dashlet-<?= json_encode((int) $this->dashletModel->id) ?> .display-style-switch').removeClass('dark');
			$('#dashlet-<?= json_encode((int) $this->dashletModel->id) ?> .display-style-switch .fa-calendar').parent().addClass('dark');

			$('#dashlet-<?= json_encode((int) $this->dashletModel->id) ?> .course-list').hide();
			$('#dashlet-<?= json_encode((int) $this->dashletModel->id) ?> .catalog-left-sidebar').hide();
			$('#dashlet-<?= json_encode((int) $this->dashletModel->id) ?> .dashlet-pager').hide();
			$('#dashlet-<?= json_encode((int) $this->dashletModel->id) ?> .catalog-calendar-content').show();
		}
	});

	<?php if($this->return_to_top): ?>
		$(document).ready(function(){
			$('html, body').scrollTop($("#footer_<?= json_encode((int) $this->dashletModel->id) ?>").offset().top);
			$('html, body').scrollTop($('.dashlet[data-dashlet-id="<?= json_encode((int) $this->dashletModel->id) ?>"]').offset().top);
		});
	<?php endif; ?>
</script>
