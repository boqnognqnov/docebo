<?php
/* @var $this DashletCatalog */
/* @var $catalog string */
/* @var $items array */
/* @var $catalogModel LearningCatalogue */

$rowLimit = (($catalog == 'all') || ($this->fltCatalog == $catalog)) ? -1 : 5;
?>

<?php if($catalog !== 'all'): ?>
	<?php $catalogModel = $this->getCatalogById($catalog); ?>
	<div class="clearfix"></div>
	<div class="w-mycourses-label row-fluid">
		<div class="label-container">
			<div class="label-icon <?= ($this->fltCatalog !== 'all') ? (isset($this->params['initial_catalog']) && $this->params['initial_catalog'] !== 'all' ? '' : 'back-to-catalogs') : '' ?>" style="background-color: #08c;">
				<i class="fa fa-<?= ($this->fltCatalog !== 'all') ? (isset($this->params['initial_catalog']) && $this->params['initial_catalog'] !== 'all' ? 'book' : 'chevron-left') : 'book' ?> fa-2x fa-inverse"></i>
			</div>
			<div class="label-title" style="color: #08c">
				<?= $catalogModel->name ?>
			</div>
			<div class="description">
				<?= Yii::app()->htmlpurifier->purify($catalogModel->description) ?>
			</div>
		</div>
	</div>
<?php endif; ?>

<?php $count = count($items); ?>
<?php for ($i=0; $i<$count; $i++) : ?>
	<?php $item = $items[$i]; ?>

	<?php if(($rowLimit > 0) && ($i < $rowLimit) && ($i % $rowLimit == 0)): ?>
		<div class="catalog-row">
	<?php endif; ?>


	<?php if(($rowLimit < 0) || ($i < $rowLimit)) {
		switch ($item['catalog_item_type']) {
			case 'coursepath':
				$lp = LearningCoursepath::model()->findByPk($item['id']);
				$this->render('catalog/plan_tile', array(
					'lp' => $lp,
					'item' => $item,
				));
				break;
			case 'course' :
				$course = LearningCourse::model()->findByPk($item['id']);
				$this->render('catalog/course_tile', array(
					'course' => $course,
					'item' => $item,
					'thumbClass' => $thumbClass
				));
				break;
		}
	}
	?>

	<?php if(($rowLimit > 0) && ($i < $rowLimit) && (($i == $count-1) || (($i+1) % $rowLimit == 0))): ?>
			<div class="clearfix"></div>
			<div class="view-more-box" data-catalog-id="<?=$catalog?>" data-remaining-courses="<?=$count - $i - 1?>">
				<p><?= Yii::t('course', 'View more courses')?></p>
			</div>
		</div>
	<?php endif; ?>

<?php endfor; ?>
