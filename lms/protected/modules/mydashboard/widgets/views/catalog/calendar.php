<?php

Yii::app()->getModule('mycalendar');
$hash = sha1(Yii::app()->user->id.ShowController::INTERNAL_SECRET_KEY);

?>

<div class="row-fluid calendar-title-container">
	<div class="span6">
		<h3 class="title-bold"><?= Yii::t('calendar', 'Upcoming Classroom and Webinar Sessions') ?></h3>
	</div>
	<div class="span6 text-right">
	</div>
</div>


<div class="main-section view-switch-container">
	<div class="filters-wrapper">
		<div id="view_switch">
			<span class="view_switch"><?php echo Yii::t('standard', 'Switch view'); ?></span><?= CHtml::button(Yii::t('statistic', '_FOR_week'), array('class' => 'btn mycalendar-btn mycalendar-btn-std', 'id' => 'toweek')).CHtml::button(Yii::t('statistic', '_FOR_month'), array('class' => 'btn close-btn mycalendar-btn-std', 'id' => 'tomonth')); ?>
		</div>
	</div>
</div>


<div id='calendar-<?=$dashletId?>'></div>

<script type="text/javascript">
	var rendered = 0;

	$(document).ready(function()
	{

		var calendar = $('#calendar-<?=$dashletId?>');

		//Istance of the calendar
		calendar.fullCalendar({
			//To display Saturday and Sunday
			weekends: true,
			//Time format, hide it in month view
			timeFormat: {
				'month': '',
				'agendaDay': 'HH:mm{ - HH:mm}',
				'agendaWeek': 'HH:mm{ - HH:mm}'
			},
			//Translation of months and days
			monthNames: [
				'<?= addslashes(Yii::t('calendar', '_JANUARY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_FEBRUARY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_MARCH')); ?>',
				'<?= addslashes(Yii::t('calendar', '_APRIL')); ?>',
				'<?= addslashes(Yii::t('standard', '_MONTH_05')); ?>',
				'<?= addslashes(Yii::t('calendar', '_JUNE')); ?>',
				'<?= addslashes(Yii::t('calendar', '_JULY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_AUGUST')); ?>',
				'<?= addslashes(Yii::t('calendar', '_SEPTEMBER')); ?>',
				'<?= addslashes(Yii::t('calendar', '_OCTOBER')); ?>',
				'<?= addslashes(Yii::t('calendar', '_NOVEMBER')); ?>',
				'<?= addslashes(Yii::t('calendar', '_DECEMBER')); ?>'
			],
			monthNamesShort: [
				'<?= addslashes(Yii::t('calendar', '_JAN')); ?>',
				'<?= addslashes(Yii::t('calendar', '_FEB')); ?>',
				'<?= addslashes(Yii::t('calendar', '_MAR')); ?>',
				'<?= addslashes(Yii::t('calendar', '_APR')); ?>',
				'<?= addslashes(Yii::t('calendar', '_MAY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_JUN')); ?>',
				'<?= addslashes(Yii::t('calendar', '_JUL')); ?>',
				'<?= addslashes(Yii::t('calendar', '_AUG')); ?>',
				'<?= addslashes(Yii::t('calendar', '_SEP')); ?>',
				'<?= addslashes(Yii::t('calendar', '_OCT')); ?>',
				'<?= addslashes(Yii::t('calendar', '_NOV')); ?>',
				'<?=addslashes( Yii::t('calendar', '_DEC')); ?>'
			],
			dayNames: [
				'<?= addslashes(Yii::t('calendar', '_SUNDAY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_MONDAY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_TUESDAY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_WEDNESDAY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_THURSDAY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_FRIDAY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_SATURDAY')); ?>'
			],
			dayNamesShort: [
				'<?= addslashes(Yii::t('calendar', '_SUN')); ?>',
				'<?= addslashes(Yii::t('calendar', '_MON')); ?>',
				'<?= addslashes(Yii::t('calendar', '_TUE')); ?>',
				'<?= addslashes(Yii::t('calendar', '_WED')); ?>',
				'<?= addslashes(Yii::t('calendar', '_THU')); ?>',
				'<?= addslashes(Yii::t('calendar', '_FRI')); ?>',
				'<?= addslashes(Yii::t('calendar', '_SAT')); ?>'
			],
			//Text to be displayed on calenda buttons
			buttonText: {
				month: '<?= addslashes(Yii::t('statistic', '_FOR_month')); ?>',
				week: '<?= addslashes(Yii::t('statistic', '_FOR_week')); ?>',
				day: '<?= addslashes(Yii::t('standard', '_DAY')); ?>'
			},
			//Calendar header element disposition
			header: {
				left: '',
				center: 'prevYear prev title next nextYear',
				right: ''//'agendaWeek,month'
			},
			//To set Monday as the first day of the week
			firstDay: 1,
			//RTL setting
			isRTL: <?= (Lang::isRTL(Yii::app()->getLanguage()) ? 'true' : 'false') ?>,
			//For display the full name of the day into the mounth view
			columnFormat: {
				month: 'dddd',
				week: 'dddd dd/MM',
				day: 'dddd dd/MM'
			},
			//Title format for the views
			titleFormat: {
				month: 'MMMM yyyy',
				week: "MMM dd[ yyyy]{ '-'[ MMM] dd, yyyy}",
				day: 'dddd, dd MMMM, yyyy'
			},
			//Url for data fetch
			eventSources: [
				{
					data: $.deparam($('form.mycourses-filter-form', '#dashlet-<?= intval($dashletId); ?>').find('input[name!="YII_CSRF_TOKEN"]:enabled, select:enabled, textarea:enabled').serialize()),
					method: "POST",
					url: './index.php?r=mycalendar/show/fetchCalendarItems&dashletId=<?= intval($dashletId) ?>',
					color: '#8D3FAF',//'#0566af',
					textColor: '#ffffff'
				}
			],
			//Remove the top corner text into week view
			allDayText: '',
			//Set the time format for the left cols into week and day views
			axisFormat: 'HH:mm',
			//Set the starting hour for the calendar week and day views
			firstHour: 0,
			//Set the action to do un event click, in our case the show of the tooltip
			eventClick: function(event, jsEvent, view)
			{
				//Manually create the tooltip
				if($('.tooltipevent'))
					$('.tooltipevent').remove();
				var tooltip = '<div class="tooltipevent"></div>';
				$("body").append(tooltip);

				//Load the tooltip body
				$.ajax({
					type: "POST",
					url: "./index.php?r=mycalendar/show/Tooltip",
					async: false,
					data: {id:event.id},
					success:function(result)
					{
						$('.tooltipevent').html(result);
					}
				});

				//Display the tooltip
				$('.tooltipevent').fadeIn('500');
				$('.tooltipevent').fadeTo('10', 1.9);

				//Set the tooltip position
				var position = $(this).offset();
				$('.tooltipevent').css('top', position.top - $('.tooltipevent').height() - 40);
//				$('.tooltipevent').css('left', position.left + ($(this).width() - $('.tooltipevent').width()) / 2 - 6);

				$('.tooltipevent').css('left', position.left + ($(this).width() / 2) - ($('.tooltipevent').width()/2));// + 30
				$('.triangle', $('.tooltipevent')).css('left', (($('.tooltipevent').width() / 2) - ($('.triangle', $('.tooltipevent')).width() / 2)));


				$(this).mouseover(function(e) {
					$(this).css('z-index', 10000);
				});
			},
			loading: function(is_loading, view)
			{
				if(is_loading)
					closePopup();
			},
			eventAfterRender: function(view)
			{
				//Needed for check if at last one event is rendered
				rendered = 1;
			},
			loading: function(loading, view)
			{
				if(loading)
					rendered = 0;
				else if(rendered == 0)
				{
					$.ajax({
						type: "POST",
						url: "./index.php?r=mycalendar/show/fetchCalendarItems&dashletId=<?= intval($dashletId) ?>",
						async: true,
						data: $.deparam($('form.mycourses-filter-form', '#dashlet-<?= intval($dashletId); ?>').find('input[name!="YII_CSRF_TOKEN"]:enabled, select:enabled, textarea:enabled').serialize()),
						success:function(result)
						{
							var json = jQuery.parseJSON(result);
							if(json.success)
							{
								calendar.fullCalendar('gotoDate', json.year, json.month, json.day);
							}
						}
					});
				}
			}
		});

//		$('#calendar').fullCalendar('gotoDate', (new Date()));


		$("#toweek", '#dashlet-<?=$dashletId?>').click(function(){
			if($('#toweek', '#dashlet-<?=$dashletId?>').hasClass('mycalendar-btn'))
			{
				$('#toweek', '#dashlet-<?=$dashletId?>').removeClass('mycalendar-btn');
				$('#toweek', '#dashlet-<?=$dashletId?>').addClass('close-btn');

				$('#tomonth', '#dashlet-<?=$dashletId?>').removeClass('close-btn');
				$('#tomonth', '#dashlet-<?=$dashletId?>').addClass('mycalendar-btn');

				calendar.fullCalendar('changeView', 'agendaWeek');
			}
		});

		$("#tomonth", '#dashlet-<?=$dashletId?>').click(function(){
			if($('#tomonth', '#dashlet-<?=$dashletId?>').hasClass('mycalendar-btn'))
			{
				$('#tomonth', '#dashlet-<?=$dashletId?>').removeClass('mycalendar-btn');
				$('#tomonth', '#dashlet-<?=$dashletId?>').addClass('close-btn');

				$('#toweek', '#dashlet-<?=$dashletId?>').removeClass('close-btn');
				$('#toweek', '#dashlet-<?=$dashletId?>').addClass('mycalendar-btn');

				calendar.fullCalendar('changeView', 'month');
			}
		});

		var cellWidth = $("#dashlet-<?=$dashletId?>").parent().parent().data('cell-width');
		var windowWidth =   $(window).width();
		var cookieName = "dashlet_display_style_<?=$dashletId?>";

		if($('form#login_confirm').length > 0) {
			cookieName += '_public';
		}

		if ($.cookie(cookieName) === 'calendar' && (windowWidth > 800 && (cellWidth == 8 || cellWidth ==12))) {
			if(cellWidth == 8) {
				$('.view-switch-container', "#dashlet-<?=$dashletId?>").hide();
			}

			$("#mini-calendar-<?=$dashletId?>").hide();
		} else if ($.cookie(cookieName) === 'calendar' && (windowWidth > 800 && (cellWidth < 8))) {
			$("#calendar-<?=$dashletId?>").hide();
			$('.view-switch-container', "#dashlet-<?=$dashletId?>").hide();
			$('.calendar-title-container', "#dashlet-<?=$dashletId?>").hide();
		}

	});


	function closePopup()
	{
		//Remove the tooltip on closing
		if($('.tooltipevent'))
			$('.tooltipevent').remove();
	}


</script>

<div class="catalog-minimal-calendar-content" style="/* display: none */">
	<?php

	$this->render('catalog/minimal_calendar', array(
				'items' => $items, 'dashletId' => (json_encode((int) $this->dashletModel->id)),
				'show_my_calendar_link' => false//$this->getParam('show_my_calendar_link', false),
			));

	?>
</div>