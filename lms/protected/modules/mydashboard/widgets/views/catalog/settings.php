<?php
	/* @var $this DashletWidget */
	/* @var $this DashletCatalog */

	$selectedInitialDisplayStyle	= $this->getParam('initial_display_style', 	DashletMyCourses::DISPLAY_STYLE_THUMB);
	$selectedInitialItemType		= $this->getParam('initial_item_type', 		DashletCatalog::TYPE_ELEARNING);
	$selectedInitialPricingType		= $this->getParam('initial_pricing_type', 	'all');
	$selectedInitialCatalog			= $this->getParam('initial_catalog', 		'all');
	
	$displayStyles 	= DashletCatalog::getDisplayStyles();
	
	$selectedSortingFields			= $this->getParam('selected_sorting_fields', array());
	$availSortingFields 			= DashletCatalog::getAvailableSortingFields();
	$fieldsToRestore = array();
	if (is_array($selectedSortingFields)) {
		foreach ($selectedSortingFields as $field) {
			$fieldsToRestore[$field] = $availSortingFields[$field];
		}
	}
	

?>

<div class="mycourses-settings">


<div class="row-fluid">
	<div class="span12">
		<label><?= Yii::t('dashboard', '<strong>Start with the following</strong> filters view') ?></label>
	</div>
</div>


<div class="row-fluid">
	<div class="span12">
		<div class="control-group">
			<label class="control-label"><?= Yii::t('dashboard','Display mode') ?></label>
			<?php foreach ($displayStyles as $k => $label) : ?>
				<label class="radio inline">
					<?= CHtml::radioButton('initial_display_style', $selectedInitialDisplayStyle==$k, array(
						'value' => $k
					)) . $label ?>
				</label>
			<?php endforeach;?>
            <br>
            <label class="control-label" style="min-height: 30px;"> &nbsp</label>
            <label class="setting-description compact"><?= Yii::t('dashboard', '\'Thumbnails\' and \'Calendar\' are available for <strong>one or two</strong> columns layout only') ?></label>
            <br>
		</div>
	</div>
</div>



<div class="row-fluid">
	<div class="span12">
		<div class="control-group">
			<label class="control-label"><?= Yii::t('standard','_TYPE') ?></label>
			<div class="controls">
				<?php
					echo CHtml::dropDownList('initial_item_type', $selectedInitialItemType, DashletCatalog::getCatalogItemTypes(true), array(
					)); 
				?>
			</div>
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="span12">
		<div class="control-group">
			<label class="control-label"><?= Yii::t('transaction','_PRICE') ?></label>
			<div class="controls">
				<?php
					echo CHtml::dropDownList('initial_pricing_type', $selectedInitialPricingType, DashletCatalog::getPricingTypes(true), array(
					)); 
				?>
			</div>
		</div>
	</div>
</div>


<div class="row-fluid">
	<div class="span12">
		<div class="control-group">
			<label class="control-label"><?= Yii::t('standard','Catalog') ?></label>
			<div class="controls">
				<?php
					echo CHtml::dropDownList('initial_catalog', $selectedInitialCatalog, DashletCatalog::getAllCatalogs(true), array(
					)); 
				?>
			</div>
		</div>
	</div>
</div>



<hr>


<div class="row-fluid">
	<div class="span12">
		<label class="checkbox">
			<?php
				$checked = $this->getParam('show_filters_button', true);
				echo CHtml::checkBox('show_filters_button', $checked, array('uncheckValue' => '0'));
				echo Yii::t('dashboard', 'Show <strong>filters</strong> selector');
			?>
		</label>
		<div class="setting-description styler indented"><?= Yii::t('dashboard', 'Allow users to filter courses by type or status') ?></div>
	</div>
</div>
<br>

<div class="row-fluid">
	<div class="span12">
		<label class="checkbox">
			<?php
				$checked = $this->getParam('show_display_style_switch', true);
				echo CHtml::checkBox('show_display_style_switch', $checked, array('uncheckValue' => '0'));
				echo Yii::t('dashboard', 'Show <strong>thumbnails/list/calendar</strong> view switch');
			?>
		</label>
		<div class="setting-description styler indented"><?= Yii::t('dashboard', 'Allow users to switch display mode') ?></div>
	</div>
</div>
<br>

<div class="row-fluid">
	<div class="span12">
		<label class="checkbox">
			<?php
				$checked = $this->getParam('show_search_bar', true);
				echo CHtml::checkBox('show_search_bar', $checked, array('uncheckValue' => '0'));
				echo Yii::t('dashboard', 'Show <strong>search</strong> bar');
			?>
		</label>
		<div class="setting-description styler indented"><?= Yii::t('dashboard', 'Allow users to search courses') ?></div>
	</div>
</div>
<br>

<div class="row-fluid">
	<div class="span12">
		<label class="checkbox">
			<?php
				$checked = $this->getParam('show_categories_selector', true);
				echo CHtml::checkBox('show_categories_selector', $checked, array('uncheckValue' => '0'));
				echo Yii::t('dashboard', 'Show <strong>categories selector</strong>');
			?>
		</label>
		<div class="setting-description styler indented"><?= Yii::t('dashboard', 'Allow users to filter by category') ?></div>
	</div>
</div>

<hr>
<div class="row-fluid">
	<div class="span12">
		<p><?=Yii::t('social', 'Sort by')?></p>
		<div id="selected-sorting-fields">
			<select name="selected_sorting_fields" multiple="multiple">
				<? if(count($fieldsToRestore)): ?>
					<? foreach($fieldsToRestore as $key=>$value): ?>
						<option selected="selected" class="selected" value="<?=$key?>"><?=$value?></option>
					<? endforeach; ?>
				<? endif; ?>
			</select>
		</div>
	</div>
</div>
<br>
<hr>

<div class="row-fluid">
	<div class="span12">
			<?php
				$n = $this->getParam('display_number_items', 25); 
				echo Yii::t('dashboard', 'Display') . "&nbsp;&nbsp;";
				echo CHtml::textField('display_number_items', $n, array(
					'class' => 'span3',	
				));
				echo '&nbsp;&nbsp;' . Yii::t('standard', 'items');
			?>
			<br>
			<span class="setting-description compact">&nbsp;<?= Yii::t('dashboard', 'Leave blank to display all items') ?></span>
	</div>
</div>	
<br>


<div class="row-fluid">
	<div class="span12">
			<?php
				$n = $this->getParam('display_number_items_fullist', 25); 
				echo Yii::t('dashboard', 'Display') . "&nbsp;&nbsp;";
				echo CHtml::textField('display_number_items_fullist', $n, array(
					'class' => 'span3',	
				));
				echo '&nbsp;&nbsp;' . Yii::t('dashboard', 'items in full list');
			?>
			<br>
			<span class="setting-description compact">&nbsp;<?= Yii::t('dashboard', 'Leave blank to display all items') ?></span>
	</div>
</div>	
<br>


<div class="row-fluid">
	<div class="span12">
		<label class="checkbox">
			<?php
				$checked = $this->getParam('paginate_dashlet', false);
				echo CHtml::checkBox('paginate_dashlet', $checked, array('uncheckValue' => '0'));
				echo Yii::t('dashboard', 'Paginate widget');
			?>
		</label>
	</div>
</div>



<div class="row-fluid">
	<div class="span12">
		<label class="checkbox">
			<?php
				$checked = $this->getParam('show_view_all_link', true);
				echo CHtml::checkBox('show_view_all_link', $checked, array('uncheckValue' => '0'));
				echo Yii::t('dashboard', 'Show "view all" link');
			?>
		</label>
	</div>
</div>



</div>


<script type="text/javascript">
	$('.mycourses-settings').closest('form').addClass('form-horizontal');

	$(function(){

		var selectElem = $('select[name="selected_sorting_fields"]');

		selectElem.fcbkcomplete({
			json_url: <?= json_encode(Docebo::createAbsoluteLmsUrl('//mydashboard/DashletWidgets/axCatalogSortingFieldsAutocomplete')) ?>,
			addontab: false, // <==buggy
			width: '98.5%',
			cache: false,
			complete_text: '',
			maxshownitems: 8,
			input_min_size: -1,
			input_name: 'maininput-name',
			filter_selected: true,
			onselect : function(){
				//console.log(arguments);
			}
		});
		
	});
	
	
</script>

