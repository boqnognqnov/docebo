<?php
	/* @var $lp LearningCoursepath */


	$stats = $lp->getUserCoursesStats(Yii::app()->user->id);
	
	$playUrl = Docebo::createLmsUrl("curricula/show", array(
		'sop'	=> 'unregistercourse',
		'id_path'=>$lp->id_path,
	));

	$idUser = Yii::app()->user->id;
	$locked = false;
	$canPlay = true;
	$isCompleted = false;
	$isNew = $stats['totalCourses'] > 0 && $stats['totalCourses']==$stats['nonStartedCourses'];
	
	$price = number_format((double) $lp->getCoursepathPrice(),2);
	$currencySymbol = Settings::get("currency_symbol");

	$topTagClass = 'tag-new'; // can be extended in the future
	
 	if($stats['totalCourses']>0 && $stats['totalCourses']==$stats['completedCourses']){
 		$isCompleted = true;
 	}

	$actionClass = false;
	if ($isCompleted) {
		$actionClass = "completed";
		$actionIconClass = "fa fa-check-circle-o";
		$actionLabel = Yii::t('standard', '_COMPLETED');
	}
	else if ($canPlay) {
		$actionClass = "play";
		$actionIconClass = "fa fa-play-circle-o";
		$actionLabel = Yii::t('standard', '_PLAY');
	}
	else if ($locked) {
		$actionClass = "locked";
		$actionIconClass = "fa fa-lock";
		$actionLabel = Yii::t('forum', '_LOCKED');
	}
	else {
		$actionClass = "enroll";
		$actionIconClass = "fa fa-plus";
		$actionLabel = Yii::t('standard', 'Enroll');
	}
	
	
	// Resolve how and if the course can be played, enrolled, etc. and return data for labels, icons, classes (presentation data)
	$accessData = $lp->resolvePlanAccess();
	
	
	
?>


<div class="course-tile type-lp">

	<?php 
		if ($accessData['linkTag']) { 
			echo $accessData['linkTag']; 
		}  
	?>

	<div class="course-logo">
		<div class="thumb">
		
			<!-- tags: in thumbnail -->
			<?php if (count($accessData['tags']) > 0): ?>
				<div class="course-tags">
					<?php foreach ($accessData['tags'] as $tag) : ?>
						<span class="course-tag <?= $tag['class'] ?>" rel="tooltip" title="<?= $tag['tooltip'] ?>"><?= $tag['label'] ?></span>
					<?php endforeach;?>
				</div>
			<?php endif; ?>
					
			<?=CHtml::image($lp->getLogo(CoreAsset::VARIANT_ORIGINAL, true), 'Logo')?>
		</div>

		<p class="course-action <?= $accessData['action']['class'] ?>">
		
			<!-- Course tags: in action label -->
			<?php if (count($accessData['tags']) > 0): ?>
				<span class="course-tags">
					<?php foreach ($accessData['tags'] as $tag) : ?>
						<span class="course-tag <?= $tag['class'] ?>"><?= $tag['label'] ?></span>
					<?php endforeach;?>
				</span>
			<?php endif; ?>
							

			<span class="icon">
				<i class="<?= $accessData['action']['icon'] ?>"></i>
			</span>
			<?php if ($accessData['canBuy']) : ?>
				<span class="price">
                    <?php
                        // Raise event right before showing coursepath price, some plugin may want to modify it
                        $orginalPrice = $currencySymbol . " " . $price;
                        $event = new DEvent($this, array('lp' => $lp, 'originalPrice' => $orginalPrice));
                        Yii::app()->event->raise('onBeforeShowLPPrice', $event);
                        if($event->return_value){
                            echo $event->return_value;
                        }else{
                            echo $orginalPrice;
                        }
                    ?>
				</span>
			<?php endif; ?>
			
			<span class="action-label"><?= $accessData['action']['label'] ?></span>
		</p>
		
		
		<h3 class="course-name"><?= $lp->path_name ?></h3>
	</div>
	<div class="clearfix"></div>
	
	<?php 
		if ($accessData['linkTag']) {
			echo "</a>";
		}	
	?>
	

	<div class="course-info">
		<span class="lp-label long">
			<?=Yii::t('standard', '_COURSEPATH')?>
		</span>
		<span class="lp-label short">
			<?=Yii::t('standard', 'LP')?>
		</span>
		|
		<?php echo Yii::t('standard', '{x} courses', array('{x}'=>intval($stats['totalCourses']))); ?>
	</div>


</div>
