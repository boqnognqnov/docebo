<div id='mini-calendar-<?=$dashletId?>' class="calendar-calview"></div>

<?php
$height = 150; // default
if($this->dashletModel->height){
	$height = intval($this->dashletModel->height) - 40;
}
?>

<script>
	$(function(){
		// Instance of the calendar
		var rendered = 0;

		var calendar = $('#mini-calendar-<?=$dashletId?>');

		var customHeightFixerMethod = function(calendarObj){
			//return;
			// A workaround to vertically center the first day of each week
			calendarObj.find('.fc-day-number').each(function(){
				var calculatedHalfTopPadding = Math.round(($(this).parent().height()-$(this).height())/2) - 1;
				$(this).css('padding-top', calculatedHalfTopPadding)
			});
		}

		calendar.fullCalendar({
			height: <?=$height?>,
			//To display Saturday and Sunday
			weekends: true,
			//Time format, hide it in month view
			timeFormat: {
				'month': '',
				'agendaDay': 'HH:mm{ - HH:mm}',
				'agendaWeek': 'HH:mm{ - HH:mm}'
			},
			viewRender: function(view, element){

			},
			//Translation of months and days
			monthNames: [
				'<?= addslashes(Yii::t('calendar', '_JANUARY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_FEBRUARY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_MARCH')); ?>',
				'<?= addslashes(Yii::t('calendar', '_APRIL')); ?>',
				'<?= addslashes(Yii::t('standard', '_MONTH_05')); ?>',
				'<?= addslashes(Yii::t('calendar', '_JUNE')); ?>',
				'<?= addslashes(Yii::t('calendar', '_JULY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_AUGUST')); ?>',
				'<?= addslashes(Yii::t('calendar', '_SEPTEMBER')); ?>',
				'<?= addslashes(Yii::t('calendar', '_OCTOBER')); ?>',
				'<?= addslashes(Yii::t('calendar', '_NOVEMBER')); ?>',
				'<?= addslashes(Yii::t('calendar', '_DECEMBER')); ?>'
			],
			monthNamesShort: [
				'<?= addslashes(Yii::t('calendar', '_JAN')); ?>',
				'<?= addslashes(Yii::t('calendar', '_FEB')); ?>',
				'<?= addslashes(Yii::t('calendar', '_MAR')); ?>',
				'<?= addslashes(Yii::t('calendar', '_APR')); ?>',
				'<?= addslashes(Yii::t('calendar', '_MAY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_JUN')); ?>',
				'<?= addslashes(Yii::t('calendar', '_JUL')); ?>',
				'<?= addslashes(Yii::t('calendar', '_AUG')); ?>',
				'<?= addslashes(Yii::t('calendar', '_SEP')); ?>',
				'<?= addslashes(Yii::t('calendar', '_OCT')); ?>',
				'<?= addslashes(Yii::t('calendar', '_NOV')); ?>',
				'<?= addslashes(Yii::t('calendar', '_DEC')); ?>'
			],
			dayNames: [
				'<?= addslashes(Yii::t('calendar', '_SUNDAY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_MONDAY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_TUESDAY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_WEDNESDAY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_THURSDAY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_FRIDAY')); ?>',
				'<?= addslashes(Yii::t('calendar', '_SATURDAY')); ?>'
			],
			dayNamesShort: [
				'<?= addslashes(Yii::t('calendar', '_SUN')); ?>',
				'<?= addslashes(Yii::t('calendar', '_MON')); ?>',
				'<?= addslashes(Yii::t('calendar', '_TUE')); ?>',
				'<?= addslashes(Yii::t('calendar', '_WED')); ?>',
				'<?= addslashes(Yii::t('calendar', '_THU')); ?>',
				'<?= addslashes(Yii::t('calendar', '_FRI')); ?>',
				'<?= addslashes(Yii::t('calendar', '_SAT')); ?>'
			],
			//Text to be displayed on calenda buttons
			buttonText: {
				month: '<?= addslashes(Yii::t('statistic', '_FOR_month')); ?>',
				week: '<?= addslashes(Yii::t('statistic', '_FOR_week')); ?>',
				day: '<?= addslashes(Yii::t('standard', '_DAY')); ?>'
			},
			//Calendar header element disposition
			header: {
				left: '',
				center: 'prev title next',
				right: ''//'agendaWeek,month'
			},
			//To set Monday as the first day of the week
			firstDay: 1,
			//RTL setting
			isRTL: <?= (Lang::isRTL(Yii::app()->getLanguage()) ? 'true' : 'false') ?>,
			//For display the full name of the day into the mounth view
			columnFormat: {
				month: 'ddd',
				week: 'dddd dd/MM',
				day: 'dddd dd/MM'
			},
			//Title format for the views
			titleFormat: {
				month: 'MMMM yyyy',
				week: "MMMM dd[ yyyy]{ '-'[ MMM] dd, yyyy}",
				day: 'dddd, dd MMMM, yyyy'
			},
			//Url for data fetch
			eventSources: [
				{
					data: $.deparam($('form.mycourses-filter-form', '#dashlet-<?= intval($dashletId); ?>').find('input[name!="YII_CSRF_TOKEN"]:enabled, select:enabled, textarea:enabled').serialize()),
					method: "POST",
					url: "./index.php?r=mycalendar/show/fetchCalendarItems&dashletId=<?= intval($dashletId) ?>",
					color: '<?= '#333333'/* $this->colors[0]; */ ?>',
					textColor: '#ffffff'
				}
			],
			//Remove the top corner text into week view
			allDayText: '',
			//Set the time format for the left cols into week and day views
			axisFormat: 'HH:mm',
			//Set the starting hour for the calendar week and day views
			firstHour: 0,
			//Set the action to do un event click, in our case the show of the tooltip
			eventClick: function(event, jsEvent, view)
			{
			},
			loading: function(is_loading, view)
			{
				if(is_loading)
					closePopup();
			},
			eventAfterRender: function(view)
			{
				//Needed for check if at last one event is rendered
				rendered = 1;
			},
			dayClick: function(clickedDate, allDay, jsEvent, view) {

				var clickedDateStart = new Date(clickedDate);
				var clickedDateEnd = new Date(clickedDateStart.getTime() + 1000*3600*24 - 1); // add 24 hours

				// Filter and get only events from the clicked date
				var eventsFromThatDay = calendar.fullCalendar('clientEvents', function(event){
					if(event.start >= clickedDateStart && event.start <= clickedDateEnd){
						return true;
					}
				});
				if(eventsFromThatDay.length===0){
					return;
				}

				// Hide all previous tooltips in this calendar
				$('.tooltipevent', 'body').remove();

				var tooltip = $('<div class="tooltipevent"></div>');
				tooltip.hide();
				$('body').append(tooltip);

				var passData = function() {
					var datesObject = {
						start: Math.floor(clickedDateStart.getTime() / 1000),
						end: Math.floor(clickedDateEnd.getTime() / 1000)
					};

					var formData = $.deparam($('form.mycourses-filter-form', '#dashlet-<?= intval($dashletId); ?>').find('input[name!="YII_CSRF_TOKEN"]:enabled, select:enabled, textarea:enabled').serialize());
					var output = $.extend({}, formData, datesObject);

					return output;
				};

				//Load the tooltip body
				$.ajax({
					type: "POST",
					url: "./index.php?r=mycalendar/show/fetchCalendarItems&dashletId=<?= intval($dashletId) ?>&getTooltip=1",
					async: false,
					cache: true,
					data: passData(),
					success:function(result)
					{
						tooltip.html(result);
					}
				});

				//Display the tooltip
				tooltip.fadeIn(500);

				// Set the tooltip position
				var position = $(this).offset();

				tooltip.css('position', 'absolute');
				var tooltipTopMargin = position.top - (tooltip.outerHeight()+12);
				if(tooltipTopMargin < 0) tooltipTopMargin = 5;
				tooltip.css('top', tooltipTopMargin);
				tooltip.css('left', position.left + ($(this).width() / 2) - (tooltip.width()/2));// + 30
				$('.triangle', tooltip).css('left', ((tooltip.width() / 2) - ($('.triangle', tooltip).width() / 2)));

				$('.close', tooltip).click(function(){
					$(this).closest('.tooltipevent').remove();
				});

			},
			eventRender: function(event, element) {

				// Add a small blue arrow at the top right
				// of dates that have at least one event in them

				var eventStartDate = event.start;

				var curr_date = ('0' + eventStartDate.getDate()).slice(-2);
				var curr_month = ('0' + (eventStartDate.getMonth()+1)).slice(-2);
				var curr_year = eventStartDate.getFullYear();

				var daysUis = calendar.find('.fc-day');
				daysUis.each(function(){
					if($(this).data('date')===curr_year + '-' + curr_month+'-'+curr_date){
						if(!$(this).hasClass('hasEvents')){ // multiple events shouldn't add multiple top right arrows
							$(this).addClass('hasEvents');
							$(this).prepend('<div class="top-right-arrow"></div>');
						}
					}
				});

				// Don't render the events on the calendar
				// "return false" prevents this. We will render
				// the events in a tooltip when the date is clicked
				return false;
			},
			viewRender: function(view, uiElem){
				customHeightFixerMethod(calendar);
			},
			windowResize: function(uiElem){
				customHeightFixerMethod(calendar);
			},
			dayRender: function(date, cell){
				customHeightFixerMethod(calendar);
			},
			loading: function(loading, view)
			{
				customHeightFixerMethod(calendar);

				if(loading)
					rendered = 0;
				else if(rendered == 0)
				{
					$.ajax({
						type: "POST",
						url: "./index.php?r=mycalendar/show/fetchCalendarItems&dashletId=<?= intval($dashletId) ?>",
						async: true,
						data: $.deparam($('form.mycourses-filter-form', '#dashlet-<?= intval($dashletId); ?>').find('input[name!="YII_CSRF_TOKEN"]:enabled, select:enabled, textarea:enabled').serialize()),
						success:function(result)
						{
							var json = jQuery.parseJSON(result);
							if(json.success)
							{
								calendar.fullCalendar('gotoDate', json.year, json.month, json.day);
							}
						}
					});
				}
			}
		});
	})
</script>