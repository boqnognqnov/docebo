<?php
/* @var $this DashletCatalog */
/* @var $topRatedCourses LearningCourse[] */
?>
<ul class="top-rated-courses clearfix">
	<?php
	$_topRatedIndex = 1;
	$starRatingJs = '';
	foreach ($topRatedCourses as $topRatedCourse) : ?>
		<li class="clearfix">
			<span class="index"><?= $_topRatedIndex ?></span>
			<span class="course-image">
				<img src="<?= $topRatedCourse->getCourseLogoUrl() ?>" class="">
			</span>
			<span class="course-title">
			<?php
				$already_subscribed = (!Yii::app()->user->getIsGuest() && LearningCourseuser::model()->isSubscribed(Yii::app()->user->id, $topRatedCourse->idCourse));
				if($already_subscribed){
					$topRatedCourseLink = Docebo::createAbsoluteUrl('lms:player', array('course_id' => $topRatedCourse->idCourse));
				}else{
					$topRatedCourseLink = Docebo::createAppUrl('lms:course/details', array('id' => $topRatedCourse->idCourse));
				}
			?>
				<a href="<?= $topRatedCourseLink ?>"><?= Docebo::ellipsis($topRatedCourse->name, 30) ?></a>
				<span class="course-rating">
                   <div id="social_rating_course" class="fixed_rating <?php echo $thumbClass == 'tb' ? 'thumb' : 'list' ?>">
                        <span class="star-type-rating rating-container" data-idcourse="<?php echo $topRatedCourse->idCourse; ?>">
                            <?php
                                $rating = $topRatedCourse->getRating();
                                for($i=0; $i<5; $i++)
                                    echo '<div role="text" class="star-rating star-rating'. ($rating > $i ? "-on" : "") .'" id="course-rating-58_'.$i.'"><a title="'.$i.'">'.$i.'</a></div>';
                            ?>
                        </span>
                       <span class="rating-value fixed_rating"><?= $topRatedCourse->getRating() ?></span>
                   </div>
				</span>
			</span>
		</li>
		<?php $_topRatedIndex++; ?>
	<?php endforeach; ?>
</ul>

<?php if($starRatingJs): ?>
<script>
	$(function() {
		<?= $starRatingJs ?>
	});
</script>
<?php endif; ?>