<?php
/* @var $this DashletCatalog */
/* @var $categoriesTree */
/* @var $treeId */
/* @var $enableFilter */
/* @var $isInSidebar */
?>

<div class="catalog-categories-tree-container">
	<?php
	$dashletId = json_encode((int) $this->dashletModel->id);
	$dashletSelector = '[id="dashlet-'.$dashletId.'"]';
	$hiddenFieldId = 'current-category-dashlet-'.$dashletId.($isInSidebar ? '-sidebar' : '-filter');
	$this->widget('common.extensions.fancytree.FancyTree', array(
		'id' => $treeId,
		'treeData' => $categoriesTree,
		'fancyOptions' => array (
			'checkbox' => false,
			'selectMode' => 1,
		),
		'eventHandlers' => array (
			'focus' => "function(e, d) { $($dashletSelector).mycourses('categoryTreeFocusEvent', e, d, '$hiddenFieldId'); }",
			"renderNode" => "function(e, d) { $($dashletSelector).mycourses('renderCategoryNode', e, d); }",
			'beforeSelect' => "function(e, d) { $($dashletSelector).mycourses('categoryTreeSelectEvent', e, d); }",
			'select' => "function(e, d) { $($dashletSelector).mycourses('categoryTreeSelectEvent', e, d); }",
			'init' => "function(e, d) { $($dashletSelector).mycourses('initCategoryTree', e, d, '$this->fltCategory'); }"
		)
	));

	echo CHtml::hiddenField('category', $this->fltCategory, array(
		'id' => $hiddenFieldId,
		"disabled" => ($enableFilter ? '' : 'disabled')
	));
	?>
</div>