<?php
	/* @var $course LearningCourse */
	/* @var $this 	DashletCatalog */


	$playUrl = Docebo::createLmsUrl("/player", array(
		'course_id'	=> $course->idCourse,
	));

	$buyUrl = Docebo::createLmsUrl("/player", array(
		'course_id'	=> $course->idCourse,
	));


	$courseDetailsUrl = Docebo::createLmsUrl('course/axDetails', array('id' => $course->idCourse));

	$idUser = Yii::app()->user->id;
	$locked = false;
	$waitingApproval = false;
	$currencySymbol = Settings::get("currency_symbol");
	$price = number_format((double)$course->getCoursePrice(),2);

	// Resolve how and if the course can be played, enrolled, etc. and return data for labels, icons, classes (presentation data)
	$accessData = $course->resolveCourseAccess();
	$enrollment = isset($accessData['enrollment']) ? $accessData['enrollment'] : false;



?>

<div class="course-tile">

	<?php
		if ($accessData['linkTag']) {
			echo $accessData['linkTag'];
		}
	?>

		<div class="course-logo">
			<div class="thumb">

				<!-- Course tags: in thumbnail -->
				<?php if (count($accessData['tags']) > 0): ?>
					<div class="course-tags">
						<?php foreach ($accessData['tags'] as $tag) : ?>
							<span class="course-tag <?= $tag['class'] ?>" rel="tooltip" title="<?= $tag['tooltip'] ?>"><?= $tag['label'] ?></span>
						<?php endforeach;?>
					</div>
				<?php endif; ?>

				<?= CHtml::image($course->getCourseLogoUrl(CoreAsset::VARIANT_ORIGINAL, true), $course->name, array()) ?>
			</div>

			<p class="course-action <?= $accessData['action']['class'] ?>">


				<!-- Course tags: in action label -->
				<?php if (count($accessData['tags']) > 0): ?>
					<span class="course-tags">
						<?php foreach ($accessData['tags'] as $tag) : ?>
							<span class="course-tag <?= $tag['class'] ?>"><?= $tag['label'] ?></span>
						<?php endforeach;?>
					</span>
				<?php endif; ?>


				<span class="icon">
					<i class="<?= $accessData['action']['icon'] ?>"></i>
				</span>
				<?php if ($accessData['canBuy']) : ?>
					<span class="price">
                        <?php
                            // Raise event right before showing course price, some plugin may want to modify it
                            $originalPrice = $currencySymbol . " " . $price;
                            $event = new DEvent($this, array('course' => $course, 'originalPrice' => $originalPrice));
                            Yii::app()->event->raise('onBeforeShowCoursePrice', $event);
                            if($event->return_value){
                                echo $event->return_value;
                            }else{
                                echo $originalPrice;
                            }
                        ?>
					</span>
				<?php endif; ?>
				<span class="action-label"><?= $accessData['action']['label'] ?></span>
			</p>

			<h3 class="course-name"><?= $course->name ?></h3>
		</div>
		<div class="clearfix"></div>

	<?php
		if ($accessData['linkTag']) {
			echo "</a>";
		}
	?>


	<div class="course-info">
		<?= $course->getCourseTypeTranslation() ?>&nbsp;
		<span class="extra-course-info">

			<?php if ($course->lang_code) : ?>
				<a rel="tooltip" title="<?= strtoupper(Lang::getBrowserCodeByCode($course->lang_code)) ?>"><i class="fa fa-globe"></i></a>
			<?php endif; ?>

			<?php if ($course->mediumTime) : ?>
				<a rel="tooltip" title="<?= $course->formatAverageCourseTime() ?>"><i class="fa fa-clock-o"></i></a>
			<?php endif; ?>

			<?php if ($enrollment && $enrollment->level) : ?>
				<?php $level = Yii::t('levels', '_LEVEL_' . $enrollment->level); ?>
				<a rel="tooltip" title="<?= ucfirst($level) ?>"><i class="fa fa-user"></i></a>
			<?php endif; ?>

		</span>

		<?php if($course->canViewRating()):?>
			<br />
			<div id="social_rating_course" class="fixed_rating <?php echo $thumbClass == 'tb' ? 'thumb' : 'list' ?>">
				<span class="star-type-rating rating-container" data-idcourse="<?php echo $course->idCourse; ?>">
					<?php
					$rating = $course->getRating();
					for($i=0; $i<5; $i++)
						echo '<div role="text" class="star-rating star-rating'. ($rating > $i ? "-on" : "") .'" id="course-rating-58_'.$i.'"><a title="'.$i.'">'.$i.'</a></div>';
					?>
				</span>
				<span class="rating-value fixed_rating"><?= $course->getRating() ?></span>
			</div>
		<?php endif; ?>
	</div>

</div>
