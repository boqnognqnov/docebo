<?php
// Display only if there are additional fields to be filtered by
if (!empty($additionalCourseFieldsModels) && is_array($additionalCourseFieldsModels) && count($additionalCourseFieldsModels) > 0) {
	$addFields = array();
	foreach($additionalCourseFieldsModels as $additionalCourseFieldsModel) {
		$addFields[$additionalCourseFieldsModel->id_field] = $additionalCourseFieldsModel->getTranslation();
	}

?>
<div class="row-fluid">
	<div class="span12">
<?php
	echo CHtml::label(Yii::t('course', 'Advanced custom fields filtering'), 'select_statement', array('style' => 'font-weight: bold'));
?>

		<div class="row-fluid">
			<div class="span11 select-statement-container">
<?php
	echo CHtml::dropDownList('select_statement', '',$addFields, array('class' => 'course-filter-fields-choose', 'style' => 'width: 100%'));
?>
			</div>
			<div id="add-course-field-condition-container" class="span1" style="margin-top: 5px; margin-left: 0;">
				<a href="javascript:;" id="add-course-field-condition<?=$dashletId; ?>"><i class="green-text fa fa-plus-circle fa-lg1"></i></a>
			</div>
		</div>
	</div>
</div>

<hr class="course-fields-fielters-delimeters" />

<div class="row-fluid">
	<div class="span12">
<?php
	//
	foreach($additionalCourseFieldsModels as $additionalCourseFieldsModel) {
		// show filter fields
		echo $additionalCourseFieldsModel->renderFilter();
	}
?>
	</div>
</div>

<hr class="course-fields-fielters-delimeters" />

<?php

}

$showFilterRows = array();

if (!empty($_REQUEST['advancedSearch']['additional']) && is_array($_REQUEST['advancedSearch']['additional']) && (count($_REQUEST['advancedSearch']['additional']) > 0)) {
	foreach($_REQUEST['advancedSearch']['additional'] as $idItem => $itemDetails) {
		if (!empty($itemDetails['type'])) {
			$showFilterRows[$idItem] = $idItem;
		}
	}
}

?>

<script type="text/javascript">
	(function($) {
		var showFilterRows<?=$dashletId; ?> = [<?=implode(',', $showFilterRows); ?>];
		var showCourseFieldsFieltersDelimeters = false;

		$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>.course-fields-fielters-delimeters').hide();

		// Hide and disable course fields filters
		$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>.row-course-field').each(function(index) {
			// exclude, the fields, that were populated and passed to be applied to the filter last apply button click
			if ($.inArray($(this).data('filter-value'), showFilterRows<?=$dashletId; ?>) == -1) {
				$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>.row-field-' + $(this).data('filter-value')).hide();
				$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>.row-field-' + $(this).data('filter-value') + ' input, <?='#mycourses-filter-form-' . $dashletId . ' '; ?>.row-field-' + $(this).data('filter-value') + ' select').prop('disabled', 'disabled');//true);
			} else {
				$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>.course-fields-fielters-delimeters').show();
			}

			// Remove the shown course fields from the dropdown list
			$.each(showFilterRows<?=$dashletId; ?>, function( index, value ) {
				$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>.course-filter-fields-choose option[value="'+value+'"]').remove();
			});
		});

		// Remove course field filter
		$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>.remove-advanced-filter-link').off();
		$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>.remove-advanced-filter-link').on('click', function() {
			$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>.row-field-'+$(this).data('filter-value')).hide();
			$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>.row-field-'+$(this).data('filter-value')+' input, <?='#mycourses-filter-form-' . $dashletId . ' '; ?>.row-field-'+$(this).data('filter-value')+' select').prop('disabled', 'disabled');//true);
			$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>.course-filter-fields-choose').append($('<option>', {
				value: $(this).data('filter-value'),
				text: $(this).data('filter-text')
			}));

			// Should we hide the course fields delimiters
			var rowCourseFieldsCount = $('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>.row-course-field').length;
			var hiddenCount =0;
			if (rowCourseFieldsCount > 0) {
				$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>.row-course-field').each(function(index, value) {
					if (this.style.display == "none")
					hiddenCount++;
				});

				if (rowCourseFieldsCount == hiddenCount) {
					$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>.course-fields-fielters-delimeters').hide();
				}
			}
		});

		// Add course field filter
		$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>#add-course-field-condition<?=$dashletId; ?>').off();
		$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>#add-course-field-condition<?=$dashletId; ?>').on('click', function() {
			$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>.row-field-'+$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>.course-filter-fields-choose').val()).show();
			$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>.row-field-'+$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>.course-filter-fields-choose').val()+' input, <?='#mycourses-filter-form-' . $dashletId . ' '; ?>.row-field-'+$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>.course-filter-fields-choose').val()+' select').prop('disabled', false);
			$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>.course-filter-fields-choose option[value="'+$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>.course-filter-fields-choose').val()+'"]').remove();
			$('<?='#mycourses-filter-form-' . $dashletId . ' '; ?>.course-fields-fielters-delimeters').show();
		});
	}) (jQuery);
</script>
