<p><strong><?=Yii::t('standard', '_SHOW')?></strong></p>
<span class="setting-description compact"><?= Yii::t('standard', 'You must select at least one item. Drag & drop to reorder items')?></span>
<br/>

<div class="auto-sortable">
	<? foreach($settings as $key=>$label): ?>
	
		<div class="row-fluid setting-row <?=$key=='total_sessions_last_year' ? 'prevent-sortable' : null?>">
			<div class="span12 setting <?=$key=='total_sessions_last_year' ? 'prevent-sortable' : null?>">
				<label class="checkbox">
					<?=CHtml::checkBox($key, $this->getParam($key, false))?>
					<?=$label?>
					<span class="p-sprite move setting-handle pull-right"></span>
				</label>
			</div>
		</div>
		
	<? endforeach; ?>
	<?=CHtml::hiddenField('field_order', implode(',', array_keys($settings)))?>
</div>