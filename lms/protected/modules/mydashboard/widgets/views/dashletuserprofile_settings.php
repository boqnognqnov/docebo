

<div>
	<?=CHtml::checkBox('show_profile_pic', $this->getParam('show_profile_pic'))?>
	<?=Yii::t('userprofile', "Show user's profile picture")?>
</div>

<br/>

<div>
	<?=CHtml::checkBox('show_email', $this->getParam('show_email'))?>
	<?=Yii::t('userprofile', "Show email")?>
</div>

<br/>

<div>
	<?=CHtml::checkBox('show_my_activities_link', $this->getParam('show_my_activities_link'))?>
	<?=Yii::t('userprofile', 'Show "view my activities" link')?>
</div>

<br/>

<div>
	<?=Yii::t('userprofile', "Show the following additional fields")?>
	<br/>
	<div id="additional-fields-select">
		<select name="additional_fields_arr">
			<?php
			if (count($additionalFieldsSelected) > 0) {
				foreach ($additionalFieldsSelected as $id=>$label) {
					$encLabel = CHtml::encode($label);
					echo "<option class=\"selected\" value=\"$id\" selected=\"selected\">$encLabel</options>";
				}
			}
			?>
		</select>
	</div>
</div>



<script type="text/javascript">
	$(function(){
		$('input').styler();

		$("#additional-fields-select select").fcbkcomplete({
			json_url: '<?=Docebo::createAbsoluteLmsUrl('//mydashboard/DashletWidgets/axAdditionalFieldsAutocomplete')?>',
			addontab: true,
			width: '98.5%',
			cache: true,
			complete_text: '',
			maxshownitems: 5,
			input_name: 'maininput-name',
			filter_selected: true
		});
	})
</script>