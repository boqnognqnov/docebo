
<div class="row-fluid">
	<div class="control-group">
		<label class="control-label">Field 1</label>
		<div class="controls">
           	<div class="row-fluid">
               	<?= CHtml::textField('example_field_1', $this->params['example_field_1'], array('class' => 'span12')) ?>
           	</div>
        </div>
    </div>
</div>

<div class="row-fluid">
	<div class="control-group">
		<label class="control-label">Field 2</label>
		<div class="controls">
           	<div class="row-fluid">
               	<?= CHtml::textField('example_field_2', $this->params['example_field_2'], array('class' => 'span12')) ?>
           	</div>
        </div>
    </div>
</div>


<div class="row-fluid">
	<div class="control-group">
		<label class="control-label">Types</label>
		<div class="controls">
           	<div class="row-fluid">
               	<?= 
               		CHtml::dropDownList('selected_types', $this->params['selected_types'], array('1' => 'Type 1', '2' => 'Type 2', '3' => 'Type 3', '4' => 'Type 4'), array(
               			'multiple' => 'multiple'
               		))
               	 ?>
           	</div>
        </div>
    </div>
</div>


