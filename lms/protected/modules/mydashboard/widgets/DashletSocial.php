<?php

/**
 * Class DashletSocial
 *
 * Format for $this->params:
 * array('active_services'=>
 *        array(
 *          array('service'=>'facebook', 'url'=>'http://facebook.com/pageName', 'hover_text'=>'bla1'),
 *          array('service'=>'twitter', 'url'=>'http://twitter.com/pageName', 'hover_text'=>'bla2'),
 *        )
 * )
 */
class DashletSocial extends DashletWidget  implements IDashletWidget {

	private $availableServices = array();

	/**
	 * This method is called internally (when this widget is used using $this->widget())
	 * or externally (when e.g. Controllers need to know the available social services)
	 *
	 * @return array
	 */
	public static function getAvailableServices(){
		// @TODO raise an event to allow other plugins to insert social services here
		return array(
			'facebook'=>array(
				'label'=>'Facebook',
			),
			'linkedin'=>array(
				'label'=>'Linkedin',
			),
			'twitter'=>array(
				'label'=>'Twitter',
			),
			'instagram'=>array(
				'label'=>'Instagram',
			),
			'pinterest'=>array(
				'label'=>'Pinterest',
			),
			'youtube'=>array(
				'label'=>'Youtube',
			)
		);
	}

	public function init(){
		$this->availableServices = self::getAvailableServices();
		parent::init();
	}
	
	
	/**
	 * @see IDashletWidget::renderFrontEnd()
	 */
	public function renderFrontEnd() {
		$activeServices = isset($this->params['active_services']) && is_array($this->params['active_services']) ? $this->params['active_services'] : array();
		$servicesMeta = isset($this->params['services_meta']) && is_array($this->params['services_meta']) ? $this->params['services_meta'] : array();
		$availableServices = self::getAvailableServices();

		foreach($activeServices as $key=>$value){
			// Remove services from the array which
			// we don't have available in the platform
			if(!in_array($value, array_keys($availableServices))){
				unset($activeServices[$key]);
			}
		}

		$this->render(strtolower(__CLASS__), array(
			'activeServices'=>$activeServices,
			'servicesMeta'=>$servicesMeta,
			'availableServices'=>$this->availableServices,
		));
	}


	/**
	 * @see IDashletWidget::renderSettings()
	 */
	public function renderSettings() {
		$cs = Yii::app()->getClientScript();
		$themeUrl = Yii::app()->theme->baseUrl;

		// FCBK
		$cs->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
		$cs->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');

		$activeServices = isset($this->params['active_services']) && is_array($this->params['active_services']) ? $this->params['active_services'] : array();
		$servicesMeta = isset($this->params['services_meta']) && is_array($this->params['services_meta']) ? $this->params['services_meta'] : array();
		$availableServices = self::getAvailableServices();

		$previousActiveServicesToRestore = array();
		$previousServiceMetas = array();

		foreach($activeServices as $codename){
			// Remove services from the array which
			// we don't have available in the platform
			if(in_array($codename, array_keys($availableServices))){
				$previousActiveServicesToRestore[$codename] = $availableServices[$codename]['label'];
			}
		}

		foreach($servicesMeta as $codename=>$meta){
			// Remove services from the array which
			// we don't have available in the platform
			if(in_array($codename, array_keys($availableServices))){
				$previousServiceMetas[$codename] = $meta;
			}
		}

		$this->render('dashletsocial_settings', array(
			'availableServices'=>$availableServices,
			'previousActiveServicesToRestore'=>$previousActiveServicesToRestore,
			'previousServiceMetas'=>$previousServiceMetas,
		));
	}
	

	protected function getServiceName($serviceCodename){
		if(isset($this->availableServices[$serviceCodename])){
			return $this->availableServices[$serviceCodename]['label'];
		}
		return null;
	}

	protected function getServiceIcon($serviceCodename){
		return "<div class='icon {$serviceCodename}'></div>";
	}
	
	/**
	 *
	 */
	public static function descriptor($params=false) {
		$sampleImageUrl = Yii::app()->mydashboard->getModule()->getAssetsUrl() . "/images/social_sample.jpg";
		
		$descriptor = new DashletDescriptor();
		$descriptor->name 					= 'core_dashlet_social';
		$descriptor->handler				= 'mydashboard.widgets.DashletSocial';
		$descriptor->title					= Yii::t('myactivities', 'Social');
		$descriptor->description			= Yii::t('dashlets', 'Shows a list of social networks icons with links to the corresponding pages');
		$descriptor->sampleImageUrl 		= $sampleImageUrl;
		$descriptor->settingsFieldNames		= array('active_services', 'services_meta');
		
		return $descriptor;
	}

	/**
	 * @see IDashletWidget::bootstrap()
	 */
	public function bootstrap($params=false) {
	}

}