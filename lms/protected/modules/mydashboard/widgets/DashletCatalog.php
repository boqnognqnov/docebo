<?php

class DashletCatalog extends DashletWidget implements IDashletWidget{


	/**
	 * Display styles/modes
	 * @var string
	 */
	const DISPLAY_STYLE_THUMB 	= 'thumb';
	const DISPLAY_STYLE_LIST 	= 'list';
	const DISPLAY_STYLE_CALENDAR 	= 'calendar';

	/**
	 * Catalog item types
	 * @var string
	 */
	const TYPE_ELEARNING		= LearningCourse::TYPE_ELEARNING;
	const TYPE_CLASSROOM		= LearningCourse::TYPE_CLASSROOM;
	const TYPE_WEBINAR			= LearningCourse::TYPE_WEBINAR;
	const TYPE_LEARNING_PLAN	= 'learningplan';


	const PRICING_TYPE_PAID		= 'paid';
	const PRICING_TYPE_FREE		= 'free';

	protected static $handler	= 'mydashboard.widgets.DashletCatalog';

	public $fltSearch;
	public $fltItemType;
	public $fltPricingType;
	public $fltCatalog;
	public $fltCategory;
	public $fltCourseFields;
	public $fltNotEnrolled;

	public $pageSize;
	public $pageNumber;

	public $return_to_top = false;

	/**
	 * @see DashletWidget::init()
	 */
	public function init() {
		parent::init();

		if(Yii::app()->request->getParam('content_refresh', false) !== false)
			$this->return_to_top = true;

		// Collect filtering information form Request, if any (in which case, set them to "initial" ones from settings, if any)
		$this->fltItemType 		= Yii::app()->request->getParam('item_type', $this->getParam('initial_item_type', self::TYPE_ELEARNING));
		$this->fltSearch		= Yii::app()->request->getParam('search_input', false);
		$this->fltCourseFields	= Yii::app()->request->getParam('advancedSearch', array());
		$this->fltNotEnrolled	= Yii::app()->request->getParam('not_enrolled_courses', 0);

		if(!Settings::get("module_ecommerce", "off") == "on")
			$this->fltPricingType 	= self::PRICING_TYPE_FREE; // Force display of only Free courses if the ecommerce app is disabled
		else
			$this->fltPricingType 	= Yii::app()->request->getParam('pricing_type', $this->getParam('initial_pricing_type', 'all'));

		// *** Catalog filter ***
		// Check if the current catalog ID filter (e.g. initial dashlet setting) is among the catalogs visible to the current user. If not, revert to all
		$this->fltCatalog = Yii::app()->request->getParam('catalog', $this->getParam('initial_catalog', 'all'));

		// This will include Public catalogs if the user is not logged in !!! /if there are set/
		$userVisibleCatalogs = $this->getUserAvailableCatalogs(false);

		if(!isset($userVisibleCatalogs[$this->fltCatalog]))
			$this->fltCatalog = 'all';

		// *** Category filter ***
		$this->fltCategory 		= Yii::app()->request->getParam('category', 'all');

		// Page size and Page number
		if ($this->idLayout < 0) {
			$this->pageSize	= Yii::app()->request->getParam('psize', $this->getParam('display_number_items_fullist', false));

			// We decided to force pagination for Virtual layouts
			if (!$this->pageSize) {
			    $this->pageSize = DashboardLayout::getVirtualLayoutPageSize();
			}

		}
		else {
			$this->pageSize	= Yii::app()->request->getParam('psize', $this->getParam('display_number_items', false));
		}

		$this->pageNumber = Yii::app()->request->getParam('pnumber', 0);
	}


	/**
	 * Return list of translated display modes
	 *
	 * @return array
	 */
	public static function getDisplayStyles() {
		return array(
				self::DISPLAY_STYLE_THUMB 	        => Yii::t('dashboard', 'Thumbnails'),
				self::DISPLAY_STYLE_LIST 	        => Yii::t('dashboard', 'List'),
				self::DISPLAY_STYLE_CALENDAR 	    => Yii::t('standard', '_CALENDAR'),
		);
	}

	/**
	 * Returns the catalog model by ID
	 * @param $catalogId
	 *
	 * @return LearningCatalogue
	 */
	public function getCatalogById($catalogId) {
		return LearningCatalogue::model()->findByPk($catalogId);
	}

	/**
	 * Return list of catalog items (courses, learning plans, ...)
	 */
	public function getCatalogItems() {

		// Each item in the returned array will have the following
		// columns that will be populate with the single catalog
		// entry's information, depending on the type of entry (course
		// or Learning plan). E.g. "name" will be either the name of the course
		// if that catalog entry/item is a course, but will contain the Learning
		// plan's name if it's a learning plan:
		// - catalog_item_type ("course" or "coursepath")
		// - id (id of course or id of LP)
		// - name (of the course or learning plan)
		// - description
		// - is_selling
		// - price
		// - img (integer; corresponds to CoreAsset entry)
		// Also select the following columns are specific to "course" entries only,
		// they are not available (NULL) for Learning Plan rows:
		// status, allow_overbooking, max_num_subscribe
		$items = array('all' => array());
		$sortingFields = $this->getParam('selected_sorting_fields');
		$showAllCoursesIfNoCatalogueIsAssigned = Yii::app()->user->isGuest || ('on' == Settings::get('on_catalogue_empty', 'on'));
		$idUser = Yii::app()->user->getIdst();

		// Load the list of catalogs this use can see
		$userCatalogs = false;
		$userAllCatalogs = array();
		if(!(Yii::app()->user->isGuest)) {
			$userCatalogs = self::getUserAvailableCatalogs(false);
			$userAllCatalogs = self::getUserAvailableCatalogs(true, true);//false);
		} else {
			// Get Catalogs visible for not logged members( selected public catalogs)
			$catalog_external_selected_catalogs = Settings::get('catalog_external_selected_catalogs', '');
			if ($catalog_external_selected_catalogs != '') {
				$catalog_external_selected_catalogs = explode(',', $catalog_external_selected_catalogs);

				if (is_array($catalog_external_selected_catalogs) && count($catalog_external_selected_catalogs) > 0) {
					$criteria = new CDbCriteria();
					$criteria->addInCondition('idCatalogue', $catalog_external_selected_catalogs);
					$sql = 'SELECT cat.idCatalogue, cat.name FROM ' . LearningCatalogue::model()->tableName().' cat WHERE '
							. $criteria->condition;
					$command = Yii::app()->db->createCommand($sql);

					$userCatalogs = $command->queryAll(true, $criteria->params);
					if (!empty($userCatalogs)) {
						foreach($userCatalogs as $catalog) {
							$catalogs[$catalog['idCatalogue']] = $catalog['name'];
						}

						$userCatalogs = $catalogs;
					}
				}
			}
		}

		// If there's a filter on a category, let's load all its descendants to build the query
		$categories = array();
		if($this->fltCategory && $this->fltCategory !== 'all')
			$categories = LearningCourseCategoryTree::model()->getCategoryAndDescendants($this->fltCategory);

		// Load list of courses the current user is subscribed in
		if(!(Yii::app()->user->isGuest)) {
			$subscribedCoursesList = CoreUser::model()->getSubscribedCoursesList($idUser, TRUE);

			// Load course IDs that we know will have to be excluded (either because of show_rules or custom plugin rules)
			$coursesToNotDisplayList = LearningCourse::model()->getCoursesToNotDisplay($subscribedCoursesList);
		} else {
			$subscribedCoursesList = array();
			$coursesToNotDisplayList = array();
		}

		// Should we exclude the courses user is already subscribed?
		if(!(Yii::app()->user->isGuest) && ($this->fltNotEnrolled == 1)) {
			$enrolledCoursesList = CoreUser::model()->getSubscribedCoursesList($idUser, false);
			$coursesToNotDisplayList = array_merge($coursesToNotDisplayList, $enrolledCoursesList);
		}

		$coursesExcludedByPlugins = array();
		Yii::app()->event->raise('OnCatalogQueryCourses', new DEvent($this, array('excludedCourses' => &$coursesExcludedByPlugins)));
		if (!empty($coursesExcludedByPlugins))
			$coursesToNotDisplayList = array_merge($coursesToNotDisplayList, $coursesExcludedByPlugins);

		if(empty($userCatalogs) || (is_array($userCatalogs) && count($userCatalogs) == 1 && isset($userCatalogs['all']))) {
			if(!$showAllCoursesIfNoCatalogueIsAssigned || (count($userAllCatalogs) > 1)) {
				// User doesn't have any catalogs assigned and the setting says to not
				// show all items in this case, so show nothing
				return $items;
			} else {
				// The setting "If no catalogs assigned to user show all courses" is enabled
				// Show all and every item. And by all we really mean ALL (of course, respecting offset and limit)
				if (!(Yii::app()->user->isGuest)) {
					$sql_courses =	"select c.idCourse as id, 'course' as catalog_item_type, c.name as name, c.description as description, c.prize as price, c.selling as is_selling, c.img_course as img, c.status as `status`, c.allow_overbooking as allow_overbooking, c.max_num_subscribe as max_num_subscribe, c.create_date as create_date, cu.status as status_order, c.code as code"."\n"
							." from ".LearningCourse::model()->tableName()." as c"."\n";

					$sql_courses .=
							" left join " . LearningCourseuser::model()->tableName() . " as cu on cu.idCourse = c.idCourse and cu.idUser = " . $idUser . "\n";
				} else {
					$sql_courses =	"select c.idCourse as id, 'course' as catalog_item_type, c.name as name, c.description as description, c.prize as price, c.selling as is_selling, c.img_course as img, c.status as `status`, c.allow_overbooking as allow_overbooking, c.max_num_subscribe as max_num_subscribe, c.create_date as create_date, NULL as status_order, c.code as code"."\n"
							." from ".LearningCourse::model()->tableName()." as c"."\n";
				}

				$sql_courses .=
								" left join ".LearningCourseFieldValue::model()->tableName() ." as lcfv on lcfv.id_course = c.idCourse  "."\n"
								." where c.status IN (".LearningCourse::$COURSE_STATUS_EFFECTIVE.", ".LearningCourse::$COURSE_STATUS_AVAILABLE.")"."\n";

				if($this->fltItemType && $this->fltItemType !== 'all')
					$sql_courses .= " and c.course_type='".addslashes($this->fltItemType)."'"."\n";

				if($this->fltCategory && $this->fltCategory != 'all')
					$sql_courses .= " and c.idCategory in (".implode(',', $categories).")"."\n";

				if($this->fltSearch)
					$sql_courses .= " and (c.name like '%".addslashes(str_replace(' ', '%', $this->fltSearch))."%' or c.description like '%".addslashes(str_replace(' ', '%', $this->fltSearch))."%' or c.code like '%".addslashes(str_replace(' ', '%', $this->fltSearch))."%')"."\n";

				// If there is any filter by course fields to be applied
				$fltCourseFields = $this->fltCourseFields['additional'];
				$hideLearningPlans = false;
				if(!empty($fltCourseFields) && is_array($fltCourseFields) && count($fltCourseFields) > 0) {
					$hideLearningPlans = true;

					foreach($this->fltCourseFields['additional'] AS $filterItemId => $filterItem) {
						if (!empty($filterItem['type'])) {
							switch ($filterItem['type']) {
								case 'dropdown':
									if (!empty($filterItem['value'])) {
										$sql_courses .= ' and lcfv.field_' . $filterItemId . ' =' . addslashes($filterItem['value']);
									}
									break;

								case 'date':
									if (!empty($filterItem['value'])) {
										$sql_courses .= ' and lcfv.field_' . $filterItemId . $filterItem['condition'] . " '" .  Yii::app()->localtime->fromLocalDate($filterItem['value']) . "' ";
									}

									break;

								case 'textfield':
									if (!empty($filterItem['value'])) {
										switch($filterItem['condition']) {
											case 'contains':
												$sql_courses .= ' and lcfv.field_' . $filterItemId . " LIKE '%" . addslashes($filterItem['value']) ."%' ";
												break;

											case 'equal':
												$sql_courses .= ' and lcfv.field_' . $filterItemId . " = '" . addslashes($filterItem['value']) ."' ";
												break;

											case 'not-equal':
												$sql_courses .= " and COALESCE(lcfv.field_" . $filterItemId . ", '') != '" . addslashes($filterItem['value']) . "' ";
												break;

											default:
												break;
										}
									}

									break;

								case 'textarea':
									if (!empty($filterItem['value'])) {
										switch($filterItem['condition']) {
											case 'contains':
												$sql_courses .= ' and lcfv.field_' . $filterItemId . " LIKE '%" . addslashes($filterItem['value']) ."%' ";
												break;

											case 'not-contains':
												$sql_courses .= ' and COALESCE(lcfv.field_' . $filterItemId . ", '') NOT LIKE '%" . addslashes($filterItem['value']) ."%' ";
												break;

											default:
												break;
										}
									}

									break;
								default:
									// Not an internal field type -> instantiate the custom field handler and let it process the case
									$fieldName = 'CourseField'.ucfirst($filterItem['type']);
									if(method_exists($fieldName, "applyCourseCatalogFilter"))
										$fieldName::applyCourseCatalogFilter($filterItemId, $filterItem['value'], $sql_courses);
							}
						}
					}
				}

				if($this->fltPricingType)
				{
					switch($this->fltPricingType)
					{
						case self::PRICING_TYPE_FREE:
							$sql_courses .= " and c.selling = 0"."\n";
						break;
						case self::PRICING_TYPE_PAID:
							$sql_courses .= " and c.selling = 1"."\n";
						break;
					}
				}

				// Add the exclude courses condition to the main criteria
				if(!empty($coursesToNotDisplayList))
					$sql_courses .= " and c.idCourse not in (".implode(',', $coursesToNotDisplayList).")"."\n";



				// *********** NOW IT'S LEARNING PLAN'S TURN ************
				$sql_learningplan =	"select id_path as id, 'coursepath' as catalog_item_type, path_name as name, path_descr as description, price as price, is_selling as is_selling, img as img, NULL as `status`, NULL as allow_overbooking, NULL as max_num_subscribe, create_date as create_date, NULL as status_order, path_code as code"."\n"
									." from ".LearningCoursepath::model()->tableName()."\n"
									." where visible_in_catalog = 1"."\n";
				if($this->fltSearch)
					$sql_learningplan .= " and (path_name LIKE '%".addslashes(str_replace(' ', '%', $this->fltSearch))."%' OR path_descr LIKE '%".addslashes(str_replace(' ', '%', $this->fltSearch))."%' OR path_code LIKE '%".addslashes(str_replace(' ', '%', $this->fltSearch))."%')"."\n";

				if($this->fltPricingType)
				{
					switch($this->fltPricingType)
					{
						case self::PRICING_TYPE_FREE:
							$sql_learningplan .= " and is_selling = 0"."\n";
						break;
						case self::PRICING_TYPE_PAID:
							$sql_learningplan .= " and is_selling = 1"."\n";
						break;
					}
				}

				if(!Yii::app()->user->getIsGodadmin() && !Yii::app()->user->getIsAdmin()){
					$allowedCoursesIds = false;
					if (!(Yii::app()->user->isGuest)) {
						$allowedCoursesIds = 	Docebo::learnerNotAllowedCoursesIds();
					}

					if($allowedCoursesIds && is_array($allowedCoursesIds)){
						$sql_courses .= " and c.idCourse NOT IN (".implode(',', $allowedCoursesIds).")"."\n";
					}

				}
				if(Yii::app()->user->isGuest){
					// show only visible to all users courses and learning paths
					$visibleCoursesIds = LearningCourse::getCoursesIdsVisibleInExternalCatalog();
					$sql_courses.=' AND c.idCourse IN("'.implode('","',$visibleCoursesIds).'") ';
				}
				if($this->fltItemType === 'all' && !($this->fltCategory && $this->fltCategory != 'all'))
					$sql = '('.$sql_courses.') union ('."\n".$sql_learningplan.')';
				elseif($this->fltItemType != self::TYPE_LEARNING_PLAN)
					$sql = $sql_courses;
				elseif($this->fltItemType == self::TYPE_LEARNING_PLAN)
					$sql = $sql_learningplan;

				// If we need to hide learning plans
				if($hideLearningPlans) {
					$sql = $sql_courses;
				}

				$order = $this->getSqlOrderColumns($sortingFields);

				if(!empty($order))
					$sql .= " order by ".implode(',', $order)."\n";


				$command = Yii::app()->db->createCommand($sql);

				$elements = $command->queryAll();

				foreach($elements as $element){
					$items['all'][] = array(
						'id'=>$element['id'],
						'catalog_item_type'=>$element['catalog_item_type'],
						'name'=> Yii::app()->htmlpurifier->purify($element['name']),
						'description'=>$element['description'],
						'price'=>$element['price'],
						'is_selling'=>$element['is_selling'],
						'img'=>$element['img'],
						'status'=>$element['status'],
						'allow_overbooking'=>$element['allow_overbooking'],
						'max_num_subscribe'=>$element['max_num_subscribe'],
					);
				}
			}
		} else { // We have to work with custom catalogs here
			$command = Yii::app()->db->createCommand();
			$command->select('
					t.idEntry AS id,
					t.type_of_entry AS catalog_item_type,
					t.idCatalogue AS idCatalogue,
					COALESCE(course.name, coursepath.path_name) AS name,
					COALESCE(course.description, coursepath.path_descr) AS description,
					COALESCE(course.code, coursepath.path_code) AS code,
					COALESCE(course.create_date, coursepath.create_date) AS create_date,
					COALESCE(course.selling, coursepath.is_selling) AS is_selling,
					COALESCE(course.prize, coursepath.price) AS price,
					COALESCE(course.img_course, coursepath.img) AS img,
					course.status,
					course.allow_overbooking,
					course.max_num_subscribe
				')
				->from(LearningCatalogueEntry::model()->tableName().' t');

			$courseOnCondition = 'course.status IN (:effective, :available)';
			$coursePathOnCondition = 'coursepath.visible_in_catalog=1';
			$command->params[':effective'] = LearningCourse::$COURSE_STATUS_EFFECTIVE;
			$command->params[':available'] = LearningCourse::$COURSE_STATUS_AVAILABLE;

			if($this->fltItemType && $this->fltItemType !== 'all') {

				if($this->fltItemType != self::TYPE_LEARNING_PLAN) {
					// Exclude all Learning Plans from query
					$command->andWhere('t.type_of_entry!=:coursepath', array(':coursepath'=>'coursepath'));
				} elseif($this->fltItemType==self::TYPE_LEARNING_PLAN) {
					// Exclude all Courses from query
					$command->andWhere('t.type_of_entry!=:course', array(':course'=>'course'));
				}

				// If "type" filter is set, filter courses only from this type
				$courseOnCondition .= ' AND course.course_type=:course_type ';
				$command->params[':course_type'] = $this->fltItemType;
			}

			// Exclude learning plans if a category filter is selected
			if($this->fltCategory !== 'all')
				$command->andWhere('t.type_of_entry!=:coursepath', array(':coursepath'=>'coursepath'));

			if ($this->fltPricingType == self::PRICING_TYPE_PAID) {
				$courseOnCondition .= ' AND course.selling=1 ';
				$coursePathOnCondition .= ' AND coursepath.is_selling=1 ';
			} elseif ($this->fltPricingType == self::PRICING_TYPE_FREE) {
				$courseOnCondition .= ' AND course.selling=0 ';
				$coursePathOnCondition .= ' AND coursepath.is_selling=0 ';
			}

			// Search text, search by name, other text fields can be searched
			if (!empty($this->fltSearch)) {
				$courseOnCondition .= ' AND (course.name LIKE :search OR course.description LIKE :search OR course.code LIKE :search) ';
				$coursePathOnCondition .= ' AND (coursepath.path_name LIKE :search OR coursepath.path_descr LIKE :search OR coursepath.path_code LIKE :search) ';
				$command->params[':search'] = '%'.str_replace(' ', '%', $this->fltSearch).'%';
			}

			// Filter by category ID?
			if(!empty($this->fltCategory) && $this->fltCategory!='all' && !empty($categories))
				$courseOnCondition .= ' AND course.idCategory IN ('.implode(',', $categories).') ';

			if($this->fltCatalog && $this->fltCatalog!='all') {
				$command->andWhere(array('IN', 't.idCatalogue', array($this->fltCatalog)));
			} else {
				// Only display courses/LPs from catalogs the user is assigned to
				$command->andWhere(array('IN', 't.idCatalogue', array_keys($userCatalogs)));
			}

			// We remove the courses having show_rules = Only users subscribed to the course
			// and the user is not subscribed.
			if(!empty($coursesToNotDisplayList)) {
				// Add the exclude courses condition to the main criteria
				$courseOnCondition .= ' AND course.idCourse NOT IN ('.implode(',', $coursesToNotDisplayList).') ';
			}

			$orderColumns = array('cat.name');
			$otherOrderColumns = $this->getMixedOrderColumns($sortingFields);
			if (!empty($otherOrderColumns)) {
				$orderColumns = array_merge($orderColumns, $otherOrderColumns);
			}

			// This double join + the WHERE below do the magic of getting
			// the catalog entries depending on their type (course|coursepath)
			// from their corresponding table (learning_course|learning_coursepath)
			$command->join(LearningCatalogue::model()->tableName().' cat', 'cat.idCatalogue = t.idCatalogue')
				->leftJoin(LearningCourse::model()->tableName().' course', 't.type_of_entry="course" AND t.idEntry=course.idCourse AND '.$courseOnCondition)
				->leftJoin(LearningCoursepath::model()->tableName().' coursepath', 't.type_of_entry="coursepath" AND t.idEntry=coursepath.id_path AND '.$coursePathOnCondition)
				->andWhere('course.idCourse IS NOT NULL OR coursepath.id_path IS NOT NULL')
				->order = implode(", ", $orderColumns);

			if(Yii::app()->user->isGuest){
				// show only visible to all users courses and learning paths
				$visibleCoursesIds = LearningCourse::getCoursesIdsVisibleInExternalCatalog();
				$command->andWhere('course.idCourse IN("'.implode('","',$visibleCoursesIds).'") OR coursepath.visible_in_catalog = 1');
			}

			// If there is any filter by course fields to be applied
			if(!empty($this->fltCourseFields['additional']) && is_array($this->fltCourseFields['additional']) && count($this->fltCourseFields['additional']) > 0) {
				$command->leftJoin(LearningCourseFieldValue::model()->tableName() .' lcfv', 'lcfv.id_course = course.idCourse ');

				foreach($this->fltCourseFields['additional'] AS $filterItemId => $filterItem) {
					if (!empty($filterItem['type'])) {
						switch ($filterItem['type']) {
							case 'dropdown':
								if (!empty($filterItem['value'])) {
									$command->andWhere('lcfv.field_' . $filterItemId . ' =' . $filterItem['value']);
								}
								break;

							case 'date':
								if (!empty($filterItem['value'])) {
									$command->andWhere('lcfv.field_' . $filterItemId . $filterItem['condition'] . ' :compareDate' . $filterItemId . ' ', array(':compareDate' . $filterItemId => Yii::app()->localtime->fromLocalDate($filterItem['value'])));
								}

								break;

							case 'textarea':
								if (!empty($filterItem['value'])) {
									switch($filterItem['condition']) {
										case 'contains':
											$command->andWhere('lcfv.field_' . $filterItemId . ' LIKE :filterValue'.$filterItemId . ' ',  array(':filterValue'.$filterItemId => '%' . $filterItem['value'] . '%'));
											break;

										case 'not-contains':
											$command->andWhere('COALESCE(lcfv.field_' . $filterItemId . ', "") NOT LIKE :filterValue'.$filterItemId . ' ',  array(':filterValue'.$filterItemId => '%' . $filterItem['value'] . '%'));
											break;

										default:
											break;
									}
								}

								break;

							case 'textfield':
								if (!empty($filterItem['value'])) {
									switch($filterItem['condition']) {
										case 'contains':
											$command->andWhere('lcfv.field_' . $filterItemId . ' LIKE :filterValue'.$filterItemId . ' ',  array(':filterValue'.$filterItemId => '%' . $filterItem['value'] . '%'));
											break;

										case 'equal':
											$command->andWhere('lcfv.field_' . $filterItemId . ' = :filterValue'.$filterItemId . ' ',  array(':filterValue'.$filterItemId => $filterItem['value']));
											break;

										case 'not-equal':
											$command->andWhere('COALESCE(lcfv.field_' . $filterItemId . ', "") != :filterValue'.$filterItemId . ' ',  array(':filterValue'.$filterItemId => $filterItem['value']));
											break;

										default:
											break;
									}
								}

								break;
							default:
								// Not an internal field type -> instantiate the custom field handler and let it process the case
								$fieldName = 'CourseField'.ucfirst($filterItem['type']);
								if(method_exists($fieldName, "applyCourseCatalogFilter"))
									$fieldName::applyCourseCatalogFilter($filterItemId, $filterItem['value'], $command);
						}
					}
				}
			}

			$rows = $command->queryAll();
			// Arrange items per catalog
			foreach($rows as $row)
				$items[$row['idCatalogue']][] = $row;
		}

		// Pagination (only available if no user catalogs are displayed or one catalog is selected)
		$currentPage 	= false;
		$pageCount 		= false;
		if(empty($userCatalogs) || ($this->fltCatalog && $this->fltCatalog !== 'all')) {
			$catalogIndex = $this->fltCatalog;
			if ($this->pageSize && $items[$catalogIndex]) {
				$paginationData = $this->calcLimitAndOffset($items[$catalogIndex]);
				$items[$catalogIndex] = array_slice($items[$catalogIndex], $paginationData['offset'], $paginationData['limit']);
				$currentPage = $this->pageNumber;
				$pageCount = $paginationData['pageCount'];
			}
		}

		$result = array(
			'items'			=> $items,
			'currentPage' 	=> $currentPage,
			'pageCount'		=> $pageCount,
		);

		return $result;
	}


	/**
	 * Return list of translated catalog item types
	 *
	 * @return array
	 */
	public static function getCatalogItemTypes($addAll=false) {
		$result = array();
		if ($addAll)
			$result['all'] = Yii::t('standard', '_ALL');
		$result[self::TYPE_ELEARNING] 		= Yii::t('course', '_COURSE_TYPE_ELEARNING');

		if(PluginManager::isPluginActive("ClassroomApp"))
			$result[self::TYPE_CLASSROOM] 		= Yii::t('standard', '_CLASSROOM');

		$result[self::TYPE_WEBINAR] 		= Yii::t('webinar', 'Webinar');

		if(PluginManager::isPluginActive("CurriculaApp"))
			$result[self::TYPE_LEARNING_PLAN]	= Yii::t('standard', '_COURSEPATH');

		return $result;
	}


	/**
	 * Return list of translated catalog item types
	 *
	 * @return array
	 */
	public static function getPricingTypes($addAll=false) {
		$result = array();
		if ($addAll)
			$result['all'] = Yii::t('standard', '_ALL');

		if (PluginManager::isPluginActive("EcommerceApp") || PluginManager::isPluginActive('ShopifyApp')) {
			$result[self::PRICING_TYPE_PAID] = Yii::t('billing', '_PAID');
		}

		$result[self::PRICING_TYPE_FREE] = Yii::t('apps', 'FREE');
		return $result;
	}


	/**
	 * Get full list of catalogs
	 */
	public static function getAllCatalogs($addAll=false) {

		$models = LearningCatalogue::model()->findAll();
		$result = array();
		if ($addAll)
			$result['all'] = Yii::t('standard', '_ALL');

		if ($models) {
			foreach ($models as $model) {
				$result[$model->idCatalogue] = $model->name;
			}
		}
		return $result;

	}

	/**
	 * Get catalogs available to the current user
	 *
	 * @return array
	 */
	public static function getUserAvailableCatalogs($addAll=false, $includeEmptyCatalogs = false) {
		$idUser = Yii::app()->user->id;
		$langCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

		$idsts = Yii::app()->authManager->getAuthAssignmentsIds($idUser);

		$catalogs = array();
		if ($addAll)
			$catalogs['all'] = Yii::t('standard', '_ALL');

		// If not logged in - get External Catalogs
		if(Yii::app()->user->isGuest && PluginManager::isPluginActive("CoursecatalogApp")) {
			$catalog_external_selected_catalogs = Settings::get('catalog_external_selected_catalogs', '');
			if ($catalog_external_selected_catalogs != '') {
				$catalog_external_selected_catalogs = explode(',', $catalog_external_selected_catalogs);

				if (is_array($catalog_external_selected_catalogs) && count($catalog_external_selected_catalogs) > 0) {
					$criteria = new CDbCriteria();
					$criteria->addInCondition('idCatalogue', $catalog_external_selected_catalogs);
					$sql = 'SELECT cat.idCatalogue, cat.name FROM ' . LearningCatalogue::model()->tableName().' cat WHERE '
							. $criteria->condition;
					$command = Yii::app()->db->createCommand($sql);

					$userCatalogs = $command->queryAll(true, $criteria->params);
					if (!empty($userCatalogs)) {
						foreach($userCatalogs as $catalog) {
							$catalogs[$catalog['idCatalogue']] = $catalog['name'];
						}
					}
				}
			}

			return $catalogs;
		}


		if (!$idsts || !PluginManager::isPluginActive("CoursecatalogApp"))
			return $catalogs;

		// Also include catalogs directly assigned to the current user
		$idsts[] = $idUser;

		// ---- A quite big workaround. Please don't touch if not needed!!! ---
		// Since a catalog may have a "orgchart+descendats" assigned to it,
		// walk though the current user's orgcharts upwards to their top most
		// parent and collect OCD IDs along the way and compare these IDs to
		// the assigned "orgchart+descendants" to the catalog
		// If a match is found, the catalog will be displayed to the current user
		// ----
		$ocdArr = array();
		$c2 = new CDbCriteria();
		$c2->addCondition('groupMembers.idstMember=:idstMember');
		$c2->params[':idstMember'] = $idUser;
		$c2->with = array('groupMembers');
		$c2->select = 't.lev, t.iLeft, t.iRight';
		$userGroups = CoreOrgChartTree::model()->findAll($c2);
		foreach($userGroups as $group){
			$tempCriteria = new CDbCriteria();
			$tempCriteria->addCondition('t.iLeft <= :currentLeft');
			$tempCriteria->addCondition('t.iRight >= :currentRight');
			$tempCriteria->params[':currentLeft'] = $group->iLeft;
			$tempCriteria->params[':currentRight'] = $group->iRight;
			$groupParent = CoreOrgChartTree::model()->findAll($tempCriteria);
			if($groupParent){
				foreach($groupParent as $parent) {
					$ocdArr[] = $parent->idst_ocd;
					if($group->iLeft === $parent->iLeft)
						$ocdArr[] = $parent->idst_oc;
				}

			}
		}

		$idsts = array_merge($idsts, $ocdArr);
		$idsts = array_unique($idsts);

		$catalogues =	Yii::app()->getDb()->createCommand()
						->select('idCatalogue')
						->from(LearningCatalogueMember::model()->tableName())
						->andWhere(array('IN', 'idst_member', $idsts))
						->queryColumn();


		$command = Yii::app()->getDb()->createCommand()
			->from(LearningCatalogue::model()->tableName().' t')
			->where(array('IN', 't.idCatalogue', $catalogues));

		if(!$includeEmptyCatalogs) {
			$command->select('t.*, COUNT(ce.idEntry) AS course_count')
					->join(LearningCatalogueEntry::model()->tableName().' ce', 't.idCatalogue=ce.idCatalogue');
		} else {
			$command->select('t.*, (0) AS course_count');
		}

		$command->group('t.idCatalogue')
			->order('t.name');

		$userCatalogs = $command->queryAll();
		if (!empty($userCatalogs)) {
			foreach($userCatalogs as $catalog)
				$catalogs[$catalog['idCatalogue']] = $catalog['name'];
		}

		return $catalogs;
	}


	/**
	 * Check if given course is in the shopping cart
	 *
	 * @param integer $idCourse
	 * @return boolean
	 */
	public function isCourseInCart($idCourse) {
		$cartPositions = Yii::app()->shoppingCart->getPositions();
		foreach($cartPositions as $courseCartItem) {
			if(!($courseCartItem instanceof CourseCartPosition))
				continue;
			if ($courseCartItem->idCourse == $idCourse) {
				return true;
			}
		}
		return false;
	}



	/**
	 * Return available sorting fields and their labels for Settings UI.
	 * Sorting fields are "meta names", NOT real table field names, because we have a mix of courses and learning plans to sort.
	 *
	 * @return array
	 */
	public static function getAvailableSortingFields(){
		return array(
			'name'						=> Yii::t('standard', '_NAME').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			'name_desc'					=> Yii::t('standard', '_NAME').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
			'creation_date' 			=> Yii::t('report', '_CREATION_DATE').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			'creation_date_desc'		=> Yii::t('report', '_CREATION_DATE').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
			'code'                      => Yii::t('standard', '_CODE').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			'code_desc'					=> Yii::t('standard', '_CODE').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
		);
	}

	/**
	 * Return an array of order columns suitable for Course+LP retrieving
	 * and mixing DB command in getCatalogItems() method
	 * This mixed DB command is only used when the user has some Catalog
	 * assigned and we retrieve courses and LPs from his assigned Catalogs
	 * via a single query
	 *
	 * @param array $sortingFields  Array of <sorting metanames>, usually a dashlet setting
	 * @return array
	 */
	protected function getMixedOrderColumns($sortingFields) {
		$orderColumns = array();
		if (is_array($sortingFields) && !empty($sortingFields)) {
			foreach ($sortingFields as $metaname) {
				switch ($metaname) {
					case "name":
						$orderColumns[] = 'name ASC';
					break;
					case "name_desc":
						$orderColumns[] = 'name DESC';
					break;
					case "creation_date":
						$orderColumns[] = 'create_date ASC';
					break;
					case "creation_date_desc":
						$orderColumns[] = 'create_date DESC';
					break;
					case "code":
						$orderColumns[] = 'code ASC';
					break;
					case "code_desc":
						$orderColumns[] = 'code DESC';
					break;
				}
			}
		}

		return $orderColumns;
	}

	protected function getSqlOrderColumns($sortingFields) {
		$orderColumns = array();
		if (is_array($sortingFields) && !empty($sortingFields)) {
			foreach ($sortingFields as $metaname) {
				switch ($metaname) {
					case "name":
						$orderColumns[] = 'name ASC';
					break;
					case "name_desc":
						$orderColumns[] = 'name DESC';
					break;
					case "creation_date":
						$orderColumns[] = 'create_date ASC';
					break;
					case "creation_date_desc":
						$orderColumns[] = 'create_date DESC';
					break;
					case "code":
						$orderColumns[] = 'code ASC';
					break;
					case "code_desc":
						$orderColumns[] = 'code DESC';
					break;
				}
			}
		}

		return $orderColumns;
	}

	/**
	 * Dashlet descriptor
	 *
	 * @return DashletDescriptor Object, describing the dashlet to the outside world
	 */
	public static function descriptor( $params = FALSE ) {
		$sampleImageUrl = Yii::app()->mydashboard->getModule()->getAssetsUrl() . "/images/catalog_sample.jpg";

		$descriptor = new DashletDescriptor();
		$descriptor->name 					= 'core_dashlet_courses_catalog';
		$descriptor->handler				= self::$handler;
		$descriptor->title					= Yii::t('standard', 'Catalog');
		$descriptor->description			= Yii::t('dashlets', 'Displays a list of avalaible courses for the user to buy/subscribe');
		$descriptor->sampleImageUrl 		= $sampleImageUrl;
		$descriptor->settingsFieldNames		= array(
			'initial_display_style',
			'initial_item_type',
			'initial_pricing_type',
			'initial_catalog',
			'show_filters_button',
			'show_display_style_switch',
			'show_search_bar',
			'show_categories_selector',
			'display_number_items',
			'display_number_items_fullist',
			'show_view_all_link',
			'selected_sorting_fields',
			'paginate_dashlet',
		);

		return $descriptor;
	}

	/**
	 * Render dashlet settings, used in Admin Dashlets UI
	 */
	public function renderSettings() {
		$cs = Yii::app()->getClientScript();
		$themeUrl = Yii::app()->theme->baseUrl;

		// FCBK
		$cs->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
		$cs->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');

		$this->render('catalog/settings');
	}

	/**
	 * Render fronend content.
	 *
	 * NOTE: Don't use Yii's CClientScript to register js/css resources
	 * here since it doesn't work. Use echo '<script src=...</script>' instead
	 */
	public function renderFrontEnd() {
		if ($this->dashletModel) {

			// Initial display style ... However, cookies are overriding
			$displayStyle = $this->getParam('initial_display_style', DashletMyCourses::DISPLAY_STYLE_THUMB);
			$currentAction = Yii::app()->request->getParam('r', 'site/index');
			$currentOpt = Yii::app()->request->getParam('opt', '');
			if($currentOpt !== '') {
				$currentAction .= '&opt=' . $currentOpt;
			}

			$currentAction = str_replace('/', '_', $currentAction);
			$cookieName = 'dashlet_display_style_' . $this->dashletModel->id;
			if(Yii::app()->user->isGuest) {
				$cookieName .= '_public';
			}

			if(isset(Yii::app()->request->cookies[$cookieName])) {
				$displayStyle = Yii::app()->request->cookies[$cookieName];
			} else {
				Yii::app()->request->cookies[$cookieName] = new CHttpCookie($cookieName, $displayStyle);
			}

			// If the dashlet is less then 1/2
			if($this->dashletModel->dashboardCell->width < 6) {
				$displayStyle = DashletMyCourses::DISPLAY_STYLE_LIST;
				Yii::app()->request->cookies[$cookieName] = new CHttpCookie($cookieName, $displayStyle);
			}

			$displayStyleSwitchIcon = ($displayStyle == "list") ? "fa-th" : "fa-list";
			$displayStyleSwitchIconCalendar = ($displayStyle == "calendar") ? "fa-th" : "fa-calendar";
			$thumbClass = ($displayStyle == "list") ? "" : "tb";

			// Check if we need to render the top rated courses box
			$topRatedCourses = LearningCourse::getTopRated();
			$showTopRatedCoursesBox = !empty($topRatedCourses);

			// Get some On/Off settings from dashlet instance parameters
			$showFiltersButton 		= $this->getParam('show_filters_button', true);
			$showSearchBar 			= $this->getParam('show_search_bar', true);
			$showDisplayStyleSwitch = $this->getParam('show_display_style_switch', true);
			$showCategoriesSelector = $this->getParam('show_categories_selector', true);

			$multidomainClient = CoreMultidomain::resolveClient();
			if ($multidomainClient) {
				$multidomainClientmodel = CoreMultidomain::model()->findByPk($multidomainClient->id);
				// If it is a MultiDomain client and it has its own catalog configuration(enabled!!!)
				if ($multidomainClientmodel->catalog_custom_settings == 1) {
					$showCategoriesSelector = ($multidomainClientmodel->catalog_use_categories_tree == 1) ? true : false;
				}
			}

			// Load real catalog items (course + learning plans)
			$catalogData 	= $this->getCatalogItems();


			// Load categories data for fancy tree
			if($showCategoriesSelector)
				$categoriesTree = $this->getCategoriesTree();
			else
				$categoriesTree = array();

			// Get additional course fields data here
			$additionalCourseFieldsModels = LearningCourse::model()->getCourseAdditionalFields(false, true);

			// Prepare view params
			$params = array(
				'hasFilterHeader' => $showFiltersButton || $showDisplayStyleSwitch || $showSearchBar || $showCategoriesSelector,
				'showCategoriesSelector' => $showCategoriesSelector,
				'showFiltersButton' => $showFiltersButton,
				'showSearchBar' => $showSearchBar,
				'showDisplayStyleSwitch' => $showDisplayStyleSwitch,
				'thumbClass' => $thumbClass,
				'displayStyleSwitchIcon' => $displayStyleSwitchIcon,
				'displayStyleSwitchIconCalendar' => $displayStyleSwitchIconCalendar,
				'catalogItems' => $catalogData['items'],
				'viewAllCoursesUrl' => Docebo::createLmsUrl('site/index', array('sclo' => 1, 'id' => $this->dashletModel->id)),
				'showViewAllCourses' => $this->getParam('show_view_all_link', false),
				'currentPage' => $catalogData['currentPage'],
				'pageCount' => $catalogData['pageCount'],
				'showTopRatedCoursesBox' => $showTopRatedCoursesBox,
				'topRatedCourses' => $topRatedCourses,
				'showSidebar' => ($this->dashletModel->dashboardCell->width > 4) && ($showCategoriesSelector || $showTopRatedCoursesBox),
				'categoriesTree' => $categoriesTree,
				'paginate' => (($this->idLayout > 0) && $this->getParam('paginate_dashlet', false)) || $this->idLayout < 0, // Follow "paginate dashlet" setting for "real layouts" and force paginate in "virtual layouts" (negative IDs)
				'additionalCourseFieldsModels' => $additionalCourseFieldsModels
			);

			$this->render('catalog/index', $params);
		}
		else
			echo "";
	}

	/**
	 * Returns the tree of category node for the fancy tree
	 */
	protected function getCategoriesTree() {

	    $language = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
	    $defaultLangCode = Settings::get('default_language', '');
	    $includeRoot = true;

	    // Cache Category tree in a file
	    // NOTE: We must include in the cache key ALL possible factors (actors) that may change the tree!
	    if (isset(Yii::app()->cache_categorytree)) {

	       $cacheKeyActors = array(
	           $language,
	           $defaultLangCode,
	           $includeRoot,
	           $this->fltCatalog,
	           Yii::app()->user->id
	       );

	       $cacheKey = md5(json_encode($cacheKeyActors));

	       $tree = Yii::app()->cache_categorytree->get($cacheKey);
	       if ($tree) {
	           return $tree;
	       }
	    }

		$_treeData = LearningCourseCategoryTree::getCatalogTreeData($language, $includeRoot);

		if (!empty($_treeData)) {
			// Calculate information about categories: number of catalog courses for each category
			$_catalogCourses = LearningCatalogue::getCatalogCoursesPage(Yii::app()->user->id, 0, 0, array('catalog' => ($this->fltCatalog == 'all' ? '' : $this->fltCatalog)), false);

			if (!empty($_catalogCourses)) {
				// Find categories root node (NOTE: it MUST be present)
				$_roots = LearningCourseCategoryTree::model()->roots()->findAll();
				$_root = $_roots[0];

				//count courses for every category
				$_countList = array();
				if (!empty($_catalogCourses['courses'])) {
					foreach ($_catalogCourses['courses'] as $_course) {
						$_index = ($_course->idCategory <= 0 ? $_root->idCategory : $_course->idCategory); //uncategorized courses are put into root
						if (!isset($_countList[(int)$_index])) { $_countList[(int)$_index] = 0; }
						$_countList[(int)$_index]++;
					}
				}

				// Calculate courses in categories tree
				$_countSubNodes = function(&$tree) use (&$_countSubNodes, &$_countList) {
					$output = 0;
					if (is_array($tree) && !empty($tree)) {
						for ($i=0; $i<count($tree); $i++) {
							$key = (int)$tree[$i]['key'];
							$output += (isset($_countList[$key]) ? $_countList[$key] : 0);
							if (!empty($tree[$i]['children'])) { $output += $_countSubNodes($tree[$i]['children']); }
						}
					}
					return $output;
				};
				$_recursiveCalculate = function(&$tree) use (&$_recursiveCalculate, &$_countSubNodes, &$_countList) {
					if (is_array($tree) && !empty($tree)) {
						for ($i=0; $i<count($tree); $i++) {
							$key = (int)$tree[$i]['key'];
							$count = (isset($_countList[$key]) ? $_countList[$key] : 0);
							$tree[$i]['data']['courses'] = $count;
							$tree[$i]['data']['courses_d'] = $count + $_countSubNodes($tree[$i]['children']);
							if (!empty($tree[$i]['children'])) { $_recursiveCalculate($tree[$i]['children']); }
						}
					}
				};
				$_recursiveCalculate($_treeData);

				// Clean tree from 0-courses nodes and subnodes
				$_clean = function(&$tree) use (&$_clean) {
					if (is_array($tree) && !empty($tree)) {
						for ($i=count($tree)-1; $i>=0; $i--) {
							if (!$tree[$i]['data']['is_root'] && $tree[$i]['data']['courses_d'] <= 0) {
								$tree[$i] = NULL;
								unset($tree[$i]);
							} else {
								if (!empty($tree[$i]['children'])) {
									$_clean($tree[$i]['children']);
								}
							}
						}
					}
				};
				$_clean($_treeData);

				// Reorganize missing indexes in children array && mark currently selected node
				$currentSelectedNode = $this->fltCategory == 'all' ? $_root->idCategory : $this->fltCategory;
				$_rearrangeIndexes = function(&$tree) use (&$_rearrangeIndexes, $currentSelectedNode) {
					if (is_array($tree) && !empty($tree)) {
						for ($i=count($tree)-1; $i>=0; $i--) {
							if($tree[$i]['key'] == $currentSelectedNode)
								$tree[$i]['selected'] = 1;
							if (!empty($tree[$i]['children'])) {
								$tree[$i]['children'] = array_values($tree[$i]['children']);
								$_rearrangeIndexes($tree[$i]['children']);
							}
						}
					}
				};
				$_rearrangeIndexes($_treeData);
			}
		}

		// Cache the result
		if (isset(Yii::app()->cache_categorytree)) {
		    Yii::app()->cache_categorytree->set($cacheKey, $_treeData);
		}

		return $_treeData;
	}

	/**
	 * Run arbitrary code just before the main page (dashboard) is going to be loaded (rendered) IF the dashlet takes part of it
	 *
	 * <strong>Note 1</strong>: this method is called BEFORE rendering of the main page, as an attribute of an instance created BEFORE rendering!
	 * Which means, DURING the rendering of the dashlet, another object instance is created/used, having no relation to the first instance
	 * The first intance is purely created (and dismissed) for the porpose of bootstraping to allow, mainly, preloading client (browser) resources
	 *
	 * <strong>Note 2</strong>: Be aware, if you have 2 dashlets of the same type in the dashboard, bootstrap() will be called 2 times!!!
	 *
	 * <strong>Hint</strong>: Use this to execute Yii::app()->getClientScript()->registerXXXXX(<url>) to load dashlet specific own javascript/css
	 *
	 * @param boolean|array $params
	 */
	public function bootstrap( $params = FALSE ) {
		$showCategoriesSelector = $this->getParam('show_categories_selector', true);
		if($showCategoriesSelector) {
			Yii::app()->fancytree->register();
			$cs = Yii::app()->getClientScript();
			$cs->registerCssFile(Yii::app()->baseUrl . '/../plugins/CoursecatalogApp/css/catalog.css');
		}

		// Check if we need to render the top rated courses box
		$topRatedCourses = LearningCourse::getTopRated();
		if (!empty($topRatedCourses)) {
			/* @var $cs CClientScript */
			$cs=Yii::app()->getClientScript();
			$cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/jquery.rating.js');
			$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/docebo-star-rating.css');
		}

		//Load some specific assets needed for the calendar
		$assetsUrl = Yii::app()->getModule('mycalendar')->getAssetsUrl();
		$cs->registerCssFile($assetsUrl.'/css/fullcalendar.css');
		$cs->registerScriptFile($assetsUrl.'/js/fullcalendar.js');
	}


	/**
	 * Try to find a dashlet having DashletCatalog handler and NOT being part of any layout, that is our dashlet by definition for FULL catalog
	 *
	 * @return DashboardDashlet
	 */
	public static function getFullCatalogDashlet() {
		$model = DashboardDashlet::model()->findByAttributes(array('handler' => self::$handler), 'cell IS NULL');
		// If there is no such, create one on the fly and used
		if (!$model) {
			$model = DashboardDashlet::createFullCatalogDashlet();
		}
		return $model;
	}


	/**
	 * Calculate limit/offset based on current page size
	 *
	 * @param array $items Data set, array of data items
	 *
	 * @return array
	 */
	protected function calcLimitAndOffset($items) {
		if ((!$this->pageSize) || ($this->pageSize<1)) {
			return array(
				'limit'		=> 999999999,
				'offset'	=> 0,
				'pageCount'	=> count($items) > 0 ? 1 : 0,
			);
		}

		$pageCount = (int)((count($items) + $this->pageSize-1)/$this->pageSize);
		$offset = $this->pageNumber * $this->pageSize;
		$limit  = $this->pageSize;
		$result = array(
			'limit'		=> $limit,
			'offset'	=> $offset,
			'pageCount'	=> $pageCount,
		);

		return $result;
	}

	/**
	 * Return default dashlet parameters, if someone needs them
	 *
	 * @return array
	 */
	public static function getDefaultParameters() {
		return array(
			"initial_display_style" 		=> "thumb",
			"initial_item_type" 			=> "all",
			"initial_pricing_type"			=> "all",
			"initial_catalog"				=> "all",
			"show_filters_button"			=> "1",
			"show_display_style_switch"		=> "0",
			"show_search_bar"				=> "1",
			"show_categories_selector"		=> (Settings::get('catalog_use_categories_tree') == 'on') ? "1" : "0",
			"display_number_items" 			=> false,
			"display_number_items_fullist" 	=> false,
			"show_view_all_link"			=> "0",
			"selected_sorting_fields"		=> false,
			"paginate_dashlet"				=> true,
		);
	}
}