<?php

class DashletAdminKPIs extends DashletWidget implements IDashletWidget {
	
	protected static $kpiList;

	/**
	 * Dashlet descriptor
	 *
	 * @return DashletDescriptor Object, describing the dashlet to the outside world
	 */
	public static function descriptor( $params = FALSE ) {
		$sampleImageUrl = Yii::app()->mydashboard->getModule()->getAssetsUrl() . "/images/adminkpi_sample.jpg";

		$descriptor = new DashletDescriptor();

		$descriptor->name               = 'core_dashlet_adminkpis';
		$descriptor->handler            = 'mydashboard.widgets.DashletAdminKPIs';
		$descriptor->title              = 'Admin KPIs';
		$descriptor->description        = Yii::t('dashlets', 'Shows a list of admin statistics related to users and the LMS');
		$descriptor->sampleImageUrl     = $sampleImageUrl;
		$descriptor->settingsFieldNames = array_merge( array( 'field_order' ), array_keys( self::getKPIList() ) );

		return $descriptor;
	}

	/**
	 * Render dashlet settings, used in Admin Dashlets UI
	 */
	public function renderSettings() {
		$tmpAvailSettings = self::getKPIList();

		$orderedSettings = array();

		// If editing a dashlet, restore custom ordering of fields, if set
		$fieldsOrdered = explode(',', $this->getParam('field_order'));
		if(!empty($fieldsOrdered)){
			foreach($fieldsOrdered as $field){
				if(in_array($field, array_keys($tmpAvailSettings))){
					$orderedSettings[$field] = $tmpAvailSettings[$field];
				}
			}
		}
		// Finally, add all settings not yet added by the ordered adding above.
		// On initial dashlet creation this array will just be populated with
		// the original settings in their original order
		foreach($tmpAvailSettings as $setting=>$label){
			if(!in_array($setting, array_keys($orderedSettings)))
				$orderedSettings[$setting] = $label;
		}

		$this->render('dashlet_adminkpis_settings', array(
			'settings'=>$orderedSettings,
		));
	}

	/**
	 * Render fronend content.
	 *
	 * NOTE: Don't use Yii's CClientScript to register js/css resources
	 * here since it doesn't work. Use echo '<script src=...</script>' instead
	 */
	public function renderFrontEnd() {
        // show widget on admin users only
        if(!Yii::app()->user->getIsGodadmin()){
            echo Yii::t('dashlets', 'You are not a super admin');
            return;
        }

		$montlySessions = array();

		$fieldsOrdered = explode(',', $this->getParam('field_order'));

		if(empty($fieldsOrdered)){
			// Get default order
			$fieldsOrdered = array_keys(self::getKPIList());
		}

		$orderedBlocks = array();

		foreach($fieldsOrdered as $settingName){

			if(!in_array($settingName, array_keys(self::getKPIList())))
				continue; // This setting is not available in the platform

			if(!$this->getParam($settingName))
				continue; // Setting not enabled in dashlet configuration screen

			if($settingName=='total_sessions_last_year'){
				// This is an exception. It will render a full width
				// bar chart and it is excluded from the $orderedBlocks
				// which will each render a half width .auto-knob + label
				continue;
			}

			$orderedBlocks[] = $settingName;
		}

		if($this->getParam('total_sessions_last_year')){

			// Create the initial data array with the bar chart
			// labels (last 12 months names ascending) and default
			// (zero) values that will be populated with the DB data later
			for($i=11; $i>=0; $i--){

				// Without 'first day of this month', the last February will
				// be skipped if today's date is e.g. 29th but that last
				// February has 28 days
				$date = new DateTime('first day of this month');

				$interval = new DateInterval("P{$i}M");
				$offset = $date->sub($interval);
				$montlySessions[$offset->format('Y').$offset->format('n')] = array(
					'label'=>$offset->format('M'), // e.g. "Jan"
					'value'=>0,
				);
			}

			// Retrieve count of sessions for last 12 months, groupped montly
			$query = Yii::app()->getDb()->createCommand()
				->select('MONTH(sess.enterTime) as month, YEAR(sess.enterTime) as year, COUNT(idUser) as count')
				->from(LearningTracksession::model()->tableName().' sess')
				// Filter out, e.g. deleted users:
				->join(CoreUser::model()->tableName().' u', 'sess.idUser=u.idst')
				// only last 12 months:
				->where('sess.enterTime >= DATE_SUB(NOW(), INTERVAL 12 MONTH)')
				->group('YEAR(sess.enterTime) ASC, MONTH(sess.enterTime) ASC')
				->queryAll();

			// Populate data array
			foreach($query as $dbRow){
				$key = $dbRow['year'].$dbRow['month'];
				if(isset($montlySessions[$key])){
					$montlySessions[$key]['value'] = $dbRow['count'];
				}
			}

		}

		$statusesCountGrouppedByStatus = Yii::app()->getDb()->createCommand()
				->select('COUNT(cu.idCourse) as count, cu.status')
				->from(LearningCourseuser::model()->tableName().' cu')
				// Filter only to existing courses:
				->join(LearningCourse::model()->tableName().' c', 'c.idCourse=cu.idCourse')
				// Filter out deleted users:
				->join(CoreUser::model()->tableName().' u', 'u.idst=cu.idUser')
				->group('cu.status')
				->queryAll();

		$totalEnrollmentsCount = 0;

		$coursesNotStarted = 0;
		$coursesInProgress = 0;
		$coursesCompleted = 0;

		foreach($statusesCountGrouppedByStatus as $tmp){
			$totalEnrollmentsCount = $totalEnrollmentsCount + intval($tmp['count']);

			switch($tmp['status']){
				case LearningCourseuser::$COURSE_USER_END:
					$coursesCompleted = $coursesCompleted + intval($tmp['count']);
					break;
				case LearningCourseuser::$COURSE_USER_BEGIN:
					$coursesInProgress = $coursesInProgress + intval($tmp['count']);
					break;
				case LearningCourseuser::$COURSE_USER_SUBSCRIBED:
					$coursesNotStarted = $coursesNotStarted + intval($tmp['count']);
			}
		}

		$this->render( 'dashlet_adminkpis', array(
			'orderedBlocks'=> $orderedBlocks,

			'montlySessions'      => $montlySessions,
			'totalEnrollmentsCount' => $totalEnrollmentsCount,
			'courses_not_started' => $coursesNotStarted,
			'courses_in_progress' => $coursesInProgress,
			'courses_completed'   => $coursesCompleted,
		) );
	}
	
	
	/**
	 * Returns list of KPIs
	 *
	 * @return array
	 */
	public static function getKPIList() {
	
		if (!empty(self::$kpiList)) {
			return self::$kpiList;
		}
	
		$list =  array(
			'total_sessions_last_year' => Yii::t( 'dashlets', 'Total number of users sessions (1 year)' ),
			'courses_not_started'      => Yii::t( 'dashlets', 'Not yet started courses' ),
			'courses_in_progress'      => Yii::t( 'dashlets', 'In progress courses' ),
			'courses_completed'        => Yii::t( 'standard', 'Completed courses' ),
		);
	
		$kips = self::collectCustomKPIs();
		if (is_array($kips)) {
			foreach ($kips as $kpi => $label) {
				$list[$kpi] = $label;
			}
		}
	
		self::$kpiList = $list;
	
		return $list;
	}
	

	/**
	 * Run arbitrary code just before the main page (dashboard) is going to be loaded (rendered) IF the dashlet takes part of it
	 *
	 * <strong>Note 1</strong>: this method is called BEFORE rendering of the main page, as an attribute of an instance created BEFORE rendering!
	 * Which means, DURING the rendering of the dashlet, another object instance is created/used, having no relation to the first instance
	 * The first intance is purely created (and dismissed) for the porpose of bootstraping to allow, mainly, preloading client (browser) resources
	 *
	 * <strong>Note 2</strong>: Be aware, if you have 2 dashlets of the same type in the dashboard, bootstrap() will be called 2 times!!!
	 *
	 * <strong>Hint</strong>: Use this to execute Yii::app()->getClientScript()->registerXXXXX(<url>) to load dashlet specific own javascript/css
	 *
	 * @param string $params
	 */
    public function bootstrap( $params = FALSE ) {
        $cs = Yii::app()->clientScript;
        $am = Yii::app()->assetManager;

        $cs->registerCssFile   ($am->publish(Yii::getPathOfAlias('mydashboard.assets')).'/css/adminkpi.css');
        $cs->registerScriptFile(Yii::app()->getModule('player')->getAssetsUrl() . "/js/jquery.knob.js");
    }

	
	/**
	 * Collect Custom AdminKPIs from whoever listens for the given event (e.g. a plugin)
	 * The event listener must return and array of id => label
	 *
	 * @return array
	 */
	public static function collectCustomKPIs() {
		// Raise an event to collect Custom MyKPI's
		$kpis = array();
		Yii::app()->event->raise('CollectCustomAdminKPIs', new DEvent(self, array(
			'kpis' => &$kpis
		)));
		return $kpis;
	}


}