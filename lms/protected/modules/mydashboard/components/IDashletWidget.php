<?php

interface IDashletWidget  {

	/**
	 * Dashlet descriptor
	 *
	 * @return DashletDescriptor Object, describing the dashlet to the outside world
	 */
	public static function descriptor($params=false);
	
	/**
	 * Render dashlet settings, used in Admin Dashlets UI
	 */
	public function renderSettings();

	
	/**
	 * Render fronend content.
	 *
	 * NOTE: Don't use Yii's CClientScript to register js/css resources
	 * here since it doesn't work. Use echo '<script src=...</script>' instead
	 */
	public function renderFrontEnd();
	
	
	/**
	 * Run arbitrary code just before the main page (dashboard) is going to be loaded (rendered) IF the dashlet takes part of it
	 * 
	 * <strong>Note 1</strong>: this method is called BEFORE rendering of the main page, as an attribute of an instance created BEFORE rendering!
	 * Which means, DURING the rendering of the dashlet, another object instance is created/used, having no relation to the first instance
	 * The first intance is purely created (and dismissed) for the porpose of bootstraping to allow, mainly, preloading client (browser) resources
	 * 
	 * <strong>Note 2</strong>: Be aware, if you have 2 dashlets of the same type in the dashboard, bootstrap() will be called 2 times!!!
	 * 
	 * <strong>Hint</strong>: Use this to execute Yii::app()->getClientScript()->registerXXXXX(<url>) to load dashlet specific own javascript/css
	 *  
	 * @param string $params 
	 */
	public function bootstrap($params=false); 
	
	
}