<?php
/**
 * A standard class serving dashlet "descriptor" mechanic.
 *  
 * All Dashlet descriptors MUST return object of this class, otherwise the descriptor is invalidated (ignored)
 * 
 *
 */
class DashletDescriptor extends stdClass {
	
	/**
	 * 
	 * @var string
	 */
	public $name;
	
	/**
	 * 
	 * @var string
	 */
	public $handler;
	
	/**
	 * 
	 * @var string
	 */
	public $title;
	
	/**
	 * 
	 * @var string
	 */
	public $description;
	
	/**
	 * URL to a sample image (preview)
	 * @var string
	 */
	public $sampleImageUrl;
	
	/**
	 * List of dashlet specific setting attributes
	 * @var array of strings
	 */
	public $settingsFieldNames;
	
	/**
	 * If this dashlet has a Height
	 * @var boolean
	 */
	public $hasHeight;
	
	/**
	 * Prevent this dashlet to be created more than once per layout 
	 * @var boolean
	 */
	public $disallowMultiInstance = false;
	
}