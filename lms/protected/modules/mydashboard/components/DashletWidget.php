<?php
/**
 * Dashlet Widget: an ancestor for all My Dashboard -> Dashlet widgets. Use it by extending!
 * 
 * @author <Plamen Petkov> plamendp@gmail.com  
 *
 */
class DashletWidget extends CWidget {

	/**
	 * If set to true, instruct widget to render its settings (fields only! No <form></form>!!!)
	 * @var boolean
	 */
	public $settings  = false;
	
	/**
	 * Dashlet model assigned by the widget caller
	 * 
	 * @var DashboardDashlet
	 */
	public $dashletModel;
	
	/**
	 * The ID of the layout this dashlet is loding into
	 * @var integer
	 */
	public $idLayout;
	

	/**
	 * Dashlet specific parameters, extracted from dashletmodle->params JSON string
	 * Just for easy access.
	 *
	 * @var array
	 */
	protected $params = array();
	
	
	/**
	 * @see CWidget::init()
	 */
	public function init() {
		parent::init();
		if ($this->dashletModel) {
			$this->params = json_decode($this->dashletModel['params'], true);
		}
	}	
	
	
	/**
	 * @see CWidget::run()
	 */
	public function run() {
		if ($this->settings) {
			$this->renderSettings();
		}
		else {
			$this->renderFrontEnd();
		}
	}

	/**
	 * Return THIS dashlet parameter/setting by its name.
	 *  
	 * @param string $paramName Dashlet setting/parameter name
	 * @param string $defaultValue Default value to return if parameter is not set or is empty 
	 * @return multitype:|string
	 */
	protected function getParam($paramName, $defaultValue = null){
		//if(isset($this->params[$paramName]))
		//if(isset($this->params[$paramName]) && !empty($this->params[$paramName]))
		//if(isset($this->params[$paramName]) && ($this->params[$paramName] != null))
		if(isset($this->params[$paramName]) && ($this->params[$paramName] !== null))
			return $this->params[$paramName];
		return $defaultValue;
	}


	/**
	 * Get array of title in different languages
	 *
	 * @param bool|false $id
	 *
	 * @return array
	 */
	public static function getTitles($id = false) {
		if (!$id) {
			$id = Yii::app()->request->getParam('id', false);
		}

		$titles = array();

		// Get languages
		$langs = Lang::getLanguages(true);

		if ($id) {
			DashboardDashletTranslation::model()->findAllByAttributes(array('id_dashlet' => $id));
			$command = Yii::app()->db->createCommand();
			$command->select('t.lang_code, t.header')
					->from(DashboardDashletTranslation::model()->tableName() . ' t')
					->where('t.id_dashlet = :id', array(':id' => $id));
			$titlesArr = $command->queryAll();

			if(is_array($titlesArr) && count($titlesArr) > 0) {
				foreach ($titlesArr as $row) {
					$titles[$row['lang_code']] = $row['header'];
				}
			}

			// If there are missing translations for some languages
			// Disabled since in certain cases the translations are >= the active languages
//			if (count($langs) > count($titles)) {
				foreach($langs as $lang) {
					if (!array_key_exists($lang['code'], $titles)) {
						$titles[$lang['code']] = '';
					}
				}
//			}
		} else {
			// Prepare the titles array with all available languages
			foreach($langs as $lang){
				$titles[$lang['code']] = '';
			}
		}

		return $titles;
	}

	/**
	 * Get the Dashlet title for the current lang or return the default language title
	 *
	 * @param bool|false $id
	 * @return mixed
	 */
	public static function getCurrentLangTitle($id = false) {
		if (!$id) {
			$id = Yii::app()->request->getParam('id', false);
		}

		$currentLang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
		$defaulLang = CoreSetting::get('default_language');

		$titleArr = self::getTitles($id);

		return !empty($titleArr[$currentLang]) ? $titleArr[$currentLang] : $titleArr[$defaulLang];
	}
}
