/**
 * Created by Georgi on 11.5.2016 г..
 */
var YoutubeCustomPlayer = false;
var VimeoCustomPlayer = false;
var WistiaCustomPlayer = false;

var trackUrlGlobal = false;
var courseIdGlobal = false;
var idReferenceGlobal = false;
var statusAttempted = false;
var statusIntro = false;
var statusCompleted = false;
var bookmark = false;
var currentTimeGlobal = 0;
var durationGlobal = false;

var track = function(roundDuration, ceilTime){
    if(bookmark !== false && trackUrlGlobal !== false && courseIdGlobal !== false && idReferenceGlobal !== false){
        var status = statusAttempted;
        var roundedTime = 0;
        if(typeof ceilTime !== "undefined" && ceilTime) {
             roundedTime = Math.ceil(currentTimeGlobal);
        } else{
            roundedTime = Math.round(currentTimeGlobal);
        }
        var duration = durationGlobal;
        if(typeof roundDuration !== "undefined" && roundDuration){
            duration = parseInt(durationGlobal);
        }

        if(roundedTime == 0){
            status = statusIntro
        } else if(roundedTime >= duration){
            status = statusCompleted;
        }

        $.ajax({
            url: trackUrlGlobal,
            type: 'post',
            dataType: 'JSON',
            data: {
                course_id: courseIdGlobal,
                id_reference: idReferenceGlobal,
                status: status,
                bookmark: currentTimeGlobal
            }
        });
    }
};

WistiaCustomPlayer = function(options){
    this.options = [];
    this.player = false;
    this.state = 0;

    if(typeof options !== "undefined"){
        this.init(options);
    }
};

WistiaCustomPlayer.prototype = {
    init: function(options){
        var $this = this;
        this.options = $.extend([], this.options, options);
        if(typeof this.options.trackUrl !== "undefined" && this.options.trackUrl !== null){
            this.trackUrl = options.trackUrl;
            trackUrlGlobal = options.trackUrl;
        }
        if(typeof this.options.course_id !== "undefined" && this.options.course_id !== null){
            courseIdGlobal = this.options.course_id;
        }

        if(typeof this.options.idReference !== "undefined" && this.options.idReference !== null){
            idReferenceGlobal = this.options.idReference;
        }

        if(typeof this.options.statusAttempted !== "undefined" && this.options.statusAttempted !== null){
            statusAttempted = this.options.statusAttempted;
        }

        if(typeof this.options.statusIntro !== "undefined" && this.options.statusIntro !== null){
            statusIntro = this.options.statusIntro;
        }

        if(typeof this.options.statusCompleted !== "undefined" && this.options.statusCompleted !== null){
            statusCompleted = this.options.statusCompleted;
        }

        if(typeof this.options.bookmark !== "undefined" && this.options.bookmark !== null){
            bookmark = this.options.bookmark;
        }

        var tag = document.createElement('script');
        tag.src = "https://fast.wistia.net/assets/external/E-v1.js";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        var $this = this;

        setTimeout(function(){
            for(;;){
                if(typeof Wistia !== "undefined"){
                    var playerElement = $('#wistia_player-element');
                    var parentElementHeight = playerElement.parent().height();
                    //Minus LO controls
                    var playerHeight = parentElementHeight - 44;
                    playerElement.append('<iframe src="//fast.wistia.net/embed/iframe/' + $this.options.videoId.hashedId + '?playerPreference=html5" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="100%" height="' + playerHeight + '"></iframe>');
                    window._wq = window._wq || [];
                    _wq.push({'_all': function(video){
                        $this.run();
                    }});
                    break;
                }
            }
        }, 500);
    },

    run: function(){
        this.player = Wistia.api(this.options.videoId.hashedId);
        var $this = this;
        durationGlobal = this.player.duration();

        $(document).off('adminViewActivated').on('adminViewActivated', function(){
            if($this.state !== 0){
                $this.player.pause();
            }
        });

        if(bookmark !== false){
            if(bookmark >= durationGlobal){
                bookmark = 0;
            }
            this.player.time(parseFloat(bookmark));
            this.player.play();
        }

        this.player.bind('secondchange', function(s) {
            currentTimeGlobal = s;
        });

        this.player.bind('play', function(){
            $this.state = 1;
            track(true);
        });

        this.player.bind('pause', function(){
            $this.state = 0;
            track(true);
        });

        this.player.bind('end', function(){
            $this.state = 0;
            track(true);
        });

        if(courseIdGlobal !== false){
            setInterval(function(){
                if($this.state !== 0){
                    track(true);
                }
            }, 3000);
        }
    }
};

VimeoCustomPlayer = function(options){
    this.options = [];
    this.player = false;
    this.state = 0;

    if(typeof options !== "undefined"){
        this.init(options);
    }
};

VimeoCustomPlayer.prototype = {
    init: function(options){
        var $this = this;
        this.options = $.extend([], this.options, options);
        if(typeof this.options.trackUrl !== "undefined" && this.options.trackUrl !== null){
            this.trackUrl = options.trackUrl;
            trackUrlGlobal = options.trackUrl;
        }
        if(typeof this.options.course_id !== "undefined" && this.options.course_id !== null){
            courseIdGlobal = this.options.course_id;
        }

        if(typeof this.options.idReference !== "undefined" && this.options.idReference !== null){
            idReferenceGlobal = this.options.idReference;
        }

        if(typeof this.options.statusAttempted !== "undefined" && this.options.statusAttempted !== null){
            statusAttempted = this.options.statusAttempted;
        }

        if(typeof this.options.statusIntro !== "undefined" && this.options.statusIntro !== null){
            statusIntro = this.options.statusIntro;
        }

        if(typeof this.options.statusCompleted !== "undefined" && this.options.statusCompleted !== null){
            statusCompleted = this.options.statusCompleted;
        }

        if(typeof this.options.bookmark !== "undefined" && this.options.bookmark !== null){
            bookmark = this.options.bookmark;
        }

        var tag = document.createElement('script');
        tag.src = "https://f.vimeocdn.com/js/froogaloop2.min.js";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        var $this = this;

        setTimeout(function(){
            for(;;){
                if(typeof $f !== "undefined"){
                    if($('iframe#vimeo_player').length > 0) break;
                    var playerElement = $('#vimeo_player-element');
                    var parentElementHeight = playerElement.parent().height();
                    //Minus LO controls
                    var playerHeight = parentElementHeight - 44;
                    playerElement.append('<iframe id="vimeo_player" src="https://player.vimeo.com/video/' + $this.options.videoId + '?api=1&player_id=vimeo_player" width="100%" height="' + playerHeight + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
                    $this.run();
                    break;
                }
            }
        }, 500);
    },

    run: function(){
        var $this = this;
        var iframe = $('#vimeo_player')[0];
        this.player = $f(iframe);
        this.player.addEvent('ready', function(){

            //timeout because the video still isn't ready and some properties are wrong or missing
            //getDuration was returning incorrect duration.
            setTimeout(function(){
                $(document).off('adminViewActivated').on('adminViewActivated', function(){
                    if($this.state != 0){
                        $this.set('pause');
                    }
                });

                $this.get('getDuration', function(data) {
                    durationGlobal = data;
                    if(Math.ceil(bookmark) == durationGlobal){
                        bookmark = 0;
                    }

                    $this.set('seekTo', bookmark);
                });

                $this.player.addEvent('play', function(){
                    $this.state = 1;
                    track();
                });

                $this.player.addEvent('pause', function(){
                    $this.state = 0;
                    track();
                });

                $this.player.addEvent('finish', function(){
                    $this.state = 0;
                    track(true, true);
                });

                $this.player.addEvent('playProgress', function(data, id) {
                    currentTimeGlobal = data.seconds;
                });

                if(courseIdGlobal !== false) {
                    setInterval(function () {
                        if ($this.state !== 0) {
                            track(true, true);
                        }
                    }, 3000);
                }
            },300);
        });
    },

    get: function(method, callback){
        this.player.api(method, function(data) {
            if(typeof callback === "function"){
                callback(data);
            }
        });
    },

    set: function(method, value){
        var data = {
            method: method
        };

        if(value){
            data.value = value;
        }

        var message = JSON.stringify(data);
        if(this.player.element.contentWindow){
            this.player.element.contentWindow.postMessage(message, '*');
        }
    }
};

YoutubeCustomPlayer = function(options){
    this.player = false;
    this.options = [];

    if(typeof options !== "undefined"){
        this.init(options);
    }
};

YoutubeCustomPlayer.prototype = {
    init: function(options){
        var $this = this;
        this.options = $.extend([], this.options, options);
        if(typeof this.options.trackUrl !== "undefined" && this.options.trackUrl !== null){
            this.trackUrl = options.trackUrl;
            trackUrlGlobal = options.trackUrl;
        }
        if(typeof this.options.course_id !== "undefined" && this.options.course_id !== null){
            courseIdGlobal = this.options.course_id;
        }

        if(typeof this.options.idReference !== "undefined" && this.options.idReference !== null){
            idReferenceGlobal = this.options.idReference;
        }

        if(typeof this.options.statusAttempted !== "undefined" && this.options.statusAttempted !== null){
            statusAttempted = this.options.statusAttempted;
        }

        if(typeof this.options.statusIntro !== "undefined" && this.options.statusIntro !== null){
            statusIntro = this.options.statusIntro;
        }

        if(typeof this.options.statusCompleted !== "undefined" && this.options.statusCompleted !== null){
            statusCompleted = this.options.statusCompleted;
        }

        if(typeof this.options.bookmark !== "undefined" && this.options.bookmark !== null){
            bookmark = this.options.bookmark;
        }

        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        setTimeout(function(){
            for(;;){
                if($this.onYoutubeIframeReady()){
                    $this.run();
                    break;
                }
            }
        }, 500);
    },

    run: function(){
        var $this = this;

        $(document).off('adminViewActivated').on('adminViewActivated', function(){
            $this.player.pauseVideo();
        });

        if(courseIdGlobal !== false) {
            setInterval(function () {
                // $this.track();
            }, 3000);
        }
    },

    onYoutubeIframeReady: function(){
        var $this = this;
        var playerContainer = $('#youtube-player');
        var parentElementHeight = playerContainer.parent().height();
        //Minus LO controls
        var playerHeight = parentElementHeight - 44;

        if(typeof YT !== "undefined"){
            this.player = new YT.Player('youtube-player', {
                height: playerHeight,
                width: '100%',
                videoId: $this.options.videoId,
                events: {
                    'onReady': $this.onPlayerReady,
                    'onStateChange': $this.onPlayerStateChange
                }
            });

            return true;
        }
        return false;
    },

    onPlayerReady: function(event){
        if(bookmark !== false){
            durationGlobal = event.target.getDuration();
            if(Math.ceil(bookmark) == durationGlobal){
                bookmark = 0;
            }
            currentTimeGlobal = bookmark;
            event.target.playVideo();
            setTimeout(function() {
                event.target.seekTo(bookmark);
            }, 1500)
        }
        track(false, true);
    },

    track: function(){
        var $this = this;
        var playbackState = this.player.getPlayerState();
        if(playbackState === YT.PlayerState.PLAYING){
            currentTimeGlobal = $this.player.getCurrentTime();
            track(false, true);
        }
    },

    onPlayerStateChange:function(event){
        currentTimeGlobal = event.target.getCurrentTime();
        track(false, true);
    }
};

//Global Listeners for tracking
//Save bookmark when closing the tab
window.onbeforeunload = function () {
    track();
};


//Save bookmark when going to another URL or refreshing, going back
$(window).unload(function () {
    track();
    return true;
});

//Added functionality to the "X"-close button, added by the LMS, to save the bookmark
$('#inline-player-close').on('click', function (e) {
    track();
    if (typeof Arena.learningObjects !== "undefined") {
        Arena.learningObjects.ajaxUpdate();
    }
    window.location.reload();
});