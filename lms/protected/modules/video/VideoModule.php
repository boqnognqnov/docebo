<?php

class VideoModule extends CWebModule
{
	private $_assetsUrl;

	public function init()
	{
		$this->setImport(array(
			//'video.models.*',
			'video.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			return true;
		}
		else
			return false;
	}

	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('video.assets'));
		}
		return $this->_assetsUrl;
	}

}
