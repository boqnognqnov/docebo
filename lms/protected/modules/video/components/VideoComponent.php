<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2014 Docebo
 */

class VideoComponent extends CComponent {

	/**
	 * Class constructor
	 */
	private function __construct() {}


	public static function track($idUser, $idReference, $idResource, $status) {

		if (Yii::app()->db->getCurrentTransaction() === NULL) {
			$transaction = Yii::app()->db->beginTransaction();
		}

		try {
			$saveTrack = true;
			$track = LearningVideoTrack::model()->findByAttributes(array('idReference' => $idReference, 'idUser' => $idUser));
			if (!$track) { //the object has not been tracked yet for this user: create the record now
				//now prepare track values
				$track = new LearningVideoTrack();
				$track->idUser = (int)$idUser;
				$track->idReference = (int)$idReference;
				$track->idResource = (int)$idResource;
				$track->status = $status;
			} else { //a track record already exists: update it
				if ($status != LearningCommontrack::STATUS_COMPLETED && $track->status == LearningCommontrack::STATUS_COMPLETED) {
					//video has already been completed in the past: do not override the "completed" status with a lower one
					$saveTrack = false;
					$rs = $track->updateCommontrack(); //update date of the attempt without changing tracking status
					if (!$rs) {
						throw new CException("Error while updating track info");
					}
				} else {
					$track->status = $status;
				}
			}

			if ($saveTrack) {
				if (!$track->save()) {
					throw new CException("Error while saving track info");
				}
			}

			if (isset($transaction)) {
				$transaction->commit();
			}
			$output = true;

		} catch (CException $e) {

			if (isset($transaction)) {
				$transaction->rollback();
			}
			$output = false; //... or throw the exception to the above level ?
		}

		return $output;
	}

}