<?php
/* @var $videoModel LearningVideo */
/* @var $mediaUrl string */

$this->widget('common.extensions.jplayer.jPlayerWidget', array(
    'mediaUrl' => $mediaUrl,
    'videoId' => 'lo_video_' . $videoModel->id_video,
    'title' => $videoModel->title,
    'layout' => 'course_videoplayer',
    'skin' => 'course_videoplayer',
));
?>
<!-- --- JS ------------------------------------------------------------------------------------------------------ -->
<script type="text/javascript">
    /*<![CDATA[*/

    $(document).ready(function () {
        // start video
        $("#jquery_jplayer").bind($.jPlayer.event.play, function (event) {
            $.ajax({
                url: 'index.php?r=video/default/axTrack',
                type: 'post',
                dataType: 'JSON',
                data: {
                    course_id: Arena.course_id,
                    id_reference: <?php echo (int)$idReference; ?>,
                    status: '<?php echo LearningCommontrack::STATUS_ATTEMPTED; ?>'
                },
                success: function() {
                    if(typeof Arena.learningObjects != 'undefined')
                        Arena.learningObjects.ajaxUpdate();
                }

            });

        });

        // end video
        $("#jquery_jplayer").bind($.jPlayer.event.ended, function (event) {
            $.ajax({
                url: 'index.php?r=video/default/axTrack',
                type: 'post',
                dataType: 'JSON',
                data: {
                    course_id: Arena.course_id,
                    id_reference: <?php echo (int)$idReference; ?>,
                    status: '<?php echo LearningCommontrack::STATUS_COMPLETED; ?>'
                },
                success: function() {
                    if(typeof Arena.learningObjects != 'undefined')
                        Arena.learningObjects.ajaxUpdate();
                }
            });
        });
    });

    setTimeout("keepalive()", 5*60*1000);
    function keepalive() {
        var d = new Date();
        var time = d.getTime();
        $.ajax({
            url: <?=CJavaScript::encode($this->createUrl('keepAlive', array('course_id' => $this->getIdCourse())))?>,
            dataType: 'json'
        });
        setTimeout("keepalive()", 5*60*1000);
    }

    /*]]>*/
</script>