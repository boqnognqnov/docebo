<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 11.5.2016 г.
 * Time: 16:52
 *
 * @var $this DefaultController
 * @var $course_id integer
 * @var $idReference integer
 * @var $videoId string
 * @var $bookmark integer
 */
?>
<div id="youtube-player"></div>
<script>
    var youtubePlayer = new YoutubeCustomPlayer({
        trackUrl: <?= json_encode(Docebo::createLmsUrl('video/default/axTrack', array())) ?>,
        course_id: <?= json_encode($course_id) ?>,
        idReference: <?= json_encode($idReference) ?>,
        statusAttempted: <?= json_encode(LearningCommontrack::STATUS_ATTEMPTED) ?>,
        statusIntro: <?= json_encode(LearningCommontrack::STATUS_AB_INITIO) ?>,
        statusCompleted: <?= json_encode(LearningCommontrack::STATUS_COMPLETED) ?>,
        videoId: <?= json_encode($videoId) ?>,
        bookmark: <?= json_encode($bookmark) ?>
    });
</script>
