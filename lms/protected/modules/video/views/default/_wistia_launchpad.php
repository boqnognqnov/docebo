<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 12.5.2016 г.
 * Time: 15:00
 * @var $course_id integer
 * @var $idReference integer
 * @var $videoId string
 * @var $bookmark float
 * @var $url string
 */
?>

<div id="wistia_player-element"></div>

<script>
    var vimeoPlayer = new WistiaCustomPlayer({
        trackUrl: <?= json_encode(Docebo::createLmsUrl('video/default/axTrack', array())) ?>,
        course_id: <?= json_encode($course_id) ?>,
        idReference: <?= json_encode($idReference) ?>,
        statusAttempted: <?= json_encode(LearningCommontrack::STATUS_ATTEMPTED) ?>,
        statusIntro: <?= json_encode(LearningCommontrack::STATUS_AB_INITIO) ?>,
        statusCompleted: <?= json_encode(LearningCommontrack::STATUS_COMPLETED) ?>,
        videoId: <?= json_encode($videoId) ?>,
        bookmark: <?= json_encode($bookmark) ?>,
        url: <?= json_encode($url) ?>
    });
</script>