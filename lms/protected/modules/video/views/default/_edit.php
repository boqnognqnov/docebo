<?php
	/* @var $videoObj LearningVideo */
	/* @var $form CActiveForm */
?>


<?php
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'htmlpage-editor-form',
		'method' => 'post',
		'action' => $this->createUrl("/htmlpage/default/axSaveLo"),
		'htmlOptions' => array()
	));
?>

	<?= $form->hiddenField($videoObj, 'idPage'); ?>


	<?= CHtml::label(Yii::t('standard', '_TITLE'), 'title', array('class'=>'')) ?>
	<?= $form->textField($videoObj, 'title', array('class'=>'input-block-level')) ?>

	<br>
	<br>
	<?= CHtml::label(Yii::t('standard', '_DESCRIPTION'), 'textof', array('class'=>'')) ?>
	<?= $form->textArea($videoObj, 'textof', array('id' => 'player-arena-htmlpage-editor-textarea')); ?>

	<br>
	<div class="text-right">
		<input type="submit" name="confirm_save_htmlpage" class="btn-docebo green"  value="<?php echo Yii::t('standard', '_SAVE'); ?>">
		<a id="cancel_save_htmlpage" class="btn-docebo black"><?php echo Yii::t('standard', '_CANCEL'); ?></a>
	</div>

<?php $this->endWidget(); ?>
