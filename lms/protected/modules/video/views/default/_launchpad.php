<?php
/**
 * @var $videoModel LearningVideo
 * @var $this DefaultController
 * @var $bookmark integer
 * @var $autoplay bool
 * @var $completed bool
 */
?>

<div class="player-launchpad-header" style="display: block;">
    <h2><?= $videoModel->title ?></h2>
	<?php
		if (PluginManager::isPluginActive('Coach7020App')) {
			$this->widget('common.widgets.CourseLoHeaderApp7020', array('loID' => $idReference));
		}
	?>
	<div style="cursor: pointer;" id="inline-player-close"  class="player-close">Close</div>
</div>

<?php
if($videoModel->type === LearningVideo::VIDEO_TYPE_UPLOAD):
// Check if HLS is disabled by global Yii parameter
$disableHls = false;
if (isset(Yii::app()->params['disableHls'])) {
	$disableHls = Yii::app()->params['disableHls'];
}

$seekable = $videoModel->seekable;

if($videoModel->seekable == LearningVideo::VIDEO_SEEKABLE_AFTER_COMPLETE && !$completed)
	$seekable = LearningVideo::VIDEO_SEEKABLE_NO;
elseif($videoModel->seekable == LearningVideo::VIDEO_SEEKABLE_AFTER_COMPLETE && $completed)
	$seekable = LearningVideo::VIDEO_SEEKABLE_YES;

$playerWidget = $this->widget('common.widgets.Flowplayer', array(
	'disableHls' => $disableHls,
	'mediaUrlMp4' => $mediaUrlMp4,
	'mediaUrlHlsPlaylist' => $mediaUrlHlsPlaylist,
	'autoplay' => $autoplay,
	// 'videoId'               => 'lo_video_' . $videoModel->id_video,
	'title' => $videoModel->title,
	// 'layout'                => 'course_videoplayer',
	'skin' => 'functional',
	'bookmark' => $bookmark,
	'seekable' => $seekable,
	'subtitlesUrls' => $subs
		)
);

$containerId = $playerWidget->containerId;
?>

<script type="text/javascript">
	/*<![CDATA[*/
	var keepAliveUrl = <?= json_encode($this->createUrl('keepAlive', array('course_id' => $this->getIdCourse()))) ?>;
	var progress<?= $playerWidget->id ?> = 0;
	$(document).ready(function () {

		// Make all PHP based JS variable here, in advance, for the sake of readbility
		var trackUrl = <?= json_encode(Docebo::createLmsUrl('video/default/axTrack', array())) ?>;
		var idReference = <?= json_encode((int) $idReference) ?>;
		var containerId = <?= json_encode($containerId) ?>;

		var statusAbInito = <?= json_encode(LearningCommontrack::STATUS_AB_INITIO) ?>;
		var statusAttempted = <?= json_encode(LearningCommontrack::STATUS_ATTEMPTED) ?>;
		var statusCompleted = <?= json_encode(LearningCommontrack::STATUS_COMPLETED) ?>;
		var periodOfBookmarkSaving = 30;
		// Start Flowplayer
		var fpObject = flowplayer($(containerId));


		//Added functionality to the "X"-close button, added by the LMS, to save the bookmark
		$('#inline-player-close').on('click', function (e) {
			var bookmark = fpObject.video.time;
			var status = statusAttempted;
			switch (parseInt(fpObject.video.time)) {
				case 0:
					bookmark = 0;
					status = statusAbInito;
					break;
				case parseInt(fpObject.video.duration):
					bookmark = fpObject.video.duration;
					status = statusCompleted;
					break;
			}
			$.ajax({
				url: trackUrl,
				type: 'post',
				dataType: 'JSON',
				data: {
					course_id: Arena.course_id,
					id_reference: idReference,
					status: status,
					bookmark: bookmark
				},
				success: function () {
					if (typeof Arena.learningObjects !== "undefined") {
						Arena.learningObjects.ajaxUpdate();
					}
				}
			}).done(function(){
				window.location.reload();
			});
		});


		//Save bookmark when closing the tab
		window.onbeforeunload = function () {
			var bookmark = fpObject.video.time;
			var status = statusAttempted;
			switch (parseInt(fpObject.video.time)) {
				case 0:
					bookmark = 0;
					status = statusAbInito;
					break;
				case parseInt(fpObject.video.duration):
					bookmark = fpObject.video.duration;
					status = statusCompleted;
					break;
			}
			$.ajax({
				url: trackUrl,
				type: 'post',
				dataType: 'JSON',
				data: {
					course_id: Arena.course_id,
					id_reference: idReference,
					status: status,
					bookmark: bookmark
				},
				success: function () {
					if (typeof Arena.learningObjects !== "undefined") {
						Arena.learningObjects.ajaxUpdate();
					}
				}
			});
		};


		//Save bookmark when going to another URL or refreshing, going back
		$(window).unload(function () {
			var bookmark = fpObject.video.time;
			var status = statusAttempted;
			switch (parseInt(fpObject.video.time)) {
				case 0:
					bookmark = 0;
					status = statusAbInito;
					break;
				case parseInt(fpObject.video.duration):
					bookmark = fpObject.video.duration;
					status = statusCompleted;
					break;
			}
			$.ajax({
				url: trackUrl,
				type: 'post',
				dataType: 'JSON',
				data: {
					course_id: Arena.course_id,
					id_reference: idReference,
					status: status,
					bookmark: bookmark
				},
				success: function () {
					if (typeof Arena.learningObjects !== "undefined") {
						Arena.learningObjects.ajaxUpdate();
					}
				}
			});
			return true;
		});


		//Reset the bookmarking counter if the user changes the playback pointer(seeks by hand)
		fpObject.on("beforeseek", function (event) {
			progress<?= $playerWidget->id ?> = 0;
		});


		//Save bookmark for every [periodOfBookmarkSaving] seconds
		//This event fires avery 250 miliseconds (1/4 of a second)
		fpObject.on("progress", function (event) {
			progress<?= $playerWidget->id ?>++;
			if (((progress<?= $playerWidget->id ?> / 4) % periodOfBookmarkSaving) == 0)
			{
				$.ajax({
					url: trackUrl,
					type: 'post',
					dataType: 'JSON',
					data: {
						course_id: Arena.course_id,
						id_reference: idReference,
						status: statusAttempted,
						bookmark: fpObject.video.time
					},
					success: function () {
						if (typeof Arena.learningObjects !== "undefined") {
							Arena.learningObjects.ajaxUpdate();
						}
					}
				});
			}
		});

		//Track the video LO - set it as ATTEMPTED when played
		fpObject.on("resume", function (event) {
			$.ajax({
				url: trackUrl,
				type: 'post',
				dataType: 'JSON',
				data: {
					course_id: Arena.course_id,
					id_reference: idReference,
					status: statusAttempted
				},
				success: function () {
					if (typeof Arena.learningObjects !== "undefined") {
						Arena.learningObjects.ajaxUpdate();
					}
				}
			});
		});

		//Track the video LO - set it as COMPLETED when finished
		fpObject.on("finish", function (event) {

//            fpObject.seek(0);

			$.ajax({
				url: trackUrl,
				type: 'post',
				dataType: 'JSON',
				data: {
					course_id: Arena.course_id,
					id_reference: idReference,
					status: statusCompleted,
					bookmark: fpObject.video.duration
				},
				success: function () {
					if (typeof Arena.learningObjects !== "undefined") {
						Arena.learningObjects.ajaxUpdate();
					}
					if(typeof PlayerNavigator !== "undefined") {
						PlayerNavigator.init({ajaxUrl: '<?=Docebo::createLmsUrl('player/training/axGetPlayerNavigator', array('course_id' => $this->getIdCourse()))?>'});
						PlayerNavigator.setCurrentKey(<?=$idReference?>);
						PlayerNavigator.reload();
					}
				}
			});
		});
	});


	setTimeout("keepalive()", 5 * 60 * 1000);
	function keepalive() {
		var d = new Date();
		var time = d.getTime();
		$.ajax({
			url: keepAliveUrl,
			dataType: 'json'
		});
		setTimeout("keepalive()", 5 * 60 * 1000);
	}

	/*]]>*/
</script>

<?php else: ?>
	<?php
	$options = array(
		'course_id' => $this->getIdCourse(),
		'idReference' => $idReference,
		'videoId' => $videoModel->videoId,
		'bookmark' => $bookmark ? $bookmark : 0,
		'url' => $videoModel->url
	);
	if($videoModel->type === LearningVideo::VIDEO_TYPE_YOUTUBE){
		$this->renderPartial('_youtube_launchpad', $options);
	} else if($videoModel->type === LearningVideo::VIDEO_TYPE_VIMEO){
		$this->renderPartial('_vimeo_launchpad', $options);
	} else if($videoModel->type === LearningVideo::VIDEO_TYPE_WISTIA){
		$this->renderPartial('_wistia_launchpad', $options);
	}
	?>

<?php endif; ?>