<?php

class DefaultController extends VideoBaseController {


	protected $allowedTypes = array('mp4', 'ogv', 'webm', 'flv');


	public $validLo = true;

	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {

		return array(
			// Admins and Teachers can do all
			array('allow',
				'users' => array('@'),
				'expression' => 'Yii::app()->user->getIsPu() || Yii::app()->controller->userCanAdminCourse && Yii::app()->controller->validLo',
			),

			// For Subscribers
			array('allow',
				'actions' => array('index', 'axLaunchpad', 'axTrack', 'keepAlive'),
				'users' => array('@'),
				'expression' => 'Yii::app()->controller->userIsSubscribed && Yii::app()->controller->validLo',
			),

			// If some permission is wrong -> the script goes here
			array('deny',
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
				'message' => Yii::t('course', '_NOENTER'),
			),
		);

	}


	/**
	 * (non-PHPdoc)
	 * @see CController::init()
	 */
	public function init() 	{
		parent::init();
	}


	/**
	 * Common tracking function for this controller
	 * @param $idUser
	 * @param $idReference
	 * @param $idResource
	 * @param $status
	 * @return bool
	 * @throws CException
	 */
	protected function _track($idUser, $idReference, $idResource, $status, $bookmark=false) {

		if (Yii::app()->db->getCurrentTransaction() === NULL) {
			$transaction = Yii::app()->db->beginTransaction();
		}

		try {
			$saveTrack = true;
			$track = LearningVideoTrack::model()->findByAttributes(array('idReference' => $idReference, 'idUser' => $idUser));
			if (!$track) { //the object has not been tracked yet for this user: create the record now
				//now prepare track values
				$track = new LearningVideoTrack();
				$track->idUser = (int)$idUser;
				$track->idReference = (int)$idReference;
				$track->idResource = (int)$idResource;
				$track->status = $status;
			} else { //a track record already exists: update it
				if ($status != LearningCommontrack::STATUS_COMPLETED && $track->status == LearningCommontrack::STATUS_COMPLETED) {
					//video has already been completed in the past: do not override the "completed" status with a lower one
					$saveTrack = false;
					$rs = $track->updateCommontrack(); //update date of the attempt without changing tracking status
					if (!$rs) {
						throw new CException("Error while updating track info");
					}
				} else {
					$track->status = $status;
				}
			}
            if($bookmark)
                $track->updateBookmark($bookmark);

			if ($saveTrack) {
				if (!$track->save()) {
					throw new CException("Error while saving track info");
				}
			}

			if (isset($transaction)) {
				$transaction->commit();
			}
			$output = true;

		} catch (CException $e) {

			if (isset($transaction)) {
				$transaction->rollback();
			}
			$output = false; //... or throw the exception to the above level ?
		}

		return $output;
	}


	/**
	 * By default, just redirect to main site.
	 *
	 */
	public function actionIndex() {
		$this->redirect($this->createUrl('/site'), true);
	}


	/**
	 * Save uploaded LO. Caller must already saved the file into
	 * <root>/files/upload_tmp folder.
	 *
	 */
	public function actionAxSaveLo() {
		$idOrg = Yii::app()->request->getParam("id_org");
		if (intval($idOrg) > 0) {
			// edit operation
			$this->forward('default/axEditLo');
		}

		$courseId = Yii::app()->request->getParam("course_id", null);
		$parentId = Yii::app()->request->getParam("parent_id", null);
		$file = Yii::app()->request->getParam("file", null);
		$seekable = Yii::app()->request->getParam("seekable", 0);

        $fallbackSub = Yii::app()->request->getParam("fallbackSub", '');
        $subtitlesArray = Yii::app()->request->getParam("subtitles", array());
        $idVideoRes = Yii::app()->request->getParam("idVideoRes", false);
		$videoSource = Yii::app()->request->getParam('videoSource', false);
		$videoUrl = Yii::app()->request->getParam('videoUrl', false);

		// Use the physically uploaded filename, put by PLUpload in the "target_name" element
		// This is tru when "unique_names" option is used for plupload JavaScript object
		if($videoSource != 1){
			if (is_array($file) && isset($file['name'])) {
				$originalFileName = urldecode($file['name']);
				if (isset($file['target_name'])) {
					$file['name'] = $file['target_name'];
				}
			}
		}

		$thumb = Yii::app()->request->getParam("thumb", null);

		// URLDecode
		$title = urldecode(trim(Yii::app()->request->getParam("title", null)));
		$description = urldecode(Yii::app()->request->getParam("description", null));
		if (is_array($file) && isset($file['name'])) {
			$shortDescription = urldecode(Yii::app()->request->getParam("short_description", ''));
			$file['name'] = urldecode($file['name']);
		}


		$response = new AjaxResult();
		$transaction = Yii::app()->db->beginTransaction();
		$videoType = LearningVideo::VIDEO_TYPE_UPLOAD;

		try {
			// Validate input parameters
			if (!$courseId) {
				throw new CException("Invalid specified course.");
			}
			if (!is_numeric($parentId) || $parentId < 0) {
				$parentId = 0;
			}
			if (empty($title)) {
				throw new CException(Yii::t('error', 'Enter a title!'));
			}

			if($videoSource != 1){
				// Validate file input
				if (!is_array($file)) {
					throw new CException("Invalid file.");
				}
				if (!isset($file['name']) || ($file['name'] == '')) {
					throw new CException("Invalid file name.");
				}
				if (!isset($file['percent']) || $file['percent'] < 100) {
					throw new CException("Incomplete file upload.");
				}

				// Check if uploaded file exists
				$uploadTmpFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $file['name'];
				if (!is_file($uploadTmpFile)) {
					throw new CException("File '" . $file['name'] . "' does not exist.");
				}

				// Create storage Object, use it to determine the VIDEO whitelist type to use
				$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_VIDEO);
				$whitelistType = ($storage->type == CFileStorage::TYPE_S3) ? FileTypeValidator::TYPE_VIDEO_EXTENDED_LIST : FileTypeValidator::TYPE_VIDEO;

				/*** File extension type validate ************************/
				$fileValidator = new FileTypeValidator($whitelistType);
				if (!$fileValidator->validateLocalFile($uploadTmpFile))
				{
					FileHelper::removeFile($uploadTmpFile);
					throw new CException($fileValidator->errorMsg);
				}
				/*********************************************************/

				// Rename uploaded file
				$extension = strtolower(CFileHelper::getExtension($file['name']));
				$newFileName = Docebo::randomHash() . "." . $extension;
				$newTmpFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $newFileName;
				if (!@rename($uploadTmpFile, $newTmpFile)) {
					throw new CException("Error renaming uploaded file.");
				}


				// Move uploaded file to final storage for later usage
				// The result from store() is FALSE OR the path (key for S3) to the file
				$destFilePath = $storage->store($newTmpFile);
				if (!$destFilePath) {
					throw new CException("Error while saving file to final storage.");
				}
				// store() MAY change format and extension of the file! Lets get them back
				$destExtension =  strtolower(CFileHelper::getExtension(pathinfo($destFilePath, PATHINFO_BASENAME)));
				$destFilename  =  pathinfo($destFilePath, PATHINFO_BASENAME);

				// Save the video information in learning_video table
				$hls_id = VideoConverter::HLS_ID;
				$hlsIdValue = preg_replace('/\\.[^.\\s]{3,4}$/', '', $destFilename);
				$videoFormats = new stdClass();
				$videoFormats->$destExtension = $destFilename;
				$videoFormats->$hls_id = $hlsIdValue;
			} else{
				$videoId = '';

				if(($id = App7020WebsiteProceeder::isVimeoLink($videoUrl)) !== false){
					$videoId = $id;
					$videoType = LearningVideo::VIDEO_TYPE_VIMEO;
				} else if(($id = App7020WebsiteProceeder::isYoutubeLink($videoUrl)) !== false){
					$videoId = $id;
					$videoType = LearningVideo::VIDEO_TYPE_YOUTUBE;
				} else if(($id = App7020WebsiteProceeder::isWistiaLink($videoUrl)) !== false){
					$videoId = $id;
					$videoType = LearningVideo::VIDEO_TYPE_WISTIA;
				} else {
					throw new CException("Video URL is not valid");
				}

				$videoFormats = array(
					'id' => $videoId,
					'url' => urlencode($videoUrl)
				);
			}

			$video = new LearningVideo();
			$video->title = $title;
			$video->description = $description;
			$video->video_formats = CJSON::encode($videoFormats);
			$video->seekable = $seekable;
			$video->type = $videoType;
			if (!$video->save()) {
				throw new CException("Error while creating resource object.");
			}
			$resourceId = $video->id_video;

			// Now save info in learning_object table
			$loManager = new LearningObjectsManager();
			$loManager->setIdCourse($courseId);
			$loParams['description'] = $description;
			$loParams['short_description'] = $shortDescription;

			if ($storage->transcodeVideo) {
				// Add "Converting..." to LO title to inform user about the process
				//$title = $title . " [".Yii::t('standard', 'Converting')."...] ";
				$loParams['visible'] = -1;
			}

			if($thumb){
				$loParams['resource'] = $thumb;
			}

			$loModel = $loManager->addLearningObject(LearningOrganization::OBJECT_TYPE_VIDEO, $resourceId, $title, $parentId, $loParams);
			if (!$loModel) {
				throw new CException("Error while creating organization object.");
			}


			// DELETE tmp file
			FileHelper::removeFile($newTmpFile);



			// Trigger event to let plugins save their own data from POST
			Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
				'loModel' => $loModel
			)));

			if (empty($resourceId)) {
				$resourceId = $idVideoRes;
			}

			// Add subtitles here
			if (count($subtitlesArray) > 0) {
				foreach ($subtitlesArray as $subLangCode => $subtitle) {
					// There are no subtitles per this language add new subtitle record and new core asset and save them
					// 1. Create new Video Subtitle model
					$videoSubModel = new LearningVideoSubtitles();

					// 2. Set the Video Subtitle Details
					$videoSubModel->date_added = Yii::app()->localtime->getUTCNow();
					$videoSubModel->added_by = Yii::app()->user->idst;
					$videoSubModel->lang_code = $subLangCode;
					$videoSubModel->fallback = 0;
					$videoSubModel->id_video = $resourceId;

					// 3. Create new CoreAsset
					$subAsset = new CoreAsset();

					// 4. Set the temporary sub file details
					$subAsset->original_filename = $subtitle['original_filename'];
					$subAsset->type = CoreAsset::TYPE_SUBTITLES . "/" . $resourceId;
					// Set the actual temp filepath
					$subAsset->sourceFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $subtitle['target_filename'];

					// 5. Save the asset
					if ($subAsset->save()) {
						// 6. Update the video subtitle db record via the corresponding model
						$videoSubModel->asset_id = $subAsset->id;

						// 7. Save the video subtitle model
						$videoSubModel->save();
					}

					// 8. Clean the temporary subtitle file
					FileHelper::removeFile(Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $subtitle['target_filename']);
				}
			}

			// If there is fallback subtitle passed for this video, update all the video subtitles records according to it !!!
			if ($fallbackSub) {
				// Update
				if (preg_match('#[a-z]#i', $fallbackSub)){
					LearningVideoSubtitles::model()->updateAll(array('fallback' => 0), 'id_video = ' . $resourceId . ' AND lang_code != "' . $fallbackSub . '"');
					LearningVideoSubtitles::model()->updateAll(array('fallback' => 1), 'id_video = ' . $resourceId . ' AND lang_code = "' . $fallbackSub . '"');
				} else {
					LearningVideoSubtitles::model()->updateAll(array('fallback' => 0), 'id_video = ' . $resourceId . ' AND id != ' . $fallbackSub);
					LearningVideoSubtitles::model()->updateAll(array('fallback' => 1), 'id_video = ' . $resourceId . ' AND id = ' . $fallbackSub);
				}
			}

			// DB Commit
			$transaction->commit();

			$data = array(
				'resourceId' => (int)$resourceId,
				'organizationId' => (int)$loModel->idOrg,
				'parentId' => (int)$parentId,
				'title' => $title,
				'message' => Yii::t('standard', '_OPERATION_SUCCESSFUL'),
			);
			$response->setData($data)->setStatus(true);

		} catch (CException $e) {
			$uploadTmpFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $file['name'];

			$msg = 'Video upload error:<br />
<br />
Platform: '.Yii::app()->createAbsoluteUrl('site/index').'
Uploaded file: '.$uploadTmpFile.'
Error code: '.$e->getCode().'<br />
Error message: '.$e->getMessage().'<br />
Trace: '.$e->getTrace().'<br />
POST: '.nl2br(var_export($_POST, true)).'<br />
GET: '.nl2br(var_export($_GET, true)).'';

			Yii::log($msg, 'mail');

			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			$transaction->rollback();
			$response->setMessage($e->getMessage())->setStatus(false);
		}

		echo $response->toJSON();
		Yii::app()->end();

	}


	public function actionAxEditLo() {
		$courseId = Yii::app()->request->getParam("course_id", null);
		$parentId = Yii::app()->request->getParam("parent_id", null);
		$file = Yii::app()->request->getParam("file", null);
		$seekable = Yii::app()->request->getParam("seekable", 0);

		$fallbackSub = Yii::app()->request->getParam("fallbackSub", '');
		$subtitlesArray = Yii::app()->request->getParam("subtitles", array());
		$idVideoRes = Yii::app()->request->getParam("idVideoRes", false);

		$videoSource = Yii::app()->request->getParam('videoSource', false);
		$videoUrl = Yii::app()->request->getParam('videoUrl', false);
		$videoType = LearningVideo::VIDEO_TYPE_UPLOAD;

		// Use the physically uploaded filename, put by PLUpload in the "target_name" element
		// This is tru when "unique_names" option is used for plupload JavaScript object
		if($videoSource != 1){
			if (is_array($file) && isset($file['name'])) {
				$originalFileName = urldecode($file['name']);
				if (isset($file['target_name'])) {
					$file['name'] = $file['target_name'];
				}
			}
		}

		$thumb = Yii::app()->request->getParam("thumb", null);

		// URLDecode
		$title = urldecode(trim(Yii::app()->request->getParam("title", null)));
		$description = urldecode(Yii::app()->request->getParam("description", null));
		$shortDescription = urldecode(Yii::app()->request->getParam("short_description", ''));
		if (is_array($file) && isset($file['name'])) {
			$file['name'] = urldecode($file['name']);
		}

		$response = new AjaxResult();
		$transaction = Yii::app()->db->beginTransaction();

		try {
			$loModel = LearningOrganization::model()->findByPk(Yii::app()->request->getParam("id_org"));
			$videoObj = LearningVideo::model()->findByPk($loModel->idResource);

			if (!$videoObj) {
				throw new CException('Invalid file');
			}

			// when editing, if the file wasn't change, we expect to receive 'file'==>false
			if ($file !== 'false') {
				// Validate input parameters
				if (!$courseId) {
					throw new CException("Invalid specified course.");
				}
				if (!is_numeric($parentId) || $parentId < 0) {
					$parentId = 0;
				}
				if (empty($title)) {
					throw new CException(Yii::t('error', 'Enter a title!'));
				}

				// Validate file input
				if (!is_array($file)) {
					throw new CException("Invalid file.");
				}
				if (!isset($file['name']) || ($file['name'] == '')) {
					throw new CException("Invalid file name.");
				}
				if (!isset($file['percent']) || $file['percent'] < 100) {
					throw new CException("Incomplete file upload.");
				}

				// Check if uploaded file exists
				$uploadTmpFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $file['name'];
				if (!is_file($uploadTmpFile)) {
					throw new CException("File '" . $file['name'] . "' does not exist.");
				}


				$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_VIDEO);
				$whitelistType = ($storage->type == CFileStorage::TYPE_S3) ? FileTypeValidator::TYPE_VIDEO_EXTENDED_LIST : FileTypeValidator::TYPE_VIDEO;

				/*** File extension type validate ************************/
				$fileValidator = new FileTypeValidator($whitelistType);
				if (!$fileValidator->validateLocalFile($uploadTmpFile))
				{
					FileHelper::removeFile($uploadTmpFile);
					throw new CException($fileValidator->errorMsg);
				}
				/*********************************************************/

				// Rename uploaded file: add course Id and L-Org Id
				$extension = strtolower(CFileHelper::getExtension($file['name']));
				$newFileName = Docebo::randomHash() . "." . $extension;
				$newTmpFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $newFileName;
				if (!@rename($uploadTmpFile, $newTmpFile)) {
					throw new CException("Error renaming uploaded file.");
				}

				// Move uploaded file to final storage for later usage
				// store() must return either FALSE or PATH/Key to the destination file
				$destFilePath = $storage->store($newTmpFile);
				if (!$destFilePath) {
					throw new CException("Error while saving file to final storage.");
				}
				// store() MAY change format and extension of the file! Lets get them back
				$destExtension =  strtolower(CFileHelper::getExtension(pathinfo($destFilePath, PATHINFO_BASENAME)));
				$destFilename  =  pathinfo($destFilePath, PATHINFO_BASENAME);

				// DELETE tmp file
				FileHelper::removeFile($newTmpFile);

				// If This Video (as a file or hls id) is not referenced in another Video Object (for example, as a result of COPY operation)
				// then we are free to delete the file itself from S3.
				if (!$videoObj->isVideoFileReferencedElsewhere()) {

					/** @var  $old - the video_formats from the Old Object * */
					$old = CJSON::decode($videoObj->video_formats);

					// Check if old MP4 and HLS are NOT empty, then delete OLD mp4 file and the HLS folder
					if (isset($old['mp4']) && !empty($old['mp4'])) {
						Yii::log('Removing MP4 file:' . $old['mp4'], CLogger::LEVEL_INFO);
						$storage->remove($old['mp4']);      // Remove the mp4 file outside the folder
					}

					if (isset($old['hls_id']) && !empty($old['hls_id'])) {
						$folderToDelete = pathinfo($old['hls_id'], PATHINFO_FILENAME);
						Yii::log('Removing Folder:' . $folderToDelete, CLogger::LEVEL_INFO);
						$storage->removeFolder($folderToDelete); // Remove the folder from Amazon if there is existing one
					}

				}

				if ($idVideoRes != false && $idVideoRes > 0) {
					$videoTracks = LearningVideoTrack::model()->findAllByAttributes(array(
						'idResource' => (int)$idVideoRes // OLD video ID
					));
					if (is_array($videoTracks)) {
						foreach ($videoTracks as $trackModel) {
							$trackModel->bookmark = (int)0;
							$trackModel->save();
						}
					}
				}

				$hlsIdValue = pathinfo($destFilename, PATHINFO_FILENAME);
				$hls_id = VideoConverter::HLS_ID;
				$videoFormats = new stdClass();
				$videoFormats->$destExtension = $destFilename;
				$videoFormats->$hls_id = $hlsIdValue;
			}

			if($videoSource == 1){
				$videoId = '';

				if(($id = App7020WebsiteProceeder::isVimeoLink($videoUrl)) !== false){
					$videoId = $id;
					$videoType = LearningVideo::VIDEO_TYPE_VIMEO;
				} else if(($id = App7020WebsiteProceeder::isYoutubeLink($videoUrl)) !== false){
					$videoId = $id;
					$videoType = LearningVideo::VIDEO_TYPE_YOUTUBE;
				} else if(($id = App7020WebsiteProceeder::isWistiaLink($videoUrl)) !== false){
					$videoId = $id;
					$videoType = LearningVideo::VIDEO_TYPE_WISTIA;
				} else {
					throw new CException("Video URL is not valid");
				}

				$videoFormats = array(
					'id' => $videoId,
					'url' => urlencode($videoUrl)
				);
			}

			// Save the video information in learning_video table
			$videoObj->title = $title;
			$videoObj->description = $description;
            $videoObj->seekable = $seekable;
			$videoObj->type = $videoType;

            if (isset($videoFormats)) {
                $videoObj->video_formats = CJSON::encode($videoFormats);
            }

			if (!$videoObj->save()) {
				throw new CException("Error while creating or saving Video object.");
			}

			// update learning organization
			$loModel->title = $videoObj->title;

			// If the storage implies a video transcoding, we must set the visibility to (-1)
			// The transcoding process will change it to -2 (error) or 1 (success)
			if ($storage && $storage->transcodeVideo) {
				$loModel->visible = -1;
			}

			if(intval($thumb) > 0){
				$loModel->resource = intval($thumb);
			}elseif($loModel->resource && !$thumb){
				$loModel->resource = 0;
			}

			$loModel->short_description = $shortDescription;
			//The update date is saved so that in the LearningOrganization->setOldInProgressVideosToError() function we can check if the video file has been updated recently
			if($file !== 'false'){
				$loModel->date_updated = Yii::app()->localtime->toLocalDateTime();
			}


			$loModel->saveNode();

			$resourceId = $videoObj->id_video;
			if (empty($resourceId)) {
				$resourceId = $idVideoRes;
			}

			// Add subtitles here
			if (count($subtitlesArray) > 0) {
				foreach ($subtitlesArray as $subLangCode => $subtitle) {
					// Try to get the video subtitle for the lang code from the current iteration
					$videoSubModel = LearningVideoSubtitles::model()->findByAttributes(array('id_video' =>  (int) $resourceId, 'lang_code' => $subLangCode));

					// First, check is there already a subtitle for this language
					if ($videoSubModel) { // There is already a video subtitle
						// If there is a subtitle for this language, remove the old core asset and add a new core asset
						// save it and update the video subtitle db table record

						// 1. Get the old subtitle asset
						$subAsset = CoreAsset::model()->findByPk($videoSubModel->asset_id);
						if ($subAsset) {
							// 2. Delete the old asset
							$subAsset->delete();
						}

						// 3. Create a new asset
						$subAsset = new CoreAsset();

						// 4. Set the temporary sub file details
						$subAsset->type = CoreAsset::TYPE_SUBTITLES . "/" . $resourceId;
						// Set the actual temp filepath
						$subAsset->sourceFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $subtitle['target_filename'];
						$subAsset->original_filename = $subtitle['original_filename'];

						// 5. Save the asset
						if ($subAsset->save()) {
							// 6. Update the video subtitle db record via the corresponding model
							$videoSubModel->asset_id = $subAsset->id;

							// 7. Set the additional video subtitle model data
							$videoSubModel->date_modified = Yii::app()->localtime->getUTCNow();
							$videoSubModel->added_by = Yii::app()->user->idst;

							// 8. Save the video subtitle model
							$videoSubModel->save();
						}

						// 9. Clean the temporary subtitle file
						FileHelper::removeFile(Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $subtitle['target_filename']);
					} else {

						// Otherwise if there are no subtitles per this language add new subtitle record and new core asset and save them
						// 1. Create new Video Subtitle model
						$videoSubModel = new LearningVideoSubtitles();

						// 2. Set the Video Subtitle Details
						$videoSubModel->date_added = Yii::app()->localtime->getUTCNow();
						$videoSubModel->added_by = Yii::app()->user->idst;
						$videoSubModel->lang_code = $subLangCode;
						$videoSubModel->fallback = 0;
						$videoSubModel->id_video = $resourceId;

						// 3. Create new CoreAsset
						$subAsset = new CoreAsset();

						// 4. Set the temporary sub file details
						$subAsset->type = CoreAsset::TYPE_SUBTITLES . "/" . $resourceId;
						// Set the actual temp filepath
						$subAsset->sourceFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $subtitle['target_filename'];
						$subAsset->original_filename = $subtitle['original_filename'];

						// 5. Save the asset
						if ($subAsset->save()) {
							// 6. Update the video subtitle db record via the corresponding model
							$videoSubModel->asset_id = $subAsset->id;

							// 7. Save the video subtitle model
							$videoSubModel->save();
						}

						// 8. Clean the temporary subtitle file
						FileHelper::removeFile(Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $subtitle['target_filename']);
					}

				}
			}

			// If there is fallback subtitle passed for this video, update all the video subtitles records according to it !!!
			if ($fallbackSub) {
				// Update
				if (preg_match('#[a-z]#i', $fallbackSub)) {
					LearningVideoSubtitles::model()->updateAll(array('fallback' => 0), 'id_video = ' . $resourceId . ' AND lang_code != "' . $fallbackSub . '"');
					LearningVideoSubtitles::model()->updateAll(array('fallback' => 1), 'id_video = ' . $resourceId . ' AND lang_code = "' . $fallbackSub . '"');
				} else {
					LearningVideoSubtitles::model()->updateAll(array('fallback' => 0), 'id_video = ' . $resourceId . ' AND id != ' . $fallbackSub);
					LearningVideoSubtitles::model()->updateAll(array('fallback' => 1), 'id_video = ' . $resourceId . ' AND id = ' . $fallbackSub);
				}
			}

			// DB Commit
			$transaction->commit();

			// Trigger event to let plugins save their own data from POST
			Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
				'loModel' => $loModel
			)));

			$data = array(
                'resourceId'     => (int) $videoObj->id_video,
				'organizationId' => (int)$loModel->idOrg,
				'parentId'       => (int)$parentId,
				'title'          => $title,
				'message'        => Yii::t('standard', '_OPERATION_SUCCESSFUL'),
			);
			$response->setData($data)->setStatus(true);

		} catch (CException $e) {
		    Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			$transaction->rollback();
			$response->setMessage($e->getMessage())->setStatus(false);
		}

		echo $response->toJSON();
		Yii::app()->end();
	}


	/**
	 * Display the so called "launch pad screen", where user is offered to "start playing" the LO
	 *
	 */
	public function actionAxLaunchpad() {
		// Prepare Ajax response class
		$response = new AjaxResult();

		$idObject = (int)Yii::app()->request->getParam('id_object', 0);
		$loModel = LearningOrganization::model()->findByPk($idObject);
		if (!$loModel || $loModel->objectType != LearningOrganization::OBJECT_TYPE_VIDEO || ($loModel->idCourse != $this->getIdCourse())) {
			echo $response->setStatus(false)->setMessage('Invalid Video Object')->toJSON();
			Yii::app()->end();
		}
		$course = LearningCourse::model()->findByPk($this->getIdCourse());

		// Get Video model
		$videoModel = LearningVideo::model()->findByPk($loModel->idResource);
		if (!$videoModel) {
			echo $response->setStatus(false)->setMessage('Invalid Video Object')->toJSON();
			Yii::app()->end();
		}

		$videoFiles = CJSON::decode($videoModel->video_formats);
		if (empty($videoFiles)) {
			$response->setStatus(false)->setMessage('No valid video file stored')->toJSON();
			Yii::app()->end();
		}

		// Adjust the object reference (in case this is a central repo LO)
		$idUser = Yii::app()->user->id;
		$masterLo = $loModel->getMasterForCentralLo($idUser);

		$video_track = LearningVideoTrack::model()->findByAttributes(array('idReference' => $masterLo->idOrg, 'idUser' => $idUser));

		$videoTrackBookmark = $video_track->bookmark;
		$completed = $video_track->status == LearningCommontrack::STATUS_COMPLETED;

		if($videoModel->type === LearningVideo::VIDEO_TYPE_UPLOAD){
			$videoFileNames = array_values($videoFiles);

			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_VIDEO);

			if (isset($videoFiles['hls_id']) ) {
				$mediaUrl = '';
				$mediaUrlMp4 = $storage->fileUrl($videoFileNames[0], $videoFiles['hls_id']);
				$mediaUrlHlsPlaylist = $storage->fileUrl($videoFiles['hls_id']. ".m3u8", $videoFiles['hls_id']);
			} else {
				$mediaUrl = $storage->fileUrl($videoFileNames[0]);
				$mediaUrlMp4 = $storage->fileUrl($videoFileNames[0]);
				$mediaUrlHlsPlaylist = null;
			}

			$videoSubtitles = LearningVideoSubtitles::model()->findAllByAttributes(array('id_video' => $loModel->idResource));
			$subs = array();
			foreach ($videoSubtitles as $videoSubtitle) {
				$subs[$videoSubtitle->lang_code]['url'] = CoreAsset::url($videoSubtitle->asset_id);
				if ($videoSubtitle->fallback == 1) {
					$subs[$videoSubtitle->lang_code]['fallback']=true;
				}
				else{
					$subs[$videoSubtitle->lang_code]['fallback']=false;
				}
			}
		} else{
			$cs = Yii::app()->getClientScript();
			/**
			 * @var $cs CClientScript
			 */
			$assetsUrl = $this->getModule()->getAssetsUrl();
			$cs->registerScriptFile($assetsUrl.'/js/embedPlayer.js');
			$cs->registerCssFile($assetsUrl . '/css/playerStyle.css');
		}

		$rs = $this->_track($idUser, $masterLo->idOrg, $masterLo->idResource, LearningCommontrack::STATUS_AB_INITIO);
		if (!$rs) {
			echo $response->setStatus(false)->setMessage('Error while tracking object')->toJSON();
			Yii::app()->end();
		}

		$html = $this->renderPartial("_launchpad", array(
			'videoModel' => $videoModel,
			'mediaUrlMp4' => $mediaUrlMp4,
			'mediaUrlHlsPlaylist' => $mediaUrlHlsPlaylist,
			'mediaUrl' => $mediaUrl,
			'idReference' => $idObject,
			'bookmark' => $videoTrackBookmark,
			'subs' => $subs,
			'autoplay' => $course->resume_autoplay,
			'completed' => $completed
		), true, true);


		// Send HTML back using res.data.html!  res.html will send data through ajaxFilter
		// Which ruins jPlayer (somehow)!
		$data = array("html" => $html);
		$response->setStatus(true)->setData($data)->toJSON();

		Yii::app()->end();

	}


	/**
	 * Handle tracking operations
	 * @throws CException
	 */
	public function actionAxTrack() {

		//prepare input data
		$idUser = Yii::app()->user->id;
		$idReference = (int)Yii::app()->request->getParam('id_reference', 0);
		$status = Yii::app()->request->getParam('status', LearningCommontrack::STATUS_ATTEMPTED);
        $bookmark = Yii::app()->request->getParam('bookmark', 0);
		//prepare output variable and start DB transaction
		$response = new AjaxResult();
		$transaction = Yii::app()->db->beginTransaction();

		try {

			//validate input parameters
			if ($idUser <= 0 || $idReference <= 0 || !$status) {
				throw new CException("Invalid input data");
			}

			//retrieve informations about the object
			$organization = LearningOrganization::model()->findByPk((int)$idReference);
			if (!$organization || $organization->objectType != LearningOrganization::OBJECT_TYPE_VIDEO) {
				throw new CException("Invalid video object (" . $idReference . ")");
			}

			// Adjust the object reference (in case this is a central repo LO)
			$organization = $organization->getMasterForCentralLo($idUser);
			$idReference = $organization->idOrg;

			//do effective tracking
			$rs = $this->_track($idUser, $idReference, $organization->idResource, $status, $bookmark);
			if (!$rs) {
				throw new CException("Error while saving track data");
			}

			$transaction->commit();
			$response->setStatus(true);

		} catch (CException $e) {

			$transaction->rollback();
			$response->setMessage($e->getMessage())->setStatus(false);
		}

		$response->toJSON();
		Yii::app()->end();
	}


	/**
	 * Register Controller specific assets
	 *
	 * (non-PHPdoc)
	 * @see VideoBaseController::registerResources()
	 */
	public function registerResources() {
		parent::registerResources();

		if (!Yii::app()->request->isAjaxRequest) {
		}

	}

	public function actionKeepAlive()
	{
		// extend the session
		Yii::app()->user->getIsGuest();

		$data = array(
			'success' => true
		);

		header('Content-type: text/javascript');
		if (isset($_GET['jsoncallback'])) {
			// JSONP callback
			$jsoncallback = preg_replace('/[^a-zA-Z\._]+/', '', $_GET['jsoncallback']);
			echo $jsoncallback. '(' . CJSON::encode($data) . ')';
		} else {
			// standard Json
			echo CJSON::encode($data);
		}

		// Update course+user session tracking to register user's course activity (time in course)
		if (isset($_REQUEST['course_id']))
			ScormApiBase::trackCourseSession((int)Yii::app()->user->idst, $_REQUEST['course_id']);

		Yii::app()->end();
	}
}