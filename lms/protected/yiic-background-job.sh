#!/bin/sh
# ----------------------------------------------------------------------------------
#  Yii command starter.
#  Differs from the default (yiic) in that it defines an
#  environment variable equal to the FIRST script parameter that is
#  used by the invoked PHP script to load per-domain configuration.
#  Final result is: we can run Yii commands based on domain name and configuration.
#
#  Example call:
#       yiic-background-job.sh my.lms.com job runBackground --hash_id=123456qweryt
#
#  Don't forget to change the file and make it executable:
#   chmod ugo+x yiic-background-job.sh
# ----------------------------------------------------------------------------------

# Set/export the following environment variable to the domain name (first parameter)
export LMS_DOMAIN=$1

domain_code=`echo $1 | sed -e 's/\W/_/g'`
split_domain_code=$(echo $domain_code|cut -c -1)/$(echo $domain_code|cut -c -2|cut -c 2-)/$domain_code


# Use the rest parameters to make the call to PHP script that will actually run the command (up to 9)
echo ">>> Starting Background Job: $1 : $2"
echo "  Domain code: $domain_code"
echo "  Split Domain code: $split_domain_code"

iteration=0
max_iterations=10000

while :
do
    iteration=$((iteration+1))
    #echo "  Iteration: $iteration"
    php yiic-domain.php job runBackground --hash_id=$2 $3 $4 $5 $6
    command_res=$?
    #command_res=1

	#echo "Job Handler exits with code: $command_res"

    # Exit code 1 or greater BUT NOT 100 (run again) => ERROR
    if [ $command_res -gt 0 ] && [ $command_res -ne 100 ]
    then
    	echo "==========================================================================================="
    	echo "ERROR: Job FAILED, please check LMS replica log files! Exiting shell script (exit code 1)"
    	echo "==========================================================================================="
    	break
    fi

    # Exit code 0 = job is done
    if [ $command_res -eq 0 ]
    then
        echo "Job done at iteration $iteration"
        break
    fi

    if [ $iteration -ge $max_iterations ]  
    then
        echo "  Max iterations number reached ($max_iterations). STOP the job!"
        php yiic-domain.php job stopBackground --hash_id=$2 --message="Job $2 stopped, max iterations reached" $3 $4 $5 $6
        break
    fi

done

echo "<<< Ending Background Job: $1 : $2"
 
