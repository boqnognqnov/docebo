<style>
	.row-fluid {
		color:#000000;
	}
</style>

<div class="container" style='width:600px'>
<?php if (Yii::app()->user->hasFlash('error')) : ?>

<div class='row-fluid'>
    <div class="span12 alert alert-error">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
</div>
<?php endif; ?>

<?php if (!isset($result)) : ?>

	<?php
		echo CHtml::beginForm(Yii::app()->createUrl('user/recoverPwd'), 'POST', array('class' => 'ajax form-horizontal', 'id' => 'lostpwd-form'));
	?>

	<div class="row-fluid">
		<div class="span12">
			<p>
				<?=$message ?>
			</p>
		</div>
	</div>

	<div class="row-fluid">
		<div class="control-group">
				<label class="span3 control-label" for="pwd"><?=Yii::t('register','_NEW_PASSWORD'); ?> </label>
				<div class="span9 controls">
					<input class='span12' type='password' id="pwd" name='pwd'  maxlength='255'>
				</div>
		</div>
		<div class="control-group">
				<label class="span3 control-label" for="re_pwd"><?=Yii::t('register','_RETYPE_PASSWORD');?> </label>
				<div class="span9 controls">
					<input class='span12' type='password' id="re_pwd" name='re_pwd'  maxlength='255'>
					<input type='hidden' id="code" name="code" value="<?=$code?>">
				</div>
		</div>
		<div class="control-group">
			<div class="span12 controls">
					<input class="span4 pull-right btn btn-success" type="submit" name="send" value="<?=Yii::t('standard','_SAVE'); ?>">
			</div>
		</div>


		<?php echo CHtml::endForm(); ?>
	</div>
<?php else: ?>
	<div class='row-fluid'>
		<div class="span12 alert alert-success">
			<?=$result?>
		</div>

	</div>
<?php endif; ?>
</div>