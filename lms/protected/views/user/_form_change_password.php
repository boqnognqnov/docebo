<? if($error): ?>
	<div class="alert">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<?=$error?>
	</div>
<? endif; ?>

<h2><?=Yii::t('profile', '_CHANGEPASSWORD')?></h2>

<p>&nbsp</p>

<?=DoceboForm::beginForm('', 'POST', array('class'=>'form-horizontal'));?>

<div class="control-group">
	<label class="control-label" for="old_pwd"><?=Yii::t('register', '_OLD_PWD')?></label>

	<div class="controls">
		<?=DoceboForm::passwordField('old_pwd');?>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="new_pwd"><?=Yii::t('register', '_NEW_PASSWORD')?></label>

	<div class="controls">
		<?=DoceboForm::passwordField('new_pwd');?>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="new_pwd_confirm"><?=Yii::t('register', '_RETYPE_PASSWORD')?></label>

	<div class="controls">
		<?=DoceboForm::passwordField('new_pwd_confirm');?>
	</div>
</div>

<div class="control-group">
	<div class="controls">
		<button type="submit" class="btn btn-success"><?=Yii::t('standard', '_SAVE');?></button>
	</div>
</div>

<?=DoceboForm::endForm();?>