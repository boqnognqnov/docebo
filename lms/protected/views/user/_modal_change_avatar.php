<?php /* @var $uniqueId string */ ?>

<style type="text/css">

.modal-change-avatar {}
.modal-change-avatar .modal-body {
	min-height: 240px;
}
.modal-change-avatar .upload-form-wrapper {
	border-top: 1px solid #e4e6e5;
	border-bottom: 1px solid #e4e6e5;
}
.modal-change-avatar .upload-form {
	border-bottom: 1px solid #ffffff;
	border-top: 1px solid #ffffff;
	background: #f1f3f2;
	padding: 7px 17px;
}

</style>

<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'change-avatar-form',
	'method' => 'post'
));
?>

	<input type="hidden" name="ChangeAvatarForm[avatar]" id="ChangeAvatarForm_avatar" value="" />


	<div class="upload-form-wrapper">
		<div class="upload-form clearfix">
			<a href="#" id="pickavatar_<?= $uniqueId ?>" class="btn-docebo green big pull-right"><?= Yii::t('standard', '_UPLOAD') ?></a>
			<?= CHtml::label(Yii::t('menu_over', '_CHANGE_AVATAR'), 'upload-btn'); ?>
		</div>
	</div>

	<br>

	<div class="row-fluid">
		<div class="span4">
		</div>
		<div class="avatar-container span4">
			<?= Yii::app()->user->getAvatar('img-rounded') ?>
		</div>
		<div class="span4">
		</div>
		
	</div>

	

	<div class="form-actions">
		<input type="submit" class="btn-docebo green big" value="<?= Yii::t('standard', '_SAVE') ?>"/>
		<input type="reset" class="btn-docebo black big close-dialog" value="<?= Yii::t('standard', '_CANCEL') ?>"/>
	</div>

<?php $this->endWidget(); ?>


<script type="text/javascript">
    (function(){
        var pluploader = null;

        function initUploader() {
            pluploader = new plupload.Uploader({
				chunk_size: '1mb',
                unique_names: true,
                runtimes: 'html5,flash',
                browse_button: 'pickavatar_<?= $uniqueId ?>',
                max_file_size: '2mb',
                url: '<?= Docebo::createAppUrl('lms:site/axUploadFile') ?>',
                flash_swf_url: '<?= Yii::app()->plupload->getAssetsUrl() . '/plupload.flash.swf' ?>',
                multipart_params: { '<?php echo Yii::app()->request->csrfTokenName; ?>': '<?= Yii::app()->request->csrfToken ?>' },
                filters: [
                    {title : "Image files", extensions : "jpg,jpeg,gif,png"}
                ]
            });

            pluploader.init();

            pluploader.bind('FilesAdded', function(up, files) {

                $('#ChangeAvatarForm_avatar').val('');
                $('#pickavatar_<?= $uniqueId ?>').addClass('disabled');

                pluploader.start();
                up.refresh(); // Reposition Flash/Silverlight
            });

            pluploader.bind('Error', function(up, err) {
                up.refresh(); // Reposition Flash/Silverlight
            });

            pluploader.bind('FileUploaded', function(up, file) {

                $('#pickavatar_<?= $uniqueId ?>').removeClass('disabled');

                // If pluploader is set to unique_names=true, the physical file is in file.target_name
				var fileName = file.target_name ? file.target_name : file.name;
                
				var imgSrc = '<?= Docebo::createAbsoluteLmsUrl('stream/image') ?>' + '&fn=' + fileName + '&col=uploads';
                
                $('.avatar-container').html('<img class="img-rounded" src="' + imgSrc + '"/>');
                $('#ChangeAvatarForm_avatar').val(fileName);
            });
        }

        $(document).delegate('.modal-change-avatar', 'dialog2.closed', function() {
            if (pluploader) {
                pluploader.stop();
                pluploader.unbindAll();
                pluploader = null;
            }
        });

        $(document).delegate('.modal-change-avatar', 'dialog2.content-update', function() {

            var $modal = $(this).find('.modal-body');

            $('#change-avatar-form').unbind('submit').bind('submit', function(e) {
                e.preventDefault();
                var that = $(this);
                $.post(that.attr('action'), that.formSerialize(), function(response) {
                    $modal.find('.modal-body').dialog2('close');
                    window.location.reload();
                });
            });

            initUploader();
        });
    })();
</script>