<?php if (Yii::app()->user->hasFlash('error')) : ?>
<div class='row-fluid'>
    <div class="span12 alert alert-error">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
</div>
<?php else: ?>
<div class='row-fluid'>
    <div class="span12 alert alert-success">
        <?=$message ?>
    </div>
</div>
<?php endif; ?>


<script type="text/javascript">
<!--

setTimeout(function() {
    window.location.href = '<?=Yii::app()->createUrl('site/index');?>';
}, 5000);


//-->
</script>
