<? if(isset($error) && !empty($error)): ?>
	<div class="alert">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<?=Yii::t('standard', 'Please fill all required fields')?>
	</div>
<? endif; ?>
<?php $msg = Yii::app()->user->getFlash('email_status_change');

if(!empty($msg)) {
	if ($msg == Yii::t('standard', 'You have confirmed your email!')) $class = 'success';
	elseif ($msg == Yii::t('standard', 'Invalid or expired email verification link!')) $class = 'danger';
	else $class = 'info';
	?>
	<div class="alert alert-<?=$class?>" style="text-align: center">
		<?=$msg;?>
	</div>
<?php } ?>

<?=Yii::app()->user->getFlash('email_verification_reminder');?>

<h2><?=Yii::t('menu', '_FIELD_MANAGER')?></h2>

<p>&nbsp</p>

<?=DoceboForm::beginForm('', 'POST', array(
    'class'=>'form-horizontal',
    'enctype' => 'multipart/form-data',
));?>

<? foreach($defaultFields as $defaultField) { ?>
    <div class="control-group">
        <?= CHtml::activeLabel($user, $defaultField, array('class' => 'control-label')); ?>
        <div class="controls">
            <?= CHtml::activeTextField($user, $defaultField); ?>
        </div>
    </div>
<? } ?>

<? foreach($fields as $field): ?>
	<?=Field::getFieldHTML($field, '*', null, true);?>

	<? if($field->getFieldType() == 'date') {
		Yii::app()->clientScript->registerScript(
			"script_" . $field->getFieldType() . '_' . $field->getFieldId(),
			"$('#field_" . $field->getFieldType() . '_' . $field->getFieldId() . "').attr('readonly', true);
        $('#field_" . $field->getFieldType() . '_' . $field->getFieldId() . "').parent().bdatepicker();
        $('#field_" . $field->getFieldType() . '_' . $field->getFieldId() . "').css('width','179px')");
	} ?>
<? endforeach; ?>


<div class="control-group">
	<div class="controls">
		<button type="submit" class="btn btn-success"><?=Yii::t('standard', '_SAVE');?></button>
	</div>
</div>

<?=DoceboForm::endForm();?>