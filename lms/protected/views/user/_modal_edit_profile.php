<?php

/* @var $form CActiveForm */
/* @var $settingTimezone CoreSettingUser */
/* @var $settingDateformat CoreSettingUser */
/* @var $settingDatelocale CoreSettingUser */


Yii::app()->bootstrap->registerAssetCss('bootstrap-datepicker.css');


$form = $this->beginWidget('CActiveForm', array(
	'id' => 'edit-profile-form',
	'htmlOptions' => array(
		'class' => 'ajax'
	),
)); ?>

	<?php /* echo $form->errorSummary($user); */ ?>

	<?php if ($editOnlyPassword) : ?>

		<div class="clearfix line-margin">
			<?php echo $form->error($user, 'new_password'); ?>
			<div class="clearfix">
				<?php echo $form->passwordField($user, 'new_password'); ?>
				<?php echo $form->labelEx($user, 'new_password'); ?>
			</div>
		</div>

		<div class="clearfix line-margin">
			<?php echo $form->error($user, 'new_password_repeat'); ?>
			<div class="clearfix">
				<?php echo $form->passwordField($user, 'new_password_repeat'); ?>
				<?php echo $form->labelEx($user, 'new_password_repeat'); ?>
			</div>
		</div>

	<?php else: ?>

		<div class="clearfix line-margin">
			<?php echo $form->error($user, 'firstname'); ?>
			<div class="clearfix">
				<?php echo $form->textField($user, 'firstname'); ?>
				<?php echo $form->labelEx($user, 'firstname'); ?>
			</div>
		</div>

		<div class="clearfix line-margin">
			<?php echo $form->error($user, 'lastname'); ?>
			<div class="clearfix">
				<?php echo $form->textField($user, 'lastname'); ?>
				<?php echo $form->labelEx($user, 'lastname'); ?>
			</div>
		</div>

		<div class="clearfix line-margin">
			<?php echo $form->error($user, 'email'); ?>
			<div class="clearfix">
				<?php echo $form->textField($user, 'email'); ?>
				<?php echo $form->labelEx($user, 'email'); ?>
			</div>
		</div>

		<div class="clearfix line-margin">
			<?php echo $form->error($user, 'new_password'); ?>
			<div class="clearfix">
				<?php echo $form->passwordField($user, 'new_password'); ?>
				<?php echo $form->labelEx($user, 'new_password'); ?>
			</div>
		</div>

		<div class="clearfix line-margin">
			<?php echo $form->error($user, 'new_password_repeat'); ?>
			<div class="clearfix">
				<?php echo $form->passwordField($user, 'new_password_repeat'); ?>
				<?php echo $form->labelEx($user, 'new_password_repeat'); ?>
			</div>
		</div>

		<div class="clearfix line-margin">
			<?php echo $form->error($settingUser, 'value'); ?>
			<div class="clearfix">
				<?php echo $form->dropDownList($settingUser, 'value', CoreLangLanguage::getActiveLanguages(), array(
                    'name' => 'CoreSettingUser[ui.language]'
                )); ?>
				<?php echo $form->labelEx($user, 'language.value'); ?>
			</div>
		</div>

        <?php if ($settingTimezone) : ?>
		<div class="clearfix line-margin">
			<?php echo $form->error($settingTimezone, 'value'); ?>
			<div class="clearfix">
				<?php echo $form->dropDownList($settingTimezone, 'value', Yii::app()->localtime->getTimezonesArray(), array(
                    'name' => 'CoreSettingUser[timezone]',
                    'prompt' => Yii::t('configuration', 'Select a time zone...')
                )); ?>
				<?php echo $form->labelEx($user, 'timezone.value'); ?>
			</div>
		</div>
        <?php endif; ?>

        <?php if ($settingDatelocale) : ?>
		<div class="clearfix line-margin">
			<?php echo $form->error($settingDatelocale, 'value'); ?>
			<div class="clearfix">
				<?php echo $form->dropDownList($settingDatelocale, 'value', CHtml::listData(Lang::getLanguages(), 'browsercode', function($lang) { return CHtml::encode($lang['description']); }), array(
                    'name' => 'CoreSettingUser[date_format_locale]'
                )); ?>
				<?php echo $form->labelEx($user, 'date_format_locale.value'); ?>
			</div>
		</div>
        <?php endif; ?>

        <?php if ($settingDateformat) : ?>
		<div class="clearfix line-margin">
			<?php echo $form->error($settingDateformat, 'value'); ?>
			<div class="clearfix">
				<?php echo $form->dropDownList($settingDateformat, 'value', Yii::app()->localtime->getDateTimeFormatsArray(), array(
                    'name' => 'CoreSettingUser[date_format]',
                    'prompt' => Yii::t('configuration', 'Select a date format...')
                )); ?>
				<?php echo $form->labelEx($user, 'date_format.value'); ?>
			</div>
		</div>
        <?php endif; ?>

		<?php if (!empty($additionalFields)) { ?>
			<div class="additional-fields">
				<?php foreach ($additionalFields as $field) { ?>
					<div class="clearfix line-margin">
						<?php /* @var $field CoreField */ ?>
						<?php echo $field->renderField(true); ?>
					</div>
				<?php } ?>
			</div>
		<?php } ?>

	<?php endif; ?>


	<div class="form-actions">
		<?php Yii::app()->event->raise('RenderEditProfileButtons', new DEvent($this, array('user' => $user))); ?>
		<input class="btn-docebo green big" type="submit" value="<?= Yii::t('standard', '_SAVE') ?>" />
		<input class="btn-docebo black big close-dialog" type="reset" value="<?= Yii::t('standard', '_CANCEL') ?>"/>
	</div>
<style type="text/css">

	.modal-edit-profile .form-actions{
		display: none;
	}

	.modal {
		top: 5%;
	}

	.modal-edit-profile.modal {
		width: 520px;
		margin-left: -260px;
	}

	.modal-edit-profile .modal-body{
		overflow-y: auto;
		max-height: 550px;
		padding: 21px 18px;
	}
	.modal-edit-profile div label {
		margin-right: 20px;
		margin-top: 8px;
	}

	#edit-profile-form { }
	#edit-profile-form .errorMessage {
		padding-bottom: 2px;
		color: #b54b4b;
		font-weight: 700;
		text-align: right;
	}
	.modal-edit-profile #edit-profile-form input.error {
		background: #f2dedf;
		color: #b54b4b;
	}
	#edit-profile-form .line-margin{
		margin-bottom: 15px;
	}
	#edit-profile-form label,
	#edit-profile-form input,
	#edit-profile-form .jClever-element,
	#edit-profile-form .additional-yes-no,
	#edit-profile-form span.jqselect,
	#edit-profile-form select,
	#edit-profile-form textarea,
	#edit-profile-form .additional-yes-no span.jq-radio,
	#edit-profile-form span.jq-file{
		float: right;
	}
	#edit-profile-form label{
		width:145px;
		word-wrap: break-word;
		}
	#edit-profile-form input[type="text"],
	#edit-profile-form input[type="text"]:focus,
	#edit-profile-form input[type="textfield"],
	#edit-profile-form input[type="password"],
	#edit-profile-form textarea,
	#edit-profile-form select,
	#edit-profile-form span.jqselect .jq-selectbox__select {
		line-height: 30px;
		border-left: 1px solid #999999;
		border-top: 1px solid #999999;
		border-bottom: 1px solid #e5e5e5;
		border-right: 1px solid #e5e5e5;
		background: #f9f9f9;
		color: #333333;
		width: 284px;
		height: 30px;
		outline: none !important;
		box-shadow: none !important;
		transition: none !important;
		-webkit-border-radius: 0;
		-moz-border-radius: 0;
		border-radius: 0;
	}
	#edit-profile-form select {
		width: 298px;
		height: 32px;
		margin: 0;
	}
    #edit-profile-form .input-append.date {
        float: right;
        position: relative;
    }
    #edit-profile-form .input-append.date input {
        padding-left: 28px;
        width: 265px;
    }
    #edit-profile-form .additional-yes-no {
        width: 298px;
        display: table;
        padding-top: 5px;
    }
    #edit-profile-form .additional-yes-no input {
        margin-top: 8px;
        float: left;
    }
    #edit-profile-form .additional-yes-no label {
        float: left;
        width: 110px;
        padding-left: 5px;
        margin-top: 3px;
    }
    #edit-profile-form .input-append.date .add-on {
        position: absolute;
        left:7px;
        top:7px;
        background: none;
        border:none;
        border-radius:0;
        z-index: 2;
    }
    #edit-profile-form input[type="file"] {
        width: 298px;
    }
</style>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(document).delegate(".modal", "dialog2.content-update", function() {

		var e = $(this).find('.modal-body');
		var autoclose = e.find('.auto-close');

		if (autoclose.length > 0) {
			e.dialog2('close');
			window.location.reload();
		}

		e.find('#<?= CHtml::activeId($settingDatelocale, 'date_format_locale') ?>').change(function () {
			var url = '<?= $this->createUrl('getCustomDateFormats') ?>';
			$.get(url, {code: $(this).val()}, function (response) {
				if (response.success === true) {
					var html = response.html.replace(/\u200f/g, '');
					e.find('#<?= CHtml::activeId($settingDatelocale, 'date_format') ?>').replaceWith(html);
				}
			}, 'json')
		}).trigger('change');
	});

	$(document).ready(function () {
		try {
			$('.modal .input-append.date input').bdatepicker();
		} catch (e) {
		}

		if ($.isFunction("bdatepicker") == false) {
			$("head").append("<link rel='stylesheet' type='text/css' href='<?php echo Yii::app()->bootstrap->getAssetsUrl().'/css/bootstrap-datepicker.css' ?>'>");

			$.getScript("<?php echo Yii::app()->bootstrap->getAssetsUrl().'/js/bootstrap.datepicker.js' ?>", function () {

				<?php
				$localeFile = DatePickerHelper::getLocaleFileName(); //localization file name for current language
				if (!empty($localeFile)):
				?>
				// Now load the localization file
				$.getScript("<?php echo Yii::app()->bootstrap->getAssetsUrl().'/js/locales/'.$localeFile ?>", function () {
					$('.modal .input-append.date input').bdatepicker();
				});
				<?php else: ?>
				$('.modal .input-append.date input').bdatepicker();
				<?php endif; ?>

			});

		} else {
			$('.modal .input-append.date input').bdatepicker();
		}
	});


</script>