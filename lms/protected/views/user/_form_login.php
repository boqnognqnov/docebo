<!DOCTYPE html>
<html>
<head>

<!-- This form should be the only bootstrap modal (dialog2) at the time it is called !!!  -->
<style type="text/css">
</style>

</head>
<body>


<h1 style="display: none;"><?php  echo CHtml::image(Yii::app()->theme->baseUrl . '/images/keys_blue.png'); ?> <?=Yii::t('login','Log in'); ?></h1>

<div class=''>

<div class='row-fluid'>
    <h4 class='message'><?= $message ?></h4>
</div>

<?php
    echo CHtml::beginForm(Yii::app()->createUrl('user/axLogin'), 'POST', array('class' => 'ajax form-horizontal', 'id' => 'login-form'));

    echo CHtml::hiddenField('redirect_url', $redirect_url);
    echo CHtml::hiddenField('reason', $reason);

?>

<?php if (Yii::app()->user->hasFlash('error')) : ?>
<div class='row-fluid'>
    <div class="span12 alert alert-error">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
</div>
<?php endif; ?>

<?php if (Yii::app()->event->raise('BeforeLoginFormRender', new DEvent($this, array(
	'redirect_url'=>$redirect_url,
	'reason'=>$reason,
)))) : ?>
<div class='row-fluid'>

    <div class="control-group">
        <label class="control-label" for="username"><?=Yii::t('standard','_USERNAME'); ?> </label>
        <div class="controls">
            <input type='text' id="username" name='username' placeholder='<?=Yii::t('standard','Username/Email'); ?>' maxlength='255'>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="password"><?=Yii::t('standard','_PASSWORD'); ?> </label>
        <div class="controls">
            <input type="password" id="password" name="password" placeholder="<?=Yii::t('standard','_PASSWORD'); ?>" maxlength="255" autocomplete="off">
        </div>
    </div>


    <div class="control-group">
        <label class="control-label" for="login_button"><a class="open-dialog" rel="dialog-lostdata" data-dialog-class="lostdata" href="<?=Yii::app()->createUrl('user/axLostdata');?>"><?php echo Yii::t('login', '_LOG_LOSTPWD'); ?>?</a></label>
        <div class="controls">
          <input class="btn btn-success pull-right" style='margin-right:10px;' type="submit" name="login_button" value="<?= Yii::t('login', '_LOGIN') ?>">
        </div>
    </div>

</div>
    <hr>
    <!--SOCIAL NETWORK LOGIN LINKS-->
    <div class='row-fluid clearfix'>

        <div class="pull-left">
            <br>
            <span><?= Yii::t('login', 'or sign in with:') ?></span>
        </div>

        <div class="pull-right">
            <div>
                <? Yii::app()->event->raise('ExternalCatalogAfterLoginForm', new DEvent($this, array())); ?>
            </div>

            <?php Social::displaySocialLoginIcons(); ?>
            <?php if (PluginManager::isPluginActive('GoogleappsApp')): ?>
                <div class="pull-left">
                    <a title="Google Apps" class="header-lostpass"
                       href="<?= Docebo::createLmsUrl('GoogleappsApp/GoogleappsApp/gappsLogin') ?>">
                        <? /*=Yii::t('login','_LOGIN_WITH') */ ?><!-- &nbsp;	--><?= CHtml::image(Yii::app()->theme->baseUrl . "/images/loginexternal/googleapps-24.png") ?>
                    </a>&nbsp;&nbsp;&nbsp;
                </div>
            <?php endif; ?>
            <?php if (PluginManager::isPluginActive('GooglessoApp') && Docebo::isCurrentLmsDomain()): ?>
                <div class="pull-left">
                    <a title="Google Login" class="header-lostpass"
                       href="<?= Docebo::createLmsUrl('GooglessoApp/GooglessoApp/login') ?>">
                        <? /*=Yii::t('login','_LOGIN_WITH') */ ?><!-- &nbsp;	--><?= CHtml::image(Yii::app()->theme->baseUrl . "/images/loginexternal/google-24.png") ?>
                    </a>&nbsp;&nbsp;&nbsp;
                </div>
            <?php endif; ?>
        </div>

    </div>
    <!--SOCIAL NETWORK LOGIN LINKS-->
    <hr>

<?php $toRegisterUrl = $this->createUrl('user/axRegister', array(
	'redirect_url' => $redirect_url,
	'courseId' => $courseId
)); ?>


<?php if($show_register_link): ?>
	<div class='modal-footer'>
		<div class='row-fluid'>
			<span class='span2' style='padding-top: 3px;'><?php echo CHtml::image(Yii::app()->theme->baseUrl .'/images/silhouette_plus.png') ?></span>
			<span class='span10' style='text-align: left;'>
			<strong><?= Yii::t("standard", "_NEW_USER") ?>?</strong><br>
			<?= Yii::t("user", "If you don't have an account, please") . " " ?>
			<!-- <a class="ajax" href='<?=$toRegisterUrl?>'><?= Yii::t("user", "Register here") ?></a> -->
			<a class="open-dialog close-current" closeOnEscape="false" data-dialog-class="register" rel="dialog-register" href="<?=$toRegisterUrl?>"><?= Yii::t("user", "Register here") ?></a>
			</span>
		</div>
	</div>
<?php endif; ?>

<?php endif; ?>

<?php echo CHtml::endForm(); ?>

</div>

<!-- --- JS ------------------------------------------------------------------------------------------------------ -->


<script type="text/javascript">
/*<![CDATA[*/

$(function(){
    // Add class to make styling real (base.css). Very important!!!
    setTimeout(function(){
        $(".modal").addClass("login");
    },50);

    $(document).controls();

	$('.close-current').click(function(){
		$(this).closest('.modal-body').dialog2('close');
	});
});

/*]]>*/
</script>


</body>
</html>