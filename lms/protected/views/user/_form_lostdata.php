<h1><?=Yii::t('login','_LOG_LOSTPWD'); ?></h1>



<br />
<?php
if ($error === false && Yii::app()->user->hasFlash('error'))
    $error = Yii::app()->user->getFlash('error');
?>
<?php if ($error !== false) : ?>
<div class='row-fluid'>
    <div class="span12 alert alert-error">
        <?php echo $error; ?>
    </div>
</div>
<?php endif; ?>
<div class='row-fluid'>
	<div class="span1">
		<?= CHtml::image(Yii::app()->theme->baseUrl .'/images/lost_password.png') ?>
	</div>
	<div class="span11">
		<h4><?=Yii::t('register','_LOST_TITLE_PWD'); ?></h4>
		<p><?=Yii::t('register','_LOST_INSTRUCTION_PWD'); ?></p>
	</div>
</div>
<?php
    echo CHtml::beginForm(Yii::app()->createUrl('user/axLostdata'), 'POST', array('class' => 'ajax form-horizontal', 'id' => 'lostpwd-form'));
?>
<div class='row-fluid'>
	<div class="span10">
		<input type='text' id="username" class="input-xlarge" name='usernameOrEmail' placeholder='<?=Yii::t('register','_REG_USERID_DEF');?>' maxlength='255'>
	</div>
	<div class="span2">
	<input class="pull-right btn btn-docebo green big" type="submit" name="lostpwd_button" value="<?=Yii::t('standard', '_SEND');?>">
	</div>
</div>

<?php echo CHtml::endForm(); ?>

