<?php

    // Be sure we do not use some already used DOM id.
    $form_id = uniqid("redirect");

	Yii::app()->event->raise('AfterUserAxLoginSuccessRedirectUrl', new DEvent($this, array('url' => &$redirect_url)));

    echo CHtml::beginForm($redirect_url, "GET", array('id' => $form_id));
    echo CHtml::endForm();

?>
<script type="text/javascript">
/*<![CDATA[*/

    // First hide all modals to avoid the "flicker"-style effect of showing a dummy modal just before redirect
    // Very annoying. Remove this line to see.
    $('.modal').hide();

    // Redirect
    document.getElementById('<?= $form_id ?>').submit();

/*]]>*/
</script>

