<?php

// List of coutries for dropdown list
$countriesListData = CHtml::listData($countries, 'iso_code_country', 'name_country');

// Add an 'empty' value first array element
$countriesListData = array_merge(array('' => Yii::t('standard', '_COUNTRY')), $countriesListData);


?>
<!DOCTYPE html>
<html>
<head>

<style type="text/css">

    .modal {
        height: auto;
    }

    .modal .modal-body {
        padding-top: 2px;
    }

    .modal .control-group {
        margin-bottom: 10px;
    }

    #edit-bill-info-form input.co-input-red {
        border: 1px solid red;
    }

</style>

</head>
<body>

<h1><?= Yii::t('getmoreuser', 'Edit billing information') ?></h1>

<?php echo CHtml::beginForm(Yii::app()->createUrl('user/axEditBillingInfo'), 'POST', array('class' => 'ajax form-horizontal', 'id' => 'edit-bill-info-form')); ?>
<?php echo CHtml::hiddenField('idUser', $idUser); ?>


<div class="row-fluid">
    <div <?php if (!Yii::app()->user->hasFlash('error')):?>style="display:none;" <?php endif; ?> id="edit-form-feedback" class="span12 alert alert-error"><?php if (Yii::app()->user->hasFlash('error')) echo Yii::app()->user->getFlash('error'); ?></div>
</div>

<div class="row-fluid">

	<div class="clearfix"><br></div>

    <div class="control-group">
        <label class="span4 control-label" for="bill_company_name"><?=Yii::t('standard','Company name'); ?> </label>
        <div class="span8 controls">
            <input class="span12" type="text" id="bill-company-name" name="bill_company_name" value="<?= $binfo->bill_company_name ?>" maxlength="255">
        </div>
    </div>

    <div class="control-group">
        <label class="span4 control-label" for="bill_vat_number"><?=Yii::t('standard','VAT Number'); ?> </label>
        <div class="span8 controls">
            <input class="span12" type="text" id="bill-vat-number" name="bill_vat_number" value="<?= $binfo->bill_vat_number ?>" maxlength="255">
        </div>
    </div>

    <div class="control-group">
        <label class="span4 control-label" for="bill_address1"><?=Yii::t('standard','Address 1'); ?> </label>
        <div class="span8 controls">
            <input class="span12" type="text" id="bill-address1" name="bill_address1" value="<?= $binfo->bill_address1 ?>" maxlength="255">
        </div>
    </div>

    <div class="control-group">
        <label class="span4 control-label" for="bill_address2"><?=Yii::t('standard','Address 2'); ?> </label>
        <div class="span8 controls">
            <input class="span12" type="text" id="bill-address2" name="bill_address2" value="<?= $binfo->bill_address2 ?>" maxlength="255">
        </div>
    </div>


    <div class="control-group">
        <label class="span4 control-label" for="bill_city"><?=Yii::t('standard','City'); ?> </label>
        <div class="span8 controls">
            <input class="span12" type="text" id="bill-city" name="bill_city" value="<?= $binfo->bill_city ?>" maxlength="255">
        </div>
    </div>

    <div class="control-group">
        <label class="span4 control-label" for="bill_state"><?=Yii::t('standard','State/Province/Region'); ?> </label>
        <div class="span8 controls">
            <input class="span12" type="text" id="bill-state" name="bill_state" value="<?= $binfo->bill_state ?>" maxlength="255">
        </div>
    </div>

    <div class="control-group">
        <label class="span4 control-label" for="bill_zip"><?=Yii::t('standard','Zip/Postal code'); ?> </label>
        <div class="span8 controls">
            <input class="span12" type="text" id="bill-zip" name="bill_zip" value="<?= $binfo->bill_zip ?>" maxlength="255">
        </div>
    </div>

    <div class="control-group">
        <label class="span4 control-label" for="bill_country_code"><?=Yii::t('standard','_COUNTRY'); ?> </label>
        <div class="span8 controls">
            <?php
                echo CHtml::dropDownList('bill_country_code', $binfo->bill_country_code, $countriesListData, array('id' => 'bill-country-code'));
            ?>
        </div>
    </div>

    <div class="control-group">
        <div class="span12 controls">
          <input class="span4 pull-right btn btn-docebo green big" type="submit" name="bill_info_submit_button" id="bill_info_submit_button" value="<?= Yii::t('standard', '_SAVE') ?>">
        </div>
    </div>


</div>


<?php echo CHtml::endForm(); ?>

<!-- --- JS ------------------------------------------------------------------------------------------------------ -->

<script type="text/javascript">
/*<![CDATA[*/
    $( document ).ready(function() {
        $("#bill_info_submit_button").click(function(e){
            return validateEditBillingForm();
        });
        <?php if ($executeJSCheck): ?>
            validateEditBillingForm();
        <?php endif;?>
    });

    function validateEditBillingForm() {
        // Hide previous alerts if any
        $('#edit-form-feedback').hide();

        // Standard error message text
        $error_feedback = '<?=addslashes(Yii::t('cart','In order to continue, please provide the requested billing information')); ?>';

        var ok = true;

        // Billing info validation
        var elementsToValidate = ['address1', 'city'];
        $('#edit-bill-info-form label').css('color', '');
        for (var i=0; i < elementsToValidate.length; i++) {
            var el = $("#edit-bill-info-form #bill-" + elementsToValidate[i]);
            var tmpText = $.trim(el.val());
            if (tmpText.length <= 0) {
                ok = false;
                el.addClass("co-input-red");
            } else {
                el.removeClass("co-input-red");
            }
        }

        var el = $("#edit-bill-info-form #bill-country-code");
        tmpText = el.val();
        if (tmpText.length <= 0) {
            ok = false;
            el.addClass("co-input-red");
        }
        else {
            el.removeClass("co-input-red");
        }

        if (!ok) {
            $('#edit-form-feedback').text($error_feedback).show();
            return false;
        }

        return true;
    }
/*]]>*/
</script>


</body>
</html>