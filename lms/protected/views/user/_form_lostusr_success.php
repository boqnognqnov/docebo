<!DOCTYPE html>
<html>
<head>

<!-- This form should be the only bootstrap modal (dialog2) at the time it is called !!!  -->
<style type="text/css">


    .modal {
        width: 437px !important;
        height: auto;
        margin-left: -208px !important;
    }

    .modal .modal-body {
        padding-top: 2px;
    }

    .modal .control-group {
        margin-bottom: 10px;
    }

    .modal-footer {
        padding: 5px 0px 0px 0px;
    }

    .modal .control-group .control-label {
        text-align: left;
    }




</style>

</head>
<body>

<h1><?=Yii::t('register','_LOST_USERID_TITLE'); ?></h1>


<br />

<?php if (Yii::app()->user->hasFlash('error')) : ?>
<div class='row-fluid'>
    <div class="span12 alert alert-error">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
</div>
<?php endif; ?>


<div class='row-fluid'>
	<div class="span12">
		<?=$message ?>
	</div>
</div>

</body>
</html>