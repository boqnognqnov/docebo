<?php
/* @var $redirect_url string */

/* @var $form CActiveForm */
?>

<h1><?=Yii::t('register','New user?'); ?></h1>


<?php if (!$isOpening && Yii::app()->user->hasFlash('error')) : ?>
	<div class="register-error">
		<div class="row-fluid">
			<div class="span12 alert alert-error">
				<?php echo Yii::app()->user->getFlash('error'); ?>
			</div>
		</div>
	</div>
<?php endif; ?>


<div class="register-instructions">
	<p>
		<?=Yii::t('register','_REG_NOTE'); ?>
		<ul>
			<li><?= Yii::t('register','_REG_MANDATORY',array('[mandatory]' => '<span class="mandatory">*</span>')); ?></li>
			<?php if($options['pass_min_char']): ?>
				<li><?= Yii::t('register','_REG_PASS_MIN_CHAR',array('[min_char]' => Settings::get('pass_min_char'))); ?></li>
			<?php endif; ?>
		</ul>
	</p>
</div>


<?php

$form = $this->beginWidget('CActiveForm', array(
	'id' => 'register-form',
	'method' => 'POST',
	'action' => Yii::app()->createUrl('user/axRegister'),
	'htmlOptions' => array(
		'class' => 'ajax form-horizontal'
	)
));

echo CHtml::hiddenField('redirect_url', $redirect_url);
echo CHtml::hiddenField('courseId', $courseId);
echo CHtml::hiddenField('from_step', 'step_1', array('id' => 'from-step-input'));
// Set client timezone
echo CHtml::hiddenField('cl_tz_offset');

?>

	<div class="register-fields">
		<div class="internal">
			<div class="row-fluid">

                <!-- Tell the custom plugins that we are about to render registration from field -->
                <?php if(Yii::app()->event->raise('onBeforeRegisterFieldRender', new DEvent($this, array('field' => 'username', 'mandatory' => $mand_sym)))): ?>
				<div class="control-group">
					<label class="control-label" for="username"><?= Yii::t('standard', '_USERNAME'); ?>&nbsp;<?= $mand_sym ?> </label>

					<div class="controls">
						<?php
						echo CHtml::textField(
							'username',
							'',
							array('class' => 'pull-right', 'id' => 'username', 'maxlength' => 255, 'autocomplete' => 'off')
						);
						?>
					</div>
				</div>
                <?php endif; ?>

                <?php if(Yii::app()->event->raise('onBeforeRegisterFieldRender', new DEvent($this, array('field' => 'email', 'mandatory' => $mand_sym)))): ?>
				<div class="control-group">
					<label class="control-label" for="email"><?= Yii::t('standard', '_EMAIL'); ?>&nbsp;<?= $mand_sym ?></label>

					<div class="controls">
						<?php
						echo CHtml::textField(
							'email',
							'',
							array('class' => 'pull-right', 'id' => 'email', 'maxlength' => 255, 'autocomplete' => 'off')
						);
						?>
					</div>
				</div>
                <?php endif; ?>

				<div class="control-group">
					<label class="control-label" for="firstname">
						<?= Yii::t('standard', '_FIRSTNAME'); ?>&nbsp;<?= (($options['lastfirst_mandatory'] == 'on') ? $mand_sym : '') ?>
					</label>
					<div class="controls">
						<?php
						echo CHtml::textField(
							'firstname',
							'',
							array('class' => 'pull-right', 'id' => 'firstname', 'maxlength' => 255, 'autocomplete' => 'off')
						);
						?>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="lastname">
						<?= Yii::t('standard', '_LASTNAME'); ?>&nbsp;<?= (($options['lastfirst_mandatory'] == 'on') ? $mand_sym : '') ?>
					</label>
					<div class="controls">
						<?php
						echo CHtml::textField(
							'lastname',
							'',
							array('class' => 'pull-right', 'id' => 'lastname', 'maxlength' => 255, 'autocomplete' => 'off')
						);
						?>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="language">
						<?= Yii::t('standard', '_LANGUAGE'); ?>
					</label>
					<div class="controls">
						<select class="pull-right" id="language" name="language">
							<?php
							$defaultLanguage = '';
							foreach ($languages as $language) {
								if ($language['browsercode'] == Yii::app()->getLanguage()) {
									$defaultLanguage = $language["code"];
								}
								echo CHtml::tag('option', array('value' => $language["code"]), $language["description"], true);
							}
							?>
						</select>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="password">
						<?= Yii::t('standard', '_PASSWORD'); ?>&nbsp;<?= $mand_sym ?>
					</label>
					<div class="controls">
						<?php
						echo CHtml::passwordField(
							'password',
							'',
							array('class' => 'pull-right', 'id' => 'password', 'maxlength' => 255, 'autocomplete' => 'off')
						);
						?>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="password_retype">
						<?= Yii::t('register', '_RETYPE_PASSWORD'); ?>&nbsp;<?= $mand_sym ?>
					</label>
					<div class="controls">
						<?php
						echo CHtml::passwordField(
							'password_retype',
							'',
							array('class' => 'pull-right', 'id' => 'password_retype', 'maxlength' => 255, 'autocomplete' => 'off')
						);
						?>
					</div>
				</div>

				<?php
				echo $extraFields;
				?>

			</div>
		</div>
	</div>

<?php if ($options['privacy_policy'] == 'on'): ?>
	<div class="register-policy">
		<div class='row-fluid'>
			<div class="control-group">
				<div class="span12 controls">
					<input type="checkbox" id="privacy" name="privacy"/> &nbsp;&nbsp;
					<a onclick="window.open(this.href);return false;" href="<?= Yii::app()->createUrl('site/privacy') ?>">
						<?= Yii::t('register', '_REG_PRIVACY_ACCEPT'); ?>
					</a>
				</div>
			</div>
		</div>
	</div>
<? endif; ?>

<div class="register-footer-multistep">
	<div class="row-fluid">
		<div class="control-group">
			<div class="span12 controls">
				<?php
				echo CHtml::submitButton(
					Yii::t('standard', '_CONFIRM'),
					array('class' => 'span3 btn btn-docebo green', 'name' => 'register_button')
				);
				?>
				<?php
				echo CHtml::button(
					Yii::t('standard', '_CANCEL'),
					array('class' => 'span3 btn btn-docebo black', 'name' => 'cancel_button', 'onclick' => 'closeRegisterDialog();')
				);
				?>
			</div>
		</div>
	</div>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">

	$(document).ready(function() {

		<?php
		echo 'var username = ' . CJSON::encode(isset($userPostedData) && isset($userPostedData["username"]) ? $userPostedData["username"] : '') . ';'."\t\n";
		echo 'var email = ' . CJSON::encode(isset($userPostedData) && isset($userPostedData["email"]) ? $userPostedData["email"] : '') . ';'."\t\n";
		echo 'var firstname = ' . CJSON::encode(isset($userPostedData) && isset($userPostedData["firstname"]) ? $userPostedData["firstname"] : '') . ';'."\t\n";
		echo 'var lastname = ' . CJSON::encode(isset($userPostedData) && isset($userPostedData["lastname"]) ? $userPostedData["lastname"] : '') . ';'."\t\n";
		echo 'var language = ' . CJSON::encode(isset($userPostedData) && isset($userPostedData["language"]) ? $userPostedData["language"] : $defaultLanguage) . ';'."\t\n";
		echo 'var privacy = ' . CJSON::encode(isset($userPostedData) && isset($userPostedData["privacy"]) ? $userPostedData["privacy"] : '') . ';'."\t\n";
		?>

		$("#username").val(username);
		$("#email").val(email);
		$("#firstname").val(firstname);
		$("#lastname").val(lastname);
		$("#language").val(language);
		if(privacy=='on') {
			$("#privacy").attr('checked',true);
		}

		// Get and set client timezone
		var d2 = new Date()
		$("#cl_tz_offset").val(-d2.getTimezoneOffset()*60);

		if ($.browser.msie && $.browser.version > 9) {
			$("form#register-form")
				.attr("enctype", "multipart/form-data")
				.attr("encoding", "multipart/form-data")
			;
		}
});

var closeRegisterDialog = function() {
	$('.modal.register').find('.modal-body').dialog2("close");
};

</script>