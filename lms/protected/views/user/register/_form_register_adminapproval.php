<!DOCTYPE html>
<html>
<head>
	<title>
		<?= Yii::t('register', '_REG_YOUR_ABI_TO_ACCESS') ?>
	</title>
</head>
<body>
<h1>
	<?= Yii::t('register', '_REG_YOUR_ABI_TO_ACCESS') ?>
</h1>
<div class="registration-success">
	<p>
		<?= Yii::t('register', '_REG_WAIT_FOR_ADMIN_OK') ?>
	</p>
</div>
</body>
</html>