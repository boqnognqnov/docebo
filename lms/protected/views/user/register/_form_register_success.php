<!DOCTYPE html>
<html>
	<head>
		<title>
			<?=Yii::t('user','Confirm your registration'); ?>
		</title>
	</head>
	<body>
		<h1><?=Yii::t('user','Confirm your registration'); ?></h1>
		<div class="registration-success">
			<p>
				<?=Yii::t('register','_REG_SUCCESS'); ?>
			</p>
		</div>
	</body>
</html>