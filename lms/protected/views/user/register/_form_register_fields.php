<?php
/* @var $redirect_url string */

/* @var $form CActiveForm */
?>

<h1><?=Yii::t('register','New user?'); ?></h1>


<?php if (Yii::app()->user->hasFlash('error')) : ?>
	<div class="register-error">
		<div class="row-fluid">
			<div class="span12 alert alert-error">
				<?php echo Yii::app()->user->getFlash('error'); ?>
			</div>
		</div>
	</div>
<?php endif; ?>


<div class="register-instructions">
	<p>
		<?=Yii::t('register','_REG_NOTE'); ?>
		<ul>
			<li><?= Yii::t('register','_REG_MANDATORY',array('[mandatory]' => '<span class="mandatory">*</span>')); ?></li>
		</ul>
	</p>
</div>


<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'register-form',
	'method' => 'POST',
	'action' => Yii::app()->createUrl('user/axRegister'),
	'htmlOptions' => array(
		'class' => 'ajax form-horizontal'
	)
));

$_username = !empty($userPostedData) && isset($userPostedData['username']) ? $userPostedData['username'] : '';
$_email = !empty($userPostedData) && isset($userPostedData['email']) ? $userPostedData['email'] : '';
$_firstname = !empty($userPostedData) && isset($userPostedData['firstname']) ? $userPostedData['firstname'] : '';
$_lastname = !empty($userPostedData) && isset($userPostedData['lastname']) ? $userPostedData['lastname'] : '';
$_password = !empty($userPostedData) && isset($userPostedData['password']) ? $userPostedData['password'] : '';
$_password_retype = !empty($userPostedData) && isset($userPostedData['password_retype']) ? $userPostedData['password_retype'] : '';
$_language = !empty($userPostedData) && isset($userPostedData['language']) ? $userPostedData['language'] : '';
$_reg_code = !empty($userPostedData) && isset($userPostedData['reg_code']) ? $userPostedData['reg_code'] : '';
$_cl_tz_offset = !empty($userPostedData) && isset($userPostedData['cl_tz_offset']) ? $userPostedData['cl_tz_offset'] : '';

//step 1 data, re-print it as hidden parameters to be submitted togheter with additional fields
echo CHtml::hiddenField('username', $_username);
echo CHtml::hiddenField('email', $_email);
echo CHtml::hiddenField('firstname', $_firstname);
echo CHtml::hiddenField('lastname', $_lastname);
echo CHtml::hiddenField('password', $_password);
echo CHtml::hiddenField('password_retype', $_password_retype);
echo CHtml::hiddenField('language', $_language);
echo CHtml::hiddenfield('reg_code', $_reg_code);
echo CHtml::hiddenField('cl_tz_offset', $_cl_tz_offset);
if ($options['privacy_policy'] == 'on') {
	echo CHtml::hiddenField('privacy', 'on'); //put it directly at true, since being here means that it MUST have been checked
}

echo CHtml::hiddenField('redirect_url', $redirect_url);
echo CHtml::hiddenField('courseId', $courseId);
echo CHtml::hiddenField('from_step', 'step_2', array('id' => 'from-step-input'));

?>


<div class="register-fields">
	<div class="internal">
		<div class="row-fluid">
			<?php
			echo $additionalFields;
			?>
		</div>
	</div>
</div>


<div class="register-footer-multistep">
	<div class="row-fluid">
		<div class="control-group">
			<div class="span12 controls">
				<!--<input class="span4 btn btn-docebo green" type="submit" name="back_button" value="<?= Yii::t('standard', '_PREV') ?>">
				&nbsp;&nbsp;
				<input class="span4 btn btn-docebo green" type="submit" name="register_button" value="<?= Yii::t('user', 'Register') ?>">-->
				<?php

				echo CHtml::submitButton(
					Yii::t('standard', '_BACK'),
					array('class' => 'span3 btn btn-docebo green', 'name' => 'back_button')
				);

				echo '&nbsp;&nbsp;';

				echo CHtml::submitButton(
					Yii::t('standard', '_CONFIRM'),//Yii::t('user', 'Register')
					array('class' => 'span3 btn btn-docebo green', 'name' => 'register_button')
				);

				echo '&nbsp;&nbsp;';

				echo CHtml::button(
					Yii::t('standard', '_CANCEL'),
					array('class' => 'span3 btn btn-docebo black', 'onclick' => 'closeRegisterDialog();')
				);
				?>
			</div>
		</div>
	</div>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">

$(document).ready(function() {

	<?php if ($loadDatePicker): ?>
	//TODO: if no date inputs are present, loading datepicker libraries and apply datepicker is unnecessary. A check should be performed.
	try {
		$('.modal .input-append.date').bdatepicker();
	} catch (e) {

		//maybe datepicker libraries have not been loaded ... try to do it now
		if ($.isFunction("bdatepicker") == false) {
			$("head").append("<link rel='stylesheet' type='text/css' href='<?php echo Yii::app()->bootstrap->getAssetsUrl().'/css/bootstrap-datepicker.css' ?>'>");

			$.getScript("<?php echo Yii::app()->bootstrap->getAssetsUrl().'/js/bootstrap.datepicker.js' ?>", function () {

				<?php
				$localeFile = DatePickerHelper::getLocaleFileName(); //localization file name for current language
				if (!empty($localeFile)):
				?>
				// Now load the localization file
				$.getScript("<?php echo Yii::app()->bootstrap->getAssetsUrl().'/js/locales/'.$localeFile ?>", function () {
					$('.modal .input-append.date input').bdatepicker();
				});
				<?php else: ?>
				$('.modal .input-append.date input').bdatepicker();
				<?php endif; ?>

			});

		} else {
			$('.modal .input-append.date input').bdatepicker();
		}

	}
	<?php endif; ?>

	if ($.browser.msie && $.browser.version > 9) {
		$("form#register-form")
			.attr("enctype", "multipart/form-data")
			.attr("encoding", "multipart/form-data")
		;
	}
});

var closeRegisterDialog = function() {
	$('.modal.register').find('.modal-body').dialog2("close");
};
</script>