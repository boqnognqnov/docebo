<!DOCTYPE html>
<html>
	<head>
		<title>
			<?= Yii::t('register', 'New user?') ?>
		</title>
	</head>
	<body>
		<h1>
			<?= Yii::t('register','New user?') ?>
		</h1>
		<div class="registration-failure">
			<p>
				<?= Yii::t('standard', '_OPERATION_FAILURE') ?>
			</p>
		</div>
	</body>
</html>