<?php
/**
 * Created by PhpStorm.
 * User: asen
 * Date: 04-Feb-16
 * Time: 11:08 AM
 */
// Yii::app()->createUrl('/test2/startJob')
echo CHtml::link(Yii::t('standard', 'Start long running task'),'#', array('id' => 'jobStart'));

?>
<script>
	$(function(){
		$('#jobStart').click(function(){
			var url = '';
			$.ajax({
				type: 'POST',
				url: url,
				data: {}
			})
			<?php
			// simulate the error TODO - real check of background service running
			if(!PluginManager::isPluginActive('NotificationApp')){ ?>
			var html = '<button type="button" class="close" data-dismiss="alert">×</button>'
					+Yii.t('standard', 'We encountered an error while submitting your task. Please, try again later or contact our Helpdesk service');
			var divContainer = $('<div id="dcb-feedback">');
			var divError = $('<div class="alert alert-error">');
			divError.html(html);
			$('#maincontent').prepend(divContainer.append(divError));
			<?php }?>
		})
	})
</script>