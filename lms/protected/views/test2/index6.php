<?php 
	$uploadUrl = Docebo::createLmsUrl('site/AxUploadFile');
	$flashUrl  = Yii::app()->plupload->getAssetsUrl() . "/plupload.flash.swf";
	
	$csrfToken = Yii::app()->request->csrfToken;
	
?>
<div class="row-fluid">
	
		<div class="span10">
		
		
			<div id="pluploader-container" class="span6">
				<a id="pickfiles" href="javascript:;" class="btn-docebo green">PICK FILE</a>
			</div>
			
			<div id="pluploader-progress" class="span6">
				<div class="docebo-progress progress progress-striped active">
					<div id="pluploader-progress-bar" class="bar full-height" style="width: 0%;"></div>
				</div>
			</div>
			
		</div>
		
		
		<div>
			<div class="pull-right"><span id="pluploader-progress-number"></span></div>
		</div>
		
	</div>
	
	
	<div class="row-fluid filename">
		<div class="span12">
			<span id="pluploader-filename"></span>
		</div>
		
		<div id="pluploader-error" style="display: none;" class="alert-danger"><span></span></div>
	</div>

	
	
<script type="text/javascript">

	var options = {
		chunk_size			: '1mb',
		multi_selection		: true,
		runtimes			: 'html5,flash',
		browse_button		: 'pickfiles',
		container			: 'pluploader-container',
		drop_element		: 'pluploader-dropzone',
		max_file_size		: '4mb',
		url					: <?= json_encode($uploadUrl) ?>,
		multipart			: true,
		multipart_params	: {
			YII_CSRF_TOKEN: <?= json_encode($csrfToken) ?>
		},
		flash_swf_url 		: <?= json_encode($flashUrl) ?>,
		//allowedExtensions	: '',
		
		//formId				: '',
		//fieldName			: '',
		//fieldId				: '',
// 		onInit				: '',
// 		onUploadComplete	: '',
// 		onFilesAdded		: '',
// 		onBeforeUpload		: '',
// 		onUploadProgress	: '',
// 		onError				: ''
	};
	


	
	var pl_uploader = new plupload.Uploader(options);

	
	pl_uploader.init();

	Docebo.log(pl_uploader);


	// Init event hander
	pl_uploader.bind('Init', function (up, params) {
		Docebo.log(102);
	});

	pl_uploader.bind('FilesAdded', function (up, files) {
		this.files = [files[0]];
		pl_uploader.start();
	});

	// Just before uploading file
	pl_uploader.bind('BeforeUpload', function (up, file) {
	});


	// Upload progress
	pl_uploader.bind('UploadProgress', function (up, file) {

		app7020.pluploaderProgress(file);
		
	});
	
	
	// On error
	pl_uploader.bind('Error', function (up, error) {
	});
	
	// On Upload complete
	pl_uploader.bind('UploadComplete', function (up, files) {
	});
	
	
</script>	


