<style>
	.progressDiv {
		margin: 10px;
	}

	.progressElement {
		position: relative;
		display: inline-block;
		background-color: #eee;
		width: 78%;
		height: 10px;
		margin-top: 10px;
	}

	.progressElementInner {
		position: absolute;
		background-color: #5EBE5D;
		top: 0;
		left: 0;
		height: 10px;
	}

	.notifyLink {
		color: #08c;
		cursor: pointer;
		margin-left: 15px;
	}

	.infoDiv {
		font-size: 0.9em;
		opacity: 0.75;
		font-weight: 200;
	}

	.notificationSubscribed {
		opacity: 1;
		font-weight: bold;
		margin-left: 15px;
	}

	.yesTick {
		background-image: url('<?=Yii::app()->theme->baseUrl?>/images/icons_elements.png');
		display: inline-block;
		width: 17px;
		height: 14px;
		background-position: -192px -104px;
		margin-right: -15px;
	}

	.percentElement {
		display: inline-block;
		margin-left: 7px;
		font-size: 1.3em;
	}

	.stopJob {
		display: inline-block;
		margin: -6px 0 0 12px;
	}

	.greenClass {
		color: #5EBE5D;
	}
	#backgroundJobList .row-fluid>.span3{
		margin-top: -3px;
	}
	#backgroundJobList .row-fluid{
		border-top: 1px solid #dadada;
	}
	h4{
		margin-bottom: 10px;
	}
	#backgroundJobList .progressDiv{
		margin: 10px 0;
		border-bottom: 1px solid #dadada;
	}
	#backgroundJobList .inbox-wrapper .circle{
		margin-top: 6px;
	}
	#backgroundJobList .inbox-wrapper{
		padding: 15px 0;
	}
</style>
<?php
$this->breadcrumbs = array(
		Yii::t('backgroundjobs', 'Background jobs'),
);
?>
<div id="backgroundJobList">
	<h4><?=Yii::t('backgroundjobs', 'Background jobs');?></h4>
	<?php
	$isGodadmin = Yii::app()->user->getIsGodadmin();
	if ($isGodadmin){ ?>
	<div class="row-fluid">
		<div class="span3">
			<ul class="nav nav-tabs">
				<li class="active">
					<a href="#all-jobs" data-toggle="tab"><?= Yii::t('backgroundjobs', 'All Jobs') ?></a>
				</li>
				<li>
					<a href="#my-jobs" data-toggle="tab"><?= Yii::t('backgroundjobs', 'My Jobs') ?></a>
				</li>
			</ul>
		</div>

		<div class="tab-content span9">
			<div id="all-jobs" class="tab-pane active"></div>
			<div id="my-jobs" class="tab-pane"></div>
		</div>
	</div>
</div>
<?php } ?>
</div>
<script>
	function renderJobsPage(jobs, inHeader) {
		inHeader = inHeader || false;
		var userId = '<?=Yii::app()->user->id?>';
		var br = 0;
		var selector = '';
		var myJobsSelector = '';
		var allJobs = {};
		var isGodadmin = '<?=$isGodadmin?>';
		if (inHeader === true) {
			selector = '#header #defaultJob #jobsContainer';
		} else {
			var counter = 0;
			jobs.forEach(function (element, index) {
				if (isGodadmin || element.authorId == userId)
					counter++;
			});
			$('#header #defaultJob .count').text(counter);
			selector = '#maincontent #content #backgroundJobList';
			if (isGodadmin) {
				myJobsSelector = selector + ' #my-jobs';
				selector += ' #all-jobs';
			}
		}

		jobs.forEach(function (element, index) {
			var update = false;
			var jobUiElement = '';
			var myJobUiElement = '';
			if (inHeader === true && br >= 5)
				return;
			if ($(selector + ' #' + element.hash_id).length > 0) {
				jobUiElement = $(selector + ' #' + element.hash_id);
				update = true;
				if (myJobsSelector != '')
					myJobUiElement = $(myJobsSelector + ' #' + element.hash_id);
			} else {
				jobUiElement = $('<div></div>');
				jobUiElement.attr('id', element.hash_id);
				jobUiElement.attr('class', 'progressDiv');

				myJobUiElement = $('<div></div>');
				myJobUiElement.attr('id', element.hash_id);
				myJobUiElement.attr('class', 'progressDiv');
			}

			var notifySpan = '<span class="notificationSubscribed yesTick" style="display: none"></span><span class="notifyLink">' + Yii.t('standard', 'Notify me when finished') + '</span>';
			var infoDiv = '<div class="infoDiv">Running from ' + element.elapsedTimeStr + notifySpan + '</div>';
			var progressElement = '<div class="progressElement"><div class="progressElementInner" style="width: ' + element.percent + '%"></div></div>';
			var greenClass = (element.percent == 100) ? 'greenClass' : '';
			var percentElement = '<span class="percentElement ' + greenClass + '">' + element.percent + '%</span>';
			var cancelJobElement = '<span class="stopJob p-sprite cross-red"></span>';

			if (element.percent == 100) {
				cancelJobElement = '';
				notifySpan = '';
				infoDiv = '';
			}
			var html = '';
			if (inHeader === true) {
				html = '<div class="inbox-item"><div class="pull-left"><a href="javascript:void(0);">' +
						'<div class="circle"></div></a></div><div class="pull-left" style="width: 413px;font-weight: 600">' + element.translatedName +
						'<br/><span style="font-size: 0.9em; opacity: 0.75;font-weight: 200">Started by ' + element.authorName + ' on ' + element.startDateStr + '</span>'
						+ infoDiv + progressElement + percentElement + cancelJobElement + '</div>' +
						'<div class="clearfix"></div></div>';
			}else{
				html = '<div class="inbox-wrapper"><div class="pull-left"><a href="javascript:void(0);">' +
						'<div class="circle"></div></a></div><div class="pull-left" style="font-weight: 600">' + element.translatedName +
						'<br/><span style="font-size: 0.9em; opacity: 0.75;font-weight: 200">Started by ' + element.authorName + ' on ' + element.startDateStr + '</span></div><div class="pull-left" style="width: 51%;margin-left: 25px">'
						+ infoDiv + progressElement + percentElement + cancelJobElement + '</div>' +
						'<div class="clearfix"></div></div>';
			}

			if (isGodadmin || element.authorId == userId) {
				jobUiElement.html(html);
				if (myJobsSelector != '' && element.authorId == userId) {
					myJobUiElement.html(html);
				}

				if (update === false) {
//					debugger;
					$(selector).append(jobUiElement);
					if (myJobsSelector != '' && element.authorId == userId) {
						$(myJobsSelector).append(myJobUiElement);
					}
				}
			}

			br++;
		});
	}
	var BackgroundJobs = [];
	var notificationUrl = '<?=Yii::app()->createUrl('/test2/notifyWhenFinish')?>';
	var socket = new eio.Socket('ws://localhost:33380/', {
		transports: ['websocket', 'polling'],
	});
	socket.on('open', function () {
		socket.send(JSON.stringify({
			domain: '<?=Docebo::getCurrentDomain()?>',
			session: '<?=Yii::app()->request->cookies['docebo_session']?>'
		}), {}, function () {
			socket.on('message', function (jsonStr) {
				var data = JSON.parse(jsonStr);

				var countJobs = data.jobs.length;
				if (data.full) {

					console.log('Receiving full jobs update. Received ' + countJobs + ' jobs');
				} else {
					console.log('Receiving incremental updates for ' + countJobs + ' jobs');
				}
				var jobUiElement;
				for (var i = 0; i < countJobs; i++) {
					var job = data.jobs[i];

					var name = job.name;
					var placeholders = job.placeholders;
					var authorId = job.authorId;
					var authorName = job.authorName;
					var startDate = job.startDate;
					var dateObj = new Date(job.startDate * 1000);
					var startDateStr = dateObj.getFullYear() + '-' + (dateObj.getMonth() + 1) + '-' + dateObj.getDate() + ' ' + dateObj.getHours() + ':' + dateObj.getMinutes() + ':' + dateObj.getSeconds();
					var translatedName = Yii.t('standard', name, placeholders);
					var currentTime = parseInt($.now() / 1000);
					var elapsedTime = currentTime - startDate;
					var hours = parseInt(elapsedTime / 3600);
					var minutes = parseInt((elapsedTime - hours * 3600) / 60);
					var elapsedTimeStr = hours + 'h ' + minutes + 'm ';
					var percent = job.percent;

					var isFound = false;
					var dataToRecord = {
						'hash_id': job.hash_id,
						'translatedName': translatedName,
						'authorId': authorId,
						'authorName': authorName,
						'startDateStr': startDateStr,
						'elapsedTimeStr': elapsedTimeStr,
						'percent': percent,
						'currentTime': currentTime,
						'startDateTimeStamp': startDate,
					};
					BackgroundJobs.forEach(function (element, index) {
						if (element.hash_id == job.hash_id) {
							isFound = true;
							BackgroundJobs[index] = dataToRecord;
						}
					});
					if (isFound === false) {
						BackgroundJobs.push(dataToRecord);
					}
				}
				BackgroundJobs.sort(function (a, b) {
					return b.startDateTimeStamp - a.startDateTimeStamp;
				});
				renderJobsPage(BackgroundJobs);
				renderJobsPage(BackgroundJobs, true);
			});
		});

		socket.on('close', function () {
			console.log('Closing socket');
		});
	});
	$(document).on('click', '#header #jobsContainer .progressDiv .infoDiv .notifyLink', function () {
		var that = $(this);

		var targetToUpdate = that.closest('.progressDiv');
		var targetId = targetToUpdate.prop('id');

		$.ajax({
			type: 'POST',
			url: notificationUrl,
			data: {userId: '<?=Yii::app()->user->id?>', jobHash: targetId}
		}).success(function (data) {
// todo send a query to node.js to update status of the notification span
			that.removeClass('notifyLink');
			that.addClass('notificationSubscribed');
			that.prevAll('.yesTick').show();
// todo translation
			that.text('You will be notified when finished');
		}).error(function () {
			that.removeClass('notifyLink');
			that.addClass('notificationSubscribed');
			that.prevAll('.yesTick').show();
// todo translation
			that.text('You will be notified when finished');
		});
	});

	$(document).on('click', '#header #jobsContainer .progressDiv .stopJob', function () {
		var that = $(this);
		that.hide();
		that.prevAll('.infoDiv').hide();
// todo send a query to node.js to stop this job!

	});
	$(document).on('click', 'ul.nav.nav-tabs>li', function(){
		var that = $(this);
		setTimeout(function(){
			var height = $('.tab-content').height();
			that.parent().height(height);
		},100);
	});
	$(function () {
		$('ul.nav.nav-tabs>li.active').trigger('click');
	});
</script>