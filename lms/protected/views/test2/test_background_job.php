<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 2/26/2016
 * Time: 1:47 PM
 */

echo CHtml::link('Start job', '#', array(
    'class' => 'btn btn-docebo green big',
    'id' => 'start_job'
));

?>

<script type="text/javascript">
    $(function(){
        $(document).on('click', '#start_job', function (){
            $.ajax({
                url: '<?= Docebo::createLmsUrl('backgroundjobs/default/createDummy') ?>'
            });
        });
    });
</script>
