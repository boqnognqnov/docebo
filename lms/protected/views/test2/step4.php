<!-- SPECIFIC PART -->
<?php
	echo CHtml::beginForm('', "POST", array(
			'id' => 'users-selector-form', 
			'name' => 'users-selector-form',
			'class' => 'ajax jwizard-form',
	)); 
?>
<!-- SPECIFIC PART -->




<div class="tabbable">


	<ul class="nav nav-tabs nav-stacked pull-left">

		<li class="active">
			<a data-toggle="tab" href="#users">
				<span class="icons-elements user"></span>&nbsp;&nbsp;<?php echo Yii::t('standard', '_USERS'); ?>
			</a>
		</li>
	
		<li>
			<a data-toggle="tab" href="#groups">
				<span class="icons-elements group"></span>&nbsp;&nbsp;<?php echo Yii::t('standard', '_GROUPS'); ?>
			</a>
		</li>
			
		<li>
			<a data-toggle="tab" href="#orgchart">
				<span class="icons-elements orgchart"></span>&nbsp;&nbsp;<?php echo Yii::t('standard', '_ORGCHART'); ?>
			</a>
		</li>
		
	</ul>



	<!-- CONTENT -->
	<div class="tab-content">

		<div id="users" class="tab-pane active">
			Users<br>
			<input type='text' name='some_field' value=''>
		</div>
	
		<div id="groups" class="tab-pane">
			Groups
		</div>
	
		<div id="orgchart" class="tab-pane">
			Org chart
			
						<div>
						<?php
							$orgchartsFancyTreeId = 'orgcharts-fancytree';
							$fancyTreeData = CoreOrgChartTree::buildFancytreeDataArray(false, true, true);
							$this->widget('common.extensions.fancytree2.FancyTree', array(
								'id' => $orgchartsFancyTreeId,
								'treeData' => $fancyTreeData,
								'preSelected' => $preSelected,
								'formId' => 'select-form',
								'useJSON' => false,
								'formFieldNames' => array(
									'selectedInputName'	 	=> 'select-orgchart',
									'activeInputName' 		=> 'orgchart_active_node',
									'selectModeInputName'	=> 'orgchart_select_mode'
								),
							));
						?>
						</div>
			
			
		</div>

	</div>
	
	

</div>



<!-- SPECIFIC PART -->
<!-- For Dialog2 - they go in footer -->
<div class="form-actions jwizard">
	<?= CHtml::submitButton(Yii::t('standard', '_PREV'), 		array('class' => 'btn btn-docebo green big jwizard-nav', 'data-jwizard-url' => '/lms/?r=test2/axStep3')); ?>
	<?= CHtml::submitButton(Yii::t('standard', 'FINISH'), 		array('name' => 'finish_wizard', 'class' => 'btn btn-docebo green big jwizard-nav jwizard-finish', 'data-jwizard-url' => '/lms/?r=test2/axWizardFinish')); ?>
	
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-docebo black big close-dialog')); ?>
</div>
<?php echo CHtml::endForm() ; ?>
<!-- SPECIFIC PART -->





<script type="text/javascript">

	$(function(){
		// Crete JS compagnion object and keep ALL the JS there, if possible
		var options = {
			formId: 'users-selector-form',
			orgchartsFancyTreeId: '<?= $orgchartsFancyTreeId ?>'
		};
		var usersSelector = new UsersSelectorClass(options);
				
		
	});

</script>
