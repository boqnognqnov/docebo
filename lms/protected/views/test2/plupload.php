<?php 
	$csrfToken = Yii::app()->request->csrfToken;
	$uploadUrl = Docebo::createLmsUrl('site/AxUploadFile');
	$flashUrl = Yii::app()->plupload->getAssetsUrl() . "/plupload.flash.swf";
?>

<?php 

	echo CHtml::beginForm();
	
	echo CHtml::hiddenField('type', FileTypeValidator::TYPE_SUBTITLES);
	
	echo CHtml::hiddenField('original_filename', '');
	echo CHtml::hiddenField('target_filename', '');
	
?>

<hr>

<div id="pluploader-wrapper">
	<div class="pluploader-wrapper-inner">
	
		<div id="pluploader-container">
			<a id="pickfiles" href="javascript:;" class="btn-docebo green">Choose file</a>
		</div>
	
	
	</div>
</div>

<hr>

<?php echo CHtml::submitButton(); ?>


<?php echo CHtml::endForm(); ?>


<script type="text/javascript">

	$(function(){

		var options = {
			unique_names		: true,	
			chunk_size			: '1mb',
			multi_selection		: false,
			runtimes			: 'html5,flash',
			browse_button		: 'pickfiles',
			container			: 'pluploader-container',
			drop_element		: 'pluploader-dropzone',
			max_file_size		: '4mb',
			url					: <?= json_encode($uploadUrl) ?>,
			multipart			: true,
			multipart_params	: {YII_CSRF_TOKEN: <?= json_encode($csrfToken) ?>},
			flash_swf_url 		: <?= json_encode($flashUrl) ?>,
			//allowedExtensions	: 'vtt,srt'
				
		};


		// 1. 
		var pl_uploader = new plupload.Uploader(options);

		// 2.
		if (!pl_uploader) {
			Docebo.log('PLUploader not properly initialized or created');
			return false;
		}

		// ----
		pl_uploader.init();
		
		pl_uploader.bind('Init', Init);
		pl_uploader.bind('BeforeUpload', BeforeUpload);
		pl_uploader.bind('FilesAdded', FilesAdded);
		pl_uploader.bind('UploadProgress', UploadProgress);
		pl_uploader.bind('Error', Error);
		pl_uploader.bind('UploadComplete', UploadComplete);


		function Init(up,params) {
			Docebo.log(up,params);
		} 

		function BeforeUpload(up,file) {
			Docebo.log(up,file);
		} 

		function FilesAdded(up,file) {

			// -- 
			pl_uploader.start();
			
			Docebo.log(up,file);
		} 

		function UploadProgress(up,file) {
			Docebo.log(up,file);
		}

		function Error(up,error) {
			Docebo.log(up,error);
		} 

		function UploadComplete(up,files) {

			var file = files[0];

			
			$('input[name="original_filename"]').val(file.name);
			$('input[name="target_filename"]').val(file.target_name);
			
			Docebo.log('Completed');
			Docebo.log(up,files);
		} 
		
		
	});

	

</script>
