<style>
<!--

	.image-selector-modal-test2 {
	    width: 600px !important;
    	margin-left: -300px  !important;
	}
	
	


-->
</style>


<?php

	$firstDialogUrl = Docebo::createLmsUrl('test2/is2');
	
	
	$this->widget('common.widgets.ImageSelector', array(
			'imageType'			=> CoreAsset::TYPE_PLAYER_BACKGROUND,
			'assetId' 			=> 0,
			'imgVariant'		=> CoreAsset::VARIANT_ORIGINAL,
			'buttonText'		=> Yii::t('setup', 'Upload your background'),
			'buttonClass'		=> 'btn btn-docebo green big',
			'dialogId'			=> 'image-selector-modal-player-bg',
			'dialogClass'		=> 'image-selector-modal',
			'inputHtmlOptions'	=> array(
					'name'	=> 'CoreMultidomain[coursePlayerImage]',
					'id'	=> 'player-bg-image-id',
					'class'	=> 'image-selector-input',
			),
	));
	

?>
<a class="open-dialog" href="<?= $firstDialogUrl ?>">Open first dialog</a>


