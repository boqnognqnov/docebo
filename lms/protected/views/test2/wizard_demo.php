<?php
/**
 * jWizard Demo - launching code 
 */ 
?>
<hr>
<h3>
<a 	
	href="/lms/?r=test2/axStep1"
	class="open-dialog wizard-starter"
	rel="wizard-demo"
	title="Enroll users"
>
	Start Dialog Wizard (no dispatcher)
</a>
</h3>
<br>
<p>
In this case, no dispatcher is specified. Every dialog (step) dictates what would be the NEXT dialog (step).<br>
If the current dialog does not set "data-jwizard-url" to its navigational button(s), the FORM.action URL will be used to handle its submissions.
</p>



<hr>
<h3>
<a 	
	href="/lms/?r=test2/axStep1"
	class="open-dialog wizard-starter"
	rel="wizard-demo"
	title="Enroll users"
	data-wizard-dispatcher-url="/lms/?r=test2/axWizardDemoDispatcher"
>
	Run Wizard using dispatcher, by explicitely setting dispatcher URL as jWizard option  
</a>
</h3>
<br>
<p>
In this case, a dispatcher URL is specified (<strong>/lms/?r=test2/axWizardDemoDispatcher</strong>). <br>
It overrides forms' action and any navigational buttons' "data-jwizard-url".    
</p>




<hr>
<h3>
<a 	
	href="/lms/?r=test2/wizardDemo2Dispatcher&idPlan=12345"
	class="open-dialog"
	data-dialog-class="users-selector"
	data-dialog-id="wizard-demo2"
	data-dialog-title="Enroll users"
>
	Run Wizard using dispatcher pointed by the HREF of this link.   
</a>
</h3>
<br>









<script type="text/javascript">

	// This is just an example how to set jWizard Dispatcher URL (IF you want! It is optional), depending on which <a> is clicked
	$(".wizard-starter").on("click", function(){
		if ( typeof $(this).data("wizard-dispatcher-url") == "string" ) {
			jwizard.options.dispatcherUrl = $(this).data("wizard-dispatcher-url");
		}
	});

	

	// Initialize jWizard class and create a global variable "jwizard". Give the wizard the ID of the opening dialog (the dialog)
	// NOTE: As of this writing the global variable MUST be named "jwizard" 
	var options = {
		dialogId: "wizard-demo",    // Must be equal to the  a.open-dialog's  'rel' attribute! See above
		// dispatcherUrl = "/lms/?r=test2/axWizardFinish"  // <--- you can set the dispatching URL directly here, upon jWizard initialization
	};
	var	jwizard = new jwizardClass(options);
	
	
</script>