<?php echo CHtml::beginForm("/lms/index.php?r=test2/example1", "POST", array('class' => 'ajax')); ?>


<div class="row-fluid">
	<div class="span4">
		<?php
		$this->widget('common.extensions.plupload.widgets.SingleFile', array(
			'maxFileSize' => 1000,
			'fieldName' => 'file1',
			'onUploadComplete' => 'onUploadComplete',
			'onError'	=> 'onError',
			'onFilesAdded' => 'onFilesAdded',
		));
		?>
	</div>
	
	
	<div class="span4">
		<?php 
			$html = $this->widget('common.extensions.plupload.widgets.SingleFile', array(
			'layout' => 'knob',
			'maxFileSize' => 1000,
			'fieldName' => 'file3',
			'onFilesAdded' => 'onFilesAdded',	
			'onError'	=> 'onError',
			'onUploadComplete' => 'onUploadComplete',
		), true);
		echo $html;
	?>
	</div>
	
	
	<div class="span4">
		<?php 
			$html = $this->widget('common.extensions.plupload.widgets.SingleFile', array(
			//'allowedExtensions' => 'jpg',
			'layout' => 'knobhorizontal',
			'maxFileSize' => 1000,
			'fieldName' => 'file3',
			'onFilesAdded' => 'onFilesAdded',	
			'onError'	=> 'onError',
			'onUploadComplete' => 'onUploadComplete',
		), true);
		echo $html;
	?>
	</div>
	
	
</div>


<div class="row-fluid">
	<div class="span12">
		<?php 
			echo CHtml::submitButton('SEND', array('class' => 'send-button'));
		?>
	</div>
</div>

<?php echo CHtml::endForm();?>

	
<!-- ----------------------------------------------- -->	
	
	
	
<hr>
<a class="open-dialog btn" href="/lms/?r=test2/axExample1Dialog" rel="upload-dialog">Example In a Dialog</a>
	


	
<script type="text/javascript">

// JS Callbacks :
// BIG NOTE: We define these in this page... and do NOT redefine in the Dialog example page.
// But you can do this in Dilaog page also (loaded by AJAX) IF they are not defined here on any initially loaded page.	


// In this example all widgets assign the same onXXXXXX() callback, but they (widgets) can define completely different ones!!!

// Keeps the number of active uploads at the moment.
var uploadsCounter = 0;

function onUploadComplete(id, file, uploader) {
	uploadsCounter--;
	if (uploadsCounter <= 0) {
		$('.send-button').prop('disabled', false);
	}
}

function onError(id, error, uploader) {
}


function onFilesAdded(id, file, uploader) {
	uploadsCounter++;
	$('.send-button').prop('disabled', true);
}



</script>	





