<h1>Step 2</h1>

<?php 
	echo CHtml::beginForm('', 'POST', array(
		'id' => 'step2-form-id',
		'name' => 'step2-form-name',		
		'class' => 'ajax jwizard-form',
	));
	
	echo CHtml::hiddenField('from_step', 'step2');
	
?>
	
	
<input class="jwizard" type="text" name="f21" value="">
<input class="jwizard" type="checkbox" name="f22" value="1">
	

<div class="form-actions jwizard">
	<?= CHtml::submitButton(Yii::t('standard', '_PREV'), 		array('class' => 'btn btn-docebo green big jwizard-nav', 'data-jwizard-url' => '/lms/?r=test2/axStep1')); ?>
	<?= CHtml::submitButton(Yii::t('standard', '_NEXT'), 		array('class' => 'btn btn-docebo black big jwizard-nav', 'data-jwizard-url' => '/lms/?r=test2/axStep3')); ?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), 			array('class' => 'btn btn-docebo black big close-dialog', 'name' => 'cancel_step')); ?>
</div>


<?php echo CHtml::endForm(); ?>

