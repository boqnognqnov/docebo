<script>

	$('#defaultJob #jobsContainer').remove();
	var viewAllElement ='<p class="text-center view-all">' +
			'<a href="<?=Docebo::createLmsUrl('//test2/viewAllJobs')?>">' +
			'<?=Yii::t('standard', 'View all')?></a></p>';
	var row = $('<div id="jobsContainer">');
	row.appendTo($('#defaultJob .unread-tooltip.text-left'));
	var html = renderJobsPage(BackgroundJobs, true);
	row.html(html);
	row.append(viewAllElement);
</script>
