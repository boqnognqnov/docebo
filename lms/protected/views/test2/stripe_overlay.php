<?php $this->breadcrumbs = array(
	Yii::t('order', 'Test stripe overlay page'),
); ?>

<div style="margin: 100px auto 0px auto; text-align: center;">
    <p>Please, click the button to trigger the event "legacy.toggleStripeOverlay". Click again on the overlay, to set it off.</p>
    <button onclick="showOverlay()">Show overlay</button>
    <br/><br/><br/>
    <p>Please, click the button to trigger the event "legacy.reloadBillingInfo".</p>
    <button onclick="reloadBillingInfo()">Reload billing</button>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".stripe-backdrop").unbind().click(hideOverlay);
    });

    function showOverlay() {
        $(".stripe-backdrop").show();
		<?= Yii::app()->legacyWrapper->toggleStripeOverlay(true, false) ?>
    }

    function hideOverlay() {
        $(".stripe-backdrop").hide();
		<?= Yii::app()->legacyWrapper->toggleStripeOverlay(false, false) ?>
    }

    function reloadBillingInfo() {
		<?= Yii::app()->legacyWrapper->reloadBillingInfo(false) ?>
    }
</script>