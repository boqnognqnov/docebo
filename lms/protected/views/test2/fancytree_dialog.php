


<?php
	Yii::app()->fancytree->register(); 
	echo CHtml::beginForm('', 'post', array(
		'class' => 'ajax',
		'id' => 'branch-selection-form'
	));
?>

<h1>Test</h1>

<h3>Testing Fancytree (Total nodes: <span id="global-counter"></span>)</h3>


<div class="form-actions">

	<input type="button" class="close-dialog" value="Cancel">
	<input type="submit" value="Submit">

</div>

<div id="branches-tree"></div>

<?php
	echo CHtml::endForm(); 
?>








<script type="text/javascript">
	var FancyTreeData = [];



	/** CREATE TESTING DATA ***/
	var globalLevel = 0;
	var globalCounter = 0;
	
	var N = 3;  	// *Default* number of FOLDERS per level
	var NL = []; 	// Specific Number of folders for given level
	NL[0] = 0; // not used
	NL[1] = 20;
	NL[2] = 4;
	NL[3] = 3;
	NL[4] = 2;	

	var M = 10;  // Number of NODE LEAFS in final folder
	var L = 5;  // Level depth
	// Count = N ^ L * M  + ??? (can't find the formula yet)  

			
	/** Build LEAFS */		
	function getChildrenList() {
		var list = [];
		var node = {};
		for (var i=1; i <= M; i++) {
			globalCounter++;
			node = {
					title: "Node " + globalCounter, 
					key: globalCounter,
					data: {},
			};
			list.push(node);
		}
		return list;
	};


	/** Recursively build Tree data **/
	function getTreeData() {
		var node = {};
		var list = [];

		globalLevel++;
		
		if (NL[globalLevel]) var X = parseInt(NL[globalLevel]);
		else var X = N;  // default
		
		for (var i=1; i <= X; i++) {
			globalCounter++;
			node = {
				title: "Folder  " + " " + globalLevel + "-" + i, 
				key: globalLevel*10+i,
				data: {},
				folder: true,
				children: (globalLevel < L) ? getTreeData() : getChildrenList()  
			};
			list.push(node); 
		}
		globalLevel--;
		 
		return list;
	}
	

	var rootNode = {
			title: "ROOT", 
			key: 0,
			data: {},
			folder: true,
			expanded: true,
			children: getTreeData()  
		};
	
	var treeData = [rootNode];
	var node = {};
	var objDate = new Date();
	$('#global-counter').text(globalCounter);
	FancyTreeData = treeData;
	
	/** CREATE TESTING DATA: END ***/









	/** Document Ready **/
		
	$(function(){
		
		$('#branches-tree').fancytree({
			extensions: ["docebo"],
			docebo: {
				selectedInputName: "select-orgchart",
				activeInputName: "orgchart_active_node",
				selectModeInputName: "orgchart_select_mode"
				//stopOnParents: false
			},
			source: FancyTreeData,
			checkbox: true,
			clickFolderMode: 3,
			fx: null,
			selectMode: 4,
			debugLevel: 0,
			minExpandLevel:0,
			generateIds: true,
			idPrefix: "branch_node_",
			icons: false

			// EVENTS
			
		});


		// FORM
		
		$('#branch-selection-form').submit(function(){
			$('#branches-tree').fancytree("getTree").ext.docebo._generateFormElements(true);
			$(this).ajaxSubmit();
			return false;
		}); 
		
		
	});




	

</script>

