<hr>
<a 
	href="/lms/?r=usersSelector/axOpen&type=<?= UsersSelector::TYPE_CUSTOM_REPORTS_COURSES ?>" 
	class="open-dialog"
	rel="users-selector"
	data-dialog-title="Assign users"
	title="Assign users" 
	>
	Courses Selector
</a>


<hr>
<a 
	href="/lms/?r=usersSelector/axOpen&type=general" 
	class="open-dialog"
	rel="users-selector"
	data-dialog-title="Assign users"
	title="Assign users" 
	>
	Users Selector
</a>



<hr>
<a 	
	href="/lms/?r=test2/axStep1"
	class="open-dialog"
	rel="my-wizard"
	title="Enroll users"
>
	Start Wizard 
</a>

	
<hr>
<h4>Fancy tree, no dialog</h4>
<?php

	$preselected = array(
		array('key' => 0, 'selectState' => 2),
	);


	$lazyMode = true;
	$startingNodeId = 0;
	$fancyTreeData = CoreOrgChartTree::buildFancytreeDataArrayV2(false, true, true, $lazyMode, $startingNodeId, true);  // 

	$this->widget('common.extensions.fancytree.FancyTree', array(
		'debug' => false,	
		'id' => 'test-orgchart',
		'view'	=> 'index',	
		'skin' => 'skin-docebo',
		'treeData' => $fancyTreeData,
		'preSelected' => $preselected,
		// http://www.wwwendt.de/tech/fancytree/doc/jsdoc/global.html#FancytreeOptions
		'fancyOptions' => array(
			'checkbox' 	=> true,
			'icons'		=> false,
			// Define LazyLoader function: better call a function defined somewhere else
			'lazyLoad' 	=> 'function(event,data) {fancytreeLazyLoader(event,data);}',

			// Callback for when, for example, LazyLoad is finished
			'loadChildren' => 'function(event,data) { }',
		),
	
		'htmlOptions'	=> array(
			'class' => '',
		),
	
		// http://www.wwwendt.de/tech/fancytree/doc/jsdoc/global.html#FancytreeEvents
		'eventHandlers' => array(
			//'click' => 'function(event, data){}',
		),


		


	));
	

?>
	
	



<script type="text/javascript">

	function fancytreeLazyLoader(event, treeData) {

		var url = '/lms/index.php?r=usersSelector/axGetOrgchartFancytreeJson';
		var requestData = {
			nodeKey: treeData.node.key,
			lazyMode: 1,
			indents: 0,
			powerUserFilter: 1,
			expandRoot: 1
		};

		// Make a SYNC (!) call to get tree JSON data
		$.ajax({
			async: false,
			type: 'POST',
			dataType: "json",
			url: url,
			data: requestData,
			success: function(responseJSON) {
				treeData.result = responseJSON;				
			}
		});
		
	}


</script>	













<script type="text/javascript">

	$(function(){
		var options = {
			dialogId: "my-wizard" 	
		};
		var jwizard = new jwizardClass(options);
	});

	


	
</script>