<?php
/**
 * jWizard Demo - Step
 * 
 * 
 * For demonstration purposes and make live easier I put CSS in the view code, but it should be moved to external CSS file
 * 
 */ 
?>

<style>
<!--
	
-->
</style>


<h1>Step 1</h1>

<?php
	// Form MUST have either ID or NAME set
	// Form MUST have classes 'jwizard-form' and 'ajax'
	// Form MAY provide 'action' URL in case there is NO button having 'data-jwizard-url' set. Feel free to experiment with BOTH
	
	// Set NEXT step to be the Uni Selector of type 'wizarddemo'
	$nextDialogUrl = Docebo::createLmsUrl('usersSelector/axOpen', array('type' => 'wizarddemo'));
	echo CHtml::beginForm($nextDialogUrl, 'POST', array(
 		'id' => 'step1-form-id',
 		'name' => 'step1-form-name',		
		'class' => 'ajax jwizard-form form-horizontal',
	));
	
	// This is not strictly related to jWizard, but is a good example how to pass information to controller/collector about WHO is calling.
	// Add more hidden fields to inform the conroller about other important, current STEP related stuff 
	echo CHtml::hiddenField('from_step', 'step1');
	
	
?>


<div class="control-group">
	<label class="control-label" for="inputEmail">Email</label>
	<div class="controls">
		<!-- By default, ALL form fields are handled by jWizard, saving and restorimg them from its own JS based global storage -->
		<!-- IF jwizrad.options.wizardFieldsSelector is SET to, for example(!), '.jwizard', only input.jwizard's will be handled. -->
		<!-- (by default jwizrad.options.wizardFieldsSelector=false) -->		
		<input name="email" class="jwizard" type="text" id="inputEmail" placeholder="Email">
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="inputPassword">Password</label>
	<div class="controls">
		<input name="passw" class="jwizard" type="password" id="inputPassword" placeholder="Password">
	</div>
</div>

<div class="control-group">
	<div class="controls">
		<label class="checkbox"  for="cb1">Remember me
			<input name="cb1" id="cb1" class="jwizard" type="checkbox"> 
		</label>
	</div>
</div>

<div class="control-group">
	<div class="controls">
		<label class="radio" for="r1">
			Yes <input name="r1" id="r1" class="jwizard" type="radio" value="1">
		</label>
	</div>
	<div class="controls">
		<label class="radio" for="r2">
			No  <input name="r1" id="r2" class="jwizard" type="radio" value="2"> 
		</label>
	</div>
</div>


<div class="control-group">
	<div class="controls">
		<label class="select">
			Age
			<select class="jwizard" name='s1'>
				<option value='1'>Under 18</option>
				<option value='2'>18-45</option>
				<option value='3'>Old</option>
			</select>
		</label>
	</div>
</div>
		
		
<!-- Always use the Dialog2 feature "form-actions" - all buttons go in the modal footer -->	
<div class="form-actions jwizard">
	<?php
		// Render Navigation button. 
		// Variant 1: Not providing URL for next wizard step. In this case, form.action will be used, as usual
		echo CHtml::submitButton(Yii::t('standard', '_NEXT'), array('class' => 'btn btn-docebo black big jwizard-nav', 'name' => 'next_button'));

		// Variant 2: Adding attribute 'data-jwizard-url'='<next-step-url>'; on clicking this button, form.action will be replaced by this attribute 
		// echo CHtml::submitButton(Yii::t('standard', '_NEXT'), array('class' => 'btn btn-docebo black big jwizard-nav', 'data-jwizard-url' => '/lms/?r=usersSelector/axOpen&type=wizarddemo'));
		
		
		
		
		// Normal 'cancel' button, no special relation to jWizard
		echo CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-docebo black big close-dialog', 'name' => 'cancel_step'));
		
	?>
	
	
</div>
	

<?php echo CHtml::endForm(); ?>

<script type="text/javascript">

	// You can set THIS STEP modal dialog class by anouncing it to jWizard.
	// After dialog update is finished, this class will be applied by jWizard event, internally
	jwizard.setDialogClass('jwizard-step1');
	

</script>