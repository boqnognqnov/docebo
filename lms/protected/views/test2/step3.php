<h1>Step 3</h1>

<?php 
	$finishUrl = '/lms/?r=test2/axWizardFinish'; 
	echo CHtml::beginForm($finishUrl, 'POST', array(
		'id' => 'step3-form-id',
		'name' => 'step3-form-name',		
		'class' => 'ajax jwizard-form',
	));
	
	echo CHtml::hiddenField('from_step', 'step3');
	
?>
	
<div class="control-group">
	<label class="control-label" for="inputEmail">Email</label>
	<div class="controls">
		<input name="email" class="jwizard" type="text" id="inputEmail" placeholder="Email">
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="inputPassword">Password</label>
	<div class="controls">
		<input name="passw" class="jwizard" type="password" id="inputPassword" placeholder="Password">
	</div>
</div>

<div class="control-group">
	<div class="controls">
		<label class="checkbox"  for="cb1">Remember me
			<input name="cb1" id="cb1" class="jwizard" type="checkbox"> 
		</label>
	</div>
</div>

<div class="control-group">
	<div class="controls">
		<label class="radio" for="r1">
			Yes <input name="r1" id="r1" class="jwizard" type="radio" value="1">
		</label>
	</div>
	<div class="controls">
		<label class="radio" for="r2">
			No  <input name="r1" id="r2" class="jwizard" type="radio" value="2"> 
		</label>
	</div>
</div>


<div class="control-group">
	<div class="controls">
		<label class="select">
			Age
			<select class="jwizard" name='s1'>
				<option value='1'>Under 18</option>
				<option value='2'>18-45</option>
				<option value='3'>Old</option>
			</select>
		</label>
	</div>
</div>



<div class="form-actions jwizard">

	<?= CHtml::submitButton(Yii::t('standard', '_PREV'), 		array('class' => 'btn btn-docebo green big jwizard-nav', 'data-jwizard-url' => '/lms/?r=usersSelector/axOpen&type=wizarddemo')); ?>
	<?= CHtml::submitButton(Yii::t('standard', 'FINISH'), 		array('class' => 'btn btn-docebo green big jwizard-nav jwizard-finish', 'name' => 'finish_button')); ?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), 			array('class' => 'btn btn-docebo black big close-dialog', 'name' => 'cancel_step')); ?>
	
	
</div>
	

<?php echo CHtml::endForm(); ?>
