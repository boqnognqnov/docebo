<?php
//	$this->widget('zii.widgets.grid.CGridView', array(
	$this->widget('common.widgets.ComboGridView', array(
		'dataProvider' => $arrayDataProvider,
		'columns' => array(
			array(
				'name' => 'username',
				'type' => 'raw',
				'value' => 'CHtml::encode($data["username"])'
			),
			array(
				'name' => 'email',
				'type' => 'raw',
				'value' => 'CHtml::link(CHtml::encode($data["email"]), "mailto:".CHtml::encode($data["email"]))',
			),
		),
	));
?>