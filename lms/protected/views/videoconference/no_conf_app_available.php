<?php if (Yii::app()->user->isGodAdmin) : ?>
    <div class="row-fluid">
        <div class="span9 alert alert-warning">
            <?= Yii::t('conference', 'To add conferencing functionality you must activate and enable one of the Conferencing Apps available at our App marketplace.'); ?>
        </div>
        <div class="span3">
            <a id="h-apps-btn" class="pull-right btn docebo nice-btn with-icon rounded orange" href="<?= Yii::app()->createUrl('app/index') ?>">
		        <?php echo Yii::t('apps', 'Paid apps');?> <span class="icon"><i></i></span>
	        </a>
        </div>
    </div>
        
<?php else : ?>
    <div class="row-fluid">
        <div class="span12 alert alert-warning"> 
            <?= Yii::t('conference', 'Conferencing functionality is not enabled. Please ask LMS administrator to activate an App.'); ?>
        </div>
    </div>
<?php endif; ?>
