<?php

    $backToCourseUrl = "/doceboLms/index.php?modname=course&op=aula&idCourse=" . $courseModel->idCourse;

    $this->widget('common.widgets.CourseMenu', array(
            "userModel" => $userModel,
            "courseModel" => $courseModel,
            "courseUserModel" => $courseUserModel,
            "authManager" => $authManager,
            ));


    // If tcurrent user is admin/tutor/etc.. ?
    $moderator = null;
    if ($allowAdminOperations) {
        $moderator = 'true';  // YES! It must be STRING!!!!!
    }


    // Columns for ACTIVE rooms tab
    $columnsArrayActive = array(
            array(
                    'name'     => Yii::t('standard', '_VIDEOCONFERENCE'),
                    'type'		 => 'raw',
                    'value'    => array($this, 'gridRenderNameColumn')
            ),
            array(
                    'name'     => Yii::t('item', '_MIME'),
                    'type'	   => 'raw',
                    'value'    => array($this, 'gridRenderTypeColumn')
            ),
            array(
                    'name'     => Yii::t('standard', 'Starting date & Time (GMT)'),
                    'value'    => array($this, 'gridRenderStarttimeColumn')
            ),
            array(
                    'name'     => Yii::t('conference', '_MEETING_HOURS'),
                    'value'    => array($this, 'gridRenderMeetingHoursColumn')
            ),
            array(
                    'name'     => '',
                    'type'     => 'raw',
                    'value'    => '$data->getLauncherLink('.$moderator.')'
            ),
    );


    // Columns for HISTORY rooms tab
    $columnsArrayHistory = array(
            array(
                    'name'     => Yii::t('standard', '_VIDEOCONFERENCE'),
                    'type'		 => 'raw',
                    'value'    => '$data[name]'
            ),
            array(
                'name'     => Yii::t('item', '_MIME'),
                'type'	   => 'raw',
                'value'    => 'ConferenceManager::$appMap[$data[tool]]'
            ),
            array(
                    'name'     => Yii::t('standard', '_START_DATE'),
                    'value'    => array($this, 'gridRenderStarttimeColumn')
            ),
            array(
                    'name'     => Yii::t('conference', '_MEETING_HOURS'),
                    'value'    => array($this, 'gridRenderMeetingHoursColumn')
            ),
    );


    // UPDATE button
    $buttonUpdate = array(
            'url' => 'Yii::app()->controller->createUrl("axHandleRoom", array("room_id" => $data["id_session"],"op"=>"update"))',
            'imageUrl' => false,
            'label' => '<span class="btn btn-primary btn-mini"><i class="icon-edit icon-white"></i></span>',
            'options' => array(
                    'class' => 'open-dialog',
                    'title' => Yii::t('standard', '_MOD'),
                    'data-dialog-class' => 'videoconf-room-dialog',
                    'rel' => 'dialog-handle-room',
                    'closeonoverlayclick' => 'false',
                    'closeOnEscape' => 'false'
            ),
    );

    // DELETE button
    $buttonDelete = array(
            'url' => 'Yii::app()->controller->createUrl("axDeleteRoom", array("room_id" => $data["id_session"],"op" => "delete"))',
            'imageUrl' => false,
            'label' => '<span class="btn btn-danger btn-mini"><i class="icon-remove icon-white"></i></span>',
            'options' => array(
                    'class' => 'open-dialog',
                    'title' => Yii::t('standard', '_DEL'),
                    'data-dialog-class' => '',
                    'rel' => 'dialog-handle-room',
                    'closeonoverlayclick' => 'false',
                    'closeOnEscape' => 'false'
            ),
    );

    // ARCHIVE button
    $buttonArchive = array(
            'url' => 'Yii::app()->controller->createUrl("axHandleRoom", array("room_id" => $data["id_session"],"op" => "archive"))',
            'imageUrl' => false,
            'label' => '<span class="btn btn-info btn-mini"><i class="icon-inbox icon-white"></i></span>',
            'options' => array(
                    'class' => 'open-dialog',
                    'title' => 'Move to history list',
                    'data-dialog-class' => '',
                    'rel' => 'dialog-handle-room',
                    'closeonoverlayclick' => 'false',
                    'closeOnEscape' => 'false'
            ),
    );


    // ADD buttons column, depending on Moderator status
    if ($allowAdminOperations) {

        $columnsArrayActive[] = array(
                'class'=>'CButtonColumn',
                'template'=>'{update} {archive} {delete}',
                //'template'=>'{update} {delete}',
                'deleteConfirmation' => true,
                'buttons' => array("update" => $buttonUpdate, "delete" => $buttonDelete, "archive" => $buttonArchive)
        );

        $columnsArrayHistory[] = array(
                'class'=>'CButtonColumn',
                'template'=>'{delete}',
                'deleteConfirmation' => true,
                'buttons' => array("delete" => $buttonDelete)
        );
    }




?>

<!--
<div class="row-fluid"><a href="<?= $backToCourseUrl ?>">&lt;&lt; <?= Yii::t('test', '_TEST_SAVEKEEP_BACK'); ?></a></div>
 -->

<br>

<div class="tabbable">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#conf-rooms-active"data-toggle="tab">Active Meeting Rooms</a></li>
        <?php if ($moderator) : ?>
            <li><a href="#conf-rooms-history" data-toggle="tab"><?= Yii::t('standard', '_HISTORY'); ?></a></li>
        <?php endif; ?>
    </ul>

    <div class="tab-content">

        <!-- ACTIVE -->
        <div class="tab-pane active" id="conf-rooms-active">

            <?php
            $this->widget('zii.widgets.grid.CGridView',
				array(
					'id' => 'conf-grid-view',
					'dataProvider'=>$dataProviderActive,
					'columns' => $columnsArrayActive,
					'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
					'afterAjaxUpdate' => "js: function() { $(document).controls(); jQuery('a[rel=\"popover\"]').popover(); }",  // Dialog2 re-render
				)
            );

            ?>

            <?php if ($allowAdminOperations) : ?>
            <div class="row-fluid">
                <a class="btn btn-primary open-dialog"
                    closeOnOverlayClick="false" closeOnEscape="false"
                    rel="dialog-handle-room" data-dialog-class="videoconf-room-dialog"
                    href="<?php
                        echo Yii::app()->createUrl('videoconference/axHandleRoom', array(
                            "course_id" => $courseModel->idCourse,
                            "op" => "create"
                        )); ?>">
                    <?= Yii::t('player', 'Add room'); ?>
                </a>
            </div>
            <?php endif;?>

        </div>


        <!-- HISTORY -->
        <?php if ($moderator) : ?>
        <div class="tab-pane" id="conf-rooms-history">
            <?php
            $this->widget('zii.widgets.grid.CGridView',
				array(
					'id' => 'conf-grid-view-history',
					'dataProvider'=>$dataProviderHistory,
					'columns' => $columnsArrayHistory,
					'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
					'afterAjaxUpdate' => "js: function() { $(document).controls(); jQuery('a[rel=\"popover\"]').popover();}",   // Dialog2 re-render
				)
            );

            ?>
        </div>
        <?php endif; ?>
    </div>

</div>



<!-- --- JS ------------------------------------------------------------------------------------------------------ -->

<script type="text/javascript">

/*<![CDATA[*/


// DOCUMENT READY
$(function(){

	// Auto close the dialog to handle a room
	$(document).on("dialog2.content-update", "#dialog-handle-room", function() {
	    var e = $(this);
		var autoClose = e.find("a.auto-close");
	    var autoCloseAndRedir = e.find("a.auto-close-and-redirect");

		if (autoClose.length > 0) {
			e.dialog2("close");

		    // Update Grid view
		    $.fn.yiiGridView.update('conf-grid-view');
		    $.fn.yiiGridView.update('conf-grid-view-history');
		}

		// If we found <a class='auto-close-and-redirect></a>
		if (autoCloseAndRedir.length > 0) {
			e.dialog2("close");
			var href = autoCloseAndRedir.attr('href');
			if (href) {
				window.location.replace(href);
			}
			return;
		}


    });



	function dialogError(message) {
		alert(message);
	}


});


/*]]>*/
</script>


