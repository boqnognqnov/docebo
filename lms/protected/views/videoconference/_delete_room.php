<h1><?= Yii::t("standard","_CONFIRM") ?></h1>

<?php

echo CHtml::form('', 'post', array(
	'class' => 'ajax form-horizontal',
	'id' => 'form-create-room'
));
?>

<?php if ($model) : ?>

<?php echo Yii::t('conference', 'Are you sure you want to delete "{name}"', array(
        '{name}' => $model->name
    )); ?>
    <br><br>


<?php echo CHtml::hiddenField('op', $operation); ?>
<?php echo CHtml::hiddenField('room_id', $model->id_session); ?>

<?php else : ?>

<?php echo Yii::t('conference', 'Invalid room'); ?>

<?php endif; ?>

<div class="row-fluid">
    <div style="display: inline-block">
        <?= CHtml::checkBox('deleteConfirmed', false)?>
        <?= CHtml::label(Yii::t('app7020', 'Yes, I want to proceed!'), 'deleteConfirmed', array(
            'style' => 'display: inline; margin-left: 5px'
        )) ?>
    </div>
</div>


<div class="form-actions">
    <input class="btn btn-docebo green big confirm-button hidden" type="submit" name="handle-room-button" id="handle-room-button" value="<?= Yii::t('standard', '_CONFIRM') ?>">
    <input class="btn btn-docebo black big close-dialog" type="button" value="<?= Yii::t('standard', '_CANCEL') ?>">
</div>



<?php echo CHtml::endForm(); ?>


<script type="text/javascript">
    $( document ).ready(function() {

        $('form#form-create-room input').styler();

        $('input[name=deleteConfirmed]').on('change', function(){
            if($(this).is(':checked')){
                $('a.confirm-button').removeClass('hidden');
            } else{
                $('a.confirm-button').addClass('hidden');
            }
        })
    });
</script>