<?php
/**
 * @var $roomModel WebinarSession
 * @var $dateModel WebinarSessionDate
 */
?>
<h1><?= $roomModel->isNewRecord ? Yii::t('conference', 'Add room') : Yii::t('conference', 'Edit room') ?></h1>

<div class="webconf-room-handle">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'web-conf-widget-room',
        'method' => 'POST',
        'htmlOptions' => array(
            'class' => 'ajax docebo-form form-horizontal'
        ),
    ));
    ?>
    <div class="form-group">
        <?php
        echo $form->labelEx($roomModel, 'name');
        echo $form->textField($roomModel, 'name', array(
            'class' => 'form-control',
        ));
        echo $form->error($roomModel, 'name');
        ?>
    </div>
    <br>
    <div class="form-group">
        <?php
        echo $form->labelEx($dateModel, 'webinar_tool');
        if($dateModel->getIsNewRecord())
            echo $form->dropDownList($dateModel, 'webinar_tool', WebinarSession::toolList(), array('class' => 'webinar-dropdown-full', 'id' => 'toolDropdown', 'prompt' => Yii::t('webinar', 'Select webinar tool')));
        else
            echo $form->dropDownList($dateModel, 'webinar_tool', WebinarSession::toolList(), array('disabled'=>'disabled','class' => 'webinar-dropdown-full', 'id' => 'toolDropdown', 'prompt' => Yii::t('webinar', 'Select webinar tool')));
        echo $form->error($dateModel, 'webinar_tool');
        ?>
    </div>
    <br>
    <div class="row-fluid" id="tool_custom" style="display: <?=($roomModel->tool && $roomModel->tool == WebinarSession::TOOL_CUSTOM) ? 'block' : 'none'?>;">
        <div class="span12">
            <div class="clearfix">
                <?php
                echo $form->labelEx($dateModel, 'webinar_custom_url');
                echo $form->textField($dateModel, 'webinar_custom_url', array('class' => 'session-name'));
                echo $form->error($dateModel, 'webinar_custom_url');
                ?>
            </div>
        </div>
    </div>

    <?php if(Yii::app()->event->raise('onBeforeRenderWebinarSessionToolsAccounts', new DEvent($this, array('session' => $roomModel)))): ?>
        <div class="row-fluid" id="tool_selection" style="display: <?=($roomModel->tool && $roomModel->tool != WebinarSession::TOOL_CUSTOM) ? 'block' : 'none'?>;">
            <div class="span12">
                <div class="clearfix tool-account-box">
                    <?php
                    Yii::app()->controller->renderPartial('lms.protected.modules.webinar.views.session._tool_accounts', array('model' => $roomModel));
                    ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <br>
    <div class="row-fluid">
        <div class="span3" data-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>'>
            <?php echo $form->labelEx($dateModel, 'day') ?>
            <div class='input-append date' data-date-format="<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>" data-date-language= "<?= Yii::app()->getLanguage() ?>">
                <?php echo $form->textField($dateModel, 'day', array(
                    'id' => 'day-calendar',
                    'readonly' => 'readonly',
                    'style' => 'width: 55%',
                    'value' => ($dateModel->day ? Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($dateModel->day)) : '')
                )); ?><span class="add-on" style="border:0px;background-color: transparent;"><!--<i class="icon-calendar"></i>--><i class="p-sprite calendar-black-small date-icon" style="height: 30px;width: 30px;"></i></span>
            </div>
            <?php echo $form->error($dateModel, 'day'); ?>
        </div>
        <div class="span3">
            <?php echo $form->labelEx($dateModel, 'time_begin') ?>
            <?php echo $form->dropDownList($dateModel, 'time_begin', WebinarSession::arrayForHours(), array('class' => 'webinar-start-input', 'prompt' => '')); ?>
            <?php echo $form->error($dateModel, 'time_begin'); ?>
        </div>
        <div class="span3" id="coaching-webinar-duration">
            <?php echo $form->labelEx($dateModel, 'duration_minutes') ?>
            <?php echo $form->dropDownList($dateModel, 'duration_minutes', WebinarSession::arrayForDuration(), array('class' => 'webinar-duration-input', 'prompt' => '')); ?> hh:mm
            <?php echo $form->error($dateModel, 'duration_minutes'); ?>
        </div>
        <div class="span3">
            <?php
            echo $form->labelEx($roomModel, 'max_enroll');
            echo $form->textField($roomModel, 'max_enroll', array(
                'class' => 'webconf-max-enroll'
            ));
            echo $form->error($roomModel, 'max_enroll');
            ?>
        </div>
    </div>
    <br><br>
    <div class="row-fluid">
        <div class="span5">
            <p class="webinar-advance-time-label">
                <?=Yii::t('webinar', 'How long would you like to display the "join" button before the webinar session starts?')?>
            </p>
        </div>
        <div class="span7">
            <div class="row-fluid">
                <div class="span6">
                    <p><?=Yii::t('levels', '_LEVEL_6')?>:</p>
                    <?=$form->dropDownList($dateModel, 'join_in_advance_time_teacher', WebinarSession::arrayForDuration(1), array('class'=>'webinar-duration-input'));?>
                    hh:mm
                </div>
                <div class="span6">
                    <p><?=Yii::t('levels', '_LEVEL_3')?>:</p>
                    <?=$form->dropDownList($dateModel, 'join_in_advance_time_user', WebinarSession::arrayForDuration(1), array('class'=>'webinar-duration-input'));?>
                    hh:mm
                </div>
            </div>
            <p class="muted smaller">
                <?=Yii::t('webinar', 'Leave blank to display the button right at the beginning of webinar.')?>
            </p>
        </div>

    </div>
    <div class="form-actions">
        <?= CHtml::submitButton(Yii::t('admin', 'Save'), array(
            'class' => 'btn btn-docebo confirm-save green big'
        )) ?>
        <?= CHtml::button(Yii::t('standard', '_CANCEL'), array(
            'class' => 'btn btn-docebo black big close-dialog'
        )) ?>
    </div>
    <?= $form->hiddenField($roomModel, 'course_id') ?>
<?php $this->endWidget(); ?>
</div>
<style>
    .webconf-room-handle input{
        width: 98%;
    }
    .webconf-room-handle select{
        width: 100%;
    }
    .video-conf-textarea{
        width: 98%;
    }
    .webinar-start-input{
        width: 100px !important;
    }
    .webinar-duration-input{
        width: 75px  !important;
    }
    .webconf-max-enroll{
        width: 60px !important;
    }
    .webinar-advance-time-label{
        margin: 0;
        padding-top: 20px;
    }
    .webconf-room-handle input[type="text"]{
        height: 31px;
        line-height: 31px;
        border-left: 1px solid #999999;
        border-top: 1px solid #999999;
        border-bottom: 1px solid #e5e5e5;
        border-right: 1px solid #e5e5e5;
        background: #f9f9f9;
        padding: 0 8px;
        color: #333333;
    }
    .webconf-room-handle select {
        outline: none !important;
        border-radius: 0;
        margin: 0;
        border-radius: 0;
        padding: 4px;
        box-shadow: none;
        /*     width: 205px; */
        height: 31px;
        line-height: 31px;
        border-left: 1px solid #999999;
        border-top: 1px solid #999999;
        border-bottom: 1px solid #e5e5e5;
        border-right: 1px solid #e5e5e5;
        background: #f9f9f9;
    }
</style>
<script type="application/javascript">
    $(function(){

        $('.input-append.date').bdatepicker({
            autoclose: true
        });

        $('.webconf-room-handle input').styler();

        $('.date-icon').on('click', function (){
            $(this).prev().focus();
        })

//        $('#toolDropdown').on('change', function(){
//            var tool = $(this).val();
//            var htmlData = '';
//
//            if(tool == 'custom'){
//                htmlData = '<label for="WebinarSession_custom_url"><?//= CHtml::activeLabelEx($roomModel, 'custom_url') ?>//</label><input type="text" name="WebinarSession[custom_url]" id="WebinarSession_custom_url">';
//                $('#tool-account').html(htmlData);
//                $('#tool-account').show();
//                return;
//            } else{
//                htmlData = '<label for="WebinarSession_id_tool_account"><?//= CHtml::activeLabelEx($roomModel, 'id_tool_account') ?>//</label><select name="WebinarSession[id_tool_account]" id="WebinarSession_id_tool_account"><option value="0">Please select...</option>';
//            }
//
//            $.ajax({
//                method: 'POST',
//                url: HTTP_HOST + 'index.php?r=videoconference/axGetToolAccounts',
//                data: {
//                    tool: tool
//                }
//            })
//            .done(function(data){
//                if(data && tool != 'custom'){
//                    var list = JSON.parse(data);
//
//                    for(var l in list){
//                        htmlData += '<option value="' + l + '">' + list[l] + '</option>';
//                    }
//                }
//                if(tool != 'custom'){
//                    htmlData += '</select>';
//                }
//                $('#tool-account').html(htmlData);
//                $('#tool-account').show();
//            });
//        })

        var showToolSettings = function(){
            var selectedTool = $("#toolDropdown").val();

            if (selectedTool == '<?=WebinarSession::TOOL_CUSTOM?>')
            {
                $("#tool_custom").show();
                $("#tool_selection").hide();
            }
            else if(selectedTool.length===0){
                $("#tool_custom").hide();
                $("#tool_selection").hide();
            }else{
                loadToolAccounts(selectedTool);
                $("#tool_custom").hide();
                $("#tool_selection").show();
            }
        };

        $("#toolDropdown").change(function(){
            showToolSettings();
        });
        showToolSettings();

        function loadToolAccounts(tool)
        {
            var session = '<?= $roomModel->id_session ?>';
            var date = '<?= $dateModel->day ?>';//$('#day-calendar').val();
            var time = $("#webinar-start-time").val();
            var duration = $("#webinar-duration-time").val();
            $.ajax({
                type: "POST",
                data: {'date': date, 'time': time, 'duration': duration, 'session': session },
                url: "<?=Docebo::createLmsUrl('webinar/session/getAccountsByTool')?>&tool=" + tool,
                context: document.body
            }).done(function(data) {
                $('.tool-account-box').html(data);
            });
        }

        $(".coaching-webinar .date-icon").on('click', function (ev) {
            $(this).prev('input').bdatepicker({
                autoclose: true
            });
        });
    });
</script>
