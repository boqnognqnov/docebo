<?php
$actionUrl = Yii::app()->createUrl('videoconference/axHandleRoom');

echo CHtml::form($actionUrl, 'post', array(
	'class' => 'ajax form-horizontal',
	'id' => 'form-archive-room'
));
?>

<h1><?= Yii::t("standard","_CONFIRM") ?></h1>

<?php if ($model) : ?>

<p>Are you sure you want to move this room to the history list?</p>

<p>Moving to history list will make it inactive and invisible to other learners</p>


<?php echo CHtml::hiddenField('op', $operation); ?>
<?php echo CHtml::hiddenField('archiveConfirmed', '1'); ?>
<?php echo CHtml::hiddenField('room_id', $model->id); ?>

<?php else : ?>

<?php echo Yii::t('conference', 'Invalid room'); ?>

<?php endif; ?>


<div class="form-actions">
    <input class="btn btn-docebo green big" type="submit" name="handle-room-button" id="handle-room-button" value="<?= Yii::t('standard','_CONFIRM'); ?>">
    <input class="btn btn-docebo black big close-dialog" type="button" value="<?= Yii::t('standard', '_CANCEL') ?>">
</div>



<?php echo CHtml::endForm(); ?>