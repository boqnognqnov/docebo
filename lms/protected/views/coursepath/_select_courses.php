<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'classroom-sessions-grid-form',
	'method' => 'POST',
	'htmlOptions' => array(
		'class' => 'docebo-form ajax'
	),
)); ?>

<?php $this->widget('DoceboCListView', array(
	'id' => 'classroom-sessions-list',
	'htmlOptions' => array('class' => 'docebo-list-view'),
	'template' => '{header}{items}{pager}',
	'pager' => array(
		'class' => 'adminExt.local.pagers.DoceboLinkPager',
	),
	'afterAjaxUpdate'	=> "function(id,data) {
					$(':radio').styler();
            		$('a[rel=\"tooltip\"]').tooltip();
				}",

	'dataProvider' => $dataProvider,
	'itemView' => '_courseListView',
	'columns' => array(
		array(
			'name' => 'id',
		),
		array(
			'name' => 'name',
			'header' => Yii::t('standard', '_NAME')
		),
		array(
			'name' => 'toggle_handle',
		)
	)
)); ?>

	<div class="form-actions text-right">
		<input class="btn-docebo green big" type="submit" value="<?php echo Yii::t('standard', '_SUBSCRIBE'); ?>" />
		<input type="reset" class="close-dialog btn-docebo black big" value="<?php echo Yii::t('standard', '_CANCEL'); ?>" />
	</div>

<?php $this->endWidget(); ?>