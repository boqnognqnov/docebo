<?php
/* @var $model LearningCoursepath */
/* @var $this CoursepathController */

$currencySymbol = Settings::get("currency_symbol");
?>

<h2><?=$model->path_name?></h2>

<hr/>

<div class="row-fluid curricula-details type-lp">
	<div class="span3">
		<!-- Left block start -->

		<?php if($model->getLogo()): ?>
			<p>
				<img src="<?=$model->getLogo()?>"/>
			</p>
		<?php endif; ?>

		<br/>

		<div class="course-count-holder" style="background: #F1F3F2; padding: 15px;">
			<p>
				<b><?=count($model->learningCourse)?></b>
			</p>
			<p><?=Yii::t('coursepath', 'Total number of courses')?></p>
		</div>
		<!-- Left block end -->
	</div>
	<div class="span9">
		<!-- Right block start -->
		<?php if($model->path_descr): ?>
			<div class="native-styled">
				<?=Yii::app()->htmlpurifier->purify($model->path_descr)?>
			</div>
		<?php endif; ?>

		<?php
		$dataproviderModel = new LearningCoursepathCourses();
		$dataproviderModel->id_path = $model->id_path;
		$dataProvider = $dataproviderModel->dataProvider();

		$columns = array();

		if(($model->is_selling == 1) && ($model->purchase_type == LearningCoursepath::PURCHASE_TYPE_SINGLE_COURSES_IN_SEQUENCE)) {
			$columns[] = array(
				'id' => 'course-check-to-buy',
				'value' => array($this, 'renderExternalCatalogCheckbox'),
				'type'=>'raw'
			);
		}

		$columns[] = array(
			'name' => 'course_name',
			'value' => '$data->idItem->name',
			'type' => 'raw',
		);
		$columns[] = array(
			'name' => 'course_type',
			'header' => Yii::t('standard', '_TYPE'),
			'value' => '$data->idItem->course_type',
			'type' => 'raw',
		);
		$columns[] = array(
			'name' => 'details',
			'header' => Yii::t('standard', '_DETAILS'),
			'value' => array($this, 'renderDetailsIconColumn'),
			'type' => 'html',
			'headerHtmlOptions'=>array(
				'style'=> null,
			),
			'htmlOptions' => array('style'=>$model->purchase_type == LearningCoursepath::PURCHASE_TYPE_FULL_PACK_ONLY ? 'max-width: 10px;' : null)
		);

		if(($model->is_selling == 1) && ($model->purchase_type == LearningCoursepath::PURCHASE_TYPE_SINGLE_COURSES_IN_SEQUENCE)) {
			$columns[] = array(
				'name' => 'price',
				'header' => Yii::t('transaction', '_PRICE'),
				'headerHtmlOptions'=>array(
					'style'=>'max-width: 20px;'
				),
				'value' => '!LearningCourseuser::isSubscribed(Yii::app()->user->getIdst(), $data->idItem->idCourse) ?
									(PluginManager::isPluginActive("EcommerceApp") && $data->idItem->selling
									? "<span class=\"btn-docebo black full text-center\">".Settings::get("currency_symbol").$data->idItem->getCoursePrice()."</span>"
									: "<span class=\"btn-docebo green full text-center\">Free</span>")
								: null',
				'type' => 'raw',
				'htmlOptions' => array('style'=>'max-width:20px')
			);
		}

		?>
		<?php $this->widget('DoceboSelGridView', array(
			'id' => 'select-courses-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'dataProvider' => $dataProvider,
			'beforeAjaxUpdate' => 'function(id, options) {
			options.type = "POST";
			if (options.data == undefined)
				options.data = {}

			options.data.contentType = "html";
			options.data.selectedItems = $(\'.modal.in\').find(\'#\'+id+\'-selected-items-list\').val();
		}',
			'ajaxUpdate' => 'select-user-grid-all-items',
			'afterAjaxUpdate' => 'function(id, data){
				$("#select-courses-grid input").styler(); afterGridViewUpdate(id);
			}',
			'columns' => $columns,
			'template' => '{items}{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
		)); ?>

		<?php if($model->is_selling == 1) : ?>
		<?php
			$lpFullPackPrice = $model->getCoursepathPrice();
			if ($model->purchase_type == LearningCoursepath::PURCHASE_TYPE_SINGLE_COURSES_IN_SEQUENCE) {
				$lpFullPackPrice = 0;
			}
		?>
		<div class="price-row">
			<?php if($model->purchase_type == LearningCoursepath::PURCHASE_TYPE_SINGLE_COURSES_IN_SEQUENCE): ?>
				<div class="pull-left">
					<?=Yii::t('coursepath', 'You have selected')?>
					<b><?=Yii::t('coursepath', '<span class="count">0</span> course')?></b>
				</div>
			<?php endif; ?>
			<div class="pull-right total-price-container">
				<?=Yii::t('billing', '_AMOUNT')?>:
				<span class="total-price">
					<?= $currencySymbol ?>
					<span class="select-total-sum" data-coursepath-price="<?=$lpFullPackPrice?>">
						<?=number_format($lpFullPackPrice, 2, '.', ' ')?>
					</span>
				</span>
			</div>
			<div class="clearfix"></div>
		</div>
		<?php endif; ?>

		<br/>

		<?php if($model->visible_in_catalog) : ?>
			<div class="pull-right">
				<?php if($model->is_selling == 1) : ?>
					<?=CHtml::button(Yii::t('order', "Buy"), array('class'=>'btn btn-docebo green big axBuyLPSelectionNow'))?>
				<?php else: ?>
					<?=CHtml::button(Yii::t('standard', "_SUBSCRIBE"), array('class'=>'btn btn-docebo green big axLPSubscribeNow'))?>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<!-- Right block end -->
	</div>
</div>

<div id="login-dialog-container" style="display: none;"></div>

<script>
	$(function(){

		<?php if($model->is_selling == 1): ?>
		$('.axBuyLPSelectionNow').click(function(){

			<?
				// shopify link on shopify platform enabled
                if (PluginManager::isPluginActive('ShopifyApp'))
                {
					$shopifySubdomain = Settings::get('shopify_subdomain', false);
					$shopifyProductMeaningfulId =
						LearningCoursepathShopifyProduct::model()->find('id_path = :id_path', [':id_path' => $model->id_path])->shopifyProductMeaningfulId;
					$buyRedirectUrl = "http://$shopifySubdomain.myshopify.com/products/$shopifyProductMeaningfulId";
					echo "window.open('$buyRedirectUrl', '_blank'); return; ";
                }
            ?>

			var selectedCheckboxes = $('.course-check-to-buy:checked')

			var coursesArr = [];
			selectedCheckboxes.each(function(){
				coursesArr[coursesArr.length] = $(this).data('idcourse');
			});

			$.ajax('<?=Docebo::createLmsUrl('coursepath/buy')?>', {
				data: {'courses': coursesArr, 'id': <?=$model->id_path?>},
				cache: false,
				success: function(data, status, xhr){
					if(data.success){
						<?php if(Yii::app()->user->getIsGuest()): ?>
							// Show login dialog
							// which will redirect to the cart on login
							$('#login-dialog-container').load("<?=Docebo::createLmsUrl('user/axLogin', array('reason'=>'buy', 'redirect_url'=>Docebo::createLmsUrl('cart/index')))?>", function(){
								// Login dialog loaded, show it
								$("#login-dialog-container").dialog2({
									showCloseHandle: true,
									removeOnClose: false,
									autoOpen: true,
									closeOnEscape: true,
									closeOnOverlayClick: true
								});
							});
						<?php else: ?>
							window.location.replace('<?=Docebo::createLmsUrl('cart/index')?>');
						<?php endif; ?>
					}
				},
				dataType: 'json',
				type: 'POST'
			});
		});

		$('.course-check-to-buy').styler();

		var updateTotalSelectedCoursesCount = function(){
			var count = $('.course-check-to-buy:checked').length;
			$('.price-row .count').html(count);
		};

		var updateTotalPrice = function(){
			<?php if($model->purchase_type == LearningCoursepath::PURCHASE_TYPE_SINGLE_COURSES_IN_SEQUENCE && $model->price): ?>
				// If all courses are selected and the Learning Plan has a fullpack
				// price set, use that fullpack price
				if($('input.course-check-to-buy:checked').length === <?=count($model->learningCourse)?>){
					$('.select-total-sum').html('<?=$model->getCoursepathPrice()?>');
					return;
				}
			<?php endif; ?>

			var calculatedPrice = 0;
			$('.course-check-to-buy:checked').each(function(){
				var price = parseFloat($(this).data('price'));
				calculatedPrice = calculatedPrice + (!isNaN(price) ? price : 0);
			});

			$('.select-total-sum').html(calculatedPrice.toFixed(2));
		}

		$('.course-check-to-buy').change(function(){
			updateTotalSelectedCoursesCount();
			updateTotalPrice();

		});

		<?php else: ?>
		<?php $enrollUrl = Docebo::createLmsUrl('coursepath/subscribe', array('id' => $model->id_path)); ?>
		$('.axLPSubscribeNow').click(function() {
			<?php if(Yii::app()->user->getIsGuest()): ?>
			// Show login dialog which will redirect to the cart on login
			$('#login-dialog-container').load("<?=Docebo::createLmsUrl('user/axLogin', array('reason'=>'subscribe', 'redirect_url'=> $enrollUrl))?>", function(){
				// Login dialog loaded, show it
				$("#login-dialog-container").dialog2({
					showCloseHandle: true,
					removeOnClose: false,
					autoOpen: true,
					closeOnEscape: true,
					closeOnOverlayClick: true
				});
			});
			<?php else: ?>
			window.location.replace("<?=$enrollUrl?>");
			<?php endif; ?>
		});
		<?php endif; ?>
	});
</script>
