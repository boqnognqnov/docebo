<?php
/* @var $model LearningCoursepath */
/* @var $id_path int */

	$courseDetailsCss =  '';
	$courseDetailsUrl = $this->createUrl('coursepath/details', array('id_path' => $model->id_path));

    $currencySymbol = Settings::get("currency_symbol");

    $params = array();
    $params['id_path'] = $model->id_path;
    if ($resetToken) {
    	$params['reset'] = $resetToken;
    }
        
    $coursePlayerUrl = $this->createUrl("coursepath/details", $params);

	$userEnrolledInLp = in_array($model->id_path, LearningCoursepathUser::getEnrolledLearningPlanIdsByUser($id_user));

	$params = array();
	$params['sop'] = 'unregistercourse';
	if ($resetToken) {
		$params['reset'] = $resetToken;
	}
	if($userEnrolledInLp){
		$courseDetailsUrl = Docebo::createLmsUrl('curricula/show', $params);
	}
?>

<div class="course-action">
	<?php if($userEnrolledInLp): ?>
		<a href="<?= $courseDetailsUrl ?>" class="play-btn"></a>
	<?php elseif($model->is_selling == 1): ?>
		<a title="<?=$model->path_name?>" href="<?= $courseDetailsUrl ?>" data-dialog-class="course-details-modal" closeOnEscape="false" rel="dialog-course-detail-<?= $model->id_path ?>"  class="<?= $courseDetailsCss ?> subscribe-now-btn btn docebo nice-btn with-icon rounded blue"><?=$currencySymbol?>&nbsp;<?=$model->getCoursepathPrice(); ?> &nbsp;&nbsp;<?= Yii::t('userlimit', 'Buy now') ?> <span class="icon"><i></i></span></a>
	<?php else: ?>
		<a title="<?=$model->path_name?>" href="<?= $courseDetailsUrl ?>" data-dialog-class="course-details-modal" closeOnEscape="false" rel="dialog-course-detail-<?= $model->id_path ?>" class="<?= $courseDetailsCss ?> subscribe-now-btn btn docebo nice-btn with-icon rounded green"><?=Yii::t('standard', '_SUBSCRIBE')?><span class="icon"><i></i></span></a>
	<?php endif; ?>
</div>

<div class="course-info">
	<div class="clearfix"></div>
	<div class="clearfix">
		<span class="label course-info-type course-type-sprite elearning"><?php echo Yii::t('standard', '_COURSEPATH'); ?></span>
	</div>
</div>