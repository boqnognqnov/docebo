<?php /* @var $data array  */ ?>
<div class="item">
	<div class="primary row-fluid">
		<div class="item-col col-id">
			<?= CHtml::checkBox('idCourse[]', false, array('value' => $data['idCourse'])) ?>
		</div>
		<div class="item-col col-name text-colored">
			<?= $data['name'] ?>
		</div>
	</div>
</div>