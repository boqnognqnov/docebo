<h1><?= Yii::t('setup', 'Upload your background') ?></h1>

<?php
	// NON AJAX FOR !!!
	$form = $this->beginWidget('CActiveForm', array(
		'method' => 'post',
		'htmlOptions' => array(
			'class' => 'form-inline',
			'id' => 'image-selector-form',	
			'enctype'=>"multipart/form-data",
		)
	));
?>

<div class="imsel-manage-panel">

	<div class="row-fluid">
	
		<!-- TABS (LIST) -->
		<div class="span3">
			<ul class="nav nav-list">
                <?php if($showStandardImages): ?>
				<li class="active">
					<a href="#system-images" data-toggle="tab">
						<?php echo Yii::t('templatemanager', 'Ours images collection'); ?> (<span id="system-images-count"></span>)
					</a>
				</li>
                <?php endif; ?>
                <li <?= $showStandardImages ? '' : 'class="active"' ?>>
					<a href="#user-images" data-toggle="tab">
						<?php echo Yii::t('templatemanager', 'Your images collection'); ?> (<span id="user-images-count"></span>)
					</a>
				</li>
			</ul>
		</div>
		
		
		
		<!-- TABS CONTENT (panes)  -->
		<div class="span9 tab-content">
		
			<!-- TAB PANE 1: System/Default images  -->
            <?php if($showStandardImages): ?>
			<div class="tab-pane active" id="system-images">

			
				<div class="row-fluid">
					<div class="span12 pane-header">
						<div>
							<?php echo Yii::t('branding', 'Select image'); ?>
						</div>
					</div>
				</div>
			
				<div class="row-fluid">
					<div class="span12 text-center">
							<div id="system-images-slider" class="carousel slide">
								<div class="carousel-inner">
									<?php foreach ($systemImages as $chunkIndex => $chunk) : ?>
										<div class="item<?php echo ($chunkIndex == 0) ? ' active' : ''; ?>">
											<?php foreach ($chunk as $imageId => $imageUrl) : ?>
												<div class="sub-item <?= ($imageId == $preselectedId) ? 'checked' : '' ?>">
													<?= CHtml::image($imageUrl) ?>
													<input type="radio" name="<?= $radioName ?>" value="<?= $imageId ?>" <?= $imageId == $preselectedId ? 'checked="checked"' : '' ?> />
													<span class="checkmark"><span class="i-sprite is-check green large"></span></span>
												</div>
											<?php endforeach; ?>
										</div>
									<?php endforeach; ?>
								</div>
								<?php if (count($systemImages) > 0) : ?>
									<a class="carousel-control left" href="#system-images-slider" data-slide="prev">&lsaquo;</a>
									<a class="carousel-control right" href="#system-images-slider" data-slide="next">&rsaquo;</a>
								<?php endif; ?>
							</div>
					</div>
				</div>
			
			</div>
            
            <?php endif; ?>
			
			
			<!-- TAB PANE 2: User uploaded images  -->
			<div class="tab-pane <?= $showStandardImages ? '' : 'active' ?>" id="user-images">
			
				<div class="row-fluid">
					<div class="span12 pane-header">
						<div>
							<?php echo Yii::t('branding', '_UPLOAD_BACKGROUND_BTN'); ?>
						</div>
						<div>
							&nbsp;<?php echo CHtml::fileField('image_manage_file', '', array('id' => 'image-manage-file', 'class' => 'image-manage-file')); ?>
						</div>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12 text-center">
						<div class="clearfix"></div>	
							<div id="user-images-slider" class="carousel slide user-images-slider">
								<!-- Carousel items -->
								<div class="carousel-inner">
									<?php foreach ($userImages as $chunkIndex => $chunk) : ?>
										<div class="item<?php echo ($chunkIndex == 0) ? ' active' : ''; ?>">
											<?php foreach ($chunk as $imageId => $imageUrl) : ?>
												<div class="sub-item <?= ($imageId == $preselectedId) ? 'checked' : '' ?>">
													<?= CHtml::image($imageUrl) ?>
													<input type="radio" name="<?= $radioName ?>" value="<?= $imageId ?>" <?= $imageId == $preselectedId ? 'checked="checked"' : '' ?> />
													<span class="checkmark"><span class="i-sprite is-check green large"></span></span>
													<span class="deleteicon"><span class="i-sprite is-remove red"></span></span>
												</div>
											<?php endforeach; ?>
										</div>
									<?php endforeach; ?>
								</div>
								<!-- Carousel nav -->
								<?php
									$showControls = false;
									$tmpStyle = '';
									if (count($userImages) > 0) {
										$showControls = true;
									}
									if (!$showControls) {
										$tmpStyle = ' style="display:none;" ';
									}
		
								?>
									<a <?= $tmpStyle ?> class="carousel-control left" href="#user-images-slider" data-slide="prev">&lsaquo;</a>
									<a <?= $tmpStyle ?> class="carousel-control right" href="#user-images-slider" data-slide="next">&rsaquo;</a>
							</div>
					</div>
				</div>
		
			</div>
		
		</div>
	
	</div>

</div>	


<!-- For Dialog2 - they go in footer -->
<div class="form-actions">
	<!-- No SUBMIT BUTTON here! We handle 'clicks' and do AJAX form submition -->
	<?= CHtml::button(Yii::t('templatemanager', 'Use image'), array('class' 	=> 'confirm-save btn-docebo green big')); ?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' 	=> 'btn-docebo black big close-dialog')); ?>
</div>

<?php $this->endWidget(); ?>


					
<script type="text/javascript">

	var options = {
		modalId	: <?= json_encode($modalId) ?>
	};								
	$('#' + <?= json_encode($elementId) ?>).imageSelector(options);								


</script>					