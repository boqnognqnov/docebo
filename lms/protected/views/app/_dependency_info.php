<?php
/* @var $app array */
/* @var $title string */
?>
<h1><?= $title ?></h1>

<!-- hack so that the 'close' button wouldn't get focused -->
<a href="#" class="hide"></a>

<p><?= Yii::t('apps', 'This app is required by the following apps and can\'t be removed:') ?>
	<strong><?= implode(', ', $app['dependency_list']) ?></strong>
</p>

<div class="form-actions">
	<button class="btn-docebo black big close-dialog"><?= Yii::t('standard', '_CLOSE') ?></button>
</div>