<?php
/* @var $this AppController */

/* @var bool $message */
/* @var bool $activatedApplication */

?>

<!-- Consumed by Dialog2, used as Dialog Title -->
<h1><?= Yii::t('shopify', 'Activating') . ' ' . $activatedApplication ?></h1>

<?= $message ?>

<div class="checkbox" style="margin-top: 10px">
	<?=CHtml::checkBox('instPolicy', false, array(
		'id' => 'instPolicy',
		'value' => false,
		'class' => 'instPolicy'
	));?>
	<?=CHtml::label(Yii::t('shopify', 'Yes, I want to proceed'), 'instPolicy', array(
		'style' => 'display: inline-block;'
	)); ?>
</div>

<br />
<div class="row-fluid right" style="text-align: right">
	<?= CHtml::button(Yii::t('shopify', 'CONFIRM'), array(
		'class' => 'btn btn-docebo green big',
		'name' => 'submit',
		'onclick' => "if ($('.instPolicy')[$('.instPolicy').length - 1].parentNode.childNodes[1].checked) { confirm() }",
	)); ?>
	<?= CHtml::button(Yii::t('shopify', 'CLOSE'), array(
		'class' => 'btn btn-docebo black big',
		'name' => 'cancel',
		'onclick' => "$('.modal').hide(); $('.modal-backdrop').hide(); ",
	)); ?>
</div>

<script>
	$('.modal-footer').hide();
	$('.instPolicy').styler();

	function confirm() {

		$.ajax({
			url: "index.php?r=app/axConfirmInstall&id=<?= $appId ?>&type=buy&confirmedECommerceShopifyInstPolicy=1"
		})
		.done(function( html ) {
				$('.modal-body')[$('.modal-body').length - 1].innerHTML = html;
				var scriptStartIndex = html.indexOf('script');
				eval(html.substr(scriptStartIndex + 7, html.length - 9 - scriptStartIndex - 7));

		});
	}
</script>