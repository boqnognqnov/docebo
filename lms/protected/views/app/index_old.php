<?php

/**
 * Shows a container, makes AJAX call(s) to load the actual grid.
 * @var $this AppController
 */

$this->breadcrumbs[] = Yii::t('apps', 'Paid apps');
?>
<!-- This is the container for the apps, which will be loaded by an ajax call -->
<div id="" class="tiles-grid"></div>
<div class="ajaxloader" style="display: none;"><?= Yii::t("standard", "_LOADING") ?>...</div>

<!-- --- JS ------------------------------------------------------------------------------------------------------ -->

<script type="text/javascript">
	/*<![CDATA[*/

	var AppsGrid = {
		loadApps: function() {
			// Ajax call to get apps grid upon page load, no filter
			$.ajax({
				url: '<?= Yii::app()->createUrl('app/axAppsGrid') ?>',
				type: 'post', // !! don't change
				dataType: 'json',
				data: null,
				beforeSend: function() {
					$('.ajaxloader').show()
				}
			})
				.done(function(res){
					$('.tiles-grid').html(res.html);

					// Dialog2 Controls. Must be called after every re-load to update new controls
					$(document).controls();
				})
				.always(function(){
					$('.ajaxloader').hide();
				})
				.fail(function(jqXHR, textStatus, errorThrown){
					//console.log(jqXHR);
					//console.log(errorThrown);
					//alert(textStatus);
				});
		},
		refresh: function() {
			AppsGrid.loadApps();
		},
		ready: function() {
			AppsGrid.loadApps();
		}
	};

	//Document ready
	$(function(){
		AppsGrid.ready();
	});

	/*]]>*/
</script>
