<?php
/* @var $app array */
/* @var $title string */

$willExpireText = "";
$remainingDays = Docebo::getTrialRemainingDays();

if ($remainingDays == 0) {
    $willExpireText =  Yii::t('userlimit', 'Your trial will expire today');
}
else if ($remainingDays > 0) {
    $willExpireText = Yii::t('userlimit', 'Your trial will expire in', array('[days]'=>$remainingDays));
}
else {
    $willExpireText =  Yii::t('userlimit', 'Your trial period is expired');
}

if (empty($extra_param)) $extra_param = array();

?>
<h1><?= $title ?></h1>
<br>

<p><?= Yii::t('apps', 'You will be able to <strong>fully use and evaluate all the functions of this app <span
	class="text-colored-orange">until the end of your Docebo trial period.</span></strong>') ?></p>

<a href="<?= Yii::app()->createUrl( !empty($custom_url) ? $custom_url : 'app/axConfirmInstall', array('confirm'=>1, 'id'=>$app['app_id']) + $extra_param) ?>" class="ajax btn btn-docebo green big pull-right span2"><?= Yii::t('apps', 'Start free trial') ?></a>

<small class="trial-expiration-notice"><?= $willExpireText ?></small>

<script type="text/javascript">
	$(document).delegate('.modal', 'dialog2.after-update-markup', function(){
		var e = $(this);
		// get the modal and make it narrower again
		$('#dialog-app-detail').parents('.modal').eq(0).removeClass('wide');
	});
</script>