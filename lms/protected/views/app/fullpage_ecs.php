<?php
/* @var bool $is_trial */
/* @var array $ecsApp */
/* @var bool $hasToContactResellerToInstall - True on wire_transfer paid LMSes */
/* @var string $lmsPaymentMethod - May be credit_card or wire_transfer */
?>
<style type="text/css">
	/* @TODO move these somewhere else */


	#fullpage-ecs .plan-name {
		font-size:      30px;
		font-weight:    bold;
		text-transform: uppercase;
		line-height:    1;
		padding-top:     10px;
	}

	#fullpage-ecs .plan-slogan {
		line-height:    1;
		font-size:      18px;
		font-weight:    bold;
		margin: 0 10px;
		padding-bottom: 10px;
		min-height: 38px;
	}
	#fullpage-ecs .badge-holder{
		position: relative;
		min-height: 120px;
	}
	#fullpage-ecs .badge-holder .upper-gray-triangle{
		position: absolute;
		width: 100%;
		height: 20px;
		margin:0;
		padding:0;
	}
	#fullpage-ecs .cloud-image.non-ecs{
		background: url('<?=Yii::app()->theme->baseUrl?>/images/apps_menu_sprite.png') no-repeat -3px -198px;
		width: 143px;
		height: 99px;
		position: absolute;
	}
	#fullpage-ecs .cloud-image.ecs{
		background: url('<?=Yii::app()->theme->baseUrl?>/images/apps_menu_sprite.png') no-repeat -3px -303px;
		width: 143px;
		height: 104px;
		position: absolute;
	}
	#fullpage-ecs .ecs-pending{
		font-size: 16px;
		font-weight: bold;
		margin-top: 10px;
	}
	#fullpage-ecs .badge-holder .badge-image{
		position: absolute;
	}
	#fullpage-ecs .feature{
		margin: 10px;
	}
	#fullpage-ecs .arrow-down {
		width: 0;
		height: 0;
		border-left: 100px solid transparent;
		border-right: 100px solid transparent;
		border-top: 10px solid #F5F5F5;
	}

	#fullpage-ecs table{
		border-collapse: collapse;
		table-layout: fixed;
		width: 100%;
		overflow: hidden;
	}
	#fullpage-ecs table tr, table td{
		border: none;
		padding:0;
		margin:0;
	}
	#fullpage-ecs table tr td{
		border: 1px solid #E4E6E5;
	}
	#fullpage-ecs tr.header-row td{
		vertical-align: top;
	}

	#fullpage-ecs table tr.feature-row{
		border-top: 1px solid #E4E6E5;
		border-bottom: 1px solid #E4E6E5;;
	}
	#fullpage-ecs table tr.feature-row td{
		border-right: 1px solid #E4E6E5;
	}
	#fullpage-ecs table tr td.noborder{
		border: none;
	}
	.no-tooltip{
		/* Just takes up space equal to the tooltip */
		width: 16px;
		height: 16px;
		margin-left: 5px;
		margin-right: 10px;
		float: right;
	}
	.tooltip-icon{
		background: url('<?=Yii::app()->theme->baseUrl?>/images/apps_menu_sprite.png') no-repeat -108px -57px;
		width: 16px;
		height: 16px;
		display: inline-block;
		margin-left: 5px;
		margin-right: 10px;
		margin-top: -1px;
	}
	#fullpage-ecs .active-plan-icon{
		background: url('<?=Yii::app()->theme->baseUrl?>/images/apps_menu_sprite.png') no-repeat -61px -93px;
		width: 30px;
		height: 30px;
		margin-left: 86px;
	}
	#fullpage-ecs .active-plan-label{
		font-size: 25px;
		text-transform: uppercase;
		color: #5fbf5f;
		font-weight: bold;
		margin-top: 10px;
		margin-bottom: 10px;
	}
	#fullpage-ecs .active-plan-sublabel span{
		color: #5fbf5f;
	}
	#fullpage-ecs .plan-price{
		font-size: 18px;
		margin-top: 20px;
		margin-bottom: 15px;
	}
	#fullpage-ecs .plan-price .green{
		font-size: 35px;
		font-weight: bold;
		color: #5fbf5f;
	}
	#fullpage-ecs .feature.action-button{
		font-size: 13px;
		margin-left: 10px;
		margin-right: 10px;
	}
	#fullpage-ecs .feature.action-button .btn-docebo.full{
		font-size: 14px;
		font-weight: bold;
		text-shadow: 0 1px 0 #000;
	}

	#fullpage-ecs .no-plan-warning{
		margin-top: 20px;
		border: 3px solid #e67e22;
		padding: 10px 15px;
	}
	#fullpage-ecs .no-plan-warning .warning-icon{
		width: 31px;
		height: 30px;
		float: left;
		margin-right: 10px;
	}
	#fullpage-ecs .no-plan-warning p{
		margin: 0 0 0 46px;
	}
	#fullpage-ecs .no-plan-warning p.orange{
		color: #e57e1f;
		font-size: 16px;
		font-weight: bold;
		margin-bottom: 10px;
	}
</style>

<? // var_dump($helpdesk_apps) ?>

<h3 class="cover-title"><?=Yii::t('apps', 'Enterprise Cloud Solution')?></h3>

<p><?=Yii::t('apps', 'Bring your LMS to an <b>Enterprise level</b> by enabling an <b>Isolated Cloud single-tenant instance</b> with additional top-notch services.');?></p>

<p><?=Yii::t('apps', 'Available payment methods: Recurring monthly / yearly by <b>credit card</b>, or recurring yearly by <b>wire transfer</b>.');?></p>

<p>&nbsp;</p>

<table>
<tr class="header-row">
	<td style="border:none">
		<!-- Empty -->
	</td>
	<td>
		<div class="bg-gray">
			<p class="plan-name text-center"><?=Yii::t('apps', 'NOW')?></p>
			<p class="plan-slogan text-center"><?=Yii::t('apps', 'Your Docebo SaaS')?></p>
		</div>
		<div class="badge-holder">
			<div class="upper-gray-triangle">
				<div class="arrow-down"></div>
			</div>
			<div class="cloud-image non-ecs autoposition horizontal"></div>
		</div>
	</td>
	<td>
		<div class="bg-gray">
			<p class="plan-name text-center blue-text"><?=Yii::t('apps', 'Extend')?></p>
			<p class="plan-slogan text-center"><?=Yii::t('apps', 'your Docebo SaaS with the <br/><span class="blue-text">{ecs_app} App</span>', array('{ecs_app}'=>Yii::t('apps', 'Enterprise Cloud Solution')))?></p>
		</div>
		<div class="badge-holder">
			<div class="upper-gray-triangle">
				<div class="arrow-down"></div>
			</div>
			<div class="cloud-image ecs autoposition horizontal"></div>
		</div>
	</td>
</tr>

<tr class="feature-row bg-gray">
	<td>
		<div class="text-right">
			<div class="pull-right no-tooltip">&nbsp;</div>
			<b><?=Yii::t('apps', 'Infrastructure')?></b>
		</div>
	</td>

	<td>
		<p class="text-center nomargin feature">
			<b><?=Yii::t('apps', 'Shared multi-tenant')?></b>
		</p>
	</td>

	<td>
		<p class="text-center nomargin feature">
			<?=Yii::t('apps', '<b>Isolated Cloud</b> LMS instance<br/><b>Dedicated</b> and <b>Auto Scalable</b>')?>
		</p>
	</td>
</tr>

<tr class="feature-row">
	<td>
		<div class="text-right">
			<a class="pull-right" rel="tooltip" title="<?=Yii::t('apps', 'Incident registration, ticket number assignment to the client, inclusion of the request in the Help Desk system.')?>"><span class="tooltip-icon"></span></a>
			<b><?=Yii::t('userlimit', 'Help Desk')?></b>
		</div>
	</td>
	<td>
		<div class="text-center feature">
			<?=Yii::t('apps', 'SILVER')?>
		</div>
	</td>
	<td>
		<div class="text-center feature">
			<b><?=Yii::t('apps', 'GOLD')?></b>
		</div>
	</td>
</tr>

<tr class="feature-row bg-gray">
	<td>
		<div class="text-right">
			<div class="no-tooltip pull-right">&nbsp;</div>
			<b><?=Yii::t('apps', 'Minor Upgrades &amp; Releases')?></b>
		</div>
	</td>
	<td>
		<div class="text-center feature">
			<?=Yii::t('apps', '<b>Auto provisioned</b> and <b>automatically deployed</b>')?>
		</div>
	</td>
	<td>
		<div class="text-center feature">
			<?=Yii::t('apps', '<b>Auto provisioned</b> and <b>automatically deployed</b>')?>
		</div>
	</td>
</tr>

<tr class="feature-row">
	<td>
		<div class="text-right">
			<div class="no-tooltip">&nbsp;</div>
			<b><?=Yii::t('apps', 'Major Upgrades &amp; Releases')?></b>
		</div>
	</td>
	<td>
		<div class="text-center feature">
			<?=Yii::t('apps', '<b>Auto provisioned</b> and <b>automatically deployed</b>')?>
		</div>
	</td>
	<td>
		<div class="text-center feature">
			<?=Yii::t('apps', '<b>Agreed upon</b> and <b>scheduled</b> with the customer')?>
		</div>
	</td>
</tr>

<tr class="feature-row bg-gray">
	<td>
		<div class="text-right">
			<div class="no-tooltip pull-right">&nbsp;</div>
			<b><?=Yii::t('apps', 'Custom developments')?></b>
		</div>
	</td>
	<td>
		<div class="text-center feature">
			-
		</div>
	</td>
	<td>
		<div class="text-center feature">
			<b><?=Yii::t('apps', '<b>Fully open to custom features development</b>')?></b>
		</div>
	</td>
</tr>

<tr class="feature-row">
	<td>
		<div class="text-right">
			<div class="no-tooltip">&nbsp;</div>
			<b><?=Yii::t('apps', 'Paid Apps included complimentary')?></b>
		</div>
	</td>
	<td>
		<div class="text-center feature">
			-
		</div>
	</td>
	<td>
		<div class="text-center feature">
			<?=Yii::t('apps', 'Custom Domain, HTTPS, E-Commerce, White-labeling')?>
		</div>
	</td>
</tr>

<tr class="feature-row bg-gray">
	<td>
		<div class="text-right">
			<div class="no-tooltip pull-right">&nbsp;</div>
			<b><?=Yii::t('apps', 'Uptime')?></b>
		</div>
	</td>
	<td>
		<div class="text-center feature">
			99.50 %
		</div>
	</td>
	<td>
		<div class="text-center feature">
			<b>99.80 %</b>
		</div>
	</td>
</tr>

<tr class="prices-row">
	<td class="noborder">
		<!-- Empty -->
	</td>
	<td class="text-center">
		<div class="text-center feature action-button">
			<? if($ecsApp['is_installed']): ?>
				<p><img class="warning-icon" src="<?=Yii::app()->theme->baseUrl;?>/images/warning-orange.png" /></p>
				<p><b><?=Yii::t('apps', 'The App can not be disabled unless you completely cancel your subscription plan.')?></b></p>
			<? else: // If ECS is disabled, this means the standard version (this block) is enabled ?>
				<div class="active-plan-icon autoposition horizontal"></div>
				<div class="active-plan-label"><?=Yii::t('standard', '_ACTIVE')?></div>
			<? endif; ?>
		</div>
	</td>
	<td class="text-center">
		<div class="text-center feature action-button">
			<? if(!$ecsApp['is_installed']): ?>
				<? if($hasToContactResellerToInstall || !$priceIsSet): ?>
					<p><?=Yii::t('apps', 'For more information')?></p>
					<p><a class="open-dialog btn-docebo blue full bigpadding" data-dialog-class="metro small app-details" rel="newdialog" href="<?= Yii::app()->createUrl('site/axContactSalesTrialExpired', array('app_id' => $app['app_id'], 'ref' => 'enterpriseAppDetails')) ?>"><?=Yii::t('apps', 'Contact Us Now')?></a></p>
				<? else: ?>
					<div class="plan-price">
						<?=$ecsApp['pricing']['currency']=='eur' ? '&euro;' : '&dollar;'; ?>
						<span class="green"><?=number_format($ecsApp['pricing']['price'], 2)?></span> + VAT/<?=$ecsApp['pricing']['payment_cycle']=='monthly' ? 'mo' : 'yr';?>
					</div>
					<p><a class="open-dialog btn-docebo green full bigpadding" href="<?= Yii::app()->createUrl('app/axAppDetails', array('id' => $ecsApp['app_id'])) ?>" data-dialog-class="metro wide app-details" rel="dialog-app-detail" title="<?= Yii::t('userlimit', 'Buy now') ?>"><?=Yii::t('apps', 'Buy now the {app} app', array('{app}'=>Yii::t('apps', 'Enterprise Cloud Solution')))?></a></p>
				<? endif; ?>
			<? else: // App is installed, but has two states: pending and active ?>
				<? if(!isset($ecsApp['pending_enterprise']) || $ecsApp['pending_enterprise']==1): // Installation pending ?>
					<div class="active-plan-icon autoposition horizontal">&nbsp;</div>
					<p class="text-center green-text ecs-pending"><?=Yii::t('apps', 'Your activation request is currently being processed')?></p>
					<p class="text-center"><?=Yii::t('apps', 'You will be notified as soon as this app is active')?></p>
				<? else: // fully installed and active ?>
					<div class="active-plan-icon autoposition horizontal">&nbsp;</div>
					<div class="active-plan-label"><?=Yii::t('standard', '_ACTIVE')?></div>
				<? endif; ?>
			<? endif; ?>
		</div>
	</td>
</tr>

<? if(!$ecsApp['is_installed']): ?>
	<tr>
		<td class="noborder"></td>
		<td colspan="2" class="noborder">
			<div class="no-plan-warning">
				<img class="warning-icon" src="<?=Yii::app()->theme->baseUrl;?>/images/warning-orange.png" />

				<? if(!$planIsGood): ?>
					<p class="orange"><?=Yii::t('apps', 'In order to enable the Enterprise Cloud Solution you must have a 500 users (or higher) subscription plan.')?></p>
				<? endif; ?>

				<p><?=Yii::t('apps', "You'll then be able to upgrade or downgrade your plan only choosing a plan among the 500 and the 20.000 ones. <b>The App can't be disabled unless you completely cancel your subscription plan.</b>")?></p>
			</div>
		</td>
	</tr>
<? endif; ?>
</table>

<? //var_dump($ecsApp) ?>