<?php
/* @var $this AppController */

/* @var array $apps */
/* @var string $coverTile */
/* @var string $backCoverTile */

	$this->widget('common.widgets.AppsGrid', array('apps' => $apps, 'coverTile' => $coverTile, 'backCoverTile' => $backCoverTile));
?>