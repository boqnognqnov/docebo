<?php
/* @var $this AppController */

/* @var array $app */
/* @var array $lang */
/* @var bool $hasToContactResellerToInstall */
/* @var bool $isInTrialPeriod */

$_currency = '';
switch ($app['pricing']['currency']) {
	case 'usd': $_currency = '&dollar;'; break;
	case 'eur':
	default: $_currency = '&euro;'; break;
}

$isEnterprise = $app['is_enterprise']; // Is this Cloud Enterprise App?

// If the LMS subscription plan is higher than 500 users
$planIsGood = (!$isInTrialPeriod && $isEnterprise && Settings::get('max_users', 0) >= 500);
?>

<!-- Consumed by Dialog2, used as Dialog Title -->
<h1><?= $lang['title'] ?></h1>

<div class="app-details-dialog">
	<div class="pull-left thumb-area">
		<div class="app-title"><?= $lang['title'] ?></div>

		<?php
		// Try to get the app's image from the collection (Why do we have an array here?)
		$appImage = false;
		if(is_array($app['image'])){
			$imgUrls = array_values($app['image']);
			$appImage = $imgUrls[0];
		}else{
			$appImage = $app['image'];
		}
		?>
		<?php if($appImage): ?>
			<img alt="<?=$lang['title']?>" class="app-image" src="<?=$appImage?>">
		<?php endif; ?>


		<?php if($isFree): ?>
			<p><a href="<?= Yii::app()->createUrl('app/axConfirmInstall', array('id' => $app['app_id'], 'type' => 'install')) ?>" class="ajax btn-docebo blue full big text-center"><?= Yii::t('apps', 'Installnow') ?></a></p>
		<?php else: ?>
			<?php if($useContactus): ?>
				<?php if($enableUserBilling == 'on'): ?>
					<?php if(strtolower($app['internal_codename']) != 'salesforce'){ ?>
						<p class="payment-cycle">
							<?= Yii::t('app', 'Recurring {cycle} by <b>{pay_method}</b>', array(
								'{cycle}' => $cycle=='MONTH' ? 'Monthly' : 'Yearly',
								'{pay_method}' => $app['lms_payment_method']=='credit_card' ? Yii::t('order', 'Credit Card') : Yii::t('order', 'Wire transfer'),
							)); ?>
						</p>
					<?php } ?>
					<div class="contact-us-btn-area">
						<p><a class="contact-us-links btn-docebo blue big full text-center" href="javascript:;"><?=Yii::t('apps', 'Contact Us Now')?></a></p>
					</div>
				<?php else: ?>
					<?php if($isReseller): ?>
						<p class="payment-cycle">
							<?= Yii::t('app', 'Recurring {cycle} by <b>{pay_method}</b>', array(
								'{cycle}' => $cycle=='MONTH' ? 'Monthly' : 'Yearly',
								'{pay_method}' => $app['lms_payment_method']=='credit_card' ? Yii::t('order', 'Credit Card') : Yii::t('order', 'Wire transfer'),
							)); ?>
						</p>
						<div class="contact-us-btn-area">
							<p><a class="contact-us-links btn-docebo blue big full text-center" href="javascript:;"><?=Yii::t('apps', 'Contact Us Now')?></a></p>
						</div>
					<?php else: ?>
						<div class="discover-more">
							<p><?= Yii::t('app', '<strong>Contact Your Docebo Partner / Reseller</strong> for more information') ?></p>
						</div>
					<?php endif; ?>
				<?php endif; ?>
			<?php else: ?>
				<?php if($isInTrialPeriod): ?>
					<div class="discover-more">
					<p class="dimmed"><?= Yii::t('apps', '{currency} {price} + VAT', array(
							// A hacky way to reuse a old string/i18n:
							'{currency}'=>'<span class="price">'.$_currency,
							'{price}'=>$app['pricing']['price'].'</span>',
							'<strong>'=>'',
							'</strong>'=>'',
						))?>/<?= ($app['market_type']=='subscription') ? strtolower($cycle) : ucwords(strtolower('ONE TIME PAYMENT')) ?></p>
					<?php if(!$app['is_enterprise']): ?>
						<p class="free-trial-days">
							<?=Yii::t('standard', '{days}-day Free Trial', array('{days}'=>Docebo::getTrialRemainingDays()));?>
						</p>
					<?php endif; ?>
						</div>
					<p><a href="<?= Yii::app()->createUrl('app/axConfirmInstall', array('id' => $app['app_id'], 'type' => 'trial')) ?>" class="ajax btn-docebo orange full big text-center"><?= Yii::t('apps', 'Startfreetrial') ?></a></p>
				<?php else: ?>
					<?php if(!$app['free_on_premium'] || !$isECSInstallation): ?>
						<?php if($enableUserBilling == 'on'): ?>
							<?php if($app['lms_payment_method'] == 'credit_card'): ?>
								<p class="payment-cycle">
									<?= Yii::t('app', 'Recurring {cycle} by <b>{pay_method}</b>', array(
										'{cycle}' => $cycle=='MONTH' ? 'Monthly' : 'Yearly',
										'{pay_method}' => $app['lms_payment_method']=='credit_card' ? Yii::t('order', 'Credit Card') : Yii::t('order', 'Wire transfer'),
									)); ?>
								</p>
								<div class="price-holder">
									<?= Yii::t('apps', '<span class="price">{currency} {price}</span> + VAT', array('{currency}'=>$_currency,'{price}'=>$app['pricing']['price'])) ?>/<?= ($app['market_type']=='subscription') ? strtolower($cycle) : ucwords(strtolower('ONE TIME PAYMENT')) ?>
								</div>
								<p><a href="<?=Yii::app()->createUrl('app/axConfirmInstall', array('id' => $app['app_id'], 'type' => 'buy')) ?>" class="ajax btn-docebo green full big text-center" data-dialog-class="wider"><?= Yii::t('userlimit', 'Buy now') ?></a></p>
							<?php else: ?>
								<div class="discover-more">
									<p class="wirecard-notice">
										<?=Yii::t('apps', 'To buy this app with your plan you must <span>contact your sales representative</span>')?>
									</p>
								</div>
								<div class="contact-us-btn-area">
									<p><a class="contact-us-links btn-docebo blue big full text-center" href="javascript:;"><?=Yii::t('apps', 'Contact Us Now')?></a></p>
								</div>
							<?php endif; ?>
						<?php else: ?>
							<?php if($isReseller): ?>
								<p class="payment-cycle">
									<?= Yii::t('app', 'Recurring {cycle} by <b>{pay_method}</b>', array(
										'{cycle}' => $cycle=='MONTH' ? 'Monthly' : 'Yearly',
										'{pay_method}' => $app['lms_payment_method']=='credit_card' ? Yii::t('order', 'Credit Card') : Yii::t('order', 'Wire transfer'),
									)); ?>
								</p>
								<div class="contact-us-btn-area">
									<p><a class="contact-us-links btn-docebo blue big full text-center" href="javascript:;"><?=Yii::t('apps', 'Contact Us Now')?></a></p>
								</div>
							<?php else: ?>
								<div class="discover-more">
									<p><?= Yii::t('app', '<strong>Contact Your Docebo Partner / Reseller</strong> for more information') ?></p>
								</div>
							<?php endif; ?>
						<?php endif; ?>
					<?php else: ?>
						<p><a href="<?= Yii::app()->createUrl('app/axConfirmInstall', array('id' => $app['app_id'], 'type' => 'install')) ?>" class="ajax btn-docebo blue full big text-center"><?= Yii::t('apps', 'Installnow') ?></a></p>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>
		<?php endif; ?>
		</div>
		<div class="description-holder <?=$isEnterprise ? 'enterprise' : ''?>">

			<?php if($isEnterprise
				// Only show this block if the Enterprise app is NOT in "pending activation" state
				&& (!isset($app['pending_enterprise']) || !$app['pending_enterprise'])): ?>
				<div class="cloud-enterprise-warning">
					<img class="warning pull-left" src="<?=Yii::app()->theme->baseUrl?>/images/warning-orange.png" alt="" />
					<div class="warning-text">
						<?php if(!$planIsGood): ?>
							<h4><?=Yii::t('app', 'You must have a 500 users or more subscription in order to activate this app.')?></h4>
						<?php endif; ?>

						<p><?=Yii::t('app', 'Once activated, this app <b>will be permanently linked to your plan</b> and <b>you won’t be able to cancel it</b> unless you cancel your 500 users or more  subscription.')?></p>
					</div>
				</div>
			<?php endif; ?>

			<?php if(!$isEnterprise): ?>
				<div class="app-description-header"><?=Yii::t('standard', '_DESCRIPTION')?>:</div>
			<?php endif; ?>
			<div class="app-description">
				<?php if (!$isEnterprise) {

					if (!empty($install_detail['custom_desciption'])) echo $install_detail['custom_desciption'];
					else echo str_replace("\n", '<br>', str_replace("\t", '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $lang['extended_description']));

				} else { // Enterprise app gets different description ?>
					<p><?= str_replace("\n", '<br>', str_replace("\t", '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $lang['extended_description'])) ?></p>

					<div class="row-fluid smaller-height-hack">
						<div class="span6">
							<h4><?=Yii::t('apps', 'With this app you will get:')?></h4>

							<ul class="features">
								<li><span></span><?=Yii::t('apps', '<b>Gold</b> Help Desk')?></li>
								<li><span></span><?=Yii::t('apps', '<b>Isolated Cloud</b> LMS Instance')?></li>
								<li><span></span><?=Yii::t('apps', '<b>Dedicated</b> Auto Scalable Infrastructure')?></li>
								<li><span></span><?=Yii::t('apps', 'Minor upgrades and updates are <b>auto provisioned</b> and <b>automatically deployed</b>')?></li>
								<li><span></span><?=Yii::t('apps', 'Migration to major versions are <b>agreed</b> upon and <b>scheduled</b> with customer')?></li>
								<li><span></span><?=Yii::t('apps', '<b>Fully open to custom features development</b>')?></li>
							</ul>
						</div>
						<div class="span6">
							<h4><?=Yii::t('apps', 'Additional apps Coming for free:')?></h4>

							<ul class="features">
								<li><span></span><?=Yii::t('app', 'custom_domain')?></li>
								<li><span></span><?=Yii::t('apps', 'Custom Domain HTTPs')?></li>
								<li><span></span><?=Yii::t('app', 'ecommerce')?></li>
								<li><span></span><?=Yii::t('templatemanager', 'White Label')?></li>
							</ul>

							<h4><?=Yii::t('apps', 'Uptime')?></h4>

							<ul class="features">
								<li><span></span><?=Yii::t('apps', '99.89%')?></li>
							</ul>
						</div>
					</div>

					<?php if(isset($app['pending_enterprise']) && $app['pending_enterprise']): // Already installed ?>
						<div class="enterprise-pending">
							<div class="pull-left green-check">
								<img alt="" src="<?=Yii::app()->theme->baseUrl?>/images/green-check-circled.png" />
							</div>
							<h3><?=Yii::t('apps', 'Your activation request is currently being processed')?></h3>
							<p><?=Yii::t('apps', 'You will be notified as soon as this app is active')?></p>
						</div>
					<?php endif; ?>
				<?php } //endif ?>
			</div>
		</div>
	</div>

	<div class="clearfix"></div>

	<script type="text/javascript">
		$(function(){
			
			$(document).on("click", ".modal-body.opened .contact-us-links", function(){
				$(this).closest(".modal-body").dialog2("close");
			});
			
			
			$('.app-details-dialog a.disabled').click(function(e){
				e.preventDefault();
				e.stopPropagation();
			});
		})
	</script>
