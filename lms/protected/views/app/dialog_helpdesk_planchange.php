<?
/* @var $this AppController */
/* @var $from - silver, gold or platinum */
/* @var $to - silver, gold or platinum */

/* @var $action - upgrade or downgrade */

/* @var $helpdesk_apps - array with keys silver,
 * gold and platinum with each containing details
 * on the corresponding app */

/* @var $subscription_expire_date - When the LMS expires,
 * also matches when the app expires */
/* @var $proRata - The proportional amount ot be paid for
 * the app until the end of the subscription period */
?>

<style>
.current-plan-gradient{
	border: 1px solid #e4e6e5;
	background: #fbfbfb; /* Old browsers */
	background: -moz-linear-gradient(top, #fbfbfb 0%, #f1f3f2 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fbfbfb), color-stop(100%,#f1f3f2)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #fbfbfb 0%,#f1f3f2 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #fbfbfb 0%,#f1f3f2 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #fbfbfb 0%,#f1f3f2 100%); /* IE10+ */
	background: linear-gradient(to bottom, #fbfbfb 0%,#f1f3f2 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fbfbfb', endColorstr='#f1f3f2',GradientType=0 ); /* IE6-9 */

	padding: 10px;
}
.current-plan-gradient .plan-badge{
	background: url('<?=Yii::app()->theme->baseUrl?>/images/apps_menu_sprite.png') no-repeat;
	margin-right: 10px;
}
.current-plan-gradient.gold .plan-badge{
	background-position: -59px -57px;
	width: 20px;
	height: 30px;
}
.current-plan-gradient.platinum .plan-badge{
	background-position: -83px -57px;
	width: 21px;
	height: 30px;
}
.current-plan-gradient h4{
	font-size: 20px;
	text-transform: uppercase;
	color: #999999;
}
.current-plan-gradient.gold h4{
	color: #f8c326;
}
.current-plan-gradient.platinum h4{
	color: #333333;
}
.current-plan-gradient .sublabel{
	font-weight: bold;
	text-transform: uppercase;
	margin: 0;
}
.current-plan-gradient .price-wrapper{
	font-size: 13px;
}
.current-plan-gradient .price-wrapper .free{
	font-size: 25px;
	line-height: 25px;
	color: #52ab52;
	font-weight: bold;
}
.current-plan-gradient .price-wrapper .paid .price{
	font-weight: bold;
	font-size: 16px;
}

	.now-and-after{
		background: #e4e6e5;
		margin-top: 20px;
		margin-bottom: 20px;
		position: relative;
	}
	.now-and-after .half{
		width: 50%;
		float: left;
		position: relative;
		overflow: visible;
	}
	.now-and-after .half p{
		text-transform: uppercase;
		font-size: 15px;
		font-weight: bold;
		padding: 10px;
		margin: 0;
	}
	.now-and-after .half.right p{
		/* Compensate for the right arrow/triangle */
		margin-left: 20px;
	}
	.now-and-after .half.left{
		background: #53ab53;
		color: #ffffff;
	}
	.now-and-after .half.left .arrow-right{
		width: 0;
		height: 0;
		position: absolute;
		left: 100%;
		top:0;
		/* Make a right pointing triangle */
		border-top: 19px solid transparent;
		border-bottom: 19px solid transparent;
		border-left: 19px solid #53ab53;
	}
	.now-and-after .arrow-top-right{
		 width: 0;
		 height: 0;
		 position: absolute;
		 right: 0;
		 top:0;
		 /* Make a right pointing triangle */
		 border-top: 19px solid #ffffff;
		 border-bottom: 19px solid transparent;
		 border-left: 19px solid transparent;
	 }
	.now-and-after .arrow-bottom-right{
		width: 0;
		height: 0;
		position: absolute;
		right: 0;
		bottom:0;
		/* Make a right pointing triangle */
		border-top: 19px solid transparent;
		border-bottom: 19px solid #ffffff;
		border-left: 19px solid transparent;
	}
	.period-details p{
		margin-right: 15px;
		margin-left: 10px;
		font-size: 14px;
	}
	.period-details p.period-after{
		font-weight: bold;
	}

	#do-helpdesk-downgrade,
	#do-helpdesk-upgrade{
		font-size: 18px;
		margin-top: 30px;
	}
</style>

<!-- Consumed by dialog2 as dialog title -->
<? if($action=='downgrade'): ?>
	<h1><?=Yii::t('apps', 'Downgrade Help Desk Plan')?></h1>
<? else: ?>
	<h1><?=Yii::t('apps', 'Upgrade Help Desk Plan')?></h1>
<? endif; ?>

<div class="current-plan-gradient <?=$to?>">
	<? if($to=='platinum' || $to=='gold'): // Silver doesn't have a badge ?>
		<div class="pull-left plan-badge <?=$to?>"></div>
	<? endif; ?>

	<div class="pull-right price-wrapper autoposition vertical">
		<!-- Price -->
		<? if($to=='silver'): ?>
			<div class="free"><?=Yii::t('apps', 'FREE')?></div>
		<? elseif($to=='platinum' || $to=='gold'): ?>
			<div class="paid text-right">
				<span class="price"><?=$newPlan['pricing']['currency']=='usd' ? '&dollar;' : '&euro;';?><?=number_format($newPlan['pricing']['price'], 2)?></span><br/>+ VAT/<?=$newPlan['pricing']['payment_cycle']?>
			</div>
		<? endif; ?>
	</div>

	<h4>
		<? if($to == 'platinum'): ?>
			<?=Yii::t('apps', 'PLATINUM')?>
		<? elseif($to == 'gold'): ?>
			<?=Yii::t('apps', 'GOLD')?>
		<? else: // Silver ?>
			<?=Yii::t('apps', 'SILVER')?>
		<? endif; ?>
	</h4>
	<p class="sublabel"><?=Yii::t('userlimit', 'Help Desk')?></p>

	<div class="clearfix"></div>
</div>

<div class="now-and-after">
	<div class="half left">
		<p><?=Yii::t('apps', 'NOW')?></p>
		<div class="arrow-right"></div>
	</div>
	<div class="half right">
		<p><?=Yii::t('apps', 'Next Renewal')?></p>
	</div>
	<div class="arrow-top-right"></div>
	<div class="arrow-bottom-right"></div>
	<div class="clearfix"></div>
</div>

<div class="row-fluid period-details">
	<div class="span6">
		<p class="green-text period-now">
			<? if($action=='downgrade'): ?>
				<b><?=Yii::t('apps', 'Your downgrade will be applied on: {renew_date}', array(
						'{renew_date}' => (date_parse($subscription_expire_date) && strtotime($subscription_expire_date)) ? date("d-m-Y", strtotime($subscription_expire_date)) : false,
					));?></b>
			<? else: ?>
				<?=Yii::t('apps', '<b>Your upgrade cost will be {currency}{pro_rata} + VAT</b> from <b>today</b> to the <b>{expire_date}</b>.', array(
					'{pro_rata}' => $proRata,
					'{expire_date}' => (date_parse($subscription_expire_date) && strtotime($subscription_expire_date)) ? date("d-m-Y", strtotime($subscription_expire_date)) : false,
					'{currency}' => ($newPlan['pricing']['currency']=='usd' ? '&dollar;' : '&euro;')
				))?>
			<? endif; ?>
		</p>
	</div>
	<div class="span6">
		<p class="period-after">
			<? if($to=='silver'): ?>
				<?=Yii::t('apps', 'Starting from the {renew_date} your help desk plan will be downgraded to the free SILVER version.', array(
					'{renew_date}'=> (date_parse($subscription_expire_date) && strtotime($subscription_expire_date)) ? date("d-m-Y", strtotime($subscription_expire_date . ' +1 day')) : false,
				))?>
			<? else: // Gold or Platinum ?>
				<?=Yii::t('apps', 'Starting from the {renew_date} your recurring {cycle} cost will be {currency}{price} + VAT ', array(
					'{cycle}' => $newPlan['pricing']['payment_cycle'],
					'{price}' => $newPlan['pricing']['price'],
					'{renew_date}'=> (date_parse($subscription_expire_date) && strtotime($subscription_expire_date)) ? date("d-m-Y", strtotime($subscription_expire_date . ' +1 day')) : false,
					'{currency}' => ($newPlan['pricing']['currency']=='usd' ? '&dollar;' : '&euro;')
				))?>
			<? endif; ?>
		</p>
	</div>
</div>

<? if($action=='downgrade'): ?>
	<a href="<?= Yii::app()->createUrl('app/axHelpdeskUpgradeDowngrade', array('from' => $from, 'to' => $to, 'action'=>$action, 'confirm'=>1)) ?>" id="do-helpdesk-upgrade" class="ajax btn-docebo black verybig full bigpadding text-center"><?=Yii::t('apps', 'Downgrade to {new_plan}', array('{new_plan}'=>$to))?></a>
<? else: // Upgrade ?>
	<a href="<?= Yii::app()->createUrl('app/axHelpdeskUpgradeDowngrade', array('from' => $from, 'to' => $to, 'action'=>$action, 'confirm'=>1)) ?>" id="do-helpdesk-downgrade" class="ajax btn-docebo green verybig full bigpadding text-center"><?=Yii::t('apps', 'Upgrade to {new_plan}', array('{new_plan}'=>$to))?></a>
<? endif; ?>

<? //var_dump($helpdesk_apps) ?>