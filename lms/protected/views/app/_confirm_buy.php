<?php
/* @var $app array */
/* @var $title string */
/* @var $userBillingEnabled string */
?>

<?php
$_currency = '';
switch ($app['pricing']['currency']) {
	case 'usd':
		$_currency = '&dollar;'; break;
	case 'eur':
	default:
		$_currency = '&euro;'; break;
}
$_billingCycle = '';
if ($app['market_type'] == 'subscription') {
	$_billingCycle = 'MONTH until canceled';
} else {
	$_billingCycle = 'ONE TIME PAYMENT';
}
if (empty($extra_param)) $extra_param = array();
?>

<h1><?= $title ?></h1>


<?php if ($app['pricing']['pro_rata'] > 0) : ?>

	<div class="alert"><?= Yii::t('apps', 'The credit card on your account will be charged <strong>{pro_rata} {currency} + VAT</strong> (pro-rata), and then
	<strong>{price} {currency} + VAT</strong>', array(
		'{pro_rata}' => $app['pricing']['pro_rata'],
		'{currency}' => $_currency,
		'{price}' => $app['pricing']['price']
	)) ?></div>

<?php else: ?>

	<div class="alert"><?= Yii::t('apps', 'The credit card on your account will be charged {price} {currency} + VAT.', array(
		'{price}' => $app['pricing']['price'],
		'{currency}' => $_currency
	)) ?></div>

<?php endif; ?>

<br>
<p class="pull-left">
	<label class="checkbox" style="display: <?= ($termsAndConditionsLink != null) ? 'block' : 'none' ?>;">
		<input type="checkbox" id="chkbox-confirm" class="chkbox-confirm" "<?= ($termsAndConditionsLink != null) ? '' : 'checked' ?>">
			<?= str_replace('http://www.Docebo.com/cms/page/92/', $termsAndConditionsLink, Yii::t('getmoreuser', '_ACCEPT_DOCEBO_SAAS_TERMS', array('http://www.docebo.com/cms/page/92/' => $termsAndConditionsLink))) ?>
		</input>
	</label>
</p>

<form class="ajax pull-right" action="<?= Yii::app()->createUrl(!empty($custom_url) ? $custom_url : 'app/axConfirmInstall', array('confirmedECommerceShopifyInstPolicy'=>1, 'confirm'=>1, 'id'=>$app['app_id']) + $extra_param) ?>" method="POST">
	<a id="btn-confirm-buy" href="" class="btn btn-docebo green big span2 <?= ($termsAndConditionsLink != null) ? 'disabled' : '' ?> btn-confirm-buy"><?= Yii::t('standard', '_CONFIRM') ?></a>
</form>

<script>

	$('.chkbox-confirm').click(function (e) {
		if ($(this).is(':checked')) {
			$('.btn-confirm-buy').removeClass('disabled');
		} else {
			$('.btn-confirm-buy').addClass('disabled');
		}
	});

	$('.btn-confirm-buy').click(function(e) {
		e.preventDefault();
		<?
			if ($_REQUEST['confirmedECommerceShopifyInstPolicy'] == 1)
			{
				?>
					var action = $(this).parents('form').eq(0).attr('action');
					$.ajax({
						url: action
					})
						.done(function( html ) {
							var scriptStartIndex = html.indexOf('script');

							$('.modal-body')[$('.modal-body').length - 1].innerHTML = html;
							//eval(html.substr(scriptStartIndex + 31, html.length - 9 - scriptStartIndex - 31 - 38 - 9));
							$($('.close')[$('.close').length - 1]).on('click', function () { location.reload() } );
						});
				<?
			}
			else
			{
				?>
					if ( ! $(this).hasClass('disabled')) {
						$(this).parents('form').eq(0).submit();
					}
				<?
			}
		?>
		return false;
	});

	$(document).delegate('.modal', 'dialog2.after-update-markup', function(){
		var e = $(this);

		// get the modal and make it narrower again
		$('#dialog-app-detail').parents('.modal').eq(0).removeClass('wide');

	});

</script>