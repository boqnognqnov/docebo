<?php
/* @var $title string */
?>

<h1><?= $title ?></h1>

<p><?= Yii::t('apps', 'Your app has been reactivated.') ?> <i class="icon-ok"></i></p>

<a href="<?= Yii::app()->createUrl('app/index') ?>" class="btn btn-docebo green big pull-right span2"><?= Yii::t('apps', 'Go to my apps') ?></a>


<script type="text/javascript">
	$(document).delegate('.modal', 'dialog2.closed', function(){
		var e = $(this);
		if(console) {
			console.log(AppsGrid);
		}

		AppsGrid.refresh();
	});
</script>