<?php
/* @var $app array */
/* @var $title string */
?>
<h1><?= $title ?></h1>
<?php if ($app['market_type'] == 'free') : ?>
	<p><?= Yii::t('apps', 'This app <strong>is going to be removed from your apps</strong>, and your monthly payment will be terminated.') ?></p>
<?php else: ?>
	<p><?= Yii::t('apps', 'This app <strong>is going to be removed from your apps</strong>') ?></p>
<?php endif; ?>
<div class="text-right">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'remove_app',
		'action' => Yii::app()->createUrl('app/axRemoveApp', array('confirm' => 1, 'id' => $app['app_id'])),
		'htmlOptions' => array(
			'class' => 'ajax'
		)
	)); ?>
		<input type="submit" class="ajax btn btn-docebo green big" value="<?= Yii::t('standard', '_CONFIRM') ?>">
	<?php $this->endWidget(); ?>
</div>