<?php
/* @var $this AppController */
/* @var bool $is_trial */

/* @var array $helpdesk_apps - has 3 keys: silver, gold, platinum and each
 *  is an array with the corresponding app details array */

/* @var string $installed_helpdesk - May be one of: silver (default), gold, platinum.
 *  Shows which version of the Help Desk is currently installed */

/* @var bool $hasToContactResellerToInstall - True when the LMS is paid by Wire Transfer */

/* @var bool $ecsInstalled - True when the Enterprise Cloud Solution app is installed in this LMS */
/* @var bool $ecsFullyActive - True when the ECS app is installed and manually activated in ERP (last step) */
?>
<style type="text/css">
	/* @TODO move these somewhere else */


	#fullpage-helpdesk .plan-name {
		font-size:      30px;
		font-weight:    bold;
		text-transform: uppercase;
		line-height:    1;
		padding-top:     10px;
	}
	#fullpage-helpdesk .plan-name.gold{
		color: #f8c326;
	}
	#fullpage-helpdesk .plan-name.platinum{
		color: #999999;
	}

	#fullpage-helpdesk .plan-slogan {
		line-height:    1;
		text-transform: uppercase;
		font-size:      13px;
		font-weight:    bold;
		margin: 0;
		padding-bottom: 10px;
	}
	#fullpage-helpdesk .badge-holder{
		position: relative;
		min-height: 60px;
	}
	#fullpage-helpdesk .badge-holder .upper-gray-triangle{
		position: absolute;
		width: 100%;
		height: 20px;
		margin:0;
		padding:0;
	}
	#fullpage-helpdesk .badge-holder .badge-image{
		position: absolute;
	}
	#fullpage-helpdesk .feature{
		margin-top: 10px;
		margin-bottom: 10px;
	}
	#fullpage-helpdesk .feature.action-button{
		margin-left: 10px;
		margin-right: 10px;
	}
	.arrow-down {
		width: 0;
		height: 0;
		border-left: 100px solid transparent;
		border-right: 100px solid transparent;
		border-top: 10px solid #F5F5F5;
	}

	table{
		border-collapse: collapse;
		table-layout: fixed;
		width: 100%;
		overflow: hidden;
	}
	table tr, table td{
		border: none;
		padding:0;
		margin:0;
	}
	table tr td{
		border: 1px solid #E4E6E5;
	}

	table tr.feature-row{
		border-top: 1px solid #E4E6E5;
		border-bottom: 1px solid #E4E6E5;;
	}
	table tr.feature-row td{
		border-right: 1px solid #E4E6E5;
	}
	table tr td.noborder{
		border: none;
	}
	.badge-image{
		background: url('<?=Yii::app()->theme->baseUrl?>/images/apps_menu_sprite.png') no-repeat;
	}
	.badge-image.gold{
		background-position: -58px -4px;
		width: 30px;
		height: 45px;
	}
	.badge-image.platinum{
		background-position: -93px -4px;
		width: 31px;
		height: 45px;
	}
	.tooltip-icon{
		background: url('<?=Yii::app()->theme->baseUrl?>/images/apps_menu_sprite.png') no-repeat -108px -57px;
		width: 16px;
		height: 16px;
		display: inline-block;
		margin-left: 5px;
		margin-right: 10px;
		margin-top: -1px;
	}
	.active-plan-icon{
		background: url('<?=Yii::app()->theme->baseUrl?>/images/apps_menu_sprite.png') no-repeat -61px -93px;
		width: 30px;
		height: 30px;
		margin-left: 86px;
	}
	.active-plan-label{
		font-size: 25px;
		text-transform: uppercase;
		color: #5fbf5f;
		font-weight: bold;
		margin-top: 10px;
		margin-bottom: 10px;
	}
	.active-plan-sublabel span{
		color: #5fbf5f;
	}
	.plan-price{
		font-size: 18px;
		margin-top: 20px;
		margin-bottom: 15px;
	}
	.plan-price .green{
		font-size: 35px;
		font-weight: bold;
		color: #5fbf5f;
	}
	.action-button .btn-docebo{
		font-size: 13px;
	}

	.no-plan-warning{
		margin-top: 20px;
		border: 3px solid #e67e22;
		padding: 10px 15px;
	}
	.no-plan-warning .warning-icon{
		width: 31px;
		height: 30px;
		float: left;
		margin-right: 10px;
	}
	.no-plan-warning p{
		color: #e57e1f;
		font-size: 13px;
		font-weight: bold;
		margin:0;
		line-height: 30px;
	}
</style>

<? // var_dump($helpdesk_apps) ?>

<table>
	<tr>
		<td style="border:none">
			<!-- Empty -->
		</td>
		<? if(!$ecsInstalled): // Hide Silver completely when ECS active ?>
		<td>
			<div class="bg-gray">
				<p class="plan-name text-center"><?=Yii::t('apps', 'SILVER')?></p>
				<p class="plan-slogan text-center"><?=Yii::t('userlimit', 'Help Desk')?></p>
			</div>
			<div class="badge-holder">
				<div class="upper-gray-triangle">
					<div class="arrow-down"></div>
				</div>
			</div>
		</td>
		<? endif; ?>
		<td>
			<div class="bg-gray">
				<p class="plan-name text-center gold"><?=Yii::t('apps', 'GOLD')?></p>
				<p class="plan-slogan text-center"><?=Yii::t('userlimit', 'Help Desk')?></p>
			</div>
			<div class="badge-holder">
				<div class="upper-gray-triangle">
					<div class="arrow-down"></div>
				</div>
				<div class="badge-image gold autoposition horizontal"></div>
			</div>
		</td>
		<td>
			<div class="bg-gray">
				<p class="plan-name text-center platinum"><?=Yii::t('apps', 'PLATINUM')?></p>
				<p class="plan-slogan text-center"><?=Yii::t('userlimit', 'Help Desk')?></p>
			</div>
			<div class="badge-holder">
				<div class="upper-gray-triangle">
					<div class="arrow-down"></div>
				</div>
				<div class="badge-image platinum autoposition horizontal"></div>
			</div>
		</td>
	</tr>

	<tr class="feature-row bg-gray">
		<td>
			<div class="text-right">
				<a class="pull-right" rel="tooltip" title="<?=Yii::t('apps', 'Incident registration, ticket number assignment to the client, inclusion of the request in the Help Desk system.')?>"><span class="tooltip-icon"></span></a>
				<b><?=Yii::t('apps', 'Request handling')?></b>
			</div>
		</td>

		<? if(!$ecsInstalled): // Hide Silver completely when ECS active ?>
		<td>
			<p class="text-center nomargin feature">
				<?=Yii::t('apps', 'H24, 7/7 - Within {hour} hours, including weekends and public holidays', array('{hour}'=>8))?>
			</p>
		</td>
		<? endif; ?>

		<td>
			<p class="text-center nomargin feature">
				<?=Yii::t('apps', 'H24, 7/7 - Within {hour} hours, including weekends and public holidays', array('{hour}'=>4))?>
			</p>
		</td>

		<td>
			<p class="text-center nomargin feature">
				<?=Yii::t('apps', 'H24, 7/7 - Within {hour} hours, including weekends and public holidays', array('{hour}'=>1))?>
			</p>
		</td>
	</tr>

	<tr class="feature-row">
		<td>
			<div class="text-right">
				<a class="pull-right" rel="tooltip" data-html="true" title="<?=Yii::t('apps', 'Via_ticket')?>"><span class="tooltip-icon"></span></a>
				<b><?=Yii::t('apps', 'First Interaction')?></b>
			</div>
		</td>
		<? if(!$ecsInstalled): // Hide Silver completely when ECS active ?>
		<td>
			<div class="text-center feature">
				<?=Yii::t('apps', 'Via ticket within {hours} hours', array('{hours}'=>24))?>
			</div>
		</td>
		<? endif; ?>
		<td>
			<div class="text-center feature">
				<?=Yii::t('apps', 'Via ticket within {hours} hours', array('{hours}'=>16))?>
			</div>
		</td>
		<td>
			<div class="text-center feature">
				<?=Yii::t('apps', 'Via phone, within {hours} working hours', array('{hours}'=>8))?>
			</div>
		</td>
	</tr>

	<tr class="feature-row bg-gray">
		<td>
			<div class="text-right">
				<a class="pull-right" rel="tooltip" title="<?=Yii::t('apps', 'ETA notification to the client (and update about any ETA changes). The agreed ETA can be guaranteed only if the customer provides Docebo with all the information needed to a proper ETA diagnosis.')?>"><span class="tooltip-icon"></span></a>
				<b><?=Yii::t('apps', 'ETA notification*')?></b>
			</div>
		</td>
		<? if(!$ecsInstalled): // Hide Silver completely when ECS active ?>
		<td>
			<div class="text-center feature">
				<b>-</b>
			</div>
		</td>
		<? endif; ?>
		<td>
			<div class="text-center feature">
				<?=Yii::t('apps', 'Within {hours} working hours since the First Interaction', array('{hours}'=>8))?>
			</div>
		</td>
		<td>
			<div class="text-center feature">
				<?=Yii::t('apps', 'Within {hours} working hours since the First Interaction', array('{hours}'=>4))?>
			</div>
		</td>
	</tr>

	<tr class="feature-row">
		<td>
			<div class="text-right">
				<a class="pull-right" rel="tooltip" title="<?=Yii::t('apps', 'The customer is informed about the successful resolution of the problem (this message closes the support process).')?>"><span class="tooltip-icon"></span></a>
				<b><?=Yii::t('apps', 'Ticket resolution')?></b>
			</div>
		</td>
		<? if(!$ecsInstalled): // Hide Silver completely when ECS active ?>
		<td>
			<div class="text-center feature">
				<?=Yii::t('apps', '9AM - 6PM CET Mon/Fri')?><br/>
				<?=Yii::t('apps', '9AM - 6PM EST Mon/Fri')?>
			</div>
		</td>
		<? endif; ?>
		<td>
			<div class="text-center feature">
				<?=Yii::t('apps', '9AM - 6PM CET Mon/Fri')?><br/>
				<?=Yii::t('apps', '9AM - 6PM EST Mon/Fri')?>
			</div>
		</td>
		<td>
			<div class="text-center feature">
				<?=Yii::t('apps', '9AM - 6PM CET Mon/Fri')?><br/>
				<?=Yii::t('apps', '9AM - 6PM EST Mon/Fri')?>
			</div>
		</td>
	</tr>

	<tr class="feature-row bg-gray">
		<td>
			<div class="text-right">
				<a class="pull-right" rel="tooltip" title="<?=Yii::t('apps', 'Severe incidents are about critical production issues/performances degrading which affect all users')?>"><span class="tooltip-icon"></span></a>
				<b><?=Yii::t('apps', 'Severe Incident')?></b>
			</div>
		</td>
		<? if(!$ecsInstalled): // Hide Silver completely when ECS active ?>
		<td>
			<div class="text-center feature">
				<?=Yii::t('apps', 'H24, 7/7 - Within {hours} hours, and priority level {level} while fixing the issues', array('{hours}'=>4, '{level}'=>4))?>
			</div>
		</td>
		<? endif; ?>
		<td>
			<div class="text-center feature">
				<?=Yii::t('apps', 'H24, 7/7 - Within {hours} hours, and priority level {level} while fixing the issues', array('{hours}'=>2, '{level}'=>2))?>
			</div>
		</td>
		<td>
			<div class="text-center feature">
				<?=Yii::t('apps', 'H24, 7/7 - Immediate, and priority level {level} while fixing the issues', array('{level}'=>1))?>
			</div>
		</td>
	</tr>

	<tr class="prices-row">
		<td class="noborder">
			<!-- Empty -->
		</td>
		<? if(!$ecsInstalled): // Hide Silver completely when ECS active ?>
		<td>
			<div class="text-center feature action-button">

				<!-- SILVER -->

				<? if($installed_helpdesk == 'silver'): // Already installed ?>
					<div class="active-plan-icon autoposition horizontal"></div>
					<div class="active-plan-label"><?=Yii::t('standard', '_ACTIVE')?></div>
					<div class="active-plan-sublabel"><?=Yii::t('apps', 'for <span>FREE</span>')?></div>
				<? elseif($installed_helpdesk == 'platinum' || $installed_helpdesk == 'gold'): // Can downgrade to this ?>
					<div class="plan-price">
						<span class="green"><?=Yii::t('standard', 'FREE')?></span>
					</div>
					<p><a class="open-dialog btn-docebo black full bigpadding" data-dialog-class="metro small app-details" rel="newdialog" href="<?= Yii::app()->createUrl('app/axHelpdeskUpgradeDowngrade', array('action'=>'downgrade', 'from'=>$installed_helpdesk, 'to'=>'silver')) ?>"><?=Yii::t('apps', 'Downgrade to {new_plan}', array('{new_plan}'=>'Silver'))?></a></p>
				<? endif; ?>
			</div>
		</td>
		<? endif; ?>

		<td class="text-center">
			<div class="text-center feature action-button">

				<!-- GOLD -->

				<? if($installed_helpdesk != 'gold'): // Gold (this) not installed yet ?>

					<? if(!$ecsInstalled): // Gold is Free when ECS app is enabled ?>
						<div class="plan-price">
							<?=$helpdesk_apps['gold']['pricing']['currency']=='eur' ? '&euro;' : '&dollar;'; ?>
							<span class="green"><?=$helpdesk_apps['gold']['pricing']['price']?></span>/<?=$helpdesk_apps['gold']['pricing']['payment_cycle']=='monthly' ? 'mo' : 'yr';?>
						</div>
					<? endif; ?>
					<? if($is_trial): ?>
						<p><div class="btn-docebo disabled green full bigpadding"><?=Yii::t('apps', 'Upgrade to {new_plan}', array('{new_plan}'=>'Gold'))?></div></p>
					<? else: ?>
						<? if($installed_helpdesk == 'silver'): // Can upgrade to this (Gold) ?>
							<p>
								<? if($hasToContactResellerToInstall): ?>
									<a class="open-dialog btn-docebo blue full bigpadding" data-dialog-class="metro small app-details" rel="newdialog" href="<?= Yii::app()->createUrl('site/axContactSalesTrialExpired', array('app_id' => $helpdesk_apps[$installed_helpdesk]['app_id'], 'ref' => 'helpdesk-upgrade')) ?>"><?=Yii::t('apps', 'Contact Us to Upgrade')?></a>
								<? else: // credit_card ?>
									<a class="open-dialog btn-docebo green full bigpadding" data-dialog-class="metro small app-details" rel="newdialog" href="<?= Yii::app()->createUrl('app/axHelpdeskUpgradeDowngrade', array('from' => $installed_helpdesk, 'to' => 'gold', 'action'=>'upgrade')) ?>"><?=Yii::t('apps', 'Upgrade to {new_plan}', array('{new_plan}'=>'Gold'))?></a>
								<? endif; ?>
							</p>
						<? elseif($installed_helpdesk == 'platinum'): // Can downgrade to this (Gold)  ?>
							<? if($ecsInstalled): // Downgrading to Gold from Platinum is free when ECS is enabled ?>
								<p class="active-plan-label"><?=Yii::t('apps', 'FREE')?>!</p>
								<p class="active-plan-sublabel"><?=Yii::t('apps', '<span>included with the {ecs}</span>', array('{ecs}'=>Yii::t('apps', 'Enterprise Cloud Solution')))?></p>
								<p><a class="open-dialog btn-docebo black full bigpadding" data-dialog-class="metro small app-details" rel="newdialog" href="<?= Yii::app()->createUrl('app/axHelpdeskUpgradeDowngrade', array('from'=>$installed_helpdesk, 'to'=>'gold', 'action'=>'downgrade')) ?>"><?=Yii::t('apps', 'Downgrade to {new_plan}', array('{new_plan}'=>'Gold'))?></a></p>
							<? else: ?>
								<p>
									<? if($hasToContactResellerToInstall): ?>
										<a class="open-dialog btn-docebo blue full bigpadding" data-dialog-class="metro small app-details" rel="newdialog" href="<?= Yii::app()->createUrl('site/axContactSalesTrialExpired', array('app_id' => $helpdesk_apps[$installed_helpdesk]['app_id'], 'ref' => 'helpdesk-downgrade')) ?>"><?=Yii::t('apps', 'Contact Us to Downgrade')?></a>
									<? else: // credit_card ?>
										<a class="open-dialog btn-docebo black full bigpadding" data-dialog-class="metro small app-details" rel="newdialog" href="<?= Yii::app()->createUrl('app/axHelpdeskUpgradeDowngrade', array('from'=>$installed_helpdesk, 'to'=>'gold', 'action'=>'downgrade')) ?>"><?=Yii::t('apps', 'Downgrade to {new_plan}', array('{new_plan}'=>'Gold'))?></a>
									<? endif; ?>
								</p>
							<? endif; ?>
						<? endif; ?>
					<? endif; ?>

				<? elseif($ecsInstalled): // Gold is automatically installed with the ECS app ?>
					<div class="active-plan-icon autoposition horizontal"></div>
					<div class="active-plan-label"><?=Yii::t('standard', '_ACTIVE')?></div>
					<div class="active-plan-sublabel"><?=Yii::t('apps', '<span>included with the {ecs}</span>', array('{ecs}'=>Yii::t('apps', 'Enterprise Cloud Solution')))?></div>
				<? else: // Gold (current) installed ?>
					<div class="active-plan-icon autoposition horizontal"></div>
					<div class="active-plan-label"><?=Yii::t('standard', '_ACTIVE')?></div>
					<div class="active-plan-sublabel"><?=Yii::t('apps', 'for {currency}<span>{price}</span>/{cycle}', array(
							'{currency}' => ($helpdesk_apps['gold']['pricing']['currency']=='eur' ? '&euro;' : '&dollar;'),
							'{price}' => number_format($helpdesk_apps['gold']['pricing']['price'], 2),
							'{cycle}' => ($helpdesk_apps['gold']['pricing']['payment_cycle'] == 'month' ? 'mo' : 'yr'),
						))?></div>
				<? endif; ?>
			</div>
		</td>
		<td class="text-center">
			<div class="text-center feature action-button">

				<!-- PLATINUM -->

				<? if($installed_helpdesk != 'platinum'): // Platinum (this) not installed yet ?>

					<div class="plan-price">
						<?=$helpdesk_apps['platinum']['pricing']['currency']=='eur' ? '&euro;' : '&dollar;'; ?>
						<span class="green"><?=$helpdesk_apps['platinum']['pricing']['price']?></span>/<?=$helpdesk_apps['platinum']['pricing']['payment_cycle']=='monthly' ? 'mo' : 'yr';?>
					</div>
					<? if($is_trial): ?>
						<p><div class="btn-docebo disabled green full bigpadding"><?=Yii::t('apps', 'Upgrade to {new_plan}', array('{new_plan}'=>'Platinum'))?></div></p>
					<? else: ?>
						<? if($installed_helpdesk == 'silver' || $installed_helpdesk == 'gold'): // Can upgrade to this (Platinum) ?>
							<p>
								<? if($hasToContactResellerToInstall): ?>
									<a class="open-dialog btn-docebo blue full bigpadding" data-dialog-class="metro small app-details" rel="newdialog" href="<?= Yii::app()->createUrl('site/axContactSalesTrialExpired', array('app_id' => $helpdesk_apps[$installed_helpdesk]['app_id'], 'ref' => 'helpdesk-upgrade')) ?>"><?=Yii::t('apps', 'Contact Us to Upgrade')?></a>
								<? else: // credit_card ?>
									<a class="open-dialog btn-docebo green full bigpadding" data-dialog-class="metro small app-details" rel="newdialog" href="<?= Yii::app()->createUrl('app/axHelpdeskUpgradeDowngrade', array('from' => $installed_helpdesk, 'to' => 'platinum', 'action'=>'upgrade')) ?>"><?=Yii::t('apps', 'Upgrade to {new_plan}', array('{new_plan}'=>'Platinum'))?></a>
								<? endif; ?>
							</p>
						<? else: // Neither Gold nor Silver (nor this - Platinum) is installed. That's wrong. What are we upgrading from? ?>
						<? endif; ?>
					<? endif; ?>

				<? else: // Platinum (current) installed ?>
					<div class="active-plan-icon autoposition horizontal"></div>
					<div class="active-plan-label"><?=Yii::t('standard', '_ACTIVE')?></div>
					<div class="active-plan-sublabel"><?=Yii::t('apps', 'for {currency}<span>{price}</span>/{cycle}', array(
							'{currency}' => ($helpdesk_apps['platinum']['pricing']['currency']=='eur' ? '&euro;' : '&dollar;'),
							'{price}' => number_format($helpdesk_apps['platinum']['pricing']['price'], 2),
							'{cycle}' => ($helpdesk_apps['platinum']['pricing']['payment_cycle'] == 'month' ? 'mo' : 'yr'),
						))?></div>
				<? endif; ?>
			</div>
		</td>
	</tr>

	<? if($is_trial): ?>
		<tr>
			<td class="noborder"></td>
			<td colspan="3" class="noborder">
				<div class="no-plan-warning">
					<img class="warning-icon" src="<?=Yii::app()->theme->baseUrl;?>/images/warning-orange.png" />
					<p><?=Yii::t('apps', 'You must have an user subscription in order to upgrade your Help Desk Plan')?></p>
				</div>
			</td>
		</tr>
	<? endif; ?>
</table>

<? if($paymentCycle=='monthly' && $paymentMethod=='credit_card' && $installed_helpdesk=='silver'): ?>
	<br/><br/>
	<div class="filters-wrapper">
		<div class="inner-filter">
			<div class="pull-right">
				<a class="open-dialog btn-docebo blue big" data-dialog-class="metro small app-details" rel="newdialog" href="<?= Yii::app()->createUrl('site/axContactSalesTrialExpired', array('app_id' => $helpdesk_apps[$installed_helpdesk]['app_id'], 'ref' => 'helpdesk-info')) ?>"><?=Yii::t('apps', 'Contact Us to Upgrade')?></a>
			</div>
			<?=Yii::t('apps', 'For more information or if you want to pay <b>yearly</b> by <b>wire transfer</b>')?>
		</div>
	</div>
<? endif; ?>