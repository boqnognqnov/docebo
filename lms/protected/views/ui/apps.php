<!--
<div class="app-cart-container row-fluid">
	<div class="span9">
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Warning!</strong> Best check yo self, you're not looking too good.
		</div>
	</div>
	<div class="span3">
		<button class="btn btn-buy-apps-cart pull-right" data-placement="bottom" data-title="Your cart's content">
			<span class="count-items">2</span>
			<span class="cart-icon"></span> Your cart</button>
	</div>
</div>
-->


<div id="remove-app-modal" class="metro modal hide fade" tabindex="-1" role="dialog">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Lorem ipsum app</h3>
	</div>
	<div class="modal-body">
		<p>This app <strong>is going to be removed from your apps</strong>, and your monthly payment <strong>will be terminated</strong>.</p>
		<a href="#" class="confirm-app-removal" data-dismiss="modal">Yes, I want to remove this app</a>
	</div>
</div>

<div id="preview-app-modal" class="metro modal wide hide fade" tabindex="-1" role="dialog">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h1>App title here</h1>
	</div>
	<div class="modal-body">
		<div class="row-fluid">

			<div class="span9">
				<p>Pie marshmallow chocolate bar biscuit lemon drops. Soufflé halvah pie chocolate bar sweet pudding sugar plum soufflé faworki. Candy icing liquorice chupa chups. Macaroon jelly-o tootsie roll liquorice muffin cotton candy soufflé gummi bears chocolate cake. Fruitcake bonbon tart macaroon. Pie pudding topping. Candy canes sesame snaps pie cake gummi bears dragée powder chupa chups muffin.</p>
				<p>Pie carrot cake sweet roll sesame snaps jelly beans tart oat cake pastry brownie. Soufflé cupcake pudding lollipop. Pudding cupcake jelly. Cookie gummi bears donut wypas cupcake brownie tiramisu bear claw. Marzipan pudding lollipop marshmallow. Tootsie roll jelly-o gummi bears candy canes chocolate bar applicake. Cheesecake gingerbread bonbon pudding. Wafer cupcake halvah pie wafer gummi bears oat cake.</p>
			</div>
			<div class="span3">
				<div class="well">
					<h4>Price</h4>
					<p class="price">
						<strong>&euro; 25.00</strong> + VAT / <strong>MONTH</strong>
					</p>
					<a href="#" class="btn btn-success">Buy now</a>
				</div>
			</div>
		</div>

		<p>Yes, I accept the <a href="#">terms and conditions</a></p>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('.btn-remove-app').click(function(e){
			e.preventDefault();
			$('#remove-app-modal').modal({
				backdrop: 'static'
			});
		});
		$('#preview-app-modal').modal('show');
		//Docebo.Feedback.show('success', 'Unknown afterGridLoad mode.');
	});
</script>


<div id="courses-grid" class="enable-five-grid">
	<div class="row-fluid">
		<div class="span1 tile cover bg-color-orange">
			<div class="tile-content">
				<h2>MY APPS</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam nec felis ut odio volutpat aliquam. Integer non elementum odio.</p>
				<div class="right-arrow"></div>
			</div>
		</div>



		<div class="span1 tile bg-gradient app">
			<div class="app-image">
				<img src="http://goo.gl/VGph5">
			</div>
			<div class="tile-content">
				<h5 class="app-title">Twitter integration</h5>
				<div class="app-description">Gingerbread apple pie I love. Carrot cake cupcake brownie I love. Soufflé biscuit gingerbread chocolate bar.</div>
			</div>
			<div class="hover">
				<div class="clearfix">
					<a href="#" class="pull-right btn-remove-app">Delete</a>
					<a href="#" class="pull-left btn-enable-app">Deactivate</a>
				</div>
				<br>

				<a href="#" class="btn-configure-app">Configure</a>

				<div class="hover-details">
					<h5>E-commerce (hover)</h5>

					<div class="description">
						Gingerbread apple pie I love. Carrot cake cupcake brownie I love. Soufflé biscuit gingerbread chocolate bar.
					</div>
				</div>
			</div>
		</div>



		<div class="span1 tile bg-gradient app">
			<div class="app-image">
				<img src="http://goo.gl/VGph5">
			</div>
			<div class="tile-content">
				<h5 class="app-title">Twitter integration</h5>
				<div class="app-description">Gingerbread apple pie I love. Carrot cake cupcake brownie I love. Soufflé biscuit gingerbread chocolate bar.</div>
			</div>
			<div class="hover">
				<div class="clearfix">
					<a href="#" class="pull-right btn-remove-app">Delete</a>
					<a href="#" class="pull-left btn-enable-app">Deactivate</a>
				</div>
				<br>

				<a href="#" class="btn-configure-app">Configure</a>

				<div class="hover-details">
					<h5>E-commerce (hover)</h5>

					<div class="description">
						Gingerbread apple pie I love. Carrot cake cupcake brownie I love. Soufflé biscuit gingerbread chocolate bar.
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row-fluid">

		<div class="span1 tile cover bg-color-blue">
			<div class="tile-content">
				<h2>FREE APPS CATALOGUE</h2>
				<p>Browse our FREE apps to extend your Docebo Cloud</p>
				<div class="image-wrapper">
					<span class="icon icon-grid"></span>
				</div>
				<div class="right-arrow"></div>
			</div>
		</div>


		<div class="span1 tile bg-gradient app">
			<div class="app-image">
				<img src="http://goo.gl/VGph5">
			</div>
			<div class="tile-content">
				<h5 class="app-title">Twitter integration</h5>
				<div class="app-description">Gingerbread apple pie I love. Carrot cake cupcake brownie I love. Soufflé biscuit gingerbread chocolate bar.</div>
			</div>
			<div class="hover">
				<a href="#" class="btn-start-trial">Start free trial</a>
				<a href="#" class="btn-more-details">More details&hellip;</a>


				<div class="hover-details">
					<h5>E-commerce (hover)</h5>

					<div class="description">
						Gingerbread apple pie I love. Carrot cake cupcake brownie I love. Soufflé biscuit gingerbread chocolate bar.
					</div>
				</div>
			</div>
		</div>


		<div class="span1 tile bg-gradient app">
			<div class="app-image">
				<img src="http://goo.gl/VGph5">
			</div>
			<div class="tile-content">
				<h5 class="app-title">Twitter integration</h5>
				<div class="app-description">Gingerbread apple pie I love. Carrot cake cupcake brownie I love. Soufflé biscuit gingerbread chocolate bar.</div>
			</div>
			<div class="hover">
				<a href="#" class="btn-buy-app">Buy now</a>
				<a href="#" class="btn-more-details">More details&hellip;</a>

				<div class="hover-details">
					<h5>E-commerce (hover)</h5>

					<div class="description">
						Gingerbread apple pie I love. Carrot cake cupcake brownie I love. Soufflé biscuit gingerbread chocolate bar.
					</div>
				</div>
			</div>
		</div>


		<div class="span1 tile bg-gradient app">
			<div class="app-image">
				<img src="http://goo.gl/VGph5">
			</div>
			<div class="tile-content">
				<h5 class="app-title">Twitter integration</h5>
				<div class="app-description">Gingerbread apple pie I love. Carrot cake cupcake brownie I love. Soufflé biscuit gingerbread chocolate bar.</div>
			</div>
			<div class="hover">
				<div class="clearfix">
					<a href="#" class="pull-right btn-remove-app">Delete</a>
					<a href="#" class="pull-left btn-enable-app">Deactivate</a>
				</div>
				<br>

				<a href="#" class="btn-configure-app">Configure</a>

				<div class="hover-details">
					<h5>E-commerce (hover)</h5>

					<div class="description">
						Gingerbread apple pie I love. Carrot cake cupcake brownie I love. Soufflé biscuit gingerbread chocolate bar.
					</div>
				</div>
			</div>
		</div>

		<a href="#" class="span1 tile bg-gradient back-cover">
			<div class="tile-content">
				<span>View more courses</span>
			</div>
		</a>




	</div>

	<div class="row-fluid">

		<div class="span1 tile cover bg-color-green">
			<div class="tile-content">
				<h2>PAID APPS CATALOGUE</h2>
				<p>Browse our PAID apps to extend your Docebo Cloud</p>
				<div class="image-wrapper">
					<span class="icon icon-cart"></span>
				</div>
				<div class="right-arrow"></div>
			</div>
		</div>

		<div class="span1 tile bg-gradient app">
			<div class="app-image">
				<img src="http://goo.gl/VGph5">
			</div>
			<div class="tile-content">
				<h5 class="app-title">Twitter integration</h5>
				<div class="app-description">Gingerbread apple pie I love. Carrot cake cupcake brownie I love. Soufflé biscuit gingerbread chocolate bar.</div>
			</div>
			<div class="hover">
				<div class="clearfix">
					<a href="#" class="pull-right btn-remove-app">Delete</a>
					<a href="#" class="pull-left btn-enable-app">Deactivate</a>
				</div>
				<br>

				<a href="#" class="btn-configure-app">Configure</a>

				<div class="hover-details">
					<h5>E-commerce (hover)</h5>

					<div class="description">
						Gingerbread apple pie I love. Carrot cake cupcake brownie I love. Soufflé biscuit gingerbread chocolate bar.
					</div>
				</div>
			</div>
		</div>

		<div class="span1 tile bg-gradient app">
			<div class="app-image">
				<img src="http://goo.gl/VGph5">
			</div>
			<div class="tile-content">
				<h5 class="app-title">Twitter integration</h5>
				<div class="app-description">Gingerbread apple pie I love. Carrot cake cupcake brownie I love. Soufflé biscuit gingerbread chocolate bar.</div>
			</div>
			<div class="hover">
				<div class="clearfix">
					<a href="#" class="pull-right btn-remove-app">Delete</a>
					<a href="#" class="pull-left btn-enable-app">Deactivate</a>
				</div>
				<br>

				<a href="#" class="btn-configure-app">Configure</a>

				<div class="hover-details">
					<h5>E-commerce (hover)</h5>

					<div class="description">
						Gingerbread apple pie I love. Carrot cake cupcake brownie I love. Soufflé biscuit gingerbread chocolate bar.
					</div>
				</div>
			</div>
		</div>

		<div class="span1 tile bg-gradient app">
			<div class="app-image">
				<img src="http://goo.gl/VGph5">
			</div>
			<div class="tile-content">
				<h5 class="app-title">Twitter integration</h5>
				<div class="app-description">Gingerbread apple pie I love. Carrot cake cupcake brownie I love. Soufflé biscuit gingerbread chocolate bar.</div>
			</div>
			<div class="hover">
				<div class="clearfix">
					<a href="#" class="pull-right btn-remove-app">Delete</a>
					<a href="#" class="pull-left btn-enable-app">Deactivate</a>
				</div>
				<br>

				<a href="#" class="btn-configure-app">Configure</a>

				<div class="hover-details">
					<h5>E-commerce (hover)</h5>

					<div class="description">
						Gingerbread apple pie I love. Carrot cake cupcake brownie I love. Soufflé biscuit gingerbread chocolate bar.
					</div>
				</div>
			</div>
		</div>

		<a href="#" class="span1 tile bg-gradient back-cover">
			<div class="tile-content">
				<span>View more courses</span>
			</div>
		</a>
	</div>
</div>