<?php
	/* @var $this UsersSelectorController */

	/**
	 * It is possible that this dialog is part of a wizard, as a step of it (see jwizard.js)
	 */
	if (is_array($wizardParams)) {
		$wizardMode 			= $wizardParams['wizard_mode']; 	// Are we ??  
		$wizardModeFirst 		= $wizardParams['first'];			// Is this dialog the FIRST one in the sequence?
		$wizardModeLast	 		= $wizardParams['last'];			// Or the last ?
		$wizardModeFinal 		= $wizardParams['final'];			// Is it also the FINAL step (having FINISH button) ?
		$wizardModeNextUrl 		= $wizardParams['next_url'];		// What is the next step ?
		$wizardModePrevUrl 		= $wizardParams['prev_url'];		// Previous URL?
		$wizardModeFinishUrl 	= $wizardParams['finish_url'];		// and the FINALIZING URL, if any
		$wizardModeBtnNextTxt	= $wizardParams['btnNextTxt'];
	}

	$selectorContainer = 'users-selector-container'; 
	$formId = $formId ? $formId : 'users-selector-form';
	$dialogClass = 'users-selector';
	
	// Load preselected items into hidden fields; they will be used to populate selection upon loading
	$preselectedUsersJSON 		= CJSON::encode($preselectedUsers);
	$preselectedGroupsJSON 		= CJSON::encode($preselectedGroups);
	$preselectedCoursesJSON		= CJSON::encode($preselectedCourses);
	$preselectedPlansJSON		= CJSON::encode($preselectedPlans);
	$preselectedPuProfilesJSON	= CJSON::encode($preselectedPuProfiles);
	$preselectedCertificationsJSON	= CJSON::encode($preselectedCertifications);
    $preselectedBadgesJSON	    = CJSON::encode($preselectedBadges);
	$preselectedContestsJSON	= CJSON::encode($preselectedContests);
	$preselectedAssetsJSON	    = CJSON::encode($preselectedAssets);
	$preselectedChannelsJSON	= CJSON::encode($preselectedChannels);

	
	// START HTML
	echo CHtml::beginForm($formAction, "POST", array(
		'id' => $formId,
		'class' => 'ajax jwizard-form',
	));

 	echo CHtml::hiddenField('selected_users'		, $preselectedUsersJSON);
 	echo CHtml::hiddenField('selected_groups'		, $preselectedGroupsJSON);
 	echo CHtml::hiddenField('selected_courses'		, $preselectedCoursesJSON);
 	echo CHtml::hiddenField('selected_plans'		, $preselectedPlansJSON);
 	echo CHtml::hiddenField('selected_puprofiles'	, $preselectedPuProfilesJSON);
	echo CHtml::hiddenField('selected_certifications'	, $preselectedCertificationsJSON);
    echo CHtml::hiddenField('selected_badges'	    , $preselectedBadgesJSON);
	echo CHtml::hiddenField('selected_contests'		, $preselectedContestsJSON);
	echo CHtml::hiddenField('selected_assets'		, $preselectedAssetsJSON);
	echo CHtml::hiddenField('selected_channels'		, $preselectedChannelsJSON);
	
 	// Pass selector parameters on submition. The "collector" may need them (actually, it WILL need them for sure)
 	foreach ($selectorParams as $paramName => $paramValue) {
		echo CHtml::hiddenField($paramName, $paramValue, array('class' => 'jwizard-no-restore'));
	}
	
	// Also, in case we are in a wizard mode, we send our "signature" (name) to inform the next step where we come from
	// jwizard-no-restore prevents loosing from_step (overwritten by previous step)
	//echo CHtml::hiddenField('from_step', $this->name, array('class' => 'jwizard-no-restore'));
	echo CHtml::hiddenField('from_step', $this->name, array());
	

	
?>


<div id="<?= $selectorContainer ?>" class="lang_<?php echo Yii::app()->getLanguage(); ?>">


	<?php
		if ($topPanel) { 
			$this->renderPartial($topPanel, array(
				'withoutCourses'=>isset($withoutCourses) ? $withoutCourses : false
			));
		}
		else {
			$this->renderPartial('_panel_top', array(
			));
		}
	?>

	<div class="tabbable main-selector-area">

		<?php if($this->showLeftBar): ?>
 		<!-- LEFT MENU -->
		<ul id="selector-tabs" class="nav nav-tabs nav-stacked pull-left">
			
			<?php if ($this->tabIsEnabled(UsersSelector::TAB_ASSETS)) : ?>
				<li>
					<a data-toggle="tab" href="#assets">
						<span class="i-sprite is-man"></span>&nbsp;&nbsp;<?php echo Yii::t('standard', 'Assets'); ?>
					</a>
				</li>
			<?php endif; ?>

			<?php if ($this->tabIsEnabled(UsersSelector::TAB_CHANNELS)) : ?>
				<li>
					<a data-toggle="tab" href="#channels">
						<span class="i-sprite is-man"></span>&nbsp;&nbsp;<?php echo Yii::t('standard', 'Channels'); ?>
					</a>
				</li>
			<?php endif; ?>

	
			<?php if ($this->tabIsEnabled(UsersSelector::TAB_USERS)) : ?>
				<li>
					<a data-toggle="tab" href="#users">
						<span class="i-sprite is-man"></span>&nbsp;&nbsp;<?php echo Yii::t('standard', '_USERS'); ?>
					</a>
				</li>
			<?php endif; ?>
		
		
			<?php if ($this->tabIsEnabled(UsersSelector::TAB_GROUPS)) : ?>
				<li>
					<a data-toggle="tab" href="#groups">
						<span class="i-sprite is-couple"></span>&nbsp;&nbsp;<?php echo Yii::t('standard', '_GROUPS'); ?>
					</a>
				</li>
			<?php endif; ?>
				
			
			<?php if ($this->tabIsEnabled(UsersSelector::TAB_ORGCHARTS)) : ?>	
				<li>
					<a data-toggle="tab" href="#orgchart">
						<span class="i-sprite is-path"></span>&nbsp;&nbsp;<?php echo Yii::t('organization_chart', '_ORG_CHART'); ?>
					</a>
				</li>
			<?php endif; ?>
			
			
			<?php if ($this->tabIsEnabled(UsersSelector::TAB_COURSES)) : ?>
				<li>
					<a data-toggle="tab" href="#courses">
						<span class="	i-sprite is-objects"></span>&nbsp;&nbsp;<?php echo Yii::t('standard', '_COURSES'); ?>
					</a>
				</li>
			<?php endif; ?>
			
			
			<?php if ($this->tabIsEnabled(UsersSelector::TAB_PLANS)) : ?>
				<li>
					<a data-toggle="tab" href="#plans">
						<span class="	i-sprite is-objects"></span>&nbsp;&nbsp;<?php echo Yii::t('myactivities', 'Learning plans'); ?>
					</a>
				</li>
			<?php endif; ?>
			
			<?php if ($this->tabIsEnabled(UsersSelector::TAB_PU_PROFILES)) : ?>
				<li>
					<a data-toggle="tab" href="#puprofiles">
						<span class="	i-sprite is-objects"></span>&nbsp;&nbsp;<?php echo Yii::t('standard', 'Power User Profiles'); ?>
					</a>
				</li>
			<?php endif; ?>
			
			<?php
			// Raise an event for external apps to add extra tabs
			$extraTabsEvent = new DEvent($this, array());
			Yii::app()->event->raise('OnRenderCustomReportTabs', $extraTabsEvent);

			if($extraTabsEvent->return_value['overrideDefault'] == true && !empty($extraTabsEvent->return_value['extraLinks'])){
				foreach($extraTabsEvent->return_value['extraLinks'] as $link){
					if($selectorParams['idForm'] == 'selector-'.$link['belongsTo']){
						echo "<li>
							<a data-toggle='tab' href='{$link['href']}'>
								<span class='	i-sprite {$link['class']}'></span>&nbsp;&nbsp;{$link['tag']}
							</a>
						</li>";
					}
				}
			}
			?>
			
		</ul>
		<!-- LEFT MENU -->
		<?php endif; ?>
	
	
		<!-- CONTENT -->
		
		<div class="tab-content">
	
			<?php if (Yii::app()->user->hasFlash('error')) : ?>
    			<div class='alert alert-error alert-compact text-center'>
    				<?= Yii::app()->user->getFlash('error'); ?>
    				<button class="close" data-dismiss="alert" type="button">&times;</button>
    			</div>
			<?php endif; ?>

			<?php if ($this->tabIsEnabled(UsersSelector::TAB_ASSETS)) : ?>
				<div id="assets" class="tab-pane">
					<?php
						// Do NOT render this when we are called to Grid-Update OTHER grids or we are NOT in grid-update (e.g. non AJAX)
						// Significantly improves the response speed !!!!!!!!!
						if (!$this->inGridUpdate || $this->inAssetsGridUpdate) {
	 						$this->renderPartial('_assets', array(
	 							'assetsDataProvider' 	=> $assetsDataProvider,
								'assetsGridId' 			=> $assetsGridId,
								'ajaxVarAssetsGrid'		=> $ajaxVarAssetsGrid,
								'selectorParams'		=> $selectorParams,
	 						));
						} 
					?>
				</div>
			<?php endif; ?>
			
			<?php if ($this->tabIsEnabled(UsersSelector::TAB_CHANNELS)) : ?>
				<div id="channels" class="tab-pane active">
					<?php
						// Do NOT render this when we are called to Grid-Update OTHER grids or we are NOT in grid-update (e.g. non AJAX)
						// Significantly improves the response speed !!!!!!!!!
						if (!$this->inGridUpdate || $this->inChannelsGridUpdate) {
	 						$this->renderPartial('_channels', array(
	 							'channelsDataProvider' 	=> $channelsDataProvider,
								'channelsGridId' 		=> $channelsGridId,
								'ajaxVarChannelsGrid'	=> $ajaxVarChannelsGrid,
								'selectorParams'		=> $selectorParams,
	 						));
						} 
					?>
				</div>
			<?php endif; ?>


			
			<?php if ($this->tabIsEnabled(UsersSelector::TAB_USERS)) : ?>
				<div id="users" class="tab-pane">
					<?php
						// Do NOT render this when we are called to Grid-Update OTHER grids or we are NOT in grid-update (e.g. non AJAX)
						// Significantly improves the response speed !!!!!!!!!
						if (!$this->inGridUpdate || $this->inUsersGridUpdate) {
	 						$this->renderPartial('_users', array(
	 							'usersDataProvider' 	=> $usersDataProvider,
								'usersGridId' 			=> $usersGridId,
								'ajaxVarUsersGrid'		=> $ajaxVarUsersGrid,
								'selectorParams'		=> $selectorParams,
	 						));
						} 
					?>
				</div>
			<?php endif; ?>
			
		
			<?php if ($this->tabIsEnabled(UsersSelector::TAB_GROUPS)) : ?>		
				<div id="groups" class="tab-pane">
					<?php
						if (!$this->inGridUpdate || $this->inGroupsGridUpdate) {
	 						$this->renderPartial('_groups', array(
	 							'groupsDataProvider' 	=> $groupsDataProvider,
								'groupsGridId' 			=> $groupsGridId,
								'ajaxVarGroupsGrid'		=> $ajaxVarGroupsGrid,
								'selectorParams'		=> $selectorParams,
	 						));
						}
					?>
				</div>
			<?php endif; ?>
			
			
			
			<?php if ($this->tabIsEnabled(UsersSelector::TAB_ORGCHARTS)) : ?>
				<div id="orgchart" class="tab-pane">
				
					<div class="filter-wrapper">
						<div class="row-fluid">
							<div class="span4">
								<div class="mass-selector">
									<?= Yii::t('standard', 'You have selected') ?> <strong><span id="orgcharts-selected-count">0</span> <?= Yii::t('standard', 'items') ?></strong>
									<br> 
									<a href="#" class="select-all"><?= Yii::t('standard', '_SELECT_ALL') ?></a>|
									<a href="#" class="deselect-all"><?= Yii::t('standard', '_UNSELECT_ALL') ?></a>
								</div>
							</div>	
						
							<div class="span5 legend">
								<?php if (UsersSelector::orgChartLegendEnabled($this->type)) : ?>
									<span class="icon-select-node-yes"></span> <?php echo Yii::t('standard', '_YES')?>
									<span class="icon-select-node-no"></span> <?php echo Yii::t('standard', '_NO')?>
									<span class="icon-select-node-descendants"></span> <?php echo Yii::t('standard', '_INHERIT')?>
								<?php endif; ?>
							</div>
							
							<div class="span3">
								<input name="fancytree-filter-input" class="orgchart-filter-input" placeholder="<?= Yii::t('standard', '_SEARCH') ?>">
								<button id ="fancytree-filter-clear-button" type="button" class="close filter-clear-button">&times;</button>
							</div>
							
						</div>
					</div>
					
					
					
					<?php /** This element is loaded by ajax. See bottom of this file **/?>
					<div id="orgchart-container">
						<br>
						<br>
						<div class="ajaxloader"><?= Yii::t("standard", "_LOADING") ?>...</div>
					</div>
					
				</div>
			<?php endif; ?>
			
			
			
			<?php if ($this->tabIsEnabled(UsersSelector::TAB_COURSES)) : ?>
				<div id="courses" class="tab-pane <?= (!$this->showLeftBar)? 'active' : '' ?>">
					<?php
						if (!$this->inGridUpdate || $this->inCoursesGridUpdate) {
							$this->renderPartial('_courses', array(
								'coursesDataProvider' 	=> $coursesDataProvider,
								'coursesGridId' 		=> $coursesGridId,
								'ajaxVarCoursesGrid'	=> $ajaxVarCoursesGrid,
								'selectorParams'		=> $selectorParams,
	 						));
						}
	 				?>
				</div>
			<?php endif; ?>
			
			
			<?php if ($this->tabIsEnabled(UsersSelector::TAB_PLANS)) : ?>
				<div id="plans" class="tab-pane">
					<?php
						if (!$this->inGridUpdate || $this->inPlansGridUpdate) {
							$this->renderPartial('_plans', array(
								'plansDataProvider' 	=> $plansDataProvider,
								'plansGridId' 			=> $plansGridId,
								'ajaxVarPlansGrid'		=> $ajaxVarPlansGrid,
								'selectorParams'		=> $selectorParams,
							));
						}
	 				?>
				</div>
			<?php endif; ?>
			
			<?php if ($this->tabIsEnabled(UsersSelector::TAB_PU_PROFILES)) : ?>
				<div id="puprofiles" class="tab-pane">
					<?php
						if (!$this->inGridUpdate || $this->inPuProfilesGridUpdate) {
							$this->renderPartial('_puprofiles', array(
								'puProfilesDataProvider' 	=> $puProfilesDataProvider,
								'puProfilesGridId' 			=> $puProfilesGridId,
								'ajaxVarPuProfilesGrid'		=> $ajaxVarPuProfilesGrid,
								'selectorParams'			=> $selectorParams,
							));
						}
	 				?>
				</div>
			<?php endif; ?>

			<?php if ($this->tabIsEnabled(UsersSelector::TAB_CERTIFICATION)) : ?>
				<div id="certification" class="tab-pane active">
					<?php

					if (!$this->inGridUpdate || $this->inCertificationGridUpdate) {
						$this->renderPartial('_certification', array(
								'certificationDataProvider' 	=> $certificationDataProvider,
								'certificationGridId' 			=> $certificationGridId,
								'ajaxVarCertificationGrid'		=> $ajaxVarCertificationGrid,
								'selectorParams'				=> $selectorParams,
						));
					}
					?>
				</div>
			<?php endif; ?>

            <?php if ($this->tabIsEnabled(UsersSelector::TAB_BADGES)) : ?>
                <div id="badges" class="tab-pane active">
                    <?php
                    if (!$this->inGridUpdate || $this->inBadgesGridUpdate) {
                        $this->renderPartial('_badges', array(
                            'badgesDataProvider' 	=> $badgesDataProvider,
                            'badgesGridId' 			=> $badgesGridId,
                            'ajaxVarBadgesGrid'		=> $ajaxVarBadgesGrid,
                            'selectorParams'		=> $selectorParams,
                        ));
                    }
                    ?>
                </div>
            <?php endif; ?>

			<?php if ($this->tabIsEnabled(UsersSelector::TAB_CONTESTS)) : ?>
				<div id="contests" class="tab-pane active">
					<?php
					if (!$this->inGridUpdate || $this->inContestsGridUpdate) {
						$this->renderPartial('_contests', array(
								'contestsDataProvider' 	=> $contestsDataProvider,
								'contestsGridId' 			=> $contestsGridId,
								'ajaxVarContestsGrid'		=> $ajaxVarContestsGrid,
								'selectorParams'		=> $selectorParams,
						));
					}
					?>
				</div>
			<?php endif; ?>
			
		</div>
		
	</div>
	
	
	<?php if ($showNavButtons) : ?>
	
		<!-- For Dialog2 - they go in footer -->
		<div class="form-actions text-right" >
		
			<?php if (!$wizardMode) : ?>
			
				<?= CHtml::submitButton(Yii::t('standard', '_CONFIRM'), array('class' => 'user-selector-confirm btn btn-docebo green big', 'name' => 'confirm')); ?>
				
			<?php else : ?>
			
				<?php if (!$wizardModeFirst) : ?>
					<?= CHtml::submitButton(Yii::t('standard', '_PREV'), 		array(
							'name'  => 'prev_button',
							'class' => 'btn btn-docebo green big jwizard-nav', 
							'data-jwizard-url' => $wizardModePrevUrl)); 
					?>
				<?php endif; ?>	
				
				<?php if (!$wizardModeLast) : ?>
					<?php
						$btnTxt = ($wizardModeBtnNextTxt && $wizardModeBtnNextTxt!='')? $wizardModeBtnNextTxt : Yii::t('standard', '_NEXT') ;
						echo CHtml::submitButton( $btnTxt , 		array(
							'name'  => 'next_button',
							'class' => 'btn btn-docebo green big jwizard-nav next-page',
							'data-jwizard-url' => $wizardModeNextUrl)); 
					?>
				<?php endif; ?>
		
				<?php if ($wizardModeFinal) : ?>
					<?= CHtml::submitButton(Yii::t('standard', '_SAVE'), 		array('name' => 'jwizard_save_button',  'class' => 'btn btn-docebo green big jwizard-nav jwizard-finish', 'data-jwizard-url' => $wizardModeFinishUrl)); ?>
				<?php endif; ?>
				
				
				
			<?php endif; ?>
			
			<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-docebo black big close-dialog')); ?>
				
		</div>
	
	
	<?php endif; ?>

	
	<?php
		if ($bottomPanel) { 
			$this->renderPartial($bottomPanel, array(
			));
		}
		else {
			$this->renderPartial('_panel_bottom', array(
			));
		}
	?>
	
	

</div>

<?php
$params = array_merge($selectorParams, array('useSessionData' => $this->useSessionData, 'id_report'=>intval($_GET['id'])));
$renderOrgChartUrl = Docebo::createLmsUrl('usersSelector/AxRenderOrgChart', $params);
?>

<script type="text/javascript">
//<![CDATA[
           
		var containerId = '<?= $selectorContainer ?>';
		var formId = '<?= $formId ?>';
		var dialogClass = '<?= $dialogClass ?>';

		// Discover the ID of the modal body (i.e. the Dialog2 ID)
		var modalBodyId = $('#'+containerId).closest('.modal-body').attr('id');
		// Selector options
		var options = {
			// Our selector is universal, we have to pass all possible information to usersselector.js: type, group, course, power user, etc.
			selectorParams:			<?= CJSON::encode($selectorParams) ?>,
			containerId: 			containerId,
			dialogId: 				modalBodyId,
			dialogClass: 			dialogClass,
			dialogTitle:			'<?= addslashes($this->title) ?>',
			formId: 				formId,
			orgchartsFancyTreeId: 	'<?= $this->orgchartsFancyTreeId ?>',
			usersGridId	: 			'<?= $usersGridId ?>',
			groupsGridId: 			'<?= $groupsGridId ?>',
			coursesGridId: 			'<?= $coursesGridId ?>',
			plansGridId: 			'<?= $plansGridId ?>',
			puProfilesGridId: 		'<?= $puProfilesGridId ?>',
			certificationGridId:	'<?= $certificationGridId?>',
            badgesGridId:	        '<?= $badgesGridId?>',
			contestsGridId:			'<?=$contestsGridId?>',
			assetsGridId:			'<?=$assetsGridId?>',
			channelsGridId:			'<?=$channelsGridId?>',
			selectAllUsersUrl:		'<?= Docebo::createLmsUrl('usersSelector/axGetUsersList') ?>',
			selectAllGroupsUrl:		'<?= Docebo::createLmsUrl('usersSelector/axGetGroupsList') ?>',
			selectAllCoursesUrl:	'<?= Docebo::createLmsUrl('usersSelector/axGetCoursesList') ?>',
			selectAllPlansUrl:		'<?= Docebo::createLmsUrl('usersSelector/axGetPlansList') ?>',
			selectAllPuProfilesUrl: '<?= Docebo::createLmsUrl('usersSelector/axGetPuProfilesList') ?>',
			selectAllCertificationsUrl: '<?= Docebo::createLmsUrl('usersSelector/axGetCertificationsList') ?>',
            selectAllBadgesUrl:     '<?= Docebo::createLmsUrl('usersSelector/axGetBadgesList') ?>',
			selectAllContestsUrl:   '<?= Docebo::createLmsUrl('usersSelector/axGetContestsList') ?>',
			selectAllAssetsUrl:		'<?= Docebo::createLmsUrl('usersSelector/axGetAssetsList') ?>',
			selectAllChannelsUrl:	'<?= Docebo::createLmsUrl('usersSelector/axGetChannelsList') ?>',
			successCallback:		'<?= $successCallback ?>'

		};


		// Create this object; Note: It is GLOBAL (no var here)
		var usersSelector = new UsersSelectorClass(options);

		<?php if (!$wizardMode) : ?>
		// This is a standalone UsersSelector dialog, lets listen for auto-close
		usersSelector.addDialogAutoCloseListener();
		<?php endif; ?>

		$('.ajaxloader').show();

		// READY
		$(function(){

			// Do not allow clicks on disabled jwizard navigation buttons 
	        $(document).on('click', '.jwizard-nav.disabled', function(){return false;});
	        $(document).on('click', '.btn.user-selector-confirm.disabled', function(){return false;});
			
			<?php if ($this->tabIsEnabled(UsersSelector::TAB_ORGCHARTS)) : ?>
			var url = '<?= $renderOrgChartUrl ?>';
			// If wizard mode, wizard buttons (Nex/Prev/...) must be disabled until tree is loaded 
			$(".jwizard-nav").addClass("disabled");
			$(".btn.user-selector-confirm").addClass("disabled");
			
			$('#orgchart-container').load(url, function(){
				// Docebo.log('You must see this ONLY ONCE: when dialog is initially loaded!');
				// In case we are in a Wizard mode, we have to notify the jWizard to restore
				// selections in FancyTree, because jWizard already did it upon dialog loading (dialog2.update)
				// BUT we missed that moment since we were still "lazy" .loading the fancytree... so we are late..
				// so.. lets ask uncle jWizard to do us a favor and restore data .. again... Doh... Clever!
				// Note: update ONLY this field name: orgcharts_selected-json
				
				// enable back jwizard buttons, if any				
				$(".jwizard-nav").removeClass("disabled");
				$(".btn.user-selector-confirm").removeClass("disabled");
				$(document).trigger('notify-jwizard-restore-data', {fieldNames: ['orgcharts_selected-json']});
				$(document).trigger('users-selector-orgchart-loaded', {});
			})
			<?php endif; ?>

			// Lets see if we have to show specific TAB, or the first one (default)
			<?php if ($this->tab !== false) : ?>
			$('#selector-tabs a[href="#' + '<?= $this->tab ?>' + '"]').tab('show');
			<?php else : ?>
			$('#selector-tabs a:first').tab('show');
			<?php endif; ?>
		});
		
//]]>
</script>

<?php echo CHtml::endForm() ; ?>
