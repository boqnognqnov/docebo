<?php
/**
 * Created by PhpStorm.
 * User: NikVas
 * Date: 01-Dec-15
 * Time: 12:18
 */

$maxSuggestionsNumber = 10;
?>
    <div class="filter-wrapper">
        <div class="row-fluid">
            <div class="span6">
                <div class="row-fluid">
                    <div class="span12 mass-selector">
                        <?= Yii::t('standard', 'You have selected') ?> <strong><span id="badges-selected-count">0</span> <?= Yii::t('standard', 'items') ?></strong> (<?=  Yii::t('gradebook', '_MAX_DIVISOR') . ' ' . $badgesDataProvider->totalItemCount ?>)
                        <br>
                        <a href="#" class="select-all"><?= Yii::t('standard', '_SELECT_ALL') ?></a>|
                        <a href="#" class="deselect-all"><?= Yii::t('standard', '_UNSELECT_ALL') ?></a>
                    </div>
                </div>
            </div>
            <div class="span6">
                <?php

                // Add overall selector params, so controller can do filterng
                $params = $selectorParams;
                $params['max_number'] = (int) $maxSuggestionsNumber;

                $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                    'id' 				=> 'badges-search-input',
                    'name' 				=> 'search_input',
                    'value' 			=> '',
                    'options' => array(
                        'minLength' => 1,
                    ),
                    'source' => Docebo::createAbsoluteLmsUrl('GamificationApp/GamificationApp/Autocomplete', $params),
                    'htmlOptions' => array('placeholder' => Yii::t('standard', '_SEARCH')),
                ));
                ?>
                <button id ="courses-grid-search-clear-button" type="button" class="close grid-search-clear">&times;</button>
                <span id ="courses-search-button" class="search-field-icon i-sprite is-search"></span>
            </div>
        </div>
    </div>
<?php

$columns = array(
    array(
        'class' => 'CCheckBoxColumn',
    ),
    array(
        'name' => 'icon',
        'value'=> '\'<img style="height:30px;" src="\'.CoreAsset::model()->findByPk($data[icon])->getUrl().\'">\'',
        'type' => 'html'
    ),
    array(
        'name'		=> 'title',
        'value' 	=> '$data[name]'
    ),
    array(
        'name'      => 'description',
        'value'     => '$data[description]',
        'type'      => 'html'
    ),
    array(
        'name'      => 'points',
        'value'     => '$data[points]',
    )
);


$this->widget('UsersSelectorCGridView', array(
    'selectableRows' 	=> 3,
    'id' 				=> $badgesGridId,
    'ajaxType' 			=> 'POST',
    'ajaxVar'			=> $ajaxVarBadgesGrid,
    'template' 			=> '{pager}{items}{pager}',
    'summaryText'		=> '<strong>' . Yii::t('code', _CODE_USED_NUMBER) . ':' . ' {count}</strong>',
    'dataProvider' 		=> $badgesDataProvider,
    'columns'			=> $columns,
    'updateSelector'	=> "{page}, {sort}, #courses-search-button",
    'itemsCssClass' 	=> 'items non-quartz-items',

    'beforeAjaxUpdate'	=> "function(id,options) {
			usersSelector.beforeGridAjaxUpdate(id,options);
		}",

    'afterAjaxUpdate'	=> "function(id,data) {
			usersSelector.afterGridAjaxUpdate(id,data);
		}",

    'selectionChanged'	=> "function(id) {
			usersSelector.gridSelectionChanged(id);
		}",
));

?>