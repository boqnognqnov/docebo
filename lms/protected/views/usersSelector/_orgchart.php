<?php
$this->widget('common.extensions.fancytree.FancyTree', array(
	'id' => $orgchartsFancyTreeId,
	'treeData' => $fancyTreeData,
	'selectMode'  => $selectMode,
	'preSelected' => $preselectedBranches,
	'formFieldNames' => array(
		'selectedInputName'	 		=> 'orgcharts_selected',
		'activeInputName' 			=> 'orgcharts_active_node',
		'selectModeInputName'		=> 'orgcharts_select_mode',
	),
	// http://www.wwwendt.de/tech/fancytree/doc/jsdoc/global.html#FancytreeOptions
	'fancyOptions' => array(
		'checkbox' 	=> true,
		'icons'		=> false,
		'filter' 	=> array(
		),
	),

	'htmlOptions'	=> array(
		'class' => '',
	), 
		
	'filterInputFieldName' 	=> 'fancytree-filter-input',
	'filterClearElemId'  	=> 'fancytree-filter-clear-button',
		

	// http://www.wwwendt.de/tech/fancytree/doc/jsdoc/global.html#FancytreeEvents		
	'eventHandlers' => array(
		'click' => 'function(event, data){
			
		}',
		'blur' 	=> 'function(event, data){
			
		}',
		'select' => 'function(event, data) {
			usersSelector.orgchartSelectCallBack(event, data);
		}',
			
		'beforePreselection' => 'function(event,data) {
			
		}',

		'afterPreselection' => 'function(event,data) {
			// Count elements after pre selection is done
			usersSelector.orgchartSelectCallBack(event, data);
		}',
						
			
	),
));
