<?php
$maxSuggestionsNumber = 10;
?>
<div class="filter-wrapper">
    <div class="row-fluid">
        <div class="span6">
            <div class="row-fluid">
                <div class="span12 mass-selector">
                    <?= Yii::t('standard', 'You have selected') ?> <strong><span id="contests-selected-count">0</span> <?= Yii::t('standard', 'items') ?></strong> (<?=  Yii::t('gradebook', '_MAX_DIVISOR') . ' ' . $contestsDataProvider->totalItemCount ?>)
                    <br>
                    <a href="#" class="select-all"><?= Yii::t('standard', '_SELECT_ALL') ?></a>|
                    <a href="#" class="deselect-all"><?= Yii::t('standard', '_UNSELECT_ALL') ?></a>
                </div>
            </div>
        </div>
        <div class="span6">
            <?php

            // Add overall selector params, so controller can do filterng
            $params = $selectorParams;
            $params['max_number'] = (int) $maxSuggestionsNumber;

            $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'id' 				=> 'contests-search-input',
                'name' 				=> 'search_input',
                'value' 			=> '',
                'options' => array(
                    'minLength' => 1,
                ),
                'source' => Docebo::createAbsoluteLmsUrl('GamificationApp/Contests/contestsAutocomplete'),
                'htmlOptions' => array('placeholder' => Yii::t('standard', '_SEARCH')),
            ));
            ?>
            <button id ="courses-grid-search-clear-button" type="button" class="close grid-search-clear">&times;</button>
            <span id ="contests-search-button" class="search-field-icon i-sprite is-search"></span>
        </div>
    </div>
</div>





<?php

$columns = array(
    array(
        'class' => 'CCheckBoxColumn',
    ),
    array(
        'name'		=> 'name',
        'value' 	=> '$data[name]'
    ),
    array(
        'name' => 'description',
        'type' => 'html'
    ),
    array(
        'name' => 'goal',
        'value' => 'GamificationContest::renderContestGoal($data)',
        'type' => 'raw',
        'header' => Yii::t('gamification', 'Goal'),
        'htmlOptions'=>array('style'=>'max-width: 300px; word-wrap: break-word; vertical-align:top'),
        'headerHtmlOptions' => array(
        ),
    ),
    array(
        'name' => 'period',
        'type' => 'raw',
        'value' => 'GamificationContest::renderContestPeriod($data)',
        'header' => Yii::t('gamification', 'Contest period')
    ),
);


$this->widget('UsersSelectorCGridView', array(
    'selectableRows' 	=> 3,
    'id' 				=> $contestsGridId,
    'ajaxType' 			=> 'POST',
    'ajaxVar'			=> $ajaxVarContestsGrid,
    'template' 			=> '{pager}{items}{pager}',
    'summaryText'		=> '<strong>' . Yii::t('code', _CODE_USED_NUMBER) . ':' . ' {count}</strong>',
    'dataProvider' 		=> $contestsDataProvider,
    'columns'			=> $columns,
    'updateSelector'	=> "{page}, {sort}, #contests-search-button",
    'itemsCssClass' 	=> 'items non-quartz-items',

    'beforeAjaxUpdate'	=> "function(id,options) {
			usersSelector.beforeGridAjaxUpdate(id,options);
		}",

    'afterAjaxUpdate'	=> "function(id,data) {
			usersSelector.afterGridAjaxUpdate(id,data);
		}",

    'selectionChanged'	=> "function(id) {
			usersSelector.gridSelectionChanged(id);
		}",
));
?>

