<?php
	$maxSuggestionsNumber = 10; 
?>
<div class="filter-wrapper">
	<div class="row-fluid">
		<div class="span6">
			<div class="row-fluid">
				<div class="span12 mass-selector">
					<?= Yii::t('standard', 'You have selected') ?> <strong><span id="groups-selected-count">0</span> <?= Yii::t('standard', 'items') ?></strong>  (<?= Yii::t('gradebook', '_MAX_DIVISOR') . ' ' .  $groupsDataProvider->totalItemCount ?>)
					<br> 
					<a href="#" class="select-all"><?= Yii::t('standard', '_SELECT_ALL') ?></a>|
					<a href="#" class="deselect-all"><?= Yii::t('standard', '_UNSELECT_ALL') ?></a>
				</div>
			</div>
		</div>
		<div class="span6">
				<?php
				 
				// Add overall selector params, so controller can do filterng
				$params = $selectorParams;
				$params['max_number'] = (int) $maxSuggestionsNumber;
				
				$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
						'id' 				=> 'groups-search-input',	
						'name' 				=> 'search_input',
						'value' 			=> '',
						'options' => array(
							'minLength' => 1,
						),
						'source' => Docebo::createAbsoluteLmsUrl('usersSelector/axGroupsAutocomplete', $params),
						'htmlOptions' => array('placeholder' => Yii::t('standard', '_SEARCH')),
					));
				?>
				<button id ="groups-grid-search-clear-button" type="button" class="close grid-search-clear">&times;</button>				
				<span id ="groups-search-button" class="search-field-icon i-sprite is-search"></span>
		</div>
	</div>
</div>




<?php

	$columns = array(
		array(
			'class' => 'CCheckBoxColumn',
		),

		array(
			'header'	=> Yii::t('standard', '_NAME'),
			'value' 	=> '$data[groupid]'
		),

		array(
			'header'	=> Yii::t('standard', '_DESCRIPTION'),
			'value' 	=> '$data[description]'
		),
	);


	$this->widget('UsersSelectorCGridView', array(
		'selectableRows' 	=> 3,
		'id' 				=> $groupsGridId,
		'ajaxType' 			=> 'POST',
		'ajaxVar'			=> $ajaxVarGroupsGrid,
		'template' 			=> '{pager}{items}{pager}',
		'summaryText'		=> '<strong>' . Yii::t('standard', _GROUPS) . ':' . ' {count} </strong>',
		'dataProvider' 		=> $groupsDataProvider,
		'columns'			=> $columns,
		'updateSelector'	=> "{page}, {sort},  #groups-search-button",
		'itemsCssClass' 	=> 'items non-quartz-items',

		'beforeAjaxUpdate'	=> "function(id,options) {
			usersSelector.beforeGridAjaxUpdate(id,options); 
		}",

		'afterAjaxUpdate'	=> "function(id,data) {
			usersSelector.afterGridAjaxUpdate(id,data);
		}",

		'selectionChanged'	=> "function(id) {
			usersSelector.gridSelectionChanged(id);
		}",
	));



?>

