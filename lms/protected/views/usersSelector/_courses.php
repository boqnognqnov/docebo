<?php
	$maxSuggestionsNumber = 10; 
?>
<div class="filter-wrapper">
	<div class="row-fluid">
		<div class="span6">
			<div class="row-fluid">
				<div class="span12 mass-selector">
					<?= Yii::t('standard', 'You have selected') ?> <strong><span id="courses-selected-count">0</span> <?= Yii::t('standard', 'items') ?></strong> (<?=  Yii::t('gradebook', '_MAX_DIVISOR') . ' ' . $coursesDataProvider->totalItemCount ?>)
					<br> 
					<a href="#" class="select-all"><?= Yii::t('standard', '_SELECT_ALL') ?></a>|
					<a href="#" class="deselect-all"><?= Yii::t('standard', '_UNSELECT_ALL') ?></a>
				</div>
			</div>
		</div>
		<div class="span6">
				<?php 
				
				// Add overall selector params, so controller can do filterng
				$params = $selectorParams;
				$params['max_number'] = (int) $maxSuggestionsNumber;
				
				$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
						'id' 				=> 'courses-search-input',	
						'name' 				=> 'search_input',
						'value' 			=> '',
						'options' => array(
							'minLength' => 1,
						),
						'source' => Docebo::createAbsoluteLmsUrl('usersSelector/axCoursesAutocomplete', $params),
						'htmlOptions' => array('placeholder' => Yii::t('standard', '_SEARCH')),
					));
				?>
				<button id ="courses-grid-search-clear-button" type="button" class="close grid-search-clear">&times;</button>
				<span id ="courses-search-button" class="search-field-icon i-sprite is-search"></span>
		</div>
	</div>
</div>




<?php

	$columns = array(
		array(
			'class' => 'CCheckBoxColumn',
		),
		array(
			'name'		=> 'code',	
			'header'	=> Yii::t('standard', '_COURSE_CODE'),
			'value' 	=> '$data[code]'
		),
		array(
			'name'		=> 'name',
			'header'	=> Yii::t('standard', '_COURSE_NAME'),
			'value' 	=> '$data[name]',
			'htmlOptions'	=> array("class" => 'usersSelectorColumn300')
		)
    );


// prepare the array of course ids to exclude
Yii::app()->event->raise('OnUserCoursesSelectorShowCoursesTab',
    new DEvent($this, array('columns' => &$columns)));


    $columns[] = 	array(
			'name'		=> 'course_type',	
			'header'	=> Yii::t('course', '_COURSE_TYPE'),
			'value' 	=> 'ucfirst($data[course_type])' 
		);


	$this->widget('UsersSelectorCGridView', array(
		'selectableRows' 	=> 3,
		'id' 				=> $coursesGridId,
		'ajaxType' 			=> 'POST',
		'ajaxVar'			=> $ajaxVarCoursesGrid,
		'template' 			=> '{pager}{items}{pager}',
		'summaryText'		=> '<strong>' . Yii::t('code', _CODE_USED_NUMBER) . ':' . ' {count}</strong>',
		'dataProvider' 		=> $coursesDataProvider,
		'columns'			=> $columns,
		'updateSelector'	=> "{page}, {sort}, #courses-search-button",
		'itemsCssClass' 	=> 'items non-quartz-items',

		'beforeAjaxUpdate'	=> "function(id,options) {
			usersSelector.beforeGridAjaxUpdate(id,options); 
		}",

		'afterAjaxUpdate'	=> "function(id,data) {
			usersSelector.afterGridAjaxUpdate(id,data);
		}",

		'selectionChanged'	=> "function(id) {
			usersSelector.gridSelectionChanged(id);
		}",
	));

	
	
?>

