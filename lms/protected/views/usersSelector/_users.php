<?php
	$maxSuggestionsNumber = 10; 
?>
<div class="filter-wrapper">
	<div class="row-fluid">
		<div class="span6">
			<div class="row-fluid">
				<div class="span12 mass-selector">
					<?= Yii::t('standard', 'You have selected') ?> <strong><span id="users-selected-count">0</span> <?= Yii::t('standard', 'items') ?></strong> (<?=  Yii::t('gradebook', '_MAX_DIVISOR') . ' ' . $usersDataProvider->totalItemCount ?>)
					<br> 
					<a href="#" class="select-all"><?= Yii::t('standard', '_SELECT_ALL') ?></a>|
					<a href="#" class="deselect-all"><?= Yii::t('standard', '_UNSELECT_ALL') ?></a>
				</div>
			</div>
		</div>
		<div class="span6">
				<?php 
				
				// Add overall selector params, so controller can do filterng
				$params = $selectorParams;
				$params['max_number'] = (int) $maxSuggestionsNumber;
				
				$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
						'id' 				=> 'users-search-input',	
						'name' 				=> 'search_input',
						'value' 			=> '',
						'options' => array(
							'minLength' => 3,
						),
						'source' => Docebo::createAbsoluteLmsUrl('usersSelector/axUsersAutocomplete', $params),
						'htmlOptions' => array('placeholder' => Yii::t('standard', '_SEARCH')),
					));
				?>
				<button id ="users-grid-search-clear-button" type="button" class="close grid-search-clear">&times;</button>
				<span 	id ="users-search-button" class="search-field-icon i-sprite is-search"></span>
		</div>
	</div>
</div>




<?php

	$columns = array(
		array(
			'class' => 'CCheckBoxColumn',
		),
		array(
			'name'		=> 'userid',
			'header'	=> Yii::t('standard', '_USERNAME'),
			'value' 	=> '$data[userid]',
			'htmlOptions'	=> array("class" => 'usersSelectorColumn300')
		),
		array(
			'name'		=> 'firstname',
			'header'	=> Yii::t('standard', '_FIRSTNAME'),
			'value' 	=> '$data[firstname]'
		),
 		array(
 			'name'		=> 'lastname',		
 			'header'	=> Yii::t('standard', '_LASTNAME'),
 			'value' 	=> '$data[lastname]'
 		),

	);

	if($this->type == UsersSelector::TYPE_REPORT_VISIBILITY){
		$columns[] = array(
			'header' => Yii::t('standard', 'Roles'),
			'value' => function($data) {
				$userModel = CoreUser::model()->findByPk($data['idUser']);
				return Yii::t('admin_directory', '_DIRECTORY_'.$userModel->adminLevel->groupid);
			}
		);
	}else{
		$columns[]= 'email';
	}

	$this->widget('UsersSelectorCGridView', array(
		'selectableRows' 	=> 3,
		'id' 				=> $usersGridId,
		'ajaxType' 			=> 'POST',
		'ajaxVar'			=> $ajaxVarUsersGrid,
		'template' 			=> '{pager}{items}{pager}',
		'summaryText'		=> '<strong>' . Yii::t('code', _CODE_USED_NUMBER) . ':' . ' {count}</strong>',
		'dataProvider' 		=> $usersDataProvider,
		'columns'			=> $columns,
		'updateSelector'	=> "{page}, {sort}, #users-search-button",
		'itemsCssClass' 	=> 'items non-quartz-items',

		'beforeAjaxUpdate'	=> "function(id,options) {
			usersSelector.beforeGridAjaxUpdate(id,options); 
		}",

		'afterAjaxUpdate'	=> "function(id,data) {
			usersSelector.afterGridAjaxUpdate(id,data);
		}",

		'selectionChanged'	=> "function(id) {
			usersSelector.gridSelectionChanged(id);
		}",
	));

	
	
?>

