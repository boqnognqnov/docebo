<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 17.11.2015 г.
 * Time: 14:49
 */

	$maxSuggestionsNumber = 10;
?>
<div class="filter-wrapper">
    <div class="row-fluid">
        <div class="span6">
            <div class="row-fluid">
                <div class="span12 mass-selector">
                    <?= Yii::t('standard', 'You have selected') ?> <strong><span id="certification-selected-count">0</span> <?= Yii::t('standard', 'items') ?></strong> (<?=  Yii::t('gradebook', '_MAX_DIVISOR') . ' ' . $certificationDataProvider->totalItemCount ?>)
                    <br>
                    <a href="#" class="select-all"><?= Yii::t('standard', '_SELECT_ALL') ?></a>|
                    <a href="#" class="deselect-all"><?= Yii::t('standard', '_UNSELECT_ALL') ?></a>
                </div>
            </div>
        </div>
        <div class="span6">
            <?php

            // Add overall selector params, so controller can do filterng
            $params = $selectorParams;
            $params['max_number'] = (int) $maxSuggestionsNumber;

            $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'id' 				=> 'certification-search-input',
                'name' 				=> 'search_input',
                'value' 			=> '',
                'options' => array(
                    'minLength' => 1,
                ),
                'source' => Docebo::createAbsoluteLmsUrl('CertificationApp/CertificationApp/certsAutocomplete', $params),
                'htmlOptions' => array('placeholder' => Yii::t('standard', '_SEARCH')),
            ));
            ?>
            <button id ="courses-grid-search-clear-button" type="button" class="close grid-search-clear">&times;</button>
            <span id ="courses-search-button" class="search-field-icon i-sprite is-search"></span>
        </div>
    </div>
</div>




<?php

$columns = array(
    array(
        'class' => 'CCheckBoxColumn',
    ),
    array(
        'name'		=> 'title',
        'value' 	=> '$data[title]'
    ),
    array(
        'name' => 'description',
        'type' => 'html'
    ),
    array(
        'name' => 'expiration',
        'value' => 'LearningReportFilter::renderCertificationExpirationColumn($data)',
        'type' => 'raw',
        'header' => Yii::t('standard', 'Expiration'),
        'htmlOptions'=>array('style'=>'max-width: 300px; word-wrap: break-word; vertical-align:top'),
        'headerHtmlOptions' => array(
        ),
    )
);


$this->widget('UsersSelectorCGridView', array(
    'selectableRows' 	=> 3,
    'id' 				=> $certificationGridId,
    'ajaxType' 			=> 'POST',
    'ajaxVar'			=> $ajaxVarCertificationGrid,
    'template' 			=> '{pager}{items}{pager}',
    'summaryText'		=> '<strong>' . Yii::t('code', _CODE_USED_NUMBER) . ':' . ' {count}</strong>',
    'dataProvider' 		=> $certificationDataProvider,
    'columns'			=> $columns,
    'updateSelector'	=> "{page}, {sort}, #courses-search-button",
    'itemsCssClass' 	=> 'items non-quartz-items',

    'beforeAjaxUpdate'	=> "function(id,options) {
			usersSelector.beforeGridAjaxUpdate(id,options);
		}",

    'afterAjaxUpdate'	=> "function(id,data) {
			usersSelector.afterGridAjaxUpdate(id,data);
		}",

    'selectionChanged'	=> "function(id) {
			usersSelector.gridSelectionChanged(id);
		}",
));



?>

