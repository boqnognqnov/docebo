<?php
	$maxSuggestionsNumber = 10; 
?>
<div class="filter-wrapper">
	<div class="row-fluid">
		<div class="span6">
			<div class="row-fluid">
				<div class="span12 mass-selector">
					<?= Yii::t('standard', 'You have selected') ?> <strong><span id="assets-selected-count">0</span> <?= Yii::t('standard', 'items') ?></strong> (<?=  Yii::t('gradebook', '_MAX_DIVISOR') . ' ' . $assetsDataProvider->totalItemCount ?>)
					<br> 
					<a href="#" class="select-all"><?= Yii::t('standard', '_SELECT_ALL') ?></a>|
					<a href="#" class="deselect-all"><?= Yii::t('standard', '_UNSELECT_ALL') ?></a>
				</div>
			</div>
		</div>
		<div class="span6">
				<?php 
				
					// Add overall selector params, so controller can do filterng
					$params = $selectorParams;
					$params['max_number'] 	= (int) $maxSuggestionsNumber;
					$params['selector_tab'] = UsersSelector::TAB_ASSETS;
					
					$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
						'id' 				=> 'assets-search-input',	
						'name' 				=> 'search_input',
						'value' 			=> '',
						'options' => array(
							'minLength' => 1,
						),
						'source' => Docebo::createAbsoluteLmsUrl('usersSelector/axSelectorAutocomplete', $params),
						'htmlOptions' => array('placeholder' => Yii::t('standard', '_SEARCH')),
					));
				?>
				<button id ="assets-grid-search-clear-button" type="button" class="close grid-search-clear">&times;</button>
				<span id ="assets-search-button" class="search-field-icon i-sprite is-search"></span>
		</div>
	</div>
</div>




<?php

	$columns = array(
		array(
			'class' => 'CCheckBoxColumn',
		),
		array(
			'name'		=> 'title',	
			'header'	=> Yii::t('standard', 'TITLE'),
			'value' 	=> '$data[title]'
		),
		array(
			'name'		=> 'publisher',	
			'header'	=> Yii::t('standard', 'PUBLISHED BY'),
			'value' 	=> '$data[publisher]'
		),
		array(
			'name'		=> 'datepublished',	
			'header'	=> Yii::t('standard', 'PUBLISHED ON'),
			'value' 	=> '$data[datePublished]'
		),
		array(
			'name'		=> 'channels',	
			'header'	=> Yii::t('standard', 'CHANNELS'),
			'value' 	=> 'App7020Assets::getChannelsNames($data[idAsset])'
		),
	);
	
	$this->widget('UsersSelectorCGridView', array(
		'selectableRows' 	=> 3,
		'id' 				=> $assetsGridId,
		'ajaxType' 			=> 'POST',
		'ajaxVar'			=> $ajaxVarAssetsGrid,
		'template' 			=> '{pager}{items}{pager}',
		'summaryText'		=> '<strong>' . Yii::t('code', _CODE_USED_NUMBER) . ':' . ' {count}</strong>',
		'dataProvider' 		=> $assetsDataProvider,
		'columns'			=> $columns,
		'updateSelector'	=> "{page}, {sort}, #assets-search-button",
		'itemsCssClass' 	=> 'items non-quartz-items',

		'beforeAjaxUpdate'	=> "function(id,options) {
			usersSelector.beforeGridAjaxUpdate(id,options); 
		}",

		'afterAjaxUpdate'	=> "function(id,data) {
			usersSelector.afterGridAjaxUpdate(id,data);
		}",

		'selectionChanged'	=> "function(id) {
			usersSelector.gridSelectionChanged(id);
		}",
	));

	
	
?>

