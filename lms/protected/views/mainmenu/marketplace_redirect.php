<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
    </head>
    <style>
        html, body {
            height: 100%;
            margin: 0;
        }
        .container {
            min-height: 100%;
        }
        .container > .marketplace_loader {
            width: 170px;
            height: 120px;
            padding: 25px 0;
            background: url('<?php echo Yii::app()->theme->baseUrl ?>/images/loading_big.gif') no-repeat;
            text-align: center;
            position: absolute;
            top: 50%;
            left: 50%;
            margin: -85px 0 0 -85px;
        }
        .container > .marketplace_loader > .image {
            margin: 0 auto;
            width: 40px;
            height: 40px;
            background: url('<?php echo Yii::app()->theme->baseUrl ?>/images/menu/sprite-admin.png') 7px -213px no-repeat;
        }
        .container > .marketplace_loader > .title {
            margin: 20px auto 0;
            width: 100px;
            color: #666;
            font-size: 12px;
            font-family: 'Open Sans',​sans-serif;
        }
    </style>
    <body>
        <div class="container">
            <div class="marketplace_loader">
                <div class="image"></div>
                <div class="title"><?php echo Yii::t('marketplace', '_REDIRECTING_TO_MARKETPLACE') ?></div>
            </div>
        </div>
        
        <form id="redirectmp" method="POST" action="<?php echo $redirect_url ?>">
		<?php foreach ($params as $name => $value) : ?>
			<input type="hidden" id="<?php echo $name ?>" name="<?php echo $name ?>" value="<?php echo $value ?>">
		<?php endforeach; ?>
		</form>
		<script type='text/javascript'>
			window.onload = function () {
                setTimeout(function(){
                    document.getElementById('redirectmp').submit();
                }, 500);
            };
		</script>
    </body>
</html>