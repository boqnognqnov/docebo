<iframe id="marketplace_area" onLoad="locationChanged(this.contentWindow);" src="<?php echo $this->createUrl('mainmenu/marketplaceRedirect') ?>" width="100%" height="100%" frameborder="0">
  <p>Your browser does not support iframes.</p>
</iframe>
<script type="text/javascript">
    $(document).ready(function(){
        if ($('#marketplace_area').height() < $('.menu').height())
            $('#marketplace_area').height($('.menu').height());
    });

    /**
     * Monitor location changes inside the marketplace frame and if the location
     * inside the frame matches the LMS homepage, just redirect the top frame
     * to that page as well. This happens when the user clicks "Go back to LMS" from
     * inside the Marketplace
     *
     * @param theLocation the current location/URL inside the iframe
     */
	var locationChanged = function(theLocation){
		if(theLocation.location == '<?=Yii::app()->getBaseUrl(true).'/';?>'){
			window.location = '<?=Docebo::createLmsUrl('site/index');?>';
		}
	}
</script>