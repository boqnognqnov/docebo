<div class="progress-container">
	<div id="progress-error"></div>
	<div id="job-html">
		<div id="loader" style="width: 36px; margin: 0 auto;"><img src="<?php echo Yii::app()->theme->baseUrl ?>/images/loading_medium.gif"></div>
	</div>
    <div id="final-message" style="display: none;">
        <?php
            if(!empty($additionalData['finalMessage'])) {
                echo $additionalData['finalMessage'];
            }
        ?>
    </div>
</div>

<script type="text/javascript">

	(function($){

		var url = '<?= $jobActionUrl ?>';
		var additionalData = <?= !empty($additionalData) ? CJavaScript::encode($additionalData) : '{}' ?>;
		var xhr = null;
		var modalBody = $('.progress-container').closest('.modal-body');
		var modalId = modalBody.attr('id');
		var modal = $('.progress-container').closest('.modal');

		//this is an attempt to provide global access to progress operation by external js functions
		$.activeProgressOperation = {
			url: url,
			xhr: xhr,
			modal: modal,
			modalBody: modalBody,
			status: 'starting'
		};

		/**
		 * Recursively called until certain condition is met
		 */
		function continueProgress(data) {

			//check if we have to add additional post data
			if (additionalData && $.isPlainObject(additionalData) && !$.isEmptyObject(additionalData)) {
				var postData = $.extend(data, additionalData);
			} else {
				var postData = data;
			}

			xhr = $.ajax({
				url: url,
				type: 'post',
				dataType: 'json',
				data: postData,
				success: function(res) {
					if (res.success == false) {
						$('#job-html').html('');
						$('#progress-error').html(res.message);
						$.activeProgressOperation = false;
					}
					else {
						// Replace the job-html element withthe incoming HTML
						$('#job-html').html(res.html ? res.html : '');
						// Trigger Dialog2 update event; It will ajaxify all the new content
						modalBody.trigger("dialog2.content-update");//$('#'+ modalId).trigger("dialog2.content-update");
						if (!res.data.stop && $.activeProgressOperation && $.activeProgressOperation.status == 'progressing') {
							continueProgress(res.data ? res.data : {});
						} else {
							$.activeProgressOperation = false;
						}

                        if(res.data.stop) {
                            $('#final-message').show();
                        }
					}
				}
			});
			$.activeProgressOperation.xhr = xhr;
		}


		// NO URL means we've got nothing to do. Show error.
		if (!url) {
			$('#progress-error').html('No job action URL specified');
			$.activeProgressOperation = false;
		}
		else {
			// This is the FIRST call, pass "start" parameter to job action
			// Also add $_POST variables to the request. The $_POST data will land at the JOB executor at the first request (start=1).
			// Then, it is up to it to decide what data to pipe to the further Progress cycling.
			var data = {};
			<?php if ($_POST) : ?>
				data = <?php echo json_encode($_POST); ?>; 
			<?php endif; ?> 
			data.start = 1;
			data.__progress_start__ = 1;  // this will replace 'start', as 'start' might be part of the request (POST/GET). Just to keep namespace clean
			$.activeProgressOperation.status = 'progressing';
			continueProgress(data);
		}

		// Abort AJAX call upon closing the dialog
		modalBody.on('dialog2.closed', function(){//$('#'+modalId).on('dialog2.closed', function(){
			xhr.abort();
			$.activeProgressOperation = false;
		});

		// Add specific class to this .modal
		modal.addClass('progressive-dialog');
        modal.addClass('<?=$additionalData['customDialogClass']?>');

		// If dialog title is empty, put inside standard "loading" TEXT (!)
		if (modal.find('.modal-header h3').is(':empty')) {
			modal.find('.modal-header h3').html('<?= Yii::t('standard', '_LOADING') ?>...');
		}	
		
		
	})(jQuery);


</script>