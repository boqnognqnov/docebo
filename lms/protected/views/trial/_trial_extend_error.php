<h2><?= Yii::t("standard", "_OPERATION_FAILURE")?></h2>
<h4><?= Yii::t("standard", $msg) ?></h4>

<script type="text/javascript">
	$(document).on("dialog2.closed", ".modal", function() {
		var e = $(this);
		window.location = '<?= $redirect_url; ?>';
	});
</script>