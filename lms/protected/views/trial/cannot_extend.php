<?php

    $contactUsUrl = "http://www.docebo.com/e-learning-contact-docebo/";
    $isGuest = Yii::app()->user->getIsGuest();
    $buySubscriptionUrl = "../doceboCore/index.php?r=adm/userlimit/buymore";

    if (!$isGuest) {
        $buyNowUrl = $buySubscriptionUrl;
    }
    else {
        $buyNowUrl = $this->createUrl("user/axLogin", array('reason' => 'trialextensioncode', 'redirect_url' => urlencode($buySubscriptionUrl)));
    }

    $name = $contact_name ? $contact_name : "";

?>
<div class="trial-ext-content">
	<h2><?= Yii::t("standard", "Welcome {user}!", array('{user}'=>'<span class="text-colored">'. $name .'</span>')) ?></h2>

	<div class="extension-info">
		<?= Yii::t("standard", "Your Free Trial Period of: {url} cannot be extended due to invalid link or extension already been performed", array(
			'{url}' => '<div class="saas-url text-colored">'.$url.'</div>'
		)) ?>
		<br/>
	    <?php if ($msg) : ?>
	        <h4 class='small'>
	            <?= $msg ?>
	        </h4>
        <?php endif; ?>
        <br>
	    <div style="width: 50%">
	        <?= Yii::t("standard", "Buy now a monthly or yearly subscription plan in order to continue using your Docebo E-Learning platform") ?>
	    </div>
		<br>

	    <?php if ($isGuest) : ?>
            <a
                href="<?= $buyNowUrl ?>"
                class="btn nice-btn docebo green btn-buy open-dialog"
                rel="dialog-trial-login">
	    <?php else : ?>
	        <a href="<?= $buyNowUrl ?>" class="btn nice-btn docebo green btn-buy  ">
	    <?php endif; ?>
	    <i></i> <?= Yii::t("userlimit", "Buy now") ?></a>

	    <a href="<?= $contactUsUrl ?>" class="btn nice-btn docebo btn-contact black"><i></i> <?= Yii::t("standard", "Contact us") ?></a>

	</div>


</div>



<!--  JS ------------------------------------------------------------------------------ -->
<script type="text/javascript">
/*<![CDATA[*/





/*]]>*/
</script>

