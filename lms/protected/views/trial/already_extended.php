<?php
    $name = $contact_name ? $contact_name : "";
?>
<div class="trial-ext-content">
	<h2><?= Yii::t("standard", "Welcome {user}!", array('{user}'=>'<span class="text-colored">'. $name .'</span>')) ?></h2>

	<div class="extension-info">
		<?= Yii::t("standard", "Your Free Trial Period of: {url} has already been extended and it is now active!", array(
			'{url}' => '<div class="saas-url text-colored">'.$url.'</div>'
		)) ?><br/>
	</div>

	<a href="<?=$url;?>" class="btn nice-btn docebo green btn-lmscontinue"><i></i><?= Yii::t("standard", "Log in now") ?></a>
	<br>
	<small><?= Yii::t('standard', '* you can login at any time and use the platform') ?></small>

</div>


<!--  JS ------------------------------------------------------------------------------ -->
<script type="text/javascript">
/*<![CDATA[*/


$(function(){

});

/*]]>*/
</script>

