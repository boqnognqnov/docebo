<img src="<?php echo Yii::app()->theme->baseUrl ?>/images/trial/trial_ext_success_modal_bg.png" alt="">

<h2><?= Yii::t("standard", "Congratulations") ?></h2>
<h4><?= Yii::t("standard", "Your trial period has been extended") ?>!</h4>

<p><?= Yii::t("standard", "Your new trial period will expire at <strong class=\"text-colored\">{newExpiryDate}", array('{newExpiryDate}'=>'<br>'.$newDate.'</strong>')) ?></p>
<br>

<a href="<?= $redirect_url ?>" class="btn nice-btn docebo green"><?= ucfirst(Yii::t('test', '_TEST_END_BACKTOLESSON')); ?></a>

<script type="text/javascript">
	$(document).on("dialog2.content-update", ".modal", function() {
		var e = $(this);
		$('.modal .modal-header .close').hide();
	});
</script>