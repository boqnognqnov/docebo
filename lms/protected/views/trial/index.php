<?php
$ajaxUrl = Yii::app()->createUrl('trial/axExtendTrialPeriod', array('code'=>$code, "contact_id" => $contact_id));
$name = $contact_name ? ", $contact_name" : "";
?>
<div class="trial-ext-content">
	<h2><?= Yii::t("standard", "Welcome {user}!", array('{user}' => '<span class="text-colored">'. $name .'</span>')) ?></h2>

	<div class="extension-info">
		<?= Yii::t("standard", "Your Free Trial Period of: {url} will be extended for {ndays} days", array(
			'{url}' => '<div class="saas-url text-colored">'.$url.'</div>',
			'{ndays}' => '<span class="text-colored">'.$numDays.'</span>'
		)) ?>
	</div>

	<a href="<?= $ajaxUrl ?>" data-dialog-class="metro trial-extended-modal" closeOnOverlayClick="false" closeOnEscape="false" class="open-dialog btn btn-success btn-extend-trial"><i></i> <?= Yii::t("standard", "Extend now") ?></a>

	<p class="expire-info"><?= Yii::t("standard", 'Your new trial period will expire at <strong class="text-colored">{newExpiryDate}', array("{newExpiryDate}" => ''.$newExpiryDate.'</strong>')) ?></p>
</div>
