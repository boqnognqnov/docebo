<div id="pagesContent">

	<?php $msg = Yii::app()->user->getFlash('email_status_change');
	if(!empty($msg)) {
		if ($msg == Yii::t('standard', 'You have confirmed your email!')) $class = 'success';
		elseif ($msg == Yii::t('standard', 'Invalid or expired email verification link!')) $class = 'danger';
		else $class = 'info';
		?>
		<div class="alert alert-<?=$class?>" style="text-align: center">
			<?=$msg;?>
		</div>
	<?php } ?>

	<?=Yii::app()->user->getFlash('email_verification_reminder')?>
	<? if($userPages): ?>
		<?  foreach($userPages as $page): ?>
			<div class="pages" id="page-<?=$page->page_id?>">
				<?= html_entity_decode(Yii::app()->htmlpurifier->purify(Yii::t('login',$page->description), 'allowFrameAndTarget')); ?>
			</div>
		<? endforeach; ?>
	<? endif; ?>
</div>
