<?php $customMessageInHeader = CoreLangWhiteLabelSetting::getCustomHeader();
Yii::app()->event->raise('GetMultidomainHeaderMessage', new DEvent($this, array('customMessageInHeader' => &$customMessageInHeader)));
if (!empty($customMessageInHeader))
	echo '<div style="margin: 30px 0 0 165px;" class="allowOlUlNumbering">' . $customMessageInHeader . '</div><div class="clearfix"></div>'; ?>

<div id="bottomLayout">
	<div class="row-fluid hidden-phone">
		<div class="span12">
			<div id="languageBtn">
				<?php
				$msg = Yii::app()->user->getFlash('email_status_change');
				if(!empty($msg)) {
					if ($msg == Yii::t('standard', 'You have confirmed your email!')) $class = 'success';
					elseif ($msg == Yii::t('standard', 'Invalid or expired email verification link!')) $class = 'danger';
					else $class = 'info';
					?>
					<div class="alert alert-<?=$class?>" style="text-align: center">
						<?=$msg;?>
					</div>
				<?php } ?>
				<?=$this->renderPartial('login/_langDropdown')?>
			</div>
			<div id="loginError">
				<?=DoceboUI::getLoginError();?>
			</div>
			<div id="passwordSuccess">
				<?php
				$flashMessage = Yii::app()->user->getFlash('success');
				if(!empty($flashMessage) && $flashMessage == Yii::t('register','Your password has been changed.')) { ?>
					<div class="password-change-success label label-important text-center">
						<?php echo $flashMessage; ?>
					</div>
				<?php } ?>
			</div>
			<? if($userPages): ?>
				<div id="loginPages">
					<ul>
						<?  foreach($userPages as $page): ?>
							<li id="btn-page-<?=$page->page_id?>"><a href="javascript: return true;" onclick="showPage(<?=$page->page_id?>);"><?=Yii::t('login',$page->title)?></a></li>
						<? endforeach; ?>
					</ul>
				</div>
			<? endif; ?>
			<? $this->widget('common.widgets.SiteImageArea'); ?>
		</div>
	</div>

	<div class="loginBottomCnt">
		<?php echo CHtml::beginForm($this->createUrl('site/login'), 'post', array(
			'id' => 'loginFormBottom',
		)) ?>

			<?php if (Yii::app()->event->raise('BeforeLoginFormRender', new DEvent($this, array()))) : ?>

			<div class="pull-left">
				<?php echo CHtml::hiddenField("client_timezone_offset");?>
				<?php echo CHtml::textField('login_userid', (isset(Yii::app()->session['login_userid']) ? Yii::app()->session['login_userid'] : ''), array(
					'placeholder' => Yii::t('standard', '_USERNAME'),
				));?>
				<?php echo CHtml::passwordField('login_pwd', '', array(
					'placeholder' => Yii::t('standard', '_PASSWORD'),
				));?>
			</div>

			<?php
			//let plugins insert their own custom login buttons in the login page header
			$_plugins = PluginManager::getActivePlugins();
			$_excluded = array('GoogleappsApp', 'GooglessoApp');
			if (!empty($_plugins) && is_array($_plugins)) {
				foreach ($_plugins as $_plugin) {
					if ($_plugin instanceof CorePlugin && !in_array($_plugin->plugin_name, $_excluded)) {
						$_module = Yii::app()->getModule($_plugin->plugin_name);
						if (method_exists($_module, 'customLoginButton')) {
							$_customLoginButton = $_module->customLoginButton();
							if (!empty($_customLoginButton)) {
								echo '<div class="loginFormBottomCustomApp" id="loginFormBottom'.$_plugin->plugin_name.'">';
								echo $_customLoginButton;
								echo '</div>';
							}
						}
					}
				}
			}
			?>

			<?php echo CHtml::submitButton(Yii::t('login', '_LOGIN'), array(
				'class' => 'btn btn-docebo green big pull-right',
			));?>

			<?php if(PluginManager::isPluginActive('GoogleappsApp')): ?>
				<div class="pull-right">
					<a title="Google Apps" class="header-lostpass google-bottom" href="<?=Docebo::createLmsUrl('GoogleappsApp/GoogleappsApp/gappsLogin')?>">
						<?/*=Yii::t('login','_LOGIN_WITH') */?><!-- &nbsp;	--><?= CHtml::image(Yii::app()->theme->baseUrl . "/images/loginexternal/googleapps-24.png") ?>
					</a>&nbsp;&nbsp;&nbsp;
				</div>
			<?php endif; ?>
			<?php if(PluginManager::isPluginActive('GooglessoApp') && Docebo::isCurrentLmsDomain()): ?>
				<div class="pull-right">
					<a title="Google Login" class="header-lostpass google-bottom" href="<?=Docebo::createLmsUrl('GooglessoApp/GooglessoApp/login')?>">
						<?/*=Yii::t('login','_LOGIN_WITH') */?><!-- &nbsp;	--><?= CHtml::image(Yii::app()->theme->baseUrl . "/images/loginexternal/google-24.png") ?>
					</a>&nbsp;&nbsp;&nbsp;
				</div>
			<?php endif; ?>
			<?php
				$event = new DEvent($this, array('layout' => 'bottom'));
				Yii::app()->event->raise('RenderLoginPageLinks', $event);
				if($event->return_value['html'])
					echo $event->return_value['html'];
			?>
			<div class="pull-right">
				<?php Social::displaySocialLoginIcons(); ?>
			</div>

			<?php endif; ?>

		<div class="clearfix"></div>

		<div class="pull-left">
			<div class="pull-left">
				<a class="open-dialog dialog-links" rel="dialog-lostdata" data-dialog-class="lostdata" href="<?=Yii::app()->createUrl('user/axLostdata');?>"><?php echo Yii::t('login', '_LOG_LOSTPWD'); ?></a>
			</div>
			<?php if (Settings::get('register_type', 'admin') != 'admin') : ?>
				<div class="pull-left" style="color: #fff;">&nbsp;-&nbsp;</div>
				<div class="pull-left">
					<a class="open-dialog dialog-links" id="register-modal-btn" data-dialog-class="register" rel="dialog-register" href="<?=Yii::app()->createUrl('user/axRegister')?>"><?=Yii::t('user','Register');?></a>
				</div>
			<?php endif; ?>
		</div>

		<?= CHtml::endForm(); ?>
	</div>
		<?php
		$loginMainTitle = Yii::t('login', '_LOGIN_MAIN_TITLE');
		// Raise event to let plugins override the login page title
		Yii::app()->event->raise('GetSigninTitle', new DEvent($this, array('signin_title' => &$loginMainTitle)));
		?>
		<?php if ($loginMainTitle && $loginMainTitle != '_LOGIN_MAIN_TITLE') : ?>
			<h1 id="mainTitleBottom">
				<?php echo $loginMainTitle ?>
			</h1>
		<?php endif; ?>
		<?php
		$loginMainCaption = Yii::t('login', '_LOGIN_MAIN_CAPTION');
		// Raise event to let plugins override the login page caption
		Yii::app()->event->raise('GetSigninCaption', new DEvent($this, array('signin_caption' => &$loginMainCaption)));
		?>
		<?php if ($loginMainCaption && $loginMainCaption != '_LOGIN_MAIN_CAPTION') : ?>
			<div id="mainCaptionBottom">

				<?php
				$expression = '/docebo/i';
				$loginMainCaption=Sanitize::pregLowercaseCall($expression,$loginMainCaption);
				echo $loginMainCaption;
				?>

			</div>
		<?php endif; ?>
	<?=$this->renderPartial('login/_loginPages', array('userPages'=>$userPages));?>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		var d = new Date();
		$("#client_timezone_offset").val(-d.getTimezoneOffset()*60);
		$("a[href*='site/sso']").each(function()
		{
			var $this = $(this);
			var _href = $this.attr("href");
			$this.attr("href", _href + '&offset='+(-d.getTimezoneOffset()*60));
		});
	});

	$(function(){
		if(<?= json_encode($triggerRegistration) ?>){
			$(document).controls();
			$("#register-modal-btn").click();
		}
	});
</script>