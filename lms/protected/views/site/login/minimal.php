<?php
$singleButtonHeight = 48;
$buttonsCounter = (PluginManager::isPluginActive('GooglessoApp') && Docebo::isCurrentLmsDomain()) + PluginManager::isPluginActive('GoogleappsApp') +
    Social::isActive('facebook') + Social::isActive('twitter') + Social::isActive('linkedin') +
	((PluginManager::isPluginActive('SimpleSamlApp') || PluginManager::isPluginActive('OktaApp')) && Yii::app()->event->hasHandler('onAfterMinimalLayoutSocialIcons'));
$errorMsg = DoceboUI::getLoginError();
$boxHeight = 292 + $buttonsCounter*$singleButtonHeight + 44*(!empty($errorMsg));

$rtl = (Lang::isRTL(Yii::app()->getLanguage()));
$directionRtl = 'rtl';
$directionLtr = 'ltr';
?>

<!-- video -->
<div class="video-background">
    <?php if($bgType=='video' && ($minimalBackground || !$fallbackImg)){?>
    <video autoplay loop muted poster='<?= CHtml::image(Yii::app()->theme->baseUrl . "/images/transparent.png") ?>' style="background: transparent url(<?php echo ($fallbackImg)?$fallbackImg:''; ?>) no-repeat 0 0; background-size:cover; ">
        <source src="<?php echo ($minimalBackground)? $minimalBackground:''; ?>" type="video/mp4"/>
    </video>
    <?php }
    elseif (($bgType=='image') || ($bgType=='video' && $fallbackImg)){ ?>
        <img src="<?php echo ($bgType=='video')? $fallbackImg : (($minimalBackground)? $minimalBackground:''); ?>" style="min-width:100%;min-height:100%;background-color: #fff;"/>
    <?php }else{ ?>
        <div style="width:100%;height:100%;background-color:<?php echo $minimalBackground; ?>;"></div>
    <?php } ?>
</div>
<!-- /video -->

<!-- content -->
<div class="content-wrapper" style="direction: <?php echo $rtl ? $directionRtl : $directionLtr ?>">
    <div class="login" style="height: <?=$boxHeight?>px;">
        <h2><?php
            $title = Yii::t('login', '_LOGIN_MAIN_TITLE');
			// Raise event to let plugins override the login page title
			Yii::app()->event->raise('GetSigninTitle', new DEvent($this, array('signin_title' => &$title)));
            echo ($title!='_LOGIN_MAIN_TITLE')? strip_tags(htmlspecialchars_decode($title)) : Yii::t('login',"_LOGIN_LEGEND");?></h2>

        <div id="description-text" class="incipit" style="text-align: center; /*white-space: nowrap;*/ margin-top:20px; margin-bottom: 30px; text-overflow: ellipsis;overflow: hidden; max-height: 55px;text-overflow-multiline:ellipsis; display: -webkit-box;  -webkit-line-clamp: 3;  -webkit-box-orient: vertical;  "><?php
            $descriptions = Yii::t('login', '_LOGIN_MAIN_CAPTION');
			// Raise event to let plugins override the login page caption
			Yii::app()->event->raise('GetSigninCaption', new DEvent($this, array('signin_caption' => &$descriptions)));
            echo ($descriptions!='_LOGIN_MAIN_CAPTION')? strip_tags(htmlspecialchars_decode($descriptions), '<a><img>') : Yii::t('login',"The World's leading source of intelligent information for businesses and professionals.");?></div>

        <?php echo CHtml::beginForm($this->createUrl('site/login'), 'post', array(
            'id' => 'loginFormRight',
            'autocomplete' => 'off',
        )) ?>
        <?php echo CHtml::hiddenField("client_timezone_offset");?>
        <div class="form-group field-loginform-username required">
            <?php echo CHtml::textField('login_userid', '', array(
                'placeholder' => Yii::t('standard', '_USERNAME'),
                'class' => 'form-control',
            ));?>
        </div>
        <div class="form-group field-loginform-password required">
            <?php echo CHtml::passwordField('login_pwd', '', array(
                'placeholder' => Yii::t('standard', '_PASSWORD'),
                'class' => 'form-control',
            ));?>
        </div>
        <div id="loginError">
            <?=$errorMsg?>
        </div>
        <div id="passwordSuccess">
            <?php
            $flashMessage = Yii::app()->user->getFlash('success');
            if(!empty($flashMessage) && $flashMessage == Yii::t('register','Your password has been changed.')) { ?>
                <div class="password-change-success-minimal label label-important text-center">
                    <?php echo $flashMessage; ?>
                </div>
            <?php } ?>
        </div>
        <?php echo CHtml::submitButton(Yii::t('login',"_LOGIN_LEGEND"), array( 'class' => 'btn btn-primary btn-lg btn-block btn-docebo green',));?>
        <div class="sso-btns">
            <?php Social::displaySocialLoginButtons(); ?>
            <?php if(PluginManager::isPluginActive('GooglessoApp') && Docebo::isCurrentLmsDomain()): ?>
                <div class="sso-login-btn google-btn">
                    <a title="Google Login"  href="<?=Docebo::createLmsUrl('GooglessoApp/GooglessoApp/login')?>">
                        <?= CHtml::image(Yii::app()->theme->baseUrl . "/images/loginexternal/google-25.png") ?><span><?=Yii::t('login','Sign in with Gmail') ?></span>
                    </a>
                </div>
            <?php endif; ?>
            <?php if(PluginManager::isPluginActive('GoogleappsApp')): ?>
                <div class="sso-login-btn google-btn">
                    <a title="Google Apps" href="<?=Docebo::createLmsUrl('GoogleappsApp/GoogleappsApp/gappsLogin')?>">
                        <?= CHtml::image(Yii::app()->theme->baseUrl . "/images/loginexternal/google-plus-24.png") ?><span><?=Yii::t('login','Sign in with Google Apps') ?></span>
                    </a>
                </div>
            <?php endif; ?>
            <? Yii::app()->event->raise('onAfterMinimalLayoutSocialIcons', new DEvent($this, array())) ?>
        </div>
        <div class="lost-pass-container">
            <a class="open-dialog dialog-links" rel="dialog-lostdata" data-dialog-class="lostdata" href="<?=Yii::app()->createUrl('user/axLostdata');?>"><?php echo Yii::t('login', '_LOG_LOSTPWD'); ?></a>
        </div>

        <?php
        //let plugins insert their own custom login buttons in the login page header
        $_plugins = PluginManager::getActivePlugins();
        $_excluded = array('GoogleappsApp', 'GooglessoApp');
        if (!empty($_plugins) && is_array($_plugins)) {
            foreach ($_plugins as $_plugin) {
                if ($_plugin instanceof CorePlugin && !in_array($_plugin->plugin_name, $_excluded)) {
                    $_module = Yii::app()->getModule($_plugin->plugin_name);
                    if (method_exists($_module, 'customLoginButton')) {
                        $_customLoginButton = $_module->customLoginButton();
                        if (!empty($_customLoginButton)) {
                            echo '<div class="minimalistic-or">- '.Yii::t('standard', 'OR').' -</div>';
                            echo '<div class="loginFormMinimalCustomApp" id="loginFormMinimal'.$_plugin->plugin_name.'">';
                            echo $_customLoginButton;
                            echo '</div>';
                        }
                    }
                }
            }
        }
        ?>

        <?php echo CHtml::endForm(); ?>
    </div>
    <!-- /content -->
</div>

<script type="text/javascript">
    $(window).load(function () {
        // Detect if the max-height of the body must be adjusted (for better scrolling of the central box)
        var logoSize = $("#header a.logo img").height();
        if(logoSize > 60)
            $("body").css('min-height', '868px');
    });

    $(document).ready(function(){
        var d = new Date();
        $("#client_timezone_offset").val(-d.getTimezoneOffset()*60);
		$("a[href*='site/sso']").each(function()
		{
			var $this = $(this);
			var _href = $this.attr("href");
			$this.attr("href", _href + '&offset='+(-d.getTimezoneOffset()*60));
		});
    });

    $(function(){
    	if(<?= json_encode($triggerRegistration) ?>){
			$(document).controls();
			$("#register-modal-btn").click();
		}

        // Adjust description text
        if($('#description-text')[0].scrollHeight >  $('#description-text').innerHeight()) {
            var wordArray = $('#description-text').text().split(' ');
            while ($('#description-text')[0].scrollHeight > $('#description-text').innerHeight()) {
                wordArray.pop();
                $('#description-text').text(wordArray.join(' ') + '...');
            }
            wordArray.pop();
            $('#description-text').text(wordArray.join(' ') + '...');
        }
    });
</script>