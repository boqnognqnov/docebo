<div id="topLayout">
	<div class="row-fluid">
		<div class="span12">
			<?php echo CHtml::beginForm($this->createUrl('site/login'), 'post', array(
				'id' => 'loginFormTop',
			)) ?>

			<?php if (Yii::app()->event->raise('BeforeLoginFormRender', new DEvent($this, array()))) : ?>

				<div id="loginFormTopContent">
					<?php echo CHtml::hiddenField("client_timezone_offset");?>
				<?php echo CHtml::textField('login_userid', (isset(Yii::app()->session['login_userid']) ? Yii::app()->session['login_userid'] : ''), array(
					'placeholder' => Yii::t('standard', '_USERNAME'),
				));?>
				<?php echo CHtml::passwordField('login_pwd', '', array(
					'placeholder' => Yii::t('standard', '_PASSWORD'),
				));?>
				<?php echo CHtml::submitButton(Yii::t('login', '_LOGIN'), array(
					'class' => 'btn btn-docebo green big ',
				));?>


					<?php
					//let plugins insert their own custom login buttons in the login page header
					$_plugins = PluginManager::getActivePlugins();
					$_excluded = array('GoogleappsApp', 'GooglessoApp');
					if (!empty($_plugins) && is_array($_plugins)) {
						foreach ($_plugins as $_plugin) {
							if ($_plugin instanceof CorePlugin && !in_array($_plugin->plugin_name, $_excluded)) {
								$_module = Yii::app()->getModule($_plugin->plugin_name);
								if (method_exists($_module, 'customLoginButton')) {
									$_customLoginButton = $_module->customLoginButton();
									if (!empty($_customLoginButton)) {
										echo '<div class="loginFormTopCustomApp" id="loginFormTop'.$_plugin->plugin_name.'">';
										echo $_customLoginButton;
										echo '</div>';
									}
								}
							}
						}
					}
					?>
				<div class="clearfix"></div>
				<div class="google-btns">
                    <?php Social::displaySocialLoginIcons(); ?>
					<?php if(PluginManager::isPluginActive('GoogleappsApp')): ?>
						<div class="pull-left">
							<a title="Google Apps" class="header-lostpass" href="<?=Docebo::createLmsUrl('GoogleappsApp/GoogleappsApp/gappsLogin')?>">
								<?/*=Yii::t('login','_LOGIN_WITH') */?><!-- &nbsp;	--><?= CHtml::image(Yii::app()->theme->baseUrl . "/images/loginexternal/googleapps-24.png") ?>
							</a>&nbsp;&nbsp;&nbsp;
						</div>
					<?php endif; ?>
					<?php if(PluginManager::isPluginActive('GooglessoApp') && Docebo::isCurrentLmsDomain()): ?>
						<div class="pull-left">
							<a title="Google Login" class="header-lostpass" href="<?=Docebo::createLmsUrl('GooglessoApp/GooglessoApp/login')?>">
								<?/*=Yii::t('login','_LOGIN_WITH') */?><!-- &nbsp;	--><?= CHtml::image(Yii::app()->theme->baseUrl . "/images/loginexternal/google-24.png") ?>
							</a>&nbsp;&nbsp;&nbsp;
						</div>
					<?php endif; ?>
				</div>


				<div class="pull-right">
					<a class="open-dialog dialog-links" rel="dialog-lostdata" data-dialog-class="lostdata" href="<?=Yii::app()->createUrl('user/axLostdata');?>"><?php echo Yii::t('login', '_LOG_LOSTPWD'); ?></a>
				</div>
				<?php if (Settings::get('register_type', 'admin') != 'admin') : ?>
					<div class="pull-right">&nbsp;-&nbsp;</div>
					<div class="pull-right">
						<a class="open-dialog dialog-links"  id="register-modal-btn" data-dialog-class="register" rel="dialog-register" href="<?=Yii::app()->createUrl('user/axRegister')?>"><?=Yii::t('user','Register');?></a>
					</div>
				<?php endif; ?>
				<?php
					$event = new DEvent($this, array('layout' => 'top'));
					Yii::app()->event->raise('RenderLoginPageLinks', $event);
					if($event->return_value['html'])
						echo $event->return_value['html'];
				?>
			</div>

			<?php endif; ?>

			<?= CHtml::endForm(); ?>

			<?php $customMessageInHeader = CoreLangWhiteLabelSetting::getCustomHeader();
			Yii::app()->event->raise('GetMultidomainHeaderMessage', new DEvent($this, array('customMessageInHeader' => &$customMessageInHeader)));
			if (!empty($customMessageInHeader))
				echo '<div style="margin: 20px 0 0 0;" class="allowOlUlNumbering">' . $customMessageInHeader . '</div><div class="clearfix"></div>'; ?>

			<div id="loginError">
				<?=DoceboUI::getLoginError();?>
			</div>
			<div id="passwordSuccess">
				<?php
				$flashMessage = Yii::app()->user->getFlash('success');
				if(!empty($flashMessage) && $flashMessage == Yii::t('register','Your password has been changed.')) { ?>
					<div class="password-change-success label label-important text-center">
						<?php echo $flashMessage; ?>
					</div>
				<?php } ?>
			</div>
			<div id="languageBtn">
				<?php
				$msg = Yii::app()->user->getFlash('email_status_change');
				if(!empty($msg)) {
				if ($msg == Yii::t('standard', 'You have confirmed your email!')) $class = 'success';
				elseif ($msg == Yii::t('standard', 'Invalid or expired email verification link!')) $class = 'danger';
				else $class = 'info';
				?>
				<div class="alert alert-<?=$class?>" style="margin-left: 18px;text-align: center">
					<?=$msg;?>
				</div>
				<?php } ?>

				<?=$this->renderPartial('login/_langDropdown')?>
			</div>
			<div class="hidden-phone">
				<? $this->widget('common.widgets.SiteImageArea'); ?>
			</div>
		</div>
	</div>
	<div id="loginTextsTop">
			<?php
			$loginMainTitle = Yii::t('login', '_LOGIN_MAIN_TITLE');
			// Raise event to let plugins override the login page title
			Yii::app()->event->raise('GetSigninTitle', new DEvent($this, array('signin_title' => &$loginMainTitle)));
			?>
			<?php if ($loginMainTitle && $loginMainTitle != '_LOGIN_MAIN_TITLE') : ?>
			<h1 id="mainTitleTop">
				<?php echo $loginMainTitle ?>
			</h1>
		<?php endif; ?>
			<?php
			$loginMainCaption = Yii::t('login', '_LOGIN_MAIN_CAPTION');
			// Raise event to let plugins override the login page caption
			Yii::app()->event->raise('GetSigninCaption', new DEvent($this, array('signin_caption' => &$loginMainCaption)));
			?>
			<?php if ($loginMainCaption && $loginMainCaption != '_LOGIN_MAIN_CAPTION') : ?>
			<div id="mainCaptionTop">

				<?php
				$expression = '/docebo/i';
				$loginMainCaption=Sanitize::pregLowercaseCall($expression,$loginMainCaption);
				echo $loginMainCaption;
				?>
			</div>
		<?php endif; ?>
	</div>
	<? if($userPages): ?>
		<div id="loginPages">
			<ul>
				<?  foreach($userPages as $page): ?>
					<li id="btn-page-<?=$page->page_id?>"><a href="javascript:;" onclick="showPage(<?=$page->page_id?>);"><?=Yii::t('login',$page->title)?></a></li>
				<? endforeach; ?>
			</ul>
		</div>
	<? endif; ?>
	<?=$this->renderPartial('login/_loginPages', array('userPages'=>$userPages));?>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		var d = new Date();
		$("#client_timezone_offset").val(-d.getTimezoneOffset()*60);
		$("a[href*='site/sso']").each(function()
		{
			var $this = $(this);
			var _href = $this.attr("href");
			$this.attr("href", _href + '&offset='+(-d.getTimezoneOffset()*60));
		});
	});

	$(function(){
		if(<?= json_encode($triggerRegistration) ?>){
			$(document).controls();
			$("#register-modal-btn").click();
		}
	});
</script>