<?php
$this->breadcrumbs[] = Yii::t('menu_over', '_MYCOURSES');
/* @var $showCatalogHint bool */ ?>

<?php
	// Give user a chance to subscribe to courses using Subscription Codes plugin
	if (PluginManager::isPluginActive('SubscriptionCodesApp') && !Yii::app()->user->getIsGuest()) {
		$this->widget('plugin.SubscriptionCodesApp.widgets.StudentSelfEnrollment', array());
	}
?>


<div class="mycourses-blank-slate-container">

	<div class="row-fluid">
		<div class="span6">
			<h2 class="text-left"><?= Yii::t('overlay_hints', 'It looks like you have no coures available yet!') ?></h2>
			<br/>
			
			<p>
			<?php if ($showCatalogHint) {
				echo Yii::t('overlay_hints', 'You may want to take a look at your catalog', array('{catalog-link}' => $this->createUrl('index', array('opt'=>'catalog'))) );
			} else {
				echo Yii::t('overlay_hints', 'Try again later');
			} ?>
			</p>
		</div>
	</div>
</div>