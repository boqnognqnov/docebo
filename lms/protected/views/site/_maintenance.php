<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'maintenance-popup',
	'htmlOptions' => array(
		'class' => 'ajax'
	),
)); ?>

	<h1>Scheduled maintenance</h1>

	<div class="text-center">
		<?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/maint_icon.png');?>
	</div>

	<div class="maintenance-dialog-content">
		<div class="row-fluid">
			<?php echo Yii::t("maintenance", "Your LMS will be down for maintenance starting from {date}", array('{date}' => '<br/><span class="maintenance-date">'.$dateFrom.'</span>'))?>
			<div class="clearfix"></div>
			<?php echo Yii::t("maintenance", "The down time won't be longer than {minutes}", array(
				'{minutes}' => '<span class="maintenance-minutes">15 '.Yii::t("standard", "_MINUTES").'</span>'
			)); ?>
			<div class="clearfix"></div>
			<br/>
		</div>
		<div class="row-fluid">
			<h4><?php echo $message; ?></h4>
		</div>
	</div>

	<div class="form-actions">
		<input class="btn btn-docebo green big close-dialog" type="button" value="<?= Yii::t("standard", "_CONFIRM") ?>">
	</div>
</div>
<?php $this->endWidget(); ?>