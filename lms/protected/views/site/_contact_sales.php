<h1>Contact sales team</h1>

<?php if (!($success) ) : ?>
<?php
	echo CHtml::beginForm($action, 'POST', array(
		'class' => 'ajax form-inline'
	));
?>

	<?= CHtml::hiddenField('app_id', $app_id); ?>

<div class="row-fluid">

<!-- Message -->
<div class="control-group">
	<br>
   	<label class="control-label"><?=$descriptionMessage?></label>
   	<div class="controls">
   	   	<br>
   		<?php
   			echo Chtml::textArea('message', $message, array(
           		'id' => 'message',
               	'class' => 'input-xlarge span12',
               	'rows' => 10
           	));
		?>
		<?php if ($errorMessage) : ?>
			<div class="text-colored-red"><?= $errorMessage ?></div>
		<?php endif;?>
	</div>
</div>

</div>


<!-- Forrm Buttons -->
<div class="buttons form-actions">
	<?= CHtml::submitButton(Yii::t('standard', '_SEND'), array('name' => 'confirm_submit', 'class' => 'btn-docebo green big')); ?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'close-dialog btn-docebo black big')); ?>
</div>

<?php echo CHtml::endForm(); ?>


<?php else: ?>

	<div class="row-fluid text-center">
		<h3 style="color: black;"><?= Yii::t('standard', 'Thanks for contacting us, our Team will reach you by email at: {email}', array(
			'{email}' => Yii::app()->user-> getUserAttrib('email')
			)) ?></h3>
	</div>

	<div class="buttons form-actions">
		<a class="btn btn-docebo big black" href="<?= Docebo::createLmsUrl('site/logout') ?>"><?= Yii::t('standard','_LOGOUT') ?></a>
		<a class="btn btn-docebo big black close-dialog" href="#"><?= Yii::t('standard','_CLOSE') ?></a>
	</div>

<?php endif; ?>

