<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/dialog.css" rel="stylesheet">
<style>
	textarea {
		resize: none;
		width: 380px !important;
		padding: 4px 6px !important;
		background-color: #eee !important;
		line-height: 20px !important;
		height: auto !important;
		margin-bottom: 10px !important;
	}
</style>


<h1><?= Yii::t('branding', 'Send the request to Docebo') ?></h1>
<?php if (Yii::app()->user->hasFlash('error-message')) : ?>
    <div id='form-feedback' class='alert alert-error text-center'><?= Yii::app()->user->getFlash('error-message'); ?></div>
<?php endif; ?>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'plan-expiration-contact',
    'htmlOptions' => array(
        'class' => 'ajax'
    ),
)); ?>

    <div class="contact-docebo-dialog">

        <div class="row-fluid">
            <div class="span3">
                <?=Yii::t('standard', 'Subject:')?>
            </div>
            <div class="span9">
                <?=Yii::t('expiration', 'Information about my annual {users} users plan expiration', array('{users}' => $users)); ?>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span3">
                <?=Yii::t('standard', 'Your message:<br/><small>(required)</small>')?>
            </div>
            <div class="span9">
                <textarea id="help-desk-text" name="plan-expiration-message" class="" rows="7"></textarea>
            </div>
        </div>

    </div>

    <div class="text-right">
        <input type="submit" name="confirm_button" value="<?php echo Yii::t('standard', '_SEND'); ?>" class="btn btn-docebo green big">
        &nbsp;
        <a class="close-dialog btn btn-docebo black big"><?php echo Yii::t('standard', '_CANCEL'); ?></a>
    </div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(function(){
		$('#expiration-contact-modal .close-dialog').on('click', function(){
			$('#expiration-contact-modal').dialog2('close');
		});
		$('#expiration-contact-modal input').styler();
	});
</script>