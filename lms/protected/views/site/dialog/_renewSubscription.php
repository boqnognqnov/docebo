<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/dialog.css" rel="stylesheet">
<style>
    .green {
        color: #6BBF6B;
        font-weight: 600;
        font-size: 14px;
    }
    textarea {
        resize: none;
        width: 515px !important;
		padding: 4px 6px !important;
		background-color: #eee !important;
		line-height: 20px !important;
		height: auto !important;
		margin-bottom: 10px !important;
    }
    .message-success {
        border: 2px solid #6BBF6B;
        padding: 10px;
        text-align: left;
        padding-right: 30px;
    }
    .icon-success {
        float: left;
        display:inline-block;
        background: url('<?php echo Yii::app()->theme->baseUrl ?>/images/apps_menu_sprite.png') no-repeat;
        background-position: -60px -92px;
        width: 33px;
        height: 35px;
        zoom: 1.5;
        margin-right: 5px;
    }
</style>


<h1><?= Yii::t('expiration', '_RENEW_SUBSCRIPTION') ?></h1>

<?php if(!$success):?>
    <?php
        $form = $this->beginWidget('CActiveForm', array(
            'method' => 'POST',
            'id' => 'renew-subscription-form',
            'htmlOptions'=>array(
                    'class' => 'ajax form-forizontal',
            ),
        ));
    ?>
    <div>
        <span class="green"><?= Yii::t('expiration', 'You are renewing your {users} users subscription plan', array('{users}' => $users))  ?></span>
        <br />
        <?= Yii::t('expiration', 'You will receive an email with the attached invoice after the plan expires (on the {expire_date})', array('{expire_date}' => $expire_date))  ?>
    </div>
    <br>
    <div class="clearfix"></div>

    <div>
        <strong><?php echo Yii::t('userlimit', 'Terms Of Service'); ?></strong>
    </div>
    <div>
        <textarea readonly="true" rows="7">terms..</textarea>
    </div>
    <div class="row-fluid">
        <div class="span12">
        <label class="checkbox">
            <?php echo CHtml::checkBox('agree', false, array()); ?>&nbsp;
            <?php echo Yii::t('expiration','Yes, I agree'); ?>
        </label>
        </div>
    </div>
    <div class="form-actions">
        <?= CHtml::submitButton(Yii::t('standard', '_CONFIRM'), array('class' => 'btn-docebo big green disabled btn-cancel-submit', 'name' => 'confirm_button')); ?>
        <?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn-docebo big black close-dialog')); ?>
    </div>
    <?php $this->endWidget(); ?>
<?php else: ?>
    <div class="message-success">
        <span class="icon-success"></span>
        <span class="green"><?php echo Yii::t('expiration','Thank you for renewing your {users} users subscription plan!', array('{users}' => $users)); ?></span>
        <br />
        <?= Yii::t('expiration', 'You will receive an email with the attached invoice after the plan expires (on the {expire_date})', array('{expire_date}' => $expire_date))  ?>
    </div>
    <div class="form-actions">
        <?= CHtml::button(Yii::t('standard', '_CLOSE'), array('class' => 'btn-docebo big black close-dialog')); ?>
    </div>
<?php endif;?>



<script type="text/javascript">
<!--

$(function(){
    $('#renew-subscription input').styler();
    
	// Clicks on disabled SUBMIT buttons must be ignored
	$(document).on('click', '.disabled', function(){return false;});
	
	$('input[name="agree"]').on('change', function() {
		$(this).prop("checked") ? $('.btn-cancel-submit').removeClass('disabled') : $('.btn-cancel-submit').addClass('disabled');
	});
    
    if('<?=$success?>')
    {
        $('.t').tooltip('destroy');
        $('.users-info-icon').removeClass('in-expiration');
        $('.users-info-icon .solid-exclam-mini-white').remove();
    }
});


//-->
</script>