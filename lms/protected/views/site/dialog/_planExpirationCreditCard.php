<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/dialog.css" rel="stylesheet">
<style>
    .message {
        padding: 10px 90px;
        border: 2px solid #e67f22 ;
        text-align: center;
        margin-bottom: 15px;
    }
    .message a {
        color: #0465ac;
        text-decoration: underline;
    }
    .hover {
        position: absolute;
        left: 0px;
        top: 0px;
        width: 304px;
        display: none;
		z-index: 1;
    }
	.modal, .modal-body {
		overflow: visible;
	}
</style>


<h1><?= Yii::t('expiration', 'Subscription Expiration') ?></h1>
<div style="text-align: center">
    <img src="<?php echo Yii::app()->theme->baseUrl ?>/images/warning-orange.png" style="margin-bottom: 15px;"/>
</div>
<div class="message">
    <?=Yii::t('expiration', 'Your annual {users} subscriptopn plan will automatically renew itself in {days} days {date}', array(
        '{users}' => Settings::get('max_users'), 
        '{days}' => $days,
        '{date}' => ($expire_date)? '('.$expire_date.')' : '',
        ))?>
    <br />
    <br />
    <?=Yii::t('expiration', 'If you\'d like to make any changes, please head {here}.', array('{here}' => '<strong><a id="hover" href="'.Docebo::createLmsUrl('billing/default/plan').'">here</a></strong>'))?>
    <img class="hover" src="<?php echo Yii::app()->theme->baseUrl ?>/images/change_plans_lens.png">
</div>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'plan-expiration-credit_card',
	'htmlOptions' => array(
		'class' => 'ajax'
	),
)); ?>
    <label class="checkbox">
        <?php echo CHtml::checkBox('remind_me', true)?>
        <?=Yii::t('expiration', 'Remind it to me on the next login')?>
    </label>
    <hr />
    <div style="float:left;">
        <?=Yii::t('userlimit', 'Need more info?')?><a href="#" onclick="contactUs(); return false;" style="color:#0465ac"><?=Yii::t('standard', 'contact us')?></a>
    </div>
    <div style="float:right;">
        <input type="submit" name="submit" value="OK" class="btn btn-docebo green big">
    </div>
<?php $this->endWidget(); ?>
<script>
    $(function(){
		$('#expiration-modal input').styler();
        $("#hover").hover(
        function() {
			var image = $('.hover');
			var left = $(this).position().left - image.width();
			var top = $(this).position().top - image.height()/2;
			image.css({top: top+5, left: left});
			image.show();
        },
        function() {
            $('.hover').hide();
        }
        );
        $('.t').tooltip('destroy');
	});
    function contactUs()
    {
        $('#expiration-modal').dialog2('close');
        $('#contactDialog').trigger('click');        
    }
</script>