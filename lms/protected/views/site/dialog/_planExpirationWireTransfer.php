<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/dialog.css" rel="stylesheet">
<style>
    .message {
        padding: 10px 20px;
        border: 2px solid #e67f22 ;
        text-align: center;
        margin-bottom: 15px;
    }
    .message a {
        color: #0465ac;
        text-decoration: underline;
    }

</style>


<h1><?= Yii::t('expiration', 'Subscription Expiration') ?></h1>
<div style="text-align: center">
    <img src="<?php echo Yii::app()->theme->baseUrl ?>/images/warning-orange.png" style="margin-bottom: 15px;"/>
</div>
<div class="message">
    <?=Yii::t('expiration', 'Your annual {users} users subscription plan expires in {days} days', array(''
        . '{users}' => Settings::get('max_users'), 
        '{days}' => $days))?>
    <?=(!empty($expire_date))?'('.$expire_date.')' : '';?>
    <br />
    <?=Yii::t('expiration', 'Do you want to renew it?');?>
</div>
<div style="text-align: center;">
    <?=  CHtml::button(Yii::t('standard', '_YES'), array('class' => 'btn btn-docebo green big', 'onclick' => 'renewSubscription(); return false;'))?>
    <?=  CHtml::button(Yii::t('standard', '_NO'), array('class' => 'btn btn-docebo red big', 'onclick' => 'terminateSubscription(); return false;'))?>
    <?=  CHtml::button(Yii::t('userlimit', 'Change plan'), array('class' => 'btn btn-docebo black big', 'onclick' => 'contactUs(); return false;'))?>
</div>
<hr />
    <div style="float:left;">
        <?=Yii::t('userlimit', 'Need more info?')?><a href="#" onclick="contactUs(); return false;" style="color:#0465ac"><?=Yii::t('standard', 'contact us')?></a>
    </div>
<script>
    function terminateSubscription()
    {
        $('#expiration-modal').dialog2('close');
        $('#cancelSubscription').trigger('click');        
    }
    function renewSubscription()
    {
        $('#expiration-modal').dialog2('close');
        $('#renewSubscription').trigger('click');        
    }
</script>