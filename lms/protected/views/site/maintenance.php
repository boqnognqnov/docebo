<?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/maint_icon.png', '', array('style' => 'float:left;'));?>
<div class="maintenance-page-content">
	<h4><?php echo Yii::t("maintenance", "We're currently down for maintenance and we expect to be back very soon.")?></h4>
	<br />
	<h4><?php echo $message; ?></h4>
</div>