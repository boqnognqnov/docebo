<!DOCTYPE html>
<html>
    <head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
        <!-- ,user-scalable=no - for safari bug -->
        <link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl ?>/images/favicon.png" type="image/png" />

        <!-- Open sans font -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

        <!-- base js -->
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/jquery.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/app.js?69"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/menu.js"></script>

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>

        <!-- i-sprite Icons -->
        <link href="<?php echo Yii::app()->theme->baseUrl ?>/css/i-sprite.css" rel="stylesheet">
        <!-- p-sprite Icons -->
        <link href="<?php echo Yii::app()->theme->baseUrl ?>/css/player-sprite.css" rel="stylesheet">


        <!-- Application -->
        <link href="<?php echo Yii::app()->theme->baseUrl ?>/css/shared.css" rel="stylesheet">
        <link href="<?php echo Yii::app()->theme->baseUrl ?>/css/base.css" rel="stylesheet">
        <link href="<?php echo Yii::app()->theme->baseUrl ?>/css/base-responsive.css" rel="stylesheet">
        <link href="<?php echo Yii::app()->theme->baseUrl ?>/css/menu.css" rel="stylesheet">

        <!-- template color scheme -->
        <link href="<?php echo Docebo::createLmsUrl('site/colorschemeCss', array('time' => time())) ?>" rel="stylesheet" id="colorSchemeCssFile">

        <?= DoceboUI::custom_css(); ?>

        <?php if(Lang::isRTL(Yii::app()->getLanguage())): ?>
            <!-- RTL styles -->
            <link href="<?php echo Yii::app()->theme->baseUrl ?>/css/bootstrap-rtl.min.css" rel="stylesheet">
            <link href="<?php echo Yii::app()->theme->baseUrl ?>/css/bootstrap-responsive-rtl.min.css" rel="stylesheet">
            <link href="<?php echo Yii::app()->theme->baseUrl ?>/css/rtl.css" rel="stylesheet">
        <?php endif; ?>

        <link href="<?php echo Yii::app()->theme->baseUrl ?>/js/formstyler/jquery.formstyler.docebo.css" rel="stylesheet">
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/formstyler/jquery.formstyler.min.js"></script>

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <link href="<?php echo Yii::app()->theme->baseUrl ?>/css/ie.css" rel="stylesheet">
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/html5.js"></script>
        <![endif]-->

        <!--[if lt IE 8]>
        <link href="<?php echo Yii::app()->theme->baseUrl ?>/css/ie6.css" rel="stylesheet">
        <![endif]-->

        <!--[if (lt IE 9) & (!IEMobile)]>
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/respond.min.js"></script>
        <![endif]-->
        <style>
            body {
                padding: 0;
            }
        </style>
    </head>
    <body>
        <div id="loader" style="width: 36px; margin: 0 auto;"><img src="<?php echo Yii::app()->theme->baseUrl ?>/images/loading_medium.gif"></div>
        <div id="file_area" style="display: none;">
            <?php $form = $this->beginWidget('CActiveForm', array(
                'id' => 'help-desk-file',
                'htmlOptions' => array(
                    'enctype' => 'multipart/form-data',
                    'class' => 'ajax'
                ),
            )); ?>
            <?php if (isset($_SESSION['helpdesk-attachment']['attachment'])) : ?>
            <div>
                <?php
                $attachment = (array) $_SESSION['helpdesk-attachment']['attachment'];
                foreach ($attachment as $key=>$val) {
                    echo $key;
                    break;
                }
                ?> &nbsp;
                <input type="hidden" name="remove_file" value="1">
                <a href="#" id="remove_file_btn"><span class="i-sprite is-remove red" title="<?php echo Yii::t('standard', '_DEL') ?>"></span></a>
            </div>
            <?php else : ?>
            <input type="file" id="help-desk-upload" name="help-desk-upload">
            <?php endif; ?>
            <?php $this->endWidget(); ?>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#loader').hide();
                $('#file_area').show();
                $('input').styler({
                    browseText: '<?php echo Yii::t('course_management', 'CHOOSE FILE') ?>',
                });

                $('#help-desk-upload').change(function(){
                    $('#help-desk-file').submit();
                    $('#loader').show();
                    $('#file_area').hide();
                });
                $('#remove_file_btn').click(function(){
                    $('#help-desk-file').submit();
                    $('#loader').show();
                    $('#file_area').hide();
                });
            });
        </script>
    </body>
</html>