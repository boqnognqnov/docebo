<?php
/* @var $requestLabel string */
/* @var $helpdeskRecipients array */
/* @var $showHelpAndManuals bool */
/* @var $onlySales bool */
?>

<style>
<!--
	.helpdesk-modal.modal {
		top: 5%;
		width: 640px;
		margin-left: -320px;
	}
	.helpdesk-modal .modal-header h3{
		padding-left:0;
	}
	.helpdesk-modal .modal-body {
		height: auto;
		max-height: none;
	}
	.helpdesk-modal .modal-footer{display:none}
-->
</style>


<h1><?= Yii::t('standard', 'Contact us') ?></h1>

<?php if (Yii::app()->user->hasFlash('error')) : ?>
	<div id='form-feedback' class='alert alert-error text-center'><?= Yii::app()->user->getFlash('error'); ?></div>
<?php endif; ?>


<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'help-desk-request',
	'htmlOptions' => array(
		//'enctype' => 'multipart/form-data',
		'class' => 'ajax'
	),
)); ?>
		<?= CHtml::hiddenField('only_sales', $onlySales) ?>

		<div class="control-group request">
			<?php if($onlySales === false): ?>
				<?php if ($requestLabel) : ?>
				<label for="help-desk-text"><i class="icon-help iconrequest"></i> <?php echo $requestLabel; ?></label>
				<?php endif; ?>
			<?php endif; ?>
			<div class="controls form-element">
				<textarea id="help-desk-text" name="help-desk-text" class="" rows="6"><?=$help_desk_text?></textarea>
			</div>
		</div>
		<?php if($onlySales === false): ?>
			<div class="upload">
				<label for="upload"></label>
				<div class="form-element">
					<iframe src="<?php echo Docebo::createLmsUrl('site/AxHelpDeskFile') ?>" scrolling="no" frameborder="0" style="width: 100%; height: 70px; border: none; overflow: hidden;" border="0"></iframe>
				</div>
			</div>
			<?php if ($is_to_docebo) : ?>
			<div class="control-group target">
					<p><?php echo Yii::t('userlimit', 'Choose the kind of support that you want'); ?></p>
					<div class="controls form-element form-radio-element">
						<input type="radio" id="help-desk-hd" name="help-desk-target" value="help" <?= (empty($help_desk_target) || ($help_desk_target == 'help')) ? 'checked="checked"' : ''?> />
						<label class="control-label" for="help-desk-hd"><?php echo Yii::t('userlimit', 'Help Desk'); ?></label>
					</div>
					<div class="controls form-element form-radio-element">
						<input type="radio" id="help-desk-sales" name="help-desk-target" value="sales"  <?=$help_desk_target == 'sales' ? 'checked="checked"' : ''?>  />
						<label class="control-label" for="help-desk-sales"><?php echo Yii::t('userlimit', 'Sales team'); ?></label>
					</div>
			</div>
			<?php else : ?>
				<?php if (!empty($helpdeskRecipients) && is_array($helpdeskRecipients)) : ?>
					<div class="control-group target">
						<p><?php echo Yii::t('userlimit', 'Choose the kind of support that you want'); ?></p>
					<?php $_helpdeskIndex = 0; ?>
					<?php foreach ($helpdeskRecipients as $k => $v) : ?>
						<div class="controls form-element form-radio-element">
							<input type="radio" id="help-desk-custom<?= $_helpdeskIndex ?>" name="help-desk-target" value="<?= (is_numeric($k) ? $v : $k) ?>"  <?= ($help_desk_target == $v || (empty($help_desk_target) && !$_helpdeskIndex)) ? 'checked="checked"' : '' ?>  />
							<label class="control-label" for="help-desk-custom<?= $_helpdeskIndex ?>"><?php echo $v; ?></label>
						</div>
						<?php $_helpdeskIndex++; ?>
					<?php endforeach; ?>
					</div>
				<?php else: ?>
					<input type="hidden" id="help-desk-hd" name="help-desk-target" value="help" />
				<?php endif; ?>
			<?php endif; ?>
		<?php endif; ?>

		<div class="text-right">
            <div style="display: block; text-align: left; float: left; width: 50%">
                <?php if( !PluginManager::isPluginActive('WhitelabelApp') ) : ?>
                    <?php $currentLanguage = Yii::app()->getLanguage(); ?>
                    <?php ($currentLanguage == 'it' ? $it = 'it/' : $it = ''); ?>
                    <?php ($currentLanguage != 'it' && $currentLanguage != 'en' ? $int = '-international' : $int = ''); ?>

                    <p class="privacy-policy" style="font-size: 11px; font-weight: bold;">
                        <?=Yii::t('standard', 'By clicking "Send", you are agreeing to the ')?>
                        <a style="color: #0465AC; text-decoration: underline;" href="https://www.docebo.com/<?=$it?>docebo-privacy-policy<?=$int?>/" target="_blank">
                            <?=Yii::t('standard', 'Privacy Policy')?>
                        </a>
                    </p>
                <?php endif; ?>
            </div>
			<input type="submit" name="confirm_button" value="<?php echo Yii::t('standard', '_SEND'); ?>" class="btn btn-docebo green big">
			&nbsp;
			<a class="close-dialog btn btn-docebo black big"><?php echo Yii::t('standard', '_CANCEL'); ?></a>
		</div>

<?php $this->endWidget(); ?>
<?php if($onlySales === false): ?>
	<?php if ($is_to_docebo) : ?>
		<?php if ($showHelpAndManuals) : ?>
		<div class="clearfix"></div>

		<div class="menu-helpdesk-search">
			<h2><?php echo Yii::t('userlimit', 'Docebo Knowledge Base'); ?></h2>

			<div class="row-fluid">
				<div class="span12">
					<ul class="help-desk">
						<li><span class="i-sprite is-home"></span>
							<a href="<?= $help['index'] ?>"><?= Yii::t('userlimit', 'Help &amp; Support Home') ?></a></li>
						<li><span class="i-sprite is-arrow-right"></span>
							<a href="<?= $help['getting-started'] ?>"><?= Yii::t('userlimit', 'Getting started') ?></a></li>
						<li><span class="i-sprite is-arrow-right"></span>
							<a href="<?= $help['user'] ?>"><?= Yii::t('standard', '_USERS') ?></a></li>
						<li><span class="i-sprite is-arrow-right"></span>
							<a href="<?= $help['courses'] ?>"><?= Yii::t('userlimit', 'Course management') ?></a></li>
						<li><span class="i-sprite is-arrow-right"></span>
							<a href="<?= $help['report'] ?>"><?= Yii::t('userlimit', 'Report') ?></a></li>
						<li><span class="i-sprite is-arrow-right"></span>
							<a href="<?= $help['settings'] ?>"><?= Yii::t('userlimit', 'Settings') ?></a></li>
						<li><span class="i-sprite is-arrow-right"></span>
							<a href="<?= $help['apps'] ?>"><?= Yii::t('userlimit', 'Apps') ?></a></li>
						<li><span class="i-sprite is-arrow-right"></span>
							<a href="<?= $help['integration'] ?>"><?= Yii::t('userlimit', 'Integrations') ?></a></li>
						<li><span class="i-sprite is-arrow-right"></span>
							<a href="<?= $help['payment'] ?>"><?= Yii::t('userlimit', 'Payments & Billing Policies') ?></a></li>
					</ul>
					<script type="text/javascript">
						$('.help-desk a').click(function(e){
							e.preventDefault();
							window.open(this.href);
						});
					</script>
				</div>
			</div>
		</div>
		<?php endif; ?>
	<?php endif; ?>
<?php endif; ?>

<script type="text/javascript">
<!--
	$(function(){
		// Close the menu in order not to hide the dialog
		$('div#close-bar i#close-button').trigger('click');

		$('#helpdesk-modal .close-dialog').on('click', function(){
			$('#helpdesk-modal').dialog2('close');
		});
		$('#helpdesk-modal input').styler();
	});
//-->
</script>