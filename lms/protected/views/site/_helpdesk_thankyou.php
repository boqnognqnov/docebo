<style>
<!--

#helpdesk-modal.modal-body {
	overflow: visible;
}

-->
</style>


<h1><?php echo Yii::t('standard', 'Contact us'); ?></h1>

<div class="row-fluid">
	<?= $message ?><?= $userEmail ?>
</div>

<div class="form-actions">
	<?php
	echo CHtml::tag('input', array(
		'type' => 'submit',
		'class' => 'close-dialog btn btn-docebo black big',
		'value' => Yii::t('standard', '_CLOSE')
	), false, true)
	?>
</div>

