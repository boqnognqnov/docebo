<?php
/* @var $this SiteController */
?>


<?php if (Docebo::isBasicVersion() && Yii::app()->user->isGodAdmin) : ?>

<script type="text/javascript">
var resizeThreeButtons = function(){
	var largestHeight = 0;

	// Loop to find which one of the 3 is largest
	$('.home-courses-access .span4').each(function(){
		var thisElemHeight = $(this).height();
		if(thisElemHeight > largestHeight){
			largestHeight = thisElemHeight;
		}
	});

	// Resize each three's heights to equal height (the largest)
	// so they look consistent.
	$('.home-courses-access .span4').each(function(){
		$(this).height(largestHeight);
	});
	
}

$(function(){
	resizeThreeButtons();
	
	$(window).resize(function(){
		resizeThreeButtons();
	});
	$('.home-courses-access').resize(function(){
		resizeThreeButtons();
	});
});
</script>

<div class="home-courses-access row-fluid">
	<a class="span4 new-course open-dialog" href="<?php echo Yii::app()->createUrl('coursemanagement/axnewcourse'); ?>" data-dialog-class="wide">
		<div class="inner-padding">
			<div class="graphic"></div>
			<h2><?=Yii::t('course','Set-up a new course');?></h2>
			<p><?=Yii::t('course','Load your video, scorm packages'); ?></p>
			<div class="clearfix"></div>
		</div>
	</a>
	<a class="span4 existing-courses" href="../doceboLms/index.php?r=lms/marketplace/gotoMarketplace">
		<div class="inner-padding">
			<div class="graphic"></div>
			<h2><?=Yii::t('course','Select from course library'); ?></h2>
			<p><?=Yii::t('course','Load existing courses from the marketplace.'); ?></p>
			<div class="clearfix"></div>
		</div>
	</a>
	<a class="span4 manage-courses" href="../doceboLms/index.php?r=coursemanagement&sop=unregistercourse">
		<div class="inner-padding">
			<div class="graphic"></div>
			<h2><?=Yii::t('menu_over','_COURSES_MANAGEMENT'); ?></h2>
			<p><?=Yii::t('course','Upload your training material, enroll users');?></p>
			<div class="clearfix"></div>
		</div>
	</a>
	<div class="clearfix"></div>
</div>
<?php endif; ?>
<?php if(Docebo::isAdvancedVersion() && ($gridType==CoursesGrid::$GRIDTYPE_INTERNAL_MYCOURSES) ) : ?>
<div class="top-label-wrapper">
	<div class="pull-left top-label-icon"><img src="<?=Yii::app()->theme->baseUrl?>/images/yourcourses_flat.png"/></div>
	<div class="top-label-text">
		<h1><?=Yii::t('course', 'Your online courses')?></h1>
		<p><?=Yii::t('course', 'click on the course image to start your training.')?></p>
	</div>

	<div class="clearfix"></div>
</div>
<?php endif; ?>