<h3>p-sprite.css</h3>
<div style="background-color: #F2DA6E; padding: 10px;">

<table border=1>
<?php
$file = __DIR__ . '/../../../../themes/spt/css/player-sprite.css';
$file = realpath($file);

//echo $file;
//die;
$fh = fopen($file,'r');
echo "<tr>";
$i = 0;
while (!feof($fh)) {
	$line = fgets($fh, 6000);
	$a = preg_split("/{/", $line);
	$ruleName = trim($a[0]);
	$ruleName = ltrim($ruleName,'.');
	$class = preg_replace('/\./', ' ', $ruleName);
	if (!preg_match("/CHARSET|\/\*/", $ruleName) && !empty($ruleName)) {
		$i++;
		echo "<td><span rel='tooltip' title='' style='float:left;' class='$class'></span></td><td>$ruleName</td>";
	}
	if ($i % 4 == 0) {
		echo "</tr><tr>";
	}
}


?>

</table>
</div>



<script type="text/javascript">

	$('.p-sprite').on('hover', function(){
		$(this).attr('title', $(this).width() + ' x ' + $(this).height());
	});
	
</script>