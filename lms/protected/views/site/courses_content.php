<?php
/**
 * @var $this SiteController
 * @var $gridType string
 */
 $msg = Yii::app()->user->getFlash('email_status_change');
	if(!empty($msg)) {
		if ($msg == Yii::t('standard', 'You have confirmed your email!')) $class = 'success';
		elseif ($msg == Yii::t('standard', 'Invalid or expired email verification link!')) $class = 'danger';
		else $class = 'info';
		?>
		<div class="alert alert-<?=$class?>" style="text-align: center">
			<?=$msg;?>
		</div>
	<?php }

echo Yii::app()->user->getFlash('email_verification_reminder');

// This will draw the extra section in the external login situation (ecommerce with external catalog enabled)
if ($gridType == CoursesGrid::$GRIDTYPE_EXTERNAL) {
	echo '<div class="row-fluid hidden-phone">';
		echo '<div class="span12">';
		?>
		<div class="home-image-wrapper">
		<?php
   			 $this->widget('common.widgets.SiteImageArea');
   		 ?>

		<?php
            $loginMainTitle = Yii::t('login', '_LOGIN_MAIN_TITLE');
            // Raise event to let plugins override the login page title
            Yii::app()->event->raise('GetSigninTitle', new DEvent($this, array('signin_title' => &$loginMainTitle)));
        ?>
		<?php if ($loginMainTitle && $loginMainTitle != '_LOGIN_MAIN_TITLE') : ?>
			<div id="mainTitleBottom">
				<span>
					<?php echo $loginMainTitle ?>
				</span>
			</div>
		<?php endif; ?>
        <?php
            $loginMainCaption = Yii::t('login', '_LOGIN_MAIN_CAPTION');
            // Raise event to let plugins override the login page caption
            Yii::app()->event->raise('GetSigninCaption', new DEvent($this, array('signin_caption' => &$loginMainCaption)));
        ?>
		<?php if ($loginMainCaption && $loginMainCaption != '_LOGIN_MAIN_CAPTION') : ?>
			<div id="mainCaptionBottom">

				<?php
					$expression = '/docebo/i';
					$loginMainCaption=Sanitize::pregLowercaseCall($expression,$loginMainCaption);
					echo $loginMainCaption;
				?>

			</div>
		<?php endif; ?>
		</div>

   		 <?php
		echo '</div>';
	echo '</div>';
}

if (PluginManager::isPluginActive('SubscriptionCodesApp') && !Yii::app()->user->getIsGuest()) {
	$this->widget('plugin.SubscriptionCodesApp.widgets.StudentSelfEnrollment', array());
}


//$this->widget('common.widgets.GridSearchForm', array(
//    'type' => $gridType,
//    'moreDropdownLinks' => array(),
//));

//$this->widget('common.components.CoursesGrid', array(
//	'gridType' => $gridType,
//	'useScrollAndLoad' => true,
//	'useLoadMoreButton' => false,
//	'flt_label' => $flt_label,
//	'flt_catalog' => $flt_catalog,
//	'moreDropdownLinks' => array(),
//));

// FIXME TODO remove me
$dashlet = DashletCatalog::getFullCatalogDashlet();
$singleCellLayout = 'sclo';
$id = $dashlet->id;

?>
<div class="mydashboard" id="mydashboard" data-layout-id="-1">
	<div id="mydashboard-dashlets" class="mydashboard-dashlets">
		<div class="row-fluid dashlets-row" data-row-id="-1" id="row--1">
			<div class="row-fluid">
				<div class="span12 dashlets-row-inner-wrapper">
					<div class="dashlets-cell span12" data-cell-id="-1" data-cell-width="12" data-cell-text-width="full">
						<ul class="dashlets-sortable">

							<li class="dashlet mydashboard.widgets.DashletCatalog" data-dashlet-id="<?= $id; ?>" id="dashlet-<?= $id; ?>">
								<div class="row-fluid">
									<div class="span12 dl-header">
										<div class="span12 dl-header-text">

										</div>
									</div>
								</div>

								<div class="row-fluid">
									<div class="dl-content" style="height: auto;">
										<?php
											$layoutModel = DashboardLayout::getVirtualLayout(DashboardLayout::VIRTUAL_TYPE_SINGLE_CELL, DashboardDashlet::model()->findByPk($id));
										?>
									</div>
								</div>

								<div class="row-fluid">
									<div class="span12 dl-footer"></div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	/*<![CDATA[*/
	$(function () {
		var id = <?= $id; ?>;//$(this).data('dashlet-id');
		var currentAction = getUrlRoute(true);
		currentAction = currentAction.replace('/', '_');
		var cookieName = 'dashlet_display_style_' + id;
	<?php if(Yii::app()->user->isGuest) { ?>
		cookieName += '_public';
	<?php } ?>

		$.cookie(cookieName, 'thumb', {path: '/'});

		if ($.fn.dashboard) {
			$('#mydashboard').dashboard({
				dashControllerUrl: <?= json_encode(Docebo::createLmsUrl('//mydashboard/dash')) ?>,
				adminDashletsMode: false,
				layoutId: <?= json_encode($layoutModel->id) ?>,
				autoLoadContent: true,
				debug: true
			});
		}

	});
	/*]]>*/
</script>

<style type="text/css">
	@media (min-width: 992px) {
		.w-mycourses .catalog-with-sidebar .course-list.full .catalog-row .course-tile:nth-child(4) {
			display: inline-block!important;
		}
	}
</style>
