<iframe 
    width="100%" 
    src="<?= $iframeUrl ?>" 
    id="embedded-iframe"
    frameborder="0"></iframe>
<script>

	// Self-executing closure
    (function(){

    	// Closure scope variables, set upon parent loading
		var ourHeight = <?= json_encode((int) $model->iframe_height) ?>; // our height, taken from the model
        var windowHeight = $(window).height() - 150;
        var parentExists = parent.document.getElementById('legacy-wrapper-iframe');
        if(parentExists){
            windowHeight = windowHeight + 100;
            if(ourHeight > 0) {
                $('body.embed-iframe').css('padding', '5px 0px 122px');
            }
        }

        var iframeElement = $('#embedded-iframe');

        iframeElement.css('height', (ourHeight > 0 ? ourHeight : windowHeight)  + 'px');

        // On Document Ready...
        $(function(){

        	// Listen for any postMessage
            $(window).off("onmessage message").on("onmessage message", function(e){
                var iframeWindow = document.getElementById('embedded-iframe').contentWindow;
                var $iframe = $('#embedded-iframe');
                var event = e.originalEvent;
                // Ensure sending IFRAME is the one know very well
                if((iframeWindow === event.source) && event.data) {
                    if (typeof event.data.height !== "undefined") {
                        $iframe.height(ourHeight > 0 ? ourHeight : event.data.height);
                    }
                }
            });
        });
        
    })(jQuery);

</script>