<html>
<head>
	<script type="text/javascript" src="<?=Yii::app()->clientScript->getCoreScriptUrl().'/jquery.js'?>"></script>
	<style type="text/css">
		#redirect-marker {
			width: 100%;
			text-align: center;
		}
		
		.redirect-modal {
			width 		: 400px; 
			margin-left : -200px;
			font-size	: 14px;
			font-weight	: 700;
			top			: 30%; 
		} 
		
	</style>
</head>

<body>
	<h1></h1>
	<p><br /></p>
	<div id="redirect-marker"><?= $message ?></div>

	<script type="text/javascript">
		$(function(){
			// If this is rendered inside a modal dialog, find it, add some CSS (just in case) and hide it; Then: redirect
			$modal = $('#redirect-marker').parents('.modal');
			if ($modal.length > 0) {
				<?php if (!$showMessage) : ?>
					$modal.hide();
				<?php else:?>		
					$modal.children('.modal-header').remove();
					$modal.addClass('redirect-modal');
					var loader = $('<div class="ajaxloader"></div>');
					$('#redirect-marker').parents('.modal-body').append(loader);
					loader.show();
				<?php endif;?>
			}
			// Just redirect
			window.location.replace('<?= $url ?>');
		});
	</script>
	
</body>

</html>
