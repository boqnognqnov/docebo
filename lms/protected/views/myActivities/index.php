<?php
/* @var $this MyActivitiesController */
/* @var $breadcrumbs string|array */
/* @var $activeTab string */
/* @var $tabContent string */

/* @var $form CActiveForm */

$sidebarItems = array();
foreach ($menuList as $menuItem) {
	$sidebarItems[] = array(
		'title' => $menuItem['label'],
		'icon' => 'icon-'.$menuItem['id'],
		'data-tab' => $menuItem['id'],
		//'data-url' => $menuItem['url']
		'url' => $menuItem['url']
	);
}

//print breadcrumbs

switch ($from) {
	case 'reportManagement': {
		$this->breadcrumbs[Yii::t('menu', 'Admin')] = Docebo::createAppUrl('admin:dashboard/index');
		$this->breadcrumbs[Yii::t('standard', '_REPORTS')] = Docebo::createAdminUrl('reportManagement/index');
		if($userModel)
			$this->breadcrumbs[] = Yii::t('report', 'User personal summary').': '.Yii::app()->user->getRelativeUsername($userModel->userid);
	} break;
	case 'dashboard': {
		$this->breadcrumbs[Yii::t('menu', 'Admin')] = Docebo::createAppUrl('admin:dashboard/index');
		if($userModel)
			$this->breadcrumbs[] = Yii::t('report', 'User personal summary').': '.Yii::app()->user->getRelativeUsername($userModel->userid);
	} break;
	default: {
		$this->breadcrumbs[] = Yii::t('course', 'My Activities');
	} break;
}
?>

<?php DoceboUI::printFlashMessages(); ?>

<?php
if ($showAdminActions) {
	$this->renderPartial('_adminActions', array('from' => $from, 'activeTab' => $activeTab, 'userModel' => $userModel));
}
?>

<? if(!$userModel): ?>
	<?= Yii::t('register', '_ERR_INVALID_USER') ?>
<? else: ?>
<div class="myactivities-sidebar">

	<div class="sidebarred">
		<div class="sidebar">
			<!--<h3><?= Yii::t('configuration', '_ECOMMERCE') ?></h3>-->

			<? if($sidebarItems): ?>
				<ul>
					<? foreach($sidebarItems as $sidebarItem): ?>
						<li>
							<?= CHtml::link(
								( $sidebarItem['title'] ? '<span></span>' . $sidebarItem['title'] : null ),
								$sidebarItem['url'],
								array_merge(
									$sidebarItem,
									array(
										'data-loaded' => 0,
										'class'=>$sidebarItem['data-tab'] . ($sidebarItem['data-tab']==$activeTab ? ' active' : '')
									)
								)
							) ?>
						</li>
					<? endforeach; ?>
				</ul>
			<? endif; ?>
		</div>
		<div class="main">
			<div class="tab-content">
				<?= $tabContent ?>
			</div>
		</div>
	</div>
</div>

<? endif; ?>
