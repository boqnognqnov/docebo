<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/print.css" rel="stylesheet" media="print">
<!-- required css for printing -->
<?php

$urlParams = array();
if (!empty($from)) { $urlParams['from'] = $from; }
//if (!empty($activeTab)) { $urlParams['tab'] = $activeTab; }

echo CHtml::form(Docebo::createLmsUrl('myActivities/userReport', $urlParams), 'post', array('id' => 'user-activities-form', 'class' => 'user-activities-form'));

?>
<div class="user-report-actions">
	<div class="clearfix">
		<span><?= Yii::t('report', '_SELECT_USER'); ?></span>
		<?php
		echo CHtml::textField('userid', '', array(
			'id' => 'advanced-search-user-report',
			'class' => 'typeahead ajaxGenerate user-search',
			'autocomplete' => 'off',
			'data-url' => Docebo::createAppUrl('admin:userManagement/usersAutocomplete'),
			'data-type' => 'core',
			'data-loader-show' => 'true',
			'placeholder' => Yii::t('report', 'Type user here'),
			'data-source-desc' => 'true',
		));
		echo CHtml::submitButton(Yii::t('certificate', '_GENERATE'), array('class' => 'new-user-summary', 'id' => 'generate-user-report'));
		echo CHtml::textField('selectedUserId', $userModel->idst, array(
			'id' => 'selected-user-id',
			'class' => 'hidden',

		));
		?>

		<script>
						$(function() {
							$('#generate-user-report').on('click', function(e) {
								function verifyInitData()
								{
									var button = $('#generate-user-report');
									var input = button.parent().find('input');
									var data = input.val();
									if (data == '') {
										input.css('border-color', '#ff0000');
										return false;
									}
									input.css('border-color', '#E4E6E5');
									return true;
								}

								if (!verifyInitData())
								{
									e.preventDefault();
								}
							});
						});
					</script>

		<div class="report-popup-actions">
			<?php

			echo CHtml::link(
				Yii::t('report', 'Print') . '<span></span>',
				'javascript:void(0);',
				array('class' => 'report-action-print')
			);

			echo CHtml::link(
				Yii::t('report', 'Download as PDF') . '<span></span>',
				Docebo::createAbsoluteUrl('admin:reportManagement/createUserReport', array('id' => Yii::app()->user->getRelativeUsername($userModel->userid), 'createPdf' => true)),
				array('class' => 'report-action-pdf')
			);

			?>
		</div>
	</div>
</div>
<?php
echo CHtml::closeTag('form');
?>
<script type="text/javascript">
	$(function() {

		//autocomplete for the user reporting search input text field
		applyTypeahead($('.typeahead'));

		//add action to printing button
		$('.report-action-print').click(function(){
			window.print();
		});

		//add action to pdf button
		$('.report-action-pdf').on('click', function (e) {

			e.preventDefault(); //make sure not to load a new page
			if ($(this).hasClass('disabled')) return false;
			// Prevent using standart LMS generation of PDF
			if ($(this).hasClass('disabled-pdf')) return;

			//TODO: in the future, donuts by "scriptTur.js" will be deprecated and removed. Remember to change this part too.
			var donutChartImageId = $('.donut-wrapper').find('.donutContainer').prop('id');
			$('.report-donut-img').css('display', 'none');
			var donutChart = $('#' + donutChartImageId).parent().html();
			$('.report-donut-img').css('display', 'block');

			var img = jqplotToImg($('#sessions_chart'));
			if(img)
				var lineChart = '<img src="' + img + '" />';
			else
				var lineChart = '';

			$.post($(this).prop('href'), {'donutContent': donutChart, 'lineContent': lineChart}, function (data) {
				if (data.fileName) {
					window.location = '<?= Docebo::createAbsoluteUrl('admin:reportManagement/downloadPdf') ?>&fileName=' + data.fileName;
				} else if(data.error) {
					Docebo.Feedback.show('error', data.error);
				}
			}, 'json');

			return false;
		});
	});

	function jqplotToImg(obj) {
		var newCanvas = document.createElement("canvas");
		if(!newCanvas.getContext)
			G_vmlCanvasManager.initElement(newCanvas);
		newCanvas.width = obj.find("canvas.jqplot-base-canvas").width();
		newCanvas.height = obj.find("canvas.jqplot-base-canvas").height()+10;
		var baseOffset = obj.find("canvas.jqplot-base-canvas").offset();
		if(newCanvas.toDataURL)
		{
			// make white background for pasting
			var context = newCanvas.getContext("2d");
			context.fillStyle = "rgba(255,255,255,1)";
			context.fillRect(0, 0, newCanvas.width, newCanvas.height);

			obj.children().each(function () {
				// for the div's with the X and Y axis
				if ($(this)[0].tagName.toLowerCase() == 'div') {
					// X axis is built with canvas
					$(this).children("canvas").each(function() {
						var offset = $(this).offset();
						newCanvas.getContext("2d").drawImage(this,
							offset.left - baseOffset.left,
							offset.top - baseOffset.top
						);
					});
					// Y axis got div inside, so we get the text and draw it on the canvas
					$(this).children("div").each(function() {
						var offset = $(this).offset();
						var context = newCanvas.getContext("2d");
						context.font = $(this).css('font-style') + " " + $(this).css('font-size') + " " + $(this).css('font-family');
						context.fillStyle = $(this).css('color');
						context.fillText($(this).text(),
							offset.left - baseOffset.left,
							offset.top - baseOffset.top + $(this).height()
						);
					});
				} else if($(this)[0].tagName.toLowerCase() == 'canvas') {
					// all other canvas from the chart
					var offset = $(this).offset();
					newCanvas.getContext("2d").drawImage(this,
						offset.left - baseOffset.left,
						offset.top - baseOffset.top
					);
				}
			});

			// add the point labels
			obj.children(".jqplot-point-label").each(function() {
				var offset = $(this).offset();
				var context = newCanvas.getContext("2d");
				context.font = $(this).css('font-style') + " " + $(this).css('font-size') + " " + $(this).css('font-family');
				context.fillStyle = $(this).css('color');
				context.fillText($(this).text(),
					offset.left - baseOffset.left,
					offset.top - baseOffset.top + $(this).height()*3/4
				);
			});

			// add the title
			obj.children("div.jqplot-title").each(function() {
				var offset = $(this).offset();
				var context = newCanvas.getContext("2d");
				context.font = $(this).css('font-style') + " " + $(this).css('font-size') + " " + $(this).css('font-family');
				context.textAlign = $(this).css('text-align');
				context.fillStyle = $(this).css('color');
				context.fillText($(this).text(),
					newCanvas.width / 2,
					offset.top - baseOffset.top + $(this).height()
				);
			});

			// add the legend
			obj.children("table.jqplot-table-legend").each(function() {
				var offset = $(this).offset();
				var context = newCanvas.getContext("2d");
				context.strokeStyle = $(this).css('border-top-color');
				context.strokeRect(
					offset.left - baseOffset.left,
					offset.top - baseOffset.top,
					$(this).width(),$(this).height()
				);
				context.fillStyle = $(this).css('background-color');
				context.fillRect(
					offset.left - baseOffset.left,
					offset.top - baseOffset.top,
					$(this).width(),$(this).height()
				);
			});

			// add the rectangles
			obj.find("div.jqplot-table-legend-swatch").each(function() {
				var offset = $(this).offset();
				var context = newCanvas.getContext("2d");
				context.fillStyle = $(this).css('background-color');
				context.fillRect(
					offset.left - baseOffset.left,
					offset.top - baseOffset.top,
					$(this).parent().width(),$(this).parent().height()
				);
			});

			obj.find("td.jqplot-table-legend").each(function() {
				var offset = $(this).offset();
				var context = newCanvas.getContext("2d");
				context.font = $(this).css('font-style') + " " + $(this).css('font-size') + " " + $(this).css('font-family');
				context.fillStyle = $(this).css('color');
				context.textAlign = $(this).css('text-align');
				context.textBaseline = $(this).css('vertical-align');
				context.fillText($(this).text(),
					offset.left - baseOffset.left,
					offset.top - baseOffset.top + $(this).height()/2 + parseInt($(this).css('padding-top').replace('px',''))
				);
			});

			// convert the image to base64 format
			return newCanvas.toDataURL("image/png");
		}
		else
			return false;
	}
</script>