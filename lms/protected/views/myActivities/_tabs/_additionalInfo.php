<?php /** @var CoreUserField $field */ ?>
<div class="printable new-user-summary">

	<div class="user-additional-fields my-activities clearfix">
		<?php if (!empty($additionalFields)) {
			$i = 0;
			foreach ($additionalFields as $field) {
				$fieldClass = ($i % 2 == 0)? 'odd' : 'even';
				$encTranslation = CHtml::encode($field->getTranslation());
				$value = $field->renderValue($field->userEntry);
				echo '<div class="field '.$fieldClass.' '. (($i <= 1) ? 'firstField' : '' ) .'"><div class="name">'.$encTranslation.'</div><div class="">'.$value.'</div></div>';
				$i++;
			}
		}	?>

	</div>

</div>