<?php
//	echo '<pre>';
//		print_r($renderData);
//	echo '</pre>';
?>
<div id="userReportQuestionsAndAnswers" class="row-fluid" data-user-id="<?php echo $renderData['searchedUserId'] ?>">
	<div class="span12">
		<h3 class="border"><?= Yii::t('app7020', 'Questions & Answers'); ?></h3>
		<div class="timeframe-selector">
			<label for="timeframe-selector"><?= Yii::t('app7020', 'Timeframe'); ?></label>
			<?php echo CHtml::dropDownList('timeframeSelect', $renderData['preselectedTimeframe'], $renderData['timeframes']); ?>
		</div>
		<div class="clear"></div>
		<h4><?php echo Yii::t('app7020', 'Overview'); ?></h4>
		<div class="row-fluid">
			<div class="span4 data-box">
				<span id="answeredQuestions" class="num"><?php echo $renderData['answeredQuestions']; ?></span>
				<span class="text"><?= Yii::t('app7020', 'Answered questions'); ?></span>
			</div>
			<div class="span4 data-box">
				<span id="questionsMade" class="num"><?php echo $renderData['questionsMade']; ?></span>
				<span class="text"><?= Yii::t('app7020', 'Question made'); ?></span>
			</div>
		</div>
		<h4><?php echo Yii::t('app7020', 'Quality'); ?></h4>
		<div class="row-fluid">
			<div class="span4 data-box">
				<span id="answersLikes" class="num"><?php echo $renderData['likes']; ?></span>
				<span class="text"><?= Yii::t('app7020', "Answers' likes"); ?></span>
			</div>
			<div class="span4 data-box">
				<span id="answersDislekes" class="num"><?php echo $renderData['dislikes']; ?></span>
				<span class="text"><?= Yii::t('app7020', "Answers' dislikes"); ?></span>
			</div>
			<div class="span4 data-box">
				<span id="bestAnswers" class="num"><?php echo $renderData['bestAnswers']; ?></span>
				<span class="text"><?= Yii::t('app7020', 'Answers marked as "Best Answer"'); ?></span>
			</div>
		</div>


		<div class="clear"></div>
		<h3><?= Yii::t('app7020', 'Activity per channel'); ?></h3>

		<div class="row-fluid">
			<div class="span12 activity-per-channel-container">
				<?php
				$this->widget('common.widgets.app7020ActivityChannels', array('data' => $renderData['chartMyActivityPerChannel']));
				?>
			</div>
		</div>

		<h3 class="border"><?= Yii::t('app7020', 'Questions vs Answers'); ?></h3>
		<div class="barchart-legend">
			<span class="green"></span>
			<?= Yii::t('app7020', 'questions') ?>
			<span class="blue"></span>
			<?= Yii::t('app7020', 'answers') ?>
		</div>
		<canvas id="app7020ChartQuestionsAndAnswers" width="820" height="200"></canvas>

		<!-- Rank by answers quality -->
		<ul class="table-top5 docebo-col-xs-12">
			<li class="table-row row-header row-fluid first-row docebo-col-xs-12">
				<span class="cell cell-name docebo-col-xs-12 docebo-col-md-6">
					<h3><?php echo Yii::t('app7020', "Rank by answers quality"); ?></h3>
					<span class="text subtitle">
						Social score based on the quality of the given answers
					</span> 
				</span>
				<span class="cell cell-best-answer cell-margin docebo-col-xs-2 docebo-col-xs-offset-6">
					<i class="fa fa-check-circle"></i> 
					<div class="text subtitle">
						<?php echo Yii::t('app7020', 'Best answers') ?>
					</div>
				</span>
				<span class="cell cell-like cell-margin docebo-col-xs-2">
					<i class="fa fa-thumbs-o-up"></i>
					<div class="text subtitle">
						<?php echo Yii::t('app7020', 'Likes') ?>
					</div>
				</span>
				<span class="cell cell-dislike cell-margin docebo-col-xs-2">
					<i class="fa fa-thumbs-o-down"></i>
					<div class="text subtitle">
						<?php echo Yii::t('app7020', 'Dislikes') ?>
					</div>
				</span>
			</li>
			<?php $counterRankExpertsByAnswersQuality = 0; ?>
			<?php foreach ($renderData['rankExpertsByAnswersQuality'] as $key=>$rankExpertsByAnswersQuality) : 
				if($rankExpertsByAnswersQuality["idst"]!="") {
				$counterRankExpertsByAnswersQuality++;
				$current = $renderData['searchedUserId'] == $rankExpertsByAnswersQuality['idst'] ? 'current' : '';
				?>
				<li class="table-row row-fluid docebo-col-xs-12 <?php echo $current; ?>">
					<span class="cell cell-index docebo-col-xs-1"><?php echo $key+1; ?></span>
					<span class="cell cell-name docebo-col-xs-5">
						<span class="avatar-container">
							<?= $rankExpertsByAnswersQuality['avatar']; ?>
						</span> 
						<?php echo $rankExpertsByAnswersQuality['expert_name'] == true ? $rankExpertsByAnswersQuality['expert_name'] : $rankExpertsByAnswersQuality['username']; ?> 
					</span>
					<span class="cell cell-best-answer docebo-col-xs-2"><?php echo $rankExpertsByAnswersQuality['count_bestAnswers'] ?></span>
					<span class="cell cell-like docebo-col-xs-2"><?php echo $rankExpertsByAnswersQuality['count_likes'] ?></span>
					<span class="cell cell-dislike docebo-col-xs-1"><?php echo $rankExpertsByAnswersQuality['count_dislikes'] ?></span>
				</li>
				<?php } endforeach; ?>
		</ul>

		<!-- Rank by first to answer rate -->
		<ul class="table-top5">
			<li class="table-row row-header row-fluid first-row docebo-col-xs-12">
				<span class="cell cell-name docebo-col-xs-12 docebo-col-md-6">
					<span class="text subtitle">
						<h3><?php echo Yii::t('app7020', "Rank by first to answer rate"); ?></h3>
						<?php echo Yii::t('app7020', 'How many questions expert answered first') ?>
					</span>
				</span>
				<div class="cell cell-question-count span2"><span class="subtitle-alone"></span></div>
			</li>
			<?php
			$counterRankExpertsByFirstToAnswer = 0;
			?>
			<?php foreach ($renderData['rankExpertsByFirstToAnswer'] as $key=>$rankExpertsByFirstToAnswer) : 
				if($rankExpertsByFirstToAnswer["idst"]!="") {
				$counterRankExpertsByFirstToAnswer++;
				$current = $renderData['searchedUserId'] == $rankExpertsByFirstToAnswer['idst'] ? 'current' : '';
				?>
				<li class="table-row row-fluid docebo-col-xs-12 <?php echo $current; ?>">
					<span class="cell cell-index  docebo-col-xs-1 "><?php echo $key+1; ?></span>
					<span class="cell cell-name docebo-col-xs-8 ">
						<span class="avatar-container">
						<?= $rankExpertsByFirstToAnswer['avatar']?>
						</span>
						<?php echo $rankExpertsByFirstToAnswer['expert_name'] == true ? $rankExpertsByFirstToAnswer['expert_name'] : $rankExpertsByFirstToAnswer['username']; ?> 
					</span>
					<span class="cell cell-question-count docebo-col-xs-2"><?php echo $rankExpertsByFirstToAnswer['count_answers']; ?></span>
				</li>
				<?php } endforeach; ?>
		</ul>
		
		<!-- Rank by partecipation rate -->
		<ul class="table-top5">
			<li class="table-row row-header row-fluid first-row docebo-col-xs-12">
				<span class="cell cell-name docebo-col-xs-10">
					<span class="text subtitle">
						<h3><?php echo Yii::t('app7020', "Rank by participation rate"); ?></h3>
						<?php echo Yii::t('app7020', 'How many questions expert answered (in total)') ?>
					</span>
				</span>
				<div class="cell cell-question-count docebo-col-xs-1 "><span class="subtitle-alone"></span></div>
			</li>
			<?php
			$counterRankExpertsByPartecipationRate = 0;
			?>
			<?php foreach ($renderData['rankExpertsByPartecipationRate'] as $key=>$rankExpertsByPartecipationRate) : 
				if($rankExpertsByPartecipationRate["idst"]!="") {
				$counterRankExpertsByPartecipationRate++;
				$current = $renderData['searchedUserId'] == $rankExpertsByPartecipationRate['idst'] ? 'current' : '';
				?>
				<li class="table-row row-fluid docebo-col-xs-12  <?php echo $current; ?>">
					<span class="cell cell-index docebo-col-xs-1"><?php echo $key+1 ?></span>
					<span class="cell cell-name docebo-col-xs-9">
						<span class="avatar-container">
							<?= $rankExpertsByPartecipationRate['avatar']; ?>
						</span>
						<?php echo $rankExpertsByPartecipationRate['expert_name'] == true ? $rankExpertsByPartecipationRate['expert_name'] : $rankExpertsByPartecipationRate['username']; ?> 
					</span>
					<span class="cell cell-question-count docebo-col-xs-2"><?php echo $rankExpertsByPartecipationRate['count_answers']; ?></span>
				</li>
				<?php } endforeach; ?>
		</ul>


	</div>
</div>

