<div>

	<div class="printable user-report-courses" style="margin-bottom: 20px;">
		<div class="courses-title"><?php echo Yii::t('myactivities', 'Learning plans'); ?></div>
		<div class="courses-grid-wrapper">
			<?php

			$columns = array();
			$columns[] = array(
				'name' => 'path_code',
				'header' => Yii::t('standard', '_CODE'),
				'value' => '$data->path->path_code;'
			);
			$columns[] = array(
						'name' => 'path_name',
						'header' => Yii::t('standard', '_NAME'),
						'value' => '$data->path->path_name;',
					);
			$columns[] = array(
						'name' => 'courses_number',
						'header' => Yii::t('myactivities', 'Courses number'),
						'value' => '$data->path->getCoursesNumber();',
						'htmlOptions' => array('class' => 'text-center'),
						'headerHtmlOptions' => array('class' => 'text-center')
					);
			$columns[] = array(
						'name' => 'progress',
						'header' => Yii::t('standard', '_PROGRESS'),
						'type' => 'raw',
						'htmlOptions' => array('class' => 'less-border'),
						'value' => function($data, $index) {
							$info = $data->path->getCoursepathPercentage($data->idUser);
							$additional = '';
							if ($info['percentage'] <= 0) { $additional = ' minimum'; }
							if ($info['percentage'] > 0 && $info['percentage'] < 100) { $additional = ' partial'; }
							if ($info['percentage'] >= 100) { $additional = ' maximum'; }
							$html = '<div class="learningplan-percentage'.$additional.'">'.$info['percentage'].'%</div>';
							return $html;
						}
					);

			if($this->doesUserHavePermission('/lms/admin/certificate/view')){
				$columns[] = array(
							'type' => 'raw',
							'htmlOptions' => array('class' => 'text-left action-icon-column'),
							'value' => function($data, $index) use (&$userModel) {
								$certificateTemplate = $data->path->certificate; /* @var $certificateTemplate LearningCertificateCoursepath */
								if ($certificateTemplate && $certificateTemplate->id_certificate != 0)
									$generatedCertificate = LearningCertificateAssignCp::loadByUserPath($userModel->getPrimaryKey(), $certificateTemplate->id_path, $certificateTemplate->id_certificate);
								else
									$generatedCertificate = null;
								$html = '';
								if ($generatedCertificate) {
									$html = CHtml::link(
										'<span class="i-sprite is-cert"></span>',
										Docebo::createLmsUrl('curricula/show/downloadCertificate', array('id_path' => $data->id_path, 'id_user' => $userModel->getPrimaryKey())),
										array(
											'id' => 'dl-cert-'.$data->id_path,
											'title' => Yii::t('certificate', '_TAKE_A_COPY'),
											'target' => '_blank'
										)
									);
								}
								return $html;
							}
						);
			}

			$gridConfig = array(
				'id' => 'coursepath-management-grid',
				'htmlOptions' => array('class' => 'grid-view clearfix'),
				'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
				'dataProvider' => $coursepathUserModel->dataProviderMyActivities($userModel->getPrimaryKey()),
				'cssFile' => false,
				'template' => '{items}{summary}{pager}',
				'summaryText' => Yii::t('standard', '_TOTAL'),
				'pager' => array(
					'class' => 'adminExt.local.pagers.DoceboLinkPager',
				),
				'ajaxUpdate' => 'all-items',
				'beforeAjaxUpdate' => 'function(id, options) {
                    options.type = "POST";
                    if (options.data == undefined) {
                        options.data = {
                            contentType: "html",
                        }
                    }
                }',
				'columns' => $columns,
			);

			// Event raised for YnY app to hook on to
			$event = new DEvent($this, array('gridConfig' => &$gridConfig));
			if(Yii::app()->event->raise('BeforeRenderCoursepathGrid', $event)){
				if($this->doesUserHavePermission('/lms/admin/coursepath/view')|| $this->doesUserHavePermission('/lms/admin/report/view')){
					$this->widget('zii.widgets.grid.CGridView', $gridConfig);
				} else {
					echo Yii::t('standard', 'Do not have the needed permissions');
				}
			}
			?>
		</div>
	</div>

</div>