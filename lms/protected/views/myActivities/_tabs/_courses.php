<style>
	a{
		color: #333333;
	}
	a:hover{
		color: #333333;
	}
</style>
<div>

	<div class="printable user-report-courses" style="margin-bottom: 20px;">
		<div class="courses-title"><?php echo Yii::t('standard', '_COURSES'); ?></div>
		<div class="courses-grid-wrapper">



			<?php
			if($this->doesUserHavePermission('/lms/admin/course/view') || $this->doesUserHavePermission('/lms/admin/report/view'))
			{


				$columns = array();
				$courseNameValue = '$data->renderCourseLink($data->course->name)';
				if(!$this->doesUserHavePermission('/lms/admin/course/view')){
					$courseNameValue = '$data->course->name';
				}

				$columns[] = array(
					'name' => 'course.code',
					'header' => Yii::t('standard', '_COURSE_CODE'),
				);
				$columns[] = array(
					'name' => 'course.name',
					'value' => $courseNameValue,
					'type' => 'raw'
				);
				$columns[] = array(
					'header' => Yii::t('profile', '_USERCOURSE_STATUS'),
					'name' => 'status',
					'type' => 'raw',
					'value' => function ($data, $index)
					{
						$additional = '';
						switch($data->status)
						{
							case LearningCourseuser::$COURSE_USER_SUBSCRIBED:
								$additional = ' to-begin';
								break;
							case LearningCourseuser::$COURSE_USER_BEGIN:
								$additional = ' in-progress';
								break;
							case LearningCourseuser::$COURSE_USER_END:
								$additional = ' completed';
								break;
						}
						$html = '<div class="user-status' . $additional . '">' . LearningCourseuser::getStatusLabel($data->status) . '</div>';
						return $html;
					}
				);
				$columns[] = array(
					'header' => Yii::t('standard', 'Enrolled'),
					'name' => 'date_inscr',
					'value' => function ($data, $index)
					{
						$html = '';
						if(!empty($data->date_inscr))
						{
							$html = Yii::app()->localtime->stripTimeFromLocalDateTime($data->date_inscr);
						}
						return $html;
					}
				);
				$columns[] = array(
					'header' => Yii::t('standard', 'Course completion'),
					'name' => 'date_complete',
					'value' => function ($data, $index)
					{
						$html = '';
						if(!empty($data->date_complete))
						{
							$html = Yii::app()->localtime->stripTimeFromLocalDateTime($data->date_complete);
						}
						return $html;
					}
				);
				$columns[] = array(
					'header' => Yii::t('standard', '_CREDITS'),
					'name' => 'credits',
					'htmlOptions' => array('class' => 'text-center'),
					'headerHtmlOptions' => array('class' => 'text-center'),
					'value' => function ($data, $index)
					{
						$html = "";
						if($data->course->credits > 0)
						{
							$html = number_format($data->course->credits, 2, '.', '');
						}
						return $html;
					}
				);
				$columns[] = array(
					'header' => Yii::t('standard', '_TOTAL_TIME'),
					'value' => '$data->renderTimeInCourse();',
				);
				$columns[] = array(
					'name' => 'course.score',
					'header' => Yii::t('standard', '_SCORE'),
					'value' => function ($data, $index)
					{
						$html = '';
						$score = LearningCourseuser::getLastScoreByUserAndCourse($data["idCourse"], $data["idUser"]);
						$score = trim($score);
						if(!empty($score) && $score != '-')
						{
							$html = $score;
						}
						return $html;
					},
					'htmlOptions' => array('class' => 'text-center'),
					'headerHtmlOptions' => array('class' => 'text-center')
				);

				if($this->doesUserHavePermission('/lms/admin/certificate/view'))
				{
					$columns[] = array(
						'type' => 'raw',
						'value' => function ($data, $index) use (&$userModel)
						{
							$html = '';
							$userCertificate = LearningCertificateAssign::loadByUserCourse($userModel->getPrimaryKey(), $data["idCourse"]);
							if($userCertificate)
							{
								$html = CHtml::link(
									'<span class="i-sprite is-cert"></span>',
									//Docebo::createLmsUrl('/player/training/downloadCertificate', array('course_id' => $data["idCourse"])),
									Docebo::createLmsUrl('myActivities/downloadCertificate', array('course_id' => $data["idCourse"], 'id_user' => $userModel->getPrimaryKey())),
									array(
										'id' => 'dl-cert-' . $data->idCourse,
										'title' => Yii::t('certificate', '_TAKE_A_COPY'),
										'target' => '_blank'
									)
								);
							}
							return $html;
						}
					);
				}

				$this->widget('DoceboCGridView', array(
					'id' => 'course-management-grid',
					'htmlOptions' => array('class' => 'grid-view clearfix'),
					'dataProvider' => $courseUserModel->dataProvider(),
					'cssFile' => false,
					'template' => '{items}{summary}{pager}',
					'summaryText' => Yii::t('standard', '_TOTAL'),
					'pager' => array(
						'class' => 'adminExt.local.pagers.DoceboLinkPager',
					),
					'ajaxUpdate' => 'all-items',
					'beforeAjaxUpdate' => 'function(id, options) {
                    options.type = "POST";
                    if (options.data == undefined) {
                        options.data = {
                            contentType: "html",
                        }
                    }
                    
                    if($("#selected-user-id").val()){
                        options.data.selectedUserId = $("#selected-user-id").val();
                    }
                }',
					'columns' => $columns
				));

			} else {
				echo Yii::t('standard', 'Do not have the needed permissions');
			}

			?>
		</div>
	</div>

</div>