<?php
//TODO: maybe not all of these js plugins are needed
$this->widget('common.extensions.jqplot.JqplotGraphWidget', array(
	'pluginScriptFile'=>array(
		'jqplot.dateAxisRenderer.js',
		'jqplot.barRenderer.js',
		'jqplot.categoryAxisRenderer.js',
		'jqplot.highlighter.js',
		'jqplot.canvasTextRenderer.js',
		'jqplot.canvasAxisLabelRenderer.js',
	)
));
?>
<div class="printable new-user-summary">

	<div class="user-report-summary">
		<div class="row-fluid inline-block">
			<div class="user-report-info span6">
				<div class="user-report-info-image">
					<?php echo $userModel->getAvatarImage(false, '', 'width: 150px;'); ?>
				</div>
				<div class="user-report-info-data">
					<div class="fullname"><?php echo $userModel->getFullName(); ?></div>
					<div class="username"><?php echo Yii::app()->user->getRelativeUsername($userModel->userid); ?></div>
					<div>
						<div class="label"><?php echo Yii::t('standard', '_LEVEL'); ?></div>
						<div
							class="data"><?php echo(!empty($userModel->adminLevel->groupid) ? Yii::t('admin_directory', '_DIRECTORY_' . $userModel->adminLevel->groupid) : ''); ?></div>
					</div>
					<div>
						<div class="label"><?php echo Yii::t('standard', '_EMAIL'); ?></div>
						<div title="<?php echo $userModel->email; ?>" class="data"><?php echo Docebo::ellipsis($userModel->email, 26); ?></div>
					</div>
					<div>
						<div class="label"><?php echo Yii::t('standard', '_GROUPS'); ?></div>
						<div class="data">
							<?php echo implode(', ', $userGroups); ?>
						</div>
					</div>
				</div>
			</div>

			<div class="user-report-stat text-center span6">
				<div class="subscription-date">
					<?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/report/subscription-date.png'); ?>
					<div class="data"><?php echo $userModel->register_date; ?></div>
					<div class="label"><?php echo Yii::t('report', '_DATE_INSCR'); ?></div>
				</div>
				<div class="last-access">
					<?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/report/last-access-date.png'); ?>
					<div
						class="data"><?php echo(!empty($userModel->lastenter) ? $userModel->lastenter : Yii::t('standard', '_NEVER')); ?></div>
					<div class="label"><?php echo Yii::t('standard', '_DATE_LAST_ACCESS'); ?></div>
				</div>
				<div class="total-time">
					<?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/report/total-time.png'); ?>
					<div class="data"><?php echo $totalTime; ?></div>
					<div class="label"><?php echo Yii::t('standard', '_TOTAL_TIME'); ?></div>
				</div>
				<div class="active-courses">
					<?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/report/active-courses.png'); ?>
					<?php $event = new DEvent($this, array('user' => $userModel));
						Yii::app()->event->raise('OnMyActivitiesActiveCoursesCount', $event);
						if(!$event->shouldPerformAsDefault())
							$count = $event->return_value['active_courses'];
						else
							$count = count($userModel->learningCourses);
					?>
					<div class="data"><?php echo $count; ?></div>
					<div class="label"><?php echo Yii::t('dashboard', '_ACTIVE_COURSE'); ?></div>
				</div>
			</div>
		</div>
	</div>



	<?php if (isset($coursesDueDate) && is_array($coursesDueDate) && !empty($coursesDueDate)): ?>
	<div class="report-courses-summary">

		<div class="my-activities-stat-title row-fluid">
			<div class="span12"><?php echo Yii::t('myactivities', 'My next {n} courses due date', array('{n}' => count($coursesDueDate))); ?></div>
		</div>
		<div class="myactivities-due-date-content row-fluid">
			<?php foreach ($coursesDueDate as $course): ?>
			<div class="course-due-container span4">

				<?php
				//computes due time
				$timeEnd = strtotime($course['date_end'].' 23:59:59');
				$timeNow = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'));
				$timeDiff = $timeEnd - $timeNow; //number of seconds between course end and now
				$daysText = '';
				$clockColor = 'yellow';
				if ($timeDiff >= 0 && $timeDiff < 86400) {
					$daysText = Yii::t('myactivities', 'Tomorrow').'!';
					$clockColor = 'red';
				} elseif ($timeDiff >= 86400 && $timeDiff < 3*86400) {
					$daysText = Yii::t('myactivities', 'In 3 days');
					$clockColor = 'orange';
				} elseif ($timeDiff > 3*86400) {
					$daysText = Yii::app()->localtime->toLocalDate($course['date_end']);
					//keep yellow as default color
				}

				echo '<span class="course-due-icon myactivities-sprite clock '.$clockColor.'"></span>';

				echo '<div class="course-due-date">';
				echo '<p class="course-name">'.CHtml::link($course['name'], Docebo::createLmsUrl('player', array('course_id' => $course['idCourse']))).'</p>';
				//echo '<br />';
				echo '<p class="days-alert">'.$daysText.'</p>';
				echo '</div>';
				?>

			</div>
			<?php endforeach; ?>
		</div>

	</div>
	<?php endif; ?>



	<div class="report-courses-summary">

		<div class="row-fluid">
			<div class="report-courses-summary-legend span5">
				<div class="my-activities-stat-title row-fluid">
					<div class="span12"><?php echo Yii::t('standard', '_PROGRESS'); ?></div>
				</div>
				<div class="row-fluid">
					<div class="donut-wrapper span7">
						<?php echo $pieChart; ?>
					</div>

					<div class="summary-legend-wrapper span5">
						<?php echo $pieChartLegend; ?>
					</div>
				</div>
			</div>

			<div class="report-activities-summary span7">
				<div class="my-activities-stat-title row-fluid">
					<div class="span12"><?php echo Yii::t('report', '_YEARLY_ACTIVITIES_SUMMARY'); ?></div>
				</div>
				<div class="lineChart-wrapper">
					<div id="sessions_chart" style="height:<?= $sessionsChart['height'] ?>;"></div>
				</div>
			</div>
		</div>

	</div>
	<?php if(PluginManager::isPluginActive('Share7020App')): ?>
	<div class="coach-share-overview">
		<div class="my-activities-stat-title row-fluid">
			<div class="span12">
				<?php echo Yii::t('standard', 'Coach & Share Overview'); ?>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span5 question-answer-chart">
				<ul class="legend">
					<li>
						<span class="color" style="background-color: #AEDEAE;"></span>
						<span><?php echo Yii::t('standard', 'questions made'); ?></span>
					</li>
					<li>
						<span class="color" style="background-color: #E5EFF8;"></span>
						<span><?php echo Yii::t('standard', 'answers given'); ?></span>
					</li>
				</ul>
				<div id="userReportQuestionsAndAnswers">
					<canvas id="app7020ChartQnA" height="212"></canvas>
				</div>
			</div>
			<div class="span7 asset-chart">
				<ul class="legend">
					<li>
						<span class="color" style="background-color: #0563A9;"></span>
						<span><?php echo Yii::t('standard', 'asset views'); ?></span>
					</li>
					<li>
						<span class="color" style="background-color: #52A1DC;"></span>
						<span><?php echo Yii::t('standard', 'shared assets'); ?></span>
					</li>
				</ul>
				<div id="assets-views-chart-container" style="height: 214px;"></div>
			</div>
		</div>
		<?php if(count($coachShareMostWatched) > 0): ?>
			<div class="most-watched-assets-weekly">
				<div class="row-fluid header-title">
					<div class="span12">
						<h4><?php echo Yii::t('standard', 'Top {count} most watched assets (weekly)', array('{count}' => 3)); ?></h4>
					</div>
				</div>
				<div class="watched-assets-wrapper row-fluid">
					<?php foreach($coachShareMostWatched as $asset): ?>
						<div class="asset-item span4">
							<div class="image-wrapper">
								<img src="<?php echo $asset['thumbnail']; ?>" alt="">
							</div>
							<p class="title" title="<?php echo $asset['title']; ?>"><a href="<?php echo Docebo::createApp7020AssetsViewUrl($asset['id_asset'])?>"><?php echo $asset['title']; ?></a></p>
							<p class="counter"><span class="count"><?php echo $asset['current_views']; ?></span><span class="suffix"><?php echo Yii::t('standard', 'views');?></span></p>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endif ;?>
	</div>
	<?php endif ;?>

	<?php if (isset($coursesTopTotalTime) && is_array($coursesTopTotalTime) && !empty($coursesTopTotalTime)): ?>
		<div class="report-courses-summary">

			<div class="my-activities-stat-title row-fluid">
				<div class="span12"><?php echo Yii::t('myactivities', 'My top {n} most viewed courses (total time)', array('{n}' => count($coursesTopTotalTime))); ?></div>
			</div>
			<div class="myactivities-top-total-time-content row-fluid">
				<?php foreach ($coursesTopTotalTime as $course): ?>
					<div class="course-due-container span4">

						<?php
						//detect and render status text
						$listStatuses = LearningCourseuser::getStatusesArray();
						$statusSpan = false;
						if (isset($course['status']) && isset($listStatuses[$course['status']])) {
							$statusText = $listStatuses[$course['status']];
							$statusStyle = false;
							switch ($course['status']) {
								case LearningCourseuser::$COURSE_USER_BEGIN: $statusStyle = 'in-progress'; break;
								case LearningCourseuser::$COURSE_USER_END: $statusStyle = 'completed'; break;
							}
							$statusSpan = '<span class="course-status'.(!empty($statusStyle) ? ' '.$statusStyle : '').'">'.$statusText.'</span>';
							$name = (strlen($course['name']) > 50) ? substr($course['name'],0,50).'...' : $course['name'];
						}

						echo '<span class="course-due-icon myactivities-sprite blue-glass"></span>';

						echo '<div class="course-due-date">';

						if(!isset($course['idCourse']) || (!$this->doesUserHavePermission('/lms/admin/course/view') || !CoreUserPuCourse::isCourseAssignedToPU(Yii::app()->user->id,  $course['idCourse']))){
							echo '<p class="course-name">'.$name.'</p>';
						}else{
							echo '<p class="course-name">'.CHtml::link($name, Docebo::createLmsUrl('player', array('course_id' => $course['idCourse'])));
						}

						echo '<p class="course-time">'.$this->convertCourseTime($course['time']).(!empty($statusSpan) ? '&nbsp;&nbsp;&nbsp;'.$statusSpan : '').'</span>';
						echo '</div>';
						?>

					</div>
				<?php endforeach; ?>
			</div>

		</div>
	<?php endif; ?>


	<div class="report-courses-summary">

		<div class="my-activities-stat-title row-fluid">
			<div class="span12"><?php echo Yii::t('myactivities', 'My performances'); ?></div>
		</div>

		<div class="myactivities-myperformances row-fluid">

			<div class="myperformaces-chart span7">
				<div id="myperformances_year_chart" style="height:230px;"></div>
			</div>

			<div class="span5 my-activities-test-statistics-container">
				<div class="my-activities-test-statistics-title row">
					<div class="span12"><?php echo Yii::t('myactivities', 'Statistics (in points)'); ?></div>
				</div>
				<div class="text-center row">

					<div class="span4">
						<div class="myperformances-icon-container">
							<span class="myactivities-sprite cup yellow-up"></span>
						</div>
						<div class="myperformances-score-container">
							<?php
							$maxScore = $performanceInfo['highest_score']['score_max'];
							$style = '';

							if(strlen($maxScore) > 5 && strlen($maxScore) < 7){
								$style = 'style="font-size: 18px;"';
							}elseif(strlen($maxScore) > 6 && strlen($maxScore) < 8){
								$style = 'style="font-size: 15px;"';
							}
							?>
							<span <?=$style;?> >
							<?php
							if (!empty($performanceInfo['highest_score'])) {
								echo $performanceInfo['highest_score']['score'].'/'.$performanceInfo['highest_score']['score_max'];
							} else {
								echo '-/-';
							}
							?>
							</span>
						</div>
						<div class="myperformances-text-container">
							<?php
							echo Yii::t('myactivities', 'Highest score at a test');
							?>
						</div>
					</div>

					<div class="span4">
						<div class="myperformances-icon-container">
							<span class="myactivities-sprite cup black-down"></span>
						</div>
						<div class="myperformances-score-container">
							<span <?=$style;?> >
							<?php
							if (!empty($performanceInfo['lowest_score'])) {
								echo $performanceInfo['lowest_score']['score'].'/'.$performanceInfo['lowest_score']['score_max'];
							} else {
								echo '-/-';
							}
							?>
							</span>
						</div>
						<div class="myperformances-text-container">
							<?php
							echo Yii::t('myactivities', 'Lowest score at a test');
							?>
						</div>
					</div>

					<div class="span4">
						<div class="myperformances-icon-container">
							<span class="myactivities-sprite bars"></span>
						</div>
						<div class="myperformances-score-container">
							<span <?=$style;?> >
							<?php
							if (!empty($performanceInfo['average_score'])) {
								//TODO: number_format is not localized, a locale format should be used
								//NOTE: str_replace is brutal!! removal of zeros should be managed in a better way
								echo str_replace('.0', '', number_format($performanceInfo['average_score']['score'], 1, '.', ''))
									.'/'.str_replace('.0', '', number_format($performanceInfo['average_score']['score_max'], 1, '.', ''));
							} else {
								echo '-/-';
							}
							?>
							</span>
						</div>
						<div class="myperformances-text-container">
							<?php
							echo Yii::t('myactivities', 'Average tests score');
							?>
						</div>
					</div>

				</div>
				<div class="my-activities-test-statistics-title row statistics-title-bottom">
					<div class="span12"><?php echo Yii::t('myactivities', 'Statistics (in percent)'); ?></div>
			</div>
				<div class="text-center row"  >

					<div class="span4">
						<div class="myperformances-icon-container">
							<span class="myactivities-sprite cup yellow-up"></span>
		</div>
						<div class="myperformances-score-container" >
							<span <?=$style; ?> >
							<?php
							if (!empty($performanceInfo['highest_score_percent'])) {
								echo $performanceInfo['highest_score_percent']['score'].'/'.$performanceInfo['highest_score_percent']['score_max'];
							} else {
								echo '-/-';
							}
							?>
							</span>
						</div>
						<div class="myperformances-text-container">
							<?php
							echo Yii::t('myactivities', 'Highest score at a test');
							?>
						</div>
					</div>

					<div class="span4">
						<div class="myperformances-icon-container">
							<span class="myactivities-sprite cup black-down"></span>
	</div>
						<div class="myperformances-score-container">
							<span <?=$style; ?> >
							<?php
							if (!empty($performanceInfo['lowest_score_percent'])) {
								echo $performanceInfo['lowest_score_percent']['score'].'/'.$performanceInfo['lowest_score_percent']['score_max'];
							} else {
								echo '-/-';
							}
							?>
							</span>
						</div>
						<div class="myperformances-text-container">
							<?php
							echo Yii::t('myactivities', 'Lowest score at a test');
							?>
						</div>
					</div>

					<div class="span4">
						<div class="myperformances-icon-container">
							<span class="myactivities-sprite bars"></span>
						</div>
						<div class="myperformances-score-container">
							<span <?=$style; ?> >
							<?php
							if (!empty($performanceInfo['average_score_percent'])) {
								//TODO: number_format is not localized, a locale format should be used
								//NOTE: str_replace is brutal!! removal of zeros should be managed in a better way
								echo str_replace('.0', '', number_format($performanceInfo['average_score_percent']['score'], 1, '.', ''))
									.'/'.str_replace('.0', '', number_format($performanceInfo['average_score_percent']['score_max'], 1, '.', ''));
							} else {
								echo '-/-';
							}
							?>
							</span>
						</div>
						<div class="myperformances-text-container">
							<?php
							echo Yii::t('myactivities', 'Average tests score');
							?>
						</div>
					</div>

				</div>
			</div>

		</div>

	</div>


	<?php if (isset($coursesMostRecentResults) && is_array($coursesMostRecentResults) && !empty($coursesMostRecentResults)): ?>
		<div class="report-courses-summary">

			<div class="my-activities-stat-title row-fluid">
				<div class="span12"><?php echo Yii::t('myactivities', 'My top {n} most recent results', array('{n}' => count($coursesMostRecentResults))); ?></div>
			</div>
			<div class="myactivities-top-recent-content row-fluid">
				<?php foreach ($coursesMostRecentResults as $course): ?>
					<div class="course-due-container span4">

						<?php
						$scorePercent = $course['score']/$course['score_max'];
						$cupColor = 'bronze';
						if ($scorePercent > 0.799 && $scorePercent <= 0.999) {
							$cupColor = 'silver';
						} elseif ($scorePercent > 0.999) {
							$cupColor = 'gold';
						}

						echo '<span class="course-due-icon myactivities-sprite cup '.$cupColor.'"></span>';

						echo '<div class="course-due-date">';
						echo '<p class="score-obtain">'.$course['score'].'/'.$course['score_max'].'</p>';
						//echo '<br />';
						if(!isset($course['idCourse']) || (!$this->doesUserHavePermission('/lms/admin/course/view') || !CoreUserPuCourse::isCourseAssignedToPU(Yii::app()->user->id,  $course['idCourse']))){
							echo '<p class="course-name">'.$course['name'].'</p>';
						}else{
							echo '<p class="course-name">'.CHtml::link($course['name'], Docebo::createLmsUrl('player', array('course_id' => $course['idCourse']))).'</p>';

						}
						//echo '<br />';
						echo '<p class="score-obtain-date">'.Yii::app()->localtime->toLocalDate($course['date']).'</p>';
						echo '</div>';
						?>

					</div>
				<?php endforeach; ?>
			</div>

		</div>
	<?php endif; ?>


</div> <!-- End of .printable -->

<script type="text/javascript">

	$(document).ready(function() {

		replacePlaceholder();
		applyDonut('.donutContainer');

		//print sessions chart

		var sessionsChartData = [];
		<?php
		foreach ($sessionsChart['data'] as $element) {
			echo "\n".'sessionsChartData.push(['.CJSON::encode($element['x']).','.((int)$element['y']).']);';
		}
		?>
		$.jqplot.config.enablePlugins = true;
		var sessionsPlot = $.jqplot('sessions_chart', [sessionsChartData], {
			gridPadding:{right:27},
			axes:{
				xaxis:{
					renderer: $.jqplot.CategoryAxisRenderer,
					label: <?= CJSON::encode($sessionsChart['subtitle']) ?>,
					labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
					tickRenderer: $.jqplot.CanvasAxisTickRenderer,
					tickOptions: {}
				}
			},
			series:[
				{lineWidth:4, markerOptions: {style:'filledCircle'}},
			],
			grid: {
				background: '#ffffff',//'#f5f5f5',
				shadow: false,
				borderColor: '#CCC',
				borderWidth: 1,
				gridLineColor: '#f5f5f5'//'#fff'
			},
			seriesDefaults: {
				color: '#0465ac'
			},
			highlighter: {
				lineWidthAdjust: 2.5,   // pixels to add to the size line stroking the data point marker
				// when showing highlight.  Only affects non filled data point markers.
				sizeAdjust: 5,          // pixels to add to the size of filled markers when drawing highlight.
				showTooltip: true,      // show a tooltip with data point values.
				tooltipLocation: 'n',  // location of tooltip: n, ne, e, se, s, sw, w, nw.
				fadeTooltip: false,      // use fade effect to show/hide tooltip.
				tooltipFadeSpeed: "fast",// slow, def, fast, or a number of milliseconds.
				tooltipOffset: 2,       // pixel offset of tooltip from the highlight.
				tooltipAxes: 'y',    // which axis values to display in the tooltip, x, y or both.
				tooltipSeparator: ', ',  // separator between values in the tooltip.
				useAxesFormatters: false, // use the same format string and formatters as used in the axes to
				// display values in the tooltip.
				tooltipFormatString: '%d'//'%.0f' // sprintf format string for the tooltip.  only used if
				// useAxesFormatters is false.  Will use sprintf formatter with
				// this string, not the axes formatters.
			}
		});


		//print performances chart
		var performanceChartData = [];
		<?php
		if (!empty($performanceInfo['monthlyStats'])) {
			foreach ($performanceInfo['monthlyStats'] as $statDate => $monthStat) {
				echo "\n".'performanceChartData.push(["'.$statDate.'", '.($monthStat*100).']);';
			}
		}
		?>

		$.jqplot.config.enablePlugins = true;
		var performancePlot = $.jqplot('myperformances_year_chart', [performanceChartData], {
			gridPadding:{right:27},
			axes:{
				xaxis:{
					renderer: $.jqplot.CategoryAxisRenderer,
					//label: '',
					labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
					tickRenderer: $.jqplot.CanvasAxisTickRenderer,
					tickOptions: {}
				}
			},
			series:[{lineWidth:4, markerOptions:{style:'filledCircle'}}],
			grid: {
				background: '#ffffff',//'#f5f5f5',
				shadow: false,
				borderColor: '#CCC',
				borderWidth: 1,
				gridLineColor: '#f5f5f5'//'#fff'
			},
			seriesDefaults: {
				color: '#0465ac'
			},
			highlighter: {
				lineWidthAdjust: 2.5,   // pixels to add to the size line stroking the data point marker
				// when showing highlight.  Only affects non filled data point markers.
				sizeAdjust: 5,          // pixels to add to the size of filled markers when drawing highlight.
				showTooltip: true,      // show a tooltip with data point values.
				tooltipLocation: 'n',  // location of tooltip: n, ne, e, se, s, sw, w, nw.
				fadeTooltip: false,      // use fade effect to show/hide tooltip.
				tooltipFadeSpeed: "fast",// slow, def, fast, or a number of milliseconds.
				tooltipOffset: 2,       // pixel offset of tooltip from the highlight.
				tooltipAxes: 'y',    // which axis values to display in the tooltip, x, y or both.
				tooltipSeparator: ', ',  // separator between values in the tooltip.
				useAxesFormatters: false, // use the same format string and formatters as used in the axes to
				// display values in the tooltip.
				tooltipFormatString: '%d'//'%.0f' // sprintf format string for the tooltip.  only used if
				// useAxesFormatters is false.  Will use sprintf formatter with
				// this string, not the axes formatters.
			}
		});

		//assets-views-chart-container
		var assets_views_data = [];
		var shared_assets_data = [];
		<?php
		if (!empty($coachShareCharts)) {
			foreach ($coachShareCharts as $key => $statGraph) {
				if(count($coachShareCharts) > 12 && $key != 0 || count($coachShareCharts) == 12){
					echo "\n".'assets_views_data.push(["'.$statGraph['x'].'", '.(int)$statGraph['y_views'].']);';
					echo "\n".'shared_assets_data.push(["'.$statGraph['x'].'", '.(int)$statGraph['y_shares'].']);';
				}
			}
		}
		?>
		<?php if(PluginManager::isPluginActive('Share7020App')): ?>
			$.jqplot.config.enablePlugins = true;
			var performancePlot = $.jqplot('assets-views-chart-container', [assets_views_data, shared_assets_data], {
				gridPadding:{right:27},
				axes:{
					xaxis:{
						renderer: $.jqplot.CategoryAxisRenderer,
						labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
						tickRenderer: $.jqplot.CanvasAxisTickRenderer,
						tickOptions: {}
					},
					yaxis:{
						min: 0
					}
				},
				series:[
					{lineWidth:4, markerOptions:{style: 'filledCircle'}, color: '#0465ac'},
					{lineWidth:4, markerOptions:{style: 'filledCircle'}, color: '#52a1dc'}
				],
				grid: {
					background: '#ffffff',//'#f5f5f5',
					shadow: false,
					borderColor: '#CCC',
					borderWidth: 1,
					gridLineColor: '#f5f5f5'//'#fff'
				},
				highlighter: {
					lineWidthAdjust: 2.5,   // pixels to add to the size line stroking the data point marker
					// when showing highlight.  Only affects non filled data point markers.
					sizeAdjust: 5,          // pixels to add to the size of filled markers when drawing highlight.
					showTooltip: true,      // show a tooltip with data point values.
					tooltipLocation: 'n',  // location of tooltip: n, ne, e, se, s, sw, w, nw.
					fadeTooltip: false,      // use fade effect to show/hide tooltip.
					tooltipFadeSpeed: "fast",// slow, def, fast, or a number of milliseconds.
					tooltipOffset: 2,       // pixel offset of tooltip from the highlight.
					tooltipAxes: 'y',    // which axis values to display in the tooltip, x, y or both.
					tooltipSeparator: ', ',  // separator between values in the tooltip.
					useAxesFormatters: false, // use the same format string and formatters as used in the axes to
					// display values in the tooltip.
					tooltipFormatString: '%d'//'%.0f' // sprintf format string for the tooltip.  only used if
					// useAxesFormatters is false.  Will use sprintf formatter with
					// this string, not the axes formatters.
				}
			});
		<? endif; ?>
	});

</script>
