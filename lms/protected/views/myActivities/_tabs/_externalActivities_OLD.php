<div>

	<?php $renderer = new TranscriptsBaseController($this->id); ?>
	<div class="user-report-courses" style="margin-bottom: 20px;">
		<div class="courses-title"><?php echo Yii::t('transcripts', 'External activities'); ?></div>
		<div class="courses-grid-wrapper">
			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id' => 'transcripts-management-grid',
				'htmlOptions' => array('class' => 'grid-view clearfix'),
				'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
				'dataProvider' => $transcriptsModel->dataProvider(),
				'cssFile' => false,
				'template' => '{items}{summary}{pager}',
				'summaryText' => Yii::t('standard', '_TOTAL'),
				'pager' => array(
					'class' => 'adminExt.local.pagers.DoceboLinkPager',
				),
				'ajaxUpdate' => 'all-items',
				'beforeAjaxUpdate' => 'function(id, options) {
					options.type = "POST";
					if (options.data == undefined) {
						options.data = {contentType: "html"}
					}
				}',
				'columns' => array(
					array(
						'name' => 'course_name',
						'value' => '$data->course_name',
						'type'=>'raw'
					),
					array(
						'name' => 'course_type',
						'value' => array($renderer , 'gridRenderCourseType'),
						'type'=>'raw'
					),
					array(
						'header' => Yii::t('standard', '_DATE'),
						'name' => 'date',
						'value' => array($renderer , 'gridRenderDate')
					),
					array(
						'header' => Yii::t('standard', '_SCORE'),
						'name' => 'score',
						'value' => array($renderer , 'gridRenderScore'),
						'htmlOptions' => array('class' => 'text-center'),
						'headerHtmlOptions' => array('class' => 'text-center')
					),
					array(
						'header' => Yii::t('standard', '_CREDITS'),
						'name' => 'credits',
						'value' => array($renderer , 'gridRenderCredits'),
						'htmlOptions' => array('class' => 'text-center'),
						'headerHtmlOptions' => array('class' => 'text-center')
					),
					array(
						'header' => Yii::t('transcripts', 'Training institute'),
						'name' => 'training_institute',
						'value' => '$data->training_institute'
					),
					array(
						'value' => function($data, $index) use (&$renderer) {
								$html = '';
								if (!empty($data->certificate)) {
									$html = $renderer->gridRenderCertificate($data, $index);
								}
								return $html;
							},//array($renderer , 'gridRenderCertificate'),
						'type' => 'raw',
						'htmlOptions' => array('class' => 'text-center'),
						'headerHtmlOptions' => array('class' => 'text-center')
					)
				),
			)); ?>
		</div>
	</div>

</div>