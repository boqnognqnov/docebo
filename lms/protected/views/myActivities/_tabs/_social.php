<?php
//TODO: maybe not all of these js plugins are needed
$this->widget('common.extensions.jqplot.JqplotGraphWidget', array(
	'pluginScriptFile'=>array(
		'jqplot.dateAxisRenderer.js',
		'jqplot.barRenderer.js',
		'jqplot.categoryAxisRenderer.js',
		'jqplot.highlighter.js',
		'jqplot.canvasTextRenderer.js',
		'jqplot.canvasAxisLabelRenderer.js',
	)
));
?>
<div>

	<div class="printable user-report-social">

		<div class="myactivities-title"><?php echo Yii::t('standard', '_FORUM'); ?></div>
		<div class="social-container">

			<div class="social-titles row-fluid">
				<div class="span8"><?= Yii::t('myactivities', 'General data') ?></div>
				<div class="span4"><?= Yii::t('myactivities', 'Weekly forum activity') ?></div>
			</div>

			<div class="social-contents row-fluid">
				<div class="bordered-bottom span8 clearfix">
					<div class="info-block">
						<div class="sb-icon"><span class="myactivities-sprite balloon-double"></span></div>
						<div class="sb-info"><?= $forumMessageModel->getCountOfTotalUserPosts($userModel->getPrimaryKey()) ?></div>
						<div class="sb-text"><?= Yii::t('myactivities', 'Total posts') ?></div>
					</div>
					<div class="info-block">
						<div class="sb-icon"><span class="myactivities-sprite balloon-single"></span></div>
						<div class="sb-info"><?= $forumThreadModel->getCountOfTotalUserThreads($userModel->getPrimaryKey()) ?></div>
						<div class="sb-text"><?= Yii::t('myactivities', 'Opened threads') ?></div>
					</div>
					<div class="info-block">
						<?php $_helpfulCounts = $forumMessageModel->getUserTotalHelpfulVotes($userModel->getPrimaryKey()); ?>
						<div class="row-fluid">
							<div class="span6">
								<div class="sb-icon"><span class="myactivities-sprite helpful-yes"></span></div>
								<div class="sb-info"><?= $_helpfulCounts['yes'] ?></div>
							</div>
							<div class="span6">
								<div class="sb-icon"><span class="myactivities-sprite helpful-not"></span></div>
								<div class="sb-info"><?= $_helpfulCounts['not'] ?></div>
							</div>
						</div>
						<div class="sb-text"><?= Yii::t('myblog', 'Helpful?') ?></div>
					</div>
					<div class="info-block">
						<div class="sb-icon">
							<div class="avg-tones-container">
							<?php
							$avg = $forumMessageModel->getUserAverageToneVotes($userModel->getPrimaryKey());
							$this->widget('common.widgets.DoceboStarRating',array(
								'id' => 'forum-message-rating-average',
								'name' => 'forum-message-rating-average',
								'type' => 'star',
								'readOnly' => true,
								'value' => round($avg),
							));
							?>
							</div>
						</div>
						<div class="sb-info"><?= Yii::app()->format->formatNumber($avg) ?>/5</div>
						<div class="sb-text"><?= Yii::t('myactivities', 'Average posts rating') ?></div>
					</div>
					<div class="info-block">
						<div class="rating-details-container">
							<table>
								<tbody>
								<?php
								for ($i=0; $i<5; $i++) {
									echo '<tr>';
									echo '<td class="rating-detail-stars">';
									$this->widget('common.widgets.DoceboStarRating',array(
										'id' => 'forum-message-rating-details-'.$i,
										'name' => 'forum-message-rating-details-'.$i,
										'type' => 'star',
										'readOnly' => true,
										'value' => $i+1,
									));
									echo '</td>';
									echo '<td class="rating-detail-count">';
									echo $forumMessageModel->countUserToneVotesByVote($userModel->getPrimaryKey(), $i+1);
									echo '</td>';
									echo '</tr>';
								}
								?>
								</tbody>
							</table>
						</div>
						<div class="sb-text"><?= Yii::t('myactivities', 'Posts rating details') ?></div>
					</div>
				</div>
				<div class="span4">
					<div class="report-activities-summary">
						<div class="lineChart-wrapper">
							<div id="forum_chart" style="height:<?= $forumChart['height'] ?>;"></div>
						</div>
					</div>
				</div>
			</div>



		</div>

	</div>


	<?php if (PluginManager::isPluginActive('MyBlogApp')): ?>
	<div class="printable user-report-social">

		<div class="myactivities-title"><?php echo Yii::t('myblog', 'Blog'); ?></div>
		<div class="social-container">

			<div class="social-titles row-fluid">
				<div class="span8"><?= Yii::t('myactivities', 'General data') ?></div>
				<div class="span4"><?= Yii::T('myactivities', 'Weekly blog activity') ?></div>
			</div>

			<div class="social-contents row-fluid">
				<div class="bordered-bottom span8 clearfix">
					<div class="info-block">
						<div class="sb-icon"><span class="myactivities-sprite blog-post"></span></div>
						<div class="sb-info"><?= $blogModel->getCountOfTotalUserPosts($userModel->getPrimaryKey()) ?></div>
						<div class="sb-text"><?= Yii::t('myactivities', 'Total posts') ?></div>
					</div>
					<div class="info-block">
						<?php $_helpfulCounts = $blogModel->getUserTotalHelpfulVotes($userModel->getPrimaryKey()); ?>
						<div class="row-fluid">
							<div class="span6">
								<div class="sb-icon"><span class="myactivities-sprite helpful-yes"></span></div>
								<div class="sb-info"><?= $_helpfulCounts['yes'] ?></div>
							</div>
							<div class="span6">
								<div class="sb-icon"><span class="myactivities-sprite helpful-not"></span></div>
								<div class="sb-info"><?= $_helpfulCounts['not'] ?></div>
							</div>
						</div>
						<div class="sb-text"><?= Yii::t('myblog', 'Helpful?') ?></div>
					</div>
					<div class="info-block">
						<div class="sb-icon">
							<div class="avg-tones-container">
							<?php
							$avg = $blogModel->getUserAverageToneVotes($userModel->getPrimaryKey());
							$this->widget('common.widgets.DoceboStarRating',array(
								'id' => 'blog-message-rating-average',
								'name' => 'blog-message-rating-average',
								'type' => 'star',
								'readOnly' => true,
								'value' => round($avg),
							));
							?>
							</div>
						</div>
						<div class="sb-info"><?= Yii::app()->format->formatNumber($avg) ?>/5</div>
						<div class="sb-text"><?= Yii::t('myactivities', 'Average posts rating') ?></div>
					</div>
					<div class="info-block">
						<div class="rating-details-container">
							<table>
								<tbody>
								<?php
								for ($i=0; $i<5; $i++) {
									echo '<tr>';
									echo '<td class="rating-detail-stars">';
									$this->widget('common.widgets.DoceboStarRating',array(
										'id' => 'blog-message-rating-details-'.$i,
										'name' => 'blog-rating-details-'.$i,
										'type' => 'star',
										'readOnly' => true,
										'value' => $i+1,
									));
									echo '</td>';
									echo '<td class="rating-detail-count">';
									echo $blogModel->countUserToneVotesByVote($userModel->getPrimaryKey(), $i+1);
									echo '</td>';
									echo '</tr>';
								}
								?>
								</tbody>
							</table>
						</div>
						<div class="sb-text"><?= Yii::t('myactivities', 'Posts rating details') ?></div>
					</div>
				</div>
				<div class="span4">
					<div id="blog_chart" style="height:<?= $blogChart['height'] ?>;"></div>
				</div>
			</div>

		</div>

	</div>
	<?php endif; ?>

</div>

<script type="text/javascript">

	$(document).ready(function() {
		//applyLineCharts('.lineChartContainer');

		try {

			//forum chart
			var forumChartData = [];
			<?php
			foreach ($forumChart['data'] as $element) {
				echo "\n".'forumChartData.push(['.CJSON::encode($element['x']).','.((int)$element['y']).']);';
			}
			?>

			$.jqplot.config.enablePlugins = true;
			var plot1 = $.jqplot('forum_chart', [forumChartData], {
				gridPadding:{right:27},
				axes:{
					xaxis:{
						renderer: $.jqplot.CategoryAxisRenderer,
						label: <?= CJSON::encode($forumChart['subtitle']) ?>,
						labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
						tickRenderer: $.jqplot.CanvasAxisTickRenderer,
						tickOptions: {}
					}
				},
				series:[{lineWidth:4, markerOptions:{style:'filledCircle'}}],
				grid: {
					background: '#ffffff',//'#f5f5f5',
					shadow: false,
					borderColor: '#CCC',
					borderWidth: 1,
					gridLineColor: '#f5f5f5'//'#fff'
				},
				seriesDefaults: {
					color: '#0465ac'
				},
				highlighter: {
					lineWidthAdjust: 2.5,   // pixels to add to the size line stroking the data point marker
					// when showing highlight.  Only affects non filled data point markers.
					sizeAdjust: 5,          // pixels to add to the size of filled markers when drawing highlight.
					showTooltip: true,      // show a tooltip with data point values.
					tooltipLocation: 'n',  // location of tooltip: n, ne, e, se, s, sw, w, nw.
					fadeTooltip: false,      // use fade effect to show/hide tooltip.
					tooltipFadeSpeed: "fast",// slow, def, fast, or a number of milliseconds.
					tooltipOffset: 2,       // pixel offset of tooltip from the highlight.
					tooltipAxes: 'y',    // which axis values to display in the tooltip, x, y or both.
					tooltipSeparator: ', ',  // separator between values in the tooltip.
					useAxesFormatters: false, // use the same format string and formatters as used in the axes to
					// display values in the tooltip.
					tooltipFormatString: '%d' // sprintf format string for the tooltip.  only used if
					// useAxesFormatters is false.  Will use sprintf formatter with
					// this string, not the axes formatters.
				}
			});

			<?php if (PluginManager::isPluginActive('MyBlogApp')): ?>
			//blog chart
			var blogChartData = [];
			<?php
			foreach ($blogChart['data'] as $element) {
				echo "\n".'blogChartData.push(['.CJSON::encode($element['x']).','.((int)$element['y']).']);';
			}
			?>

			$.jqplot.config.enablePlugins = true;
			var plot1 = $.jqplot('blog_chart', [blogChartData], {
				gridPadding:{right:27},
				axes:{
					xaxis:{
						renderer: $.jqplot.CategoryAxisRenderer,
						label: <?= CJSON::encode($blogChart['subtitle']) ?>,
						labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
						tickRenderer: $.jqplot.CanvasAxisTickRenderer,
						tickOptions: {}
					}
				},
				series:[{lineWidth:4, markerOptions:{style:'filledCircle'}}],
				grid: {
					background: '#ffffff',//'#f5f5f5',
					shadow: false,
					borderColor: '#CCC',
					borderWidth: 1,
					gridLineColor: '#f5f5f5'//'#fff'
				},
				seriesDefaults: {
					color: '#0465ac'
				},
				highlighter: {
					lineWidthAdjust: 2.5,   // pixels to add to the size line stroking the data point marker
					// when showing highlight.  Only affects non filled data point markers.
					sizeAdjust: 5,          // pixels to add to the size of filled markers when drawing highlight.
					showTooltip: true,      // show a tooltip with data point values.
					tooltipLocation: 'n',  // location of tooltip: n, ne, e, se, s, sw, w, nw.
					fadeTooltip: false,      // use fade effect to show/hide tooltip.
					tooltipFadeSpeed: "fast",// slow, def, fast, or a number of milliseconds.
					tooltipOffset: 2,       // pixel offset of tooltip from the highlight.
					tooltipAxes: 'y',    // which axis values to display in the tooltip, x, y or both.
					tooltipSeparator: ', ',  // separator between values in the tooltip.
					useAxesFormatters: false, // use the same format string and formatters as used in the axes to
					// display values in the tooltip.
					tooltipFormatString: '%d'//'%.0f' // sprintf format string for the tooltip.  only used if
					// useAxesFormatters is false.  Will use sprintf formatter with
					// this string, not the axes formatters.
				}
			});
			<?php endif; ?>
		} catch (e) {
			//alert(e);
		}


	});

</script>