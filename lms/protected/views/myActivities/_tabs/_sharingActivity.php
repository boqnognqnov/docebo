<?php
//echo '<pre>';
//	print_r($renderData);
//echo '</pre>';
?>



<div id="userReportSharingActivity" class="row-fluid" data-user-id="<?php echo $renderData['searchedUserId'] ?>">
	<div class="span12">
		<h3 class="border"><?= Yii::t('app7020', 'Sharing Activity'); ?></h3>
		<div class="timeframe-selector">
			<label for="timeframe-selector"><?= Yii::t('app7020', 'Timeframe'); ?></label>
			<?php echo CHtml::dropDownList('timeframeSelect', $renderData['preselectedTimeframe'], $renderData['timeframes']); ?>
		</div>
		<div class="clear"></div>
		<h4><?php echo Yii::t('app7020', 'Overview'); ?></h4>
		<!-- Info boxes -->
		<div class="row-fluid">
			<div class="span4 data-box average-rating-container">
				<div class="row-fluid rating-row">
					<div class="span8">
						<?php
						// contentId is Fake because I don't need it in this case
						$this->widget('common.widgets.ContentRating', array('contentId' => 999999999, 'readOnly' => true, 'avgRating' => $renderData['averageRating']));
						?>
						<div class="rating-text">
							<?php
							echo Yii::t('app7020', 'Shared assets avg. rating');
							?>
						</div>
					</div>
					<div class="span4 num">
						<?php echo $renderData['averageRating']; ?>
					</div>
				</div>
			</div>
			<div class="span4 data-box">
				<span id="sharedContents" class="num"><?php echo $renderData['sharedContents']; ?></span>
				<span class="text"><?= Yii::t('app7020', "Shared contents"); ?></span>
			</div>
			<div class="span4 data-box">
				<span id="totalViews" class="num"><?php echo $renderData['totalViews']; ?></span>
				<span class="text"><?= Yii::t('app7020', 'Total views'); ?></span>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span4 data-box">
				<span id="averageWatchRate" class="num"><?php echo $renderData['averageWatchRate']; ?><span>%</span></span>
				<span class="text"><?= Yii::t('app7020', 'Avg. watch rate'); ?></span>
			</div>
		</div>
		<!-- Chart 'Activity per channel' --> 
		<h3><?= Yii::t('app7020', 'Activity per channel'); ?></h3>
		<div class="row-fluid">
			<div class="span12 activity-per-channel-container">
				<?php
				$this->widget('common.widgets.app7020ActivityChannels', array('data' => $renderData['chartMyActivityPerChannel']));
				?>
			</div>
		</div>
		<!-- Chart 'Shared assets (contributions)' --> 
		<h3><?= Yii::t('app7020', 'My Shared assets (contributions)'); ?></h3>
		<canvas id="app7020ChartSharedAssets" width="820" height="200"></canvas>
		<!-- Chart 'Assets views' --> 
		<h3><?= Yii::t('app7020', 'My Assets views'); ?></h3>
		<canvas id="app7020ChartAssetsViews" width="820" height="200"></canvas>






	</div>
</div>