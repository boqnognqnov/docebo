<div>

	<div class="printable user-report-courses" style="margin-bottom: 20px;">

		<div class="myactivities-badges-info row-fluid">
			<div class="span6">
				<div class="myactivities-title"><?php echo Yii::t('gamification', 'My Badges and Points'); ?></div>
			</div>
			<div class="span3">
				<span class="gamification-sprite star-square-small-black"></span>
				<?= Yii::t('gamification', 'My Badges') . ': ' . CHtml::tag('strong', array(), intval($countBadges)) ?>
			</div>
			<div class="span3">
				<span class="gamification-sprite star-big"></span>
				<?= Yii::t('gamification', 'My Points') . ': ' . CHtml::tag('strong', array(), intval($countPoints)) ?>
			</div>
		</div>

		<div class="courses-grid-wrapper">
			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id' => 'badges-management-grid',
				'htmlOptions' => array('class' => 'grid-view clearfix'),
				'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
				'dataProvider' => $assignedBadgesModel->dataProviderMyActivities($userModel->getPrimaryKey()),
				'cssFile' => false,
				'template' => '{items}{summary}{pager}',
				'summaryText' => Yii::t('standard', '_TOTAL'),
				'pager' => array(
					'class' => 'adminExt.local.pagers.DoceboLinkPager',
				),
				'ajaxUpdate' => 'all-items',
				'beforeAjaxUpdate' => 'function(id, options) {
                    options.type = "POST";
                    if (options.data == undefined) {
                        options.data = {
                            contentType: "html",
                        }
                    }
                }',
				'columns' => array(
					array(
						'name' => 'icon',
						'header' => '',
						'type' => 'raw',
						'htmlOptions' => array('class' => 'myactivities-badge-column'),
						'value' => function($data, $index) {
								return CHtml::image($data->gamificationBadge->getIconUrl(), Yii::t('gamification', 'Badge icon'), array('id' => 'badge-icon-'.$data->id_badge));
							}
					),
					array(
						'name' => 'name',
						'header' => Yii::t('standard', '_NAME'),
						'value' => '$data->gamificationBadge->renderBadgeName();',
					),
					array(
						'header' => Yii::t('test', '_TEST_SCORES'),
						'name' => 'score',
						'value' => '$data->gamificationBadge->renderBadgePoints();',
						'htmlOptions' => array('class' => 'text-center'),
						'headerHtmlOptions' => array('class' => 'text-center')
					),
					array(
						'header' => Yii::t('gamification', 'Issued on'),
						'name' => 'issued_on',
						'value' => function($data, $index) {
								$html = '';
								if (!empty($data->issued_on)) {
									$html = Yii::app()->localtime->stripTimeFromLocalDateTime($data->issued_on);
								}
								return $html;
							}
					),
					array(
						'header' => Yii::t('standard', '_EVENT'),
						'name' => 'event_name',
						'value' => '$data->renderBadgeEvent();'
					)
				),
			)); ?>
		</div>
	</div>

</div>