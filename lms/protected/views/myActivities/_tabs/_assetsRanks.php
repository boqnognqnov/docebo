<div id="userReportAssetsRanks" class="row-fluid" data-user-id="<?php echo $renderData['searchedUserId'] ?>">
	<div class="span12">
		<h3><?= Yii::t('app7020', 'Assets ranks'); ?></h3>
		<?php
			$listParams = array(
				'listId' => "assets-ranks",
				'disableMassSelection' => true,
				'disableMassActions' => true,
				'disableCheckBox' => true,
				'hiddenFields' => array('orderASC' => 0,'orderType'=>'views'),
				'afterAjaxUpdate' => 'app7020.userReportAssetsRanks.toggleClass();',
				'viewData' => array(
					'assetsTypes' => $renderData['assetsTypes']
				),
				'dataProvider' => $renderData['assetsRanks'],
				'restyleAfterUpdate' => false,
				'listItemView' => 'app7020.protected.views.userReport._assets_ranks_item',
				'columns' => 1,
				'template' =>
				'<div class="row-fluid assets-ranks-header">
						<div class="span4 title"></div>
						<div class="span1 views" data-type="views">' . Yii::t('app7020', 'VIEWS') . '</div>
						<div class="span1 rating break-word" data-type="rating">' . Yii::t('app7020', 'RATING') . '</div>
						<div class="span2 invitation" data-type="invitation">' . Yii::t('app7020', 'INVITATION') . '</div>
						<div class="span2 watch-rate" data-type="wr">' . Yii::t('app7020', 'WATCH RATE') . '</div>
						<div class="span2 reaction-time">' . Yii::t('app7020', 'REACTION TIME') . '</div>
					</div>'
				. '{items}{pager}{summary}'
			);

			$this->widget('common.widgets.ComboListView', $listParams);
		?>		
	</div>	
</div>		