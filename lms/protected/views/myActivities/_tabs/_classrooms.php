<style>
	a{
		color: #333333;
	}
	a:hover{
		color: #333333;
	}
</style>
<div>

	<?php if(PluginManager::isPluginActive("ClassroomApp")): ?>
		<div class="printable user-report-classroom-courses">
			<div class="courses-title"><?php echo Yii::t('classroom', 'Classrooms'); ?></div>
			<div class="courses-grid-wrapper">
				<?php

				if($this->doesUserHavePermission('/lms/admin/classroomsessions/view') || $this->doesUserHavePermission('/lms/admin/report/view'))
				{

					$columns = array();
					$sessionNameValue = '$data->renderCourseLink($data->session->course->name." - ".$data->session->name)';
					if(!$this->doesUserHavePermission('/lms/admin/classroomsessions/view')){
						$sessionNameValue = '$data->session->course->name." - ".$data->session->name';
					}

					$columns[] = array(
						'name' => 'session.name',
						'header' => Yii::t('classroom', 'Course and session information'),
						'value' => $sessionNameValue,
						'type' => 'raw'
					);
					$columns[] = array(
						'header' => Yii::t('standard', 'Enrolled'),
						'name' => 'date_subscribed',
						'value' => function ($data, $index)
						{
							$html = '';
							if(!empty($data->date_subscribed))
							{
								$html = Yii::app()->localtime->stripTimeFromLocalDateTime($data->date_subscribed);
							}
							return $html;
						}
					);
					$columns[] = array(
						'header' => Yii::t('standard', '_ATTENDANCE'),
						'name' => 'session.attendance',
						'value' => '$data->session->renderAttendance($data->id_user)',
					);
					$columns[] = array(
						'header' => Yii::t('standard', '_STATUS'),
						'name' => 'status',
						'type' => 'raw',
						'value' => function ($data, $index)
						{
							$trans = Yii::t("standard", LtCourseuserSession::getStatusLangLabel($data->status));
							$additional = '';
							switch($data->status)
							{
								case LearningCourseuser::$COURSE_USER_SUBSCRIBED:
									$additional = ' to-begin';
									break;
								case LearningCourseuser::$COURSE_USER_BEGIN:
									$additional = ' in-progress';
									break;
								case LearningCourseuser::$COURSE_USER_END:
									$additional = ' completed';
									break;
							}
							$html = '<div class="user-status' . $additional . '">' . $trans . '</div>';
							return $html;
						}
					);
					$columns[] = array(
						//							'name' => 'score',
						'header' => Yii::t('standard', '_SCORE'),
						'value' => '$data->renderSessionScore()',
					);

					if($this->doesUserHavePermission('/lms/admin/certificate/view'))
					{
						$columns[] = array(
							'type' => 'raw',
							'value' => function ($data, $index) use (&$userModel)
							{
								$html = '';
								$idCourse = $data->ltCourseSession->course->idCourse;
								$userCertificate = LearningCertificateAssign::loadByUserCourse($userModel->getPrimaryKey(), $idCourse);
								if($userCertificate)
								{
									$html = CHtml::link(
										'<span class="i-sprite is-cert"></span>',
										//Docebo::createLmsUrl('/player/training/downloadCertificate', array('course_id' => $idCourse)),
										Docebo::createLmsUrl('myActivities/downloadCertificate', array('course_id' => $idCourse, 'id_user' => $userModel->getPrimaryKey())),
										array(
											'id' => 'dl-cert-' . $idCourse,
											'title' => Yii::t('certificate', '_TAKE_A_COPY'),
											'target' => '_blank'
										)
									);
								}
								return $html;
							}
						);
					}

					$this->widget('DoceboCGridView', array(
						'id' => 'classroom-course-management-grid',
						'htmlOptions' => array('class' => 'grid-view clearfix'),
						'dataProvider' => LtCourseuserSession::model()->dataProviderClassroomCoursesActivity($userModel->getPrimaryKey()),
						'cssFile' => false,
						'template' => '{items}{summary}{pager}',
						'summaryText' => Yii::t('standard', '_TOTAL'),
						'pager' => array(
							'class' => 'adminExt.local.pagers.DoceboLinkPager',
						),
						'ajaxUpdate' => 'all-items',
						'beforeAjaxUpdate' => 'function(id, options) {
                    options.type = "POST";
                    if (options.data == undefined) {
                        options.data = {
                            contentType: "html",
                        }
                    }
                }',
						'columns' => $columns
					));

				} else {
					echo Yii::t('standard', 'Do not have the needed permissions');
				}

				?>
			</div>
		</div>
	<?php endif; ?>

</div>


<style>
	/* dunno why, but these were totally broken - http://prntscr.com/6ic706 */
	.gridItemsContainer {
		clear: none;
	}
	.doceboPager {
		width: 80%;
		margin-top: 22px;
		float: right;
	}
</style>