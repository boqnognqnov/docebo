<div class="printable user-report-courses" style="margin-bottom: 20px;">
	<div class="courses-title">
		<?=Yii::t('certification', 'Certifications & Retraining')?>
	</div>

<?php


$groupHtml =
	//'Show also &nbsp;&nbsp;' .
	CHtml::checkBox('show_expired', true, array('uncheckValue'=>0)).' '.Yii::t('certifications', 'Show also expired certifications') . "&nbsp;&nbsp;"
	//. CHtml::checkBox('show_archived', false, array('uncheckValue'=>0)).' '.Yii::t('certifications', 'Archived certifications')
	;


$certsGridParams = array(
	'gridId'			=> 'activities-certifications-grid',
	'dataProvider'		=> $dataProvider,
	'columns'			=> $columns,
	'autocompleteRoute'	=> '//CertificationApp/CertificationApp/certsAutocomplete',
	'disableMassSelection'=>true,
	'disableMassActions'=>true,
	'customFilterGroups'	=> array(
		array(
			'groupClass'=>'show-expired',
			'groupHtml'=>$groupHtml,
		)
	),
);
$this->widget('common.widgets.ComboGridView', $certsGridParams);
?>

</div>

<script>
	$(function(){
		$('input').styler();

		$('.show-expired .jq-checkbox, .children-node-content .jq-checkbox').click(function () {
			$('#' + <?= json_encode($certsGridParams['gridId']) ?>).comboGridView('updateGrid');
		});
	})
</script>