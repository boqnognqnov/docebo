<?php
	/**
	 * Upon rendering, make an AJAX call to render USER-aware "External training" interface
	 * The OLD approcah (as a separate item in My Menu) is copied to _externalActivities_OLD.php
	 */
	$url = Docebo::createAbsoluteLmsUrl('TranscriptsApp/TranscriptsUser/indexPartial', array('id_user' => $userModel->getPrimaryKey()));
?>
<div id="user-external-activities-container">
	<div class="ajaxloader"></div>
</div>

<script type="text/javascript">

<!--
	var url = '<?= $url ?>';

	$(function(){
		$('.ajaxloader').css('display', 'block').show();
		$.ajax({
			type: 'POST',
			url: url,
			dataType: 'html',
			success: function(data) {
				$('#user-external-activities-container').html(data);
				$('#user-external-activities-container').controls();
			}
		});
	});


//-->
</script>
