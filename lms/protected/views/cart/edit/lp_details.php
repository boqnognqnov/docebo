<?php
/* @var $position CoursepathCartPosition */
/* @var $pos_id integer */
?>
<div>
	<a href="#" class="open-dialog" rel="dialog-details-<?=$pos_id?>">
		<?= Yii::t('standard', '_DETAILS') ?>
	</a>
</div>
<script type="text/javascript">
	$(function(){
		$("#dialog-details-<?=$pos_id?>").dialog2({
			showCloseHandle: true,
			removeOnClose: false,
			autoOpen: false,
			closeOnEscape: true,
			closeOnOverlayClick: true
		});
		$('.open-dialog').click(function(){
			if($('#'+$(this).attr('rel')).length > 0){
				$('#'+$(this).attr('rel')).dialog2('open');
			}
		});
	});
</script>
<div id="dialog-details-<?=$pos_id?>" style="display: none">
	<h1><?= Yii::t('standard', '_SELECTED') ?></h1>
	<? foreach($position->getCourses() as $course): /* @var $course LearningCourse */ ?>
		<? $thumb = $course->getCourseLogoUrl(CoreAsset::VARIANT_SMALL); ?>

		<div style="margin-right: 20px; width: 90px; float: left; height: 90px;">
			<? if($thumb): ?>
				<img src="<?=$thumb?>" class="pull-left" style="max-width: 90px;"/>
			<? endif; ?>
		</div>

		<h3><?=$course->name?></h3>
		<p><?=$course->description?></p>
		<div class="clearfix"></div>

		<br/>
	<? endforeach; ?>
</div>