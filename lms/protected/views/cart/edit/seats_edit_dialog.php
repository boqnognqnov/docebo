<div id="dialog-details-<?=$pos_id?>" >
	<h1><?= Yii::t('standard', 'Seats for {course}', array('{course}' => $position->getCourseNameFormatted())) ?></h1>
	<?=CHtml::beginForm(Docebo::createLmsUrl('course/buyMoreSeats', array('id'=>$position->idCourse)), 'POST', array(
		'class'=>'form ajax'
	))?>

	<div class="row-fluid">
		<div class="span8"><?= CHtml::label(Yii::t('standard', 'Update the number of seats to purchase'),'course_'.$position->idCourse)?></div>
		<div class="span4" style="text-align: right"><?= CHtml::numberField('course['.$position->idCourse.']', $position->getSeats(), array(
				'class' => '',
				'style' => 'width: 140px;',
				'min' => 0,
				'id' => 'course_' . $position->idCourse
			));?>
		</div>
	</div>

	<div class="form-actions">
		<input class="btn-docebo green big" id="block-edit-save-button" type="submit"
		       value="<?php echo Yii::t('standard', '_SAVE'); ?>">&nbsp;&nbsp;
		<input class="btn-docebo black big close-dialog" type="button" value="<?php echo Yii::t('standard', '_CANCEL'); ?>">
	</div>
	<?=CHtml::endForm()?>
</div>