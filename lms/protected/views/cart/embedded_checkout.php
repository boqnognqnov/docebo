<?php
/** @var $this CartController */
/** @var $response [] */
?>

<style>
<!--


-->
</style>

<div class="checkout-wrapper">

    <!-- HEADER PART -->
    <div class="row-fluid">
        <div class="span7">
            <div class="span2 sprite-checkout cart"></div>
            <div class="span10">
                <h1 class="checkout-header"><?= Yii::t('cart', 'Complete and confirm your order') ?></h1>
            </div>
        </div>
        <div class="span5"></div>
    </div>
    <br>

    <div class="row-fluid">

		<?php
		if ($response['RESULT'] != 0) {
			pre($response, "Payflow call failed");
			exit(0);
		} else {
			$securetoken = $response['SECURETOKEN'];
			$securetokenid = $response['SECURETOKENID'];
			$mode = $response['mode'];
		}

		echo '<div style="width:492px; height:567px;">'; // wrap iframe in a dashed wireframe for demo purposes
			echo "  <iframe src='https://payflowlink.paypal.com?SECURETOKEN=$securetoken&SECURETOKENID=$securetokenid&MODE=$mode' width='490' height='565' border='0' frameborder='0' scrolling='no' allowtransparency='true'>\n</iframe>";
			echo "</div>";

		/*
		?>
		<div style="font-family:sans-serif;font-weight:normal;">
			<p><big><strong>Fake credit card data</strong></big></p>
			<ul><li>4111111111111111</li><li>12/15</li></ul>
			<p><big><strong>References</strong></big></p>
			<ol>
				<li><a href="https://cms.paypal.com/cms_content/US/en_US/files/developer/PayflowGateway_Guide.pdf">Payflow Gateway Developer Guide</a> (.pdf)</li>
				<li><a href="https://cms.paypal.com/cms_content/US/en_US/files/developer/Embedded_Checkout_Design_Guide.pdf">Embedded Checkout Design Guide</a> (for Layout C)</li>
				<li><a href="https://www.x.com/developers/community/blogs/pp_integrations_preston/testing-paypal-payflow-gateway">Testing with the PayPal Payflow Gateway</a></li>
			</ol>
		</div>
	<?php */ ?>

	</div>

</div>