<?php 
	$hasError = Yii::app()->user->hasFlash('error');

?>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<style>
		html, body{
			height:100%;width:100%;margin:0;padding:0;overflow:hidden;
		}
		.loading{
			text-align: center;
			position: absolute;
			height: 170px;
			width: 400px;
			margin: -200px 0 0 -200px;
			left:50%;
			top:50%;
		}
		
		
		.alert {
			color: red;
		}
		
	</style>
</head>
<body>
	<div class="loading">
		<?php if ($hasError) : ?>
			<div class="alert"><?= Yii::app()->user->getFlash('error') ?></div>
			<br />
			<p><a href="<?= $redirectUrl ?>"><?= Yii::t("standard", "Click here if you are not redirected automatically") ?></a></p>
		<?php endif; ?>
	</div>
</body>
</html>


<script type="text/javascript">
 	window.onload = function() {
 	 	<?php if ($hasError) : ?>
 	 		setTimeout(function() {window.location.replace('<?= $redirectUrl ?>'); },5000);
 	 	<?php else : ?>
 	 		window.location.replace('<?= $redirectUrl ?>');
 	 	<?php endif;?>
 	}
</script>