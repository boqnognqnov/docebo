<p align="left"></p>
<p>Dear customer,</p>
Thank you for ordering from us. <br />
The following email is a summary of your order. Please use this as your proof of purchase.<br />
<hr />
<font style="color:#003D6B; font-size: 18px;">Order Summary</font>
<hr />
Order Number: <?=$transactionModel->id_trans ?><br />
Total: <?= $total ?> <?= $transactionModel->payment_currency ?><br />
<br />
Billed to: <?= $userModel->firstname . ' ' . $userModel->lastname ?><br />
<br />
Order details:<br />
------------------------------------------------------------------------<br />
<?php foreach ($transactionModel->transaction_items as $transaction_item) : ?>
	<?= $transaction_item->name . " ($transaction_item->price $transactionModel->payment_currency)" ?><br>
<?php endforeach; ?>        
------------------------------------------------------------------------<br />
Total:  <?= $total  . $transactionModel->payment_currency ?><br>


<br />

