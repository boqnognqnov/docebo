

<div class='checkout-wrapper'>


    <!-- HEADER PART -->
    <div class='row-fluid'>
        <div class='span7'>
            <div class='span2 sprite-checkout cart'></div>
            <div class='span10'>
                <h1 class='checkout-header'><?= Yii::t('cart', 'The transaction failed') ?></h1>
            </div>
        </div>

        <div class='span5'>
        </div>
		<div class="clearfix"></div>
    </div>

    <br>


    <div class='row-fluid'>
        <div class='span6'>
            <div class='span3'>
								<div class='cart-checkout-failed'>
									<i class="fa fa-close"></i>
								</div>
            </div>

            <div class='span9 thank-you-text' style='font-size: 1.6em;'>
							<?=Yii::t('cart','You have not been added to any courses');?>

            <br>
            <br>
            <a class='btn docebo nice-btn rounded blue' href="<?=Docebo::createLmsUrl('site/index')?>"> <?=Yii::t('cart','Back to My Dashboard');?></a>

            </div>

        </div>

        <!-- Course portlets of the courses just bought -->
        <div class='span6'>
            <?php if (!empty($courses)) : ?>
                    <?php
                    foreach ( $courses as $course ) {
                        echo"<div class='span6' style='margin-left: 0px; text-align: center;'>";
                        $portlet = $this->widget('common.widgets.CoursePortlet', array(
                            'courseModel' => $course,
                            'gridType' => CoursesGrid::$GRIDTYPE_INTERNAL_MYCOURSES,
                        ));
                        echo"</div>";
                    }
                    ?>
            <?php else : ?>
                <div class='alert alert-error text-center'>
                    <?=Yii::t('cart','There are no courses associated with this transaction');?>
                </div>
            <?php endif; ?>
        </div>


    </div>


</div>



<script type="text/javascript">
/*<![CDATA[*/

	// Ajax call to get the course details when hovering over a course box
	var event = 'hover touchstart';

	$(document).on(event, '.tile.course', function(e){
		if (e.type == 'mouseenter' || e.type == 'touchstart')
		{
			var $this = $(this);
			var courseInfo = $this.find('.course-hover-details');
			if (courseInfo.hasClass('empty'))
			{
				courseInfo.load($(this).attr('data-details-url'), function(){
					courseInfo.removeClass('empty');
					$(document).controls();
				});
			}
		}
	});


/*]]>*/
</script>
