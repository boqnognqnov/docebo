<?php
/* @var $transaction_success bool */
?>
<style>
<!--

-->
</style>



<div class='checkout-wrapper'>


    <!-- HEADER PART -->
    <div class='row-fluid'>
        <div class='span7'>
            <div class='span2 sprite-checkout cart'></div>
            <div class='span10'>
                <h1 class='checkout-header'><?= $transaction_success ? Yii::t('cart', 'Thank you') : Yii::t('cart', 'The transaction was declined') ?></h1>
            </div>
        </div>

        <div class='span5'>
        </div>
		<div class="clearfix"></div>
    </div>

    <br>


    <div class='row-fluid'>
        <div class='span6'>

            <?php if ($transaction_success) : ?>

            <div class='span3'>
                <div class='sprite-checkout checkmark'></div>
            </div>

            <div class='span9 thank-you-text' style='font-size: 1.6em;'>
                <?=Yii::t('cart','Your transaction <strong>has been <br>successfully completed</strong> and your new <br>courses have been added to your <br>E-Learning Platform');?>
                <br>
                <br>
                <a class='btn docebo nice-btn rounded blue' href="<?=Docebo::createLmsUrl('site/index')?>"> <?=Yii::t('cart','Back to My Dashboard');?></a>
            </div>

            <?php else: ?>

                <div class="span12">
                    <?=Yii::t('cart', 'The transaction could not be completed. Please check your funding source and try again.')?>
                    <br>
                    <br>
                    <a class='btn docebo nice-btn rounded blue' href="<?=Docebo::createLmsUrl('site/index')?>"> <?=Yii::t('cart','Back to My Dashboard');?></a>
                </div>

            <?php endif; ?>

        </div>

        <!-- Course portlets of the courses just bought -->
        <div class='span6'>
            <?php if (!empty($purchased_items)) : ?>
                    <?php
                    foreach ( $purchased_items as $item ) {
                        echo"<div class='span6' style='margin-left: 0px; text-align: center;'>";
	                    if($item instanceof LearningCourse){
                        $portlet = $this->widget('common.widgets.CoursePortlet', array(
			                    'courseModel' => $item,
			                    'courseDetailUrl' => Yii::app()->createUrl('course/axDetails',array('id' => $item->idCourse)),
                            'gridType' => CoursesGrid::$GRIDTYPE_INTERNAL_MYCOURSES,
                        ));
	                    }elseif($item instanceof LearningCoursepath){
		                    $portlet = $this->widget('common.widgets.CoursepathPortlet', array(
			                    'coursepathModel' => $item,
			                    'coursepathDetailUrl' => Yii::app()->createUrl('coursepath/details',array('id_path' => $item->id_path)),
			                    'gridType' => CoursesGrid::$GRIDTYPE_INTERNAL_MYCOURSES,
		                    ));
	                    }
                        echo"</div>";
                    }
                    ?>
            <?php else : ?>
                <div class='alert alert-error text-center'>
                    <?=Yii::t('cart','There are no courses associated with this transaction');?>
                </div>
            <?php endif; ?>
        </div>


    </div>


</div>



<script type="text/javascript">
/*<![CDATA[*/

	// Ajax call to get the course details when hovering over a course box
	var event = 'hover touchstart';

	$(document).on(event, '.tile.course', function(e){
		if (e.type == 'mouseenter' || e.type == 'touchstart')
		{
			var $this = $(this);
			var courseInfo = $this.find('.course-hover-details');
			if (courseInfo.hasClass('empty'))
			{
				courseInfo.load($(this).attr('data-details-url'), function(){
					courseInfo.removeClass('empty');
					$(document).controls();
				});
			}
		}
	});


/*]]>*/
</script>
