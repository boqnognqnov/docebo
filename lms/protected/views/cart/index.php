<?php
/**
 *    Note: Using jQuery.form. See: http://www.malsup.com/jquery/form/
 *
 */

$this->breadcrumbs = array(
	Yii::t('standard', 'Catalog') => Docebo::createAbsoluteLmsUrl('site/index', array('opt' => 'catalog')),
	Yii::t('catalogue', '_SHOPPING_CART'),
);



$idUser = Yii::app()->user->id;
$cart = Yii::app()->shoppingCart;
/* @var $positions CourseCartPosition[] */
$positions = $cart->getPositions();
$positionsCount = count($positions);
$current_coupon = $cart->getCoupon();
$currencySymbol = Settings::get('currency_symbol');

// List of coutries for dropdown list
$countriesListData = CHtml::listData($countries, 'iso_code_country', 'name_country');

if ($binfo == null) {
    $countriesListData = array_merge(array('' => Yii::t('standard', '_COUNTRY')), $countriesListData);
}

// TOS Url
$tosUrl = $this->createUrl("site/axGetPrivacyTos", array("language" => Yii::app()->getLanguage()));

// Edit User Bill Info URL
$editBillInfoUrl = $this->createUrl("user/axEditBillingInfo", array("idUser" => $idUser));

$continueShoppingUrl = $this->createUrl("site/index", array("opt" => "catalog"));

echo CHtml::beginForm('','POST', array('id' => 'checkout-form'));

// Add a hidden field to indicate if we have to SAVE Billing information on the fly
echo CHtml::hiddenField("save_binfo", (($binfo == null) || !$binfo->bill_city || !$binfo->bill_address1) ? 1 : 0, array("id" => "save-binfo"));

?>
<style>
<!--

.co-input-red {
    border: 1px solid red;
}

-->
</style>


<?php
//Stripe stuff
if (Payment::isMethodEnabled(Payment::$PAYMENT_METHOD_STRIPE)):
?>

<script src="https://checkout.stripe.com/checkout.js"></script>

<script>

    var handler = StripeCheckout.configure({
        key: '<?= CartController::retrieveStripeSandbox() ? CartController::retrieveStripePublicKeyTest() : CartController::retrieveStripePublicKey() ?>',
        image: '',
        locale: '<?= Stripe::getCurrentLocale() ?>',
			<?php
			//See some references:
			// - https://stripe.com/docs/checkout
			// - https://stripe.com/docs/alipay
			if (CartController::retrieveStripeAlipayEnabled()):
			?>
			alipay: true, //NOTE: possible values are true, false, "auto" (BEWARE: "true" and "false" as string may cause problems!!)
			<?php
			endif;
			?>
        token: function(token, args) {
            $.post
            (
                '<?= $this->createUrl('axStripeCharge') ?>',
                {
                    stripeToken: token.id,
                    amount: getAmountWithDiscount(),
                    coupon_code: $('#coupon_code')[0].value,
                    /*
                    billing_address_city: args.billing_address_city,
                    billing_address_country: args.billing_address_country,
                    billing_address_country_code: args.billing_address_country_code,
                    billing_address_line1: args.billing_address_line1,
                    billing_address_zip: args.billing_address_zip,
                    billing_name: args.billing_name,
                    */

                    billing_address_city: $('#binfo-form #city')[0].value,
                    billing_address_country: $('#binfo-form #bill-country-code option:selected')[0].innerHTML,
                    billing_address_country_code: $('#binfo-form #bill-country-code')[0].value,
                    billing_address_line1: $('#binfo-form #address1')[0].value,
                    billing_address_line2: $('#binfo-form #address2')[0].value,
                    billing_address_zip: $('#binfo-form #zip')[0].value,
                    billing_name: $('#binfo-form #name')[0].innerText,
                    billing_email: $('#binfo-form #email')[0].innerText,
                    billing_company: $('#binfo-form #company-name')[0].value,
                    billing_vat: $('#binfo-form #vat-number')[0].value,
                    billing_state: $('#binfo-form #state')[0].value,
					receipt_email: token.email
                },
                function(response)
                {
                    /*
                    document.write(
                        response +
                        '<p>Total with discount: ' + $('#cartTotal').text() + '</p>' +
                        '<p>---</p>' +
                        '<p>Billing Address City: ' + args.billing_address_city + '</p>' +
                        '<p>Billing Address Country: ' + args.billing_address_country + '</p>' +
                        '<p>Billing Address Line 1: ' + args.billing_address_line1 + '</p>' +
                        '<p>Billing Address State: ' + args.billing_address_state + '</p>' +
                        '<p>Billing Address ZIP: ' + args.billing_address_zip + '</p>' +
                        '<p>Billing Name: ' + args.billing_name + '</p>' +

                        '<a href="index.php?r=site/index">Back</a>'
                    );
                    */
                    
                    var urlParams = (response != '' ? '&success=0' : '&success=1');
                    window.location.replace('index.php?r=cart/stripeFinished' + urlParams);
                }
            );
        }
    });

    // Close Checkout on page navigation
    $(window).on('popstate', function() {
        handler.close();
    });
</script>

<?php
endif;
//end stripe part
?>

<div class="checkout-wrapper">

    <!-- HEADER PART -->
    <div class="row-fluid">
        <div class="span7">
            <div class="span2 sprite-checkout cart"></div>
            <div class="span10">
                <h1 class="checkout-header"><?= Yii::t('cart', 'Complete and confirm your order') ?></h1>
            </div>
        </div>
        <div class="span5"></div>
    </div>
    <br>

    <div class="row-fluid">

        <!-- BILLING INFO -->

        <div class="span6 billing-cart-container">

            <h5><?= Yii::t('billing', 'Confirm order') ?></h5>

            <!-- CART CONTENT AND manipulation -->

            <table class="cart-positions table">
                <?php
                    unset(Yii::app()->session['shoppingCartItems']);
                    $shoppingCartItems = [];
                ?>
                <?php foreach ($positions as $pos_id => $position ) { ?>
                    <?php
                        if ($position instanceof LearningCourse)
                            $shoppingCartItems[] = array('id' => $position->idCourse, 'type' => 'course');
                        elseif ($position instanceof LearningCoursepath)
                            $shoppingCartItems[] = array('id' => $position->id_path, 'type' => 'learning_path');
                        $cartItemImage = $position->getImage();
                    ?>
                    <tr id="<?= $position->getCartItemId() ?>" class="cart-position">
                        <td class="span2">
                            <?php echo CHtml::image($cartItemImage, "Logo", array("class" => "course-logo")); ?>
                        </td>
                        <td class="span7 name">
	                        <?=$position->getPositionName()?>
	                        <? if($position instanceof CoursepathCartPosition): ?>
								<?php $this->renderPartial('edit/lp_details', array('pos_id' => $pos_id, 'position' => $position)); ?>
							<? elseif($position instanceof CourseseatsCartPosition): ?>
								<?php $this->renderPartial('edit/seats_edit', array('pos_id' => $pos_id, 'position' => $position)); ?>
							<? elseif($position instanceof CourseCartPosition) : ?>
								<? if ($position->getSession()) : ?>
									<br><span style="color: #000; font-weight: normal"><?=Yii::t('classroom', 'Session').': '.$position->getSession()->name?></span>
								<?endif; ?>
		                    <? endif; ?>
                        </td>
                        <td class="span2 sum"><?= $currencySymbol ?>&nbsp;<?= number_format($position->getSumPrice(),2) ?></td>
                        <td class="span1"><a title="<?= Yii::t('cart', 'Remove this item from the cart') ?>" data-position-id="<?= $position->getCartItemId() ?>" href="#" class="sprite-checkout remove remove-position"></a></td>
                    </tr>
                <?php }
                    Yii::app()->session['shoppingCartItems'] = $shoppingCartItems;
                ?>

                <tr id="empty-cart" style="display: none;"><td colspan="4" class="span12"><div class="text-center alert alert-error"><?= Yii::t('billing', 'Cart is empty') ?></div></td></tr>
            </table>

            <table class="cart-totals table">
                <tr style="display: none;">
                    <td class="span2 total-label-column">
                        <?= Yii::t('coupon', 'Subtotal') ?>
                    </td>
                    <td class="span7"></td>
                    <td class="span2 total-value-column sum">
                        <span class="striked" id="cartDiscountInitial"><?=$currencySymbol ?>&nbsp;<?= number_format($cart->getCost(),2) ?></span>
                    </td>
                    <td class="span1"></td>
                </tr>
                <tr style="display: none;">
                    <td class="span2 total-label-column">
                        <?= Yii::t('billing', 'Coupon') ?>
                    </td>
                    <td class="span7">
                        <span id="cartDiscountDescription"></span>
                    </td>
                    <td class="span2 total-value-column sum">
                        <span id="cartDiscountAmount"></span>
                    </td>
                    <td class="span1"></td>
                </tr>
                <tr>
                    <td class="span2 total-label-column">
                        <?= Yii::t('standard','_TOTAL', array('{count}'=>''))?>
                    </td>
                    <td class="span7"></td>
                    <td class="span2 total-value-column sum">
                        <span id="cartTotal"><?=$currencySymbol ?>&nbsp;<?= number_format($cart->getCost(),2) ?></span>
                        <div class="tax-not-included" style="display:none;">
                            <span>(<?= Yii::t('ecommerce', 'Tax not included') ?>)</span>
                        </div>
                    </td>
                    <td class="span1"></td>
                </tr>
            </table>

            <div class="cart-coupon-form row-fluid" style="display: <?= ($cart->getCost() > 0.00) ? 'block' : 'none' ?>">
                <div class="span2">
                    <label for=""><?= Yii::t('billing', 'Coupon') ?>?</label>
                </div>
                <div class="span10">
                    <a class="sprite-checkout remove pull-right" href="#" style="display: none;"></a>
                    <button class="pull-right btn btn-docebo green big span3"><?= Yii::t('billing', 'Apply') ?></button>
                    <input type="text" id="coupon_code" name="coupon_code" class="span9" placeholder="<?= Yii::t('billing', 'Insert your coupon') ?>" value="<?= $current_coupon ?>"/>
                </div>
            </div>

            <div style="display: none;" class="cart-error-container alert alert-error">
                <?= Yii::t('billing', 'Cart is empty') ?>
            </div>

            <?php if (Yii::app()->user->hasFlash('error')) : ?>
                <div id="form-feedback" class="alert alert-error text-center"><?= Yii::app()->user->getFlash('error'); ?></div>
            <?php else : ?>
                <div style="display:none;" id="form-feedback" class="alert alert-error text-center"></div>
            <?php endif; ?>

            <div class="payment-method <?php echo Yii::app()->getLanguage(); ?>" style="display: <?= ($cart->getCost() > 0.00) ? 'block' : 'none' ?>">
                <h5><?= Yii::t('cart', 'Payment method') ?></h5>

                <div class="span12">
	                <?php
					$at_least_one_enabled = false;

	                // Allow plugins to override the displayed payment methods
	                $event = new DEvent($this, array('shoppingCart' => Yii::app()->shoppingCart));
	                Yii::app()->event->raise('ShoppingCartPaymentIcons', $event);

					if($event->handled)
						$at_least_one_enabled = true;

					if(isset($event->params['at_least_one_enabled']))
						$at_least_one_enabled = $event->params['at_least_one_enabled'];

	                if(!$event->handled) :
	                ?>
                    <?php if (Payment::isMethodEnabled(Payment::$PAYMENT_METHOD_PAYPAL) && Settings::get('paypal_mail') != '') : $at_least_one_enabled = true; ?>
                    <div class="span6">
                        <?= CHtml::label(
                            CHtml::radioButton('payment_method', true, array('value' => Payment::$PAYMENT_METHOD_PAYPAL, 'id'=>'payment_method_paypal')) . CHtml::tag('span',array('class'=>'sprite-checkout paypal'), '&nbsp;'),
                            'payment_method_paypal',
                            array('class'=>'radio')
                        ) ?>
                    </div>
                    <?php endif; ?>

					<?php if (Payment::isMethodEnabled(Payment::$PAYMENT_METHOD_PAYPAL_ADAPTIVE)) : $at_least_one_enabled = true; ?>
						<div class="span6">
							<?= CHtml::label(
								CHtml::radioButton('payment_method', true, array('value' => Payment::$PAYMENT_METHOD_PAYPAL_ADAPTIVE, 'id'=>'payment_method_paypal_adaptive')) . CHtml::tag('span',array('class'=>'sprite-checkout paypal')),
								'payment_method_',
								array('class'=>'radio')
							) ?>
						</div>
					<?php endif; ?>

                    <?php if (Payment::isMethodEnabled(Payment::$PAYMENT_METHOD_AUTHORIZE) && Settings::get('authorize_loginid') != '' && Settings::get('authorize_key') != '' && Settings::get('authorize_hash') != '') : $at_least_one_enabled = true; ?>
                    <div class="span6">
                        <?= CHtml::label(
                            CHtml::radioButton('payment_method', true, array('value' => Payment::$PAYMENT_METHOD_AUTHORIZE, 'id'=>'payment_method_authorize')) . CHtml::tag('span',array('class'=>'sprite-checkout authorizedotnet'), '&nbsp;'),
                            'payment_method_paypal_adaptive',
                            array('class'=>'radio')
                        ) ?>
                    </div>
                    <?php endif; ?>

					<?php if (Payment::isMethodEnabled(Payment::$PAYMENT_METHOD_ADYEN) && Settings::get('adyen_merchant_account') != '' && Settings::get('adyen_hmac_key') != '' && Settings::get('adyen_skin_code') != '') : $at_least_one_enabled = true; ?>
					<div class="span6">
						<?= CHtml::label(
							CHtml::radioButton('payment_method', true, array('value' => Payment::$PAYMENT_METHOD_ADYEN, 'id'=>'payment_method_adyen')) . CHtml::tag('span',array('class'=>'sprite-checkout adyen'), '&nbsp;'),
							'payment_method_adyen',
							array('class'=>'radio')
						) ?>
					</div>
					<?php endif; ?>

					<?php if (Payment::isMethodEnabled(Payment::$PAYMENT_METHOD_STRIPE) && Settings::get('stripe_account_email') != '' && Settings::get('stripe_private_key') != '' && Settings::get('stripe_public_key') != '') : $at_least_one_enabled = true; ?>
						<div class="span6">
							<?= CHtml::label(
								CHtml::radioButton('payment_method', true, array('value' => Payment::$PAYMENT_METHOD_STRIPE, 'id' => 'payment_method_stripe')) . CHtml::tag('span', array('class' => 'sprite-checkout stripe'), '&nbsp;'),
								'payment_method_stripe',
								array('class' => 'radio')
							) ?>
						</div>
					<?php endif; ?>

					<?php if (Payment::isMethodEnabled(Payment::$PAYMENT_METHOD_PAYFLOW)) : $at_least_one_enabled = true; ?>
						<div class="span12">
							<?= CHtml::label(
								CHtml::radioButton('payment_method', true, array('value' => Payment::$PAYMENT_METHOD_PAYFLOW, 'id'=>'payment_method_payflow')) . CHtml::tag('span',array('class'=>'sprite-checkout paypal')),
								'payment_method_payflow',
								array('class'=>'radio')
							) ?>
						</div>
					<?php endif; ?>

                    <?php if (Payment::isMethodEnabled(Payment::$PAYMENT_METHOD_CYBERSOURCE)) : $at_least_one_enabled = true; ?>
						<div class="span6">
							<?= CHtml::label(
								CHtml::radioButton('payment_method', true, array('value' => Payment::$PAYMENT_METHOD_CYBERSOURCE, 'id'=>'payment_method_cybersource')) . CHtml::tag('span',array('class'=>'sprite-checkout cybersource')),
								'payment_method_cybersource',
								array('class'=>'radio')
							) ?>
						</div>
					<?php endif; ?>

					<?php if(!$at_least_one_enabled) : ?>
						<div class="span12">
							<span class="errorMessage">
								<?= Yii::t('ecommerce', 'No payment gateway found, please contact the administrator to solve the issue'); ?>
							</span>
						</div>
					<?php endif; ?>

					<?php endif; ?>
                </div>
            </div>

        </div>

        <div class="span6">

            <div class="billing-info" style="display: <?= ($cart->getCost() > 0.00) ? 'block' : 'none' ?>">
                <a href="<?= $editBillInfoUrl ?>" class="classic-link open-dialog pull-right" rel="dialog-billing-info"><?= Yii::t('standard','_MOD') ?></a>
                <h5><?= Yii::t('billing', '_BILLING_INFORMATION') ?></h5>

                <!-- SHOW Billing INFO -->
                <div id="binfo-show-only" style="display: none;" class="form-horizontal">
                    <br>

                    <div class="row-fluid">
                        <label class="span4"  for="name"><?=Yii::t('standard','_FULLNAME'); ?></label>
                        <span class="span8 data" id="name"><?= $uinfo->firstname . " " . $uinfo->lastname ?></span>
                    </div>

                    <div class="row-fluid">
                        <label class="span4" for="email"><?=Yii::t('standard','_EMAIL'); ?></label>
                        <span class="span8 data" id="email"><?= $uinfo->email ?></span>
                    </div>

                    <div class="row-fluid">
                        <label class="span4"  for="company-name"><?=Yii::t('billing','_COMPANY_NAME'); ?></label>
                        <span class="span8 data" id="company-name"><?= $binfo->bill_company_name ?></span>
                    </div>

                    <div class="row-fluid">
                        <label class="span4" for="vat-number"><?=Yii::t('billing','_VAT'); ?></label>
                        <span class="span8 data" id="vat-number"><?= $binfo->bill_vat_number ?></span>
                    </div>


                    <div class="row-fluid">
                        <label class="span4" for="address1"><?=Yii::t('standard','Address 1'); ?></label>
                        <span class="span8 data" id="address1"><?= $binfo->bill_address1?></span>
                    </div>

                    <div class="row-fluid">
                        <label class="span4" for="address2"><?=Yii::t('standard','Address 2'); ?></label>
                        <span class="span8 data" id="address2"><?= $binfo->bill_address2?></span>
                    </div>


                    <div class="row-fluid">
                        <label class="span4" for="city"><?=Yii::t('classroom','_CITY'); ?></label>
                        <span class="span8 data" id="city"><?= $binfo->bill_city ?></span>
                    </div>

                    <div class="row-fluid">
                        <label class="span4" for="state"><?=Yii::t('classroom','_STATE'); ?></label>
                        <span class="span8 data" id="state"><?= $binfo->bill_state ?></span>
                    </div>

                    <div class="row-fluid">
                        <label class="span4" for="zip"><?=Yii::t('classroom','_ZIP_CODE'); ?></label>
                        <span class="span8 data" id="zip"><?= $binfo->bill_zip ?></span>
                    </div>


                    <div class="row-fluid">
                        <label class="span4" for="country"><?=Yii::t('standard','_COUNTRY'); ?></label>
                        <span class="span8 data" id="country"><?= $countriesListData[$binfo->bill_country_code] ?></span>
                    </div>

                </div>
            </div>




            <!-- FORM -->
            <div id="binfo-form" style="display: none;" class='form-horizontal'>

                <div class="row-fluid">
                    <label class="span4"  for="name"><?=Yii::t('standard','_FULLNAME'); ?></label>
                    <span class="span8 data" id="name"><?= $uinfo->firstname . " " . $uinfo->lastname ?></span>
                </div>

                <div class="row-fluid">
                    <label class="span4" for="email"><?=Yii::t('standard','_EMAIL'); ?></label>
                    <span class="span8 data" id="email"><?= $uinfo->email ?></span>
                </div>

                <div class="row-fluid">
                    <label class="span4"  for="company-name"><?=Yii::t('billing','_COMPANY_NAME'); ?></label>
                    <input class="span8 data" id="company-name" name='bill_company_name' value="<?= $binfo->bill_company_name ?>">
                </div>

                <div class="row-fluid">
                    <label class="span4" for="vat-number"><?=Yii::t('billing','_VAT'); ?></label>
                    <input class="span8 data" id="vat-number" name="bill_vat_number" value="<?= $binfo->bill_vat_number ?>">
                </div>

                <div class="row-fluid">
                    <label class="span4" for="address1"><?=Yii::t('standard','Address 1'); ?></label>
                    <input class="span8 data" id="address1" name="bill_address1" value="<?= $binfo->bill_address1?>">
                </div>

                <div class="row-fluid">
                    <label class="span4" for="address2"><?=Yii::t('standard','Address 2'); ?></label>
                    <input class="span8 data" id="address2" name="bill_address2" value="<?= $binfo->bill_address2?>">
                </div>


                <div class="row-fluid">
                    <label class="span4" for="city"><?=Yii::t('classroom','_CITY'); ?></label>
                    <input class="span8 data" id="city" name="bill_city" value="<?= $binfo->bill_city ?>">
                </div>

                <div class="row-fluid">
                    <label class="span4" for="state"><?=Yii::t('classroom','_STATE'); ?></label>
                    <input class="span8 data" id="state" name="bill_state" value="<?= $binfo->bill_state ?>">
                </div>

                <div class="row-fluid">
                    <label class="span4" for="zip"><?=Yii::t('classroom','_ZIP_CODE'); ?></label>
                    <input class="span8 data" id="zip" name="bill_zip" value="<?= $binfo->bill_zip ?>">
                </div>


                <div class="row-fluid">
                    <label class="span4" for="country"><?=Yii::t('standard','_COUNTRY'); ?> </label>
                    <div style="margin-left: 0px;" class="span8 data">
                        <?php
                        echo CHtml::dropDownList('bill_country_code', $binfo->bill_country_code, $countriesListData, array('id' => 'bill-country-code'));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- FOOTER PART -->
    <div class="cart-footer">

        <a id="buy-now<?= (!$at_least_one_enabled && $cart->getCost() > 0.00 ? '-disabled' : '') ?>" class="btn btn-docebo green btn-buy-now"<?= (!$at_least_one_enabled && $cart->getCost() > 0.00 ? ' disabled="disabled"' : '') ?>><?= ($cart->getCost() > 0.00) ? Yii::t('userlimit', 'Buy now') : Yii::t('standard','_CONFIRM') ?></a>

        <a href="<?=$continueShoppingUrl?>" class="btn btn-docebo black btn-continue"><?= Yii::t('cart', 'CONTINUE SHOPPING') ?></a>

		<?php if($cart->getCost() > 0.00): ?>
        <label class="checkbox">
            <input type="checkbox" name="cb_agree" id="cb-agree">&nbsp;
            <?=Yii::t('cart','I agree with the {link}Terms and Conditions of the service.{\link}',array(
                '{link}' => '<a href=' . $tosUrl . ' data-dialog-class="read-tos-modal" closeOnEscape="false" class="classic-link tos-link open-dialog" rel="read-tos-modal" href="#">',
                '{\link}' => '</a>')); ?>
        </label>
		<?php endif; ?>

    </div>

</div>

<?php echo CHtml::endForm() ?>

<!-- --- JS ------------------------------------------------------------------------------------------------------ -->

<script type="text/javascript">
/*<![CDATA[*/

// URL to Remove position from cart
var removeUrl = '<?= $removeUrl ?>';
var positionsCount = <?= $positionsCount  ?>;
var ID_USER = '<?= $idUser ?>';
var BINFO_AVAIL = <?= (($binfo != null) && $binfo->bill_city && $binfo->bill_address1) ? 'true' : 'false' ?>;console.log('BINFO_AVAIL: ');console.log(BINFO_AVAIL);
var inRedirect = false;
var freeCart = false;

function cartErrorShow(message) {
    $('.cart-error-container').html(message).show();
}
function cartErrorHide() {
    $('.cart-error-container').hide();
}

// Update visual status
function updateVisualStatus() {
    if (positionsCount <= 0) {
        $("#empty-cart").show();
        $("#buy-now").attr("disabled","").addClass("disabled");
        $('.cart-totals tr').hide();
        $('.cart-coupon-form').hide();
		$('#form-feedback').hide();
    }
}


// Makes an Ajax call
function xhr_removePosition( id ) {

    // URL
    var url = removeUrl;

	var xhr = $.ajax({
		url: url,
		type: 'post',  // !! don't change
		dataType: 'json',
		data: {id: id},
		beforeSend: function() { $('.ajaxloader').show() }
	})
		.always(function(){
			$('.ajaxloader').hide();
		})
		.fail(function(jqXHR, textStatus, errorThrown){
			if(console) {
				console.log(jqXHR);
				console.log(errorThrown);
			}
			//alert(textStatus);
		})
		.success(function(jqXHR){
			if(jqXHR.payment_gateway != '')
			{
				$('.payment-method').html(jqXHR.payment_gateway);
				$('input').styler();
			}
		});
	return xhr;
}



// Basic form fields validation callback, hooked to ajaxForm submition
// @todo Improve
function validateForm(formData, jqForm, options) {

    // Disabling the button
    $("#buy-now").attr("disabled","").addClass("disabled");

    // Hide previous alerts if any
    $('#form-feedback').hide();

	<?php if($cart->getCost() > 0.00): ?>
    // Check "Agree"
    $ok = $("#cb-agree").attr("checked") == "checked";
    if ($('.cart-footer label.checkbox').is(':visible') && !$ok ) {
        $error_feedback = '<?= addslashes(Yii::t('cart','In order to continue, please read and agree with the Terms and Conditions')) ?>';
        $("#cb-agree").focus();
        $('#form-feedback').text($error_feedback).show();
        return false;
    }

    // Standard error message text
    $error_feedback = '<?=addslashes(Yii::t('cart','In order to continue, please provide the requested billing information')); ?>';

    ok = true;

    // Billing info validation
    elementsToValidate = ['address1', 'city'];
    $('#checkout-form label').css('color', '');
    for (var i=0; i < elementsToValidate.length; i++) {
        if (BINFO_AVAIL) { // spans
            el = $("#binfo-show-only #"+elementsToValidate[i]);
            tmpText = $.trim(el.text());
            if (tmpText.length <= 0) {
                ok = false;
                $('[for="'+elementsToValidate[i]+'"]').css('color', 'red');
            }
        }
        else { // inputs
            el = $("#binfo-form #"+elementsToValidate[i]);
            tmpText = $.trim(el.val());
            if (tmpText.length <= 0) {
                ok = false;
                el.addClass("co-input-red");
            }
            else {
                el.removeClass("co-input-red");
            }
        }
    }

    // Check country: different checks
    if (BINFO_AVAIL) {
        tmpText = $.trim($("#binfo-show-only #country").text());
        if (tmpText.length <= 0) {
            ok = false;
            $('[for="country"]').css('color', 'red');
        }
    }
    else {
        el = $("#binfo-form #bill-country-code");
        tmpText = el.val();
        if (tmpText.length <= 0) {
            ok = false;
            el.addClass("co-input-red");
        }
        else {
            el.removeClass("co-input-red");
        }
    }

    if ($('#binfo-form').is(':visible') && !ok) {
        $('#form-feedback').text($error_feedback).show();
        return false;
    }
	<?php endif; ?>

    return true;
}


// ajaxForm 'success' callback
// Receives a redirection form
function formSubmitSuccess(res) {

    // Show some dialog ?  ("Redirecting to ... Please wait...")

    if ( res.success === true ) {
        if(res.url != '')
        	window.location.replace(res.url);
        else
        {
	        form = res.form;
	        $('body').append(form);
	        $('.ajaxloader').show();
	        $("#buy-now").attr("disabled","").addClass("disabled");
	        inRedirect = true;
	        $("#payment_redirect_form").submit();
        }
    }
    else {
        alert(res.message);
    }

}

function formSumbitError() {
    //alert('Form Submission error occured');
}

function updateTotals() {
    cartErrorHide();
    var $discountInput = $('#coupon_code');
    var discountCode = '';
    if(positionsCount > 0)
        discountCode = $discountInput.val();

    $.post('<?= $this->createUrl('axApplyDiscount') ?>', {code:discountCode}, function(response) {
        if (response.error.length) {
            cartErrorShow(response.error);
        }
        else if(typeof response.total == "number") {
            // Was a couponp applied ?
            if(response.code) {

                // Update coupon description
                var discountDescription = response.code;
                if($.isArray(response.valid_courses) && response.valid_courses.length > 0) {
                    discountDescription += '<br/><span><?= Yii::t('coupon', 'Valid course(s)')?>:</span>';
                    $.each(response.valid_courses, function(index, value) {
                        discountDescription += '<br/>'+value;
                    });
                }
                $('#cartDiscountDescription').html(discountDescription);

                // Update subtotal, discount amount and final total
                $('#cartDiscountInitial').html('<?=$currencySymbol?>&nbsp;' + parseFloat(response.total).toFixed(2));
                $('#cartDiscountAmount').html('<?=$currencySymbol?>&nbsp;' + parseFloat(response.discount).toFixed(2));
                $('#cartTotal').html('<?=$currencySymbol?>&nbsp;' + parseFloat(response.discounted).toFixed(2));

                // Show all total row
                $('.cart-totals tr').show();

                // Disable coupon editing (just delete icon)
                $('.cart-coupon-form').find('button').hide();
                $('.cart-coupon-form').find('.remove').show();
                $discountInput.attr('readonly', true);
				if(response.discounted == 0)
					freeCart = true;
            } else {
                $('.cart-totals tr:nth-child(1),.cart-totals tr:nth-child(2)').hide();
                $('.cart-coupon-form').find('.remove').hide();
                $('.cart-coupon-form').find('button').show();
                $discountInput.attr('readonly', false);
                $discountInput.val('');
                $('#cartTotal').html('<?=$currencySymbol?>&nbsp;' + parseFloat(response.total).toFixed(2));

				// Hide a few elements if total is zero
				if(response.total == 0) {
					$('.payment-method').hide();
					$('#buy-now').html(<?= json_encode(Yii::t('standard','_CONFIRM')) ?>);
					$('.cart-footer label.checkbox').hide();
					$('.billing-info').hide();
					if (!BINFO_AVAIL)
						$('#binfo-form').hide();
					$('.cart-coupon-form').hide();
					freeCart = true;
				} else {
					$('.payment-method').show();
					$('#buy-now').html(<?= json_encode(Yii::t('userlimit', 'Buy now')) ?>);
					$('.cart-footer label.checkbox').show();
					$('.billing-info').show();
					if (!BINFO_AVAIL)
						$('#binfo-form').show();
					$('.cart-coupon-form').show();
				}
            }
        }
    }, 'json');
}

function updateBillInfo(data) {
    $("#company-name").text(data.bill_company_name);
    $("#vat-number").text(data.bill_vat_number);
    $("#address1").text(data.bill_address1);
    $("#address2").text(data.bill_address2);
    $("#city").text(data.bill_city);
    $("#country").text(data.extra_country_name);
    $('#checkout-form .billing-info label').css('color', '');
    $("#state").text(data.bill_state);
    $("#zip").text(data.bill_zip);
}

// //////////////////////////////////////////////////////////////////////////////////////

function getAmountWithDiscount()
{
    var strCost = $('#cartTotal')[0].innerHTML;
    while (isNaN(parseInt(strCost.substr(0,1)))) strCost = strCost.substr(1);

    return parseInt(strCost.replace(',', '').replace('.', ''));
}

// Document ready
$(function() {

    /**
     * Coupon discounts
     * */
    $('.cart-coupon-form').find('button').click(function(e) {
        e.preventDefault();
        updateTotals();
    });

    $('.cart-coupon-form').find('.remove').live('click', function(e) {
        e.preventDefault();
        $('#coupon_code').val('');
        updateTotals();
    });

    // Depending on if billing information is available at the moment, show different parts
    if (BINFO_AVAIL) {
        $("#binfo-show-only").show();
    }
    else {
        $("#binfo-form").show();
    }

    // Add loader indicator
    $('body').append('<div class="ajaxloader" style="display: none;"></div>');

    updateVisualStatus();
    updateTotals();

    // BINDINGS
    $('#buy-now').on("click", function(e){
        e.preventDefault();
        if (inRedirect) {
            return false;
        }
        if (positionsCount <= 0) {
            return false;
        }

        var agreeChecked = false;
        if(freeCart)
        	agreeChecked = true;
       	else
       		agreeChecked = $('#cb-agree')[0].checked;

        if(agreeChecked)
        {
            if (freeCart == false && $('#payment_method_stripe').length > 0 && $('#payment_method_stripe')[0].checked) {

                //first check if billing fields are correctly filled
                $res  = validateForm();
                if ($res == false) {
                    $("#buy-now").removeAttr('disabled').removeClass('disabled'); //make sure to leave the button in its original clickable status
                    return false;
                }

                // Open Checkout with further options
                var email = '';
                if ($('#email')[0])
                    email = $('#email')[0].innerHTML;
                handler.open({
                    name: <?= json_encode(Yii::t('standard', 'Payment')) ?>,
                    description: <?= json_encode(Yii::t('standard', 'Credit card payment checkout')) ?>,
                    currency: "<?= strtolower(Yii::app()->shoppingCart->currency); ?>",
                    amount: getAmountWithDiscount(),
                    zipCode: false,//$('#checkout-form input[id="zip"]')[0].value,

                    billingAddress: false,//address,
	                //Shipping address was not needed and adding it raised a warning from stripe which lead to the payment modal not showing in IE.
//                    shippingAddress: false, //address,

                    email: email
                });
            }
            else
                $("#checkout-form").submit();
        }
        else
            alert(<?= json_encode(Yii::t('standard', 'Please accept Terms and Conditions')) ?>);
    });

    // If "REMOVE" has been clicked
    $(".remove-position").on("click", function(e){

        // Position Id (key)
        var id = $(this).data("position-id");
        e.preventDefault();

        // Make ajax call to remove the position (id) and handle result
        xhr_removePosition(id).done(function(res){
            if (res.success === true) {
                // Update screen data (count, feedback, et.)
                positionsCount = res.count;
                $('#'+id).remove();
                newCountText = positionsCount <= 0 ? '' : positionsCount;
                $('#cart-items-count').text(newCountText);

                updateVisualStatus();
                updateTotals();
            }
        });
    });

    // Using jQuery.form
    // See: http://www.malsup.com/jquery/form/
    $("#checkout-form").ajaxForm( {
        beforeSubmit: function() {
            $res  = validateForm();
            if($res==false) {
                $("#buy-now").removeAttr('disabled').removeClass('disabled');
            }
            return $res;
        },
        dataType: 'json',
        success: formSubmitSuccess,
        error: formSumbitError,
        type: 'POST',
        url: '<?=Yii::app()->getBaseUrl()?>/?r=cart/axCheckout'
    });

    $("#checkout-form").on("focus", function(){
        $('#form-feedback').hide();
    });

    //----------
    //first check for some graphical adjustments
    try {
        var totalWidth = $('#cartTotal').width();
        var taxTextWidth = $('.tax-not-included').width();
        var taxTextOffset = 0;
        if (totalWidth > 0) {
            taxTextOffset = (Math.floor(totalWidth / 2) - Math.floor(taxTextWidth / 2));
        }
        if (taxTextOffset < 0) {
            $('.tax-not-included').css('position', 'relative');
            $('.tax-not-included').css('left', Math.abs(taxTextOffset) + 'px');
        }
        if (taxTextOffset > 0) {
            $('.tax-not-included').css('position', 'relative');
            $('.tax-not-included').css('right', Math.abs(taxTextOffset) + 'px');
        }
    } catch(e) {
        //in case of error just ignore the above and go on
    }
    //then check for tax text displaying
    var checkStripeOrder = function() {
        var stripeOrderEnabled = <?= (Payment::isMethodEnabled(Payment::$PAYMENT_METHOD_STRIPE) && Settings::get('stripe_type') == 'advanced' ? 'true' : 'false') ?>;
        if (stripeOrderEnabled) {
            if ($('#payment_method_stripe').prop('checked')) {
                $('.tax-not-included').css('display', 'inline');
            } else {
                $('.tax-not-included').css('display', 'none');
            }
        } else {
            $('.tax-not-included').css('display', 'none');
        }
    };
    $('input[id^="payment_method_"]').on('change', function() {
        checkStripeOrder();
    });
    checkStripeOrder();
    //----------

    $(document).controls();

    function getBillingInfo(){
        // URL
        var url = '/lms/?r=user/axGetBillingInfo';

        var xhr = $.ajax({
            url: url,
            type: 'post',  // !! don't change
            dataType: 'json',
            data: {idUser: ID_USER},
            beforeSend: function() { $('.ajaxloader').show() }
        })
            .always(function(){
                $('.ajaxloader').hide();
            })
            .fail(function(jqXHR, textStatus, errorThrown){
                if(console) {
                    console.log(jqXHR);
                    console.log(errorThrown);
                    //alert(textStatus);
                }
            })
            .done(function(res){
                if (res.success === true) {
                    // Looks like we have successfully saved billing info
                    BINFO_AVAIL = true;

                    // Hide form and show on-screen data only
                    $("#binfo-show-only").show();
                    $("#binfo-form").hide();

                    $("#save-binfo").val(0);

                    updateBillInfo(res.data);
                }
            });
    }

	// Handle Auto-close Billing info dialog (put <a class='auto-close'></a> in the content)
	$(document).on("dialog2.content-update", '#dialog-billing-info', function(){
		var e = $(this);
		var autoClose = e.find("a.auto-close");
		if (autoClose.length > 0) {
			e.dialog2("close");
            getBillingInfo();
		}
	});

    $('input').styler();
});

/*]]>*/
</script>