<?php
/* @var $transaction_success bool */
?>
<div class='checkout-wrapper'>


	<!-- HEADER PART -->
	<div class='row-fluid'>
		<div class='span7'>
			<div class='span2 sprite-checkout cart'></div>
			<div class='span10'>
				<h1 class='checkout-header'><?= $transaction_success ? Yii::t('cart', 'Thank you') : Yii::t('cart', 'The transaction was declined') ?></h1>
			</div>
		</div>

		<div class='span5'>
		</div>
		<div class="clearfix"></div>
	</div>

	<br>


	<div class='row-fluid'>
		<div class='span6'>

			<?php if ($transaction_success) : ?>

				<div class='span3'>
					<div class='sprite-checkout checkmark'></div>
				</div>

				<div class='span9 thank-you-text' style='font-size: 1.6em;'>
					<?=Yii::t('cart','Your transaction <strong>has been <br>successfully completed</strong>');?>
					<br>
					<br>
					<a class='btn docebo nice-btn rounded blue' href="<?=Docebo::createAdminUrl('courseManagement/index')?>"> <?=Yii::t('cart','Go to Courses Management');?></a>
				</div>

			<?php else: ?>

				<div class="span12">
					<?=Yii::t('cart', 'The transaction could not be completed. Please check your funding source and try again.')?>
					<br>
					<br>
					<a class='btn docebo nice-btn rounded blue' href="<?=Docebo::createLmsUrl('site/index')?>"> <?=Yii::t('cart','Back to My Dashboard');?></a>
				</div>

			<?php endif; ?>

		</div>

		<!-- Right side block is empty -->
		<div class='span6'></div>

	</div>


</div>