<h5><?= Yii::t('cart', 'Payment method') ?></h5>

<div class="span12">
	<?php
	$at_least_one_enabled = false;

	// Allow plugins to override the displayed payment methods
	$event = new DEvent($this, array('shoppingCart' => Yii::app()->shoppingCart));
	Yii::app()->event->raise('ShoppingCartPaymentIcons', $event);

	if($event->handled)
		$at_least_one_enabled = true;

	if(isset($event->params['at_least_one_enabled']))
		$at_least_one_enabled = $event->params['at_least_one_enabled'];

	if(!$event->handled) :
	?>
	<?php if (Payment::isMethodEnabled(Payment::$PAYMENT_METHOD_PAYPAL) && Settings::get('paypal_mail') != '') : $at_least_one_enabled = true; ?>
	<div class="span6">
		<?= CHtml::label(
			CHtml::radioButton('payment_method', true, array('value' => Payment::$PAYMENT_METHOD_PAYPAL, 'id'=>'payment_method_paypal')) . CHtml::tag('span',array('class'=>'sprite-checkout paypal'), '&nbsp;'),
			'payment_method_paypal',
			array('class'=>'radio')
		) ?>
	</div>
	<?php endif; ?>

	<?php if (Payment::isMethodEnabled(Payment::$PAYMENT_METHOD_PAYPAL_ADAPTIVE)) : $at_least_one_enabled = true; ?>
		<div class="span6">
			<?= CHtml::label(
				CHtml::radioButton('payment_method', true, array('value' => Payment::$PAYMENT_METHOD_PAYPAL_ADAPTIVE, 'id'=>'payment_method_paypal_adaptive')) . CHtml::tag('span',array('class'=>'sprite-checkout paypal')),
				'payment_method_',
				array('class'=>'radio')
			) ?>
		</div>
	<?php endif; ?>

	<?php if (Payment::isMethodEnabled(Payment::$PAYMENT_METHOD_AUTHORIZE) && Settings::get('authorize_loginid') != '' && Settings::get('authorize_key') != '' && Settings::get('authorize_hash') != '') : $at_least_one_enabled = true; ?>
	<div class="span6">
		<?= CHtml::label(
			CHtml::radioButton('payment_method', true, array('value' => Payment::$PAYMENT_METHOD_AUTHORIZE, 'id'=>'payment_method_authorize')) . CHtml::tag('span',array('class'=>'sprite-checkout authorizedotnet'), '&nbsp;'),
			'payment_method_paypal_adaptive',
			array('class'=>'radio')
		) ?>
	</div>
	<?php endif; ?>

	<?php if (Payment::isMethodEnabled(Payment::$PAYMENT_METHOD_ADYEN) && Settings::get('adyen_merchant_account') != '' && Settings::get('adyen_hmac_key') != '' && Settings::get('adyen_skin_code') != '') : $at_least_one_enabled = true; ?>
	<div class="span6">
		<?= CHtml::label(
			CHtml::radioButton('payment_method', true, array('value' => Payment::$PAYMENT_METHOD_ADYEN, 'id'=>'payment_method_adyen')) . CHtml::tag('span',array('class'=>'sprite-checkout adyen'), '&nbsp;'),
			'payment_method_adyen',
			array('class'=>'radio')
		) ?>
	</div>
	<?php endif; ?>

	<?php if (Payment::isMethodEnabled(Payment::$PAYMENT_METHOD_CYBERSOURCE)) : $at_least_one_enabled = true; ?>
	<div class="span6">
		<?= CHtml::label(
			CHtml::radioButton('payment_method', true, array('value' => Payment::$PAYMENT_METHOD_CYBERSOURCE, 'id'=>'payment_method_cybersource')) . CHtml::tag('span',array('class'=>'sprite-checkout cybersource')),
			'payment_method_cybersource',
			array('class'=>'radio')
		) ?>
	</div>
	<?php endif; ?>

	<?php if (Payment::isMethodEnabled(Payment::$PAYMENT_METHOD_STRIPE) && Settings::get('stripe_account_email') != '' && Settings::get('stripe_private_key') != '' && Settings::get('stripe_public_key') != '') : $at_least_one_enabled = true; ?>
		<div class="span6">
			<?= CHtml::label(
				CHtml::radioButton('payment_method', true, array('value' => Payment::$PAYMENT_METHOD_STRIPE, 'id'=>'payment_method_stripe')) . CHtml::tag('span',array('class'=>'sprite-checkout stripe'), '&nbsp;'),
				'payment_method_stripe',
				array('class'=>'radio')
			) ?>
		</div>
	<?php endif; ?>

	<?php if (Payment::isMethodEnabled(Payment::$PAYMENT_METHOD_PAYFLOW)) : $at_least_one_enabled = true; ?>
		<div class="span12">
			<?= CHtml::label(
				CHtml::radioButton('payment_method', true, array('value' => Payment::$PAYMENT_METHOD_PAYFLOW, 'id'=>'payment_method_payflow')) . CHtml::tag('span',array('class'=>'sprite-checkout paypal')),
				'payment_method_payflow',
				array('class'=>'radio')
			) ?>
		</div>
	<?php endif; ?>

	<?php if(!$at_least_one_enabled) : ?>
		<div class="span12">
			<span class="errorMessage">
				<?= Yii::t('ecommerce', 'No payment gateway found, please contact the administrator to solve the issue'); ?>
			</span>
		</div>
	<?php endif; ?>

	<?php endif; ?>
</div>