<?php
/* @var $bgCss CSS class to specify the background of the tile */
/* @var $title */
/* @var $description */
/* @var $icon Show an image based on this */
?>
<li class="tile cover <?= (!empty($bgCss) ? $bgCss : '' ) ?>">
	<div class="tile-content">
		<h2><?= $title ?></h2>
		<p><?= $description ?></p>

<?php
switch ($icon) {
	case 'grid':
		$icon = '<span class="icon icon-grid"></span>';
		break;
	case 'cart':
		$icon = '<span class="icon icon-cart"></span>';
		break;
	default:
		$icon = '';
		break;
}
?>

		<?php if (!empty($icon)) : ?>
			<div class="image-wrapper">
			<?= $icon ?>
		</div>
		<?php endif; ?>

		<div class="right-arrow"></div>
	</div>
</li>