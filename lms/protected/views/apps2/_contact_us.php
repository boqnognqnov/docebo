<?php
/* @var $this AppController */
/* @var array $app */
?>

<!-- Consumed by Dialog2, used as Dialog Title -->
<h1><?=Yii::t('standard', 'Contact Docebo');?></h1>

<div class="form">
	<?=CHtml::beginForm('', 'POST', array('class'=>'ajax'))?>

	<div class="contact-docebo-dialog">

		<div class="row-fluid">
			<div class="span3">
				<?=Yii::t('standard', 'Subject:')?>
			</div>
			<div class="span9">
			</div>
		</div>

		<div class="row-fluid">
			<div class="span3">
				<?=Yii::t('standard', 'Your message:<br/><small>(required)</small>')?>
			</div>
			<div class="span9">
				<?=CHtml::textArea('message', '', array('class'=>'contact-docebo-textarea'))?>
			</div>
		</div>

	</div>

	<div class="form-actions">
		<?php echo CHtml::button(Yii::t('standard','Submit'), array('class' => 'btn-docebo green big btn-confirm')); ?>
		<?= CHtml::button(Yii::t('standard', '_CANCEL'), array(
			'class' => 'btn-docebo black big close-dialog'
		)); ?>
	</div>

	<script>
		$(function(){
			// Remove the ".wide" class from the current dialog
			$('.modal.app-details').removeClass('wide');

			$(document).on('click', '.btn-confirm', function(e){
				e.preventDefault();
				e.stopPropagation();
				$(this).closest('form').submit();
			});
		});
	</script>

	<?=CHtml::endForm()?>
</div>