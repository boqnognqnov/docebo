<?
/* @var $this AppController */
?>

<h1><?=Yii::t('standard', 'Warning')?></h1>

<?=CHtml::beginForm('', 'POST', array('class'=>'ajax form enterprise-tos'))?>

	<?=CHtml::hiddenField('id', $id)?>
	<?=CHtml::hiddenField('step', '2')?>

	<? if(isset($error) && $error): ?>
		<p class="text-center errorMessage"><?=$error?></p>
	<? endif; ?>

	<p><?=Yii::t('apps', 'While buying the <b>Enterprise Cloud Solution</b> app, I understand that:')?></p>

	<p>
		<?=CHtml::checkBox('tos_plan_linked', isset($_POST['tos_plan_linked']))?>
		<?=Yii::t("app", "Once activated, this app <b>will be permanently linked to your plan</b> and <b>you won’t be able to cancel it</b> unless you cancel your 500 users or more  subscription.")?>
	</p>

	<p>
		<?=CHtml::checkBox('tos_activate_days', isset($_POST['tos_activate_days']))?>
		<?=Yii::t("apps", "The activation of this app can take <b>up to {days} working days</b>", array('{days}'=>'5')); ?>
	</p>

	<p>
		<?=CHtml::checkBox('tos_service_delay', isset($_POST['tos_service_delay']))?>
		<?=Yii::t("apps", "I may experience an <b>interruption of the service</b> (for some hours) while migrating the installation to the new cloud infrastructure")?>
	</p>

	<p>
		<?=CHtml::checkBox('tos_docebo_agree', isset($_POST['tos_docebo_agree']))?>
		<?=Yii::t('cart','I agree with the {link}Terms and Conditions of the service.{\link}',array(
			'{link}' => '<a href="' . (Yii::app()->getLanguage()=='it' ? 'http://www.docebo.com/it/docebo-terms-and-conditions/' : 'http://www.docebo.com/docebo-terms-and-conditions/') . '" data-dialog-class="read-tos-modal" closeOnEscape="false" class="tos-link open-dialog" rel="read-tos-modal">',
			'{\link}' => '</a>'));?>
	</p>

<div class="form-actions">
	<?php echo CHtml::submitButton(Yii::t('standard','_CONFIRM'), array('class' => 'btn-docebo green big btn-submit')); ?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn-docebo black big close-dialog')); ?>
</div>

<script type="text/javascript">
	$(function(){
		$('input[type=checkbox]').styler();

		// Un-widen  the current dialog
		$('.modal.app-details').removeClass('wide');

		$('input.btn-submit').click(function(e){
			e.preventDefault();
			e.stopPropagation();
			$(this).closest('form').submit();
		});
	});
</script>

<?=CHtml::endForm()?>