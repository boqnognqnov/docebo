<?php
/* @var $this AppController */

/* @var array $apps */
/* @var string $coverTile */
/* @var string $backCoverTile */
/* @var string $action */

$this->widget('common.widgets.Apps2Grid', array(
	'action' => $action,
	'apps' => $apps,
	'featuredApps' => $featuredApps,
	'coverTile' => $coverTile,
	'backCoverTile' => $backCoverTile,
	'isECSInstallation' => $isECSInstallation
));