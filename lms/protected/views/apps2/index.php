<?php

/**
 * Shows a container, makes AJAX call(s) to load the actual grid.
 * @var $this AppController
 */

$this->breadcrumbs[] = Yii::t('apps', 'Paid apps');
$this->widget('OverlayHints', array('hints'=>'bootstroNewAppsDesign'));

$myPlan = " <strong>" . Yii::t("standard", "(my plan)") . "</strong>";

if ($isInTrialPeriod) {
    $myPlan = "";
}

?>
<div class="myapps-search">
	<div class="filters-wrapper">
		<div class="inner-filter">
			<div class="row-fluid">
				<div id="app_and_integrations_filter" class="span9 radio-buttons">
					<?= Yii::t("standard", "Available for") . " " ?>
					<?
					echo CHtml::radioButtonList('app_type', false, array(
                        ''              => Yii::t('standard', 'All plans'),
						'starter'       => ucfirst(strtolower(Yii::t('billing', 'Starter plan'))) . (($pricingPlan==='starter') ? $myPlan : ""),
						'advanced'      => ucfirst(strtolower(Yii::t('billing', 'Advanced plan'))) . (($pricingPlan==='advanced') ? $myPlan : ""),
					    'enterprise'    => ucfirst(strtolower(Yii::t('billing', 'Enterprise plan'))) . (($pricingPlan==='enterprise') ? $myPlan : ""),
					), array(
                        'template' => "<div class='radio-container'>{input} {label}</div>",
                        'container' => '',
                        'separator' => '',
					));
					?>
				</div>
			<div class="span3">
				<div class="pull-right">
					<div class="search-wrapper">
						<?php
						echo CHtml::textField('myapps_search', '', array(
								'autocomplete' => 'off'
						));
						?>
						<span class="search-clear-icon"></span>
						<span class="search-icon"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<br>
<div class="myapps-container">

<div class="filtered-apps" style="display:none;"></div>

<div class="un-filtered-apps">
	<?php 
        $this->beginWidget('common.widgets.Sidebarred', array(
			'sidebarId' => "app_and_integrations_sidebar",
			'sidebarTitle' => Yii::t('userlimit', 'Apps & Integrations'),
        )); 
        $this->endWidget(); 
    ?>
</div>

</div>

<?php echo CHtml::hiddenField('selectedTab', '', array('id' => 'selectedTab')); ?>


<script type="text/javascript">


	function openCommCenter() {
		<?php if (Yii::app()->legacyWrapper->hydraFrontendEnabled()) { ?>
		    <?= Yii::app()->legacyWrapper->openCommunicationCenter(false) ?>
		<?php } else { ?>
			$(document).trigger("communication-center.open", "sales");
		<?php } ?>
	}

	$(function(){
		
		/**
		 * Upon "Contact Us" clicks, open Communication center -> Sales
		 */
		$(document).on("click", ".contact-us-links", function(){
			openCommCenter();
		});

		$('.radio-container input').styler();

        $('body').on('click', '.install-once', function (){
			$(this).hide();
		});

		$('.sidebarred .ajaxloader').show();

		loadCategories();

		$(window).hashchange(function(){
			var hash = location.hash,
				hasStr = "",
				link = null;

			hashStr = hash.substr(1);

			if(hashStr.length==0)
				return;

			if($('.sidebarred .sidebar a').length==0)
				return;

			link = $('.sidebarred .sidebar a.' + hashStr);

			$('.sidebarred .sidebar a').removeClass('active');
			link.addClass('active');

			if (link.data('loaded') == 0) {
				loadTabContent(link);
			}

			$('.tab-content .single-tab-content').hide();
			$('.tab-content').find(hash).show();
			$('#selectedTab').val(hashStr);

			$(window).trigger('resize');
		});

		$(window).hashchange();

		// Capture free/paid/all apps radio filtering change
		$(document).on('change', '.single-tab-content input[name=app_type]', function(){
			var selectedType = $(this).val();

			switch(selectedType){
			
				case 'starter':
					$('.single-app-holder', $(this).closest('.single-tab-content')).each(function(){
						if(!$(this).hasClass('starter')){
							$(this).hide();
						}else{
							$(this).show();
						}
					});
					break;
					
				case 'advanced':
					$('.single-app-holder', $(this).closest('.single-tab-content')).each(function(){
						if(!$(this).hasClass('advanced')){
							$(this).hide();
						}else{
							$(this).show();
						}
					});
					break;

				case 'enterprise':
					$('.single-app-holder', $(this).closest('.single-tab-content')).each(function(){
						if(!$(this).hasClass('enterprise')){
							$(this).hide();
						}else{
							$(this).show();
						}
					});
					break;

					
				default: // Show all apps in current category (free and paid)
					$(this).closest('.single-tab-content').find('.single-app-holder').show();
			}
		});
	});

	var options = {
		mainContainer: 'myapps-container',
		url: '<?= Docebo::createAbsoluteUrl('apps2/getAllApps') ?>',
		buttonListener: 'myapps_search',
		filterButtons: 'app_type',
		clearTextIconClass: 'search-clear-icon',
		backButtonText: '<?= Yii::t('standard', '_BACK') ?>',
		resultsFoundText: '<?= Yii::t('app', 'results found') ?>',
		isInTrialPeriod: '<?=$isInTrialPeriod?>',
		debug: false
	};
	var searchEngine = new SearchEngine(options);

	function loadCategories(){
		if($('.sidebarred .sidebar .ajaxloader').length == 0){
			var loader = $('<div class="ajaxloader"><?= Yii::t("standard", "_LOADING") ?>...</div>');
			$('.sidebarred .sidebar').append(loader);
		}
		$('.sidebarred .sidebar .ajaxloader').show();

		$.ajax({
			url: '<?=Docebo::createLmsUrl('apps2/getAppCategories')?>',
			cache: false,
			timeout: 4000,
			error: function(){
				console.log('Error with getting app categories');
				console.log(arguments);
				return true;
			},
			success: function (result) {

				$('.sidebarred .sidebar .ajaxloader').hide();

				var res;
				try{
					res = $.parseJSON(result);
				}catch(e){
					console.log(e);
					return;
				}

				if(res){
					var container = $('<ul></ul>');
					$.each(res, function(catName, html){
						var div = $('<li></li>').html(html);
						container.append(div);
					});
					$('.sidebarred .sidebar').append(container);
				}

				if (location.hash == "" && $('.sidebarred .sidebar a').length > 0) {
					// If there are no links in the sidebar, don't change the hash for now
					// (Usually those links are loaded via Ajax and the success event of that
					// ajax should trigger $(window).hashchange() to re-call this)
					location.hash = '#' + $('.sidebarred .sidebar a').first().data('tab');
					try{
						location.redirect();
					}catch(e){}
				}

				$(window).hashchange();
			}
		});
	}

	function loadTabContent(link) {
		$('.sidebarred .tab-content .ajaxloader').css('display', 'block').show();

		var url = yii.urls.base + '/index.php?r=' + link.data('url');
		
		$.ajax({
			url: url,
			cache: false,
			timeout: 4000,
			error: function(){
				console.log('Error with getting tab contents');
				console.log(arguments);
				return true;
			},
			success: function (result) {
				$('.sidebarred .tab-content .ajaxloader').hide();

				// Prevent double loading on page load or if quickly switching between tabs
				$('.tab-content .single-tab-content').hide();

				link.data('loaded', 1);

				var res;
				try{
					res = $.parseJSON(result);
				}catch(e){
					console.log(e);
					return;
				}

				var html = res.html;

				var categories = res.categories ? res.categories : false;

				if(categories){
					$.each(categories, function(i, val){

					});
				}

				// If the tab exists previously (although it shouldn't) - remove it
				$('.tab-content #'+link.data('tab')).remove();

				var div = $('<div/>', { 'id': link.data('tab'), 'class': 'single-tab-content' }).html(html);
				$('.tab-content').append(div);

				// if (link.data('tab') != 'logo')
				$('.tab-content input, .tab-content select').styler();

				$('a[rel=tooltip]').tooltip();

				// Suck Dialog2 controls
				$(document).controls();

				// Some elements inside the loaded tab content will need to know this,
				// since triggering this event from inside the tab content doesn't work
				$(window).trigger('resize');

				// Equalize the height of Top Extensions boxes
				var maxH = 0;
				$('.promoted-apps .promoted-app').each(function(){
					if($(this).height() > maxH)
						maxH = $(this).height();
				});
				$('.promoted-apps .promoted-app').height(maxH);

				if(link.data('tab')=='myapps'){
					// If the My Apps tab is being viewed and there are
					// no installed apps, show the Bootstro
					if($('.myapps-empty').length > 0){
						// .myapps-empty is only visible when there are no apps installed
						// see apps_grid_loader.php for usage
						setTimeout(function() {
							bootstro.start('bootstroNewAppsDesign', {}, {
								placement:'bottom',
								width:'320px'
							});
						}, 2500);
					}
				}

			}
		});
	}

	$(function(){

		/**
		 * Resize CSS based triangles so they are as wide as their parent
		 */
		function resizeTriangle(){
			setTimeout(function(){
				var arrowParentHalfWidth = $('.arrow-down').parent().width()/2;
				if(arrowParentHalfWidth){
					$('.arrow-down').css('border-left-width', arrowParentHalfWidth);
					$('.arrow-down').css('border-right-width', arrowParentHalfWidth);
				}
			}, 10);
		};

		/**
		 * Recenter elements that have .autoposition in their container.
		 * Note that you may also optionally add .vertical or .horizontal to
		 * recenter them only in that direction
		 * @TODO make this a jQuery plugin
		 */
		var repositionCentered = function(){
			$('.autoposition').each(function(){
				var t = $(this);
				var parent = $(this).parent();

				var pWidth = parent.width();
				var pHeight = parent.height();

				if(t.hasClass('horizontal')){
					t.css('margin-left', (pWidth/2 - t.outerWidth()/2));
				}else if(t.hasClass('vertical')){
					t.css('margin-top', (pHeight/2 - t.outerHeight()/2));
				}else{
					// Center horizontally and vertically
					t.css('margin-left', (pWidth/2 - t.outerWidth()/2));
					t.css('margin-top', (pHeight/2 - t.outerHeight()/2));
				}
			});
		};

		var equalizeRowElementsHeights = function(){
			$('.compare-row').each(function(){
				var children = $('.one-fourth', this);

				var maxH = 0;
				children.each(function(){
					if($(this).height() > maxH)
						maxH = $(this).height();
				});
				children.height(maxH);
			});
		};

		$(window).on('resize', function(){
			resizeTriangle();
			equalizeRowElementsHeights();

			repositionCentered();
		}).trigger('resize');

		// This will be set to true if somewhere inside the dialog
		// you return .reload-after-dialog-close, which will cause a
		// reload of the current page when the dialog is closed
		var reloadPageOnDialogClose = false;

		// Handle a.auto-close and a.auto-close.reload
		$(document).on("dialog2.content-update", ".modal", function () {
			if($(this).find("a.auto-close.reload").length){
				$(this).find(".modal-body.opened").dialog2("close");
				window.location.reload();
			}else if ($(this).find("a.auto-close").length > 0) {
				$(this).find(".modal-body.opened").dialog2("close");
			}else{
				// Things inside the dialog need to know this (e.g. the .autoposition tool)
				$(window).trigger('resize');
			}

			if($(this).find('.reload-after-dialog-close').length){
				reloadPageOnDialogClose = true;
			}
		});

		$(document).on('dialog2.closed', '.modal', function(){
			// Reload the current page if requested from inside the dialog

			if(reloadPageOnDialogClose)
				window.location.reload();
		});

	});
</script>