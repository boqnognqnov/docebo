<?php
/* @var $app array */
?>
<h1><?= $app['title'] ?></h1>

<a href="#" class="hide"></a>

<p><?= Yii::t('apps', 'This app is required by the following apps and can\'t be removed:') ?>
	<ul>
		<?php foreach ($app['required_by'] as $codeName) { ?>
			<li><?= ucfirst($codeName) ?></li>
		<?php } ?>
	</ul>
</p>

<div class="form-actions">
	<button class="btn-docebo black big close-dialog"><?= Yii::t('standard', '_CLOSE') ?></button>
</div>