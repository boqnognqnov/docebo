<?php
/* @var $app array */
?>
<h1><?= $app['title'] ?></h1>
<p><?= Yii::t('apps', 'This app <strong>is going to be removed from your apps</strong>') ?></p>
<div class="text-right">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id'          => 'remove_app',
		'action'      => Yii::app()->createUrl('apps2/axRemoveApp', array(
		    'confirm' => 1, 
		    'code'    => $app['internal_codename'],
        )),
		'htmlOptions' => array(
			'class' => 'ajax'
		)
	)); ?>
	<input type="submit" class="ajax btn btn-docebo green big" value="<?= Yii::t('standard', '_CONFIRM') ?>">
	<?php $this->endWidget(); ?>
</div>