<?php
/* @var $title string */
?>

<h1><?= $title ?></h1>

<?php if($success): ?>

<p><?= Yii::t('apps', 'Your app has been removed from <strong>MY APPS</strong>.') ?> <i class="icon-ok"></i></p>

<?php else: ?>

	<p><?= Yii::t('apps', 'Error') ?> <i class="icon-minus-sign"></i></p>
	<br>

	<?php
		if(Yii::app()->user->hasFlash('error')):
			?>

			<p><?= Yii::app()->user->getFlash('error') ?></p>

			<?php

		endif;
	?>

<?php endif ?>

<a href="<?= Yii::app()->createUrl('apps2/index') ?>" class="btn btn-docebo green big pull-right span2"><?= Yii::t('apps', 'Go to my apps') ?></a>


<script type="text/javascript">
	$(document).delegate('.modal', 'dialog2.closed', function(){
		var e = $(this);
		if(console) {
			console.log(AppsGrid);
		}

		AppsGrid.refresh();
	});
</script>