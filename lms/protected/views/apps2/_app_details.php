<?php /** @var $this Apps2Controller */ ?>

<?php
    $skipDefaultButton = false;
    if (isset($install_detail['skip_default_button'])) {
        $skipDefaultButton = $install_detail['skip_default_button'];
    }
    
    $contactUsUrl = Yii::app()->createUrl('site/axContactSalesTrialExpired', array('app_id' => $app['app_id'])); 
?>

<h1><?= $app['title'] ?></h1>
<div class="app-details-dialog">
	<div class="pull-left thumb-area">
		<div class="app-title"><?= $app['title'] ?></div>
		<?php if($app['image_url']) { ?>
			<img alt="<?=$app['title']?>" class="app-image" src="<?=$app['image_url']?>">
		<?php } ?>

		<?php if (!$skipDefaultButton) { ?>

            <? if ($action == Apps2Widget::ACTION_ACTIVATE) { ?>
            	
            	<p><a href="<?= Yii::app()->createUrl('apps2/axConfirmInstall', array('code' => $app['internal_codename'], 'action' => $action)) ?>" class="ajax btn-docebo blue full big text-center"><?= Yii::t('apps', 'Install app') ?></a></p>
            	
            <? } else if ($action == Apps2Widget::ACTION_DISCOVER_MORE) { ?>
            
            	<div class="discover-more">
            		<p><?= Yii::t('app', '<strong>Contact Your Docebo Partner / Reseller</strong> for more information') ?></p>
            	</div>
    
            <? } else if ($action == Apps2Widget::ACTION_UPGRADE) { ?>
    			<p><a class="btn-docebo green big full text-center" href="<?= Yii::app()->createUrl('billing/default/upgrade', array()) ?>"><?= Yii::t('apps', 'Upgrade now') ?></a></p>
    			<p><a class="contact-us-links btn-docebo blue  big full text-center" href="javascript:;"><?= Yii::t('apps', 'Contact Us Now') ?></a></p>
            	
            <? } ?>
        
        <?php } ?> 

		<div class="clearfix"></div>

	</div>

	<div class="description-holder">
		<div class="app-description-header"><?=Yii::t('standard', '_DESCRIPTION')?>:</div>
		<div class="app-description">
			<?php if (!empty($install_detail['custom_desciption'])) { ?>
				<?= $install_detail['custom_desciption'] ?>
			<?php } else { ?>
				<?= str_replace("\n", '<br>', str_replace("\t", '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $app['extended_description'])) ?>
			<?php } ?>
		</div>		
	</div>
	
	
	
</div>


<div class="clearfix"></div>



<?php if ( ($action == Apps2Widget::ACTION_TRY_FOR_FREE) && !$skipDefaultButton) { ?>
	<div class="trial-to-plan-explanation">
    <div class="row-fluid">
    	<div class="span9">
    		<div class="pull-left exclamation-icon">
    			<i class="fa fa-lg fa-exclamation-triangle"></i>&nbsp;
    		</div>
    		<div class="explanation-text">
				<?php echo Yii::t("apps", "After your trial period, you'll need {required_plan} in order to use this App.", array(
				    "{required_plan}" => '<strong>'.ucfirst($app['available_in_plan']).' plan</strong>',
				)); ?>
    			<br>
    			<?php echo Yii::t("apps", "Need more information? {contact_us}!", array(
                    '{contact_us}' => '<a href="javascript:;" class="contact-us-links trial-contact-us contact-us">'.Yii::t('apps', 'Contact Us Now').'</a>',
                )); ?>
    		</div>
    	</div>
    	<div class="span3 text-center button-cell">
			<a href="<?= Yii::app()->createUrl('apps2/axConfirmInstall', array('code' => $app['internal_codename'], 'action' => $action)) ?>" class="ajax btn-docebo orange full big text-center"><?= Yii::t('apps', 'Start free trial') ?></a>
    		<?= Yii::t("standard", "14-day <strong>Free Trial</strong>") ?>
    	</div>
    </div>
    </div>
<?php } ?>


<script type="text/javascript">
	$(function(){
		
		$(document).on("click", ".contact-us-links.trial-contact-us, .modal-body.opened .contact-us-links", function(){
			$(this).closest(".modal-body").dialog2("close");
		});
		
		$('.app-details-dialog a.disabled').click(function(e){
			e.preventDefault();
			e.stopPropagation();
		});
	})
</script>
