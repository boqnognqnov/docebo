<?php /* @var $data array  */ ?>
<div class="item">
    <div class="primary row-fluid">
        <div class="item-col col-id">
            <?= CHtml::radioButton('courseSessionId', false, array('value' => $data['id_session'])) ?>
        </div>
        <div class="item-col col-name text-colored">
            <?= ($data->isMaxEnrolledReached() ? '<i class="i-sprite is-clock orange" style="margin-bottom:4px;" rel="tooltip" title="" data-original-title="'.Yii::t("course", "_ALLOW_OVERBOOKING").'"></i> ' : '').$data->name; ?>
        </div>
        <div class="item-col col-start">
            <?= $data['date_begin'] ?>
        </div>
        <div class="item-col col-end">
            <?= $data['date_end'] ?>
        </div>
        <div class="item-col col-location">
            <?php
            if (!empty($data->learningCourseSessionDates)) {
                /* @var $location LtLocation */
                $location = $data->learningCourseSessionDates[0]->ltLocation;
                echo CHtml::link($location->name, '#', array(
                    'title' => $location->name,
                    'rel' => 'tooltip'
                ));
            } else {
                echo '-';
            }
            ?>
        </div>
        <div class="item-col col-toggle_handle">
            <?= CHtml::tag('span', array('class'=>'toggle-handle docebo-sprite ico-arrow-bottom', 'data-expand-class'=>'ico-arrow-top', 'data-retract-class'=>'ico-arrow-bottom'), '&nbsp;') ?>
        </div>
    </div>
    <?php if (!empty($data->learningCourseSessionDates)) : ?>
        <div class="secondary hide">
            <div class="schedule">
                <div class="title"><strong><?= Yii::t('standard', '_SCHEDULE') ?></strong></div>
                <div class="days">

                    <?php /* @var $sessionDate LtCourseSessionDate */ ?>
                    <?php foreach ($data->learningCourseSessionDates as $sessionDate) : ?>
                        <div class="day">
                            <div class="col-day-name"><?= $sessionDate->name ?></div>
                            <div class="col-day-date"><?= Yii::app()->localtime->toLocalDateTime($sessionDate->day . ' ' . $sessionDate->time_begin, 'short', 'short', null, $sessionDate->timezone);// Yii::app()->localtime->toLocalDate($sessionDate->day) ?></div>
                            <div class="col-day-break">
                                <?= Yii::app()->localtime->toLocalDateTime($sessionDate->day . ' ' . $sessionDate->time_end, 'short', 'short', null, $sessionDate->timezone);//$sessionDate->renderDaySchedule() ?>
                            </div>
                            <div class="col-day-location">
                                <?= $sessionDate->ltLocation->name .
                                (
                                ($sessionDate->ltClassroom)
                                    ? ' - ' . $sessionDate->ltClassroom->name
                                    : ''
                                ) ?> <br/>
                                <?= $sessionDate->ltLocation->renderAddress() ?>
                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
<script type="text/javascript">
	if($('i[rel="tooltip"]'))
		$('i[rel="tooltip"]').tooltip();
</script>