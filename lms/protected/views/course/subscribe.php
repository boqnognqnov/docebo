<br />
<?php if (Yii::app()->user->hasFlash('error')) : ?>
<div class='row-fluid'>
    <div class="span12 alert alert-error">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
</div>
<?php else: ?>
<div class='row-fluid'>
    <div class="span12 alert alert-success">
			<?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
</div>
<?php endif; ?>