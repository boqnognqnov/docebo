<style>
	/*this is needed for correct displaying the tabbed table when expanding sessions*/
	.ui-accordion-content{
		height: 0 !important;
		background-color: rgba(0, 0, 0, 0.10);
	}
	.activeRow{
		background-color: rgba(0, 0, 0, 0.20);
	}
	.innerTable{
		width: 100%;
	}
	.innerTable tr td{
		width: 9%;
		padding: 3px;
	}
	.innerTable tr td:first-child{
		width: 12%;
	}
	.innerTable tr td:last-child{
		width: 2%;
	}
	#accordion th{
		text-transform: uppercase;
	}
	.row-accordion{
		cursor: pointer;
	}
	.row-accordion td{
		width: 12%;
	}
	.row-accordion td:first-child{
		width: 15%;
	}
	.row-accordion td:last-child{
		width: 2%;
	}
</style>
<?php
/** @var LearningCourse $courseModel */
/** @var array $trainingMaterials */
/** @var LearningCourse[] $topRatedCourses */

Yii::app()->clientScript->registerCoreScript('jquery.ui');
$this->breadcrumbs = array(
	Yii::t('standard', 'Catalog') => Docebo::createAppUrl('lms:site/index', array('opt'=>'catalog')),
	$courseModel->name
);


/* @var $courseModel LearningCourse */
$currencySymbol = Settings::get("currency_symbol");
$course_id = $courseModel->idCourse;
$courseLogoUrl = $courseModel->getCourseLogoUrl();
$icon_globe_url = Yii::app()->theme->baseUrl . '/images/globe.png';
$icon_clock_url = Yii::app()->theme->baseUrl . '/images/clock.png';

$preview_arrow_url = Yii::app()->theme->baseUrl . '/images/preview_course_arrow.png';

// On-login-success redirect URLs for different 'reasons'


if (Yii::app()->user->isGuest && PluginManager::isPluginActive('ClassroomApp') && $courseModel->isClassroomCourse()) {
	// if this is a classroom course, after login we must trigger the course
	// detail modal through the course_code url param so that the user can
	// select the sessions to enroll to
	$buyRedirectUrl = Docebo::createAppUrl('lms:site/index', array('course_code' => $courseModel->code));
}
else {
	$buyRedirectUrl = Docebo::createAppUrl('lms:course/buy', array('course_id' => $course_id));
}
$subscribeRedirectUrl = Docebo::createAppUrl('lms:course/subscribe', array('course_id' => $course_id));
$overbookRedirectUrl = Docebo::createAppUrl('lms:course/overbook', array('course_id' => $course_id));

// URLs used when user is a Guest and to clicks Buy/Subscribe/etc
$buyRedirectUrlForLogin = Docebo::createAppUrl('lms:course/details', array('id' => $course_id, 'click'=>'btn-course-details-buy'));
$toBuyLoginUrl = Docebo::createAppUrl('lms:user/axLogin', array('reason' => 'buy', 'redirect_url' => urlencode($buyRedirectUrlForLogin)));

$shopifyLink = false;
// shopify link on shopify platform enabled
if ($courseModel->selling && PluginManager::isPluginActive('ShopifyApp'))
{
	$shopifySubdomain = Settings::get('shopify_subdomain', false);
	$shopifyProductMeaningfulId =
		LearningCourseShopifyProduct::model()->find('idCourse = :idCourse', [':idCourse' => $course_id])->shopifyProductMeaningfulId;
	$toBuyLoginUrl = "http://$shopifySubdomain.myshopify.com/products/$shopifyProductMeaningfulId";
	$buyRedirectUrl = "http://$shopifySubdomain.myshopify.com/products/$shopifyProductMeaningfulId";
	$shopifyLink = true;
}

$guestSubscribeRedirectUrl = Docebo::createAppUrl('lms:course/details', array('id' => $course_id, 'click'=>'btn-course-details-subscribe'));
$toSubscribeLoginUrl = Docebo::createAppUrl('lms:user/axLogin', array('reason' => 'subscribe', 'redirect_url' => urlencode($guestSubscribeRedirectUrl), 'course_id'=> $course_id));
$toOverbookLoginUrl = Docebo::createAppUrl('lms:user/axLogin', array('reason' => 'overbook', 'redirect_url' => urlencode($overbookRedirectUrl)));

// Buttons HTML (depend on user status)
if (Yii::app()->user->isGuest) {
	$buyButtonHtml = "<a style='color: white;' href='$toBuyLoginUrl' class='ajax btn btn-docebo big green input-block-level btn-course-details-buy'>" . Yii::t('userlimit', 'Buy now').($isOverbooked ? '<br />('.Yii::t('course', 'OVERBOOK').')' : '') . "</a>\n";
	$subscribeButtonHtml = "<a style='color: white;' href='$toSubscribeLoginUrl' class='ajax btn btn-docebo big green input-block-level'>" . $label . "</a>\n";
	$overbookButtonHtml = "<a style='color: white;' href='$toOverbookLoginUrl' class='ajax btn btn-warning input-block-level'>" . Yii::t('course', 'OVERBOOK') . "</a>\n";
} else {
	// wether to show the select session dialog
	$showModal = (PluginManager::isPluginActive('ClassroomApp') && $courseModel->isClassroomCourse());
	$buyButtonHtml = "<a style='color: white;' href='$buyRedirectUrl' class='ajax btn btn-docebo big green input-block-level btn-course-details-buy'>" . Yii::t('userlimit', 'Buy now') . ' (' . $currencySymbol . $courseModel->getCoursePrice() . ')'.($isOverbooked ? '<br />('.Yii::t('course', 'OVERBOOK').')' : '') . "</a>\n";
	$subscribeButtonHtml = "<a style='color: white;' href='$subscribeRedirectUrl' class='ajax btn btn-docebo big green input-block-level btn-course-details-subscribe'>" . $label . "</a>\n";
	$overbookButtonHtml = "<a style='color: white;' href='$overbookRedirectUrl' class='btn btn-warning input-block-level'>" . Yii::t('course', 'OVERBOOK') . "</a>\n";
}

// Preview/Demo related
if ($demoType == LearningCourse::$DEMO_MEDIA_CATEGORY_UNKNOWN) {
	$previewButtonText = Yii::t('standard', 'Preview');
	$aClass = "";
} else {
	if ($demoType === false) {
		$previewButtonText = "";
	} else {
		$previewButtonText = Yii::t('standard', 'Preview');
		$aClass = "ajax";
	}
}

?>

<div class="row-fluid course-details">

	<!-- LOGO and Preview/Demo buttons -->
	<div class="span4 course-details-sidebar">

		<div class="course-details-sidebox" style="margin-top: 0px;">

			<h2><?= Yii::t('course', 'Course details') ?></h2>

			<div class="course-info-primary">
				<img src="<?= $courseLogoUrl ?>" class="input-block-level">

				<?php if ($demoType !== false) : ?>
					<!-- Preview/Download links -->
					<a href="<?= $previewActionUrl ?>" class="<?= $aClass ?> preview-btn"<?php if ($demoType == LearningCourse::$DEMO_MEDIA_CATEGORY_UNKNOWN) echo ' target="_blank"'; ?>>
						<img src="<?= $preview_arrow_url ?>">
					</a>
					<a href="<?= $previewActionUrl ?>"
					   style="color: white; opacity: 0.8 !important; position: absolute; bottom: 80px; left: 26px;"
						<?php if ($demoType == LearningCourse::$DEMO_MEDIA_CATEGORY_UNKNOWN) echo ' target="_blank"'; ?>
					   class="<?= $aClass ?> btn btn-docebo big green"><?= $previewButtonText ?></a>
				<?php else : ?>
					<span
						style="opacity: 0.8 !important; position: absolute; bottom: 80px; left: 15px;"><?= $previewButtonText ?></span>
				<?php endif; ?>

				<?php if ($showButton) : ?>
					<?php if ($courseModel->selling) : ?>
						<?= $buyButtonHtml ?>
					<?php elseif ($isOverbooked) : ?>
						<?= $overbookButtonHtml ?>
					<?php else : ?>
						<?= $subscribeButtonHtml ?>
					<?php endif; ?>
				<?php endif; ?>
			</div>

			<div class="course-info-secondary">
				<ul class="course-details-list">
					<?php if ($courseModel->mediumTime) : ?>
						<li>
							<h4><?= Yii::t('statistic', '_HOW_MUCH_TIME') ?></h4>
							<?= $courseModel->getRoundedCourseTimeByHoursMinutes() ?>
						</li>
					<?php endif; ?>
					<li>
						<h4><?= Yii::t('standard', '_LANGUAGE') ?></h4>
						<?= $langName ?>
					</li>
					<?php if ($courseModel->canViewRating()) : ?>
					<li>
						<h4><?= Yii::t('course', 'Rate this course') ?></h4>
						<div class="course-rating">
							<?php
								$this->widget('common.widgets.DoceboStarRating',array(
									'id'=>'course-rating-'.$courseModel->idCourse,
									'name'=>'course_rating_'.$courseModel->idCourse,
									'type' => 'star',
									'readOnly' => !$courseModel->canRate(Yii::app()->user->id),
									'value' => $courseModel->getRating(),
									'ratingUrl' => Docebo::createAppUrl('lms:course/axRate'),
									'urlParams' => array(
										'idCourse' => $courseModel->idCourse
									),
									'id_course' => $courseModel->idCourse,
									'id_user' => Yii::app()->user->id
								));
							?>
							<span class="rating-value fixed_rating"><?= $courseModel->getRating() ?></span>
						</div>
					</li>
					<? endif; ?>
				</ul>
			</div>
		</div>
		<?php if (!empty($topRatedCourses) && ($courseModel->course_rating_permission != AdvancedSettingsSocialRatingForm::COURSE_RATING_DISABLED)) : ?>
		<div class="course-details-sidebox">
			<h2><?= Yii::t('course', 'Top {count} rated courses', array('{count}'=>count($topRatedCourses))) ?></h2>
			<ul class="top-rated-courses clearfix">
				<?php
				$_topRatedIndex = 1;
				foreach ($topRatedCourses as $topRatedCourse) : ?>
				<li class="clearfix">
					<span class="index"><?= $_topRatedIndex ?></span>
					<span class="course-image">
						<img src="<?= $topRatedCourse->getCourseLogoUrl() ?>" class="">
					</span>
					<span class="course-title">
						<?php
							$already_subscribed = (!Yii::app()->user->getIsGuest() && LearningCourseuser::model()->isSubscribed(Yii::app()->user->id, $topRatedCourse->idCourse));
							if($already_subscribed){
								$topRatedCourseLink = Docebo::createAbsoluteUrl('lms:player', array('course_id' => $topRatedCourse->idCourse));
							}else{
								$topRatedCourseLink = Docebo::createAppUrl('lms:course/details', array('id' => $topRatedCourse->idCourse));
							}
						?>
						<a href="<?= $topRatedCourseLink ?>"><?= $topRatedCourse->name ?></a>
						<span class="course-rating">
                            <div id="social_rating_course" class="fixed_rating <?php echo $thumbClass == 'tb' ? 'thumb' : 'list' ?>">
                                <span class="star-type-rating rating-container" data-idcourse="<?php echo $topRatedCourse->idCourse; ?>">
                                    <?php
                                        $rating = $topRatedCourse->getRating();
                                        for($i=0; $i<5; $i++)
                                            echo '<div role="text" class="star-rating star-rating'. ($rating > $i ? "-on" : "") .'" id="course-rating-58_'.$i.'"><a title="'.$i.'">'.$i.'</a></div>';
                                    ?>
                                </span>
                                <span class="rating-value fixed_rating"><?= $topRatedCourse->getRating() ?></span>
                            </div>
						</span>
					</span>
				</li>
				<?php
				$_topRatedIndex++;
				endforeach; ?>
			</ul>
		</div>
		<?php endif; ?>
	</div>

	<!-- Language, Duration and <action> Button -->
	<div class="span8 course-details-content">

		<h1 class="course-details-main-title"><?= $courseModel->name ?></h1>
		<div class="course-description">
			<?= $courseModel->description ?>
			<?php
			$current = 0;
			$addClass = '';
			foreach($additionalCourseFields as $fieldName => $fieldItem): ?>
				<?php $fieldValue = $fieldItem['value']; ?>
				<?php $inColumn = $fieldItem['in_column']; ?>
				<?php $current++; $addClass = ($current == 1) ? 'catalog-course-details-additional-fields-first' : '';?>
				<?php if (!empty($fieldValue)): ?>
					<div class="row-fluid catalog-course-details-additional-fields <?=$addClass?>">
						<?php if($inColumn):?>
							<div class="span4 catalog-course-details-additional-fields-label"><?=$fieldName?>:</div>
							<div class="span8 catalog-course-details-additional-fields-value"><?=$fieldValue?></div>
						<?php else: ?>
							<div><b><?= $fieldName ?></b></div>
							<div style="margin-bottom: 20px;"><?= $fieldValue ?></div>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
		</div>

		<?php if ($courseModel->isClassroomCourse() || $courseModel->isWebinarCourse()) : ?>
			<h1 class="course-details-lo-title"><?= Yii::t('dashboard', 'Sessions') ?></h1>
			<?php
				$showExpiredSessions = Yii::app()->user->getIsGodadmin();
				$_sessions = $courseModel->getSessions($showExpiredSessions);
			?>
			<?php if (empty($_sessions)) : ?>
				<i><?= Yii::t('course', 'No sessions') ?></i>
			<?php else: ?>
				<p>
					<span id="expandAll" style="cursor: pointer"><strong><?=Yii::t('curricula_management', 'Expand all')?></strong></span> /
					<span id="collapseAll" style="cursor: pointer"><strong><?=Yii::t('curricula_management', 'Collapse all')?></strong></span>
				</p>
				<table id="accordion" class="table table-bordered">
					<tr>
						<th><?=Yii::t('standard', '_NAME')?></th>
						<th><?=Yii::t('standard', '_START')?></th>
						<th><?=Yii::t('standard', '_END')?></th>
						<?php  if ($courseModel->isClassroomCourse()): ?>
							<th><?=Yii::t('classroom', 'Location')?></th>
						<?php else: ?>
							<th><?=Yii::t('webinar', 'Tool')?></th>
						<?php endif;?>
						<th></th>
					</tr>
					<?php foreach ($_sessions as $session) {
						$dates = $session->getDates();
						?>
						<tr class="row-accordion">
							<td class="course-details-lo-title" style="font-size: 1.1em"><?= $session->name ?></td>
							<td><?=($courseModel->isClassroomCourse()) ? $session->date_begin :  WebinarSession::getStartDate($session->id_session)?></td> <!--Yii::app()->localtime->toLocalDateTime($session->date_begin)-->
							<td><?=($courseModel->isClassroomCourse()) ? $session->date_end :  WebinarSession::getEndDate($session->id_session) ?></td>
							<td >
							<?php  if ($courseModel->isClassroomCourse()): ?>
								<?php
								if (!empty($dates)){
									$locationString = array();
									foreach ($dates as $date) {
										if (!in_array($date->location->name, $locationString)) {
											$locationString[] = $date->location->name;
										}
									}
									$locationString = implode(', ', $locationString);
									echo $locationString;
								}
								?>
							<?php else: ?>
								<?=  $session->getToolName(); ?>
							<?php endif;?>
							</td>
							<td>
								<span class="toggler">
									<i class="fa fa-chevron-down"></i>
								</span>
							</td>
						</tr>
						<tr style="display: none" class="ui-accordion-content">
							<td colspan="5">
								<h6><?=Yii::t('standard', '_SCHEDULE')?></h6>
								<table class="innerTable">
									<?php
									foreach ($dates as $date) {?>
										<tr>
											<td><?=$date->name?></td>
											<td><?= $courseModel->isClassroomCourse() ? Yii::app()->localtime->toLocalDate($date->day) : Yii::app()->localtime->toLocalDate($date->getDateStartDate()) ?></td>
										<?php  if ($courseModel->isClassroomCourse()): ?>
											<td><?=$date->time_begin;?> - <?=$date->time_end."<br />".$date->renderTimezone()?></td>
											<td><?=$date->location->name.'<br/>'.$date->location->address." ".$date->location->country->name_country?></td>
										<?php else: ?>
											<td><?=Yii::app()->localtime->toLocalTime($date->getDateStartDate());?> - <?=Yii::app()->localtime->toLocalTime($date->getDateEndTime())."<br />"?></td>
											<td><?=$date->getToolName()?></td>
										<?php endif;?>
											<td></td>
										</tr>
									<?php } ?>
								</table>
							</td>
						</tr>
					<?php
					} ?>
				</table>
				<script>
					$(function(){
						$('#expandAll').click(function(){
							$('.ui-accordion-content').show();
							$('.row-accordion').addClass('activeRow');
							$('.row-accordion').find('.toggler i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
						});

						$('#collapseAll').click(function(){
							$('.ui-accordion-content').hide();
							$('.row-accordion').removeClass('activeRow');
							$('.row-accordion').find('.toggler i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
						});

						$('.row-accordion').click(function () {
							var clickedElement = $(this);
							if (clickedElement.find('.toggler i').hasClass('fa-chevron-up')) {
								clickedElement.removeClass('activeRow');
								clickedElement.find('.toggler i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
								clickedElement.next('tr').hide();
							} else {
								clickedElement.addClass('activeRow');
								clickedElement.next('tr').show();
								clickedElement.find('.toggler i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
							}
						});
					});
				</script>
			<?php endif; ?>
		<?php endif; ?>
		<h1 class="course-details-lo-title"><?= Yii::t('standard', 'Training materials') ?></h1>
        <?php if (empty($trainingMaterials)) : ?>
            <i><?= Yii::t('course', 'No training materials added') ?></i>
        <?php else: ?>
            <ul class="course-details-lo-list">
                <?php
                $lev = 0;
                foreach($trainingMaterials as $lo)
                {
                    if($lev == 0)
                        $lev = $lo['lev'];

                    if($lo['lev'] > $lev)
                    {
                        $lev = $lo['lev'];
                        echo '<ul>';
                    }
                    elseif($lo['lev'] < $lev)
                    {
                        $lev = $lo['lev'];
                        echo '</ul>';
                    }

                    $icon = '';

                    switch($lo['objectType'])
                    {
                        case '':
                            $icon = '<span class="p-sprite is-folder opened" style="background-position:-520px -477px;width:20px;"></span>';
                            break;
                        case LearningOrganization::OBJECT_TYPE_SCORMORG:
                            $icon = '<span class="object-icon i-sprite objlist-sprite is-zip"></span>';
                            break;
                        case LearningOrganization::OBJECT_TYPE_AICC:
                            $icon = '<span class="object-icon i-sprite objlist-sprite fa fa-archive"></span>';
                            break;
                        case LearningOrganization::OBJECT_TYPE_AUTHORING:
                            $icon = '<span class="i-sprite is-converter"></span>';
                            break;
                        default:
                            $icon = '<span class="object-icon i-sprite objlist-sprite is-'.$lo['objectType'].'"></span>';
                            break;
                    }

                    echo '<li style="margin-left:'.(30 * ($lev - 1)).'px">'.$icon.' '.$lo['title'].'</li>';
                }

                if($lev > 2)
                    for($i = 0; $i < ($lev - 2); $i++)
                        echo '</ul>';
                ?>
            </ul>
        <?php endif; ?>
	</div>
<!--cdffds-->
</div>

<?php
$triggerAfterLoad = Yii::app()->SESSION['trigger_after_load'];
$clickBtnClass = Yii::app()->request->getParam('click', '');
if ($triggerAfterLoad == null) {
	$triggerAfterLoad = false;
}
?>


<script type="text/javascript">
	$(document).ready(function() {

		$(document).controls();
		$('.course-info-primary a.ajax').click(function(event) {
			<?php
				if ($shopifyLink)
					echo 'return; ';
			?>

			event.preventDefault();
			var element = $("<div/>");

			var options = {
				modal: true,
				modalClass: 'course-details-modal',
				id: '<?="dialog-course-detail-$course_id"?>',
				title: <?= CJSON::encode($courseModel->name) ?>,
				content: $(this).attr("href")
			};

			element.dialog2(options);
		});

		<?php if ('' !== ($clickBtnClass = Yii::app()->request->getParam('click', ''))) : ?>
//		$('.<?// //= $clickBtnClass ?>//').trigger('click');
		<?php endif; ?>
		
		defineTriggerEvent();
	});

	function defineTriggerEvent() {
		var isGuest='<?= Yii::app()->user->isGuest ?>';
		var triggerAfterLoad='<?= $triggerAfterLoad ?>';

		var isOnSeparateWindow = '<?= Yii::app()->SESSION['on_separate_window'] ?>';

//		COURSE SETTINGS ->Show the course details as a dedicated page SWITCH:
		if (isOnSeparateWindow == '') {
			if (($('a.ajax').hasClass('btn-course-details-buy') || $('a.ajax').hasClass('btn-course-details-subscribe')) && isGuest != 1 && triggerAfterLoad == true) {
				<?php unset(Yii::app()->SESSION['trigger_after_load']); ?>
				$('.<?= $clickBtnClass ?>').trigger('click');
			}
		} else if (isOnSeparateWindow == 1) {
			if (($('a.ajax').hasClass('btn-course-details-buy') || $('a.ajax').hasClass('btn-course-details-subscribe')) && isGuest != 1 && triggerAfterLoad == true) {

				<?php unset(Yii::app()->SESSION['trigger_after_load']); ?>
				<?php unset(Yii::app()->SESSION['on_separate_window']); ?>
				$('.<?= $clickBtnClass ?>').trigger('click');
			}
		}


	}
</script>

<?php
Yii::app()->SESSION['trigger_after_load']=Yii::app()->user->isGuest;
?>
