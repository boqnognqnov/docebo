<!DOCTYPE html>
<html>
<head></head>
<body>
<!-- Add full HTML just to test Dialog2 -->


<!-- First H1 is used by Dialog2 as dialog Title -->
<h1> <?=$courseModel->name ?> (<?= Yii::t('standard', '_PREVIEW') ?>)</h1>

<div style='position: relative; left: 0; top: 0;'>

    <?php if ( $demoType == LearningCourse::$DEMO_MEDIA_CATEGORY_VIDEO ) : ?>
	<?php

	$browserAgent = (isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null);

	// ivo fix : stopping this check for firefox
	if ( $demoType == LearningCourse::$DEMO_MEDIA_CATEGORY_SWF ) { ?>
		<div>
			<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
				   codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0">
				<param name="SRC" value="<?= $demoUrl ?>">
				<embed class='demo-swf' src="<?= $demoUrl ?>"></embed>
			</object>
		</div>
	<?php
	}else{
		$this->widget('common.extensions.jplayer.jPlayerWidget', array(
			'mediaUrl' => $demoUrl,
			'layout' => 'dialog2_videoplayer',
			'skin' => 'dialog2_videoplayer',
			'autoplay' => true
		));
	}
	?>
    <?php endif;  ?>
    <a href='<?= $backUrl ?>' class='ajax btn btn-primary' style='color: white; opacity: 0.5; position: absolute; right: 5px; top: 5px;'><?= Yii::t('standard', '_BACK') ?></a>
</div>



<script type="text/javascript">
	$(function(){
		$(".modal").addClass("course-details-demo");
		$(".modal").css({ position: "absolute" });
		$('.jp-type-single .jp-jplayer').attr('style', 'margin: 0px auto;');
	});
</script>


</body>
</html>