<?php
/* @var $this CourseController */
/* @var $course LearningCourse */
/* @var $form CActiveForm */
/* @var $dataProvider CActiveDataProvider */
?>

<style type="text/css">
	#webinar-session-list .col-start,
	#webinar-session-list .col-end {
		width: 150px;
	}

	.modal-body {
		max-height: 500px !important;
		width: 850px !important;
	}

	.modal {
		width: 900px;
		margin-left: -450px;
	}
</style>

<h1><?= CHtml::encode($course->name) ?></h1>

<?php
$this->widget('lms.protected.modules.player.components.widgets.CourseHeader', array(
	'course' => $course
));
?>

<div class="classroom-sessions-grid-container" style="margin: 0px; width: 840px;">
	<div class="inner">
		<h3 class="title-bold"><?= Yii::t('classroom', 'Enroll to a session') ?></h3>
		<p><?= Yii::t('classroom', 'Select session') ?></p>
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'classroom-sessions-grid-form',
			'method' => 'POST',
			'htmlOptions' => array(
				'class' => 'docebo-form ajax'
			),
		)); ?>

		<div class="classroom-sessions-grid">
			<?php
			if(isset($codes) && !empty($codes)){
				foreach($codes as $key=>$code){ ?>
					<input type="hidden" value="<?= $code ?>" name="<?= $key ?>"/>
				<?php }
			}
			?>
			<?php $this->widget('DoceboCListView', array(
				'id' => 'webinar-session-list',
				'htmlOptions' => array('class' => 'docebo-list-view'),
				'template' => '{header}{items}{pager}',
				'pager' => array(
					'class' => 'adminExt.local.pagers.DoceboLinkPager',
				),
				'afterAjaxUpdate'	=> "function(id,data) {
					$('.classroom-sessions-grid-container :radio').styler();
            		$('a[rel=\"tooltip\"]').tooltip();
				}",

				'dataProvider' => $dataProvider,
				'itemView' => '_enrollWebinarListView',
				'columns' => array(
					array(
						'name' => 'id',
					),
					array(
						'name' => 'name',
						'header' => Yii::t('standard', '_NAME')
					),
					array(
						'name' => 'start',
						'header' => Yii::t('standard', '_START'),
					),
					array(
						'type' => 'raw',
						'name' => 'end',
						'header' => Yii::t('statistic', '_END')
					),
					array(
						'type' => 'raw',
						'name' => 'tool',
						'header' => Yii::t('webinar', 'Tool')
					),
				)
			)); ?>

		</div>

		<br>
		<div class="form-actions text-right">
			<input class="btn-docebo green big" type="submit" value="<?php echo Yii::t('standard', '_SUBSCRIBE'); ?>" />
			<input type="reset" class="close-dialog btn-docebo black big" value="<?php echo Yii::t('standard', '_CANCEL'); ?>" />
		</div>

		<?php $this->endWidget(); ?>
	</div>
</div>

<script type="text/javascript">

	$(function(){
		var updateGrid = function() {
			$.fn.yiiListView.update('webinar-session-list', {
				data: $("#session-grid-search-form").serialize()
			});
		};
		$('#searchfilter-selectMonth').bind("change", function(e) {
			updateGrid();
		});
		$('#searchfilter-selectYear').bind("change", function(e) {
			updateGrid();
		});
		$('#session-grid-search-form').bind('submit', function(e) {
			updateGrid();
			return false;
		});
	});
	var unloadScripts = function() {
		var incomingScripts = $('.modal').find('script[src],link[rel="stylesheet"]');
		for (var i = 0, len = incomingScripts.length; i < len; i++) {
			if ($._loadedScripts[incomingScripts[i].href]) {
				$._loadedScripts[incomingScripts[i].href] = 0;
			}
		}
	}

	// Upon dialog2 content update....
	$(document).delegate(".course-details-modal", "dialog2.content-update", function() {
		var e = $(this);


		// Check for "close" order...
		var autoclose = e.find("a.auto-close");
		if (autoclose.length > 0) {
			e.find('.modal-body').dialog2("close");

			var href = autoclose.attr('href');
			if (href) {
				window.location.href = href;
			}
		}
		else {
			// mark the stylesheets as not being loaded
			unloadScripts();

			$('a[rel="tooltip"]').tooltip();


			// Check the first available session
			var $sessionList = $('#webinar-session-list');
			$sessionList.find('.col-id input').eq(0).attr('checked', true);
			$sessionList.find(':radio').styler();


			// css class name for a selected list row
			var cssItemSelected = 'selected';

			// Delegated/Live event listener for show/hide session schedule panel

			$(document).on('click', '.col-name', function(){
				$(this).closest('.item').find('.col-id input').attr('checked', true);
			});


		}
	});
</script>