<?php
/**
 * This partial view renders the first box in the courses grid layout,
 *
 * which is defined as a box with a title, description, a link and a background color
 *
 * @var string $bgCss - optional (represents a css class that applies a certain background color to the 'tile' or 'box')
 * - check the css for possible css classes
 * @var string $bgColor - optional (used to set the tile's background color -  should be in the form of #0465AC)
 * @var string $title
 * @var string $description
 * @var string $link
 */
?>
<li class="tile cover pointer-off <?= (!empty($bgCss) ? $bgCss : '' ) ?>" style="<?= (!empty($bgColor) ? 'background-color: ' . $bgColor . ';' : '' ) ?><?php echo (!empty($clear) ? 'clear:both;' : '' ); ?>">
	<div class="tile-content">
		<h2 class="course-title-ellipsis"><?= CHtml::encode($title) ?></h2>
        <i class="colorable fa <?= $icon ?> fa-3x fa-inverse" style="font-size: 9em; margin-top: -0.1em !important;margin-left: 0.35em;"></i>
		<div class="course-desc">
			<p><?= strip_tags($description) ?></p>
		</div>
		<div class="right-arrow"></div>
	</div>
</li>