<?php

/* @var LearningCourse[] $courses */
/* @var string $viewMoreUrl */
/* @var string $firstBoxHtml */
/* @var string $gridType */

	// ---------------------------

	$i = 1;
	$closed = true;

	// Calculate expected tiles, including: Courses, FIRST BOX and VIEW MORE one.
	$expectedTiles = count($courses);
    if (!empty($firstBoxHtml)) {
        $expectedTiles++;
    }
    if (!empty($viewMoreUrl) && (count($courses)>0)) {
        $expectedTiles++;
    }

    // Index for Courses array
    $n = 0;

    // Run through all tiles
    for ($i=1; $i<=$expectedTiles;$i++) {

        // Open a Row?
        if ( $closed ) {
           // echo "<div class='row-fluid'>";
            $closed = false;
        }

        // FIRST BOX ??
        if( ($i==1) && !empty($firstBoxHtml) ){
            echo $firstBoxHtml;
        }
        // VIEW MORE ???
        else if (!empty($viewMoreUrl) && (count($courses)>0) && ($i == $expectedTiles)) {
            echo '
			<a href="' . $viewMoreUrl . '" class="tile bg-gradient back-cover">
				<div class="tile-content">
					<span>' . Yii::t('course','View more courses') . '</span>
				</div>
			</a>';
        }
        // Oh well, a course then....
        else {
            if (isset($courses[$n])) {
                $courseOrCoursepath = $courses[$n];
                $n++;
	            if($courseOrCoursepath instanceof LearningCourse){
		            /* @var $courseOrCoursepath LearningCourse */
                $portlet = $this->widget('common.widgets.CoursePortlet', array(
			            'courseModel'     => $courseOrCoursepath,
			            'courseDetailUrl' => Yii::app()->createUrl('course/axDetails', array('id' => $courseOrCoursepath->idCourse)),
                    'gridType' => $gridType,
                    'idUser' => Yii::app()->user->id,
                ));
	            }elseif($courseOrCoursepath instanceof LearningCoursepath){
		            $portlet = $this->widget('common.widgets.CoursepathPortlet', array(
			            'coursepathModel'     => $courseOrCoursepath,
			            'coursepathDetailUrl' => Yii::app()->createUrl('coursepath/details', array('id_path' => $courseOrCoursepath->id_path)),
			            'gridType'        => $gridType,
			            'idUser'          => Yii::app()->user->id,
		            ));
	            }elseif($courseOrCoursepath instanceof LearningCatalogueEntry){
		            // We are browsing a catalog (external or internal)

		            /* @var $courseOrCoursepath LearningCatalogueEntry */

		            if($courseOrCoursepath->type_of_entry == 'course') {
			            $portlet = $this->widget('common.widgets.CoursePortlet', array(
				            'courseModel'     => $courseOrCoursepath->course,
				            'courseDetailUrl' => Yii::app()->createUrl('course/axDetails', array('id' => $courseOrCoursepath->course->idCourse)),
				            'gridType'        => $gridType,
				            'idUser'          => Yii::app()->user->id,
			            ));
		            }elseif($courseOrCoursepath->type_of_entry == 'coursepath'){
			            $portlet = $this->widget('common.widgets.CoursepathPortlet', array(
				            'coursepathModel'     => $courseOrCoursepath->coursepath,
				            'coursepathDetailUrl' => Yii::app()->createUrl('coursepath/details', array('id_path' => $courseOrCoursepath->coursepath->id_path)),
				            'gridType'        => $gridType,
				            'idUser'          => Yii::app()->user->id,
			            ));
            }
        }
            }
        }

        if ( $i % 5 == 0 ) {
            //echo "</div>";
            $closed = true;
        }



    }


	if ( ! $closed ) {
	//	echo "</div>";
	}

?>