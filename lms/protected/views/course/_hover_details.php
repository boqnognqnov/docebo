<?php
/* @var $available_seats int */
/* @var $courseModel LearningCourse */
/* @var $stats array */
/* @var $user_level string */
/* @var $course_lang string */
/* @var $course_id int */
/* @var $userIsSubscribed bool */
/* @var $is_locked bool */
/* @var $showButton bool */



	$courseDetailsCss =  'open-dialog';
	$courseDetailsUrl = $this->createUrl('course/axDetails', array('id' => $course_id));

	if (Settings::get('show_course_details_dedicated_page') === 'on') {
		$courseDetailsCss = '';
		$courseDetailsUrl = $this->createUrl('course/details', array('id' => $course_id));
	}

    $currencySymbol = Settings::get("currency_symbol");

    $params = array();
    $params['course_id'] = $courseModel->idCourse;
    if ($resetToken) {
    	$params['reset'] = $resetToken;
    }
    $coursePlayerUrl = $this->createUrl("/player", $params);

$subscribeButtonHtml = '<a title="'.$courseModel->name.'" href="'. $courseDetailsUrl .'" data-dialog-class="course-details-modal" closeOnEscape="false" rel="dialog-course-detail-'. $course_id .'" class="'.$courseDetailsCss.' subscribe-now-btn btn docebo nice-btn with-icon rounded green">'.$label .' <span class="icon"><i></i></span></a>';

$buyButtonHtml = '<a title="'.$courseModel->name.'" href="'. $courseDetailsUrl .'" data-dialog-class="course-details-modal" closeOnEscape="false" rel="dialog-course-detail-'. $course_id .'"  class="'.$courseDetailsCss.' subscribe-now-btn btn docebo nice-btn with-icon rounded blue">'.$currencySymbol.'&nbsp;'.number_format((double)$courseModel->getCoursePrice(),2) .' &nbsp;&nbsp;'. Yii::t('userlimit', 'Buy now') .' <span class="icon"><i></i></span></a>';

$overbookButtonHtml = '<a title="'.$courseModel->name.'" href="'. $courseDetailsUrl .'" data-dialog-class="course-details-modal" closeOnEscape="false" rel="dialog-course-detail-'. $course_id .'"  class="'.$courseDetailsCss.' subscribe-now-btn btn docebo nice-btn with-icon rounded orange">'. Yii::t('course', 'OVERBOOK') .' <span class="icon"><i></i></span></a>';

Yii::app()->event->raise('BeforeCourseHoverDetailsContentRender', new DEvent($this, array(
	'userIsSubscribed' => &$userIsSubscribed,
	'showButton' => &$showButton,
	'isOverbooked' => &$isOverbooked,
	'courseModel' => &$courseModel,
	'buyButtonHtml' => &$buyButtonHtml,
	'subscribeButtonHtml' => &$subscribeButtonHtml,
	'overbookButtonHtml' => &$overbookButtonHtml,
)));

?>

<div class="course-action">
	<?php if ($userIsSubscribed) : ?>
		<?php if (!$is_locked) : ?>
		    <a href="<?= $coursePlayerUrl ?>" class="play-btn <?= ($is_locked) ? 'hidden' : '' ?>"></a>
		    <!--
			<a href="../doceboLms/index.php?modname=course&op=aula&idCourse=<?= $courseModel->idCourse ?>" class="play-btn <?= ($is_locked) ? 'hidden' : '' ?>"></a>
			 -->
		<?php else: ?>
			<div class="course-action-locked"><?=$reason?></div>
		<?php endif; ?>
	<?php else : ?>

				<?php if($showButton): ?>
					<div class="seats-available">
					<?php if (!empty($available_seats) && !$isOverbooked) : ?>
						<?= Yii::t('userlimit', '{n} seats available', array('{n}' => $available_seats)) ?>
					<?php elseif($isOverbooked): ?>
						<?= Yii::t('userlimit', '{n} on queue', array('{n}' => abs($available_seats))) ?>
					<?php endif; ?>
					</div>

					<br>
			<?php if ($isOverbooked) : ?>
						<?= $overbookButtonHtml ?>
			<?php elseif ($courseModel->selling==1) : ?>
						<?= $buyButtonHtml ?>
			<?php else: ?>
						<?= $subscribeButtonHtml ?>
			<?php endif; ?>
					<br>
				<?php else: ?>
						<div class="course-action-locked"><?=$label?></div>
				<?php endif; ?>

	<?php endif; ?>

</div>

<?php if (Yii::app()->event->raise('BeforeCourseTileHoverInfoSectionRender', new DEvent($this, array(
	'level' => $user_level,
	'stats' => $stats,
	'lang' => $course_lang,
	'time' => (!empty($courseModel->mediumTime)) ? $courseModel->formatAverageCourseTime() : ''
)))) : ?>
<div class="course-info">
	<div class="clearfix">
	<?php if (!empty($user_level)) : ?>
		<span class="label course-info-role"><?= ucfirst(strtolower($user_level)) ?></span>
	<?php endif; ?>
	</div>
	<div class="clearfix">
		<?php if ( !empty($stats)) : ?>
			<span class="label course-info-progress"><?= $stats[LearningCommontrack::STATUS_COMPLETED] .'/'. $stats['total'] ?></span>
		<?php endif; ?>
		<?php if (!empty($courseModel->mediumTime)) : ?>
			<span class="label course-info-time"><?= $courseModel->formatAverageCourseTime() ?></span>
		<?php endif; ?>
		<?php if (!empty($course_lang)) : ?>
			<span class="label course-info-category"><?= strtoupper($course_lang) ?></span>
		<?php endif; ?>
	</div>
	<?php
		switch ($courseModel->course_type) {
			case LearningCourse::TYPE_ELEARNING:
			case LearningCourse::TYPE_CLASSROOM:
			case LearningCourse::TYPE_MOBILE:
			case LearningCourse::TYPE_WEBINAR:
				$courseTypeLabel = $courseModel->getCourseTypeTranslation();
				$courseTypeClass = $courseModel->course_type;
				break;
			default:
				$courseTypeLabel = '';
				$courseTypeClass = '';
		}
	?>
	<div class="clearfix">
		<span class="label course-info-type course-type-sprite <?=$courseTypeClass?>"><?=$courseTypeLabel?></span>
	</div>
</div>
<?php endif; ?>