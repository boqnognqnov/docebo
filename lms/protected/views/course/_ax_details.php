<?php
/* @var $courseModel LearningCourse */
$currencySymbol = Settings::get("currency_symbol");
$course_id = $courseModel->idCourse;
$courseLogoUrl = $courseModel->getCourseLogoUrl();
$icon_globe_url = Yii::app()->theme->baseUrl . '/images/globe.png';
$icon_clock_url = Yii::app()->theme->baseUrl . '/images/clock.png';

$preview_arrow_url = Yii::app()->theme->baseUrl . '/images/preview_course_arrow.png';

// On-login-success redirect URLs for different 'reasons'
$buyRedirectUrl = Docebo::createAppUrl('lms:course/details', array('id' => $course_id, 'click'=>'btn-course-details-buy'));
$params = array('course_id' => $course_id, 'subscription_approved' => true);
if(isset($codes) && !empty($codes)){
	foreach ($codes as $key => $code) {
		$params[$key] = $code;
	}
}
$subscribeRedirectUrl = $this->createUrl('course/subscribe', $params);
$overbookRedirectUrl = $this->createUrl('course/overbook', array('course_id' => $course_id));

// URLs used when user is a Guest and to clicks Buy/Subscribe/etc
$toBuyLoginUrl = $this->createUrl('user/axLogin', array('reason' => 'buy', 'redirect_url' => urlencode($buyRedirectUrl)));
$guestSubscribeRedirectUrl = $this->createUrl('course/details', array('id' => $course_id, 'click'=>'btn-course-details-subscribe'));
$toSubscribeLoginUrl = $this->createUrl('user/axLogin', array('reason' => 'subscribe', 'course_id' => $course_id , 'redirect_url' => urlencode($guestSubscribeRedirectUrl)));
$toOverbookLoginUrl = $this->createUrl('user/axLogin', array('reason' => 'overbook', 'redirect_url' => urlencode($overbookRedirectUrl)));

// Buttons HTML (depend on user status)
if (Yii::app()->user->isGuest) {
	$buyButtonHtml = "<a style='color: white;' href='$toBuyLoginUrl' class='ajax btn btn-docebo big green span7 btn-course-details-buy'>" . Yii::t('userlimit', 'Buy now').($isOverbooked ? '<br />('.Yii::t('course', 'OVERBOOK').')' : '') . "</a>\n";
	$subscribeButtonHtml = "<a style='color: white;' href='$toSubscribeLoginUrl' class='ajax btn btn-docebo big green span9 btn-course-details-subscribe'>" . $label . "</a>\n";
	$overbookButtonHtml = "<a style='color: white;' href='$toOverbookLoginUrl' class='ajax btn btn-warning span9'>" . Yii::t('course', 'OVERBOOK') . "</a>\n";
} else {
    // wether to show the select session dialog
	$buyRedirectUrl = Docebo::createAppUrl('lms:course/buy', array('course_id' => $course_id));
    $showModal = (PluginManager::isPluginActive('ClassroomApp') && $courseModel->isClassroomCourse());
    $buyButtonHtml = "<a style='color: white;' href='$buyRedirectUrl' class='ajax btn btn-docebo big green span7 btn-course-details-buy'>" . Yii::t('userlimit', 'Buy now').($isOverbooked ? '<br />('.Yii::t('course', 'OVERBOOK').')' : '') . "</a>\n";
    $subscribeButtonHtml = "<a style='color: white;' href='$subscribeRedirectUrl' class='ajax btn btn-docebo big green span9 btn-course-details-subscribe'>" . $label . "</a>\n";
	$overbookButtonHtml = "<a style='color: white;' href='$overbookRedirectUrl' class='btn btn-warning span9'>" . Yii::t('course', 'OVERBOOK') . "</a>\n";
}

if ($courseModel->selling == 1 && PluginManager::isPluginActive('ShopifyApp'))
{
	$shopifySubdomain = Settings::get('shopify_subdomain', false);
	$shopifyProductMeaningfulId =
		LearningCourseShopifyProduct::model()->find('idCourse = :idCourse', [':idCourse' => $courseModel->idCourse])->shopifyProductMeaningfulId;
	$buyRedirectUrl = "http://$shopifySubdomain.myshopify.com/products/$shopifyProductMeaningfulId";
	$buyButtonHtml =
		"<a style='color: white;' href='$buyRedirectUrl' onclick=\"window.open(this.href, '_blank');\" class='ajax btn btn-docebo big green span7 btn-course-details-buy'>" . Yii::t('userlimit', 'Buy now') . "</a>\n";
}

// Preview/Demo related
if ($demoType == LearningCourse::$DEMO_MEDIA_CATEGORY_UNKNOWN) {
	$previewButtonText = Yii::t('standard', 'Preview');
	$aClass = "";
} else {
	if ($demoType === false) {
		$previewButtonText = "";
	} else {
		$previewButtonText = Yii::t('standard', 'Preview');
		$aClass = "ajax";
	}
}

?>
<!-- Consumed by Dialog2, used as Dialog Title -->
<h1><?= $courseModel->name ?></h1>

<div class="row-fluid">

	<!-- LOGO and Preview/Demo buttons -->
	<div class="span4">
		<div class="span12" style="position: relative; left: 0; top: 0;">
			<img class="thumbnail"
				 src="<?= $courseLogoUrl ?>">

			<?php if ($demoType !== false) : ?>
				<!-- Preview/Download links -->
				<a href="<?= $previewActionUrl ?>" class="<?= $aClass ?>"<?php if ($demoType == LearningCourse::$DEMO_MEDIA_CATEGORY_UNKNOWN) echo ' target="_blank"'; ?>>
					<img style="position: absolute; top: 60px; left: 90px;" src="<?= $preview_arrow_url ?>">
				</a>
				<a href="<?= $previewActionUrl ?>"
				   style="color: white; opacity: 0.8 !important; position: absolute; bottom: 20px; left: 26px;"
                   <?php if ($demoType == LearningCourse::$DEMO_MEDIA_CATEGORY_UNKNOWN) echo ' target="_blank"'; ?>
				   class="<?= $aClass ?> btn btn-docebo big green"><?= $previewButtonText ?></a>
			<?php else : ?>
				<span
					style="opacity: 0.8 !important; position: absolute; bottom: 20px; left: 15px;"><?= $previewButtonText ?></span>
			<?php endif; ?>

			<?php if ($courseModel->canViewRating()) : ?>
				<br />
				<div id="social_rating_course" class="fixed_rating details">
					<div class="span12">
						<?php echo CHtml::label(Yii::t('social_rating', 'Rating') . ":", '', array('class' => 'social_rate_title pull-left details')); ?>
					</div>
					<div class="span12" style="margin-left: 0px">
						<?php
							$this->widget('common.widgets.DoceboStarRating',array(
								'id'=>'course-rating-'.$courseModel->idCourse,
								'name'=>'course_rating_'.$courseModel->idCourse,
								'type' => 'star',
								'readOnly' => !$courseModel->canRate(Yii::app()->user->id),
								'value' => $courseModel->getRating(),
								'ratingUrl' => Docebo::createAppUrl('lms:course/axRate'),
								'urlParams' => array(
									'idCourse' => $courseModel->idCourse
								),
								'id_course' => $courseModel->idCourse,
								'id_user' => Yii::app()->user->id
							));
						?>
						<span class="rating-value fixed_rating"><?= $courseModel->getRating() ?></span>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>

	<!-- Language, Duration and <action> Button -->
	<div class="span8">

		<div class="row-fluid">
			<div class="span12 well well-small">
				<div class="span3">
					<div class="row-fluid">
                        <span class="span3">
                            <?php echo CHtml::image($icon_globe_url, "Language"); ?>
                        </span>
                        <span class="span9">
                            <a><strong><?= Yii::t('standard', '_LANGUAGE') ?></strong></a><br>
							<?= $langName ?>
                        </span>
					</div>
				</div>

				<div class="span3">
					<div class="row-fluid">
                        <?php if ($courseModel->mediumTime) : ?>
                        <span class="span3">
                            <?php echo CHtml::image($icon_clock_url, "Duration"); ?>
                        </span>
                        <span class="span9">
                            <a><strong><?= Yii::t('statistic', '_HOW_MUCH_TIME') ?></strong></a><br>
								<?= $courseModel->getRoundedCourseTimeByHoursMinutes() ?>
                        </span>
                        <?php endif; ?>
					</div>
				</div>

				<div class="span6">
					<div class="row-fluid text-right">
						<?php if ($showButton) : ?>
							<?php if ($isOverbooked && !$courseModel->selling) : ?>
								<span class="span3"></span>
								<?= $overbookButtonHtml ?>
							<?php elseif ($courseModel->selling) : ?>
								<span style='font-size: 20px; padding-top:6px;'
									  class='span5'><?= $currencySymbol ?> <?= $courseModel->getCoursePrice() ?></span>
								<?= $buyButtonHtml ?>
							<?php else : ?>
								<span class="span3"></span>
								<?= $subscribeButtonHtml ?>
							<?php endif; ?>
						<?php endif; ?>
					</div>
				</div>

			</div>
		</div>

		<div class='row-fluid'>
			<div class="native-styled">
				<?= $courseModel->description ?>
				<?php
					$current = 0;
					$addClass = '';
					if (is_array($additionalCourseFields) && !empty($additionalCourseFields)):
						foreach($additionalCourseFields as $fieldName => $fieldItem): ?>
						<?php $fieldValue = $fieldItem['value']; ?>
						<?php $inColumn = $fieldItem['in_column']; ?>
						<?php $current++; $addClass = ($current == 1) ? 'catalog-course-details-additional-fields-first' : '';?>
						<?php if (!empty($fieldValue)): ?>
							<div class="row-fluid catalog-course-details-additional-fields <?=$addClass?>">
								<?php if($inColumn):?>
									<div class="span4 catalog-course-details-additional-fields-label"><?=$fieldName?>:</div>
									<div class="span8 catalog-course-details-additional-fields-value"><?=$fieldValue?></div>
								<?php else: ?>
									<div class="catalog-course-details-additional-fields-label"><?= $fieldName ?></div>
									<div style="margin-bottom: 20px;"><?= $fieldValue ?></div>
								<?php endif; ?>
							</div>
						<?php endif; ?>
						<?php
						endforeach;
					endif;
					?>
			</div>
		</div>

	</div>

</div>
