<?
/* @var $courses array */
?>
<?=CHtml::beginForm('', 'POST', array(
	'class'=>'form ajax'
))?>

<? if(Yii::app()->user->hasFlash('error')):?>
	<div class="error">
		<?=Yii::app()->user->getFlash('error')?>
	</div>
<? endif; ?>

<?
$showSaveButton = false;
$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'course-management-grid',
	'htmlOptions' => array('class' => 'grid-view clearfix'),
	'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
	'dataProvider' => $dataProvider,
	'template' => '{items}{summary}{pager}',
	'summaryText' => Yii::t('standard', '_TOTAL'),
	'pager' => array(
		'class' => 'DoceboCLinkPager',
		'maxButtonCount' => 8,
	),
	'cssFile' => false,
	'columns' => array(
		'name',
		array(
			'type' => 'raw',
			'header' => Yii::t('standard', 'Seats'),
			'value' => function($data, $index){
				if($data['selling'] == 1)
					echo $data['available_seats'] .'/'. $data['total_seats'];
				else
					echo Yii::t('standard', '_COURSE_S_FREE');
			},
			'headerHtmlOptions' => array('class' => 'center-aligned'),
			'cssClassExpression' => '"max-available-column"'
		),
		array(
			'type' => 'raw',
			'header' => Yii::t('standard', 'Buy more seats'),
			'value' => function($data, $index) use (&$showSaveButton){
				if($data['selling'] == 1) {
					$showSaveButton = true;
					echo CHtml::numberField('course['.$data['idCourse'].']', '', array(
						'class'=>'',
						'style'=>'width: 80px;',
						'min'=>0,
					));
				}
			},
			'headerHtmlOptions' => array(
				'class' => 'center-aligned',
				'style'=>'width:100px;',
			)
		),
	),
));
?>

	<div class="form-actions">
		<?php if($showSaveButton):?>
		<input class="btn-docebo green big" id="block-edit-save-button" type="submit"
		       value="<?php echo Yii::t('standard', '_SAVE'); ?>">&nbsp;&nbsp;
		<?php endif;?>
		<input class="btn-docebo black big close-dialog" type="button" value="<?php echo Yii::t('standard', '_CANCEL'); ?>">
	</div>
<?=CHtml::endForm()?>