<?php
$this->breadcrumbs[] = Yii::t('course_autoregistration', '_AUTOREGISTRATION');
?>

<?php if ($errorMessage) : ?>

	<div class="row-fluid">
		<div class="span12 alert alert-error">
			<?php echo $errorMessage; ?>
		</div>
	</div>

<?php else : ?>

	<?php echo CHtml::beginForm($this->createUrl('autoreg'), 'POST', array(
		'class' => "form-inline"
	)); ?>
	<?php if ($feedback) : ?>
		<div class="alert alert-error">
			<?= $feedback ?>
		</div>
	<?php endif; ?>
 
    <h3 class="title-bold"><?= Yii::t('course_autoregistration', '_AUTOREGISTRATION') ?></h3>

	<div class="row-fluid">

		<div class="span12">

			<div class="autoreg-form text-center">

				<div class="control-group">
					<label class="autoreg-note">
						<?= Yii::t('course_autoregistration', 'Type a valid code and submit. You will be automatically subscribed to the course associated to it.') ?>
						&nbsp;
					</label>
				</div>

				<div class="control-group">
					<?= CHtml::textField('autoreg_code', '', array('class' => 'autoreg-code', 'placeholder' => 'Type code here...')) ?>
					<?= CHtml::submitButton(Yii::t('course_autoregistration', '_AUTOREGISTRATION'), array(
						'class' => 'btn-docebo green big'
					)) ?>
				</div>

			</div>

		</div>

	</div>

	<?php echo CHtml::endForm(); ?>

<?php endif; ?>