<?php
/* @var $this CourseController */
/* @var $course LearningCourse */
/* @var $message string */
/* @var $already_subscribed bool */
?>

<h1><?= CHtml::encode($course->name) ?></h1>

<?php
$this->widget('lms.protected.modules.player.components.widgets.CourseHeader', array(
    'course' => $course
));
?>
<?php if($message): ?>
	<h3><?= $message ?></h3>
<?php endif;?>
<?php if(isset($already_subscribed) && $already_subscribed) : ?>
	<br />
	<h4>
		<?= Yii::t('course', '_SUBSCRIBED_T'); ?>

		<a href="<?= Docebo::createAbsoluteUrl('lms:player', array('course_id' => $course->idCourse)); ?>">
			<span class="btn btn-docebo big blue span2" style="float:none;">
				<span class="icon" style="float:left;font-size:18px;">
					<i class="fa fa-play-circle-o"></i>
				</span>

				<span class="action-label" style="float:right;font-size:13px;"><?= Yii::t('standard', '_PLAY'); ?></span>
			</span>
		</a>
	</h4>
<?php endif; ?>

<div class="form-actions text-right">
    <input type="reset" class="close-dialog btn-docebo black big" value="<?php echo Yii::t('standard', '_CLOSE'); ?>" />
</div>

<script type="text/javascript">
    var unloadCss = function() {
        var incomingScripts = $('.modal').find('link[rel="stylesheet"]');
        for (var i = 0, len = incomingScripts.length; i < len; i++) {
            if ($._loadedScripts[incomingScripts[i].href]) {
                $._loadedScripts[incomingScripts[i].href] = 0;
            }
        }
    }
    $(document).ready(function() {
        unloadCss();
    });
</script>