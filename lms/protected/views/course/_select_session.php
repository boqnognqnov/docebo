<?php
/* @var $this CourseController */
/* @var $course LearningCourse */
/* @var $form CActiveForm */
/* @var $dataProvider CActiveDataProvider */
?>

<style type="text/css">
    #classroom-sessions-list .col-start,
    #classroom-sessions-list .col-end {
        width: 150px;
    }
</style>

<h1><?= CHtml::encode($course->name) ?></h1>

<?php
$this->widget('lms.protected.modules.player.components.widgets.CourseHeader', array(
    'course' => $course
));
?>

<div class="classroom-sessions-grid-container">
    <div class="inner">
        <h3 class="title-bold"><?= Yii::t('classroom', 'Enroll to a session') ?></h3>
        <p><?= Yii::t('classroom', 'Select session') ?></p>

        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'classroom-sessions-grid-form',
            'method' => 'POST',
            'htmlOptions' => array(
                'class' => 'docebo-form ajax'
            ),
        )); ?>

        <div class="classroom-sessions-grid">
			<?php
			if(isset($codes) && !empty($codes)){
				foreach($codes as $key=>$code){ ?>
					<input type="hidden" value="<?= $code ?>" name="<?= $key ?>"/>
				<?php }
			}
			?>

            <?php $this->widget('DoceboCListView', array(
                'id' => 'classroom-sessions-list',
                'htmlOptions' => array('class' => 'docebo-list-view'),
                'template' => '{header}{items}{pager}',
            	'pager' => array(
            		'class' => 'adminExt.local.pagers.DoceboLinkPager',
            	),
           		'afterAjaxUpdate'	=> "function(id,data) {
					$('.classroom-sessions-grid-container :radio').styler();
            		$('a[rel=\"tooltip\"]').tooltip();
				}",
            		
                'dataProvider' => $dataProvider,
                'itemView' => '_enrollListView',
                'columns' => array(
                    array(
                        'name' => 'id',
                    ),
                    array(
                        'name' => 'name',
                        'header' => Yii::t('standard', '_NAME')
                    ),
                    array(
                        'name' => 'start',
                        'header' => Yii::t('standard', '_START')
                    ),
                    array(
                        'name' => 'end',
                        'header' => Yii::t('standard', '_END')
                    ),
                    array(
                        'name' => 'location',
                        'header' => Yii::t('classroom', 'Location name'),
                    ),
                    array(
                        'name' => 'toggle_handle',
                    )
                )
            )); ?>

        </div>

        <br>
        <div class="form-actions text-right">
            <input class="btn-docebo green big" type="submit" value="<?php echo Yii::t('standard', '_SUBSCRIBE'); ?>" name="subscribeToCourse"/>
            <input type="reset" class="close-dialog btn-docebo black big" value="<?php echo Yii::t('standard', '_CANCEL'); ?>" />
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div>

<script type="text/javascript">
    var unloadScripts = function() {
        var incomingScripts = $('.modal').find('script[src],link[rel="stylesheet"]');
        for (var i = 0, len = incomingScripts.length; i < len; i++) {
            if ($._loadedScripts[incomingScripts[i].href]) {
                $._loadedScripts[incomingScripts[i].href] = 0;
            }
        }
    }

    // Upon dialog2 content update....
    $(document).delegate(".course-details-modal", "dialog2.content-update", function() {
        var e = $(this);


        // Check for "close" order...
        var autoclose = e.find("a.auto-close");
        if (autoclose.length > 0) {
            e.find('.modal-body').dialog2("close");

            var href = autoclose.attr('href');
            if (href) {
                window.location.href = href;
            }
        }
        else {
            // mark the stylesheets as not being loaded
            unloadScripts();

            $('a[rel="tooltip"]').tooltip();


			// Check the first available session            
            var $sessionList = $('#classroom-sessions-list');
            $sessionList.find('.col-id input').eq(0).attr('checked', true);
            $sessionList.find(':radio').styler();
            

            // css class name for a selected list row
            var cssItemSelected = 'selected';

            // Delegated/Live event listener for show/hide session schedule panel
            $(document).on('click', '.toggle-handle', function(){
                var $item = $(this).closest('.item');

                var doWhat = $item.hasClass(cssItemSelected)
                    ? 'retract-row'
                    : 'expand-row';

				if (doWhat == 'retract-row') {
                    var $handle = $(this);
                    var $target = $item.find('.secondary');

                    $item.removeClass(cssItemSelected);
                    $handle
                        .removeClass($handle.data('expand-class'))
                        .addClass($handle.data('retract-class'));
                    $target.hide();
				}

				if (doWhat == 'expand-row') {
                    var $handle = $(this);
                    var $target = $item.find('.secondary');

                	// retract all other rows
                	$item.parent().find('.item').siblings().trigger('retract-row');

                	$item.addClass(cssItemSelected);
                	$handle
                    	.removeClass($handle.data('retract-class'))
                    	.addClass($handle.data('expand-class'));
                	$target.slideDown().removeClass('hide');
                	$item.find('.col-id input').attr('checked', true);
				}
				
                
                
			});


            $(document).on('click', '.col-name', function(){
            	$(this).closest('.item').find('.col-id input').attr('checked', true);
            });
			
            
        }
    });
</script>