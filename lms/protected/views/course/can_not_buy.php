<?php if (Yii::app()->user->hasFlash('error')) : ?>
<div class='row-fluid'>
    <div class="span12 alert alert-error">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
</div>
<?php endif; ?>


<!-- --- JS ------------------------------------------------------------------------------------------------------ -->


<script type="text/javascript">
/*<![CDATA[*/

    setTimeout(function() {
        window.location.href = '<?= $redirectUrl ?>';
    }, 5000);           
           
/*]]>*/
</script>
