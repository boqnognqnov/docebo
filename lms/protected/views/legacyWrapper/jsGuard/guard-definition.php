<script type="text/javascript">
    var LegacyWrapperJsGuard = function() {
        return {
            isLoginPage: <?= $isLoginPage ? 'true' : 'false' ?>,
            appendCss: function(content) {
                var style = document.createElement("style");
                style.innerHTML = content;
                document.getElementsByTagName("head")[0].appendChild(style);
            },

            hydraBridge : function()
            {
                // Detect if the request is loaded in an iframe
                if(window.self !== window.top) {
					<?php if(Yii::app()->legacyWrapper->needsReboot()): ?>
                        // Reboot Hydra
                        <?= Yii::app()->legacyWrapper->triggerReboot(false); ?>
					<?php elseif (Yii::app()->user->isGuest): ?>
					    <?= Yii::app()->legacyWrapper->setHydraRoute("/learn", false) ?>
					<?php else: ?>
                        <?php if(Yii::app()->legacyWrapper->checkRequestHasHydraEquivalent()): ?>
                        // Redirect to Hydra FE equivalent page if exists
                        this.redirectToHydraPage();
                        <?php endif; ?>

                        // Inject stylesheet to override styles in embedded mode
                        this.appendCss(<?= json_encode(file_get_contents(Yii::app()->theme->basePath.'/css/hydra.css')) ?>);

                        // If parent window is on a different route, update it to reflect this loaded page
                        <?php $goToUrl = Yii::app()->urlManager->convertToHydraUrl() ?>
                        var gotoUrl = '<?= $goToUrl ?>';
                        var parent  = decodeURIComponent(window.parent.location.pathname + window.parent.location.search);

                        if (gotoUrl != parent && decodeURIComponent(gotoUrl) != parent) // Sometimes the gotoUrl comes unencoded
                            <?= Yii::app()->legacyWrapper->setHydraRoute($goToUrl, false) ?>
					<?php endif; ?>
                } else {
					<?php if(!Yii::app()->legacyWrapper->hydraFrontendEnabled()): ?>
                        // We're here without hydra being enabled. Check if we're actually in preview mode and, if not, bridge has nothing to do.
                        <?php $hostname = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME'];?>
                        var themePreviewKey = <?= json_encode('preview.theme.'.$hostname.(!empty($_GET['domain'])? ('.'.$_GET['domain']) : '')) ?>;
                        if((typeof localStorage == 'undefined') || (!localStorage.getItem(themePreviewKey)))
                            return;
                    <?php endif; ?>
                        // Legacy LMS is being opened outside the iframe (non embedded)
                        // We need to decide where to redirect to
                        // lasturl - Redirect the user to the previous requested link. Add link to localStorage to allow Hydra to use it.
                        <?php if(isset(Yii::app()->session['lsasturl']) && Yii::app()->session['lasturl'] !== '' && Yii::app()->legacyWrapper->hydraFrontendEnabled()): ?>
                            // **** TEMPORARILY DISABLE THIS BECAUSE IT CREATES AN ANNOYING REDIRECT ****
                            // **** REENABLE IT WHEN FIXED THE REDIRECT ISSUE ****
                             localStorage.setItem("original_url", <?= json_encode(Yii::app()->request->getHostInfo()) ?> + "<?=Yii::app()->session['lasturl']?>");
                            <?php unset(Yii::app()->session['lasturl']);?>
                        <?php endif; ?>

                    window.location = <?= json_encode(Yii::app()->request->getHostInfo()) ?>+
                        <?= json_encode(!empty($_GET['domain'])? ('/'.$_GET['domain']) : '') ?>
                        +(this.isLoginPage ? '/learn' : '<?= Yii::app()->urlManager->convertToHydraUrl()?>');
                }
            },

            redirectToHydraPage : function()
            {
                // Redirecting users from the legacy links to the Hydra Frontend
				<?= Yii::app()->legacyWrapper->setHydraRoute(Yii::app()->urlManager->convertToHydraUrl(), false) ?>
            },

            updateNotificationsCounter : function()
            {
                // Updating notifications counter in the Hydra Frontend
                <?= Yii::app()->legacyWrapper->updateNotificationsCounter() ?>
            }
        };
    };

    var guard = LegacyWrapperJsGuard();
    guard.hydraBridge();
</script>
