<?php

/**
 * This is the model class for table "salesforce_running_process".
 *
 * The followings are the available columns in table 'salesforce_running_process':
 * @property integer $process_id
 * @property integer $status
 * @property string $start_date
 * @property string $end_date
 * @property string $process_data
 * @property string $process_name
 * @property integer $user_id
 */
class SalesforceRunningProcess extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'salesforce_running_process';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('process_name', 'required'),
			array('status, user_id', 'numerical', 'integerOnly'=>true),
			array('process_name', 'length', 'max'=>255),
			array('start_date, end_date, process_data', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('process_id, status, start_date, end_date, process_data, process_name, user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'process_id' => 'Process',
			'status' => 'Status',
			'start_date' => 'Start Date',
			'end_date' => 'End Date',
			'process_data' => 'Process Data',
			'process_name' => 'Process Name',
			'user_id' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('process_id',$this->process_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('start_date',$this->start_date,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('process_data',$this->process_data,true);
		$criteria->compare('process_name',$this->process_name,true);
		$criteria->compare('user_id',$this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SalesforceRunningProcess the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
