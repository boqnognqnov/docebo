<?php
/**
 * PHP Script to run yiic command in a "domain-aware" environment.
 * Works also if configuration is loaded from Redis.
 * 
 * Normally, Yii command is run in a way that a single database can be defined.
 * For LMS we need a way to load different databases, based on the domain (passed as the FIRST parameter of the script).
 * That's what both scripts do for us.
 * 
 * This script MUST be loaded by a invoker shell/batch script setting environment variable LMS_DOMAIN, e.g.
 * 
 *  	set LMS_DOMAIN=my.domain.com
 *  
 *  Invokers:
 *  	Windows:  yiic-domain.bat
 *  	Unix: yiic-domain.sh 
 *  
 * 
 */

require_once(__DIR__. "/../../common/config/config_helper.php");

defined('YII_APP_RUN_TYPE') or define('YII_APP_RUN_TYPE', 'console');

// This define is used by common/config/console.php to set the path where LOGS will be saved
// Comment it if you want logs to be saved PER DOMAIN 
define('RUNTIME_PATH', __DIR__ . '/runtime');

// Get the domain from environment and 'fake' the HTTP_HOST used in Yii 
// config files to determine the calling domain (including Redis config loader!)
$_SERVER['HTTP_HOST'] = @getenv('LMS_DOMAIN'); 

// Load config (console specific)
$config = require(dirname(__FILE__).'/config/console.php');

// BEGIN: We move code from    /../../yii/yiic.php
// so that we can try/catch the Application creation process, basically to catch DB issues
defined('STDIN') or define('STDIN', fopen('php://stdin', 'r'));

require_once(dirname(__FILE__).'/../../yii/yii.php');

// Define Yii aliases
require(_base_ . '/common/config/parts/alias.php');


// Try/catch App creation so we can catch DB issues in advance
try {
    if(isset($config))
    {
        $app=Yii::createConsoleApplication($config);
        $app->commandRunner->addCommands(YII_PATH.'/cli/commands');
    }
    else {
        $app=Yii::createConsoleApplication(array('basePath'=>dirname(__FILE__).'/../../yii/cli'));
    }
    
    $env=@getenv('YII_CONSOLE_COMMANDS');
    
    if(!empty($env)) {
        $app->commandRunner->addCommands($env);
    }
    
    // Now, run the App
    $app->run();
    
    
}
catch (CDbException $e) {
    if(YII_DEBUG === true){
        print_r($e->getTraceAsString());
    }
    else {
        echo $e->getMessage();
    }
}
