<?php
/**
 * NOTE: Videoconferencing has been moved in the new Player and almost all methods here are obsolete/not used.
 * Used methods are (as of 10 May 2013):
 * 	actionJoinMeeting,
 * 	axHandleRoom
 * 	validateRoom
 * 	webexCallback
 * 	teleskillCallback
 *
 * But it deifnitely needs some cleaning... :-)
 *
 * -- Later:
 * -- Calendar (?)
 * -- Booking system (?)
 * -- Optionally meeting recordings
 *
 */
class VideoconferenceController extends Controller {

    /**
     * (non-PHPdoc)
     * @see CController::filters()
     */
	public function filters() 	{
	    return array(
	            'accessControl', // perform access control for CRUD operations
	    );
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {
	    return array(

	        array('allow',
	            'actions' => array('webexCallback', 'teleskillCallback'),  // call back from Cisco Webex; no way to be logged in (?)
	            'users'=>array('*'),
            ),


            array('allow',
                    'actions' => array('index','axHandleRoom', 'joinMeeting', 'notifyCourseSubscribers', 'webexCallback', 'teleskillCallback', 'AxGetToolAccounts', 'AxDeleteRoom'),
                    'users'=>array('@'),
            ),

            array('deny',
                    'users'=>array('*'),
            		'deniedCallback' => array($this, 'accessDeniedCallback'),
            ),
	    );
	}

	/**
	 * Show grid, list of rooms.
	 *
	 */
	public function actionIndex() {

		DatePickerHelper::registerAssets();

	    // get the Conf. Systems App activated and enabled
	    $availableConfSystems = $this->getConferenceSystemsInfo();


	    // Render warning if no single Conferencing app is availble.
	    $numConfSystems = count($availableConfSystems);
	    if( $numConfSystems <= 0) {
	        $this->render('no_conf_app_available');
	        Yii::app()->end();
	    }

        $course_id = (int) Yii::app()->request->getParam('course_id', false);

        $user_id = Yii::app()->user->id;

        // Check for valid course id (int and not 0)
        if ( $course_id <= 0 ) {
            throw new CHttpException(404, Yii::t('standard', '_OPERATION_FAILURE'));
        }

        $courseUserModel = LearningCourseuser::model()->findByAttributes(array("idUser" => $user_id, "idCourse" => $course_id));
        $isSubscribed = !empty($courseUserModel);
        if ( ! $isSubscribed ) {
            throw new CHttpException(404, Yii::t('standard', 'You must be subscribed to this course.'));
        }

        // Get course model
        $courseModel = LearningCourse::model()->findByPk($course_id);

        // Get user model
        $userModel = CoreUser::model()->findByPk($user_id);

        // Some actions are restricted to Course admin only
        $level = $courseUserModel->level;
        $allowAdminOperations = $level > LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR;

        // get Auth manager instance
        $authManager = Yii::app()->authManager;

        // Get Active and History Rooms data providers
        $dataProviderActive = WebinarSession::model()->getRoomsListDataProvider($course_id, ConferenceRoom::STATUS_ACTIVE);
        $dataProviderHistory = WebinarSession::model()->getRoomsListDataProvider($course_id, ConferenceRoom::STATUS_HISTORY);


        $this->render('index', array(
                "userModel" => $userModel,
                "courseModel" => $courseModel,
                "courseUserModel" => $courseUserModel,
                "allowAdminOperations" => $allowAdminOperations,
                "authManager" => $authManager,
                "dataProviderActive" => $dataProviderActive,
                "dataProviderHistory" => $dataProviderHistory,
        ));


	}



	/**
	 * Redirect user to a meeting. Do some checks first.
	 * Redirect back to course/home if something is wrong.
	 *
	 */
	public function actionJoinMeeting() {

	    $roomId = (int) Yii::app()->request->getParam('room_id', '');
		$day = Yii::app()->request->getParam('day', '');

		// Get the first available date for this session
		// the Video conference widget only supports single-date sessions for now
		$sessionDate = WebinarSessionDate::model()->with('webinarSession')->findByPk(array(
			'id_session'=>$roomId,
			'day' => $day
		));

	    $redirectUrl = $sessionDate->getWebinarUrl(true);

	    // Strange, should not happen at this stage.. but..
	    if ($redirectUrl === false) {
	        $this->redirect($this->createUrl("site/index"), true);
	        Yii::app()->end();
	    }

	    $this->redirect($redirectUrl, true);
	    Yii::app()->end();
	}



	/**
	 * Notify (email) all users entitled to be notified
	 *
	 */
    public function actionNotifyCourseSubscribers() {

        // Get room info
        $roomId = (int) Yii::app()->request->getParam('room_id', '');
        $roomModel = WebinarSession::model()->findByPk($roomId);

        // @TODO check if This Yii user is an admin of the course (level > student)

        // @TODO Get the room, get the course it is associated with,  get all subscribers,
        // get their emails... loop and send email... text of the email to be defined
        // Check booking list as well, if any

    }


    /**
     * Called by Webex in certain events (error joining, after attendee joins, etc.).
     * Set in Conference manager as a Callback Url
     *
     */
    public function actionWebexCallback() {
        // Standard Webex URL parameters
        $action     = Yii::app()->request->getParam('AT', '');
        $status     = Yii::app()->request->getParam('ST', '');
        $reason     = Yii::app()->request->getParam('RS', '-');

        // Added by our callback
        $courseId   = (int) Yii::app()->request->getParam('course_id', '');
        
        switch ($action) {
			case 'EN': // coming from enroll event
				if(($status == 'SUCCESS') || ($reason == 'AlreadyEnrolled')) {
					$apiClient = new WebexApiClient();
					$roomId = Yii::app()->request->getParam('room_id', false);
					if($roomId) {
						$roomModel = WebinarSession::model()->findByPk($roomId);
						/**
						 * @var $roomModel WebinarSession
						 */
						$meetingDetails = CJSON::decode($roomModel->tool_params);
						if($meetingDetails) {
							$user = Yii::app()->user->loadUserModel();
							$firstname = trim($user->firstname) ? trim($user->firstname) : ("User ".$user->idst);
							$lastname = trim($user->lastname) ? trim($user->lastname) : "Lastname";
							$join_url = $apiClient->getAttendeeJoinEventUrl($meetingDetails['session_id'], $firstname, $lastname, $user->email);
							$callBack = urlencode(Yii::app()->createAbsoluteUrl("videoconference/webexCallback", array("course_id" => $roomModel->course_id)));
							$join_url .= "&BU=$callBack";
							$this->redirect($join_url, true);
							Yii::app()->end();
						}
					}
				}
			case 'JE':  // coming from join event
            case 'JM':  // coming from join meeting
            case 'JS':  // coming from join meeting
            	if ($status != 'SUCCESS') {
                    Yii::app()->user->setFlash('error', 'WebEx Server Error: ' . Yii::t('conference', $reason));
                }
                break;
        }

        // Redirect user to course or to home page
        if ($courseId > 0) {
            $this->redirect($this->createUrl("index", array("course_id" => $courseId)), true);
        }
        else {
            $this->redirect($this->createUrl("site/index"), true);
        }

        Yii::app()->end();
    }



    /**
     *
     */
    public function actionTeleskillCallback() {

        $log = print_r($_REQUEST,true);

        Yii::log($log, 'debug', __CLASS__);

        Yii::app()->end();

    }

	/**
	 * Create/Edit Conferencing rooms
	 *
	 */
	public function actionAxHandleRoom() {

		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/webinar.css');
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/admin.css');

		$request = Yii::app()->request;
		$room_id = $request->getParam('room_id', false);
		$roomModel = null;
		$course_id = $request->getParam('course_id', false);

		/** @var WebinarSession $roomModel */
		if($room_id){
			$roomModel = WebinarSession::model()->findByPk($room_id);
		} else{
			$roomModel = new WebinarSession('create');
		}

		// Get the first available date for this session (the video conferencing widget
		// doesn't support multi-date webinar sessions yet)
		/** @var WebinarSessionDate $dateModel */
		$dateModel = WebinarSessionDate::model()->findByAttributes(array(
			'id_session'=>$room_id
		));
		if(!$dateModel) {
			$dateModel = new WebinarSessionDate();
		}
		$dateModel->setScenario('addTogetherWithSession');

		/**
		 * @var $roomModel WebinarSession
		 */

		if($course_id){
			$roomModel->course_id = $course_id;
		}

		if(isset($_POST['WebinarSession']) && isset($_POST['WebinarSessionDate'])){

			$roomModel->attributes = $_POST['WebinarSession'];
			$dateModel->setAttributes($_POST['WebinarSessionDate']);
			$dateModel->name = $roomModel->name;
			if($dateModel->webinar_tool==WebinarSession::TOOL_CUSTOM) {
				$dateModel->webinar_custom_url = $_POST['WebinarSessionDate']['webinar_custom_url'];
			}

			$dateModel->timezone_begin = Yii::app()->localtime->getLocalDateTimeZone()->getName();
			if(isset($_POST['webinar_id_tool_account']) && $_POST['webinar_id_tool_account']) {
				$dateModel->id_tool_account = $_POST['webinar_id_tool_account'];
			}

			if(isset($_POST['webex_password']) && $_POST['webex_password']){
				//$dateModel->webinar_tool_params = array('webex_password'=>$_POST['webex_password']);
				$toolparams = CJSON::decode($dateModel->webinar_tool_params);
				$toolparams['webex_password'] = $_POST['webex_password'];
				$dateModel->webinar_tool_params = $toolparams;
			}

			$dateModel->day = Yii::app()->localtime->fromLocalDate($dateModel->day);

			try{
				if($roomModel->save()){
					$dateModel->id_session = $roomModel->id_session;

					if($dateModel->save()){
						$sessionDateRecording = WebinarSessionDateRecording::model()->findByAttributes(array(
							'id_session' => $dateModel->id_session,
							'day' => $dateModel->day
						));
						if(empty($sessionDateRecording)){
							$sessionDateRecording = new WebinarSessionDateRecording();
							$sessionDateRecording->id_session = $dateModel->id_session;
							$sessionDateRecording->day = $dateModel->day;
							$sessionDateRecording->id_course = $course_id;
							$sessionDateRecording->type = WebinarSessionDateRecording::RECORDING_TYPE_NONE;
							$sessionDateRecording->path = '-';
							$sessionDateRecording->status = WebinarSessionDateRecording::RECORDING_STATUS_NON_EXISTANT;
							$sessionDateRecording->save();
						}

						echo '<a class="auto-close"></a>';
						Yii::app()->end();
					}else{
						Yii::log(var_export($dateModel->errors,true), CLogger::LEVEL_ERROR);
					}
				}else{
					$dateModel->validate(); //so we can show errors on this model also if there are any
					Yii::log(var_export($roomModel->getErrors(),true), CLogger::LEVEL_ERROR);
				}
			} catch(Exception $e){
				Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
				$this->renderPartial('lms.protected.modules.webinar.views._common._errorMessage', array(
						'message' => $e->getMessage()
				));
				Yii::app()->end();
			}
		}

		$this->renderPartial('_handle_room', array(
			'roomModel' => $roomModel,
			'dateModel'=>$dateModel,
		), false, true);
		Yii::app()->end();
	}

	public function actionAxGetToolAccounts(){
		$request = Yii::app()->request;

		$tool = $request->getParam('tool', false);

		if($tool){
			$listData = CHtml::listData(WebinarSession::accountListByTool($tool), 'id_account', 'name');

			if(!empty($listData)){
				echo CJSON::encode($listData);
			}
		}
	}

	public function actionAxDeleteRoom(){

		$request = Yii::app()->request;

		$roomId = $request->getParam('room_id', false);

		if($roomId){
			$model = WebinarSession::model()->findByPk($roomId);

			if($model instanceof WebinarSession){


				if(isset($_POST['deleteConfirmed']) && $_POST['deleteConfirmed'] == 1){
					try{
						if($model->deleteSession()){
							echo "<a class='auto-close'></a>";
							Yii::app()->end();
						}
					} catch(Exception $e){
						Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
						echo "<a class='auto-close'></a>";
						Yii::app()->end();
					}
				}
				$this->renderPartial('_delete_room', array(
					'model' => $model
				));
				Yii::app()->end();
			}
			echo "<a class='auto-close'></a>";
			Yii::app()->end();
		}
		echo "<a class='auto-close'></a>";
		Yii::app()->end();
	}

	/**
	 * Returns true if details are valid, false otherwise
	 *
	 * @param string $conferenceName
	 * @param string $startDateTime
	 * @param string $nowDateTime
	 * @return boolean
	 */
	private function validateRoom($accountId, $roomType, $conferenceName, $startDateTime, $nowDateTime, $duration) {

	    if(empty($conferenceName)) {
	        Yii::app()->user->setFlash('error', Yii::t('standard', 'Please fill all required fields'));
	        return false;
	    }

	    if(Yii::app()->localtime->isGt($nowDateTime, $startDateTime)) {
	        Yii::app()->user->setFlash('error', Yii::t('standard', 'Invalid starting date or time.'));
	        return false;

	    }

        // Check if the room has overlapping dates with an old rooms but only for the Adobe because in the widget only Adobe has accounts
        if($roomType == ConferenceManager::CONFTYPE_ADOBECONNECT){
            $hasOverlapping = ConferenceManager::manageAdobeRoomDateOverlapping($accountId, $startDateTime, $duration);
            if($hasOverlapping === true)
                return false;
        }

	    return true;
	}


	/**
	 * Call conference manager and get information about AVAILABLE conferencing systems (i.e. activated && enabled)
	 *
	 * @return array
	 */
	private function getConferenceSystemsInfo() {
	    $result = array();
	    $result = WebinarTool::getAllTools();
	    return $result;
	}


    /**
     * Called by the CGridView in the index.php view to render the 'email' column
     * @param array $row
     * @param int $index
     * @return string
     */
    protected function gridRenderEmailColumn($row, $index)
    {
        $link = Yii::app()->createUrl('videoconference/notifyCourseSubscribers', array('room_id'=>$row['id']));
        return '<a href="'.$link.'" rel="tooltip" title="'.CHtml::encode('Notify subscribers about this meeting').'"><i class="icon-envelope"></i></a>';
    }

    /**
     * Called by the CGridView in the index.php view to render the 'type' column
     * @param array $row
     * @param int $index
     * @return string
     */
    protected function gridRenderTypeColumn($row, $index)
    {
        return ConferenceManager::$appMap[$row['tool']];
    }

    /**
     * Called by the CGridView in the index.php view to render the 'name' column
     * @param array $row
     * @param int $index
     * @return string
     */
    protected function gridRenderNameColumn($row, $index)
    {
        return '<a class="nolink" href="#" onClick="return false;" rel="popover" data-trigger="hover" title="'.CHtml::encode($row['name']).'" data-content="'.CHtml::encode($row['description']).'">'.CHtml::encode($row['name']).'</a>';
    }

    /**
     * Called by the CGridView in the index.php view to render the 'starting time & date' column
     * @param array $row
     * @param int $index
     * @return string
     */
    protected function gridRenderStarttimeColumn($row, $index)
    {
        return $row['date_begin'];
    }

    /**
     * Called by the CGridView in the index.php view to render the 'meeting hour' column
     * @param array $row
     * @param int $index
     * @return string
     */
    protected function gridRenderMeetingHoursColumn($row, $index)
    {
		/**
		 * @var $row WebinarSession
		 */
        return $row->getTotalHours();
    }

}