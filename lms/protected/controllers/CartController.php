<?php

require_once('../common/vendors/stripe/init.php');
require_once('../common/vendors/stripe/lib/Stripe.php');
require_once('../common/vendors/stripe/lib/Charge.php');
require_once('../common/vendors/stripe/lib/Customer.php');
require_once('../common/vendors/stripe/lib/Product.php');
require_once('../common/vendors/stripe/lib/SKU.php');
require_once('../common/vendors/stripe/lib/Order.php');
require_once('../common/vendors/stripe/lib/Coupon.php');

use Stripe\Charge;
use Stripe\Customer;
use Stripe\Product;
use Stripe\SKU;
use Stripe\Order;
use Stripe\Coupon;

/**
 *
 * @author
 *
 *
 */
class CartController extends Controller {

    /**
     * (non-PHPdoc)
     * @see CController::init()
     */
    public function init() {
        parent::init();
    }


    /**
     * (non-PHPdoc)
     * @see CController::filters()
     */
    public function filters() {
        return array(
            'accessControl',
        );
    }


    /**
     * (non-PHPdoc)
     * @see CController::accessRules()
     */
    public function accessRules() {
        return array(

            array('allow',
                'actions' => array('ipnPaypal', 'ipnPaypalAdaptive', 'ipnAuthnet', 'authnetRelayResponse', 'adyenResponse', 'testCompleteCheckout'),
                'users'=>array('*'),
            ),

            array('allow',
                'actions' => array('index', 'axRemove', 'axCheckout', 'embeddedCheckout', 'cancelled', 'finished', 'stripeFinished', 'axApplyDiscount', 'axEditSeatsToBuy', 'axStripeCharge', 'validateCybersource', 'declineCyberSource'),
                'users'=>array('@'),
            ),

            array('deny',
                'users'=>array('*'),
            	'deniedCallback' => array($this, 'accessDeniedCallback'),
            ),
        );
    }



	public function beforeAction($action) {
		//Add some specific log. Due to some issue with Paypal, we need more logs for its transactions
		switch ($action->id) {
			case 'axCheckout':
				$payment_method = Yii::app()->request->getParam('payment_method', "");
				if ($payment_method == Payment::$PAYMENT_METHOD_PAYPAL) {
					$this->_log('PAYPAL TRANSACTION LOG. Action = '.$action->id, $_POST);
				}
				break;
			case 'cancelled':
				$transaction_id = Yii::app()->request->getParam('id_trans', "");
				$this->_log('TRANSACTION CANCELLED. Id Transaction = '.$transaction_id, $_POST);
				break;
			case 'finished':
				$transaction_id = Yii::app()->request->getParam('id_trans');
				$this->_log('TRANSACTION FINISHED. Id Transaction = '.$transaction_id, $_POST);
				break;
			case 'ipnPaypal':
				$this->_log('PAYPAL TRANSACTION LOG. Action = '.$action->id, $_POST);
				break;
		}
		return true;
	}


	/**
	 * Generic logger for ecommerce transaction. Format a specific text and data in a predefined templace and make it
	 * appear in the logger.
	 *
	 * @param $message
	 * @param bool $data
	 */
	protected function _log($message, $data = false) {
		$output = '=== ECOMMERCE TRANSACTION TRACKING LOG. ===';
		$message = trim($message);
		if (!empty($message)) {
			$output .= "\n".$message;
		}
		if (is_array($data) && !empty($data)) {
			$output .= "\n".'DATA CHECK:';
			foreach ($data as $key => $value) {
				$output .= "\n".' - '.$key.' : '.(is_array($value) ? implode(', ', $value) : $value);
			}
		}
		Yii::log($output, CLogger::LEVEL_WARNING);
	}


    /**
     * Show Shopping cart (checkout)
     *
     */
    public function actionIndex() {

        // URL to remove a position (ajax)
        $removeUrl = Docebo::createLmsUrl('cart/axRemove');

        $countries = CoreCountry::model()->findAll(array('order' => 'name_country'));

        $idUser = Yii::app()->user->id;

        $billingInfo = CoreUserBilling::model()->getUserLastBillingInfo($idUser);
        $userInfo = CoreUser::model()->findByPk($idUser);

        $event = new DEvent($this, array(
            'removeUrl' => $removeUrl,
            'countries' => $countries,
            'binfo' => &$billingInfo,
            'uinfo' => $userInfo,
        ));

		// Trigger event to let plugins perform custom actions before rendering the shopping cart
		Yii::app()->event->raise('OnBeforeShoppingCartRender', $event);
        if($event->shouldPerformAsDefault() && !empty($event->return_value)){
            echo $event->return_value;
        }

        /* @var $shoppingCart EShoppingCart */
        $shoppingCart = Yii::app()->shoppingCart;

        $this->render("index", array(
            'removeUrl' => $removeUrl,
            'countries' => $countries,
            'binfo' => $billingInfo,
            'uinfo' => $userInfo,

        ));

    }

	public static function enableCourse($idUser, $idCourse)
	{
		//make sure that existing enrollment in "waiting" status is activated
		$sql = 'update learning_courseuser set waiting = 0 where idUser = :idUser and idCourse = :idCourse';
		$parameters = array(':idUser' => $idUser, ':idCourse' => $idCourse);
		Yii::app()->db->createCommand($sql)->execute($parameters);

		//check if an enrollment for the user alredy exists
		$sql = 'select count(*) from learning_courseuser where idUser = :idUser and idCourse = :idCourse';
		$parameters = array(':idUser' => $idUser, ':idCourse' => $idCourse);
		$countCourseUserRelation = Yii::app()->db->createCommand($sql)->queryScalar($parameters);

		//if no previous enrollment could be find, then create it
		if ($countCourseUserRelation == 0) {
			//physically insert enrollment record
			$sql = 'insert into learning_courseuser (`idUser`, `idCourse`, `date_inscr`, `level`) values (:idUser, :idCourse, :date_inscr, :level)';
			$parameters = array(':idUser' => $idUser, ':idCourse' => $idCourse, ':date_inscr' => time(), ':level' => LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
			Yii::app()->db->createCommand($sql)->execute($parameters);
		}
	}

	public static function enableLearningPath($idUser, $id_path)
	{
		$sql = 'update learning_coursepath_user set waiting = 0 where idUser = :idUser and id_path = :id_path';
		$parameters = array(':idUser' => $idUser, ':id_path' => $id_path);
		Yii::app()->db->createCommand($sql)->execute($parameters);

		$sql = 'select count(*) from learning_coursepath_user where idUser = :idUser and id_path = :id_path';
		$parameters = array(':idUser' => $idUser, ':id_path' => $id_path);
		$countCourseUserRelation = Yii::app()->db->createCommand($sql)->queryScalar($parameters);

		if ($countCourseUserRelation == 0) {
			$sql = 'insert into learning_coursepath_user (`idUser`, `id_path`, `date_assign`) values (:idUser, :id_path, :date_assign)';
			$parameters = array(':idUser' => $idUser, ':id_path' => $id_path, ':date_assign' => time());
			Yii::app()->db->createCommand($sql)->execute($parameters);
		}
	}


    private function makeCoursesAvailable($cart, $idUser)
    {
        $positions = $cart->getPositions();

        if (count($positions) <= 0) {
            return false;
        }

        foreach ($positions as $position) {
            if($position instanceof LearningCourse){
                self::enableCourse($idUser, $position->idCourse);
            } elseif ($position instanceof LearningCoursepath){
                self::enableLearningPath($idUser, $position->id_path);

                $selectedCoursesToBuy = $position->getCourses();
                foreach ($selectedCoursesToBuy as $courseModel){
                    $courseUser = new LearningCourseuser();
                    self::enableCourse($idUser, $courseModel->idCourse);
                }


            }
        }
    }


	private function clearOldTransactions($idUser)
	{
		$sql = 'UPDATE ecommerce_transaction SET abandoned = 1 WHERE id_user = :id_user';
		$parameters = array(':id_user' => $idUser);
		Yii::app()->db->createCommand($sql)->execute($parameters);
	}


	private function subscribeUser()
	{
		$idUser = Yii::app()->user->id;
		$cart = Yii::app()->shoppingCart;

		$waiting = 1;
		$this->preSubscribeUser($cart, $idUser, $waiting);
		//$this->makeCoursesAvailable($cart, $idUser);
		$this->clearOldTransactions($idUser);
	}


    private function generateReceipt()
    {
        $result = '';

        $cart = Yii::app()->shoppingCart;
        $positions = $cart->getPositions();

        $sum = 0;
        $result .= '<p><img src="https://b.stripecdn.com/manage/assets/logo-b496a9631d3f807acc3cb7dd969629c8.png" /></p>';
        foreach ($positions as $position) {
            $result .= '<p>' . $position->name . " :: " . $position->price . " " . $cart->getCurrency() . '</p>';
            $sum += floatval($position->price);
        }
        $result .= '<p>---</p>';
        $result .= '<p>Sum: ' . $sum . " " . $cart->getCurrency() . '</p>';

        return $result;

    }

    public static function retrieveStripeEnabled()
    {
        $multidomainDomain = CoreMultidomain::resolveClient();
        if (empty($multidomainDomain) || ($multidomainDomain instanceof CoreMultidomain && !$multidomainDomain->ecommerceEnabled)) {
            // no subdomain, use core setting
            return (Settings::get('stripe_enabled') == '1');
        } else {
            return (CoreMultidomain::model()->findByPk($multidomainDomain->id)->stripe_enabled == 1);
        }
    }

    public static function retrieveStripeAlipayEnabled()
    {
        $multidomainDomain = CoreMultidomain::resolveClient();
        if (empty($multidomainDomain) || ($multidomainDomain instanceof CoreMultidomain && !$multidomainDomain->ecommerceEnabled)) {
            // no subdomain, use core setting
            return Settings::get('stripe_alipay_enabled') == '1';
        } else {
            return (CoreMultidomain::model()->findByPk($multidomainDomain->id)->stripe_alipay_enabled == 1);
        }
    }

    public static function retrieveStripePublicKeyTest()
    {
        $multidomainDomain = CoreMultidomain::resolveClient();
        if (empty($multidomainDomain) || ($multidomainDomain instanceof CoreMultidomain && !$multidomainDomain->ecommerceEnabled))
            // no subdomain, use core setting
            return CoreSetting::model()->findByPk('stripe_public_key_test')->param_value;
        else
            return CoreMultidomain::model()->findByPk($multidomainDomain->id)->stripe_public_key_test;
    }

    public static function retrieveStripePrivateKeyTest()
    {
        $multidomainDomain = CoreMultidomain::resolveClient();
        if (empty($multidomainDomain) || ($multidomainDomain instanceof CoreMultidomain && !$multidomainDomain->ecommerceEnabled))
            // no subdomain, use core setting
            return CoreSetting::model()->findByPk('stripe_private_key_test')->param_value;
        else
            return CoreMultidomain::model()->findByPk($multidomainDomain->id)->stripe_private_key_test;
    }

    public static function retrieveStripePublicKey()
    {
        $multidomainDomain = CoreMultidomain::resolveClient();
        if (empty($multidomainDomain) || ($multidomainDomain instanceof CoreMultidomain && !$multidomainDomain->ecommerceEnabled))
            // no subdomain, use core setting
            return CoreSetting::model()->findByPk('stripe_public_key')->param_value;
        else
            return CoreMultidomain::model()->findByPk($multidomainDomain->id)->stripe_public_key;
    }

    public static function retrieveStripePrivateKey()
    {
        $multidomainDomain = CoreMultidomain::resolveClient();
        if (empty($multidomainDomain) || ($multidomainDomain instanceof CoreMultidomain && !$multidomainDomain->ecommerceEnabled))
            // no subdomain, use core setting
            return CoreSetting::model()->findByPk('stripe_private_key')->param_value;
        else
            return CoreMultidomain::model()->findByPk($multidomainDomain->id)->stripe_private_key;
    }

    public static function retrieveStripeSandbox()
    {
        $multidomainDomain = CoreMultidomain::resolveClient();
        if (empty($multidomainDomain) || ($multidomainDomain instanceof CoreMultidomain && !$multidomainDomain->ecommerceEnabled))
            // no subdomain, use core setting
            return Settings::get('stripe_sandbox') == '1';
        else
        {
            return (CoreMultidomain::model()->findByPk($multidomainDomain->id)->stripe_sandbox == 1);
        }
    }

    /**
     * Retrieves an array of the stripe settings for either multi domain or regular environment
     * @return array
     */
    public static function getStripeSettings()
    {
        $multidomainDomain = CoreMultidomain::resolveClient();
        $settings = array();
        if (empty($multidomainDomain) || ($multidomainDomain instanceof CoreMultidomain && !$multidomainDomain->ecommerceEnabled)) {
            $settings['stripe_account_email'] =    Settings::get('stripe_account_email', '');
            $settings['stripe_alipay_enabled'] =   Settings::get('stripe_alipay_enabled', 0) == 1?true:false;
            $settings['stripe_enabled'] =          Settings::get('stripe_enabled', 0) == 1?true:false;
            $settings['stripe_private_key'] =      Settings::get('stripe_private_key', '');
            $settings['stripe_private_key_test'] = Settings::get('stripe_private_key_test', '');
            $settings['stripe_public_key'] =       Settings::get('stripe_public_key', '');
            $settings['stripe_public_key_test'] =  Settings::get('stripe_public_key_test', '');
            $settings['stripe_sandbox'] =          Settings::get('stripe_sandbox') == 1?true:false;
            $settings['stripe_tax_code'] =         Settings::get('stripe_tax_code', '');
            $settings['stripe_type'] =             Settings::get('stripe_type', '');
            $settings['domain'] =                  'default';
            $settings['currency'] =                Settings::get('currency_code', 'EUR');
        } else {
            $multiDomainSettings = CoreMultidomain::model()->findByPk($multidomainDomain->id);
            $settings['stripe_account_email'] =    $multiDomainSettings->stripe_account_email;
            $settings['stripe_alipay_enabled'] =   $multiDomainSettings->stripe_alipay_enabled == 1?true:false;
            $settings['stripe_enabled'] =          $multiDomainSettings->stripe_enabled == 1?true:false;
            $settings['stripe_private_key'] =      $multiDomainSettings->stripe_private_key;
            $settings['stripe_private_key_test'] = $multiDomainSettings->stripe_private_key_test;
            $settings['stripe_public_key'] =       $multiDomainSettings->stripe_public_key;
            $settings['stripe_public_key_test'] =  $multiDomainSettings->stripe_public_key_test;
            $settings['stripe_sandbox'] =          $multiDomainSettings->stripe_sandbox == 1?true:false;
            $settings['stripe_tax_code'] =         $multiDomainSettings->stripe_tax_code;
            $settings['stripe_type'] =             $multiDomainSettings->stripe_type;
            $settings['domain'] =                  $multidomainDomain->id;
            $settings['currency'] =                $multidomainDomain->currency;
        }
        return $settings;
    }

    public function actionAxStripeCharge()
    {
        try {
            // Fetch all Stripe settings
            $stripeSettings = self::getStripeSettings();

            if(!$_POST['billing_name'])             $_POST['billing_name'] = "N/A";
            if(!$_POST['billing_company'])          $_POST['billing_company'] = "N/A";
            if(!$_POST['billing_address_line1'])    $_POST['billing_address_line1'] = "N/A";
            if(!$_POST['billing_address_line2'])    $_POST['billing_address_line2'] = "N/A";
            if(!$_POST['billing_address_zip'])      $_POST['billing_address_zip'] = "N/A";
            if(!$_POST['billing_address_city'])     $_POST['billing_address_city'] = "N/A";
            if(!$_POST['billing_state'])            $_POST['billing_state'] = "N/A";
            if(!$_POST['billing_address_country'])  $_POST['billing_address_country'] = "N/A";
            if(!$_POST['billing_email'])            $_POST['billing_email'] = "N/A";
            if(!$_POST['billing_vat'])              $_POST['billing_vat'] = "N/A";

            $metadataCustomer = array(
                "01. Name" => $_POST['billing_name'],
                "02. Company" => $_POST['billing_company'],
                "03. Address line 1" => $_POST['billing_address_line1'],
                "04. Address line 2" => $_POST['billing_address_line2'],
                "05. Postal code" => $_POST['billing_address_zip'],
                "06. City" => $_POST['billing_address_city'],
                "07. State" => $_POST['billing_state'],
                "08. Country" => $_POST['billing_address_country'],
                "09. E-mail" => $_POST['billing_email'],
                "10. VAT" => $_POST['billing_vat'],
            );

            $metadataOrder = $metadataCustomer;

            $shippingData = array(
                'address' => array(
                    'city' => $_POST['billing_address_city'],
                    'country' => $_POST['billing_address_country'],
                    'line1' => $_POST['billing_address_line1'],
                    'line2' => $_POST['billing_address_line2'],
                    'postal_code' => $_POST['billing_address_zip'],
                    'state' => $_POST['billing_state'],
                ),
                'name' => $_POST['billing_name']
            );

            $cart = Yii::app()->shoppingCart;
            $positions = $cart->getPositions();
            $coupon = $cart->getCoupon();
            $addInteger = true;
            if(count($positions) <= 1){
                $addInteger = false;
            }
            $count = 1;
            $products = array();
            $orderedItems = array();
            foreach ($positions as $key => $pos) {
                if($addInteger){
                    $addKey = "Item ".$count;
                } else {
                    $addKey = "Item";
                }
                $addValue = $key . " - " . (isset($pos['name']) ? $pos['name'] : $pos['path_name']);
                if($pos instanceof CourseCartPosition)
                {
                    $_code = (!empty($pos->code) ? $pos->code : $pos->idCourse); //we absolutely need a code, if we don't have it then fallback on the ID
                    $addValue .= " (".$_code.")";

                    $products[] = array(
                        'title' => $pos->name,
                        'lms_id' => $pos->idCourse,
                        'price' => $pos->prize,
                        'type' => 'course',
                        'code' => $_code,
                        'quantity' => $pos->getQuantity()
                    );
                }
                elseif ($pos instanceof CoursepathCartPosition)
                {
                    $_code = (!empty($pos->path_code) ? $pos->path_code : $pos->id_path); //we absolutely need a code, if we don't have it then fallback on the ID
                    $addValue .= " (".$_code.")";

                    $products[] = array(
                        'title' => $pos->path_name,
                        'lms_id' => $pos->id_path,
                        'price' => $pos->price,
                        'type' => 'plan',
                        'code' => $_code,
                        'quantity' => $pos->getQuantity()
                    );
                }
                elseif ($pos instanceof CourseseatsCartPosition)
                {
                    $_code = (!empty($pos->code) ? $pos->code : $pos->idCourse);
                    $addValue .= " (".$_code.")";

                    $products[] = array(
                        'title' => $pos->getPositionNameSingleSeat(),
                        'lms_id' => $pos->idCourse,
                        'price' => $pos->getPriceSingleSeat(),
                        'type' => 'seats',
                        'code' => $_code,
                        'quantity' => $pos->getSeats()
                    );
                }
                else {
                    Yii::log("***DEBUG*** {invalid cart position} $key : ".print_r($pos, true), CLogger::LEVEL_INFO);
                }
                $metadataOrder[$addKey] = $addValue;
                $count++;
            }

            \Stripe\Stripe::setApiKey(CartController::retrieveStripeSandbox() ? CartController::retrieveStripePrivateKeyTest() : CartController::retrieveStripePrivateKey()); //Replace with your Secret Key

            // 1. Retrieve customer information
            $stripeCustomer = StripeCustomer::model()->findByAttributes(array(
                'id_user_lms' => Yii::app()->user->id
            ));

            $userName = Yii::app()->user->username;
            $userEmail = Yii::app()->user->email;
            $stripeDescription = "LMS user ".$userName." (".$userEmail.")";

            $customerData = array(
                'description' => $stripeDescription,
                'source' => $_POST['stripeToken'],
                'email' => $userEmail,
                'metadata' => $metadataCustomer,
                'shipping' => $shippingData
            );

            if(empty($stripeCustomer)){ // New stripe customer
                $customer = Customer::create($customerData);

                $stripeCustomer = new StripeCustomer();
                $stripeCustomer->id_user_lms = Yii::app()->user->id;
                $stripeCustomer->id_customer_stripe = $customer->id;
                $stripeCustomer->save();
            } else { // Existing stripe customer
                try{
                    $customer = Customer::retrieve($stripeCustomer->id_customer_stripe);
                    if(isset($customer->deleted) && ($customer->deleted == true)){
                        throw new Exception('user has been deleted');
                    }
                    $customer->description = $customerData['description'];
                    $customer->source = $customerData['source'];
                    $customer->metadata = $customerData['metadata'];
                    $customer->shipping = $customerData['shipping'];
                    $customer->save();
                } catch(Exception $e){
                    // Should only happen when the account has been deleted on Stripe or when stripe user account got corrupted in the LMS
                    $customer = Customer::create($customerData);

                    $stripeCustomer->id_customer_stripe = $customer->id;
                    $stripeCustomer->save();
                }
            }

            // Splitting up the logic here between a simple charge and a stripe order
            if($stripeSettings['stripe_type'] == "simple") { // Simple charge
                $charge = Charge::create(array(
                    "amount" => $_POST['amount'],
                    "currency" => strtolower(Yii::app()->shoppingCart->currency),
                    "customer" => $stripeCustomer->id_customer_stripe,
                    "description" => "",
                    "metadata" => $metadataOrder,
                    "receipt_email" => $_POST['receipt_email'],
                    "shipping" => $shippingData
                ));
            } else { // Create a stripe order
                foreach($products as $lmsProduct){
                    // 2. Check if Stripe products already exist
                    $productModel = StripeProduct::model()->findByAttributes(array(
                        'id_product_lms' => $lmsProduct['lms_id'],
                        'product_type' => $lmsProduct['type'],
                        'domain' => $stripeSettings['domain']
                    ));

                    $productData = array(
                        'name' => $lmsProduct['title'],
                        'attributes' => array('domain'),
                        'shippable' => false,
                        'metadata' =>  array(
                            'Product code' => $lmsProduct['code'],
                            'Product type' => $lmsProduct['type']
                        )
                    );

                    if(!empty($stripeSettings['stripe_tax_code'])){
                        $productData['metadata']['tax_code'] = $stripeSettings['stripe_tax_code'];
                    }

                    if(empty($productModel)){ // Product doesn't exist yet --> create it
                        $product = Product::create($productData);

                        $productModel = new StripeProduct();
                        $productModel->id_product_lms = $lmsProduct['lms_id'];
                        $productModel->product_type = $lmsProduct['type'];
                        $productModel->domain = $stripeSettings['domain'];
                        $productModel->id_product_stripe = $product->id;
                        $productModel->price_stripe = $lmsProduct['price'];

                    } else { // Product already exists --> update if price has changed
                        try{
                            $product = Product::retrieve($productModel->id_product_stripe);
                            if(isset($product->deleted) && ($product->deleted == true)){
                                throw new Exception('product has been deleted');
                            }
                            $product->name = $productData['name'];
                            $product->shippable = $productData['shippable'];
                            $product->metadata = $productData['metadata'];
                            $product->save();
                        } catch(Exception $e){
                            // Should only happen when the product has been deleted on Stripe or when stripe product record got corrupted in the LMS
                            $product = Product::create($productData);

                            $productModel->id_product_stripe = $product->id;
                        }
                    }

                    // 3. Check if Stripe SKU's already exist
                    $skuData = array(
                        'currency' => $stripeSettings['currency'],
                        'inventory' => array(
                            'type' => 'infinite'
                        ),
                        'price' => round($lmsProduct['price'] * 100), // always multiply by 100 for prices in EUR, USD, etc. Needs some rework for JPY and other non /100 currencies
                        'product' => $product->id,
                        'attributes' => array(
                            'domain' => (!empty($stripeSettings['domain']))?$stripeSettings['domain']:'default'
                        )
                    );

                    if(empty($productModel->id_sku_stripe)){
                        $sku = SKU::create($skuData);

                        $productModel->id_sku_stripe = $sku->id;
                    } else {
                        try{
                            $sku = SKU::retrieve($productModel->id_sku_stripe);
                            if(isset($sku->deleted) && ($sku->deleted == true)){
                                throw new Exception('sku has been deleted');
                            }
                            $sku->currency = $skuData['currency'];
                            $sku->inventory = $skuData['inventory'];
                            $sku->price = $skuData['price'];
                            $sku->product = $skuData['product'];
                            $sku->attributes = $skuData['attributes'];
                            $sku->save();
                        } catch(Exception $e){
                            // Should only happen when the SKU has been deleted on Stripe or when stripe product record got corrupted in the LMS
                            $sku = SKU::create($skuData);

                            $productModel->id_sku_stripe = $sku->id;
                        }
                    }

                    // 4. Save local settings + create product array for Stripe Order.
                    $productModel->save();
                    $orderedItems[] = array(
                        'currency' => strtolower(Yii::app()->shoppingCart->currency),
                        'parent' => $sku->id,
                        'quantity' => $lmsProduct['quantity'],
                        'type' => 'sku'
                    );
                }

                // 5. Check coupon use
                if(!empty($coupon)){
                    $lmsCoupon = EcommerceCoupon::model()->findByAttributes(array(
                        'code' => $coupon
                    ));
                    if(!empty($lmsCoupon)){
                        $couponData = array(
                            'id' => $coupon,
                            'duration' => 'once',
                            'metadata' => array(
                                'description' => $lmsCoupon->description
                            )
                        );
                        switch($lmsCoupon->discount_type){
                            case EcommerceCoupon::DISCOUNT_TYPE_AMOUNT:
                                $couponData['amount_off'] = round($lmsCoupon->discount * 100);
                                $couponData['currency'] = $stripeSettings['currency'];
                                break;
                            case EcommerceCoupon::DISCOUNT_TYPE_PERCENT:
                                $couponData['percent_off'] = round($lmsCoupon->discount);
                                break;
                        }
                        try{ // try to retrieve an existing coupon
                            $stripeCoupon = Coupon::retrieve($coupon);
                        } catch(Exception $e){ // else, create it
                            $stripeCoupon = Coupon::create($couponData);
                        }
                    }
                }

                // 6. Create and pay order
                $stripeOrder = array(
                    'currency' => strtolower(Yii::app()->shoppingCart->currency),
                    'customer' => $stripeCustomer->id_customer_stripe,
                    'email' => addslashes($_POST['billing_email']),
                    'items' => $orderedItems,
                    'metadata' => $metadataOrder
                );

                //check coupon
                if(!empty($coupon)){
                    $stripeOrder['coupon'] = $coupon;
                }
                $order = Order::create($stripeOrder);
                $order->pay(array(
                    'customer' => $customer->id
                ));

                if(!in_array($order->status, array('paid', 'fulfilled'))){
                    throw new Exception('Stripe payment could not be completed.');
                }
            }

            Yii::app()->session['coupon_code'] = $_POST['coupon_code'];

            Yii::app()->session['billing_address_city'] = $_POST['billing_address_city'];
            Yii::app()->session['billing_address_country'] = $_POST['billing_address_country'];
            Yii::app()->session['billing_address_country_code'] = $_POST['billing_address_country_code'];
            Yii::app()->session['billing_address_line1'] = $_POST['billing_address_line1'];
            Yii::app()->session['billing_address_zip'] = $_POST['billing_address_zip'];
            Yii::app()->session['billing_name'] = $_POST['billing_name'];

            if($stripeSettings['stripe_type'] == "simple") {
                Yii::app()->session['transaction_id'] = $charge->balance_transaction;
            } else {
                Yii::app()->session['transaction_id'] = $order->id;
            }

            $receiptHtml = ''; //$this->generateReceipt();

            $this->subscribeUser();

            echo $receiptHtml;
        } catch (Stripe_CardError $e) {
            Yii::log('card error: '.$e->getMessage(), CLogger::LEVEL_ERROR, "stripe");
        } catch (Stripe_InvalidRequestError $e) {
            Yii::log('API error: '.$e->getMessage(), CLogger::LEVEL_ERROR, "stripe");
        } catch (Stripe_AuthenticationError $e) {
            Yii::log('Authentication error: '.$e->getMessage(), CLogger::LEVEL_ERROR, "stripe");
        } catch (Stripe_ApiConnectionError $e) {
            Yii::log('API connection error: '.$e->getMessage(), CLogger::LEVEL_ERROR, "stripe");
        } catch (Stripe_Error $e) {
            Yii::log('Stripe error: '.$e->getMessage(), CLogger::LEVEL_ERROR, "stripe");
        } catch (Exception $e) {
            Yii::log('General error: '.$e->getMessage(), CLogger::LEVEL_ERROR, "stripe");
        }
    }

    /**
     * Remove one position id from the shopping cart
     *
     */
    public function actionAxRemove() {

        // Position id/key
        $id =  Yii::app()->request->getParam('id', null);

        // Any Id ?
        if ($id) {
            /* @var $cart EShoppingCart */
            $cart = Yii::app()->shoppingCart;

            // Get position with the provided id
            /** @var $position IECartPosition */
            $position = $cart->itemAt($id);
            if($position) {
                // Remove from cart
                $cart->remove($id);

                $idUser = Yii::app()->user->id;

	            if($position instanceof CourseCartPosition){
		            // Unsubscribe from course (if already subscribed and in waiting status)
		            $criteria = new CDbCriteria();
		            $criteria->addCondition('idUser = :id_user');
		            $criteria->addCondition('idCourse = :id_course');
		            $criteria->addCondition('waiting = 1');
		            $criteria->params = array(
			            ':id_user' => $idUser,
			            ':id_course' => $position->idCourse
		            );
		            /* @var $courseUserModel LearningCourseuser */
		            $courseUserModel = LearningCourseuser::model()->find($criteria);
		            if($courseUserModel) {
			            $courseUserModel->unsubscribeUser($idUser, $position->idCourse);
						if (($session = $position->getSession())!=null) {
							if($courseUserModel->course->course_type == LearningCourse::TYPE_CLASSROOM)
								LtCourseuserSession::model()->unsubscribeUser($idUser, $session->id_session);
							else
								WebinarSessionUser::model()->unsubscribeUser($idUser, $session->id_session);
						}
		            }
	            }elseif($position instanceof CoursepathCartPosition){
                    // Unsubscribe user from the courses he selected to buy but reverted transaction
                    /* @var $position CoursepathCartPosition */
                    $selectedCoursesToBuy = $position->getCourses();

                    foreach($selectedCoursesToBuy as $courseModel){
                        $courseUser = new LearningCourseuser();
                        $courseUser->unsubscribeUser($idUser, $courseModel->idCourse);
                    }
                }
            }

            // Recalculate number of items in the cart and (if reaches 0 abandon the cart)
            $positionsCount = count($cart->getPositions());
			$payment_gateway = '';
            if($positionsCount <= 0)
                $cart->abandonCart();
			else
				$payment_gateway = $this->renderPartial('_payment_gateway', null, true);



            $response = array(
                'success' => true,
                'count' => $positionsCount,
                'total' => $cart->getCost(),
				'payment_gateway' => $payment_gateway
            );
        }
        else {
            $response = array('success' => false);
        }

        echo CJSON::encode($response);

    }


	/**
	 * Receives an Ajax call from Chekout page, prepares the whole process of the payment
	 * and sends back a proper response to the caller with all required information
	 * to proceed (i.e. redirect user) to Payment processor (e.g. PayPal, Authorize.net,...), including form data
	 *
	 */
	public function actionAxCheckout()
	{

		$ok = true;

		// Collect messages in this array
		$messages = array();

		// Get payment method and validate
		$payment_method = Yii::app()->request->getParam('payment_method', "");
		$coupon_code = Yii::app()->request->getParam('coupon_code');
		$paymentGw = null;
		/* @var $paymentGw Payment|null */

		// Make life easier
		$idUser = Yii::app()->user->id;
		$cart = Yii::app()->shoppingCart;

		switch ($payment_method) {
			case Payment::$PAYMENT_METHOD_PAYPAL:
				$testMode = strtolower(Settings::get('paypal_sandbox')) == 'on';
				$paymentGw = new Paypal($testMode);
				break;
			case Payment::$PAYMENT_METHOD_PAYPAL_ADAPTIVE:
				$testMode = strtolower(Settings::get('paypal_sandbox')) == 'on';
				$paymentGw = new AdaptivePaypal($testMode);
				break;
			case Payment::$PAYMENT_METHOD_AUTHORIZE:
				$testMode = strtolower(Settings::get('authorize_sandbox')) == 'on';
				$paymentGw = new AuthorizeDotnet($testMode);
				break;
			case Payment::$PAYMENT_METHOD_PAYFLOW:
				$testMode = strtolower(Settings::get('paypal_sandbox')) == 'on';
				$paymentGw = new PayflowNVPAPI($testMode);
				break;
			case Payment::$PAYMENT_METHOD_ADYEN:
				$testMode = strtolower(Settings::get('adyen_sandbox')) == 'on';
				$paymentGw = new Adyen($testMode);
				break;
			case Payment::$PAYMENT_METHOD_STRIPE:
				$paymentGw = new Stripe(false);
				break;
			case Payment::$PAYMENT_METHOD_CYBERSOURCE:
				$paymentGw = new Cybersource(boolval(Settings::get('cybersource_sandbox_mode')));
				break;
			default:
				$event = new DEvent($this, array(
					'testMode' => true
				));

				// The event handler should change $event->return_value to
				// its custom payment gateway class that extends common/components/Payment.php
				Yii::app()->event->raise('ShoppingCartCheckout', $event);

				if ($event->handled && $event->return_value) {
					$paymentGw = $event->return_value;
				} else {
					if ($cart->getCost() > 0) {
						$ok = false;
						Yii::log("Payment method not specified.", 'error', __CLASS__);
						$messages[] = "Unknown payment method";
					}
				}
		}

		// Are we ordered to Save incoming billing info (checkout form data)
		$save_binfo = Yii::app()->request->getParam('save_binfo', 0);
		if ($save_binfo) {
			$saveOk = $this->saveBillingInfo($idUser, $_POST);
			if (!$saveOk) {
				$response = array(
					"success" => false,
					"message" => "Failed to save user billing information from checkout form",
				);
				echo CJSON::encode($response);
				Yii::app()->end();
			}
		}

		// Allow logging (development)
		if ($cart->getCost() > 0)
			$paymentGw->doLog = true;

		// 2. Create Transaction and Save
		$transactionModel = $this->createTransaction($idUser, $cart, $payment_method, $coupon_code);
		if ($payment_method == Payment::$PAYMENT_METHOD_PAYPAL) { $this->_log('PAYPAL ecommerce_transaction RECORD CREATED, ID = '.$transactionModel->id_trans); }
		if (!$transactionModel) {
			$ok = false;
			$messages[] = "Error creating transaction";
			Yii::log("Error creating transaction", 'error', __CLASS__);
		}

		$oldStripRelease = Yii::app()->urlManager->getStripRelease();
		Yii::app()->urlManager->setStripRelease(false);

		if ($cart->getCost() > 0) {
			$paymentGw->cancelUrl = Docebo::createAbsoluteLmsUrl("cart/cancelled", array("id_trans" => $transactionModel->id_trans));
			$paymentGw->returnUrl = Docebo::createAbsoluteLmsUrl("cart/finished", array("id_trans" => $transactionModel->id_trans));
		}

		// For Authorize.net, set the Relay-Response where transaction response is handled
		if ($payment_method == Payment::$PAYMENT_METHOD_AUTHORIZE) {
			$paymentGw->returnUrl = '';
			$paymentGw->relayResponseUrl = Docebo::createAbsoluteLmsUrl("cart/authnetRelayResponse");
		}

		Yii::app()->urlManager->setStripRelease($oldStripRelease);

		// 3. pre-Subscribe user to all courses in the shopping cart
		$waiting = 1;
		$this->preSubscribeUser($cart, $idUser, $waiting);

		if ($ok) {

			try {
				if ($cart->getCost() > 0) {
					// 4. Prepare data for payment process.
					$paymentGw->preparePayment($idUser, $cart, $transactionModel->id_trans);

					// 5. Get form to submit (filled with data, see above)
					$redirect_form = $paymentGw->getSubmitForm();
				}

				// 6. Clear Shopping cart

				Yii::app()->shoppingCart->clear();
			} catch (Exception $exception) {
				if ($payment_method == Payment::$PAYMENT_METHOD_PAYPAL) { $this->_log('PAYPAL ERROR WHILE PREPARING PAYMENT, message = '.$exception->getMessage()); }
				$ok = false;
				$messages[] = $exception->getMessage();
			}

		}

		// 7. If everyting is Ok, make a 'good to go' response
		if ($ok === true) {
			$redirect_url = '';
			if ((round($transactionModel->totalPrice - $transactionModel->discount, 2)) <= 0) {
				$redirect_url = Docebo::createAbsoluteLmsUrl("cart/finished", array("id_trans" => $transactionModel->id_trans, "free_checkout" => 1));
			}
			$response = array(
				"success" => true,
				"form" => $redirect_form,
				"url" => $redirect_url
			);
		} // Or not..
		else {
			if ($payment_method == Payment::$PAYMENT_METHOD_PAYPAL) { $this->_log('PAYPAL TRANSACTION WAS NOT OK. Id Transaction = '.$transactionModel->id_trans); }
			$response = array(
				"success" => false,
				"message" => implode(", ", $messages), // send messages as string imploded string, for now..
			);
		}

		// Hello there AJAX
		echo CJSON::encode($response);
		Yii::app()->end();

	}



	public function actionEmbeddedCheckout() {
		$this->render('embedded_checkout', array(
			'response' => Yii::app()->session['payflowData']
		));
	}


	/**
	 * Handle the IPN notification from Paypal after payment is done
	 *
	 */
	public function actionIpnPaypal()
	{

		// Enclose everything in a try/catch as it makes calls in the dark
		try {

			$paymentGw = new Paypal(strtolower(Settings::get('paypal_sandbox')) == 'on');
			$paymentGw->doLog = true;
			$validIpn = $paymentGw->validateTransaction();

			if ($validIpn) {

				Yii::log('Successfully validated IPN call from PayPal', 'debug', __CLASS__);

				// Further check received data
				$payment_status = $paymentGw->ipnData["payment_status"];
				if ($payment_status != 'Completed') {
					throw new CException("IPN validated, but Paypal returned non-completed status ($payment_status) Transaction NOT completed");
				}

				$id_trans = $paymentGw->ipnData["invoice"];
				$transactionModel = EcommerceTransaction::model()->findByPk($id_trans);

				if (!$transactionModel) {
					throw new CException("IPN validated, but failed to load transaction model for Trans Id ($id_trans). Transaction NOT completed.");
				}


				// 1. Match seller emails
				$our_sellerEmail = strtolower(Settings::get('paypal_mail', ''));
				if ($our_sellerEmail == '') {
					throw new CException('IPN validated, but our Paypal email is empty. Transaction NOT completed.');
				}

				$theirs_sellerEmail = isset($paymentGw->ipnData['business']) ? strtolower($paymentGw->ipnData['business']) : '';
				if (empty($theirs_sellerEmail)) {
					throw new CException('IPN validated, but Paypal does not provide valid receiver email. Transaction NOT completed.');
				}
				if ($our_sellerEmail != $theirs_sellerEmail) {
					throw new CException('IPN validated, but our seller email and Paypal seller email do not match! Transaction NOT completed.');
				}

				// 2. Match amount/total
				$our_Total = EcommerceTransactionInfo::getTransactionTotal($id_trans);
				$theirs_Total = $paymentGw->ipnData["mc_gross"];
				/*
				 * Even though PayPal documentation says mc_gross is the toatl amount paid by client, 'tax' is added to
				 * the amount and mc_gross is returned as REQUESTED amount + tax. This makes this check pointless, since tax is not known in advance.
				 * Use Case: Canadian LMS has been paid for a course and client has been charged by tax (LMS: chad.docebosaas.com)
				if ($our_Total != $theirs_Total) {
						throw new CException('IPN validated, but our transaction total amount does not match Paypal amount! Transaction NOT completed.');
				}
				*/

				// 3. Currency code
				$our_CurrencyCode = strtolower($transactionModel->payment_currency);
				$theirs_CurrencyCode = strtolower($paymentGw->ipnData["mc_currency"]);
				if ($our_CurrencyCode != $theirs_CurrencyCode) {
					throw new CException('IPN validated, but our transaction Currency does not match Paypal Currency! Transaction NOT completed.');
				}


				// Good, payment successfully finished, lets activate courses;
				// "invoice" is what we use for Transaction Id   receipt_id
				$complete = $this->completeTransaction(
					$id_trans,
					$paymentGw->ipnData["txn_id"],
					$paymentGw->ipnData["receipt_id"],
					$paymentGw->ipnData);


				// If everything is ok, send emails
				if ($complete) {
					$result = $this->sendEmailNotifications($id_trans);
					if (!$result) {
						Yii::log('IPN validated, transaction is completed/activated, but failed to send notification emails.', 'error', __CLASS__);
					} else {
						Yii::log("Successfully completed/activated Transaction $id_trans", 'debug', __CLASS__);
					}
				} else {
					throw new CException('IPN validated, but failed to complete transaction.');
				}


			} else {
				throw new CException('Failed to validate IPN call from PayPal. IPN Data: ' . print_r($paymentGw->ipnData, true));
			}

			Yii::app()->end();

		} catch (Exception $e) {
			Yii::log("IPN PAYPAL Exception: " . $e->getMessage(), 'error', __CLASS__);
			Yii::app()->end();
		}
	}





    /**
     * Same as the regular Paypal IPN action but with some specific checks
     * and some checks removed
     */
    public function actionIpnPaypalAdaptive(){
        // Enclose everything in a try/catch as it makes calls in the dark
        try {

            $paymentGw = new AdaptivePaypal(strtolower(Settings::get('paypal_sandbox')) == 'on');

            $paymentGw->doLog = true;
            $validIpn = $paymentGw->validateTransaction();
            if ($validIpn) {

                Yii::log('Successfully validated IPN call from PayPal', 'debug', __CLASS__);

                // Further check received data
                $payment_status = $paymentGw->ipnData["status"];
                if ( $payment_status != 'COMPLETED') {
                    throw new CException("IPN validated, but Paypal returned non-completed status ($payment_status) Transaction NOT completed");
                }

                $payKey = $paymentGw->ipnData["pay_key"];
                $transactionModel = EcommerceTransaction::model()->findByAttributes(array(
					'payment_txn_id'=>$payKey
                ));

  				if ( !$transactionModel ) {
                    throw new CException("IPN validated, but failed to load trnasaction model for Trans Id ($payKey). Transaction NOT completed.");
                }

                if($transactionModel->id_trans != Yii::app()->getRequest()->getParam('id_trans')){
                    throw new CException("IPN validated, but transaction IDs don't match. Requested to complete ".Yii::app()->getRequest()->getParam('id_trans')." but the PayPal payment is for ID ".$transactionModel->id_trans);
                }

                // Good, payment successfully finished, lets activate courses;
                // "invoice" is what we use for Transaction Id   receipt_id
				if(!$transactionModel->paid) {
					$complete = $this->completeTransaction(
						$transactionModel->id_trans,
						$payKey,
						null,
						$paymentGw->ipnData );


					// If everything is ok, send emails
					if ($complete)  {

						// Raise event so plugins can do extra stuff after IPN callback
						Yii::app()->event->raise('AfterTransactionComplete', new DEvent($this, array(
							'id_trans'=>$transactionModel->id_trans,
						)));

						//IF the transaction is not a refund then  we send email notifications
						if(!(isset($paymentGw->ipnData['reason_code']) && $paymentGw->ipnData['reason_code'] == 'Refund')){
							$result = $this->sendEmailNotifications($transactionModel->id_trans);
							if (!$result) {
								Yii::log('IPN validated, transaction is completed/activated, but failed to send notification emails.', 'error', __CLASS__);
							}
							else {
								Yii::log("Successfully completed/activated Transaction $transactionModel->id_trans", 'debug', __CLASS__);
							}
						}
					}
					else {
						throw new CException('IPN validated, but failed to complete transaction.');
					}
				} else {
					Yii::log("The transaction $transactionModel->id_trans is already completed/activated", 'debug', __CLASS__);
				}


            }
            else {
                throw new CException('Failed to validate IPN call from PayPal. IPN Data: ' . print_r($paymentGw->ipnData,true) );
            }

            Yii::app()->end();

        }

        catch (Exception $e)  {
            Yii::log("Exception: " . $e->getMessage(), 'error', __CLASS__);
            Yii::app()->end();
        }
    }



    /**
     * Authorize.net Relay-Response action.
     *
     * Payment gateway will POST transaction response here; We render back a simple HTML + JS to redirect to final page: actionFinished
     */
    public function actionAuthnetRelayResponse() {

    	try {

    		$testMode 			= strtolower(Settings::get('authorize_sandbox')) == 'on';
    		$paymentGw 			= new AuthorizeDotnet($testMode);
    		$paymentGw->doLog 	= true;


    		// ipnData is loaded from REQUEST during object construction
    		$id_trans = $paymentGw->ipnData['id_trans'];

    		// False by default
    		$isTransactionValid = false;

    		$isTransactionValid = $paymentGw->validateTransaction();


    	    if ($isTransactionValid) {

    	    	Yii::log('Authorize.net: Successfully validated transaction', 'debug', __CLASS__);

    	    	$xResponseCode = $paymentGw->ipnData['x_response_code'];
    	    	if (intval($xResponseCode) != 1) {
    	    		throw new CException('Authorize.net: Transaction NOT approved. Response Code:' . $xResponseCode . " (1:Approved, 2: Declined, 3: Error, 4: Held for review)", 501);
    	    	}

    	    	$transactionModel = EcommerceTransaction::model()->findByPk($id_trans);

    	    	if ( !$transactionModel ) {
    	    		throw new CException("Authorize.net: Transaction validated, but failed to load transaction model for transaction #$id_trans. Purchase failed.");
    	    	}

    	    	// So far so good, lets complete the purchase (activating purchased courses)
    	    	$complete = $this->completeTransaction(
    	    		$id_trans,
    	    		$paymentGw->ipnData['x_trans_id'],
    	    		$paymentGw->ipnData['x_invoice_num'],
    	    		$paymentGw->ipnData
    	    	);

    	    	// If everything is ok, send emails
    	    	if ($complete)  {
    	    		$result = $this->sendEmailNotifications($id_trans);
    	    		if (!$result) {
    	    			Yii::log('Authorize.net: Transaction validated, transaction is completed/activated, but failed to send notification emails.', 'error', __CLASS__);
    	    		}
    	    		else {
    	    			Yii::log("Authorize.net: Successfully completed/activated Transaction $id_trans", 'debug', __CLASS__);
    	    		}
    	    	}
    	    	else {
    	    		throw new CException('Authorize.net: Tansaction validated, but failed to complete transaction.');
    	    	}

    	    	// On success, redirect to after-purchase user interface
    			$redirectUrl = Docebo::createAbsoluteLmsUrl('cart/finished', array('id_trans' => $id_trans));
    		}
    		else {
    			throw new CException("Authorize.net: Failed to validate transaction $id_trans ");
    		}

    	}
    	catch (Exception $e)  {
    		Yii::log("Authorize.net Exception: " . $e->getMessage(), 'error', __CLASS__);

    		if ($e->getCode() == 501) { // Real error response from authorize net
    			Yii::app()->user->setFlash('error', Yii::t("standard", "Authorize.net transaction failed (declined or error)"));
    		}
    		else {
    			Yii::app()->user->setFlash('error', Yii::t("standard", "_OPERATION_FAILURE"));
    		}
    		$redirectUrl = Docebo::createAbsoluteLmsUrl('site/index', array());
    	}


    	// Render relay-response. It will just redirect, using JS
    	$this->renderPartial('_authnet_relay_response', array(
    			'redirectUrl' 	=> $redirectUrl,
    	));

    }

    /**
     * URL to redirect user if/when order is cancelled at Payment gateway.
     * Common action for some payment methods, like Paypal and Authorize.net.
     *
     * If not appropriate [for another payment], please create specific action.
     *
     */
    public function actionCancelled() {
        $transaction_id = Yii::app()->request->getParam('id_trans', "");
        if ( !empty($transaction_id)) {
        	// Revert (i.e. "unsubscribe", delete from "waiting" list) **CURRENT** user from course(s) associated to the transaction "id_trans".
        	// User MUST be checked. Otherwise, someone can unsubscribe another user by simply using 'cancelled' URL
            $result = $this->revertTransactionActions($transaction_id, Yii::app()->user->id);
            if (!$result) {
            	Yii::app()->user->setFlash('error', Yii::t("standard", "_OPERATION_FAILURE"));
            }
        }
        $this->redirect(Docebo::createLmsUrl("site/index"), true);
    }


	/**
	 * URL to redirect **user** after successful payment.
	 * Note this is NOT transaction validation process, it is just the after-payment landing page!
	 *
	 * Common action for some payment methods, like Paypal and Authorize.net.
	 *
	 * If not appropriate [for another payment], please create specific action.
	 */
	public function actionFinished()
	{

		if (Payment::isMethodEnabled(Payment::$PAYMENT_METHOD_PAYFLOW)) {
			// Check if we just returned inside the iframe.  If so, store payflow response and redirect parent window with javascript.
			if (isset($_POST['RESULT']) || isset($_GET['RESULT'])) {
				Yii::app()->session['payflowresponse'] = array_merge($_GET, $_POST);

				$finishedUrl = Docebo::createAppUrl('lms:cart/finished', array('id_trans' => Yii::app()->request->getParam('id_trans')));
				echo '<script type="text/javascript">window.top.location.href = "' . $finishedUrl . '";</script>';
				Yii::app()->end();
			}
		}

		$isTransactionValid = true;
		$transaction_id = Yii::app()->request->getParam('id_trans');
		$transactionModel = EcommerceTransaction::model()->findByPk($transaction_id);
		$free_checkout = Yii::app()->request->getParam('free_checkout', 0);

		if (!$transactionModel) {
			Yii::app()->user->setFlash('error', Yii::t("standard", "_OPERATION_FAILURE"));
			$this->redirect(Docebo::createLmsUrl('site/index'), true);
		}

		// Now that we have a transaction model, check if currently logged in user is EQUAL to the one from transaction (security)
		if ($transactionModel->id_user != Yii::app()->user->id) {
			Yii::app()->user->setFlash('error', Yii::t("standard", "_OPERATION_FAILURE"));
			$this->redirect(Docebo::createLmsUrl('site/index'), true);
		}

		// The if() block below only relates to "free checkout" purchases
		// that is carts that are worth 0 after the discount is applied
		if ($free_checkout == 1 && ($transactionModel->totalPrice - $transactionModel->discount) <= 0) {
			$complete = $this->completeTransaction($transaction_id);

			// If everything is ok, send emails
			if ($complete) {
				Yii::app()->shoppingCart->clear();
				$result = $this->sendEmailNotifications($transaction_id);
				if (!$result) {
					Yii::log('Free checkout: transaction is completed/activated, but failed to send notification emails.', 'error', __CLASS__);
				} else {
					Yii::log("Free checkout: Successfully completed/activated Transaction $transaction_id", 'debug', __CLASS__);
				}
			}
		}

		$event = new DEvent($this, array(
			'transaction_id' => $transaction_id,
			'testMode' => true,
		));
		Yii::app()->event->raise('ShoppingCartCheckoutFinished', $event);
		if ($event->handled) return;

		$purchasedItems = array();
		if (!empty($transaction_id)) {

			$transactionModel = EcommerceTransaction::model()->findByPk($transaction_id + 0);

			if ($transactionModel) {
				$purchasedItems = $transactionModel->getRelatedItems();
			}
		}

		if (Payment::isMethodEnabled(Payment::$PAYMENT_METHOD_PAYFLOW)) {
			//Check whether we stored a server response.  If so, print it out.
			if (!empty(Yii::app()->session['payflowresponse'])) {
				Yii::log("Response from payflow:", CLogger::LEVEL_INFO);
				Yii::log(var_export(Yii::app()->session['payflowresponse'], true), CLogger::LEVEL_INFO);
				$response = Yii::app()->session['payflowresponse'];
				unset(Yii::app()->session['payflowresponse']);
				$isTransactionValid = ($response['RESULT'] == 0);
				if ($isTransactionValid) {
					if ($this->completeTransaction($transaction_id)) {
						// If everything is ok, send emails
						Yii::app()->shoppingCart->clear();
						$result = $this->sendEmailNotifications($transaction_id);
						if (!$result)
							Yii::log('Embedded checkout: transaction is completed/activated, but failed to send notification emails.', 'error', __CLASS__);
						else
							Yii::log("Embedded checkout: Successfully completed/activated Transaction $transaction_id", 'debug', __CLASS__);
					}
				}
			}
		}

		$viewFile = 'return_gw';
		if (CoreUserPU::isPUAndSeatManager()) {
			foreach ($transactionModel->transaction_items as $ecommerceTransactionItem) {
				/* @var $ecommerceTransactionItem EcommerceTransactionInfo */
				if ($ecommerceTransactionItem->item_type == EcommerceTransactionInfo::TYPE_COURSESEATS) {
					$viewFile = 'return_gw_pu_seats_bought';
				}
			}
		}

		$this->render($viewFile, array(
			'transaction_id' => $transaction_id,
			'purchased_items' => $purchasedItems,
			'transaction_success' => $isTransactionValid
		));

	}




    public function actionStripeFinished(){

			$success = (Yii::app()->request->getParam('success', 1) > 0);

        $viewFile = 'return_gw';

        $purchasedItems = [];
        $courseIndicesArray = (Yii::app()->session['shoppingCartItems']);
        for ($i = 0; $i < count($courseIndicesArray); $i++){
            $courseOrPath = $courseIndicesArray[$i];
            if ($courseOrPath['type'] == 'course')
                $learningCourseOrPath = LearningCourse::model()->findByPk($courseOrPath['id']);
            elseif ($courseOrPath['type'] == 'learning_path')
                $learningCourseOrPath = LearningCoursepath::model()->findByPk($courseOrPath['id']);

            if ($learningCourseOrPath != null)
                $purchasedItems[] = $learningCourseOrPath;
        }

			if (!$success) {
				$this->render('transaction_failed', array(
					'courses' => $purchasedItems
				));
				Yii::app()->end();
			}

        $idUser = Yii::app()->user->id;
        $cart = Yii::app()->shoppingCart;

        $billingInfo = new CoreUserBilling();
        $billingInfo->bill_address1 = Yii::app()->session['billing_address_line1'];
        $billingInfo->bill_address2 = '';
        $billingInfo->bill_city = Yii::app()->session['billing_address_city'];
        $billingInfo->bill_country_code = Yii::app()->session['billing_address_country_code'];
        $billingInfo->bill_company_name = Yii::app()->session['billing_name'];
        $billingInfo->bill_state = Yii::app()->session['billing_address_country'];
        $billingInfo->bill_zip = Yii::app()->session['billing_address_zip'];
        $billingInfo->idst = $idUser;
        $billingInfo->save();

        $transactionModel = $this->createTransaction($idUser, $cart, 'stripe', Yii::app()->session['coupon_code'], $billingInfo);
		$complete = $this->completeTransaction($transactionModel->id_trans, Yii::app()->session['transaction_id']);
		if ($complete) {
			$result = $this->sendEmailNotifications($transactionModel->id_trans);
			if (!$result) {
				Yii::log('Stripe: Transaction is completed/activated, but failed to send notification emails.', 'error', __CLASS__);
			}
			else {
				Yii::log("Stripe: Successfully completed/activated Transaction $transactionModel->id_trans", 'debug', __CLASS__);
			}
		}

        unset(Yii::app()->session['coupon_code']);
        unset(Yii::app()->session['billing_address_city']);
        unset(Yii::app()->session['billing_address_country']);
        unset(Yii::app()->session['billing_address_country_code']);
        unset(Yii::app()->session['billing_address_line1']);
        unset(Yii::app()->session['billing_address_zip']);
        unset(Yii::app()->session['billing_name']);

        unset(Yii::app()->session['transaction_id']);

        $cart->clear();
        $this->clearOldTransactions($idUser);

        $this->render($viewFile, array(
            'purchased_items' => $purchasedItems,
            'transaction_success' => true
        ));


    }
        /**
	 * Warning! Only to be used for debugging. Will mark a given
	 * transaction as paid and activate all courses/learning plans inside.
	 * This will emulate the behavior of PayPal/Authorize/Other IPN call
	 *
	 * @param $id
	 */
	public function actionTestCompleteCheckout($id = null){
		return;
		if(!$id){
			// Get the last transaction
			$id = Yii::app()->getDb()->createCommand()
				->select("MAX(id_trans)")
				->from(EcommerceTransaction::model()->tableName())
				->queryScalar();
		}
		$this->completeTransaction($id);
	}

    /**
     *
     */
    public function actionAxApplyDiscount()
    {
        $couponCode = Yii::app()->request->getParam('code');
        $cartTotal = Yii::app()->shoppingCart->getCost();
        $data = array('total' => $cartTotal, 'error' => '');

        if($couponCode) {
            $coupon = EcommerceCoupon::model()->findByAttributes(array('code' => $couponCode));
            if ($coupon) {
                $cartCourses = array();
	            $cartLearningPlans = array();
	            $cartSeats = array();
	            if(!$coupon->can_have_courses){
                foreach (Yii::app()->shoppingCart->getPositions() as $position) {
					if($position instanceof CourseCartPosition) {
						$cartCourses[] = $position->idCourse;
			            }elseif($position instanceof CoursepathCartPosition){
				            $cartLearningPlans['ids'][$position->id_path] = $position->id_path;
				            $cartLearningPlans['prices'][$position->id_path] = $position->getSumPrice();
			            }elseif($position instanceof CourseseatsCartPosition){
				            $cartSeats['ids'][$position->idCourse] =  $position->idCourse;
				            $cartSeats['prices'][$position->idCourse] = $position->getSumPrice();
					}
                }
	            }else{
		            foreach (Yii::app()->shoppingCart->getPositions() as $position) {
			            if($position instanceof CourseCartPosition) {
				            $cartCourses[] = $position->idCourse;
			            }
		            }
	            }


                $discountResult = $coupon->applyDiscount($cartTotal, $cartCourses, $cartLearningPlans, $cartSeats);
                $data = array_merge($data, $discountResult);
                if(!$data['error'])
                    Yii::app()->shoppingCart->setCoupon($couponCode);
            }
            else
                $data['error'] = Yii::t('coupon', 'Expired or invalid coupon');
        } else
            Yii::app()->shoppingCart->setCoupon(null);

        echo CJSON::encode($data);
    }



	/**
	 * Create new transaction record and fill required data, save.
	 *
	 * @param int $idUser
	 * @param EShoppingCart $cart
	 * @param string $payment_method
	 * @param string $coupon_code
	 * @return NULL|EcommerceTransaction
	 */
	private function createTransaction($idUser, $cart, $payment_method, $coupon_code, $externalBillingInfo = null)
	{

		$positions = $cart->getPositions();

		$userModel = CoreUser::model()->findByPk($idUser);

		// If cart is empty (should NOT be if we are here, but anyway)
		if (count($positions) <= 0) {
			return null;
		}

		$discountResult = array();
		$coupon = EcommerceCoupon::model()->findByAttributes(array('code' => $coupon_code));

		if ($coupon) {
			$cartCourses = array();
			$cartLearningPlans = array();
			$cartSeats = array();
			if (!$coupon->can_have_courses) {
				foreach (Yii::app()->shoppingCart->getPositions() as $position) {
					if ($position instanceof CourseCartPosition) {
						$cartCourses[] = $position->idCourse;
					} elseif ($position instanceof CoursepathCartPosition) {
						$cartLearningPlans['ids'][$position->id_path] = $position->id_path;
						$cartLearningPlans['prices'][$position->id_path] = $position->getSumPrice();
					} elseif ($position instanceof CourseseatsCartPosition) {
						$cartSeats['ids'][$position->idCourse] = $position->idCourse;
						$cartSeats['prices'][$position->idCourse] = $position->getSumPrice();
					}
				}
			} else {
				foreach ($positions as $position) {
					if ($position instanceof CourseCartPosition) {
						$cartCourses[] = $position->idCourse;
					}
				}
			}

			$discountResult = $coupon->applyDiscount(Yii::app()->shoppingCart->getCost(), $cartCourses, $cartLearningPlans, $cartSeats);
		}

		// Get billing information id

		$billingInfo = null;
		if ($externalBillingInfo != null)
			$billingInfo = $externalBillingInfo;
		else
			$billingInfo = CoreUserBilling::model()->getUserLastBillingInfo($idUser);

		// Main transaction
		$transactionModel = new EcommerceTransaction();

		$transactionModel->id_user = $idUser;
		$transactionModel->location = 'lms';
		$transactionModel->date_creation = Yii::app()->localtime->localNow;
		$transactionModel->billing_info_id = $billingInfo !== null ? $billingInfo->id : NULL;
		$transactionModel->payment_currency = $cart->currencyCode;
		$transactionModel->payment_type = $payment_method;
		$transactionModel->paid = 0;
		if ($coupon) {
			//Write coupon_id in transaction only if it's really used for discount
			if ($discountResult['discount'] != 0)
				$transactionModel->id_coupon = $coupon->id_coupon;

			if (floatval($discountResult['discount']) != 0)
				$transactionModel->discount = $discountResult['discount'];
		}

		Yii::app()->event->raise('NewEcommerceTransactionCreated', new DEvent($this, array(
			'transaction' => &$transactionModel
		)));

		$ok = $transactionModel->save();
		if (!$ok) {
			return null;
		}

		// Transaction items
		$transaction_id = $transactionModel->id_trans;
		foreach ($positions as $position) {

			/* @var $position ECartPositionBehaviour|CourseCartPosition|CoursepathCartPosition|CourseseatsCartPosition */

			$transactionItemModel = new EcommerceTransactionInfo();
			$transactionItemModel->id_trans = $transaction_id;

			$transactionItemModel->id_date = '';
			$transactionItemModel->id_edition = '';
			$transactionItemModel->code = '';
			$transactionItemModel->activated = 0;
			$transactionItemModel->name = $position->getPositionName();

			// Get a "snapshot" array of all [attribute_name]->[attribute_value]
			// values for the position/model. This is needed, because later if the
			// actual model is deleted, we need to know
			// what was bought (at least the name and basic info)
			// Feel free to add stuff to this array with related info, e.g.
			// course sessions being purchased (since relations of the model are
			// not returned by getAttributes())
			$snapshotOfItem = $position->getAttributes();

			$jsonArrData = array();

			if ($position instanceof CourseCartPosition) {
				$transactionItemModel->id_course = $position->idCourse;
				$transactionItemModel->price = $position->getSumPrice();
				$transactionItemModel->code = $position->code;
				$transactionItemModel->item_type = EcommerceTransactionInfo::TYPE_COURSE;

				if (method_exists($position, 'getSession')) {
					/* @var $_session LtCourseSession */
					$_session = $position->getSession();
					if ($_session) {
						$transactionItemModel->id_date = $_session->id_session;

						// Store a snapshot of the purchased session
						$sessionSnapshot = array(
							'name' => $_session->name,
							'date_begin' => Yii::app()->localtime->toUTC($_session->date_begin),
							'date_end' => Yii::app()->localtime->toUTC($_session->date_end),
						);
						$snapshotOfItem['session'] = $sessionSnapshot;
					}
				}
			} elseif ($position instanceof CoursepathCartPosition) {
				$transactionItemModel->price = $position->getPrice();
				$transactionItemModel->id_path = $position->id_path;
				$transactionItemModel->code = $position->path_code;
				$transactionItemModel->item_type = EcommerceTransactionInfo::TYPE_COURSEPATH;

				// User us buying a Learning Plan. Preserve the selected
				// courses from the Learning Plan because he may buy it only
				// partially and on callback from the payment gateway we need
				// to activate his subscription for only those courses
				$idCoursesArr = array();
				$snapshotOfItem['courses_names'] = array();
				foreach ($position->getCourses() as $course) {
					$idCoursesArr[] = $course->idCourse;

					$snapshotOfItem['courses_names'][] = $course->name;

				}
				$jsonArrData['courses'] = $idCoursesArr;

			} elseif ($position instanceof CourseseatsCartPosition) {
				$transactionItemModel->price = $position->getPrice();
				$transactionItemModel->item_type = EcommerceTransactionInfo::TYPE_COURSESEATS;
				$transactionItemModel->id_course = $position->idCourse;
				$transactionItemModel->code = $position->code;

				$jsonArrData['seats'] = $position->getSeats();
			}

			$snapshotOfItem['buyer_user'] = $userModel->getAttributes();

			$jsonArrData['position_snapshot'] = $snapshotOfItem;

			$transactionItemModel->item_data_json = CJSON::encode($jsonArrData);


			$ok = $transactionItemModel->save();

			if (!$ok) {
				return null;
			}

		}

		return $transactionModel;
	}



	/**
	 * Create subscription records, subject to activation etc.
	 *
	 * @todo Very basic subscription, definitely not for production!!
	 *
	 * @param EShoppingCart $cart
	 * @param               $idUser
	 * @param int           $waiting Approval status
	 *
	 * @return bool
	 */
	private function preSubscribeUser($cart, $idUser, $waiting)
	{
		$positions = $cart->getPositions();

		if (count($positions) <= 0) {
			return false;
		}

		foreach ($positions as $position) {
			// Trigger event to let plugins use their custom cart presubscribe operations
			$event = new DEvent($this, array (
				'user_id' => $idUser,
				'cart' => $cart,
				'position' => $position,
				'waiting' => $waiting
			));
			Yii::app()->event->raise('OnBeforeCourseCartPresubscribe', $event);
			$continue = true;
			if ($event->shouldPerformAsDefault() && !empty($event->return_value)) {
				//some plugins may not need normal subscribe actions .. let them avoid those
				if (isset($event->return_value['continue']) && !$event->return_value['continue']) {
					$continue = false;
				}
			}
			// end event
			if ($continue) {
				if ($position instanceof CourseCartPosition) {
					// Cart position is a Course
					$courseUserModel = new LearningCourseuser();
					$courseUserModel->subscribeUser($idUser, Yii::app()->user->id, $position->idCourse, $waiting, false, true);
					if (($session = $position->getSession()) != null) {
						switch($courseUserModel->course->course_type) {
							case LearningCourse::TYPE_CLASSROOM:
								LtCourseuserSession::enrollUser($idUser, $session->id_session, LtCourseuserSession::$SESSION_USER_WAITING_LIST, true);
								break;
							case LearningCourse::TYPE_WEBINAR:
								WebinarSessionUser::enrollUser($idUser, $session->id_session, WebinarSessionUser::$SESSION_USER_WAITING_LIST, true);
								break;
						}

					}
				} elseif ($position instanceof CoursepathCartPosition) {
					// Cart position is a Learning Plan

					// Enroll user in learning plan
					$alreadyEnrolled = LearningCoursepathUser::model()->findByAttributes(array(
						'id_path'=>$position->id_path,
						'idUser'=>$idUser,
					));
					if (!$alreadyEnrolled) {
						$enrollToLp = new LearningCoursepathUser();
						$enrollToLp->id_path = $position->id_path;
						$enrollToLp->idUser = $idUser;
						$enrollToLp->subscribed_by = Yii::app()->user->getIdst();
						$enrollToLp->date_assign = Yii::app()->localtime->toLocalDateTime();
						$enrollToLp->waiting = 1;
						$enrollToLp->save();
						// Don't call $enrollToLp->subscribeUser() here
						// since it will subscribe the user to all courses
						// inside the LP. We don't want that, we only want
						// to pre-subscribe him to the courses he is buying
					}

					// Subscribe user to all courses selected to purchase from Learning Plan
					foreach ($position->getCourses() as $course) {
						$courseUserModel = new LearningCourseuser();
						$courseUserModel->subscribeUser($idUser, Yii::app()->user->id, $course->idCourse, $waiting, false, true);
					}
				}
			}
		}

		return true;
	}


    /**
     * Revert all actions done due to transaction (like e.g. when transaction is canceled)
     *
     * @param int             $transaction_id
     *
     * @param integer|boolean $idUser Optional, but if provided will enforce a check if the given
     * transaction's initiating user matches the one passed as parameter here. Usually
     * the current user's ID is passed
     *
     * @return bool
     * @throws CDbException
     */
    private function revertTransactionActions($transaction_id, $idUser=false) {

    	$transactionModel = EcommerceTransaction::model()->findByPk($transaction_id);

       	$transactionIdUser = $transactionModel->id_user;

       	// Check if transaction user matches requested idUser (if any)
       	if ($idUser && !($idUser === $transactionIdUser )) {
       		return false;
       	}

       	if ( !$transactionModel ) {
           	Yii::log("An attempt to revert invalid transaction ($transaction_id) has been made", 'error', __CLASS__);
           	return false;
       	}

	    $event = new DEvent($this, array('transaction'=>$transactionModel));
	    Yii::app()->event->raise('onTransactionRevert', $event);

	    if($event->return_value){
		    return $event->return_value;
	    }

       	$transactionItemModels = $transactionModel->transaction_items;

		if ( count($transactionItemModels) <= 0) {
           	Yii::log("An attempt to revert empty transaction ($transaction_id) has been made. No transaction items found.", 'error', __CLASS__);
           	return false;
       	}


        /*
       	// Enumerate all pre-subscribed courses and delete/unsubscribe
       	foreach ( $transactionItemModels as $transaction_info ) {
        	// Unsubscribe

	        if($transaction_info->item_type == EcommerceTransactionInfo::TYPE_COURSE) {
		        $courseUser = new LearningCourseuser();
		        $courseUser->unsubscribeUser($transactionIdUser, $transaction_info->id_course, $transaction_info->id_edition);
		        // unsubscribe user from the session
		        if (intval($transaction_info->id_date) > 0) {
			        LtCourseuserSession::model()->unsubscribeUser($transactionIdUser, $transaction_info->id_date);
		        }
	        }elseif($transaction_info->item_type == EcommerceTransactionInfo::TYPE_COURSEPATH){
		        // Unsubscribe user from the courses he selected to buy but reverted transaction
		        $rawCartItemData = CJSON::decode($transaction_info->item_data_json, false);

		        if($rawCartItemData && isset($rawCartItemData->courses)){
			        $selectedCourseIdsArr = $rawCartItemData->courses;

			        foreach($selectedCourseIdsArr as $idCourse){
				        $courseUser = new LearningCourseuser();
				        $courseUser->unsubscribeUser($transactionIdUser, $idCourse);
			        }

					// Check if the user is still subscribed to at least one course of the LP
					if(!empty($selectedCourseIdsArr)) {
                        $stillSubscribedToCourses = Yii::app()->getDb()->createCommand()
                            ->select('COUNT(lcu.idCourse)')
                            ->from(LearningCoursepathCourses::model()->tableName().' lcc')
                            ->join(LearningCourseuser::model()->tableName().' lcu', 'lcu.idCourse = lcc.id_item AND lcu.idUser = :transactionIdUser', array(':transactionIdUser'=>$transactionIdUser))
                            ->where('lcc.id_path=:idPath', array(':idPath'=>$transaction_info->id_path))
                            ->queryScalar();

						if(!$stillSubscribedToCourses)
							LearningCoursepathUser::model()->deleteAllByAttributes(array('id_path' => $transaction_info->id_path, 'idUser' => $transactionIdUser));
					}
		        }
	        }

	        // Delete transaction info (item)
			$transaction_info->delete();
       	}

       	// Delete Main transaction record
       	return $transactionModel->delete();
        */
        // substitute the above code with soft deletion of transaction
        $transactionModel->cancelled = 1;
        $transactionModel->done_by = EcommerceTransaction::DONE_BY_USER;
        $rs = $transactionModel->save();
        if (!$rs) {
            Yii::log(__METHOD__.' (debugging): unable to cancel transaction #'.$transactionModel->id_trans."\n".print_r($transactionModel->getErrors(), true));
        }
        return $rs;
    }

    /**
     * Complete transaction (supposedly called AFTER payment validation)
     * @todo This function is a bit PayPal specific !!!
     *
     * @param int $transaction_id
     * @param array $ipnData
     * @return boolean
     */

    /**
     * Complete transaction (supposedly called AFTER payment validation)
     *
     * @param int $transaction_id OUR Transaction Id
     * @param string $gw_trans_id Payment GATEWAY transaction Id
     * @param string $gw_receipt_id Receipt Id, if any
     * @param string $gw_data_array An array of ALL data received by payment gateway, to archive
     * @return boolean
     */
    private function completeTransaction($transaction_id, $gw_trans_id=null, $gw_receipt_id=null, $gw_data_array=null) {

        $transactionModel = EcommerceTransaction::model()->findByPk($transaction_id);

        if ( !$transactionModel ) {
            Yii::log("An attempt to activate products from an invalid transaction ($transaction_id) has been made", 'error', __CLASS__);
            return false;
        }

        $transactionItemModels = $transactionModel->transaction_items;

        if ( count($transactionItemModels) <= 0) {
            Yii::log("An attempt to activate products from an empty transaction ($transaction_id) has been made. No transaction items found.", 'error', __CLASS__);
            return false;
        }


        $idUserBuyer = $transactionModel->id_user;

        // Enumerate all transaction items and activate
        foreach ( $transactionItemModels as $transaction_item ) {

	        if($transaction_item->item_type == EcommerceTransactionInfo::TYPE_COURSE){
				// The subscription can be deleted before the callback, so enroll the user to the course if not
				$waiting = 0;
				$courseModel = LearningCourse::model()->findByPk($transaction_item->id_course);
				if($courseModel->course_type == LearningCourse::TYPE_ELEARNING && $courseModel->max_num_subscribe > 0)
				{
					$numEnrolled = $courseModel->getTotalSubscribedUsers(false, false, false);
					$waiting = $numEnrolled >= $courseModel->max_num_subscribe;
				}
				if(!LearningCourseuser::isSubscribed($idUserBuyer, $transaction_item->id_course)) {
					$courseUser = new LearningCourseuser();
					$courseUser->subscribeUser($idUserBuyer, Yii::app()->user->id, $transaction_item->id_course, $waiting);
				}

				$activated = false;
				if($waiting)
					$activated = true;
				else
					$activated = LearningCourseuser::model()->activateSubscription($idUserBuyer, $transaction_item->id_course, $transaction_item->id_edition);

		        if($activated)
				{
			        // enroll the user to the session if this is a classroom or webinar course
			        if (intval($transaction_item->id_date) > 0) {
						// Differentiate between classroom & webinar courses
						switch($transaction_item->course->course_type) {
							case LearningCourse::TYPE_CLASSROOM:
								if(!LtCourseuserSession::isSubscribed($idUserBuyer, $transaction_item->id_date)) {
									LtCourseuserSession::enrollUser($idUserBuyer, $transaction_item->id_date, LtCourseuserSession::$SESSION_USER_WAITING_LIST);
								}
								$sessionModel = LtCourseSession::model()->findByPk(array('id_session' => $transaction_item->id_date));
								if(!$sessionModel->isMaxEnrolledReached()){
									LtCourseuserSession::model()->activateSession($idUserBuyer, $transaction_item->id_date);
                                } else {
                                        $event = new DEvent($this, array(
                                            'session_id' => $sessionModel->id_session,
                                            'user' => $idUserBuyer,
                                        ));
                                        Yii::app()->event->raise(EventManager::EVENT_USER_CLASSROOM_SESSION_WAITING_FOR_APPROVAL, $event);
                                }
								break;
							case LearningCourse::TYPE_WEBINAR:
								if(!WebinarSessionUser::isSubscribed($idUserBuyer, $transaction_item->id_date)) {
									WebinarSessionUser::enrollUser($idUserBuyer, $transaction_item->id_date, WebinarSessionUser::$SESSION_USER_WAITING_LIST);
								}
								$sessionModel = WebinarSession::model()->findByPk(array('id_session' => $transaction_item->id_date));
								if(!$sessionModel->isMaxEnrolledReached()) {
									WebinarSessionUser::model()->activateSession($idUserBuyer, $transaction_item->id_date);
                                } else {
                                    $webinarSessionUser = WebinarSessionUser::model()->findByPk(array('id_session'=>$sessionModel->id_session, 'id_user' => $idUserBuyer));
                                    $event = new DEvent($this, array(
                                        'enrollment'	=> $webinarSessionUser,
                                        'session_id' => $webinarSessionUser->id_session,
                                        'user' => $idUserBuyer
                                    ));
                                    Yii::app()->event->raise(EventManager::EVENT_USER_WEBINAR_SESSION_WAITING_FOR_APPROVAL, $event);
                                }
								break;
						}
			        }
		        }
	        }elseif($transaction_item->item_type == EcommerceTransactionInfo::TYPE_COURSEPATH){
		        // Activate user's subscription for the purchased courses from Learning Plan
		        $rawCartItemData = CJSON::decode($transaction_item->item_data_json, false);

		        if($rawCartItemData && isset($rawCartItemData->courses)){
			        $selectedCourseIdsArr = $rawCartItemData->courses;
			        foreach($selectedCourseIdsArr as $idCourse){
						if(!LearningCourseuser::isSubscribed($idUserBuyer, $idCourse)) {
							$courseUser = new LearningCourseuser();
							$courseUser->subscribeUser($idUserBuyer, Yii::app()->user->id, $idCourse);
						}
						$waiting = 0;
						$courseModel = LearningCourse::model()->findByPk($idCourse);
						if($courseModel->course_type == LearningCourse::TYPE_ELEARNING && $courseModel->max_num_subscribe > 0)
						{
							$numEnrolled = $courseModel->getTotalSubscribedUsers(false, false, false);
							$waiting = $numEnrolled >= $courseModel->max_num_subscribe;
						}
						if(!$waiting)
							LearningCourseuser::model()->activateSubscription($idUserBuyer, $idCourse, 0);
			        }
		        }
	        }elseif($transaction_item->item_type == EcommerceTransactionInfo::TYPE_COURSESEATS){
                // Increase the PUs available seats to distribute
                // in the courses he has purchased seats for
                $rawCartItemData = CJSON::decode($transaction_item->item_data_json, false);

                if($rawCartItemData && isset($rawCartItemData->seats)){
                    $seatsNum = intval($rawCartItemData->seats);

                    $puCourseModel = CoreUserPuCourse::model()->findByAttributes(array(
                        'puser_id'=>$idUserBuyer,
                        'course_id'=>$transaction_item->id_course
                    ));

                    if(!$puCourseModel){
                        Yii::log('Can not increase Power User seats during cart checkout completion, because we can not find core_user_pu_course model for power user '.$idUserBuyer.' and course '.$transaction_item->id_course, CLogger::LEVEL_ERROR);
                        continue;
                    }else{
                        $puCourseModel->total_seats = $puCourseModel->total_seats + $seatsNum;
                        $puCourseModel->available_seats = $puCourseModel->available_seats + $seatsNum;
                        if(!$puCourseModel->save()){
                            Yii::log('Can not increase Power User seats during cart checkout. Error saving model: '. print_r($puCourseModel->getErrors(), true));
                            continue;
                        }else{
	                        // Successfully increased seats in course. Log audit trail
	                        Yii::app()->event->raise('onPuBuySeats', new DEvent($this, array(
		                        'idPowerUser'=>$idUserBuyer,
		                        'idCourse'=>$transaction_item->id_course,
		                        'data'=>array(
			                        'seats'=>$seatsNum
		                        )
	                        )));
                        }
                    }
                }
            }

	        $transaction_item->activated = 1;
	        $transaction_item->save();
        }

        // Mark main transaction as paid, save more transaction related data
        $transactionModel->paid = 1;
        $transactionModel->date_activated = Yii::app()->localtime->localNow;

        $transactionModel->payment_txn_id = $gw_trans_id;
        $transactionModel->raw_data_json = CJSON::encode($gw_data_array);

        // this is reveived only for 'guests' PayPal payment (credit card)
        $transactionModel->receipt_id = $gw_receipt_id;

        return $transactionModel->save();


    }

	private function sendEmailNotifications($id_trans) {

		$transactionModel = EcommerceTransaction::model()->findByPk($id_trans);
		$userModel = CoreUser::model()->findByPk($transactionModel->id_user);

		Yii::app()->event->raise('UserBoughtCourse', new DEvent($this, array(
			'user' 			=> $userModel,
			'transaction' 	=> $transactionModel
		)));

		foreach($transactionModel->transaction_items as $transaction_item)
		{
			if ($transaction_item->item_type == EcommerceTransactionInfo::TYPE_COURSE) {
				$courseModel = LearningCourse::model()->findByPk($transaction_item->id_course);
				Yii::app()->event->raise(EventManager::EVENT_USER_SUBSCRIBED_COURSE, new DEvent($this, array(
					'user' => $userModel,
					'course' => $courseModel
				)));

				if($courseModel->course_type == LearningCourse::TYPE_CLASSROOM && $transaction_item->id_date) {
					$sessionEnrollment = LtCourseuserSession::model()->findByPk(array(
						'id_user' => $transactionModel->id_user,
						'id_session' => $transaction_item->id_date
					));
					if($sessionEnrollment && $sessionEnrollment->status != LtCourseuserSession::$SESSION_USER_WAITING_LIST) {
						Yii::app()->event->raise(EventManager::EVENT_USER_ENROLLED_IN_CLASSROOM_SESSION, new DEvent($this, array(
							'session_id' => $transaction_item->id_date,
							'user' => $transactionModel->id_user,
						)));
					}
				}

				if($courseModel->course_type == LearningCourse::TYPE_WEBINAR && $transaction_item->id_date) {
					$sessionEnrollment = WebinarSessionUser::model()->findByPk(array(
						'id_user' => $transactionModel->id_user,
						'id_session' => $transaction_item->id_date
					));
					if($sessionEnrollment && $sessionEnrollment->status != WebinarSessionUser::$SESSION_USER_WAITING_LIST) {
						Yii::app()->event->raise(EventManager::EVENT_USER_ENROLLED_IN_WEBINAR_SESSION, new DEvent($this, array(
							'session_id' => $transaction_item->id_date,
							'user' => $transactionModel->id_user,
						)));
					}
				}
			}

			if ($transaction_item->item_type == EcommerceTransactionInfo::TYPE_COURSEPATH) {
				$model = LearningCoursepath::model()->findByPk($transaction_item->id_path);
				Yii::app()->event->raise(EventManager::EVENT_USER_ENROLLED_IN_LEARNING_PLAN, new DEvent($this, array(
					'user_id' => $transactionModel->id_user,
					'learning_plan' => $model
				)));
			}
		}

		return true;
	}


    /**
     * Save user's billing information;
     *
     * @param int   $idUser
     * @param array $data Supposedly an array of data coming from Checkout page form (see cart/index view)
     *
     * @return bool
     */
    private function saveBillingInfo($idUser, $data) {

        // Get current billing info record, if any
        // Create new one, if not found
        $billingInfo = CoreUserBilling::model()->getUserLastBillingInfo($idUser);
        if ( !$billingInfo ) {
            $billingInfo = new CoreUserBilling();
            $billingInfo->idst = $idUser;
        }

        // Get POSTed data
        $billingInfo->bill_company_name = isset($data["bill_company_name"]) ? $data["bill_company_name"] : "";
        $billingInfo->bill_vat_number = isset($data["bill_vat_number"]) ? $data["bill_vat_number"] : "";
        $billingInfo->bill_address1 = isset($data["bill_address1"]) ? $data["bill_address1"] : "";
        $billingInfo->bill_address2 = isset($data["bill_address2"]) ? $data["bill_address2"] : "";
        $billingInfo->bill_city = isset($data["bill_city"]) ? $data["bill_city"] : "";
        $billingInfo->bill_state = isset($data["bill_state"]) ? $data["bill_state"] : "";
        $billingInfo->bill_zip = isset($data["bill_zip"]) ? $data["bill_zip"] : "";
        $billingInfo->bill_country_code = isset($data["bill_country_code"]) ? $data["bill_country_code"] : "";

        // Get old SHA1 and calculate new one
        $sha1_old = $billingInfo->sha1_sum;
        $sha1_new = $billingInfo->sha1();

        // Different SHA1s is a signal for differet data content, so, create new record instead of save
        $ok = true;
        if ($sha1_new != $sha1_old) {
            $billingInfo->sha1_sum = $sha1_new;
            $billingInfo->id = null;  // !!! needed
            $billingInfo->setIsNewRecord(true);  // set record as new (will be INSTERTED when save()
            $ok = $billingInfo->save();
        }
        else {
            $ok = $billingInfo->save();
        }


        if (!$ok) {
            return false;
        }

        return true;

    }

	/**
	 * This is a highly specific ajax call that is only called on this scenario:
	 * 1. Current user is a PU and has a PU profile that is marked to only allow
	 * user enrollments in courses through purchasing "seats" first
	 * 2. The PU added some "more seats to buy for a given course" item in his cart
	 * 3. While in the cart he clicked "Edit" because he decided to
	 * modify the number of seats to purchase before checkout.
	 * This ajax method will display the contents of this edit seats to buy dialog
	 */
	public function actionAxEditSeatsToBuy(){
		$positionId = isset($_GET['position']) ? $_GET['position'] : -1;

		$positionModel = null;
		foreach(Yii::app()->shoppingCart->getPositions() as $id=>$position){
			if($id==$positionId){
				$positionModel = $position;
				break;
			}
		}

		if(!$positionModel){
			throw new CException('This cart position is invalid');
		}

		$this->renderPartial('edit/seats_edit_dialog', array(
			'pos_id'=>$positionId,
			'position'=>$position,
		));
	}

	public function actionAdyenResponse()
	{
		try {
			$testMode 			= strtolower(Settings::get('adyen_sandbox')) == 'on';
			$paymentGw 			= new Adyen($testMode);
			$paymentGw->doLog 	= true;
			// False by default
			$isTransactionValid = false;
			$isTransactionValid = $paymentGw->validateTransaction();

			if ($isTransactionValid) {

				// 1. Check response status
				$payment_status = Yii::app()->request->getParam('authResult', '');
				Yii::log('Successfully validated call from Adyen with status - ' . $payment_status, 'debug', __CLASS__);

				if ( $payment_status != 'AUTHORISED') {
					Yii::log("Exception: Call is validated, but Adyen returned non-completed status ($payment_status) Transaction NOT completed", 'error', __CLASS__);

					// Revert transaction and show message to the user
					$this->revertTransactionActions(Yii::app()->request->getParam('merchantReturnData', ''), Yii::app()->user->id);
					Yii::app()->user->setFlash('error', Yii::t("cart", Adyen::$KoMessages[strtoupper($payment_status)]));
					$this->redirect(Docebo::createLmsUrl("site/index"), true);
				}

				// 2. Check Transaction model
				$id_trans = Yii::app()->request->getParam('merchantReturnData', '');
				$transactionModel = EcommerceTransaction::model()->findByPk($id_trans);
				if ( $transactionModel  === null) {
					throw new CException("Call is validated, but failed to load trnasaction model for Trans Id ($id_trans). Transaction NOT completed.");
				}

				// Good, payment successfully finished, lets activate courses;
				// "invoice" is what we use for Transaction Id   receipt_id
				$complete = $this->completeTransaction(
					$id_trans,
					Yii::app()->request->getParam("pspReference", ''),
					null,
					$_GET );

				// If everything is ok, send emails
				if ($complete)  {
					$result = $this->sendEmailNotifications($id_trans);
					if (!$result) {
						Yii::log('Call is validated, transaction is completed/activated, but failed to send notification emails.', 'error', __CLASS__);
						$this->redirect('cancelled', array('id_trans',Yii::app()->request->getParam('merchantReturnData', '') ));
					}
					else {
						Yii::log("Successfully completed/activated Transaction $id_trans", 'debug', __CLASS__);

						$viewFile = 'return_gw';

						if(CoreUserPU::isPUAndSeatManager()) {
							foreach ( $transactionModel->transaction_items as $ecommerceTransactionItem ) {
								/* @var $ecommerceTransactionItem EcommerceTransactionInfo */
								if ( $ecommerceTransactionItem->item_type == EcommerceTransactionInfo::TYPE_COURSESEATS ) {
									$viewFile = 'return_gw_pu_seats_bought';
								}
							}
						}
						$this->render($viewFile, array(
							'transaction_id' => $id_trans,
							'purchased_items' => $transactionModel->getRelatedItems(),
							'transaction_success' => $isTransactionValid
						));
					}
				}
				else {
					throw new CException('Call validated, but failed to complete transaction.');
				}
			}
			else {
				throw new CException('Failed to validate call from Adyen. Data: ' . print_r($_GET,true) );
			}
			Yii::app()->end();
		}
		catch (Exception $e) {
			Yii::log("Exception: " . $e->getMessage(), 'error', __CLASS__);
			$this->redirect(Docebo::createLmsUrl('cart/cancelled', array('id_trans' => Yii::app()->request->getParam('merchantReturnData', ''))));
		}
	}

    public function actionValidateCybersource(){
        try{
            $paymentGw = new Cybersource(boolval(Settings::get('cybersource_sandbox_mode')));
            $id_trans = Yii::app()->request->getParam('req_reference_number');
            $transactionModel = EcommerceTransaction::model()->findByPk($id_trans);
            if (!$id_trans || !$transactionModel) {
                Yii::log('Failed to validate call from Cybersource. Data: ' . print_r($_POST, true), CLogger::LEVEL_INFO);
                throw new CException('No transaction id has been returned from the provider OR no active transaction');
            } else {
                $isTransactionValid = $paymentGw->validateTransaction();
                if ($isTransactionValid === true) {
                    Yii::log("Successfully completed/activated Transaction $id_trans", 'debug', __CLASS__);

                    // Good, payment successfully finished, lets activate courses;
                    $complete = $this->completeTransaction($id_trans, $_POST['transaction_id'], null, $_POST);
                    if($complete) {
                        $result = $this->sendEmailNotifications($id_trans);
                        if (!$result) {
                            Yii::log('Call is validated, transaction is completed/activated, but failed to send notification emails.', 'error', __CLASS__);
                            $this->redirect('cancelled', array('id_trans', $id_trans));
                        } else {
                            $viewFile = 'return_gw';
                            if(CoreUserPU::isPUAndSeatManager()) {
                                foreach ( $transactionModel->transaction_items as $ecommerceTransactionItem ) {
                                    /* @var $ecommerceTransactionItem EcommerceTransactionInfo */
                                    if ( $ecommerceTransactionItem->item_type == EcommerceTransactionInfo::TYPE_COURSESEATS ) {
                                        $viewFile = 'return_gw_pu_seats_bought';
                                    }
                                }
                            }
                            $this->render($viewFile, array(
                                'transaction_id' => $id_trans,
                                'purchased_items' => $transactionModel->getRelatedItems(),
                                'transaction_success' => $isTransactionValid
                            ));
                        }
                    }
                } else {
                    Yii::log('Failed to validate call from Cybersource. Data: ' . print_r($_POST, true), CLogger::LEVEL_INFO);
                    throw new CException('Operation failure!');
                }
            }
            Yii::app()->end();
        }catch (Exception $e){
//            $this->revertTransactionActions($id_trans, $transactionModel->id_user);
            Yii::log("Exception: " . $e->getMessage(), 'error', __CLASS__);
            Yii::app()->user->setFlash('error',
                Yii::t('standard', '_OPERATION_FAILURE') . '<br/>'
                . Yii::t('standard', 'Error code') . ' ' . $_POST['reason_code'] . ' - ' . $_POST['message'].'<br/>'
                . Yii::t('course', 'Please contact your administrator'));
            $this->redirect(Docebo::createLmsUrl("site/index"), true);
        }
    }

    public function actionDeclineCyberSource(){
        try {
            $paymentGw = new Cybersource(boolval(Settings::get('cybersource_sandbox_mode')));
            $transaction_id = Yii::app()->request->getParam('req_reference_number');
            if (!$transaction_id) {
                throw new CException('No transaction id has been returned from the provider');
            }
            if (!empty($transaction_id)) {
                $isTransactionValid = $paymentGw->validateCancelTransaction();
                if ($isTransactionValid) {
                    $transactionModel = EcommerceTransaction::model()->findByPk($transaction_id);
                    // Revert (i.e. "unsubscribe", delete from "waiting" list) **CURRENT** user from course(s) associated to the transaction "id_trans".
                    // User MUST be checked. Otherwise, someone can unsubscribe another user by simply using 'cancelled' URL
                    $result = $this->revertTransactionActions($transaction_id, $transactionModel->id_user);
                    if (!$result) {
                        Yii::app()->user->setFlash('error', Yii::t("standard", "_OPERATION_FAILURE"));
                    }else{
                        // transaction is reverted
                        Yii::log('transaction with id: '.$transaction_id.' has been reverted!', CLogger::LEVEL_INFO);
                    }
                }
            }
            $this->redirect(Docebo::createLmsUrl("site/index"), true);
        } catch (Exception $e) {
            Yii::log("Exception: " . $e->getMessage(), 'error', __CLASS__);
            $this->redirect(Docebo::createLmsUrl('cart/cancelled', array('id_trans' => $transaction_id)));
        }
    }
}