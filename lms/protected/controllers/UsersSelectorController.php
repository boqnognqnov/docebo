<?php

/**
 * @TODO: Rename/Refactor:  UsersSelector ==>  UniSelector (Universal Selector)
 * 		Reason:Because we can make this to "select" any kind of data. Like it is now used for selecting Courses.
 * 
 * Used to serve Users Selector Dialog  (users, groups, organizational charts, courses, ...).
 * 
 * UserSelector comprises several components/parts:
 * 
 * 		= this controller (rendering different parts of the selector, loads preselections, i.e. Controller)
 * 		= /common/components/UsersSelector.php  (constants, helper methods)
 * 		= 
 * 		= /themes/../js/usersselector.js  Single file JS class, serving all needs of the selector, capsulated, 
 * 		  isolated, no interference with global JS
 * 		= /themes/../css/usersselector.css
 * 
 * 
 * Uses:
 * 		= /common/extenstions/fancytree
 * 		= UsersSelectorCGridView (extends Yii CGridView) 
 * 		= DoceboCLinkPager (extends Yii CBasePager)
 * 		= /themes/spt/../css/DoceboPager.css
 *
 */
class UsersSelectorController extends Controller {

	// Constants used by Yii AJAX grid(s) update requests. 
	// We use them to detect if current request is a grid update call or not.
	// Based on that, we do or do NOT load certain set of data, thus greatly improving the speed 
	private static $AJAX_VAR_USERS_GRID = 'ajaxusersgrid';
	private static $AJAX_VAR_GROUPS_GRID = 'ajaxgroupsgrid';
	private static $AJAX_VAR_COURSES_GRID = 'ajaxucoursesgrid';
	private static $AJAX_VAR_PLANS_GRID = 'ajaxplansgrid';
	private static $AJAX_VAR_PUPROFULES_GRID = 'ajaxpuprofilessgrid';
	private static $AJAX_VAR_CERTIFICATON_GRID = 'ajaxcertificationgrid';
	private static $AJAX_VAR_BADGES_GRID = 'ajaxbadgesgrid';
	private static $AJAX_VAR_CONTESTS_GRID = 'ajaxcontetsgrid';
	private static $AJAX_VAR_ASSETS_GRID = 'ajaxassetsgrid';
	private static $AJAX_VAR_CHANNELS_GRID = 'ajaxchannelsgrid';

	/**
	 * Selector type, in terms of its purpose and usage: for groups management, for power user, etc. 
	 * @see UsersSelector constants
	 * @var string
	 */
	protected $type;

	/**
	 * Count of items on page
	 * @var int
	 */
	protected $itemsPerPage = null;

	/**
	 * Arbitrary name assigned to this selector instance
	 * @var string
	 */
	protected $name = UsersSelector::NAME;

	/**
	 * Sometimes, we want to pass some data to the selector;
	 * To use this functionality, caller of the selector explicitely must declare this, by setting 
	 * a REQUEST parameter to 'true':  'useSessionData'. By default session data is NOT considered
	 * @var boolean|numeric
	 */
	protected $useSessionData = false;

	/**
	 * If useSessionData is requested, additionaly preserveSessionData can be also requested, which will keep session data upon reading.
	 * Normally, reading selector session data is DESTRUCTIVE: data is destroyed upon reading to prevent misselections
	 * @var boolean
	 */
	protected $preserveSessionData = false;

	/**
	 * Selector modal dialog title
	 * @var string
	 */
	protected $title = '';

	/**
	 * Various class attributes loaded with data upon requests execution.
	 * Used on case-by-case bassis, depending on the $type
	 * @var mixed
	 */
	protected $idGroup;	 // In case of Groups management users selector, the group ID
	protected $idUser;
	protected $idCourse;	// ... similar logic
	protected $idReport;	// Custom report ID
	protected $idReportType;			// Custom report type being managed where this selector is taking part of
	protected $idCatalog;	// Catalog Users selection
	protected $idPlan;	 // Curricula ID
	protected $idPuProfile;	// Power USer Profile, which is a group idst
	protected $idSession;	// Classroom session ID
	protected $idModule;	// Main menu module ID
	protected $idEnrollRule;   // Enrollment Rule ID
	protected $idBadge;	 // Badge ID used in gamification assign badges
	protected $idAsset;	 //
	protected $idChannel;	//
	protected $idOrg;	 // Orgchart node ID for "Orgchart assign users" selector
	protected $mode;					// Multipuspose parameter to use in various use-case scenarios
	protected $idForm;	 // Selector FORM ID; defaults to 'users-selector-form'
	protected $idGeneral;	// Just general ID
	protected $idMenu;	 // Menu item ID
	protected $rule_branches;   // Rule branches
	protected $notificationType;  // Notification type
	// This is very important: Sometimes selector is launched in a page, where Yii Grid/List view JS is already loaded.
	// We should know this in advance in PHP code (because we know where we gonna load the selector)
	// In such cases pass 'noyiiviewjs'	=> true in parameters list of usersSelector/axOpen URL
	protected $noyiixviewjs = false;   // Should we suppress registering Yii grid/list view JS?

	/**
	 * String. Comma separated list of tabs to show in the selector. If left false, system defined tab(s) for selected selector type will be used
	 * See UsersSelector::tabIsEnabled()
	 */
	protected $tabs = false;

	/**
	 * Path alias to view(s) to display on top/bottom part of the selector (will resolve first as relative to UsersSelector views path)
	 * Example:  'plugin.NotificationApp.views.someview'.
	 * 
	 * These panels will be rendered partially with ProcessOutput = FALSE to avoid messing with selector JS/CSS, which is already too complex!
	 *
	 * @var string
	 */
	protected $tp;	// Top Panel
	protected $bp;  // Bottom Panel

	/**
	 * Provide possibility to ask the selector (controller) to run in basic wizard mode (no collector url, nothing else... just yes/no).
	 * Passing 'wizmode=1' to controller will make the selector to run in Wizard mode, with collector URL equal to the main page.
	 * 
	 * @var integer 0|1
	 */
	protected $wizmode; // Wizard Mode?? (0|1)
	// Show/No show navigation buttons
	protected $showNavButtons = true;
	protected $showLeftBar = true;
	protected $search_term;
	protected $tab = false;
	protected $collector_url;

	/**
	 * Name of globaly available Javascript function to call on auto-close.success
	 * @var string
	 */
	protected $successCallback;

	/**
	 * List of users/groups/etc. to preselect in the selector grids
	 *
	 * @var array
	 */
	protected $preselectedUsers = array();
	protected $preselectedGroups = array();
	protected $preselectedBranches = array();
	protected $preselectedCourses = array();
	protected $preselectedPlans = array();
	protected $preselectedPuProfiles = array();
	protected $preselectedCertification = array();
	protected $preselectedBadges = array();
	protected $preselectedContests = array();
	protected $preselectedAssets = array();
	protected $preselectedChannels = array();

	/**
	 * List of users/groups/etc. to exclude from selector
	 * 
	 * @var array
	 */
	protected $excludeUsers = array();
	protected $excludeGroups = array();
	protected $excludeBranches = array();
	protected $excludeCourses = array();
	protected $excludePlans = array();
	protected $excludePuProfiles = array();
	protected $excludeAssets = array();
	protected $excludeChannels = array();

	/**
	 * List of users/groups/etc. to filter data providers with. 
	 * All other IDs will be ignored and not presented. 
	 * For example, to show only users enrolled into a given course and ignore the rest.
	 * 
	 * @var array
	 */
	protected $filterUsers;
	protected $filterGroups;
	protected $filterBranches;
	protected $filterCourses;
	protected $filterPlans;
	protected $filterPuProfiles;
	protected $filterAssets;
	protected $filterChannels;
	protected $userLevelOnly = false;
	protected $showSuspended = false;

	/**
	 * FancyTree selection mode; default is 10 (this/this & desc)
	 * @var number
	 */
	protected $orgChartSelectMode = FancyTree::SELECT_MODE_DEFAULT;
	protected $wizardParams = array();
	// @TODO MOVE this to UsersCSelector components as constant (and everything else that is suitable)
	protected $orgchartsFancyTreeId = 'orgcharts-fancytree';

	/**
	 * Boolean flag, set at the begining of controller run, to indicate if we are in a "grid update" call/request
	 * @var voolean
	 */
	protected $inUsersGridUpdate = false;
	protected $inGroupsGridUpdate = false;
	protected $inCoursesGridUpdate = false;
	protected $inPlansGridUpdate = false;
	protected $inPuProfilesGridUpdate = false;
	protected $inCertificationGridUpdate = false;
	protected $inBadgesGridUpdate = false;
	protected $inContestsGridUpdate = false;
	protected $inAssetsGridUpdate = false;
	protected $inChannelsGridUpdate = false;
	protected $inGridUpdate = false;

	/**
	 * Class variable names which are passed to users selector as an array for further usage.
	 * They are NOT all used at the same time, and we pass only NON nulls
	 * 
	 * @var array
	 */
	private static $SELECTOR_PARAMS_NAMES = array(
		'preserveSessionData',
		'idGroup',
		'idCourse',
		'idUser',
		'idReport',
		'idReportType',
		'idCatalog',
		'idPlan',
		'idPuProfile',
		'idSession',
		'idModule',
		'idEnrollRule',
		'idBadge',
		'idAsset',
		'idChannel',
		'idOrg',
		'mode',
		'idForm',
		'successCallback',
		'idGeneral',
		'noyiixviewjs',
		'tabs',
		'tp',
		'bp',
		'wizmode',
		'idMenu',
		'rule_branches',
		'notificationType'
	);

	/**
	 * (non-PHPdoc)
	 * @see CController::init()
	 */
	public function init() {
		parent::init();

		$this->showSuspended = (bool) Yii::app()->request->getParam('showSuspended', false);
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl',
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		return array(
			array('allow',
				'users' => array('@'),
				'expression' => "(Yii::app()->user->isGodAdmin || Yii::app()->user->getIsPu())",
			),
			array('deny',
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
			),
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::beforeAction()
	 */
	public function beforeAction($action) {

		$this->collectRequestInput();
		$this->validateInput();
		$this->prepareData();

		return parent::beforeAction($action);
	}

	/**
	 * Open Users selector dialog
	 */
	public function actionAxOpen() {
		switch ($this->type) {

			case UsersSelector::TYPE_GROUPS_MANAGEMENT:
				$this->collector_url = Docebo::createAdminUrl('groupManagement/assignUsersToGroup', array(
							'idGroup' => $this->idGroup
				));
				break;

			case UsersSelector::TYPE_AUTOMATION_RULE:
				$this->tabs = implode(",", UsersSelector::$TABS_PRESET_GROUPS_CHARTS_ONLY);

				$this->collector_url = Docebo::createAdminUrl('AutomationApp/ruleWizard/finishGroupBranchSelection', array(
				));

				break;

			case UsersSelector::TYPE_POWERUSER_MANAGEMENT:
				$this->collector_url = Docebo::createAdminUrl('PowerUserApp/powerUserManagement/assignUsersToPowerUser', array(
							'idUser' => $this->idUser,
				));
				break;

			// Enroll users to a given course; It is a wizard of 2 steps:
			// Step 1: Select users (showin users selector)
			case UsersSelector::TYPE_COURSE_ENROLLMENT:
				$this->wizardParams = array(
					'wizard_mode' => true,
					'first' => true,
					'last' => false,
					'final' => false,
					'next_url' => Docebo::createAdminUrl('courseManagement/axEnrollUsersToSingleCourse'),
				);
				$this->collector_url = ""; // NOt important in Wizard mode
				if (false === $this->title)
					$this->title = Yii::t('standard', 'Enroll users');

				break;

			case UsersSelector::TYPE_CATALOG_MANAGEMENT:
				$this->collector_url = Docebo::createAdminUrl('CoursecatalogApp/CourseCatalogManagement/assignUsersToCatalog', array(
							'idCatalog' => $this->idCatalog
				));
				break;

			case UsersSelector::TYPE_CATALOG_MANAGEMENT_COURSES:
				if (PluginManager::isPluginActive("CurriculaApp")) {
					$this->tabs = implode(",", UsersSelector::$TABS_PRESET_COURSES_PLANS_ONLY);
				} else {
					$this->tabs = implode(",", UsersSelector::$TABS_PRESET_COURSES_ONLY);
				}
				$this->collector_url = Docebo::createAdminUrl('CoursecatalogApp/CourseCatalogManagement/assignCoursesToCatalog', array(
							'idCatalog' => $this->idCatalog
				));
				break;

			case UsersSelector::TYPE_CURRICULA_MANAGEMENT:
				$this->collector_url = Docebo::createAdminUrl('CurriculaApp/curriculaManagement/assignUsersToPlan', array(
							'idPlan' => $this->idPlan,
				));

				break;

			case UsersSelector::TYPE_CLASSROOM_ENROLLMENT:
				if (LearningCourse::isCourseSelling($this->idCourse) && CoreUserPU::isPUAndSeatManager()) {
					// Check if the PU has at least some seats available in this course
					if (!CoreUserPuCourse::checkIfPuHasEnoughSeatsInCourse(1, $this->idCourse)) {
						Yii::app()->user->setFlash('error', Yii::t('standard', "You don't have enough seats for this course"));
					}
				}
				$this->collector_url = Docebo::createAdminUrl('ClassroomApp/enrollment/enrollUsers', array(
							'idCourse' => $this->idCourse,
							'idSession' => $this->idSession,
				));

				break;

			case UsersSelector::TYPE_MODULE_MANAGEMENT:
				$this->collector_url = Docebo::createAdminUrl('branding/assignUsersToModule', array(
							'idModule' => $this->idModule,
				));
				break;


			case UsersSelector::TYPE_CUSTOM_REPORTS_USERS:
				$this->wizardParams = array(
					'wizard_mode' => true,
				);
				break;

			case UsersSelector::TYPE_CUSTOM_REPORT_ASSETS_STATS:
				$this->wizardParams = array(
					'wizard_mode' => true,
				);
				break;

			case UsersSelector::TYPE_CUSTOM_REPORT_CHANNELS_STATS:
				$this->wizardParams = array(
					'wizard_mode' => true,
				);
				break;
			case UsersSelector::TYPE_CUSTOM_REPORT_EXPERT_STATS:
				$this->wizardParams = array(
					'wizard_mode' => true,
				);
				break;


			case UsersSelector::TYPE_CUSTOM_REPORTS_COURSES:
				// Allow custom report types to modify the available tabs in the course selector step
				if (isset($_REQUEST['idReportType'])) {
					switch ($_REQUEST['idReportType']) {
						case LearningReportType::ECOMMERCE_TRANSACTIONS:
							$this->tabs = 'courses,plans';
							break;
						case LearningReportType::USERS_COURSEPATH:
							$this->tabs = 'plans';
							break;
					}
				}
				$this->wizardParams = array(
					'wizard_mode' => true,
				);
				break;

			case UsersSelector::TYPE_CUSTOM_REPORT_CERTIFICATION:
				$this->wizardParams = array(
					'wizard_mode' => true,
				);
				break;

			case UsersSelector::TYPE_CUSTOM_REPORT_BADGES:
				$this->wizardParams = array(
					'wizard_mode' => true,
				);
				break;

			case UsersSelector::TYPE_ENROLLRULES_BRANCHES:
			case UsersSelector::TYPE_ENROLLRULES_GROUPS:
			case UsersSelector::TYPE_ENROLLRULES_COURSES:
				$this->collector_url = Docebo::createAdminUrl('enrollmentRules/assignItemsToRule');
				break;

			case UsersSelector::TYPE_NEWSLETTER_RECIPIENTS:
				$this->collector_url = Docebo::createAdminUrl('newsletter/collectUsersList');
				break;

			case UsersSelector::TYPE_GAMIFICATION_ASSIGN_BADGES:
				$this->collector_url = Docebo::createAdminUrl('GamificationApp/gamificationApp/assignUsersToBadge');
				break;

			case UsersSelector::TYPE_ORGCHART_ASSIGN_USERS:
				$this->collector_url = Docebo::createAdminUrl('orgManagement/assignUsersV2', array(
							'idOrg' => $this->idOrg,
							'chuncked' => true,
				));
				break;


			case UsersSelector::TYPE_SUBSCRIPTIONCODES_COURSES:
				$this->collector_url = Docebo::createAdminUrl('SubscriptionCodesApp/SubscriptionCodesApp/assignCoursesToSet');
				break;


			case UsersSelector::TYPE_MASS_ENROLLMENT_USERS:
			case UsersSelector::TYPE_MASS_ENROLLMENT_COURSES:
				$this->wizardParams = array(
					'wizard_mode' => true,
					'first' => false,
					'last' => false,
					'final' => false,
				);
				if ($this->type == UsersSelector::TYPE_MASS_ENROLLMENT_USERS) {
					$this->wizardParams['first'] = true;
				}
				$this->collector_url = '';
				break;


			case UsersSelector::TYPE_NOTIFICATIONS_USER_FILTER:
			case UsersSelector::TYPE_NOTIFICATIONS_COURSES_ASSOC:
				// We use the same selector in two environments: standalone and wizard. Check for wizmode=1 first
				if ($this->wizmode !== null) {
					if (!$this->wizardParams) {
						$this->wizardParams = array(
							'wizard_mode' => true,
						);
					}
					// Direct all sumbits to the wizard action
					$this->collector_url = Docebo::createAdminUrl('notification/notificationWizard', array(
								'id' => $this->idGeneral,
					));
				}
				// In No wizard mode, pass the collector URL to the controller! (collectorUrl=xxxxxx) 
				break;

			case UsersSelector::TYPE_CERTIFICATIONS_ASSIGN_ITEMS:
				$this->collector_url = Docebo::createAdminUrl('//CertificationApp/CertificationApp/assignItems', array(
							'id_cert' => $this->idGeneral,
				));
				break;


			case UsersSelector::TYPE_DASHBOARD_USER_FILTER:
				$this->collector_url = Docebo::createAdminUrl('mydashboard/dash/userFilter', array(
							'id' => $this->idGeneral,
				));
				break;


			case UsersSelector::TYPE_DASHBOARD_PU_PROFULES:
				$this->collector_url = Docebo::createAdminUrl('mydashboard/dash/assignPuProfiles', array(
							'id' => $this->idGeneral,
				));
				break;


			case UsersSelector::TYPE_MASS_ENROLL_LP_USERS:
			case UsersSelector::TYPE_MASS_ENROLL_LP_COURSES:
				$this->collector_url = Docebo::createLmsUrl('test2/wizardDemo2Dispatcher', array(
							'idPlan' => $this->idPlan,
				));

				Yii::app()->event->raise('OnTypeMassUsersCoursesEnroll', new DEvent($this, array(
					'url' => &$this->collector_url,
					'idPlan' => &$this->idPlan,
					'type' => &$this->type
				)));

				break;

			// WIZARD TEST/DEMO	
			case 'wizarddemo':
				$this->wizardParams = array(
					'wizard_mode' => true,
					'first' => false,
					'last' => false,
					'final' => false,
					'next_url' => '/lms/?r=test2/axStep3',
					'prev_url' => '/lms/?r=test2/axStep1',
					'finish_url' => '/lms/?r=test2/axWizardFinish',
				);
				break;


			case UsersSelector::TYPE_WEBINAR_ENROLLMENT:
				if (LearningCourse::isCourseSelling($this->idCourse) && CoreUserPU::isPUAndSeatManager()) {
					// Check if the PU has at least some seats available in this course
					if (!CoreUserPuCourse::checkIfPuHasEnoughSeatsInCourse(1, $this->idCourse)) {
						Yii::app()->user->setFlash('error', Yii::t('standard', "You don't have enough seats for this course"));
					}
				}
				$this->collector_url = Docebo::createAdminUrl('webinar/enrollment/enrollUsers', array(
							'idCourse' => $this->idCourse,
							'idSession' => $this->idSession,
				));
				break;

			case UsersSelector::TYPE_FILTER_MENU_ITEMS_FOR_USERS:
				$this->collector_url = Docebo::createAdminUrl('menuManagement/filterMenuItem', array(
							'idMenu' => $this->idMenu,
				));
				break;

			case UsersSelector::TYPE_APP7020_EXPERTS:
				$experts = Yii::app()->request->getParam("preselectedExperts", array());
				if ($experts) {
					$this->preselectedUsers = $experts;
				}
				break;

			case UsersSelector::TYPE_APP7020_TOPICS:

				$groups = Yii::app()->request->getParam("preselectedGroups", array());
				$branches = Yii::app()->request->getParam("preselectedBranches", array());
				if ($groups) {
					$this->preselectedGroups = $groups;
				}
				$_SESSION['preselectedBranches'] = null;
				if ($branches) {
					foreach ($branches as $key=>$value) {
						$this->preselectedBranches[] = $branches = array('key' => $value['key'], 'selectState' => $value['selectState']);
					}
					$_SESSION['preselectedBranches'] = $this->preselectedBranches;
				}

				break;

			case UsersSelector::TYPE_GAMIFICATION_LEADERBOARD:
			case UsersSelector::TYPE_CUSTOM_REPORT_CONTESTS:
				$this->wizardParams = array(
					'wizard_mode' => true,
				);
				break;
			case UsersSelector::TYPE_GAMIFICATION_LEADERBOARD_NO_WIZARD:
			case UsersSelector::TYPE_REWARDS_SET_FILTER:
			case UsersSelector::TYPE_GAMIFICATION_CONTEST_NO_WIZARD:
				break;

			case UsersSelector::TYPE_REPORT_VISIBILITY:
				$canEdit = LearningReportAccess::checkPermissionToEdit(Yii::app()->user->id, intval($_GET['id']));
				if (!$canEdit) {
					echo "don't have access to do this!";
					Yii::app()->end();
				}
				$this->collector_url = Docebo::createAdminUrl('reportManagement/showAccessFilter', array('id' => intval($_GET['id'])));
				$this->tp = 'admin.protected.views.reportManagement._filterForm';
				break;

			default:
				// Trigger event to let plugins add their own user selector handler
				$event = new DEvent($this, array());
				Yii::app()->event->raise("HandleCustomUserSelector", $event);
				if (!$event->shouldPerformAsDefault() && isset($event->return_value['collector_url'])) {
					$this->collector_url = $event->return_value['collector_url'];
				}
				break;
		}

		$this->dialog();
	}

	/**
	 * Real opening of the dialog
	 */
	protected function dialog() {

		if(!isset($_REQUEST[self::$AJAX_VAR_USERS_GRID])) {

			/*
			 * Clear session for Users Select All when open a dialog window,
			 * but keep it when in Groups tab
			 * */
			if(Yii::app()->getRequest()->getParam('ajaxgroupsgrid', false) == false) {
				unset(Yii::app()->session['usingSearchForUsers']);
				unset(Yii::app()->session['usersSelectorPage']);
				Yii::app()->session['usersSelectorPage'] = 1;
			}
		}

		// Resolve Users Grid id based on incoming request (ajaxVar)
		$ajaxVarUsersGrid = self::$AJAX_VAR_USERS_GRID;
		if (isset($_REQUEST[$ajaxVarUsersGrid])) {

			if(Yii::app()->getRequest()->getParam('page', false) !== false) {
				if(Yii::app()->getRequest()->getParam('page', false) !== Yii::app()->session['usersSelectorPage'] && Yii::app()->session['usersSelectorPage'] !== null) {
					Yii::app()->session['usersSelectorPage'] = Yii::app()->getRequest()->getParam('page', false);
				} else {
					unset(Yii::app()->session['usersSelectorPage']);
				}
			} else {
				if(!is_null(Yii::app()->session['usersSelectorPage']) && Yii::app()->session['usersSelectorPage'] !== 1) {
					Yii::app()->session['usersSelectorPage'] = 1;
				} else {
					unset(Yii::app()->session['usersSelectorPage']);
				}
			}

			// Set $this->search_term for Users Select All in session
			if(Yii::app()->session['usersSelectorPage'] === null) {
				Yii::app()->session['usingSearchForUsers'] = $this->search_term;
			}

			$usersGridId = $_REQUEST[$ajaxVarUsersGrid];
		} else {
			$usersGridId = 'users-selector-users-grid-' . time();
		}

		// Get data provider for Users
		if(Yii::app()->getRequest()->getParam('ajaxusersgrid', false)) {
			if(Yii::app()->getRequest()->getParam('page', false) !== false) {
				Yii::app()->session['usersSelectorPage'] = Yii::app()->getRequest()->getParam('page', false);
			} else {
				Yii::app()->session['usersSelectorPage'] = 1;
			}
			$usersDataProvider = $this->dataProviderUsersSellAll();
		} else {
			$usersDataProvider = $this->dataProviderUsers();
		}



		if(!isset($_REQUEST[self::$AJAX_VAR_GROUPS_GRID])) {

			/*
			 * Clear session for Groups Select All when open a dialog window,
			 * but keep it when in Users tab
			 * */
			if(Yii::app()->getRequest()->getParam('ajaxusersgrid', false) == false) {
				unset(Yii::app()->session['usingSearchForGroups']);
				unset(Yii::app()->session['groupsSelectorPage']);
				Yii::app()->session['groupsSelectorPage'] = 1;
			}
		}

		// Resolve Groups Grid id based on incoming request (ajaxVar)
		$ajaxVarGroupsGrid = self::$AJAX_VAR_GROUPS_GRID;
		if (isset($_REQUEST[$ajaxVarGroupsGrid])) {

			if(Yii::app()->getRequest()->getParam('page', false) !== false) {
				if(Yii::app()->getRequest()->getParam('page', false) !== Yii::app()->session['groupsSelectorPage'] && Yii::app()->session['groupsSelectorPage'] !== null) {
					Yii::app()->session['groupsSelectorPage'] = Yii::app()->getRequest()->getParam('page', false);
				} else {
					unset(Yii::app()->session['groupsSelectorPage']);
				}
			} else {
				if(!is_null(Yii::app()->session['groupsSelectorPage']) && Yii::app()->session['groupsSelectorPage'] !== 1) {
					Yii::app()->session['groupsSelectorPage'] = 1;
				} else {
					unset(Yii::app()->session['groupsSelectorPage']);
				}
			}

			// Set $this->search_term for Groups Select All in session
			if(Yii::app()->session['groupsSelectorPage'] === null) {
				Yii::app()->session['usingSearchForGroups'] = $this->search_term;
			}

			$groupsGridId = $_REQUEST[$ajaxVarGroupsGrid];
		} else {
			$groupsGridId = 'users-selector-groups-grid-' . time();
		}

		// Get data provider for Groups
		if(Yii::app()->getRequest()->getParam('ajaxgroupsgrid', false)) {
			if(Yii::app()->getRequest()->getParam('page', false) !== false) {
				Yii::app()->session['groupsSelectorPage'] = Yii::app()->getRequest()->getParam('page', false);
			} else {
				Yii::app()->session['groupsSelectorPage'] = 1;
			}
			$groupsDataProvider = $this->dataProviderGroupsSellAll();
		} else {
			$groupsDataProvider = $this->dataProviderGroups();
		}

		// Get other data providers

		$coursesDataProvider = $this->dataProviderCourses();

		$plansDataProvider = $this->dataProviderPlans();

		$puProfilesDataProvider = $this->dataProviderPuProfiles();

		$certificationDataProvider = (PluginManager::isPluginActive("CertificationApp") ? $this->certificationDataProvider() : null);

		$badgesDataProvider = (PluginManager::isPluginActive("GamificationApp") ? $this->dataProviderBadges() : null);

		$contestsDataProvider = (PluginManager::isPluginActive("GamificationApp") ? $this->dataProviderContests() : null);

		$assetsDataProvider = $this->dataProviderAssets();

		$channelsDataProvider = $this->dataProviderChannels();

		// Resolve Courses Grid id based on incoming request (ajaxVar)
		$ajaxVarCoursesGrid = self::$AJAX_VAR_COURSES_GRID;
		if (isset($_REQUEST[$ajaxVarCoursesGrid])) {
			$coursesGridId = $_REQUEST[$ajaxVarCoursesGrid];
		} else {
			$coursesGridId = 'users-selector-courses-grid-' . time();
		}


		// Resolve Plans Grid id based on incoming request (ajaxVar)
		$ajaxVarPlansGrid = self::$AJAX_VAR_PLANS_GRID;
		if (isset($_REQUEST[$ajaxVarPlansGrid])) {
			$plansGridId = $_REQUEST[$ajaxVarPlansGrid];
		} else {
			$plansGridId = 'users-selector-plans-grid-' . time();
		}

		// Resolve Pu Profiles Grid id based on incoming request (ajaxVar)
		$ajaxVarPuProfilesGrid = self::$AJAX_VAR_PUPROFULES_GRID;
		if (isset($_REQUEST[$ajaxVarPuProfilesGrid])) {
			$puProfilesGridId = $_REQUEST[$ajaxVarPuProfilesGrid];
		} else {
			$puProfilesGridId = 'users-selector-puprofiles-grid-' . time();
		}

		$ajaxVarCertificationGrid = self::$AJAX_VAR_CERTIFICATON_GRID;
		if (isset($_REQUEST[$ajaxVarCertificationGrid])) {
			$certificationGridId = $_REQUEST[$ajaxVarCertificationGrid];
		} else {
			$certificationGridId = 'users-selector-certification-grid-' . time();
		}

		$ajaxVarBadgesGrid = self::$AJAX_VAR_BADGES_GRID;
		if (isset($_REQUEST[$ajaxVarBadgesGrid])) {
			$badgesGridId = $_REQUEST[$ajaxVarBadgesGrid];
		} else {
			$badgesGridId = 'users-selector-badges-grid-' . time();
		}

		$ajaxVarContestsGrid = self::$AJAX_VAR_CONTESTS_GRID;
		if (isset($_REQUEST[$ajaxVarContestsGrid])) {
			$contestsGridId = $_REQUEST[$ajaxVarContestsGrid];
		} else {
			$contestsGridId = 'users-selector-contests-grid-' . time();
		}

		$ajaxVarAssetsGrid = self::$AJAX_VAR_ASSETS_GRID;
		if (isset($_REQUEST[$ajaxVarAssetsGrid])) {
			$assetsGridId = $_REQUEST[$ajaxVarAssetsGrid];
		} else {
			$assetsGridId = 'users-selector-assets-grid-' . time();
		}

		$ajaxVarChannelsGrid = self::$AJAX_VAR_CHANNELS_GRID;
		if (isset($_REQUEST[$ajaxVarChannelsGrid])) {
			$channelsGridId = $_REQUEST[$ajaxVarChannelsGrid];
		} else {
			$channelsGridId = 'users-selector-channels-grid-' . time();
		}


		$selectorParams = array();
		$selectorParams['type'] = $this->type;
		$selectorParams['name'] = $this->name;
		foreach (self::$SELECTOR_PARAMS_NAMES as $name) {
			if (null !== $this->$name) {
				$selectorParams[$name] = $this->$name;
			}
		}

		// Make sure all INTEGERS are.. integeres (JS will cry or will give bad results if they are strings)
		$tmp = array();
		if (is_array($this->preselectedUsers)) {
			foreach ($this->preselectedUsers as $i => $id) {
				$tmp[] = (int) $id;
			}
		}
		$this->preselectedUsers = $tmp;

		if(Yii::app()->user->getIsPu()){
			$puAssignedUsers = CoreUserPU::getList();

			if($puAssignedUsers && is_array($puAssignedUsers)){
				$tmp = array();
				foreach ($this->preselectedUsers as $i => $id) {
					if(in_array($id, $puAssignedUsers)){
						$tmp[] = (int) $id;
					}
				}
				$this->preselectedUsers = $tmp;
			}
		}

		$tmp = array();
		if (is_array(($this->preselectedGroups))) {
			foreach ($this->preselectedGroups as $i => $id) {
				$tmp[] = (int) $id;
			}
		}
		$this->preselectedGroups = $tmp;

		$tmp = array();
		if (is_array(($this->preselectedBranches))) {
			foreach ($this->preselectedBranches as $i => $branch) {
				$tmp[] = array('key' => (int) $branch['key'], 'selectState' => (int) $branch['selectState']);
			}
		}
		$this->preselectedBranches = $tmp;

		$tmp = array();
		if (is_array(($this->preselectedCourses))) {
			foreach ($this->preselectedCourses as $i => $id) {
				$tmp[] = (int) $id;
			}
		}
		$this->preselectedCourses = $tmp;

		$tmp = array();
		if (is_array(($this->preselectedPlans))) {
			foreach ($this->preselectedPlans as $i => $id) {
				$tmp[] = (int) $id;
			}
		}
		$this->preselectedPlans = $tmp;


		$tmp = array();
		if (is_array(($this->preselectedPuProfiles))) {
			foreach ($this->preselectedPuProfiles as $i => $id) {
				$tmp[] = (int) $id;
			}
		}
		$this->preselectedPuProfiles = $tmp;

		$tmp = array();
		if (is_array($this->preselectedCertification)) {
			foreach ($this->preselectedCertification as $i => $id) {
				$tmp[] = (int) $id;
			}
		}
		$this->preselectedCertification = $tmp;

		$tmp = array();
		if (is_array($this->preselectedBadges)) {
			foreach ($this->preselectedBadges as $i => $id) {
				$tmp[] = (int) $id;
			}
		}
		$this->preselectedBadges = $tmp;

		$tmp = array();
		if (is_array($this->preselectedContests)) {
			foreach ($this->preselectedContests as $i => $id) {
				$tmp[] = (int) $id;
			}
		}
		$this->preselectedContests = $tmp;


		$tmp = array();
		if (is_array($this->preselectedAssets)) {
			foreach ($this->preselectedAssets as $i => $id) {
				$tmp[] = (int) $id;
			}
		}
		$this->preselectedAssets = $tmp;


		$tmp = array();
		if (is_array($this->preselectedChannels)) {
			foreach ($this->preselectedChannels as $i => $id) {
				$tmp[] = (int) $id;
			}
		}
		$this->preselectedChannels = $tmp;


		// jQuery is definitely already loaded as well as Jquery UI (at least in Admin layout)
		Yii::app()->clientScript->scriptMap = array(
			'jquery.js' => false,
			'jquery.min.js' => false,
			'jquery-ui.min.js' => false,
		);

		if ($this->noyiixviewjs) {
			Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;
			Yii::app()->clientScript->scriptMap['jquery.yiilistview.js'] = false;
			Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
		}
		$withoutCourses = false;
		$notification = CoreNotification::model()->findByPk($this->idGeneral);
		$type = '';
		if (!$notification) {
			$type = $_SESSION['notifType'];
		} else {
			$type = $notification->type;
		}
		if ($type == CoreNotification::NTYPE_NEW_COURSE_UNLOCKED_IN_LEARNING_PLAN && strpos($this->tabs, 'plans') !== false) {
			$this->tabs = 'plans';
			$withoutCourses = true;
		}

		$this->renderPartial('index', array(
			'formAction' => $this->collector_url,
			'usersDataProvider' => $usersDataProvider,
			'groupsDataProvider' => $groupsDataProvider,
			'coursesDataProvider' => $coursesDataProvider,
			'plansDataProvider' => $plansDataProvider,
			'puProfilesDataProvider' => $puProfilesDataProvider,
			'certificationDataProvider' => $certificationDataProvider,
			'badgesDataProvider' => $badgesDataProvider,
			'contestsDataProvider' => $contestsDataProvider,
			'assetsDataProvider' => $assetsDataProvider,
			'channelsDataProvider' => $channelsDataProvider,
			'wizardParams' => $this->wizardParams,
			'usersGridId' => $usersGridId,
			'ajaxVarUsersGrid' => $ajaxVarUsersGrid,
			'preselectedUsers' => $this->preselectedUsers,
			'groupsGridId' => $groupsGridId,
			'ajaxVarGroupsGrid' => $ajaxVarGroupsGrid,
			'preselectedGroups' => $this->preselectedGroups,
			'coursesGridId' => $coursesGridId,
			'ajaxVarCoursesGrid' => $ajaxVarCoursesGrid,
			'preselectedCourses' => $this->preselectedCourses,
			'plansGridId' => $plansGridId,
			'ajaxVarPlansGrid' => $ajaxVarPlansGrid,
			'preselectedPlans' => $this->preselectedPlans,
			'puProfilesGridId' => $puProfilesGridId,
			'ajaxVarPuProfilesGrid' => $ajaxVarPuProfilesGrid,
			'preselectedPuProfiles' => $this->preselectedPuProfiles,
			'certificationGridId' => $certificationGridId,
			'ajaxVarCertificationGrid' => $ajaxVarCertificationGrid,
			'preselectedCertifications' => $this->preselectedCertification,
			'badgesGridId' => $badgesGridId,
			'ajaxVarBadgesGrid' => $ajaxVarBadgesGrid,
			'preselectedBadges' => $this->preselectedBadges,
			'contestsGridId' => $contestsGridId,
			'ajaxVarContestsGrid' => $ajaxVarContestsGrid,
			'preselectedContests' => $this->preselectedContests,
			'assetsGridId' => $assetsGridId,
			'ajaxVarAssetsGrid' => $ajaxVarAssetsGrid,
			'preselectedAssets' => $this->preselectedAssets,
			'channelsGridId' => $channelsGridId,
			'ajaxVarChannelsGrid' => $ajaxVarChannelsGrid,
			'preselectedChannels' => $this->preselectedChannels,
			'preselectedBranches' => $this->preselectedBranches,
			'selectorParams' => $selectorParams,
			'showNavButtons' => $this->showNavButtons,
			'formId' => $this->idForm,
			'successCallback' => $this->successCallback ? $this->successCallback : 'usersSelectorSuccessCallback',
			'tabs' => $this->tabs,
			'topPanel' => $this->tp,
			'bottomPanel' => $this->bp,
			'withoutCourses' => $withoutCourses
				), false, true);
	}

	/**
	 * Return list of found users as a response to "Autocomplete" (search as you type) request
	 * See http://jqueryui.com/autocomplete/ for the response format:
	 * array( array('label' => 'A Label',  'value' => 'A Value'), ... ) 
	 * 
	 */
	public function actionAxUsersAutocomplete() {

		$maxNumber = Yii::app()->request->getParam('max_number', 10);

		// Return FCBKcomplete compliant JSON (https://github.com/emposha/FCBKcomplete)
		// By default the returned format is for jQueryUI  Autocomplete
		$fcbkVariant = Yii::app()->request->getParam('fcbk', false);
		$fcbkTag = Yii::app()->request->getParam('tag', false);
		if ($fcbkVariant) {
			$this->search_term = $fcbkTag;
		}
		$type = Yii::app()->request->getParam('type', false);
		if($type && $type == UsersSelector::TYPE_CUSTOM_REPORTS_USERS)
			$this->showSuspended = true;

		// Search & Return user emails 
		$emailsMode = Yii::app()->request->getParam('emails', false);


		$usersDataProvider = $this->dataProviderUsers();
		$usersDataProvider->setPagination(array(
			'pageSize' => $maxNumber,
		));

		$data = $usersDataProvider->getData();

		$result = array();
		if ($data) {
			foreach ($data as $item) {
				$name = $item['firstname'] . " " . $item['lastname'];
				$label = ltrim($item['userid'], '/') . (trim($name) ? ' - ' . trim($name) : '');
				$value = ltrim($item['userid'], '/');

				// Onle emails returned
				if ($emailsMode) {
					$label = $name ? "&lt;" . $name . "&gt; " . $item['email'] : $item['email'];
					$value = $item['email'];
				}

				// FCBK expects different JSON
				if ($fcbkVariant) {
					$result[] = array(
						'key' => $value,
						'value' => $label,
					);
				}
				// jQuery UI compliant
				else {
					$result[] = array(
						'label' => $label,
						'value' => $value,
					);
				}
			}
		}

		echo CJSON::encode($result);
	}

	/**
	 * Return list of found groups as a response to "Autocomplete" (search as you type) request
	 * See http://jqueryui.com/autocomplete/ for the response format:
	 * array( array('label' => 'A Label',  'value' => 'A Value'), ... )
	 *
	 */
	public function actionAxGroupsAutocomplete() {

		$maxNumber = Yii::app()->request->getParam('max_number', 10);

		$dataProvider = $this->dataProviderGroups();
		$dataProvider->setPagination(array(
			'pageSize' => $maxNumber,
		));

		$data = $dataProvider->getData();

		$result = array();
		if ($data) {
			foreach ($data as $item) {
				$label = ltrim($item['groupid'], '/');
				$value = $label;
				$result[] = array(
					'label' => $label,
					'value' => $value,
				);
			}
		}

		echo CJSON::encode($result);
	}

	/**
	 * Return list of found courses as a response to "Autocomplete" (search as you type) request
	 * See http://jqueryui.com/autocomplete/ for the response format:
	 * array( array('label' => 'A Label',  'value' => 'A Value'), ... )
	 *
	 */
	public function actionAxCoursesAutocomplete() {

		$maxNumber = Yii::app()->request->getParam('max_number', 10);

		$dataProvider = $this->dataProviderCourses();
		$dataProvider->setPagination(array(
			'pageSize' => $maxNumber,
		));

		$data = $dataProvider->getData();

		$result = array();
		if ($data) {
			foreach ($data as $item) {
				$label = $item['name'];
				$value = $label;
				$result[] = array(
					'label' => $label,
					'value' => $value,
				);
			}
		}

		echo CJSON::encode($result);
	}

	public function actionAxPlansAutocomplete() {

		$maxNumber = Yii::app()->request->getParam('max_number', 10);

		$dataProvider = $this->dataProviderPlans();
		$dataProvider->setPagination(array(
			'pageSize' => $maxNumber,
		));

		$data = $dataProvider->getData();

		$result = array();
		if ($data) {
			foreach ($data as $item) {
				$label = $item['name'];
				$value = $label;
				$result[] = array(
					'label' => $label,
					'value' => $value,
				);
			}
		}

		echo CJSON::encode($result);
	}

	public function actionAxSelectorAutocomplete() {

		$maxNumber = Yii::app()->request->getParam('max_number', 10);
		$selectorTab = Yii::app()->request->getParam('selector_tab', false);

		switch ($selectorTab) {
			case UsersSelector::TAB_PLANS:
				$dataProvider = $this->dataProviderPlans();
				$itemValueKey = "path_name";
				$itemLabelKey = "path_name";
				break;
			case UsersSelector::TAB_PU_PROFILES:
				// str_replace('/framework/adminrules/', '', $item[$itemLabelKey])

				$dataProvider = $this->dataProviderPuProfiles();
				$itemValueKey = "groupid";
				$itemLabelKey = "groupid";
				break;
			case UsersSelector::TAB_ASSETS:
				$dataProvider = $this->dataProviderAssets();
				$itemValueKey = "title";
				$itemLabelKey = "title";
				break;
			case UsersSelector::TAB_CHANNELS:
				$dataProvider = $this->dataProviderChannels();
				$itemValueKey = "channelName";
				$itemLabelKey = "channelName";
				break;
		}

		$dataProvider->setPagination(array(
			'pageSize' => $maxNumber,
		));

		$data = $dataProvider->getData();

		$result = array();
		if ($data) {
			foreach ($data as $item) {
				$result[] = array(
					'label' => $item[$itemLabelKey],
					'value' => $item[$itemValueKey],
				);
			}
		}

		echo CJSON::encode($result);
	}

	/**
	 * Common method to get the SQL Data Provider, either for Autocomplete or for the grid
	 * 
	 * @return CSqlDataProvider
	 */
	protected function dataProviderUsers() {

		//Suspended users too if Report
		$type = Yii::app()->request->getParam('type', false);
		if($type && $type == UsersSelector::TYPE_CUSTOM_REPORTS_USERS)
			$this->showSuspended = true;

		// BASE
		$commandBase = Yii::app()->db->createCommand();

		// Main table
		$commandBase->from('core_user u');

		if ($this->type == UsersSelector::TYPE_APP7020_EXPERTS || $this->type == UsersSelector::TYPE_CUSTOM_REPORT_EXPERT_STATS ) {
			$commandBase->join(App7020Experts::model()->tableName() . " 7020e", "u.idst=7020e.idUser");
		}

		// Exclude Anonymous
		$commandBase->where = "LOWER(u.userid) <> '/anonymous'";

		if (PluginManager::isPluginActive('ElucidatApp')) {
			// Exclude Elucidat User
			$commandBase->andWhere("LOWER(u.userid) <> :elucidatUser", array(':elucidatUser' => ElucidatAppModule::ELUCIDAT_USERID));
		}

		// Power User filtering
		if (Yii::app()->user->getIsPu()) {
			$commandBase->join(CoreUserPU::model()->tableName() . " pu", "(pu.puser_id =:id)  AND (u.idst = pu.user_id)", array(
				':id' => (int) Yii::app()->user->id,
			));
		}

		// Restrict the list to '/framework/level/user's  only, if requested
		if ($this->userLevelOnly) {
			$commandBase->join('core_group_members cgm', 'u.idst=cgm.idstMember');
			$commandBase->join('core_group g', 'cgm.idst=g.idst');
			$commandBase->andWhere('groupid=:groupid', array(':groupid' => '/framework/level/user'));
		}

		if (!$this->showSuspended)
			$commandBase->andWhere('u.valid = :valid', array(':valid' => CoreUser::STATUS_VALID));


		// Exclude users ???
		// If the selector is of type "Enroll users to a single course", use the "LEFT JOIN/NOT NULL" approach
		if ($this->type == UsersSelector::TYPE_COURSE_ENROLLMENT) {
			$commandBase->leftJoin('learning_courseuser lcu', '(u.idst=lcu.idUser) AND (lcu.idCourse=:idCourse)', array(
				':idCourse' => $this->idCourse
			));
			$commandBase->andWhere('lcu.idUser IS NULL');
		}
		// Otherwise, check if we have some IDs in this class attribute and use "NOT IN()"
		else if (count($this->excludeUsers)) {
			$commandBase->andWhere('u.idst NOT IN (' . implode(',', $this->excludeUsers) . ')');
		}

		// SEARCH (split the search string by "spaces" and do "AND" search for all elements
		// This way we can search for AND FIND "lorenzo domingo", while the simple search won't match it
		if ($this->search_term) {
			$terms = explode(' ', trim($this->search_term));
			$i = 0;
			foreach ($terms as $term) {
				$i++;
				$commandBase->andWhere("CONCAT(u.userid, u.firstname,  u.lastname, u.email) LIKE :search_query_" . $i, array(
					':search_query_' . $i => "%" . $term . "%",
				));
			}
		}

		// Narrow the users list to this list of IDs, i.e. filter out all other users ?
		if (is_array($this->filterUsers)) {
			if (empty($this->filterUsers)) {
				$commandBase->andWhere('FALSE'); // Return NOTHING, filter is empty
			} else {
				$commandBase->andWhere('u.idst IN (' . implode(',', $this->filterUsers) . ')');
			}
		}

		if ($this->type == UsersSelector::TYPE_REPORT_VISIBILITY) {

			$pusersIds = Yii::app()->db->createCommand()
							->select('puser_id')
							->from(CoreUserPU::model()->tableName())
							->group('puser_id')->queryColumn();

			// exclude current user id
			$targetIds = array_diff($pusersIds, array(Yii::app()->user->id));
			$commandBase->andWhere(array('IN', 'u.idst', $targetIds));
		}

		// DATA
		$commandData = clone $commandBase;


		// COUNTER
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(u.idst)');
		$numRecords = $commandCounter->queryScalar();


		// ORDERING
		$commandData->order = "";
		$sort = new CSort();
		$sort->attributes = array(
			'userid' => array(
				'asc' => 'u.userid',
				'desc' => 'u.userid DESC',
			),
			'firstname' => array(
				'asc' => 'u.firstname',
				'desc' => 'u.firstname DESC',
			),
			'lastname' => array(
				'asc' => 'u.lastname',
				'desc' => 'u.lastname DESC',
			),
			'email' => array(
				'asc' => 'u.email',
				'desc' => 'u.email DESC',
			),
		);
		$sort->defaultOrder = array(
			'userid' => CSort::SORT_ASC,
		);

		// DATA-II
		$commandData->select("u.idst idUser, TRIM(LEADING '/' FROM u.userid) as userid, u.firstname, u.lastname, u.email");


		// END

		$pageSize = $this->itemsPerPage ? $this->itemsPerPage : Settings::get('elements_per_page', 20);

		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => array('pageSize' => $pageSize),
			'keyField' => 'idUser',
			'sort' => $sort,
		);

		// Pass dbCommand, not SQL text, to apply parameters binding !!!
		$dataProvider = new CSqlDataProvider($commandData, $config);


		return $dataProvider;
	}

	/**
	 * Common method to get the SQL Data Provider for Users Select All functionality
	 *
	 * @return CSqlDataProvider
	 */
	protected function dataProviderUsersSellAll() {

		//Suspended users too if Report
		$type = Yii::app()->request->getParam('type', false);
		if($type && $type == UsersSelector::TYPE_CUSTOM_REPORTS_USERS)
			$this->showSuspended = true;

		// BASE
		$commandBase = Yii::app()->db->createCommand();

		// Main table
		$commandBase->from('core_user u');

		if ($this->type == UsersSelector::TYPE_APP7020_EXPERTS || $this->type == UsersSelector::TYPE_CUSTOM_REPORT_EXPERT_STATS ) {
			$commandBase->join(App7020Experts::model()->tableName() . " 7020e", "u.idst=7020e.idUser");
		}

		// Exclude Anonymous
		$commandBase->where = "LOWER(u.userid) <> '/anonymous'";

		if (PluginManager::isPluginActive('ElucidatApp')) {
			// Exclude Elucidat User
			$commandBase->andWhere("LOWER(u.userid) <> :elucidatUser", array(':elucidatUser' => ElucidatAppModule::ELUCIDAT_USERID));
		}

		// Power User filtering
		if (Yii::app()->user->getIsPu()) {
			$commandBase->join(CoreUserPU::model()->tableName() . " pu", "(pu.puser_id =:id)  AND (u.idst = pu.user_id)", array(
				':id' => (int) Yii::app()->user->id,
			));
		}

		// Restrict the list to '/framework/level/user's  only, if requested
		if ($this->userLevelOnly) {
			$commandBase->join('core_group_members cgm', 'u.idst=cgm.idstMember');
			$commandBase->join('core_group g', 'cgm.idst=g.idst');
			$commandBase->andWhere('groupid=:groupid', array(':groupid' => '/framework/level/user'));
		}

		if (!$this->showSuspended)
			$commandBase->andWhere('u.valid = :valid', array(':valid' => CoreUser::STATUS_VALID));


		// Exclude users ???
		// If the selector is of type "Enroll users to a single course", use the "LEFT JOIN/NOT NULL" approach
		if ($this->type == UsersSelector::TYPE_COURSE_ENROLLMENT) {
			$commandBase->leftJoin('learning_courseuser lcu', '(u.idst=lcu.idUser) AND (lcu.idCourse=:idCourse)', array(
				':idCourse' => $this->idCourse
			));
			$commandBase->andWhere('lcu.idUser IS NULL');
		}
		// Otherwise, check if we have some IDs in this class attribute and use "NOT IN()"
		else if (count($this->excludeUsers)) {
			$commandBase->andWhere('u.idst NOT IN (' . implode(',', $this->excludeUsers) . ')');
		}

		// SEARCH (split the search string by "spaces" and do "AND" search for all elements
		// This way we can search for AND FIND "lorenzo domingo", while the simple search won't match it
		// Modified SEARCH using session variable
		if (!empty(Yii::app()->session['usingSearchForUsers'])) {
			$terms = explode(' ', trim(Yii::app()->session['usingSearchForUsers']));
			$i = 0;
			foreach ($terms as $term) {
				$i++;
				$commandBase->andWhere("CONCAT(u.userid, u.firstname,  u.lastname, u.email) LIKE :search_query_" . $i, array(
					':search_query_' . $i => "%" . $term . "%",
				));
			}
		}

		// Narrow the users list to this list of IDs, i.e. filter out all other users ?
		if (is_array($this->filterUsers)) {
			if (empty($this->filterUsers)) {
				$commandBase->andWhere('FALSE'); // Return NOTHING, filter is empty
			} else {
				$commandBase->andWhere('u.idst IN (' . implode(',', $this->filterUsers) . ')');
			}
		}

		if ($this->type == UsersSelector::TYPE_REPORT_VISIBILITY) {

			$pusersIds = Yii::app()->db->createCommand()
				->select('puser_id')
				->from(CoreUserPU::model()->tableName())
				->group('puser_id')->queryColumn();

			// exclude current user id
			$targetIds = array_diff($pusersIds, array(Yii::app()->user->id));
			$commandBase->andWhere(array('IN', 'u.idst', $targetIds));
		}

		// DATA
		$commandData = clone $commandBase;


		// COUNTER
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(u.idst)');
		$numRecords = $commandCounter->queryScalar();


		// ORDERING
		$commandData->order = "";
		$sort = new CSort();
		$sort->attributes = array(
			'userid' => array(
				'asc' => 'u.userid',
				'desc' => 'u.userid DESC',
			),
			'firstname' => array(
				'asc' => 'u.firstname',
				'desc' => 'u.firstname DESC',
			),
			'lastname' => array(
				'asc' => 'u.lastname',
				'desc' => 'u.lastname DESC',
			),
			'email' => array(
				'asc' => 'u.email',
				'desc' => 'u.email DESC',
			),
		);
		$sort->defaultOrder = array(
			'userid' => CSort::SORT_ASC,
		);

		// DATA-II
		$commandData->select("u.idst idUser, TRIM(LEADING '/' FROM u.userid) as userid, u.firstname, u.lastname, u.email");


		// END

		$pageSize = $this->itemsPerPage ? $this->itemsPerPage : Settings::get('elements_per_page', 20);

		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => array('pageSize' => $pageSize),
			'keyField' => 'idUser',
			'sort' => $sort,
		);

		// Pass dbCommand, not SQL text, to apply parameters binding !!!
		$dataProvider = new CSqlDataProvider($commandData, $config);


		return $dataProvider;
	}

	/**
	 * Common method to get the SQL Data Provider, either for Autocomplete or for the grid
	 *
	 * @return CSqlDataProvider
	 */
	protected function dataProviderGroups() {

		// BASE
		$commandBase = Yii::app()->db->createCommand();

		// Main table
		$commandBase->from('core_group t');

		// Filter out hidden groups
		$commandBase->andWhere("t.hidden != 'true'");


		// Exclude list of users, if set
		if (count($this->excludeGroups)) {
			$commandBase->andWhere('t.idst NOT IN (' . implode(',', $this->excludeGroups) . ')');
		}


		// SEARCH
		if ($this->search_term) {
			$commandBase->andWhere("CONCAT(t.groupid, t.description) LIKE :search_query", array(
				':search_query' => "%" . $this->search_term . "%",
			));
		}

		// Filter out Power Users		
		if (Yii::app()->user->getIsAdmin()) {
			$commandBase->join('core_admin_tree at', 't.idst = at.idst AND at.idstAdmin = :idst_admin', array(
				':idst_admin' => Yii::app()->user->id
			));
		}


		// DATA
		$commandData = clone $commandBase;


		// COUNTER
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(t.idst)');
		$numRecords = $commandCounter->queryScalar();


		// ORDERING
		$commandData->order = "";
		$sort = new CSort();
		$sort->attributes = array(
			'groupid' => array(
				'asc' => 't.groupid ASC',
				'desc' => 't.groupid DESC',
			),
			'description' => array(
				'asc' => 't.description ASC',
				'desc' => 't.description DESC',
			),
		);
		$sort->defaultOrder = 'groupid ASC';


		// DATA-II
		$commandData->select("t.idst idGroup, TRIM(LEADING '/' FROM t.groupid) as groupid, t.description");

		// END
		$pageSize = $this->itemsPerPage ? $this->itemsPerPage : Settings::get('elements_per_page', 20);
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => array('pageSize' => $pageSize),
			'keyField' => 'idGroup',
			'sort' => $sort,
		);

		// Pass dbCommand, not SQL text, to apply parameters binding !!!
		$dataProvider = new CSqlDataProvider($commandData, $config);


		return $dataProvider;
	}

	/**
	 * Common method to get the SQL Data Provider for Groups Select All functionality
	 *
	 * @return CSqlDataProvider
	 */
	protected function dataProviderGroupsSellAll() {

		// BASE
		$commandBase = Yii::app()->db->createCommand();

		// Main table
		$commandBase->from('core_group t');

		// Filter out hidden groups
		$commandBase->andWhere("t.hidden != 'true'");


		// Exclude list of users, if set
		if (count($this->excludeGroups)) {
			$commandBase->andWhere('t.idst NOT IN (' . implode(',', $this->excludeGroups) . ')');
		}


		// Modified SEARCH using session variable
		if (!empty(Yii::app()->session['usingSearchForGroups'])) {
			$commandBase->andWhere("CONCAT(t.groupid, t.description) LIKE :search_query", array(
				':search_query' => "%" . Yii::app()->session['usingSearchForGroups'] . "%",
			));
		}

		// Filter out Power Users
		if (Yii::app()->user->getIsAdmin()) {
			$commandBase->join('core_admin_tree at', 't.idst = at.idst AND at.idstAdmin = :idst_admin', array(
				':idst_admin' => Yii::app()->user->id
			));
		}


		// DATA
		$commandData = clone $commandBase;


		// COUNTER
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(t.idst)');
		$numRecords = $commandCounter->queryScalar();


		// ORDERING
		$commandData->order = "";
		$sort = new CSort();
		$sort->attributes = array(
			'groupid' => array(
				'asc' => 't.groupid ASC',
				'desc' => 't.groupid DESC',
			),
			'description' => array(
				'asc' => 't.description ASC',
				'desc' => 't.description DESC',
			),
		);
		$sort->defaultOrder = 'groupid ASC';


		// DATA-II
		$commandData->select("t.idst idGroup, TRIM(LEADING '/' FROM t.groupid) as groupid, t.description");

		// END
		$pageSize = $this->itemsPerPage ? $this->itemsPerPage : Settings::get('elements_per_page', 20);
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => array('pageSize' => $pageSize),
			'keyField' => 'idGroup',
			'sort' => $sort,
		);

		// Pass dbCommand, not SQL text, to apply parameters binding !!!
		$dataProvider = new CSqlDataProvider($commandData, $config);


		return $dataProvider;
	}

	/**
	 * Common method to get the SQL Data Provider, either for Autocomplete or for the grid
	 *
	 * @return CSqlDataProvider
	 */
	protected function dataProviderCourses() {

		// BASE
		$commandBase = Yii::app()->db->createCommand();

		// Main table
		$commandBase->from('learning_course t');

		// Filter the courses by type to be only from a session type if the request is for Session Reports
		if ($this->idReportType == LearningReportType::USERS_SESSION)
			$commandBase->andWhere('course_type = "' . LearningCourse::CLASSROOM . '"');

		if (in_array($this->notificationType, CoreNotification::$ILT_NOTIFICATIONS))
			$commandBase->andWhere('course_type = "' . LearningCourse::CLASSROOM . '"');
		if (in_array($this->notificationType, CoreNotification::$WEBINAR_NOTIFICATIONS))
			$commandBase->andWhere('course_type = "' . LearningCourse::TYPE_WEBINAR . '"');

		// Power User filtering
		if (Yii::app()->user->getIsPu()) {
			$commandBase->join(CoreUserPuCourse::model()->tableName() . " pu", "(pu.puser_id =:id)  AND (t.idCourse = pu.course_id)", array(
				':id' => (int) Yii::app()->user->id,
			));
		}
		// Exclude items, if set
		if (count($this->excludeCourses)) {
			$commandBase->andWhere('t.idCourse NOT IN (' . implode(',', $this->excludeCourses) . ')');
		}

		// SEARCH (split the search string by "spaces" and do "AND" search for all elements
		// This way we can search for AND FIND "lorenzo domingo", while the simple search won't match it
		if ($this->search_term) {
			if ($this->type == UsersSelector::TYPE_CENTRALREPO_PUSH_TO_COURSE){
				$commandBase->andWhere("CONCAT(t.code, t.name) LIKE :search_query", array(
					':search_query' => "%" . $this->search_term . "%",
				));
			} else {
				$terms = explode(' ', trim($this->search_term));
				$i = 0;
				foreach ($terms as $term) {
					$i++;
					$commandBase->andWhere("CONCAT(t.code, t.name, t.description) LIKE :search_query_" . $i, array(
						':search_query_' . $i => "%" . $term . "%",
					));
				}
			}
		}

		// DATA
		$commandData = clone $commandBase;


		// COUNTER
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(t.idCourse)');
		$numRecords = $commandCounter->queryScalar();


		// ORDERING
		$commandData->order = "";
		$sort = new CSort();
		$sort->attributes = array(
			'code' => array(
				'asc' => 't.code',
				'desc' => 't.code DESC',
			),
			'name' => array(
				'asc' => 't.name',
				'desc' => 't.name DESC',
			),
			'course_type' => array(
				'asc' => 't.course_type',
				'desc' => 't.course_type DESC',
			),
		);
		$sort->defaultOrder = array(
			'name' => CSort::SORT_ASC,
		);



		// DATA-II
		$commandData->select("t.idCourse idCourse, t.code, t.name, t.description, t.course_type");


		// END
		$pageSize = Settings::get('elements_per_page', 20);
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => array('pageSize' => $pageSize),
			'keyField' => 'idCourse',
			'sort' => $sort,
		);

		// Pass dbCommand, not SQL text, to apply parameters binding !!!
		$dataProvider = new CSqlDataProvider($commandData, $config);


		return $dataProvider;
	}

	protected function dataProviderPlans() {

		// BASE
		$commandBase = Yii::app()->db->createCommand();

		// Main table
		$commandBase->from('learning_coursepath t');

		$courseType = false;
		if (in_array($this->notificationType, CoreNotification::$ILT_NOTIFICATIONS))
			$courseType = LearningCourse::TYPE_CLASSROOM;
		if (in_array($this->notificationType, CoreNotification::$WEBINAR_NOTIFICATIONS))
			$courseType = LearningCourse::TYPE_WEBINAR;

		if ($courseType == LearningCourse::TYPE_CLASSROOM || $courseType == LearningCourse::TYPE_WEBINAR) {
			$commandBase->join('(SELECT cpc.id_path FROM learning_coursepath_courses cpc JOIN learning_course c ON c.idCourse = cpc.id_item AND c.course_type = :course_type GROUP BY cpc.id_path) coursepaths', 'coursepaths.id_path = t.id_path', array(':course_type' => $courseType)
			);
		}

		// Power User filtering
		if (Yii::app()->user->getIsPu()) {
			$commandBase->join(CoreUserPuCoursepath::model()->tableName() . " pu", "(pu.puser_id =:id)  AND (t.id_path = pu.path_id)", array(
				':id' => (int) Yii::app()->user->id,
			));
		}


		// Exclude items, if set
		if (count($this->excludePlans)) {
			$commandBase->andWhere('t.id_path NOT IN (' . implode(',', $this->excludePlans) . ')');
		}

		// SEARCH (split the search string by "spaces" and do "AND" search for all elements
		// This way we can search for AND FIND "lorenzo domingo", while the simple search won't match it
		if ($this->search_term) {
			$terms = explode(' ', trim($this->search_term));
			$i = 0;
			foreach ($terms as $term) {
				$i++;
				$commandBase->andWhere("CONCAT(t.path_code, t.path_name, t.path_descr) LIKE :search_query_" . $i, array(
					':search_query_' . $i => "%" . $term . "%",
				));
			}
		}

		// DATA
		$commandData = clone $commandBase;


		// COUNTER
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(t.id_path)');
		$numRecords = $commandCounter->queryScalar();


		// ORDERING
		$commandData->order = "";
		$sort = new CSort();
		$sort->attributes = array(
			'code' => array(
				'asc' => 't.path_code',
				'desc' => 't.path_code DESC',
			),
			'name' => array(
				'asc' => 't.path_name',
				'desc' => 't.path_name DESC',
			),
			'descr' => array(
				'asc' => 't.path_descr',
				'desc' => 't.path_descr DESC',
			),
		);

		$sort->defaultOrder = array(
			'name' => CSort::SORT_ASC,
		);



		// DATA-II
		$commandData->select("t.id_path id_path, t.path_code, t.path_name, t.path_descr");


		// END
		$pageSize = Settings::get('elements_per_page', 20);
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => array('pageSize' => $pageSize),
			'keyField' => 'id_path',
			'sort' => $sort,
		);

		// Pass dbCommand, not SQL text, to apply parameters binding !!!
		$dataProvider = new CSqlDataProvider($commandData, $config);


		return $dataProvider;
	}

	protected function dataProviderPuProfiles() {

		// BASE
		$commandBase = Yii::app()->db->createCommand();

		// Main table
		$commandBase->from('core_group t');

		// Exclude items, if set
		if (count($this->excludePuProfiles)) {
			$commandBase->andWhere('t.idst NOT IN (' . implode(',', $this->excludePuProfiles) . ')');
		}

		$commandBase->andWhere('t.groupid LIKE :groupid', array(
			':groupid' => '%/framework/adminrules%',
		));


		// SEARCH (split the search string by "spaces" and do "AND" search for all elements
		// This way we can search for AND FIND "lorenzo domingo", while the simple search won't match it
		if ($this->search_term) {
			$terms = explode(' ', trim($this->search_term));
			$i = 0;
			foreach ($terms as $term) {
				$i++;
				$commandBase->andWhere("CONCAT(t.groupid) LIKE :search_query_" . $i, array(
					':search_query_' . $i => "%" . $term . "%",
				));
			}
		}

		// DATA
		$commandData = clone $commandBase;


		// COUNTER
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(t.idst)');
		$numRecords = $commandCounter->queryScalar();


		// ORDERING
		$commandData->order = "";
		$sort = new CSort();
		$sort->attributes = array(
			'groupid' => array(
				'asc' => 't.groupid',
				'desc' => 't.groupid DESC',
			),
		);

		$sort->defaultOrder = array(
			'description' => CSort::SORT_ASC,
		);



		// DATA-II
		$commandData->select("t.*, t.idst as keyField");

		// END
		$pageSize = Settings::get('elements_per_page', 20);
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => array('pageSize' => $pageSize),
			'keyField' => 'keyField',
			'sort' => $sort,
		);

		// Pass dbCommand, not SQL text, to apply parameters binding !!!
		$dataProvider = new CSqlDataProvider($commandData, $config);


		return $dataProvider;
	}

	protected function dataProviderContests() {
		/**
		 * @var $commandBase CDbCommand
		 */
		$params = array();
		$defaultTimezone = Settings::get('timezone_default');
		$commandBase = Yii::app()->db->createCommand();

		$commandBase->from(GamificationContest::model()->tableName() . ' t');
		$commandBase->join(GamificationContestTranslation::model()->tableName() . ' gct', 'gct.id_contest = t.id AND gct.lang_code="' . Lang::getCodeByBrowserCode(Yii::app()->getLanguage()) . '"');
		$commandBase->andWhere('CONVERT_TZ(t.to_date, IFNULL(t.timezone, "' . $defaultTimezone . '"), "+00:00") < NOW()');

		if ($this->search_term) {
			$commandBase->andWhere("CONCAT(gct.name) LIKE :search_query OR CONCAT(gct.description) LIKE :search_query");
			$params[':search_query'] = "%" . $this->search_term . "%";
		}

		if (Yii::app()->user->getIsPu()) {
			$puContests = GamificationContest::model()->getContestsOfPu(Yii::app()->user->id);
			if (!empty($puContests)) {
				$commandBase->where('t.id IN (' . implode(',', $puContests) . ')');
			}
		}


		// ORDERING
		$commandBase->order = "";
		$sort = new CSort();
		$sort->attributes = array(
			'gct.name' => array(
				'asc' => 'gct.name',
				'desc' => 'gct.name DESC',
			),
			'gct.description' => array(
				'asc' => 'gct.description',
				'desc' => 'gct.description DESC',
			),
		);

		$sort->defaultOrder = array(
			'gct.name' => CSort::SORT_ASC,
		);
		//Counter
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(t.id)');
		$numRecords = $commandCounter->queryScalar($params);

		$commandBase->select("t.id id_contest, t.goal, t.from_date, t.to_date, name, description");
		$pageSize = Settings::get('elements_per_page', 20);
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => array('pageSize' => $pageSize),
			'keyField' => 'id_contest',
			'sort' => $sort,
			'params' => $params,
		);

		// Pass dbCommand, not SQL text, to apply parameters binding !!!
		$dataProvider = new CSqlDataProvider($commandBase, $config);
		return $dataProvider;
	}

	protected function dataProviderBadges() {
		/**
		 * @var $commandBase CDbCommand
		 */
		$params = array();
		$commandBase = Yii::app()->db->createCommand();

		$commandBase->from(GamificationBadge::model()->tableName() . ' t');
		$commandBase->join(GamificationBadgeTranslation::model()->tableName() . ' gbt', 'gbt.id_badge = t.id_badge AND gbt.lang_code="' . Lang::getCodeByBrowserCode(Yii::app()->getLanguage()) . '"');

		if ($this->search_term) {
			$commandBase->andWhere("CONCAT(name) LIKE :search_query OR CONCAT(description) LIKE :search_query");
			$params[':search_query'] = "%" . $this->search_term . "%";
		}


		// ORDERING
		$commandBase->order = "";
		$sort = new CSort();
		$sort->attributes = array(
			'name' => array(
				'asc' => 'name',
				'desc' => 'name DESC',
			),
			'description' => array(
				'asc' => 'description',
				'desc' => 'description DESC',
			),
		);

		$sort->defaultOrder = array(
			'name' => CSort::SORT_ASC,
		);
		//Counter
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(t.id_badge)');
		$numRecords = $commandCounter->queryScalar($params);

		$commandBase->select("t.id_badge id_badge,t.icon icon,t.score points,name, description");
		$pageSize = Settings::get('elements_per_page', 20);
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => array('pageSize' => $pageSize),
			'keyField' => 'id_badge',
			'sort' => $sort,
			'params' => $params,
		);

		// Pass dbCommand, not SQL text, to apply parameters binding !!!
		$dataProvider = new CSqlDataProvider($commandBase, $config);
		return $dataProvider;
	}

	protected function certificationDataProvider() {
		/**
		 * @var $commandBase CDbCommand
		 */
		$commandBase = Yii::app()->db->createCommand();

		$commandBase->from(Certification::model()->tableName() . ' t');
		$commandBase->andWhere(' deleted = 0 ');
		//Counter
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(t.id_cert)');
		$numRecords = $commandCounter->queryScalar();

		if ($this->search_term) {
			$terms = explode(' ', trim($this->search_term));
			$i = 0;
			foreach ($terms as $term) {
				$i++;
				$commandBase->andWhere("CONCAT(t.title) LIKE :search_query_" . $i, array(
					':search_query_' . $i => "%" . $term . "%",
				));
				$commandBase->orWhere("CONCAT(t.description) LIKE :search_query_" . $i, array(
					':search_query_' . $i => "%" . $term . "%",
				));
			}
		}

		$commandData = clone $commandBase;

		// ORDERING
		$commandData->order = "";
		$sort = new CSort();
		$sort->attributes = array(
			'title' => array(
				'asc' => 't.title',
				'desc' => 't.title DESC',
			),
		);

		$sort->defaultOrder = array(
			'description' => CSort::SORT_ASC,
		);

		$commandBase->select('t.id_cert id_cert, t.title, t.description, t.duration, t.duration_unit');
		$pageSize = Settings::get('elements_per_page', 20);
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => array('pageSize' => $pageSize),
			'keyField' => 'id_cert',
			'sort' => $sort,
		);

		// Pass dbCommand, not SQL text, to apply parameters binding !!!
		$dataProvider = new CSqlDataProvider($commandBase, $config);

		return $dataProvider;
	}

	/**
	 * @TODO FINISH!
	 */
	protected function dataProviderAssets() {
		/**
		 * @var $commandBase CDbCommand
		 */
		$params = array();
		$commandBase = Yii::app()->db->createCommand();

		$commandBase->from(App7020Assets::model()->tableName() . ' t');

		if ($this->search_term) {
			$commandBase->andWhere("t.title LIKE :search_query");
			$params[':search_query'] = "%" . $this->search_term . "%";
		}
		$commandBase->andWhere('t.conversion_status = 20');
		// ORDERING
		$commandBase->order = "";
		$sort = new CSort();
		$sort->attributes = array(
			'title' => array(
				'asc' => 'title',
				'desc' => 'title DESC',
			),
			'description' => array(
				'asc' => 'description',
				'desc' => 'description DESC',
			),
		);

		$sort->defaultOrder = array(
			'title' => CSort::SORT_ASC,
		);
		//Counter
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(t.id)');
		$numRecords = $commandCounter->queryScalar($params);

		$commandBase->select("t.id id, t.id as idAsset, t.title title, t.description description, "
				. "(SELECT id FROM app7020_content_published WHERE idContent = idAsset AND actionType = 1 ORDER BY datePublished DESC LIMIT 1) as publishedId, "
				. "(SELECT datePublished FROM app7020_content_published WHERE id=publishedId) as datePublished, "
				. "(SELECT idUser FROM app7020_content_published WHERE id=publishedId) as idPublisher,"
				. "(SELECT IF(firstname ='' AND lastname = '', TRIM(LEADING '/' FROM userid), CONCAT(firstname, ' ' , lastname)) FROM core_user WHERE idst=idPublisher) as publisher");

		$pageSize = Settings::get('elements_per_page', 20);
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => array('pageSize' => $pageSize),
			'keyField' => 'id',
			'sort' => $sort,
			'params' => $params,
		);


		// Pass dbCommand, not SQL text, to apply parameters binding !!!
		$dataProvider = new CSqlDataProvider($commandBase, $config);

		return $dataProvider;
	}

	/**
	 * @TODO FINISH!
	 */
	protected function dataProviderChannels() {
		/**
		 * @var $commandBase CDbCommand
		 */
		$params = array();
		$commandBase = Yii::app()->db->createCommand();

		$defaultLanguage = Lang::getBrowserCodeByCode(Settings::get('default_language', 'english'));

		// Resolve requested language
		$language = App7020Channels::resolveLanguage();

		// Make SQL (double JOIN to get default language as well)
		$commandBase->andWhere('channel.enabled = 1');
		$commandBase->andWhere('channel.type > 1');
		$commandBase->from(App7020Channels::model()->tableName() . ' channel')
				->leftJoin(App7020ChannelTranslation::model()->tableName() . ' trans', '(channel.id=trans.idChannel) 		AND (trans.lang=:language)')
				->leftJoin(App7020ChannelTranslation::model()->tableName() . ' transDef', '(channel.id=transDef.idChannel) 	AND (transDef.lang=:languageDef)');

			$visibleChannels = App7020Channels::getUsersVisibleChannels(true);
			
			if (!$visibleChannels) {
				$dataProvider = new CArrayDataProvider(array(), array('totalItemCount' => 0, 'pagination' => false));
				return $dataProvider;
			}
			$filteredChannels = array();
			foreach ($visibleChannels as $value) {
				$filteredChannels[] = $value['idChannel'];
			}
			$commandBase->andWhere(array("IN", "channel.id", $filteredChannels));

		$params = array(
			':language' => $language,
			':languageDef' => $defaultLanguage
		);



		// ORDERING
		$commandBase->order = "";
		$sort = new CSort();
		$sort->attributes = array(
			'id' => array(
				'asc' => 'id',
				'desc' => 'id DESC',
			),
				// @TODO more sorting
		);

		$sort->defaultOrder = array(
			'channelName' => CSort::SORT_ASC,
		);
		//Counter
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(channel.id)');
		$numRecords = $commandCounter->queryScalar($params);

		$commandBase->select('channel.id AS id, channel.id AS idChannel, (CASE WHEN trans.name IS NULL THEN transDef.name ELSE trans.name END) AS channelName,'
				. '(CASE WHEN trans.description IS NULL THEN transDef.description ELSE trans.description END) AS channelDescription, '
				. '(SELECT COUNT(id) FROM ' . App7020ChannelAssets::model()->tableName() . ' WHERE idChannel = channel.id) as channelAssets ');

		if ($this->search_term) {
			$commandBase->having("channelName LIKE :search_query");
			$params[':search_query'] = "%" . $this->search_term . "%";
		}

		$pageSize = Settings::get('elements_per_page', 20);
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => array('pageSize' => $pageSize),
			'keyField' => 'id',
			'sort' => $sort,
			'params' => $params,
		);

		// Pass dbCommand, not SQL text, to apply parameters binding !!!
		$dataProvider = new CSqlDataProvider($commandBase, $config);
		return $dataProvider;
	}

	public function actionAxGetUsersList() {
		$dataProvider = $this->dataProviderUsersSellAll(); // Call this method when Select All is pressed
		$dataProvider->setPagination(false);
		$data = $dataProvider->keys;
		$response = new AjaxResult();
		$response->setStatus(true)->setData($data)->toJSON();
		Yii::app()->end();
	}

	public function actionAxGetGroupsList() {
		$dataProvider = $this->dataProviderGroupsSellAll(); // Call this method when Select All is pressed
		$dataProvider->setPagination(false);
		$data = $dataProvider->keys;
		$response = new AjaxResult();
		$response->setStatus(true)->setData($data)->toJSON();
		Yii::app()->end();
	}

	public function actionAxGetCoursesList() {
		$dataProvider = $this->dataProviderCourses();
		$dataProvider->setPagination(false);
		$data = $dataProvider->keys;
		$response = new AjaxResult();
		$response->setStatus(true)->setData($data)->toJSON();
		Yii::app()->end();
	}

	public function actionAxGetPlansList() {
		$dataProvider = $this->dataProviderPlans();
		$dataProvider->setPagination(false);
		$data = $dataProvider->keys;
		$response = new AjaxResult();
		$response->setStatus(true)->setData($data)->toJSON();
		Yii::app()->end();
	}

	public function actionAxGetPuProfilesList() {
		$dataProvider = $this->dataProviderPuProfiles();
		$dataProvider->setPagination(false);
		$data = $dataProvider->keys;
		$response = new AjaxResult();
		$response->setStatus(true)->setData($data)->toJSON();
		Yii::app()->end();
	}

	public function actionAxGetCertificationsList() {
		$dataProvider = $this->certificationDataProvider();
		$dataProvider->setPagination(false);
		$data = $dataProvider->keys;
		$response = new AjaxResult();
		$response->setStatus(true)->setData($data)->toJSON();
		Yii::app()->end();
	}

	public function actionAxGetBadgesList() {
		$dataProvider = $this->dataProviderBadges();
		$dataProvider->setPagination(false);
		$data = $dataProvider->keys;
		$response = new AjaxResult();
		$response->setStatus(true)->setData($data)->toJSON();
		Yii::app()->end();
	}

	public function actionAxGetContestsList() {
		$dataProvider = $this->dataProviderContests();
		$dataProvider->setPagination(false);
		$data = $dataProvider->keys;
		$response = new AjaxResult();
		$response->setStatus(true)->setData($data)->toJSON();
		Yii::app()->end();
	}

	public function actionAxGetAssetsList() {
		$dataProvider = $this->dataProviderAssets();
		$dataProvider->setPagination(false);
		$data = $dataProvider->keys;
		$response = new AjaxResult();
		$response->setStatus(true)->setData($data)->toJSON();
		Yii::app()->end();
	}

	public function actionAxGetChannelsList() {
		$dataProvider = $this->dataProviderChannels();
		$dataProvider->setPagination(false);
		$data = $dataProvider->keys;
		$response = new AjaxResult();
		$response->setStatus(true)->setData($data)->toJSON();
		Yii::app()->end();
	}

	public function actionAxRenderOrgChart() {
		if ($this->useSessionData) {
			$this->preselectedBranches = UsersSelector::getDataFromSession('preselectedBranches', $this->name, !$this->preserveSessionData);
		}

		if ($this->type == UsersSelector::TYPE_REPORT_VISIBILITY) {
			$idReport = intval($_GET['id_report']);
			$visibilityModel = LearningReportAccess::model()->findByAttributes(array('id_report' => $idReport));
			if ($visibilityModel->visibility_type == LearningReportAccess::TYPE_SELECTION) {
				$this->preselectedBranches = LearningReportAccessMember::getMembersByMemberType($idReport, LearningReportAccessMember::TYPE_BRANCH);
			}
		}
		$preselectedBranches = UsersSelector::checkOrgchartSelection($this->preselectedBranches);

		if ($this->type == UsersSelector::TYPE_APP7020_TOPICS) {
			$preselectedBranches = $_SESSION['preselectedBranches'];
		}
		$fancyTreeData = CoreOrgChartTree::buildFancytreeDataArray(false, true, true);
		$this->renderPartial('_orgchart', array(
			'preselectedBranches' => $preselectedBranches,
			'orgchartsFancyTreeId' => $this->orgchartsFancyTreeId,
			'fancyTreeData' => $fancyTreeData,
			'selectMode' => $this->orgChartSelectMode,
				), false, true);
	}

	/**
	 * Build Orgchart Fancytree data array and return it as a JSON. AJAX called.
	 *  
	 */
	public function actionAxGetOrgchartFancytreeJson() {
		/* @var $rq CHttpRequest */
		$rq = Yii::app()->request;

		//check if the tree has changed while the client page was opened
		$treeGenerationTime = $rq->getParam('treeGenerationTime', false);
		if ($treeGenerationTime != false) {
			if (CoreRunningOperation::isBeforeLastOperationTime('org_chart_tree_change', $treeGenerationTime)) {
				echo CJSON::encode(array('update_tree' => true));
				Yii::app()->end();
			}
		}

		//if tree has not been changed then proceed with regular work
		$nodeKey = $rq->getParam('nodeKey', false);
		$lazyMode = $rq->getParam('lazyMode', false);
		$indents = $rq->getParam('indents', false);
		$powerUserFilter = $rq->getParam('powerUserFilter', true);
		$expandRoot = $rq->getParam('expandRoot', false);

		$data = array();
		if ($nodeKey !== false) {
			$data = CoreOrgChartTree::buildFancytreeDataArrayV2($indents, $powerUserFilter, $expandRoot, $lazyMode, $nodeKey, false);
		}
		echo CJSON::encode($data);
		Yii::app()->end();
	}

	/**
	 * Get all possible/expected information that might come from the request.
	 * This selector is universal and we just make a CATCH-ALL collection.
	 * 
	 */
	protected function collectRequestInput() {

		// REQUIRED
		$this->type = Yii::app()->request->getParam('type', false);
		$this->itemsPerPage = Yii::app()->request->getParam('itemsPerPage', false);

		// POSSIBLE for all selector types
		$this->search_term = Yii::app()->request->getParam('term', false);
		$this->tab = Yii::app()->request->getParam('tab', false);
		$this->showNavButtons = Yii::app()->request->getParam('show_buttons', true);

		// POSSIBLE, Specific for different selectors; listed in calss static array; try to get them ALL!
		foreach (self::$SELECTOR_PARAMS_NAMES as $name) {
			$this->$name = Yii::app()->request->getParam($name, null);
		}

		// OPTIONAL
		$this->name = Yii::app()->request->getParam('name', $this->name);
		$this->title = Yii::app()->request->getParam('title', false);
		$this->useSessionData = Yii::app()->request->getParam('useSessionData', false);
		$this->preserveSessionData = Yii::app()->request->getParam('preserveSessionData', false);
		$this->wizardParams = Yii::app()->request->getParam('wizardParams', false);
		$this->collector_url = Yii::app()->request->getParam('collectorUrl', false);
		$this->successCallback = Yii::app()->request->getParam('successCallback', false);

		//Hide or Show the Menu on the left
		$this->showLeftBar = Yii::app()->request->getParam('showLeftBar', true);


		// If we are in "grid update call", set flags to avoid loading some data later; greatly improves overall performance!!
		$this->inUsersGridUpdate = isset($_REQUEST[self::$AJAX_VAR_USERS_GRID]);
		$this->inGroupsGridUpdate = isset($_REQUEST[self::$AJAX_VAR_GROUPS_GRID]);
		$this->inCoursesGridUpdate = isset($_REQUEST[self::$AJAX_VAR_COURSES_GRID]);
		$this->inPlansGridUpdate = isset($_REQUEST[self::$AJAX_VAR_PLANS_GRID]);
		$this->inPuProfilesGridUpdate = isset($_REQUEST[self::$AJAX_VAR_PUPROFULES_GRID]);
		$this->inCertificationGridUpdate = isset($_REQUEST[self::$AJAX_VAR_CERTIFICATON_GRID]);
		$this->inBadgesGridUpdate = isset($_REQUEST[self::$AJAX_VAR_BADGES_GRID]);
		$this->inContestsGridUpdate = isset($_REQUEST[self::$AJAX_VAR_CONTESTS_GRID]);
		$this->inAssetsGridUpdate = isset($_REQUEST[self::$AJAX_VAR_ASSETS_GRID]);
		$this->inChannelsGridUpdate = isset($_REQUEST[self::$AJAX_VAR_CHANNELS_GRID]);


		$this->inGridUpdate = $this->inUsersGridUpdate ||
				$this->inGroupsGridUpdate ||
				$this->inCoursesGridUpdate ||
				$this->inPlansGridUpdate ||
				$this->inPuProfilesGridUpdate ||
				$this->inCertificationGridUpdate ||
				$this->inBadgesGridUpdate ||
				$this->inContestsGridUpdate ||
				$this->inAssetsGridUpdate ||
				$this->inChannelsGridUpdate
		;

		if ($this->type == UsersSelector::TYPE_NOTIFICATIONS_COURSES_ASSOC) {
			$notification = CoreNotification::model()->findByPk($this->idGeneral);
			if ($notification) {
				$this->notificationType = $notification->type;
			} elseif (isset(Yii::app()->session['notifType'])) {
				$this->notificationType = Yii::app()->session['notifType'];
			}
		}
	}

	/**
	 * Validate incoming data, like checking if some model exists, additional complex permissions check, etc.
	 * Call by beforeAction(), using alredy set class attributes
	 * 
	 */
	protected function validateInput() {

		// Check Group ID
		if ((int) $this->idGroup > 0) {
			$model = CoreGroup::model()->findByPk($this->idGroup);
			if (!$model) {
				$this->error(Yii::t('standard', _OPERATION_FAILURE) . " (invalid group)", 'error');
			}
		}

		// Check COURSE
		if ((int) $this->idCourse > 0) {
			$model = LearningCourse::model()->findByPk($this->idCourse);
			if (!$model) {
				$this->error(Yii::t('standard', _OPERATION_FAILURE) . " (invalid course)", 'error');
			}
		}
	}

	/**
	 * Based on the type of the requested selector, load some data used by the rest of the controller,
	 * like data providers.
	 */
	protected function prepareData() {


		switch ($this->type) {

			case UsersSelector::TYPE_GROUPS_MANAGEMENT:
				if (!$this->inGridUpdate) {
					$this->preselectedUsers = CoreGroup::model()->findByPk($this->idGroup)->getUsers();
				}
				$this->excludeGroups = array($this->idGroup);
				break;


			case UsersSelector::TYPE_AUTOMATION_RULE:
				// Resolve preselected groups/charts
				if (!$this->inGridUpdate) {
					$selectedGroups = Yii::app()->request->getParam('rule_groups', '');
					$selectedBranches = Yii::app()->request->getParam('rule_branches', '{}');
					$selectedBranches = CJSON::decode($selectedBranches);

					// Resolve already assigned groups
					$selectedGroups = explode(',', $selectedGroups);
					foreach ($selectedGroups as $groupInfo) {
						if (!empty($groupInfo)) {
							$this->preselectedGroups[] = (int) $groupInfo; //$groupInfo['idst'];
						}
					}

					// Resolve Preselected ORG CHARTS
					$preSelected = array();
					if (is_array($selectedBranches)) {
						foreach ($selectedBranches as $key => $selectState) {
							$preSelected[] = array("key" => $key, "selectState" => $selectState);
						}
					}

					$this->preselectedBranches = $preSelected;
				}
				break;

			// Assign users/groups/org charts to a Power user (idUser)
			case UsersSelector::TYPE_POWERUSER_MANAGEMENT:

				// Resolve preselectied users/groups/charts
				if (!$this->inGridUpdate) {

					// Directly assigned users
					$assignedUsers = array_keys(CoreAdminTree::getPowerUserUsers($this->idUser));
					foreach ($assignedUsers as $idst) {
						$this->preselectedUsers[] = (int) $idst;
					}

					// Resolve already assigned groups
					$powerUserOrgChartsMembers = CoreAdminTree::getPowerUserMembers($this->idUser, array(CoreAdminTree::TYPE_GROUP));
					foreach ($powerUserOrgChartsMembers as $groupInfo) {
						$this->preselectedGroups[] = (int) $groupInfo['idst'];
					}

					// Resolve Preselected ORG CHARTS
					$powerUserOrgChartsMembers = CoreAdminTree::getPowerUserMembers($this->idUser, array(CoreAdminTree::TYPE_ORGCHART, CoreAdminTree::TYPE_ORGCHART_DESC));
					$selectedOrgCharts = array();
					foreach ($powerUserOrgChartsMembers as $info) {
						$selectedOrgCharts[$info['id']] = $info['type'] == CoreAdminTree::TYPE_ORGCHART ? 1 : 2;
					}
					$preSelected = array();
					foreach ($selectedOrgCharts as $key => $selectState) {
						$preSelected[] = array("key" => $key, "selectState" => $selectState);
					}
					$this->preselectedBranches = $preSelected;
				}

				break;


			// Enroll MANY users to SINGLE course (idCourse)	
			case UsersSelector::TYPE_COURSE_ENROLLMENT:
				break;

			case UsersSelector::TYPE_CATALOG_MANAGEMENT:

				// Preselected Users
				if (!$this->inGridUpdate) {
					$rawSelectedItems = LearningCatalogueMember::model()->getCatalogMembers($this->idCatalog, array('user'));
					foreach ($rawSelectedItems as $item) {
						$this->preselectedUsers[] = (int) $item['idst'];
					}

					$rawSelectedItems = LearningCatalogueMember::model()->getCatalogMembers($this->idCatalog, array('group'));
					foreach ($rawSelectedItems as $item) {
						$this->preselectedGroups[] = (int) $item['idst'];
					}

					// Preselected Orgcharts
					$rawSelectedItems = LearningCatalogueMember::model()->getCatalogMembers($this->idCatalog, array('orgchart', 'orgchart_desc'));
					foreach ($rawSelectedItems as $item) {
						$this->preselectedBranches[] = array(
							'key' => (int) $item['id'],
							'selectState' => $item['type'] == 'orgchart' ? 1 : 2,
						);
					}
				}

				break;


			case UsersSelector::TYPE_CATALOG_MANAGEMENT_COURSES:
				// Exclude Already assigned Courses
				$this->excludeCourses = LearningCatalogue::getCatalogCoursesListV2($this->idCatalog);
				// Exclude Already assigned Plans
				$this->excludePlans = LearningCatalogue::getCatalogPlansList($this->idCatalog);
				break;


			case UsersSelector::TYPE_CURRICULA_MANAGEMENT:
				if (!$this->inGridUpdate) {

					$assignedUsers = Yii::app()->db->createCommand()
									->select('lcu.idUser')
									->from(LearningCoursepathUser::model()->tableName() . " lcu")
									->where('lcu.id_path = :id_path', array(':id_path' => $this->idPlan))->queryAll();

					foreach ($assignedUsers as $user) {
						if (isset($user['idUser'])) {
							$this->preselectedUsers[] = (int) $user['idUser'];
						}
					}
				}
				break;


			case UsersSelector::TYPE_CLASSROOM_ENROLLMENT:
				if (!$this->inGridUpdate) {
					$sessionModel = LtCourseSession::model()->findByPk($this->idSession);
					$tmp = $sessionModel->getEnrolledUsers();
					foreach ($tmp as $item) {
						$this->preselectedUsers[] = (int) $item['id_user'];
					}
				}
				break;

			case UsersSelector::TYPE_WEBINAR_ENROLLMENT:
				if (!$this->inGridUpdate) {
					$sessionModel = WebinarSession::model()->findByPk($this->idSession);
					$tmp = $sessionModel->getEnrolledUsers();
					foreach ($tmp as $item) {
						$this->preselectedUsers[] = (int) $item['id_user'];
					}
				}
				break;

			case UsersSelector::TYPE_MODULE_MANAGEMENT:
				if (!$this->inGridUpdate) {
					$module = 'mo_' . $this->idModule;
					$criteria = new CDbCriteria();
					$criteria->condition = 't.obj_index = :obj_index';
					$criteria->params[':obj_index'] = $module;
					$model = LearningMiddlearea::model()->find($criteria);
					if ($model) {
						$idstList = unserialize($model->idst_list);
						if (is_array($idstList)) {
							//$this->preselectedUsers = $this->retrieveUsersFromSelection($idstList); //modules do not deal with users
							$this->preselectedGroups = $this->retrieveGroupsFromSelection($idstList);
							$this->preselectedBranches = $this->retrieveBranchesFromSelection($idstList);
						}
					}
				}
				break;

			case UsersSelector::TYPE_ENROLLRULES_BRANCHES:
				$ruleModel = CoreEnrollRule::model()->findByPk($this->idEnrollRule);
				if ($ruleModel) {
					foreach ($ruleModel->branchItems as $item) {
						$this->preselectedBranches[] = array(
							'key' => $item->item_id,
							'selectState' => ($item->selection_state) ? $item->selection_state : CoreEnrollRuleItem::SELECTION_STATE_BRANCH_ONLY);
					}
				}
				break;


			case UsersSelector::TYPE_ENROLLRULES_GROUPS:
				if (!$this->inGridUpdate) {
					$ruleModel = CoreEnrollRule::model()->findByPk($this->idEnrollRule);
					if ($ruleModel) {
						foreach ($ruleModel->groupItems as $item) {
							$this->preselectedGroups[] = $item->item_id;
						}
					}
				}
				break;


			case UsersSelector::TYPE_ENROLLRULES_COURSES:
				if (!$this->inGridUpdate) {
					$ruleModel = CoreEnrollRule::model()->findByPk($this->idEnrollRule);
					if ($ruleModel) {
						foreach ($ruleModel->courseItems as $item) {
							$this->preselectedCourses[] = $item->item_id;
						}
					}
				}
				break;


			case UsersSelector::TYPE_CUSTOM_REPORTS_COURSES:
				if (!$this->inGridUpdate) {
					$model = LearningReportFilter::model()->findByPk((int) $this->idReport);
					if ($model) {
						$this->preselectedCourses = $model->getCoursesFromFilter();
						$this->preselectedPlans = $model->getPlansFromFilter();
					}
				}
				break;

			case UsersSelector::TYPE_CUSTOM_REPORTS_USERS:
				if (!$this->inGridUpdate) {
					$model = LearningReportFilter::model()->findByPk((int) $this->idReport);
					if ($model) {
						$this->preselectedUsers = $model->getUsersFromFilter();
						$this->preselectedGroups = $model->getGroupsOnlyFromFilter();
						$this->preselectedBranches = $model->getOrgchartsOnlyFromFilter();
					}
				}
				break;

			case UsersSelector::TYPE_CUSTOM_REPORT_ASSETS_STATS:
				if (!$this->inGridUpdate) {
					$model = LearningReportFilter::model()->findByPk((int) $this->idReport);
					if ($model) {
						$this->preselectedAssets = $model->getAssetsFromFilter();
						$this->preselectedChannels = $model->getChannelsFromFilter();
					}
				}
				break;


			case UsersSelector::TYPE_CUSTOM_REPORT_CHANNELS_STATS:
				if (!$this->inGridUpdate) {
					$model = LearningReportFilter::model()->findByPk((int) $this->idReport);
					if ($model) {
						$this->preselectedChannels = $model->getChannelsFromFilter();
					}
				}
				break;

			case UsersSelector::TYPE_CUSTOM_REPORT_EXPERT_STATS:
				if (!$this->inGridUpdate) {
					$model = LearningReportFilter::model()->findByPk((int) $this->idReport);
					if ($model) {
						$this->preselectedUsers = $model->getExpertsFromFilter();
					}
				}
				break;


			case UsersSelector::TYPE_CUSTOM_REPORT_CERTIFICATION:
				if (!$this->inGridUpdate) {
					$model = LearningReportFilter::model()->findByPk((int) $this->idReport);
					if ($model) {
						$this->preselectedCertification = $model->getCertificationFromFilter();
					}
				}
				break;

			case UsersSelector::TYPE_CUSTOM_REPORT_BADGES:
				if (!$this->inGridUpdate) {
					$model = LearningReportFilter::model()->findByPk((int) $this->idReport);
					if ($model) {
						$this->preselectedBadges = $model->getBadgesFromFilter();
					}
				}
				break;

			case UsersSelector::TYPE_CUSTOM_REPORT_CONTESTS:
				if (!$this->inGridUpdate) {
					$model = LearningReportFilter::model()->findByPk((int) $this->idReport);

					if ($model) {
						/**
						 * @var $model LearningReportFilter
						 */
						$this->preselectedContests = $model->getContestsFromFilter();
					}
				}
				break;

			case UsersSelector::TYPE_GAMIFICATION_ASSIGN_BADGES:
				break;


			case UsersSelector::TYPE_ORGCHART_ASSIGN_USERS:

				// Do NOT load pre-selected users if this is YiiGrid update (detected by AJAX var)
				if (!$this->inGridUpdate) {
					$criteria = new CDbCriteria;
					$criteria->select = array('t.*');
					$chart = CoreOrgChartTree::model()->findByPk($this->idOrg);
					if ($chart) {
						$this->preselectedUsers = $chart->getNodeUsers();
					}
				}
				break;


			case UsersSelector::TYPE_SUBSCRIPTIONCODES_COURSES:
				if (!$this->inGridUpdate) {
					$model = SubscriptioncodesSet::model()->findByPk($this->idGeneral);
					// Exclude already assigned courses
					if ($model) {
						$this->excludeCourses = $model->assignedCoursesList();
					}
				}
				break;


			case UsersSelector::TYPE_NOTIFICATIONS_USER_FILTER:
				if (!$this->inGridUpdate) {
					$model = CoreNotification::model()->findByPk($this->idGeneral);
					if ($model) {
						$info = $model->getUserFilterInfo();
						$this->preselectedGroups = $info['groups'];
						$this->preselectedBranches = $info['branches'];
						$this->preselectedUsers = $info['users'];
					}
				}
				break;


			case UsersSelector::TYPE_NOTIFICATIONS_COURSES_ASSOC:
				if (!$this->inGridUpdate) {
					$model = CoreNotification::model()->findByPk($this->idGeneral);
					$type = $this->notificationType ? $this->notificationType : $model->type;
					if ($model) {
						$info = $model->getAssocInfo(false, true, $type);
						$this->preselectedCourses = $info['courses'];
						$this->preselectedPlans = $info['plans'];
					}
				}
				break;


			case UsersSelector::TYPE_CERTIFICATIONS_ASSIGN_ITEMS:
				// Removed because courses & LP already assigned to some certification should not be listed in the grid!
				//if ( ! $this->inGridUpdate ) {
				$this->excludeCourses = CertificationItem::globalAssignedItems(CertificationItem::TYPE_COURSE);
				$this->excludePlans = CertificationItem::globalAssignedItems(CertificationItem::TYPE_LEARNING_PLAN);
				//}
				break;

			case UsersSelector::TYPE_DASHBOARD_USER_FILTER:
				if (!$this->inGridUpdate) {
					$info = DashboardLayout::model()->findByPk($this->idGeneral)->getUserFilterInfo();
					$this->preselectedGroups = $info['groups'];
					$this->preselectedBranches = $info['branches'];
					$this->preselectedUsers = $info['users'];
				}
				break;

			case UsersSelector::TYPE_DASHBOARD_PU_PROFULES:
				if (!$this->inGridUpdate) {
					$this->preselectedPuProfiles = DashboardLayout::model()->findByPk($this->idGeneral)->getAssignedPuProfiles();
				}
				break;

			case UsersSelector::TYPE_MASS_ENROLL_LP_USERS:
				if (!$this->inGridUpdate) {
					if (empty($this->idUser)) {
						$curriculaModel = LearningCoursepath::model()->findByPk($this->idPlan);
						$assignedUsers = CHtml::listData($curriculaModel->coreUsers, 'idst', 'idst');
						foreach ($assignedUsers as $idUser) {
							//$this->preselectedUsers[] = (int) $idUser; // this doesn't allow correct multiple users/courses edit !!!
							$this->excludeUsers[] = (int) $idUser;
						}
					} else {
						// there is just one user selected(to edit)
						$this->preselectedUsers[] = (int) $this->idUser;
					}
				}
				break;

			case UsersSelector::TYPE_MASS_ENROLL_LP_COURSES:
				if (!$this->inGridUpdate) {
					if (empty($this->idUser)) { //as part of the wizard mass assign users to LP courses
						// get current Learning plan and assigned users to it
						$curriculaModel = LearningCoursepath::model()->findByPk($this->idPlan);
						$assignedUsers = CHtml::listData($curriculaModel->coreUsers, 'idst', 'idst');
						foreach ($assignedUsers as $idUser) {
							$this->preselectedUsers[] = (int) $idUser;
						}

						$assignedCourses = CHtml::listData($curriculaModel->learningCourse, 'idCourse', 'idCourse');
						foreach ($assignedCourses as $idCourse) {
							$this->preselectedCourses[] = (int) $idCourse;
						}

						// get courses outside of the LP and exclude them !!!
						$this->excludeCourses = LearningCoursepath::getCoursesOutsideLP($this->idPlan);

						// check all courses, that are selling and has seats for PU only
						if (Yii::app()->user->getIsPu() && CoreUserPU::isPUAndSeatManager()) {
							//get all courses, that belongs to this LP
							$lpCourses = LearningCoursepath::getCoursesOfLP($this->idPlan);

							foreach ($lpCourses as $lpCourse) {
								$learningCourse = LearningCourse::model()->findByPk($lpCourse);

								if ($learningCourse->selling && CoreUserPU::isPUAndSeatManager()) {
									$seats = CoreUserPuCourse::getTotalAndAvailableSeats($lpCourse);
									// fixme - exclude courses with no seats available
									// NOTE: those courses won't be excluded in future, but they won't be selectable
									// in courses selector
									if ($seats['available'] == 0) {
										$this->excludeCourses[] = (int) $lpCourse;
										if (($deleteKey = array_search($lpCourse, $this->preselectedCourses)) !== false) {
											unset($this->preselectedCourses[$deleteKey]); //fix for preselected users !!!
										}
									}
								}
							}
						}
					} else {
						// in this case the user id is set and it is just one!!!
						// in other words we are editing an user, that belongs to the current Learning Plan
						$this->preselectedUsers[] = (int) $this->idUser;

						//get the courses, that user is assigned to and belongs to the current Learning Plan
						$selectedCourses = LearningCoursepath::getUserCoursesInLP($this->idUser, $this->idPlan); //
						//prepare (pre-)selected courses array
						$this->preselectedCourses = array_merge($this->preselectedCourses, $selectedCourses);

						// get courses outside of the LP and exclude them !!!
						$this->excludeCourses = LearningCoursepath::getCoursesOutsideLP($this->idPlan);

						// check all courses, that are selling and has seats for PU only
						if (Yii::app()->user->getIsPu() && CoreUserPU::isPUAndSeatManager()) {
							//get all courses, that belongs to this LP
							$lpCourses = LearningCoursepath::getCoursesOfLP($this->idPlan);

							foreach ($lpCourses as $lpCourse) {
								$learningCourse = LearningCourse::model()->findByPk($lpCourse);

								if ($learningCourse->selling && CoreUserPU::isPUAndSeatManager()) {
									$seats = CoreUserPuCourse::getTotalAndAvailableSeats($lpCourse);
									// fixme - exclude courses with no seats available
									// NOTE: those courses won't be excluded in future, but they won't be selectable
									// in courses selector
									if ($seats['available'] == 0) {
										//NOTE: show this course if user is already assigned to it !!!
										if (!in_array($lpCourse, $selectedCourses))
											$this->excludeCourses[] = (int) $lpCourse;
									}
								}
							}
						}
					}
				}
				break;

			case UsersSelector::TYPE_APP7020_ASSIGN_EXPERTS:
				$channelModel = App7020Channels::model()->findByPk($this->idGeneral);
				$this->excludeUsers = $channelModel->getAssignedExperts();
				break;

			case UsersSelector::TYPE_APP7020_ADD_EXPERTS:
				$this->orgChartSelectMode = FancyTree::SELECT_MODE_DOCEBO2;
				$excludeUsers = array();
				$experts = App7020Experts::model()->findAll();
				foreach ($experts as $value) {
					$excludeUsers[] = $value->idUser;
				}
				$this->excludeUsers = $excludeUsers;
				break;

			/**
			 * Admin -> Courses -> Course -> Enrollment -> Coaching management -> Assign users to coaching session
			 */
			case UsersSelector::TYPE_ASSIGN_USERS_COACHING:
				$coachingSessionModel = LearningCourseCoachingSession::model()->findByPk($this->idGeneral);
				$courseModel = $coachingSessionModel->course;
				$idCourse = $courseModel->idCourse;
				$filterObject = new stdClass();
				$filterObject->level = LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT;

				$enrollments = LearningCourseuser::getEnrollments($idCourse, false, $filterObject);

				// Make an ultimate filter for users: all other users will be ignored; we show only users enrolled to the course of this session
				// Make the default list EMPTY, NO ONE
				$this->filterUsers = array();
				foreach ($enrollments as $enrollment) {
					$this->filterUsers[] = $enrollment['idUser'];
				}

				// Get all users assigned to ANY session of the course...            	
				$usersAssignedToAnySessionInTheCourse = $courseModel->getUsersAssignedToCourseCoachingSession(false, true);
				// then get all users assigned to THIS session
				$usersAssignedToThisSession = $courseModel->getUsersAssignedToCourseCoachingSession($coachingSessionModel->idSession, true);

				// Users NOT assigned to this session but assigned to ANY other, must be excluded
				$usersToExclude = array();
				$usersToExclude = array_diff($usersAssignedToAnySessionInTheCourse, $usersAssignedToThisSession);
				foreach ($usersToExclude as $idUser) {
					$this->excludeUsers[] = $idUser;
				}

				// Users assigned to THIS session are pre-selected
				foreach ($usersAssignedToThisSession as $idUser) {
					$this->preselectedUsers[] = $idUser;
				}

				break;

			// Filter Menu Items for certain users
			case UsersSelector::TYPE_FILTER_MENU_ITEMS_FOR_USERS:
				if (!$this->inGridUpdate) {
					$info = CoreMenuItem::model()->findByPk($this->idMenu)->getMenuFilterInfo();
					$this->preselectedGroups = $info['groups'];
					$this->preselectedBranches = $info['branches'];
				}
				break;

			case UsersSelector::TYPE_GAMIFICATION_LEADERBOARD:
			case UsersSelector::TYPE_GAMIFICATION_LEADERBOARD_NO_WIZARD:
				$leaderboardModel = GamificationLeaderboard::model()->findByPk($this->idGeneral);
				if ($leaderboardModel) {
					$selection = $leaderboardModel->getSelectionFromFilter();
					$this->preselectedGroups = (isset($selection['groups'])) ? $selection['groups'] : array();
					$this->preselectedBranches = (isset($selection['branches'])) ? $selection['branches'] : array();
				}
				break;

			case UsersSelector::TYPE_REWARDS_SET_FILTER:
				$rewardsSetModel = GamificationRewardsSet::model()->findByPk($this->idGeneral);
				if ($rewardsSetModel) {
					$selection = $rewardsSetModel->getSelectionFromFilter();
					$this->preselectedGroups = (isset($selection['groups'])) ? $selection['groups'] : array();
					$this->preselectedBranches = (isset($selection['branches'])) ? $selection['branches'] : array();
				}
				break;
			case UsersSelector::TYPE_GAMIFICATION_CONTEST_NO_WIZARD:
				$contestModel = GamificationContest::model()->findByPk($this->idGeneral);
				if ($contestModel) {
					$selection = $contestModel->getSelectionFromFilter();
					$this->preselectedGroups = (isset($selection['groups'])) ? $selection['groups'] : array();
					$this->preselectedBranches = (isset($selection['branches'])) ? $selection['branches'] : array();
				}
				break;
			case UsersSelector::TYPE_REPORT_VISIBILITY:
				$reportId = intval($_GET['id']);
				$visibilityModel = LearningReportAccess::model()->findByAttributes(array('id_report' => $reportId));
				if ($visibilityModel->visibility_type == LearningReportAccess::TYPE_SELECTION) {
					$this->preselectedUsers = LearningReportAccessMember::getMembersByMemberType($reportId, LearningReportAccessMember::TYPE_USER);
					$this->preselectedGroups = LearningReportAccessMember::getMembersByMemberType($reportId, LearningReportAccessMember::TYPE_GROUP);
					$this->preselectedBranches = LearningReportAccessMember::getMembersByMemberType($reportId, LearningReportAccessMember::TYPE_BRANCH);
				}
				break;

			case UsersSelector::TYPE_CENTRALREPO_PUSH_TO_COURSE:
				$this->wizardParams = array(
						'wizard_mode' => true,
						'btnNextTxt'  => Yii::app()->request->getParam("btnNextTxt", false),
				);

				$filterCoursesByType = Yii::app()->request->getParam('filterCoursesByType', Yii::app()->request->getParam('mode', false));
				if($filterCoursesByType !== false) {
					$excludeCourses = Yii::app()->db->createCommand()
						->select('idCourse')
						->from(LearningCourse::model()->tableName())
						->where(array("or", "course_type = :web", "course_type = :class"))
						->queryColumn(array(":web" => LearningCourse::TYPE_WEBINAR, ":class" => LearningCourse::TYPE_CLASSROOM));

					if($excludeCourses !== array())
						$this->excludeCourses = array_merge($this->excludeCourses, $excludeCourses);

					// Set the ID for further needs
					$this->mode = $filterCoursesByType;
				}

				$singleObjectId = Yii::app()->request->getParam('singleObjectId', Yii::app()->request->getParam("idGeneral", false));

				if($singleObjectId) {
					$canOverride = LearningRepositoryObject::canObjectInCourseBeOverridden($singleObjectId);
					// If the Selected Single object support versioning, and HAVE more then one version
					// THEN let's NOT filter the Courses by courses that the object is already part of!
					if ( !$canOverride ) {
						$this->excludeCourses = array_merge($this->excludeCourses, LearningRepositoryObject::getCourseAssignmentsIds($singleObjectId));
					}


					$this->idGeneral = $singleObjectId;
					$this->wizardParams['first'] = true;
				}

				break;

			default:
				break;
		}


		// SESSION DATA: session[<selector-session-key-and-subkey>] => data
		// Check session for incoming data, but use ONLY if instructed to
		// NOTE: we do NOT extract BRANCHES information here, because it is taken ON AJAX rendering call later
		// This will just override all calculations above; sort of double work, but lets keep it this way for now
		if (!$this->inGridUpdate && $this->useSessionData) {
			$this->preselectedUsers = UsersSelector::getDataFromSession('preselectedUsers', $this->name, !$this->preserveSessionData);
			$this->preselectedGroups = UsersSelector::getDataFromSession('preselectedGroups', $this->name, !$this->preserveSessionData);
			$this->preselectedCourses = UsersSelector::getDataFromSession('preselectedCourses', $this->name, !$this->preserveSessionData);
			$this->preselectedPlans = UsersSelector::getDataFromSession('preselectedPlans', $this->name, !$this->preserveSessionData);
			$this->preselectedPuProfiles = UsersSelector::getDataFromSession('preselectedPuProfiles', $this->name, !$this->preserveSessionData);
			$this->preselectedAssets = UsersSelector::getDataFromSession('preselectedAssets', $this->name, !$this->preserveSessionData);
			$this->preselectedChannels = UsersSelector::getDataFromSession('preselectedChannels', $this->name, !$this->preserveSessionData);
		}
	}

	/**
	 * Return universal, all-for-one ERROR view inside the opened dialog
	 * @param string $message
	 * @param string $class CSS class for the message element
	 */
	public function error($message, $class = 'error') {

		$this->renderPartial('admin.protected.views.common/_dialog2_error', array(
			'message' => $message,
			'type' => $class,
		));

		Yii::app()->end();
	}

	/**
	 * Check if given tab (users, groups, courses, ...) is enabled either by a passed Selector parameter ($this->tabs)
	 * or by pre-sets, hard coded in UsersSelector component (tabs per selector type)
	 * 
	 * By using $this->tabs (passed to usersSelector/axOpen) we can choose which TABS gonna be shown 
	 *   
	 * @param string $tab Name of the tab (see UsersSelector::TAB_????)
	 * @return boolean
	 */
	public function tabIsEnabled($tab) {
		if (!$this->tabs) {
			return UsersSelector::tabIsEnabled($this->type, $tab);
		} else {
			$tabs = $this->tabs;

			if (is_string($this->tabs) && stripos($this->tabs, ',') !== false) {
				$tabs = explode(',', $this->tabs);
				foreach ($tabs as $index => $cleanup) {
					$tabs[$index] = trim($cleanup);
				}
			}

			if (is_array($tabs))
				return in_array($tab, $tabs);
			else
				return ($tab == $tabs);
		}
	}

	/**
	 * Given a list of generic idsts, only the ones which are users will be filtered and returned to caller
	 * @param $list array list of idsts
	 * @return array filtered users idsts
	 */
	public function retrieveUsersFromSelection($list) {
		$output = array();

		//validate input
		if (!is_array($list) || empty($list)) {
			return $output;
		}

		//extract all users idsts (this will be faster than doing a big IN () in the query)
		$reader = Yii::app()->db->createCommand("SELECT idst FROM " . CoreUser::model()->tableName() . " WHERE userid <> '/Anonymous'")->query();
		while ($record = $reader->read()) {
			if (in_array($record['idst'], $list)) {
				$output[] = (int) $record['idst'];
			}
		}

		return $output;
	}

	/**
	 * Given a list of generic idsts, only the ones which are groups will be filtered and returned to caller
	 * @param $list array list of idsts
	 * @return array filtered groups idsts
	 */
	public function retrieveGroupsFromSelection($list) {
		$output = array();

		//validate input
		if (!is_array($list) || empty($list)) {
			return $output;
		}

		//extract groups idsts
		$reader = Yii::app()->db->createCommand("SELECT idst FROM " . CoreGroup::model()->tableName() . " WHERE hidden = 'false'")->query();
		while ($record = $reader->read()) {
			if (in_array($record['idst'], $list)) {
				$output[] = (int) $record['idst'];
			}
		}

		return $output;
	}

	/**
	 * Given a list of generic idsts, only the ones which are branches will be filtered and returned to caller (either single branch or branch + descendants)
	 * @param $list array list of idsts
	 * @return array filtered list of branches, each branch in the format of array('key', 'selectState'), to be used in trees input/output)
	 */
	public function retrieveBranchesFromSelection($list) {
		$output = array();

		//validate input
		if (!is_array($list) || empty($list)) {
			return $output;
		}

		//extract orgchart groups idsts
		$ocList = array();
		$ocdList = array();
		$reader = Yii::app()->db->createCommand("SELECT * FROM " . CoreOrgChartTree::model()->tableName())->query();
		while ($record = $reader->read()) {
			$ocList[(int) $record['idOrg']] = (int) $record['idst_oc'];
			$ocdList[(int) $record['idOrg']] = (int) $record['idst_ocd'];
		}

		//check list items
		foreach ($ocList as $idOrg => $ocItem) {
			if (in_array($ocItem, $list)) {
				$output[] = array('key' => (int) $idOrg, 'selectState' => 1);
			}
		}
		foreach ($ocdList as $idOrg => $ocdItem) {
			if (in_array($ocdItem, $list)) {
				$output[] = array('key' => (int) $idOrg, 'selectState' => 2);
			}
		}

		return $output;
	}

}
