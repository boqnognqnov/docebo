<?php

/**
 * This controller is used only as a user interface development tool
 */

class UiController extends Controller {

	/**
	 * This is the action to handle external exceptions.
	 * This action is set as errorHandler in application config.
	 */
	public function actionError() {
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				$this->render('error', $error);
			}
		}
	}

	/**
	 *
	 * @throws CHttpException
	 */
	public function actionApps() {

		$this->render('apps', array());

	}
}