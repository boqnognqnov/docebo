<?php

/**
 * The purpose of this class is to provide support for trial extending functionality.
 */

class TrialController extends Controller {
	/**
	 * (non-PHPdoc)
	 * @see CController::init()
	 */
	public function init() {
		parent::init();

		$this->layout = '//layouts/basic';
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		return array(

			array('allow',
				'users' => array('*'),
			),

			array('deny',
				'users' => array('*'),
			),
		);
	}


	/**
	 * AJAX called
	 */
	public function actionAxExtendTrialPeriod() {
		$result = array();

		// Checking if it is a valid ajax request
		if (Yii::app()->request->isAjaxRequest) {

			// Gets code and contact id
			$code = Yii::app()->request->getParam('code', false);
			$contact_id = Yii::app()->request->getParam('contact_id', false);

			// Makes API call which AGAIN, internally, should check validity of the code
			// It will record (in ERP DB) the fact that the code is consumed and who (contact id) did that
			$language = Yii::app()->getLanguage();
			$resultApi = ErpApiClient::apiExtendTrialPeriod($code, $contact_id, $language);

			if ($resultApi['success']) {

				// We use the ERP New expire date, as being more reliable. Returned by API. (date + time)
				$newDateTime = $resultApi["data"]["erp_new_expire_date"];
				$this->updateTrialPeriod($code, $newDateTime);

				// Date only
				$newDate = date('jS F Y', strtotime($newDateTime));

				echo $this->renderPartial('_trial_extend_success', array(
					"redirect_url" => Docebo::getUserHomePageUrl(),
					"newDate" => $newDate,
				));

			} else {
				echo $this->renderPartial('_trial_extend_error', array(
					'msg' => Yii::t("standard", $resultApi['msg']),
					'redirect_url' => Docebo::getUserHomePageUrl()
				));
			}
		}
		Yii::app()->end();
	}


	/**
	 *
	 */
	public function actionIndex() {
		// TEST
//         $params = array();
//         $params['numDays'] =  10;
//         $params['newExpiryDate'] =  date('Y-m-d', time());
//         $params['url'] =  Settings::get('url');
//         $this->render('cannot_extend',$params);

		$this->actionExtend();
	}


	/**
	 * Public action
	 */
	public function actionExtend() {

		$urlPlatform = Settings::get('url');
		$p = Yii::app()->request->getParam('p', false);

		// Missing "p", redirect
		if (empty($p) || !Docebo::isTrialPeriod()) {
			$this->redirect($urlPlatform, true);
		}

		// Params are coming as a JSON string !!!
		$params = base64_decode(urldecode($p));
		$paramsArr = CJSON::decode($params);

		$code = @$paramsArr["code"];
		$contact_id = @$paramsArr["cid"];
		$contact_name = @$paramsArr["cnm"];

		// Missing code/contact id is just ignored action. Redirect to home.
		if (!$code || !$contact_id) {
			$this->redirect($urlPlatform, true);
		}


		// Check if we have already used this code
		$trialExtensionUsedCodes = Settings::get('trial_extension_codes', '');
		if (in_array($code, explode(";", $trialExtensionUsedCodes))) {
			$this->render('already_extended', array(
				'url' => $urlPlatform,
				'contact_name' => $contact_name,
			));
			Yii::app()->end();
		}

		// Validate code
		$result = $this->validateCode($code);

		// Show "cannot extend" on failure
		if ($result["success"] == false) {
			$this->render('cannot_extend', array(
				'url' => $urlPlatform,
				'msg' => Yii::t("standard", $result["msg"]),
				'contact_name' => $contact_name,
			));
		} else {
			// Code is Valid. Show welcome and ask user to "Activate/Consume" the code.

			// !!! We trust the ERP DB based expiration date
			$oldDateTime = $result["data"]["trial_expire_date"];
			if (time() > strtotime($oldDateTime)) {
				$oldDateTime = date('Y-m-d');
			}
			$oldDateTime = date('Y-m-d', strtotime($oldDateTime)) . " 23:59:59";
			$newDateTime = date('Y-m-d', strtotime($oldDateTime . ' +' . $result['data']['extension_days'] . ' day')) . " 23:59:59";
			$newDate = date("jS F Y", strtotime($newDateTime));

			$params = array();
			$params['numDays'] = $result['data']['extension_days'];
			$params['newExpiryDate'] = $newDate;
			$params['url'] = $urlPlatform;
			$params['code'] = $code;
			$params['contact_name'] = $contact_name;
			$params['contact_id'] = $contact_id;


			$this->render('index', $params);
		}

		Yii::app()->end();
	}


	/**
	 *
	 * @param $codeToUse
	 * @param $newDate
	 */
	private function updateTrialPeriod($codeToUse, $newDateTime) {

		if (!empty($newDateTime)) {
			$this->extendTrialPeriod($newDateTime);
			// Adding the used code to the parameter trial_extension_codes
			$trialExtensionUsedCode = Settings::get('trial_extension_codes', '');

			if (empty($trialExtensionUsedCode)) {
				Settings::save('trial_extension_codes', $codeToUse);
			} else {
				$toAdd = explode(";", $trialExtensionUsedCode);
				$toAdd[] = $codeToUse;
				Settings::save('trial_extension_codes', implode(";", $toAdd));
			}
		}
	}


	public function actionExtended() {
		$params = array();
		$params['numDays'] = 10;
		$params['newExpiryDate'] = date('Y-m-d', time());
		$params['url'] = Settings::get('url');
		$this->render('cannot_extend', $params);
	}


	/**
	 *
	 * @param unknown $newDate
	 */
	private function extendTrialPeriod($newDateTime) {
		Settings::save('trial_expiration_date', $newDateTime);
	}


	/**
	 *
	 * @param int $code
	 * @return array
	 */
	private function validateCode($code) {

		// Make an api call to ERP to retrieve all the info about the code provided.
		// ERP makes all the checks for code validity
		$trialExtensionInfo = ErpApiClient::apiGetTrialExtensionInfo($code);

		if ($trialExtensionInfo['success'] == false) {
			$result = array(
				"success" => false,
				"msg" => Yii::t("standard", $trialExtensionInfo['msg']), // like 'expired', 'not exists', etc.
			);
		} else {
			$result = array(
				"success" => true,
				"data" => $trialExtensionInfo['data'],
			);
		}

		return $result;
	}


	public function actionTest() {

		$params = array("code" => '333', "cid" => 4, "cnm" => "Plamen Rossi");
		$params_json = CJSON::encode($params);
		//$paramsStr = implode(":", $params);
		$params64 = urlencode(base64_encode($params_json));
		$link = "http://lms.local/lms/?r=trial/extend&p={$params64}";

		$this->redirect($link);
	}


	/**
	 * To make manual tests, passing different parameters.
	 * Generates a proper link (json/base64 based) and redirects.
	 *
	 * Example: http://lms.local/lms/?r=trial/testx&code=222&cid=1&cnm=plamen
	 */
	public function actionTestx() {

		$code = Yii::app()->request->getParam('code', false);
		$contact_name = Yii::app()->request->getParam('cnm', '');
		$contact_id = Yii::app()->request->getParam('cid', false);

		$params = array("code" => $code, "cid" => $contact_id, "cnm" => $contact_name);
		$params_json = CJSON::encode($params);
		//$paramsStr = implode(":", $params);
		$params64 = urlencode(base64_encode($params_json));

		$urlPlatform = Settings::get('url');

		$link = $urlPlatform . "lms/?r=trial/extend&p={$params64}";

		$this->redirect($link);
	}


	public function actionTest2() {

		$daysToExtend = 2;

		// today, tonight
		$oldDate = date('Y-m-d');

		// some date, 23:59:59
		//$oldDate = "2013-02-13 02:11:11";

		$oldDate = date('Y-m-d', strtotime($oldDate)) . " 23:59:59";
		$newDate = date('Y-m-d', strtotime($oldDate . ' +' . $daysToExtend . ' day')) . " 23:59:59";

		echo "<br>" . $oldDate . " ----->> " . $newDate;

		$trial_remaining_days = 0;
		$trial_expiration_date = $newDate;
		if ($trial_expiration_date) {
			$trial_remaining_days = strtotime($trial_expiration_date)-time();
			if ($trial_remaining_days > 0) {
				// convert from seconds to days
				$trial_remaining_days = intval(($trial_remaining_days/3600)/24);
			} else {
				$trial_remaining_days = 0;
			}
		}

		echo "<br>" . $trial_remaining_days;
	}


}
