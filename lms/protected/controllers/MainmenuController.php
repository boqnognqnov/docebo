<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */


class MainmenuController extends Controller {

	// Character used to implode() parameters
	static private $_glue = '|';


	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		return array(
			array('allow', // allow authenticated users to access all actions
				'actions' => array('docebohelp'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated users to access all actions
				'actions' => array('marketplace', 'marketplaceRedirect'),
				'expression' => 'BrandingWhiteLabelForm::isMenuVisible() && Yii::app()->user->isGodAdmin && (Settings::get("enable_user_billing", "on") == "on" || isset(Yii::app()->request->cookies["reseller_cookie"]))'
			),
			array('deny',
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
			),


		);
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::init()
	 */
	public function init() {
		parent::init();
	}

	public function actionDocebohelp() {

		// retrive form data
		$attachment = false;
		$request = Yii::app()->request->getPost('help-desk-text');
		$target = Yii::app()->request->getPost('help-desk-target');
		if (isset($_FILES['help-desk-upload'])) {
			// manage uploaded file
			$attachment = array($_FILES['help-desk-upload']['name'] => $_FILES['help-desk-upload']['tmp_name']);
		}

		// other data needed
		$installation_id = Docebo::getErpInstallationId();

		// use erp api to retrive installation info
		$raw_api_res = ErpApiClient::apiErpGetCustomerInfo(array(
			'installation_id' => $installation_id
		));
		$customer_info = (array)CJSON::decode($raw_api_res, true);

		// compose message
		$mail_subject = Yii::t('standard', 'Contact us') . ' - ' . ucfirst($target);

		$mail_body = 'Installation: ' . Yii::app()->getBaseUrl(true)
			. "\n<br/>------------------------<br/>\n"
			. "E-Mail: " . Yii::app()->user->getUserAttrib('email') . "<br />
			First Name: " . Yii::app()->user->getUserAttrib('firstname') . "<br />
			Last Name: " . Yii::app()->user->getUserAttrib('lastname') . "<br />
			Company or Organization: " . (isset($customer_info['info']) ? $customer_info['info']['company_name'] : '') . "<br />
			Country: " . (isset($customer_info['info']) ? $customer_info['info']['country'] : '') . "<br />
			Billing data provided: " . (isset($customer_info['info']) && $customer_info['info']['has_customer'] == 1 ? 'yes' : 'no') . "<br />
			<br/>
			User Ip : " .Yii::app()->request->getUserHostAddress() . "<br/>
			User Agent : " .Yii::app()->request->getUserAgent() . "<br/>
			<br/>
			To: " . $target . "<br />
			Message:  <br />\n"
			. nl2br($request);

		// save this info as a crm entry in erp
		$raw_api_res = ErpApiClient::apiErpGetExtHelpdeskManager(array(
			'installation_id' => $installation_id
		));
		$response = (array)CJSON::decode($raw_api_res, true);
		$error = '';
		$res = false;

		try {
			if (empty($response) || !$response['success']) {
				$error = (!empty($response['msg'])) ? $response['msg'] : 'Operation failed';
				throw new CException($error);
			}
			$mailer = new MailManager();

			// from: info@docebo.com and reply-to: user's email so as the email is handled correctly by spam filters
			// Claudio asked to use the original email anyway
			$user_email = Yii::app()->user-> getUserAttrib('email');

			$sender = ( !empty($user_email) ? $user_email : Yii::app()->params['support_sender_email'] ); //;
			$reply_to = array($user_email);

			$emailList = $response['data'];
			$isHandledByDocebo = empty($emailList);

			if ($isHandledByDocebo) {
				// support handled by docebo

				if ($target == 'help') {
					$helpEmail = Yii::app()->params['support_help_email'];
					if (!$helpEmail) {
						throw new CException('Docebo help email not found.');
					} else {
						$recipients = array($helpEmail);
						$res = $mailer->mail($sender, $recipients, $mail_subject, $mail_body, $attachment, false, false, $reply_to);
					}
				} else {
					// sales
					$customerIsExtraItaly = ($customer_info['info']['country'] == 'italy' ? false : true);
					$salesEmail = $customerIsExtraItaly
						? Yii::app()->params['support_sales_international_email']
						: Yii::app()->params['support_sales_national_email'];
					$recipients = array($salesEmail);
					$res = $mailer->mail($sender, $recipients, $mail_subject, $mail_body, $attachment, false, false, $reply_to);
				}
			} else {
				// support handled by reseller
				$res = $mailer->mail($sender, $emailList, $mail_subject, $mail_body, $attachment, false, false, $reply_to);
			}

			// Add the entry as a contact history in the ERP/CRM:
			$crm_contact_history_param = array(
				'installation_id' => $installation_id,
				'text' 			=> Yii::app()->input->stripClean($mail_body),
				'incl_domain_name' => false,
				'h_type' 		=> 'customer_action',
				'h_from' 		=> 'ticket',
				'h_param02' 	=> 'ticket_'.$target,

			);
			ErpApiClient::apiErpAddCrmContactHistory($crm_contact_history_param);

			// Change the Incoming referrer to "site":
			$crm_company_param = array(
				'installation_id' => $installation_id,
				'referrer' => 'site',
				'refresh_incoming' => true
			);
			ErpApiClient::apiErpUpdateCrmCompanyInfo($crm_company_param);

			// prepare success message
			Yii::app()->user->setFlash('success', Yii::t('standard', 'Thanks for contacting us, our Team will reach you by email at: {email}', array(
				'{email}' => Yii::app()->user-> getUserAttrib('email')
			)));
		} catch (CException $ex) {

			$error = $ex->getMessage();
			// save it as flash message
			Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
		}
		// back to ... home page
		$this->redirect(array('site/index'));
	}

    /**
     * Marketplace page (iframe to ERP marketplace)
     */
	public function actionMarketplace() {

		$this->layout = '//layouts/menu-only';

		$this->render('marketplace');
	}

    /**
     * Redirect to Marketplace with POST data
     */
	public function actionMarketplaceRedirect() {

		$this->layout = false;

        $params = $this->getParamsArray();

        //CVarDumper::dump($params,20,true); die;

		echo $this->renderPartial('marketplace_redirect', array(
            'params' => $params,
            'redirect_url' => $this->getUrl($params),
        ));
	}

	/**
	 * Create and return an array of LMS parameters, ment to be known by Marketplace,
	 * All included in authorization key/hash.
	 */
	private function getParamsArray() {

		$domain_name =preg_replace('/^www\\./', '', strtolower($_SERVER['HTTP_HOST']));

        $user = Yii::app()->user->loadUserModel();

		$res = array(
				'domain_name' => $domain_name,
				'installation_id' => Settings::getCfg('erp_installation_id'),
				'erp_key' => Settings::getCfg('erp_key'),
				//'userid' => $user->userid, // actually, the user name
				'userid' => $user->getClearUserId(), // actually, the user name
				'idst' => $user->idst, // user ID

		);

		return $res;
	}

	/**
	 * Build Redirection URL, incl. the hash key
	 */
	private function getUrl($params) {
		$mp_http = 		Settings::getCfg('marketplace_http'		, 'http');
		$mp_url = 		Settings::getCfg('marketplace_url'		, 'test-erp.docebo.info/marketplace');
		$mp_landing = 	Settings::getCfg('marketplace_landing'	, 'lmsZone/lmsLanding');

		if(Settings::get('https_force_redirect', 'off') == 'on' || Yii::app()->request->isSecureConnection)
			$mp_http = 'https';


		// Now, lets add some salt
		$params[] = Settings::getCfg('erp_secret_key');

		// Stringify...
		$combo = implode(self::$_glue, $params);

		// Scramble...
		$auth_key = base64_encode(sha1($combo));

		$url = $mp_http . "://" . $mp_url . "/index.php?r=" . $mp_landing . "&a=" . $auth_key;

		return $url;
	}

}
