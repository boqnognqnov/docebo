<?php


class ImageSelectorController extends Controller {
	
	
	public function actionDialog() {
	
		$modalId		= Yii::app()->request->getParam('modal_id', 'image-selector-modal-id');
		$elementId 		= Yii::app()->request->getParam('element_id', false);
		$imageType 		= Yii::app()->request->getParam('type', CoreAsset::TYPE_GENERAL_IMAGE);
		$returnVariant 	= Yii::app()->request->getParam('return_variant', CoreAsset::VARIANT_SMALL);
		$preselectedId	= (int) Yii::app()->request->getParam('preselected_id', null);
        $showStandardImages = Yii::app()->request->getParam('showStandardImages');
	
		// SAVE button
		$confirm_save = Yii::app()->request->getParam("confirm_save", false);
	
		// Delete requested
		$delete_image = Yii::app()->request->getParam("delete_image", false);

		$radioName = 'image_id_radio';
	
		$transaction = null;
		if (Yii::app()->db->getCurrentTransaction() === NULL) { $transaction = Yii::app()->db->beginTransaction(); }
	
		// Wrap everything in try/catch and provide DB transaction commit/rollback
		try {
	
			// If user selected an image to upload, automatic AJAX form submition is executed
			// Lets get the file and save it. No cropping functionality.
			if (!empty($_FILES["image_manage_file"]) && !$confirm_save && !$delete_image) {
	
				// Get hthe uploaded file
				$uploadedFile 	= CUploadedFile::getInstanceByName("image_manage_file");
				$filePath      	= $uploadedFile->getTempName();
				$tmpFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $uploadedFile->getName();
				copy($filePath, $tmpFilePath);
	
				$asset = new CoreAsset();
				$asset->sourceFile = $tmpFilePath;
				$asset->type = $imageType;
	
				// Validate!! && Save
				$ok = $asset->save();
				if (!$ok) {
					throw new CException(Yii::t("standard", '_OPERATION_FAILURE'));
				}
	
				// Commit
				if ($transaction) $transaction->commit();
	
				$ajaxResult = new AjaxResult();
	
				$data = array(
						'image_id' => $asset->id,
						'image_url' => $asset->getUrl($returnVariant),
				);
	
				$ajaxResult->setStatus(true)->setData($data)->toJSON();
	
				// Clean up
				FileHelper::removeFile($tmpFilePath);
	
				Yii::app()->end();
	
			}
	
			// SAVE CHANGES button is clicked
			if ($confirm_save) {
				$selectedImageId = Yii::app()->request->getParam($radioName, false);
				if ($selectedImageId) {
					$src = CoreAsset::url($selectedImageId, $returnVariant);
					$ajaxResult = new AjaxResult();
					$data = array(
							'image_id' => $selectedImageId,
							'image_url' => $src,
					);
					$ajaxResult->setStatus(true)->setData($data)->toJSON();
				}
				Yii::app()->end();
			}
	
			// DELETE AJAX call
			if ($delete_image) {
				$imageId = Yii::app()->request->getParam("image_id", false);
				$success = false;
				if ((int) $imageId > 0) {
					$asset = CoreAsset::model()->findByPk($imageId);
					if ($asset) {
						$success = $asset->delete();
					}
				}
				$ajaxResult = new AjaxResult();
				$data = array();
				$ajaxResult->setStatus($success)->setData($data)->toJSON();
				if ($transaction) $transaction->commit();
				Yii::app()->end();
			}
	
	
			if ($transaction) $transaction->commit();
	
	
	
		}
		catch (CException $e) {
			// Clean up
			FileHelper::removeFile($tmpFilePath);
			// Log error
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			// Rollback
			if ($transaction) $transaction->rollback();
	
			// Send result to caller
			$ajaxResult = new AjaxResult();
			$ajaxResult->setStatus(false)->setMessage($e->getMessage())->toJSON();
	
			Yii::app()->end();
		}
	
	
		// We end up here on first load
		$chunkSize = 6;
	
		$assets = new CoreAsset();
		$systemImages   = $assets->urlList($imageType, CoreAsset::SCOPE_SHARED, $chunkSize, CoreAsset::VARIANT_SMALL);
		$userImages   	= $assets->urlList($imageType, CoreAsset::SCOPE_PRIVATE, $chunkSize, CoreAsset::VARIANT_SMALL);
		
		$html = $this->renderPartial('_modal', array(
				'systemImages' 	=> $systemImages,
				'userImages'	=> $userImages,
				'elementId'		=> $elementId,
				'modalId'		=> $modalId,
				'imageType'		=> $imageType,
				'radioName'		=> $radioName,
				'preselectedId'	=> $preselectedId,
                'showStandardImages' => $showStandardImages
		),true,true);
	
		// Echo HTML to Dialog2
		echo $html;
	
	}
	
	
	
}