<?php

class ScriptController extends Controller {

    private $columnsAlreadyInGmt = array(
        '`core_newsletter`' => array('stime'),
        '`core_user`' => array('register_date'),
        '`core_password_history`' => array('pwd_date'),
        '`core_deleted_user`' => array('deletion_date'),
        '`learning_courseuser`' => array('date_inscr')
    );

    private $tablesToSkip = array(
        'gamification_assigned_badges', 'myblog_comment', 'myblog_post'
    );

    public function actionUpgradeDatetime() {

        if (Yii::app()->request->isPostRequest) {
            $fromTz = Yii::app()->request->getParam('from_tz');
            $toTz   = Yii::app()->request->getParam('to_tz');

            $datetimeColumns = $this->_getDatetimeColumns();
            $sql = $this->_generateDatetimeUpgradeSql($datetimeColumns, $fromTz, $toTz);
            echo nl2br($sql);
            Yii::app()->end();
            //$filename = 'datetime_to_timestamp_'.date('Y_m_d_H_i_s').'.sql';
            //Yii::app()->request->sendFile($filename, $sql, 'class');
        }

        echo CHtml::tag('h2', array(), 'Generate sql to convert datetime columns to timestamp')
        . CHtml::tag('div', array('style' => 'font-weight: bold;'), "Don't forget to import the timezone table into mysql:")
        . CHtml::tag('div', array('style' => 'font-style: italic;'), "mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql -u root -p mysql")
        . CHtml::tag('br', array())
        . CHtml::form()
            . CHtml::label('From timezone:', 'from_tz') . '<br>' . CHtml::textField('from_tz', '+01:00') . '<br>'
            . CHtml::label('To timezone:', 'to_tz') . '<br>' . CHtml::textField('to_tz', '+00:00') . '<br>'
            . '<br>' . CHtml::submitButton('Generate SQL')
        . CHtml::endForm();
    }

    /**
     * @return array
     */
    private function _getDatetimeColumns() {
        $dbSchema = Yii::app()->db->schema;
        $datetimeColumns = array();
        $tables = $dbSchema->getTables();
        foreach ($tables as $tbl)
        {
            if(in_array($tbl->rawName, $this->tablesToSkip))
                continue;

            $tblDatetimeColumns = array();
            foreach ($tbl->columnNames as $name) {
                $columnSchema = $tbl->getColumn($name);
                if ($columnSchema->dbType == 'datetime') {
                    $tblDatetimeColumns[] = $name;
                }
            }
            if (!empty($tblDatetimeColumns)) {
                $datetimeColumns[$tbl->rawName] = $tblDatetimeColumns;
            }
        }
        return $datetimeColumns;
    }

    /**
     * @param array $datetimeColumns
     * @return string
     */
    private function _generateDatetimeUpgradeSql($datetimeColumns = array(), $fromTz = '', $toTz = '') {
        $alterSql = array();
        $updateSql = array();

        foreach ($datetimeColumns as $tblName => $columnNames) {
            $_modifySql = array();
            foreach ($columnNames as $columnName) {
                $_modifySql[] = 'MODIFY COLUMN `'.$columnName.'` TIMESTAMP';
            }
            $_modifySql = implode(",\n\t", $_modifySql);
            $alterSql[] = "ALTER TABLE $tblName\n\t$_modifySql;";

            if (!$fromTz || !$toTz) continue;

            foreach ($columnNames as $columnName) {
                if(!isset($this->columnsAlreadyInGmt[$tblName]) || !in_array($columnName, $this->columnsAlreadyInGmt[$tblName])) {
                    $updateSql[] = "UPDATE $tblName SET\n\t`$columnName` = CONVERT_TZ(`$columnName`, '$fromTz', '$toTz')".
                        "WHERE `$columnName` <> '0000-00-00 00:00:00' AND `$columnName` IS NOT NULL;";
                }
            }
        }
        $alterSql = implode("\n", $alterSql);
        $updateSql = implode("\n", $updateSql);
        return implode("\n\n", array($updateSql, $alterSql));
    }
}

