<?php

class CoursepathController extends Controller {

	/**
	 * (non-PHPdoc)
	 * @see CController::init()
	 */
	public function init() {
		parent::init();
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl',
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		return array(

			// Allow all users the following
			array('allow',
				'actions' => array('axHoverDetails', 'details', 'buy'),
				'users' => array('*'),
			),

			array('allow',
				'actions' => array('subscribe', 'deeplink'),
				'users' => array('@'),
			),

			// Deny all other actions not whitelisted above
			array('deny',
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
			),
		);
	}

	/**
	 * Self enrollment action
	 */
	public function actionSubscribe() {
		$id = (int) Yii::app()->request->getParam('id'); // ID of LearningCoursepath
		try {
			// Load learning plan ...
			$model = LearningCoursepath::model()->findByPk($id); /* @var $model LearningCoursepath */
			if (!$model)
				throw new CException('Learning plan not found');

			// ... and check that is visible in catalog and not on sale
			if (!$model->visible_in_catalog || $model->is_selling == 1)
				throw new CException("This learning plan does not allow free self enrollment");

			// Enroll the logged in user in the learning plan (container)
			$userId = Yii::app()->user->id;
			$user = new LearningCoursepathUser();
			$user->id_path = $id;
			$user->idUser = $userId;
			$user->waiting = 0;
			$user->course_completed = 0;
			$user->date_assign = Yii::app()->localtime->toLocalDateTime();
			$user->subscribed_by = $userId;
			$user->save();

			// Subscribe user to all LP's courses
			$model->refresh(); // !! Important
			$model->subscribeSingleUserToCourses(Yii::app()->user->getIdst());

			// If user is enrolled to a Learning Plan. Notification: User was enrolled to a Learning Plan is fired.
			Yii::app()->event->raise(EventManager::EVENT_USER_ENROLLED_IN_LEARNING_PLAN, new DEvent($this, array('user_id' => $userId, 'learning_plan' => $model)));

		} catch(Exception $e) {
			Yii::app()->user->setFlash('error', Yii::t("standard", '_OPERATION_FAILURE'));
		}

		$this->redirect(Docebo::createLmsUrl("curricula/show"), true);
	}

	public function actionBuy(){
		$id = (int) $_REQUEST['id']; // ID of LearningCoursepath

		/* @var $model CoursepathCartPosition */
		$model = CoursepathCartPosition::model()->findByPk($id);

		$selectedCourseIds = (array) $_REQUEST['courses'];

		$res = array();

		try{
			if(!$model)
				throw new CException(Yii::t("curricula",'Learning plan not found'));

			if(!$model->is_selling){
				throw new CException(Yii::t("curricula",'This learning plan is not for sale'));
			}

			$allCoursesInLp = $model->learningCourse;

			if(empty($allCoursesInLp))
				throw new CException(Yii::t("curricula",'Learning plan is empty'));

			if($model->purchase_type == LearningCoursepath::PURCHASE_TYPE_FULL_PACK_ONLY){
				$model->setCourses($model->learningCourse);
			}elseif($model->purchase_type == LearningCoursepath::PURCHASE_TYPE_SINGLE_COURSES_IN_SEQUENCE){
				if(empty($selectedCourseIds)){
					throw new CException('No courses selected');
				}

				$selectedCoursesToBuy = array();
				foreach($allCoursesInLp as $lpCourse){
					if(in_array($lpCourse->idCourse, $selectedCourseIds))
						$selectedCoursesToBuy[] = $lpCourse;
				}

				$model->setCourses($selectedCoursesToBuy);
			}

			$key = $model->getCartItemId();

			if(Yii::app()->shoppingCart->itemAt($key) instanceof IECartPosition)
				Yii::app()->shoppingCart->remove($key);

			Yii::app()->shoppingCart->put($model);

			Yii::log("Added Learning Plan '{$model->path_name}' to cart. Selected ".count($model->getCourses())." courses. Purchase type: " . $model->purchase_type, CLogger::LEVEL_INFO);

			$res = array(
				'success'=>true,
			);
		}catch(Exception $e){
			$res = array(
				'success'=>false,
				'error'=>$e->getMessage(),
			);
		}

		$this->sendJSON($res);
	}

	public function actionDetails(){
		if(Yii::app()->user->isGuest && Settings::get('catalog_external', 'off') != 'on')
		{
			Yii::app()->user->setFlash('error', Yii::t("standard", "Unauthorized access"));
			$this->redirect(Docebo::createAppUrl('lms:site/index'));
		}

		$idPath = Yii::app()->request->getParam('id_path');

		/* @var $lpModel LearningCoursepath */
		$lpModel = LearningCoursepath::model()->with('learningCourse')->findByPk($idPath);

        if (Yii::getPathOfAlias('jplayer') === false)
            Yii::setPathOfAlias('jplayer', Yii::app()->basePath.'/../../common/extensions/jplayer');

        $assetsPath = Yii::getPathOfAlias('jplayer.assets');
        $assetsUrl = Yii::app()->assetManager->publish($assetsPath);
        Yii::app()->getClientScript()->registerCssFile($assetsUrl . "/skin/dialog2_videoplayer/jplayer.css");
        Yii::app()->getClientScript()->registerScriptFile($assetsUrl . '/js/jquery.jplayer.min.js', CClientScript::POS_HEAD);

		if(!$lpModel)
			throw new CHttpException('Learning plan does not exist');

		if(!Yii::app()->user->isGuest && !Yii::app()->user->getIsGodadmin() && !LearningCoursepath::coursepathDetailsIsVisibleForUser($idPath))
		{
			Yii::app()->user->setFlash('error', Yii::t("standard", "Unauthorized access"));
			$this->redirect(Docebo::createAppUrl('lms:site/index'));
		}

		$params = array(
			'model'=>$lpModel,
			'idUser'=>Yii::app()->user->getIdst(),
		);

        // Raise Event right before the coursepath details page is shown
        $event = new DEvent($this, array('coursepath' => $lpModel));
        Yii::app()->event->raise('beforeRenderCoursePathDetails', $event);
        if($event->shouldPerformAsDefault() && $event->return_value){
            echo $event->return_value;
        }

		$this->render('details_coursepath', $params);
	}

	public function actionAxHoverDetails() {
		$idPath 	= Yii::app()->request->getParam('id_path');
		$resetToken = Yii::app()->request->getParam('reset', false);

		/* @var $lpModel LearningCoursepath */
		$lpModel = LearningCoursepath::model()->with('learningCourse')->findByPk($idPath);

		if ($lpModel) {
			$params = array(
				'model' => $lpModel,
				'id_path' => $lpModel->id_path,
				'id_user'=>Yii::app()->user->id,
				'resetToken' => $resetToken,
			);

			$showButton = NULL;

            // Raise Event right before the coursepath details page is shown
            $event = new DEvent($this, array('coursepath' => $lpModel, 'params' => $params));
            Yii::app()->event->raise('beforeRenderCoursePathAxHoverDetails', $event);
            if($event->shouldPerformAsDefault() && $event->return_value){
                echo $event->return_value;
            }

			$this->renderPartial('_hover_details', $params);
		}
	}

	protected function renderExternalCatalogCheckbox($data, $row){
		// $data the current data object from the dataprovider
		// $row the row index

		/* @var $data LearningCoursepathCourses */
		$course = $data->idItem; /* @var $course LearningCourse */

		$userEnrolledInCourse = LearningCourseuser::isSubscribed(Yii::app()->user->getIdst(), $course->idCourse);

		if($userEnrolledInCourse){
			return '<span class="i-sprite is-check green"></span>';
		}

		return CHtml::checkBox('course-check-to-buy', false, array(
			// 'disabled' => ($row==0 ? '' : 'disabled'),
			'class'=>'course-check-to-buy',
			'data-idcourse'=>$course->idCourse,
			'data-price'=>$course->getCoursePrice(),
			'data-sequence'=>$data->sequence,
		));
	}

	protected function renderDetailsIconColumn($data, $row){
		// $data the current data object from the dataprovider
		// $row the row index

		/* @var $data LearningCoursepathCourses */
		$course = $data->idItem; /* @var $course LearningCourse */

		$detailsUrl = Docebo::createLmsUrl('course/axDetails', array('id'=>$course->idCourse, 'only_details'=>'1'));

		echo '<a id="course-details-'.$course->idCourse.'" title="'.$course->name.'" rel="dialog-course-detail-'.$course->idCourse.'" href="'.$detailsUrl.'" class="open-dialog" data-dialog-class="course-details-modal fixed-dialog"><span class="i-sprite is-search"></span></a>';
	}

	private function getRedirectUrlAndSetFlash($errorCode, $coursepath = null){
		$redirectUrl = '';
		$hasNewThemeActivated = Yii::app()->legacyWrapper->hydraFrontendEnabled();
		$slug = Yii::app()->urlManager->slugify($coursepath->path_name);

		switch ($errorCode) {
			case '2001':
				//redirect to home and operation failure
				Yii::app()->user->setFlash('error', Yii::t("standard", '_OPERATION_FAILURE'));
				$redirectUrl = ($hasNewThemeActivated) ? Docebo::createHydraUrl('learn', 'home', array('msg' => $errorCode)) : Docebo::createLmsUrl("site/index");
				break;
			case '2020':
				//redirect to home and operation failure
				Yii::app()->user->setFlash('error', Yii::t('coursepath','Deep linking is not working for Learning Plans. Please, contact your administrator for further information.'));
				$redirectUrl = ($hasNewThemeActivated) ? Docebo::createHydraUrl('learn', 'home', array('msg' => $errorCode)) : Docebo::createLmsUrl("site/index");
				break;
			case '2021':
				//redirect to lp details and operation failure
				Yii::app()->user->setFlash('error', Yii::t('coursepath','You need to buy the Learning Plan'));
				$redirectUrl = ($hasNewThemeActivated) ? Docebo::createHydraUrl('learn', 'learning_plan/view/' . $coursepath->id_path . '/' . $slug) : Docebo::createLmsUrl('coursepath/details', array('id_path' => $coursepath->id_path));
				break;
			case '2022':
				//redirect to lp details and operation success
				$redirectUrl = ($hasNewThemeActivated) ? Docebo::createHydraUrl('learn', 'learning_plan/view/' . $coursepath->id_path . '/' . $slug) : Docebo::createLmsUrl('coursepath/details', array('id_path' => $coursepath->id_path));
				break;
			default:
				//redirect to lp on success
				$redirectUrl = ($hasNewThemeActivated) ? Docebo::createHydraUrl('learn', 'lp/' . $coursepath->id_path  . '/' . $slug) : Docebo::createLmsUrl('curricula/show', array('id_path' => $coursepath->id_path));
				break;
		}

		return $redirectUrl;
	}

    public function actionDeeplink() {

        $id_path        = Yii::app()->getRequest()->getQuery('id_path');
        $generated_by   = Yii::app()->getRequest()->getQuery('generated_by');
        $hash           = Yii::app()->getRequest()->getQuery('hash');
        $currentUser    = Yii::app()->user->id;
        $localHash = sha1($id_path.",".$generated_by.",".Yii::app()->params['deeplink_secret_key']);

		$redirectCode = '';
		$learningCoursepath = null;
		if(PluginManager::isPluginActive('CurriculaApp')) {
            try {
                if ( $hash === $localHash ) {
					$learningCoursepath = LearningCoursepath::model()->findByPk(array('id_path' => $id_path));
					if (!$learningCoursepath || $learningCoursepath->deep_link == 0) {
						$redirectCode = '2020';
					} else {
						if ($learningCoursepath->is_selling) { // PAID LEARNING PLAN
							$redirectCode = '2021';
						} else {
							$isSubscribed = LearningCoursepathUser::loadSubscription($currentUser, $id_path);

							if (!$isSubscribed) { // USER NOT SUBSCRIBED TO LEARNING PLAN
								$subscribed = LearningCoursepathUser::subscribeUser($currentUser,
									$id_path); // SUBSCRIBE USER TO LEARNING PLAN AND COURSES
								if ($subscribed) {
									$coursepathUserModel = LearningCoursepathUser::model()->findByPk(array(
										'id_path' => $id_path,
										'idUser' => $currentUser
									));
									$coursepathUserModel->deeplinked_by = $generated_by;
									$coursepathUserModel->save(); // SAVE LearningCoursepathUser !!!
								}
								$redirectCode = '2022';
							} else { // USER IS SUBSCRIBED TO LEARNING PLAN
								$coursepathUserModel = LearningCoursepathUser::model()->findByPk(array(
									'id_path' => $id_path,
									'idUser' => $currentUser
								));
								$coursepathUserModel->deeplinked_by = $generated_by;
								$coursepathUserModel->save();   // SAVE LearningCoursepathUser !!!
								$redirectCode = '2022';
							}
						}
					}
                } else { // HASH IS NOT CORRECT ---- redirect to index with general failure
					$redirectCode = '2001';
                }
            } catch ( Exception $e) {
				$redirectCode = '2001';
              }
        }
        else{
			$redirectCode = '2020';
        }

		$this->redirect($this->getRedirectUrlAndSetFlash($redirectCode, $learningCoursepath), true);
    }
}