<?php

class SiteController extends Controller {

	const INDEX = 'trainingIndexFlag';

	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl',
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		$res = array(

			array('allow',
				'actions' => array('error', 'login', 'logout', 'openSessionViaToken', 'loginRest', 'hybridauthEndpoint', 'index', 'colorschemeCss', 'getUserPage', 'privacy', 'sso', 'axUploadFile', 'axApproveLastTos', 'autoLogin', 'isAlive', 'setShopifySession'),
				'users' => array('*'),
			),
		);

		$admin_only_actions = array('planExpirationContact', 'planExpirationCretditCard', 'planExpirationWireTransfer', 'renewSubscription');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
			'deniedCallback' => array($this, 'accessDeniedCallback'),
			'message' => Yii::t('course', '_NOENTER'),
		);

		$res[] = array('allow',
			'users' => array('@'),
		);

		$res[] = array('deny',
			'users' => array('*'),
			'deniedCallback' => array($this, 'accessDeniedCallback'),
		);

		return $res;
	}

	/**
	 * This is the action to handle external exceptions.
	 * This action is set as errorHandler in application config.
	 */
	public function actionError() {
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo CHtml::encode($error['message']);
			} else {
				$this->renderPartial('error', $error);
			}
		}
	}

	/**
	 * Dummy endpoint used by Hydra FE to open a PHP session
	 * and return the session cookie for legacy API calls (e.g. scorm tracking)
	 */
	public function actionOpenSessionViaToken(){
	    $skipReenterCc = (bool) Yii::app()->request->getParam('skipreentercc', false);
	    if ($skipReenterCc) {
	        Yii::app()->session['reinsert-cc-data'] = false;
	    }
		return;
	}

	/**
	 * Action that handles sso with token
	 * Example URL: /lms/?r=site/sso&login_user=XXXX&time=12321321321&token=AERQWETWERTR
	 * [optional] &id_course=1234
	 * [optional] &destination=[mycourse|catalog|learningplan]
	 */
	public function actionSso() {

		try {
            // Keep in session if the user is coming from Hydra
            $isComingFromHydra = isset($_GET['sso_auth_mode']) && $_GET['sso_auth_mode']  == 'oauth2' && isset($_GET['sso_target']) && $_GET['sso_target'] == 'hydra';
            Yii::app()->session['isComingFromHydra'] = $isComingFromHydra || Yii::app()->legacyWrapper->hydraFrontendEnabled();

			// Prepare a SSO dispatch table, to determine which app will handle the current request
			$sso_dispatch_table = array();
			Yii::app()->event->raise('RegisterSSOHandlers', new DEvent($this, array('map' => &$sso_dispatch_table)));
			if (empty($sso_dispatch_table)) {
                // Check from where is coming the user and redirect him accordingly
			    if ( Yii::app()->session['isComingFromHydra'] ) {
                    unset(Yii::app()->session['isComingFromHydra']);
                    $url = Docebo::createHydraUrl('learn', 'signin', array( 'type' => 'oauth2_response', 'error' => Yii::t('login', '_NOACCESS')) );
                    $this->redirect($url);
                }

				throw new Exception("No SSO handlers registered. Aborting SSO request.");
            }

			// Get the sso type of the current request
			$sso_type = Yii::app()->request->getParam('sso_type', 'token');

			// Read auth mode. Possible auth modes:
			// - "session" (default): LMS login session
			// - "oauth2": generates and returns an OAuth2 session (used only by webapp)
			$authentication_mode = Yii::app()->session['sso_auth_mode'];
			if(!$authentication_mode)
				$authentication_mode = Yii::app()->request->getParam('auth_mode');

			// If we're being called by a Mobile app user agent and "auth_mode" was not specified,
			// force the mode to "oauth2"
			if(!$authentication_mode && Docebo::isMobile())
				$authentication_mode = 'oauth2';

			Yii::app()->session['sso_auth_mode'] = $authentication_mode ? $authentication_mode : 'session';

			$offset = Yii::app()->request->getParam('offset', '');
			if($offset !== '')
			{
				Yii::app()->session['offset'] = $offset;
			}

			// Dispatch the request to the proper handler
			if (isset($sso_dispatch_table[$sso_type]) && !is_null($sso_dispatch_table[$sso_type])) {
				/* @var $handler ISsoHandler */
				$handler = $sso_dispatch_table[$sso_type];
				$userid = $handler->login();
				if (!$userid) {

                    // Check from where is coming the user and redirect him accordingly
                    if ( Yii::app()->session['isComingFromHydra'] ) {
                        unset(Yii::app()->session['isComingFromHydra']);
                        $url = Docebo::createHydraUrl('learn', 'signin', array( 'type' => 'oauth2_response', 'error' => Yii::t('login', '_NOACCESS')) );
                        $this->redirect($url);
                    }

					throw new Exception("No valid user logged in");
                }

				// Search for the user in DB and authenticate him without password
				$ui = new DoceboUserIdentity($userid, null);
				if ($ui->authenticateWithNoPassword()) {
					if ($ui->checkUserAlreadyLoggedIn()){
					    $message = Yii::t('login', '_TWO_USERS_LOGGED_WITH_SAME_USERNAME');
                        // Check from where is coming the user and redirect him accordingly
                        if ( Yii::app()->session['isComingFromHydra'] ) {
                            unset(Yii::app()->session['isComingFromHydra']);
                            $url = Docebo::createHydraUrl('learn', 'signin', array( 'type' => 'oauth2_response', 'error' => $message) );
                            $this->redirect($url);
                        }

						throw new Exception($message, 4);
                    }

                    // Check from where is coming the user and redirect him accordingly
                    if ( Yii::app()->session['isComingFromHydra'] ) {

                        /** Since he is already Authenticated, Create Token for him and Return him back to Hydra Frontend */
                        $server = DoceboOauth2Server::getInstance();
                        $server->init();
                        $token = $server->createAccessToken("hydra_frontend", $ui->getId(), "api");
                        unset(Yii::app()->session['isComingFromHydra']);
                        $url = Docebo::createHydraUrl('learn', 'signin',array_merge(array("type" => "oauth2_response"),$token) );
                        $this->redirect($url);

                    } else if($authentication_mode == 'oauth2') {
						unset(Yii::app()->session['sso_auth_mode']);
						$server = DoceboOauth2Server::getInstance();
						if($server) {
							$server->init();
							$token = $server->createAccessToken("mobile_app", $ui->getId(), "webapp");
							if($token)
								$response = array('success' => true, 'token' => $token);
							else
								$response = array('success' => false, 'message' => "Could not create Oauth2 token");
						} else
							$response = array('success' => false, 'message' => "Could not initialize Oauth2 server component");

                        $this->redirect(Docebo::getWebappUrl(true) . '?oauth2_response=' . urlencode(CJSON::encode($response)));
					} else {
						Yii::app()->user->login($ui);

						if(isset(Yii::app()->session['offset']))
						{
							$settingTimezone = CoreSettingUser::model()->findByAttributes(array(
								'id_user' =>  Yii::app()->user->id,
								'path_name' => 'timezone',
							));

							// get client timezone and set it if have't selected in lms
							if ((!$settingTimezone || !$settingTimezone->value) && Settings::get('timezone_allow_user_override', 'off') === 'on'){
								$clientTimezoneOffset = Yii::app()->session['offset'];
								unset(Yii::app()->session['offset']);
								$serverTimezones = Yii::app()->localtime->getTimezoneOffsetsSecondsArray();
								$foundedTimezone = array_search($clientTimezoneOffset, $serverTimezones);
								if($foundedTimezone){
									$userId =  Yii::app()->user->id ;

									if ($settingTimezone === null){
										$settingTimezone = new CoreSettingUser('create');
										$settingTimezone->id_user =  $userId;
										$settingTimezone->path_name = 'timezone';
									}

									$settingTimezone->value = $foundedTimezone;
									if($settingTimezone->save()){
										$userName = Yii::app()->user->getUsername();
										$user = $userName ? $userName : $userId;
										Log::_('User: '.$user.' time zone '.$foundedTimezone.' has been automatically set.');
									}
								}
							}
						}

						// Login is NOT enough to say that user is Active;
						// As of Mid December'13 User is counted as active when he/she enters a course
						// UserLimit::logActiveUserLogin();
						// if the authentication goes well we need to check for a suspended installation process
						Docebo::resumeSaasInstallation();

						// Determine which URL to jump to
						$redirect_url = $handler->getLandingURL();
						if (!$redirect_url)
							$redirect_url = Docebo::getUserAfetrLoginRedirectUrl();

						// Finally, redirect!!
						$this->redirect($redirect_url);
					}
				} else {

                    // Check from where is coming the user and redirect him accordingly
                    if ( Yii::app()->session['isComingFromHydra'] ) {
                        unset(Yii::app()->session['isComingFromHydra']);
                        $url = Docebo::createHydraUrl('learn', 'signin', array( 'type' => 'oauth2_response', 'error' => Yii::t('login', '_NOACCESS')) );
                        $this->redirect($url);
                    }

					$ui->logFailedLoginAttempt();
					throw new Exception("Invalid username or user suspended", 2);
				}
			} else {
                // Check from where is coming the user and redirect him accordingly
                if ( Yii::app()->session['isComingFromHydra'] ) {
                    unset(Yii::app()->session['isComingFromHydra']);
                    $url = Docebo::createHydraUrl('learn', 'signin', array( 'type' => 'oauth2_response', 'error' => Yii::t('login', '_NOACCESS')) );
                    $this->redirect($url);
                }

				throw new Exception("Wrong or unknown sso handler.");
            }
		} catch (Exception $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);

			$authentication_mode = Yii::app()->request->getParam('auth_mode', 'session');
            if ( Yii::app()->session['isComingFromHydra'] ) {
                unset(Yii::app()->session['isComingFromHydra']);
                $url = Docebo::createHydraUrl('learn', 'signin', array( 'type' => 'oauth2_response', 'error' => $e->getMessage() ) );
                $this->redirect($url);
            } else if($authentication_mode == 'oauth2') {
				$response = array('success' => false, 'message' => $e->getMessage());
				header("Location: " . Docebo::getWebappUrl(true) . '?oauth2_response=' . urlencode(CJSON::encode($response)));
			} else
				$this->redirect(Yii::app()->createAbsoluteUrl('site/index', array('error' => $e->getCode())), true, 302, true);
		}
	}

	/**
	 * Provides a mechanism to login via Rest authentication
	 *
	 * @see CoreRestAuthentication
	 * @see AuthToken
	 * @throws Exception
	 */
	public function actionLoginRest($userid, $rest_token) {

		$userid = urldecode($userid);
		$user = CoreUser::model()->find('userid=:userId OR userid=:userIdOld', array(
			':userId' => $userid,
			':userIdOld' => '/' . $userid,
		));

		if (!$user) {
			Yii::log('REST login failed because user not found', CLogger::LEVEL_WARNING);
			$this->redirect(Yii::app()->createAbsoluteUrl('site/index', array('error' => 2)));
		}

		$auth = new AuthToken($user->idst);
		$existingToken = $auth->get(false);

		if (!$existingToken) {
			Yii::log('Token not found for user', CLogger::LEVEL_WARNING);
			$this->redirect(Yii::app()->createAbsoluteUrl('site/index', array('error' => 2)));
		}

		if ($existingToken != $rest_token) {
			// The provided token doesn't match the user's one
			Yii::log('Invalid or expired token', CLogger::LEVEL_ERROR);
			$this->redirect(Yii::app()->createAbsoluteUrl('site/index', array('error' => 2)));
		}

		// Read auth mode. Possible auth modes:
		// - "session" (default): LMS login session
		// - "oauth2": generates and returns an OAuth2 session (used only by webapp)
		$authentication_mode = Yii::app()->session['sso_auth_mode'];
		if(!$authentication_mode)
			$authentication_mode = Yii::app()->request->getParam('auth_mode');

		// If we're being called by a Mobile app user agent and "auth_mode" was not specified,
		// force the mode to "oauth2"
		if(!$authentication_mode && Docebo::isMobile())
			$authentication_mode = 'oauth2';

		Yii::app()->session['sso_auth_mode'] = $authentication_mode ? $authentication_mode : 'session';

		// Search for the user in DB and authenticate him without password
		$ui = new DoceboUserIdentity($userid, null);
		if ($ui->authenticateWithNoPassword()) {
			if ($ui->checkUserAlreadyLoggedIn())
				throw new Exception(Yii::t('login', '_TWO_USERS_LOGGED_WITH_SAME_USERNAME'), 4);

			if($authentication_mode == 'oauth2') {
				unset(Yii::app()->session['sso_auth_mode']);
				$server = DoceboOauth2Server::getInstance();
				if($server) {
					$server->init();
					$token = $server->createAccessToken("mobile_app", $ui->getId(), "webapp");
					if($token)
						$response = array('success' => true, 'token' => $token);
					else
						$response = array('success' => false, 'message' => "Could not create Oauth2 token");
				} else
					$response = array('success' => false, 'message' => "Could not initialize Oauth2 server component");

				header("Location: " . Docebo::getWebappUrl(true) . '?oauth2_response=' . urlencode(CJSON::encode($response)));
				die();
			} else {
				Yii::app()->user->login($ui);

				// Login is NOT enough to say that user is Active;
				// As of Mid December'13 User is counted as active when he/she enters a course
				// UserLimit::logActiveUserLogin();
				// if the authentication goes well we need to check for a suspended installation process
				Docebo::resumeSaasInstallation();

				// Determine which URL to jump to
				$redirect_url = Yii::app()->createAbsoluteUrl('site/index', array('login' => 1));
				// *** HOOK FOR PLUGINS ***
				$loginEvent = new DEvent($this, array('redirect_url' => &$redirect_url));
				Yii::app()->event->raise('ActionSsoLogin', $loginEvent);

				// Finally, redirect!!
				$this->redirect($redirect_url);
			}
		} else {
			$ui->logFailedLoginAttempt();
			throw new Exception("Invalid username or user suspended", 2);
		}
	}

	/**
	 * Our generic HybridAuth endpoint that is the starting AND ending point
	 * for all HybridAuth requests.
	 *
	 * Meaning that when you make a $hybridauth->authenticate("VENDOR_NAME") request,
	 * HybridAuth will redirect you to this endpoint, which will redirect to the vendor website
	 * for user consent, after approving which the user is redirected back to the endpoint
	 * URL (which serves as a callback). The endpoint is then responsible for verifying
	 * if everything is OK and redirecting to the final URL (checks if the saved session
	 * data matches the one received from the callback and if the user consent is made)
	 *
	 * For sample usage, see GooglessoAppController->actionLogin().
	 * On first visit it will try to $hybridauth->authenticate("Google"), which
	 * will redirect to http://lmsURL/hybridauth/ (a mod_rewrite mask for the controller below)
	 * which is actually the abstract HybridAuth endpoint.
	 */
	public function actionHybridauthEndpoint() {
		Yii::import('common.vendors.hybridauth.Hybrid.Auth', 1);
		Yii::import('common.vendors.hybridauth.Hybrid.Endpoint', 1);
		Hybrid_Endpoint::process();
	}

	/**
	 * A "health check" URL which always loads with plugins disabled
	 * Emulates the display of an actual login page, without the actual
	 * username/password fields
	 */
	public function actionIsAlive() {

		Yii::app()->event->on('BeforeLoginFormRender', function(DEvent $event) {
			// Remove the username/password fields since this is not a "real" login page
			// It's just for health checks and emulates the login page
			return false;
		});

		$layout = Settings::get('login_layout');

		// Let apps draw the homepage
		$isHomePageRendered = false;
		Yii::app()->event->raise('BeforeLoginPageRender', new DEvent($this, array(
			'isHomePageRendered' => &$isHomePageRendered,
			'login_layout' => &$layout,
		)));

		if ($isHomePageRendered) // If already rendered, give up
			return;

		$userPages = DoceboUI::getUserPages();

		switch ($layout) {
			case 'layout1':
				$view = 'right';
				break;
			case 'layout2':
				$view = 'top';
				break;
			case 'layout3':
				$view = 'bottom';
				break;
			default:
				$view = 'top'; //let's use a default one to prevent errors
		}

		Yii::app()->event->on('loginPageBeforeBodyTagClose', function(DEvent $event) {
			echo '<!-- Your LMS is alive -->';
		});
		$this->render('login/' . $view, array(
			'userPages' => $userPages,
		));
	}

	/**
	 * Basic action to process login attempt
	 * For testing only.
	 */
	public function actionLogin() {

		// Event raised for YnY app to hook on to
		Yii::app()->event->raise('BeforeLoginPostAction', new DEvent($this, array()));

		$username = $_POST['login_userid'];
		$password = $_POST['login_pwd'];

		if (!$username || !$password) {
			if (!$username)
				unset(Yii::app()->session['login_userid']);
			else
				Yii::app()->session['login_userid'] = $username;
			// Username or password not entered
			$this->redirect(Yii::app()->createAbsoluteUrl('site/index', array('error' => 1)));
		}

		$ui = new DoceboUserIdentity($username, $password);
		if ($ui->isUserHacker()) {
			unset(Yii::app()->session['login_userid']);
			$this->redirect(Yii::app()->createAbsoluteUrl('site/index', array('error' => 3)));
		}

		if ($ui->authenticate()) {
			if ($ui->checkUserAlreadyLoggedIn())
				$this->redirect(Yii::app()->createAbsoluteUrl('site/index', array('error' => 4)));

			Yii::app()->user->login($ui);

			LearningTracksession::model()->updateAll(array('active' => 0), 'active = 1 AND idUser = ' . Yii::app()->user->id);

			// Login is NOT enough to say that user is Active;
			// As of Mid December'13 User is counted as active when he/she enters a course
			// UserLimit::logActiveUserLogin();
			// if the authentication goes well we need to check for a suspended installation process
			// we can identify this thing trough a setting
			Docebo::resumeSaasInstallation();
			$settingTimezone = CoreSettingUser::model()->findByAttributes(array(
				'id_user' =>  Yii::app()->user->id,
				'path_name' => 'timezone',
			));

			// get client timezone and set it if have't selected in lms
			if ((!$settingTimezone || !$settingTimezone->value) && Settings::get('timezone_allow_user_override', 'off') === 'on'){
				$clientTimezoneOffset = $_POST['client_timezone_offset'];
				$serverTimezones = Yii::app()->localtime->getTimezoneOffsetsSecondsArray();
				$foundedTimezone = array_search($clientTimezoneOffset, $serverTimezones);
				if($foundedTimezone){
					$userId =  Yii::app()->user->id ;

					if ($settingTimezone === null){
						$settingTimezone = new CoreSettingUser('create');
						$settingTimezone->id_user =  $userId;
						$settingTimezone->path_name = 'timezone';
					}

					$settingTimezone->value = $foundedTimezone;
					if($settingTimezone->save()){
						$userName = Yii::app()->user->getUsername();
						$user = $userName ? $userName : $userId;
						Log::_('User: '.$user.' time zone '.$foundedTimezone.' has been automatically set.');
					}
				}
			}
		} else {
			Yii::app()->session['login_userid'] = $username;

			$user = CoreUser::model()->find('userid = :userid AND valid=:valid', array(
					':userid' => Yii::app()->user->getAbsoluteUsername($username),
					':valid' => 0,
			));
			$ui->logFailedLoginAttempt();
			if ($user && Yii::app()->user->isAccountExpired($user)) {
				Yii::app()->user->setFlash('expired_account', Yii::t('standard', 'Your account has expired on {date}. Please, contact your administrator for further information.', array('{date}' => $user->expiration)));
				$this->redirect(Yii::app()->createAbsoluteUrl('site/index', array('error' => 5)), true, 302, true);
			}
			// Unable to authenticate, wrong username or password
			$this->redirect(Yii::app()->createAbsoluteUrl('site/index', array('error' => 2)), true, 302, true);
		}

		if (isset(Yii::app()->session['login_userid']))
			unset(Yii::app()->session['login_userid']);

		$this->actionHomePage();
	}

	/**
	 * Used only to redirect the user correctly after the login to the platform
	 */
	public function actionHomePage() {
		$after_login_url = Docebo::getUserAfetrLoginRedirectUrl();
		$query   = parse_url($after_login_url, PHP_URL_QUERY);
		$uriPath = parse_url($after_login_url, PHP_URL_PATH);

		// If there is no query string and path is to "/lms/", make it more canonical
		if (!$query && ($uriPath === "/lms/")) {
		    $after_login_url = rtrim($after_login_url, "/") . "/index.php?r=site/index";
		}

		// YES, again
		$query = parse_url($after_login_url, PHP_URL_QUERY);
		if ($query) {
		    $queryParams = parse_str($query);
		    if (!isset($queryParams["login"])) {
		        $after_login_url .= "&login=1";
		    }
		}
		else {
		    $after_login_url .= "?login=1";
		}

        $senderEmail = 1;
        $fromAdminEmail = Settings::get('mail_sender');
        if($fromAdminEmail != null) {
            $senderEmail = 2;
        } else {
            Yii::log("Sender email not configured in administrator panel", 'warning', __CLASS__);
        }
        Yii::app()->user->setState('senderEmail', $senderEmail);

		$this->redirect($after_login_url);
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout() {
		if (Yii::app()->user->id && !Yii::app()->user->getIsGuest()) {
			LearningTracksession::model()->updateAll(array('active' => 0), 'active = 1 AND idUser = ' . Yii::app()->user->id);
		}

		$previouslyLoggedUser = Yii::app()->user->loadUserModel();
		Yii::app()->user->logout();

		// Trigger event for plugins post-logout processing. This call may never return.
		Yii::app()->event->raise("UserLogout", new DEvent($this, array('user' => $previouslyLoggedUser)));

		if (Settings::get('user_logout_redirect') == 'on' && Settings::get('user_logout_redirect_url')) {
			$url = Settings::get('user_logout_redirect_url');
			if (strpos($url, 'http') === false)
				$url = 'http://' . $url;

			$this->redirect($url);
		}

		$this->redirect(Docebo::getRelativeUrl('.'));
	}

    /**
     * @deprecated
     */
	public function actionIndex_OLD() {
		if (
				(
				Settings::get('module_ecommerce') != 'on' || Settings::get('catalog_external') != 'on' || Docebo::isPlatformExpired()
				) &&
				(
				Yii::app()->user->getIsGuest()
				)
		) {
			$this->showLogin();
		} else {
			// If the user has some mandatory "additional fields" unfilled
			// redirect him so that he may fill them
			if (!Yii::app()->user->isGuest) {
				if (Settings::get('request_mandatory_fields_compilation') == 'on') {
					$hasToFill = CoreField::model()->getUnfilledMandatoryFields();
					if ($hasToFill && count($hasToFill)) {
						$this->redirect($this->createUrl('user/additionalFields'));
					}
				}
			}

			// Lets see if we can show MyDashboard
			$option = Yii::app()->request->getParam('opt', false);
			$forceMyCourses = Yii::app()->request->getParam('xxx', false);
			if (!Yii::app()->user->getIsGuest() && !$option && !$forceMyCourses) {
				$this->_myDashboard();
				Yii::app()->end();
			}

			if (Yii::app()->user->getIsGodadmin() && 0 == LearningCourse::model()->count()) {
				$this->breadcrumbs[Yii::t('standard', 'Welcome')] = '#';
				$this->render('superadmin_nocourses');
			} else {
				// Trigger event to allow plugins to replace My Courses page with their own external web portal
				Yii::app()->event->raise('OnBeforeMyCourses', new DEvent($this, array()));
				$this->_home();
			}
		}
	}

	public function actionIndex() {

		//Add translation category in Yii.translation.dictionary
		JsTrans::addCategories('billing');

		//start Shopify
		if (!Yii::app()->user->isGuest && Yii::app()->user->isGodAdmin && Yii::app()->session['shopify_installer_token'] && Yii::app()->session['shopify_installer_domain'])
		{
			Settings::save('shopify_subdomain', str_replace('.myshopify.com', '', Yii::app()->session['shopify_installer_domain']) );
			Settings::save('shopify_authentication_token', Yii::app()->session['shopify_installer_token']);

			if (!PluginManager::isPluginActive('ShopifyApp'))
			{
				AppsMpApiClient::apiAddApplication([
					'internal_codename' => 'Shopify',
					'installation_id' => Docebo::getErpInstallationId()
				]);
				PluginManager::activateAppByCodename('Shopify');
			}
			AppsMpApiClient::apiSetShopifyDomain(array('shopify_domain' => Yii::app()->session['shopify_installer_domain'], 'installation_id' => Docebo::getErpInstallationId()));
//			ShopifyRegistrationProxy::afterTokenReceivedActions();

			unset(Yii::app()->session['shopify_installer_token']);
			unset(Yii::app()->session['shopify_installer_domain']);

			$this->redirect(Docebo::createAdminUrl('ShopifyApp/ShopifyApp/settings'));
		}
		//end Shopify

		if (PluginSettings::get( 'external_sso_force', 'ApiApp' ) && Yii::app()->user->isGuest) {
			if(isset($_GET['staff.docebo']) || isset($_GET['staff_docebo'])){
				// Do nothing, force stop of the external SSO requested
			} else {
				$externalLoginUrl = PluginSettings::get('external_sso_url', 'ApiApp');
				if (!empty($externalLoginUrl))
					$this->redirect($externalLoginUrl);
			}
		}

		if (!Yii::app()->event->raise('onBeforeHomepage', new DEvent($this, array()))) {
			Yii::app()->end();
		}

		if ((/* Settings::get('module_ecommerce') != 'on'  || */Settings::get('catalog_external') != 'on' || Docebo::isPlatformExpired()) && Yii::app()->user->getIsGuest()) {
			$this->showLogin();
		} else {
			// If the user has some mandatory "additional fields" unfilled
			// redirect him so that he may fill them
			if(!Yii::app()->user->isGuest){
//				if(Settings::get('request_mandatory_fields_compilation')=='on'){
//					$hasToFill = CoreField::model()->getUnfilledMandatoryFields();
//                    $defaultFields = CoreUser::getUnfilledDefaultFields();
//					if(($hasToFill && count($hasToFill)) || count($defaultFields) > 0){
//						$this->redirect($this->createUrl('user/additionalFields'));
//					}
//				}
			}

			// Trigger event to allow plugins to replace My Courses page with their own external web portal
			$useDashlets = Settings::get('use_dashlets', 'on');
			$event = new DEvent($this, array());
			Yii::app()->event->raise('OnBeforeMyCourses', $event);
			if(!$event->shouldPerformAsDefault() || ($useDashlets == 'off')) {
				$this->_home();
				Yii::app()->end();
			}

			if (!Yii::app()->user->isGuest) {
				if (Yii::getPathOfAlias('jplayer') === false)
					Yii::setPathOfAlias('jplayer', Yii::app()->basePath.'/../../common/extensions/jplayer');

				$assetsPath = Yii::getPathOfAlias('jplayer.assets');
				$assetsUrl = Yii::app()->assetManager->publish($assetsPath);
				Yii::app()->getClientScript()->registerCssFile($assetsUrl . "/skin/dialog2_videoplayer/jplayer.css");
				Yii::app()->getClientScript()->registerScriptFile($assetsUrl . '/js/jquery.jplayer.min.js', CClientScript::POS_HEAD);

				$this->forward('//mydashboard');
			}
			else {
				$this->_homeAnonymous();
			}
			Yii::app()->end();
		}
	}

	/**
	 * Renders non-catalog login layout, without site header widget.
	 * Just stand-alone login with some common features
	 */
	public function showLogin() {

		$layout = Settings::get('login_layout');
		$triggerRegistration = false;
		if(isset(Yii::app()->session['registerRedirect']) && Yii::app()->session['registerRedirect']
			&& Yii::app()->user->getIsGuest() && (Settings::get('register_type', 'admin') != 'admin')){
			unset(Yii::app()->session['registerRedirect']);
			$triggerRegistration = true;
		}

		// Let apps draw the homepage
		$isHomePageRendered = false;
		Yii::app()->event->raise('BeforeLoginPageRender', new DEvent($this, array(
			'isHomePageRendered' => &$isHomePageRendered,
			'login_layout' => &$layout,
		)));

		if ($isHomePageRendered) // If already rendered, give up
			return;

		$userPages = DoceboUI::getUserPages();
		$minimalBackground = false;
		$bgType = false;
		$fallbackImg = false;
		switch ($layout) {
			case 'layout1':
				$view = 'right';
				break;
			case 'layout2':
				$view = 'top';
				break;
			case 'layout3':
				$view = 'bottom';
				break;
			case 'layout4':
				$bgType = Settings::get('minimal_login_type');
				$minimalBackground = Settings::get('minimal_login_background');
//                $error = false;
				if ($bgType == 'video' || $bgType == 'image') {
					$minimalBackground = CoreAsset::model()->findByPk($minimalBackground);
					if ($minimalBackground)
						$minimalBackground = $minimalBackground->getUrl();
					else
						$minimalBackground = false;
				}
				if ($bgType == 'video' && $fallbackImg = Settings::get('minimal_login_video_fallback_img')) {
					$fallbackImg = CoreAsset::model()->findByPk($fallbackImg);
					if ($fallbackImg)
						$fallbackImg = $fallbackImg->getUrl();
					else
						$fallbackImg = false;
				}
				$this->layout = '//layouts/login_minimal';
				$view = 'minimal';
				break;
			default:
				$view = 'top'; //let's use a default one to prevent errors
		}

		$this->render('login/' . $view, array(
			'userPages' => $userPages,
            'bgType' => $bgType,
            'minimalBackground' => $minimalBackground,
            'fallbackImg' => $fallbackImg,
			'triggerRegistration' => $triggerRegistration,
		));
	}

	/**
	 * Render the new User Dashboard
	 */
	private function _myDashboard() {
		// Using forward to preserve/use all REQUEST parameters in mydashboard module
		$this->forward('//mydashboard');
		Yii::app()->end();
	}

	/**
	 * Display the course grid
	 * @throws CHttpException
	 */
	private function _home()
	{
		Yii::app()->clientScript->registerCoreScript('bbq');
		Yii::app()->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl.'/js/jquery.cookie.js');
		Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl.'/css/docebo-star-rating.css');

		$assetsPath = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('mydashboard.assets'));

		Yii::app()->getClientScript()->registerCoreScript('jquery.ui');
		Yii::app()->getClientScript()->registerCssFile(Yii::app()->getClientScript()->getCoreScriptUrl() . '/jui/css/base/jquery-ui.css');
		Yii::app()->getClientScript()->registerScriptFile($assetsPath.'/js/mydashboard.js');
		Yii::app()->getClientScript()->registerCssFile($assetsPath."/css/mydashboard.css");

		$assetsUrl = Yii::app()->getModule( 'mycalendar' )->assetsUrl;
		Yii::app()->getClientScript()->registerCssFile( Yii::app()->theme->baseUrl . '/js/formstyler/jquery.formstyler.css' );
		Yii::app()->getClientScript()->registerCssFile( $assetsUrl . '/css/fullcalendar.css' );
		Yii::app()->getClientScript()->registerScriptFile( $assetsUrl . '/js/fullcalendar.js' );

		if(Yii::getPathOfAlias('jplayer') === false)
			Yii::setPathOfAlias('jplayer', Yii::app()->basePath . '/../../common/extensions/jplayer');

		$assetsPath = Yii::getPathOfAlias('jplayer.assets');
		$assetsUrl = Yii::app()->assetManager->publish($assetsPath);
		Yii::app()->getClientScript()->registerCssFile($assetsUrl . "/skin/dialog2_videoplayer/jplayer.css");
		Yii::app()->getClientScript()->registerScriptFile($assetsUrl . '/js/jquery.jplayer.min.js', CClientScript::POS_HEAD);

		// required for the user registration dialog
		DatePickerHelper::registerAssets();

		// Possible Label Id (from Labels -> View more... of a Label)
		$flt_label = Yii::app()->request->getParam('flt_label', '');

		// Possible Catalog Id (from Multi Catalogs -> View more of a Catalog)
		$flt_catalog = Yii::app()->request->getParam('flt_catalog', '');

		// DEFAULT is only mycourses
		$gridType = CoursesGrid::$GRIDTYPE_INTERNAL_MYCOURSES;

		// If user is logged in, select the Home Content type (grid type)
		if (PluginManager::isPluginActive('CoursecatalogApp')) {
			if (!Yii::app()->user->getIsGuest()) {

				/*
				  //check catalog visibility for the current logged user
				  $isCatalogEnabled = false;
				  $cm = LearningModule::model()->findByAttributes(array('module_name' => 'course', 'default_name' => '_CATALOGUE'));
				  if ($cm) {
				  $catalogIndex = 'mo_'.$cm->idModule;
				  $cma =  LearningMiddlearea::model()->findByPk($catalogIndex);
				  if ($cma) {
				  $isCatalogEnabled = $cma->currentCanAccessObj();
				  }
				  }
				 */
				$isCatalogEnabled = true;
				/*
				 * NOTE: at this point, we must not check the catalog user visibliity from LearningMiddlearea table, because
				 * that is intended for menu only.
				 */

				if ($isCatalogEnabled) {

					// Get App catalog/home-related option
					$appCatalogSetting = Settings::get('catalog_type', 'mycourses');

					// Allow showing catalog in some way?? (App setting 'mycourses' should be 'nocatalog', for example!!)
					if ($appCatalogSetting != 'mycourses') {

						// Check if we've got a GET option  (opt) which means "show <option> grid type"!!!!!
						$option = Yii::app()->request->getParam('opt', false);

						// If no option, then follow App setting
						if ($option == false) {
							// Show catalog in mycourses area?? (in mixed way)
							if ($appCatalogSetting == 'mixed') {
								$gridType = CoursesGrid::$GRIDTYPE_INTERNAL_MIXED;
							}
						} else {
							switch ($option) {
								case 'catalog' :
									//TODO: move courses grid into CatalogcoursesApp module when possible
									//$url = Docebo::createLmsUrl('CoursecatalogApp/CoursecatalogApp/index');
									//Yii::app()->request->redirect($url);
									//---
									$gridType = CoursesGrid::$GRIDTYPE_INTERNAL_CATALOG;
									break;
								case 'mixed' :
									$gridType = CoursesGrid::$GRIDTYPE_INTERNAL_MIXED;
									break;
								case 'mycourses' :
									$gridType = CoursesGrid::$GRIDTYPE_INTERNAL_MYCOURSES;
									break;
							}
						}
					}
				}
			} else {
				// If external catalog is Enabled and user is not logged in, show External catalog
				if (Settings::get('catalog_external') == 'on') {
					$gridType = CoursesGrid::$GRIDTYPE_EXTERNAL;
				}
			}
		}

		// show the blank slate if the user is logged in and he isn't subscribed to any courses
		if (!Yii::app()->user->getIsGuest()) {

			if (!in_array($gridType, array(CoursesGrid::$GRIDTYPE_INTERNAL_CATALOG, CoursesGrid::$GRIDTYPE_INTERNAL_MIXED))) {
				$countSubscriptions = LearningCourseuser::getCountSubscriptions(Yii::app()->user->id);
				if (0 == $countSubscriptions) {
					$this->actionNoCourses();
					Yii::app()->end();
				}
			}
		}

		if ($gridType == CoursesGrid::$GRIDTYPE_INTERNAL_CATALOG) {
			$this->breadcrumbs[] = Yii::t('standard', 'Catalog');
		} else {
			$this->breadcrumbs[] = Yii::t('menu_over', '_MYCOURSES');
		}

		if ($gridType) {

			if (Yii::app()->user->getIsGuest()) {
				// Let apps draw the homepage
				$isHomePageRendered = false;
				Yii::app()->event->raise('BeforeLoginPageRender', new DEvent($this, array(
					'isHomePageRendered' => &$isHomePageRendered
				)));

				if ($isHomePageRendered) // If already rendered, give up
					return;
			}

			$this->render('courses_content', array(
				'gridType' => $gridType,
				'flt_label' => $flt_label,
				'flt_catalog' => $flt_catalog,
			));
		} else {
			throw new CHttpException(404, 'Unknown grid type!');
		}
	}

	/**
	 * Display the course grid
	 * @throws CHttpException
	 */
	private function _homeAnonymous() {
		// required for the user registration dialog
		DatePickerHelper::registerAssets();

		// Possible Label Id (from Labels -> View more... of a Label)
		$flt_label = Yii::app()->request->getParam('flt_label', '');

		// Possible Catalog Id (from Multi Catalogs -> View more of a Catalog)
		$flt_catalog = Yii::app()->request->getParam('flt_catalog', '');

		// DEFAULT is only mycourses
		$gridType = CoursesGrid::$GRIDTYPE_INTERNAL_MYCOURSES;

		// If user is logged in, select the Home Content type (grid type)
		if (PluginManager::isPluginActive('CoursecatalogApp')) {
			// If external catalog is Enabled and user is not logged in, show External catalog
			if (Settings::get('catalog_external') == 'on') {
				$gridType = CoursesGrid::$GRIDTYPE_EXTERNAL;
			}
		}

		if ($gridType == CoursesGrid::$GRIDTYPE_INTERNAL_CATALOG) {
			$this->breadcrumbs[] = Yii::t('standard', 'Catalog');
		} else {
			$this->breadcrumbs[] = Yii::t('menu_over', '_MYCOURSES');
		}

		if ($gridType) {
			// Let apps draw the homepage
			$isHomePageRendered = false;
			Yii::app()->event->raise('BeforeLoginPageRender', new DEvent($this, array(
				'isHomePageRendered' => &$isHomePageRendered
			)));

			if ($isHomePageRendered) // If already rendered, give up
				return;

			// Load specific resources for mydashboard, mycalendar
			if (!Yii::app()->request->isAjaxRequest) {
				JsTrans::addCategories('mydashboard');
				$cs = Yii::app()->getClientScript();
				$cs->registerCssFile	($cs->getCoreScriptUrl() . '/jui/css/base/jquery-ui.css');
				$cs->registerScriptFile (Yii::app()->mydashboard->getModule()->getAssetsUrl(). '/js/mydashboard.js');
				Yii::app()->getClientScript()->registerCssFile(Yii::app()->mydashboard->getModule()->getAssetsUrl() . '/css/mydashboard.css');

				// Add docebo-star-rating.css for GOLD stars layout and comment jquery.rating.css (RED stars layout)
				//$cs->registerCssFile($cs->getCoreScriptUrl().'/rating/jquery.rating.css');
				Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl.'/css/docebo-star-rating.css');

				Yii::app()->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.cookie.js');
				Yii::app()->getClientScript()->registerCoreScript('bbq');

				//Load some specific assets needed for the calendar
				$assetsUrl = Yii::app()->getModule('mycalendar')->getAssetsUrl();
				$cs->registerCssFile(Yii::app()->theme->baseUrl . '/js/formstyler/jquery.formstyler.css');
				$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/admin.css');
				$cs->registerCssFile($assetsUrl.'/css/fullcalendar.css');
				$cs->registerCssFile($assetsUrl.'/css/docebotheme.css');
				$cs->registerScriptFile($assetsUrl.'/js/fullcalendar.js');
				$cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/clipboard.min.js');
			}

			$this->render('courses_content', array(
				'gridType' => $gridType,
				'flt_label' => $flt_label,
				'flt_catalog' => $flt_catalog,
			));
		}
		else {
			throw new CHttpException(404, 'Unknown grid type!');
		}
	}

	// show the blank slate if the user is logged in and he isn't subscribed to any courses
	public function actionNoCourses() {
		$showCatalogHint = false;
		if (PluginManager::isPluginActive('CoursecatalogApp') && 'mycourses' != Settings::get('catalog_type', 'mycourses')) {
			/*
			  //check user accessibility to catalog module
			  $cm = LearningModule::model()->findByAttributes(array('module_name' => 'course', 'default_name' => '_CATALOGUE'));
			  if ($cm) {
			  $catalogIndex = 'mo_'.$cm->idModule;
			  $cma =  LearningMiddlearea::model()->findByPk($catalogIndex);
			  if ($cma) {
			  $showCatalogHint = $cma->currentCanAccessObj();
			  }
			  }
			 */

			/*
			 * NOTE: at this point, we must not check the catalog user visibliity from LearningMiddlearea table, because
			 * that is intended for menu only.
			 */

			$showCatalogHint = true;
		}
		$this->render('user_no_courses', array(
			'showCatalogHint' => $showCatalogHint
		));
	}

	/**
	 * Get a user created page from Admin->Gear->Sign in web pages
	 * Those are external web-pages (pre-login)
	 */
	public function actionGetUserPage() {
		$id = intval(Yii::app()->request->getParam('id'));

		$criteria = new CDbCriteria();
		$criteria->condition = 'page_id = :page_id AND lang_code = :lang_code';
		$criteria->params = array(
			':page_id' => $id,
			':lang_code' => Lang::getCodeByBrowserCode(Yii::app()->getLanguage()),
		);
		$model = LearningWebpageTranslation::model()->find($criteria);

		echo "<div class='user-page-wrapper'>";
		echo "<h2 class='user-page-title'>{$model->title}</h2>";
		echo "<div class='user-page-contents'>{$model->description}</div>";
		echo "</div>";
		Yii::app()->end();
	}

	/**
	 * Return the css with that overrides the css colors
	 */
	public function actionColorschemeCss() {
		header("Content-type: text/css");
		echo CoreScheme::generateCss();
		Yii::app()->end();
	}

	/**
	 * AJAX called actions (by a Dialog2) to return text (could be HTML) of the TOS, based on language
	 */
	public function actionAxGetPrivacyTos() {
		$language = Yii::app()->request->getParam('language', Yii::app()->getLanguage());
		$tosText = Docebo::getPrivacyTos($language);
		echo '<h1>' . Yii::t('userlimit', 'Terms Of Service', $language) . '</h1><a href="#"></a>' . $tosText;
	}

	/**
	 * Display user privacy terms
	 */
	public function actionPrivacy() {
		$this->layout = 'privacy';
		$this->render('privacy_terms');
	}

	/**
	 * todo: temporary for the new buymore
	 */
	public function actionBuymore() {

		$this->renderPartial('buymore');
	}

	/**
	 * Very simple Sprite cheat sheet, to show sprite icons and rules
	 *
	 */
	public function actionSs() {
		$this->render("ss", true);
	}

	public function actionSs2() {
		$this->render("ss2", true);
	}

	/**
	 * Display modal dialog and send "contact" email from user
	 *
	 */
	public function actionAxContactSalesTrialExpired($ref = null) {

		$confirm_submit = Yii::app()->request->getParam('confirm_submit', false);
		$message = Yii::app()->request->getParam('message', false);
		$app_id = Yii::app()->request->getParam('app_id', false);

		switch ($ref) {
			case 'helpdesk-upgrade':
				$descriptionMessage = Yii::t('apps', 'Help Desk Plan upgrade request');
				break;
			case 'helpdesk-downgrade':
				$descriptionMessage = Yii::t('apps', 'Help Desk Plan downgrade request');
				break;
			case 'helpdesk-info':
				$descriptionMessage = Yii::t('apps', 'Help Desk Plan information request');
				break;
			default:
				$descriptionMessage = Yii::t('userlimit', 'Describe your e-learning project and how many users will be involved');
		}

		if ($confirm_submit) {
			$message = trim($message);

			if (!empty($message)) {

				// GOOD!
				$installation_id = Settings::getCfg('erp_installation_id');
				$data_params = array('installation_id' => $installation_id);
				$raw_api_res = ErpApiClient::apiErpGetCustomerInfo($data_params);
				$api_res = (array) CJSON::decode($raw_api_res);

				$pCompanyEmail = (isset($api_res['info']) ? $api_res['info']['email'] : '');
				$pCompanyName = (isset($api_res['info']) ? $api_res['info']['company_name'] : '');
				$pCompanySize = (isset($api_res['info']) ? $api_res['info']['company_size'] : '');
				$erpIndustries = ErpApiClient::getErpCrmIndustries();
				$pCompanyIndustry = (isset($api_res['info']) && $erpIndustries[$api_res['info']['area']]) ? $erpIndustries[$api_res['info']['area']] : '';

				$appName = '';
				if($app_id) {
					$params = array(
						'installation_id' => Docebo::getErpInstallationId(),
						'app_id' => $app_id
					);

					$response = CJSON::decode(AppsMpApiClient::apiGetAppDetails($params));
					if(isset($response['data']['internal_codename']))
						$appName = $response['data']['internal_codename'];
				}

				// Send email
				try {

					// Send email to Buyer and Seller
					$mm = new MailManager();
					$mm->setContentType('text/html');

					$mail_subject = (!$app_id) ? 'Trial extension' : ('App Activation Request ('.$appName.')');
					$mail_body = '';
					if($appName)
						$mail_body ="Requested App: ".$appName."<br>\n";

					$mail_body .= 'Installation: ' . Settings::get('url') . "<br>\n-----------------------------------------------------<br>\n";

					$mail_body.="E-Mail: " . $pCompanyEmail . "<br>\n
First Name: " . Yii::app()->user->getUserAttrib('firstname') . "<br>\n
Last Name: " . Yii::app()->user->getUserAttrib('lastname') . "<br>\n
Company or Organization: " . $pCompanyName . "<br>\n
Company Size: " . $pCompanySize . "<br>\n
Industry: " . $pCompanyIndustry . "<br>\n
Country: " . (isset($api_res['info']) ? $api_res['info']['country'] : '') . "<br>\n<br>\n<br>\n
To: Sales team<br>\n<br>\n
Message:  <br>\n------------------------------------------------------------------------<br>\n
" . nl2br($message) . "<br>\n
------------------------------------------------------------------------<br>\n
";

					$senderEmail = Yii::app()->params['support_sender_email'];
					$customerIsExtraItaly = (isset($api_res['info']) && $api_res['info']['country'] == 'italy' ? false : true);
					$salesEmail = $customerIsExtraItaly ? Yii::app()->params['support_sales_international_email'] : Yii::app()->params['support_sales_national_email'];
					$params = array(MAIL_REPLYTO => Yii::app()->user->getEmail());
					$recipients = array($salesEmail);

					$result = $mm->mail($senderEmail, $recipients, $mail_subject, $mail_body);

					// Mail sent successfully?
					if (!$result) {
						$errorMessage = Yii::t('standard', '_OPERATION_FAILURE');
					} else {
						// Add the entry as a $company_sizes_arr history in the ERP/CRM:
						$crm_contact_history_param = $data_params; // installation_id
						$crm_contact_history_param['text'] = addslashes(strip_tags($mail_body));
						$crm_contact_history_param['incl_domain_name'] = false;
						$crm_contact_history_param['h_type'] = 'customer_action';
						$crm_contact_history_param['h_param02'] = 'ticket_sales';
						ErpApiClient::apiErpAddCrmContactHistory($crm_contact_history_param);

						// Change the Incoming referrer to "site":
						$crm_company_param = $data_params; // installation_id
						$crm_company_param['referrer'] = 'site';
						$crm_company_param['refresh_incoming'] = true;
						ErpApiClient::apiErpUpdateCrmCompanyInfo($crm_company_param);
					}
				} catch (CException $e) {
					$errorMessage = $e->getMessage();
				}
			} else {
				// No empty messages please
				$errorMessage = Yii::t('standard', 'Please fill all required fields');
			}


			if (!$errorMessage) {
				// Good!
				$success = true;
			}
		}

		$html = $this->renderPartial('_contact_sales', array(
			'action' => $this->createUrl('axContactSalesTrialExpired'),
			'message' => $message,
			'descriptionMessage' => $descriptionMessage,
			'errorMessage' => $errorMessage,
			'success' => $success, // if true, dialog will show only Thank you and 'signout'
			'app_id' => $app_id
		));

		echo $html;

		Yii::app()->end();
	}

	public function actionAxUploadFile() {
		// HTTP headers for no cache etc
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		// NOTE: if there is 'extra' param passed - return it with the response
		$extraParam = Yii::app()->request->getParam('extra', '');
		$extraParam = Yii::app()->htmlpurifier->purify($extraParam);
		$for_subtitle = Yii::app()->request->getParam('for_subtitle', 0);

		$targetDir = Docebo::getUploadTmpPath();

		$cleanupTargetDir = true; // Remove old files
		$maxFileAge = 5 * 3600; // Temp file age in seconds
		// 5 minutes execution time
		@set_time_limit(5 * 60);

		// Uncomment this one to fake upload time
		//usleep(5000);
		// Get parameters
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
		$fileName = isset($_REQUEST["name"]) ? basename($_REQUEST["name"]) : '';

		// We accept also FILE Meta-TYPE  (video, doc, etc., see FileTypeValidator)
		$type = isset($_REQUEST["type"]) ? $_REQUEST["type"] : false;
		if ($type === false) {
			$type = FileTypeValidator::TYPE_ITEM;
		}

		// Run some extension checks on the file
		$fileHelper = new CFileHelper();
		$extension = strtolower($fileHelper->getExtension($fileName));

		if (!in_array($extension, FileTypeValidator::getWhiteListArray($type)))
			die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "File type not allowed"}, "id" : "id", "extra" : "' . $extraParam . '"}');

		$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

		// Remove old temp files
		if ($cleanupTargetDir) {
			if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
				while (($file = readdir($dir)) !== false) {
					$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

					// Remove temp file if it is older than the max age and is not the current file
					if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
						@unlink($tmpfilePath);
					}
				}
				closedir($dir);
			} else {
				die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory: ' . $targetDir . '."}, "id" : "id", "extra" : "' . $extraParam . '"}');
			}
		}

		// Look for the content type header
		if (isset($_SERVER["HTTP_CONTENT_TYPE"])) {
			$contentType = $_SERVER["HTTP_CONTENT_TYPE"];
		}

		if (isset($_SERVER["CONTENT_TYPE"])) {
			$contentType = $_SERVER["CONTENT_TYPE"];
		}

		// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
		if (strpos($contentType, "multipart") !== false) {
			if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
				// Open temp file
				$out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
				if ($out) {
					// Read binary input stream and append it to temp file
					$in = @fopen($_FILES['file']['tmp_name'], "rb");

					if ($in) {
						while ($buff = fread($in, 4096))
							fwrite($out, $buff);
					} else
						die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id", "extra" : "' . $extraParam . '"}');
					@fclose($in);
					@fclose($out);
					@unlink($_FILES['file']['tmp_name']);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id", "extra" : "' . $extraParam . '"}');
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id", "extra" : "' . $extraParam . '"}');
		} else {
			// Open temp file
			$out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
			if ($out) {
				// Read binary input stream and append it to temp file
				$in = @fopen("php://input", "rb");

				if ($in) {
					while ($buff = fread($in, 4096))
						fwrite($out, $buff);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id", "extra" : "' . $extraParam . '"}');

				@fclose($in);
				@fclose($out);
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id", "extra" : "' . $extraParam . '"}');
		}

		// Check if file has been uploaded
		if (!$chunks || $chunk == $chunks - 1) {
			// Strip the temp .part suffix off
			rename("{$filePath}.part", $filePath);

			// Get array of allowed mime types
			$mimeTypes = FileTypeValidator::getMimeWhiteListArray($type);
			if (!empty($mimeTypes)) {
				$fileMimeType = $fileHelper->getMimeType($filePath);
				if(!$fileMimeType || !in_array($fileMimeType, $mimeTypes)) {
					if($for_subtitle == 1 && $fileMimeType !== 'text/x-c++' && $fileMimeType !== 'text/x-c')
					{
						@unlink($filePath);
						die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Detected file type "' . $fileMimeType . '" not allowed"}, "id" : "id", "extra" : "' . $extraParam . '"}');
					}
				}
			}
		}

		die('{"jsonrpc" : "2.0", "result" : null, "id" : "id", "extra" : "' . $extraParam . '"}');
	}

	public function actionAxApproveLastTos() {
		if (!Yii::app()->params['tos_last_update']) {
			throw new CException('Last TOS update time not set in Yii params.');
		}

		if (Yii::app()->user->getIsGodadmin()) {
			Settings::save('tos_last_approval_date', Yii::app()->params['tos_last_update']);
		} else {
			throw new CException('Current user not a godadmin so we can not update the last TOS approval date');
		}
	}

	/**
	 * Help-desk lightbox and submit management
	 */
	public function actionAxHelpDesk() {

		$allowAccess = false;
		$event = new DEvent($this, array());
		Yii::app()->event->raise('ShouldDisplayHelpdeskButton', $event);
		if(!$event->shouldPerformAsDefault())
			$allowAccess = $event->return_value['show'];

		if(!Yii::app()->user->isGodAdmin && !$allowAccess)
			parent::accessDeniedCallback(false);

		Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/js/formstyler/jquery.formstyler.docebo.css');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/formstyler/jquery.formstyler.min.js');

		// retrive form data
		$attachment = false;
		$confirm_button = Yii::app()->request->getParam('confirm_button', false);
		$request = Yii::app()->request->getPost('help-desk-text');
		$target = Yii::app()->request->getPost('help-desk-target');

		$onlySales = Yii::app()->request->getParam('only_sales', false);

		if($onlySales !== false && strlen($onlySales) === 0){
			$onlySales = false;
		}

		if ($confirm_button) {
			try {

				if($onlySales !== false){
					$target = 'sales';
				}

				if (isset($_FILES['help-desk-upload'])) {
					// manage uploaded file
					$attachment = array($_FILES['help-desk-upload']['name'] => $_FILES['help-desk-upload']['tmp_name']);
				} elseif (isset($_SESSION['helpdesk-attachment']['attachment'])) {
					$attachment = $_SESSION['helpdesk-attachment']['attachment'];
				}

				if (empty($request) || empty($target)) {
					$error = Yii::t('standard', 'Please fill all required fields');
					throw new CException($error);
				}


				$version = Docebo::platformVersion();

				if (BrandingWhiteLabelForm::isHelpDeskToDocebo()) {

					// the help-desk is directed to us
					// other data needed
					$installation_id = Docebo::getErpInstallationId();

					// use erp api to retrive installation info
					$raw_api_res = ErpApiClient::apiErpGetCustomerInfo(array(
								'installation_id' => $installation_id
					));
					$customer_info = (array) CJSON::decode($raw_api_res, true);

					// compose message
					$mail_subject = '[' .
							strtoupper(Settings::get("helpdesk_level", "silver"))
							. ( PluginManager::isPluginActive('EcsApp') ? ' ECS' : ' SAAS' ) . '] '
							. Yii::t('standard', 'Contact us') . ' - ' . ucfirst($target);

					$mail_body = "Platform Version: " . $version . "\n"
							. "Installation: " . Docebo::createAbsoluteLmsUrl()
							. "\n<br/>------------------------<br/>\n"
							. "E-Mail: " . Yii::app()->user->getUserAttrib('email') . "<br />
                        First Name: " . Yii::app()->user->getUserAttrib('firstname') . "<br />
                        Last Name: " . Yii::app()->user->getUserAttrib('lastname') . "<br />
                        Company or Organization: " . (isset($customer_info['info']) ? $customer_info['info']['company_name'] : '') . "<br />
                        Country: " . (isset($customer_info['info']) ? $customer_info['info']['country'] : '') . "<br />
                        Billing data provided: " . (isset($customer_info['info']) && $customer_info['info']['has_customer'] == 1 ? 'yes' : 'no') . "<br />
                        User plan: " . ( Settings::get("max_users", "0") ? "<b>" . Settings::get("max_users", "0") . "</b>" : 'trial' ) . "<br/>
                        <br/>
                        User Ip : " . Yii::app()->request->getUserHostAddress() . "<br/>
                        User Agent : " . Yii::app()->request->getUserAgent() . "<br/>
                        <br/>
                        To: " . $target . "<br />
                        Message:  <br />\n"
							. nl2br($request);

					// save this info as a crm entry in erp
					$raw_api_res = ErpApiClient::apiErpGetExtHelpdeskManager(array(
								'installation_id' => $installation_id
					));
					$response = (array) CJSON::decode($raw_api_res, true);
					$error = '';
					$res = false;


					if (empty($response) || !$response['success']) {
						$error = (!empty($response['msg'])) ? $response['msg'] : 'Operation failed';
						throw new CException($error);
					}
					$mailer = new MailManager();
					$mailer->useAttachmentKeyForFilename();

					// from: info@docebo.com and reply-to: user's email so as the email is handled correctly by spam filters
					$sender = Yii::app()->params['support_sender_email'];

					$reply_to = trim(Yii::app()->user->getUserAttrib('email'));
					if (empty($reply_to)) {
						$error = Yii::t('standard', 'Reply-to email must not be empty');
						throw new CException($error);
					}
					$reply_to = array($reply_to);

					$emailList = $response['data'];

					$isHandledByDocebo = empty($emailList);

					if ($isHandledByDocebo) {
						// support handled by docebo

						$recipients = array();

						switch ($target) {
							case 'help':
								$helpEmail = Yii::app()->params['support_help_email'];
								if (!$helpEmail) {
									throw new CException('Docebo help email not found.');
								} else {
									$recipients = array($helpEmail);
								}
								break;
							case 'sales':
								// sales
								$customerIsExtraItaly = ($customer_info['info']['country'] == 'italy' ? false : true);
								$salesEmail = $customerIsExtraItaly ? Yii::app()->params['support_sales_international_email'] : Yii::app()->params['support_sales_national_email'];
								$recipients = array($salesEmail);
								break;
							default:
								// custom recipient; $target represents a valid email address
								$recipients = array($target);
								break;
						}

						if (!empty($recipients)) {
							// send the email
							$res = $mailer->mail($sender, $recipients, $mail_subject, $mail_body, $attachment, false, false, $reply_to);
						}
					} else {
						// support handled by reseller
						//$emailList = array('plamen@petkoff.eu');
						$res = $mailer->mail($sender, $emailList, $mail_subject, $mail_body, $attachment, false, false, $reply_to);
					}


					// Add the entry as a contact history in the ERP/CRM:
					$crm_contact_history_param = array(
						'installation_id' => $installation_id,
						'text' => Yii::app()->htmlpurifier->purify($mail_body),
						'incl_domain_name' => false,
						'h_type' => 'customer_action',
						'h_from' => 'ticket',
						'h_param02' => 'ticket_' . $target,
					);
					ErpApiClient::apiErpAddCrmContactHistory($crm_contact_history_param);

					// Change the Incoming referrer to "site":
					/*
					  $crm_company_param = array(
					  'installation_id' => $installation_id,
					  'referrer' => 'site',
					  'refresh_incoming' => true
					  );
					  ErpApiClient::apiErpUpdateCrmCompanyInfo($crm_company_param);
					 */
					$html = $this->renderPartial('_helpdesk_thankyou', array(
						'message' => Yii::t('standard', 'Thanks for contacting us, our Team will reach you by email at: {email}', array(
							'{email}' => null,
						)),
						'userEmail' => $this->getUserInformation(),
							), true, true);
				} else {
					// the help-desk is managed by the white labeler
					// compose message
					$mail_subject = Yii::t('standard', 'Contact us') . ' - ' . ucfirst($target);

					//$mail_body = 'Installation: ' . Docebo::createAbsoluteLmsUrl()
					$mail_body = "Platform Version: " . $version . "\n"
							. "Installation: " . Docebo::createAbsoluteLmsUrl()
							. "\n<br/>------------------------<br />\n"
							. "LMS Version: " . $version . "<br />\n"
							. "E-Mail: " . Yii::app()->user->getUserAttrib('email') . "<br />
                        First Name: " . Yii::app()->user->getUserAttrib('firstname') . "<br />
                        Last Name: " . Yii::app()->user->getUserAttrib('lastname') . "<br />
                        <br/>
                        User Ip : " . Yii::app()->request->getUserHostAddress() . "<br/>
                        User Agent : " . Yii::app()->request->getUserAgent() . "<br/>
                        <br/>
                        To: " . $target . "<br />
                        Message:  <br />\n"
							. nl2br($request);

					$mailer = new MailManager();
					$mailer->useAttachmentKeyForFilename();

					// the white label email
					$sender = BrandingWhiteLabelForm::getHelpDeskEmail();

					$user_email = trim(Yii::app()->user->getUserAttrib('email'));
					if (empty($user_email)) {
						$error = Yii::t('standard', 'Reply-to email must not be empty');
						throw new CException($error);
					}

					// send the mail to the white label email
					$mailer->mail(array($user_email => Yii::app()->user->getDisplayName()), $sender, $mail_subject, $mail_body, $attachment, false, false, array($user_email));

					$html = $this->renderPartial('_helpdesk_thankyou', array(
						'message' => Yii::t('standard', 'Thanks for contacting us, our Team will reach you by email at: {email}', array(
							'{email}' => null,)),
						'userEmail' => $this->getUserInformation(),
							), true, true);
				}

				if (isset($_SESSION['helpdesk-attachment'])) {
					if (isset($_SESSION['helpdesk-attachment']['tmp']) && file_exists($_SESSION['helpdesk-attachment']['tmp']))
						unlink($_SESSION['helpdesk-attachment']['tmp']);
					unset($_SESSION['helpdesk-attachment']);
				}

				echo $html;
				Yii::app()->end();
			} catch (CException $ex) {
				//$error = $ex->getMessage();
				//Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
				Yii::app()->user->setFlash('error', $ex->getMessage());
			}
		}

		$it_help_link = array(
			'index' => 'http://www.docebo.com/it/elearning-help-supporto/',
			'getting-started' => 'http://www.docebo.com/it/elearning-help-supporto/primi-passi-con-docebo/',
			'user' => 'http://www.docebo.com/it/elearning-help-supporto/gestione-utenti-elearning/',
			'courses' => 'http://www.docebo.com/it/elearning-help-supporto/gestione-corsi/',
			'report' => 'http://www.docebo.com/it/elearning-help-supporto/reportistica/',
			'settings' => 'http://www.docebo.com/it/elearning-help-supporto/impostazioni/',
			'apps' => 'http://www.docebo.com/it/lms-elearning-moduli-e-integrazioni/',
			'integration' => 'http://www.docebo.com/it/elearning-help-supporto/integrazioni-lms/',
			'payment' => 'http://www.docebo.com/it/elearning-help-supporto/politiche-di-pagamento/',
		);

		$en_help_link = array(
			'index' => 'http://www.docebo.com/elearning-knowledge-base-support/',
			'getting-started' => 'http://www.docebo.com/elearning-knowledge-base-support/getting-started-with-docebo/',
			'user' => 'http://www.docebo.com/elearning-knowledge-base-support/lms-users-management/',
			'courses' => 'http://www.docebo.com/elearning-knowledge-base-support/course-management/',
			'report' => 'http://www.docebo.com/elearning-knowledge-base-support/manage-report/',
			'settings' => 'http://www.docebo.com/elearning-knowledge-base-support/lms-settings/',
			'apps' => 'http://www.docebo.com/lms-elearning-modules-integrations/',
			'integration' => 'http://www.docebo.com/elearning-knowledge-base-support/integrations-lms/',
			'payment' => 'http://www.docebo.com/elearning-knowledge-base-support/payment-billing-policies/',
		);

		$requestLabel = Yii::t('userlimit', 'Contact Docebo team');
		$helpdeskRecipients = array();
		$showHelpAndManuals = true;

		Yii::app()->event->raise('BeforeHelpdeskRender', new DEvent($this, array(
			'requestLabel' => &$requestLabel,
			'helpdeskRecipients' => &$helpdeskRecipients,
			'showHelpAndManuals' => &$showHelpAndManuals
		)));

		// Render the help desk form, here i can
		$html = $this->renderPartial('_helpdesk', array(
			'is_to_docebo' => BrandingWhiteLabelForm::isHelpDeskToDocebo(),
			'help' => ( Yii::app()->getLanguage() == 'it' ? $it_help_link : $en_help_link ),
			'help_desk_text' => $request,
			'help_desk_target' => $target,
			'requestLabel' => $requestLabel,
			'helpdeskRecipients' => $helpdeskRecipients,
			'showHelpAndManuals' => $showHelpAndManuals,
			'onlySales' => $onlySales
				), true, true);
		echo $html;
	}

	public function actionAxHelpDeskFile() {
		if (isset($_FILES['help-desk-upload'])) {
			// manage uploaded file
			$name = $_FILES['help-desk-upload']['name'];
			$dir = realpath(dirname(__FILE__) . '/../../../assets');
			$tmp = tempnam($dir, 'helpdesk');
			move_uploaded_file($_FILES['help-desk-upload']['tmp_name'], $tmp);

			$attachment = array($name => $tmp);
			$_SESSION['helpdesk-attachment'] = array(
				'tmp' => $tmp,
				'attachment' => $attachment,
			);
		} elseif (isset($_POST['remove_file']) && isset($_SESSION['helpdesk-attachment'])) {
			if (isset($_SESSION['helpdesk-attachment']['tmp']) && file_exists($_SESSION['helpdesk-attachment']['tmp'])) {
				unlink($_SESSION['helpdesk-attachment']['tmp']);
			}
			unset($_SESSION['helpdesk-attachment']);
		}

		// disable YiiDebugToolbar
		foreach (Yii::app()->log->routes as $route) {
			if ($route instanceof CWebLogRoute || $route instanceof CFileLogRoute || $route instanceof YiiDebugToolbarRoute) {
				$route->enabled = false;
			}
		}

		// Render the help desk form, here i can
		$html = $this->renderPartial('_helpdesk_file', array(
				), true, true);
		echo $html;
	}

	/**
	 * Yii based version of the Moxiemanager api.php file
	 */
	public function actionMoxiemanagerApi() {

		// Purify folder name in case of folder creation
		if(isset($_POST['json']) && ($params = json_decode($_POST['json'], true))) {
			if($params['method'] == 'createDirectory') {
				$params['params']['path'] = strip_tags($params['params']['path']);
				$_POST['json'] = json_encode($params);
			}
		}

		// Store in the session that the passed cookie is valid only for the current domain
		$domain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME'];
		if(!isset(Yii::app()->session['moxiemanager_domain']))
			Yii::app()->session['moxiemanager_domain'] = $domain;

		// Validate the cookie for this domain
		if(Yii::app()->session['moxiemanager_domain'] != $domain) {
			Yii::log("Attempt to use cookie for domain '".Yii::app()->session['moxiemanager_domain']."' with another domain '".$domain."'.", CLogger::LEVEL_ERROR);
			return false;
		}

		//--- Pre-process input ---
		// Since S3 storage may have problems with non-word characters, we pre-process input file name when uploading images.
		// The file name is "sanitized" in order to have the most issue-free keys for S3 storage.
		if (isset($_GET['action']) && $_GET['action'] == 'upload') {
			if (isset($_GET['name']) && $_GET['name'] != '') {
				//lowerize the file name ... maybe not necessary, but it's still a good practice
				$fileName = strtolower($_GET['name']);
				//remove accented letters and substitute them with non-accented ones, in order to have a processed name most similar to the original one
				$accented = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'Ά', 'ά', 'Έ', 'έ', 'Ό', 'ό', 'Ώ', 'ώ', 'Ί', 'ί', 'ϊ', 'ΐ', 'Ύ', 'ύ', 'ϋ', 'ΰ', 'Ή', 'ή');
				$nonAccented = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o', 'Α', 'α', 'Ε', 'ε', 'Ο', 'ο', 'Ω', 'ω', 'Ι', 'ι', 'ι', 'ι', 'Υ', 'υ', 'υ', 'υ', 'Η', 'η');
				$fileName = str_replace($accented, $nonAccented, $fileName);
				//remove extension (if any) from file name
				$parts = explode('.', $fileName);
				if (count($parts) > 1) {
					$extension = array_pop($parts);
				} else {
					$extension = false;
				}
				//process the file name without its extension, remove all non-word characters and substitute them with underscores
				$fileName = implode('_', $parts);
				$fileName = preg_replace('/\W/', '_', $fileName);
				//re-join extension to file name
				if ($extension) {
					$fileName .= '.' . strtolower($extension);
				}
				//manipulate input with new processed file name
				// NOTE: this is not the best practice and should be avoided. However we are going to use third party code which
				// directly accesses $_GET and $_POST variables and we have no other ways to "sanitize" file names without changing
				// third party libraries (which must be avoided too).
				$_GET['name'] = $fileName;
			}
		}
		//--- ---
		// Static method MOXMAN::getConfig() can't access this
		// if not global, and in turn all MoxieManager PHP plugins won't work.
		global $moxieManagerConfig;

		$userId = Yii::app()->user->getId();

		// When false, will redirect to the login page
		// (set in 'authenticator.login_page' config param)
		// when the moxiemanager button is clicked
		$_SESSION['moxieUserLoggedIn'] = !Yii::app()->user->isGuest;

		// Do NOT forget to set userfiles collection to be an S3 based!
		$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_USERFILES);

		if ($storage->type != CFileStorage::TYPE_S3)
			throw new CException('Invalid collection storage type for userfiles');

		// S3 specific calls! But we are sure 'userfiles' is S3, arent we  ????
		$bucketName = $storage->getS3BucketName();
		$bucketSharedKey = $storage->getS3Key();
		$bucketSecretKey = $storage->getS3Secret();

		$moxieManagerConfig = array();

		$moxieManagerConfig['general.language'] = Yii::app()->getLanguage();

		// Filesystem
		// You can include multiple "browse" locations by separating them with ";"
		// e.g. $moxieManagerConfig['filesystem.rootpath'] = 'Local=../../../files;S3=s3://bucketname/subfolder';
		// The user can browse/manage only his uploaded S3 files, so we allow him
		// to browse only his custom "subfolder" that includes his user ID (see $path)
		// Little security wont't harm. The result is a message from Moxie saying "invalid blah blah"... Which is good anyway...
		if (!$userId) {
			return false;
		}


		$moxieManagerConfig['filesystem.rootpath'] = 'S3=s3://' . $bucketName . $storage->path() . '/' . $userId;

		// Authentication
		$moxieManagerConfig['authenticator.login_page'] = Docebo::createAbsoluteLmsUrl('site/index', array('error' => 'moxiemanager_login'));

		// Local filesystem
		$moxieManagerConfig['filesystem.local.cache'] = false;

		// Log
		$moxieManagerConfig['log.path'] = str_replace('/', '\\', Docebo::filesPathAbs());

		// Cache
		$moxieManagerConfig['cache.connection'] = "sqlite:" . str_replace('/', '\\', Docebo::filesPathAbs() . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'moxiemanager-cache.s3db');

		// Storage
		$moxieManagerConfig['storage.path'] = str_replace('/', '\\', Docebo::filesPathAbs() . DIRECTORY_SEPARATOR . 'tmp');

		// Amazon S3 plugin
		$moxieManagerConfig['amazons3.buckets'] = array(
			$bucketName => array(
				'publickey' => $bucketSharedKey,
				'secretkey' => $bucketSecretKey,
				// Non-US Standard Region buckets MUST be accessed using a virual host style endpoint:
				// http://yourbucket.s3.amazonaws.com/yourobject
				// To use the path-style request, the bucket must be in the US Standard Region:
				// http://s3.amazonaws.com/yourbucket/yourobject
				'subDomainBasedUrl' => true,
				/**
				 * Callback function to be used to parse the file and get a correct URL
				 * Currently we use it to generate a CDN link for a S3 file.
				 *
				 * In the current implementation only MoxieManager's AmazonS3 plugin uses this setting.
				 * @see MOXMAN_AmazonS3_File::getUrl()
				 */
				'urlParser' => function($fileName, $pathToFile) use ($userId, $storage) {
					// In case the file is in a subfolder, we need to extract it
					$moxiSubfolder = ltrim(substr($pathToFile, mb_strlen($storage->path() . '/' . $userId)), "\/\\");
					$url = $storage->fileUrl($fileName, $userId . ($moxiSubfolder ? '/' . $moxiSubfolder : ''));
					return $url;
				},
				// Since we use per-user subfolders and the cache is filesystem based
				// it's not useful in our case. It allows one user to view/manage the
				// files of another user (if they are cached).
				'cache' => false,
			)
		);

		if (Yii::app()->params['moxiemanager'])
			$moxieManagerConfig = array_merge(Yii::app()->params['moxiemanager'], $moxieManagerConfig);
		else
			throw new CException('MoxieManager config param missing');

		// Let plugins override specific moxiemanager configuration params
		$event = new DEvent($this, array('current_config' => $moxieManagerConfig));
		Yii::app()->event->raise('OverrideMoxieManagerConfig', $event);
		if (!empty($event->params['config_changes']) && is_array($event->params['config_changes']))
			$moxieManagerConfig = array_merge($moxieManagerConfig, $event->params['config_changes']);

		// Custom autoloaders conflict with Yii's ones (only in YII_DEBUG mode, but still...)
		spl_autoload_unregister(array('YiiBase', 'autoload'));
		require_once(Yii::getPathOfAlias('common') . '/vendors/moxiemanager/classes/MOXMAN.php');
		spl_autoload_register(array('YiiBase', 'autoload'));

		define("MOXMAN_API_FILE", __FILE__);
		$context = MOXMAN_Http_Context::getCurrent();
		$pluginManager = MOXMAN::getPluginManager();
		foreach ($pluginManager->getAll() as $plugin) {
			if ($plugin instanceof MOXMAN_Http_IHandler) {
				$plugin->processRequest($context);
			}
		}
	}

	public function actionPlanExpirationCretditCard() {
		$expire_date = Settings::get('expiration_date');
		if (DateTime::createFromFormat('Y-m-d H:i:s', $expire_date) || DateTime::createFromFormat('Y-m-d', $expire_date)) {
			$expire_date = Yii::app()->localtime->convertDateString(date('Y-m-d H:i:s', strtotime($expire_date)), 'Y-m-d H:i:s', 'd-m-Y');
			$days = Yii::app()->localtime->countDayDiff(date('Y-m-d'), $expire_date);
		} else {
			$expire_date = '';
			$days = 0;
		}

		if (isset($_POST['submit'])) {
			if (!isset($_POST['remind_me'])) {
				Settings::save('expiring_notification_show', 'no');
			}
			$_SESSION['credit_card_expiration_remind_on_next_login'] = true;
			echo '<a class="auto-close"></a>';
			die;
		}
		$this->renderPartial('dialog/_planExpirationCreditCard', array(
			'days' => $days,
			'expire_date' => $expire_date,
		));
	}

	public function actionPlanExpirationContact() {
		$users = Settings::get('max_users');
		$confirm_button = Yii::app()->request->getParam('confirm_button', false);
		$message = Yii::app()->request->getPost('plan-expiration-message');

		if ($confirm_button) {
			if (empty($message)) {
				Yii::app()->user->setFlash('error-message', Yii::t('standard', 'Please fill all required fields'));
			} else {
				$installation_id = Docebo::getErpInstallationId();

				// use erp api to retrive installation info
				$raw_api_res = ErpApiClient::apiErpGetCustomerInfo(array(
							'installation_id' => $installation_id
				));
				$customer_info = (array) CJSON::decode($raw_api_res, true);
				$customerIsExtraItaly = ($customer_info['info']['country'] == 'italy' ? false : true);
				$salesEmail = $customerIsExtraItaly ? Yii::app()->params['support_sales_international_email'] : Yii::app()->params['support_sales_national_email'];
				$to_arr = array($salesEmail);
				$from_arr = array(Yii::app()->user->getUserAttrib('email'));
				// compose message
				$mail_subject = Yii::t('expiration', 'Information about my annual {users} users plan expiration', array('{users}' => $users));

				$mail_body = "Platform Version: " . Docebo::platformVersion() . "\n"
						. "Installation: " . Docebo::createAbsoluteLmsUrl()
						. "\n<br/>------------------------<br/>\n"
						. "E-Mail: " . Yii::app()->user->getUserAttrib('email') . "<br />
					First Name: " . Yii::app()->user->getUserAttrib('firstname') . "<br />
					Last Name: " . Yii::app()->user->getUserAttrib('lastname') . "<br />
					Company or Organization: " . (isset($customer_info['info']) ? $customer_info['info']['company_name'] : '') . "<br />
					Country: " . (isset($customer_info['info']) ? $customer_info['info']['country'] : '') . "<br />
					Billing data provided: " . (isset($customer_info['info']) && $customer_info['info']['has_customer'] == 1 ? 'yes' : 'no') . "<br />
					User plan: " . ( Settings::get("max_users", "0") ? "<b>" . Settings::get("max_users", "0") . "</b>" : 'trial' ) . "<br/>
					<br/>
					User Ip : " . Yii::app()->request->getUserHostAddress() . "<br/>
					User Agent : " . Yii::app()->request->getUserAgent() . "<br/>
					<br/>
					Message:  <br />\n"
						. nl2br($message);

				$mailer = new MailManager();
				$mailer->mail($from_arr, $to_arr, $mail_subject, $mail_body);

				// Add the entry as a contact history in the ERP/CRM:
				$crm_contact_history_param = array(
					'installation_id' => $installation_id,
					'text' => Yii::app()->htmlpurifier->purify($mail_body),
					'incl_domain_name' => false,
					'h_type' => 'customer_action',
					'h_param02' => 'plan_expiration',
				);
				ErpApiClient::apiErpAddCrmContactHistory($crm_contact_history_param);

				echo '<a class="auto-close"></a>';
				die;
			}
		}
		$this->renderPartial('dialog/_planExpirationContact', array(
			'users' => $users,
		));
	}

	public function actionPlanExpirationWireTransfer() {
		$users = Settings::get('max_users');
		$expire_date = Settings::get('expiration_date');
		if (DateTime::createFromFormat('Y-m-d H:i:s', $expire_date) || DateTime::createFromFormat('Y-m-d', $expire_date)) {
			$expire_date = Yii::app()->localtime->convertDateString(date('Y-m-d H:i:s', strtotime($expire_date)), 'Y-m-d H:i:s', 'd-m-Y');
			$days = Yii::app()->localtime->countDayDiff(date('Y-m-d'), $expire_date);
		} else {
			$expire_date = '';
			$days = 0;
		}

		$this->renderPartial('dialog/_planExpirationWireTransfer', array(
			'days' => $days,
			'expire_date' => $expire_date,
			'users' => $users,
		));
	}

	public function actionRenewSubscription() {
		$expire_date = Settings::get('expiration_date');
		if (DateTime::createFromFormat('Y-m-d H:i:s', $expire_date) || DateTime::createFromFormat('Y-m-d', $expire_date)) {
			$expire_date = Yii::app()->localtime->convertDateString(date('Y-m-d H:i:s', strtotime($expire_date)), 'Y-m-d H:i:s', 'd-m-Y');
		} else {
			$expire_date = '';
		}

		$success = false;
		$users = Settings::get('max_users');

		$confirm_button = Yii::app()->request->getParam('confirm_button', false);
		$agreement = Yii::app()->request->getPost('agree', false);

		if ($confirm_button && $agreement) {
			ErpApiClient::wireRenewOrder();
			ErpApiClient::setBackupSetting('expiring_notif_answer', 'yes');
			Settings::save('expiring_notification', '');
			Settings::save('expiring_notification_show', '');
			$success = true;
		}

		$this->renderPartial('dialog/_renewSubscription', array(
			'expire_date' => $expire_date,
			'success' => $success,
			'users' => $users
		));
	}

	/**
	 * Get user information
	 *
	 * @return string
	 */
	protected static function getUserInformation() {
		if (Yii::app()->user->getUserAttrib('email'))
			$email = Yii::app()->user->getUserAttrib('email');
		elseif (Yii::app()->user->getUserAttrib('firstname') || Yii::app()->user->getUserAttrib('lastname')) {
			$names = array();
			if (Yii::app()->user->getUserAttrib('firstname'))
				$names[] = Yii::app()->user->getUserAttrib('firstname');
			if (Yii::app()->user->getUserAttrib('lastname'))
				$names[] = Yii::app()->user->getUserAttrib('lastname');
			$email = join(' ', $names);
		} else
			$email = Yii::app()->user->getRelativeUsername(Yii::app()->user->getUserAttrib('userid'));

		return $email;
	}

	/**
	 * Used to forward ERP resellers here before the login in order to set him a special cookie
	 * Later during buy process, the user is identified as reseller and the billing part in ERP is managed by different way
	 */
	public function actionSetResellerCookie()
	{
		if(isset($_GET['res'])) {
			Yii::app()->request->cookies['reseller_cookie'] = new CHttpCookie('reseller_cookie', $_GET['res']);
		}
		switch ($_GET['url']) {
			case 'apps' :
				$this->redirect(Docebo::createLmsUrl('app/index'));
				break;
			case 'plan' :
				$this->redirect(Docebo::createLmsUrl('billing/default/plan'));
				break;
			case 'courses' :
				$this->redirect(Docebo::createLmsUrl('mainmenu/marketplace'));
				break;
			default :
				$this->redirect(Docebo::createLmsUrl("site/index"), true);
		}
	}

	/**
	 * When someone give us permission to Shopify account in the installer
	 * and he already have LMS, we are setting some special session variables and he is forwarded to the login
	 * after login, if the user is super admin we are connecting his Shopify account to his LMS
	 */
	public function actionSetShopifySession()
	{
		if(isset($_GET['shopify_data'])) {
			$shopifyData = base64_decode($_GET['shopify_data']);
			$shopifyData = explode(',', $shopifyData);
			$token = isset($shopifyData[0]) ? $shopifyData[0] : '';
			$domain = isset($shopifyData[1]) ? $shopifyData[1] : '';
			if ($token && $domain)
			{
				Yii::app()->session['shopify_installer_token'] = $token;
				Yii::app()->session['shopify_installer_domain'] = $domain;
			}
		}
		$this->redirect(Docebo::createLmsUrl("site/index"), true);
	}

	public function actionShowMaintenancePopup() {
		$message = Yii::app()->params['maintenance_mode']['message'];
		$dateFrom = Yii::app()->params['maintenance_mode']['from'];

		$this->renderPartial('_maintenance', array(
			'message' => $message,
			'dateFrom' => date('D, F jS Y g:iA', strtotime($dateFrom))
		));
	}

	public function actionMaintenance() {
		$this->render('maintenance', array(
			'message' => Yii::app()->params['maintenance_mode']['message']
		));
	}

	public function actionAutoLogin($userId, $authToken) {

		$userId = intval($userId);
		$user = CoreUser::model()->find('idst=:userId', array(
			':userId' => $userId,
		));

		if (!$user) {
			Yii::log('Auto login failed because user not found', CLogger::LEVEL_WARNING);
			$this->redirect(Yii::app()->createAbsoluteUrl('site/index', array('error' => 2)));
		}

		$auth = new AuthToken($user->idst);
		$existingToken = $auth->get(false);

		if (!$existingToken) {
			Yii::log('Token not found for user', CLogger::LEVEL_WARNING);
			$this->redirect(Yii::app()->createAbsoluteUrl('site/index', array('error' => 2)));
		}

		if ($existingToken != $authToken) {
			// The provided token doesn't match the user's one
			Yii::log('Invalid or expired token', CLogger::LEVEL_ERROR);
			$this->redirect(Yii::app()->createAbsoluteUrl('site/index', array('error' => 2)));
		}

		// Search for the user in DB and authenticate him without password
		$ui = new DoceboUserIdentity($user->userid, null);
		if ($ui->authenticateWithNoPassword()) {
			Yii::app()->user->login($ui);
			$redirect_url = Yii::app()->createAbsoluteUrl('site/index', array('login' => 1));
			$loginEvent = new DEvent($this, array('redirect_url' => &$redirect_url));
			Yii::app()->event->raise('ActionSsoLogin', $loginEvent);
			$this->redirect($redirect_url);
		} else {
			$ui->logFailedLoginAttempt();
			throw new Exception("Invalid username or user suspended", 2);
		}
	}

	function actionAxSetGotIt() {

		$array_allowed_got_it = array(
			'contributeMobileApp' => 'contribute_mobile_app_gotit',
			'contributeChannelsPrNotice' => 'channels_pr_notice_gotit',
			'requests' => 'guru_dashboard_satisfy_gotit',
			'assets' => 'guru_dashboard_assets_gotit',
			'questions' => 'guru_dashboard_questions_gotit',
			'gotItSomeFeatures' => 'gotItSomeFeatures'
		);

		$method = Yii::app()->request->getParam('method', false);
		$coreUserSettings = new CoreSettingUser();
		$coreUserSettings->id_user = Yii::app()->user->idst;
		$coreUserSettings->value = 1;

		if ($method && isset($array_allowed_got_it[$method]) && !empty($array_allowed_got_it[$method])) {
			if (!CoreSettingUser::model()->findByAttributes(array('path_name' => $array_allowed_got_it[$method], 'id_user' => Yii::app()->user->idst))) {
				$coreUserSettings->path_name = $array_allowed_got_it[$method];
				$coreUserSettings->save();
			}
			$this->sendJSON(array('success' => true));
			Yii::app()->end();
		} else if ($method && !isset($array_allowed_got_it[$method])) {
			if (!CoreSettingUser::model()->findByAttributes(array('path_name' => $method, 'id_user' => Yii::app()->user->idst))) {
				$coreUserSettings->path_name = $method;
				$coreUserSettings->save();
			}
			$this->sendJSON(array('success' => true));
			Yii::app()->end();
		}
		$this->sendJSON(array('success' => false));
		Yii::app()->end();
	}

	public function actionEmbedIframe() {
		$iframeId = Yii::app()->request->getParam('iframeId', false);
		$iframeUrl = '';
		if($iframeId) {
			$model = CoreMenuItem::model()->findByAttributes(array('id' => $iframeId));
			$url = $model->url;
			$clientId = $model->oauth_client;
			$salt = $model->salt_secret;
			$iframeUrl = $model->useAccredIframe ? $this->getIframeUrl($url, $clientId, $salt) : $url;
		}

		$this->layout = 'embed-iframe';
		$this->render('embed_iframe', array(
			'model' => $model,
			'iframeUrl' => $iframeUrl,
		));
	}

	private function getIframeUrl($url, $clientId, $salt) {

	    // Parse current iframe url
	    $url_parts = parse_url($url);
	    parse_str($url_parts['query'], $params);
	     
		// Build the params array
	    $paramsToHash = array(
			'user_id' => urlencode(Yii::app()->user->getId()),
			'username' => urlencode(Yii::app()->user->getUsername()),
			'auth_code' => $this->getOAuthAuthorizationCode($clientId),
		);
	    
	    $params = array_merge($params, $paramsToHash);
	    
		// Calculate the sha1 hash of the previous params + the secret
	    $paramsToHash = array_merge($paramsToHash, array($salt));
		$params['hash'] = hash("sha256", implode(",", $paramsToHash));

		// this will url_encode all values
		$url_parts['query'] = http_build_query($params);
		
		$fragment = !empty($url_parts["fragment"]) ? "#" . $url_parts["fragment"] : "";
		
		// Return url
		$url = $url_parts['scheme'] . '://' . $url_parts['host'] . $url_parts['path'] . '?' . $url_parts['query'] . $fragment;
		
		return $url;

	}

	private function getOAuthAuthorizationCode($clientId)
	{
		$authCode = "";
		$server = DoceboOauth2Server::getInstance();
		if ($server) {
			$server->init();
			$authCode = $server->generateAuthorizationCode(Yii::app()->user->id, $clientId, null, 'api', 30);
		}
		return $authCode;
	}

}
