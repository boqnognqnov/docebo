<?php

include '../common/vendors/hybridauth/Hybrid/thirdparty/LinkedIn/LinkedIn.php';

class SocialLoginController extends Controller {

	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl',
		);
	}

	public function beforeAction($action) {

        $isComingFromHydra = isset($_GET['sso_auth_mode']) && $_GET['sso_auth_mode']  == 'oauth2' && isset($_GET['sso_target']) && $_GET['sso_target'] == 'hydra';
        Yii::app()->session['isComingFromHydra'] = $isComingFromHydra || Yii::app()->legacyWrapper->hydraFrontendEnabled();

        return parent::beforeAction($action);
    }


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		return array(

			array('allow',
				'users' => array('*'),
			),

			array('allow',
				'users' => array('@'),
			),

			array('deny',
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
			),


		);
	}

	public function actionFbLogin(){

		$fbObject = Social::getFacebookObject();
		if(!$fbObject->getUser()) {
			$this->redirect($fbObject->getLoginUrl(array( 'redirect_uri' => Docebo::createAbsoluteLmsUrl('socialLogin/fbLogin'), 'scope' => 'email' )));
		}

		$userData = $fbObject->api('/me?fields=id,name,email');
		if(isset($userData['email'])) {
			$ui = new DoceboUserIdentity($userData['email'], 'dummy');
			if ($ui->authenticateWithEmail()) {
				if(Yii::app()->user->login($ui)){
					// Check if the User is Coming from the New Hydra Frontend
                    if( Yii::app()->session['isComingFromHydra'] ) {
                        /** Since he is already Authenticated, Create Token for him and Return him back to Hydra Frontend */
                        $server = DoceboOauth2Server::getInstance();
                        $server->init();
                        $token = $server->createAccessToken("hydra_frontend", $ui->getId(), "api");
                        $this->redirectToHydra( array_merge(array("type" => "oauth2_response"),$token) );
                    }

					$this->redirect(Docebo::getUserAfetrLoginRedirectUrl());
				}
			}

			// The user did not Manage to login, Check from where he is coming from and redirect him accordingly
            if( Yii::app()->session['isComingFromHydra'] ) {
                $this->redirectToHydra( array( 'error' => Yii::t('login', '_NOACCESS')) );
            }

			$this->redirect(Docebo::createLmsUrl('site/index', array('error'=>2)));
		}

		// The user did not Manage to login, Check from where he is coming from and redirect him accordingly
        if( Yii::app()->session['isComingFromHydra'] ) {
            $this->redirectToHydra( array( 'error' => Yii::t('login', '_NOACCESS')) );
        }
	}

	public function actionLinkedinLogin(){

		try {
			// include the LinkedIn class
			Yii::import('addons.simple-linkedinphp.LinkedIn');
			// config variables
			$API_CONFIG = array(
				'appKey'    => Settings::get('social_linkedin_access'),
				'appSecret' => Settings::get('social_linkedin_secret')
			);

			// start the session
			if(!session_start()) {
				throw new LinkedInException('This script requires session support, which appears to be disabled according to session_start().');
			}

			// set index
			$_REQUEST[LinkedIn::_GET_TYPE] = isset($_REQUEST[LinkedIn::_GET_TYPE]) ? $_REQUEST[LinkedIn::_GET_TYPE] : '';
			switch($_REQUEST[LinkedIn::_GET_TYPE]) {
				case 'initiate':
                    // Check if there are any problems with the authentication
					if (isset($_REQUEST['oauth_problem']) && $_REQUEST['oauth_problem'] == 'user_refused') {
                        // The user did not Manage to login, Check from where he is coming from and redirect him accordingly
                        if( Yii::app()->session['isComingFromHydra'] ) {
                            $this->redirectToHydra( array( 'error' => Yii::t('login', '_NOACCESS')) );
                        }

						$this->redirect(Docebo::createLmsUrl('site/index'));
					}
					/**
					 * Handle user initiated LinkedIn connection, create the LinkedIn object.
					 */
					// set the callback url
					$API_CONFIG['callbackUrl'] = Docebo::createAbsoluteLmsUrl('socialLogin/linkedinLogin', array(
						'lType'=>'initiate',
						'lResponse'=>1
					));
					$OBJ_linkedin = new LinkedIn($API_CONFIG);

					// check for response from LinkedIn
					$_GET[LinkedIn::_GET_RESPONSE] = (isset($_GET[LinkedIn::_GET_RESPONSE])) ? $_GET[LinkedIn::_GET_RESPONSE] : '';
					if(!$_GET[LinkedIn::_GET_RESPONSE]) {
						// LinkedIn hasn't sent us a response, the user is initiating the connection

						// send a request for a LinkedIn access token
						$response = $OBJ_linkedin->retrieveTokenRequest();
						if($response['success'] === TRUE) {
							// store the request token
							$_SESSION['oauth']['linkedin']['request'] = $response['linkedin'];

							// redirect the user to the LinkedIn authentication/authorisation page to initiate validation.
							$this->redirect(LinkedIn::_URL_AUTH . $response['linkedin']['oauth_token']);
						} else {

                            // The user did not Manage to login, Check from where he is coming from and redirect him accordingly
                            if( Yii::app()->session['isComingFromHydra'] ) {
                                $this->redirectToHydra( array( 'error' => Yii::t('login', '_NOACCESS')) );
                            }

                            $this->redirect(Docebo::createLmsUrl('site/index', array('error'=>2)));
						}
					} else {
						// LinkedIn has sent a response, user has granted permission, take the temp access token, the user's secret and the verifier to request the user's real secret key
						$response = $OBJ_linkedin->retrieveTokenAccess($_SESSION['oauth']['linkedin']['request']['oauth_token'], $_SESSION['oauth']['linkedin']['request']['oauth_token_secret'], $_GET['oauth_verifier']);
						if($response['success'] === TRUE) {
							// the request went through without an error, gather user's 'access' tokens
							$_SESSION['oauth']['linkedin']['access'] = $response['linkedin'];

							// set the user as authorized for future quick reference
							$_SESSION['oauth']['linkedin']['authorized'] = TRUE;

							$OBJ_linkedin->setResponseFormat(LinkedIn::_RESPONSE_JSON);
							$profile = $OBJ_linkedin->profile('~/email-address');

							if($profile && json_decode($profile['linkedin'])) {
								$userEmail = json_decode($profile['linkedin']);

								$ui = new DoceboUserIdentity($userEmail, 'dummy');

								if ($ui->authenticateWithEmail()) {

                                    // Check if the User is Coming from the New Hydra Frontend
                                    if( Yii::app()->session['isComingFromHydra'] ) {
                                        /** Since he is already Authenticated, Create Token for him and Return him back to Hydra Frontend */
                                        $server = DoceboOauth2Server::getInstance();
                                        $server->init();
                                        $token = $server->createAccessToken("hydra_frontend", $ui->getId(), "api");
                                        $this->redirectToHydra(array_merge(array("type" => "oauth2_response"),$token));
                                    }

									if(Yii::app()->user->login($ui)){
										// Redirect to the user area
										$this->redirect(Docebo::getUserAfetrLoginRedirectUrl());
									}
								}

                                // The user did not Manage to login, Check from where he is coming from and redirect him accordingly
                                if( Yii::app()->session['isComingFromHydra'] ) {
                                    $this->redirectToHydra( array( 'error' => Yii::t('login', '_NOACCESS')) );
                                }

								$this->redirect(Docebo::createLmsUrl('site/index', array('error'=>2)));
							}
						} else {

                            // The user did not Manage to login, Check from where he is coming from and redirect him accordingly
                            if( Yii::app()->session['isComingFromHydra'] ) {
                                $this->redirectToHydra( array( 'error' => Yii::t('login', '_NOACCESS')) );
                            }

                            $this->redirect(Docebo::createLmsUrl('site/index', array('error'=>2)));
						}
					}
					break;
				default:
					// nothing being passed back, display demo page
					$this->redirect(Docebo::createAbsoluteLmsUrl('socialLogin/linkedinLogin', array(LinkedIn::_GET_TYPE=>'initiate')));
					break;
			}
		} catch(LinkedInException $e) {

            // The user did not Manage to login, Check from where he is coming from and redirect him accordingly
            if( Yii::app()->session['isComingFromHydra'] ) {
                $this->redirectToHydra(  array( 'error' => Yii::t('login', '_NOACCESS')) );
            }

			// exception raised
			echo $e->getMessage();
		}

        // The user did not Manage to login, Check from where he is coming from and redirect him accordingly
        if( Yii::app()->session['isComingFromHydra'] ) {
            $this->redirectToHydra( array( 'error' => Yii::t('login', '_NOACCESS')) );
        }
	}

	public function actionTwitterLogin(){
        // Load the Twitter Oauth Library
        Yii::import('addons.social.twitter.*');
        Yii::import('addons.social.twitter.Util.JsonDecoder');

		$consumerKey = Settings::get('social_twitter_consumer');
		$consumerSecret = Settings::get('social_twitter_secret');

		if(!empty($_GET['oauth_verifier']) && !empty($_SESSION['oauth_token']) && !empty($_SESSION['oauth_token_secret'])){
			// We've got everything we need. Exchange request tokens for access token

			// TwitterOAuth instance, with two new parameters we got in twitter_login.php
			$twitteroauth = new TwitterOAuth($consumerKey, $consumerSecret, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);

			// Let's request the access token
			$access_token = $twitteroauth->oauth("oauth/access_token", array("oauth_verifier" => $_GET['oauth_verifier']));

            // Save it in a session var
            $_SESSION['access_token'] = $access_token;

            $connection = new TwitterOAuth($consumerKey, $consumerSecret, $access_token['oauth_token'], $access_token['oauth_token_secret']);

            /**
             * TODO: Investigate how to Retrieve the user's mail so we can Authenticate him in our system
             * Till then this functionality is not working properly !!!!
             */

            // Check from where the user is coming and redirect him accordingly
            if(Yii::app()->session['isComingFromHydra']) {
                $this->redirectToHydra( array( 'error' => Yii::t('login', '_NOACCESS')) );
            }

            $this->redirect(Docebo::createLmsUrl('site/index', array('error'=>2)));

		} else {
			// Request authentication by the user by redirecting him to Twitter
			// The TwitterOAuth instance
			$twitteroauth = new TwitterOAuth($consumerKey, $consumerSecret);
			// Requesting authentication tokens, the parameter is the URL we will be redirected to
			$request_token = $twitteroauth->oauth("oauth/request_token", array( "oauth_callback" => Docebo::createAbsoluteLmsUrl('socialLogin/twitterLogin') ));

			// Saving them into the session
			$_SESSION['oauth_token'] = $request_token['oauth_token'];
			$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];

			// If everything goes well..
			if($twitteroauth->getLastHttpCode()==200){
				// Let's generate the URL and redirect
				$url = $twitteroauth->url("oauth/authorize", array("oauth_token" => $request_token['oauth_token']));
                $this->redirect($url);
			}
            // Check from where the user is coming and redirect him accordingly
			if(Yii::app()->session['isComingFromHydra']) {
                $this->redirectToHydra( array( 'error' => Yii::t('login', '_NOACCESS')) );
            }
            // It's a bad idea to kill the script, but we've got to know when there's an error.
            die('Something wrong happened. Check consumer key and secret.');
		}

	}

	private function redirectToHydra($params) {
        unset(Yii::app()->session['isComingFromHydra']);
        $url = Docebo::createHydraUrl('learn', 'signin', array_merge(array( 'type' => 'oauth2_response' ), $params) );
        $this->redirect($url);
    }


}

