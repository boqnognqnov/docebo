<?php

class UserController extends Controller {


    /**
     * (non-PHPdoc)
     * @see CController::filters()
     */
    public function filters() {
        return array(
            'accessControl',
        );
    }


    /**
     * (non-PHPdoc)
     * @see CController::accessRules()
     */
    public function accessRules() {
        return array(

            array('allow',
            	'actions' => array(	'axLogin', 'axLostdata', 'recoverPwd', 'confirmRegistration'),
                'users' => array('*'),
            ),

	        array('allow',
	            'actions'=>array('axRegister'),
                'expression' => "(!Yii::app()->user->isGuest || (Yii::app()->user->isGuest && strtolower(Settings::get('register_type', '')) != admin))",
	        ),

        	array('allow',
       			'actions' => array(	'changePassword', 'changeAvatar', 'editProfile', 'getCustomDateFormats', 'additionalFields', 'axEditBillingInfo', 'axGetBillingInfo'),
        		'users' => array('@'),
        	),


        	array('deny',
        		'users' => array('*'),
        		'deniedCallback' => array($this, 'accessDeniedCallback'),
        	),

        );
    }

    /**
     * Ajax called action to show (html ajax response) login propmt, process the login request and redirect accordingly.
     *
     * NB: It is important to note that this action will (must) be always called by a Dialog2 instance!!!!!
     * Extremely Dialog2 specific things are used inside the rendered views!!
     *
     * Call this in a.ajax Dialog2 style. Internally it uses form.ajax Dialog2 approach.
     *
     */
    public function actionAxLogin() {
	    if(Yii::app()->request->isAjaxRequest){
	        // If login button is defined? (meaning login form is being posted)
	        $login_button = Yii::app()->request->getParam('login_button', false);

	        // Reason for calling this action ? (like: user click 'buy' while still not logged in)
	        // Possible reasons (as of this writing): 'buy', 'subscribe', 'overbook', etc.
	        $reason =  Yii::app()->request->getParam('reason', false);

	        // Analyze the 'reason' and prepare the message to the user
	        // ! It is tempting to have ONE single message and just replace buy/subscribe/overbook,
	        // but lets give us a chance to hace arbitrary messages for all 'reasons'

	        // Default:
	        switch ($reason) {
	            case 'trialextensioncode':
	                $message = Yii::t("user", 'Existing user? Log in now!');
	                break;

				case 'buy' :
				case 'overbook' :
	            case 'subscribe' :

				default: {
	                $message = Yii::t("user", "Existing user? Log in now!");
	                break;
	            }
	        }


	        // Get the redirect URL, if any (url encoded!!!)
	        $redirectUrl = Yii::app()->request->getParam('redirect_url', '');
		    $courseId = Yii::app()->request->getParam('course_id', null);

	        // Is this a Login form POST action? Authenticate, redirect, etc.
	        if ( $login_button ) {

	            $username = Yii::app()->request->getParam('username', '');
	            $password = Yii::app()->request->getParam('password', '');

	            $ui = new DoceboUserIdentity($username, $password);

				if ($ui->authenticate())
				{
					if($ui->checkUserAlreadyLoggedIn())
						Yii::app()->user->setFlash('error', Yii::t('login', '_TWO_USERS_LOGGED_WITH_SAME_USERNAME'));
					elseif(Yii::app()->user->login($ui))
					{
						LearningTracksession::model()->updateAll(array('active' => 0), 'active = 1 AND idUser = '.Yii::app()->user->id);

						// Success. Now redirect to requested URL, if any
						if ( empty($redirectUrl) ) {
							$redirectUrl = $this->createUrl('site/index');
						}

						// We are inside a Dialog2 content. Need to render a simple, non-ajax-ed form to redirect the user.
						$html = $this->renderPartial("_login_success_redirect", array(
								'redirect_url' => urldecode($redirectUrl),
							), true);


						echo $html;

						Yii::app()->end();
					}
					else
						Yii::app()->user->setFlash('error',Yii::t("login", '_NOACCESS'));
	                // END. Get out!
	            }else {
	                // Error
	                // @todo Analyze and give more usefull feedback
	                Yii::app()->user->setFlash('error',Yii::t("login", '_NOACCESS'));
	            }

            }


	        // Default workflow: Render and get the form html
	        $html = $this->renderPartial("_form_login", array(
	                'reason' => $reason,
	                'message' => $message,
	                'redirect_url' => $redirectUrl,
			        'courseId' => $courseId,
					'show_register_link' => (Settings::get('register_type', 'admin') != 'admin')
	            )
	        , true);


	        // Echo back the result [to Dialog2!!]
	        echo $html;

	    }else{
		    $this->redirect(Docebo::createLmsUrl('site/index'));
		    Yii::app()->end();
	    }

    }

	/**
	 * This method validates all the fields submitted in the registration form.
	 * @return array('success'=>boolean,'message' =>error)
	 */
	public function validateFormRegister() {

		$helper = new RegisterHelper();
		$options = $helper->getRegisterOptions();

		$result = array('success'=>false,'message' =>'');

		// Empty Username
		$username = trim(Yii::app()->request->getParam('username'));
		if(empty($username)) {
			$result['message'] = Yii::t('register','_ERR_INVALID_USER');
			return $result;
		}

		// Checking duplicate userid in core_user
		$criteria = new CDbCriteria();
		$criteria->addCondition("userid LIKE :userid");
		$criteria->params[':userid'] = '/'.$username;
		$userModel = CoreUser::model()->count($criteria);
		//$userModel = CoreUser::model()->countByAttributes(array('userid' => "/" . $username));
		if($userModel) {
			//$result['message'] = Yii::t('register','_ERR_INVALID_USER');
			$result['message'] = Yii::t('register','_ERR_DUPLICATE_USER');
			return $result;
		}
		// ... and in core_user_temp too
		$criteria = new CDbCriteria();
		$criteria->addCondition("userid LIKE :userid");
		$criteria->params[':userid'] = $username; //NOTE: core_user_temp table do not use the '/' at the beginning of the userid field
		$userModel = CoreUserTemp::model()->count($criteria);
		//$userModel = CoreUserTemp::model()->countByAttributes(array('userid' => $username));
		if($userModel) {
			//$result['message'] = Yii::t('register','_ERR_INVALID_USER');
			$result['message'] = Yii::t('register','_ERR_DUPLICATE_USER');
			return $result;
		}

		// Checking duplicate email in core_user_temp
		$userModel = CoreUserTemp::model()->countByAttributes(array('email' => Yii::app()->request->getParam('email')));
		if($userModel) {
			$result['message'] = Yii::t('register','_ERR_DUPLICATE_MAIL');
			return $result;
		}

		// Checking if firstname and lastname are mandatory
		if($options['lastfirst_mandatory'] == 'on') {
			$firstname=Yii::app()->request->getParam('firstname');
			$lastname=Yii::app()->request->getParam('lastname');
			if(empty($firstname) || empty($lastname)) {
					$result['message'] = Yii::t('register','_SOME_MANDATORY_EMPTY');
					return $result;
			}
		}

		// privacy policy accepted?
		if($options['privacy_policy'] == 'on') {
				if(Yii::app()->request->getParam('privacy')!='on') {
					$result['message'] = Yii::t('register','_ERR_POLICY_NOT_CHECKED');
					return $result;
				}
		}

		$password = Yii::app()->request->getParam('password');
		// The password must contain at least C chars
		if(strlen($password)<Settings::get('pass_min_char')) {
			$result['message'] = Yii::t('register','_PASSWORD_TOO_SHORT');
			return $result;
		}

		// Checking if password must be alfanumeric
		if($options['pass_alfanumeric'] == 'on' ) {
				if( !preg_match('/[a-z]/i', $password) || !preg_match('/[0-9]/', $password) ) {
					$result['message'] = Yii::t('register','_ERR_PASSWORD_MUSTBE_ALPHA');
					return $result;
				}
		}


		// password should be different from username
		if($options['pass_not_username'] == 'on' && strtolower($password) == strtolower($username)) {
				$result['message'] = Yii::t('configuration','_PASS_NOT_USERNAME');
				return $result;
		}

		// The two passwords must be equal.
		if(Yii::app()->request->getParam('password') != Yii::app()->request->getParam('password_retype')) {
			$result['message'] = Yii::t('register','_ERR_PASSWORD_NO_MATCH');
			return $result;
		}

		// Checking if the email is valid -
		$regex = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';
		if (!preg_match($regex, Yii::app()->request->getParam('email'))) {
			$result['message'] = Yii::t('register','_ERR_INVALID_MAIL');
			return $result;
		}

		// Checking duplicate email in core_user
		$userModel = CoreUser::model()->countByAttributes(array('email' => Yii::app()->request->getParam('email')));
		if($userModel) {
			$result['message'] = Yii::t('register','_ERR_DUPLICATE_MAIL');
			return $result;
		}


		// Checking duplicate email in core_user_temp
		$userModel = CoreUserTemp::model()->countByAttributes(array('email' => Yii::app()->request->getParam('email')));
		if($userModel) {
			$result['message'] = Yii::t('register','_ERR_DUPLICATE_MAIL');
			return $result;
		}

		// Checking if org chart is mandatory and none selected
		$regCodeMandatory = Settings::get('mandatory_code')=='on' && (Settings::get('registration_code_type')=='tree_man' || Settings::get('registration_code_type')=='tree_drop');
		if ($regCodeMandatory){

			// What the user submitted in the form
			$regcode = Yii::app()->request->getParam('reg_code');
			$found = false;

			// ivo fix. reg_code 0 is valid code of group
			if ($regcode>=0){

				// Check if this org chart is valid/exists
				if(Settings::get('registration_code_type')=='tree_man'){
					$found = CoreOrgChartTree::model()->countByAttributes(array('code'=>$regcode));
				}elseif(Settings::get('registration_code_type')=='tree_drop'){
					$found = CoreOrgChartTree::model()->countByAttributes(array('idOrg'=>$regcode));
				}
			}

			// Org chart code IS mandatory, but the user hasn't selected one
			// or the code he entered doesn't exist as a org chart
			if(!$found){
				$result['message'] = Yii::t('register','_INVALID_CODE');
				return $result;
			}
		}

		$result['success']=true;
		return $result;
	}



	/**
	 * Ajax called action to show (html ajax response) lost data form in which an user can
	 * recover the username or the password.
	 */
	public function actionAxLostdata() {

		$lostPwd =  Yii::app()->request->getParam('lostpwd_button',false);
        $error = false;

		/*
		// The requested action is for lost user?
		if(!empty($lostUser)) {
			$email = Yii::app()->request->getParam('email');

			$userModel = CoreUser::model()->findByAttributes(array('email' => $email));

			if(filter_var($email, FILTER_VALIDATE_EMAIL) !== false) {

				if($userModel)
				{
					// If DomainBrandingApp is enabled, use the HTTP_HOST and ignore 'url' setting
					if (PluginManager::isPluginActive('DomainBrandingApp')) {
						$url = parse_url(Yii::app()->getBaseUrl(true), PHP_URL_HOST);
					} else {
						$url = Settings::get('url', '');
					}

					$body = Yii::t('register', '_LOST_USERID_MAILTEXT', array(
						'[date_request]' => date("d-m-Y"),
						'[url]' => $url,
						'[userid]' => substr($userModel->userid, 1) // removing ACL_SEPARATOR ???
					));

					$subject = Yii::t('register', '_LOST_USERID_TITLE');
					$fromAdminEmail = array(Settings::get('mail_sender', 'noreply@docebo.com'));
					$toEmail = array($email);

					// Sending the email
					$mm = new MailManager();
					$mm->setContentType('text/html');

					$result = $mm->mail($fromAdminEmail, $toEmail, $subject, $body);
				}

				$html = $this->renderPartial("_form_lostusr_success", array(
													'message' => Yii::t('register','_MAIL_SEND_SUCCESSFUL'),
													'result' => $result,
													'fromAdminEmail' => $fromAdminEmail,
													'toEmail' => $toEmail,
													'body'=> $body

											), true);
				echo $html;


				Yii::app()->end();

			} else {
				$error = Yii::t("register", '_ERR_INVALID_MAIL');
			}



		} else */
		if(!empty($lostPwd)) {

			$usernameOrEmail = Yii::app()->request->getParam('usernameOrEmail');

			$userModel = CoreUser::model()->find(
				'userid = :userid OR email = :email ORDER BY idst DESC',
				array(':userid' => '/' . $usernameOrEmail, ':email' => $usernameOrEmail)
			);

			if($userModel && !empty($usernameOrEmail))
			{

				// check if user is disabled (not active) / expired
				if($userModel->expiration || $userModel->valid != 1){
					$currentDayInLms = Yii::app()->localtime->getLocalNow('Y-m-d');
					$expirationDate = CoreUser::getExpirationDate($userModel->idst);
					if (($currentDayInLms > $expirationDate) || $userModel->valid != 1) {
						Yii::app()->user->setFlash('error', Yii::t('user', 'This user has been deactivated or his active status has expired.'));
						$html = $this->renderPartial('_form_lostdata', array(
							'error' => $error,
						));
						echo $html;
						Yii::app()->end();
					}
				}


				$code = md5(mt_rand() . mt_rand());
				// Checking if the code generated already exists in core_pwd_recover
				$corePwdRecoverModel = CorePwdRecover::model()->findByPk($userModel->idst);

				if ($corePwdRecoverModel === NULL) {
					$corePwdRecoverModel = new CorePwdRecover();
					$corePwdRecoverModel->idst_user = $userModel->idst;
					$corePwdRecoverModel->random_code = $code;
					$corePwdRecoverModel->request_date = Yii::app()->localtime->toLocalDateTime();
					$corePwdRecoverModel->save();

				} else {
					$corePwdRecoverModel->random_code = $code;
					$corePwdRecoverModel->request_date = Yii::app()->localtime->toLocalDateTime();
					$corePwdRecoverModel->update();
				}

				// Get the link, but prevent Url manager to strip "release", if any
				$oldStripRelease = Yii::app()->urlManager->getStripRelease();
				Yii::app()->urlManager->setStripRelease(false);

				$link = Yii::app()->createAbsoluteUrl('user/recoverPwd', array('code' => $code));

				Yii::app()->urlManager->setStripRelease($oldStripRelease);


				$fromAdminEmail = array(Settings::get('mail_sender', 'noreply@docebo.com'));
				$toEmail = array($userModel->email);
				$subject = Yii::t('register', '_LOST_PWD_TITLE');

				$body = Yii::t('register', '_LOST_PWD_MAILTEXT', array(
					'[link]' => $link,
					'[username]' => $userModel->getClearUserId()
				));

				$mm = new MailManager();
				$mm->setContentType('text/html');

				try {
					$result = $mm->mail($fromAdminEmail, $toEmail, $subject, $body);
					$html = $this->renderPartial("_form_lostpwd_success", array('message' => Yii::t('register', '_MAIL_SEND_SUCCESSFUL_PWD'), 'body' => $body), true);
					echo $html;
					Yii::app()->end();
				} catch (Swift_RfcComplianceException $e) {
					$error = Yii::t('standard', '_OPERATION_FAILURE');
				}
			} else {
				Yii::app()->user->setFlash('error', Yii::t('user', 'Please make sure to enter a username or an email that is of an existing user.'));
				$html = $this->renderPartial('_form_lostdata', array(
					'error' => $error,
				));
				echo $html;
				Yii::app()->end();
			}
		}

		$html = $this->renderPartial('_form_lostdata', array(
            'error' => $error,
        ));
		echo $html;
	}


	/**
	 * This action is called when the link of recover password sent by email has been clicked by the user.
	 * By default it shows a form with the fields (password, retype password, code(hidden)).
	 *
	 */
	public function actionRecoverPwd() {
		$message="";

		$randomCode = Yii::app()->request->getParam('code');
		$send = Yii::app()->request->getParam('send',false);

		$pwdRecoverModel = CorePwdRecover::model()->findByAttributes(array('random_code' => $randomCode));

		$userModel = CoreUser::model()->findByPk($pwdRecoverModel->idst_user);

		// check if the user account is expired
		if($userModel->expiration && !CoreUser::isUserGodadmin($userModel->idst)){
			$currentDayInLms = Yii::app()->localtime->getLocalNow('Y-m-d');
			$expirationDate = CoreUser::getExpirationDate($userModel->idst);
			if ($currentDayInLms > $expirationDate) {
				// the user account is expired, don't let him login, but show the message for that error
				Yii::app()->user->setFlash('expired_account', Yii::t('standard', 'Your account has expired on {date}. Please, contact your administrator for further information.', array('{date}' => $userModel->expiration)));

				Yii::app()->getController()->redirect(Yii::app()->createAbsoluteUrl('site/index', array('error' => 5)), true, 302, true);
				return false;
			}
		}
		// We show the form to recover the pwd
		if(!$send && $pwdRecoverModel) {
				$this->render('recover_pwd', array('code'=>$randomCode,'message' => Yii::t('register','_CHOOSE_NEW_PASSWORD')));
				Yii::app()->end();
		}

		if($pwdRecoverModel) {

				if($send) {
					$pwd = Yii::app()->request->getParam('pwd');
					$rePwd = Yii::app()->request->getParam('re_pwd');

//					// Password are the same?
//					if($pwd === $rePwd) {
//
//						$minChars = Settings::get('pass_min_char');
//						// Checking min char
//                        if(strlen($pwd) >= $minChars) {
//                            // update the password
//                            $userModel->new_password = $pwd;
//                            // Checking for common passwords and legit words
//                            if ( $userModel->passwordDictionaryCheck('new_password')) {
//                                $userModel->pass = Yii::app()->security->hashPassword($pwd);
//
//                                $userModel->scenario = 'changePassword';
//
//                                if($userModel->save(false)) {
//
//                                    // Delete all password reset codes issued for this user
//                                    // since he just changed his password using one of them
//                                    CorePwdRecover::model()->deleteAllByAttributes(array(
//                                        'idst_user'=>$pwdRecoverModel->idst_user
//                                    ));
//
//                                    Yii::app()->user->setFlash('success', Yii::t('register','Your password has been changed.'));
//                                    $url = Docebo::createAppUrl('lms:site/index');
//                                    $this->redirect($url);
//                                    Yii::app()->end();
//
//                                } else {
//                                    $error = Yii::t('standard','_OPERATION_FAILURE');
//                                    Yii::app()->user->setFlash('error', $error);
//                                    $this->render('recover_pwd', array('code'=>$randomCode,'message' => $message));
//                                    Yii::app()->end();
//                                }
//                            } else {
//                                $error = Yii::t('register', 'Password cannot contain common dictionary words and most common passwords');
//                                Yii::app()->user->setFlash('error', $error);
//                                $this->render('recover_pwd', array('code'=>$randomCode,'message' => $message));
//                                Yii::app()->end();
//                            }
//
//                        } else {
//							$error = Yii::t('register','_PASSWORD_TOO_SHORT');
//							Yii::app()->user->setFlash('error', $error);
//							$this->render('recover_pwd', array('code'=>$randomCode,'message' => Yii::t('register','_CHOOSE_NEW_PASSWORD')));
//
//							Yii::app()->end();
//						}
//
//					} else {
//						$error = Yii::t('register','_ERR_PASSWORD_NO_MATCH');
//						Yii::app()->user->setFlash('error', $error);
//						$this->render('recover_pwd', array('code'=>$randomCode,'message' => Yii::t('register','_CHOOSE_NEW_PASSWORD')));
//						Yii::app()->end();
//					}
                    if(isset($userModel->userid) && $userModel->userid){
                        $username = $userModel->getClearUserId();
                    }
                    else{
                        $username = false;
                    }
                    $result = Yii::app()->security->checkPasswordStrength($pwd, $rePwd, $username);
                    if(!$result['success'] && $result['message']!='')
                    {
                        $error = $result['message'];
						Yii::app()->user->setFlash('error', $error);
						$this->render('recover_pwd', array('code'=>$randomCode,'message' => Yii::t('register','_CHOOSE_NEW_PASSWORD')));
						Yii::app()->end();
                    }
                    else{
                        $userModel->new_password = $pwd;
                        $userModel->pass = Yii::app()->security->hashPassword($pwd);

                        $userModel->scenario = 'changePassword';

                        if($userModel->save(false)) {
                            // Delete all password reset codes issued for this user
                            // since he just changed his password using one of them
                            CorePwdRecover::model()->deleteAllByAttributes(array(
                                'idst_user'=>$pwdRecoverModel->idst_user
                            ));

                            Yii::app()->user->setFlash('success', Yii::t('register','Your password has been changed.'));
                            $url = Docebo::createAppUrl('lms:site/index');
                            $this->redirect($url);
                            Yii::app()->end();

                        } else {
                            $error = Yii::t('standard','_OPERATION_FAILURE');
                            Yii::app()->user->setFlash('error', $error);
                            $this->render('recover_pwd', array('code'=>$randomCode,'message' => $message));
                            Yii::app()->end();
                        }
                    }

				}
		} else {
			$error = Yii::t('register','_INVALID_RANDOM_CODE');
			Yii::app()->user->setFlash('error', $error);
			$this->render('recover_pwd', array('code'=>$randomCode,'message' => Yii::t('register','_CHOOSE_NEW_PASSWORD')));
			Yii::app()->end();

		}



	}


	/*
	private function getRegisterOptions() {
		$options = array(
			'lastfirst_mandatory' => Settings::get('lastfirst_mandatory'),
			'register_type' 		=> Settings::get('register_type'),
			'pass_alfanumeric' 		=> Settings::get('pass_alfanumeric'),
			'pass_not_username' 		=> Settings::get('pass_not_username'),
			'pass_min_char' 		=> Settings::get('pass_min_char'),
			'hour_request_limit' 	=> Settings::get('hour_request_limit'),
			'privacy_policy' 		=> Settings::get('privacy_policy'),
			'mail_sender'			=> Settings::get('mail_sender'),
			'field_tree'			=> Settings::get('field_tree')
		);
		return $options;
	}
	*/

	private function getRegisterExtraFields($mand_sym = '<span class="mandatory">*</span>') {
		$html = '';
		$extraFields = array();

		/* ~ registration code */
		$registration_code_type = Settings::get('registration_code_type', '0');
		$code_is_mandatory 		= Settings::get('mandatory_code', false) == 'on';

        // Trigger event to let plugins perform custom actions before rendering the Registraion extra fields
        $event =  new DEvent($this, array(
            'code_is_mandatory' => $code_is_mandatory,
            'registration_code_type' => $registration_code_type,
            'mand_sym' => $mand_sym
        ));
        // If we want to render custom fields
        Yii::app()->event->raise('onBeforeRenderRegistrationExtraFields',$event);
        if(!$event->shouldPerformAsDefault() && $event->return_value){
            return $event->return_value;
        }

		switch($registration_code_type) {
			case "0" :
				//nothin to do
				break;
			case "tree_course" :
			case "code_module" :
			case "tree_man" :
				// we must ask the user to insert a manual code
				$regCodeValue = Yii::app()->request->getParam('reg_code', '');
				$regCodeLabel = Yii::t('standard', '_CODE') . ($code_is_mandatory ? ' '.$mand_sym : '');
				$html .=
				'<div class="control-group">' .
					CHtml::label($regCodeLabel, 'reg_code', array(
						'class' => 'control-label'
					)) .
					'<div class="controls">' .
					CHtml::textField('reg_code',  $regCodeValue, array(
						'class' => 'pull-right',
						'maxlength' => 24,
						'autocomplete' => 'off'
					)) .
					'</div>
				</div>';
			break;
			case "tree_drop" :
				// we must show to the user a selection of code
				$dropdownData = array(-1 => '-');
				$regCodeValue = Yii::app()->request->getParam('reg_code', '');
				$regCodeLabel = Yii::t('standard', '_CODE') . ($code_is_mandatory ? ' '.$mand_sym : '');

				// retrieving all folder names
				$criteria = new CDbCriteria();
				$criteria->alias = 't1';
				$criteria->select = 't1.idOrg, t1.code';
				$criteria->with = array(
					'coreOrgChart' => array(
						'alias' => 't2',
						'select' => 't2.translation',
						'joinType' => 'INNER JOIN',
						'condition' => 't2.lang_code = :lang_code AND t1.code != ""',
						'params' => array(
							':lang_code' => Yii::app()->session['current_lang']
						)
					)
				);


				// If we're in a multidomain environment, let's show only nodes under the current branch
				if(PluginManager::isPluginActive("MultidomainApp") && ($multidomainClient = CoreMultidomain::resolveClient())) {
					/* @var $multidomainClient CoreMultidomain */
					if($multidomainClient->orgChart) {
						$criteria->addCondition("t1.iLeft >= :iLeft AND t1.iRight <= :iRight");
						$criteria->params[':iLeft'] = $multidomainClient->orgChart->iLeft;
						$criteria->params[':iRight'] = $multidomainClient->orgChart->iRight;
					}
				}

				$criteria->order = 't2.translation';
				$criteria->together = true;
				$tree_codes = CoreOrgChartTree::model()->findAll($criteria);

				foreach ($tree_codes as $tree_code) {
					/* @var $tree_code CoreOrgChartTree */
					$dropdownData[$tree_code->idOrg] = $tree_code->coreOrgChart->translation();
				}

				$html .=
					'<div class="control-group">' .
						CHtml::label($regCodeLabel, 'reg_code', array(
							'class' => 'control-label'
						)) .
						'<div class="controls">' .
						CHtml::dropDownList('reg_code', Yii::app()->request->getParam('reg_code', ''), $dropdownData, array(
							'class' => 'pull-right'
						)) .
						'</div>
				</div>';
			break;
		}

		return $html;
	}



	/**
	 * Displays a form with all mandatory fields, unfilled by the current user
	 * (only in the current language)
	 */
	public function actionAdditionalFields(){

        $defaultFields = CoreUser::getUnfilledDefaultFields();
        $mandatoryFields = CoreUserField::getUnfilledMandatoryFields();

		if((!$mandatoryFields || empty($mandatoryFields)) && count($defaultFields) <= 0){
			$this->redirect($this->createUrl('site/index'));
		}

		$error = false;

        $idst = Yii::app()->user->id;
        $user = CoreUser::model()->findByPk($idst);

		if(isset($_POST) && !empty($_POST)) {

			// Loop through user's submitted values and
			// check if all mandatory fields were filled.
			// Otherwise, add an error and get back to the same page

            foreach($defaultFields as $field) {
                $defaultFieldData = isset($_POST["CoreUser"][$field]) ? $_POST["CoreUser"][$field] : false;
                if($defaultFieldData || strlen($defaultFieldData) > 0) {
                    $user->$field = $defaultFieldData;
                    if($user->validate(array($field)))
                        $user->save(false);
                } else {
                    $error = true;
                }
            }

			foreach($mandatoryFields as $model){
				/* @var $model CoreField */
				$fieldValueProvided = isset($_POST["field_{$model->getFieldType()}"][$model->getFieldId()])
							? $_POST["field_{$model->getFieldType()}"][$model->getFieldId()]
							: false;

				if($model->getFieldType() == CoreUserField::TYPE_UPLOAD)
				{
					// Special case for fields with type "file upload"
					// User value for them is not a string, but an uploaded file
					// that is stored in storage in the collection CFileStorage::COLLECTION_CORE_FIELD
					// with the filename saved as user entry as a pointer to the actual file
					$files = Yii::app()->request->getParam('FileUpload', array());
					if (!empty($files) && isset($files[$model->getFieldId()]) && !empty($files[$model->getFieldId()]))
						// This will trigger saving the uploaded file to the storage on CoreFieldUserentry::beforeSave()
						Field::store($model->getFieldId(), Yii::app()->user->getId(), $files[$model->getFieldId()]);
					else
						// Field mandatory but user hasn't uploaded a file for it yet
						$error = true;
				} elseif($model->getFieldType() == CoreUserField::TYPE_TEXT || $model->getFieldType() == CoreUserField::TYPE_TEXTAREA){
					if($fieldValueProvided || strlen($fieldValueProvided) > 0){
						// All other Field types store the user's entry as a string value.
						// Save this provided value in DB here
						Field::store($model->getFieldId(), Yii::app()->user->getId(), $fieldValueProvided);
					}

				}elseif($fieldValueProvided){
					// All other Field types store the user's entry as a string value.
					// Save this provided value in DB here
					Field::store($model->getFieldId(), Yii::app()->user->getId(), $fieldValueProvided);
				}else{
					// The current looped field is mandatory and form was submitted
					// but the user hasn't provided a value for this field. An error
					$error = true;
				}
			}

			//check for non-assigned upload fields
			Yii::app()->user->loadUserModel(true)->removeUploadsOnRequested();
			$stillUnfilledMandatoryFields = CoreUserField::getUnfilledMandatoryFields();
            $stillUnfilledDefaultFields = CoreUser::getUnfilledDefaultFields();
			if ((is_array($stillUnfilledMandatoryFields) && count($stillUnfilledMandatoryFields) > 0)
            || count($stillUnfilledDefaultFields) > 0) {
				$error = true;
			}

			if(!$error){

				//Group Auto Assign
				$hasSelfPopulatingGroups = Yii::app()->db->createCommand("SELECT COUNT(*) FROM core_group WHERE assign_rules = 1")->queryScalar();
				if($hasSelfPopulatingGroups > 0)
				{
					GroupAutoAssignTask::singleUserRun(Yii::app()->user->loadUserModel());
					Yii::app()->authManager->refreshCache(Yii::app()->user->id);
				}
				// No more mandatory additional fields left for the user to fill
				// Redirect to homepage
				$this->redirect(Docebo::getUserHomePageUrl());
			}
		}

		//load bootstrap DatePicker css/js libraries
		Yii::app()->bootstrap->registerAssetCss('bootstrap-datepicker.css');
		Yii::app()->bootstrap->registerAssetJs('bootstrap.datepicker.js');

		$this->render('_form_additional_fields', array(
            'user' => $user,
            'defaultFields' => $defaultFields,
			'fields' => $mandatoryFields,
			'error' => $error,
		));
	}




	/**
	 * Confirm a registration trought a registration link sent by email.
	 */
	public function actionConfirmRegistration() {

		$randomCode = Yii::app()->request->getParam('random_code','');
		$userTempInfo = CoreUserTemp::model()->findByAttributes(array('random_code'=>$randomCode, 'confirmed' => 0));

		/**
		 * @var $userTempInfo CoreUserTemp
		 */


		$regCourse = null;
		if($userTempInfo) {
			if(isset($userTempInfo->self_subscribe_course)){
				$regCourse = $userTempInfo->self_subscribe_course;
			}

			// Registration link has expired
			$requestTimeLimit = strtotime(Yii::app()->localtime->fromLocalDateTime($userTempInfo->request_on)) + 3600 * intval(Settings::get('hour_request_limit'));
			$utcRequestTimeLimit = date('Y-m-d H:i:s', $requestTimeLimit);

			if (strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s')) > strtotime($utcRequestTimeLimit)) {
				Yii::app()->user->setFlash('error',Yii::t('register','_REG_ELAPSEDREQUEST'));
				$userTempInfo->delete();
			} else {

				$registerType = Settings::get('register_type', '', true);

                //edge case when a user is confirmed by an admin and the user clicks the email link at the same time
                $userExists = CoreUser::model()->exists('idst = :idst', array(':idst' => $userTempInfo->idst));
                if($userExists) {
                    $this->render('registration_result',array(
                        'message' => Yii::t('register', '_REG_YOUR_ABI_TO_ACCESS')//'Registration Confirmed'
                    ));
                    Yii::app()->end();
                }

				$helper = new RegisterHelper();
				if ($helper->_confirmRegistration($userTempInfo, $registerType)) {
					$newUserModel = CoreUser::model()->findByPk($userTempInfo->idst);
					if ($newUserModel) {
						$newUserModel->email_status = 1;
						$newUserModel->save(false);
					}

					if($regCourse && (Settings::get('registration_code_type')=='tree_course')){
						$courseModel = LearningCourse::model()->findByPk($regCourse);
						if($courseModel && $newUserModel){
							$maxSubsReached     =   $courseModel->course_type != LearningCourse::TYPE_ELEARNING || ($courseModel->max_num_subscribe==0 || $courseModel->max_num_subscribe > $courseModel->getTotalSubscribedUsers(false, false, false)) ? false : true;
							$subscribeAsWaiting =  ($maxSubsReached && $courseModel->allow_overbooking) ? true : false ;
							$courseUserModel = new LearningCourseuser('self-subscribe');
							if($courseModel->course_type == LearningCourse::TYPE_ELEARNING && !$subscribeAsWaiting && !($courseModel->subscribe_method == LearningCourse::SUBSMETHOD_MODERATED)){
							    $courseUserModel->subscribeUser($newUserModel->idst, $newUserModel->idst, $courseModel->idCourse, 0, false, false, false, false, false, false);
							}else{
                                $waiting = 1;
                                if (in_array($courseModel->course_type, array(
                                        LearningCourse::TYPE_CLASSROOM,
                                        LearningCourse::TYPE_WEBINAR
                                    )) &&
                                    ($courseModel->max_num_subscribe == 0 || $courseModel->max_num_subscribe > $courseModel->getTotalSubscribedUsers(false,false, false)) &&
                                    $courseModel->subscribe_method == LearningCourse::SUBSMETHOD_FREE
                                ) {
                                    $waiting = 0;
                                }

                                $courseUserModel->subscribeUser($newUserModel->idst, $newUserModel->idst, $courseModel->idCourse, $waiting, false, false, false, false, false, false);
							}
						}
					}
				}

				//Group Auto Assign
				$hasSelfPopulatingGroups = Yii::app()->db->createCommand("SELECT COUNT(*) FROM core_group WHERE assign_rules = 1")->queryScalar();
				if($hasSelfPopulatingGroups > 0) {
					$scheduler = Yii::app()->scheduler;
					$params = array(
						'user_idst' => $userTempInfo->idst,
						'offset' => 0
					);
					$scheduler->createImmediateJob(GroupAutoAssignTask::JOB_NAME, GroupAutoAssignTask::id(), $params);
				}

				if($registerType=='moderate'){
					$this->render('registration_result',array('message' => Yii::t('register','Registration Confirmed. An admin must approve your account before you can login.')));
					Yii::app()->end();
				}
			}

			// Debug code...
			$this->render('registration_result',array(
				'message' => Yii::t('register', '_REG_YOUR_ABI_TO_ACCESS')//'Registration Confirmed'
			));

		} else {
			$this->render('registration_result', array('message' => Yii::t('register', 'This link is invalid.')));
		}

	}




	/**
	 * This method add a new user in the table core_user.
	 * This is a porting in yii of the function registerUser (lib.aclmanager.php)
	 * @param type $params : associative array with all users data
	 */
	private function addUser($params) {
		$idSt = null;
		$pwdExpireAt = null;
        if (intval(Settings::get('pass_max_time_valid')) > 0) {
            $timestampPwdExpireAt = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s')) + intval(Settings::get('pass_max_time_valid')*24*3600);
            $utcPwdExpireAt = date("Y-m-d H:i:s", $timestampPwdExpireAt);
            $pwdExpireAt = Yii::app()->localtime->toLocalDateTime($utcPwdExpireAt);
        }

		$userModel = new CoreUser();

		$idSt = isset($params['idst']) ? $params['idst'] : $this->createSt();

		// Create a new user
		//$userModel->idst = $idSt;  // <-- THIS one won't work! PK is overwritten by save()!!!!!!!!! [plamen] => idst is always the next one!
		$userModel->idst = $idSt;  // Now it works because beforeSave() is checking for idst and does NOT create new idst if already set


		// ACL_SEPARETOR
		$userModel->userid ='/' . $params['userid'];
		$userModel->pass = $params['pass'];
		$userModel->firstname = $params['firstname'];
		$userModel->lastname = $params['lastname'];
		$userModel->email = $params['email'];
		$userModel->signature = true;
		$userModel->facebook_id =(isset($params['facebook_id']) ? $params['facebook_id'] : NULL);
		$userModel->twitter_id =(isset($params['twitter_id']) ? $params['twitter_id'] : NULL);
		$userModel->linkedin_id =(isset($params['linkedin_id']) ? $params['linkedin_id'] : NULL);;
		$userModel->google_id =(isset($params['google_id']) ? $params['google_id'] : NULL);
		$userModel->register_date = Yii::app()->localtime->toLocalDateTime();
		$userModel->pwd_expire_at = $pwdExpireAt;

		if($userModel->save()){
			// We have to add a new record in _password_history
			$passwordHistoryModel = new CorePasswordHistory();
			// idst_user, pwd_date, passw, changed_by
			$passwordHistoryModel->idst_user = $userModel->idst;
			$passwordHistoryModel->passw = $params['pass'];
			$passwordHistoryModel->pwd_date = Yii::app()->localtime->toLocalDateTime();
			$passwordHistoryModel->changed_by = $userModel->idst; // Need the value of the logged user? Yii::app()->user->id
			$passwordHistoryModel->save();

			// Assign the user to the "Users" group
			try {
				$auth = Yii::app()->authManager;
				$auth->assign(Yii::app()->user->level_user, $userModel->idst);
				$auth->assign('/oc_0', $userModel->idst);
				$auth->assign('/ocd_0', $userModel->idst);
			} catch (Exception $e) {
				// already assign, just ignore
			}

			// Save the user's preferred language during registration as his main lang
			$usrLang = strtolower(Lang::getCodeByBrowserCode($params['language']));
			if($usrLang){
				$usrPref = new CoreSettingUser();
				$usrPref->id_user = $userModel->idst;
				$usrPref->value = $usrLang;
				$usrPref->path_name = 'ui.language';
				$usrPref->save();
				if($usrPref->hasErrors()){
					Yii::log("Error saving user's lang preference. Errors: ".CVarDumper::dumpAsString($usrPref->getErrors(), 10, true));
				}
			}

			return $userModel->idst;

		} else {
			return FALSE;
		}

	}


	/**
	 * This method generates a new idst in the table core_st table.
	 */
	private function createSt() {

		$coreStModel = new CoreSt();
		$coreStModel->save();
		return $coreStModel->primaryKey; // Returns the last id

	}




	/**
	 * Edit Billing Information form (Ajax called, Dialog2).
	 * Does 2 tasks: Show form and Save data
	 *
	 */
	public function actionAxEditBillingInfo() {

	    if ( !Yii::app()->request->isAjaxRequest ) {
	        throw new CHttpException(400, "Invalid request.");
	    }

	    // Get user id, coming from initial caller or from the form (hidden field)
	    $idUser = Yii::app()->request->getParam('idUser','');

	    // Check
	    if ( !$idUser ) {
	        echo "Invalid user id.";
	        Yii::app()->end();
	    }


	    // Is submit button clicked ??
	    $submit_button = Yii::app()->request->getParam('bill_info_submit_button', false);
		$billingInfo = CoreUserBilling::model()->getUserLastBillingInfo($idUser);

		$ok = true;
	    if ($submit_button ) {
			// Get current billing info record, if any
			// Create new one, if not found
			if (!$billingInfo) {
				$billingInfo = new CoreUserBilling();
				$billingInfo->idst = $idUser;
			}

			// Get POSTed data
			$billingInfo->bill_company_name = isset($_POST["bill_company_name"]) ? $_POST["bill_company_name"] : "";
			$billingInfo->bill_vat_number = isset($_POST["bill_vat_number"]) ? $_POST["bill_vat_number"] : "";
			$billingInfo->bill_address1 = isset($_POST["bill_address1"]) ? $_POST["bill_address1"] : "";
			$billingInfo->bill_address2 = isset($_POST["bill_address2"]) ? $_POST["bill_address2"] : "";
			$billingInfo->bill_city = isset($_POST["bill_city"]) ? $_POST["bill_city"] : "";
			$billingInfo->bill_state = isset($_POST["bill_state"]) ? $_POST["bill_state"] : "";
			$billingInfo->bill_zip = isset($_POST["bill_zip"]) ? $_POST["bill_zip"] : "";
			$billingInfo->bill_country_code = isset($_POST["bill_country_code"]) ? $_POST["bill_country_code"] : "";

			if (!isset($_POST["bill_city"]) || empty($_POST["bill_city"]) || !isset($_POST["bill_address1"]) || empty($_POST["bill_address1"])
				|| !isset($_POST["bill_country_code"]) || empty($_POST["bill_country_code"])){

				$ok = false;
				Yii::app()->user->setFlash('error',Yii::t('cart','In order to continue, please provide the requested billing information'));
			} else {
				// Get old SHA1 and calculate new one
				$sha1_old = $billingInfo->sha1_sum;
				$sha1_new = $billingInfo->sha1();

				// Different SHA1s is a signal for differet data content, so, create new record instead of save
				if ($sha1_new != $sha1_old) {
					$billingInfo->sha1_sum = $sha1_new;
					$billingInfo->id = null;  // !!! needed
					$billingInfo->setIsNewRecord(true);  // set record as new (will be INSTERTED when save()
					$ok = $billingInfo->save();
				} else {
					$ok = $billingInfo->save();
				}
			}
	        // Handle errors
	        // @todo
	        if ( !$ok ) {
	            //echo "Error (todo: handle errors)";
	        }
	        else {

				// Trigger event to let plugins perform custom actions after saving the new billing information
				Yii::app()->event->raise('OnBillingInfoUpdated', new DEvent($this, array(
					'billingInfo' => &$billingInfo
				)));

	            $html = "<a class='auto-close'></a>";
	            echo $html;
	            Yii::app()->end();
	        }

	    }

	    // Ok, looks like this is the initial call to this dialog2
	    //$billingInfo = CoreUserBilling::model()->getUserLastBillingInfo($idUser);
	    $countries = CoreCountry::model()->findAll(array('order' => 'name_country'));

	    $html = $this->renderPartial("_form_billing_info", array(
            'countries' => $countries,
            'binfo' => $billingInfo,
	        'idUser' => $idUser,
			'executeJSCheck' => !$ok
	    ), true);


	    echo $html;

	}


	/**
	 * AJAX called action to return a JSON-ed billing information about user
	 */
	public function actionAxGetBillingInfo() {

	    if ( !Yii::app()->request->isAjaxRequest ) {
	        throw new CHttpException(400, "Invalid request.");
	    }


	    $result = array('success' => false);

	    $idUser = Yii::app()->request->getParam('idUser','');

	    if (!$idUser) {
	        echo CJSON::encode($result);
	        Yii::app()->end();
	    }


	    $billingInfo = CoreUserBilling::model()->getUserLastBillingInfo($idUser);

	    if (!$billingInfo) {
	        echo CJSON::encode($result);
	        Yii::app()->end();
	    }

	    $data = $billingInfo->attributes;
	    $data['extra_country_name'] = CoreCountry::getNameByIsoCode($billingInfo->bill_country_code);
	    $result = array('success' => true, 'data' => $data);

	    echo CJSON::encode($result);


	}

	public function actionChangePassword(){
		if(Yii::app()->user->isGuest){
			$this->redirect($this->createUrl('site/index'));
			Yii::app()->end();
		}

		$old_pwd = isset($_POST['old_pwd']) && $_POST['old_pwd'] ? $_POST['old_pwd'] : null;
		$new_pwd = isset($_POST['new_pwd']) && $_POST['new_pwd'] ? $_POST['new_pwd'] : null;
		$new_pwd_confirm = isset($_POST['new_pwd_confirm']) && $_POST['new_pwd_confirm'] ? $_POST['new_pwd_confirm'] : null;
		$newPwdEncrypted = Yii::app()->security->hashPassword($new_pwd);
		$currentRealPassEncrypted = Yii::app()->user->getUserAttrib('pass');

		$error = null;

		if($old_pwd){

			if(md5($old_pwd) != $currentRealPassEncrypted && !Yii::app()->security->validatePassword($old_pwd, $currentRealPassEncrypted)){
				// Old password entered is wrong
				$error = Yii::t("register", "_ERR_PWD_OLD");
			}

			// New password empty
			if(!$new_pwd || !$new_pwd_confirm)
				$error = Yii::t("register", "_PASSWORD_TOO_SHORT");

			// New password and confirm new password don't match
			if($new_pwd!=$new_pwd_confirm)
				$error = Yii::t("register", "_ERR_PASSWORD_NO_MATCH");

			// Old password is the same as the new one
			if($old_pwd==$new_pwd)
				$error = Yii::t("register", "_REG_PASS_MUST_DIFF", array(
					'[diff_pwd]' => '1'
				));

			// Password length
			if(Settings::get('pass_min_char') && strlen($new_pwd) < Settings::get('pass_min_char')){
				$error = Yii::t("register", "_PASSWORD_TOO_SHORT");
			}

			// Password must be alphanumeric only setting check
			if(Settings::get('pass_alfanumeric')=='on'){
                //LB-158 the password is not accepted if special characters are included
                //NOTE: it was allowing just numbers and letters or: 'if(!ctype_alnum($new_pwd)) {'
                //but the rest of the LMS code checks only is/are there any letter(-s) or number(-s) in the password
				if(!(preg_match('/[a-z]/i', $new_pwd) && preg_match('/[0-9]/', $new_pwd))) {
					$error = Yii::t("register", "_ERR_PASSWORD_MUSTBE_ALPHA");
				}
			}

			// Check password must not be same as username
			if(Settings::get('pass_not_username') == 'on' && strtolower($new_pwd) == strtolower(Yii::app()->user->getUsername())) {
				$error = Yii::t("configuration", "_PASS_NOT_USERNAME");
			}

			// Check password history
			$maxOldPwdsToCheck = (int) Settings::get('user_pwd_history_length');
			if($maxOldPwdsToCheck > 0){

				// New password same as current one for the user
				if($newPwdEncrypted == $currentRealPassEncrypted || md5($new_pwd) == $currentRealPassEncrypted){
					$error = str_replace('[diff_pwd]', $maxOldPwdsToCheck, Yii::t('register', '_REG_PASS_MUST_DIFF'));
				}else{
					$c = new CDbCriteria();
					$c->select = 'passw';
					$c->addCondition('idst_user = :current_user');
					$c->addCondition('passw IS NOT NULL');
					$c->addCondition("passw <> ''");
					$c->params[':current_user'] = Yii::app()->user->id;
					$c->order = 'pwd_date DESC';
					$c->limit = $maxOldPwdsToCheck;
					$c->index = 'passw';

					$oldPasswords = CorePasswordHistory::model()->findAll($c);
					if($oldPasswords && isset($oldPasswords[$newPwdEncrypted])){
						// You've used this password recently (history limit: core_setting.user_pwd_history_length)
						$error = str_replace('[diff_pwd]', $maxOldPwdsToCheck, Yii::t('register', '_REG_PASS_MUST_DIFF'));
					}
				}
			}

			if(!$error){
				// All good, change the password here

				$currentUser = Yii::app()->user->loadUserModel(true); //load current user model, with most recent updated info
				if($currentUser){
					$currentUser->pass = $newPwdEncrypted;
					$currentUser->password_changed = 1;
					$currentUser->force_change = 0;
					$currentUser->scenario = 'changePassword';

					if($currentUser->save(false)){

						$currentUser->refresh();

						if($currentUser->pass != $newPwdEncrypted){
							// The new password failed to be saved correctly
							$currentUser->pass = md5($new_pwd);
							$currentUser->save(false);
							Yii::log('Can not save password in new encryption algorithm. Migrations run?', CLogger::LEVEL_ERROR);
						}

						// Will not redirect the user to this page from all other
						// pages anymore (since he now changed his password)
						unset($_SESSION['must_renew_pwd']);

						// Redirect back to index
						$url = Docebo::getUserAfetrLoginRedirectUrl();
						$this->redirect($url);
					}
				}else{
					// User not found in database, better don't do anything (hacking attempt?)
					$this->redirect($this->createAbsoluteUrl('site/index'));
				}

			}

		}

		$this->render('_form_change_password', array(
			'error'=>$error
		));
	}


	public function actionChangeAvatar() {

		if (isset($_POST['ChangeAvatarForm'])) {

			// move image to it's final storage location => /files/doceboCore/photo/_filename_
			$filename = trim($_POST['ChangeAvatarForm']['avatar']);
			$uploadTmpFile = Docebo::getUploadTmpPath() . DS . $filename;

            if (file_exists($uploadTmpFile) && is_file($uploadTmpFile)) {

                $newFilename = Yii::app()->user->getIdst() . '_' . time() . '.' . strtolower(CFileHelper::getExtension($filename));

                $storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
                $storageManager->storeAs($uploadTmpFile, $newFilename);

                FileHelper::removeFile($uploadTmpFile);

                $user = Yii::app()->user->loadUserModel();

                // First, remove old file/avatar
                if (!empty($user->avatar)) {
                	$storageManager->remove($user->avatar);
                }

                // Then set the new one
                $user->avatar = $newFilename;
                $user->save(false);
            }

            Yii::app()->end();
		}

        $uniqueId = substr(md5(uniqid(rand(), true)), 0, 10);
		$this->renderPartial('_modal_change_avatar', array('uniqueId'=>$uniqueId));
	}

    public function actionGetCustomDateFormats() {

        if (Yii::app()->request->isAjaxRequest) {
            $form = new CActiveForm();
            $locale = Yii::app()->request->getParam('code');

            $settingDatelocale = CoreSettingUser::model()->findByAttributes(array(
                'id_user' => Yii::app()->user->id,
                'path_name' => 'date_format',
            ));
            if (!$settingDatelocale) {
                $settingDatelocale = new CoreSettingUser();
            }

			// removed the 'size' attribute since it breaks the look of the html dropdown element
            $dropdown = $form->dropDownList($settingDatelocale, 'value', Yii::app()->localtime->getDateTimeFormatsArray($locale), array('name' => 'CoreSettingUser[date_format]', 'prompt'=>Yii::t('configuration', 'Select a date format...')));

            $this->sendJSON(array(
                'success' => true,
                'html' => $dropdown
            ));
        }
    }




	public function actionEditProfile() {

		$idst = Yii::app()->user->getIdst();
		$editOnlyPassword = (Settings::get('profile_only_pwd', 'on') == 'on');

		$user = CoreUser::model()->findByPk($idst);
		if (!$user) {
			echo 'User not found';
			Yii::app()->end();
		}

		if ($editOnlyPassword)
			$user->scenario = 'changePassword';
		else
			$user->scenario = 'selfEditProfile';


		// model for user language
		$settingUser = CoreSettingUser::model()->findByAttributes(array(
			'id_user' => $user->idst,
			'path_name' => 'ui.language',
		));
		if ($settingUser === null) {
			$settingUser = new CoreSettingUser();
			$settingUser->id_user = $user->idst;
			$settingUser->path_name = 'ui.language';
			$settingUser->value =  CoreUser::getDefaultLangCode();
		}
		$settingUser->scenario = 'selfEditProfile';


		// model for user timezone
		$settingTimezone = null;
		// is the user allowed to set his timezone?
		if ('on' === Settings::get('timezone_allow_user_override')) {
			$settingTimezone = CoreSettingUser::model()->findByAttributes(array(
				'id_user' => $user->idst,
				'path_name' => 'timezone',
			));
			if ($settingTimezone === null) {
				$settingTimezone = new CoreSettingUser();
				$settingTimezone->id_user = $user->idst;
				$settingTimezone->path_name = 'timezone';
				$settingTimezone->value = Settings::get('timezone_default');
			}
			$settingUser->scenario = 'selfEditProfile';
		}


		// model for user date format
		$settingDateformat = null;
		$settingDatelocale = null;
		// is the user allowed to set the date format?
		if ('user_selected' === Settings::get('date_format')) {
			$settingDateformat = CoreSettingUser::model()->findByAttributes(array(
				'id_user' => $user->idst,
				'path_name' => 'date_format',
			));
			if ($settingDateformat === null) {
				$settingDateformat = new CoreSettingUser();
				$settingDateformat->id_user = $user->idst;
				$settingDateformat->path_name = 'date_format';
			}
			$settingDateformat->scenario = 'selfEditProfile';
			$settingDatelocale = CoreSettingUser::model()->findByAttributes(array(
				'id_user' => $user->idst,
				'path_name' => 'date_format_locale',
			));
			if ($settingDatelocale === null) {
				$settingDatelocale = new CoreSettingUser();
				$settingDatelocale->id_user = $user->idst;
				$settingDatelocale->path_name = 'date_format_locale';
				$settingDatelocale->value = Lang::getBrowserCodeByCode($settingUser->value); // Set user language as default
			}
			$settingDatelocale->scenario = 'selfEditProfile';
		}


		//if confirm button has been pressed then process the request
		if (isset($_POST['CoreUser'])) {

			// Purify POST variables to prevent XSS attacks
			foreach($_POST['CoreUser'] as $key => $value)
				$_POST['CoreUser'][$key] = Yii::app()->htmlpurifier->purify($value);

			$oldEmail = $user->email;
			$user->attributes = $_POST['CoreUser'];
			if ($oldEmail != $user->email) {
				$user->email_status = 0;
			}

			if(isset($_POST['CoreUser']['additional'])) {
				$user->setAdditionalFieldValues($_POST['CoreUser']['additional'], $_POST['FileUpload']);
			}

			//different cases need different validation process. EMPTY IF??? MAYBE SHOULD BE REMOVED?
			if ($editOnlyPassword) {

			}
			if ($user->save()) {

				$user->removeUploadsOnRequested();

				if (isset($_POST['CoreSettingUser'])) {

					foreach ($_POST['CoreSettingUser'] as $path_name => $value) {
						switch ($path_name) {
							case 'ui.language':
								$settingUser->value = $value;
								$settingUser->save();
								break;
							case 'timezone':
								$settingTimezone->value = $value;
								$settingTimezone->save();
								break;
							case 'date_format':
								$settingDateformat->value = $value;
								$settingDateformat->save();
								break;
							case 'date_format_locale':
								$settingDatelocale->value = $value;
								$settingDatelocale->save();
								break;
							default:
								break;
						}
					}
				}

				$hasSelfPopulatingGroups = Yii::app()->db->createCommand("SELECT COUNT(*) FROM core_group WHERE assign_rules = 1")->queryScalar();
				if($hasSelfPopulatingGroups > 0)
				{
					GroupAutoAssignTask::singleUserRun(Yii::app()->user->loadUserModel());
					Yii::app()->authManager->refreshCache(Yii::app()->user->id);
				}

				echo CHtml::link('', '', array('class'=>'auto-close'));
				Yii::app()->end();
			} else {
				Yii::log('Errors in '.__METHOD__.':'."\n".implode("\n", $user->getErrors()));
			}
		}
		//end of request processing

		//from now on it is standard dialog rendering
		$user->userid = Yii::app()->user->getRelativeUsername($user->userid);

		DatePickerHelper::registerAssets();

		$additionalFields = $user->getAdditionalFields();
		if (Settings::get('use_node_fields_visibility', 'off') == 'on') {
			//exclude non-visible fields
			$visibleFields = $user->getVisibleFields();
			if (!empty($visibleFields)) {
				$filteredFields = array();
				foreach ($additionalFields as $additionalField) {
					if (in_array($additionalField->getFieldId(), $visibleFields)) {
						$filteredFields[] = $additionalField;
					}
				}
				$additionalFields = $filteredFields;
			} else {
				$additionalFields = array();
			}
		}

		$params = array(
			'user' => $user,
			'settingUser' => $settingUser,
			'editOnlyPassword' => $editOnlyPassword,
			'additionalFields' => $additionalFields,
			'settingTimezone' => $settingTimezone,
			'settingDateformat' => $settingDateformat,
			'settingDatelocale' => $settingDatelocale,
		);

		if (Yii::app()->event->raise('BeforeModalEditProfileRender', new DEvent($this, $params))) {
			$this->renderPartial('_modal_edit_profile', $params,false,true);
		}

	}



	/**
	 * Ajax called action to show (html ajax response) register new account propmt.
	 * If core_setting.register_type != admin, u can register a new account.
	 * If the value is *self*: the user can auto-register, then he received an email with the confirmation link.
	 * If the value is *moderate*: TO CHECK, I guess an admin has to approve the registration.
	 * In both case, the new user is temporarily saved on *core_user_temp* table and a new *idst* is store in *core_st* table.
	 */
	public function actionAxRegister() {

		if(Yii::app()->request->isAjaxRequest){
			//load datepicker
			DatePickerHelper::registerAssets();

			$rq = Yii::app()->request;
			$db = Yii::app()->db;
			/* @var $rq CHttpRequest */
			/* @var $db CDbConnection */

			$helper = new RegisterHelper();

			//-----

			//--- generic parameters and data
			$regCodeMandatory = (Settings::get('mandatory_code')=='on' && (Settings::get('registration_code_type')=='tree_man' || Settings::get('registration_code_type')=='tree_drop'));
			$symbolForMandatory = '<span class="mandatory">*</span>';
			$options = $helper->getRegisterOptions();


			//analyze additional fields availability in advance (visibility depends on multidomain/registration code
			$regCode = $rq->getParam('reg_code', false);
			if (is_string($regCode) && trim($regCode) == '') { $regCode = false; }
			if (is_numeric($regCode) && $regCode < 0) { $regCode = false; }
			//if $regCode === false then the function "RegisterHelper::getFieldsFilter()" will ignore the registration code

			if ($regCode === false) {
				//empty reg. code is valid if it's not mandatory
				$isValidRegCode = !$regCodeMandatory;
			}
			if ($regCode !== false) {
				//check if reg. code matches a org. branch code/id (depending on settings)
				$isValidRegCode = $helper->checkRegistrationCode($regCode);
			}

			if ($isValidRegCode) {
				//we can filter visible fields using org. branch assigned nodes, since we have a valid code
				$fieldsFilter = $helper->getFieldsFilter($regCode);
				if (is_array($fieldsFilter)) {
					$numFields = count($fieldsFilter);
				} elseif ($fieldsFilter === false) {
                    $numFields = CoreUserField::model()->countByAttributes(['invisible_to_user' => 0]);
				} else {
					throw new CException('Error while resolving fields visibility'); //something went wrong while trying to detect visible fields
				}
			} else {
				//no valid registration code provided, so we can just perform a general count of additional fields to decide if we are in multistep case
                $numFields = CoreUserField::model()->countByAttributes(['invisible_to_user' => 0]);
			}

			//only if additional fields are available then we can use multistep registration
			$multiStep = ($numFields > 0);

			$userPostedData  = $_POST;
			$username = isset($_POST['username'])? strip_tags($_POST['username']) : '';
			// Sanitize the input params for core user db table !!!
			foreach($userPostedData as $attrKey => $attrValue) {
				if (is_string($attrValue)) {
					$userPostedData[$attrKey] = trim($attrValue);
					$_POST[$attrKey] = trim($attrValue);
				}
			}

			if(isset($userPostedData['username'])) $userPostedData['username'] = $username;
			if(isset($_POST['username'])) $_POST['username'] = $username;

			$confirmButtonPressed = $rq->getParam('register_button', false);
			$backButtonPressed = $rq->getParam('back_button', false);
			$fromStep = $rq->getParam('from_step', false);
			//if we have pressed confirm button, then

            $numDateFields = CoreUserField::model()->countByAttributes(['type' => CoreUserField::TYPE_DATE]);
			$loadDatePicker = (intval($numDateFields) > 0); //we do not have datepickers in stepped user info form
			$languages = Lang::getLanguages(true);
			Yii::app()->session['redirect_url_after_login'] = $redirect_url = Yii::app()->request->getParam('redirect_url', ''); //get the redirect URL, if any (url encoded!!!)
			Yii::app()->session['subscribe_course_id_after_login'] = $courseId = Yii::app()->request->getParam('courseId', false);
			//---


			if ($multiStep) {

				//--- Calculate the current step to be executed
				$currentStep = false;

				switch ($fromStep) {

					case 'step_1':
						if($isValidRegCode && $fieldsFilter){
							$additionalFields = Field::getAdditionalFields($symbolForMandatory, 'pull-right', $fieldsFilter, true);
							if(!$additionalFields){
								$currentStep = 'step_3';
							} else{
								$currentStep = 'step_2';
							}
						}else{
							$currentStep = 'step_2';
						}

						break;

					case 'step_2':

						if ($confirmButtonPressed) {
							$currentStep = 'step_3';
						}
						if ($backButtonPressed) {
							$currentStep = 'step_1';
						}
						break;

					case false:
						$currentStep = 'step_1';
						break;

				}


				//after detecting the current step to be executed, do it
				switch ($currentStep) {

					case 'step_1':
						//just print the dialog with first step content, no validations required here
						$html = $this->renderPartial("register/_form_register", array(
							'languages' => $languages,
							'userPostedData' => $userPostedData,
							'options' => $options,
							'mand_sym' => $symbolForMandatory,
							'extraFields' => $this->getRegisterExtraFields($symbolForMandatory),
							'redirect_url' => $redirect_url,
							'courseId' => $courseId,
							'multiStep' => $multiStep,
							'loadDatePicker' => $loadDatePicker
						), true, true);
						echo $html; // Echo back the result [to Dialog2!!]
						Yii::app()->end();
						break;

					case 'step_2':
						//second step: we need to validate submitted user data from first step and -in case of negative output- fallback to step_1 again
						$validation = $helper->validateUserInfo();

						if ($validation['success'] && !$isValidRegCode) {
							//mandatory or less, we cannot retrieve fields if invalid registration code has been provided
							$validation['success'] = false;
							$validation['message'] = Yii::t('register','_INVALID_CODE');
						}

						if ($validation['success']) {

							//print fields form
							$html = $this->renderPartial("register/_form_register_fields", array(
								'languages' => $languages,
								'userPostedData' => $userPostedData,
								'options' => $options,
								'mand_sym' => $symbolForMandatory,
								'additionalFields' => Field::getAdditionalFields($symbolForMandatory, 'pull-right', $fieldsFilter),
								'redirect_url' => $redirect_url,
								'courseId' => $courseId,
								'multiStep' => $multiStep,
								'loadDatePicker' => $loadDatePicker
							), true, true);
							echo $html; // Echo back the result [to Dialog2!!]
							Yii::app()->end();

						} else {

							//something went wrong during validation, so re-print first step form with proper error message
							Yii::app()->user->setFlash('error',Yii::t("user", $validation['message']));
							$html = $this->renderPartial("register/_form_register", array(
								'languages' => $languages,
								'userPostedData' => $userPostedData,
								'options' => $options,
								'mand_sym' => $symbolForMandatory,
								'extraFields' => $this->getRegisterExtraFields($symbolForMandatory),
								'redirect_url' => $redirect_url,
								'courseId' => $courseId,
								'multiStep' => $multiStep,
								'loadDatePicker' => $loadDatePicker
							), true, true);
							echo $html; // Echo back the result [to Dialog2!!]
							Yii::app()->end();

						}

						break;

					case 'step_3':

						//We have submitted second step form.
						//Now cases are the following:
						// - fields in second form are not well filled: re-print second form + error message
						// - fields in second form are OK: save the data in DB and print success message (or redirect in a new page)
						//NOTE: although it may look unnecessary, we still check for first dialog input validation

						$validationStep1 = $helper->validateUserInfo(); //step 1 input data is still in the submitted form

						if ($validationStep1['success'] == false) {
							//something went wrong during validation of first step, so re-print first step form with proper error message
							Yii::app()->user->setFlash('error', Yii::t("user", $validationStep1['message']));
							$html = $this->renderPartial("register/_form_register", array(
								'languages' => $languages,
								'userPostedData' => $userPostedData,
								'options' => $options,
								'mand_sym' => $symbolForMandatory,
								'extraFields' => $this->getRegisterExtraFields($symbolForMandatory),
								'redirect_url' => $redirect_url,
								'courseId' => $courseId,
								'multiStep' => $multiStep,
								'loadDatePicker' => $loadDatePicker
							), true, true);
							echo $html; // Echo back the result [to Dialog2!!]
							Yii::app()->end();

						} else {

							//first step was ok, now validate additional fields
							if (!is_array($fieldsFilter) && $fieldsFilter !== false) { throw new CException('Error while resolving fields visibility'); } //validate fields visibility
							$validationAdditionalFields = $helper->validateAdditionalFields($fieldsFilter); //check for unfilled mandatory fields
                            if (!$validationAdditionalFields) {
								//some mandatory fields have not been filled: re-print step 2 with error message
								Yii::app()->user->setFlash('error', Yii::t('register', '_SOME_MANDATORY_EMPTY'));
								$html = $this->renderPartial("register/_form_register_fields", array(
									'languages' => $languages,
									'userPostedData' => $userPostedData,
									'options' => $options,
									'mand_sym' => $symbolForMandatory,
									'additionalFields' => Field::getAdditionalFields($symbolForMandatory, 'pull-right', $fieldsFilter),
									'redirect_url' => $redirect_url,
									'courseId' => $courseId,
									'multiStep' => $multiStep,
									'loadDatePicker' => $loadDatePicker
								), true, true);
								echo $html; // Echo back the result [to Dialog2!!]
								Yii::app()->end();
							}

                            //if all is ok, save user info
							$registration = $helper->registerUser(false, $fieldsFilter);

                            if (!is_array($registration)) {

								//something went wrong while saving user info
								throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));

							} elseif (!$registration['success']) {

								//registration process gave some error with message. Just stay in step 2 and show message
								Yii::app()->user->setFlash('error', isset($registration['message']) ? $registration['message'] : Yii::t('standard', '_OPERATION_FAILURE'));
								$html = $this->renderPartial("register/_form_register_fields", array(
									'languages' => $languages,
									'userPostedData' => $userPostedData,
									'options' => $options,
									'mand_sym' => $symbolForMandatory,
									'additionalFields' => Field::getAdditionalFields($symbolForMandatory, 'pull-right', $fieldsFilter),
									'redirect_url' => $redirect_url,
									'courseId' => $courseId,
									'multiStep' => $multiStep,
									'loadDatePicker' => $loadDatePicker
								), true, true);
								echo $html; // Echo back the result [to Dialog2!!]
								Yii::app()->end();


							} else {

								//registration was successful, process returned output accordingly to settings

								//redirecting to login form
								if (isset($registration['redirect_url'])) {


									//Group Auto Assign
									$hasSelfPopulatingGroups = Yii::app()->db->createCommand("SELECT COUNT(*) FROM core_group WHERE assign_rules = 1")->queryScalar();
									if($hasSelfPopulatingGroups > 0) {
										$scheduler = Yii::app()->scheduler;
										$params = array(
											'user_idst' => $registration['idst'],
											'offset' => 0
										);
										$scheduler->createImmediateJob(GroupAutoAssignTask::JOB_NAME, GroupAutoAssignTask::id(), $params);
									}

									// We are inside a Dialog2 content. Need to render a simple, non-ajax-ed form to redirect the user.
									$html = $this->renderPartial("_login_success_redirect", array(
										'redirect_url' => $registration['redirect_url'],
									), true);
									echo $html;
								}

								//we have sent an email to the user
								if (isset($registration['email'])) {
									$html = $this->renderPartial("register/_form_register_success", array(
										'email' => $registration['email']
									), true);
									echo $html;
								} //html output should always be present if the registration has been successful

								if (isset($registration['admin_approval']) && $registration['admin_approval']) {
									$html = $this->renderPartial("register/_form_register_adminapproval", array(), true);
									echo $html;
								}

								Yii::app()->end();

							}

						}
						break;

					default:
						throw new CException('Invalid step');
						break;
				}


			} else {

				//single step, when no fields are present in the LMS

				// Submitted a registration form??
				if ($confirmButtonPressed) {

					$validation = $helper->validateUserInfo();
					$userPostedData = $_POST;

					if ($validation['success'] == true) {

						$registration = $helper->registerUser(false, $fieldsFilter);

						if (!$registration || !$registration['success']) {

							//something wrong happened while trying to register the user
							/*
							$html = $this->renderPartial("register/_form_register_failure", array(), true);
							echo $html;
							Yii::app()->end();
							*/
							//set an error message and fallback to user form again
							Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));

						} else {
							//registration was successful, process returned output accordingly to settings

							//redirecting to login form
							if (isset($registration['redirect_url'])) {
								//Group Auto Assign
								$hasSelfPopulatingGroups = Yii::app()->db->createCommand("SELECT COUNT(*) FROM core_group WHERE assign_rules = 1")->queryScalar();
								if($hasSelfPopulatingGroups > 0) {
									$scheduler = Yii::app()->scheduler;
									$params = array(
										'user_idst' => $registration['idst'],
										'offset' => 0
									);
									$scheduler->createImmediateJob(GroupAutoAssignTask::JOB_NAME, GroupAutoAssignTask::id(), $params);
								}

								// We are inside a Dialog2 content. Need to render a simple, non-ajax-ed form to redirect the user.
								$html = $this->renderPartial("_login_success_redirect", array(
									'redirect_url' => $registration['redirect_url'],
								), true);
								echo $html;
							}

							//we have sent an email to the user
							if (isset($registration['email'])) {
								$html = $this->renderPartial("register/_form_register_success", array(
									'email' => $registration['email']
								), true);
								echo $html;
							} //html output should always be present if the registration has been successful

							if (isset($registration['admin_approval']) && $registration['admin_approval']) {
								$html = $this->renderPartial("register/_form_register_adminapproval", array(), true);
								echo $html;
							}

							Yii::app()->end();
						}
					} else {
						Yii::app()->user->setFlash('error',Yii::t("user", $validation['message']));
					}

				}

				// Default workflow: Render and get the form html
				$html = $this->renderPartial("register/_form_register", array(
					'languages' => $languages,
					'userPostedData' => $userPostedData,
					'options' => $options,
					'mand_sym' => $symbolForMandatory,
					'extraFields' => $this->getRegisterExtraFields($symbolForMandatory),
					'courseId' => $courseId,
					'redirect_url' => $redirect_url,
					'multiStep' => $multiStep,
					'loadDatePicker' => $loadDatePicker
				), true, true);

				// Echo back the result [to Dialog2!!]
				echo $html;

				Yii::app()->end();

			}
		}else{
			Yii::app()->session['registerRedirect'] = true;
			$this->redirect(Docebo::createLmsUrl('site/index'));
			Yii::app()->end();
		}


	}


}