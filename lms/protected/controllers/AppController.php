<?php

class AppController extends Controller
{

	// PRIVATE!! Used only in this controller! Must match  ERP>>BackupSubscription model constants!
	private static $CYCLE_MONTHLY = 'monthly';
	private static $CYCLE_YEARLY = 'yearly';

	/**
	 * Apps on sale with no visible price & activate only by Sales
	 * @var array
	 */
	private $contactUsApps = array('OfflinePlayer', 'Okta', 'SimpleSaml', 'Salesforce', 'AuditTrail');

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$res = array();


		// keep it in the following order:

		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'expression' => 'BrandingWhiteLabelForm::isMenuVisible()'
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
			'deniedCallback' => array($this, 'accessDeniedCallback'),
		);

		return $res;
	}


	public function actionActivate()
	{
		PluginManager::loadPlugin('TwitterApp');
		echo TwitterApp::plugin()->activate();
	}


	public function actionIndexOLD()
	{
		$this->render('index_old');
	}

	public function actionIndex(){
	    
	    // Comment this block of code and will get the OLD behavior completely!
	    $api = new ErpApiClient2();
	    $isBundledPricing = $api->call('lms/is-bundle-pricing');
	    if ($isBundledPricing) {
	       $this->forward("apps2/index", true);
	       Yii::app()->end();
	    }
	    
		Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/myapps-search.js');
		$isInTrialPeriod = Docebo::isTrialPeriod();
		$this->render('index', array(
			'isInTrialPeriod' => $isInTrialPeriod
		));
	}

	public function actionTestlang() {
		$lang = Yii::app()->getLanguage();
		var_dump($lang);
	}

	public function actionTestapi1() {
		$params = array(
			'installation_id' => Docebo::getErpInstallationId(),
			'filter' => 'all' // all|free|paid|my_apps
		);
		$response = AppsMpApiClient::apiListAvailableApps($params);
		echo($response);
		exit;
	}

	public function actionTestapi2() {
		$params = array(
			'installation_id' => Docebo::getErpInstallationId(),
			'app_id' => Yii::app()->request->getParam('id', 9)
		);

		$response = AppsMpApiClient::apiGetAppDetails($params);
		echo($response);
		exit;
	}


	public function actionTestapi3() {
		$appId = Yii::app()->request->getParam('id');

		$app = $this->_getAppDetails($appId);

		var_dump($app);
		var_dump(Docebo::isTrialPeriod());
		exit;
	}

	/**
	 * Get all apps and filter them to use in Search Engine of Apps Page
	 *
	 * @throws CException
	 */
	public function actionGetAllApps(){
		$apps  = $this->_getAllApps();
		$appsData = array();
		$lang = Yii::app()->getLanguage();

		//Fall back to engish if the language is not en or it!!
		switch($lang){
			case 'en':
			case 'it':
				break;
			default:
				$lang = 'en';
				break;
		}
		$isECSInstallation = PluginManager::isPluginActive('EcsApp');

		foreach($apps as $key => $app){
			$isFree = ($app['market_type'] === 'free' ? true : false);
			$appsData[] = array(
				'app_id' => $app['app_id'],
				'name' => $app['lang'][$lang]['title'],
				'description' => $app['lang'][$lang]['short_description'],
				'isFree' => $isFree,
				'body' => $this->renderPartial('_app_template', array(
					'app' => $app,
					'isECSInstallation' => $isECSInstallation
				), true),
				'app' => $app,
				'isECSInstallation' => $isECSInstallation
			);
		}

		echo CJSON::encode(array(
				'html'  => $appsData,
		));
		Yii::app()->end();
	}

	public function actionAxAppsGrid()
	{
		$html = '';

		/*
		 * Call the erp api to get all apps
		 */
		$apps  = $this->_getAllApps();

		// we'll distribute the apps from the response between the following arrays
		$installedApps = array();
		$freeApps = array();
		$paidApps = array();

		if ($apps) {
			foreach ($apps as $id => $app) {

				// Check if this App is available as Yii module/plugin here
				$plugin_name =  ucfirst($app['internal_codename']) . "App";
				if (! PluginManager::isModulePlugin($plugin_name) ) {
					continue;
				}

				if ($app['is_installed'] == 1) {
					$installedApps[] = $app;
				}
				else if ($app['market_type'] == 'free') {
					$freeApps[] = $app;
				} else {
					$paidApps[] = $app;
				}
			}
		}

		$html .= $this->renderPartial('_apps_grid', array(
			'apps'=>$installedApps,
			'coverTile'=>$this->renderPartial('_cover_tile', array(
					'bgCss' => 'bg-color-orange',
					'title' => Yii::t('apps', 'My<br> apps'),
					'description' => Yii::t('apps', 'Your apps list.')
				), true),
			'backCoverTile'=> (empty($installedApps)) ? $this->renderPartial('_no_installed_apps', array(), true) : ''
		), true);

		$html .= $this->renderPartial('_apps_grid', array(
			'apps'=>$freeApps,
			'coverTile'=>$this->renderPartial('_cover_tile', array(
					'bgCss' => 'bg-color-blue',
					'title' => Yii::t('apps', 'Free apps'),
					'description' => Yii::t('apps', 'Browse our <strong>FREE</strong> apps to extend your Docebo Cloud'),
					'icon' => 'grid'
				), true)
		), true);

		$html .= $this->renderPartial('_apps_grid', array(
			'apps'=>$paidApps,
			'coverTile'=>$this->renderPartial('_cover_tile', array(
					'bgCss' => 'bg-color-green',
					'title' => Yii::t('apps', 'Paid apps'),
					'description' => Yii::t('apps', 'Browse our <strong>PAID</strong> apps to extend your Docebo Cloud'),
					'icon' => 'cart'
				), true)
		), true);

		echo CJSON::encode(array(
			'html'  => $html,
		));
		Yii::app()->end();
	}

	/*
	 * These will be displayed in the left sidebar in the Apps page.
	 * Note that the order here is important! It will be preserved during display.
	 */
	private function _getAppCategoryTranslations(){
		return array(
			'docebo_additional_features' => Yii::t('app', 'Docebo Additional Features'),
			'third_party_integrations' => Yii::t('app', 'Third party integrations'),
			'web_conferencing' => Yii::t('menu_course', '_TELESKILL_ROOM'),
			'single_sing_on' => Yii::t('app', 'Single Sign On'),
		);
	}

	public function actionGetAppCategories(){

		$categoryTranslations = $this->_getAppCategoryTranslations();

		// [category]=>[css class] pair. The CSS class will be
		// applied to the <a> link of the category in the Sidebarred widget
		// E.g. 'single_sing_on'=>'three-dots'
		// will apply the following CSS to the category 'single_sign_on':
		// .sidebarred .sidebar ul li a.three-dots span{}
		// See shared.css for the available classes
		$categoryIcons = array(
			'single_sing_on' => 'lock',
			'docebo_additional_features' => 'bullet-list',
			'third_party_integrations' => 'chain',
			'web_conferencing' => 'webcam',
		);

		$categories = array();

		// Add 'My apps' as the first category
		$categories[Yii::t('apps', 'My<br> apps')] = CHtml::link('<span></span>'.Yii::t('apps', 'My<br> apps'), '#myapps', array(
			'data-loaded' => 0,
			'class'=>'myapps category tiles myappsitem',
			'data-tab' => 'myapps',
			'data-url' => 'app/getApps&action=myapps',
		));

		foreach($categoryTranslations as $key=>$categoryName){

			$categories[$categoryName] = CHtml::link('<span></span>'.$categoryName, '#'. $key, array(
				'id' => $key.'_tab_btn',
				'data-loaded' => 0,
				'class'=>'myappsitem category '.$key . ($categoryIcons[$key] ? ' '.$categoryIcons[$key] : ''),
				'data-tab' => $key,
				'data-url' => 'app/getApps&action=category&catName='.trim($key),
			));
		}

		/*$categories[Yii::t('userlimit', 'Help Desk')] = CHtml::link('<span></span>'.Yii::t('userlimit', 'Help Desk'), '#fullpage-helpdesk', array(
			'data-loaded' => 0,
			'class'=>'fullpage-helpdesk category helpdesk myappsitem',
			'data-tab' => 'fullpage-helpdesk',
			'data-url' => 'app/getFullpageApp&app=helpdesk',
		));

		$categories[Yii::t('apps', 'Enterprise Cloud Solution')] = CHtml::link('<span></span>'.Yii::t('apps', 'Enterprise Cloud Solution'), '#fullpage-ecs', array(
			'data-loaded' => 0,
			'class'=>'fullpage-ecs category ecs myappsitem',
			'data-tab' => 'fullpage-ecs',
			'data-url' => 'app/getFullpageApp&app=ecs',
			'data-bootstro-id' => "bootstroNewAppsDesign",
		));*/

		echo CJSON::encode($categories);
		Yii::app()->end();
	}

	/**
	 * Return content/info for a special app that has a standalone category-like page
	 * (e.g. Help Desk app or the ECS app)
	 *
	 * @param string $app
	 */
	public function actionGetFullpageApp($app = null){
		$apps  = $this->_getAllApps();
		$appByGroup = array(); // sorted by price
		if($apps){
			foreach($apps as $singleApp){
				if($singleApp['app_group']){
					// Exclude apps without a group

					if(!isset($appByGroup[$singleApp['app_group']])){
						$appByGroup[$singleApp['app_group']] = array();
					}

					// Add this app to its group
					// Use the price as array key so we can easily sort the array by price later
					$appByGroup[$singleApp['app_group']][$singleApp['app_id']] = $singleApp;
				}
			}
		}

		// Sort the apps in each group by price
		foreach($appByGroup as $group => &$appsInGroup){
			ksort($appsInGroup);
		}

		switch($app){
//			case 'helpdesk':
//				$helpDeskAppsSorted = isset($appByGroup['helpdesk']) ? $appByGroup['helpdesk'] : array();
//
//				if(count($helpDeskAppsSorted) < 3){
//					echo CJSON::encode(array(
//						'html'  => 'The helpdesk app must have at least 3 levels'
//					));
//					Yii::app()->end();
//				}
//
//				$helpDeskSilver = array_slice($helpDeskAppsSorted, 0, 1, true);
//				$helpDeskSilver = reset($helpDeskSilver);
//
//				$helpDeskGold = array_slice($helpDeskAppsSorted, 1, 1, true);
//				$helpDeskGold = reset($helpDeskGold);
//
//				$helpDeskPlatinum = array_slice($helpDeskAppsSorted, 2, 1, true);
//				$helpDeskPlatinum = reset($helpDeskPlatinum);
//
//				$lmsPaymentMethod = ($helpDeskSilver['lms_payment_method'] == 'wire_transfer') ? 'wire_transfer' : 'credit_card';
//
//				$hasToContactResellerToInstall = ( !Docebo::isTrialPeriod() && $lmsPaymentMethod == 'wire_transfer' );
//
//				if($helpDeskPlatinum['is_installed'])
//					$highestInstalledHelpdesk = 'platinum';
//				elseif($helpDeskGold['is_installed'])
//					$highestInstalledHelpdesk = 'gold';
//				else // By default, silver is installed to all LMSes
//					$highestInstalledHelpdesk = 'silver';
//
//				$isTrial = Docebo::isTrialPeriod();
//
//				/*
//				 * To simplify debugging, you can emulate some simple things
//				 * like the current helpdesk level and Enterprise Cloud Solution
//				 * installed/not installed exactly where this comment is located
//				 * like this:
//				 * $highestInstalledHelpdesk = 'platinum';
//				 * $ecsInstalled = true; // ECS app installed
//				 * $ecsFullyActive = true; // ECS app installed and its migration finished
//				 * $isTrial = true;
//				 * $highestInstalledHelpdesk = true (true when LMS paid via Wire Transfer)
//				 */
//
//				$html = $this->renderPartial('fullpage_helpdesk', array(
//					'is_trial' => $isTrial, // bool
//					'helpdesk_apps' => array(
//						'silver'=> $helpDeskSilver, // array
//						'gold' => $helpDeskGold, // array
//						'platinum' => $helpDeskPlatinum, // array
//					),
//					'installed_helpdesk' => $highestInstalledHelpdesk, // silver, gold, platinum
//					'hasToContactResellerToInstall' => $hasToContactResellerToInstall, // bool
//					'ecsInstalled' => $ecsInstalled, // bool
//					'ecsFullyActive' => $ecsFullyActive, // bool
//					'paymentCycle' => $helpDeskSilver['pricing']['payment_cycle'], // monthly, yearly
//					'paymentMethod' => $helpDeskSilver['lms_payment_method'], // credit_card, wire_transfer
//				), true);
//				echo CJSON::encode(array(
//					'html'  => $html
//				));
//				break;
//			case 'ecs': // Enterprise Cloud Solution
//				$ecsApp = false;
//				if($apps){
//					// Find the ECS app
//					foreach($apps as $app){
//						if($app['is_enterprise']){
//							$ecsApp = $app;
//							break;
//						}
//					}
//				}
//
//				if(!$ecsApp){
//					echo CJSON::encode(array(
//						'html'  => 'Can not find the ECS app'
//					));
//					Yii::app()->end();
//				}
//
//				$lmsPaymentMethod = ($ecsApp['lms_payment_method'] == 'wire_transfer') ? 'wire_transfer' : 'credit_card';
//
//				$hasToContactResellerToInstall = ( !Docebo::isTrialPeriod() && $lmsPaymentMethod == 'wire_transfer' );
//
//				$html = $this->renderPartial('fullpage_ecs', array(
//					'is_trial' => Docebo::isTrialPeriod(),
//					'ecsApp' => $ecsApp,
//					'planIsGood' => (Settings::get('max_users') >= 500),
//					'priceIsSet' => ($ecsApp['pricing']['price'] > -1),
//					'lmsPaymentMethod' => $ecsApp['lms_payment_method'],
//					'hasToContactResellerToInstall' => $hasToContactResellerToInstall,
//				), true);
//				echo CJSON::encode(array(
//					'html'  => $html
//				));
//				break;
			default:
				echo CJSON::encode(array(
					'html'  => 'App is required'
				));
				Yii::app()->end();
		}
	}

	public function actionAxHelpdeskUpgradeDowngrade($from = null, $to = null, $confirm = 0){

		if(!$to){
			echo 'Target Help Desk plan not provided';
			Yii::app()->end();
		}

		$apps  = $this->_getAllApps();

		$appByGroup = array(); // sorted by price
		$ecsInstalled = false;
		if($apps){
			foreach($apps as $singleApp){
				if($singleApp['app_group']){
					// Exclude apps without a group

					if(!isset($appByGroup[$singleApp['app_group']])){
						$appByGroup[$singleApp['app_group']] = array();
					}

					// Add this app to its group
					// Use the price as array key so we can easily sort the array by price later
					$appByGroup[$singleApp['app_group']][$singleApp['app_id']] = $singleApp;
				}

				if($singleApp['is_enterprise'] && $singleApp['is_installed'])
					$ecsInstalled = true;
			}
		}

		// Sort the apps in each group by price
		foreach($appByGroup as $group => &$appsInGroup){
			ksort($appsInGroup);
		}

		$helpDeskAppsSorted = isset($appByGroup['helpdesk']) ? $appByGroup['helpdesk'] : false;

		if(!$helpDeskAppsSorted || !is_array($helpDeskAppsSorted) || count($helpDeskAppsSorted)<3){
			echo 'Invalid number of Help Desk app plans';
			Yii::app()->end();
		}

		$helpDeskSilver = array_slice($helpDeskAppsSorted, 0, 1, true);
		$helpDeskSilver = reset($helpDeskSilver);

		$helpDeskGold = array_slice($helpDeskAppsSorted, 1, 1, true);
		$helpDeskGold = reset($helpDeskGold);

		$helpDeskPlatinum = array_slice($helpDeskAppsSorted, 2, 1, true);
		$helpDeskPlatinum = reset($helpDeskPlatinum);

		$lmsPaymentMethod = ($helpDeskSilver['lms_payment_method'] == 'wire_transfer') ? 'wire_transfer' : 'credit_card';

		$hasToContactResellerToInstall = ( !Docebo::isTrialPeriod() && $lmsPaymentMethod == 'wire_transfer' );

		if($helpDeskPlatinum['is_installed'])
			$highestInstalledHelpdesk = 'platinum';
		elseif($helpDeskGold['is_installed'])
			$highestInstalledHelpdesk = 'gold';
		else
			$highestInstalledHelpdesk = 'silver';

		// Check which action we are performing now (upgrade or downgrade)
		// based on the current Helpdesk plan and the new one request
		$action = false;
		switch($highestInstalledHelpdesk){
			case 'platinum':
				if($to=='silver' || $to=='gold')
					$action = 'downgrade';
				break;
			case 'gold':
				if($to=='platinum')
					$action = 'upgrade';
				elseif($to=='silver')
					$action = 'downgrade';
				break;
			case 'silver':
				if($to=='gold' || $to=='platinum')
					$action = 'upgrade';
				break;
		}

		if(!$action){
			echo 'Invalid HelpDesk plan change. Are we upgrading or downgrading?';
			Yii::app()->end();
		}

		$newPlan = false;
		if($to=='platinum')
			$newPlan = $helpDeskPlatinum;
		elseif($to=='gold')
			$newPlan = $helpDeskGold;
		elseif($to=='silver')
			$newPlan = $helpDeskSilver;

		if($confirm){
			// ========== Actually install this HelpDesk plan ==========
			//             (ERP call + LMS plugin activation)

			Yii::log('Upgrade/downgrade Helpdesk called. From: '.$from.', to: '.$to, CLogger::LEVEL_INFO);
			Yii::log('Calling ERP to install app with ID: '.$newPlan['app_id'], CLogger::LEVEL_INFO);

			// call the erp api to install the app
			$apiParams = array(
				'installation_id' => Docebo::getErpInstallationId(),
				'app_id' => $newPlan['app_id']
			);
			$apiRes = AppsMpApiClient::apiAddApplication($apiParams);
			$apiRes = CJSON::decode($apiRes);

			// Activate the app in the LMS side, except on Helpdesk downgrade,
			// in which case the app activation is done by the ERP via API
			if($action != 'downgrade'){
				$res = PluginManager::activateAppByCodename($newPlan['internal_codename']);

				if(!$res){
					Yii::log('Can not activate Help Desk app with level '.$to. '. Invalid internal_codename: '.$newPlan['internal_codename'], CLogger::LEVEL_ERROR);
				}

				// install also dependent apps returned from api
				if (isset($apiRes['dependencies']) && is_array($apiRes['dependencies']))
				{
					foreach ($apiRes['dependencies'] as $appCodename)
					{
						PluginManager::activateAppByCodename($appCodename);
					}
				}
			}

			echo '<a class="auto-close reload"></a>';
			Yii::app()->end();
		}

		$this->renderPartial('dialog_helpdesk_planchange', array(
			'from' => $highestInstalledHelpdesk, // silver, gold or platinum
			'to' => $to, // silver, gold or platinum
			'action' => $action=='downgrade' ? 'downgrade' : 'upgrade',
			'proRata' => $newPlan['pricing']['pro_rata'],
			'subscription_expire_date' => $newPlan['subscription_expire_date'],
			'newPlan' => $newPlan,
			'helpdesk_apps' => array(
				'silver'=> $helpDeskSilver,
				'gold' => $helpDeskGold,
				'platinum' => $helpDeskPlatinum,
			),
		));
	}

	public function actionGetApps($action, $catName = false)
	{

		try{
			// Call the erp api to get all apps (unorganized)
			$apps  = $this->_getAllApps();
			if(!$apps) throw new CException('No apps available');

			// we'll distribute the apps from the response between the following arrays
			$installedApps = array();
			$freeApps = array();
			$paidApps = array();
			$featuredApps = array();

			$categories = array();

			if (is_array($apps)) {
				foreach ($apps as $id => $app) {

					// Check if this App is available as Yii module/plugin here
					$plugin_name =  ucfirst($app['internal_codename']) . "App";
					if (! PluginManager::isModulePlugin($plugin_name) ) {
						unset($apps[$id]); continue;
					}

					if ($app['is_installed'] == 1) {
						$installedApps[] = $app;
					}
					else if ($app['market_type'] == 'free') {
						$freeApps[] = $app;
					} else {
						$paidApps[] = $app;
					}

					if($app['is_featured']) {
						$featuredApps[] = $app;
					}

					// Also, split apps in categories
					// (in case we need such categorization in the future)
					if(!$categories[$app['category']]) $categories[$app['category']] = array();
					$categories[$app['category']][] = $app;
				}
			}


			$appsToInclude = array();
			switch($action){
				case 'myapps':
					$appsToInclude = $installedApps;
					break;
				case 'category':
					if($catName){
						// Filter only apps from this category
						foreach($apps as $app){
							if($app['category']==$catName)
								$appsToInclude[] = $app;
						}
					}else{
						// Something went wrong or category not specified.
						// Show all apps.
						$appsToInclude = $apps;
					}
					break;
				default:
			}

			$categoryTranslations = $this->_getAppCategoryTranslations();
			$categoryName = isset($categoryTranslations[$catName]) ? $categoryTranslations[$catName] : $catName;
			$isECSInstallation = PluginManager::isPluginActive('EcsApp');


            $html = $this->renderPartial('_apps_grid_new', array(
                'action' => $action, // myapps|category
                'apps'=>$appsToInclude,
                'featuredApps' => $featuredApps,
                'coverTile'=> ($action=='category' && $catName) ? $categoryName : Yii::t('apps', 'My<br> apps'),
				'isECSInstallation' => $isECSInstallation
            ), true);
        }catch(Exception $e){
            echo CJSON::encode(array(
                'html'  => $e->getMessage()
            ));
            Yii::app()->end();
        }

		echo CJSON::encode(array(
			'html'  => $html,
			'categories' => $categories,
		));
		Yii::app()->end();
	}

	/**
	 * Displays a dialog with details for a specific app
	 */
	public function actionAxAppDetails()
	{
		$appId = Yii::app()->request->getParam('id');

		$app = $this->_getAppDetails($appId);

		if(!$app){
			throw new CException('Can not retrieve app details', CLogger::LEVEL_ERROR);
		}

		$langData = $app['lang'];
		$userLang = Yii::app()->getLanguage();
		$lang = (isset($langData[$userLang])) ? $langData[$userLang] : $langData['en'];

		// Payment cycle for this App for this installation
		$cycle = "";
		if ($app['market_type'] != 'free') {
			if (strtolower($app['pricing']['payment_cycle']) == self::$CYCLE_MONTHLY)
				$cycle = "MONTH";
			elseif (strtolower($app['pricing']['payment_cycle']) == self::$CYCLE_YEARLY)
				$cycle = "YEAR";
			else
				throw new CException('Invalid payment cycle received for app details');
		}

		// Round the price to 2 decimal places so 5.23456 becomes 5.23
		if(isset($app['pricing']['price'])) {
			$app['pricing']['price'] = round($app['pricing']['price'], 2);
		}

		$isReseller = isset(Yii::app()->request->cookies['reseller_cookie']);
		$hasToContactResellerToInstall = (($app['market_type'] !== 'free') && ($app['use_contact_us'] == 1) && $isReseller === true);

		$install_detail = false;
		$plugin_name = $app['internal_codename'] . 'App';
		$plugin_file = _plugins_ . '/' . $plugin_name . '/' . $plugin_name .'.php';
		if (file_exists($plugin_file)) {
			include($plugin_file);
			$install_detail = call_user_func(array($plugin_name, 'installDetail'), $app, $lang, $hasToContactResellerToInstall, Docebo::isTrialPeriod(), $cycle);
		}

		$enableUserBilling = Settings::get('enable_user_billing', 'on');

		$showDiscoverMore = false;
		if($enableUserBilling != 'on'){
			$showDiscoverMore = true;
		}

		$isECSInstallation = PluginManager::isPluginActive('EcsApp');

		$this->renderPartial('_app_details', array(
			'app' => $app,
			'lang' => $lang,
			'hasToContactResellerToInstall' => $hasToContactResellerToInstall,
			'useContactus' => $app['use_contact_us'],
			'isInTrialPeriod' => Docebo::isTrialPeriod(),
			'cycle' => $cycle,
			'install_detail' => $install_detail,
			'showDiscoverMore' => $showDiscoverMore && $app['market_type'] != 'free' ? true : false,
			'isFree' => ($app['market_type'] == 'free'),
			'isECSInstallation' => $isECSInstallation,
			'isReseller' => $isReseller,
			'enableUserBilling' => $enableUserBilling
		));
	}

	/**
	 * Displays a Contact Us dialog. Usually triggered from the App Details dialog.
	 *
	 * @param int $app_id - If provided, the app ID from which this form was requested
	 * @param string $ref - The reason why the user wants to contact us (also set in the
	 *                      triggering/referring dialog)
	 *
	 * @TODO Right now we open the default sales dialog instead of this one,
	 * so maybe remove this method in the future?
	 */
	public function actionAxContactUs($app_id = null, $ref = null){

		$app = $this->_getAppDetails($app_id);

		$langData = $app['lang'];
		$userLang = Yii::app()->getLanguage();
		$lang = (isset($langData[$userLang])) ? $langData[$userLang] : $langData['en'];

		if($_POST['message']){
			// Form submitted, send email
		}

		$hasToContactResellerToInstall = ( !Docebo::isTrialPeriod() && $app['lms_payment_method'] == 'wire_transfer' );

		$this->renderPartial('_contact_us', array(
			'app' => $app,
			'ref' => $ref,
			'hasToContactResellerToInstall' => $hasToContactResellerToInstall,
			'isInTrialPeriod' => Docebo::isTrialPeriod(),
		));
	}

	/**
	 * Displays a modal asking a confirmation from the user
	 * if he wants to buy, start trial or install the selected app
	 */
	public function actionAxConfirmInstall($step = null, $confirm = null)
	{
		// get app details again
		$appId = Yii::app()->request->getParam('id');

		$userConfirmed = (1 === intval(Yii::app()->request->getParam('confirm', 0)));
		$confirmType = Yii::app()->request->getParam('type', 'install');

		if(!$step)
			$step = isset($_POST['step']) ? $_POST['step'] : 1;

		$app = $this->_getAppDetails($appId);

		if ($app['lang']['en']['title'] == 'E-Commerce')
		{
			if (Yii::app()->request->getParam('confirmedECommerceShopifyInstPolicy') == '1')
			{ }
			else {
				if (count(CorePlugin::model()->findAll("plugin_name = 'ShopifyApp' and is_active = 1")) == 1) {
					$this->renderPartial('_acceptECommerceShopifyInstPolicy', array(
						'message' => Yii::t('shopify', 'By activating E-Commerce APP the currently used Shopify APP will be disabled.'),
						'activatedApplication' => 'E-Commerce',
						'appId' => $appId,
					));
					Yii::app()->end();
				}
			}
		}
		else
			if ($app['lang']['en']['title'] == 'Shopify')
			{
				if (Yii::app()->request->getParam('confirmedECommerceShopifyInstPolicy') == '1')
				{ }
				else {
					if (count(CorePlugin::model()->findAll("plugin_name = 'EcommerceApp' and is_active = 1")) == 1) {
						$this->renderPartial('_acceptECommerceShopifyInstPolicy', array(
							'message' => Yii::t('shopify', 'By activating the Shopify APP, the currently used E-Commerce APP will be disabled. All courses already put on sale will be synchronized with your Shopify store.'),
							'activatedApplication' => 'Shopify',
							'appId' => $appId,
						));
						Yii::app()->end();
					}
				}
			}



		if($app['is_enterprise']
			&& !$userConfirmed // means the user has accepted all terms and pricing
		){
			// Enterprise solution app special case

			switch($step){
				case 2:
					// App specific TOS is confirmed? Check:
					if(!isset($_POST['tos_plan_linked']) || !isset($_POST['tos_activate_days'])
						|| !isset($_POST['tos_service_delay']) || !isset($_POST['tos_docebo_agree'])){

						$this->renderPartial('_enterprise_tos_accept', array(
							'id'=>$appId,
							'error' => Yii::t('apps', 'You must accept all terms of the Enterprise Cloud Solution app'),
						));
						Yii::app()->end();
					}
					// All is fine, continue with regular buy/activate procedure
					break;
				case 1: default:
				// Show app specific TOS the user needs to accept
				$this->renderPartial('_enterprise_tos_accept', array(
					'id'=>$appId,
				));
				Yii::app()->end();
				break;
			}
		}

		$langData = $app['lang'];
		$userLang = Yii::app()->getLanguage();
		$lang = (isset($langData[$userLang])) ? $langData[$userLang] : $langData['en'];

		if ($userConfirmed || $confirmType=='install') {

			/*
			 * then activate the app (plugin)
			 */
			$res = PluginManager::activateAppByCodename($app['internal_codename']);

			if($res){
				/*
				 * call the erp api to install the app
				 */
				$apiParams = array(
					'installation_id' => Docebo::getErpInstallationId(),
					'app_id' => $appId,
					'reseller_cookie' => isset(Yii::app()->request->cookies['reseller_cookie']) ? Yii::app()->request->cookies['reseller_cookie'] : false
				);
				$apiRes = AppsMpApiClient::apiAddApplication($apiParams);
				$apiRes = CJSON::decode($apiRes);


				//install also dependent apps returned from api
				if (isset($apiRes['dependencies']) && is_array($apiRes['dependencies']))
				{
					foreach ($apiRes['dependencies'] as $appCodename)
					{
						PluginManager::activateAppByCodename($appCodename);
					}
				}
			}

			$this->renderPartial('_app_installed', array(
				'success' => $res,
				'title' => $lang['title']
			));

			// Schedule the page to be reloaded when the dialog is closed (see views/app/index.php)
			echo '<span class="reload-after-dialog-close"></span>';
		} else {
			/*
			 * display a different popup for 'buy now' / 'start trial' / 'install now'
			 */

			switch ($confirmType) {
				case 'buy':
					$userBillingEnabled = Settings::get('enable_user_billing', 'off');

					$termsLink =
						($app['lang']['en']['title'] == 'E-Commerce')
							? null
							:
								(
									($app['lang']['en']['title'] == 'Shopify')
										? 'https://www.shopify.com/legal/terms'
										: null
								);
					$this->renderPartial('_confirm_buy', array(
						'app' => $app,
						'title' => $lang['title'],
						'userBillingEnabled' => $userBillingEnabled,
						'termsAndConditionsLink' => $termsLink,
					));
					break;
				case 'trial':
					$this->renderPartial('_confirm_trial', array(
						'app' => $app,
						'title' => $lang['title']
					));
					break;
			}
		}
	}

	public function actionAxMoocConfirmInstall() {

		// get app details again
		$appId = Yii::app()->request->getParam('id');

		$userConfirmed = (1 === intval(Yii::app()->request->getParam('confirm', 0)));

		$public_catalog    = Yii::app()->request->getParam('public_catalog', false);
		$self_registration = Yii::app()->request->getParam('self_registration', false);
		$ecommerce         = Yii::app()->request->getParam('ecommerce', false);
		$gamification      = Yii::app()->request->getParam('gamification', false);

		$app = $this->_getAppDetails($appId);

		$langData = $app['lang'];
		$userLang = Yii::app()->getLanguage();
		$lang = (isset($langData[$userLang])) ? $langData[$userLang] : $langData['en'];

		// if user had confirmed of no paid app have been selected, install
		if ($userConfirmed || (!$ecommerce && !$gamification) ) {

			/*
			 * call the erp api to install the app
			 */
			$apiParams = array(
				'installation_id' => Docebo::getErpInstallationId(),
				'app_id' => $appId,
				'reseller_cookie' => isset(Yii::app()->request->cookies['reseller_cookie']) ? Yii::app()->request->cookies['reseller_cookie'] : false
			);
			$apiRes = AppsMpApiClient::apiAddApplication($apiParams);
			$apiRes = CJSON::decode($apiRes);

			/*
			 * then activate the app (plugin)
			 */
			$res = PluginManager::activateAppByCodename($app['internal_codename']);

			$apps  = $this->_getAllApps();
			if(!$apps){
				echo CJSON::encode(array('html'  => 'Can not retrieve apps'));
				Yii::app()->end();
			}

			$apps_id = array();
			foreach ($apps as $id => $app) {

				// Check if this App is available as Yii module/plugin here
				$plugin_name =  ucfirst($app['internal_codename']) . "App";
				$apps_id[$plugin_name] = $id;
			}

			// Now based on the seleciton above we have to perform some activation as well
			if (($public_catalog || $ecommerce) && !PluginManager::isPluginActive('CoursecatalogApp')) {

				// find app id and activate it
				PluginManager::activateErpAndLocalAppByCodename($apps_id['CoursecatalogApp'], 'Coursecatalog');

				// do some custom setup
				Settings::save('catalog_enabled', '1', 'int', 1);
				Settings::save('catalog_type', 'catalog', 'string', 255);
				Settings::save('catalog_external', 'on', 'enum', 3);
			}
			if ($ecommerce && !PluginManager::isPluginActive('EcommerceApp')) {

				// find app id and activate it
				PluginManager::activateErpAndLocalAppByCodename($apps_id['EcommerceApp'], 'Ecommerce');
			}
			if ($gamification && !PluginManager::isPluginActive('GamificationApp')) {

				// find app id and activate it
				PluginManager::activateErpAndLocalAppByCodename($apps_id['GamificationApp'], 'Gamification');
			}

			// custom setup for self registration
			if ($self_registration) {

				Settings::update('register_type', 'moderate');
				Settings::update('privacy_policy', 'on');
			}

			$this->renderPartial('_app_installed', array(
				'title' => $lang['title']
			));

			// Schedule the page to be reloaded when the dialog is closed (see views/app/index.php)
			echo '<span class="reload-after-dialog-close"></span>';
		} else {

			/*
			 * display a different popup for 'buy now' / 'start trial' / 'install now'
			 */
			$extra_param = array();
			if ($public_catalog) $extra_param['public_catalog'] = 1;
			if ($self_registration) $extra_param['self_registration'] = 1;
			if ($ecommerce) $extra_param['ecommerce'] = 1;
			if ($gamification) $extra_param['gamification'] = 1;

			if (Docebo::isTrialPeriod()) {

				$this->renderPartial('_confirm_trial', array(
					'extra_param' => $extra_param,
					'custom_url' => 'app/axMoocConfirmInstall',
					'app' => $app,
					'title' => $lang['title']
				));
			} else {
				$userBillingEnabled = Settings::get('enable_user_billing', 'off');

				$this->renderPartial('_confirm_buy', array(
					'extra_param' => $extra_param,
					'custom_url' => 'app/axMoocConfirmInstall',
					'app' => $app,
					'title' => $lang['title'],
					'userBillingEnabled' => $userBillingEnabled
				));
			}

		}
	}

	public function actionAxRemoveApp()
	{
		// get app details again
		$appId = Yii::app()->request->getParam('id');
		$app = $this->_getAppDetails($appId);
		$langData = $app['lang'];
		$userLang = Yii::app()->getLanguage();
		$lang = (isset($langData[$userLang])) ? $langData[$userLang] : $langData['en'];

		$userConfirmed = (1 === intval(Yii::app()->request->getParam('confirm', 0)));

		if ($userConfirmed) {
			if(!Yii::app()->request->isAjaxRequest)
				Yii::app()->end();

			// call the api to remove this app for the current installation
			$apiParams = array(
				'installation_id' => Docebo::getErpInstallationId(),
				'app_id' => $appId
			);

			// Deactivate it only
			// 1. if it's != subscription, because it will be deactivated once it expires
			// 2. LMS is in trial
			//if ( ($appDetails['market_type'] != 'subscription') || Docebo::isTrialPeriod())
			//{
				// Run plugin's "deactivate" method
				$res = PluginManager::deactivateAppByCodename($app['internal_codename']);
			//}


			/**
			 * The order of calling this methods is changed, because we add check
			 * if the current plugin can be deactivated or not!!!
			 */

			if($res){
				// Deactivate/remove/cancel application in ERP
				$apiRes = AppsMpApiClient::apiAppCancel($apiParams);

				$appDetails = $this->_getAppDetails($appId);

				if($app['internal_codename']=='customdomain'){
					// When the Custom Domain app is deactivated and enters a period where
					// we can reactivate it, we need to have at least one DB param
					// to mark it as such. 'module_customdomain' is the one we use.
					// @TODO Figure out a better way to handle these (maybe in the app/plugin itself?)
					Settings::save('module_customdomain', 'off');
				}
			}
			// @TODO Do some checks of the API and Plugin responses and handle errors

			$this->renderPartial('_app_removed', array(
				'success' => $res,
				'title' => $lang['title']
			));

			// Schedule the page to be reloaded when the dialog is closed (see views/app/index.php)
			echo '<span class="reload-after-dialog-close"></span>';

		} else {
			$this->renderPartial('_confirm_remove', array(
				'app' => $app,
				'title' => $lang['title']
			));
		}
	}

	public function actionAxRemoveDependency()
	{
		// get app details again
		$appId = Yii::app()->request->getParam('id');
		$app = $this->_getAppDetails($appId);
		$langData = $app['lang'];
		$userLang = Yii::app()->getLanguage();
		$lang = (isset($langData[$userLang])) ? $langData[$userLang] : $langData['en'];

		$this->renderPartial('_dependency_info', array(
			'app' => $app,
			'title' => $lang['title']
		));
	}

	public function actionAxReactivateApp()
	{
		// get app details again
		$appId = Yii::app()->request->getParam('id');
		$app = $this->_getAppDetails($appId);
		$langData = $app['lang'];
		$userLang = Yii::app()->getLanguage();
		$lang = (isset($langData[$userLang])) ? $langData[$userLang] : $langData['en'];

		// call the api to remove this app for the current installation
		$apiParams = array(
			'installation_id' => Docebo::getErpInstallationId(),
			'app_id' => $appId
		);

		// Run plugin's "activate" method
		$res = PluginManager::activateAppByCodename($app['internal_codename']);

		if($res){
			// Reactivate application in ERP
			$apiRes = AppsMpApiClient::apiAppReactivate($apiParams);
		}

		// @TODO Do some checks of the API and Plugin responses

		$this->renderPartial('_app_reactivated', array(
			'title' => $lang['title']
		));

		// Schedule the page to be reloaded when the dialog is closed (see views/app/index.php)
		echo '<span class="reload-after-dialog-close"></span>';
	}


	public function actionAxReactivateOneTimeApp()
	{
		// get app details again
		$appId = Yii::app()->request->getParam('id');
		$app = $this->_getAppDetails($appId);
		$langData = $app['lang'];
		$userLang = Yii::app()->getLanguage();
		$lang = (isset($langData[$userLang])) ? $langData[$userLang] : $langData['en'];

		// call the api to remove this app for the current installation
		$apiParams = array(
			'installation_id' => Docebo::getErpInstallationId(),
			'app_id' => $appId
		);


		// Reactivate application in ERP
		$apiRes = AppsMpApiClient::apiAppReactivateOneTimeApp($apiParams);

		// Run plugin's "activate" method
		$res = PluginManager::activateAppByCodename($app['internal_codename']);

		// @TODO Do some checks of the API and Plugin responses

		$this->renderPartial('_app_reactivated', array(
			'title' => $lang['title']
		));

		// Schedule the page to be reloaded when the dialog is closed (see views/app/index.php)
		echo '<span class="reload-after-dialog-close"></span>';
	}

	/**
	 * Makes an api call to get an app's details
	 *
	 * @param $appId
	 * @return array|bool
	 */
	protected function _getAppDetails($appId)
	{
		$params = array(
			'installation_id' => Docebo::getErpInstallationId(),
			'app_id' => $appId,
			'lang' => Yii::app()->getLanguage(),
//			'ref' => isset(Yii::app()->request->cookies['reseller_cookie'])? Yii::app()->request->cookies['reseller_cookie']->value : false
		);
		$apiResponse = AppsMpApiClient::apiGetAppDetails($params);
		$response = CJSON::decode($apiResponse);

		//Yii::log(print_r($response,true),'debug', __CLASS__);

		if (!empty($response) && isset($response['success']) && $response['success']===true) {
			return $response['data'];
		}

		return false;
	}

	/**
	 * Makes an api call to retrieve all apps
	 *
	 * @param string $filter - Possible values all|free|paid|my_apps
	 * @return array|bool
	 */
	protected function _getAllApps($filter = 'all')
	{
		$params = array(
			'installation_id' => Docebo::getErpInstallationId(),
			'filter' => $filter,
			'lang' => Yii::app()->getLanguage(),
//			'ref' => isset(Yii::app()->request->cookies['reseller_cookie'])? Yii::app()->request->cookies['reseller_cookie']->value : false
		);

		$apiResponse = false;
		if($filter=='all'){
			// When all apps are requested, cache them for the current execution
			static $cache = null;
			if($cache===null){
				$apiResponse = AppsMpApiClient::apiListAvailableApps($params);
				if($apiResponse)
					$cache = $apiResponse;
			}
		}else{
			// If filtering is used, disable cache
			$apiResponse = AppsMpApiClient::apiListAvailableApps($params);
		}
		$response = CJSON::decode($apiResponse);

		if(!$response){
			Yii::log('Cannot retrieve apps from ERP. Invalid JSON response. (' . $apiResponse . ')', CLogger::LEVEL_ERROR);
		}else{
			if(isset($response['success']) && $response['success']===true) {
				$apps = $response['data'];
				if(!empty($apps)) {

					// Check if we are an ECS
					$ecsInstalled = false;
					foreach($apps as $singleApp) {
						if($singleApp['is_enterprise'] && $singleApp['is_installed']) {
							$ecsInstalled = true;
							break;
						}
					}

					// If current LMS is not ECS, filter out some well-known ECS-only apps
					$ecsOnlyApps = array(); // empty array for now
					if(!empty($ecsOnlyApps) && !$ecsInstalled) {
						$filteredApps = array();
						foreach($apps as $id => $app) {
							$plugin_name =  ucfirst($app['internal_codename']) . "App";
							if(!in_array($plugin_name, $ecsOnlyApps))
								$filteredApps[$id] = $app;
						}

						return $filteredApps;
					} else
						return $apps;
				} else
					return FALSE;
			} else
				Yii::log('ERP apps api failed with message: '. isset($response['msg']) ? $response['msg'] : null, CLogger::LEVEL_ERROR);
		}

		return false;
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
