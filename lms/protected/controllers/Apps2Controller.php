<?php

class Apps2Controller extends Controller
{

	const CYCLE_MONTHLY            = 'monthly';
	const CYCLE_YEARLY             = 'yearly';
	
	const PLAN_STARTER             = 'starter';
	const PLAN_ADVANCED            = 'advanced';
	const PLAN_ENTERPRISE          = 'enterprise';
	const PLAN_ENTERPRISE_PLUS     = 'enterprise_plus';
	
	public static $appGroups = [
	    self::PLAN_STARTER => 'Starter',
	    self::PLAN_ADVANCED => 'Advanced',
	    self::PLAN_ENTERPRISE => 'Enterprise',
	    self::PLAN_ENTERPRISE_PLUS => 'Enterprise Plus',
	];
	
	
	protected $lmsComboInfo;
	

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$res = array();


		// keep it in the following order:

		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'expression' => 'BrandingWhiteLabelForm::isMenuVisible()'
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
			'deniedCallback' => array($this, 'accessDeniedCallback'),
		);

		return $res;
	}

	public function actionIndex(){
	    
	    // Comment this block of code and will get the OLD behavior completely!
	    $api = new ErpApiClient2();
	    $isBundledPricing = $api->call('lms/is-bundle-pricing');
	    if (!$isBundledPricing) {
	        $this->forward("app/index", true);
	        Yii::app()->end();
	    }
	     
	    
		Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/myapps2-search.js');
		$isInTrialPeriod = Docebo::isTrialPeriod();
		$this->render('index', array(
			'isInTrialPeriod' => $isInTrialPeriod,
		    'pricingPlan'     => $this->resolvePricingPlan(),
		));
	}

	/**
	 * Get all apps and send them back, in a form suitable for "searching" client side
	 *
	 * @throws CException
	 */
	public function actionGetAllApps(){
		
	    // Get all, unfiltered apps
	    $apps  = $this->getApps();
		
		$isECSInstallation = PluginManager::isPluginActive('EcsApp');
		foreach($apps as $key => $app){
			$appsData[] = array(
				'name'              => $app['title'],
				'description'       => $app['short_description'],
				'app'               => $app,
				'isECSInstallation' => $isECSInstallation,
			    'body'              => $this->renderPartial('_app_template', array(
			        'app' => $app,
			        'isECSInstallation' => $isECSInstallation
			    ), true),
			);
		}

		$response = new AjaxResult();
		$response->setStatus(true)->setData($appsData)->toJSON();
		
		Yii::app()->end();
		
	}

	/*
	 * These will be displayed in the left sidebar in the Apps page.
	 * Note that the order here is important! It will be preserved during display.
	 */
	private function _getAppCategoryTranslations(){
		return array(
			'docebo_additional_features' => Yii::t('app', 'Docebo Additional Features'),
			'third_party_integrations' => Yii::t('app', 'Third party integrations'),
			'web_conferencing' => Yii::t('menu_course', '_TELESKILL_ROOM'),
			'single_sing_on' => Yii::t('app', 'Single Sign On'),
		);
	}
	

	/**
	 * 
	 */
	public function actionGetAppCategories(){

		$categoryTranslations = $this->_getAppCategoryTranslations();

		// [category]=>[css class] pair. The CSS class will be
		// applied to the <a> link of the category in the Sidebarred widget
		// E.g. 'single_sing_on'=>'three-dots'
		// will apply the following CSS to the category 'single_sign_on':
		// .sidebarred .sidebar ul li a.three-dots span{}
		// See shared.css for the available classes
		$categoryIcons = array(
			'single_sing_on' => 'lock',
			'docebo_additional_features' => 'bullet-list',
			'third_party_integrations' => 'chain',
			'web_conferencing' => 'webcam',
		);

		$categories = array();

		// Add 'My apps' as the first category
		$categories[Yii::t('apps', 'My<br> apps')] = CHtml::link('<span></span>'.Yii::t('apps', 'My<br> apps'), '#myapps', array(
			'data-loaded' => 0,
			'class'=>'myapps category tiles myappsitem',
			'data-tab' => 'myapps',
			'data-url' => 'apps2/getApps&action=myapps',
		));

		foreach($categoryTranslations as $key=>$categoryName){

			$categories[$categoryName] = CHtml::link('<span></span>'.$categoryName, '#'. $key, array(
				'id' => $key.'_tab_btn',
				'data-loaded' => 0,
				'class'=>'myappsitem category '.$key . ($categoryIcons[$key] ? ' '.$categoryIcons[$key] : ''),
				'data-tab' => $key,
				'data-url' => 'apps2/getApps&action=category&catName='.trim($key),
			));
		}

		echo CJSON::encode($categories);
		Yii::app()->end();
	}


    /**
     * Return rendered HTML of Apps, based on "myapps" or "category" clicked
     * 
     * @param string $action
     * @param string $catName
     * @throws CException
     */
	public function actionGetApps($action, $catName = false)
	{

		try {
			// Call the erp api to get all apps (unorganized)
			$apps  = $this->getApps();
			
			if(!$apps) 
			    throw new CException('No apps available');

			// Those having is_active=1
			$installedApps = array();
			
			// Those having is_available=1
			$availableApps = array();
			
			// Featured Apps, Free Apps, etc.
			$featuredApps = array();
			$freeApps = array();

			$categories = array();

			if (is_array($apps)) {
				foreach ($apps as $codeName => $app) {

					// Check if this App is available as Yii module/plugin here
					$plugin_name =  ucfirst($codeName) . "App";
					if (!PluginManager::isModulePlugin($plugin_name)) {
						unset($apps[$id]); continue;
					}

					if ($app['is_active'] == 1) {
						$installedApps[] = $app;
					}

					if($app['is_available'] == 1) {
						$availableApps[] = $app;
					}
					
					if(!$app['is_active'] && isset($app['is_featured']) && $app['is_featured']) {
                        $featuredApps[] = $app;
					}
						
					if (isset($app['market_type']) && $app['market_type'] == 'free') {
					    $freeApps[] = $app;
					}

					// Add app to a category array
					if(!isset($categories[$app['category']])) { 
					    $categories[$app['category']] = array();
					}
					$categories[$app['category']][] = $app;
				}
			}


			$appsToInclude = array();
			switch($action){
				case 'myapps':
					$appsToInclude = $installedApps;
					break;
				case 'category':
					if($catName){
						foreach($apps as $app){
							if($app['category']==$catName)
								$appsToInclude[] = $app;
						}
					} else {
						// Something went wrong or category not specified.
						// Show all apps.
						$appsToInclude = $apps;
					}
					break;
				default:
			}

			$categoryTranslations = $this->_getAppCategoryTranslations();
			$categoryName = isset($categoryTranslations[$catName]) ? $categoryTranslations[$catName] : $catName;
			$isECSInstallation = PluginManager::isPluginActive('EcsApp');

			// Shuffle and pick first 3
			shuffle($featuredApps);
			
			$featuredApps = array_slice($featuredApps,0, 3);

            $html = $this->renderPartial('_apps_grid_new', array(
                'action'            => $action, // myapps|category
                'apps'              => $appsToInclude,
                'featuredApps'      => $featuredApps,
                'coverTile'         => ($action=='category' && $catName) ? $categoryName : Yii::t('apps', 'My<br> apps'),
				'isECSInstallation' => $isECSInstallation
            ), true);
            
        } catch (Exception $e){
            echo CJSON::encode(array(
                'html'  => $e->getMessage()
            ));
            Yii::app()->end();
        }

		echo CJSON::encode(array(
			'html'  => $html,
			'categories' => $categories,
		));
		
		Yii::app()->end();
	}

	/**
	 * Displays a dialog with details for a specific app
	 */
	public function actionAxAppDetails()
	{
		$codeName = Yii::app()->request->getParam('code', false);
		$action   = Yii::app()->request->getParam('action', false);

		$app = $this->getApp($codeName, true);

		$allowedAction = $this->allowedAction($app);
		
		// Someone is trying to do something not allowed??
		if ($allowedAction != $action) {
		    $this->forward('actionNotAllowed');
		}

		
		$install_detail = false;
		$plugin_name = $app['internal_codename'] . 'App';
		$plugin_file = _plugins_ . '/' . $plugin_name . '/' . $plugin_name .'.php';
		if (file_exists($plugin_file)) {
		    include($plugin_file);
		    $install_detail = call_user_func(array($plugin_name, 'installDetail'), $app, false, false, false, false);
		}
		
		$this->renderPartial('_app_details', array(
		    'action'  => $action, 
			'app'    => $app,
		    'install_detail' => $install_detail,
		));
	}

	/**
	 * Displays a Contact Us dialog. Usually triggered from the App Details dialog.
	 *
	 * @param int $app_id - If provided, the app ID from which this form was requested
	 * @param string $ref - The reason why the user wants to contact us (also set in the
	 *                      triggering/referring dialog)
	 *
	 * @TODO Right now we open the default sales dialog instead of this one,
	 * so maybe remove this method in the future?
	 */
	public function actionAxContactUs(){

	    $code                     = Yii::app()->request->getParam('code', false);
	    $action                   = Yii::app()->request->getParam('action', false);
	     
		$app = $this->getApp($code, true);
		
		if ($action != $this->allowedAction($app)) {
		    $this->forward('actionNotAllowed');
		}

		$this->renderPartial('_contact_us', array(
			'app' => $app,
		));
	}

	/**
	 * Displays a modal asking a confirmation from the user
	 * if he wants to buy, start trial or install the selected app
	 */
	public function actionAxConfirmInstall()
	{
		$code                     = Yii::app()->request->getParam('code', false);
		$action                   = Yii::app()->request->getParam('action', false); 
		$confirm                  = (bool) Yii::app()->request->getParam('confirm', false);
		$confirmShoplifyPolicy    = (bool) Yii::app()->request->getParam('confirmedECommerceShopifyInstPolicy', false);

		$app = $this->getApp($code, true);
		
		$this->allowedAction($app);
		
		$isInTrialPeriod   = Docebo::isTrialPeriod();
		
		if ($action != $this->allowedAction($app)) {
		    $this->forward('actionNotAllowed');
		}

		if ($code == 'ecommerce')
		{
			if (!$confirmShoplifyPolicy) {
			     
				if (PluginManager::isPluginActive('ShopifyApp')) {
					$this->renderPartial('_acceptECommerceShopifyInstPolicy', array(
						'message' => Yii::t('shopify', 'By activating E-Commerce APP the currently used Shopify APP will be disabled.'),
						'activatedApplication' => 'E-Commerce',
						'code' => $code,
					));
					Yii::app()->end();
				}
			}
		}
		else
			if ($code == 'Shopify')
			{
				if (Yii::app()->request->getParam('confirmedECommerceShopifyInstPolicy') == '1')
				{ }
				else {
					if (count(CorePlugin::model()->findAll("plugin_name = 'EcommerceApp' and is_active = 1")) == 1) {
						$this->renderPartial('_acceptECommerceShopifyInstPolicy', array(
							'message' => Yii::t('shopify', 'By activating the Shopify APP, the currently used E-Commerce APP will be disabled. All courses already put on sale will be synchronized with your Shopify store.'),
							'activatedApplication' => 'Shopify',
							'code' => $code,
						));
						Yii::app()->end();
					}
				}
			}


        // If LMS is in trial and TRY-FOR-FREE is requested (and aknowledged before as possible action) .. allow activation
		$trialActivation = $isInTrialPeriod && ($action == Apps2Widget::ACTION_TRY_FOR_FREE);

		// Of course, user should confirm...   
		if (($action == Apps2Widget::ACTION_ACTIVATE) || $trialActivation) {

			$res = PluginManager::activateAppByCodename($app['internal_codename']);

			if($res){
			    $resellerCookie = isset(Yii::app()->request->cookies['reseller_cookie']) ? Yii::app()->request->cookies['reseller_cookie'] : false;
			    $api = new ErpApiClient2();
			    $api->params = array(
			        'internal_codename'      => $code,
                    'reseller_cookie'        => $resellerCookie,
			    );
			    
			    // NOTE: ERP does not activate App in Trial mode yet
			    $api->call('lms/activate-app');
			    
				//install also dependent apps returned from api
				if (isset($app['dependency_list']) && is_array($app['dependency_list']))
				{
					foreach ($app['dependency_list'] as $appCodename) {
					    $api->params = array(
					        'internal_codename'    => $appCodename,
					        'reseller_cookie'      => $resellerCookie,
					    );
					    $api->call('lms/activate-app');
						PluginManager::activateAppByCodename($appCodename);
					}
				}
			}

			$this->renderPartial('_app_installed', array(
				'success'   => $res,
				'title'     => $app['title']
			));

			// Schedule the page to be reloaded when the dialog is closed (see views/apps2/index.php)
			echo '<span class="reload-after-dialog-close"></span>';
		}
		
		
	}

	public function actionAxMoocConfirmInstall() {
	    
	    $code                     = Yii::app()->request->getParam('code', false);
	    $action                   = Yii::app()->request->getParam('action', false);
	    $confirm                  = (bool) Yii::app()->request->getParam('confirm', false);
	    
	    $moocApp = $this->getApp($code, true);
	    
	    if ($action != $this->allowedAction($moocApp)) {
	        $this->forward('actionNotAllowed');
	    }
	     
		$public_catalog    = Yii::app()->request->getParam('public_catalog', false);
		$self_registration = Yii::app()->request->getParam('self_registration', false);
		$ecommerce         = Yii::app()->request->getParam('ecommerce', false);
		$gamification      = Yii::app()->request->getParam('gamification', false);

		
		if (true) {
		    
		    $api = new ErpApiClient2();
		    $api->params = array(
		        'internal_codename'   => $code,
		    );
		    // $api->call('lms/activate-app');
		    
			// PluginManager::activateAppByCodename($code);

			// $apps  = $this->getApps();

			$toActivate = array();
			
			if (($public_catalog || $ecommerce) && !PluginManager::isPluginActive('CoursecatalogApp')) {
			    $toActivate[] = 'coursecatalog';
				//Settings::save('catalog_enabled', '1', 'int', 1);
				//Settings::save('catalog_type', 'catalog', 'string', 255);
				//Settings::save('catalog_external', 'on', 'enum', 3);
			}
			
			if ($ecommerce && !PluginManager::isPluginActive('EcommerceApp')) {
			    //$toActivate[] = 'ecommerce';
			}
			
			if ($gamification && !PluginManager::isPluginActive('GamificationApp')) {
			    //$toActivate[] = 'gamification';
			}

			if ($self_registration) {
				//Settings::update('register_type', 'moderate');
				//Settings::update('privacy_policy', 'on');
			}

			if (!empty($toActivate)) {
			    foreach ($toActivate as $codeNameToActivate) {
			        // Log::_($codeNameToActivate);
			    }
			}
			
			$this->renderPartial('_app_installed', array(
				'title'     => $moocApp['title'],
			    'success'   => true,
			));

			// Schedule the page to be reloaded when the dialog is closed (see views/apps2/index.php)
			echo '<span class="reload-after-dialog-close"></span>';
			
		} 
		
	}

	public function actionAxRemoveApp()
	{
	    $code                     = Yii::app()->request->getParam('code', false);
	    $confirm                  = (bool) Yii::app()->request->getParam('confirm', false);
	    
	    $app = $this->getApp($code);
	    
	    if ($this->allowedAction($app) !== Apps2Widget::ACTION_DEACTIVATE) {
	        $this->forward('actionNotAllowed');
	    }
	    
		if ($confirm) {
		    $api = new ErpApiClient2();
		    $api->params = array(
		        'internal_codename'  => $code,
		        'reseller_cookie' => isset(Yii::app()->request->cookies['reseller_cookie']) ? Yii::app()->request->cookies['reseller_cookie'] : false
		    );
		    
		    $res = PluginManager::deactivateAppByCodename($code);
		    if ($res && $code == 'customdomain') {
		        Settings::save('module_customdomain', 'off');
		    }
		    if ($res) {
		        $apiRes = $api->call('lms/deactivate-app');
		        if ($apiRes === null) {
		        }
		    }
			$this->renderPartial('_app_removed', array(
				'success'   => $res,
				'title'     => $app['title']
			));

			// Schedule the page to be reloaded when the dialog is closed (see views/apps2/index.php)
			echo '<span class="reload-after-dialog-close"></span>';

		} else {
		    
		    if (!empty($app['required_by'])) {
		        $this->renderPartial('_dependency_info', array(
                    'app' => $app,
		        ));
		    }
            else {		    
                $this->renderPartial('_confirm_remove', array(
                    'app' => $app,
                ));
            }
		}
	}


	/**
	 * Makes an api call to get an app's details
	 *
	 * @param $appId
	 * @return array|bool
	 */
	protected function getApp($codeName, $single=false)
	{
	    // Caching (script-wide)
	    static $cache = null;
	    $cacheKey = md5(serialize(func_get_args()));
	    if (isset($cache[$cacheKey])) {
	        return $cache[$cacheKey];
	    }
	     
	    
	    if ($single) {
            $api = new ErpApiClient2();
            $api->params = array(
                'internal_codename' => $codeName,
            );
            $app = $api->call('lms/get-app');
            if ($app) {
                $apps = array($app);
                $this->setAppImageUrl($apps);
                $app = $apps[0];
            }
	    }
	    else {
	        // Yes, we get all again, because we need to reveal the dependency relations! (setting required-by attribute of all Apps)
	        // So far, we don't get this info from ERP
	        $apps = $this->getApps();
	        $app = $apps[$codeName];
	    }
	    
	    // Set "cache"
	    $cache[$cacheKey] = $app;
	     
	    return $app;
	    
	}

	/**
	 * Get filetered list of apps
	 *  
	 * @param string $forPlans
	 * @param string $myOnly
	 * 
	 * @throws CException
	 * @return array
	 */
	protected function getApps($forPlans=false, $myOnly=false, $categories=false) {

	    // Caching (script-wide)
	    static $cache = null;
	    $cacheKey = md5(serialize(func_get_args()));
	    if (isset($cache[$cacheKey])) {
	        return $cache[$cacheKey];
	    }
	    
	    $api = new ErpApiClient2();
	    
	    // All available Apps from ERP
	    $api->params = array('language' => Yii::app()->getLanguage());
	    $fullList = $api->call('lms/get-all-apps');

	    // Handle possible error
	    if (!is_array($fullList)) {
	        throw new CException('Unable to retrieve Apps list from ERP');
	    }
	    
	    // Make sure some parameters are arrays
	    if ($forPlans !== false && !is_array($forPlans)) {
	        $forPlans = array($forPlans);
	    }

	    if ($categories !== false && !is_array($categories)) {
	        $categories = array($categories);
	    }
	     
	    
	    
	    // Prepare the resulting array
	    $result = array();
	    foreach ($fullList as $app) {
	        
	        // By Default, get the app.. 
	        $ok = true;
	        
	        // My Apps only?
	        if ($myOnly && !$app['is_active']) {
	           $ok = false;
	           
	        }
	        // Available in a give plan or plans only?
	        if (is_array($forPlans) && !in_array(strtolower($app['available_in_plan']), $forPlans)) {
	            $ok = false;
	        }

	        // Being part of a given category/categories?
	        if (is_array($categories) && !in_array(strtolower($app['category']), $categories)) {
	            $ok = false;
	        }
	         
	        // Good, if it is Ok, get it..
	        if ($ok) {
	            $app['allowed_action'] = $this->allowedAction($app);
	            $result[$app['internal_codename']] = $app;
	        }
	        
	    }

	    if (!empty($result)) {
	        $this->setRequiredByAttribute($result);
	        $this->setAppImageUrl($result);
	    }
	    
	    // Set "cache"
	    $cache[$cacheKey] = $result;
	    
	    return $result;
	    
	}
	
	
	protected function allowedAction($app) {
	    
	    $isInstalled       = (bool) $app['is_active'];
	    $isInTrialPeriod   = Docebo::isTrialPeriod();
	    $isAvailable       = (bool) $app['is_available'];
	    $availableInPlan   = $app['available_in_plan'];
	    $isContactUsOnly   = (bool) $app['use_contact_us'];
	    $enableUserBilling = Settings::get('enable_user_billing', 'on') === 'on';
	    $isReseller        = isset(Yii::app()->request->cookies['reseller_cookie']);
	    
	    if ($isInstalled) {
            return Apps2Widget::ACTION_DEACTIVATE;	        
	    }
	    
	    if ($isContactUsOnly) {
	        if ($enableUserBilling) {
	            return Apps2Widget::ACTION_CONTACT_US;
	        }
	        else {
	            if ($isReseller) {
	                return Apps2Widget::ACTION_CONTACT_US;
	            }
	            else {
	                return Apps2Widget::ACTION_DISCOVER_MORE;
	            }
	        }
	    }
	    else {
	        if ($isInTrialPeriod) {
	            return Apps2Widget::ACTION_TRY_FOR_FREE;
	        }
	        else {
	            if ($isAvailable) {
	                return Apps2Widget::ACTION_ACTIVATE;
	            }
	            else {
	                if ($enableUserBilling) {
	                    return Apps2Widget::ACTION_UPGRADE;
	                }
	                else {
	                    if ($isReseller) {
	                        return Apps2Widget::ACTION_UPGRADE;
	                    }
	                    else {
	                        return Apps2Widget::ACTION_DISCOVER_MORE;
	                    }
	                }
	            }
	        }
	    }
	     
	    
	}
	
	
	protected function setRequiredByAttribute(&$apps) {
	    
	    foreach ($apps as $c => $a) {
	        $apps[$c]['required_by'] = array();
	    }
	    
	    reset($apps);
	    
	    foreach ($apps as $dependantAppCode => $app) {
	        if (is_array($app['dependency_list']) && $app['is_active']) {
	            foreach ($app['dependency_list'] as $requiredAppCode) {
                   $apps[$requiredAppCode]['required_by'][] = $dependantAppCode;
	            }
	        }
	    }
	    
	    
	}
	
	
	protected function setAppImageUrl(&$apps) {
	    
	    foreach ($apps as $c => $a) {
	        $apps[$c]['image_url'] = rtrim(Yii::app()->params['erp_url'], '/') . '/www/img/docebo_avatar.png';
	        $apps[$c]['image_url'] = false;
	    }
	     
	    reset($apps);
	     
	    foreach ($apps as $code => $app) {
	        if (isset($app['images']) && is_array($app['images'])) {
	            $apps[$code]['image_url'] = rtrim(Yii::app()->params['erp_url'], '/') . '/www/img/appImages/thumbs/' . $app['images'][0];
	        }
	    }
	     
	    
	}
	
	public function actionTest() {
	    
	    $data = $this->getApp('api');

	    var_dump($data);
	    
	}

	public function actionActionNotAllowed() {
	    $this->renderPartial('admin.protected.views.common/_dialog2_error', array(
	        'message'  => "<p><br><br>" . Yii::t('apps', 'This App status does not allow the requested operation! It might be not activated or disabled on ERP side') . "</p>",
	        'type'     => 'error',
	    ));
	    Yii::app()->end();
	}
	
	
	protected function getLmsInfo() {
	    
	    if ($this->lmsComboInfo) {
	        return $this->lmsComboInfo;
	    }
	    
	    $apiParams = array(
	        'installation_id'           => ErpApiClient::getInstallationId(),
	        'current_installation_only' => true
	    );
	    
	    if (isset(Yii::app()->request->cookies['reseller_cookie'])) {
	        $apiParams['request_from_partner'] = true;
	    }
	    
        $comboInfo = ErpApiClient::apiErpGetComboInfo($apiParams);
        $comboInfo = CJSON::decode($comboInfo);
        
	    $this->lmsComboInfo = $comboInfo;
	    
	    return $comboInfo;
	         
	}

	
	protected function resolvePricingPlan() {
	    
	    $info = $this->getLmsInfo();
	    
	    if (!is_array($info)) 
	        return false;
	    
	    $planInfo = isset($info['lms_order_info']['plan_info']) ? $info['lms_order_info']['plan_info'] : false;
	    
	    if (!$planInfo)
	        return false;
	    
	    if ((int) $planInfo['pricing_version'] != 2)
	        return false;
	        
        return Docebo::resolveLmsPricingPlan((int) $planInfo['default_ammount']);
            
	}
	
}
