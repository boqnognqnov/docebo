<?php

class CourseController extends Controller {

	/**
	 * (non-PHPdoc)
	 * @see CController::init()
	 */
	public function init() {
		parent::init();
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl',
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		return array(
			array('allow',
				'actions' => array("details", "axDetails", "axHoverDetails", "axCoursePreview", "axFilteredCoursesGrid", "axRate"),
				'users' => array('*'),
			),
			array('allow',
				'actions' => array('overbook', 'lockTest', 'autoreg',"deeplink"),
				'users' => array('@'),
			),
			array('allow',
				'actions' => array('subscribe', 'buy'),
				'expression' => array($this, 'checkCourseAccess'),
				'users' => array('@'),
			),
			// Allow power users to buy more seats to a course
			array(
				'allow',
				'actions'=>array('buyMoreSeats'),
				'expression' => ' (Yii::app()->user->getIsPu())',
			),
			array('deny',
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
			),
		);
	}


	public function checkCourseAccess(){

		$accessAllowed = true;
		$courseId = Yii::app()->request->getParam('course_id', false);
		if((!Yii::app()->user->getIsGodadmin()) && $courseId){
			$activePlugin = true;
			$userCatalogs = DashletCatalog::getUserAvailableCatalogs(false);
			if(!PluginManager::isPluginActive("CoursecatalogApp")){
				$activePlugin = false;
			}
			$showAllCoursesIfNoCatalogueIsAssigned = ('on' == Settings::get('on_catalogue_empty', 'on'));
			$subscribedCoursesList = CoreUser::model()->getSubscribedCoursesList(Yii::app()->user->getId(), TRUE);
			$coursesToNotDisplayList = LearningCourse::model()->getCoursesToNotDisplay($subscribedCoursesList);
			if(is_array($coursesToNotDisplayList) && in_array($courseId, $coursesToNotDisplayList)){
				$accessAllowed = false;
			}

			if(empty($userCatalogs) && $activePlugin && $showAllCoursesIfNoCatalogueIsAssigned && $accessAllowed){
				return $accessAllowed;
			}

			if(Yii::app()->user->getIsPu() && $accessAllowed){
				$accessAllowed = LearningCourseuser::checkUsersCourseAccess(array_keys($userCatalogs), $courseId, true, $activePlugin);

			}elseif(!Yii::app()->user->getIsPu() && $accessAllowed){
				if(empty($userCatalogs)){
					$accessAllowed = false;
				}else{
					$accessAllowed = LearningCourseuser::checkUsersCourseAccess(array_keys($userCatalogs), $courseId, false, $activePlugin);
				}
			}
		}
		return $accessAllowed;
	}

	/**
	 * AJAX called action to return HTML for Course Details popup dialog
	 *
	 */
	public function actionAxDetails() {
		if(Yii::app()->request->isAjaxRequest){
			// Get id from request
			$courseId = Yii::app()->request->getParam('id', null);

			// If true, does not display a Buy Now/Subscribe now button
			$showOnlyDetails = Yii::app()->request->getParam('only_details', false);

			// Validate request
			if (!$courseId) {
				if (Yii::app()->request->isAjaxRequest) {
					echo "Invalid course Id";
					Yii::app()->end();
				} else {
					throw new CHttpException(404, 'Invalid course Id');
				}
			}

			// Get a course model
			/**
			 * @var LearningCourse $courseModel
			 */
			$courseModel = LearningCourse::model()->findByPk($courseId);

			if (!$courseModel) {
				echo
					CHtml::tag('h1', array(), Yii::t('catalogue', '_NO_COURSE_FOUND')) .  // modal's title
					Yii::t('catalogue', '_NO_COURSE_FOUND'); // modal's content

				// or simply close the modal with no message
				//echo CHtml::link('', '', array('class' => 'auto-close'));

				Yii::app()->end();
			}

			// Get number of subscribers
			$subscribersCount = $courseModel->getTotalSubscribedUsers();

			// Language related data
			$langCode = $courseModel->lang_code; // Not browser code!!!
			$langName = Lang::getNameByCode($langCode);

			// Preview Ajax action URL (used in details dialog to load preview/demo html)
			$previewActionUrl = $this->createUrl('axCoursePreview', array('id' => $courseId));

			// Demo media type
			$demoType = $courseModel->getDemoMediaCategory();
			if ($demoType == LearningCourse::$DEMO_MEDIA_CATEGORY_UNKNOWN) {
				$demoFileName = $courseModel->course_demo;
				$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_COURSEDOCS);
				$previewActionUrl = $storageManager->fileUrl($demoFileName);
			}

			// Overbooking
			$isOverbooked = $courseModel->isOverbooked();

			$coursePolicy = $courseModel->getCoursePolicy();

			// Get additional course fields data here
			$additionalCourseFieldsModels = $courseModel->getCourseAdditionalFields(false, true);
			$additionalCourseFields = $this->getCourseFieldsToDisplay($courseModel, $additionalCourseFieldsModels, 'course_catalog_modal');

	        $params = array(
	            'courseModel'               => $courseModel,
	            'subscribersCount'          => $subscribersCount,
	            'isOverbooked'              => $isOverbooked,
	            'langName'                  => $langName,
	            'previewActionUrl'          => $previewActionUrl,
	            'demoType'                  => $demoType,
	            'showButton'                => !$showOnlyDetails && $coursePolicy["showButton"],
	            'label'                     => $coursePolicy["label"],
	            'additionalCourseFields'    => $additionalCourseFields
	        );

	        //Trigger event to let plugins doing their own operations
	        $continue = Yii::app()->event->raise('BeforeCourseAxDetailsPageRender', new DEvent($this, $params));

			if ($continue) {
				if (Yii::app()->user->isGuest) {
					$tempUrl = Yii::app()->request->url;
//					CHANGE URL EXAMPLE
//					'/lms/index.php?r=course/axDetails&id=815&YII_CSRF_TOKEN=fa3567a627debba14ce651bc946b2930f5861fb5'
//					TO
//					'/lms/index.php?r=course/details&id=815'
					$tempUrl = str_replace('axDetails', 'details', $tempUrl);
					Yii::app()->session['lasturl'] = $tempUrl;
					Yii::app()->SESSION['trigger_after_load'] = true;
					Yii::app()->SESSION['on_separate_window'] = true;
				}
				$this->renderPartial("_ax_details", $params, false, true);
			}
		}else{
			$this->redirect(Docebo::createLmsUrl('site/index'));
			Yii::app()->end();
		}
	}

	/**
	 * @param $courseModel
	 * @param $additionalCourseFieldsModels
	 */
	private function getCourseFieldsToDisplay($courseModel, $additionalCourseFieldsModels, $whereFrom) {
		$additionalFields = array();
		foreach($additionalCourseFieldsModels as $fieldModel) {
			/* @var $fieldModel LearningCourseField */
			$tempValue = Yii::app()->getDb()->createCommand()
				->select('field_' . $fieldModel->id_field)
				->from(LearningCourseFieldValue::model()->tableName() . ' v')
				->where('v.id_course=:id_course', array(':id_course' => $courseModel->idCourse))
				->queryScalar();
			if ($tempValue) {
				$fieldModel->course = $courseModel;
				$className = 'CourseField' . ucfirst($fieldModel->type);
				$fieldValue = $className::renderFieldValue($tempValue, $fieldModel, $whereFrom);
				$in_column = method_exists($className, "shouldDisplayInColumn") ? $className::shouldDisplayInColumn($whereFrom) : true;
				if (empty($fieldValue) || $fieldValue === false || $fieldValue === '')
					continue;

				$additionalFields[$fieldModel->getTranslation()] = array('value' => $fieldValue, 'in_column' => $in_column);
			}
		}

		return $additionalFields;
	}

	/**
	 * Dedicated page for course details
	 */
	public function actionDetails() {
		if(Yii::app()->user->isGuest && Settings::get('catalog_external', 'off') != 'on')
		{
			Yii::app()->user->setFlash('error', Yii::t("standard", "Unauthorized access"));
			$this->redirect(Docebo::createAppUrl('lms:site/index'));
		}

		$courseId = Yii::app()->request->getParam('id');
		$courseModel = LearningCourse::model()->with('learningCourseRating')->findByPk($courseId);

		if($courseModel->status == LearningCourse::$COURSE_STATUS_PREPARATION)
		{
			Yii::app()->user->setFlash('error', Yii::t("standard", "Unauthorized access"));
			$this->redirect(Docebo::createAppUrl('lms:site/index'));
		}

		if (Yii::getPathOfAlias('jplayer') === false)
			Yii::setPathOfAlias('jplayer', Yii::app()->basePath.'/../../common/extensions/jplayer');

		if(Yii::app()->user->isGuest)
		{
			if(array_search($courseId, LearningCourse::getCoursesIdsVisibleInExternalCatalog()) === false)
			{
				Yii::app()->user->setFlash('error', Yii::t("standard", "Unauthorized access"));
				$this->redirect(Docebo::createAppUrl('site/index'));
			}
		}
		else
		{
			if(!Yii::app()->user->getIsGodadmin() && !LearningCourse::courseDetailsIsVisibleForUser($courseId))
			{
				Yii::app()->user->setFlash('error', Yii::t("standard", "Unauthorized access"));
				$this->redirect(Docebo::createAppUrl('lms:site/index'));
			}
		}

		$assetsPath = Yii::getPathOfAlias('jplayer.assets');
		$assetsUrl = Yii::app()->assetManager->publish($assetsPath);
		Yii::app()->getClientScript()->registerCssFile($assetsUrl . "/skin/dialog2_videoplayer/jplayer.css");
		Yii::app()->getClientScript()->registerScriptFile($assetsUrl . '/js/jquery.jplayer.min.js', CClientScript::POS_HEAD);
        Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/css/docebo-star-rating.css');

		// Validate request
		if (!$courseModel) {
			throw new CHttpException(404, 'Invalid course Id');
		}

		if(!Yii::app()->user->isGuest && LearningCourseuser::isSubscribed(Yii::app()->user->id, $courseId)) {
			$this->redirect(Docebo::createLmsUrl("player/training", array("course_id" => $courseId)));
		}

		// Get number of subscribers
		$subscribersCount = $courseModel->getTotalSubscribedUsers();

		// Language related data
		$langCode = $courseModel->lang_code; // Not browser code!!!
		$langName = Lang::getNameByCode($langCode);

		// Preview Ajax action URL (used in details dialog to load preview/demo html)
		$previewActionUrl = $this->createUrl('axCoursePreview', array('id' => $courseId));

		// Demo media type
		$demoType = $courseModel->getDemoMediaCategory();
		if ($demoType == LearningCourse::$DEMO_MEDIA_CATEGORY_UNKNOWN) {
			$demoFileName = $courseModel->course_demo;
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_COURSEDOCS);
			$previewActionUrl = $storageManager->fileUrl($demoFileName);
		}

		// Overbooking
		$isOverbooked = $courseModel->isOverbooked();
		$coursePolicy = $courseModel->getCoursePolicy();

		// Get additional course fields data here
		$additionalCourseFieldsModels = $courseModel->getCourseAdditionalFields(false, true);
		$additionalCourseFields = $this->getCourseFieldsToDisplay($courseModel, $additionalCourseFieldsModels, 'course_catalog_inpage');

		if(Yii::app()->user->isGuest){
			Yii::app()->session['lasturl'] = Yii::app()->request->url;
		}

		$params = array(
			'courseModel'               => $courseModel,
			'subscribersCount'          => $subscribersCount,
			'isOverbooked'              => $isOverbooked,
			'langName'                  => $langName,
			'previewActionUrl'          => $previewActionUrl,
			'demoType'                  => $demoType,
			'showButton'                => $coursePolicy["showButton"],
			'label'                     => $coursePolicy["label"],
			'trainingMaterials' => LearningObjectsManager::getCourseLearningObjects($courseId, false, false),
			'topRatedCourses'           => LearningCourse::getTopRated(),
			'controller'                => $this,
			'additionalCourseFields'    => $additionalCourseFields
		);

		//Trigger event to let plugins doing their own operations
		$continue = Yii::app()->event->raise('BeforeCourseDetailsPageRender', new DEvent($this, $params));

		if ($continue)
			$this->render("details", $params);
	}

	public function actionAxHoverDetails() {
		$idCourse 			= Yii::app()->request->getParam('idCourse');
		$resetToken 		= Yii::app()->request->getParam('reset', false);

		// Get a course model
		/* @var $courseModel LearningCourse */
		$courseModel = LearningCourse::model()->findByPk($idCourse);

		if ($courseModel) {
			$params = array('courseModel' => $courseModel);

			$idUser = Yii::app()->user->id;
			/* @var $courseUserModel LearningCourseuser */
			$courseUserModel = LearningCourseuser::model()->findByAttributes(array('idUser' => $idUser, 'idCourse' => $courseModel->idCourse));
			$userIsSubscribed = (null != $courseUserModel);
			$params['userIsSubscribed'] = $userIsSubscribed;

			if ($courseModel->course_type == LearningCourse::TYPE_ELEARNING) {
				$courseStats = LearningObjectsManager::getCourseLearningObjectsStats($idCourse, false, true);
				$params['stats'] = $courseStats;
			}

			if (!$userIsSubscribed) {
				if (intval($courseModel->max_num_subscribe) > 0) {
					$countSubscribedUsers = $courseModel->getTotalSubscribedUsers(false, false, false);
					$diff = (intval($courseModel->max_num_subscribe) - $countSubscribedUsers);
					if ($diff > 0) {
						$params['available_seats'] = $diff;
					} else {
						$params['available_seats'] = 0;
					}
				}
				$params['isOverbooked'] = $courseModel->isOverbooked();
			}
			$params['course_id'] = $courseModel->idCourse;
			$params['course_lang'] = Lang::getBrowserCodeByCode($courseModel->lang_code);

			// todo: port the function from the old lms to check for permissions
			$isCourseLocked = false;
			$reason = '';
			$showButton = NULL;
			$label = NULL;
			if ($courseUserModel) {
				$canEnterCourse = $courseUserModel->canEnterCourse();
				if (!$canEnterCourse['can']) {
					$isCourseLocked = true;
					switch ($canEnterCourse['reason']) {
						case 'prerequisites':
							$reason = Yii::t('organization', '_ORGLOCKEDTITLE');
							break;
						case 'waiting':
							$reason = Yii::t('catalogue', '_WAITING_APPROVAL');
							break;
						case 'user_status':
						case 'course_status':
						case 'subscription_expired':
						case 'subscription_not_started':
						case 'course_date':
						default:
							$reason = Yii::t('storage', '_LOCKED');
							break;
					}
				}
			}
			else
			{
				$cartPositions = Yii::app()->shoppingCart->getPositions();
				foreach($cartPositions as $courseCartItem) {
					if(!($cartPositions instanceof CourseCartPosition))
						continue;

					if ($courseCartItem->idCourse == $idCourse) {
						$isCourseLocked = TRUE;
						$reason         = Yii::t('cart', 'You can\'t add a course in the cart more than once.');
						$showButton     = FALSE;
						$label          = Yii::t('cart', 'You can\'t add a course in the cart more than once.');
					}
				}
			}

			$coursePolicy = $courseModel->getCoursePolicy();

			$params['is_locked'] = $isCourseLocked;
			$params['reason'] = $reason;
			$params['showButton'] = is_null($showButton) ? $coursePolicy['showButton'] : $showButton;
			$params['label'] = is_null($label) ? $coursePolicy['label'] : $label;
			$params['resetToken'] = $resetToken;


			if ($userIsSubscribed) {
				$params['user_level'] = Yii::t('levels', '_LEVEL_' . $courseUserModel->level);
			}

			if (Yii::app()->event->raise('BeforeCourseHoverDetailsRender', new DEvent($this, $params))) {
				$this->renderPartial('_hover_details', $params);
			}
		}
	}

	/**
	 * Called by Course Details view to show course demo
	 *
	 */
	public function actionAxCoursePreview() {

		$courseId = Yii::app()->request->getParam('id', null);

		// Get a course model
		$courseModel = LearningCourse::model()->findByPk($courseId);
		if (Yii::getPathOfAlias('jplayer') === false)
			Yii::setPathOfAlias('jplayer', Yii::app()->basePath.'/../../common/extensions/jplayer');

		$assetsPath = Yii::getPathOfAlias('jplayer.assets');
		$assetsUrl = Yii::app()->assetManager->publish($assetsPath);
		Yii::app()->getClientScript()->registerCssFile($assetsUrl . "/skin/dialog2_videoplayer/jplayer.css");
		Yii::app()->getClientScript()->registerScriptFile($assetsUrl . '/js/jquery.jplayer.min.js', CClientScript::POS_HEAD);

		$demoFileName = $courseModel->course_demo;

		$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_COURSEDOCS);
		$demoUrl = $storageManager->fileUrl($demoFileName);


		// BACK link URL (course details)
		$backUrl = $this->createUrl("axDetails", array("id" => $courseId));

		// demo type (video, swf, unknown)
		$demoType = $courseModel->getDemoMediaCategory();


		$response = $this->renderPartial("_course_demo", array(
			'courseModel' => $courseModel,
			'demoUrl' => $demoUrl,
			'backUrl' => $backUrl,
			'demoType' => $demoType,
		),false, true);

		// Send back a response HTML
		echo $response;
	}

	/**
	 *  AJAX action: returns HTML + other data of a filtered list of courses in a grid
	 *
	 */
	public function actionAxFilteredCoursesGrid() {

		//TODO: shouldn't we put here a check for CoursecatalogApp activation?

		// Filter courses, according to incoming filter (POST/GET)
		// Filter by various types: paid, free, view all, etc.
		$fltType = Yii::app()->request->getParam('flt_type', '');
		$fltQuery = trim(Yii::app()->request->getParam('flt_query', ''));
		$fltCategory = Yii::app()->request->getParam('flt_category', '');

		// An workaround for IE8 SEARCH query (input) placeholder value being sent always.
		if ($fltQuery == trim(Yii::t('standard', '_SEARCH'))) {
			$fltQuery = "";
		}

		// Possible filtering by Label or By Multi Catalog
		$fltLabel = trim(Yii::app()->request->getParam('flt_label', ''));
		$fltCatalog = Yii::app()->request->getParam('flt_catalog', '');


		// Additionally, see if we are ordered to load another page
		$page = Yii::app()->htmlpurifier->purify(Yii::app()->request->getParam('page', 0));

		// Get the gridType requested; default to mycourses
		$gridType = Yii::app()->request->getParam('grid_type', CoursesGrid::$GRIDTYPE_INTERNAL_MYCOURSES);

		// Show First Box at all? This is kind of option
		//$showFirstBox = Yii::app()->request->getParam('show_first_box', true);
		$showFirstBox = ($page == 0);

		// Get page size config settings
		$pageSize = Yii::app()->params['catalogGridPageSize'];
		Yii::app()->event->raise('OverrideCoursesGridPageSize', new DEvent($this, array(
			'pageSize' => &$pageSize
		)));

		$grid = null;

		// Just as an elevated flexibility, we have these two parameters
		$enableLabels = isset(Yii::app()->params['enable_labels']) ? Yii::app()->params['enable_labels'] : true;
		$enableLabels = $enableLabels && ($fltType != CoursesGrid::$FILTER_TYPE_NOLABELS);

		$enableMulticatalog = isset(Yii::app()->params['enable_multicatalog']) ? Yii::app()->params['enable_multicatalog'] : true;
		$showAllCoursesIfNoCatalogueIsAssigned = ('on' == Settings::get('on_catalogue_empty', 'on'));

		// Handle different Grid types
		switch ($gridType) {
			case CoursesGrid::$GRIDTYPE_EXTERNAL:
				$_flt = array(
					'type' => $fltType,
					'query' => $fltQuery
				);
				if (Settings::get('catalog_use_categories_tree') === 'on') {
					$_flt['category'] = $fltCategory;
				}
				$grid = $this->buildGridForCatalog($gridType, null, $page, $pageSize/*, $fltType, $fltQuery*/, $_flt);
				break;

			case CoursesGrid::$GRIDTYPE_INTERNAL_CATALOG:
				// By default: do not show multicatalog
				$showMulticatalog = false;

				$multiCatalogData = array('catalogs'=>array());

				// If Multicatalog is enabled (see params)
				if ($enableMulticatalog && ($fltCatalog == '')) {
					$_flt = array(
						'type' => $fltType,
						'query' => $fltQuery
					);
					if (Settings::get('catalog_use_categories_tree') === 'on') {
						$_flt['category'] = $fltCategory;
					}
					$multiCatalogData = $this->getMultiCatalogData(Yii::app()->user->id, $page, $pageSize/*, $fltType, $fltQuery*/, $_flt);

					if (empty($multiCatalogData) || count($multiCatalogData) == 0) {
						$showMulticatalog = false;
					}
					// We show mutlicatalog only if there is something to show to user
					$showMulticatalog = $multiCatalogData['showMultiCatalog'];
				}

				if ($showMulticatalog) {
					$grid = $this->buildGridForMultiCatalog($multiCatalogData["catalogs"], $gridType);
					if (!empty($fltCatalog)) {
						$grid['extra_json']['grid_subtype'] = CoursesGrid::$GRIDTYPE_INTERNAL_CATALOG_ONECATALOG;
					} else {
						$grid['extra_json']['grid_subtype'] = CoursesGrid::$GRIDTYPE_INTERNAL_MCATALOG;
					}
				} else {
					if (!$showAllCoursesIfNoCatalogueIsAssigned && empty($fltCatalog)) {
						/**
						 * if the user doesn't have a catalog assigned to him, show nothing at all, just a "no course available" message
						 */
						$grid = array(
							'html' => '',
							'total_results' => 0,
							'extra_json' => array_merge(
								array(
									'grid_subtype' => CoursesGrid::$GRIDTYPE_INTERNAL_CATALOG,
									'override_mode' => 'error',
									'error_html' => Yii::t("catalogue", "_NO_COURSE"),
								)
							)
						);
					} else {
						/**
						 * if at least one catalog is assigned to the user, show the catalog selection page (as it work right now)
						 */
						$_flt = array(
							'type' => $fltType,
							'query' => $fltQuery,
							'catalog' => $fltCatalog
						);
						if (Settings::get('catalog_use_categories_tree') === 'on') {
							$_flt['category'] = $fltCategory;
						}
						$grid = $this->buildGridForCatalog($gridType, Yii::app()->user->id, $page, $pageSize/*, $fltType, $fltQuery, $fltCatalog*/, $_flt);
						if (!empty($fltCatalog)) {
							$grid['extra_json']['grid_subtype'] = CoursesGrid::$GRIDTYPE_INTERNAL_CATALOG_ONECATALOG;
						} else {
							$grid['extra_json']['grid_subtype'] = CoursesGrid::$GRIDTYPE_INTERNAL_CATALOG;
						}
						$grid['extra_json']['page'] = $page;
					}
				}
				break;

			case CoursesGrid::$GRIDTYPE_INTERNAL_MYCOURSES:
				$grid = $this->buildGridForInternalMycourses($gridType, $page, $pageSize, $fltType, $fltQuery, $fltLabel, $enableLabels);
				break;

			case CoursesGrid::$GRIDTYPE_INTERNAL_MIXED:
				$_flt = array(
					'type' => $fltType,
					'query' => $fltQuery,
					'catalog' => $fltCatalog
				);
				if (Settings::get('catalog_use_categories_tree') === 'on') {
					$_flt['category'] = $fltCategory;
				}
				$internalGrid = $this->buildGridForCatalog($gridType, Yii::app()->user->id, $page, $pageSize/*, $fltType, $fltQuery, ''*/, $_flt, true);
				$mycoursesGrid = $this->buildGridForInternalMycourses($gridType, $page, $pageSize, $fltType, $fltQuery, $fltLabel, false, true);

				$html = $mycoursesGrid['html'] . ($mycoursesGrid['html'] != '' ? '<br><br>' : '') . $internalGrid['html'];

				if(isset($mycoursesGrid['extra_json']['override_mode']) && $mycoursesGrid['extra_json']['override_mode'] == 'error' && !isset($internalGrid['extra_json']['override_mode']))
				{
					unset($mycoursesGrid['extra_json']['override_mode']);
					unset($mycoursesGrid['extra_json']['error_html']);
				}
				elseif(isset($internalGrid['extra_json']['override_mode']) && $internalGrid['extra_json']['override_mode'] == 'error' && !isset($mycoursesGrid['extra_json']['override_mode']))
				{
					unset($internalGrid['extra_json']['override_mode']);
					unset($internalGrid['extra_json']['error_html']);
				}

				$grid = array(
					'html' => $html,
					'total_results' => 0,
					'extra_json' => array_merge(
						array_merge($mycoursesGrid['extra_json'], $internalGrid['extra_json']), array('grid_subtype' => CoursesGrid::$GRIDTYPE_INTERNAL_MIXED)
					)
				);
				break;


			default:
				break;
		}

		// Page count and current page loaded
		$totalResults = $grid['total_results'];
		$tmp = $totalResults / $pageSize;
		if ($tmp > floor($tmp)) {
			$pageCount = floor($tmp) + 1; // we've got some 'partial' page, still counting as page
		} else {
			$pageCount = floor($tmp);
		}

		// Compose response, include information about pagination
		// and merge with any other existing extra response data
		$response = array_merge(array(
			'html' => $grid['html'],
			'page_count' => $pageCount,
			'current_page' => $page
			), $grid['extra_json']);

		echo CJSON::encode($response);
	}

	/**
	 * Builds the html for the internal catalog
	 *
	 * @param       $gridType E.g.: CoursesGrid::$GRIDTYPE_EXTERNAL, $GRIDTYPE_INTERNAL_MYCOURSES, $GRIDTYPE_INTERNAL_CATALOG, $GRIDTYPE_INTERNAL_MIXED
	 * @param null  $idUser
	 * @param int   $page
	 * @param       $pageSize
	 * @param array $flt
	 * @param bool  $showViewMoreBox
	 *
	 * @internal param string $fltType
	 * @internal param string $fltQuery
	 * @return array
	 */
	private function buildGridForCatalog($gridType, $idUser = null, $page = 0, $pageSize/*, $fltType = '', $fltQuery = '', $fltCatalog = ''*/, $flt = array(), $showViewMoreBox = false) {
		//$gridType = ($idUser==null) ? CoursesGrid::$GRIDTYPE_EXTERNAL : CoursesGrid::$GRIDTYPE_INTERNAL_CATALOG;
		$showFirstBox = ($page == 0);

		$fltType = (is_array($flt) && isset($flt['type']) ? $flt['type'] : '');
		$fltQuery = (is_array($flt) && isset($flt['query']) ? $flt['query'] : '');
		$fltCatalog = (is_array($flt) && isset($flt['catalog']) ? $flt['catalog'] : '');
		$fltCategory = (is_array($flt) && isset($flt['category']) ? $flt['category'] : '');

		// If it is an external catalog
		if ($gridType == CoursesGrid::$GRIDTYPE_EXTERNAL) {
			$fltCatalog = LearningCatalogue::getVisiblePublicCatalogs($fltCatalog);
		}

		$viewMoreUrl = '';
		if ($showViewMoreBox) {
			$pageSize -= 1;
			$viewMoreUrl = Yii::app()->createUrl('site/index', array('opt' => 'catalog'));
		}


		/// VERY IMPORTANT CALCULATION. PLEASE DO NOT CHANGE. ASK PLAMEN FIRST! THANK YOU!
		$showFirstBox = ($page == 0);
		if ($page == 0) {
			$limit = $pageSize - 1;
			$offset = 0;
		} else {
			$limit = $pageSize;
			$offset = ($page - 1) * $pageSize + ($pageSize - 1);
		}


		$firstBoxHtml = '';
		if ($showFirstBox) {

			$title = Yii::t('standard', 'Catalog');
			$description = "";

			if ($fltCatalog > 0) {
				$catalogModel = LearningCatalogue::model()->findByPk($fltCatalog);
				$title = $catalogModel->name;
				$description = $catalogModel->description;
			}

			$firstBoxHtml = '';
			$params = array(
				'bgCss' => 'bg-color-darkgray',
				'title' => &$title,
				'description' => &$description,
				'link' => $viewMoreUrl,
				'clear' => true,
				'gridType' => $gridType,
				'html' => &$firstBoxHtml
			);
			if ( Yii::app()->event->raise('RenderCoursesLabelTile', new DEvent($this, $params)) ) {
				$firstBoxHtml = $this->renderPartial('_course_cover', $params, true);
			}
		}

		$_flt = array(
			'type' => $fltType,
			'query' => $fltQuery,
			'catalog' => $fltCatalog,
			'category' => $fltCategory
		);

		$coursesResult = array();

		// allow plugins to display a custom courses catalogue
		$continue = Yii::app()->event->raise('GetCatalogCoursesPage', new DEvent($this, array(
			'idUser' => $idUser,
			'offset' => $offset,
			'limit' => $limit,
			'flt' => $_flt,
			'coursesResult' => &$coursesResult
		)));

		//if the catalog is external, order the courses alphabetically
        $order = ($gridType === CoursesGrid::$GRIDTYPE_EXTERNAL) ? 'name ASC' : null;
        // if no plugin does this, return a page from the default catalogue
        // Check Point Charlie
		if ($continue) {
			$coursesResult = $this->getCatalogCoursesPage(
				$idUser,
				$offset,
				$limit,
				$_flt,
                $order
			);
		}

		$extra_json = array(
			'has_for_sale' => $coursesResult['has_for_sale'],
			'has_for_free' => $coursesResult['has_for_free']
		);

		// Render the courses grid for the current label
		if (count($coursesResult['courses'])) {
			$coursesGridHtml = '';
			$params = array(
				'gridType' => $gridType,
				'firstBoxHtml' => $firstBoxHtml,
				'courses' => $coursesResult['courses'],
				'viewMoreUrl' => $viewMoreUrl,
				'html' => &$coursesGridHtml
			);
			if (Yii::app()->event->raise('RenderCoursesGrid', new DEvent($this, $params))) {
				$coursesGridHtml = $this->renderPartial('_courses_grid', $params, true);
			}
			$gridHtml = $coursesGridHtml;
		} else if ($page == 0) {
			// prepare this error only if this is the first page, otherwise it's not needed
			// Yii::app()->user->setFlash('error', Yii::t("catalogue", "_NO_COURSE"));
			// $gridHtml = DoceboUI::printFlashMessages(true);
			$extra_json["override_mode"] = 'error';
			$extra_json["error_html"] = Yii::t("catalogue", "_NO_COURSE");
			$gridHtml = '';
		}

		$response = array(
			'html' => $gridHtml,
			'total_results' => $coursesResult['total'] + 1, // +1 for the first box, so pageCount to be correct
			'extra_json' => $extra_json,
		);
		return $response;
	}

	private function buildGridForMultiCatalog($catalogs, $gridType) {

		$gridHtml = "";

		// Enumerate Catalogs
		foreach ($catalogs as $catalog) {

			$catalogModel = $catalog['catalog'];

			if($catalog['totalCoursesInCatalog'] <= 3){
				// Hide the "View more" courses tile, since
				// we are showing all available courses in the catalog
				// already - there is nothing else to show
				$viewMoreUrl = null;
			}else{
				$viewMoreUrl = Yii::app()->createUrl('site/index', array(
					'flt_catalog' => $catalogModel->idCatalogue,
					'opt' => 'catalog',
					'flt_query' => Yii::app()->request->getParam('flt_query', ''),
				));
			}

			$firstBoxHtml = '';
			$params = array(
				'bgCss' => 'bg-color-darkgray',
				'title' => $catalogModel->name,
				'description' => $catalogModel->description,
				'link' => 'catalog',
				'clear' => true,
				'gridType' => $gridType,
				'html' => &$firstBoxHtml
			);
			if ( Yii::app()->event->raise('RenderCoursesLabelTile', new DEvent($this, $params)) ) {
				$firstBoxHtml = $this->renderPartial('_course_cover', $params, true);
			}

			// Render the courses grid for the current catalog
			$coursesGridHtml = '';
			$params = array(
				'gridType' => $gridType,
				'firstBoxHtml' => $firstBoxHtml,
				'courses' => $catalog['courses'],
				'coursesCount' => $catalog['totalCoursesInCatalog'],
				'viewMoreUrl' => $viewMoreUrl,
				'html' => &$coursesGridHtml
			);
			if (Yii::app()->event->raise('RenderCoursesGrid', new DEvent($this, $params))) {
				$coursesGridHtml = $this->renderPartial('_courses_grid', $params, true);
			}
			$gridHtml .= $coursesGridHtml;
		}

		$response = array(
			'html' => $gridHtml,
			'total_results' => 1,
			'extra_json' => array(
				'has_for_sale' => true,
				'has_for_free' => true,
			)
		);

		return $response;
	}

	/**
	 * Builds the html in the 'mycourses' area
	 *
	 * @param string $gridType
	 * @param int $page
	 * @param $pageSize
	 * @param string $fltType
	 * @param string $fltQuery
	 * @param string $fltLabel
	 * @param bool $showLabels
	 * @param bool $showViewMoreBox
	 * @return array
	 */
	private function buildGridForInternalMycourses($gridType, $page = 0, $pageSize, $fltType = '', $fltQuery = '', $fltLabel = '', $showLabels = true, $showViewMoreBox = false) {
		$gridHtml = '';

		/**
		 * There are 3 cases:
		 *
		 * 1. There are NO labels assigned to the user's subscribed courses for the current language => show the normal layout
		 *
		 * 2. There ARE labels assigned to the user's courses for the current language => show all labels with preview of the courses
		 *
		 * 3. If a label is specified => show the normal layout with only the courses that have that label assigned
		 *
		 */
		$idUser = Yii::app()->user->id;
		$langCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
		$validLabels = array();
		$extra_json = array();

		if ($showLabels && $page == 0 && $fltLabel == '') {
			// because we're not filtering by label, we try to get and show all labels

			$userLabels = LearningLabel::model()->getUserLabels($idUser, $langCode);
			if (!empty($userLabels)) {
				foreach ($userLabels as $label) {
					//if (LearningLabelCourse::model()->count(array('id_common_label'=>$userLabels->id_common_label))>0) {
					if ($label['courseuser_count'] > 0 && $label['id_common_label'] != 0) {
						$validLabels[] = $label;
					}
				}
			}
		}

		if (!empty($validLabels)) {

			// CASE 2
			// show the layout for labels
			$countCoursesToGet = 3;

			$countCoursesAllLabels = 0; // to have an idea if we are showing something at all

			foreach ($validLabels as $label) {
				$viewAllUrl = Yii::app()->createUrl('site/index',
						array(
							'opt' => 'mycourses',
							'flt_label' => $label['id_common_label'],
							'flt_query' => Yii::app()->request->getParam('flt_query', '')
							)
						);

				/* @var $label LearningLabel */
				$firstBoxHtml = '';
				$params = array(
					'bgColor' => $label['color'],
					'title' => $label['title'],
					'description' => $label['description'],
					'icon' => $label['icon'],
					'link' => $viewAllUrl,
					'clear' => true,
					'gridType' => $gridType,
					'html' => &$firstBoxHtml
				);
				if ( Yii::app()->event->raise('RenderCoursesLabelTile', new DEvent($this, $params)) ) {
					$firstBoxHtml = $this->renderPartial('_course_cover', $params, true);
				}

				$courses = array();

				$params = array(
					'idUser' => $idUser,
					'idLabel' => $label['id_common_label'],
					'langCode' => $langCode,
					'offset' => 0,
					'limit' => $countCoursesToGet + 1,
					'fltType' => $fltType,
					'fltQuery' => $fltQuery,
					'returnTotal' => false,
					'courses' => &$courses
				);

				$event = new DEvent($this, $params);
				if ( Yii::app()->event->raise('GetLabeledCourses', $event) ) {

					// get the overriden params
					$params = $event->params;
					// update local vars
					$countCoursesToGet = intval($params['limit']) - 1;

					$result = $this->getLabeledCourses($params['idUser'], $params['idLabel'], $params['langCode'], $params['offset'], $params['limit'], $params['fltType'], $params['fltQuery'], $params['returnTotal']);
					$courses = $result['courses'];

				} else {
					// the '$courses' array will contain the courses returned by the event's listener method
				}

				$viewMoreUrl = '';
				if (count($courses) > $countCoursesToGet) {
					array_splice($courses, $countCoursesToGet);
					$viewMoreUrl = $viewAllUrl;
				}

				// Render the courses grid for each label
				if (count($courses) > 0) {
					$coursesGridHtml = '';
					$params = array(
						'gridType' => $gridType,
						'firstBoxHtml' => $firstBoxHtml,
						'courses' => $courses,
						'viewMoreUrl' => $viewMoreUrl,
						'html' => &$coursesGridHtml
					);
					if (Yii::app()->event->raise('RenderCoursesGrid', new DEvent($this, $params))) {
						$coursesGridHtml = $this->renderPartial('_courses_grid', $params, true);
					}
					$gridHtml .= $coursesGridHtml;
					$countCoursesAllLabels += count($courses);
				}
			}

			// if there are courses with no labels attached, group them under a 'no label' label (as in the layout)
			$result = $this->getUnlabeledCourses($idUser, $langCode, 0, $countCoursesToGet + 1, $fltType, $fltQuery);
			$unlabeledCourses = $result['courses'];

			if (!empty($unlabeledCourses)) {
				$viewAllUrl = Yii::app()->createUrl('site/index',
						array(
								'opt' => 'mycourses',
								'flt_label' => 0,
								'flt_query' => Yii::app()->request->getParam('flt_query', '')
							)
						);
				$firstBoxHtml = '';
				$params = array(
					'bgCss' => 'bg-color-orange',
					'title' => Yii::t('label', 'No label'),
					'description' => '',
					'link' => $viewAllUrl,
					'clear' => true,
					'gridType' => $gridType,
					'html' => &$firstBoxHtml
				);
				if ( Yii::app()->event->raise('RenderCoursesLabelTile', new DEvent($this, $params)) ) {
					$firstBoxHtml = $this->renderPartial('_course_cover', $params, true);
				}

				$viewMoreUrl = '';
				if (count($unlabeledCourses) > $countCoursesToGet) {
					array_splice($unlabeledCourses, $countCoursesToGet);
					$viewMoreUrl = $viewAllUrl;
				}

				// Render the courses grid for the current label
				if (count($unlabeledCourses) > 0) {
					$coursesGridHtml = '';
					$params = array(
						'gridType' => $gridType,
						'firstBoxHtml' => $firstBoxHtml,
						'courses' => $unlabeledCourses,
						'viewMoreUrl' => $viewMoreUrl,
						'html' => &$coursesGridHtml
					);
					if (Yii::app()->event->raise('RenderCoursesGrid', new DEvent($this, $params))) {
						$coursesGridHtml = $this->renderPartial('_courses_grid', $params, true);
					}
					$gridHtml .= $coursesGridHtml;
					$countCoursesAllLabels = +count($unlabeledCourses);
				}
			}

			$extra_json = array('grid_subtype' => CoursesGrid::$GRIDTYPE_INTERNAL_MYCOURSES_LABELS);

			if ($countCoursesAllLabels <= 0) {
				$extra_json['override_mode'] = 'error';
				$extra_json['error_html'] = Yii::t("catalogue", "_NO_COURSE");
				$gridHtml = '';
				//Yii::app()->user->setFlash('error', Yii::t("catalogue", "_NO_COURSE"));
				//$gridHtml = DoceboUI::printFlashMessages(true); // xxxx
			}


			// Compose response, include information about pagination
			$response = array(
				'html' => $gridHtml,
				'total_results' => 0,
				'extra_json' => $extra_json
			);


			return $response;
		} else {
			// show the normal courses layout filtered by label (if specified)

			$viewMoreUrl = '';
			if ($showViewMoreBox) {
				$pageSize -= 1;
				$viewMoreUrl = Yii::app()->createUrl('site/index', array('opt' => 'mycourses'));
			}


			/// VERY IMPORTANT CALCULATION. PLEASE DO NOT CHANGE. ASK PLAMEN FIRST! THANK YOU!
			$showFirstBox = ($page == 0);
			if ($page == 0) {
				$limit = $pageSize - 1;
				$offset = 0;
			} else {
				$limit = $pageSize;
				$offset = ($page - 1) * $pageSize + ($pageSize - 1);
			}

			$firstBoxHtml = '';
			$callMethodName = 'subscribed';

			if (is_numeric($fltLabel)) {
				if ($fltLabel == 0) {
					$callMethodName = 'unlabeled';
				} else {
					$callMethodName = 'labeled';
				}
			}

			if ($showFirstBox) {
				if (is_numeric($fltLabel)) {
					if ($fltLabel == 0) {

						$firstBoxHtml = '';
						$params = array(
							'bgCss' => 'bg-color-orange',
							'title' => Yii::t('label', 'No label'),
							'description' => '',
							'link' => $viewMoreUrl,
							'gridType' => $gridType,
							'html' => &$firstBoxHtml
						);
						if ( Yii::app()->event->raise('RenderCoursesLabelTile', new DEvent($this, $params)) ) {
							$firstBoxHtml = $this->renderPartial('_course_cover', $params, true);
						}

						$callMethodName = 'unlabeled';
					} else {

						$label = LearningLabel::model()->findByAttributes(array(
							'id_common_label' => $fltLabel,
							'lang_code' => $langCode
						));
						$firstBoxHtml = '';
						$params = array(
							'bgColor' => $label->color,
							'title' => $label->title,
							'description' => $label->description,
							'link' => $viewMoreUrl,
							'clear' => true,
							'gridType' => $gridType,
							'html' => &$firstBoxHtml
						);
						if ( Yii::app()->event->raise('RenderCoursesLabelTile', new DEvent($this, $params)) ) {
							$firstBoxHtml = $this->renderPartial('_course_cover', $params, true);
						}
						$callMethodName = 'labeled';
					}
				} else {
					if ($fltType == CoursesGrid::$FILTER_TYPE_NOLABELS) {
						$firstBoxHtml = '';
						$params = array(
							'bgCss' => 'bg-color-blue',
							'title' => Yii::t('standard', '_ALL_COURSES'),
							'description' => '',
							'link' => $viewMoreUrl,
							'gridType' => $gridType,
							'html' => &$firstBoxHtml
						);
						if ( Yii::app()->event->raise('RenderCoursesLabelTile', new DEvent($this, $params)) ) {
							$firstBoxHtml = $this->renderPartial('_course_cover', $params, true);
						}
					} else {
						$firstBoxHtml = '';
						$params = array(
							'bgCss' => 'bg-color-blue',
							'title' => Yii::t('menu_over', '_MYCOURSES'),
							'description' => Yii::t('label', '_ALL_DESCRIPTION'),
							'link' => $viewMoreUrl,
							'gridType' => $gridType,
							'html' => &$firstBoxHtml
						);
						if ( Yii::app()->event->raise('RenderCoursesLabelTile', new DEvent($this, $params)) ) {
							$firstBoxHtml = $this->renderPartial('_course_cover', $params, true);
						}
					}
				}
			}

			switch ($callMethodName) {
				case 'unlabeled':
					$coursesResult = $this->getUnlabeledCourses($idUser, $langCode, $offset, $limit, $fltType, $fltQuery, true);
					$extra_json = array('grid_subtype' => CoursesGrid::$GRIDTYPE_INTERNAL_MYCOURSES_ONELABEL);
					break;
				case 'labeled':
					$coursesResult = $this->getLabeledCourses($idUser, $fltLabel, $langCode, $offset, $limit, $fltType, $fltQuery, true);
					$extra_json = array('grid_subtype' => CoursesGrid::$GRIDTYPE_INTERNAL_MYCOURSES_ONELABEL);
					break;
				default:
					$coursesResult = $this->getSubscribedCoursesPage($idUser, $offset, $limit, $fltType, $fltQuery, true);
					$extra_json = array('grid_subtype' => CoursesGrid::$GRIDTYPE_INTERNAL_MYCOURSES);
					break;
			}


			// Render the courses grid for the current label
			if (count($coursesResult['courses']) > 0) {
				$coursesGridHtml = '';
				$params = array(
					'gridType' => $gridType,
					'firstBoxHtml' => $firstBoxHtml,
					'courses' => $coursesResult['courses'],
					'viewMoreUrl' => $viewMoreUrl,
					'html' => &$coursesGridHtml
				);
				if (Yii::app()->event->raise('RenderCoursesGrid', new DEvent($this, $params))) {
					$coursesGridHtml = $this->renderPartial('_courses_grid', $params, true);
				}
				$gridHtml .= $coursesGridHtml;
			} else {
				if ($gridType != CoursesGrid::$GRIDTYPE_INTERNAL_MIXED) {
					//Yii::app()->user->setFlash('error', Yii::t("catalogue", "_NO_COURSE"));
					//$gridHtml = DoceboUI::printFlashMessages(true);
					$extra_json["override_mode"] = 'error';
					$extra_json["error_html"] = Yii::t("catalogue", "_NO_COURSE");
					$gridHtml = '';
				} else {
					$gridHtml = "";
				}
			}

			// Compose response, include information about pagination
			$response = array(
				'html' => $gridHtml,
				'total_results' => $coursesResult['total'] + 1, // +1 for the first box, so pageCount to be correct
				'extra_json' => $extra_json
			);

			return $response;
		}
	}

	/**
	 * Search database and return set of courses (one page) for so called 'catalog' grid
	 *
	 * @param null $idUser May be null for the external catalog
	 * @param int $offset
	 * @param int $limit
	 * @param string $flt_type
	 * @param string $flt_query
	 * @return array
	 */
	private function getCatalogCoursesPage($idUser = null, $offset = 0, $limit = 0/*, $flt_type = '', $flt_query = '', $fltCatalog = ''*/, $flt, $order) {
		return LearningCatalogue::getCatalogCoursesPageNew($idUser, $offset, $limit, $flt, $order);
	}

	/**
	 *  Returns the courses that don't have a label assigned
	 *
	 * @param $idUser
	 * @param $langCode
	 * @param int $offset
	 * @param int $limit
	 * @param string $flt_type
	 * @param string $flt_query
	 * @return array
	 */
	private function getUnlabeledCourses($idUser, $langCode, $offset = 0, $limit = 0, $flt_type = '', $flt_query = '', $returnTotal = false) {

		/*
		 * the simplest way to get unlabeled courses is to substract the labeled ones from all user courses
		 */
		$result = $this->getLabeledCourses($idUser, null, $langCode, 0, 0, $flt_type, $flt_query);
		$arrLabeledCoursesIds = array();
		if (!empty($result['courses'])) {
			$labeledCourses = $result['courses'];
			foreach ($labeledCourses as $course) {
				$arrLabeledCoursesIds[] = $course->idCourse;
			}
		}

		$criteria = new CDbCriteria;
		$criteria->alias = 'course';

		$courseProgressCondition = '';
		if ($flt_type == CoursesGrid::$FILTER_TYPE_MYCOURSES) {
			// Get all courses, filter out Completed (2)
			$courseProgressCondition = 'courseuser.status != ' . LearningCourseuser::$COURSE_USER_END;
		} else {
			if ($flt_type == CoursesGrid::$FILTER_TYPE_COMPLETED) {
				// Get completed courses only (2)
				$courseProgressCondition = 'courseuser.status = ' . LearningCourseuser::$COURSE_USER_END;
			}
		}

		$criteria->with = array(
			'learningCourseusers' => array(
				'alias' => 'courseuser',
				'select' => false,
				'joinType' => 'INNER JOIN',
				'condition' => 'courseuser.idUser = :idUser' . ($courseProgressCondition != '' ? ' AND ' . $courseProgressCondition : ''),
				'params' => array(':idUser' => $idUser)
			),
			//'image.attachment'
		);

		//Remove classroom course if the plug-in wasn't active
		if(!PluginManager::isPluginActive('ClassroomApp'))
			$criteria->addCondition("course.course_type <> 'classroom'");

		/*
		 * get only the courses that are not labeled
		 */
		if (!empty($arrLabeledCoursesIds)) {
			$criteria->addNotInCondition('course.idCourse', $arrLabeledCoursesIds);
		}

		// Set order
		$orderBy = LearningCourse::getOrderBy('course', 'status,name');
		$criteria->order = $orderBy;

		// Search text, search by name, other text fields can be searched
		$flt_query = trim($flt_query);
		if (!empty($flt_query)) {
			$criteria->addCondition("course.name LIKE :query OR course.code LIKE :query OR course.description LIKE :query");
 			$criteria->params[':query'] = '%' . $flt_query . '%';
		}

		$total = ($returnTotal) ? LearningCourse::model()->count($criteria) : '';

		if ($offset > 0) {
			$criteria->offset = $offset;
		}
		if ($limit > 0) {
			$criteria->limit = $limit;
		}

		// make the SQL query in 'together' style  (VERY IMPORTANT)
		// See: http://www.yiiframework.com/doc/api/1.1/CDbCriteria#together-detail
		$criteria->together = true;

		// Get a course model
		$courses = LearningCourse::model()->findAll($criteria);

		return array(
			'courses' => $courses,
			'total' => $total
		);
	}

	/**
	 * Returns all courses that a user is subscribed to filtered by a specific label.
	 * If no label is specified, then all labeled courses will be returned
	 *
	 * @param $idUser
	 * @param int $idLabel
	 * @param $langCode
	 * @param int $offset
	 * @param int $limit
	 * @param string $flt_type
	 * @param string $flt_query
	 * @param bool $returnTotal
	 * @return array
	 */
	private function getLabeledCourses($idUser, $idLabel = 0, $langCode, $offset = 0, $limit = 0, $flt_type = '', $flt_query = '', $returnTotal = false) {

		$criteria = new CDbCriteria;

		$criteria->alias = 'course';

		$courseProgressCondition = '';
		if ($flt_type == CoursesGrid::$FILTER_TYPE_MYCOURSES) {
			// Get all courses, filter out Completed (2)
			$courseProgressCondition = 'courseuser.status != ' . LearningCourseuser::$COURSE_USER_END;
		} else {
			if ($flt_type == CoursesGrid::$FILTER_TYPE_COMPLETED) {
				// Get completed courses only (2)
				$courseProgressCondition = 'courseuser.status = ' . LearningCourseuser::$COURSE_USER_END;
			}
		}

		$learningLabelsCondition = 'label.lang_code = :langCode';
		$learningLabelsParams = array(':langCode' => $langCode);
		if ($idLabel > 0) {
			$learningLabelsCondition .= ' AND label.id_common_label = :idLabel';
			$learningLabelsParams[':idLabel'] = $idLabel;
		}

		$criteria->with = array(
			'learningCourseusers' => array(
				'alias' => 'courseuser',
				'select' => false,
				'joinType' => 'INNER JOIN',
				'condition' => 'courseuser.idUser = :idUser' . ($courseProgressCondition != '' ? ' AND ' . $courseProgressCondition : ''),
				'params' => array(':idUser' => $idUser)
			),
			'learningLabels' => array(
				'alias' => 'label',
				'select' => false,
				'joinType' => 'INNER JOIN',
				'condition' => $learningLabelsCondition,
				'params' => $learningLabelsParams
			),
			//'image.attachment'
		);

		//Remove classroom course if the plug-in wasn't active
		if(!PluginManager::isPluginActive('ClassroomApp'))
			$criteria->addCondition("course.course_type <> 'classroom'");

		// Set order
		$orderBy = LearningCourse::getOrderBy('course', 'status,name');
		$criteria->order = $orderBy;


		// Search text, search by name, other text fields can be searched
		$flt_query = trim($flt_query);
		if (!empty($flt_query)) {
			$criteria->addCondition("course.name LIKE :query OR course.code LIKE :query OR course.description LIKE :query");
 			$criteria->params[':query'] = '%' . str_replace(' ', '%', $flt_query) . '%';
		}

		$total = ($returnTotal) ? LearningCourse::model()->count($criteria) : '';

		if ($offset > 0) {
			$criteria->offset = $offset;
		}
		if ($limit > 0) {
			$criteria->limit = $limit;
		}

		// make the SQL query in 'together' style  (VERY IMPORTANT)
		// See: http://www.yiiframework.com/doc/api/1.1/CDbCriteria#together-detail
		$criteria->together = true;

		// Get a course model
		$courses = LearningCourse::model()->findAll($criteria);

		return array(
			'courses' => $courses,
			'total' => $total
		);
	}

	/**
	 * @param $idUser
	 * @param $page
	 * @param $pageSize
	 * @param bool $showFirstBox
	 * @param bool $getTotal
	 * @param string $flt_type
	 * @param string $flt_query
	 * @param null $idLabel
	 * @param null $langCode
	 * @return array
	 */
	private function getSubscribedCoursesPage($idUser, $offset = 0, $limit = 0, $flt_type = '', $flt_query = '', $returnTotal = false) {

		$criteria = new CDbCriteria;
		$criteria->alias = 'course';

		switch ($flt_type) {
			case CoursesGrid::$FILTER_TYPE_MYCOURSES:
				// Get all courses, filter out Completed (2)
				$courseProgressCondition = 'courseuser.status != ' . LearningCourseuser::$COURSE_USER_END;
				break;
			case CoursesGrid::$FILTER_TYPE_COMPLETED:
				// Get completed courses only (2)
				$courseProgressCondition = 'courseuser.status = ' . LearningCourseuser::$COURSE_USER_END;
				break;
			default:
				$courseProgressCondition = '';
				break;
		}

		// filter by user
		$criteria->with = array(
			'learningCourseusers' => array(
				'alias' => 'courseuser',
				'select' => false,
				'joinType' => 'INNER JOIN',
				'condition' => 'courseuser.idUser = :idUser' . ($courseProgressCondition != '' ? ' AND ' . $courseProgressCondition : ''),
				'params' => array(':idUser' => $idUser)
			),
			//'image.attachment'
		);

		//Remove classroom course if the plug-in wasn't active
		if(!PluginManager::isPluginActive('ClassroomApp'))
			$criteria->addCondition("course.course_type <> 'classroom'");

		// Set order
		$orderBy = LearningCourse::getOrderBy('course', 'status,name');
		$criteria->order = $orderBy;

		// Search text, search by name, other text fields can be searched
		if (!empty($flt_query)) {
			$criteria->addCondition("course.name LIKE :query OR course.code LIKE :query OR course.description LIKE :query");
 			$criteria->params[':query'] = '%' . str_replace(' ', '%', $flt_query) . '%';
		}

		/*
		 * we make another query to calculate the total number of results
		 * only if this is specified !
		 */
		$total = ($returnTotal) ? LearningCourse::model()->count($criteria) : '';

		//Yii::log("Inside: Offset = $offset, Limit=$limit, Total = $total", 'debug', __CLASS__);

		if ($offset > 0) {
			$criteria->offset = $offset;
		}
		if ($limit > 0) {
			$criteria->limit = $limit;
		}

		// make the SQL query in 'together' style  (VERY IMPORTANT)
		// See: http://www.yiiframework.com/doc/api/1.1/CDbCriteria#together-detail
		$criteria->together = true;

		$courses = LearningCourse::model()->findAll($criteria);

		$result = array(
			'courses' => $courses,
			'total' => $total
		);

		return $result;
	}

	/**
	 * Add course to shopping cart and redirect to checkout page
	 *
	 */
	public function actionBuy() {
		/* @var $cs CClientScript */
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/classroom.css');
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/webinar.css');

		$course_id = Yii::app()->request->getParam('course_id', "");
		/* @var $course CourseCartPosition */
		$course = CourseCartPosition::model()->findByPk($course_id);

		$courseModel = LearningCourse::model()->findByPk($course_id);
		$event = new DEvent($this, array('course' => $courseModel));
		Yii::app()->event->raise('CheckForCodesToSubscribe', $event);
		$codes = array();
		if(!$event->shouldPerformAsDefault()){
			$codes = $event->return_value;
		}

		// Check if user is alreadu subscribed to this course
		$isUserSubscribed = LearningCourseuser::model()->findByAttributes(array(
			"idCourse" => $course_id,
			"idUser" => Yii::app()->user->id,
		));

		// If we found a model then this user is subscribed to this course: prevent buy
		if ($isUserSubscribed) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $this->renderPartial('_course_error_modal', array(
					'already_subscribed' => $isUserSubscribed,
					'course' => $course
//					'message' => Yii::t("standard", 'You are already subscribed to this course'),

					), true, true);
			} else {
				$this->redirect(Docebo::createAppUrl('lms:player', array('course_id'=>$course_id)));
			}
			Yii::app()->end();
		}

		// Trigger event to let plugins show their custom subscribe actions
        $errorMsg = null;
		$event = new DEvent($this, array ('course_id' => $course_id, 'error_msg' => &$errorMsg));
		Yii::app()->event->raise('OnBeforeCourseSelfBuy', $event);
        if (!empty($errorMsg)) {
            echo $this->renderPartial('_course_error_modal', array(
                'course' => $course,
                'message' => $errorMsg
            ), true, true);
            Yii::app()->end();
        }
		if ($event->shouldPerformAsDefault() && !empty($event->return_value)) {
			//some plugins may not need normal subscribe actions .. let them avoid those
			if (isset($event->return_value['redirect'])) { $this->redirect($event->return_value['redirect'], true); }
			if (isset($event->return_value['continue']) && !$event->return_value['continue']) return;
		}
		// end event

		if (PluginManager::isPluginActive('ClassroomApp') && $course->isClassroomCourse()) {
			$sessionId = intval($_POST['courseSessionId']);
			/* @var $session LtCourseSession */
			$session = LtCourseSession::model()->findByPk($sessionId);

			if (Yii::app()->request->isPostRequest && $session) {

				// Trigger event to let plugins show their custom subscribe actions
				$event = new DEvent($this, array (
					'course_id' => $course_id,
					'course' => $course
				));
				Yii::app()->event->raise('OnBeforeCourseSelfSubscribe', $event);
				if ($event->shouldPerformAsDefault() && !empty($event->return_value)) {
					//some plugins may not need normal subscribe actions .. let them avoid those
					if (isset($event->return_value['redirect'])) { $this->redirect($event->return_value['redirect'], true); }
					if (isset($event->return_value['continue']) && !$event->return_value['continue']) return;
				}
				// end event

				// Remove item from cart if already added
				$key = $course->getCartItemId();
				if (Yii::app()->shoppingCart->itemAt($key) instanceof IECartPosition) {
					Yii::app()->shoppingCart->remove($key);
				}

				// Add course to cart and the selected session
				$course->setSession($session);
				Yii::app()->shoppingCart->put($course);

				$showCartUrl = $this->createUrl('cart/index');
				echo CHtml::link('', $showCartUrl, array('class' => 'auto-close'));
			} else {

				$dataProvider = LtCourseSession::model()->sessionEnrollmentDataProvider($course->getPrimaryKey(), $course->allow_overbooking);

				if (0 == $dataProvider->getTotalItemCount()) {
					echo $this->renderPartial('_course_error_modal', array(
						'course' => $course,
						'message' => Yii::t('classroom', 'This course has no sessions available at this time. Please come back later.')
						), true, true);
				} else {
					echo $this->renderPartial('_select_session', array(
						'course' => $course,
						'dataProvider' => $dataProvider,
						'codes' => $codes
						), true, true);
				}
			}
		} else if ($course->isWebinarCourse()) {
			$sessionId = intval($_POST['courseSessionId']);
			/* @var $session LtCourseSession */
			$session = WebinarSession::model()->findByPk($sessionId);

			if (Yii::app()->request->isPostRequest && $session) {

				// Trigger event to let plugins show their custom subscribe actions
				$event = new DEvent($this, array (
					'course_id' => $course_id,
					'course' => $course
				));
				Yii::app()->event->raise('OnBeforeCourseSelfSubscribe', $event);
				if ($event->shouldPerformAsDefault() && !empty($event->return_value)) {
					//some plugins may not need normal subscribe actions .. let them avoid those
					if (isset($event->return_value['redirect'])) { $this->redirect($event->return_value['redirect'], true); }
					if (isset($event->return_value['continue']) && !$event->return_value['continue']) return;
				}
				// end event

				// Remove item from cart if already added
				$key = $course->getCartItemId();
				if (Yii::app()->shoppingCart->itemAt($key) instanceof IECartPosition) {
					Yii::app()->shoppingCart->remove($key);
				}

				// Add course to cart and the selected session
				$course->setSession($session);
				Yii::app()->shoppingCart->put($course);

				$showCartUrl = $this->createUrl('cart/index');
				echo CHtml::link('', $showCartUrl, array('class' => 'auto-close'));
			} else {

				$dataProvider = WebinarSession::model()->sessionEnrollmentDataProvider($course->getPrimaryKey(), $course->allow_overbooking);

				if (0 == $dataProvider->getTotalItemCount()) {
					echo $this->renderPartial('_course_error_modal', array(
						'course' => $course,
					), true, true);
				} else {
					echo $this->renderPartial('_select_webinar_session', array(
						'course' => $course,
						'dataProvider' => $dataProvider,
						'codes' => $codes
					), true, true);
				}
			}
		} else {
			// Add course to cart only once
			$key = $course->getCartItemId();
            if ( ! (Yii::app()->shoppingCart->itemAt($key) instanceof IECartPosition) ) {
				Yii::app()->shoppingCart->put($course);
			}

			$showCartUrl = $this->createUrl('cart/index');

			/**
			 * The 'buy' button has the 'ajax' class.
			 * As a consequence, we have to make the redirect using javascript.
			 */
			$this->renderPartial('lms.protected.views.site._js_redirect', array('url'=>$showCartUrl));
		}
	}

	/**
	 *
	 */
	public function actionSubscribe() {

		$course_id = Yii::app()->request->getParam('course_id');
		$course = LearningCourse::model()->findByPk($course_id);
		$event = new DEvent($this, array('course' => $course));
		Yii::app()->event->raise('CheckForCodesToSubscribe', $event);
		$codes = array();
		if(!$event->shouldPerformAsDefault()){
			$codes = $event->return_value;
		}
        $myCoursesUrl = $this->createUrl('site/index', array('opt' => 'mycourses'));

        $event = new DEvent($this, array (
            'course_id' => $course_id,
            'course' => $course
        ));

        if (PluginManager::isPluginActive('ClassroomApp') && $course->isClassroomCourse()) {

			$sessionId = intval($_POST['courseSessionId']);
			/* @var $session LtCourseSession */
			$session = LtCourseSession::model()->findByPk($sessionId);

			if (Yii::app()->request->isPostRequest && $session) {
				// Trigger event to let plugins show their custom subscribe actions
				$event->params['before_session_select'] = false;
				Yii::app()->event->raise('OnBeforeCourseSelfSubscribe', $event);
				if ($event->shouldPerformAsDefault() && !empty($event->return_value)) {
					//some plugins may not need normal subscribe actions .. let them avoid those
					if (isset($event->return_value['redirect'])) { $this->redirect($event->return_value['redirect'], true); }
					if (isset($event->return_value['continue']) && !$event->return_value['continue']) return;
				}
				// end event

				$waiting = 0;
				if ($course->allow_overbooking && $session->isMaxEnrolledReached()) {
					$waiting = 1;
				}

				// subscribe user; the method checks if it should set the status to waiting
				$this->_subscribeUser($course_id, $waiting, $sessionId);

				$setCusStatus = false;
				if ($course->subscribe_method != LearningCourse::SUBSMETHOD_FREE || $waiting) {
					$setCusStatus = LtCourseuserSession::$SESSION_USER_WAITING_LIST;
				}

				if (!$course->selling) {
					// enroll to session
					if (LtCourseuserSession::enrollUser(Yii::app()->user->id, $sessionId, $setCusStatus))
						echo CHtml::link('', $myCoursesUrl, array('class' => 'auto-close'));
				}
			}
			else {
				/* @var $cs CClientScript */
				$cs = Yii::app()->getClientScript();
				$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/classroom.css');

				$showOverbookedSessions = ($course->allow_overbooking);
				$dataProvider = LtCourseSession::model()->sessionEnrollmentDataProvider($course->getPrimaryKey(), $showOverbookedSessions, true);

				if (0 == $dataProvider->getTotalItemCount()) {
					$already_subscribed = LearningCourseuser::model()->isSubscribed(Yii::app()->user->id, $course->getPrimaryKey()) && !Yii::app()->user->getIsGuest();
					echo $this->renderPartial('_course_error_modal', array(
						'course' => $course,
						'message' => Yii::t('classroom', 'This course has no sessions available at this time. Please come back later.'),
						'already_subscribed' => $already_subscribed
						), true, true);
				} else {
					// Trigger event to let plugins show their custom subscribe actions
					$event->params['before_session_select'] = true;
					Yii::app()->event->raise('OnBeforeCourseSelfSubscribe', $event);
					if ($event->shouldPerformAsDefault() && !empty($event->return_value)) {
						//some plugins may not need normal subscribe actions .. let them avoid those
						if (isset($event->return_value['redirect'])) { $this->redirect($event->return_value['redirect'], true); }
						if (isset($event->return_value['continue']) && !$event->return_value['continue']) return;
					}

					echo $this->renderPartial('_select_session', array(
						'course' => $course,
						'dataProvider' => $dataProvider,
						'codes' => $codes
						), true, true);
				}
			}
		} else if ($course->isWebinarCourse()) {
			$sessionId = intval($_POST['courseSessionId']);
			/* @var $session WebinarSession */
			$session = WebinarSession::model()->findByPk($sessionId);

			if (Yii::app()->request->isPostRequest && $session) {
				// Trigger event to let plugins show their custom subscribe actions
				$event->params['before_session_select'] = false;
				Yii::app()->event->raise('OnBeforeCourseSelfSubscribe', $event);
				if ($event->shouldPerformAsDefault() && !empty($event->return_value)) {
					//some plugins may not need normal subscribe actions .. let them avoid those
					if (isset($event->return_value['redirect'])) { $this->redirect($event->return_value['redirect'], true); }
					if (isset($event->return_value['continue']) && !$event->return_value['continue']) return;
				}
				// end event

				$waiting = 0;
				if ($course->allow_overbooking && $session->isMaxEnrolledReached()) {
					$waiting = 1;
				}

				// subscribe user; the method checks if it should set the status to waiting
				$this->_subscribeUser($course_id, $waiting);

				$setCusStatus = false;
				if ($course->subscribe_method != LearningCourse::SUBSMETHOD_FREE || $waiting) {
					$setCusStatus = WebinarSessionUser::$SESSION_USER_WAITING_LIST;
				}

				if (!$course->selling) {
					// enroll to session
					if (WebinarSessionUser::enrollUser(Yii::app()->user->id, $sessionId, $setCusStatus))
						echo CHtml::link('', $myCoursesUrl, array('class' => 'auto-close'));
				}
			}
			else {
				/* @var $cs CClientScript */
				$cs = Yii::app()->getClientScript();
				$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/webinar.css');
				$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/admin.css');

				$showOverbookedSessions = ($course->allow_overbooking);
				$dataProvider = WebinarSession::model()->sessionEnrollmentDataProvider($course->getPrimaryKey(), $showOverbookedSessions, true);

				if (0 == $dataProvider->getTotalItemCount() && !isset($_GET['ajax'])) {
					$already_subscribed = LearningCourseuser::model()->isSubscribed(Yii::app()->user->id, $course->getPrimaryKey()) && !Yii::app()->user->getIsGuest();
					echo $this->renderPartial('_course_error_modal', array(
						'course' => $course,
						'message' => Yii::t('classroom', 'This course has no sessions available at this time. Please come back later.'),
						'already_subscribed' => $already_subscribed
					), true, true);
				} else {
					// Trigger event to let plugins show their custom subscribe actions
					$event->params['before_session_select'] = true;
					Yii::app()->event->raise('OnBeforeCourseSelfSubscribe', $event);
					if ($event->shouldPerformAsDefault() && !empty($event->return_value)) {
						//some plugins may not need normal subscribe actions .. let them avoid those
						if (isset($event->return_value['redirect'])) { $this->redirect($event->return_value['redirect'], true); }
						if (isset($event->return_value['continue']) && !$event->return_value['continue']) return;
					}

					echo $this->renderPartial('_select_webinar_session', array(
						'course' => $course,
						'dataProvider' => $dataProvider,
						'codes' => $codes
					), true, true);
				}
			}
		} else {
	        $subscriptionApproved = Yii::app()->request->getParam('subscription_approved', false);
	        if ($subscriptionApproved) {
		        // Trigger event to let plugins show their custom subscribe actions
		        Yii::app()->event->raise('OnBeforeCourseSelfSubscribe', $event);
		        if ($event->shouldPerformAsDefault() && !empty($event->return_value)) {
			        //some plugins may not need normal subscribe actions .. let them avoid those
			        if (isset($event->return_value['redirect'])) { $this->redirect($event->return_value['redirect'], true); }
			        if (isset($event->return_value['continue']) && !$event->return_value['continue']) return;
		        }
		        // end event

		        $success = $this->_subscribeUser($course_id, 0);

				// this is from the event result, no affect to the behavior of the product
				if (isset($_SESSION['codesEntered'])) unset($_SESSION['codesEntered']);
		        /**
		         * Some plugins can intercept this action and might render an additional screen in the enroll dialog,
		         * therefore the 'subscribe' button must have the 'ajax' class to allow this.
		         * As a consequence, we have to make the redirect using javascript.
		         */
		        $url = $myCoursesUrl;
		        // Check this advanced option (and if enrollment was successfull) and redirect the user directly to the course, if option is "on"
		        if ($success && Settings::get('show_course_after_enrollment', 'off') === 'on')
				{
					$courseModel = LearningCourse::model()->findByPk($course_id);
					if($courseModel->subscribe_method == LearningCourse::SUBSMETHOD_FREE)
                        $url = Docebo::createAppUrl('lms:player', array('course_id'=>$course_id));
		        }
		        echo CHtml::script("window.location.href = '$url';");
	        }else{

		        $already_subscribed = LearningCourseuser::model()->isSubscribed(Yii::app()->user->id, $course->getPrimaryKey()) && !Yii::app()->user->getIsGuest();
		        if($already_subscribed){
			        echo $this->renderPartial('_course_error_modal', array(
				        'course' => $course,
				        'message' => false,
				        'already_subscribed' => $already_subscribed
			        ), true, true);
		        }else{
			        // Get number of subscribers
			        $subscribersCount = $course->getTotalSubscribedUsers();

			        // Language related data
			        $langCode = $course->lang_code; // Not browser code!!!
			        $langName = Lang::getNameByCode($langCode);

			        // Preview Ajax action URL (used in details dialog to load preview/demo html)
			        $previewActionUrl = $this->createUrl('axCoursePreview', array('id' => $course_id));

			        // Demo media type
			        $demoType = $course->getDemoMediaCategory();
			        if ($demoType == LearningCourse::$DEMO_MEDIA_CATEGORY_UNKNOWN) {
				        $demoFileName = $course->course_demo;
				        $storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_COURSEDOCS);
				        $previewActionUrl = $storageManager->fileUrl($demoFileName);
			        }

			        // Overbooking
			        $isOverbooked = $course->isOverbooked();

			        $coursePolicy = $course->getCoursePolicy();


			        $response = $this->renderPartial("_ax_details", array(
				        'courseModel' => $course,
				        'subscribersCount' => $subscribersCount,
				        'isOverbooked' => $isOverbooked,
				        'langName' => $langName,
				        'previewActionUrl' => $previewActionUrl,
				        'demoType' => $demoType,
				        'showButton' => $coursePolicy["showButton"],
				        'label' => $coursePolicy["label"],
				        'codes' => $codes,
			        ), true, true);

			        echo $response;
		        }
	        }
		}
	}

	/**
	 * @param        $id
	 * @param string $type - The type of IDs we are passing: 'course' (default) or 'coursepath'
	 *
	 * @throws CException
	 */
	public function actionBuyMoreSeats($id, $type = 'course'){
		$courseIds = (array) $id;

		if($type == 'coursepath'){
			$cp = LearningCoursepath::model()->with('learningCourse')->findByPk($id);

			$courseIds = array();
			if($cp && $cp->learningCourse){
				foreach($cp->learningCourse as $course){
					$courseIds[] = $course->idCourse;
				}
			}
		}

		if(!CoreUserPU::isPUAndSeatManager()){
			throw new CException('This functionality is only available for Power Users for now');
		}

		$query = Yii::app()->getDb()->createCommand()
			->select('course.name, course.idCourse, course.max_num_subscribe, course.selling, t.total_seats, t.available_seats')
			->from(CoreUserPuCourse::model()->tableName().' t')
			->join(LearningCourse::model()->tableName().' course', 't.course_id=course.idCourse')
			->where('t.puser_id=:idUser', array(':idUser'=>Yii::app()->user->getIdst()))
			->andWhere(array('IN', 'course.idCourse', $courseIds))
			->queryAll();

		$courseDetails = array();

		foreach($query as $row){
			$courseDetails[$row['idCourse']] = $row;
		}

		$dataProvider = new CArrayDataProvider(array_values($courseDetails), array(
			'id'=>'idCourse'
		));

		if(isset($_POST['course']) && !empty($_POST['course'])){
			// User has entered amount of seats to buy for one
			// or more courses and submitted the form

			$coursesAndAmounts = (array) $_POST['course'];

			$courseIdsAssignedToPu = array_keys($courseDetails);

			foreach($coursesAndAmounts as $courseId=>$seatsToBuy){
				$seatsToBuy = intval($seatsToBuy);

				if($seatsToBuy <= 0) {
					continue; // no seats to buy entered
				}

				if(!in_array($courseId, $courseIdsAssignedToPu)){
					// User trying to buy seats in a course he is not assigned
					continue;
				}

				$thisCourseDetails = $courseDetails[$courseId];

				$maxSubscrQuota = $thisCourseDetails['max_num_subscribe'];
				$totalSeats = $thisCourseDetails['total_seats'];
				$availableSeats = $thisCourseDetails['available_seats'];

				if($maxSubscrQuota > 0){
					// This course has a "Max subscription quota" set

					// Respect the quota by preventing all PUs combined to buy more seats than this total quota
					// So if max_num_subscribe=10 and PU1 and PU2 buy 7 and 3 seats respectively,
					// PU3 should not be able to buy any more seats.

					// Additionally, changing the max subscr. quote is not applied retroactively,
					// so if the godadmin reduces the max subscr. quota from 10 to 5 for example,
					// and a PU has already purchased 10 seats before that, he will still have 10
					// seats available to distribute, not 5

					$combinedSeatsCountAlreadyBoughtByPUs = Yii::app()->getDb()->createCommand()
					                                          ->select('SUM(seats.total_seats)')
					                                          ->from(CoreUserPuCourse::model()->tableName().' seats')
					                                          ->join(LearningCourse::model()->tableName().' course', 'course.idCourse=seats.course_id AND seats.course_id=:idCourse', array(':idCourse'=>$courseId))
					                                          ->queryScalar();

					if(($combinedSeatsCountAlreadyBoughtByPUs+$seatsToBuy) > $maxSubscrQuota){
						// Don't allow to buy any more seats
						Yii::app()->user->setFlash('error', Yii::t('course', 'Max subscription quota reached for course: {course}', array('{course}'=>$thisCourseDetails['name'])));
						$this->renderPartial('buy_more_seats', array(
							'dataProvider'=>$dataProvider,
						));
						Yii::app()->end();
					}
				}


				$item = CourseseatsCartPosition::model()->findByPk($courseId); /* @var $item CourseseatsCartPosition */
				if(!$item) continue; // Course not found

				$item->setSeats(intval($seatsToBuy));

				// Add the item to cart
				$key = $item->getCartItemId();

				if(Yii::app()->shoppingCart->itemAt($key) instanceof IECartPosition)
					Yii::app()->shoppingCart->remove($key);

				Yii::app()->shoppingCart->put($item);
			}

			if(!Yii::app()->shoppingCart->isEmpty()){
				// We've added some items to the cart
				// now redirect to it
				echo '<a class="auto-close redirect" href="'.Docebo::createLmsUrl('cart/index').'"></a>';
				Yii::app()->end();
			}
		}

        if (Yii::app()->event->raise('OnRenderBuyMoreSeats', new DEvent($this, array('dataProvider' => $dataProvider)))) {
            $this->renderPartial('buy_more_seats', array(
                'dataProvider'=>$dataProvider,
            ), false, true);
        }
	}

	/**
	 * Controller Private method to subscribe a user to a course
	 *
	 * @param number $course_id
	 * @param number $waiting
	 *
	 */
	private function _subscribeUser($course_id, $waiting = 0, $sessionId = false) {

		$myCoursesUrl = $this->createUrl('site/index', array('opt' => 'mycourses'));

		$courseModel = LearningCourse::model()->findByPk($course_id);

		// Course not found ?
		if (!$courseModel) {
			Yii::app()->user->setFlash('error', Yii::t("catalogue", '_NO_COURSE_FOUND'));
			return;
		}

		// If this course is paid, User can't call this action directly
		if ($courseModel->selling) {
			Yii::app()->user->setFlash('error', Yii::t("standard", '_OPERATION_FAILURE'));
			return;
		}

		// Set waiting according to policy
		if ($courseModel->subscribe_method != LearningCourse::SUBSMETHOD_FREE) {
			$waiting = 1;
		}

		// Can current user subscribe this course ?
		$userCanSubscribe = $courseModel->canUserSubscribe(Yii::app()->user->id);

		$courseUserModel = new LearningCourseuser('self-subscribe');
		$result = $courseUserModel->subscribeUser(Yii::app()->user->id, Yii::app()->user->id, $course_id, $waiting, false, false, false, false, false, $sessionId);

		if ($result == true || ($result == false && $courseModel->course_type !== LearningCourse::ELEARNING && $courseUserModel->errorMessage == LearningCourseuser::$ALREADY_SUBSCRIBED)) {
			// Raise event
			$user = CoreUser::model()->findByPk(Yii::app()->user->id);
			Yii::app()->user->setFlash('success', Yii::t("standard", '_OPERATION_SUCCESSFUL'));
			return true;
		} else {
			if ($courseUserModel->errorMessage == LearningCourseuser::$ALREADY_SUBSCRIBED) {
				Yii::app()->user->setFlash('error', Yii::t("standard", 'You are already subscribed to this course'));
			} else {
				Yii::app()->user->setFlash('error', Yii::t("standard", '_OPERATION_FAILURE'));
			}
			return false;
		}
	}

	/**
	 * Subscribe a user in a course that is overbooked.
	 * It subscribes an user with the flag waiting = 1. So the admin has to approve the
	 * subscription request.
	 */
	public function actionOverbook() {

		$course_id = Yii::app()->request->getParam('course_id');
		// Subscribe in waiting mode
		$this->_subscribeUser($course_id, 1);

		$myCoursesUrl = $this->createUrl('site/index', array('opt' => 'mycourses'));
		$this->redirect($myCoursesUrl, true);
	}

	private function learningCatalogsDefined() {
		$criteria = new CDbCriteria();
		$criteria->compare('e.type_of_entry', 'course');
		$criteria->with = array(
			'catalog_entries' => array('alias' => 'e', 'select' => false),
		);

		$activeCatalogsCount = LearningCatalogue::model()->findAll($criteria);

		return ($activeCatalogsCount > 0);
	}

	public function getMultiCatalogData($idUser, $page = 0, $pageSize/*, $fltType = '', $fltQuery = ''*/, $_flt) {

		$fltType = (is_array($_flt) && isset($_flt['type']) ? $_flt['type'] : '');
		$fltQuery = (is_array($_flt) && isset($_flt['query']) ? $_flt['query'] : '');
		$fltCategory = (is_array($_flt) && isset($_flt['category']) ? $_flt['category'] : '');

		// Get all authentication Ids for the user
		$authManager = Yii::app()->authManager;
		$idsts = $authManager->getAuthAssignmentsIds($idUser);

		$criteria = new CDbCriteria();
		$criteria->with = array(
			"catalog_members"=>array('joinType'=>'JOIN'),
		);

		// Order catalogs by their name
		$criteria->order = "t.name ASC, t.description ASC";

		// A quite big workaround. Please don't touch if not needed!!! (questions: Dzhuneyt)
		// Since a catalog may have a "orgchart+descendats" assigned to it,
		// walk though the current user's groups upwards to their parent
		// groups and get those groups' OCD IDs. Then if one of those IDs
		// match an ID assigned to a catalog, we should display this catalog
		// to the user, since the user is a member of a descendant group of a
		// "group+descendants" assigned to the catalog.
		$idsts = array_merge($idsts, CoreOrgChartTree::getUserOCDArr($idUser));
		$idsts = array_unique($idsts);

		$criteria->compare('catalog_members.idst_member', $idsts);

		$userAvailableCatalogs = LearningCatalogue::model()->findAll($criteria);

		if (count($userAvailableCatalogs) > 0) {
			// TRIGGER: Show Multiple catalogs: ON
			$showMultiCatalog = true;
		} else {
			// TRIGGER: Show Multiple catalogs: ON
			// because user is not able to see ANY course (non-classroom) that is part of ANY catalog
			// So.. no point showing multi catalogs - they will be empty
			$showMultiCatalog = false;
		}


		// Get out if we are NOT going to show Multi Catalogs
		$result = array('showMultiCatalog' => $showMultiCatalog);
		if (!$showMultiCatalog) {
			return $result;
		}

		// Compose results to return
		// Array of Array("catalog" => LearningCatalogue, "courses" => Array of Array( "<course-id>" => LearningCourse))
		// Example:
		// $catalogs[0] = array("catalog" => <catalog-model>, "courses" => array("1" => <course-1-model>, "6" => <course-6-model>))
		// $catalogs[1] = array("catalog" => <catalog model>, "courses" => array("4" => <course-4-model>, "6" => <course-6-model>))
		$catalogs = array();

		$subscribedCourseIds = LearningCourse::getCourseIdsByUser($idUser);

		// Collect "catalog entries" separately to reduce complexity
		foreach($userAvailableCatalogs as $item){

			// Reinitialize catalog element array to avoid merging with previous entries
			$catalogElement = array();

			$catalogElement["catalog"] = $item;

			$criteria = new CDbCriteria();

			$courseOnCondition = 't.idEntry=course.idCourse AND t.type_of_entry="course" AND course.status IN (:effective, :available)';
			$criteria->params[':effective'] = LearningCourse::$COURSE_STATUS_EFFECTIVE;
			$criteria->params[':available'] = LearningCourse::$COURSE_STATUS_AVAILABLE;

			$coursePathOnCondition = 't.idEntry=coursepath.id_path AND t.type_of_entry="coursepath" ';

			if(!Yii::app()->user->getIsGuest()){
				// Not viewing external catalog, we are in internal catalog

				$userLearningPlanIds = LearningCoursepathUser::getEnrolledLearningPlanIdsByUser(Yii::app()->user->getIdst());

				// In this case, show LPs only if one of these is met:
				// 1. User is enrolled in them (the LP, not necessary in courses in it)
				// 2. User not enrolled in LP and LP is put on sale
				// LPs not put on sale and user not enrolled in them are hidden
				if(!empty($userLearningPlanIds)){
					$coursePathOnCondition .= ' AND (coursepath.id_path IN ('.implode(',', $userLearningPlanIds).') OR coursepath.visible_in_catalog=1)';
				}
			}else{
				// User anonymous (e.g. viewing External catalog)
				// Only get paid LPs, since you can't do much from the external
				// catalog, e.g. you can't subscribe to courses in LP not put on sale
				$coursePathOnCondition .= ' AND coursepath.visible_in_catalog=1 ';
			}

			// Get only FREE courses if e-commerce is disabled
			$eCommerceEnabled = Settings::get("module_ecommerce", "off") == "on";
			if(!$eCommerceEnabled){
				$fltType = CoursesGrid::$FILTER_TYPE_FREE;
			}

			if ($fltType == CoursesGrid::$FILTER_TYPE_PAID) {
				$courseOnCondition .= ' AND course.selling=1 ';
				$coursePathOnCondition .= ' AND coursepath.is_selling=1 ';
			} elseif ($fltType == CoursesGrid::$FILTER_TYPE_FREE) {
				$courseOnCondition .= ' AND course.selling=0 ';
				$coursePathOnCondition .= ' AND coursepath.is_selling=0 ';
			}

			// Search text, search by name, other text fields can be searched
			if (!empty($fltQuery)) {
				$courseOnCondition .= ' AND (course.name LIKE :search OR course.description LIKE :search) ';
				$coursePathOnCondition .= ' AND coursepath.path_name LIKE :search ';
				$criteria->params[':search'] = '%'.$fltQuery.'%';
			}

			// Filtering by category
			if (Settings::get('catalog_use_categories_tree') === 'on' && !empty($fltCategory)) {
				$categories = array((int)$fltCategory);
				//find subcategories
				$_node = LearningCourseCategoryTree::model()->findByPk($fltCategory);
				$_descendants = $_node->descendants()->findAll();
				if (!empty($_descendants)) {
					foreach ($_descendants as $_descendant) {
						$categories[] = (int)$_descendant->idCategory;
					}
				}
				if (!empty($fltCategory) && (int)$fltCategory > 0) {
					$courseOnCondition .= ' AND course.idCategory IN ('.implode(',',$categories).') ';
				}
			}

			if(!PluginManager::isPluginActive('ClassroomApp')){
				// If Classroom app is NOT active, show only elearning courses
				// (ignore classroom type courses)
				$courseOnCondition .= " AND course.course_type='elearning' ";
			}

			$excludedCourses = array();
			Yii::app()->event->raise('OnCatalogQueryCourses', new DEvent($this, array(
				'excludedCourses' => &$excludedCourses
			)));
			if (!empty($excludedCourses)) {
				$courseOnCondition .= " AND course.idCourse NOT IN (".implode(',', $excludedCourses).") ";
			}

			// Only get entries from the currently iterated catalog
			$criteria->compare('t.idCatalogue', $item->idCatalogue);

			// Limit (Multicatalog is like MyCourses: show first N then link to More Courses, so we need only first N
			$criteria->limit = 3;

			// WATCH OUT WITH THE JOINS BELOW
			// We use Left Join to get both Courses
			// and Learning Plans (regular JOIN will get only one or the other)
			// and then filter rows that have course.idCourse or coursepath.id_path
			// (if both these columns are null, remove the joined row)
			$criteria->with = array(
				'course'=>array(
					'on'=>$courseOnCondition,
					'joinType'=>'LEFT JOIN'
				),
				'coursepath'=>array(
					'on'=>$coursePathOnCondition,
					'joinType'=>'LEFT JOIN'
				),
			);
			$criteria->addCondition('course.idCourse IS NOT NULL OR coursepath.id_path IS NOT NULL');

			$criteria->order = 'course.name, coursepath.path_name ASC';

			$catalogEntries = LearningCatalogueEntry::model()->findAll($criteria);

			// But also find out how much courses there are total in the catalog
			// since we show/hide the "View more courses" tile based on that
			// (if there are only 3 or less, hide it)
			$criteria->limit = null;
			$catalogElement['totalCoursesInCatalog'] = LearningCatalogueEntry::model()->count($criteria);

			// If courses are available, add them to 'catalog' element
			$catalogElement["courses"] = array();
			if ($catalogEntries) {
				foreach ($catalogEntries as $catalogEntry) {
					if($catalogEntry->type_of_entry=='course'){
						if(in_array($catalogEntry->course->idCourse, $subscribedCourseIds)){
							$catalogEntry->course->isCurrentUserSubscribedTo = true;
						}
					}
					$catalogElement['courses'][] = $catalogEntry;
				}
				$catalogElement["courses"] = array_values($catalogElement["courses"]); // renumber

				// Add the catalog with its entries to the final array
				// to be returned (only done here, after the check if the
				// catalog has entries because we want to exclude empty ones)
				$catalogs[] = $catalogElement;
			}
		}

		return array(
			'showMultiCatalog' => true, // true even there are no courses in the catalog, just show empty
			'catalogs' => $catalogs,
		);
	}

	/**
	 * Show Course Autoregistration form
	 */
	public function actionAutoreg() {

		// Get the code, if any
		$code = Yii::app()->request->getParam('autoreg_code', false);

		// If we got the code... try to handle it
		if ($code) {

			// Try to subscribe, also try to use the new Subscription Cose Plugin (last true parameter)
			$errorMessage = LearningCourseuser::subscribeUserByOldCode($code, Yii::app()->user->id, true);
			if (!$errorMessage) {
				Yii::app()->user->setFlash('success', Yii::t("standard", '_OPERATION_SUCCESSFUL'));
				$this->redirect($this->createUrl("site/index"));
				Yii::app()->end();
			}
		}

		$this->render('autoreg', array(
			'feedback' => $errorMessage,
		));
		Yii::app()->end();

	}

	/**
	 * Ajax action to rate a course (when using the star rating control)
	 */
	public function actionAxRate() {

		if (Yii::app()->request->isAjaxRequest)
		{
			$idUser = Yii::app()->user->getIdst();
			$idCourse = Yii::app()->request->getParam('idCourse');

			if(Yii::app()->user->getIsGuest()) {
				// Prevent rating spam/frau
				// 1. Using course specific cookie
				// 2. Last IP rated this course in less than <N> seconds
				$rating_cookie_name = 'mprtcid' . $idCourse;
				// Check if cookie is set (like:  mprtcid49)
				if(isset($_COOKIE[$rating_cookie_name]))
					return;
			}

			$model = LearningCourse::model()->with('learningCourseRating')->findByPk($idCourse);

			$value = intval( Yii::app()->request->getParam('value') );

			if ($model) {
				try {
					// Write a cookie to indicate that this User Agent already rated this course
					//$cookieName = 'mprtcid' . $idCourse;
					//setcookie($cookieName, time(), time()+60*60*24*30); // 30 days
					if($model->saveRating($idUser, $value)) {
						$rating = $model->getRating();
					}
					$course_rating = LearningCourseRating::model()->findByPk($idCourse)->rate_average;
					//echo $course_rating;
					$tmpRes = '';
					for($i=0; $i<5; $i++)
						$tmpRes .= '<div role="text" class="star-rating star-rating'. ($course_rating > $i ? "-on" : "") .'" id="course-rating-58_'.$i.'"><a title="'.$i.'">'.$rating.'</a></div>';
					echo json_encode( array( 'stars' => $tmpRes, 'rating' => $course_rating ) );

				}
				catch (Exception $ex) {
					Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
				}
			}
		}

	}

	private function getRedirectUrlAndSetFlash($errorCode, $course = null, $msg = ''){
		$redirectUrl = '';
		$hasNewThemeActivated = Yii::app()->legacyWrapper->hydraFrontendEnabled();
		$courseSlug  = ($course->slug_name && !empty($course->slug_name)) ? $course->slug_name : Yii::app()->urlManager->slugify($course->name);
		switch ($errorCode) {
			case '2001':
				//redirect to home and operation failure
				Yii::app()->user->setFlash('error', Yii::t("standard", '_OPERATION_FAILURE'));
				$redirectUrl = ($hasNewThemeActivated) ? Docebo::createHydraUrl('learn', 'home', array('msg' => $errorCode)) : Docebo::createLmsUrl("site/index");
				break;
			case '2002':
				//redirect to home and operation failure
				if ($msg != '')
					Yii::app()->user->setFlash('error', Yii::t('course',$msg));
				else
					Yii::app()->user->setFlash('error', Yii::t('course','Deep linking not working for this course. Please, contact your administrator for further details.'));

				$redirectUrl = ($hasNewThemeActivated) ? Docebo::createHydraUrl('learn', 'home', array('msg' => $errorCode)) : Docebo::createLmsUrl("site/index");
				break;
			case '2003':
				//redirect to course details page
				$redirectUrl = ($hasNewThemeActivated) ? Docebo::createHydraUrl('learn', 'course/view/' . $course->course_type . '/' . $course->idCourse . '/' . $courseSlug) :  Docebo::createLmsUrl("course/details", array('id' => $course->idCourse));
			break;
			case '2004':
				//redirect to mycourses on success
				Yii::app()->user->setFlash('success', Yii::t("course", 'You have been successfully enrolled to the course <strong>{name}</strong>.', array('{name}' => $course->name)));
				$redirectUrl = ($hasNewThemeActivated) ? Docebo::createHydraUrl('learn', 'mycourses', array('msg' => $errorCode)) : Docebo::createLmsUrl("site/index", array('opt' => 'fullmycourses'));
				break;
			default:
				//redirect to player on success
				$redirectUrl = ($hasNewThemeActivated) ? Docebo::createHydraUrl('learn', 'course/' .  $course->idCourse . '/' . $courseSlug) : Docebo::createLmsUrl('player', array('course_id' =>  $course->idCourse));
				break;
		}

		return $redirectUrl;
	}

    public function actionDeeplink(){
        $course_id   = Yii::app()->getRequest()->getParam('course_id');
        $gid         = Yii::app()->getRequest()->getParam('generated_by');
        $hash        = Yii::app()->getRequest()->getParam('hash');
        $key 		 = Yii::app()->params['deeplink_secret_key'];
        $hashValid   = hash('sha1', $course_id . "," . $gid . "," . $key);

		// redirect code, used
		$redirectCode = '';
		$msg = '';
		if($hashValid===$hash){
            $courseModel 	= LearningCourse::model()->findByPk($course_id);

            if($courseModel->deep_link==0){
				$redirectCode = '2002';
            } else {
				$isSubscribed = LearningCourseuser::isSubscribed(Yii::app()->user->id, $courseModel->idCourse);
				if (!$isSubscribed) {
					if (!Yii::app()->user->getIsGodadmin() && $courseModel->show_rules == LearningCourse::DISPLAY_ONLY_SUBSCRIBED_USERS) {
						$redirectCode = '2001';
					}else {
						$courseIsPaid = $courseModel->selling;
						$subscriptionIsOpen = ($courseModel->can_subscribe == LearningCourse::COURSE_SUBSCRIPTION_CLOSED) ? false : (($courseModel->can_subscribe == LearningCourse::COURSE_SUBSCRIPTION_OPEN) ? true : $courseModel->checkSubscribtionDates());
						$maxSubsReached = $courseModel->course_type != LearningCourse::TYPE_ELEARNING || ($courseModel->max_num_subscribe == 0 || $courseModel->max_num_subscribe > $courseModel->getTotalSubscribedUsers(false,
								false, false)) ? false : true;
						$overbooking = $courseModel->allow_overbooking;
						$enrollPolicy = $courseModel->subscribe_method;

						$waiting = 0;
						$shouldEnroll = false;

						if (!$courseIsPaid && $subscriptionIsOpen && (!$maxSubsReached || ($maxSubsReached && $overbooking)) && $enrollPolicy != LearningCourse::SUBSMETHOD_ADMIN) {
							$shouldEnroll = true;
							if ($enrollPolicy == LearningCourse::SUBSMETHOD_FREE && !$maxSubsReached) {
								/*******CASE 1
								 * And the course is free (not on sale)
								 * And subscriptions are open
								 * And the subscription quota is not reached
								 * And the enrollment policy is "Free"
								 *******/
								$redirectCode = '';
							} else {
								/*******CASE 2
								 * And the course is free (not on sale)
								 * And subscriptions are open
								 * And the subscription quota is not reached
								 * And the enrollment policy is "Pending Admin Approval"
								 *******/
								$redirectCode = '2004';
								$waiting = 1;
							}
						} else {
							if ($courseIsPaid && $subscriptionIsOpen && (!$maxSubsReached || ($maxSubsReached && $overbooking)) && $enrollPolicy != LearningCourse::SUBSMETHOD_ADMIN) {
								$redirectCode = '2003';
							} else {
								/*******CASE 3
								 * And the course is free (not on sale)
								 * And subscriptions are closed
								 * Or subscriptions are not available today
								 * Or the subscription quota has been reached
								 * Or the enrollment policy is "Only admin can subscribed users"
								 *******/
								/*******CASE 4
								 * And the course is free (not on sale)
								 * And subscriptions are open
								 * And the subscription quota has been reached
								 * And overbooking is not allowed for this course
								 *******/
								$redirectCode = '2002';
							}
						}
						if ($shouldEnroll) {
							$courseUserModel = new LearningCourseuser('self-subscribe');

							$result = $courseUserModel->subscribeUser(Yii::app()->user->id, Yii::app()->user->id, $course_id, $waiting, false, false, false, false, false, false, $gid);
							$canEnter = $courseUserModel->canEnterCourse();

							if (!$canEnter['can']) {
								$waiting = 1;
								$redirectCode = '2002';
								switch ($canEnter['reason']) {
									case 'prerequisites':
										$msg = Yii::t('organization', '_ORGLOCKEDTITLE');
										break;
									case 'waiting':
										$msg = Yii::t('catalogue', '_WAITING_APPROVAL');
										break;
									case 'user_status':
									case 'course_status':
									case 'subscription_expired':
									case 'subscription_not_started':
									case 'course_date':
									default:
									$msg = Yii::t('storage', '_LOCKED');
										break;
								}
							}
							if (!$result) {
								$redirectCode = '2001';
							} else {
								if ($waiting == 1) {
									$redirectCode = '2004';
								}
							}
						}
					}
				} else {
					$subscriptionModel = LearningCourseuser::model()->findByAttributes(array(
						'idCourse' => $course_id,
						'idUser' => Yii::app()->user->id
					));
					if ($subscriptionModel->waiting == 1) {
						$redirectCode = '2004';
					}
				}

			}

            $this->redirect($this->getRedirectUrlAndSetFlash($redirectCode, $courseModel, $msg) , true);
        }else{
            $this->redirect($this->getRedirectUrlAndSetFlash('2001'), true);
        }
    }
}
