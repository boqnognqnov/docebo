<?php

class AmazonSnsController extends Controller {

	
	public function actionIndex() {
	}
	

	/**
	 * This action will be subscribed to Transcoding Pipeline(s) Topics
	 */
	public function actionTranscoding() {

		// Get data from the request payload
		$payload = file_get_contents('php://input');
		$data = CJSON::decode($payload);

		// Get all headers
		$headersOriginal = getallheaders();

		// Make headers's keys and values LOWER case.
		$headers = array();
		foreach ($headersOriginal as $key => $value) {
			$headers[strtolower($key)] = $value;
		}

		// Get Message type
		$snsMessageType =  isset($headers['x-amz-sns-message-type']) ? $headers['x-amz-sns-message-type'] : false;
		if (!$snsMessageType) {
			Yii::app()->end();
		} 

		// SNS SUBSCRIPTION COnfirmation request: Is this a Subscription Confirmation message???
		if ($snsMessageType == 'SubscriptionConfirmation') {
			// Get storage; it will give us the S3 credentials and region
			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_VIDEO);
			AmazonSnsHelper::confirmSubscription($storage->getS3Key(), $storage->getS3Secret(), $storage->getS3Region(), $data['Token'], $data['TopicArn']);
			Yii::app()->end();
		}


		// NOTIFICATION: Transcoding Notification received??
		if ($snsMessageType == 'Notification') {
			// Get the message from payload
			$message  = CJSON::decode($data['Message']);

			// Get 'marker' from URL, because that's the way we build the URL/Endpoint
			// As 'marker' we use the Video Filename (without the extension). Just saying.
			$marker = Yii::app()->request->getParam("marker", false);
			if (!$marker) {
				Yii::app()->end();
			}

			// Get the Key (actually the output file, i.e. OUR file) from message
			// Index 0: because JOB may have MANY outputs. We have ONE only.
			$key = $message['outputs'][0]['key'];

			// If Key from message and Marker from URL does not match (partially), that means this is a
			// notification coming from some another job; Another URL will handle it (another LMS), because
			// Amazon SNS will send notifications to ALL URLs... but no worries.. we will unsubscribe our LMS in a second.. later...
			// Ideally, Topic Subscription should be empty (when all LMSs are idle) 
			if (strpos($key, $marker) === false) {
			    Yii::app()->end();
			}

			// Get STATE
			$state = $message['state'];

			$collection = Yii::app()->request->getParam("collection", "");
			$switcher = (empty($collection)
				? CFileStorage::COLLECTION_LO_VIDEO //default collection for video files
				: $collection);


			// Different collections must be handled in different ways
			switch ($switcher) {

				case CFileStorage::COLLECTION_LO_VIDEO:

					// Get VideoConverter object to retrieve Transcoding Pipeline Notifications Topics;
					// We are going to UNSUBSCRIBE our URL from all of them
					// See later in the code!
					$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_VIDEO);
					$vc = new VideoConverter($storage);

					// Find the file in video table
					$criteria = new CDbCriteria();
					$criteria->addCondition('video_formats LIKE :marker');
					$criteria->params[':marker'] = "%$marker%";
					$videoObj = LearningVideo::model()->find($criteria);
					if (!$videoObj) {
						Yii::log("Amazon Elastic Transcoder notified us about a job, but the video file does not match our database. Removing the converted file to keep our space clean!", CLogger::LEVEL_INFO);
						Yii::log('==== Amazon have sent us the following information ===== ', CLogger::LEVEL_INFO);
						Yii::log(print_r($data, true), CLogger::LEVEL_INFO);
						Yii::log('======================================================== ', CLogger::LEVEL_INFO);

						$storage->remove($key);
						Yii::app()->end();
					}
					// Find the LO corresponding to this Video Object (bind to idResource)
					$criteria = new CDbCriteria();
					$criteria->addCondition("(idResource=:idVideo) AND (objectType='" . LearningOrganization::OBJECT_TYPE_VIDEO . "') AND (id_object IS NULL)");
					$criteria->params[':idVideo'] = $videoObj->id_video;
					$lo = LearningOrganization::model()->find($criteria);
					$processWithLO = true;
					if (!$lo) {
						Yii::log("Amazon Elastic Transcoder notified us about a job, but the Learning Object associated to the video file does not exists anymore. Removing the converted file to keep our space clean!", CLogger::LEVEL_INFO);
						Yii::log('==== Amazon have sent us the following information ===== ', CLogger::LEVEL_INFO);
						Yii::log(print_r($data, true), CLogger::LEVEL_INFO);
						Yii::log('======================================================== ', CLogger::LEVEL_INFO);

						Yii::log('Trying to find if there is some Learning Repository Object Version, witch has that video as a resource', CLogger::LEVEL_INFO);

						$criteria = new CDbCriteria();
						$criteria->addCondition('(id_resource = :idVideo) AND (object_type = "' . LearningOrganization::OBJECT_TYPE_VIDEO . '")');
						$criteria->params[':idVideo'] = $videoObj->id_video;
						$repoVersion = LearningRepositoryObjectVersion::model()->find($criteria);

						if ($repoVersion) {
							if ($state == 'COMPLETED') {
								Yii::log("Amazon Elastic Transcoder notified us about COMPLETED transcoding. Good job!", CLogger::LEVEL_INFO);
								Yii::log('==== Amazon have sent us the following information ===== ', CLogger::LEVEL_INFO);
								Yii::log(print_r($data, true), CLogger::LEVEL_INFO);
								Yii::log('======================================================== ', CLogger::LEVEL_INFO);
								$repoVersion->visible = 1;
							} else {
								$repoVersion->visible = -2;

								// Log IF ERROR properly so we have clue what is going on here
								if ($state == 'ERROR') {
									Yii::log("Looks like there was some error while we were trying to convert a Video file.", CLogger::LEVEL_INFO);
									Yii::log('==== Amazon have sent us the following information ===== ', CLogger::LEVEL_INFO);
									Yii::log(print_r($data, true), CLogger::LEVEL_INFO);
									Yii::log('======================================================== ', CLogger::LEVEL_INFO);
								}
							}

							if ($repoVersion->save()) {
								$repoVersion->changeVisibleStatusToPushedLO();
								$processWithLO = false;
							}
						} else {
							$storage->remove($key);
							Yii::app()->end();
						}
					}

					// LO has been found, now do whatever we have to do ON JOB notification, based on state, etc.
					//    Unhide LO
					//    Send email?
					//    ...
					// Remove "Converting..." from title
					//$lo->title = preg_replace("# \[".Yii::t('standard', 'Converting')."...\] #", "", $lo->title);

					if ($processWithLO) {
						if ($state == 'COMPLETED') {
							Yii::log("Amazon Elastic Transcoder notified us about COMPLETED transcoding. Good job!", CLogger::LEVEL_INFO);
							Yii::log('==== Amazon have sent us the following information ===== ', CLogger::LEVEL_INFO);
							Yii::log(print_r($data, true), CLogger::LEVEL_INFO);
							Yii::log('======================================================== ', CLogger::LEVEL_INFO);
							$lo->visible = 1;

							Yii::app()->event->raise('onAmazonTranscodingComplete', new DEvent($this, array(
								'marker' => $marker,
								'outputKeyPrefix' => $message['outputKeyPrefix'],
							)));
						} else {
							Yii::log("Amazon Elastic Transcoder notified us about NON-completed job status: " . $state, CLogger::LEVEL_INFO);
							$lo->visible = -2;
							//$lo->title = $lo->title . " [transcoding error]";

							// Log IF ERROR properly so we have clue what is going on here
							if ($state == 'ERROR') {
								Yii::log("Looks like there was some error while we were trying to convert a Video file.", CLogger::LEVEL_INFO);
								Yii::log('==== Amazon have sent us the following information ===== ', CLogger::LEVEL_INFO);
								Yii::log(print_r($data, true), CLogger::LEVEL_INFO);
								Yii::log('======================================================== ', CLogger::LEVEL_INFO);
							}

						}
						$lo->saveNode();
					}

					break;

				case CFileStorage::COLLECTION_WEBINAR_RECORDING:

					// Get VideoConverter object to retrieve Transcoding Pipeline Notifications Topics;
					// We are going to UNSUBSCRIBE our URL from all of them
					// See later in the code!
					$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_WEBINAR_RECORDING);
					$vc = new VideoConverter($storage);

					// Find the file in recordings table
					$criteria = new CDbCriteria();
					$criteria->addCondition('path LIKE :marker');
					$criteria->params[':marker'] = "%$marker%";
					$recording = WebinarSessionDateRecording::model()->find($criteria);
					if (!$recording) {
						Yii::log("Amazon Elastic Transcoder notified us about a job, but the video file does not match our database. Removing the converted file to keep our space clean!", CLogger::LEVEL_INFO);
						Yii::log('==== Amazon have sent us the following information ===== ', CLogger::LEVEL_INFO);
						Yii::log(print_r($data, true), CLogger::LEVEL_INFO);
						Yii::log('======================================================== ', CLogger::LEVEL_INFO);

						$storage->remove($key);
						Yii::app()->end();
					}

					//retrieve conversion logging info
					$criteria = new CDbCriteria();
					$criteria->addCondition('filename LIKE :marker');
					$criteria->addCondition('status LIKE :status');
					$criteria->params[':marker'] = "%$marker%";
					$criteria->params[':status'] = 'transcoding';
					$conversionLog = WebinarConversion::model()->find($criteria);

					switch ($state) {
						case 'COMPLETED':
							// Updating recording status in the database
							$recording->status = WebinarSessionDateRecording::RECORDING_STATUS_COMPLETED;
							$recording->save();
							if (!empty($conversionLog)) {
								$conversionLog->status = 'success';
								$conversionLog->save();
							}
							break;
						case 'ERROR':
							// Updating recording status in the database
							$recording->status = WebinarSessionDateRecording::RECORDING_STATUS_FAILED;
							$recording->save();
							if (!empty($conversionLog)) {
								$conversionLog->status = 'error';
								$conversionLog->error = 'An error occurred while converting video file';
								$conversionLog->save();
							}
							break;
					}

					break;

				default:
					Yii::log("Amazon Elastic Transcoder notified us about a job, but the video file does not match our database. Removing the converted file to keep our space clean!", CLogger::LEVEL_INFO);
					Yii::log('==== Amazon have sent us the following information ===== ', CLogger::LEVEL_INFO);
					Yii::log(print_r($data, true), CLogger::LEVEL_INFO);
					Yii::log('======================================================== ', CLogger::LEVEL_INFO);

					$storage = CFileStorage::getDomainStorage($collection);
					$storage->remove($key);
					Yii::app()->end();
					break;

			}

			// Job is done, we don't need this URL to be notified anymore, Unsubscribe please...

			// Build this URL the EXACT same way it was build in VideoConverter (where we subscribed it to topics)
			$url = AmazonSnsHelper::getSnsTranscodingEndpoint($marker, $collection);

			// At the moment we are subscrining to COMPLETED and ERROR only
			AmazonSnsHelper::unsubscribeUrlFromTopic($storage->getS3Key(), $storage->getS3Secret(), $storage->getS3Region(), $url, $vc->snsTopicCompleted);
			AmazonSnsHelper::unsubscribeUrlFromTopic($storage->getS3Key(), $storage->getS3Secret(), $storage->getS3Region(), $url, $vc->snsTopicError);
			//AmazonSnsHelper::unsubscribeUrlFromTopic($storage->getS3Key(), $storage->getS3Secret(), $storage->getS3Region(), $url, $vc->snsTopicProgressing);
			//AmazonSnsHelper::unsubscribeUrlFromTopic($storage->getS3Key(), $storage->getS3Secret(), $storage->getS3Region(), $url, $vc->snsTopicWarning);


			Yii::app()->end();

			// WE ARE DONE
		}

		Yii::app()->end();
	}


}
