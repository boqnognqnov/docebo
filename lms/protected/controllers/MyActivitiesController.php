<?php

class MyActivitiesController extends Controller {

	/**
	 * (non-PHPdoc)
	 * @see CController::init()
	 */
	public function init() {
		parent::init();
		Yii::app()->event->on('cleanCertificateOutputBuffer', array($this, 'cleanCertificateOutputBuffer'));
	}

	public function cleanCertificateOutputBuffer($event)
	{
		$event->return_value['output_buffer'] = false;
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl',
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		return array(
			array('allow',
				'actions' => array('index', 'downloadCertificate'),
				'users' => array('@'),
			),
			array('allow',
				'actions' => array('axGetQuestionsAndAnswersContent', 'axGetSharingActivityContent'),
				'roles' => array('/lms/admin/report/view'),
			),
			array('allow',
				'actions' => array('userReport'),
				// The /framework/admin/usermanagement/view PU management check is removed for the LB-6332 ticked
				'expression' => '(Yii::app()->user->getIsGodadmin() || Yii::app()->user->getIsPU())'
			),
			array('deny',
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
			),
		);
	}




	//internal methods

	protected $_cached_user = null;

	/**
	 * Retrieve the idst of the reported user by page parameters. User AR model is cached for multiple calls.
	 * @return int the idst of the target user
	 * @throws CHttpException
	 */
	protected function getUserId($selectedUserId = null) {
		$idUser = Yii::app()->request->getParam('id_user', false);
		if (empty($idUser)) {
			//no user idst has been specified as page input parameter, check for other inputs
			$userid = Yii::app()->request->getParam('userid', false);
			if (!empty($userid)) {
				//an userid parameter has been specified as input parameter: retrieve the user model and its idst
				if (empty($this->_cached_user) || Yii::app()->user->getRelativeUsername($this->_cached_user->userid) != $userid) {
					$criteria = new CDbCriteria();
					$criteria->addCondition("userid LIKE :userid");
					$criteria->params[':userid'] = Yii::app()->user->getAbsoluteUsername($userid);
					$user = CoreUser::model()->find($criteria);
					if (empty($user)) { throw new CHttpException(500, Yii::t('register', '_ERR_INVALID_USER')); }
					$output = $user->getPrimaryKey();
					$this->_cached_user = $user;
				} else {
					$output = $this->_cached_user->getPrimaryKey();
				}
			} else {
				//no input has been passed to the page: fallback to current user
				$output = Yii::app()->user->id;
				if (empty($this->_cached_user) || $this->_cached_user->getPrimaryKey() != Yii::app()->user->id) {
					$this->_cached_user = Yii::app()->user->loadUserModel();
				}
			}
		} else {
			//the target user idst has been specified as page input parameter
			if (empty($this->_cached_user) || $this->_cached_user->getPrimaryKey() != $idUser) {
				$user = CoreUser::model()->findByPk($idUser);
				if (empty($user)) { throw new CHttpException(Yii::t('register', '_ERR_INVALID_USER')); }
				$output = $idUser;
				$this->_cached_user = $user;
			} else {
				$output = $idUser;
			}
		}

		if($selectedUserId && isset($userid) && empty($userid)){
			$user =  CoreUser::model()->findByPk( $selectedUserId);
			if($user){
				$output = $selectedUserId;
				$this->_cached_user = $user;
			}
		}

		return $output;
	}


	/**
	 * Retrieve the AR model of the reported user by page parameters. User AR model is cached for multiple calls.
	 * @return CoreUser the AR model of the target user
	 */
	protected function getUserModel($selectedUserId = null) {
		$this->getUserId($selectedUserId); //this is called just to generate cached user AR model (if user is invalid an exception is raised)
		return $this->_cached_user;
	}



	protected function getMenuList($showAdminActions = false, $from = false) {
		$list = array();
		$urlParams = array();
		if (!empty($from)) { $urlParams['from'] = $from; }
		if ($this->getUserId() != Yii::app()->user->id) { $urlParams['id_user'] = $this->getUserId(); }
		$action = ($showAdminActions ? 'userReport' : 'index');

		$urlParams['tab'] = 'statistics';
		$list[] = array(
			'id' => 'statistics',
			'label' => Yii::t('standard', '_STATISTICS'),
			'url' => $this->createUrl($action, $urlParams)
		);

		if (Settings::get('use_node_fields_visibility', 'off') == 'on') {
			$userModel = CoreUser::model()->findByPk($this->getUserId());
			$countFields = count($userModel->getVisibleFields());
		} else {
			$qry = "SELECT COUNT(*) FROM ".CoreUserField::model()->tableName()." WHERE invisible_to_user = 0";
			$countFields = Yii::app()->db->createCommand($qry)->queryScalar();
		}
		if ($countFields > 0) {
			$urlParams['tab'] = 'additional-info';
			$list[] = array(
				'id' => 'additional-info',
				'label' => Yii::t('myactivities', 'Additional info'),
				'url' => $this->createUrl($action, $urlParams)
			);
		}

		$urlParams['tab'] = 'courses';
		$list[] = array(
			'id' => 'courses',
			'label' => Yii::t('standard', '_COURSES'),
			'url' => $this->createUrl($action, $urlParams)
		);

		if (PluginManager::isPluginActive('ClassroomApp')) {
			$urlParams['tab'] = 'classrooms';
			$list[] = array(
				'id' => 'classrooms',
				'label' => Yii::t('classroom', 'Classrooms'),
				'url' => $this->createUrl($action, $urlParams)
			);
		}

		if (PluginManager::isPluginActive('CurriculaApp')) {
			$urlParams['tab'] = 'learning-plans';
			$list[] = array(
				'id' => 'learning-plans',
				'label' => Yii::t('myactivities', 'Learning plans'),
				'url' => $this->createUrl($action, $urlParams)
			);
		}

		// Hide this tab if Transcripts are NOT allowed in User Menu -> My Activities
		if (PluginManager::isPluginActive('TranscriptsApp')) {
			$urlParams['tab'] = 'external-activities';
			$list[] = array(
				'id' => 'external-activities',
				'label' => Yii::t('transcripts', 'External activities'),
				'url' => $this->createUrl($action, $urlParams)
			);
		}

		if (PluginManager::isPluginActive('GamificationApp')) {
			$urlParams['tab'] = 'badges';
			$list[] = array(
				'id' => 'badges',
				'label' => Yii::t('gamification', 'Badges'),
				'url' => $this->createUrl($action, $urlParams)
			);
		}
		

		$urlParams['tab'] = 'social';
		$list[] = array(
			'id' => 'social',
			'label' => Yii::t('myactivities', 'Social'),
			'url' => $this->createUrl($action, $urlParams)
		);

		$urlParams['tab'] = 'webinar';
		$list[] = array(
			'id' => 'webinar',
			'label' => Yii::t('webinar', 'Webinars'),
			'url' => $this->createUrl($action, $urlParams)
		);

		if (PluginManager::isPluginActive('CertificationApp')) {
			$urlParams['tab'] = 'certification';
			$list[] = array(
				'id' => 'certification',
				'label' => Yii::t('certification', 'Certification'),
				'url' => $this->createUrl($action, $urlParams)
			);
		}
		
		if (PluginManager::isPluginActive('Share7020App')) {
			$urlParams['tab'] = 'questionsAndAnswers';
			$list[] = array(
				'id' => 'questionsAndAnswers',
				'label' => Yii::t('app7020', 'Questions & Answers'),
				'url' => $this->createUrl($action, $urlParams)
			);
		}
		
		if (PluginManager::isPluginActive('Share7020App')) {
			$urlParams['tab'] = 'sharing-activity';
			$list[] = array(
				'id' => 'sharing-activity',
				'label' => Yii::t('app7020', 'Sharing Activity'),
				'url' => $this->createUrl($action, $urlParams)
			);
		}
		
		if (PluginManager::isPluginActive('Share7020App')) {
			$urlParams['tab'] = 'assets-ranks';
			$list[] = array(
				'id' => 'assets-ranks',
				'label' => Yii::t('app7020', 'Assets ranks'),
				'url' => $this->createUrl($action, $urlParams)
			);
		}
		
		return $list;
		
	}



	protected function showReport($showAdminActions = false, $from = false, $selectedUserId = null) {
		Yii::app()->tinymce->init();
		//import admin css
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/admin.css');
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/myactivities.css');
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/bootstrap4-grid.css');

		//detect displayed tab, then prepare menu and content
		$activeTab = Yii::app()->request->getParam('tab', 'statistics');

		if ($showAdminActions) {
			//load some external scripts for admin actions
			$assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin.js'));
			$cs->registerScriptFile($assetsUrl . '/script.js');
		}

		try{
			$userModel = $this->getUserModel($selectedUserId);
		} catch (Exception $e){
			$userModel = false;
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
		}

		//check admin visibility for user
		if (!empty($userModel) && Yii::app()->user->getIsPU()) {
			$check = CoreUserPU::model()->findByAttributes(array(
				'puser_id' => Yii::app()->user->id,
				'user_id' => $userModel->getPrimaryKey()
			));
			if (empty($check)) {
				//the given user is not assigned to this PU, don't allow him to see user's informations
				$userModel = false;
			}
		}

		$this->render('index', array(
			'activeTab' => $activeTab,
			'tabContent' => $this->getContent($activeTab, $userModel),
			'showAdminActions' => $showAdminActions,
			'from' => $from,
			'userModel' => $userModel,
			'menuList' => $userModel ? $this->getMenuList($showAdminActions, $from) : array()
		));
	}



	//actions

	public function actionIndex() {
		$this->showReport(false, false);
	}


	public function actionUserReport() {
		$selectedUserId = null;
		if(isset($_GET['ajax']) &&  $_GET['ajax'] == 'course-management-grid'){
			$selectedUserId = Yii::app()->request->getParam('selectedUserId', null);
		}
		//detect origin of the request, different parts/actions will be displayed
		$from = Yii::app()->request->getParam('from', 'dashboard');
		$showAdminActions = true;
		$this->showReport($showAdminActions, $from, $selectedUserId);
	}

	/**
	 * Download certificate file for a specified course & user
	 */
	public function actionDownloadCertificate() {
		$rq = Yii::app()->request;
		$idUser = $rq->getParam('id_user', Yii::app()->user->id);
		$idCourse = $rq->getParam('course_id');
		if (empty($idUser) || empty($idCourse)) {
			Yii::log(get_class($this).'::'.__METHOD__.' : invalid specified user or course (id_user='.$idUser.'; course_id='.$idCourse.')', CLogger::LEVEL_INFO);
			throw new CHttpException('Invalid specified user or course');
		}
		$model = LearningCertificateAssign::loadByUserCourse($idUser, $idCourse);
		if ($model) {
			$model->downloadCertificate();
		} else {
			Yii::log(get_class($this).'::'.__METHOD__.' : unable to retrieve certificate (id_user='.$idUser.'; course_id='.$idCourse.')', CLogger::LEVEL_INFO);
			throw new CHttpException('Unable to retrieve certificate');
		}
	}


	//internal methods

	protected function getContent($tab, $userModel = null) {
		switch ($tab) {
			case 'statistics': return $this->getStatisticsContent(); break;
			case 'additional-info': return $this->getAdditionalInfoContent(); break;
			case 'courses': return $this->getCoursesContent($userModel); break;
			case 'classrooms': return $this->getClassroomsContent(); break;
			case 'learning-plans': return $this->getLearningPlansContent(); break;
			case 'external-activities': return $this->getExternalActivitiesContent(); break;
			case 'badges': return $this->getBadgesContent(); break;
			case 'social': return $this->getSocialContent(); break;
			case 'certification': return $this->getCertificationContent(); break;
			case 'webinar': return $this->getWebinarContent(); break;
			case 'questionsAndAnswers': return $this->getQuestionsAndAnswersContent(); break;
			case 'sharing-activity': return $this->getSharingActivityContent(); break;
			case 'assets-ranks': return $this->getAssetsRanksContent(); break;
			
		}
		return '';
	}


	/**
	 * Convert a timestamp from seconds to hh mm format
	 * @param integer $seconds the number of seconds to be converted
	 * @return string the time in hh mm format
	 */
	protected function convertCourseTime($seconds) {
		$numHours = floor( $seconds / 3600 );
		$minutesSeconds = $seconds - $numHours * 3600;
		$numMinutes = floor( $minutesSeconds / 60 );
		$arrTime = array();
		if ($numHours <= 0 && $numMinutes <= 0 && $minutesSeconds > 0) { $numMinutes = 1; } //prevent some rounding oddities
		if ($numHours > 0) $arrTime[] = $numHours.'h';
		if ($numMinutes > 0) $arrTime[] = $numMinutes.'m';
		return implode(' ', $arrTime);
	}



	protected function getStatisticsContent() {
		Yii::app()->getClientScript()->registerCssFile(App7020Helpers::getAssetsUrl() . '/css/app7020.css');
		$js = Yii::app()->getClientScript();
		App7020Helpers::registerApp7020Options();
		$js->registerScriptFile(App7020Helpers::getAssetsUrl() . '/js/app7020.js', CClientScript::POS_END);
		$js->registerScriptFile(Yii::app()->theme->baseUrl . '/js/Chart.min.js', CClientScript::POS_HEAD);

		$assetsPath = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin.js'));

		$cs = Yii::app()->clientScript;
		$cs->registerScriptFile($assetsPath . '/scriptModal.js');
		$cs->registerScriptFile($assetsPath . '/scriptTur.js');
		$cs->registerScriptFile($assetsPath . '/charts/d3.v3.min.js');
		$cs->registerScriptFile($assetsPath . '/charts/jquery.tipsy.min.js');
		$cs->registerCssFile($assetsPath . '/charts/charts.css');

		$with = array(
			'learningCourseusers',
			'groups'
		);
		try{
			$userModel = $this->getUserModel();
		}catch(Exception $e){
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			return 'Invalid user';
		}
		if (!$userModel) {
			Yii::log('Report requested for invalid user', CLogger::LEVEL_ERROR);
			return 'Invalid user';
		}

		$userGroups = array();
		if (!empty($userModel->groups)) {
			foreach ($userModel->groups as $group) {
				if ($group->hidden == 'false') {
					$userGroups[] = CoreGroup::model()->relativeId($group->groupid);
				}
			}
		}

		$totalTime = LearningTracksession::model()->getUserTotalTime($userModel->idst);
		$user_subscription = LearningCourseuser::getCountSubscriptions($userModel->idst);
		$courseSummaryList = LearningCourseuser::model()->getUserCourseSummary($userModel->idst, $user_subscription);
		$activitiesCategories = LearningTracksession::model()->getMonthsList();
		$courseSummaryActivities = LearningTracksession::model()->getUserMonthlyActivities($userModel->idst);

		$pieChart = Yii::app()->chart->getPieChart($courseSummaryList);
		$pieChartLegend = Yii::app()->chart->getPieChartLegend($courseSummaryList);
		//$lineChart = Yii::app()->chart->getLineChart($courseSummaryActivities, $activitiesCategories);
		$sessionsChartData = array();
		for ($i=0; $i<12; $i++) {
			$sessionsChartData[] = array(
				'x' => $activitiesCategories[$i],
				'y' => $courseSummaryActivities[$i]
			);
		}
		$sessionsChart = array(
			'data' => $sessionsChartData,
			'height' => '240px',
			'subtitle' => Yii::t('report', '_TH_USER_NUMBER_SESSION')
		);

		//courses due to end
		//TODO: add student filter? what about admins/teachers?
		$coursesDueDate = Yii::app()->db->createcommand()
			->select("c.idCourse, c.code, c.name, c.course_type, c.status, c.date_end")
			->from(LearningCourse::model()->tableName()." c")
			->join(LearningCourseuser::model()->tableName()." cu", "c.idCourse = cu.idCourse AND cu.idUser = :id_user", array(':id_user' => $userModel->getPrimaryKey()))
			->where("c.date_end >= :date_end", array(':date_end' => Yii::app()->localtime->getUTCNow('Y-m-d')))
			->order("c.date_end ASC")
			->limit(3, 0)
			->queryAll();


		//courses top total time
		$coursesTopTotalTime = array(
			//example: array('idCourse' => 1, 'name' => 'course-test-1', 'time' => 4000, 'status' => 2),
		);
		$reader = Yii::app()->db->createCommand()
			->select("t.idCourse, SUM(UNIX_TIMESTAMP(t.lastTime) - UNIX_TIMESTAMP(t.enterTime)) AS course_time, c.code, c.name, cu.status")
			->from(LearningTracksession::model()->tableName()." t")
			->join(LearningCourse::model()->tableName()." c", "c.idCourse = t.idCourse")
			->join(LearningCourseuser::model()->tableName()." cu", "cu.idCourse = t.idCourse AND cu.idUser = t.idUser")
			->where("t.idUSer = :id_user", array(':id_user' => $userModel->getPrimaryKey()))
			->group("t.idCourse")
			->order("course_time DESC")
			->limit(3, 0)
			->query();
		while($row = $reader->read()) {
			$coursesTopTotalTime[] = array(
				'idCourse' => (int)$row['idCourse'],
				'time' => (int)$row['course_time'],
				'status' => $row['status'],
				'code' => $row['code'],
				'name' => $row['name']
			);
		}


		//course most recent results
		$coursesMostRecentResults = array(
			//example: array('idCourse' => 1, 'name' => 'course-test-1', 'score' => 96, 'score_max' => 100, 'date' => '2014-07-01 13:14:15'),
		);
		$reader = Yii::app()->db->createCommand()
			->select("ct.idReference, ct.score, ct.score_max, ct.dateAttempt, c.idCourse, c.code, c.name")
			->from(LearningCommontrack::model()->tableName()." ct")
			->join(LearningOrganization::model()->tableName()." o", "o.idOrg = ct.idReference")// AND o.milestone = :milestone", array(':milestone' => LearningOrganization::MILESTONE_END))
			->join(LearningCourse::model()->tableName()." c", "c.idCourse = o.idCourse")
			->where("ct.idUser = :id_user", array(':id_user' => $userModel->getPrimaryKey()))
			->andWhere("ct.score IS NOT NULL AND ct.score_max IS NOT NULL AND ct.score_max > 0") //exclude all NULL values for scores
			->order("ct.dateAttempt DESC")
			->limit(3,0)
			->query();
		while ($row = $reader->read()) {
			$coursesMostRecentResults[] = array(
				'idCourse' => (int)$row['idCourse'],
				'code' => $row['code'],
				'name' => $row['name'],
				'score' => $row['score'],
				'score_max' => $row['score_max'],
				'date' => substr($row['dateAttempt'], 0, 10) //directly extract the date (yyyy-mm-dd) from sql datetime (yyyy-mm-dd hh:ii:ss)
			);
		}
		if (!empty($coursesMostRecentResults)) {
			usort($coursesMostRecentResults, function($a, $b) {
				$perc_a = $a['score'] / $a['score_max'];
				$perc_b = $b['score'] / $b['score_max'];
				if ($perc_a == $perc_b) { return 0; }
				return ($perc_a < $perc_b) ? -1 : 1;
			});
		}

		//LOs performance info
		//detect dates to use in queries for monthly statistics
		//TODO: due to various changes over time, this part should be refactored in a more readable way
		$monthsTrans = array(
			'01' => Yii::t('calendar', '_JAN'),
			'02' => Yii::t('calendar', '_FEB'),
			'03' => Yii::t('calendar', '_MAR'),
			'04' => Yii::t('calendar', '_APR'),
			'05' => Yii::t('calendar', '_MAY'),
			'06' => Yii::t('calendar', '_JUN'),
			'07' => Yii::t('calendar', '_JUL'),
			'08' => Yii::t('calendar', '_AUG'),
			'09' => Yii::t('calendar', '_SEP'),
			'10' => Yii::t('calendar', '_OCT'),
			'11' => Yii::t('calendar', '_NOV'),
			'12' => Yii::t('calendar', '_DEC'),
		);
		$monthsLimits = array();
		$chartDates = array();
		$chartMonths = array();
		$localNow = Yii::app()->localtime->toLocalDateTime(null, 'short', 'medium', 'Y-m-d H:i:s');
		$localMonthBegin = date('Y-m').'-01 00:00:00';
		$monthsLimits[] = Yii::app()->localtime->toUTC($localNow);
		$monthsLimits[] = Yii::app()->localtime->toUTC($localMonthBegin);
		$chartDates[] = date('m/d/Y');
		$chartDates[] = date('m/01/Y');
		$chartMonths[] = date('m');
		$startTime = strtotime($localMonthBegin);
		for ($i=1; $i<=12; $i++) {
			$targetTime = strtotime('-'.$i.' month', $startTime);
			$monthsLimits[] = Yii::app()->localtime->toUTC(date('Y-m-d H:i:s', $targetTime));
			$chartDates[] = date('m/d/Y', $targetTime);
			$chartMonths[] = date('m', $targetTime);
		}
		//extract data from DB and prepare it to be displayed
		$performanceInfo = array(
			'monthlyStats' => array(), //chart data, this array will contain 12 cells, one for every month
			'highest_score' => false,
			'lowest_score' => false,
			'average_score' => false,
			'highest_score_percent' => false,
			'lowest_score_percent'  => false,
			'average_score_percent' => false
		);
		$dateBegin = $monthsLimits[12];
		$dateEnd = $monthsLimits[0];
		$reader = Yii::app()->db->createCommand()
			->select("ct.idReference, ct.score, ct.score_max, ct.dateAttempt, c.idCourse, c.code, c.name, o.objectType")
			->from(LearningCommontrack::model()->tableName()." ct")
			->join(LearningOrganization::model()->tableName()." o", "o.idOrg = ct.idReference")
			->join(LearningCourse::model()->tableName()." c", "c.idCourse = o.idCourse")
			->where("ct.idUser = :id_user", array(':id_user' => $userModel->getPrimaryKey()))
			->andWhere("ct.score IS NOT NULL AND ct.score_max IS NOT NULL AND ct.score_max > 0") //exclude all NULL values for scores
			->andWhere("ct.dateAttempt <= :date_end AND ct.dateAttempt >= :date_begin", array(':date_end' => $dateEnd, ':date_begin' => $dateBegin))
			->andWhere("o.objectType != :type", array(':type' => LearningOrganization::OBJECT_TYPE_TEST))
			->query();
		$readerTests = Yii::app()->db->createCommand()
			->select("ct.idReference, ct.score, ct.score_max, ct.dateAttempt, c.idCourse, c.code, c.name, o.objectType, lt.point_type, lt.idTest")
			->from(LearningCommontrack::model()->tableName()." ct")
			->join(LearningOrganization::model()->tableName()." o", "o.idOrg = ct.idReference")
			->join(LearningCourse::model()->tableName()." c", "c.idCourse = o.idCourse")
			->join(LearningTest::model()->tableName()." lt", "lt.idTest = o.idResource")
			->where("ct.idUser = :id_user", array(':id_user' => $userModel->getPrimaryKey()))
			->andWhere("ct.score IS NOT NULL AND ct.score_max IS NOT NULL AND ct.score_max > 0") //exclude all NULL values for scores
			->andWhere("ct.dateAttempt <= :date_end AND ct.dateAttempt >= :date_begin", array(':date_end' => $dateEnd, ':date_begin' => $dateBegin))
			->andWhere("o.objectType = :objectType", array(':objectType' => LearningOrganization::OBJECT_TYPE_TEST))
			->query();

		$count = 0;
		$sumPercent = 0;
		$sumScoreMax = 0;
		$monthsAverages = array();
		$countPercent = 0;
		$sumPercentTest = 0;
		$sumScoreMaxPercent = 0;
		while($rowTest = $readerTests->read()){
			if(isset($rowTest['point_type']) && ($rowTest['point_type'] == LearningTest::POINT_TYPE_PERCENT)){
				$countPercent++;
				if($rowTest['score_max'] && $rowTest['score_max'] > 0)
					$percentScore = ($rowTest['score'] / $rowTest['score_max']); //this will always be a float number between 0 and 1
				else
					$percentScore = 0;
				$sumPercentTest += $percentScore;
				$sumScoreMaxPercent += $rowTest['score_max'];

				//initialize months average values
				if (!isset($monthsAverages[$i])) {
					$monthsAverages[$i] = array(
						'sum_percent' => 0,
						'count' => 0
					);
				}

				//detect year's lowest score
				if ($performanceInfo['lowest_score_percent'] === false) {
					$performanceInfo['lowest_score_percent'] = array(
						'score' => $rowTest['score'],
						'score_max' => $rowTest['score_max'],
						'percent' => $percentScore
					);
				} else {
					if ($percentScore < $performanceInfo['lowest_score_percent']['percent'] || ($percentScore == $performanceInfo['lowest_score_percent']['percent'] && $performanceInfo['lowest_score_percent']['score'] > $rowTest['score'])) {
						$performanceInfo['lowest_score_percent']['score'] = $rowTest['score'];
						$performanceInfo['lowest_score_percent']['score_max'] = $rowTest['score_max'];
						$performanceInfo['lowest_score_percent']['percent'] = $percentScore;
					}
				}
				//detect year's highest score
				if ($performanceInfo['highest_score_percent'] === false) {
					$performanceInfo['highest_score_percent'] = array(
						'score' => $rowTest['score'],
						'score_max' => $rowTest['score_max'],
						'percent' => $percentScore
					);
				} else {
					if ($percentScore > $performanceInfo['highest_score_percent']['percent'] || ($percentScore == $performanceInfo['highest_score_percent']['percent'] && $performanceInfo['highest_score_percent']['score'] < $rowTest['score'])) {
						$performanceInfo['highest_score_percent']['score'] = $rowTest['score'];
						$performanceInfo['highest_score_percent']['score_max'] = $rowTest['score_max'];
						$performanceInfo['highest_score_percent']['percent'] = $percentScore;
					}
				}


			}else{
				if(isset($rowTest['idTest']) ){
					$testModel = LearningTest::model()->findByPk($rowTest['idTest']);
					if($testModel){
						$rowTest['score_max'] = $testModel->getMaxScore($userModel->idst, true, false);
					}
				}

				$count++;
				if($rowTest['score_max'] && $rowTest['score_max'] > 0)
					$percentScore = ($rowTest['score'] / $rowTest['score_max']); //this will always be a float number between 0 and 1
				else
					$percentScore = 0;
				$sumPercent += $percentScore;
				$sumScoreMax += $rowTest['score_max'];

				//initialize months average values
				if (!isset($monthsAverages[$i])) {
					$monthsAverages[$i] = array(
						'sum_percent' => 0,
						'count' => 0
					);
				}

				//detect year's lowest score
				if ($performanceInfo['lowest_score'] === false) {
					$performanceInfo['lowest_score'] = array(
						'score' => $rowTest['score'],
						'score_max' => $rowTest['score_max'],
						'percent' => $percentScore
					);
				} else {
					if ($percentScore < $performanceInfo['lowest_score']['percent'] || ($percentScore == $performanceInfo['lowest_score']['percent'] && $performanceInfo['lowest_score']['score'] > $rowTest['score'])) {
						$performanceInfo['lowest_score']['score'] = $rowTest['score'];
						$performanceInfo['lowest_score']['score_max'] = $rowTest['score_max'];
						$performanceInfo['lowest_score']['percent'] = $percentScore;
					}
				}
				//detect year's highest score
				if ($performanceInfo['highest_score'] === false) {
					$performanceInfo['highest_score'] = array(
						'score' => $rowTest['score'],
						'score_max' => $rowTest['score_max'],
						'percent' => $percentScore
					);
				} else {
					if ($percentScore > $performanceInfo['highest_score']['percent'] || ($percentScore == $performanceInfo['highest_score']['percent'] && $performanceInfo['highest_score']['score'] < $rowTest['score'])) {
						$performanceInfo['highest_score']['score'] = $rowTest['score'];
						$performanceInfo['highest_score']['score_max'] = $rowTest['score_max'];
						$performanceInfo['highest_score']['percent'] = $percentScore;
					}
				}
			}

			//detect time period for monthly stats
			$foundTest = false;
			for ($i=0; $i<12 && !$foundTest; $i++) {
				$limitBegin = $monthsLimits[12 - $i];
				$limitEnd = $monthsLimits[12 - ($i+1)];
				if ($rowTest['dateAttempt'] > $limitBegin && $rowTest['dateAttempt'] <= $limitEnd) {
					$found = true;
					$monthsAverages[$i]['sum_percent'] += $percentScore;
					$monthsAverages[$i]['count']++;
				}
			}
		}


		while ($row = $reader->read()) {

			$count++;
			if($row['score_max'] && $row['score_max'] > 0)
				$percentScore = ($row['score'] / $row['score_max']); //this will always be a float number between 0 and 1
			else
				$percentScore = 0;
			$sumPercent += $percentScore;
			$sumScoreMax += $row['score_max'];

			//initialize months average values
			if (!isset($monthsAverages[$i])) {
				$monthsAverages[$i] = array(
					'sum_percent' => 0,
					'count' => 0
				);
			}

			//detect year's lowest score
			if ($performanceInfo['lowest_score'] === false) {
				$performanceInfo['lowest_score'] = array(
					'score' => $row['score'],
					'score_max' => $row['score_max'],
					'percent' => $percentScore
				);
			} else {
				if ($percentScore < $performanceInfo['lowest_score']['percent'] || ($percentScore == $performanceInfo['lowest_score']['percent'] && $performanceInfo['lowest_score']['score'] > $row['score'])) {
					$performanceInfo['lowest_score']['score'] = $row['score'];
					$performanceInfo['lowest_score']['score_max'] = $row['score_max'];
					$performanceInfo['lowest_score']['percent'] = $percentScore;
				}
			}
			//detect year's highest score
			if ($performanceInfo['highest_score'] === false) {
				$performanceInfo['highest_score'] = array(
					'score' => $row['score'],
					'score_max' => $row['score_max'],
					'percent' => $percentScore
				);
			} else {
				if ($percentScore > $performanceInfo['highest_score']['percent'] || ($percentScore == $performanceInfo['highest_score']['percent'] && $performanceInfo['highest_score']['score'] < $row['score'])) {
					$performanceInfo['highest_score']['score'] = $row['score'];
					$performanceInfo['highest_score']['score_max'] = $row['score_max'];
					$performanceInfo['highest_score']['percent'] = $percentScore;
				}
			}

			//detect time period for monthly stats
			$found = false;
			for ($i=0; $i<12 && !$found; $i++) {
				$limitBegin = $monthsLimits[12 - $i];
				$limitEnd = $monthsLimits[12 - ($i+1)];
				if ($row['dateAttempt'] > $limitBegin && $row['dateAttempt'] <= $limitEnd) {
					$found = true;
					$monthsAverages[$i]['sum_percent'] += $percentScore;
					$monthsAverages[$i]['count']++;
				}
			}
		}
		//set monthly stats for chart
		for ($i=0; $i<12; $i++) {
			//$index = $chartDates[12 - $i - 1];
			$index = $monthsTrans[''.$chartMonths[12 - $i - 1]];
			if (isset($monthsAverages[$i]) && $monthsAverages[$i]['count'] > 0) {
				$performanceInfo['monthlyStats'][$index] = $monthsAverages[$i]['sum_percent'] / $monthsAverages[$i]['count'];
			} else {
				$performanceInfo['monthlyStats'][$index] = 0;
			}
		}
		//processing last data
		if ($count > 0) {
			$averagePercent = $sumPercent / $count;
			$averageScoreMax = $sumScoreMax / $count;
			$averageScore = $averagePercent * $averageScoreMax;
			$performanceInfo['average_score'] = array(
				'percent' => $averagePercent,
				'score' => $averageScore,
				'score_max' => $averageScoreMax
			);
		}

		if ($countPercent > 0) {
			$averagePercentTest = $sumPercentTest / $countPercent;
			$averageScoreMaxTest = $sumScoreMaxPercent / $countPercent;
			$averageScoreTest = $averagePercentTest * $averageScoreMaxTest;
			$performanceInfo['average_score_percent'] = array(
				'percent' => $averagePercentTest,
				'score' => $averageScoreTest,
				'score_max' => $averageScoreMaxTest
			);
		}

        //Coach & Share Overview asset views and asset shared chart
        $assetSharedVsViewsData = array();
        $assetSharedVsViews = App7020SharingStatistics::sharedAssetsViews(365, $userModel->getPrimaryKey());
        foreach($assetSharedVsViews['periods'] as $date){
            $assetSharedVsViewsData[]['x'] = date('M', strtotime($date));
        }
        foreach($assetSharedVsViews['assets'] as $key => $count){
            $assetSharedVsViewsData[$key]['y_shares'] = $count;
        }
        foreach($assetSharedVsViews['views'] as $key => $count){
            $assetSharedVsViewsData[$key]['y_views'] = $count;
        }
        //Coach & Share Overview questions made / answers given chart
        $questions = App7020Question::getQuestionsCountByPeriod($userModel->getPrimaryKey(), 7);
        $questionsChart = App7020Helpers::getChartData($questions);
        $answers = App7020Answer::getAnswersCountByPeriod($userModel->getPrimaryKey(), 7);
        $answersChart = App7020Helpers::getChartData($answers);
        Yii::app()->clientScript->registerScript(
            'app7020UserReportChartQuestionsAndAnswers',
            "var app7020UserReportChartQuestionsAndAnswers = " . CJSON::encode(array('questions' => $questionsChart, 'answers' => $answersChart)), CClientScript::POS_HEAD);

        return $this->renderPartial('_tabs/_statistics', array(
			'userModel' => $userModel,
			'userGroups' => $userGroups,
			'totalTime' => $totalTime,

			'pieChart' => $pieChart,
			'pieChartLegend' => $pieChartLegend,
			//'lineChart' => $lineChart,
			'sessionsChart' => $sessionsChart,
			'coachShareCharts' => $assetSharedVsViewsData,
            // Coach & Share Overview Top3 most watched assets
			'coachShareMostWatched' => App7020SharingStatistics::getMostWatchedAsset($userModel->getPrimaryKey(), false, false, 7, 3),
			'coursesDueDate' => $coursesDueDate,
			'coursesTopTotalTime' => $coursesTopTotalTime,
			'coursesMostRecentResults' => $coursesMostRecentResults,
			'performanceInfo' => $performanceInfo
		), true);
	}


	protected function getAdditionalInfoContent() {
	    /** @var CoreUserField $fieldModel */
		$userModel = $this->getUserModel();
		
		// Load all User additional fields, viible to that given user, also populating field values 
		// (only values, no dropdown translation wahtsoever)
		$additionalFields = $userModel->getAdditionalFields(true);

		return $this->renderPartial('_tabs/_additionalInfo', array(
			'additionalFields' => $additionalFields
		), true);
	}


	protected function getCoursesContent($userModel = null) {

		//$with = array('learningCourseusers');
		if(!$userModel){
			$userModel = $this->getUserModel();//CoreUser::model()->with($with)->findByPk($this->getUserId());
		}
		
		if (!$userModel) { throw new CHttpException(500, 'Invalid user'); }

		$courseUserModel = new LearningCourseuser();
		$courseUserModel->idUser = $userModel->getPrimaryKey();
		$courseUserModel->level = '';
		$courseUserModel->waiting = '';

		return $this->renderPartial('_tabs/_courses', array(
			'userModel' => $userModel,
			'courseUserModel' => $courseUserModel,
		), true);
	}


	protected function getClassroomsContent() {

		if (!PluginManager::isPluginActive('ClassroomApp')) return '';

		$userModel = $this->getUserModel();//CoreUser::model()->findByPk($this->getUserId());
		if (!$userModel) { throw new CHttpException(500, 'Invalid user'); }

		return $this->renderPartial('_tabs/_classrooms', array(
			'userModel' => $userModel,
		), true);
	}

	protected function getWebinarContent() {

		$userModel = $this->getUserModel();//CoreUser::model()->findByPk($this->getUserId());
		if (!$userModel) { throw new CHttpException(500, 'Invalid user'); }

		return $this->renderPartial('_tabs/_webinar', array(
			'userModel' => $userModel,
		), true);
	}



	protected function getLearningPlansContent() {

		if (!PluginManager::isPluginActive('CurriculaApp')) return '';

		$userModel = $this->getUserModel();//CoreUser::model()->findByPk($this->getUserId());
		if (!$userModel) { throw new CHttpException(500, 'Invalid user'); }

		$coursepathUserModel = LearningCoursepathUser::model();

		return $this->renderPartial('_tabs/_learningPlans', array(
			'userModel' => $userModel,
			'coursepathUserModel' => $coursepathUserModel
		), true);
	}



	protected function getExternalActivitiesContent() {

		if (!PluginManager::isPluginActive('TranscriptsApp')) return '';

		$userModel = $this->getUserModel();//CoreUser::model()->findByPk($this->getUserId());
		if (!$userModel) { throw new CHttpException(500, 'Invalid user'); }

		$transcriptsModel = TranscriptsRecord::model();
		$transcriptsModel->scenario = 'user-activities-list';
		$transcriptsModel->id_user = $userModel->getPrimaryKey(); //this will act as a filter in the data provider

		return $this->renderPartial('_tabs/_externalActivities', array(
			'userModel' => $userModel,
			'transcriptsModel' => $transcriptsModel
		), true);
	}


	protected function getBadgesContent() {

		if (!PluginManager::isPluginActive('GamificationApp')) return '';

		$userModel = $this->getUserModel();//CoreUser::model()->findByPk($this->getUserId());
		if (!$userModel) { throw new CHttpException(500, 'Invalid user'); }

		$assignedBadgesModel = GamificationAssignedBadges::model();

		//calculate badges info
		$scorePoints = 0;
		$countBadges = 0;
		$badges = GamificationAssignedBadges::model()->getUserBadges($this->getUserId());
		if (count($badges) > 0) {
			foreach ($badges as $badge) {
				$scorePoints += intval($badge->gamificationBadge->badgeCounterScore);
				$countBadges += (is_null($badge->gamificationBadge->badgeCounter) ? 1 : $badge->gamificationBadge->badgeCounter);
			}
		}

		return $this->renderPartial('_tabs/_badges', array(
			'userModel' => $userModel,
			'assignedBadgesModel' => $assignedBadgesModel,
			'countBadges' => $countBadges,
			'countPoints' => $scorePoints
		), true);
	}


	protected function getSocialContent() {

		//import js scripts
		$assetsPath = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin.js'));
		$cs = Yii::app()->clientScript;
		$cs->registerScriptFile($assetsPath . '/scriptTur.js');
		$cs->registerScriptFile($assetsPath . '/charts/d3.v3.min.js');
		$cs->registerScriptFile($assetsPath . '/charts/jquery.tipsy.min.js');
		$cs->registerCssFile($assetsPath . '/charts/charts.css');

		$userModel = $this->getUserModel();//CoreUser::model()->findByPk($this->getUserId());

		//calculate chart data
		$localNowTime = time();
		$utcNowDate = Yii::app()->localtime->getUTCNow('Y-m-d');
		$weekDays = array(
			'Mon' => Yii::t('calendar', '_MON'),
			'Tue' => Yii::t('calendar', '_TUE'),
			'Wed' => Yii::t('calendar', '_WED'),
			'Thu' => Yii::t('calendar', '_THU'),
			'Fri' => Yii::t('calendar', '_FRI'),
			'Sat' => Yii::t('calendar', '_SAT'),
			'Sun' => Yii::t('calendar', '_SUN'),
		);

		//prepare axis variables
		$xData = array(); //this will contain days names
		$yData = array(); //this will contain forum info

		//create posts count command
		$command = Yii::app()->db->createCommand()
			->select("COUNT(*)")
			->from(LearningForummessage::model()->tableName())
			->where("author = :author")
			->andWhere("posted >= :date_1 AND posted <= :date_2");
		/* @var $command CDbCommand */

		//get UTC intervals in which count the posted messages for each one of them
		for ($i=6; $i>=0; $i--) {
			$tmpDate = ($i <= 0 ? $utcNowDate : date('Y-m-d', strtotime('-'.$i.' day', strtotime($utcNowDate))));
			$command->bindValue(':author', $userModel->getPrimaryKey());
			$command->bindValue(':date_1', $tmpDate.' 00:00:00');
			$command->bindValue(':date_2', $tmpDate.' 23:59:59');
			$yData[] = $command->queryScalar();
			$xData[] = $weekDays[date('D', $localNowTime - $i*86400)];
		}

		//prepare rendering parameters
		$params = array();
		$params['userModel'] = $userModel;

		//forum specific parameters
		$params['forumThreadModel'] = LearningForumthread::model();
		$params['forumMessageModel'] = LearningForummessage::model();
		$forumChartData = array();
		for ($i=0; $i<7; $i++) {
			$forumChartData[] = array(
				'x' => $xData[$i],
				'y' => $yData[$i]
			);
		}
		$params['forumChart'] = array(
			'data' => $forumChartData,
			'height' => '190px',
			'subtitle' => Yii::t('myactivities', 'Number of messages')
		);



		//if blogs have been activated, the pass blog models too
		if (PluginManager::isPluginActive('MyBlogApp')) {
			//prepare axis variables
			//$xData = array(); //this will contain days names
			$yData = array(); //this will contain forum info

			//create blog posts count command
			$command = Yii::app()->db->createCommand()
				->select("COUNT(*)")
				->from(MyblogPost::model()->tableName())
				->where("id_user = :id_user")
				->andWhere("posted_on >= :date_1 AND posted_on <= :date_2");
			/* @var $command CDbCommand */

			//get UTC intervals in which count the posted messages for each one of them
			for ($i=6; $i>=0; $i--) {
				$tmpDate = ($i <= 0 ? $utcNowDate : date('Y-m-d', strtotime('-'.$i.' day', strtotime($utcNowDate))));
				$command->bindValue(':id_user', $userModel->getPrimaryKey());
				$command->bindValue(':date_1', $tmpDate.' 00:00:00');
				$command->bindValue(':date_2', $tmpDate.' 23:59:59');
				$yData[] = $command->queryScalar();
				//$xData[] = $weekDays[date('D', $localNowTime - $i*86400)];
			}

			//add blog parameters for the view
			$params['blogModel'] = MyblogPost::model();
			$blogChartData = array();
			for ($i=0; $i<7; $i++) {
				$blogChartData[] = array(
					'x' => $xData[$i],
					'y' => $yData[$i]
				);
			}
			$params['blogChart'] = array(
				'data' => $blogChartData,
				'height' => '190px',
				'subtitle' => Yii::t('myactivities', 'Number of messages')
			);
		}

		return $this->renderPartial('_tabs/_social', $params, true);
	}

	protected function getCertificationContent(){
		if (!PluginManager::isPluginActive('CertificationApp')) return '';

		//load some external scripts for admin actions
		$cs = Yii::app()->clientScript;
		$assetsPath = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin.js'));
		
		$cs->registerScriptFile($assetsPath . '/script.js');

		$model = new CertificationUser();
		$model->id_user = $this->getUserModel()->idst;

		$showExpired  = (bool) Yii::app()->getRequest()->getParam('show_expired', 1);
		$showArchived = (bool) Yii::app()->getRequest()->getParam('show_archived', 0);

		$searchKeyword = Yii::app()->getRequest()->getParam('search_input');

		
		$model->includeArchived = $showArchived;
		$dataprovider = $model->sqlDataProvider($showExpired, $searchKeyword);

		$params = array(
			'dataProvider'=>$dataprovider,
			'columns'=>array(
				array(
					'name'=>'title',
					'header'=>Yii::t('standard', '_TITLE'),
					'type'=>'text',
					'htmlOptions' => array(
						'style'=>'min-width:100px;'
					),
                    'value' =>function($data, $index){
	                         $title = strip_tags($data['title']);
		                    if(isset($data['deleted'])  &&  $data['deleted']){
								$title = Yii::t('standard', '_USER_STATUS_CANCELLED')." - ".$title ;
		                    }
		                    return $title;
	                    },
				),
				array(
					'name'	=> 'description',
					'header'=>Yii::t('standard', '_DESCRIPTION'),
					'type'	=> 'html',
					'value'	=> 'strip_tags($data[description])',	 	
				),	
				array(
					'name'=>'on_datetime',
					'header'=>Yii::t('gamification', 'Issued on'),
					'value'=>function($data, $index){
						if ($data['timeToExpire'] < 0) {
							echo "<span class='i-sprite is-solid-exclam orange'></span>&nbsp;";
						}
						echo Yii::app()->localtime->toLocalDate($data['on_datetime']);
					},
					'cssClassExpression' => function($index, $data) {
						$class = 'text-center';
						if ($data['timeToExpire'] < 0) {
							$class .= ' certification-expired-color';
						}
						return $class;						
					},
					'headerHtmlOptions' => array(
						'class' => 'text-center',
						'style'=>'min-width:100px;'
					),
				),
				array(
					'name'=>'expire_at',
					'header'=>Yii::t('standard', 'Expiration'),
					'headerHtmlOptions' => array(
						'class' => 'text-center',
						'style'=>'min-width:100px;'
					),
					'value'=>function($data, $index){
						if(!$data['expire_at'] || $data['expire_at']==='0000-00-00 00:00:00'){
							echo Yii::t('standard', '_NEVER');
							return;
						}
						$durationUnitLabel = null;
						switch($data['duration_unit']){
							case 'day':
								if($data['duration']==1){
									$durationUnitLabel = Yii::t('standard', 'Every day');
								}elseif($data['duration']>1){
									$durationUnitLabel = Yii::t('standard', 'Every {days} days', array('{days}'=>$data['duration']));
								}
								break;
							case 'month':
								if($data['duration']==1){
									$durationUnitLabel = Yii::t('standard', 'month');
								}elseif($data['duration']>1){
									$durationUnitLabel = Yii::t('standard', 'Every {months} months', array('{months}'=>$data['duration']));
								}
								break;
							case 'year':
								if($data['duration']==1){
									$durationUnitLabel = Yii::t('classroom', 'Yearly');
								}elseif($data['duration']>1){
									$durationUnitLabel = Yii::t('standard', 'Every {years} years', array(
										'{years}'=>$data['duration']
									));
								}
								break;
						}
						echo $durationUnitLabel."<br/>";
                        /* Change the DateTime to Date only because it is unnecessary information */
						echo Yii::t('standard', 'Next exp').': '.Yii::app()->localtime->toLocalDate($data['expire_at']);
					},
					'cssClassExpression' => function($index, $data) {
						$class = "text-center";
						if ($data['timeToExpire'] < 0) {
							$class .= ' certification-expired-color';
						}
						return $class;
					},
						
				),
				array(
					'name'=>'renew_in',
					'header'=>Yii::t('standard', 'To renew in'),
					'headerHtmlOptions' => array(
						'class' => 'text-center',
						'style'=>'min-width:100px;'
					),
					'cssClassExpression' => function($index, $data) {
						return 'text-center';
					},
					'value'=>function($data, $index){
						if(!$data['expire_at'] || $data['expire_at']==='0000-00-00 00:00:00' || (isset($data['deleted'])  &&  $data['deleted'])){
							echo '-';
							return;
						}
						$certUser = CertificationUser::model()->findByPk($data['cu_id']);
						$expireTimeRemaining = $certUser->timeToExpire();
						if ($expireTimeRemaining >= 0) {
							$daysToExpire = intval($expireTimeRemaining / 3600 / 24);
							echo Yii::t('standard', '{days} days', array('{days}'=>$daysToExpire));
							echo '<br/>';
						}


						$renewButton = CHtml::link(Yii::t('standard', 'Renew now'), Docebo::createLmsUrl('CertificationApp/certificationApp/renew', array('cu_id'=>$data['cu_id'])), array(
							'class'=>'certification-renew'
						));
						if(isset($data['cu_id']) && CertificationUser::showRenewButton($data['cu_id'])){
							echo $renewButton;
						}elseif(!isset($data['cu_id'])){
							echo $renewButton;
						}
					}
				),
			),
		);
		return $this->renderPartial('_tabs/_certification', $params, true);
	}
	
	/**
	 * Ajax call to change needed data on page "lms/index.php?r=myActivities/userReport&from=reportManagement&tab=questionsAndAnswers"
	 */
	public function actionAxGetQuestionsAndAnswersContent() {
		if (Yii::app()->request->isAjaxRequest) {
			$userId = Yii::app()->request->getParam('userId', false);

			$timeframe = intval(Yii::app()->request->getParam('timeframe', false));
			
			// Data for info boxes 'Answered questions', 'Answers likes', 'Answrs dislikes', 'Best answers'
			$questionsAndAnswers = App7020QuestionAnswerStatistic::answersBaseStats($timeframe, $userId);
			
			// Data for chart "Activity per channel"
			$chartMyActivityPerChannel = App7020PeerReviewStatistic::chartMyActivityPerChannel($timeframe, $userId);

			$questions = App7020Question::getQuestionsCountByPeriod($userId, $timeframe);

			// Data for info box 'Questions made'
			$questionsMade = array('questionsMade' => App7020Helpers::arrSumByKey($questions, 'count'));
			
			// Data for chart "Questions vs Answers"
			$chartQuestionsAnswers = array('chartQuestionsAnswers' => array(
					'questions' => App7020Helpers::getChartData($questions, $timeframe),
					'answers' => App7020Helpers::getChartData(App7020Answer::getAnswersCountByPeriod($userId, $timeframe), $timeframe)
				)
			);
			
			// Data for chart "Activity per channel"
			$chartMyActivityPerChannelHTML = array('chartMyActivityPerChannelHTML' => $this->widget('common.widgets.app7020ActivityChannels', array('data' => $chartMyActivityPerChannel), true));
			
			// Data for JS
			$data = array('userReportData' => array_merge(
					$questionsMade,
					$questionsAndAnswers,
					$chartMyActivityPerChannelHTML,
					$chartQuestionsAnswers
					)
				) ;
			
			$this->sendJSON($data);
			Yii::app()->end();
		}
	}
	
	
	protected function getQuestionsAndAnswersContent() {
		
		Yii::app()->getClientScript()->registerCssFile(App7020Helpers::getAssetsUrl() . '/css/app7020.css');
		$js = Yii::app()->getClientScript();
		App7020Helpers::registerApp7020Options();
		$js->registerScriptFile(App7020Helpers::getAssetsUrl() . '/js/app7020.js', CClientScript::POS_END);
		$js->registerScriptFile(Yii::app()->theme->baseUrl . '/js/Chart.min.js', CClientScript::POS_HEAD);
		
		$userId = $this->getUserId();
		// Default value
		$timeframe = 30;
		
		// Data for info boxes 'Answered questions', 'Answers likes', 'Answrs dislikes', 'Best answers'
		$questionsAndAnswers = App7020QuestionAnswerStatistic::answersBaseStats($timeframe, $userId);
		
		// Data for chart "Activity per channel"
		$chartMyActivityPerChannel = array('chartMyActivityPerChannel' => App7020PeerReviewStatistic::chartMyActivityPerChannel($timeframe, $userId));
		
		$questions = App7020Question::getQuestionsCountByPeriod($userId, $timeframe);
		
		// Data for info box 'Questions made'
		$questionsMade = array('questionsMade' => App7020Helpers::arrSumByKey($questions, 'count'));
		
		// Rankings data
		$rankExpertsByAnswersQuality = array('rankExpertsByAnswersQuality' => App7020QuestionAnswerStatistic::expertsByAnswersQuality($userId));
		$rankExpertsByFirstToAnswer = array('rankExpertsByFirstToAnswer' => App7020QuestionAnswerStatistic::topExpertsByAnswer(true,$userId));
		$rankExpertsByPartecipationRate = array('rankExpertsByPartecipationRate' => App7020QuestionAnswerStatistic::topExpertsByAnswer(false,$userId));					
		
		// Timeframe
		$timeframeDropdown = array(
			7 => Yii::t('app7020', 'Weekly'),
			30 => Yii::t('app7020', 'Monthly'),
			365 => Yii::t('app7020', 'Yearly')
			);
		$timeframeDropdown = array('timeframes' => $timeframeDropdown);
		
		$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
		$avatarImg = $storage->absoluteBaseUrl . '/avatar/';
		$additionalInfo = array(
			'avatarPath' => $avatarImg,
			'searchedUserId' => $userId,
			'preselectedTimeframe' => $timeframe
				);
		
		// Data for view
		$renderData = array('renderData' => array_merge(
				$additionalInfo,
				$questionsMade,
				$questionsAndAnswers,
				$chartMyActivityPerChannel,
				$rankExpertsByAnswersQuality,
				$rankExpertsByFirstToAnswer,
				$rankExpertsByPartecipationRate,
				$timeframeDropdown
				)
			) ;
		
		// Data for chart "Question vs Answers"
		$questionsChart = App7020Helpers::getChartData($questions, $timeframe);
		// Answers
		$answers = App7020Answer::getAnswersCountByPeriod($userId, $timeframe);
		$answersChart = App7020Helpers::getChartData($answers, $timeframe);
		// Inject data in JS
		Yii::app()->clientScript->registerScript('app7020UserReportChartQuestionsAndAnswers', "var app7020UserReportChartQuestionsAndAnswers = " . CJSON::encode(array('questions' => $questionsChart, 'answers' => $answersChart)), CClientScript::POS_HEAD);
		
		return $this->renderPartial('_tabs/_questionsAndAnswers', $renderData, true);
	}
	
	
	protected function getSharingActivityContent() {
		Yii::app()->getClientScript()->registerCssFile(App7020Helpers::getAssetsUrl() . '/css/app7020.css');
		$js = Yii::app()->getClientScript();
		App7020Helpers::registerApp7020Options();
		$js->registerScriptFile(App7020Helpers::getAssetsUrl() . '/js/app7020.js', CClientScript::POS_END);
		$js->registerScriptFile(Yii::app()->theme->baseUrl . '/js/Chart.min.js', CClientScript::POS_HEAD);
		
		$userId = $this->getUserId();
		
		// Default value
		$timeframe = 30;
		
		// Info box 'Shared assets avg. rating'
		$averageRating = App7020SharingStatistics::averageRating($timeframe, $userId);
		
		// Info box 'Shared contents'
		$sharedContents = App7020SharingStatistics::sharedContents($timeframe, $userId);
		

		// Info box 'Avg. watch rate'
		$averageWatchRate = round(App7020SharingStatistics::averageWatchRate($timeframe, $userId));
		
		// Chart 'My Activity Per Channel'
		$chartMyActivityPerChannel = App7020SharingStatistics::chartMyActivityPerChannel($timeframe, $userId);
		
		// Data for 2 charts - "My shared assets" and "My asset views"
		$chartSharedAssetsViews = App7020SharingStatistics::sharedAssetsViews($timeframe, $userId);
        // Info box 'Total views'
        $totalViews =$chartSharedAssetsViews['count'];
        // Timeframe Dropdown
		$timeframeDropdown = array(
			7 => Yii::t('app7020', 'Weekly'),
			30 => Yii::t('app7020', 'Monthly'),
			365 => Yii::t('app7020', 'Yearly')
		);
		
		// Inject chart data in JS
		Yii::app()->clientScript->registerScript('app7020UserReportSharingActivity', "var app7020UserReportSharingActivity = " . CJSON::encode($chartSharedAssetsViews), CClientScript::POS_HEAD);
		
		// Data for view
		$renderData = array('renderData' =>  array(
				'preselectedTimeframe' => $timeframe,
				'averageRating' => $averageRating,
				'sharedContents' => $sharedContents,
				'totalViews' => $totalViews,
				'averageWatchRate' => $averageWatchRate,
				'chartMyActivityPerChannel' => $chartMyActivityPerChannel,
				'timeframes' => $timeframeDropdown,
				'searchedUserId' => $userId
			)
		);
		
		
		return $this->renderPartial('_tabs/_sharingActivity', $renderData, true);
	}
	
	
	/**
	 * Ajax call to change needed data on page "lms/index.php?r=myActivities/userReport&from=reportManagement&tab=sharing-activity"
	 */
	public function actionAxGetSharingActivityContent() {
		if (Yii::app()->request->isAjaxRequest) {
			$userId = Yii::app()->request->getParam('userId', false);
			$timeframe = intval(Yii::app()->request->getParam('timeframe', false));
			
			// Info box 'Shared assets avg. rating'
			$averageRating = App7020SharingStatistics::averageRating($timeframe, $userId);
			// Info box 'Shared contents'
			$sharedContents = App7020SharingStatistics::sharedContents($timeframe, $userId);
			// Info box 'Total views'
			$totalViews = App7020SharingStatistics::sharedAssetsViews($timeframe, $userId)['count'];
			// Info box 'Avg. watch rate'
			$averageWatchRate = round(App7020SharingStatistics::averageWatchRate($timeframe, $userId));
			
			// Data for JS
			$data = array(
				'averageRating' => $averageRating,
				'sharedContents' => $sharedContents,
				'totalViews' => $totalViews,
				'averageWatchRate' => $averageWatchRate,
				'chartMyActivityPerChannelHTML' => $this->widget('common.widgets.app7020ActivityChannels', array('data' => App7020SharingStatistics::chartMyActivityPerChannel($timeframe, $userId)), true),
				'chartsSharedAssetsAndViews' => App7020SharingStatistics::sharedAssetsViews($timeframe, $userId)
			);
			
			$this->sendJSON($data);
			Yii::app()->end();
		}
	}
	
		protected function getAssetsRanksContent() {
			Yii::app()->getClientScript()->registerCssFile(App7020Helpers::getAssetsUrl() . '/css/app7020.css');
			$js = Yii::app()->getClientScript();
			App7020Helpers::registerApp7020Options();
			$js->registerScriptFile(App7020Helpers::getAssetsUrl() . '/js/app7020.js', CClientScript::POS_END);

			$userId = $this->getUserId();

			// Search in table app7020_content.title
			$searchInput = Yii::app()->request->getParam('search_input', false);
			
			// Order by number of views
			$orderASC = Yii::app()->request->getParam('orderASC', false);
			$orderType = Yii::app()->request->getParam('orderType', false);

			// Data provider
			$assetsRanks = App7020Assets::getAssetsRanksDataProvider($userId, $searchInput, $orderASC, $orderType);
			
			// Assets types
			$assetsTypes = array(
				App7020Assets::CONTENT_TYPE_VIDEO => Yii::t('app7020', 'Video'),
				App7020Assets::CONTENT_TYPE_DOC => Yii::t('app7020', 'Document'),
				App7020Assets::CONTENT_TYPE_EXCEL => Yii::t('app7020', 'Excel'),
				App7020Assets::CONTENT_TYPE_PPT => Yii::t('app7020', 'Power point'),
				App7020Assets::CONTENT_TYPE_PDF => Yii::t('app7020', 'PDF'),
				App7020Assets::CONTENT_TYPE_TEXT => Yii::t('app7020', 'Text'),
				App7020Assets::CONTENT_TYPE_IMAGE => Yii::t('app7020', 'Image'),
				App7020Assets::CONTENT_TYPE_QUESTION => Yii::t('app7020', 'Question'),
				App7020Assets::CONTENT_TYPE_ANSWER => Yii::t('app7020', 'Answer'),
				App7020Assets::CONTENT_TYPE_OTHER => Yii::t('app7020', 'Other'),
				App7020Assets::CONTENT_TYPE_DEFAULT_OTHER => Yii::t('app7020', 'Other'),
				App7020Assets::CONTENT_TYPE_DEFAULT_MUSIC => Yii::t('app7020', 'Music'),
				App7020Assets::CONTENT_TYPE_DEFAULT_ARCHIVE => Yii::t('app7020', 'Archive'),
				App7020Assets::CONTENT_TYPE_LINKS => Yii::t('app7020', 'Link')
			);
			
			// Data for view
			$renderData = array('renderData' => 
				array(
					'searchedUserId' => $userId,
					'assetsRanks' => $assetsRanks,
					'assetsTypes' => $assetsTypes,
					
					
				) 
			);
			return $this->renderPartial('_tabs/_assetsRanks', $renderData, true);
		}
	
		public function doesUserHavePermission($role){
			$userModel = $this->getUserModel();
			if (!$userModel) {
				throw new CHttpException(500, 'Invalid user');
			}

			//if power user is viewing some user profile via the reports
			//don't check for permission when viewing own profile
			if(Yii::app()->user->getIsPu() && ($userModel->getPrimaryKey() != Yii::app()->user->getIdst())) {
				if(!Yii::app()->user->checkAccess($role)){
					return false;
				}
			}

			return true;
		}

}
