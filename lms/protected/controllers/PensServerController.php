<?php

class PensServerController extends Controller {

	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl',
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		return array(
			array('allow',
				'actions' => array('index'),
				'users' => array('*'),
			),
			array('allow',
				'users' => array('@'),
			),
			array('deny',
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
			),
		);
	}





	public function actionIndex() {

		$handler = new DoceboPENSPackageHandler();
		$handler->setSupportedPackageTypes(array("scorm-pif", "aicc-pkg", "xapi-pkg"));
		$handler->setSupportedPackageFormats(array("zip"));

		$server = DoceboPENSServer::singleton();
		$server->setPackageHandler($handler);

		$server->receiveCollect();
	}


}
