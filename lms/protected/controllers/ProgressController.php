<?php
/**
 * A controller providing common-used "progressive job dialog presentation".
 *
 * Example:
 * 	Runing an export of a large set of data, splitting the export in smaller parts, executed one after another and showing the progress in the dialog.
 *
 *
 */
class ProgressController extends Controller {


	/**
	 * Return JOB SPECIFIC URL, normally, an URL to controller/action along with specific parameters, if any
	 *
	 * Anyone can define a JOBTYPE_* constant in Progress component and add here his/her own JOB specific URL.
	 * This URL will be AJAX-called repeatedly until it returns a specific JSON data (see THIS controller "index" view and the JS)
	 * At the moment, returning response.data.stop=true is the way to stop the AJAX calls.
	 *
	 * For Real live usage see /admin/protected/controllers/ReportManagementController.php::actionProgressiveExporter()
	 *
	 * @param array $data Incoming data, basically the whole HTTP Request
	 * @return string
	 */
	protected function getJobActionUrl($data) {

		$url = "";
		$type = $data['type'];

		switch ($type)  {

			case Progress::JOBTYPE_CUSTOM_REPORT:
				$url = Docebo::createAbsoluteAdminUrl('reportManagement/progressiveExporter', array(
					'type' 			=> $type,		// required
					// specific
					'idReport' 		=> $data['idReport'],
					'exportType'	=> $data['exportType'],
				));
				break;

			case Progress::JOBTYPE_COURSE_USERS_REPORT:
				$params = array(
					'type' 			=> $type,		// required
					// specific
					'course_id' 		=> $data['course_id'], // for PLAYER, course_id is required
					'exportType'		=> $data['exportType'],
					'search_input'		=> $data['CoreUser']['search_input'] ? $data['CoreUser']['search_input'] : '',
				);
				$url = Docebo::createAbsoluteLmsUrl('//player/report/progressiveExporterCourseStats', $params);
				break;

			case Progress::JOBTYPE_COURSE_OBJECTS_REPORT:
				$params = array(
					'type' 			=> $type,		// required
					// specific
					'course_id' 		=> $data['course_id'], // for PLAYER, course_id is required
					'exportType'		=> $data['exportType'],
					'search_input'		=> $data['objects_search'] ? $data['objects_search'] : '',
					'user_id'			=> null,
				);
				if (isset($data['user_id'])) {
					$params['user_id'] = $data['user_id'];
				}
				$url = Docebo::createAbsoluteLmsUrl('//player/report/progressiveExporterCourseStats', $params);
				break;

			case Progress::JOBTYPE_COURSE_SINGLE_OBJECT_STATS:
				$params = array(
					'type' 			=> $type,		// required
					// specific
					'course_id' 		=> $data['course_id'], // for PLAYER, course_id is required
					'idOrg' 			=> $data['idOrg'],
					'idScormItem'		=> $data['idScormItem'],
					'exportType'		=> $data['exportType'],
					'search_input'		=> $data['objects_search'] ? $data['objects_search'] : '',
					//'quests'			=> $data['quests']
				);
				$url = Docebo::createAbsoluteLmsUrl('//player/report/progressiveExporterCourseStats', $params);
				break;

			case Progress::JOBTYPE_LDAP_IMPORT:
				$params = array(
					'type' => $type,		// required

				);
				$url = Docebo::createAbsoluteLmsUrl('LdapApp/LdapApp/progressiveImportUsers', $params);
				break;
				
			case Progress::JOBTYPE_IMPORT_USERS_CSV:
				$params = array(
					'type' => $type,	// required
				);
				$url = Docebo::createAbsoluteAdminUrl('userManagement/axProgressiveUsersImport', $params);
				break;	

			case Progress::JOBTYPE_ASSIGN_USERS_TO_BRANCH:
				$params = array(
					'type' => $type, //required
					//specific
					'select-orgchart' => $data['select-orgchart'],
					//'idst' => $data['idst'],
					'assign_type' => $data['assign_type']
				);
				$url = Docebo::createAbsoluteAdminUrl('userManagement/axProgressiveUsersAssignToNode', $params);
				break;

			case Progress::JOBTYPE_ASSIGN_USERS_TO_SINGLE_BRANCH:

				$url = Docebo::createAbsoluteAdminUrl('userManagement/axProgressiveUsersAssignToSingleNode');

			break;

			case Progress::JOBTYPE_ENROLL_USERS_TO_COURSE:
				$url = Docebo::createAbsoluteAdminUrl('courseManagement/axProgressiveEnrollUsersToSingleCourse');
				break;

			case Progress::JOBTYPE_ENROLL_USERS_TO_MANY_COURSES:
				$url = Docebo::createAbsoluteAdminUrl('courseManagement/axProgressiveEnrollUsersToManyCourses');
				break;

			case Progress::JOBTYPE_CHANGE_ENROLLMENT_STATUS:
				$url = Docebo::createAbsoluteAdminUrl('courseManagement/axProgressiveChangeEnrollmentStatus');
				break;

			case Progress::JOBTYPE_CHANGE_ENROLLMENT_DEADLINE:
				$url = Docebo::createAbsoluteAdminUrl('courseManagement/axProgressiveChangeEnrollmentDeadline');
				break;

			// Delete User enrollment from course
			case Progress::JOBTYPE_DELETE_USER_ENROLLMENT:
				$url = Docebo::createAbsoluteAdminUrl('courseManagement/axProgressiveDeleteUserEnrolment');
				break;

			case Progress::JOBTYPE_SEND_EMAIL_TO_ENROLLED_USERS:
				$url = Docebo::createAbsoluteAdminUrl('courseManagement/axProgressiveSendEmailToEnrolledUsers');
				break;

			case Progress::JOBTYPE_ENROLL_USERS_TO_SESSION:
				$url = Docebo::createAbsoluteAdminUrl('ClassroomApp/enrollment/axProgressiveEnrollUsersToSession');
				break;

			case Progress::JOBTYPE_DELETE_USERS_FROM_CLASSROOM_SESSION:
				$url = Docebo::createAbsoluteAdminUrl('ClassroomApp/enrollment/axProgressiveDeleteUsersFromSession');
				break;

			case Progress::JOBTYPE_ENROLL_USERS_TO_WEBINAR_SESSION:
				$url = Docebo::createAbsoluteAdminUrl('webinar/enrollment/axProgressiveEnrollUserseToWebinarSession');
				break;

			case Progress::JOBTYPE_DELETE_USERS_FROM_WEBINAR_SESSION:
				$url = Docebo::createAbsoluteAdminUrl('webinar/enrollment/axProgressiveDeleteUsersFromWebinarSession');
				break;

			case Progress::JOBTYPE_EXPORT_USERS_TO_CSV:
				$url = Docebo::createAbsoluteAdminUrl('userManagement/axProgressiveExportUsers');
				break;

			case Progress::JOBTYPE_AUTO_ASSIGN_USERS_TO_SINGLE_GROUP:
				$url = Docebo::createAbsoluteAdminUrl('groupManagement/axProgressiveAutoAssignUsersToSingleGroup');
				break;

			case Progress::JOBTYPE_UNASSIGN_USERS_FROM_GROUP:
				$url = Docebo::createAbsoluteAdminUrl('groupManagement/axProgressiveUnassignUsersFromGroup');
				break;

			case Progress::JOBTYPE_UNASSIGN_USERS_FROM_BRANCH:
				$url = Docebo::createAbsoluteAdminUrl('userManagement/unassignUsersFromNodeProgressive');
				break;

			case Progress::JOBTYPE_CHANGE_USER_STATUS:
				$url = Docebo::createAbsoluteAdminUrl('userManagement/axProgressiveChangeStatus');
				break;

			case Progress::JOBTYPE_IMPORT_SESSIONS:
				$params = array(
					'insertUpdate' => isset($data['SessionImportForm']['insertUpdate']) ? $data['SessionImportForm']['insertUpdate'] : 0
				);
				$url = Docebo::createAbsoluteAdminUrl('courseManagement/axProgressiveImportSessions', $params);
				break;

			case Progress::JOBTYPE_EXPORT_ACTIVE_USERS_REPORT:
				$url = Docebo::createAbsoluteLmsUrl('billing/default/axProgressiveExportActiveUsersReport', array(
					'exportType'	=> $data['exportType'],
					'model' => $data['LearningUserlimitLoginLog'],
				));
				break;

			case Progress::JOBTYPE_EXPORT_SALESFORCE_LOG:
				$url = Docebo::createAbsoluteAdminUrl('SalesforceApp/salesforceApp/axProgressiveExportLogs', array(
					'exportType' => $data['exportType']
				));
				break;

			case Progress::JOBTYPE_DELETE_USERS:
				$url = Docebo::createAbsoluteAdminUrl('userManagement/axProgressiveDeleteUsers');
				break;

			case Progress::JOBTYPE_ASSIGN_USERS_TO_BADGE:
				$url = Docebo::createAbsoluteAdminUrl('GamificationApp/GamificationApp/axProgressiveAssignUsersToBadge');
				break;

			case Progress::JOBTYPE_DELETE_MULTI_ASSIGNED_BADGE:
				$url = Docebo::createAbsoluteAdminUrl('GamificationApp/GamificationApp/axProgressiveDeleteMultiAssignedBadge');
				break;

			default:
				// check if the REQUEST is passing Job URL
				if (isset($data['job_url'])) {
					$url = $data['job_url'];
				}
				break;
		}

		if (!$url) {
			// Trigger event to let plugin handle custom Job action URLs
			$event = new DEvent($this, array('data' => $data));
			Yii::app()->event->raise('CollectProgressiveJobRun', $event);
			if(!$event->shouldPerformAsDefault() && isset($event->return_value['url']))
				$url = $event->return_value['url'];
		}

		return $url;
	}


	/**
	 * Based on type request, extract additional data (that will be passed as POST parameter in progress ajax requests).
	 * Useful when we have to specify big data such as list of IDs or whatever.
	 * @param $data
	 * @return array
	 */
	protected function getJobAdditionalData($data) {

		$output = array();
		$type = $data['type'];
		$customDialogClass = isset($data['customDialogClass']) ? $data['customDialogClass'] : '';

		switch ($type)  {

			case Progress::JOBTYPE_ASSIGN_USERS_TO_BRANCH:
				$output['idst'] = $data['idst'];
				break;

			case Progress::JOBTYPE_ASSIGN_USERS_TO_SINGLE_BRANCH:
				$output['idOrg'] = $data['idOrg'];
				$output['idst'] = $data['idst'];
				break;

			case Progress::JOBTYPE_IMPORT_SESSIONS:
				$output = array(
					'type' => Progress::JOBTYPE_IMPORT_SESSIONS,
					'action' => 'importSession'
				);
				break;

			case Progress::JOBTYPE_COURSE_SINGLE_OBJECT_STATS:
				$output['quests'] = $data['quests'];
				break;
		}

		$output['finalMessage'] = Yii::app()->request->getParam('finalMessage', '');
		$output['customDialogClass'] = $customDialogClass;

		return $output;
	}



	/**
	 * Render the initial dialog, which is job-irrelevant.
	 * Inside the dialog, AJAX calls will be made (recursively) to job-specific URL.
	 * Effectively, what is loaded in the dialog (and when to stop) is dictated by the job-specific action.
	 */
	public function actionRun() {

		$url = $this->getJobActionUrl($_REQUEST);
		$additionalData = $this->getJobAdditionalData($_REQUEST);
		$params = array(
			'jobActionUrl' => $url,
			'additionalData' => $additionalData
		);

		switch (Yii::app()->request->getParam('format')) {
			case 'json':
				$html = $this->renderPartial('index', $params, true);
				$this->sendJSON(array('html' => $html));
				break;
			default:
				$this->renderPartial('index', $params);
				break;
		}
	}


}
