<?php
/**
 * Send file content back to browser, based on REQUEST parameters. 
 * Designed to serve "/files" only using CFileStorage
 * 
 * URL parameters:
 * 	fn:  file name,  NO PATH
 *  col:  File storage "collection name". See CFileStorage for constants.
 *       Basically, this tells us where to search for this file in "/files" structure ONLY !
 *  sf:  (optional) Subfolder inside the "collection", i.e. /files/doecboLms/scorm  => doceboLms is a "collection", "scorm" is subfolder
 *  
 *    Example:  <img src='http://lmsp.petkoff.eu/lms/index.php?r=stream/file&fn=some_uploaded_file.png&col=uploads'>
 *    
 *  
 *  No access rules yet, but can be improved: per collection, per user privileges etc.
 *
 */
class StreamController extends Controller {
	

	private static $imageMimes = array('image/png', 'image/jpeg', 'image/jpg', 'image/gif');
	

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
				'accessControl', // perform access control for CRUD operations
		);
	}
	
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$res = array();
	
	
		// keep it in the following order:
	
		$res[] = array(
				'allow',
				'users' => array('@'),
		);
	
		$res[] = array(
				'deny', // deny all
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
		);
	
		return $res;
	}
	
	
	
	/**
	 * (non-PHPdoc)
	 * @see CController::init()
	 */
	public function init() {
	}

	
	/**
	 * Stream image back to browser
	 *
	 * @return string
	 */
	public function actionImage()  {
		$filename = basename(Yii::app()->request->getParam('fn', false));
		$collection = Yii::app()->request->getParam('col', false);  		// Collection name, optional
		$customSubFolder  = Yii::app()->request->getParam('sf', '');  	    // Subfolder inside collection, optional, defaults to ''
		
		// Collection Defaults _NONE
		$collection = $collection ? $collection : CFileStorage::COLLECTION_NONE;
		
		// Sanitize $filename here!!!!
		$filename = htmlspecialchars( stripslashes( $filename ) ) ;
		
		$storage = CFileStorage::getDomainStorage($collection);
		$storage->sendFile($filename, $filename, $customSubFolder);
		
	}
	
	
	
	/**
	 * Direct stream an arbitrary file by its name. 
	 * !!! NOTE !!! This is a dangerous operation (guessing a filename and collection name is enough)
	 * 
	 * NOTE2: Made only to test the possibility to stream a file from the file storage
	 * 
	 * 
	 */
	public function actionFile() {
		
		$filename = Yii::app()->request->getParam('fn', false);				// REAL file name stored in storage, usualy a hashed name
		$filenameDL = Yii::app()->request->getParam('fndl', false);			// The name of the file to send this file as (i.e. using some human readable name)
		$collection = Yii::app()->request->getParam('col', false);  		// Collection name, optional 
		$customSubFolder  = Yii::app()->request->getParam('sf', '');  	    // Subfolder inside collection, optional, defaults to ''

		// Sanitize 
		$filename 			= htmlspecialchars( stripslashes( urldecode($filename) ) ) ;
		$filenameDL 		= htmlspecialchars( stripslashes( urldecode($filenameDL) ) ) ;
		$collection 		= htmlspecialchars( stripslashes( urldecode($collection) ) ) ;
		$customSubFolder 	= htmlspecialchars( stripslashes( urldecode($customSubFolder) ) ) ;
		
		$collection = $collection ? $collection : CFileStorage::COLLECTION_NONE;
		$storage = CFileStorage::getDomainStorage($collection);
		
		$filenameDL = $filenameDL ? $filenameDL : $filename;
		$storage->sendFile($filename, $filenameDL, $customSubFolder);
		
	}
	
	
	
	
	
}