<?php

class CalloutsController extends Controller {

	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		return array(

			array('allow',
				'users' => array('*'),
			),

			array('deny',
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
			),
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 * This action is set as errorHandler in application config.
	 */
	public function actionError() {
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				$this->render('error', $error);
			}
		}
	}


	/**
	 * AJAX called action to mark a callout message as being read
	 *
	 */
	public function actionAxRead() {
		CalloutMessages::read( intval(Yii::app()->request->getParam('id')) );
	}
}

