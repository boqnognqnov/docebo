<?php
/**
 * Testing Controller.
 * @TODO Free to delete.
 *
 * It is NOT TestController, because there is always such in /admin and they apparently messing each other
 *
 * @author Plamen Petkov
 *
 */


class Test2Controller extends Controller
{


    /**
     * Image selector test
     */
    public function actionErpApi() {


        $getApp         = "lms/get-app";
        $getAllApps     = "lms/get-all-apps";

        $api = new ErpApiClient2();

        $res = $api->call($getAllApps);

        Log::_($res);

        CVarDumper::dump($res, 80, true);

    }


    /**
     * Image selector test
     */
    public function actionCf() {

        $this->render('index');

    }


    public function actionIs2() {


        echo $this->renderPartial('_is_first_dialog', array(

        ), false, true);


    }


    public function actionIndex()
    {

        echo realpath(Yii::getPathOfAlias('gii.assets'));



    }

    public function actionIndex2()
    {

        //$dp = App7020Helpers::sqlAssetUsersDataProvider(109);
        $params = array(
            //"idUser" => 391897,
            "idUser" => 212575,
            "idUser" => 522844,
            "idUser" => 13006,
            "idUser" => 212575,
            "idUser" => 1029664,
            "idUser" => 13002,
            "idUser" => 12301,
            //"idChannel" => array(14,7),

            //"search" => "001",

            //"idsOnly" => true,
            //"idAsset" => 1,
            //"pageSize" => 10,
            //"ignoreInvited" => true,
            "ignoreVisallChannels" => true,
            //"ignoreGodAdminEffect" => true,
            //"ignoreInvitedAssets"  => true,
            "conversionStatus" => array(20),
        );
        $dp = DataProvider::factory("UserAssets", $params)->provider();

        // Run-time pagination and sorting  set (after we get the provider)
//      $dp->pagination = array(
//          "pageSize"  => 30,
//          "currentPage" => 0,
//      );

        // No pagination, all items
//      $dp->pagination = false;

        // Set desired sorting, if different from default one (set IN data provider)
        // $sort = $dp->getSort();
        // $sort->defaultOrder = array("name" => CSort::SORT_ASC);
        // $dp->setSort($sort);

        $result = $dp->data;

        CVarDumper::dump($dp->totalItemCount, true);
        CVarDumper::dump($result, 80, true);


    }



    public function actionIndex3()
    {
        //$dp = App7020Helpers::sqlAssetUsersDataProvider(109);
        $params = array(
            "idChannel" => 20,

            //"idUser" => array(1067501,1067801),
            "ignoreVisallChannels" => true,
            "ignoreGodAdminEffect" => true,
            //"search"  => "Abby Jackson",
            //"knownTotalCount" => 879664,

            "idsOnly" => true,
            //"idAsset" => 432,
            //"pageSize" => 10,
        );
        $dp = DataProvider::factory("ChannelUsers", $params)->provider();

        // Run-time pagination and sorting  set (after we get the provider)
//          $dp->pagination = array(
//              "pageSize"  => 10,
//              "currentPage" => 0,
//          );

        // No pagination, all items
        $dp->pagination = false;

        // Set desired sorting, if different from default one (set IN data provider)
        // $sort = $dp->getSort();
        // $sort->defaultOrder = array("name" => CSort::SORT_ASC);
        // $dp->setSort($sort);

        $result = $dp->data;

        //Log::_($result);
        CVarDumper::dump($dp->totalItemCount, true);
        CVarDumper::dump($result, 80, true);

    }


    public function actionIndex4() {

        //$dp = App7020Helpers::sqlAssetUsersDataProvider(109);
        $params = array(
            //"idUser" => 391897,
            "idUser" => 212575,
            "idUser" => 522844,
            "idUser" => 13006,
            "idUser" => 212575,
            "idUser" => 1029664,
            "idUser" => 522844,
            "idUser" => 13002,
            //"idUser" => 391896,
            //"idChannel" => array(14,7),

            //"search" => "visall",

            //"idsOnly" => true,
            //"idAsset" => 1,
            //"pageSize" => 10,
            //"ignoreInvited" => true,
            //"ignoreVisallChannels" => true,
            //"ignoreGodAdminEffect" => true,
            //"ignoreInvitedAssets"  => true,
        );
        $dp = DataProvider::factory("UserChannels", $params)->provider();

        // Run-time pagination and sorting  set (after we get the provider)
        //      $dp->pagination = array(
        //          "pageSize"  => 30,
        //          "currentPage" => 0,
        //      );

        // No pagination, all items
        //      $dp->pagination = false;

        // Set desired sorting, if different from default one (set IN data provider)
        // $sort = $dp->getSort();
        // $sort->defaultOrder = array("name" => CSort::SORT_ASC);
        // $dp->setSort($sort);

        $result = $dp->data;

        //Log::_($result);
        CVarDumper::dump($dp->totalItemCount, true);
        CVarDumper::dump($result, 80, true);


    }


    public function actionIndex5() {

        //$dp = App7020Helpers::sqlAssetUsersDataProvider(109);
        $params = array(
            "idUser" => 13006,
            "idChannel" => array(14,16),

            //"search" => "ajax",

            "idsOnly" => true,
            //"idAsset" => array(11),
            //"pageSize" => 10,
            //"ignoreInvited" => true,
            "invitedOnly" => true,
        );
        $dp = DataProvider::factory("ChannelAssets", $params)->provider();

        // Run-time pagination and sorting  set (after we get the provider)
        //      $dp->pagination = array(
        //          "pageSize"  => 30,
        //          "currentPage" => 0,
        //      );

        // No pagination, all items
        //      $dp->pagination = false;

        // Set desired sorting, if different from default one (set IN data provider)
        // $sort = $dp->getSort();
        // $sort->defaultOrder = array("name" => CSort::SORT_ASC);
        // $dp->setSort($sort);

        $result = $dp->data;

        //Log::_($result);
        CVarDumper::dump($dp->totalItemCount, true);
        CVarDumper::dump($result, 80, true);


    }


    public function actionPlupload() {

        $this->render('plupload');

    }




    public function actionIndex6() {



    }

    public function actionIndex7() {

        $result = sha1(
                'docebo_setup:' .
                Settings::getCfg('erp_secret_key') . ':' .
                Settings::getCfg('erp_installation_id') . ':' .
                Settings::getCfg('install_from')
        );

        CVarDumper::dump($result, 80, true);

    }


    public function actionCreateExample7020Topics()
    {

        $m0 = new App7020TopicTree();
        $m0->saveNode();
        $trans = new App7020TopicTranslation();
        $trans->idTopic = $m0->id;
        $trans->language = 'en';
        $trans->text = 'Topic 0';
        $trans->save();


        $m = new App7020TopicTree();
        $m0->append($m);
        $trans = new App7020TopicTranslation();
        $trans->idTopic = $m->id;
        $trans->language = 'en';
        $trans->text = 'Topic 1';
        $trans->save();

        $m = new App7020TopicTree();
        $m0->append($m);
        $trans = new App7020TopicTranslation();
        $trans->idTopic = $m->id;
        $trans->language = 'en';
        $trans->text = 'Topic 2';
        $trans->save();


                $m1 = new App7020TopicTree();
                $m->append($m1);
                $trans = new App7020TopicTranslation();
                $trans->idTopic = $m1->id;
                $trans->language = 'en';
                $trans->text = 'Topic 2-1';
                $trans->save();

                $m1 = new App7020TopicTree();
                $m->append($m1);
                $trans = new App7020TopicTranslation();
                $trans->idTopic = $m1->id;
                $trans->language = 'en';
                $trans->text = 'Topic 2-2';
                $trans->save();

        $m = new App7020TopicTree();
        $m0->append($m);
        $trans = new App7020TopicTranslation();
        $trans->idTopic = $m->id;
        $trans->language = 'en';
        $trans->text = 'Topic 3';
        $trans->save();

                $m1 = new App7020TopicTree();
                $m->append($m1);
                $trans = new App7020TopicTranslation();
                $trans->idTopic = $m1->id;
                $trans->language = 'en';
                $trans->text = 'Topic 3-1';
                $trans->save();


                        $m2 = new App7020TopicTree();
                        $m->append($m2);
                        $trans = new App7020TopicTranslation();
                        $trans->idTopic = $m2->id;
                        $trans->language = 'en';
                        $trans->text = 'Topic 3-1-1';
                        $trans->save();



        $sql = "
        ";




    }



    public function actionHardApp7020Example() {

        $sql = <<< SQL

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

TRUNCATE TABLE `app7020_content`;
INSERT IGNORE INTO `app7020_content` (`id`, `title`, `description`, `filename`, `originalFilename`, `contentType`, `conversion_status`) VALUES
(1, 'content 1', 'content 1', 'fdgdfg', 'gfhfgh', 'video', 0),
(2, 'content 2', 'content 2', 'hgjgj', 'ghjghj', 'video', 0),
(3, 'content 3', 'content 3', 'dfgdfg', 'dfgdfg', 'video', 0);

TRUNCATE TABLE `app7020_tag`;
INSERT IGNORE INTO `app7020_tag` (`id`, `tagText`) VALUES
(1, 'Tag 1'),
(2, 'Tag 2'),
(3, 'Tag 3'),
(4, 'Tag 4'),
(5, 'Tag 5'),
(6, 'Tag 6'),
(7, 'Tag 7');

TRUNCATE TABLE `app7020_tag_link`;
INSERT IGNORE INTO `app7020_tag_link` (`id`, `idTag`, `idContent`) VALUES
(1, 1, 1),
(2, 1, 2),
(7, 1, 3),
(3, 2, 2),
(4, 3, 2);

TRUNCATE TABLE `app7020_topic_content`;
INSERT IGNORE INTO `app7020_topic_content` (`id`, `idTopic`, `idContent`) VALUES
(1, 25, 1),
(2, 25, 2),
(3, 27, 2),
(4, 30, 1),
(5, 30, 2),
(6, 30, 3);

TRUNCATE TABLE `app7020_topic_translation`;
INSERT IGNORE INTO `app7020_topic_translation` (`id`, `idTopic`, `language`, `text`) VALUES
(18, 24, 'en', 'Topic 0'),
(19, 25, 'en', 'Topic 1'),
(20, 26, 'en', 'Topic 2'),
(21, 27, 'en', 'Topic 2-1'),
(22, 28, 'en', 'Topic 2-2'),
(23, 29, 'en', 'Topic 3'),
(24, 30, 'en', 'Topic 3-1'),
(25, 31, 'en', 'Topic 3-1-1'),
(26, 31, 'bg', 'Topic 3-1-1 - Bulgarian');

TRUNCATE TABLE `app7020_topic_tree`;
INSERT IGNORE INTO `app7020_topic_tree` (`id`, `iLeft`, `iRight`, `level`, `subtree`) VALUES
(24, 1, 16, 1, NULL),
(25, 2, 3, 2, NULL),
(26, 4, 9, 2, NULL),
(27, 5, 6, 3, NULL),
(28, 7, 8, 3, NULL),
(29, 10, 15, 2, NULL),
(30, 11, 12, 3, NULL),
(31, 13, 14, 3, NULL);
SET FOREIGN_KEY_CHECKS=1;

SQL;

        Yii::app()->db->createCommand($sql)->execute();


    }


    /**
     * Simulate current user posting a new reply to a random forum discussion
     */
    public function actionForumNewReply()
    {
        $thread = LearningForumthread::model()->find(); // get any thread (first available in this case)
        $user = CoreUser::model()->findByPk(Yii::app()->user->getId());
        $course = $thread->forum->course; // get from relation

        Yii::app()->event->raise('NewReply', new DEvent($thread, array(
            'thread' => $thread, // AR relation
            'user'=> $user, // AR relation
            'course' => $course, // AR relation
        )));
    }



    public function actionUpload() {

        $this->render('index');

    }




    public function actionExample1() {

        $this->render('example1', array(
            'data' => $_POST,
        ));

    }


    public function actionExample2() {

        $this->render('example2', array(
                'data' => $_POST,
        ));

    }



    public function actionAxExample1Dialog() {

        $html = $this->renderPartial('dialog', array(
            'data' => $_POST,
        ), true, true);
        echo $html;

    }


    public function actionAxExample2Dialog() {

        $html = $this->renderPartial('dialog2', array(
                'data' => $_POST,
        ), true, true);
        echo $html;

    }


    public function actionFancytree() {

        $fancyTreeData = CoreOrgChartTree::buildFancytreeDataArray(false,true,true);

        Log::_(Yii::app()->cache);



        echo 'Done'; die;

        $preSelected = array(
            array('key' => 10, 'selectState' => 2),
            array('key' => 124, 'selectState' => 1),
        );

        $this->render('fancytree_test', array(
            'treeData' => $treeData,
            'preSelected' => $preSelected,
        ));

    }


    public function actionFancytreeIndex() {

        $this->render('fancytree_index');
    }




    public function actionAxFancyTreeDialog() {
        $html = $this->renderPartial('fancytree_dialog', array(), true, true);
        echo $html;
    }



    public function actionU2() {

        $cs = Yii::app()->getClientScript();
        $themeUrl = Yii::app()->theme->baseUrl;
        $cs->registerScriptFile($themeUrl . '/js/jwizard.js');


        // Register Usersselector JS/CSS; this is required for IE8/9
        $cs->registerCssFile($themeUrl . '/css/admin.css');
        $cs->registerCssFile($themeUrl . '/css/usersselector.css');
        $cs->registerScriptFile($themeUrl . '/js/usersselector.js');

        UsersSelector::registerClientScripts();

        $this->render('u2');

    }

    public function actionU3() {

        $cs = Yii::app()->getClientScript();
        $themeUrl = Yii::app()->theme->baseUrl;
        $cs->registerScriptFile($themeUrl . '/js/jwizard.js');
        Yii::app()->clientScript->registerCoreScript('jquery.ui');

        UsersSelector::registerClientScripts();

        $this->render('u3');

    }


    /**
     * Users Selector Dialog result collector
     *
     */
    public function actionCollectorTest() {


        echo '<a class="auto-close success"></a>';

    }


    /**
     * Final action called by jWizard Demo. Receives collected data from all steps into one big POST Array:  $_POST['Wizard']
     * Feel free to experiment and see the structure of the $_POST
     */
    public function actionAxWizardFinish() {

        // Get resulting users list from the Users Selector, used for Step 2 in this Demo
        // You can run many selectors in a row (steps), just give them different "formId" when you call it
        $usersList = UsersSelector::getUsersList($_POST['Wizard']['users-selector-form']);  // <-- 'users-selector-form' is the formId, by default

        $html = CVarDumper::dumpAsString($usersList, 20, true);

        // To see the $POST inside the dialog, no close
        //$html = CVarDumper::dumpAsString($_POST, 20, true);

        // To close the wizard
        //$html = '<a class="auto-close success"></a>';

        echo $html;
    }


    public function actionAxStep1() {
        $html = $this->renderPartial('step1', array(),true,true);
        echo $html;
    }


    public function actionAxStep2() {
        $html = $this->renderPartial('step2', array(),true,true);
        echo $html;
    }


    public function actionAxStep3() {
        $html = $this->renderPartial('step3', array(),true,true);
        echo $html;
    }


    public function actionAxStep4() {
        $html = $this->renderPartial('step4', array(),true,true);
        echo $html;
    }



    public function actionTestImg() {

        echo CFileStorage::getSystemFilesStorage(CFileStorage::COLLECTION_ASSETS, CFileStorage::TYPE_FILESYSTEM)->fileUrl('somefile.txt');
        echo '<hr>';
        echo CFileStorage::getSystemFilesStorage(CFileStorage::COLLECTION_ASSETS, CFileStorage::TYPE_S3)->fileUrl('somefile.txt');
        echo '<hr>';


    }


    public function actionTestImg2() {

//      $storage = CFileStorage::getSystemFilesStorage(CFileStorage::COLLECTION_ASSETS);
//      $list =  $storage->findFiles('images\courselogo\micro');
//      var_dump($list);

//      $model = new CoreAsset();
//      $model->assetType = CoreAsset::TYPE_PLAYER_BACKGROUND;
//      echo $model->assetType;

        //$model->populateImagesFromSharedAssets();


//      $assets = new CoreAsset();
//      $systemImagesTmp = $assets->getSharedPlayerBgImageURLs(CoreAsset::TYPE_PLAYER_BACKGROUND, CoreAsset::VARIANT_ORIGINAL, 6);
//      var_dump($systemImagesTmp);


//      $asset = CoreAsset::model()->findByPk(1689);
//      if ($asset) {
//          echo $asset->getUrl();
//      }
//      Yii::app()->end();


//      $asset = new CoreAsset();
//      //$asset->sourceFile = 'E:\www\docebo_lms_63\files\lms63_petkoff_eu\upload_tmp\16_full_slide1_1.jpg';
//      $asset->sourceFile = 'E:\Docebo\15_3_1373024274_Guia do curso.pdf';
//      $asset->type = CoreAsset::TYPE_GENERAL_ASSET;
//      $asset->save();


//      $asset = new CoreAsset();
//      $x = $asset->urlList(CoreAsset::TYPE_PLAYER_BACKGROUND, CoreAsset::SCOPE_PRIVATE, 6, CoreAsset::VARIANT_ORIGINAL);
//      var_dump($x);

        //$model = $assets->findByPk(938);
        //echo $model->getUrl();


//      $asset = CoreAsset::model()->findByPk(2064);
//      var_dump($asset);
//      $asset->delete();


//      $asset = new CoreAsset();
//      $asset->sourceFile ='E:\Docebo\1.pdf';
//      if (!$asset->save()) {
//          var_dump($asset->errors);
//          Yii::app()->end();
//      }
//      var_dump($asset);

//      $asset = new CoreAsset();
//      $asset->type = CoreAsset::TYPE_GENERAL_IMAGE;
//      $asset->sourceFile ='E:\Docebo\1.png';
//      $asset->save();


        echo Docebo::getRootBasePath() . DIRECTORY_SEPARATOR . 'plugins' . DIRECTORY_SEPARATOR . 'GamificationApp' . DIRECTORY_SEPARATOR . 'assets';



    }


    public function actionGenerateSharedImages() {

        // http://lms63.petkoff.eu/lms/index.php?r=test2/generateSharedImages

        $model = new CoreAsset();
        // Generate shared images FROM local /themes into file storage (be it local or S3)
        $model->generateSharedImages();

    }


    public function actionPopulateTableFromSharedImages() {

        // http://lms63.petkoff.eu/lms/index.php?r=test2/populateTableFromSharedImages

        $model = new CoreAsset();
        // Generate shared images FROM local /themes into file storage (be it local or S3)
        $model->populateImagesFromSharedAssets();

    }


    public function actionGoAssets() {

        $model = new CoreAsset();
        // Generate shared images FROM local /themes into file storage (be it local or S3)
        $model->generateSharedImages();
        // Populate core_asset table with all data about shared images
        $model->populateImagesFromSharedAssets();

        // We are ready to go....

    }



    public function actionWizardDemo() {

        $cs = Yii::app()->getClientScript();
        $themeUrl = Yii::app()->theme->baseUrl;
        $cs->registerScriptFile($themeUrl . '/js/jwizard.js');


        $cs->registerCoreScript('jquery.ui');
        $cs->registerCssFile($cs->getCoreScriptUrl().'/jui/css/base/jquery-ui.css');


        // Register Usersselector JS/CSS; this is required for IE8/9
        $cs->registerCssFile($themeUrl . '/css/admin.css');
        $cs->registerCssFile($themeUrl . '/css/usersselector.css');
        $cs->registerScriptFile($themeUrl . '/js/usersselector.js');


        $this->render('wizard_demo');

    }


    /**
     *
     */
    public function actionAxWizardDemoDispatcher() {


        if ($_POST['finish_button']) {
            $this->renderPartial('step_final');
            Yii::app()->end();
        }

        if ($_POST['from_step'] == 'step1') {
            $this->redirect(Docebo::createLmsUrl('usersSelector/axOpen', array('type' => 'wizarddemo')));
            Yii::app()->end();
        }

        if (($_POST['from_step'] == 'uni-selector') && $_POST['next_button']) {
            $this->renderPartial('step3');
            Yii::app()->end();
        }
        if (($_POST['from_step'] == 'uni-selector') && $_POST['prev_button']) {
            $this->renderPartial('step1');
            Yii::app()->end();
        }

        if ($_POST['from_step'] == 'step3') {
            $this->redirect(Docebo::createLmsUrl('usersSelector/axOpen', array('type' => 'wizarddemo')));
            Yii::app()->end();
        }




    }


    /**
     * MOST Up-to-date
     */
    public function actionWizardDemo2() {
        $cs = Yii::app()->getClientScript();
        $themeUrl = Yii::app()->theme->baseUrl;
        $cs->registerCoreScript('jquery.ui');
        $cs->registerCssFile($themeUrl . '/css/admin.css');
        $cs->registerCssFile($cs->getCoreScriptUrl().'/jui/css/base/jquery-ui.css');


        UsersSelector::registerClientScripts();
        $this->render('wizard_demo');
    }




    /**
     * Run a simpole wizard of TWO  selectors:  one for Users and one for Courses
     *
     */
    public function actionWizardDemo2Dispatcher() {


        $fromStep                   = Yii::app()->request->getParam('from_step', 'initial');
        $nextButton                 = Yii::app()->request->getParam('next_button', false);
        $finishButton               = Yii::app()->request->getParam('jwizard_save_button', false);
        $prevButton                 = Yii::app()->request->getParam('prev_button', false);
        $idForm                     = Yii::app()->request->getParam('idForm', false);

        $idPlan                     = Yii::app()->request->getParam('idPlan', false);

        if ($finishButton) {
            echo '<a class="auto-close success"></a>';
            Yii::app()->end();
        }


        // The "from step" is always "uni-selector" for "Uni Selector" dialog.
        // We must recognize the 'from step' based on the FORM ID we previosly assigned
        if ($fromStep == 'uni-selector' && $idForm == 'selector-users') {
            $fromStep = 'select-users-step';
        }
        else if ($fromStep == 'uni-selector' && $idForm == 'selector-courses') {
            $fromStep = 'select-courses-step';
        }

        $usersSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
                'type'          => UsersSelector::TYPE_MASS_ENROLL_LP_USERS,
                'idPlan'        => $idPlan,
                'idForm'        => 'selector-users',
                'wizmode'       => 1, // mandatory
                'wizardParams'  => array(
                    'wizard_mode'   => 1, // mandatory
                    'first'         => 1,
                ),
        ));


        $coursesSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
                'type'          => UsersSelector::TYPE_MASS_ENROLL_LP_COURSES,
                'idPlan'        => $idPlan,
                'idForm'        => 'selector-courses',
                'wizmode'       => 1,  // mandatory
                'wizardParams'  => array(
                    'wizard_mode'   => 1,  // mandatory
                    'final'         => 1,
                    'last'          => 1
                ),

        ));


        switch ($fromStep) {

            // First/Initial step, when FROM step is not coming from the request
            case 'initial':
                $this->redirect($usersSelectorUrl);
                Yii::app()->end();
                break;

            // Coming from USERS selector?  Then we know - next one is COURSES
            case 'select-users-step':
                $this->redirect($coursesSelectorUrl);
                Yii::app()->end();
                break;

            // Coming from COURSES? We know this is the last step, also we already handled the FINISH case, so we go back to USERS
            // We also can analyze "prev_button"/ "next_button", if clicked..
            case 'select-courses-step':
                $this->redirect($usersSelectorUrl);
                Yii::app()->end();
                break;

        }




    }


    /**
     * Analyzes Bootstrap Datepicker JS localization files and compares
     * them to the available languages in the current LMS (DB wise)
     * and logs all missing localization JS files for debugging purposes
     */
    public function actionCheckMissingBootstrapLocales(){
        $allLangs = CoreLangLanguage::model()->findAll();
        foreach($allLangs as $lang){
            if(!is_file(Yii::getPathOfAlias('bootstrap.assets')."/js/".'locales/bootstrap-datepicker.'.$lang->lang_browsercode.'.js')){
                Yii::log('Missing Bootstrap datepicker locale browsercode:' .$lang->lang_browsercode, CLogger::LEVEL_ERROR);
            }
        }
    }

    public function actionShowInteractions($trackId = null) {
        $criteria = new CDbCriteria();

        if($trackId) {
            $criteria->addCondition("idscorm_tracking = :id_track");
            $criteria->params[':id_track'] = $trackId;
        } else
            $criteria->addCondition("xmldata LIKE '%interactions%'");
        $criteria->limit = 2;

        $trackings = LearningScormTracking::model()->findAll($criteria);
        foreach($trackings as $tracking) {
            try {
                Log::_($tracking->getLearnerInteractions());
            } catch(Exception $e) {
                Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
            }
        }
    }

    public function actionShowInteractionStatistics($objectId) {
        $scormItem = LearningScormItems::model()->findByPk($objectId);
        if($scormItem) {
            Log::_($scormItem->collectQuestionsAndAnswers());
        }
    }

    public function actionTestAdobeConnect() {
        $adobeTool = WebinarTool::getById('adobeconnect');
        $result = $adobeTool->createRoom('Test Raffaele', 'hello world', 2, '2015-02-08 21:45', '2015-02-08 22:00', 10);
        Log::_($result);
        $result = $adobeTool->updateRoom('Test Raffaele updated', 'hello world', 2, $result, '2015-02-08 21:45', '2015-02-08 23:00', 10);
        Log::_($result);
        Log::_($adobeTool->getJoinRoomUrl('Test Raffaele', 0, 2, $result, Yii::app()->user->loadUserModel(), null));
        Log::_($adobeTool->getStartRoomUrl('Test Raffaele', 0, 2, $result, Yii::app()->user->loadUserModel(), null));
        $adobeTool->deleteRoom(2, $result);
    }

    public function actionTestBigbluebutton() {
        $bbbTool = WebinarTool::getById('bigbluebutton');
        $result = $bbbTool->createRoom('Test Raffaele', 'hello world', 3, '2015-02-08 21:45', '2015-02-08 22:00', 10);
        Log::_($result);
        $result = $bbbTool->updateRoom('Test Raffaele updated', 'hello world', 3, $result, '2015-02-08 21:45', '2015-02-08 23:00', 10);
        Log::_($result);
        Log::_($bbbTool->getJoinRoomUrl('Test Raffaele', 0, 3, $result, Yii::app()->user->loadUserModel(), null));
        Log::_($bbbTool->getStartRoomUrl('Test Raffaele', 0, 3, $result, Yii::app()->user->loadUserModel(), null));
        $bbbTool->deleteRoom(3, $result);
    }

    public function actionTestGotomeeting() {
        $g2mTool = WebinarTool::getById('gotomeeting');
        $result = $g2mTool->createRoom('Test Raffaele', 'hello world', 5, '2015-02-10 21:45', '2015-02-10 22:00', 10);
        Log::_($result);
        $result = $g2mTool->updateRoom('Test Raffaele updated', 'hello world', 5, $result, '2015-02-10 21:45', '2015-02-10 23:00', 10);
        Log::_($result);
        Log::_($g2mTool->getJoinRoomUrl('Test Raffaele', 0, 5, $result, Yii::app()->user->loadUserModel(), null));
        Log::_($g2mTool->getStartRoomUrl('Test Raffaele', 0, 5, $result, Yii::app()->user->loadUserModel(), null));
        $g2mTool->deleteRoom(5, $result);
    }

    public function actionTestOnsync() {
        $onsyncTool = WebinarTool::getById('onsync');
        $result = $onsyncTool->createRoom('Test Raffaele', 'hello world', 6, '2015-02-10 21:45:00', '2015-02-10 22:00:00', 10);
        Log::_($result);
        $result = $onsyncTool->updateRoom('Test Raffaele updated', 'hello world', 6, $result, '2015-02-10 21:45:00', '2015-02-10 23:00:00', 10);
        Log::_($result);
        Log::_($onsyncTool->getJoinRoomUrl('Test Raffaele', 0, 6, $result, Yii::app()->user->loadUserModel(), null));
        Log::_($onsyncTool->getStartRoomUrl('Test Raffaele', 0, 6, $result, Yii::app()->user->loadUserModel(), null));
        $onsyncTool->deleteRoom(6, $result);
    }

    public function actionTestSkymeeting() {
        $skymeetingTool = WebinarTool::getById('skymeeting');
        $result = $skymeetingTool->createRoom('Test Raffaele', 'hello world', 7, '2015-02-10 21:45:00', '2015-02-10 22:00:00', 10);
        Log::_($result);
        $result = $skymeetingTool->updateRoom('Test Raffaele updated', 'hello world', 7, $result, '2015-02-10 21:45:00', '2015-02-10 23:00:00', 10);
        Log::_($result);
        Log::_($skymeetingTool->getJoinRoomUrl('Test Raffaele', 0, 7, $result, Yii::app()->user->loadUserModel(), null));
        Log::_($skymeetingTool->getStartRoomUrl('Test Raffaele', 0, 7, $result, Yii::app()->user->loadUserModel(), null));
        $skymeetingTool->deleteRoom(7, $result);
    }

    public function actionTestTeleskill() {
        $teleskillTool = WebinarTool::getById('teleskill');
        $result = $teleskillTool->createRoom('Test Raffaele', 'hello world', 8, '2015-02-10 21:45:00', '2015-02-10 22:00:00', 10);
        Log::_($result);
        $result = $teleskillTool->updateRoom('Test Raffaele updated', 'hello world', 8, $result, '2015-02-10 21:45:00', '2015-02-10 23:00:00', 10);
        Log::_($result);
        Log::_($teleskillTool->getJoinRoomUrl('Test Raffaele', 0, 8, $result, Yii::app()->user->loadUserModel(), null));
        Log::_($teleskillTool->getStartRoomUrl('Test Raffaele', 0, 8, $result, Yii::app()->user->loadUserModel(), null));
        $teleskillTool->deleteRoom(8, $result);
    }

    public function actionTestWebexMeetings() {
        $sessionModel = WebinarSession::model()->findByPk(1);
        $webexTool = WebinarTool::getById('webex_meetings');
        $result = $webexTool->createRoom('Test Raffaele', 'hello world', 9, '2015-02-11 21:45:00', '2015-02-11 22:00:00', 10);
        Log::_($result);
        $result = $webexTool->updateRoom('Test Raffaele updated', 'hello world', 9, $result, '2015-02-11 21:45:00', '2015-02-11 23:00:00', 10);
        Log::_($result);
        Log::_($webexTool->getJoinRoomUrl('Test Raffaele', 0, 9, $result, Yii::app()->user->loadUserModel(), $sessionModel));
        Log::_($webexTool->getStartRoomUrl('Test Raffaele', 0, 9, $result, Yii::app()->user->loadUserModel(), $sessionModel));
        $webexTool->deleteRoom(9, $result);
    }

    public function actionTestWebexEvents() {
        $sessionModel = WebinarSession::model()->findByPk(1);
        $webexTool = WebinarTool::getById('webex_events');
        $result = $webexTool->createRoom('Test Raffaele', 'hello world', 9, '2015-02-11 21:45:00', '2015-02-11 22:00:00', 10);
        Log::_($result);
        $result = $webexTool->updateRoom('Test Raffaele updated', 'hello world', 9, $result, '2015-02-11 21:45:00', '2015-02-11 22:00:00', 10);
        Log::_($result);
        Log::_($webexTool->getJoinRoomUrl('Test Raffaele', 0, 9, $result, Yii::app()->user->loadUserModel(), $sessionModel));
        Log::_($webexTool->getStartRoomUrl('Test Raffaele', 0, 9, $result, Yii::app()->user->loadUserModel(), $sessionModel));
        $webexTool->deleteRoom(9, $result);
    }


    public function actionWindowSize() {
        $this->render('windowSize');
    }


    public function actionTestCourseFields() {
        $courseField = new LearningCourseField();
        $courseField->type = 'textfield';
        $courseField->sequence = $courseField->getNextSequence();
        $courseField->setFieldTranslations(array(
            'english' => 'Food',
            'italian' => 'Spaghetti'
        ));

        try {
            if($courseField->save()) {
                Log::_("Created field with ID = e" . $courseField->id_field);
                $courseField->delete();
                Log::_("Deleted field with ID = " . $courseField->id_field);
            } else
                throw new Exception(CHtml::errorSummary($courseField));
        } catch(Exception $e) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
        }
    }

    /**
     * Testing Amazon Elastic Transcoder
     */
    public function actionSnsEndpoint() {

        // Get data from the request payload
        $payload = file_get_contents('php://input');
        $data = CJSON::decode($payload);

        // Get all headers
        $headersOriginal = getallheaders();

        // Make headers's keys and values LOWER case.
        $headers = array();
        foreach ($headersOriginal as $key => $value) {
            $headers[strtolower($key)] = $value;
        }

        Log::_('SERVER');
        Log::_($_SERVER);
        Log::_('SNS HEADERS');
        Log::_($headers);
        Log::_('SNS PAYLOAD DATA');
        Log::_($data);

        // Get Message type
        $snsMessageType =  isset($headers['x-amz-sns-message-type']) ? $headers['x-amz-sns-message-type'] : false;
        if (!$snsMessageType) {
            Yii::app()->end();
        }


        // SNS SUBSCRIPTION COnfirmation request: Is this a Subscription Confirmation message???
        if ($snsMessageType == 'SubscriptionConfirmation') {
            // Get storage; it will give us the S3 credentials and region
            $storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_VIDEO);
            AmazonSnsHelper::confirmSubscription($storage->getS3Key(), $storage->getS3Secret(), $storage->getS3Region(), $data['Token'], $data['TopicArn']);
            Yii::app()->end();
        }


    }
    /**
     * Testing Flowplayer Widget
     */
    public function actionFlowplayerW() {
        $this->widget('common.widgets.Flowplayer');
    }


    public function actionUploadSubs() {


        $f1 = "E:\\Docebo\\english.vtt";
        $f2 = "E:\\Docebo\\bulgarian.vtt";
        $idVideo = 7;

        $asset = new CoreAsset();
        $asset->type = CoreAsset::TYPE_SUBTITLES . "/" . $idVideo;
        $asset->sourceFile = $f1;
        $asset->save();
        var_dump($asset->errors);
        $f1Id = $asset->id;

        $asset = new CoreAsset();
        $asset->type = CoreAsset::TYPE_SUBTITLES . "/" . $idVideo;
        $asset->sourceFile = $f2;
        $asset->save();
        var_dump($asset->errors);
        $f2Id = $asset->id;

        var_dump($f1Id);
        var_dump($f2Id);

        $s = new LearningVideoSubtitles();
        $s->id_video = $idVideo;
        $s->lang_code = "english";
        $s->fallback = 1;
        $s->asset_id = $f1Id;
        $s->date_added = Yii::app()->localtime->getUTCNow();
        $s->save();
        var_dump($s->errors);

        $s = new LearningVideoSubtitles();
        $s->id_video = $idVideo;
        $s->lang_code = "bulgarian";
        $s->fallback = 0;
        $s->asset_id = $f2Id;
        $s->date_added = Yii::app()->localtime->getUTCNow();
        $s->save();
        var_dump($s->errors);



    }



    public function actionElucidat() {

        $key = "97158BB6-4E84-38A3-810D-F369FB0EB6F0";
        $secret = "6E9778C3-F56C-F143-BA91-3ED6BBEC47EE";
        $simulation = false;

        $client = new ElucidatApiClient($key, $secret, $simulation);

        //CVarDumper::dump($client->getAuthors(), 80, true); die;

//         $url = Docebo::createAbsoluteLmsUrl("test2/elucidatListener");
//         var_dump($url);
//         CVarDumper::dump($client->subscribeToEvent(ElucidatApiClient::EVENT_RELEASE_COURSE, $url), 80, true); die;

        CVarDumper::dump($client->getEventCallbacks(), 80, true); die;

        //CVarDumper::dump($client->getSsoUrl("daniele.minoia@docebo.com", "https://app.elucidat.com/"), 80, true); die;

        //$url = $client->getSsoUrl("dev.danieleminoia@gmail.com", "https://app.elucidat.com/projects"); $this->redirect($url, true);die;

        //CVarDumper::dump($client->getSsoUrl("dev.danieleminoia@gmail.com", "http://lms664.petkoff.eu"), 80, true); die;


        //CVarDumper::dump($client->getProjects(), 80, true); die;


        //CVarDumper::dump($client->getReleases("55fd78e63b2a2"), 80, true); die;
        //CVarDumper::dump($client->getReleases("5608e10346bb5"), 80, true); die;

        //CVarDumper::dump($client->getReleaseDetails("55fd83b707e4f"), 80, true); die;
        //CVarDumper::dump($client->getReleaseDetails("5608e16b33c52"), 80, true); die;

        //CVarDumper::dump($client->getLaunchUrl("55fd83b707e4f", "Plamen Petkov", "plamen@petkoff.eu"), 80, true); die;
        CVarDumper::dump($client->getLaunchUrl("560b92413c1fc", "Plamen Petkov", "plamen@petkoff.eu"), 80, true); die;


        //$params = new ElucidatProjectParams();
        //$params->xapi_lrs_endpoint_url = "http://lms664.petkoff.eu";
        //$params->xapi_lrs_endpoint_username = "/admin";
        //$params->xapi_lrs_endpoint_password = '$2a$13$39n43prvtoe4M9Yp77CdmOoqTG/EVOTXw9kJVyrFfOergENqzHaYi';
        // CVarDumper::dump($client->configureProject("55fd78e63b2a2", $params), 80, true); die;



        //CVarDumper::dump($client->getFullInfoArray(), 80, true); die;



    }



    public function  actionElucidatListener() {


        Log::_("LISTENER");
        Log::_($_POST);
        Log::_($_GET);
        Log::_(getallheaders());
        Log::_(301);
    }



    public function actionRedis() {

        //$r1 = new RedisBase(REDIS_CONFIG_HOST, REDIS_CONFIG_PORT, REDIS_CONFIG_DB);
        //CVarDumper::dump($r1, 80, true); die;

        $x = new RedisSettings();
        CVarDumper::dump(RedisSettings::getAllLmsSettings(), 80, true); die;


    }




    /**
     * Test Notification shift period calculations
     */
    public function actionShift() {


        function getTargetPeriodForShifted($shiftType, $shiftNumber, $shiftPeriod, $pivotUtc = false, $wholeDay=false) {

            if ($pivotUtc === false) {
                $pivotUtc = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
            }

            $tzUtc  = new DateTimeZone('UTC');
            $dt     = new DateTime($pivotUtc, $tzUtc);

            switch ($shiftPeriod) {
                case CoreNotification::SCHED_SHIFT_PERIOD_HOUR:
                    $hours = ' H:i:s';
                    $dtInterval         = new DateInterval('PT' . ((int) $shiftNumber) . 'H');
                    $dtIntervalSingle   = new DateInterval('PT1H');
                    break;
                case CoreNotification::SCHED_SHIFT_PERIOD_WEEK:
                    $hours = $wholeDay ? false : ' H:i:s';
                    $dtInterval         = new DateInterval('P' . ((int) $shiftNumber * 7) . 'D');
                    $dtIntervalSingle   = new DateInterval('P1D');
                    break;
                case CoreNotification::SCHED_SHIFT_PERIOD_DAY:
                default:
                    $hours = $wholeDay ? false : ' H:i:s';
                    $dtInterval         = new DateInterval('P' . ((int) $shiftNumber) . 'D');
                    $dtIntervalSingle   = new DateInterval('P1D');
                    break;
            }


            switch ($shiftType) {
                case CoreNotification::SCHEDULE_AFTER:
                    // Sub N periods
                    $dt->sub($dtInterval);
                    $targetTimeTo = $dt->format('Y-m-d' . ($hours ? $hours : ' 23:59:59'));

                    // Sub one more period move back in time
                    if ($shiftPeriod == CoreNotification::SCHED_SHIFT_PERIOD_HOUR || !$wholeDay) $dt->sub($dtIntervalSingle);

                    $targetTimeFrom = $dt->format('Y-m-d' . ($hours ? $hours : ' 00:00:00'));
                    break;

                case CoreNotification::SCHEDULE_BEFORE:
                    // Add N periods
                    $dt->add($dtInterval);
                    $targetTimeFrom = $dt->format('Y-m-d' . ($hours ? $hours : ' 00:00:00'));

                    // Add one more period move forward in time
                    if ($shiftPeriod == CoreNotification::SCHED_SHIFT_PERIOD_HOUR || !$wholeDay) $dt->add($dtIntervalSingle);

                    $targetTimeTo = $dt->format('Y-m-d' . ($hours ? $hours : ' 23:59:59'));
                    break;

                default:
                    $targetTimeFrom = Yii::app()->localtime->getUTCNow('Y-m-d' . ($hours ? $hours : ' 00:00:00'));
                    $targetTimeTo = $targetTimeFrom;
                    break;

            }

            $result = array($targetTimeFrom,$targetTimeTo);

            return $result;


       }



        $x = getTargetPeriodForShifted('after', 365, 'day', false, true);

        var_dump($x);



    }

    /**
     * Test CGridView and ComboGridView with the ArrayDataProvider
     */
    public function actionGridViewArray() {

        $search_input = Yii::app()->request->getParam('search_input', false);

        $rawData=array(
            array('id'=>1, 'username'=>'test 1', 'email'=>'hello1@example.com'),
            array('id'=>2, 'username'=>'test 2', 'email'=>'hello2@example.com'),
            array('id'=>3, 'username'=>'test 3', 'email'=>'hello3@example.com'),
            array('id'=>4, 'username'=>'test 4', 'email'=>'hello4@example.com'),
            array('id'=>5, 'username'=>'test 5', 'email'=>'hello5@example.com'),
            array('id'=>6, 'username'=>'test 6', 'email'=>'hello6@example.com'),
            array('id'=>7, 'username'=>'test 7', 'email'=>'hello7@example.com'),
            array('id'=>8, 'username'=>'test 8', 'email'=>'hello8@example.com'),
            array('id'=>9, 'username'=>'test 9', 'email'=>'hello9@example.com'),
            array('id'=>10, 'username'=>'test 10', 'email'=>'hello10@example.com'),
            array('id'=>11, 'username'=>'test 11', 'email'=>'hello11@example.com'),
            array('id'=>12, 'username'=>'test 12', 'email'=>'hello12@example.com'),
            array('id'=>13, 'username'=>'test 13', 'email'=>'hello13@example.com'),
            array('id'=>14, 'username'=>'test 14', 'email'=>'hello14@example.com'),
            array('id'=>15, 'username'=>'test 15', 'email'=>'hello15@example.com'),
            array('id'=>16, 'username'=>'test 16', 'email'=>'hello16@example.com'),
            array('id'=>17, 'username'=>'test 17', 'email'=>'hello17@example.com'),
            array('id'=>18, 'username'=>'test 18', 'email'=>'hello18@example.com'),
            array('id'=>19, 'username'=>'test 19', 'email'=>'hello19@example.com'),
        );
        // or using: $rawData=User::model()->findAll();

        // do filtering here
        if ($search_input) {
            foreach($rawData as $key => $item) {
                if(!strpos($item['username'], $search_input)) {
                    unset($rawData[$key]);
                }
            }
        }

        $arrayDataProvider=new CArrayDataProvider($rawData, array(
            'id'=>'id',
            /* 'sort'=>array(
                'attributes'=>array(
                    'username', 'email',
                ),
            ), */
            'pagination'=>array(
                'pageSize'=>10,
            ),
        ));

        $params =array(
            'arrayDataProvider'=>$arrayDataProvider,
        );

        if(!isset($_GET['ajax'])) $this->render('grid_view_array', $params);
        else  $this->renderPartial('grid_view_array', $params);
    }


    /**
     *
     */
    public function actionNested()
    {

        // Get all DESCENDANTS of a node (11) (children and grand children)
        $sql = "
        SELECT t.*
        FROM
            core_org_chart_tree AS t,
            core_org_chart_tree AS tparent
        WHERE
            t.iLeft BETWEEN tparent.iLeft AND tparent.iRight
            AND
            tparent.idOrg = 11
        ORDER BY
            t.iLeft;
        ";


        // Get FULL tree, including DEPTH (!!! FULL, you cannot limit by idOrg here!!!)
        $sql = "
        SELECT (COUNT(parent.idOrg) - 1) AS depth, node.*
        FROM
            core_org_chart_tree AS node,
            core_org_chart_tree AS parent
        WHERE
            node.iLeft BETWEEN parent.iLeft AND parent.iRight
        GROUP BY
            node.idOrg
        ORDER BY
            node.iLeft;
        ";


        // List of ALL nodes with their parents
        $sql = "
        SELECT t1.idOrg,
            (SELECT t2.idOrg
             FROM core_org_chart_tree t2
             WHERE t2.iLeft < t1.iLeft AND t2.iRight > t1.iRight
             ORDER BY t2.iRight-t1.iRight ASC
             LIMIT 1) AS idParent


        FROM core_org_chart_tree t1
        ORDER BY t1.iRight - t1.iLeft DESC

        ";


        // Subtree of a node, including DEPTH
        $sql = "

SELECT (COUNT(parent.idOrg) - (sub_tree.depth + 1)) AS depth, node.*
FROM core_org_chart_tree AS node,
        core_org_chart_tree AS parent,
        core_org_chart_tree AS sub_parent,
        (
                SELECT node.idOrg, (COUNT(parent.idOrg) - 1) AS depth
                FROM core_org_chart_tree AS node,
                core_org_chart_tree AS parent
                WHERE node.iLeft BETWEEN parent.iLeft AND parent.iRight
                AND node.idOrg = 11
                GROUP BY node.idOrg
                ORDER BY node.iLeft
        ) AS sub_tree
WHERE
        node.iLeft BETWEEN parent.iLeft AND parent.iRight
        AND node.iLeft BETWEEN sub_parent.iLeft AND sub_parent.iRight
        AND sub_parent.idOrg = sub_tree.idOrg

GROUP BY node.idOrg
ORDER BY node.iLeft;


        ";

        // Get immediate subordinates of a node (11)
        $sql = "

SELECT (COUNT(parent.idOrg) - (sub_tree.depth + 1)) AS depth, node.idOrg
FROM core_org_chart_tree AS node,
        core_org_chart_tree AS parent,
        core_org_chart_tree AS sub_parent,
        (
                SELECT node.idOrg, (COUNT(parent.idOrg) - 1) AS depth
                FROM core_org_chart_tree AS node,
                        core_org_chart_tree AS parent
                WHERE node.iLeft BETWEEN parent.iLeft AND parent.iRight
                        AND node.idOrg = 11
                GROUP BY node.idOrg
                ORDER BY node.iLeft
        )AS sub_tree
WHERE node.iLeft BETWEEN parent.iLeft AND parent.iRight
        AND node.iLeft BETWEEN sub_parent.iLeft AND sub_parent.iRight
        AND sub_parent.idOrg = sub_tree.idOrg
GROUP BY node.idOrg
HAVING depth <= 1
ORDER BY node.iLeft;

        ";



        // Get all LEAFS
        $sql = "
        SELECT *
        FROM
            core_org_chart_tree
        WHERE
            iRight = iLeft + 1;
        ";


        // Path to node (15)
        $sel = "
        SELECT tparent.*
        FROM
           core_org_chart_tree AS t,
           core_org_chart_tree AS tparent
        WHERE
            t.iLeft BETWEEN tparent.iLeft
            AND
            tparent.iRight
            AND
            t.idOrg = 15
        ORDER BY
            t.iLeft;
        ";




        $map = CoreOrgChartTree::getNodeToParentMap();
        var_dump($map);

        die;


        $sql = "START TRANSACTION;\n";
        $values = array();
        foreach ($map as $idOrg => $idParent) {
            if ($idParent != null) {
                $sql .= "UPDATE core_org_chart_tree SET idParent=$idParent WHERE idOrg=$idOrg; \n";
            }
        }
        $sql .= "COMMIT;\n";


        try {
           Yii::app()->db->createCommand($sql)->execute();
        }
        catch (Exception $e) {
           echo "FAIL: " . $e->getMessage() ;
        }






    }

    public function actionAssetURLS()
    {
        Log::_(123);
        $cloudConvertObject = new CloudConvert(CFileStorage::COLLECTION_LO_AUTHORING);
        $cloudConvertObject->slidesConvertFromS3ToS3(11);
    }

    public function actionEndConversion(){
        $req = '{
    "id": "mEq84N29Uv7cxSfVOIuD",
    "url": "//hostiorgjt.cloudconvert.com/process/FQc1Nbolzusm0IXaW5DB",
    "percent": 100,
    "message": "Uploaded 13 files to S3 Bucket",
    "step": "finished",
    "starttime": 1452296186,
    "expire": 1452382615,
    "input": {
        "type": "s3",
        "filename": "11_1.pdf",
        "name": "11_1",
        "ext": "pdf",
        "size": 1251915
    },
    "output": {
        "files": [
            "28_1-0.jpg",
            "28_1-1.jpg",
            "28_1-2.jpg",
            "28_1-3.jpg",
            "28_1-4.jpg",
            "28_1-5.jpg",
            "28_1-6.jpg",
            "28_1-7.jpg",
            "28_1-8.jpg",
            "28_1-9.jpg",
            "28_1-10.jpg",
            "28_1-11.jpg",
            "28_1-12.jpg"
        ],
        "ext": "zip",
        "filename": "11_1.zip",
        "type": "s3"
    },
    "converter": {
        "format": "jpg",
        "type": "imagemagick",
        "options": {
            "resizemode": "maximum",
            "quality": "70",
            "density": "300",
            "disable_alpha": true
        },
        "packformat": "zip"
    },
    "group": "document",
    "endtime": 1452296215,
    "minutes": 1
}';
        $this->redirect(Docebo::createAbsoluteUrl('authoring:main/updateConversion',CJSON::decode($req)));

    }

    public function actionNgCsrf(){
        echo CJSON::encode(array('works' => '1'));
    }

    public function actionBackground(){
        $this->render('background_job');
    }

    public function actionTestNode() {
        $this->render('nodejs_client');
    }
    public function actionStartJob(){

//      if(!PluginManager::isPluginActive('NotificationApp')){
//          Yii::app()->user->setFlash('error', Yii::t('standard', 'We encountered an error while submitting your task. Please, try again later or contact our Helpdesk service'));
//          $this->redirect(Yii::app()->createUrl('/test2/background'));
//      }else{
//          $this->redirect(Yii::app()->createUrl('/test2/background'));
//      }
    }

    public function actionAxGetUnreadBackgroundJobs(){
        $this->renderPartial('_backgroundJobs');
    }

    public function actionVFWSCommands() {
        $cmd = Yii::app()->db->createCommand();

        $params = array();

        $cmd->insert('gamification_rewards_set', array(
            'name' => 'Set 1',
            'description' => 'Aceptance test 1',
            'filter' => 'all'
        ));

        $cmd->insert('gamification_rewards_set', array(
            'name' => 'Catalogue 2',
            'description' => 'Aceptance test 1',
            'filter' => 'all'
        ));


        $cmd->insert('gamification_reward', array(
            'coins' => !empty($params['coins']) ? $params['coins'] : 100,
            'availability' => !empty($params['availability']) ? $params['availability'] : 20,
            'picture' => !empty($params['picture']) ? $params['picture'] : md5('gamification_reward_picture_test') . '.png',
            'association' => !empty($params['association']) ? $params['association'] : 'all',
        ));

        $lastInsertedId = Yii::app()->db->lastInsertID;

        $cmd->insert('gamification_reward_translation', array(
            'id_reward' => $lastInsertedId,
            'lang_code' => !empty($params['lang_code']) ? $params['lang_code'] : 'english',
            'name' => !empty($params['name']) ? $params['name'] : "New Reward {$lastInsertedId}",
            'description' => !empty($params['description']) ? $params['description'] : "New Reward {$lastInsertedId} Description",
        ));

        var_dump(Yii::app()->db->lastInsertID);exit;
    }
//
//    private function getApplicationLogFilePath() {
//
//        $path_ = explode('\\',realpath(dirname(__FILE__)));
//        $path_ = array_reverse($path_);
//        foreach($path_ as $k => $v) {
//            unset($path_[$k]);
//            if($v == 'lms') {
//                break;
//            }
//        }
//        $path_ = implode('\\', array_reverse($path_));
//        $server_name = implode('_', explode('.', $_SERVER['SERVER_NAME']));
//
//        return "{$path_}\\lmslogs\\" . substr($server_name, 0, 1) . "\\" . substr($server_name, 1, 1) . "\\{$server_name}\\application.log";
//    }
//
//    public function actionWatcher() {
//        $rq = Yii::app()->request;
//
//        $application_log_path = $this->getApplicationLogFilePath();
//
//        $lastLogHash = $rq->getParam('ll', md5(''));
//        $currentLog = @file_get_contents($application_log_path);
//        $currentLogHash = md5($currentLog);
//
//        $result = array(
//            'lastLog' => $lastLogHash,
//            'currentLog' => $currentLogHash,
//        );
//
//        if($lastLogHash !== $currentLogHash) {
//            $result['log'] = $currentLog;
//        }
//
//        if($rq->getParam('scava', false) == 'line') {
//            echo CJSON::encode($result);
//        }
//    }
//
//    public function actionLogWatch() {
//
//        $application_log_path = $this->getApplicationLogFilePath();
//
//        $currentLog = @file_get_contents($application_log_path);
//        $currentLogHash = md5($currentLog);
//        if($rq->getParam('scava', false) == 'line') {
//            $this->renderPartial('log_watch', array(
//                'currentLog' => $currentLog,
//                'currentLogHash' => $currentLogHash
//            ));
//        }
//    }
//
//    public function actionReleaseLog() {
//        if($rq->getParam('scava', false) == 'line') {
//            $application_log_path = $this->getApplicationLogFilePath();
//
//            unlink($application_log_path);
//            Yii::log('Application Log Cleared!', 'info', 'Watcher');
//
//            $this->redirect(Docebo::createLmsUrl('test2/Watcher', array('scava'=>'line')));
//            Yii::app()->end();
//        }
//    }

    public function actionPushLO() {
        $objectVersion = LearningRepositoryObjectVersion::model()->findByAttributes(array('id_object' => 26, 'version' => 1));
        if($objectVersion) {
            $objectVersion->pushToCourse(211);
        }
    }

    public function actionViewAllJobs(){
        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/admin.css');
        $this->render('allJobs');
    }

    public function actionTestBackgroundJob(){
        $this->render('test_background_job');
    }

    public function actionDomainAndFiles(){
        $originalDomain = Docebo::getOriginalDomain();//$GLOBALS['cfg']['valid_domains'][0];
        $domainCode = preg_replace('/(\W)/is', '_', $originalDomain);
        $splitDomainCode = $domainCode[0] . "/" . $domainCode[1] . "/" . $domainCode;
        $files  = _base_ . DIRECTORY_SEPARATOR . "lmstmp"   . DIRECTORY_SEPARATOR . $splitDomainCode . DIRECTORY_SEPARATOR . "upload_tmp";
        Log::_($files);
        Log::_( FileHelper::findFiles($files,array('fileTypes'=>array('csv'), 'absolutePaths'=>false)) );
//      $handle=opendir($files);
//      if($handle===false)
//          FileHelper::createDirectory('bg_jobs')
    }

    public function actionTestLocalToCentralLO() {
        $object = LearningRepositoryObject::createFromCourseLearningObject(LearningOrganization::model()->findByPk(59));
    }

    /**
     * Debug/test for building new functionality on additional fields, using the new tables/models
     */
    public function actionUserAdditionalFields()
    {
        $user = CoreUser::model()->find(array(
            'condition' => 'idst = :idst',
            'params' => array(':idst' => \Yii::app()->user->idst)
        ));

        if ($user === null) {
            throw new \Exception('You\'re not authenticated!');
        }

//        # [Test] get additional fields functionality (returns model instances of the fields)
//        var_dump($user->getAdditionalFields());
//        die;

//        # [Test] get visible additional fields for user by branch
//        var_dump($user->getVisibleAdditionalFieldsByBranch());
//        die;

//        # [Test] set user additional fields data (updating the current additional field values, but not saving them)
//        $newUserFieldValues = [
//            3014 => ['value' => 'Test update value for 3014'],
//            3113 => ['value' => 'Test update value for 3113'],
//            3245 => ['value' => 'Test update value for 3245'],
//        ];
//        $user->setAdditionalFieldValues($newUserFieldValues);
//        var_dump($user->getAdditionalFields());
//        die;

//        # [Test] unsetting user additional field value
//        $fieldId = 3014;
//        $user->unsetAdditionalFieldValue($fieldId);
//
//        echo 'Run queries:';
//        echo '<p>SELECT `core_user_field_value`.*
//            FROM `core_user_field_value`
//            WHERE `core_user_field_value`.`id_user` = ' . Yii::app()->user->idst . ';</p>';
//        die;

//        # [Test] saving additional field value on saving user instance
//        $newUserFieldValues = [
//            3014 => ['value' => 'Test update value for 3014 5'],
//            3113 => ['value' => '5555'],
//            3245 => ['value' => '5'],
//        ];
//
//        $user->setAdditionalFieldValues($newUserFieldValues);
//        $user->save();
//        var_dump($user->getAdditionalFields());
//        die;

//        # [Test] Automatic assignment of users to groups via GroupAutoAssignTask::singleUserRun()
//        GroupAutoAssignTask::singleUserRun($user);
//        die;

//        # [Test] Automatic assignment of users to groups via GroupAutoAssignTask::run()
////        var_dump(CoreUser::getAdditionalFieldEntryValuesArray([13010, 13416]));
////        die;
//
//        $scheduler = Yii::app()->scheduler;
//        $params = array(
//            'user_idst' => $user->idst,
//            'offset' => 0
//        );
//
//        $scheduler->createImmediateJob(GroupAutoAssignTask::JOB_NAME, GroupAutoAssignTask::id(), $params);
//        die;

//        # [Test] get additional fields list for filters (returns the list of fields as array data for filter columns / filter items)
//        var_dump($user->getAdditionalFieldFilterColumns());
//        die;

//        # [Test] CoreUser::getTestField() changes to work with the proper additional fields data source
//        $additionalFields = $user->getAdditionalFields();
//        var_dump($additionalFields[0]->getTranslation());
//        die;
//        var_dump($user->getTestField(3014,3014));
//        var_dump($user->getTestField(3377,3377));
//        var_dump($user->getTestField(3014,3014));
//        var_dump($user->getTestField(3047,3047));
//        var_dump($user->getTestField(3080,3080));
//        var_dump($user->getTestField(3113,3113));
//        var_dump($user->getTestField(3146,3146));
//        var_dump($user->getTestField(3179,3179));
//        var_dump($user->getTestField(3212,3212));
//        var_dump($user->getTestField(3245,3245));

//        # [Test] Dropdown field render class compatibility
//        $dropdownField = FieldDropdown::model()->findByAttributes(array('id_field' => 3113));
//        var_dump($dropdownField->renderFieldValue(2249));
//        var_dump($dropdownField->getOptions());
//        var_dump($dropdownField->renderFilterField());
//        var_dump($dropdownField->render());
//        die;

//        # [Test] Codicefiscale field render class compatibility
//        $fiscalCodeField = FieldCodicefiscale::model()->findByAttributes(array('id_field' => 3014));
//
//        var_dump($fiscalCodeField->render());
//        var_dump($fiscalCodeField->renderFilterField());
//        die;

//        # [Test] Country field render class compatibility
//        $countryField = FieldCountry::model()->findByAttributes(array('id_field' => 3047));
//
//        var_dump($countryField->getCountriesList());
//        var_dump($countryField->renderFilterField());
//        var_dump($countryField->renderFieldValue(3047, 13010));
//        var_dump($countryField->render());
//        die;

//        # [Test] Date field render class compatibility
//        $dateField = FieldDate::model()->findByAttributes(array('id_field' => 3080));
//
//        var_dump($dateField->getDateWidth());
//        var_dump($dateField->render());
//        var_dump($dateField->renderFilterField());
//        var_dump($dateField->renderFieldValue('2017-01-31'));
//        die;

//        # [Test] Freetext field render class compatibility
//        $dateField = FieldFreetext::model()->findByAttributes(array('id_field' => 3146));
//
//        var_dump($dateField->render());
//        var_dump($dateField->renderFilterField());
//        die;

//        # [Test] Textfield field render class compatibility
//        $textfieldField = FieldTextfield::model()->findByAttributes(array('id_field' => 3179));
//
////        var_dump($textfieldField);
//        var_dump($textfieldField->render());
//        die;

//        # [Test] Upload field render class compatibility
//        $additionalFields = $user->getAdditionalFields();
//        $uploadField = FieldUpload::model()->findByAttributes(array('id_field' => 3212));
//        $uploadField->userEntry = $additionalFields[6]->userEntry;
//
//        var_dump($uploadField);
//        print_r($uploadField->render());
//        print_r($uploadField->renderFilterField());
//        print_r($uploadField->renderFieldValue($additionalFields[6]->userEntry));
//        die;

//        # [Test] Yesno field render class compatibility
//        $yesnoField = FieldYesno::model()->findByAttributes(array('id_field' => 3245));
//        var_dump($yesnoField);
//        var_dump($yesnoField->render());
//        var_dump($yesnoField->renderFilterField());
//        var_dump($yesnoField->renderFieldValue(2));
//        var_dump($yesnoField->getLabelFromValue(2));
//        die;

//        # [Test] CoreUserFieldValue delete -> deleting files for upload type of fields
//        $valueRow = CoreUserFieldValue::model()->findByAttributes([
//            'id_user' => 13118
//        ]);
//
//        $valueRow->delete();

//        # [Test] CoreUser::validateAdditionalFields()
//        $user->validateAdditionalFields();
//        var_dump($user);
//        die;

//        # [Test] CoreUser::attachAdditionalFields()
//        $fields = [
//            3014 => [
//                'value' => 'Test update value for 3014 6',
//            ],
//        ];
//        $user->attachAdditionalFields($fields);
//        var_dump($user);
//        die;

//        # [Test] CoreUser::processAdditionalFilters()
//        $user->processAdditionalFilters();
//        var_dump($user);
//        die;

//        $this->redirect(Docebo::createLmsUrl('user/additionalFields'));
//
//        # [Test] CoreUser::getVisibleFields()
//        var_dump($user->getVisibleFields());
//        die;

//        # Test getting a dropdown option id by label
//        $id = CoreUserFieldDropdown::getOptionValueByLabel('aaa-bg', 3113, 'bulgarian');
//        var_dump($id);
//        die;

//      $users = CoreUser::model()->findAllByAttributes([
//          'idst' => [19029, 13389, 19028, 19031]
//      ]);
////        foreach ($users as $user) {
////            var_dump($user->getVisibleFields());
////        }
//
//      $idCommontList = [
//            "3014",
//            "3047",
//            "3080",
//            "3113",
//            "3146",
//            "3179",
////            "3212",
//            "3245"
//        ];
//      $fieldValues = CoreUserField::getValuesForUsers([19029, 13389, 19028, 19031], true, $idCommontList);
//      var_dump($fieldValues);
//      die;

        /*
        ?>
        <?= CHtml::beginForm('http://x.boychev.scavaline.com/admin/index.php?r=additionalFields/orderFields', 'post'); ?>
            <input type="text" name="ids[]" value="3014">
            <input type="text" name="ids[]" value="3047">
            <input type="text" name="ids[]" value="3080">
            <input type="text" name="ids[]" value="3113">
            <input type="text" name="ids[]" value="3146">
            <input type="text" name="ids[]" value="3179">
            <input type="text" name="ids[]" value="3212">
            <input type="text" name="ids[]" value="3245">
            <input type="text" name="ids[]" value="3261">
            <button type="submit">GO :)</button>
<!--        </form>-->
        <?= CHtml::endForm(); ?>
        <?php

        die;
         */

//      # Working on Safesforce app support of CoreUserField and related models as additional fields source
//      $command = Yii::app()->db->createCommand("
//            SELECT CONCAT(cuf.type, '|', saf.lmsFieldId) AS lmsFieldname, saf.sfObjectType, saf.sfFieldName
//            FROM ".SalesforceAdditionalFields::model()->tableName()." AS saf
//            INNER JOIN ".CoreUserField::model()->tableName()." AS cuf ON saf.lmsFieldId = cuf.id_field
//            INNER JOIN ".CoreUserFieldTranslations::model()->tableName()." AS cuft ON cuf.id_field = cuft.id_field
//            WHERE saf.lmsAccountId = '' AND saf.sfObjectType = '' AND cuft.translation <> ''
//            GROUP BY cuf.id_field
//        ");
//      var_dump($command->text);
//
//        $record = CoreUserFieldValue::model()->findByAttributes(['id_user' => 13011]);
//        if ($record === null) {
//            $record = new CoreUserFieldValue;
//        }
//        var_dump($record);
//
//        $fields = Yii::app()->db->createCommand("
//            SELECT `cuf`.`id_field`, `cuf`.`type`, `cuft`.`translation`
//            FROM `" . CoreUserField::model()->tableName() . "` `cuf`
//            INNER JOIN `" . CoreUserFieldTranslations::model()->tableName() . "` `cuft` ON `cuf`.`id_field` = `cuft`.`id_field`
//            WHERE `cuft`.`translation` <> '' AND `cuft`.`translation` IS NOT NULL AND `cuf`.`type` IN ('textfield', 'yesno', 'date', 'freetext')
//            GROUP BY id_field
//        ")->queryAll();
//
//        var_dump($fields);
//
//        die;

//        $ids = CoreOrgChartTree::getPuBranchIds();
//        var_dump($ids);die;
//
//        /** @var CoreUser $currentUserModel */
//        $visibleFields = CoreField::model()->getIdsForUserByBranch(array(167));
//        var_dump($visibleFields);
//
//        $currentUserModel = Yii::app()->user->loadUserModel();
//        $visibleFields = $currentUserModel->getVisibleAdditionalFieldsByBranch(array(167));
//        var_dump($visibleFields);
//        die;

//        $certTag = new CertificateTag;
//        $result = $certTag->getUserSubstitution($user->idst);
//        var_dump($result);
//        die;

//        $tool = new UserAdditionalFieldValuesCondition;

//        $data = CoreUserField::model()->getOrgChartVisibilityDataProvider()->getData();
//        var_dump($data);


//        $fields = SimpleSamlFieldsHelper::getLdapFields ();
//        var_dump($fields);

        $fields = [
            3014 => 'Test 3014',
            3047 => 'Bulgaria',
            3080 => '2017-01-31',
            3113 => 'ccc-bg',
            3146 => 'Test entry 3146',
            3179 => 'Test entry 3179',
            3212 => 'Test entry 3212',
            3245 => 1,
        ];

        require_once realpath(__DIR__ . '/../../../api/protected/components/ApiModule.php');
        require_once realpath(__DIR__ . '/../../../api/protected/components/UserApiModule.php');

        $api = new UserApiModule;

//        $fields = $api->readAdditionalFields($fields);
//        var_dump($fields);

//        $api->edit();

//        var_dump($api->fields());

//        var_dump($api->profile());

        var_dump($api->profile_credential());


        die(':)');
    }

    private function showSideBySide(array $results, $dump = true)
    {
        ob_start();
        echo '<table><tr>';
        foreach ($results as $result) {
            echo '<td style="vertical-align: top;">';
            if ($dump === true) {
                var_dump($result);
            } else {
                echo $result;
            }
            echo '</td>';
        }
        echo '</tr></table>';
        echo ob_get_clean();
    }

    public function actionStripeOverlay() {
		$this->layout = '//layouts/erp';
		$this->render("stripe_overlay");
	}

    public function actionFormatterService()
    {
        $locales = ["ar", "ar_dz", "bs", "bg", "hr", "cs", "da", "nl", "nl_be", "en", "en_au", "en_be", "en_ca", "en_hk", "en_za", "en-gb", "fa", "fi", "fr", "fr_ca", "fr_ch", "de", "el", "he", "hu", "id", "it", "it_ch", "ja", "ko", "no", "pl", "pt", "pt-br", "ro", "ru", "zh", "zh_cn", "zh_hans_hk", "zh_hans_sg", "zh_hant", "zh_hant_hk", "zh_hant_mo", "sl", "es", "es_cl", "es_pa", "es_us", "sv", "th", "tr", "uk"];

        $widths = ['full','long','medium','short'];

        $localtime  = Yii::app()->localtime;
        $sampleDate = new DateTime('2017-03-01 10:45:15', new \DateTimeZone('UTC'));
        $yiiFormatter = new CDateFormatter;

        ?>
            <style>
                body { font-family: "Ubuntu", "Tahoma", "Arial", sans-serif; font-size: 12px; }
                table { border-collapse: collapse; width: 100%; font-size: 12px; }
                td, th { border: 1px solid #ddd; padding: 8px 12px; text-align: left; vertical-align: top; }
                th { background-color: #e9e9e9; }

                .example-date { color: #000; }
                .format { color: #555; }

                th.cldr,
                td.cldr {
                    background-color: #fff3d0;
                }
                th.cldr.date,
                td.cldr.date {
                    background-color: rgba(255, 243, 208, 0.7);
                }
                th.cldr.time,
                td.cldr.time {
                    background-color: rgba(255, 243, 208, 0.4);
                }

                th.php,
                td.php {
                    background-color: #dcffd4;
                }
                th.php.date,
                td.php.date {
                    background-color: rgba(220, 255, 212, 0.7);
                }
                th.php.time,
                td.php.time {
                    background-color: rgba(220, 255, 212, 0.4);
                }

                th.moment-js,
                td.moment-js {
                    background-color: #dffcff;
                }
                th.moment-js.date,
                td.moment-js.date {
                    background-color: rgba(223, 252, 255, 0.7);
                }
                th.moment-js.time,
                td.moment-js.time {
                    background-color: rgba(223, 252, 255, 0.4);
                }
            </style>
            <table data-timezone="UTC">
                <thead>
                    <tr>
                        <th rowspan="2">Language (locale)</th>
                        <th rowspan="2">Format width</th>
                        <th colspan="3">CLDR</th>
                        <th colspan="3">PHP</th>
                        <th colspan="3">Moment.js</th>
                    </tr>
                    <tr>
                        <th class="cldr format date-time">Datetime:</th>
                        <th class="cldr format date"">Date:</th>
                        <th class="cldr format time"">Time:</th>
                        <th class="php format date-time">Datetime:</th>
                        <th class="php format date"">Date:</th>
                        <th class="php format time"">Time:</th>
                        <th class="moment-js format datetime">Datetime:</th>
                        <th class="moment-js format date">Date:</th>
                        <th class="moment-js format time">Time:</th>
                        </th>
                    </tr>
                </thead>
                <tbody>
        <?php

        foreach ($locales as $locale) {
            $localeObj = Yii::app()->getLocale($locale);
            $first = true;
            $cldrDateTimeFormat =
            $phpDateTimeFormat =
            $momentJsDateTimeFormat = trim($localeObj->getDateTimeFormat());

            foreach ($widths as $width) {
                // Collecting CLDR formats for date/time of the language
                $cldrDateFormat     = trim($localeObj->getDateFormat($width));
                $cldrTimeFormat     = trim($localeObj->getTimeFormat($width));

                // Converting CLDR formats to PHP formats
                $phpDateFormat     = trim($localtime->YiitoPHPDateFormat($cldrDateFormat));
                $phpTimeFormat     = trim($localtime->YiitoPHPDateFormat($cldrTimeFormat));

                // Converting PHP formats to moment.js formats
                $momentJsDateFormat     = trim($this->convertPHPToMomentFormat($phpDateFormat));
                $momentJsTimeFormat     = trim($this->convertPHPToMomentFormat($phpTimeFormat));

                ?>
                    <tr>
                        <?php if ($first) : ?><td rowspan="8"><?= $locale ?></td><?php endif; ?>
                        <td rowspan="2"><?= $width ?></td>
                        <td class="cldr format date-time"><?= $cldrDateTimeFormat ?></td>
                        <td class="cldr format date"><?= $cldrDateFormat ?></td>
                        <td class="cldr format time"><?= $cldrTimeFormat ?></td>
                        <td class="php format date-time"><?= $phpDateTimeFormat ?></td>
                        <td class="php format date"><?= $phpDateFormat ?></td>
                        <td class="php format time"><?= $phpTimeFormat ?></td>
                        <td class="moment-js format datetime"><?= $momentJsDateTimeFormat ?></td>
                        <td class="moment-js format date"><?= $momentJsDateFormat ?></td>
                        <td class="moment-js format time"><?= $momentJsTimeFormat ?></td>
                    </tr>
                    <tr>
                        <td class="cldr example-date date-time"><?php echo $yiiFormatter->format($cldrDateTimeFormat, $sampleDate->getTimestamp()) ?></td>
                        <td class="cldr example-date date"><?php /*echo $yiiFormatter->format($cldrDateFormat, $sampleDate->getTimestamp())*/ ?></td>
                        <td class="cldr example-date time"><?php /*echo $yiiFormatter->format($cldrTimeFormat, $sampleDate->getTimestamp())*/ ?></td>
                        <td class="php example-date date-time"><?php /* echo $phpDateTimeFormat */ ?></td>
                        <td class="php example-date date"><?= $sampleDate->format($phpDateFormat) ?></td>
                        <td class="php example-date time"><?= $sampleDate->format($phpTimeFormat) ?></td>
                        <td class="moment-js example-date datetime"><?php /* echo $momentJsDateTimeFormat */ ?></td>
                        <td class="moment-js example-date date"><?php /* echo $momentJsDateFormat */ ?></td>
                        <td class="moment-js example-date time"><?php /* echo $momentJsTimeFormat */ ?></td>
                    </tr>
                <?php
                $first = false;
            }
        }

        ?>
                </tbody>
            </table>
        <?php
    }

    public function convertPHPToMomentFormat($format)
    {
        $replacements = [
            'd' => 'DD',
            'D' => 'ddd',
            'j' => 'D',
            'l' => 'dddd',
            'N' => 'E',
            'S' => 'o',
            'w' => 'e',
            'z' => 'DDD',
            'W' => 'W',
            'F' => 'MMMM',
            'm' => 'MM',
            'M' => 'MMM',
            'n' => 'M',
            't' => '', // no equivalent
            'L' => '', // no equivalent
            'o' => 'YYYY',
            'Y' => 'YYYY',
            'y' => 'YY',
            'a' => 'a',
            'A' => 'A',
            'B' => '', // no equivalent
            'g' => 'h',
            'G' => 'H',
            'h' => 'hh',
            'H' => 'HH',
            'i' => 'mm',
            's' => 'ss',
            'u' => 'SSS',
            'e' => 'zz', // deprecated since version 1.6.0 of moment.js
            'I' => '', // no equivalent
            'O' => '', // no equivalent
            'P' => '', // no equivalent
            'T' => '', // no equivalent
            'Z' => '', // no equivalent
            'c' => '', // no equivalent
            'r' => '', // no equivalent
            'U' => 'X',
        ];
        $momentFormat = strtr($format, $replacements);
        return $momentFormat;
    }

}



