<?php defined("IN_DOCEBO") or die('Direct access is forbidden.');
/* ======================================================================== \
| 	DOCEBO - The E-Learning Suite											|
| 																			|
| 	Copyright (c) 2012 (Docebo)												|
| 	http://www.docebo.com													|
\ ======================================================================== */

/**
 * This class receives API call from ERP with installation based auth
 */

class AppsMpApiController extends CController {


	private $_output;


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res =array();

		if (!$this->checkKeys($_POST)) {
			$this->_output['success']=false;
			$this->_output['msg']='auth failed';
			// $this->_output['debug']=var_export($_POST, true);
			$this->returnOutput();
		}

		return $res;
	}


	public function beforeAction($action) {
	    
	    if (!parent::beforeAction($action)) {
	        return false;
	    }
	    
        IPHelper::checkForIPwhitelist();
	    
	    // !! Always
	    return true;
	     
	}
	
	/**
	 * Check if the request is valid
	 * @param array $params the parameters recived by the api
	 * @return boolean
	 */
	public function checkKeys($params) {
		// TODO: better way to read this.. ?
		// note: this is read from the *specific* config file of each install, not from the main config
		$erp_key = $GLOBALS['cfg']['erp_key'];
		$erp_secret_key = $GLOBALS['cfg']['erp_secret_key'];

		// retive the hash recived with the api call
		if(!isset($_SERVER['HTTP_X_AUTHORIZATION'])) return false;

		// calculate the same hash locally
		$auth =  base64_decode(str_replace('Docebo ', '', $_SERVER['HTTP_X_AUTHORIZATION']) );
		$hash_sha1 = sha1( implode(',', $params). ',' . $erp_secret_key );

		// check if the two hashes match each other
		if($auth != $erp_key.':'.$hash_sha1) return false;
		else return true;
	}


	public function returnOutput() {
	    header('Content-Type: application/json');
		echo CJSON::encode($this->_output);
		Yii::app()->end();
	}


	protected function getServiceParams() {
		return $_POST;
	}


	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	public function actionIndex() {
		// nothing here
	}


	// --------------------------------------------------------------------------

	public function actionSetShopifySettings()
	{
		$params = $this->getServiceParams();
		Settings::save('shopify_subdomain', str_replace('.myshopify.com', '', $params['shopify_domain']) );
		Settings::save('shopify_authentication_token', $params['shopify_token']);

//		ShopifyRegistrationProxy::afterTokenReceivedActions();
		ShopifyCustomerProxy::registerShopifyHooks();
		$this->_output['success']=true;
		$this->returnOutput();

	}
	/**
	 * Activate the specified app for the current platform
	 * params['installation_id']
	 * params['internal_codename']
	 */
	public function actionActivateApp() {
		$res = true;
		$params = $this->getServiceParams();

		$res = PluginManager::activateAppByCodename($params['internal_codename']);

		/** Raising an event to allow plugins to perform specific events when being activated during LMS trial setup */
		if (isset($params['is_from_installer']) && $params['is_from_installer']){
			$event = new DEvent($this, array('plugin' => $params['internal_codename']));
			Yii::app()->event->raise(EventManager::EVENT_INSTALL_PLUGIN_FROM_INSTALLER, $event);
		}

		//		$this->_output['msg'] = ''; // error message if $res == false
		$this->_output['success']=$res;
		$this->returnOutput();
	}


	/**
	 * Deactivate the specified app for the current platform
	 * params['installation_id']
	 * params['internal_codename']
	 */
	public function actionDeactivateApp() {
		$res = true;
		$params = $this->getServiceParams();

		$res = PluginManager::deactivateAppByCodename($params['internal_codename']);

		//		$this->_output['msg'] = ''; // error message if $res == false
		$this->_output['success']=$res;
		$this->returnOutput();
	}

	public function actionGetMPCoursesList()
	{
		$res = true;

		$models = LearningCourse::model()->findAll(array(
			'condition' => 'mpidCourse != 0',
			'order' => 'name ASC',
		));

		$this->_output['success'] = $res;
		$this->_output['data'] = $models;
		$this->returnOutput();
	}

	public function actionLastAccessStats()
	{
		$res = true;
		$models = CoreUser::model()->findAll(array(
			'condition' => 'lastenter IS NOT NULL AND lastenter != "0000-00-00 00:00:00" AND lastenter > "'.date('Y-m-d H:i:s', strtotime('-365 day')).'"',
			'order' => 'lastenter DESC',
		));

		$this->_output['success'] = $res;
		$this->_output['data'] = $models;
		$this->returnOutput();
	}

	public function actionGetActiveUsers()
	{
		$params = $this->getServiceParams();
		$res = array();

		if(!empty(Settings::get('subscription_begin_date'))) {
			$params['monthDay'] = strtotime(Settings::get('subscription_begin_date'));
			$params['monthDay'] = date('d', $params['monthDay']);
		}

		for ($i = 11; $i >= 0; $i--)
		{
			$date = date('Y-m-1', strtotime('-'.$i.' month'));
			$to = date('Y-m', strtotime($date)).'-'.($params['monthDay'] - 1) . ' 23:59:59';
			$from =  date('Y-m', (strtotime($date)-864000)).'-'. $params['monthDay'] . ' 00:00:00';
			$res[$date] = LearningUserlimitLoginLog::activeUsersForPeriod($from, $to);
			$res_from[$date] = $from;
			$res_to[$date] = $to;
		}

		$this->_output['success'] = true;
		$this->_output['data'] = $res;
		$this->_output['from'] = $res_from;
		$this->_output['to'] = $res_to;
		$this->_output['starting_day'] = $params['monthDay'];
		$this->returnOutput();
	}

	public function actionSetExpiringFlag()
	{
		$params = $this->getServiceParams();
		if (isset($params['expiring_notification']))
		{
			Settings::save('expiring_notification', $params['expiring_notification']);
			$this->_output['success'] = true;
		}
		else
		{
			$this->_output['success'] = true;
			$this->_output['data'] = 'Param "expiring_notification" is not set';
		}

		$this->returnOutput();
	}

	public function actionSetExpiringFlagAnswer()
	{
		$params = $this->getServiceParams();
		if (isset($params['expiring_notification_show']))
		{
			Settings::save('expiring_notification_show', $params['expiring_notification_show']);
			$this->_output['success'] = true;
		}
		else
		{
			$this->_output['success'] = true;
			$this->_output['data'] = 'Param "expiring_notification_show" is not set';
		}

		$this->returnOutput();
	}

	public function actionSetDemoPlatformFlag()
	{
		$params = $this->getServiceParams();
		if (isset($params['demo_platform']))
		{
			Settings::save('demo_platform', $params['demo_platform']);
			$this->_output['success'] = true;
		}
		else
		{
			$this->_output['success'] = true;
			$this->_output['data'] = 'Param "demo_platform" is not set';
		}

		$this->returnOutput();
	}

}
