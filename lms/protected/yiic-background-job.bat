@echo off

rem ------------------------------------------------------------------------------------------------------------------------
rem  Yii command starter.
rem  Differs from the default (yiic) in that it defines an
rem  environment variable equal to the FIRST script parameter that is
rem  used by the invoked PHP script to load per-domain configuration.
rem  Final result is: we can run Yii commands based on domain name and configuration.
rem
rem  Example call:
rem  	yiic-background-job      mylms.domain.com  job 1234qwer
rem     yiic-background-job.sh   mylms.domain.com  1234qwer    (UNIX shell)
rem     yiic-background-job.bat  mylms.domain.com  1234qwer    (Windows only, note the DOUBLE quotes)
rem ------------------------------------------------------------------------------------------------------------------------

rem @setlocal

rem Set/export the following environment variable to the domain name (first parameter)
set LMS_DOMAIN=%1

set BIN_PATH=%~dp0

set HASH_ID=%2

if "%PHP_COMMAND%" == "" set PHP_COMMAND=php.exe
goto again

set i=0
rem Use the rest parameters to make the call to PHP script that will actually run the command (up to %9)
:again
    echo %time%
    set /A i = %i% + 1
    echo Going to next iteration: %i%
    "%PHP_COMMAND%" "%BIN_PATH%yiic-domain.php" "job" "runBackground" "--hash_id=%HASH_ID%" %3 %4 %5 %6 %7
    if %ERRORLEVEL% EQU 0 (
        echo Stop by the job by iteration %i%
        goto end
     )

     if %ERRORLEVEL% EQU 2 (
            set /A i=0
            echo Rerunning the job
          )
    rem If the current iteration is more than 10 000 - stop the job
    if %i% GEQ 10000 (
        echo Stop by max iteration
        "%PHP_COMMAND%" "%BIN_PATH%yiic-domain.php" "job" "stopBackground" "--hash_id=%HASH_ID%" %3 %4 %5 %6 %7
        goto end
     )
    goto again

:end
set /A i=0
echo %time%

rem @endlocal