<?php

/**
 * Class RegisterHelper
 * Collect various functions and features used in user registration process. This is mostly for code readability and
 * easier maintainability.
 */
class RegisterHelper extends CComponent {


	/**
	 * This method generates a new idst in the table core_st table.
	 */
	private function createSt() {
		$coreStModel = new CoreSt();
		$coreStModel->save();
		return $coreStModel->primaryKey; // Returns the last id
	}




	/**
	 * Collect all register options from LMS settings
	 * @return array the list of register options
	 */
	public function getRegisterOptions() {
		$options = array(
			'lastfirst_mandatory'   => Settings::get('lastfirst_mandatory'),
			'register_type' 		=> Settings::get('register_type'),
			'pass_alfanumeric' 		=> Settings::get('pass_alfanumeric'),
			'pass_not_username'     => Settings::get('pass_not_username'),
			'pass_min_char' 		=> Settings::get('pass_min_char'),
			'hour_request_limit' 	=> Settings::get('hour_request_limit'),
			'privacy_policy' 		=> Settings::get('privacy_policy'),
			'mail_sender'			=> Settings::get('mail_sender'),
			'field_tree'			=> Settings::get('field_tree'),
			'pass_dictionary_check' => Settings::get('pass_dictionary_check')
		);
		return $options;
	}


	/**
	 * Read parameters from request input. Some of them may need formatting or specific handling and this function takes
	 * care of those cases.
	 * @param $name string the input parameter name
	 * @return mixed the input param value
	 */
	public function readParameterFromInputForm($name) {
		switch ($name) {
			case 'userid':
			case 'firstname':
			case 'lastname':
			case 'email':
				$output = trim(Yii::app()->request->getParam($name, ''));
				break;
			default:
				$output = Yii::app()->request->getParam($name, NULL);
				break;
		}
		return $output;
	}



	/**
	 * This method validates user fields submitted in the registration form.
	 * Additional fields are validate in a separate function.
	 * @param $data bool|array optional manually provided input parameters values
	 * @return array('success'=>boolean,'message' =>string)
	 */
	public function validateUserInfo($data = false) {

		$options = $this->getRegisterOptions();

		//preapre output variable
		$result = array(
			'success'=>false,
			'message' =>''
		);

		//read fields from form input
		$username = trim( !empty($data) && isset($data['username']) ? $data['username'] : trim(strip_tags(Yii::app()->request->getParam('username'))) );
		$email = trim( !empty($data) && isset($data['email']) ? $data['email'] : trim(Yii::app()->request->getParam('email')) );
		$firstname = !empty($data) && isset($data['firstname']) ? $data['firstname'] : trim(Yii::app()->request->getParam('firstname'));
		$lastname = !empty($data) && isset($data['lastname']) ? $data['lastname'] : trim(Yii::app()->request->getParam('lastname'));
		$password = !empty($data) && isset($data['password']) ? $data['password'] : Yii::app()->request->getParam('password');
		$password_retype = !empty($data) && isset($data['password_retype']) ? $data['password_retype'] : Yii::app()->request->getParam('password_retype');
		$reg_code = !empty($data) && isset($data['reg_code']) ? $data['reg_code'] : Yii::app()->request->getParam('reg_code', -1);

        $event = new DEvent($this, array(
            'username' => &$username,
            'email' => &$email,
            'firstname' => &$firstname,
            'lastname' => &$lastname,
            'password' => &$password,
            'password_retype' => &$password_retype,
            'reg_code' => &$reg_code,
        ));
        // Tell to the plugins that we are about to validate user registration entry data
        Yii::app()->event->raise('onBeforeValidateRegistrationFields', $event);
        if(!$event->shouldPerformAsDefault()){
            $result['message'] = $event->return_value;
            return $result;
        }

		//--- fields validation ---

		// Empty Username
		if (empty($username)) {
			$result['message'] = Yii::t('register','_ERR_INVALID_USER');
			return $result;
		}

		// Checking duplicate userid in core_user
		$criteria = new CDbCriteria();
		$criteria->addCondition("userid = :userid");
		$criteria->params[':userid'] = '/'.$username;
		$count = CoreUser::model()->count($criteria);
		if ($count) {
			//$result['message'] = Yii::t('register','_ERR_INVALID_USER');
			$result['message'] = Yii::t('register','_ERR_DUPLICATE_USER');
			return $result;
		}
		// ... and in core_user_temp too
		$criteria = new CDbCriteria();
		$criteria->addCondition("userid = :userid");
		$criteria->params[':userid'] = $username; //NOTE: core_user_temp table do not use the '/' at the beginning of the userid field
		$count = CoreUserTemp::model()->count($criteria);
		//$userModel = CoreUserTemp::model()->countByAttributes(array('userid' => $username));
		if ($count > 0) {
			//$result['message'] = Yii::t('register','_ERR_INVALID_USER');
			$result['message'] = Yii::t('register','_ERR_DUPLICATE_USER');
			return $result;
		}

		// Checking if the email is valid -
		$regex = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';
		if (!preg_match($regex, $email)) {
			$result['message'] = Yii::t('register','_ERR_INVALID_MAIL');
			return $result;
		}

		// Check if the email is from the allowed Domains
		$validEmail = $this->filterEmailDomain($email);
		if($validEmail === false){
			$result['message'] = Yii::t('register', "You'r email address is not allowed for self-registration. Please, contact the platform administrator for further information.");
			return $result;
		}

		// Checking duplicate email
		$criteria = new CDbCriteria();
		$criteria->addCondition("email LIKE :email");
		$criteria->params[':email'] = $email;
		$count1 = CoreUser::model()->count($criteria);
		$count2 = CoreUserTemp::model()->count($criteria);
		if ($count1 > 0 || $count2 > 0) {
			$result['message'] = Yii::t('register','_ERR_DUPLICATE_MAIL');
			return $result;
		}

		// Checking if firstname and lastname are mandatory
		if ($options['lastfirst_mandatory'] == 'on') {
			if (empty($firstname) || empty($lastname)) {
				$result['message'] = Yii::t('register','_SOME_MANDATORY_EMPTY');
				return $result;
			}
		}

//        // Check for 3 or more repeating characters
//        if (preg_match('/([\w\W])\1{2,}/', $password)) {
//            $result['message'] = Yii::t('register','Password cannot contain 3 or more repeating characters!');
//            return $result;
//        }
//        // Check for 3 or more sequential numbers
//        if (preg_match('/(012|123|234|345|456|567|678|789|987|876|765|654|543|432|321|210)+/', strtolower($password))) {
//            $result['message'] = Yii::t('register','Password cannot contain 3 or more sequential numbers!');
//            return $result;
//        }
//        // Check for 4 or more alphabetically sequential characters
//        if (preg_match('/((abcd|bcde|cdef|defg|efgh|fghi|ghij|hijk|ijkl|jklm|klmn|lmno|mnop|nopq|opqr|pqrs|qrst|rstu|stuv|tuvw|uvwx|vwxy|wxyz)|(zyxw|yxwv|xwvu|wvut|vuts|utsr|tsrq|srqp|rqpo|qpon|ponm|onml|nmlk|mlkj|lkji|kjih|jihg|ihgf|hgfe|gfed|fedc|edcb|dcba))+/', strtolower($password))) {
//            $result['message'] = Yii::t('register','Password cannot contain 4 or more alphabetically sequential characters!');
//            return $result;
//        }
//        // Check for 4 or more US-keyboard-sequential characters
//        if (preg_match('/((qwer|wert|erty|rtyu|tyui|yuio|uiop|asdf|sdfg|dfgh|fghj|ghjk|hjkl|zxcv|xcvb|cvbn|vbnm)|(mnbv|nbvc|bvcx|vcxz|lkjh|kjhg|jhgf|hgfd|gfds|fdsa|poiu|oiuy|iuyt|uytr|ytre|trew|rewq))+/', strtolower($password))) {
//            $result['message'] = Yii::t('register','Password cannot contain 4 or more US-keyboard-sequential characters!');
//            return $result;
//        }
//		// The password must contain at least C chars
//		if (strlen($password) < Settings::get('pass_min_char', 0)) {
//			$result['message'] = Yii::t('register','_PASSWORD_TOO_SHORT');
//			return $result;
//		}
//
//		// Checking if password must be alfanumeric
//		if ($options['pass_alfanumeric'] == 'on' ) {
//			if (!preg_match('/[a-z]/i', $password) || !preg_match('/[0-9]/', $password)) {
//				$result['message'] = Yii::t('register','_ERR_PASSWORD_MUSTBE_ALPHA');
//				return $result;
//			}
//		}
//
//		// password should be different from username
//		if ($options['pass_not_username'] == 'on' && strtolower($password) == strtolower($username)) {
//			$result['message'] = Yii::t('configuration','_PASS_NOT_USERNAME');
//			return $result;
//		}
//
//		// The two passwords must be equal.
//		if ($password != $password_retype) {
//			$result['message'] = Yii::t('register','_ERR_PASSWORD_NO_MATCH');
//			return $result;
//		}
//
//        // The password must not be a dictionary word
//        if ( $options['pass_dictionary_check'] == 'on' ) {
//            $base = Yii::app()->getBasePath();
//
//            $arrayOfWords = file($base . DIRECTORY_SEPARATOR . 'words.txt');
//            $arrayOfWords = array_map('trim', $arrayOfWords);
//            $flippedArray = array_flip($arrayOfWords);
//
//            if ( isset($flippedArray[$password]) ) {
//                $result['message'] = Yii::t('register', 'Password cannot contain common dictionary words and most common passwords');
//                return $result;
//            }
//        }


        //Checking for password validity(all checks are inc;uded, if the respective options are active)
        $resultPassCheck = Yii::app()->security->checkPasswordStrength($password, $password_retype, $username);
        if(!$resultPassCheck['success'] && $resultPassCheck['message']!='')
        {
            $result['message'] = $resultPassCheck['message'];
			return $result;
		}

		// Checking if org chart is mandatory and none selected
		$regCodeMandatory = Settings::get('mandatory_code')=='on' && (Settings::get('registration_code_type')=='tree_man' || Settings::get('registration_code_type')=='tree_drop' || Settings::get('registration_code_type')=='tree_course');
		if ($regCodeMandatory){

			$found = false;
			if ($reg_code !== false && $reg_code !== '') {

				$criteria = new CDbCriteria();
				if (Settings::get('registration_code_type')=='tree_man') {
					$criteria->addCondition('code = :reg_code');
					$criteria->params[':reg_code'] = $reg_code;
				} elseif (Settings::get('registration_code_type')=='tree_drop') {
					$criteria->addCondition('idOrg = :idOrg');
					$criteria->params[':idOrg'] = $reg_code;
				}elseif(Settings::get('registration_code_type')=='tree_course'){
					$criteria->addCondition('code = :code');
					$criteria->params[':code'] = substr($reg_code, 0, 10);
				}

				if(PluginManager::isPluginActive("MultidomainApp") && ($multidomainClient = CoreMultidomain::resolveClient())) {
					/* @var $multidomainClient CoreMultidomain */
					if ($multidomainClient->orgChart) {
						$criteria->addCondition("iLeft >= :iLeft AND iRight <= :iRight");
						$criteria->params[':iLeft'] = $multidomainClient->orgChart->iLeft;
						$criteria->params[':iRight'] = $multidomainClient->orgChart->iRight;
			}
				}

				$found = CoreOrgChartTree::model()->count($criteria);
			}

			// Org chart code IS mandatory, but the user hasn't selected one
			// or the code he entered doesn't exist as a org chart
			if(!$found){
				$result['message'] = Yii::t('register','_INVALID_CODE');
				return $result;
			}
		}


		// privacy policy accepted?
		if ($options['privacy_policy'] == 'on') {
			if (Yii::app()->request->getParam('privacy') != 'on') {
				$result['message'] = Yii::t('register','_ERR_POLICY_NOT_CHECKED');
				return $result;
			}
		}

        // Checking whether or not the admin sender email is configured
        $fromAdminEmail = Settings::get('mail_sender');
        if($fromAdminEmail == null) {
            Yii::log("Sender email not configured in administrator panel", 'error', __CLASS__);
            $result['message'] = Yii::t('notification', 'Badly configured sender email') . '.' . ' ' . Yii::t('course', 'Please contact your administrator') . '.';
            return $result;
        }

		$result['success'] = true;
		return $result;
	}





	/**
	 * Check if mandatory fields have been filled.
	 *
	 * @param bool|array $fieldsFilter - an optional filter on fields to be checked
	 * @return bool
	 */
	public function validateAdditionalFields($fieldsFilter = false)
    {

		// Get the default groups' IDs
		$arr_idst = array();
		$tmp = CoreGroup::getGroup( false, '/oc_0' );
		$arr_idst[] = $tmp->idst;
		$tmp = CoreGroup::getGroup( false, '/ocd_0' );
		$arr_idst[] = $tmp->idst;

        $criteria = new CDbCriteria;
        $criteria->addCondition('t.invisible_to_user = 0');
		$criteria->group = 't.id_field';
		$criteria->index = 'id_field';

        // Get all fields for these groups in the current language
        $fields = CoreUserField::model()->language(Yii::app()->session['current_lang'], true)->findAll($criteria);

		if (!$fields) return true;

		$success = true;

		foreach ($fields as $field) {
            /** @var CoreUserField $field */
			$id_common  = $field->getFieldId();
			$type_field = $field->getFieldType();

			//we may not want to check all fields, so provide the ability to check only some fields by a filter
			if (is_array($fieldsFilter) && !in_array($id_common, $fieldsFilter)) {
				//ignore fields not in the filter
				continue;
			}

			if ($field->mandatory) {
				// This field is mandatory. Check if it's been filled (based on the type of the field
				// the process may be a bit different, e.g. on file upload type fields))
				switch($type_field){
					case CoreUserField::TYPE_UPLOAD:
						if (!isset($_POST['FileUpload'][$id_common]) || empty($_POST['FileUpload'][$id_common])) { $success = false; }
						break;
					case CoreUserField::TYPE_TEXT:
					case 'freetext':
							if(strlen(trim($_POST['field_'.$type_field][$id_common])) <= 0){
								$success = false;
							}
						break;
					default:
						$additionalFieldValue = isset($_POST['field_'.$type_field][$id_common]) && !empty($_POST['field_'.$type_field][$id_common])
							? trim($_POST['field_'.$type_field][$id_common])
							: false;
						if (!$additionalFieldValue || empty($additionalFieldValue)) { $success = false; }
						break;
				}
			}
		}

		return (bool) $success;
	}



	/**
	 * Check and process any filled additional fields.
	 * Also checks if mandatory ones are filled.
	 *
	 * @param int $userId - The ID of the user that has just been registered
	 * (may be even a user, that is not admin approved yet)
	 * @param bool|array $fieldsFilter optional filter on fields
	 *
	 * @return bool
	 */
	private function processAdditionalFields($userId, $fieldsFilter = false){

		if (is_array($fieldsFilter) && count($fieldsFilter) == 0) {
			//this means that no fields have to be processed, so nothig has to be done here
			return true;
		}

		// Get the default groups' IDs
		$arr_idst = array();
		$tmp = CoreGroup::getGroup( false, '/oc_0' );
		$arr_idst[] = $tmp->idst;
		$tmp = CoreGroup::getGroup( false, '/ocd_0' );
		$arr_idst[] = $tmp->idst;

		$criteria = new CDbCriteria();
		$criteria->addCondition('t.invisible_to_user = 0');
		$criteria->index = 'id_field';

		// Get all fields for these groups in the current language
		$fields = CoreUserField::model()->language(Yii::app()->session['current_lang'], true)->findAll($criteria);

		if (!$fields) return true;

		$success = true; //prepare output variable

		foreach ($fields as $field) {
			$id_common = $field->getFieldId();
			$type_field = $field->getFieldType();

            if (is_array($fieldsFilter) && !in_array($id_common, $fieldsFilter)) {
				//ignore fields not in the filter
				continue;
			}

			if ($field->mandatory) {
				// This field is mandatory. Check if it's been filled (based on the type of the field
				// the process may be a bit different, e.g. on file upload type fields))
				switch($type_field){
					case CoreUserField::TYPE_UPLOAD:
						if(!isset($_POST['FileUpload'][$id_common]) || empty($_POST['FileUpload'][$id_common])){
							$success = false;
						} else {
							// Store the uploaded file to storage
							Field::store($id_common, $userId, $_POST['FileUpload'][$id_common], true);
						}
						break;
					case CoreUserField::TYPE_TEXT:
					case 'freetext':
						$additionalFieldValue = (isset($_POST['field_'.$type_field][$id_common]) || strlen($_POST['field_'.$type_field][$id_common]) > 0) ? trim($_POST['field_'.$type_field][$id_common]) : false;

						Field::store($id_common, $userId, $additionalFieldValue, true);
						break;
					default:
						$additionalFieldValue = isset($_POST['field_'.$type_field][$id_common]) && !empty($_POST['field_'.$type_field][$id_common]) ? trim($_POST['field_'.$type_field][$id_common]) : false;

						// The additional field's value is not provided by the user
						// This will cause the registration form to be reloaded with an error message
						if ($additionalFieldValue){
							if ($type_field == CoreUserField::TYPE_DATE) {
								//set date width
								if (!Yii::app()->localtime->forceLongYears()) {
									//trying to prevent short year format to be used in input fields
									switch (LocalTime::DEFAULT_DATE_WIDTH) {
										case LocalTime::LONG: $dateWidth = LocalTime::LONG; break;
										default: $dateWidth = LocalTime::MEDIUM; break;
									}
								} else {
									//long years are forced, all date widths are safe
									$dateWidth = LocalTime::DEFAULT_DATE_WIDTH;
								}
								$additionalFieldValue = Yii::app()->localtime->fromLocalDate($additionalFieldValue, $dateWidth);
							}
							Field::store($id_common, $userId, $additionalFieldValue, true);
						} else {
							$success = false;
						}
				}
			} else {
				// Field is not mandatory, so we don't care if its value is
				// provided or not. Just store that value if it's set and
				// return successfull response from function

				$additionalFieldValue = false;
				switch($type_field){
					case CoreUserField::TYPE_UPLOAD:
						$additionalFieldValue = isset($_POST['FileUpload'][$id_common])  ?  $_POST['FileUpload'][$id_common] :  false;
						break;
					default:
						// For all other additional field types
						// we get the user's input value directly from $_POST
						// and save it in the database
						$additionalFieldValue = isset($_POST['field_'.$type_field][$id_common]) ? trim($_POST['field_'.$type_field][$id_common]) : false;
						if (!empty($additionalFieldValue) && $type_field == CoreUserField::TYPE_DATE) {
							//set date width
							if (!Yii::app()->localtime->forceLongYears()) {
								//trying to prevent short year format to be used in input fields
								switch (LocalTime::DEFAULT_DATE_WIDTH) {
									case LocalTime::LONG: $dateWidth = LocalTime::LONG; break;
									default: $dateWidth = LocalTime::MEDIUM; break;
								}
							} else {
								//long years are forced, all date widths are safe
								$dateWidth = LocalTime::DEFAULT_DATE_WIDTH;
							}
							$additionalFieldValue = Yii::app()->localtime->fromLocalDate($additionalFieldValue, $dateWidth);
						}
				}
				if ($additionalFieldValue) {
					Field::store($id_common, $userId, $additionalFieldValue, true);
				}
			}
		}

		if (!$success){
			// Something went wrong. Most likely a mandatory field was not filled
			// so the user registration will fail anyway after this point.
			// In this case, delete all fields for the newly created user ID,
			// because this ID will not be present anywhere (neither in CoreUser, nor CoreUserTemp)
			// Use AR so that model to call its own afterDelete() which will clean Uploaded files, if any
            $userFieldEntries = CoreUserFieldValue::model()->findAllByAttributes((array('id_user' => $userId)));
			foreach ($userFieldEntries as $fieldEntry) {
				$fieldEntry->delete();
			}
		}

		return (bool)$success;
	}


	/**
	 * get visible fields by specified orgchart node (registration code, which is the node idOrg) or multidomain
	 * @param bool|int $registrationCode the idOrg of the node, false if no node has been specified
	 * @return array|bool list of fields id (false = no filter has to be applied)
	 */
	public function getFieldsFilter($registrationCode = false) {

		//first check node visibility setting ... if it is not set, no checks have to be performed about node visibility
		if (Settings::get('use_node_fields_visibility', 'off') == 'off') { return false; }

		//if a registration (folder) code has been specified, use it (otherwise continue with multidomain checking)
		if (!empty($registrationCode) || (is_numeric($registrationCode) && $registrationCode >= 0)) { //NOTE: in some cases the ID of the target node can be 0 !!
			// Get the actual org chart node from DB, based on the provided code or ID
			$criteria = new CDbCriteria();
			if (Settings::get('registration_code_type') == 'tree_man'){
				$criteria->addCondition('code = :reg_code');
				$criteria->params[':reg_code'] = $registrationCode;
			} elseif (Settings::get('registration_code_type') == 'tree_drop'){
				$criteria->addCondition('idOrg = :idOrg');
				$criteria->params[':idOrg'] = $registrationCode;
			}

			if(PluginManager::isPluginActive("MultidomainApp") && ($multidomainClient = CoreMultidomain::resolveClient())) {
				/* @var $multidomainClient CoreMultidomain */
				if ($multidomainClient->orgChart) {
					$criteria->addCondition("iLeft >= :iLeft AND iRight <= :iRight");
					$criteria->params[':iLeft'] = $multidomainClient->orgChart->iLeft;
					$criteria->params[':iRight'] = $multidomainClient->orgChart->iRight;
				}
			}

			$node = CoreOrgChartTree::model()->find($criteria);

			if ($node) {
				if ($node->isRoot()) { return false; } //root node always shows all fields
				return $this->retrieveNodeFields($node->getPrimaryKey());
			} else {
				throw new CException('Invalid registration code provided ['.$registrationCode.']');
			}
		}

		//detect if some filter on additional fields visibility must be applied for multidomain
		$fieldsFilter = false;
		if (Settings::get('use_node_fields_visibility', 'off') == 'on' && PluginManager::isPluginActive('MultidomainApp')) {
			CoreMultidomain::resolveClient();
			$clientInfo = Yii::app()->session['multidomain_client'];
			if ($clientInfo && isset($clientInfo['client'])) {
				$client = $clientInfo['client']; /* @var $client CoreMultidomain */
				if ($client && is_numeric($client->org_chart)) {
					$fieldsFilter = $this->retrieveNodeFields($client->org_chart);
				}
			}
		}

		return $fieldsFilter;
	}


	/**
	 * effectively retrieve fields associated to a specified note
	 * @param $node int|CoreOrgChartTree the node AR or node ID
	 * @return array|bool list of nodes or false if an error occurs
	 */
	public function retrieveNodeFields($node) {
		//check input parameter
		if (is_numeric($node)) {
			$node = CoreOrgChartTree::model()->findByPk($node);
		}
		//validate retrieved node (at this point it must be a AR node of CoreOrgChartTree class)
		if (!is_object($node) || get_class($node) != 'CoreOrgChartTree') {
			return false;
		}
		
		$fields = false; 
		
        if ($node) {
		    
            $fields = array();

            // Node's direct assignment
            $cmd = Yii::app()->db->createCommand();
			$fields = $cmd
                ->select("cgf.id_field")
			    ->from("core_group_fields cgf")
			    ->join("core_user_field cuf", "cuf.id_field=cgf.id_field")
			    ->where("idst = :idst_oc OR idst = :idst_ocd", array(
			        ":idst_oc"  => $node->idst_oc,
			        ":idst_ocd" => $node->idst_ocd,
			    ))
			    ->queryColumn();

			 // Inherited fields
		     $inherited = array();
		     $cmd = Yii::app()->db->createCommand();
		     $inherited = $cmd
                ->select("cgf.id_field")
                ->from("core_org_chart_tree tree")
                ->join("core_group_fields cgf", "cgf.idst=tree.idst_oc OR cgf.idst=tree.idst_ocd")
                ->where("tree.iLeft < :iLeft AND tree.iRight > :iRight AND tree.idOrg <> :root", array(
                    ':iLeft'  => $node->iLeft,
                    ':iRight' => $node->iRight,
                    ':root'   => CoreOrgChartTree::getOrgRootNode()->idOrg
                ))
                ->queryColumn();
		     
             $fields = array_unique(array_merge($fields, $inherited));
			     
		}

		return $fields;
	}


	/**
	 * Check if provided registration code is actually a valid org. tree code
	 * @param $code the code to check
	 * @return bool true/false node is valid or not
	 */
	public function checkRegistrationCode($reg_code) {

		$found = false;
		if ($reg_code !== false && $reg_code !== '') {

			// Check if this org chart is valid/exists
			$criteria = new CDbCriteria();
			if (Settings::get('registration_code_type')=='tree_man') {
				$criteria->addCondition('code = :reg_code');
				$criteria->params[':reg_code'] = $reg_code;
			} elseif (Settings::get('registration_code_type')=='tree_drop') {
				$criteria->addCondition('idOrg = :idOrg');
				$criteria->params[':idOrg'] = $reg_code;
			}

			if(PluginManager::isPluginActive("MultidomainApp") && ($multidomainClient = CoreMultidomain::resolveClient())) {
				/* @var $multidomainClient CoreMultidomain */
				if ($multidomainClient->orgChart) {
					$criteria->addCondition("iLeft >= :iLeft AND iRight <= :iRight");
					$criteria->params[':iLeft'] = $multidomainClient->orgChart->iLeft;
					$criteria->params[':iRight'] = $multidomainClient->orgChart->iRight;
		}
			}

			$found = CoreOrgChartTree::model()->count($criteria);
		}

		return $found;
	}




	/**
	 * @param $userTempInfo
	 * @param null $registerType
	 * @return bool
	 */
	public function _confirmRegistration($userTempInfo, $registerType = null) {

		if (null === $registerType) {
			$registerType = Settings::get('register_type', '', true);
		}

		if ($registerType == 'moderate') {
			$userTempInfo->confirmed = 1;
			$save = $userTempInfo->save();

			if($save){
				// Raise event that a waiting user has confirmed his email and is ready for admin approval
				Yii::app()->event->raise(CoreNotification::NTYPE_USER_WAITING_APPROVAL_SUBSCRIPTION, new DEvent($this, array(
					'userwaiting' => $userTempInfo,
				)));
				return true;
			}

			return false;
		}

		// Convert a core_user_temp to core_user
		// Also, reapply any org charts he may have selected during creation
		$idSt = $userTempInfo->confirm();

		// Raise event
		if ($idSt) {
			$userModel = CoreUser::model()->findByPk($idSt);
			$userModel->removeUploadsOnRequested(); //this is needed when user is directly registered, but it won't have any effect in other cases
			Yii::app()->event->raise(CoreNotification::NTYPE_NEW_USER_CREATED_SELFREG, new DEvent($this, array(
				'target_user' => $userModel,
                'user' => $userModel
			)));

			// Apply base enrollment rule to this new registered user (subscribe him to courses)
			CoreRules::model()->applyBaseEnrollmentRuleToUser($idSt);
			$isSendConfirmationLinkActive = Settings::get('disable_registration_email_confirmation');
			if($isSendConfirmationLinkActive === 'on'){
				$userModel->email_status = 0;
			}else{
				$userModel->email_status = $userTempInfo->confirmed;
			}
			$userModel->save();
			return true;
		}

		// Setting lastEnter in core_user: is it to do???
		return false;
	}






	/**
	 * Effective user registration
	 * @param bool|array $data optional manually provided user data
	 * @return array
	 */
	public function registerUser($data = false, $fieldsFilter = false) {

		//in stepped process, some parameters mey not be available in request input and must be provided as argument for this function
		$postData = array(
			'username' => trim( is_array($data) && isset($data['username']) ? $data['username'] : trim(strip_tags(Yii::app()->request->getParam('username'))) ),
			'firstname' => trim( is_array($data) && isset($data['firstname']) ? $data['firstname'] : trim(Yii::app()->request->getParam('firstname')) ),
			'lastname' => trim( is_array($data) && isset($data['lastname']) ? $data['lastname'] : trim(Yii::app()->request->getParam('lastname')) ),
			'password' => is_array($data) && isset($data['password']) ? $data['password'] : Yii::app()->request->getParam('password'),
			'email' => is_array($data) && isset($data['email']) ? $data['email'] : trim(Yii::app()->request->getParam('email')),
			'language' => is_array($data) && isset($data['language']) ? $data['language'] : Yii::app()->request->getParam('language'),
			'reg_code' => is_array($data) && isset($data['reg_code']) ? $data['reg_code'] : Yii::app()->request->getParam('reg_code'),
			'cl_tz_offset' => is_array($data) && isset($data['cl_tz_offset']) ? $data['cl_tz_offset'] : Yii::app()->request->getParam('cl_tz_offset')
		);

		$output = array('success' => false); //initialize output variable

		$regCodeMandatory = (Settings::get('mandatory_code')=='on' && (Settings::get('registration_code_type')=='tree_man' || Settings::get('registration_code_type')=='tree_drop'));

		if (Yii::app()->event->raise('BeforeUserSelfRegisterSave', new DEvent($this, array()))) {

			$coreUserTempModel = new CoreUserTemp();
			$idSt = $this->createSt();
			$additionalFieldsFilled = $this->processAdditionalFields($idSt, $fieldsFilter);

			if ($additionalFieldsFilled) {

				$newUserName = $postData['username'];

				// Creating a random code
				$randomCode = md5($newUserName.mt_rand().mt_rand().mt_rand());

				// Creating a temp user
				$coreUserTempModel->idst = $idSt;
				$coreUserTempModel->random_code = $randomCode;

				$coreUserTempModel->userid = $newUserName;
				$coreUserTempModel->firstname = $postData['firstname'];
				$coreUserTempModel->lastname = $postData['lastname'];
				$coreUserTempModel->pass = md5($postData['password']);
				$coreUserTempModel->email = $postData['email'];
				$coreUserTempModel->request_on = Yii::app()->localtime->toLocalDateTime();
				$coreUserTempModel->create_by_admin = 0;

				$currentLang = strtolower($postData['language'] ? $postData['language'] : Yii::app()->getLanguage());

				if ($currentLang)
					$coreUserTempModel->language = $currentLang;


				if (Yii::app()->request->getParam('reg_code')) {
					// Org chart selected, subscribe the temporary user to it (without triggering Enrollment rules yet though)

					// What the user provided in the form
					$regcode = $postData['reg_code'];
					if($regcode && (Settings::get('registration_code_type')=='tree_course')){
						$courseModel = LearningCourse::model()->findByAttributes(array('autoregistration_code' => substr($regcode, 10, 10)));
						if($courseModel){
							$subscriptionIsOpen =   ($courseModel->can_subscribe == LearningCourse::COURSE_SUBSCRIPTION_CLOSED)? false : (($courseModel->can_subscribe == LearningCourse::COURSE_SUBSCRIPTION_OPEN) ? true : $courseModel->checkSubscribtionDates());
							$maxSubsReached     =   $courseModel->course_type != LearningCourse::TYPE_ELEARNING || ($courseModel->max_num_subscribe==0 || $courseModel->max_num_subscribe > $courseModel->getTotalSubscribedUsers(false, false, false)) ? false : true;
							$subscribeAsWaiting =  ($maxSubsReached && $courseModel->allow_overbooking) ? true : false ;
							if($subscriptionIsOpen  && !($courseModel->subscribe_method == LearningCourse::SUBSMETHOD_ADMIN)){
								$coreUserTempModel->self_subscribe_course = $courseModel->idCourse;
							}
						}
					}

					// Get the actual org chart from DB, based on the provided code or ID
					$criteria = new CDbCriteria();
					if (Settings::get('registration_code_type') == 'tree_man'){
						$criteria->addCondition('code = :reg_code');
						$criteria->params[':reg_code'] = $regcode;
					} elseif (Settings::get('registration_code_type')=='tree_drop'){
						$criteria->addCondition('idOrg = :idOrg');
						$criteria->params[':idOrg'] = $regcode;
					}elseif(Settings::get('registration_code_type')=='tree_course'){
						$criteria->addCondition('code = :code');
						$criteria->params[':code'] = substr($regcode, 0, 10);
					}

					if(PluginManager::isPluginActive("MultidomainApp") && ($multidomainClient = CoreMultidomain::resolveClient())) {
						/* @var $multidomainClient CoreMultidomain */
						if ($multidomainClient->orgChart) {
							$criteria->addCondition("iLeft >= :iLeft AND iRight <= :iRight");
							$criteria->params[':iLeft'] = $multidomainClient->orgChart->iLeft;
							$criteria->params[':iRight'] = $multidomainClient->orgChart->iRight;
						}
					}

					$orgChart = CoreOrgChartTree::model()->find($criteria);

					if ($orgChart) {
						// Org chart exists, get groups associated
						// with it and subscribe the temp user to them
						$c = new CDbCriteria();
						$c->addInCondition('idst', array($orgChart->idst_oc, $orgChart->idst_ocd));
						$groups = CoreGroup::model()->findAll($c);
						if($groups){
							foreach($groups as $group){
								// Remember to use the 'self-register' scenario
								// to avoid triggering enrollment rules
								// which won't work on temporary user IDs (the
								// user is still at core_user_temp until he is approved
								// by admin or confirms his email (on "free" self-registration mode)
								$addMember = new CoreGroupMembers('self-register');
								$addMember->idst = $group->idst;
								$addMember->idstMember = $coreUserTempModel->idst;
								$addMember->save();
							}
						}
					} else {
						// No org chart found for the provided ID/code

						if($regCodeMandatory){
							// Hey, you have entered a org chart code that doesn't exist
							// Here's a failed registration for you
							Yii::app()->user->setFlash('error',Yii::t('register', '_INVALID_CODE'));
						}
					}

				} else {
					// Reg code not provided by user. This should usually not be reached
					// since the validation is done far before reaching this point.
					// See: $this->validateFormRegister()
				}

				$coreUserTempModel->save();

				Yii::app()->event->raise('AfterUserSelfRegisterSave', new DEvent($this, array(
					'coreUserTemp' => $coreUserTempModel
				)));


				$output['idst'] = $coreUserTempModel->idst;
				// check if we require email confirmation
				$isEmailConfirmationRequired = (Settings::get('disable_registration_email_confirmation') !== 'on');
				if ($isEmailConfirmationRequired) {
					// Sending an email to user
					$subject = Yii::t('register', '_MAIL_OBJECT');
					$fromAdminEmail = array(Settings::get('mail_sender') => Settings::get('mail_sender'));
					$toEmail = array(Yii::app()->request->getParam('email')=>Yii::app()->request->getParam('email'));

					$link = Yii::app()->createAbsoluteUrl('user/confirmRegistration', array('random_code'=> $randomCode));

					// If Multidomain is enabled, use the HTTP_HOST and ignore custom domain settings
					if (!PluginManager::isPluginActive('MultidomainApp')) {
						// If the Custom Domain App is active and configured use the new URL in the confirm email link
						$originalDomain = parse_url($link, PHP_URL_HOST);
						$customDomain = rtrim(Docebo::getCurrentDomain(), '/');
						if ($customDomain && $originalDomain)
							$link = str_ireplace($originalDomain, $customDomain, $link);
					}

					// We store the protocol sometimes, so there can be those weird URLs
					$link = str_ireplace('http://http://', 'http://', $link);
					$link = str_ireplace('https://https://', 'https://', $link);
					$link = str_ireplace('https://http://', 'https://', $link);
					$link = str_ireplace('http://https://', 'https://', $link);

					$body = Yii::t('register', '_REG_MAIL_TEXT', array(
						'[userid]' => $newUserName,
						'[password]'=> $postData['password'],
						'[firstname]' => $postData['firstname'],
						'[lastname]' => $postData['lastname'],
						'[link]' => $link,
						'[hour]' => Settings::get('hour_request_limit')

					));
					$mm = new MailManager();
					$mm->setContentType('text/html');
					$mm->mail($fromAdminEmail,$toEmail, $subject, $body);

					$output['success'] = true;
					$output['email'] = $body;
					return $output;

				} else {

					// confirm the user registration without sending any emails
					if ($this->_confirmRegistration($coreUserTempModel)) {
						// also automatically login the user

						$username = $postData['username'];
						$password = $postData['password'];

						$ui = new DoceboUserIdentity($username, $password);

						if (Settings::get('register_type') == 'self') {

							//free registration: login the user directly into LMS
							if ($ui->authenticate() && Yii::app()->user->login($ui))
							{
								// Autodetect timezone when user is login after self-registering
								$settingTimezone = CoreSettingUser::model()->findByAttributes(array(
									'id_user' =>  Yii::app()->user->id,
									'path_name' => 'timezone',
								));

								if ((!$settingTimezone || !$settingTimezone->value) && Settings::get('timezone_allow_user_override', 'off') === 'on'){
									$clientTimezoneOffset = $postData['cl_tz_offset'];
									$serverTimezones = Yii::app()->localtime->getTimezoneOffsetsSecondsArray();
									$foundedTimezone = array_search($clientTimezoneOffset, $serverTimezones);
									if($foundedTimezone){
										$userId =  Yii::app()->user->id ;

										if ($settingTimezone === null){
											$settingTimezone = new CoreSettingUser('create');
											$settingTimezone->id_user =  $userId;
											$settingTimezone->path_name = 'timezone';
										}

										$settingTimezone->value = $foundedTimezone;
										if($settingTimezone->save()){
											$userName = Yii::app()->user->getUsername();
											$user = $userName ? $userName : $userId;
											Log::_('User: '.$user.' time zone '.$foundedTimezone.' has been automatically set after self registration and automatic login to LMS.');
										}
									}
								}

								$redirectUrl =  Docebo::getUserAfetrLoginRedirectUrl();
								if(isset(Yii::app()->session['subscribe_course_id_after_login']) && Yii::app()->session['subscribe_course_id_after_login']){
									$courseModel = LearningCourse::model()->findByPk(Yii::app()->session['subscribe_course_id_after_login']);
									if($courseModel && ($courseModel->course_type == LearningCourse::TYPE_ELEARNING) && $courseModel->subscribe_method == LearningCourse::SUBSMETHOD_FREE){
										$courseUserModel = new LearningCourseuser('self-subscribe');
										$result = $courseUserModel->subscribeUser(Yii::app()->user->id, Yii::app()->user->id, $courseModel->idCourse, 0, false, false, false, false, false, false);
										if($result){
											$redirectUrl = Docebo::createLmsUrl('site/index', array('opt' => 'fullmycourses'));
										}
									}
								}

								$regcode = $postData['reg_code'];
								if($regcode && (Settings::get('registration_code_type')=='tree_course')){
									$courseModel = LearningCourse::model()->findByAttributes(array('autoregistration_code' => substr($regcode, 10, 10)));
									if($courseModel){
										$subscriptionIsOpen =   ($courseModel->can_subscribe == LearningCourse::COURSE_SUBSCRIPTION_CLOSED)? false : (($courseModel->can_subscribe == LearningCourse::COURSE_SUBSCRIPTION_OPEN) ? true : $courseModel->checkSubscribtionDates());
										$maxSubsReached     =   $courseModel->course_type != LearningCourse::TYPE_ELEARNING || ($courseModel->max_num_subscribe==0 || $courseModel->max_num_subscribe > $courseModel->getTotalSubscribedUsers(false, false, false)) ? false : true;
										$subscribeAsWaiting =  ($maxSubsReached && $courseModel->allow_overbooking) ? true : false ;
										if($subscriptionIsOpen  && !($courseModel->subscribe_method == LearningCourse::SUBSMETHOD_ADMIN)){
											$courseUserModel = new LearningCourseuser('self-subscribe');
											if($courseModel->course_type == LearningCourse::TYPE_ELEARNING && !$subscribeAsWaiting && !($courseModel->subscribe_method == LearningCourse::SUBSMETHOD_MODERATED)){
												$result = $courseUserModel->subscribeUser(Yii::app()->user->id, Yii::app()->user->id, $courseModel->idCourse, 0, false, false, false, false, false, false);
											}else{
                                                $waiting = 1;
                                                //if it's a course with sessions, it's not full, and is not moderated, let the user choose their session (dont put him in the waitinglist)
                                                //i tried to refactor the upper code but it was taking to long and was creating new bugs ...
                                                if (in_array($courseModel->course_type, array(
                                                        LearningCourse::TYPE_CLASSROOM,
                                                        LearningCourse::TYPE_WEBINAR
                                                    )) &&
                                                    ($courseModel->max_num_subscribe == 0 || $courseModel->max_num_subscribe > $courseModel->getTotalSubscribedUsers(false,false, false)) &&
                                                    $courseModel->subscribe_method == LearningCourse::SUBSMETHOD_FREE
                                                ) {
                                                    $waiting = 0;
                                                }

												$result = $courseUserModel->subscribeUser(Yii::app()->user->id, Yii::app()->user->id, $courseModel->idCourse, $waiting, false, false, false, false, false, false);
											}
											if($result){
												$redirectUrl = Docebo::createLmsUrl('site/index', array('opt' => 'fullmycourses'));
											}
										}
									}
								}



								$output['success'] = true;
								$output['redirect_url'] = $redirectUrl;
								return $output;

							} else {
								// TODO: handle this error
							}

						} else {

							//newly created user still need admin approvation, just show a success message
							$output['success'] = true;
							$output['admin_approval'] = true;
						}

					}
				}

			} else {
				//what if additional fields are not filled ?!?
				$output['success'] = false;
				$output['message'] = Yii::t('register', '_SOME_MANDATORY_EMPTY');
			}
		}

		return $output;
	}

	private function filterEmailDomain($input){
		// Lets strip everything till the @
		$email =  preg_replace('/^[^@]*/', '', $input);
		$domains = Settings::get('allowed_domains');
		$allowedDomains = is_string($domains) ? CJSON::decode($domains) : $domains;
		// There are no domains to compare with
		if(empty($allowedDomains))
			return true;

		return in_array($email, $allowedDomains);
	}

}