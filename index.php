<?php
 
/* ========================================================================	\
|	DOCEBO - The E-Learning Suite											|
| 																			|
|	Copyright (c) 2013 (Docebo)												|
| 	http://www.docebo.com													|
\ ======================================================================== */

if(!empty($_GET['domain']))
    Header("Location: /".$_GET['domain']."/lms/");
else
    Header("Location: lms/");
exit();