#!/bin/sh

# Make this executable or just use it like this:
# sh cleancaches.sh

# Do come cache cleanup
# Note: LMS System caches only, not per domain

# Yii caches
rm -R admin/assets/*
rm -R lms/assets/*
rm -R authoring/assets/*

# Some more Yii temp files and junk files created by some libraries
rm -R files/tmp/*.bin
rm -rf lms/protected/runtime/HTML
rm -rf lms/protected/runtime/URI
rm -rf files/HTML
rm -rf files/URI

# This one will prevent "I don't see the new translation" in JS 
rm -R common/extensions/JsTrans/assets/dictionary-*

rm -Rf themes/spt/assets/*
rm -R themes/spt/css/merged_*