#!/bin/sh

chmod -R 777 admin/assets
chown -R www-data.www-data admin/assets
chmod -R 777 admin/protected/runtime
chown -R www-data.www-data admin/protected/runtime
chmod -R 777 lms/assets
chown -R www-data.www-data lms/assets
chmod -R 777 lms/protected/runtime
chown -R www-data.www-data lms/protected/runtime
chmod -R 777 common/extensions/JsTrans/assets
chown -R www-data.www-data common/extensions/JsTrans/assets
chmod -R 777 authoring/assets
chown -R www-data.www-data authoring/assets
chmod -R 777 authoring/protected/runtime
chown -R www-data.www-data authoring/protected/runtime
chmod -R 777 media
chown -R www-data.www-data media
chmod -R 777 themes/spt/assets
chown -R www-data.www-data themes/spt/assets
chmod -R 777 config
chown -R www-data.www-data config
chmod -R 777 lmslogs
chown -R www-data.www-data lmslogs
chmod -R 777 app7020/protected/runtime
chown -R www-data.www-data app7020/protected/runtime


                                               