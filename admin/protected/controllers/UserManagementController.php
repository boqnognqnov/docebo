<?php

class UserManagementController extends AdminController {

	public function beforeAction($action) {
		JsTrans::addCategories('user_management');
		DatePickerHelper::registerAssets();
		return parent::beforeAction($action);
	}

	public function filters() {
		return array(
				'accessControl' => 'accessControl',
		);
	}

	public function accessRules() {
		$res = array();

		//Additional permission check for the usersAutocomplete function
		$res[] = array(
				'allow',
				'roles' => array('/lms/admin/report/view'),
				'actions' => array(
						'usersAutocomplete'
				),
		);

		$res[] = array(
				'allow',
				'roles' => array('/framework/admin/usermanagement/view'),
				'actions' => array(
						'index',
						'home', 'axRenderBranchActionsMenu', 'axRenderOrgchartFancyTree',
						'saveFilterSelection',
						'usersAutocomplete',
						'syncGappsUsers',
						'exportUsersToCsv',
						'axProgressiveExportUsers'
				),
		);

		$res[] = array(
				'allow',
				'roles' => array('/framework/admin/usermanagement/del'),
				'actions' => array('deleteUser', 'axProgressiveDeleteUsers'),
		);

		$res[] = array(
				'allow',
				'roles' => array('/framework/admin/usermanagement/associate_user'),
				'actions' => array('unasignUser'),
		);

		$res[] = array(
				'allow',
				'roles' => array('/framework/admin/usermanagement/approve_waiting_user'),
				'actions' => array(
						'activateUser',
						'deactivateUser',
						'waitingApproval',
						'confirmUser',
						'deleteTempUser',
						'tempUserSendMessage',
						'tempUserDetails',
						'axGetWaitingUsersCount',
						'exportUsersToCsv'
				),
		);

		$res[] = array(
				'allow',
				'roles' => array('/framework/admin/usermanagement/mod'),
				'actions' => array('changeStatus', 'deactivateUser', 'assignToNode', 'axProgressiveChangeStatus', 'unassignFromNode', 'exportUsersToCsv', 'unassignUsersFromNodeProgressive', 'axProgressiveExportUsersToCsv', 'axProgressiveExportUsers'),
		);
		$res[] = array(
				'allow',
				'roles' => array('/framework/admin/usermanagement/mod'),
				'actions' => array('changeStatus', 'activateUser'),
		);

		$res[] = array(
				'allow',
				'roles' => array('/framework/admin/usermanagement/add'),
				'actions' => array('createUser', 'enrollNewUser', 'updateAdditionalFields', 'getCustomDateFormats'),
		);
		$res[] = array(
				'allow',
				'roles' => array('/framework/admin/usermanagement/mod'),
				'actions' => array('updateUser', 'updateUserMassive', 'getCustomDateFormats',  'axProgressiveUsersAssignToNode', 'exportUsersToCsv'),
		);

		$res[] = array(
				'allow',
			//'roles' => array('/framework/admin/usermanagement/mod'),
				'roles' => array('/framework/admin/usermanagement/add'), //imported user are being added, not modded !!
				'actions' => array('importUser', 'axProgressiveUsersImport', 'endUserCsvImport'),
		);

		$res[] = array(
				'allow',
				'users' => array('@'),
				'actions' => array('verifyEmail', 'AxProgressiveUsersAssignToSingleNode'),
		);

		$res[] = array(
				'allow',
				'users' => array('*'),
				'actions' => array('login', 'verifyEmail', 'userWarning'),
		);

		$res[] = array(
				'deny',
			// deny all
				'users' => array('*'),
				'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);

		return $res;
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
     * @deprecated Should not be served as a valid action
	 */
	public function actionIndex_OLD() {

		// Yii::app()->clientScript->registerScriptFile($assetsPath . '/ie.js');
		$assetsPath = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin.js'));

		//Yii::app()->clientScript->registerScriptFile($assetsPath . '/charts/ie.js');
		Yii::app()->clientScript->registerScriptFile($assetsPath . '/charts/aight.min.js');

		Yii::app()->clientScript->registerScriptFile($assetsPath . '/charts/jquery.tipsy.min.js');
		Yii::app()->clientScript->registerCssFile($assetsPath . '/charts/charts.css');

		//NOTE: on some IE versions, the editing dialog will break if fields of type 'upload' are displayed
		//because the browser is unable to load plUploader JS script at runtime, so pre-include the script here
		//(not the best solution, but it seems to work).
		//TODO: try to implement a better solution.
		Yii::import('common.extensions.plupload.widgets.SingleFile', false);
		$sf = new SingleFile();
		Yii::app()->clientScript->registerScriptFile($sf->getAssetsUrl() . '/singlefile.js' , CClientScript::POS_HEAD);

		$userModel = CoreUser::model();
		$userModel->scenario = 'search';
		$filterColumns = $this->getFilterColumns();
		$selectedColumns = $this->getSelectedColumns($filterColumns);

		// $_REQUEST in this case can be weird since it takes data
		// directly from the <form> on submit and attaches some custom
		// (search) attributes via JS. See userManagement/index.php 'beforeAjaxUpdate'
		// on how the options.data JS object is built
		$userModel->attributes = $_REQUEST['CoreUser'];

		// save selected filters
		$attributesForSave = $userModel->attributes;
		$attributesForSave['containChildren'] = $_REQUEST['CoreUser']['containChildren'];

		// is used for autocomplete
		if (isset($_REQUEST['data']['autocomplete'])) {
			$userModel = Yii::app()->session[$this->action->id.'FilterModel'];
			$userModel->search_input = addcslashes($_REQUEST['data']['query'], '%_');
			$dataProvider = $userModel->dataProvider();
			$dataProvider->pagination = false;
			$result = array('options' => array());
			foreach ($dataProvider->data as $user) {
				$username = Yii::app()->user->getRelativeUsername($user->userid);
				$text = "{$username}";

				$names = array();
				if (!empty($user->firstname))
					$names[] = $user->firstname;
				if (!empty($user->lastname))
					$names[] = $user->lastname;
				$names = join(' ', $names);
				if ($names)
					$text .= " - $names";

				$result['options'][$username] = $text;
			}
			$this->sendJSON($result);
		}
		Yii::app()->session[$this->action->id.'FilterModel'] = $userModel;

		// show overlay hints for admin
		$showHints = false;
		if (Yii::app()->user->getIsGodadmin() || Yii::app()->user->getIsAdmin()) {
			$userDataProvider = $userModel->dataProvider(true);
			$showHints = (0 == $userDataProvider->itemCount);
		}

		$selectedColumns['actions'] = array(
				'type' => 'raw',
				'value' => '$data->renderUserActions();',
		);

		$this->render('index', array(
				'userModel' => $userModel,
				'filterColumns' => $filterColumns,
				'selectedColumns' => $selectedColumns,
				'additionalFields' => CoreUserField::model()->filters()->language()->findAll(),
				'showHints' => $showHints,
				'permissions' => CoreUser::getAdminPermissions(),
				'assetsPath' => $assetsPath,
		));
	}





	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
		// This will set current node ID in session (used by data provider later)
		$currentNodeId = $this->resolveCurrentOrgchartNodeId();

		// SELECT ALL request: just return list of users based on current filter
		if (isset($_REQUEST['selectAll'])) {
			/* @var $userModel CoreUser */
			$userModel = Yii::app()->session[$this->action->id.'FilterModel'];
			$dataProvider = $userModel->dataProvider(true);
			$dataProvider->pagination = false;  // get all
			$keys =  array();

			//chunking to avoid excessive memory usage by AR models
			$chunk = 3000;
			$index = 0;
			$cloneDataProvider = clone $dataProvider;
			$totalItemCount = $cloneDataProvider->getTotalItemCount();
			$cloneDataProvider = NULL;
			unset($cloneDataProvider);
			for ($i = 0; $i <= $totalItemCount; $i=$i+$chunk){
				$criteria = $dataProvider->criteria;
				$criteria->select= 't.idst AS select_all_idst'.(!empty($criteria->select) ? ', '.$criteria->select : '' );
				$criteria->offset=$i;
				$criteria->limit=$chunk;
				$criteria->order = 'select_all_idst ASC';
				$recs = CoreUser::model()->findAll($criteria);
				foreach ($recs as $rec) {
					$keys[$index] = (int) $rec->idst;
					$index++;
				}
				//free memory
				$rec = NULL;
				$recs = NULL;
				unset ($rec);
				unset ($recs);
			}

			$this->sendJSON(CJSON::encode($keys));
			Yii::app()->end();
		}


		// Admin JS/CSS asset path
		$assetsPath = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin.js'));

		Yii::app()->clientScript->registerScriptFile($assetsPath . '/charts/aight.min.js');
		Yii::app()->clientScript->registerScriptFile($assetsPath . '/charts/jquery.tipsy.min.js');
		Yii::app()->clientScript->registerCssFile($assetsPath . '/charts/charts.css');

		//NOTE: on some IE versions, the editing dialog will break if fields of type 'upload' are displayed
		//because the browser is unable to load plUploader JS script at runtime, so pre-include the script here
		//(not the best solution, but it seems to work).
		Yii::import('common.extensions.plupload.widgets.SingleFile', false);
		$sf = new SingleFile();
		Yii::app()->clientScript->registerScriptFile($sf->getAssetsUrl() . '/singlefile.js' , CClientScript::POS_HEAD);


		// UserMan JS/CSS
		$cs = Yii::app()->getClientScript();
		$themeUrl = Yii::app()->theme->baseUrl;
		$cs->registerScriptFile($themeUrl . '/js/user_management.js', CClientScript::POS_HEAD);
		$cs->registerCssFile($themeUrl . '/css/user_management.css');
		$cs->registerScriptFile($assetsPath . '/jquery.cookie.js');

        // FancyTree initialization
        $baseUrl = Yii::app()->fancytree->getAssetsUrl();
        $cs->registerCssFile($baseUrl . '/skin-docebo-userman/ui.fancytree.css');

		//check if we need some specific js for external fields (used in search filters)
		if (CoreUserField::model()->findByAttributes(array('type' => CoreUserField::TYPE_DATE, 'invisible_to_user' => 0))) {
			Yii::app()->bootstrap->registerAssetCss('bootstrap-datepicker.css');
			Yii::app()->bootstrap->registerAssetJs('bootstrap.datepicker.js');
		}

		$userModel = CoreUser::model();
		$userModel->scenario = 'search';
		list($filterColumns, $hiddenColumns) = $this->getFilterColumnsV2();
		$selectedColumns = $this->getSelectedColumnsV2($filterColumns);

		// $_REQUEST in this case can be weird since it takes data
		// directly from the <form> on submit and attaches some custom
		// (search) attributes via JS. See userManagement/index.php 'beforeAjaxUpdate'
		// on how the options.data JS object is built
		$userModel->attributes = $_REQUEST['CoreUser'];

		// save selected filters
		$attributesForSave = $userModel->attributes;
		$attributesForSave['containChildren'] = $_REQUEST['CoreUser']['containChildren'];

		// Autocomplete request?
		if (isset($_REQUEST['autocomplete'])) {
			$userModel = Yii::app()->session[$this->action->id.'FilterModel'];
			$userModel->search_input = addcslashes($_REQUEST['term'], '%_');
			$dataProvider = $userModel->dataProvider(true);
			$dataProvider->pagination = array('pageSize' => isset($_REQUEST['max_number']) ? (int) $_REQUEST['max_number'] : 10);
			$result = array();
			foreach ($dataProvider->data as $user) {
				$username = Yii::app()->user->getRelativeUsername($user->userid);
				// Cut long usernames to 25 characters
				$label = Docebo::ellipsis($username, 25, "...");
				$value = $username;
				$names = array();
				if (!empty($user->firstname))
					$names[] = $user->firstname;
				if (!empty($user->lastname))
					$names[] = $user->lastname;
				$names = join(' ', $names);
				if ($names)
					$label .= " - $names";

				$result[] = array('label' => $label, 'value' => $value);
			}
			$this->sendJSON($result);
		}

		Yii::app()->session[$this->action->id.'FilterModel'] = $userModel;

		$selectedColumns['active'] = array(
				'type' => 'raw',
				'value' => '$data->renderUserActive();',
		);

		$selectedColumns['actions'] = array(
				'type' => 'raw',
				'value' => '$data->renderUserActions();',
		);


		$userManAction = Yii::app()->controller->id . "/" . Yii::app()->controller->action->id;

		// If this is an Yii Grid Update request (detected by ajax=<users-grid-ID> in the Request)
		// just render-partial the grid. This hugely improves the speed!!!
		$usersGridId = "userman-users-management-grid";
		if (isset($_REQUEST['ajax']) && ($_REQUEST['ajax']==$usersGridId)) {
			$this->renderPartial('_usersGrid', array(
					'usersGridId' 		=> $usersGridId,
					'userModel'			=> $userModel,
					'selectedColumns'	=> $selectedColumns
			));
		}
		else {
			// Register required script for users selector, as we are going to use it in this page
			UsersSelector::registerClientScripts();

			$this->render('index', array(
					'userManAction' 	=> $userManAction,
					'usersGridId' 		=> $usersGridId,
					'userModel' 		=> $userModel,
					'filterColumns' 	=> $filterColumns,
					'hiddenColumns' 	=> $hiddenColumns,
					'selectedColumns' 	=> $selectedColumns,
					'additionalFields' 	=> CoreUserField::model()->filters()->language()->findAll(),
					'showHints' 		=> isset($showHints) ? $showHints : false,
					'permissions' 		=> CoreUser::getAdminPermissions(),
					'assetsPath' 		=> $assetsPath,
			));
		}

	}

	public function getFilterColumns($selectedOnly = false) {
		$hiddenFields = array();
		$columns = CoreUser::model()->filterColumns();

		$cookieName = 'userManagementSelectedColumns';
		$selected = isset(Yii::app()->request->cookies[$cookieName]) ? unserialize(Yii::app()->request->cookies[$cookieName]) : array();

		if (empty($selected)) {
			$additional = array_keys(CoreUser::model()->getAdditionalFieldFilterColumns());

			if (!empty($additional)) {
				foreach ($additional as &$field) {
					$field = "'" . $field . "'";
				}
			}

			$hiddenFields = array_merge(array(
					"'userLanguage'",
					"'userLevel'"
			), $additional);
		} else {
			foreach ($columns as $type => $items) {
				foreach ($items as $column => $data) {
					if ($data['class'] == 'locked') {
						$columns[$type][$column]['class'] = 'locked';
					} else {
						$rawColumnKey = str_replace('td-', '', $data['cssClass']);
						if (!in_array($column, $selected)) {
							$columns[$type][$column]['class'] = '';
							$hiddenFields[] = "'$rawColumnKey'";
						} else {
							$columns[$type][$column]['class'] = 'selected';
						}
					}
				}
			}
		}

		Yii::app()->clientScript->registerScript('userManagement', 'userManagement = { hiddenFields: ['.implode(',', $hiddenFields).'] };', CClientScript::POS_HEAD);
		return $columns;
	}



	public function getFilterColumnsV2($selectedOnly = false) {
		$hiddenFields = array();
		$columns = CoreUser::model()->filterColumns();

		$cookieName = 'userman_selected_grid_columns';
		$selected = isset(Yii::app()->request->cookies[$cookieName]) ? CJSON::decode(Yii::app()->request->cookies[$cookieName]) : array();

		if (empty($selected)) {
			$additional = array_keys(CoreUser::model()->getAdditionalFieldFilterColumns());

			if (!empty($additional)) {
				foreach ($additional as &$field) {
					$field = "'" . $field . "'";
				}
			}

			$hiddenFields = array_merge(array(
					"'userLanguage'",
					"'userLevel'",
					"'expiration'",
					"'email_status'"
			), $additional);
		} else {
			foreach ($columns as $type => $items) {
				foreach ($items as $column => $data) {
					if ($data['class'] == 'locked') {
						$columns[$type][$column]['class'] = 'locked';
					} else {
						$rawColumnKey = str_replace('td-', '', $data['cssClass']);
						if (!in_array($column, $selected)) {
							$columns[$type][$column]['class'] = '';
							$hiddenFields[] = "'$rawColumnKey'";
						} else {
							$columns[$type][$column]['class'] = 'selected';
						}
					}
				}
			}
		}

		return array($columns, $hiddenFields);
	}



	public function getSelectedColumns($filterColumns) {
		$columns = array(
				array(
						'class' => 'CCheckBoxColumn',
						'selectableRows' => 2,
						'id' => 'users-management-grid-checkboxes',
						'checked' => 'in_array($data->idst, Yii::app()->session[\'selectedItems\'])',
				),
		);

		foreach ($filterColumns as $type => $items) {
			foreach ($items as $key => $column) {
				if (!empty($column['value'])) {
					$columns[] = array(
							'name' => $key,
							'value' => $column['value'],
							'type' => 'raw',
							'header' => CHtml::encode($column['title']),
							'htmlOptions' => array(
									'class' => $column['cssClass'],
							),
							'headerHtmlOptions' => array(
									'class' => $column['cssClass'],
							),
					);
				} else {
					$columns[] = array(
							'name' => $key,
							'header' => CHtml::encode($column['title']),
							'htmlOptions' => array(
									'class' => $column['cssClass'],
							),
							'headerHtmlOptions' => array(
									'class' => $column['cssClass'],
							),
					);
				}
			}
		}

		return $columns;
	}



	public function getSelectedColumnsV2($filterColumns) {
		$columns = array(
				array(
						'class' => 'CCheckBoxColumn',
						'id' => 'userman-grid-checkboxes',
					//'checked' => 'in_array($data->idst, Yii::app()->session[\'selectedItems\'])',
				),
		);

		foreach ($filterColumns as $type => $items) {
			foreach ($items as $key => $column) {
				if (!empty($column['value'])) {
					$columns[] = array(
							'name' => $key,
							'value' => $column['value'],
							'type' => 'raw',
							'header' => CHtml::encode($column['title']),
							'htmlOptions' => array(
									'class' => $column['cssClass'],
							),
							'headerHtmlOptions' => array(
									'class' => $column['cssClass'],
							),
					);
				} else {
					$columns[] = array(
							'name' => $key,
							'header' => CHtml::encode($column['title']),
							'htmlOptions' => array(
									'class' => $column['cssClass'],
							),
							'headerHtmlOptions' => array(
									'class' => $column['cssClass'],
							),
					);
				}
			}
		}

		return $columns;
	}



	public function getTreeRootBreadcrumbs($root, $defaultOrgChartTreeTranslation) {
		$node_name = $root->coreOrgChart->translation;
		if (!$node_name) $node_name = $defaultOrgChartTreeTranslation[$root->idOrg];
		$breadcrumbs = '<ul class="clearfix"><li class="last-node">'.$node_name.'</li></ul>';
		return $breadcrumbs;
	}

	public function getTreeBreadcrumbs($currentNode, $defaultOrgChartTreeTranslation) {
		$breadcrumbs = '';
		if ($currentNode) {
			$breadcrumbsNodes = $currentNode->ancestors()->findAll();
			if (!empty($breadcrumbsNodes)) {
				$breadcrumbs .= '<ul class="clearfix">';
				foreach ($breadcrumbsNodes as $breadcrumbsNode) {
					$node_name = $breadcrumbsNode->coreOrgChartTranslated->translation;
					if (!$node_name) $node_name = $defaultOrgChartTreeTranslation[$breadcrumbsNode->idOrg];
					$breadcrumbs .= '<li>'. $node_name.' &raquo;</li>';
				}
				$node_name = $currentNode->coreOrgChartTranslated->translation;
				if (!$node_name) $node_name = $defaultOrgChartTreeTranslation[$currentNode->idOrg];
				$breadcrumbs .= '<li class="last-node">'.$node_name.'</li>';
				$breadcrumbs .= '</ul>';
			}
		}
		return $breadcrumbs;
	}

	/**
	 * Create new user
	 */
	public function actionCreateUser() {
		if (Yii::app()->request->isAjaxRequest) {
			$result = array();
			$levels = Yii::app()->user->getUserLevelIds();
			//prevent power users from creating admins or other level users
			if(!Yii::app()->user->getIsGodadmin() && (isset($_POST['CoreGroupMembers']['idst']) && $levels[$_POST['CoreGroupMembers']['idst']] != Yii::app()->user->level_user)) {
				$result['success'] = false;
				$result['status'] = 'error';
				$result['html'] = 'Invalid user level';
				$this->sendJSON($result);
			}

			$result['success'] = true;
			$result['status'] = 'new';
			$result['html'] = '';

			// core_user real entry
			$user = new CoreUser('create');
			$user->unsetAttributes();

			// groups (org_chart)
			$groupMember = new CoreGroupMembers('create');
			$groupMember->unsetAttributes();

			// ui.language, set default language, will be overriden later
			$settingUser = new CoreSettingUser('create');
			$settingUser->unsetAttributes();
			$settingUser->path_name = 'ui.language';
			$settingUser->value = CoreUser::getDefaultLangCode();

			// model for user timezone
			$settingDateformat = null;
			$settingDatelocale = null;

			// is the user allowed to set the date format?
			if (Settings::get('date_format') == 'user_selected') {

				$settingDateformat = new CoreSettingUser('create');
				$settingDateformat->path_name = 'date_format';

				$settingDatelocale = new CoreSettingUser('create');
				$settingDatelocale->path_name = 'date_format_locale';
			}

			$errors = array();
			$activeTab = 'userForm-details';


			if (Yii::app()->session['currentNodeId'] > 1) {
				$user->setOrgChartGroupsList(array(Yii::app()->session['currentNodeId']));
			}

			if (isset($_POST['CoreUser']) && isset($_POST['CoreGroupMembers']) && isset($_POST['CoreSettingUser'])) {

				// Save the user
				$result['success'] = false;

				// Sanitize the input params for core user db table !!!
				foreach($_POST['CoreUser'] as $attrKey => $attrValue) {
					if (is_string($attrValue)) {
						$user->$attrKey = trim($attrValue);
					}
				}

				if(isset($_POST['CoreUser']['userid'])) {
					$user->userid = strip_tags($_POST['CoreUser']['userid']);
				}

				// Sanitize also additional fields
				$postedAdditionalFields = array();
				if (!empty($_POST['CoreUser']['additional']) && is_array($_POST['CoreUser']['additional'])) {
					foreach($_POST['CoreUser']['additional'] as $attrKey => $attrValue) {
						$postedAdditionalFields[$attrKey] = $attrValue;
						if (is_string($attrValue['value'])) {
							$postedAdditionalFields[$attrKey]['value'] = trim($attrValue['value']);
						}
					}
				}

				$user->setAdditionalFieldValues($postedAdditionalFields, $_POST['FileUpload']);

				// !!!!! Handle Fancytree based style of data submition, where selected branches are sent in the form of
				// select-orgchart[<node id>] = <select state> (1 for selected, 0 for unselected)
				if (isset($_POST['select-orgchart'])) {
					foreach ($_POST['select-orgchart'] as $nodeId => $selectState) {
						$user->chartGroups[] = $nodeId;
					}
				}

				/**
				 * if the user is PU and he manages only one branch, users he creates will be automatically
				 * assigned to that branch
				 */
				if (Yii::app()->user->getIsPU()) {
					$branches = CoreOrgChartTree::getPuBranchIds();
					if (count($branches)==1) {
						// add the branch id that the PU manages
						$user->chartGroups[] = $branches[0];
					}
				}

				$groupMember->attributes = $_POST['CoreGroupMembers'];

				// Maybe this user is a Power User? Validate branch selection:
				// Rule: PU MUST select at least one branch for the user.
				$ok = CoreUser::validatePowerAdminChartSelection($user->chartGroups, true);
				if (!$ok) {
					$errors[] = Yii::t('user_management', 'You are required to select a branch');
					$activeTab = 'userForm-orgchart'; // Activate this tab/pane on dialog open
				}

				$tempUser = CoreUserTemp::model()->findByAttributes(array('userid' => $user->userid));
				if($tempUser)
				{
					$errors[] = Yii::t('organization_chart', '_USERID_DUPLICATE');
					$activeTab = 'userForm-details';
				}

				if ($ok && $user->validate() && $groupMember->validate() && $settingUser->validate() && !$tempUser) {

					$saveResult = $user->save();

					// $user->save() may return FALSE **and** change the scenario on the fly if current user is a Power User
					// with NO permissions to directly create and activate users
					if ($saveResult || ($user->scenario == 'powerUserCreateTempUser')) {

						$groupMember->idstMember = $user->idst;
						$groupMember->save();

						// Sync with the Hydra RBAC
						HydraRBAC::manageUserToRole(array( 'user' => $user->getPrimaryKey(), 'group' => $groupMember->idst ));

						$user->saveChartGroups();
						$newNodes = $user->chartGroups;
						if (($key = array_search(CoreOrgChartTree::getOrgRootNode()->idOrg, $newNodes)) !== false) {
							unset($newNodes[$key]);
						}
						foreach($newNodes as $node){
							Yii::app()->event->raise(CoreNotification::NTYPE_USER_ASSIGNED_TO_BRANCH, new DEvent($this, array(
									'targetUser' => $user->idst,
									'BranchIdOrg'  => $node
							)));
						}

						// extra setting
						if (isset($_POST['CoreSettingUser']) && $user->scenario != 'powerUserCreateTempUser') {

							foreach ($_POST['CoreSettingUser'] as $path_name => $value) {
								switch ($path_name) {
									case 'ui.language':
										$settingUser->id_user = $user->idst;
										$settingUser->value = $value;
										$settingUser->save();
										break;
									case 'date_format':
										$settingDateformat->id_user = $user->idst;
										$settingDateformat->value = $value;
										$settingDateformat->save();
										break;
									case 'date_format_locale':
										$settingDatelocale->id_user = $user->idst;
										$settingDatelocale->value = $value;
										$settingDatelocale->save();
										break;
									default: break;
								}
							}
						}

						// Normal process, normal user creation
						if ($user->scenario != 'powerUserCreateTempUser') {

							// Raise event. Must be raised AFTER user settings are saved to allow gathering more user info by Notification agent
							$user->passwordPlainText = $user->new_password;
							Yii::app()->event->raise('NewUserCreated', new DEvent($this, array(
									'user' => $user,
							)));
							if (Yii::app()->session['currentNodeId'] > 1) {
								$branchError = $user->checkBranchError(Yii::app()->session['currentNodeId']);
							}else{
								$branchError = $user->checkBranchError();
							}



							if (isset($_POST['sendEmailBox']) && $_POST['sendEmailBox'] == 1 && $user->email_status == 0) {
								if ($user->setVerificationLink(false))
									Yii::app()->event->raise('UserEmailVerified', new DEvent($this, array('user' => $user)));
							}

							$this->redirect(Yii::app()->createUrl($this->id . '/enrollNewUser', array(
									'idst' => $user->idst,
									'branchError' => $branchError
							)));
						}
						// Looks like current user is PU with NO permission to directly create an user.
						// At this point TEMP user is already created (inside the CoreUser model saving workflow)
						else {
							// Lets set the language of the TEMP user
							if ($settingUser->path_name == 'ui.language' && !empty($settingUser->value)) {
								$userLang = $settingUser->value;
								$tempUser = CoreUserTemp::model()->findByPk($user->idst);
								$tempUser->language = $userLang;
								$tempUser->save();
							}

							// Raise an event
							// Note: we need another event type:  NewWaitingUserCreated
							$tempUser->passwordPlainText = $user->new_password;
							Yii::app()->event->raise('NewUserCreated', new DEvent($this, array(
									'user' => $tempUser,
							)));

							$result['html'] = '';
							$result['success'] = true;
							$this->sendJSON($result);
						}
					}
				}
			}

			//This is true only when the user opens the dialog, then false if dialog is reloaded (e.g. wrong password etc.)
			//Note that in the view it is always set to 0. If the parameter is not present then we know that this is the first
			// time we are trying to open the dialog and so read a default '1' value.
			$isFirstLoading = (Yii::app()->request->getParam('first-loading', 1) > 0);

			$orgChartFancyTreePreselected = array();
			if ($isFirstLoading) {
				// Get the currently selectoed org chart node from session and "preselect it" in Branch Tree (fancytree)
				if (isset(Yii::app()->session['currentNodeId']) && ((int)Yii::app()->session['currentNodeId'] > 0)) {
					$orgChartFancyTreePreselected[] = array('key' => (int)Yii::app()->session['currentNodeId'], 'selectState' => 1);
				}
			} else {
				//in case of error and dialog reloaded, keep the already selected nodes
				if (isset($_POST['select-orgchart'])) {
					foreach ($_POST['select-orgchart'] as $nodeId => $selectState) {
						$orgChartFancyTreePreselected[] = array('key' => $nodeId, 'selectState' => $selectState);
					}
				}
			}
			$prePolupatedFields = array();
			// get selected additional fields for this PU
			if(CoreUser::isPowerUser(Yii::app()->user->id)){
				$puModel = CoreUser::model()->findByPk(Yii::app()->user->id)->with('adminProfile');
				$profileName = $puModel->adminProfile[0]->relativeProfileId();
				$selectedFields = explode(',', Yii::app()->db->createCommand()
						->select('additional_fields')
						->from(CoreGroup::model()->tableName() . ' cg')
						->join(CoreGroupMembers::model()->tableName() . ' cgm', 'cg.`idst`=cgm.`idst`')
						->where(array('LIKE', 'cg.groupid', '%' . $profileName . '%'))->queryScalar());

                $prePolupatedFields = CoreUser::getAdditionalFieldEntryValuesArray([$puModel->idst], true, true);
                $prePolupatedFields = $prePolupatedFields[$puModel->idst];

				foreach($selectedFields as $field){
					if($field == 'language'){
						$prePolupatedFields[$field] = $puModel->getCurrentUserLanguage();
						continue;
					}
				}
			}
			$prePolupatedFields = array_filter($prePolupatedFields);
			$isActiveNotificationOfType = CoreNotification::isActiveNotificationOfType(CoreNotification::NTYPE_USER_EMAIL_MUST_VERIFY);
			$fancyTreeData = CoreOrgChartTree::buildFancytreeDataArray(false, true, true , true, true);
			$result['html'] .= $this->renderPartial('_userForm', array(
					'user' 					=> $user,
					'groupMember' 			=> $groupMember,
					'settingUser' 			=> $settingUser,
					'settingDatelocale' 	=> $settingDatelocale,
					'settingDateformat' 	=> $settingDateformat,
					'allowEditUserid' 		=> true,
					'errors'				=> $errors,
					'activeTab'				=> $activeTab, // Activate this tab/pane on dialog open
					'orgChartFancyTreeData'	=> $fancyTreeData,
					'orgChartFancyTreePreselected' => $orgChartFancyTreePreselected,
					'isFirstLoading' => $isFirstLoading,
					'prePolupatedFields' => $prePolupatedFields,
					'isActiveNotificationOfType' => $isActiveNotificationOfType,
			), true,true);

			$this->sendJSON($result);
		}
	}

	public function actionUpdateAdditionalFields() {
		$selectedNodes = Yii::app()->request->getParam('selectedNodes', array());
		$additionalFields = CoreUser::model()->getVisibleAdditionalFieldsByBranch($selectedNodes);
		$response = new AjaxResult();
		$response->setStatus(true)->setData(array('fields' => $additionalFields))->toJSON();
	}

	public function actionEnrollNewUser($idst) {
		if (Yii::app()->request->isAjaxRequest) {

			$user = CoreUser::model()->findByPk($idst);

			$branchError = Yii::app()->request->getParam('branchError', false);
			$updateError = Yii::app()->request->getParam('updateError', false);

			if (isset($_POST['LearningCourse'])) {
				$idCourse = Yii::app()->request->getParam('idCourse');
				$course = LearningCourse::model()->findByPk($idCourse);
				if(!$course)
					$course = LearningCourse::model()->find('name = :name OR CONCAT(name, " - ", code) = :name', array(
							'name' => $_POST['LearningCourse']['name'],
					));
				$idSession = Yii::app()->request->getParam('sessionId', null);
				$courseType = Yii::app()->request->getParam('courseType', null);
				if ($course) {
                	$sessions = $course->getSessions(false);
					if($course->selling && CoreUserPU::isPUAndSeatManager()){
						if(!CoreUserPuCourse::checkIfPuHasEnoughSeatsInCourse(1, $idCourse)){
							$html = $this->renderPartial('_enrollNewUser', array('user' => $user, 'seatsError' => true, 'branchError' => $branchError), true);
							$this->sendJSON(array(
								'html' => $html,
								'modalClass' => 'enroll-new-user',
							));
						}
					}

                    // Course type check here is advised but not necessary due to Elearning type courses no having any sessions and thus failing the check
                    // Course type is not being registered as a variable if the text field is still being focused due to a JS problem with the DOM elements
                    if((is_null($idSession) || ($idSession == '')) && $sessions){
                        $html = $this->renderPartial('_enrollNewUser', array('user' => $user, 'enrollError' => true, 'branchError' => $branchError), true);
                        $this->sendJSON(array(
                            'html' => $html,
                            'modalClass' => 'enroll-new-user',
                        ));
                    }

					$enrollment = new LearningCourseuser;
					$idUser = Yii::app()->request->getParam('idst');
					if ($idSession && ($courseType == LearningCourse::TYPE_CLASSROOM || $courseType == LearningCourse::TYPE_WEBINAR)) {
						$idCourse = $course->idCourse;
						$this->enrollNewUser($courseType, $idUser, $idSession, $course->idCourse);
					}
					$result = $enrollment->subscribeUser($idst, $idUser, $course->idCourse, 0, LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);

					if($result){
						if(LearningCourse::isCourseSelling($idCourse) && CoreUserPU::isPUAndSeatManager()){
							CoreUserPU::modifyAvailableSeats($idCourse, -1);
						}
					}

					$result = new AjaxResult();
					$result->setStatus(true);
					$result->setData(array('res'=>"<script>$('#popup').modal('hide');</script>"))->toJSON();
					Yii::app()->end();
				}
			}



			$html = $this->renderPartial('_enrollNewUser', array('user' => $user, 'branchError' => $branchError), true);
			$this->sendJSON(array(
					'html' => $html,
					'modalClass' => 'enroll-new-user',
			));
		}
	}

	public function actionUserWarning() {
		if (Yii::app()->request->isAjaxRequest) {
			$html = $this->renderPartial('_updateUserWarning', array(), true);

			$this->sendJSON(array(
				'html' => $html,
				'modalClass' => 'enroll-new-user',
			));
		}
	}
    public function actionUnassignFromNode( $idst = null ) {
        if( !$idst ) $idst = $_REQUEST['idst'];

		if( !$idst || empty($idst) ) {
			Yii::log('No users selected to assign to nodes', CLogger::LEVEL_WARNING);
			$this->sendJSON(array(
					'success' => false
			));
		}

		$original_idst = $idst;
		if(is_string($idst)){
			if(stripos($idst, ',')!==false){
				// Case where $idst comes as a comma separated list
				$idst = explode(',', $idst);
			} else {
				$idst = array($idst);
			}
		}

		$default_with = array(
				'coreOrgChart' => array(
						'on' => 'coreOrgChart.lang_code = :lang',
						'params' => array('lang' => Settings::get('default_language', 'english'))
				)
		);
		$defaultOrgChartTree = CoreOrgChartTree::model()->with($default_with)->findAll();
		$defaultOrgChartTreeTranslation = array();
		foreach ( $defaultOrgChartTree as $node ) {
			$defaultOrgChartTreeTranslation[$node->idOrg] = $node->coreOrgChart->translation;
		}
		$currentNode =  Yii::app()->session['currentNodeId'];

		$coreOrgCartTree = CoreOrgChartTree::model()->findByPk($currentNode);

		$breadcrumbs = $this->getTreeBreadcrumbs($coreOrgCartTree, $defaultOrgChartTreeTranslation);

		$type = !empty( $_REQUEST['assign_type'] ) && $_REQUEST['assign_type'] == 'remove' ? 'remove' : '';

		$proceed = false;
		if ( $_REQUEST['idst'] != '' ) {
			$proceed = true;
		}
		$groups = array();
		if ( $type == 'remove' && $proceed )
		{
			$groups = array($coreOrgCartTree->idst_oc, $coreOrgCartTree->idst_ocd);

		}

		$this->renderPartial('unassign_users_from_node', array(
				'idst' => $idst,
				'groups' => $groups,
				'assignType' => $type,
				'currentNode' => $currentNode,
				'breadcrumbs' => $breadcrumbs
		));

		Yii::app()->end();
	}


	/**
	 * Unassign users from branch
	 */
	public function actionUnassignUsersFromNodeProgressive() {

		$chunkSize = 100;
		$offset = Yii::app()->request->getParam('offset', 0);
		$idst = Yii::app()->request->getParam('idst', '');
		$selectedUsers = explode(',', $idst); // All users
		$countUsers = count($selectedUsers);
		$groups = Yii::app()->request->getParam('groups', '');
		$selectedGroups = explode(',', $groups); // All groups

		$newOffset = $offset + $chunkSize;
		$start = ($offset == 0); // first run
		$stop = ($newOffset < $countUsers) ? false : true;

		if ( !isset($_POST['confirmDelete']) && $start ) {
			$this->sendJSON(array(
					'html'=>'<a class="auto-close"></a>'
			));
			return;
		}

		$tmpUsers = array();

		$counter = 0;
		for ($i = $offset; ($i < $newOffset && $i < $countUsers); $i++) {
			$counter ++;
			if ( $counter > $chunkSize ) break; // BAD
			$userId = ( isset( $selectedUsers[ $i ] ) ? $selectedUsers[ $i ] : false );
			if (!$userId || empty($userId))continue;
			$tmpUsers[] = $userId;
		}

		if ( $groups && $tmpUsers ) { /* Actual unassigning of users $tmpUsers from $groups*/
			CoreGroupMembers::model()->deleteAllByAttributes(array()," idstMember IN (".implode(',', $tmpUsers).") AND idst IN (".implode(',', $selectedGroups).") ");
			$deletedNodes = array();
			foreach($selectedGroups as $selectdeGroup){
				$deletedNodes[] = CoreOrgChartTree::getIdOrgByOcOrOcd($selectdeGroup);
			}
			$deletedNodes = array_unique($deletedNodes);
			if (($key = array_search(CoreOrgChartTree::getOrgRootNode()->idOrg, $deletedNodes)) !== false) {
				unset($deletedNodes[$key]);
			}
			foreach($tmpUsers as $tmpUser){
				foreach($deletedNodes as $deletedNode){
					Yii::app()->event->raise(CoreNotification::NTYPE_USER_REMOVED_FROM_BRANCH, new DEvent($this, array(
							'targetUser' => $tmpUser,
							'BranchIdOrg'  => $deletedNode
					)));
				}
			}
		}
		$completedPercent = $countUsers==0 ? 100 : $i / $countUsers*100;
		if($completedPercent>100) $completedPercent = 100;
		if($completedPercent<0) $completedPercent = 0;

		$html =  $this->renderPartial('_unassign_users_from_branch_progress', array(
				'offset' 				=> $offset,
				'processedUsers' 		=> $i,
				'totalUsers' 			=> $countUsers,
				'completedPercent' 		=> intval($completedPercent),
				'start' 				=> ($offset == 0),
				'stop' 					=> $stop,
		), true, true);

		$data = array(

				'offset'    => $newOffset,
				'idst'      => $idst,
				'groups'    => $groups,
				'count'     => $countUsers,
				'stop'      => $stop,
		);

		$response = new AjaxResult();
		$response->setStatus(true);
		$response->setHtml($html)->setData($data)->toJSON();

		Yii::app()->end();
	}


	public function actionAssignToNode($idst = null){
		if(!$idst) $idst = $_REQUEST['idst'];

		// Type can be either "move" or "append"
		// where the first will move the selected users to the
		// new selected branches, deleting all old branches assignment
		// whereas the second will append to existing branches assigned
		// to the users
		$type = isset($_REQUEST['assign_type']) && $_REQUEST['assign_type']=='move' ? 'move' : 'append';

		if(!$idst || empty($idst)){
			Yii::log('No users selected to assign to nodes', CLogger::LEVEL_WARNING);
			$this->sendJSON(array(
					'success'=>false
			));
		}

		if(is_string($idst)){
			if(stripos($idst, ',')!==false){
				// Case where $idst comes as a comma separated list
				$idst = explode(',', $idst);
			}else{
				$idst = array($idst);
			}
		}

		if (isset($_POST['select-orgchart'])) {
			// Form submission

			// Collect IDs of org chart nodes selected
			$selectedNodes = array();
			foreach ($_POST['select-orgchart'] as $nodeId => $selectState) {
				if($selectState)
					$selectedNodes[] = $nodeId;
			}

			// Assign those users to the selected nodes
			if($type=='append' || $type == 'move'){
				// Append selected nodes to existing nodes assigned to the user

				// Make sure idst array coming from user input
				// contains only valid user IDs from the platform
				$idst = Yii::app()->getDb()->createCommand()
						->select('idst')
						->from(CoreUser::model()->tableName())
						->where(array('IN', 'idst', $idst))
						->queryColumn();

				$nodes = Yii::app()->getDb()->createCommand()
						->select('idOrg, idst_oc, idst_ocd')
						->from(CoreOrgChartTree::model()->tableName())
						->where(array('IN', 'idOrg', $selectedNodes))
						->queryAll(true);

				$membershipGroupsToAdd = array();
				foreach($nodes as $node){
					$membershipGroupsToAdd[] = $node['idst_oc'];
					$membershipGroupsToAdd[] = $node['idst_ocd'];
				}

				$params = array();
				$groupsSqlQueryBuilding = array();
				if(count($membershipGroupsToAdd)){
					foreach($idst as $userId){
						foreach($membershipGroupsToAdd as $group){
							$group = (int) $group;
							$userId = (int) $userId;

							// Take advantage of the param binding
							// since user IDs come from user input
							// to avoid SQL injections
							$params[':userId_'.$userId] = $userId;
							$params[':groupId_'.$group] = $group;
							$groupsSqlQueryBuilding[] = "(:groupId_{$group}, :userId_{$userId})";
						}
					}
				}

				// Yii doesn't provide a out-of-the box way to do
				// INSERT IGNORE/INSERT...ON DUPLICATE KEY UPDATE
				// so we have to use a regular SQL command built manually
				// We could easily switch this to AR/DAO based approach
				// but we would lose the single-query insert and would
				// stress the server too much ("for each group and user selected
				// insert a row in core_group_members", which would mean
				// [selected users] X [selected groups] insertions...
				// not good for AR)

				// @TODO in case this fails for HUGE selections,
				// use array_slice($groupsSqlQueryBuilding) and split/run the command for
				// each N entries (e.g. split into chunks of 500 insertions).
				// Remember to split the SQL params array as well since just passing it
				// fully may produce errors about missmatch between param count and param values count
				if(count($groupsSqlQueryBuilding)){
					//$groupsSqlQueryBuilding = array_slice($groupsSqlQueryBuilding, 0, $insertElemsPerChunk);
					//$params = array_slice($params, 0, $insertElemsPerChunk*2); // we have 2 params per insertion
					Yii::app()->getDb()->getCommandBuilder()
							->createSqlCommand('INSERT IGNORE INTO '.CoreGroupMembers::model()->tableName().' (`idst`, `idstMember`) VALUES '.implode(',', $groupsSqlQueryBuilding), $params)
							->execute();
				}

				if($type == 'move')
				{
					//Removing the users from the old nodes
					//Add the oc_0 and ocd_0 to the exclusion list
					$_roots = CoreOrgChartTree::model()->roots()->findAll();
					$rootNode = $_roots[0];
					$membershipGroupsToAdd[] = $rootNode->idst_oc;
					$membershipGroupsToAdd[] = $rootNode->idst_ocd;

					$nodes = Yii::app()->getDb()->createCommand()
							->select('idOrg, idst_oc, idst_ocd')
							->from(CoreOrgChartTree::model()->tableName())
							->queryAll(true);
					$all_nodes = array();
					foreach($nodes as $node)
					{
						$all_nodes[] = $node['idst_oc'];
						$all_nodes[] = $node['idst_ocd'];
					}


					CoreGroupMembers::model()->deleteAllByAttributes(array('idstMember' => $idst), "idst IN (".implode(',', $all_nodes).") AND idst NOT IN (".implode(',', $membershipGroupsToAdd).")");
				}
			}else{
				throw new CException('Invalid org chart assignment "type" param');
			}

			$this->sendJSON(array(
					'success' => true,
			));
		}

		// Get the currently selectoed org chart node from session and "preselect it" in Branch Tree (fancytree)
		$orgChartFancyTreePreselected = array();
		if (isset(Yii::app()->session['currentNodeId']) && ( (int) Yii::app()->session['currentNodeId'] > 0 ) && $type == 'append') {
			$orgChartFancyTreePreselected[] = array('key' => (int) Yii::app()->session['currentNodeId'], 'selectState' => 1);
		}

		$fancyTreeData = CoreOrgChartTree::buildFancytreeDataArray(false, true, true, true, true);

		$html = $this->renderPartial('assign_users_to_node', array(
				'users' => $idst,
				'assignType' => $type,
				'orgChartFancyTreeData'	=> $fancyTreeData,
				'orgChartFancyTreePreselected' => $orgChartFancyTreePreselected,
		), true);
		$this->sendJSON(array(
				'html' => $html,
				'modalClass' => 'assign-to-node',
		));
	}

	/**
	 * Update user
	 *
	 * @param null $idst
	 */
	public function actionUpdateUser($idst = null) {
		// todo i/ probably need to refactor this action !!!
		//NOTE: the massive editing action has been moved into new method actionUpdateUserMassive() and it is no more handled here

		$errors = array();
		$activeTab = 'userForm-details';
		$branchError = false;

		if (Yii::app()->request->isAjaxRequest) {
			if (($idst === null) && isset($_POST['idst'])) {
				$idst = $_POST['idst'];
				if(is_string($idst))
				{
					if(stripos($idst, ',')!==false)
						$idst = explode(',', $idst);
					else
						$idst = array($idst);
				}
			}

			$result = array();
			$result['success'] = true;
			$result['html'] = '';

			$massiveUpdate = false;

			//prevent power users from setting users (or themselves) to admin or other level
			if(!Yii::app()->user->getIsGodadmin() && (isset($_POST['CoreGroupMembers']['idst']))) {
				unset($_POST['CoreGroupMembers']['idst']);
			}

			if (is_array($idst)) {
				$newUserModel = new CoreUser('massiveUpdate');
				$newUserModel->unsetAttributes();

				if (isset($_POST['userFields'])) {
					foreach ($_POST['userFields'] as $key => $field) {
						if ($key == 'new_password') {
							$newUserModel->new_password_repeat = $_POST['CoreUser']['new_password'];
						}
						$newUserModel->$key = $_POST['CoreUser'][$key];
					}
				}

				$newGroupMembersModel = new CoreGroupMembers('massiveUpdate');
				$newGroupMembersModel->unsetAttributes();
				if (isset($_POST['groupFields'])) {
					foreach ($_POST['groupFields'] as $key => $field) {
						$newGroupMembersModel->$key = $_POST['CoreGroupMembers'][$key];
					}
				}
				$massiveUpdate = true;
			}
			$users = CoreUser::model()->findAllByPk($idst);

			if (empty($users)) {
				$result['success'] = false;
				$result['status'] = 'not-found';
				$result['html'] = 'User not found';
				$this->sendJSON($result);
			}

			foreach ($users as $user) {
				if ($massiveUpdate) {
					$user->scenario = 'massiveUpdate';
				} else {
					$user->scenario = 'update';
				}

				// model for admin level
				$groupMember = CoreGroupMembers::model()->findAdminLevel($user->idst);
				$oldUserGroup = $groupMember->group;
				if ($groupMember === null) {
					$groupMember = new CoreGroupMembers();
					$groupMember->idstMember = $user->idst;
				}
				$groupMember->scenario = 'update';

				// model for user language
				$settingUser = CoreSettingUser::model()->findByAttributes(array(
						'id_user' => $user->idst,
					//'value' => $user->language->lang_code,
						'path_name' => 'ui.language',
				));

				if ($settingUser === null) {
					$settingUser = new CoreSettingUser();
					$settingUser->id_user = $user->idst;
					$settingUser->path_name = 'ui.language';
					//$settingUser->value =  $user->language->lang_code;
					$settingUser->value =  CoreUser::getDefaultLangCode();
				}
				$settingUser->scenario = 'update';

				// model for user timezone
				$settingDateformat = null;
				$settingDatelocale = null;

				// is the user allowed to set the date format?
				if ('user_selected' === Settings::get('date_format')) {
					$settingDateformat = CoreSettingUser::model()->findByAttributes(array(
							'id_user' => $user->idst,
							'path_name' => 'date_format',
					));
					if ($settingDateformat === null) {
						$settingDateformat = new CoreSettingUser();
						$settingDateformat->id_user = $user->idst;
						$settingDateformat->path_name = 'date_format';
					}
					$settingDateformat->scenario = 'update';

					$settingDatelocale = CoreSettingUser::model()->findByAttributes(array(
							'id_user' => $user->idst,
							'path_name' => 'date_format_locale',
					));
					if ($settingDatelocale === null) {
						$settingDatelocale = new CoreSettingUser();
						$settingDatelocale->id_user = $user->idst;
						$settingDatelocale->path_name = 'date_format_locale';
						//$settingDatelocale->value = Yii::app()->getLanguage();
					}
					$settingDatelocale->scenario = 'update';
				}


				if ((isset($_POST['CoreUser']) && (isset($_POST['CoreGroupMembers']) || ($user->idst == Yii::app()->user->id)) && isset($_POST['CoreSettingUser'])) || ($massiveUpdate && ($_POST['userFields'] || $_POST['groupFields']))) {
					if(!$massiveUpdate)
						$result['success'] = false;

					if ($massiveUpdate) {
						if ($newUserModel->validate()) {
							if (isset($_POST['userFields'])) {
								foreach ($_POST['userFields'] as $key => $field) {
									$user->$key = $_POST['CoreUser'][$key];
									if ($massiveUpdate && $key == 'new_password') {
										$user->new_password_repeat = $_POST['CoreUser']['new_password'];
									}
								}
							}
							if (isset($_POST['groupFields'])) {
								foreach ($_POST['groupFields'] as $key => $field) {
									if (($field != 'idst') && (Yii::app()->user->id != $user->idst)) {
										$groupMember->$key = $newGroupMembersModel->$key;
									}
								}
							}
						}
					} else {
						// Purify firstname & lastname
						$_POST['CoreUser']['firstname'] = trim($_POST['CoreUser']['firstname']);
						$_POST['CoreUser']['lastname'] = trim($_POST['CoreUser']['lastname']);
						$_POST['CoreUser']['userid'] = trim(strip_tags($_POST['CoreUser']['userid']));
						$_POST['CoreUser']['email'] = trim($_POST['CoreUser']['email']);
						$_POST['CoreUser']['email_status'] = trim($_POST['CoreUser']['email_status']);
						$oldEmail = $user->email;

						$user->attributes = $_POST['CoreUser'];

						if ($user->expiration == '') $user->expiration = null;

						if (isset($_POST['sendEmailBox']) && $_POST['sendEmailBox'] == 1 && $_POST['CoreUser']['email_status'] == 0) {
							if ($user->setVerificationLink())
								Yii::app()->event->raise('UserEmailVerified', new DEvent($this, array('user' => $user)));
						}

						if($user->email != $oldEmail){
							$user->email_status = 0;
						}

						// Sanitize also additional fields
						$postedAdditionalFields = array();
						if (isset($_POST['CoreUser']['additional']) && is_array($_POST['CoreUser']['additional'])) {
							foreach($_POST['CoreUser']['additional'] as $attrKey => $attrValue) {
								$postedAdditionalFields[$attrKey] = $attrValue;
								if (is_string($attrValue['value'])) {
									$postedAdditionalFields[$attrKey]['value'] = trim($attrValue['value']);
								}
							}
						}
						$user->setAdditionalFieldValues($postedAdditionalFields, $_POST['FileUpload']);

						// !!!!! Handle Fancytree based style of data submition, where selected branches are sent in the form of
						// select-orgchart[<node id>] = <select state>
						$previousGroups = $user->getOrgChartGroupsList();
						if (($key = array_search(CoreOrgChartTree::getOrgRootNode()->idOrg, $previousGroups)) !== false) {
							unset($previousGroups[$key]);
						}
						if (isset($_POST['select-orgchart'])) {
							foreach ($_POST['select-orgchart'] as $nodeId => $selectState) {
								$user->chartGroups[] = $nodeId;
							}
						}

						$branchError = $user->checkBranchError();


						$groupMember->attributes = $_POST['CoreGroupMembers'];

						if (isset($_POST['CoreSettingUser'])) {

							foreach ($_POST['CoreSettingUser'] as $path_name => $value) {
								switch ($path_name) {
									case 'ui.language':
										$settingUser->value = $value;
										$settingUser->save();
										break;
									case 'date_format':
										//format timestamp and date attributes to UTC before change the date format
										$timestampAttributesUtc = $dateAttributesUtc = array();
										foreach($user->timestampAttributes as $attr) {
											list($attr, $dateWidth, $timezoneFlag) = explode(' ', $attr);
											$timestampAttributesUtc[$attr] = Yii::app()->localtime->fromLocalDateTime($user->$attr);
										}
										foreach($user->dateAttributes as $attr) {
											list($attr, $dateWidth, $timezoneFlag) = explode(' ', $attr);
											$dateAttributesUtc[$attr] = Yii::app()->localtime->fromLocalDate($user->$attr);
										}

										//change date format
										$settingDateformat->value = $value;
										$settingDateformat->save();

										//format timestamp and date attributes to local dateTime/date with the NEW format
										foreach($timestampAttributesUtc as $attr => $utcValue) {
											$user->$attr = Yii::app()->localtime->toLocalDateTime($utcValue);
										}
										foreach($dateAttributesUtc as $attr => $utcValue) {
											$finalAttrValue = (empty($utcValue) || $utcValue == '0000-00-00' ? null : Yii::app()->localtime->toLocalDate($utcValue));
											$user->$attr = $finalAttrValue;
										}

										break;
									case 'date_format_locale':
										$settingDatelocale->value = $value;
										$settingDatelocale->save();
										break;
									default: break;
								}
							}
						}


					}

					if (!$massiveUpdate || ($massiveUpdate && $newUserModel->validate())) {

						$ok = CoreUser::validatePowerAdminChartSelection($user->chartGroups, true);
						if (!$ok) {
							$errors[] = Yii::t('user_management', 'You are required to select a branch');
							$activeTab = 'userForm-orgchart'; // Activate this tab/pane on dialog open
						}

						// todo i/ probably need to create constant with 'ui.language' etc.
						$settingUser->path_name = 'ui.language';

						// Prevent Super Admins from EVER having an "expiration date". No exceptions
						$godadminGroupId = Yii::app()->getDb()->createCommand()->select('idst')->from('core_group')->where('groupid=:superAdmin', array(':superAdmin'=>Yii::app()->user->getGodadminLevelLabel()))->queryScalar();
						if($godadminGroupId && $godadminGroupId==$groupMember->idst){
							Yii::log('Removing expiration date field for Super Admin '. $user->idst .'. Super admins are always excluded from expiration dates', CLogger::LEVEL_WARNING);
							$user->expiration = null;
						}

						if ($ok && $user->validate() && $groupMember->validate() && $settingUser->validate()) {
							$saved = $user->save(false);
							$saveOrgChart = ($massiveUpdate ? true : $user->saveChartGroups());
							//in case of massive editing, we do not have to change orgchart assignments for users, just
							//leave as they are without executing "saveChartGroups()" function

							$newNodes       =   array_diff($user->chartGroups,$previousGroups);
							if (($key = array_search(CoreOrgChartTree::getOrgRootNode()->idOrg, $newNodes)) !== false) {
								unset($newNodes[$key]);
							}
							$deletedNodes    =   array_diff($previousGroups,$user->chartGroups);
							if (($key = array_search(CoreOrgChartTree::getOrgRootNode()->idOrg, $newNodes)) !== false) {
								unset($newNodes[$key]);
							}
							foreach($newNodes as $newNode){
								Yii::app()->event->raise(EventManager::EVENT_USER_ASSIGNED_TO_BRANCH, new DEvent($this, array(
										'targetUser' => $user->idst,
										'BranchIdOrg'  => $newNode
								)));
							}
							foreach($deletedNodes as $deletedNode){
								Yii::app()->event->raise(EventManager::EVENT_USER_REMOVED_FROM_BRANCH, new DEvent($this, array(
										'targetUser' => $user->idst,
										'BranchIdOrg'  => $deletedNode
								)));
							}

							if ($saved && $saveOrgChart) {
								// Raise event
								Yii::app()->event->raise('UserModified', new DEvent($this, array(
										'user' => $user,
								)));
								// save user level - update 'core_group_members' db table
								$groupMember->save(false);

								// Sync with the Hydra RBAC
								HydraRBAC::manageUserToRole(array( 'user' => $user->getPrimaryKey(), 'group' => $groupMember->idst ));

								//clear power user data if change user level from power user to any other level
								if ($oldUserGroup->groupid == Yii::app()->user->level_admin && $oldUserGroup->idst != $groupMember->idst) {
									$user->cleanUpPowerUserData();
								}

								// get the new user level/core_group
								$newUserGroup = CoreGroup::getGroup($groupMember->idst, FALSE);

								// if the user level is changed to 'Power User'
								// then we need to assign to power user himself
								// NOTE: the db table, that corresponds to this is 'core_admin_tree'
								// and on every login this info is stored inside 'core_user_pu' db table
								// Please, note that we should avoid changes to 'core_user_pu' db table
								if (isset($newUserGroup->groupid) && $newUserGroup->groupid == Yii::app()->user->level_admin) {
									// Assign this power user to himself. if not yet assigned
									// This is to allow Power User to manage himself.
									$puModel = CoreAdminTree::model()->findByAttributes(array('idst' => $user->idst, 'idstAdmin' => $user->idst));
									if (!$puModel) {
										CoreAdminTree::addPowerUserMember($user->idst, $user->idst, false);  // false: do NOT update internally!!!!!!!
									}
								}

								$settingUser->save(false);
								$result['status'] = 'saved';
							}
						}
						else {
							$validationErrors = array_merge($user->errors, $groupMember->errors, $settingUser->errors);
							Yii::log('Update user process (single or massive): some user validation failed:', 'error');
							Yii::log(print_r($validationErrors,true), 'error');
						}
					}
				}
				$user->userid = Yii::app()->user->getRelativeUsername($user->userid);
				if (!$massiveUpdate) {
					$singleUser = $user;
				}
			}

			if (!isset($_POST['userFields']) && $massiveUpdate) {
				// for popup closing
				$result['status'] = 'saved';
			}

			$isActiveNotificationOfType = CoreNotification::isActiveNotificationOfType(CoreNotification::NTYPE_USER_EMAIL_MUST_VERIFY);
			if ($massiveUpdate) {
				$result['success'] = true;
				$result['html'] .= $this->renderPartial('_userMassiveUpdate', array(
						'user' => $newUserModel,
						'groupMember' => $newGroupMembersModel,
						'users' => $idst,
						'isActiveNotificationOfType' => $isActiveNotificationOfType,
						'errors' => $errors, // Overall form errors, not model related
				), true);
				$this->sendJSON($result);
			} else {
				// only admins can edit the user id
				//$allowEditUserid = (!Yii::app()->user->getIsUser() && Yii::app()->user->getIdst() != $singleUser->idst);
				$allowEditUserid = (Yii::app()->user->isGodAdmin
						|| (Yii::app()->user->isAdmin && Yii::app()->user->checkAccess('/framework/admin/usermanagement/mod')));


				// Build fancytree specific PRE-selected array of assigned Branches
				$orgChartFancyTreePreselected = array();
				if (isset($_POST['select-orgchart'])) {
					foreach ($_POST['select-orgchart'] as $nodeId => $selectState) {
						$orgChartFancyTreePreselected[] = array('key' => $nodeId, 'selectState' => $selectState);
					}
				} else {
					$userOrgChartGroups = $singleUser->getOrgChartGroupsList();
					if (is_array($userOrgChartGroups) && (count($userOrgChartGroups) > 0)) {
						$orgRootNode = CoreOrgChartTree::getOrgRootNode();
						foreach ($userOrgChartGroups as $idOrg) {
							if ($idOrg != $orgRootNode->idOrg) {
								$orgChartFancyTreePreselected[] = array("key" => $idOrg, "selectState" => 1);
							}
						}
					}
				}

				if(isset($result['status']) && ($result['status'] == 'saved') && (!$massiveUpdate) && $branchError){
						$this->redirect(Yii::app()->createUrl($this->id.'/UserWarning', array()));
				}

				$fancyTreeData = CoreOrgChartTree::buildFancytreeDataArray(false, true, true , true, true);

				$result['html'] .= $this->renderPartial('_userForm', array(
						'user' => $singleUser,
						'groupMember' => $groupMember,
						'settingUser' => $settingUser,
						'settingDatelocale' => $settingDatelocale,
						'settingDateformat' => $settingDateformat,
						'allowEditUserid' => $allowEditUserid,
						'errors'			=> $errors, 	// Overall form errors, not model related
						'activeTab'			=> $activeTab,  // Tab pane to acivate on dialog open
						'orgChartFancyTreeData'	=> $fancyTreeData,
						'orgChartFancyTreePreselected'	=> $orgChartFancyTreePreselected,
						'isActiveNotificationOfType'	=> $isActiveNotificationOfType,
				), true, true);

				$user->removeUploadsOnRequested();
				$this->sendJSON($result);
			}
		}
	}



	public function actionGetCustomDateFormats()
	{

		if (Yii::app()->request->isAjaxRequest) {
			$form = new CActiveForm();
			$idUser = Yii::app()->request->getParam('idUser');
			$locale = Yii::app()->request->getParam('code');

			$settingDatelocale = CoreSettingUser::model()->findByAttributes(array(
					'id_user' => $idUser,
					'path_name' => 'date_format',
			));
			if (!$settingDatelocale) {
				$settingDatelocale = new CoreSettingUser();
			}

			$data = ($locale)
					? Yii::app()->localtime->getDateTimeFormatsArray($locale)
					: array();

			$dropdown = $form->dropDownList($settingDatelocale, 'value', $data, array('size' => 1, 'name' => 'CoreSettingUser[date_format]', 'prompt' => Yii::t('configuration', 'Select a date format...')));

			$this->sendJSON(array(
					'success' => true,
					'html' => $dropdown
			));
		}
	}



	/**
	 * @param $idst Primary Keys of users
	 * @return string String of json code with success and message
	 */
	public function actionDeleteUser($idst = null) {
		$users = array();
		if (!empty($_POST['idst'])) {
			$idst = $_POST['idst'];
			if(is_string($idst))
			{
				if(stripos($idst, ',')!==false)
					$users = explode(',', $idst);
				else
					$users = array($idst);
			}
		} elseif(!empty($idst)) {
			$users = array($idst);
		}

		if (empty($users)) {
			$this->sendJSON(array());
		}

		if (!empty($_POST['CoreUser'])) {
			if ($_POST['CoreUser']['confirm']) {
				if(stripos($idst, ',')!==false) {
					$html = $this->renderPartial('_mass_user_delete', array(
							'idst' => $idst, // Build string with the all users ids
					), true);

					$this->sendJSON(array('html'=>$html));
					Yii::app()->end();
				} else {
					$user = CoreUser::model()->findByPk($idst);
					// Only goaddmins and power users can do this action.
					// Additionally, power users can't delete godadmins
					if ($user && Yii::app()->user->id != $user->idst && (Yii::app()->user->getIsGodadmin() || !CoreUser::isUserGodadmin($user->idst)) && Settings::get('whitelabel_menu_userid', '') != $user->idst) {
						// delete one time message /if exists/
						Yii::app()->db->createCommand()
								->delete(CoreSettingUser::model()->tableName(),
										'path_name="one_time_flash_msg" AND id_user = :id', array(':id' => $user->idst));
						Log::_('DELETION: User with id '.$user->idst.' deleted by user '.Yii::app()->user->id, CLogger::LEVEL_WARNING);
						$user->delete();

						// Sync with the Hydra RBAC
						HydraRBAC::manageUserToRole(array( 'user' => $user->idst, 'group' => null ));
					}
					$this->sendJSON(array());
				}
			}
		}
		if (Yii::app()->request->isAjaxRequest) {
			$content = $this->renderPartial('delete', array(
					'model' => new CoreUser(),
					'users' => $users,
					'idst' => $idst
			), true);
			$this->sendJSON(array(
					'html' => $content,
			));
		}
		Yii::app()->end();
	}

	public function actionAxProgressiveDeleteUsers() {
		set_time_limit(0);
		$chunkSize = 10;
		$tmpUsers = array();
		$idst = $_POST['idst'];

		// Check if this is string
		if(is_string($idst))
		{
			$userList = explode(',', $idst);
		}
		else{
			$this->sendJSON(array());
		}

		// Get current offset and calculate stop flag
		$offset = Yii::app()->request->getParam('offset', 0);
		$totalCount = count($userList);
		$newOffset = $offset + $chunkSize;

		$stop = ($newOffset < $totalCount) ? false : true; // last run

		$counter = 0;
		for ($index = $offset; ($index < $newOffset && $index < $totalCount); $index++) { //NOTE: $newOffset may be greater than $totalCount, but we wouldn't count over $totalCount
			$counter ++;
			if ( $counter > $chunkSize ) break; //NOTE: this shouldn't happen

			$userId = ( isset( $userList[ $index ] ) ? $userList[ $index ] : false );
			if (!$userId || empty($userId))continue;
			$tmpUsers[] = $userId;
		}

		$coreUsers = CoreUser::model()->findAllByAttributes(array('idst' => $tmpUsers));
		foreach ($coreUsers as $user) {
			// Only goaddmins and power users can do this action.
			// Additionally, power users can't delete godadmins
			if (Yii::app()->user->id != $user->idst && (Yii::app()->user->getIsGodadmin() || !CoreUser::isUserGodadmin($user->idst)) && Settings::get('whitelabel_menu_userid', '') != $user->idst) {
				// delete one time message /if exists/
				Yii::app()->db->createCommand()
						->delete(CoreSettingUser::model()->tableName(),
								'path_name="one_time_flash_msg" AND id_user = :id', array(':id' => $user->idst));
				Log::_('DELETION: User with id '.$user->idst.' deleted by user '.Yii::app()->user->id, CLogger::LEVEL_WARNING);
				$user->delete();

				// Sync with the Hydra RBAC
				HydraRBAC::manageUserToRole(array( 'user' => $user->idst, 'group' => null ));
			}
		}

		$completedPercent = $totalCount==0 ? 100 : $index/$totalCount*100;
		if($completedPercent>100) $completedPercent = 100;
		if($completedPercent<0) $completedPercent = 0;

		$html = $this->renderPartial('_progressive_delete_users', array(
				'offset' 				=> $offset,
				'processedUsers' 		=> $index,
				'totalUsers' 			=> $totalCount,
				'completedPercent' 		=> intval($completedPercent),
				'start' 				=> ($offset == 0),
				'stop' 					=> $stop,
		), true, true);

		// Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
		// These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stopping when $stop == true)
		$data = array(
				'offset'				=> $newOffset,
				'stop'					=> $stop,
				'idst'					=> $idst,
		);

		$response = new AjaxResult();
		$response->setStatus(true);
		$response->setHtml($html)->setData($data)->toJSON();
		Yii::app()->end();
	}

	public function actionChangeStatus() {

		$status = Yii::app()->request->getParam('status');
		$idst = Yii::app()->request->getParam('idst');
		$username = '';

		$countIltSession = 0;
		$countWebinarSessions = 0;

		$countUsers = Yii::app()->db->createCommand()
				->select('COUNT(idst)')
				->from(CoreUser::model()->tableName())
				->where( array('in', 'idst',  explode(',', $idst) ) )
				->queryScalar();

		if (empty($countUsers)) {
			$this->sendJSON(array(
					'status' => 'error', // success|error
					'html' => Yii::t('standard', '_OPERATION_FAILURE') .
							"<script>$('.modal.btn-submit').remove();</script>",
			));
		}

		if($countUsers == 1) {
			$user = CoreUser::model()->findByPk($idst);
			if($user)
				$username = Yii::app()->user->getRelativeUsername($user->userid);


			$data = $this->getFutureSessionsCount($idst);

			$countIltSession = $data['classroomSessionsCount'];
			$countWebinarSessions = $data['webinarSessionsCount'];


		} else{
			$data = $this->getFutureSessionsCount($idst);

			$countIltSession = $data['classroomSessionsCount'];
			$countWebinarSessions = $data['webinarSessionsCount'];
		}

		if (!empty($_POST['CoreUser'])) {
			if ($_POST['CoreUser']['confirm']) {

				$unenrollIltSessions = Yii::app()->request->getParam('unenroll_ilt_sessions', 0);
				$unenrollWebinarSessions = Yii::app()->request->getParam('unenroll_webinar_sessions', 0);
				$unenrollAllUsers = Yii::app()->request->getParam('unenroll_users_from_sessions', 0);

				$html = $this->renderPartial('_mass_user_change_status', array(
						'idst' => $idst, // Build string with the all users ids
						'status' => $status,
						'unenrollIltSessions' => $unenrollIltSessions,
						'unenrollWebinarSessions' => $unenrollWebinarSessions,
						'unenrollAllUsers' => $unenrollAllUsers
				), true);

				$this->sendJSON(array('html'=>$html));
				Yii::app()->end();
			}
		}
		if (Yii::app()->request->isAjaxRequest) {
			$content = $this->renderPartial('changeStatus', array(
					'model' => new CoreUser(),
					'countUsers' => $countUsers,
					'status' => $status,
					'idst' => $idst,
					'username' => $username,
					'countIltSessions' => $countIltSession,
					'countWebinarSessions' => $countWebinarSessions
			), true);
			$this->sendJSON(array(
					'html' => $content,
			));
		}
		Yii::app()->end();
	}

	public function actionAxProgressiveChangeStatus() {
		$chunkSize = 200;
		$userIdsThisChunk = array();
		$idst = $_POST['idst'];
		$status = $_POST['status'];
		$unenrollWebinarSessions = isset($_POST['unenrollWebinarSessions']) ? $_POST['unenrollWebinarSessions'] : false;
		$unenrollIltSessions = isset($_POST['unenrollIltSessions']) ? $_POST['unenrollIltSessions'] : false;
		$unenrollAllUsers = isset($_POST['unenrollAllUsers']) ? $_POST['unenrollAllUsers'] : false;
		$collectedIgnoredExpiredUserIds = Yii::app()->getRequest()->getParam('ignored_expired_idst');

		// Check if this is string
		if(is_string($idst))
		{
			$userList = explode(',', $idst);
		}
		else{
			$this->sendJSON(array());
		}

		$collectedIgnoredExpiredUserIds = $collectedIgnoredExpiredUserIds ? explode(',', $collectedIgnoredExpiredUserIds) : array();

		// Get current offset and calculate stop flag
		$offset = Yii::app()->request->getParam('offset', 0);
		$totalUsersCount = count($userList);
		$newOffset = $offset + $chunkSize;

		$isLastStep = ($newOffset < $totalUsersCount) ? false : true; // last run

		$i = 0;
		for ($index = $offset; ($index < $newOffset && $index < $totalUsersCount); $index++) { //NOTE: $newOffset may be greater than $totalCount, but we wouldn't count over $totalCount
			$i ++;
			if ( $i > $chunkSize ) break; //NOTE: this shouldn't happen
			// Get current enrollment "job". If we are over the limit, invalidate value and stop this iteration
			$userId = ( isset( $userList[ $index ] ) ? $userList[ $index ] : false );
			if (!$userId || empty($userId)) continue;
			$userIdsThisChunk[] = $userId;
		}

		// Set new values for the columns in core_user
		$newColumnValues = array('valid' => $status);
		if($status == CoreUser::STATUS_VALID) {
			$newColumnValues['suspend_date'] = NULL;
		} else {
			$newColumnValues['suspend_date'] = Yii::app()->localtime->getUTCNow();
		}

		// Prepare building the condition that will
		// match which users will be affected by this operation
		// (the criteria should match only user IDs from this
		// progressive chunk)
		$condition = ' idst != :currentUser AND idst IN ('.implode(',', $userIdsThisChunk).') ';

		// power user can NOT modify the status of super admins
		// (super admins can modify other super admins though)
		if(Yii::app()->user->getIsAdmin()) {
			$godadminsAllIds = Yii::app()->user->getAllGodadmins(true);
			$condition .= ' AND idst NOT IN (' . implode( ',', $godadminsAllIds ) . ') ';
		}

		$params = array(
				':currentUser' => Yii::app()->user->id
		);

		// Collect only user IDs that DO NOT already
		// have the new status yet (and only update them after that)
		$userIdsWhichWillBeAffected = Yii::app()->getDb()->createCommand()
			->selectDistinct('idst')
			->from('core_user')
			->where($condition, $params)
			// only affect users which are not expired
			->andWhere('expiration IS NULL OR expiration="" OR expiration="0000-00-00" OR DATE(expiration) >= DATE(NOW())')
			->queryColumn();

		// Get ALL users that match this chunk (both expired and not)
		$userIdsMatchingThisChunk = Yii::app()->getDb()->createCommand()
			->selectDistinct('idst')
			->from('core_user')
			->where($condition, $params)
			->queryColumn();

		$expiredUsersToBeIgnored = Yii::app()->getDb()->createCommand()
			->select('idst, userid, firstname, lastname')
			->from('core_user')
			->where(array('IN', 'idst', array_merge($userIdsMatchingThisChunk, $collectedIgnoredExpiredUserIds)))
			->andWhere('expiration != "0000-00-00" AND DATE(expiration) < DATE(NOW())')
			->group('idst')
			->order('idst')
			->queryAll();
		// Update the "global" array of ignored user IDs
		// that is passed between progressive chunks and keeps
		// a list of user IDs which are expired and have been
		// ignored from the suspension/unsuspension operation for that reason
		$collectedIgnoredExpiredUserIds = array();
		foreach($expiredUsersToBeIgnored as $row){
			$collectedIgnoredExpiredUserIds[] = $row['idst'];
		}

		// Get all user models for which we are changing the status for
		// suspended=>unsuspended or unsuspended=>suspended
		// Models are generally not recommended, but they are needed
		// by the Yii events below and this is the reason why we fetch them here
		// Besides, it's a progressive action, so they are not that much bottleneck
		$c = new CDbCriteria();
		$c->addInCondition('idst', $userIdsWhichWillBeAffected);
		$c->addCondition('valid <> :newStatus');
		$c->params[':newStatus'] = $status;
		$oldUserModels = CoreUser::model()->findAll($c);

		// Update the status of the users which don't have this new status yet
		Yii::app()->db->createCommand()
				->update(
					CoreUser::model()->tableName(),
					$newColumnValues,
					array('IN', 'idst', $userIdsWhichWillBeAffected));

		// Raise an event for each user we are changing the status of (suspended/unsuspended or vice versa)
		foreach($oldUserModels as $userModel){
			if($status==CoreUser::STATUS_VALID){
				Yii::app()->event->raise(CoreNotification::NTYPE_USER_UNSUSPENDED, new DEvent($this, array('user'=>$userModel)));
			}elseif($status==CoreUser::STATUS_NOTVALID){
				Yii::app()->event->raise(CoreNotification::NTYPE_USER_SUSPENDED, new DEvent($this, array('user'=>$userModel)));
			}
		}

		if($unenrollAllUsers || $unenrollWebinarSessions || $unenrollIltSessions){

			$condition = "id_user IN (" . implode(',', $userIdsThisChunk) . ')';

			if($unenrollAllUsers){
				Yii::app()->db->createCommand()
						->delete(LtCourseuserSession::model()->tableName(), $condition);

				Yii::app()->db->createCommand()
						->delete(WebinarSessionUser::model()->tableName(), $condition);

				$command2 = Yii::app()->db->createCommand()
						->select('idCourse')
						->from(LearningCourse::model()->tableName())
						->where('course_type = :classroom or course_type = :webinar', array(
								':classroom' => LearningCourse::TYPE_CLASSROOM,
								':webinar' => LearningCourse::TYPE_WEBINAR
						));
				$courses = $command2->queryAll();

				$coursesIds = array();
				foreach($courses as $course){
					$coursesIds[] = $course['idCourse'];
				}

				Yii::app()->db->createCommand()
						->delete(LearningCourseuser::model()->tableName(), 'idCourse IN (' . implode(',', $coursesIds) . ') AND idUser IN (' . implode(',', $userIdsThisChunk) . ')');

			}

			if($unenrollIltSessions){
				Yii::app()->db->createCommand()
						->delete(LtCourseuserSession::model()->tableName(), $condition);

				$command2 = Yii::app()->db->createCommand()
						->select('idCourse')
						->from(LearningCourse::model()->tableName())
						->where('course_type = :classroom', array(
								':classroom' => LearningCourse::TYPE_CLASSROOM,
						));
				$courses = $command2->queryAll();
				$coursesIds = array();
				foreach($courses as $course){
					$coursesIds[] = $course['idCourse'];
				}

				Yii::app()->db->createCommand()
						->delete(LearningCourseuser::model()->tableName(), 'idCourse IN (' . implode(',', $coursesIds) . ') AND idUser IN (' . implode(',', $userIdsThisChunk) . ')');
			}

			if($unenrollWebinarSessions){
				Yii::app()->db->createCommand()
						->delete(WebinarSessionUser::model()->tableName(), $condition);

				$command2 = Yii::app()->db->createCommand()
						->select('idCourse')
						->from(LearningCourse::model()->tableName())
						->where('course_type = :webinar', array(
								':webinar' => LearningCourse::TYPE_WEBINAR,
						));
				$courses = $command2->queryAll();
				$coursesIds = array();
				foreach($courses as $course){
					$coursesIds[] = $course['idCourse'];
				}

				Yii::app()->db->createCommand()
						->delete(LearningCourseuser::model()->tableName(), 'idCourse IN (' . implode(',', $coursesIds) . ') AND idUser IN (' . implode(',', $userIdsThisChunk) . ')');
			}
		}

		$completedPercent = $totalUsersCount==0 ? 100 : $index/$totalUsersCount*100;
		if($completedPercent>100) $completedPercent = 100;
		if($completedPercent<0) $completedPercent = 0;

		$html = $this->renderPartial('_progressive_change_status', array(
				'offset' 				=> $offset,
				'processedUsers' 		=> $index,
				'totalUsers' 			=> $totalUsersCount,
				'completedPercent' 		=> intval($completedPercent),
				'start' 				=> ($offset == 0),
				'stop' 					=> $isLastStep,
				'status'				=> $status,
				'ignored'=>$expiredUsersToBeIgnored,
		), true, true);

		// Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
		// These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stopping when $stop == true)
		$data = array(
				'offset'				=> $newOffset,
				'stop'					=> $isLastStep,
				'idst'					=> $idst,
				'status'				=> $status,
				'ignored_expired_idst'=>implode(',', $collectedIgnoredExpiredUserIds),
		);

		$response = new AjaxResult();
		$response->setStatus(true);
		$response->setHtml($html)->setData($data)->toJSON();
		Yii::app()->end();
	}

	/**
	 * Activate Users
	 */
	/*public function actionActivateUser($idst = null) {

		if (Yii::app()->request->isAjaxRequest) {
			if (!$idst && isset($_POST['idst'])) {
				$idst = $_POST['idst'];
			}
			$result = array('success' => false);

			if ($idst !== null) {
				$models = CoreUser::model()->findAllByAttributes(array('idst' => $idst));
				if (!empty($models)) {
					foreach ($models as $model) {
						$model->scenario = 'activate';
						if (Yii::app()->user->id != $model->idst) {
							$model->activate();
						}
					}
					$result['success'] = true;
					$result['message'] = 'User has been activated successfully';
				} else {
					$result['message'] = 'User has not been found';
				}
			}

			$this->sendJSON($result);
		}
	}*/

	/**
	 * Deactivate Users
	 */
	/*public function actionDeactivateUser($idst = null) {
		if (Yii::app()->request->isAjaxRequest) {
			//$this->sendJSON(array('data' => $idst));
			if (($idst === null) && isset($_POST['idst'])) {
				$idst = $_POST['idst'];
			}
			$result = array('success' => false);

			if ($idst !== null) {
				$models = CoreUser::model()->findAllByAttributes(array('idst' => $idst));
				if (!empty($models)) {
					foreach ($models as $model) {
						$model->scenario = 'deactivate';
						if (Yii::app()->user->id != $model->idst) {
							$model->deactivate();
						}
					}
					$result['success'] = true;
					$result['message'] = 'User has been deactivated successfully';
				} else {
					$result['message'] = 'User has not been found';
				}
			}

			$this->sendJSON($result);
		}
	}*/

	/**
	 * Import of users from csv file
	 */
	public function actionImportUser() {
		// download sample csv file
		if (isset($_GET['download']) && ($_GET['download'] == 'sample' || $_GET['download'] == 'sample_activation')) {
			$sep = DIRECTORY_SEPARATOR;
			$fileName = $_GET['download'].'.csv';
			$filePath = Yii::getPathOfAlias('common').$sep.'extensions'.$sep.$fileName;
			if (file_exists($filePath)) {
				$fileContent = file_get_contents($filePath);
				Yii::app()->request->sendFile('sample.csv', $fileContent, NULL, false);
			}else{
				echo $_GET['download'].' . csv not found';
			}
			Yii::app()->end();
		}

		DatePickerHelper::registerAssets();

		$step = 1;
		if (isset(Yii::app()->session['importUsersStep']) && (isset($_POST['UserImportForm']) || Yii::app()->request->isAjaxRequest)) {
			$step = Yii::app()->session['importUsersStep'];
		} else {
			unset(Yii::app()->session['importUsersStep']);
		}
		// step 1
		if ($step == 1) {
			unset(Yii::app()->session['importUsersModel']);
			if (isset(Yii::app()->session['importModel']) && !empty(Yii::app()->session['importModel'])) {
				$model = Yii::app()->session['importModel'];
				$model->scenario = 'step_one';
				unset(Yii::app()->session['importModel']);
			} else {
				// This will set current node ID in session (used by data provider later)
				$currentNodeId = $this->resolveCurrentOrgchartNodeId();
				$model = new UserImportForm('step_one');
				$model->node = $currentNodeId;
			}
			if (isset($_POST['UserImportForm'])) {
				$model->attributes = $_POST['UserImportForm'];
				$model->file = CUploadedFile::getInstance($model, 'file');
				if(isset($_POST['fromActivationTab']) && $_POST['fromActivationTab'] == 1){
					$model->importTabType = UserImportForm::SCENARIO_ACTIVATION;
				}
				if ($model->validate()) {

					$newFileName = Docebo::randomHash() . "." . $model->file->extensionName;
					$model->localFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $newFileName;
					@copy($model->file->tempName, $model->localFile);
					$model->originalFilename = $model->file->name;
					Yii::app()->session['importUsersStep'] = $step = 2;
					Yii::app()->session['importUsersModel'] = $model;
				}
			}
		}

		if ($step == 1) {
			// "Force password change on first signin: yes" selected by default
			// if the setting is checked for the platform
			if(Settings::get('pass_change_first_login')=='on')
				$model->passwordChanging = 'yes';
			$this->render('import', array('model' => $model));
		}
		if ($step == 2) {
			// Get/Load first few users ONLY from the CSV
			$items = (!empty($model) ? $model->getData(5) : false);

			if ($items == false) {
				$this->redirect(array($this->id.'/importUser'));
			}

			$columns = array();
			foreach ($items[0] as $key => $value) {
				$column = array(
						'name' => $key,
						'htmlOptions' => array('class' => 'column-'.$key),
				);
				if ($model->firstRowAsHeader) {
					$column['header'] = $value;
				}
				$columns[] = $column;
			}

			// UserMan JS/CSS
			$cs = Yii::app()->getClientScript();
			$themeUrl = Yii::app()->theme->baseUrl;
			$cs->registerScriptFile($themeUrl . '/js/user_management.js', CClientScript::POS_HEAD);
			$cs->registerCssFile($themeUrl . '/css/user_management.css');
            // BlockUI
            $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.blockUI.js');


			$this->render('import_step2', array(
					'columns' => $columns,
					'model' => $model,
			));
		}
	}

	public function actionSaveFilterSelection() {
		$fields = serialize($_REQUEST['fields']);
		$cookieName = 'userManagementSelectedColumns';

		$cookie = new CHttpCookie($cookieName, $fields);
		$cookie->expire = 0;
		$cookie->domain = Yii::app()->request->serverName;
		$cookie->httpOnly = true;

		Yii::app()->request->cookies[$cookieName] = $cookie;

		$this->sendJSON(array(
				'success' => true,
		));
	}

	public function actionUsersAutocomplete() {
		$query = addcslashes($_REQUEST['data']['query'], '%_');

		$show_first_name_first = Settings::get('show_first_name_first', 'off');

		if (!empty($_REQUEST['data']['type'])) {
			$userType = $_REQUEST['data']['type'];
		}

		$criteriaConfig = array(
				'condition' => '(userid LIKE :match OR userid LIKE :match2 OR firstname LIKE :firstname OR lastname LIKE :lastname OR email LIKE :email) AND userid<>"/Anonymous"',
				'params' => array(
						':match' => "/%$query%",
						':match2' => "%$query%",
						':firstname' => "%$query%",
						':lastname' => "%$query%",
						':email' => "%$query%",
				),
				'with' => array(
					// 'adminLevel',
						'language',
				),
		);

		if ($_REQUEST['data']['onlyusers']) {
			$criteriaConfig['with']['adminLevel'] = array(
					'condition' => 'adminLevel.groupid = :groupid',
					'params' => array(
							':groupid' => '/framework/level/user',
					),
			);
		} else {
			$criteriaConfig['with'][] = 'adminLevel';
		}

		// Autocomplete for only users assigned to the Power User
		if (Yii::app()->user->getIsAdmin()) {
			$criteriaConfig['with']['onlyAssignedUsers'] = array(
					'condition' => 'puser_id = :userId',
					'params' => array(
							':userId' => Yii::app()->user->id,
					),
			);
		} elseif ($_REQUEST['data']['onlyassignedusers']) {
			$criteriaConfig['with']['onlyAssignedUsers'] = array(
					'condition' => 'puser_id = :userId',
					'params' => array(
							':userId' => Yii::app()->session['currentPowerUserId'],
					),
			);
		}

		if (!empty($_REQUEST['data']['course']) && is_numeric($_REQUEST['data']['course'])) {
			$criteriaConfig['with'][] = 'learningCourseusers';
			$criteriaConfig['condition'] = '('.$criteriaConfig['condition'].') AND learningCourseusers.idCourse=:course';
			$criteriaConfig['params'][':course'] = $_REQUEST['data']['course'];

			if (isset($_REQUEST['data']['waiting'])) {
				$criteriaConfig['condition'] = $criteriaConfig['condition'].' AND learningCourseusers.waiting=:waiting';
				$criteriaConfig['params'][':waiting'] = $_REQUEST['data']['waiting'];
			}
		} elseif (!empty($_REQUEST['data']['catalogue']) && is_numeric($_REQUEST['data']['catalogue'])) {
			$criteriaConfig['with'] = 'learningCatalogueMember';
			$criteriaConfig['condition'] = '('.$criteriaConfig['condition'].') AND learningCatalogueMember.idCatalogue=:catalogue';
			$criteriaConfig['params'][':catalogue'] = $_REQUEST['data']['catalogue'];
		} elseif (!empty($_REQUEST['data']['group'])) {
			$criteriaConfig['with'] = 'memberGroups';
			$criteriaConfig['condition'] = '('.$criteriaConfig['condition'].') AND memberGroups.idst=:group';
			$criteriaConfig['params'][':group'] = $_REQUEST['data']['group'];
		}


		$criteria = new CDbCriteria($criteriaConfig);

		if ($userType == 'temp') {
			$model = CoreUserTemp::model();
		} else {
			$model = CoreUser::model();
		}

		$users = $model->findAll($criteria);

		$result = array(
				'options' => array(),
		);

		if (!empty($users)) {
			foreach ($users as $user) {
				$username = Yii::app()->user->getRelativeUsername($user->userid);
				$text = "{$username}";

				$names = array();

				if ($show_first_name_first == 'off') {
					if (!empty($user->lastname))
						$names[] = $user->lastname;
					if (!empty($user->firstname))
						$names[] = $user->firstname;
				} else {
					if (!empty($user->firstname))
						$names[] = $user->firstname;
					if (!empty($user->lastname))
						$names[] = $user->lastname;
				}

				$names = join(' ', $names);


				if ($names)
					$text .= " - $names";

				$result['options'][$username] = $text;
			}
		}

		$this->sendJSON($result);
	}

	public function actionWaitingApproval() {
		$tempUser = CoreUserTemp::model();
		$tempUser->scenario = 'search';

		// UserMan JS/CSS
		$cs = Yii::app()->getClientScript();
		$themeUrl = Yii::app()->theme->baseUrl;
		$cs->registerScriptFile($themeUrl . '/js/user_management.js', CClientScript::POS_HEAD);
		$cs->registerCssFile($themeUrl . '/css/user_management.css');

		if (Yii::app()->request->isAjaxRequest && isset($_REQUEST['CoreUserTemp']))
		{
			$tempUser->attributes = $_REQUEST['CoreUserTemp'];
			$tempUser->search_input = $_REQUEST['CoreUserTemp']['search_input'];
		}

		if (isset($_REQUEST['autocomplete']))
		{
			$tempUser->search_input = addcslashes($_REQUEST['term'], '%_');
			$dataProvider = $tempUser->dataProvider();
			$dataProvider->pagination = array('pageSize' => isset($_REQUEST['max_number']) ? (int) $_REQUEST['max_number'] : 10);
			$result = array();
			foreach ($dataProvider->data as $user) {
				$username = Yii::app()->user->getRelativeUsername($user->userid);
				$label = $username;
				$value = $username;
				$names = array();
				if (!empty($user->firstname))
					$names[] = $user->firstname;
				if (!empty($user->lastname))
					$names[] = $user->lastname;
				$names = join(' ', $names);
				if ($names)
					$label .= " - $names";

				$result[] = array('label' => $label, 'value' => $value);
			}
			$this->sendJSON($result);
		}

		$this->render('waitingApproval', array('model' => $tempUser));
	}

	public function actionAxGetWaitingUsersCount(){
		echo CoreUserTemp::model()->count();
		Yii::app()->end();
	}

	public function actionConfirmUser($idst = null) {
		if (!empty($_POST['idst'])) {
			$idst = $_POST['idst'];
		}
		$tempUser = CoreUserTemp::model()->findAllByAttributes(array('idst' => $idst));
		if (empty($tempUser)) {
			$this->sendJSON(array());
		}

		if (!empty($_POST['CoreUserTemp'])) {
			if ($_POST['CoreUserTemp']['confirm']) {
				foreach ($tempUser as $user) {
					if($user->userid){
						$userid = trim($user->userid);
						$criteria = new CDbCriteria();
						$criteria->addCondition('LOWER(userid) = :userid');
						$criteria->params['userid'] = strtolower(CoreUser::getAbsoluteUsername($userid));
						$coreUser = CoreUser::model()->find($criteria);


						if($coreUser){
							$coreUserTempModel = new CoreUserTemp();
							$coreUserTempModel->addError('confirm', Yii::t('user', 'A user with the username "{username}" already exists',
								array('{username}'=>$userid)));
							if (Yii::app()->request->isAjaxRequest) {
								$content = $this->renderPartial('confirmTemp', array(
									'model' => $coreUserTempModel,
									'users' => $tempUser,
								), true);
								$this->sendJSON(array(
									'html' => $content,
									'modalTitle' => Yii::t('standard', '_CONFIRM', array(count($tempUser))),
									'confirmBtnTitle' => Yii::t('standard', '_CONFIRM'),
									'cancelBtnTitle' => Yii::t('standard', '_CANCEL'),
								));
								Yii::app()->end();
							}
						}
					}
					$regCourse = null;
					if(isset($user->self_subscribe_course)){
						$regCourse = $user->self_subscribe_course;
					}

					$idst = $user->confirm();

					$userModel = $idst ? CoreUser::model()->findByPk($idst) : false;
					if($userModel){
						if($regCourse && (Settings::get('registration_code_type')=='tree_course')){
							$courseModel = LearningCourse::model()->findByPk($regCourse);
							if($courseModel && $userModel){
								$maxSubsReached     =   $courseModel->course_type != LearningCourse::TYPE_ELEARNING || ($courseModel->max_num_subscribe==0 || $courseModel->max_num_subscribe > $courseModel->getTotalSubscribedUsers(false, false, false)) ? false : true;
								$subscribeAsWaiting =  ($maxSubsReached && $courseModel->allow_overbooking) ? true : false ;
								$courseUserModel = new LearningCourseuser('self-subscribe');
								if($courseModel->course_type == LearningCourse::TYPE_ELEARNING && !$subscribeAsWaiting && !($courseModel->subscribe_method == LearningCourse::SUBSMETHOD_MODERATED)){
									 $courseUserModel->subscribeUser($userModel->idst, Yii::app()->user->id, $courseModel->idCourse, 0, false, false, false, false, false, false);
								}else{
                                    $waiting = 1;
                                    if (in_array($courseModel->course_type, array(
                                            LearningCourse::TYPE_CLASSROOM,
                                            LearningCourse::TYPE_WEBINAR
                                        )) &&
                                        ($courseModel->max_num_subscribe == 0 || $courseModel->max_num_subscribe > $courseModel->getTotalSubscribedUsers(false,false, false)) &&
                                        $courseModel->subscribe_method == LearningCourse::SUBSMETHOD_FREE
                                    ) {
                                        $waiting = 0;
                                    }

                                    $courseUserModel->subscribeUser($userModel->idst, Yii::app()->user->id, $courseModel->idCourse, $waiting, false, false, false, false, false, false);
								}
							}
						}


						//Group Auto Assign
						$hasSelfPopulatingGroups = Yii::app()->db->createCommand("SELECT COUNT(*) FROM core_group WHERE assign_rules = 1")->queryScalar();
						if($hasSelfPopulatingGroups > 0) {
							$scheduler = Yii::app()->scheduler;
							$params = array(
									'user_idst' => $userModel->idst,
									'offset' => 0
							);
							$scheduler->createImmediateJob(GroupAutoAssignTask::JOB_NAME, GroupAutoAssignTask::id(), $params);
						}

						// Confirm successful. Raise event/notification
						Yii::app()->event->raise('NewUserCreatedSelfreg', new DEvent($this, array(
								'target_user' => $userModel,
                                'user' => $userModel
						)));
					}
				}
				$this->sendJSON(array());
			}
		}
		if (Yii::app()->request->isAjaxRequest) {
			$content = $this->renderPartial('confirmTemp', array(
					'model' => new CoreUserTemp(),
					'users' => $tempUser,
			), true);
			$this->sendJSON(array(
					'html' => $content,
					'modalTitle' => Yii::t('standard', '_CONFIRM', array(count($tempUser))),
					'confirmBtnTitle' => Yii::t('standard', '_CONFIRM'),
					'cancelBtnTitle' => Yii::t('standard', '_CANCEL'),
			));
		}
		Yii::app()->end();
	}

	public function actionTempUserSendMessage(){
		$idst = $_GET['idst'];

		if(!$idst)
			return FALSE;

		$tempUser = CoreUserTemp::model()->findByAttributes(array('idst'=>$idst));
		if (!$tempUser)
			$this->sendJSON(array());

		if(isset($_POST['confirm'])){
			// Sending an email to user
			$subject = Yii::t('register', '_MAIL_OBJECT');
			$fromAdminEmail = array(Settings::get('mail_sender') => Settings::get('mail_sender'));
			$toEmail = array($tempUser->email => $tempUser->email);

			$link = Docebo::createAbsoluteLmsUrl('user/confirmRegistration',array('random_code'=> $tempUser->random_code));

			// If the Custom Domain App is active and configured
			// use the new URL in the confirm email link
			$originalDomain = parse_url($link, PHP_URL_HOST);
			$customDomain = rtrim(Docebo::getCurrentDomain(), '/');
			if($customDomain && $originalDomain)
				$link = str_ireplace($originalDomain, $customDomain, $link);

			// We store the protocol sometimes, so there can be those weird URLs
			$link = str_ireplace('http://http://', 'http://', $link);
			$link = str_ireplace('https://https://', 'https://', $link);
			$link = str_ireplace('https://http://', 'https://', $link);
			$link = str_ireplace('http://https://', 'https://', $link);

			$body = Yii::t('register', '_REG_MAIL_TEXT', array(
					'[userid]' => $tempUser->userid,
					'[password]'=> Yii::t('register', 'Hidden for privacy'),
					'[firstname]' => $tempUser->firstname,
					'[lastname]' => $tempUser->lastname,
					'[link]' => $link,
					'[hour]' => Settings::get('hour_request_limit')
			));
			$mm = new MailManager();
			$mm->setContentType('text/html');

			$mm->mail($fromAdminEmail,$toEmail, $subject, $body);

			$this->sendJSON(array());
			Yii::app()->end();
		}

		$this->sendJSON(array(
				'html' => $this->renderPartial('temp_user_resend_email', array('user'=>$tempUser), true),
				'modalTitle' => Yii::t('user_management', '_RESEND_EMAIL', array(count($tempUser))),
				'confirmBtnTitle' => Yii::t('standard', '_CONFIRM'),
				'cancelBtnTitle' => Yii::t('standard', '_CANCEL'),
		));
		exit;
	}

	public function actionTempUserDetails($idst = null){
		$tempUser = CoreUserTemp::model()->findByAttributes(array('idst'=>$idst));
		if(!$tempUser){
			$this->sendJSON(array());
			Yii::app()->end();
		}

		$roleMembers = Yii::app()->getDb()->createCommand()
				->select('idst')
				->from(CoreGroupMembers::model()->tableName())
				->where('idstMember=:id', array(':id'=>$tempUser->idst))
				->queryColumn();
		if(!empty($roleMembers)) {
			$branchPath             = CoreOrgChartTree::getOcGroupPath( $roleMembers );
		}else{
			$root = CoreOrgChartTree::getOrgRootNode();
			$branchPath = $root ? CoreOrgChartTree::getOcGroupPath( $root->idst_oc ) : null;
		}

		$additionalFieldValues = CoreUser::getAdditionalFieldEntryValuesArray([$tempUser->idst], true, true);
        $additionalFieldValues = array_filter($additionalFieldValues[$tempUser->idst]);
        $additionalFieldKeys = array_keys($additionalFieldValues);
        $additionalFields = CoreUserField::model()->findAllByAttributes(['id_field' => $additionalFieldKeys]);
        $indexedAdditionalFields = [];
        foreach ($additionalFields as $field) {
            $fieldId = $field->id_field;
            $field->userEntry = $additionalFieldValues[$fieldId];
            $indexedAdditionalFields[$fieldId] = $field;
        }

		$details = $this->renderPartial('temp_user_details', array(
				'model'=>$tempUser,
				'branchPath' => $branchPath,
				'additionalFieldValues'=>$indexedAdditionalFields,
		), true);

		$this->sendJSON(
				array(
						'html'=>$details,
						'modalTitle' => Yii::t('standard', '_MORE_INFO'),
						'confirmBtnTitle' => Yii::t('standard', '_CONFIRM'),
						'cancelBtnTitle' => Yii::t('standard', '_CANCEL'),
				)
		);
	}

	public function actionDeleteTempUser($idst = null) {
		if (!empty($_POST['idst'])) {
			$idst = $_POST['idst'];
			if (is_string($idst)) {
				if (stripos($idst, ',') !== false)
					$idst = explode(',', $idst);
				else
					$idst = array($idst);
			}
		}
		$tempUser = CoreUserTemp::model()->findAllByAttributes(array('idst' => $idst));
		if (empty($tempUser)) {
			$this->sendJSON(array());
		}

		if (!empty($_POST['CoreUserTemp'])) {
			if ($_POST['CoreUserTemp']['confirm']) {
				foreach ($tempUser as $user) {
					if (Yii::app()->user->id != $user->idst) {
						$user->doRejectedCleanup();
						$user->delete();
					}
				}
				$this->sendJSON(array());
			}
		}
		if (Yii::app()->request->isAjaxRequest) {
			$content = $this->renderPartial('delete', array(
					'model' => new CoreUserTemp(),
					'users' => $tempUser,
					'idst' => $idst
			), true);
			$this->sendJSON(array(
					'html' => $content,
					'modalTitle' => Yii::t('standard', '_DEL', array(count($tempUser))),
					'confirmBtnTitle' => Yii::t('standard', '_CONFIRM'),
					'cancelBtnTitle' => Yii::t('standard', '_CANCEL'),
			));
		}
		Yii::app()->end();
	}

	public function actionSyncGappsUsers(){
		PluginManager::activateAppByCodename('Googleapps');
		$this->redirect(Docebo::createAdminUrl('GoogleappsApp/GoogleappsApp/syncUsers'));
	}



	/**
	 * Try to reslve Current Orgchart Node to use
	 *
	 * @return number
	 */
	protected function resolveCurrentOrgchartNodeId() {

		$currentNodeId = false;
		$cookieName = 'userman-fancytree-active';

		// Check REQUEST
		if (isset($_REQUEST['currentNodeId'])) {
			Yii::app()->session['currentNodeId'] = (int) $_REQUEST['currentNodeId'];
			$nodeModel = CoreOrgChartTree::model()->findByPk($_REQUEST['currentNodeId']);
			if($nodeModel){
				Yii::app()->session['currentNodePath'] = $nodeModel->getNodePath();
			}

			Yii::app()->request->cookies[$cookieName] = new CHttpCookie($cookieName, (int) $_REQUEST['currentNodeId']);
			$currentNodeId = (int) $_REQUEST['currentNodeId'];
		}
		// Check fancytree "persist" cookie
		else if (isset(Yii::app()->request->cookies[$cookieName])) {
			$model =  CoreOrgChartTree::model()->findByPk((int) Yii::app()->request->cookies[$cookieName]->value);
			if ($model) {
				Yii::app()->session['currentNodeId'] = $model->idOrg;
				$currentNodeId = $model->idOrg;
			}
			else {
				unset(Yii::app()->request->cookies[$cookieName]);
			}
		}
		// Check session
		else if (isset(Yii::app()->session['currentNodeId'])) {
			$currentNodeId = Yii::app()->session['currentNodeId'];
			Yii::app()->request->cookies[$cookieName] = new CHttpCookie($cookieName, $currentNodeId);
		}

		// If not set yet..  use root node
		if ($currentNodeId === false) {
			$rootNode = CoreOrgChartTree::getOrgRootNode();
			Yii::app()->session['currentNodeId'] = $currentNodeId = $rootNode->idOrg;
			Yii::app()->request->cookies[$cookieName] = new CHttpCookie($cookieName, $currentNodeId);
		}

		return $currentNodeId;

	}



	/**
	 * Render per-Branch actions dropdown menu. Called 'on click' by user_management.js method
	 *
	 */
	public function actionAxRenderBranchActionsMenu() {

		$response = new AjaxResult();

		$idOrg = Yii::app()->request->getParam('idOrg', false);
		$node = CoreOrgChartTree::model()->findByPk($idOrg);

		if(!$node)
		{
			$response->setStatus(false)->toJSON();
			Yii::app()->end();
		}

		if(Yii::app()->user->getIsAdmin())
			if(!CoreUserPU::checkFolderAssociation($node->idst_oc, $node->idst_ocd, Yii::app()->user->id))
			{
				$response->setStatus(false)->toJSON();
				Yii::app()->end();
			}

		$html = $this->renderPartial('_nodeActionsV2', array(
				'node' => $node,
				'id' => $node->idOrg,
				'name' => $node->coreOrgChartTranslated->translation,
				'isRoot' => $node->isRoot()
		),true);

		$response->setStatus(true)->setHtml($html)->toJSON();
		Yii::app()->end();
	}


	/**
	 * Render OrgChart Tree and return HTML (ajax called, always)
	 */
	public function actionAxRenderOrgchartFancyTree() {
		$orgchartFancyTreeId = Yii::app()->request->getParam('orgchartFancyTreeId', false);
		Yii::app()->event->raise('SetSessionVarForVisibilityNodes',
				new DEvent($this, array('filter'=>Yii::app()->request->getParam('filter'))));
		// jQuery UI is already loaded by the main page (actionHome)
		// This is very important: if we do not disable it, UI is sort of not functioning correctly!!
		// Because it will be loaded (registered) by the fancytree
		Yii::app()->clientScript->scriptMap['jquery-ui.min.js'] = false;

		//if we are reloading tree due to tree changes, do some additional checks
		if (Yii::app()->request->getParam('reload', 0) > 0) {
			if (isset(Yii::app()->session['currentNodeId'])) {
				$currentSelectedNode = Yii::app()->session['currentNodeId'];
				//check if node currently selected in this session still exists (it may have been deleted by another user in another session)
				if (!CoreOrgChartTree::model()->findByPk($currentSelectedNode)) {
					//the selected node does not exists anymore, so fallback on the root node
					$rootNode = CoreOrgChartTree::model()->getOrgRootNode();
					if ($rootNode) {
						Yii::app()->session['currentNodeId'] = $rootNode->getPrimaryKey();
					} else {
						unset(Yii::app()->session['currentNodeId']);
					}
				}
			}
		}

		$html = $this->renderPartial('_fancyChartTree', array(
				'id' => $orgchartFancyTreeId
		),true, true);  // <<---   processOutput MUTS be TRUE !!!!
		echo $html;
	}


	/**
	 * This is a faster DOA version of the updateUser action for massive editing
	 * @throws CException
	 */
	public function actionUpdateUserMassive() {

		$db = Yii::app()->db;
		$rq = Yii::app()->request;
		$errors = array();

		if (Yii::app()->request->isAjaxRequest) {

			//$idstList = $rq->getParam('idst', array());
			$idstListStr = trim($rq->getParam('idst-list', ''));
			$idstList = (!empty($idstListStr) ? explode(',', $idstListStr) : array());
			$userFields = $rq->getParam('userFields', array());
			$groupFields = $rq->getParam('groupFields', array());

			//prepare and initialize output variable
			$result = array();
			$input = array();

			$result['success'] = true;
			$result['html'] = '';

			//validate input users
			if (!is_array($idstList) || empty($idstList)) {
				$result['success'] = false;
				$result['status'] = 'not-found';
				$result['html'] = 'User not found';
				$this->sendJSON($result);
			}

			if (isset($_POST['userFields'])) {
				$userConditions = array();
				$input['CoreUser'] = $rq->getParam('CoreUser', array());
				foreach ($_POST['userFields'] as $key => $field) {
					switch ($key) {
						case 'firstname': $userConditions[] = "firstname = :firstname"; break;
						case 'lastname': $userConditions[] = "lastname = :lastname"; break;
						case 'email': $userConditions[] = "email = :email"; break;
						case 'email_status': $userConditions[] = "email_status = :email_status"; break;
						case 'new_password':
							$userConditions[] = "pass = :new_password";
                            if(Yii::app()->user->getIsGodadmin()){

								$options['minChars'] = true;
								$options['alphaNum'] = false;
								$options['repeating'] = false;
								$options['sequentialNum'] = false;
								$options['sequentialLet'] = false;
								$options['sequentialKey'] = false;
								$options['dictionary'] = false;

								$resultPassCheck = Yii::app()->security->checkPasswordStrength($input['CoreUser']['new_password'], $input['CoreUser']['new_password_repeat'], false, $options);
								if (!$resultPassCheck['success'] && $resultPassCheck['message'] != '') {
									$result['success'] = false;
									$result['status'] = 'error';
									$result['html'] = $resultPassCheck['message'];
									$this->sendJSON($result);
								} else {
									$encryptedPassword = CoreUser::model()->encryptPassword($input['CoreUser']['new_password']);
									break;
								}
                            }
                            else {
                                if (Settings::get('pass_not_username') == 'on'){
                                    $usersIdsCommand = $db->createCommand("SELECT idst, userid FROM " . CoreUser::model()->tableName() . " WHERE idst IN (:idst)");
                                    $usersIdsCommand = $usersIdsCommand->query(array(':idst' => $idstListStr));
                                    $passwordIsEqualToAnyUsername = false;
                                    while ($row = $usersIdsCommand->read()) {
                                        if (substr($row['userid'], 0, 1) == '/') {
                                            $unerId = substr($row['userid'], 1);
                                        } else {
                                            $unerId = $row['userid'];
                                        }

                                        //Check if username is equal to password
                                        if (strtolower($unerId) == strtolower($input['CoreUser']['new_password'])) {
                                            $passwordIsEqualToAnyUsername = true;
                                            break;
                                        }

                                    }

                                    if ($passwordIsEqualToAnyUsername) {
                                        $result['success'] = false;
                                        $result['status'] = 'error';
                                        $result['html'] = Yii::t('configuration', '_PASS_NOT_USERNAME');
                                        $this->sendJSON($result);
                                    }
                                }
                                $resultPassCheck = Yii::app()->security->checkPasswordStrength($input['CoreUser']['new_password'], $input['CoreUser']['new_password_repeat']);
                                if (!$resultPassCheck['success'] && $resultPassCheck['message'] != '') {
                                    $result['success'] = false;
                                    $result['status'] = 'error';
                                    $result['html'] = $resultPassCheck['message'];
                                    $this->sendJSON($result);
                                } else {
                                    $encryptedPassword = CoreUser::model()->encryptPassword($input['CoreUser']['new_password']);
							        break;
                                }
                            }
					}
				}

				//prepare sql commands
				$userCommand = (!empty($userConditions)
						? $db->createCommand("UPDATE ".CoreUser::model()->tableName()." SET ".implode(", ", $userConditions)." WHERE idst = :idst")
						: false);
			}

			if (isset($_POST['groupFields']) && Yii::app()->user->getIsGodadmin()) {
				$input['CoreGroupMembers'] = $rq->getParam('CoreGroupMembers', array());
				foreach ($_POST['groupFields'] as $key => $field) {
					switch ($key) {
						case 'idst':
							//retrieve and store users already assigned to the specified level group
							$already = array();
							$alreadyCommand = $db->createCommand("SELECT idst, idstMember FROM ".CoreGroupMembers::model()->tableName()." WHERE idst = :idst");
							$alreadyReader = $alreadyCommand->query(array(':idst' => $input['CoreGroupMembers']['idst']));
							while ($row = $alreadyReader->read()) {
								if (in_array($row['idstMember'], $idstList)) { //exclude non-selected idsts, which are not useful
									$already[] = (int)$row['idstMember'];
								}
							}
							break;
					}
				}
				//prepare sql commands
				$userLevels = array_keys(Yii::app()->user->getUserLevelIds());
				$deleteCommand = $db->createCommand("DELETE FROM ".CoreGroupMembers::model()->tableName()." WHERE idst IN (".implode(",", $userLevels).") AND idstMember = :idst_member");
				$groupCommand = $db->createCommand("INSERT INTO ".CoreGroupMembers::model()->tableName()." (idst, idstMember) VALUES (:idst, :idst_member)");
			}

			$transaction = $db->beginTransaction();
			try {

				$userBinded = false;

				//iterate selected users
				foreach ($idstList as $idst) {
					if(isset($_POST['sendEmails'])
							&& isset($_POST['userFields']['email_status'])
							&& $_POST['userFields']['email_status'] == 1
							&& intval($_POST['sendEmails']) === 1
							&& isset($_POST['CoreUser']['email_status'])
							&& $_POST['CoreUser']['email_status'] == 0){
						// send emails to the users according to the UserEmailVerified notification handler
						$userModel = CoreUser::model()->findByPk($idst);
						if ($userModel->setVerificationLink())
							Yii::app()->event->raise('UserEmailVerified', new DEvent($this, array('user' => $userModel)));
					}
					if (isset($_POST['userFields']) && !empty($userCommand)) {
						if (!$userBinded) {
							foreach ($_POST['userFields'] as $key => $field) {
								switch ($key) {
									case 'firstname': $userCommand->bindParam(':firstname', $input['CoreUser']['firstname']); break;
									case 'lastname': $userCommand->bindParam(':lastname', $input['CoreUser']['lastname']); break;
									case 'email': $userCommand->bindParam(':email', $input['CoreUser']['email']); break;
									case 'email_status': $userCommand->bindParam(':email_status', $input['CoreUser']['email_status']); break;
									case 'new_password': $userCommand->bindParam(':new_password', $encryptedPassword); break;
								}
							}
							$userBinded = true; //with this, we perform the binding operation only the first time (for non-changing input data)
						}
						$oldEmail = Yii::app()->db->createCommand()
								->select('email')
								->from(CoreUser::model()->tableName())
								->where('idst=:id', array(':id' => $idst))->queryScalar();
						if ($oldEmail != $input['CoreUser']['email']) {
							Yii::app()->db->createCommand()
									->update(CoreUser::model()->tableName(), array('email_status' => 0), 'idst=:id', array(':id' => $idst));
						}
						$userCommand->bindParam(':idst', $idst);
						$userCommand->execute();
					}

					if (isset($_POST['groupFields']) && Yii::app()->user->getIsGodadmin()) {
						foreach ($_POST['groupFields'] as $key => $field) {
							switch ($key) {
								case 'idst':
									if (!in_array($idst, $already)) {
										//first remove old group association
										$deleteCommand->execute(array(':idst_member' => $idst));
										//assign new group
										$groupCommand->execute(array(':idst' => $input['CoreGroupMembers']['idst'], ':idst_member' => $idst));
									}

									// Start PU self-assign section
									// get the 'groipid' only if it is not already taken
									if (!isset($newUserGroup)) {
										$newUserGroup = CoreGroup::getGroup($input['CoreGroupMembers']['idst'], FALSE);
									}

									// if the user level is changed to 'Power User'
									// then we need to assign to power user himself
									// NOTE: the db table, that corresponds to this is 'core_admin_tree'
									// and on every PU login this info is stored inside 'core_user_pu' db table
									// Please, note that we should avoid changes to 'core_user_pu' db table
									if (isset($newUserGroup->groupid) && $newUserGroup->groupid == Yii::app()->user->level_admin) {
										// Assign this power user to himself. if not yet assigned
										// This is to allow Power User to manage himself.
										$puModel = CoreAdminTree::model()->findByAttributes(array('idst' => $idst, 'idstAdmin' => $idst));
										if (!$puModel) {
											CoreAdminTree::addPowerUserMember($idst, $idst, false);  // false: do NOT update internally!!!!!!!
										}
									}

									// Sync with the Hydra RBAC
									HydraRBAC::manageUserToRole(array( 'user' => $idst, 'group' => $input['CoreGroupMembers']['idst'] ));

									break;
							}
						}
					}
					// Raise event
					Yii::app()->event->raise('UserModified', new DEvent($this, array(
						'user' => CoreUser::model()->findByPk($idst),
					)));
				}

				$transaction->commit();

			} catch (CException $e) {

				//revert back db changes made so far
				$transaction->rollback();

				$result['success'] = false;
				$result['status'] = 'error';
				$result['html'] = $e->getMessage();
				$this->sendJSON($result);
			}

			//send output
			$result['status'] = 'saved';
			$this->sendJSON($result);
		}
	}


	public function actionExportUsersToCsv($idst = null)
	{
		$fileType = '';
		if (isset($_REQUEST)) {
			$idst = !empty($_REQUEST['idst']) ? $_REQUEST['idst'] : $idst;
			$fileType = $_REQUEST['fileType'];
		}
		if (!$idst || empty($idst)) {
			Yii::log('No users selected to assign to nodes', CLogger::LEVEL_WARNING);
			$this->sendJSON(array(
					'success' => false
			));
		}

		if (is_string($idst)) {
			if (stripos($idst, ',') !== false) {
				// Case where $idst comes as a comma separated list
				$idst = explode(',', $idst);
			} else {
				$idst = array($idst);
			}
		}
		$usersList = implode(',', $idst);

		$default_with = array(
				'coreOrgChart' => array(
						'on' => 'coreOrgChart.lang_code = :lang',
						'params' => array('lang' => Settings::get('default_language', 'english'))
				)
		);
		$defaultOrgChartTree = CoreOrgChartTree::model()->with($default_with)->findAll();
		$defaultOrgChartTreeTranslation = array();
		foreach ($defaultOrgChartTree as $node) {
			$defaultOrgChartTreeTranslation[$node->idOrg] = $node->coreOrgChart->translation;
		}

		$currentNode = Yii::app()->session['currentNodeId'];

		$coreOrgCartTree = CoreOrgChartTree::model()->findByPk($currentNode);

		$breadcrumb = $this->getTreeBreadcrumbs($coreOrgCartTree, $defaultOrgChartTreeTranslation);

		if (empty($breadcrumb)) {
			$breadcrumb = 'Docebo';
		}

		/*********- Get Fields -********/
		$f = new ReportFieldsForm();
		if ($f) {
			$allFields = $f->getUserFields(false,true);
			$allFields['selectable'] += array(
					'register_date' => Yii::t('report', '_CREATION_DATE'),
					'lastenter' => Yii::t('standard', '_DATE_LAST_ACCESS'),
					'language' => Yii::t('standard', '_LANGUAGE'),
					'level' => Yii::t('standard', '_LEVEL'),
			);
		}

		if ($_REQUEST['confirm']) {
			//we are going to cache the idst selection in a separate file, since it potentially can be huge, possibly tens of thousands or more !
			$cacheKey = $this->_setCachedUsersToBeExported($idst);
			//start progressive action
			$additionalCustomFields = implode(',', $allFields['custom_fields']);
			$this->renderPartial('axProgressiveExportUsers', array(
					'idst' => $idst,
					'allFields' => $allFields,
					'fileType' => $fileType,
					'additionalCustomFields' => $additionalCustomFields,
					'cacheKey' => $cacheKey
			));
			Yii::app()->end();
		} else {
			$this->renderPartial('export_users_to_csv', array(
					'idst' => $idst,
					'breadcrumb' => $breadcrumb,
					'allFields' => $allFields,
					'fileType' => $fileType,
			));
			Yii::app()->end();
		}
	}



	//--- internal functions for user export managing ---

	protected function _generateUserExportCacheKey($prefix = '') {
		return uniqid($prefix).'_'.Yii::app()->user->id.'_'.time();
	}

	protected function _setCachedUsersToBeExported(array $users) {
		$content = implode(',', $users);
		$cacheKey = $this->_generateUserExportCacheKey('_users_export_'); //generate a key (it will be used as file name for cached data)
		$pathName = Docebo::getUploadTmpPath();
		$fileName = $cacheKey.'.tmp';
		$file = fopen($pathName . DIRECTORY_SEPARATOR . $fileName, 'w');
		if (!$file) {
			//TODO: handle error
		}
		fwrite($file, $content);
		fclose($file);
		return $cacheKey;
	}

	protected function _unsetCachedUsersToBeExported($cacheKey) {
		$pathName = Docebo::getUploadTmpPath();
		$fileName = $cacheKey.'.tmp';
		unlink($pathName . DIRECTORY_SEPARATOR . $fileName);
	}

	protected function _getCachedUsersToBeExported($cacheKey) {
		$pathName = Docebo::getUploadTmpPath();
		$fileName = $cacheKey.'.tmp';
		if (!file_exists($pathName . DIRECTORY_SEPARATOR . $fileName)) { throw new CException('Cache file not found [key: '.$cacheKey.']'); }
		$content = file_get_contents($pathName . DIRECTORY_SEPARATOR . $fileName);
		return explode(',', $content);
	}

	//--- ---

	public function actionAxProgressiveExportUsers()
	{

		$response = new AjaxResult();
		$defaultLimit = 10;

		$idst = Yii::app()->request->getParam('idst', '');
		$limit = Yii::app()->request->getParam('limit', $defaultLimit);
		$offset = Yii::app()->request->getParam('offset', 0);
		$start = Yii::app()->request->getParam('start', false);
		$download = Yii::app()->request->getParam('download', false);
		$downloadGo = Yii::app()->request->getParam('downloadGo', false);
		$exportType = Yii::app()->request->getParam('exportType', 'csv');
		$phase = Yii::app()->request->getParam('phase', 'start');
		$separator = Yii::app()->request->getParam('separator', false);
		$manualSep = Yii::app()->request->getParam('manual-separator', false);
		$type = Yii::app()->request->getParam('type', false);
		$fileType = Yii::app()->request->getParam('fileType', false);
		$dataColumns = Yii::app()->request->getParam('dataColumns', '');
		$customFilter = Yii::app()->request->getParam('custom_filter', array());
		$includeNames = Yii::app()->request->getParam('include-names', array());
		$additionalData = Yii::app()->request->getParam('additionalData', array());
		$selectedUserFields = Yii::app()->request->getParam('selectedUserFields', array());
		$selectedAdditionalFields = Yii::app()->request->getParam('selectedAdditionalFields', array());

		if(!empty($additionalData))
			$additionalData = json_decode($additionalData, true);

		$cacheKey = Yii::app()->request->getParam('_key_', '');


		if ($fileType == 'export_users_xls') {
			$exportType = Gnumeric::FORMAT_XLS;
		}

		// DOWNLOAD button clicked in the dialog. This is just a signal to close the dialog
		if ($download) {
			echo '<a class="auto-close"></a>';
			Yii::app()->end();
		}


		// first thing first, let's retrive or create the tmp csv filename, since it contains
		// a system path, it's going in session
		$tmp_csv_name = !empty(Yii::app()->session['export_users_tmp_csv_name'])
			? Yii::app()->session['export_users_tmp_csv_name']
			: Sanitize::fileName("Exported-Users-" . rand(1, 9999) . ".csv", true);

		// save the file for next steps
		Yii::app()->session['export_users_tmp_csv_name'] = $tmp_csv_name;

		// Create the temporary path for the file
		$tmp_csv_path = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $tmp_csv_name;

		// Real DOWNLOAD action: a result of JS redirection from the dialog
		if ($downloadGo) {
            unset(Yii::app()->session['export_users_tmp_csv_name']);
            $storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_UPLOADS);
            $storage->sendFile($tmp_csv_name);
			Yii::app()->end();
		}

		//$idstArr = explode(',', $idst); //No more used: the list of users idsts is now readed from cached file

		//read input and prepare cached data if needed
		if (empty($cacheKey)) {
			if (!empty($idst)) {
				//NOTE: if we enter this branch of the "if", then we are starting the progressive operation, since 'idst' input parameter can be passed the first time only.
				//Form submission from assigning dialog: read input data and create local file cache
				$idstArr = explode(',', $idst);

				//

				$cacheKey = $this->_setCachedUsersToBeExported($idstArr);
			} else {
				$result['success'] = false;
				$result['message'] = 'No selected users';
				$result['status'] = 'error';
				$result['stop'] = true;
				$this->sendJSON($result);
				Yii::app()->end();
			}
		}

		//read cached data
		try {
			$idstArr = $this->_getCachedUsersToBeExported($cacheKey);
		} catch (Exception $e) {
			$cacheError = true;
		}
		if (empty($idstArr) || !is_array($idstArr)) { $cacheError = true; } //make sure that the var type is right
		if ($cacheError) {
			//something went wrong while reading cached data ... handle the error
			$result['success'] = false;
			$result['message'] = Yii::t('standard', '_OPERATION_FAILURE');
			$result['status'] = 'error';
			$result['stop'] = true;
			$this->sendJSON($result);
			Yii::app()->end();
		}

		// set a reasonable limit based on number of idsts
		// FABIO NOTE: Why this ? the point here is not to show a progress but to avoid a possibile memory limit hit.
		/*
		$totalNumber = count($idstArr);
		if ($totalNumber < 100) {
			$limit = 5;
		} elseif ($totalNumber >= 100 && $totalNumber < 150) {
			$limit = 7;
		}  elseif ($totalNumber >= 150 && $totalNumber < 200) {
			$limit = 10;
		} elseif ($totalNumber >= 200 && $totalNumber < 300) {
			$limit = 15;
		} elseif ($totalNumber >= 300 && $totalNumber < 500) {
			$limit = 20;
		} elseif ($totalNumber >= 500 && $totalNumber < 1000) {
			$limit = 30;
		} elseif ($totalNumber >= 1000 && $totalNumber < 3000) {
			$limit = 50;
		} elseif ($totalNumber >= 3000 && $totalNumber < 6000) {
			$limit = 70;
		} elseif ($totalNumber >= 6000) {
			$limit = 100;
		}*/
		$totalNumber = count($idstArr);
		$limit = 50;

		$sepSymbol = ','; // separator symbol
		/*Generate separator*/
		switch ($separator) {
			case 1:
				$sepSymbol = ',';
				break;
			case 2:
				$sepSymbol = ';';
				break;
			case 3:
				$sepSymbol = $manualSep;
				break;
		}

		$nextOffset = $offset;

		if ($phase == 'start' || $start) {

			$dynamicColumns = array(
					'idst' => Yii::t('standard', 'User unique ID'),
					'firstname' => Yii::t('standard', '_FIRSTNAME'),
					'lastname' => Yii::t('standard', '_LASTNAME'),
					'email' => Yii::t('standard', '_EMAIL'),
					'lastenter' => Yii::t('standard', '_DATE_LAST_ACCESS'),
					'register_date' => Yii::t('report', '_CREATION_DATE'),
					'level' => Yii::t('standard', '_LEVEL'),
					'language' => Yii::t('standard', '_LANGUAGE'),
					'expiration' => Yii::t('standard', 'Expiration'),
					'email_status' => Yii::t('standard', 'Email validation status'),
			);

			$headers = array(Yii::t('standard', '_USERNAME'),);

			foreach ($selectedUserFields as $k => $v) {
				if (isset($dynamicColumns[$k])) {
					$headers[] = $dynamicColumns[$k];
				}
			}
			/*******- Add the $additionalFields to the $headers[] array -*******/
			/*******- If they are checked in the form                   -*******/
			$additionalData = array();
            $additionalFields = CoreUser::model()->getAdditionalFields();

			foreach ($additionalFields as $aField) {
				if (isset($selectedAdditionalFields[$aField->getFieldId()])) {
					$additionalData[$aField->getFieldId()] = array(
                        'translation' => $aField->getTranslation(),
                        'fieldId' => $aField->getFieldId(),
                        'fieldType' => $aField->getFieldType(),
                        // @todo Find a proper way of returning field's language code, for now return the current language
                        // 'lang_code' => $aField->translationEntry->lang_code,
                        'lang_code' => Lang::getCodeByBrowserCode(Yii::app()->getLanguage()),
					);
					$headers[] = $aField['translation'];
				}
			}

			$fh = fopen($tmp_csv_path, 'w');
			// Write a HEADER line
			if ($includeNames) // If "Include Columns" is checked
				CSVParser::filePutCsv($fh, $headers, $sepSymbol);

			if (!$totalNumber) {
				$totalNumber = count($idstArr);
			}
		} else if ($phase == 'extract') {

			$nextOffset = $offset + $limit;

			$totalNumber = count($idstArr);
			$chunkUsers = (($totalNumber - $offset) < ($offset)) ? ($totalNumber - $offset) : ($offset + $limit);

			if ($chunkUsers !== 0) {

				$sliceSize = (($totalNumber - $offset) < $limit ? ($totalNumber - $offset) : $limit);
				$tmpUsers = array_slice($idstArr, $offset, $sliceSize/*$chunkUsers*/, true);

				// userid (by default) + idst (excluded in output) + select $dataColumns ( checked values from the dialog )
				/* @var $command CDbCommand */
				$command = Yii::app()->db->createCommand();

				$select = 't1.userid, t1.idst';
				foreach ($selectedUserFields as $key => $field) {
					if ($key == 'level') {
						$select .= ', cg.groupid AS level';
						$command->join(CoreGroupMembers::model()->tableName() . ' cgm', 't1.idst = cgm.idstMember');
						$command->join(CoreGroup::model()->tableName() . ' cg', 'cgm.idst = cg.idst AND cg.groupid LIKE "%framework/level%"');
					} elseif ($key == 'language') {
						$select .= ', t2.value AS language';
						$command->leftJoin(CoreSettingUser::model()->tableName() . " t2", "t2.id_user = t1.idst AND t2.path_name = 'ui.language'");
					} else {
						$filedReplace = preg_replace("/[^a-zA-Z-_]+/", "", $key);
						if($filedReplace){
							$select .= ', t1.' . $filedReplace;
						}

					}
				}

				$command->select($select);
				$command->from(CoreUser::model()->tableName() . ' t1');
				//$command->where('t1.idst IN (' . implode(',', $tmpUsers) . ')');
				$command->where(array('IN', 't1.idst', $tmpUsers));

				$command->group('userid'); // ?!?
				$users = $command->queryAll(); //NOTE: number of extracted record cannot be greater than $limit value, so we do not worry about output size

				//retrieve all users additional fields values
				$idstList = array();
				$idCommonList = array();
				foreach ($users as $user) { $idstList[] = $user['idst'];/*$useridIdstMap[$user['username']];*/ }
				foreach ($additionalData as $bField) { $idCommonList[] = $bField["fieldId"]; }
				if (!empty($idstList) && !empty($idCommonList)) {
                    $fieldValuesMap = CoreUserField::getValuesForUsers($idstList, true, $idCommonList);

				} else {
					$fieldValuesMap = array();
				}
				if($exportType == Gnumeric::FORMAT_XLS){
					$concatString = "'";
				}else{
					$concatString = '';
				}

				//fetch all users of this chunk
				$users2 = array();
				foreach ($users as $tmpUser) {
					$user = array(); //prepare variable for record values
					foreach ($tmpUser as $_key => $_value) {
						switch ($_key) {
							case 'userid':
								$user[$_key] = Yii::app()->user->getRelativeUsername($_value); //format username value
								break;
							case 'expiration':
								$userFormat = Yii::app()->localtime->getPHPLocalDateFormat();
								$userFormatExpiration = DateTime::createFromFormat('Y-m-d',$_value);
								$user[$_key] = !empty($userFormatExpiration) ? $userFormatExpiration->format($userFormat) : '';
								break;
							case 'email_status':
								$user[$_key] = ($_value === '1') ? Yii::t('standard', 'Verified') : Yii::t('standard', 'Unverified');;
								break;
							default:
								$user[$_key] = $_value; //simply copy the value in the output record
								break;
						}
					}
					//$currentIdst = CoreUser::getIdstByUserId(current($user)); //NOTE: this call executes a query every time it's called. that's why we are using an array of cached data.
					$currentIdst = $tmpUser['idst'];//$useridIdstMap[$user['username']];
					if (isset($user['level']))
						$user['level'] = Yii::t('admin_directory', '_DIRECTORY_' . $user['level']);
					if (isset($user['lastenter'])) {
						$user['lastenter'] = ($user['lastenter'] == '0000-00-00 00:00:00' || empty($user['lastenter'])
								? ''
								: Yii::app()->localtime->toLocalDateTime($user['lastenter']));
					}
					if (isset($user['register_date'])) {
						$user['register_date'] = ($user['register_date'] == '0000-00-00 00:00:00' || empty($user['register_date'])
								? ''
								: Yii::app()->localtime->toLocalDateTime($user['register_date']));
					}

                    foreach ($additionalData as $bField) {
						$fieldValue = (isset($fieldValuesMap[$currentIdst][$bField['fieldId']])
								? $fieldValuesMap[$currentIdst][$bField['fieldId']]
								: '');

						switch ($bField['fieldType']) {
							case CoreUserField::TYPE_YESNO:
								$fieldValue = FieldYesno::getLabelFromValue($fieldValue);
								break;
							case CoreUserField::TYPE_COUNTRY:
								$fieldValue = FieldCountry::model()->renderFieldValue($fieldValue);
								break;
							case CoreUserField::TYPE_UPLOAD:
								$fieldValue = '"N/A"';
								break;
							case CoreUserField::TYPE_DROPDOWN:
								$model = new FieldDropdown();
								$model->id_field = $bField['fieldId'];
								$fieldValue = $model->renderFieldValue($fieldValue);
								break;
							default: //no transformation operation needed in this case
								break;
						}

						$user[$bField['fieldId']] = $fieldValue;
					}

					/*
					 * We DO need to user idst parameter in order to make the proper calculation for the his additional fields.
					 * So the idst column will always appear at the report no matter if the user were selected it.
					 * Now we will check if the column where selected in the very beginning and if is not we will remove if from the export file.
					 */
					if(!isset($selectedUserFields['idst']) && isset($user['idst']))
						unset($user['idst']);

					if(isset($user) && is_array($user) && ($exportType == Gnumeric::FORMAT_XLS)){
						foreach($user as $key => $val){
							$user[$key] = "'".$val;
						}
					}

					$users2[] = $user;
				}

				/****************- INSERT ADDITIONAL FIELDS -****************/

				$fh = fopen($tmp_csv_path, 'a');

				$value = null; // not used
				if ($users2 && is_array($users2) && !empty($users2)) {
					CSVParser::filePutCsvMultiple($fh, $users2, $sepSymbol);
				}
			}

		} else if ($phase == 'convert') {

			$gnumeric = new Gnumeric();
			$pathInfo = pathinfo($tmp_csv_path);
			$extension = $exportType;
			$fileName = $pathInfo['filename'] . "." . $extension;

			$out = $pathInfo['dirname'] . DIRECTORY_SEPARATOR . $fileName;

			$format = false;

			switch ($exportType) {
				case LearningReportFilter::EXPORT_TYPE_CSV:
					$format = Gnumeric::FORMAT_CSV;
					break;
				case LearningReportFilter::EXPORT_TYPE_XLS;
					$format = Gnumeric::FORMAT_XLS;
					break;
			}
			$result = $gnumeric->convertTo($tmp_csv_path, $out, $format);

			if (!$result) {
				throw new CException($gnumeric->getError());
			}

			FileHelper::removeFile($tmp_csv_path);
			Yii::app()->session['export_users_tmp_csv_name'] = basename($out);
		}
		if (isset($fh)) {
			fclose($fh);
		}

		$exportingNumber = $nextOffset + $limit;
		$exportingNumber = min($exportingNumber, $totalNumber);

		// Also, in percentage ...
		$exportingPercent = 0;
		if ($totalNumber > 0) {
			$exportingPercent = $exportingNumber / $totalNumber * 100;
		}

		// 'Phase' control
		switch ($phase) {
			case 'start':      // after 'start', we begin extraction
				$nextPhase = 'extract';
				break;
			case 'extract':    // we continue extracttion phase until end-of-records; then switch to 'convert'/'download'
				$nextPhase = $phase;
				if ($nextOffset > $totalNumber) {
					if (($exportType == LearningReportFilter::EXPORT_TYPE_XLS) && ($totalNumber > Gnumeric::MAX_ROWS_XLS)) {
						$exportType = LearningReportFilter::EXPORT_TYPE_CSV;
						$nextPhase = 'download';
						$forceCsvMaxRows = true;
					} else {
						// Check if we have healthy Gnumeric installation
						$gnumeric = new Gnumeric();
						$check = $gnumeric->checkGnumeric();
						if (!$check) {
							$forceCsvMissingGnumeric = true;
							$exportType = LearningReportFilter::EXPORT_TYPE_CSV;
							$nextPhase = 'download';
						} else {
							$nextPhase = $exportType == LearningReportFilter::EXPORT_TYPE_CSV ? 'download' : 'convert';
						}
					}
				}
				break;
			case 'convert':
				$nextPhase = 'download';
				break;
		}

		$stop = ($nextPhase == 'download') || ($totalNumber <= 0);

		//we are finished here, we do not need cache file anymore
		if ($stop && !empty($cacheKey)) {
			$this->_unsetCachedUsersToBeExported($cacheKey);
		}

		$nextOffset = !empty($nextOffset) ? $nextOffset : 0;
		$html = $this->renderPartial('axProgressiveExportUsers', array(
				'totalNumber' => $totalNumber,
				'exportingNumber' => $exportingNumber,
				'exportingPercent' => $exportingPercent,
				'nextPhase' => $nextPhase,
				'exportType' => $exportType,
				'forceCsvMaxRows' => $forceCsvMaxRows,
				'forceCsvMissingGnumeric' => $forceCsvMissingGnumeric,
				'stop' => $stop,
				'separator' => $separator,
				'type' => $type,
				'fileType' => $fileType,
				'cacheKey' => $cacheKey
		), true, true);

		$data = array(
				'offset' => $nextOffset,
				'stop' => $stop,
				'totalNumber' => $totalNumber,
				'exportType' => $exportType,
				'phase' => $nextPhase,
				'custom_filter' => $customFilter,
				'_key_' => $cacheKey,
				'separator' => $separator,
				'type' => $type,
				'fileType' => $fileType,
				'dataColumns' => $dataColumns,
				'manual-separator' => $manualSep,
				'additionalData' => !empty($additionalData) ? json_encode($additionalData) : $additionalData,
				'selectedUserFields' => $selectedUserFields,
				'selectedAdditionalFields' => $selectedAdditionalFields,
		);

		$response->setStatus(true);
		$response->setHtml($html)->setData($data)->toJSON();
		Yii::app()->end();

	}




	/**
	 * An action responsible for Progressively Importing users for a CSV file.
	 * Used in conjunction with  ProgressController, providing ajax based, 'chunked' job execution (importing users in portions)
	 *
	 */
	public function actionAxProgressiveUsersImport() {

		$response = new AjaxResult();
		$defaultLimit = 5;

		$start 			= Yii::app()->request->getParam('__progress_start__', false);
		$phase			= Yii::app()->request->getParam('phase', 'start');
		$limit 			= (int) Yii::app()->request->getParam('limit', $defaultLimit);
		$offset 		= (int) Yii::app()->request->getParam('offset', 0);
		$csvOffset 		= (int) Yii::app()->request->getParam('csv_offset', 0);

		// These are coming from the Step2 UI, which calls the Progressive controller
		$UserImportForm = Yii::app()->request->getParam('UserImportForm', array());
		$importMap		= Yii::app()->request->getParam('import_map', 0);
		$insertUpdate	= isset($UserImportForm['insertUpdate']) ? $UserImportForm['insertUpdate'] : false;
		$ignoreForcePasswordChange	= isset($UserImportForm['ignorePasswordChangeForExistingUsers']) ? $UserImportForm['ignorePasswordChangeForExistingUsers'] : false;

		$cacheKey = Yii::app()->request->getParam('cacheKey', false);

		try {
			/* @var $importForm UserImportForm */
			// Get the importForm object from the session, saved there earlier by previous steps or runs
			$importForm = Yii::app()->session['importUsersModel'];

			// Check if session data is good, at least the class name
			if (get_class($importForm) != 'UserImportForm') {
				throw new CException(Yii::t('standard', 'Invalid session data'));
			}

			// At the first call to this progressive importer, add data to the model from the LAST Step and save back to session
			if ($phase=='start' || $start) {

				$importForm->scenario = 'step_two';
				$importForm->clearErrors();
				$importForm->cumulativeFailedLines = array();
				$importForm->failedLines = array();
				$importForm->setImportMap($importMap);
				$importForm->insertUpdate = $insertUpdate;
				$importForm->ignorePasswordChangeForExistingUsers = (bool)$ignoreForcePasswordChange;
				$importForm->successCounter = 0;

				// Also, set (and save to session) a model attribute to keep power user branches, used in a loop later, if any
				if (Yii::app()->user->getIsPu()) {

					$onlyLeafs = false;
					$includeAncestors = true;
					if (Yii::app()->user->isAdmin)
					{
						$event = new DEvent($this, array());
						Yii::app()->event->raise('PowerUserOrgChartGeneration', $event);

						if (!$event->shouldPerformAsDefault()) {
							$onlyLeafs = true;
							$includeAncestors = false;
						}
					}

					$puBrancherInfo = CoreAdminTree::model()->getPowerUserOrgChartsOnly(Yii::app()->user->id, $includeAncestors);
					if ($onlyLeafs)
						$importForm->powerUserBranches = $puBrancherInfo['leafs'];
					else
						$importForm->powerUserBranches = $puBrancherInfo['nodes'];

					if (!$importForm->powerUserBranches) {
						$importForm->powerUserBranches = array();
					}
				}


				// Note, even you do this here, any change to the object attributes LATER on, will be also saved in the session variable
				// That's the way PHP (or Yii) works.
				Yii::app()->session['importUsersModel'] = $importForm;

				// Validate data
				$importForm->hasErrors();

                if ($importForm->hasErrors()) {
					$message = '';
					foreach ($importForm->errors as $attrErrors)
						foreach ($attrErrors as $attr => $error)
							$message .= $error . "<br>";

					$html = $this->renderPartial('admin.protected.views.common._dialog2_error', array(
							'type' => 'error',
							'message' => $message,
					),true);

					$data = array('stop' => true);
					$response->setStatus(true);
					$response->setHtml($html)->setData($data)->toJSON();
					$importForm->clearErrors();
					Yii::app()->end();
				}

			}

			//try to set a more appropriate $limit parameter, if needed, but keep chunck sizes into a reasonable range
			$checkTotalUsers = $importForm->totalUsers;
			if ($checkTotalUsers < 30) {
				$limit = $defaultLimit;
			} elseif ($checkTotalUsers >= 30 && $checkTotalUsers <= 100) {
				$limit = 10;
			} elseif ($checkTotalUsers >= 100 && $checkTotalUsers <= 200) {
				$limit = 15;
			} elseif ($checkTotalUsers >= 200) {
				$limit = 20;
			}

            //
			// GOOD! At this point we should have ALL the data we need for the import
			//

			// A CSV file where we are going to collect errors, one line per user, with the error as first field
			$csvErrorFileName = 'errors_' . basename($importForm->originalFilename);
			$csvErrorFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $csvErrorFileName;

			// Initial state
			$nextOffset = $offset;
			$nextCsvOffset = $csvOffset;

			// Act according to CURRENT run phase
			if ($phase =='start') {
				// So far, nothing to do

				//set cache file (this will list effectively created/updated users in a flat IDSTs list, to be used in later operations
				$cacheKey = $this->_setCacheFile('_users_imported_');


				if (Yii::app()->backgroundJob->bgJobEnabled(BackgroundJob::FAMILTY_IMPORTFROMCSV)) {
					$filename = explode(DIRECTORY_SEPARATOR, $importForm->localFile);
					$filename = $filename[count($filename) - 1];
					//Niki's code
					$input = array(
							'filename' => $filename,
							'firstRowAsHeader' => $importForm->firstRowAsHeader,
							'totalUsers' => $importForm->totalUsers,
					);

					$params = array(
							'filename' => $filename,
							'charset' => $importForm->charset,
							'separator' => $importForm->separator,
							'manualSeparator' => $importForm->manualSeparator,
							'node' => $importForm->node,
							'firstRowAsHeader' => $importForm->firstRowAsHeader,
							'passwordChanging' => $importForm->passwordChanging,
							'UserImportForm' => $UserImportForm,
							'import_map' => $importForm->getImportMap(),
							'insertUpdate' => $importForm->insertUpdate,
							'totalUsers' => $importForm->totalUsers,
							'unenroll_deactivated' => $importForm->unenroll_deactivated,
							'activation' => $importForm->activation,
							'powerUserBranches' => $importForm->powerUserBranches,
							'importTabType' => $importForm->importTabType,
							'autoAssignBranches' => $importForm->autoAssignBranches
//						'importForm_isValidated'=> $date_expire_validity,
					);

					$jobName = 'Importing';

					if($importForm->importTabType === UserImportForm::SCENARIO_ACTIVATION){
						if($importForm->activation == 'yes'){
							$jobName = 'Activating';
						} else if($importForm->activation == 'no'){
							$jobName = 'Deactivating';
						}
					}

					$this->createBackgroundJob(array(
							'module' => 'user',
							'value' => $jobName . ' {users} users from {file}',
							'params' => array(
									'users' => $importForm->totalUsers,
									'file' => $importForm->originalFilename
							)
					), 'lms.protected.modules.backgroundjobs.components.handlers.ImportUsersFromCSV', $input, $params);

//					$response->setStatus(true);
//					$response->toJSON();
					Yii::app()->session['openJobsPanel'] = true;
					$this->renderPartial('lms.protected.views.site._js_redirect', array('url' => Docebo::createAdminUrl("userManagement/index")));
					Yii::app()->end();
				}
			}
			// IMPORT users
			else if ($phase == 'import') {

				$nextOffset = $offset + $limit;

				$delimiter = UserImportForm::translateSeparatorName($importForm->separator);

				// Create CSV manager, set numLines = 1 (1 user at max, just to have the object)
				$csvFile = new QsCsvFile(array(
						'path' => $importForm->localFile,
						'charset' => $importForm->charset,
						'delimiter' => $delimiter,
						'firstRowAsHeader' => $importForm->firstRowAsHeader,
						'numLines' => 1, // excluding header, if any
				));

				// Get portion (chunk) of users...  and import them
				//$chunkData = $csvFile->getChunk($limit, $offset);
				$tmpChunk = $csvFile->getChunkNoEmptyLines($limit, $csvOffset);
				$chunkData = $tmpChunk['data'];
				$nextCsvOffset += $limit + $tmpChunk['emptyLines'];

				// Set Form data to chunk data
				$importForm->setData($chunkData);
				$importForm->failedLines = array();

                $importForm->saveUsers(false, $offset === 0, $offset);  // Save users, do NOT validate, skip header if this is the very first 'page'
                $importForm->cumulativeFailedLines = array_merge($importForm->cumulativeFailedLines, $importForm->failedLines);

				if (!empty($cacheKey)) {
					$savedUsers = $importForm->getCreatedUsers();
					if (is_array($savedUsers) && !empty($savedUsers)) {
						$this->_addCacheFileContent($cacheKey, implode(',', $savedUsers) . ',');
					}
				}
			}
			// 'finish' button clicked, redirect to main page
			else if ($phase == 'finish') {

				if (!empty($cacheKey)) {
					$cachedSavedUsers = $this->_getCacheFileContent($cacheKey);
					$cachedSavedUsers = trim($cachedSavedUsers, ',');
					if (!empty($cachedSavedUsers)) {
						$savedUsers = explode(',', $cachedSavedUsers);
						//set job for assigning rules, if needed
						GroupAutoAssignTask::setUpdateJob($savedUsers);
						//set job for power user updating, if needed
						if (PluginManager::isPluginActive('PowerUserApp')) {
							PowerUserAppModule::setUpdateJob($savedUsers);
						}
					}
					$this->_unsetCacheFile($cacheKey);
				}

				// Redirect to main User Management page
				$this->renderPartial('lms.protected.views.site._js_redirect', array('url' => Docebo::createAdminUrl("userManagement/index")));
				Yii::app()->end();
			}

			// Calculate the extraction offset for the NEXT (!) request which we show NOW!
			// i.e. in current render we show what is GOING TO BE done in the next cycle
			$totalNumber 		= $importForm->totalUsers;
			$number 			= $nextOffset + $limit;
			$number 	= 		min($number, $totalNumber);

			// Also, in percentage ...
			$percent = 0;
			if ($totalNumber > 0) {
				$percent =  $number / $totalNumber * 100;
			}


			// 'Phase' control
			switch ($phase) {
				case 'start':  		// after 'start', we begin importing
					$nextPhase = 'import';
					break;
				case 'import':
					$nextPhase = $phase;
					if ($nextOffset > $totalNumber) {
						$nextPhase = 'finish';

						// See if we have some collected failed lines
						// Write them to the "errors" file
						if (count($importForm->cumulativeFailedLines) > 0) {
							$fh = fopen($csvErrorFilePath, 'w');
							foreach ($importForm->cumulativeFailedLines as $failedLine) {
								fputs($fh, $failedLine . "\n");
							}
							fclose($fh);
						}
					}
					break;

			}

			// We completely stop calling ourselves when we reach the end
			$stop = $nextOffset > $importForm->totalUsers;


			$html = $this->renderPartial('_import_progress', array(
					'importForm' 		=> $importForm,
					'totalNumber'		=> $importForm->totalUsers,
					'number'			=> $number,
					'percent'			=> $percent,
					'nextPhase'			=> $nextPhase,
					'csvErrorFileName'	=> $csvErrorFileName,
					'cacheKey' => $cacheKey
			), true, true);


			// These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stoppping when $stop == true)
			$data = array(
					'offset'						=> $nextOffset,
					'stop'							=> $stop,
					'phase'							=> $nextPhase,
					'csv_offset'				=> $nextCsvOffset,
					'cacheKey'					=> $cacheKey
			);

			$response->setStatus(true);
			$response->setHtml($html)->setData($data)->toJSON();
			Yii::app()->end();
		}
		catch (CException $e) {
			if (!empty($cacheKey)) { $this->_unsetCacheFile($cacheKey); }
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			$response->setStatus(false)->setMessage($e->getMessage());
			$response->toJSON();
			Yii::app()->end();
		}

		Yii::app()->end();

	}



	public function actionEndUserCsvImport() {
		//import dialog has not been terminated by pressing "cancel" button, so we have to finish our job here
		$cacheKey = Yii::app()->request->getParam('cacheKey', false);
		if (!empty($cacheKey)) {
			$cachedSavedUsers = $this->_getCacheFileContent($cacheKey);
			$cachedSavedUsers = trim($cachedSavedUsers, ',');
			if (!empty($cachedSavedUsers)) {
				$savedUsers = explode(',', $cachedSavedUsers);
				//set job for assigning rules, if needed
				GroupAutoAssignTask::setUpdateJob($savedUsers);
				//set job for power user updating, if needed
				if (PluginManager::isPluginActive('PowerUserApp')) {
					PowerUserAppModule::setUpdateJob($savedUsers);
				}
			}
			$this->_unsetCacheFile($cacheKey);
		}
		//jsut go back to main page
		$this->redirect(Docebo::createAdminUrl('userManagement/index'));
	}




	protected function _setCacheFile($name, $content = '') {
		//generate a key (it will be used as file name for cached data)
		$cacheKey = $this->_generateCacheKey($name);

		$pathName = Docebo::getUploadTmpPath();
		$fileName = $cacheKey.'.tmp';
		$file = fopen($pathName . DIRECTORY_SEPARATOR . $fileName, 'w');
		if (!$file) {
			//TODO: handle error
		}
		if (!empty($content)) { fwrite($file, $content); }
		fclose($file);

		return $cacheKey;
	}

	protected function _getCacheFileContent($cacheKey) {
		$pathName = Docebo::getUploadTmpPath();
		$fileName = $cacheKey.'.tmp';
		if (!file_exists($pathName . DIRECTORY_SEPARATOR . $fileName)) { throw new CException('Cache file not found [key: '.$cacheKey.']'); }
		$content = file_get_contents($pathName . DIRECTORY_SEPARATOR . $fileName);
		return $content;
	}

	protected function _addCacheFileContent($cacheKey, $content) {
		$pathName = Docebo::getUploadTmpPath();
		$fileName = $cacheKey.'.tmp';
		if (!file_exists($pathName . DIRECTORY_SEPARATOR . $fileName)) { throw new CException('Cache file not found [key: '.$cacheKey.']'); }
		$file = fopen($pathName . DIRECTORY_SEPARATOR . $fileName, 'w');
		if (!empty($content)) { fwrite($file, $content); }
		fclose($file);
	}

	protected function _unsetCacheFile($cacheKey) {
		$pathName = Docebo::getUploadTmpPath();
		$fileName = $cacheKey.'.tmp';
		unlink($pathName . DIRECTORY_SEPARATOR . $fileName);
	}




	protected function _generateCacheKey($prefix = '') {
		return uniqid($prefix).'_'.time();
	}

	protected function _setCachedUsersToBeAssigned(array $branches, array $users) {

		//extract groups IDSTs from selected nodes
		$membershipGroupsToAdd = array();
		$reader = Yii::app()->db->createCommand()
				->select('idOrg, idst_oc, idst_ocd')
				->from(CoreOrgChartTree::model()->tableName())
				->where(array('IN', 'idOrg', $branches))
				->query();
		while ($node = $reader->read()){
			$membershipGroupsToAdd[] = array(
					$node['idst_oc'],
					$node['idst_ocd']
			);
		}

		//prepare data structure to be cached into file
		$cacheArray = array(
				'groups' => array(),
				'users' => array()
		);
		foreach ($users as $user) {
			$cacheArray['users'][] = (int)$user;
		}
		foreach ($membershipGroupsToAdd as $branch) {
			$cacheArray['groups'][] = array(
					(int)$branch[0],
					(int)$branch[1]
			);
		}
		$content = CJSON::encode($cacheArray);

		//generate a key (it will be used as file name for cached data)
		$cacheKey = $this->_generateCacheKey('_users_to_be_assigned_');

		$pathName = Docebo::getUploadTmpPath();
		$fileName = $cacheKey.'.tmp';
		$file = fopen($pathName . DIRECTORY_SEPARATOR . $fileName, 'w');
		if (!$file) {
			//TODO: handle error
		}
		fwrite($file, $content);
		fclose($file);

		return $cacheKey;
	}

	protected function _unsetCachedUsersToBeAssigned($cacheKey) {
		$pathName = Docebo::getUploadTmpPath();
		$fileName = $cacheKey.'.tmp';
		unlink($pathName . DIRECTORY_SEPARATOR . $fileName);
	}

	protected function _getCachedUsersToBeAssigned($cacheKey) {
		$pathName = Docebo::getUploadTmpPath();
		$fileName = $cacheKey.'.tmp';
		if (!file_exists($pathName . DIRECTORY_SEPARATOR . $fileName)) { throw new CException('Cache file not found [key: '.$cacheKey.']'); }
		$content = file_get_contents($pathName . DIRECTORY_SEPARATOR . $fileName);
		return CJSON::decode($content, true);
	}

	public function actionAxProgressiveUsersAssignToSingleNode(){
		$chunkSize = 100;
		$chart = null;
		$initial = true ;
		if(isset($_POST['idOrg']) && $_POST['idOrg']){
			$chart = CoreOrgChartTree::model()->findByPk($_POST['idOrg']);

		}

		if (!$chart ) {
			$result['success'] = false;
			$result['message'] = 'No selected nodes';
			$result['status'] = 'error';
			$result['stop'] = true;
			$this->sendJSON($result);
			Yii::app()->end();
		}

		if (!isset($_POST['_key_'])){
			$usersStr = trim(Yii::app()->request->getParam('idst', ''));
			if (empty($usersStr)) {
				$result['success'] = false;
				$result['message'] = 'No selected users';
				$result['status'] = 'error';
				$result['stop'] = true;
				$this->sendJSON($result);
				Yii::app()->end();
			}
			$users = explode(",", $usersStr);


			$cacheKey = $this->_setCachedUsersToBeAssigned(array(), $users);
		}else{
			// Get the users array from the cache
			$cacheKey = Yii::app()->request->getParam('_key_', 0);
		}


		//read cached data
		try {
			$res = $this->_getCachedUsersToBeAssigned($cacheKey);
		} catch (Exception $e) {
			$cacheError = true;
		}

		if (empty($res) || !is_array($res)) { $cacheError = true; } //make sure that the var type is right

		if ($cacheError) {
			//something went wrong while reading cached data ... handle the error
			$result['success'] = false;
			$result['message'] = Yii::t('standard', '_OPERATION_FAILURE');//$e->getMessage();
			$result['status'] = 'error';
			$result['stop'] = true;
			$this->sendJSON($result);
			Yii::app()->end();
		}

		// Get current offset and calculate stop flag
		$offset = Yii::app()->request->getParam('offset', 0);
		$totalCount = count($res['users']);
        $progressBarCount = Yii::app()->request->getParam('counter', '');
		$newOffset = $offset + $chunkSize;
		$stop = ($newOffset < $totalCount) ? false : true;

		// Preparing result for importer
		$result = array (
			'usersAssigned' => 0,
			'usersFailed' => 0,
		);

		$initial = Yii::app()->request->getParam('initial', true);
		if($initial && $chart && $users &&  is_array($users)){
			CoreGroupMembers::model()->deleteAllByAttributes(array('idst' => $chart->idst_oc), array(
				'condition'=>'idstMember NOT IN (:userIds)',
				'params'=>array(':userIds'=>$users)
			));
			CoreGroupMembers::model()->deleteAllByAttributes(array('idst' => $chart->idst_ocd), array(
				'condition'=>'idstMember NOT IN  (:userIds)',
				'params'=>array(':userIds'=>$users)
			));
		}


		$counter = 0;
		for ($index = $offset; ($index < $newOffset && $index < $totalCount); $index++) { //NOTE: $newOffset may be greater than $totalCount, but we wouldn't count over $totalCount

			//security check
			$counter++;
			if ($counter > $chunkSize) break; //NOTE: this shouldn't happen

			//read user ID, if we are over user list limit, then invalidate the value and stop this iteration
			$user = (isset($res['users'][$index]) ? $res['users'][$index] : -1);
			if ($user < 0) continue;

			//main operations
			try {

				//validate input user
				if (!$user) { throw new CException('Invalid user (index: '.$index.')'); }

				//check OC
				$exists = CoreGroupMembers::model()->find(array(
					'condition' => 'idst = :idst_oc AND idstMember = :userId',
					'params' => array(':idst_oc' => $chart->idst_oc, ':userId' => $user)
				));
				if (!$exists)
				{
					$groupMember = new CoreGroupMembers();
					$groupMember->idst = $chart->idst_oc;
					$groupMember->idstMember = $user;
					$groupMember->save(false);
				}

				//check OCD
				$exists = CoreGroupMembers::model()->find(array(
					'condition' => 'idst = :idst_ocd AND idstMember = :userId',
					'params' => array(':idst_ocd' => $chart->idst_ocd, ':userId' => $user)
				));
				if (!$exists)
				{
					$groupMember = new CoreGroupMembers();
					$groupMember->idst = $chart->idst_ocd;
					$groupMember->idstMember = $user;
					$groupMember->save(false);
				}


				$result['usersAssigned']++;
			} catch(Exception $e) {
				Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
				$result['usersFailed']++;
			}


		}




		//render dialog
		if ($index > $progressBarCount) { $index = $progressBarCount; } //normalize value
        if (!$progressBarCount)
            $completedPercent = 100;
		else
		    $completedPercent = ($progressBarCount == 0 ? 0 : (number_format($index/$progressBarCount * 100, 0)));
		$html = $this->renderPartial('single_assign_progressive_dialog', array(
			'offset' => $offset,
			'assigned' => $result['usersAssigned'],
			'failed' => $result['usersFailed'],
			'processedUsers' => $index,
			'totalUsers' => $progressBarCount,
			'completedPercent' => $completedPercent,
			'start' => ($offset == 0),
			'stop' => $stop,
			'cacheKey' => $cacheKey,
			'idOrg' => $chart->idOrg,
 		), true, true);

		//we are finished here, we do not need cache file anymore
		if ($stop) {
			$this->_unsetCachedUsersToBeAssigned($cacheKey);

			//TODO: this is better to be handled by events instead directly here !!!
			if (PluginManager::isPluginActive('PowerUserApp')) {
				//power users assignments may need to be updated
				if (is_array($res) && isset($res['users']) && !empty($res['users'])) {
					PowerUserAppModule::setUpdateJob($res['users']);
				}
			}
		}

		// Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
		// These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stopping when $stop == true)
		$data = array(
			'offset'	=> $newOffset,
			'stop'		=> $stop,
			//specific parameters for this function
			'_key_'		=> $cacheKey,
			'initial'  =>  $initial,
		);
		$response = new AjaxResult();
		$response->setStatus(true);
		$response->setHtml($html)->setData($data)->toJSON();
		Yii::app()->end();

	}

	public function actionAxProgressiveUsersAssignToNode() {

		//define chunk size
		$chunkSize = 10;

		//read input and preapre cached data
		if (isset($_POST['select-orgchart']) && !isset($_POST['_key_'])) {

			//NOTE: if we enter this branch of the "if", then we are starting the progressive operation, since 'select-orgchart' input parameter is passed the first time only.

			//Form submission from assigning dialog: read input data and create local file cache
			$usersStr = trim(Yii::app()->request->getParam('idst', ''));
			if (empty($usersStr)) {
				$result['success'] = false;
				$result['message'] = 'No selected users';
				$result['status'] = 'error';
				//$result['html'] = Yii::t('standard', '_OPERATION_FAILURE');
				$result['stop'] = true;
				$this->sendJSON($result);
				Yii::app()->end();
			}
			$users = explode(",", $usersStr);

			// Collect IDs of org chart nodes selected
			$selectedNodes = array();
			$puBranchIdsArray = CoreOrgChartTree::getPuBranchIds();
			foreach ($_POST['select-orgchart'] as $nodeId => $selectState) {
				if ($selectState && (!Yii::app()->user->getIsPu() || (Yii::app()->user->getIsPu() && in_array($nodeId, $puBranchIdsArray))))
					$selectedNodes[] = $nodeId;
			}
			if (empty($selectedNodes)) {
				$result['success'] = false;
				$result['message'] = 'No selected nodes';
				$result['status'] = 'error';
				//$result['html'] = Yii::t('standard', '_OPERATION_FAILURE');
				$result['stop'] = true;
				$this->sendJSON($result);
				Yii::app()->end();
			}

			$cacheKey = $this->_setCachedUsersToBeAssigned($selectedNodes, $users);

		} else {

			// Get the users array from the cache
			$cacheKey = Yii::app()->request->getParam('_key_', 0);
		}

		//read other input data
		if (!isset($selectedNodes)) { $selectedNodes = Yii::app()->request->getParam('selected_nodes', ''); }
		$assignType = strtolower(Yii::app()->request->getParam('assign_type', ''));
		if ($assignType != 'move')  { $assignType = 'append'; } //make sure assign_type value is right

		//read cached data
		try {
			$res = $this->_getCachedUsersToBeAssigned($cacheKey);
		} catch (Exception $e) {
			$cacheError = true;
		}
		if (empty($res) || !is_array($res)) { $cacheError = true; } //make sure that the var type is right
		if ($cacheError) {
			//something went wrong while reading cached data ... handle the error
			$result['success'] = false;
			$result['message'] = Yii::t('standard', '_OPERATION_FAILURE');//$e->getMessage();
			$result['status'] = 'error';
			//$result['html'] = Yii::t('standard', '_OPERATION_FAILURE');
			$result['stop'] = true;
			$this->sendJSON($result);
			Yii::app()->end();
		}

		// Get current offset and calculate stop flag
		$offset = Yii::app()->request->getParam('offset', 0);
		$totalCount = count($res['users']);
		$newOffset = $offset + $chunkSize;
		$stop = ($newOffset < $totalCount) ? false : true;

		// Preparing result for importer
		$result = array (
				'usersAssigned' => 0,
				'usersFailed' => 0,
		);

		//get flat list of branches groups from $res
		$branchGroups = array();
		foreach ($res['groups'] as $groups) {
			$branchGroups[] = $groups[0];
			$branchGroups[] = $groups[1];
		}

		//prepare some data to be used later, if needed
		if ($assignType == 'move') {

			//Removing the users from the old nodes
			//Add the oc_0 and ocd_0 to the exclusion list
			$_roots = CoreOrgChartTree::model()->roots()->findAll();
			$rootNode = $_roots[0];

			//retrieve all orgchart nodes (except root node)
			$nodesReader = Yii::app()->getDb()->createCommand()
					->select('idOrg, idst_oc, idst_ocd')
					->from(CoreOrgChartTree::model()->tableName())
					->where("idOrg <> :id_root")
					->query(array(':id_root' => $rootNode->getPrimaryKey()));
			$allNodes = array();
			while ($node = $nodesReader->read()) {
				$allNodes[] = (int)$node['idst_oc'];
				$allNodes[] = (int)$node['idst_ocd'];
			}

			//remove input nodes from all nodes selection
			$toBeRemoved = array_diff($allNodes, $branchGroups);

			//free memory
			$allNodes = NULL;
			unset($allNodes);
		}

		//main loop
		$toBeUnselected = array();
		$counter = 0;
		for ($index = $offset; ($index < $newOffset && $index < $totalCount); $index++) { //NOTE: $newOffset may be greater than $totalCount, but we wouldn't count over $totalCount

			//security check
			$counter++;
			if ($counter > $chunkSize) break; //NOTE: this shouldn't happen

			//read user ID, if we are over user list limit, then invalidate the value and stop this iteration
			$user = (isset($res['users'][$index]) ? $res['users'][$index] : -1);
			if ($user < 0) continue;

			//main operations
			try {

				//validate input user
				if (!$user) { throw new CException('Invalid user (index: '.$index.')'); }

				//check if the current user is already assigned to target branch(es)
				$existentGroups = array();
				$newNodes = array();
				$deletedNodes = array();
				$reader = Yii::app()->db->createCommand()
						->select("*")
						->from(CoreGroupMembers::model()->tableName())
						->where("idstMember = :id_user")
//					->andWhere(array("IN", "idst", $branchGroups))
						->query(array(':id_user' => $user));
				while ($row = $reader->read()) {
					if(in_array((int)$row['idst'],$branchGroups)) {
						$existentGroups[] = (int)$row['idst'];
					}
					else{
						//Get all old branches that are going to be removed by idOrg
						$deletedNodes[] = CoreOrgChartTree::getIdOrgByOcOrOcd($row['idst']);
					}
				}

				foreach ($branchGroups as $branchGroup) {
					if (!in_array($branchGroup, $existentGroups)) {
						$newMember = new CoreGroupMembers();
						$newMember->idst = $branchGroup;
						$newMember->idstMember = $user;
						$newMember->save(); //this will make sure that all related event listeners are called
						//Get all new branches by idOrg
						$newNodes[] = CoreOrgChartTree::getIdOrgByOcOrOcd($branchGroup);
					}
				}
				//Clearing the arrays
				$newNodes = array_unique($newNodes);
				$deletedNodes = array_unique($deletedNodes);
				if (($key = array_search(CoreOrgChartTree::getOrgRootNode()->idOrg, $deletedNodes)) !== false) {
					unset($deletedNodes[$key]);
				}
				if (($key = array_search(CoreOrgChartTree::getOrgRootNode()->idOrg, $newNodes)) !== false) {
					unset($newNodes[$key]);
				}
				//Raise events for the brand NEW branches
				foreach($newNodes as $newNode){
					Yii::app()->event->raise(CoreNotification::NTYPE_USER_ASSIGNED_TO_BRANCH, new DEvent($this, array(
							'targetUser' => $user,
							'BranchIdOrg'  => $newNode
					)));
				}

				//the moving action has some further operations to be done
				if ($assignType == 'move') {
					//delete users assignments from remaining nodes
					CoreGroupMembers::model()->deleteAllByAttributes(
							array('idstMember' => $user),
							"idst IN (".implode(',', $toBeRemoved).")"
					);
					//Raise events for the OLD branches that are going to be removed
					foreach($deletedNodes as $deletedNode){
						Yii::app()->event->raise(CoreNotification::NTYPE_USER_REMOVED_FROM_BRANCH, new DEvent($this, array(
								'targetUser' => $user,
								'BranchIdOrg'  => $deletedNode
						)));
					}
				}

				$result['usersAssigned']++;
				$toBeUnselected[] = (int)$user;

			} catch(Exception $e) {
				Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
				$result['usersFailed']++;
			}

		}

		//render dialog
		if ($index > $totalCount) { $index = $totalCount; } //normalize value
		$completedPercent = ($totalCount == 0 ? 0 : (number_format($index/$totalCount * 100, 0)));
		$html = $this->renderPartial('assign_progressive_dialog', array(
				'offset' => $offset,
				'assigned' => $result['usersAssigned'],
				'failed' => $result['usersFailed'],
				'processedUsers' => $index,
				'totalUsers' => $totalCount,
				'totalBranches' => count($res['groups']),
				'completedPercent' => $completedPercent,
				'start' => ($offset == 0),
				'stop' => $stop,
				'toBeUnselected' => $toBeUnselected,
				'cacheKey' => $cacheKey,
				'type' => $assignType
		), true, true);

		//we are finished here, we do not need cache file anymore
		if ($stop) {
			$this->_unsetCachedUsersToBeAssigned($cacheKey);

			//TODO: this is better to be handled by events instead directly here !!!
			if (PluginManager::isPluginActive('PowerUserApp')) {
				//power users assignments may need to be updated
				if (is_array($res) && isset($res['users']) && !empty($res['users'])) {
					PowerUserAppModule::setUpdateJob($res['users']);
				}
			}
		}

		// Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
		// These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stopping when $stop == true)
		$data = array(
				'offset'	=> $newOffset,
				'stop'		=> $stop,
			//specific parameters for this function
				'_key_'		=> $cacheKey,
				'assign_type' => $assignType
		);
		$response = new AjaxResult();
		$response->setStatus(true);
		$response->setHtml($html)->setData($data)->toJSON();
		Yii::app()->end();
	}

	private function enrollNewUser($courseType, $idUser, $idSession, $idCourse){
		switch($courseType){
			case LearningCourse::TYPE_WEBINAR:
				$sessionModel = WebinarSession::model()->findByPk($idSession);
				if ($sessionModel->course_id != $idCourse) {
					throw new CException('Invalid session id (' . $idSession . ')');
				}
				$canContinue = $this->validations($sessionModel, $idUser);
				if ($canContinue) {
					$model = new WebinarSessionUser();
					$this->fillSessionWithData($model, $idSession, $idUser);
				}
				break;
			case LearningCourse::TYPE_CLASSROOM:
				$sessionModel = LtCourseSession::model()->findByPk($idSession);
				if ($sessionModel->course_id != $idCourse) {
					throw new CException('Invalid session id (' . $idSession . ')');
				}

				if (Yii::app()->user->getIsPu() && $sessionModel->lastSubscriptionDatePassed()) {
					throw new CException('Last subscription date passed');
				}
				$canContinue = $this->validations($sessionModel, $idUser);
				if ($canContinue) {
					$model = new LtCourseuserSession();
					$this->fillSessionWithData($model, $idSession, $idUser);
				}
				break;
		}
	}

	private function validations($sessionModel, $idUser){
		$sessionUsers = array();
		foreach ($sessionModel->getEnrolledUsers() as $enrolledUser) {
			$sessionUsers[] = $enrolledUser['id_user'];
		}

		$toEnroll = array();
		if (!in_array($idUser, $sessionUsers)) {
			$toEnroll[] = $idUser;
		}
		if (($sessionModel->countEnrolledUsers(false, true) + count($toEnroll)) > $sessionModel->max_enroll) {
			$courseType = Yii::app()->request->getParam('courseType');
			switch ($courseType) {
				// enroll to session
				case LearningCourse::TYPE_WEBINAR:
					$setCusStatus = WebinarSessionUser::$SESSION_USER_WAITING_LIST;
					WebinarSessionUser::enrollUser($idUser,$sessionModel->id_session,$setCusStatus);
					return false;
				case LearningCourse::TYPE_CLASSROOM:
					$setCusStatus = LtCourseuserSession::$SESSION_USER_WAITING_LIST;
					LtCourseuserSession::enrollUser($idUser, $sessionModel->id_session, $setCusStatus);
					return false;
			}
		}
		return true;
	}

	private function fillSessionWithData($model, $idSession, $idUser){
		$model->id_session = $idSession;
		$model->id_user = $idUser;
		$model->date_subscribed = Yii::app()->localtime->getUTCNow();
		$model->save();
	}
	private function getFutureSessionsCount($idst){
		$data = array(
				'classroomSessionsCount' => 0,
				'webinarSessionsCount' => 0
		);

		if(strpos($idst, ',')){
			$idst = explode(',', $idst);
		}

		if($idst || !empty($idst)){
			if(is_array($idst)){
				$countIltSession = Yii::app()->db->createCommand()
						->select('DISTINCT (lts.id_session) as session')
						->from(LtCourseSession::model()->tableName() . ' lts')
						->join(LtCourseuserSession::model()->tableName() . ' ltsu', 'lts.id_session = ltsu.id_session')
						->where( array('in', 'ltsu.id_user',  $idst ) )
						->andWhere('lts.date_end >= :now', array(
								':now' => Yii::app()->localtime->getUTCNow('Y-m-d H:i:s')
						))
						->queryAll();

				$data['classroomSessionsCount'] = count($countIltSession);

				$countWebinarSessions = Yii::app()->db->createCommand()
						->select('DISTINCT (ws.id_session) as session')
						->from(WebinarSession::model()->tableName() . ' ws')
						->join(WebinarSessionUser::model()->tableName() . ' wsu', 'ws.id_session = wsu.id_session')
						->where( array('in', 'wsu.id_user',  $idst ) )
						->andWhere('ws.date_end >= :now', array(
								':now' => Yii::app()->localtime->getUTCNow('Y-m-d H:i:s')
						))
						->queryAll();

				$data['webinarSessionsCount'] = count($countWebinarSessions);
			} else{
				$countIltSession = Yii::app()->db->createCommand()
						->select('DISTINCT (lts.id_session) as session')
						->from(LtCourseSession::model()->tableName() . ' lts')
						->join(LtCourseuserSession::model()->tableName() . ' ltsu', 'lts.id_session = ltsu.id_session')
						->where('ltsu.id_user = :user AND lts.date_end >= :now', array(
								':user' => $idst,
								':now' => Yii::app()->localtime->getUTCNow('Y-m-d H:i:s')
						))
						->queryAll();

				$data['classroomSessionsCount'] = count($countIltSession);


				$countWebinarSessions = Yii::app()->db->createCommand()
						->select('DISTINCT (ws.id_session) as session')
						->from(WebinarSession::model()->tableName() . ' ws')
						->join(WebinarSessionUser::model()->tableName() . ' wsu', 'ws.id_session = wsu.id_session')
						->where('wsu.id_user = :user AND ws.date_end >= :now', array(
								':user' => $idst,
								':now' => Yii::app()->localtime->getUTCNow('Y-m-d H:i:s')
						))
						->queryAll();

				$data['webinarSessionsCount'] = count($countWebinarSessions);
			}
		}

		return $data;
	}

	public function actionVerifyEmail(){
		$user = CoreUser::model()->findByPk(intval(Yii::app()->request->getParam('userId')));
		$isLinkValid = false;
		if ($user) {
			$hash = Yii::app()->request->getParam('hash');
			if ($hash === $user->email_code) {
				$currentTime = strtotime(Yii::app()->localtime->getUTCNow());
				// check date format
				$format = Yii::app()->localtime->getPHPLocalDateTimeFormat();
				$date = new DateTime();
				$requestTime = $date->createFromFormat($format, $user->requested_on);
				$requestTime = $requestTime->getTimestamp();
				$validTime = Settings::get('hour_request_limit') * 60 * 60 + $requestTime;
				if ($currentTime <= $validTime) {
					$user->email_status = 1;
					if ($user->save()) {
						$isLinkValid = true;
					}
				}
			}
		}
		if ($isLinkValid === true) {
			Yii::app()->user->setFlash('email_status_change', Yii::t('standard', 'You have confirmed your email!'));
		} else {
			// invalid or expired link
			Yii::app()->user->setFlash('email_status_change', Yii::t('standard', 'Invalid or expired email verification link!'));
		}
		$this->redirect(Docebo::createLmsUrl('site/index'));
	}
}

