<?php
/**
 * A controller serving all the "Coaching" administrative tasks
 *
 */
class CoachingController extends AdminController {

	/**
	 * This attribute will be set (from the HTTP request, if any) to the ID of the course involved 
	 * @var integer
	 */
	public $idCourse;
	
	public $courseModel;
	
	/**
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}

	/**
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		
		$res = array();
		
		if (!$this->courseModel->enable_coaching) {
			$res[] = array(
				'deny',
				'users' => array('*'),
				'actions' => array('index'),
				'deniedCallback' => array($this, 'adminHomepageRedirect'),
			);
		}
		
		$res[] = array(
			'allow',
			'roles' => array('/lms/admin/course/mod'),
			//'actions' => array('create'),
		);
		
		$res[] = array(
			'deny',
			'users' => array('*'),
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);
		
		return $res;
	}


	/**
	 * @see Controller::init()
	 */
	public function init() {
		parent::init();
		// Get course ID from the REQUEST, if any, and save it to controller-wide attribute, making it available to all actions & views later
		$this->idCourse = Yii::app()->request->getParam("course_id", FALSE);
		$this->courseModel = LearningCourse::model()->findByPk($this->idCourse); 
		
	}

	
	
	/**
	 * Render the main Coaching management grid, the base page
	 */
	public function actionIndex() {
        $search_input 	= Yii::app()->request->getParam('search_input', false);
        $date_start 	= Yii::app()->request->getParam('date-start', false);
        $date_end       = Yii::app()->request->getParam('date-end', false);
        $show_available_only = Yii::app()->request->getParam('show_available_only', false);
        $date_start 	= ($date_start) ? Yii::app()->localtime->toUTC(Yii::app()->localtime->fromLocalDate($date_start).' 00:00:00') : false;//Yii::app()->localtime->fromLocalDate($date_start);
        $date_end 	    = ($date_end) ? Yii::app()->localtime->toUTC(Yii::app()->localtime->fromLocalDate($date_end).' 23:59:59') : false;//Yii::app()->localtime->fromLocalDate($date_end);

		$assetsUrl = $this->getAdminJsAssetsUrl();
		Yii::app()->clientScript->registerScriptFile($assetsUrl . '/coaching.js');
		
		// Register selector resources
		UsersSelector::registerClientScripts();
        //DatePickerHelper::registerAssets();
		
		$params = array(
            'search_input'  => $search_input,
            'date_start'    => $date_start,
            'date_end'      => $date_end,
            'show_available_only' => $show_available_only
        );
		$this->render('index', $params);
		
	}
	

	/**
	 * Render and handle Creaion and Editing of a Coaching session related to the course
	 */
	public function actionAxEditCoachingSession() {
		$idSession     = Yii::app()->request->getParam('idSession', false);
        $confirm 	= Yii::app()->request->getParam("confirm_button", FALSE);
        if ($confirm) {
            $sessionData = Yii::app()->request->getParam('LearningCourseCoachingSession',array());
            $start      =   Yii::app()->localtime->toUTC(Yii::app()->localtime->fromLocalDate($sessionData['start_date']).' 00:00:00');
            $end        =   Yii::app()->localtime->toUTC(Yii::app()->localtime->fromLocalDate($sessionData['end_date']).' 23:59:59');
            $maxUsers   =   ($sessionData['max_users'])?    $sessionData['max_users'] : 0;

			if($idSession)
				$sessionModel = LearningCourseCoachingSession::model()->findByPk($idSession);
			else
				$sessionModel = new LearningCourseCoachingSession();

			try {
				$sessionModel->idCourse = $this->idCourse;
				$sessionModel->idCoach = $sessionData['idCoach'];
				if(!$sessionModel->idCoach)
					throw new CException(Yii::t('coaching', 'Cannot create a session without a selected coach.'));
                if(!$sessionData['start_date'] || !$sessionData['end_date'])
                    throw new CException(Yii::t('coaching', 'Cannot create a session without selected dates.'));
				$sessionModel->max_users = $maxUsers;
				$sessionModel->datetime_start = $start;
				$sessionModel->datetime_end = $end;

                if (Yii::app()->request->getParam('isMerging',false)) {
                    $targetSession = Yii::app()->request->getParam('targetSession',false);
                    if(!$targetSession)
                        throw new CException(Yii::t('coaching', 'Cannot merge a session without a selected target recipient session.'));
                    else {
						try {
							$targetSessionModel = LearningCourseCoachingSession::model()->findByPk($targetSession);
							$sessionModel->mergeInto($targetSessionModel, true);
						} catch (Exception $e) {
							Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
							$this->renderPartial('admin.protected.views.common/_dialog2_error', array(
								'message' => Yii::t('standard', '_OPERATION_FAILURE'),
								'type' => 'error',
							));
							Yii::app()->end();
						}
					}
                } else if(!$sessionModel->save())
					throw new CException(CHtml::errorSummary($sessionModel));
			} catch(CException $e) {
				$this->renderPartial('admin.protected.views.common/_dialog2_error', array(
					'message' 	=> $e->getMessage(),
					'type'		=> 'error',
				));
				Yii::app()->end();
			} catch(Exception $e) {
				Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
				$this->renderPartial('admin.protected.views.common/_dialog2_error', array(
					'message' 	=> Yii::t('standard', '_OPERATION_FAILURE'),
					'type'		=> 'error',
				));
				Yii::app()->end();
			}

            echo '<a class="auto-close"></a>';
            Yii::app()->end();
        }

		$search_input   = Yii::app()->request->getParam('search_input', false);
		$start_date     = Yii::app()->request->getParam('start_date', false);
		$end_date     = Yii::app()->request->getParam('end_date', false);
        $fromDelete     = Yii::app()->request->getParam('fromDelete', false);

		$params = array(
			'idCourse' => $this->idCourse,
			'search_input' => $search_input,
			'start_date' => ($start_date)?Yii::app()->localtime->toUTC(Yii::app()->localtime->fromLocalDate($start_date).' 23:59:59'):false,
			'end_date' => ($end_date)?Yii::app()->localtime->toUTC(Yii::app()->localtime->fromLocalDate($end_date).' 00:00:00'):false,
			'idSession' => $idSession,
            'fromDelete'    => $fromDelete,
		);

		echo $this->renderPartial('_edit_coaching_session', $params, true, true);
        Yii::app()->end();
	}
	
	/**
	 * Provides selector with users to assign to a particular Coaching session. 
	 * Only users enrolled to related course are presented (See users selector controller for TYPE_ASSIGN_USERS_COACHING)
	 * 
	 * Apply assignment upon submition (as a Collector of the selector) 
	 * 
	 */
	public function actionAxAssignUsersToSession() {
		
		$idSession 	= Yii::app()->request->getParam("idSession", FALSE);
		$confirm 	= Yii::app()->request->getParam("confirm", FALSE);

		$transaction = null;
		if (Yii::app()->db->getCurrentTransaction() === NULL) { $transaction = Yii::app()->db->beginTransaction(); }
		
		try {
			$sessionModel 	= LearningCourseCoachingSession::model()->findByPk($idSession);
			if (!$sessionModel || !$sessionModel->course) {
				throw new Exception('Invalid session or course');
			}

			// Confirm button has been clicked ?
			if ($confirm) {
				$selectedUsers = UsersSelector::getUsersListOnly($_REQUEST);
				if (!$sessionModel->max_users || (count($selectedUsers) <= $sessionModel->max_users)) {

					// list with currently selected users from the wizard
					$selectedUsers = UsersSelector::getUsersListOnly($_REQUEST);

					// Get already assigned to the session users
					$getCurrentlyAssignedUsers = Yii::app()->db->createCommand
					('SELECT idUser FROM '.LearningCourseCoachingSessionUser::model()->tableName() . ' WHERE idSession='.$idSession)
					->queryAll();

					// Array to collect user id(-st) of user to be unassigned
					$unassignUsers = array();

					// Array to collect user id(-st) of user to be assigned
					$assignUsers = array();

					// Array with already assigned to session users
					$currentlyAssignUsers = array();

					// Prepare array with currently assigned users to the coaching sesssion !!!
					if (is_array($getCurrentlyAssignedUsers) && count($getCurrentlyAssignedUsers) > 0) {
						foreach ($getCurrentlyAssignedUsers as $itemUserId) {
							$currentlyAssignUsers[] = $itemUserId['idUser'];
						}
					}

					// Get only the newly added users to be assigned !!!
					if (is_array($selectedUsers) && count($selectedUsers) > 0) {
						foreach ($selectedUsers as $itemUserId) {
							if (!in_array($itemUserId, $currentlyAssignUsers)) {
								$assignUsers[] = $itemUserId;
							}
						}
					}

					// Fill the above array with ids of users, that should be unassigned
					foreach ($currentlyAssignUsers as $itemUserId) {
						if (!in_array($itemUserId, $selectedUsers)) {
							// This means, that user is deselected from the users list and should be unassigned from this coaching session
							$unassignUsers[] = $itemUserId;
						}
					}

					// If there are users to be unassigned clean their messages
					if (count($unassignUsers) > 0) {
							foreach ($unassignUsers as $itemUserId) {
								// Remove all messages for this session and user- cleanup garbage
								LearningCourseCoachingMessages::model()->deleteAllByAttributes(array('idLearner' => $itemUserId, 'idSession' => $idSession));
							}

						// Deassign users only if there are users to be deassigned !!!
						$sessionModel->deassignUsers($unassignUsers);
					}

					// Do actual assign of the users
                    $sessionModel->assignUsers($assignUsers);

					if ($transaction) $transaction->commit();
					echo '<a class="auto-close"></a>';
					Yii::app()->end();
				}
				else {
					Yii::app()->user->setFlash('error', Yii::t('coaching', 'Selected number of users exceeds maximum allowed for this session ({c})', array('{c}' => $sessionModel->max_users)));
				}
			}

			// Where selector will submit its data to be collected (THIS action again)			
			$collectorUrl = Docebo::createAdminUrl('coaching/axAssignUsersToSession', array(
				'idSession'	=> $idSession,
			));
		
		
			$selectorDialogUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
				'idGeneral'		=> $idSession,  // The session ID, passed along the AJAX request
				'type'			=> UsersSelector::TYPE_ASSIGN_USERS_COACHING,
				'tp' 			=> 'admin.protected.views.coaching._assign_users_to_session_top_panel',  // top panel partial! view
				'collectorUrl' 	=> $collectorUrl,  // Make this action also collector of the users selector submition
			));
		
		
			// Redirect to UsersSelector
			$this->redirect($selectorDialogUrl, true);
			Yii::app()->end();
					
		}
		catch (Exception $e) {
			if ($transaction) $transaction->rollback();
			$this->renderPartial('admin.protected.views.common/_dialog2_error', array(
				'message' 	=> $e->getMessage(),
				'type'		=> 'error',
			));
			Yii::app()->end();
		}
		
		
	}

    /**
     * The backend counterpart of the "coach search autocomplete" (JuiAutocomplete)
     */
    public function actionCoachesAutocomplete() {

        $maxNumber 		= Yii::app()->request->getParam('max_number', 10);
        $term 			= Yii::app()->request->getParam('term', false);
        $idCourse       = Yii::app()->request->getParam('course_id', false);
        $idSession      = Yii::app()->request->getParam('idSession', false);
        $start_date     = Yii::app()->request->getParam('start_date', false);//NOT WORKING
        $start_date     = Yii::app()->localtime->fromLocalDateTime($start_date);
        $end_date       = Yii::app()->request->getParam('end_date', false);//NOT WORKING
        $end_date       = Yii::app()->localtime->fromLocalDateTime($end_date);
        $model          = new CoreUser();

        $search_input = $term ? $term : null;
        $dataProvider = $model->sqlCoachesForCourses($idCourse,$search_input,$start_date,$end_date,$idSession);
        $dataProvider->setPagination(array(
            'pageSize' => $maxNumber,
        ));
        $data = $dataProvider->getData();
        $result = array();
        if ($data) {
            foreach ($data as $item) {
                $result[] = array(
                    'label' => ltrim($item["userid"],"/"),
                    'value' => ltrim($item["userid"],"/"),
                );
            }
        }
        echo CJSON::encode($result);
    }

    /**
     * The backend counterpart of the "coach search autocomplete" (JuiAutocomplete)
     */
    public function actionSessionsAutocomplete() {

        $maxNumber 		= Yii::app()->request->getParam('max_number', false);
        $term 			= Yii::app()->request->getParam('term', false);

        $filter         = new StdClass;
        $filter->search_input = $term ? $term : null;
        $dataProvider   = LearningCourse::coachingSessionsSqlDataProvider($this->idCourse,$filter);
        $dataProvider->setPagination(array(
            'pageSize' => $maxNumber,
        ));
        $data = $dataProvider->getData();
        $result = array();
        if ($data) {
            foreach ($data as $item) {

                $result[] = array(
                    'label' => ltrim($item["userid"],"/").' - '.'('.Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($item["datetime_start"])).' - '.Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($item["datetime_end"])).')',
                    'value' => $item["idSession"],
                );
            }
        }
        echo CJSON::encode($result);
    }

	/**
	 * Deletes the coaching session (or merges it)
	 */
    public function actionAxDeleteCoachingSession() {
    	$idSession  = Yii::app()->request->getParam('idSession', false);
        $idCourse   =  Yii::app()->request->getParam('idCourse', false);
        $confirm = Yii::app()->request->getParam('confirm_button', false);
        $agreed = Yii::app()->request->getParam('agree_deletion', false);
        $fromDelete = Yii::app()->request->getParam('fromDelete',false);
		$deletionMode = Yii::app()->request->getParam('deletionMode', 'merge');
		$transaction = null;

		try {
			// Load session id
			$sessionModel = LearningCourseCoachingSession::model()->findByPk($idSession);
			if (!$sessionModel)
				throw new Exception('Invalid session');

			if($deletionMode == 'delete' && $confirm && $agreed) {
				if (Yii::app()->db->getCurrentTransaction() === NULL)
					$transaction = Yii::app()->db->beginTransaction();

                if(!$sessionModel->delete())
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));

				if ($transaction)
					$transaction->commit();

				echo '<a class="auto-close"></a>';
				Yii::app()->end();

            } else if($deletionMode == 'merge' && ($confirm || $fromDelete)) {
                $params = array(
                    'idSession'        => $idSession,
                    'course_id'         => $idCourse,
                    'fromDelete'        => true
                );
                Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;
                Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
                $targetSession = Yii::app()->request->getParam('targetSession',false);
                $isMerging = Yii::app()->request->getParam('isMerging',false);

                if(!$targetSession && $isMerging) {
                    throw new CException(Yii::t('coaching', 'Cannot merge a session without a selected target recipient session.'));
                }
                elseif(!$targetSession && !$isMerging){
                    echo $this->renderPartial('_edit_coaching_session', $params, true, true);
                    Yii::app()->end();
                }
                elseif($targetSession && $isMerging){
                    try {
                        $sessionModel = LearningCourseCoachingSession::model()->findByPk($idSession);
                        $targetSessionModel = LearningCourseCoachingSession::model()->findByPk($targetSession);
                        $sessionModel->mergeInto($targetSessionModel, true);
                        echo '<a class="auto-close"></a>';
                        Yii::app()->end();
                    }
                    catch(Exception $e) {
                    	Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
                        $this->renderPartial('admin.protected.views.common/_dialog2_error', array(
                            'message' 	=> Yii::t('standard', '_OPERATION_FAILURE'),
                            'type'		=> 'error',
                        ));
                        Yii::app()->end();
                    }
                }
            } else {
				$assignedCount = $sessionModel->countAssignedUsers();
                $this->renderPartial('_delete_session', array(
					'session' => $sessionModel,
					'coachName' => $sessionModel->coach->getClearUserId(),
					'startDate' => Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($sessionModel->datetime_start)),
					'endDate' => Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($sessionModel->datetime_end)),
					'assignedCount' => $assignedCount,
					'deletionMode' => ($assignedCount > 0) ? $deletionMode : 'delete'
				));
            }
		}
		catch (Exception $e) {
			if ($transaction)
				$transaction->rollback();

			$this->renderPartial('admin.protected.views.common/_dialog2_error', array(
				'message' 	=> $e->getMessage(),
				'type'		=> 'error',
			));
			Yii::app()->end();
		}
    } 

    /**HTML for the last column of the main gridView of coaching/index
     * 
     * @param unknown $data
     * @param unknown $index
     */
    protected function renderOperationsColumn($data, $index) {
    	
    	$html = CHtml::link('<span class="i-sprite is-edit"></span>',
    		Docebo::createAdminUrl('coaching/axEditCoachingSession', array(
    				'course_id'		=> $this->idCourse,
    				'idSession'		=> $data['idSession'],
    		)),
    		array(
    			'class' => "open-dialog",
				'data-dialog-id'    => "coaching-edit-session-dialog",
				'data-dialog-class' => "coaching-edit-session-dialog",
				'data-dialog-title'	=> Yii::t('coaching', 'Edit coaching session'),
    		)	
    	);
    	 $html .= "&nbsp;&nbsp;". CHtml::link('<span class="i-sprite is-remove red"></span>',
             Docebo::createAdminUrl('coaching/axDeleteCoachingSession', array(
                 'idSession'		=> $data['idSession'],
                 'idCourse'         => $this->idCourse,
             )),
             array(
                 'class' => "open-dialog",
                 'data-dialog-id'       => "delete-certification-dialog",
                 'data-dialog-class'    => "delete-item-modal coaching-delete-modal",
                 'closeOnEscape'        => "true",
                 'closeOnOverlayClick'  => "true",
                 'data-dialog-title'	=> Yii::t('coaching', 'Delete coaching session'),
             )
         );
    	echo $html;
    			     	
    }

    protected function renderAssignColumn($data, $index){
        $model = LearningCourseCoachingSession::model()->findByPk($data["idSession"]);
        $takenSeats = ($model) ? $model->countAssignedUsers() : 0; 
        $html = CHtml::link(CHtml::encode($takenSeats) . (($data['max_users'] > 0)  ? (" / ".CHtml::encode($data["max_users"])) : '').' <span class="enrolled"></span>',
            Docebo::createAdminUrl('coaching/axAssignUsersToSession', array(
                'idSession'	=> $data['idSession'],
            )),
            array(
                'class'             =>"open-dialog",
                'data-dialog-id'    =>"users-selector",
                'data-dialog-title' =>Yii::t('coaching', 'Assign users to a coaching session')
            ));
        echo $html;
    }

	protected function renderTimeframe($data, $index) {
		return Yii::t("course", '_SUBSCRIPTION_DATE_BEGIN') . " " . Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($data["datetime_start"]))
		. " " . Yii::t("course", "_SUBSCRIPTION_DATE_END") . "  " . Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($data["datetime_end"]));
	}
}