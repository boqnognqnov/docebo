<?php
class QuestCategoryController extends AdminController
{

	public $layout='//layouts/adm';


	public function filters() {
		return array(
				'accessControl' => 'accessControl',
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		$res = array();

		$res[] = array(
				'allow',
				'expression' => '(Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess("/lms/admin/course/mod"))',
		);

		$res[] = array(
			'deny',
			// deny all
			'users' => array('*'),
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);

		return $res;
	}


	/**
	 * (non-PHPdoc)
	 * @see Controller::init()
	 */
	public function init() {

		$cs = Yii::app()->getClientScript();

		DatePickerHelper::registerAssets();

		// Register JS files
		$cs->registerScriptFile(Yii::app()->baseUrl . '/js/questcat.js', CClientScript::POS_HEAD);

		// Register JS script(s)
		$options = array();
		$options = CJavaScript::encode($options);
		$script = "var QuestCat = new QuestionCategory($options);";
		$cs->registerScript("questcatJS1", $script , CClientScript::POS_HEAD);

		return parent::init();
	}




	/**
	 * Create / Edit  Category
	 *
	 * @throws CException
	 */
	public function actionAxUpdateCategory() {

		$category_id = Yii::app()->request->getParam("category_id", false);
		$model = LearningQuestCategory::model()->findByPk($category_id);

		$db_transaction = Yii::app()->db->beginTransaction();

		try {

			// If we have incoming category and invalid model.. that's an error!
			if ($category_id && !$model) {
				throw new CException(Yii::t('standard','_OPERATION_FAILURE'));
			}

			// If no model, then we are going to CREATE new category
			if (!$model) {
				$model = new LearningQuestCategory();
			}

			if (!empty($_POST['LearningQuestCategory'])) {
				$model->attributes = $_POST['LearningQuestCategory'];
				if ($model->validate()) {
					$model->save(false);
					$db_transaction->commit();
					echo '<a class="auto-close success"></a>';
					Yii::app()->end();
				}
			}

			$html = $this->renderPartial('_form', array(
					'model' => $model,
			), true);
			echo $html;
			Yii::app()->end();

		}
		catch (CException $e) {
			$db_transaction->rollback();
			$html = $this->renderPartial('//common/_dialog2_error', array(
					'message' => $e->getMessage(),
			), true);
			echo $html;
			Yii::app()->end();
		}

	}



	/**
	 * Update existing question category
	 *
	 * @throws CException
	 */
	public function actionAxDeleteCategory() {

		$category_id = Yii::app()->request->getParam("category_id", false);
		$confirm_delete = Yii::app()->request->getParam("confirm_delete", false);
		$model = LearningQuestCategory::model()->findByPk($category_id);

		$db_transaction = Yii::app()->db->beginTransaction();

		try {
			if (empty($model)) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			if ($confirm_delete) {

				if ($model->isUsed()) {
					throw new CException(Yii::t('questcategory', '_CATEGORY_IN_USE'));
				}

				if (!$model->delete()) {
					throw new CException(Yii::t('standard','_OPERATION_FAILURE'));
				}

				$db_transaction->commit();
				echo '<a class="auto-close success"></a>';
				Yii::app()->end();
			}


			$html = $this->renderPartial('_delete', array(
					'model' => $model,
			), true);
			echo $html;
			Yii::app()->end();

		}
		catch (CException $e) {
			$db_transaction->rollback();
			$html = $this->renderPartial('//common/_dialog2_error', array(
					'message' => $e->getMessage(),
					'type' => 'alert-warning'
			), true);
			echo $html;
			Yii::app()->end();
		}

	}





	/**
	 * Buttons column (operations)
	 *
	 * @param unknown $row
	 * @param unknown $index
	 */
	protected function _renderGridOperationsColumn($row, $index) {
		$html = $this->renderPartial('_operations_column', array(
				'model' => $row,
		), true);

		return $html;

	}



	/**
	 *
	 */
	public function actionIndex() {

 		$model = new LearningQuestCategory('search');

 		// Search might coming?
 		if ($data = Yii::app()->request->getParam('LearningQuestCategory', false)) {
 			$model->attributes = $data;
 		}

		$this->render('index', array(
			'model' => $model,
		));

	}

	/**
	 * Quest Category autocomplete suggestions
	 *
	 * @param data - search query
	 *
	 * return JSON (idCategory -> name)
	 */
	public function actionAutoComplete(){

		$requestData = Yii::app()->request->getParam('data', false);

		$query = '';
		if(!empty($requestData['query'])) {
			$query = addcslashes($requestData['query'], '%_');
		}

		$criteria = new CDbCriteria();
		$criteria->addCondition('t.name LIKE :filter OR t.textof LIKE :filter');
		$criteria->params[':filter'] = '%' . $query . "%";

		$result = LearningQuestCategory::model()->findAll($criteria);
		$usersList = CHtml::listData($result, 'idCategory', 'name');
		$this->sendJSON(array('options' => $usersList));
	}


}
