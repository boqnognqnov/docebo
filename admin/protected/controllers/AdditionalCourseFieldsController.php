<?php

class AdditionalCourseFieldsController extends AdminController {

    public $userCanAdminCourse = false;
    public $course_id = null;
    public $userIsSubscribed = false;

    public function init() {

        parent::init();

        JsTrans::addCategories("user_management");

        $this->course_id = Yii::app()->request->getParam("course_id", FALSE);

        $model = LearningCourseuser::model()->findByAttributes(array("idUser" => Yii::app()->user->id, "idCourse" => $this->course_id));
		if ($model) {
			$levelInCourse = $model->level;
			$this->userIsSubscribed = true;

			// Check if this user is not an admin and can enter course
			if (Yii::app()->user->getIsUser()) {
				$canEnterCourse = $model->canEnterCourse();
				if (!$canEnterCourse['can']) {
					Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
					$this->redirect(array('/site'), TRUE);
				}
			}
		} else {
			$levelInCourse = 0;
			$this->userIsSubscribed = false;
			// Only subscribed users can play this course (with the exception of admins)
			if (Yii::app()->user->getIsUser()) {
				Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
				$this->redirect(array('/site'), TRUE);
			}
		}

		// Is this a power user and the course was assigned to him?
		$pUserCourseAssigned = CoreUserPuCourse::model()->findByAttributes(array(
			'puser_id' => Yii::app()->user->id,
			'course_id' => $this->course_id
		));

        $this->userCanAdminCourse = ($levelInCourse && $levelInCourse > LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT
									&& $levelInCourse != LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR && $levelInCourse != LearningCourseuser::$USER_SUBSCR_LEVEL_COACH)
									|| Yii::app()->user->isGodAdmin
									|| ($pUserCourseAssigned && Yii::app()->user->checkAccess('/lms/admin/course/mod'));
    }

    public function filters() {
        return array(
            'accessControl' => 'accessControl',
        );
    }

    /***
     * @return array
     */
    public function accessRules() {
        $res = array();

        $res[] = array(
            'allow',
            'actions' => array('index', 'fieldsAutocomplete', 'createFieldModal', 'axCourseDescWidgetSettings', 'editField', 'fcbkAutocomplete'),
            'expression' => 'Yii::app()->controller->userCanAdminCourse',

        );

        $res[] = array(
            'allow',
            'roles' => array('/framework/admin/field_manager/add'),
            'actions' => array('index', 'createFieldModal', 'create', 'editField', 'createDropdown', 'editDropdown', 'orderCourseFields', 'saveDropdownSingleLanguage'),
        );

        $res[] = array(
            'allow',
            'roles' => array('/framework/admin/field_manager/del'),
            'actions' => array('deleteField'),
        );

        $res[] = array(
            'allow',
            'roles' => array('/framework/admin/field_manager/mod'),
            'actions' => array('index', 'createFieldModal', 'editField'),
        );

        $res[] = array(
            'deny',
            // deny all
            'users' => array('*'),
            'deniedCallback' => array($this, 'adminHomepageRedirect'),
        );

        return $res;
    }


    /**
     *
     */
    public function actionIndex() {

        $cs = Yii::app()->clientScript;

        // Accreditation IFRAME custom field JS helper (TR)
        $themeUrl = Yii::app()->theme->baseUrl;
        $cs->registerScriptFile($themeUrl . "/js/accreditation_iframe.js", CClientScript::POS_BEGIN);
        
        DatePickerHelper::registerAssets();
        $cs->registerCoreScript('jquery.ui');
        $cs->registerScriptFile(Yii::app()->baseUrl.'/js/dropdown_course_custom_field.js', CClientScript::POS_END);

        $coreField = LearningCourseField::model();
        $fieldTypes = LearningCourseField::getCourseFieldsTypes(); // for courses

        $event = new DEvent();
        Yii::app()->event->raise('BeforeBuildingCourseFieldTypes', $event);

        if(!empty($event->return_value)){
            $fieldTypes = array_merge($fieldTypes, $event->return_value);
        }

        $this->render('index', array(
            'coreField' => $coreField,
            'fieldTypes' => $fieldTypes,
        ));

    }

    protected function gridRenderTranslation($data, $index)
    {
        return (empty($data->translation) && !empty($data->enTranslation)) ? $data->enTranslation[0]->translation : $data->translation;
    }

    public function actionFieldsAutocomplete() {

        $query = '%'.strtolower(addcslashes($_REQUEST['data']['query'], '%_')).'%';

		$lang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
		$def_lang = Settings::get('default_language', 'english');

		$command = Yii::app()->db->createCommand();
		$command->select = 't.translation, def.translation AS def_translation';
		$command->from = LearningCourseFieldTranslation::model()->tableName().' AS t';
		$command->join = "JOIN ".LearningCourseFieldTranslation::model()->tableName()." AS def ON def.lang_code = :def_lang AND def.id_field = t.id_field";
		$command->where = "t.lang_code = :lang_code AND (t.translation LIKE :query OR (t.translation = '' AND def.translation LIKE :query))";
		$command->order = "CONCAT(t.translation, def.translation) ASC";
		$command->params = array(
			':lang_code' => $lang,
			':def_lang' => $def_lang,
			':query' => $query
		);

		$translations = $command->queryAll();

        $response = array();
        foreach ( $translations as $translation_check)
		{
            if($translation_check['translation'] != '')
				$response[] = $translation_check['translation'];
			else
				$response[] = $translation_check['def_translation'];
        }

        $this->sendJSON(array('options' => $response));
    }

    public function actionFcbkAutocomplete(){
    	$request = Yii::app()->request;

    	if($request->isAjaxRequest){
    		$tag = $request->getParam('tag', null);

    		if(!$tag){
    			return;
    		}

    		$lang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

    		$command = Yii::app()->db->createCommand();
    		$command->select('translation, lcf.id_field');
    		$command->join('learning_course_field_translation lcft', "lcft.id_field = lcf.id_field AND lcft.lang_code = :lang AND translation LIKE :tag AND invisible_to_user = 1");
    		$command->from(LearningCourseField::model()->tableName() . ' lcf');
    		$command->params[':lang'] = $lang;
    		$command->params[':tag'] = '%' . $tag . '%';
    		$fields = $command->queryAll();

    		$result = array();
    		if($fields){
    			foreach ($fields as $field){
    				$result[] = array(
    						'key' => $field['id_field'],
    						'value' => $field['translation']
    				);
    			}
    		}

    		echo CJSON::encode($result);
    	}
    }

    public function actionDeleteField( $id ) {

        $confirmYes = Yii::app()->request->getParam('confirm_yes', null);
        $courseField = LearningCourseField::model()->findByPk( $id );
        if ( empty( $courseField)) {
            $this->sendJSON( array());
        }
        $courseField->getTranslation();
        if ( $confirmYes ) {
            $courseField->delete();

            echo '<a class="auto-close"></a>';

        }
        if ( Yii::app()->request->isAjaxRequest && $confirmYes != 'yes') {
            $this->renderPartial('deleteModal', array(
                'model' => $courseField,
            ),false);
            Yii::app()->end();
        }
        Yii::app()->end();
    }

    public function actionCreateFieldModal($type = 'default') {
        $fieldTranslation   = new LearningCourseFieldTranslation();
        $fieldTranslation->lang_code = Settings::get('default_language', 'english');
        $field = new LearningCourseField();

        if (!empty($_REQUEST['type']))
            $type = $_REQUEST['type'];

        if ( !empty($_POST['LearningCourseField']) && !empty($_POST['LearningCourseFieldTranslation']['translation'][$fieldTranslation->lang_code]) ) {
            $enteredLanguages = $_POST['LearningCourseFieldTranslation']['translation'];

            $baseField = new LearningCourseField();
            $baseField->setFieldTranslations(array_filter($enteredLanguages));
            $baseField->type                = $_POST['LearningCourseField']['type'];
            $baseField->invisible_to_user   = $_POST['LearningCourseField']['invisible_to_user'];
            $baseField->sequence            = LearningCourseField::model()->getNextSequence();
            $baseField->settings            = CJSON::encode(is_array($_POST['LearningCourseField']['settings']) ? $_POST['LearningCourseField']['settings'] : array());

            $success = false;
            if ( $baseField->validate() ) {

                $transaction = Yii::app()->db->beginTransaction();

                if (empty($_POST['LearningCourseFieldTranslation']['translation'])) {
                    $success = true;
                } else {

                    $filteredTranslations = array_filter($_POST['LearningCourseFieldTranslation']['translation']);
                    $newField = new LearningCourseField();
                    $newField->setFieldTranslations($filteredTranslations);
                    $newField->type = $baseField->type;
                    $newField->invisible_to_user = $_POST['LearningCourseField']['invisible_to_user'];
                    $newField->sequence = $baseField->sequence;
                    $newField->settings = $baseField->settings;
                    $success = $newField->save();

                    if ($success) {
                        $transaction->commit();
                    } else {
                        $transaction->rollback();
                    }
                }
            }
            $this->sendJSON(array(
               'success' => $success,
            ));
        }

        $field->type = $type;
        $activeLanguagesList    = CoreLangLanguage::getActiveLanguages();
		$lang_code = Settings::get('default_language', 'english');
		$activeLanguagesList[$lang_code] = $activeLanguagesList[$lang_code] . " (" . Yii::t('standard', '_DEFAULT_LANGUAGE') . ")";
        $modelDropdown = new LearningCourseFieldDropdown();
        $modelDropdownTranslation = new LearningCourseFieldDropdownTranslations();

        $html = $this->renderPartial('createModal',
            array(
                'modelField'                => $field,
                'modelTranslation'          => $fieldTranslation,
                'modelDropdown'             => $modelDropdown,
                'modelDropdownTranslation'  => $modelDropdownTranslation,
                'activeLanguagesList'       => $activeLanguagesList
            ), true);

        $this->sendJSON(array(
            'html' => $html,
        ));
    }

    public function actionEditField( $id )
    {
        $lang_code = Settings::get('default_language', 'english');
        $activeLanguagesList = CoreLangLanguage::getActiveLanguages();
        $activeLanguagesList[$lang_code] = $activeLanguagesList[$lang_code] . " (" . Yii::t('standard', '_DEFAULT_LANGUAGE') . ")";
        $modelTranslation   = new LearningCourseFieldTranslation();
        $modelTranslation->lang_code = Settings::get('default_language', 'english');

        if ( $id ) {
            $fieldTranslations = LearningCourseFieldTranslation::getFieldTranslations( $id );
            $baseField = LearningCourseField::model()->findByPk( $id );
            if ( isset($_REQUEST['LearningCourseField']['invisible_to_user']) )
            {
                $baseField->invisible_to_user = $_REQUEST['LearningCourseField']['invisible_to_user'];
                $baseField->settings            = CJSON::encode(is_array($_POST['LearningCourseField']['settings']) ? $_POST['LearningCourseField']['settings'] : array());
                $baseField->save();
            }
        }

        if ( !empty($_POST['LearningCourseFieldTranslation']) && !empty($_POST['LearningCourseField'])) {
            $platformDefaultLang = Settings::get('default_language', 'english');
            if (empty($_POST['LearningCourseFieldTranslation']['translation'][$platformDefaultLang])) {

                $fieldTranslations->addError('translation', Yii::t('user_management', '_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED'));

            } else {
                $success = false;

                $transaction = Yii::app()->db->beginTransaction();

                foreach ( $activeLanguagesList as $k => $l) {
                    $translationModel = LearningCourseFieldTranslation::model()->findByAttributes(array(
                        'id_field'  => $id,
                        'lang_code' => $k
                    ));

                    if(is_null($translationModel))
					{
                        $translationModel = new LearningCourseFieldTranslation;
                        $translationModel->lang_code = $k;
						$translationModel->id_field = $id;
                    }

                    $translationModel->translation = $_POST['LearningCourseFieldTranslation']['translation'][$k];
                    $success = $translationModel->save();
                    if (!$success) {
                        Yii::log(print_r($translationModel->getErrors(), true), CLogger::LEVEL_ERROR);
                        $result['success'] = false;
                        break;
                    }
                }

                if ($success) {
                    $transaction->commit();
                } else {
                    $transaction->rollback();
                }

                $this->sendJSON(array(
                    'success' => $success,
                ));
            }
        }

        $html = $this->renderPartial('editModal',
            array(
                'modelField'            => $baseField,
                'activeLanguagesList'   => $activeLanguagesList,
                'translationsList'      => $fieldTranslations,
                'modelTranslation'      => $modelTranslation
            ), true );

        $this->sendJSON(array(
            'html' => $html,
        ));
    }

    public function actionCreateDropdown() {
        $transaction = Yii::app()->db->beginTransaction();
        $success = false;
    	$optionsJsonData = Yii::app()->request->getParam('dropdownElemsJson', false);
        $filteredTranslations = array();

        if ( !empty($_POST['LearningCourseFieldTranslation']) && !empty($_POST['LearningCourseFieldTranslation']['translation'][CoreUser::getDefaultLangCode()])) {
            $translationList = $_POST['LearningCourseFieldTranslation']['translation'];
            $fieldTranslations = array();

            foreach ($translationList as $langCode => $translation) {
                if ( $translation['field'])
                $fieldTranslations[$langCode] = $translation['field'];
                }
            $filteredTranslations = array_filter($fieldTranslations);
        }

        $invisible = 0;
        if ( !empty($_POST['LearningCourseField']) )
            $invisible = $_POST['LearningCourseField']['invisible_to_user'];

            if (!$optionsJsonData) {
                $success = false;
            }

        $optionsData = CJSON::decode($optionsJsonData);

    	$optionsTranslations = $optionsData['CoreField']['translation'];

    	$baseField = new LearningCourseField();
    	$baseField->type = LearningCourseField::TYPE_FIELD_DROPDOWN;
        $baseField->invisible_to_user = $invisible;
        $baseField->settings            = CJSON::encode(is_array($_POST['LearningCourseField']['settings']) ? $_POST['LearningCourseField']['settings'] : array());
        $baseField->sequence = LearningCourseField::model()->getNextSequence();

        if ( $filteredTranslations ) $baseField->setFieldTranslations($filteredTranslations);

    	if ($baseField->save()) {

    		$idBaseField = $baseField->id_field;

    		// Get the FIRST possible language translation
    		$tmp = array_values($optionsTranslations);
            $transCount = array();
            $cTrans = array();
            foreach ( $tmp as $index => $trans) {
                $cTrans = array_filter($trans['options']['translation']);
                $transCount[] = count($cTrans);
            }
            $count = max($transCount);
    		$tmpArray =  array_shift($tmp);
            $option_sequence = 0;
    		$map = array();
    		foreach ($tmpArray['options']['translation'] as $index => $translation) {
    			/*=- $count - 2 is
    			{ 1 for [0]index of array,
    			 2 because the last element of $tmpArray is empty translation } -=*/
    			if ( $index > ($count-1) ) break;

    			$tmpOptionModel = new LearningCourseFieldDropdown();
    			$tmpOptionModel->id_field = $idBaseField;
                $tmpOptionModel->sequence = $option_sequence;
                $option_sequence++;
    			if ($tmpOptionModel->save()) {
    				$map[$index] = $tmpOptionModel->id_option;
    			} else {
                    $success = false;
                    break;
    			}
    		}

    		if (!empty($map)) {
                $translationArray = array();
                $countOptions = array();
                foreach ( $optionsTranslations as $k => $v ) {
                    $translationArray = array_filter($v['options']['translation']);
                    $countOptions[] = count($translationArray);
                }
                $maxOptions = max($countOptions);

    			foreach ($optionsTranslations as $langCode => $translationInfo) {
                    for ($i = 0; $i <= $maxOptions - 1; $i++){
                        if(!isset($translationInfo['options']['translation'][$i])) $translationInfo['options']['translation'][$i] = "";
                    }
                    array_pop($translationInfo['options']['translation']);

                    if ( count($translationInfo['options']['translation']) > $maxOptions ) {
                        foreach ($translationInfo['options']['translation'] as $k => $v) {
                            if ( $k > $maxOptions || $k = $maxOptions) {
                                unset($translationInfo['options']['translation'][$k]);
                            }
                        }
                    }
    					foreach ($translationInfo['options']['translation'] as $index => $translation) {

                        if ($map[$index]) {
    						$translationModel = new LearningCourseFieldDropdownTranslations();
    						$translationModel->id_option = (int) $map[$index];
    						$translationModel->lang_code = $langCode;
    						$translationModel->translation = $translation;

    						if (!$translationModel->save()) {
                                $success = false;
                                break;
    						}
    					} else {
                            $success = false;
                            break;
                        }
    				}
    			}
    		}
    	} else {
            $success = false;
    	}
        if ($success) {
            $transaction->commit();
        } else {
            $transaction->rollback();
        }

        $this->sendJSON(array(
            'success' => $success,
        ));

    }


    public function actionEditDropdown($id) {

        $success = false;
        $optionsJsonData        = Yii::app()->request->getParam('dropdownElemsJson', false);
        $lang_code              = Settings::get('default_language', 'english');
        $activeLanguagesList    = CoreLangLanguage::getActiveLanguages();
        $activeLanguagesList[$lang_code] = $activeLanguagesList[$lang_code] . " (" . Yii::t('standard', '_DEFAULT_LANGUAGE') . ")";
        $modelTranslation       = new LearningCourseFieldTranslation();
        $modelTranslation->lang_code = Settings::get('default_language', 'english');
        $fieldTranslations      = LearningCourseFieldTranslation::getFieldTranslations( $id );
        if ($id)
            $baseField = LearningCourseField::model()->findByPk($id);
        if ( isset($_POST['LearningCourseField']) ) {
            $invisible = $_POST['LearningCourseField']['invisible_to_user'];
            $baseField->invisible_to_user = $invisible;
            $baseField->settings            = CJSON::encode(is_array($_POST['LearningCourseField']['settings']) ? $_POST['LearningCourseField']['settings'] : array());
            $baseField->save();
        }

        if (!$optionsJsonData) {
            $success = false;
        } else {
            $optionsData = CJSON::decode($optionsJsonData);
            $optionsTranslations = $optionsData['CoreField']['translation'];
            $transaction = Yii::app()->db->beginTransaction();
            try {
                foreach  ( $activeLanguagesList as $lang_code => $lang ) {
                    $translationArray = $optionsTranslations[$lang_code]['options']['translation'];
                    foreach ($translationArray as $key => $translation) {

                        $idOption = $optionsTranslations[$lang_code]['options']['idSon'][$key];

                        $object = LearningCourseFieldDropdownTranslations::model()->findByAttributes(
                            array(
                                'id_option' => $idOption,
                                'lang_code' => $lang_code,
                            ));

                        if ( $object instanceof LearningCourseFieldDropdownTranslations ) {
                            if($translation == "") {
                                //unset the coursefields
                                Yii::app()->db->createCommand()->update(LearningCourseFieldValue::model()->tableName(), array("field_$id" => null), "field_$id = :id_option", array(':id_option' => $idOption));
                                //decrement the sequence
                                Yii::app()->db->createCommand()->update(
                                    LearningCourseFieldDropdown::model()->tableName(),
                                    array('sequence' => new CDbExpression('sequence - 1')),
                                    'sequence > 0 && id_option > :id_option',
                                    array(':id_option' => $idOption)
                                );
                                LearningCourseFieldDropdown::model()->deleteAllByAttributes(array('id_option' => $idOption));
                                $object->delete();
                            } else {
                                $object->translation = $translation;
                                $object->save();
                            }
                        } else {
                            //dont save the field if it's empty
                            if($translation != ""){
                                $newOption = new LearningCourseFieldDropdown();
                                $newOption->id_field = $id;
                                $newOption->save();

                                $object = new LearningCourseFieldDropdownTranslations();
                                $object->translation = $translation;
                                $object->lang_code = $lang_code;
                                $object->id_option = $newOption->id_option;
                                $object->save();
                            }
                        }
                        if($_POST['LearningCourseFieldTranslation']['translation'][$lang_code]["field"]!="") {
                            $object = LearningCourseFieldTranslation::model()->findByAttributes(
                            array(
                                'id_field' => $id,
                                'lang_code' => $lang_code
                            ));
                            $translated = $_POST['LearningCourseFieldTranslation']['translation'][$lang_code]["field"];
                            $object->translation = $translated;
                            $object->save();
                        }
                    }
                }
                $transaction->commit();
            }
            catch(Exception $e)
            {
                $transaction->rollback();
                Yii::log($e->getErrors(), CLogger::LEVEL_ERROR);
            }
        }

        $html = $this->renderPartial('editModal', array(
            'modelField'            => $baseField,
            'activeLanguagesList'   => $activeLanguagesList,
            'translationsList'      => $fieldTranslations,
            'modelTranslation'      => $modelTranslation
        ), true);

        $this->sendJSON(array(
            'html' => $html,
        ));
    }

    public function actionOrderCourseFields() {
        $ids = $_POST['ids'];
        $command = Yii::app()->db->createCommand();

        if (!empty($ids)) {
            foreach ($ids as $weight => $id)
            {
                $tmp = LearningCourseField::model()->findByPk($id);
                $id_field = $tmp->id_field;

                $command->update(LearningCourseField::model()->tableName(), array('sequence' => $weight), 'id_field = :id', array(':id' => $id_field));
            }

            $this->sendJSON(array('success' => true));
        } else {
            $this->sendJSON(array('success' => false));
        }
    }

    //this is just a dummy function for the moment
    public function actionSaveDropdownSingleLanguage() {
        $this->sendJSON(array('success' => true));
    }

    public function actionAxCourseDescWidgetSettings(){
        $block_id = Yii::app()->request->getParam('block_id', false);
        $course_id = Yii::app()->request->getParam('course_id', false);
        $block = PlayerBaseblock::model()->findByPk($block_id);

        $courseDescParams = CJSON::decode($block->params);

        $showDesc = $courseDescParams['show_desc'];
        $courseAdditionaFileds = $courseDescParams['additional_fields'];
        $error = null;

        if($_POST['show_desc'] || $_POST['additional_fields']){
            $showDesc = $_POST['show_desc'];
            $courseAdditionaFileds = $_POST['additional_fields'];

            $courseDescParams['show_desc'] = $showDesc;
            $courseDescParams['additional_fields'] = $courseAdditionaFileds;

            $block->params = CJSON::encode($courseDescParams);
            $block->save(false);

            echo "<a class='auto-close success'></a>";
            Yii::app()->end();
        }

        if(empty($_POST['show_desc']) && empty($_POST['additional_fields']) && $_POST['next_button']){
            $error = Yii::t('course', 'Please, select at least one additional field, if you want to hide the description!');
        }

        $this->renderPartial('_course_desc_widget_settings', array(
            'showDesc' => $showDesc,
            'additionalFields' => $courseAdditionaFileds,
            'course_id' => $course_id,
            'error' => $error
        ));
    }

}