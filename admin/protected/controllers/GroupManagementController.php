<?php
	class GroupManagementController extends AdminController {

		public function filters() {
			return array(
				'accessControl' => 'accessControl',
			);
		}

		/**
		 * Returns list of allowed actions.
		 * @return array actions configuration.
		 */
		public function accessRules() {
			$res = array();

			$res[] = array(
				'allow',
				'roles' => array('/framework/admin/groupmanagement/view'),
				'actions' => array(
					'index',
					'groupAutocomplete',
					'assign',
				),
			);

			$res[] = array(
				'allow',
				//'roles' => array('/framework/admin/groupmanagement/associate_user'),
				'roles' => array('/framework/admin/groupmanagement/mod'),
				'actions' => array(
					'assignUsers',
					'assignUsersToGroup',
					'deleteGroupMember',
					'groupMemberAutocomplete',
					'importFromCsv',
                    'createGroup',
                    'getFieldData',
                    'ruleField',
                    'axProgressiveAutoAssignUsersToSingleGroup',
                    'axProgressiveUnassignUsersFromGroup',
                    'processGroupAfterAutoAssign',
                    'editGroupAssignment',
				),
			);

			$res[] = array(
				'allow',
				'roles' => array('/framework/admin/groupmanagement/add'),
				'actions' => array(
					'createGroup',
					'processNewGroup'
				),
			);

			$res[] = array(
				'allow',
				'roles' => array('/framework/admin/groupmanagement/mod'),
				'actions' => array('updateGroup'),
			);

			$res[] = array(
				'allow',
				'roles' => array('/framework/admin/groupmanagement/del'),
				'actions' => array('deleteGroup'),
			);

			$res[] = array(
				'deny',
				// deny all
				'users' => array('*'),
				'deniedCallback' => array($this, 'adminHomepageRedirect'),
			);

			return $res;
		}

		/**
		 * This is the default 'index' action that is invoked
		 * when an action is not explicitly requested by users.
		 */
		public function actionIndex() {
            $cs = Yii::app()->getClientScript();
			$themeUrl = Yii::app()->theme->baseUrl;

			// FCBK
			$cs->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
			$cs->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');

			$assetsUrl = $this->getAdminJsAssetsUrl();
			Yii::app()->clientScript->registerScriptFile($assetsUrl . '/group_assign_rules.js');

            DatePickerHelper::registerAssets();

			$groupModel = new CoreGroup();
			if (Yii::app()->request->isAjaxRequest && isset($_REQUEST['CoreGroup'])) {
				$groupModel->attributes = $_REQUEST['CoreGroup'];
			}
			$groupModel->scenario = 'group_management';
			$this->render('index', array(
				'groupModel' => $groupModel,
			));
		}

		public function actionGroupAutocomplete() {
			$query = addcslashes($_REQUEST['data']['query'], '%_');
			$criteria = new CDbCriteria(array(
				'condition' => '(groupid LIKE :match OR description LIKE :match2) AND hidden = :hidden AND type = :type',
				'params' => array(
					':match' => "/%$query%",
					':match2' => "%$query%",
					':hidden' => 'false',
					':type' => 'free',
				),
			));
			$groups     = CoreGroup::model()->findAll($criteria);
			$groupsList = array_values(CHtml::listData($groups, 'idst', 'groupid'));
			if (!empty($groupsList)) {
				foreach ($groupsList as $key => $value) {
					$groupsList[$key] = CoreGroup::model()->relativeId($value);
				}
			}
			$this->sendJSON(array('options' => $groupsList));
		}

        /**
         * Function gets HTML data for selected additional field from the dropdown.
         * Function is using renderRuleField to generate the HTML
         *
         * POST param 'id' => id_common of the addditional fild
         * POST param 'index' => the next index the generate the sequences of medels CoreGroupAssignRules
         */
        public function actionGetFieldData(){
            if(Yii::app()->request->isAjaxRequest){
                $id = Yii::app()->request->getParam('id');
                $index = Yii::app()->request->getParam('index');
                $lang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

                $field = CoreUserField::model()->findByPk($id);

                $this->renderRuleField($field, $index);
            }
        }

        /**
         *
         * Function is rendering single Additional field HTML.
         *
         * @param CoreUserField $field - the additional field instance
         * @param type $index - idnex to generate the sequence of models CoreGroupAssignRules
         */
        private function renderRuleField(CoreUserField $field, $index){

            $lang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
            $label = "";
            $rule = "";
            $fieldID = null;
            $fieldID = 'rule' . $field->getFieldId() . time();
            //Hidden field for CoreGroupAssignRules['idFieldCommon']
            $idFieldHidden = CHtml::hiddenField('CoreGroupAssignRules['.$index.'][idFieldCommon]', $field->getFieldId());

            $type = $field->getFieldType();

            switch($type){
                case CoreUserField::TYPE_TEXT:
                    $label = CHtml::label($field->translation, 'field'. $field->getFieldId()) . "&nbsp&nbsp";
                    $rule = CHtml::dropDownList('CoreGroupAssignRules['.$index.'][condition_type]', '0', array(
                        CoreGroupAssignRules::CONDITION_TYPE_CONTAINS => Yii::t('standard', '_CONTAINS'),
                        CoreGroupAssignRules::CONDITION_TYPE_EQUAL => Yii::t('standard', '_EQUAL'),
                        CoreGroupAssignRules::CONDITION_TYPE_NOT_EQUAL => Yii::t('standard', '_NOT_EQUAL')
                    ), array(
                        'class' => 'input-small'
                    )) . "&nbsp&nbsp";
                    $rule .= CHtml::textField('CoreGroupAssignRules['.$index.'][condition_value]', '', array('id' => 'filed'.$field->getFieldId(), 'class' => 'input-medium'), array(
                        'class' => 'input-medium'
                    )) . "&nbsp&nbsp";
                    break;
                case CoreUserField::TYPE_COUNTRY:
                    $data = CoreCountry::getCountryListWithId();
                    $label = CHtml::label($field->translation . ' ' . Yii::t('group_management', 'is'), 'rule' . $field->getFieldId()) . "&nbsp&nbsp";
                    $rule = CHtml::dropDownList('CoreGroupAssignRules['.$index.'][condition_value]', '', $data, array(
                        'id' => $fieldID,
                        'class' => 'fbkcompleate'
                    )) . "&nbsp&nbsp";
                    $rule .= CHtml::hiddenField('CoreGroupAssignRules['.$index.'][condition_type]', CoreGroupAssignRules::CONDITION_TYPE_IN);
                    break;
                case CoreUserField::TYPE_YESNO:
                    $label = CHtml::label($field->translation . ' ' . Yii::t('group_management', 'is'), $fieldID) . "&nbsp&nbsp";
                    $rule = CHtml::dropDownList('CoreGroupAssignRules['.$index.'][condition_value]', '', array(
                        '1' => 'Yes',
                        '2' => 'No'
                    ), array('class' => 'input-small'));
                    $rule .= CHtml::hiddenField('CoreGroupAssignRules['.$index.'][condition_type]', CoreGroupAssignRules::CONDITION_TYPE_IS);
                    break;
                case CoreUserField::TYPE_DATE:
                    $localDateFormat = Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short'));
                    $label = CHtml::label($field->translation, $fieldID) . "&nbsp&nbsp";
                    $rule = "<div class='span4'>" . CHtml::dropDownList('CoreGroupAssignRules['.$index.'][condition_type]', '', array(
                        '0' => '---',
                        CoreGroupAssignRules::CONDITION_TYPE_FROM_TO => Yii::t('standard', '_FROM') . '/' . Yii::t('standard', '_TO'),
                        CoreGroupAssignRules::CONDITION_TYPE_BEFORE => Yii::t('group_management','before'),
                        CoreGroupAssignRules::CONDITION_TYPE_AFTER => Yii::t('group_management','after'),
                        CoreGroupAssignRules::CONDITION_TYPE_IS => Yii::t('group_management', 'is')
                    ), array(
                        'style' => 'width: 113px;',
                        'data-div' => $fieldID,
                        'data-index' => $index,
                        'data-date_format' => $localDateFormat,
                        'id' => 'date_select_' . $fieldID
                    )) . "</div>";
                    $rule .= "<div class='span8' id='" . $field->getFieldType() . '_dates_' . $fieldID ."'></div>";
                    break;
                case CoreUserField::TYPE_DROPDOWN:
                    $dropdown = new FieldDropdown;
                    $dropdown->id_field = $field->getFieldId();
                    $dropdown->type = $field->getFieldType();
                    $options = $dropdown->getOptions($lang);
                    $label = CHtml::label($field->translation . ' ' . Yii::t('group_management', 'is'), '' . $field->getFieldId()) . "&nbsp&nbsp";
                    $rule = CHtml::dropDownList('CoreGroupAssignRules['.$index.'][condition_value]', '', $options, array(
                        'id' => $fieldID,
                        'class' => 'fbkcompleate'
                    )) . "&nbsp&nbsp";
                    $rule .= CHtml::hiddenField('CoreGroupAssignRules['.$index.'][condition_type]', CoreGroupAssignRules::CONDITION_TYPE_IN);
                    break;
            }
            $content = "<div class='row-fluid controls form-inline'><div class='span3 text-center'>" . $label . "</div><div class='span8'>" . $rule . $idFieldHidden . "</div><div class='span1' style='padding-top: 6px;'><a href='#' class='delete-action'></a></div></div></div>";
            $this->sendJSON(array('html' => $content, 'id' => $fieldID, 'type' => $type));
        }

        /**
         * Function is wizard base to create new group with assign rules.
         */
        public function actionCreateGroup(){
            $fromStep = Yii::app()->request->getParam('fromStep', (int) 0);
            $toStep = 0;
            $coreGroup = new CoreGroup();
            $assignRules = new CoreGroupAssignRules();
            $groupId = Yii::app()->request->getParam('idst');

            //If we want to edit a group
            if(!empty($groupId)){
                $coreGroup = CoreGroup::model()->findByPk($groupId);
                $coreGroup->groupid = $coreGroup->relativeId($coreGroup->groupid);
                $assignRules = CoreGroupAssignRules::model()->findAll('idGroup = :group', array(
                    ':group' => $groupId
                ));

                foreach ($assignRules as $condition) {
                    $fieldType = Yii::app()->db->createCommand()
                        ->select('type')
                        ->from(CoreUserField::model()->tableName())
                        ->where('id_field=:id', array(':id' => $condition['idFieldCommon']))
                        ->limit(1)->queryScalar();

                    if ($fieldType == CoreUserField::TYPE_DATE) {
                        if (strpos($condition['condition_value'], ',') !== false) {
                            $date = explode(',', $condition['condition_value']);
                            $date[0] = $this->transformCustomDateToCurrentDateFormat($date[0]);
                            $date[1] = $this->transformCustomDateToCurrentDateFormat($date[1]);
                            $condition['condition_value'] = implode(',', $date);
                        } else {
                            $condition['condition_value'] = $this->transformCustomDateToCurrentDateFormat($condition['condition_value']);
                        }
                    }
                }
            }


            if(!empty($_POST['CoreGroup'])){
                $coreGroup->attributes = $_POST['CoreGroup'];
                $coreGroup->groupid = $coreGroup->absoluteId($coreGroup->groupid);
                if (!$coreGroup->validate()) {
                    //If its not valid, go back
                    $toStep = $fromStep - 1;
                }
                else{
                    if($fromStep == 1){
                        //if we choose mannualy assign or automatically
                        if($coreGroup->assign_rules == 1){
							if(Yii::app()->user->getIsGodadmin())
								$toStep = $fromStep;
							else
							{
								$coreGroup->save();
								$toStep = 2;
							}
                        } else{
                            //Delete all assign rules, if the group alredy exist
                            if(!empty($coreGroup->idst)){
                                $criteria = new CDbCriteria();
                                $criteria->addCondition('idGroup = :group');
                                $criteria->params = array(
                                    ':group' => $coreGroup->idst
                                );
                                $del = CoreGroupAssignRules::model()->deleteAll($criteria);
                            }
                            //We don't want assign rulse, so just insert a group!
                            $coreGroup->show_on_platform = 'framework,lms,';
                            $coreGroup->assign_rules = 0;
                            $coreGroup->save();
                            Yii::app()->session['newGroupId'] = $coreGroup->idst;
                            //handle power users assignment
                            if (Yii::app()->user->getIsAdmin()) {
                                //Check for existing records Group-PowerUser
                                if(CoreAdminTree::checkPowerUserMember(Yii::app()->user->id, $coreGroup->idst) < 1) {
                                    $rs = CoreAdminTree::model()->addPowerUserMember(Yii::app()->user->id, $coreGroup->idst);
                                }
                            }
                            $toStep = 2;
                        }
                    } else if($fromStep == 2 && !empty($_POST['CoreGroupAssignRules']) &&  empty($_POST['prev'])){

                        $coreGroup->save();
						Yii::app()->session['newGroupId'] = $coreGroup->idst;
                        $result = $this->handleAssignedRules($_POST['CoreGroupAssignRules'], $coreGroup);
                        if($result['success'] == true){
                            if(!isset($_POST['apply_now'])){
                                //Go to next step
                                $toStep = $fromStep;
                            } else{
                                //Go to progressive controller to apply the group rules!
                                $this->autoAssignUsersToSingleGroupProgressively($coreGroup->idst);
                                $toStep = $fromStep + 1;
                            }
                        } else{
                            $toStep = $fromStep - 1;
                            $assignRules = $result['assignRulesModel'];
                        }
                }
                }
                //Reset the group name (remove '\') if we have some error
                $coreGroup->groupid = $coreGroup->relativeId($coreGroup->groupid);
            } else
				$toStep = $fromStep;

            switch($toStep){
                case 0:
                    //We are in the first step when filling the name and the description of the group
                    $this->renderPartial('create_group_step1', array(
                        'model' => $coreGroup,
                    ));
                    Yii::app()->end();
                    break;
                case 1:
                    //Here is the dialog for adding group rules
                    $criteria = new CDbCriteria();
					$criteria->addInCondition('type', array('country','date','dropdown','textfield','yesno'));
                    //Get all additional fields, based on the language
                    $fields = CoreUserField::model()->findAll($criteria);
                    $this->renderPartial('create_group_step2', array(
                        'modelGroup' => $coreGroup,
                        'ruleModels' => $assignRules,
                        'nextId' => count($assignRules),
                        'fields' => CHtml::listData($fields, 'id_field', 'translation')
                     ));
                     Yii::app()->end();
                    break;
                case 2:
                    //Hewe we are in the Success dialog
                    Yii::import('common.widgets.MainMenu');
					if(isset(Yii::app()->session['newGroupId']))
					{
						$groupId = Yii::app()->session['newGroupId'];
						unset(Yii::app()->session['newGroupId']);
					}
                    $this->renderPartial('_processNewGroupNew', array('groupId' => $groupId));
                    Yii::app()->end();
                    break;
            }
        }

        /*
         * Method runs the progressive controller for applying assign rules to single group
         */
        public function autoAssignUsersToSingleGroupProgressively($groupId){
            $command = Yii::app()->db->createCommand()
                    ->select('idst')
                        ->from('core_user cu');
            $users = $command->queryColumn();
            if (isset($_POST['apply_now']) && intval($_POST['apply_now']) === 1) {
                $operator = $_POST['CoreGroup']['assign_rules_logic_operator'];
                if($operator == 'OR')
                    $users = array();
                foreach ($_POST['CoreGroupAssignRules'] as $condition) {
                    $fieldId = $condition['idFieldCommon'];
                    $command = Yii::app()->db->createCommand()
                            ->select('idst')
                            ->from('core_user cu');
                    $command->join(CoreUserFieldValue::model()->tableName() . ' cfu', 'cu.idst = cfu.id_user');
                    if(Yii::app()->user->getIsPu()){
                        // Searching in PU assigned users
                        $command->join(CoreUserPU::model()->tableName().' pu', 'pu.user_id=cu.idst AND pu.puser_id=:puId', array(':puId'=>Yii::app()->user->getIdst()));
                    }
                    $conditions = '';
                    $glueLeft = '';
                    $glueRight = '';
					$fieldType = Yii::app()->db->createCommand()
					    ->select('type')
                        ->from(CoreUserField::model()->tableName())
                        ->where('id_field = :id', array(':id' => $fieldId))
                        ->limit(1)->queryScalar()
                    ;
					$isDateField = false;
					if ($fieldType == CoreUserField::TYPE_DATE) {
						$isDateField = true;
					}

					switch ($condition['condition_type']) {
						case CoreGroupAssignRules::CONDITION_TYPE_IN:
							$glueLeft = 'IN(';
							$glueRight = ')';
							if (is_array($condition['condition_value']))
								$condition['condition_value'] = implode(', ', $condition['condition_value']);
							break;
                        case CoreGroupAssignRules::CONDITION_TYPE_CONTAINS:
							$glueLeft = 'LIKE BINARY"%';
							$glueRight = '%"';
							break;
						case CoreGroupAssignRules::CONDITION_TYPE_BEFORE:
							$glueLeft = '<"';
							$glueRight = '"';
							$condition['condition_value'] = $this->transformCustomDateToDbDate($condition['condition_value']);
							break;
						case CoreGroupAssignRules::CONDITION_TYPE_AFTER:
							$glueLeft = '>"';
							$glueRight = '"';
							$condition['condition_value'] = $this->transformCustomDateToDbDate($condition['condition_value']);
							break;
						case CoreGroupAssignRules::CONDITION_TYPE_EQUAL:
						case CoreGroupAssignRules::CONDITION_TYPE_IS:
							if ($isDateField)
								$condition['condition_value'] = $this->transformCustomDateToDbDate($condition['condition_value']);
							$glueLeft = '="';
							$glueRight = '"';
							break;
						case CoreGroupAssignRules::CONDITION_TYPE_NOT_EQUAL:
							if ($isDateField)
								$condition['condition_value'] = $this->transformCustomDateToDbDate($condition['condition_value']);
							$glueLeft = '!="';
							$glueRight = '"';
							break;
						case CoreGroupAssignRules::CONDITION_TYPE_FROM_TO:
							$condition['condition_value'][0] = $this->transformCustomDateToDbDate($condition['condition_value'][0]);
							$condition['condition_value'][1] = $this->transformCustomDateToDbDate($condition['condition_value'][1]);
							$condition['condition_value'] = '>="' . $condition['condition_value'][0] . '" AND cfu.field_' . $fieldId . ' <= "' . $condition['condition_value'][1] . '"';
							break;
					}
					$conditions = 'cfu.field_' . $fieldId . ' ' . $glueLeft . $condition['condition_value'] . $glueRight.' AND cfu.field_' . $fieldId . ' != ""';
					$command->where($conditions);

					$usersPartial = $command->queryColumn();
                    if($operator == 'AND'){
                        $users = array_intersect($users, $usersPartial);
                    } elseif ($operator == 'OR') {
                        $users = array_merge($users, $usersPartial);
                    }
                }
            }
            $users = array_unique(array_values($users));

            $this->renderPartial('_auto_assign_users_to_single_group', array(
                'users' => implode(',', $users),
                'group' => $groupId,
            ));
            Yii::app()->end();
        }

        public function actionAxProgressiveUnassignUsersFromGroup(){
            $chunkSize = 20;
            $tmpUsers = array();
            $users = $_POST['users'];
            $group = $_POST['group'];

            // Check if this is string
            if(is_string($users)) {
                // if is only one user
                if(!strpos($users,',') !== false)
                    $userList = array($users);
                else
                    $userList = explode(',', $users);
            }
            else
                $this->sendJSON(array());

            // Get current offset and calculate stop flag
            $offset = Yii::app()->request->getParam('offset', 0);
            $totalCount = count($userList);
            $newOffset = $offset + $chunkSize;
            $start = ($offset==0); // first run
            $stop = ($newOffset < $totalCount) ? false : true;
            $counter = 0;

            for ($index = $offset; ($index < $newOffset && $index < $totalCount); $index++) { //NOTE: $newOffset may be greater than $totalCount, but we wouldn't count over $totalCount
                $counter ++;
                if ( $counter > $chunkSize ) break; //NOTE: this shouldn't happen
                // Get current enrollment "job". If we are over the limit, invalidate value and stop this iteration
                $userId = ( isset( $userList[ $index ] ) ? $userList[ $index ] : false );
                if (!$userId || empty($userId))
                    continue;
                $tmpUsers[] = $userId;
            }

            $groupMembers = CoreGroupMembers::model()->findAllByAttributes(array('idst' => $group, 'idstMember' => $tmpUsers));
            foreach ($groupMembers as $member){
                $member->delete();
            }

            $completedPercent = $totalCount==0 ? 100 : $index/$totalCount*100;
            if($completedPercent>100) $completedPercent = 100;
            if($completedPercent<0) $completedPercent = 0;

            $html = $this->renderPartial('_progressive_user_unassign', array(
                'offset' 				=> $offset,
                'processedUsers' 		=> $index,
                'totalUsers' 			=> $totalCount,
                'completedPercent' 		=> intval($completedPercent),
                'start' 				=> ($offset == 0),
                'stop' 					=> $stop,
                'afterUpdateCallback'	=> ($stop) ? "$.fn.yiiListView.update('group-member-management-list');" : null
            ), true, true);

            // Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
            // These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stopping when $stop == true)
            $data = array(
                'offset'				=> $newOffset,
                'stop'					=> $stop,
                'users'					=> $users,
                'group'                 => $group,
            );

            $response = new AjaxResult();
            $response->setStatus(true);
            $response->setHtml($html)->setData($data)->toJSON();
            Yii::app()->end();
        }

        /**
         * Progressively action for applying rules for fingle group
         */
        public function actionAxProgressiveAutoAssignUsersToSingleGroup(){
            $chunkSize = 20;
            $tmpUsers = array();
            $users = $_POST['users'];
            $group = $_POST['group'];

            // Check if this is string
            if(is_string($users)) {
                // if is only one user
                if(!strpos($users,',') !== false)
                    $userList = array($users);
                else
                    $userList = explode(',', $users);
            }
            else
                $this->sendJSON(array());


            // Get current offset and calculate stop flag
            $offset = Yii::app()->request->getParam('offset', 0);
            $totalCount = count($userList);
            $newOffset = $offset + $chunkSize;
            $start = ($offset==0); // first run
            $stop = ($newOffset < $totalCount) ? false : true;
            $counter = 0;

            for ($index = $offset; ($index < $newOffset && $index < $totalCount); $index++) { //NOTE: $newOffset may be greater than $totalCount, but we wouldn't count over $totalCount
                $counter ++;
                if ( $counter > $chunkSize ) break; //NOTE: this shouldn't happen
                // Get current enrollment "job". If we are over the limit, invalidate value and stop this iteration
                $userId = ( isset( $userList[ $index ] ) ? $userList[ $index ] : false );
                if (!$userId || empty($userId))
					continue;
                $tmpUsers[] = $userId;
            }

            try {
                $criteria = new CDbCriteria(array(
                    'condition' => 'idGroup = :id',
                    'params' => array(
                        ':id' => $group
                    )
                ));

                $rules = CoreGroupAssignRules::model()->findAll($criteria);

                $criteria = new CDbCriteria(array(
                    'condition' => 'idst = :id',
                    'params' => array(
                        ':id' => $group
                    )
                ));
                $groupModel = CoreGroup::model()->find($criteria);
                $logicOperator = $groupModel->assign_rules_logic_operator;

                foreach($tmpUsers as $idUser => $user){
                    //Check if the user is already in the group
                    $alreadyInGroup = CoreGroupMembers::model()->find('idst = :group AND idstMember = :user', array(
                        ':group' =>$group,
                        ':user' => $user
                    ));

                    //If it is continue to nex user
                    if(!empty($alreadyInGroup)){
                        continue;
                    }

                    $success = array();
                    $countRules = count($rules);
                    $userEntry = Yii::app()->db->createCommand()
                        ->from('core_user_field_value')
                        ->where('id_user = :idUser', array(
//                            ':idField' => $rule->idFieldCommon,
                            ':idUser' => $user
                        ))
                        ->queryRow();
                    foreach($rules as $idRule => $rule){
                        $fieldColumn = 'field_' . $rule->idFieldCommon;
                        if(!empty($userEntry[$fieldColumn]) && $userEntry[$fieldColumn] != '') {
                            $fieldType = $rule->getFieldType();
                            $ruleConditionType = $rule->condition_type;
                            $ruleValue = $rule->condition_value;
                            switch($fieldType){
                                case CoreUserField::TYPE_DATE:
                                    switch($ruleConditionType){
                                        case CoreGroupAssignRules::CONDITION_TYPE_AFTER:
                                            $ruleValue =  date('Y-m-d', strtotime($ruleValue));
                                            if($ruleValue < $userEntry[$fieldColumn]){
                                                $success[$idRule] = true;
                                            } else{
                                                $success[$idRule] = false;
                                            }
                                            break;
                                        case CoreGroupAssignRules::CONDITION_TYPE_BEFORE:
                                            $ruleValue =  date('Y-m-d', strtotime($ruleValue));
                                            if($ruleValue > $userEntry[$fieldColumn]){
                                                $success[$idRule] = true;
                                            } else{
                                                $success[$idRule] = false;
                                            }
                                            break;
                                        case CoreGroupAssignRules::CONDITION_TYPE_FROM_TO:
                                            $dates = explode(',', $ruleValue);
                                            foreach($dates as $idDate => $date){
                                                $dates[$idDate] = date('Y-m-d', strtotime($date));
                                            }
                                            if($dates[0] <= $userEntry[$fieldColumn] && $dates[1] >= $userEntry[$fieldColumn]){
                                                $success[$idRule] = true;
                                            } else{
                                                $success[$idRule] = false;
                                            }
                                            break;
                                        case CoreGroupAssignRules::CONDITION_TYPE_IS:
                                            $ruleValue =  date('Y-m-d', strtotime($ruleValue));
                                            if($ruleValue == $userEntry[$fieldColumn]){
                                                $success[$idRule] = true;
                                            } else{
                                                $success[$idRule] = false;
                                            }
                                            break;
                                    }
                                    break;
                                case CoreUserField::TYPE_COUNTRY:
                                case CoreUserField::TYPE_DROPDOWN:
                                    if(empty($userEntry[$fieldColumn])){
                                        $success[$idRule] = false;
                                        break;
                                    }
                                    $values = explode(',', $ruleValue);
                                    if(in_array($userEntry[$fieldColumn], $values)){
                                        $success[$idRule] = true;
                                    } else{
                                        $success[$idRule] = false;
                                    }
                                    break;
                                case CoreUserField::TYPE_TEXT:
                                    switch($ruleConditionType){
                                        case CoreGroupAssignRules::CONDITION_TYPE_CONTAINS:
                                            $success[$idRule] = (strpos($userEntry[$fieldColumn], $ruleValue) !== false) ? true : false;
                                            break;
                                        case CoreGroupAssignRules::CONDITION_TYPE_EQUAL:
                                            if(strcmp($ruleValue, $userEntry[$fieldColumn]) == 0){
                                                $success[$idRule] = true;
                                            } else{
                                                $success[$idRule] = false;
                                            }
                                            break;
                                        case CoreGroupAssignRules::CONDITION_TYPE_NOT_EQUAL:
                                            if(strcmp($ruleValue, $userEntry[$fieldColumn]) != 0){
                                                $success[$idRule] = true;
                                            } else{
                                                $success[$idRule] = false;
                                            }
                                            break;
                                    }
                                    break;
                                case CoreUserField::TYPE_YESNO:
                                    if($ruleValue == $userEntry[$fieldColumn]){
                                        $success[$idRule] = true;
                                    } else{
                                        $success[$idRule] = false;
                                    }
                                    break;
                            }
                        }
                    }
                    if(!empty($success)){
                        switch($logicOperator){
                            case CoreGroupAssignRules::LOGIC_OPERATOR_AND:
                                if(!in_array(false, $success) && count($success) == $countRules){
                                    $newGroupMember = new CoreGroupMembers();
                                    $newGroupMember->idst = $group;
                                    $newGroupMember->idstMember = $user;
                                    $newGroupMember->save();
                                }
                                break;
                            case CoreGroupAssignRules::LOGIC_OPERATOR_OR:
                                if(in_array(true, $success)){
                                    $newGroupMember = new CoreGroupMembers();
                                    $newGroupMember->idst = $group;
                                    $newGroupMember->idstMember = $user;
                                    $newGroupMember->save();
                                }
                                break;
                        }
                    }
                }

            } catch (CException $e) {
                Yii::log($e->getMessage(), CLogger::LEVEL_ERROR, __CLASS__);
                $response = new AjaxResult();
                $response->setStatus(true);
                $response->setHtml($e->getMessage())->setData(array('stop'=>1))->toJSON();
                Yii::app()->end();
            }

            $completedPercent = $totalCount==0 ? 100 : $index/$totalCount*100;
            if($completedPercent>100) $completedPercent = 100;
            if($completedPercent<0) $completedPercent = 0;

            $html = $this->renderPartial('_progressive_auto_assign_users_to_single_group', array(
                'offset' 				=> $offset,
                'processedUsers' 		=> $index,
                'totalUsers' 			=> $totalCount,
                'completedPercent' 		=> intval($completedPercent),
                'start' 				=> ($offset == 0),
                'stop' 					=> $stop,
                'afterUpdateCallback'	=> ($stop) ? '$.fn.yiiGridView.update("group-management-grid");' : null
            ), true, true);

            // Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
            // These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stopping when $stop == true)
            $data = array(
                'offset'				=> $newOffset,
                'stop'					=> $stop,
                'users'					=> $users,
                'group'                 => $group,
            );

            $response = new AjaxResult();
            $response->setStatus(true);
            $response->setHtml($html)->setData($data)->toJSON();
            Yii::app()->end();
        }

        /**
         * Render when finish creating a group
         */
        public function actionProcessGroupAfterAutoAssign(){
            if(Yii::app()->request->isAjaxRequest){
                $this->renderPartial('process_after_auto_assign_users');
                Yii::app()->end();
            }
        }

        /**
         * Action for editing group assignments
         */
        public function actionEditGroupAssignment()
        {
            $groupId = Yii::app()->request->getParam('idst');

            $assignRules = CoreGroupAssignRules::model()->findAll('idGroup = :group', array(
                ':group' => $groupId
            ));
            $coreGroup = CoreGroup::model()->findByPk($groupId);

            $step = 2;

            if(!empty($_POST['CoreGroupAssignRules'])){
                $coreGroup = CoreGroup::model()->find('idst = :id', array(
                    ':id' => $groupId
                ));

                if($coreGroup){
                    //Change the logic operator!
                    $coreGroup->assign_rules_logic_operator = $_POST['CoreGroup']['assign_rules_logic_operator'];
                    $coreGroup->save();
                    $result = $this->handleAssignedRules($_POST['CoreGroupAssignRules'], $coreGroup);
                    if($result['success'] == true){
                        if(!empty($_POST['apply_now'])){
                            $step = 3;
                        } else{
                            $step = 4;
                        }
                    }
                    else{
                        $step = 2;
                        $assignRules = $result['assignRulesModel'];
                    }
                }
            }
            switch($step){
                case 2:
                    $criteria = new CDbCriteria();
					$criteria->addInCondition('type', array('country','date','dropdown','textfield','yesno'));
                    //Get all additional fields, based on the language
                    $fileds = CoreUserField::model()->language(Yii::app()->getLanguage())->findAll($criteria);

                    $this->renderPartial('create_group_step2', array(
                        'modelGroup' => $coreGroup,
                        'edit' => true,
                        'ruleModels' => $assignRules,
                        'nextId' => count($assignRules),
                        'fields' => CHtml::listData($fileds, 'id_field', 'translation')
                     ));
                     Yii::app()->end();
                    break;
                case 3:
                    $this->autoAssignUsersToSingleGroupProgressively($groupId);
                    break;
                case 4:
                    //Hewe we are in the Success dialog
                    Yii::import('common.widgets.MainMenu');
                    $this->renderPartial('process_after_auto_assign_users', array('groupId' => $groupId));
                    Yii::app()->end();
                    break;
        }
        }

        /**
         *
         * Function that handles the creating or updating assign rules for a group.
         * Its used when validating a assign rules.
         *
         * @param array $rules array of assign rules
         * @param array $group CoreGroup model
         * @return array Returns a success and an array of CoreGroupAssingRules that are modified
         */
        private function handleAssignedRules($rules, $group = null){
            if(empty($rules)){
                return false;
            }

            $models = array();
            $result = array();
            $success = true;
            $newRecords = false;
            $updatedIds = array();
            $dateFieldsIds = Yii::app()->db->createCommand()
                ->select('DISTINCT(id_field)')
                ->from(CoreUserField::model()->tableName())
                ->where('type = :type', array(':type' => CoreUserField::TYPE_DATE))->queryColumn();
            foreach($rules as $id => $rule){
                // this one is needed, because of the different date format, saved in the db, which is "m/d/Y",
                // but we pass now from the datepicker the current local date format
                if(in_array($rule['idFieldCommon'], $dateFieldsIds)){
                    $dateFormat = Yii::app()->localtime->getPHPLocalDateFormat();
                    if(is_array($rule['condition_value'])) {
                        $date1 = DateTime::createFromFormat($dateFormat, $rule['condition_value'][0]);
                        $rule['condition_value'][0] = $date1->format('m/d/Y');
                        $date2 = DateTime::createFromFormat($dateFormat, $rule['condition_value'][1]);
                        $rule['condition_value'][1] = $date2->format('m/d/Y');
                    } else {
                        $date = DateTime::createFromFormat($dateFormat, $rule['condition_value']);
                        $rule['condition_value'] = $date->format('m/d/Y');
                    }
                }
                $assignRules = CoreGroupAssignRules::model()->find('id = :id', array(
                    ':id' => $rule['id']
                ));

                if(!$assignRules){
                    $assignRules = new CoreGroupAssignRules();
                    $assignRules->attributes = $rule;
                    $isArray = false;
                    $arraySize = 0;
                    if(is_array($rule['condition_value'])){
                        $assignRules->condition_value = implode(',', $rule['condition_value']);
                        $isArray = true;
                        $arraySize = count($rule['condition_value']);
                    }

                    $hasError = false;

                    if($isArray == true && $arraySize > 1){

                    	$hasError = (strlen($assignRules->condition_value) <= 1) ? true : false;
                   	}

                    if ($hasError == true){

                    	$assignRules->addError('condition_value', Yii::t('standard', 'Please fill all required fields'));
                    	$success = false;
                    } else{
                    	$assignRules->idGroup = 0;
                    	$assignRules->id = null;
                    	$assignRules->isNewRecord = true;
                    	if($assignRules->validate()){
                    		$updatedIds[] = $assignRules->id;
                    		$success = (!$success) ? false : true;
                    	} else{
                    		$success = false;
                    	}
                    	$newRecords = true;
                    }
                } else{
                    $ruleValue = ((!is_array($rule['condition_value']) && !empty($rule['condition_value'])) || (is_array($rule['condition_value']) && array_search(null, $rule['condition_value']) === false)) ? $rule['condition_value'] : null;

                    if(!$ruleValue){
                        $assignRules->addError('condition_value', Yii::t('standard', 'Please fill all required fields'));
                        $success = false;
                    } else{
                        $assignRules->condition_type = $rule['condition_type'];
                        if(is_array($ruleValue) && $ruleValue !== null){
                            $assignRules->condition_value = implode(',', $rule['condition_value']);
                        } else{
                            $assignRules->condition_value = $rule['condition_value'];
                        }
                        if($assignRules->save()){
                            $updatedIds[] = $assignRules->id;
                        }
                }
                }
                $models[$id] = $assignRules;
            }

            //Delete the unnecessary fields
            if(!empty($updatedIds)){
                $updatedIds = array_filter($updatedIds);
                $criteria = new CDbCriteria();
                $criteria->addCondition('idGroup = :group');
                $criteria->params = array(
                    ':group' => $group->idst
                );
                $criteria->addNotInCondition('id', $updatedIds);

                $del = CoreGroupAssignRules::model()->deleteAll($criteria);
            }

            //Enter here only if creating a new group
            if ($success === true) {
                if ($newRecords) {
                    //Change the new name of the group(if any)
                    $group->attributes = $_POST['CoreGroup'];
                    $group->groupid = $group->absoluteId($group->groupid);
                    if (empty($group->idst)) {
                        if ($group->validate()) {
                            $group->show_on_platform = 'framework,lms,';
                            if ($group->save()) {
                                Yii::app()->session['newGroup'] = $group->idst;
                                if (Yii::app()->user->getIsAdmin()) {
                                    $rs = CoreAdminTree::model()->addPowerUserMember(Yii::app()->user->id, $group->idst);
                                }
                                foreach ($models as $key => $item) {
                                    $item->idGroup = $group->idst;
                                    $item->save();
                                }
                            }
                        }
                    } else {
                        $group->save();
                        $puModel = CoreAdminTree::model()->findByAttributes(array('idst' =>  $group->idst, 'idstAdmin' => Yii::app()->user->id));
                        if (!$puModel) {
                            if (Yii::app()->user->getIsAdmin()) {
                                $rs = CoreAdminTree::model()->addPowerUserMember(Yii::app()->user->id, $group->idst);
                            }
                        }
                        foreach ($models as $key => $item) {
                            $item->idGroup = $group->idst;
                            $item->save();
                        }
                    }
                }
            }

        $result['assignRulesModel'] = $models;
            $result['success'] = $success;

            return $result;
        }

		public function actionProcessNewGroup() {
			Yii::import('common.widgets.MainMenu');
			$groupId = Yii::app()->session['newGroupId'];
			unset(Yii::app()->session['newGroupId']);
			$this->sendJSON(array('html' => $this->renderPartial('_processNewGroup', array('groupId' => $groupId), true)));
		}

		public function actionUpdateGroup($idst) {
			$this->forward('createGroup', array('idst' => $idst));
		}

		public function actionDeleteGroup($idst) {
			$coreGroup = CoreGroup::model()->findByPk($idst);
			if (empty($coreGroup)) {
				$this->sendJSON(array());
			}

			if (!empty($_POST['CoreGroup'])) {
				if ($_POST['CoreGroup']['confirm']) {
					$coreGroup->delete();
					$this->sendJSON(array());
				}
			}
			if (Yii::app()->request->isAjaxRequest) {
				$content = $this->renderPartial('delete', array(
					'model' => $coreGroup,
				), true);
				$this->sendJSON(array(
					'html' => $content,
				));
			}
			Yii:app()->end();
		}

		public function actionAssign($id = null) {
            // FancyTree initialization
            Yii::app()->fancytree->init();

			// todo: i/ little fix for changing language on page
			// this code should be removed after fix language changing
			if ($id === null) {
				$id = isset(Yii::app()->session['currentGroupId']) ? Yii::app()->session['currentGroupId'] : null;
			}

			$groupModel = CoreGroup::model()->findByPk($id);
			Yii::app()->session['currentGroupId'] = $id;
			$groupMemberModel = new CoreGroupMembers();
			$groupMemberModel->users = new CoreUser();
			$groupMemberModel->idst = $id;
			if (Yii::app()->request->isAjaxRequest && isset($_REQUEST['CoreUser'])) {
				$groupMemberModel->users->userid = $_REQUEST['CoreUser']['search_input'];
//				pr($groupMemberModel->users->attributes);
			}
			if (Yii::app()->request->isAjaxRequest && isset($_REQUEST['CoreGroupMembers'])) {
				$groupMemberModel->attributes = $_REQUEST['CoreGroupMembers'];
			}

			// Register required script for users selector, as we are going to use it in this page
			UsersSelector::registerClientScripts();

			//do some cleaning, to avoid dirt in the databse (invalid users references)
			$groupModel->cleanUsersGroup();

			$this->render('assign', array(
				'groupModel' => $groupModel,
				'groupMemberModel' => $groupMemberModel,
			));
		}


		/**
		 * Intercept Selector submition and assign users to a given group
		 * @throws CException
		 */
		public function actionAssignUsersToGroup() {

			// Check group ID and its model (the group we are editing)
			$idGroup = Yii::app()->request->getParam('idGroup', false);

			$usersList = UsersSelector::getUsersList($_REQUEST);
			$useSession = false;

			try {

				if (empty($usersList)) {
					$useSession = true; // use session transport mechanic to preserve current selection
					UsersSelector::storeAllDataInSession($_REQUEST);
					throw new CException(Yii::t('standard', '_SELECT_USERS'));
				}

				$model = CoreGroup::model()->findByPk($idGroup);
				if (!$model) {
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				}

				$newUsersList = $usersList;

				$existing = CoreGroupMembers::model()->findAllByAttributes(array('idst' => $idGroup, 'idstMember' => $usersList));
				if (!empty($existing)) {
					$existingList = CHtml::listData($existing, 'idstMember', 'idstMember');
					$newUsersList = array_diff($usersList, $existingList);
				}

				//add a check to avoid the insert of invalid users in the group
				//example: while we are selecting users in the selector, at the same time someone may be deleting the same user/s from another connection
				$allUsersIdsts = array();
				$reader = Yii::app()->db->createCommand("SELECT idst FROM ".CoreUser::model()->tableName())->query(); //NOTE: this query is very fast even in case of thousands of users
				while ($record = $reader->read()) {
					$key = (int)$record['idst'];
					$allUsersIdsts[$key] = true;
				}

				//add physically the user in the group at DB level
				$countInserts = 0;
				if (!empty($newUsersList)) {
					foreach ($newUsersList as $idUser) {
						if (array_key_exists((int)$idUser, $allUsersIdsts)) {
							$newGroupMember = new CoreGroupMembers();
							$newGroupMember->idst = $idGroup;
							$newGroupMember->idstMember = $idUser;
							$newGroupMember->save();
							$countInserts++;
						} else {
							//log attempt to add an invalid user reference to the group
							Yii::log('Attempting to add invalid user reference #'.$idUser.' into group #'.$idGroup, CLogger::LEVEL_ERROR);
						}
					}
				}

				//TODO: this is better handled by some event catched into PowerUserApp module instead directly here !!
				if (PluginManager::isPluginActive('PowerUserApp')) {
					//power users assignments may need to be updated
					if (is_array($newUsersList) && !empty($newUsersList) && $countInserts > 0) {
						PowerUserAppModule::setUpdateJob($usersList);
					}
				}

				echo "<a class='auto-close success'></a>";

				Yii::app()->end();

			}
			catch (CException $e) {
				Yii::log($e->getMessage(),CLogger::LEVEL_ERROR);
				Yii::app()->user->setFlash('error', $e->getMessage());
				$selectorDialogUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
						'type' 	=> UsersSelector::TYPE_GROUPS_MANAGEMENT,
						'idGroup'	=> $idGroup,
						'useSessionData' => $useSession,
				));
				$this->redirect($selectorDialogUrl, true);
			}

			echo "<a class='auto-close success'></a>";
			Yii::app()->end();


		}




		public function actionAssignUsers($idst = null) {

			$groupModel = CoreGroup::model()->findByPk($idst);
			if (empty($groupModel)) {
				$this->sendJSON(array());
			}

			$user           = CoreUser::model();
			$user->scenario = 'search';
			$user->unsetAttributes();
			if (isset($_REQUEST['CoreUser'])) {
				$user->attributes = $_REQUEST['CoreUser'];
			}

			$group = new CoreGroup();
			if (isset($_REQUEST['CoreGroup'])) {
				$group->attributes = $_REQUEST['CoreGroup'];
			}

			$usersList = Yii::app()->userSelectHelper->getUsersListFromRequest(
				array(
					'users' => 'select-user-grid-selected-items',
					'groups' => 'select-group-grid-selected-items',
					'orgChart' => 'select-orgchart',
				)
			);

			if (!empty($usersList)) {
				$existsGroupMembers = CoreGroupMembers::model()->findAllByAttributes(array('idst' => $idst, 'idstMember' => $usersList));
				if (!empty($existsGroupMembers)) {
					$existsGroupMembersList = CHtml::listData($existsGroupMembers, 'idstMember', 'idstMember');
					$newUserList = array_diff($usersList, $existsGroupMembersList);
				} else {
					$newUserList = $usersList;
				}
				if (!empty($newUserList)) {
					foreach ($newUserList as $newGroupMemberId) {
						$newGroupMember = new CoreGroupMembers();
						$newGroupMember->idst = $idst;
						$newGroupMember->idstMember = $newGroupMemberId;
						$newGroupMember->save();
					}
				}
				$this->sendJSON(array('status' => 'saved'));
			}

			if (Yii::app()->request->isAjaxRequest) {

				$fancyTreeData = CoreOrgChartTree::buildFancytreeDataArray(false, true, true);
				$content = $this->renderPartial('//common/_usersSelector', array(
					'userModel' => $user,
					'groupModel' => $group,
					'isNeedRegisterYiiGridJs' => true,
					'fancyTreeData' => $fancyTreeData,
				), true, true);

				if (isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html')) {
					echo $content;
				} else {
					$this->sendJSON(array('html' => $content));
				}
			}
			Yii::app()->end();
		}

		public function actionDeleteGroupMember($idst = null) {
            $userIdsts = array();
            if (!empty($_POST['idst'])) {
                $idst = $_POST['idst'];
                if(is_string($idst))
                {
                    if(stripos($idst, ',')!==false)
                        $userIdsts = explode(',', $idst);
                    else
                        $userIdsts = array($idst);
                }
            } elseif(!empty($idst)) {
                $userIdsts = array($idst);
            }

            if (empty($userIdsts)) {
                $this->sendJSON(array());
            }

			$coreUsers = CoreUser::model()->findAllByAttributes(array('idst' => $userIdsts));
			if (empty($coreUsers)) {
				$this->sendJSON(array());
			}

			if (!empty($_POST['CoreUser'])) {
				if ($_POST['CoreUser']['confirm']) {
					$groupId = Yii::app()->session['currentGroupId'];

                    if(count($userIdsts) > 1) {
                        $html = $this->renderPartial('_mass_user_unassign', array(
                            'users' => $userIdsts,
                            'group' => $groupId
                        ), true);

                        $this->sendJSON(array('html'=>$html));
                        Yii::app()->end();
                    } else {
                        //delete one user
                        $groupMember = CoreGroupMembers::model()->findByAttributes(array('idst' => $groupId, 'idstMember' => $userIdsts));
                        $groupMember->delete();
					    $this->sendJSON(array());
                    }
				}
			}
			if (Yii::app()->request->isAjaxRequest) {
				$content = $this->renderPartial('deleteMember', array(
					'model' => new CoreUser(),
					'users' => $coreUsers,
                    'idst' => $userIdsts
				), true);
				$this->sendJSON(array(
					'html' => $content,
					'modalTitle' => Yii::t('standard', '_UNASSIGN'),
					'confirmBtnTitle' => Yii::t('standard', '_CONFIRM'),
					'cancelBtnTitle' => Yii::t('standard', '_CANCEL'),
				));
			}
			Yii::app()->end();
		}

		public function actionImportFromCsv($groupId = null) {
			if (empty($groupId) && !empty(Yii::app()->session['currentGroupId'])) {
				$groupId = Yii::app()->session['currentGroupId'];
			}
			$coreGroup = CoreGroup::model()->findByPk($groupId);

			if (empty($coreGroup)) {
				$this->sendJSON(array());
			}

			if (!empty($_FILES['ImportCsv'])) {
				$file    = CUploadedFile::getInstanceByName('ImportCsv');
				$users   = Yii::app()->csvParser->parse($file, array('hasHeader' => true));
				$result  = CoreGroupMembers::model()->processImportCSVUpload($groupId, $users);
				$content = $this->renderPartial('importFromCsvSuccess', array('result' => $result), true);
				$this->sendJSON(array(
					'status' => 'success',
					'html' => $content,
				));
			} else {
				$content = $this->renderPartial('importFromCsv', array('coreGroup' => $coreGroup), true);
			}

			$this->sendJSON(array(
				'html' => $content,
			));
		}

        private function transformCustomDateToDbDate($customDateString){
            $dateFormat = Yii::app()->localtime->getPHPLocalDateFormat();
            $date = DateTime::createFromFormat($dateFormat, $customDateString);
            $customDateString = $date->format('Y-m-d');
            return $customDateString;
	    }

        private function transformCustomDateToCurrentDateFormat($customDateString){
            $dateFormat = Yii::app()->localtime->getPHPLocalDateFormat();
            $date = DateTime::createFromFormat('m/d/Y', $customDateString);
            $customDateString = $date->format($dateFormat);
            return $customDateString;
        }
	}
