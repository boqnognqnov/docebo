<?php
class CertificateManagementController extends AdminController
{

	public function beforeAction($action) {


		if (!Yii::app()->request->isAjaxRequest) {
		}

		return parent::beforeAction($action);
	}

	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}

	/**
	 * Returns list of allowed actions.
	 * @return array actions configuration.
	 */
	public function accessRules() {
		$res = array();

		$res[] = array(
			'allow',
			'roles' => array('/lms/admin/certificate/view'),
			'actions' => array(
				'index',
				'released',
				'download',
				'downloadTemplate',
				// 'groupAutocomplete',
			),
		);

		$res[] = array(
			'allow',
			'roles' => array('/lms/admin/certificate/mod'),
			'actions' => array(
				'create',
				'update',
				'template',
				'delete',
				'generateCertificate',
				'removeCertificate',
				'deleteImage',
			),
		);

		// $res[] = array(
		// 	'allow',
		// 	'roles' => array('/framework/admin/groupmanagement/associate_user'),
		// 	'actions' => array(
		// 		'assign',
		// 		'assignUsers',
		// 		'deleteGroupMember',
		// 		'groupMemberAutocomplete',
		// 		'importFromCsv',
		// 	),
		// );

		// $res[] = array(
		// 	'allow',
		// 	'roles' => array('/framework/admin/groupmanagement/add'),
		// 	'actions' => array(
		// 		'createGroup',
		// 	),
		// );


		// $res[] = array(
		// 	'allow',
		// 	'roles' => array('/framework/admin/groupmanagement/del'),
		// 	'actions' => array('deleteGroup'),
		// );

		$res[] = array(
			'deny',
			// deny all
			'users' => array('*'),
			'deniedCallback' => array($this,'adminHomepageRedirect'),
		);

		return $res;
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
		$certificateModel = new LearningCertificate();

		Yii::app()->tinymce;

		$this->render('index', array(
			'certificateModel' => $certificateModel,
		));
	}

	public function actionCreate() {
		$certificateModel = new LearningCertificate('update');
		if (!empty($_POST['LearningCertificate'])) {
			$certificateModel->attributes = $_POST['LearningCertificate'];
			if ($certificateModel->validate()) {
				$certificateModel->save();
				$this->sendJSON(array());
			}
		}
		if (Yii::app()->request->isAjaxRequest) {
			$content           = $this->renderPartial('_form', array(
				'model' => $certificateModel,
			), true);
			$this->sendJSON(array(
				'html' => $content,
			));
		}
		Yii::app()->end();
	}

	public function actionUpdate($id) {
		$certificateModel = LearningCertificate::model()->findByPk($id);
		if (empty($certificateModel)) {
			$this->sendJSON(array());
		}
        $certificateModel->setScenario('update');
		if (!empty($_POST['LearningCertificate'])) {
			$certificateModel->attributes = $_POST['LearningCertificate'];
			if ($certificateModel->validate()) {
				$certificateModel->save();
				$this->sendJSON(array());
			}
		}
		if (Yii::app()->request->isAjaxRequest) {
			$content           = $this->renderPartial('_form', array(
				'model' => $certificateModel,
			), true);
			$this->sendJSON(array(
				'html' => $content,
			));
		}
		Yii::app()->end();
	}

	public function actionDelete($id) {
		$certificateModel = LearningCertificate::model()->findByPk($id);
		if (empty($certificateModel)) {
			$this->sendJSON(array());
		}

		if (!empty($_POST['LearningCertificate'])) {
			if ($_POST['LearningCertificate']['confirm']) {
				$certificateModel->delete();
				$this->sendJSON(array());
			}
		}
		if (Yii::app()->request->isAjaxRequest) {
			$content = $this->renderPartial('delete', array(
				'model' => $certificateModel,
			), true);
			$this->sendJSON(array(
				'html' => $content,
			));
		}
		Yii:app()->end();
	}

	public function actionReleased($id) {
		if (Yii::app()->request->isAjaxRequest) {

            $params = array();
            $params['certificateId'] = $id;

			$coursesWithThisCertificateAssigned = array();

			$q = Yii::app()->getDb()->createCommand()
				->select('id_course, course.name, course.code')
				->from(LearningCertificateCourse::model()->tableName().' t')
				->join(LearningCourse::model()->tableName().' course', 't.id_course=course.idCourse')
				->where('t.id_certificate=:id', array(':id'=>$id))
				->group('t.id_course')
				->order('course.name, course.code');

			if(Yii::app()->user->getIsPu()) {
				// Current user is Power User. Limit the results to only courses assigned to him
				$puCoursesIds = CoreUserPuCourse::model()->getList(Yii::app()->user->getId());
				$q->andWhere(array('IN', 't.id_course', $puCoursesIds));
			}
			$query = $q->queryAll();
			foreach($query as $row){
				$coursesWithThisCertificateAssigned[$row['id_course']] = ($row['code'] !== '' ? $row['code'].' - ' : '').$row['name'];
			}

            $params['certificateCourseList'] = $coursesWithThisCertificateAssigned;

            $isCurriculaPluginActive = PluginManager::isPluginActive('CurriculaApp');
            $params['isCurriculaPluginActive'] = $isCurriculaPluginActive;
			$params['id_path'] = 0;

            if ($isCurriculaPluginActive) {
				$q = Yii::app()->getDb()->createCommand()
					->select('t.id_path, cp.path_name, cp.path_code')
					->from(LearningCertificateCoursepath::model()->tableName().' t')
					->join(LearningCoursepath::model()->tableName().' cp', 't.id_path=cp.id_path')
					->where('t.id_certificate=:id', array(':id'=>$id))
					->group('t.id_path')
					->order('cp.path_name, cp.path_code');

				if(Yii::app()->user->getIsPu()){
					// Current user is Power User. Limit the results to only learning plans assigned to him
					$currentPuCoursepaths = CoreUserPuCoursepath::model()->getList(Yii::app()->user->getIdst());
					$q->andWhere(array('IN', 't.id_path', $currentPuCoursepaths));
				}

				$certificateAssignList = array();

				$query = $q->queryAll();
				foreach($query as $row){
					$certificateAssignList[$row['id_path']] = ($row['path_name'] !== '' ? $row['path_name'].' - ' : '').$row['path_name'];
				}

                $params['certificateAssignList'] = $certificateAssignList;

                $learningCoursepathUser = new LearningCoursepathUser();
                $learningCoursepathUser->user = new CoreUser();
                $learningCoursepathUser->learningCertificateAssignCp = new LearningCertificateAssignCp();
                $learningCoursepathUser->learningCertificateCoursepath = new LearningCertificateCoursepath();

                $learningCoursepathUser->learningCertificateAssignCp->id_certificate = $id;
                $learningCoursepathUser->learningCertificateCoursepath->id_certificate = $id;

                $params['id_path'] = $learningCoursepathUser->id_path = (!empty($_REQUEST['LearningCoursepathUser']['id_path'])) ? $_REQUEST['LearningCoursepathUser']['id_path'] : 0;
                if (isset($_REQUEST['CoreUser'])) {
                    $learningCoursepathUser->user->attributes = $_REQUEST['CoreUser'];
                }

                $params['learningCoursepathUser'] = $learningCoursepathUser;

                if ($learningCoursepathUser->id_path) {
                    $params['dataProvider'] = $learningCoursepathUser->dataProviderCertificate();
                }
            }

			$learningCourseUser = new LearningCourseuser();
			$learningCourseUser->user = new CoreUser();
			$learningCourseUser->learningCertificateAssign = new LearningCertificateAssign();
			$learningCourseUser->learningCertificateCourse = new LearningCertificateCourse();

			$learningCourseUser->learningCertificateAssign->id_certificate = $id;
			$learningCourseUser->learningCertificateCourse->id_certificate = $id;

			$params['idCourse'] = $learningCourseUser->idCourse = (!empty($_REQUEST['LearningCourseuser']['idCourse'])) ? $_REQUEST['LearningCourseuser']['idCourse'] : 0;
			if (isset($_REQUEST['CoreUser'])) {
				$learningCourseUser->user->attributes = $_REQUEST['CoreUser'];
			}
            $params['learningCourseUser'] = $learningCourseUser;

            if (!isset($params['dataProvider'])) {
                $params['dataProvider'] = $learningCourseUser->dataProviderCertificate();
            }

			$params['canModCertificates'] = Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/lms/admin/certificate/mod');

			Yii::app()->event->raise('onBeforeIssuedCertificatesRender', new DEvent($this, array(
				'params'=>&$params,
			)));

			$content = $this->renderPartial('_release', $params, true, true);
			if ($_REQUEST['contentType'] == 'html') {
				echo $content;
			} else {
				$this->sendJSON(array('html' => $content));
			}
		}
		Yii::app()->end();
	}

	public function actionTemplate($id) {
		Yii::app()->tinymce;

		$certificate = LearningCertificate::model()->findByPk($id);

		// Keep the old image
		$oldBgImage = $certificate->bgimage;

		if (empty($certificate)) {
			$this->sendJSON(array());
		}

		if (!empty($_POST['LearningCertificate'])) {

			// Handle background image, if any; Save it as an Asset
			$uploadedFile = CUploadedFile::getInstanceByName("LearningCertificate[background_image]");
			$asset = null;
			if ($uploadedFile) {
				$asset = new CoreAsset();
				$asset->type = CoreAsset::TYPE_GENERAL_IMAGE;
				$asset->sourceFile = $uploadedFile;
				$asset->save();
			}

			$certificate->scenario = 'template';
			$certificate->attributes = $_POST['LearningCertificate'];
			if ($certificate->validate()) {
				// Save ID of the Asset !! Preserve old one if no file has been uploaded
				$certificate->bgimage = $uploadedFile ? ($asset ? $asset->id : $oldBgImage) : $oldBgImage;
				if ($certificate->bgimage != $oldBgImage) {
					$asset = CoreAsset::model()->findByPk($oldBgImage);
					if ($asset) {
						$asset->delete();
					}
				}
				$certificate->save(false);
				$this->redirect(array('certificateManagement/index'));
			}

		}
		$tags = Yii::app()->certificateTag->getSubstitutionTags();
		Yii::app()->event->raise('GetAdditionalTags', new DEvent($this, array('tags' => &$tags, 'id' => $certificate->id_certificate)));
		if($certificate->base_language == '')
			$certificate->base_language = CoreLangLanguage::getDefaultLanguage();

		$this->render('template', array(
			'model' => $certificate,
			'tags' => $tags,
		));
	}

	public function actionDownload($id, $user_id, $course_id = null, $path_id = null) {
		if($course_id){
			// Requested to download a course certificate
			$certificate = LearningCertificateAssign::model()->findByAttributes(array(
				'id_certificate' => $id,
				'id_user' => $user_id,
				'id_course' => $course_id,
			));
		}elseif($path_id){
			// Requested to download a Learning Plan certificate
			$certificate = LearningCertificateAssignCp::model()->findByAttributes(array(
				'id_certificate' => $id,
				'id_user' => $user_id,
				'id_path' => $path_id,
			));
		}else{
			throw new CException('Neither Course or Learning Plan ID provided. Which certificate to download?');
		}
		if (empty($certificate)) {
			header('Location: ' . $_SERVER['HTTP_REFERER']);
			exit;
		}
		$certificate->downloadCertificate();
		Yii::app()->end();
	}

	public function actionDownloadTemplate($id) {
		$certificate = LearningCertificate::model()->findByPk($id);
		if (!empty($certificate)) {
			$bgImage = $certificate->getBgImageUrl(CoreAsset::VARIANT_ORIGINAL, true);

			$event = new DEvent($this, array(
				'certificateModel'=>$certificate
			));
			Yii::app()->event->raise('getCertificatePaperSize', $event);

			// Plugins may force the generated PDF to be of different paper size, e.g. "USLETTER" or "USLEGAL"
			// The default is A4
			$customFormat = $event->return_value ? $event->return_value : "A4";

			Yii::app()->certificate->getPdf($certificate->cert_structure , $certificate->name, $bgImage, $certificate->orientation, true, false, false, $customFormat, $certificate->base_language);
		}
		Yii::app()->end();
	}

	public function actionGenerateCertificate($id, $course_id = null, $plan_id = null, $user_ids = null) {
		if (Yii::app()->request->isAjaxRequest) {
			if (!empty($_POST['user_ids'])) {
				$user_ids = $_POST['user_ids'];
			}

			if(!$course_id && !$plan_id){
				throw new CException('Course ID or Learning Plan ID is required for certificate creation');
			}

			$results = array();

			if($course_id){
				// Requested to generate Course certificates
				$oldCertificates = LearningCertificateAssign::model()->findAllByAttributes(array(
					'id_certificate' => $id,
					'id_user' => $user_ids,
					'id_course' => $course_id,
				));
				if (!is_array($user_ids)) {
					$user_ids = array($user_ids);
				}

				if (!empty($oldCertificates)) {
					$oldCertificatesList = CHtml::listData($oldCertificates, 'id_user', 'id_user');
					$user_ids = array_diff($user_ids, $oldCertificatesList);
				}

				if (!empty($user_ids)) {
					foreach ($user_ids as $user_id) {
						$courseUser = LearningCourseuser::model()->with(array('learningCertificateCourse', 'course', 'user'))->findByAttributes(array('idUser' => $user_id, 'idCourse' => $course_id));
						if ($courseUser->getCertificateUserLevel()) {

							$event = new DEvent($this, array('course'=>$courseUser->course, 'user'=>$courseUser->user));
							Yii::app()->event->raise('GenerateCertificate', $event);

							if(!$event->handled) {
								$substituton = Yii::app()->certificateTag->getSubstitution($user_id, $course_id, false, true);
								Yii::app()->certificate->create($id, $user_id, $course_id, $substituton, FALSE);
							}
						}
					}
				}
			}elseif($plan_id){
				// Requested to generate Learning Plan certificates
				$oldCertificates = LearningCertificateAssignCp::model()->findAllByAttributes(array(
					'id_certificate' => $id,
					'id_user' => $user_ids,
					'id_path' => $plan_id,
				));
				if (!is_array($user_ids)) {
					$user_ids = array($user_ids);
				}

				if (!empty($oldCertificates)) {
					$oldCertificatesList = CHtml::listData($oldCertificates, 'id_user', 'id_user');
					$user_ids = array_diff($user_ids, $oldCertificatesList);
				}

				if (!empty($user_ids)) {
					foreach ($user_ids as $user_id) {
						// Is the certificate generated?
						$generated = LearningCertificateAssignCp::model()->findByAttributes(array(
							'id_user' => $user_id,
							'id_path' => $plan_id,
							'id_certificate' => $id,
						));
						if(!$generated && LearningCertificateAssignCp::userCompletedLearningPlan($user_id, $plan_id)){
							// The certificate for that user has not been generated yet
							// and he has completed the Learning Plan, so generate one now

							try {
								$substitution = Yii::app()->certificateTag->getSubstitution($user_id, $plan_id, TRUE);
								Yii::app()->certificate->createForLearningPlan($id, $user_id, $plan_id, $substitution, FALSE);
							} catch(Exception $e){
								Yii::log('Can not generate certificate for learning plan. LearningCertificateAssignCp::loadByUserPath', CLogger::LEVEL_ERROR);
							}
						}
						elseif(!LearningCertificateAssignCp::userCompletedLearningPlan($user_id, $plan_id))
						{
							$user = CoreUser::model()->findByPk($user_id);

							if(!isset($results['messages']))
								$results['messages'] = '';

							if($results['messages'] !== '')
								$results['messages'] .= "<br />";

							$results['messages'] .= addslashes(Yii::t('certificate', 'The user [user] has not yet finished the learning plan', array('[user]' => $user->getFullName())));
						}
					}
				}
			}

			$this->sendJSON($results);
		}
		Yii::app()->end();
	}

	public function actionRemoveCertificate($id, $course_id = null, $plan_id = null, $user_ids = null) {
		if (Yii::app()->request->isAjaxRequest) {
			if (!empty($_POST['user_ids'])) {
				$user_ids = $_POST['user_ids'];
			}

			if($course_id){
				$existCertificates = LearningCertificateAssign::model()->findAllByAttributes(array(
					'id_certificate' => $id,
					'id_user' => $user_ids,
					'id_course' => $course_id,
				));
			}elseif($plan_id){
				$existCertificates = LearningCertificateAssignCp::model()->findAllByAttributes(array(
					'id_certificate' => $id,
					'id_user' => $user_ids,
					'id_path' => $plan_id,
				));
			}else{
				throw new CException('No course or learning plan provided. Nothing to remove');
			}

			if (!empty($existCertificates)) {
				foreach ($existCertificates as $certificate) {
					$certificate->delete();
				}
			}
			$this->sendJSON(array());
		}
		Yii::app()->end();
	}

	public function actionDeleteImage($id)
	{
		$ajax = new AjaxResult();
		$status = false;
		$msg = Yii::t('standard', '_OPERATION_FAILURE');
		$model = LearningCertificate::model()->findByPk($id);
		if ($model)
		{
			if ($model->deleteImage())
			{
				$status = true;
				$msg = Yii::t('standard', '_OPERATION_SUCCESSFUL');
			}
		}

		$ajax->setStatus($status)->setMessage($msg)->toJSON();
	}

	protected function gridRenderDownloadTemplateButton($data, $index) {
		return CHtml::link('', array('certificateManagement/downloadTemplate', 'id' => $data->id_certificate), array(
			'class' => 'ajaxLink1 view-action',
			'title'=>Yii::t('certificate', 'Download certificate template'),
			'rel'=>'tooltip'
		));
	}

	protected function gridRenderTemplateButton($data, $index) {
		return CHtml::link('', array('certificateManagement/template', 'id' => $data->id_certificate), array(
			'class' => 'template-action',
			'title' => Yii::t('certificate', '_STRUCTURE_CERTIFICATE'),
			'rel'=>'tooltip'
		));
	}

	protected function gridRenderReleasedButton($data, $index) {
		$buttons = array();
		if(Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/lms/admin/certificate/mod'))
			$buttons[] = array('type' => 'submit', 'title' => Yii::t('standard', '_CONFIRM'));

		$buttons[] = array('type' => 'cancel', 'title' => Yii::t('standard', '_CANCEL'));

		return CHtml::link('', '', array(
			'class' => 'ajaxModal release-action',
			'data-toggle' => 'modal',
			'data-modal-class' => 'release-node',
			'data-modal-title' => Yii::t('menu', '_REPORT_CERTIFICATE'),
			'data-buttons' => json_encode($buttons),
			'data-url' => 'certificateManagement/released&id='.CHtml::encode($data->id_certificate),
			'data-after-loading-content' => 'function(){ replacePlaceholder(); applyTypeahead($(".typeahead")); }',
			'data-after-submit' => 'updateCertificateContent',
			'title' => Yii::t('menu', '_REPORT_CERTIFICATE'),
			'rel' => 'tooltip'
		));
	}

	protected function gridRenderUpdateButton($data, $index) {
		return CHtml::link('', '', array(
			'class' => 'ajaxModal edit-action',
			'data-toggle' => 'modal',
			'data-modal-class' => 'edit-certificate',
			'data-modal-title' => Yii::t('standard', '_MOD'),
			'data-buttons' => '[
				{"type": "submit", "title": "' . Yii::t('standard', '_CONFIRM') . '"},
				{"type": "cancel", "title": "' . Yii::t('standard', '_CANCEL') . '"}
			]',
			'data-url' => 'certificateManagement/update&id=' . CHtml::encode($data->id_certificate),
			'data-after-loading-content' => 'function(){ initTinyMCE("#" + $(".modal.in").find("textarea").attr("id"), 150); }',
			"data-before-submit" => "beforeCertificateSubmit",
			'data-after-submit' => 'updateCertificateContent',
			'title' => Yii::t('standard', '_MOD'),
			'rel' => 'tooltip'
		));
	}

	protected function gridRenderDeleteButton($data, $index) {
		return CHtml::link('', '', array(
			'class' => 'ajaxModal delete-action',
			'data-toggle' => 'modal',
			'data-modal-class' => 'delete-node',
			'data-modal-title' => Yii::t('standard', '_DEL'),
			'data-buttons' => '[
				{"type": "submit", "title": "' . Yii::t('standard', '_CONFIRM') . '"},
				{"type": "cancel", "title": "' . Yii::t('standard', '_CANCEL') . '"}
			]',
			'data-url' => 'certificateManagement/delete&id=' . CHtml::encode($data->id_certificate),
			'data-after-loading-content' => 'hideConfirmButton();',
			'data-after-submit' => 'updateCertificateContent',
			'title' => Yii::t('standard', '_DEL'),
			'rel' => 'tooltip'
		));
	}

}
