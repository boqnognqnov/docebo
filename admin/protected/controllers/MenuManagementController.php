<?php

class MenuManagementController extends AdminController {

	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}

	// TODO: change access rules
	public function accessRules() {
		$res = array();

		$res[] = array(
			'allow',
			'roles' => array('/lms/admin/middlearea/view'),
			'actions' => array(
				'index',
				'createMenuItem',
				'updateMenuItem',
				'changeMenuItemStatus',
				'changeMenuItemPosition',
				'deleteMenuItem',
				'filterMenuItem',
				'changeDefaultMenuItem',
			),
		);

		$res[] = array(
			'deny',
			// deny all
			'users' => array('*'),
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);

		return $res;
	}

	public function actionIndex()
	{
		$model = new CoreMenuItem();
		if (Yii::app()->request->isAjaxRequest) {
			$html = $this->renderPartial('index', array('model' => $model), true, true);
			if (isset($_GET['ajax'])){
				echo $html;
			}else{
				$this->sendJSON(array(
					'success' => true,
					'html' => $html,
				));
			}
		} else {
			Yii::app()->fontawesome->loadIconPicker();

			// Register selector resources
			UsersSelector::registerClientScripts();
			$this->render('index', array('model' => $model));
		}
	}

	public function actionCreateMenuItem()
	{
		$model = new CoreMenuItem();
		// Set lang_code to be the default language of the LMS
		$model->lang_code = Settings::get('default_language');
		$activeLanguagesList = CoreLangLanguage::getActiveLanguages();
		$lang_code = Settings::get('default_language', 'english');
		$activeLanguagesList[$lang_code] = $activeLanguagesList[$lang_code] . " (" . Yii::t('standard', '_DEFAULT_LANGUAGE') . ")";
		foreach($activeLanguagesList as $code => $lang){
			$translations[$code] = '';
		}

		if(isset($_POST['CoreMenuItem'])) {
			$root = CoreMenuItem::getMainItemByTitle('Menu');
			$model->idParent = $root->id;
			$model->is_custom = 1;
			if($_POST['CoreMenuItem']['salt_secret'] != $_POST['CoreMenuItem']['repeatSaltSecret']) {
				$model->addError('repeatSaltSecret', Yii::t('thomsonreuters', 'The repeated salt is wrong'));
			}
			$model->attributes = $_POST['CoreMenuItem'];
			if($model->useSecretKey)
				$model->setScenario('useSecret');

			if($model->useAccredIframe)
				$model->setScenario('useAccred');

			//Lms default language code translation is required
			if(!$_POST['CoreMenuTranslation'][$model->lang_code])
				$model->addError('lang_code', Yii::t('user_management', '_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED'));

			if(!$model->hasErrors() && $model->validate()){
				// Set menu Additional options as target type,secret key etc
				$model->setMenuAdditionalOptions();
				if($model->appendTo($root)){
					// If everything is ok we can add the translations from the user input for this menu item
					foreach($_POST['CoreMenuTranslation'] as $lang_code=>$label)
					{
						$trans = new CoreMenuTranslation();
						$trans->idMenu = $model->id;
						$trans->lang_code = $lang_code;
						$trans->label = $label;
						$trans->save();
					}

					echo '<a class="auto-close"></a>';
				}
			}
		}

		// Get List of OAuth Clients
		$clientsList = array();
		foreach(OauthClients::model()->findAllByAttributes(array('enabled' => 1, 'hidden' => 0)) as $client) {
			$clientsList[$client['client_id']] = $client['app_name'];
		}

		echo $this->renderPartial('_menuItemForm', array(
			'model' 				=> $model,
			'translations' 			=> $translations,
			'activeLanguagesList' 	=> $activeLanguagesList,
			'clientsList'			=> $clientsList,
		),true, true);
	}

	public function actionUpdateMenuItem($id)
	{
		$model = CoreMenuItem::model()->findByPk($id);
		$activeLanguagesList = CoreLangLanguage::getActiveLanguages();
		$lang_code = Settings::get('default_language', 'english');
		$activeLanguagesList[$lang_code] = $activeLanguagesList[$lang_code] . " (" . Yii::t('standard', '_DEFAULT_LANGUAGE') . ")";
		$translations = array();
		foreach($model->translation as $lang)
			$translations[$lang->lang_code] = $lang->label;

		if(isset($_POST['CoreMenuItem']))
		{
		    
			
			$model->attributes = $_POST['CoreMenuItem'];

			if($model->useSecretKey)
				$model->setScenario('useSecret');

			if($model->useAccredIframe) {
				$model->setScenario('useAccred');
				if ($model->salt_secret !== $model->repeatSaltSecret) {
				    $model->addError('repeatSaltSecret', Yii::t('thomsonreuters', 'The repeated salt is wrong'));
				}
			}
				

			if(!$_POST['CoreMenuTranslation'][$model->lang_code])
				$model->addError('lang_code', Yii::t('user_management', '_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED'));

			if(!$model->hasErrors() && $model->validate())
			{
				$model->setMenuAdditionalOptions();
				$model->saveNode();
				foreach($model->translation as $trans){
					if(isset($_POST['CoreMenuTranslation'][$trans->lang_code]))
					{
						$trans->label = $_POST['CoreMenuTranslation'][$trans->lang_code];
						$trans->save();
					}
				}
				echo '<a class="auto-close"></a>';
			}
		}

		// Get List of OAuth Clients
		$clientsList = array();
		foreach(OauthClients::model()->findAllByAttributes(array('enabled' => 1, 'hidden' => 0)) as $client) {
			$clientsList[$client['client_id']] = $client['app_name'];
		}

		echo $this->renderPartial('_menuItemForm', array(
			'model' 				=> $model,
			'translations' 	=> $translations,
			'activeLanguagesList' => $activeLanguagesList,
			'clientsList'			=> $clientsList,
		),true, true);
	}

	public function actionChangeMenuItemStatus()
	{
		$id = Yii::app()->request->getParam('menuItem', null);
		Yii::app()->db->createCommand('UPDATE core_menu_item SET visible = NOT visible WHERE id = :id')->execute(array( ':id' => $id ));
	}

	public function actionChangeMenuItemPosition()
	{
		if(isset($_POST['sorted']))
		{
			foreach($_POST['sorted'] as $key => $id)
			{
				$model = CoreMenuItem::model()->findByPk($id);
				if($key == 0) continue;
				$prevId = isset($_POST['sorted'][$key - 1]) ? $_POST['sorted'][$key - 1] : null;
				$prevModel = CoreMenuItem::model()->findByPk($prevId);
				$model->moveAfter($prevModel);
			}
			$response = new AjaxResult();
			$response->setStatus(false);
			$response->toJSON();
			Yii::app()->end();
		}
	}

	public function actionDeleteMenuItem()
	{
		try {
			$id = Yii::app()->request->getParam('id', false);
			$model = CoreMenuItem::model()->findByPk($id);

			if ($model == null) {
				throw new Exception('Invalid layout, can not delete');
			}

			if (isset($_REQUEST['confirm'])) {
				if (isset($_REQUEST['agree_delete']) && $_REQUEST['agree_delete'] == 'agree') {
					$model->deleteNode();
				}
				echo '<a class="auto-close success"></a>';
				Yii::app()->end();
			}

			$this->renderPartial('_delete_layout', array(
				'model'	=> $model,
			));
			Yii::app()->end();
		}
		catch (Exception $e) {
			$this->renderPartial('//common/_dialog2_error', array('type' => 'error',  'message' => $e->getMessage()));
			Yii::app()->end();
		}
	}

	public function actionFilterMenuItem()
	{
		$idMenu = Yii::app()->request->getParam('idMenu',null);
		$ufilterOption 	= Yii::app()->request->getParam('ufilter_option', false);
		$groupList = array();
		$branchesList = array();

		try {

			$model = CoreMenuItem::model()->findByPk($idMenu);
			if ($model === null)
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));

			if($ufilterOption == DashboardLayout::LAYOUT_OPTION_ALL)
				$model->clearFilters();
			else{
				$groupList = UsersSelector::getGroupsListOnly($_REQUEST);
				$branchesList = UsersSelector::getBranchesListOnly($_REQUEST);
				$model->assignGroups($groupList);
				$model->assignBranches($branchesList);
			}

			echo "<a class='auto-close success'></a>";
			Yii::app()->end();
		}
		catch (CException $e) {
			Yii::log($e->getMessage(),CLogger::LEVEL_ERROR);
			Yii::app()->user->setFlash('error', $e->getMessage());
			$selectorDialogUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
				'type' 	=> UsersSelector::TYPE_FILTER_MENU_ITEMS_FOR_USERS,
				'idMenu'	=> $idMenu,
			));
			$this->redirect($selectorDialogUrl, true);
		}

		echo "<a class='auto-close success'></a>";
		Yii::app()->end();
	}

	public function actionChangeDefaultMenuItem()
	{
		$id = Yii::app()->request->getParam('id', false);
		$response = new AjaxResult();
		$response->setStatus(true);
		$model = CoreMenuItem::model()->findByPk($id);
		if (!$model) {
			$response->toJSON();
			Yii::app()->end();
		}
		CoreMenuItem::model()->updateAll(array('default' => 0));
		$model->default = 1;
		$model->saveNode();
		$response->toJSON();
	}

	public function actionDefaultPage()
	{
		echo 'shiiit';
	}

}