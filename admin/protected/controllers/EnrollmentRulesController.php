<?php
class EnrollmentRulesController extends AdminController
{

	public $layout = '//layouts/adm';


	public function filters()
	{
		return array(
			'accessControl' => 'accessControl',
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules()
	{
		$res = array();

		$res[] = array(
			'allow',
			'expression' => 'Yii::app()->user->isGodAdmin',
		);

		$res[] = array(
			'deny',
			// deny all
			'users' => array('*'),
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);

		return $res;
	}


	/**
	 * (non-PHPdoc)
	 * @see Controller::init()
	 */
	public function init()
	{
		$cs = Yii::app()->getClientScript();

		DatePickerHelper::registerAssets();

		// Register JS files
		$cs->registerScriptFile(Yii::app()->baseUrl . '/js/enrollmentRules.js', CClientScript::POS_HEAD);

		// Register JS script(s)
		$options = array();
		$options = CJavaScript::encode($options);
		$script = "var EnrollmentRule = new EnrollmentRules($options);";
		$cs->registerScript("enrollmentRulesJS1", $script, CClientScript::POS_HEAD);

		return parent::init();
	}


	/**
	 *
	 */
	public function actionIndex()
	{
        // FancyTree initialization
        Yii::app()->fancytree->init();

		$model = new CoreEnrollRule('search');

		// Search might coming?
		if ($data = Yii::app()->request->getParam(get_class($model), false)) {
			$model->attributes = $data;
		}

		UsersSelector::registerClientScripts();

		$this->render('index', array(
			'model' => $model,
		));
	}

	public function actionCreateRule()
	{
		$id = Yii::app()->request->getParam("id", false);
		if ($id)
			$model = CoreEnrollRule::model()->findByPk($id);
		else
			$model = new CoreEnrollRule();

		$model->scenario = 'create';

		if (isset($_POST['CoreEnrollRule'])) {
			$model->attributes = $_POST['CoreEnrollRule'];
			if ($model->validate()) {
				$model->rule_type = $model->generateRuleType();
				$model->save(false);

				/*
				// NO MORE Wizard mode. Just create the Rule and later assign items through the existing UI
				//Go to step 2 (group OR branch)
				if ($model->subtype_first == 'group')
					$url = $this->createUrl('enrollmentRules/assignGroup', array('id' => $model->rule_id));
				else
					$url = $this->createUrl('enrollmentRules/assignBranch', array('id' => $model->rule_id));

				echo '<a href="'.$url.'" class="ajax navigate-to"></a>';
				*/

				echo '<a class="auto-close success"></a>';
				Yii::app()->end();
			}
		}
		$html = $this->renderPartial('_form_create_rule', array(
			'model' => $model,
		), true);

		echo $html;
	}

	public function actionUpdateRule($id)
	{
		$model = CoreEnrollRule::model()->findByPk($id);
		$model->scenario = 'update';

		if (isset($_POST['CoreEnrollRule'])) {
			$model->attributes = $_POST['CoreEnrollRule'];
			if ($model->validate()) {
				$model->save(false);

				echo '<a class="auto-close"></a>';
				Yii::app()->end();
			}
		}
		$html = $this->renderPartial('_form_update_rule', array(
			'model' => $model,
		), true);

		echo $html;
	}

	/**
	 * Step2 - group
	 * @param $id
	 */
	public function actionAssignGroup($id)
	{
		$singleStep = Yii::app()->request->getParam("singleStep", false);
		$ruleModel = CoreEnrollRule::model()->findByPk($id);
		$selected = array();


		if (isset($_POST['group-grid-selected-items-list'])) {
			CoreEnrollRuleItem::removeAllByTypeAndRule(CoreEnrollRuleItem::ITEM_TYPE_GROUP, $ruleModel->rule_id);
			$items = explode(',', $_POST['group-grid-selected-items-list']);
			foreach ($items as $item)
			{
				$model = new CoreEnrollRuleItem();
				$model->item_type = CoreEnrollRuleItem::ITEM_TYPE_GROUP;
				$model->rule_id = $ruleModel->rule_id;
				$model->item_id = $item;
				$model->save(false);
			}
			if ($singleStep)
				echo '<a class="auto-close"></a>';
			else
			{
				if ($ruleModel->rule_type == CoreEnrollRule::RULE_GROUP_COURSE || $ruleModel->rule_type == CoreEnrollRule::RULE_BRANCH_COURSE)
					$url = $this->createUrl('enrollmentRules/assignCourse', array('id' => $ruleModel->rule_id));
				else
					$url = $this->createUrl('enrollmentRules/assignCurricula', array('id' => $ruleModel->rule_id));
				echo '<a href="'.$url.'" class="ajax navigate-to"></a>';
			}
			Yii::app()->end();
		}
		else
		{
			if (isset($_GET['selected_items']))
				$selected = explode(",", $_GET['selected_items']);
			else
			{
				foreach ($ruleModel->groupItems as $item)
					$selected[] = $item->item_id;
			}
		}
		$cs=Yii::app()->clientScript;
		$cs->scriptMap=array( //always remove these because they are already added since a grid is used on the non-ajax page
			'jquery.js'=>false,
			'jquery.min.js'=>false,
			'jquery.yiigridview.js'=>false,
			'jquery.ba-bbq.js'=>false,
		);

		$processOutput = (isset($_GET['ajax'])) ? false : true;
		Yii::app()->session['selectedItems'] = $selected;

		echo $this->renderPartial('_form_assign_group', array(
			'ruleModel' => $ruleModel,
			'singleStep' => $singleStep,
		), true, $processOutput);
	}

	/**
	 * Step2 - Branch
	 * @param $id
	 */
	public function actionAssignBranch($id)
	{
		$singleStep = Yii::app()->request->getParam("singleStep", false);
		$ruleModel = CoreEnrollRule::model()->findByPk($id);

		if (isset($_POST['submit'])) {
			CoreEnrollRuleItem::removeAllByTypeAndRule(CoreEnrollRuleItem::ITEM_TYPE_BRANCH, $ruleModel->rule_id);
			if (isset($_POST['chartGroups']))
			{
				foreach ($_POST['chartGroups'] as $item)
				{
					$model = new CoreEnrollRuleItem();
					$model->item_type = CoreEnrollRuleItem::ITEM_TYPE_BRANCH;
					$model->rule_id = $ruleModel->rule_id;
					$model->item_id = $item;
					$model->save(false);
				}
			}
			if ($singleStep)
				echo '<a class="auto-close"></a>';
			else
			{
				if ($ruleModel->rule_type == CoreEnrollRule::RULE_GROUP_COURSE || $ruleModel->rule_type == CoreEnrollRule::RULE_BRANCH_COURSE)
					$url = $this->createUrl('enrollmentRules/assignCourse', array('id' => $ruleModel->rule_id));
				else
					$url = $this->createUrl('enrollmentRules/assignCurricula', array('id' => $ruleModel->rule_id));
				echo '<a href="'.$url.'" class="ajax navigate-to"></a>';
			}
			Yii::app()->end();
		}

		$selected = array();
		foreach ($ruleModel->branchItems as $item)
			$selected[] = $item->item_id;

		$orgChartsInfo = CoreOrgChartTree::getFullOrgChartTree();
		echo $this->renderPartial('_form_assign_branch', array(
			'ruleModel' => $ruleModel,
			'singleStep' => $singleStep,
			'selected' => $selected,
			'fullOrgChartTree' 	=> $orgChartsInfo['list'],
			'puLeafs'			=> $orgChartsInfo['leafs'],

		), true);
	}

	/**
	 * Step3 - Course
	 * @param $id
	 */
	public function actionAssignCourse($id)
	{
		$singleStep = Yii::app()->request->getParam("singleStep", false);
		$ruleModel = CoreEnrollRule::model()->findByPk($id);

		if (isset($_POST['course-grid-selected-items-list'])) {
			$items = explode(',', $_POST['course-grid-selected-items-list']);
			CoreEnrollRuleItem::removeAllByTypeAndRule(CoreEnrollRuleItem::ITEM_TYPE_COURSE, $ruleModel->rule_id);
			foreach ($items as $item)
			{
				$model = new CoreEnrollRuleItem();
				$model->item_type = CoreEnrollRuleItem::ITEM_TYPE_COURSE;
				$model->rule_id = $ruleModel->rule_id;
				$model->item_id = $item;
				$model->save(false);
			}
			//activate rule on last step
			$ruleModel->active = 1;
			$ruleModel->save(false);
			echo '<a class="auto-close"></a>';
			Yii::app()->end();
		}
		else
		{
			if (isset($_GET['selected_items']))
				$selected = explode(",", $_GET['selected_items']);
			else
			{
				foreach ($ruleModel->courseItems as $item)
					$selected[] = $item->item_id;
			}
		}

		$cs=Yii::app()->clientScript;
		$cs->scriptMap=array( //always remove these because they are already added since a grid is used on the non-ajax page
			'jquery.js'=>false,
			'jquery.min.js'=>false,
			'jquery.yiigridview.js'=>false,
			'jquery.ba-bbq.js'=>false,
		);

		$processOutput = (isset($_GET['ajax'])) ? false : true;
		Yii::app()->session['selectedItems'] = $selected;

		echo $this->renderPartial('_form_assign_course', array(
			'ruleModel' => $ruleModel,
			'singleStep' => $singleStep,
		), true, $processOutput);
	}

	/**
	 * Step3 - Curricula
	 * @param $id
	 */
	public function actionAssignCurricula($id)
	{
		$singleStep = Yii::app()->request->getParam("singleStep", false);
		$ruleModel = CoreEnrollRule::model()->findByPk($id);
		if (isset($_POST['curricula-grid-selected-items-list'])) {
			CoreEnrollRuleItem::removeAllByTypeAndRule(CoreEnrollRuleItem::ITEM_TYPE_CURRICULA, $ruleModel->rule_id);
			if(!empty($_POST['curricula-grid-selected-items-list'])) {
				$items = explode(',', $_POST['curricula-grid-selected-items-list']);
				foreach ($items as $item)
				{
					$model = new CoreEnrollRuleItem();
					$model->item_type = CoreEnrollRuleItem::ITEM_TYPE_CURRICULA;
					$model->rule_id = $ruleModel->rule_id;
					$model->item_id = $item;
					$model->save(false);
				}
			}
			//activate rule on last step
			$ruleModel->active = 1;
			$ruleModel->save(false);
			echo '<a class="auto-close"></a>';
			Yii::app()->end();
		}
		else
		{
			if (isset($_GET['selected_items']))
				$selected = explode(",", $_GET['selected_items']);
			else
			{
				foreach ($ruleModel->curriculaItems as $item)
					$selected[] = $item->item_id;
			}
		}

		$cs=Yii::app()->clientScript;
		$cs->scriptMap=array( //always remove these because they are already added since a grid is used on the non-ajax page
			'jquery.js'=>false,
			'jquery.min.js'=>false,
			'jquery.yiigridview.js'=>false,
			'jquery.ba-bbq.js'=>false,
		);

		$processOutput = (isset($_GET['ajax'])) ? false : true;
		Yii::app()->session['selectedItems'] = $selected;

		echo $this->renderPartial('_form_assign_curricula', array(
			'ruleModel' => $ruleModel,
			'singleStep' => $singleStep,
		), true, $processOutput);
	}

	public function actionToggleActive($id)
	{
		$model = CoreEnrollRule::model()->findByPk($id);
		$model->active = $model->active ? 0 : 1;
		$model->save(false);
	}

	public function actionDeleteRule($id)
	{
		$model = CoreEnrollRule::model()->findByPk($id);

		if (isset($_POST['delete'])) {
			$model->delete();
			echo '<a class="auto-close"></a>';
			Yii::app()->end();
		}
		$html = $this->renderPartial('_form_delete_rule', array(
			'model' => $model,
		), true);

		echo $html;
	}

	public function actionLog()
	{
		$model = new CoreEnrollLog('search');
		$this->render('log', array(
			'model' => $model,
		));
	}

	public function actionRollbackLog($id)
	{
		$model = CoreEnrollLog::model()->findByPk($id);

		if (isset($_POST['rollback'])) {
			$model->rollback();
			echo '<a class="auto-close"></a>';
			Yii::app()->end();
		}
		$html = $this->renderPartial('_form_rollback_log', array(
			'model' => $model,
		), true);

		echo $html;
	}

	public function actionRollbackLogItem($id)
	{
		$model = CoreEnrollLogItem::model()->findByPk($id);

		if (isset($_POST['rollback'])) {
			$model->rollback();
			echo '<a class="auto-close"></a>';
			Yii::app()->end();
		}
		$html = $this->renderPartial('_form_rollback_log', array(
			'model' => $model,
		), true);

		echo $html;
	}

	public function actionListLogItems($id)
	{
		$model = new CoreEnrollLogItem('search');
		$model->enroll_log_id = $id;

		$cs=Yii::app()->clientScript;
		$cs->scriptMap=array( //always remove these because they are already added since a grid is used on the non-ajax page
			'jquery.js'=>false,
			'jquery.min.js'=>false,
			'jquery.yiigridview.js'=>false,
			'jquery.ba-bbq.js'=>false,
		);

		$processOutput = (isset($_GET['ajax'])) ? false : true;
		$html = $this->renderPartial('_form_log_items', array(
			'model' => $model,
		), true, $processOutput);

		echo $html;
	}




	/**
	 * Receive POST from Selector Dialog and assign Branches/Groups/Courses to a given enrollment rule
	 */
	public function actionAssignItemsToRule()
	{
		$idEnrollRule = Yii::app()->request->getParam("idEnrollRule", false);
		$ruleModel = CoreEnrollRule::model()->findByPk($idEnrollRule);
		$selectorType = Yii::app()->request->getParam("type", false);


		try {

			if (!$ruleModel) {
				throw new CException('Invalid rule');
			}


			if (isset($_REQUEST['confirm'])) {

				$itemType = false;
				$selectedItems = array();
				switch ($selectorType) {
					case UsersSelector::TYPE_ENROLLRULES_BRANCHES:
						$itemType = CoreEnrollRuleItem::ITEM_TYPE_BRANCH;
						$selectedItems 	= UsersSelector::getBranchesListOnly($_REQUEST);
						break;

					case UsersSelector::TYPE_ENROLLRULES_GROUPS:
						$itemType = CoreEnrollRuleItem::ITEM_TYPE_GROUP;
						$selectedItems = UsersSelector::getGroupsListOnly($_REQUEST);
						break;

					case UsersSelector::TYPE_ENROLLRULES_COURSES:
						$itemType = CoreEnrollRuleItem::ITEM_TYPE_COURSE;
						$selectedItems = UsersSelector::getCoursesListOnly($_REQUEST);
						break;

				}

				if (!$itemType) {
					throw new CException('Invalid item or selector type');
				}

				// Cleanup: remove old assignments
				CoreEnrollRuleItem::removeAllByTypeAndRule($itemType, $ruleModel->rule_id);

				// And assign new items
				foreach ($selectedItems as $idItem) {
					$model = new CoreEnrollRuleItem();
					$model->item_type = $itemType;
					$model->rule_id = $ruleModel->rule_id;
					if($selectorType == UsersSelector::TYPE_ENROLLRULES_BRANCHES) {
						$model->item_id = $idItem['key'];
						$model->selection_state = $idItem['selectState'];
					} else {
						$model->item_id = $idItem;
					}
					$model->save(false);
				}
			}


		}
		catch (CException $e) {
			$this->renderPartial('admin.protected.views.common._dialog2_error', array(
					'type' => 'error',
					'message' => $e->getMessage(),
			));
			Yii::app()->end();
		}


		echo '<a class="auto-close success">';
		Yii::app()->end();



	}


}
