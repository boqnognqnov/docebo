<?php

/**
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */
class SandboxController extends AdminController
{

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
	{
		$res = array();

		$res[] = array(
			'allow',
			'expression' => 'Yii::app()->user->getIsGodadmin()',
			'actions' => array('create'),
		);

		$res[] = array(
			'deny',
			// deny all
			'users' => array('*'),
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);

		return $res;
	}

	public function actionCreate()
	{
		$erpClient = new ErpApiClient2();

		$tosAccepted = Yii::app()->request->getParam('tos_accept');
		if ($tosAccepted) {

			$sandboxCreation = $erpClient->createSandbox();

			if ($sandboxCreation && isset($sandboxCreation['success']) && $sandboxCreation['success']) {
				$raw = Settings::get('sandbox_details');

				$jsonDetails = array();
				if (!empty($raw)) {
					$jsonDetails = CJSON::decode($raw);
					if(!is_array($jsonDetails)){
						$jsonDetails = array();
					}
				}
				if (!isset($jsonDetails['sandbox_details']) || empty($jsonDetails['sandbox_details'])) {
					$jsonDetails['sandbox_details'] = array();
				}
				$jsonDetails['id_user'] = Yii::app()->user->getIdst();
				$jsonDetails['sandbox_details']['requested_timestamp'] = time();
				$jsonDetails['sandbox_details']['status'] = SandboxCreatorHelper::SANDBOX_STATUS_ACTIVATING;
				Settings::save('sandbox_details', CJSON::encode($jsonDetails));

				$this->renderPartial('sandboxCreationStarted');
				Yii::app()->end();
			}

		}

		// Build the domain name for the sandbox
		$originalDomain = Docebo::getOriginalDomain();
		$thirdLevelDomainName = substr($originalDomain, 0, strpos($originalDomain, "."));
		$sandboxDomain = $thirdLevelDomainName."-70.docebosandbox.com";

		$this->renderPartial('createSandbox', array(
			'sandboxDomain' => $sandboxDomain,
			'expirationDate' => date('F d, Y', strtotime($erpClient->getSandboxExpireDate())),
			'newVersion'=>'7.0'
		));
	}

}