<?php

class BrandingController extends AdminController {

	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}

	// TODO: change access rules
	public function accessRules() {
		$res = array();

		$res[] = array(
			'allow',
			'roles' => array('/lms/admin/middlearea/view'),
			'actions' => array(
				'index',
				'logo',
				'signin',
				'layoutColors',
				'customizeCss',
				'moduleActivation',
				'moduleUsers',
				'moduleChangeStatus',
				'downloadCss',
				'manageBackground',
				'getBackground',
				'addExternalPage',
				'editExternalPage',
				'editPageVisibility',
				'orderPages',
				'togglePagePublish',
				'deletePage',
				'addColorsScheme',
				'editColorsScheme',
				'toggleSchemeEnabled',
				'deleteScheme',
				'coursePlayer',
				'whiteLabel',
				'axPlayerBg',
				'AssignUsersToModule',
				'handleFile',
				'webpages',
			),
		);

		$res[] = array(
			'deny',
			// deny all
			'users' => array('*'),
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);

		return $res;
	}

	public function actionIndex() {
		Yii::app()->clientScript->registerCssFile('js/farbtastic/farbtastic.css');
//		Yii::app()->clientScript->registerCoreScript('jquery.ui');
		Yii::app()->clientScript->registerScriptFile('js/farbtastic/farbtastic.min.js');
//		Yii::app()->clientScript->registerScriptFile('js/jquery.tools.min.js');
		Yii::app()->clientScript->registerScriptFile('js/scriptBranding.js');
		Yii::app()->clientScript->registerScriptFile('js/jquery.cookie.js');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.blockUI.js');
		Yii::app()->fontawesome->loadIconPicker();

		//Yii::app()->tinymce;
		Yii::app()->tinymce->init();


		// Because of IE8
 		Yii::import('common.extensions.plupload.widgets.SingleFile', false);
 		$sf = new SingleFile();
 		Yii::app()->clientScript->registerScriptFile($sf->getAssetsUrl() . '/singlefile.js' , CClientScript::POS_HEAD);


		// Register selector resources
		UsersSelector::registerClientScripts();

		$this->render('index', array(
			'signin' => array(
				'model' => new BrandingSigninForm(),
				//'pageTranslationModel' => LearningWebpageTranslation::model(),
				'pageModel' => LearningWebpage::model()
			),
			'layout' => array(
				'model' => new BrandingLayoutColorsForm(),
				'schemeModel' => CoreScheme::model(),
			),
		));
	}

	public function actionLogo() {
		$defaultLanguage = CoreLangLanguage::getDefaultLanguage();
		if(!empty($_POST['BrandingLogoForm'])){
			$isActive = intval($_POST['BrandingLogoForm']['header_message_active']);
			$valuesForHeaderMsg = $_POST['valuesForHeaderMsg'];
			$isEmptyValueDefaultLang = empty($valuesForHeaderMsg[$defaultLanguage]);
			if (!$isEmptyValueDefaultLang || $isActive == 0)
				Settings::save('header_message_active', $isActive);

			if ($isActive == 1) {
				if($isEmptyValueDefaultLang) {
					$_POST['BrandingLogoForm']['header_message_active'] = Settings::get('header_message_active', 0);
					Yii::app()->user->setFlash('error', Yii::t('user_management', '_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED'));
				}
				else {
					Settings::save('header_message_active', $isActive);
					foreach ($valuesForHeaderMsg as $lang => $msg) {
						$model = CoreLangWhiteLabelSetting::model()->findByAttributes(array(
								'language' => $lang,
								'type' => CoreLangWhiteLabelSetting::TYPE_HEADER_MESSAGE
						));
						if (!$model) $model = new CoreLangWhiteLabelSetting();
						$model->language = $lang;
						$model->type = CoreLangWhiteLabelSetting::TYPE_HEADER_MESSAGE;
						$model->value = $msg;
						$model->save();
					}
				}
			}
		}
		$form = new BrandingLogoForm();
		$this->processForm($form);
		$languages = CoreLangLanguage::getActiveLanguages();

		$languages[$defaultLanguage] .=' ('.Yii::t('standard', '_DEFAULT_LANGUAGE').')';
		$languagesValuesDB = Yii::app()->db->createCommand()
				->select()->from(CoreLangWhiteLabelSetting::model()->tableName())
				->where('type=:type', array(':type' => CoreLangWhiteLabelSetting::TYPE_HEADER_MESSAGE))
				->andWhere(array('IN', 'language', array_keys($languages)))->queryAll();
		$languagesValues = array();
		foreach ($languagesValuesDB as $lang) {
			if (!empty($lang['value'])) {
				$languagesValues[$lang['language']] = $lang['value'];
				$languages[$lang['language']].=' *';
			}
		}

		if (Yii::app()->request->isAjaxRequest) {
			$this->sendJSON(array(
					'success' => true,
					'html' => $this->renderPartial('logo',
							array(
									'model' => $form,
									'languages' => $languages,
									'languagesValues' => $languagesValues,
									'defaultLanguage' => $defaultLanguage
							), true),
			));
		}
	}

	public function actionWebpages() {
		Yii::app()->clientScript->registerScriptFile('js/scriptBranding.js');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.blockUI.js');
		Yii::app()->tinymce->init();

		$this->render("public_webpages", array('pageModel' => LearningWebpage::model()));
	}

	public function actionSignin() {
		$form = new BrandingSigninForm();
		$className = get_class($form);

		if (!empty($_POST[$className])) {
			// Old minimal layout settings
			$oldMinimalLoginType = Settings::get('minimal_login_type');
			$oldMinimalLoginBg = Settings::get('minimal_login_background');

			$form->attributes = $_POST[$className];

			// Reset to default minimal login beackground value if the minimal login type is changed(before we add asset(-s))
			if($oldMinimalLoginType <> $form->minimal_login_type) {
				$form->minimal_login_background = '#ffffff';
			}

			if(!empty($_POST[$className]['minimal_login_background_color']) && $_POST[$className]['minimal_login_type'] == 'color') {
				$form->minimal_login_background = strval($_POST[$className]['minimal_login_background_color']);
			}

			if ($form->validate()) {
				$form->save();

				$minimal_bg_img_path = !empty($_POST[$className]['minimal_bg_img_path']) ? $_POST[$className]['minimal_bg_img_path'] : '';
				$minimal_bg_img_original_filename = !empty($_POST[$className]['minimal_bg_img_original_filename']) ? $_POST[$className]['minimal_bg_img_original_filename'] : '';

				$minimal_bg_video_path = !empty($_POST[$className]['minimal_bg_video_path']) ? $_POST[$className]['minimal_bg_video_path'] : '';
				$minimal_bg_video_original_filename = !empty($_POST[$className]['minimal_bg_video_original_filename']) ? $_POST[$className]['minimal_bg_video_original_filename'] : '';

				$minimal_video_fallback_img_path = !empty($_POST[$className]['minimal_video_fallback_img_path']) ? $_POST[$className]['minimal_video_fallback_img_path'] : '';
				$minimal_video_fallback_img_original_filename = !empty($_POST[$className]['minimal_video_fallback_img_original_filename']) ? $_POST[$className]['minimal_video_fallback_img_original_filename'] : '';

				if($form->minimal_login_type == 'image' && !empty($minimal_bg_img_original_filename) && !empty($minimal_bg_img_path)) {
					BrandingSigninForm::createAsset($minimal_bg_img_original_filename, $minimal_bg_img_path, $form, 'minimal_login_background', CoreAsset::TYPE_GENERAL_ASSET, array(), true);
				}

				if($form->minimal_login_type == 'video' && !empty($minimal_bg_video_original_filename) && !empty($minimal_bg_video_path)) {
					BrandingSigninForm::createAsset($minimal_bg_video_original_filename, $minimal_bg_video_path, $form, 'minimal_login_background', CoreAsset::TYPE_GENERAL_ASSET);
				}

				if($form->minimal_login_type == 'video' && !empty($minimal_video_fallback_img_original_filename) && !empty($minimal_video_fallback_img_path)) {
					BrandingSigninForm::createAsset($minimal_video_fallback_img_original_filename, $minimal_video_fallback_img_path, $form, 'minimal_login_video_fallback_img', CoreAsset::TYPE_GENERAL_ASSET, array(), true);
				}

			} else {
				$_SESSION['processFormErrors'] = CHtml::errorSummary($form);
			}

			$url = Yii::app()->createUrl('branding/index', array('#' => $_POST['selectedTab']));
			$this->redirect($url);
		}
	}

    /**
     * Return "Layout & Colors" tab
     */
	public function actionLayoutColors() {
        $this->sendJSON(array(
            'success' => true,
            'html' => $this->renderPartial('layoutColors', array(
                'model' => new BrandingLayoutColorsForm(),
                'schemeModel' => CoreScheme::model(),
			), true),
        ));
    }

    public function actionManageBackground() {
		$gallerySize = 6;
		$form = new BrandingManageBackgroundForm();
		$path = Yii::app()->theme->basePath . '/images/login';
		
		
		// Handle Delete Image AJAX calls 
		if (isset($_REQUEST['delete_image']) && isset($_REQUEST['image_id'])) {
			
			$imageId = Yii::app()->request->getParam("image_id", false);
			$success = false;
			if ((int) $imageId > 0) {
				$asset = CoreAsset::model()->findByPk($imageId);
				if ($asset) {
					$success = $asset->delete();
				}
			}
			$ajaxResult = new AjaxResult();
			$data = array();
			$ajaxResult->setStatus($success)->setData($data)->toJSON();
			if (isset($transaction)) $transaction->commit();
			Yii::app()->end();
		}


		// Get list of URLs of SHARED and USER player backgrounds
		$assets = new CoreAsset();
		$defaultImages  = $assets->urlList(CoreAsset::TYPE_LOGIN_BACKGROUND, CoreAsset::SCOPE_SHARED, false, CoreAsset::VARIANT_SMALL);
		$userImages   	= $assets->urlList(CoreAsset::TYPE_LOGIN_BACKGROUND, CoreAsset::SCOPE_PRIVATE, false, CoreAsset::VARIANT_SMALL);
		
		foreach ($defaultImages as $key => $value) {
			if ($key == $form->home_login_img) {
				$selected = 'default';

				$this->reorderArray($defaultImages, $key, $value);
				break;
			}
		}

		if (empty($selected)) {
			foreach ($userImages as $key => $value) {
				if ($key == $form->home_login_img) {
					$selected = 'user';

					$this->reorderArray($userImages, $key, $value);
					break;
				}
			}
		}

		$selected = empty($selected) ? 'default' : $selected;

		$count = array(
			'default' => count($defaultImages),
			'user' => count($userImages),
		);

		$defaultImages = array_chunk($defaultImages, $gallerySize, true);
		$userImages = array_chunk($userImages, $gallerySize, true);

		$this->sendJSON(array(
			'success' => true,
			'html' => $this->renderPartial('manageBackground',
				array(
					'model' => $form,
					'defaultImages' => $defaultImages,
					'userImages' => $userImages,
					'count' => $count,
					'selectedTab' => $selected,
				), true),
		));
	}

	public function actionGetBackground($id) {
		
		$asset = CoreAsset::model()->findByPk($id);
		if ($asset) {
			Settings::save('home_login_img', $id);
			$this->sendJSON(array(
				'imageUrl' => $asset->getUrl(CoreAsset::VARIANT_SMALL),
			));
		}
	}
	

	public function actionAddExternalPage() {
		$model = new LearningWebpage();
		$languages = CoreLangLanguage::getActiveLanguages();
		$lang_code = Settings::get('default_language', 'english');
		$languages[$lang_code] = $languages[$lang_code] . " (" . Yii::t('standard', '_DEFAULT_LANGUAGE') . ")";
		$translationsArray = array();
		$emptyCount = 0;

		if (!empty($_POST['LearningWebpageTranslation'])) {
			$transaction = Yii::app()->db->beginTransaction();

			try {
				$model->attributes = $_POST['LearningWebpage'];
				$model->save();

				foreach ($_POST['LearningWebpageTranslation'] as $translation) {
					if ($translation['title'] == '' && $translation['description'] == '') {
						$emptyCount++;
						continue;
					}

					$t = new LearningWebpageTranslation();
					$t->attributes = $translation;
					$t->page_id = $model->id;
					$t->save();
				}

				if (count($languages) == $emptyCount) {
					throw new Exception("You must specify at least one language", 1);
				}

			} catch (Exception $e) {
				$transaction->rollback();
				echo '<a class="error" data-message='.CJSON::encode($e->getMessage()).'></a>';
				Yii::app()->end();
				//$this->sendJSON(array('success' => false, 'message' => $e->getMessage()));
			}

			$transaction->commit();
			echo '<a class="auto-close"></a>';
			Yii::app()->end();
			//$this->sendJSON(array('success' => true));
		}

		foreach ($languages as $langCode => $whatever) {
			$newLang = new LearningWebpageTranslation();
			$newLang->lang_code = $langCode;

			$translationsArray[] = $newLang;
		}

		$this->renderPartial('_externalPageForm', array(
			'isEditing' => false,
			'model' => $model,
			'languages' => $languages,
			'translations' => $translationsArray,
			'maxSequence' => $model->getMaxSequence(),
			'modalTitle' => Yii::t('branding', 'Add external page')
		));
	}

    public function actionEditPageVisibility($id) {
        /* @var $model LearningWebpage */
        $model = LearningWebpage::model()->with('translations')->findByPk($id);
        if (!$model) {
            $this->sendJSON(array('success' => false));
        }

        if (!empty($_POST['branding-page-visibility-grid-checkboxes']) || ($_POST['visible_nodes'] == 'all')) {
            $nodes = $_POST['branding-page-visibility-grid-checkboxes'];

            $optVisibleNodes = (isset($_POST['visible_nodes']) && in_array($_POST['visible_nodes'], array('all', 'selection'))) ? $_POST['visible_nodes'] : 'selection';

            $transaction = Yii::app()->db->beginTransaction();

            try {
                $model->visible_nodes = $optVisibleNodes;
                $model->save();

                LearningWebpageDomainbranding::model()->deleteAllByAttributes(array('id_page'=>$id));

                if ($optVisibleNodes === 'selection') {
                    // set page visible to only the selected nodes

                    foreach ($nodes as $nodeId) {
                        $visibleNode = new LearningWebpageDomainbranding();
                        $visibleNode->id_branding = $nodeId;
                        $visibleNode->id_page = $id;
                        $visibleNode->save();
                    }
                }

            } catch (Exception $e) {
                $transaction->rollback();
                $this->sendJSON(array('success' => false, 'message' => $e->getMessage()));
            }

            $transaction->commit();
            $this->sendJSON(array('success' => true));
        }

        // first save the list of nodes for which this page is visible
        $nodesVisibility = LearningWebpageDomainbranding::model()->findAllByAttributes(array('id_page'=>$id));
        $nodesVisibility = CHtml::listData($nodesVisibility, 'id_branding', 'id_branding');
        Yii::app()->session['branding_nodes_visibility'] = empty($nodesVisibility) ? array() : $nodesVisibility;

        $fullOrgChartTree = CoreOrgChartTree::getFullOrgChartTree();

        // filter out the nodes that don't have a branding scheme
        $coreOrgChartBrandingNodes = CHtml::listData(CoreOrgChartBranding::model()->findAll(), 'id_org', 'id_org');
        $orgChartNodes = array();

        foreach ($fullOrgChartTree['list'] as $node) {
            if (in_array($node->idOrg, $coreOrgChartBrandingNodes))
                $orgChartNodes[] = $node;
        }

        $dataProvider = new CArrayDataProvider($orgChartNodes, array(
            'keyField' => 'idOrg',
            'pagination' => false
        ));

        $this->sendJSON(array(
            'html' => $this->renderPartial('_editPageVisibility',
                array(
                    'model' => $model,
                    'dataProvider' => $dataProvider,
                ), true),
        ));
    }

	public function actionEditExternalPage($id) {
		$model = LearningWebpage::model()->with('translations')->findByPk($id);
		$emptyCount = 0;
		if (!$model) {
			$this->sendJSON(array('success' => false));
		}

		$languages = CoreLangLanguage::getActiveLanguages();
		$lang_code = Settings::get('default_language', 'english');
		$languages[$lang_code] = $languages[$lang_code] . " (" . Yii::t('standard', '_DEFAULT_LANGUAGE') . ")";
		$translationsArray = $model->translations;

		$translations = array();
		foreach ($model->translations as $translation) {
			$translations[$translation->lang_code] = $translation->id;
		}

		// Check if we're missing some languages translations
		$empty = array_diff_key($languages, $translations);
		foreach ($empty as $langCode => $whatever)
		{
			$newLang = new LearningWebpageTranslation();
			$newLang->page_id = $id;
			$newLang->lang_code = $langCode;

			$translationsArray[] = $newLang;
		}

		if (!empty($_POST['LearningWebpageTranslation'])) {
			$transaction = Yii::app()->db->beginTransaction();

			try {
				$model->attributes = $_POST['LearningWebpage'];
				$model->save();

				foreach ($_POST['LearningWebpageTranslation'] as $translation) {
					if ($translation['title'] == '' && $translation['description'] == '') {
						$emptyCount++;
						continue;
					}

					if ($translation['id'])
						$t = LearningWebpageTranslation::model()->findByAttributes(array('id' => $translation['id']));
					else
						$t = new LearningWebpageTranslation();

					$t->attributes = $translation;
					$t->save();
				}

				if (count($languages) == $emptyCount) {
					throw new Exception("You must specify at least one language", 1);
				}

			} catch (Exception $e) {
				$transaction->rollback();
				echo '<a class="error" data-message='.CJSON::encode($e->getMessage()).'></a>';
				Yii::app()->end();
				//$this->sendJSON(array('success' => false, 'message' => $e->getMessage()));
			}

			$transaction->commit();
			echo '<a class="auto-close"></a>';
			Yii::app()->end();
			//$this->sendJSON(array('success' => true));
		}

		$this->renderPartial('_externalPageForm', array(
			'isEditing' => true,
			'model' => $model,
			'languages' => $languages,
			'translations' => $translationsArray,
			'maxSequence' => $model->getMaxSequence(),
			'modalTitle' => Yii::t('branding', 'Edit external page')
		));
		/*
		$this->sendJSON(array(
			'html' => $this->renderPartial('_externalPageForm',
				array(
					'model' => $model,
					'languages' => $languages,
					'translations' => $translationsArray,
					'maxSequence' => $model->getMaxSequence(),
				), true),
		));
		*/
	}

	public function actionTogglePagePublish($id) {
		$model = LearningWebpage::model()->findByPk($id);

		if ($model) {
			$model->publish = $model->publish == 1 ? 0 : 1;
			$model->save();
			$result['success'] = true;
		} else {
			$result['success'] = false;
		}

		$this->sendJSON($result);
	}

	public function actionDeletePage($id) {
		$page = LearningWebpage::model()->with('translations:language')->findByPk($id);

		if (empty($page)) {
			$this->sendJSON(array());
		}

		if (!empty($_POST['LearningWebpage'])) {
			if ($_POST['LearningWebpage']['confirm']) {
				$page->delete();
				$this->sendJSON(array());
			}
		}
		if (Yii::app()->request->isAjaxRequest) {
			$content = $this->renderPartial('externalPageDelete', array(
				'model' => $page,
			), true);
			$this->sendJSON(array(
				'html' => $content,
			));
		}
		Yii::app()->end();
	}

	public function actionToggleSchemeEnabled($id) {
		$model = CoreScheme::model()->findByPk($id);

		if ($model && $model->enabled == 1) {
			$result['success'] = false;
		} else {
			$transaction = Yii::app()->db->beginTransaction();

			try {
				$command = Yii::app()->db->createCommand();
				$command->update('core_scheme', array('enabled' => 0));

				$model->enabled = 1;
				$model->save();
			} catch (Exception $e) {
				$transaction->rollback();
			}

			$transaction->commit();
			$result['success'] = true;
		}

		$this->sendJSON($result);
	}

	public function actionDeleteScheme($id) {
		$model = CoreScheme::model()->findByPk($id);
		if (empty($model)) {
			$this->sendJSON(array('success' => true));
		}

		if (!empty($_POST['CoreScheme'])) {
			if ($_POST['CoreScheme']['confirm']) {
                $result = array();
                if ($model && $model->delete()) {
                    $result['success'] = true;
                } else {
                    $result['success'] = false;
                }

				$this->sendJSON($result);
			}
		}
		if (Yii::app()->request->isAjaxRequest) {
			$content = $this->renderPartial('deleteScheme', array(
				'model' => new CoreScheme(),
				'users' => $model,
			), true);
			$this->sendJSON(array(
				'html' => $content,
			));
		}
		Yii::app()->end();
	}

	public function actionOrderPages() {
		$ids = $_POST['ids'];
		$command = Yii::app()->db->createCommand();

		if (!empty($ids)) {
			foreach ($ids as $weight => $id) {
				$command->update(LearningWebpage::model()->tableName(), array('sequence' => $weight), 'id = :id', array(':id' => $id));
			}
		}

		$this->sendJSON(array('success' => true));
	}

	public function actionAddColorsScheme() {
		$model = new CoreScheme();

		if (!empty($_POST['CoreScheme'])) {
			$transaction = Yii::app()->db->beginTransaction();
			$model->attributes = $_POST['CoreScheme'];

			try {
				if (!$model->save()) {
					throw new Exception($model->getError('title'), 1);
				}

				foreach ($_POST['CoreSchemeColor'] as $colorId => $color) {
					$cscs = new CoreSchemeColorSelection();
					$cscs->scheme_id = $model->id;
					$cscs->color_id = $colorId;
					$cscs->color = $color;
					$cscs->save();
				}
			} catch (Exception $e) {
				$transaction->rollback();
				$this->sendJSON(array('success' => false, 'message' => $e->getMessage()));
			}

			$transaction->commit();
			$this->sendJSON(array('success' => true));
		}

		$colors = CoreSchemeColor::model()->findAll();

		foreach ($colors as $color) {
			$cscs = new CoreSchemeColorSelection();
			$cscs->color = $color->color;
			$cscs->color_id = $color->id;

			$selection[] = $cscs;
		}

		$this->sendJSON(array(
			'html' => $this->renderPartial('_colorSchemeForm',
				array(
					'model' => $model,
					// 'colors' => CoreSchemeColor::model()->findAll(),
					'selection' => $selection,
				), true),
		));
	}

	public function actionEditColorsScheme($id) {
		$model = CoreScheme::model()->with('colors')->findByPk($id);

		if (!empty($_POST['CoreScheme'])) {
			$transaction = Yii::app()->db->beginTransaction();
			$model->attributes = $_POST['CoreScheme'];

			try {
				if (!$model->save()) {
					throw new Exception($model->getError('title'), 1);
				}

				foreach ($_POST['CoreSchemeColor'] as $colorId => $color) {
					$cscs = CoreSchemeColorSelection::model()->findByAttributes(array('scheme_id' => $id, 'color_id' => $colorId));
					if (empty($cscs)) {
						$cscs = new CoreSchemeColorSelection();
					}
					$cscs->scheme_id = $model->id;
					$cscs->color_id = $colorId;
					$cscs->color = $color;
					$cscs->save();
				}
			} catch (Exception $e) {
				$transaction->rollback();
				$this->sendJSON(array('success' => false, 'message' => $e->getMessage()));
			}

			$transaction->commit();
			$this->sendJSON(array('success' => true));
		}

		$this->sendJSON(array(
			'html' => $this->renderPartial('_colorSchemeForm',
				array(
					'model' => $model,
					'selection' => $model->colorsSelection,
				), true),
		));
	}

	public function actionCustomizeCss() {
		$form = new BrandingCustomizeCssForm();
		$this->processForm($form);

		if (Yii::app()->request->isAjaxRequest) {
			$this->sendJSON(array(
				'success' => true,
				'html' => $this->renderPartial('customizeCss', array('model' => $form), true),
			));
		}
	}

	public function actionDownloadCss($name) {
		$form = new BrandingCustomizeCssForm();

		$form->sendCss($name);

		$url = Yii::app()->createUrl('branding/index', array('#' => 'customizeCss'));
		$this->redirect($url);
	}

	public function actionModuleActivation() {
		$this->forward('menuManagement/index');
		return;
	}

    /**
     * Data provider for grid
     *
     * @return \CActiveDataProvider
     */
    public function moduleDataProvider() {
        $criteria = new CDbCriteria;
        $criteria->together = true;
        $criteria->with = array(
            'learningMenucourseUnders' => array(
            'joinType' => 'RIGHT JOIN',
            'select' => 'my_name, sequence'
        ));
        $criteria->order = 'learningMenucourseUnders.sequence asc';
        $criteria->select = 't.idModule, t.default_name';
        $criteria->condition = 't.module_info IN (\'all\',\'user\') AND t.mvc_path <> \'elearning/show\'';

        $array = array();
        $rows = LearningModule::model()->findAll($criteria);
        $disabledList = LearningMiddlearea::getDisabledList();

        foreach ($rows as $row) {
            if ($row->idModule == 47)
                $activate = CHtml::link(Yii::t('apps', 'Go to my apps'), '/lms/index.php?r=app/index');
            else
                $activate = CHtml::link("", Yii::app()->createUrl("branding/moduleChangeStatus", array("module" => "mo_{$row->idModule}")), array("class" => "module-status ".(!isset($disabledList["mo_{$row->idModule}"]) ? "suspend-action" : "activate-action")));

            
            $usersButtonHtml = $this->renderPartial('_assignUsersButton', array('idModule' => (int) $row->idModule), true);

            // OLD Variant (pre-UsersSelector)
            /*
            $usersButtonHtml = $this->renderPartial('//common/_modal', array(
                    'config' => array(
                        'сlass' => 'group-select-users',
                        'modalTitle' => Yii::t('standard', '_SELECT_USERS'),
                        'linkTitle' => '<span class="enrolled"></span>',
                        'linkOptions' => array(
                            'alt' => Yii::t('standard', '_SELECT_USERS')
                        ),
                        'url' => 'branding/moduleUsers&module=mo_'.  urlencode($row->idModule),
                        'buttons' => array(
                            array(
                                'type' => 'submit',
                                'title' => Yii::t('standard', '_SAVE'),
                                'formId' => 'select-form',
                            ),
                            array(
                                'type' => 'cancel',
                                'title' => Yii::t('standard', '_CANCEL'),
                            ),
                        ),
                        'afterLoadingContent' => 'function() { replacePlaceholder(); applyTypeahead($(\'.typeahead\')); $.cookie(\'moduleActivation\', true); }',
                        'beforeSubmit' => 'initModuleActivationData',
                        'afterSubmit' => 'updateModuleActivationContent',
                    ),
            ), true);
            */


					//handle some text key exceptions
					$titleText = '';
					switch ($row->default_name) {
						case 'My Calendar': { $titleText = ucfirst(Yii::t('calendar', 'My Calendar')); } break;
						default: { $titleText = Yii::t("menu_over", $row->default_name); } break;
					}

            $array[] = array(
                'title' => $titleText,
                'users' => $usersButtonHtml,
                'activate' => $activate,
            );
            
            
        }

        $config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));
        $data = new CArrayDataProvider($array, $config);

        return $data;
    }


	/**
	 * Receive data from Users Selector (POST) and assign users to given module
	 */
	public function actionAssignUsersToModule()
	{

		$rq = Yii::app()->request;

		$idModule = $rq->getParam('idModule', false);
		$module = 'mo_' . $idModule;

		$criteria = new CDbCriteria();
		$criteria->condition = 't.obj_index = :obj_index';
		$criteria->params[':obj_index'] = $module;
		$model = LearningMiddlearea::model()->find($criteria);

		//$usersList = UsersSelector::getUsersList($_REQUEST);
		//retrieve groups and branches idsts from input data
		$inputList = array();
		$selectedGroups = CJSON::decode($rq->getParam('selected_groups', '[]'));
		$selectedBranches = CJSON::decode($rq->getParam('orgcharts_selected-json', '[]'));
		//resolve groups
		if (is_array($selectedGroups)) {
			foreach ($selectedGroups as $selectedGroup) {
				$inputList[] = (int)$selectedGroup;
			}
		}
		//resolve orgchart branches
		if (is_array($selectedBranches)) {
			//first collect all existing branch information
			$orgList = array();
			$reader = Yii::app()->db->createCommand("SELECT idOrg, idst_oc, idst_ocd FROM ".CoreOrgChartTree::model()->tableName()."")->query();
			while ($record = $reader->read()) {
				$orgList[(int)$record['idOrg']] = array((int)$record['idst_oc'], (int)$record['idst_ocd']);
			}
			//then check selection input
			foreach ($selectedBranches as $selectedBranch) {
				if (is_array($selectedBranch) && isset($selectedBranch['key']) && isset($selectedBranch['selectState']) && isset($orgList[(int)$selectedBranch['key']])) {
					switch ((int)$selectedBranch['selectState']) {
						case 1: //single folder, no descendants --> get idst_oc
							$inputList[] = $orgList[(int)$selectedBranch['key']][0];
							break;
						case 2: //folder + descendants --> get idst_ocd
							$inputList[] = $orgList[(int)$selectedBranch['key']][1];
							break;
					}
				}
			}
		}
		//make sure not to have undesired duplicates
		$inputList = array_unique($inputList);

		try {

			if (!$model) {
				$model = new LearningMiddlearea;
				$model->obj_index = $module;
			}

			if (!$model) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			$model->idst_list = serialize($inputList);
			$model->save(false);

			echo "<a class='auto-close success'></a>";
			Yii::app()->end();
		}
		catch (CException $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			Yii::app()->user->setFlash('error', $e->getMessage());
			$selectorDialogUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
				'type' 	=> UsersSelector::TYPE_MODULE_MANAGEMENT,
				'idModule'	=> $idModule,
			));
			$this->redirect($selectorDialogUrl, true);
		}

		echo "<a class='auto-close success'></a>";
		Yii::app()->end();
	}
    
    
    
    
    /**
     * Select users that will use this module
     *
     * @param string $module
     */
	public function actionModuleUsers($module) {

		$criteria = new CDbCriteria();
        $criteria->condition = 't.obj_index = :obj_index';
        $criteria->params[':obj_index'] = $module;
        $model = LearningMiddlearea::model()->find($criteria);

        if ($model && !(isset($_COOKIE['moduleActivation']) && $_COOKIE['moduleActivation'] == 'true') &&
                !(isset($_REQUEST['select-user-grid-selected-items']) ||
                isset($_REQUEST['select-group-grid-checkboxes_all']) || isset($_REQUEST['select-group-grid-checkboxes']))
        ) {
            $users = unserialize($model->idst_list);
            if (is_array($users))
                Yii::app()->session['selectedItems'] = $users;
        }

        $user = CoreUser::model();
        $user->scenario = 'search';
        $user->unsetAttributes();
        if (isset($_REQUEST['CoreUser'])) {
            $user->attributes = $_REQUEST['CoreUser'];
        }

        $group = new CoreGroup();
        if (isset($_REQUEST['CoreGroup'])) {
            $group->attributes = $_REQUEST['CoreGroup'];
        }

        $userlist = array();

        $close = false;

        // Users
        // has selected users
        if (isset($_REQUEST['select-user-grid-selected-items']) && $_REQUEST['select-user-grid-selected-items']) {
            $rows = explode(',', $_REQUEST['select-user-grid-selected-items']);
            foreach ($rows as $val)
                $userlist[$val] = $val;
            $close = true;
        }

        // Groups
        // has selected groups
        if (isset($_REQUEST['select-group-grid-selected-items']) && $_REQUEST['select-group-grid-selected-items']) {
            $criteria = new CDbCriteria;
            $criteria->select = 't.idstMember';
            $criteria->addInCondition('t.idst', $_REQUEST['select-group-grid-checkboxes']);
            $rows = CoreGroupMembers::model()->findAll($criteria);

            foreach ($rows as $row)
                $userlist[$row->idstMember] = $row->idstMember;
            $close = true;
        }

        // Organization chart
        // has selected some orgs
        if (isset($_REQUEST['select-orgchart']) && $_REQUEST['select-orgchart']) {
            $orgChartGroups = $_REQUEST['select-orgchart'];

            $allNodes = array();
            // Get the root node to handle the "allnodes" case
            $rootOrgNode = CoreOrgChartTree::getOrgRootNode();
            foreach ($orgChartGroups as $id => $value) {
                if (($id == $rootOrgNode->idOrg) && ($value != '0')) {
                    $allNodes = 'allnodes';
                    break;
                }
                if ($value == '1') {
                    if (!in_array($id, $allNodes)) {
                        $allNodes[] = $id;
                    }
                } elseif ($value == '2') {
                    $allChildrenNodesIds = CoreUser::model()->getCurrentAndChildrenNodesList($id);
                    if (!empty($allChildrenNodesIds)) {
                        foreach ($allChildrenNodesIds as $childId) {
                            if (!in_array($childId, $allNodes)) {
                                $allNodes[] = $childId;
                            }
                        }
                    }
                }
            }

            if (!empty($allNodes)) {
                $criteria = new CDbCriteria();

                // criteria for all users not in current course
                $criteriaAll = CoreUser::model()->dataProvider()->criteria;

                if (($allNodes !== 'allnodes') && (is_array($allNodes))) {
                    // criteria for selected nodes
                    $criteria->with = array(
                        'orgChartGroups' => array(
                            'joinType' => 'INNER JOIN',
                            'condition' => 'orgChartGroups.idOrg IN (\'' . implode('\',\'', $allNodes) . '\')',
                        ),
                    );
                    $criteria->together = true;
                } else {
                    $criteria = $criteriaAll;
                }

                $users = CoreUser::model()->findAll($criteria);
                foreach ($users as $user) {
                    $userlist[$user->idst] = $user->idst;
                }
                $close = true;
            }
        }

        // Save user list
        if ($_REQUEST['save']) {
            $_userlist = $userlist;
            $userlist = array();
            foreach($_userlist as $val)
                $userlist[] = $val;

            unset($_userlist);

            if (!$model) {
                $model = new LearningMiddlearea;
                $model->obj_index = $module;
            }
            $model->idst_list = serialize($userlist);
            $model->save(false);
            $this->sendJSON(array('status' => 'saved'));
            Yii::app()->end();
        } elseif ($close) {
            $this->sendJSON(array('status' => 'saved'));
            Yii::app()->end();
        }

        if (Yii::app()->request->isAjaxRequest) {
        	$orgChartsInfo = CoreOrgChartTree::getFullOrgChartTree();
            $content = $this->renderPartial('selectUsers', array(
                'userModel' => $user,
                'groupModel' => $group,
                'fullOrgChartTree' 	=> $orgChartsInfo['list'],
            	'puLeafs' 			=> $orgChartsInfo['leafs'],	
                'isNeedRegisterYiiGridJs' => true,
                ), true);

            if (isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html')) {
                echo $content;
            } else {
                $this->sendJSON(array('html' => $content));
            }
        }
        Yii::app()->end();
	}

    public function actionModuleChangeStatus($module) {

        $criteria = new CDbCriteria();
        $criteria->condition = 't.obj_index = :obj_index';
        $criteria->params[':obj_index'] = $module;
        $object = LearningMiddlearea::model()->find($criteria);

        if (!$object) {
            $idst_list = serialize(array());

            $object = new LearningMiddlearea;
            $object->idst_list = $idst_list;
            $object->obj_index = $module;
            $object->disabled = true;
        } else {
            $object->disabled = $object->disabled ? false : true;
        }

        $object->save(false);

        $this->sendJSON(array());
        Yii::app()->end();
    }

	private function processForm($form, $clearErrors = true) {
		$className = get_class($form);

		if (!empty($_POST[$className])) {
			$form->attributes = $_POST[$className];

			if ($form->validate(NULL, $clearErrors)) {
				$form->save();

				// Trigger hydra invalidation
				Yii::app()->legacyWrapper->invalidate();
			} else
				$_SESSION['processFormErrors'] = CHtml::errorSummary($form);

			if(isset($_POST['panel']) && $_POST['panel'])
				$url = Yii::app()->createUrl('branding/'.$_POST['panel']);
			else
				$url = Yii::app()->createUrl('branding/index', array('#' => $_POST['selectedTab']));
			$this->redirect($url);
		}
	}

    public function actionCoursePlayer() {
        $form = new BrandingCoursePlayerForm();
        $this->processForm($form);

        if (Yii::app()->request->isAjaxRequest) {
            $this->sendJSON(array(
                'success' => true,
                'html' => $this->renderPartial('coursePlayer', array('model' => $form), true),
            ));
        }
    }

	public function actionWhiteLabel()
	{
		if (PluginManager::isPluginActive('WhitelabelApp'))
		{
			$form = new BrandingWhiteLabelForm();
			$this->processForm($form, false);
			$languagesForTextFooter = CoreLangLanguage::getActiveLanguages();
			$languagesForUrlHeader = CoreLangLanguage::getActiveLanguages();
			$defaultLanguage = CoreLangLanguage::getDefaultLanguage();
			foreach ($languagesForTextFooter as $key => &$lang) {
				if($key == $defaultLanguage)
					$lang .= ' (' . Yii::t('standard', '_DEFAULT_LANGUAGE') . ')';
			}
			$languagesForUrlHeader[$defaultLanguage] .= ' (' . Yii::t('standard', '_DEFAULT_LANGUAGE') . ')';

			$languagesForUrl = $languagesForTextFooter;
			$translationsListDB = Yii::app()->db->createCommand()
					->select()->from(CoreLangWhiteLabelSetting::model()->tableName())
					->where('value != "" AND type=:type', array(':type'=>BrandingWhiteLabelForm::POWEREDBY_CUSTOM_TEXT))
					->andWhere(array('IN', 'language', array_keys($languagesForUrl)))->queryAll();
			$translationsList = array();
			foreach ($translationsListDB as $item) {
				$translationsList[$item['language']] = $item;
				$languagesForTextFooter[$item['language']] .= ' *';
			}

			$translationsListForURL = array();
			$translationsListDBForURL = Yii::app()->db->createCommand()
					->select()->from(CoreLangWhiteLabelSetting::model()->tableName())
					->where('value != "" AND value != "http://" AND type=:type', array(':type'=>BrandingWhiteLabelForm::POWEREDBY_CUSTOM_URL))
					->andWhere(array('IN', 'language', array_keys($languagesForUrl)))->queryAll();

			foreach ($translationsListDBForURL as $item) {
				$translationsListForURL[$item['language']] = $item;
					$languagesForUrl[$item['language']] .= ' *';
			}

			$translationsListDBForURLHeader = Yii::app()->db->createCommand()
					->select()->from(CoreLangWhiteLabelSetting::model()->tableName())
					->where('value != "" AND value != "http://" AND type=:type', array(':type'=>BrandingWhiteLabelForm::POWEREDBY_CUSTOM_URL_HEADER))
				    ->andWhere(array('IN', 'language', array_keys($languagesForUrlHeader)))->queryAll();

			foreach ($translationsListDBForURLHeader as $item) {
				$translationsListForURLHeader[$item['language']] = $item;
				$languagesForUrlHeader[$item['language']] .= ' *';
			}

			$viewParams = array(
				'model' => $form,
				'languages' => $languagesForTextFooter,
				'languagesForUrl' => $languagesForUrl,
				'languagesForUrlHeader' => $languagesForUrlHeader,
				'defaultLanguage' => $defaultLanguage,
				'translationsList' => $translationsList,
				'translationsListForURL' => $translationsListForURL,
				'translationsListForURLHeader' => $translationsListForURLHeader
			);

			if (Yii::app()->request->isAjaxRequest) {
				$viewParams['isInsideTabs'] = true;
				$content = $this->renderPartial('whiteLabel', $viewParams, true, true);  // processOutput, as we have a widget inside partial
			} else {
				Yii::app()->tinymce->init();
				$viewParams['isInsideTabs'] = false;
				$this->render('whiteLabel', $viewParams);
			}
		}
		else {
			if (Yii::app()->request->isAjaxRequest)
				$content = $this->renderPartial('whiteLabelInactive', array(), true);
			else
				$this->render('whiteLabelInactive', array());
		}


		if (Yii::app()->request->isAjaxRequest) {
			$this->sendJSON(array(
				'success' => true,
				'html' => $content,
			));
		}
	}


	/**
	 * Show modal dialog to upload/change Per Course background image
	 */
	public function actionAxPlayerBg() {

		// SAVE button
		$confirm_save = Yii::app()->request->getParam("confirm_save", false);

		// Delete requested
		$delete_image = Yii::app()->request->getParam("delete_image", false);


		$transaction = null;
		if (Yii::app()->db->getCurrentTransaction() === NULL) { $transaction = Yii::app()->db->beginTransaction(); }

		// Wrap everything in try/catch and provide DB transaction commit/rollback
		try {

			// If user selected an image to upload, automatic AJAX form submition is executed
			// Lets get the file and save it. No cropping functionality.
			if (!empty($_FILES["image_manage_file"]) && !$confirm_save && !$delete_image) {
				
				// Get hthe uploaded file
				$uploadedFile 	= CUploadedFile::getInstanceByName("image_manage_file");
				$filePath      	= $uploadedFile->getTempName();
				$tmpFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $uploadedFile->getName();
				copy($filePath, $tmpFilePath);
				
				$asset = new CoreAsset(); 
				$asset->sourceFile = $tmpFilePath;
				$asset->type = CoreAsset::TYPE_PLAYER_BACKGROUND;
				
				// Validate!! && Save
				$ok = $asset->save();
				if (!$ok) {
					throw new CException(Yii::t("standard", '_OPERATION_FAILURE'));
				}
				
				// Commit
				if ($transaction) $transaction->commit();
				
				$ajaxResult = new AjaxResult();
				
				$data = array(
						'image_id' => $asset->id,
						'image_url' => $asset->getUrl(CoreAsset::VARIANT_SMALL),
				);
				
				$ajaxResult->setStatus(true)->setData($data)->toJSON();
				
				// Clean up
				FileHelper::removeFile($tmpFilePath);
				
				Yii::app()->end();
				
			}

			// SAVE CHANGES button is clicked
			if ($confirm_save) {
				$selectedImageId = Yii::app()->request->getParam("image_id_radio", false);
				if ($selectedImageId) {
					$src = CoreAsset::url($selectedImageId, CoreAsset::VARIANT_SMALL);
					$ajaxResult = new AjaxResult();
					$data = array(
						'image_id' => $selectedImageId,
						'image_url' => $src,
					);
					$ajaxResult->setStatus(true)->setData($data)->toJSON();
				}
				Yii::app()->end();
			}

			// DELETE AJAX call
			if ($delete_image) {
				$imageId = Yii::app()->request->getParam("image_id", false);
				$success = false;
				if ((int) $imageId > 0) {
					$asset = CoreAsset::model()->findByPk($imageId);
					if ($asset) {
						$success = $asset->delete();
					}
				}
				$ajaxResult = new AjaxResult();
				$data = array();
				$ajaxResult->setStatus($success)->setData($data)->toJSON();
				if ($transaction) $transaction->commit();
				Yii::app()->end();
			}
			
			
			if ($transaction) $transaction->commit();
			
			

		}
		catch (CException $e) {
			// Clean up
			FileHelper::removeFile($tmpFilePath);
			// Log error
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			// Rollback 
			if ($transaction) $transaction->rollback();
			
			// Send result to caller
			$ajaxResult = new AjaxResult();
			$ajaxResult->setStatus(false)->setMessage($e->getMessage())->toJSON();
			
			Yii::app()->end();
		}


		// We end up here on first load
		$chunkSize = 6;

		// Get list of URLs of SHARED and USER player backgrounds
		$assets = new CoreAsset();
		$systemImages   = $assets->urlList(CoreAsset::TYPE_PLAYER_BACKGROUND, CoreAsset::SCOPE_SHARED, $chunkSize, CoreAsset::VARIANT_SMALL);
		$userImages   	= $assets->urlList(CoreAsset::TYPE_PLAYER_BACKGROUND, CoreAsset::SCOPE_PRIVATE, $chunkSize, CoreAsset::VARIANT_SMALL);
		$courseModel = LearningCourse::model();

		$html = $this->renderPartial('_modal_player_bg', array(
				'courseModel'	=> $courseModel,
				'systemImages' 	=> $systemImages,
				'userImages'	=> $userImages,
				'chunkSize'		=> $chunkSize
		),true,true);

		// Echo HTML to Dialog2
		echo $html;

	}

	public function actionHandleFile() {
		if (isset($_FILES['file'])) {
			$fileDir = Docebo::getUploadTmpPath();
			$fileId = Yii::app()->request->getParam('fileId', $_FILES['file']['name']);
			$extensions = Yii::app()->request->getParam('extensions', ''); // get allowed extensions from the request as string
			$extensions = explode(',', $extensions); // prepare array with allowed extensions

			$original_filename = $_FILES['file']['name'];

			// Run some extension checks on the file
			$fileHelper = new CFileHelper();
			$extension = strtolower($fileHelper->getExtension($original_filename ));

			// Check file extension - not discussed yet
			if(is_array($extensions) && count($extensions) > 0) {
				if (!in_array($extension, $extensions)) {
					echo '{"jsonrpc" : "2.0", "error" : {"code": 500, "message": "'.Yii::t('fileValidator', 'File type not allowed').'"}, "id" : "id"}';
					Yii::app()->end();
				}
			}

			if (file_exists($fileDir) && is_writable($fileDir) && is_dir($fileDir)) {
				$file = $_FILES['file'];

				file_put_contents( $fileDir . DIRECTORY_SEPARATOR . $fileId . '.' . $extension, file_get_contents($file['tmp_name']),FILE_APPEND );
			}

			$_SESSION['most_recent_activity'] = time();

			// if this is last portion, rename the file to the original name
			if ($_FILES['file']['size'] < (10 * 1024 * 1024)) {
				rename($fileDir . DIRECTORY_SEPARATOR . $fileId . '.' . $extension, $fileDir . DIRECTORY_SEPARATOR . $fileId . '.' . $extension);
				unset($_SESSION['most_recent_activity']);
				unset($_SESSION['tempFileName']);
			}

			echo '{"jsonrpc" : "2.0", "error" : {"code": 200, "message": ""}, "id" : "id","status": 200, "success": true, "result" : ""}';
			Yii::app()->end();
		}
	}


}