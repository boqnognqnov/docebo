<?php

	class ModalController extends Controller {

		public function afterAction() {
			Yii::app()->end();
		}

		public function actionModalTest() {
			echo 'Modal Test text';
		}

		public function actionTabTest() {
			$this->widget('ext.local.widgets.TabsWidget', array(
				'tabs' => array(
					array(
						'title' => 'Tab 1',
						'container' => 'users-tab',
						'class' => 'users-tab',
						'url' => 'modal/tab',
						'urlParams' => array('view' => 1),
					),
					array(
						'title' => 'Tab 2',
						'class' => 'second-tab',
						'container' => 'second-tab',
						'url' => 'modal/tab',
						'urlParams' => array('view' => 2),
					),
					array(
						'title' => 'Tab 3',
						'class' => 'third-tab',
						'container' => 'third-tab',
						'url' => 'modal/tab',
						'urlParams' => array('view' => 3),
					),
				),
			));
		}

		public function actionTab($view) {
			echo 'Content for tab #' . $view;
		}
	}