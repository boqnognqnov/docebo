<?php
/**
 * Class WebinarController
 *
 * Main controller for webinar account management
 */
class WebinarManagementController extends AdminController {

	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules()
	{
		$res = array();

		$res[] = array(
			'allow',
			'roles' => array('/framework/admin/dashboard/view'),
		);

		$res[] = array(
			'deny',
			// deny all
			'users' => array('*'),
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);

		return $res;
	}

	/**
	 * Deletes a webinar account
	 */
	public function actionDeleteAccount($id_account) {
		// Load account model
		$account = WebinarToolAccount::model()->findByPk($id_account);
		if (empty($account))
			$this->sendJSON(array());

		if (isset($_POST['confirm']) && $_POST['confirm']) {
			try {
				$account->delete();
			} catch(Exception $e) {
				Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			}

			$this->sendJSON(array());
		}

		if (Yii::app()->request->isAjaxRequest) {

			// Ask the widget to render the delete modal
			$content = $this->widget('common.widgets.WebinarAccountsEditor', array(
				'accountModel' => $account,
				'showDeleteView' => true
			), true);

			// Send HTML via json object
			$this->sendJSON(array(
				'html' => $content,
			));
		}


		Yii::app()->end();
	}
		
}