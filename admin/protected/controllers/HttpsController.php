<?php

class HttpsController extends Controller {
	
	public function filters() {
		return array(
				'accessControl' => 'accessControl',
		);
	}
	
	// TODO: change access rules
	public function accessRules() {
		$res = array();
		return $res;
	}
	

	
	/**
	 * Remove certificate for a given Https record (id)
	 * 
	 */
	public function actionRemoveCertificate() {
		/* @var $model CoreHttps */
		
		try {
			$id 		= Yii::app()->request->getParam('id');
			$model = CoreHttps::model()->findByPk($id);
			
			if (!$model)  {
				throw new Exception('Invalid model');
			}
			
			if (isset($_REQUEST['confirm']) && isset($_REQUEST['agree_delete']) && $_REQUEST['agree_delete']=='agree') {

				$model->sslStatus = CoreHttps::SSL_STATUS_NOT_INSTALLED;
				$model->certFile = null;
				$model->intermCaFile = null;
				
				if ($model->keyFileOrigin == CoreHttps::KEYFILE_ORIGIN_CUSTOMER || (isset($_REQUEST['also_delete_system_key']) && $_REQUEST['also_delete_system_key'] == 'alsosystemkey')) {
					$model->keyFile = null;
					$model->keyFileOrigin = null;
				}
				
				$model->save();

				echo '<a class="auto-close success"></a>';
				Yii::app()->end();
			}
				
			$this->renderPartial('_remove_certificate', array(
				'model'	=> $model,
			));
			Yii::app()->end();
				
		}
		catch (Exception $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			$this->renderPartial('//common/_dialog2_error', array('type' => 'error',  'message' => Yii::t('standard', '_OPERATION_FAILURE')));
			Yii::app()->end();
		}
		
		
		
	}
	
	
	public function actionCheckSslInstallationStatus() {
		$id = Yii::app()->request->getParam('id');
		$model = CoreHttps::model()->findByPk($id);
		$response = new AjaxResult();
		
		$status 	= $model->sslStatus;
		$lastError 	= $model->sslStatusLastError;
		
		if ($status == CoreHttps::SSL_STATUS_ERROR) {
			$model->sslStatus = null;
			$model->save(false);
		}
		
		$data = array(
			'sslStatus' 			=> $status,
			'sslStatusLastError'	=> $lastError,
		);
		
		
		$response->setStatus(true)->setData($data)->toJSON();
		
	}
	
	

	/**
	 * 
	 */
	public function actionCheckCsrStatus() {
		
		$id = Yii::app()->request->getParam('id');
		$model = CoreHttps::model()->findByPk($id);
		$response = new AjaxResult();
		
		$status = $model->csrGenerationStatus;
		
		$data = array(
			'csrStatus' 			=> $status,
			'sslStatusLastMessage'	=> $model->csrGenerationLastMessage,
			'keyFile'				=> $model->keyFile,
		);
		$response->setStatus(true)->setData($data)->toJSON();
	}
	
	
	public function actionDownload() {
		
		$id 	= Yii::app()->request->getParam('id');
		$type 	= Yii::app()->request->getParam('type');
		
		$model = CoreHttps::model()->findByPk($id);
		$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_NONE, CoreHttps::FILESTORAGE_CONFIG);
		
		switch ($type) {
			case CoreHttps::FILETYPE_KEY:
				$storage->sendFile($model->keyFile);
				Yii::app()->end();
				break;
				
			case CoreHttps::FILETYPE_CSR:
				$storage->sendFile($model->csrFile);
				Yii::app()->end();
				break;	
		}
		
	}
	
	
	/**
	 * Accept Https Agent's callbacks
	 */
	public function actionAgent() {
		
		/* @var $httpsModel CoreHttps */
		
		$dataJson 	= file_get_contents('php://input');
		$id 		= Yii::app()->request->getParam('id', false);
		$apiAction 	= Yii::app()->request->getParam('apiAction', false);
		$token 		= Yii::app()->request->getParam('token', false);
		
		$httpsModel = CoreHttps::model()->findByPk($id);
		$data		= json_decode($dataJson, true);
		if (!$httpsModel || !is_array($data) || empty($data)) {
			Yii::log('Got unknown or wrong data in Https/Agent. Ignoring request.', CLogger::LEVEL_ERROR);
			Yii::app()->end();
		}

		$goodToken = ($token && ($token === $httpsModel->httpsAgentToken));
		if (!$goodToken) {
			Yii::log('Bad token received from Https/Agent. Ignoring request.', CLogger::LEVEL_ERROR);
			Yii::app()->end();
		}
		
		switch ($apiAction) {
			case HttpsApiClient::API_ACTION_INSTALL_SSL:
				$this->processSslInstallation($httpsModel, $data);				
				break;
		
			case HttpsApiClient::API_ACTION_GENCSR:
				$this->processCsrGeneration($httpsModel, $data);		 
				break;
				
			case HttpsApiClient::API_ACTION_CHECK_CERT:
				$this->processCheckCertificate($httpsModel, $data);
				break;	
		}
		
	}
	
	
	
	public function actionSettings() {
		
		$action 	= Yii::app()->request->getParam('action', false);
		
		$response 	= new AjaxResult();
		$response->setStatus(false);
		
		if (!$action) {
			$response->setMessage('Invalid action');
			$response->toJSON();
			Yii::app()->end();
		}
		
		switch ($action) {
			case 'removeKeyFile':
				$this->removeKeyFile($response);
				break;

			case 'cancelPendingSslInstallation':
				$this->cancelPendingSslInstallation($response);
				break;
				
			case 'cancelPendingCsrGeneration':
				$this->cancelPendingCsrGeneration($response);
				break;	
				
			default:
				break;	
		}
		
		$response->toJSON();
		Yii::app()->end();
		
	}
	
	
	
	private function cancelPendingSslInstallation($response) {
		
		$id 		= Yii::app()->request->getParam('id', false);
		$httpsModel = CoreHttps::model()->findByPk($id);
		
		if (!$httpsModel) {
			$response->setMessage('Invalid data');
			return false;
		}
		
		$httpsModel->sslStatus = CoreHttps::STATUS_CANCELLED;
		
		if (!$httpsModel->save()) {
			$response->setMessage('Error saving Https settings data');
			return false;
		}
		
		$response->setStatus(true);
		return true;
		
		
	}
	
	
	private function cancelPendingCsrGeneration($response) {
	
		$id 		= Yii::app()->request->getParam('id', false);
		$httpsModel = CoreHttps::model()->findByPk($id);
	
		if (!$httpsModel) {
			$response->setMessage('Invalid data');
			return false;
		}
	
		$httpsModel->csrGenerationStatus = CoreHttps::STATUS_CANCELLED;
	
		if (!$httpsModel->save()) {
			$response->setMessage('Error saving Https settings data');
			return false;
		}
	
		$response->setStatus(true);
		return true;
	
	
	}
	
	

	
	
	private function removeKeyFile($response) {

		$id 		= Yii::app()->request->getParam('id', false);
		$httpsModel = CoreHttps::model()->findByPk($id);
		
		if (!$httpsModel) {
			$response->setMessage('Invalid data');
			return false;
		}
		
		$httpsModel->keyFile = null;
		$httpsModel->keyFileOrigin = null;
		if (!$httpsModel->save()) {
			$response->setMessage('Error saving Https settings data');
			return false;
		}
		
		$response->setStatus(true);
		return true;
		
	}
	
	
	/**
	 * 
	 * @param CoreHttps $httpsModel
	 * @param unknown $data
	 */
	private function processCheckCertificate(CoreHttps $httpsModel, $data) {
		
		Yii::log('Processing Check Certificate API callback from Https/Agent', CLogger::LEVEL_INFO);
		Yii::log('Https Model ID: ' . $httpsModel->id, CLogger::LEVEL_INFO);
		Yii::log('Data: ' . print_r($data,true), CLogger::LEVEL_INFO);
		
		// Analyze for errors
		if (!isset($data['Status']) || $data['Status'] != 'Success') {
			if (!isset($data['Status'])) {
				$errorMessage = 'Https Agent Callback returned no success status';
			}
			else {
				$errorMessage = $data['Status'];
			}
		}
		
		// Record the certificate expiration time
		$httpsModel->sslExpireTime = isset($data['Expiration']) ? $data['Expiration'] : null; 
		
		if ($errorMessage) {
			Yii::log($errorMessage, CLogger::LEVEL_ERROR);
			$httpsModel->sslStatus = CoreHttps::SSL_STATUS_ERROR;
			$httpsModel->sslStatusLastError = $errorMessage;
			$httpsModel->save(false);
			Yii::app()->end();
		}
		
		$httpsModel->save(false);
		
		$data = array(
			'lms'				=> Docebo::getOriginalDomain(),
			'cert_filename'		=> $httpsModel->certFile,
			'key_filename'		=> $httpsModel->keyFile,
			'bundle_filename'	=> $httpsModel->intermCaFile,	
			'callback_url'		=> HttpsApiClient::generateCallbackUrl($httpsModel,  HttpsApiClient::API_ACTION_INSTALL_SSL),
		);
		
		
		$resultArray = Yii::app()->httpsapi->installSsl($data);

		if (!$resultArray['success']) {
			Yii::log($resultArray['error'], CLogger::LEVEL_ERROR);
			$httpsModel->sslStatus = CoreHttps::SSL_STATUS_ERROR;
			$httpsModel->sslStatusLastError = Yii::t("standard", "Error calling API: " . $resultArray['status'] . ' ' . $resultArray['error']);
			$httpsModel->save(false);
		}
		
		Yii::app()->end();
		
		
	}
	

	/**
	 * 
	 * @param CoreHttps $httpsModel
	 * @param unknown $data
	 */
	private function processSslInstallation(CoreHttps $httpsModel, $data) {
		
		Yii::log('Processing SSL Installation API callback from Https/Agent', CLogger::LEVEL_INFO);
		Yii::log('Https Model ID: ' . $httpsModel->id, CLogger::LEVEL_INFO);
		Yii::log('Data: ' . print_r($data,true), CLogger::LEVEL_INFO);
		
		// Analyze for errors
		if (!isset($data['Status']) || $data['Status'] != 'Success') {
			$errorMessage = 'Https Agent Callback returned no success status';
		}
		
		if ($errorMessage) {
			Yii::log($errorMessage, CLogger::LEVEL_ERROR);
			$httpsModel->sslStatus = CoreHttps::SSL_STATUS_ERROR;
			$httpsModel->sslStatusLastError = $errorMessage;
			$httpsModel->save(false);
			Yii::app()->end();
		}

		$httpsModel->isWildCard = isset($data['isWildCard']) ? $data['isWildCard'] : 0;
		$httpsModel->sslInstallTime = isset($data['Date']) ? $data['Date'] : null;		
		$httpsModel->keyFile  = $data['PrivateKey'];
		$httpsModel->certFile = $data['Certificate'];
		$httpsModel->keyFileOrigin = CoreHttps::KEYFILE_ORIGIN_CUSTOMER;
		$httpsModel->sslStatus = CoreHttps::SSL_STATUS_INSTALLED;
		$httpsModel->sslStatusLastError = '';
		if (!$httpsModel->save()) {
			Yii::log('Error while saving Https model in Https/Agent callback', CLogger::LEVEL_ERROR);
			Yii::log(print_r($httpsModel->errors, true), CLogger::LEVEL_ERROR);
			Yii::app()->end();
		}
		
		Yii::log('SSL Certificate properly installed and set in Https table', CLogger::LEVEL_INFO);
		
		
	}
	
	
	/**
	 * 
	 * @param CoreHttps $httpsModel
	 * @param unknown $data
	 */
	private function processCsrGeneration(CoreHttps $httpsModel, $data) {
		
		Yii::log('Processing CSR Generation API callback from Https/Agent', CLogger::LEVEL_INFO);
		Yii::log('Https Model ID: ' . $httpsModel->id, CLogger::LEVEL_INFO);
		Yii::log('Data: ' . print_r($data,true), CLogger::LEVEL_INFO);
		
		// Analyze for errors
		if (!isset($data['Status']) || $data['Status'] != 'Success') {
			$errorMessage = 'Https Agent Callback returned no success status';
		}
		
		if (!$data['CSR']) {
			$errorMessage = 'Https Agent Callback returned empty CSR file name';
		}
		
		if (!$data['PrivateKey']) {
			$errorMessage = 'Https Agent Callback returned empty Private Key file name';
		}

		
		if ($errorMessage) {
			Yii::log($errorMessage, CLogger::LEVEL_ERROR);
			$httpsModel->csrGenerationStatus = CoreHttps::CSRGEN_STATUS_ERROR;
			$httpsModel->csrGenerationLastMessage = $errorMessage;
			$httpsModel->save(false);
			Yii::app()->end();
		}
		
		$httpsModel->csrGenerationTime = isset($data['Date']) ? $data['Date'] : null;
		$httpsModel->csrFile = $data['CSR'];
		$httpsModel->keyFile = $data['PrivateKey'];
		$httpsModel->keyFileOrigin = CoreHttps::KEYFILE_ORIGIN_CSRGEN;
		$httpsModel->csrGenerationStatus = CoreHttps::CSRGEN_STATUS_READY;
		$httpsModel->csrGenerationLastMessage = '';
		if (!$httpsModel->save()) {
			Yii::log('Error while saving Https model in Https/Agent callback', CLogger::LEVEL_ERROR);
			Yii::log(print_r($httpsModel->errors, true), CLogger::LEVEL_ERROR);
			Yii::app()->end();
		}
		
		Yii::log('CSR properly generated and set in Https table', CLogger::LEVEL_INFO);
		
	}
	
	
}