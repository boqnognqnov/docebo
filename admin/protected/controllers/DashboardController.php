<?php

class DashboardController extends AdminController {

	public $layout = '//layouts/adm';

	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	
	public function accessRules() {
		$res = array();

		$res[] = array(
			'allow',
			'roles' => array('/framework/admin/dashboard/view'),
			'actions' => array('index', 'reorder'),
		);

		$res[] = array(
			'deny',
			// deny all
			'users' => array('*'),
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);

		return $res;
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
        // FancyTree initialization
        Yii::app()->fancytree->init();
		Yii::app()->clientScript->registerCssFile(
			Yii::app()->clientScript->getCoreScriptUrl() .
			'/jui/css/base/jquery-ui.css'
		);

		$ielt9 = preg_match('/(?i)msie [1-8]/',$_SERVER['HTTP_USER_AGENT']);
		$assetsPath = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin.js'));

		//Yii::app()->clientScript->registerScriptFile($assetsPath . '/charts/ie.js');
		Yii::app()->clientScript->registerScriptFile($assetsPath . '/charts/aight.js');

		//Yii::app()->clientScript->registerScriptFile($assetsPath . '/charts/d3.v3.min.js');

		// Yii::app()->clientScript->registerScriptFile($assetsPath . '/charts/aight.d3.min.js');

		Yii::app()->clientScript->registerScriptFile($assetsPath . '/charts/jquery.tipsy.min.js');
		Yii::app()->clientScript->registerCssFile($assetsPath . '/charts/charts.css');

		Yii::app()->clientScript->registerScriptFile($assetsPath . '/friendlyText/jquery.friendly-text.js');
		Yii::app()->clientScript->registerCssFile($assetsPath . '/friendlyText/jquery.friendly-text.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/DoceboPager.css');

		Yii::app()->clientScript->registerCoreScript('bbq');

		Yii::app()->bootstrap->registerAssetCss('bootstrap-datepicker.css');
		Yii::app()->bootstrap->registerAssetJs('bootstrap.datepicker.js');

		UsersSelector::registerClientScripts();
		App7020Helpers::registerApp7020Options();
		/* @var $dashboard Dashboard */
		$filtersForm = new DashboardFiltersForm();
		if (isset($_POST['DashboardFiltersForm'])) {
			$filtersForm->attributes = $_POST['DashboardFiltersForm'];
		}
		// save the filters in session
		$filtersForm->saveFilters();

		$dashboard = Yii::app()->dashboard;
		$stats = $dashboard->getStats();
		$coachShareStats = $dashboard->getCoachShareStats();
		$blocksPosition = $dashboard->getBlocksPosition();
		$colors = Yii::app()->theme->getChartColors();

		Yii::app()->tinymce;
		
		$googlePluginsActive = (PluginManager::isPluginActive('GoogleappsApp') || PluginManager::isPluginActive('googleapps') || PluginManager::isPluginActive('gapps'));

		$this->render('index', array(
			'blocks' => $blocksPosition,
			'data' => array(
				'stats' => $stats,
				'coachShareStats' => $coachShareStats,
				'showGoogleImport' => $googlePluginsActive,
			),
			'colors' => $colors,
			'assetsPath' => $assetsPath,
			'filtersForm' => $filtersForm
		));
	}

	public function actionReorder() {
		if (!empty($_POST['blocks'])) {
			$dashboard = Yii::app()->dashboard;
			$dashboard->saveBlocksPosition($_POST['blocks']);
		}

		$this->sendJSON(array());
	}
}