<?php

class AdminController extends Controller {

	/**
	 * @var bool Set this to true, to show the full width layout
	 */
	protected $useFluidContainer = false;

	protected $adminJsAssetsUrl = null;
	
	public function adminHomepageRedirect($rule = false) {
		parent::accessDeniedCallback($rule);
	}

	public function beforeAction($action) {
		
		if (Yii::app()->request->isAjaxRequest && !empty($_POST['selectedItems'])) {
			Yii::app()->session['selectedItems'] = explode(',', $_POST['selectedItems']);
		} else {
			Yii::app()->session['selectedItems'] = array();
		}

		IPHelper::checkForIPwhitelist();

		Yii::app()->event->raise('OnBeforeCControllerAction', new DEvent($this, array('appType' => 'admin' )));

		// This one was missing here. I am adding it, but keep on commented, to avoid side effects. I wonder, is it on purpose ?? [plamen]
		return parent::beforeAction($action);
		
	}

	protected function reorderArray(&$array, $key, $value) {
		unset($array[$key]);
		$array = array($key => $value) + $array;
	}
	
	
	/**
	 * Publish Admin JS assets and return URL 
	 */
	public function getAdminJsAssetsUrl()	{
		if ($this->adminJsAssetsUrl === null) {
			$this->adminJsAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin.js'));
		}
		return $this->adminJsAssetsUrl;
	}
	

}