<?php

class CropController extends AdminController {

	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}

	public function accessRules() {
		$res = array();

		$res[] = array(
			'allow',
			'users' => array('*'),
			'actions' => array(
				'index',
			),
		);
		$res[] = array(
			'deny',
			// deny all
			'users' => array('*'),
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);

		return $res;
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
		if (Yii::app()->request->isAjaxRequest) {
			$className = Yii::app()->request->getQuery('class', '');
			$adminRoot = Yii::getPathOfAlias('admin');

			// File just have been uploaded, lets run the cropping process
			if (!empty($_FILES[$className])) {

				$validator = new FileTypeValidator(FileTypeValidator::TYPE_IMAGE);
				if (!$validator->validateFile($className . '[attachments]')) {
					throw new Exception($validator->errorMsg, 1);
				}

				$uploadedFile = CUploadedFile::getInstanceByName($className . '[attachments]');
				$filePath = $uploadedFile->getTempName();

				if (!empty($filePath)) {

					$widthNew = 540;
					$aspectRatioX = isset($_REQUEST['width']) ? $_REQUEST['width'] : 300;
					$aspectRatioY = isset($_REQUEST['height']) ? $_REQUEST['height'] : 300;

					// Prepare some names
					$fileExtension = substr($uploadedFile->getName(), strrpos($uploadedFile->getName(), '.') + 1);
					$newFileName = Docebo::randomHash() . '.' . $fileExtension;
					$cropFileNameHash = 'crop_' . $newFileName;
					$fullFileNameHash = 'full_' . $newFileName;

					// Get LOCAL FILE SYSTEM Storage, Assets collection !!!!!  And disable using cdn!!!
					$assetsStorage = CFileStorage::getLocalTempDomainStorage(CFileStorage::COLLECTION_ASSETS);
					$assetsStorage->disableCdn();

					// Copy uploaded file in the temporary storage under new names
					$assetsStorage->storeAs($filePath, $fullFileNameHash, 'tmp');
					$assetsStorage->storeAs($filePath, $cropFileNameHash, 'tmp');

					// Get paths to "crop" and "full" images
					$tmpFilePath = $assetsStorage->path() . DIRECTORY_SEPARATOR . 'tmp';
					$fullFilePath = $tmpFilePath . DIRECTORY_SEPARATOR . $fullFileNameHash;
					$cropFilePath = $tmpFilePath . DIRECTORY_SEPARATOR . $cropFileNameHash;

					// Also we need URLs to them
					$fullFileUrl = $assetsStorage->fileUrl($fullFileNameHash, 'tmp');
					$cropFileUrl = $assetsStorage->fileUrl($cropFileNameHash, 'tmp');

					// Convert "full" top "crop"; this is to be sure image fits the CROPPING dialog
					Yii::app()->imageMagick
							->setSourcePath($fullFilePath)
							->setDestinationPath($cropFilePath)
							->setWidth($widthNew)
							->setHeight('')
							->setQuality(85)
							->setResizeModificators(array())
 							->resize();
					$modalId = substr(md5(uniqid(rand(), true)), 0, 10);
 					$cropImageSize = getimagesize($cropFilePath);

					$content = $this->renderPartial('crop', array(
						'modalId' => $modalId,
						'cropImage' => $cropFileUrl,
						'widthNew' => $widthNew,
						'cropImageSize' => $cropImageSize,
						'name' => $uploadedFile->getName(),
						'full' => $fullFileUrl,
						'imageType' => Yii::app()->request->getParam("imageType", 0),
						'cropCallback' => Yii::app()->request->getParam("cropCallback", ''),
						'full_path' => $fullFilePath,
						'crop_path' => $cropFilePath,
							), true);

					$return = array(
						'modalId' => 'modal-' . $modalId,
						'content' => $content,
						'aspectRatioX' => $aspectRatioX,
						'aspectRatioY' => $aspectRatioY,
						'x' => 100,
						'y' => 100,
						'x2' => 100 + $aspectRatioX,
						'y2' => 100 + $aspectRatioY,
					);
					$this->sendJson($return);
				}
			} elseif (!empty($_POST['CropParams'])) {

				$params = $_POST['CropParams'];

				// Get full and cropping image info
				$fullFilePath = $_POST['CropParams']['full_path'];
				$cropFilePath = $_POST['CropParams']['crop_path'];
				$fullImageSize = getimagesize($fullFilePath);
				$fullW = $fullImageSize[0];
				$cropImageSize = getimagesize($cropFilePath);
				$cropW = $cropImageSize[0];

				// Calculate geometry mapped on FULL image based on crop selection from "copping" image
				$R = $fullW / $cropW;
				$x = $_POST['CropParams']['x'];
				$y = $_POST['CropParams']['y'];
				$w = $_POST['CropParams']['w'];
				$h = $_POST['CropParams']['h'];

				$xx = (int) ($x * $R);
				$yy = (int) ($y * $R);
				$ww = (int) ($w * $R);
				$hh = (int) ($h * $R);

				$command = 'mogrify -crop "' . $ww . 'x' . $hh . '+' . $xx . '+' . $yy . '" +repage ' . '"' . $fullFilePath . '"';

				$output = array();
				exec($command, $output);

				$asset = new CoreAsset();

				switch ($className) {
					case 'LearningCourse' :
						$asset->type = CoreAsset::TYPE_COURSELOGO;
						break;
					case 'LearningOrganization':
						$asset->type = CoreAsset::TYPE_LO_IMAGE;
						break;
					case 'BrandingManageBackgroundForm':
						$asset->type = CoreAsset::TYPE_LOGIN_BACKGROUND;
						break;
					default:
						Yii::log('Unable to save asset: unknown class name during cropping', CLogger::LEVEL_ERROR);
						throw new CException(Yii::t("standard", '_OPERATION_FAILURE'));
						break;
				}

				$asset->sourceFile = $fullFilePath;
				$ok = $asset->save();

				if (!$ok) {
					Yii::log('Unable to save asset', CLogger::LEVEL_ERROR);
					Yii::log(var_export($asset->getErrors(),true), CLogger::LEVEL_ERROR);
					throw new CException(Yii::t("standard", '_OPERATION_FAILURE'));
				}

				// Clean up
				FileHelper::removeFile($fullFilePath);
				FileHelper::removeFile($cropFilePath);

				if (!empty($params['cropCallback']) && $params['cropCallback'] != 'undefined') {
					$this->sendJSON(array('cropCallback' => $params['cropCallback']));
				} else {
					$this->sendJSON(array());
				}
			}
		}

		Yii::app()->end();
	}

}
