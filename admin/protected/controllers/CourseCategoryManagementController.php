<?php

/**
 * Class CourseCategoryManagementController
 */

class CourseCategoryManagementController extends AdminController {

	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}


	public function accessRules() {
		$res = array();
		$res[] = array(
			'allow',
			'roles' => array('/lms/admin/course/mod'),
			'actions' => array('create'),
		);
		$res[] = array(
			'allow',
			'roles' => array('/lms/admin/course/view'),
			'actions' => array(
				'edit',
				'move',
				'getNodeParents',
				'settings'
			),
		);
		$res[] = array(
			'allow',
			'roles' => array('/lms/admin/course/mod'),
			'actions' => array('delete'),
		);
		$res[] = array(
			'deny',
			// deny all
			'users' => array('*'),
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);
		return $res;
	}


	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
		echo 'index';
		exit;
	}




	public function actionCreate() {

		$rq = Yii::app()->request;

		$learningCourseCategory = new LearningCourseCategory();
		$learningCourseCategory->lang_code = CoreUser::getDefaultLangCode();
		$activeLanguagesList = CoreLangLanguage::getActiveLanguages();
		$lang_code = Settings::get('default_language', 'english');
		$activeLanguagesList[$lang_code] = $activeLanguagesList[$lang_code] . " (" . Yii::t('standard', '_DEFAULT_LANGUAGE') . ")";
		// This parameter is send only when Creating Category from Central LO
		$isCentralLO = $rq->getParam("central_lo", false);

		//--- check if the tree has changed while the client page was opened
		$treeGenerationTime = $rq->getParam('treeGenerationTime', false);
		if ($treeGenerationTime != false) {
			if (CoreRunningOperation::isBeforeLastOperationTime('course_categories_tree_change', $treeGenerationTime)) {
				echo CJSON::encode(array('update_tree' => true));
				Yii::app()->end();
			}
		}

		//check if we have confirmed the action
		if (!empty($_POST['LearningCourseCategory'])) {
			if (!empty($_POST['LearningCourseCategory']['translation'])) {
				$allTranslationEmpty = true;
				foreach($_POST['LearningCourseCategory']['translation'] as $translation) {
					if (!empty($translation)) {
						$allTranslationEmpty = false;
					}
				}
				if ($allTranslationEmpty) {
					$learningCourseCategory->addError('translation', Yii::t("standard", "Category name can't be empty"));
				} elseif (empty($_POST['LearningCourseCategory']['translation'][$lang_code])) {
					$learningCourseCategory->addError('lang_code', Yii::t("standard", "Default language value not set"));
				}elseif ($learningCourseCategory->validate()) {

					//if tree is ok then we can start our operation and continue with regular action
					if (!CoreRunningOperation::startOperation('course_categories_tree_change')) {
						// We cannot start operating on the tree structure because someone else is already doing it and we must not
						// interfere to avoid conflicts/data corruption
						$response = new AjaxResult();
						$response->setStatus(false)->setData(array('update_tree' => true))->toJSON();
						Yii::app()->end();
					}
					//---

					$criteria = new CDbcriteria();
					$criteria->select = 'max(idCategory) as idCategory';
					$maxCourseCategory = $learningCourseCategory->find($criteria);
					$currentId = $maxCourseCategory->idCategory+1;
					if (!empty($currentId) && !empty($activeLanguagesList)) {
						foreach($activeLanguagesList as $key => $lang) {
							$translation = new LearningCourseCategory();
							$translation->idCategory = $currentId;
							$translation->lang_code = $key;
							$translation->translation = $_POST['LearningCourseCategory']['translation'][$key];
							$translation->save();
						}
					}

					// Let's manage the selected Node, But since the node can be selected from couple of places,
					// we must define from where the selection is coming
					if(!$isCentralLO){
						// Here we have the normal selection from the Course Category Management
						if (isset(Yii::app()->session['currentCourseNodeId'])) {
							$id = Yii::app()->session['currentCourseNodeId'];
							$root = LearningCourseCategoryTree::model()->findByPk($id);
							if (empty($root)) {
								//the node reference saved in the session may have been deleted and no more available ... fallback on the tree root node
								$root = LearningCourseCategoryTree::model()->roots()->find();
								Yii::app()->session['currentCourseNodeId'] = $root->getPrimaryKey();
							}
						} else {
							$root = LearningCourseCategoryTree::model()->roots()->find();
						}
					} else {
						// We can be sure that the selection of the current node, is from Central LO Repository
						if (isset(Yii::app()->session['currentCentralRepoNodeId'])) {
							$id = Yii::app()->session['currentCentralRepoNodeId'];
							$root = LearningCourseCategoryTree::model()->findByPk($id);
							if (empty($root)) {
								//the node reference saved in the session may have been deleted and no more available ... fallback on the tree root node
								$root = LearningCourseCategoryTree::model()->roots()->find();
								Yii::app()->session['currentCentralRepoNodeId'] = $root->getPrimaryKey();
							}
						} else {
							$root = LearningCourseCategoryTree::model()->roots()->find();
						}
					}

					$learningCourseCategoryTree = new LearningCourseCategoryTree();
					$learningCourseCategoryTree->idCategory = $currentId;
					if (empty($root)) {
						$learningCourseCategoryTree->saveNode();
					} else {
						$learningCourseCategoryTree->appendTo($root);
					}

					LearningCourseCategory::flushTreeCache();

					$parentId = $learningCourseCategoryTree->parent()->find()->idCategory;
					// Again let's save the parent id
					// but to do this we must know from where the selection is coming
					if(!$isCentralLO){
						Yii::app()->session['currentCourseNodeId'] = $parentId;
					} else {
						Yii::app()->session['currentCentralRepoNodeId'] = $parentId;
					}

					CoreRunningOperation::endOperation('course_categories_tree_change');

					$this->sendJSON(array(
						'redirect' => Yii::app()->createAbsoluteUrl('courseManagement/index'),
					));
				}
			}
		}

		//display dialog
		if ($rq->isAjaxRequest) {

			$content = '';

			$currentNode = Yii::app()->session['currentCourseNodeId'];
			if($isCentralLO){
				$currentNode = Yii::app()->session['currentCentralRepoNodeId'];
			}

			$params = array(
				'model' => $learningCourseCategory,
				'activeLanguagesList' => $activeLanguagesList,
				'currentCourseNodeId' => $currentNode,
				'html' => &$content
			);

			if ( Yii::app()->event->raise('BeforeCourseCategoryManagementCreateRender', new DEvent($this, $params)) ) {
				$content = $this->renderPartial('create', $params, true);
			}

			$this->sendJSON(array(
				'html' => $content,
			));
		} else {
			$this->renderPartial('create', array(
				'model' => $learningCourseCategory,
				'activeLanguagesList' => $activeLanguagesList
			));
		}
	}


	public function actionEdit($id) {

		$rq = Yii::app()->request;
		$learningCourseCategory = new LearningCourseCategory();
		$activeLanguagesList = CoreLangLanguage::getActiveLanguages();
		$lang_code = Settings::get('default_language', 'english');
		$activeLanguagesList[$lang_code] = $activeLanguagesList[$lang_code] . " (" . Yii::t('standard', '_DEFAULT_LANGUAGE') . ")";

		if (!empty($_POST['LearningCourseCategory'])) {
			if (!empty($_POST['LearningCourseCategory']['translation'])) {
				$allTranslationEmpty = true;
				foreach($_POST['LearningCourseCategory']['translation'] as $translation) {
					if (!empty($translation)) {
						$allTranslationEmpty = false;
					}
				}
				if ($allTranslationEmpty) {
					$learningCourseCategory->addError('translation', Yii::t('organization_chart', 'Node name can\'t be empty'));
				} elseif ($learningCourseCategory->validate()) {

					//--- check if the tree has changed while the client page was opened
					$treeGenerationTime = $rq->getParam('treeGenerationTime', false);
					if ($treeGenerationTime != false) {
						if (CoreRunningOperation::isBeforeLastOperationTime('course_categories_tree_change', $treeGenerationTime)) {
							echo CJSON::encode(array('update_tree' => true));
							Yii::app()->end();
						}
					}
					//if tree is ok then we can start our operation and continue with regular action
					if (!CoreRunningOperation::startOperation('course_categories_tree_change')) {
						// We cannot start operating on the tree structure because someone else is already doing it and we must not
						// interfere to avoid conflicts/data corruption
						$response = new AjaxResult();
						$response->setStatus(false)->setData(array('update_tree' => true))->toJSON();
						Yii::app()->end();
					}
					//---

					$trans = Yii::app()->db->beginTransaction();
					$output = array();
					try {
						if (!empty($activeLanguagesList)) {
							$translations = LearningCourseCategory::model()->findAllByAttributes(array(
								'idCategory' => $id,
							), array('condition' => 'lang_code IN (\'' . implode('\',\'', array_keys($activeLanguagesList)) . '\')'));
							$translationsByKey = array();
							if (!empty($translations)) {
								foreach ($translations as $value) {
									$translationsByKey[$value->lang_code] = $value;
								}
							}
							foreach ($activeLanguagesList as $key => $lang) {
								if (empty($translationsByKey[$key])) {
									$translationsByKey[$key] = new LearningCourseCategory();
									$translationsByKey[$key]->idCategory = $id;
									$translationsByKey[$key]->lang_code = $key;
								}
								$translationsByKey[$key]->translation = $_POST['LearningCourseCategory']['translation'][$key];
								$translationsByKey[$key]->save();
							}
							LearningCourseCategory::flushTreeCache();
						}
						$trans->commit();
						CoreRunningOperation::endOperation('course_categories_tree_change');
					} catch (CException $e) {
						//$output['error'] = $e->getMessage();
						$trans->rollback();
						CoreRunningOperation::abortOperation('course_categories_tree_change');
					}
					$this->sendJSON($output);
				}
			}
		}

		if (!empty($_POST['LearningCourseCategory']['translation'])) {
			$translationsList = $_POST['LearningCourseCategory']['translation'];
		} else {
			$translations = LearningCourseCategory::model()->findAllByAttributes(array('idCategory' => $id));
			$translationsList = CHtml::listData($translations, 'lang_code', 'translation');
		}

		if (!empty($activeLanguagesList)) {
			foreach($activeLanguagesList as $key => $value) {
				if (!empty($translationsList[$key])) {
					$activeLanguagesList[$key] .= ' *';
				}
			}
		}

		if ($rq->isAjaxRequest) {
			$content = $this->renderPartial('edit', array(
				'model' => $learningCourseCategory,
				'translationsList' => $translationsList,
				'activeLanguagesList' => $activeLanguagesList
			), true);
			$this->sendJSON(array(
				'html' => $content,
			));
		} else {
			$this->renderPartial('edit', array(
				'model' => $learningCourseCategory,
				'translationsList' => $translationsList,
				'activeLanguagesList' => $activeLanguagesList
			));
		}
	}


	public function actionMove($id) {

		$rq = Yii::app()->request;
		$actionConfirmed = !empty($_POST['LearningCourseCategoryTree']);

		//--- check if the tree has changed while the client page was opened
		//NOTE: the tree is used in dialog too and needs to be up to date
		$treeGenerationTime = $rq->getParam('treeGenerationTime', false);
		if ($treeGenerationTime != false) {
			if (CoreRunningOperation::isBeforeLastOperationTime('course_categories_tree_change', $treeGenerationTime)) {
				echo CJSON::encode(array('update_tree' => true));
				Yii::app()->end();
			}
		}

		//if tree is ok then we can start our operation and continue with regular action
		if ($actionConfirmed) {
			if (!CoreRunningOperation::startOperation('course_categories_tree_change')) {
				// We cannot start operating on the tree structure because someone else is already doing it and we must not
				// interfere to avoid conflicts/data corruption
				$response = new AjaxResult();
				$response->setStatus(false)->setData(array('update_tree' => true))->toJSON();
				Yii::app()->end();
			}
			//---
		}

		$learningCourseCategory = LearningCourseCategoryTree::model()->findByAttributes(array('idCategory' => $id));

		$default_with = array(
			'coursesCategory' => array(
				'on' => 'coursesCategory.lang_code = :lang',
				'params' => array('lang' => Settings::get('defaultLanguage', 'english'))
			)
		);
		$defaultCourseTree = LearningCourseCategoryTree::model()->with($default_with)->findAll(array('order' => 'coursesCategory.translation'));
		$defaultCourseTree = LearningCourseCategoryTree::arrangeTreeAlphabetical($defaultCourseTree);
		$defaultCourseTreeTranslation = array();
		foreach($defaultCourseTree as $category) {
			$defaultCourseTreeTranslation[$category->idCategory] = $category->coursesCategory->translation;
		}

		$lang = CoreLangLanguage::model()->findByAttributes(array('lang_browsercode' => Yii::app()->getLanguage()));
		$with = array(
			'coursesCategory' => array(
				'on' => 'coursesCategory.lang_code = :lang',
				'params' => array('lang' => $lang->lang_code)
			)
		);
		$fullOrgChartTree     = LearningCourseCategoryTree::model()->with($with)->findAll(array(
			'order' => 'coursesCategory.translation'
		));

		$fullOrgChartTree = LearningCourseCategoryTree::arrangeTreeAlphabetical($fullOrgChartTree);
		$fullOrgChartTreeList = CHtml::listData($fullOrgChartTree, 'idOrg', 'coursesCategory.translation');

		//do effective action on tree structure
		if ($actionConfirmed) {

			$trans = Yii::app()->db->beginTransaction();
			$output = array();
			try {
				$idCategory = 1;
				if (!empty($_POST['LearningCourseCategoryTree']['idCategory'])) {
					foreach ($_POST['LearningCourseCategoryTree']['idCategory'] as $postIdCategory) {
						if ($postIdCategory > 0) {
							$idCategory = $postIdCategory;
						}
					}
				}
				$newParent = LearningCourseCategoryTree::model()->findByAttributes(array('idCategory' => $idCategory));
				$learningCourseCategory->moveAsFirst($newParent);

				Yii::app()->session['currentCourseNodeId'] = $idCategory;
				Yii::app()->session['currentCentralRepoNodeId'] = $idCategory;

				LearningCourseCategory::flushTreeCache();
				$trans->commit();
				CoreRunningOperation::endOperation('course_categories_tree_change');

			} catch (CException $e) {

				//$output['error'] = $e->getMessage();
				$trans->rollback();
				CoreRunningOperation::abortOperation('course_categories_tree_change');
			}

			$this->sendJSON($output);
		}

		//display dialog
		if ($rq->isAjaxRequest) {
			$currentNode = LearningCourseCategoryTree::model()->findByPk($id);
			$nodeParents = $currentNode->ancestors()->findAll();
			$allChildrenNodesIds = CHtml::listData($currentNode->descendants()->findAll(), 'idCategory', 'idCategory');
			$allChildrenNodesIds = array_merge(array($id => $id), $allChildrenNodesIds);

			$contentParents = $this->renderPartial('_nodeParents', array(
				'currentNode' => $currentNode,
				'nodeParents' => $nodeParents,
			), true);

			$content = $this->renderPartial('move', array(
				'model' => $learningCourseCategory,
				'contentParents' => $contentParents,
				'fullOrgChartTree' => $fullOrgChartTree,
				'defaultCourseTreeTranslation' => $defaultCourseTreeTranslation,
				'fullOrgChartTreeList' => $fullOrgChartTreeList,
				'allChildrenNodesIds' => $allChildrenNodesIds,
			), true);
			
			LearningCourseCategory::flushTreeCache();
			
			$this->sendJSON(array(
				'html' => $content,
			));
		}
		
		LearningCourseCategory::flushTreeCache();
		
		exit;
	}


	public function actionDelete($id) {
		$rq = Yii::app()->request;
		$lang = CoreLangLanguage::model()->findByAttributes(array('lang_browsercode' => Yii::app()->getLanguage()));
		$with = array(
			'coursesCategory' => array(
				'on' => 'coursesCategory.lang_code = :lang',
				'params' => array('lang' => $lang->lang_code)
			)
		);
		$currentNode = LearningCourseCategoryTree::model()->with($with)->findByPk($id);

		if (empty($currentNode)) {
			$this->sendJSON(array());
		}

		//action confirmed
		if (!empty($_POST['LearningCourseCategoryTree'])) {
			if ($_POST['LearningCourseCategoryTree']['confirm']) {
				if ($currentNode->isLeaf()) {
					//--- check if the tree has changed while the client page was opened
					$treeGenerationTime = $rq->getParam('treeGenerationTime', false);
					if ($treeGenerationTime != false) {
						if (CoreRunningOperation::isBeforeLastOperationTime('course_categories_tree_change', $treeGenerationTime)) {
							echo CJSON::encode(array('update_tree' => true));
							Yii::app()->end();
						}
					}
					//if tree is ok then we can start our operation and continue with regular action
					if (!CoreRunningOperation::startOperation('course_categories_tree_change')) {
						// We cannot start operating on the tree structure because someone else is already doing it and we must not
						// interfere to avoid conflicts/data corruption
						$response = new AjaxResult();
						$response->setStatus(false)->setData(array('update_tree' => true))->toJSON();
						Yii::app()->end();
					}
					//---
					$trans = Yii::app()->db->beginTransaction();
					$output = array();
					try {
						LearningCourseCategoryTree::model()->removeNode($id);
						LearningCourseCategory::flushTreeCache();
						$trans->commit();
						CoreRunningOperation::endOperation('course_categories_tree_change');
					} catch (CException $e) {
						//$output['error'] = $e->getMessage();
						$trans->rollback();
						CoreRunningOperation::abortOperation('course_categories_tree_change');
					}
					$this->sendJSON($output);
				}
			}
		}

		//display dialog
		if ($rq->isAjaxRequest) {
			$content = $this->renderPartial('deleteCategory', array(
				'model' => new LearningCourseCategoryTree(),
				'currentNode' => $currentNode,
			), true);
			$this->sendJSON(array(
				'html' => $content,
			));
		}
		Yii::app()->end();
	}


	public function actionGetNodeParents($selectedId, $currentId) {

		$currentNode = LearningCourseCategoryTree::model()->findByPk($selectedId);
		$movingNode = LearningCourseCategoryTree::model()->findByPk($currentId);

		$default_with = array(
			'coursesCategory' => array(
				'on' => 'coursesCategory.lang_code = :lang',
				'params' => array('lang' => Settings::get('defaultLanguage', 'english'))
			)
		);
		$defaultCourseTree     = LearningCourseCategoryTree::model()->with($default_with)->findAll(array('order' => 'iLeft'));
		$defaultCourseTreeTranslation = array();
		foreach ($defaultCourseTree as $category) {
			$defaultCourseTreeTranslation[$category->idCategory] = $category->coursesCategory->translation;
		}
		if (!empty($currentNode)) {

			$breadcrumbsNodes = $currentNode->ancestors()->findAll();
			$breadcrumbs      = '';
			$breadcrumbs .= '<ul class="clearfix">';
			if (!empty($breadcrumbsNodes)) {
				foreach ($breadcrumbsNodes as $breadcrumbsNode) {

					$node_name = $breadcrumbsNode->coursesCategoryTranslated->translation;
					if (!$node_name) $node_name = $defaultCourseTreeTranslation[$breadcrumbsNode->idCategory];

					$breadcrumbs .= '<li>&nbsp;' . $node_name . '&nbsp;</li>';
				}
				$node_name = $currentNode->coursesCategoryTranslated->translation;
				if (!$node_name) $node_name = $defaultCourseTreeTranslation[$currentNode->idCategory];
				$breadcrumbs .= '<li>&nbsp;' .$node_name . '&nbsp;</li>';
			}

			$node_name = $movingNode->coursesCategoryTranslated->translation;
			if (!$node_name) $node_name = $defaultCourseTreeTranslation[$movingNode->idCategory];
			$breadcrumbs .= '<li class="last-node">&nbsp;' .$node_name . '</li>';
			$breadcrumbs .= '</ul>';

			$this->sendJSON(array(
				'content' => $breadcrumbs,
			));
			Yii::app()->end();
		}

	}

	public function actionSettings($id) {
		$learningCourseCategory = LearningCourseCategoryTree::model()->findByPk($id);
		$content = '';
		if ($learningCourseCategory) {
			if (!empty($_POST['LearningCourseCategoryTree'])) {
				$learningCourseCategory->soft_deadline = (!isset($_POST['enable_soft_deadline_option'])) ? null : $_POST['LearningCourseCategoryTree']['soft_deadline'];
				$learningCourseCategory->saveNode();

				LearningCourseCategory::flushTreeCache();
				
				echo '<a class="auto-close"></a>';
				Yii::app()->end();
			}

			$content = $this->renderPartial('settings', array('model' => $learningCourseCategory), true);
		}

		echo $content;
		Yii::app()->end();
	}
}
