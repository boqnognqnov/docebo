<?php

class Test1Controller extends Controller {

	public function actionU2() {
	
		$cs = Yii::app()->getClientScript();
		$themeUrl = Yii::app()->theme->baseUrl;
		$cs->registerScriptFile($themeUrl . '/js/jwizard.js');
	
	
		// Register Usersselector JS/CSS; this is required for IE8/9
		$cs->registerCssFile($themeUrl . '/css/admin.css');
		$cs->registerCssFile($themeUrl . '/css/usersselector.css');
		$cs->registerScriptFile($themeUrl . '/js/usersselector.js');
	
		$this->render('u2');
	}
		
}