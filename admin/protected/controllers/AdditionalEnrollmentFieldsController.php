<?php

class AdditionalEnrollmentFieldsController extends AdminController {

    public $userCanAdminCourse = false;
    public $course_id = null;
    public $userIsSubscribed = false;

    public function init() {

        parent::init();

        JsTrans::addCategories("user_management");

        $this->course_id = Yii::app()->request->getParam("course_id", FALSE);

        $model = LearningCourseuser::model()->findByAttributes(array("idUser" => Yii::app()->user->id, "idCourse" => $this->course_id));
		if ($model) {
			$levelInCourse = $model->level;
			$this->userIsSubscribed = true;

			// Check if this user is not an admin and can enter course
			if (Yii::app()->user->getIsUser()) {
				$canEnterCourse = $model->canEnterCourse();
				if (!$canEnterCourse['can']) {
					Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
					$this->redirect(array('/site'), TRUE);
				}
			}
		} else {
			$levelInCourse = 0;
			$this->userIsSubscribed = false;
			// Only subscribed users can play this course (with the exception of admins)
			if (Yii::app()->user->getIsUser()) {
				Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
				$this->redirect(array('/site'), TRUE);
			}
		}

		// Is this a power user and the course was assigned to him?
		$pUserCourseAssigned = CoreUserPuCourse::model()->findByAttributes(array(
			'puser_id' => Yii::app()->user->id,
			'course_id' => $this->course_id
		));

        $this->userCanAdminCourse = ($levelInCourse && $levelInCourse > LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT
									&& $levelInCourse != LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR && $levelInCourse != LearningCourseuser::$USER_SUBSCR_LEVEL_COACH)
									|| Yii::app()->user->isGodAdmin
									|| ($pUserCourseAssigned && Yii::app()->user->checkAccess('/lms/admin/course/mod'));
    }

    public function filters() {
        return array(
            'accessControl' => 'accessControl',
        );
    }

    /***
     * @return array
     */
    public function accessRules() {
        $res = array();

        $res[] = array(
            'allow',
            'actions' => array('index', 'fieldsAutocomplete', 'createFieldModal', 'axCourseDescWidgetSettings', 'editField', 'fcbkAutocomplete'),
            'expression' => 'Yii::app()->controller->userCanAdminCourse',

        );

        $res[] = array(
            'allow',
            'roles' => array('/framework/admin/field_manager/add'),
            'actions' => array('index', 'createFieldModal', 'create', 'editField', 'createDropdown', 'editDropdown', 'orderEnrollmentFields', 'saveDropdownSingleLanguage'),
        );

        $res[] = array(
            'allow',
            'roles' => array('/framework/admin/field_manager/del'),
            'actions' => array('deleteField'),
        );

        $res[] = array(
            'allow',
            'roles' => array('/framework/admin/field_manager/mod'),
            'actions' => array('index', 'createFieldModal', 'editField'),
        );

        $res[] = array(
            'deny',
            // deny all
            'users' => array('*'),
            'deniedCallback' => array($this, 'adminHomepageRedirect'),
        );

        return $res;
    }


    /**
     *
     */
    public function actionIndex() {

        $cs = Yii::app()->clientScript;

        DatePickerHelper::registerAssets();
        $cs->registerCoreScript('jquery.ui');
        $cs->registerScriptFile(Yii::app()->baseUrl.'/js/dropdown_enrollment_custom_field.js', CClientScript::POS_END);

        $coreField = LearningEnrollmentField::model();
        $fieldTypes = LearningEnrollmentField::getEnrollmentFieldsTypes(); // for courses
		
		
        $event = new DEvent();
        Yii::app()->event->raise('BeforeBuildingCourseFieldTypes', $event);


        $this->render('index', array(
            'coreField' => $coreField,
            'fieldTypes' => $fieldTypes,
        ));

    }

    protected function gridRenderTranslation($data, $index)
    {
        return (empty($data->translation) && !empty($data->enTranslation)) ? $data->enTranslation[0]->translation : $data->translation;
    }

    public function actionFieldsAutocomplete() {
        $query = strtolower($_REQUEST['data']['query']);
		$lang = Yii::app()->getLanguage();
	
		$criteria = new CDbCriteria(array('order' => 'sequence asc'));
		$criteria->select = ["type", "sequence", "translation->'$.\"" . $lang . "\"' as translation"];
		$criteria->condition = "JSON_SEARCH(translation, 'one', :query) IS NOT NULL AND JSON_EXTRACT(translation, '$.\"" . $lang . "\"') > 1";
		$criteria->params = [
			':query' => '%'.$query.'%'
		];
		
		$response = array_values( CHtml::listData( LearningEnrollmentField::model()->findAll($criteria), 'translation', 'translation' ) );
		

        $this->sendJSON(array('options' => $response));
    }

    public function actionFcbkAutocomplete(){
    	$request = Yii::app()->request;

    	if($request->isAjaxRequest){
    		$tag = $request->getParam('tag', null);

    		if(!$tag){
    			return;
    		}

    		$lang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

    		$command = Yii::app()->db->createCommand();
    		$command->select('translation, lcf.id_field');
    		$command->join('learning_course_field_translation lcft', "lcft.id_field = lcf.id_field AND lcft.lang_code = :lang AND translation LIKE :tag AND invisible_to_user = 1");
    		$command->from(LearningCourseField::model()->tableName() . ' lcf');
    		$command->params[':lang'] = $lang;
    		$command->params[':tag'] = '%' . $tag . '%';
    		$fields = $command->queryAll();

    		$result = array();
    		if($fields){
    			foreach ($fields as $field){
    				$result[] = array(
    						'key' => $field['id_field'],
    						'value' => $field['translation']
    				);
    			}
    		}

    		echo CJSON::encode($result);
    	}
    }

    public function actionDeleteField( $id )
    {
        $confirmYes = Yii::app()->request->getParam('confirm_yes', null);
        $lang = Yii::app()->getLanguage();
        $enrollField = LearningEnrollmentField::model()->findByPk($id);
        $enrollFieldTranslations = json_decode($enrollField->translation, true);
        $fieldTitle = $enrollFieldTranslations[$lang];
       
        if ( empty( $enrollField)) {
            $this->sendJSON( array());
        }

        if ( $confirmYes ) {
            if ($enrollField->type == LearningEnrollmentField::TYPE_FIELD_DROPDOWN) {
                LearningEnrollmentFieldDropdown::model()->deleteAll(array(
                    'condition' => "`id_field` = ':id_field'",
                    'params' => array(':id_field' => $enrollField->id ),
                ));
            }

            Yii::app()->db->createCommand("UPDATE learning_courseuser SET enrollment_fields=JSON_REMOVE(enrollment_fields, '$.\"".$enrollField->id."\"') WHERE JSON_EXTRACT(enrollment_fields, '$.\"".$enrollField->id."\"')  is not null")->execute();
            Yii::app()->db->createCommand("UPDATE learning_courseuser SET enrollment_fields=NULL WHERE enrollment_fields = '{}'")->execute();

            $enrollField->delete();
            self::reIndexFieldSequences();

            echo '<a class="auto-close"></a>';
        }

        if ( Yii::app()->request->isAjaxRequest && $confirmYes != 'yes') {
            $this->renderPartial('deleteModal', array(
                'enrollmentFieldTitle' => $fieldTitle,
            ),false);
            Yii::app()->end();
        }
        Yii::app()->end();
    }

    private static function reIndexFieldSequences()
    {
        $allEnrollmentFields = LearningEnrollmentField::model()->findAll(array('order' => 'sequence ASC'));

        $counter = 1;
        foreach ($allEnrollmentFields as $oneField) {
            $oneField->sequence = $counter;
            $oneField->save();
            $counter++;
        }
    }

    
    /**
     * Create Custom field
     */
    public function actionCreateFieldModal() {
        $this->forward("editField");
    }

    /**
     * Create/Edit Enrollment Custom field
     */
    public function actionEditField() {
    
        $id             = Yii::app()->request->getParam("id", false);
        $type           = Yii::app()->request->getParam("type", false);
        
        $fieldModel     = $this->getFieldObject($id, $type);
        $fieldClass     = get_class($fieldModel);
        
        // Save in learning_enrollment_fields
        if (!empty($_POST['translation']) && !empty($_POST['translation'][Yii::app()->getLanguage()])) {
            
            $translationsArray = array_filter($_POST['translation']);
            	
            $fieldModel->translation = json_encode($translationsArray);
            if ($_POST[$fieldClass]['course'] && $_POST['courses']) {
                $fieldModel->courses = str_replace('"','',json_encode(array($_POST['courses'])));
            } else {
                $fieldModel->courses = null;
            }
    
            $fieldModel->mandatory          = $_POST[$fieldClass]['mandatory'];
            $fieldModel->visible_to_user    = $_POST[$fieldClass]['visible_to_user'];
            $fieldModel->settings           = isset($_POST[$fieldClass]['settings']) ? json_encode($_POST[$fieldClass]['settings']) : "{}";
            	
            $success = $fieldModel->save();
            	
            $this->sendJSON(array(
                'success' => $success,
            ));
        }
    
        $activeLanguagesList    = CoreLangLanguage::getActiveLanguages();
        $lang_code = Settings::get('default_language', 'english');
        $activeLanguagesList[$lang_code] = $activeLanguagesList[$lang_code] . " (" . Yii::t('standard', '_DEFAULT_LANGUAGE') . ")";
        $modelDropdown = new LearningEnrollmentFieldDropdown();
    
        $html = $this->renderPartial('editModal',
            array(
                'modelField'                => $fieldModel,
                'modelDropdown'             => $modelDropdown,
                'activeLanguagesList'       => $activeLanguagesList,
                'categoriesDropdown'		=> LearningCourseCategory::getCategoriesList(CoreUser::getDefaultLangCode()),
                'fieldType'                 => $fieldModel->type
            ), true);
    
        $this->sendJSON(array(
            'html' => $html,
        ));
    }
    
	
	
	
	
	
	
	
//    public function actionEditField( $id )
//    {
//        $lang_code = Settings::get('default_language', 'english');
//        $activeLanguagesList = CoreLangLanguage::getActiveLanguages();
//        $activeLanguagesList[$lang_code] = $activeLanguagesList[$lang_code] . " (" . Yii::t('standard', '_DEFAULT_LANGUAGE') . ")";
//        $modelTranslation   = new LearningCourseFieldTranslation();
//        $modelTranslation->lang_code = Settings::get('default_language', 'english');
//
//        if ( $id ) {
//            $fieldTranslations = LearningCourseFieldTranslation::getFieldTranslations( $id );
//            $baseField = LearningCourseField::model()->findByPk( $id );
//            if ( isset($_REQUEST['LearningCourseField']['invisible_to_user']) )
//            {
//                $baseField->invisible_to_user = $_REQUEST['LearningCourseField']['invisible_to_user'];
//                $baseField->settings            = CJSON::encode(is_array($_POST['LearningCourseField']['settings']) ? $_POST['LearningCourseField']['settings'] : array());
//                $baseField->save();
//            }
//        }
//
//        if ( !empty($_POST['LearningCourseFieldTranslation']) && !empty($_POST['LearningCourseField'])) {
//            $platformDefaultLang = Settings::get('default_language', 'english');
//            if (empty($_POST['LearningCourseFieldTranslation']['translation'][$platformDefaultLang])) {
//
//                $fieldTranslations->addError('translation', Yii::t('user_management', '_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED'));
//
//            } else {
//                $success = false;
//
//                $transaction = Yii::app()->db->beginTransaction();
//
//                foreach ( $activeLanguagesList as $k => $l) {
//                    $translationModel = LearningCourseFieldTranslation::model()->findByAttributes(array(
//                        'id_field'  => $id,
//                        'lang_code' => $k
//                    ));
//
//                    if(is_null($translationModel))
//					{
//                        $translationModel = new LearningCourseFieldTranslation;
//                        $translationModel->lang_code = $k;
//						$translationModel->id_field = $id;
//                    }
//
//                    $translationModel->translation = $_POST['LearningCourseFieldTranslation']['translation'][$k];
//                    $success = $translationModel->save();
//                    if (!$success) {
//                        Yii::log(print_r($translationModel->getErrors(), true), CLogger::LEVEL_ERROR);
//                        $result['success'] = false;
//                        break;
//                    }
//                }
//
//                if ($success) {
//                    $transaction->commit();
//                } else {
//                    $transaction->rollback();
//                }
//
//                $this->sendJSON(array(
//                    'success' => $success,
//                ));
//            }
//        }
//
//        $html = $this->renderPartial('editModal',
//            array(
//                'modelField'            => $baseField,
//                'activeLanguagesList'   => $activeLanguagesList,
//                'translationsList'      => $fieldTranslations,
//                'modelTranslation'      => $modelTranslation
//            ), true );
//
//        $this->sendJSON(array(
//            'html' => $html,
//        ));
//    }

	public function actionCreateDropdown() {
		$transaction = Yii::app()->db->beginTransaction();
		$success = false;
		
		if (!empty($_POST['LearningCourseFieldTranslation']) && !empty($_POST['LearningCourseFieldTranslation']['translation'][Yii::app()->getLanguage()])) {
			$translationOptionsList = $_POST['LearningCourseFieldTranslation']['translation'];
			
			$translationList = $_POST['LearningEnrollmentField']['translation'];

			$translationsArray = array();
			foreach ($translationList as $key => $value) {
				if ( !empty($value['field']) ) {
					$translationsArray[$key] = $value['field'];
				}
			}
			
			
			$baseField = new LearningEnrollmentField();
			$baseField->type = LearningEnrollmentField::TYPE_FIELD_DROPDOWN;
			$baseField->visible_to_user = $_POST['LearningCourseField']['visible_to_user'];
			$baseField->translation = json_encode($translationsArray);
			if ($_POST['LearningEnrollmentField']['course'] && $_POST['courses']) {
				$baseField->courses = json_encode(array(intval($_POST['courses'])));
			}
			$baseField->sequence = LearningEnrollmentField::model()->getNextSequence();
			
			if ($baseField->save()) {
				$traslationsDropdownFinal = array();
				$optionsCount = 0;
				foreach ($translationOptionsList as $language => $value) {
					if (!empty($value['options'])) {
						if(count($value['options']['translation']) - 1 > $optionsCount){
							$optionsCount = count($value['options']['translation']) - 1;
						}
					}
				}



				foreach ($translationOptionsList as $language => $value) {
					if (!empty($value['options'])) {
						for($i = 0; $i < $optionsCount; $i++){
							if(!empty($value['options']['translation'][$i])){
								$traslationsDropdownFinal[$i][$language] = $value['options']['translation'][$i];
							}
						}
						
					}
				}
				
				foreach ($traslationsDropdownFinal as $value) {
					$dropdownModel = new LearningEnrollmentFieldDropdown();
					$dropdownModel->id_field = $baseField->id;
					$dropdownModel->translation = json_encode($value);
					if($dropdownModel->save()){
						$success = true;
					}
				}
				
			}
			
			
		}


		if ($success) {
			$transaction->commit();
		} else {
			$transaction->rollback();
		}

		$this->sendJSON(array(
			'success' => $success,
		));
	}


	public function actionEditDropdown($id) {
		$activeLanguagesList = CoreLangLanguage::getActiveLanguages();
		$lang_code = Settings::get('default_language', 'english');
		$activeLanguagesList[$lang_code] = $activeLanguagesList[$lang_code] . " (" . Yii::t('standard', '_DEFAULT_LANGUAGE') . ")";
        $modelDropdown = new LearningEnrollmentFieldDropdown();
		
		$transaction = Yii::app()->db->beginTransaction();
		$success = false;
		
		$baseField = LearningEnrollmentField::model()->findByPk($id);
		
		if (!empty($_POST['LearningEnrollmentField']['translation']) && !empty($_POST['LearningEnrollmentField']['translation'][Yii::app()->getLanguage()])) {
			$translationOptionsList = $_POST['LearningCourseFieldTranslation']['translation'];
			$translationsArray = $_POST['LearningEnrollmentField']['translation'];
			
			$translationsArrayFinal = array();
			foreach ($translationsArray as $key => $value) {
				if ( !empty($value['field']) ) {
					$translationsArrayFinal[$key] = $value['field'];
				}
			}

			$baseField->visible_to_user = $_POST['LearningEnrollmentField']['visible_to_user'];
			$baseField->mandatory = $_POST['LearningEnrollmentField']['mandatory'];
			$baseField->translation = json_encode($translationsArrayFinal);
			if ($_POST['LearningEnrollmentField']['course'] && $_POST['courses']) {
				$baseField->courses = json_encode(array($_POST['courses']));
			} else {
				$baseField->courses = null;
			}
			
			if ($baseField->save()) {
				$success = true;
				
				LearningEnrollmentFieldDropdown::model()->deleteAll(
						"id_field = :id",
						array(':id' => $id)
				);
				
				
				$traslationsDropdownFinal = array();
				$optionsCount = 0;
				foreach ($translationOptionsList as $language => $value) {
					if (!empty($value['options'])) {
						if(count($value['options']['translation']) - 1 > $optionsCount){
							$optionsCount = count($value['options']['translation']) - 1;
						}
					}
				}


				foreach ($translationOptionsList as $language => $value) {
					if (!empty($value['options'])) {
						for($i = 0; $i < $optionsCount; $i++){
							if(!empty($value['options']['translation'][$i])){
								$traslationsDropdownFinal[$i][$language] = $value['options']['translation'][$i];
							}
						}
						
					}
				}
				
				foreach ($traslationsDropdownFinal as $value) {
					$dropdownModel = new LearningEnrollmentFieldDropdown();
					$dropdownModel->id_field = $baseField->id;
					$dropdownModel->translation = json_encode($value);
					if($dropdownModel->save()){
						$success = true;
					}
				}
			}
		}


		if ($success) {
			$transaction->commit();
		} else {
			$transaction->rollback();
		}

		
		$html = $this->renderPartial('editModal',
            array(
                'modelField'                => $baseField,
                'modelDropdown'             => $modelDropdown,
                'activeLanguagesList'       => $activeLanguagesList,
				'categoriesDropdown'		=> LearningCourseCategory::getCategoriesList(CoreUser::getDefaultLangCode()),
				'fieldType'					=> $baseField->type
            ), true);
		
		
		$this->sendJSON(array(
			'html' => $html,
		));
	}
	
    public function actionOrderEnrollmentFields() {
        $ids = $_POST['ids'];
        $command = Yii::app()->db->createCommand();

        if (!empty($ids)) {
            foreach ($ids as $weight => $id)
            {
                $tmp = LearningEnrollmentField::model()->findByPk($id);
                $id_field = $tmp->id;

                $command->update(LearningEnrollmentField::model()->tableName(), array('sequence' => $weight), 'id = :id', array(':id' => $id_field));
            }

            $this->sendJSON(array('success' => true));
        } else {
            $this->sendJSON(array('success' => false));
        }
    }

    //this is just a dummy function for the moment
    public function actionSaveDropdownSingleLanguage() {
        $this->sendJSON(array('success' => true));
    }

    public function actionAxCourseDescWidgetSettings(){
        $block_id = Yii::app()->request->getParam('block_id', false);
        $course_id = Yii::app()->request->getParam('course_id', false);
        $block = PlayerBaseblock::model()->findByPk($block_id);

        $courseDescParams = CJSON::decode($block->params);

        $showDesc = $courseDescParams['show_desc'];
        $courseAdditionaFileds = $courseDescParams['additional_fields'];
        $error = null;

        if($_POST['show_desc'] || $_POST['additional_fields']){
            $showDesc = $_POST['show_desc'];
            $courseAdditionaFileds = $_POST['additional_fields'];

            $courseDescParams['show_desc'] = $showDesc;
            $courseDescParams['additional_fields'] = $courseAdditionaFileds;

            $block->params = CJSON::encode($courseDescParams);
            $block->save(false);

            echo "<a class='auto-close success'></a>";
            Yii::app()->end();
        }

        if(empty($_POST['show_desc']) && empty($_POST['additional_fields']) && $_POST['next_button']){
            $error = Yii::t('course', 'Please, select at least one additional field, if you want to hide the description!');
        }

        $this->renderPartial('_course_desc_widget_settings', array(
            'showDesc' => $showDesc,
            'additionalFields' => $courseAdditionaFileds,
            'course_id' => $course_id,
            'error' => $error
        ));
    }
    
    /**
     * Load/Create proper Enrollment Fields object, based eithr on incoming ID or TYPE
     * 
     * @param string $id 
     * @param string $type  @see LearningEnrollmentField TYPE constants
     * 
     * @return NULL|LearningEnrollmentField|LearningEnrollmentFieldIframe
     */
    private function getFieldObject($id=false, $type=false) {
        
        $model = null;
        
        if ($id !== false) {
            $model = LearningEnrollmentField::model()->findByPk((int) $id);
            if ((int) $model->type == LearningEnrollmentField::TYPE_FIELD_IFRAME) {
                $model = LearningEnrollmentFieldIframe::model()->findByPk((int) $id);
            }
        }
        else if ($type !== false) {
            switch ((int) $type) {
                case LearningEnrollmentField::TYPE_FIELD_IFRAME:
                    $model = new LearningEnrollmentFieldIframe();
                    break;
                default:
                    $model = new LearningEnrollmentField();
                    break;
            }
            $model->type = $type;
        }
        
        return $model;
        
    }
    

}