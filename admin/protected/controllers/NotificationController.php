<?php

class NotificationController extends AdminController {

	const STEP_INITIAL = 'step_initial';
	const STEP_BASE = 'step_base';
	const STEP_SCHEDULE = 'step_schedule';
	const STEP_UNI_SELECTOR = 'uni-selector';
	const STEP_USER_FILTER = 'step_user_filter';
	const STEP_COURSE_ASSOC = 'step_course_assoc';
	const STEP_RECIPIENTS = 'step_recipients';
	const STEP_SAVE = 'step_save';

	public $layout = '//layouts/adm';

	/**
	 * Keep the notification ID we are dealing with during this call. Set in beforeAction()
	 * @var integer
	 */
	protected $idNot;

	/**
	 * Holds controller-wide notification type. Set in beforeAction()
	 * @var array
	 */
	protected $notType;

	/**
	 * Holds controller-wide notification descriptor. Set in beforeAction()
	 * @var string
	 */
	protected $notInfo;

	public function beforeAction($action) {

		// Resolve Notification ID, if any
		$this->idNot = Yii::app()->request->getParam("id", false);
		if (!$this->idNot) {
			// This may come from users/courses selectors
			$this->idNot = isset($_REQUEST['idGeneral']) ? $_REQUEST['idGeneral'] : false;
		}

		// Get the current model notification, if any
		if ($this->idNot) {
			$model = CoreNotification::model()->findByPk($this->idNot);
			$this->notType = $model->type;
		}

		// Maybe we are in Wizard mode? Check incoming Wizard data and override the notification type
		$this->notType = isset($_REQUEST['Wizard']['edit-notification-form']['CoreNotification']['type']) ? $_REQUEST['Wizard']['edit-notification-form']['CoreNotification']['type'] : $this->notType;

		// Collect notification descriptor
		if ($this->notType) {
			$this->notInfo = CoreNotification::getNotificationInfo($this->notType);
		}

		return parent::beforeAction($action);
	}

	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		$res = array();

		$res[] = array(
			'allow',
			'roles' => array('/framework/admin/setting/view'),
		);

		$res[] = array(
			'deny',
			// deny all
			'users' => array('*'),
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);

		return $res;
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller::init()
	 */
	public function init() {
		$this->registerResources();
		Yii::app()->tinymce->init();
		JsTrans::addCategories('notifications');
		return parent::init();
	}

	/**
	 * main page (notifications management)
	 */
	public function actionIndex() {

		$model = new CoreNotification();
		$searchParams = array(
			'code' => Yii::app()->request->getParam('notification-search-input', null),
			'type' => Yii::app()->request->getParam('notification-type-select', null)
		);
		if ($searchParams['type'] === '0')
			$searchParams['type'] = null;
		$model->attributes = $searchParams;

		$ft = new FancyTree();
		Yii::app()->getClientScript()->registerCssFile($ft->getAssetsUrl() . '/skin-docebo/ui.fancytree.css');

		if (!Yii::app()->request->isAjaxRequest) {
			// Register UserSelector JS
			UsersSelector::registerClientScripts();

			$cs = Yii::app()->getClientScript();
			$themeUrl = Yii::app()->theme->baseUrl;
			$cs->registerCssFile($themeUrl . '/css/notifications.css');
		}

		//load some specific css for editor transports tabs
		$activeTransports = CoreNotificationTransport::getActiveTransportsList();
		if (is_array($activeTransports)) {
			foreach ($activeTransports as $activeTransport) {
				$managerObject = $activeTransport['editor_class'];
				if (!empty($managerObject)) {
					$managerObject->loadEditorAssets();
				}
			}
		}

		$notifDescriptors = CoreNotification::getNotificationsDescriptors();
		$notifyTypes = array();
		$notificationGroups = CoreNotification::groupsPerType();
		$populatedNotificationGroups = $this->getPopulatedNotificationGroups($notificationGroups);
		foreach ($notifDescriptors as $type => $descriptor) {
			if ($descriptor['group']) {
				$group = $descriptor['group'];
			} else {
				$group = isset($notificationGroups[$type]) ? CoreNotification::groupTranslation($notificationGroups[$type]) : Yii::t('setup', 'Other');
			}

			if (array_key_exists($type, $populatedNotificationGroups)) {
				// Uncomment the following line to include the counter to be shown next to the notification type
				$notifyTypes[$group][$type] = $descriptor['title'] /* . ' ('.$populatedNotificationGroups[$type].')' */;
			}
		}

		$this->render('index', array(
			'model' => $model,
			'notifyTypes' => $notifyTypes,
			'eventTypes' => CoreNotification::types(),
		));
	}

	public function getPopulatedNotificationGroups($notificationGroups) {
		$currentNotifications = array();
		$currentNotificationsTmp = Yii::app()->db->createCommand("
			SELECT type, COUNT(id) AS counter
			FROM " . CoreNotification::model()->tableName() . "
			GROUP BY type
		")->queryAll();
		if (!empty($currentNotificationsTmp)) {
			foreach ($currentNotificationsTmp as $tmp) {
				$currentNotifications[$tmp['type']] = $tmp['counter'];
			}
		}
		return $currentNotifications;
	}

	/**
	 * Edit Base notification info (type, subject, body, etc.)
	 *
	 * @throws CException
	 */
	public function actionAxEditBase($wizardMode = false) {
		$languages = CoreLangLanguage::getActiveLanguagesByBrowsercode();
		$transports = CoreNotificationTransport::getActiveTransportsList();

		// Mark the default language
		$defaultLang = Lang::getBrowserCodeByCode(Settings::get('default_language'));
		$languages[$defaultLang] = $languages[$defaultLang] . " (" . Yii::t('standard', '_DEFAULT_LANGUAGE') . ")";
		$model = CoreNotification::model()->findByPk($this->idNot);

		// We have some ID, but no model ??? Definitely error
		if ($this->idNot && !$model)
			throw new CException(Yii::t('standard', "_OPERATION_FAILURE"));

		if (!$model)
			$model = new CoreNotification();

		// Prepare messages and subjects
		$subjects = array();
		$messages = array();
		$languageLists = array();

		foreach ($transports as $transport) {
			foreach ($languages as $language => $name) {
				$tr_index = 'transport_' . $transport['id'];
				$subjects[$tr_index][$language] = '';
				$messages[$tr_index][$language] = '';
			}
			$languageLists[$tr_index] = $languages;
		}

		if ($model) {
			/* @var $cmd CDbCommand */
			$cmd = Yii::app()->db->createCommand();
			$cmd->select("*");
			$cmd->from(CoreNotificationTranslation::model()->tableName());
			$cmd->where("notification_id = :notification_id");
			$reader = $cmd->query(array(':notification_id' => $model->id));
			if ($reader) {
				while ($translation = $reader->read()) {
					if (in_array($translation['language'], array_keys($languages))) {
						// This notification translation is amongst the active platform langs
						$tr_index = 'transport_' . $translation['transport_id'];
						$subjects[$tr_index][$translation['language']] = $translation['subject'];
						$messages[$tr_index][$translation['language']] = Docebo::decodeSquareBrackets($translation['message']);
						if (!empty($translation['subject']) || !empty($translation['message'])) {
							// Add an asterix to identify languages that are already filled
							$languageLists[$tr_index][$translation['language']] = $languages[$translation['language']] . ' *';
						}
					}
				}
			}
		}

		// Count how much subjects are filled
		$countAssigned = array();
		// initialize the counters ...
		foreach ($transports as $transport) {
			$countAssigned['transport_' . $transport['id']] = 0;
		}
		// do the count
		foreach ($subjects as $transportId => $langList) {
			foreach ($langList as $lang => $subject) {
				if ($subject) {
					$countAssigned['transport_' . $transportId] ++;
				}
			}
		}

		// Get all notifications descriptors and resolve overall data
		$notifDescriptors = CoreNotification::getNotificationsDescriptors();
		$notifyTypes = array();
		$notificationGroups = CoreNotification::groupsPerType();
		$shortcodes = array();
		$templates = array();
		foreach ($notifDescriptors as $type => $descriptor) {
			if ($descriptor['group']) {
				$group = $descriptor['group'];
			} else {
				$group = isset($notificationGroups[$type]) ? CoreNotification::groupTranslation($notificationGroups[$type]) : Yii::t('setup', 'Other');
			}

			$notifyTypes[$group][$type] = $descriptor['title'];
			$shortcodes[$type] = $descriptor['shortcodes'];
			$templates[$type] = $descriptor['template'];
		}

		// Detect which transport types can apply templates
		$templatesApplyTo = array();
		foreach ($transports as $transport) {
			if (isset($transport['additional_info']['template']) && $transport['additional_info']['template']) {
				$templatesApplyTo[] = $transport['id'];
			}
		}

		// Raise an event that has access to the shortcodes
		// and their hints, organized by type
		// so the listener may modify/remove/add to these
		// arrays for modifying the displayed shortcodes UI
		Yii::app()->event->raise('NotificationShortcodesGet', new DEvent($this, array(
			'shortcodes' => &$shortcodes
		)));

		// Retrieve currently activated transports for the current notification
		if ($model->getIsNewRecord()) {
			$selectedTransports = array();
		} else {
			/* @var $cmd CDbCommand */
			$cmd = Yii::app()->db->createCommand();
			$cmd->select('*');
			$cmd->from(CoreNotificationTransportActivation::model()->tableName());
			$cmd->where("notification_id = :notification_id AND active = 1");
			$selectedTransports = $cmd->queryAll(true, array(':notification_id' => $model->getPrimaryKey()));
		}

		$html = $this->renderPartial('wizard/_step_base', array(
			'model' => $model,
			'languages' => $languages,
			'notifyTypes' => $notifyTypes,
			'shortcodes' => $shortcodes,
			'subjects' => $subjects,
			'messages' => $messages,
			'templates' => $templates,
			'countAssigned' => isset($countAssigned) ? $countAssigned : 0,
			'wizardMode' => $wizardMode,
			'selectedTransports' => $selectedTransports,
			'languageLists' => $languageLists,
			'templatesApplyTo' => $templatesApplyTo
			), true, true);

		echo $html;
		Yii::app()->end();
	}

	/**
	 * Edit Notification scheduling options
	 *
	 * @param boolean $wizardMode
	 */
	public function actionAxEditSchedule($wizardMode = false) {

		// Get the notification model
		$model = CoreNotification::model()->findByPk($this->idNot);

		if ($this->idNot && !$model) {
			throw new CException(Yii::t('standard', "_OPERATION_FAILURE"));
		}

		// Create new model if not available
		if (!$model) {
			$model = new CoreNotification();
		}

		//Convert schedule_time from UTC to user timezone
		$model->convertTime(new DateTimeZone('UTC'), new DateTimeZone(Yii::app()->localtime->getTimeZone()));

		// There is a chance notification type is overriden (by Wizard-edit flow)
		$model->type = $this->notType;

		// Set default scheduling type to "AT"
		if (!$model->schedule_type) {
			$model->schedule_type = CoreNotification::SCHEDULE_AT;
		}

		if (!$model->schedule_shift_period) {
			$model->schedule_shift_period = CoreNotification::SCHED_SHIFT_PERIOD_DAY;
		}

		if (!$model->schedule_shift_number) {
			$model->schedule_shift_number = 1; // ONE [day|week|...]
		}

		// Get notification descriptor
		$notInfo = CoreNotification::getNotificationInfo($model->type);

		$html = $this->renderPartial('wizard/_step_schedule', array(
			'model' => $model,
			'wizardMode' => $wizardMode,
			'notInfo' => $notInfo,
			), true, true);

		echo $html;
		Yii::app()->end();
	}

	/**
	 * Edit Notification recipients options
	 *
	 * @param boolean $wizardMode
	 */
	public function actionAxEditRecipients($wizardMode = false) {

		$model = CoreNotification::model()->findByPk($this->idNot);

		if ($this->idNot && !$model) {
			throw new CException(Yii::t('standard', "_OPERATION_FAILURE"));
		}

		// Create new model if not available
		if (!$model) {
			$model = new CoreNotification();
		}

		// There is a chance notification type is overriden (by Wizard-edit flow)
		$model->type = $this->notType ? $this->notType : $model->type;

		// Get notification descriptor
		$notInfo = CoreNotification::getNotificationInfo($model->type);

		// get the list of PU's profiles
		$criteria = new CDbCriteria;
		$criteria->scopes = array('group' => array('groupKey' => '/framework/adminrules'));
		$puProfiles = CoreGroup::model()->findAll($criteria);
		$profilesList = CHtml::listData($puProfiles, 'idst', function($data) {
				return $data->relativeProfileId();
			});
		$profilesList = array(Yii::t('standard', 'All power user profiles')) + $profilesList;
		$html = $this->renderPartial('wizard/_step_recipients', array(
			'model' => $model,
			'wizardMode' => $wizardMode,
			'notInfo' => $notInfo,
			'profilesList' => $profilesList,
			), true, true);

		echo $html;
		Yii::app()->end();
	}

	/**
	 * Render the last step of the wizard: SAVE
	 */
	public function showWizardSaveStep() {

		$model = CoreNotification::model()->findByPk($this->idNot);

		if ($this->idNot && !$model) {
			throw new CException(Yii::t('standard', "_OPERATION_FAILURE"));
		}

		$html = $this->renderPartial('wizard/_step_save', array(
			'model' => $model,
			), true, true);

		echo $html;
		Yii::app()->end();
	}

	public function registerResources() {

		if (!Yii::app()->request->isAjaxRequest) {
			$cs = Yii::app()->getClientScript();

			// Register JS files
			$cs->registerScriptFile(Yii::app()->baseUrl . '/js/notification.js', CClientScript::POS_HEAD);

			// Register JS script(s)
			$options = array();
			$options = CJavaScript::encode($options);
			$script = "var Notification = new NotificationClass($options);";
			$cs->registerScript("NotificationJS1", $script, CClientScript::POS_HEAD);
		}
	}

	/**
	 * Show DELETE dialog and handle user submit
	 *
	 * @throws CException
	 */
	public function actionAxDelete() {

		$confirm_delete = Yii::app()->request->getParam("confirm_delete", false);

		// DB Start transaction
		$transaction = Yii::app()->db->beginTransaction();

		try {
			$model = CoreNotification::model()->findByPk($this->idNot);
			if (!$model) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			if ($confirm_delete) {
				// First, delete all translations
				$translations = $model->translations;
				foreach ($translations as $item) {
					if (!$item->delete()) {
						throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
					}
				}
				// Now delete the notification itself
				if (!$model->delete())
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));

				$transaction->commit();
				echo '<a class="auto-close success"></a>';
				Yii::app()->end();
			}

			// Show Confirm Dialog
			$html = $this->renderPartial('_delete', array(
				'model' => $model,
				), true);

			echo $html;
			Yii::app()->end();
		} catch (CException $e) {
			$transaction->rollback();
			$html = $this->renderPartial('//common/_dialog2_error', array(
				'message' => $e->getMessage(),
				), true);

			echo $html;
			Yii::app()->end();
		}
	}

	/**
	 * Set a Notification active/inactive
	 *
	 * @throws CException
	 */
	public function actionAxSetActive() {

		$active = Yii::app()->request->getParam("active", false);

		// DB Start transaction
		$transaction = Yii::app()->db->beginTransaction();

		try {
			$model = CoreNotification::model()->findByPk($this->idNot);
			if (!$model) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			$model->active = $active;

			if (!$model->validate()) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			$model->save(false);

			$transaction->commit();
			Yii::app()->end();
		} catch (CException $e) {
			$transaction->rollback();
			$html = $this->renderPartial('//common/_dialog2_error', array(
				'message' => $e->getMessage(),
				), true);

			echo $html;
			Yii::app()->end();
		}
	}

	/**
	 *
	 * @param unknown $data
	 * @param unknown $index
	 * @return Ambigous <string, string, mixed>
	 */
	protected function renderGridCodeColumn($data, $index) {
		$notInfo = $data->getNotificationInfo($data->code);

		$html = $this->renderPartial('_code_column', array(
			'data' => $data,
			'notInfo' => $notInfo,
			), true);

		return $html;
	}

	/**
	 * 
	 * @param unknown $data
	 * @param unknown $index
	 * @return Ambigous <string, string, mixed>
	 */
	protected function renderGridEventNameColumn($data, $index) {

		$notInfo = $data->getNotificationInfo($data->type);

		$html = $this->renderPartial('_event_column', array(
			'data' => $data,
			'notInfo' => $notInfo,
			), true);

		return $html;
	}

	/**
	 *
	 * @param unknown $data
	 * @param unknown $index
	 * @return Ambigous <string, string, mixed>
	 */
	protected function renderGridScheduleColumn($data, $index) {

		$notInfo = CoreNotification::getNotificationInfo($data->type);

		if (empty($notInfo))
			return "N/A";


		if (!in_array(CoreNotification::PROP_SCHEDULE, $notInfo['allowedProperties']))
			return "N/A";


		$html = $this->renderPartial('_schedule_column', array(
			'data' => $data,
			), true);

		return $html;
	}

	/**
	 * Render notifications grid actions column (edit, delete, ...)
	 *
	 * @param CoreNotification $data
	 * @param integer $index
	 *
	 * @return string
	 */
	protected function renderGridOperationsColumn($data, $index) {

		return $this->renderPartial('_operations', array(
				'model' => $data,
				), true);
	}

	/**
	 * Render "translations" grid column
	 *
	 * @param CoreNotification $data
	 * @param integer $index

	 * @return string
	 */
	protected function renderGridAssignedLanguages(CoreNotification $data, $index) {
		$assignedCount = $data->assignedCount;
		$languagesCount = count(CoreLangLanguage::getActiveLanguagesByBrowsercode());

		$html = "<span>$assignedCount</span> / <span>$languagesCount</span>";

		return $html;
	}

	/**
	 * Render "user filter" grid column
	 *
	 * @param CoreNotification $data
	 * @param integer $index
	 *
	 * @return string
	 */
	protected function renderGridUserFilterColumn($data, $index) {

		$notInfo = CoreNotification::getNotificationInfo($data->type);

		if (empty($notInfo))
			return "N/A";


		if (!in_array(CoreNotification::PROP_USER_FILTER, $notInfo['allowedProperties']))
			return "N/A";


		$tooltipText = "";
		$html = $this->renderPartial('_filter_column', array(
			'data' => $data,
			'index' => $index,
			'tooltipText' => $tooltipText,
			), true);
		return $html;
	}

	/**
	 * Render "course association" grid column
	 *
	 * @param CoreNotification $data
	 * @param integer $index
	 *
	 * @return string
	 */
	protected function renderGridAssocColumn($data, $index) {

		$notInfo = CoreNotification::getNotificationInfo($data->type);

		if (empty($notInfo))
			return "N/A";

		if (!in_array(CoreNotification::PROP_COURSE_ASSOC, $notInfo['allowedProperties']) && !in_array(CoreNotification::PROP_COURSE_ONLY, $notInfo['allowedProperties']))
			return "N/A";

		$html = $this->renderPartial('_assoc_column', array(
			'data' => $data,
			'index' => $index,
			'notInfo' => $notInfo,
			), true);
		return $html;
	}

	/**
	 * Render "recipient" grid column
	 *
	 * @param CoreNotification $data
	 * @param integer $index
	 *
	 * @return string
	 */
	protected function renderGridRecipientsColumn($data, $index) {

		$notInfo = CoreNotification::getNotificationInfo($data->type);

		if (empty($notInfo))
			return "N/A";


		if (!in_array(CoreNotification::PROP_RECIPIENTS, $notInfo['allowedProperties']))
			return "N/A";


		$html = $this->renderPartial('_recipients_column', array(
			'data' => $data,
			'index' => $index,
			), true);
		return $html;
	}

	/**
	 * Called by Users Selector -> FILTER to set the notification users/recipients filter
	 */
	public function actionSetNotificationFilter() {
		/* @var $model CoreNotification */

		try {
			$filterOption = Yii::app()->request->getParam('ufilter_option', false);

			$params = array(
				'id' => $this->idNot,
				'filterOption' => $filterOption,
				'users' => UsersSelector::getUsersListOnly($_REQUEST),
				'groups' => UsersSelector::getGroupsListOnly($_REQUEST),
				'branches' => UsersSelector::getBranchesListOnly($_REQUEST),
			);

			$result = CoreNotification::updateUserFilter($params);

			if (!$result) {
				throw new Exception(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			echo "<a class='auto-close success'></a>";
			Yii::app()->end();
		} catch (Exception $e) {
			$this->renderPartial('admin.protected.views.common._dialog2_error', array(
				'type' => 'error',
				'message' => $e->getMessage(),
			));
			Yii::app()->end();
		}
	}

	/**
	 * Edit Courses associated/assigned to a given Notification
	 */
	public function actionAssocCourses() {

		if (!Yii::app()->request->isAjaxRequest) {
			// Register UserSelector JS
			UsersSelector::registerClientScripts();
            Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/DoceboPager.css');
            Yii::app()->clientScript->scriptMap['pager.css'] = false;
		}

		// beforeAction() will assign notification ID to controller's attribute $this->idNot checking for "id" in the request
		$model = CoreNotification::model()->findByPk($this->idNot);
		$model->attributes = Yii::app()->request->getParam('CoreNotification');
		if (!$model) {
			$this->redirect($this->createUrl('index'));
		}


		// Render
		$this->render('assoc_courses', array(
			"model" => $model,
		));
	}

	/**
	 * Called by Courses Selector to set the notification association
	 */
	public function actionSetCourseAssoc() {
		/* @var $model CoreNotification */

		try {
			$filterOption = Yii::app()->request->getParam('cfilter_option', false);

			$params = array(
				'id' => $this->idNot,
				'filterOption' => $filterOption,
				'courses' => UsersSelector::getCoursesListOnly($_REQUEST),
				'plans' => UsersSelector::getPlansListOnly($_REQUEST),
			);

			$result = CoreNotification::updateCourseAssoc($params);

			if (!$result) {
				throw new Exception(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			echo "<a class='auto-close success'></a>";
			Yii::app()->end();
		} catch (Exception $e) {
			$this->renderPartial('admin.protected.views.common._dialog2_error', array(
				'type' => 'error',
				'message' => $e->getMessage(),
			));
			Yii::app()->end();
		}
	}

	public function actionUnassignCourses() {
		if (Yii::app()->request->isAjaxRequest) {
			$rows = array();
			$pk = Yii::app()->request->getParam('pk');

			if (!is_array($pk))
				$pk = array($pk);

			foreach ($pk as $key) {
				$pk = explode('_', $key);

				if (count($pk) != 3)
					continue;

				$id = $pk[0];
				$idItem = $pk[1];
				$type = $pk[2];

				$row = CoreNotificationAssoc::model()->findByAttributes(array(
					'id' => $id,
					'idItem' => $idItem,
					'type' => $type
				));

				if ($row)
					$rows[] = array_merge(
						$row->attributes, array(
						'pk' => $row->id . '_' . $row->idItem . '_' . $row->type,
						'obj' => $row
						)
					);
			}

			if (empty($rows)) {
				$this->sendJSON(array());
			}

			if (!empty($_POST['confirm'])) {
				foreach ($rows as $row)
					$row['obj']->delete();
				$this->sendJSON(array());
			}

			$this->sendJSON(array(
				'html' => $this->renderPartial('_unassignCourses', array('rows' => $rows), true),
			));
		}
	}

	public function actionAssocCoursesAutocomplete() {
		// core_notification id
		$id = Yii::app()->request->getParam('id');
		$query = addcslashes($_REQUEST['data']['query'], '%_');

		$data = CoreNotification::autocomplete($id, $query);
		$this->sendJSON(array('options' => $data));
	}

	/**
	 * Dispatch moving from dialog to dialog
	 * Based on jwizard.js
	 *
	 */
	public function actionWizard() {

		$fromStep = Yii::app()->request->getParam('from_step', self::STEP_INITIAL);
		$nextButton = Yii::app()->request->getParam('next_button', false);
		$prevButton = Yii::app()->request->getParam('prev_button', false);
		$jumpIn = Yii::app()->request->getParam('jump_in', false);
		$finishWizardList = Yii::app()->request->getParam('finish_wizard_list', false);
		$finishWizardActivate = Yii::app()->request->getParam('finish_wizard_activate', false);
		$wizardSaveButton = Yii::app()->request->getParam('jwizard_save_button', false);
		$idForm = Yii::app()->request->getParam('idForm', false);
		$loadYiiGridViewJs = Yii::app()->request->getParam('loadyiigridview', false);

		$noWizardNav = Yii::app()->request->getParam('nowiznav', false);

		// Translate User/Course from_step in a local STEP name
		// Because from_step is always "uni-selector" in User/Course selectors we use
		// the idForm to distinguish them
		if ($fromStep == self::STEP_UNI_SELECTOR && $idForm == 'selector-users') {
			$fromStep = self::STEP_USER_FILTER;
		} else if ($fromStep == self::STEP_UNI_SELECTOR && $idForm == 'selector-courses') {
			$fromStep = self::STEP_COURSE_ASSOC;
		}

		// Wizard parameters for Users/Courses selector
		$wizardParams = array(
			'wizard_mode' => 1,
		);
		// Hide PREV/NEXT button by declaring the step as FIRST/LAST if requested
		// Also, show SAVE (final)
		// Apply to Users/Courses selector only
		if ($noWizardNav) {
			$wizardParams['first'] = 1;
			$wizardParams['last'] = 1;
			$wizardParams['final'] = 1;
		}

		// Get Notification descriptor
		$notInfo = CoreNotification::getNotificationInfo($this->notType);


		$usersSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
				'type' => UsersSelector::TYPE_NOTIFICATIONS_USER_FILTER,
				'idGeneral' => $this->idNot, // notification ID
				'tp' => 'admin.protected.views.notification._ufilter_top', // top panel partial! view
				'idForm' => 'selector-users',
				'wizmode' => 1,
				'noyiixviewjs' => !$loadYiiGridViewJs, // very important: main page already has widget(s) loading yiigridview.js, so, don't
				'wizardParams' => $wizardParams,
		));

		$coursesSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
				'type' => UsersSelector::TYPE_NOTIFICATIONS_COURSES_ASSOC,
				'idGeneral' => $this->idNot, // notification ID
				'tp' => 'admin.protected.views.notification._cfilter_top', // top panel partial! view
				'idForm' => 'selector-courses',
				'wizmode' => 1,
				'noyiixviewjs' => !$loadYiiGridViewJs, // very important: main page already has widget(s) loading yiigridview.js, so, don't
				'wizardParams' => $wizardParams,
				'tabs' => (isset($notInfo['allowedProperties']) && in_array(CoreNotification::PROP_COURSE_ONLY, $notInfo['allowedProperties'])) ? 'courses' : 'courses, plans',
				'notificationType' => $this->notType
		));

		// Just have all submitted data in this array
		$inData = $this->collectSubmittedData(true);


		// Validate incoming step and go back to the same step if failed
		$valid = $this->validateStepData($inData, $fromStep);
		if (!$valid) {
			$this->goStep($fromStep, $usersSelectorUrl, $coursesSelectorUrl);
			Yii::app()->end();
		}


		// If we are finishing the wizard, get data
		if ($finishWizardActivate || $finishWizardList || $wizardSaveButton) {

			$transaction = Yii::app()->db->beginTransaction();
			try {
				$model = CoreNotification::model()->findByPk($this->idNot);
				$this->saveNotification($model, $inData, $notInfo, !empty($finishWizardActivate));
				$transaction->commit();
			} catch (Exception $e) {
				$transaction->rollback();
				$html = $this->renderPartial('//common/_dialog2_error', array(
					'message' => $e->getMessage(),
					'type' => 'error',
					), true);
				echo $html;
				Yii::app()->end();
			}

			unset($_SESSION['notifType']);
			echo '<a class="auto-close success"></a>';
			Yii::app()->end();
		}

		// Some plugins may disallow certain steps
		switch ($fromStep) {
			case self::STEP_BASE:
				if (!in_array(CoreNotification::PROP_SCHEDULE, $notInfo['allowedProperties'])) {
					// Coming from STEP 'base': goto : >> Schedule
					// USUALLY, but not now, since the Schedule step is disallowed
					// for this Notification type
					$fromStep = self::STEP_SCHEDULE;
				}
				$_SESSION['notifType'] = $inData['type'];
				break;
		}


		// Depending on FROM STEP and PREV/NEXT button, render/execute the next dialog/action
		switch ($fromStep) {
			// Coming from NOWHERE, initial opening: goto : >> Base Edit  || Directly jump in a requested step
			case self::STEP_INITIAL:
				if ($jumpIn) {
					$this->goStep($jumpIn, $usersSelectorUrl, $coursesSelectorUrl);
				} else {
					$this->goStep(self::STEP_BASE);
				}
				break;

			// Coming from STEP 'base': goto : >> Schedule
			case self::STEP_BASE:

				$this->goStep(self::STEP_SCHEDULE);
				break;

			// Coming from STEP 'schedule': goto :  >> Edit User filtering || << Base Edit
			case self::STEP_SCHEDULE:

				if ($nextButton) {
					if (!strstr($_SESSION['notifType'], 'App7020')) {
						if (in_array(CoreNotification::PROP_USER_FILTER, $notInfo['allowedProperties'])) {
							$this->goStep(self::STEP_USER_FILTER, $usersSelectorUrl, $coursesSelectorUrl);
						} else {
							$this->goStep(self::STEP_RECIPIENTS);
						}
					} else {
						$this->goStep(self::STEP_SAVE);
					}
				} else {
					$this->goStep(self::STEP_BASE);
				}
				break;

			// From User Filter: goto >> Recipients || << Schedule
			case self::STEP_USER_FILTER:

				if ($nextButton) {

					$this->goStep(self::STEP_RECIPIENTS);
				} else {
					$this->goStep(self::STEP_SCHEDULE);
				}
				break;

			// From Course Assoc: goto >> SAVE || << Recipients
			case self::STEP_COURSE_ASSOC:
				if ($nextButton) {
					$this->goStep(self::STEP_SAVE);
				} else {
					$this->goStep(self::STEP_RECIPIENTS);
				}


			// Coming from STEP 'Recipients': goto : >> Courses || << Users
			case self::STEP_RECIPIENTS:
				if ($nextButton) {
					if (in_array(CoreNotification::PROP_COURSE_ASSOC, $notInfo['allowedProperties']) || in_array(CoreNotification::PROP_COURSE_ONLY, $notInfo['allowedProperties'])) {
						//$this->redirect($coursesSelectorUrl, true);
						$this->goStep(self::STEP_COURSE_ASSOC, $usersSelectorUrl, $coursesSelectorUrl);
					} else {
						$this->goStep(self::STEP_SAVE);
					}
				} else {
					if (in_array(CoreNotification::PROP_USER_FILTER, $notInfo['allowedProperties'])) {
						//$this->redirect($usersSelectorUrl, true);
						$this->goStep(self::STEP_USER_FILTER, $usersSelectorUrl, $coursesSelectorUrl);
					} else {
						$this->goStep(self::STEP_SCHEDULE);
					}
				}
				break;

			// Coming from STEP 'Save': goto : << Course Association
			case self::STEP_SAVE:
				if (in_array(CoreNotification::PROP_COURSE_ASSOC, $notInfo['allowedProperties'])) {
					//$this->redirect($coursesSelectorUrl, true);
					$this->goStep(self::STEP_COURSE_ASSOC, $usersSelectorUrl, $coursesSelectorUrl);
				} else {
					if (!strstr($_SESSION['notifType'], 'App7020')) {
						$this->goStep(self::STEP_RECIPIENTS);
					} else {
						$this->goStep(self::STEP_SCHEDULE);
					}
				}
				break;
		}
	}

	/**
	 * Returns the default email sender using platform settings
	 */
	private function getDefaultSenderEmail() {
		$fromEmail = trim(Settings::get('sender_event', FALSE));
		if (empty($fromEmail))
			$fromEmail = isset(Yii::app()->params['support_sender_email']) ? Yii::app()->params['support_sender_email'] : '';

		// Fallback to info@docebo.com
		if (empty($fromEmail))
			$fromEmail = "info@docebo.com";

		return $fromEmail;
	}

	/**
	 * Collect data submitted by wizard or single dialog (from $_REQUEST)
	 *
	 * @param boolean $wizardMode
	 */
	protected function collectSubmittedData($wizardMode = true) {

		$data = array();

		if ($wizardMode) {
			$rData = $_REQUEST['Wizard'];
			$baseData = $rData['edit-notification-form'];
			$scheduleData = $rData['edit-notification-schedule-form'];
			$userFilterData = $rData['selector-users'];
			$recipientsData = $rData['edit-notification-recipients-form'];
			$courseAssocData = $rData['selector-courses'];
		} else {
			$rData = $_REQUEST;
			$baseData = $rData;
			$scheduleData = $rData;
			$userFilterData = $rData;
			$recipientsData = $rData;
			$courseAssocData = $rData;
		}

		// BASE
		if (isset($baseData['CoreNotification']['type']))
			$data['type'] = $baseData['CoreNotification']['type'];

		if (isset($baseData['CoreNotification']['code']))
			$data['code'] = $baseData['CoreNotification']['code'];

		if (isset($baseData['notification_transports']))
			$data['notification_transports'] = $baseData['notification_transports'];

		if (isset($baseData['subjects']))
			$data['subjects'] = $baseData['subjects'];

		if (isset($baseData['messages']))
			$data['messages'] = $baseData['messages'];

		// custom data acquiring
		if (is_array($baseData['notification_transports'])) {
			$data['transports_parameters'] = array();
			foreach ($baseData['notification_transports'] as $transportId) {
				$transport = CoreNotificationTransport::model()->findByPk($transportId);
				if (!empty($transport)) {
					$manager = $transport->getManagerObject();
					if (is_subclass_of($manager, 'NotificationTransportManager')) {
						$data['transports_parameters'][$transportId] = $manager->readInputParameters($baseData);
					}
				}
			}
		}


        // SCHEDULE
		if (isset($scheduleData['CoreNotification']['schedule_type']))
			$data['schedule_type'] = $scheduleData['CoreNotification']['schedule_type'];

        switch ($scheduleData['CoreNotification']['schedule_type']) {
            case CoreNotification::SCHEDULE_EVERY:
                if (isset($scheduleData['CoreNotification']['schedule_shift_period_every'])) {
                    $data['schedule_shift_period'] = $scheduleData['CoreNotification']['schedule_shift_period_every'];
                    switch ($scheduleData['CoreNotification']['schedule_shift_period_every']) {
                        case CoreJob::PERIOD_MONTH:
                            if (isset($scheduleData['CoreNotification']['schedule_on_day'])) {
                                $data['schedule_shift_number'] = $scheduleData['CoreNotification']['schedule_on_day'];
                            }
                            break;
                        default:
                            if (isset($scheduleData['CoreNotification']['schedule_day'])) {
                                $data['schedule_shift_number'] = $scheduleData['CoreNotification']['schedule_day'];
                            }
                            break;
                    }
                }
                if (isset($scheduleData['CoreNotification']['schedule_time_every'])) {
                    $data['schedule_time'] = (int)$scheduleData['CoreNotification']['schedule_time_every'];
                }
                break;
            default:
                if (isset($scheduleData['CoreNotification']['schedule_shift_period'])) {
                    $data['schedule_shift_period'] = $scheduleData['CoreNotification']['schedule_shift_period'];
                }

                if (isset($scheduleData['CoreNotification']['schedule_shift_number'])) {
                    $data['schedule_shift_number'] = $scheduleData['CoreNotification']['schedule_shift_number'];
                }

                if (isset($scheduleData['CoreNotification']['schedule_time'])) {
                    $data['schedule_time'] = $scheduleData['CoreNotification']['schedule_time'];
                }
                break;
        }




		// USER FILTER
		if (isset($userFilterData['selected_users']))
			$data['selected_users'] = json_decode($userFilterData['selected_users'], true);

		if (isset($userFilterData['selected_groups']))
			$data['selected_groups'] = json_decode($userFilterData['selected_groups'], true);

		if (isset($userFilterData['orgcharts_selected-json']))
			$data['selected_branches'] = json_decode($userFilterData['orgcharts_selected-json'], true);

		if (isset($userFilterData['ufilter_option']))
			$data['ufilter_option'] = $userFilterData['ufilter_option'];



		// RECIPIENTS/ROLES	 (sort of BASE anyway)
		if (isset($recipientsData['CoreNotification']['recipient'])) {
			$data['recipient'] = $recipientsData['CoreNotification']['recipient'];
			if ($recipientsData['CoreNotification']['recipient'] != CoreNotification::NOTIFY_RECIPIENTS_POWER_USER) {
				$data['puProfileId'] = 0;
			} else {
				$data['puProfileId'] = $recipientsData['CoreNotification']['puProfileId'];
			}
		}


		// COURSES/PLANS
		if (isset($courseAssocData['selected_courses']))
			$data['selected_courses'] = json_decode($courseAssocData['selected_courses'], true);

		if (isset($courseAssocData['selected_plans']))
			$data['selected_plans'] = json_decode($courseAssocData['selected_plans'], true);

		if (isset($courseAssocData['cfilter_option']))
			$data['cfilter_option'] = $courseAssocData['cfilter_option'];


		return $data;
	}

	/**
	 * Execute a combined notification saving process: base data, courses, users, etc...
	 *
	 * @param CoreNotification $model
	 * @param array $data Collected data
	 * @param array $notInfo Notification descriptor
	 * @param string $activate  Activate the notification (true)? Or just save (false) ?
	 *
	 * @throws CException
	 * @return boolean
	 */

    public function forceSaveNotification($model, $data, $notInfo, $activate = false){
        $this->saveNotification($model, $data, $notInfo, $activate);
    }

	protected function saveNotification($model, $data, $notInfo, $activate = false) {

		if (!$model) {
			$model = new CoreNotification();
		}

		// BASE && Schedule && Recipient type/params are assigned here
		$model->setAttributes($data, false);
		if (!$model["recipient"]) {
			$model["recipient"] = 'user';
		}
		// Assign the job handler that Notification declares to be its .. handler...
		$model->job_handler_id = $notInfo['jobHandler'];

		// Convert schedule_time from user timezone to UTC
		$model->convertTime(new DateTimeZone(Yii::app()->localtime->getTimeZone()), new DateTimeZone('UTC'));


		// Set the Author ID. If it is a PowerUser, later, job handlers can filter data
		// Do not change the AUTHOR once it is set !!!!
		if (!$model->id_author)
			$model->id_author = Yii::app()->user->id;


		if (isset($data['ufilter_option']))
			$model->ufilter_option = $data['ufilter_option'];

		if (isset($data['cfilter_option']))
			$model->cfilter_option = $data['cfilter_option'];

		// Force active status
		if ($activate === true) {
			$model->active = 1;
		}

		if (!$model->save()) {
			Yii::log(__METHOD__ . ': Unable to save notification' . "\n" . print_r($model->getErrors(), true), CLogger::LEVEL_ERROR);
			throw new CException(Yii::t('notification', 'Unable to save notification'));
		}

		//read and save selected delivery systems
		if (isset($data['notification_transports'])) {
			//pre-deactivate all currently active notifications
			CoreNotificationTransportActivation::model()->updateAll(array('active' => 0), "notification_id = :nid", array(':nid' => $model->id));
			//check input data and create/update transports info
			$transports = $data['notification_transports'];
			if (!empty($transports) && is_array($transports)) {
				foreach ($transports as $transportId) {
					//retrieve additional info for the current transport, if any
					$additionalInfo = array(); //initialize the info with default empty value
					if (isset($data['transports_parameters']) && is_array($data['transports_parameters'])) {
						if (isset($data['transports_parameters'][$transportId])) {
							$additionalInfo = $data['transports_parameters'][$transportId];
						}
					}
					//save stuff
					$activation = CoreNotificationTransportActivation::model()->findByAttributes(array(
						'notification_id' => $model->id,
						'transport_id' => $transportId
					));
					if (empty($activation)) {
						//this type of transport has not been used previously, so create it brand new now
						$activation = new CoreNotificationTransportActivation();
						$activation->notification_id = $model->id;
						$activation->transport_id = $transportId;
					}
					$activation->active = 1;
					$activation->additional_info = CJSON::encode($additionalInfo);
					$activation->save();
				}
			}
		}

		// Subjects & Messages
		if (isset($data['subjects']) && isset($data['messages'])) {
			$this->saveMessageData($model, $transports, $data['subjects'], $data['messages']);
		}

		// Users filter
		if (isset($data['ufilter_option'])) {

			$params = array(
				'id' => $model->id,
				'filterOption' => $data['ufilter_option'],
				'users' => $data['selected_users'],
				'groups' => $data['selected_groups'],
				'branches' => $data['selected_branches'],
			);
			$result = CoreNotification::updateUserFilter($params);
		}


		// Courses / Plans
		if (isset($data['cfilter_option'])) {
			$params = array(
				'id' => $model->id,
				'filterOption' => $data['cfilter_option'],
				'courses' => $data['selected_courses'],
				'plans' => $data['selected_plans'],
			);
			$result = CoreNotification::updateCourseAssoc($params);
		}

		// Now push Jobs if Schedule type is 'after' or 'before'
		$job = $this->saveScheduledJob($model, $data);

		if ($job) {
			$model->id_job = $job->id_job;
			$model->save();
		}

		// Goooood.... finally... get out...

		return true;
	}

	/**
	 * Save subjects and messages translations
	 *
	 * @param CoreNotification $model
	 * @param array $subjects
	 * @param array $messages
	 *
	 * @throws CException
	 * @return boolean
	 */
	protected function saveMessageData($model, $transports, $subjects, $messages) {

		$languages = CoreLangLanguage::getActiveLanguagesByBrowsercode();

		$translationsModel = new CoreNotificationTranslation();
		$translationsModel->deleteAllByAttributes(array('notification_id' => $model->id));

		$defaultLang = Lang::getBrowserCodeByCode(Settings::get('default_language'));

		if (empty($transports) || !is_array($transports)) {
			throw new CExcpetion(Yii::t('notification', 'Please choose at least 1 delivery system'));
		}

		foreach ($transports as $transportId) {

			$hasDefaultSubjcet = (!isset($subjects[$transportId][$defaultLang]) || empty($subjects[$transportId][$defaultLang]));
			$hasDefaultMessage = (!isset($messages[$transportId][$defaultLang]) || empty($messages[$transportId][$defaultLang]));
			if ($hasDefaultSubjcet || $hasDefaultMessage) {
				throw new CException(Yii::t('user_management', "_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED"));
			}

			foreach ($languages as $language => &$name) {
				$data = array();
				$data['notification_id'] = $model->id;
				$data['transport_id'] = $transportId;
				$data['language'] = $language;

				if (isset($subjects[$transportId][$language])) {
					$subjects[$transportId][$language] = trim($subjects[$transportId][$language]);
					if (!empty($subjects[$transportId][$language])) {
						$data['subject'] = $subjects[$transportId][$language];
					}
				}

				if (isset($messages[$transportId][$language])) {
					$messages[$transportId][$language] = trim($messages[$transportId][$language]);
					if (!empty($messages[$transportId][$language])) {
						$data['message'] = $messages[$transportId][$language];
					}
				}

				if ($data['subject'] || $data['message']) {
					$translationsModel = new CoreNotificationTranslation();
					$translationsModel->attributes = $data;

					if (!$model->validate()) {
						Yii::log(__METHOD__ . ": Erro while saving notification translation.\n" . print_r($model->getErrors(), true), CLogger::LEVEL_ERROR);
						throw new CException(Yii::t('standard', "_OPERATION_FAILURE"));
					}

					$translationsModel->save(false);
				}
			}
		}

		return true;
	}

	/**
	 *
	 * @param CoreNotification $model
	 * @param unknown $data
	 */
	protected function saveScheduledJob(CoreNotification $model, $data) {

		$job = $model->scheduledJob;
		// "At the time of event" is NOT really a scheduled event. It will be executed as an immediate job instead.
		if ($model->schedule_type == CoreNotification::SCHEDULE_AT) {
			// If there is job assigned, delete it..
			if ($job) {
				Yii::app()->scheduler->deleteJob($job, true);
			}
			return;
		}

		// Set notification parameters array. It will be saved as job attribute and used by
		// the notification Job Handler. Pss here whatever you think is useful. It is JSON-ed.
		$params = array(
			'notification_id' => $model->id,
        );
        // If we are requested to notify "several HOURS before/after..." we need to run the job every hour
//		if ($model->schedule_shift_period == CoreNotification::SCHED_SHIFT_PERIOD_HOUR) {
//			$period = CoreJob::PERIOD_HOUR;
//		} else {
//			$period = CoreJob::PERIOD_DAY;
//		}
        $atDayOfWeek = false;
        $atDayOfMonth = false;
        $jobType = CoreJob::TYPE_RANDOM;
        if ($model->schedule_type == CoreNotification::SCHEDULE_EVERY) {
            $jobType = CoreJob::TYPE_RECURRING;
            switch ($model->schedule_shift_period) {
                case CoreNotification::SCHED_SHIFT_PERIOD_HOUR:
                    $period = CoreJob::PERIOD_HOUR;
                    break;
                case CoreNotification::SCHED_SHIFT_PERIOD_DAY:
                    $period = CoreJob::PERIOD_DAY;
                    break;
                case CoreNotification::SCHED_SHIFT_PERIOD_WEEK:
                    $period = CoreJob::PERIOD_WEEK;
                    $atDayOfWeek = $model->schedule_shift_number;
                    break;
                case CoreNotification::SCHED_SHIFT_PERIOD_MONTH:
                    $atDayOfMonth = $model->schedule_shift_number;
                    $period = CoreJob::PERIOD_MONTH;
                    break;
            }
        } else {
            switch ($model->schedule_shift_period) {
                case CoreNotification::SCHED_SHIFT_PERIOD_HOUR:
                    $period = CoreJob::PERIOD_HOUR;
                default:
                    $period = CoreJob::PERIOD_DAY;
            }
        }
		// If there is NO job, create one
		if (!$job) {
            $job = Yii::app()->scheduler->createJob(

                $model->type,
                $model->job_handler_id,
                $jobType,
                $period,
                1,
                0, // minute is always 0
				$model->schedule_time, // hour
                false,
                $atDayOfWeek,
                $atDayOfMonth,
                false,
                $params
			);
		}
		// Else, modify the old one
		else {
            $job = Yii::app()->scheduler->modifyJob($job, $model->type, $model->job_handler_id, $jobType, $period, 1, 0,
                // minute is always 0
				$model->schedule_time, // hour
                false, $atDayOfWeek, $atDayOfMonth, false, $params
			);
		}

		return $job;
	}

	/**
	 * Validate && Sanitize user input from different steps.
	 * If error is found, setFlash() is issued and user is redirected BACK to the same step, where error is displayd at the top.
	 *
	 * @param array $data Incoming (usually submitted) data
	 * @param string $fromStep Which step we are coming FROM ?
	 *
	 * @return boolean
	 */
	protected function validateStepData($data, $fromStep) {

		switch ($fromStep) {

			case self::STEP_BASE:
				// TYPE
				if (!$data['type']) {
					Yii::app()->user->setFlash('error', Yii::t('notification', 'Please choose a notification type'));
					return false;
				}

				//transports have been selected
				if (empty($data['notification_transports']) || !is_array($data['notification_transports'])) {
					Yii::app()->user->setFlash('error', Yii::t('notification', 'Please choose at least 1 delivery system'));
					return false;
				}

				// Subjects && Message bodies
				if (is_array($data['notification_transports'])) {
					$defaultLang = Lang::getBrowserCodeByCode(Settings::get('default_language'));
					foreach ($data['notification_transports'] as $transportId) {
						$subjectNoLang = (!isset($data['subjects'][$transportId][$defaultLang]) || empty($data['subjects'][$transportId][$defaultLang]));
						$messageNoLang = (!isset($data['messages'][$transportId][$defaultLang]) || empty($data['messages'][$transportId][$defaultLang]));
						if ($subjectNoLang || $messageNoLang) {
							Yii::app()->user->setFlash('error', Yii::t('user_management', "_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED"));
							return false;
						}
					}
				}

				// custom validation for each selected transport type
				if (is_array($data['notification_transports'])) { // 'notification_transports' is the list of checkboxes in "general settings" tab
					foreach ($data['notification_transports'] as $transportId) {
						$transport = CoreNotificationTransport::model()->findByPk($transportId);
						if (!empty($transport)) {
							$manager = $transport->getManagerObject();
							if (is_subclass_of($manager, 'NotificationTransportManager')) { //make sure that object has been correctly instantiated
								if (!$manager->validateParameters($data['transports_parameters'][$transportId], $data, $fromStep)) {
									// NOTE: setting an appropriate error message has been left to the manager method above
									return false;
								}
							}
						}
					}
				}

				break;


			case self::STEP_SCHEDULE:
				if (!$data['schedule_type']) {
					Yii::app()->user->setFlash('error', Yii::t('notification', 'Please choose scheduling type'));
					return false;
				}
				$n = (int) $data['schedule_shift_number'];
                if (!$n && $data['schedule_type'] != CoreNotification::SCHEDULE_AT && $data['schedule_type'] != CoreNotification::SCHEDULE_EVERY) {
					Yii::app()->user->setFlash('error', Yii::t('notification', 'Please select valid number'));
					return false;
				}

				break;

			case self::STEP_RECIPIENTS:
				if (!$data['recipient']) {
					Yii::app()->user->setFlash('error', Yii::t('notification', 'Please select role'));
					return false;
				}
				break;

			case self::STEP_USER_FILTER:
				if (!$data['ufilter_option']) {
					Yii::app()->user->setFlash('error', Yii::t('notification', 'Please choose valid filtering options'));
					return false;
				} elseif ($data['ufilter_option'] == 2 && (empty($data['selected_groups']) && empty($data['selected_branches']))) {
					Yii::app()->user->setFlash('error', Yii::t('notification', 'Please choose valid filtering options'));
					return false;
				}
				break;

			case self::STEP_COURSE_ASSOC:
				if (!$data['cfilter_option']) {
					Yii::app()->user->setFlash('error', Yii::t('notification', 'Please choose valid filtering options'));
					return false;
				} elseif ($data['cfilter_option'] == 2 && (empty($data['selected_courses']) && empty($data['selected_plans']))) {
					Yii::app()->user->setFlash('error', Yii::t('notification', 'Please choose valid filtering options'));
					return false;
				}
				break;


			default:
				return true;
				break;
		}

		return true;
	}

	/**
	 * Go to wizard step ...

	 * @param string $step
	 * @param string $usersSelectorUrl
	 * @param string $coursesSelectorUrl
	 */
	protected function goStep($step, $usersSelectorUrl = '', $coursesSelectorUrl = '') {

		switch ($step) {
			case self::STEP_BASE:
				$this->actionAxEditBase(true);
				break;
			case self::STEP_USER_FILTER:
				$this->redirect($usersSelectorUrl);
				break;
			case self::STEP_COURSE_ASSOC:
				$this->redirect($coursesSelectorUrl);
				break;
			case self::STEP_SCHEDULE:
				$this->actionAxEditSchedule(true);
				break;
			case self::STEP_RECIPIENTS:
				$this->actionAxEditRecipients(true);
				break;
			case self::STEP_SAVE:
				$this->showWizardSaveStep();
				break;
		}
	}

	public function actionAxNotificationsAutocomplete() {
		$requestData = Yii::app()->request->getParam('data', array());

		$query = '';
		if (!empty($requestData['query'])) {
			$query = addcslashes($requestData['query'], '%_');
		}
		$objectsQuery = Yii::app()->db->createCommand()
			->select(array('id', 'code'))
			->from(CoreNotification::model()->tableName())
			->where(array('like', 'code', "%" . $query . "%"));

		$objects = $objectsQuery->queryAll(true);

		$list = CHtml::listData($objects, 'id', 'code');
		$this->sendJSON(array('options' => $list));
	}

}
