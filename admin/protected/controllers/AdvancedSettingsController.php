<?php

class AdvancedSettingsController extends AdminController {

	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}

	// TODO: change access rules
	public function accessRules() {
		$res = array();

		$res[] = array(
			'allow',
			'roles' => array('/framework/admin/setting/view'),
			'actions' => array('index', 'register', 'user', 'passwordPolicy', 'eLearning', 'advanced', 'datetime', 'getCustomDateFormats', 'socialRating', 'pens', 'emails', 'https',
				'axAddIframeUrl', 'axDeleteIframeUrl', 'axShowError', 'axPensApplicationsAutocomplete', 'axPensAddApplication', 'axPensEditApplication', 'axPensDeleteApplication', 'axPensToggleEnable'),
		);

		$res[] = array(
			'deny',
			// deny all
			'users' => array('*'),
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);

		return $res;
	}

	public function actionIndex() {
		$cs = Yii::app()->getClientScript();
		$themeUrl = Yii::app()->theme->baseUrl;

		$cs->registerScriptFile(Yii::app()->theme->baseUrl . "/js/https_settings.js", CClientScript::POS_END);
		$cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/tokenInput/jquery.tokeninput.js');

		$cs->registerCssFile(Yii::app()->theme->baseUrl.'/js/tokenInput/token-input.css');
		$cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/advanced_settings.css');

		// FCBK
		$cs->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
		$cs->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');

		// BlockUI
		$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.blockUI.js');

		/*
		//yii grid view
		$cs->registerScriptFile(Yii::getPathOfAlias('zii.widgets.assets.grid').'/jquery.yiigridview.js');
		$cs->registerScriptFile($themeUrl . '/js/docebo.yiigridview.js');

		//Combo Grid View
		$cs->registerScriptFile($themeUrl . '/js/combogridview.js');
		*/

		$this->render('index');
	}

	public function actionRegister() {
		$form = new AdvancedSettingsRegisterForm();
		$this->processForm($form);

		if (Yii::app()->request->isAjaxRequest) {
			$this->sendJSON(array(
				'success' => true,
				'html' => $this->renderPartial('register', array('model' => $form), true),
			));
		}
	}

	public function actionUser() {
		$form = new AdvancedSettingsUserForm();
		$this->processForm($form);

		if (Yii::app()->request->isAjaxRequest) {
			$this->sendJSON(array(
				'success' => true,
				'html' => $this->renderPartial('user', array('model' => $form), true),
			));
		}
	}

	public function actionPasswordPolicy() {
		$form = new AdvancedSettingsPasswordForm();
		$this->processForm($form);

		if (Yii::app()->request->isAjaxRequest) {
			$this->sendJSON(array(
				'success' => true,
				'html' => $this->renderPartial('passwordPolicy', array('model' => $form), true),
			));
		}
	}

	public function actionELearning() {
		$filter = Yii::app()->request->getParam("filter", false);
		$filter_clear = Yii::app()->request->getParam("filter_clear", false);
		$form = new AdvancedSettingsELearningForm();
		$this->processForm($form);
		$whitelistModel = CoreSettingWhitelist::model()->findByAttributes(array('type' => 'lo_iframe_url'));
		$elements_per_page = CoreSetting::model()->findByAttributes(array('param_name' => 'elements_per_page'));
		if(!$elements_per_page || $elements_per_page->param_value == 0){
			$items_per_page = 10;
		} else {
			$items_per_page = $elements_per_page->param_value;
		}

		// Get whitelist with iframes allowed for LOs
		$whitelist = array();
		if($whitelistModel) {
			$whitelist = CJSON::decode($whitelistModel->whitelist);
		}

		if($filter || $filter_clear){
			if(!$filter_clear){
				$whitelist = array_filter($whitelist, function($el) use ($filter) {
					return ( strpos($el, $filter) !== false );
				});
			}

			if (Yii::app()->request->isAjaxRequest) {
				$this->sendJSON(array(
					'success' => true,
					'html' => $this->renderPartial('_whiteList',
						array(
							'model' => $form,
							'whitelist' => $whitelist,
							'elements_per_page'=>$items_per_page,
							'filter'=>$filter
						),
						true),
				));
			}
		} else {
			if (Yii::app()->request->isAjaxRequest) {
				$this->sendJSON(array(
					'success' => true,
					'html' => $this->renderPartial('eLearning',
						array(
							'model' => $form,
							'whitelist' => $whitelist,
							'elements_per_page'=>$items_per_page,
							'filter'=>$filter
						),
						true),
				));
			}
		}


	}

	public function actionAdvanced() {
		$form = new AdvancedSettingsForm();
		$this->processForm($form);

		if (Yii::app()->request->isAjaxRequest) {
			$this->sendJSON(array(
				'success' => true,
				'html' => $this->renderPartial('advanced', array('model' => $form), true),
			));
		}
	}

	public function actionDatetime() {
		$form = new AdvancedSettingsForm();
		$this->processForm($form);

		if (Yii::app()->request->isAjaxRequest) {
			$this->sendJSON(array(
				'success' => true,
				'html' => $this->renderPartial('datetime', array('model' => $form), true),
			));
		}
	}


	public function actionGetCustomDateFormats()
	{
		if (Yii::app()->request->isAjaxRequest) {
			$form = new CActiveForm();
			$locale = Yii::app()->request->getParam('code');
			$model = new AdvancedSettingsForm();

			$dropdown = $form->listBox($model, 'date_format_custom', Yii::app()->localtime->getDateTimeFormatsArray($locale), array('size' => 1, 'prompt' => Yii::t('configuration', 'Select a date format...')));

			$this->sendJSON(array(
				'success' => true,
				'html' => $dropdown
			));
		}
	}


	public function actionSocialRating()
	{
		$form = new AdvancedSettingsSocialRatingForm();
		$this->processForm($form);

		if (Yii::app()->request->isAjaxRequest) {
			$this->sendJSON(array(
				'success' => true,
				'html' => $this->renderPartial('socialRating', array('model' => $form), true),
			));
		}
	}



	public function actionPens()
	{
		/* @var $rq CHttpRequest */
		$rq = Yii::app()->request;

		if ($rq->isAjaxRequest) {
			$html = $this->renderPartial('pens', array('searchInput' => $rq->getParam('search_input', false)), true);
			switch ($rq->getParam('content_format', 'html')) {
				case 'json':
					$this->sendJSON(array(
						'success' => true,
						'html' => $html,
					));
					break;
				case 'html':
					echo $html;
					break;
			}
		}
	}



	public function actionEmails(){

		// Begin possible typeahead request
		if(isset($_GET['q'])){
			// An ajax autocomplete request
			// We are just reusing the same controller action

			$keyword = $_GET['q'];

			$results = array();

			$users = Yii::app()->getDb()->createCommand()
				->select('idst,email')
				->from('core_user')
				->where('email LIKE :search OR firstname LIKE :search OR lastname LIKE :search OR userid LIKE :search', array(':search'=>'%'.$keyword.'%'))
				->andWhere('email IS NOT NULL AND email <> ""')
				->limit(10, 0)
				->queryAll();

			// Always add the suggested email to the list (if it looks like a valid email)

			if( empty( $users ) ) {
				if( filter_var( $keyword, FILTER_VALIDATE_EMAIL ) ) {
					$avatarSrc = CoreUser::getAvatarByUserId(-1); // get "No avatar" image
					$results[] = array(
						'id' => $keyword,
						'name' => ($avatarSrc ? '<img class="email-avatar" src="' . $avatarSrc . '" />' : '') . Yii::app()->htmlpurifier->purify($keyword)
					);
				}
			} else {
				foreach ( $users as $row ) {
					$avatarSrc = CoreUser::getAvatarByUserId( $row['idst'] );
					$results[] = array(
						'id'   => $row['email'],
						'name' => ($avatarSrc ? '<img class="email-avatar" src="' . $avatarSrc . '" />' : '') . $row['email']
					);
				}
			}

			echo CJSON::encode($results);
			Yii::app()->end();
		}
		// End possible typeahead request

		$model = new AdvancedSettingsEmailsForm();
		$this->processForm($model);

		if (Yii::app()->request->isAjaxRequest) {

			// Those two should be replaced with the real values,
			// retrieved from the ERP api, when this API is ready
			// @TODO
			$erpClient = new ErpApiClient2();
			$emails = $erpClient->getInstallationEmails();
			if(empty($emails)){
				$billingEmailsFromERP = array();
				$productEmailsFromERP = array();
			}else{
				$tmpBilling = $emails['billing'];
				if(is_string($tmpBilling)){
					$tmpBilling = explode(',', $tmpBilling);
				}elseif(is_array($tmpBilling)){
					// All good
				}else{
					$tmpBilling = array();
				}
				$tmpProduct = $emails['product'];
				if(is_string($tmpProduct)){
					$tmpProduct = explode(',', $tmpProduct);
				}elseif(is_array($tmpProduct)){
					// All good
				}else{
					$tmpProduct = array();
				}
				$billingEmailsFromERP = array_filter($tmpBilling);
				$productEmailsFromERP = array_filter($tmpProduct);
			}

			$model->billingEmails = implode(',', $billingEmailsFromERP); // comma separated string
			$model->productEmails = implode(',', $productEmailsFromERP); // comma separated string

			$localUsersQuery = Yii::app()->getDb()->createCommand()
				->select('idst,email,avatar')
				->from('core_user')
				->where(array('IN', 'email', array_merge($billingEmailsFromERP, $productEmailsFromERP)))
				->queryAll();

			// Organize the array
			$localUsersOrganizedByEmail = array();
			foreach($localUsersQuery as $localUser){
				$localUsersOrganizedByEmail[$localUser['email']] = $localUser;
			}

			/**
			 * A small utility function to help with converting
			 * a list of plain emails into a $.tokenInput() compatible
			 * format (for easier restoration inside the view file)
			 * @param array $emails
			 * @return array
			 */
			$typeAheadCompatibilityConverter = function(array $emails) use ($localUsersOrganizedByEmail){
				$formattedArr = array();
				foreach($emails as $email){
					$email = Yii::app()->htmlpurifier->purify($email);
					$foundLocalUser = isset($localUsersOrganizedByEmail[$email]) ? $localUsersOrganizedByEmail[$email] : false;
					if($foundLocalUser){
						$avatarSrc = CoreUser::getAvatarByUserId($foundLocalUser['idst']);
					}else{
						$avatarSrc = CoreUser::getAvatarByUserId(-1); // get "No avatar" image
					}
					$formattedArr[] = array(
						'id'=>$email,
						'name'=>($avatarSrc ? '<img class="email-avatar" src="' . $avatarSrc . '" />' : '').$email,
					);
				}
				return $formattedArr;
			};

			// Parse the emails, restored from the ERP, in a format
			// that is friendly to the Typeahead widget
			$restoredBillingEmails = $typeAheadCompatibilityConverter($billingEmailsFromERP);
			$restoredProductEmails = $typeAheadCompatibilityConverter($productEmailsFromERP);

			$this->sendJSON(array(
				'success' => true,
				'html' => $this->renderPartial('emails', array(
					'model'                              => $model,
					'prepopulatedBillingEmailsTypeahead' => $restoredBillingEmails,
					'prepopulatedProductEmailsTypeahead' => $restoredProductEmails,
				), true),
			));
		}
	}
	
	
	/**
	 *  AJAX called to return Admin/Advanced Settings/Https UI
	 */
	public function actionHttps() {
		if(!PluginManager::isPluginActive('HttpsApp'))
			$this->sendJSON(array('success' => false));

		$httpsModel = CoreHttps::getGeneralHttps();
		
		$valid = $httpsModel->processSettingsRequest($_REQUEST, CoreHttps::CONTEXT_GENERAL_HTTPS);
		
		if (Yii::app()->request->isAjaxRequest) {
			$this->sendJSON(array(
				'success' => true,
				// We are using THE same VIEW from Multidomain settings
				'html' => $this->renderPartial('plugin.MultidomainApp.views.main._settings_https', array(
					'model' 	=> $httpsModel,
					'context'	=> CoreHttps::CONTEXT_GENERAL_HTTPS,	
				), true, false),
			));
		}
		
	}

	
	/**
	 * 
	 * @param unknown $form
	 * @return boolean
	 */
	private function processForm($form) {
		$className = get_class($form);

		if (!empty($_POST[$className])) {
			$form->attributes = $_POST[$className];
			
			// Handle incoming TinCan URLs, if any
			if ($form instanceof AdvancedSettingsELearningForm) {
                $this->updateTincanUrls($form);
			}

			//check validity of session time (cannot be less than 15 minutes or more then 3 hours)
			if (isset($_POST[$className]['ttlSession'])) {
				$ttlSession = (int)$_POST[$className]['ttlSession'];
				$form->ttlSession = Yii::app()->session->validateSessionTimeout($ttlSession);
			}

			if(isset($_POST[$className]['allowed_domains'])){
				$form->allowed_domains = $_POST[$className]['allowed_domains'];
			}else{
				// There are a few case where the property is not present
				if(property_exists($form, 'allowed_domains'))
					$form->allowed_domains = array();
			}

			if ($form->validate()) {
				$form->save();

				// Trigger hydra invalidation
				Yii::app()->legacyWrapper->invalidate();

				// Event raised for YnY app to hook on to
				Yii::app()->event->raise('SaveCustomAdvancedSettings', new DEvent($this, array('form' => $form, 'data' => $_POST)));

				$url = Yii::app()->createUrl('advancedSettings/index', array('#' => $_POST['selectedTab']));
				$this->redirect($url);
			} else {
				$url = Yii::app()->createUrl('advancedSettings/index', array('#' => $_POST['selectedTab']));
				Yii::app()->user->setFlash('error', CHtml::errorSummary($form, '', '', array(
					'class' => ''
				)));
				$this->redirect($url, true);
			}
		}
	}

	public function actionAxAddIframeUrl() {
		$addIframeSourse = Yii::app()->request->getParam('add_iframe_sourse', false);
		$filter = Yii::app()->request->getParam("filter", false);

		// If there is passed URL to be added do it
		if ($addIframeSourse) {
			//
			if(!filter_var($addIframeSourse, FILTER_VALIDATE_URL)) {
				echo CJSON::encode(array('success' => false, 'error' => Yii::t('standard', 'Please, enter a valid URL').
					'. <br />'.Yii::t('standard', 'Please, use "<b>http://</b>" or "<b>https://</b>" as URL prefix!')));
				Yii::app()->end();
			}

			$model = CoreSettingWhitelist::model()->findByAttributes(array('type' => 'lo_iframe_url'));
			if ($model) {
				$allSources = CJSON::decode($model->whitelist);

				$alreadyInd = false;
				foreach ($allSources as $src) {
					if ($src == $addIframeSourse) {
						$alreadyInd = true;
					}
				}

				if (!$alreadyInd) {
					array_unshift($allSources, $addIframeSourse);
				} else {
					echo CJSON::encode(array('success' => false, 'error' => Yii::t('standard', 'The URL, that you try to add it already added')));
					Yii::app()->end();
				}

				$model->whitelist = CJSON::encode($allSources);

				if($model->save()) {
					// ok
					echo CJSON::encode(array('success' => true, 'error' => '','counter' => count($allSources) ));
					Yii::app()->end();
				} else {
					// not ok
					echo CJSON::encode(array('success' => false, 'error' => 'Operation failed'));
					Yii::app()->end();
				}
			} else {
				// There is no such record in the db, so we need to create it
				$allSources = CJSON::encode(array($addIframeSourse => $addIframeSourse));

				$model = new CoreSettingWhitelist();
				$model->type = 'lo_iframe_url';
				$model->whitelist = $allSources;

				if($model->save()) {
					// ok
					echo CJSON::encode(array('success' => true, 'error' => ''));
				} else {
					// not ok
					echo CJSON::encode(array('success' => false, 'error' => Yii::t('standard', '_OPERATION_FAILURE')));
				}
				Yii::app()->end();
			}
		}

		echo CJSON::encode(array('success' => false, 'error' => Yii::t('standard', '_OPERATION_FAILURE')));
		Yii::app()->end();
	}

	public function actionAxDeleteIframeUrl() {
		// Get the removeUrl from the request
		$removeUrl = Yii::app()->request->getParam('removeUrl', false);
		$confirm = Yii::app()->request->getParam('confirm', false);

		if (!$confirm) {
			if (Yii::app()->request->isAjaxRequest) {
				$content = $this->renderPartial('_delete_confirm', array(
					'model' => new CoreSettingWhitelist(),
					'removeUrl' => $removeUrl
				), false);
			}

			Yii::app()->end();
		}

		// If there is passed URL to be removed do it
		if ($removeUrl) {
			// Get the whitelist from the DB
			$model = CoreSettingWhitelist::model()->findByAttributes(array('type' => 'lo_iframe_url'));
			if ($model) {
				$allSources = CJSON::decode($model->whitelist);

				$alreadyInd = false;
				foreach ($allSources as $srcKey => $src) {
					if ($src == $removeUrl) {
						$alreadyInd = $srcKey;
					}
				}

				if($alreadyInd !== false) {
					if(array_key_exists($alreadyInd, $allSources)) {
						unset($allSources[$alreadyInd]);

						$model->whitelist = CJSON::encode($allSources);

						if($model->save()) {
							// successfully deleted
							echo '<a class="auto-close success"></a>';

							// ok
							Yii::app()->end();
						}
					}
				}
			}
		}

		// error in deletion operation
		echo '<a class="auto-close unsuccess"></a>';

		Yii::app()->end();
	}


	public function actionAxShowError() {
		$message = Yii::app()->request->getParam('message', Yii::t('standard', '_OPERATION_FAILURE'));
		$this->renderPartial('_error', array(
			'message' => $message
		));

		Yii::app()->end();
	}





	public function actionAxPensApplicationsAutocomplete() {
		/* @var $rq CHttpRequest */
		$rq = Yii::app()->request;

		if (!$rq->isAjaxRequest) return; //only ajax available for this action

		//read and validate query input
		$term = $rq->getParam('term');
		$maxNumber = $rq->getParam('max_number', 10);
		if (empty($term)) { return; }
		if ($maxNumber <= 0) { $maxNumber = 1; }

		//escaping special characters
		$query = '%'.strtolower(addcslashes($term, '%_')).'%';

		/* @var $command CDbCommand */
		$command = Yii::app()->db->createCommand();
		$command->select('id, name');
		$command->from(PensApplication::model()->tableName());
		$command->where("name LIKE :query");
		$command->order("name ASC");
		$command->limit($maxNumber, 0);
		$command->params[':query'] = $query;
		$matches = $command->queryAll();

		$result = array();
		if ($matches) {
			foreach ($matches as $match){
				$result[] = array(
					'key' => $match['id'],
					'value' => $match['name']
				);
			}
		}

		echo CJSON::encode($result);
	}


	public function actionAxPensAddApplication() {
		/* @var $rq CHttpRequest */
		/* @var $lt LocalTime */
		$rq = Yii::app()->request;
		$lt = Yii::app()->localtime;

		$confirm = ($rq->getParam('confirm', 0) > 0);
		$model = new PensApplication();

		//=== action has been confirmed ===
		if ($confirm) {

			$model->setAttributes($rq->getParam('PensApplication'));
			$model->date_creation = $lt->toLocalDateTime();
			$model->date_update = null;
			$model->not_editable = 0;
			if ($model->validate()) {
				$model->save();
				echo CJSON::encode(array(
					'success' => true,
					'id' => $model->getPrimaryKey()
				));
				Yii::app()->end();
			}
		}
		//===

		//if we came here then no confirm button has been pressed (or some errors occurred while saving data): just display the dialog content
		echo CJSON::encode(array(
			'success' => true,
			'html' => $this->renderPartial('_pens_form', array(
				'model' => $model
			), true)
		));
	}


	public function actionAxPensEditApplication() {
		/* @var $rq CHttpRequest */
		/* @var $lt LocalTime */
		$rq = Yii::app()->request;
		$lt = Yii::app()->localtime;

		$id = $rq->getParam('id', 0);
		$confirm = ($rq->getParam('confirm', 0) > 0);
		$model = PensApplication::model()->findByPk($id);
		if (!$model) {
			throw new CException('Invalid application');
		}
		if ($model->not_editable == 1) {
			throw new CException('Model #'.$id.' is not editable');
		}

		//=== action has been confirmed ===
		if ($confirm) {

			$model->setAttributes($rq->getParam('PensApplication'));
			$model->date_update = $lt->toLocalDateTime();
			if ($model->validate()) {
				$model->save();
				echo CJSON::encode(array(
					'success' => true,
					'id' => $model->getPrimaryKey()
				));
				Yii::app()->end();
			}
		}
		//===

		//if we came here then no confirm button has been pressed (or some errors occurred while saving data): just display the dialog content
		echo CJSON::encode(array(
			'success' => true,
			'html' => $this->renderPartial('_pens_form', array(
				'model' => $model
			), true)
		));
	}


	public function actionAxPensDeleteApplication() {
		/* @var $rq CHttpRequest */
		$rq = Yii::app()->request;

		$id = $rq->getParam('id', 0);
		$confirm = ($rq->getParam('confirm', 0) > 0);
		$model = PensApplication::model()->findByPk($id);
		if (!$model) {
			throw new CException('Invalid application');
		}
		if ($model->not_editable == 1) {
			throw new CException('Model #'.$id.' is not editable');
		}

		//=== action has been confirmed ===
		if ($confirm) {
			if ($model->delete()) {
				echo CJSON::encode(array(
					'success' => true
				));
				Yii::app()->end();
			} else {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}
		}

		//if we came here then no confirm button has been pressed (or some errors occurred while saving data): just display the dialog content
		echo CJSON::encode(array(
			'success' => true,
			'html' => $this->renderPartial('_pens_delete', array(
				'model' => $model
			), true)
		));
	}


	public function actionAxPensToggleEnable() {
		/* @var $rq CHttpRequest */
		$rq = Yii::app()->request;

		$id = $rq->getParam('id');
		$newValue = ($rq->getParam('new_value', 0) > 0 ? 1 : 0);

		$model = PensApplication::model()->findByPk($id);
		if (!$model) {
			throw new CException('Invalid application');
		}

		$model->enable = $newValue;
		$model->save();

		echo CJSON::encode(array('success' => true, 'new_value' => $newValue));
	}

	/**
	 * Use the incoming array of TinCan URLs and save them if any
	 * 
	 * @param AdvancedSettingsELearningForm $form
	 */
	private function updateTincanUrls(AdvancedSettingsELearningForm $form) {

	    if (!$form->tincan_urls || !is_array($form->tincan_urls)) {
	        CoreSettingTincanUrl::model()->deleteAll();
	        return;
	    }
	        

	    $currentUrls = $form->getTinCanUrls();
	    $newUrls     = array_values($form->tincan_urls);
	    
	    $toDelete = array_diff($currentUrls, $newUrls);
	    $toAdd    = array_diff($newUrls, $currentUrls);

	    // Delete 
	    if (is_array($toDelete) && !empty($toDelete)) {
            CoreSettingTincanUrl::model()->deleteAllByAttributes(array("url" => $toDelete));
	    }
        
	    if (is_array($toAdd) && !empty($toAdd)) {
            $model = new CoreSettingTincanUrl();
            foreach ($toAdd as $url) {
                $model->isNewRecord = true;
                $model->id = null;
                $model->url = $url;
                $model->save();
            }
	    }
	    
	        
	}

}