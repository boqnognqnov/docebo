<?php

class MarketplaceController extends AdminController {


	/**
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}


	/**
	 * @see CController::accessRules()
	 */
	public function accessRules() {

		$res = array();

		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => array('index'),
		);

		$res[] = array(
			'deny',
			'users' => array('*'),
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);

		return $res;
	}



	public function init() {
		//do normal stuff for standard controllers
		parent::init();
		//force a different layout for this specific controller
		$this->layout = '//layouts/hydra';
	}


	public function actionIndex() {

		//build url for marketplace iframe
		$url = Yii::app()->getRequest()->getHostInfo().'/marketplace';

		$this->render('index', array(
			'url' => $url
		));
	}

}