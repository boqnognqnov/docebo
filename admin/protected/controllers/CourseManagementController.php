<?php

	class CourseManagementController extends AdminController {

		public function beforeAction($action) {
			parent::beforeAction($action);
			DatePickerHelper::registerAssets();
			return true;
		}

		public function filters() {
			return array(
				'accessControl' => 'accessControl',
			);
		}

		public function accessRules() {

			$res = array();

			//Additional permission check for the courseAutocomplete function
			$res[] = array(
				'allow',
				'roles' => array('/lms/admin/report/view'),
				'actions' => array(
					'courseAutocomplete'
				),
			);

			$res[] = array(
				'allow',
				'roles' => array('/lms/admin/course/view'),
				'actions' => array(
					'index',
					'enrollment',
					'getCategoryTree',
					'courseAutocomplete',
					'courseEnrollmentsAutocomplete',
					'usersAutocomplete',
					'getSlidersContent',
					'axMassiveChangeStatus',
					'axMassiveChangeEnrollmentDeadline',
					'renderSessionsGridForMassEnrollment',
					'manageSeats',
					'coursePuAutocomplete',
					'attendance'
				),
			);

			$res[] = array(
				'allow',
				'roles' => array('/lms/admin/course/add'),
				'actions' => array(
					'createCourse',
					'copyCourse',
					'processNewCourse',
				),
			);
			$res[] = array(
				'allow',
				'roles' => array('/lms/admin/course/mod'),
				'actions' => array(
					'updateCourse',
					'manageSeats',
					'coursePuAutocomplete',
					'processNewCourse'
				),
			);
			$res[] = array(
				'allow',
				'roles' => array('/lms/admin/course/del'),
				'actions' => array('deleteCourse',),
			);
			$res[] = array(
				'allow',
				'expression' => 'LearningCourseuser::userLevel(Yii::app()->user->id, Yii::app()->request->getParam("course_id")) == LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR',
				'actions' => array('enrollment')
			);
			$res[] = array(
				'allow',
				'roles' => array('enrollment/create'),
				'actions' => array(
					'enroll',
					'enrollment',
					'confirmEnrollment',
					'cancelEnrollment',
					'copyEnrollments',
					'enrollFromCsv',
					'resetDates',
					'sendEmails',
					'axEnrollUsersToManyCourses',
					'axEnrollUsersToSingleCourse',
					'axProgressiveEnrollUsersToSingleCourse',
					'axProgressiveEnrollUsersToManyCourses',
					'axProgressiveChangeEnrollmentStatus',
					'axProgressiveChangeEnrollmentDeadline',
					'axProgressiveDeleteUserEnrolment',
					'axProgressiveSendEmailToEnrolledUsers',
					'deleteCertificate',
					'importSessions',
					'axProgressiveImportSessions',
				    'iframeEnrollmentFieldMassAction',
				),
			);

			$res[] = array(
				'allow',
				'roles' => array('enrollment/update'),
				'actions' => array(
					'editEnrollment',
				),
			);

			$res[] = array(
				'allow',
				'roles' => array('enrollment/delete'),
				'actions' => array(
					'deleteEnrollment',
				),
			);

			$res[] = array(
				'allow',
				'roles' => array('/lms/admin/course/moderate'),
				'actions' => array(
					'waitingEnrollment'
				)
			);
			$res[] = array(
				'allow',
				'users' => array('@'),
				'actions' => array('selfUnenroll', 'getSessions'),
			);
			$res[] = array(
				'allow',
				'users' => array('*'),
				'actions' => array('login'),
			);
			$res[] = array(
				'allow',
				'users' => array('*'),
				'actions' => array('getThumbnails'),
			);
			$res[] = array(
				'deny',
				// deny all
				'users' => array('*'),
				'deniedCallback' => array($this,'adminHomepageRedirect'),
			);

			return $res;
		}

		/**
		 * (non-PHPdoc)
		 * @see CController::init()
		 */
		public function init()
		{
			parent::init();

			JsTrans::addCategories(array('course', 'course_management'));

			if ($this->getAction() == 'enroll') {
				$courseModel = LearningCourse::model()->findByPk($_GET['id']);

				// Player App. component is preloaded. Here we just set its attributes
				Yii::app()->player->setCourse($courseModel);
			}
		}

		/**
		 * Returns list of allowed actions.
		 * @return array actions configuration.
		 */
		//		public function actions() {
		//			return array(
		//				'uploadFile' => 'ext.local.plupload.ActionUploadFile',
		//			);
		//		}

		public function behaviors() {
			$behaviors = parent::behaviors();
			$behaviors['wizard'] = array(
				'class' => 'ext.local.behaviors.WizardBehavior',
				'steps' => array(
					'Select users' => 'selectUser',
					'Select course' => 'selectCourse',
					'Select session' => 'selectSession',
					'Select role' => 'selectRole',
					//'Confirmation' => 'confirmation',
				),
				'events' => array(
					'onStart' => 'wizardStart',
					'onProcessStep' => 'registrationWizardProcessStep',
					'onFinished' => 'wizardFinished',
					'onInvalidStep' => 'wizardInvalidStep',
					'onSaveDraft' => 'wizardSaveDraft'
				),
				'autoAdvance' => false,
				'forwardOnly' => true
			);
			return $behaviors;
		}



		// ------- Wizard Behavior Controls ---------

		public function wizardStart($event) {
			$event->handled = true;
		}


		public function registrationWizardProcessStep($event) {
			switch ($event->step) {
				case 'selectUser':
					$this->stepSelectUser($event);
					break;
				case 'selectCourse':
					$this->stepSelectCourse($event);
					break;
				case 'selectSession':
					$this->stepSelectSession($event);
					break;
				case 'selectRole':
					$this->stepSelectRole($event);
					break;
				default:
					throw new CHttpException(404, 'The requested page does not exist.');
			}
		}

		private function managementError(){
			$userManagementError = array();
			$userManagementError['data']['error'] = Yii::t('user_management', 'Some of the users you are trying to manipulate are not active');
			echo CJSON::encode($userManagementError);
			Yii::app()->end();
		}

		protected function stepSelectUser($event) {
			$modalFirstShow = Yii::app()->session['modalFirstShow'];

			$user = CoreUser::model();
			$user->scenario = 'search';
			$user->unsetAttributes();
			if (isset($_REQUEST['CoreUser'])) {
				$user->attributes = $_REQUEST['CoreUser'];
			}

			$userlist = array();

			// if isset selected users
			if (isset($_REQUEST['courseEnroll-user-grid-selected-items']) && $_REQUEST['courseEnroll-user-grid-selected-items'] != '') {
				$userlist = explode(',', $_REQUEST['courseEnroll-user-grid-selected-items']);
			}

			if (isset($_REQUEST['courseEnroll-orgchart'])) {
				$orgChartGroups = $_REQUEST['courseEnroll-orgchart'];

				$allNodes = array();
				// Get the root node to handle the "allnodes" case
				$rootOrgNode = CoreOrgChartTree::getOrgRootNode();
				foreach ($orgChartGroups as $id => $value) {
					if (($id == $rootOrgNode->idOrg) && ($value != '0')) {
						$allNodes = 'allnodes';
						break;
					}
					if ($value == '1') {
						if (!in_array($id, $allNodes)) {
							$allNodes[] = $id;
						}
					} elseif ($value == '2') {
						$allChildrenNodesIds = CoreUser::model()->getCurrentAndChildrenNodesList($id);
						if (!empty($allChildrenNodesIds)) {
							foreach ($allChildrenNodesIds as $childId) {
								if (!in_array($childId, $allNodes)) {
									$allNodes[] = $childId;
								}
							}
						}
					}
				}

				if (!empty($allNodes)) {
					$criteria = new CDbCriteria();

					// criteria for all users not in current course
					$criteriaAll = CoreUser::model()->dataProviderEnroll(Yii::app()->session['currentCourseId'])->criteria;

					if (($allNodes !== 'allnodes') && (is_array($allNodes))) {
						// criteria for selected nodes
						$criteria->with = array(
							'orgChartGroups' => array(
								'joinType' => 'INNER JOIN',
								'condition' => 'orgChartGroups.idOrg IN (\''.implode('\',\'', $allNodes).'\')',
							),
							'learningCourseusers' => $criteriaAll->with['learningCourseusers'],
						);
						$criteria->together = true;
					} else {
						$criteria = $criteriaAll;
					}

					$users = CoreUser::model()->findAll($criteria);
					foreach ($users as $user) {
						if (!in_array($user->idst, $userlist)) {
							$userlist[] = $user->idst;
						}
					}
				}
			}

			$group = new CoreGroup();
			if (isset($_REQUEST['CoreGroup'])) {
				$group->attributes = $_REQUEST['CoreGroup'];
			}

			if (isset($_REQUEST['group-grid-selected-items'])) {
				if ($_REQUEST['group-grid-selected-items'] != '') {
					$grouplist = $_REQUEST['group-grid-selected-items'];
					$criteria = new CDbCriteria();
					$criteria->with = array(
						'groups' => array(
							'joinType' => 'INNER JOIN',
							'condition' => 'groups.idst IN ('.$grouplist.')',
						),
					);
					$criteria->together = true;
					$users = CoreUser::model()->findAll($criteria);

					foreach ($users as $user) {
						if (!in_array($user->idst, $userlist)) {
							$userlist[] = $user->idst;
						}
					}
				}
			}

			if (!empty($userlist)) {
				$courselist = array(); //$data['courselist']
				if (isset(Yii::app()->session['currentCourseId'])) {
					$courselist[] = Yii::app()->session['currentCourseId'];
				}

				$event->sender->save(array(
					'userlist' => $userlist,
					'courselist' => $courselist,
				));
				$event->handled = true;
				return;
			}

			$processOutput = true;
			if ((isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html')) || (isset($modalFirstShow) && ($modalFirstShow == 'false'))) {
				$processOutput = false;
			}

			$fancyTreeData = CoreOrgChartTree::buildFancytreeDataArray(false, true, true);
			$html = $this->renderPartial('enroll_user', array(
				'userModel' => $user,
				'groupModel' => $group,
				'courseId' => Yii::app()->session['currentCourseId'],
				'fancyTreeData' => $fancyTreeData,
			), true, true);

			if (isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html')) {
				echo $html;
			} else {
				$this->sendJSON(array('html' => $html));
			}
		}



		protected function stepSelectCourse($event) {
			if (Yii::app()->session['currentCourseId'] !== null) {
				$event->sender->save(array());
				$event->handled = true;
				return;
			}

			$courseModel = LearningCourse::model();
			$courseModel->unsetAttributes();
			if (isset($_REQUEST['LearningCourse'])) {
				$courseModel->attributes = $_REQUEST['LearningCourse'];
			}

			if(Yii::app()->user->getIsPu()){
				// If current user is a Power User, apply his filter to the model
				// (so he can see only his courses)
				// @see LearningCourse::dataProviderCatalogue() for how it's used
				$courseModel->powerUserManager = CoreUser::model()->findByPk(Yii::app()->user->getId());
			}

			$courselist = array();
			if (isset($_REQUEST['courseEnroll-course-grid-selected-items']) && ($_REQUEST['courseEnroll-course-grid-selected-items'] != '')) {
				$courselist = explode(',', $_REQUEST['courseEnroll-course-grid-selected-items']);
			}

			if (!empty($courselist)) {
				$data = $event->sender->read('selectUser');

				$data['courselist'] = $courselist;
				$event->sender->save($data, 'selectUser');

				$event->sender->save(array());
				$event->handled = true;
				return;
			}

			$processOutput = true;
			if ((isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html')) || (isset($modalFirstShow) && ($modalFirstShow == 'false'))) {
				$processOutput = false;
			}

            //get the users count to be assigned
            $usersCount = 0;
            if(!empty($_REQUEST['step']) && $_REQUEST['step'] == 'selectCourse') {
                $usersData = $event->sender->read('selectUser');
                if(!empty($usersData['userlist'])) {
                    $usersCount = count($usersData['userlist']);
                }
            }

			$html = $this->renderPartial('enroll_course', array(
				'model' => $courseModel,
				'usersCount' => $usersCount
			), true, $processOutput);

			if (isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html')) {
				echo $html;
			} else {
				$this->sendJSON(array(
					'html' => $html,
					'modalClass' => 'select-course',
					'modalTitle' => Yii::t('report', '_REPORT_COURSE_SELECTION'),
					'backUrl' => Docebo::createAppUrl('admin:courseManagement/enroll')
				));
			}
		}



		protected function stepSelectSession($event) {

			$data = $event->sender->read('selectUser');
			$users = array();
			$postEnrollCourse = Yii::app()->request->getParam('enrollCourse');

			if ($postEnrollCourse != null) {
				/*
				Save the course enrollment data in session and process it in the next step
				Sample data returned:
						array(
								'2' => '3',
								'7' => ''
						)
				Representing:
						first kvp: for course id = '2', enroll users to session id = '3' of the course
						second kvp: for course id = '7', don't enroll users to a specific session, only to the course
				*/
				$data['courseEnroll'] = $postEnrollCourse;
				$event->sender->save($data, 'selectUser');
			} else if (PluginManager::isPluginActive('ClassroomApp')) {

				// check to see if any classroom type courses have been selected in the previous step
				$courselist = $data['courselist'];

				if (!empty ($courselist)) {

					$criteria = new CDbCriteria();
					$criteria->with = array('learningCourseSessions');

					$criteria->addCondition('course_type = :course_type');
					$criteria->params[':course_type'] = LearningCourse::TYPE_CLASSROOM;
					$criteria->addInCondition('idCourse', $courselist);

					$dataProvider = new CActiveDataProvider('LearningCourse', array(
						'criteria' => $criteria,
						'pagination' => false
					));

					if ($dataProvider->getTotalItemCount() > 0) {

						$processOutput = true;
						if ((isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html')) || (isset($modalFirstShow) && ($modalFirstShow == 'false'))) {
							$processOutput = false;
						}

						$html = $this->renderPartial('enroll_sessions', array(
							'dataProvider' => $dataProvider,
						), true, $processOutput);

						if (isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html')) {
							echo $html;
						} else {
							$this->sendJSON(array(
								'html' => $html,
								'modalClass' => 'select-session',
								'modalTitle' => Yii::t('classroom', 'Select a session for each classroom course'),
								'backUrl' => Docebo::createAppUrl('admin:courseManagement/enroll')
							));
						}

					}

				}
			}

			$event->sender->save(array());
			$event->handled = true;
			return;
		}



		protected function stepSelectRole($event) {

			$data = $event->sender->read('selectUser');

			if (Yii::app()->request->getParam('enrollment_role', false) !== false) {
				$role = $_REQUEST['enrollment_role'];

				if (is_numeric($role) && array_key_exists($role, LearningCourseuser::getLevelsArray())) {

					$courseEnroll = (!empty($data['courseEnroll'])) ? $data['courseEnroll'] : array();
					$classroomCourses = array_keys($courseEnroll);

					foreach ($data['userlist'] as $idUser) {
						foreach ($data['courselist'] as $idCourse) {

							// enroll user to the course (unassigned)
							$enrollment = LearningCourseuser::model()->findByAttributes(array(
								'idCourse' => $idCourse,
								'idUser' => $idUser,
							));

							if ($enrollment == null) {
								$enrollment = new LearningCourseuser();
								$enrollment->subscribeUser($idUser, Yii::app()->user->id, $idCourse, 0, $role);
							}

							// enroll user to a specific session
							if (in_array($idCourse, $classroomCourses)) {
								$sessionsToEnrollIn = array();
								$event = new DEvent($this, array('course_id' => $idCourse, 'sessions' => &$sessionsToEnrollIn));
								Yii::app()->event->raise('collectSessionsForClassroomCourse', $event);
								if (!empty($sessionsToEnrollIn)) {
									foreach ($sessionsToEnrollIn as $idSession) {
										LtCourseuserSession::enrollUser($idUser, $idSession);
										}
								} else {
									$idSession = intval( $courseEnroll[$idCourse]['session'] );
									if ($idSession > 0) {
										LtCourseuserSession::enrollUser($idUser, $idSession);
										}
									}
								}
							}
						}

					unset(Yii::app()->session['currentCourseId']);

					$event->sender->save(array());
					$event->handled = true;
					return;
				}
			}

			$html = $this->renderPartial('enroll_role', array(
				'userlist' => $data['userlist'],
				'courselist' => $data['courselist'],
				'userLevels' => $this->_getLevelsArray()
			), true);
			$this->sendJSON(array(
				'html' => $html,
				'modalClass' => 'select-role',
				'modalTitle' => Yii::t('standard', '_SELECT'),
			));
		}

		public function wizardFinished($event) {
			$event->sender->reset();
			$this->sendJSON(array('status' => 'saved'));
		}

		public function wizardInvalidStep($event) {
			Yii::app()->user->setFlash('notice', 'This is not a vaild step in this wizard');
		}



		// ---------------------------------------------------



		/**
		 * This is the default 'index' action that is invoked
		 * when an action is not explicitly requested by users.
		 */
		public function actionIndex() {
            // FancyTree initialization
            Yii::app()->fancytree->init();

			if (PluginManager::isPluginActive('ClassroomApp')) {
				Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/classroom.css');
			}

			$courseModel = LearningCourse::model();

			$courseModel->attributes = $_REQUEST['LearningCourse'];
			// save selected filters
			$attributesForSave                         = $courseModel->attributes;
			$attributesForSave['containChildren']      = $_REQUEST['LearningCourse']['containChildren'];
			$attributesForSave['filterType']           = $_REQUEST['LearningCourse']['filterType'];

			Yii::app()->tinymce;
			//			Yii::app()->plupload;

			$cs = Yii::app()->getClientScript();
			$themeUrl = Yii::app()->theme->baseUrl;

			// FCBK
			$cs->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
			$cs->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');


			$showHints = array();
			if (Yii::app()->user->getIsGodadmin() || Yii::app()->user->getIsAdmin()) {
				// if there are no courses, show the hint on the 'new course' button
				if (0 == $courseModel->count()) {
					$showHints[] = 'bootstroNewCourse';
				}
			}

			//add power user course filter
			//TO DO: implement it in a better way
			if (Yii::app()->user->getIsAdmin()) {
				$courseModel->powerUserManager = CoreUser::model()->findByPk(Yii::app()->user->id);
			}

			// Refister scripts required by new UserseSelector used in the rendered page
			UsersSelector::registerClientScripts();


			//--- Prepare all data required by the Courses Grid widget

			$showLPColumn                    = 0; //default hide LPs column
			$disallowPUEnrollCoursesWithLPs  = 0; //default allow PU to enroll users to courses, that belongs to one or more LPs
			$tooltip                         = 0; //default do not force showing tooltips
			$tooltipTitle                    = ''; //default value for the tooltip title
			$tooltipTitleLP                  = ''; //default value for the LP column tooltip title
			Yii::app()->event->raise('BeforeRenderCourseManagementGrid', new DEvent($this, array(
			    'showLPColumn' => &$showLPColumn,
			    'disallowPUEnrollCoursesWithLPs' => &$disallowPUEnrollCoursesWithLPs,
			    'tooltip' => &$tooltip,
			    'tooltipTitle' => &$tooltipTitle,
			    'tooltipTitleLP' => &$tooltipTitleLP
            )));

			$customCourseDescriptors = array();
			Yii::app()->event->raise('CollectCustomCourseTypes',
			    new DEvent($this, array('types' => &$customCourseDescriptors)));

			$customCourseTypeJsCall = '';
			$customCourseTypeJsCode = '';
			foreach($customCourseDescriptors as $id => $descriptor) {
			    if(isset($descriptor['cell_render_js_call']))
			        $customCourseTypeJsCall .= $descriptor['cell_render_js_call'];
			    if(isset($descriptor['cell_render_js']))
			        $customCourseTypeJsCode .= $descriptor['cell_render_js'];
			}


			// -- Make an array of Grid Parameters (used in _coursesGrid, which is loaded wither throug index or directly rendered partialy)
			$coursesGridParams = array(
			    'columns'                => $this->getCoursesGridColumns($showLPColumn, $tooltipTitleLP, $disallowPUEnrollCoursesWithLPs, $tooltip, $tooltipTitle, $courseModel),
			    'customCourseTypeJsCall' => $customCourseTypeJsCall,
			    'courseModel'            => $courseModel,
			);


			// If this is an AJAX call to update the courses grid, just render the grid (optimization)
			if (isset($_GET['ajax']) && $_GET['ajax'] == 'course-management-grid') {
                $this->renderPartial('_coursesGrid', $coursesGridParams, false, true);
                Yii::app()->end();
			}

			$indexParams = array(
					'courseModel'           => $courseModel,
					'showHints'             => $showHints,
					'coursesGridParams'     => $coursesGridParams,
					'showLPColumn'          => $showLPColumn,
					'disallowPUEnrollCoursesWithLPs' => $disallowPUEnrollCoursesWithLPs,
					'tooltip'               => $tooltip,
					'tooltipTitle'          => $tooltipTitle,
					'tooltipTitleLP'        => $tooltipTitleLP,
					'customCourseTypeJsCall'    => $customCourseTypeJsCall,
					'customCourseTypeJsCode'    => $customCourseTypeJsCode,
			);

			$openModal = Yii::app()->request->getParam('open_modal', false);
			if ($openModal) {
				$idCourse = Yii::app()->request->getParam('id', false);
				if ($idCourse) {
					$indexParams['idCourse'] = $idCourse;
					$indexParams['openModal'] = $openModal;
				}
			}

			$this->render('index', $indexParams);

			//getimagesize('C:/xampp/htdocs/docebo_6.8/lmstmp/3/0/30596_erp_loc/assets/tmp/crop_a328e32b4f31499ad661475f003d6528626e30ce.png');
		}

		public function actionGetCategoryTree() {

			/* @var $rq CHttpRequest */
			$rq = Yii::app()->request;

			$id = $rq->getParam('id', false);
			$ajaxUpdate = $rq->getParam('ajaxUpdate', false);
			$updateTree = $rq->getParam('updateTree', false);
			$reload = ($rq->getParam('reload', 0) > 0);

			if ($rq->isAjaxRequest) {

				if ($reload) {
					//in this case make sure to have the generation time anyway
					$newTreeGenerationTime = CoreRunningOperation::getCurrentTimeMicroseconds();
				}

				if ($ajaxUpdate) {
					Yii::app()->session['currentCourseNodeId'] = $id;
					$nodeModel = LearningCourseCategoryTree::model()->findByPk($id);
					if($nodeModel){
						$ancestors = $nodeModel->ancestors(1)->findAll();

						if($ancestors && isset($ancestors[0]) && isset($ancestors[0]->idCategory) && $ancestors[0]->idCategory){
							Yii::app()->session['currentCourseNodeAncestorId'] = $ancestors[0]->idCategory;
						}
					}
				} else {
					$id = Yii::app()->session['currentCourseNodeId'];
				}

				$default_with = array(
					'coursesCategory' => array(
						'on' => 'coursesCategory.lang_code = :lang',
						'params' => array('lang' => Settings::get('defaultLanguage', 'english'))
					)
				);
				$defaultCourseTree = LearningCourseCategoryTree::model()->with($default_with)->findAll(array('order' => 'coursesCategory.translation'));
                $defaultCourseTree = LearningCourseCategoryTree::arrangeTreeAlphabetical($defaultCourseTree);
				$defaultCourseTreeTranslation = array();
				foreach ($defaultCourseTree as $category) {
					$defaultCourseTreeTranslation[$category->idCategory] = $category->coursesCategory->translation;
				}

				$lang = CoreLangLanguage::model()->findByAttributes(array('lang_browsercode' => Yii::app()->getLanguage()));
				$with = array(
					'coursesCategory' => array(
						'on' => 'coursesCategory.lang_code = :lang',
						'params' => array('lang' => $lang->lang_code)
					)
				);

				if ($updateTree) {
					// is this PU
					$pu = Yii::app()->user->getIsAdmin();
					if ($pu) {
						$criteria = new CDbCriteria;
						$criteria->condition = 'powerUserManagers.idst = :idst';
						$criteria->params[':idst'] = Yii::app()->user->id;
						if(!Yii::app()->event->hasHandler('BeforeChartTreeCategoriesRetrieval'))
							$criteria->group = 't.idCategory';

						$res = LearningCourse::model()->with('powerUserManagers')->findAll($criteria);

						if (!$res)
							return array();

						$_categories = array();
						$_categoryNodes = array();

						$continue = Yii::app()->event->raise('BeforeChartTreeCategoriesRetrieval', new DEvent($this, array(
							'courses' => $res,
							'categories' => &$_categories,
							'categoryNodes' => &$_categoryNodes
						)));

						if ($continue) {

							$cac = CoreAdminCourse::model()->findAllByAttributes(array(
								'idst_user' => Yii::app()->user->id
							));

							foreach($cac as $adminCourse){

								if($adminCourse->type_of_entry !== CoreAdminCourse::TYPE_CATEGORY && $adminCourse->type_of_entry !== CoreAdminCourse::TYPE_CATEGORY_DESC){
									continue;
								}

								if(in_array($adminCourse->id_entry, $_categories)){
									continue;
								}

								$_categories[] = $adminCourse->id_entry;
								$_subcat = LearningCourseCategoryTree::model()->findByPk($adminCourse->id_entry);

								$_categoryNodes[$adminCourse->id_entry] = array(
									'lev' => $_subcat->lev,
									'iLeft' => $_subcat->iLeft,
									'iRight' => $_subcat->iRight,
								);

								if($adminCourse->type_of_entry === CoreAdminCourse::TYPE_CATEGORY_DESC){
									$_subCats1 = Yii::app()->db->createCommand()
										->select('idCategory, lev, iLeft, iRight')
										->from(LearningCourseCategoryTree::model()->tableName().' t')
										->where('t.lev > :lev AND t.iLeft > :iLeft AND t.iRight < :iRight', array(
											':lev' => $_subcat->lev,
											':iLeft' => $_subcat->iLeft,
											':iRight' => $_subcat->iRight,
										))
										->queryAll();

									foreach ($_subCats1 as $_row1) {
										$_categories[] = $_row1['idCategory'];
										$_categoryNodes[$_row1['idCategory']] = array(
											'lev' => $_row1['lev'],
											'iLeft' => $_row1['iLeft'],
											'iRight' => $_row1['iRight'],
										);
									}
								}

								$_subCats = Yii::app()->db->createCommand()
									->select('idCategory, lev, iLeft, iRight')
									->from(LearningCourseCategoryTree::model()->tableName().' t')
									->where('t.lev < :lev AND t.iLeft < :iLeft AND t.iRight > :iRight', array(
										':lev' => $_subcat->lev,
										':iLeft' => $_subcat->iLeft,
										':iRight' => $_subcat->iRight,
									))
									->queryAll();

								foreach ($_subCats as $_row) {
									$_categories[] = $_row['idCategory'];
									$_categoryNodes[$_row['idCategory']] = array(
										'lev' => $_row['lev'],
										'iLeft' => $_row['iLeft'],
										'iRight' => $_row['iRight'],
									);
								}
							}



							foreach ($res as $_row) {
								// get sub categories
								$_categories[] = $_row->idCategory;
								$_subcat = LearningCourseCategoryTree::model()->findByPk($_row->idCategory);

								$_categoryNodes[$_row->idCategory] = array(
									'lev' => $_subcat->lev,
									'iLeft' => $_subcat->iLeft,
									'iRight' => $_subcat->iRight,
								);

								$_subCats = Yii::app()->db->createCommand()
									->select('idCategory, lev, iLeft, iRight')
									->from(LearningCourseCategoryTree::model()->tableName().' t')
									->where('t.lev < :lev AND t.iLeft < :iLeft AND t.iRight > :iRight', array(
										':lev' => $_subcat->lev,
										':iLeft' => $_subcat->iLeft,
										':iRight' => $_subcat->iRight,
									))
									->queryAll();

								foreach ($_subCats as $_row) {
									$_categories[] = $_row['idCategory'];
									$_categoryNodes[$_row['idCategory']] = array(
										'lev' => $_row['lev'],
										'iLeft' => $_row['iLeft'],
										'iRight' => $_row['iRight'],
									);
								}
							}
                        }

						$fullCourseTree = LearningCourseCategoryTree::model()->with($with)->findAll(array(
							'condition' => 't.idCategory IN (' . join(',', $_categories) . ')',
							'order' => 'coursesCategory.translation'
						));
					} else {
						$fullCourseTree = LearningCourseCategoryTree::model()->with($with)->findAll(array('order' => 'coursesCategory.translation'));
					}

                    $fullCourseTree = LearningCourseCategoryTree::arrangeTreeAlphabetical($fullCourseTree);

					$disableNodeActions = array();
					$params = array(
						'pu' => $pu,
						'_categoryNodes' => (isset($_categoryNodes) ? $_categoryNodes : array()),
						'currentNodeId' => $id,
						'fullCourseTree' => $fullCourseTree,
						'defaultCourseTreeTranslation' => $defaultCourseTreeTranslation,
						'disableNodeActions' => &$disableNodeActions
					);

					Yii::app()->event->raise('BeforeChartTreeRender', new DEvent($this, $params));

					$newTreeGenerationTime = CoreRunningOperation::getCurrentTimeMicroseconds();
					$params['treeGenerationTime'] = $newTreeGenerationTime;
					$content = $this->renderPartial('_chartTree', $params, true);
				}

				if (!empty($id) && $id > 1) {
					$currentNode = LearningCourseCategoryTree::model()->findByPk($id);
					$breadcrumbs = $this->getTreeBreadcrumbs($currentNode, $defaultCourseTreeTranslation);
				} else {
					$root = LearningCourseCategoryTree::model()->roots()->with($with)->find();
					$breadcrumbs = $this->getTreeRootBreadcrumbs($root, $defaultCourseTreeTranslation);
				}

				echo CJSON::encode(array(
					'treeGenerationTime' => $newTreeGenerationTime,
					'content' => $content,
					'breadcrumbs' => $breadcrumbs
				));
			}
			Yii::app()->end();
		}



        public function getTreeRootBreadcrumbs($root, $defaultCourseTreeTranslation) {
			$node_name = $root->coursesCategory->translation;
			if (!$node_name) $node_name = $defaultCourseTreeTranslation[$root->idCategory];
			$breadcrumbs = '<ul class="clearfix"><li class="last-node">' . $node_name . '</li></ul>';
			return $breadcrumbs;
		}



		public function getTreeBreadcrumbs($currentNode, $defaultCourseTreeTranslation) {
			if (!empty($currentNode)) {
				$breadcrumbsNodes = $currentNode->ancestors()->findAll();
				$breadcrumbs      = '';
				if (!empty($breadcrumbsNodes)) {
					$breadcrumbs .= '<ul class="clearfix">';
					foreach ($breadcrumbsNodes as $breadcrumbsNode) {
						$url = Yii::app()->createUrl('courseManagement/getCategoryTree');
						if ($breadcrumbsNode->idCategory > 1) {
							$url .= '&id=' . $breadcrumbsNode->idCategory;
						}
						$node_name = $breadcrumbsNode->coursesCategoryTranslated->translation;
						if (!$node_name) $node_name = $defaultCourseTreeTranslation[$breadcrumbsNode->idCategory];

						$breadcrumbs .= '<li>' . CHtml::link($node_name, 'javascript:void(0);', array('class' => 'ajaxLinkNode', 'data-url' => $url)) . '</li>';
					}
					$node_name = $currentNode->coursesCategoryTranslated->translation;
					if (!$node_name) $node_name = $defaultCourseTreeTranslation[$currentNode->idCategory];
					$breadcrumbs .= '<li class="last-node">' .$node_name . '</li>';
					$breadcrumbs .= '</ul>';
				}
				return $breadcrumbs;
			}
			return '';
		}

		/**
		 * Create new course
		 */
		public function actionCreateCourse() {

			$rq = Yii::app()->request;
			$selectedImage = $rq->getParam('selectedImage', null);
			// Handle Delete Logo Image AJAX calls
			// It will end application if everything is going fine
			$this->handleDeleteLogoAjax();


			$learningCourse = new LearningCourse();
			if (!empty($_POST['LearningCourse'])) {
				$learningCourse->setScenario('create');
				$learningCourse->setCourseDefaults();
				$learningCourse->attributes = $_POST['LearningCourse'];
				if ($learningCourse->validate()) {
					$learningCourse->idCategory = Yii::app()->session['currentCourseNodeId'];
					$learningCourse->img_course = !empty($_POST['LearningCourse']['selectedImage']) ? $_POST['LearningCourse']['selectedImage'] : null;
					if(!$learningCourse->img_course && $selectedImage){
						$learningCourse->img_course = intval($selectedImage);
					}
					$learningCourse->saveCourse();
					Yii::app()->session['newCourseId'] = $learningCourse->idCourse;
					$this->sendJSON(array());
				}
			}
			if (Yii::app()->request->isAjaxRequest) {
				list($count, $defaultImages, $userImages, $selected) = $this->prepareThumbnails($learningCourse);

				$content = '';
				$params = array(
					'currentCourseNodeId' => Yii::app()->session['currentCourseNodeId'],
					'html' => &$content
				);

				if($userImages && isset($userImages[0])){
					$userImages = array(0 =>$userImages[0]);
				}

				if (Yii::app()->event->raise('BeforeCourseManagementCreateRender', new DEvent($this, $params))) {
					$content = $this->renderPartial('create', array(
						'model' => $learningCourse,
						'userImages' => $userImages,
						'defaultImages' => $defaultImages,
						'count' => $count,
						'selectedTab' => $selected,
					), true);
				}

				$this->sendJSON(array(
					'html' => $content,
				));
			}
			Yii::app()->end();
		}

		public function actionGetThumbnails(){

			$assets = new CoreAsset();
			$rq = Yii::app()->request;
			$pageId = $rq->getParam('pageId', 0);
			$totalCount = $rq->getParam('totalCount', false);
			$courseModelId = $rq->getParam('courseId', false);
			$selectedImageId = $rq->getParam('selectedImageId', null);
			$imagesArray = array();
			$assets = new CoreAsset();
			$userImages = array();

			$offset = $pageId*10;
			$maxPages =  ceil($totalCount/10) - 1;
			if($offset < 0){
				$offset = $maxPages*10;
				$pageId =  $maxPages;
			}elseif($pageId > $maxPages){
				$offset = 0;
				$pageId = 0;
			}
			if($courseModelId){
				$learningCourse = LearningCourse::model()->findByPk($courseModelId);
				if($learningCourse){
					if($learningCourse->img_course && !$selectedImageId){
						$selectedImageId = $learningCourse->img_course;
					}
					$userImages  = $assets->urlList(CoreAsset::TYPE_COURSELOGO, CoreAsset::SCOPE_PRIVATE, false, CoreAsset::VARIANT_SMALL);
					foreach ($userImages as $key => $value) {
						if ($key == $learningCourse->img_course) {
							$this->reorderArray($userImages, $key, $value);
							break;
						}
					}
					$userImages = array_chunk($userImages, 10, true);
				}
				if(isset($userImages[$pageId])){
					$userImages = $userImages[$pageId];
				}elseif(isset($userImages[$pageId-1])){
					$userImages = $userImages[$pageId-1];
					$pageId = $pageId-1;
				}elseif(isset($userImages[0])){
					$userImages = $userImages[0];
					$pageId = 0;
				}

			}else{
				$learningCourse = new LearningCourse();
				$userImages = $assets->urlList(CoreAsset::TYPE_COURSELOGO, CoreAsset::SCOPE_PRIVATE, false, CoreAsset::VARIANT_SMALL, $offset , 10);

			}

			if(!$userImages){
				$pageId = 0;
				if($pageId < 0){
					$pageId = $maxPages-1;
				}
				$offset = $pageId*10;
				$userImages = $assets->urlList(CoreAsset::TYPE_COURSELOGO, CoreAsset::SCOPE_PRIVATE, false, CoreAsset::VARIANT_SMALL, $offset , 10);
			}

			$html = $this->renderPartial('_carouselThumbnails', array(
				'model' => $learningCourse,
				'userImages' => $userImages,
				'selectedImageId' => $selectedImageId,

			),true,true);
			$this->sendJSON(array(
				'html' => $html,
				'currentPage' => $pageId,
			));

			Yii::app()->end();

		}


		public function actionProcessNewCourse() {
			Yii::import('common.widgets.MainMenu');
			$courseId = Yii::app()->session['newCourseId'];
			$courseModel = LearningCourse::model()->findByPk($courseId);
			$categoryIssue = false;
			if(isset($courseModel->idCategory) && $courseModel->idCategory){
				$categoryTree = LearningCourseCategoryTree::model()->findByPk($courseModel->idCategory);
				if(!$categoryTree){
					$categoryIssue = true;
				}elseif($categoryTree){
					$ancestors = $categoryTree->ancestors(1)->findAll();

					if($ancestors && isset($ancestors[0]) && isset($ancestors[0]->idCategory) && $ancestors[0]->idCategory
						&&  isset(Yii::app()->session['currentCourseNodeAncestorId']) && ($ancestors[0]->idCategory != Yii::app()->session['currentCourseNodeAncestorId'])){
							$categoryIssue = true;
							unset(Yii::app()->session['currentCourseNodeAncestorId']);

					}elseif(isset(Yii::app()->session['currentCourseNodeId']) && Yii::app()->session['currentCourseNodeId'] && $categoryTree->idCategory && ($categoryTree->idCategory != Yii::app()->session['currentCourseNodeId'])){
						$categoryIssue = true;
						unset(Yii::app()->session['currentCourseNodeId']);
					}
				}
			}

			unset(Yii::app()->session['newCourseId']);
			$this->sendJSON(array('html' => $this->renderPartial('_processNewCourse', array('courseId' => $courseId, 'courseModel' => $courseModel, 'categoryIssue' => $categoryIssue), true)));
		}

		public function actionGetSlidersContent() {
			if (Yii::app()->request->isAjaxRequest) {
				$id = !empty($_REQUEST['id'])?$_REQUEST['id']:'';
				$learningCourse = LearningCourse::model()->findByPk($id);
				if (empty($learningCourse)) {
					$learningCourse = new LearningCourse();
				}
				list($count, $defaultImages, $userImages, $selected) = $this->prepareThumbnails($learningCourse);
				if(isset($userImages[0])){
					$userImages = array(0 => $userImages[0]);
				}
				$uniqueId = substr(md5(uniqid(rand(), true)), 0, 10);
				$content = $this->renderPartial('_sliders', array(
					'model' => $learningCourse,
					'defaultImages' => $defaultImages,
					'userImages' => $userImages,
					'count' => $count,
					'selectedTab' => 'user',
					'uniqueId' => $uniqueId,

				), true);
				$this->sendJSON(array('content' => $content));
				// return $content;
			}
			return false;
		}



		/**
		 * Update course
		 */
		public function actionUpdateCourse($id) {

			$rq = Yii::app()->request;
			$selectedImage = $rq->getParam('selectedImage', null);
			$learningCourse = LearningCourse::model()->findByPk($id);
			$learningCourse->name = htmlspecialchars_decode($learningCourse->name, ENT_NOQUOTES);
			if (empty($learningCourse)) {
				$this->sendJSON(array());
			}

			// Handle Delete Logo Image AJAX calls
			// It will end application if everything is going fine
			$this->handleDeleteLogoAjax();

			if (!empty($_POST['LearningCourse'])) {

				if(isset($_POST['LearningCourse']['course_type']))
					unset($_POST['LearningCourse']['course_type']);

				$learningCourse->setScenario('update');
				$learningCourse->attributes = $_POST['LearningCourse'];
				if($selectedImage){
					$learningCourse->img_course = intval($selectedImage);
				}

				if (!empty($_POST['LearningCourse']['selectedImage']))
					$learningCourse->img_course = $_POST['LearningCourse']['selectedImage'];

				if ($learningCourse->validate()) {
					$learningCourse->save();
					Yii::app()->session['newCourseId'] = $learningCourse->idCourse;
					$this->sendJSON(array());
				}
			}
			if (Yii::app()->request->isAjaxRequest) {
				list($count, $defaultImages, $userImages, $selected) = $this->prepareThumbnails($learningCourse);
				if(isset($userImages[0])){
					$userImages = array(0 =>$userImages[0]);
				}
				$content = $this->renderPartial('update', array(
					'model' => $learningCourse,
					'userImages' => $userImages,
					'defaultImages' => $defaultImages,
					'count' => $count,
					'selectedTab' => $selected,
				), true);

				$this->sendJSON(array(
					'html' => $content,
				));
			}
			Yii::app()->end();
		}



		private function prepareThumbnails($model) {
			$gallerySize = 10;
			$path = Yii::app()->theme->basePath . '/images/course_tb';

			// Get list of URLs of SHARED and USER player backgrounds
			$assets = new CoreAsset();
			$defaultImages   = $assets->urlList(CoreAsset::TYPE_COURSELOGO, CoreAsset::SCOPE_SHARED, false, CoreAsset::VARIANT_SMALL);
			$userImages   	= $assets->urlList(CoreAsset::TYPE_COURSELOGO, CoreAsset::SCOPE_PRIVATE, false, CoreAsset::VARIANT_SMALL);

			$count = array(
				'default' => count($defaultImages),
				'user' => count($userImages),
			);

			foreach ($defaultImages as $key => $value) {
				if ($key == $model->img_course) {
					$selected = 'default';

					$this->reorderArray($defaultImages, $key, $value);
					break;
				}
			}

			if (empty($selected)) {
				foreach ($userImages as $key => $value) {
					if ($key == $model->img_course) {
						$selected = 'user';
						$this->reorderArray($userImages, $key, $value);
						break;
					}
				}
			}

			$selected = empty($selected) ? 'default' : $selected;

			$defaultImages = array_chunk($defaultImages, $gallerySize, true);
			$userImages = array_chunk($userImages, $gallerySize, true);

			return array($count, $defaultImages, $userImages, $selected);
		}


		/**
		 * Make a copy of a course ID $id
		 *
		 * @param number $id
		 * @throws CException
		 */
		public function actionCopyCourse($id) {

            /* @var $learningCourse LearningCourse */
			$learningCourse = LearningCourse::model()->findByPk($id);

			if (empty($learningCourse)) {
				$this->sendJSON(array());
			}

			if (!empty($_POST['LearningCourse'])) {

				// DB Start transaction
				$transaction = Yii::app()->db->beginTransaction();

				try {
					$learningCourseCopy             = new LearningCourse();
					$learningCourseCopy->scenario 	= 'combocopy'; // makes virtual properties safe to assign
					$learningCourseCopy->attributes = $learningCourse->attributes;
					$learningCourseCopy->attributes = $_POST['LearningCourse'];
					$learningCourseCopy->img_course = $learningCourse->img_course;
					$learningCourseCopy->player_bg 	= $learningCourse->player_bg;

					if ($learningCourseCopy->validate()) {
						$learningCourseCopy->save(false);

						if (Yii::app()->user->getIsPu()) {
							//if a power user is creating a new course, the course must be assigned to himself
							$existing = CoreAdminCourse::model()->findByPk(array(
								'idst_user' => Yii::App()->user->id,
								'type_of_entry' => CoreAdminCourse::TYPE_COURSE,
								'id_entry' => $learningCourseCopy->getPrimaryKey()
							));
							if (!$existing) {
								$adminCourse = new CoreAdminCourse();
								$adminCourse->idst_user = Yii::App()->user->id;
								$adminCourse->type_of_entry = CoreAdminCourse::TYPE_COURSE;
								$adminCourse->id_entry = $learningCourseCopy->getPrimaryKey();
								if (!$adminCourse->save()) {
									throw new CException('Error while updating power user course data');
								}
							}

							$existing = CoreUserPuCourse::model()->findByPk(array(
								'puser_id' => Yii::app()->user->id,
								'course_id' => $learningCourseCopy->getPrimaryKey()
							));
							if (!$existing) {
								$puCourse = new CoreUserPuCourse();
								$puCourse->puser_id = Yii::app()->user->id;
								$puCourse->course_id = $learningCourseCopy->getPrimaryKey();
								if (!$puCourse->save()) {
									throw new CException('Error while updating power user course data');
								}
							}
						}

						// To copy or not to copy is determined by checkboxes in UI

						// COPY LOs ?
						if ($learningCourseCopy->copyTraining) {
							if (!$learningCourse->copyLearningObjectsTo($learningCourseCopy->idCourse)) {
								throw new CException(Yii::t('standard','_OPERATION_FAILURE'));
							}
						}

						// Copy Enrollments ?
						if ($learningCourseCopy->copyEnrollments) {
							$allowOnlyStudentEnrollments = $this->_allowOnlyStudentEnrollments();
							$enrollParams = $allowOnlyStudentEnrollments ? array('force_level'=>LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT) : false;
							if (!$learningCourse->copyEnrollmentsTo($learningCourseCopy->idCourse, $enrollParams)) {
								throw new CException(Yii::t('standard','_OPERATION_FAILURE'));
							}
						}

						// Copy blocks ?
						if ($learningCourseCopy->copyBlockWidgets) {
							if (!$learningCourse->copyBlockWidgetsTo($learningCourseCopy->idCourse)) {
								throw new CException(Yii::t('standard','_OPERATION_FAILURE'));
							}
						}

						$transaction->commit();
						$this->sendJSON(array());
					}
					else {
						throw new CException(Yii::t('standard','_OPERATION_FAILURE'));
					}

				}
				catch (CException $e) {
					$transaction->rollback();
					$content = $e->getMessage();
					$this->sendJSON(array(
							'html' => $content,
					));
				}

			}

			if (Yii::app()->request->isAjaxRequest) {
				$content = $this->renderPartial('copy', array(
					'model' => $learningCourse,
				), true);
				$this->sendJSON(array(
					'html' => $content,
				));
			}
			Yii::app()->end();
		}

		public function actionDeleteCourse($id) {
			$learningCourse = LearningCourse::model()->findByPk($id);
			if (empty($learningCourse)) {
				$this->sendJSON(array());
			}

			if (!empty($_POST['LearningCourse'])) {
				if ($_POST['LearningCourse']['confirm']) {
					$learningCourse->delete();
					Log::_('DELETION: Course with id '.$id.' deleted by user '.Yii::app()->user->id, CLogger::LEVEL_WARNING);
					$this->sendJSON(array());
				}
			}
			if (Yii::app()->request->isAjaxRequest) {
				$content = $this->renderPartial('delete', array(
					'model' => $learningCourse,
				), true);
				$this->sendJSON(array(
					'html' => $content,
				));
			}
			Yii::app()->end();
		}

		public function actionCourseAutocomplete() {
            $requestData = Yii::app()->request->getParam('data', array());

			// What will the format of the final JSON returned array be
			$responseType = Yii::app()->request->getParam('responseType', 'quartz');

            $query = '';
			if(!empty($requestData['query'])) {
                $query = addcslashes($requestData['query'], '%_');
            }

			if(!$query){
                $term = Yii::app()->request->getParam('term', '');
				$query = addcslashes($term, '%_');
			}

			$condition = 'name LIKE :match OR name LIKE :match2';
			$params = array(
				':match' => "/%$query%",
				':match2' => "%$query%"
			);
			if(isset($requestData['formattedCourseName']) && $requestData['formattedCourseName']) {
				$condition .= ' OR CONCAT(name, " - ", code) LIKE :match3';
				$params[':match3'] = "%$query%";
			}
			$criteria = new CDbCriteria(array(
				'condition' => $condition,
				'params' => $params,
			));

			if (Yii::app()->user->getIsPu()) {
				$criteria->with['powerUserManagers'] = array(
					'condition' => 'powerUserManagers.idst=:puser_id',
					'params' => array(':puser_id' => Yii::app()->user->id),
					'together' => true,
				);
			} elseif (isset($requestData['powerusermanager'])) {
				$criteria->with['powerUserManagers'] = array(
					'condition' => 'powerUserManagers.idst=:puser_id',
					'params' => array(':puser_id' => $requestData['powerusermanager']),
					'together' => true,
				);
			} elseif (!empty($requestData['catalogue']) && is_numeric($requestData['catalogue'])) {

				$criteria->with['learningCatalogueEntry'] = array(
					'condition' => 'learningCatalogueEntry.idCatalogue=:catalogue',
					'params' => array(':catalogue' => $requestData['catalogue']),
					'together' => true,
				);

				// $criteriaConfig['with'] = 'learningCatalogueEntry';
				// $criteriaConfig['condition'] = '('.$criteriaConfig['condition'].') AND learningCatalogueEntry.idCatalogue=:catalogue';
				// $criteriaConfig['params'][':catalogue'] = $_REQUEST['data']['catalogue'];
			} elseif (!empty($requestData['coupon']) && is_numeric($requestData['coupon'])) {
                // filter by coupon id
                $criteria->with['ecommerceCoupons'] = array(
                    'condition' => 'ecommerceCoupons.id_coupon = :id_coupon',
                    'params' => array(':id_coupon' => intval($requestData['coupon']))
                );
            }

			if (isset($requestData['poweruserNot'])) {
				$powerUserModel = CoreUser::model()->findByPk($requestData['poweruserNot']);
				$criteria->with['powerUserManagers'] = array(
					'joinType' => 'LEFT JOIN',
				);
				if (!empty($powerUserModel->manageCourses)) {
					$criteria->with['powerUserManagers']['condition'] = 't.idCourse NOT IN ('.implode(',', CHtml::listData($powerUserModel->manageCourses, 'idCourse', 'idCourse')).') OR powerUserManagers.idst IS NULL';
				}
				$criteria->together = true;
				$criteria->group = 't.idCourse';
			}

			if(!PluginManager::isPluginActive('ClassroomApp'))
				$criteria->addCondition("t.course_type <> 'classroom'");

			$courses = LearningCourse::model()->findAll($criteria);
//			if(isset($requestData['formattedCourseName']) && $requestData['formattedCourseName']) {
//				$coursesList = CHtml::listData($courses, 'idCourse', 'courseNameFormatted');
//			} else {
            $coursesList = CHtml::listData($courses, 'idCourse', 'name');

            //Remove the HTML entities
            foreach ($coursesList as $key => $course){
                $coursesList[$key] = html_entity_decode($course);
            }
//			}

            // get also Coursepaths of the power user
            if(!empty($requestData['powerusermanager'])) {
                // also search for learning plans added to this power user assignments
                $coursePathNames = Yii::app()->getDb()->createCommand()
                    ->select('t.path_name')
                    ->from(LearningCoursepath::model()->tableName().' t')
                    ->join(CoreUserPuCoursepath::model()->tableName().' cupucp', 'cupucp.path_id=t.id_path AND cupucp.puser_id=:puser_id',
                        array(':puser_id' => (int) $requestData['powerusermanager']))
                    ->where('t.path_name LIKE :path_name OR t.path_name LIKE :path_name2',
                        array(':path_name'=>"%$query%", ':path_name2'=>"/%$query%"))
                    ->queryColumn();

                $coursesList = array_merge($coursesList, $coursePathNames);

                // also search for categories added to this power user assignments
                $categoryPathNames = Yii::app()->getDb()->createCommand()
                    ->select('c2.translation AS name')
                    ->from(CoreAdminCourse::model()->tableName().' cat_assigned')
                    ->join(LearningCourseCategory::model()->tableName().' c2',
                        'cat_assigned.id_entry=c2.idCategory AND cat_assigned.idst_user=:puserId AND c2.lang_code=:lang
                        AND cat_assigned.type_of_entry IN (:cat, :catDesc)',
                        array(':puserId' => (int) $requestData['powerusermanager'],
                            ':lang' => Lang::getCodeByBrowserCode(Yii::app()->getLanguage()),
                            ':cat' => CoreAdminCourse::TYPE_CATEGORY,
                            ':catDesc' => CoreAdminCourse::TYPE_CATEGORY_DESC))
                    ->where('c2.translation LIKE :translation OR c2.translation LIKE :translation2',
                        array(':translation'=>"%$query%", ':translation2'=>"/%$query%"))
                    ->queryColumn();

                $coursesList = array_merge($coursesList, $categoryPathNames);
            }

			if(isset($requestData['catalogue']) && isset($requestData['onlyassignedcourses'])){
				// Now that we can also add Learning Plans to catalogs
				// also search for learning plans added to this catalog
				if($requestData['onlyassignedcourses']){
					$catalogNames = Yii::app()->getDb()->createCommand()
						->select('t.path_name')
						->from(LearningCoursepath::model()->tableName().' t')
						->join(LearningCatalogueEntry::model()->tableName().' ce', 'ce.idEntry=t.id_path AND ce.type_of_entry="coursepath" AND ce.idCatalogue=:idCatalogue', array(':idCatalogue'=>$requestData['catalogue']))
						->where('t.path_name LIKE :path_name', array(':path_name'=>'%'.$requestData['query'].'%'))
						->queryColumn();

					$coursesList = array_merge($coursesList, $catalogNames);
				}
			}

			// Cut long course names to 25 characters
			/*foreach($coursesList as $key=>$course) {
				$newCoursesList[$key] = Docebo::ellipsis($course, 25, "...");
			}

            $courseCodesList = CHtml::listData($courses, 'idCourse', 'code');
			foreach($courseCodesList as $key=>$code) {
			    if($code != '')
			        $newCoursesList[$key] .= ' - ' . $code;
            }*/

			switch($responseType){
				case 'cjuiautocomplete':
					$this->sendJSON($coursesList);
					break;
				case 'quartz':default:
					$this->sendJSON(array('options' => $coursesList));
			}
		}

		public function actionCourseEnrollmentsAutocomplete() {
			$query = addcslashes($_REQUEST['data']['query'], '%_');
			$currentCourseId = Yii::app()->session['currentCourseId'];

			$criteria = new CDbCriteria(array(
				'condition' => '(name LIKE :match OR name LIKE :match2) AND (idCourse <> :currentCourseId)',
				'params' => array(
					':match' => "/%$query%",
					':match2' => "%$query%",
					':currentCourseId' => $currentCourseId,
				),
			));

			// If current user is a Power User, restrict courses to assigned only (JOIN core user table through course pu table)
			if (Yii::app()->user->getIsAdmin()) {
				$criteria->with['powerUserManagers'] = array(
					'condition' => 'powerUserManagers.idst=' . Yii::app()->user->id,
				);
			}

			$courses     = LearningCourse::model()->findAll($criteria);
			$coursesList = array_values(CHtml::listData($courses, 'idCourse', 'name'));
			$this->sendJSON(array('options' => $coursesList));
		}

		public function actionEnrollment() {
			Yii::app()->clientScript->registerCoreScript('jquery.ui');
			Yii::app()->fancytree->register();
			Yii::app()->tinymce->init();
			Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/../admin/js/form.change.auto.submit.js');

			$cs = Yii::app()->getClientScript();
			$themeUrl = Yii::app()->theme->baseUrl;

			// FCBK
			$cs->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
			$cs->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');
			
			$cs->registerScriptFile($themeUrl . '/js/enrollment_custom_field_iframe.js');

			//sometimes it may happen that some enrolled/imported users have a invalid enrollment level. Trying to fix it.
			LearningCourseuser::fixEnrollmentsLevel();

			$id = Yii::app()->request->getParam('id');
			if(!$id){
				$this->redirect(Yii::app()->createUrl('courseManagement/index'));
			}

                           $pUserRights = Yii::app()->user->checkPURights($id, Yii::app()->user->id);
				// If this is a PU, check if the course is assigned to him first
			if ($pUserRights->isPu && !$pUserRights->courseIsAssigned && !$pUserRights->isInstructor) {
					// This course is not assigned to the PU
					$this->adminHomepageRedirect();
				}

			$courseModel = LearningCourse::model()->findByPk($id);
			if(!$courseModel)
				$this->redirect(Yii::app()->createUrl('courseManagement/index'));

			/*
			 * If user is PU redirect to Course management page if he's on Enrollment page but he's not an Instructor,
			 * no enrollments and no permission to enroll users
			*/
			if($pUserRights->isPu) {
				$countEnrollments = LearningCourseuser::getCountEnrolledByLevel($courseModel->idCourse, null, false);
				$canEnroll = (Yii::app()->user->isGodAdmin || ($pUserRights->all && Yii::app()->user->checkAccess('enrollment/view')));
				if ((!$pUserRights->isInstructor && $countEnrollments < 1) && !$canEnroll)
					$this->redirect(Yii::app()->createUrl('courseManagement/index'));

			}

			// Let "player" Yii component know about the course we are in
			// This will allow Main Menu to show "course related" tiles: "play" and "manage courses"
			Yii::app()->player->setCourse($courseModel);

			$model = CoreUser::model();
			$model->scenario = 'search';

			$model->learningCourse = LearningCourseuser::model();
			$model->learningCourse->idCourse = $id;
			$model->learningCourse->waiting = 0;

			Yii::app()->session['currentCourseId'] = $id;

			if (isset($_REQUEST['CoreUser'])) {
				$model->attributes = $_REQUEST['CoreUser'];
			}

			if (isset($_REQUEST['LearningCourseuser'])) {
				$model->learningCourse->attributes = $_REQUEST['LearningCourseuser'];
				if ($_REQUEST['LearningCourseuser']['level'] == '') {
					$model->learningCourse->unsetAttributes(array('level'));
				}
			}

			if (isset($_REQUEST['node'])) {
				Yii::app()->session['enrollNodeId'] = $_REQUEST['node'];
			} elseif (!isset($_REQUEST['pager']) && !isset($_REQUEST['data']['autocomplete'])) {
				unset(Yii::app()->session['enrollNodeId']);
			}

            if (isset($_REQUEST['group'])) {
                Yii::app()->session['enrollGroupId'] = $_REQUEST['group'];
            } elseif (!isset($_REQUEST['pager']) && !isset($_REQUEST['data']['autocomplete'])) {
                unset(Yii::app()->session['enrollGroupId']);
            }

			// is used for autocomplete
			if (isset($_REQUEST['data']['autocomplete'])) {
				$model = Yii::app()->session[$this->action->id.'FilterModel'];
				$model->search_input = addcslashes($_REQUEST['data']['query'], '%_');
				$dataProvider = $model->dataProviderEnrollment();
				$dataProvider->pagination = false;
				$result = array('options' => array());
				foreach ($dataProvider->data as $user) {
					$result['options'][] = Yii::app()->user->getRelativeUsername($user->userid);
				}
				$this->sendJSON($result);
			}
			Yii::app()->session[$this->action->id.'FilterModel'] = $model;

			// show overlay hints for admin
			$showHints = false;
            //Check if the PowerUsers has available seats for the course, to show the hints!
            $puSeats = CoreUserPuCourse::getTotalAndAvailableSeats($id);

                           $pUserRights = Yii::app()->user->checkPURights($id, Yii::app()->user->id);

			if (Yii::app()->user->getIsGodadmin() || ($pUserRights->all && $puSeats['available'] != 0) || TRUE) {
				$enrollments = $model->dataProviderEnrollment();
				$showHints = (0 == $enrollments->itemCount);
			}


			// Register scripts required by new UsersSelector used in the rendered page
			UsersSelector::registerClientScripts();

			$this->render('enrollment', array(
				'model' => $model,
				'courseModel' => $courseModel,
				'showHints' => $showHints
			));
		}

		public function actionWaitingEnrollment($id) {

			$model = LearningCourseuser::model();
			$model->idCourse = $id;
			$model->waiting = 1;

			Yii::app()->session['currentCourseId'] = $id;

			$user = $model->getUser();
			$user->scenario = 'search';
			if (isset($_REQUEST['CoreUser'])) {
				$user->attributes = $_REQUEST['CoreUser'];
			}

			if (isset($_REQUEST['LearningCourseuser']))
				$model->attributes = $_REQUEST['LearningCourseuser'];
			else
				$model->overbooked = 3;

			$this->render('waitingEnrollment', array('model' => $model));
		}

		public function actionSendEmails(){
			$ids = '';
			$emails = '';
			if(is_array($_REQUEST['ids'])){
				$ids = implode(',', $_REQUEST['ids']);
			}elseif(is_string($_REQUEST['ids'])){
				$ids = $_REQUEST['ids'];
			}

			// IF there is no selected items
			if(!$ids)
				$this->sendJSON(array(
					'status' => 'error', // success|error
					'html' => Yii::t('standard', 'No users selected'),
				));

			//Emails
			$emails = Yii::app()->db->createCommand()
				->select('GROUP_CONCAT(DISTINCT email SEPARATOR ",")')
				->from('core_user cu')
				->where(array('in', 'idst', explode(',', $ids)))
				->andWhere('email != ""')
				->queryScalar();

			// The $emails will always contain string, empty or not
			if(!$emails)
				$this->sendJSON(array(
					'status' => 'error', // success|error
					'html' => Yii::t('standard', 'The selected users have no email configured') .
					"<script>$('.btn-submit').remove();</script>",
				));

			$errors = array();
			$status = 'success';

			if(isset($_POST['from']) || isset($_POST['body']) || isset($_POST['subject'])){
				// form submitter, send emails

				// Do validation on form submit
				// .. in a very old school way... come on guys, active forms are for that :(
				if(isset($_POST['subject']) && empty($_POST['subject'])){
					$errors['subject'] = Yii::t('yii','{attribute} cannot be blank.', array('{attribute}'=>Yii::t('standard', '_SUBJECT')));
				}
				if(isset($_POST['body']) && empty($_POST['body'])){
					$errors['body'] = Yii::t('yii','{attribute} cannot be blank.', array('{attribute}'=>Yii::t('standard', '_TEXTOF')));
				}
				if(isset($_POST['from']) && empty($_POST['from'])){
					$errors['from'] = Yii::t('yii','{attribute} cannot be blank.', array('{attribute}'=>Yii::t('standard', '_FROM')));
				} else {
					$validator = new CEmailValidator;
					if(!$validator->validateValue($_POST['from'])){
						$errors['from'] = Yii::t('yii','{attribute} is not a valid email address.', array('{attribute}'=>Yii::t('standard', '_SENDER')));
					}elseif(strpos($_POST['from'], 'docebosaas') !== false){
						$errors['from'] = Yii::t('notification', 'Please provide valid sender email');
					}

					if(!empty($_POST['ccMails']) && !$validator->validateValue($_POST['ccMails'])){
						$ccEmails = explode(',', $_POST['ccMails']);
						$ccEmails = array_map('trim', $ccEmails);
						$ccEmails = array_filter($ccEmails);
						foreach ($ccEmails as $ccMail) {
							if (!$validator->validateValue($ccMail)) {
								$errors['ccMails'] = Yii::t('yii', '{attribute} is not a valid email address(es).', array('{attribute}' => Yii::t('standard', 'One or more emails from CC field')));
								break;
							}
						}
					}
				}

				if(empty($errors)) {
					if(!empty($_POST['ccMails'])) {
						$ccMails = $_POST['ccMails'];
					}else{
						$ccMails = '';
					}

					// Progressive send mail
					$html = $this->renderPartial('_mass_send_email_to_enrolled_users', array(
							'users' => $ids, // Build string with the all users ids
							'from' => $_POST['from'],
							'subject' => addslashes($_POST['subject']),
							'message' => addslashes($_POST['body']),
							'courseId' => intval($_POST['courseId']),
							'sessionId' => intval($_POST['sessionId']),
							'ccMails' => $ccMails
					), true);

					$this->sendJSON(array('preserveSelection' => true, 'html' => $html . '<a class="auto-close"></a>'));
					Yii::app()->end();
				}
			}

			$shortcodes = array(
					'username' => '[username]',
					'first_name' => '[first_name]',
					'last_name' => '[last_name]',
					'email' => '[email]',
					'course_code' => '[course_code]',
					'course_name' => '[course_name]',
					'course_description' => '[course_description]',
					'course_link' => '[course_link]'
			);
			$courseId = intval($_POST['courseId']);
			$sessionId = intval($_POST['sessionId']);
			$courseModel = LearningCourse::model()->findByPk(intval($_POST['courseId']));

			if($courseModel) {
				$courseType = $courseModel->course_type;
				switch ($courseType) {
					case LearningCourse::TYPE_ELEARNING:
						break;
					case LearningCourse::TYPE_WEBINAR:
						$sessionModel = WebinarSession::model()->findByPk(intval($_POST['sessionId']));
						if(!$sessionModel) $errors['subject'] = Yii::t('standard', 'Invalid session ID');
						$shortcodes += array(
								'webinar_tool' => '[webinar_tool]',
								'session_datetimes' => '[session_datetimes]',
								'session_name' => '[session_name]',
								'session_description' => '[session_description]',
								'session_link' => '[session_link]',
						);
						break;
					case LearningCourse::TYPE_CLASSROOM:
						$sessionModel = LtCourseSession::model()->findByPk(intval($_POST['sessionId']));
						if(!$sessionModel) $errors['subject'] = Yii::t('standard', 'Invalid session ID');
						$shortcodes += array(
								'session_location' => '[session_location]',
								'session_datetimes' => '[session_datetimes]',
								'session_name' => '[session_name]',
								'session_description' => '[session_description]',
								'session_link' => '[session_link]',
						);
						break;
				}

				$content = $this->renderPartial('sendEmails', array(
						'ids' => $ids,
						'emails' => $emails,
						'errors' => $errors,
						'shortcodes' => $shortcodes,
						'courseId' => $courseId,
						'sessionId' => $sessionId,
				), true);
			}else{
				$errors['subject'] = Yii::t('standard', '_INVALID_COURSE');
			}


			if(!empty($errors))
				$status = 'error';

			$this->sendJSON(array(
				'status' => $status, // success|error
				'html' => $content, // html
			));
		}

		protected function isSalesforceUser($idUser, $type=false) {

			if ($type === false)
				$findArray = array('id_user_lms' => $idUser);
			else
				$findArray = array('id_user_lms' => $idUser, 'sf_type' => $type);

			$sfUser = SalesforceUser::model()->findByAttributes($findArray);

			if (!$sfUser) return false;

			return $sfUser;
		}

		private function hasSalesforcePluginActivated()
		{
			$corePlugin =
				CorePlugin::model()->find(
					"plugin_name = 'SalesforceApp' and is_active = 1"
				);
			if ($corePlugin)
				return true;
			return false;
		}

		/**
		 * Converting the passed timestamp to target user's timezone and format, taking into account the
		 * time difference between the source and target users' timezones
		 *
		 * example:
		 * 	timestamp : 2017-01-12 12:00:00
		 * 	source user timezone offset : -2 hours
		 * 	target user timezone offset : +2 hours
		 *  time difference calculated  : 4 hours
		 * 	source user format : d/m/Y H:i:s
		 *  return value: 1/12/2017 16:00:00
		 *
		 * @param string $timestamp - timestamp in the source user's default or short format, requires a valid timestamp to be passed
		 * @param integer $sourceUserId - the primary key of the source user
		 * @param integer $targetUserId - the primary key of the target user
		 * @return string
		 */
		private function convertTimestampBetweenUsers($timestamp, $sourceUserId, $targetUserId)
		{
			// 1. Converting the user's formatted timestamp to UTC formatted
			// Trying user's default format:
			$format = Yii::app()->localtime->getDateFormatFromSettings($sourceUserId);
			$dateBridge = DateTime::createFromFormat($format, $timestamp);
			if ($dateBridge === false) {
				// Trying date only format if it was unsuccessful
				$format = Yii::app()->localtime->getPHPLocalDateFormat('short');
				$dateBridge = DateTime::createFromFormat($format, $timestamp);
			}
			$timestamp = $dateBridge->format('Y-m-d');

			// 2. Converting the passed date to source user's timezone
			$sourceTimezone = Yii::app()->localtime->getTimezoneFromSettings($sourceUserId);
			$sourceTimezone = new \DateTimeZone($sourceTimezone);

			$date = new \DateTime($timestamp, $sourceTimezone);

			// 3. Converting the passed date to target in user's timezone
			$currentUserTimeZone = Yii::app()->localtime->getTimezoneFromSettings($targetUserId);
			$targetTimeZone = new \DateTimeZone($currentUserTimeZone);
			$date->setTimezone($targetTimeZone);

			// ! NB ! Because of a known issue with converting formats throughout the project the timestamp
			// is set in format 'short', even though the format for date_complete in the behavior is 'medium'!
			$relativeTimestamp = $date->format(Yii::app()->localtime->getPHPLocalDateTimeFormat());

			return $relativeTimestamp;
		}

		public function actionEditEnrollment($courseId, $userId) {

			if (Yii::app()->request->isAjaxRequest) {
				$model = LearningCourseuser::model()->findByAttributes(array(
					'idUser' => $userId,
					'idCourse' => $courseId,
				)); /* @var $model LearningCourseuser */
				if ($model === null) {
					$this->sendJSON(array());
				}
				
				// Array to hold the allowed course user types
				$restrictLevels = array();
				$removeLevels = array();
				
				// Restrict the user subscription levels depending on the course type
				if ($model->course->course_type != LearningCourse::TYPE_ELEARNING) {
					$removeLevels = array(
							LearningCourseuser::$USER_SUBSCR_LEVEL_COACH,
					);
				}
				$course = LearningCourse::model()->findByPk($courseId);
				$nowTimestamp = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d'));
				$dateEndTimestamp = ($course->date_end && $course->date_end != '0000-00-00') ? strtotime(Yii::app()->localtime->fromLocalDate($course->date_end)) : null;
				$showDeadlineSelector = !$dateEndTimestamp || ($nowTimestamp <= $dateEndTimestamp);
				$enrollment_fields = LearningEnrollmentField::getFieldsByCourse($courseId);
				
				if (isset($_POST['LearningCourseuser'])) {
					$model->attributes = $_POST['LearningCourseuser'];
					if(isset($model->status) && ($model->status == LearningCourseuser::$COURSE_USER_WAITING_LIST || $model->status == LearningCourseuser::$COURSE_USER_CONFIRMED)){
						$model->waiting = 1;
					}

					$localFormat = Yii::app()->localtime->YiitoPHPDateFormat(Yii::app()->localtime->getLocalDateTimeFormat('short', 'medium'));
					$dateBeginValidity = $_POST['LearningCourseuser']['date_begin_validity'];
					if(isset($dateBeginValidity) && !empty($dateBeginValidity)) {
						$dateBeginValidityTimestamp = strtotime(Yii::app()->localtime->fromLocalDate($dateBeginValidity) . " 00:00:00");
						$model->date_begin_validity = date($localFormat, $dateBeginValidityTimestamp);
					}

					$dateExpireValidity = $_POST['LearningCourseuser']['date_expire_validity'];
					if(isset($dateExpireValidity) && !empty($dateExpireValidity)) {
						$dateExpireValidityTimestamp = strtotime(Yii::app()->localtime->fromLocalDate($dateExpireValidity) . " 23:59:59");
						$model->date_expire_validity = date($localFormat, $dateExpireValidityTimestamp);
					}

					// Enumerate all possible fields and see if we got something for each
					$enrollmentFieldsValues = array();
					foreach($enrollment_fields as $field){
						if(!empty($_POST['field-'.$field['id']])){
						    // IFRAME fields bring JSON, lets decode them, before continue,
						    // because, the whole fields array is encoded later.
						    // This way we keep IFRAME fields natively JSON in the saved data, being a JSON object themselves.
						    if ($field['type'] == LearningEnrollmentField::TYPE_FIELD_IFRAME) {
						        $tmpValue = json_decode($_POST['field-'.$field['id']]);
						    }
						    else {
						        $tmpValue = $_POST['field-'.$field['id']];
						    }
						    // Collect field values in the array (JSON-encoded later!)
							$enrollmentFieldsValues[$field['id']] = $tmpValue;
						}
					}
					
					$validFields = LearningEnrollmentField::validateEnrollmentArray($enrollmentFieldsValues, $courseId);
					if(!$validFields['success']){
						$model->addError('enrollment_fields', Yii::t('coaching', "Missing required fields or invalid fields data provided."));

						$this->sendJSON(array(
								'status' => 'error',
								'html' => $this->renderPartial(
										'_editEnrollment',
										array(
												'model' => $model,
												'course' => $course,
												'userLevels' => $this->_getLevelsArray($restrictLevels, $removeLevels),
												'showDeadlineSelector' => $showDeadlineSelector,
												'enrollment_fields'=>$enrollment_fields
										),
										true
										),
						));
					
						// $arrayResponse = array(
						// 		'success' => false,
						// 		'errorText' => Yii::t('coaching', "Missing required fields or invalid fields data provided.")
						// 	);
						Yii::app()->end();
					}
					
					if(!empty($enrollmentFieldsValues)){
						$model->enrollment_fields = json_encode($enrollmentFieldsValues);
					}

					// Setting values for forced completion details
					if ($model->status == LearningCourseuser::$COURSE_USER_END) {
						// Using passed 'forced' completion details - score and date of completion
						$forcedCompletionDetails = Yii::app()->request->getParam('forced_completion_details', []);
						$forcedDateComplete = null;
						$forcedScoreValue = null;
						$isScoreForced = null;

						// Loading sanitized forced score flag
						$isScoreForced = isset($forcedCompletionDetails['forced_score_given']);

						// Loading sanitized forced score value
						if ($isScoreForced === true) {
							$forcedScoreValue = static::sanitizeNumber($forcedCompletionDetails['score_given']);

							if ($forcedScoreValue === null) {
								$isScoreForced = false;
							}
						}

						// Loading sanitized and properly formatted date of completion value
						if (empty($forcedCompletionDetails['date_complete']) === false) {
							$mysqlDatetimeFormat = 'Y-m-d';
//							$forcedDateComplete = Yii::app()->localtime->convertLocalDateToFormat($forcedCompletionDetails['date_complete'], $mysqlDatetimeFormat) . ' 00:00:00';
							$forcedDateComplete = $forcedCompletionDetails['date_complete'];
							$forcedDateComplete = $this->convertTimestampBetweenUsers($forcedDateComplete, $model->idUser, Yii::app()->user->id);
						}

						// Setting the flag for forced score and score's value
						$model->forced_score_given = $isScoreForced;
						if ($isScoreForced === true) {
							$model->score_given = $forcedScoreValue;
						}

						// Update `date_complete` by passed value
						if ($forcedDateComplete !== null) {
							$model->date_complete = $forcedDateComplete;
						}
					} else {
						$model->forced_score_given = false;
					}

					if ($model->save()) {

						// update enrollment on SalesForce
						if ($this->hasSalesforcePluginActivated()) {
							$sfUser = $this->isSalesforceUser($userId);
							$isLearningPlan = false;
							if (empty($sfUser)) {
							} else {
								$itemType = SalesforceApiClient::ITEM_TYPE_COURSE;
								$enrollment = LearningCourseuser::model()->findByAttributes(array(
									'idCourse' => $courseId,
									'idUser' => $userId,
								));
								if ($enrollment != null) {
								} else {
									$itemType = SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN;
									$enrollment = LearningCoursepathUser::model()->findByAttributes(array(
										'id_path' => $courseId,
										'idUser' => $userId,
									));
									$isLearningPlan = true;
								}

								if ($enrollment != null) {
									$sfLmsAccount = SalesforceLmsAccount::model()->findByPk($sfUser->sf_lms_id);
									$api = new SalesforceApiClient($sfLmsAccount);

									$sfTaskId = '';
									if ($itemType = SalesforceApiClient::ITEM_TYPE_COURSE) {
										$sfTaskId =
											Yii::app()->db->createCommand(
												'SELECT salesforce_task_id FROM salesforce_enrollment_sf_task WHERE course_id = :course_id and user_id = :user_id')
												->queryScalar(
													array(
														":course_id" => $courseId,
														":user_id" => $userId
													)
												);
									} else {
										$sfTaskId =
											Yii::app()->db->createCommand(
												'SELECT salesforce_task_id FROM salesforce_enrollment_sf_task WHERE path_id = :path_id')
												->queryScalar(
													array(
														":path_id" => $courseId
													)
												);
									}

									$sfResponce = $api->deleteTask($sfTaskId);
									$sfResponce = 0;
									if ($isLearningPlan) {
										$newSfTaskId = $api->createTaskFromLearningPlan($enrollment, $sfUser);
									} else {
										$newSfTaskId = $api->createTaskFromCourse($enrollment, $sfUser);
									}

									Yii::app()->db->createCommand(
										'update salesforce_enrollment_sf_task set salesforce_task_id = :new_task_id where salesforce_task_id = :old_task_id')
										->query(
											array(
												":new_task_id" => $newSfTaskId,
												":old_task_id" => $sfTaskId
											)
										);

								}
							}
						}

                        if ($model->level == LearningCourseuser::$USER_SUBSCR_LEVEL_COACH) {
                            //If set as coach unassign user from all coaching session
                            $transaction = Yii::app()->db->beginTransaction();
                            try {
                                $coachingSessions = LearningCourse::getAllCoachingSessions($courseId);
                                foreach ($coachingSessions as $coachingSessionId) {
                                    $coachingSession = LearningCourseCoachingSession::model()->findByPk($coachingSessionId);
                                    if ($userId != $coachingSession->idCoach) {
                                        $webinarSessions = $coachingSession->getFutureAndPresentWebinarSessions($userId);
                                        foreach ($webinarSessions as $webinarSession) {
                                            $webinarSession->unassignUsers($userId);
                                        }
                                        $coachingSession->deassignUsers($userId);
                                    }
                                }
                                $transaction->commit();
                            }
                            catch(Exceprion $e){
                                $transaction->rollback();

                            }
                        }
						$this->sendJSON(array('status' => 'saved'));
					}
				}

				// Array to hold the allowed course user types
				$restrictLevels = array();
				$removeLevels = array();

				// Restrict the user subscription levels depending on the course type
				if ($model->course->course_type != LearningCourse::TYPE_ELEARNING) {
					$removeLevels = array(
						LearningCourseuser::$USER_SUBSCR_LEVEL_COACH,
					);
				}
                $course = LearningCourse::model()->findByPk($courseId);
				$nowTimestamp = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d'));
				$dateEndTimestamp = ($course->date_end && $course->date_end != '0000-00-00') ? strtotime(Yii::app()->localtime->fromLocalDate($course->date_end)) : null;
				$showDeadlineSelector = !$dateEndTimestamp || ($nowTimestamp <= $dateEndTimestamp);

				if (empty($model->date_complete) === false) {
					$model->date_complete = $forcedDateComplete = $this->convertTimestampBetweenUsers($model->date_complete, Yii::app()->user->id, $model->idUser);
				}

				$html = $this->renderPartial('_editEnrollment', array(
                    'model' => $model,
                    'course' => $course,
                    'userLevels' => $this->_getLevelsArray($restrictLevels, $removeLevels),
					'showDeadlineSelector' => $showDeadlineSelector,
					'enrollment_fields'=>$enrollment_fields,
                ), true);
				$this->sendJSON(array('html' => $html));
			}
		}

		public function actionConfirmEnrollment($id = null) {
			$currentCourseId = Yii::app()->session['currentCourseId'];

			if (!empty($_POST['ids'])) {
				$id = array();
				foreach ($_POST['ids'] as $value) {
					$params = explode(',', $value);
					$id[] = $params[0];
				}
			}
			$learningCourseUsers = LearningCourseuser::model()->findAllByAttributes(array('idCourse' => $currentCourseId, 'idUser' => $id));
			if (empty($learningCourseUsers)) {
				$this->sendJSON(array());
			}
			if (!empty($_POST['LearningCourseuser'])) {
				if ($_POST['LearningCourseuser']['confirm']) {
					foreach ($learningCourseUsers as $courseUser) {

						if($courseUser->user->valid <= 0){
							$this->managementError();
						}

						$courseUser->waiting = 0;
						$courseUser->status = 0;
						$courseUser->save();

						// Raised when the user's subscription for a moderated course is approved by admin
						Yii::app()->event->raise('UserApproval', new DEvent($this, array(
							'course' => $courseUser->course, // AR relation
							'user' => $courseUser->user, // AR relation
						)));

					}
					$this->sendJSON(array());
				}
			}
			if (Yii::app()->request->isAjaxRequest) {
				$content = $this->renderPartial('confirmEnrollment', array(
					'model' => new LearningCourseuser(),
					'courseUser' => $learningCourseUsers,
				), true);
				$this->sendJSON(array(
					'html' => $content,
				));
			}
			Yii::app()->end();
		}

		public function actionCancelEnrollment($id = null) {
			$currentCourseId = Yii::app()->session['currentCourseId'];
			$learnersInLP = 0;

			if (!empty($_POST['ids'])) {
				$id = array();
				foreach ($_POST['ids'] as $value) {
					$params = explode(',', $value);
					$id[] = $params[0];
				}
			}
			$learningCourseUsers = LearningCourseuser::model()->findAllByAttributes(array('idCourse' => $currentCourseId, 'idUser' => $id));
			if (empty($learningCourseUsers)) {
				$this->sendJSON(array());
			}

			//Counting the users that are subscribed in Learning Plan
			foreach ($learningCourseUsers as $courseUser) {
				if($courseUser->isSubscribedtoLearningPlan() === true) {
					$learnersInLP++;
				}
			}

			if (!empty($_POST['LearningCourseuser'])) {
				if ($_POST['LearningCourseuser']['confirm']) {
					foreach ($learningCourseUsers as $courseUser) {
						if($courseUser->isSubscribedtoLearningPlan() === false) {
						$courseUser->unsubscribeUser($courseUser->idUser, $currentCourseId);
					}
					}
					$this->sendJSON(array());
				}
			}
			if (Yii::app()->request->isAjaxRequest) {
				$content = $this->renderPartial('cancelEnrollment', array(
					'model' => new LearningCourseuser(),
					'courseUser' => $learningCourseUsers,
					'usersInLearningPlan' => $learnersInLP
				), true);
				$this->sendJSON(array(
					'html' => $content,
				));
			}
			Yii::app()->end();
		}

		public function actionUsersAutocomplete() {
			$model = CoreUser::model();

			if (!empty($_REQUEST['data']['courseid'])) {
				$courseId = $_REQUEST['data']['courseid'];
			} else {
				$courseId = null;
			}

			$users = $model->findAll($model->dataProviderEnroll($courseId)->criteria);
			$result = array(
				'options' => array(),
			);

			if (!empty($users)) {
				foreach ($users as $user) {
					$username = Yii::app()->user->getRelativeUsername($user->userid);
					$text = "{$username}";

					$names = array();
					if (!empty($user->firstname))
						$names[] = $user->firstname;
					if (!empty($user->lastname))
						$names[] = $user->lastname;
					$names = join(' ', $names);
					if ($names)
						$text .= " - $names";

					$result['options'][$username] = $text;
				}
			}

			$this->sendJSON($result);
		}

		public function actionCopyEnrollments() {
            $allowOnlyStudentEnrollments = $this->_allowOnlyStudentEnrollments();

			$currentCourseId = Yii::app()->session['currentCourseId'] = isset($_REQUEST['courseId']) ? $_REQUEST['courseId'] : null;

			$currentCourse = LearningCourse::model()->findByPk($currentCourseId);
			$learningCourse = $currentCourse;
			if (empty($learningCourse)) {
				$this->sendJSON(array());
			}

			$learningCourse = new LearningCourse();
			$courseUser = new LearningCourseuser();

			if (!empty($_POST['LearningCourse'])) {
				$learningCourse->name = $_POST['LearningCourse']['name'];
				$courseUser->level    = $_POST['LearningCourseuser']['level'];
				$courseUser->status   = $_POST['LearningCourseuser']['status'];
				$sourceCourse         = LearningCourse::model()->findByAttributes(array('name' => $_POST['LearningCourse']['name']));
				if (!empty($sourceCourse) && $sourceCourse->idCourse != $currentCourseId) {
					$attributes = array(
						'idCourse' => $sourceCourse->idCourse,
					);
					if (!empty($courseUser->level)) {
						$attributes['level'] = $courseUser->level;
					}
					if ($courseUser->status != '') {
						$attributes['status'] = $courseUser->status;
					}
					$sourceCourseUsers = LearningCourseuser::model()->findAllByAttributes($attributes);
					$countEnrolled     = array();
					if (!empty($sourceCourseUsers)) {

						if($currentCourse->selling && CoreUserPU::isPUAndSeatManager()){
							// The current user is a Power user and he is
							// allowed to enroll via "seats" only.
							// Check if he has enough available for this course
							if(!CoreUserPuCourse::checkIfPuHasEnoughSeatsInCourse(count($sourceCourseUsers), $currentCourseId)){
								$learningCourse->addError('name', Yii::t('standard', "You don't have enough seats for this course"));

								$currentLearningCourse = LearningCourse::model()->findByPk($currentCourseId);

								// Restrict the user subscription levels depending on the course type
								$restrictLevels = array();
								$removeLevels = array();
								if ($currentLearningCourse->course_type != LearningCourse::TYPE_ELEARNING) {
									$removeLevels = array(
										LearningCourseuser::$USER_SUBSCR_LEVEL_COACH
									);
								}

								$content = $this->renderPartial('copyEnrollments', array(
									'model'             => $courseUser,
									'learningCourse'    => $learningCourse,
									'courseUserlevels'  => LearningCourseuser::getLevelsArray($restrictLevels, $removeLevels),
									'course'            => $currentCourse
								), true);
								$this->sendJSON(array(
									'html'              => $content,
								));
							}
						}

						$localFormat = Yii::app()->localtime->getPHPLocalDateTimeFormat('short', 'medium');
						$settingEnrollmentDeadlines = !empty($_REQUEST['set_enrollment_deadlines']) ?  $_REQUEST['set_enrollment_deadlines'] : false;

						// Should we set soft-deadlines
						if($settingEnrollmentDeadlines) {
							$date_begin_validity = false;
							if ($_REQUEST['LearningCourseuser']['date_begin_validity']) {
								$date_begin_validity_input = $_REQUEST['LearningCourseuser']['date_begin_validity'];
								$date_begin_validity = date($localFormat, strtotime(Yii::app()->localtime->fromLocalDate($date_begin_validity_input) . ' 00:00:00'));
							}

							$date_expire_validity = false;
							if ($_REQUEST['LearningCourseuser']['date_expire_validity']) {
								$date_expire_validity_input = $_REQUEST['LearningCourseuser']['date_expire_validity'];
								$date_expire_validity = date($localFormat, strtotime(Yii::app()->localtime->fromLocalDate($date_expire_validity_input) . ' 23:59:59'));
							}

							if ($date_begin_validity && $date_expire_validity && Yii::app()->localtime->fromLocalDateTime($date_begin_validity) > Yii::app()->localtime->fromLocalDateTime($date_expire_validity)) {
								$learningCourse->addError('status', Yii::t('coaching', "Ending date cannot be earlier than the starting date"));
								$status = 'error';

								// Skip showing soft-deadlines
								$skipSoftDeadline = false;

								// If there is at least one not eLearning course skip soft deadlines
								if ($learningCourse->course_type != LearningCourse::TYPE_ELEARNING) {
									$skipSoftDeadline = true;
								}

								$this->sendJSON(array(
									'status' => $status,
									'html' => $this->renderPartial(
										'copyEnrollments',
										array(
											'model'                 => $courseUser,
											'learningCourse'        => $learningCourse,
											'courseUserlevels'      => LearningCourseuser::getLevelsArray($restrictLevels, $removeLevels),
											'skipSoftDeadline'      => $skipSoftDeadline,
											'date_begin_validity'   => $date_begin_validity_input,
											'date_expire_validity'  => $date_expire_validity_input,
											'course'                => $currentCourse
										),
										true
									),
								));

								Yii::app()->end();
							}
						}

						foreach ($sourceCourseUsers as $courseUser) {
							if(Yii::app()->user->getIsPu()  &&  !(CoreUserPU::isAssignedToPU($courseUser->idUser, 'user'))){
								continue;
							}

							$learningCourseUser = new LearningCourseuser();
                            // force the enrolled user level to 'student' if this option is set
                            $enrollLevel = $allowOnlyStudentEnrollments ? LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT : $courseUser->level;
							$enrollResultInteger = $learningCourseUser->subscribeUser($courseUser->idUser, Yii::app()->user->id, $currentCourseId, LearningCourseuser::$COURSE_USER_SUBSCRIBED, $enrollLevel, false, $date_begin_validity, $date_expire_validity);

							$countEnrolled[$enrollResultInteger]++;

							if($enrollResultInteger){
								// Check first is the user PU and seat manager, and also is the course selling
								if(LearningCourse::isCourseSelling($currentCourseId) && CoreUserPU::isPUAndSeatManager()) {
									CoreUserPU::modifyAvailableSeats($currentCourseId, -1);
									Yii::app()->event->raise('onPuSeatConsume', new DEvent($this, array(
										'idUser' => $courseUser->idUser,
										'idCourse' => $currentCourseId,
										'idPowerUser' => Yii::app()->user->id,
									)));
								}

							}

						}
					}
					$content = $this->renderPartial('copyEnrollmentsSuccess', array('countEnrolled' => $countEnrolled), true);
					$this->sendJSON(array(
						'html' => $content,
						'status' => 'success'
					));
				} else {
					$learningCourse->addError('name', Yii::t('standard', '_OPERATION_FAILURE'));
				}
			}
			if (Yii::app()->request->isAjaxRequest) {


				$currentLearningCourse = LearningCourse::model()->findByPk($currentCourseId);

				// Restrict the user subscription levels depending on the course type
				$restrictLevels = array();
				$removeLevels = array();
				if ($currentLearningCourse->course_type != LearningCourse::TYPE_ELEARNING) {
					$removeLevels = array(
						LearningCourseuser::$USER_SUBSCR_LEVEL_COACH
					);
				}

				// Skip showing soft-deadlines
				$skipSoftDeadline = false;

				// If there is at least one not eLearning course skip soft deadlines
				if ($learningCourse->course_type != LearningCourse::TYPE_ELEARNING) {
					$skipSoftDeadline = true;
				}

				$date_begin_validity    = Yii::app()->localtime->toLocalDate(Yii::app()->localtime->getUTCNow());
				$date_expire_validity   = '';

				$content = $this->renderPartial('copyEnrollments', array(
					'model'                 => $courseUser,
					'learningCourse'        => $learningCourse,
					'courseUserlevels'      => LearningCourseuser::getLevelsArray($restrictLevels, $removeLevels),
					'date_begin_validity'   => $date_begin_validity,
					'date_expire_validity'  => $date_expire_validity,
					'skipSoftDeadline'      => $skipSoftDeadline,
					'course'                => $currentCourse
				), true);
				$this->sendJSON(array(
					'html' => $content,
				));
			}
			Yii::app()->end();
		}

		public function actionEnrollFromCsv($courseId) {
			$currentCourseId = Yii::app()->session['currentCourseId'] = isset($courseId) ? $courseId : null;
			
			/* @var $enrollmentFields EnrollmentFields */
			$enrollmentFields = LearningEnrollmentField::getFieldsByCourse($currentCourseId);

            /* @var $learningCourse LearningCourse */
			$learningCourse = LearningCourse::model()->findByPk($currentCourseId);

			// Skip showing soft-deadlines
			$skipSoftDeadline = false;

			// If there is at least one not eLearning course skip soft deadlines
			// of it is a learning course, but it's not set using of soft deadline from course settings
			//if ($learningCourse->course_type != LearningCourse::TYPE_ELEARNING || ($learningCourse->course_type == LearningCourse::TYPE_ELEARNING && $learningCourse->soft_deadline != 1)) {
			if ($learningCourse->course_type != LearningCourse::TYPE_ELEARNING) {
				$skipSoftDeadline = true;
			}

			if (empty($learningCourse)) {
				$this->sendJSON(array());
			}

			if (!empty($_FILES['EnrollCsv'])) {
				$file = CUploadedFile::getInstanceByName('EnrollCsv');
				$separator = Yii::app()->request->getParam('separator', ',');
				$enrollHeader = Yii::app()->request->getParam('enroll_header', false);
				$manualSeparator =  Yii::app()->request->getParam('manual_separator', ',');

				switch ($separator) {
					case 1:
						$sepSymbol = ',';
						break;
					case 2:
						$sepSymbol = ';';
						break;
					case 3:
						$sepSymbol = $manualSeparator;
						break;
				}


				$users = Yii::app()->csvParser->parse($file, array(
					'hasHeader' => $enrollHeader,
					'carriageParse' => true,
					'delimiter' => $sepSymbol
				));

				$localFormat = Yii::app()->localtime->getPHPLocalDateTimeFormat('short', 'medium');
				$settingEnrollmentDeadlines = !empty($_REQUEST['set_enrollment_deadlines']) ?  $_REQUEST['set_enrollment_deadlines'] : false;
				$settingEnrollmentFields = !empty($_REQUEST['set_enrollment_fields']) ? $_REQUEST['set_enrollment_fields'] : false;

				// Should we set soft-deadlines
				if($settingEnrollmentDeadlines) {
					$date_begin_validity = false;
					if ($_REQUEST['LearningCourseuser']['date_begin_validity']) {
						$date_begin_validity = $_REQUEST['LearningCourseuser']['date_begin_validity'];
						$date_begin_validity = date($localFormat, strtotime(Yii::app()->localtime->fromLocalDate($date_begin_validity) . ' 00:00:00'));
					}

					$date_expire_validity = false;
					if ($_REQUEST['LearningCourseuser']['date_expire_validity']) {
						$date_expire_validity = $_REQUEST['LearningCourseuser']['date_expire_validity'];
						$date_expire_validity = date($localFormat, strtotime(Yii::app()->localtime->fromLocalDate($date_expire_validity) . ' 23:59:59'));
					}

					if ($date_begin_validity && $date_expire_validity && Yii::app()->localtime->fromLocalDateTime($date_begin_validity) > Yii::app()->localtime->fromLocalDateTime($date_expire_validity)) {
						$learningCourse->addError('status', Yii::t('coaching', "Ending date cannot be earlier than the starting date"));
						$status = 'error';

						$this->sendJSON(array(
							'status' => $status,
							'html' => $this->renderPartial(
								'enrollFromCsv',
								array(
									'learningCourse'        => $learningCourse,
									'skipSoftDeadline'      => $skipSoftDeadline,
									'date_begin_validity'   => $date_begin_validity,
									'date_expire_validity'  => $date_expire_validity,
									'enrollmentFields' => $enrollmentFields
								),
								true),
						));

						Yii::app()->end();
					}
				}

				// Should we set enrollment fields
				$enrollmentFieldsValues = array();
				if ($settingEnrollmentFields) {
					foreach ($enrollmentFields as $field) {
						if (!empty($_POST['field-' . $field['id']])) {
							$enrollmentFieldsValues[$field['id']] = $_POST['field-' . $field['id']];
						}
					}
				
					$validFields = LearningEnrollmentField::validateEnrollmentArray($enrollmentFieldsValues, $currentCourseId);
					if (!$validFields['success']) {
						$date_begin_validity = !empty($date_begin_validity) ? $date_begin_validity : Yii::app()->localtime->toLocalDate(Yii::app()->localtime->getUTCNow());
						$date_expire_validity = !empty($date_expire_validity) ? $date_expire_validity : '';
				
						$learningCourse->addError('status', Yii::t('coaching', "Missing required fields or invalid fields data provided."));
						$this->sendJSON(array(
								'status' => 'error',
								'html' => $this->renderPartial(
										'enrollFromCsv', array(
												'learningCourse' => $learningCourse,
												'skipSoftDeadline' => $skipSoftDeadline,
												'date_begin_validity' => $date_begin_validity,
												'date_expire_validity' => $date_expire_validity,
												'enrollmentFields' => $enrollmentFields
										), true),
						));
				
						Yii::app()->end();
					}
				}
				
                $enrollParams = array();
                $allowOnlyStudentEnrollments = $this->_allowOnlyStudentEnrollments();
                if ($allowOnlyStudentEnrollments) {
                    $enrollParams['force_level'] = LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT;
                }

				$date_begin_validity    = !empty($date_begin_validity) ? $date_begin_validity : '';
				$date_expire_validity   = !empty($date_expire_validity) ? $date_expire_validity : '';

				$enrollParams['date_begin_validity']    = $date_begin_validity;
				$enrollParams['date_expire_validity']   = $date_expire_validity;
				$enrollParams['enrollment_fields_values'] = json_encode($enrollmentFieldsValues);

				if($learningCourse->selling && CoreUserPU::isPUAndSeatManager()){
					// The current user is a Power User and he is only
					// allowed to enroll users to courses through "seats"
					// Do a check if the PU has enough seats to distribute in
					// the course (enough the cover the selected users)
					$usersToEnrollCount = 0;
					foreach($users as $user){
						if(!isset($user[0])) continue; // skip empty lines
						$usersToEnrollCount++;
					}

					if(!CoreUserPuCourse::checkIfPuHasEnoughSeatsInCourse($usersToEnrollCount, $currentCourseId)){
						$learningCourse->addError('status', Yii::t('standard', "You don't have enough seats for this course"));
						$status = 'error';

						$this->sendJSON(array(
							'status' => $status,
							'html' => $this->renderPartial(
								'enrollFromCsv',
								array(
									'learningCourse'        => $learningCourse,
									'skipSoftDeadline'      => $skipSoftDeadline,
									'date_begin_validity'   => $date_begin_validity,
									'date_expire_validity'  => $date_expire_validity,
									'enrollmentFields' => $enrollmentFields
								),
								true),
						));
						Yii::app()->end();
					}
				}

				$result = $learningCourse->processEnrollmentCSVUpload($users, $enrollParams);

				if($learningCourse->selling && CoreUserPU::isPUAndSeatManager()){
					// PU has enrolled some users in a course, reduce his available
					// seats by how much he enrolled
					$countEnrolledSuccessfullyFromCSV = intval($result['enrolled']);
					CoreUserPU::modifyAvailableSeats($currentCourseId, -$countEnrolledSuccessfullyFromCSV);
				}
				$content = $this->renderPartial('enrollFromCsvSuccess', array('result' => $result, 'course' => $learningCourse), true);
				$status = 'success';
			} else {
				if (!empty($_POST['LearningCourse']['status'])) {
					$learningCourse->addError('status', Yii::t('standard', '_OPERATION_FAILURE'));
				}

				$date_begin_validity    = !empty($date_begin_validity) ? $date_begin_validity : Yii::app()->localtime->toLocalDate(Yii::app()->localtime->getUTCNow());
				$date_expire_validity   = !empty($date_expire_validity) ? $date_expire_validity : '';

				$content = $this->renderPartial(
					'enrollFromCsv',
					array(
						'learningCourse'        => $learningCourse,
						'skipSoftDeadline'      => $skipSoftDeadline,
						'date_begin_validity'   => $date_begin_validity,
						'date_expire_validity'  => $date_expire_validity,
						'enrollmentFields' => $enrollmentFields

					),
					true
				);
				$status = 'error';
			}

			$this->sendJSON(array(
				'status' => $status,
				'html' => $content,
			));
		}

		public function actionDeleteEnrollment() {
			$currentCourseId = Yii::app()->session['currentCourseId'];
			$rq = Yii::app()->request;
			/*
			 * Analyze input and detect single/massive case
			 * always keep ids in string, no array
			 */
			$singleId = $rq->getParam('id', false);
			if ($singleId !== false) {
				$id = (string) $singleId;
			} else {
				if (!empty($_POST['ids'])) {
					$id = array();
					if (is_array($_POST['ids'])) {
						$id = implode(",", $_POST['ids']);
					} elseif (is_string($_POST['ids'])) {
						$id = $_POST['ids'];
					} elseif (is_numeric($_POST['ids'])) {
						$id = (string) $_POST['ids'];
					}
				}
			}

			$idsList = explode(',', $id); //to perform certain operations, we need the array form

			//make sure that the passed IDs are effectively part of course enrollments
			$learningCourseUsers = Yii::app()->db->createCommand()
				->select('COUNT(idUser)')
				->from('learning_courseuser')
				->where('idCourse = :idCourse', array(':idCourse'=>$currentCourseId))
				->andWhere( array("IN", 'idUser', $idsList) )
				->queryScalar();

            //Check if there are coaches with sessions in the selection
            $filter = new stdClass();
            $filter->idCoach = explode(',', $id);
            $sessionsForCoachesDP = LearningCourse::coachingSessionsSqlDataProvider($currentCourseId, $filter);
            $sessionsForCoaches = Yii::app()->db->createCommand();
            $sessionsForCoaches->text = $sessionsForCoachesDP->sql;
            $sessionsForCoaches = $sessionsForCoaches->query($sessionsForCoachesDP->params);
            //If there are coaches with sessions in the selection
            $coaches = array();
            $resultError = Yii::t('coaching','Please un-assign the following coaches from their respective sessions:');
            if ($sessionsForCoaches->getRowCount() > 0){
                while ($record = $sessionsForCoaches->read()) {
                    $coaches[str_replace("/", "", $record["userid"])][$record['idSession']] = Yii::t("standard", "_FROM") . " " . Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($record["datetime_start"])) . " " . Yii::t("standard", "_TO") . "  " . Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($record["datetime_end"]));
                }
                foreach($coaches as $coachId => $coachSessions){
                    $resultError .=  '<br /><br />'.Yii::t('coaching','<strong>{coachId}</strong>\'s sessions', array('{coachId}'=>$coachId)).' : <ul style="margin-left:50px;list-style-type: disc;">';
                    foreach($coachSessions as $sessionId => $sessionDates){
                        $resultError .= '<li>'.$sessionDates.'</li>';
                    }
                    $resultError .= '</ul>';
                }
                $this->sendJSON(array(
                    'status' => 'error',
                    'html' => $resultError."<script>$('.btn.btn-submit').remove();</script>",
                ));
                Yii::app()->end();
            }

			// If something is not ok with $learningCourseUsers
			if (!$learningCourseUsers) {
				$this->sendJSON(array(
					'status' => 'error', // success|error
					'html' => Yii::t('standard', '_OPERATION_FAILURE') .
						"<script>$('.modal.btn-submit').remove();</script>",
				));
				Yii::app()->end();
			}

			$excludedUsers = 0;

			if (PluginManager::isPluginActive('CurriculaApp')) {
				//check if and which enrollments can be performed due to LPs constraints
				$lpEnrollments = LearningCoursepath::checkEnrollmentsMultipleUsers($idsList, $currentCourseId);
				if (is_array($lpEnrollments) && !empty($lpEnrollments)) {
					$toBeExcluded = array();
					foreach ($lpEnrollments as $lpIdUser => $lpList) {
						if (!empty($lpList)) {
							$toBeExcluded[] = $lpIdUser;
						}
					}
					//update input list, removing non-unenrollable users
					if (!empty($toBeExcluded)) {
						$idsList = array_diff($idsList, $toBeExcluded);
						$id = implode(',', $idsList);
						$excludedUsers = count($toBeExcluded);
						$learningCourseUsers -= $excludedUsers;
					}
					//check if any user is remaining to be unenrolled, otherwise send a message to the user
					if (empty($idsList)) {
						$this->sendJSON(array(
							'status' => 'error', // success|error
							'html' => Yii::t('curricula', '{n} selected user(s) have been assigned to a learning plan and cannot be unenrolled from course', array('{n}' => '<strong>'.count($toBeExcluded).'</strong>')) .
								"<script>$('.modal.btn-submit').remove();</script>",
						));
						Yii::app()->end();
					}
				}
			}

			if (!empty($_POST['LearningCourseuser'])) {
				if ($_POST['LearningCourseuser']['confirm']) {

					$html = $this->renderPartial('_mass_delete_user_enrollment', array(
						'users'=> $id, // Built string with the all users ids
						'currentCourseId'=>$currentCourseId,
					), true);

					$this->sendJSON(array('preserveSelection' => true, 'html'=>$html.'<a class="auto-close"></a>'));
					Yii::app()->end();
				}
			}

			if (Yii::app()->request->isAjaxRequest) {
				$course = LearningCourse::model()->findByPk($currentCourseId);

				$content = $this->renderPartial('deleteEnrollment', array(
					'model' => new LearningCourseuser(),
					'courseUser' => $learningCourseUsers,
					'excludedUsers' => $excludedUsers,
					'ids'=>$id,
					'showAutoEnrollMessage'	=> ($course->getFreePositions(null, $learningCourseUsers) > 0),
				), true);
				$this->sendJSON(array(
					'html' => $content,
				));
			}
			Yii::app()->end();
		}




		public function actionSelfUnenroll($idCourse)
		{
			$userId = Yii::app()->user->id;

			$learningCourseUser = LearningCourseuser::model()->findByAttributes(array('idCourse' => $idCourse, 'idUser' => $userId));
			if (!isset($learningCourseUser)) {
				$this->renderPartial('lms.protected.views.site._js_redirect', array('url' => Docebo::createLmsUrl("site/index")));
			}

			if (PluginManager::isPluginActive('CurriculaApp')) {
				//are there any learning plans constraints preventing user unenrollments?
				if (LearningCoursepathCourses::isCourseAssignedToLearningPaths($idCourse)) {
					$lpInfo = LearningCoursepath::checkEnrollment($userId, $idCourse);
					if (!empty($lpInfo)) {
						//NOTE: user interface should preventing reaching this point !!
						throw new CException('User is assigned to a learning plan(s) containing this course and cannot be unenrolled');
					}
				}
			}

			if (!empty($_POST['confirm'])) {
				$courseModel = $learningCourseUser->course;

//				$userSessions IS EMPTY IF IS E-LEARNING TYPE
				$userSessions = $courseModel->getUserSessions($userId);

				$courseSession = null;
				if ($courseModel->course_type == LearningCourse::TYPE_CLASSROOM) {
					$courseSession = LtCourseSession::model()->findByPk($userSessions[0]->id_session);
					//unenroll user from all sessions if the course is clasroom course
					$courseModel->removeAllUserSessionsByUserId($userId);
				} elseif ($courseModel->course_type == LearningCourse::TYPE_WEBINAR) {
					$courseSession = WebinarSession::model()->findByPk($userSessions[0]->id_session);
					//unenroll user from all sessions if the course is clasroom course
					$courseModel->removeAllUserSessionsByUserId($userId);
				}



				$learningCourseUser->unsubscribeUser($userId, $idCourse);

				// To reset user's Total Time for this course
				$criteria = new CDbCriteria();
				$criteria->condition = 'idUser=:idUser AND idCourse=:idCourse';
				$criteria->params = array(':idUser'=>$userId, ':idCourse'=>$idCourse);
				$LearningTracksessionModel = LearningTracksession::model()->deleteAll($criteria);

				$courseModel->automaticallyEnroll($courseModel->course_type, $courseSession, 1);

				$this->renderPartial('lms.protected.views.site._js_redirect', array('url' => Docebo::createLmsUrl("site/index")));
			}

			if (Yii::app()->request->isAjaxRequest) {
				echo $this->renderPartial('selfUnenroll', array(
					'model' => new LearningCourseuser(),
				), true, true);
			}
			Yii::app()->end();
		}

		public function actionResetDates($course_id = null, $user_id = null) {
			if (Yii::app()->request->isAjaxRequest) {
				$course = LearningCourse::model()->findByAttributes(array(
					'idCourse' => $course_id
				));

				if (!empty($course)) {
					if (intval($course->valid_time) > 0) {
						$field = ($course->valid_time_type == LearningCourse::VALID_TIME_TYPE_FIRST_COURSE_ACCESS) ? 'date_first_access' : 'date_inscr';
						LearningCourseuser::model()->updateAll(
							array('date_expire_validity' => new CDbExpression('ADDDATE(' . $field . ', ' . intval($course->valid_time) . ')'),
								'date_begin_validity' => new CDbExpression($field)),
							'idCourse = :idCourse and idUser= :idUser',
							array(":idCourse" => $course->idCourse, ':idUser' => $user_id)
						);
					}else {
						LearningCourseuser::model()->updateAll(
							array('date_expire_validity' => null, 'date_begin_validity' => null),
							'idCourse = :idCourse and idUser= :idUser',
							array(":idCourse" => $course->idCourse, ':idUser' => $user_id)
						);
					}

					$this->sendJSON(array('status' => 'success'));
				}
			}
			Yii::app()->end();
		}



		/**
		 * Handle SINGLE COURSE -> MANY users Enrollment (Admin -> Courses -> Course-Enrollment -> Enroll Users) using UsersSelector
		 * Initiate a jWizard.js run:
		 * 		STEP 1: Users Selector
		 * 		STEP 2: Select Role
		 *
		 * Basically, jWizard is based on the rule "A STEP instructs the jWizard about the URL of the NEXT step".
		 *
		 * In this case, BOTH steps make a call to THIS action which decides where to go next, making it a sort of "steps dispatcher".
		 *
		 * Submitted data by all STEPS are saved and restored by jWizard internally and if "finish wizard" is initiated (a click)
		 * a "one big Wizard array" is submited to this action for the real work: subscribing users.
		 *
		 */
		public function actionAxEnrollUsersToSingleCourse() {

			// Passed upon first dialog opening and also by every STEP
			$idCourse = Yii::app()->request->getParam('idCourse', false);

			// Every step CAN send (and in this case IT DOES) its own identifier so THIS action to make decisions
			$fromStep = Yii::app()->request->getParam('from_step', false);

			// FINISH wizard button clicked?
			$finish   = Yii::app()->request->getParam('finish_wizard', false);

			$usersSelectorName = 'users-selector';


			$usersSelectorOpenUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
				'type' 	=> UsersSelector::TYPE_COURSE_ENROLLMENT,   // Step 1
				'name'	=> $usersSelectorName,
				'idCourse' 	=> $idCourse,
			));

			// Check if course exists and show COMMON error dialog if not
			$course = LearningCourse::model()->findByPk($idCourse);
			if (!$course) {
				$this->renderPartial('admin.protected.views.common._dialog2_error', array(
						'type' => 'error',
						'message' => Yii::t('standard','_OPERATION_FAILURE') . ' (invalid course)',
				));
				Yii::app()->end();
			}

			// If we are called "from users selector step", show the "select role step"
			if ($fromStep == $usersSelectorName) {
				// Get FULL list of selected users in UsersSelector
				$usersList = UsersSelector::getUsersList($_REQUEST, false, true);

				if (count($usersList) > 0) {
					// Give the next Step (role selection) the finish URL (this action again)
					$finishUrl = Docebo::createAdminUrl('courseManagement/axEnrollUsersToSingleCourse');

					// Restrict the user subscription levels depending on the course type
					$restrictLevels = array();
					$removeLevels = array();
					if ($course->course_type != LearningCourse::TYPE_ELEARNING) {
						$removeLevels = array(
							LearningCourseuser::$USER_SUBSCR_LEVEL_COACH
						);
					}

					$nowTimestamp = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d'));
					$dateBeginTimestamp = ($course->date_begin && $course->date_begin != '0000-00-00') ? strtotime(Yii::app()->localtime->fromLocalDate($course->date_begin)) : null;
					$dateEndTimestamp = ($course->date_end && $course->date_end != '0000-00-00') ? strtotime(Yii::app()->localtime->fromLocalDate($course->date_end)) : null;
					$showDeadlineSelector = !$dateEndTimestamp || ($nowTimestamp <= $dateEndTimestamp);
					$initialDateBegin = $dateBeginTimestamp ? $course->date_begin : Yii::app()->localtime->toLocalDate(Yii::app()->localtime->getUTCNow());
					$initialDateEnd = $dateEndTimestamp ? $course->date_end : '';
					
					$fields = LearningEnrollmentField::getFieldsByCourse($idCourse);					

					// Render "role selection" STEP2
					$this->renderPartial('enroll_role_step', array(
						'course' => $course,
						'usersCount' => count($usersList),
						'finishUrl' => $finishUrl,
						'userLevels' => $this->_getLevelsArray($restrictLevels, $removeLevels),
						'usersSelectorOpenUrl' => $usersSelectorOpenUrl,
						'showDeadlineSelector' => $showDeadlineSelector,
						'initialDateBegin' => $initialDateBegin,
						'initialDateEnd' => $initialDateEnd,
						'fields' => $fields
					));
					Yii::app()->end();
				}

				Yii::app()->user->setFlash('error', Yii::t('standard', '_SELECT_USERS'));
			}


			// Wizard got "finished" ?  Do some checks/validation and subscribe users to THE course (single course, NO sessions!!!)
			// jWizard makes a union of all form data he ran through, putting them in a "one big" Wizard[] array
			// At this point, we should know all form names, so we extract incoming data
			if ($finish && isset($_REQUEST['Wizard'])) {

				$WizardData = $_REQUEST['Wizard'];

				// Get a combined list of selected users
				$usersList = UsersSelector::getUsersList($WizardData['users-selector-form'], false, true);

				// Get role ID from the last step
				$role = $WizardData['enroll-role-step-form']['enrollment_role'];
				

				$enrollmentFields = LearningEnrollmentField::getFieldsByCourse($idCourse);
				
				$fieldsValues = array();
				
				foreach ($enrollmentFields as $field) {
					if (!empty($WizardData['enroll-role-step-form']['field-' . $field['id']])) {
						$fieldsValues[$field['id']] = $WizardData['enroll-role-step-form']['field-' . $field['id']];
					}
				}
					
				$validFields = LearningEnrollmentField::validateEnrollmentArray($fieldsValues, $idCourse);
				if(!$validFields['success']){
					$errors_enroll = array(
							'required' => Yii::t('standard', 'Missing required fields or invalid fields data provided')
					);
				
					$fields = LearningEnrollmentField::getFieldsByCourse($idCourse);
				
					$this->renderPartial('enroll_role_step', array(
							'course' => $course,
							'usersCount' => count($usersList),
							'finishUrl' => $finishUrl,
							'userLevels' => $this->_getLevelsArray($restrictLevels, $removeLevels),
							'usersSelectorOpenUrl' => $usersSelectorOpenUrl,
							'showDeadlineSelector' => $showDeadlineSelector,
							'initialDateBegin' => $initialDateBegin,
							'initialDateEnd' => $initialDateEnd,
							'fields' => $fields,
							'errors' => $errors_enroll
					));
				
					Yii::app()->end();
				}

                $localFormat = Yii::app()->localtime->getPHPLocalDateTimeFormat('short', 'medium');
                $settingEnrollmentDeadlines = $WizardData['enroll-role-step-form']['set_enrollment_deadlines'];
                if($settingEnrollmentDeadlines) {
                    $date_begin_validity = false;
                    if ($WizardData['enroll-role-step-form']['LearningCourseuser']['date_begin_validity']) {
                        $date_begin_validity = $WizardData['enroll-role-step-form']['LearningCourseuser']['date_begin_validity'];
                        $date_begin_validity = date($localFormat, strtotime(Yii::app()->localtime->fromLocalDate($date_begin_validity) . ' 00:00:00'));
                    }
                    $date_expire_validity = false;
                    if ($WizardData['enroll-role-step-form']['LearningCourseuser']['date_expire_validity']) {
                        $date_expire_validity = $WizardData['enroll-role-step-form']['LearningCourseuser']['date_expire_validity'];
                        $date_expire_validity = date($localFormat, strtotime(Yii::app()->localtime->fromLocalDate($date_expire_validity) . ' 23:59:59'));
                    }
                    if ($date_begin_validity && $date_expire_validity && Yii::app()->localtime->fromLocalDateTime($date_begin_validity) > Yii::app()->localtime->fromLocalDateTime($date_expire_validity)) {
                        $this->renderPartial('admin.protected.views.common._dialog2_error', array(
                            'type' => 'error',
                            'message' => Yii::t('coaching', "Ending date cannot be earlier than the starting date"),
                        ));
                        Yii::app()->end();
                    }
                }

				$ok = true;

				// Restrict the user subscription levels depending on the course type
				$restrictLevels = array();
				$removeLevels = array();

				if ($course->course_type != LearningCourse::TYPE_ELEARNING) {
					$removeLevels = array(
						LearningCourseuser::$USER_SUBSCR_LEVEL_COACH
					);
				}

				// Validate: we must have 1) List of selected users and 2) Selected Role ID
				$ok &= count($usersList) > 0;
				$ok &= (is_numeric($role) && array_key_exists($role, LearningCourseuser::getLevelsArray($restrictLevels, $removeLevels)));

				if (!$ok) {
					$this->renderPartial('admin.protected.views.common._dialog2_error', array(
						'type' => 'error',
						'message' => Yii::t('standard','_OPERATION_FAILURE'),
					));
					Yii::app()->end();
				}

                //do a check if current user is PU and there is a course max subscription set
                if(($course->max_num_subscribe > 0) && Yii::app()->user->getIsPu()) {
                    $maxSubscrQuota = $course->max_num_subscribe;
                    $allSeatsCountByCourse = $course->getTotalSubscribedUsers(false, false, false);

                        //check is there enough quota seats for this course
                        if(($maxSubscrQuota - $allSeatsCountByCourse - count($usersList)) < 0) {
                            //PU cannot enroll the selected users to this course as the there is not enough seat free as per quota set
                            $this->renderPartial('admin.protected.views.common._dialog2_error', array(
                                'type' => 'error',
                                'message' => Yii::t('course',
                                    '_COURSE_INTRO_WITH_MAX',
                                    array(
                                        '[course_type]'     => $course->course_type,
                                        '[enrolled]'        => $allSeatsCountByCourse,
                                        '[max_subscribe]'   => $maxSubscrQuota,
                                        '[course_status]'   => $course->getStatusLabelTrsnaslated($course->status)
                                    )
                                ),
                            ));
                            Yii::app()->end();
                        }
                }

				if($course->selling && CoreUserPU::isPUAndSeatManager()){
					// The current user is a Power User and he is only
					// allowed to enroll users to courses through "seats"
					// Do a check if the PU has enough seats to distribute in
					// the course (enough to cover the selected users)
					$seatsInfo = CoreUserPuCourse::getTotalAndAvailableSeats($idCourse);

					$availableSeats = $seatsInfo['available'];

					if($availableSeats < count($usersList)){
						Yii::app()->user->setFlash('error', Yii::t('standard', "You don't have enough seats for this course"));
						$this->redirect($usersSelectorOpenUrl, true);
						Yii::app()->end();
					}
				}

				if (Yii::app()->backgroundJob->bgJobEnabled(BackgroundJob::FAMILTY_MASSENROLL)) {
				    $input = array(
				        'idst' => $usersList,
				        'courses' => array($idCourse)
				    );
				    $params = array(
				        'role' => $role,
				        'date_begin_validity' => $date_begin_validity,
				        'date_expire_validity' => $date_expire_validity,
			    		'fields' => $fieldsValues
				    );

    				$this->createBackgroundJob(array(
    						'module' => 'course',
    						'value' => 'Enrolling {users} in course "{course_name}"',
    						'params' => array(
    								'users' => count($usersList),
    								//'courses' => 1
									'course_name' => $course->name,
    						)
    				), 'lms.protected.modules.backgroundjobs.components.handlers.EnrollUsersToManyCourses', $input, $params);
    				echo '<a class="auto-close success"></a>';
    				Yii::app()->end();
				}

				// Open new "progressive" dialog, while passing in the
				//selected data (user IDs and selected role/level ID)
				?>
				<script type="text/javascript">
					$(function(){
						$.post('<?=Docebo::createLmsUrl('progress/run')?>', {
							'type': <?=Progress::JOBTYPE_ENROLL_USERS_TO_COURSE?>,
							'role': <?=$role?>,
                            'date_begin_valid': <?='"'.$date_begin_validity.'"'?>,
                            'date_expire_valid': <?='"'.$date_expire_validity.'"'?>,
							'idst': '<?=is_array($usersList) ? implode(',', $usersList) : $usersList?>',
							'idCourse': <?=$idCourse?>,
							'fields': <?= json_encode($fieldsValues) ?>
						}, function(data){
							var dialogId = 'enroll-users-progressive';

							$('<div/>').dialog2({
								autoOpen: true, // We will open the dialog later
								id: dialogId,
								title: '<?=Yii::t('catalogue', '_COURSE_SUBSCRIPTION')?>'
							});
							$('#'+dialogId).html(data);
						});
					})
				</script>
				<?php

				// Close dialog2 etc.
				echo '<a class="auto-close success"></a>';
				Yii::app()->end();
			}

			if($course->selling && CoreUserPU::isPUAndSeatManager() && !CoreUserPuCourse::checkIfPuHasEnoughSeatsInCourse(1, $idCourse)){
				Yii::app()->user->setFlash('error', Yii::t('standard', "You don't have enough seats for this course"));
			}


			// Looks like we just have to START the wizard (no finish, not coming from UsersSelector...)
			$this->redirect($usersSelectorOpenUrl, true);
			Yii::app()->end();
		}

		public function actionAxProgressiveEnrollUsersToSingleCourse(){
			//define chunk size
			$chunkSize = 100;

			//if we call this function for the first time,
			//store the userIds in the session and don't send them as parameter in the request anymore
			if(isset($_POST['idst'])) {
				Yii::app()->session['userIds'] = $_POST['idst'];
			}
			$idst =	Yii::app()->session['userIds'];
			$role = $_POST['role']; // int
            $date_begin_valid = $_POST['date_begin_valid'];
            $date_expire_valid = $_POST['date_expire_valid'];
			$idCourse = $_POST['idCourse'];
			$fileds = $_POST['fields'];

			$userList = explode(',', $idst);

			if(Yii::app()->user->getIsPu()){
				// Only allow user IDs that this PU has assigned
				// This will prevent manual tampering with the request
				$userList = Yii::app()->getDb()->createCommand()
				               ->selectDistinct('t.user_id')
				               ->from(CoreUserPU::model()->tableName().' t')
				               ->join(CoreUser::model()->tableName().' u', 'u.idst=t.user_id')
				               ->where('puser_id=:currentUser', array(':currentUser'=>Yii::app()->user->getIdst()))
				               ->andWhere(array('IN', 'user_id', $userList))
				               ->queryColumn();
				$idst = implode(',', $userList);

				// Check to make sure the selected course is owned by the PU
				$idCourse = Yii::app()->getDb()->createCommand()
					->select('course_id')
					->from(CoreUserPuCourse::model()->tableName())
					->where('puser_id=:idUser AND course_id=:idCourse', array(':idUser'=>Yii::app()->user->getIdst(), ':idCourse'=>$idCourse))
					->queryScalar();
			}

			if(empty($userList)) $result['message'] = 'No users selected';
			if(empty($role)) $result['message'] = 'No role selected';
			if(empty($idCourse)) $result['message'] = 'No course selected';
			if (empty($userList) || empty($role) || empty($idCourse)) {
				$result['success'] = false;
				$result['status'] = 'error';
				$result['stop'] = true;
				$this->sendJSON($result);
				Yii::app()->end();
			}

			// Get current offset and calculate stop flag
			$offset = Yii::app()->request->getParam('offset', 0);
			$totalCount = count($userList);
			$newOffset = $offset + $chunkSize;
			$stop = ($newOffset < $totalCount) ? false : true;

			$counter = 0;
			$successfullyEnrolledCnt = 0;
			for ($index = $offset; ($index < $newOffset && $index < $totalCount); $index++) { //NOTE: $newOffset may be greater than $totalCount, but we wouldn't count over $totalCount

				//security check
				$counter ++;
				if ( $counter > $chunkSize ) {
					break;
				} //NOTE: this shouldn't happen

				//read user ID, if we are over user list limit, then invalidate the value and stop this iteration
				$userId = ( isset( $userList[ $index ] ) ? $userList[ $index ] : - 1 );
				if ( $userId < 0 ) {
					continue;
				}

				// Enroll user to course here
				$enrollment = new LearningCourseuser();
				$enrolled = $enrollment->subscribeUser($userId, Yii::app()->user->id, $idCourse, 0, $role,false,$date_begin_valid,$date_expire_valid, false, false, false, json_encode($fileds));

				if($enrolled){
					$successfullyEnrolledCnt++;

					// Check first is the user PU and seat manager, and also is the course selling
					if(LearningCourse::isCourseSelling($idCourse) && CoreUserPU::isPUAndSeatManager()) {
						Yii::app()->event->raise('onPuSeatConsume', new DEvent($this, array(
							'idCourse' => $idCourse,
							'idUser' => $userId,
							'idPowerUser' => Yii::app()->user->id,
						)));
					}
				}
			}

			if($successfullyEnrolledCnt > 0 && LearningCourse::isCourseSelling($idCourse) && CoreUserPU::isPUAndSeatManager()){
				// The current user is a PU only allowed to enroll
				// users in courses using "seats", so decrease his available
				// seats in the course by the amount of users he enrolled
				CoreUserPU::modifyAvailableSeats($idCourse, -$successfullyEnrolledCnt);
			}

			$completedPercent = $totalCount==0 ? 100 : $index/$totalCount*100;
			if($completedPercent>100) $completedPercent = 100;
			if($completedPercent<0) $completedPercent = 0;

			if($stop && isset(Yii::app()->session['userIds']))
				unset(Yii::app()->session['userIds']);

			$html = $this->renderPartial('_progressive_enroll_users', array(
				'offset' => $offset,
				'processedUsers' => $index,
				'totalUsers' => $totalCount,
				'completedPercent' => intval($completedPercent),
				'start' => ($offset == 0),
				'stop' => $stop,
				'afterUpdateCallback'=>($stop ? '$.fn.yiiListView.update("course-enrollment-list");' : null)
			), true, true);

			// Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
			// These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stopping when $stop == true)
			$data = array(
				'offset'	=> $newOffset,
				'stop'		=> $stop,
				'role'=>$role,
				'idCourse'=>$idCourse,
			);

			$response = new AjaxResult();
			$response->setStatus(true);
			$response->setHtml($html)->setData($data)->toJSON();
			Yii::app()->end();
		}

		public function actionAxProgressiveEnrollUsersToManyCourses(){
			//define chunk size
			$chunkSize = 50;

			$idst = $_POST['idst']; // array|string of user IDs to enroll
			$role = $_POST['role']; // int
			$courses = $_POST['courses'];
			$sessions = $_POST['sessions'];
			$fields = $_POST['fields'];
			$webinarSessions = $_POST['webinarSessions'];
			$date_begin_validity = Yii::app()->request->getParam('date_begin_valid', false);
			$date_expire_validity = Yii::app()->request->getParam('date_expire_valid', false);
			$finalMessage = $_POST['finalMessage'];

			$userList = explode(',', $idst);
			$coursesList = explode(',', $courses);
			$sessionsList = explode(',', $sessions);
			$webinarSessionsList = explode(',', $webinarSessions);

			if(Yii::app()->user->getIsPu()){
				// Anti tampering checks follow below (possible hack attempts)

				// Filter out any users the PU doesn't have assigned
				$userList = Yii::app()->getDb()->createCommand()
				               ->selectDistinct('t.user_id')
				               ->from(CoreUserPU::model()->tableName().' t')
				               ->join(CoreUser::model()->tableName().' u', 'u.idst=t.user_id')
				               ->where('puser_id=:currentUser', array(':currentUser'=>Yii::app()->user->getIdst()))
				               ->andWhere(array('IN', 'user_id', $userList))
				               ->queryColumn();
				$idst = implode(',', $userList);

				// Filter out any passed courses the PU doesn't have assigned
				$coursesList = Yii::app()->getDb()->createCommand()
									->selectDistinct('course_id')
									->from(CoreUserPuCourse::model()->tableName())
									->where('puser_id=:idUser', array(':idUser'=>Yii::app()->user->getIdst()))
									->andWhere(array('IN', 'course_id', $coursesList))
									->queryColumn();
				$courses = implode(',', $coursesList);

				// Filter out any passed sessions that do not belong to courses assigned to the PU
				$sessionsList = Yii::app()->getDb()->createCommand()
				                   ->selectDistinct('id_session')
				                   ->from(LtCourseSession::model()->tableName())
				                   ->andWhere(array('IN', 'course_id', $coursesList))
									->andWhere(array('IN', 'id_session', $sessionsList))
				                   ->queryColumn();
				$sessions = implode(',', $sessionsList);

				$webinarSessionsList = Yii::app()->getDb()->createCommand()
				                   ->selectDistinct('id_session')
				                   ->from(WebinarSession::model()->tableName())
				                   ->andWhere(array('IN', 'course_id', $coursesList))
									->andWhere(array('IN', 'id_session', $webinarSessionsList))
				                   ->queryColumn();
				$webinarSessions = implode(',', $webinarSessionsList);
			}

			if(empty($userList)) $result['message'] = 'No users selected';
			if(empty($role)) $result['message'] = 'No role selected';

			// When user is PU and when max quota seats is reached show the right message
			if(Yii::app()->user->getIsPu()) {
				if(!empty($finalMessage)) {
					$result['message'] = $finalMessage;
				} elseif(empty($coursesList)) {
					$result['message'] = 'No courses selected';
				}
			} else {
				if(empty($coursesList)) $result['message'] = 'No courses selected';
			}

			if (empty($userList) || empty($role) || empty($coursesList)) {
				$result['success'] = false;
				$result['status'] = 'error';
				$result['stop'] = true;
				$this->sendJSON($result);
				Yii::app()->end();
			}

			/**
			 * Create a special array with all "jobs" that this
			 * progressive action must do. This array makes counting
			 * and offsetting those "jobs" easier
			 */
			$jobs = $this->_buildSpecialArrayForMassEnrollment($userList, $coursesList, $sessionsList, $webinarSessionsList);

			// Get current offset and calculate stop flag
			$offset = Yii::app()->request->getParam('offset', 0);
			$totalCount = count($jobs);
			$newOffset = $offset + $chunkSize;
			$stop = ($newOffset < $totalCount) ? false : true;

			$counter = 0;
			for ($index = $offset; ($index < $newOffset && $index < $totalCount); $index++) { //NOTE: $newOffset may be greater than $totalCount, but we wouldn't count over $totalCount

				//security check
				$counter ++;
				if ( $counter > $chunkSize ) {
					break;
				} //NOTE: this shouldn't happen

				// Get current enrollment "job". If we are over the limit, invalidate value and stop this iteration
				$job = ( isset( $jobs[ $index ] ) ? $jobs[ $index ] : false );

				if (!$job || empty($job)) {
					continue;
				}

				$idUser = $job['idUser'];
				$idCourse = $job['idCourse'];
				$sessionsArray = $job['sessions'];
				$webinarSessionsArray = $job['webinarSessions'];

				// Enroll user to course here
				$enrollment = new LearningCourseuser();
				$enrolled = $enrollment->subscribeUser($idUser, Yii::app()->user->id, $idCourse, 0, $role, false, $date_begin_validity, $date_expire_validity, false, false, false, json_encode($fields));

				if($enrolled && LearningCourse::isCourseSelling($idCourse) && CoreUserPU::isPUAndSeatManager()){
					// The current user is a PU only allowed to enroll
					// users in courses using "seats", so decrease his available
					// seats in the course by the amount of users he enrolled
					CoreUserPU::modifyAvailableSeats($idCourse, -1);

					// Check first is the user PU and seat manager, and also is the course selling
					if(LearningCourse::isCourseSelling($idCourse) && CoreUserPU::isPUAndSeatManager()) {
						Yii::app()->event->raise('onPuSeatConsume', new DEvent($this, array(
							'idCourse' => $idCourse,
							'idUser' => $idUser,
							'idPowerUser' => Yii::app()->user->id,
						)));
					}
				}

				// Also enroll the user to the selected sessions in this course
				foreach($sessionsArray as $idSession){
					if(LtCourseuserSession::enrollUser($idUser, $idSession));
					}

				// enroll user to the selected webinar session in this course
				foreach ($webinarSessionsArray as $idSession)
				{
					WebinarSessionUser::enrollUser($idUser, $idSession);
				}
			}

			$completedPercent = $totalCount==0 ? 100 : $index/$totalCount*100;
			if($completedPercent>100) $completedPercent = 100;
			if($completedPercent<0) $completedPercent = 0;

			$html = $this->renderPartial('_progressive_enroll_users', array(
				'offset' => $offset,
				'processedUsers' => $index,
				'totalUsers' => $totalCount,
				'totalUsers' => $totalCount,
				'completedPercent' => intval($completedPercent),
				'start' => ($offset == 0),
				'stop' => $stop,
				'afterUpdateCallback'=>($stop ? "$.fn.yiiGridView.update('course-management-grid');" : null)
			), true, true);

			// Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
			// These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stopping when $stop == true)
			$data = array(
				'offset'	=> $newOffset,
				'stop'		=> $stop,
				'idst'=>$idst,
				'role'=>$role,
				'courses'=>$courses,
				'sessions'=>$sessions,
                'webinarSessions' => $webinarSessions
			);

			$response = new AjaxResult();
			$response->setStatus(true);
			$response->setHtml($html)->setData($data)->toJSON();
			Yii::app()->end();
		}

		public function actionAxProgressiveChangeEnrollmentStatus(){
			//define chunk size
			$chunkSize = 100;

			$idst = $_POST['idst']; // array|string of user IDs to enroll
			$idCourse = $_POST['idCourse'];
			$newStatus = $_POST['newStatus'];
			// Those values are sanitized by us on the original request, so we don't need to verify them again, just
			// typecasting them for better semantics
			$isScoreForced = isset($_POST['isScoreForced']) ? (boolean) $_POST['isScoreForced'] : null;
			$forcedScoreValue = isset($_POST['forcedScoreValue']) ? (float) $_POST['forcedScoreValue'] : null;
			$forcedDateComplete = isset($_POST['forcedDateComplete']) ? $_POST['forcedDateComplete'] : null;

			$userList = explode(',', $idst);

			if(Yii::app()->user->getIsPu()){
				// Anti tampering checks follow below (possible hack attempts)

				// Filter out any users the PU doesn't have assigned
				$userList = Yii::app()->getDb()->createCommand()
				               ->selectDistinct('t.user_id')
				               ->from(CoreUserPU::model()->tableName().' t')
				               ->join(CoreUser::model()->tableName().' u', 'u.idst=t.user_id')
				               ->where('puser_id=:currentUser', array(':currentUser'=>Yii::app()->user->getIdst()))
				               ->andWhere(array('IN', 'user_id', $userList))
				               ->queryColumn();
				$idst = implode(',', $userList);
			}

			if(empty($userList)) $result['message'] = 'No users selected';
			if(empty($idCourse)) $result['message'] = 'No role selected';
			if (empty($userList) || empty($idCourse)) {
				$result['success'] = false;
				$result['status'] = 'error';
				$result['stop'] = true;
				$this->sendJSON($result);
				Yii::app()->end();
			}

			// Get current offset and calculate stop flag
			$offset = Yii::app()->request->getParam('offset', 0);
			$totalCount = count($userList);
			$newOffset = $offset + $chunkSize;
			$stop = ($newOffset < $totalCount) ? false : true;

			$counter = 0;
			for ($index = $offset; ($index < $newOffset && $index < $totalCount); $index++) { //NOTE: $newOffset may be greater than $totalCount, but we wouldn't count over $totalCount

				//security check
				$counter ++;
				if ( $counter > $chunkSize ) {
					break;
				} //NOTE: this shouldn't happen

				// Get current enrollment "job". If we are over the limit, invalidate value and stop this iteration
				$userId = ( isset( $userList[ $index ] ) ? $userList[ $index ] : false );

				if (!$userId || empty($userId)) {
					continue;
				}

				// find enrollment record
				$model = LearningCourseuser::model()->findByAttributes(array(
					'idUser' => $userId,
					'idCourse' => $idCourse
				));
				if (!$model) { continue; }
				//set new status and save data
				$model->status = $newStatus;
				if(isset($model->status) && ($model->status == LearningCourseuser::$COURSE_USER_WAITING_LIST || $model->status == LearningCourseuser::$COURSE_USER_CONFIRMED)){
					$model->waiting = 1;
				}

				// Loading sanitized and properly formatted date of completion value
				if (empty($forcedDateComplete) === false) {
					$mysqlDatetimeFormat = 'Y-m-d H:i:s';
					$forcedDateComplete = Yii::app()->localtime->convertLocalDateToFormat($forcedDateComplete, $mysqlDatetimeFormat);
					$forcedDateComplete = $this->convertTimestampBetweenUsers($forcedDateComplete, $model->idUser, Yii::app()->user->id);
				}

				// Setting values for forced completion details
				if ($newStatus == LearningCourseuser::$COURSE_USER_END) {
					 // Setting the flag for forced score and score's value
					 $model->forced_score_given = $isScoreForced;
					 if ($isScoreForced === true) {
						 $model->score_given = $forcedScoreValue;
					 }

					 // Update `date_complete` by passed value
					 if ($forcedDateComplete !== null) {
						 $model->date_complete = $forcedDateComplete;
					 }
				} else {
					$model->forced_score_given = false;
				}

				$model->save();
			}

			$completedPercent = $totalCount==0 ? 100 : $index/$totalCount*100;
			if($completedPercent>100) $completedPercent = 100;
			if($completedPercent<0) $completedPercent = 0;

			$html = $this->renderPartial('_progressive_change_user_statuses_in_course', array(
				'offset' => $offset,
				'processedUsers' => $index,
				'totalUsers' => $totalCount,
				'completedPercent' => intval($completedPercent),
				'start' => ($offset == 0),
				'stop' => $stop,
				'afterUpdateCallback'=>($stop ? 'if(typeof $.fn.updateCheckboxesAfterChangeStatus != "undefined")$.fn.updateCheckboxesAfterChangeStatus(); if(typeof $.fn.yiiListView != "undefined")$.fn.yiiListView.update("course-enrollment-list");' : null)
			), true, true);

			// Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
			// These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stopping when $stop == true)
			$data = array(
				'offset'	=> $newOffset,
				'stop'		=> $stop,
				'idst'=>$idst,
				'idCourse'=>$idCourse,
				'newStatus'=>$newStatus,
			);

			$response = new AjaxResult();
			$response->setStatus(true);
			$response->setHtml($html)->setData($data)->toJSON();
			Yii::app()->end();
		}

        public function actionAxProgressiveChangeEnrollmentDeadline(){
            //define chunk size
            $chunkSize = 100;

            $idst = $_POST['idst']; // array|string of user IDs to enroll
            $idCourse = $_POST['idCourse'];
            $dateBegin = $_POST['dateBegin'];
            $dateEnd    =   $_POST['dateEnd'];

            $userList = explode(',', $idst);

            if(Yii::app()->user->getIsPu()){
                // Anti tampering checks follow below (possible hack attempts)
                // Filter out any users the PU doesn't have assigned
                $userList = Yii::app()->getDb()->createCommand()
                    ->selectDistinct('t.user_id')
                    ->from(CoreUserPU::model()->tableName().' t')
                    ->join(CoreUser::model()->tableName().' u', 'u.idst=t.user_id')
                    ->where('puser_id=:currentUser', array(':currentUser'=>Yii::app()->user->getIdst()))
                    ->andWhere(array('IN', 'user_id', $userList))
                    ->queryColumn();
                $idst = implode(',', $userList);
            }
            if(empty($userList)) $result['message'] = 'No users selected';
            if(empty($idCourse)) $result['message'] = 'No role selected';
            if (empty($userList) || empty($idCourse)) {
                $result['success'] = false;
                $result['status'] = 'error';
                $result['stop'] = true;
                $this->sendJSON($result);
                Yii::app()->end();
            }

            // Get current offset and calculate stop flag
            $offset = Yii::app()->request->getParam('offset', 0);
            $totalCount = count($userList);
            $newOffset = $offset + $chunkSize;
            $stop = ($newOffset < $totalCount) ? false : true;

            $counter = 0;
            for ($index = $offset; ($index < $newOffset && $index < $totalCount); $index++) { //NOTE: $newOffset may be greater than $totalCount, but we wouldn't count over $totalCount

                //security check
                $counter ++;
                if ( $counter > $chunkSize ) {
                    break;
                } //NOTE: this shouldn't happen

                // Get current enrollment "job". If we are over the limit, invalidate value and stop this iteration
                $userId = ( isset( $userList[ $index ] ) ? $userList[ $index ] : false );

                if (!$userId || empty($userId)) {
                    continue;
                }

                // find enrollment record
                $model = LearningCourseuser::model()->findByAttributes(array(
                    'idUser' => $userId,
                    'idCourse' => $idCourse
                ));
                if (!$model) { continue; }
                //set new status and save data
                $localFormat = Yii::app()->localtime->getPHPLocalDateTimeFormat('short', 'medium');
                $model->date_begin_validity = ($dateBegin=='0000-00-00 00:00:00')? null : date($localFormat, strtotime(Yii::app()->localtime->fromLocalDate($dateBegin).' 00:00:00'));
                $model->date_expire_validity = ($dateEnd=='0000-00-00 00:00:00')? null : date($localFormat, strtotime(Yii::app()->localtime->fromLocalDate($dateEnd).' 23:59:59'));
                $model->save();
            }

            $completedPercent = $totalCount==0 ? 100 : $index/$totalCount*100;
            if($completedPercent>100) $completedPercent = 100;
            if($completedPercent<0) $completedPercent = 0;

            $html = $this->renderPartial('_progressive_change_enrollment_deadline', array(
                'offset' => $offset,
                'processedUsers' => $index,
                'totalUsers' => $totalCount,
                'completedPercent' => intval($completedPercent),
                'start' => ($offset == 0),
                'stop' => $stop,
                'afterUpdateCallback'=>($stop ? '$.fn.updateCheckboxesAfterChangeStatus(); $.fn.yiiListView.update("course-enrollment-list");' : null)
            ), true, true);

            // Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
            // These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stopping when $stop == true)
            $data = array(
                'offset'	=> $newOffset,
                'stop'		=> $stop,
                'idst'      => $idst,
                'idCourse'  => $idCourse,
                'dateBegin' => $dateBegin,
                'dateEnd'   => $dateEnd,
            );

            $response = new AjaxResult();
            $response->setStatus(true);
            $response->setHtml($html)->setData($data)->toJSON();
            Yii::app()->end();
        }
		public function actionAxProgressiveDeleteUserEnrolment()
		{
			$chunkSize = 100;
			$tmpUsers = array();
			$html = "";
			$ids = $_POST['idst'];
			$idCourse = $_POST['idCourse'];

			// Check if this is tring
			if(is_string($ids))
			{
				// if is only one user
				if(!strpos($ids,',') !== false)
					$userList = array($ids);
				else
					$userList = explode(',', $ids);
			}
			else{
				$this->sendJSON(array());
			}

			// Get current offset and calculate stop flag
			$offset = Yii::app()->request->getParam('offset', 0);
			$totalCount = count($userList);
			$newOffset = $offset + $chunkSize;

			$start = ($offset==0); // first run
			$stop = ($newOffset < $totalCount) ? false : true; // last run

			$counter = 0;
			for ($index = $offset; ($index < $newOffset && $index < $totalCount); $index++) { //NOTE: $newOffset may be greater than $totalCount, but we wouldn't count over $totalCount
				$counter ++;
				if ( $counter > $chunkSize ) break; //NOTE: this shouldn't happen
				// Get current enrollment "job". If we are over the limit, invalidate value and stop this iteration
				$userId = ( isset( $userList[ $index ] ) ? $userList[ $index ] : false );
				if (!$userId || empty($userId))continue;
				$tmpUsers[] = $userId;
			}

			$isPuAndCourseSeatManager = CoreUserPU::isPUAndSeatManager();
			$failedToUnenrollDueToTakenSeat = Yii::app()->request->getParam('failed', '');
			$tmpFailedToUnenrollDueToTakenSeat = array();
			$countUnsubscribed = 0;
			$learningCourseUsers = LearningCourseuser::model()->findAllByAttributes(array('idCourse' => $idCourse, 'idUser' => $tmpUsers));

			if($start && LearningCourse::isCourseSelling($idCourse) && $isPuAndCourseSeatManager && !CoreUserPuCourse::checkIfPuHasEnoughSeatsInCourse(1, $idCourse)){
				// At the start of this progressive operation a Power User
				// who is a "seat manager" had no seats available in this course
				// When the progressive operation ends, check if he now has some
				// seats and if yes, reload the UI after he closes the progressive
				// dialog so he now sees updated UI (accessible button to enroll new
				// users, since he now has seats)
				Yii::app()->user->setState('reload_page_after_dialog_close', true);
			}

			if(!empty($learningCourseUsers))
			{
				foreach ($learningCourseUsers as $courseUser) {
					if(LearningCourse::isCourseSelling($idCourse) && $isPuAndCourseSeatManager){
						if($courseUser->status == LearningCourseuser::$COURSE_USER_SUBSCRIBED){
							$res = $courseUser->unsubscribeUser($courseUser->idUser, $idCourse);
							if($res) {
								$countUnsubscribed++;

								if(LearningCourse::isCourseSelling($idCourse) && CoreUserPU::isPUAndSeatManager()) {
									Yii::app()->event->raise( 'onPuSeatRestore', new DEvent( $this, array(
										'idCourse'    => $idCourse,
										'idUser'=>$courseUser->idUser,
										'idPowerUser' => Yii::app()->user->getIdst(),
									) ) );
								}
							}
						}else{
							$tmpFailedToUnenrollDueToTakenSeat[] = $courseUser->user->idst;
						}
					}else{
						// Regular operation. Current user may not be a PU at all
						$res = $courseUser->unsubscribeUser($courseUser->idUser, $idCourse);
						if($res)
							$countUnsubscribed++;
					}
				}

			}

			// If $failedToUnenrollDueToTakenSeat is different than "", empty string then add comma in front before add more failedRecords
			if(!empty($tmpFailedToUnenrollDueToTakenSeat))
				$failedToUnenrollDueToTakenSeat .= ($failedToUnenrollDueToTakenSeat == '' ?  "" : ',' ) . implode(',', $tmpFailedToUnenrollDueToTakenSeat) ;

			/*
			 * If this is the last loop and there is failed unenrolments
			 * Show them to the user
			 */
			if($stop && $failedToUnenrollDueToTakenSeat != '')
				$html .= $this->handleFailedUnenrollments($idCourse, $isPuAndCourseSeatManager, $countUnsubscribed, $failedToUnenrollDueToTakenSeat);

			// Increase seats
			if(LearningCourse::isCourseSelling($idCourse) && $isPuAndCourseSeatManager && $countUnsubscribed > 0) {
				// Increase PUs seats allowance inside the course,
				// now that he has unsubscribed some users that haven't
				// used their seat inside the course
				CoreUserPU::modifyAvailableSeats($idCourse, $countUnsubscribed);
			}

			$courseModel = LearningCourse::model()->findByPk($idCourse);
			$courseModel->automaticallyEnroll(LearningCourse::TYPE_ELEARNING, null, $countUnsubscribed);

			$completedPercent = $totalCount==0 ? 100 : $index/$totalCount*100;
			if($completedPercent>100) $completedPercent = 100;
			if($completedPercent<0) $completedPercent = 0;

			$html .= $this->renderPartial('_progressive_delete_user_enrollment', array(
				'offset' 				=> $offset,
				'processedUsers' 		=> $index,
				'totalUsers' 			=> $totalCount,
				'completedPercent' 		=> intval($completedPercent),
				'start' 				=> ($offset == 0),
				'stop' 					=> $stop,
			), true, true);

			// Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
			// These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stopping when $stop == true)
			$data = array(
				'offset'				=> $newOffset,
				'stop'					=> $stop,
				'idst'					=> $ids,
				'idCourse'				=> $idCourse,
				'failed' 				=> $failedToUnenrollDueToTakenSeat,
				'countUnsubscribed' 	=> $countUnsubscribed,
			);

			$response = new AjaxResult();
			$response->setStatus(true);
			$response->setHtml($html)->setData($data)->toJSON();
			Yii::app()->end();
		}

		public function handleFailedUnenrollments($idCourse, $isPuAndCourseSeatManager, $countUnsubscribed, $failedToUnenrollDueToTakenSeat)
		{
			$html = '';

			// Get all the users
			$learningCourseUsers = Yii::app()->db->createCommand()
				->select('GROUP_CONCAT( IF(cu.`firstname` != "" AND cu.`lastname` != "", CONCAT(cu.`firstname`, " ", cu.`lastname`), IF(cu.`firstname` != "", cu.`firstname`, REPLACE(cu.`userid`, "/", ""))) SEPARATOR "," )')
				->from('learning_courseuser lc')
				->join('core_user cu', 'cu.`idst` = lc.`idUser`')
				->where('idCourse = :idCourse', array(':idCourse'=>$idCourse))
				->andWhere( array('in', 'idUser',  explode(',', $failedToUnenrollDueToTakenSeat) ) )
				->queryScalar();

			if($learningCourseUsers)
			{
				$learningCourseUsersArr = explode(',', $learningCourseUsers);
				if(is_array($learningCourseUsersArr) && !empty($learningCourseUsersArr)){
					$html = Yii::t('standard', 'Number of users that could not be unenrolled').": <strong>";
					$html .= '('.count($learningCourseUsersArr).')';
				$html .= '</strong><br/><br/>';
			}

			}

			return $html;
		}

		public function actionAxProgressiveSendEmailToEnrolledUsers()
		{
			$chunkSize = 50;
			$tmpUsers = array();
			$html = "";
			$recipients = array();

			$ids = $_POST['idst'];
			$from = $_POST['from'];
			$subject = $_POST['subject'];
			$message = $_POST['message'];
			if(!empty($_POST['courseId'])){
				$_SESSION['courseId'] = intval($_POST['courseId']);
			}
			$courseId = $_SESSION['courseId'];

			if(!empty($_POST['sessionId'])){
				$_SESSION['sessionId'] = intval($_POST['sessionId']);
			}
			$sessionId = intval($_SESSION['sessionId']);
			if($sessionId === 0) $sessionId = null;
			// Check if this is tring
			if(is_string($ids))
			{
				// if is only one user
				if(!strpos($ids,',') !== false)
					$userList = array($ids);
				else
					$userList = explode(',', $ids);
			}
			else{
				$this->sendJSON(array(
					'status' => 'error', // success|error
					'html' => Yii::t('standard', '_OPERATION_FAILURE') .
						"<script>$('.btn-submit').remove();</script>",
				));
			}

			// Get current offset and calculate stop flag
			$offset = Yii::app()->request->getParam('offset', 0);
			$totalCount = count($userList);
			$newOffset = $offset + $chunkSize;
			$stop = ($newOffset < $totalCount) ? false : true;
			$counter = 0;
			for ($index = $offset; ($index < $newOffset && $index < $totalCount); $index++) { //NOTE: $newOffset may be greater than $totalCount, but we wouldn't count over $totalCount
				$counter ++;
				if ( $counter > $chunkSize )break; //NOTE: this shouldn't happen
				// Get current enrollment "job". If we are over the limit, invalidate value and stop this iteration
				$userId = ( isset( $userList[ $index ] ) ? $userList[ $index ] : false );
				if (!$userId || empty($userId))continue;
				$tmpUsers[] = $userId;
			}

			if(!empty($_POST['ccMails'])) {
				$ccMails = array_filter(explode(',', $_POST['ccMails']));
				$ccMails = array_map('trim', $ccMails);
			}else{
				$ccMails = array();
			}

			//Emails
			$query = Yii::app()->db->createCommand()->select('idst, email')->from('core_user')->where(array('in', 'idst', $tmpUsers))->andWhere("email != ''")->queryAll();
            if(!empty($query)){
			foreach($query as $row)
				$recipients[$row['idst']] = $row['email'];

				// Sending emails
				// Send emails in batches of 50
				$matches = array();
				$subjectMatches = array();
				preg_match_all('(\[\w+\])', $message, $matches)[0];
				preg_match_all('(\[\w+\])', $subject, $subjectMatches)[0];
				$matches = $matches[0];
				$subjectMatches = $subjectMatches[0];

				foreach ($recipients as $userId => $recipient) {
					try {
						$messageReplaced = $this->getShortCodesData($message, $matches, $courseId, $userId, $sessionId);
						$subjectReplaced = $this->getShortCodesData($subject, $subjectMatches, $courseId, $userId, $sessionId);
						$mm = new MailManager();
						$mm->mail($from, $recipient, strip_tags($subjectReplaced), $messageReplaced, false, $ccMails);
					} catch (Exception $e) {
						$this->sendJSON(array(
								'status' => 'error', // success|error
								'html' => Yii::t('standard', '_OPERATION_FAILURE') . " <br /><br />" . $e->getMessage()
						));
					}
				}
			}


			$completedPercent = $totalCount==0 ? 100 : $index/$totalCount*100;
			if($completedPercent>100) $completedPercent = 100;
			if($completedPercent<0) $completedPercent = 0;

			$html .= $this->renderPartial('_progressive_send_email_to_enrolled_users', array(
				'offset' => $offset,
				'processedUsers' 		=> $index,
				'totalUsers' 			=> $totalCount,
				'completedPercent' 		=> intval($completedPercent),
				'start' 				=> ($offset == 0),
				'stop' 					=> $stop,
			), true, true);

			// Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
			// These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stopping when $stop == true)
			$data = array(
				'offset'	=> $newOffset,
				'stop'		=> $stop,
				'idst'		=>$ids,
				'from' 		=> $from,
				'subject' 	=> $subject,
				'message' 	=> $message,
			);

			$response = new AjaxResult();
			$response->setStatus(true);
			$response->setHtml($html)->setData($data)->toJSON();
			Yii::app()->end();
		}

		/**
		 * This is a highly specific method that is only useful in the
		 * mass MANY users=>MANY courses enrollment (+ enroll to course sessions).
		 *
		 * Please, don't change if not needed and consult me if needed (Dzhuneyt).
		 * It generates an array that is easy to digest by the progressive tool
		 * and makes it easy to calculate total items to process, offsets, index
		 * and so on.
		 *
		 * Each element in the returned array contains:
		 *
		 *
		 * @param $userIdsList
		 * @param $courseIdsList
		 * @param $sessionIdsList
		 * @param $webinarSessionsList
		 *
		 * @return array where each element contains:
		 * array
		 *  'idUser'=>integer,
		 *  'idCourse'=>integer,
		 *  'sessions'=>array, // selected sessions from this course to which to enroll the user (besides enrolling him to the course itself)
		 * )
		 */
		private function _buildSpecialArrayForMassEnrollment($userIdsList, $courseIdsList, $sessionIdsList, $webinarSessionsList){
			// Organize selected session by course ID first
			$tmpSessionRows = Yii::app()->getDb()->createCommand()
				->select('id_session, course_id')
				->from(LtCourseSession::model()->tableName())
				->where(array('IN', 'course_id', $courseIdsList))
				->andWhere(array('IN', 'id_session', $sessionIdsList))
				->queryAll();

			$tmpWebinarSessionRows = Yii::app()->getDb()->createCommand()
				->select('id_session, course_id')
				->from(WebinarSession::model()->tableName())
				->where(array('IN', 'course_id', $courseIdsList))
				->andWhere(array('IN', 'id_session', $webinarSessionsList))
				->queryAll();

			$res = array();
			foreach($courseIdsList as $idCourse){
				$sessionsToEnrollIn = array();
				$webinarSessionsToEnrollIn = array();

				$event = new DEvent($this, array('course_id'=>$idCourse, 'sessions' => &$sessionsToEnrollIn));
				Yii::app()->event->raise('collectSessionsForClassroomCourse', $event);
				if(empty($sessionsToEnrollIn)) {
				foreach($tmpSessionRows as $tmp){
					// Collection sessions which were:
					// - selected in the wizard (the SQL query above itself filters out others) AND
					// - belong to the currently iterated course ID
					if($tmp['course_id'] == $idCourse) $sessionsToEnrollIn[] = $tmp['id_session'];
				}
				}

				foreach($tmpWebinarSessionRows as $tmp){
					// Collection sessions which were:
					// - selected in the wizard (the SQL query above itself filters out others) AND
					// - belong to the currently iterated course ID
					if($tmp['course_id'] == $idCourse) $webinarSessionsToEnrollIn[] = $tmp['id_session'];
				}

				foreach($userIdsList as $idUser){
					$res[] = array(
						'idUser'=>$idUser,
						'idCourse'=>$idCourse,
						'sessions'=>$sessionsToEnrollIn,
						'webinarSessions'=>$webinarSessionsToEnrollIn
					);
				}
			}

			return $res;
		}

        /**
         * Check if user is Power User and if he's only allowed to enroll users as students
         * @return bool
         */
        protected function _allowOnlyStudentEnrollments() {
            if (Yii::app()->user->getIsPu()) {
                $allowOnlyStudentEnrollments = CoreUser::adminRuleEnabled(Yii::app()->user->id, 'allow_only_student_enrollments');
                if ($allowOnlyStudentEnrollments)
                    return true;
            }
            return false;
        }


        /**
         * Process the user levels array
         * @return array
         */
        protected function _getLevelsArray($restrict = array(), $remove = array()) {
            $restrictLevels = ($this->_allowOnlyStudentEnrollments()) ? array(LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT) : $restrict;
            $userLevels = LearningCourseuser::getLevelsArray($restrictLevels, $remove);
            return $userLevels;
        }


		/**
		 * User clicked on 'delete logo icon' and controller ask this function to delete it
		 * Internally, if everything is ok, send back JSON and end the application!
		 */
		private function handleDeleteLogoAjax() {
			// Handle Delete Logo Image AJAX calls
			if (isset($_REQUEST['delete_image']) && isset($_REQUEST['image_id'])) {
				$imageId = Yii::app()->request->getParam("image_id", false);
				$success = false;
				if ((int) $imageId > 0) {
					$asset = CoreAsset::model()->findByPk($imageId);
					if ($asset) {
						$success = $asset->delete();
					}
				}
				$ajaxResult = new AjaxResult();
				$data = array();
				$ajaxResult->setStatus($success)->setData($data)->toJSON();
				//if ($transaction) $transaction->commit();
				Yii::app()->end();
			}
        }

		/**
		 * Validates the passed parameter is a valid number - integer/float/double
		 * @param mixed $number
		 * @return integer|float|null
		 */
        private static function sanitizeNumber($number)
		{
			if ($number === null) {
				return null;
			}

			if (is_numeric($number) === true) {
				return (float) $number;
			}

			return null;
		}

        /**
         * Change status in the course for a list of enrolled users
         * @throws CException
         */
        public function actionAxMassiveChangeStatus() {

        	$rq = Yii::app()->request;
        	$currentCourseId = Yii::app()->session['currentCourseId'];

        	//check if we have confirmed action
        	if ($rq->getParam('confirm', false) !== false) {

        		//read dialog inputs
        		$idList = $rq->getParam('id-list', '');
        		$newStatus = $rq->getParam('new-status', false);

                $forcedCompletionDetails = $rq->getParam('forced_completion_details', []);
                $forcedDateComplete = null;
                $forcedScoreValue = null;
                $isScoreForced = null;

                // Loading sanitized forced score flag
                $isScoreForced = isset($forcedCompletionDetails['forced_score_given']);

                // Loading sanitized forced score value
                if ($isScoreForced === true) {
                    $forcedScoreValue = static::sanitizeNumber($forcedCompletionDetails['score_given']);

                    if ($forcedScoreValue === null) {
                        $isScoreForced = false;
                    }
                }

                // Loading sanitized and properly formatted date of completion value
                if (empty($forcedCompletionDetails['date_complete']) === false) {
					$forcedDateComplete = $forcedCompletionDetails['date_complete'];
                    $forcedDateComplete = Yii::app()->localtime->convertLocalDateToFormat($forcedCompletionDetails['date_complete'], 'Y-m-d');
					$date = new \DateTime($forcedDateComplete, new \DateTimeZone('UTC'));
                }

				//validate inputs
        		if (empty($idList)) {
        			throw new CException('Invalid specified user(s)');
        		}

				$users = explode(',', $idList);
        		$statuses = LearningCourseuser::getStatusesArray();
        		if ($newStatus === false || !isset($statuses[$newStatus])) {
        			throw new CException('Invalid specified status');
        		}

		        // Open new "progressive" dialog, while passing in the
		        //selected data (user IDs and selected role/level ID)
				$params = array(
					'newStatus'=>$newStatus,
					'users'=>$users,
					'currentCourseId'=>$currentCourseId,
				);

				$params['isScoreForced'] = $isScoreForced;

				if ($forcedDateComplete !== null) {
					$params['forcedDateComplete'] = $forcedDateComplete;
				}

				if ($isScoreForced === true) {
					$params['forcedScoreValue'] = $forcedScoreValue;
				}

				$html = $this->renderPartial('_mass_enrollment_status_change_last_step', $params, true);

		        $this->sendJSON(array('status' => 'saved', 'preserveSelection' => true, 'html'=>$html.'<a class="auto-close"></a>'));
		        Yii::app()->end();

				// Commented out, was here already, not executable, probably left as a reference
				// //since we have multiple operations involved, use transactions to avoid partial updates
				// $transaction = Yii::app()->db->beginTransaction();
				// try {
				//     foreach ($users as $idUser) {
				//         //find enrollment record
				//         $model = LearningCourseuser::model()->findByAttributes(array(
				//                 'idUser' => $idUser,
				//                 'idCourse' => $currentCourseId
				//         ));
				//         if (!$model) { throw new CException('Invalid user/course'); }
				//
				//         //set new status and save data
				//         $model->status = $newStatus;
				//         if(isset($model->status) && ($model->status == LearningCourseuser::$COURSE_USER_WAITING_LIST || $model->status == LearningCourseuser::$COURSE_USER_CONFIRMED)){
				//             $model->waiting = 1;
				//         }

				//         // Setting the flag for forced score and score's value
				//         $model->forced_score_given = $isScoreForced;
				//         if ($isScoreForced === true) {
				//             $model->score_given = $forcedScoreGiven;
				//         }
				//
				//         // Update `date_complete` by passed value
				//         if ($forcedDateComplete !== null) {
				//             $model->date_complete = $forcedDateComplete;
				//         }
				//
				//         $model->save();
				//     }
				//
				//     $transaction->commit();
				// } catch (CException $e) {
				//     //revert DB changes done so far
				//     $transaction->rollback();
				//     //re-throw exception in order to be managed at higher level
				//     throw $e;
				// }
				// $this->sendJSON(array('status' => 'saved', 'preserveSelection' => true));
        	}

	        $ids = $rq->getParam('ids', array());
	        if(is_string($ids) && stripos($ids, ',')!==false){
		        $ids = explode(',', $ids);
	        }elseif(!stripos($ids, ',')!==false ){ // if is selected only one user, cast it to array
				$ids = array($ids);
			}

        	//render dialog
        	$content = $this->renderPartial('_massiveChangeStatus', array(
        			'ids' => $ids
        	), true);
        	$this->sendJSON(array('html' => $content));
        }

        /**
         * Change enrollment deadline for the course for a list of enrolled users
         * @throws CException
         */
        public function actionAxMassiveChangeEnrollmentDeadline(){
            $currentCourseId = Yii::app()->session['currentCourseId'];
            $rq = Yii::app()->request;

            if ($rq->getParam('confirm', false) !== false) {
                //read dialog inputs
                $idList = $rq->getParam('id-list', '');
                $dates  = $rq->getParam('LearningCourseuser', false);


                //validate inputs
                if (empty($idList)) {
                    throw new CException('Invalid specified user(s)');
                }
                if (empty($dates)) {
                    throw new CException('Invalid specified date(s)');
                }
                $dateBegin  = ($dates['date_begin_validity'])? $dates['date_begin_validity'] : '0000-00-00 00:00:00';
                $dateEnd    = ($dates['date_expire_validity'])? $dates['date_expire_validity'] : '0000-00-00 00:00:00';

                if ($dates['date_begin_validity'] && $dates['date_expire_validity'] && Yii::app()->localtime->fromLocalDateTime($dateBegin) > Yii::app()->localtime->fromLocalDateTime($dateEnd)) {
                    $this->sendJSON(array(
                        'status' => 'error',
                        'html' => Yii::t('coaching', "Ending date cannot be earlier than the starting date")."<script>$('.btn-submit').remove();</script>",
                    ));
                    Yii::app()->end();
                }
                $users = explode(',', $idList);
                $html = $this->renderPartial('_mass_change_enrollment_deadline', array(
                    'dateBegin'=>$dateBegin,
                    'dateEnd'=>$dateEnd,
                    'users'=>$users,
                    'currentCourseId'=>$currentCourseId,
                ), true);

                $this->sendJSON(array('status' => 'saved', 'preserveSelection' => true, 'html'=>$html.'<a class="auto-close"></a>'));
                Yii::app()->end();


            }
            $ids = $rq->getParam('ids', array());
            if(is_string($ids) && stripos($ids, ',')!==false){
                $ids = explode(',', $ids);
            }elseif(!stripos($ids, ',')!==false ){ // if is selected only one user, cast it to array
                $ids = array($ids);
            }

			$course = LearningCourse::model()->findByPk($currentCourseId);
			$nowTimestamp = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d'));
			$dateBeginTimestamp = ($course->date_begin && $course->date_begin != '0000-00-00') ? strtotime(Yii::app()->localtime->fromLocalDate($course->date_begin)) : null;
			$dateEndTimestamp = ($course->date_end && $course->date_end != '0000-00-00') ? strtotime(Yii::app()->localtime->fromLocalDate($course->date_end)) : null;
			$showDeadlineSelector = !$dateEndTimestamp || ($nowTimestamp <= $dateEndTimestamp);
			$initialDateBegin = $dateBeginTimestamp ? $course->date_begin : Yii::app()->localtime->toLocalDate(Yii::app()->localtime->getUTCNow());
			$initialDateEnd = $dateEndTimestamp ? $course->date_end : '';

	        $commmonDateBeginArray = LearningCourseuser::getCommonDate($course->idCourse,$ids, false, true, true);
	        $commmonDateBegin = $commmonDateBeginArray['date'];

	        $commmonDateEndArray = LearningCourseuser::getCommonDate($course->idCourse,$ids,true, true, true);
	        $commmonDateEnd = $commmonDateEndArray['date'];

	        $initialDateBegin = ($commmonDateBegin) ? $commmonDateBegin : $initialDateBegin;
	        $initialDateEnd = ($commmonDateEnd) ? $commmonDateEnd : $initialDateEnd;

	        // Whatever to show warning multiple deadlines selected
	        $showMultipleDeadlinesAssignedWarning   = false;
	        $outlineDateBegin                       = false;
	        $outlineDateEnd                         = false;

	        if (($commmonDateBeginArray['count'] > 1) || ($commmonDateEndArray['count'] > 1)) {
				$showMultipleDeadlinesAssignedWarning = true;

		        if ($commmonDateBeginArray['count'] > 1) {
			        $outlineDateBegin = true;
		        }

		        if ($commmonDateEndArray['count'] > 1) {
			        $outlineDateEnd = true;
		        }
	        }

	        $content = $this->renderPartial('_massiveChangeEnrollmentDeadline', array(
                'ids'                                   => $ids,
                'course'                                => $course,
				'showDeadlineSelector'                  => $showDeadlineSelector,
				'initialDateBegin'                      => $initialDateBegin,
				'initialDateEnd'                        => $initialDateEnd,
				'showMultipleDeadlinesAssignedWarning'  => $showMultipleDeadlinesAssignedWarning,
				'outlineDateBegin'                      => $outlineDateBegin,
				'outlineDateEnd'                        => $outlineDateEnd
            ), true);

            $this->sendJSON(array('html' => $content));
        }

        /**
         * Handle "Mass enrollment wizard (Many Users to Many Courses enrollment)"
         * Uses UsersSelector and jWizard to travel from dialog to dialog (forth and back)
         *
         * Based on "from_step" (i.e. from which dialog we are coming) and if the PREV or NEXT button is clicked,
         * a decision is taken which dialog to show NEXT while checking if some prerequisites are met
         * (example: can not move to Courses selection if NO users are selected in the previous step).
         *
         */
        public function actionAxEnrollUsersToManyCourses() {

        	// FromStep is basically the NAME of the dialog we are giving when we launch it
        	$fromStep 	= Yii::app()->request->getParam('from_step', false);

        	// Every dialog, part of jWizard sequence, **may** have a "finish wizard" button
        	$finish   	= Yii::app()->request->getParam('finish_wizard', false);

        	// The same with PREV and NEXT buttons
        	$prevButton = Yii::app()->request->getParam('prev_button', false);
        	$nextButton = Yii::app()->request->getParam('next_button', false);

        	// Selectors names (also used for form IDs and for step/dialog naming)
        	$usersName 		= 'users-selector-users';
        	$coursesName 	= 'users-selector-courses';
        	$sessionsName 	= 'enroll-session-mass';
        	$roleName 		= 'enroll-role-step';

        	// Limit selections of users and courses
        	//$maxUsers 	= isset(Yii::app()->params['mass_enrollment_max_users']) ? (int) Yii::app()->params['mass_enrollment_max_users'] : 1000;
        	$maxCourses = isset(Yii::app()->params['mass_enrollment_max_courses']) ? (int) Yii::app()->params['mass_enrollment_max_courses'] : 100;

	        $loadGridHelperScripts = (isset($_REQUEST['include_grid_scripts']) && $_REQUEST['include_grid_scripts']);


        	// Finish button clicked ?
        	if ($finish) {
        		// By convention, jWizard is POSTing all collected data from ALL dialogs in a special POST variable, named "Wizard"
        		$WizardData 	= $_REQUEST['Wizard'];

        		// Which has specific (byt well known) structure, following the names of dialogs we specified, when we open them
        		$usersList 		= UsersSelector::getUsersList($WizardData[$usersName], false, true);
        		$coursesList 	= UsersSelector::getCoursesListOnly($WizardData[$coursesName]);
        		$role 			= $WizardData['enroll-role-step-form']['enrollment_role'];
        		$enrollmentFields = LearningEnrollmentField::getFieldsByCourse();


        		//			ADD FIELD VALUES IN LEARNING_COURSEUSER TABLE IN ADDITIONAL_FIELDS COLUMN
        		$enrollmentFieldsForCourseUser=[];
        		foreach ($enrollmentFields as $field){
        			$fieldValue=Yii::app()->request->getParam('field-'.$field['id'], '');
        			$enrollmentFieldsForCourseUser[$field['id']]=$fieldValue;
        		}
        		foreach ($coursesList as $oneCourseId) {
        		
        			//				GET ONLY AVAILABLE ENROLLMENT FIELD FOR THIS COURSE
        			$availableFieldsForThisCouseRaw = LearningEnrollmentField::getFieldsByCourse($oneCourseId);
        			$availableFieldsForThisCouse = [];
        			foreach ($availableFieldsForThisCouseRaw as $oneAvailable) {
        				$availableFieldsForThisCouse[$oneAvailable['id']] = $oneAvailable['id'];
        			}
        		
        			//				GET ONLY ENROLLMENT FIELDS FROM $availableFieldsForThisCouse WHO ARE IN $enrollmentFieldsForCourseUser
        			$fieldsForThisCourse=array_intersect_key($enrollmentFieldsForCourseUser, $availableFieldsForThisCouse);
        		
        			foreach ($usersList as $oneUserId) {
        		
        		
        				$courseUserModel = LearningCourseuser::model()->findByAttributes(
        						array('idUser' => $oneUserId, 'idCourse' => $oneCourseId)
        						);
        		
        				//					SAVE IN COURSE_USER_MODEL
        				if ($courseUserModel) {
        					$courseUserModel->enrollment_fields = json_encode($fieldsForThisCourse);
        					$courseUserModel->save(false);
        				}
        			}
        		}
        		
        			
        		$fieldsValues = array();
        		
        		foreach ($enrollmentFields as $field) {
        			if (!empty($WizardData['enroll-role-step-form']['field-' . $field['id']])) {
        				$fieldsValues[$field['id']] = $WizardData['enroll-role-step-form']['field-' . $field['id']];
        			}
        		}
        			
        		$validFields = LearningEnrollmentField::validateEnrollmentArray($fieldsValues);
        		if(!$validFields['success']){
        			$errors_mass_enroll = array(
        					'required' => Yii::t('standard', 'Missing required fields or invalid fields data provided')
        			);
        		
        			$fields = LearningEnrollmentField::getFieldsByCourse();
        			$finishUrl = Docebo::createAdminUrl('courseManagement/axEnrollUsersToManyCourses');
        			$restrictLevels = array();
        			$removeLevels = array();
        		
        			$skipSoftDeadline = false;
        			$skipCoachLevel = false;
        		
        			// Check all courses for their type and coaching enabled
        			foreach ($coursesList as $courseModelId) {
        				// Get current iteration course model
        				$courseModel = LearningCourse::model()->findByPk($courseModelId);
        		
        				// If there is at least one not eLearning course skip coach level
        				if ($courseModel->course_type != LearningCourse::TYPE_ELEARNING) {
        					$skipCoachLevel = true;
        				}
        		
        				// If there is at least one not eLearning course skip soft deadlines
        				// of it is a learning course, but it's not set using of soft deadline from course settings
        				//if ($courseModel->course_type != LearningCourse::TYPE_ELEARNING || ($courseModel->course_type == LearningCourse::TYPE_ELEARNING && $courseModel->soft_deadline != 1)) {
        				if ($courseModel->course_type != LearningCourse::TYPE_ELEARNING) {
        					$skipSoftDeadline = true;
        				}
        			}
        		
        			$html = $this->renderPartial('enroll_role_step_mass', array(
        					'coursesCount' => count($coursesList),
        					'usersCount' => count($usersList),
        					'finishUrl' => $finishUrl,
        					'userLevels' => $this->_getLevelsArray($restrictLevels, $removeLevels),
        					'skipSoftDeadline' => $skipSoftDeadline,
        					'fields' => $fields,
        					'errors' => $errors_mass_enroll
        			), true);
        			echo $html;
        			Yii::app()->end();
        		
        			//
        			//				$this->renderPartial('admin.protected.views.common._dialog2_error', array(
        			//					'type' => 'error',
        			//					'message' => Yii::t('coaching', "Missing required fields or invalid fields data provided"),
        			//				));
        			//				Yii::app()->end();
        		}
        		
        		
        		
		        $localFormat = Yii::app()->localtime->getPHPLocalDateTimeFormat('short', 'medium');
		        $settingEnrollmentDeadlines = !empty($WizardData['enroll-role-step-form']['set_enrollment_deadlines']) ?  $WizardData['enroll-role-step-form']['set_enrollment_deadlines'] : false;

		        if($settingEnrollmentDeadlines) {
			        $date_begin_validity = false;
			        if ($WizardData['enroll-role-step-form']['LearningCourseuser']['date_begin_validity']) {
				        $date_begin_validity = $WizardData['enroll-role-step-form']['LearningCourseuser']['date_begin_validity'];
				        $date_begin_validity = date($localFormat, strtotime(Yii::app()->localtime->fromLocalDate($date_begin_validity) . ' 00:00:00'));
			        }

			        $date_expire_validity = false;
			        if ($WizardData['enroll-role-step-form']['LearningCourseuser']['date_expire_validity']) {
				        $date_expire_validity = $WizardData['enroll-role-step-form']['LearningCourseuser']['date_expire_validity'];
				        $date_expire_validity = date($localFormat, strtotime(Yii::app()->localtime->fromLocalDate($date_expire_validity) . ' 23:59:59'));
			        }

			        if ($date_begin_validity && $date_expire_validity && Yii::app()->localtime->fromLocalDateTime($date_begin_validity) > Yii::app()->localtime->fromLocalDateTime($date_expire_validity)) {
				        $this->renderPartial('admin.protected.views.common._dialog2_error', array(
					        'type' => 'error',
					        'message' => Yii::t('coaching', "Ending date cannot be earlier than the starting date"),
				        ));
				        Yii::app()->end();
			        }
		        }


		        // Restrict the user subscription levels depending on the course type
		        $restrictLevels = array();
		        $removeLevels = array();
		        $skipCoachLevel = false;

		        // Check all courses for their type and coaching enabled
		        foreach ($coursesList as $courseModelId) {
			        // Get current iteration course model
			        $courseModel = LearningCourse::model()->findByPk($courseModelId);

			        // If there is at least one not eLearning course skip coach level
			        if ($courseModel->course_type != LearningCourse::TYPE_ELEARNING) {
				        $skipCoachLevel = true;
			        }
		        }

		        // Exclude Coach level here if needed
		        if ($skipCoachLevel) {
			        $removeLevels = array(
				        LearningCourseuser::$USER_SUBSCR_LEVEL_COACH
			        );
		        }

		        $ok = (is_numeric($role) && array_key_exists($role, LearningCourseuser::getLevelsArray($restrictLevels, $removeLevels)));
		        if (!$ok) {
        			$this->renderPartial('admin.protected.views.common._dialog2_error', array(
        					'type' => 'error',
        					'message' => Yii::t('standard','_OPERATION_FAILURE') . ' (invalid role)',
        			));
        			Yii::app()->end();
        		}

		        if(CoreUserPU::isPUAndSeatManager()) {
			        // The current user is a Power User and he is only
			        // allowed to enroll users to courses through "seats"
			        // Do a check if the PU has enough seats to distribute in
			        // the selected courses (enough the cover the selected users)
			        $coursesWhereNotEnoughSeats = CoreUserPuCourse::getMassCoursesWithInsufficientSeats($coursesList, count($usersList));

			        if(!empty($coursesWhereNotEnoughSeats)){
				        $errorMessage = '';
				        foreach($coursesWhereNotEnoughSeats as $tmpCourse){
					        $errorMessage .= '<p>'.Yii::t('standard', 'Not enough seats in course: :name', array(':name'=>$tmpCourse['name'])).'</p>';
				        }
				        $this->renderPartial('admin.protected.views.common._dialog2_error', array(
					        'type' => 'error',
					        'message' => Yii::t('standard','_OPERATION_FAILURE') . $errorMessage,
				        ));
				        Yii::app()->end();
			        }

		        }

                // Error message holder
                $errorQuotaMessages = '';
                $quotaExceededCoursesCount = 0;
                // Do a check if current user is PU and there is a course max subscription set
                if(Yii::app()->user->getIsPu()) {
                    foreach ($coursesList as $courseItem) {
                        // Get course data
                        $courseDetails = Yii::app()->getDb()->createCommand()
                            ->select('course.*')
                            ->from(LearningCourse::model()->tableName() . ' course')
                            ->where('course.idCourse=:idCourse',
                                array(
                                    ':idCourse' => $courseItem
                                )
                            )
                            ->queryRow();

                        if (($courseDetails['max_num_subscribe'] > 0)) {
                            $maxSubscrQuota = $courseDetails['max_num_subscribe'];
							$course = LearningCourse::model()->findByPk($courseItem);
                            $allSeatsCountByCourse = $course->getTotalSubscribedUsers(false, false, false);

                            // Check is there enough quota seats for this course
                            if (($maxSubscrQuota - $allSeatsCountByCourse - count($usersList)) < 0) {
                                // PU cannot enroll the selected users to this course as the there is not enough seat free as per quota set
                                $errorQuotaMessages .= $courseDetails['name'] . ' - ' .
                                    Yii::t('course',
                                        '_COURSE_INTRO_WITH_MAX',
                                        array(
                                            '[course_type]' => $courseDetails['course_type'],
                                            '[enrolled]' => $allSeatsCountByCourse,
                                            '[max_subscribe]' => $maxSubscrQuota,
                                            '[course_status]' => LearningCourse::getStatusLabelTrsnaslated($courseDetails['status'])
                                        )
                                    ) . '<br />';

                                // Increase counter as there is one more problematic course with max subscription quota exceeded
                                $quotaExceededCoursesCount++;

								// Remove course from the list of courses to be processed
                                if (($keyCourse = array_search($courseItem, $coursesList)) !== false) {
                                    unset($coursesList[$keyCourse]);
                                }
                            }
                        }
                    }

                    // If there are errors display then prepare them
                    if($quotaExceededCoursesCount) {
                        $errorQuotaMessages = '<span class="error">' . Yii::t('course',
                                'No users were enrolled to [quotaExceededCoursesCount] courses as users count exceeds the max subscriptions quota',
                                array(
                                    '[quotaExceededCoursesCount]' => $quotaExceededCoursesCount
                                )
                            ) . '</span>';
                    }
                }


                // Any Sessions selected ?
                $sessionsArray = array();
				$webinarSessionsArray = array();
		        if (is_array($WizardData['enroll-session-mass']['enrollCourse'])) {
			        foreach ($WizardData['enroll-session-mass']['enrollCourse'] as $idCourse => $sessionSelection) {
						$course = LearningCourse::model()->findByPk($idCourse);
						if ($course)
						{
							if ($course->course_type == LearningCourse::TYPE_CLASSROOM)
							{
								$idSession = (int)$sessionSelection['session'];
								if ($idSession > 0) {
									if (LtCourseSession::checkIfEnrollmentsAreAvailable($idSession, count($usersList)))
										$sessionsArray[] = $idSession;
									else
									{
										$session = LtCourseSession::model()->findByPk($idSession);
										$sessionName = $session ? $session->name : '';
										$this->renderPartial('admin.protected.views.common._dialog2_error', array(
											'type' => 'error',
											'message' => Yii::t('standard','_OPERATION_FAILURE') .'<br>'.$sessionName.' - '. Yii::t('classroom', 'Limit of max enrollable users reached'),
										));
										Yii::app()->end();
									}
								}
							}
							else if ($course->course_type == LearningCourse::TYPE_WEBINAR)
							{
								$idSession = (int)$sessionSelection['session'];
								if ($idSession > 0) {
									if (WebinarSession::checkIfEnrollmentsAreAvailable($idSession, count($usersList)))
										$webinarSessionsArray[] = $idSession;
									else
									{
										$session = WebinarSession::model()->findByPk($idSession);
										$sessionName = $session ? $session->name : '';
										$this->renderPartial('admin.protected.views.common._dialog2_error', array(
											'type' => 'error',
											'message' => Yii::t('standard','_OPERATION_FAILURE') .'<br>'.$sessionName.' - '. Yii::t('classroom', 'Limit of max enrollable users reached'),
										));
										Yii::app()->end();
									}
								}
							}
						}
			        }
		        }

				$input = array(
					'idst' => $usersList,
					'courses' => $coursesList
				);

				$params = array(
					'sessions' => $sessionsArray,
					'webinarSessions' => $webinarSessionsArray,
					'role' => $role,
					'date_begin_validity' => $date_begin_validity,
					'date_expire_validity' => $date_expire_validity,
					'fields' => $fieldsValues
				);

				// If BG Jobs is enabled for this kind of activity, run BG Job
				// Otherwise, use the old way : Progressive UI
				if (Yii::app()->backgroundJob->bgJobEnabled(BackgroundJob::FAMILTY_MASSENROLL)) {
					$this->createBackgroundJob(array(
							'module' => 'course',
							'value' => 'Enrolling {users} in {courses} courses',
							'params' => array(
									'users' => count($usersList),
									'courses' => count($coursesList)
							)
					), 'lms.protected.modules.backgroundjobs.components.handlers.EnrollUsersToManyCourses', $input, $params);

					echo '<a class="auto-close success"></a>';
					Yii::app()->end();
				}

		        // Open new "progressive" dialog, while passing in the
		        // selected data (user IDs and selected role/level ID)
		        ?>
		        <script type="text/javascript">
			        $(function(){
				        $.post('<?=Docebo::createLmsUrl('progress/run')?>', {
					        'type': <?=Progress::JOBTYPE_ENROLL_USERS_TO_MANY_COURSES?>,
					        'role': <?=$role?>,
					        'idst': '<?=is_array($usersList) ? implode(',', $usersList) : $usersList?>',
					        'date_begin_valid': <?='"'.$date_begin_validity.'"'?>,
					        'date_expire_valid': <?='"'.$date_expire_validity.'"'?>,
					        'fields': <?= json_encode($fieldsValues)?>,
					        'courses': '<?=is_array($coursesList) ? implode(',', $coursesList) : $coursesList?>',
					        'sessions': '<?=is_array($sessionsArray) ? implode(',', $sessionsArray) : $sessionsArray?>',
					        'webinarSessions': '<?=is_array($webinarSessionsArray) ? implode(',', $webinarSessionsArray) : $webinarSessionsArray?>',
					        'finalMessage': '<?=$errorQuotaMessages?>'
				        }, function(data){
					        var dialogId = 'enroll-users-to-many-courses-progressive';

					        $('<div/>').dialog2({
						        autoOpen: true, // We will open the dialog later
						        id: dialogId,
						        title: '<?=Yii::t('catalogue', '_COURSE_SUBSCRIPTION')?>'
					        });
					        $('#'+dialogId).html(data);
				        });
			        })
		        </script>
		        <?php

        		// Close dialog2 etc.
        		echo '<a class="auto-close success"></a>';
        		Yii::app()->end();

        	}


        	$anyClassRoom = false;
        	$coursesList 	= UsersSelector::getCoursesListOnly($_REQUEST['Wizard'][$coursesName]);

			$criteria = new CDbCriteria();
			$criteria->with = array('learningCourseSessions', 'learningWebinarSessions');
			$criteria->addCondition('course_type = :classroom_type OR course_type = :webinar_type');
			$criteria->params[':classroom_type'] = LearningCourse::TYPE_CLASSROOM;
			$criteria->params[':webinar_type'] = LearningCourse::TYPE_WEBINAR;
			$criteria->addInCondition('idCourse', $coursesList);
			$dataProviderSessions = new CActiveDataProvider('LearningCourse', array(
					'criteria' => $criteria,
					'pagination' => false
			));
			if ($dataProviderSessions->getTotalItemCount() > 0) {
				$anyClassRoom = true;
			}



        	// -- Decide what to show NEXT
        	$show = false;
        	if ($fromStep == $roleName && $prevButton) {
        		$show = 'courses';
        		if ($anyClassRoom) {
        			$show = 'enroll_session';
        		}
        	}
        	else if ( $fromStep == $usersName && $nextButton ) {
        		$usersList = UsersSelector::getUsersList($_REQUEST, false, true);
        		if (count($usersList) <= 0) {
        			$show = 'users';
        			Yii::app()->user->setFlash('error', Yii::t('standard', '_SELECT_USERS'));
        		}
//        		else if (count($usersList) > $maxUsers) {
//        			$show = 'users';
//        			Yii::app()->user->setFlash('error', Yii::t('standard', 'Please select up to {N} users', array('{N}' => $maxUsers)));
//        		}
        		else {
        			$show = 'courses';
        		}
        	}
        	else if (($fromStep == $coursesName && $prevButton) || empty($fromStep)) {
        		$show = 'users';
        	}
        	else if ($fromStep == $coursesName && $nextButton) {
        		if (count($coursesList) <= 0) {
        			$show = 'courses';
        			Yii::app()->user->setFlash('error', Yii::t('report', '_REPORT_COURSE_SELECTION'));
        		}
        		else if ((count($coursesList) > $maxCourses) && !Yii::app()->backgroundJob->bgJobEnabled(BackgroundJob::FAMILTY_MASSENROLL)) {
        			$show = 'courses';
        			Yii::app()->user->setFlash('error', Yii::t('standard', 'Please select up to {N} courses', array('{N}' => $maxCourses)));
        		}
        		else {
        			$show = 'role';
        			if ($anyClassRoom) {
        				$show = 'enroll_session';
        			}
        		}
        	}
        	else if ($fromStep == $sessionsName && $prevButton) {
        		$show = "courses";
        	}
        	else if ($fromStep == $sessionsName && $nextButton) {
        		$show = "role";
        	}
        	else {
        		$this->renderPartial('admin.protected.views.common._dialog2_error', array(
        				'type' => 'error',
        				'message' => Yii::t('standard','_OPERATION_FAILURE'),
        		));
        		Yii::app()->end();
        	}



        	// -- Show
        	switch ($show) {
        		case 'users':
        			$usersSelectorOpenUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
        					'type' 	=> UsersSelector::TYPE_MASS_ENROLLMENT_USERS,
        					'name'	=> $usersName,
        					'idForm' 	=> $usersName,
        					'noyiixviewjs'	=> !$loadGridHelperScripts,
        			));
        			$this->redirect($usersSelectorOpenUrl, true);
        			Yii::app()->end();
        			break;

        		case 'courses':
        			$usersSelectorOpenUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
        					'type' 		=> UsersSelector::TYPE_MASS_ENROLLMENT_COURSES,
        					'name'		=> $coursesName,
        					'idForm' 	=> $coursesName,
        					'noyiixviewjs'	=> !$loadGridHelperScripts,
        			));
        			$this->redirect($usersSelectorOpenUrl, true);
        			Yii::app()->end();
        			break;

        		case 'role':
        			$usersList 		= UsersSelector::getUsersList($_REQUEST['Wizard'][$usersName], false, true);
        			$coursesList 	= UsersSelector::getCoursesListOnly($_REQUEST['Wizard'][$coursesName]);

        			$fields=LearningEnrollmentField::getFieldsByCourse();
        			
        			$fieldsValues = array();
        			foreach ($fields as $field) {
        				if (!empty($_REQUEST['Wizard']['enroll-role-step-form']['field-' . $field['id']])) {
        					$fieldsValues[$field['id']] = $_REQUEST['Wizard']['enroll-role-step-form']['field-' . $field['id']];
        				}
        			}
        			$errors_mass_enroll = array();
        			$validFields = LearningEnrollmentField::validateEnrollmentArray($fieldsValues);
        			if(!$validFields['success'] && $finish){
        				$errors_mass_enroll = array(
        						'required' => Yii::t('standard', 'Missing required fields or invalid fields data provided')
        				);
        			}
        			 

					// Prepare array of allowed course levels in order to skip those, that are not allowed
			        $restrictLevels = array();
			        $removeLevels = array();
					$skipCoachLevel = false;
					$skipSoftDeadline = false;

			        // Check all courses for their type and coaching enabled
			        foreach ($coursesList as $courseModelId) {
				        // Get current iteration course model
				        $courseModel = LearningCourse::model()->findByPk($courseModelId);

				        // If there is at least one not eLearning course skip coach level
				        if ($courseModel->course_type != LearningCourse::TYPE_ELEARNING) {
					        $skipCoachLevel = true;
				        }

				        // If there is at least one not eLearning course skip soft deadlines
				        // of it is a learning course, but it's not set using of soft deadline from course settings
				        //if ($courseModel->course_type != LearningCourse::TYPE_ELEARNING || ($courseModel->course_type == LearningCourse::TYPE_ELEARNING && $courseModel->soft_deadline != 1)) {
				        if ($courseModel->course_type != LearningCourse::TYPE_ELEARNING) {
					        $skipSoftDeadline = true;
				        }
			        }

			        // Exclude Coach level here if needed
			        if ($skipCoachLevel) {
				        $removeLevels = array(
					        LearningCourseuser::$USER_SUBSCR_LEVEL_COACH
				        );
			        }

        			$finishUrl = Docebo::createAdminUrl('courseManagement/axEnrollUsersToManyCourses');
        			

//					$fields = LearningEnrollmentField::getFieldsByCourse();
        			
        			if($validFields['success'] && $finish){
        				echo '<a class="auto-close success"></a>';
        				Yii::app()->end();
        			}
        			
        			 
        			
        			$html = $this->renderPartial('enroll_role_step_mass', array(
        					'coursesCount'      => count($coursesList),
        					'usersCount'        => count($usersList),
        					'finishUrl'         => $finishUrl,
        					'userLevels'        => $this->_getLevelsArray($restrictLevels, $removeLevels),
        					'skipSoftDeadline'  => $skipSoftDeadline,
        					'fields' => $fields,
        					'errors' => $errors_mass_enroll,
        			), true);
        			echo $html;
        			Yii::app()->end();
        			break;

        		case 'enroll_session':
         			$html = $this->renderPartial('enroll_sessionsV2', array(
         				'dataProvider' => $dataProviderSessions
         			), true, true);
         			echo $html;
         			break;

        		default:
        			$this->renderPartial('admin.protected.views.common._dialog2_error', array(
        					'type' => 'error',
        					'message' => Yii::t('standard','_OPERATION_FAILURE'),
        			));
        			Yii::app()->end();
        			break;
        	}
        }


        /**
         * Action to render "Sessions in a course" grid, as part of the mass **enrollment wizard**
         *
         */
		public function actionRenderSessionsGridForMassEnrollment() {

			$idCourse 	= Yii::app()->request->getParam('idCourse', false);
			$course = LearningCourse::model()->findByPk($idCourse);
			$html = $this->renderPartial('_mass_enroll_session', array(
				'course' => $course,
				'idCourse' => $idCourse
			), true, true);


			echo $html;

			Yii::app()->end();

		}

		public function actionDeleteCertificate($id_course = false, $id_user = false)
		{
			if(!$id_course || !$id_user){
				throw new CException('Invalid user or course');
			}

			$models = LearningCertificateAssign::model()->findAllByAttributes(array(
				'id_course'=>$id_course,
				'id_user'=>$id_user,
			));

			$success = true;

			if(empty($models))
				$success = false;

			foreach($models as $model)
				$success &= $model->delete();

			$this->sendJSON(array('success'=>$success));
		}

		public function actionGetSessions(){
			$courseName = Yii::app()->request->getParam('courseName');
			$course = LearningCourse::model()->findByPk($courseName);
			if(!$course){
				$course = LearningCourse::model()->find('name = :name OR CONCAT(name, " - ", code) = :name', array(':name' => $courseName));
			}
			if (!$course) {
				return;
			}
			$sessions = $course->getSessions(false);
			$sessionsList = array();
			$data = array('courseType' => $course->course_type);
			$fullSessionsIds = array();
			if ($sessions !== false && !empty($sessions)) {
				foreach ($sessions as $key => $session){
					$sessionsList[$session->id_session] = $session->name;
					$usersInSession = $session->countEnrolledUsers(false, true);
					$max = $session->max_enroll;
					if($usersInSession >= $max) {
						$fullSessionsIds[] = $key;
				}
			}
			}
			$data['sessions'] = $sessionsList;
			$data['fullSessionsIds'] = $fullSessionsIds;
			$html = $this->renderPartial('_listSessions', $data, true);
			echo $html;
		}

		public function actionImportSessions()
		{
			// download sample csv file
			if (isset($_GET['download']) && ($_GET['download'] == 'sample')) {
				$sampleFile = 'session-sample.csv';
				$sep = DIRECTORY_SEPARATOR;
				$filePath = Yii::getPathOfAlias('common').$sep.'extensions'.$sep.$sampleFile;
				if (file_exists($filePath)) {
					$fileContent = file_get_contents($filePath);
					Yii::app()->request->sendFile($sampleFile, $fileContent, NULL, false);
				} else
					echo $sampleFile . ' not found';
				Yii::app()->end();
			}

			$step = 1;
			if (isset(Yii::app()->session['import']) && (isset($_POST['SessionImportForm']) || Yii::app()->request->isAjaxRequest))
				$step = Yii::app()->session['import'];
			else
				unset(Yii::app()->session['import']);

			// step 1
			$csvChecks = array();
			if ($step == 1) {
				$sessionModel = 'SessionImportModel';
				unset(Yii::app()->session[$sessionModel]);
				if (isset(Yii::app()->session[$sessionModel])) {
					$model = Yii::app()->session[$sessionModel];
					$model->scenario = 'step_one';
				} else
					$model = new SessionImportForm('step_one');

				if (isset($_POST['SessionImportForm'])) {
					$model->attributes = $_POST['SessionImportForm'];
					$model->file = CUploadedFile::getInstance($model, 'file');
					if ($model->validate()) {

						// Save file in local upload_tmp folder
						$newFileName = Docebo::randomHash() . "." . $model->file->extensionName;
						$model->localFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $newFileName;
						@copy($model->file->tempName, $model->localFile);
						$model->originalFilename = $model->file->name;

						// Save CSV file and update items count
						$model->saveCSVFile();

						// Save form in session (without the rows!!!)
						Yii::app()->session['import'] = $step = 2;
						Yii::app()->session['SessionImportModel'] = $model;

						if($model->getConsistencyChecks()){
							$this->render('import_step2', array(
								'model' => $model,
								'dataProvider' => $model->dataProvider(),
								'gridColumns' => $model->getGridColumnsImportSessions(),
							));
							Yii::app()->end();
						}
					}
				}
			}

			$this->render('import', array(
				'model' => $model,
			));
		}

		/**
		 * Progressive action (shared between all imports using abstract ImportForm class)
		 */
		public function actionaxProgressiveImportSessions() {


			$response = new AjaxResult();
			$defaultLimit = 10;

			$start 			= false;
			$phase			= Yii::app()->request->getParam('phase', 'start');
			$limit 			= (int) Yii::app()->request->getParam('limit', $defaultLimit);
			$offset 		= (int) Yii::app()->request->getParam('offset', 0);

			try {
				// Get the importForm object from the session, saved there earlier by previous steps or runs
				$importForm = Yii::app()->session['SessionImportModel'];
				if(isset($_REQUEST['import_map']))
					$importForm->userImportMap = Yii::app()->request->getParam('import_map', array());

				// At the first call to this progressive importer, add data to the model from the LAST Step and save back to session
				if ($phase=='start' || $start) {

					$importForm->scenario = 'step_two';
					$importForm->clearErrors();
					$importForm->cumulativeFailedLines = array();
					$importForm->failedLines = array();
					$importForm->successCounter = 0;
					Yii::app()->session['SessionImportModel'] = $importForm;

					// Validate data
					$importForm->validate();
					if ($importForm->hasErrors()) {
						$message = '';
						foreach ($importForm->errors as $attrErrors)
							foreach ($attrErrors as $error)
								$message .= $error . "<br>";

						$html = $this->renderPartial('admin.protected.views.common._dialog2_error', array(
							'type' => 'error',
							'message' => $message,
						),true);

						$data = array('stop' => true);
						$response->setStatus(true);
						$response->setHtml($html)->setData($data)->toJSON();
						$importForm->clearErrors();
						Yii::app()->end();
					}
				}

				//
				// GOOD! At this point we should have ALL the data we need for the import
				//

				// A CSV file where we are going to collect errors, one line per user, with the error as first field
				$csvErrorFileName = 'errors_' . basename($importForm->originalFilename);
				$csvErrorFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $csvErrorFileName;

				// Initial state
				$nextOffset = $offset;

				// Act according to CURRENT run phase
				if ($phase =='start') {
					// So far, nothing to do
				} else if ($phase == 'import') { // IMPORT users
					// Set Form data to chunk data
					$nextOffset = $offset + $limit;
					$importForm->failedLines = array();
					$importForm->processItems($offset, $limit);
					$importForm->cumulativeFailedLines = array_merge($importForm->cumulativeFailedLines, $importForm->failedLines);
					Yii::app()->session['SessionImportModel'] = $importForm;
				} else if ($phase == 'finish') { // 'finish' button clicked, redirect to main page
					$this->renderPartial('lms.protected.views.site._js_redirect', array('url' => Docebo::createAdminUrl("courseManagement/index")));
					Yii::app()->end();
				}

				// Calculate the extraction offset for the NEXT (!) request which we show NOW!
				// i.e. in current render we show what is GOING TO BE done in the next cycle
				$totalNumber 		= $importForm->totalItems;
				$number 			= $nextOffset + $limit;
				$number 			= min($number, $totalNumber);

				// Also, in percentage ...
				$percent = 0;
				if ($totalNumber > 0)
					$percent =  $number / $totalNumber * 100;

				// 'Phase' control
				switch ($phase) {
					case 'start':  		// after 'start', we begin importing
						$nextPhase = 'import';
						break;
					case 'import':
						$nextPhase = $phase;
						if ($nextOffset > $totalNumber) {
							$nextPhase = 'finish';
							// See if we have some collected failed lines
							// Write them to the "errors" file
							if (count($importForm->cumulativeFailedLines) > 0) {
								$fh = fopen($csvErrorFilePath, 'w');
								foreach ($importForm->cumulativeFailedLines as $failedLine) {
									fputs($fh, $failedLine . "\n");
								}
								fclose($fh);
							}
						}
						break;
				}

				// We completely stop calling ourselves when we reach the end
				$stop = $nextOffset > $importForm->totalItems;

				$html = $this->renderPartial('_import_progress', array(
					'importForm' 		=> $importForm,
					'totalNumber'		=> $importForm->totalItems,
					'number'			=> $number,
					'percent'			=> $percent,
					'nextPhase'			=> $nextPhase,
					'csvErrorFileName'	=> $csvErrorFileName,
				), true, true);


				// These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stoppping when $stop == true)
				$data = array(
					'offset'			=> $nextOffset,
					'stop'				=> $stop,
					'phase'				=> $nextPhase,
				);

				$response->setStatus(true);
				$response->setHtml($html)->setData($data)->toJSON();
				Yii::app()->end();
			}
			catch (Exception $e) {
				Yii::log($e->getTraceAsString(), CLogger::LEVEL_ERROR);
				$response->setStatus(false)->setMessage($e->getMessage());
				$response->toJSON();
				Yii::app()->end();
			}

			Yii::app()->end();
		}

		/**
		 *  actionManageSeats() - God admin(-s) manage PU seats to a course
		 *
		 */
		public function actionManageSeats() {
			if (!Yii::app()->request->isAjaxRequest) { return; }

			// Add the admin.css in order the Manage Seats to be displayed correctly in the Player -> Side Menu
			Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/admin.css');

			// Check is the assign extra seats button clicked
			$assignButton = Yii::app()->request->getParam('assign_button', false);

			// Check os the finish button clicked
			$finishButton = Yii::app()->request->getParam('confirm_button', false);

			// get the course id
			$idCourse = Yii::app()->request->getParam('id', false);

			$extraSeats = Yii::app()->request->getParam('extra_seats', false);

			if ($idCourse) {
				// Get the course model to be used in the event logging
				$courseModel = LearningCourse::model()->findByPk($idCourse);
			}

			// if clicked assign button
			if ($assignButton) {
				// Get the extra seats count to add
				$addSeats = Yii::app()->request->getParam('add_seats', 0);
				// Get the list of selected PUs
				$choosePus = Yii::app()->request->getParam('choose_pus', false);

				if ($addSeats > 0 && $choosePus && is_array($choosePus) && count($choosePus)) {
					// Go through all selected PUs and assign to them extra seats
					foreach ($choosePus as $currentPuId) {
						// Get current (CoreUser)PuCourse model
						$currentPuModel = CoreUserPuCourse::model()->findByPk(
							array(
								'puser_id' => $currentPuId,
								'course_id' => $idCourse
							)
						);

						// Set the extra seats to the corresponding seats columns
						$currentPuModel->extra_seats = $currentPuModel->extra_seats + $addSeats;
						$currentPuModel->total_seats = $currentPuModel->total_seats + $addSeats;
						$currentPuModel->available_seats = $currentPuModel->available_seats + $addSeats;

						// Try to save and apply added extra seats
						if($currentPuModel->save()) {
							// Raise event
							Yii::app()->event->raise('AdminAddedExtraSeatsToPu', new DEvent($this, array(
								'course'            => $courseModel,
								'user'              => $currentPuModel,
								'puUsername'        => CoreUser::model()->getUserIdByIdst($currentPuModel->puser_id),
								'addedExtraSeats'   => $addSeats,
								'adminId'           => Yii::app()->user->getIdst(),
								'adminUserName'     => Yii::app()->user->getUsername()
							)));
						} else {
							// Saving failed
							throw new CException(Yii::t('standard','_OPERATION_FAILURE'));
						}
					}
				}

			} elseif ($extraSeats && is_array($extraSeats)) { // if clicked apply extra seats icon
				foreach($extraSeats as $currentPuId => $newExtraSeatsValue) {
					$newExtraSeatsValue = intval($newExtraSeatsValue);

					// If it is set to zero set it to zero !!!
					$newExtraSeatsValue = ($newExtraSeatsValue < 0) ? 0 : $newExtraSeatsValue;

					// Get current (CoreUser)PuCourse model
					$currentPuModel = CoreUserPuCourse::model()->findByPk(
						array(
							'puser_id' => $currentPuId,
							'course_id' => $idCourse
						)
					);

					// If we are removing some extra seats !!!
					if ($currentPuModel->extra_seats > $newExtraSeatsValue) {
						// How many seats do we need to remove
						$removeExtraSeats = $currentPuModel->extra_seats - $newExtraSeatsValue;

						if ($removeExtraSeats > $currentPuModel->available_seats) {
							// Show error cannot remove extra seats, that are already assigned to users!!!
							$this->renderPartial('admin.protected.views.common/_dialog2_error', array(
								'message' => Yii::t('course','You cannot remove extra seats, that are already assigned to users'),
								'type' => 'error',
							));
							Yii::app()->end();
						} else {
							$currentPuModel->extra_seats = $newExtraSeatsValue;
							$currentPuModel->available_seats = $currentPuModel->available_seats - $removeExtraSeats;
							$currentPuModel->total_seats = $currentPuModel->total_seats - $removeExtraSeats;

							// Try to save and apply removed extra seats
							if($currentPuModel->save()) {
								// Raise event
								Yii::app()->event->raise('AdminRemovedExtraSeatsToPu', new DEvent($this, array(
									'course'        => $courseModel,
									'user'          => $currentPuModel,
									'puUsername'    => CoreUser::model()->getUserIdByIdst($currentPuModel->puser_id),
									'remSeatsCount' => $removeExtraSeats,
									'adminId'       => Yii::app()->user->getIdst(),
									'adminUserName' => Yii::app()->user->getUsername()
								)));
							} else { // This means, that we are adding new extra seats
								// Saving failed
								throw new CException(Yii::t('standard','_OPERATION_FAILURE'));
							}
						}

					} else {
						// proceed by calculating how many seats are needed
						$addExtraSeats = $newExtraSeatsValue - $currentPuModel->extra_seats;

						// If there are seats to add proceed
						if ($addExtraSeats > 0) {
							// Set the extra seats to the corresponding seats columns
							$currentPuModel->extra_seats = $currentPuModel->extra_seats + $addExtraSeats;
							$currentPuModel->total_seats = $currentPuModel->total_seats + $addExtraSeats;
							$currentPuModel->available_seats = $currentPuModel->available_seats + $addExtraSeats;

							// Try to save and apply added extra seats
							if($currentPuModel->save()) {
								// Raise event
								Yii::app()->event->raise('AdminAddedExtraSeatsToPu', new DEvent($this, array(
									'course'            => $courseModel,
									'user'              => $currentPuModel,
									'puUsername'        => CoreUser::model()->getUserIdByIdst($currentPuModel->puser_id),
									'addedExtraSeats'   => $addExtraSeats,
									'adminId'           => Yii::app()->user->getIdst(),
									'adminUserName'     => Yii::app()->user->getUsername()
								)));
							} else {
								// Saving failed
								throw new CException(Yii::t('standard','_OPERATION_FAILURE'));
							}
						}
					}

				}
			}

			if ($finishButton) { // The Confirm button is pressed

				// complete the dialog
				echo '<a class="auto-close success"></a>';
				Yii::app()->end();
			}

			$course = LearningCourse::model()->findByPk($idCourse);
			if (!$course) {
				throw new CHttpException(401,'Course not found');
			} /*- Get the course */

			$coreUserModel = new CoreUser();

			$this->renderPartial('_manage_seats', array(
				'course'                => $course,
				'coreUserModel' => $coreUserModel,
			), false, true);
			Yii::app()->end();
		}

		/**
		 *  actionCoursePuAutocomplete() returns json with PUs to be shown in Fcbk autocomplete
		 */
		public function actionCoursePuAutocomplete() {
			// Get search tag
			$query = Yii::app()->request->getParam('tag', false);
			$idCourse = Yii::app()->request->getParam('idCourse', false);

			// Get the data from the db
			$pusers = Yii::app()->db->createCommand()
					->select('cu.*')
					->from('core_user_pu_course cupuc')
					->join('core_user cu', 'cu.idst=cupuc.puser_id')
					->where('(cu.userid LIKE :match OR cu.firstname LIKE :match2  OR cu.lastname LIKE :match3)
					AND cupuc.course_id=:idCourse',
					array(
						':match' => "/%$query%",
						':match2' => "%$query%",
						':match3' => "%$query%",
						'idCourse' => (int) $idCourse
					))
					->queryAll();

			// Prepare arrays
			$pusersList = array_values(CHtml::listData($pusers, 'idst', 'idst'));
			$puList = array();

			// Get course PUs, that are seat managers too
			$courseSeatManagers = CoreUserPU::model()->getCoursePUAndSeatManagers($idCourse);

			// If there are any records found prepare data to be displayed in the fcbkselector
			if (!empty($pusersList) && count($pusersList)) {
				foreach ($pusersList as $key => $value) {
					$currentUserModel = CoreUser::model()->findByPk($value);
					if($currentUserModel && in_array($value, $courseSeatManagers)) {
						$puList[] = array('key' => $value, 'value' => Yii::app()->user->getRelativeUsername($currentUserModel->userid) . ": " . $currentUserModel->getFullName());
					}
				}
			}

			// Ensure, that we are returning an array !!!
			$pusersList = (!empty($pusersList) && is_array($pusersList)) ? $pusersList : array();

			// send the Power Users List
			$this->sendJSON($puList);
		}

		public function actionAttendance(){

			// get main settings model
			$settings = AttendanceSheetMainField::model()->findByPk(1);
			if(isset($_POST['submitSettings']) && !empty($_POST['submitSettings'])) {
				$settings->fieldName1 = $_POST['fieldName1'];
				$settings->fieldName2 = $_POST['fieldName2'];
				$settings->fieldName3 = $_POST['fieldName3'];
				$settings->fieldName4 = $_POST['fieldName4'];
				$settings->fieldName5 = $_POST['fieldName5'];
				if ($settings->fieldName1 == $settings->fieldName2
						&& $settings->fieldName2 == $settings->fieldName3
						&& $settings->fieldName3 == $settings->fieldName4
						&& $settings->fieldName4 == $settings->fieldName5
						&& $settings->fieldName1 == 0){
					Yii::app()->user->setFlash('error', Yii::t('standard', 'You must select at least one item. Drag & drop to reorder items'));
				}else {
					// there is at least one active field
					$settings->order = $_POST['order'];
					$settings->prevent_session_overlap = $_POST['prevent_session_overlap'];
					if ($settings->save())
						Yii::app()->user->setFlash('success', Yii::t('standard', '_OPERATION_SUCCESSFUL'));
					else
						Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
				}
			}
			$this->widget('common.widgets.attendanceSheet.AttendanceSheetWidget', array('settings' => $settings, 'renderPartial' => false));
		}

		protected function getCoursesGridColumns($showLPColumn, $tooltipTitleLP, $disallowPUEnrollCoursesWithLPs, $tooltip, $tooltipTitle, $courseModel) {

		    $courseLogoVariant = CoreAsset::VARIANT_MICRO;

		    $columns = array(
		        'code',
		        'image' => array(
		            'header' => Yii::t('standard', 'Thumbnail'),
		            'type' => 'raw',
		            'value' => '$data->renderCourseLogo("'.$courseLogoVariant.'");',
		            'htmlOptions' => array('class' => 'center-aligned'),
		            'headerHtmlOptions' => array('class' => 'center-aligned'),
		        ),
		        'lang_code' => array(
		            'header' => Yii::t('standard', '_LANGUAGE'),
		            'value' => 'CHtml::tag(\'span\', array(\'class\' => \'lang-sprite lang_\'. strtolower($data->lang_code)))',
		            'type' => 'raw',
		            'htmlOptions' => array('class' => 'center-aligned'),
		            'headerHtmlOptions' => array('class' => 'center-aligned'),
		        ),
		        'name' => array(
					'name' => 'name',
					'htmlOptions' => array('class' => 'wrapped'),
					'type' => 'raw'
				),
		    );

		    $columns[] = array(
		        'type' => 'raw',
		        'header' => Yii::t('standard', '_TYPE'),
		        'value' => '$data->renderType();',
		        'htmlOptions' => array('class' => 'center-aligned'),
		        'headerHtmlOptions' => array('class' => 'center-aligned'),
		    );

		    if (CoreUserPU::isPUAndSeatManager()) {
		        $columns[] = array(
		            'type' => 'raw',
		            'header' => Yii::t('standard', 'Seats'),
		            'value' => function($data, $index){
		                $idUser = Yii::app()->user->getIdst();
		                $idCourse = $data->idCourse;

		                if(!LearningCourse::isCourseSelling($idCourse)) return; // Seats free

						$seatsInfoArr = CoreUserPuCourse::getTotalAndAvailableSeats($idCourse, $idUser);
						$link = '';
						if (Yii::app()->legacyWrapper->hydraFrontendEnabled()) {
							/* @var $courseModel LearningCourse */
							// we need to redirect the power user to the new course layout, from where he can access new shopping cart!
							$bridge = new HydraFrontendBridge();
							$link = $bridge->getHydraCourseViewUrl(LearningCourse::model()->findByPk($idCourse));

							echo $seatsInfoArr['available'] . '/' . $seatsInfoArr['total'] . ' <a onClick="top.window.location.href=\''.$link.'\'" href="' . $link . '" title="' . Yii::t('standard', 'Seats') . '"><span class="maxAvailable"></span></a>';
						} else {
							$link = Docebo::createLmsUrl('course/buyMoreSeats', array('id' => $idCourse));
							echo $seatsInfoArr['available'] . '/' . $seatsInfoArr['total'] . ' <a href="' . $link . '" class="open-dialog" data-dialog-class="course-buy-seats" title="' . Yii::t('standard',
									'Seats') . '" data-dialog-title="'
								. Yii::t('standard', 'Seats') . '"><span class="maxAvailable"></span></a>';
						}

		            },
		            'headerHtmlOptions' => array('class' => 'center-aligned'),
		            'htmlOptions' => array('class' => 'center-aligned')
		        );
		    }

		    $columns[] = array(
		        'type' => 'raw',
		        'header' => Yii::t('standard', 'Marketplace'),
		        'value' => '$data->renderMaxAvailable();',
		        'headerHtmlOptions' => array('class' => 'center-aligned'),
		        'cssClassExpression' => '$data->renderCssClassMain()'
		    );


		    $columns[] = array(
		        'type' => 'raw',
		        'header' => Yii::t('standard', '_WAITING'),
		        'value' => '$data->renderWaiting();',
		        'headerHtmlOptions' => array('class' => 'center-aligned'),
		        'cssClassExpression' => '$data->renderCssClassDel()'
		    );

		    /* LPs column start */
		    if ($showLPColumn) {
		        $columns[] = array(
		            'type' => 'html',
		            'header' => Yii::t('standard', 'LP'),
		            'value' => '$data->renderLPs(array(\'tooltipTitleLP\' => "' . $tooltipTitleLP . '"));',
		            'headerHtmlOptions' => array('class' => ''), //'center-aligned' in the other columns
		            'cssClassExpression' => '$data->renderCssClassLP()'
		        );
		    }
		    /* LPs column end */

		    /* ENROLLED column start */
		    $columns[] = array(
		        'type' => 'html',
		        'header' => Yii::t('standard', 'Enrolled'),
		        'value' => '$data->renderEnrolled(array(\'disallowPUEnrollCoursesWithLPs\' => '. $disallowPUEnrollCoursesWithLPs . ', \'tooltip\' => '. $tooltip . ', \'tooltipTitle\' => "' . $tooltipTitle . '"));',
		        'headerHtmlOptions' => array('class' => 'center-aligned'),
		        'cssClassExpression' => '$data->renderCssClassDel()'
		    );
		    /* ENROLLED column end */

		    $event = new DEvent($this, array('controller' => $this, 'courseModel' => $courseModel));
		    Yii::app()->event->raise('OnCourseListingRender', $event);

		    if(!empty($event->return_value['columns'])){
		        $columns = array_merge($columns, $event->return_value['columns']);
		    }


		    $columns['actions'] = array(
		        'type' => 'raw',
		        'value' => '$data->renderCourseActions();',
		    );


		    return $columns;

		}

		private function getShortCodesData($messageContent, $shortCodes, $courseId, $userId, $sessionId = null){
			$courseModel = LearningCourse::model()->findByPk($courseId);

			if ($courseModel) {
				if ($courseModel->course_type == LearningCourse::TYPE_CLASSROOM) {
					$sessionModel = LtCourseSession::model()->findByPk($sessionId);
				} elseif ($courseModel->course_type == LearningCourse::TYPE_WEBINAR) {
					$sessionModel = WebinarSession::model()->findByPk($sessionId);
				}

				$user = CoreUser::model()->findByPk($userId);
				if (!empty($user)) {
					foreach ($shortCodes as $shortCode) {
						switch ($shortCode) {
							case '[username]':
								$value = $user->getClearUserId();
								break;
							case '[first_name]':
								$value = $user->firstname;
								break;
							case '[last_name]':
								$value = $user->lastname;
								break;
							case '[email]':
								$value = $user->email;
								break;
							case '[course_code]':
								$value = $courseModel->code;
								break;
							case '[course_name]':
								$value = $courseModel->name;
								break;
							case '[course_description]':
								$value = $courseModel->description;
								break;
							case '[course_link]':
								$text = Docebo::createAbsoluteLmsUrl('//player', array('course_id' => $courseModel->idCourse));
								$value = CHtml::link($text, $text);
								break;

							case '[webinar_tool]':
								$value = $sessionModel->tool;
								break;
							case '[session_location]':
								$value = $sessionModel->renderLocations(true);
								break;

							case '[session_datetimes]':
								$value = $sessionModel->compileShortcodeDates();
								break;
							case '[session_name]':
								$value = $sessionModel->name;
								break;
							case '[session_description]':
								if ($courseModel->course_type == LearningCourse::TYPE_CLASSROOM) {
									$value = $sessionModel->other_info;
								} elseif ($courseModel->course_type == LearningCourse::TYPE_WEBINAR) {
									$value = $sessionModel->description;
								}
								break;
							case '[session_link]':
								if ($courseModel->course_type == LearningCourse::TYPE_CLASSROOM) {
									$text = Docebo::createAbsoluteLmsUrl('player/training/session', array('course_id' => $courseId, 'session_id' => $sessionId));
									$value = CHtml::link($text, $text);
								} elseif ($courseModel->course_type == LearningCourse::TYPE_WEBINAR) {
									$text = Docebo::createAbsoluteLmsUrl('player/training/webinarSession', array('course_id' => $courseId, 'session_id' => $sessionId));
									$value = CHtml::link($text, $text);
								}
								break;
						}
						$messageContent = str_replace($shortCode, $value, $messageContent);
					}
				}
			}
			return $messageContent;
		}
		

		/**
		 * Return a JSON to "quartz"-type modal dialog, rendering an IFRAME enrollment field inside.
		 * 
		 * This action deals with ONE enrollment field only! (selected from actions dropdown)
		 * 
		 * Also, accept & process dialog's form data submition
		 *  
		 */
		public function actionIframeEnrollmentFieldMassAction() {
		    
		    $fieldId          = Yii::app()->request->getParam("fieldId");
		    $courseId         = Yii::app()->request->getParam("courseId");
		    $sessionId        = Yii::app()->request->getParam("sessionId");
		    $userIds          = Yii::app()->request->getParam("ids");
		    $dataSubmitted    = Yii::app()->request->getParam("data_submitted", false);
		    
		    $iframeField      = LearningEnrollmentFieldIframe::model()->findByPk($fieldId);
		    $userIds          = explode(",", $userIds);
		    
		    // If this is a data form submition, handle it
		    if ($dataSubmitted) {
		        
		        $fieldData = Yii::app()->request->getParam("field-" . $fieldId, false);
		        
		        if ($fieldData !== false) {
		            $dataToProcess = array();
		            $fieldDataArray = json_decode($fieldData, true);
		            $data = array(
		                'fieldData'       => json_decode($fieldData, true),
		                'fieldId'         => $fieldId,
		                'courseId'        => $courseId,
		                'sessionId'       => $sessionId,
		                'userIds'         => $userIds,
		            );
		            $iframeField->processSingleFieldManyEnrollments($data);
		        }
		        
		        $this->sendJSON(array(
		            'status'  => "saved",
		        ));
		        Yii::app()->end();
		    }

		    
		    $enrollments = array();
		    foreach ($userIds as $userId) {
		        $enrollmentModel  = LearningCourseuser::model()->findByAttributes(array("idUser" => $userId, "idCourse" => $courseId));
		        $enrollments[]    = $iframeField->prepareSingleEnrollmentInfo($enrollmentModel);
		    }
		    
		    $html = $this->renderPartial("_iframe_enrollment_field_mass_edit", array(
		        'iframeField' => $iframeField,
		        'courseId'    => $courseId,
		        'userIds'     => $userIds,
		        'sessionId'   => $sessionId,
		        'enrollments' => $enrollments,
		    ), true);
		    $status = false;
		    
		    $this->sendJSON(array(
		        'status' => $status, 
		        'html' => $html, 
		    ));
		    
		    Yii::app()->end();
		    
		}
		
	}