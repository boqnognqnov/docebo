<?php

class OrgManagementController extends AdminController {

	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}

	public function accessRules() {
		$res   = array();


		//!!! THIS IS BAD! Allowing all logged in users ...
		// But we need to decide what to do with PowerUser permissions.
		// At the moment PU does NOT have /framework/admin/usermanagement/mod_org role,
		// but is required to be able to assign users, hence.. I allow him to run this action for now...
		$res[] = array(
			'allow',
			//'roles' => array('/framework/admin/usermanagement/mod_org'),
			'actions' => array('assignUsersV2'),
			'users' => array('@'),
		);


		$res[] = array(
			'allow',
			'roles' => array('/framework/admin/usermanagement/mod_org'),
			'actions' => array('create', 'axCheckBranchCode'),
		);
		$res[] = array(
			'allow',
			'roles' => array('/framework/admin/usermanagement/mod_org'),
			'actions' => array('edit', 'move', 'getNodeParents', 'axMoveNode', 'axCheckBranchCode'),
		);
		$res[] = array(
			'allow',
			'roles' => array('/framework/admin/usermanagement/mod_org'),
			'actions' => array('delete', 'axCheckBranchCode'),
		);
		if (Settings::get('use_node_fields_visibility', 'off') == 'on') {
			$res[] = array(
				'allow',
				'roles' => array('/framework/admin/usermanagement/mod_org'),
				'actions' => array('editFields'),
			);
		}
		$res[] = array(
			'deny',
			// deny all
			'users' => array('*'),
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);
		return $res;
	}



	public function beforeAction($action) {

		switch ($action->id) {
			case 'create':
				// NOTE: real operation starts when confirm button has been pressed
				if (empty($_POST['CoreOrgChart'])) {
					//check if the tree has changed while the client page was opened
					//NOTE: this is the same check of below. In this case we need it in the fist dialog opening too.
					$treeGenerationTime = Yii::app()->request->getParam('treeGenerationTime', false);
					if ($treeGenerationTime != false) {
						if (CoreRunningOperation::isBeforeLastOperationTime('org_chart_tree_change', $treeGenerationTime)) {
							CoreRunningOperation::endOperation('org_chart_tree_change');
							CoreOrgChartTree::model()->flushCache();
							echo CJSON::encode(array('success' => false, 'update_tree' => true));
							Yii::app()->end();
						}
					}
				}
				break;
			case 'edit':
				// NOTE: real operation starts when confirm button has been pressed
				if (empty($_POST['CoreOrgChart'])) {
					break;
				}
				if (!empty($_POST['CoreOrgChart']) && empty($_POST['CoreOrgChart']['translation'])) {
					break;
				}
			case 'move':
				// NOTE: real operation starts when confirm button has been pressed
				if (empty($_POST['CoreOrgChartTree'])) {
					break;
				}
			case 'delete':
				// NOTE: real operation starts when confirm button has been pressed
				if (empty($_POST['CoreOrgChartTree'])) {
					break;
				}
				if (!empty($_POST['CoreOrgChartTree']['confirm']) && !$_POST['CoreOrgChartTree']['confirm']) {
					break;
				}
			case 'axMoveNode':
				//check if the tree has changed while the client page was opened
				$treeGenerationTime = Yii::app()->request->getParam('treeGenerationTime', false);
				if ($treeGenerationTime != false) {
					if (CoreRunningOperation::isBeforeLastOperationTime('org_chart_tree_change', $treeGenerationTime)) {
						CoreRunningOperation::endOperation('org_chart_tree_change');
						CoreOrgChartTree::model()->flushCache();
						echo CJSON::encode(array('success' => false, 'update_tree' => true));
						Yii::app()->end();
					}
				}

				//if tree is ok then we can start our operation and continue with regular action
				if (!CoreRunningOperation::startOperation('org_chart_tree_change')) {
					// We cannot start operating on the tree structure because someone else is already doing it and we must not
					// interfere to avoid conflicts/data corruption
					$response = new AjaxResult();
					$response->setStatus(false)->setData(array('update_tree' => true))->toJSON();
					Yii::app()->end();
				}
				break;
		}

		return parent::beforeAction($action);
	}



	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
		echo 'index';
		exit;
	}





	public function actionCreate()
	{
		$coreOrgChart = new CoreOrgChart();
		$coreOrgChart->lang_code = CoreUser::getDefaultLangCode();
		$coreOrgChartTree     = new CoreOrgChartTree();
		$activeLanguagesList = CoreLangLanguage::getActiveLanguages();
		$lang_code = Settings::get('default_language', 'english');
		$activeLanguagesList[$lang_code] = $activeLanguagesList[$lang_code] . " (" . Yii::t('standard', '_DEFAULT_LANGUAGE') . ")";
		if (!empty($_POST['CoreOrgChart'])) {
			if (!empty($_POST['CoreOrgChart']['translation'])) {
				$allTranslationEmpty = true;
				foreach ($_POST['CoreOrgChart']['translation'] as $translation) {
					if (!empty($translation)) {
						$allTranslationEmpty = false;
					}
				}

				$coreOrgChartTree = new CoreOrgChartTree();

				$coreOrgChartTree->code = (isset($_POST['CoreOrgChartTree']['code']) && $_POST['CoreOrgChartTree']['code'] != '' ? $_POST['CoreOrgChartTree']['code'] : null);
				$coreOrgChartTree->validate();

				if ($allTranslationEmpty) {
					$coreOrgChart->addError('translation', Yii::t('organization_chart', 'Node name can\'t be empty'));
				}
				elseif(isset($_POST['CoreOrgChart']['translation'][CoreLangLanguage::getDefaultLanguage()]) && !$_POST['CoreOrgChart']['translation'][CoreLangLanguage::getDefaultLanguage()])
				{
					$coreOrgChart->addError('lang_code', Yii::t('user_management', '_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED'));
				}
				elseif($coreOrgChartTree->hasErrors())
				{
					$coreOrgChartTree->clearErrors();
					$coreOrgChartTree->addError('code', Yii::t('organization_chart', 'You have allready used this organization code'));
				}
				elseif ($coreOrgChart->validate())
				{
					if (isset(Yii::app()->session['currentNodeId'])) {
						$id   = Yii::app()->session['currentNodeId'];
						$root = CoreOrgChartTree::model()->findByPk($id);
					} else {
						$root = CoreOrgChartTree::model()->roots()->find();
					}

					//$coreOrgChartTree->idOrg = $currentId;
					if (empty($root)) {
						$coreOrgChartTree->saveNode();
					} else {
					    // Set the parent ID of newly created branch to its.. parent ID :-)
					    // We started tracking nodes' parents again to have a mean to recover in case we mess Nested left/right data
					    // Old Trees (parent IDs) must have been already fixed/set by a migration (m151114_140000_ALTER_core_org_chart_tree_FIX_PARENTS)  
					    $coreOrgChartTree->idParent = $root->idOrg;
					    
						$coreOrgChartTree->appendTo($root);
					}

					$parentNode = $coreOrgChartTree->parent()->find();
					$parentId = $parentNode->idOrg;

					//due to foreign key constraints, translations must be added AFTER tree node creation
					$currentId = $coreOrgChartTree->getPrimaryKey();

					if (!empty($currentId) && !empty($activeLanguagesList)) {
						foreach ($activeLanguagesList as $key => $lang) {
							$translation              = new CoreOrgChart();
							$translation->id_dir      = $currentId;
							$translation->lang_code   = $key;
							$translation->translation = $_POST['CoreOrgChart']['translation'][$key];
							$translation->save();
						}
					}

					//inheritance of additional fields:
					// - if coming from root node then set all fields ON
					// - otherwise set only the parent folder fields without considering the root
					$rootNode = CoreOrgChartTree::getOrgRootNode();
					$additionalFields = array();
					if ($parentId != $rootNode->getPrimaryKey()) {
						//find all additional fields inherited from ancestor nodes
						//NOTE: parent node alone should already own all needed fields, but read from all ancestors the same to prevent possible errors
						$cmd = Yii::app()->db->createCommand("SELECT gf.id_field "
							." FROM ".CoreOrgChartTree::model()->tableName()." oct "
							." JOIN ".CoreGroupFields::model()->tableName()." gf ON (gf.idst = oct.idst_oc OR gf.idst = oct.idst_ocd) "
							." WHERE oct.iLeft <= :iLeft AND oct.iRight >= :iRight AND oct.idOrg <> :root");
						$reader = $cmd->query(array(
							':iLeft' => $parentNode->iLeft,
							':iRight' => $parentNode->iRight,
							':root' => $rootNode->getPrimaryKey()
						));
						while ($row = $reader->read()) {
							$additionalFields[] = $row['id_field'];
						}
						$additionalFields = array_unique($additionalFields);
					}
					else {
						//plain assign all additional fields
						$cmd = Yii::app()->db->createCommand("SELECT DISTINCT(id_field) FROM ".CoreUserField::model()->tableName());
						$reader = $cmd->query();
						while ($row = $reader->read()) { $additionalFields[] = $row['id_field']; }
					}

					$fieldCurrent = $coreOrgChartTree::model()->findByPk($currentId);

					$transaction = Yii::app()->db->beginTransaction();

						try {
							//insert new values
							foreach ($additionalFields as $additionalField) {
									$assignment = new CoreGroupFields();
									$assignment->idst = $fieldCurrent->idst_ocd;
									$assignment->id_field = $additionalField;
									$assignment->mandatory = 'true';
									$rs = $assignment->save();
									if (!$rs) { throw new CException('Error while saving new field node assignments'); }
							}

							$transaction->commit();

						} catch (CException $e) {
							//something went wrong
							$transaction->rollback();
						}

					/////// end of setting additional fields

					CoreRunningOperation::endOperation('org_chart_tree_change');

					$this->sendJSON(array(
						'redirect' => Yii::app()->createAbsoluteUrl('userManagement/index', array('id' => $parentId)),
					));
					Yii::app()->end();

				}
			}
		}

		if (Yii::app()->request->isAjaxRequest) {
			$content = $this->renderPartial('create', array(
				'model' => $coreOrgChart,
				'model2' => $coreOrgChartTree,
				'activeLanguagesList' => $activeLanguagesList
			), true);
			echo CJSON::encode(array(
				'html' => $content,
			));
			exit;
		} else {
			$this->renderPartial('create', array(
				'model' => $coreOrgChart,
				'model2' => $coreOrgChartTree,
				'activeLanguagesList' => $activeLanguagesList
			));
		}
	}




	public function actionEdit($id) {
		$coreOrgChart        = new CoreOrgChart();
		$coreOrgChartTree    = CoreOrgChartTree::model()->findByPk($id);
		$activeLanguagesList = CoreLangLanguage::getActiveLanguages();
		$lang_code = Settings::get('default_language', 'english');
		$activeLanguagesList[$lang_code] = $activeLanguagesList[$lang_code] . " (" . Yii::t('standard', '_DEFAULT_LANGUAGE') . ")";

		if (!empty($_POST['CoreOrgChart']))
		{
			if (!empty($_POST['CoreOrgChart']['translation']))
			{
				$allTranslationEmpty = true;
				foreach ($_POST['CoreOrgChart']['translation'] as $translation)
				{
					if (!empty($translation))
						$allTranslationEmpty = false;
				}

				$coreOrgChartTree->code = (isset($_POST['CoreOrgChartTree']['code']) && $_POST['CoreOrgChartTree']['code'] != '' ? $_POST['CoreOrgChartTree']['code'] : null);
				$coreOrgChartTree->validate();

				if ($allTranslationEmpty) {
					$coreOrgChart->addError('translation', Yii::t('organization_chart', 'Node name can\'t be empty'));
				}
				elseif(isset($_POST['CoreOrgChart']['translation'][CoreLangLanguage::getDefaultLanguage()]) && !$_POST['CoreOrgChart']['translation'][CoreLangLanguage::getDefaultLanguage()])
				{
					$coreOrgChart->addError('lang_code', Yii::t('user_management', '_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED'));
				}
				elseif($coreOrgChartTree->hasErrors())
				{
					$coreOrgChartTree->clearErrors();
					$coreOrgChartTree->addError('code', Yii::t('organization_chart', 'You have allready used this organization code'));
				}
				elseif ($coreOrgChart->validate())
				{
					if(!$coreOrgChartTree->hasErrors())
						$coreOrgChartTree->saveNode();

					if (!empty($activeLanguagesList))
					{
						$translations = CoreOrgChart::model()->findAllByAttributes(array(
							'id_dir' => $id,
						), array('condition' => 'lang_code IN (\'' . implode('\',\'', array_keys($activeLanguagesList)) . '\')'));
						$translationsByKey = array();
						if (!empty($translations))
						{
							foreach ($translations as $value)
							{
								$translationsByKey[$value->lang_code] = $value;
							}
						}
						foreach ($activeLanguagesList as $key => $lang)
						{
							if(empty($translationsByKey[$key])) {
								$translationsByKey[$key]              = new CoreOrgChart();
								$translationsByKey[$key]->id_dir      = $id;
								$translationsByKey[$key]->lang_code   = $key;
							}
							$translationsByKey[$key]->translation = $_POST['CoreOrgChart']['translation'][$key];
							$translationsByKey[$key]->save();
						}
					}

					CoreRunningOperation::endOperation('org_chart_tree_change');
					$newTreeGenerationTime = CoreRunningOperation::getCurrentTimeMicroseconds();

					// NOTE: it shouldn't be really necessary to send the new generation time in the EDIT operation, since the
					// tree is reloaded after that the editing popup is closed. Anyway it won't hurt.
					$this->sendJSON(array('new_tree_generation_time' => $newTreeGenerationTime));
					Yii::app()->end();
				}
			}
		}

		if (!empty($_POST['CoreOrgChart']['translation'])) {
			$translationsList = $_POST['CoreOrgChart']['translation'];
		} else {
			$translations = CoreOrgChart::model()->findAllByAttributes(array('id_dir' => $id));
			$translationsList = CHtml::listData($translations, 'lang_code', 'translation');
		}

		if (!empty($activeLanguagesList)) {
			foreach ($activeLanguagesList as $key => $value) {
				if (!empty($translationsList[$key])) {
					$activeLanguagesList[$key] .= ' *';
				}
			}
		}

		if (Yii::app()->request->isAjaxRequest) {
			$content = $this->renderPartial('edit', array(
				'model' => $coreOrgChart,
				'model2' => $coreOrgChartTree,
				'translationsList' => $translationsList,
				'activeLanguagesList' => $activeLanguagesList
			), true);
			echo CJSON::encode(array(
				'html' => $content,
			));
			exit;
		} else {
			$this->renderPartial('edit', array(
				'model' => $coreOrgChart,
				'model2' => $coreOrgChartTree,
				'translationsList' => $translationsList,
				'activeLanguagesList' => $activeLanguagesList
			));
		}
	}

	public function actionMove($id) {
		$coreOrgChartNode = CoreOrgChartTree::model()->findByAttributes(array('idOrg' => $id));

		// Flush Orgchart cache
		if ($coreOrgChartNode) {
			$coreOrgChartNode->flushCache();
		}

		$default_with = array(
			'coreOrgChart' => array(
				'on' => 'coreOrgChart.lang_code = :lang',
				'params' => array('lang' => Settings::get('defaultLanguage', 'english'))
			)
		);
		$defaultOrgChartTree = CoreOrgChartTree::model()->with($default_with)->findAll(array('order' => 'iLeft'));
		$defaultCourseTreeTranslation = array();
		foreach ($defaultOrgChartTree as $node) {
			$defaultOrgChartTranslation[$node->idOrg] = $node->coreOrgChart->translation;
		}

		$lang = CoreLangLanguage::model()->findByAttributes(array('lang_browsercode' => Yii::app()->getLanguage()));
		$with = array(
			'coreOrgChart' => array(
				'on' => 'coreOrgChart.lang_code = :lang',
				'params' => array('lang' => $lang->lang_code)
			)
		);
		$fullOrgChartTree     = CoreOrgChartTree::model()->with($with)->findAll(array('order' => 'iLeft'));
		$fullOrgChartTreeList = CHtml::listData($fullOrgChartTree, 'idOrg', 'coreOrgChart.translation');
		if (!empty($_POST['CoreOrgChartTree'])) {
			$idOrg = 0;
			if (!empty($_POST['CoreOrgChartTree']['idOrg'])) {
				foreach ($_POST['CoreOrgChartTree']['idOrg'] as $postIdOrg) {
					if ($postIdOrg > 0) {
						$idOrg = $postIdOrg;
					}
				}
			}
			$newParent = CoreOrgChartTree::model()->findByAttributes(array('idOrg' => $idOrg));
			$coreOrgChartNode->moveAsFirst($newParent);

			Yii::app()->session['currentNodeId'] = $idOrg;

			CoreRunningOperation::endOperation('org_chart_tree_change');

			$this->sendJSON(array(
				'redirect' => Yii::app()->createAbsoluteUrl('userManagement/index'),
			));
		}

		if (Yii::app()->request->isAjaxRequest) {
			$currentNode = CoreOrgChartTree::model()->findByPk($id);
			$nodeParents = $currentNode->ancestors()->findAll();
			$allChildrenNodesIds = CHtml::listData($currentNode->descendants()->findAll(), 'idOrg', 'idOrg');
			$allChildrenNodesIds = array_merge(array($id => $id), $allChildrenNodesIds);

			$contentParents = $this->renderPartial('_nodeParents', array(
				'currentNode' => $currentNode,
				'nodeParents' => $nodeParents,
			), true);

			$content = $this->renderPartial('move', array(
				'model' => $coreOrgChartNode,
				'contentParents' => $contentParents,
				'fullOrgChartTree' => $fullOrgChartTree,
				'defaultOrgChartTranslation' => $defaultOrgChartTranslation,
				'fullOrgChartTreeList' => $fullOrgChartTreeList,
				'allChildrenNodesIds' => $allChildrenNodesIds,
			), true);

			echo CJSON::encode(array(
				'html' => $content,
			));
		}
		exit;
	}

	public function actionDelete() {
		$id = Yii::app()->request->getParam('id');

		$lang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
		$with = array('coreOrgChartTranslated', 'coreOrgChartTranslatedDefaultLang');
		$currentNode = CoreOrgChartTree::model()->with($with)->findByPk($id);

		$nodeName = null;
		if($currentNode){
			if($currentNode->coreOrgChart && $currentNode->coreOrgChart->translation){
				// The node has a translation in the current language
				$nodeName = $currentNode->coreOrgChart->translation;
			}elseif($currentNode->coreOrgChartTranslatedDefaultLang){
				// Try to get the node's translation in the platform's default language
				$nodeName = $currentNode->coreOrgChartTranslatedDefaultLang->translation;
			}
		}

		$nodeToDelete = CoreOrgChartTree::model()->findByPk($id);

		if (empty($nodeToDelete)) {
			$this->sendJSON(array());
		}

		if (!empty($_POST['CoreOrgChartTree'])) {
			if ($_POST['CoreOrgChartTree']['confirm']) {
				if ($nodeToDelete->isLeaf()) {
					CoreOrgChartTree::model()->removeNode($nodeToDelete);
					$rootNode = CoreOrgChartTree::getOrgRootNode();
					Yii::app()->session['currentNodeId'] = $rootNode->idOrg;
					CoreRunningOperation::endOperation('org_chart_tree_change');
					$this->sendJSON(array());
					Yii::app()->end();
				}
			}
		}

//		CHECH IF BRANCH IS EMPTY AND STOP DELETE ACTION IF IS NOT EMPTY
		$branch = CoreOrgChartTree::model()->findByAttributes(
			array('idOrg' => $nodeToDelete['idOrg'])
		);
		
		$membersOfBranch = CoreGroupMembers::model()->findByAttributes(
			array('idst' => $branch['idst_oc'])
		);

		if (Yii::app()->request->isAjaxRequest) {
			if (empty($membersOfBranch)) {
				$content = $this->renderPartial('delete', array(
					'model' => new CoreOrgChartTree(),
					'currentNode' => $currentNode,
					'nodeName' => $nodeName,
				), true);
				$this->sendJSON(array(
					'html' => $content,
				));
			} else {
				$content = $this->renderPartial('stopDeleteBranchIsNotEmpty', array(
					'model' => new CoreOrgChartTree(),
					'currentNode' => $currentNode,
					'nodeName' => $nodeName
				), true);
				$this->sendJSON(array(
					'html' => $content,
				));
			}
		}

//		CHECH IF BRANCH IS EMPTY AND STOP DELETE ACTION IF IS NOT EMPTY

		Yii::app()->end();
	}

	public function actionGetNodeParents($selectedId, $currentId) {

		$currentNode = CoreOrgChartTree::model()->findByPk($selectedId);
		$movingNode = CoreOrgChartTree::model()->findByPk($currentId);

		$default_with = array(
			'coreOrgChart' => array(
				'on' => 'coreOrgChart.lang_code = :lang',
				'params' => array('lang' => Settings::get('defaultLanguage', 'english'))
			)
		);
		$defaultOrgChartTree     = CoreOrgChartTree::model()->with($default_with)->findAll(array('order' => 'iLeft'));
		$defaultCourseTreeTranslation = array();
		foreach ($defaultOrgChartTree as $node) {
			$defaultOrgChartTranslation[$node->idOrg] = $node->coreOrgChart->translation;
		}
		if (!empty($currentNode)) {

			$breadcrumbsNodes = $currentNode->ancestors()->findAll();
			$breadcrumbs      = '';
			$breadcrumbs .= '<ul class="clearfix">';
			if (!empty($breadcrumbsNodes)) {
				foreach ($breadcrumbsNodes as $breadcrumbsNode) {

					$node_name = $breadcrumbsNode->coreOrgChartTranslated->translation;
					if (!$node_name) $node_name = $defaultOrgChartTranslation[$breadcrumbsNode->idOrg];

					$breadcrumbs .= '<li>&nbsp;' . $node_name . '&nbsp;</li>';
				}
				$node_name = $currentNode->coreOrgChartTranslated->translation;
				if (!$node_name) $node_name = $defaultOrgChartTranslation[$currentNode->idOrg];
				$breadcrumbs .= '<li>&nbsp;' .$node_name . '&nbsp;</li>';
			}

			$node_name = $movingNode->coreOrgChartTranslated->translation;
			if (!$node_name) $node_name = $defaultOrgChartTranslation[$movingNode->idOrg];
			$breadcrumbs .= '<li class="last-node">&nbsp;' .$node_name . '</li>';
			$breadcrumbs .= '</ul>';

			$this->sendJSON(array(
				'content' => $breadcrumbs,
			));
			Yii::app()->end();
		}
	}

	public function actionAssignUsers($id)
	{
		$userModel = new CoreUser();
		if (isset($_POST['search_input']) && $_POST['search_input'] != '')
			$userModel->search_input = $_POST['search_input'];

		$chart = CoreOrgChartTree::model()->findByPk($id);
		if (isset($_POST['CoreUser']))
		{
			CoreGroupMembers::model()->deleteAllByAttributes(array('idst' => $chart->idst_oc));
			CoreGroupMembers::model()->deleteAllByAttributes(array('idst' => $chart->idst_ocd));
			$users = explode(',', $_POST['userAssign-user-grid-selected-items-list']);
			foreach ($users as $user)
			{
				//check OC
				$exists = CoreGroupMembers::model()->find(array(
					'condition' => 'idst = :idst_oc AND idstMember = :userId',
					'params' => array(':idst_oc' => $chart->idst_oc, ':userId' => $user)
				));
				if (!$exists)
				{
					$groupMember = new CoreGroupMembers();
					$groupMember->idst = $chart->idst_oc;
					$groupMember->idstMember = $user;
					$groupMember->save(false);
				}

				//check OCD
				$exists = CoreGroupMembers::model()->find(array(
					'condition' => 'idst = :idst_ocd AND idstMember = :userId',
					'params' => array(':idst_ocd' => $chart->idst_ocd, ':userId' => $user)
				));
				if (!$exists)
				{
					$groupMember = new CoreGroupMembers();
					$groupMember->idst = $chart->idst_ocd;
					$groupMember->idstMember = $user;
					$groupMember->save(false);
				}
			}
			$this->sendJSON(array());
			exit;
		}
		$processOutput = (isset($_GET['ajax'])) ? false : true;

		$cs=Yii::app()->clientScript;
		$cs->scriptMap=array( //always remove these because they are already added since a grid is used on the non-ajax page
			'jquery.js'=>false,
			'jquery.min.js'=>false,
			'jquery.yiigridview.js'=>false,
			'jquery.ba-bbq.js'=>false,
		);
		$content = $this->renderPartial('assignUsers', array(
			'userModel' => $userModel,
			'id' => $id,
		), true, $processOutput);
		if (isset($_GET['ajax']))
			echo $content;
		else
			$this->sendJSON(array('html' => $content, 'id' => $id));
	}





	public function actionAssignUsersV2($idOrg)
	{

        $criteria = new CDbCriteria();
        $criteria->with= array(
            'orgChartGroups' => array(
                'joinType' => 'INNER JOIN',
                'scopes' => array('nodeUsersOnly'),
            )
        );
        $assignedUsers = CoreUser::model()->findAll($criteria);

        $usersList = UsersSelector::getUsersList($_REQUEST);
        if (!empty($usersList)) {
            $usersAssignedInBranch = array();
            foreach ($assignedUsers as $assignedUser) {
                $usersAssignedInBranch[] = $assignedUser->idst;
            }

            $toEnroll = array();
            foreach ($usersList as $idUser) {
                if (!in_array($idUser, $usersAssignedInBranch)) {
                    $toEnroll[] = $idUser;
                }
            }
        }

		try {
			$chart = CoreOrgChartTree::model()->findByPk($idOrg);
			if (!$chart) {
				throw new CException('Invalid node ID/model');
			}
			$name = $chart->coreOrgChartTranslated->translation;
			$rq = Yii::app()->request;
			$chuncked = $rq->getParam('chuncked', false);

			if($chuncked && $usersList){
			    
			    $progressRunUrl  = Docebo::createLmsUrl('progress/run');
			    $progressType    =  Progress::JOBTYPE_ASSIGN_USERS_TO_SINGLE_BRANCH;
			    $progressUsers   = is_array($usersList) ? implode(',', $usersList) : $usersList;
			    $progressCount   = count($toEnroll);
			    $progressTitle   = Yii::t('standard', '_ASSIGN_USERS') . ": $name";
			    $progressiveJs   = <<< PJS
				<script type="text/javascript">
					$(function(){
						$.post('$progressRunUrl', {
							'type': $progressType,
							'idOrg':$idOrg,
							'idst': '$progressUsers',
                            'counter': $progressCount

						}, function(data){
							var dialogId = 'assign-users-to-single-branch-progressive';

							$('<div/>').dialog2({
								autoOpen: true, // We will open the dialog later
								id: dialogId,
								title: '$progressTitle'
							});
							$('#'+dialogId).html(data);
						});
					})
				</script>
PJS;
			    
			    echo $progressiveJs;
				echo '<a class="auto-close success"></a>';
				Yii::app()->end();
			}


			CoreGroupMembers::model()->deleteAllByAttributes(array('idst' => $chart->idst_oc));
			CoreGroupMembers::model()->deleteAllByAttributes(array('idst' => $chart->idst_ocd));

			foreach ($usersList as $idUser)
			{
				//check OC
				$exists = CoreGroupMembers::model()->find(array(
						'condition' => 'idst = :idst_oc AND idstMember = :userId',
						'params' => array(':idst_oc' => $chart->idst_oc, ':userId' => $idUser)
				));
				if (!$exists)
				{
					$groupMember = new CoreGroupMembers();
					$groupMember->idst = $chart->idst_oc;
					$groupMember->idstMember = $idUser;
					$groupMember->save(false);
				}

				//check OCD
				$exists = CoreGroupMembers::model()->find(array(
						'condition' => 'idst = :idst_ocd AND idstMember = :userId',
						'params' => array(':idst_ocd' => $chart->idst_ocd, ':userId' => $idUser)
				));
				if (!$exists)
				{
					$groupMember = new CoreGroupMembers();
					$groupMember->idst = $chart->idst_ocd;
					$groupMember->idstMember = $idUser;
					$groupMember->save(false);
				}
			}

			//TODO: this is better to be handled by events instead directly here !!!
			if (PluginManager::isPluginActive('PowerUserApp')) {
				//power users assignments may need to be updated
				if (is_array($usersList) && !empty($usersList)) {
					PowerUserAppModule::setUpdateJob($usersList);
				}
			}

			echo "<a class='auto-close success'></a>";

			Yii::app()->end();



		}
		catch (CException $e) {
			Yii::log($e->getMessage(),CLogger::LEVEL_ERROR);
			Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
			$selectorDialogUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
					'type' 	=> UsersSelector::TYPE_ORGCHART_ASSIGN_USERS,
					'idOrg'	=> $idOrg,
			));
			$this->redirect($selectorDialogUrl, true);
		}

		echo "<a class='auto-close success'></a>";
		Yii::app()->end();

	}



	/**
	 * Move Orgchart node around
	 * @throws CException
	 */
	public function actionAxMoveNode() {

		$rq = Yii::app()->request;

		$idOrg = $rq->getParam('idOrg', false);  // the node to be moved
		$idOrgTarget = $rq->getParam('idOrgTarget', false);  // target node whereabout node is moved into/before/after
		$hitMode = $rq->getParam('hitMode', false);   // where to: inside? (over), after, before ?

		$response = new AjaxResult();

		$transaction = Yii::app()->db->beginTransaction();

		try {

			$movingNode 		= CoreOrgChartTree::model()->findByAttributes(array('idOrg' => $idOrg));
			$targetParentNode 	= CoreOrgChartTree::model()->findByAttributes(array('idOrg' => $idOrgTarget));

			if (!$movingNode || !$targetParentNode || !$hitMode) {
				throw new CException("Invalid nodes or moving mode");
			}

			CoreOrgChartTree::model()->flushCache();

			switch ($hitMode) {
				case 'before':
					$movingNode->moveBefore($targetParentNode);
					break;
				case 'after':
					$movingNode->moveAfter($targetParentNode);
					break;
				case 'over':
					$movingNode->moveAsFirst($targetParentNode);
					break;
				default:
					throw new CException("Invalid moving mode");
			}

			CoreGroupFields::propagateBranchFieldAssignments($targetParentNode);
			$movingNode->updateEnrollmentRulesBranches();

			$transaction->commit();

			CoreRunningOperation::endOperation('org_chart_tree_change');
			$newTreeGenerationTime = CoreRunningOperation::getCurrentTimeMicroseconds();

			$response->setStatus(true)->setData(array('new_tree_generation_time' => $newTreeGenerationTime))->toJSON();
			Yii::app()->end();

		}
		catch (CException $e) {
			$transaction->rollback();
			CoreRunningOperation::abortOperation('org_chart_tree_change');
			$response->setStatus(false)->setMessage($e->getMessage())->toJSON();
			Yii::app()->end();
		}

		Yii::app()->end();
	}


	/**
	 * Set node fields visibility
	 */
	public function actionEditFields() {

		if (Settings::get('use_node_fields_visibility', 'off') != 'on') return; //always check just to be sure

		$rq = Yii::app()->request;

		//read and validate input data
		$idOrg = $rq->getParam('id', false);  //the ID of node to be handled
		$node = CoreOrgChartTree::model()->findByPk($idOrg);
		if (!$node) { throw new CException('Invalid specified node'); }

		// Find all inherited fields
		$rootNode = CoreOrgChartTree::model()->getOrgRootNode();
		$inherited = array();
		$cmd = Yii::app()->db->createCommand("SELECT gf.id_field "
			." FROM ".CoreOrgChartTree::model()->tableName()." oct "
			." JOIN ".CoreGroupFields::model()->tableName()." gf ON (gf.idst = oct.idst_oc OR gf.idst = oct.idst_ocd) "
			." WHERE oct.iLeft < :iLeft AND oct.iRight > :iRight AND oct.idOrg <> :root");
		
		$inherited = $cmd->queryColumn(array(
			':iLeft' => $node->iLeft,
			':iRight' => $node->iRight,
			':root' => $rootNode->getPrimaryKey()
		));
		
		// Check if action has been confirmed
		if ($rq->getParam('confirm', false) !== false) {
		    
			$transaction = Yii::app()->db->beginTransaction();
			
			try {
			    
			    // Remove all direct assignments from the branch
			    CoreGroupFields::model()->deleteAll("idst = :idst_oc OR idst = :idst_ocd", array(
			        ':idst_oc' => $node->idst_oc,
			        ':idst_ocd' => $node->idst_ocd
			    ));
			    
			    // Read field IDs from input
			    $newFields = $rq->getParam('orgchart-fields-visibility-grid-checkboxes', array());
			    	
			    // And Assign all "new fields"
				foreach ($newFields as $idField) {
				    // Skip Inherited fields 
				    if (!in_array($idField, $inherited)) {
    					$assignment = new CoreGroupFields();
	   		      		$assignment->idst = $node->idst_ocd;
                        $assignment->id_field = $idField;   
                        $rs = $assignment->save();
				    }
				}

				$transaction->commit();

			} catch (CException $e) {
				//something went wrong
				$transaction->rollback();
				Yii::log('['.__METHOD__.'] : '.$e->getMessage(),CLogger::LEVEL_ERROR);
			}

			$this->sendJSON(array());
			Yii::app()->end();
		}

		//retrieve already assigned fields
		$criteria = new CDbCriteria();
		$criteria->addCondition("idst = :idst_oc OR idst = :idst_ocd");
		$criteria->params[':idst_oc'] = $node->idst_oc;
		$criteria->params[':idst_ocd'] = $node->idst_ocd;
		$assignedData = CoreGroupFields::model()->findAll($criteria);
		$assigned = array(
			'oc' => array(),
			'ocd' => array()
		);
		if (!empty($assignedData)) {
			foreach ($assignedData as $assignedItem) {
				if ($assignedItem->idst == $node->idst_oc) { $assigned['oc'][] = $assignedItem->id_field; }
				if ($assignedItem->idst == $node->idst_ocd) { $assigned['ocd'][] = $assignedItem->id_field; }
			}
		}
		
		// Render dialog
		if (Yii::app()->request->isAjaxRequest) {
			$content = $this->renderPartial('editFieldsVisibility', array(
				'idOrg'         => $idOrg,
				'node_name'     => $node->coreOrgChartTranslated && trim($node->coreOrgChartTranslated->translation) ? $node->coreOrgChartTranslated->translation : $node->coreOrgChartTranslatedDefaultLang->translation,
				'assigned'      => $assigned,
				'inherited'     => $inherited
			), true);
			$this->sendJSON(array(
				'html' => $content,
			));
		}
		Yii::app()->end();
	}



	/**
	 * Check if a given CODE is already assigned to ANY Branch, excluding the $idOrg from the check
	 *
	 * @param string $code Branch code
	 * @param number $idOrg
	 */
	public function actionAxCheckBranchCode($code, $idOrg) {
		$response = new AjaxResult();

		$model = CoreOrgChartTree::model()->findByAttributes(array('code' => $code));

		$data['exists'] = false;
		if ($model && ($model->idOrg != $idOrg)) {
			$data['exists'] = true;
		}

		$response->setStatus(true)->setData($data)->toJSON();

		Yii::app()->end();
	}


}
