<?php
class TransactionController extends AdminController
{

	public $layout='//layouts/adm';


	public function filters() {
		return array(
				'accessControl' => 'accessControl',
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		$res = array();

		$res[] = array(
				'allow',
				'expression' => 'Yii::app()->user->isGodAdmin',
		);

		$res[] = array(
			'deny',
			// deny all
			'users' => array('*'),
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);

		return $res;
	}


	/**
	 * (non-PHPdoc)
	 * @see Controller::init()
	 */
	public function init() {

		$cs = Yii::app()->getClientScript();

		DatePickerHelper::registerAssets();

		// Register JS files
		$cs->registerScriptFile(Yii::app()->baseUrl . '/js/transaction.js', CClientScript::POS_HEAD);

		// Register JS script(s)
		$options = array();
		$options = CJavaScript::encode($options);
		$script = "var Transaction = new TransactionAdmin($options);";
		$cs->registerScript("transactionJS1", $script , CClientScript::POS_HEAD);

		JsTrans::addCategories('EcommerceApp');
		
		return parent::init();
	}



	/**
	 *
	 */
	public function actionIndex() {

		$model = new EcommerceTransaction('search');

		// Search might coming?
		if ($data = Yii::app()->request->getParam(get_class($model), false)) {
			$model->attributes = $data;
		}

		$this->render('index', array(
			'model' => $model,
		));

	}


	/**
	 *
	 * @param unknown $row
	 * @param unknown $index
	 */
	protected function _renderGridOperationsColumn($row, $index) {
		$html = $this->renderPartial('_transaction_operations', array(
				'model' => $row,
		), true);

		return $html;

	}



	/**
	 * Build HTML for One transaction popover, showing its content
	 *
	 * @param object $row Transaction model (a row from grid view)
	 * @return string
	 */
	protected function _getPopoverContent($row) {
		$html = $this->renderPartial('_transaction_content_popover', array(
				'transaction' => $row,
		), true);
		return $html;
	}


	public function actionAxGetTransactionPopoverContent() {
		$id_trans = Yii::app()->request->getParam('id_trans', false);
		$transaction = EcommerceTransaction::model()->findByPk($id_trans);
		$html = $this->_getPopoverContent($transaction);
		return $html;
	}

	/**
	 * Edit transaction items (Dialog)
	 */
	public function actionAxEdit() {

		$id_trans = Yii::app()->request->getParam("id_trans", false);

		// DB Start transaction
		$db_transaction = Yii::app()->db->beginTransaction();


		try {
			$criteria = new CDbCriteria();
			$criteria->addCondition('id_trans=:id_trans');
			$criteria->params[':id_trans'] = $id_trans;
			$criteria->with = array('user', 'billingInfo');
			$criteria->together = true;

			$model = EcommerceTransaction::model()->find($criteria);

			if (!$model) {
				throw new CException(Yii::t('standard','_OPERATION_FAILURE'));
			}

			// Get Transactipon items model, used in Grid View
			$itemsModel = new EcommerceTransactionInfo();
			$itemsModel->id_trans = $id_trans;

			// Form submition ??
			if ($transactionData = Yii::app()->request->getParam("EcommerceTransaction", false)) {
				$model->attributes = $transactionData;
				if ($model->validate()) {
					$model->save(false);

					// Any "activate" checkboxes data? Update "activated" attribute
					if ($itemsData = Yii::app()->request->getParam("EcommerceTransactionInfo", false)) {
						$items = $model->transaction_items;
						// Enumerate all items and activate/deactivate
						foreach($items as $item) {
							/* @var $item EcommerceTransactionInfo */
							$item->activated = isset($itemsData[$item->id_trans]) ? $itemsData[$item->id_trans] : 0;
							$item->save();
							if ($item->activated) {
								LearningCourseuser::model()->activateSubscription($model->id_user, $item->id_course, $item->id_edition);
								// enroll the user to the session if this is a classroom or webinar course
								if (intval($item->id_date) > 0) {
									// Differentiate between classroom & webinar courses
									switch($item->course->course_type) {
										case LearningCourse::TYPE_CLASSROOM:
											LtCourseuserSession::model()->activateSession($model->id_user, $item->id_date);
											break;
										case LearningCourse::TYPE_WEBINAR:
											WebinarSessionUser::model()->activateSession($model->id_user, $item->id_date);
											break;
									}
								}
							}
							else {
								LearningCourseuser::model()->suspendSubscription($model->id_user, $item->id_course, $item->id_edition);
							}
						}
					}


					$db_transaction->commit();
					echo '<a class="auto-close success"></a>';
					Yii::app()->end();
				}
				else {
					throw new CException(Yii::t('standard','_OPERATION_FAILURE'));
				}
			}

			$html = $this->renderPartial('_edit_transaction_items', array(
					'model' 		=> $model,
					'itemsModel' 	=> $itemsModel,
			));

			echo $html;
			Yii::app()->end();

		}
		catch (CException $e) {
			$db_transaction->rollback();
			$html = $this->renderPartial('//common/_dialog2_error', array(
					'message' => $e->getMessage(),
			), true);
			echo $html;
			Yii::app()->end();
		}

	}


	/**
	 * Show DELETE Transaction dialog and handle user submit
	 *
	 * @throws CException
	 */
	public function actionAxDelete() {

		$id_trans = Yii::app()->request->getParam("id_trans", false);
		$confirm_delete = Yii::app()->request->getParam("confirm_delete", false);

		// DB Start transaction
		$transaction = Yii::app()->db->beginTransaction();

		try {
			$model = EcommerceTransaction::model()->findByPk($id_trans);
			if (!$model) {
				throw new CException(Yii::t('standard','_OPERATION_FAILURE'));
			}

			if ($confirm_delete) {
				
				/*
				 * NOTE: Ecommerce transaction are no more deleted, they are ALWAYS present, even if directly cancelled 
				 * by an admin. We always keep track of past transaction in the LMS, the records never go away.
				 */
				/*
				// First, delete all transaction items
				$items = $model->transaction_items;
				foreach ($items as $item) {
					if (!$item->delete()) {
						throw new CException(Yii::t('standard','_OPERATION_FAILURE'));
					}
				}
				// Now delete the transaction itself
				if (!$model->delete()) {
					throw new CException(Yii::t('standard','_OPERATION_FAILURE'));
				}
				*/
				if ($model->cancelled == 0) {
					$model->cancelled = 1;
					$model->done_by = EcommerceTransaction::DONE_BY_ADMIN;
					$model->save();
				} else {
					throw new CException('Transaction #'.$model->id_trans.' has already been cancelled');
				}

				$transaction->commit();
				echo '<a class="auto-close success"></a>';
				Yii::app()->end();
			}

			// Show Confirm Dialog
			$html = $this->renderPartial('_delete_transaction', array(
				'model' => $model,
			), true);

			echo $html;
			Yii::app()->end();

		}
		catch (CException $e) {
			$transaction->rollback();

			Yii::log(__METHOD__.' : '.$e->getMessage(), CLogger::LEVEL_ERROR);

			$html = $this->renderPartial('//common/_dialog2_error', array(
				'message' => $e->getMessage(),
			), true);

			echo $html;
			Yii::app()->end();
		}


	}


	/**
	 * Export filtered data to XLS/CSV/Html/...
	 *
	 */
	public function actionExport()
	{
		$model = new EcommerceTransaction();
		$model->unsetAttributes();

		// Serialized form data
		$data = Yii::app()->request->getParam('EcommerceTransaction', array());
		$exportType = Yii::app()->request->getParam('export_transactions', "Excel5");
		$model->setScenario('search');
		$model->attributes = $data;

		$columns = array(
				Yii::t('admin_directory', '_DIRECTORY_GROUPID'),
                Yii::t('standard', '_DATE'),
				Yii::t('standard', '_USERNAME'),
				Yii::t('standard', '_FIRSTNAME'),
				Yii::t('standard', '_LASTNAME'),
                Yii::t('coupon','Subtotal'),
                Yii::t('coupon','Discount'),
                Yii::t('coupon', 'Total paid'),
				Yii::t('standard', 'Currency'),
                Yii::t('billing', '_PAID') . "?",
                Yii::t('cart', 'Payment method'),
				Yii::t('standard', 'Payment Txn Id'),
				Yii::t('standard', '_NOTES'),
				Yii::t('standard','_EMAIL'),
				Yii::t('billing','_COMPANY_NAME'),
				Yii::t('order','Vat number'),
				Yii::t('standard','Address 1'),
				Yii::t('standard','Address 2'),
				Yii::t('classroom','_CITY'),
				Yii::t('classroom','_STATE'),
				Yii::t('classroom','_ZIP_CODE'),
		);

		$excelData = array();

		$dataProvider = $model->transactionsManageDataProvider(false);

		$i = 0;


		$i++;
		foreach ($dataProvider->data as $row)
		{
			$excelData[$i][] = $row['id_trans'];
			$excelData[$i][] = Yii::app()->localtime->toLocalDateTime($row['date_creation'],'short');
			$excelData[$i][] = (substr($row['userid'], 0, 1) == '/' ? substr($row['userid'], 1) : $row['userid']);
			$excelData[$i][] = $row['firstname'];
			$excelData[$i][] = $row['lastname'];
			$excelData[$i][] = ($row['totalPrice'] ? $row['totalPrice'] : 0);
			$excelData[$i][] = $row['discount'];
			$excelData[$i][] = ((($row['totalPrice'] ? $row['totalPrice'] : 0) - $row['discount']) < 0 ? 0 : ($row['totalPrice'] ? $row['totalPrice'] : 0) - $row['discount']);
			$excelData[$i][] = $row['payment_currency'];
			$excelData[$i][] = $row['paid'] ? "YES" : "NO";
			$excelData[$i][] = $row['payment_type'];
			$excelData[$i][] = $row['payment_txn_id'];
			$excelData[$i][] = $row['note'];
			$excelData[$i][] = isset($row['email']) ?  $row['email'] : "";
			$excelData[$i][] = isset($row['bill_company_name']) ?  $row['bill_company_name'] : "";
			$excelData[$i][] = isset($row['bill_vat_number']) ?  $row['bill_vat_number'] : "";
			$excelData[$i][] = isset($row['bill_address1']) ?  $row['bill_address1'] : "";
			$excelData[$i][] = isset($row['bill_address2']) ?  $row['bill_address2'] : "";
			$excelData[$i][] = isset($row['bill_city']) ?  $row['bill_city'] : "";
			$excelData[$i][] = isset($row['bill_state']) ?  $row['bill_state'] : "";
			$excelData[$i][] = isset($row['bill_zip']) ?  $row['bill_zip'] : "";
			$i++;
		}

		Yii::import('common.extensions.array2excel.Array2Excel');
		$xls = new Array2Excel($excelData, $columns);
		$xls->autoWidth = true;
		if (array_key_exists($exportType, EcommerceTransaction::exportTypes())) {
			$xls->exportType = $exportType;
		}
		else
		{
			Yii::app()->user->setFlash('error', 'Opaaaa');
			$this->redirect($this->createUrl('transaction/index'), true);
		}

		$xls->filename = Yii::t('standard', '_TRANSACTION');
		$xls->run();
	}
}
