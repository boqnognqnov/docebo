<?php
class QuestionBankController extends AdminController
{

	public $layout='//layouts/adm';


	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		$res = array();

		$res[] = array(
			'allow',
			'expression' => 'LearningCourseuser::isUserLevel(Yii::app()->user->id, Yii::app()->request->getParam("idCourse"), LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR)',
			'actions' => array('import')
		);

		$res[] = array(
			'allow',
			'expression' => '(Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess("/lms/admin/course/mod"))',
		);

		$res[] = array(
			'allow',
			'roles' => array('/framework/admin/setting/view'),
		);

		$res[] = array(
			'deny',
			// deny all
			'users' => array('*'),
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);

		return $res;
	}


	/**
	 * (non-PHPdoc)
	 * @see Controller::init()
	 */
	public function init() {
		return parent::init();
	}

	public function actionIndex()
	{
		$model = new LearningTestquest();
		if (isset($_GET['LearningTestquest']))
		{
			$model->attributes = $_GET['LearningTestquest'];
		}
		$this->render('index', array(
			'model' => $model
		));
	}

	public function actionImport($idTest)
	{
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/admin.css');
		$model = new LearningTestquest();
		if (isset($_GET['LearningTestquest']))
		{
			$model->attributes = $_GET['LearningTestquest'];
		}
		if (isset($_POST['selection']) && trim($_POST['selection']) != '')
		{
			$items = explode(',', $_POST['selection']);
			foreach ($items as $questId)
			{
				$rel = new LearningTestQuestRel();
				$rel->id_test = $idTest;
				$rel->id_question = $questId;
				$rel->sequence = LearningTestquest::getTestNextSequence($idTest);
				$rel->page = LearningTestquest::getTestPageNumber($idTest);
				$rel->save(false);

				$test = LearningTest::model()->findByPk($idTest);

				if($test->display_type == LearningTest::DISPLAY_TYPE_GROUPED && ($test->order_type == LearningTest::ORDER_TYPE_SEQUENCE || $test->order_type == LearningTest::ORDER_TYPE_RANDOM))
				{
					$page_sequence = LearningTestQuestRel::model()->countByAttributes(array('id_test' => $idTest, 'page' => $rel->page)) + 1;

					$query = "
						INSERT INTO ".LearningTesttrackQuest::model()->tableName()."
						SELECT t.idTrack, ".$rel->id_question.", ".$rel->page.", ".$page_sequence.", NULL
						FROM ".LearningTesttrack::model()->tableName()." AS t
						JOIN ".LearningTesttrackPage::model()->tableName()." AS tp ON tp.idTrack = t.idTrack
						WHERE t.idTest = ".(int)$idTest."
						AND tp.page = ".(int)$rel->page."
						AND t.score_status IN ('".LearningTesttrack::STATUS_DOING."', '".LearningTesttrack::STATUS_NOT_COMPLETE."')";

					if(Yii::app()->db->createCommand($query)->execute() === false)
						throw new CException('Error while executing operation');
				}
			}

            $idOrg = Yii::app()->request->getParam("idOrg", false);
			if($idOrg){
				$LearningOrganizationModel = LearningOrganization::model()->findByPk($idOrg);
				$LearningOrganizationModel->visible = 1;
				$LearningOrganizationModel->saveNode();
			}

			echo '<a class="auto-close"></a>';
			exit;
		}

		// The font awesome is loading twice so prewent if from loading
		Yii::app()->clientScript->scriptMap = array(
			'font-awesome.min.css' => false
		);

		$this->renderPartial('_import', array(
			'model' => $model,
			'idTest' => $idTest,
			'dataProvider' => $model->getAvailableBankQuestions($idTest)
		), false, true);
	}

	public function actionQuestionUsage($idQuest)
	{
		$question = LearningTestquest::model()->findByPk($idQuest);

		echo $this->renderPartial('_question_usage', array(
			'question' => $question,
		), false, true);
	}

	public function actionAxToggleVisibility($idQuest)
	{
		$model = LearningTestquest::model()->findByPk($idQuest);
		if ($model) {
			$model->visible = !$model->visible;
			$model->save(false);
		}
	}

	public function actionAxDelete($idQuest)
	{
		$model = LearningTestquest::model()->findByPk($idQuest);
		$error = '';

		if ($model) {
			if ($model->getTestCount() == 0)
			{
				if (isset($_POST['delete'])) {
					$model->delete();
					echo '<a class="auto-close"></a>';
					exit;
				}
			}
			else
				$error = Yii::t('test', 'Question <strong>{question}</strong> is assigned to a test!', array('{questin}' => $model->getTitleQuestTruncated()));
		}
		else {
			$error = Yii::t('test', 'The question is not found or already deleted!');
		}
		echo $this->renderPartial('_delete', array(
			'model' => $model,
			'error' => $error,
		));
	}

	public function actionAxCreateQuestion()
	{
		if (isset($_POST['questionType'])) {
			$url = Docebo::createLmsUrl('test/question/create', array('question_type' => $_POST['questionType']));
			$this->renderPartial('lms.protected.views.site._js_redirect', array('url' => $url ));
		}


		echo $this->renderPartial('_create_question', array(
		));
	}

	public function actionAxMassAction($items)
	{
		if ($items != '')
		{
			$criteria = new CDbCriteria();
			$criteria->addInCondition('idQuest', explode(',', $items));
			LearningTestquest::model()->updateAll(array('visible'=>0), $criteria);
		}
	}
}
