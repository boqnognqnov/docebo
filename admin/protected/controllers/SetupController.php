<?php

class SetupController extends AdminController {

	public $layout = '//layouts/adm';

	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules() {
		$res = array();

		$res[] = array(
			'allow',
			'roles' => array('/framework/admin/dashboard/view'),
		);

		$res[] = array(
			'allow',
			'actions' => array("colorschemeCss"),
			'users' => array('*'),
		);

		$res[] = array(
			'deny',
			'users' => array('*'),
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);

		return $res;
	}

	public function init() {

		// If we got a token, someone is either coming for the first time or ... maybe try to hack...
		$token = Yii::app()->request->getParam("token", false);
		if ($token)  {
			$username = Yii::app()->request->getParam("un", false);
			if (!$this->_loginGodAdminWithToken($token, $username)) {
				$this->adminHomepageRedirect();
			}

			Yii::app()->request->cookies['dr'] = new CHttpCookie('dr', Yii::app()->params['releaseSubfolder'], array(
			'domain' => '',
			'expire' => 0,
			'secure' => false,
			'path' => '/',
			'httpOnly' => true,
			));

			$setupUrl = Docebo::createAdminUrl('setup');
			$this->redirect($setupUrl);


		}

		/* @var $cs CClientScript */
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/setup.css');

		// BlockUI
		$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.blockUI.js');


		parent::init();
	}


	protected function _getLoginImages() {
		$gallerySize = 6;
		$defaultImages  = CoreAsset::model()->urlList(CoreAsset::TYPE_LOGIN_BACKGROUND, CoreAsset::SCOPE_SHARED, false, CoreAsset::VARIANT_SMALL);
		$defaultImages = array_chunk($defaultImages, $gallerySize, true);
		return $defaultImages;
	}


	/**
	 * Logs in admin or other user by setup token
	 *
	 * @param string $token
	 * @param string $username
	 * @return boolean
	 */
	protected function _loginGodAdminWithToken($token, $username=false) {

		$myToken = sha1(
				'docebo_setup:' .
				Settings::getCfg('erp_secret_key') . ':' .
				Settings::getCfg('erp_installation_id') . ':' .
				Settings::getCfg('install_from')
		);

		$token = trim($token);
		if (($myToken != $token) || empty($token)) {
			return false;
		}

		if ($username === false) {
			$godAdmins = Yii::app()->user->getAllGodAdmins();
			if (!$godAdmins || count($godAdmins) <= 0) {
				return false;
			}
			$godAdmin = $godAdmins[0];
			$username = $godAdmin->getClearUserId();
		}

		$ui = new DoceboUserIdentity($username, 'dummy');

		if ($ui->authenticateWithNoPassword($ui)) {
			return Yii::app()->user->login($ui);
		}

		return false;

	}



	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {

		// If we land here, we're a trial that completed the installation process.
		// Therefore, set up "Docebo 7.0" as the active theme
		Yii::app()->legacyWrapper->enableHydraFrontend();
		Settings::save('saas_installation_status', 0);
		$this->redirect(Yii::app()->getRequest()->getHostInfo()."/setup");
		return;


		$errors = array();
		if (isset($_POST['Setup']))
		{
			$setupData = $_POST['Setup'];
			try
			{
				if (!empty($setupData['logo'])) {
					$filename = $setupData['logo'];
					$uploadTmpFile = Docebo::getUploadTmpPath() . DS . $filename;
					if (!is_file($uploadTmpFile)) { throw new CException("File '" . $filename . "' does not exist."); }
					
					$asset = new CoreAsset();
					$asset->type = CoreAsset::TYPE_GENERAL_IMAGE;
					$asset->sourceFile = $uploadTmpFile;
					if (!$asset->save()) { throw new CException('Could not save logo!'); }
					Settings::save('company_logo', $asset->id);
				}


				if (!empty($setupData['loginimage'])) {
					$filename = $setupData['loginimage'];
					$uploadTmpFile = Docebo::getUploadTmpPath() . DS . $filename;
					if (!is_file($uploadTmpFile)) { throw new CException("File '" . $filename . "' does not exist."); }
					
					
					$asset = new CoreAsset();
					$asset->type = CoreAsset::TYPE_LOGIN_BACKGROUND;
					$asset->sourceFile = $uploadTmpFile;
					if (!$asset->save()) { throw new CException('Could not save login image!'); }
					Settings::save('home_login_img', $asset->id);
					
				}
				/**
				 * If a custom login image was not uploaded, check if one of the default login images has been set
				 */
				else if (isset($setupData['default_loginimage'])) {
					$asset = CoreAsset::model()->findByPk(intval($setupData['default_loginimage']));
					if ($asset) { 
						Settings::save('home_login_img', $asset->id);
					}
				}

				// save color scheme
				$colorscheme = CoreScheme::model()->findByPk((int) $setupData['colorscheme']);
				if (!$colorscheme) {
					$colorscheme = CoreScheme::model()->findByAttributes(array(
                        'enabled' => true
                    ));
				}
				if ($colorscheme) {
					Settings::save('selected_color_scheme', $colorscheme->getPrimaryKey());
                    // enable
                    $transaction = Yii::app()->db->beginTransaction();

                    try {
                        $command = Yii::app()->db->createCommand();
                        $command->update('core_scheme', array('enabled' => 0));

                        $colorscheme->enabled = 1;
                        $colorscheme->save();
                    } catch (Exception $e) {
                        $transaction->rollback();
                    }

                    $transaction->commit();
				}

				// after saving data
				Settings::save('saas_installation_status', 1);
				$this->redirect($this->createUrl('info'));
			}
			catch (Exception $ex)
			{
				$errors[] = Yii::t('setup', 'In order to continue, please provide the requested information');
			}
		}

		// getting logo size, providing also the defaults
		$logoSize = array(
			'width' => 230,
			'height' => 85,
			'filesize' => '10mb'
		);

		$loginImageSize = array(
			'width' => 900,
			'height' => 372,
			'filesize' => '10mb'
		);

		// get the custom login bg if it exist
		$customLoginImgUrl = '';
		$homeLoginImg = trim(Settings::get('home_login_img'));
		$customLoginImgUrl = CoreAsset::url($homeLoginImg, CoreAsset::VARIANT_SMALL);

		$this->breadcrumbs[Yii::t('standard', 'Welcome')] = '#';

		$this->render('index', array(
			'colorschemes' => CoreScheme::getColorSchemeList(),
			'defaultImages' => $this->_getLoginImages(),
			'logoSize' => $logoSize,
			'loginImageSize' => $loginImageSize,
			'customLoginImgUrl' => $customLoginImgUrl,
			'homeLoginImg' => $homeLoginImg,
			'installationUrl' => 'http://' . $_SERVER['HTTP_HOST'],
			'errors' => $errors
		));
	}

	public function actionInfo()
	{
		$errors = array();
		$data_params = array();

		// Form fields we are going to handle
		$fields = array(
		    "name", 
		    "company_name", 
		    "phone", 
		    "country", 
		    "company_size", 
		    "area", 
		    "department", 
		    "role",
		    "pp_challenge",
		    "pp_already_lms",
		    "pp_end_users",
		);
		
		
		// Fields we are going to skip in the form, if they are extracted from ERP and NOT empty
		$fieldsToSkip = array();

		// Collect CRM data about this installation by calling ERP API (v2)
	    $api       = new ErpApiClient2();
	    $crmData   = $api->getInstallationCrmData();
	    
	    // Set this to true/false if ANY of the $fields is empty in ERP 
	    $hasEmpty  = false;
		if ($crmData !== false) {
	       foreach ($crmData as $name => $value) {
	           $data_params[$name] = $value;
	           if (in_array($name, $fields) && empty($data_params[$name])) {
	               // If the field is pp_end_users and pp_already_lms is NOT not_interested_in_lms, yes, it is empty 
	               if (  ($name != 'pp_end_users') || 
	                     ( isset($data_params['pp_already_lms']) && ($name == 'pp_end_users')  && ($data_params['pp_already_lms'] != 'not_interested_in_lms') )
	                   ) {
	                   $hasEmpty = true;
	               }
	           }
	           else {
	               // Non-empty field? Add to skip-list, do not allow/try to edit at all
                   $fieldsToSkip[] = $name;
	           }
	       }
		}

		// If there is NO single empty field in ERP, just skip the whole Step
 		if (!$hasEmpty) {
 		    Settings::save('saas_installation_status', 0);
 		    $this->redirect(Docebo::createAppUrl('lms:site/index'));
 		}
		
 		// Handle form submit
		if (isset($_POST['Setup']))
		{
		    // Merge ERP extracted data with Form submitted ones, we need to be sure all required fields are filled
			$data_params = array_merge($data_params, $_POST['Setup']);
			$requiredFields = array(
			    'name', 
			    'company_name', 
			    'country', 
			    'area', 
			    'company_size', 
			    'department',
			    "role",
			    "pp_challenge",
			    "pp_already_lms",
			);

			if ($data_params['pp_already_lms'] !== 'not_interested_in_lms') {
			    $requiredFields[] = 'pp_end_users';
			}

			$isFormValid = true;
			foreach ($requiredFields as $requiredField) {
				if (empty($data_params[$requiredField])) {
					$isFormValid = false;
					break;
				}
			}
			
			if (!$isFormValid) {
				$errors[] = Yii::t('setup', 'In order to continue, please provide the requested information');
			} else {
			    // Form is valid, all required fields are filled
				$erp_installation_id = Docebo::getErpInstallationId();
				if (empty($post_params['phone'])) $post_params['phone'] = '';
				$data_params['installation_id'] = $erp_installation_id;
				// Save LMS info into ERP
				$api_res = ErpApiClient::apiErpSetupSetCustomerInfo($data_params);
				if ($api_res) {
					Settings::save('saas_installation_status', 0);
					$this->redirect(Docebo::createAppUrl('lms:site/index'));

				} else {
					$errors[] = Yii::t('standard', 'internal error - contact us');
				}
			}
		}

		$this->breadcrumbs[Yii::t('standard', 'Welcome')] = '#';

		$this->render('info', array(
		    'fieldsToSkip' => $fieldsToSkip,
			'infoData'=>$data_params,
			'countries'=>ErpApiClient::getErpCountriesArr(),
			'company_sizes'=>ErpApiClient::getErpCrmCompanySizes(),
			'company_industries'=>ErpApiClient::getErpCrmIndustries(),
			'errors'=>$errors
		));
		
	}

	public function actionColorschemeCss($scheme = false)
	{
		header("Content-type: text/css");
		echo CoreScheme::generateCss($scheme);
		Yii::app()->end();
	}



	/**
	 * Temporary put this here
	 * @param unknown $string
	 */
	protected function getUniqueHash($string) {
		$hash = md5(uniqid($string,true));
		return $hash;
	}


}