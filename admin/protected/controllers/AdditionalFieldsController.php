<?php

class AdditionalFieldsController extends AdminController {

	public function init() {
		parent::init();
		JsTrans::addCategories("user_management");
	}

	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}

	public function accessRules() {
		$res = array();

		$res[] = array(
			'allow',
			'roles' => array('/framework/admin/field_manager/view'),
			'actions' => array('index', 'fieldsAutocomplete'),
		);

		$res[] = array(
			'allow',
			'roles' => array('/framework/admin/field_manager/add'),
			'actions' => array('index', 'createFieldModal', 'create', 'editField', 'createDropdown', 'editDropdown', 'orderFields', 'saveDropdownSingleLanguage'),
		);

		$res[] = array(
			'allow',
			'roles' => array('/framework/admin/field_manager/del'),
			'actions' => array('deleteField'),
		);

		$res[] = array(
			'allow',
			'roles' => array('/framework/admin/field_manager/mod'),
			'actions' => array('index'),
		);

		$res[] = array(
			'deny',
			// deny all
			'users' => array('*'),
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);

		return $res;
	}



	public function beforeAction($action) {

		switch ($action->id) {
			case 'index':

				// Check if all fields have all required translations
				// NOTE: usually all fields languages are ok and translations are updated every time a language is
				// being activated, but it may happen that they are not perfectly synchronized (maybe
				// after some particular migration, version update, etc.).
				// When translations are missing, some visualization problems may arise in fields management interfaces (and
				// possibly in other parts where fields are used), so try to avoid it as much as possible with this check.

				$activeLanguagesList = CoreLangLanguage::getActiveLanguages();
				$activeLanguages = array_keys($activeLanguagesList);

				//first handle field translations
                CoreUserFieldTranslations::insertMissing($activeLanguages);

				//then handle field option translations (if any)
                CoreUserFieldDropdownTranslations::insertMissing($activeLanguages);

				break;
		}

		return parent::beforeAction($action);
	}


    /**
     * Index action of additional fields
     * Listing existing additional fields with options for creating new/ updating/ deleting entries
     *
     * @return void
     */
    public function actionIndex()
    {
		$cs = Yii::app()->clientScript;

		DatePickerHelper::registerAssets();
		$cs->registerCoreScript('jquery.ui');
		$cs->registerScriptFile(Yii::app()->baseUrl.'/js/dropdown_field_management.js', CClientScript::POS_HEAD);

		$coreField = CoreUserField::model();
		$fieldTypes = [
            CoreUserField::TYPE_COUNTRY     => CoreUserField::TYPE_COUNTRY,
            CoreUserField::TYPE_DATE        => CoreUserField::TYPE_DATE,
		    CoreUserField::TYPE_DROPDOWN    => CoreUserField::TYPE_DROPDOWN,
		    CoreUserField::TYPE_FISCALCODE  => CoreUserField::TYPE_FISCALCODE,
		    CoreUserField::TYPE_TEXT        => CoreUserField::TYPE_TEXT,
		    CoreUserField::TYPE_TEXTAREA    => CoreUserField::TYPE_TEXTAREA,
		    CoreUserField::TYPE_UPLOAD      => CoreUserField::TYPE_UPLOAD,
		    CoreUserField::TYPE_YESNO       => CoreUserField::TYPE_YESNO,
        ];

		$this->render('index', array(
			'coreField' => $coreField,
			'fieldTypes' => $fieldTypes,
		));
	}


	protected function gridRenderTranslation($data, $index)
	{
		return (empty($data->translation) && !empty($data->enTranslation)) ? $data->enTranslation[0]->translation : $data->translation;
	}

    protected function deleteRenderTranslation($data) {
        $defaultLangCode = Settings::get(CoreSetting::$params['default_language']);
        $defaultLangTranslation = Yii::app()->db->createCommand('SELECT translation FROM core_user_field_translation
          WHERE lang_code = :default_lang AND id_field = :id')->bindValues(array(':default_lang' => $defaultLangCode, ':id' => $data->id_field))->queryScalar();
        return empty($data->translation) ? $defaultLangTranslation : $data->translation;
    }





	public function actionFieldsAutocomplete() {
		$query = strtolower(addcslashes($_REQUEST['data']['query'], '%_'));

		$criteria = new CDbCriteria(array(
			'condition' => 'LOWER(translation) LIKE :match AND lang_code = :langcode',
			'params' => array(
				':match' => "%$query%",
				':langcode' => 'english',
			),
		));

		$model = CoreUserFieldTranslations::model();

		$fields  = $model->findAll($criteria);
		$result = array(
			'options' => array(),
		);

		if (!empty($fields)) {
			foreach ($fields as $field) {
				$result['options'][] = $field->translation;
			}
		}

		$this->sendJSON($result);
	}

    /**
     * Deleting additional fields
     *
     * @param integer $id
     * @return void
     */
	public function actionDeleteField($id)
    {
        $field = CoreUserField::model()->findByPk($id);
		if (empty($field)) {
			$this->sendJSON(array());
		}

		if (!empty($_POST['CoreUserField'])) {
			if ($_POST['CoreUserField']['confirm']) {
                // delete field from attendance sheets
				Yii::app()->db
                    ->createCommand('UPDATE ' . AttendanceSheetMainField::model()->tableName()
                        . ' SET fieldName1 = CASE WHEN fieldName1=:name THEN 0 ELSE fieldName1 END,'
                        . ' fieldName2 = CASE WHEN fieldName2=:name THEN 0 ELSE fieldName2 END,'
                        . ' fieldName3 = CASE WHEN fieldName3=:name THEN 0 ELSE fieldName3 END,'
                        . ' fieldName4 = CASE WHEN fieldName4=:name THEN 0 ELSE fieldName4 END,'
                        . ' fieldName5 = CASE WHEN fieldName5=:name THEN 0 ELSE fieldName5 END')->execute(array(':name' => $id)
                    )
                ;

				// clear from all PU profiles this field
				$puProfilesWithFieldAsAdditional = Yii::app()->db->createCommand()
                    ->select('idst, additional_fields')
                    ->from(CoreGroup::model()->tableName())
                    ->where(array('LIKE', 'additional_fields', '%' . $field->getFieldId() . '%'))
                    ->queryAll()
                ;
				if(!empty($puProfilesWithFieldAsAdditional)) {
					foreach ($puProfilesWithFieldAsAdditional as $puProfile) {
						$additional_fields = explode(',', $puProfile['additional_fields']);
						$additional_fields = array_diff($additional_fields, array($field->getFieldId()));
						$additional_fields = implode(',', $additional_fields);
						Yii::app()->db->createCommand()->update(CoreGroup::model()->tableName(),
								array('additional_fields' => $additional_fields), 'idst = :id', array(':id' => $puProfile['idst']));
					}
				}

				// Deleting the related entries that would not be cascade removed via foreign keys
				$coreGroupFields = CoreGroupFields::model()->deleteAllByAttributes(array('id_field' => $field->getFieldId()));
				$coreGroupAssignRules = CoreGroupAssignRules::model()->deleteAllByAttributes(array('idFieldCommon' => $field->getFieldId()));

                // Deleting the entry, invoking "afterDelete", effectively removing the
                // column for user entry on the related table
                $field->delete();

				$this->sendJSON(array());
			}
		}

		// Showing a modal for conformation
		if (Yii::app()->request->isAjaxRequest) {
			$content = $this->renderPartial('deleteModal', array(
				'model' => $field,
			), true);
			$this->sendJSON(array(
				'html' => $content,
				'modalTitle' => Yii::t('standard', '_DEL'),
				'confirmBtnTitle' => Yii::t('standard', '_CONFIRM'),
				'cancelBtnTitle' => Yii::t('standard', '_CANCEL'),
			));
		}

		Yii:app()->end();
	}

	public function actionCreateFieldModal($type = 'default')
    {
		$field = new CoreUserField();
        $field->lang_code = Settings::get('default_language', 'english');

		if (!empty($_REQUEST['fieldType'])) {
			$type = $_REQUEST['fieldType'];
		}

		if (!empty($_POST['CoreUserField']) && !empty($_POST['CoreUserField']['translation'][$field->lang_code])) {
			$baseField = new CoreUserField();
			$baseField->type = $_POST['CoreUserField']['type_field'];
            $baseField->mandatory = $_POST['CoreUserField']['mandatory'];
			$baseField->invisible_to_user = $_POST['CoreUserField']['invisible_to_user'];
			$baseField->sequence = CoreUserField::model()->getNextSequence();

			// No idea why this was set. Long, long time ago (and translation in default language not saved?!?!)
			// unset($_POST['CoreUserField']['translation'][$field->lang_code]);

			$success = false;
			if ($baseField->validate()) {
				$transaction = Yii::app()->db->beginTransaction();

				$baseField->save();

				if ($baseField->save()) {
					if(empty($_POST['CoreUserField']['translation']))
						$success = true;
					else {
						foreach ($_POST['CoreUserField']['translation'] as $langCode => $translation) {
                            $fieldTranslation = new CoreUserFieldTranslations;
                            $fieldTranslation->id_field = $baseField->getFieldId();
                            $fieldTranslation->lang_code = $langCode;
                            $fieldTranslation->translation = trim(Yii::app()->htmlpurifier->purify($translation));

							$success = $fieldTranslation->save();
							if (!$success) {
								break;
							}
						}
					}
				}

				if ($success) {
					$transaction->commit();
				} else {
					$transaction->rollback();
				}
			}

			$this->sendJSON(array(
				'success' => $success,
			));
		}

		$field->type = $type;
		$activeLanguagesList = CoreLangLanguage::getActiveLanguages();
		$lang_code = Settings::get('default_language', 'english');
		$activeLanguagesList[$lang_code] = $activeLanguagesList[$lang_code]  . " (".Yii::t('standard', '_DEFAULT_LANGUAGE').")";
		$html = $this->renderPartial('createModal',
			array(
				'model' => $field,
				'activeLanguagesList' => $activeLanguagesList
			), true);

		$this->sendJSON(array(
			'html' => $html,
		));
	}

	public function actionEditField($id) {
		$lang_code = Settings::get('default_language', 'english');
		$activeLanguagesList = CoreLangLanguage::getActiveLanguages();
		$activeLanguagesList[$lang_code] = $activeLanguagesList[$lang_code]  . " (".Yii::t('standard', '_DEFAULT_LANGUAGE').")";

		$baseField = CoreUserField::model()->with('translations')->findByAttributes(array('id_field' => $id));
        $baseField->lang_code = $baseField->translationEntry->lang_code;

		$translationsList = CHtml::listData($baseField->translations, 'lang_code', 'translation');

        /**
         * @todo This piece of.. logic seems unnecessary - remove if so
         */
		if (!$translationsList) {
			$baseField = CoreUserField::model()->with('translations')->findByAttributes(array('id_field' => $baseField->getFieldId()));
			$translationsList = CHtml::listData($baseField->translations, 'lang_code', 'translation');
		}

		if (!empty($_POST['CoreUserField'])){

			$platformDefaultLang = Settings::get('default_language');

			if(empty($_POST['CoreUserField']['translation'][$platformDefaultLang])){

				$baseField->addError('translation', Yii::t('user_management', '_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED'));

			}else{
				$success = false;
				$transaction = $transaction = Yii::app()->db->beginTransaction();

				foreach ($activeLanguagesList as $key => $lang) {
					$translation = CoreUserFieldTranslations::model()->findByAttributes(array('id_field' => $baseField->getFieldId(), 'lang_code' => $key));

					if (empty($translation)) {
						$translation = new CoreUserFieldTranslations();
						$translation->lang_code = $key;
						$translation->id_field = $baseField->getFieldId();
					}

					$translation->translation = trim(Yii::app()->htmlpurifier->purify($_POST['CoreUserField']['translation'][$key]));
					$success = $translation->save();

					if (!$success) {
						Yii::log("Error messages:" . var_export($translation->getErrors(),true), CLogger::LEVEL_ERROR);
						$result['success'] = false;
						break;
					}
				}
				// update mandatory and invisible_to_user for all languages including not active languages
				CoreUserField::model()->updateAll(array('mandatory' => $_POST['CoreUserField']['mandatory'], 'invisible_to_user' => $_POST['CoreUserField']['invisible_to_user']),
					'id_field = :id_field', array(':id_field' => $baseField->id_field));

				if ($success) {
					$transaction->commit();
				} else {
					$transaction->rollback();
				}

				$this->sendJSON(array(
					'success' => $success,
				));
			}
		}

		$html = $this->renderPartial('editModal',
			array(
				'model' => $baseField,
				'activeLanguagesList' => $activeLanguagesList,
				'translationsList' => $translationsList
			), true);

		$this->sendJSON(array(
			'html' => $html,
		));
	}

    /**
     * Submitting a request for creating a new dropdown user field
     *
     * @return void
     */
	public function actionCreateDropdown()
    {
		// For Dropdowns we send all Elem<==>Language pairs
		// as a single POST field because otherwise we easily reach
		// the 'max_input_vars' server setting
		$serializedDropdownValues = array();
		if(isset($_POST['dropdownElemsJson']))
			$serializedDropdownValues = CJSON::decode($_POST['dropdownElemsJson'], true);

		if($serializedDropdownValues && !empty($serializedDropdownValues))
			$_POST = array_merge_recursive($_POST, $serializedDropdownValues);

		if (!empty($_POST['CoreUserField']) && !empty($_POST['CoreUserField']['translation'][CoreUser::getDefaultLangCode()])) {
            $fieldData = $_POST['CoreUserField'];
			$translationList = $fieldData['translation'];
            $languageKeys = array_keys($fieldData['translation']);
            $optionDetails = json_decode($_POST['dropdownElemsJson'], true);
            $optionDetails = current(array_shift($optionDetails));

            $db = Yii::app()->db;
            if (($transaction = $db->getCurrentTransaction()) === NULL) {
                $transaction = $db->beginTransaction();
            }
			$success = false;

            // Creating the field entry itself
            $field = new CoreUserField;
            $field->type      = CoreUserField::TYPE_DROPDOWN;
            $field->sequence  = CoreUserField::getNextSequence();
            $field->mandatory = $fieldData['mandatory'];
            $field->lang_code = $fieldData['lang_code'];
            $field->invisible_to_user = $fieldData['invisible_to_user'];

            $success = $field->save();

            if ($success === true) {
                // Inserting the option entries for the dropdown
                $options = [];
                $firstTranslation = current($optionDetails);
                $optionsCount = count($firstTranslation['options']['translation']) - 1;

                for ($i=0; $i<$optionsCount; $i++) {
                    $option = new CoreUserFieldDropdown;
                    $option->id_field = $field->getFieldId();
                    $option->sequence = ($i+1);
                    $option->save();
                    $options[$i] = $option;
                }

                // Inserting language related entries
                foreach ($languageKeys as $locale) {
                    // Inserting the translation entries of the field
                    $translation = new CoreUserFieldTranslations;
                    $translation->lang_code   = $locale;
                    $translation->id_field    = $field->getFieldId();
                    $translation->translation = $translationList[$locale]['field'];
                    $translation->save();

                    // Inserting option translation values
                    foreach ($options as $optionIndex => $option) {
                        $optionTranslation = new CoreUserFieldDropdownTranslations;
                        $optionTranslation->id_option   = $option->id_option;
                        $optionTranslation->lang_code   = $locale;
                        $optionTranslation->translation = $optionDetails[$locale]['options']['translation'][$optionIndex];
                        $optionTranslation->save();
                    }
                }
            }

			if ($success) {
				$transaction->commit();
			} else {
				$transaction->rollback();
			}

			$this->sendJSON(array(
				'success' => $success,
			));
		}
	}

    /**
     * Edit interface for dropdown fields
     *
     * @param integer $id
     * @return void
     */
	public function actionEditDropdown($id)
    {
        $lang_code = Settings::get('default_language', 'english');
		$activeLanguagesList = CoreLangLanguage::getActiveLanguages();
		$activeLanguagesList[$lang_code] = $activeLanguagesList[$lang_code]  . " (".Yii::t('standard', '_DEFAULT_LANGUAGE').")";

        /** @var $field CoreUserField */
		$field = CoreUserField::model();

        if (Yii::app()->request->isPostRequest === false) {
            $field = $field->findByPk($id);

            // Rendering the dropdown edit modal
            $translationList = [];
            foreach ($field->fieldTranslations as $transl) {
                $translationList[$transl->lang_code] = [
                    'field'   => $transl->translation ?: '',
                    'options' => [],
                ];
            }
            foreach ($field->dropdowns as $dropDown) {
                foreach ($dropDown->translations as $ddTr) {
                    $translationList[$ddTr->lang_code]['options'][] = [
                        'idSon'         => $ddTr->id_option,
                        'translation'   => $ddTr->translation,
                        'id_common_son' => $ddTr->id_option,
                    ];
                }
            }

            // Rendering the dropdown edit modal content
            $html = $this->renderPartial('editModal',
                array(
                    'model'               => $field,
                    'activeLanguagesList' => $activeLanguagesList,
                    'translationsList'    => $translationList
                ), true);

            $this->sendJSON(array(
                'html' => $html,
            ));
        } else {
            // Saving the submitted changes
            $fieldDetails  = Yii::app()->request->getPost('CoreUserField');
            $optionDetails = current(current(json_decode(Yii::app()->request->getPost('dropdownElemsJson'), true)));

            // Loading the field from the database with it's needed relations
            $field = $field->with('fieldTranslations', 'dropdowns', 'dropdowns.translations')->findByPk($id);
            $languageLocales = array_keys($activeLanguagesList);

            // Fetching dropdown details, storing those as an array, because we'll want to add some new items if need be
            // and this could cause problems since it's a relationship, not a real property of the field
            $dropdowns = $field->dropdowns;
            // Reindexing the dropdowns collection using option's id
            $dropdowns = array_combine(array_map(function($dropdown) { return $dropdown->id_option; }, $dropdowns), $dropdowns);

            // Fetching dropdown option translation details
            $dropdownTranslations = [];
            foreach ($dropdowns as $dropdownId => $dropdown) {
                $dropdownTranslations[$dropdownId] = [];
                foreach ($dropdown->translations as $translation) {
                    $dropdownTranslations[$dropdownId][$translation->lang_code] = $translation;
                }
            }

            // Reindex the the field translations using language code
            $fieldTranslations = array_combine(array_map(function($transl) { return $transl->lang_code; }, $field->fieldTranslations), $field->fieldTranslations);

            // Fetching a set of details from the input data for reference to how many dropdown options were submitted
            $optionDetailsInfo = current(current($optionDetails));
            $optionsCount = count($optionDetailsInfo['translation']) - 1;

            // Calculating the highest sequence of the field's dropdowns
            $dropdownMaxSequence = array_map(function($dropdown) { return intval($dropdown->sequence); }, $dropdowns);
            krsort($dropdownMaxSequence);
            $dropdownMaxSequence = array_shift($dropdownMaxSequence);

            $db = Yii::app()->db;
            if (($transaction = $db->getCurrentTransaction()) === NULL) {
                $transaction = $db->beginTransaction();
            }

            $success = true;

                // Updating the fields direct properties
            $field->mandatory = $fieldDetails['mandatory'];
            $field->invisible_to_user = $fieldDetails['invisible_to_user'];
            if ($field->save() === false) {
                $success = false;
            }

            // Persisting the dropdown entries
            for ($i=0; $i<$optionsCount; $i++) {
                $optionId = isset($optionDetailsInfo['idSon'][$i]) ? $optionDetailsInfo['idSon'][$i] : null;
                if ($optionId === null) {
                    ++$dropdownMaxSequence;
                    $dropdown = new CoreUserFieldDropdown;
                    $dropdown->id_field = $field->getFieldId();
                    $dropdown->sequence = $dropdownMaxSequence;

                    if ($dropdown->save() === false) {
                        $success = false;
                    }

                    $dropdowns[$dropdown->id_option] = $dropdown;
                }
            }

            // Persisting translation entries
            foreach ($languageLocales as $locale) {
                // Persisting field translation entries
                if (isset($fieldTranslations[$locale]) === false || isset($fieldDetails['translation'][$locale]) === false) {
                    // Creating a new translation entry if for some reason
                    // there is no value provided for the current language
                    $fallbackTranslation = new CoreUserFieldTranslations;
                    $fieldTranslations[$locale] = $fallbackTranslation;
                    $value = '';
                    flush($fallbackTranslation);
                } else {
                    $value = $fieldDetails['translation'][$locale];
                }

                $fieldTranslations[$locale]->id_field    = $field->getFieldId();
                $fieldTranslations[$locale]->lang_code   = $locale;
                $fieldTranslations[$locale]->translation = $value['field'];

                if ($fieldTranslations[$locale]->save() === false) {
                    $success = false;
                }

                // Persisting dropdown translation entries
                $optionsTr = $optionDetails[$locale]['options'];
                array_pop($optionsTr['translation']);

                foreach ($optionsTr['translation'] as $index => $value) {
                    $optionId = null;
                    if (isset($optionsTr['idSon'][$index])) {
                        $optionId = $optionsTr['idSon'][$index];
                        $optionTranslation = $dropdownTranslations[$optionId][$locale];
                    } else {
                        $optionId = array_keys($dropdowns);
                        $optionId = $optionId[$index];
                        $optionTranslation = new CoreUserFieldDropdownTranslations;
                        $optionTranslation->id_option = $optionId;
                        $optionTranslation->lang_code = $locale;
                    }

                    $optionTranslation->translation = isset($optionsTr['translation'][$index]) ? $optionsTr['translation'][$index] : '';
                    if ($optionTranslation->save() === false) {
                        $success = false;
                    }
                }
            }

            if ($success) {
                $transaction->commit();
            } else {
                $transaction->rollback();
            }

            $this->sendJSON(array(
                'success' => $success,
            ));
        }
	}

	public function actionOrderFields() {
		$ids = $_POST['ids'];
		$command = Yii::app()->db->createCommand();

		if (!empty($ids)) {
			foreach ($ids as $weight => $id)
			{
                ++$weight;
				$tmp = CoreUserField::model()->findByPk($id);
				$fieldId = $tmp->id_field;

                $command->update(CoreUserField::model()->tableName(), array('sequence' => $weight), 'id_field = :id', array(':id' => $fieldId));
			}

			$this->sendJSON(array('success' => true));
		} else {
			$this->sendJSON(array('success' => false));
		}
	}



	//this is just a dummy function for the moment
	public function actionSaveDropdownSingleLanguage() {
		$this->sendJSON(array('success' => true));
	}

}
