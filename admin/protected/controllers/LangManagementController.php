<?php

class LangManagementController extends AdminController {

	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}


	/**
	 * Returns list of allowed actions.
	 * @return array actions configuration.
	 */
	public function accessRules() {
		$res = array();

		$res[] = array(
			'allow',
			'roles' => array('/framework/admin/lang/view'),
			'actions' => array(
				'index',
			),
		);

		$res[] = array(
			'allow',
			'roles' => array('/framework/admin/lang/mod'),
			'actions' => array(
				'updateLang',
				'changeStatus',
				'exportLang',
				'translateLang',
				'translateLangEdit',
				'importLang',
			),
		);

		$res[] = array(
			'deny',
			// deny all
			'users' => array('*'),
			'deniedCallback' => array($this,'adminHomepageRedirect'),
		);

		return $res;
	}


	/**
	 * Delete cache files (assets, .js dictionaries, database .bin etc.)
	 * TODO: cache clearing can be done in a more optimal way, now too many files are being removed !!
	 * @param $langcode
	 * @param $module
	 */
	protected function _clearLangCache($langcode, $module) {

		//1) clear cached messages
		Yii::app()->messages->resetCache($module);

		//2) clear DB cache
		//TODO: this will flush ALL cached data !!! is there a way to targeting only translations related files?
		Yii::app()->cache->flush();

		//3) clear js dictionaries.
		//NOTE: since at the moment there is no way to detect dictionary files based on language/category then we have to delete them all
		JsTrans::flushDictionaries();

		//4) invalidate cache between dockers
		Yii::app()->cache->invalidateCacheFiles();
	}



	/**
	 * (non-PHPdoc)
	 * @see Controller::init()
	 */
	public function init() {

		return parent::init();
	}


	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
		$langModel = new CoreLangLanguage();

		if (Yii::app()->request->isAjaxRequest && isset($_REQUEST['CoreLangLanguage'])) {
			$langModel->attributes = $_REQUEST['CoreLangLanguage'];
		}

		$this->render('index', array(
			'langModel' => $langModel,
		));
	}


	/**
	 * Change status of language
	 *
	 * @param strung $langcode
	 */
	public function actionChangeStatus($langcode) {
		$coreLangLanguage = CoreLangLanguage::model()->findByPk($langcode);
		if (!$coreLangLanguage) {
			$this->sendJSON(array());
		}

		$status = $coreLangLanguage->lang_active;

		if ($status != CoreLangLanguage::STATUS_VALID) {
			$coreLangLanguage->scenario = 'activate';
			$coreLangLanguage->activate();

			//insert records in core_field for this language if not exist
			CoreUserFieldTranslations::insertMissingTranslationForLanguage($coreLangLanguage->lang_code);

			//insert records in core_field_son for this language if not exist
            CoreUserFieldDropdownTranslations::insertMissingTranslationForLanguage($coreLangLanguage->lang_code);

			//insert labels for this language if not exist
			if(PluginManager::isPluginActive('LabelApp')) {
				LearningLabel::model()->insertLabelsForLanguage($coreLangLanguage->lang_code);
			}
		} else {
			$coreLangLanguage->scenario = 'deactivate';
			$coreLangLanguage->deactivate();
		}

		$this->sendJSON(array());
		Yii::app()->end();
	}


	/**
	 * Update information about selected language
	 *
	 * @param string $langcode
	 */
	public function actionUpdateLang($langcode) {
		$coreLangLanguage = CoreLangLanguage::model()->findByPk($langcode);
		$coreLangLanguageActive = $coreLangLanguage->lang_active;
		if (!$coreLangLanguage) {
			$this->sendJSON(array());
		}

		$coreLangLanguage->scenario = 'update';
		$coreLangLanguage->isDefaultLanguage();

		if (!empty($_POST['CoreLangLanguage'])) {

			if(!$coreLangLanguageActive){
				$defaultLanguageError = array();
				$defaultLanguageError['data']['error'] = Yii::t('admin_lang', 'You cannot mark a language as the default one, when it is disabled.');
				echo CJSON::encode($defaultLanguageError);
				Yii::app()->end();
			}

			$coreLangLanguage->attributes = $_POST['CoreLangLanguage'];
			if ($coreLangLanguage->validate()) {
				$coreLangLanguage->save();
				$this->sendJSON(array());
			}
		}
		if (Yii::app()->request->isAjaxRequest) {
			$content = $this->renderPartial('update', array(
				'model' => $coreLangLanguage,
			), true);
			$this->sendJSON(array(
				'html' => $content,
			));
		}
		Yii::app()->end();
	}

	public function actionExportLang() {

		$lang_to_export = Yii::app()->request->getParam("langcode", Yii::app()->getLanguage());

		$convertedCode = Lang::getBrowserCodeByCode($lang_to_export);
		if (!$convertedCode) {
			//TODO: how to handle this situation ?
		}

		$xliff = '<?xml version="1.0" encoding="utf-8"?>' . PHP_EOL
			. '<!DOCTYPE xliff PUBLIC "-//XLIFF//DTD XLIFF//EN" "http://www.oasis-open.org/committees/xliff/documents/xliff.dtd">' . PHP_EOL
			. '<xliff version="1.1">' . PHP_EOL
			. '<file datatype="html" source-language="en" target-language="' . $convertedCode . '" date="' . date('Y-m-d') . '">' . PHP_EOL
			. '<body>' . PHP_EOL;

		$translations = CoreLangText::model()->getAllTranslations($lang_to_export);

		foreach ($translations['english'] as $k => $phrase) {

			$xliff .= "\t" . '<trans-unit module="' . $phrase['text_module'] . '" key="' . htmlspecialchars( $phrase['text_key'], ENT_COMPAT, 'UTF-8' ) . '">' . PHP_EOL
				. "\t\t" . '<source><![CDATA[' . htmlspecialchars($translations['english'][$k]['translation_text'], ENT_COMPAT, 'UTF-8' ) . ']]></source>' . PHP_EOL
				. "\t\t" . '<target><![CDATA[' . htmlspecialchars($translations[$lang_to_export][$k]['translation_text'], ENT_COMPAT, 'UTF-8' ) . ']]></target>' . PHP_EOL
				. "\t" . '</trans-unit>' . PHP_EOL;
		}

		$xliff .= '</body>' . PHP_EOL
			. '</file>' . PHP_EOL
			. '</xliff>';

		Yii::app()->request->sendFile('xliff-'.$lang_to_export.'.xliff', $xliff, NULL, false);
		Yii::app()->end();
	}




	public function actionImportLang() {
		$model = new LanguageImportForm();
		$error = false;

		if(isset($_POST['LanguageImportForm'])) {
			try {
				$model->attributes = $_POST['LanguageImportForm'];
				$model->file = CUploadedFile::getInstance($model, 'file');
				if ($model->validate()) {
					if ($model->importLanguage()) {
						echo '<a class="auto-close"></a>';
						echo '<script type="text/javascript">Docebo.Feedback.show("success", '.CJSON::encode(Yii::t('standard', '_OPERATION_SUCCESSFUL')).');</script>';
						Yii::app()->end();
					}
				}
			} catch (Exception $e) {
				$error = $e->getMessage();
			}
		}

		$this->renderPartial('_importLang', array(
			'model' => $model,
			'error' => $error
		));
	}


	/**
	 * User interface for translate of texts
	 *
	 * @param string $module
	 * @param string $langcode
	 * @param null $comparison
	 * @param null $search
	 * @param bool $lang_diff
	 */
	public function actionTranslateLang($module = 'all', $langcode = 'all', $comparison = null, $search = null, $lang_diff = false) {

		$langModel = new CoreLangText;
		$langFilter = new LangManagementFilter;
		$langFilter->module = isset($_REQUEST['LangManagementFilter']['module']) ? $_REQUEST['LangManagementFilter']['module'] : $module;
		$langFilter->langcode = isset($_REQUEST['LangManagementFilter']['langcode']) ? $_REQUEST['LangManagementFilter']['langcode'] : $langcode;
		$langFilter->lang_diff = isset($_REQUEST['LangManagementFilter']['lang_diff']) ? $_REQUEST['LangManagementFilter']['lang_diff'] : $lang_diff;

		if (Yii::app()->request->isAjaxRequest && isset($_REQUEST['CoreLangLanguage'])) {
			$langModel->attributes = $_REQUEST['CoreLangLanguage'];
		}

		$module_list = $this->getModuleList();
		array_unshift($module_list, Yii::t('standard', '_ALL'));

		$language_list_diff = $language_list = $this->getLangCodeList();
		array_unshift($language_list_diff, Yii::t('standard', '_NONE'));

		$langcode = isset($_REQUEST['LangManagementFilter']['langcode']) ? $_REQUEST['LangManagementFilter']['langcode'] : $langcode;
		$lang_diff = isset($_REQUEST['LangManagementFilter']['lang_diff']) ? $_REQUEST['LangManagementFilter']['lang_diff'] : $lang_diff;

		$this->render('translate', array(
			'langcode' => $langcode,
			'lang_diff' => $lang_diff,
			'model' => $langModel,
			'filter' => $langFilter,
			'module_list' => $module_list,
			'language_list' => $language_list,
			'language_list_diff' => $language_list_diff,
		));
	}


	/**
	 * Create or edit individual translation of text
	 *
	 * @param $langcode
	 * @param $module
	 * @param $id
	 * @param $text_key
	 */
	public function actionTranslateLangEdit($langcode, $module, $id, $text_key)
	{
		$coreLangTranslation = false;
		if ($id) {
			$coreLangTranslation = CoreLangTranslation::model()->findByAttributes(array(
				'id_text' => $id,
				'lang_code' => $langcode,
			));
		}

		if (!$coreLangTranslation) {
			$coreLangTranslation = new CoreLangTranslation('insert');
		} else {
			$coreLangTranslation->scenario = 'update';
		}

		if (!$coreLangTranslation->translation_text) {
			$empty = true;
			if (!$text_key) {
				$coreLangText = CoreLangText::model()->findByAttributes(array(
					'id_text' => $id,
				));
				$text_key = $coreLangText->text_key;
			}

			if ($text_key) {
				$arr = CoreLangText::get_module_from_file($module, $langcode);
				$text_key_amp = str_replace('&', '&amp;', $text_key);

				foreach ($arr as $key => $val) {
					$key = trim($key);
					if ($key == $text_key_amp || $key == $text_key) {
						$coreLangTranslation->translation_text = $val;
						break;
					}
				}
			} else {

			}
		} else {
			$empty = false;
		}

		if (!empty($_POST['CoreLangTranslation'])) {

			// empty current category cache
			// Yii::app()->language->resetCache() is buggy for some reason,
			// so we replicate the same functionality here. Besides,
			// we know the language being edited, so no need to refresh the cache
			// of the category for ALL languages
			if ($text_key) {
				$key = 'messages-' . Lang::getBrowserCodeByCode($langcode) . '-' . $module;
				if (($cache = Yii::app()->cache) !== null) {
					if (($data = $cache->get($key)) !== false) {
						$cache->delete($key);
					}
				}
			}

			$old_text = $coreLangTranslation->translation_text;
			$coreLangTranslation->attributes = $_POST['CoreLangTranslation'];

			if ($empty && $coreLangTranslation->translation_text == $old_text) {
				$coreLangTranslation = CoreLangTranslation::model()->findByAttributes(array(
					'id_text' => $id,
					'lang_code' => $langcode,
				));

				if ($coreLangTranslation) {
					$coreLangTranslation->delete();
				}

				$this->_clearLangCache($langcode, $module);
				$this->sendJSON(array());
				Yii::app()->end();
			} elseif ($coreLangTranslation->scenario == 'insert' && $coreLangTranslation->translation_text) {
				if (!$coreLangText) {
					$coreLangText = CoreLangText::model()->findByAttributes(array(
						'text_key' => $text_key,
						'text_module' => $module,
					));
					if (!$coreLangText) {
						$coreLangText = new CoreLangText;
						$coreLangText->text_key = $text_key;
						$coreLangText->text_module = $module;
						$coreLangText->text_attributes = $coreLangText->context = '';
						$coreLangText->save(false);
					}

					$id = $coreLangText->getPrimaryKey();
				}
				$sql = "INSERT INTO `" . CoreLangTranslation::model()->tableName() . "` "
					." (id_text, lang_code, translation_text, translation_status, save_date) "
					." VALUES (:id_text, :lang, :translation_text, :status, :save_date)";

				$values = array(
					':id_text' => $id,
					':lang' => $langcode,
					':translation_text' => CoreLangTranslation::prepareTranslationText($coreLangTranslation->translation_text),
					':status' => '',
					':save_date' => Yii::app()->localtime->getUTCNow(),
				);
				$result = Yii::app()->db->createCommand($sql)->bindValues($values)->execute();

				$this->_clearLangCache($langcode, $module);
				$this->sendJSON(array());
				Yii::app()->end();
			} elseif ($coreLangTranslation->scenario != 'insert' && !$coreLangTranslation->translation_text) {
				$coreLangTranslation->delete();

				$this->_clearLangCache($langcode, $module);
				$this->sendJSON(array());
				Yii::app()->end();
			} elseif ($coreLangTranslation->scenario != 'insert' && $coreLangTranslation->validate()) {
				$coreLangTranslation->save();

				$this->_clearLangCache($langcode, $module);
				$this->sendJSON(array());
				Yii::app()->end();
			}
		}

		$content = $this->renderPartial('translate-update', array(
			'model' => $coreLangTranslation,
		), true);

		$this->sendJSON(array(
			'html' => $content,
		));
		Yii::app()->end();
	}


	/**
	 * Return all the list of modules transleated
	 *
	 * @return array
	 */
	public function getModuleList() {
		return CoreLangText::model()->getModuleList();
	}


	/**
	 * Return the list of all the languages only with the code
	 *
	 * @return array
	 */
	public function getLangCodeList() {
		$result = array();

		$criteria = new CDbCriteria;
		$criteria->select = 't.lang_code';
		$criteria->condition = '1';
		$criteria->order = 't.lang_code ASC';

		$res = CoreLangLanguage::model()->findAll($criteria);
		foreach($res as $row) {
			$result[$row->lang_code] = ucfirst($row->lang_code);
		}
		return $result;
	}


	public function getListOfComparisons() {
		return array();
	}

}
