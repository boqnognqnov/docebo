<?php

	class SiteController extends AdminController {

		public $layout = '//layouts/adm';


			public function filters() {
				return array(
					'accessControl', // perform access control for CRUD operations
				);
			}

		public function accessRules() {
			$res = array();

			$res[] = array(
				'allow',
				'roles' => array('/framework/admin/dashboard/view'),
				'actions' => array('error'),
			);

			$res[] = array(
				'allow',
				'users' => array('*'),
				'actions' => array('login'),
			);

			$res[] = array(
				'allow',
				'users' => array('@'),
				'actions' => array('index', 'logout'),
			);

			$res[] = array(
				'deny',
				// deny all
				'users' => array('*'),
				'deniedCallback' => array($this, 'adminHomepageRedirect'),
			);

			return $res;
		}

		/**
		 * This is the default 'index' action that is invoked
		 * when an action is not explicitly requested by users.
		 */
		public function actionIndex() {
			$this->redirect(Docebo::createLmsUrl('site/index'));
		}

		public function actionTree() {
			$this->render('tree');
		}

		public function actionAjaxTree($operation = null, $id = null) {
			$allowedOperations = array(
				'get_root',
				'get_children',
			);

			if (!in_array($operation, $allowedOperations)) {
				throw new CHttpException(400, 'Error operation');
			}

			$data = array();

			switch ($operation) {
				case 'get_root' :
					$rootNode    = array(
						'attr' => array(
							'id' => 'node_' . 0,
							'rel' => 'docebo'
						),
						'data' => 'Docebo',
						//					'state' => 'open',
						'state' => 'closed',
					);
					$parentNodes = CoreOrgChartTree::model()->getParentNodes();
					if ($parentNodes) {
						$rootNode['children'] = $parentNodes;
					}
					$data[] = $rootNode;
					break;
				case 'get_children' :
					$coreOrgChartTree = CoreOrgChartTree::model()->findByPk($id);
					$childrenNodes    = $coreOrgChartTree->with(array(
						'coreOrgChart' => array(
							'on' => 'coreOrgChart.lang_code = :lang',
							'params' => array('lang' => 'english')
						)
					))->children()->findAll();

					if (!empty($childrenNodes)) {
						foreach ($childrenNodes as $child) {
							$data[] = array(
								'attr' => array(
									'id' => 'node_' . $child->idOrg,
									'rel' => 'folder'
								),
								'data' => $child->coreOrgChart->translation,
								'state' => 'open',
							);
						}
					}

					//					pr($coreOrgChartTree->children()->attributes);

					/*if ($id < 0) {
						$data[] = array(
							'attr' => array(
								'id' => 'node_' . 0,
								'rel' => 'docebo'
							),
							'data' => 'Docebo',
							'state' => 'open',
							//	'state' => 'closed',
							'icon' => 'folder',
						);

						$coreOrgChartTree = CoreOrgChartTree::model()->with(array(
							'coreOrgChart' => array(
								'on' => 'coreOrgChart.lang_code = :lang',
								'params' => array('lang' => 'english')
							)
						))->findAllByAttributes(array('idParent' => 0));
						$chidrenData      = array();
						if (!empty($coreOrgChartTree)) {
							foreach ($coreOrgChartTree as $parentNode) {
								//								pr($parentNode->coreOrgChart->translation);
								$chidrenData[] = array(
									'attr' => array(
										'id' => 'node_' . $parentNode->idOrg,
										//									'rel' => 'docebo'
									),
									'data' => $parentNode->coreOrgChart->translation,
									'state' => 'open',
									'icon' => 'folder',
								);
							}

						}
						if (!empty($chidrenData)) {
							$data[0]['children'] = $chidrenData;
						}
						//						pr($data);
						//						pr($CoreOrgChartTree[0]->attributes);

					}*/
					break;
				default:
					pr(1);
					break;
			}

			//			$roots = CoreOrgChartTree::model()->roots();
			//			pr($roots->attributes);
			//			pr($roots[0]->attributes);

			//			$CoreOrgChartTree = new CoreOrgChartTree();
			//			pr($CoreOrgChartTree->roots()->attributes);
			//			$CoreOrgChartTree = CoreOrgChartTree::model()->findAll();
			//			pr($CoreOrgChartTree->roots()->attributes);
			//			pr($CoreOrgChartTree->children()->attributes);
			//			pr(CoreOrgChartTree::model()->children()->attributes);


			//				'[{"attr":{"id":"node_2","rel":"drive"},"data":"Drive","state":"closed"}]'

			echo CJSON::encode($data);
			exit;

			pr($operation, 0);
			pr($id);
			pr($_GET);
			$this->render('tree');
		}

		public function actionTimezone() {
			$this->render('timezone');
		}

		/**
		 * This is the default 'index' action when the user isn't logged in
		 */
		public function actionLogin() {
			$this->redirect(Docebo::createLmsUrl('site/index'));
		}

		public function actionSample() {
			$this->render('sample', array(
				'auth' => Yii::app()->authManager,
			));
		}

		/**
		 * This is the action to handle external exceptions.
		 */
		public function actionError() {
			if ($error = Yii::app()->errorHandler->error) {
				if (Yii::app()->request->isAjaxRequest) {
					echo CHtml::encode($error['message']);
				} else {
					$this->renderPartial('error', $error);
				}
			}
		}

		/**
		 * Logs out the current user and redirect to homepage.
		 */
		public function actionLogout() {
			if (Yii::app()->user->id && !Yii::app()->user->getIsGuest()) {
				LearningTracksession::model()->updateAll(array('active' => 0), 'active = 1 AND idUser = '.Yii::app()->user->id);
			}

			$previouslyLoggedUser = Yii::app()->user->loadUserModel();
			Yii::app()->user->logout();

			// Trigger event for plugins post-logout processing. This call may never return.
			Yii::app()->event->raise("UserLogout", new DEvent($this, array('user' => $previouslyLoggedUser)));

			if(Settings::get('user_logout_redirect') == 'on' && Settings::get('user_logout_redirect_url')){
				$url = Settings::get('user_logout_redirect_url');
				if(strpos($url , 'http') === false)
					$url = 'http://' . $url;

				$this->redirect($url);
			}

			$this->redirect(Docebo::createAbsoluteLmsUrl('site/index'));
		}
	}