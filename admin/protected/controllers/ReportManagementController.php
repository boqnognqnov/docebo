<?php

class ReportManagementController extends AdminController {

	const WIZ_NEXT_REDIRECT = 'redirect';
	const WIZ_NEXT_RENDER = 'render';
	const WIZ_STEP_1 = 'step1';	// report type
	const WIZ_STEP_USERS = 'users';  // users
	const WIZ_STEP_COURSES = 'courses'; // courses
	const WIZ_STEP_STEP_4 = 'step4';  // fields selection and other filtering options
	const WIZ_STEP_STEP_ORDER = 'order';  // fields ordering by
	const WIZ_STEP_STEP_5 = 'step5';  // FINAL

	private $_assetsUrl = null;

	public function init() {


		return parent::init();
	}

	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}

	/**
	 * Returns list of allowed actions.
	 * @return array actions configuration.
	 */
	public function accessRules() {

		$res = array();

		$res[] = array(
			'allow',
			'roles' => array('/lms/admin/report/view'),
			'actions' => array(
				'createUserReport', // Allow the admin(PowerUser) to generate Report trough User module
				'index',
				'downloadPdf',
				'reportAutocomplete',
				'view',
				'export',
				'sendEmail',
				'customView',
				'getStandardActions',
				'exportCustomReport',
				'customReportWizard',
				'progressiveExporter',
				'createCourseReport',
				'expertsAutocomplete',
				'createExpertReport',
				'assetsAutocomplete',
				'channelsAutocomplete',
				'generateExpertReport',
				'generateQuestionsAnswersPdf',
				'generateSharingActivityPdf',
				'skillsAutocomplete',
				'rolesAutocomplete',
			),
		);

		$res[] = array(
			'allow',
			'roles' => array('/lms/admin/report/mod'),
			'actions' => array(
				'createUserReport',
				'editReport',
				'toggleReportPublic',
				'deleteReport',
				'finishCustomReport',
				'axEditSchedule',
				'axReportRecipients',
				'axToggleJobActiveStatus',
				'showAccessFilter',
			),
		);

		$res[] = array(
			'allow',
			'users' => array('@'),
			'actions' => array('userActivity'),
		);

		// allow admins and instructors can see the classroom reports linked from the lms menu
		$course_id = Yii::app()->request->getParam('course_id', 0);
		$res[] = array(
			'allow',
			'expression' => "Yii::app()->user->isGodAdmin ||
                (Yii::app()->user->isPu && CoreUserPuCourse::model()->findByAttributes(array('puser_id' => Yii::app()->user->id,'course_id' => " . $course_id . ")) && Yii::app()->user->checkAccess('/lms/admin/course/view')) ||
                (LearningCourseuser::userLevel(Yii::app()->user->id," . $course_id . ') >= LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR)',
			'actions' => array(
				'classroomCourseReport', 'webinarCourseReport',
			),
		);

		$res[] = array(
			'deny',
			// deny all
			'users' => array('*'),
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);

		return $res;
	}

	/**
	 * NEW Index action, implementing the new Custom Reports Wizard
	 *
	 */
	public function actionIndex() {
        // FancyTree initialization
        Yii::app()->fancytree->init();

		Yii::app()->event->raise('DeleteNotActiveCodes', new DEvent($this));
		DatePickerHelper::registerAssets();

		$assetsUrl = $this->getAdminAssetsUrl();

		Yii::app()->clientScript->registerScriptFile($assetsUrl . '/charts/aight.min.js');
		Yii::app()->clientScript->registerScriptFile($assetsUrl . '/charts/jquery.tipsy.min.js');
		Yii::app()->clientScript->registerCssFile($assetsUrl . '/charts/charts.css');
		Yii::app()->clientScript->registerScriptFile($assetsUrl . '/print/jquery.print-preview.js');
		Yii::app()->clientScript->registerCssFile($assetsUrl . '/print/css/print-preview.css');
		Yii::app()->clientScript->registerCssFile(Yii::app()->theme->getBaseUrl() . '/css/classroom.css');
		Yii::app()->clientScript->registerCssFile(
				Yii::app()->clientScript->getCoreScriptUrl() .
				'/jui/css/base/jquery-ui.css'
		);

		$updateAuthors = Yii::app()->request->getParam('updateAuthors', false);


		if (!Yii::app()->request->isAjaxRequest) {

			// Register report manager JS
			Yii::app()->clientScript->registerScriptFile($assetsUrl . '/reportmanager.js');

			// Register jWizard JS
			UsersSelector::registerClientScripts();
			$cs = Yii::app()->getClientScript();
			$themeUrl = Yii::app()->theme->baseUrl;
			$cs->registerScriptFile($themeUrl . '/js/jwizard.js');

			// FCBK
			$cs->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
			$cs->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');
		}

		if (Yii::app()->user->getIsPu()) {
			// Power users can only see authors of public reports
			$authors = CoreUser::getPublicReportAuthors();
			$users = array();
			if ($authors) {
				foreach ($authors as $author) {
					$users[$author->idst] = Yii::app()->user->getRelativeUsername($author->userid);
				}
			}
		} else {
			$users = CoreUser::getUsersListByRole('/lms/admin/report/view');
		}

		$reportFilterModel = new LearningReportFilter();

		// Report filtering request might be coming
		if (isset($_REQUEST['LearningReportFilter'])) {
			$reportFilterModel->attributes = $_REQUEST['LearningReportFilter'];
		}

		if($updateAuthors && Yii::app()->request->isAjaxRequest){
			$html = CHtml::activeDropDownList($reportFilterModel, 'author', array(), array('empty' => '(' . Yii::t('standard', '_ALL') . ')', 'class' => 'ajaxChangeSubmit' , 'id' => 'authorsDropDown'));
			if(isset($users) && $users){
				$html = CHtml::activeDropDownList($reportFilterModel, 'author', $users, array('empty' => '(' . Yii::t('standard', '_ALL') . ')', 'class' => 'ajaxChangeSubmit' , 'id' => 'authorsDropDown'));;
			}

			$this->sendJSON(array(
				'html' => $html,
			));

			Yii::app()->end();
		}

		$this->render('index', array(
			'reportFilterModel' => $reportFilterModel,
			'users' => $users,
			'assetsUrl' => $assetsUrl,
		));
	}

	public function actionCreateUserReport() {

        $id = Yii::app()->request->getParam('id');
        $idst = Yii::app()->request->getParam('idst');
        $createPdf = Yii::app()->request->getParam('createPdf', false);

//		if (Yii::app()->request->isAjaxRequest) {

			$with = array(
				'groups'
			);

            $findUserParams = (intval($idst) > 0)
                ? array('idst' => $idst)
                : array('userid' => Yii::app()->user->getAbsoluteUsername($id));

			$userModel = CoreUser::model()->with($with)->findByAttributes($findUserParams);
			if(!$userModel || (Yii::app()->user->getIsPu() && !CoreUserPU::isAssignedToPU($userModel->idst, 'user'))) {
				$content = $this->renderPartial('createUserReportNotFound', array('username' => $id), true, true);
				$this->sendJSON(array('html' => $content));
			}

			$std_fields = CoreUserField::model()->language(Settings::get('default_language', 'english'), true)->findAll('invisible_to_user=0');
			$std_fields_translation = CHtml::listData($std_fields, 'id_field','translation');
			$fields = CoreUserField::model()->language()->findAll('invisible_to_user=0');
			$additionalFieldsT = CHtml::listData($fields, 'id_field', 'translation');
			if (Settings::get('use_node_fields_visibility', 'off') == 'on') {
				$userOrgChartGroups = $userModel->getOrgChartGroupsList();
				$nodes = array();
				if (is_array($userOrgChartGroups) && (count($userOrgChartGroups) > 0)) {
					$orgRootNode = CoreOrgChartTree::getOrgRootNode();
					foreach ($userOrgChartGroups as $idOrg) {
						if ($idOrg != $orgRootNode->idOrg) {
							$nodes[] = $idOrg;
						}
					}
				}
				$visibleIdFields = $userModel->getVisibleAdditionalFieldsByBranch($nodes);
			} else {
				$visibleIdFields = array_keys($additionalFieldsT);
			}

            $userFields = $userModel->getAdditionalFields(true);
			if (!empty($userFields)) {
				foreach ($userFields as $key => $field) {
					if (in_array($field->id_field, $visibleIdFields)) {
						$translation = $field->translation;
						if(!$translation)
							$translation = $std_fields_translation[$field->id_field];

						$additionalFields[$translation] = $field->renderValue($field->userEntry, $field->userEntry);
					}
				}
			}

			if (empty($userModel)) {
				$content = $this->renderPartial('createUserReportNotFound', array('username' => $id), true, true);
				$this->sendJSON(array('html' => $content));
			}

			$userGroups = array();
			if (!empty($userModel->groups)) {
				foreach ($userModel->groups as $group) {
					if ($group->hidden == 'false') {
						$userGroups[] = CoreGroup::model()->relativeId($group->groupid);
					}
				}
			}

			$totalTime = LearningTracksession::model()->getUserTotalTime($userModel->idst);

			$user_subscription = LearningCourseuser::getCountSubscriptions($userModel->idst);

			$courseSummaryList = LearningCourseuser::model()->getUserCourseSummary($userModel->idst, $user_subscription);

			$courseUserModel = new LearningCourseuser();
			$courseUserModel->idUser = $userModel->idst;
			$courseUserModel->level = '';
			$courseUserModel->waiting = '';

			$activitiesCategories = LearningTracksession::model()->getMonthsList();
			$courseSummatyActivities = LearningTracksession::model()->getUserMonthlyActivities($userModel->idst);

			$pieChart = Yii::app()->chart->getPieChart($courseSummaryList);
			$pieChartLegend = Yii::app()->chart->getPieChartLegend($courseSummaryList);

			$lineChart = Yii::app()->chart->getLineChart($courseSummatyActivities, $activitiesCategories);

			$renderData = array(
				'userModel' => $userModel,
				'userGroups' => $userGroups,
				'totalTime' => $totalTime,
				'additionalFields' => $additionalFields,
				'courseUserModel' => $courseUserModel,
				'pieChart' => $pieChart,
				'pieChartLegend' => $pieChartLegend,
				'lineChart' => $lineChart,
			);

			if ($createPdf) {
				$renderData['pieChart'] = $_POST['donutContent'];
				$renderData['lineChart'] = $_POST['lineContent'];
				$content = $this->renderPartial('_createUserReport', $renderData, true);

				$tempKey = substr(md5(uniqid(rand(), true)), 0, 5);
		   		$pdfFileName = 'userReport_'.date('m_d_Y').'_'.$tempKey .'.pdf';

				$pdfFileName = $this->createPdf($content, $pdfFileName);
				$this->sendJSON(array('fileName' => $pdfFileName));
			}
			$content = $this->renderPartial('createUserReport', $renderData, true, false);

			if (isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html')) {
				echo $content;
			} else {
				$this->sendJSON(array('html' => $content));
			}
//		}
		Yii::app()->end();
	}

	public function actionUserActivity() {

		$assetsUrl = $this->getAdminAssetsUrl();

        Yii::app()->clientScript->registerScriptFile($assetsUrl . '/charts/d3.v3.min.js');

        Yii::app()->clientScript->registerScriptFile($assetsUrl . '/charts/jquery.tipsy.min.js');
        Yii::app()->clientScript->registerCssFile($assetsUrl . '/charts/charts.css');


        $createPdf = false;
        $id = Yii::app()->user->getIdst();


		$with = array(
			'learningCourseusers',
			'groups'
		);

		$userModel = CoreUser::model()->with($with)->findByPk($id);

		$userFields = $userModel->getAdditionalFields();
		if (!empty($userFields)) {
			foreach ($userFields as $key => $field) {
				if (!empty($field->translation)) {
					$additionalFields[$field->translation] = $field->renderValue($field->userEntry, $field);
				}
			}
		}

		$userGroups = array();
		if (!empty($userModel->groups)) {
			foreach ($userModel->groups as $group) {
				if ($group->hidden == 'false') {
					$userGroups[] = CoreGroup::model()->relativeId($group->groupid);
				}
			}
		}

		$totalTime = LearningTracksession::model()->getUserTotalTime($userModel->idst);

		//$user_subscription = LearningCourseuser::model()->with('course')->countByAttributes(array("idUser" => $userModel->idst));
		$user_subscription = LearningCourseuser::getCountSubscriptions($userModel->idst);

		$courseSummaryList = LearningCourseuser::model()->getUserCourseSummary($userModel->idst, $user_subscription);

		$courseUserModel = new LearningCourseuser();
		$courseUserModel->idUser = $userModel->idst;
		$courseUserModel->level = '';
		$courseUserModel->waiting = '';

		$activitiesCategories = LearningTracksession::model()->getMonthsList();
		$courseSummatyActivities = LearningTracksession::model()->getUserMonthlyActivities($userModel->idst);

		$pieChart = Yii::app()->chart->getPieChart($courseSummaryList);
		$pieChartLegend = Yii::app()->chart->getPieChartLegend($courseSummaryList);

		$lineChart = Yii::app()->chart->getLineChart($courseSummatyActivities, $activitiesCategories);

		$renderData = array(
			'userModel' => $userModel,
			'userGroups' => $userGroups,
			'totalTime' => $totalTime,
			'additionalFields' => $additionalFields,
			'courseUserModel' => $courseUserModel,
			'pieChart' => $pieChart,
			'pieChartLegend' => $pieChartLegend,
			'lineChart' => $lineChart,
		);

		$this->render('userActivity', $renderData);
	}

	protected function createElearningCourseReport($courseModel, $createPdf = false) {
		// Get some common data; Respect POWER USER! (true)
		$totalCourseUsers = $courseModel->getTotalSubscribedUsers(true);
		$idCourse = $courseModel->idCourse;

		// Users completed withing 30 days; Respect POWER USER! (true)
		$completed30days = $courseModel->countUsersCompletedWithinNDays(30, true);
		$completed30daysPercent = ($totalCourseUsers > 0) ? number_format(100 * $completed30days / $totalCourseUsers, 1) : 0;
		$singlePieChart = Yii::app()->chart->getSinglePieChart($completed30days, $completed30daysPercent, $createPdf);

		// Days since course launch
		$totalCourseDays = $courseModel->getCourseTotalDays();

		// Training materials
		$trainingMaterials = LearningObjectsManager::getCourseLearningObjectsStats($idCourse);
		$totalOrganizationCourses = $trainingMaterials['total'];

		// Progress summary Pie chart; Respect POWER USER! (true)
		$courseSummaryList = LearningCourseuser::model()->getCourseUserSummary($idCourse, $totalCourseUsers, true);
		$pieChart = Yii::app()->chart->getPieChart($courseSummaryList, $createPdf);
		$pieChartLegend = Yii::app()->chart->getPieChartLegend($courseSummaryList);


		// Line chart; Respect POWER USER! (true)
		$courseSummatyActivities = LearningTracksession::model()->getCourseMonthlyActivities($idCourse, true);
		$activitiesCategories = LearningTracksession::model()->getMonthsList();
		$lineChart = Yii::app()->chart->getLineChart($courseSummatyActivities, $activitiesCategories, $createPdf);


		// Grid report data provider
		$courseUserModel = new LearningCourseuser();
		$courseUserModel->idCourse = $idCourse;
		$courseUserModel->level = '';
		$courseUserModel->waiting = '';

		$renderData = array(
			'totalCourseUsers' => $totalCourseUsers,
			'courseModel' => $courseModel,
			'totalCourseDays' => $totalCourseDays,
			'totalOrganizationCourses' => $totalOrganizationCourses,
			'courseUserModel' => $courseUserModel,
			'completed30days' => $completed30days,
			'completed30daysPercent' => $completed30daysPercent,

			'pieChart' => $pieChart,
			'pieChartLegend' => $pieChartLegend,
			'singlePieChart' => $singlePieChart,
			'lineChart' => $lineChart,
		);

		if ($createPdf) {
			//this is needed for the svg images
			if (!Yii::app()->chart->isIE8()) {
				$renderData['pieChart'] = $_POST['donutContent'];
				$renderData['lineChart'] = $_POST['lineContent'];
				$renderData['singlePieChart'] = $_POST['singleDonutContent'];
			}
			$chartsHtml = $this->renderPartial('_createElearningCourseReport', $renderData, true);

			$tempKey = substr(md5(uniqid(rand(), true)), 0, 5);
			$pdfFileName = 'courseReport_' . date('m_d_Y') . '_' . $tempKey . '.pdf';

			set_time_limit(0);
			$tableData = $this->collectDataForElearningCourseReport($courseUserModel);

			//Default orientation
			$orientation = 'P';

			$event = new DEvent($this, array('courseModel' => $courseModel));
			Yii::app()->event->raise('OnCourseSummaryPdfRender', $event);

			if (!empty($event->return_value['orientation'])) {
				$orientation = $event->return_value['orientation'];
			}

			//create a pdf with the Charts
			$this->createPdf($chartsHtml, $pdfFileName, $orientation);

			//append a table with the users report data to the pdf
			$this->exportTableToPdf($pdfFileName, $tableData['headers'], $tableData['data'], $tableData['widths'], true, $orientation);

			$this->sendJSON(array('fileName' => $pdfFileName));
		}

		$content = $this->renderPartial('createElearningCourseReport', $renderData, true);
		return $content;
	}

	/**
	 * @param LearningCourseuser $courseUserModel
	 * @return array
	 */
	protected function collectDataForElearningCourseReport(LearningCourseuser $courseUserModel) {
		$event = new DEvent($this, array('courseUserModel' => $courseUserModel));
		Yii::app()->event->raise('CollectTableDataForElearningCourseReport', $event);
		if(!$event->shouldPerformAsDefault() && is_array($event->return_value['tableData']))
			return $event->return_value['tableData'];

		$dataProvider = $courseUserModel->reportSqlDataProvider();
		$dataProvider->pagination = false;
		$data = $dataProvider->getData();

		$headers = array(
			Yii::t('standard', '_USERNAME'),
			Yii::t('report', '_DATE_INSCR'),
			Yii::t('standard', '_DATE_FIRST_ACCESS'),
			Yii::t('standard', 'Course completion'),
			Yii::t('standard', '_PROGRESS'),
			Yii::t('standard', '_TOTAL_TIME'),
			Yii::t('profile', '_USERCOURSE_STATUS'),
			Yii::t('standard', '_SCORE')
		);

		$lastScores = LearningCourseuser::getLastScores(array($courseUserModel->idCourse));
		$statusesArray = LearningCourseuser::getStatusesArray();
		$totalTimes = LearningTracksession::getTotalTimesByCourse($courseUserModel->idCourse);
		$progress = LearningOrganization::getProgressByCourse($courseUserModel->idCourse);
		$reportData = array();

		foreach($data as $row)
		{
			$values = array();
			$values[] = $row["userid"];
			$values[] = ($row["date_inscr"])? Yii::app()->localtime->toLocalDateTime($row["date_inscr"]): '';
			$values[] = ($row["date_first_access"])? Yii::app()->localtime->toLocalDateTime($row["date_first_access"]): '';
			$values[] = ($row["date_complete"])? Yii::app()->localtime->toLocalDateTime($row["date_complete"]): '';
			$values[] = (isset($progress[$row["idUser"]]))? $progress[$row["idUser"]].'%' : '0%';
			$values[] = (isset($totalTimes[$row["idUser"]]))? $totalTimes[$row["idUser"]] : '';
			$values[] = (isset($statusesArray[$row["status"]]))? $statusesArray[$row["status"]] : '';
			$values[] = (isset($lastScores[$row["idUser"]][$row["idCourse"]]))? $lastScores[$row["idUser"]][$row["idCourse"]] : '-';
			$reportData[] = array_values($values);
		}

		$tableData = array(
			'headers' => $headers,
			'widths' => array(30, 28, 28, 28, 20, 20, 20, 15),
			'data' => $reportData
		);


		return $tableData;
	}

	public function collectDataForClassroomCourseReport($session)
	{
		$dataProvider = LtCourseuserSession::model()->dataProviderUserStatistics($session->getPrimaryKey());
		$dataProvider->pagination = false;
		$data = $dataProvider->getData();

		$content = array();

		foreach($data as $row)
		{
			$content[] = array(
				Yii::app()->user->getRelativeUsername($row->learningUser->userid),
				$row->ltCourseSession->renderAttendance($row->learningUser->idst),
				$row->renderEvaluationStatus(),
				$row->renderSessionScore()
			);
		}

		$headers = array(
			Yii::t('standard', '_USERNAME'),
			Yii::t('standard', '_ATTENDANCE'),
			Yii::t('standard', '_STATUS'),
			Yii::t('standard', '_SCORE')
		);

		$tableData = array(
			'headers' => $headers,
			'data' => $content,
			'widths' => array(47, 47, 47, 47)
		);

		return $tableData;
	}

	/**
	 * @param $pdfFileName string
	 * @param $headers array
	 * @param $data array
	 * @param $columnWidths array
	 * @param bool $appendToExistingPdf
	 * @return mixed
	 */
	protected function exportTableToPdf($pdfFileName, $headers, $data, $columnWidths, $appendToExistingPdf = false, $orientation = 'P') {
		Yii::import('common.extensions.PDF_Table');
        $tmpPdfDir = Yii::app()->basePath . '/runtime/tmpPdf/';
        if (!is_dir($tmpPdfDir)) {
            FileHelper::createDirectory($tmpPdfDir, 0777, true);
        }

		$pdf = new PDF_Table();
		$pdf->SetTitle($pdfFileName);
		$pdf->setWidths($columnWidths);

		if($appendToExistingPdf)
			$pdf->addExistingPdf($tmpPdfDir.$pdfFileName, '/MediaBox', $orientation);

		$totalText = strip_tags(Yii::t('standard', '_TOTAL', array('{count}' => count($data))));
		$pdf->exportTable($headers, $data, $totalText, $orientation);

		//Output
		$tmpPdfDir = Yii::app()->basePath . '/runtime/tmpPdf/';
        if (!is_dir($tmpPdfDir)) {
            FileHelper::createDirectory($tmpPdfDir, 0777, true);
        }
		$pdfFullPath = $tmpPdfDir . $pdfFileName;

		$pdf->Output($pdfFullPath, 'F');

		return $pdfFileName;
	}

	/**
	 * @param $course LearningCourse
	 */
	protected function createClassroomCourseReport($course, $createPdf = false) {

		$session = LtCourseSession::model()->findByAttributes(array(
			'id_session' => Yii::app()->request->getParam('session_id'),
			'course_id' => $course->getPrimaryKey()
		));
		if (!$session) {
			$session = new LtCourseSession();
		}

		$pieChartData = array();
		if (!$session->isNewRecord) {
			// get current session stats
			$pieChartData = $session->getPassStats();
			$pieChartData['totalHours'] = $session->total_hours;
		} else {
			// get total sessions stats
			$pieChartData = LtCourseuserSession::model()->getPassStats($course->getPrimaryKey());

			$totalHours = 0;
			foreach ($course->learningCourseSessions as $courseSession) {
				/* @var $session LtCourseSession */
				$totalHours += intval($courseSession->total_hours);
			}
			$pieChartData['totalHours'] = $totalHours;
		}

		$singlePieChart = Yii::app()->chart->getSinglePieChart($pieChartData['count'], $pieChartData['percent'], $createPdf);

		$renderData = array(
			'course' => $course,
			'session' => $session,
			'pieChartData' => $pieChartData,
			'singlePieChart' => $singlePieChart,
		);

		if ($createPdf) {
			//this is needed for the svg images
			if (!Yii::app()->chart->isIE8()) {
				$renderData['singlePieChart'] = $_POST['singleDonutContent'];
			}
			$content = $this->renderPartial('_createClassroomCourseReport', $renderData, true);

			$tempKey = substr(md5(uniqid(rand(), true)), 0, 5);
			$pdfFileName = 'courseReport_' . date('m_d_Y') . '_' . $tempKey . '.pdf';

			$pdfFileName = 'courseReport_' . date('m_d_Y') . '_' . $tempKey . '.pdf';

			set_time_limit(0);
			$tableData = $this->collectDataForClassroomCourseReport($session);

			//Default orientation
			$orientation = 'P';

			$event = new DEvent($this, array('courseModel' => $course));
			Yii::app()->event->raise('OnCourseSummaryPdfRender', $event);

			if (!empty($event->return_value['orientation'])) {
				$orientation = $event->return_value['orientation'];
			}

			//create a pdf with the Charts
			$pdfFileName = $this->createPdf($content, $pdfFileName, $orientation);
			//append a table with the users report data to the pdf
			$this->exportTableToPdf($pdfFileName, $tableData['headers'], $tableData['data'], $tableData['widths'], true, $orientation);

			$this->sendJSON(array('fileName' => $pdfFileName));
		}

		$content = $this->renderPartial('createClassroomCourseReport', $renderData, true);

		return $content;
	}


	public function actionCreateCourseReport($id = null, $createPdf = false)
	{
		if (Yii::app()->request->isAjaxRequest) {

			// now we have id of the course, so just search by it
			$courseModel = LearningCourse::model()->findByPk($id);
			if (!$courseModel) {
				$courseModel = LearningCourse::model()->find('name = :name OR CONCAT(name, " - ", code) = :name', array(':name' => $id));
			}
			if (empty($courseModel)) {
				Yii::log('Report requested for invalid course', CLogger::LEVEL_ERROR);
				$this->sendJSON(array('html' => Yii::t('standard', '_INVALID_COURSE')));
			}

			if(Yii::app()->user->getIsPu() && $courseModel && isset($courseModel->idCourse)){
				if(!CoreUserPuCourse::model()->countByAttributes(array('puser_id' => Yii::app()->user->id, 'course_id' =>$courseModel->idCourse))){
					$this->sendJSON(array('html' => Yii::t('standard', '_INVALID_COURSE')));
				}
			}



			if (PluginManager::isPluginActive('ClassroomApp') && $courseModel->course_type == LearningCourse::TYPE_CLASSROOM) {
                $content = $this->createClassroomCourseReport($courseModel, $createPdf);
			} else if($courseModel->course_type == LearningCourse::TYPE_WEBINAR){
                $content = $this->createWebinarCourseReport($courseModel, $createPdf);
            } else {
                $content = $this->createElearningCourseReport($courseModel, $createPdf);
            }

			if (isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html')) {
				echo $content;
			} else {
				$this->sendJSON(array('html' => $content));
			}
		}
		Yii::app()->end();
	}

	public function actionClassroomCourseReport() {

		$course = LearningCourse::model()->with('learningCourseusers')->findByPk(Yii::app()->request->getParam('course_id'));
		if (!$course || !PluginManager::isPluginActive('ClassroomApp') || $course->course_type != LearningCourse::TYPE_CLASSROOM) {
			$this->redirect($this->createUrl('index'));
		}

		if (Yii::app()->player) {
			//we need to simulate being into a course
			Yii::app()->player->setCourse($course);
		}

		DatePickerHelper::registerAssets();


		$assetsUrl = $this->getAdminAssetsUrl();

		Yii::app()->clientScript->registerScriptFile($assetsUrl . '/charts/d3.v3.min.js');

		Yii::app()->clientScript->registerScriptFile($assetsUrl . '/charts/jquery.tipsy.min.js');
		Yii::app()->clientScript->registerCssFile($assetsUrl . '/charts/charts.css');

		Yii::app()->clientScript->registerScriptFile($assetsUrl . '/print/jquery.print-preview.js');
		Yii::app()->clientScript->registerCssFile($assetsUrl . '/print/css/print-preview.css');

		Yii::app()->clientScript->registerCssFile(Yii::app()->theme->getBaseUrl() . '/css/classroom.css');

		$session = LtCourseSession::model()->findByAttributes(array(
			'id_session' => Yii::app()->request->getParam('session_id'),
			'course_id' => $course->getPrimaryKey()
		));

		if (!$session) {
			$session = new LtCourseSession();
		}

		$pieChartData = array();
		if (!$session->isNewRecord) {
			// get current session stats
			$pieChartData = $session->getPassStats();
			$pieChartData['totalHours'] = $session->total_hours;
		} else {
			// get total sessions stats
			$pieChartData = LtCourseuserSession::model()->getPassStats($course->getPrimaryKey());

			$totalHours = 0;
			foreach ($course->learningCourseSessions as $courseSession) {
				/* @var $session LtCourseSession */
				if(Yii::app()->user->getIsPu()) {
					if(Yii::app()->user->checkAccess('/lms/admin/classroomsessions/add')) {
						if(!Yii::app()->user->checkAccess('/lms/admin/classroomsessions/mod') && !Yii::app()->user->checkAccess('/lms/admin/classroomsessions/assign')) {
							if($courseSession->created_by == Yii::app()->user->id) {
								$totalHours += intval($courseSession->total_hours);
							} else {
								$totalHours += 0;
							}
						} else {
							$totalHours += intval($courseSession->total_hours);
						}
					} else {
						$totalHours += intval($courseSession->total_hours);
					}
				} else {
					$totalHours += intval($courseSession->total_hours);
				}
			}
			$pieChartData['totalHours'] = $totalHours;
		}

		$singlePieChart = Yii::app()->chart->getSinglePieChart($pieChartData['count'], $pieChartData['percent']);

		$renderData = array(
			'course' => $course,
			'session' => $session,
			'pieChartData' => $pieChartData,
			'singlePieChart' => $singlePieChart,
		);

		$createPdf = false; // Is this needed?
		if ($createPdf) {
			$renderData['singlePieChart'] = $_POST['singleDonutContent'];
			$content = $this->renderPartial('_createClassroomCourseReport', $renderData, true);

			$tempKey = substr(md5(uniqid(rand(), true)), 0, 5);
			$pdfFileName = 'courseReport_' . date('m_d_Y') . '_' . $tempKey . '.pdf';

			$pdfFileName = $this->createPdf($content, $pdfFileName);
			$this->sendJSON(array('fileName' => $pdfFileName));
		}

		if (Yii::app()->request->isAjaxRequest) {
			$content = $this->renderPartial('classroomCourseReport', $renderData, true, true);
			echo $content;
		} else {
			$this->render('classroomCourseReport', $renderData);
		}
	}

	public function createWebinarCourseReport($course, $createPdf = false) {

		$session = WebinarSession::model()->findByAttributes(array(
			'id_session' => Yii::app()->request->getParam('session_id'),
			'course_id' => $course->getPrimaryKey()
		));

		if (!$session) {
			$session = new WebinarSession();
		}

		$pieChartData = array();
		if (!$session->isNewRecord) {
			// get current session stats
			$pieChartData = $session->getPassStats();
			$pieChartData['totalHours'] = intval($session->getTotalHoursDecimal());
		} else {
			// get total sessions stats
			$pieChartData = WebinarSessionUser::model()->getPassStats($course->getPrimaryKey());

			$totalHours = 0;
			foreach ($course->learningWebinarSessions as $courseSession) {
				/* @var $session WebinarSession */
				$totalHours += $courseSession->getTotalHoursDecimal();
			}
			$pieChartData['totalHours'] = intval($totalHours);
		}

		$singlePieChart = Yii::app()->chart->getSinglePieChart($pieChartData['count'], $pieChartData['percent']);

		$renderData = array(
			'course' => $course,
			'session' => $session,
			'pieChartData' => $pieChartData,
			'singlePieChart' => $singlePieChart,
		);

		if ($createPdf) {
			$renderData['singlePieChart'] = $_POST['singleDonutContent'];
			$content = $this->renderPartial('_createWebinarCourseReport', $renderData, true);

			$tempKey = substr(md5(uniqid(rand(), true)), 0, 5);
			$pdfFileName = 'courseReport_' . date('m_d_Y') . '_' . $tempKey . '.pdf';

			//Default orientation
			$orientation = 'P';

			$event = new DEvent($this, array('courseModel' => $courseModel));
			Yii::app()->event->raise('OnCourseSummaryPdfRender', $event);

			if (!empty($event->return_value['orientation'])) {
				$orientation = $event->return_value['orientation'];
			}

			$pdfFileName = $this->createPdf($content, $pdfFileName, $orientation);
			$this->sendJSON(array('fileName' => $pdfFileName));
		}

		$content = $this->renderPartial('createWebinarCourseReport', $renderData, true);
		return $content;
	}

	public function actionWebinarCourseReport() {

		$course = LearningCourse::model()->with('learningCourseusers')->findByPk(Yii::app()->request->getParam('course_id'));
		if ($course->course_type != LearningCourse::TYPE_WEBINAR) {
			$this->redirect($this->createUrl('index'));
		}

		if (Yii::app()->player) {
			//we need to simulate being into a course
			Yii::app()->player->setCourse($course);
		}

		DatePickerHelper::registerAssets();


		$assetsUrl = $this->getAdminAssetsUrl();

		Yii::app()->clientScript->registerScriptFile($assetsUrl . '/charts/d3.v3.min.js');

		Yii::app()->clientScript->registerScriptFile($assetsUrl . '/charts/jquery.tipsy.min.js');
		Yii::app()->clientScript->registerCssFile($assetsUrl . '/charts/charts.css');

		Yii::app()->clientScript->registerScriptFile($assetsUrl . '/print/jquery.print-preview.js');
		Yii::app()->clientScript->registerCssFile($assetsUrl . '/print/css/print-preview.css');

		Yii::app()->clientScript->registerCssFile(Yii::app()->theme->getBaseUrl() . '/css/classroom.css');

		$session = WebinarSession::model()->findByAttributes(array(
			'id_session' => Yii::app()->request->getParam('session_id'),
			'course_id' => $course->getPrimaryKey()
		));

		if (!$session) {
			$session = new WebinarSession();
		}

		if (!$session->isNewRecord) {
			// get current session stats
			$pieChartData = $session->getPassStats();
			$pieChartData['totalHours'] = intval($session->getTotalHoursDecimal());
		} else {
			// get total sessions stats
			$pieChartData = WebinarSessionUser::model()->getPassStats($course->getPrimaryKey());

			$totalHours = 0;
			foreach ($course->learningWebinarSessions as $courseSession) {
				/* @var $session WebinarSession */
				if(Yii::app()->user->getIsPu()) {
					if(Yii::app()->user->checkAccess('/lms/admin/webinarsessions/add')) {
						if(!Yii::app()->user->checkAccess('/lms/admin/webinarsessions/mod') && !Yii::app()->user->checkAccess('/lms/admin/webinarsessions/assign')) {
							if($courseSession->created_by == Yii::app()->user->id) {
								$totalHours += $courseSession->getTotalHoursDecimal();
							} else {
								$totalHours += 0;
							}
						} else {
							$totalHours += $courseSession->getTotalHoursDecimal();
						}
					} else {
						$totalHours += $courseSession->getTotalHoursDecimal();
					}
				} else {
					$totalHours += $courseSession->getTotalHoursDecimal();
				}
			}
			$pieChartData['totalHours'] = intval($totalHours);
		}

		$singlePieChart = Yii::app()->chart->getSinglePieChart($pieChartData['count'], $pieChartData['percent']);

		$renderData = array(
			'course' => $course,
			'session' => $session,
			'pieChartData' => $pieChartData,
			'singlePieChart' => $singlePieChart,
		);

		$createPdf = false; // Is this needed?
		if ($createPdf) {
			$renderData['singlePieChart'] = $_POST['singleDonutContent'];
			$content = $this->renderPartial('_createClassroomCourseReport', $renderData, true);

			$tempKey = substr(md5(uniqid(rand(), true)), 0, 5);
			$pdfFileName = 'courseReport_' . date('m_d_Y') . '_' . $tempKey . '.pdf';

			$pdfFileName = $this->createPdf($content, $pdfFileName);
			$this->sendJSON(array('fileName' => $pdfFileName));
		}

		if (Yii::app()->request->isAjaxRequest) {
			$content = $this->renderPartial('webinarCourseReport', $renderData, true, true);
			echo $content;
		} else {
			$this->render('webinarCourseReport', $renderData);
		}
	}

	public static function createPdf($content, $pdfFileName, $orientation = 'P', $additionalCss=false) {

		// $content = '<div class="new-user-summary">'.$content.'</div>';
        if (!is_file(Yii::app()->basePath . '/runtime/content.html')) {
            file_put_contents(Yii::app()->basePath . '/runtime/content.html', $content);
        }


		// $mPDF1 = Yii::app()->ePdf->mpdf();
		// pr($mPDF1);
		# You can easily override default constructor's params

		$format = 'A4';

		if ($orientation !== 'P') {
			$format .= '-' . $orientation;
		}

		$mPDF1 = Yii::app()->ePdf->mpdf('', $format);
		$mPDF1->useAdobeCJK = true;
		$mPDF1->SetAutoFont(AUTOFONT_ALL);
		# Load a stylesheet
		$stylesheet = file_get_contents(Yii::app()->theme->basePath . '/css/report-pdf.css');
		$mPDF1->WriteHTML($stylesheet, 1);
		$stylesheet2 = file_get_contents(Yii::getPathOfAlias('admin') . '/js/charts/charts.css');
		$mPDF1->WriteHTML($stylesheet2, 1);

		if ($additionalCss) {
			$mPDF1->WriteHTML(file_get_contents($additionalCss), 1);
		}

		// $stylesheet3 = file_get_contents(Yii::getPathOfAlias('admin') . '/js/jqplot/jquery.jqplot.min.css');
		// $mPDF1->WriteHTML($stylesheet3, 1);
		# render (full page)

		$mPDF1->WriteHTML($content, 2);
		// pr($mPDF1);
		# renderPartial (only 'view' of current controller)
		// $mPDF1->WriteHTML($this->renderPartial('index', array(), true));
		# Renders image
		// $mPDF1->WriteHTML(CHtml::image(Yii::getPathOfAlias('webroot.css') . '/bg.gif' ));
		# Outputs ready PDF
        $tmpPdfDir = Yii::app()->basePath . '/runtime/tmpPdf/';
        if (!is_dir($tmpPdfDir)) {
            FileHelper::createDirectory($tmpPdfDir, 0777, true);
        }

		$pdfFullPath = $tmpPdfDir . $pdfFileName;

		$mPDF1->Output($pdfFullPath, 'F');

		return $pdfFileName;
	}

	public function actionDownloadPdf($fileName = null) {
		$pdfFullPath = Yii::app()->basePath . '/runtime/tmpPdf/' . basename($fileName);

		Yii::app()->request->sendFileDirect($pdfFullPath, $fileName);

		if (!empty($fileName) && file_exists($pdfFullPath)) {
			/* $isIE8 = preg_match('/(?i)msie [6-8]/',$_SERVER['HTTP_USER_AGENT']); */ //TODO: IE8 detection seems to not correctly work this way
			if (/* $isIE8 && */isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) != 'off') {
				//--- IE-8 compatibility headers for HTTPS connections ---
				//content type set to generic fiel download
				header("Content-type: application/download");
				//cache control
				header("Cache-control: private");
				header('Pragma: private');
				//sending creation time
				header('Expires: ' . gmdate('D, d M Y H: i: s') . ' GMT');
			} else {
				//--- normal headers ---
				// We'll be outputting a PDF
				header('Content-type: application/pdf');
				// It will be called downloaded.pdf
				header('Content-Disposition: attachment; filename="' . $fileName . '"');
			}
			// The PDF source is in original.pdf
			readfile($pdfFullPath);
		}
		Yii::app()->end();
	}

	public function actionSendEmail() {
		if (!empty($_POST['ids'])) {
			$ids = $_POST['ids'];
		} else {
			$ids = array();
		}
		$coreUsers = CoreUser::model()->findAllByAttributes(array('idst' => $ids));
		if (empty($coreUsers)) {
			$this->sendJSON(array());
		}

		$emailModel = new EmailForm();

		if (!empty($_POST['EmailForm'])) {
			$emailModel->attributes = $_POST['EmailForm'];
			if ($emailModel->validate()) {
				$fromName = Yii::app()->user->getRelativeUsername(Yii::app()->user->username);
				foreach ($coreUsers as $user) {
					if (!empty($user->email)) {
						$mail = new MailManager();
						$mail->mail(Yii::app()->params['support_sender_email'], $user->email, $emailModel->subject, $emailModel->message/* , $attachments */);
					}
				}
				$this->sendJSON(array());
			}
		}
		if (Yii::app()->request->isAjaxRequest) {
			$content = $this->renderPartial('send', array(
				// 'model' => new CoreUser(),
				'emailModel' => $emailModel,
				'users' => $coreUsers,
					), true);
			$this->sendJSON(array(
				'html' => $content,
			));
		}
		Yii::app()->end();
	}

	public function actionReportAutocomplete() {
		$query = addcslashes($_REQUEST['data']['query'], '%_');
		$criteria = new CDbCriteria(array(
			'condition' => 'LOWER(filter_name) LIKE :match AND is_standard=0',
			'params' => array(
				':match' => "%" . strtolower($query) . "%",
			),
		));

		if (!Yii::app()->user->getIsGodadmin() && Yii::app()->user->getIsAdmin()) {
			// Is a Power User
			// The PU can see only his own reports (public and private)
			// and public reports by other users
			$criteria->addCondition('is_public=1 OR author=:pu');
			$criteria->params[':pu'] = Yii::app()->user->getId();
		}

		$reports = LearningReportFilter::model()->findAll($criteria);
		$reportsList = array_values(CHtml::listData($reports, 'id_filter', 'filter_name'));

		$this->sendJSON(array('options' => $reportsList));
	}

	public function actionToggleReportPublic($id) {
		$model = LearningReportFilter::model()->findByPk($id);

		if ($model) {
			$model->is_public = $model->is_public == 1 ? 0 : 1;
			$model->save();
			$result['success'] = true;

			Yii::app()->event->raise('ToggledPublicCustomReport', new DEvent($this, array(
				'id_filter' => $model->id_filter,
				'model' => $model
			)));
		} else {
			$result['success'] = false;
		}

		$this->sendJSON($result);
	}

	public function actionFinishCustomReport($show = false) {
		$attributes = LearningReportFilter::getSessionAttributes();

		if (!empty($attributes['reportId'])) {
			$model = LearningReportFilter::model()->findByPk($attributes['reportId']);

			if (empty($model)) {
				$this->sendJSON(array());
			}
		} else {
			$model = new LearningReportFilter;
		}

		$model->attributes = $attributes;

		$model->author = Yii::app()->user->idst;
		$model->creation_date = Yii::app()->localtime->toLocalDateTime();
		$model->is_standard = 0;

		$reportData = CJSON::encode($attributes);
		$model->filter_data = $reportData;
		if ($model->save()) {
			if ($show) {
				$this->sendJSON(array(
					'redirect' => Yii::app()->createAbsoluteUrl('reportManagement/customView', array('id' => $model->id_filter)),
				));
			} else {
				$this->sendJSON(array());
			}
		}
	}

	public function processSelectUsers($attributes = array(), $step = 1) {
		// Users selection
		$user = CoreUser::model();
		$user->scenario = 'search';
		$user->unsetAttributes();
		if (isset($_REQUEST['CoreUser'])) {
			$user->attributes = $_REQUEST['CoreUser'];
		}

		$group = new CoreGroup();
		if (isset($_REQUEST['CoreGroup'])) {
			$group->attributes = $_REQUEST['CoreGroup'];
		}

		$groupsList = array();
		$orgChartNodesList = array();
		$usersList = array();

		$usersList = Yii::app()->userSelectHelper->getUsersListFromRequest(
				array(
					'users' => 'select-user-grid-selected-items',
					'groups' => 'select-group-grid-selected-items',
					'orgChart' => 'select-orgchart',
				)
		);

		if (!empty($_REQUEST['select-user-grid-selected-items'])) {
			Yii::app()->session['selectedItems'] = explode(',', $_REQUEST['select-user-grid-selected-items']);
		} elseif (!empty($_REQUEST['selectedItems'])) {
			Yii::app()->session['selectedItems'] = explode(',', $_REQUEST['selectedItems']);
		} else {
			$userItems = (!empty($attributes['gridData']['select-user-grid-checkboxes']) ? $attributes['gridData']['select-user-grid-checkboxes'] : array());
			$groupItems = (!empty($attributes['gridData']['select-group-grid-checkboxes']) ? $attributes['gridData']['select-group-grid-checkboxes'] : array());
			Yii::app()->session['selectedItems'] = CMap::mergeArray($userItems, $groupItems);
			Yii::app()->session['selectedOrgChartItems'] = $attributes['gridData']['select-orgchart'];
		}
		if (!empty($usersList)) {
			LearningReportFilter::setSessionAttributes(
					array(
						'users' => $usersList,
						'groups' => $groupsList,
						'orgchartnodes' => $orgChartNodesList,
						'gridData' => array(
							'select-user-grid-checkboxes' => !empty($_REQUEST['select-user-grid-selected-items']) ? explode(',', $_REQUEST['select-user-grid-selected-items']) : array(),
							'select-group-grid-checkboxes' => !empty($_REQUEST['select-group-grid-checkboxes']) ? $_REQUEST['select-group-grid-checkboxes'] : array(),
							'select-orgchart' => !empty($_REQUEST['select-orgchart']) ? $_REQUEST['select-orgchart'] : array(),
							'select-user-grid-selected-items' => !empty($_REQUEST['select-user-grid-selected-items']) ? $_REQUEST['select-user-grid-selected-items'] : array(),
							'select-group-grid-selected-items' => !empty($_REQUEST['select-group-grid-selected-items']) ? $_REQUEST['select-group-grid-selected-items'] : array(),
						),
					)
			);

			$this->sendJSON(array(
				'step' => ++$step,
			));
		}

		// Build fancytree specific PRE-selected array
		$preSelected = array();
		if (isset($attributes['gridData']['select-orgchart']) && (count($attributes['gridData']['select-orgchart']) > 0))
			foreach ($attributes['gridData']['select-orgchart'] as $key => $selectState) {
				$preSelected[] = array("key" => $key, "selectState" => $selectState);
			}

		$fancyTreeData = CoreOrgChartTree::buildFancytreeDataArray(false, true, true);
		$content = $this->renderPartial('//common/_usersSelector', array(
			'userModel' => $user,
			'groupModel' => $group,
			'isNeedRegisterYiiGridJs' => false,
			'fancyTreeData' => $fancyTreeData,
			'preSelected' => $preSelected,
				), true, true);
		return $content;
	}

	public function processSelectCourses($attributes = array(), $step = 1) {
		$courseModel = LearningCourse::model();
		$courseModel->unsetAttributes();
		if (isset($_REQUEST['LearningCourse'])) {
			$courseModel->attributes = $_REQUEST['LearningCourse'];
		}

		if (Yii::app()->user->getIsAdmin())
			$courseModel->powerUserManager = CoreUser::model()->findByPk(Yii::app()->user->id);

		$courselist = array();
		if (!empty($_REQUEST['select-course-grid-selected-items'])) {
			Yii::app()->session['selectedItems'] = $courselist = explode(',', $_REQUEST['select-course-grid-selected-items']);
		} elseif (!empty($_REQUEST['selectedItems'])) {
			Yii::app()->session['selectedItems'] = explode(',', $_REQUEST['selectedItems']);
		} else {
			Yii::app()->session['selectedItems'] = (!empty($attributes['gridData']['select-course-grid-selected-items']) ? explode(',', $attributes['gridData']['select-course-grid-selected-items']) : array());
		}

		if (!empty($courselist)) {
			LearningReportFilter::setSessionAttributes(array(
				'courses' => $courselist,
				'gridData' => array(
					'select-course-grid-selected-items' => !empty($_REQUEST['select-course-grid-selected-items']) ? $_REQUEST['select-course-grid-selected-items'] : array(),
					'select-course-grid-checkboxes' => !empty($_REQUEST['courseEnroll-course-grid-checkboxes']) ? $_REQUEST['courseEnroll-course-grid-checkboxes'] : array(),
				)
			));
			// $attributes = LearningReportFilter::getSessionAttributes();
			// pr($attributes);
			$this->sendJSON(array(
				'step' => ++$step,
			));
		}

		$processOutput = true;
		if ((isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html'))) {
			$processOutput = false;
		}

		$content = $this->renderPartial('//courseManagement/enroll_course', array(
			'model' => $courseModel,
				), true, $processOutput);

		return $content;
	}

	public function actionGetStandardActions($id) {
		$model = LearningReportFilter::model()->findByPk($id);

		if (!$model) {
			$this->sendJSON(array());
		}

		$html = $this->renderPartial('_standardReportActions2', array('model' => $model), true);

		$this->sendJSON(array('html' => $html));
	}

	public function actionCreateReport($step) {
		$model = new LearningReportFilter;
		$attributes = LearningReportFilter::getSessionAttributes();
		$success = true;
		$model->attributes = $attributes;

		if (isset($_POST['LearningReportFilter'])) {
			$model->attributes = $_POST['LearningReportFilter'];

			if ($step == 1) {
				$success = $model->validate();
			}

			if ($success) {
				$this->sendJSON(array(
					'step' => ++$step,
				));
			}
		}

		$this->sendJSON(array(
			'step' => $step,
			'html' => $this->renderPartial('steps/step_' . $step, array('model' => $model), true),
		));
	}

	public function actionEditReport($id) {
		$this->sendJSON(array());
	}

	public function actionDeleteReport($id) {
		$report = LearningReportFilter::model()->findByPk($id);

		if (empty($report)) {
			$this->sendJSON(array());
		}

		if (!empty($_POST['LearningReportFilter'])) {
			if ($_POST['LearningReportFilter']['confirm']) {
				$report->delete();
				$this->sendJSON(array());
			}
		}
		if (Yii::app()->request->isAjaxRequest) {
			$content = $this->renderPartial('delete', array(
				'model' => $report,
					), true);
			$this->sendJSON(array(
				'html' => $content,
			));
		}
		Yii::app()->end();
	}

	/**
	 * Dispatch and generate custom report views
	 *
	 * @param int $id  Report Filter Id
	 * @param string $referrer Used to build the BACK button URL
	 * @throws CException
	 */
	public function actionCustomView($id = null, $referrer = null) {

		// Request a full width layout
		$this->useFluidContainer = true;

		$reportFilterModel = LearningReportFilter::model()->findByPk($id);

		$customFilter = isset($_REQUEST['custom_filter']) && is_array($_REQUEST['custom_filter']) ? $_REQUEST['custom_filter'] : false;

		// First, Some navigation stuff to set up
		$dashboard = 'dashboard/index';
		if ($referrer == 'reports' || !Yii::app()->request->urlReferrer) {
			$back = Yii::app()->createUrl('reportManagement/index');
		} elseif ($referrer == 'dashboard') {
			$back = Yii::app()->createUrl('dashboard/index');
		} else {
			$this->redirect($this->createUrl('reportManagement/customView', array(
						'id' => $id,
						'referrer' => (substr(Yii::app()->request->urlReferrer, strlen($dashboard) * (-1)) == $dashboard ? 'dashboard' : 'reports')
			)));
			Yii::app()->end();
		}

		// Get an array of [SQL Data Provider] and [Grid Columns] for this custom report filter ID
		$pagination = array('pageSize' => 20);
		$reportData = $this->getReportDataProviderInfo($id, $pagination, $customFilter);
		$dataProvider = $reportData['dataProvider'];
		$gridColumns = $reportData['gridColumns'];
		// Handling the event for custom applications
		$event = new DEvent($this, array(
			'report_filter' => $reportFilterModel,
			'pagination' => $pagination
		));
		Yii::app()->event->raise('CustomReportView', $event);
		if (!empty($event->return_value)) {
			$dataProvider = $event->return_value['dataProvider'];
			$gridColumns = $event->return_value['gridColumns'];
		}

		if (!$dataProvider) {
			Yii::log('No data reported for custom report with ID ' . $id, CLogger::LEVEL_ERROR);
			$this->redirect(Docebo::createAdminUrl('reportManagement/index'), true);
			Yii::app()->end();
		}
		// Render report
		$this->render('view_report', array(
			'reportFilterModel' => $reportFilterModel,
			'dataProvider' => $dataProvider,
			'gridColumns' => $gridColumns,
			'back' => $back,
		));
	}

	/**
	 * Export Custom report. Takes filter data from learning_report_filter table.
	 *
	 * Available Formats:  Excel5, CSV, HTML
	 *
	 * @param string $id Report filter id
	 * @throws CException
	 */
	public function actionExportCustomReport($id = null) {

		// Some validation checks
		if (!$id) {
			throw new CException('Invalid report filter ID');
		}

		$type = Yii::app()->request->getParam('type', false);
		if (!array_key_exists($type, LearningReportFilter::exportTypes())) {
			throw new CException('Invalid export type requested');
		}

		// Get dataprovider and columns; this is the same set of information used for the grid, but with disabled pagination
		$reportData = $this->getReportDataProviderInfo($id);
		$dataProvider = $reportData['dataProvider'];
		$columns = $reportData['gridColumns'];
		$reportTitle = $reportData['reportTitle'];


		// CSV REPORTS:  Use separate method for exporting data to CSV file and sending it to the client browser
		if ($type == 'CSV') {
			$csvFileName = Sanitize::fileName($reportTitle . ".csv", true);
			$csvFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $csvFileName;
			$ok = $this->exportDataToCsv($dataProvider, $columns, $csvFilePath);
			if ($ok) {
				ob_end_clean();
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Pragma: public');
				header('Content-type: text/csv');
				header('Content-Disposition: attachment; filename="' . basename($csvFileName));
				header('Cache-Control: max-age=0');
				readfile($csvFilePath);
				unlink($csvFilePath);
			}
			Yii::app()->end();
		}


		// Buils Export headers and rows/values
		$headers = array();
		$values = array();

		// HEADERS
		foreach ($columns as $column) {
			if (isset($column['header'])) {
				$headers[] = $column['header'];
			} else if (isset($column['name'])) {
				$headers[] = $column['name'];
			}
		}

		$sql = $dataProvider->sql;

		$dataReader = Yii::app()->db->createCommand($sql)->query();

		$rowIndex = 1;
		$value = null; // reused
		foreach ($dataReader as $data) {
			foreach ($columns as $column) {
				eval('$value = ' . $column['value'] . ';');
				$values[$rowIndex][] = $value;
			}
			$rowIndex++;
		}


		// Run the export
		Yii::import('common.extensions.array2excel.Array2Excel');
		$xls = new Array2Excel($values, $headers);
		$xls->exportType = $type;
		$xls->filename = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . Sanitize::fileName($reportTitle, true);


		// Save the file locally first (due to memory issues with direct stream of data)
		$xls->stream = false;

		$xls->run();

		// Send the local file to the browser for download
		ob_end_clean();
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-type: ' . $xls->mimeTypes[$xls->exportType]['Content-type']);
		header('Content-Disposition: attachment; filename="' . basename($xls->filename) . '.' . $xls->mimeTypes[$xls->exportType]['extension'] . '"');
		header('Cache-Control: max-age=0');
		readfile($xls->filename);

		unlink($xls->filename);

		Yii::app()->end();
	}

	/**
	 * Export Custom report. Takes filter data from learning_report_filter table.
	 *
	 * Available Formats:  Excel5, CSV, HTML
	 *
	 * @param string $id Report filter id
	 * @throws CException
	 */
	public function actionExportCustomReport_NEW($id = null) {

		// Some validation checks
		if (!$id) {
			throw new CException('Invalid report filter ID');
		}

		$type = Yii::app()->request->getParam('type', false);
		if (!array_key_exists($type, LearningReportFilter::exportTypes())) {
			throw new CException('Invalid export type requested');
		}

		// Get dataprovider and columns; this is the same set of information used for the grid, but with disabled pagination
		$reportData = $this->getReportDataProviderInfo($id);
		$dataProvider = $reportData['dataProvider'];
		$columns = $reportData['gridColumns'];
		$reportTitle = $reportData['reportTitle'];

		$csvFileName = Sanitize::fileName($reportTitle . ".csv", true);
		$csvFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $csvFileName;

		$ok = $this->exportDataToCsv($dataProvider, $columns, $csvFilePath);

		if ($ok) {
			ob_end_clean();
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-type: text/csv');
			header('Content-Disposition: attachment; filename="' . basename($csvFileName));
			header('Cache-Control: max-age=0');
			readfile($csvFilePath);
			unlink($csvFilePath);
		}


		Yii::app()->end();
	}

	/**
	 * Export report data to CSV file, row by row and send it back to client browser
	 * @param array $reportData Incoming Report date (collected earlier)
	 * @return bool
	 */
	protected function exportDataToCsv($dataProvider, $columns, $csvFilePath) {

		$pageSize = 20000; // num of records to handle per chunk (LIMIT clause)

		$fh = fopen($csvFilePath, 'w');

		$headers = array();
		$values = array();

		// HEADERS
		foreach ($columns as $column) {
			if (isset($column['header'])) {
				$headers[] = $column['header'];
			} else if (isset($column['name'])) {
				$headers[] = $column['name'];
			}
		}

		// Write a HEADER line
		CSVParser::filePutCsv($fh, $headers);

		$finished = false;
		$offset = 0;
		$pages = 0;

		while (!$finished) {
			$counter = 0;
			$pages++;
			$sql = $dataProvider->sql;
			$sql = $sql . " LIMIT $offset,$pageSize";
			$dataReader = Yii::app()->db->createCommand($sql)->query();
			$value = null; // reused
			foreach ($dataReader as $data) {
				$counter++;
				$values = array();
				foreach ($columns as $column) {
					eval('$value = ' . $column['value'] . ';');
					$values[] = $value;
				}
				CSVParser::filePutCsv($fh, $values);
			}
			if ($counter < $pageSize) {
				$finished = true;
			}
			$offset = $offset + $pageSize;
		}

		fclose($fh);

		return true;
	}

	/**
	 * Based on the passed Report filter ID, performs report specific SQL Dataprovider composition as well as Grid Columns to show
	 *
	 * @param number     $id Report filter id
	 * @param array|bool $pagination
	 *
	 * @param bool       $customFilter
	 *
	 * @return array
	 * @throws CException
	 */
	public function getReportDataProviderInfo($id, $pagination = false, $customFilter = false, $convertDateToUserFormat = true) {
		return LearningReportFilter::getReportDataProviderInfo($id, $pagination, $customFilter, $convertDateToUserFormat);
	}

	/**
	 * NOT FINISHED, NOT USED  [plamen]
	 *
	 * A replacement of Custom Report Create/Edit process
	 *
	 */
	function actionCustomReportWizard() {

		$fromStep = Yii::app()->request->getParam('from_step', 'initial_create');
		$finishWizardList = Yii::app()->request->getParam('finish_wizard_list', false);
		$finishWizardShow = Yii::app()->request->getParam('finish_wizard_show', false);
		$finishWizardAndSchedule = Yii::app()->request->getParam('finish_wizard_schedule', false);
		$finishWizardNoSchedule = Yii::app()->request->getParam('finish_wizard_no_schedule', false);
		$idReport = Yii::app()->request->getParam('idReport', false);
		$idReportType = Yii::app()->request->getParam('idReportType', false);
		$filter_name = trim(Yii::app()->request->getParam('filter_name', false));

		$nextButton = Yii::app()->request->getParam('next_button', false);
		$prevButton = Yii::app()->request->getParam('prev_button', false);
		$confirmSchedule = Yii::app()->request->getParam('confirm_schedule', false);
		$cancelSchedule = Yii::app()->request->getParam('cancel_schedule', false);

		Yii::app()->event->raise('ReportCreationWizard', new DEvent($this, array(
			'fromStep' => $fromStep,
			'idReport' => $idReport, // set if editing
			'idReportType' => $idReportType,
			'filter_name' => $filter_name, // Report name
		)));


		// In case we are in EDIT mode (exisitng model), take the report type from the model
		$reportModel = LearningReportFilter::model()->findByPk((int) $idReport);
		if ($reportModel && !$idReportType) {
			$idReportType = $reportModel->report_type_id;
		}
		// If we are finishing the wizard, get data
		if ($finishWizardShow || $finishWizardList || $finishWizardAndSchedule || $finishWizardNoSchedule) {
			if (!$reportModel) {
				$reportModel = new LearningReportFilter();
				$reportModel->creation_date = Yii::app()->localtime->toLocalDateTime();
				$reportModel->is_public = 0;
				$reportModel->is_standard = 0;
				$reportModel->author = Yii::app()->user->id;

				// add default visibility option
				$accessReportModel = new LearningReportAccess();
				$accessReportModel->id_report = $reportModel->id_filter;
				$accessReportModel->visibility_type = LearningReportAccess::TYPE_PRIVATE;
				$accessReportModel->save();
			}

			$reportModel->report_type_id = $_REQUEST['Wizard']['report-wizard-step-1-form']['idReportType'];
			$reportModel->filter_name = trim($_REQUEST['Wizard']['report-wizard-step-1-form']['filter_name']);
			if (isset($_REQUEST['Wizard']['report-wizard-order-form']['ReportFieldsForm']['filters']['orderBy']) && $_REQUEST['Wizard']['report-wizard-order-form']['ReportFieldsForm']['filters']['orderBy'] == '0')
				unset($_REQUEST['Wizard']['report-wizard-order-form']);
			// Set helper filter property to request. It will be used by beforeSave() to build final filter_data field
			if ($_REQUEST["Wizard"]["selector-channels"]["channels-filter"] == 1) {
				$_REQUEST["Wizard"]["selector-channels"]["selected_channels"] = '[]';
			}
			if ($_REQUEST["Wizard"]["selector-assets"]["assets-filter"] == 1) {
				$_REQUEST["Wizard"]["selector-assets"]["selected_assets"] = '[]';
			}
			if ($_REQUEST["Wizard"]["selector-experts"]["experts-filter"] == 1) {
				$_REQUEST["Wizard"]["selector-experts"]["selected_users"] = '[]';
			}
			$reportModel->rawFilterData = $_REQUEST;
			$reportModel->save();


			// If Save & Show is selected as finishing step
			if ($finishWizardShow) {
				$redirectUrl = Docebo::createAdminUrl('reportManagement/customView', array('id' => $reportModel->id_filter));
				// Use commonly used Dialog2/JS content to redirect
				$this->renderPartial('lms.protected.views.site._js_redirect', array(
					'url' => $redirectUrl,
					'showMessage' => true,
					'message' => Yii::t('standard', 'Redirecting') . "...",
				));
				Yii::app()->end();
			}

			// ..Or if Schedule -> CONFIRM has been requested
			else if ($finishWizardAndSchedule) {
				try {
					$this->updateScheduledJob($reportModel, $_REQUEST['Wizard']['report-schedule-dialog-form']);
				} catch (CException $e) {
					Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
				}
			}

			echo '<a class="auto-close success"></a>';
			Yii::app()->end();
		}


		// If the report type is USERS COURSEPATH skip showing the top bar with the select all courses suggestion
		if ($reportModel->report_type_id == LearningReportType::USERS_COURSEPATH ||
				(!empty($_REQUEST['Wizard']['report-wizard-step-1-form']['idReportType']) && $_REQUEST['Wizard']['report-wizard-step-1-form']['idReportType'] == LearningReportType::USERS_COURSEPATH)) {
			// Selectors URLs
			$coursesSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
						'type' => UsersSelector::TYPE_CUSTOM_REPORTS_COURSES,
						'idForm' => 'selector-courses',
						'idReport' => $idReport,
						'idReportType' => $idReportType,
						'noyiixviewjs' => true, // very important: main page already has widget(s) loading yiigridview.js, so, don't
			));
		} else {
			// Leave the Top Panel for courses selection for all other report types
			// Selectors URLs
			$coursesSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
						'type' => UsersSelector::TYPE_CUSTOM_REPORTS_COURSES,
						'tp' => 'admin.protected.views.reportManagement.wizard._report_all_courses_top',
						'idForm' => 'selector-courses',
						'idReport' => $idReport,
						'idReportType' => $idReportType,
						'noyiixviewjs' => true, // very important: main page already has widget(s) loading yiigridview.js, so, don't
			));
		}



		$usersSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
			'type'  		=> UsersSelector::TYPE_CUSTOM_REPORTS_USERS,
			'idForm' 		=> 'selector-users',
			'idReport' 		=> $idReport,
			'idReportType' 	=> $idReportType,
			'showSuspended' => true,
			'noyiixviewjs'	=> true,  // very important: main page already has widget(s) loading yiigridview.js, so, don't
			'tabs'			=> ($idReportType != LearningReportType::GROUPS_COURSES) ? false : UsersSelector::TAB_GROUPS . "," . UsersSelector::TAB_ORGCHARTS,
		));

		$certificatonSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
					'type' => UsersSelector::TYPE_CUSTOM_REPORT_CERTIFICATION,
					'tp' => 'admin.protected.views.reportManagement.wizard._report_all_certifications_top',
					'idForm' => 'selector-certification',
					'idReport' => $idReport,
					'idReportType' => $idReportType,
					'noyiixviewjs' => true, // very important: main page already has widget(s) loading yiigridview.js, so, don't
					'tabs' => UsersSelector::TAB_CERTIFICATION,
					'showLeftBar' => false
		));

		$badgesSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
					'type' => UsersSelector::TYPE_CUSTOM_REPORT_BADGES,
					'tp' => 'admin.protected.views.reportManagement.wizard._report_all_or_select_badges_top',
					'idForm' => 'selector-badges',
					'idReport' => $idReport,
					'idReportType' => $idReportType,
					'noyiixviewjs' => true, // very important: main page already has widget(s) loading yiigridview.js, so, don't
					'tabs' => UsersSelector::TAB_BADGES,
					'showLeftBar' => false
		));

		$contestsSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
					'type' => UsersSelector::TYPE_CUSTOM_REPORT_CONTESTS,
					'tp' => 'admin.protected.views.reportManagement.wizard._report_all_contests_top',
					'idForm' => 'selector-contests',
					'idReport' => $idReport,
					'idReportType' => $idReportType,
					'noyiixviewjs' => true, // very important: main page already has widget(s) loading yiigridview.js, so, don't
					'tabs' => UsersSelector::TAB_CONTESTS,
					'showLeftBar' => false
		));

		$assetsSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
					'type' => UsersSelector::TYPE_CUSTOM_REPORT_ASSETS_STATS,
					'tp' => 'admin.protected.views.reportManagement.wizard._report_all_assets_top',
					'idForm' => 'selector-assets',
					'idReport' => $idReport,
					'idReportType' => $idReportType,
					'noyiixviewjs' => true,
		));
		$app7020ExpertsSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
					'type' => UsersSelector::TYPE_CUSTOM_REPORT_EXPERT_STATS,
					'tp' => 'admin.protected.views.reportManagement.wizard._report_all_experts_top',
					'idForm' => 'selector-experts',
					'idReport' => $idReport,
					'idReportType' => $idReportType,
					'noyiixviewjs' => true, // very important: main page already has widget(s) loading yiigridview.js, so, don't
					'tabs' => UsersSelector::TAB_USERS,
 					'showLeftBar' => false
		));

		$channelsSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
					'type' => UsersSelector::TYPE_CUSTOM_REPORT_CHANNELS_STATS,
					'tp' => 'admin.protected.views.reportManagement.wizard._report_all_or_select_channels_top',
					'idForm' => 'selector-channels',
					'idReport' => $idReport,
					'idReportType' => $idReportType,
					'noyiixviewjs' => true, // very important: main page already has widget(s) loading yiigridview.js, so, don't
					'tabs' => UsersSelector::TAB_CHANNELS,
					'showLeftBar' => false
		));

		// Dispatch NEXT action based on where we are coming from and other data
		switch ($fromStep) {

			// Coming from nowhere, initial opening
			case 'initial_create':
				$next = self::WIZ_NEXT_RENDER;
				$nextRender = "step_1";
				$renderParams = array(
					'reportModel' => $reportModel,
					'idReportType' => $idReportType,
				);
				break;


			// Coming FROM: Selecting Report type and giving the report a name
			case 'step1':
				// Go back to step_1 if no report type or report name is provided
				if (!$idReportType) {
					Yii::app()->user->setFlash('error', Yii::t('report', 'Select report type'));
					$next = self::WIZ_NEXT_RENDER;
					$renderParams = array(
						'reportModel' => $reportModel,
						'idReportType' => $idReportType,
					);
					$nextRender = "step_1";
				} else if (empty($filter_name)) {
					Yii::app()->user->setFlash('error', Yii::t('report', 'Select report name'));
					$next = self::WIZ_NEXT_RENDER;
					$renderParams = array(
						'reportModel' => $reportModel,
						'idReportType' => $idReportType,
					);
					$nextRender = "step_1";
				} else {
					// Next URL/Step depends on report type
					$nextUrl = $usersSelectorUrl;
					$next = self::WIZ_NEXT_REDIRECT;

					switch ($idReportType) {
						case LearningReportType::COURSES_USERS:
							$nextUrl = $coursesSelectorUrl;
							break;
						case LearningReportType::AUDIT_TRAIL:
							if (Yii::app()->user->getIsPu()) {
								// Power users skip the user selection step in Audit Trail report creation wizard
								$nextUrl = $coursesSelectorUrl;
							}
							break;
						case LearningReportType::CERTIFICATION_USERS:
							$nextUrl = $certificatonSelectorUrl;
							break;

						case LearningReportType::APP7020_ASSETS:
							$nextUrl = $assetsSelectorUrl;
							break;
						case LearningReportType::USERS_CONTESTS:
							$nextUrl = $usersSelectorUrl;
							break;
						case LearningReportType::APP7020_EXPERTS:
 							$nextUrl = $app7020ExpertsSelectorUrl;
							break;
						case LearningReportType::APP7020_CHANNELS:
							$nextUrl = $channelsSelectorUrl;
							break;
						case LearningReportType::APP7020_USER_CONTRIBUTIONS:
							$nextUrl = $usersSelectorUrl;
							break;
					}
				}
				break;

			// Coming FROM: FIELDS selection and other filtering options
			case 'step4':
				if ($_REQUEST['next_button']) {
					/* DISABLED for now: check  Users x Courses and issue a warning about "too big report"
					  // List of users & courses; get them from Wizard collected data
					  $usersList  	= UsersSelector::getUsersList($_REQUEST['Wizard']['selector-users']);
					  $coursesList	= UsersSelector::getCoursesListOnly($_REQUEST['Wizard']['selector-courses']);
					  $m = count($usersList) * count($coursesList);
					  if ($m > 1000) {
					  $renderParams = array('amountWarning' => Yii::t('standard', 'Generated report might be time and resource consuming due to the amount of selected users, courses and filtering options. We recommend reduce the report size by limiting  your selections.'));
					  }
					 */

					$next = self::WIZ_NEXT_RENDER;
					$nextRender = "order";  // i.e. ordering by
				} else {
					// !!!! Watch out! URLs are Reversed below!!!!!
					if ($idReportType == LearningReportType::CERTIFICATION_USERS || $idReportType == LearningReportType::USERS_EXTERNAL_TRAINING) {
						$nextUrl = $usersSelectorUrl;
						$next = self::WIZ_NEXT_REDIRECT;
					} else if ($idReportType == LearningReportType::USERS_CERTIFICATION) {
						$nextUrl = $certificatonSelectorUrl;
						$next = self::WIZ_NEXT_REDIRECT;
					} else if ($idReportType == LearningReportType::USERS_BADGES) {
						$nextUrl = $badgesSelectorUrl;
						$next = self::WIZ_NEXT_REDIRECT;
					} else if ($idReportType == LearningReportType::USERS_CONTESTS) {
						$nextUrl = $contestsSelectorUrl;
						$next = self::WIZ_NEXT_REDIRECT;
					} else if($idReportType == LearningReportType::APP7020_ASSETS){
						$nextUrl = $assetsSelectorUrl;
						$next = self::WIZ_NEXT_REDIRECT;
 					} else if($idReportType == LearningReportType::APP7020_EXPERTS){
						$nextUrl = $app7020ExpertsSelectorUrl;
						$next = self::WIZ_NEXT_REDIRECT;
					} else if($idReportType == LearningReportType::APP7020_CHANNELS){
						$nextUrl = $channelsSelectorUrl;
						$next = self::WIZ_NEXT_REDIRECT;
 					} else if($idReportType == LearningReportType::APP7020_USER_CONTRIBUTIONS){
						$nextUrl = $usersSelectorUrl;
						$next = self::WIZ_NEXT_REDIRECT;
 					} else{
						$nextUrl = ($idReportType == LearningReportType::COURSES_USERS || $idReportType == LearningReportType::USERS_NOTIFICATIONS) ? $usersSelectorUrl : $coursesSelectorUrl;
						$next = self::WIZ_NEXT_REDIRECT;
					}
				}
				break;

			case 'order':
				if ($_REQUEST['next_button']) {
					$next = self::WIZ_NEXT_RENDER;
					$nextRender = "step_5";
				} else {
					$next = self::WIZ_NEXT_RENDER;
					$nextRender = "step_4";
				}
				break;

			// Coming Step5 (Schedule, Save & Show, Save & List,...)
			case 'step5':
				if ($_REQUEST['prev_button']) {
					$next = self::WIZ_NEXT_RENDER;
					$nextRender = "step_4";
				} else if ($_REQUEST['next_button']) {
					$next = self::WIZ_NEXT_RENDER;
					$nextRender = "schedule";
				}
				break;

			// Coming from Schedule UI, usually means CANCEL Schedule has been requested; Show the normal step5 again
			case 'schedule':
				if ($_REQUEST['prev_button']) {
					$next = self::WIZ_NEXT_RENDER;
					$nextRender = "step_5";
				}
				break;


			// Coming FROM: one of Users/Courses selectors ('uni-selector' is the default name of the selector, hence the "from_step" too)
			case 'uni-selector':

				// Which selector exactly we are coming from ???
				// From courses? Ok, analyze also navigation buttons
				if ($_REQUEST['idForm'] == 'selector-courses') {
					$coursesList = UsersSelector::getCoursesListOnly($_REQUEST);
					$coursepathsList = UsersSelector::getPlansListOnly($_REQUEST);
					if (( $nextButton && ($idReportType == LearningReportType::COURSES_USERS)) ||
							( $prevButton && ($idReportType != LearningReportType::COURSES_USERS))) {

						if ($nextButton && (count($coursesList) <= 0 && $_REQUEST['Wizard']['selector-courses']['courses-filter'] != 1) && ($idReportType != LearningReportType::AUDIT_TRAIL)) {
							Yii::app()->user->setFlash('error', Yii::t('report', '_REPORT_COURSE_SELECTION'));
							$nextUrl = $coursesSelectorUrl;
							$next = self::WIZ_NEXT_REDIRECT;
						} else {
							$nextUrl = $usersSelectorUrl;
							$next = self::WIZ_NEXT_REDIRECT;
							switch ($idReportType) {
								case LearningReportType::AUDIT_TRAIL:
									// Power users skip the user selection step in Audit Trail report creation wizard
									if (Yii::app()->user->getIsPu()) {
										$next = self::WIZ_NEXT_RENDER;
										$nextRender = "step_1";
									}
									break;
							}
						}
					} else if ($nextButton) {
						// Audit Trail report does not require Courses selection, list can be empty
						$countAll = count($coursesList) + count($coursepathsList);
						if (
								((/* count($coursesList) */$countAll <= 0 && $_REQUEST['Wizard']['selector-courses']['courses-filter'] != 1) && ($idReportType != LearningReportType::AUDIT_TRAIL && $idReportType != LearningReportType::USERS_COURSEPATH)) || (count($coursepathsList) <= 0 && $idReportType == LearningReportType::USERS_COURSEPATH )
						) {
							Yii::app()->user->setFlash('error', Yii::t('report', '_REPORT_COURSE_SELECTION'));
							$nextUrl = $coursesSelectorUrl;
							$next = self::WIZ_NEXT_REDIRECT;
						} else {
							$next = self::WIZ_NEXT_RENDER;
							$nextRender = "step_4";
						}
					} else {
						$next = self::WIZ_NEXT_RENDER;
						$nextRender = "step_1";
					}
				}


				// From users? Ok, analyze also navigation buttons
				if ($_REQUEST['idForm'] == 'selector-users'  ) {
					$usersList = UsersSelector::getUsersListOnly($_REQUEST);
					$groupsList = UsersSelector::getGroupsListOnly($_REQUEST);
					$orgChartsList = UsersSelector::getBranchesListOnly($_REQUEST);
					$numUsersSelected = count($usersList);

					$somethingSelected = $numUsersSelected || count($groupsList) || count($orgChartsList);

					if (( $nextButton && ($idReportType != LearningReportType::COURSES_USERS) && ($idReportType != LearningReportType::USERS_NOTIFICATIONS)) ||
							( $prevButton && ($idReportType == LearningReportType::COURSES_USERS))) {

						if ($nextButton && !$somethingSelected && ($idReportType != LearningReportType::AUDIT_TRAIL)) {
							Yii::app()->user->setFlash('error', Yii::t('standard', '_SELECT_USERS'));
							$nextUrl = $usersSelectorUrl;
							$next = self::WIZ_NEXT_REDIRECT;
						} else if ($idReportType == LearningReportType::USERS_EXTERNAL_TRAINING) {
							$next = self::WIZ_NEXT_RENDER;
							$nextRender = 'step_4';
						} else {

							if ($idReportType == LearningReportType::CERTIFICATION_USERS) {
								$nextRender = 'step_4';
								$next = self::WIZ_NEXT_RENDER;
							} else if ($idReportType == LearningReportType::USERS_BADGES) {
								$nextUrl = $badgesSelectorUrl;
								$next = self::WIZ_NEXT_REDIRECT;
							} else if ($idReportType == LearningReportType::USERS_CERTIFICATION) {
								$nextUrl = $certificatonSelectorUrl;
								$next = self::WIZ_NEXT_REDIRECT;
							} else if ($idReportType == LearningReportType::USERS_CONTESTS) {
								$nextUrl = $contestsSelectorUrl;
								$next = self::WIZ_NEXT_REDIRECT;
 							} else if ($idReportType == LearningReportType::APP7020_EXPERTS) {
								$nextUrl = $app7020ExpertsSelectorUrl;
								$next = self::WIZ_NEXT_REDIRECT;
							}  else if ($idReportType == LearningReportType::APP7020_USER_CONTRIBUTIONS) {
								$nextRender = 'step_4';
								$next = self::WIZ_NEXT_RENDER;
							}
							else {
								$nextUrl = $coursesSelectorUrl;
								$next = self::WIZ_NEXT_REDIRECT;
							}
						}
					} else if ($nextButton) {
						// Audit Trail report does not require Users selection, list can be empty
						if (!$somethingSelected && ($idReportType != LearningReportType::AUDIT_TRAIL)) {
							Yii::app()->user->setFlash('error', Yii::t('standard', '_SELECT_USERS'));
							$nextUrl = $usersSelectorUrl;
							$next = self::WIZ_NEXT_REDIRECT;
						} else {
							$next = self::WIZ_NEXT_RENDER;
							$nextRender = "step_4";
						}
					} else {

						if ($idReportType == LearningReportType::CERTIFICATION_USERS) {
							$next = self::WIZ_NEXT_REDIRECT;
							$nextUrl = $certificatonSelectorUrl;
						} else {
							$next = self::WIZ_NEXT_RENDER;
							$nextRender = "step_1";
						}
					}
				}
				if ($_REQUEST['idForm'] == 'selector-badges') {
					$badgesList = UsersSelector::getBadgesListOnly($_REQUEST);
					$numSelected = count($badgesList);
					if ($nextButton) {
						$shouldProceed = false;
						if (isset($_REQUEST['badges-filter']) && $_REQUEST['badges-filter'] == 1) {
							$shouldProceed = true;
						} else if (isset($_REQUEST['badges-filter']) && $_REQUEST['badges-filter'] == 0 && $numSelected == 0) {
							$shouldProceed = false;
						} else {
							$shouldProceed = true;
						}

						if ($shouldProceed) {
							$nextRender = 'step_4';
							$next = self::WIZ_NEXT_RENDER;
						} else {
							Yii::app()->user->setFlash('error', Yii::t('standard', 'Select badges'));
							$nextUrl = $badgesSelectorUrl;
							$next = self::WIZ_NEXT_REDIRECT;
						}
					} else if ($prevButton) {
						$nextUrl = $usersSelectorUrl;
						$next = self::WIZ_NEXT_REDIRECT;
					}
				}

				if ($_REQUEST['idForm'] == 'selector-certification') {
					$certificationList = UsersSelector::getCertificationsListOnly($_REQUEST);
					$numSelected = count($certificationList);

					if (($nextButton && ($idReportType == LearningReportType::CERTIFICATION_USERS)) || ($prevButton && ($idReportType == LearningReportType::CERTIFICATION_USERS)) || ($nextButton && ($idReportType == LearningReportType::USERS_CERTIFICATION)) || ($prevButton && ($idReportType == LearningReportType::USERS_CERTIFICATION))) {
						if ($nextButton) {

							$shouldProceed = false;
							if (isset($_REQUEST['certifications-filter']) && $_REQUEST['certifications-filter'] == 1) {
								$shouldProceed = true;
							} else if (isset($_REQUEST['certifications-filter']) && $_REQUEST['certifications-filter'] == 0 && $numSelected == 0) {
								$shouldProceed = false;
							} else {
								$shouldProceed = true;
							}

							if ($shouldProceed) {
								if ($idReportType == LearningReportType::CERTIFICATION_USERS) {
									$nextUrl = $usersSelectorUrl;
									$next = self::WIZ_NEXT_REDIRECT;
								} else if ($idReportType == LearningReportType::USERS_CERTIFICATION) {
									$nextRender = 'step_4';
									$next = self::WIZ_NEXT_RENDER;
								}
							} else {
								Yii::app()->user->setFlash('error', Yii::t('standard', 'Select certifications'));
								$nextUrl = $certificatonSelectorUrl;
								$next = self::WIZ_NEXT_REDIRECT;
							}
						} else if ($prevButton) {
							if ($idReportType == LearningReportType::CERTIFICATION_USERS) {
								$nextRender = 'step_1';
								$next = self::WIZ_NEXT_RENDER;
							} else if ($idReportType == LearningReportType::USERS_CERTIFICATION) {
								$nextUrl = $usersSelectorUrl;
								$next = self::WIZ_NEXT_REDIRECT;
							}
						}
					} else {
						$next = self::WIZ_NEXT_RENDER;
						$nextRender = "step_1";
					}
				}

				if ($_REQUEST['idForm'] == 'selector-contests') {
					$certificationList = UsersSelector::getContestsListOnly($_REQUEST);
					$numSelected = count($certificationList);

					if (($nextButton && ($idReportType == LearningReportType::USERS_CONTESTS)) || ($prevButton && ($idReportType == LearningReportType::USERS_CONTESTS))) {
						if ($nextButton) {

							$shouldProceed = false;
							if (isset($_REQUEST['contests-filter']) && $_REQUEST['contests-filter'] == 1) {
								$shouldProceed = true;
							} else if (isset($_REQUEST['contests-filter']) && $_REQUEST['contests-filter'] == 0 && $numSelected == 0) {
								$shouldProceed = false;
							} else {
								$shouldProceed = true;
							}

							if ($shouldProceed) {
								$nextRender = 'step_4';
								$next = self::WIZ_NEXT_RENDER;
							} else {
								Yii::app()->user->setFlash('error', Yii::t('standard', 'Select contests'));
								$nextUrl = $contestsSelectorUrl;
								$next = self::WIZ_NEXT_REDIRECT;
							}
						} else if ($prevButton) {
							if ($idReportType == LearningReportType::USERS_CONTESTS) {
								$nextUrl = $usersSelectorUrl;
								$next = self::WIZ_NEXT_REDIRECT;
							}
						}
					} else {
						$next = self::WIZ_NEXT_RENDER;
						$nextRender = "step_1";
					}
				}

				if($_REQUEST['idForm'] == 'selector-assets'){
					$assetsList = UsersSelector::getAssetsListOnly($_REQUEST);
					$channelsList = UsersSelector::getChannelsListOnly($_REQUEST);
					$somethingSelected = count($assetsList) || count($channelsList);

					if(($nextButton && ($idReportType == LearningReportType::APP7020_ASSETS)) || ($prevButton && ($idReportType == LearningReportType::APP7020_ASSETS))){
						if($nextButton){

							$shouldProceed = false;
							if(isset($_REQUEST['assets-filter']) && $_REQUEST['assets-filter'] ==1){
								$shouldProceed = true;
							} else if(isset($_REQUEST['assets-filter']) && $_REQUEST['assets-filter'] == 0 && $somethingSelected == 0){
								$shouldProceed = false;
							} else{
								$shouldProceed = true;
							}

							if($shouldProceed){
								$nextRender = 'step_4';
								$next = self::WIZ_NEXT_RENDER;
							} else{
								Yii::app()->user->setFlash('error', Yii::t('standard', 'Select asset'));
								$nextUrl = $assetsSelectorUrl;
								$next = self::WIZ_NEXT_REDIRECT;
							}
						} else if($prevButton){
							if($idReportType == LearningReportType::APP7020_ASSETS){
								$next = self::WIZ_NEXT_RENDER;
								$nextRender = "step_1";
							}
						}
					} else{
						$next = self::WIZ_NEXT_RENDER;
						$nextRender = "step_1";
					}
				}

				if($_REQUEST['idForm'] == 'selector-experts'){
					$expertsList = UsersSelector::getExpertsListOnly($_REQUEST);
 					$somethingSelected = count($expertsList);

					if(($nextButton && ($idReportType == LearningReportType::APP7020_EXPERTS)) || ($prevButton && ($idReportType == LearningReportType::APP7020_EXPERTS))){
						if($nextButton){

							$shouldProceed = false;
							if(isset($_REQUEST['experts-filter']) && $_REQUEST['experts-filter'] ==1){
								$shouldProceed = true;
							} else if(isset($_REQUEST['experts-filter']) && $_REQUEST['experts-filter'] == 0 && $somethingSelected == 0){
								$shouldProceed = false;
							} else{
								$shouldProceed = true;
							}

							if($shouldProceed){
								$nextRender = 'step_4';
								$next = self::WIZ_NEXT_RENDER;
							} else{
								Yii::app()->user->setFlash('error', Yii::t('standard', 'Select experts'));
								$nextUrl = $app7020ExpertsSelectorUrl;
								$next = self::WIZ_NEXT_REDIRECT;
							}
						} else if($prevButton){
							if($idReportType == LearningReportType::APP7020_EXPERTS){
								$next = self::WIZ_NEXT_RENDER;
								$nextRender = "step_1";
							}
						}
					} else{
						$next = self::WIZ_NEXT_RENDER;
						$nextRender = "step_1";
					}
				}

				if($_REQUEST['idForm'] == 'selector-channels'){
					$channelsList = UsersSelector::getChannelsListOnly($_REQUEST);

					$somethingSelected = count($channelsList);

					if(($nextButton && ($idReportType == LearningReportType::APP7020_CHANNELS)) || ($prevButton && ($idReportType == LearningReportType::APP7020_CHANNELS))){
						if($nextButton){

							$shouldProceed = false;
							if(isset($_REQUEST['channels-filter']) && $_REQUEST['channels-filter'] ==1){
								$shouldProceed = true;
							} else if(isset($_REQUEST['channels-filter']) && $_REQUEST['channels-filter'] == 0 && $somethingSelected == 0){
								$shouldProceed = false;
							} else{
								$shouldProceed = true;
							}

							if($shouldProceed){
								$nextRender = 'step_4';
								$next = self::WIZ_NEXT_RENDER;
							} else{
								Yii::app()->user->setFlash('error', Yii::t('standard', 'Select channels'));
								$nextUrl = $channelsSelectorUrl;
								$next = self::WIZ_NEXT_REDIRECT;
							}
						} else if($prevButton){
							if($idReportType == LearningReportType::APP7020_CHANNELS){
								$next = self::WIZ_NEXT_RENDER;
								$nextRender = "step_1";
							}
						}
					} else{
						$next = self::WIZ_NEXT_RENDER;
						$nextRender = "step_1";
					}
				}

				break;

		}


		// Now take the action being a result of the above navigational checks
		switch ($next) {
			case self::WIZ_NEXT_RENDER:

				// Going to Fields & Options?
				if ($nextRender == 'step_4') {
					$form = array('filters' => array(), 'fields' => array());
					if ($reportModel) {
						$form['fields'] = $reportModel->getSelectionFieldsFromFilter();
						$form['filters'] = $reportModel->getFilteringOptionsFromFilter(true);
					}
					$renderParams['form'] = $form;
				}

				// 'Schedule report' view is used also in NON-wizard envrionment, make it aware THIS TIME it is about wizard
				if ($nextRender == 'schedule') {
					$renderParams['wizardMode'] = true;
					$renderParams['idReportType'] = $_REQUEST['idReportType'];
					$renderParams['idReport'] = $_REQUEST['idReport'];

					if (!$reportModel) {
						$reportModel = new LearningReportFilter();
						$reportModel->filter_name = $_REQUEST['Wizard']['report-wizard-step-1-form']['filter_name'];
						$reportModel->author = Yii::app()->user->id;
						$reportModel->creation_date = Yii::app()->localtime->toLocalDateTime();
					}
					$renderParams['reportModel'] = $reportModel;
				}

				if ($nextRender == 'order') {
					if (!$reportModel) {
						$reportModel = new LearningReportFilter();
						$reportModel->filter_name = $_REQUEST['Wizard']['report-wizard-step-1-form']['filter_name'];
						$reportModel->author = Yii::app()->user->id;
						$reportModel->creation_date = Yii::app()->localtime->toLocalDateTime();
					}
					$selectedFilters = $reportModel->getFilteringOptionsFromFilter();
					$fields = $_POST['ReportFieldsForm']['fields'];



					if (!empty($fields)) {
						foreach ($fields as $key1 => &$field) {
							$counter = count($field);
							$br = 0;
							foreach ($field as $key => &$value) {
								if ($br >= $counter)
									break;
								unset($field[$key]);
								$key = $key1 . '.' . $key;
								$value = $reportModel->getAttributeLabel($key);
								$field[$key] = $value;
								$br++;
							}
						}

						$allowedSortableFields = LearningReportFilter::$remappedConstants;
						foreach ($fields as $key => $v) {
							foreach ($v as $k => $text) {
								if (!array_key_exists($k, $allowedSortableFields)) {
									unset($fields[$key][$k]);
								}
							}
							$v = $fields[$key];
							unset($fields[$key]);
							$key = ucfirst($key);
							$fields[$key] = $v;
						}
					} else {
						$fields = array();
					}

					$renderParams['fields'] = $fields;
					$renderParams['selectedOrder'] = array($selectedFilters['order']['orderBy'], $selectedFilters['order']['type']);
				}
//Log::_($nextRender);
//Log::_($renderParams);
				$this->renderPartial('wizard/' . $nextRender, $renderParams);
				break;

			case self::WIZ_NEXT_REDIRECT:
				$this->redirect($nextUrl, true);
				break;
		}

		Yii::app()->end();
	}

	/**
	 * Initially called by ProgressController action, then start AJAX-calling itself (but with the help of Progress controller view/js.
	 * Exports a report data into CSV file in portions (chunks)
	 *
	 * @throws CException
	 */
	public function actionProgressiveExporter() {

		$response = new AjaxResult();

		// Number of rows to extract per chunk; make it as high as possible
		$defaultLimit = Yii::app()->params['report_export_chunk_size'];

		$idReport = Yii::app()->request->getParam('idReport', false);
		$limit = Yii::app()->request->getParam('limit', $defaultLimit);
		$offset = Yii::app()->request->getParam('offset', 0);
		$start = Yii::app()->request->getParam('start', false);
		$fileName = basename(Yii::app()->request->getParam('fileName', false));
		$totalNumber = Yii::app()->request->getParam('totalNumber', false);
		$download = Yii::app()->request->getParam('download', false);
		$downloadGo = Yii::app()->request->getParam('downloadGo', false);
		$exportType = Yii::app()->request->getParam('exportType', LearningReportFilter::EXPORT_TYPE_CSV);
		$phase = Yii::app()->request->getParam('phase', 'start');

		$customFilter = Yii::app()->request->getParam('custom_filter', array());

		// DOWNLOAD button clicked in the dialog. This is just a signal to close the dialog
		if ($download) {
			echo '<a class="auto-close"></a>';
			Yii::app()->end();
		}

		// Real DOWNLOAD action: a result of JS redirection from the dialog
		if ($downloadGo) {
			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_UPLOADS);
			$storage->sendFile($fileName);
			Yii::app()->end();
		}


		try {
			if (!$idReport) {
				throw new CException('Invalid or missing report ID');
			}

			//$convertDateToUserFormat=false is used for XLS to convert date/time to  environment format which is used in ssconvert from scv to xls
			//$convertDateToUserFormat = ($exportType != LearningReportFilter::EXPORT_TYPE_XLS);
			
			// I don't understand why XLS export is excluded from this (effectively toLocalDatetime())?
			// I am allowing it again [plamen]
			$convertDateToUserFormat = true;
			
			$reportData = $this->getReportDataProviderInfo($idReport, false, $customFilter, $convertDateToUserFormat);

			$dataProvider = $reportData['dataProvider'];
			$columns = $reportData['gridColumns'];
			$reportTitle = $reportData['reportTitle'];

			$csvFileName = Sanitize::fileName($reportTitle . ".csv", true);
			$csvFileName = $fileName ? $fileName : $csvFileName;
			$csvFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $csvFileName;

			$nextOffset = $offset;


			if ($phase == 'start' || $start) {
				$fh = fopen($csvFilePath, 'w');
				$headers = array();
				$values = array();

				// HEADERS
				foreach ($columns as $column) {
					if (isset($column['header'])) {
						$headers[] = $column['header'];
					} else if (isset($column['name'])) {
						$headers[] = $column['name'];
					}
				}

				// Write a HEADER line
				CSVParser::filePutCsv($fh, $headers);
			} else if ($phase == 'extract') {

				$fh = fopen($csvFilePath, 'a');
				$nextOffset = $offset + $limit;
				$sql = $dataProvider->sql;

				if($sql instanceof CDbCommand){
					$sql = $sql->getText();
				}

				$sql = $sql . " LIMIT $offset,$limit";
				$dataReader = Yii::app()->db->createCommand($sql)->query($dataProvider->params);

				$value = null; // reused
				foreach ($dataReader as $data) {
					$values = array();
					foreach ($columns as $field => $column) {
						if ($column['value'] instanceof Closure) {
							// Yii column values can be anonymous or external functions as well, not just strings
							// @see: LearningReportFilter::getSingleColumn()
							$value = call_user_func($column['value'], $data);
						} else {
							eval('$value = ' . $column['value'] . ';');
						}
						LearningReportFilter::columnValuePostProcessing($field, $value, $data);
						$values[] = ($exportType == LearningReportFilter::EXPORT_TYPE_XLS ? "'" : '') . $value;
					}
					CSVParser::filePutCsv($fh, $values);
				}
			} else if ($phase == 'convert') {
				$gnumeric = new Gnumeric();
				$in = $csvFilePath;
				$pathInfo = pathinfo($in);
				$extension = $exportType;
				$fileName = $pathInfo['filename'] . "." . $extension;

				$out = $pathInfo['dirname'] . DIRECTORY_SEPARATOR . $fileName;

				// Use gnumeric to convert the file
				$format = false;

				// Note: this is almost useless, because Gnumeric recognize the output extension and uses correct format
				switch ($exportType) {
					case LearningReportFilter::EXPORT_TYPE_CSV:
						$format = Gnumeric::FORMAT_CSV;
						break;
					case LearningReportFilter::EXPORT_TYPE_XLS;
						$format = Gnumeric::FORMAT_XLS;
						break;

					case LearningReportFilter::EXPORT_TYPE_HTML:
						$format = Gnumeric::FORMAT_HTML;
						break;

					case LearningReportFilter::EXPORT_TYPE_PDF:
						$format = Gnumeric::FORMAT_PDF;
						break;
				}

				// Do the conversion and get result (true, false)
				$result = $gnumeric->convertTo($in, $out, $format);

				if (!$result) {
					throw new CException($gnumeric->getError());
				}

				// Clean up the INPUT file (csv)
				FileHelper::removeFile($in);
			}

			// Close file if we've got a valid file handler
			if ($fh) {
				fclose($fh);
			}

			// We count records the first time we've got a request ('start' phase) and pass the counter to next call to save some work
			if (!$totalNumber) {
				$totalNumber = $dataProvider->getTotalItemCount();
			}

			// Calculate the extraction offset for the NEXT (!) request which we show NOW!
			// i.e. in current render we show what is GOING TO BE done in the next cycle
			$exportingNumber = $nextOffset + $limit;
			$exportingNumber = min($exportingNumber, $totalNumber);

			// Also, in percentage ...
			$exportingPercent = 0;
			if ($totalNumber > 0) {
				$exportingPercent = $exportingNumber / $totalNumber * 100;
			}


			// 'Phase' control
			switch ($phase) {
				case 'start':	// after 'start', we begin extraction
					$nextPhase = 'extract';
					break;
				case 'extract':  // we continue extracttion phase until end-of-records; then switch to 'convert'/'download'
					$nextPhase = $phase;
					if ($nextOffset > $totalNumber) {
						if (($exportType == LearningReportFilter::EXPORT_TYPE_XLS) && ($totalNumber > Gnumeric::MAX_ROWS_XLS)) {
							$exportType = LearningReportFilter::EXPORT_TYPE_CSV;
							$nextPhase = 'download';
							$forceCsvMaxRows = true;
						} else {
							// Check if we have healthy Gnumeric installation
							$gnumeric = new Gnumeric();
							$check = $gnumeric->checkGnumeric();
							if (!$check) {
								$forceCsvMissingGnumeric = true;
								$exportType = LearningReportFilter::EXPORT_TYPE_CSV;
								$nextPhase = 'download';
							} else {
								$nextPhase = $exportType == LearningReportFilter::EXPORT_TYPE_CSV ? 'download' : 'convert';
							}
						}
					}
					break;
				case 'convert':  // after conversion we are going to 'download' the final file
					$nextPhase = 'download';
					break;
			}


			// We completely stop calling ourselves when we reach the "download" phase. Tell this to the ProgressController JS
			$stop = ($nextPhase == 'download') || ($totalNumber <= 0);

			// Render the dialog to show what is going ot be done on NEXT CALL (if any)
			// or to offer final actions, like "Doanload"
			$html = $this->renderPartial('_progressive_exporter', array(
				'reportTitle' => $reportTitle,
				'totalNumber' => $totalNumber,
				'exportingNumber' => $exportingNumber,
				'exportingPercent' => $exportingPercent,
				'fileName' => $fileName,
				'nextPhase' => $nextPhase,
				'exportType' => $exportType,
				'forceCsvMaxRows' => $forceCsvMaxRows,
				'forceCsvMissingGnumeric' => $forceCsvMissingGnumeric,
					), true, true);


			// These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stoppping when $stop == true)
			$data = array(
				'offset' => $nextOffset,
				'stop' => $stop,
				'fileName' => $csvFileName,
				'totalNumber' => $totalNumber,
				'exportType' => $exportType,
				'phase' => $nextPhase,
				'custom_filter' => $customFilter,
			);
			// Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
			$response->setStatus(true);
			$response->setHtml($html)->setData($data)->toJSON();
			Yii::app()->end();
		} catch (CException $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			$response->setStatus(false)->setMessage($e->getMessage());
			$response->toJSON();
			Yii::app()->end();
		}
	}

	/**
	 * Render the "Report management actions dropdown (Schedule, edit, remove, ..); Trigger AND content
	 *
	 * @return string
	 */
	public function renderReportManageActions($rowData) {
		$permissions = CoreUser::getAdminPermissions();
		$actions = $this->renderPartial('_reportActions', array(
			'permissions' => $permissions,
			'data' => $rowData
				), true);
		$content = Chtml::link('<i class="fa fa-ellipsis-h" aria-hidden="true"></i>', 'javascript:void(0);', array(
					'id' => 'popover-' . uniqid(),
					'class' => 'popover-action popover-trigger report-manage-actions-trigger',
					'data-toggle' => 'popover',
					'data-content' => $actions,
		));
		return $content;
	}

	/**
	 *
	 */
	public function actionAxEditSchedule() {

		$id = Yii::app()->request->getParam('id', false);

		$reportModel = LearningReportFilter::model()->findByPk($id);

		if (!$reportModel) {
			$this->renderPartial('admin.protected.views.common._dialog2_error', array(
				'type' => 'error',
				'message' => Yii::t('standard', 'Invalid report'),
			));
			Yii::app()->end();
		}

		if ($_REQUEST['confirm_button']) {

			try {
				$this->updateScheduledJob($reportModel, $_REQUEST);
				echo '<a class="auto-close success"></a>';
				Yii::app()->end();
			} catch (CException $e) {
				Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
				$this->renderPartial('admin.protected.views.common._dialog2_error', array(
					'type' => 'error',
					'message' => $e->getMessage(),
				));
				Yii::app()->end();
			}
		}

		if ($_REQUEST['unschedule_button']) {

			try {
				$job = $reportModel->scheduledJob;
				if ($job) {
					// Remove the job from Sidekiq Scheduler and the job itself
					Yii::app()->scheduler->deleteJob($job);
					$job->delete();
				}
				echo '<a class="auto-close success"></a>';
				Yii::app()->end();
			} catch (CException $e) {
				Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
				$this->renderPartial('admin.protected.views.common._dialog2_error', array(
					'type' => 'error',
					'message' => $e->getMessage(),
				));
				Yii::app()->end();
			}
		}



		// RENDER dialog/editor
		$renderParams = array(
			'jWizard' => false,
			'reportModel' => $reportModel,
		);
		$html = $this->renderPartial('wizard/schedule', $renderParams, true);
		echo $html;
	}

	/**
	 * Resolve/Translate Report schedule editor to Job-aware data
	 *
	 * @param array $request Should be coming out from the $_REQUEST
	 * @return array
	 */
	protected function resolveJobDataFromScheduleEditorRequest($request) {

		switch ($request['recurring_period']) {

			case CoreJob::PERIOD_METANAME_HOURLY:
				$recLength = 1;
				$recPeriod = CoreJob::PERIOD_HOUR;
				break;

			case CoreJob::PERIOD_METANAME_DAILY:
				$recLength = 1;
				$recHour = $request['daily_rec_hour'];
				$recTimeZone = $request['timezone_daily_rec_hour'];
				$recPeriod = CoreJob::PERIOD_DAY;
				break;

			case CoreJob::PERIOD_METANAME_WEEKLY:
				$recLength = 1;
				$recHour = $request['weekly_rec_hour'];
				$recTimeZone = $request['timezone_weekly_rec_hour'];
				$recPeriod = CoreJob::PERIOD_WEEK;
				$recDayOfWeek = $request['rec_day_of_week'];
				break;

			case CoreJob::PERIOD_METANAME_MONTHLY:
				$recLength = 1;
				$recHour = $request['monthly_rec_hour'];
				$recTimeZone = $request['timezone_monthly_rec_hour'];
				$recPeriod = CoreJob::PERIOD_MONTH;
				$recDayOfMonth = $request['rec_day_of_month'];
				break;

			case CoreJob::PERIOD_METANAME_EVERY3MONTHS:
				$recLength = 3;
				$recHour = $request['everynmonth_rec_hour'];
				$recTimeZone = $request['timezone_everynmonth_rec_hour'];
				$recPeriod = CoreJob::PERIOD_MONTH;
				$startDate = $request['start_date'];
				break;

			case CoreJob::PERIOD_METANAME_EVERY6MONTHS:
				$recLength = 6;
				$recHour = $request['everynmonth_rec_hour'];
				$recTimeZone = $request['timezone_everynmonth_rec_hour'];
				$recPeriod = CoreJob::PERIOD_MONTH;
				$startDate = $request['start_date'];
				break;

			case CoreJob::PERIOD_METANAME_YEARLY:
				$recLength = 12;
				$recHour = $request['everynmonth_rec_hour'];
				$recTimeZone = $request['timezone_everynmonth_rec_hour'];
				$recPeriod = CoreJob::PERIOD_MONTH;
				$startDate = $request['start_date'];
				break;
		}


		$data = array(
			'recPeriod' => $recPeriod,
			'recLength' => $recLength,
			'recMinute' => 0,
			'recHour' => $recHour,
			'recTimeZone' => $recTimeZone,
			'recDayOfWeek' => $recDayOfWeek,
			'recDayOfMonth' => $recDayOfMonth,
			'startDate' => $startDate,
		);

		return $data;
	}

	/**
	 * Create new or Update Exisitng a scheduled job and assign it to the passed report model
	 *
	 * @param LearningReportFilter $reportModel
	 * @param array $incomingData Usually data from web request
	 */
	protected function updateScheduledJob($reportModel, $incomingData) {

		// Job parameters, specific for Reports scheduling
		$params = array(
			'id_filter' => $reportModel->id_filter,
			'recipients' => $incomingData['email_recipients'],
			'compressed' => isset($incomingData['compressed']) && $incomingData['compressed'] == 1 ? 1 : 0
		);

		// Resolve Job data from incoming data (e.g. from web request)
		$data = $this->resolveJobDataFromScheduleEditorRequest($incomingData);

		// Get current scheduled job from report data, if any
		$job = $reportModel->scheduledJob;

		// Get he ID of the prospected job handler
 		$handlerId = CustomReportGeneration::id();

		// If there is NO job, create one
		if (!$job) {
			try {
				$transaction = Yii::app()->db->beginTransaction();
				$job = Yii::app()->scheduler->createJob($reportModel->filter_name, $handlerId, CoreJob::TYPE_RECURRING,
					$data['recPeriod'],
					$data['recLength'],
					0,  // minute is always 0
					$data['recHour'],
					$data['recTimeZone'],
					$data['recDayOfWeek'],
					$data['recDayOfMonth'],
					$data['startDate'],
					$params,
					false,
					false,
					false
				);
				$transaction->commit();
			}
			catch (Exception $e) {
				$transaction->rollback();
				throw new CException(Yii::t('standard', 'Could not connect to scheduling service'));
			}
		}
		// Else, modify the old one
		else {
			try {
				$transaction = Yii::app()->db->beginTransaction();
				$job = Yii::app()->scheduler->modifyJob($job, $reportModel->filter_name, $handlerId, CoreJob::TYPE_RECURRING,
					$data['recPeriod'],
					$data['recLength'],
					0, // minute is always 0
					$data['recHour'],
					$data['recTimeZone'],
					$data['recDayOfWeek'],
					$data['recDayOfMonth'],
					$data['startDate'],
					$params,
					false,
					false,
					false
				);
				$transaction->commit();
			}
			catch (Exception $e) {
				$transaction->rollback();
				throw new CException(Yii::t('standard', 'Could not connect to scheduling service'));
			}
		}

		// If job work was successful...
		if ($job) {
			// Assign the job to report
			$reportModel->id_job = $job->id_job;
			
			// Save report  model
			if (!$reportModel->save()) {
				throw new CException('Report model saving failed, but job has been created: ' . $job->id_job);
			}
			return true;
		} else {
			throw new CException('Report Scheduled Job creation/update failed');
		}
	}

	/**
	 * Render "Scheduled" grid column
	 *
	 * @param LearningReportFilter $data
	 * @param integer $index
	 * @return string
	 */
	public function renderScheduledColumn(LearningReportFilter $data, $index) {



		$job = $data->scheduledJob;
		if ($job) {

			// Get schedule info from Job
			$scheduleInfo = $job->getRecurringScheduleInfoText();
			$nextRunInfo = $job->getNextRunFromNowInfoText();


			$nextRunInfo = '<strong>' . Yii::t('report', 'Next Run') . '</strong>: ' . $nextRunInfo;

			if (!$job->active) {
				$inactiveText = ' (' . Yii::t('report', 'paused') . ')';
				$nextRunInfo = '';
			}
			$info = "
				<div class=\"schedule-tooltip-title\"><strong>" . Yii::t('conference', 'Scheduled') . "</strong>$inactiveText: $scheduleInfo</div>
				<div class=\"schedule-tooltip-nextrun\">$nextRunInfo</div>
			";

			$html = Chtml::link('', 'javascript:void(0);', array(
						"class" => "toggle-scheduled-job-status tooltipize docebo-sprite ico-clock" . ($job->active ? ' green ' : ''),
						"title" => $info,
						'data-html' => true,
						'data-job-hash' => $job->hash_id
			));
		} else {
			/*
			  $editScheduleUrl = Docebo::createAdminUrl('reportManagement/axEditSchedule', array(
			  'id' => $data->id_filter
			  ));
			  $info = Yii::t('standard', 'Not scheduled');
			  $html =  CHtml::link('', $editScheduleUrl, array(
			  'class' => 'open-dialog tooltipize docebo-sprite ico-clock',
			  'rel' 	=> 'edit-schedule-report',
			  'data-dialog-title' => Yii::t('standard', '_SCHEDULE'),
			  'title'	=> Yii::t('report', $info)
			  ));
			 */

			$html = "";
		}



		return $html;
	}

	/**
	 * Toggle JOB status (by HASH ID!)
	 */
	public function actionAxToggleJobActiveStatus() {
		$hash_id = Yii::app()->request->getParam('hash_id', false);
		$job = CoreJob::model()->findByHashId($hash_id);
		if ($job) {
			$job->active = $job->active ? 0 : 1;
			$job->save();
		}
		$response = new AjaxResult();
		$response->setStatus(true)->toJSON();
		Yii::app()->end();
	}

	/**
	 * Get assets URL
	 * @return string
	 */
	public function getAdminAssetsUrl() {
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin.js'));
		}
		return $this->_assetsUrl;
	}

	protected function renderDropdownCustomField($idCommon, $name, $select, $prompt = false) {
		$htmlOptions = array('class' => 'custom-dropdown-field');
		if ($prompt) {
			$htmlOptions['prompt'] = $prompt;
		}
		$html = "";
		$field = FieldDropdown::model()->filters()->language()->findByAttributes(array('id_field' => $idCommon));
		if ($field)
			$html = CHtml::dropDownlist($name, $select, $field->getOptions(), $htmlOptions);
		return $html;
	}

	public function actionShowAccessFilter() {
		$visibilityModel = LearningReportAccess::model()->findByAttributes(array('id_report' => intval($_GET['id'])));
		if (!$visibilityModel) {
			$visibilityModel = new LearningReportAccess();
		}
		$errors = array();
		if (isset($_POST['confirm']) && $_POST['confirm'] === Yii::t('standard', '_CONFIRM')) {
			// clear old data
			Yii::app()->db->createCommand()
					->delete(LearningReportAccessMember::model()->tableName(), 'id_report = :id', array(':id' => $visibilityModel->id_report));
			if ($_POST['visibility_type'] == LearningReportAccess::TYPE_SELECTION) {
				if ($_POST['selected_users'] == '[]' && $_POST['selected_groups'] == '[]' && $_POST['orgcharts_selected-json'] == '[]') {
					$errors[] = 'empty selection list';
					Yii::app()->user->setFlash('error', Yii::t('user_managment', '_NO_VALUE'));
				} else {

					// prepare users to be inserted
					$assignedUsers = json_decode($_POST['selected_users']);
					$usersToInsert = array();
					foreach ($assignedUsers as $userId) {
						$usersToInsert[LearningReportAccessMember::TYPE_USER . $userId] = array(
							'id_report' => $visibilityModel->id_report,
							'member_type' => LearningReportAccessMember::TYPE_USER,
							'member_id' => $userId,
						);
					}

					// prepare groups to be inserted
					$assignedGroups = json_decode($_POST['selected_groups']);
					$groupsToInsert = array();
					foreach ($assignedGroups as $groupId) {
						$groupsToInsert[LearningReportAccessMember::TYPE_GROUP . $groupId] = array(
							'id_report' => $visibilityModel->id_report,
							'member_type' => LearningReportAccessMember::TYPE_GROUP,
							'member_id' => $groupId,
						);
					}

					// prepare branches to be inserted
					$assignedBranches = json_decode($_POST['orgcharts_selected-json']);
					$branchesToInsert = array();
					foreach ($assignedBranches as $branch) {
						$branchModel = CoreOrgChartTree::model()->findByAttributes(array('idOrg' => $branch->key));
						$branchesToInsert[LearningReportAccessMember::TYPE_BRANCH . $branch->key] = array(
							'id_report' => $visibilityModel->id_report,
							'member_type' => LearningReportAccessMember::TYPE_BRANCH,
							'member_id' => $branch->key,
							'select_state' => $branch->selectState,
						);
						if ($branch->selectState == 2) {
							$childrenNodes = Yii::app()->db->createCommand()
									->select('idOrg')
									->from(CoreOrgChartTree::model()->tableName())
									->where(array('and', 'iLeft > :left', 'iRight < :right'), array(':left' => $branchModel->iLeft, ':right' => $branchModel->iRight))
									->queryColumn();

							foreach ($childrenNodes as $child) {
								$branchesToInsert[LearningReportAccessMember::TYPE_BRANCH . $child] = array(
									'id_report' => $visibilityModel->id_report,
									'member_type' => LearningReportAccessMember::TYPE_BRANCH,
									'member_id' => $child,
								);
							}
						}
					}
					// no errors, just save the data
					$insertData = array_merge($usersToInsert, $groupsToInsert, $branchesToInsert);
					Yii::app()->getDb()->getCommandBuilder()
							->createMultipleInsertCommand(LearningReportAccessMember::model()->tableName(), $insertData)->execute();
				}
			}
			if (empty($errors)) {
				$visibilityModel->visibility_type = $_POST['visibility_type'];
				$visibilityModel->save();
				echo '<a class="auto-close"></a>';
				?>
				<script>$('#report-management-grid').yiiGridView('update');</script>
				<?php

				Yii::app()->end();
			}
		}
		$this->redirect(Docebo::createLmsUrl('usersSelector/axOpen', array('id' => $visibilityModel->id_report, 'type' => UsersSelector::TYPE_REPORT_VISIBILITY)));
	}

	function actionAssetsAutocomplete() {

		if (Yii::app()->request->isAjaxRequest) {
			$requestData = Yii::app()->request->getParam('data', array());
			$title = '%' . $requestData['query'] . '%';
			$command = Yii::app()->db->createCommand();
			$command->select('id, title');
			$command->from(App7020Assets::model()->tableName() . ' c');
			$command->where("title LIKE :title");
			$result = $command->queryAll(true, array(':title' => $title));

			$assetsArr = array();
			foreach ($result as $asset) {
				$assetsArr[$asset['id']] = $asset['title'];
			}

			$this->sendJSON(array('options' => $assetsArr));
			Yii::app()->end();
		}
	}


	function actionChannelsAutocomplete() {

		if (Yii::app()->request->isAjaxRequest) {
			$language = Yii::app()->getLanguage();
			$defaultLanguage = Lang::getBrowserCodeByCode(Settings::get('default_language', 'english'));
			$requestData = Yii::app()->request->getParam('data', array());
			$name = '%' . $requestData['query'] . '%';
			$command = Yii::app()->db->createCommand();
			$command->select('c.id, trans.name as name');
			$command->from(App7020Channels::model()->tableName() . ' c');
			$command->leftJoin(App7020ChannelTranslation::model()->tableName() . ' trans', 'trans.idChannel = c.id');
			$command->where('trans.lang = :language AND trans.name LIKE :name');
			$command->andWhere('c.enabled = 1 AND c.type != 1 '); //only enabled channels
			$result = $command->queryAll(true, array(':name'=>$name , ':language'=>$language));
			$channelsArr = array();
			foreach ($result as $channel) {
				$channelsArr[$channel['id']] = $channel['name'];
			}
			$this->sendJSON(array('options' => $channelsArr));
			Yii::app()->end();
		}
	}

	public function actionGenerateExpertReport() {
		$expertId = Yii::app()->request->getParam('expertId', false);
		$timeframe = Yii::app()->request->getParam('timeframe', 7);
		$chartQuestions = Yii::app()->request->getParam('chartQuestions', false);
		$chartAssets = Yii::app()->request->getParam('chartAssets', false);

		if ($chartQuestions) {
			$imgQuestionsData = str_replace(' ', '+', $chartQuestions);
			$imgQuestionsData = substr($imgQuestionsData, strpos($imgQuestionsData, ",") + 1);
			$imgQuestionsData = base64_decode($imgQuestionsData);
			$chartQuestionsImgName = 'chartQuestions_' . microtime(true) * 10000 . '_' . substr(md5(uniqid(rand(), true)), 0, 5) . '.jpg';
			//		// Path where the image is going to be saved
            $tmpPdfDir = Yii::app()->basePath . '/runtime/tmpPdf/';
            if (!is_dir($tmpPdfDir)) {
                FileHelper::createDirectory($tmpPdfDir, 0777, true);
            }
            $fileQuestionsPath = $tmpPdfDir . $chartQuestionsImgName;
			//		// Write $imgData into the image file
			$fileQuestions = fopen($fileQuestionsPath, 'w');
			fwrite($fileQuestions, $imgQuestionsData);
			fclose($fileQuestions);
		}

		if ($chartAssets) {
			$imgAssetsData = str_replace(' ', '+', $chartAssets);
			$imgAssetsData = substr($imgAssetsData, strpos($imgAssetsData, ",") + 1);
			$imgAssetsData = base64_decode($imgAssetsData);
			$chartAssetsImgName = 'chartAssets_' . microtime(true) * 10000 . '_' . substr(md5(uniqid(rand(), true)), 0, 5) . '.jpg';
			//		// Path where the image is going to be saved
            $tmpPdfDir = Yii::app()->basePath . '/runtime/tmpPdf/';
            if (!is_dir($tmpPdfDir)) {
                FileHelper::createDirectory($tmpPdfDir, 0777, true);
            }
            $fileAssetsPath = $tmpPdfDir . $chartAssetsImgName;
			//		// Write $imgData into the image file
			$fileAssets = fopen($fileAssetsPath, 'w');
			fwrite($fileAssets, $imgAssetsData);
			fclose($fileAssets);
		}

		$allChartsImgs = array('allChartsImgs' => array(
				'chartQuestions' => $fileQuestionsPath,
				'chartAssets' => $fileAssetsPath
		));

		$coachActivity = App7020QuestionAnswerStatistic::getAllCoachActivity(intval($timeframe), $expertId);
		$peerActivity = App7020PeerReviewStatistic::getAllPeerReviewActivity(intval($timeframe), $expertId);
		$userModel = array('userModel' => CoreUser::model()->findByPk($expertId));
		$additionalInfo = array('timeframeStr' =>  App7020Helpers::timeframeStr($timeframe));

		$renderData = array_merge($allChartsImgs, $coachActivity, $peerActivity, $userModel, $additionalInfo);

		$content = $this->renderPartial('app7020.protected.views.summary.expertSummaryPdf', array('renderData' => $renderData), true);

		$tempKey = substr(md5(uniqid(rand(), true)), 0, 5);
		$pdfFileName = 'expertReport_' . date('m_d_Y') . '_' . $tempKey . '.pdf';

		$css = Yii::app()->theme->basePath . '/css/expertSummary.css';
		$pdfFileName = $this->createPdf($content, $pdfFileName, 'P', $css);
		$this->sendJSON(array('fileName' => $pdfFileName));
	}

	public function actionGenerateQuestionsAnswersPdf() {
		$userId = Yii::app()->request->getParam('userId', FALSE);
		$timeframe = Yii::app()->request->getParam('timeframe', 7);
		$chartQuestionsAnswers = Yii::app()->request->getParam('chartQuestionsAnswers', false);

		// Data for info boxes 'Answered questions', 'Answers likes', 'Answrs dislikes', 'Best answers'
		$questionsAndAnswers = App7020QuestionAnswerStatistic::answersBaseStats(intval($timeframe), $userId);

		$questions = App7020Question::getQuestionsCountByPeriod($userId, $timeframe);

		// Data for info box 'Questions made'
		$questionsMade = array('questionsMade' => App7020Helpers::arrSumByKey($questions, 'count'));

		$renderData = array(
			'timeframeStr' =>  App7020Helpers::timeframeStr($timeframe),
			'chartQuestionsAnswers' => App7020Helpers::makeChartjsJpg($chartQuestionsAnswers, 'chartQuestionsAnswers'),
			'infoBoxes' => array_merge($questionsAndAnswers, $questionsMade),
			'chartMyActivityPerChannel' => App7020PeerReviewStatistic::chartMyActivityPerChannel(intval($timeframe), $userId),
			'rankExpertsByAnswersQuality' => App7020QuestionAnswerStatistic::expertsByAnswersQuality(),
			'rankExpertsByFirstToAnswer' => App7020QuestionAnswerStatistic::topExpertsByAnswer(),
			'rankExpertsByPartecipationRate' => App7020QuestionAnswerStatistic::topExpertsByAnswer(false)
		);


		$content = $this->renderPartial('app7020.protected.views.userReportPdf.questionsAnswers', array('renderData' => $renderData), true);

		$tempKey = substr(md5(uniqid(rand(), true)), 0, 5);
		$pdfFileName = 'userReport_QuestionsAndAnswers_' . date('m_d_Y') . '_' . $tempKey . '.pdf';
		$css = Yii::app()->theme->basePath . '/css/expertSummary.css';
		$pdfFileName = $this->createPdf($content, $pdfFileName, 'P', $css);
		$this->sendJSON(array('fileName' => $pdfFileName));
	}

	public function actionGenerateSharingActivityPdf() {
		$userId = Yii::app()->request->getParam('userId', FALSE);
		$timeframe = intval(Yii::app()->request->getParam('timeframe', 7));
		$chartSharedAssets = Yii::app()->request->getParam('chartSharedAssets', false);
		$chartAssetsViews = Yii::app()->request->getParam('chartAssetsViews', false);
		// Info box 'Shared assets avg. rating'
		$averageRating = App7020SharingStatistics::averageRating($timeframe, $userId);
		// Info box 'Shared contents'
		$sharedContents = App7020SharingStatistics::sharedContents($timeframe, $userId);
		// Info box 'Total views'
		$totalViews = App7020Assets::getAssetsViewsCountByUser($userId, $timeframe);
		// Info box 'Avg. watch rate'
		$averageWatchRate = round(App7020SharingStatistics::averageWatchRate($timeframe, $userId));

		$renderData = array(
			'timeframeStr' =>  App7020Helpers::timeframeStr($timeframe),
			'averageRating' => $averageRating,
			'sharedContents' => $sharedContents,
			'totalViews' => $totalViews,
			'averageWatchRate' => $averageWatchRate,
			'chartMyActivityPerChannel' => App7020SharingStatistics::chartMyActivityPerChannel($timeframe, $userId),
			'chartSharedAssets' => App7020Helpers::makeChartjsJpg($chartSharedAssets, 'chartSharedAssets'),
			'chartAssetsViews' => App7020Helpers::makeChartjsJpg($chartAssetsViews, 'chartAssetsViews')
		);

		$content = $this->renderPartial('app7020.protected.views.userReportPdf.sharedActivity', array('renderData' => $renderData), true);

		$tempKey = substr(md5(uniqid(rand(), true)), 0, 5);
		$pdfFileName = 'userReport_SharedActivity_' . date('m_d_Y') . '_' . $tempKey . '.pdf';
		$css = Yii::app()->theme->basePath . '/css/expertSummary.css';
		$pdfFileName = $this->createPdf($content, $pdfFileName, 'P', $css);
		$this->sendJSON(array('fileName' => $pdfFileName));


	}


	/**
	 * Autocomplete Skill titles for the ReportManagement/index
	 */
	function actionSkillsAutocomplete() {
		if (Yii::app()->request->isAjaxRequest) {
			// Get lang
			$lang = Lang::getBrowserCodeByCode(Settings::get('default_language', 'english'));

			$requestData = Yii::app()->request->getParam('data', array());
			$title = '%' . $requestData['query'] . '%';
			$command = Yii::app()->db->createCommand();
			$command->select('s.id AS id, IF(s_t.title IS NULL OR s_t.title = "", s.title, s_t.title) AS title ');
			$command->from('skill_skills s');

			$command->leftJoin('skill_skills_translations s_t', 's.id = s_t.idSkill AND s_t.lang = :lang', array(':lang' => $lang));

			$command->having("title LIKE :title");
			$result = $command->queryAll(true, array(':title' => $title));

			$assetsArr = array();
			foreach ($result as $asset) {
				$assetsArr[$asset['id']] = $asset['title'];
			}

			$this->sendJSON(array('options' => $assetsArr));
			Yii::app()->end();
		}
	}

	/**
	 * Autocomplete Role titles for the ReportManagement/index
	 */
	function actionRolesAutocomplete() {
		if (Yii::app()->request->isAjaxRequest) {
			// Get lang
			$lang = Lang::getBrowserCodeByCode(Settings::get('default_language', 'english'));

			$requestData = Yii::app()->request->getParam('data', array());
			$title = '%' . $requestData['query'] . '%';
			$command = Yii::app()->db->createCommand();
			$command->select('s.id AS id, IF(s_t.title IS NULL OR s_t.title = "", s.title, s_t.title) AS title ');
			$command->from('skill_roles s');

			$command->leftJoin('skill_roles_translations s_t', 's.id = s_t.idRole AND s_t.lang = :lang', array(':lang' => $lang));

			$command->having("title LIKE :title");
			$result = $command->queryAll(true, array(':title' => $title));

			$assetsArr = array();
			foreach ($result as $asset) {
				$assetsArr[$asset['id']] = $asset['title'];
			}

			$this->sendJSON(array('options' => $assetsArr));
			Yii::app()->end();
		}
	}
}
