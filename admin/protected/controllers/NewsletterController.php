<?php

class NewsletterController extends AdminController {

	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}

	public function accessRules() {
		$res = array();

		$res[] = array(
			'allow',
			'roles' => array('/framework/admin/newsletter/view'),
			'actions' => array(
				'index',
				'handleFile',
				'send',
				'collectUsersList',
				'axGetRecipientsFromSession'
			),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);

		return $res;
	}

	public function actionIndex() {
		Yii::app()->tinymce;
		Yii::app()->plupload;

		$model = new CoreNewsletter('create');

		if (isset($_POST['CoreNewsletter'])) {
			$recipients = Yii::app()->session['newsletterRecipients'];
			$_POST['CoreNewsletter']['msg'] = Yii::app()->htmlpurifier->purify($_POST['CoreNewsletter']['msg']);
			$model->attributes = $_POST['CoreNewsletter'];
			if(isset($_POST['inactive_users']))
				$recipients = $this->filterInactiveRecipients($recipients);

			$model->tot = $recipients !== null ? count($recipients) : 0;
			$model->validate();

			if ($recipients) {
				if ($model->save()) {
					foreach ($recipients as $idst) {
						$sendTo = new CoreNewsletterSendto();
						$sendTo->id_send = $model->id;
						//$sendTo->stime = Yii::app()->localtime->toLocalDateTime();
						$sendTo->idst = $idst;
						$sendTo->save();
					}
					UsersSelector::destroyAllSessionData();
					unset(Yii::app()->session['newsletterRecipients']);

					$event = new DEvent($this, array(
						'newsletter'=>$model->id,
					));
					Yii::app()->event->raise('NewsletterSent', $event);

					$this->redirect(Yii::app()->createUrl('newsletter/send', array('id' => $model->id)));
				}
			}
		} else {
			UsersSelector::destroyAllSessionData();
			unset(Yii::app()->session['newsletterRecipients']);
			unset(Yii::app()->session['newsletterUserList']);
			unset(Yii::app()->session['newsletterUserTempList']);
			unset(Yii::app()->session['newsletterGroupList']);
			unset(Yii::app()->session['newsletterGroupTempList']);
			unset(Yii::app()->session['newsletterOrgchart']);
		}

		//Add page related scripts
		UsersSelector::registerClientScripts();
        Yii::app()->fancytree->init();
        Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/css/DoceboPager.css');

		$this->render('index', array('model' => $model));
	}

	public function actionSend($id) {
		$model = CoreNewsletter::model()->findByPk($id);
		$sendToCount = CoreNewsletterSendto::model()->countByAttributes(array('id_send' => $model->id, 'processed'=>'0'));
		$settingForm = new AdvancedSettingsForm();
		$chunkSize = $settingForm->nl_sendpercycle;
		$emailsSentCount = 0;

		if (!$model || $sendToCount == 0) {
			$this->redirect(Yii::app()->createUrl($this->id.'/index'));
		}

		$sendType = $model->send_type;
		$sendTo = ($model->tot - $sendToCount) / $chunkSize + 1;
		$chunkCount = ceil($model->tot / $chunkSize);
		$chunkFrom = ($sendTo * $chunkSize) - $chunkSize;
		if ($sendTo < $chunkCount) {
			$chunkTo = $sendTo * $chunkSize;
		} else {
			$chunkTo = $model->tot;
		}

		$result = array();
		$result['progress'] = ceil(($chunkTo * 100) / ($model->tot));
		$result['currentChunk'] = $chunkFrom.'-'.$chunkTo;
		$result['pause'] = $settingForm->nl_sendpause * 1000;
		$result['max_file_size'] = 'ok';

		if (isset($_POST['sendTo'])) {
			$sendToNext = $sendTo < $chunkCount ? $sendTo + 1 : $sendTo;
			$chunkFrom = ($sendToNext * $chunkSize) - $chunkSize;
			$chunkTo = $sendToNext < $chunkCount ? $sendToNext * $chunkSize : $model->tot;
			$result['currentChunk'] = $chunkFrom.'-'.$chunkTo;

			if ($sendTo < $chunkCount) {
				$result['status'] = 'send';
			} elseif ($sendTo == $chunkCount) {
				$result['status'] = 'done';
			} else {
				$result['status'] = 'error';
			}

			$criteria = new CDbCriteria();
			$criteria->compare('t.id_send', $model->id);
			$criteria->compare('t.processed', 0);
			$criteria->limit = $chunkSize;
			$criteria->with = 'user';

			$recipients = CoreNewsletterSendto::model()->findAll($criteria);

			if(in_array($sendType, array(CoreNewsletter::SEND_TYPE_EMAIL, CoreNewsletter::SEND_TYPE_INBOX_AND_EMAIL))){
				// Those types send an email

				$attachments = array();
				$sumFileSizes = 0;
				foreach (CJSON::decode($model->file) as $file) {
					$file = Docebo::getUploadTmpPath() . '/' . $file;
					if (file_exists($file)) {
						$attachments[] = $file;
						$sumFileSizes += filesize($file);
					}
				}

				$mailTo = array();
				foreach ($recipients as $i => $item) {
					$mailResult = false;
					// we set limit of 10mb to all files attached
					if ($sumFileSizes <= 10485760 ) {
						if($item->user->email != ''){
							$mail = new MailManager();
							$mailResult = $mail->mail(array($model->fromemail), $item->user->email, $model->sub, $model->msg, $attachments);
						}
					}else{
						$result['status'] = 'error';
						$result['max_file_size'] = 'error';
					}
					
					$recipients[$i]->processed = 1;
					$recipients[$i]->save();

					if($mailResult){
						$emailsSentCount++;
					}
				}

				$result['rec'] = $mailTo;

				// if we are done sending the newsletter we can remove the temp files, even if an error occurs, like maximum file size exceeded
				if ($result['status'] == 'done' || $result['status'] == 'error') {
					Yii::log('DEBUG: Total sent emails: '.$emailsSentCount.'.');
					foreach (CJSON::decode($model->file) as $file) {
						$file = Docebo::getUploadTmpPath() . '/' . $file;
						if (file_exists($file)) {
							@unlink($file);
						}
					}
				} // end if
			}

			if($result['status'] != 'error' && in_array($sendType, array(CoreNewsletter::SEND_TYPE_INBOX, CoreNewsletter::SEND_TYPE_INBOX_AND_EMAIL))){
				// Save this message in the recipient's "Inbox" (see InboxApp)
				$inboxModel = new InboxAppNotification();
				$inboxModel->type = CoreInboxItems::$TYPE_NEWSLETTER_MESSAGE;
				$inboxModel->json_data = json_encode(array(
					'subject'=>$model->sub,
					'body'=>$model->msg,
				));
				$inboxModel->save();

				$userIds = array();
				foreach($recipients as $i=>$item){
					$userIds[] = $item->user->idst;
					$recipientModel = new InboxAppNotificationDelivered();
					$recipientModel->user_id = $item->user->idst;
					$recipientModel->notification_id = $inboxModel->id;
					$recipientModel->save();

					$recipients[$i]->processed = 1;
					$recipients[$i]->save();
				}
			}

			$this->sendJSON($result);
		}

		$this->render('send', array(
			'model' => $model,
			'chunkSize' => $chunkSize,
			'currentChunk' => $result['currentChunk'],
		));
	}


	public function actionHandleFile() {

		if (isset($_FILES['file'])) {
			$fileDir = Docebo::getUploadTmpPath();

			// if time between ajax calls is more than 3 sec, assume that this is another file for upload, so in the next step we give another hash
			if((time()-$_SESSION['most_recent_activity'])>3){
				unset($_SESSION['most_recent_activity']);
				unset($_SESSION['tempFileName']);
			}

			if (file_exists($fileDir . DIRECTORY_SEPARATOR . $_FILES['file']['name'])) {
				unlink($fileDir . DIRECTORY_SEPARATOR . $_FILES['file']['name']);
			}
			if ($_FILES['file']['name'] == 'blob') {

				// if it's a new file, add new hash to the file name
				if ($_FILES['file']['name'] == 'blob' && !isset($_SESSION['tempFileName']) && !isset($_SESSION['most_recent_activity'])) {
					$dotPosition = strpos($_POST['name'], '.');
					$tempFileName = substr($_POST['name'], 0, $dotPosition) . '_' . Docebo::randomHash() . substr($_POST['name'], $dotPosition);
					$_SESSION['tempFileName'] = $tempFileName;
					$_SESSION['most_recent_activity'] = time();
				}
				$_FILES['file']['name'] = $_SESSION['tempFileName'];
			}

			if (file_exists($fileDir) && is_writable($fileDir) && is_dir($fileDir)) {
				$file = $_FILES['file'];
				file_put_contents( $fileDir . DIRECTORY_SEPARATOR . $file['name'], file_get_contents($file['tmp_name']),FILE_APPEND );
			}

			$_SESSION['most_recent_activity'] = time();

			// if this is last portion, rename the file to the original name
			if ($_FILES['file']['size'] < 1048576) {
				rename($fileDir . DIRECTORY_SEPARATOR . $file['name'], $fileDir . DIRECTORY_SEPARATOR . $_POST['name']);
				unset($_SESSION['most_recent_activity']);
				unset($_SESSION['tempFileName']);
			}
		}
	}


	/**
	 * Accept users selection from Selector and save to session, used later to send emails
	 */
	public function actionCollectUsersList() {

		// Get linear list of users (IDs)
		$userlist = UsersSelector::getUsersList($_REQUEST);

		// Reset previously saved list of users
		Yii::app()->session['newsletterRecipients'] = array();
		UsersSelector::destroyAllSessionData();

		if (count($userlist) > 0) {
			// First save current selection in Selector session data so next time we open the dialog we see selected items
			UsersSelector::storeAllDataInSession($_REQUEST);

			// Save the list in session; used later at SEND time by THIS controller
			// We CAN use UserSelector's session data, but let's separate the two processes
			Yii::app()->session['newsletterRecipients'] = $userlist;
		}

		// Close the dialog
		echo '<a class="auto-close success"></a>';

	}


	/**
	 * Called by Newsletter UI to get list of recipients stored in session earlier
	 */
	public function actionAxGetRecipientsFromSession() {

		$recipients = array();
		if (isset(Yii::app()->session['newsletterRecipients'])) {
			$recipients = Yii::app()->session['newsletterRecipients'];
		}

		$response = new AjaxResult();
		$data = array('recipients' => $recipients, 'count' => count($recipients));
		$response->setData($data)->setStatus(true)->toJSON();

	}
        /**
         * Function filters the valid users
         * 
         * @param array $rec - idst of the recipients
         * @return array - filtered recipents, who are only valid users
         */
        private function filterInactiveRecipients($rec) {
			if(empty($rec))
				return $rec;

			$sql = "SELECT idst FROM core_user WHERE valid = 1 AND idst IN (".implode(",",$rec).")";
            $onlyActiveUsers = Yii::app()->db->createCommand($sql)->queryColumn();
            return $onlyActiveUsers;
        }
}
