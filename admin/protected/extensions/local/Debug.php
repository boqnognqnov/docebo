<?php
	/**
	 * irbDebug - Functions of diagnosing of logic errors (trace)
	 * NOTE: Requires PHP version 5 or later
	 * @author    IT studio IRBIS-team
	 * @copyright © 2009 IRBIS-team
	 * @version   0.1
	 * @license   http://www.opensource.org/licenses/rpl1.5.txt
	 */
	///////////////////////////////////////////////////////////////////

	class Debug {

		var $stime = null;
		var $etime = null;

		function __construct() {
		}

		static function init($trace = true, $error_level = 'all') {
			if (!defined('IRB_TRACE') && $trace) {
				define('IRB_TRACE', true);
			}
			switch ($error_level) {
				case 'all'    :
					error_reporting(E_ALL);
					break;
				case 'high'   :
					error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE | E_STRICT);
					break;
				case 'notice' :
					error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
					break;
				case 'simple' :
					error_reporting(E_ERROR | E_WARNING | E_PARSE);
					break;
				case 'off'    :
					error_reporting(0);
					break;
			}
			if (extension_loaded('xdebug')) {
				ini_set('xdebug.var_display_max_data', -1);
				ini_set('xdebug.var_display_max_depth', -1);
			}
			ini_set('display_errors', 'on');
			ini_set('display_startup_errors', 'on');
		}

		########## Trace (Tur)
		/**
		 * Выводит на экран трассировку
		 * @param string $name = "" - название
		 * @param bool   $html = true - выводит в формате хтмл
		 *
		 * @return null
		 *
		qwe();
		function qwe() {
		function qwe2() {
		function qwe3() {
		p(debug::showTrace('trace 1'),'','',1);

		}
		qwe3();
		}
		qwe2();
		}
		 *
		 */
		static function showTrace($name = "", $html = true) {
			$msg   = '{{Stat trace: ' . ($html ? "<b>" : "") . $name . ($html ? "</b>" : "") . '}}' . "\n";
			$exc   = new Exception("Debug Point");
			$trace = $exc->getTrace();
			array_shift($trace);
			$ln = sizeof($trace);
			for ($i = 0; $i < $ln; $i++) {
				if (!isset($trace[$i]["type"])) {
					$trace[$i]["type"] = '';
				}
				if (!isset($trace[$i]["class"])) {
					$trace[$i]["class"] = '';
				}
				$lna  = sizeof($trace[$i]["args"]);
				$args = array();
				for ($j = 0; $j < $lna; $j++) {
					switch (gettype($trace[$i]["args"][$j])) {
						case 'boolean'  :
							$args[$j] = ($trace[$i]["args"][$j] ? "true" : "false");
							break;
						case 'array'    :
							$args[$j] = var_export($trace[$i]["args"][$j], true);
							break;
						case 'resource' :
							$args[$j] = ($html ? "<i style='font-size:75%;'>" : "(") . "Resource" . ($html ? "" : ")") . " " . get_resource_type($trace[$i]["args"][$j]);
							break;
						case 'object'   :
							$args[$j] = ($html ? "<i style='font-size:75%;'>" : "(") . "Object" . ($html ? "</i>" : ")") . " " . get_class($trace[$i]["args"][$j]);
							break;
						case 'NULL'     :
							$args[$j] = "NULL";
							break;
						case 'string'   :
							$args[$j] = "'" . $trace[$i]["args"][$j] . "'";
							break;
					}
				}
				$msg .= ($html ? "<b>" : "") . "#$i" . ($html ? "</b>" : "") . " " . $trace[$i]["file"] . "(" . $trace[$i]["line"] . "): " . ($html ? "<b>" : "") . $trace[$i]["class"] . $trace[$i]["type"] . $trace[$i]["function"] . ($html ? "</b>" : "") . "(" . (empty($trace[$i]["args"]) ? "" : implode(", ", $args)) . ")\n";
			}
			$msg .= '{{End trace: ' . ($html ? "<b>" : "") . $name . ($html ? "</b>" : "") . '}}';
			$msg .= "\n\n";
			if ($html) {
				$msg = nl2br($msg);
			}
			return $msg;
		}

		########## End Trace

		static function setSTime() {
			return microtime();
		}

		//    Example:
		//    $stime = debug::setSTime();
		//    sleep(5);
		//    debug::setETime($stime, 'test');

		static function setETime($startTime, $name = null) {
			$mtimeEnd   = explode(" ", microtime());
			$mtimeStart = explode(" ", $startTime);
			$tpassed    = $mtimeEnd[1] + $mtimeEnd[0] - $mtimeStart[1] - $mtimeStart[0];
			if ($name == null) {
				pr($tpassed, 0);
			} else {
				pr($name . '_' . $tpassed, 0);
			}
			return $tpassed;
		}

		function printVars($var1 = null, $var2 = null, $var3 = null, $exit = 0) {
			if ($var1 != null) {
				$this->printVar($var1, $exit);
			}
			if ($var2 != null) {
				$this->printVar($var2, $exit);
			}
			if ($var3 != null) {
				$this->printVar($var3, $exit);
			}
		}

		/**
		 * @static
		 *
		 * @param      $var
		 * @param bool $exit
		 *
		 * @return void
		 */
		static function printVar(&$var, $exit = false) {
			if (IRB_TRACE) {
				static $num = 0;
				++$num;
				if (!Yii::app()->request->isAjaxRequest) {
					?>
					<div style="background-color:#F0F0F0; width:80%; padding-left:20px; margin-left:5%; border:1px solid;">
						<div><h3 style="color:#0000CC; background-color:#D0D0D0;">TRACE № <?php echo $num; ?>.</h3></div>
						<?php

						$trace = debug_backtrace();
						$old = $var;
						$var = microtime();
						$vname = false;
						$vstring = '<b style="color:blue;">';

						foreach ($GLOBALS as $key => $val) {
							if ($val === $var) {
								$vname = $key;
							}
						}

						if (empty($vname) && isset($_POST)) {
							foreach ($_POST as $key => $val) {
								if ($val === $var) {
									$vname = '_POST[<span style="color:red;">"' . $key . '"</span>]';
								} elseif (empty($vname) && isset($_GET)) {
									foreach ($_GET as $key => $val) {
										if ($val === $var) {
											$vname = '_GET[<span style="color:red" >"' . $key . '"</span>]';
										} elseif (empty($vname) && isset($_SESSION)) {
											foreach ($_SESSION as $key => $val) {
												if ($val === $var) {
													$vname = '_SESSION[<span style="color:red" >"' . $key . '"</span>]';
												} elseif (empty($vname) && isset($_COOKIE)) {
													foreach ($_COOKIE as $key => $val) {
														if ($val === $var) {
															$vname = '_COOKIE[<span style="color:red" >"' . $key . '"</span>]';
														}
													}
												}
											}
										}
									}
								}
							}
						}

						$var = $old;

						if (empty($vname) && empty($var)) {
							$vstring .= '&nbsp;</b><b style="color:red">The variable is not defined in function</b><br>';
						} elseif (empty($var)) {
							$vstring .= '&nbsp;</b><b style="color:red">The variable is not defined or empty</b><br>';
						} elseif (empty($vname)) {
							$vstring .= '&nbsp;</b><b style="color:green">It was not possible to define a variable name</b><br>';
						} else {
							$vstring .= '$' . $vname . '</b><b style="color:green"> = </b>';
						}

						echo  '<b style="color:#000000;">File: </b><b style="color:#CC0066">' . $trace[1]['file'] . '</b><br>';
						echo  !empty($trace[1]['class']) ? '<b style="color:#000000;">Class: </b><b style="color:#CC0066">' . $trace[1]['class'] . '</b><br>' : NULL;
						echo  (!empty($trace[1]['function']) ? '<b style="color:#000000;">Function: </b><b style="color:#CC0066">' . $trace[1]['function'] : '<b style="color:#000000;">GLOBALS') . '</b><br>';
						echo  !empty($trace[1]['function']) ? '<b style="color:#000000;">Line: </b><b style="color:#CC0066">' . $trace[1]['line'] . '</b><br><br>' : '';

						echo $vstring . '<pre style="color:#000000;">';
						self::printVarDamp($var);
						echo '</pre>';
						if ($exit) {
							echo '<b>exit</b>';
						}
						?>
					</div>
				<?php } else {
					$trace = debug_backtrace();
					echo '<<|'
						.(!empty($trace[1]['file']) ? $trace[1]['file']:'').'|'
						.(!empty($trace[1]['class']) ? $trace[1]['class']: '').'|'
//						.(!empty($trace[1]['function']) ? $trace[1]['function'] : '') .'|'
						.(!empty($trace[1]['line']) ? $trace[1]['line'] : '') .'[ ';
					self::printVarDamp($var, true);
					echo ' ]>>'.PHP_EOL;
				}
				if ($exit) {
					exit();
				}
			}
		}

		static function printVarDamp($var, $isAjax = false) {
			if (is_array($var) || is_object($var) || $isAjax) {
				print_r($var);
			} elseif (isset($var)) {
				var_dump($var);
			}
			return true;
		}

	}

