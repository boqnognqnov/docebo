<?php

class ModelSelectBehavior extends CActiveRecordBehavior {

	/**
	 * Checks if $ownerModel->someField exists in current session $list.
	 * Used to preserve various statuses across page loads
	 * 
	 * @param string $list
	 * @param string $fieldName
	 * @return boolean
	 */
	public function inSessionList($list, $fieldName) {
		if (is_string($list)) {
			if (isset(Yii::app()->session[$list]) && is_array(Yii::app()->session[$list])) {
				return in_array($this->owner->$fieldName, Yii::app()->session[$list]);
			}
		}
		return false;
	}

}