<?php

	Yii::import('qsExt.qs.lib.web.widgets.QsLinkPager');

	class DoceboLinkPager extends QsLinkPager {

		protected $_view = 'admin.protected.views.pagers.doceboPager';

		public $header = '';


		/**
		 * Creates a page button.
		 *
		 * @param string  $label    the text label for the button.
		 * @param integer $page     the page number.
		 * @param string  $class    the CSS class for the page button.
		 * @param boolean $hidden   whether this page button is visible.
		 * @param boolean $selected whether this page button is selected.
		 *
		 * @return string|array the generated button.
		 */
		protected function createPageButton($label, $page, $class, $hidden, $selected) {
			if (empty($this->_view)) {
				return parent::createPageButton($label, $page, $class, $hidden, $selected);
			} else {
				if ($hidden || $selected) {
					$class .= ' ' . ($hidden ? self::CSS_HIDDEN_PAGE : self::CSS_SELECTED_PAGE);
				}

				$button = array(
					'label' => $label,
					'class' => $class,
					'url' => CHtml::normalizeUrl($this->createPageUrl($page)),
					'page' => $page,
					'hidden' => $hidden,
					'selected' => $selected,
				);
				return $button;
			}
		}

		/**
		 * Creates the page buttons.
		 * @return array a list of page buttons (in HTML code).
		 */
		protected function createPageButtons() {
			if (($pageCount = $this->getPageCount()) <= 1) {
				return array();
			}

			list($beginPage, $endPage) = $this->getPageRange();
			$currentPage = $this->getCurrentPage(false); // currentPage is calculated in getPageRange()
			$buttons     = array();

			if ($currentPage == 0) {
				$buttons[] = array(
					'label' => '',
					'class' => 'prev-page disabled',
					'url' => '',
				);
			} else {
				$buttons[] = $this->createPageButton('', $currentPage - 1, 'prev-page', false, false);
			}

			// internal pages
			$separated = false;
			$i         = ($currentPage - 3) > $beginPage ? ($currentPage - 1) : $beginPage;
			$j         = 0;

			if ($currentPage > 3) {
				$buttons[] = $this->createPageButton(1, 0, $class, false, false);
				$buttons[] = $this->createPageButton(2, 1, $class, false, false);

				$buttons[] = array(
					'label' => '...',
					'class' => 'separator',
					'url' => '',
				);
			}

			for ($i; $i < $pageCount; ++$i) {
				if ($j == 0) {
					$class = $this->internalPageCssClass . ' first';
				} else {
					$class = $this->internalPageCssClass;
				}

				if ($i == $currentPage) {
					$buttons[] = array(
						'label' => $i + 1,
						'class' => $class . ' selected',
						'url' => '',
					);

					$j = 1;
					continue;
				}

				$buttons[] = $this->createPageButton($i + 1, $i, $class, false, $i == $currentPage);
				//  && ($endPage - 2) > $i
				if (!$separated && $i == $currentPage + 1 && ($pageCount - 3) > $i) {
					// echo $endPage;
					// exit;
					// add separator
					$buttons[] = array(
						'label' => '...',
						'class' => 'separator',
						'url' => '',
					);

					$i         = $pageCount - 3;
					$separated = true;
					continue;
				}

				$j = 1;
			}

/*			if (($currentPage + 1) >= $pageCount) {
				$buttons[] = array(
					'label' => 'Last',
					'class' => 'last-page disabled',
					'url' => '',
				);
			} else {
				$buttons[] = $this->createPageButton('Last', $pageCount - 1, 'last-page', false, false);
			}

			if ($currentPage == 0) {
				$buttons[] = array(
					'label' => '',
					'class' => 'prev-page disabled',
					'url' => '',
				);
			} else {
				$buttons[] = $this->createPageButton('', $currentPage - 1, 'prev-page', false, false);
			}*/

			if (($currentPage + 1) >= $pageCount) {
				$buttons[] = array(
					'label' => '',
					'class' => 'next-page disabled',
					'url' => '',
				);
			} else {
				$buttons[] = $this->createPageButton('', $currentPage + 1, 'next-page', false, false);
			}

			return $buttons;
		}
	}