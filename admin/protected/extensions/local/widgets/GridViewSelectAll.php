<?php

class GridViewSelectAll extends CWidget {

	/*	Widget usage example:

	$this->widget('ext.local.widgets.GridViewSelectAll', array(

	));

	*/
	public $gridId;
	public $dataProvider;
	public $class = '';
	public $itemValue;
	public $sessionName;

	public function init() {
		$this->dataProvider->pagination = false;
		echo $this->getContent();
	}

	protected function getContent() {
		$content = '<p>'.Yii::t('standard', 'You have selected').' <strong><span id="'.$this->gridId.'-selected-items-count">0</span> '.Yii::t('standard', 'items').'</strong>.</p>';
		$content .= '<a href="javascript:void(0);" class="select-all">'.Yii::t('standard', '_SELECT_ALL').'</a>';
		$content .= '<a href="javascript:void(0);" class="deselect-all">'.Yii::t('standard', '_UNSELECT_ALL').'</a>';

		$content .= '<div id="'.$this->gridId.'-all-items">';
		$content .= CHtml::hiddenField($this->gridId.'-all-items-list', $this->getAllItemsList());
		$content .= CHtml::hiddenField($this->gridId.'-selected-items-list', $this->getSelectedItemsList());
		$content .= CHtml::hiddenField($this->gridId.'-all-items-count', $this->dataProvider->getTotalItemCount());
		$content .= '</div>';

		$html = CHtml::tag('div', array(
			'class' => trim('grid-selections '.$this->class),
			'data-grid' => $this->gridId,
		), $content);

		return $html;
	}
	
	
	
	
	protected function getAllItemsList() {
		
		$items = array();
		
		if (get_class($this->dataProvider) != 'CActiveDataProvider') {
			return $this->getAllItemsListOld();
		}

		// Get the grid dataprovider criteria and class name
		$criteria = clone $this->dataProvider->criteria;
		$modelClass = $this->dataProvider->modelClass;

		// Grid items must have a key value (unique). Grid caller is rsponsible for this and defines the key either by 
		//   1) passing a field name, like 'idst'  or
		//   2) passing an expression to be evaluated in form of  '$data->idst'
		// We recognize this by the '$' sign as 1st character. This is VERY ugly, but we pay the price for the ugly grids anyway!
		// Perhaps Grid caller (when creating the widget) must be asked to specify if this is an expression or not. 
		// @TODO: improve this please
		// Hint: search for 'itemVlaue'
		
		// If the item key is to be evaluated we must select '*', no escape from this for now.
		// Otherwise, we select ONLY the field that is required to build the item keys list
		
		
		$criteria->select = '*';
		if ( $this->itemValue !== null ) {
			if (strpos($this->itemValue, '$') === 0) {
				$itemValueEval = true;
			}
			else {
				$itemValueEval = false;
				$criteria->select = $this->itemValue;
			}
		}
		
		// Create another dataProvider, not sorted,  pagination false, to get all the records
		$config = array();
		$config['criteria'] = $criteria;
		$config['sort'] = false;
		$config['pagination'] = false;
		$dataProvider = new CActiveDataProvider($modelClass, $config);

		if($this->itemValue !== null && !$itemValueEval) {
			$value = $this->itemValue;
			$tableAlias = $this->dataProvider->model->getTableAlias();

			$criteria = clone $this->dataProvider->criteria;
			$criteria->select = array(
				'*',
				new CDbExpression("GROUP_CONCAT(DISTINCT $tableAlias.$this->itemValue SEPARATOR ',') AS $value"),
			);

			Yii::app()->db->createCommand('SET SESSION group_concat_max_len = 5000000')->execute();

			$dataProvider = new CActiveDataProvider($this->dataProvider->modelClass, array(
				'criteria' => $criteria,
				'pagination' => false,
			));
			$result = $dataProvider->getData();

			if(count($result) > 1) {
				$tmp = array();
				foreach($result as $res)
					$tmp[] = $res->$value;

				return implode(',', $tmp);
			} elseif(isset($result[0])) {
				return $result[0]->$value;
			}
		} else {
			// Get data and keys
			$start = microtime(true);
			$dataSet = $dataProvider->data;
			$end = microtime(true);
			$keys = $dataProvider->keys;

			// Enumerate and build the items list (keys)
			foreach ($dataSet as $row => $data) {
				if($this->itemValue !== null && $itemValueEval) {
					$items[] = $this->evaluateExpression($this->itemValue, array(
						'data' => $data,
						'row' => $row,
					));
				} else {
					$items[] = $keys[$row];
				}
			}
		}

		return implode(',', $items);
	}
	
	
	/**
	 * The old method. 
	 * @return string
	 */
	protected function getAllItemsListOld() {
		$items = array();
	
		$itemValueEval = false;
		if ( $this->itemValue !== null ) {
			if (strpos($this->itemValue, '$') === 0) {
				$itemValueEval = true;
			}
			else {
				$itemValueEval = false;
			}
		}
		
		foreach ($this->dataProvider->data as $row => $data) {
			if($this->itemValue !== null) {
				if (!$itemValueEval) {
					$items[] = $data[$this->itemValue];
				}
				else {
					$items[] = $this->evaluateExpression($this->itemValue, array(
						'data' => $data,
						'row' => $row,
					));
				}
			} else {
				$items[] = $this->dataProvider->keys[$row];
			}
		}
	
		return implode(',', $items);
	}
	
	protected function getSelectedItemsList() {
		if (is_string($this->sessionName) && isset(Yii::app()->session[$this->sessionName])) {
			return implode(',', Yii::app()->session[$this->sessionName]);
		} elseif (isset(Yii::app()->session['selectedItems'])) {
			return implode(',', Yii::app()->session['selectedItems']);
		} else {
			return '';
		}
	}

}
