<?php

	class TabsWidget extends CWidget {

		/*	Widget usage example:

			$this->widget('ext.local.widgets.TabsWidget', array(
					'tabs' => array(
						array(
							'title' => 'Tab 1',
							'container' => 'users-tab',
							'class' => 'users-tab',
							'url' => 'modal/tab',
							'urlParams' => array('view' => 1),
						),
						array(
							'title' => 'Tab 2',
							'class' => 'second-tab',
							'container' => 'second-tab',
							'url' => 'modal/tab',
							'urlParams' => array('view' => 2),
						),
						array(
							'title' => 'Tab 3',
							'class' => 'third-tab',
							'container' => 'third-tab',
							'url' => 'modal/tab',
							'urlParams' => array('view' => 3),
						),
					),
			));
		*/

		public $tabs = array();

		protected function renderModalBody() {
			$controller = Yii::app()->controller;

			$ul = "<ul class='nav nav-tabs' id='tabs-'" . $this->id . ">";
			foreach ($this->tabs as $key => $tab) {
				$active = $key == 0 ? 'active' : '';
				$ul .= '<li class="' . $active . '"><a href="#' . $tab['container'] . '" data-source="' . $controller->createUrl($tab['url'], $tab['urlParams']) . '">' . $tab['title'] . '</a></li>';
				$containers .= '<div class="tab-pane" id="' . $tab['container'] . '" data-loaded="0"></div>';
			}
			$ul .= "</ul>";

			$body = '<div class="tab-content">' . $containers . '</div>';

			return $ul . $body;
		}

		public function init() {
			echo $this->renderModalBody();
			echo "<script type='text/javascript'>
		    	var active = $('.modal .nav-tabs .active a')
			    active.click();
			    $(active.attr('href')).addClass('active');
	    </script>";
		}
	}