<?php
	Yii::import('zii.widgets.CListView');

	class CXListView extends CListView {

		public function init() {

			if ($this->itemView === null) {
				throw new CException(Yii::t('zii', 'The property "itemView" cannot be empty.'));
			}

			/*********/

			if($this->dataProvider===null)
				throw new CException(Yii::t('zii','The "dataProvider" property cannot be empty.'));

			$this->dataProvider->getData();

			$this->htmlOptions['id']=$this->getId();

			if($this->enableSorting && $this->dataProvider->getSort()===false)
				$this->enableSorting=false;
			if($this->enablePagination && $this->dataProvider->getPagination()===false)
				$this->enablePagination=false;
			/********/


			if (!isset($this->htmlOptions['class'])) {
				$this->htmlOptions['class'] = 'list-view clearfix';
			}
			if ($this->baseScriptUrl === null) {
				$this->baseScriptUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('adminExt.local.widgets.assets')) . '/listview';
			}

			if ($this->cssFile !== false) {
				if ($this->cssFile === null) {
					$this->cssFile = $this->baseScriptUrl . '/styles.css';
				}
				Yii::app()->getClientScript()->registerCssFile($this->cssFile);
			}

			$this->emptyText = Yii::t('report', '_NULL_REPORT_RESULT');
		}

	}
