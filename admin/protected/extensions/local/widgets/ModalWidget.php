<?php

class ModalWidget extends CWidget {

	/*	Widget usage example:

		$this->widget('ext.local.widgets.ModalWidget', array(
			'type' => 'link',
				'handler' => array(
					'title' => 'Open modal',
					'htmlOptions' => array(
						'class' => 'open-modal-link',
					),
				),
			),
			'headerTitle' => 'Header Text',
			'url' => 'modal/modalTest',
			'urlParams' => array(),
			'buttons' => array(
				array(
					'title' => 'Close',
					'type' => 'close',
					'class' => 'close-btn',
				),
			),
	));
*/
	public $uniqueId;
	public $modalClass = "";
	public $handlerClass = "";
	public $type = 'link';

	/**
	 * @property array Settings for popup main handler
	 */
	public $handler;

	public $headerTitle = '';

	/**
	 * @property string Url router params for main handler of popup
	 */
	public $url = '';

	/**
	 * @property array Additional url params for main handler of popup
	 */
	public $urlParams = array();
	
	/**
	 * @property string Type of URL to be generated (admin/lms)
	 */
	public $urlType = 'lms'; // lms|admin

	/**
	 * @property string Type of data, which is sent by server for content of popup
	 */
	public $remoteDataType = 'html';

	/**
	 * @property boolean If true, content will be updated every time when popup is shown
	 */
	public $updateEveryTime = false;

	public $buttons = array();

	public $beforeShowModalCallBack = '';

	public $handleUrlBeforeSend = '';
	public $preProcessAjaxResponse = '';


	protected $_mainModalClass = 'modal fade';

	public function renderModal() {
		$handler = $this->renderModalHandler();
		$body = $this->renderModalHeader();
		$body .= $this->renderModalBody();
		$body .= $this->renderModalFooter();

		return $this->renderWrapper($handler, $body);
	}

	protected function renderWrapper($handler, $body) {
		// $html = $handler;
		// echo $handler;
		$html = '<div id="modal-'.$this->uniqueId.'" class="'.$this->_mainModalClass.' '.$this->modalClass.'" aria-labelledby="label-'.$this->uniqueId.'">';
		$html .= $body;
		$html .= '</div>';

		if (Yii::app()->request->isAjaxRequest) {
			//$modalUrl = Yii::app()->createUrl($this->url, $this->urlParams);
			if($this->urlType=='admin'){
				$modalUrl = Docebo::createAdminUrl($this->url, $this->urlParams);
			}else{
				$modalUrl = Docebo::createLmsUrl($this->url, $this->urlParams);
			}
			echo '<script type="text/javascript">if (!$("#modal-' . $this->uniqueId . '").length) $("body").append(\'' . $html . '\'); $("#modal-' . $this->uniqueId . '").data("url", "' . $modalUrl . '");</script>';
		} else {
			Yii::app()->clientScript->registerScript('modal-append-' . $this->uniqueId, '$("body").append(\'' . $html . '\');', CClientScript::POS_END);
		}

		return $handler;
	}

	protected function renderModalHandler() {

		if ($this->handlerClass == '') {
			$this->handlerClass = $this->modalClass;
		}

		$htmlOptions = array(
			'class' => !empty($this->handler['class']) ? $this->handler['class'] : "",
		);

		if (isset($this->handler['htmlOptions'])) {
			$htmlOptions = array_merge($htmlOptions, $this->handler['htmlOptions']);
		}
		// add special class for handler
		$htmlOptions['class'] = trim($htmlOptions['class'].' '.$this->handlerClass.'-handler');

		$htmlOptions['modal-url'] = Docebo::createAppUrl($this->url, $this->urlParams);
		$htmlOptions['data-keyboard'] = 1;
		$htmlOptions['modal-title'] = $this->headerTitle;
		Yii::app()->clientScript->registerScript('modal-handlers-'.$this->uniqueId, '
					$(document).on(\'click\', \'.'.$this->handlerClass.'-handler\', function() {
						if ($(this).attr(\'modal-url\') != undefined) {
							$("#modal-'.$this->uniqueId.'").data("url", $(this).attr(\'modal-url\'));
						}
						else {
							$("#modal-'.$this->uniqueId.'").data("url", $(this).attr(\'href\'));
						}

						if ($(this).data(\'modal-class\') == undefined) {
							$("#modal-'.$this->uniqueId.'").attr("class", "'.$this->_mainModalClass.' '.$this->modalClass.'");
						} else {
							$("#modal-'.$this->uniqueId.'").attr("class", "'.$this->_mainModalClass.' "+$(this).data(\'modal-class\'));
						}

						$("#modal-'.$this->uniqueId.'").data("modal-send-data", $(this).data("modal-send-data"));
						$("#label-'.$this->uniqueId.'").html($(this).attr(\'modal-title\'));

						$("#modal-'.$this->uniqueId.'").modal(\'show\');

						return false;
					});
				');

		if (Yii::app()->request->isAjaxRequest) {
			echo '<script>'.$this->getModalContentScript().'</script>';
		} else {
			Yii::app()->clientScript->registerScript('modal-'.$this->uniqueId, $this->getModalContentScript());
		}
		switch ($this->type) {
			case 'button':
				return CHtml::button($this->handler['title'], $htmlOptions);
			case 'link':
				return CHtml::link($this->handler['title'], '#', $htmlOptions);
		}
	}

	protected function getModalContentScript() {
		$script = '
				if (!$("#modal-' . $this->uniqueId . '").data("url")) {
					$("#modal-' . $this->uniqueId . '").data("url", "' . Yii::app()->createUrl($this->url, $this->urlParams) . '");
				}
				if ($("#modal-'.$this->uniqueId.'").attr("firstShow") != false) {
					$("#modal-'.$this->uniqueId.'").attr("firstShow", true);
				}

				$(document).on("show", "#modal-' . $this->uniqueId . '", function (e) {
					if ($(e.target).hasClass("modal")) {
						var th = $(this);
						if ((th.attr("firstShow") == "false") && ('.(int)$this->updateEveryTime.' == false)) {
							return true;
						}
						data = th.data("modal-send-data");
						if (data == undefined) {
							data = {};
						}

						var url = th.data("url");
						'.(!empty($this->handleUrlBeforeSend) ? $this->handleUrlBeforeSend : '').';

						$.extend(data, {firstShow: th.attr("firstShow")});
						$.ajax({
							"dataType" : "'.$this->remoteDataType.'",
							"type" : "POST",
							"data" : data,
							"url" : url,
							"cache" : false,
							"success" : function (data) {
								th.attr("firstShow", false);

								'.(!empty($this->preProcessAjaxResponse) ? $this->preProcessAjaxResponse : '').'

								if (this.dataType == "json") {
									if (data && data.html) {
										th.find(".modal-body").html(data.html);
									} else {
										$(\'#modal-'. $this->uniqueId .'\').modal(\'hide\');
									}
								}
								else {
									th.find(".modal-body").html(data);
								}
								$(\'.new-course.in input\').styler({browseText: ' . CJSON::encode(Yii::t('organization', 'Upload File')) . '});
								$(\'#course_form input[type="file"]\').styler({browseText: "BROWSE"});

								// Apply styler for THIS modal only!!
								$(e.target).find(\'input, select\').styler();

								modalPosition();
								$(window).resize(function(){
									modalPosition();
								}).trigger(\'resize\');

								//$(\'#user_form, #orgChart_form, #add-field-form\').jClever({applyTo: {select: true,checkbox: false,radio: false,button: false,file: false,input: false,textarea: false},fileUploadText: "' . Yii::t('course_management', 'CHOOSE FILE') . '"});
								replacePlaceholder();

								$(\'#userForm-details, #userForm-orgchat\').jScrollPane({
								autoReinitialise: true
							});
                                
 							'. $this->beforeShowModalCallBack .'
							},
							"beforeSend" : function() {
								th.find(".modal-body").html("");
							}
						});
					}
				});';
		return $script;
	}

	protected function renderModalHeader() {
		return '<div class="modal-header"><button type="button" class="close" data-dismiss="modal">×</button><h3 id="label-' . $this->uniqueId . '">' . addslashes($this->headerTitle) . '</h3></div>';
	}

	protected function renderModalBody() {
		return '<div class="modal-body"></div>';
	}

	protected function renderModalFooter() {
		$html = '<div class="modal-footer">';

		if (!empty($this->buttons)) {
			foreach ($this->buttons as $button) {
				$html .= $this->renderButton($button);
			}
		}

		$html .= '</div>';

		return $html;
	}

	protected function renderButton($button) {
		return CHtml::button($button['title'], array(
			'class' => 'btn '.$button['class'],
			'data-dismiss' => $button['type'] == 'close' ? 'modal' : '',
		));
	}

	public function init() {
		$this->uniqueId = Docebo::randomHash();
		echo $this->renderModal();
	}
}
