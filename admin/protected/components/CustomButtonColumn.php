<?php
/**
 * This class is because someone SHOULD NOT use common CSS selectors and to screw up further customizations.
 */
Yii::import('zii.widgets.grid.CButtonColumn');
class CustomButtonColumn extends CButtonColumn
{
	/**
	 * @var array the HTML options for the data cell tags.
	 */
	public $htmlOptions=array('class'=>'custom-button-column');
	/**
	 * @var array the HTML options for the header cell tag.
	 */
	public $headerHtmlOptions=array('class'=>'custom-button-column');
	/**
	 * @var array the HTML options for the footer cell tag.
	 */
	public $footerHtmlOptions=array('class'=>'custom-button-column');
}