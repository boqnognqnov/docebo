<?php

class EnrollmentRulesGridFormatter extends CWidget
{
	public $model;
	public $subtype;

	public function init()
	{
	}

	public function run()
	{
		if ($this->subtype == 'first')
		{
			switch($this->model->rule_type)
			{
				case CoreEnrollRule::RULE_BRANCH_COURSE:
				case CoreEnrollRule::RULE_BRANCH_CURRICULA:
					$this->formatBranchColumn();
					break;
				case CoreEnrollRule::RULE_GROUP_COURSE:
				case CoreEnrollRule::RULE_GROUP_CURRICULA:
					$this->formatGroupColumn();
					break;
			}
		}
		else if ($this->subtype == 'second')
		{
			switch($this->model->rule_type)
			{
				case CoreEnrollRule::RULE_BRANCH_COURSE:
				case CoreEnrollRule::RULE_GROUP_COURSE:
					$this->formatCourseColumn();
					break;
				case CoreEnrollRule::RULE_BRANCH_CURRICULA:
				case CoreEnrollRule::RULE_GROUP_CURRICULA:
					$this->formatCurriculaColumn();
					break;
			}
		}
	}

	private function formatBranchColumn_OLD()
	{
		$itemNum = count($this->model->branchItems);
		$colorClass = $itemNum ? '' : 'orange';
		echo '<a href="'.Yii::app()->createUrl('enrollmentRules/assignBranch', array('id' => $this->model->rule_id, 'singleStep' => 'true')).'" title="'.Yii::t('standard', '_MOD').'" class="open-dialog">'.$itemNum.' <i class="i-sprite is-people '.$colorClass.'"></i></a>';
	}
	
	/**
	 * Format "assign branches" Column/Link to use Selector
	 */
	private function formatBranchColumn()
	{
		$items = array();
		foreach($this->model->branchItems as $item) {
			$items [$item->item_id] = $item->item_id;
			if($item->selection_state == CoreEnrollRuleItem::SELECTION_STATE_BRANCH_AND_DESCENDANTS) {
				$orgChartTreeModel = CoreOrgChartTree::model()->findByPk($item->item_id);
				$descendants = $orgChartTreeModel->descendants()->findAll();
				foreach($descendants as $desc)
					$items[$desc->idOrg] = $desc->idOrg;
			}
		}
		$itemNum = count($items);
		$colorClass = $itemNum ? '' : 'orange';
		echo '<a href="'.Docebo::createLmsUrl('usersSelector/axOpen', array('type' => UsersSelector::TYPE_ENROLLRULES_BRANCHES, 'idEnrollRule' => $this->model->rule_id)).
			'" title="'.Yii::t('standard', 'branches').
			'" data-dialog-title="'.Yii::t('standard', 'branches').
			'" rel="uni-selector" class="open-dialog">'.$itemNum.' <i class="i-sprite is-people '.$colorClass.'"></i></a>';
	}

	private function formatGroupColumn_OLD()
	{
		$itemNum = count($this->model->groupItems);
		$colorClass = $itemNum ? '' : 'orange';
		echo '<a href="'.Yii::app()->createUrl('enrollmentRules/assignGroup', array('id' => $this->model->rule_id, 'singleStep' => 'true')).'" title="'.Yii::t('standard', '_MOD').'" class="open-dialog">'.$itemNum.' <i class="i-sprite is-people '.$colorClass.'"></i></a>';
	}
	
	
	
	/**
	 * Format "assign branches" Column/Link to use Selector
	 */
	private function formatGroupColumn()
	{
		$itemNum = count($this->model->groupItems);
		$colorClass = $itemNum ? '' : 'orange';
		echo '<a href="'.Docebo::createLmsUrl('usersSelector/axOpen', array('type' => UsersSelector::TYPE_ENROLLRULES_GROUPS, 'idEnrollRule' => $this->model->rule_id)).
		'" title="'.Yii::t('standard', '_GROUPS').
		'" data-dialog-title="'.Yii::t('standard', '_GROUPS').
		'" rel="uni-selector" class="open-dialog">'.$itemNum.' <i class="i-sprite is-people '.$colorClass.'"></i></a>';
	}
	

	private function formatCourseColumn_OLD()
	{
		$itemNum = count($this->model->courseItems);
		$colorClass = $itemNum ? '' : 'orange';
		echo '<a href="'.Yii::app()->createUrl('enrollmentRules/assignCourse', array('id' => $this->model->rule_id, 'singleStep' => 'true')).'" title="'.Yii::t('standard', '_MOD').'" class="open-dialog">'.$itemNum.' <i class="i-sprite is-forum '.$colorClass.'"></i></a>';
	}
	
	
	/**
	 * Format "assign branches" Column/Link to use Selector
	 */
	private function formatCourseColumn()
	{
		$itemNum = count($this->model->courseItems);
		$colorClass = $itemNum ? '' : 'orange';
		echo '<a href="'.Docebo::createLmsUrl('usersSelector/axOpen', array('type' => UsersSelector::TYPE_ENROLLRULES_COURSES, 'idEnrollRule' => $this->model->rule_id)).
		'" title="'.Yii::t('standard', '_COURSES').
		'" data-dialog-title="'.Yii::t('standard', '_COURSES').
		'" rel="uni-selector" class="open-dialog">'.$itemNum.' <i class="i-sprite is-forum '.$colorClass.'"></i></a>';
	}
	

	private function formatCurriculaColumn()
	{
		$itemNum = count($this->model->curriculaItems);
		$colorClass = $itemNum ? '' : 'orange';
		echo '<a href="'.Yii::app()->createUrl('enrollmentRules/assignCurricula', array('id' => $this->model->rule_id, 'singleStep' => 'true')).'" title="'.Yii::t('standard', '_MOD').'" class="open-dialog">'.$itemNum.' <i class="i-sprite is-forum '.$colorClass.'"></i></a>';
	}

}