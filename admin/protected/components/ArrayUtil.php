<?php
/**
 * Created by Andrei Amariutei
 * Date: 4/25/12
 * Time: 8:57 AM
 * Contains useful array functions
 */

class ArrayUtil {

    /**
     * Sorts an array of arrays by column name
     *
     * Example usage:
     *  $a = array(
                array(
                'name'=>'cube',
                'frame'=>'carbon',
                'fork'=>'magura'
                ),
                array(
                'name'=>'cube',
                'frame'=>'aluminium',
                'fork'=>'recon'
                ),
            );
        $sorted_array = array_orderby($a, 'name', SORT_ASC);
     * or
        $sorted_array = array_orderby($a, 'frame', SORT_DESC);
     * or even
        $sorted_array = array_orderby($a, 'name', SORT_ASC, 'frame', SORT_DESC);
     *
     * @static
     * @return array
     */
    public static function array_orderby()
    {
        $args = func_get_args();
        $data = array_shift($args);
        foreach ($args as $n => $field) {
            if (is_string($field)) {
                $tmp = array();
                foreach ($data as $key => $row)
                    $tmp[$key] = $row[$field];
                $args[$n] = $tmp;
            }
        }
        $args[] = &$data;
        @call_user_func_array('array_multisort', $args);
        return array_pop($args);
    }
    
    // Source: http://ephp.info/get-by-key-associative-array
    // Find and return a value from an associative array by searching for the key
    public static function find_by_key($searched, $array){
            if(!is_array($array)) return FALSE; // We haven't passed a valid array

            foreach($array as $key=>$value){
                    if($key==$searched) return $value; // Match found, return value
            }
            return FALSE; // Nothing was found
    }
}