<?php
return ConfigHelper::merge(
	require(dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../../common/config/main.php'),
	array(
		'name' => 'admin',
		'basePath'=> realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..'),
	)
);
