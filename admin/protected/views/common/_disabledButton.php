<?php

$errorFlashBox =
    "<div id='dcb-feedback'>" +
        "<div class='alert alert-error'>" +
            "<button type='button' class='close' data-dismiss='alert'>�</button>" +
            "Option not available for GoToWebinar" +
        "</div>" +
    "</div>";

$mainConfig = array(
    'class' => 'ajaxModal ' . $config['?lass'] . ' ' . $config['iconCls'],
    'onclick' => "$('#dcb-feedback').show(); $('#dcb-feedback')[0].innerHTML = actionErrorMsgHtml; $('.arrow').parent().hide(); window.scrollTo(0, 0); return false;",
);

if (!empty($config['linkOptions'])) {
    $mainConfig = CMap::mergeArray($mainConfig, $config['linkOptions']);
}

echo CHtml::link($config['linkTitle'], '', $mainConfig);