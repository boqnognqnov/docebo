<?php
	$id = $field->idField;
?>
<div style="width: 56px; float: right;" class="text-right"><span id="uploaded-percent-gauge-<?= $id ?>">&nbsp;</span></div>
<div class="docebo-progress additional-fields-progress progress progress-striped active" style="width: 120px; float: right;">
	<div id="uploader-progress-bar-<?= $id ?>" class="bar full-height" style="width: 0%;"></div>
</div>

<div id="plu-container-<?= $id ?>"><a id="pickfiles-fields-<?= $id ?>" href="javascript:;" style="float: right;">Select file</a></div>
<?= CHtml::label($field->translation, 'CoreUser[additional]['.$field->idField.']'); ?>

<?= CHtml::hiddenField('CoreUser[additional]['.$field->idField.'][type_field]', $field->type_field); ?>


<script type="text/javascript">
// Custom example logic

$(function(){


	// Create uploader instance
	var pl_uploader = new plupload.Uploader({
		chunk_size:       '1mb',
		multi_selection:  false,
		runtimes:         'html5,flash',
		browse_button:    'pickfiles-fields-<?= $id ?>',
		container:        'plu-container-<?= $id ?>',
		max_file_size:    '1024mb',
		url:              '/lms/?r=site/axUploadFile',
		multipart:        true,
		multipart_params: {
			YII_CSRF_TOKEN: '<?= Yii::app()->request->csrfToken ?>',
		},
		flash_swf_url : 'http://rawgithub.com/moxiecode/moxie/master/bin/flash/Moxie.cdn.swf',
		silverlight_xap_url : 'http://rawgithub.com/moxiecode/moxie/master/bin/silverlight/Moxie.cdn.xap',
	});


	// Init event hander
	pl_uploader.bind('Init', function (up, params) {
	});

	// Run uploader
	pl_uploader.init();


	// Just before uploading file
	pl_uploader.bind('BeforeUpload', function (up, file) {
		Docebo.log('BeforeUpload');
	});


	// Handle 'files added' event
	pl_uploader.bind('FilesAdded', function (up, files) {
		this.files = [files[0]];
		pl_uploader.start();
	});


	// Upload progress
	pl_uploader.bind('UploadProgress', function (up, file) {
		$('#uploader-progress-bar-<?= $id ?>').css('width', file.percent + '%');
		$('#uploaded-percent-gauge-<?= $id ?>').html('&nbsp;' + file.percent + '%');
	});

	// On error
	pl_uploader.bind('Error', function (up, error) {
		//$('#player-save-uploaded-lo').removeClass("disabled");
		//Docebo.Feedback.show('error', 'Failed to upload a file: ' + error.message + ' (' + error.status + ')');
	});


	// On Upload complete
	pl_uploader.bind('UploadComplete', function (up, files) {
		$('#plu-container-<?= $id ?>').closest('form').find('.confirm-btn').removeClass('disabled');

		// Add NAME of the uploaded file to the FORM.
		// Server knows where to find it (upload_tmp, usualy)
		theFile = up.files[0];
		var form = $('#plu-container-<?= $id ?>').closest('form');
		$('input[name="FileUpload[<?= $field->idField ?>]"]').remove();
		$fileNameFormField = $('<input type="hidden" name="FileUpload[<?= $field->idField ?>]" value="'+theFile.name+'">');
		form.append($fileNameFormField);
	});






});


</script>

