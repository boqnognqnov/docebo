<div class="main-actions clearfix">
	<h3 class="title-bold"><?php echo Yii::t('standard', '_COURSES'); ?></h3>
	<ul class="clearfix">
		<li>
			<div>
				<?php Yii::app()->tinymce; ?>
				<?php $this->renderPartial('//common/_modal', array(
					'config' => array(
						'class' => 'new-course',
						'modalTitle' => 'New course',
						'linkTitle' => 'New course',
						'linkOptions' => array(
							'alt' => 'Lorem Ipsum bla bla bla'
						),
						'url' => 'courseManagement/createCourse',
						'buttons' => array(
							array(
								'type' => 'submit',
								'title' => 'Confirm',
								'formId' => 'course_form',
								'callback' => 'function() {alert(999); }',
							),
							array(
								'type' => 'submit',
								'title' => 'Confirm 2',
//								'formId' => 'qwe',
								'callback' => 'function() {alert(10000); }',
							),
							array(
								//									'class' => 'ajaxSubmit btn ',
								'type' => 'cancel',
								'title' => 'Cancel',
								'callback' => 'testCallback("abc", 123);',
							),
							array(
								'type' => 'ok',
								'class' => 'asd',
								'title' => 'Ok',
								//									'callback' => 'modalHide();'
							),
						),
						'afterLoadingContent' => 'courseFormInit();',
						'beforeSubmit' => 'beforeCourseSubmit',
						'afterSubmit' => 'afterCourseSubmit2',
					),
				)); ?>

				<?php /*$courseCreateWidget = $this->widget('ext.local.widgets.ModalWidget', array(
				'type' => 'link',
				'modalClass' => 'new-course',
				'handler' => array(
					'title' => '<span></span>New course',
					'htmlOptions' => array(
						'class' => 'new-course',
						'alt' => 'Lorem Ipsum bla bla bla',
						'modal-title' => '<span></span>Create a new course',
					),
				),
				'headerTitle' => 'New course',
				'url' => $this->id . '/createCourse',
				'urlParams' => array(),
				'updateEveryTime' => false,
				'remoteDataType' => 'json',
				'buttons' => array(
					array(
						'title' => 'Create',
						'type' => 'confirm',
						'class' => 'confirm-btn',
					),
					array(
						'title' => 'Cancel',
						'type' => 'close',
						'class' => 'close-btn',
					),
				),
				'beforeShowModalCallBack' => '
					courseFormInit();
				',
			)); ?>
			<script type="text/javascript">
				$('#modal-<?php echo $courseCreateWidget->uniqueId; ?> .confirm-btn').live('click', function () {
					var modal = $('#modal-<?php echo $courseCreateWidget->uniqueId; ?> .modal-body');
					var form = modal.find('form');

					form.find('textarea').val(tinymce.activeEditor.getContent());

					var options = {
						url         : form.attr('action'),
						type        : 'post',        // 'get' or 'post', override for form's 'method' attribute
						dataType    : 'json',        // 'xml', 'script', or 'json' (expected server response type)
						beforeSubmit: showRequest,
						success     : function() {
							if (data.html) {
								modal.html(data.html);
								courseFormInit();
								modal.find('input').styler({browseText: "Choose file"});
							} else {
								$('#modal-<?php echo $courseCreateWidget->uniqueId; ?>').modal('hide');
								$('#course-management-grid').yiiGridView('update');
							}
						}
					};
					// bind form using 'ajaxForm' and submit
					form.ajaxForm(options).submit().unbind('submit');
					return false;
				});
			</script>
 <?*/?>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4></h4>
			<p></p>
		</div>
	</div>
</div>
