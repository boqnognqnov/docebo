<style>
<!--

.users-selector {
	min-height: 450px;
}

.users-selector ul.fancytree-container {
	height: 400px;
/* 	overflow: scroll; */
}

-->
</style>


<script type="text/javascript">

	function selectCallback(event, data) {
		$('#selected-count-orgcharts').text(data.tree.countSelected());		
	}
	
</script>

<div class="users-selector">

	<div class="users-selector-tabs">
	
		<ul class="nav nav-tabs">
			<li class="active">
				<a data-target=".users" href="#">
					<span class="courseEnroll-users"></span><?php echo Yii::t('standard', '_USERS'); ?>
				</a>
			</li>
			<li>
				<a data-target=".groups" href="#">
					<span class="courseEnroll-groups"></span><?php echo Yii::t('standard', '_GROUPS'); ?>
				</a>
			</li>
			
			<!-- HIDE IF NO BRANCHES -->
			<?php if(!empty($fancyTreeData)) : ?>
				<li>
					<a data-target=".orgchart" href="#">
						<span class="courseEnroll-orgchart"></span><?php echo Yii::t('standard', '_ORGCHART'); ?>
					</a>
				</li>
			<?php endif;?>
		</ul>
	
		<div class="select-user-form-wrapper">
		
			<!-- FILTERS -->
			<div class="tab-content">
			
				<div class="users tab-pane active">
					<?php $this->renderPartial('//common/selectUsers/_userFilters', array(
						'model' => $userModel,
						'onlyUsers' => (!empty($onlyUsers)?$onlyUsers:false),
					)); ?>
				</div>
				
				<div class="groups tab-pane">
					<?php $this->renderPartial('//common/selectUsers/_groupFilters', array(
						'model' => $groupModel,
					)); ?>
				</div>
				
				<div class="orgchart tab-pane">
					<div class="filters-wrapper">
						<div class="selections">
							<div class="input-wrapper-orgchart">
								<span class="icon-select-node-yes"></span> <?php echo Yii::t('standard', '_YES')?>
								<span class="icon-select-node-no"></span> <?php echo Yii::t('standard', '_NO')?>
								<span class="icon-select-node-descendants"></span> <?php echo Yii::t('standard', '_INHERIT')?>
							</div>
							<div class="left-selections">
								<p><?php echo Yii::t('standard', 'You have selected'); ?> <strong><span id="selected-count-orgcharts">0</span> <?php echo Yii::t('standard', 'branches'); ?></strong>.</p>
								<a href="javascript:void(0);" class="select-all-orgcharts" data-value="1"><?php echo Yii::t('standard', '_SELECT_ALL'); ?></a>
								<a href="javascript:void(0);" class="deselect-all-orgcharts" data-value="0"><?php echo Yii::t('standard', '_UNSELECT_ALL'); ?></a>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			
			
			<!-- CONTENT -->
			<?php $form = $this->beginWidget('CActiveForm', array(
				//'id' => 'users-selector-form',
				'id' => 'select-form',
				'htmlOptions' => array(
					'data-grid-items' => 'true',
				),
			)); ?>
	
			<div class="tab-content">
				<div class="users tab-pane active">
					<?php
						$this->renderPartial('//common/selectUsers/_user', array(
							'model' => $userModel,
							'isNeedRegisterYiiGridJs' => $isNeedRegisterYiiGridJs,
						), false, true); 
					?>
				</div>
				<div class="groups tab-pane">
					<?php
						$this->renderPartial('//common/selectUsers/_group', array(
							'model' => $groupModel,
							'isNeedRegisterYiiGridJs' => false,
						), false, true); 
					?>
				</div>
					
				<!-- HIDE IF NO BRANCHES -->
				<?php if(!empty($fancyTreeData)) : ?>
					<div class="orgchart tab-pane">
						<div>
						<?php
							$id = 'orgcharts-fancytree';
							$this->widget('common.extensions.fancytree.FancyTree', array(
								'id' => $id,
								'treeData' => $fancyTreeData,
								'preSelected' => $preSelected,
								'formFieldNames' => array(
									'selectedInputName'	 	=> 'select-orgchart',
									'activeInputName' 		=> 'orgchart_active_node',
									'selectModeInputName'	=> 'orgchart_select_mode'
								),

								'eventHandlers' => array(
									'select' => 'function(event, data) {
										selectCallback(event, data);
									}',
								),

							));
						?>
						</div>
					</div>
				<?php endif;?>
			</div>
			
	
			<?php $this->endWidget(); ?>
	
		</div>
	</div>


</div>



<script type="text/javascript">

					
	$(function(){

		// Handle Select All clicks
		$(".users-selector .select-all-orgcharts").on("click", function(){
			var tree = $('#<?= $id ?>').fancytree('getTree');
			tree.rootNode.children[0].setSelectState(2);
		});

		// Handle De-Select All clicks
		$(".users-selector .deselect-all-orgcharts").on("click", function(){
			var tree = $('#<?= $id ?>').fancytree('getTree');
			tree.rootNode.children[0].setSelectState(0);
		});

		
	});						

						
					
</script>
