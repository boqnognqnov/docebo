<?php $canAddCertificates = Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/lms/admin/certificate/mod'); ?>
<div class="main-actions clearfix">
	<h3 class="title-bold"><?php echo Yii::t('standard', '_CERTIFICATES'); ?></h3>
	<ul class="clearfix">
		<?php if ($canAddCertificates): ?>
		<li>
			<div>
				<?php $this->renderPartial('//common/_modal', array(
					'config' => array(
						'class' => 'new-certificate',
						'modalTitle' => Yii::t('certificate', '_NEW_CERTIFICATE'),
						'linkTitle' => '<span></span>'.Yii::t('certificate', '_NEW_CERTIFICATE'),
						'linkOptions' => array(
							'alt' => Yii::t('helper', 'New certificate')
						),
						'url' => 'certificateManagement/create',
						'buttons' => array(
							array(
								'type' => 'submit',
								'title' => Yii::t('standard', '_CONFIRM'),
							),
							array(
								'type' => 'cancel',
								'title' => Yii::t('standard', '_CANCEL'),
							),
						),
						'afterLoadingContent' => 'function(){ initTinyMCE(\'#\' + $(\'.modal.in\').find(\'textarea\').attr(\'id\'), 150); }',
						'beforeSubmit' => 'beforeCertificateSubmit',
						'afterSubmit' => 'updateCertificateContent',
					),
				)); ?>
			</div>
		</li>
		<?php endif; ?>
	</ul>
	<div class="info">
		<div>
			<h4></h4>
			<p></p>
		</div>
	</div>
</div>
