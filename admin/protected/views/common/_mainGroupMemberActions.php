<div class="main-actions clearfix">
	<h3 class="title-bold back-button">
		<?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('groupManagement/index')); ?>
		<span><?php echo Yii::t('standard', '_ASSIGN_USERS'); ?></span>
	</h3>
	<ul class="clearfix">
		<?php if (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/groupmanagement/mod')): ?>
		<li>
			<div>
				<?php $this->renderPartial('//common/_modal', array(
					'config' => array(
						'class' => 'group-select-users',
						'modalTitle' => Yii::t('standard', '_ASSIGN_USERS'),
						'linkTitle' => '<span></span>'.Yii::t('standard', '_ASSIGN_USERS'),
						'linkOptions' => array(
							'alt' => Yii::t('helper', 'Group assign users')
						),
						'url' => 'groupManagement/assignUsers&idst='.$id,
						'buttons' => array(
							array(
								'type' => 'submit',
								'title' => Yii::t('standard', '_ADD'),
								'formId' => 'select-form',
							),
							array(
								'type' => 'cancel',
								'title' => Yii::t('standard', '_CANCEL'),
							),
						),
						'afterLoadingContent' => 'function() { replacePlaceholder(); applyTypeahead($(\'.typeahead\')); }',
						'beforeSubmit' => 'initGroupMemberData',
						'afterSubmit' => 'updateGroupMemberContent',
					),
				)); ?>
			</div>
		</li>
		<?php endif; ?>
	</ul>
	<div class="info">
		<div>
			<h4></h4>
			<p></p>
		</div>
	</div>
</div>
