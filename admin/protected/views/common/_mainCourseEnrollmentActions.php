<?php
/* @var $courseModel LearningCourse */
$pUserRights = Yii::app()->user->checkPURights($course->idCourse);
$canEnroll = (Yii::app()->user->isGodAdmin || ($pUserRights->all));
if ($canEnroll):
?>
<div class="main-actions course-enrollments-actions clearfix">
	<h3 class="title-bold back-button" style="word-break:normal;">
		<?php
		//in some cases PUs may have enrollment permission for courses, but not the view permission (weird, but possible)
		$isPUWithNoCourseViewPermission = false;
		if (Yii::app()->user->isAdmin && !Yii::app()->user->checkAccess('/lms/admin/course/view')) {
			$isPUWithNoCourseViewPermission = true;
		}
		$backUrl = (!$isPUWithNoCourseViewPermission
			// Add possibility to back to course training material
			? !empty(Yii::app()->getRequest()->getParam('course_id', false))
				? Docebo::createLmsUrl('player/training&course_id='. Yii::app()->getRequest()->getParam('course_id'). '&ref_src=admin')
				: Yii::app()->createUrl('courseManagement/index')
			: Docebo::createLmsUrl('site/index'));
		echo CHtml::link(Yii::t('standard', '_BACK'), $backUrl);
		?>
		<span>
			<?=$courseModel->name?>
		</span>
	</h3>
	<ul class="clearfix">
	
	<?php if(Yii::app()->user->checkAccess('enrollment/create')) { ?>
		<li data-bootstro-id="bootstroEnrollUsers">
			<?php
				// Use the new userselector approach
				$this->renderPartial('_enrollUsersActionButton', array('course' => $course,));
			?>
		</li>

		<li>
			<div>

				<div>
					<?php $courseCopyEnrollmentWidget = $this->widget('ext.local.widgets.ModalWidget', array(
						'type' => 'link',
						'modalClass' => 'copy-course-enrollment',
						'handler' => array(
							'title' => '<span></span>'.Yii::t('subscribe', '_IMPORT_FROM_COURSE'),
							'htmlOptions' => array(
								'class' => 'copy-from-course',
								'alt' => Yii::t('helper', 'Copy enrollment'),
								'modal-title' => Yii::t('subscribe', '_IMPORT_FROM_COURSE'),
							),
						),
						'headerTitle' => Yii::t('subscribe', '_IMPORT_FROM_COURSE'),
						'url' => $this->id.'/copyEnrollments',
						'urlParams' => array('courseId' => $course->idCourse),
						'updateEveryTime' => true,
						'remoteDataType' => 'json',
						'buttons' => array(
							array(
								'title' => Yii::t('standard', '_MAKE_A_COPY'),
								'type' => 'confirm',
								'class' => 'confirm-btn',
							),
							array(
								'title' => Yii::t('standard', '_CANCEL'),
								'type' => 'close',
								'class' => 'close-btn',
							),
						),
						'beforeShowModalCallBack' => '
							applyTypeahead($(\'.copy-course-enrollment .typeahead\')); $(\'.modal-footer\').show();
							replacePlaceholder();
							$(\'.modal.fade.in\').find(\'select\').styler();
						',
					));


					?>
					<script type="text/javascript">
						//<![CDATA[
						$('#modal-<?php echo $courseCopyEnrollmentWidget->uniqueId; ?> .confirm-btn').live('click', function () {
							var modal = $('#modal-<?php echo $courseCopyEnrollmentWidget->uniqueId; ?> .modal-body');
							$.ajax({
								'dataType' : 'json',
								'type' : 'POST',
								'url' : modal.find('#course_enrollments_form').attr('action'),
								'cache' : false,
								'data' : modal.find("form").serialize(),
								'success' : function (data) {
									modal.html(data.html);
									$('.modal.fade.in').find('input, select').styler();
									applyTypeahead($('.copy-course-enrollment .typeahead'));
									if (data.status == 'success') {
										$('.modal-footer').hide();
									} else if (data.status == 'saved') {
										$('#modal-<?php echo $courseCopyEnrollmentWidget->uniqueId; ?>').modal('hide');
									}
									$.fn.yiiListView.update('course-enrollment-list');
								}
							});
						});
						//]]>
					</script>
				</div>
			</div>
		</li>
		<li>
			<div>
				<?php $courseEnrollWidget = $this->widget('ext.local.widgets.ModalWidget', array(
					'type' => 'link',
					'modalClass' => 'course-enrollment-from-csv',
					'handler' => array(
						'title' => '<span></span>'.Yii::t('course', 'Enroll from CSV'),
						'htmlOptions' => array(
							'class' => 'enroll-from-csv',
							'alt' => Yii::t('course', 'Enroll users by uploading a CSV file'),
							'modal-title' => Yii::t('course', 'Enroll from CSV'),
						),
					),
					'headerTitle' => Yii::t('course', 'Enroll from CSV'),
					'url' => $this->id.'/enrollFromCsv',
					'urlParams' => array('courseId' => $course->idCourse),
					'updateEveryTime' => true,
					'remoteDataType' => 'json',
					'buttons' => array(
						array(
							'title' => Yii::t('standard', '_UPLOAD'),
							'type' => 'confirm',
							'class' => 'confirm-btn',
						),
						array(
							'title' => Yii::t('standard', '_CANCEL'),
							'type' => 'close',
							'class' => 'close-btn',
						),
					),
					'beforeShowModalCallBack' => 'setTimeout(function(){$(\'.modal.in .modal-footer\').show();}, 200);',
				)); ?>
				<script type="text/javascript">
					//<![CDATA[
					$('#modal-<?php echo $courseEnrollWidget->uniqueId; ?> .confirm-btn').live('click', function () {

						var modal = $(this).parents('.modal'),
							form = modal.find('.modal-body form'),
							url = HTTP_HOST + '?r=courseManagement/enrollFromCsv&courseId=' + <?php echo $course->idCourse; ?>,
							options = {
								url         : url,
								type        : 'post',        // 'get' or 'post', override for form's 'method' attribute
								dataType    : 'json',        // 'xml', 'script', or 'json' (expected server response type)
								beforeSubmit: function() { },  // pre-submit callback
								success     : function(responseText, statusText, xhr, modal) {
									enrollFromCsvCallback(responseText, statusText, xhr, modal);

									$.fn.yiiListView.update('course-enrollment-list');
								}
							};

						form.ajaxForm(options).submit().unbind('submit');
					});
					//]]>
				</script>
			</div>
		</li>
		<?php } ?>
		<?php if(PluginManager::isPluginActive('CoachingApp') && $courseModel->enable_coaching): ?>
		<li>
			<div>
				<a  href="<?= Docebo::createAdminUrl('coaching/index', array('course_id' => $courseModel->idCourse)) ?>"
				    alt="<?= Yii::t('coaching', 'Coaching management'); ?>" class="new-node">
					<span></span> <?= Yii::t('coaching', 'Coaching management') ?>
				</a>
			</div>
		</li>
		<?php endif; ?>
		
	</ul>
	<div class="info">
		<div>
			<h4></h4>
			<p></p>
		</div>
	</div>


</div>
<?php endif; ?>
