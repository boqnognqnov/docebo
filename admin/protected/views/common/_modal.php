<?php

$mainConfig = array(
	'class' => 'ajaxModal ' . $config['class'],
	'data-toggle' => 'modal',
	'data-modal-class' => $config['class'],
	'data-modal-title' => $config['modalTitle'],
	'data-buttons' => CJSON::encode($config['buttons']),
	'data-url' => $config['url'],
	'modal-request-type' => strtoupper((!empty($config['requestType'])) ? $config['requestType']: 'get'),
	'modal-request-data' => CJSON::encode((!empty($config['requestData'])) ? $config['requestData'] : array()),
	'data-before-loading-content' => (!empty($config['beforeLoadingContent'])) ? $config['beforeLoadingContent'] : '',
	'data-after-loading-content' => (!empty($config['afterLoadingContent'])) ? $config['afterLoadingContent'] : '',
	'data-before-submit' => (!empty($config['beforeSubmit'])) ? $config['beforeSubmit'] : '',
	'data-after-submit' => (!empty($config['afterSubmit'])) ? $config['afterSubmit'] : '',
);
if (!empty($config['linkOptions'])) {
	$mainConfig = CMap::mergeArray($mainConfig, $config['linkOptions']);
}

echo CHtml::link($config['linkTitle'], '', $mainConfig);