<div class="main-actions clearfix">
	<h3 class="title-bold"><?php echo Yii::t('standard', '_GROUPS'); ?></h3>
	<ul class="clearfix">
		<?php if (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/groupmanagement/add')): ?>
		<li>
			<div>
				<?php echo CHtml::link('<span></span>'.Yii::t('standard', '_CREATE'), $this->createUrl('groupManagement/createGroup'), array(
					'class' => 'open-dialog new-group',
					'id' => 'new-group_button',
					'data-dialog-class' => 'new-group',
					'title' => Yii::t('group_management', 'Create group'),
					'alt' => Yii::t('helper', 'New group')
				)); ?>
			</div>
		</li>
		<?php endif; ?>
	</ul>
	<div class="info">
		<div>
			<h4></h4>
			<p></p>
		</div>
	</div>
</div>
