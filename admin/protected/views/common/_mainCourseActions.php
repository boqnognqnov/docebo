<div class="main-actions clearfix">
	<h3 class="title-bold"><?php echo Yii::t('standard', '_COURSES'); ?></h3>
	<ul class="clearfix" data-bootstro-id="bootstroNewCourse">
		<?php if (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/lms/admin/course/add')): ?>
		<li>
			<div id="new-course-id">
			<?php
				$this->renderPartial('//common/_modal', array(
					'config' => array(
						'class' => 'new-course',
						'modalTitle' => Yii::t('standard', '_NEW_COURSE'),
						'linkTitle' => '<span></span>'.Yii::t('standard', '_NEW_COURSE'),
						'linkOptions' => array(
							'alt' => Yii::t('helper', 'New course'),
						),
						'url' => 'courseManagement/createCourse',
						'buttons' => array(
							array(
								'type' => 'submit',
								'title' => Yii::t('standard', '_CONFIRM'),
								'formId' => 'course_form',
							),
							array(
								'type' => 'cancel',
								'title' => Yii::t('standard', '_CANCEL'),
							),
						),
						'afterLoadingContent' => 'courseFormInit(111);',
						'beforeSubmit' => 'beforeCourseCreateSubmit',
						'afterSubmit' => 'updateCourseContent',
					),
				));
			?>
			</div>
		</li>
		<?php endif; ?>
		<?php
			$showNewCategoryButton = false;
			Yii::app()->event->raise('BeforeNewCourseCategoryButtonRender', new DEvent($this, array(
				'showNewCategoryButton' => &$showNewCategoryButton
			)));
			if (Yii::app()->user->getIsGodAdmin() || $showNewCategoryButton): ?>
		<li>
        <div>
					<?php
						$orgCreateWidget = $this->widget('ext.local.widgets.ModalWidget', array(
							'type' => 'link',
							'handler' => array(
								'title' => '<span></span>'.Yii::t('standard', '_NEW_CATEGORY'),
								'htmlOptions' => array(
									'class' => 'new-category',
									'alt' => Yii::t('helper', 'New category'),
								),
							),
							'modalClass' => 'new-category',
							'headerTitle' => Yii::t('standard', '_NEW_CATEGORY'),
							'url' => 'courseCategoryManagement/create',
							'urlParams' => array(),
							'updateEveryTime' => true,
							'remoteDataType' => 'json',
							'buttons' => array(
								array(
									'title' => Yii::t('standard', '_CONFIRM'),
									'type' => 'confirm',
									'class' => 'confirm-btn',
								),
								array(
									'title' => Yii::t('standard', '_CLOSE'),
									'type' => 'close',
									'class' => 'close-btn',
								),
							),
							'handleUrlBeforeSend' => 'var tgt = $("#tree-generation-time"); '
								.'if (tgt.length > 0 && tgt.val() != "") {'
								.'url += "&treeGenerationTime=" + tgt.val();'
								.'}',
							'preProcessAjaxResponse' => 'if (data && ((data.data && data.data.update_tree) || data.update_tree)) { '
								.'$(".modal.in").modal("hide"); '
								.'courseCategoriesTreeHelper.reloadCoursesCategoriesTree({'
								.' title: '.CJSON::encode(Yii::t('course_management', 'Courses categories tree update')).', '
								.' text: '.CJSON::encode(Yii::t('course_management', 'Courses categories tree has changed and needs to be updated')).', '
								.' button: '.CJSON::encode(Yii::t('standard', '_CONFIRM'))
								.'}); '
								.'return; '
								.'}'
						));
					?>
					<script type="text/javascript">
						$('#modal-<?php echo $orgCreateWidget->uniqueId; ?> .confirm-btn').live('click', function () {
							var modal = $('#modal-<?php echo $orgCreateWidget->uniqueId; ?> .modal-body');
							$.ajax({
								'dataType': 'json',
								'type': 'POST',
								'url': modal.find('form').attr('action'),
								'cache': false,
								'data': modal.find("form").serialize(),
								'success': function (data) {
									//first check if a tree update has been requested
									if (data && ((data.data && data.data.update_tree) || data.update_tree)) {
										$('#modal-<?php echo $orgCreateWidget->uniqueId; ?>').modal('hide');
										courseCategoriesTreeHelper.reloadCoursesCategoriesTree({
											title: <?= CJSON::encode(Yii::t('course_management', 'Courses categories tree update')) ?>,
											text: <?= CJSON::encode(Yii::t('course_management', 'Courses categories tree has changed and needs to be updated'))?>,
											button: <?= CJSON::encode(Yii::t('standard', '_CONFIRM')) ?>
										});
										return;
									}
									//proceed with regular execution
									if (data.html) {
										modal.html(data.html);
									} else {
										$('#modal-<?php echo $orgCreateWidget->uniqueId; ?>').modal('hide');
										$('#tree-wrapper').loadTree('', true, false);
									}
								}
							});
						});
					</script>
			</div>
		</li>
		<?php endif; ?>
		<?php if (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('enrollment/create')): ?>
		<li>
				<?php
					$this->renderPartial('_massEnrollUsersActionButton', array());
				?>
		</li>
		<?php endif; ?>

		<!--Import Session to Classrooms-->
		<?php if(PluginManager::isPluginActive('ClassroomApp')&& Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess("/lms/admin/classroomsessions/add")): ?>
			<li>
				<div>
					<a href="<?php echo $this->createUrl('courseManagement/importSessions'); ?>" class="import" alt="<?php echo Yii::t('helper', 'Import ILT sessions from CSV file'); ?>">
						<span></span><?php echo Yii::t('helper', 'Import ILT sessions'); ?>
					</a>
				</div>
			</li>
		<?php endif ?>
        <?php if (Yii::app()->user->getIsGodadmin()): ?>
            <li>
                <div id="additional-course-fields-id">
                    <a href="<?php echo $this->createUrl('additionalCourseFields/index'); ?>" class="additional-fields" alt="<?php echo Yii::t('helper', 'Extend your default courses view by creating and assigning brand new custom fields'); ?>">
                        <span></span><?php echo Yii::t('menu', '_FIELD_MANAGER'); ?>
                    </a>
                </div>
            </li>
        <?php endif; ?>
		<!-- Adding Additional enrollment fields button-->
		<?php if (Yii::app()->user->getIsGodadmin()): ?>
            <li>
                <div>
                    <a href="<?php echo $this->createUrl('additionalEnrollmentFields/index'); ?>"  alt="<?php echo Yii::t('helper', 'Extend default enrollment view by creating and assigning brand new custom fields'); ?>">
                        <div class="icons">
	                        <i class="fa fa-list" aria-hidden="true"></i>
	                        <i class="fa fa-user" aria-hidden="true"></i>
                        </div>
                        <p>
                        	<?php echo Yii::t('menu', '_ENROLLMENT_FIELD_MANAGER'); ?>
                        </p>
                    </a>
                </div>
            </li>
        <?php endif; ?>
			
	</ul>
	<div class="info">
    <div>
		<h4></h4>
		<p></p>
    </div>
	</div>

</div>

<?php
$courseEditWidget = $this->widget('ext.local.widgets.ModalWidget', array(
	'type' => 'link',
	'modalClass' => 'course-node-edit',
	'handler' => array(
		'title' => Yii::t('standard', '_MOD'),
		'class' => 'course-node-edit',
		'htmlOptions' => array(
			'style' => 'display: none;',
		),
	),
	'handlerClass' => 'course-node-edit',
	'headerTitle' => Yii::t('standard', '_MOD'),
	'url' => 'courseManagement/updateCourse',
	'updateEveryTime' => true,
	'remoteDataType' => 'json',
	'buttons' => array(
		array(
			'title' => Yii::t('standard', '_CONFIRM'),
			'type' => 'confirm',
			'class' => 'confirm-btn',
		),
		array(
			'title' => Yii::t('standard', '_CLOSE'),
			'type' => 'close',
			'class' => 'close-btn',
		),
	),
	'beforeShowModalCallBack' => '
		courseFormInit();
	',
));
?>
<script type="text/javascript">
	$('#modal-<?php echo $courseEditWidget->uniqueId; ?> .confirm-btn').live('click', function () {

		var modal = $('#modal-<?php echo $courseEditWidget->uniqueId; ?> .modal-body');
		if (tinymce.activeEditor != null) {
			$('#modal-<?php echo $courseEditWidget->uniqueId; ?>').find('textarea').val(tinymce.activeEditor.getContent());
		}
		$.ajax({
			'dataType': 'json',
			'type': 'POST',
			'url': modal.find('form').attr('action'),
			'cache': false,
			'data': modal.find("form").serialize(),
			'success': function (data) {
				if (data.html) {
					modal.html(data.html);
					courseFormInit();
				} else {
					$('#modal-<?php echo $courseEditWidget->uniqueId; ?>').modal('hide');
					$('#course-management-grid').yiiGridView('update');
				}
			}
		});
	});
</script>

<?php
$courseDefaultWidget = $this->widget('ext.local.widgets.ModalWidget', array(
	'type' => 'link',
	'modalClass' => 'course-default',
	'handler' => array(
		'title' => Yii::t('standard', '_MAKE_A_COPY'),
		'class' => 'course-node-edit',
		'htmlOptions' => array(
			'style' => 'display: none;',
		),
	),
	'headerTitle' => 'Update Course',
	'url' => 'courseManagement/updateCourse',
	'updateEveryTime' => true,
	'remoteDataType' => 'json',
	'buttons' => array(
		array(
			'title' => Yii::t('standard', '_CONFIRM'),
			'type' => 'confirm',
			'class' => 'confirm-btn',
		),
		array(
			'title' => Yii::t('standard', '_CLOSE'),
			'type' => 'close',
			'class' => 'close-btn',
		),
	),
	'beforeShowModalCallBack' => '
		courseFormInit();
	',
));
?>
<script type="text/javascript">

	$(document).ready(function() {
		<?php if(Yii::app()->request->getParam('open') == 'new-course'): ?>
		$("a.new-course").trigger('click');
		<?php elseif(Yii::app()->request->getParam('open') == 'enroll-multiple'): ?>
		$(document).controls();
		$("a.course-enrollment").trigger("click");
		<?php endif; ?>
	});


	$('#modal-<?php echo $courseDefaultWidget->uniqueId; ?> .confirm-btn').live('click', function () {
		var modal = $('#modal-<?php echo $courseDefaultWidget->uniqueId; ?> .modal-body');

		// Disable confirm button after sending ajax request
		var btn = $(this);
		btn.addClass('disabled');
		btn.prop('disabled', true);

		$.ajax({
			'dataType': 'json',
			'type': 'POST',
			'url': modal.find('form').attr('action'),
			'cache': false,
			'data': modal.find("form").serialize(),
			'success': function (data) {
				if (data.html) {
					modal.html(data.html);
				} else {
					$('#modal-<?php echo $courseDefaultWidget->uniqueId; ?>').modal('hide');
					$('#course-management-grid').yiiGridView('update');
				}
			}
		});

		// After modal is closed confirm button is enabled again
		$('#modal-<?php echo $courseDefaultWidget->uniqueId; ?>').on('hidden.bs.modal', function () {
			btn.removeClass('disabled');
			btn.prop('disabled', false);
		});

	});

	$('.modal.new-course a.btn.btn-submit').live('click', function(ev) {
		var btn = $(this);
		btn.addClass('disabled');
		btn.prop('disabled', true);

		var modal = $('.modal.new-course');
		var data = modal.find("form").serializeArray();
		data.push({
			name: 'LearningCourse[description]',
			value: tinymce.activeEditor.getContent()
		});

		$.ajax({
			'dataType': 'json',
			'type': 'POST',
			'url': modal.find('form').attr('action'),
			'cache': false,
			'data': data,
			'success': function (data) {
				$.fn.updateCourseContent(data);
				hideLoading('contentLoading');
				setTimeout(function() {
					btn.removeClass('disabled');
					btn.prop('disabled', false);
				}, 1000);
			}
		});
	})

</script>
