<?php

	if (Yii::app()->user->getIsPu()){
		$unconfirmedCount = count(CoreUserPU::listUsersOfPowerUserInWaitingList(Yii::app()->user->id));
	}else{
		$unconfirmedCount = CoreUserTemp::model()->count();
	}
	
	$canAddUsers = (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/usermanagement/add'));
	$canModUsers = (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/usermanagement/mod'));
	$canApproveWaitingUsers = (Yii::app()->user->getIsGodadmin() || (Yii::app()->user->checkAccess('/framework/admin/usermanagement/approve_waiting_user')));
	
	if (Yii::app()->user->getIsAdmin()) {
		if (CoreUser::adminRuleEnabled(Yii::app()->user->id, 'direct_user_insert')) {
			$canApproveWaitingUsers = &$canApproveWaitingUsers;
		}
		else {
			$canApproveWaitingUsers = false;
		}
	}
	
?>
<div class="main-actions clearfix">
	<h3 class="title-bold"><?php echo Yii::t('standard', '_USERS'); ?></h3>
	<ul class="clearfix">
		<?php if ($canAddUsers): ?>
		<li>
			<div>
				<?php
					echo CHtml::link('<span></span>'.Yii::t('standard', '_NEW_USER'), Yii::app()->createUrl('userManagement/createUser'), array(
						'class' => 'new-user popup-handler',
						'data-modal-title' => Yii::t('standard', '_NEW_USER'),
						'alt' => Yii::t('helper', 'New user'),
						'data-modal-class' => 'new-user',
						'data-after-close' => 'newUserAfterClose();',
						'data-after-send' => 'newUserAfterLoad();',
						'data-before-send' => 'disableModalSubmitButton();',
						//'data-after-send' => 'newUserAfterSubmit(data);',
					));
				?>
			</div>
		</li>
		<?php endif; ?>
		<?php if (Yii::app()->user->getIsGodadmin()): ?>
		<li>
			<div>
				<?php
				$orgCreateWidget = $this->widget('ext.local.widgets.ModalWidget', array(
					'type' => 'link',
					'handler' => array(
						'title' => '<span></span>' . Yii::t('standard', '_ORGCHART_ADDNODE'),
						'htmlOptions' => array(
							'class' => 'open-node-modal-link new-node',
							'alt' => Yii::t('helper', 'New node'),
						),
					),
					'modalClass' => 'new-node',
					'headerTitle' => Yii::t('standard', '_ORGCHART_ADDNODE'),
					'url' => 'orgManagement/create',
					'urlParams' => array(),
					'updateEveryTime' => true,
					'remoteDataType' => 'json',
					'buttons' => array(
						array(
							'title' => Yii::t('standard', '_CONFIRM'),
							'type' => 'confirm',
							'class' => 'confirm-btn',
						),
						array(
							'title' => Yii::t('standard', '_CLOSE'),
							'type' => 'close',
							'class' => 'close-btn',
						),
					),
					'beforeShowModalCallBack' => 'if (UserMan && data && data.update_tree) { $(".modal.new-node").modal("hide"); UserMan.reloadOrgchartTree(); }',
					'handleUrlBeforeSend' => 'var tgt = $("#tree-generation-time"); '
						.'if (tgt.length > 0 && tgt.val() != "") { '
						.'	var treeGenerationTime = "" + tgt.val(); '
						.'	url += "&treeGenerationTime=" + treeGenerationTime; '
						.'}'
				));
				?>
				<script type="text/javascript">
					$('#modal-<?php echo $orgCreateWidget->uniqueId; ?> .confirm-btn').live('click', function () {
						var modal = $('#modal-<?php echo $orgCreateWidget->uniqueId; ?> .modal-body');
						$.ajax({
							'dataType': 'json',
							'type': 'POST',
							'url': modal.find('form').attr('action'),
							'cache': false,
							'data': modal.find("form").serialize(),
							'success': function (data) {

								// USERMAN-v2
								if (typeof UserMan !== "undefined") {
									 UserMan.loadOrgChartTree();
								}
								
								if (data.html) {
									modal.html(data.html);
								} else {
									$('#modal-<?php echo $orgCreateWidget->uniqueId; ?>').modal('hide');
								}
							}
						});
					});
				</script>
			</div>
		</li>
		<?php endif; ?>
		<?php if ($canAddUsers): ?>
		<li id="importUsersButtonUserMgmt">
			<div>
				<a style="padding-top: 25px" href="<?php echo $this->createUrl('userManagement/importUser'); ?>" class="import" alt="<?= Yii::t('helper', 'You can massively manage users (import, update, activate or deactivate) by uploading a CSV file with users data (CSV can be created from Excel, or similar products).'); ?>">
<!--					--><?//= Yii::t('helper', 'Import users - um'); ?>
					<div class="fa-stack fa-lg" style="margin-bottom: 12px;position: relative;margin-top: 10px;width: 100%">
						 <i class="fa fa-user fa-2x"></i>
						 <i class="fa fa-gear fa-1x" style="position: absolute;right: 15px"></i>
					</div>
					<?= Yii::t('organization_chart', 'Manage users via CSV'); ?>
				</a>
			</div>
		</li>
		<?php endif; ?>
		<?php
			$canImportFromGapps = $canAddUsers && (!Yii::app()->user->getIsAdmin() || (Yii::app()->user->getIsAdmin() && CoreUser::adminRuleEnabled(Yii::app()->user->id, 'direct_user_insert')) ); 
		?>
		<?php if ($canImportFromGapps) : ?>
		<?php if (Settings::getCfg('install_from') == 'gapps_docebo' || Settings::get('googleapps_active') == 'on') : ?>
		<li>
			<div>
				<a href="<?=Docebo::createAdminUrl('userManagement/syncGappsUsers')?>" class="import-google" alt="<?php echo Yii::t('helper', 'Import users - googleapps'); ?>">
					<span></span><?php echo Yii::t('user_management', 'Import users from Google Apps'); ?>
				</a>
			</div>
		</li>
		<?php endif; ?>
		<?php endif; ?>
		
		<?php if ( $canApproveWaitingUsers && (in_array(strtolower(Settings::get('register_type')), array('self','moderate')) || ($unconfirmedCount > 0)) ) : ?>
			<li>
				<div>
					<?php if ($unconfirmedCount > 0): ?>
						<span class="number waiting-users-count"><?php echo $unconfirmedCount; ?></span>
					<?php endif; ?>
					<a href="<?php echo $this->createUrl('userManagement/waitingApproval'); ?>" class="waiting-approval" alt="<?php echo Yii::t('helper', 'Waiting users'); ?>">
						<span></span><?php echo Yii::t('course', '_USERWAITING'); ?>
					</a>
				</div>
			</li>
		<?php endif; ?>
		
		<?php if (Yii::app()->user->getIsGodadmin()): ?>
		<li>
			<div>
				<a href="<?php echo $this->createUrl('additionalFields/index'); ?>" class="additional-fields" alt="<?php echo Yii::t('helper', 'Additional fields'); ?>">
					<span></span><?php echo Yii::t('menu', '_FIELD_MANAGER'); ?>
				</a>
			</div>
		</li>
		<?php endif; ?>
		<?
		// Raised for plugins to allow rendering more buttons/tiles in User management page
		Yii::app()->event->raise('UserManagementTiles', new DEvent($this));
		?>

	</ul>
	<div class="info">
    <div>
		<h4></h4>
		<p></p>
    </div>
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function() {
		<?php if(Yii::app()->request->getParam('open') == 'new-user'): ?>
		$("a.new-user").trigger('click');
		<?php endif; ?>
	});
</script>