<div class="main-actions clearfix" style="height: 150px;">
	<ul class="clearfix">
		<li>
        	<div>
				<?php $this->renderPartial('//common/_modal', array(
					'config' => array(
						'class' => 'new-colors-scheme',
						'modalTitle' => Yii::t('branding', 'New colors scheme'),
						'linkTitle' => '<span></span>'.Yii::t('branding', 'New colors scheme'),
						'linkOptions' => array(
							'alt' => Yii::t('helper', 'New colors scheme')
						),
						'url' => Docebo::createAbsoluteAdminUrl('branding/addColorsScheme'),
						'buttons' => array(
							array(
								'type' => 'submit',
								'title' => Yii::t('standard', '_SAVE'),
							),
							array(
								'type' => 'cancel',
								'title' => Yii::t('standard', '_CANCEL'),
							),
						),
						'afterLoadingContent' => 'schemeFormInit();',
						'afterSubmit' => 'schemeCallback',
						// 'afterLoadingContent' => 'courseFormInit();',
					),
				)); ?>
			</div>
		</li>
	</ul>
	<div class="info">
	    <div>
			<h4></h4>
			<p></p>
	    </div>
	</div>
</div>