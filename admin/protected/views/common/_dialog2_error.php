<div id="dialog2-error">

	<h1><?= Yii::t('standard','_ERRORS') ?></h1>
	<div class="row-fluid" style="text-align: center;">
		<div class="<?= $type ?>"><?= $message ?></div>
	</div>



	<div class="form-actions">
		<?= CHtml::button(Yii::t('standard', '_CLOSE'), array('class' => 'ajax btn btn-docebo black big close-dialog')); ?>
	</div>


</div>

<script type="text/javascript">
	$('#dialog2-error').parents('.modal').removeClass().addClass('modal dialog2-error');
</script>