<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'select-group-form',
	'htmlOptions' => array('class' => 'ajax-grid-form', 'data-grid' => '#select-group-grid'),
)); ?>
<div class="main-section">

	<div class="filters-wrapper">
		<div class="selections" data-grid="select-user-grid">
			<div class="input-wrapper">
				<?php echo $form->textField($model, 'groupid', array(
					'id' => 'advanced-search-group',
					'class' => 'typeahead',
					'autocomplete' => 'off',
					'data-url' => Docebo::createAppUrl('admin:groupManagement/groupAutocomplete'),
					'placeholder' => Yii::t('standard', '_SEARCH'),
				)); ?>
				<span class="search-icon"></span>
			</div>
			<?php $this->widget('ext.local.widgets.GridViewSelectAll', array(
				'gridId' => 'select-group-grid',
				'class' => 'left-selections',
				'dataProvider' => $model->dataProvider(),
				'itemValue' => 'idst',
			)); ?>
		</div>
	</div>
</div>

<input type="hidden" name="contentType" value="html"/>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>