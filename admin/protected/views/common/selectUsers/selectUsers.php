<div class="courseEnroll-tabs">
	<ul class="nav nav-tabs">
		<li class="active">
			<a data-target=".courseEnroll-page-users" href="#">
				<span class="courseEnroll-users"></span><?php echo Yii::t('standard', '_USERS'); ?>
			</a>
		</li>
		<li>
			<a data-target=".courseEnroll-page-groups" href="#">
				<span class="courseEnroll-groups"></span><?php echo Yii::t('standard', '_GROUPS'); ?>
			</a>
		</li>
		
		<!-- HIDE IF NO BRANCHES -->
		<?php if(!empty($fullOrgChartTree)) : ?>
			<li>
				<a data-target=".courseEnroll-page-orgchart" href="#">
					<span class="courseEnroll-orgchart"></span><?php echo Yii::t('standard', '_ORGCHART'); ?>
				</a>
			</li>
		<?php endif;?>
		
		
		
		<!--<li>
			<a data-target=".courseEnroll-page-roles" href="#">
				<span class="courseEnroll-roles"></span><?php echo Yii::t('standard', '_FUNCTIONAL_ROLE'); ?>
			</a>
		</li>-->
	</ul>

	<div class="select-user-form-wrapper">
		<div class="tab-content">
			<div class="courseEnroll-page-users tab-pane active">
				<?php $this->renderPartial('//common/selectUsers/_userFilters', array(
					'model' => $userModel,
					'onlyUsers' => (!empty($onlyUsers)?$onlyUsers:false),
				)); ?>
			</div>
			<div class="courseEnroll-page-groups tab-pane">
				<?php $this->renderPartial('//common/selectUsers/_groupFilters', array(
					'model' => $groupModel,
				)); ?>
			</div>
			<div class="courseEnroll-page-orgchart tab-pane">
				<?php $this->renderPartial('//common/selectUsers/_orgChartFilters'); ?>
			</div>
			<div class="courseEnroll-page-roles tab-pane">
				filter-4
			</div>
		</div>

		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'select-form',
			'htmlOptions' => array(
				'data-grid-items' => 'true',
			),
		)); ?>

			<div class="tab-content">
				<div class="courseEnroll-page-users tab-pane active">
					<?php $this->renderPartial('//common/selectUsers/_user', array(
						'model' => $userModel,
						'isNeedRegisterYiiGridJs' => $isNeedRegisterYiiGridJs,
//						'courseId' => $courseId,
					), false, true); ?>
				</div>
				<div class="courseEnroll-page-groups tab-pane">
					<?php $this->renderPartial('//common/selectUsers/_group', array(
						'model' => $groupModel,
						'isNeedRegisterYiiGridJs' => false,
					), false, true); ?>
				</div>
				
				<!-- HIDE IF NO BRANCHES -->
				<?php if(!empty($fullOrgChartTree)) : ?>
					<div class="courseEnroll-page-orgchart tab-pane">
						<?php 
							$this->renderPartial('//common/selectUsers/_orgChart', array(
								'fullOrgChartTree' 	=> $fullOrgChartTree,
								'puLeafs'			=> $puLeafs,
							)); 
						?>
					</div>
				<?php endif;?>
				
				
				<div class="courseEnroll-page-roles tab-pane">
					form-4
				</div>
			</div>

		<?php $this->endWidget(); ?>

	</div>
</div>