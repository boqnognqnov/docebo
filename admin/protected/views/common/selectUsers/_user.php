<?php
	Yii::app()->clientScript->scriptMap['jquery.js'] = false;
	Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
	if (empty($isNeedRegisterYiiGridJs)) {
		Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;
		Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
	}
?>
<div id="grid-wrapper" class="courseEnroll-user-table">
<h2><?php echo Yii::t('standard', '_SELECT_USERS'); ?></h2>
	<?php $this->widget('adminExt.local.widgets.CXGridView', array(
		'id' => 'select-user-grid',
		'htmlOptions' => array('class' => 'grid-view clearfix'),
		'dataProvider' => $model->dataProvider(),
		'cssFile' => false,
		'beforeAjaxUpdate' => 'function(id, options) {
			options.type = "POST";
			if (options.data == undefined)
				options.data = {}

			options.data.contentType = "html";
			options.data.selectedItems = $(\'.modal.in\').find(\'#\'+id+\'-selected-items-list\').val();
		}',
		'ajaxUpdate' => 'select-user-grid-all-items',
		'afterAjaxUpdate' => 'function(id, data){$("#select-user-grid input").styler(); afterGridViewUpdate(id);}',
		'columns' => array(
			array(
				'class' => 'CCheckBoxColumn',
				'selectableRows' => 2,
				'id' => 'select-user-grid-checkboxes',
				'value' => '$data->idst',
				'checked' => 'in_array($data->idst, Yii::app()->session[\'selectedItems\'])',
			),
			array(
				'name' => 'userid',
				'value' => 'Yii::app()->user->getRelativeUsername($data->userid);',
				'type' => 'raw',
			),
			array(
				'name' => 'fullname',
				'header' => Yii::t('standard', '_FULLNAME'),
				'value' => '$data->fullname',
				'type' => 'raw',
			),
			'email',
		),
		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'pager' => array(
			'class' => 'DoceboCLinkPager',
			'maxButtonCount' => 8,
		),
	)); ?>
</div>
