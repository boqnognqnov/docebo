<?php
	Yii::app()->clientScript->scriptMap['jquery.js'] = false;
	Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
	Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;
	Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
?>
<div id="grid-wrapper" class="courseEnroll-user-table">
	<?php $this->widget('adminExt.local.widgets.CXGridView', array(
		'id' => 'select-group-grid',
		'htmlOptions' => array('class' => 'grid-view clearfix'),
		'dataProvider' => $model->dataProvider(),
		'cssFile' => false,
		'beforeAjaxUpdate' => 'function(id, options) {
			options.type = "POST";
			if (options.data == undefined) {
				options.data = {
					contentType: "html",
					selectedItems: $(\'#\'+id+\'-selected-items-list\').val()
				}
			}
		}',
		'ajaxUpdate' => 'select-group-grid',
		'afterAjaxUpdate' => 'function(id, data){$("#select-group-grid input").styler(); afterGridViewUpdate(id);}',
		'columns' => array(
			array(
				'class' => 'CCheckBoxColumn',
				'selectableRows' => 2,
				'id' => 'select-group-grid-checkboxes',
				'value' => '$data->idst',
				'checked' => 'in_array($data->idst, Yii::app()->session[\'selectedItems\'])',
			),
			array(
				'name' => 'groupid',
				'value' => '$data->relativeId($data->groupid);',
				'type' => 'raw',
			),
			'description',
			//			array(
			//				'name' => 'fullname',
			//				'header' => 'Full Name',
			//				'value' => '$data->fullname',
			//				'type' => 'raw',
			//			),
		),
		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'pager' => array(
			'class' => 'DoceboCLinkPager',
			'maxButtonCount' => 8,
		),
	)); ?>
</div>
