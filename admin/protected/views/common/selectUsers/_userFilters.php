<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'select-user-form',
	'htmlOptions' => array('class' => 'ajax-grid-form', 'data-grid' => '#select-user-grid'),
)); ?>
<div class="main-section">

	<div class="filters-wrapper">
		<div class="selections" data-grid="select-user-grid">
			<div class="input-wrapper">
				<?php echo $form->textField($model, 'search_input', array(
					'id' => 'advanced-search-group',
					'class' => 'typeahead',
					'autocomplete' => 'off',
					'data-url' => Docebo::createAppUrl('admin:userManagement/usersAutocomplete'),
					'data-onlyUsers' => $onlyUsers,
					'placeholder' => Yii::t('standard', '_SEARCH'),
					'data-source-desc' => 'true',
				)); ?>
				<span class="search-icon"></span>
			</div>
			<?php $this->widget('ext.local.widgets.GridViewSelectAll', array(
				'gridId' => 'select-user-grid',
				'class' => 'left-selections',
				'dataProvider' => $model->dataProvider(false, $onlyUsers),
				'itemValue' => 'idst',
			)); ?>
		</div>
	</div>
</div>

<input type="hidden" name="contentType" value="html"/>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>