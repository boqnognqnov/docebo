<?php
	$form = $this->beginWidget('CActiveForm', array(
		'method' => 'POST',
		'id' => 'delete-transaction-form',
		'enableAjaxValidation' => false,
		'htmlOptions'=>array(
				'class' => 'ajax',
		),
	));

?>
<h1><?= Yii::t('admin','Delete transaction') ?></h1>
<br>
<div class="clearfix"></div>
<label class="checkbox">
	<?php echo CHtml::checkBox('confirm_delete', false, array()); ?>&nbsp;
	<?php echo Yii::t('standard','Yes, I confirm I want to proceed'); ?>
</label>

<div class="form-actions">
	<?= CHtml::submitButton(Yii::t('standard', '_DEL'), array('class' => 'btn-submit disabled')); ?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn-cancel close-dialog')); ?>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
<!--

$(function(){
	$('input[type="checkbox"]').styler();

	$('input[name="confirm_delete"]').on('change', function() {
		$(this).prop("checked") ? $('.btn-submit').removeClass('disabled') : $('.btn-submit').addClass('disabled');
	});
	
});	

//-->
</script>