<?php
    /* @var $transaction EcommerceTransaction */
	$currency = $transaction->payment_currency;
	$items = $transaction->transaction_items;
?>
<div class="popover-content">

	<div class='row-fluid'>
		<div class="span12 title"><?= Yii::t('standard','_COURSE_LIST') ?></div>
	</div>


<?php
    foreach ($items as $item) {
        echo "<div class='row-fluid'>";
        echo "<div class='span8'>$item[name]</div>\n";
        echo "<div class='span3 text-right'>$item[price] $currency</div>\n";
        if ($item['activated']) {
            echo "<div class='span1 text-right'><span class='i-sprite is-wallet green'></span> </div>\n";
        }
        else {
            echo "<div class='span1 text-right'><span class='i-sprite is-wallet grey'></span> </div>\n";
        }
        echo "</div>";
    }

?>

    <?php if (floatval($transaction->discount) != 0) : ?>
        <div class="popover-transaction-total">
            <div class="row-fluid line-subtotal">
                <div class="span4 offset7 line-content">
                    <div class="pull-left"><?= Yii::t('transaction','Subtotal') ?></div><?= $transaction->renderTotalAmount() ?>
                </div>
                <div class="span1"></div>
            </div>
            <div class="row-fluid line-discount">
                <div class="span4 offset7 line-content">
                    <div class="pull-left"><?= Yii::t('coupon','Discount') ?></div><?= $transaction->renderDiscount() ?>
                </div>
                <div class="span1"></div>
            </div>
            <div class="row-fluid line-total">
                <div class="span4 offset7 line-content">
                    <div class="pull-left"><?= Yii::t('standard','_TOTAL', array('{count}' => '')) ?></div><?= $transaction->renderTotalPaid() ?>
                </div>
                <div class="span1"></div>
            </div>
        </div>
    <?php else: ?>
        <div class='row-fluid total'>
            <div class="span11 text-right"><?= Yii::t('standard','_TOTAL', array('{count}' => '')) . "&nbsp;&nbsp;&nbsp;" . $transaction->totalPrice . " " . $transaction->payment_currency ?></div>
            <div class="span1"></div>
        </div>
    <?php endif; ?>


</div>