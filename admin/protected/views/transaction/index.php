<?php

/* @var $model EcommerceTransaction */
$this->breadcrumbs = array(
    Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
    Yii::t('ecommerce', 'Transactions'),
); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'transaction-search-form',
    'htmlOptions' => array(
        'class' => '',
        'data-grid' => '#transactions-grid'
    ),
)); ?>

<h3 class="title-bold"><?= Yii::t('standard', '_TRANSACTION') ?></h3>

<div class="<?php echo Yii::app()->getLanguage(); ?> main-section search-form transactions-filters">
    <div class="filters-wrapper">
        <div class="filters clearfix">

            <div class="row-fluid">

                <div class="span4">
                    <div class="input-wrapper date dates" id="search-transactions-date-from"
                         data-date-format="<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>"
                         data-date-language='<?= Yii::app()->getLanguage() ?>'>
                        <?= Yii::t('standard', '_FROM') ?>
                        <?= CHtml::activeTextField($model, 'search_date_from', array()) ?>
                        <i class="add-on i-sprite is-calendar-date"></i>
                    </div>
                    <div class="input-wrapper date dates" id="search-transactions-date-to"
                         data-date-format="<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>"
                         data-date-language='<?= Yii::app()->getLanguage() ?>'>
                        &nbsp;&nbsp;&nbsp;<?= Yii::t('standard', '_TO') ?>&nbsp;&nbsp;
                        <?= CHtml::activeTextField($model, 'search_date_to', array()) ?>
                        <i class="add-on i-sprite is-calendar-date"></i>
                    </div>
                </div>

                <div class="span8">
                    <div class="select-wrapper status-filter">
                        <?= Yii::t('standard', 'Status') ?>&nbsp;
                        <?php
                        $_statuses = ['' => Yii::t('standard', 'All')];
                        foreach (EcommerceTransaction::getStatusesList() as $_status_key => $_status_info) {
                            $_statuses[$_status_key] = $_status_info['title'];
                        }
                        /*
                        echo CHtml::dropDownList('status_transactions',
                            '',
                            $_statuses,
                            array('class' => 'status-transactions'));
                        */
                        echo $form->dropDownList($model, 'search_status', $_statuses, array(
                            'id' => 'search-transactions-status',
                            'placeholder' => Yii::t('standard', '_SEARCH'),
                            'class' => 'search-status status-transactions'
                        ));
                        ?>
                        &nbsp;&nbsp;
                    </div>

                    <div class="select-wrapper export-type">
                        <?= Yii::t('standard', 'Export as') ?>&nbsp;
                        <?= CHtml::dropDownList('export_transactions',
                            '',
                            EcommerceTransaction::exportTypes(true),
                            array('data-export-url' => $this->createUrl('transaction/export'), 'class' => 'export-transactions')) ?>
                        &nbsp;&nbsp;
                    </div>


                    <div class="input-wrapper">
                        <?php echo $form->textField($model, 'search_input', array(
                            'id' => 'search-transactions-text',
                            'placeholder' => Yii::t('standard', '_SEARCH'),
                            'class' => 'search-input'
                        )); ?>
                        <span class="search-icon"></span>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<!-- Grid -->
<?php
//'afterAjaxUpdate' => 'function(id, data){ filterColumns(); $("#users-management-grid input").styler(); }'

$columns = array('visible' => false);
$event = new DEvent($this, array());
Yii::app()->event->raise('OnBeforeTransactionsListColumnsRender', $event);
if ($event->return_value) {
    $columns = $event->return_value;
}

$this->widget('common.components.DoceboCGridView', array(
    'id' => 'transactions-grid',
    'htmlOptions' => array(''),
    'afterAjaxUpdate' => 'function(){$(document).controls(); Transaction.harvestGridPopovers();}',
    'dataProvider' => $model->transactionsManageDataProvider(),
    'template' => '{items}{pager}',
    'rowHtmlOptionsExpression' => 'array("data-id-trans" => $data[\'id_trans\'], "rel"=>"popover",  "data-content" => $this->owner->_getPopoverContent(EcommerceTransaction::model()->findByPk($data[\'id_trans\'])))',
    // required by Popover
    'pager' => array(
        'class' => 'DoceboCLinkPager',
        'maxButtonCount' => 8,
    ),
    'columns' => array(
        array(
            'name' => 'ID',
            'value' => '$data[\'id_trans\']'
        ),
        array(
            'name' => Yii::t('standard', '_USERNAME'),
            'value' => '(substr($data[\'userid\'], 0, 1) == \'/\' ? substr($data[\'userid\'], 1) : $data[\'userid\'])',
        ),
        array(
            'name' => Yii::t('standard', '_FIRSTNAME'),
            'value' => '$data[\'firstname\']',
        ),
        array(
            'name' => Yii::t('standard', '_LASTNAME'),
            'value' => '$data[\'lastname\']',
        ),
        array(
            'name' => Yii::t('standard', '_DATE'),
            'value' => 'Yii::app()->localtime->toLocalDateTime($data[\'date_creation\'], \'medium\')'
        ),
        array(
            'name' => Yii::t('subscribe', '_DATE_BEGIN_VALIDITY'),
            'value' => 'Yii::app()->localtime->toLocalDateTime($data[\'date_activated\'], \'medium\')'
        ),
        $columns,
        /*
        array(
            'id' => 'total-header',
            'name' => Yii::t('coupon', 'Subtotal'),
            'cssClassExpression' => '"text-right"',
            'value' => '($data[\'totalPrice\'] ? $data[\'totalPrice\'] : 0)." ".$data[\'payment_currency\']'
        ),
        array(
            'type' => 'raw',
            'name' => Yii::t('coupon', 'Discount'),
            'cssClassExpression' => '"text-right"',
            'value' => 'number_format($data[\'discount\'], 2) . \' \' . $data[\'payment_currency\']',
            'htmlOptions' => array('class' => 'text-right'),
            'headerHtmlOptions' => array('class' => 'text-right'),
        ),
        */
        array(
            'type' => 'raw',
            'name' => Yii::t('standard', 'Status'),
            'value' => function($data, $index) {
                if ($data['cancelled']) {
                    return Yii::t('course', '_CST_CANCELLED');
                } else {
                    switch ($data['paid']) {
                        case EcommerceTransaction::PAYMENT_STATUS_PENDING :
                            return Yii::t('gamification', 'Pending');
                            break;
                        case EcommerceTransaction::PAYMENT_STATUS_SUCCESSFUL:
                            return Yii::t('standard', 'Successful');
                            break;
                        case EcommerceTransaction::PAYMENT_STATUS_FAILED:
                            return Yii::t('standard', 'failed');
                            break;
                    }
                }
            },
        ),
        array(
            'type' => 'raw',
            'name' => Yii::t('standard', 'Done by'),
            'value' => function($data, $index) {
                switch ($data['done_by']) {
                    case EcommerceTransaction::DONE_BY_ADMIN:
                        return Yii::t('menu', 'Admin');
                        break;
                    case EcommerceTransaction::DONE_BY_USER:
                    default:
                        return Yii::t('standard', '_USER');
                        break;
                }
            },
        ),

        array(
            'type' => 'raw',
            'name' => Yii::t('coupon', 'Total paid'),
            'cssClassExpression' => '"text-right"',
            'value' => 'EcommerceTransaction::renderTotalPaidFromData($data)',
            'htmlOptions' => array('class' => 'text-right'),
            'headerHtmlOptions' => array('class' => 'text-right'),
        ),

        // Using regular column instead of button column, because Admin CSS is terrible!
        array(
            'cssClassExpression' => '"text-right"',
            'name' => '',
            'type' => 'raw',
            'value' => array($this, '_renderGridOperationsColumn')
        ),
    ),
));
?>

<script type="text/javascript">
    <!--
    $(function () {
        // Attach Date Pickers

        $('#search-transactions-date-from').bdatepicker();
        $('#search-transactions-date-to').bdatepicker();

        $('input, select').styler();

        Transaction.harvestGridPopovers();

        //$('#transactions-grid table tr').popover({placement: 'top', html: true}).popover('show');

        $('#search-transactions-status').on('change', function() {
            $('#transactions-grid').yiiGridView('update', {
                data: $('#transaction-search-form').serialize()
            });
        });
    });
    //-->
</script>