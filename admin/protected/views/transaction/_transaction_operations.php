<?php

	$editUrl = $this->createUrl("transaction/axEdit", array('id_trans' => $model['id_trans']));
	$deleteUrl = $this->createUrl("transaction/axDelete", array('id_trans' => $model['id_trans']));

?>
<?php
	$color = ($model['paid'] == EcommerceTransaction::PAYMENT_STATUS_SUCCESSFUL) ? "green" : "grey";
?>
<span class="i-sprite is-wallet <?= $color ?>"></span>&nbsp;
<a
	href="<?= $editUrl ?>"
	class="open-dialog"
	data-dialog-class="edit-transaction-items edit-item-modal"
	closeOnEscape="false"
	closeOnOverlayClick="false"
	rel="edit-transaction-modal-<?= $model['id_trans'] ?>">

	<span class="i-sprite is-edit"></span>
</a>

<?php if ($model['cancelled'] == 0): ?>
<a href="<?php echo $deleteUrl; ?>"
	class="open-dialog"
	data-dialog-class="delete-item-modal"
	closeOnEscape="false"
	closeOnOverlayClick="false"
	rel="delete-transaction-modal-<?= $model['id_trans']; ?>">

	<span class="i-sprite is-remove red"></span>
</a>
<?php else: ?>
<span style="visibility:hidden">
	<span class="i-sprite is-remove red"></span>
</span>
<?php endif; ?>