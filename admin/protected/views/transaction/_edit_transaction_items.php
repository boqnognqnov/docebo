<?php

/* @var $model EcommerceTransaction */

	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'transaction-items-form',
		'htmlOptions' => array(
			'class' => 'ajax form-inline',
		),
	));

	$currency = $model->payment_currency;

?>
<h1><?= Yii::t('standard', 'Edit Transaction Content') ?></h1>

<div class="row-fluid">
	<div class="span4">
		<h2 class="text-colored"><?= Yii::t('standard', '_DETAILS') ?></h2>
		<div class="row-fluid"><?= Yii::t('standard','_USERNAME') ?>: <?= $model->user->clearUserId ?></div>
		<div class="row-fluid"><?= Yii::t('standard','_FIRSTNAME') ?>: <?= $model->user->firstname ?></div>
		<div class="row-fluid"><?= Yii::t('standard','_LASTNAME') ?>: <?= $model->user->lastname ?></div>
        <?php if ($model->ecommerceCoupon) : ?>
		<div class="row-fluid"><?= Yii::t('transaction','Subtotal') ?>: <?= $model->renderTotalAmount() ?></div>
		<div class="row-fluid"><?= Yii::t('coupon','Discount') ?>: <?= $model->renderDiscount() ?></div>
        <?php endif; ?>
		<div class="row-fluid"><?= Yii::t('coupon', 'Total paid') ?>: <?= $model->renderTotalPaid() ?></div>
		<div class="row-fluid"><?= Yii::t('standard','_DATE') ?>: <?= $model->date_creation ?></div>
	</div>
	<div class="span4">
		<h2 class="text-colored"><?= Yii::t('billing', '_BILLING_INFORMATION') ?></h2>
		<div class="row-fluid"><?= $model->billingInfo->bill_company_name ?></div>
		<div class="row-fluid"><?= $model->billingInfo->bill_address1 . " " . $model->billingInfo->bill_address2 ?></div>
		<div class="row-fluid"><?= $model->billingInfo->bill_state . " " . $model->billingInfo->bill_city . " " . $model->billingInfo->bill_zip ?></div>
		<div class="row-fluid"><?= CoreCountry::getNameByIsoCode($model->billingInfo->bill_country_code) ?></div>

	</div>
	<div class="span4">
		<h2 class="text-colored"><?= Yii::t('standard', '_TRANSACTION') ?></h2>
		<div class="row-fluid"><?= Yii::t('billing','_PAYMENT_METHOD') ?>: <?= strtoupper($model->payment_type) ?></div>
		<div class="row-fluid">Txn Id: <?= $model->payment_txn_id ? $model->payment_txn_id : 'None' ?></div>

        <?php if ($model->ecommerceCoupon) : ?>
        <div class="row-fluid transaction-coupon-stats">

            <div class="coupon-code">
                <i class="coupons-sprite icon-dollartag"></i>
                <?= $model->ecommerceCoupon->code ?>
            </div>

            <?php if ($model->ecommerceCoupon->valid_from && $model->ecommerceCoupon->valid_to) : ?>
                <div class="coupon-validity">
                    <i class="p-sprite calendar-black-small date-icon"></i>
                    <?= CHtml::tag('strong',array(),Yii::t('standard', '_FROM')) . ' ' . date('Y-m-d', strtotime($model->ecommerceCoupon->valid_from)) ?>
                    <?= CHtml::tag('strong',array(),Yii::t('standard', '_TO')) . ' ' . date('Y-m-d', strtotime($model->ecommerceCoupon->valid_to)) ?>
                </div>
            <?php endif; ?>

            <div class="coupon-discount">
                <i class="coupons-sprite icon-scissors"></i>
                <strong><?= $model->ecommerceCoupon->renderDiscount() ?></strong>
            </div>

        </div>
        <?php endif; ?>
	</div>

</div>

<br>
<div class="celarfix"></div>
<div><?= Yii::t('standard', '_NOTES') ?></div>
<div class="clearfix"></div>

<div class="row-fluid">
	<?= $form->textArea($model, 'note', array('class' => 'span12')) ?>
</div>
<br>
<div class="celarfix"></div>

<h2><?= Yii::t('standard', '_COURSE_LIST') ?></h2>

<?php
	$this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'transactions-items-grid',
		'htmlOptions' => array(),
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
		'template' => '{items}{pager}',
		'dataProvider' => $itemsModel->searchByTransactionId(),

		'pager' => array(
			'class' => 'DoceboCLinkPager',
			'maxButtonCount' => 8,
		),

		'columns' => array(
			array(
				'name' => 'Id',
				'value' => '$data->id_trans'
			),

 			array(
 				'name' => Yii::t('standard', '_COURSE_CODE'),
 				'value' => '($data->id_course != 0 ? $data->course->code : $data->coursepath->path_code)'
 			),
			array(
				'name' => Yii::t('standard', '_TITLE'),
				'value' => '($data->id_course != 0 ? $data->course->name : $data->coursepath->path_name)'
			),
			array(
				'id' => 'price-header',
				'name' => Yii::t('transaction', '_PRICE'),
				'value' => '$data->price . " " . $data->transaction->payment_currency',
				'cssClassExpression' => '"price"',
				'footer' =>
                    ((floatval($model->discount) != 0)
                        ? (
                            CHtml::tag('span', array(), CHtml::tag('span',array('class'=>'pull-left'),Yii::t('transaction','Subtotal').':') . $model->renderTotalAmount()) .
                            CHtml::tag('span', array(), CHtml::tag('span',array('class'=>'pull-left'),Yii::t('coupon','Discount').':') . $model->renderDiscount())
                        ) : '') .
                    CHtml::tag('span', array('class'=>'line-total'), CHtml::tag('span',array('class'=>'pull-left'),Yii::t('standard','_TOTAL', array('{count}' => ''))) . $model->renderTotalPaid()),
				'footerHtmlOptions' => array('class' => 'price-footer'),
			),
			array(
				'name' => Yii::t('transaction', '_MARK_AS_PAID'),
				'id' => 'activated-header',
				'type' => 'raw',
				'value' => function($data) use ($form) {return $form->checkBox($data, "activated", array('name' => get_class($data) . "[$data->id_trans]")); },
				'cssClassExpression' => '"activated"',
			),



		)

	));
?>


<?php
	// Keeps selected checkboxes; Required, because 'grid-view' has attached listeners to all checkboxes inside
	// And if there is no such field, JS error is generated. Weird!
	echo CHtml::hiddenField('transactions-items-grid-selected-items-list', '');
?>


<div class="row-fluid text-right">
	<label class="checkbox flag-paid">
		<?= Yii::t('transaction','Flag the whole transaction as paid') ?>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $form->checkBox($model, 'paid'); ?>
	</label>
</div>

<!-- For Dialog2 - they go in footer -->
<div class="form-actions">
	<?php echo CHtml::submitButton(Yii::t('standard','_SAVE'), array('class' => 'btn btn-submit')); ?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
</div>


<?php $this->endWidget(); ?>

<script type="text/javascript">
<!--
	$(function(){
		$('input[type="checkbox"]').styler();
	});


//-->
</script>

