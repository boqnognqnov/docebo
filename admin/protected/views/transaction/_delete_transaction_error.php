<div class="alert error"><?= $message ?></div>
<div class="form-actions">
	<?= CHtml::button(Yii::t('standard', '_CLOSE'), array('class' => 'btn-cancel close-dialog')); ?>
</div>
