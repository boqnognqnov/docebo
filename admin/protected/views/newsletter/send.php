<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', '_NEWSLETTER'),
); ?>

<h3 class="title-bold"><?php echo Yii::t('standard', '_NEWSLETTER'); ?></h3>

<div class="sending-progress-wrapper">
	<div class="sending-progress process">
		<div><?php echo Yii::t('newsletter', '_SENDING_TO', array(
				'{total}' => $model->tot,
				'{size}' => $chunkSize,
				'{chunk}' => $currentChunk,
			));?></div>
		<div class="progress-wrapper">
			<span class="start-quantity">0</span>
			<div class="progress-line-wrapper">
				<div class="progress-line" style="width: 0%;"></div>
			</div>
			<span class="end-quantity"><?php echo $model->tot; ?></span>
		</div>
	</div>

	<div class="sending-progress sent-success" style="display: none;">
		<div>
			<span class="sent-successes-icon"></span>
			<div>
			<span><?php echo Yii::t('standard', '_OPERATION_SUCCESSFUL', array($model->tot)); ?></span>
			<p><?php echo Yii::t('newsletter', '_YOU_CAN_LEAVE_PAGE'); ?></p>
			</div>
		</div>
	</div>
	<div class="sending-progress" id="sent-error" style="display: none;">
		<div>
<!--			<span class="sent-successes-icon"></span>-->
			<div>
			<span><?php echo Yii::t('standard', '_OPERATION_FAILURE', array($model->tot)); ?></span>
			<p id="max_file_size" style="display: none;"><?php echo Yii::t('newsletter', 'Maximum total file size {size} mb reached!', array('{size}'=>10)); ?></p>
			</div>
		</div>
	</div>
</div>
<div class="newsletter-form-wrapper sending-newsletter">

	<div class="row recepients">
		<?php echo CHtml::label(Yii::t('standard', '_TO'), 'recepient'); ?>
		<div><span><span></span><?php echo Yii::t('standard', '_SELECTED', array($model->tot)); ?></span></div>
	</div>
	<div class="row">
		<label><?php echo CHtml::activeLabel($model, 'fromemail'); ?></label>
		<div><?php echo $model->fromemail; ?></div>
	</div>
	<div class="row">
		<label><?php echo CHtml::activeLabel($model, 'sub'); ?></label>
		<div><?php echo $model->sub; ?></div>
	</div>
	<div class="row">
		<label><?php echo CHtml::activeLabel($model, 'msg'); ?></label>
		<div class="description-text"><?php echo $model->msg; ?></div>
	</div>

</div>

<script type="text/javascript">
	$(document).ready(function() {
		sendNewsletter('<?=Docebo::createAdminUrl('newsletter/send', array('id'=>$model->id))?>');
	});
</script>

