<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', '_NEWSLETTER'),
); ?>

	<h3 class="title-bold"><?php echo Yii::t('standard', '_NEWSLETTER'); ?></h3>

<div class="newsletter-form-wrapper">
<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'newsletter-form',
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
	),
)); ?>

<?php //echo $form->errorSummary($model); ?>

<div class="row recipients">
		<?php echo CHtml::label(Yii::t('standard', '_TO'), 'recipient'); ?>
		<span class="p-sprite people-small-b-black"></span>
		<span>&nbsp;<?php echo Yii::t('standard', '_RECIPIENTS', array($model->tot)); ?></span>&nbsp;(<span id="recipients-count"><?= $model->tot ?></span>)&nbsp;

		<?php
			$url 	= Docebo::createLmsUrl('usersSelector/axOpen', array(
						'type' 	=> UsersSelector::TYPE_NEWSLETTER_RECIPIENTS,
						'useSessionData' => true,  // will read preselections from session; see Selector component
						'preserveSessionData' => true,
			));
			echo CHtml::link(Yii::t('standard', '_ADD'), $url, array(
				'class' => 'open-dialog newsletter-select-users btn btn-docebo green big',
				'rel' 	=> 'users-selector',
				'data-dialog-title' => Yii::t('standard', '_RECIPIENTS'),
				'title'	=> Yii::t('standard', '_RECIPIENTS')
			));
		?>


		<?php echo $form->error($model, 'tot'); ?>
	</div>
    <br/>
    <div class="row">
        <?=CHtml::label(Yii::t('inbox', 'Inactive Users'), 'inactive_users', array(
            'class' => 'nomargin'
        ));?>
        
        <span class="pull-left">
            <?=CHtml::checkBox('inactive_users'); ?>
            <?= Yii::t('inbox', 'Exclude suspended users from the recipients list'); ?>
        </span>
    </div>

	<br/>
	<div class="row">
		<?=CHtml::label(Yii::t('standard', 'Deliver'), 'deliver_type');?>

		<span class="pull-left">
			<?=$form->radioButton($model, 'send_type', array('value'=>CoreNewsletter::SEND_TYPE_EMAIL, 'uncheckValue'=>null))?>
			<?=Yii::t('inbox', 'By e-mail')?>
			<br/>
			<?=$form->radioButton($model, 'send_type', array('value'=>CoreNewsletter::SEND_TYPE_INBOX, 'uncheckValue'=>null))?>
			<?=Yii::t('inbox', "In the user's notifications area")?>
			<br/>
			<?=$form->radioButton($model, 'send_type', array('value'=>CoreNewsletter::SEND_TYPE_INBOX_AND_EMAIL, 'uncheckValue'=>null))?>
			<?=Yii::t('inbox', "By email and in the user's notifications area")?>
		</span>
		<div class="clearfix"></div>
	</div>
	<br/>

	<div class="row">
		<?php echo $form->labelEx($model, 'fromemail'); ?>
		<?php echo $form->textField($model, 'fromemail'); ?>
		<?php echo $form->error($model, 'fromemail'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'sub'); ?>
		<?php echo $form->textField($model, 'sub'); ?>
		<?php echo $form->error($model, 'sub'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'msg'); ?>
		<?php echo $form->textArea($model, 'msg'); ?>
		<?php echo $form->error($model, 'msg'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'file'); ?>
		<div id="container">
            <div id="fileserror"></div>
			<div id="filelist"><?php echo $model->renderFile(); ?></div>
			<a id="pickfiles" class="confirm-btn" href="javascript:void(0);"><?php echo Yii::t('organization', 'Upload File'); ?></a>
			<span><?php echo Yii::t('newsletter', 'Maximum total file size {size} mb', array('{size}'=>10)); ?></span>
		</div>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('standard', '_SEND'), array('class' => 'btn confirm-btn')); ?>
		<?php echo CHtml::link(Yii::t('standard', '_CANCEL'), Docebo::createAdminUrl('dashboard/index'), array('class' => 'btn cancel-btn')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">


	function usersSelectorSuccessCallback(data) {
		var url = '<?= Docebo::createAdminUrl('newsletter/axGetRecipientsFromSession') ?>';
		var data = {};
		$.get(url, data, function(data){
			res = jQuery.parseJSON(data);
			if (res.success == true) {
				$('#recipients-count').text(res.data.count);
			}

		});

	}

	(function() {
		TinyMce.attach("textarea", {height: 200,
			plugins : [
				"advlist autolink lists link image charmap print preview anchor",
				"searchreplace visualblocks code fullscreen",
				"insertdatetime media table contextmenu paste moxiemanager textcolor"
			],
			toolbar : "code | undo redo | styleselect | bold italic fontsizeselect forecolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
			language: yii.language  // this one is set in /common/controllers/Controller.php, beforeRender()
		});
	})();

	// Custom example logic
	$(function () {        
        var allFilesSize = 0; //Current files size;
        var maxFilesSize = 10485760; //10MB

		$('input, select').styler();

		var uploader = new plupload.Uploader({
			chunk_size: '10mb',
			runtimes: 'html5,flash,html4,gears,silverlight,browserplus',
			browse_button: 'pickfiles',
			container: 'container',
			max_file_size: '10mb',
			url : yii.urls.base + '/index.php?r=newsletter/handleFile',
			//flash_swf_url: '/plupload/js/plupload.flash.swf',
			flash_swf_url: yii.urls.base + '/../common/extensions/plupload/assets/plupload.flash.swf',
			silverlight_xap_url: '/plupload/js/plupload.silverlight.xap',
			/*filters: [
				{title: "Image files", extensions: "jpg,gif,png"},
				{title: "Zip files", extensions: "zip"}
			],*/
			multipart_params: {
				<?php echo Yii::app()->request->csrfTokenName; ?>: '<?php echo Yii::app()->request->csrfToken; ?>'
			}
		});

		uploader.bind('Init', function (up, params) {            
			$('#filelist .filesize').each(function() {
				$(this).html('('+plupload.formatSize(parseInt($(this).html()))+')');
			});
            
            //Get if previous uploaded files!            
            if($('.uploaded-file-size').length > 0){
                $('.uploaded-file-size').each(function(i){
                    allFilesSize += parseInt($(this).val());
                });
            }
			//$('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
		});

		uploader.init(); 

		uploader.bind('FilesAdded', function (up, files) {  
                        
            $('#fileserror').html("");                                                                                                                             
            $.each(files, function (i, file) { 
                
                //Calculate new total files size!
                allFilesSize += file.size;     
                
                //If we outweigh the files limits!
                if(allFilesSize > maxFilesSize){
                    $('#fileserror').append([
                        '<div>Error: <?=Yii::t('newsletter', 'Maximum total file size {size} mb reached!', array('{size}'=>10));?></div>'
                    ].join(''));               
                    
                    //Remove the uploaded file and change the files size!
                    uploader.removeFile(file);
                    allFilesSize -= file.size;
                } else{
                    var fileSize = '';
                    if (file.size != undefined) {
                        fileSize = '(' + plupload.formatSize(file.size) + ')';
                    }
                    $('#filelist').append([
                        '<div id="' + file.id + '" class="file">',
                        '<span class="file-icon"></span>',
                        '<span class="filename">' + file.name + '</span>',
                        '<span class="filesize">'+fileSize+'</span>',
                        '<div class="progress"><div></div></div>',
                        '<input type="hidden" name="CoreNewsletter[file]['+file.id+'][name]" value="'+file.name+'"/>',
                        '<input type="hidden" class="uploaded-file-size" name="CoreNewsletter[file]['+file.id+'][size]" value="'+file.size+'"/>',
                        '<span class="close">&times;</span></div>'
                    ].join(''));                                        
                }
            });                
            up.refresh(); // Reposition Flash/Silverlight

            uploader.start();           
		});

		uploader.bind('UploadProgress', function (up, file) {
			$('#' + file.id + " .progress div").css('width', file.percent + "%");
		});

		uploader.bind('Error', function (up, err) {
			/*$('#filelist').append("<div>Error: " + err.code +
				", Message: " + err.message +
				(err.file ? ", File: " + err.file.name : "") +
				"</div>"
			);*/
			$('#filelist').append([
				'<div>Error: ' + err.message,
				(err.file ? " File: " + err.file.name : ""),
				'</div>'
			].join(''));

			up.refresh(); // Reposition Flash/Silverlight
		});

		uploader.bind('FileUploaded', function (up, file) {
			$('#' + file.id + " .progress div").css('width', '100%').addClass('completed');
		});
                
        //Event for removing a file - remove file from the queue and change the total files size!      
        $(document.body).on('click', 'span.close', function(e) {
            e.preventDefault();
            var size = $(this).closest("div.file").find("input[class='uploaded-file-size']").val();
            var id = $(this).closest("div.file").attr('id');
            uploader.removeFile(uploader.getFile(id));            
            allFilesSize -= size;
          });
	});
</script>

