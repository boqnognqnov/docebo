<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'group-form',
	'htmlOptions' => array('class' => 'ajax-grid-form', 'data-grid' => '#group-grid'),
)); ?>
<div class="main-section">

	<div class="filters-wrapper">
		<div class="selections" data-grid="group-grid">
			<div class="input-wrapper">
				<input data-url="<?= Docebo::createAppUrl('admin:groupManagement/groupAutocomplete') ?>" data-type="core" class="typeahead" id="enroll-advanced-search-group" autocomplete="off" type="text" name="CoreGroup[groupid]" placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"/>
				<span class="search-icon"></span>
			</div>
			<?php $this->widget('ext.local.widgets.GridViewSelectAll', array(
				'gridId' => 'group-grid',
				'class' => 'left-selections',
				'dataProvider' => $model->dataProvider(),
				'itemValue' => 'idst',
				'sessionName' => 'newsletterGroupList',
			)); ?>
		</div>
	</div>
</div>

<input type="hidden" name="contentType" value="html"/>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>
<script type="text/javascript">
	(function ($) {
		$(function () {
			replacePlaceholder();
		});
	});
</script>