<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'user-form',
	'htmlOptions' => array('class' => 'ajax-grid-form', 'data-grid' => '#user-grid'),
)); ?>
<div class="main-section">

	<div class="filters-wrapper">
		<div class="selections" data-grid="user-grid">
			<div class="input-wrapper">
				<input data-url="<?= Docebo::createAppUrl('admin:userManagement/usersAutocomplete') ?>" data-type="core" data-source-desc="true" class="typeahead" id="enroll-advanced-search-user" autocomplete="off" type="text" name="CoreUser[search_input]" placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"/>
				<span class="search-icon"></span>
			</div>
			<?php $this->widget('ext.local.widgets.GridViewSelectAll', array(
				'gridId' => 'user-grid',
				'class' => 'left-selections',
				'dataProvider' => $model->dataProvider(),
				'itemValue' => 'idst',
				'sessionName' => 'newsletterUserList',
			)); ?>
		</div>
	</div>
</div>

<input type="hidden" name="contentType" value="html"/>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>
<script type="text/javascript">
	(function ($) {
		$(function () {
			replacePlaceholder();
		});
	});
</script>