<div class="courseEnroll-tabs">
	<ul class="nav nav-tabs">
		<li class="active">
			<a data-target=".courseEnroll-page-users" href="#">
				<span class="courseEnroll-users"></span><?php echo Yii::t('standard', '_USERS'); ?>
			</a>
		</li>
		<li>
			<a data-target=".courseEnroll-page-groups" href="#">
				<span class="courseEnroll-groups"></span><?php echo Yii::t('standard', '_GROUPS'); ?>
			</a>
		</li>
		<li>
			<a data-target=".courseEnroll-page-orgchart" href="#">
				<span class="courseEnroll-orgchart"></span><?php echo Yii::t('standard', '_ORGCHART'); ?>
			</a>
		</li>
		<!--
		<li>
			<a data-target=".courseEnroll-page-roles" href="#">
				<span class="courseEnroll-roles"></span><?php echo Yii::t('standard', '_FUNCTIONAL_ROLE'); ?>
			</a>
		</li>
		-->
	</ul>

	<div class="select-user-form-wrapper">
		<div class="tab-content">
			<div class="courseEnroll-page-users tab-pane active">
				<?php $this->renderPartial('_userFilters', array('model' => $userModel)); ?>
			</div>
			<div class="courseEnroll-page-groups tab-pane">
				<?php $this->renderPartial('_groupFilters', array('model' => $groupModel)); ?>
			</div>
			<div class="courseEnroll-page-orgchart tab-pane">
				<?php $this->renderPartial('_orgchartFilters'); ?>
			</div>
			<div class="courseEnroll-page-roles tab-pane">
				filter-4
			</div>
		</div>

		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'select-user-form',
			'htmlOptions' => array(
				'data-grid-items' => 'true',
			),
		)); ?>

			<div class="tab-content">
				<div class="courseEnroll-page-users tab-pane active">
					<?php $this->renderPartial('_user', array('model' => $userModel)); ?>
				</div>
				<div class="courseEnroll-page-groups tab-pane">
					<?php $this->renderPartial('_group', array('model' => $groupModel)); ?>
				</div>
				<div class="courseEnroll-page-orgchart tab-pane">
					<?php 
						$this->renderPartial('_orgchart', array(
							'fullOrgChartTree' => $fullOrgChartTree,
							'puLeafs' => $puLeafs,
						)); 
					?>
				</div>
				<div class="courseEnroll-page-roles tab-pane">
					form-4
				</div>
			</div>

		<?php $this->endWidget(); ?>

	</div>
</div>
<div class="modal-footer">
	<input class="btn confirm-btn" type="button" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" name="submit" data-submit-form="select-user-form"/>
	<input class="btn close-btn" type="button" value="<?php echo Yii::t('standard', '_CLOSE'); ?>"/>
</div>