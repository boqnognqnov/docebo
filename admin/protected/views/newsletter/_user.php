<div id="grid-wrapper" class="user-table">
<h2><?php echo Yii::t('standard', '_SELECT_USERS'); ?></h2>
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'user-grid',
		'htmlOptions' => array('class' => 'grid-view clearfix'),
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
		'dataProvider' => $model->dataProvider(),
		'cssFile' => false,
		'columns' => array(
			array(
				'class' => 'CCheckBoxColumn',
				'selectableRows' => 2,
				'id' => 'user-grid-checkboxes',
				'value' => '$data->idst',
				'checked' => '$data->inSessionList(\'newsletterUserTempList\', \'idst\');',
			),
			array(
				'name' => 'userid',
				'value' => 'Yii::app()->user->getRelativeUsername($data->userid);',
				'type' => 'raw',
			),
			array(
				'name' => 'fullname',
				'header' => Yii::t('standard', '_FULLNAME'),
				'value' => '$data->fullname',
				'type' => 'raw',
			),
			'email',
		),
		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'pager' => array(
			'class' => 'DoceboCLinkPager',
			'maxButtonCount' => 8,
		),
		'beforeAjaxUpdate' => 'function(id, options) {
							options.type = "POST";

							if (options.data == undefined)
								options.data = {}

							options.data.contentType = "html";
							options.data.selectedItems = $(\'#\'+id+\'-selected-items-list\').val();
						}',
		'ajaxUpdate' => 'user-grid-all-items',
		'afterAjaxUpdate' => 'function(id, data){$("#user-grid input").styler(); afterGridViewUpdate(id);}',
	)); ?>
</div>
<script type="text/javascript">
	$(function() {
		afterGridViewUpdate('user-grid');
	});
</script>