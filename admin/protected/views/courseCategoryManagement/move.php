<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'courseCategory_move',
		'htmlOptions'=>array(
			'class' => 'orgChart_move'
		),
	)); ?>

	<?php echo $form->hiddenField($model, 'idCategory', array('id' => 'currentNodeId')); ?>

	<div class="chart-tree">

		<?php if (!empty($fullOrgChartTree)) { ?>
			<ul class='node-tree'>
				<?php $level = 1; ?>
				<?php foreach ($fullOrgChartTree as $node) {
					if (in_array($node->idCategory, $allChildrenNodesIds)) {
						continue;
					}
					$left  = 10 * $node->lev;
					$rootNode = '';
					$hasChildren = '';
					if (!$node->isLeaf()) {
						$hasChildren = 'hasChildren';
					}
					if ($node->isRoot()) {
						$rootNode = 'rootNode';
					}
					?>
					<?php if ($node->lev < $level) { ?>
						<?php //for ($i = $node->lev; $i < $level; $i++) { echo '</ul></li>'; } ?>
						<?php for ($i = $node->lev; $i < $level; $i++) {
							echo '</ul>';
						} ?>
					<?php } ?>

				<li class="node nodeTree-node <?php echo $hasChildren.' ' .$rootNode; ?>">
					<div class="nodeTree-border<?php/* echo $rootNode; */?>">
						<div class="nodeTree-margin nodeTree-selectNode">
							<div class="clearfix" <?php if (empty($rootNode)) { ?>style="margin: 0px <?php echo $left; ?>px;"<?php } ?>>
								<div class="label-wrapper">
									<!--<span class="collapse-node"></span>-->
									<span></span>
									<div class="label-for-radio"><?php
										echo ( $node->coursesCategory->translation ? $node->coursesCategory->translation : $defaultCourseTreeTranslation[$node->idCategory] );
									?></div>
								</div>
								<?php echo $form->radioButton($model, 'idCategory[]', array('value' => $node->idCategory)); ?>
							</div>
						</div>
					</div>
					<?php if (!empty($hasChildren)) { ?>
					<? /*?><li class="nodeTree-node <?php echo $hasChildren; ?>"><?*/ ?>
					<ul class="nodeUl">
				<?php } ?>

					<?php if (empty($hasChildren)) { ?>
						</li>
					<?php } ?>

					<?php $level = $node->lev; ?>
				<?php } ?>
			</ul>
		<?php } ?>

	</div>

	<?php echo CHtml::hiddenField('treeGenerationTime', '', array('id' => 'tree-generation-time-node-move-input', 'class' => 'tree-generation-time-node-move-input')) ?>

	<?php $this->endWidget(); ?>
</div>

<div class="nodeParentsContainer">
	<div class="nodeParentsTitle"><?php echo Yii::t('organization', 'Your Category will be moved to:');?></div>
	<div class="nodeParents">
		<?php echo $contentParents; ?>
	</div>
</div>

<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input').styler();
			$('#courseCategory_move').jScrollPane({autoReinitialise: true});
		});

		//pass tree generation time as parameter
		var tgt = $('#tree-generation-time');
		if (tgt.length > 0 && tgt.val() != '') {
			var el = $('.tree-generation-time-node-move-input');
			if (el.length > 0) { el.val('' + tgt.val()); }
		}
	})(jQuery);
</script>