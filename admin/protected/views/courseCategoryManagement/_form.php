<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'courseCategory_form',
	)); ?>

	<div class="clearfix">
		<div class="languagesList">
			<div class="orgChart_form_title"><?php echo Yii::t('branding', '_PAGE_LANGUAGE_LABEL'); ?></div>
			<?php echo $form->dropDownList($model, 'lang_code', $activeLanguagesList); ?>
			<?php echo $form->error($model, 'lang_code'); ?>
		</div>

		<div class="orgChart_translationsList">
			<?php if (!empty($activeLanguagesList)) {
				reset($activeLanguagesList);
				$first_key = key($activeLanguagesList);

				?>
				<?php foreach ($activeLanguagesList as $key => $lang) {
					if (($key == $first_key && empty($model->lang_code)) || ($model->lang_code == $key)) {
						$class = 'show';
					} else {
						$class = 'hide';
					}
					?>
					<div id="NodeTranslation_<?php echo $key; ?>" class="languagesName <?php echo $class; ?>">
						<div class="orgChart_form_title"><?php echo Yii::t('standard', '_CATEGORY', array(str_replace(' *', '', $lang))); ?></div>
						<?php echo $form->textField($model, 'translation[' . $key . ']', array(
							'class' => 'orgChart_translation',
							'value' => (!empty($translationsList[$key]) ? $translationsList[$key] : '')
						)); ?>
						<?php echo $form->error($model, 'translation[' . $key . ']'); ?>
					</div>
				<?php } ?>
				<?php echo $form->error($model, 'translation'); ?>
			<?php } ?>
		</div>
	</div>

	<div class="orgChart_languages">
		<div><?php echo Yii::t('admin_lang', '_LANG_ALL').': <span>' . count($activeLanguagesList) . '</span>'; ?></div>
		<div id="orgChart_assignedCount"><?php echo Yii::t('standard', 'Filled languages').': <span>' . ((!empty($translationsList)) ? count(array_filter($translationsList)) : 0) . '</span>'; ?></div>
	</div>

	<?php echo CHtml::hiddenField('treeGenerationTime', '', array('id' => 'tree-generation-time-node-edit-input', 'class' => 'tree-generation-time-node-edit-input')) ?>

	<?php
	$html = "";
	// Event raised for YnY app to hook on to
	Yii::app()->event->raise('CourseCategoryFormAfterRender', new DEvent($this, array(
		'html' => &$html,
		'idCategory' => Yii::app()->getRequest()->getParam('id'),
		'form' => $form
 	)));
	echo $html;
	?>

	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
	(function ($) {
		$(function () {
			$('select').styler();
		});

		//pass tree generation time as parameter
		var tgt = $('#tree-generation-time');
		if (tgt.length > 0 && tgt.val() != '') {
			var el = $('.tree-generation-time-node-edit-input');
			if (el.length > 0) { el.val('' + tgt.val()); }
		}
	})(jQuery);
</script>

