<ul class="clearfix">
	<?php foreach ($nodeParents as $parent) { ?>
		<li><span><?php echo $parent->coursesCategory->translation;?></span></li>
	<?php } ?>
	<?php if (!empty($selectedNode->coursesCategory->translation)) { ?>
		<li><span><?php echo $selectedNode->coursesCategory->translation; ?></span></li>
	<?php } ?>
	<li class="last-node"><span><?php echo $currentNode->coursesCategory->translation; ?></span></li>
</ul>