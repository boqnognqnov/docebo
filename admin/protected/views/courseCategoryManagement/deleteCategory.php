<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'user_form',
	));	?>

	<p><?php echo Yii::t('standard', '_CATEGORY') . ': "' . $currentNode->coursesCategory->translation . '"'; ?></p>

	<div class="clearfix">
		<?php if ($currentNode->isLeaf()) { ?>
			<?php echo $form->checkbox($model, 'confirm', array('id' => CHtml::activeId($model, 'confirm').$checkboxIdKey, 'onchange' => $onChange)); ?>
			<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed', array($currentNode->coursesCategory->translation)), CHtml::activeId($model, 'confirm').$checkboxIdKey); ?>
			<?php echo $form->error($model, 'confirm'); ?>
		<?php } else { ?>
			<p><?php echo Yii::t('course_management', 'You can\'t delete a category that contains sub categories'); ?></p>
		<?php } ?>

		<?php echo CHtml::hiddenField('treeGenerationTime', '', array('id' => 'tree-generation-time-node-delete-input', 'class' => 'tree-generation-time-node-delete-input')) ?>

	</div>
	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
	(function ($) {
		//pass tree generation time as parameter
		var tgt = $('#tree-generation-time');
		if (tgt.length > 0 && tgt.val() != '') {
			var el = $('.tree-generation-time-node-delete-input');
			if (el.length > 0) { el.val('' + tgt.val()); }
		}
		//handle ajax form response
		$(document).delegate(".modal-branding-external-page", "dialog2.content-update", function() {
			var e = $(this), autoclose = e.find("a.auto-close");
			if (autoclose.length > 0) {
				e.find('.modal-body').dialog2("close");
				$.fn.yiiListView.update("external-pages-management-list");
			} else {
				var err = e.find("a.error");
				if (err.length > 0) {
					var msg = $(err[0]).data('message');
					Docebo.Feedback.show('error', msg);
					e.find('.modal-body').dialog2("close");
				}
			}
		});
	})(jQuery);
</script>