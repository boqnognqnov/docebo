<?php /* @var $model LearningCourseCategoryTree */?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'courseCategory_settings',
		'htmlOptions'=> array(
			'class' => 'ajax form-horizontal docebo-form'
		),
	)); ?>

	<div style="margin-bottom: 20px;"><?= Yii::t('course', '_CATEGORY_SELECTED')?>: <b><?= $model->coursesCategoryTranslated->translation?></b></div>

	<div class="control-container odd">
		<div class="control-group">
			<?php echo $form->labelEx($model, 'soft_deadline', array('class' => 'control-label')); ?>
			<div class="controls">
				<label class="checkbox">
					<input type="checkbox" name="enable_soft_deadline_option" id="enable_soft_deadline_option" value="1" <?php if(!is_null($model->soft_deadline)):?>checked="checked"<?php endif;?>>
					<?php echo Yii::t('course', 'Enable custom deadline settings for all courses in this category'); ?>
				</label>
				<label id="soft_deadline_checkBox_wrapper" class="checkbox" <?php if(is_null($model->soft_deadline)):?>style="opacity: 0.5;;"<?php endif;?>>
					<?php
					$checkStatus = false;
					if(!is_null($model->soft_deadline))
						$checkStatus = $model->soft_deadline == LearningCourse::SOFT_DEADLINE_ON;
					else {
						$categoryLevelDeadline = $model->getInheritedSoftDeadline();
						if($categoryLevelDeadline)
							$checkStatus = $categoryLevelDeadline['soft_deadline'] == LearningCourse::SOFT_DEADLINE_ON;
						else
							$checkStatus = Settings::get('soft_deadline', 'off') == 'on';
					}

					echo $form->checkbox($model, 'soft_deadline', array(
							'disabled' => is_null($model->soft_deadline) ? "disabled" : "",
							'uncheckValue' => 0,
							'value' => 1,
							'checked' => $checkStatus
						)
					);
					?>
					<?php echo Yii::t('course', 'Allow users to enter an E-Learning course even if the enrollment validity or course end date is expired'); ?>
				</label>
			</div>
		</div>
	</div>

	<!-- For Dialog2 - they go in footer -->
	<div class="form-actions">
		<?= CHtml::submitButton(Yii::t('standard','_CONFIRM'), array('name'  => 'confirm_button', 'class' => 'btn-docebo green big')); ?>
		<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' 	=> 'btn-docebo black big close-dialog')); ?>
	</div>

	<?php $this->endWidget(); ?>
</div>

<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input').styler();

			$('#enable_soft_deadline_option').on('change',function(){
				if($(this).is(':checked')){
					$('#soft_deadline_checkBox_wrapper').css('opacity', '1');
					$('#LearningCourseCategoryTree_soft_deadline').prop('disabled', false).trigger('refresh');
				}
				else {
					$('#soft_deadline_checkBox_wrapper').css('opacity', '0.5');
					<?php if(is_null($model->soft_deadline) && $model->hasSoftDeadline(true)): ?>
					$('#LearningCourseCategoryTree_soft_deadline').attr('checked', true).prop('disabled', 'disabled').trigger('refresh');
					<?php else: ?>
					$('#LearningCourseCategoryTree_soft_deadline').attr('checked', false).prop('disabled', 'disabled').trigger('refresh');
					<?php endif; ?>
				}
			});
		});
	})(jQuery);
</script>