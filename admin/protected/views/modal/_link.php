<?php $id = uniqid(); ?>

<a data-toggle="modal" data-remote="<?php echo $this->createUrl($url, !empty($params) ? $params : array()); ?>" data-target="#modal-<?php echo $id; ?>">click
	me</a>
<div id="modal-<?php echo $id; ?>" class="modal fade" aria-labelledby="label-<?php echo $id; ?>">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="label-<?php echo $id; ?>"><?php echo $headerTitle; ?></h3>
	</div>
	<div class="modal-body"></div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal">Close</button>
		<button class="btn btn-primary">Save changes</button>
	</div>
</div>