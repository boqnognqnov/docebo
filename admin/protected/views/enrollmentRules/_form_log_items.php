<h1><?= Yii::t('standard', '_DETAILS') ?></h1>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'log-items-grid',
	'htmlOptions' => array('class' => 'grid-view clearfix', 'style' => 'padding-top: 16px;'),
	'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
	'afterAjaxUpdate'=>'function(id, data){$(document).controls();}',
	'dataProvider' => $model->search(),
	'cssFile' => false,
	'columns' => array(
		array(
			'name' => Yii::t('standard', '_USERNAME'),
			'value' => '(is_null($data->user->clearUserId) ? $data->deleted_user->getClearUserId()." ('.Yii::t('standard', '_USER_STATUS_CANCELLED').')" : $data->user->clearUserId)',
		),
		array(
			'name' => Yii::t('standard', '_FIRSTNAME'),
			'value' => '(is_null($data->user->firstname) ? $data->deleted_user->firstname : $data->user->firstname)',
		),
		array(
			'name' => Yii::t('standard', '_LASTNAME'),
			'value' => '(is_null($data->user->lastname) ? $data->deleted_user->lastname : $data->user->lastname)',
		),
		array(
			'name' => Yii::t('standard', '_CODE'),
			'value' => '($data->enrollLog->rule->rule_type == CoreEnrollRule::RULE_GROUP_COURSE || $data->enrollLog->rule->rule_type == CoreEnrollRule::RULE_BRANCH_COURSE) ? $data->course->code : $data->curricula->path_code',
		),
		array(
			'name' => Yii::t('standard', '_NAME'),
			'value' => '($data->enrollLog->rule->rule_type == CoreEnrollRule::RULE_GROUP_COURSE || $data->enrollLog->rule->rule_type == CoreEnrollRule::RULE_BRANCH_COURSE) ? $data->course->name : $data->curricula->path_name',
		),
		array(
			'class' => 'CustomButtonColumn',
			'buttons' => array(
				'rollback' => array(
					'url' => 'Yii::app()->createUrl("enrollmentRules/rollbackLogItem", array("id" => $data->log_item_id))',
					'label' => '',
					'visible' => '$data->rollback ? false : true',
					'options' => array(
						'class' => 'open-dialog i-sprite is-assign',
						'title' => Yii::t('standard', '_ROLLBACK'),
					),
				),
				'rollbackDisabled' => array(
					'url' => '',
					'label' => '',
					'visible' => '$data->rollback ? true : false',
					'options' => array(
						'class' => 'i-sprite is-assign grey',
						'title' => Yii::t('standard', '_EMPTY_SELECTION'),
					),
				),
			),
			'template' => '{rollback}{rollbackDisabled}',
		)
	),
	'template' => '{items}{summary}{pager}',
	'summaryText' => Yii::t('standard', '_TOTAL'),
	'pager' => array(
		'class' => 'DoceboCLinkPager',
		'maxButtonCount' => 8,
	),
)); ?>


<script type="text/javascript">
	$(function(){
		$(document).controls();
	});
</script>