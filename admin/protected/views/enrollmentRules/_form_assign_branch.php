<style>
	<!--
	.modal {
		width: 640px;
		margin-left: -320px;
		top: 5%;
	}
	.modal-body {
		width: 600px;
	}
	-->
</style>

<h1><?= Yii::t('standard', 'branches') ?></h1>
<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'group-form',
	'htmlOptions' => array('class' => 'ajax', 'data-grid' => '#group-grid'),
)); ?>
<div class="tab-pane" id="userForm-orgchat">
	<?php if (!empty($fullOrgChartTree)) { ?>
		<ul class='node-tree'>
			<?php $level = 1; ?>
			<?php foreach ($fullOrgChartTree as $node) {
				/*if ($node->idOrg == $model->idOrg) {
					continue;
				}*/
				$left = 10 * $node->lev;
				$rootNode = '';
				
				
				$hasChildren = 'hasChildren';
				// If node is a REAL leaf, set hasChildren to '' (NO children)
				if ($node->isLeaf()) {
					$hasChildren = '';
				}
				// Maybe this node is a "fake" leaf due to Power User assignments? (see CoreAdminTree::getPowerUserOrgChartsOnly())
				else if (in_array($node->idOrg, $puLeafs)) {
					$hasChildren = '';
				}
								
				
				
				
				if ($node->isRoot()) {
					$rootNode = 'rootNode';
				}
				?>
				<?php if ($node->lev < $level) { ?>
					<?php //for ($i = $node->lev; $i < $level; $i++) { echo '</ul></li>'; } ?>
					<?php for ($i = $node->lev; $i < $level; $i++) {
						echo '</ul>';
					} ?>
				<?php } ?>

			<li class="node nodeTree-node <?php echo $hasChildren.' '.$rootNode; ?>">
				<div>
					<?php if ($node->isRoot()) { ?>
						<span class="node-icon"></span>
					<?php } elseif (!$node->isLeaf()) { ?>
						<span class="open-close-link"></span>
					<?php } ?>
					<?php
					$htmlOptions = array(
						'value' => $node->idOrg,
						'id' => 'chartGroups_'.$node->idOrg,
					);
					$checked = in_array($node->idOrg, $selected) ? true : false;
					if ($node->lev > 1) {
						echo CHtml::checkBox('chartGroups[]', $checked, $htmlOptions);
					}
					echo CHtml::label($node->coreOrgChart->translation,'chartGroups_'.$node->idOrg, array(
						'label' => $node->coreOrgChart->translation,
						'class' => 'label',
					));
					?>
				</div>
				<?php if (!empty($hasChildren)) { ?>
				<ul class="nodeUl">
			<?php } ?>

				<?php if (empty($hasChildren)) { ?>
					</li>
				<?php } ?>

				<?php $level = $node->lev; ?>
			<?php } ?>
		</ul>

		<!-- .node-tree -->
	<?php } ?>
</div>
<?php echo CHtml::hiddenField('id', $ruleModel->rule_id);?>
<?php echo CHtml::link('', $this->createUrl('enrollmentRules/createRule', array('id' => $ruleModel->rule_id)), array('class' => 'ajax previous-step', 'style'=>'display: none;')); ?>
<div class="form-actions">
	<? if ($singleStep) : ?>
		<?php echo CHtml::submitButton(Yii::t('standard','_SAVE'), array('class' => 'btn btn-submit', 'name' => 'submit')); ?>
	<? else : ?>
		<?php echo CHtml::button(Yii::t('standard','_PREV'), array('class' => 'btn btn-cancel enrollrule-prev pull-left')); ?>
		<?php echo CHtml::submitButton(Yii::t('standard','_NEXT'), array('class' => 'btn btn-submit', 'name' => 'submit')); ?>
	<? endif; ?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(function(){
		$('input, select').styler();
	});
</script>