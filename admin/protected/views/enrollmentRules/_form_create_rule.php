<?php
$textAreaId = 'message-textarea';
?>

<style>
	<!--
	.modal {
		width: 500px;
		margin-left: -250px;
		top: 5%;
	}
	.modal-body {
		width: 500px;
	}
	-->
</style>

<h1><?= Yii::t('preassessment', '_ADD_RULE') ?></h1>
<div class="form">
<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'core-enroll-rule-form',
	'htmlOptions' => array(
		'class' => 'ajax',
	)
)); ?>
<div class="row-fluid" style="width: 450px;">
	<div class="span12">
		<?php echo $form->labelEx($model, 'title'); ?>
		<?php echo $form->textField($model, 'title'); ?>
		<?php echo $form->error($model, 'title'); ?>
	</div>
	<div class="row-fluid">
	<div class="span5">
		<?php echo $form->labelEx($model, 'subtype_first'); ?>
		<?php echo $form->dropDownList($model, 'subtype_first', CoreEnrollRule::getFirstSubTypeList(), array('style'=>'width: 183px;')); ?>
		<?php echo $form->error($model, 'subtype_first'); ?>
	</div>
	<div class="span2 subtype-arrow">
		<div class="i-sprite is-arrow-right"></div>
	</div>
	<div class="span5">
		<?php echo $form->labelEx($model, 'subtype_second'); ?>
		<?php echo $form->dropDownList($model, 'subtype_second', CoreEnrollRule::getSecondSubTypeList(), array('style'=>'width: 183px;')); ?>
		<?php echo $form->error($model, 'subtype_second'); ?>
	</div>
	</div>
</div>
	<div class="form-actions">
		<?php echo CHtml::submitButton(Yii::t('standard','_NEXT'), array('class' => 'btn btn-submit notification-submit')); ?>
		<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
	</div>
<?php $this->endWidget(); ?>
</div>

<script type="text/javascript">
	<!--
	$(function(){
		$('input, select').styler();
	});
	//-->
</script>