<style>
	<!--
	.modal {
		width: 640px;
		margin-left: -320px;
		top: 5%;
	}
	.modal-body {
		width: 600px;
	}
	-->
</style>

<h1><?= Yii::t('standard', '_COURSEPATH') ?></h1>
<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'group-form',
	'htmlOptions' => array('class' => 'ajax', 'data-grid' => '#curricula-grid'),
)); ?>
<div class="main-section">
	<h3 class="title-bold"><?php echo Yii::t('standard', '_SEARCH')?></h3>

	<div class="filters-wrapper">
		<div class="selections" data-grid="curricula-grid">
			<div class="input-wrapper">
				<input id="search-input" type="text" name="search_input" placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"/>
				<span class="search-icon"></span>
			</div>
			<?php $this->widget('ext.local.widgets.GridViewSelectAll', array(
				'gridId' => 'curricula-grid',
				'class' => 'left-selections',
				'dataProvider' => $ruleModel->curriculaSearch(),
				'itemValue' => 'id_path',
			)); ?>
		</div>
	</div>
</div>
<?php echo CHtml::hiddenField('id', $ruleModel->rule_id);?>
<?php echo CHtml::link('', $this->createUrl('enrollmentRules/assign'.ucfirst($ruleModel->subtype_first), array('id' => $ruleModel->rule_id)), array('class' => 'ajax previous-step', 'style'=>'display: none;')); ?>
<div class="form-actions">
	<? if ($singleStep) : ?>
		<?php echo CHtml::submitButton(Yii::t('standard','_SAVE'), array('class' => 'btn btn-submit')); ?>
	<? else : ?>
		<?php echo CHtml::button(Yii::t('standard','_PREV'), array('class' => 'btn btn-cancel enrollrule-prev pull-left')); ?>
		<?php echo CHtml::submitButton(Yii::t('standard','_SAVE'), array('class' => 'btn btn-submit')); ?>
	<? endif; ?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
</div>
<?php $this->endWidget(); ?>

<div id="grid-wrapper" class="group-table">
	<h2><?php echo Yii::t('standard', '_COURSEPATH'); ?></h2>
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'curricula-grid',
		'htmlOptions' => array('class' => 'grid-view clearfix enrollment-rules-grid'),
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
		'dataProvider' => $ruleModel->curriculaSearch(),
		'cssFile' => false,
		'columns' => array(
			array(
				'class' => 'CCheckBoxColumn',
				'selectableRows' => 2,
				'id' => 'curricula-grid-checkboxes',
				'value' => '$data->id_path',
				'checked' => '$data->inSessionList(\'selectedItems\', \'id_path\');',
			),
			array(
				'name' => Yii::t('standard', '_CODE'),
				'value' => '$data->path_code',
				'type' => 'raw',
			),
			array(
				'name' => Yii::t('standard', '_NAME'),
				'value' => '$data->path_name',
				'type' => 'raw',
			),
		),
		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'pager' => array(
			'class' => 'DoceboCLinkPager',
			'maxButtonCount' => 8,
		),
		'beforeAjaxUpdate' => 'function(id, options) {
			options.type = "POST";
			if (options.data == undefined) {
				options.data = {
					contentType: "html",
					selectedItems: $(\'#\'+id+\'-selected-items-list\').val()
				}
			}
		}',
		'afterAjaxUpdate' => 'function(id, data){$("#curricula-grid input").styler(); afterGridViewUpdate(id);}',
	)); ?>
</div>

<script type="text/javascript">
	$(function(){
		$('input, select').styler();
		updateSelectedCheckboxesCounter('curricula-grid');
	});
</script>