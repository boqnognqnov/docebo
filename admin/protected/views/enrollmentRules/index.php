<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', '_ENROLLRULES'),
);

$this->renderPartial('_menuLinks', array());
?>
<h2><?php echo Yii::t('standard', '_ENROLLRULES'); ?></h2>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'enrollment-rules-grid',
	'htmlOptions' => array('class' => 'grid-view clearfix', 'style' => 'padding-top: 16px;'),
	'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
	'afterAjaxUpdate'=>'function(id, data){$(document).controls();}',
	'dataProvider' => $model->search(),
	'cssFile' => false,
	'columns' => array(
		'title',
		array(
			'name' => 'rule_type',
			'value' => '$data->getTypeLabel()',
		),
		array(
			'name' => '',
			'value' => '$this->grid->controller->widget("EnrollmentRulesGridFormatter", array("model"=>$data, "subtype" => "first"), true);',
			'type' => 'raw',
		),
		array(
			'name' => '',
			'value' => '$this->grid->controller->widget("EnrollmentRulesGridFormatter", array("model"=>$data, "subtype" => "second"), true);',
			'type' => 'raw',
		),
		array(
			'class' => 'CustomButtonColumn',
			'buttons' => array(
				'active' => array(
					'url' => 'Yii::app()->createUrl("enrollmentRules/toggleActive", array("id" => $data->rule_id))',
					'label' => '',
					'click' => 'js:function() {
						EnrollmentRule.toggleActive($(this).attr(\'href\'));
						return false;
					}',
					'visible' => '$data->active ? true : false',
					'options' => array(
						'class' => 'i-sprite is-circle-check green',
						'title' => Yii::t('standard', '_DEACTIVATE'),
					)
				),
				'inactive' => array(
					'url' => 'Yii::app()->createUrl("enrollmentRules/toggleActive/activateUser", array("id" => $data->rule_id))',
					'label' => '',
					'visible' => '!$data->active ? true : false',
					'click' => 'js:function() {
						EnrollmentRule.toggleActive($(this).attr(\'href\'));
						return false;
					}',
					'options' => array(
						'class' => 'i-sprite is-circle-check grey',
						'title' => Yii::t('standard', '_ACTIVATE'),
					)
				),
				'edit' => array(
					'url' => 'Yii::app()->createUrl("enrollmentRules/updateRule", array("id" => $data->rule_id))',
					'label' => '',
					'options' => array(
						'class' => 'open-dialog i-sprite is-edit',
					),
				),
				'remove' => array(
					'url' => 'Yii::app()->createUrl("enrollmentRules/deleteRule", array("id" => $data->rule_id))',
					'label' => '',
					'options' => array(
						'class' => 'open-dialog i-sprite is-remove red'
					)
				),
			),
			'template' => '{active}{inactive}{edit}{remove}',
		)
	),
	'template' => '{items}{summary}{pager}',
	'summaryText' => Yii::t('standard', '_TOTAL'),
	'pager' => array(
		'class' => 'DoceboCLinkPager',
		'maxButtonCount' => 8,
	),
)); ?>