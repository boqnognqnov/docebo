<style>
	<!--
	.modal {
		width: 500px;
		margin-left: -250px;
		top: 10%;
	}
	.modal-body {
		width: 500px;
	}
	-->
</style>

<h1><?= Yii::t('standard', '_DEL') ?></h1>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'delete-rule-form',
		'htmlOptions' => array(
			'class' => 'ajax',
		)
	)); ?>
	<input type="hidden" name="delete"/>
	<p><?=Yii::t('standard', 'Yes, I confirm I want to proceed')?></p>
	<div class="form-actions">
		<?php echo CHtml::submitButton(Yii::t('standard','_DEL'), array('class' => 'btn btn-danger notification-submit')); ?>
		<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>