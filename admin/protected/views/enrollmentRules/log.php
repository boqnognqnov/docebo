<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', '_ENROLLRULES') => Docebo::createAppUrl('admin:enrollmentRules/index'),
	Yii::t('enrollrules', '_SHOW_LOGS'),
);

$this->renderPartial('_menuLinks', array());
?>
	<h2><?php echo Yii::t('standard', '_ENROLLRULES'); ?></h2>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'rules-log-grid',
	'htmlOptions' => array('class' => 'grid-view clearfix', 'style' => 'padding-top: 16px;'),
	'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
	'afterAjaxUpdate'=>'function(id, data){$(document).controls();}',
	'dataProvider' => $model->search(),
	'cssFile' => false,
	'columns' => array(
		array(
			'name' => Yii::t('standard', '_DATE'),
			'value' => '$data->date_created',
		),
		array(
			'name' => 'rule_type',
			'value' => '$data->rule->getTypeLabel()',
		),
		array(
			'class' => 'CustomButtonColumn',
			'buttons' => array(
				'rollback' => array(
					'url' => 'Yii::app()->createUrl("enrollmentRules/rollbackLog", array("id" => $data->enroll_log_id))',
					'label' => '',
					'visible' => '$data->isRollbackPerformed ? false : true',
					'options' => array(
						'class' => 'open-dialog i-sprite is-assign',
						'title' => Yii::t('standard', '_ROLLBACK'),
					),
				),
				'rollbackDisabled' => array(
					'url' => '',
					'label' => '',
					'visible' => '$data->isRollbackPerformed ? true : false',
					'options' => array(
						'class' => 'i-sprite is-assign grey',
						'title' => Yii::t('standard', '_EMPTY_SELECTION'),
					),
				),
				'list' => array(
					'url' => 'Yii::app()->createUrl("enrollmentRules/listLogItems", array("id" => $data->enroll_log_id))',
					'label' => '',
					'options' => array(
						'class' => 'open-dialog i-sprite is-menu',
						'title' => Yii::t('standard', '_USERS'),
					),
				),
			),
			'template' => '{rollback}{rollbackDisabled}{list}',
		)
	),
	'template' => '{items}{summary}{pager}',
	'summaryText' => Yii::t('standard', '_TOTAL'),
	'pager' => array(
		'class' => 'DoceboCLinkPager',
		'maxButtonCount' => 8,
	),
)); ?>