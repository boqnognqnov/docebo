<div class="main-actions clearfix">
	<h3 class="title-bold"><?php echo Yii::t('standard', '_ENROLLRULES'); ?></h3>
	<ul class="clearfix">
		<li>
			<div>

				<a href="<?=$this->createUrl('enrollmentRules/createRule')?>"
				   alt="<?php echo Yii::t('helper', 'New enroll rule'); ?>"
				   class="open-dialog new-node"
				   closeOnEscape="false"
				   closeOnOverlayClick="false"
				   rel="enrollment-rule-modal"
				   data-dialog-class="new-enrollment-rule-modal">

					<span></span> <?= Yii::t('preassessment', '_ADD_RULE') ?>
				</a>
			</div>
		</li>
		<li>
			<div>
				<a href="<?=$this->createUrl('enrollmentRules/log')?>"
				   alt="<?php echo Yii::t('helper', 'Enroll rules show logs'); ?>"
				   class="new-node">
					<span></span> <?= Yii::t('enrollrules', '_SHOW_LOGS') ?>
				</a>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4></h4>

			<p></p>
		</div>
	</div>
</div>
