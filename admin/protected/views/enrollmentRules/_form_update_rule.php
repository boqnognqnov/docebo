<?php
$textAreaId = 'message-textarea';
?>

<style>
	<!--
	.modal {
		width: 500px;
		margin-left: -250px;
		top: 5%;
	}
	.modal-body {
		width: 500px;
	}
	-->
</style>

<h1><?= Yii::t('preassessment', 'Edit rule') ?></h1>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'core-enroll-rule-form',
		'htmlOptions' => array(
			'class' => 'ajax',
		)
	)); ?>
	<div class="row-fluid" style="width: 450px;">
		<div class="span12">
			<?php echo $form->labelEx($model, 'title'); ?>
			<?php echo $form->textField($model, 'title'); ?>
			<?php echo $form->error($model, 'title'); ?>
		</div>
	</div>
	<div class="form-actions">
		<?php echo CHtml::submitButton(Yii::t('standard','_SAVE'), array('class' => 'btn btn-submit notification-submit')); ?>
		<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>

<script type="text/javascript">
	<!--
	$(function(){
		$('input, select').styler();
	});
	//-->
</script>