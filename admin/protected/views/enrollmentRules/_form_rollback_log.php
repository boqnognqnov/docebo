<style>
	<!--
	.modal {
		width: 500px;
		margin-left: -250px;
		top: 10%;
	}
	.modal-body {
		width: 500px;
	}
	-->
</style>

<h1><?= Yii::t('standard', '_ROLLBACK') ?></h1>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'delete-rule-form',
		'htmlOptions' => array(
			'class' => 'ajax',
		)
	)); ?>
	<input type="hidden" name="rollback"/>
	<?=CHtml::checkBox('agree', false, array('onchange'=>'EnrollmentRule.toggleConfirmButton(this)'))?> <?=CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed'), 'agree', array('style'=>'display: inline-block'))?>
	<div class="form-actions">
		<?php echo CHtml::submitButton(Yii::t('standard','_CONFIRM'), array('class' => 'btn btn-danger confirm-dialog-button disabled')); ?>
		<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>

<script type="text/javascript">
	$(function(){
		$('input, select').styler();
	});

</script>