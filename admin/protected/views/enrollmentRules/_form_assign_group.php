<style>
	<!--
	.modal {
		width: 640px;
		margin-left: -320px;
		top: 5%;
	}
	.modal-body {
		width: 600px;
	}
	-->
</style>

<h1><?= Yii::t('standard', '_GROUPS') ?></h1>
<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'group-form',
	'htmlOptions' => array('class' => 'ajax', 'data-grid' => '#group-grid'),
)); ?>
	<div class="main-section">

		<div class="filters-wrapper">
			<div class="selections" data-grid="group-grid">
				<div class="input-wrapper">
					<input id="search-input" type="text" name="search_input" placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"/>
					<span class="search-icon"></span>
				</div>
				<?php $this->widget('ext.local.widgets.GridViewSelectAll', array(
					'gridId' => 'group-grid',
					'class' => 'left-selections',
					'dataProvider' => $ruleModel->groupSearch(),
					'itemValue' => 'idst',
				)); ?>
			</div>
		</div>
	</div>
<?php echo CHtml::hiddenField('id', $ruleModel->rule_id);?>
<?php echo CHtml::link('', $this->createUrl('enrollmentRules/createRule', array('id' => $ruleModel->rule_id)), array('class' => 'ajax previous-step', 'style'=>'display: none;')); ?>
<div class="form-actions">
	<? if ($singleStep) : ?>
		<?php echo CHtml::submitButton(Yii::t('standard','_SAVE'), array('class' => 'btn btn-submit')); ?>
	<? else : ?>
		<?php echo CHtml::button(Yii::t('standard','_PREV'), array('class' => 'btn btn-cancel enrollrule-prev pull-left')); ?>
		<?php echo CHtml::submitButton(Yii::t('standard','_NEXT'), array('class' => 'btn btn-submit')); ?>
	<? endif; ?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
</div>
<?php $this->endWidget(); ?>

<div id="grid-wrapper" class="group-table">
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'group-grid',
		'htmlOptions' => array('class' => 'grid-view clearfix enrollment-rules-grid'),
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
		'dataProvider' => $ruleModel->groupSearch(),
		'cssFile' => false,
		'columns' => array(
			array(
				'class' => 'CCheckBoxColumn',
				'selectableRows' => 2,
				'id' => 'group-grid-checkboxes',
				'value' => '$data->idst',
				'checked' => '$data->inSessionList(\'selectedItems\', \'idst\');',
			),
			array(
				'name' => 'groupid',
				'value' => '$data->relativeId($data->groupid)',
				'type' => 'raw',
			),
		),
		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'pager' => array(
			'class' => 'DoceboCLinkPager',
			'maxButtonCount' => 8,
		),
		'beforeAjaxUpdate' => 'function(id, options) {
			options.type = "POST";
			if (options.data == undefined) {
				options.data = {
					contentType: "html",
					selectedItems: $(\'#\'+id+\'-selected-items-list\').val()
				}
			}
		}',
		'afterAjaxUpdate' => 'function(id, data){$("#group-grid input").styler(); afterGridViewUpdate(id);}',
	)); ?>
</div>

<script type="text/javascript">
	$(function(){
		$('input, select').styler();
		updateSelectedCheckboxesCounter('group-grid');
	});
</script>