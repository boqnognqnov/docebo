<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('menu', '_USER_MANAGMENT') => array('userManagement/index'),
	Yii::t('organization_chart', 'Manage users via CSV'),
); ?>
<style>
	@media (max-width: 980px) and  (min-width: 768px){
		#importTab{
			width: 28%;
		}
		.tab-content{
			width: 68%;
		}
	}
	#user-import-form, #user-import-form-activation{
		margin-bottom: 0 !important;
	}
</style>
	<ul class="nav nav-tabs" id="importTab" style="display: inline-block;width: 25%">
		<li>
			<a href="#importOld" data-toggle="tab">
				<div class="icon">
					<span class="i-sprite is-putin" style="margin-right: 10px;"></span>
					<?= Yii::t('standard', 'Import users') ?>
				</div>
			</a>
		</li>
		<li>
			<a href="#importNew" data-toggle="tab">
				<div class="icon">
					<span class="i-sprite is-circle-check" style="margin-right: 10px;"></span>
					<?= Yii::t('standard', 'Activation/Deactivation') ?>
				</div>
			</a>
		</li>
	</ul>

<div class="tab-content" style="display: inline-block;margin-left: 20px;width: 70%">
	<div id="importOld" class="tab-pane">
		<?php $this->renderPartial('import_index', array('model' => $model)) ?>
	</div>
	<div id="importNew" class="tab-pane">
		<?php $this->renderPartial('import_activation', array('model' => $model)) ?>
	</div>
</div>

<script type="text/javascript">

	(function ($) {
		$(function () {
			function changeStyle(){
				$('#importTab span.white').removeClass('white');
				$(this).find('span').addClass('white');
				setTimeout(function(){
					var h = $('ul#importTab').parent().height();
					$('ul#importTab').height(h);
				},10)

			}

//			$('input,select').styler({browseText: '<?//=Yii::t('course_management', 'CHOOSE FILE')?>//'});
			$('#importTab a:first').tab('show');
			$('#importTab a').on('click',changeStyle);
			$('#importTab a').first().trigger('click');
			$('#importTab li').first().find('span').addClass('white');
		});
	})(jQuery);
</script>