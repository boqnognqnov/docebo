<?php
$onChange      = 'return (($(this).prop("checked")) ? $(".btn.btn-docebo.green.big").show().removeClass("hidden") : $(".btn.btn-docebo.green.big").hide().addClass("hidden"));';
$checkboxIdKey = time();
$count         = count($idst);

$progressiveRunUrl = Docebo::createLmsUrl('progress/run', array(
		'type'		=> Progress::JOBTYPE_UNASSIGN_USERS_FROM_BRANCH,
        'groups'    => $groups,

	));
?>

<?php
if (!$currentNode): // If Selected Root Branch ?>
    <div class="error">
        <?php echo Yii::t('standard', 'Operation <b>Remove</b> is not allowed on the root branch'); ?>
    </div>

<?php else: ?>
    <div class="form">
        <?php
        echo CHtml::beginForm($progressiveRunUrl, 'post', array(
            'class'	    => 'ajax',
            'id' 		=> 'user_form',
        ));
        ?>
        <div class="row-fluid">
            <div id="remove-user" style="display: inline-block;"><?= Yii::t('standard', '_REMOVE_FROM_NODE')?><b><?=$breadcrumbs?></b></div>
        </div>
        <div style="margin-top: 20px;" class="row-fluid span12 confirm-unnasign">
                <div class="row-fluid">
                    <div class="span6">
                        <div class="row-fluid">
                            <?= CHtml::checkBox('confirmDelete', false, array(
                                'style' => 'float: left;',
                                'name' => 'confirmDelete',
                                'onchange' => $onChange
                            )) ?>
                            &nbsp;
                            <?php
                            echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed'),
                                'confirmDelete',
                                array(
                                'style' => 'display: inline-block;',
                                ));
                            ?>
                        </div>
                    </div>
                </div>
        </div>

        <?= CHtml::hiddenField('idst', (is_array($idst) ? implode(',', $idst) : $idst)) ?>
        <?= CHtml::hiddenField('groups', (is_array($groups) ? implode(',', $groups) : $groups)) ?>
        <?= CHtml::hiddenField('assign_type', $assignType) ?>

        <div class="form-actions">
            <?= CHtml::submitButton(Yii::t('standard', '_CONFIRM'), array(
                'class' => 'btn btn-docebo green big hidden',)); ?>

            <?= CHtml::button(Yii::t('standard', '_CLOSE'), array('class' => 'btn btn-docebo black big close-dialog')); ?>
        </div>

        <?php echo CHtml::endForm(); ?>
    </div>

    <script type="text/javascript">
        $().ready(function () {
            $("#remove-user").find("ul").addClass("inline").css('display','inline-block');
            $("<span style='margin-left: 7px;'>&raquo;</span>").appendTo($("a.ajaxLinkNode"));
        });
        $("input").styler();

    </script>
<?php endif; ?>