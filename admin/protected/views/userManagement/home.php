<?php
	$this->breadcrumbs = array(
		Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
		Yii::t('menu', '_USER_MANAGMENT'),
	);
	$usermanContainerId			= "userman-container";
	$usersGridId 				= "userman-users-management-grid";
	$searchFormId				= "userman-search-form";
	$orgchartFancyTreeId		= "userman-orgchart-tree";
	$selectedUsersFieldName 	= "userman_selected_users";
	$fancytreeOrgchartWrapperId	= "fancytree-orgchart-wrapper";
?>


<div id="<?= $usermanContainerId ?>">

	<?php $this->renderPartial('//common/_mainActionsV2'); ?>

	<!--[if IE & (lt IE 9)]>
		<script src="<?=$assetsPath?>/charts/r2d3.js" charset="utf-8"></script>
	<![endif]-->

	<!--[if IE & (gte IE 9)]>
		<script src="<?=$assetsPath?>/charts/d3.v3.min.js" charset="utf-8"></script>
		<script src="<?=$assetsPath?>/charts/aight.d3.min.js" charset="utf-8"></script>
	<![endif]-->
	<!--[if !IE]> -->
		<script src="<?=$assetsPath?>/charts/d3.v3.min.js" charset="utf-8"></script>
		<script src="<?=$assetsPath?>/charts/aight.d3.min.js" charset="utf-8"></script>
	<!-- <![endif]-->


	<?php
		$form = $this->beginWidget('CActiveForm', array(
			'id' => $searchFormId,
			'htmlOptions' => array(
        		'class' => 'form-inline',
    		),
		));
	?>

	<div class="main-section">

		<div class="filters-wrapper">
			<table class="filters">
				<tbody>
					<tr>
						<td class="table-padding">
							<table class="table-border">
								<tr>
									<td class="group userman-include-descendants">
										<table>
											<tr>
												<td><?= Yii::t('admin_directory', '_DIRECTORY_FILTER_FLATMODE') ?></td>
												<td>&nbsp;&nbsp;<?= $form->checkBox($userModel, 'containChildren', array('value' => 1)) ?></td>
											</tr>
										</table>
									</td>
									<td class="group userman-select-users-type">
										<table>
											<tr>
												<?php
													echo $form->radioButtonList($userModel, 'valid', array(
														'' => Yii::t('standard', '_ALL'),
														CoreUser::STATUS_VALID => Yii::t('standard', '_ACTIVE'),
														CoreUser::STATUS_NOTVALID => Yii::t('standard', '_SUSPENDED'),
													),
													array(
														'template' => "<td>{input} {label}</td>",
														'container' => '',
														'separator' => ''
													));
												?>
											</tr>
										</table>
									</td>
									<td class="group userman-advanced-search-box">
										<table>
											<tr>
												<td>

													<div class="search-input-wrapper">
														<?php
															$params = array();
															$params['max_number'] = 10;
															$params['autocomplete'] = 1;
															$autoCompleteUrl = Docebo::createAdminUrl($userManAction, $params);
															$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
																'id' 				=> 'userman-search-input',
																'name' 				=> 'CoreUser[search_input]',
																'value' 			=> '',
																'options' => array(
																	'minLength' => 1,
																),
																'source' => $autoCompleteUrl,
																'htmlOptions' => array('class' => 'search-query', 'placeholder' => Yii::t('standard', '_SEARCH')),
															));
														?>
														<button type="button" class="close clear-search">&times;</button>
														<span 	class="perform-search search-icon"></span>
													</div>


												</td>
												<td>
													<a href="javascript:void(0);" class="userman-advanced-search-link"><?php echo Yii::t('standard', '_ADVANCED_SEARCH'); ?></a>
												</td>
											</tr>
										</table>
									</td>
									<td class="group picker">
										<a 	href="javascript:void(0);"
									 		data-content='<?php $this->renderPartial('_filterColumnsV2', array('columns' => $filterColumns)); ?>'
									 		class="popover-trigger popover-destroy userman-select-columns">picker</a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>

						<td colspan="4" class="">

							<div id="userman-advanced-search" style="display: none;">
								<div class="box-title">
									<h4><?php echo Yii::t('standard', '_ADVANCED_SEARCH'); ?></h4>
								</div>

								<div class="filter-row-top clearfix">
									<label for="add-filter-select"><?php echo Yii::t('standard', '_NEW_FILTER');?></label><select name="add-filter-select" id="add-filter-select">
										<option value="" class="selector"><?php echo Yii::t('standard', '_ADD'); ?></option>
										<option value="userid"><?php echo Yii::t('standard', '_USERNAME'); ?></option>
										<option value="email"><?php echo Yii::t('standard', '_EMAIL'); ?></option>
										<option value="firstname"><?php echo Yii::t('standard', '_FIRSTNAME'); ?></option>
										<option value="lastname"><?php echo Yii::t('standard', '_LASTNAME'); ?></option>

										<?php foreach ($additionalFields as $field): ?>
											<option class="additional" value="additional-<?php echo $field->idField; ?>" data-field-id="<?php echo $field->idField; ?>" data-field-type="<?php echo $field->type_field; ?>">
												<?php echo $field->translation; ?>
											</option>
										<?php endforeach; ?>
									</select>
								</div>

								<div id="userman-advanced-search-filters"></div>


								<div class="row-fluid">
									<div class="advanced-filter-logical-cond span10">
										<label class="radio" for="condition-and"><?php echo Yii::t('standard', '_FILTER_ALL_CONDS'); ?>
											<input name="advancedSearch[condition]" type="radio" value="and" id="condition-and" checked="checked">&nbsp;
										</label>
										<label class="radio"  for="condition-or"><?php echo Yii::t('standard', '_FILTER_ONE_COND'); ?>
											<input name="advancedSearch[condition]" type="radio" value="or" id="condition-or">&nbsp;
										</label>
									</div>
									<div class="span2">
										<span class="btn-search"></span>
									</div>
								</div>
								<input name="advancedSearch[visible]" type="hidden" value="0" id="advancedSearchVisibility">
							</div>


						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>


	<!-- Additional fields repo -->
	<div class="additional-fields-elements" style="display: none;">
		<?php foreach ($additionalFields as $field): ?>
			<div class="additional-<?php echo $field->idField; ?>">
				<?php echo $field->renderFilter(); ?>
			</div>
		<?php endforeach; ?>
	</div>


	<?php
		echo CHtml::hiddenField($selectedUsersFieldName, '');
		$this->endWidget();
	?>

	<div id="userman-tree-breadcrumbs"></div>


	<div id="<?= $fancytreeOrgchartWrapperId ?>"></div>
	<div class="userman-tree-show-hide hide-tree"><span></span> <?php echo Yii::t('standard', '_COLLAPSE'); ?></div>


	<div class="selections clearfix">

		<div class="left-selections clearfix">
			<p><?= Yii::t('standard', 'You have selected') ?> <strong><span id="userman-users-selected-count">0</span> <?= Yii::t('standard', 'items') ?></strong>.</p>
			<a href="javascript:void(0);" class="userman-select-all"> <?= Yii::t('standard', '_SELECT_ALL') ?> </a>
			<a href="javascript:void(0);" class="userman-deselect-all"> <?= Yii::t('standard', '_UNSELECT_ALL') ?> </a>
		</div>


		<?php if ($permissions['mod'] || ($permissions['del'])): ?>
			<div class="right-selections clearfix">
				<select name="massive_action" id="userman-users-massive-action">
					<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
					<?php if ($permissions['mod']): ?>
						<option value="activate"><?php echo Yii::t('standard', '_ACTIVATE'); ?></option>
						<option value="deactivate"><?php echo Yii::t('standard', '_DEACTIVATE'); ?></option>
					<?php endif; ?>
					<?php if ($permissions['mod']): ?>
						<option value="edit"><?php echo Yii::t('standard', '_MOD'); ?></option>
					<?php endif; ?>
					<?php if ($permissions['del']): ?>
						<option value="delete"><?php echo Yii::t('standard', '_DEL'); ?></option>
					<?php endif; ?>
				</select>
				<?php
					echo CHtml::link('<span></span>'.Yii::t('standard', '_MOD'), Yii::app()->createUrl($this->id.'/updateUser'), array(
						'id' => 'user-massive-update',
						'class' => 'popup-handler',
						'data-modal-title' => Yii::t('standard', '_MOD'),//'Edit',
						'data-modal-class' => 'user-edit-massive',
						'data-before-send' => 'massiveUserEditBeforeSend(data);',
						'data-after-send' => 'massiveUserEditAfterSend(data);',
						'style' => 'display:none;',
					));
				?>
				<label for="userman-users-massive-action"><?php echo Yii::t('standard', '_ON_SELECTED'); ?></label>
			</div>
		<?php endif; ?>
	</div>

	<div class="bottom-section clearfix">
		<?php
			$this->renderPartial('_usersGrid', array(
				'usersGridId' 		=> $usersGridId,
				'userModel'			=> $userModel,
				'selectedColumns'	=> $selectedColumns
			));
		?>
	</div>
</div>



<?php
	if ($showHints) {
		$this->widget('OverlayHints', array('hints'=>'bootstroEnrollUsers'));
	}
?>


<script type="text/javascript">

var userManOptions = {
		containerId					: '<?= $usermanContainerId ?>',
		usersGridId					: '<?= $usersGridId ?>',
		searchFormId				: '<?= $searchFormId ?>',
		orgchartFancyTreeId			: '<?= $orgchartFancyTreeId ?>',
		hiddenGridColumns			: [<?php echo implode(',', $hiddenColumns) ?>],
		selectedUsersFieldName  	: '<?= $selectedUsersFieldName ?>',
		fancytreeOrgchartWrapperId	: '<?= $fancytreeOrgchartWrapperId ?>',
		allowMoveNode				: <?= Yii::app()->user->isGodAdmin ? 'true' : 'false' ?>,
		userManActionUrl			: '<?= Docebo::createAdminUrl($userManAction) ?>'
};

var UserMan = new AdminUserManagement(userManOptions);

	$(function () {
		$('body').addClass('users-management-body');
		// Initial Loading Orgchart tree
		UserMan.loadOrgChartTree();
	});



</script>
