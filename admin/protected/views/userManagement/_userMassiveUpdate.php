<div class="form">

	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'user_form',
		'action' => Docebo::createAdminUrl('UserManagement/updateUserMassive')
	)); ?>

	<?php /*echo $form->errorSummary($user); */?>

	<div class="clearfix">
        <div class="users-count"><?php echo Yii::t('user_management', 'Selected users: <span>{selected_users}</span>', array('{selected_users}' => count($users))); ?></div>
		<div class="select-fields-title"><?php echo Yii::t('user_management', 'Select the field(s) you want to edit'); ?></div>
	</div>

    <?php echo $form->error($user, 'firstname'); ?>
    <div class="clear-line"></div>
	<div class="clearfix">
		<?php echo CHtml::checkBox('userFields[firstname]', isset($_POST['userFields']['firstname']), array(
			'class' => 'checker',
			'box-content' => 'firstname',
		)); ?>

		<?php if (!isset($_POST['userFields']['firstname'])) {
			$htmlOptions = array('disabled' => 'disabled');
			$disabled = 'disabled ';
		} else {
			$htmlOptions = array();
			$disabled = '';
		} ?>
		<div class="firstname <?php echo $disabled; ?>clearfix">
			<?php echo $form->textField($user, 'firstname', $htmlOptions); ?>
            <?php echo $form->labelEx($user, 'firstname'); ?>
		</div>
	</div>

    <?php echo $form->error($user, 'lastname'); ?>
    <div class="clear-line"></div>
	<div class="clearfix">
		<?php echo CHtml::checkBox('userFields[lastname]', isset($_POST['userFields']['lastname']), array(
			'class' => 'checker',
			'box-content' => 'lastname',
		)); ?>

		<?php if (!isset($_POST['userFields']['lastname'])) {
			$htmlOptions = array('disabled' => 'disabled');
			$disabled = 'disabled ';
		} else {
			$htmlOptions = array();
			$disabled = '';
		} ?>
		<div class="lastname <?php echo $disabled; ?>clearfix">
			<?php echo $form->textField($user, 'lastname', $htmlOptions); ?>
            <?php echo $form->labelEx($user, 'lastname'); ?>
		</div>
	</div>

    <?php echo $form->error($user, 'email'); ?>
    <div class="clear-line"></div>
	<div class="clearfix">
		<?php echo CHtml::checkBox('userFields[email]', isset($_POST['userFields']['email']), array(
			'class' => 'checker',
			'box-content' => 'email',
		)); ?>

		<?php if (!isset($_POST['userFields']['email'])) {
			$htmlOptions = array('disabled' => 'disabled');
			$disabled = 'disabled ';
		} else {
			$htmlOptions = array();
			$disabled = '';
		} ?>
		<div class="email <?php echo $disabled; ?>clearfix">
			<?php echo $form->textField($user, 'email', $htmlOptions); ?>
            <?php echo $form->labelEx($user, 'email'); ?>
		</div>
	</div>

    <?php echo $form->error($user, 'new_password'); ?>
    <div class="clear-line"></div>
	<div class="clearfix">
		<?php echo CHtml::checkBox('userFields[new_password]', isset($_POST['userFields']['new_password']), array(
			'class' => 'checker',
			'box-content' => 'password',
		)); ?>

		<?php if (!isset($_POST['userFields']['new_password'])) {
			$htmlOptions = array('disabled' => 'disabled');
			$disabled = 'disabled ';
		} else {
			$htmlOptions = array();
			$disabled = '';
		} ?>
		<div class="password <?php echo $disabled; ?>clearfix">
			<?php echo $form->passwordField($user, 'new_password', $htmlOptions); ?>
            <?php echo $form->labelEx($user, 'new_password'); ?>
		</div>
	</div>

    <?php echo $form->error($user, 'new_password_repeat'); ?>
    <div class="clear-line"></div>
	<div class="clearfix">
		<div class="password <?php echo $disabled; ?>clearfix">
			<?php echo $form->passwordField($user, 'new_password_repeat', $htmlOptions); ?>
            <?php echo $form->labelEx($user, 'new_password_repeat'); ?>
		</div>
	</div>

    <?php echo $form->error($groupMember, 'idst'); ?>
    <div class="clear-line"></div>
	<div class="clearfix">
		<?php
			$isGodAdmin = Yii::app()->user->getIsGodadmin();
			echo CHtml::checkBox('groupFields[idst]', isset($_POST['groupFields']['idst']), array(
				'class' => 'checker',
				'box-content' => 'level',
				'disabled' => !$isGodAdmin
			));
		?>

		<?php if (!isset($_POST['groupFields']['idst'])) {
			$htmlOptions = array('disabled' => 'disabled');
			$disabled = 'disabled ';
		} else {
			$htmlOptions = array();
			$disabled = '';
		} ?>
		<div class="level <?php echo $disabled; ?>clearfix">
			<?php
				$levels = CoreGroup::getAdminLevels(true, !$isGodAdmin, $isGodAdmin);
				echo $form->dropDownList($groupMember, 'idst', $levels, $htmlOptions);
				echo CHtml::label($user->getAttributeLabel('level'), CHtml::activeId($groupMember, 'idst'));
			?>
		</div>
	</div>
    <div class="clear-line"></div>

	<div class="clearfix">
		<?php if (!isset($_POST['userFields']['email_status'])) {
			$htmlOptions = array('disabled' => 'disabled');
			$disabled = 'disabled ';
		} else {
			$htmlOptions = array();
			$disabled = '';
		}
		$choices = array(1 => Yii::t('standard', 'Verified'), 0 => Yii::t('standard', 'Unverified'));?>
		<?= CHtml::checkBox('userFields[email_status]', isset($_POST['userFields']['email_status']), array(
				'class' => 'checker',
				'box-content' => 'email_status',
		)); ?>
		<div class="email_status <?= $disabled; ?>clearfix">
			<?= $form->dropDownList($user, 'email_status', $choices, $htmlOptions); ?>
			<?= CHtml::label($user->getAttributeLabel('email_status'), CHtml::activeId($user, 'email_status')); ?>
		</div>
	</div>
	<div class="clear-line"></div>
	<?php
	if (isset($isActiveNotificationOfType) && $isActiveNotificationOfType === true) { ?>
		<div class="clearfix" style="display: none; margin-left: 20px" id="sendEmails">
			<?= CHtml::checkBox('sendEmails', false, array()); ?>
			<div class="send_emails clearfix"
				 style="display: inline-block;text-align: left; margin-left: 7%;width: 86%">
				<?= CHtml::label(Yii::t('standard', 'Send verification email to selected users'), 'sendEmails', array('style' => 'float: left;cursor:pointer')); ?>
			</div>
		</div>
		<div class="clear-line"></div>
	<?php } ?>

	<?php
	/*foreach ($users as $idst) {
		echo CHtml::hiddenField('idst[]', $idst);
	}*/
	echo CHtml::hiddenField('idst-list', implode(',', array_values($users)), array('id' => 'idst-list-input'));
	?>

	<?php $this->endWidget(); ?>
</div>

<script type="text/javascript">
    (function ($) {
        $(function () {
            //Styler is applied by modal callback (themes/spt/js/scriptModal.js
            //$('input, select').styler();
			$('#CoreUser_email_status').on('change', function () {
				var value = $(this).val();
				if (value == 0) {
					$('#sendEmails').show();
				} else {
					$('#sendEmails').hide();
				}
			});
			setTimeout(function(){
				$('#userFields_email_status-styler').click(function(){
					var isChecked = $(this).hasClass('checked');
					if(!isChecked){
						$('#sendEmails').hide();
					}else if($('#CoreUser_email_status').val() == 0){
						$('#sendEmails').show();
					}
				})
			}, 1000);

         });

    })(jQuery);
</script>

<div class="modal-footer">
    <div style="display: block; text-align: left; float: left; width: 50%">
        <?php if( !PluginManager::isPluginActive('WhitelabelApp') ) : ?>
            <?php $currentLanguage = Yii::app()->getLanguage(); ?>
            <?php ($currentLanguage == 'it' ? $it = 'it/' : $it = ''); ?>
            <?php ($currentLanguage != 'it' && $currentLanguage != 'en' ? $int = '-international' : $int = ''); ?>

            <p class="privacy-policy" style="font-size: 11px; font-weight: bold;">
                <?=Yii::t('standard', 'By clicking "Confirm", you are agreeing to the')?>
                <a style="color: #0465AC; text-decoration: underline;" href="https://www.docebo.com/<?=$it?>docebo-privacy-policy<?=$int?>/" target="_blank">
                    <?=Yii::t('standard', 'Privacy Policy')?>
                </a>
            </p>
        <?php endif; ?>
    </div>
	<input class="btn confirm-btn" type="button" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" name="submit" data-submit-form="user_form"/>
	<input class="btn close-btn" type="button" value="<?php echo Yii::t('standard', '_CLOSE'); ?>"/>
</div>
