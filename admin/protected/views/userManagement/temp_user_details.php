<? if ($model->userid): ?>
	<div class="detail">
		<div class="pull-left input-medium">
			<?=$model->getAttributeLabel('userid')?>
		</div>
		<?= $model->userid ?>
	</div>
<? endif; ?>

	<div class="clearfix"></div>

<? if ($model->email): ?>
	<div class="detail">
		<div class="pull-left input-medium">
			<?=$model->getAttributeLabel('email')?>
		</div>
		<?= $model->email ?>
	</div>
<? endif; ?>

	<div class="clearfix"></div>

<? if ($model->firstname || $model->lastname): ?>
	<div class="detail">
		<div class="pull-left input-medium">
			<?=Yii::t('standard', '_NAME')?>
		</div>
		<?
		if ($model->firstname && $model->lastname) {
			echo $model->firstname . ' ' . $model->lastname;
		} elseif ($model->firstname) {
			echo $model->firstname;
		} elseif ($model->lastname) {
			echo $model->lastname;
		}
		?>
	</div>
<? endif; ?>
<? if ($model->request_on && strtotime($model->request_on)): // valid time ?>
	<div class="detail">
		<div class="pull-left input-medium">
			<?=$model->getAttributeLabel('request_on')?>
		</div>
		<?
		echo $model->request_on;
		?>
	</div>
<? endif; ?>

<div class="detail">
	<div class="pull-left input-medium">
		<?=Yii::t('standard', 'branches')?>
	</div>
	<?=$branchPath?>
	<div class="clearfix"></div>
</div>

<? foreach($additionalFieldValues as $value): ?>
	<div class="detail">
		<div class="pull-left input-medium">
			<?=$value->getTranslation()?>
		</div>
		<?=$value->renderValue($value->userEntry);?>
		&nbsp;
	</div>
	<div class="clearfix"></div>
<? endforeach ?>