<style>
	#user-import-form-activation label {
		vertical-align: baseline;
		padding-right: 10px;
	}
	#user-import-form-activation{
		margin-top: 14px;
		margin-bottom: 24px;
	}
	.row-border > label {
		vertical-align: middle !important;
	}
	#checkForFirstRowHeader{
		width: 100%;
		text-align: left;
	}
	#explanationMsg{
		color: grey;
		display: inline-block;
		width: 23.5%;
		padding: 5px 0;
		margin-right: 10px;
	}
	@media (min-width: 980px) and (max-width: 1200px){
		#checkForFirstRowHeader{
			width: 100% !important;
		}
		#explanationMsg{
			width: 30%;
		}
	}
</style>
<?php
/**
 * Created by PhpStorm.
 * User: Asen Nikolov
 * Date: 04-Dec-15
 * Time: 9:29 AM
 */
?>

<div class="main-section import">
	<h3 class="title-bold"><?php echo Yii::t('standard', 'Activation/Deactivation'); ?></h3>

	<?php
	/* @var $form CActiveForm */
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'user-import-form-activation',
		'htmlOptions' => array(
			'enctype' => 'multipart/form-data',
		),
	));
	?>
	<?php echo $form->errorSummary($model); ?>
	<input type="hidden" value="1" name="fromActivationTab" />
	<div class="row has-border">
		<div class="row-border clearfix">
			<?php echo $form->labelEx($model, 'file'); ?>
			<?php echo $form->fileField($model, 'file'); ?>
			<div>
				<span class="no-file"></span>
				<span class="file-name"></span>

				<div>(Max. <?php echo $model->getMaxFileSize('MB'); ?> Mb)</div>
			</div>
			<div class="download-file" style="margin-left: 0;vertical-align: top"><span><span
							class="download-icon"></span><?php echo CHtml::link(Yii::t('standard', '_DOWNLOAD'), Yii::app()->createUrl($this->id . '/importUser', array('download' => 'sample_activation'))); ?></span> <?php echo Yii::t('standard', '_A_SAMPLE_CSV_FILE'); ?>
			</div>
			<br/>
			<div class="clearfix" id="checkForFirstRowHeader">
				<div>
					<span id="explanationMsg">
						<?=Yii::t('user_management', 'Activate/deactivate users by uploading a CSV file (single column, one username per row)')?>
					</span>
					<?php echo $form->checkBox($model, 'firstRowAsHeader'); ?>&nbsp;
					<?php echo $form->labelEx($model, 'firstRowAsHeader'); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'charset'); ?>
		<?php echo $form->dropDownList($model, 'charset', UserImportForm::getDropDownCharsets()); ?>
	</div>

	<div class="row has-border">
		<div class="row-border clearfix">
			<?php echo $form->labelEx($model, 'activation');
				$model->activation = 'yes';
			?>
			<?php echo $form->radioButtonList($model, 'activation', array(
					'yes' => Yii::t('standard', 'Activation'),
					'no' => Yii::t('standard', 'Deactivation'),
				//'server' => '_CONFIG_BY_SERVER',
				//'nothing' => '_DO_NOTHING',
			), array(
					'separator' => '',
			)); ?>
		</div>
	</div>

	<div class="row btns">
		<?php echo CHtml::link(Yii::t('standard', '_CANCEL'), array('userManagement/index'), array('class' => 'btn close-btn')); ?>
		<?php echo CHtml::submitButton(Yii::t('standard', '_NEXT'), array('class' => 'btn confirm-btn')); ?>
	</div>

	<?php $this->endWidget(); ?>
</div>

<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input,select').styler({browseText: '<?=Yii::t('course_management', 'CHOOSE FILE')?>'});
		});
	})(jQuery);
</script>