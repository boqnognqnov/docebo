<div id="advanced-search" style="display: none;">
	<div class="clearfix">
		<h4><?php echo Yii::t('standard', '_ADVANCED_SEARCH'); ?></h4>
	</div>

	<div class="filter-row-top clearfix">
		<label for="select-filter"><?php echo Yii::t('standard', '_NEW_FILTER');?></label><select name="select-filter" id="select-filter">
			<option value="" class="selector"><?php echo Yii::t('standard', '_ADD'); ?></option>
			<option value="userid"><?php echo Yii::t('standard', '_USERNAME'); ?></option>
			<option value="email"><?php echo Yii::t('standard', '_EMAIL'); ?></option>
			<option value="firstname"><?php echo Yii::t('standard', '_FIRSTNAME'); ?></option>
			<option value="lastname"><?php echo Yii::t('standard', '_LASTNAME'); ?></option>

			<?php foreach ($additionalFields as $field): ?>
				<option class="additional" value="additional-<?php echo $field->idField; ?>" data-field-id="<?php echo $field->idField; ?>"><?php echo $field->getTranslation(); ?></option>
			<?php endforeach; ?>
		</select>
	</div>
	<div id="selected-filters">

	</div>
	<div id="conditions" class="clearfix">
		<input name="advancedSearch[condition]" type="radio" value="and" id="condition-and" checked="checked">
		<label for="condition-and"><?php echo Yii::t('standard', '_FILTER_ALL_CONDS'); ?></label>
		<input name="advancedSearch[condition]" type="radio" value="or" id="condition-or">
		<label for="condition-or"><?php echo Yii::t('standard', '_FILTER_ONE_COND'); ?></label>
		<input type="submit" class="btn-search">
	</div>
	<div class="elements" style="display: none;">
		<?php foreach ($additionalFields as $field): ?>
			<div class="additional-<?php echo $field->idField; ?>">
				<?php echo $field->renderFilter(); ?>
			</div>
		<?php endforeach; ?>
	</div>
	<input name="advancedSearch[visible]" type="hidden" value="0" id="advancedSearchVisibility">
</div>
