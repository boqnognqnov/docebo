<div class="userman-select-columns">
	<p class="description"><?php echo Yii::t('standard', '_CLICK_TO_SELECT_COLUMNS_TO_DISPLAY', array(6)); ?></p>
	<ul>
		<?php foreach ($columns as $type => $items): ?>
			<?php foreach ($items as $key => $column): ?>
				<li data-column="<?php echo $column['cssClass']; ?>" data-column-key="<?php echo $key; ?>" class="<?php echo $column['class']; ?>">
					<?php
					$column['title'] = CHtml::encode($column['title']); //encode any special symbols that might come from the DB
					//NOTE: field names which are too long will cause visualization problems, so we will truncate them (full name can still be viewed by a span title)
					if (strlen($column['title']) > 50) {
						echo  CHtml::tag('span', array('title' => $column['title']),  Docebo::ellipsis($column['title'], 47).'...');
					} else {
						echo $column['title'];
					}
					?>
				</li>
			<?php endforeach; ?>
		<?php endforeach; ?>
	</ul>
</div>
