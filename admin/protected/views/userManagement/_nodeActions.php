<div class="node-actions">
	<ul>
		<?php if (!$isRoot): ?>

		<li class="node-action">
			<?php $this->renderPartial('//common/_modal', array(
				'config' => array(
					'id' => 'assign' . $id,
					'class' => 'node-assign',
					'linkTitle' => Yii::t('standard', '_ASSIGN_USERS'),
					'modalTitle' => Yii::t('standard', '_ASSIGN_USERS'),
					'url' => 'orgManagement/assignUsers&id=' . $id,
					'buttons' => array(
						array(
							'type' => 'submit',
							'title' => Yii::t('standard', '_CONFIRM'),
						),
						array(
							'type' => 'cancel',
							'title' => Yii::t('standard', '_UNDO'),
						),
					),
					'afterSubmit' => 'updateUserContent',
				),
			)); ?>
		</li>

		<?php endif; ?>

		<?php if (Yii::app()->user->getIsGodadmin()): ?>

			<li class="node-action">
				<?php echo CHtml::link(Yii::t('standard', '_MOD'), Yii::app()->createUrl('orgManagement/edit', array('id' => $id)), array(
					'class' => 'node-edit new-node-handler',
					'data-modal-class' => 'editNode',
					'modal-title' => Yii::t('standard', '_MOD'),
				)); ?>
			</li>

			<?php if (!$isRoot): ?>

		        <?php
		        // Raise event to let plugins add menu items
				/**
				 * @devent docebo.event Raise an event when we render the orgchart tree node menu
				 * @source 1 1 Raise the event
				 */
		        Yii::app()->event->raise('RenderOrgChartTreeNodeMenu', new DEvent($this, array('id' => $id)));
		        ?>

				<li class="node-action">
					<?php echo CHtml::link(Yii::t('standard', '_MOVE'), Yii::app()->createUrl('orgManagement/move', array('id' => $id)), array(
						'class' => 'node-move new-node-handler',
						'data-modal-class' => 'move-node',
						'modal-title' => Yii::t('standard', '_MOVE'),
					)); ?>
				</li>

				<li class="node-action">
					<?php $this->renderPartial('//common/_modal', array(
						'config' => array(
							'class' => 'delete-node',
							'linkTitle' => Yii::t('standard', '_DEL'),
							'modalTitle' => Yii::t('standard', '_DEL'),
							'url' => 'orgManagement/delete&id=' . $id,
							'buttons' => array(
								array(
									'type' => 'submit',
									'title' => Yii::t('standard', '_CONFIRM'),
								),
								array(
									'type' => 'cancel',
									'title' => Yii::t('standard', '_UNDO'),
								),
							),
							'afterLoadingContent' => 'hideConfirmButton();',
							'afterSubmit' => 'updateOrgTreeContent',
						),
					)); ?>
				</li>

			<?php endif; ?>

		<?php endif; ?>

	</ul>
</div>