<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'user_form',
	));	?>

	<p><?php echo Yii::t('standard', '_CONFIRM').': "<strong>'.count($users).'</strong>" '.Yii::t('standard', '_USERS'); ?></p>

	<div class="clearfix">
		<?php echo $form->checkbox($model, 'confirm', array('id' => 'LearningCourseuser_confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo chtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed', array(count($users), '{username}' => Yii::app()->user->getRelativeUsername($users[0]->userid))), 'LearningCourseuser_confirm_'.$checkboxIdKey); ?>
		<?php echo $form->error($model, 'confirm'); ?>
	</div>

	<?php foreach ($users as $user) {
		echo CHtml::hiddenField('idst[]', $user->idst);
	}
	?>
	<?php $this->endWidget(); ?>
</div>