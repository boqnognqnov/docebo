<?=DoceboForm::beginForm() ?>

<?=DoceboForm::hiddenField('confirm', 1) ?>

	<?php echo Yii::t('user_management', '_RESEND_EMAIL_CONFIRM_TEXT', array('{userid}' => $user->userid)) ?>
<?=DoceboForm::endForm() ?>