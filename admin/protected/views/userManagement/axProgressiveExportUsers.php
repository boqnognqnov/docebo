<?php

echo CHtml::beginForm('', 'POST', array(
	'class' => 'ajax',
));

echo CHtml::hiddenField('type', $type);
echo CHtml::hiddenField('fileName', $fileName);
echo CHtml::hiddenField('exportType', $exportType);
echo CHtml::hiddenField('_key_', $cacheKey);

$downloadFileUrl = Docebo::createAbsoluteAdminUrl('userManagement/axProgressiveExportUsers', array(
	'downloadGo' => 1,
));

$progressClass = in_array($nextPhase, array('extract', 'convert')) ? 'progress-striped active' : '';

switch ($nextPhase) {
	case 'extract'  :
		$dialogTitle = '<span></span>' . Yii::t('standard', '_EXPORT') . '...';
		break;
	case 'convert'  :
		$dialogTitle = '<span></span>' . Yii::t('standard', 'Converting') . '...';
		break;
	case 'download' :
		$dialogTitle = '<span></span>' . Yii::t('standard', 'Export complete');
		break;
}

?>

<div id="export-user-progressive-container" class="export-user-progressive-container">
	<div class="row-fluid">
		<div class="span12 pull-left sub-title" style="font-size: 17px;">
			<div class="pull-left sub-title"><?= Yii::t('standard', '_EXPORT') ?>: <strong><?= $fileTitle ?></strong></div>
			<div class="pull-right percentage-gauge"><?= number_format($exportingPercent, 2) ?>%</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="docebo-progress progress progress-success <?= $progressClass ?>">
				<div style="width: <?= $exportingPercent ?>%;" class="bar full-height" id="uploader-progress-bar"></div>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12 action-undergoing">
			<?php if ($nextPhase == 'extract') : ?>
				<?= Yii::t('standard', 'Exporting {x} out of {y}', array('{x}' => $exportingNumber, '{y}' => $totalNumber)) ?>
			<?php elseif ($nextPhase == 'convert') : ?>
				<?= Yii::t('standard', 'Now converting your report') ?> ...
			<?php elseif ($nextPhase == 'download') : ?>
				<div class="download-note"><?= Yii::t('standard', 'You can now download your file') ?></div>
				<?php if ($forceCsvMaxRows) : ?>
					<div class="forced-csv">
						<?= Yii::t('standard', 'Export is over the limit') ?>
					</div>
				<?php endif; ?>

				<?php if ($forceCsvMissingGnumeric) : ?>
					<div class="forced-csv">
						<?= Yii::t('standard', 'Conversion utility missing in this installation. Format changed to CSV') ?>!
					</div>
				<?php endif; ?>


			<?php endif; ?>
		</div>
	</div>
	<?php if ($stop): ?>
		<div class="form-actions">
			<?= CHtml::submitButton(Yii::t('standard', '_DOWNLOAD'), array(
				'class' => 'btn-docebo green big download-file',
				'name' => 'download',
			));
			?>
			<input class="btn btn-docebo black big close-dialog" type="submit"
						 value=<?= Yii::t('standard', '_CLOSE') ?>>
		</div>
	<?php else: ?>
		<div class="form-actions">
			<?php
			echo CHtml::button(Yii::t('standard', '_CANCEL'), array(
				'class' => 'btn-docebo black big close-dialog'));
			?>
		</div>
	<?php endif; ?>
</div>
<?= CHtml::endForm() ?>

<script type="text/javascript">

	$(function () {
		var $modal = $('#users-export-csv-dialog, #users-export-xls-dialog').parents('.modal');
		// Set dialog title, depending on export phase we are in
		var dialogTitle = '<?= $dialogTitle ?>';
		$modal.find('.modal-header h3').html(dialogTitle);

		// On DOWNLOAD click
		$('input[name="download"]').on('click', function (e) {
			e.preventDefault();
			$modal.find(".modal-body").dialog2("close");
			var url = '<?= $downloadFileUrl ?>';
			window.open(url);
		});

	});

</script>