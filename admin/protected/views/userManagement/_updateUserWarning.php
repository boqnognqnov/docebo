<div class="clearfix row-fluid">
	<div class="new-user-issue span12">
		<div class="register-message-icon">
	        <span>
				<i class="fa  fa-2x fa-warning"></i>
			</span>
		</div>
		<?php echo Yii::t('user_management', 'The user was not created in the selected branch, because the branch was modified', array(
		)); ?>
	</div>
</div>