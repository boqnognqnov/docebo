<?php echo CHtml::beginForm('', 'POST', array(
	'class'=>'ajax',
)); ?>

<div id="unassign-user-progressive-container" class="unassign-user-progressive-container">
	<div class="row-fluid">
		<div class="span12">
			<div class="pull-left sub-title" >
				<b>
                    <?= Yii::t('standard', 'Unassigning {x} out of {y}', array(
                        '{x}' => $processedUsers,
                        '{y}' => $totalUsers))
                    ?>
                </b>
			</div>
			<div class="pull-right percentage-gauge"><?=$completedPercent?>%</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="docebo-progress progress progress-success">
				<div style="width: <?=$completedPercent?>%;" class="bar full-height" id="uploader-progress-bar"></div>
			</div>
		</div>
	</div>
    <?php if($stop): ?>
        <div class="form-actions">
            <input class="btn btn-docebo black big close-dialog" type="submit" value=<?= Yii::t('standard', '_CLOSE') ?>>
        </div>

        <script type="text/javascript">
            $(function(){
               var gridId = "userman-users-management-grid";
                $.fn.yiiGridView.update(gridId);
                $("a.userman-deselect-all").trigger("click");
            });
        </script>
    <?php else: ?>
        <div class="form-actions">
            <?php
            echo CHtml::button(Yii::t('standard', '_CANCEL'), array(
                'class' => 'btn-docebo black big close-dialog'));
            ?>
        </div>
    <?php endif; ?>
</div>

<?= CHtml::endForm() ?>