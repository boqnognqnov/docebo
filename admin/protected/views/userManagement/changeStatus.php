<?php
/**
 * @var $countIltSessions int
 * @var $countWebinarSessions int
 */
?>

<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'user_form',
	));	?>

        <?php
            if ($countUsers > 1):               

                if($status != CoreUser::STATUS_VALID):                    
                    if($countWebinarSessions > 0 || $countIltSessions > 0): ?>
                        <div class="row-fluid">
                            <p>
                                <?= Yii::t('classroom', 'Some users are enrolled in future sessions') ?>
                            </p>
                        </div>
                        <div class="row-fluid">

                            <?= CHtml::radioButtonList('unenroll_users_from_sessions', 0, array(
                                0 => Yii::t('user', 'Keep users enrolled'),
                                1 => Yii::t('user', 'Unenroll users')
                            ), array(
                                'separator' => '&nbsp;&nbsp;&nbsp;',
                                'labelOptions' => array('style' => 'display: inline-block;'),
                            )) ?>

                        </div>
                    <?php   endif;
                endif;





                if ($status != CoreUser::STATUS_VALID)
                    $msg = "<p>" . Yii::t('user_management', 'Are you sure you want to Deactivate "{users} Users"?', array('{users}' => $countUsers)) . "</p>";
                else
                    $msg = "<p>" . Yii::t('user_management', 'Are you sure you want to Activate "{users} Users"?', array('{users}' => $countUsers)) . "</p>";
            else:

                if($status != CoreUser::STATUS_VALID):

                    if($countIltSessions > 0 || $countWebinarSessions > 0):
                ?>

                        <div class="row-fluid">
                            <p class="change-status-user-data"><?= Yii::t('user', 'User "{name}" is enrolled in:', array(
                                    '{name}' => $username
                                )) ?></p>
                        </div>
                <?php

                            if($countIltSessions > 0): ?>
                        <div class="row-fluid">
                                <p>
                                    <?= Yii::t('classroom', '<b>{count} ILT-Classroom</b> future sessions', array(
                                        '{count}' => $countIltSessions
                                    )) ?>
                                </p>
                        </div>
                        <div class="row-fluid">

                                <?= CHtml::radioButtonList('unenroll_ilt_sessions', 0, array(
                                    0 => Yii::t('user', 'Keep user enrolled'),
                                    1 => Yii::t('user', 'Unenroll user')
                                ), array(
                                    'separator' => '&nbsp;&nbsp;&nbsp;',
                                    'labelOptions' => array('style' => 'display: inline-block;'),
                                )) ?>

                        </div>
                    <?php    endif;
                            if($countWebinarSessions > 0): ?>
                                <div class="row-fluid">
                                    <p>
                                        <?= Yii::t('classroom', '<b>{count} Webinar</b> future sessions', array(
                                            '{count}' => $countWebinarSessions
                                        )) ?>
                                    </p>
                                </div>
                                <div class="row-fluid">

                                    <?= CHtml::radioButtonList('unenroll_webinar_sessions', 0, array(
                                        0 => Yii::t('user', 'Keep user enrolled'),
                                        1 => Yii::t('user', 'Unenroll user')
                                    ), array(
                                        'separator' => '&nbsp;&nbsp;&nbsp;',
                                        'labelOptions' => array('style' => 'display: inline-block;'),
                                    )) ?>

                                </div>
                            <?php    endif;
                            ?>

            <?php
                    endif;
                else:

                endif;


                if ($status != CoreUser::STATUS_VALID)
                    $msg = "<p>" . Yii::t('user_management', 'Are you sure you want to Deactivate user "{username}"?', array('{username}' => $username)) . "</p>";
                else
                    $msg = "<p>" . Yii::t('user_management', 'Are you sure you want to Activate user "{username}"?', array('{username}' => $username)) . "</p>";
            endif;
            echo $msg;
        ?>

	<div class="clearfix">
		<?php echo $form->checkbox($model, 'confirm', array('id' => 'LearningCourseuser_confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed'), 'LearningCourseuser_confirm_'.$checkboxIdKey); ?>
		<?php echo $form->error($model, 'confirm'); ?>
	</div>

	<?php echo CHtml::hiddenField('idst', $idst); ?>
	<?php $this->endWidget(); ?>
</div>