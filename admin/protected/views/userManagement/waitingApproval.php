<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('menu', '_USER_MANAGMENT') => array('userManagement/index'),
	Yii::t('course', '_USERWAITING'),
); ?>

<?php $this->renderPartial('/common/_mainActions', array(
	'usersWaitingForApproval' => true
)); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'user-management-form',
	'htmlOptions' => array('class' => 'ajax-grid-form', 'data-grid' => '#users-management-grid'),
)); ?>
<div class="main-section waiting-approval">

	<div class="filters-wrapper">
        <div class="filters clearfix">
            <div class="input-wrapper">
				<?php
					$params = array();
					$params['max_number'] = 10;
					$params['autocomplete'] = 1;
					$autoCompleteUrl = Docebo::createAdminUrl('userManagement/waitingApproval', $params);
					$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
						'id' 				=> 'userman-search-input',
						'name' 				=> 'CoreUserTemp[search_input]',
						'value' 			=> '',
						'options' => array(
							'minLength' => 1,
						),
						'source' => $autoCompleteUrl,
						'htmlOptions' => array('class' => 'search-query', 'placeholder' => Yii::t('standard', '_SEARCH')),
					));
				?>
				<button type="button" class="close clear-search">&times;</button>
				<span 	class="perform-search search-icon"></span>
            </div>
        </div>
	</div>
</div>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<div class="selections clearfix">
	<?php $this->widget('ext.local.widgets.GridViewSelectAll', array(
		'gridId' => 'users-management-grid',
		'class' => 'left-selections clearfix',
		'dataProvider' => $model->dataProvider(),
		'itemValue' => 'idst',
	)); ?>
	<div class="right-selections clearfix">

		<select name="massive_action" id="waiting-users-massive-action">
			<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
			<option value="confirm"><?php echo Yii::t('standard', '_CONFIRM'); ?></option>
			<option value="decline"><?php echo Yii::t('admin_directory', '_REFUSE_USER'); ?></option>
		</select>
	</div>
</div>

<div class="bottom-section border-bottom-section clearfix">

	<?php
	$selectedColumns[] = array(
		'name' => '',
		'type' => 'raw',
		'value' => 'CHtml::link("", "", array(
				"class" => "ajaxModal sendMessage",
				"data-toggle" => "modal",
				"data-modal-class" => "resend-email",
				"data-modal-title" => "'.Yii::t('standard', '_CONFIRM').'",
				"data-buttons" => \'[
					{"type": "submit", "title": "'.Yii::t('standard', '_CONFIRM').'"},
					{"type": "cancel", "title": "'.Yii::t('standard', '_CANCEL').'"}
				]\',
				"data-url" => "userManagement/tempUserSendMessage&idst=$data->idst",
				"data-after-submit" => "updateUsersOrgChartTree",
		))',
		'htmlOptions' => array('class' => 'button-column-single')
	);
	$selectedColumns[] = array(
		'name' => '',
		'type' => 'raw',
		'value' => 'CHtml::link("", "", array(
				"class" => "ajaxModal details",
				"data-toggle" => "modal",
				"data-modal-class" => "tmp-user-details",
				"data-modal-title" => "'.Yii::t('standard', 'User Details').'",
				"data-buttons" => \'[]\',
				"data-url" => "userManagement/tempUserDetails&idst=$data->idst",
				"data-after-submit" => "updateUsersOrgChartTree",
		))',
		'htmlOptions' => array('class' => 'button-column-single')
	);
	?>

<?php

	$selectedColumns[] = array(
		'name' => '',
		'type' => 'raw',
		'value' => 'CHtml::link("", "", array(
				"class" => "ajaxModal confirm",
				"data-toggle" => "modal",
				"data-modal-class" => "delete-node",
				"data-modal-title" => "'.Yii::t('standard', '_CONFIRM').'",
				"data-buttons" => \'[
					{"type": "submit", "title": "'.Yii::t('standard', '_CONFIRM').'"},
					{"type": "cancel", "title": "'.Yii::t('standard', '_CANCEL').'"}
				]\',
				"data-url" => "userManagement/confirmUser&idst=$data->idst",
				"data-after-loading-content" => "hideConfirmButton()",
				"data-after-submit" => "updateUserContent",
		))',
		'htmlOptions' => array('class' => 'button-column-single')
	);
	$selectedColumns[] = array(
		'name' => '',
		'type' => 'raw',
		'value' => 'CHtml::link("", "", array(
				"class" => "ajaxModal delete-action",
				"data-toggle" => "modal",
				"data-modal-class" => "delete-node",
				"data-modal-title" => "'.Yii::t('standard', '_DEL').'",
				"data-buttons" => \'[
					{"type": "submit", "title": "'.Yii::t('standard', '_CONFIRM').'"},
					{"type": "cancel", "title": "'.Yii::t('standard', '_CANCEL').'"}
				]\',
				"data-url" => "userManagement/deleteTempUser&idst=$data->idst",
				"data-after-loading-content" => "hideConfirmButton();",
				"data-after-submit" => "updateUserContent",
		));',
		'visible' => 'Yii::app()->user->id != $data->idst',
		'htmlOptions' => array('class' => 'button-column-single')
	);
	?>

<div id="grid-wrapper">
<!--<div id="grid-wrapper" class="waiting-approval-table">-->
	<?php $this->widget('DoceboCGridView', array(
		'id' => 'users-management-grid',
		'htmlOptions' => array('class' => 'grid-view clearfix'),
		'dataProvider' => $model->dataProvider(),
		'columns' => array_merge(array(
				array(
					'class' => 'CCheckBoxColumn',
					'selectableRows' => 2,
					'id' => 'users-management-grid-checkboxes',
					'value' => '$data->idst',
					'checked' => 'in_array($data->idst, Yii::app()->session[\'selectedItems\'])',
				),
				array(
					'name' => 'userid',
					'value' => 'Yii::app()->user->getRelativeUsername($data->userid);',
					'type' => 'raw',
				),
				'firstname',
				'lastname',
				'email',
				array(
					'name' => 'confirmed',
					'type' => 'raw',
					'value' => '$data->confirmed == CoreUserTemp::STATUS_CONFIRMED ? "<span class=\'confirmed\'></span>" : "<span class=\'unconfirmed\'></span>";',
                     'headerHtmlOptions' => array(
                      'class' => 'confirmed-column',
                     ),
                     'htmlOptions' => array(
                      'class' => 'confirmed-column',
                     ),
				),
				array(
					'name' => 'request_on',
					'value' => '$data->request_on',
				),
			),
			$selectedColumns),
		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'ajaxUpdate' => 'users-management-grid-all-items',
		'beforeAjaxUpdate' => 'function(id, options) {
				options.type = "POST";
				if (options.data == undefined) {
					options.data = {
						selectedItems: $(\'#\'+id+\'-selected-items-list\').val()
					}
				}
			}',
		'afterAjaxUpdate' => 'function(id, data){
			$("#"+id+" input").styler();

			afterGridViewUpdate(id);

			$(\'.popover-action\').popover({
				placement: \'bottom\',
				html: true
			});
		}',
		'pager' => array(
			'class' => 'DoceboCLinkPager',
			'maxButtonCount' => 8,
		),
	)); ?>
</div>
</div>

<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input, select').styler();
			replacePlaceholder();
		});
	})(jQuery);

	$('.perform-search.search-icon').click(function(e){
		$('#users-management-grid').yiiGridView('update', {
			data: $('#user-management-form').serialize()
		});
	});

	$('#userman-search-input').on('autocompleteselect', function(e){
		$('#users-management-grid').yiiGridView('update', {
			data: $('#user-management-form').serialize()
		});
	});

	$('.close.clear-search').click(function(e){
		$('#userman-search-input').val('');
		$('#users-management-grid').yiiGridView('update', {
			data: $('#user-management-form').serialize()
		});
	});

</script>
