<?
// A bit of a copy-paste and trimmed version of admin/protected/views/userManagement/_userForm.php
?>
<div class="new-user-tabs">

	<ul class="nav nav-tabs" id="userFormTab">
		<!-- HIDE IF NO BRANCHES -->
		<?php if (!empty($orgChartFancyTreeData)) : ?>
			<li class="active">
				<a data-target="#userForm-orgchat" href="#"><span class="user-form-orgchat"></span><?php echo Yii::t('standard', 'branches'); ?></a>
			</li>
		<?php endif;?>


	</ul>

	<div class="form new-user-form clearfix">
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'user_form',
			'action' => Docebo::createAbsoluteLmsUrl('progress/run', array('type' => Progress::JOBTYPE_ASSIGN_USERS_TO_BRANCH, 'format' => 'json')),
			'htmlOptions' => array(
				'enctype' => 'multipart/form-data',
				'class'=>'ajax',
			),
		)); ?>

		<?=CHtml::hiddenField('idst', implode(',', $users))?>
		<?=CHtml::hiddenField('assign_type', $assignType)?>

		<div class="tab-content" id="userFormTabContent">
			<div class="tab-pane active" id="userForm-orgchat">
				<?php if (!empty($orgChartFancyTreeData)) : ?>

					<?php
					// Show Org Charts using Fancytree
					$id = 'orgcharts-fancytree'.time();
					$this->widget('common.extensions.fancytree.FancyTree', array(
						'id' => $id,
						'treeData' => $orgChartFancyTreeData,
						'preSelected' => $orgChartFancyTreePreselected,
						'selectMode' => 2,
						'formFieldNames' => array(
							'selectedInputName'	 	=> 'select-orgchart',
							'activeInputName' 		=> 'orgchart_active_node',
							'selectModeInputName'	=> 'orgchart_select_mode'
						),
					));
					?>

				<?php endif; ?>
			</div>

		</div>
		<?php $this->endWidget(); ?>
	</div>
</div>