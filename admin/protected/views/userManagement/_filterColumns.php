<div class="select-columns">
	<p class="description"><?php echo Yii::t('standard', '_CLICK_TO_SELECT_COLUMNS_TO_DISPLAY', array(6)); ?></p>
	<ul>
		<?php foreach ($columns as $type => $items): ?>
			<?php foreach ($items as $key => $column): ?>
				<li data-column="<?php echo $column['cssClass']; ?>" data-column-key="<?php echo $key; ?>" class="<?php echo $column['class']; ?>"><?php echo $column['title']; ?></li>
			<?php endforeach; ?>
		<?php endforeach; ?>
	</ul>
</div>
