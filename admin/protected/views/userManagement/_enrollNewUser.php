<div class="clearfix row-fluid">
	<?php if($branchError): ?>
    <div class="new-user-issue span3">
	    <div class="register-message-icon">
	        <span>
				<i class="fa  fa-2x fa-warning"></i>
			</span>
	    </div>
		<?php echo Yii::t('user_management', 'The user was not created in the selected branch, because the branch was modified', array(
			'{username}' => Yii::app()->user->getRelativeUsername($user->userid)
		)); ?>
    </div>
	<?php elseif( isset($seatsError) && $seatsError ): ?>
		<div class="new-user-issue span3">
			<div class="register-message-icon">
        <span>
			<i class="fa  fa-2x fa-warning"></i>
		</span>
			</div>
			<?php echo  Yii::t('standard', "You don't have enough seats for this course"); ?>
		</div>
    <?php elseif ( isset($enrollError) && $enrollError ): ?>
        <div class="new-user-issue span3">
            <div class="register-message-icon">
        <span>
			<i class="fa  fa-2x fa-warning"></i>
		</span>
            </div>
            <?php echo  Yii::t('standard', "You need to choose a session"); ?>
        </div>
	<?php else: ?>

	<div class="new-user-success span3">
		<div class="register-message-icon">
	        <span>
				<i class="fa  fa-2x fa-check-circle-o"></i>
			</span>
		</div>
		<?php echo Yii::t('user_management', 'New user {username} added successfully', array(
			'{username}' => Yii::app()->user->getRelativeUsername($user->userid)
		)); ?>
	</div>
	<?php endif; ?>


    <div class="new-user-success-content">
	<h3><?php echo Yii::t('standard', 'What do you want to do next?'); ?></h3>

	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'user_form',
	)); ?>

	<?
	$event = new DEvent($this, array());
	Yii::app()->event->raise('AfterUserCreationActionRender', $event);

	if (!$event->shouldPerformAsDefault()) : //add mass enrollment instead of standard user->single course enrollment (little dirty way, but if open the dialog directly here there are errors)
	?>
		<div class="go-back clearfix" style="height: 37px">
			<span><?php echo Yii::t('standard', 'Enroll users'); ?></span>
			<?php echo CHtml::link(Yii::t('standard', 'Enroll users'), Docebo::createAdminUrl('courseManagement/index', array('enrollUsers'=>1)), array(
				'class' => 'btn redirect-btn mass-enrollment',
			)); ?>
		</div>
		<script>
			$('.mass-enrollment').click(function(e){
				e.preventDefault();
				e.stopPropagation();
				window.location = $('.mass-enrollment').attr('href');
			});
		</script>
	<? else : ?>

	<div class="enroll-to-a-course">
		<?php echo Yii::t('user_management', 'Enroll {username} to a course', array(
			'{username}' => Yii::app()->user->getRelativeUsername($user->userid)
		)); ?>
	</div>

	<input data-url="<?= Docebo::createAppUrl('admin:courseManagement/courseAutocomplete') ?>" id="advanced-search-course" type="text" name="LearningCourse[name]" placeholder="<?=Yii::t('course', '_COURSE_SELECTION')?>"/>
	<?php echo CHtml::link(Yii::t('standard', 'Enroll'), 'javascript:void(0);', array(
		'class' => 'btn confirm-btn',
		'data-submit-form' => 'user_form',
	)); ?>
	<div id="sessions"></div>
	<? endif; ?>

	<div class="another-user-create clearfix">
		<span><?php echo Yii::t('user_management', 'I want to create another user'); ?></span>
		<?php echo CHtml::link(Yii::t('standard', '_NEW_USER'), Yii::app()->createUrl('userManagement/createUser'), array(
			'class' => 'btn redirect-btn',
		)); ?>
	</div>
	<div class="go-back clearfix">
	    <span><?php echo Yii::t('standard', 'i want go back'); ?></strong></span>
		<?php echo CHtml::link(Yii::t('standard', '_BACK'), 'javascript:void(0);', array(
			'class' => 'btn close-btn',
			'data-dismiss' => 'modal',
		)); ?>

		<?php $this->endWidget(); ?>
	    </div>
    </div>
</div>
<script type="text/javascript">
	(function ($) {
		$(function () {
			replacePlaceholder();

			$( "#advanced-search-course" ).autocomplete({
				source: function( request, response ) {
					$.ajax({
						url: "<?=Docebo::createAppUrl('admin:courseManagement/courseAutocomplete')?>",
						dataType: "json",
						type: "POST",
						data: {
							data:{
								query: request.term,
								formattedCourseName: true
							}
						},
						success: function( data ) {
							$('#idCourse').remove();
							var options = [];
							$.each(data.options, function(index, value){
								options.push({
									"value" : index,
									"label" : value
								})
							});
							response( options );
						}
					});
				},
				minLength: 1,
				delay: 500,
				select: itemSelected,
				focus: itemSelected,
				change: function (event, ui) {
					getSessions($(this));
				},
				close: function (event, ui) {
					this.blur();
				}
			});
		});
	})(jQuery);

	function itemSelected(event, ui) {
		$('#idCourse').remove();
		var hiddenField = $('<input type="hidden" id="idCourse" name="idCourse" value="' + ui.item.value + '"/>');
		$( "#user_form" ).append(hiddenField);
		ui.item.value = ui.item.label;
	}

	$('.ui-autocomplete').css({
		position: "fixed",
		maxHeight: "50%",
		overflowY: "auto",
		overflowX: "hidden"
	});

	function getSessions(that){
		var url = '<?=Docebo::createAbsoluteAdminUrl("courseManagement/getSessions")?>';
		var name = that.parent().find('input[id="idCourse"]').val();
		if(!name)
			var name = that.val();
		$('div#sessions').html('<?=Yii::t("calendar","_PLS_WAIT")?>');
		$.ajax({
			url: url,
			type: 'post',
			data: {'courseName': name}
		}).success(function (data) {
			$('div#sessions').html(data);
		});
	}
</script>
