<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAdminUrl('dashboard/index'),
	Yii::t('menu', '_USER_MANAGMENT') => array('userManagement/index'),
	Yii::t('organization_chart', 'Manage users via CSV'),
); ?>
<style>
	#msgContainer{
		margin: 20px 0;
	}
</style>
<?php //$this->renderPartial('/common/_mainActions'); ?>

<div class="main-section import">

	<?php
	$options = array(
			'id' => 'user-import-form-step2',
	);

	if (Yii::app()->backgroundJob->bgJobEnabled(BackgroundJob::FAMILTY_IMPORTFROMCSV)) {
		$options['action'] = Docebo::createAppUrl("admin:userManagement/axProgressiveUsersImport");
		$options['method'] = 'POST';
	}

	$form = $this->beginWidget('CActiveForm', $options);

	if (!isset($_POST['fromActivationTab']) || $_POST['fromActivationTab'] != 1) { ?>
		<h3 class="title-bold"><?php echo Yii::t('organization_chart', 'Manage users via CSV'); ?></h3>
	<?php }
	if (!empty($_POST['UserImportForm']['activation'])) {
		echo CHtml::hiddenField('activation', $_POST['UserImportForm']['activation']);
	}
	$hideHeader = true;
	if(!isset($_POST['fromActivationTab']) || $_POST['fromActivationTab'] != 1){
		$hideHeader = false; ?>
		<?php if(!Yii::app()->user->getIsPu() || (Yii::app()->user->getIsPu() && Yii::app()->user->checkAccess('/framework/admin/usermanagement/mod'))) {?>
		<div class="user-import-options">
			<div class="user-import-options-border clearfix">
				<h4><?= Yii::t('admin', 'Options')?></h4>
				<div class="user-import-options-check">
					<div class="clearfix">
						<?php echo $form->checkBox($model, 'insertUpdate'); ?>
						<div class="user-import-options-label">
							<?php echo $form->labelEx($model, 'insertUpdate'); ?>
						</div>
					</div>
					<div class="clearfix ignorePasswordChange">
						<?php echo $form->checkBox($model, 'ignorePasswordChangeForExistingUsers'); ?>
						<div class="user-import-options-label">
							<?php echo $form->labelEx($model, 'ignorePasswordChangeForExistingUsers'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php }  ?>
		<h3 class="title-bold"><?php echo Yii::t('organization_chart', '_IMPORT_MAP'); ?></h3>

		<?php foreach ($columns as $i => $column) {?>
			<?php
			$columns[$i]['header']  = CHtml::dropDownList('import_map['.$column['name'].']', $model->importMap[$i], UserImportForm::getImportMapList());
			$columns[$i]['header'] .= '<p>'.$column['header'].'</p>';
			$columns[$i]['cssClassExpression'] = '"users-import-col"';
			?>
		<?php } ?>
	<?php }?>
	<?php echo $form->errorSummary($model); ?>

	<?php
	if(!isset($_POST['fromActivationTab']) || $_POST['fromActivationTab'] != 1) {
		$this->widget('zii.widgets.grid.CGridView', array(
				'id' => 'users-management-import-grid',
				'htmlOptions' => array('class' => 'grid-view clearfix'),
				'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
				'dataProvider' => $model->dataProvider(),
				'enablePagination' => false,
				'columns' => $columns,
				'hideHeader' => $hideHeader,
				'template' => '<div class="table-scroll">{items}</div>',
				'summaryText' => Yii::t('standard', '_TOTAL'),
		));
	}
	?>

	<div class="row-fluid">
		<?php
		if(!isset($_POST['fromActivationTab']) || $_POST['fromActivationTab'] != 1) {
			echo Yii::t('standard', '{N} users to be imported. Displaying few of them for review only', array('{N}' => $model->totalUsers));
		} else {
			$msg = ($model->activation == 'yes') ?
					Yii::t('user_management', 'Are you sure you want to Activate "{users} Users"?', array('{users}' => $model->totalUsers))
					: Yii::t('user_management', 'Are you sure you want to Deactivate "{users} Users"?', array('{users}' => $model->totalUsers));
			$msg = ucfirst(strtolower($msg)); ?>
			<div id="msgContainer">
				<h2><?= $msg; ?></h2>
				<hr/>
				<?=Yii::t('user_management', 'Click Submit to start the process')?>
			</div>
		<?php }

		?>
	</div>
	
	<div class="row btns">
		<?php 
			echo CHtml::link(Yii::t('standard', '_CANCEL'), array('userManagement/index'), array('class' => 'btn close-btn'));

		if (Yii::app()->backgroundJob->bgJobEnabled(BackgroundJob::FAMILTY_IMPORTFROMCSV)) {
			$progressUrl = Docebo::createAbsoluteUrl('admin:userManagement/axProgressiveUsersImport');
			echo CHtml::submitButton(Yii::t('standard', 'Submit'), array(
					'class' => 'open-dialog btn confirm-btn',
					)
			);
		}
//		elseif (false){
//			$progressUrl = Docebo::createAbsoluteUrl('admin:userManagement/axProgressiveUsersImport');
//			echo '<a id="createBGJobImport" class="btn confirm-btn">'.Yii::t('standard', 'Submit').'</a><script type="text/javascript">
//					$("#createBGJobImport").on("click",function(){
//						var data = $("#user-import-form-step2").serialize();
//						data["type"] = '.Progress::JOBTYPE_IMPORT_USERS_CSV.';
//						$.post("'.$progressUrl.'", data, function(data){
//							window.location.href = "'.Docebo::createAdminUrl("userManagement/index").'";
//						});
//					});
//				</script>
//			';
//		}
		else {
			$progressUrl = Docebo::createAbsoluteLmsUrl('progress/run', array(
					'type' => Progress::JOBTYPE_IMPORT_USERS_CSV,
			));
			echo CHtml::link(Yii::t('standard', 'Submit'), $progressUrl, array(
					'class' => 'open-dialog btn confirm-btn',
					'rel' => 'progressive-import-dialog',
					'data-dialog-title' => Yii::t('standard', 'Importing users'),
					'data-ajaxType' => 'POST',    // see events below!
			));
		}
		?>
	</div>

	<?php $this->endWidget(); ?>
</div>


<script type="text/javascript">
	
	(function ($) {
		$(function () {
            $('input, select').styler();
            autoSelectWidth();
			$('.table-scroll').jScrollPane({
				autoReinitialise : true
			});

			// Add Form data to POST request upon opening the final progressive dialog
			// Disable when progressing really starts so Dialog's form data to be posted.
			//$(document).on('dialog2.before-send', '#progressive-import-dialog', function(e, xhr, settings) {
			//	settings.data = $('#user-import-form-step2').serialize();
			//});

		});

		// See themes/spt/js/user_management.js
		var UserManImportCsv = new AdminUserManagementImportCsv({});

		/**
		 * Using jQuery blockUI plugin, block specific element
		 */
		var blockBlockable = function(elem) {
			elem.block({
				message: "",
				timeout: 0,
				overlayCSS:  {
					backgroundColor: 	'#f1f3f2',
					opacity:         	0.6,
					cursor:          	'auto'
				}
			});
		};

		blockBlockable($('div.ignorePasswordChange'));

		$('input[id*="insertUpdate"]').on("change", function(){
			var enabled = $(this).is(":checked");
			if(enabled){
				$('div.ignorePasswordChange').unblock();
			}else{
				$('input[id*="ignorePassword"]:nth-child(2)').attr('checked', false);
				$('input[id*="ignorePassword"]:nth-child(2) + span').removeClass('checked');
				blockBlockable($('div.ignorePasswordChange'));
			}
		});

	})(jQuery);
	
</script>
