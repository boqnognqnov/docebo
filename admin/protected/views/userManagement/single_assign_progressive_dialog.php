<?php
echo CHtml::beginForm('', 'POST', array(
	'class'=>'ajax',
));

echo CHtml::hiddenField('_key_', $cacheKey, array('id' => '_key_-input'));
echo CHtml::hiddenField('idOrg', $idOrg, array('id' => 'selected_nodes-input'));

?>
	<div id="assign-user-progressive-container" class="assign-user-progressive-container">


		<div class="row-fluid">
			<div class="span12">

				<div class="pull-left sub-title" >
					<b></b><?= Yii::t('standard', 'Importing {x} out of {y}', array('{x}' => $processedUsers, '{y}' => $totalUsers)) ?></b>
				</div>
				<div class="pull-right percentage-gauge"><?=$completedPercent?>%</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span12">
				<div class="docebo-progress progress progress-success">
					<div style="width: <?=$completedPercent?>%;" class="bar full-height" id="uploader-progress-bar"></div>
				</div>
			</div>
		</div>
		<? if($stop): ?>
			<div class="form-actions">
				<input class="btn btn-docebo black big close-dialog" type="submit" value=<?= Yii::t('standard', '_CLOSE') ?>>
			</div>
		<? endif; ?>

	</div>

<?= CHtml::endForm() ?>

<script type="text/javascript">

	$(function(){

		//while doing progressive stuff, no confirm button has to be displayed
		var b = $('.modal.fade.in').find('.btn-submit');
		if (b.length > 0) { b.remove(); }

		var updateGrid = function() {
			if (UserMan) { UserMan.updateUsersGrid(); } //NOTE: UserMan variable is globally available in user management page
		};

		<?php if ($stop): ?>
		updateGrid();
		<?php endif; ?>

		<?php if ($start && !$stop): ?>
		var c = $('.modal.fade.in').find('.btn-cancel');
		if (c.length > 0) { c.on('click', function(e) { updateGrid(); }); }
		var c = $('.modal.fade.in').find('.btn-close');
		if (c.length > 0) { c.on('click', function(e) { updateGrid(); }); }
		<?php endif; ?>

	});

</script>