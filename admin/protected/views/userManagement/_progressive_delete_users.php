<div class="row-fluid">
	<div class="span12">

		<div class="pull-left sub-title" >
			<b><?=Yii::t('user_management', 'Deleting {x} out of {y}', array('{x}' => $processedUsers, '{y}' => $totalUsers))?></b>
		</div>
		<div class="pull-right percentage-gauge"><?=$completedPercent?>%</div>
	</div>
</div>
<div class="row-fluid">
	<div class="span12">
		<div class="docebo-progress progress progress-success">
			<div style="width: <?=$completedPercent?>%;" class="bar full-height" id="uploader-progress-bar"></div>
		</div>
	</div>
</div>
<?php if($stop): ?>
	<div class="form-actions">
		<input class="btn btn-docebo black big close-dialog" type="submit" value=<?= Yii::t('standard', '_CLOSE') ?>>
	</div>
<?php endif; ?>
<?php if($stop): // Called only once when the progressive operation reaches 100% ?>
	<script type="text/javascript">
		$(function(){
			var gridId = "userman-users-management-grid";
			updateSelectAllCounter(gridId,"");
			$(document).on('dialog2.closed', '#delete-users-progressive', function(){
				$.fn.updateUserContent('');
			});
		});
	</script>
<?php endif; ?>