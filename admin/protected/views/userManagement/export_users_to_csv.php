<?php
$progressiveRunUrl = Docebo::createLmsUrl('progress/run', array(
    'type'		=> Progress::JOBTYPE_EXPORT_USERS_TO_CSV
));
?>

<div class="row-fluid">
	<div class="span12 export-users">
		<div id="export-user"><?= Yii::t('standard', '_ORGCHART') ?>: <b><?= $breadcrumb ?></b></div>
	</div>
</div>
<hr />
<?php if($fileType == 'export_users_csv') :?>
	<div style="margin-top: 15px; margin-bottom: 20px; margin-left: 0px;" class="span12">
		<?= Yii::t('standard', 'Choose separator').':' ?>
	</div>
<?php endif; ?>
<div class="row-fluid form">

	<?php
	echo CHtml::beginForm($progressiveRunUrl, 'post', array(
		'class' => 'ajax',
		'id' => 'user_form_export',
	));
	?>
	<?php if($fileType == 'export_users_csv') :?>
		<div class="span12 separators">
			<label class="radio pull-left" for="r_1">
				<?= CHtml::radioButton('separator', true, array(
					'value' => '1',
					'id' => 'r_1',
					'class' => 'radioButtons',
					'uncheckValue' => null)) . ' ,' ?>
			</label>
			<label class="radio pull-left" for="r_2">
				<?= CHtml::radioButton('separator', false, array(
					'value' => '2',
					'id' => 'r_2',
					'class' => 'radioButtons',
					'uncheckValue' => null)) . ' ;' ?>
			</label>
			<label style="margin-top: -8px" class="radio pull-left" for="r_3">
				<?= CHtml::radioButton('separator', false, array(
					'value' => '3',
					'id' => 'r_3',
					'class' => 'radioButtons',
					'uncheckValue' => null)) . Yii::t('standard', '_MANUAL') .': ' .CHtml::textField('manual-separator', '', array(
					'maxlength' => '1') ) ?>
			</label>
		</div>
	<?php endif; ?>

	<div class="row-fluid">
		<div style="margin-top: 10px; margin-bottom: 10px;" class="span12">
			<label class="checkbox pull-left" for="include-names">
				<?= CHtml::checkBox('include-names', false, array('class' => 'checkBoxes')) .
				Yii::t('standard', 'Include column names in the first row') ?>
			</label>
		</div>
	</div>
	<hr/>
	<div class="row-fluid all-fields">
		<div class="span12">
			<div class="span6 user-fields">
				<div class="header-fields">
					<?= Yii::t('standard', 'Select <b>user fields:</b>') ?>
				</div>
				<ul>
					<?php if(isset($allFields['fixed']['userid'])) : ?>
					<li class="fields">
						<i class="i-sprite is-lock grey" title="" rel="tooltip" data-html="true"
							 data-original-title=""> </i><?= ' ' . $allFields['fixed']['userid'] ?>
						<label class="checkbox pull-left hidden" for="userid">
							<?= CHtml::checkBox('userid', true, array('class' => 'checkBoxes')) . $allFields['fixed']['userid'] ?>
						</label><br/>
					</li>
					<?php endif;?>
					<?php foreach ($allFields['selectable'] as $l => $v): ?>
						<?php if ( isset($allFields['custom_fields']) && is_array($allFields['custom_fields']) &&  !in_array($l, array_keys($allFields['custom_fields'])) && $l != 'groups_list' && $l != 'suspend_date' && $l != 'valid') : ?>
							<li class="fields">
								<label class="checkbox pull-left" for="<?= $l ?>">
									<?= CHtml::checkBox('selectedUserFields[' . $l . ']', false, array('class' => 'checkBoxes')) . $v ?>
								</label><br/>
							</li>
						<?php
							// don't include the additional fields in the first column (Select user fields:)
							elseif(in_array($l, array_keys($allFields['custom_fields']))):
								continue;
						?>

						<?php elseif($l != 'groups_list' && $l != 'suspend_date' && $l != 'valid'): ?>
							<li class="fields">
								<label class="checkbox pull-left" for="<?= $l ?>">
									<?= CHtml::checkBox('selectedUserFields[' . $l . ']', false, array('class' => 'checkBoxes')) . $v ?>
								</label><br/>
							</li>
						<?php endif; ?>
					<?php endforeach; ?>
				</ul>
			</div>
			<div class="span6 custom-fields">
				<div class="header-fields">
					<?= Yii::t('standard', 'Select <b>additional fields:</b>') ?>
				</div>
				<ul>
					<?php if(isset($allFields['custom_fields'])) : ?>
					<?php foreach ($allFields['custom_fields'] as $l => $v) : ?>
						<li class="fields">
							<label class="checkbox pull-left" for="<?= $l ?>">
								<?= CHtml::checkBox('selectedAdditionalFields[' . $l . ']', false, array('class' => 'checkBoxes')) . $v ?>
							</label><br/>
						</li>
					<?php endforeach; ?>
					<?php endif; ?>
				</ul>
			</div>
		</div>
	</div>
	<hr />
	<?= CHtml::hiddenField('fileType', $fileType) ?>
	<?= CHtml::hiddenField('idst', (is_array($idst) ? implode(',', $idst) : $idst)) ?>
	<div class="form-actions">
		<?= CHtml::submitButton(Yii::t('standard', '_CONFIRM'), array(
			'class' => 'btn btn-docebo green big')); ?>

		<?= CHtml::button(Yii::t('standard', '_CLOSE'), array('class' => 'btn btn-docebo black big close-dialog')); ?>
	</div>
	<?php echo CHtml::endForm(); ?>
</div>
<script type="text/javascript">
	$().ready(function () {
		var separator = $("#manual-separator");

		$("div#export-user").find("ul").addClass("inline").css('display', 'inline-block');
		$("<span style='margin-left: 7px;'>&raquo;</span>").appendTo($("a.ajaxLinkNode"));

		$(".btn.btn-docebo.green.big").on("click",
			function () {
				if ($("span#r_3-styler").hasClass("checked") && separator.val().length < 1 && $("span#separator-error").length < 1) {
					separator
						.addClass("error")
						.focus()
						.after("<span id='separator-error' style='margin-left: 7px;' class='error'><?= Yii::t('standard', 'Please enter a separator') ?></span>");
					return false;
				}
				if ($("span#separator-error").length) {
					return false;
				}
			});
		separator.on('blur', function () {
			if ($(this).hasClass("error")) {
				$(this).removeClass("error");
				$("span#separator-error").remove();
			}
		});
		/****- stylers -****/
		$('input.radioButtons[type="radio"]').styler();
		$('input.checkBoxes[type="checkbox"]').styler();
	});
</script>
<style>
	hr {
		margin: 0px 0px;
	}

	.radio.pull-left {
		margin-right: 20px;
	}

	#manual-separator {
		width: 50px;
		margin-left: 10px;
	}

	div.header-fields {
		margin-top: 5px;
		margin-bottom: 15px;
	}

	li.fields {
		margin-bottom: 7px;
	}
</style>
