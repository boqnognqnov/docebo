<div class="userman-node-actions">
	<ul>
		<?php if (!$isRoot): ?>
			<li class="userman-node-action node-action">
				<?php

					$url 	= Docebo::createLmsUrl('usersSelector/axOpen', array(
						'type' 	=> UsersSelector::TYPE_ORGCHART_ASSIGN_USERS,
						'idOrg' 	=> $id,
					));

				echo CHtml::link(Yii::t('standard', '_ASSIGN_USERS'), $url, array(
						'class' => 'open-dialog',
						'rel' 	=> 'users-selector',
						'data-dialog-class' => 'node-assign-users-selector', // important! This class is used to listen for users selector close/success events
						'data-dialog-title' => Yii::t('standard', '_ASSIGN_USERS') . ": $name",
						'title'	=> Yii::t('standard', '_ASSIGN_USERS')
					));

				?>
			</li>
		<?php endif; ?>

		<?php if (Yii::app()->user->getIsGodadmin()): ?>

		<li class="node-action"><hr/></li>

		<?php endif; ?>

		<?php
		/**
		 * @devent Raise event to let plugins add menu items
		 * @param int $id the id of the current node
		 * @param CoreOrgChartTree $node this is the model of the current node
		 * @param bool $isRoot a boolean that is true if the current node is the root node of the branch
		 * @source 4
		 */
		$extra_menu = Yii::app()->event->raise('RenderOrgChartTreeNodeMenu', new DEvent($this, array(
			'id' => $id,
			'node' => $node,
			'isRoot' => $isRoot
		)));

		?>

		<?php if (Yii::app()->user->getIsGodadmin()): ?>

			<li class="node-action">
				<?php
					echo CHtml::link(Yii::t('standard', '_MOD'), Yii::app()->createUrl('orgManagement/edit', array('id' => $id)), array(
						'class' => 'node-edit new-node-handler',
						'data-modal-class' => 'editNode',
						'modal-title' => Yii::t('standard', '_MOD'),
					));
				?>
			</li>

			<?php if (!$isRoot): ?>
				<?php if (Settings::get('use_node_fields_visibility', 'off') == 'on'): ?>
				<li class="node-action manage-fields-visibility">
					<?php
					echo CHtml::link(Yii::t('admin_directory', 'Set additional fields visibility'), Yii::app()->createUrl('orgManagement/editFields', array('id' => $id)), array(
						'class' => 'training-materials new-node-handler',
						'data-modal-class' => 'editNodeFields',
						'modal-title' => Yii::t('admin_directory', 'Set additional fields visibility'),
					));
					?>
				</li>
				<?php endif; ?>

				<li class="node-action">
					<?php
					$buttons = array(
						array(
							'type' => 'submit',
							'title' => Yii::t('standard', '_CONFIRM'),
						),
						array(
							'type' => 'cancel',
							'title' => Yii::t('standard', '_UNDO'),
						),
					);


						$this->renderPartial('//common/_modal', array(
							'config' => array(
								'class' => 'delete-node',
								'linkTitle' => Yii::t('standard', '_DEL'),
								'modalTitle' => Yii::t('standard', '_DEL'),
								'url' => 'orgManagement/delete&id=' . $id,
								'buttons' => $buttons,
								'afterLoadingContent' => 'hideConfirmButton(); if (UserMan && data && data.update_tree) { $(".modal.delete-node").modal("hide"); UserMan.reloadOrgchartTree(); }',
								'afterSubmit' => 'updateUsersOrgChartTree',
							),
						)); ?>
				</li>

			<?php endif; ?>

		<?php endif; ?>

	</ul>
</div>