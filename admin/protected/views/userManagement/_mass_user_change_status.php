<script type='text/javascript'>
	$(function(){
		$.post('<?=Docebo::createLmsUrl('progress/run')?>', {
			'type': <?=Progress::JOBTYPE_CHANGE_USER_STATUS?>,
			'idst': '<?= $idst ?>',
			'status': <?= $status ?>,
			'unenrollIltSessions': <?= $unenrollIltSessions ?>,
			'unenrollWebinarSessions': <?= $unenrollWebinarSessions ?>,
			'unenrollAllUsers': <?= $unenrollAllUsers ?>
		}, function(data){
			removeModal();
			var dialogId = 'change-user-status-progressive';

			$('<div/>').dialog2({
				autoOpen: true, // We will open the dialog later
				id: dialogId,
				title: '<?php echo ($status ? Yii::t('standard', '_ACTIVATE') : Yii::t('standard', '_DEACTIVATE'))?>'
			});
			$('#'+dialogId).html(data);
		});
	});
</script>