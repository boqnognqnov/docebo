<?php if(!empty($ignored)): $tmpUserModel = new CoreUser(); ?>
	<div class="alert alert-error">
	<?=Yii::t('standard', 'The following users have been ignored because they have expired')?> :
		<?php $str = ''; ?>
		<?php foreach($ignored as $ignoredUser):
			$tmpUserModel->setAttributes($ignoredUser);
			$tmpUserModel->idst = $ignoredUser['idst']; ?>
			<?php $str .= $tmpUserModel->getFullName().', '; ?>
		<?php endforeach; ?>
		<?php echo rtrim($str, ', '); ?>
	</div>
<?php unset($tmpUserModel); endif; ?>

<div class="row-fluid">
	<div class="span12">

		<div class="pull-left sub-title" >
			<?php if($status):?>
				<b><?=Yii::t('course_management', 'Activating {x} out of {y}', array('{x}' => $processedUsers, '{y}' => $totalUsers))?></b>
			<?php else: ?>
				<b><?=Yii::t('course_management', 'Deactivating {x} out of {y}', array('{x}' => $processedUsers, '{y}' => $totalUsers))?></b>
			<?php endif; ?>
		</div>
		<div class="pull-right percentage-gauge"><?=$completedPercent?>%</div>
	</div>
</div>
<div class="row-fluid">
	<div class="span12">
		<div class="docebo-progress progress progress-success">
			<div style="width: <?=$completedPercent?>%;" class="bar full-height" id="uploader-progress-bar"></div>
		</div>
	</div>
</div>
<?php if($stop): ?>
	<div class="form-actions">
		<input class="btn btn-docebo black big close-dialog" type="submit" value=<?= Yii::t('standard', '_CLOSE') ?>>
	</div>
<?php endif; ?>
<?php if($stop): // Called only once when the progressive operation reaches 100% ?>
	<script type="text/javascript">
		$(function(){
			var gridId = "userman-users-management-grid";
			updateSelectAllCounter(gridId,"");
			$(document).on('dialog2.closed', '#change-user-status-progressive', function(){
				$.fn.updateUserContent('');
			});
		});
	</script>
<?php endif; ?>