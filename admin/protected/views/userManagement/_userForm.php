<?php /* @var $allowEditUserid bool */ ?>
<style>
	#sendEmail {
		display: none;
	}

	#sendEmail .send_email > label {
		cursor: pointer;
	}
</style>
<?php

	// [CertaproApp]
	// Allow plugins to disable Branches TAB.
	$enableBranches = true;
	// Raise an event to ask listeners for some extra information, if any
	Yii::app()->event->raise('beforeUserManagementUserFormRender', new DEvent($this, array(
		'enableBranches'	=> &$enableBranches
	)));

	if(!$enableBranches)
		$activeTab = 'userForm-details';
?>


<?php if (!empty($errors)) : ?>
	<div class="errorMessage text-right">
		<?php
			foreach ($errors as $error) {
				echo $error . "<br>";
			}
		?>
	</div>
	<div class="clearfix"></div>
<?php endif; ?>

<div class="new-user-tabs">
	<ul class="nav nav-tabs" id="userFormTab">
		<li class="<?= ($activeTab == 'userForm-details') ? 'active' : '' ?>">
			<a data-target="#userForm-details" href="#"><span class="user-form-details"></span><?php echo Yii::t('profile', '_PROFILE'); ?></a>
		</li>

		<!-- HIDE IF NO BRANCHES -->
		<?php if (!empty($orgChartFancyTreeData) && $enableBranches) : ?>
			<li class="<?= ($activeTab == 'userForm-orgchart') ? 'active' : '' ?>">
				<a data-target="#userForm-orgchat" href="#"><span class="user-form-orgchat"></span><?php echo Yii::t('standard', 'branches'); ?></a>
			</li>
		<?php endif;?>


	</ul>

	<div class="form new-user-form clearfix">
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'user_form',
			'htmlOptions' => array(
				'enctype' => 'multipart/form-data',
			),
		)); ?>

		<?php
		//from now on every reloading of the dialog won't be the "first loading"
		echo CHtml::hiddenField('first-loading', 0);
		?>

		<div class="tab-content" id="userFormTabContent">
			<div class="tab-pane <?= ($activeTab == 'userForm-details') ? 'active' : '' ?>" id="userForm-details">
				<?php if ($allowEditUserid) : ?>
				<div class="clearfix line-margin">
					<?php echo $form->error($user, 'userid'); ?>
					<div class="clearfix">
						<?php echo $form->textField($user, 'userid'); ?>
						<?php echo $form->labelEx($user, 'userid'); ?>
					</div>
				</div>
				<?php endif; ?>

				<div class="clearfix line-margin">
					<?php echo $form->error($user, 'firstname'); ?>
					<div class="clearfix">
						<?php echo $form->textField($user, 'firstname'); ?>
						<?php echo $form->labelEx($user, 'firstname'); ?>
					</div>
				</div>

				<div class="clearfix line-margin">
					<?php echo $form->error($user, 'lastname'); ?>
					<div class="clearfix">
						<?php echo $form->textField($user, 'lastname'); ?>
						<?php echo $form->labelEx($user, 'lastname'); ?>
					</div>
				</div>

				<div class="clearfix line-margin">
					<?php echo $form->error($user, 'email'); ?>
					<div class="clearfix">
						<?php echo $form->textField($user, 'email'); ?>
						<?php echo $form->labelEx($user, 'email'); ?>
					</div>
				</div>

				<div class="clearfix line-margin">
					<?php echo $form->error($user, 'new_password'); ?>
					<div class="clearfix">
						<?php echo $form->passwordField($user, 'new_password'); ?>
						<?php echo $form->labelEx($user, 'new_password'); ?>
					</div>
				</div>

				<div class="clearfix line-margin">
					<?php echo $form->error($user, 'new_password_repeat'); ?>
					<div class="clearfix">
						<?php echo $form->passwordField($user, 'new_password_repeat'); ?>
						<?php echo $form->labelEx($user, 'new_password_repeat'); ?>
					</div>
				</div>

				<div class="clearfix force_change line-margin">
					<?php echo $form->error($user, 'force_change'); ?>
					<div class="clearfix">
						<?php echo $form->checkBox($user, 'force_change', array('checked'=>((Settings::get('pass_change_first_login')=='on' || $user->force_change == true) ? 'checked' : ''))); ?>
						<?php echo $form->labelEx($user, 'force_change'); ?>
					</div>
				</div>

				<div class="clearfix"></div>
				<div class="clearfix line-margin">
					<?php echo $form->error($groupMember, 'idst'); ?>
					<div class="clearfix">
						<?php $htmlOptions = array();
						if ($user->idst == Yii::app()->user->id) {
							$htmlOptions['disabled'] = 'disabled';
						} ?>

						<?php
							// Exclude "Godadmin" level from dropdown if dialog is viewed from a Power User
							$isGodAdmin = Yii::app()->user->getIsGodadmin();
							/*$levels = CoreGroup::getAdminLevels(true, !$isGodAdmin, $isGodAdmin);
							echo $form->dropDownList($groupMember, 'idst', $levels, $htmlOptions);
							echo CHtml::label($user->getAttributeLabel('level'), CHtml::activeId($groupMember, 'idst'));*/

							// Exclude "Godadmin" level from dropdown if dialog is viewed from a Power User
							if($isGodAdmin)
							{
								$levels = CoreGroup::getAdminLevels(true);
								echo $form->dropDownList($groupMember, 'idst', $levels, $htmlOptions);
								echo CHtml::label($user->getAttributeLabel('level'), CHtml::activeId($groupMember, 'idst'));
							}
							elseif(is_null($groupMember->idst))
							{
								$levels = CoreGroup::getAdminLevels(true, !$isGodAdmin, $isGodAdmin);
								echo $form->dropDownList($groupMember, 'idst', $levels, $htmlOptions);
								echo CHtml::label($user->getAttributeLabel('level'), CHtml::activeId($groupMember, 'idst'));
							}
							else
							{
								$levels = CoreGroup::getAdminLevels(true);
								$htmlOptions['disabled'] = 'disabled';
								echo $form->dropDownList($groupMember, 'idst', $levels, $htmlOptions);
								echo CHtml::label($user->getAttributeLabel('level'), CHtml::activeId($groupMember, 'idst'));
								echo CHtml::hiddenField(CHtml::activeName($groupMember, 'idst'), $groupMember->idst);
							}
						?>
					</div>
				</div>
				<div class="clearfix line-margin">
					<?php echo $form->error($settingUser, 'value'); ?>
					<div class="clearfix">
						<?php
						if (isset($prePolupatedFields['language']))
							$settingUser->value = $prePolupatedFields['language'];
						echo $form->dropDownList($settingUser, 'value', CoreLangLanguage::getActiveLanguages(), array(
							'name' => 'CoreSettingUser[ui.language]',
						)); ?>
						<?php echo $form->labelEx($user, 'language.value', array(
							'for' => 'CoreSettingUser_ui.language',
						)); ?>
					</div>
				</div>

                <?php if ($settingDatelocale) : ?>
                    <div class="clearfix line-margin">
                        <?php echo $form->error($settingDatelocale, 'value'); ?>
                        <div class="clearfix">
                            <?php echo $form->dropDownList($settingDatelocale, 'value', CHtml::listData(Lang::getLanguages(), 'browsercode', function($lang) { return CHtml::encode($lang['description']); }), array(
                                'name' => 'CoreSettingUser[date_format_locale]',
                                'prompt' => Yii::t('configuration', 'Select a date locale...'),
                            )); ?>
                            <?php echo $form->labelEx($user, 'date_format_locale.value', array(
								'for' => 'CoreSettingUser_date_format_locale',
							)); ?>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if ($settingDateformat) : ?>
                    <div class="clearfix line-margin">
                        <?php echo $form->error($settingDateformat, 'value'); ?>
                        <div class="clearfix">
                            <?php echo $form->dropDownList($settingDateformat, 'value', Yii::app()->localtime->getDateTimeFormatsArray(), array(
                                'name' => 'CoreSettingUser[date_format]',
                                'prompt' => Yii::t('configuration', 'Select a date format...')
                            )); ?>
                            <?php echo $form->labelEx($user, 'date_format.value'); ?>
                        </div>
                    </div>
                <?php endif; ?>

				<?
				/* @var $user CoreUser */

				$editedUserIsGodadmin = false;

				if($user->idst != null || $user->idst != ''){
					$editedUserIsGodadmin = Yii::app()->user->getHighestLevel($user->idst)==Yii::app()->user->getGodadminLevelLabel();
				}

				$editedUserIsPu = Yii::app()->user->getHighestLevel($user->idst)==Yii::app()->user->getAdminLevelLabel();

				$showExpirationField =  Yii::app()->user->getIsGodadmin() // Current user is godadmin, only he can manage expiration dates
									   && !$editedUserIsGodadmin; // godadmins can not have expiration date enforced

				if(!Yii::app()->event->raise('onBeforeUserExpirationField', new DEvent($this, array('user'=>&$user)))){
					$showExpirationField = false;
				}

				if($showExpirationField): ?>

					<? if(Yii::app()->event->raise('onBeforeUserExpirationField', new DEvent($this, array('user'=>&$user)))): ?>

							<div class="clearfix line-margin">
								<?php echo $form->error($user, 'expiration'); ?>
								<div class="clearfix">
									<div class='input-append date' data-date-format='<?=Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat())?>' data-date-language='<?=Yii::app()->getLanguage()?>'>
									<?=$form->textField($user, 'expiration', array(
										'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat()),
										'data-date-language' => Yii::app()->getLanguage()
									));?>
									<span class="add-on"><i class="icon-calendar"></i></span></div>
									<?php echo $form->labelEx($user, 'expiration'); ?>
								</div>
							</div>
					<? endif; ?>
				<? endif; ?>

				<div class="clearfix line-margin">
					<?php
					$choices = array(0 => Yii::t('standard', 'Unverified'), 1 => Yii::t('standard', 'Verified'));
					echo $form->error($user, 'email_status'); ?>
					<div>
						<?= $form->dropDownList($user, 'email_status', $choices); ?>
						<?= $form->labelEx($user, 'email_status'); ?>
					</div>
				</div>
				<?php
				if (isset($isActiveNotificationOfType) && $isActiveNotificationOfType === true) { ?>
					<div class="clearfix force_change line-margin">
						<div id="sendEmail" class="clearfix">
							<div class="send_email">
								<?= CHtml::checkBox('sendEmailBox', false, array()); ?>
								<?= CHtml::label(Yii::t('standard', 'Send verification email'), 'sendEmailBox'); ?>
							</div>
						</div>
					</div>
				<?php }

				if (!empty($user->additionalFields)) { ?>
				 <style>.additional-fields label{ word-wrap: break-word; }</style>

					<div class="additional-fields">
						<?php
						$nodes = array();
						foreach($orgChartFancyTreePreselected as $node)
							$nodes[] = $node['key'];
						$visibleFields = $user->getVisibleAdditionalFieldsByBranch($nodes);
						foreach ($user->additionalFields as $field) { ?>
							<?php $visible = (isset($visibleFields[$field->getFieldId()]))? true : false;?>
							<div class="clearfix line-margin" id="additional-field-<?=$field->getFieldId()?>" style="<?=($visible? '' : 'display:none;')?>">
								<?php
								// TRUE is for "render this for AJAX, if possible"
								if(empty($field->userEntry)) {
									if (!empty($prePolupatedFields) && array_key_exists($field->getFieldId(), $prePolupatedFields)) {
										$field->userEntry = $prePolupatedFields[$field->getFieldId()];
									} elseif (isset($prePolupatedFields) && is_array($prePolupatedFields)) {
										$field->userEntry = '';
									}
								}
								echo $field->renderField(true);
								?>
							</div>
						<?php } ?>
					</div>
				<?php } ?>


			</div>

			<script>
				function onNodeClickHandler(event, data) {

					var target = null;
					if (!event) { event = window.event; }
					if (event.target) { target = event.target; } else if (event.srcElement) { target = event.srcElement; }
					if (!target) {
						Docebo.log("onNodeClickHandler: Invalid target element");
						return false;
					}

					if (data && data.targetType == "checkbox" && target.parentNode.classList.contains('fancytree-selected')) {
						$nodeInPathToRoot = data.node;
						do
						{
							if ($nodeInPathToRoot.title == 'Salesforce') { return false; }
							$nodeInPathToRoot = $nodeInPathToRoot.parent;
						} while ($nodeInPathToRoot.title != 'root');
					}

				}

				function onNodeSelectHandler(event, data) {
					<?php if((Settings::get('use_node_fields_visibility', 'off') == 'on')):?>
						var nodes = $('*[id^="orgcharts-fancytree"]').first().fancytree('getTree').getSelectedNodes();
						var selectedNodes = [];
						$.each(nodes, function (index, value) {
							selectedNodes[index] = value.key;
						});

						$.ajax({
							url: '<?= $this->createUrl('updateAdditionalFields') ?>',
							method: "POST",
							dataType: 'json',
							data: {selectedNodes: selectedNodes}
						}).done(function (res) {
							if (res.success == true) {
								$('.additional-fields').children().hide();
								$.each(res.data.fields, function (index, value) {
									$('#additional-field-'+index).show()
								});
							}
						});
					<?php endif;?>
				}
			</script>

			<div class="tab-pane <?= ($activeTab == 'userForm-orgchart') ? 'active' : '' ?>" id="userForm-orgchat">
				<?php if (!empty($orgChartFancyTreeData) && $enableBranches) : ?>
					<?php
						// Show Org Charts using Fancytree
						$id = 'orgcharts-fancytree';
						$this->widget('common.extensions.fancytree.FancyTree', array(
								'id' => $id,
								'treeData' => $orgChartFancyTreeData,
								'preSelected' => $orgChartFancyTreePreselected,
								//'selectMode' => FancyTree::SELECT_MODE_DOCEBO,
								'selectMode' => 2,  // PLAMEN
								'formFieldNames' => array(
									'selectedInputName'	 	=> 'select-orgchart',
									'activeInputName' 		=> 'orgchart_active_node',
									'selectModeInputName'	=> 'orgchart_select_mode'
								),
								'eventHandlers' => array(
										'click'				=> 'onNodeClickHandler'
								)

							)
						);
					?>

				<?php endif; ?>
			</div>

		</div>

		<?php $this->endWidget(); ?>
	</div>
</div>
<div class="modal-footer">
    <div style="display: block; text-align: left; float: left; width: 50%">
        <?php if( !PluginManager::isPluginActive('WhitelabelApp') ) : ?>
            <?php $currentLanguage = Yii::app()->getLanguage(); ?>
            <?php ($currentLanguage == 'it' ? $it = 'it/' : $it = ''); ?>
            <?php ($currentLanguage != 'it' && $currentLanguage != 'en' ? $int = '-international' : $int = ''); ?>

            <p class="privacy-policy" style="font-size: 11px; font-weight: bold;">
                <?=Yii::t('standard', 'By clicking "Confirm", you are agreeing to the ')?>
                <a style="color: #0465AC; text-decoration: underline;" href="https://www.docebo.com/<?=$it?>docebo-privacy-policy<?=$int?>/" target="_blank">
                    <?=Yii::t('standard', 'Privacy Policy')?>
                </a>
            </p>
        <?php endif; ?>
    </div>
	<input class="btn confirm-btn" type="button" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" name="submit" data-submit-form="user_form"/>
	<input class="btn close-btn" type="button" value="<?php echo Yii::t('standard', '_CLOSE'); ?>"/>
</div>

<script type="text/javascript">
	(function ($) {
		$(function () {
			OrgChartTree();
			$('#userForm-orgchat').jScrollPane({
				autoReinitialise: true
			});

			$('#user_form').find('#<?= CHtml::activeId($settingDatelocale, 'date_format_locale') ?>')
				.unbind('change')
				.bind('change', function() {
					var url = '<?= $this->createUrl('getCustomDateFormats') ?>';
					$.get(url, {code:$(this).val(), idUser:'<?= $user->idst ?>'}, function(response) {
						if (response.success === true) {
							$('#user_form').find('#<?= CHtml::activeId($settingDatelocale, 'date_format') ?>').replaceWith(response.html);
						}
					}, 'json')
				}).trigger('change');

			//this is a fix for jscrollpane (sometimes the height of the orgchart content is not calculated correctly by jscrollpane plugin)
			var checkHeight = function() {
				var h, p = $('#userForm-orgchat .jspPane'), e = $('#orgcharts-fancytree');
				if (p.length > 0 && e.length > 0) {
					h = $('#orgcharts-fancytree').outerHeight(true);
					if (h > 0) {
						p.css('height', h+'px');
					}
					setTimeout(checkHeight, 700); //periodically checked as long the orgchart jscrollpane exists
				}
			};
			setTimeout(checkHeight, 700);

			// Hide the expiration date field when the level is changed to Super Admin
			$('#CoreGroupMembers_idst').change(function(){
				var currentUserLevelSelected = $(this).val();
				<? $godadminGroupId = Yii::app()->getDb()->createCommand()->select('idst')->from('core_group')->where('groupid=:superAdmin', array(':superAdmin'=>Yii::app()->user->getGodadminLevelLabel()))->queryScalar(); ?>
				var godadminUserLevelId = <?=$godadminGroupId ? $godadminGroupId : 0?>;
				if(currentUserLevelSelected==godadminUserLevelId){
					$('#CoreUser_expiration').closest('.line-margin').hide();
				}else{
					$('#CoreUser_expiration').closest('.line-margin').show();
				}
			});

			$('#CoreUser_email_status').change(function () {
				var value = $(this).val();
				if (value == 0) {
					$('#sendEmail').show();
				} else {
					$('#sendEmail').hide();
				}
			});
			setTimeout(function(){
				if($('#CoreUser_email_status').val() == 0){
					$('#sendEmail').show();
				}
			},500);

			$("#<?=$id;?>").bind("fancytreeselect", function(event, data){
				onNodeSelectHandler(event, data);
			});
		});
	})(jQuery);
</script>

