<?php
/* @var $user CoreUser */
/* @var $permissions array */
?>
<div class="node-actions">
	<ul>

        <?php if ((Yii::app()->user->isGodAdmin) || Yii::app()->user->checkAccess("/lms/admin/report/view")): ?>

		<?php echo CHtml::tag('li', array('class' => 'node-action'),
			CHtml::tag('a', array(
				'class' => 'ajaxModal node-summary',
				'data-url' => implode('&', array(
					'reportManagement/createUserReport',
					'idst=' . $user->idst
				)),
				'data-buttons' => '[]',
				'data-modal-title' => Yii::t('report', 'User personal summary'),
				'data-modal-class' => 'new-user-summary',
				'data-after-loading-content' => 'function() {applyTypeahead($(\'.modal.in .typeahead\')); applyDonut(\'.modal.in .donutContainer\'); applyLineCharts(\'.modal.in .lineChartContainer\');}',
				'title' => Yii::t('report', 'User personal summary'),
				'rel' => 'tooltip',
			), '<i class="fa fa-user"></i>'.Yii::t('report', 'User personal summary'))
		); ?>

        <?php endif; ?>

		<?php if (PluginManager::isPluginActive('MyBlogApp')) : ?>

			<li class="node-action">
				<a class="node-blog"
				   href="<?= Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/blog', array('idUser' => $user->idst)) ?>">
					<?= Yii::t('myblog', 'Blog') ?>
				</a>
			</li>

		<?php endif; ?>

		<li class="node-action"><hr/></li>

		<?php
		/**
		 * @devent UserManagementContextMenu
		 * @param CoreUser $user is the user of the current row user
		 */
		$event = new DEvent($this, array('user' => $user));
		Yii::app()->event->raise('UserManagementContextMenu', $event);
		$extra_menu = !empty($event->return_value);
		if (is_array($event->return_value) && isset($event->return_value['html'])) {
			//we don't know if the event handler would use echoes or return html to be printed, we allow both options
			echo $event->return_value['html'];
		}

		if ($extra_menu) {
			echo '<li class="node-action"><hr/></li>';
		}

		?>

		<?php if(PluginManager::isPluginActive('Share7020App') && Yii::app()->user->isGodAdmin){ ?>
				<li class="node-action">
						<a class="node-television"
						   href="<?php echo Docebo::createLmsUrl('channels/index', array('#' => '/pchannel/' . $user->idst)) ?>">
							<i class="fa fa-television"></i><?php echo Yii::t('app7020', 'User channel') ?>
				</a>
				</li>
		<?php } ?>

		<?php

		if ($permissions['mod']) {

			/**
			 *
			 * Show it in these scenarios:
			 * - Viewed by Godadmin
			 * - Viewed by Power User and current row/user is NOT a godadmin
			 * - Viewed by Power User and current row/user is NOT my self (profile_only_pwd = true)
			 */
			if (Yii::app()->user->getIsGodadmin() || (!CoreUser::model()->isUserGodadmin($user->idst) &&
					(Settings::get('profile_only_pwd', 'off') == 'off') || Yii::app()->user->id != $user->idst)) {
				echo CHtml::tag('li', array('class' => 'node-action'),
					CHtml::tag('a', array(
						'href' => Docebo::createAppUrl('admin:userManagement/updateUser', array('idst' => $user->idst)),
						'class' => 'node-edit new-user-handler popup-handler',
						'data-modal-title' => Yii::t('standard', '_MOD'),
						'data-modal-class' => 'user-edit',
						'data-after-send' => 'editUserAfterSubmit(data);',
						'rel' => 'tooltip',
						'title' => Yii::t('standard', '_MOD'),
					), '<i class="fa fa-edit"></i>'.Yii::t('standard', '_MOD'))
				);
			}
		}

		if ($permissions['del']) {

			$userIsNotChildOfSalesforceNode = true;
			foreach ($user->orgChartGroups as $orgChartGroup) {
				$parentNodeName =
					CoreOrgChart::model()->findAll(
						'id_dir = :id_dir and lang_code = :lang_code',
						array(
							':id_dir' => $orgChartGroup->idOrg,
							':lang_code' => 'english'
						)
					)[0]->translation;
				if ($parentNodeName == 'Salesforce') {
					$userIsNotChildOfSalesforceNode = false;
					break;
				}

				$ancestorNodes = $orgChartGroup->getAncestorsNodes($orgChartGroup);
				foreach ($ancestorNodes as $ancestorNode){
					$ancestorNodeId = $ancestorNode->idOrg;

					$ancestorNodeName =
						CoreOrgChart::model()->findAll(
							'id_dir = :id_dir and lang_code = :lang_code',
							array(
								':id_dir' => $ancestorNodeId,
								':lang_code' => 'english'
							)
						)[0]->translation;
					if ($ancestorNodeName == 'Salesforce'){
						$userIsNotChildOfSalesforceNode = false;
						break 2;
					}
				}
			}

			/**
			 *
			 * Show it in these scenarios:
			 * - Viewed by Godadmin
			 * - Viewed by Power User and current row/user is NOT a godadmin
			 */
			if (
					Yii::app()->user->id != $user->idst
					&&
					(Yii::app()->user->getIsGodadmin() || !CoreUser::isUserGodadmin($user->idst))
					&&
					Settings::get('whitelabel_menu_userid', '') != $user->idst
					&&
					$userIsNotChildOfSalesforceNode
				) {
				echo CHtml::tag('li', array('class' => 'node-action'),
						CHtml::link('<i class="fa fa-close"></i>'.Yii::t("standard", "_DEL"), '', array(
							'class' => 'node-delete ajaxModal',
							'data-toggle' => 'modal',
							'data-modal-class' => 'delete-node',
							'data-modal-title' => Yii::t("standard", "_DEL"),
							'data-buttons' => '[
								{"type": "submit", "title": "' . Yii::t("standard", "_CONFIRM") . '"},
								{"type": "cancel", "title": "' . Yii::t("standard", "_CANCEL") . '"}
							]',
							'data-url' => implode('&', array(
								'userManagement/deleteUser',
								'idst=' . $user->idst
							)),
							"data-after-loading-content" => "hideConfirmButton();",
							"data-after-submit" => "updateUserContent",
							'modal-request-type' => 'GET',
							'modal-request-data' => CJSON::encode(array()),
							'rel' => 'tooltip',
						)
					)
				);
			}
		}

		?>
	</ul>
</div>