<style>
	#user-import-form label {
		vertical-align: baseline;
		padding-right: 10px;
	}
	.import form .row > div{
		padding-left: 180px;
	}

	.row-border > label {
		vertical-align: middle !important;
	}
</style>
<div class="main-section import">
	<h3 class="title-bold"><?php echo Yii::t('organization_chart', 'Manage users via CSV'); ?></h3>

	<?php
	/* @var $form CActiveForm */
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'user-import-form',
		'htmlOptions' => array(
			'enctype' => 'multipart/form-data',
		),
	));
	?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row has-border">
		<div class="row-border clearfix">
			<?php echo $form->labelEx($model, 'file'); ?>
			<?php echo $form->fileField($model, 'file'); ?>
			<div>
				<span class="no-file"></span>
				<span class="file-name"></span>

				<div>(Max. <?php echo $model->getMaxFileSize('MB'); ?> Mb)</div>
			</div>
			<div class="download-file"><span><span
						class="download-icon"></span><?php echo CHtml::link(Yii::t('standard', '_DOWNLOAD'), Yii::app()->createUrl($this->id . '/importUser', array('download' => 'sample'))); ?></span> <?php echo Yii::t('standard', '_A_SAMPLE_CSV_FILE'); ?>
			</div>
		</div>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'separator'); ?><?php echo $form->radioButtonList($model, 'separator', array(
			'auto' => Yii::t('standard', '_AUTODETECT'),
			'comma' => ',',
			'semicolon' => ';',
			'manual' => Yii::t('standard', '_MANUAL') . ':',
		), array(
			'separator' => '',
		)); ?>
		<?php echo $form->textField($model, 'manualSeparator'); ?>

		<div>
			<?php echo $form->checkBox($model, 'firstRowAsHeader'); ?>
			<?php echo $form->labelEx($model, 'firstRowAsHeader'); ?>
		</div>
	</div>

	<div class="row has-border">
		<div class="row-border clearfix">
			<?php echo $form->labelEx($model, 'charset'); ?>
			<?php echo $form->dropDownList($model, 'charset', UserImportForm::getDropDownCharsets()); ?>
		</div>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'node'); ?>
		<?php
		$onlyLeafs = false;
		if (Yii::app()->user->isAdmin) {
			$event = new DEvent($this, array());
			Yii::app()->event->raise('PowerUserOrgChartGeneration', $event);

			if (!$event->shouldPerformAsDefault())
				$onlyLeafs = true;
		}
		$indents = ($onlyLeafs ? false : true);
		$orgChartsInfo = CoreOrgChartTree::getFullOrgChartTree($indents, true, true);

		//if only leafs, remove all else except the assigned leaf
		if (!$onlyLeafs) {
			$fake_node = new CoreOrgChartTree();
			$fake_node->idOrg = '-1';
			$fake_node->coreOrgChart = new CoreOrgChart();
			$fake_node->coreOrgChart->translation = Yii::t('preassessment', '_DO_NOTHING');
			array_unshift($orgChartsInfo['list'], $fake_node);
		} else {
			foreach ($orgChartsInfo['list'] as $i => $tmpOrg) {
				if (!in_array($tmpOrg->idOrg, $orgChartsInfo['leafs']))
					unset($orgChartsInfo['list'][$i]);
			}
		}

		if (Yii::app()->user->isAdmin) {
			//when using a power user, make sure that non-assigned nodes cannot be selected
			$puBranches = CoreOrgChartTree::getPuBranchIds();
			echo CHtml::tag('select', array('encode' => false, 'id' => 'UserImportForm_node', 'name' => 'UserImportForm[node]'), false, false);
			foreach (CHtml::listData($orgChartsInfo['list'], 'idOrg', 'coreOrgChart.translation') as $idOrg => $translation) {
				$optionParams = array('value' => $idOrg);
				if ($idOrg != -1 && !in_array($idOrg, $puBranches)) {
					$optionParams['disabled'] = true;
				} //not assigned PU node, but showed anyway: must not be selectable
				if ($idOrg != -1 && in_array($idOrg, $puBranches) && $idOrg == Yii::app()->session['currentNodeId']) {
					$optionParams['selected'] = true;
				}
				echo CHtml::tag('option', $optionParams, $translation);
			}
			echo CHtml::closeTag('select');
		} else {
			//god admins have no limitations for nodes visibility
			echo $form->dropDownlist($model, 'node', CHtml::listData($orgChartsInfo['list'], 'idOrg', 'coreOrgChart.translation'), array('encode' => false));
		}
		?>
	</div>

	<div class="row has-border">
		<div class="row-border clearfix">
			<?php echo $form->labelEx($model, 'passwordChanging'); ?>
			<?php echo $form->radioButtonList($model, 'passwordChanging', array(
				'no' => Yii::t('standard', '_NO'),
				'yes' => Yii::t('standard', '_YES'),
				//'server' => '_CONFIG_BY_SERVER',
				//'nothing' => '_DO_NOTHING',
			), array(
				'separator' => '',
			)); ?>
		</div>
	</div>

	<div class="row">
			<?php echo $form->labelEx($model, 'autoAssignBranches'); ?>
			<?php echo $form->checkBox($model, 'autoAssignBranches'); ?>
	</div>


	<div class="row btns">
		<?php echo CHtml::link(Yii::t('standard', '_CANCEL'), array('userManagement/index'), array('class' => 'btn close-btn')); ?>
		<?php echo CHtml::submitButton(Yii::t('standard', '_NEXT'), array('class' => 'btn confirm-btn')); ?>
	</div>

	<?php $this->endWidget(); ?>

</div>
<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input,select').styler({browseText: '<?=Yii::t('course_management', 'CHOOSE FILE')?>'});
		});
	})(jQuery);
</script>
