<?php

	// We are using Fancytree in Table view: lets define the template
	$tableTemplate = '	
	<table id="'.$id.'" width="100%">
    	<colgroup>
    		<col></col>
    		<col></col>
    		<col></col>
	    	<col width="25"></col>
    		<col width="41" align="right"></col>
    	</colgroup>
    	<thead>
	        <tr> 
	        	<th></th>
	        	<th></th> 
	        	<th></th> 
	        	<th></th> 
	        	<th></th> 
	        </tr>
	    </thead>
	    <tbody>
	    </tbody>
	</table>	
	';


	$lazyMode = true;
	$startingNodeId = 0; // We always start from 0
	$fancyTreeData = CoreOrgChartTree::buildFancytreeDataArrayV2(false, true, true, $lazyMode, $startingNodeId, true);
	$treeGenerationTime = CoreRunningOperation::getCurrentTimeMicroseconds();
	$extensions =  array('docebo','filter', 'persist', 'table');
	if (Yii::app()->user->isGodAdmin) {
		$extensions[] = 'dnd';
	}
	$this->widget('common.extensions.fancytree.FancyTree', array(
		'id' => $id,
		'view'	=> 'index',	
		'skin' => 'skin-docebo-userman',
		'treeData' => $fancyTreeData,
		'preSelected' => $preselected,
		'tableTemplate' => $tableTemplate,	
		// http://www.wwwendt.de/tech/fancytree/doc/jsdoc/global.html#FancytreeOptions
		'fancyOptions' => array(
			//'autoCollapse'	=> true,
			'checkbox' 	=> false,
			'icons'		=> true,
			'extensions' => $extensions,
			'persist' => array(
				'expandLazy' => true, 
				'store' => 'cookie', 
				'types' => 'expanded active focus selected',
				'cookiePrefix' => 'userman-fancytree-',	
				'cookie' => array(
					'path' => '/'
				),
			),

			//some parameters to handle customized orgchart reloading
			'docebo' => '{
				reloadTreeDialog: {
					title: '.CJSON::encode(Yii::t('organization_chart', 'Organization chart update')).',
					text: '.CJSON::encode(Yii::t('organization_chart', 'Organization chart has changed and needs to be updated')).',
					button: '.CJSON::encode(Yii::t('standard', '_CONFIRM')).'
				},
				reloadTreeHandler: function(ctx, source) { UserMan.reloadTreeHandler.call(UserMan, ctx, source); }
			}',

			// Drag & Drop extension options; again, all the work is done in UserMan	
			'dnd' => '{
				draggable		: {
					handle: ".userman-drag-handler"
				},
				dragStart		: function(node,data) {return UserMan.dnd_dragStart(node,data);},
				dragEnter		: function(node,data) {return UserMan.dnd_dragEnter(node,data);},
				dragDrop		: function(node,data) {return UserMan.dnd_dragDrop(node,data);},
				preventVoidMoves: true,
				preventRecursiveMoves: true,
				autoExpandMS	: 1000
			}',
				
			'renderColumns' => 'function(event,data) {UserMan.treeRenderColumns(event,data);}',	
				
		),
	
		'htmlOptions'	=> array(
			'class' => '',
		),
	
		// http://www.wwwendt.de/tech/fancytree/doc/jsdoc/global.html#FancytreeEvents
		'eventHandlers' => array(
			'activate' 			=> 'function(event,data){ UserMan.onBranchActivate(event,data); }',
			
			'click'				=> 'function(event,data) { return UserMan.onFancyTreeClick(event,data);}',
			
			'beforeActivate' 	=> 'function(event,data) { return UserMan.beforeFancyTreeNodeActivate(event,data);}',
			
			'lazyLoad' 	=> 'function(event,data) {
				data.result = UserMan.fancyChartTreeLazyLoaderAjaxObject(event, data);
			}',
			
		),
	));

echo CHtml::hiddenField('tree-generation-time', $treeGenerationTime, array('id' => 'tree-generation-time', 'data-reference-tree' => $id));

?>
