<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('menu', '_USER_MANAGMENT') => array('userManagement/index'),
	Yii::t('organization_chart', '_ORG_CHART_IMPORT_USERS'),
); ?>

<?php //$this->renderPartial('/common/_mainActions'); ?>

<div class="main-section import">

	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'user-import-form-step2',
	)); ?>

		<h3 class="title-bold"><?php echo Yii::t('standard', 'Import users'); ?></h3>

        <div class="user-import-options">
		    <div class="user-import-options-border clearfix">
			    <h4>Options</h4>
                <div class="user-import-options-check">
                    <? /* <div class="clearfix">
			            <?php echo $form->checkBox($model, 'sendAlertToUsers'); ?>
                        <div class="user-import-options-label">
			                <?php echo $form->labelEx($model, 'sendAlertToUsers'); ?>
                        </div>
                    </div> */ ?>
                    <div class="clearfix">
			            <?php echo $form->checkBox($model, 'insertUpdate'); ?>
                        <div class="user-import-options-label">
			                <?php echo $form->labelEx($model, 'insertUpdate'); ?>
                        </div>
                    </div>
                </div>
            </div>
		</div>

		<h3 class="title-bold"><?php echo Yii::t('organization_chart', '_IMPORT_MAP'); ?></h3>

	<?php echo $form->errorSummary($model); ?>

	<?php foreach ($columns as $i => $column) { ?>

		<?php $columns[$i]['header'] = CHtml::dropDownList('import_map['.$column['name'].']', $model->importMap[$i], UserImportForm::getImportMapList()).'<p>'.$column['header'].'</p>';  ?>

	<?php } ?>

		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'users-management-import-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
			'dataProvider' => $model->dataProvider(),
			'columns' => $columns,
			'template' => '<div class="table-scroll">{items}</div>{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'pager' => array(
				'class' => 'DoceboCLinkPager',
				'maxButtonCount' => 8,
			),
			'beforeAjaxUpdate' => 'function(id, options) {
				options.type = "POST";
				options.data = $("#user-import-form-step2").serialize();
			}',
			'afterAjaxUpdate' => 'function() {
                                   // $("#user-import-form-step2").jCleverAPI("refresh");
                                  //$("select").hide();
								  $("select").styler();
								  autoSelectWidth();
                                  $(".table-scroll").jScrollPane({autoReinitialise: true});
            }'
		)); ?>

		<div class="row btns">
			<?php echo CHtml::link(Yii::t('standard', '_CANCEL'), array('userManagement/index'), array('class' => 'btn close-btn')); ?>
            <?php echo CHtml::submitButton(Yii::t('standard', 'Submit'), array('class' => 'btn confirm-btn')); ?>
		</div>

	<?php $this->endWidget(); ?>


</div>
<script type="text/javascript">
	(function ($) {
		$(function () {
            $('input, select').styler();
            autoSelectWidth();
			$('.table-scroll').jScrollPane({
				autoReinitialise : true
			});




	//		$('#user-import-form-step2').jClever(
//				{
//					applyTo : {
//						select : true,
//						checkbox : false,
//						radio : false,
//						button : false,
//						file : false,
//						input : false,
//						textarea : false
//					}
//				}
//			);
		});
	})(jQuery);
</script>
