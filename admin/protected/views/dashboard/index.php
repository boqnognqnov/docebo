<?php
/* @var $filtersForm CFormModel */

/* @var $form CActiveForm */

$this->breadcrumbs[(Yii::app()->user->getIsPu() ? Yii::t('standard', '_PUBLIC_ADMIN_USER') : Yii::t('menu', 'Admin'))] = Docebo::createAppUrl('admin:dashboard/index');
$this->breadcrumbs[] = Yii::t('menu', '_DASHBOARD');

?>
<style>
	div.pull-left>label{
		display: inline-block;
		margin-right: 10px;
	}
</style>
<?php $this->renderPartial('_actions', array('showGoogleImport'=>$data['showGoogleImport']));
if($filtersForm->filterBy != 'dataRange'){
	$filtersForm->dateFrom = null;
	$filtersForm->dateTo = null;
}
?>
<input type="hidden" value="<?=$filtersForm->filterBy;?>" id="previousChoice"/>

<!--<div class="actions">

</div>-->

<div id="dashboard-wrapper">

	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'dashboard-filters-form',
		'method' => 'POST',
		'htmlOptions' => array(
			'class' => 'ajax docebo-form form-horizontal',
			'style' => 'width:94.5%',
		),
	));
	?>
	<hr/>
	<div class="row-fluid">
		<div class="pull-left" style="padding: 5px;">
			<h3 class="title-bold"
				style="float: left;line-height: 30px;padding-right: 20px;"><?= Yii::t('dashboard', 'Filter by date') ?>
			</h3>

			<div class="pull-left" style="position: relative;top:6px;right: 10px;margin-bottom: 10px">
				<label>
					<?= $form->radioButton($filtersForm, 'filterBy', array('value' => 'begin', 'uncheckValue' => NULL)); ?>
					<?= Yii::t('standard', 'From the beginning') ?>
				</label>
				<label>
					<?= $form->radioButton($filtersForm, 'filterBy', array('value' => 'thisMonth', 'uncheckValue' => NULL)); ?>
					<?= Yii::t('standard', 'This month') ?>
				</label>
				<label>
					<?= $form->radioButton($filtersForm, 'filterBy', array('value' => 'lastMonth', 'uncheckValue' => NULL)); ?>
					<?= Yii::t('standard', 'Last month') ?>
				</label>
				<label>
					<?= $form->radioButton($filtersForm, 'filterBy', array('value' => 'dataRange', 'checked' => 'checked', 'uncheckValue' => NULL)); ?>
					<?= Yii::t('report', '_TIME_BELT') ?>
				</label>
			</div>
		</div>
		<div class="pull-left" style="background-color: #f1f3f2;border: 1px solid #e4e6e5;padding: 5px">
			<div class="input-wrapper date dates pull-left" id="dashboard-filter-date-from">
				<?= ucfirst(Yii::t('standard', '_FROM')) ?>
				<?= $form->textField($filtersForm, 'dateFrom', array('style' => 'width:70px;')) ?>
				<i class="add-on i-sprite is-calendar-date"></i>
			</div>
			<div class="input-wrapper date dates pull-left" id="dashboard-filter-date-to">
				&nbsp;&nbsp;&nbsp;<?= Yii::t('standard', '_TO') ?>&nbsp;&nbsp;
				<?= $form->textField($filtersForm, 'dateTo', array('style' => 'width:70px;')) ?>
				<i class="add-on i-sprite is-calendar-date"></i>
			</div>

			<div class="pull-left" style="margin-left: 20px;padding-top: 5px;">
				<i class="i-sprite is-check green" style="cursor: pointer"></i>
				<i class="i-sprite is-remove red" style="cursor: pointer"></i>
				<input class="btn-docebo green big" type="submit" value="<?= Yii::t('standard', '_CONFIRM') ?>"
					   style="display: none"/>
				<input class="btn-docebo black big btn-reset-date-filters" type="reset"
					   value="<?php echo Yii::t('standard', '_RESET'); ?>" style="display: none"/>
			</div>
		</div>
	</div>

	<?php $this->endWidget(); ?>
	<hr style="width: 94.5%"/>

	<div id="top-left" class="section top">
		<?php if (!empty($blocks['top-left'])) {
			foreach ($blocks['top-left'] as $block) {
				$this->renderPartial('blocks/' . $block, array('data' => $data));
			}
		} ?>
	</div>
	<div id="top-right" class="section top">
		<?php if (!empty($blocks['top-right'])) {
			foreach ($blocks['top-right'] as $block) {
				$this->renderPartial('blocks/' . $block, array('data' => $data));
			}
		} ?>
	</div>

	<div id="middle" class="section">

		<? Yii::app()->event->raise('BeforeAdminDashboard', new DEvent($this, array(
			'filtersForm' => $filtersForm
		))); ?>

		<?php $this->renderPartial('blocks/recentActivities', array('data' => $data['stats'], 'colors' => $colors)); ?>
	</div>

	<?php if(PluginManager::isPluginActive('Share7020App')): ?>
	<div id="middle" class="section">
		<?php $this->renderPartial('blocks/coachShareOverview', array('data' => $data['coachShareStats'], 'colors' => $colors)); ?>
	</div>
	<?php endif ;?>
	
	<div id="bottom-left" class="section bottom">
		<?php
		if (!empty($blocks['bottom-left'])) {
			foreach ($blocks['bottom-left'] as $block) {
				$this->renderPartial('blocks/' . $block, array('data' => $data));
			}
		}
		$event = new DEvent($this, array('filtersForm' => $filtersForm));
		Yii::app()->event->raise('OnRenderAdminDashboardBottomLeft', $event);
		?>
	</div>

	<div id="bottom-right" class="section bottom">
		<?php
		if (!empty($blocks['bottom-right'])) {
			foreach ($blocks['bottom-right'] as $block) {
				$this->renderPartial('blocks/' . $block, array('data' => $data));
			}
		}
		$event = new DEvent($this, array('filtersForm' => $filtersForm));
		Yii::app()->event->raise('OnRenderAdminDashboardBottomRight', $event);
		?>
	</div>
</div>
<!--[if IE & (lt IE 9)]>
<script src="<?=$assetsPath?>/charts/r2d3.js" charset="utf-8"></script>
<![endif]-->

<!--[if IE & (gte IE 9)]>
<script src="<?=$assetsPath?>/charts/d3.v3.min.js" charset="utf-8"></script>
<script src="<?=$assetsPath?>/charts/aight.d3.min.js" charset="utf-8"></script>
<![endif]-->
<!--[if !IE]> -->
<script src="<?=$assetsPath?>/charts/d3.v3.min.js" charset="utf-8"></script>
<script src="<?=$assetsPath?>/charts/aight.d3.min.js" charset="utf-8"></script>
<!-- <![endif]-->
<script type="text/javascript">
$(function(){
	try{
        // Attach Date Pickers
        $('#dashboard-filter-date-from').bdatepicker({format: 'yyyy-mm-dd'});
        $('#dashboard-filter-date-to').bdatepicker({format: 'yyyy-mm-dd'});
        $('.btn-reset-date-filters').click(function() {
            var $form = $(this).closest('form');
            $form.find('input[type="text"]').val('');
            $form.submit();
        });

		// Not supported on some IE versions
        $('.make-me-friendly').friendlyText({slideStepBack: 3});
	}catch(e){}

	$('input').styler();

	$('i.i-sprite.is-check').click(function(){
		$('input.btn-docebo[type="submit"]').trigger('click');
	});
	$('i.i-sprite.is-remove').click(function(){
		$('input.btn-reset-date-filters[type="reset"]').trigger('click');
	});

	$('input[name="DashboardFiltersForm[filterBy]"]').next().click(function(){
		var val = $(this).prev().val();
		if(val == 'dataRange'){
			$('#dashboard-filter-date-from').css('opacity',1);
			$('#dashboard-filter-date-from').find('input').prop('disabled',false);
			$('#dashboard-filter-date-to').css('opacity',1);
			$('#dashboard-filter-date-to').find('input').prop('disabled',false);
			$('.add-on.i-sprite').addClass('is-calendar-date');

		} else {
			$('#dashboard-filter-date-from').css('opacity', 0.4);
			$('#dashboard-filter-date-from').find('input').prop('disabled', true);
			$('#dashboard-filter-date-to').css('opacity', 0.4);
			$('#dashboard-filter-date-to').find('input').prop('disabled', true);

			$('.add-on.i-sprite.is-calendar-date').removeClass('is-calendar-date').css('width', '20px');
			var previousChoice = $('#previousChoice').val();
			if (val != previousChoice) {
				$('input.btn-docebo[type="submit"]').trigger('click');
			}
		}
	});

	// click on the selected filter
	var filter = '<?=$filtersForm->filterBy?>';
	if (filter != undefined)
		$('input[value="' + filter + '"]').next().trigger('click');

});
</script>