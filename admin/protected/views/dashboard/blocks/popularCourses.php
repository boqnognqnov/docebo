<div class="item narrow most-popular-courses" data-template="popularCourses">
	<h3 class="title-bold"><?php echo Yii::t('dashboard', 'Most popular courses'); ?><span class="move">Move</span></h3>
	<div class="content">
		<div class="clearfix">
			<div class="popular-courses-icon"></div>
			<div class="bars">
				<ul>
					<?php foreach ($data['stats']['popular_courses'] as $i => $course): ?>
						<li>
							<div class="bar bar-<?php echo ($course['positionInTopThree']); ?>">
								<p><?php echo $course['name']; ?></p>
							</div>
							<div class="info">
								<span class="enrolled-count"><?php echo $course['totalCount']; ?></span>
								<span class="trend <?php echo $course['trendClass']; ?>"></span>
								<p><?php echo Yii::t('standard', 'Enrolled'); ?></p>
							</div>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
	</div>
</div>