<div class="item narrow" data-template="statuses">
	<h3 class="title-bold"><?php echo Yii::t('dashboard', 'Course and Students Status'); ?><span class="move"><?= Yii::t('standard', '_MOVE') ?></span></h3>
	<div class="content">
		<div>
			<?php
			$colors = Yii::app()->theme->getColors();

			$legends = array();

			$donutDatas = array(
				array(
					'initRadius' => 40,
					'thickness' => 20,
					'boxSize' => 221,
					'data' => array(
						array('color' => array($colors['@charts3'])),
						array('color' => array($colors['@charts2'])),
						array('color' => array($colors['@charts1'])),
					),
				),
				array(
					'initRadius' => 63,
					'thickness' => 20,
					'boxSize' => 221,
					'data' => array(
						//array('color' => array($colors['charts_3'])),
						array('color' => array($colors['@charts2'])),
						array('color' => array($colors['@charts1'])),
					),
				),
			);

			$j = 0;
			foreach ($data['stats']['statuses'] as $type => $values) {
				$i = 1;
				$legendContent = '';
				foreach ($values as $key => $value) {
					$donutDatas[$j]['data'][$i-1] += array(
						'color' => array_push($donutDatas[$j]['data'][$i-1]['color'], CoreSchemeColor::CHART_BACKGROUND_COLOR),
						'data' => array($value, 100 - $value),
					);

					$legendContent .= '<div class="legend-field'.$i.'">';
					$legendContent .= '<div class="legend-count">'. $value['count'] .'</div>';
					$legendContent .= '<div class="legend-percent">' . $value . '<span>%</span></div>';

					/* Added to display translated labels */
					if ($key == 'closed') {
						$key = Yii::t('standard', 'Closed');
					} elseif ($key == 'active') {
						$key = Yii::t('standard', '_ACTIVE');
					} elseif ($key == 'under_maintenance') {
						$key = Yii::t('standard', 'Unpublished');
					} elseif ($key == 'not_yet_started') {
						$key = Yii::t('standard', 'To start');
					} elseif ($key == 'in_progress') {
						$key = Yii::t('standard', '_USER_STATUS_BEGIN');
					} elseif ($key == 'completed') {
						$key = Yii::t('standard', '_USER_STATUS_END');
					}
					/* /Added to display translated labels */

					$legendContent .= '<div class="legend-title">' . str_replace('_', ' ', $key) . '</div><hr>';
					$legendContent .= '</div>';
					$i++;
				}
				$legends[$j] = $legendContent;
				$j++;
			}
			?>

			<div class="status-wrapper clearfix">
				<div class="student-status-content">
					<div class="donut-wrapper">
						<?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/course-status.png');?>
						<div class="donutContainer" id="donut-<?php echo uniqid(); ?>" data-dataset='<?php echo json_encode($donutDatas[1]); ?>'></div>
					</div>
					<div class="summary-legend-wrapper">
						<?php echo $legends[1]; ?>
					</div>
				</div>
				<div class="course-status-content">
					<div class="donut-wrapper">
						<?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/student-status.png');?>
						<div class="donutContainer" id="donut-<?php echo uniqid(); ?>" data-dataset='<?php echo json_encode($donutDatas[0]); ?>'></div>
					</div>
					<div class="summary-legend-wrapper">
						<?php echo $legends[0]; ?>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>