<div class="item narrow alerts" data-template="alerts">
	<h3 class="title-bold"><?php echo Yii::t('standard', 'Warnings'); ?><span class="move"><?php echo Yii::t('standard', '_MOVE'); ?>></span></h3>
	<div class="content">
		<div>
			<table width="100%">
				<tbody>
					<tr>
						<td>
							<table>
								<tbody>
									<tr>
										<td>
											<table>
												<tbody>
													<tr>
														<td class="number-of-courses">
														<?php echo $data['stats']['alerts']['emptyEnrollments']; ?>
														</td>
														<td class="courses-description">
														<?php echo Yii::t('dashboard', 'Course with no enrollments'); ?>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
										<td>
											<table>
												<tbody>
													<tr>
														<td class="number-of-courses">
														<?php echo $data['stats']['alerts']['noTrainingResources']; ?>
														</td>
														<td class="courses-description">
														<?php echo Yii::t('dashboard', 'Course with no materials'); ?>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
										<td>
											<table>
												<tbody>
													<tr>
														<td class="number-of-courses">
														<?php echo $data['stats']['alerts']['noCompletion']; ?>
														</td>
														<td class="courses-description">
														<?php echo Yii::t('dashboard', 'Course with no completion'); ?>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>