<!--topBestContributors-->
<?php if(PluginManager::isPluginActive('Share7020App') && !empty($data['coachShareStats']['topContributors'])): ?>
<div class="item narrow best-contributors" data-template="topBestContributors">
	<h3 class="title-bold"><?php echo Yii::t('report', 'Top {count} best contributors',array('{count}' => 3)); ?><span class="move">Move</span></h3>
	<div class="content">
		<div>
			<ul>
				<?php
                foreach ($data['coachShareStats']['topContributors'] as $contributor): ?>
					<li>
						<div class="left">
							<div class="avatar-contributor">
								<?php echo $contributor['avatar']; ?>
							</div>
						</div>
						<div class="center">
							<p class="title"><a href="<?php echo Docebo::createApp7020Url('channels/index', array("#" => '/pchannel/'.$contributor["id"])); ?>"><?php echo $contributor['name']; ?></a></p>
						</div>
						<div class="right">
							<p class="count"><?php echo $contributor['totalCount']; ?></p>
							<p class="countSuffix"><?php echo Yii::t('report', 'contributions') ?></p>
						</div>
					</li>
				<?php
                endforeach;
                ?>
			</ul>
		</div>
	</div>
</div>
<?php endif ;?>