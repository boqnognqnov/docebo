<?php if (Yii::app()->user->getIsGodadmin() || (Yii::app()->user->getIsPu() && Yii::app()->user->checkAccess('/lms/admin/report/view'))) : ?>
    <?php
    /* @var $model LearningReportFilter */
    if(Yii::app()->user->getIsPu()) {
	    $criteria = new CDbCriteria;
	    // allowed reports to see with "selection" access filter
	    $allReportsIds = Yii::app()->db->createCommand()
		    ->select('id_filter')
		    ->from(LearningReportFilter::model()->tableName())->queryColumn();
	    $targetIds = array();
	    foreach ($allReportsIds as $reportId) {
		    if (LearningReportAccess::checkPermissionToEdit(Yii::app()->user->id, $reportId) === true)
			    $targetIds[] = $reportId;
	    }

	    $criteria->join = 'JOIN ' . LearningReportAccess::model()->tableName() . ' lrv ON t.id_filter = lrv.id_report';
	    $criteria->addCondition('t.author = :author OR lrv.visibility_type = :type OR(lrv.visibility_type = :selection AND t.`id_filter` IN(' . "'" . implode("', '", $targetIds) . "'" . '))');
	    $criteria->params[':type'] = LearningReportAccess::TYPE_PUBLIC;
	    $criteria->params[':author'] = Yii::app()->user->id;
	    $criteria->params[':selection'] = LearningReportAccess::TYPE_SELECTION;
    }else{
	    $criteria = '';

    }

    $reports =LearningReportFilter::model()->findAll($criteria);
    ?>
    <div class="item narrow generate-reports" data-template="generateReports">
        <h3 class="title-bold"><?php echo Yii::t('standard', '_REPORTS') ?><span class="move">Move</span></h3>
        <div class="content">
            <div>
                <?php echo CHtml::label(Yii::t('standard', '_VIEW'), 'LearningReportFilter_id_filter', array(
                    'class'=>'no-pointer'
                )); ?>
                <?php echo CHtml::dropDownList('LearningReportFilter[id_filter]', '', CHtml::listData($reports, 'id_filter', function($row) {
                    return CHtml::encode( Yii::t('report', $row->filter_name) );
                }), array('empty' => Yii::t('report', 'Select report...'))); ?>

                <div class="links">
                <span>
				    <a href="javascript:void(0);" class="view-report"><?php echo Yii::t('standard', '_VIEW'); ?></a>
                </span>
                <span class="popover-container">
				    <a href="javascript:void(0);" class="export-report popover-trigger"><?php echo Yii::t('standard', '_EXPORT'); ?></a>
                </span>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('select').styler();
    </script>
<?php endif; ?>