<!--topMostWatchedAssets-->
<?php if(PluginManager::isPluginActive('Share7020App') && !empty($data['coachShareStats']['topWatchedAssets'])): ?>
<div class="item narrow most-watched-assets" data-template="topMostWatchedAssets">
	<h3 class="title-bold"><?php echo Yii::t('report', 'Top {count} most watched assets',array('{count}' => 3)); ?><span class="move">Move</span></h3>
	<div class="content">
		<div>
			<ul>
				<?php
                    foreach ($data['coachShareStats']['topWatchedAssets'] as $asset):
                        ?>
                        <li>
							<div class="left">
								<div class="avatar-asset">
									<img src="<?php echo $asset['thumb']; ?>" alt="">
								</div>
							</div>
							<div class="center">
								<p class="title"><a href="<?php echo Docebo::createApp7020AssetsViewUrl($asset['id'])?>"><?php echo $asset['title']; ?></a></p>
							</div>
							<div class="right">
								<p class="count"><?php echo $asset['totalCount']; ?></p>

								<p class="countSuffix"><?php echo Yii::t('report', 'views') ?></p>
							</div>
                        </li>
                    <?php endforeach; ?>
			</ul>
		</div>
	</div>
</div>
<?php endif ;?>