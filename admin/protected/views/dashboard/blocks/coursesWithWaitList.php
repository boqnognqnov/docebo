<div class="item narrow courses-with-waitlist" data-template="coursesWithWaitList">
	<h3 class="title-bold"><?php echo Yii::t( 'dashboard', 'Courses with waiting users' ); ?>
		<span class="move">
			<?php echo Yii::t('standard', '_MOVE'); ?>
		</span>
	</h3>

	<div class="content">
		<div>

			<?php if (isset($data['stats']) && !empty($data['stats']['courses_with_waitlist'])): ?>

				<ul>
					<?php foreach ( $data['stats']['courses_with_waitlist'] as $key => $course ): ?>
						<?php $class = ''; ?>
						<?php if( $key == 0 ) {
							$class = 'first';
						} elseif( $key == 100 ) {
							$class = 'last';
						} ?>
						<?php $uniqueId = uniqid(); ?>
						<li class="<?php echo $class; ?>">
							<p class="title make-me-friendly">
								<?php
									$link = '';
									switch($course['course_type']) {
										case LearningCourse::TYPE_CLASSROOM :
											$link = Docebo::createAdminUrl('ClassroomApp/session/assign',array('course_id'=>$course['idCourse']));
											break;
										case LearningCourse::TYPE_WEBINAR :
											$link = Docebo::createAdminUrl('webinar/session/assign',array('course_id'=>$course['idCourse']));
											break;
										case LearningCourse::TYPE_ELEARNING :
											$link = Docebo::createAdminUrl('courseManagement/waitingEnrollment',array('id'=>$course['idCourse']));
											break;
									}
								?>
								<a href="<?= $link ?>">
									<?php echo $course['name']; ?>
								</a>
							</p>

							<p class="percents">
								<span class="ints"><?php echo  $course['waiting'] ?></span>
							</p>

						</li>
					<?php endforeach; ?>
				</ul>

			<?php else: ?>

				<ul>
					<li class="first last">
						<p class="title make-me-friendly">
							<?php echo Yii::t('report', '_NULL_REPORT_RESULT'); ?>
						</p>
					</li>
				</ul>

			<?php endif; ?>

		</div>

	</div>
</div>