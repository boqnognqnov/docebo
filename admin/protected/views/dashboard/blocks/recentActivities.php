<?php
	$titles = array(
		'sessions' => Yii::t('report', '_TH_USER_NUMBER_SESSION'),
		'completions' => Yii::t('standard', 'Course completion'),
		'enrollments' => Yii::t('standard', 'Enrollments'),
	);
?>
<div class="item recent-activities">
	<h3 class="title-bold"><?php echo Yii::t('dashboard', 'Recent activities'); ?></h3>
	<div class="content">
		<div class="clearfix">
			<div class="legend">

				<?php foreach ($data['legends'] as $key => $values): ?>
					<div class="row <?php echo $key ?>">
						<h4><span></span><?php echo $titles[$key]; ?></h4>
						<hr>
						<ul class="clearfix">
							<li>
								<span class="stat-number"><?php echo intval($values['today']); ?></span>
								<span class="period"><?php echo Yii::t('calendar', '_TODAY'); ?></span>
								<span class="trend <?php echo $values['dayTrendClass']; ?>"></span>
							</li>
							<li>
								<span class="stat-number"><?php echo intval($values['thisWeek']); ?></span>
								<span class="period"><?php echo Yii::t('dashboard', '_THIS_WEEK'); ?></span>
								<span class="trend <?php echo $values['weekTrendClass']; ?>"></span>
							</li>
						</ul>
					</div>
				<?php endforeach; ?>
			</div>
			<div class="chart-container">
				<div class="chart-colors">
					<ul>
						<li>
							<span class="color" style="background-color: <?php echo $colors[0]; ?>"></span>
							<span><?php echo $titles['sessions']; ?></span>
						</li>
						<li>
							<span class="color" style="background-color: <?php echo $colors[1]; ?>"></span>
							<span><?php echo $titles['completions']; ?></span>
						</li>
						<li>
							<span class="color" style="background-color: <?php echo $colors[2]; ?>"></span>
							<span><?php echo $titles['enrollments']; ?></span>
						</li>
					</ul>
				</div>
				<div class="chart-wrapper">
					<div id="multiseries-line-chart-<?php echo str_shuffle(round(microtime(true) * 1000)) ?>" class="multiSeriesLineChartContainer" style="height: 240px; width: 750px;" data-dataset='<?php echo json_encode($data['activity']); ?>' data-legend-titles='<?php echo json_encode($data['titles']); ?>' data-chart-colors='<?php echo json_encode($colors); ?>'></div>
				</div>
			</div>
		</div>
	</div>
</div>