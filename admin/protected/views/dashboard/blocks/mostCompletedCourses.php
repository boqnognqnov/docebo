<div class="item narrow most-completed" data-template="mostCompletedCourses">
	<h3 class="title-bold"><?php echo Yii::t('report', 'Best performer'); ?><span class="move">Move</span></h3>
	<div class="content">
		<div>
			<ul>
			<?php foreach ($data['stats']['course_completion']['most'] as $key => $course): ?>
				<?php $class = ''; ?>
				<?php if ($key == 0) {
					$class = 'first';
				} elseif ($key == (count($data['stats']['course_completion']['most']) - 1)) {
					$class = 'last';
				} ?>
				<?php $uniqueId = uniqid(); ?>
				<li class="<?php echo $class; ?>">
					<p class="title make-me-friendly"><?php echo $course['title']; ?></p>
					<p class="percents">
						<span class="ints"><?php echo $course['percent'][0]; ?>.</span>
						<span class="decimals"><?php echo $course['percent'][1]; ?><span class="sign">%</span></span>
					</p>
					<?php
						$donutSingleData = array(
							'initRadius' => 11,
							'thickness' => 5,
							'boxSize' => 35,
							'data' => array(
								array(
									'data' => array($course['completed'], $course['total'] - $course['completed']),
									'color' => array(CoreSchemeColor::CHART_SINGLE_GREEN_COLOR, CoreSchemeColor::CHART_BACKGROUND_COLOR)),
							),
						);
					?>
					<div class="donutContainer" id="donut-single-<?php echo $uniqueId; ?>" data-dataset='<?php echo json_encode($donutSingleData); ?>'></div>
				</li>
			<?php endforeach; ?>
			</ul>
		</div>
	</div>
</div>