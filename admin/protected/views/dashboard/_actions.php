<div class="main-actions dashboard-actions clearfix">
	<h3 class="title-bold"><?php echo Yii::t('menu', '_DASHBOARD'); ?></h3>
	<ul class="clearfix">
		<?php if(Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/lms/admin/report/view')): ?>
		<li>
			<div>
				<?php echo CHtml::link(
					'<span></span>'.Yii::t('report', 'User personal summary'), Docebo::createLmsUrl('myActivities/userReport', array('from' => 'dashboard')), array(
						'class' => 'user-profile',
						'alt' => Yii::t('helper', 'Personal summary')
					)); ?>
			</div>
		</li>
		<?php endif; ?>
		<li>
			<div>
			<?php echo CHtml::link(
				'<span></span>'.Yii::t('profile', '_CHANGEPASSWORD'), Docebo::createLmsUrl('user/editProfile'), array(
					'class' => 'open-dialog change-password',
					'alt' => Yii::t('helper', 'Change password'),
					'data-dialog-class' => 'modal-edit-profile',
					'title' => Yii::t('profile', '_CHANGEPASSWORD'),
				)); ?>
			</div>

		</li>
		<?php if(Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/usermanagement/add')): ?>
		<li>
			<div>
			<?php echo CHtml::link(
				'<span></span>'.Yii::t('standard', '_NEW_USER'), Yii::app()->createUrl('userManagement/createUser'), array(
					'class' => 'new-user popup-handler',
					'alt' => Yii::t('helper', 'New user'),
					'data-modal-title' => Yii::t('standard', '_NEW_USER'),
					'data-modal-class' => 'new-user',
					'data-after-close' => 'newUserAfterClose();',
					'data-after-send' => 'newUserAfterLoad();',
					'data-before-send' => 'disableModalSubmitButton();',
				)); ?>
			</div>
		</li>
		<?php endif; ?>
		<?php if(Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('enrollment/create')): ?>
			<li>
				<div>
					<?php
					$title 	= '<span></span>'.Yii::t('standard', 'Enroll users');

					// Dialog content is taken from this action
					$url = Docebo::createAdminUrl('courseManagement/axEnrollUsersToManyCourses', array('include_grid_scripts'=>1));

					echo CHtml::link($title, $url, array(
						'data-dialog-class' => 'users-selector',
						'class' => 'open-dialog course-enrollment',
						'rel' 	=> 'mass-enrollment-wizard',
						'alt' => Yii::t('helper', 'Enroll users'),
						'data-dialog-title' => Yii::t('standard', 'Enroll users'),
						'title'	=> Yii::t('standard', 'Enroll users')
					));
					?>
					<script type="text/javascript">
						//<![CDATA[
						// jWizard Options
						var options = {
							dialogId: 'mass-enrollment-wizard',
							alwaysPostGlobal: true,
							dispatcherUrl: '<?= $url ?>'  // All Wizard requests go here
						};
						var jwizardMassEnrollment = new jwizardClass(options);

						// Upon closing (with CONFIRM, i.e. on SUCCESS), jWizard fires a JS event
						$(document).on('jwizard.closed.success', '', function(e, data){
							//$.fn.yiiGridView.update('course-management-grid');
						});
						//]]>
					</script>
				</div>
			</li>
		<?php endif; ?>
		<?php if(Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/usermanagement/add')) : ?>
		<li id="importUsersButtonUserMgmt">
			<div>
				<a href="<?php echo $this->createUrl('userManagement/importUser'); ?>" class="import" alt="<?php echo Yii::t('helper', 'Import users - um'); ?>">
					<span></span><?php echo Yii::t('organization_chart', '_ORG_CHART_IMPORT_USERS'); ?>
				</a>
			</div>
		</li>
		<?php endif; ?>
		<?php if(isset($showGoogleImport) && $showGoogleImport && (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/usermanagement/view'))): ?>
			<li>
				<div>
					<a href="<?=Docebo::createAdminUrl('userManagement/syncGappsUsers')?>" class="import-google" alt="<?php echo Yii::t('helper', 'Import users - googleapps'); ?>">
						<span></span><?php echo Yii::t('user_management', 'Import users from Google Apps'); ?>
					</a>
				</div>
			</li>
		<?php endif; ?>
		<?php if(Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/course/add')): ?>
		<li>
			<div>
			<?php $this->renderPartial('//common/_modal', array(
				'config' => array(
					'class' => 'new-course',
					'modalTitle' => Yii::t('standard', '_NEW_COURSE'),
					'linkTitle' => '<span></span>'.Yii::t('standard', '_NEW_COURSE'),
					'linkOptions' => array(
						'alt' => Yii::t('helper', 'New course'),
					),
					'url' => 'courseManagement/createCourse',
					'buttons' => array(
						array(
							'type' => 'submit',
							'title' => Yii::t('standard', '_CONFIRM'),
							'formId' => 'course_form',
						),
						array(
							'type' => 'cancel',
							'title' => Yii::t('standard', '_CANCEL'),
						),
					),
					'afterLoadingContent' => 'courseFormInit();',
					'beforeSubmit' => 'beforeCourseCreateSubmit',
					'afterSubmit' => 'updateCourseContent',
				),
			)); ?>
			</div>
		</li>
		<?php endif; ?>
	</ul>
	<div class="info">
    <div>
		<h4></h4>
		<p></p>
    </div>
	</div>

</div>

<script>

	$('.modal.new-course a.btn.btn-submit').live('click', function(ev) {
		var btn = $(this);
		btn.addClass('disabled');
		btn.prop('disabled', true);

		var modal = $('.modal.new-course');
		var data = modal.find("form").serializeArray();
		data.push({
			name: 'LearningCourse[description]',
			value: tinymce.activeEditor.getContent()
		});

		$.ajax({
			'dataType': 'json',
			'type': 'POST',
			'url': modal.find('form').attr('action'),
			'cache': false,
			'data': data,
			'success': function (data) {
				$.fn.updateCourseContent(data);
				hideLoading('contentLoading');
				setTimeout(function() {
					btn.removeClass('disabled');
					btn.prop('disabled', false);
				}, 1000);
			}
		});
	})

</script>
