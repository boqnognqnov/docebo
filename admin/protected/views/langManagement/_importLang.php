<?php

/* @var $form CActiveForm */

$form = $this->beginWidget('CActiveForm', array(
	'id' => 'import-language-form',
	'htmlOptions' => array(
		'class' => 'ajax form-horizontal',
		'enctype' => 'multipart/form-data',
	),
)); ?>

<?php if ($error): ?>
<div id="language-import-error-message">
	<div class="alert alert-error in alert-block fade">
		<button type="button" class="close" data-dismiss="alert">x</button>
		<?php echo $error; ?>
	</div>
</div>
<?php endif; ?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->labelEx($model, 'file', array('class' => 'control-label')); ?>
<div class="controls">
	<?php echo $form->fileField($model, 'file'); ?>
	<div style="display: inline-block">
		<span class="no-file"></span>
		<span class="file-name"></span>
		<div>(Max. <?php echo $model->getMaxFileSize('MB'); ?> Mb)</div>
	</div>
</div>

<?php echo $form->labelEx($model, 'replaceIfExist', array('class' => 'control-label')); ?>
<div class="controls">
	<?php echo $form->checkBox($model, 'replaceIfExist'); ?>
</div>


<div class="form-actions">
	<input class="btn-docebo green big" type="submit" value="<?= Yii::t('standard', '_SAVE') ?>" />
	<input class="btn-docebo black big close-dialog" type="reset" value="<?= Yii::t('standard', '_CANCEL') ?>"/>
</div>
<?php $this->endWidget(); ?>
<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input,select').styler({browseText:'<?=Yii::t('course_management', 'CHOOSE FILE')?>'});
		});
	})(jQuery);
</script>