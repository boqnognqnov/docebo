<div class="form-wrapper">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'lang_text_form',
        'htmlOptions' => array('enctype' => 'multipart/form-data')
        )
    );
    ?>
    <div class="languages">
        <div class="input-fields">
            <div class="clearfix">
                <?php echo CHtml::textarea('translation_text', $model->translation_text, array('class' => 'text', 'style' => 'width: 500px  ; height: 120px')); ?>
            </div>
        </div>
    </div>

		<div class="actions right-buttons">
			<?php echo CHtml::submitButton(Yii::t('standard', '_CONFIRM'), array('class' => 'btn btn-save')); ?>
            &nbsp; 
			<?php echo CHtml::submitButton(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel', 'onclick' => '$(\'#popover-area\').html(\'\');return false;')); ?>
		</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('#lang_text_form').submit(function(){

        $.ajax({
            url: $('#lang_text_form').attr('action'),
            data: {CoreLangTranslation: {translation_text: $('#translation_text').val()}},
            type: "POST",
            success: function(){
                $('#lang-management-grid').yiiGridView('update');
            }
        });

        $('#popover-area').html('');
        return false;
    });
});
</script>
<?php $this->endWidget(); ?>