<div class="form" style="width: 350px;">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'lang_form',
        'htmlOptions' => array(
            'class' => 'docebo-modalbox-form'
        ),
    ));
    ?>

    <div class="docebo-form-row docebo-form-type-freetext">
        <label><?php echo Yii::t('standard', '_LANGUAGE') ?>:</label> <strong><?php echo ucfirst($model->lang_code); ?></strong> <img src="<?php echo $model->getFlagSrc(); ?>" alt="<?php echo $model->lang_code; ?>" class="flag">
    </div>
    
    <div class="docebo-form-row docebo-form-type-text">
    	<label><?= $model->getAttributeLabel('lang_browsercode') ?> : <strong><?= $model->lang_browsercode ?></strong></label>
    </div>

    <div class="docebo-form-row docebo-form-type-text">
        <?php echo $form->labelEx($model, 'lang_description'); ?>
        <?php echo $form->textField($model, 'lang_description'); ?>
        <?php echo $form->error($model, 'lang_description'); ?>
    </div>

    <div class="docebo-form-row docebo-form-type-checkboxlist">
        <?php echo $form->labelEx($model, 'lang_direction'); ?>
        <?php echo $form->radioButtonList($model, 'lang_direction', CoreLangLanguage::getDirectionOptions()); ?>
        <?php echo $form->error($model, 'lang_direction'); ?>
    </div>


    <div class="docebo-form-row docebo-form-type-checkbox">
        <?php echo $form->labelEx($model, 'set_as_default'); ?>
        <?php echo $form->checkBox($model, 'set_as_default'); ?>
        <?php echo $form->error($model, 'set_as_default'); ?>
    </div>
<?php $this->endWidget(); ?>
