<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('menu', '_LANG') => Yii::app()->createUrl('langManagement/index'),
	Yii::t('admin_lang', '_TRANSLATELANG'),
); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'group-management-form',
	'htmlOptions' => array(
		'class' => 'ajax-grid-form',
		'data-grid' => '#lang-management-grid'
	),
)); ?>

<div class="main-section lang-management">
	<h3 class="title-bold back-button">
		<?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('langManagement/index')); ?>
		<span><?php echo Yii::t('admin_lang', '_TRANSLATELANG') ?></span>
	</h3>
	<br/>
	<div class="filters-wrapper">
		<table class="filters" width="100%">
			<tbody>
			<tr>
				<td class="table-padding">
					<table class="table-border">
						<tr>
							<td class="group select-lang-module">
								<table>
									<tr>
										<td valign="middle">
											<?php echo Yii::t('admin_lang', '_LANG_MODULE') ?>
										</td>
										<td class="select">
                                            <?php echo $form->dropDownList($filter, 'module', $module_list, array('onChange' => '$(\'#group-management-form\').submit()')); ?>
										</td>
									</tr>
								</table>
							</td>
							<td class="group select-users">
								<table>
									<tr>
										<td valign="middle">
											<?php echo Yii::t('standard', '_LANGUAGE') ?>
										</td>
										<td class="select">
										<?php echo $form->dropDownList($filter, 'langcode', $language_list, array('onChange' => '$(\'#group-management-form\').submit()')); ?>
										</td>
									</tr>
								</table>
							</td>
							<td class="group select-users">
								<table>
									<tr>
										<td valign="middle">
											<?php echo Yii::t('admin_lang', '_LANG_COMPARE') ?>
										</td>
										<td class="select">
										<?php echo $form->dropDownList($filter, 'lang_diff', $language_list_diff, array('onChange' => '$(\'#group-management-form\').submit()')); ?>
										</td>
									</tr>
								</table>
							</td>
							<td class="group advanced-search-box">
								<table>
									<tr>
										<td>
											<div class="input-wrapper">
                                                <?php echo $form->textField($filter, 'search', array('class' => 'typeahead', 'placeholder' => Yii::t('standard', '_SEARCH'))); /* 'id' => 'advanced-search-translate' */ ?>
												<span class="search-icon"></span>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
</div>
<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<div class="bottom-section clearfix">
	<div id="grid-wrapper">
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'lang-management-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
			'dataProvider' => $model->dataProvider(),
			'template' => '{items}{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			/*'pager' => array(
				'class' => 'ext.local.pagers.DoceboLinkPager',
				'pageSize' => 50,
			),*/
			'pager' => array(
				'class' => 'DoceboCLinkPager',
				'maxButtonCount' => 8,
				'pageSize' => 50,
			),
			'ajaxUpdate' => 'all-items',
			'columns' => array(
				array(
                    'header' => CHtml::encode(Yii::t('admin_lang', '_LANG_MODULE')),
                    'name' => 'text_module',
                    'htmlOptions' => array(
                        'width' => '10%',
                    ),
                ),
				array(
                    'header' => CHtml::encode(Yii::t('admin_lang', '_LANG_KEY')),
                    'name' => 'text_key',
                    'type' => 'raw',
                    'htmlOptions' => array(
                        'width' => '30%',
                    ),
                ),
				array(
                    'header' => CHtml::encode(Yii::t('admin_lang', '_LANG_TRANSLATION')),
                    'name' => 'translation_text',
                    'type' => 'raw',
                    'value' => '\'<div rel="\'.$data["id"].\'" langcode="'.$langcode.'" text_key="\'.htmlentities($data["text_key"]).\'" module="\'.$data["text_module"].\'">\'.$data["translation_text"].\'</div>\'',
                    'htmlOptions' => array(
                        'width' => '35%',
                        'class' => 'editable',
                    ),
                ),
				array(
                    'header' => CHtml::encode(Yii::t('admin_lang', '_LANG_COMPARE')),
                    'name' => 'translation_text_diff',
                    'type' => 'raw',
                    'value' => '\'<div rel="\'.$data["id"].\'" langcode="'.$lang_diff.'" text_key="\'.htmlentities($data["text_key"]).\'" module="\'.$data["text_module"].\'">\'.$data["translation_text_diff"].\'</div>\'',
                    'htmlOptions' => array(
                        'width' => '35%',
                        'class' => 'editable',
                    ),
                ),
			),
		)); ?>
	</div>
</div>
<div id="popover-area"></div>

<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input, select').styler();
			replacePlaceholder();
		});

        $(document).ready(function(){
            $('#LangManagementFilter_search').off().keyup(function(){
                typewatch(function () {
                    $('#group-management-form').submit();
                }, 350);
            });

            $('#group-management-form').submit(function(){
                var ajaxGridForm = $(this);
                $(ajaxGridForm.data('grid')).yiiGridView('update', {
                    data: ajaxGridForm.serialize()
                });
                return false;
            });

            var typewatch = (function(){
                var timer = 0;
                return function(callback, ms){
                    clearTimeout (timer);
                    timer = setTimeout(callback, ms);
                }
            })();
        });

        var url = "<?php echo Yii::app()->createUrl('langManagement/translateLang') ?>";
        $('#lang-management-grid table.items tbody tr td.editable').live('click', function(e){
            var node = $('div', this);
            if (node.length > 0 && node.attr('langcode') != '') {
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('langManagement/translateLangEdit') ?>',
                    data: {
                        id: node.attr('rel'),
                        langcode: node.attr('langcode'),
                        module: node.attr('module'),
                        text_key: node.attr('text_key')
                    },
                    type: "GET",
                    success: function(data){
                        var x = node.parent().position().top+$('#lang-management-grid').position().top+50;
                        var y = ($(window).width() / 2) - ($('.popover-content').width() / 2);
                        /* var y = $(document).width() - (node.parent().position().left+$('#lang-management-grid').position().left+70); */
                        $('#popover-area').html(
                                '<div class="popover editable fade bottom in" style="top: '+x+'px; right: '+y+'px; display: block;"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content">'+
                                data.html+
                                '</div></div>'
                        );
                    }
                });
            }
        });

        $.fn.updateLangContent = function(data) {
          if (data.html) {
            $('.modal.in .modal-body').html(data.html);
          } else {
            $('#lang-management-grid').yiiGridView('update');
            $('.modal.in').modal('hide');
          }
        }
	})(jQuery);
</script>

<style>
    input[type="textfield"], input[type="text"], input[type="text"]:focus, .modal-body input[type="text"], .modal-body input[type="password"], .modal-body textarea, span.jqselect .jq-selectbox__select {
        width: 112px;
    }
    .jq-selectbox.jqselect {
        margin-left: 4px;
    }
    .group td[valign="middle"] {
        vertical-align: middle;
    }
    .advanced-search-box {
        border-right-width: 0;
        padding-right: 0;
        width: auto;
    }
    .group .select .jq-selectbox__select {
        background-color: #FFF;
    }
    .popover.editable {
        max-width: 630px;
    }
    .popover.editable .popover-content {
        padding-top: 18px;
        padding-bottom: 18px;
    }
</style>