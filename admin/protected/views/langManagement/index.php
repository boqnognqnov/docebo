<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('menu', '_LANG'),
); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id'          => 'group-management-form',
	'htmlOptions' => array(
		'class'     => 'ajax-grid-form',
		'data-grid' => '#lang-management-grid'
	),
)); ?>

<div class="main-section lang-management">
	<h3 class="title-bold"><?php echo Yii::t('menu', '_LANG') ?></h3>
</div>
<?php echo CHtml::link(Yii::t('standard', '_IMPORT'), Yii::app()->createUrl('langManagement/importLang'), array(
	'class' => 'btn btn-docebo green big pull-right open-dialog',
	'data-dialog-title' => Yii::t('standard', '_IMPORT'),
	'data-dialog-class' => 'import-language'
));
?>
<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<div class="bottom-section clearfix">
	<div id="grid-wrapper">
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'              => 'lang-management-grid',
			'htmlOptions'     => array('class' => 'grid-view clearfix'),
			'emptyText'       => Yii::t('report', '_NULL_REPORT_RESULT'),
			'dataProvider'    => $langModel->dataProvider(),
			'template'        => '{items}{summary}{pager}',
			'summaryText'     => Yii::t('standard', '_TOTAL'),
			/*'pager' => array(
				'class' => 'ext.local.pagers.DoceboLinkPager',
				'pageSize' => 50,
			),*/
			'pager' => array(
				'class' => 'DoceboCLinkPager',
				'maxButtonCount' => 8,
				'pageSize' => 50,
			),
			'ajaxUpdate'      => 'all-items',
			'afterAjaxUpdate' => 'function(id, data){ $(\'a[rel="tooltip"]\').tooltip(); }',
			'columns'         => array(
				'lang_code' => array(
					'header' => Yii::t('standard', '_LANGUAGE'),
					'type'   => 'raw',
					'value'  => '"<img src=\"".$data->getFlagSrc()."\" alt=\" \"> {$data->lang_code}"',
				),
				'lang_description',
				array(
					'name'        => '',
					'type'        => 'raw',

					// If lang set as default language, don't allow to disable it
					// but if it's accidentally disabled for some reason (e.g. before this
					// policy was put in place), allow it to be reenabled
					'value'       => '$data->lang_code==Settings::get(\'default_language\') && $data->lang_active == CoreLangLanguage::STATUS_VALID ? "" : CHtml::link("", Yii::app()->createUrl("langManagement/changeStatus", array("langcode" => $data->lang_code)), array("class" => "lang-status ".($data->lang_active == CoreLangLanguage::STATUS_VALID ? "suspend-action" : "activate-action"), "rel"=>"tooltip", "title"=>($data->lang_active == CoreLangLanguage::STATUS_VALID ? Yii::t("standard", "_DEACTIVATE") : Yii::t("standard", "_ACTIVATE")), "tooltip_activate"=>Yii::t("standard", "_ACTIVATE"), "tooltip_deactivate"=>Yii::t("standard", "_DEACTIVATE")))',
					'htmlOptions' => array('class' => 'button-column-single')
				),
				array(
					'name'        => '',
					'type'        => 'raw',
					'value'       => 'CHtml::link("<div class=\"i-sprite is-menu\"></div>", "index.php?r=langManagement/translateLang&langcode=$data->lang_code", array(
						"class" => "edit-translate",
						"rel" => "tooltip",
                        "title" => "' . Yii::t('admin_lang', '_TRANSLATELANG') . '",
					));',
					'htmlOptions' => array('class' => 'button-column-single-i-sprite')
				),
				array(
					'name'        => '',
					'type'        => 'raw',
					'value'       => 'CHtml::link("", "", array(
						"class" => "ajaxModal edit-action",
						"data-toggle" => "modal",
						"data-modal-class" => "edit-node-half",
						"data-modal-title" => "' . Yii::t('standard', '_MOD') . '",
						"data-buttons" => \'[
							{"type": "submit", "title": "' . Yii::t('standard', '_CONFIRM') . '"},
							{"type": "cancel", "title": "' . Yii::t('standard', '_CANCEL') . '"}
						]\',
						"data-url" => "langManagement/updateLang&langcode=$data->lang_code",
						"data-after-submit" => "updateLangContent",
						"rel" => "tooltip",
                        "title" => "' . Yii::t('standard', '_MOD') . '",
					));',
					'htmlOptions' => array('class' => 'button-column-single')
				),
				array(
					'name'  => '',
					'type' => 'raw',
					'value' => function($data) {
							return CHtml::link('', Yii::app()->createUrl('langManagement/exportLang', array('langcode' => $data->lang_code)), array(
								'rel' => 'tooltip',
								'title' => Yii::t('standard', '_EXPORT'),
								'class' => 'report-export',
								'target' => '_blank'
							));
						},
					'htmlOptions' => array('class' => 'button-column-single')
				)
			),
		)); ?>
	</div>
</div>

<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input, select').styler();
			replacePlaceholder();
		});

		$('.lang-status').live('click', function () {
			var node = $(this);

			$.ajax({
				url:     $(this).attr('href'),
				success: function () {
					if (node.hasClass('suspend-action')) {
						node.attr('class', 'lang-status activate-action');
						node.attr('data-original-title', node.attr('tooltip_activate'));
						if(node.is(':hover')) {
							node.mouseover();
						}
					} else {
						node.attr('class', 'lang-status suspend-action');
						node.attr('data-original-title',  node.attr('tooltip_deactivate'));
						if(node.is(':hover')) {
							node.mouseover();
						}
					}
				}
			});
			return false;
		});

		$.fn.updateLangContent = function (data) {

			if($.type(data.data) != 'undefined' && $.type(data.data) == 'object'){
				var msg = data.data.error;
				Docebo.Feedback.show('error', msg);
			}

			if (data.html) {
				$('.modal.in .modal-body').html(data.html);
			} else {
				$('#lang-management-grid').yiiGridView('update');
				$('.modal.in').modal('hide');
			}
		}
	})(jQuery);

	$(document).delegate(".import-language", "dialog2.content-update", function() {
		var e = $(this);
		var autoclose = e.find("a.auto-close");
		if (autoclose.length > 0) {
			e.find('.modal-body').dialog2("close");
			$("#lang-management-grid").yiiGridView('update');
		}
	});
</script>
