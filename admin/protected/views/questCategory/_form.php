<div class="form">

	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'questcat_form',
		'htmlOptions' => array(	
			'class' => 'ajax form-horizontal',
		),	
	)); ?>



	<div class="row-fluid">
		<?php //echo $form->errorSummary($model); ?>
	</div>

	<div class="row-fluid">
	
		<div class="control-group">
			<?php echo $form->labelEx($model, 'name', array('class' => 'control-label', 'for' => 'name')); ?>
			<div class="controls">
				<?php echo $form->textField($model, 'name'); ?>
				<?php echo $form->error($model, 'name'); ?>
			</div>
		</div>
		<div class="control-group">
			<?php echo $form->labelEx($model, 'textof', array('class' => 'control-label', 'for' => 'textof')); ?>
			<div class="controls">
				<?php echo $form->textArea($model, 'textof'); ?>
				<?php echo $form->error($model, 'textof'); ?>
			</div>
		</div>
	
	</div>
	
	
	<!-- For Dialog2 - they go in footer -->
	<div class="form-actions">
		<?php echo CHtml::submitButton(Yii::t('standard','_SAVE'), array('class' => 'btn btn-submit')); ?>
		<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
	</div>

	


	<?php $this->endWidget(); ?>
</div>	
