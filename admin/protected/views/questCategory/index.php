
<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('test', '_TEST_MODALITY') => Docebo::createAdminUrl('questCategory/index'),
	Yii::t('menu', '_QUESTCATEGORY'),
); ?>

<div class="admin-advanced-wrapper">
	<div id="sidebar" class="advanced-sidebar">
		<ul>
			<?
			echo '<li>' . CHtml::link('<i class="fa fa-database"></i></span>' . Yii::t('test', 'Question bank'), Docebo::createAdminUrl('questionBank/index'), array('class' => 'questionBank')) . '</li>';
			echo '<li>' . CHtml::link('<i class="fa fa-folder-open"></i>' . Yii::t('menu', '_QUESTCATEGORY'), Docebo::createAdminUrl('questCategory/index'), array('class' => 'questCategory active')) . '</li>';
			?>
		</ul>
	</div>
	<div id="main" class="advanced-main">
		<div class="content">

			<div class=main-actions clearfix">
				<h3 class="title-bold"><?php echo Yii::t('menu', '_QUESTCATEGORY'); ?></h3>
				<?php $this->renderPartial('_mainActions'); ?>
			</div>

			<?php $form = $this->beginWidget('CActiveForm', array(
				'id' => 'questcat-management-form',
				'htmlOptions' => array(
					'class' => 'ajax-grid-form',
					'data-grid' => '#questcat-management-grid'
				),
			)); ?>

			<div class="main-section questcat-management">
				<div class="filters-wrapper">
					<div class="filters clearfix">
						<div class="input-wrapper">
							<input autocomplete="off" data-url="<?= Docebo::createAppUrl('admin:questCategory/autoComplete') ?>" id="advanced-search-qustion-category-grid" class="typeahead" type="text" name="LearningQuestCategory[search_input]" placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"/>
						<span class="search-icon"></span>
						</div>
					</div>
				</div>
			</div>
			<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
			<?php $this->endWidget(); ?>

			<div class="bottom-section clearfix">
				<div id="grid-wrapper">
					<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id' => 'questcat-management-grid',
						'htmlOptions' => array('class' => 'grid-view clearfix'),
						'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
						'afterAjaxUpdate'=>'function(){$(document).controls();}',
						'dataProvider' => $model->search(),
						'template' => '{items}{pager}',
						'pager' => array(
							'class' => 'DoceboCLinkPager',
							'maxButtonCount' => 8,
						),
						//'ajaxUpdate' => 'all-items',
						'columns' => array(
							//'idCategory',
							'name',
							'textof',

							// Using regular column instead of button column, because Admin CSS is terrible!
							array(
								'name' => '',
								'type' => 'raw',
								'cssClassExpression' => '"text-right"',
								'value' => array($this, '_renderGridOperationsColumn')
							),
						),
					)); ?>
				</div>
			</div>

			<script type="text/javascript">
			<!--
				(function ($) {
					$(function () {
						$('input, select').styler();
						replacePlaceholder();
					});
				})(jQuery);
			//-->
			</script>

	</div>
</div>
</div>
