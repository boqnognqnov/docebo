<?php

$editUrl = $this->createUrl("questCategory/axUpdateCategory", array('category_id' => $model->idCategory));
$deleteUrl = $this->createUrl("questCategory/axDeleteCategory", array('category_id' => $model->idCategory));

?>
<a href="<?= $editUrl ?>"
	title="<?= Yii::t('standard', '_MOD') ?>"
	class="open-dialog"
	data-dialog-class="edit-questcat-modal"
	closeOnEscape="false"
	closeOnOverlayClick="false"
	rel="questcat-modal">

	<span class="i-sprite is-edit"></span>
</a>
<a href="<?php echo $deleteUrl; ?>"
   title="<?= Yii::t('standard', '_DEL') ?>"
   class="open-dialog"
   data-dialog-class="delete-item-modal"
   closeOnEscape="false"
   closeOnOverlayClick="false"
   rel="questcat-modal">

	<span class="i-sprite is-remove red"></span>
</a>