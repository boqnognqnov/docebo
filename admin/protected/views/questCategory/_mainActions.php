<?php

	//$createUrl = $this->createUrl('questCategory/axCreateCategory');
	$createUrl = $this->createUrl('questCategory/axUpdateCategory');

?>
<div class="main-actions clearfix">
	<ul class="clearfix">
		<li>
			<div>
				<a href="<?= $createUrl ?>"
					title="<?= Yii::t('standard', '_NEW_CATEGORY') ?>"
					alt="<?= Yii::t('helper', 'New question category') ?>"
					class="new-node open-dialog"
					closeOnEscape="false"
					closeOnOverlayClick="false"
					rel="questcat-modal"
					data-dialog-class="edit-questcat-modal">

					<span></span><?= Yii::t('standard','_NEW_CATEGORY') ?>
				</a>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>
			<p></p>
		</div>
	</div>
</div>
