<div class="row" data-page-id="<?php echo $data->page->id; ?>">
	<div class="title">
		<?php echo $data->title; ?>
	</div>
	<div class="actions">
		<ul>
			<li class="move"><?= Yii::t('standard', '_MOVE'); ?></li>
			<li class="<?php echo $data->page->publish == 1 ? 'enabled' : 'disabled'; ?>">
				<?php echo CHtml::link('Activate', 'javascript:void(0);', array('class' => 'toggle-active-state', 'data-url' => 'branding/togglePagePublish', 'data-id' => $data->page->id)); ?>
			</li>
			<li class="edit" data-id="<?php echo $data->id; ?>">
				<?php $this->renderPartial('//common/_modal', array(
					'config' => array(
						'class' => 'branding-external-page',
						'modalTitle' => Yii::t('branding', 'Edit external page'),
						'linkTitle' => 'Edit',
						'linkOptions' => array(
							'class' => 'ajaxModal branding-external-page',
						),
						'url' => 'branding/editExternalPage&id=' . $data->page->id,
						'buttons' => array(
							array(
								'type' => 'submit',
								'title' => Yii::t('standard', '_SAVE'),
							),
							array(
								'type' => 'cancel',
								'title' => Yii::t('standard', '_CANCEL'),
							),
						),
						'afterLoadingContent' => 'externalPageFormInit();',
						'afterSubmit' => 'externalPageCallback',
						'beforeSubmit' => 'externalPageFormPrepare'
					),
				)); ?>
			</li>
			<li class="delete">
				<?php echo CHtml::link("", "", array(
						'class' => 'ajaxModal delete-action',
						'data-toggle' => 'modal',
						'data-modal-class' => 'delete-node',
						'data-modal-title' => Yii::t('standard', '_DEL'),
						'data-buttons' => json_encode(array(
							array('type' => 'submit', 'title' => Yii::t('standard', '_CONFIRM')),
							array('type' => 'cancel', 'title' => Yii::t('standard', '_CANCEL')),
						)),
						'data-url' => 'branding/deletePage&id=' . $data->page->id,
						'data-after-loading-content' => 'hideConfirmButton();',
						'data-after-submit' => 'updateExternalPagesContent',
					)); ?>
				<?php //echo CHtml::link('Delete', 'javascript:void(0);', array('class' => 'delete-link', 'data-url' => 'branding/deletePage', 'data-id' => $data->page->id)); ?>
			</li>
		</ul>
	</div>
</div>