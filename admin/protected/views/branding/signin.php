<style type="text/css">
	.visibility-hidden{
		overflow: hidden !important;
		height: 0px !important;
		visibility: hidden !important;
		min-height: 0px !important;
	}
	.minimal-layout-image-selector{
		display: block !important;
	}
</style>
<div class="form-wrapper">
	<?php $form = $this->beginWidget('CActiveForm',
		array(
			'id' => 'branding-signin-form',
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
			'action' => Yii::app()->createUrl('branding/signin'),
		)
	); ?>

	<div class="messages"><?php echo $form->errorSummary($model); ?></div>

	<div class="section signin">
		<h5><?= Yii::t('templatemanager', 'Login page layout'); ?></h5>

		<div class="row even has-border">
			<div class="setting-name">
				<?php echo Yii::t('branding', 'Login form'); ?>
				<p class="description description-regular"><?php echo Yii::t('branding', 'Use this option if you want only login via Google,Okta,SAML,etc'); ?></p>
			</div>
			<div class="values">
				<?php echo $form->checkbox($model, 'hide_signin_form', array('id' => 'hide_signin_form'));?>
				<label for="hide_signin_form"><?php echo Yii::t('branding', 'Show only SSO buttons and hide login form'); ?></label>
			</div>
		</div>


		<div class="row odd has-border">
			<div class="row blockable-layout">
				<?php
				$catalog_external = Settings::get('catalog_external', 'off', false, false);
				if ($catalog_external == 'on') { ?>
				<div class="row-fluid">
					<div class="span12">
						<?php
							$this->widget('common.widgets.warningStrip.WarningStrip',
								array(
									'message'=>Yii::t('branding',
										'All options below are not applicable when the Public Catalog is enabled in Catalog App settings page.'),
									'type'=>'warning'
								)
							);
						?>
							<script type="text/javascript">
								$('#branding-signin-form .blockable-layout').block({
									message: "",
									timeout: 0,
									overlayCSS:  {
										backgroundColor: 	'#FFF',
										opacity:         	0.6,
										cursor:          	'auto'
									}
								});

								$('.admin-advanced-wrapper .section.signin .row.blockable-layout').css('max-height', '400px');
							</script>

					</div>
				</div>
				<?php } ?>

				<div class="setting-name">
					<?php echo $form->labelEx($model, 'login_layout'); ?>
					<p class="description"><?php echo Yii::t('branding', 'Sign in layout description'); ?></p>
				</div>
				<div class="values">
					<div class="row-fluid">
						<div class="span4">
							<?php echo $form->radioButtonList($model, 'login_layout',
							array(
								'layout2' => Yii::t('templatemanager', 'Top login form'),
								'layout3' => Yii::t('templatemanager', 'Bottom login form'),
								'layout1' => Yii::t('templatemanager', 'Side Login form'),
								'layout4' => Yii::t('templatemanager', 'Minimal Sign In form'),
							)); ?>
						</div>
						<div class="span8">
							<div class="login-images">
								<div class="top layout2">
									<span class="background"><?php echo Yii::t('certificate', '_BACK_IMAGE'); ?></span>
									<span class="title-text"><?php echo Yii::t('branding', '_TITLE_TEXT_SECTION'); ?></span>
									<span class="external"><?php echo Yii::t('admin_webpages', '_WEBPAGES_CAPTION'); ?></span>
									<?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/loginlayouts/top.jpg'); ?>
								</div>
								<div class="bottom layout3">
									<span class="background"><?php echo Yii::t('certificate', '_BACK_IMAGE'); ?></span>
									<span class="text"><?php echo Yii::t('branding', '_TITLE_TEXT_SECTION'); ?></span>
									<span class="external"><?php echo Yii::t('admin_webpages', '_WEBPAGES_CAPTION'); ?></span>
									<?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/loginlayouts/bottom.jpg'); ?>
								</div>
								<div class="side layout1">
									<span class="background"><?php echo Yii::t('certificate', '_BACK_IMAGE'); ?></span>
									<span class="title-text"><?php echo Yii::t('branding', '_TITLE_TEXT_SECTION'); ?></span>
									<span class="external"><?php echo Yii::t('admin_webpages', '_WEBPAGES_CAPTION'); ?></span>
									<?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/loginlayouts/side.jpg'); ?>
								</div>
								<div class="minimal layout4">
									<span class="background"><?php echo Yii::t('certificate', '_BACK_IMAGE'); ?></span>
									<span class="title-text"><?php echo Yii::t('branding', '_TITLE_TEXT_SECTION'); ?></span>
									<?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/loginlayouts/minimal.jpg'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row even" id="home-login-img">
			<div class="setting-name">
				<?php echo $form->labelEx($model, 'home_login_img'); ?>
				<p class="description description-regular"><?php echo Yii::t('branding', '_HOME_LOGIN_IMG_DESCRIPTION'); ?></p>
				<p class="description description-minimal" style="display: none"><?php echo Yii::t('branding', 'Select a color or upload image or video for the background'); ?></p>
			</div>
			<div class="values">
				<div class="values-regular row-fluid">
					<div class="current_ background_ span6 text-center">
						<!-- Image selector -->
						<?php
						$this->widget('common.widgets.ImageSelector', array(
							'imageType'			=> CoreAsset::TYPE_LOGIN_BACKGROUND,
							'assetId' 			=> $model->home_login_img,//$model->signInPageImage,
							'imgVariant'		=> CoreAsset::VARIANT_ORIGINAL,
							'buttonText'		=> Yii::t('setup', 'Upload your background'),
							'buttonClass'		=> 'btn btn-docebo green big',
							'dialogId'			=> 'image-selector-modal-signin',
							'dialogClass'		=> 'image-selector-modal',
							'inputHtmlOptions'	=> array(
								'name'	=> 'BrandingSigninForm[home_login_img]',
								'id'	=> 'signin-image-id',
								'class'	=> 'image-selector-input',
							),
						));
						?>
						<!-- Image selector -->
					</div>
					<div class="login-img-position span6">
						<br />
						<?php
						$sub_array = array('900' => '900');
						if(Settings::get('catalog_external') == 'on')
							$sub_array = array('900' => '1156');

						echo $form->radioButtonList($model, 'home_login_img_position',
						array(
							'fill' => Yii::t('branding', '_HOME_IMG_POSITION_FILL', $sub_array),
							'tile' => Yii::t('branding', '_HOME_IMG_POSITION_TILE'),
						)); ?>
	                </div>
				</div>
				<div class="values-minimal" style="display: none">
					<?= CHtml::hiddenField('BrandingSigninForm[home_login_img_position]', $model->home_login_img_position); ?>
					<?= CHtml::hiddenField('BrandingSigninForm[minimal_login_video_fallback_img]', $model->minimal_login_video_fallback_img); ?>
					<?= CHtml::hiddenField('BrandingSigninForm[minimal_login_background]', $model->minimal_login_background); ?>

					<?= CHtml::radioButton('BrandingSigninForm[minimal_login_type]', (($model->minimal_login_type == 'color') ? true : false), array('id' => 'minimal_login_type_color', 'value' => 'color')); ?>
					<?= CHtml::label(Yii::t('branding', 'Color'), 'minimal_login_type_color'); ?> <br />

					<div class="color-selector colors">
						<div class="color-item">
							<div class="wrap">
								<?=CHtml::textField('BrandingSigninForm[minimal_login_background_color]',
									(strpos($model->minimal_login_background, '#') !== false) ? $model->minimal_login_background : '#FFFFFF',
									array(
										'autocomplete' => 'off',
										'class' => 'color-tooltip',
									)) ?>
								<div id="farbtastic-tooltip-signin-bg" class="tooltip farbtastic-tooltip" style=" opacity: 1; display: none;">
									<div class="colorpicker"></div>
								</div>
								<div class="color-preview-wrap">
									<div class="color-preview" style="display: block; width: 21px; height: 21px; background-color: <?php echo ((strpos($model->minimal_login_background, '#') !== false) ? $model->minimal_login_background : '#FFFFFF'); ?>;"></div>
								</div>
							</div>
						</div>
						<p class="description"><?= Yii::t('branding', 'Insert HEX code or choose a color by clicking on the icon'); ?></p>
					</div>


					<?= CHtml::radioButton('BrandingSigninForm[minimal_login_type]', (($model->minimal_login_type == 'image') ? true : false), array('id' => 'minimal_login_type_image', 'value' => 'image')); ?>
					<?= CHtml::label(Yii::t('branding', 'Full width background image'), 'minimal_login_type_image'); ?>
					<div class="minimal-layout-image-selector visibility-hidden">
						<p class="description" style="margin-top: 0px; font-weight: normal;"><?= Yii::t('branding', 'Suggested image dimensions at least'); ?> 1280x720 px</p>
						<div id="container_minimal_bg_img">
							<div style="float: left; width: 100%; ">
								<a id="pickfiles_minimal_bg_img" style="float: left" class="pickfiles confirm-btn btn-docebo green big" href="javascript:void(0);"><?php echo Yii::t('organization', 'UPLOAD'); ?></a>
								<?= CHtml::hiddenField('BrandingSigninForm[minimal_bg_img_path]'); ?>
								<?= CHtml::hiddenField('BrandingSigninForm[minimal_bg_img_original_filename]'); ?>
								<div id="minimal_bg_img_file" style="display: inline-block; float: left; margin-left: 10px; vertical-align: middle; margin-top: 5px;"></div>
							</div>

							<div style="float: right; margin-right: 60px;">
							<?php if(strpos($model->minimal_login_background, '#') === false && $model->minimal_login_background <> '' && !is_null($model->minimal_login_background) && $model->minimal_login_type == 'image') {
								$bgImgAsset =  CoreAsset::model()->findByPk($model->minimal_login_background);
								$bgImgUrl = ($bgImgAsset) ? $bgImgAsset->getUrl() : '';
								if($bgImgUrl <> '') {
								?>

								<img id="minimal-layout-img-preview" src="<?=$bgImgUrl; ?>" style="outline: 1px solid #e4e6e5; padding: 1px; height: 100px; width: 185px; position:relative; top: -70px;" />
							<?php }
							} ?>
							</div>
						</div>
					</div> <br />

					<?= CHtml::radioButton('BrandingSigninForm[minimal_login_type]', (($model->minimal_login_type == 'video') ? true : false), array('id' => 'minimal_login_type_video', 'value' => 'video')); ?>
					<?= CHtml::label(Yii::t('branding', 'Full width background video'), 'minimal_login_type_video'); ?>
					<div class="minimal-layout-video-selector visibility-hidden" style="width: 100%; margin-left: 25px;">
						<p class="description" style="margin-top: 0px; font-weight: normal;"><?= Yii::t('branding', 'Video formats supported MP4, H264. Suggested size 1,5 to 2 MB, bitrate 500 to 800 kbps, resolution 1280x720 at least'); ?></p>
						<div id="container_minimal_bg_video">
							<div style="float: left; width: 100%; ">
								<a id="pickfiles_minimal_bg_video" style="float: left" class="pickfiles confirm-btn btn-docebo green big" href="javascript:void(0);"><?php echo Yii::t('organization', 'UPLOAD'); ?></a>
								<?= CHtml::hiddenField('BrandingSigninForm[minimal_bg_video_path]'); ?>
								<?= CHtml::hiddenField('BrandingSigninForm[minimal_bg_video_original_filename]'); ?>
								<div id="minimal_bg_video_file" style="display: inline-block; float: left; margin-left: 60px; vertical-align: middle; margin-top: 5px; font-weight: bold;"><?php
									if ($model->minimal_login_background && (strpos($model->minimal_login_background, '#') === false) && ($model->minimal_login_type == 'video')) {
										$bgVideoAsset =  CoreAsset::model()->findByPk($model->minimal_login_background);
										if($bgVideoAsset) { echo $bgVideoAsset->original_filename; }
									}
									?></div>
							</div>

							<br />
							<br />
							<br />
							<label style="margin-left: 0;"><?= Yii::t('branding', 'Fallback image'); ?></label>
							<p class="description" style="margin-top: 0px; font-weight: normal;"><?= Yii::t('branding', 'In case of video is not supported or did not load this image will remain as fallback.'); ?></p>
							<div id="container_minimal_video_fallback_img">
								<div style="float: left; width: 100%;">
									<a id="pickfiles_minimal_video_fallback_img" style="float: left" class="pickfiles confirm-btn btn-docebo green big" href="javascript:void(0);"><?php echo Yii::t('organization', 'UPLOAD'); ?></a>
									<?= CHtml::hiddenField('BrandingSigninForm[minimal_video_fallback_img_path]'); ?>
									<?= CHtml::hiddenField('BrandingSigninForm[minimal_video_fallback_img_original_filename]'); ?>
									<div id="minimal_video_fallback_img_file" style="position: absolute; left: -10px; top: 30px; width: 130px; word-break: break-all;"></div>
								</div>
								<div style="float: right; margin-right: 60px; position: relative; top: -30px;">
									<?php if ($model->minimal_login_video_fallback_img <> '' && !is_null($model->minimal_login_video_fallback_img)) {
									$bgFallbackImgAsset =  CoreAsset::model()->findByPk($model->minimal_login_video_fallback_img);
									$bgFallbackImgUrl = ($bgFallbackImgAsset) ? $bgFallbackImgAsset->getUrl() : '';
									if($bgFallbackImgUrl <> '') {
										?>
										<img id="minimal-layout-fallback-img-preview" src="<?=$bgFallbackImgUrl; ?>" style="outline: 1px solid #e4e6e5; padding: 1px; height: 100px; width: 185px; position:relative; right: 270px;" />
									<?php }
									} ?>
								</div>
							</div>

						</div>
					</div><br />

				</div>
			</div>
		</div>


		<div class="row odd has-border">
			<div class="row">
				<div class="setting-name">
					<?php echo Yii::t('branding', '_TITLE_TEXT_SECTION'); ?>
					<p class="description"><?php echo Yii::t('branding', '_TITLE_TEXT_SECTION_DESCRIPTION'); ?></p>
				</div>
				<div class="values">
						<div class="languages">
						<p><?php echo Yii::t('branding', '_PAGE_LANGUAGE_LABEL'); ?></p>
							<div class="lang-wrapper">
								<?php echo CHtml::listBox('languages', '0', array('0' => Yii::t('adminrules', '_SELECT_LANG_TO_ASSIGN')) + $model->getLanguages(), array('size' => 1, 'id' => 'login-pages-lang-select', 'class' => 'language-selector')); ?>
								<div class="stats">
									<p class="available"><?php echo Yii::t('admin_lang', '_LANG_ALL'); ?>: <span><?php echo count($model->getLanguages()); ?></span></p>
									<p class="assigned"><?php echo Yii::t('standard', 'Filled languages'); ?>: <span></span></p>
								</div>
							</div>
							<div class="input-fields" id="input-fields-for-login-page" style="position: relative;">
								<p><?php echo Yii::t('standard', '_TITLE'); ?></p>
								<div class="max-input">
									<?php echo CHtml::textfield(Yii::t('standard', '_TITLE'), '', array('class' => 'title', 'id'=>'sign-in-page-title')); ?>
								</div>
								<p><?php echo Yii::t('standard', '_TEXTOF'); ?></p>
								<?php echo CHtml::textarea(Yii::t('branding', '_TITLE_TEXT_SECTION'), '', array('class' => 'text', 'id'=>'sign-in-page-description')); ?>
							</div>
							<div class="translations" style="display: none;">
								<?php $translations = $model->getTranslations(); ?>
								<?php foreach ($translations as $langCode => $translation): ?>
									<div class="<?php echo $langCode ?>">
										<?php echo $form->textfield($translation['_LOGIN_MAIN_TITLE'], 'translation_text', array('name' => "CoreLangTranslation[$langCode][{$translation['_LOGIN_MAIN_TITLE']->id_text}][translation_text]")); ?>
										<?php echo $form->textarea($translation['_LOGIN_MAIN_CAPTION'], 'translation_text', array('name' => "CoreLangTranslation[$langCode][{$translation['_LOGIN_MAIN_CAPTION']->id_text}][translation_text]")); ?>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
			</div>
		</div>

		<div class="row-fluid" id="show-hide-webpages-warning">
			<div class="span12">
				<?php $this->widget('common.widgets.warningStrip.WarningStrip',
					array(
						'message'=>Yii::t('branding',
							'Web pages are not supported in the "Minimal sign in form" home page layout. Please, switch to top, bottom or side sign in form layouts in order to enable them.'),
						'type'=>'warning'
					)
				); ?>
			</div>
		</div>

		<div class="row even">
			<div class="setting-name">
				<?php echo Yii::t('admin_webpages', '_TITLE_WEBPAGES'); ?>
				<p class="description"><?php echo Yii::t('branding', '_EXTERNAL_PAGES_SECTION_DESCRIPTION'); ?></p>
			</div>
			<div class="values items-sortable-wrapper blockable">
				<h6><?php echo Yii::t('configuration', '_PAGE_TITLE'); ?></h6>
				<?php /*$this->renderPartial('//common/_modal', array(
					'config' => array(
						'class' => 'branding-external-page',
						'modalTitle' => Yii::t('branding', 'Add external page'),
						'linkTitle' => Yii::t('admin_webpages', '_ADD_WEBPAGES'),
						'linkOptions' => array(
							'class' => 'ajaxModal branding-external-page',
						),
						'url' => 'branding/addExternalPage',
						'buttons' => array(
							array(
								'type' => 'submit',
								'title' => Yii::t('standard', '_SAVE')
							),
							array(
								'type' => 'cancel',
								'title' => Yii::t('standard', '_UNDO'),
							),
						),
						'afterLoadingContent' => 'externalPageFormInit();',
						'afterSubmit' => 'externalPageCallback',
						'beforeSubmit' => 'externalPageFormPrepare'
					),
				));*/ ?>
				<?php

				echo CHtml::link(
					Yii::t('admin_webpages', '_ADD_WEBPAGES'),
					$this->createUrl('addExternalPage'),
					array(
						'class' => 'branding-external-page open-dialog',
						'data-dialog-class' => 'modal-branding-external-page branding-external-page',
						'removeOnClose' => 'true',
						'closeOnOverlayClick' => 'true',
						'closeOnEscape' => 'true',
					)
				);

				?>
				<?php $this->widget('zii.widgets.CListView', array(
					'id' => 'external-pages-management-list',
					'htmlOptions' => array('class' => 'list-view clearfix'),
					//'dataProvider' => $pageTranslationModel->dataProvider(),
					'dataProvider' => $pageModel->dataProvider(),
					'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
					'itemView' => '_view_webpages',
					// 'itemsCssClass' => 'items clearfix',
					'template' => '{items}',
					'ajaxUpdate' => true,
					'afterAjaxUpdate' => 'function(id, data) {
							setExternalPagesListSortable();
							$("#external-pages-management-list").controls();
							//$(document).controls();
							$(\'a[rel="tooltip"]\').tooltip();
						}'
				)); ?>
			</div>
		</div>

		<div class="actions right-buttons">
			<?php echo CHtml::hiddenField('selectedTab', '', array('id' => 'selectedTab')); ?>
			<?php echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class' => 'btn-save', 'id' => 'signin-save-btn')); ?>
		</div>
	</div>
	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
	(function($) {
	<?php if ($catalog_external == 'on') { ?>
	$('#BrandingSigninForm_login_layout_0').trigger('click');
	<?php } ?>

		$('#signin-save-btn').on('click', function(e) {
			var radioChecked = $('input[name="BrandingSigninForm[login_layout]"]:checked');

			// If minimal layout is chosen !!!
			if (radioChecked.val() == 'layout4') {
				// If there is no image selected !!!
				if ($('#minimal_login_type_image').is(':checked')) {
					if($('#minimal-layout-img-preview').length == 0 && $('#BrandingSigninForm_minimal_bg_img_original_filename').val() == '') {
						// Display error
						Docebo.Feedback.show('error', Yii.t('app7020', 'Error') + ': ' + Yii.t('branding', 'Please, select an image for the background'));
						e.preventDefault();
						return false;
					}
				}

				// If there is no video selected - minimal-layout-fallback-img-preview
				if ($('#minimal_login_type_video').is(':checked')) {
					if($('#minimal_bg_video_file').html() == '' && $('#BrandingSigninForm_minimal_bg_video_original_filename').val() == '') {
						// Display error
						Docebo.Feedback.show('error', Yii.t('app7020', 'Error') + ': ' + Yii.t('branding', 'Please, select a video for the background'));
						e.preventDefault();
						return false;
					}

					//
					if($('#minimal-layout-fallback-img-preview').length == 0 && $('#BrandingSigninForm_minimal_video_fallback_img_original_filename').val() == '') {
						// Display error
						Docebo.Feedback.show('error', Yii.t('app7020', 'Error') + ': ' + Yii.t('branding', 'Please, select a fallback image for the background'));
						e.preventDefault();
						return false;
					}
				}
			}
		});

		$('#login-pages-lang-select').on('change', function(){
			tinyMCE.get('sign-in-page-description').focus();
			if($(this).val()==0){
				$('#input-fields-for-login-page').append('<div id="overlay-login-page" class="blockUI blockOverlay" style="z-index: 1000; border: medium none; margin: 0px; padding: 0px; width: 100%; height: 100%; top: 0px; left: 0px; background-color: #f3f3f3; opacity: 0.6; cursor: auto; position: absolute;"></div>');
			}
			else{
				$('#input-fields-for-login-page #overlay-login-page').remove();
			}
		});
		if($('#login-pages-lang-select').val()==0){
			$('#input-fields-for-login-page').append('<div id="overlay-login-page" class="blockUI blockOverlay" style="z-index: 1000; border: medium none; margin: 0px; padding: 0px; width: 100%; height: 100%; top: 0px; left: 0px; background-color: #f3f3f3; opacity: 0.6; cursor: auto; position: absolute;"></div>');
		}
		else{
			$('#input-fields-for-login-page #overlay-login-page').remove();
		}
	})(jQuery);
</script>