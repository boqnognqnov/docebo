<div class="form-wrapper">
    <div class="bottom-section clearfix">
        <div id="grid-wrapper">
					<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id' => 'module-activation-grid',
						'htmlOptions' => array('class' => 'grid-view clearfix'),
						'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
						'dataProvider' => $this->moduleDataProvider(),
						'template' => '{items}{summary}{pager}',
						'summaryText' => '',
						'pager' => array(
							//'class' => 'ext.local.pagers.DoceboLinkPager',
							'class' => 'common.components.DoceboCLinkPager',
						),
						'ajaxUpdate' => 'all-items',
						'columns' => array(
							array(
								'header' => Yii::t('standard', '_MAN_MENU'),
								'type' => 'raw',
								'value' => '$data["title"]',
							),
							array(
								'header' => '',
								'type' => 'raw',
								'value' => '$data["users"]',
								'htmlOptions' => array('width' => '30')
							),
							array(
								'header' => '',
								'type' => 'raw',
								'value' => '$data["activate"]',
								'htmlOptions' => array('width' => '90', 'align' => 'center')
							),
						),
					)); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
        $('.module-status').live('click', function(){
            var node = $(this);

            $.ajax({
                url: $(this).attr('href'),
                success: function(){
                    if (node.hasClass('suspend-action')) {
                        node.attr('class', 'module-status activate-action');
                    } else {
                        node.attr('class', 'module-status suspend-action');
                    }
                }
            });
            return false;
        });

        function deamonModalBox() {
            if (!($('.moduleActivation.active').length > 0 && $('#bootstrapContainer .modal.group-select-users.in').length > 0)) {
                $.cookie('moduleActivation', false);
            }
            setTimeout(deamonModalBox, 200);
        }
        deamonModalBox();
    });
    $(function(){
        $.fn.initModuleActivationData = function(formData, jqForm) {
            formData.push({
                name: 'save',
                value: true
            });
            formData.push({
                name: 'select-user-grid-selected-items',
                value: $('#select-user-grid-selected-items-list').val()
            });
            formData.push({
                name: 'select-group-grid-selected-items',
                value: $('#select-group-grid-selected-items-list').val()
            });
            return true;
        }
        $.fn.updateModuleActivationContent = function(data) {
          if (data.html) {
            $('.modal.in .modal-body').html(data.html);
          } else if (data.status == 'saved') {
            $('.modal.in').modal('hide');
          } else {
            $('.modal.in').modal('hide');
          }
          if (data.status == 'success') {
            $('.modal.in .modal-footer').hide();
            $('.modal.in').addClass('success');
          }
        }
    })
</script>
