<div class="row" data-page-id="<?php echo $data->id; ?>">
	<div class="title">
		<?php echo $data->title; ?>
	</div>
	<div class="actions">
		<ul>
			<li class="preview">
				<?php echo CHtml::link(Yii::t('standard', 'Preview'), 'javascript:void(0);', array(
                    'class' => 'preview-scheme',
                    'data-url' => Docebo::createAppUrl('admin:setup/colorschemeCss', array('scheme' => $data->getPrimaryKey(), 'time' => time())),
                )); ?>
			</li>
			<li class="<?php echo $data->enabled == 1 ? 'enabled' : 'disabled'; ?>">
				<?php echo CHtml::link('Enable', 'javascript:void(0);', array(
                    'class' => 'toggle-active-state',
                    'data-url' => 'branding/toggleSchemeEnabled',
                    'data-id' => $data->id,
                    'scheme-url' => Docebo::createAppUrl('admin:setup/colorschemeCss', array('scheme' => $data->getPrimaryKey(), 'time' => time())),
                )); ?>
			</li>
			<li class="edit" data-id="<?php echo $data->id; ?>">
				<?php $this->renderPartial('//common/_modal', array(
					'config' => array(
						'class' => 'branding-scheme',
						'modalTitle' => Yii::t('branding', 'Edit colors scheme'),
						'linkTitle' => 'Edit',
						'linkOptions' => array(
							'class' => 'ajaxModal branding-scheme',
						),
						'url' => 'branding/editColorsScheme&id=' . $data->id,
						'buttons' => array(
							array(
								'type' => 'submit',
								'title' => Yii::t('standard', '_SAVE'),
							),
							array(
								'type' => 'cancel',
								'title' => Yii::t('standard', '_CANCEL'),
							),
						),
						'afterLoadingContent' => 'schemeFormInit();',
						'afterSubmit' => 'schemeCallback',
					),
				)); ?>
			</li>
			<li class="delete">
                <?php
                    if (!$data->enabled) {
                        echo CHtml::link(Yii::t('standard', '_DEL'), '', array(
                            "class" => "ajaxModal delete-action",
                            "data-toggle" => "modal",
                            "data-modal-class" => "delete-node",
                            "data-modal-title" => Yii::t("standard", "_DEL"),
                            "data-buttons" => '[
                                {"type": "submit", "title": "'.Yii::t("standard", "_CONFIRM").'"},
                                {"type": "cancel", "title": "'.Yii::t("standard", "_CANCEL").'"}
                            ]',
                            "data-url" => "branding/deleteScheme&id=$data->id",
                            "data-after-loading-content" => "hideConfirmButton();",
                            "data-after-submit" => "schemeCallback",
                            //'data-url' => 'branding/deleteScheme',
                            //'data-id' => $data->id,
                        ));
                    } else {
                        echo '<div style="width: 23px; height: 23px;"></div>';
                    }
                ?>
			</li>
		</ul>
	</div>
</div>