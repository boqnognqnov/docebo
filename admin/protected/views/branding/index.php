<?php
if(Yii::app()->request->getParam('access_token')) {
	$this->breadcrumbs = array(
	    Yii::t('theme', 'Manage Themes') => array('route' => '/manage/themes'),
        'Docebo 6.9 - '.Yii::t('menu', 'Branding, Menu & Contents')
    );
} else {
    $this->breadcrumbs = array(
        Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
        Yii::t('menu', 'Branding, Menu & Contents')
    );
}
?>

<?php $sidebarLinks = array(
	'logo' => array(
		'class' => 'logo',
		'title' => '<span></span>' . Yii::t('templatemanager', 'Logo'),
		'data-tab' => 'logo',
		'data-url' => 'branding/logo',
	),
	'signin' => array(
		'class' => 'signin',
		'title' => '<span></span>' . Yii::t('templatemanager', 'Sign In Page'),
		'data-tab' => 'signin',
		'data-url' => 'branding/signin',
		'data-loaded' => 1,
	),
	'layoutColors' => array(
		'class' => 'layoutColors',
		'title' => '<span></span>' . Yii::t('templatemanager', 'Colors'),
		'data-tab' => 'layoutColors',
		'data-url' => 'branding/layoutColors',
		'data-loaded' => 1,
	),
	'customizeCss' => array(
		'class' => 'customizeCss',
		'title' => '<span></span>' . Yii::t('templatemanager', 'Customize css'),
		'data-tab' => 'customizeCss',
		'data-url' => 'branding/customizeCss',
	),
	'moduleActivation' => array(
		'class' => 'moduleActivation',
		'title' => '<span></span>' . Yii::t('templatemanager', 'Customize Menu'),
		'data-tab' => 'moduleActivation',
		'data-url' => 'branding/moduleActivation',
	),
	'coursePlayer' => array(
		'class' => 'coursePlayer',
		'title' => '<span></span>' . Yii::t('templatemanager', 'Course Player'),
		'data-tab' => 'coursePlayer',
		'data-url' => 'branding/coursePlayer',
	),
); ?>

<?php
if (BrandingWhiteLabelForm::isMenuVisible())
	$sidebarLinks['whiteLabel'] = array(
		'class' => 'whiteLabel',
		'title' => '<span></span>' . Yii::t('templatemanager', 'White Label'),
		'data-tab' => 'whiteLabel',
		'data-url' => 'branding/whiteLabel',
	);
?>

<?php if($_SESSION['processFormErrors']) { ?>
<div class="row-fluid">
	<div class="alert alert-error">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<?php /* echo $form->errorSummary($model); */ ?>
		<?php echo str_replace('errorSummary', '', $_SESSION['processFormErrors']); $_SESSION['processFormErrors'] = null; ?>
	</div>
</div>
<?php } ?>
<div class="admin-advanced-wrapper">
	<div id="sidebar" class="advanced-sidebar">
		<ul>
			<?php 
				foreach ($sidebarLinks as $key => $link) {
					$title = $link['title'];
					unset($link['title']);
					if (isset($link['redirect']) && $link['redirect']) {
						echo '<li>' . CHtml::link($title, $link['data-url'], $link + array('data-loaded' => 0)) . '</li>';
					}	
					else {		
						echo '<li>' . CHtml::link($title, '#'. $link['data-tab'], $link + array('data-loaded' => 0)) . '</li>';
					}
				} 
			?>
		</ul>
	</div>
	<div id="main" class="advanced-main">
		<div class="content tab-content">
			<div id="signin">
				<?php $this->renderPartial('signin', $signin); ?>
			</div>
			<div id="layoutColors">
				<?php $this->renderPartial('layoutColors', $layout); ?>
			</div>
		</div>
	</div>
</div>
