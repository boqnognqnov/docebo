<div class="form-wrapper">
	<?php $form = $this->beginWidget('CActiveForm',
		array(
			'id' => 'branding-color-scheme-form',
			'htmlOptions' => array('enctype' => 'multipart/form-data')
		)
	); ?>

	<?php echo $form->label($model, 'title'); ?>
	<?php echo $form->textfield($model, 'title'); ?>

	<br/>
	<br/>

	<div class="instructions">
		<?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/colors_instructions.jpg', 'User interface colors instructions'); ?>
		<p class="hover"><?php echo Yii::t('branding', '_CS_INSTRUCTION_HOVER'); ?></p>
		<p class="selected"><?php echo Yii::t('branding', '_CS_INSTRUCTION_SELECTED'); ?></p>
		<p class="menu-headers"><?php echo Yii::t('branding', '_CS_INSTRUCTION_MENU_HEADERS'); ?></p>
		<p class="title"><?php echo Yii::t('branding', '_CS_INSTRUCTION_TITLE'); ?></p>
		<p class="action-btn"><?php echo Yii::t('branding', '_CS_INSTRUCTION_ACTION_BTN'); ?></p>
		<p class="other-btn"><?php echo Yii::t('branding', '_CS_INSTRUCTION_OTHER_BTN'); ?></p>
		<p class="secondary-btn"><?php echo Yii::t('branding', '_CS_INSTRUCTION_SECONDARY_BTN'); ?></p>
		<p class="text"><?php echo Yii::t('branding', '_CS_INSTRUCTION_TEXT'); ?></p>
		<p class="charts"><?php echo Yii::t('branding', '_CS_INSTRUCTION_CHARTS'); ?></p>
	</div>
	<h3><?php echo Yii::t('branding', '_COLORS_SCHEME'); ?></h3>
	<div class="colors">
		<?php foreach ($selection as $color) {
			$this->renderPartial('_colorView', array('model' => $color, 'form' => $form));
		} ?>
	</div>
</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
$(function() {

	// Init color picker tooltips
	removePrevFarbtasticListeners = true;
	$('#branding-color-scheme-form .color-tooltip').tooltip({
		title: function() {
			return $(this).closest('.wrap').find('.farbtastic-tooltip').html();
		},
		trigger:  'focus',
		html: true
	});

//	// Init farbtastic picker
//	$('#branding-color-scheme-form .color-tooltip').on('shown.bs.tooltip', function() {
//		schemeFormInit();
//		removePrevFarbtasticListeners = true;
//	});

	// Focus at color text box on click over color preview container
	$(document).on('click', '#branding-color-scheme-form .color-preview-wrap', function() {
		if ($(this).closest('.wrap').find('.core-scheme-color').length > 0) {
			$(this).closest('.wrap').find('.core-scheme-color').trigger('focus');
			//This is workaround for 'shown.bs.tooltip' callback which is called multiple times.
			// Now will be called once per tooltip.
			setTimeout(function(){
				schemeFormInit();
				removePrevFarbtasticListeners = true;
			}, 200);
		}
	});

});
</script>
