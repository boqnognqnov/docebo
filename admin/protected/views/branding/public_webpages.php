<?php /** @var $pageModel LearningWebpage */ ?>
<style>
    .public-webpages-grid .list-view .items > .row {
        margin-left: 0px;
    }
</style>
<div class="form-wrapper">
	<div class="main-actions clearfix" style="height: 150px;">
		<h3 class="title-bold"><?php echo Yii::t('admin_webpages', '_TITLE_WEBPAGES'); ?></h3>
		<ul class="clearfix">
			<li>
				<div>
					<?php
					echo CHtml::link('<span></span>'.Yii::t('admin_webpages', '_ADD_WEBPAGES'), $this->createUrl('addExternalPage'), array(
						'class' => 'new-node branding-external-page open-dialog',
						'data-dialog-class' => 'modal-branding-external-page branding-external-page',
						'removeOnClose' => 'true',
						'closeOnOverlayClick' => 'true',
						'closeOnEscape' => 'true',
					));
					?>
				</div>
			</li>
		</ul>
		<div class="info">
			<div>
				<h4></h4>
				<p></p>
			</div>
		</div>
	</div>

    <?php
        $catalog_external = Settings::get('catalog_external', 'off', false, false);
        if ($catalog_external == 'off'):
    ?>
    <div class="row-fluid" id="show-hide-webpages-warning">
        <div class="span12">
			<?php $this->widget('common.widgets.warningStrip.WarningStrip',
				array(
					'message'=>Yii::t('branding',
						'Web pages are not supported if ther Public Catalog is not enabled. Please, go to Catalog App settings page if you want to enable it.'),
					'type'=>'warning'
				)
			); ?>
        </div>
    </div>
    <?php endif; ?>

	<div id="grid-wrapper" class="public-webpages-grid">
		<div class="values items-sortable-wrapper blockable" id="main">
			<h6><?php echo Yii::t('configuration', '_PAGE_TITLE'); ?></h6>
			<?php $this->widget('zii.widgets.CListView', array(
				'id' => 'external-pages-management-list',
				'htmlOptions' => array('class' => 'list-view clearfix'),
				'dataProvider' => $pageModel->dataProvider(),
				'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
				'itemView' => '_view_webpages',
				'template' => '{items}',
				'ajaxUpdate' => true,
				'afterAjaxUpdate' => 'function(id, data) {
								setExternalPagesListSortable();
								$("#external-pages-management-list").controls();
								$(\'a[rel="tooltip"]\').tooltip();
							}'
			)); ?>
		</div>
	</div>
</div>
