<div class="row" data-page-id="<?php echo $data->id; ?>">
	<div class="title">
		<?php
			$currentLanguageTitle = trim($data->translation->title);
			if (!empty($currentLanguageTitle)) {
				echo $currentLanguageTitle;
			}
			else {
				echo '-';
			}
		?>
	</div>
	<div class="actions">
		<ul>
			<li class="move"><?= Yii::t('standard', '_MOVE'); ?></li>
            <?php if (PluginManager::isPluginActive('DomainBrandingApp')) : ?>
            <li class="visibility">
                <?php $this->renderPartial('//common/_modal', array(
                    'config' => array(
                        'class' => 'branding-page-visibility',
                        'modalTitle' => Yii::t('branding', 'Web page visibility for each node'),
                        'linkTitle' => Yii::t('branding', 'Edit visibility'),
                        'linkOptions' => array(
                            'class' => 'ajaxModal branding-page-visibility',
                        ),
                        'url' => 'branding/editPageVisibility&id=' . $data->id,
                        'buttons' => array(
                            array(
                                'type' => 'submit',
                                'title' => Yii::t('standard', '_CONFIRM'),
                            ),
                            array(
                                'type' => 'cancel',
                                'title' => Yii::t('standard', '_CANCEL'),
                            ),
                        ),
                        //'afterLoadingContent' => 'externalPageFormInit();',
                        'afterSubmit' => 'editPageVisibilityCallback',
                        //'beforeSubmit' => 'externalPageFormPrepare'
                    ),
                )); ?>
            </li>
            <?php endif; ?>
			<li class="<?php echo $data->publish == 1 ? 'enabled' : 'disabled'; ?>">
				<?php echo CHtml::link('Activate', 'javascript:void(0);', array('class' => 'toggle-active-state', 'data-url' => 'branding/togglePagePublish', 'data-id' => $data->id)); ?>
			</li>
			<li class="edit" data-id="<?php echo $data->translation->id; ?>">
				<?php /*$this->renderPartial('//common/_modal', array(
					'config' => array(
						'сlass' => 'branding-external-page',
						'modalTitle' => Yii::t('branding', 'Edit external page'),
						'linkTitle' => Yii::t('standard', '_MOD'),
						'linkOptions' => array(
							'class' => 'ajaxModal branding-external-page',
						),
						'url' => 'branding/editExternalPage&id=' . $data->id,
						'buttons' => array(
							array(
								'type' => 'submit',
								'title' => Yii::t('standard', '_SAVE'),
							),
							array(
								'type' => 'cancel',
								'title' => Yii::t('standard', '_CANCEL'),
							),
						),
						'afterLoadingContent' => 'externalPageFormInit();',
						'afterSubmit' => 'externalPageCallback',
						'beforeSubmit' => 'externalPageFormPrepare'
					),
				));*/ ?>
				<?php
				echo CHtml::link('<span class="i-sprite is-edit"></span>', $this->createUrl('editExternalPage', array('id' => $data->id)), array(
					'class' => 'edit-action open-dialog',
					'data-dialog-class' => 'modal-branding-external-page branding-external-page',
					'removeOnClose' => 'true',
					'closeOnOverlayClick' => 'true',
					'closeOnEscape' => 'true',
					//'title' => Yii::t('standard', '_MOD'),
					//'rel' => 'tooltip',
				));
				?>
			</li>
			<li class="delete">
				<?php echo CHtml::link("", "", array(
						'class' => 'ajaxModal delete-action',
						'data-toggle' => 'modal',
						'data-modal-class' => 'delete-node',
						'data-modal-title' => Yii::t('standard', '_DEL'),
						'data-buttons' => json_encode(array(
							array('type' => 'submit', 'title' => Yii::t('standard', '_CONFIRM')),
							array('type' => 'cancel', 'title' => Yii::t('standard', '_CANCEL')),
						)),
						'data-url' => 'branding/deletePage&id=' . $data->id,
						'data-after-loading-content' => 'hideConfirmButton();',
						'data-after-submit' => 'updateExternalPagesContent',
					)); ?>
				<?php //echo CHtml::link('Delete', 'javascript:void(0);', array('class' => 'delete-link', 'data-url' => 'branding/deletePage', 'data-id' => $data->page->id)); ?>
			</li>
		</ul>
	</div>
</div>