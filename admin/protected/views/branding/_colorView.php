<div class="color-item">
	<div class="wrap">
		<?php echo $form->textfield($model, 'color', array(
				'name' => 'CoreSchemeColor['.$model->color_id.']',
				'id' => 'CoreSchemeColor_'.$model->color_id,
				'class' => 'core-scheme-color color-tooltip',
				)
		); ?>
		<div id="farbtastic-tooltip-<?= $model->color_id; ?>" class="tooltip farbtastic-tooltip" style="opacity: 1; display: none;">
			<div class="colorpicker"></div>
		</div>
		<div class="color-preview-wrap">
			<div class="color-preview" style="display: block; width: 21px; height: 21px; background-color: <?php echo $model->color; ?>;"></div>
		</div>
	</div>

	<p class="title"><?php
	switch ($model->color_id) {
		/*
			(1, 'Menu & Headers', 'Mainly used in Menu Bar and popup headers', '#333333'),
			(2, 'Title text', 'Used for all the titles', '#0465AC'),
			(3, 'Text', 'Main text colour', '#333333'),
			(4, 'Hover item', 'Used for hover items', '#666666'),
			(5, 'Selected items', 'Used for selected items', '#333333'),
			(6, 'Action buttons', 'Used for buttons like confirm, save, add...', '#333333'),
			(7, 'Secondary buttons', 'Used for buttons like cancel, previous, go back, etc...', '#333333'),
			(8, 'Other buttons', 'Used for other buttons', '#666666'),
			(9, 'Charts 1', 'Mainly used in charts', '#003D6B'),
			(10, 'Charts 2', 'Mainly used in charts', '#0465AC'),
			(11, 'Charts 3', 'Mainly used in charts', '#52A1DD');
			(12, 'Menu header background', 'Background color for menu header', '#555555');
		*/
		case 1 : { echo Yii::t('branding', '_CS_INSTRUCTION_MENU_HEADERS'); };break;
		case 2 : { echo Yii::t('branding', '_CS_INSTRUCTION_TITLE'); };break;
		case 3 : { echo Yii::t('standard', '_TEXTOF'); };break;
		case 4 : { echo Yii::t('branding', '_CS_INSTRUCTION_HOVER'); };break;
		case 5 : { echo Yii::t('branding', '_CS_INSTRUCTION_SELECTED'); };break;
		case 6 : { echo Yii::t('branding', '_CS_INSTRUCTION_ACTION_BTN'); };break;
		case 7 : { echo Yii::t('branding', '_CS_INSTRUCTION_OTHER_BTN'); };break;
		case 8 : { echo Yii::t('branding', '_CS_INSTRUCTION_SECONDARY_BTN'); };break;
		case 9 : { echo Yii::t('branding', '_CS_INSTRUCTION_CHARTS'); };break;
		case 10 : { echo Yii::t('branding', '_CS_INSTRUCTION_CHARTS'); };break;
		case 11 : { echo Yii::t('branding', '_CS_INSTRUCTION_CHARTS'); };break;
		case 12 : { echo Yii::t('branding', 'Background color for menu header'); };break;
	}?></p>
</div>