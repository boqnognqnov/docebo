<div class="form-wrapper">
	<div class="section">
		<h3><?= Yii::t('templatemanager', 'White Label'); ?></h3>

		<div class="row odd has-border">
			<div class="row">
				<div class="row-fluid no-space">
					<div class="span2"></div>
					<div class="span8 text-center white-label-inactive-title"><?= Yii::t('templatemanager', 'Do you want to White Label your Docebo?') ?></div>
				</div>
				<div class="row-fluid no-space">
					<div class="span2"></div>
					<div class="span8">
						<ul class="white-label-inactive-text">
							<li>
								<div class="span1"><i class="i-sprite is-check green"></i></div><div class="span11"> <?= Yii::t('templatemanager', 'Hide/Customize "Powered by Docebo" in the Footer') ?></div>
							</li>
							<li>
								<div class="span1"><i class="i-sprite is-check green"></i></div><div class="span11"><?= Yii::t('templatemanager', 'Hide BUY NOW, Course Marketplace, APPS buttons from the menu and the users counter label from header') ?></div>
							</li>
							<li>
								<div class="span1"><i class="i-sprite is-check green"></i></div><div class="span11"><?= Yii::t('templatemanager', 'Choose the recipient for the help desk area requests') ?></div>
							</li>
							<li>
								<div class="span1"><i class="i-sprite is-check green"></i></div><div class="span11"><?= Yii::t('templatemanager', 'Remove the word Docebo from your LMS') ?></div>
							</li>
						</ul>
					</div>
				</div>
				<div class="row-fluid no-space">
					<div class="span2"></div>
					<div class="span8 text-center">
						<a href="<?= Docebo::createLmsUrl('app/index', array('#'=>'docebo_additional_features')) ?>"class="branding-orange-btn"><?= Yii::t('templatemanager', 'Get this App') ?></a>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>