<?php
$textAreaId = 'pageDescription-' . uniqid();
?>
<h1>
	<?= $modalTitle ?>
</h1>

<div class="form add-external-page-dialog">

	<?php $form = $this->beginWidget('CActiveForm',
		array(
			'id' => 'branding-signin-form',
			'htmlOptions' => array(
				'class' => 'ajax branding-external-page-form',
				'enctype' => 'multipart/form-data'
			)
		)
	); ?>

	<div class="languages">
		<div class="lang-wrapper">
			<label for="external-pages-lang-select">
				<?php echo Yii::t('branding', '_PAGE_LANGUAGE_LABEL'); ?>
			</label>
			<br />
			<?php //echo CHtml::listBox('languages', Lang::getCodeByBrowserCode(Yii::app()->getLanguage()), array('0' => Yii::t('adminrules', '_SELECT_LANG_TO_ASSIGN')) + $languages, array('size' => 1, 'id' => 'external-pages-lang-select', 'class' => 'language-selector')); ?>
			<?php echo CHtml::listBox('languages', '0', array('0' => Yii::t('adminrules', '_SELECT_LANG_TO_ASSIGN')) + $languages, array('size' => 1, 'id' => 'external-pages-lang-select', 'class' => 'language-selector')); ?>
			<div class="stats">
				<p class="available"><?php echo Yii::t('admin_lang', '_LANG_ALL'); ?>: <span><?php echo count($languages); ?></span></p>
				<p class="assigned"><?php echo Yii::t('standard', 'Filled languages'); ?>: <span></span></p>
			</div>
			<br />
		</div>
		<div class="input-fields">
			<div class="clearfix">
				<label for="title"><?php echo Yii::t('standard', '_TITLE') ?></label>
				<div class="max-input">
					<?php echo CHtml::textfield('title', '', array('class' => 'title')); ?>
				</div>
			</div>
			<div class="clearfix">
				<label for="description"><?php echo Yii::t('htmlpage', '_SECT_PAGE') ?></label>
				<?php echo CHtml::textarea('description', '', array('class' => 'text', 'id' => $textAreaId)); ?>
			</div>
		</div>
		<div class="translations" style="display: none;">
			<?php foreach ($translations as $translation): ?>
				<div class="<?php echo $translation->lang_code; ?>">
					<?php echo $form->textField($translation, 'title', array('name' => "LearningWebpageTranslation[$translation->lang_code][title]")); ?>
					<?php echo $form->textArea($translation, 'description', array('name' => "LearningWebpageTranslation[$translation->lang_code][description]")); ?>
					<?php echo $form->hiddenField($translation, 'page_id', array('name' => "LearningWebpageTranslation[$translation->lang_code][page_id]")); ?>
					<?php echo $form->hiddenField($translation, 'lang_code', array('name' => "LearningWebpageTranslation[$translation->lang_code][lang_code]")); ?>
					<?php echo $form->hiddenField($translation, 'id', array('name' => "LearningWebpageTranslation[$translation->lang_code][id]")); ?>
				</div>
			<?php endforeach; ?>
		</div>
		<?php
		if (!$isEditing) {
			//if we are creating a new web page then specify its sequence number
			echo CHtml::hiddenField('LearningWebpage[sequence]', ($maxSequence + 1));
		}
		?>
	</div>

	<div class="publish-checkbox-wrapper">
		<?php echo CHtml::activeCheckbox($model, 'publish', false); ?>
		<label for="LearningWebpage_publish"><?php echo Yii::t('standard', '_PUBLISH'); ?></label>
	</div>


	<div class="form-actions">
		<input class="btn-docebo green big" type="submit" value="<?= Yii::t('standard', '_CONFIRM') ?>" />
		<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
	</div>

	<?php $this->endWidget(); ?>

</div>

<script type="text/javascript">
	$(function() {

		function modalPopulateAssignedLanguages() {
			if ($('.section.signin').length == 0) { return false; }

			var count = 0;
			var set = $('.modal-branding-external-page').find('.translations > div');
			var assigned = $('.modal-branding-external-page').find('.assigned span');

			set.each(function() {
				if ($(this).find('input').val() != "" || $(this).find('textarea').val() != "") {
					count++;
				}
			});

			assigned.html(count);
		}

		$('.modal-branding-external-page .input-fields .title, .input-fields .text').live('blur', function() {
			$(this).closest('.languages').find('select.language-selector').trigger('change');
			modalPopulateAssignedLanguages();
		});


		//initialize contents

		modalPopulateAssignedLanguages();

        TinyMce.attach('#<?= $textAreaId ?>', {height: 275, setup: function (ed) {
            ed.on('init', function(args) {
                ed.on('blur', function(e) {
                    //required to fill inputs with latest editor's content
                    var select = $('.modal-branding-external-page select.language-selector');
                    if (select.length > 0) {
                        var inputTitle = $('.modal-branding-external-page input.title').val();
                        var inputDescription = tinymce.activeEditor.getContent();
                        var lang = select.val();
                        $('#LearningWebpageTranslation_'+lang+'_title').val(inputTitle);
                        $('#LearningWebpageTranslation_'+lang+'_description').val(inputDescription);
                    }
                });
            });
        }});

		$('.modal-branding-external-page .publish-checkbox-wrapper input').styler();
		$('.modal-branding-external-page select').styler();

		//clean possible delegated events from previous dialogs
		$(document).undelegate('.modal-branding-external-page', "dialog2.content-update");
		$(document).undelegate('.modal-branding-external-page', "dialog2.closed");

		//set dialog behaviors on server answer
		$(document).delegate(".modal-branding-external-page", "dialog2.content-update", function() {
			var e = $(this), autoclose = e.find("a.auto-close");
			if (autoclose.length > 0) {
				e.find('.modal-body').dialog2("close");
				$.fn.yiiListView.update("external-pages-management-list");
			} else {
				var err = e.find("a.error");
				if (err.length > 0) {
					var msg = $(err[0]).data('message');
					Docebo.Feedback.show('error', msg);
					e.find('.modal-body').dialog2("close");
				}
			}
		});

		// On closing the dialog we must destroy TinyMce editors;
		$(document).delegate(".modal-branding-external-page", "dialog2.closed", function() {
			TinyMce.removeEditorById('#<?= $textAreaId ?>');
			//clean delegated events
			$(document).undelegate('.modal-branding-external-page', "dialog2.content-update");
			$(document).undelegate('.modal-branding-external-page', "dialog2.closed");
		});
	});
</script>