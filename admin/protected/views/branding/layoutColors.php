<?php echo $this->renderPartial('//common/_brandingColorsSchemeActions', false, true); ?>
<h5><?= Yii::t('branding', '_COLORS_SCHEME'); ?></h5>
<p class="layout-colors-title"><strong><?php echo Yii::t('standard', '_TITLE'); ?></strong></p>
<div class="form-wrapper">
	<?php $this->widget('zii.widgets.CListView', array(
		'id' => 'schemes-management-list',
		'htmlOptions' => array('class' => 'list-view clearfix'),
		'dataProvider' => $schemeModel->dataProvider(),
		'itemView' => '_viewScheme',
		// 'itemsCssClass' => 'items clearfix',
		'template' => '{items}{summary}{pager}',
		'pager' => array('class' => 'common.components.DoceboCLinkPager'),
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
		'ajaxUpdate' => true,
	)); ?>
</div>