<style>
	#header_message_settings{
		margin-top: 10px;
	}
	.orgChart_languages > div:first-of-type{
		width: 55%;
		display: inline-block;
	}
	.orgChart_languages > div:nth-of-type(2){
		width: 41%;
		display: inline-block;
	}
	.orgChart_languages{
		margin-bottom: 10px;
	}
</style>
<input type="hidden" id="languages" value="<?=(implode(',', array_keys($languages)))?>">
<div class="form-wrapper">
	<?php $form = $this->beginWidget('CActiveForm',
		array(
			'id' => 'settings-password-form',
			'htmlOptions' => array('enctype' => 'multipart/form-data')
		)
	); ?>

	<div class="messages"><?php echo $form->errorSummary($model); ?></div>

	<div class="section logo">
		<?php
		if(isset($_SESSION['processFormErrors'])) {
			echo $_SESSION['processFormErrors'];
			unset($_SESSION['processFormErrors']);
		} ?>
		<h5><?= Yii::t('templatemanager', 'Logo'); ?></h5>
		<div class="row even">
			<div class="setting-name">
				<?php echo $form->labelEx($model, 'page_title'); ?>
				<p class="description"><?php echo Yii::t('branding', '_PAGE_TITLE_DESCRIPTION'); ?></p>
			</div>
			<div class="values max-input">
			<p><?= Yii::t('standard', '_TITLE'); ?></p>
				<?php echo $form->textfield($model, 'page_title'); ?>
			</div>
		</div>

		<div class="row odd has-border">
			<div class="row">
				<div class="setting-name">
					<?php echo $form->labelEx($model, 'company_logo'); ?>
					<p class="description"><?php echo Yii::t('branding', '_COMPANY_LOGO_DESCRIPTION'); ?></p>
				</div>
				<div class="values">
					<div class="current">
						<?php echo Yii::app()->theme->renderLogo(CoreAsset::VARIANT_SMALL, 'Docebo', true); ?>
					</div>
					<?php echo $form->fileField($model, 'logo'); ?>
				</div>
			</div>
		</div>

		<div class="row even">
			<div class="setting-name">
				<?php echo $form->labelEx($model, 'header_message_active'); ?>
				<p class="description"><?= Yii::t('branding', 'Add here an header message that will be displayed underneath the logo (make sure it fits within your Sign In Form layout)') ?></p>
			</div>
			<div class="values max-input">
				<div class="current">

				</div>
				<label style="margin-left: 0">
					<?= CHtml::activeCheckBox($model, 'header_message_active', array('checked' => Settings::get('header_message_active'))) ?>
					<?= Yii::t('standard', 'Enable') . ' <span style="text-transform:lowercase">' . Yii::t('templatemanager', 'Header') . ' ' . Yii::t('htmlframechat', '_MSGTXT') . '</span>'; ?>
				</label>

				<div id="header_message_settings">
					<div class="orgChart_languages">
						<div>
							<strong><?= Yii::t('branding', '_PAGE_LANGUAGE_LABEL') ?></strong>
							<?= $form->dropDownList($model, 'language', $languages, array('options' => array($defaultLanguage => array('selected' => true)), 'id' => 'custom_text_header', 'style'=>'margin-top:5px')) ?>
						</div>
						<div>
							<?= Yii::t('admin_lang', '_LANG_ALL') . ': <span>' . count($languages) . '</span>'; ?>
							<div id="orgChart_assignedCount"><?php echo Yii::t('standard', 'Filled languages') . ' <span>' . count($languagesValues) . '</span>'; ?></div>
						</div>
					</div>
					<div style="margin-bottom: 5px">
						<?= Yii::t('htmlframechat', '_MSGTXT') ?>
					</div>

					<?= CHtml::textArea('header_message_area') ?>
				</div>
			</div>
		</div>

        <div class="row odd has-border">
			<div class="row">
				<div class="setting-name">
					<?php echo $form->labelEx($model, 'favicon'); ?>
					<p class="description"><?= Yii::t('branding', 'Upload a 16x16 png/ico image that will represent your website\'s favicon.') ?></p>
				</div>
				<div class="values max-input">
					<div class="current">
						<?php echo Yii::app()->theme->renderFavicon(true, CoreAsset::VARIANT_ORIGINAL, null, true); // Render here the default platform favicon (no override) ?>
					</div>
					<?= $form->fileField($model, 'favicon') ?>
				</div>
			</div>
		</div>

		<div class="actions right-buttons">
			<?php echo CHtml::hiddenField('selectedTab', '', array('id' => 'selectedTab')); ?>
			<?php echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class' => 'btn-save', 'id' => 'logoSave')); ?>
		</div>
    </div>
	<?php $this->endWidget(); ?>
	<?php
	$languagesValues = json_encode($languagesValues, JSON_HEX_APOS);
	?>
</div>
<script type="text/javascript">
	var translationsList = <?=$languagesValues?>;
	var languages = $('#languages').val().split(',');

	$(function(){
		$('input#BrandingLogoForm_logo').styler({browseText:'<?php echo Yii::t('setup', 'Upload your logo');?>'});
        $('input#BrandingLogoForm_favicon').styler({browseText:'<?php echo Yii::t('setup', 'Upload your favicon');?>'});
		$('input').styler();
		setTimeout(function(){
			initTinyMCE('#header_message_area', 150);
		}, 2000)

		$(document).on('change', '#BrandingLogoForm_header_message_active', function(){
			var that = $(this);
			if(that.is(':checked')){
				$('#header_message_settings').show();
			}else{
				$('#header_message_settings').hide();
			}
		});

		tinymce.EditorManager.on("AddEditor", function (e) {
			var editor = e.editor;

			if (editor.id == 'header_message_area') {
				editor.on('input change', function (e) {
					var value = editor.getContent();
					var lang = $('#custom_text_header').val();
					$('input[name="valuesForHeaderMsg[' + lang + ']"]').val(value);
				});
			}
		});

		// create hidden fields for custom text inputs
		$.each(languages, function (index, lang) {
			var value = translationsList[lang];
			if (value != undefined) {
				$('<input>').attr({
					type: 'hidden',
					name: 'valuesForHeaderMsg[' + lang + ']',
					value: value
				}).appendTo('#header_message_settings');
			}else{
				$('<input>').attr({
					type: 'hidden',
					name: 'valuesForHeaderMsg[' + lang + ']',
					value: ''
				}).appendTo('#header_message_settings');
			}
		});

		var time = setInterval(function () {
			if (tinymce.activeEditor != null) {
				$('#custom_text_header').trigger('change');
				clearInterval(time);
			}
		}, 500);

		$(document).on('change', '#custom_text_header', function(){
			if(headerMessageMce = tinyMCE.get('header_message_area'))
			    headerMessageMce.focus();

			var chosenLanguage = $(this).val();
			var newVal = '';
			newVal = $('input[name="valuesForHeaderMsg[' + chosenLanguage + ']"]').val();
			tinymce.activeEditor.setContent(newVal);
		});

		var checkboxHeaderActive = $('#BrandingLogoForm_header_message_active');
		if(checkboxHeaderActive.is(':checked')){
			$('#header_message_settings').show();
		}else{
			$('#header_message_settings').hide();
		}
	});
</script>
