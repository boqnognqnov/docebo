<h3>Customize CSS</h3>
<div class="form-wrapper">
	<?php $form = $this->beginWidget('CActiveForm',
		array(
			'id' => 'branding-customize-css-form',
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
		)
	); ?>
	<div class="section customize-css">
		<div class="row odd has-border">
			<div class="row">
				<div class="setting-name">
					<p class="description"><?php echo Yii::t('templatemanager', '_CUSTOM_CSS_DESCRIPTION'); ?></p>
				</div>
				<div class="values modify-css">
					<?php echo $form->labelEx($model, 'custom_css'); ?>
					<?php echo $form->textArea($model, 'custom_css'); ?>
				</div>
			</div>
		</div>

		<div class="row even">
			<div class="setting-name">
				<label><?php echo Yii::t('standard', '_DOWNLOAD') ?></label>
				<p class="description"><?php echo Yii::t('branding', '_CUSTOM_CSS_DOWNLOAD_DESCRIPTION'); ?></p>
			</div>
			<div class="values download-css">
			<?php if (!empty($model->cssFiles)): ?>
				<ul>
					<?php foreach ($model->cssFiles as $name => $css): ?>
						<li><div><?php echo $css['name']; ?><?php echo CHtml::link(Yii::t('standard', '_DOWNLOAD'), array('branding/downloadCss', 'name' => $name)); ?></div></li>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>
			</div>
		</div>
	</div>

	<div class="actions right-buttons">
	<?php echo CHtml::hiddenField('selectedTab', 'customizeCss'); ?>
		<?php echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class' => 'btn-save')); ?>
	</div>

	<?php $this->endWidget(); ?>
</div>