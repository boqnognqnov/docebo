<?php
/* @var $model LearningWebpage */
/* @var $dataProvider CArrayDataProvider */
?>
<div class="form-wrapper">
	<?php $form = $this->beginWidget('CActiveForm',
		array(
			'id' => '',
			'htmlOptions' => array()
		)
	); ?>

	<div class="toggle-nodes-wrapper">
		<label class="radio pull-left" style="margin-right: 50px;">
			<input type="radio" name="visible_nodes" value="all" data-hide-grid="on" <?= ($model->visible_nodes === 'all' ? 'checked' : '') ?> />
			All nodes
		</label>
		<label class="radio">
			<input type="radio" name="visible_nodes" value="selection" data-hide-grid="off" <?= ($model->visible_nodes === 'selection' ? 'checked' : '') ?> />
			Select a node
		</label>
	</div>

	<div class="branding-visibility-grid-wrapper">
		<hr/>

		<div class="selections clearfix">
			<?php $this->widget('ext.local.widgets.GridViewSelectAll', array(
				'gridId' => 'branding-page-visibility-grid',
				'class' => 'left-selections clearfix',
				'dataProvider' => $dataProvider,
				'itemValue' => 'idOrg',
			)); ?>
		</div>

		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'branding-page-visibility-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
			'dataProvider' => $dataProvider,
			'columns' => array(
				array(
					'selectableRows' => 2,
					'id' => 'branding-page-visibility-grid-checkboxes',
					//'type' => 'raw',
					'header' => '',
					'name' => 'idOrg',
					'value' => '$data->idOrg',
					'checked' => 'in_array($data->idOrg, Yii::app()->session["branding_nodes_visibility"])',
					'class' => 'CCheckBoxColumn',
				),
				array(
					'type' => 'raw',
					'header' => '',
					'name' => 'translation',
					'value' => '$data->coreOrgChart->translation'
				),
			),
			'template' => '{items}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'pager' => array(
				'class' => 'DoceboCLinkPager',
				'maxButtonCount' => 8,
			),
			'ajaxUpdate' => 'branding-page-visibility-grid-all-items',
			'beforeAjaxUpdate' => 'function(id, options) {
				// The Request type MUST be POST!
				options.type = "POST";
				if (typeof options.data == "undefined") {
					options.data = {};
				}
				// To allow passing selected items over pagination
				options.data.selectedItems = $(\'#\'+id+\'-selected-items-list\').val();
			}',
			'afterAjaxUpdate' => 'function(id, data){
			    $(\'a[rel="tooltip"]\').tooltip();
			    $(\'.popover-action\').popover({
					placement: \'bottom\',
					html: true
				});
				afterGridViewUpdate(\'branding-page-visibility-grid\');
			}'
		)); ?>
		</div>

	<?php $this->endWidget(); ?>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$('.toggle-nodes-wrapper').find('input').change(function () {
			$grid = $('.branding-visibility-grid-wrapper');
			if ($(this).data('hide-grid') === 'on') {
				$grid.addClass('hide').hide();
			} else {
				$grid.show();
			}
		}).filter(':checked').trigger('change');
	});
</script>