<?php
	$title 	= '<span class="enrolled"></span>'; 
	$url 	= Docebo::createLmsUrl('usersSelector/axOpen', array(
				'type' 	=> UsersSelector::TYPE_MODULE_MANAGEMENT,
				'idModule' 	=> $idModule,	
			));
	
	echo CHtml::link($title, $url, array(
		'class' => 'open-dialog group-select-users',
		'rel' 	=> 'users-selector',	
		'alt' 	=> Yii::t('helper', 'Group assign users'),
		'data-dialog-title' => Yii::t('standard', '_SELECT_USERS'),
		'title'	=> Yii::t('standard', '_SELECT_USERS'), 
	)); 
?>
