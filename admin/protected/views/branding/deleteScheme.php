<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'scheme_form',
	));	?>

	<p><?php echo Yii::t('standard', '_DEL'); ?></p>

	<div class="clearfix">
		<?php echo $form->checkbox($model, 'confirm', array('id' => 'scheme_confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo chtml::label(Yii::t('standard','Yes, I confirm I want to proceed'), 'scheme_confirm_'.$checkboxIdKey); ?>
		<?php echo $form->error($model, 'confirm'); ?>
	</div>

	<?php $this->endWidget(); ?>
</div>