<?php
/* @var $form CActiveForm */
/* @var $model BrandingCoursePlayerForm */
?>
<div class="form-wrapper">
	<?php $form = $this->beginWidget('CActiveForm',
		array(
			'id' => 'course-player-form',
			'htmlOptions' => array('enctype' => 'multipart/form-data')
		)
	); ?>

	<div class="messages"><?php echo $form->errorSummary($model); ?></div>

	<div class="section coursePlayer">

		<div class="row odd has-border">
			<div class="row">
				<div class="setting-name">
					<?php echo $form->labelEx($model, 'player_layout'); ?>
					<p class="description"><?php echo Yii::t('branding', "Choose the layout to to display your course's training material"); ?></p>
				</div>

				<div class="values">
					<div class="row-fluid">
						<div class="span4">
							<label style="margin-left:0; height: 55px; overflow: hidden;">
								<?=$form->radioButton($model, 'player_layout', array('value'=>LearningCourse::$PLAYER_LAYOUT_PLAY_BUTTON, 'uncheckValue'=>NULL))?>
								<?=Yii::t('branding', 'Player view (browser mode)')?>
							</label>
							<div class="player-layout-preview player"></div>
						</div>
						<div class="span4">
							<label style="margin-left:0; height: 55px; overflow: hidden;">
								<?=$form->radioButton($model, 'player_layout', array('value'=>LearningCourse::$PLAYER_LAYOUT_NAVIGATOR, 'uncheckValue'=>NULL))?>
								<?=Yii::t('branding', 'Player view (prev-next mode)')?>
							</label>
							<div class="player-layout-preview navigator"></div>
						</div>
						<div class="span4">
							<label style="margin-left:0; height: 55px; overflow: hidden;">
								<?=$form->radioButton($model, 'player_layout', array('value'=>LearningCourse::$PLAYER_LAYOUT_LISTVIEW, 'uncheckValue'=>NULL))?>
								<?=Yii::t('branding', 'List view')?>
							</label>
							<div class="player-layout-preview listview"></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row even">
			<div class="row">
				<div class="setting-name">
					<?php echo $form->labelEx($model, 'player_bg'); ?>
					<p class="description"><?php echo Yii::t('branding', 'Upload or select a default image for the course player background'); ?></p>
				</div>

				<!-- model = form model -->
				<input type="hidden" name="<?= get_class($model) . '[player_bg]' ?>" id="<?= get_class($model) . '_player_bg_id' ?>" value="<?= $model->player_bg ?>" />

				<div class="values">
					<p><?= Yii::t('templatemanager', 'Current background image') ?></p>
					<div class="row-fluid">
						<div class="span4">
							<div class="current">
								<?= $model->renderCoursePlayerBg(CoreAsset::VARIANT_SMALL) ?>
							</div>
						</div>
						<div class="span8">
							<?= $form->labelEx($model, 'player_bg_aspect', array(
								'label' => $form->radioButton($model, 'player_bg_aspect', array('id'=>'player_bg_aspect_opt_a', 'value'=>'fill', 'uncheckValue'=>NULL)) . ' ' . Yii::t('branding', '_HOME_IMG_POSITION_FILL', array('{w}'=>1045, '{h}'=>411)),
								'for' => 'player_bg_aspect_opt_a',
								'class' => 'radio'
							)) ?>
							<br>
							<?= $form->labelEx($model, 'player_bg_aspect', array(
								'label' => $form->radioButton($model, 'player_bg_aspect', array('id'=>'player_bg_aspect_opt_b', 'value'=>'tile', 'uncheckValue'=>NULL)) . ' ' . Yii::t('branding', '_HOME_IMG_POSITION_TILE'),
								'for' => 'player_bg_aspect_opt_b',
								'class' => 'radio'
							)) ?>
						</div>
					</div>

					<br>
					<a 	class="open-dialog btn btn-docebo green big"
					      rel="modal-player-bg"
					      data-dialog-class="modal-player-bg"
					      href="<?= Docebo::createAdminUrl('branding/axPlayerBg') ?>">
						<?= Yii::t('setup', 'Upload your background') ?>
					</a>

					<?php //echo $form->fileField($model, 'player_bg'); ?>
				</div>
			</div>
		</div>

		<div class="row odd has-border">
			<div class="row">
				<div class="setting-name">
					<?php echo $form->labelEx($model, 'html_page_css'); ?>
					<p class="description"><?php echo Yii::t('branding', 'HTML page CSS help'); ?></p>
				</div>
				<div class="values modify-css">
					<?php echo $form->textArea($model, 'html_page_css'); ?>
				</div>
			</div>
		</div>

		<div class="actions right-buttons">
			<?php echo CHtml::hiddenField('selectedTab', '', array('id' => 'selectedTab')); ?>
			<?php echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class' => 'btn-save')); ?>
		</div>
    </div>
	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
	 $('input').styler({browseText:'<?php echo Yii::t('setup', 'Upload your logo');?>'});
	 var oImageManagePanel = false;
</script>
