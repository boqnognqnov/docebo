<?php $form = $this->beginWidget('CActiveForm',
	array(
		'id' => 'branding-background-form',
		'htmlOptions' => array('enctype' => 'multipart/form-data')
	)
); ?>

<div class="courseEnroll-tabs">
	<ul class="nav nav-tabs">
		<li class="<?php echo $selectedTab == 'default' ? 'active' : ''; ?>">
			<a data-target=".courseEnroll-page-users" href="#">
				<?php echo Yii::t('templatemanager', 'Ours images collection'); ?> (<?php echo $count['default']; ?>)
			</a>
		</li>
		<li class="<?php echo $selectedTab == 'user' ? 'active' : ''; ?>">
			<a data-target=".courseEnroll-page-groups" href="#">
				<?php echo Yii::t('templatemanager', 'Your images collection'); ?> (<span id="user-images-count"><?php echo $count['user']; ?></span>)    
			</a>
		</li>
	</ul>

	<div class="select-user-form-wrapper">

		<div class="tab-content">
			<div class="courseEnroll-page-users tab-pane <?php echo $selectedTab == 'default' ? 'active' : ''; ?>">
				<h4><?php echo Yii::t('standard', '_IMAGES'); ?></h4>
				<div id="defaultSlider" class="carousel slide thumbnails-carousel">
					<!-- Carousel items -->
					<div class="carousel-inner">
						<?php if (!empty($defaultImages)) { ?>
							<?php foreach ($defaultImages as $key => $defaultThumbnail) { ?>
								<div class="item<?php echo ($key == 0) ? ' active' : ''; ?>">
									<?php foreach ($defaultThumbnail as $key => $imageUrl) {
										$html = CHtml::image($imageUrl);
										$html .= '<input type="radio" name="BrandingManageBackgroundForm[selectedImage]" value="' . $key . '" '. ($key == $model->home_login_img ? 'checked="checked"' : '') .'/>';

										echo '<div class="sub-item ' . ($key == $model->home_login_img ? 'checked' : '') .'">' . $html . '</div>';
									}
									?>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
					<!-- Carousel nav -->
					<a class="carousel-control left" href="#defaultSlider" data-slide="prev">&lsaquo;</a>
					<a class="carousel-control right" href="#defaultSlider" data-slide="next">&rsaquo;</a>
				</div>
			</div>
			<div class="courseEnroll-page-groups tab-pane <?php echo $selectedTab == 'user' ? 'active' : ''; ?>">
				<div class="upload-form-wrapper">
					<div class="upload-form">
						<label for="upload-btn"><?php echo Yii::t('branding', '_UPLOAD_BACKGROUND_BTN'); ?></label>
						<?php echo CHtml::fileField('BrandingManageBackgroundForm[attachments]', '', array(
								'class' => 'ajaxCrop custom-image',
								'data-class' => 'BrandingManageBackgroundForm',
								'data-width' => (Settings::get('catalog_external', 'off') == 'on')? 257 : 200,
								'data-height' => 82.66,
								'data-image-type' => CoreAsset::TYPE_LOGIN_BACKGROUND,
						)); ?>
					</div>
				</div>
				<div id="userSlider" class="carousel slide thumbnails-carousel">
					<!-- Carousel items -->
					<div class="carousel-inner">
						<?php if (!empty($userImages)) { ?>
							<?php foreach ($userImages as $key => $userThumbnail) { ?>
								<div class="item<?php echo ($key == 0) ? ' active' : ''; ?>">
									<?php foreach ($userThumbnail as $key => $imageUrl) {
										$html = CHtml::image($imageUrl);
										$html .= '<input type="radio" name="BrandingManageBackgroundForm[selectedImage]" value="' . $key . '" '. ($key == $model->home_login_img ? 'checked="checked"' : '') .' />';
										
										echo '<div class="sub-item ' . ($key == $model->home_login_img ? 'checked' : '') .'">' . $html;
										echo '<span class="deleteicon"><span class="i-sprite is-remove red"></span></span>';
										echo '</div>';
									}
									?>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
					<!-- Carousel nav -->
					<a class="carousel-control left" href="#userSlider" data-slide="prev">&lsaquo;</a>
					<a class="carousel-control right" href="#userSlider" data-slide="next">&rsaquo;</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">

	function updateControls(nav) {
		if (nav == 'show' ) {
			$('#userSlider .carousel-control.right').show();
			$('#userSlider .carousel-control.left').show();
		}
		else if (nav == 'hide' ) {
			$('#userSlider .carousel-control.right').hide();
			$('#userSlider .carousel-control.left').hide();
		}	
	}	
									
									
	$('input').styler({browseText: '<?= Yii::t('organization', 'Upload File')?>'});


	var navStatus = $('#userSlider .sub-item').length > 0 ? 'show' : 'hide';
	updateControls(navStatus);
	

	/**
	 * DELETE User image
	 */
	$(document).off('click', '#userSlider .deleteicon').on('click', '#userSlider .deleteicon', function(e) {

		var imageId = $(this).closest('.sub-item').find('input[type="radio"]').val();
		var form = $(this).closest('form');
		
	    var options = {
			type        : 'post', 
			dataType    : 'json',
			data: {
				delete_image	: true,
				image_id		: imageId
			}, 
			success     : function (res) {
				if (res && (res.success == true)) {
					$('#userSlider').find('input[type="radio"][value="'+ imageId +'"]').closest('.sub-item').remove();
					$('#user-images-count').text($('#userSlider .sub-item').length);
					var navStatus = $('#userSlider .sub-item').length > 0 ? 'show' : 'hide';
					updateControls(navStatus);
				}

				
			}
		};

	    e.stopPropagation();
	    form.ajaxForm(options).submit();
		return false;
		
	});
	

	
</script>