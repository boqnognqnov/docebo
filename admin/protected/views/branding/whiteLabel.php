<?php if(!$isInsideTabs): ?>
	<?php if($_SESSION['processFormErrors']) { ?>
        <div class="row-fluid">
            <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo str_replace('errorSummary', '', $_SESSION['processFormErrors']); $_SESSION['processFormErrors'] = null; ?>
            </div>
        </div>
	<?php } ?>
<div class="admin-advanced-wrapper">
    <div id="main" class="advanced-main" style="margin-left: 0px;">
        <div class="content tab-content">
<?php endif; ?>

<style>
<!--

	a[id^="pickfiles-"] {
		height: 30px;
		padding: 5px;
	}

	.max-input.show-hide-footer-text .orgChart_languages > div:first-of-type,
	.max-input.show-hide-header-url .orgChart_languages > div:first-of-type,
	.max-input.show-hide-footer-url .orgChart_languages > div:first-of-type {
		display: inline-block;
		width: 55%;
	}

	.max-input.show-hide-footer-text .orgChart_languages > div:nth-of-type(2),
	.max-input.show-hide-header-url .orgChart_languages > div:nth-of-type(2),
	.max-input.show-hide-footer-url .orgChart_languages > div:nth-of-type(2) {
		display: inline-block;
		width: 41%;
	}
-->
</style>
<?=DoceboUI::printFlashMessages();?>

<input type="hidden" id="languages" value="<?=(implode(',', array_keys($languages)))?>">
<div class="form-wrapper">
	<?php $form = $this->beginWidget('CActiveForm',
		array(
			'id' => 'branding-whitelabel-form',
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
			'action' => Yii::app()->createUrl('branding/whiteLabel'),
		)
	); ?>

    <?php if(!$isInsideTabs): ?>
    <input type="hidden" id="panel" name="panel" value="whiteLabel">
    <?php endif; ?>

	<div class="section">
		<h3><?= Yii::t('templatemanager', 'White Label'); ?></h3>
		<div class="row odd has-border" style="display: none;">
			<div class="row">
				<div class="setting-name">
					<p class="description"><?php echo Yii::t('branding', 'Hide some Docebo\'s custom branding elements from your Docebo E-Learning Platform'); ?></p>
				</div>
				<div class="values">
					<span class="wlabel-preview wl-menu"><?=Yii::t('adminrules', '_ADMIN_MENU')?></span>
					<span class="wlabel-preview wl-user-counter"><?=Yii::t('templatemanager', 'User Counter')?></span>
					<span class="wlabel-preview wl-footer"><?=Yii::t('templatemanager', 'Foooter')?></span>
					<div class="white-label-preview"></div>
				</div>
			</div>
		</div>

		<div class="row even">
			<div class="setting-name">
				<?php echo Yii::t('templatemanager', 'Header'); ?>
			</div>
			<div class="values">
				<?php echo $form->radioButtonList($model, 'whitelabel_header',
					array(
						'current_header' => Yii::t('templatemanager', 'Use current header'),
						'external_header' => Yii::t('templatemanager', 'Load an external custom header (iframe)'),
					)); ?>
				<div class="max-input show-hide-header-url">
					<div class="orgChart_languages">
						<div>
							<strong><?= Yii::t('branding', '_PAGE_LANGUAGE_LABEL') ?></strong>
							<?= $form->dropDownList($model, 'languageForUrlHeader', $languagesForUrlHeader, array('options' => array($defaultLanguage => array('selected' => true)),'id'=>'customurlHeader')) ?>
						</div>
						<div>
							<?= Yii::t('admin_lang', '_LANG_ALL') . ': <span>' . count($languagesForUrlHeader) . '</span>'; ?>
							<div id="orgChart_assignedCount"><?php echo Yii::t('standard', 'Filled languages') . ' <span>' . count($translationsListForURLHeader) . '</span>'; ?></div>
						</div>
					</div>
					<br>
					<?php echo $form->textField($model, 'whitelabel_header_ext_url', array()) ?>
					<br />
					<div class="max-input-footnote"><?=Yii::t('templatemanager', 'Write here the external URL for the custom header'); ?></div>
					<br />
					<div class="row-fluid">
						<div class="span4"><?=Yii::t('templatemanager', 'Header') ?> <?=Yii::t('templatemanager', 'maximum width') ?></div>
						<div class="span8"><?=Yii::t('templatemanager', 'Header') ?> <?=Yii::t('templatemanager', 'height') ?></div>
					</div>
					<div class="row-fluid">
						<div class="span4">1100px</div>
						<div class="span8"><?php echo $form->textField($model, 'whitelabel_header_ext_height', array('style' => "width: 80px", 'min' => 50)) ?> px</div>
					</div>
					<div class="row-fluid">
						<div class="span4"><?=Yii::t('templatemanager', 'Width is responsive'); ?></div>
						<div class="span8"><?=Yii::t('templatemanager', 'Min height') ?>: 50px</div>
					</div>
					<?php foreach($languages as $key=>$lang){ ?>
					<input type="hidden" name="valuesForUrlHeader[<?=$key?>]" value="<?=($translationsListForURLHeader[$key]['value']) ? $translationsListForURLHeader[$key]['value'] : 'http://' ?>">
					<?php } ?>
				</div>
			</div>
		</div>

		<div class="row odd has-border">
			<div class="row">
				<div class="setting-name">
					<?php echo Yii::t('templatemanager', 'Foooter'); ?>
				</div>
				<div class="values">
					<?php
					echo $form->radioButton($model, 'whitelabel_footer',	array('value' => 'show_docebo',
						'id' => 'whitelabel_footer_show_docebo',
						'uncheckValue' => null
					));
					echo CHtml::label(Yii::t('branding', 'Show "Powered by Docebo" in the footer', array('{year}'=>date('Y'))),'whitelabel_footer_show_docebo');
					echo '<br />';

					echo $form->radioButton($model, 'whitelabel_footer',	array('value' => 'hide_docebo',
						'id' => 'whitelabel_footer_hide_docebo',
						'uncheckValue' => null
					));
					echo CHtml::label(Yii::t('branding', 'Hide "Powered by Docebo" in the footer', array('{year}'=>date('Y'))),'whitelabel_footer_hide_docebo');
					echo '<br />';

					echo $form->radioButton($model, 'whitelabel_footer',	array('value' => 'custom_text',
						'id' => 'whitelabel_footer_custom_text',
						'uncheckValue' => null
					));
					echo CHtml::label(Yii::t('branding', 'Use customized text in the footer'),'whitelabel_footer_custom_text');

					?>
					<div class="max-input show-hide-footer-text">
						<div class="orgChart_languages">
							<div>
								<strong><?= Yii::t('branding', '_PAGE_LANGUAGE_LABEL') ?></strong>
								<?= $form->dropDownList($model, 'language', $languages, array('options' => array($defaultLanguage => array('selected' => true)), 'id'=>'customtext')) ?>
							</div>
							<div>
								<?= Yii::t('admin_lang', '_LANG_ALL') . ': <span>' . count($languages) . '</span>'; ?>
								<div id="orgChart_assignedCount"><?php echo Yii::t('standard', 'Filled languages') . ' <span>' . count($translationsList) . '</span>'; ?></div>
							</div>
						</div>
						<br/>
						<?php
						$valueForDefaultLanguage = !empty($translationsList[$defaultLanguage]['value']) ? $translationsList[$defaultLanguage]['value'] : '';
						?>
						<?= $form->textArea($model, 'whitelabel_footer_text', array('id' => 'tiny_whitelabel_footer_text', 'value'=>$valueForDefaultLanguage)); ?>

					</div>
					<br />
					<?php echo $form->radioButton($model, 'whitelabel_footer',	array('value' => 'custom_url',
						'id' => 'whitelabel_footer_custom_url',
						'uncheckValue' => null
						));
					echo CHtml::label(Yii::t('branding', 'Load an external custom footer (iframe)'),'whitelabel_footer_custom_url'); ?>
					<div class="max-input show-hide-footer-url">
						<div class="orgChart_languages">
							<div>
								<strong><?= Yii::t('branding', '_PAGE_LANGUAGE_LABEL') ?></strong>
								<?= $form->dropDownList($model, 'languageForUrl', $languagesForUrl, array('options' => array($defaultLanguage => array('selected' => true)),'id'=>'customurl')) ?>
							</div>
							<div>
								<?= Yii::t('admin_lang', '_LANG_ALL') . ': <span>' . count($languagesForUrl) . '</span>'; ?>
								<div id="orgChart_assignedCount"><?php echo Yii::t('standard', 'Filled languages') . ' <span>' . count($translationsListForURL) . '</span>'; ?></div>
							</div>
						</div>
						<br>
						<?php
						$valueForDefaultLanguage = !empty($translationsListForURL[$defaultLanguage]['value']) ? $translationsListForURL[$defaultLanguage]['value'] : '';
						?>
						<?= $form->textField($model, 'whitelabel_footer_ext_url', array('value' => $valueForDefaultLanguage)) ?>
						<?php
						foreach($languages as $key=>$lang){ ?>
							<input type="hidden" name="valuesForUrl[<?=$key?>]" value="<?=($translationsListForURL[$key]['value']) ? $translationsListForURL[$key]['value'] : 'http://' ?>">
						<?php } ?>

						<br />
						<div class="max-input-footnote"><?=Yii::t('templatemanager', 'Write here the external URL for the custom footer'); ?></div>
						<br />
						<div class="row-fluid">
							<div class="span4"><?=Yii::t('templatemanager', 'Foooter') ?> <?=Yii::t('templatemanager', 'maximum width') ?></div>
							<div class="span8"><?=Yii::t('templatemanager', 'Foooter') ?> <?=Yii::t('templatemanager', 'height') ?></div>
						</div>
						<div class="row-fluid">
							<div class="span4">1100px</div>
							<div class="span8"><?php echo $form->textField($model, 'whitelabel_footer_ext_height', array('style' => "width: 80px", 'min' => 30)) ?> px</div>
						</div>
						<div class="row-fluid">
							<div class="span4"><?=Yii::t('templatemanager', 'Width is responsive'); ?></div>
							<div class="span8"><?=Yii::t('templatemanager', 'Min height') ?>: 30px</div>
						</div>

					</div>
				</div>
			</div>
		</div>

		<div class="row even">
			<div class="row-fluid">
				<div class="setting-name">
					<?php echo Yii::t('adminrules', '_ADMIN_MENU'); ?>
				</div>
				<div class="values">
					<?php echo $form->checkbox($model, 'whitelabel_menu', array('id' => 'whitelabel_menu'));?>
					<?php if(Yii::app()->legacyWrapper->hydraFrontendEnabled()): ?>
						<label for="whitelabel_menu"><?php echo Yii::t('branding', 'Hide (except for me - {username}) BUY NOW, Course Marketplace, APPS buttons from the admin menu, the users counter label from the header and the white label page (this page)', array(
								'{username}' => Yii::app()->user->getUsername(),
								'{userName}' => Yii::app()->user->getUsername(),
							)); ?></label>
					<?php else: ?>
						<label for="whitelabel_menu"><?php echo Yii::t('branding', 'Hide except for me', array(
								'{username}' => Yii::app()->user->getUsername(),
								'{userName}' => Yii::app()->user->getUsername(),
							)); ?></label>
					<?php endif; ?>
				</div>
				<div class="values">
					<br />
					<?= Yii::t('templatemanager', 'Checking this option will also enable the possibility to change the email address of the "Contact Us" option in the Communication Center.'); ?>
				</div>
			</div>

			<div class="row-fluid" id="helpdesk_mail_setting">
				<br />
				<div class="setting-name">
					<?php echo Yii::t('standard', 'Contact us'); ?>
					<br />
					<br />
					<span class="setting-description">
						<?= Yii::t('templatemanager', 'This email will be used as contact by all Super Administrators except the current one.'); ?>
					</span>
				</div>
				<div class="values">
					<?php echo $form->radioButtonList($model, 'whitelabel_helpdesk',
						array(
							'to_docebo' => Yii::t('branding', 'Send the request to Docebo'),
							'to_email' => Yii::t('branding', 'Send the requests to a specific email address'),
						)); ?>
					<div class="max-input show-hide-helpdesk-email">
						<br>
						<?php echo $form->textField($model, 'whitelabel_helpdesk_email', array()) ?>
					</div>
				</div>
			</div>

		</div>

		<div class="row odd has-border">
			<div class="row">
				<div class="setting-name">
					<?php echo Yii::t('templatemanager', 'Naming'); ?>
				</div>
				<div class="values">
					<?php echo $form->checkbox($model, 'whitelabel_naming', array('id' => 'whitelabel_naming'));?>
					<label for="whitelabel_naming"><?php echo Yii::t('branding', 'Replace, in every page of your E-Learning platform, the word <b>Docebo</b> with a custom word'); ?></label>
					<div class="max-input show-hide-naming-input">
						<br>
						<?php echo $form->checkbox($model, 'whitelabel_disable_naming', array('id' => 'whitelabel_disable_naming'));?>
						<label for="whitelabel_disable_naming"><?php echo Yii::t('branding', 'Do not replace the text in translations containing hyperlinks'); ?></label>
						<br>
						<?php echo $form->textField($model, 'whitelabel_naming_text', array()) ?>
					</div>
					<br/>

					<?php echo $form->checkbox($model, 'whitelabel_naming_site_enable', array('id' => 'BrandingWhiteLabelForm_whitelabel_naming_site_enable'));?>
					<label for="BrandingWhiteLabelForm_whitelabel_naming_site_enable" class="no-space">
						<?php echo Yii::t('branding', 'Replace, where used, the website link <b>www.docebo.com</b> with a custom URL'); ?>
					</label>
					<div class="max-input show-hide-naming-site-input">
						<br/>
						<?php echo $form->textField($model, 'whitelabel_naming_site', array()) ?>
					</div>
				</div>
			</div>
		</div>
		<div class="actions right-buttons">
			<?php echo CHtml::hiddenField('selectedTab', '', array('id' => 'selectedTab')); ?>
			<?php echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class' => 'btn-save')); ?>
		</div>
	</div>
	<?php $this->endWidget(); ?>
</div>

<script type="text/javascript">
		$('input').styler();

		$(document).ready(function() {
			// scroll to the top of the page if there is an error
			if ($('.alert-error').length && window.location.hash) {
				$(document).scroll( function() {
					if (window.location.hash == '#whiteLabel') {
						document.body.scrollTop = document.documentElement.scrollTop = 0;
					}
					$(document).unbind("scroll");
				});
			}
		});

		// PlUpload callback (see the widget above)
		function updateUploadedSplash(id, file, up) {
			var imgSrc = '<?= Docebo::createAbsoluteLmsUrl('stream/image') ?>' + '&fn=' + file.name + '&col=uploads';
			$('img#splash-screen-image').attr('src', imgSrc);
		}


		if ('<?= $model->whitelabel_mobile ?>' == '<?= BrandingWhiteLabelForm::MOBILE_OWN_APP ?>') {
			$('#own-app-options').show();
		}
		else {
			$('#own-app-options').hide();
		}

		$(document).on('change', 'input[name="BrandingWhiteLabelForm[whitelabel_mobile]"]', function(){
			if ($(this).val() == '<?= BrandingWhiteLabelForm::MOBILE_OWN_APP ?>') {
				$('#own-app-options').slideDown();
			}
			else {
				$('#own-app-options').slideUp();
			}
		});


		var smartphonnesImageUrl = '<?= Yii::app()->theme->baseUrl . "/images/mobileapp_splashscreen_example.png" ?>';
		var popoverContent = '<img src="' + smartphonnesImageUrl + '">';
		$('#what-is-splashscreen').popover({
			trigger: 'hover',
			html: true,
			placement: 'top',
			content: function() {
				var html = popoverContent;
				return html;
			}
		});
		var translationsList = <?=json_encode($translationsList, JSON_HEX_APOS)?>;
		var translationsListForURL = JSON.parse('<?=json_encode($translationsListForURL)?>');
		var languages = $('#languages').val().split(',');
		(function ($) {
			$(function () {
				// Remember the header/footer url user set/get from db
				var initialHeaderUrl = $('#BrandingWhiteLabelForm_whitelabel_header_ext_url').val();
				initialHeaderUrl = initialHeaderUrl.replace(' ', '');
				if (initialHeaderUrl == '') { initialHeaderUrl = 'http://'; }
				var initialFooterUrl = $('#BrandingWhiteLabelForm_whitelabel_footer_ext_url').val();
				initialFooterUrl = initialFooterUrl.replace(' ', '');
				if (initialFooterUrl == '') { initialFooterUrl = 'http://'; }

				if ($('#whitelabel_footer_custom_text').is(':checked')) {
					$('.show-hide-footer-text').show();
				} else {
					$('.show-hide-footer-text').hide();
				}

				// Show/Hide set custom footer text
				$(document).on('change', 'input[name=\'BrandingWhiteLabelForm[whitelabel_footer]\']', function(){
					if($('#whitelabel_footer_custom_text').is(':checked')){
						$('.show-hide-footer-text').show();
					} else{
						$('.show-hide-footer-text').hide();
					}
				});


				//whitelabel_helpdesk_email
				if ($('#whitelabel_menu').is(':checked'))
					$('#helpdesk_mail_setting').show();
				else
					$('#helpdesk_mail_setting').hide();

				// Show/Hide set whitelabel_helpdesk_email
				$(document).on('change', '#whitelabel_menu', function(){
					if ($('#whitelabel_menu').is(':checked'))
						$('#helpdesk_mail_setting').show();
					else
						$('#helpdesk_mail_setting').hide();
				});

				//whitelabel_naming
				if ($('#whitelabel_naming').is(':checked')) {
					//$('#BrandingWhiteLabelForm_whitelabel_naming_text').show();
					$('.show-hide-naming-input').show();
				} else {
					//$('#BrandingWhiteLabelForm_whitelabel_naming_text').hide();
					$('.show-hide-naming-input').hide();
				}

				// Show/Hide set whitelabel_naming input
				$(document).on('change', '#whitelabel_naming', function(){
					if($('#whitelabel_naming').is(':checked')){
						//$('#BrandingWhiteLabelForm_whitelabel_naming_text').show();
						$('.show-hide-naming-input').show();
					} else{
						$('.show-hide-naming-input').hide();
//						$('#BrandingWhiteLabelForm_whitelabel_naming_text').hide();
					}
				});


				// whitelabel_naming_site_enable
				if ($('#BrandingWhiteLabelForm_whitelabel_naming_site_enable').is(':checked')) {
					$('.show-hide-naming-site-input').show();
				} else {
					$('.show-hide-naming-site-input').hide();
				}

				// Show/Hide set whitelabel_naming_site_enable
				$(document).on('change', '#BrandingWhiteLabelForm_whitelabel_naming_site_enable', function(){
					if($('#BrandingWhiteLabelForm_whitelabel_naming_site_enable').is(':checked')){
						$('.show-hide-naming-site-input').show();
					} else{
						$('.show-hide-naming-site-input').hide();
					}
				});


				// max-input show-hide-header-url
				if ($('#BrandingWhiteLabelForm_whitelabel_header_1').is(':checked')) {
					$('.show-hide-header-url').show();
					if($('#BrandingWhiteLabelForm_whitelabel_header_ext_url').val() == '') {
						if (initialHeaderUrl == '') { initialHeaderUrl = 'http://'; }
						$('#BrandingWhiteLabelForm_whitelabel_header_ext_url').val(initialHeaderUrl);//'http://');
					}

					if($('#BrandingWhiteLabelForm_whitelabel_header_ext_height').val() == '' || $('#BrandingWhiteLabelForm_whitelabel_header_ext_height').val() == 0) {
						$('#BrandingWhiteLabelForm_whitelabel_header_ext_height').val(50);
					}
				} else {
					$('.show-hide-header-url').hide();
					if($('#BrandingWhiteLabelForm_whitelabel_header_ext_url').val() != '') { initialHeaderUrl = $('#BrandingWhiteLabelForm_whitelabel_header_ext_url').val(); }
					$('#BrandingWhiteLabelForm_whitelabel_header_ext_url').val('');
				}

				// Show/Hide set max-input show-hide-header-url on change
				$(document).on('change', 'input[name=\'BrandingWhiteLabelForm[whitelabel_header]\']', function(){
					if($('#BrandingWhiteLabelForm_whitelabel_header_1').is(':checked')){
						$('.show-hide-header-url').show();
						if($('#BrandingWhiteLabelForm_whitelabel_header_ext_url').val() == '') {
							if (initialHeaderUrl == '') { initialHeaderUrl = 'http://'; }
							$('#BrandingWhiteLabelForm_whitelabel_header_ext_url').val(initialHeaderUrl);//'http://');
						}

						if($('#BrandingWhiteLabelForm_whitelabel_header_ext_height').val() == '' || $('#BrandingWhiteLabelForm_whitelabel_header_ext_height').val() == 0) {
							$('#BrandingWhiteLabelForm_whitelabel_header_ext_height').val(50);
						}
					} else{
						$('.show-hide-header-url').hide();
						if($('#BrandingWhiteLabelForm_whitelabel_header_ext_url').val() != '') { initialHeaderUrl = $('#BrandingWhiteLabelForm_whitelabel_header_ext_url').val(); }
						$('#BrandingWhiteLabelForm_whitelabel_header_ext_url').val('');
					}
				});


				// show-hide-footer-url
				if ($('#whitelabel_footer_custom_url').is(':checked')) {
					$('.show-hide-footer-url').show();
					if($('#BrandingWhiteLabelForm_whitelabel_footer_ext_url').val() == '') {
						if (initialFooterUrl == '') { initialFooterUrl = 'http://'; }
						$('#BrandingWhiteLabelForm_whitelabel_footer_ext_url').val(initialFooterUrl);//'http://');
					}

					if($('#BrandingWhiteLabelForm_whitelabel_footer_ext_height').val() == '' || $('#BrandingWhiteLabelForm_whitelabel_footer_ext_height').val() == 0) {
						$('#BrandingWhiteLabelForm_whitelabel_footer_ext_height').val(30);
					}
				} else {
					$('.show-hide-footer-url').hide();
					if ($('#BrandingWhiteLabelForm_whitelabel_footer_ext_url').val() != '') { initialFooterUrl = $('#BrandingWhiteLabelForm_whitelabel_footer_ext_url').val(); }
					$('#BrandingWhiteLabelForm_whitelabel_footer_ext_url').val('');
				}

				// Show/Hide set show-hide-footer-url on change
				$(document).on('change', 'input[name=\'BrandingWhiteLabelForm[whitelabel_footer]\']', function(){
					if($('#whitelabel_footer_custom_url').is(':checked')){
						$('.show-hide-footer-url').show();
						if($('#BrandingWhiteLabelForm_whitelabel_footer_ext_url').val() == '') {
							initialFooterUrl = initialFooterUrl.replace(' ', '');
							if (initialFooterUrl == '') { initialFooterUrl = 'http://'; }
							$('#BrandingWhiteLabelForm_whitelabel_footer_ext_url').val(initialFooterUrl);//'http://');
						}

						if($('#BrandingWhiteLabelForm_whitelabel_footer_ext_height').val() == '' || $('#BrandingWhiteLabelForm_whitelabel_footer_ext_height').val() == 0) {
							$('#BrandingWhiteLabelForm_whitelabel_footer_ext_height').val(30);
						}
					} else {
						$('.show-hide-footer-url').hide();
						if ($('#BrandingWhiteLabelForm_whitelabel_footer_ext_url').val() != '') { initialFooterUrl = $('#BrandingWhiteLabelForm_whitelabel_footer_ext_url').val(); }
						$('#BrandingWhiteLabelForm_whitelabel_footer_ext_url').val('');
					}
				});

				$(document).on('change', '#customtext', function(){
					tinyMCE.get('tiny_whitelabel_footer_text').focus();
					var chosenLanguage = $(this).val();
					var newVal = '';
					newVal = $('input[name="valuesForText[' + chosenLanguage + ']"]').val();
					tinymce.activeEditor.setContent(newVal);
				});

				$(document).on('change', '#customurl', function () {
					var chosenLanguage = $(this).val();
					var newVal = 'http://';
					newVal = $('input[name="valuesForUrl[' + chosenLanguage + ']"]').val();
					$('#BrandingWhiteLabelForm_whitelabel_footer_ext_url').val(newVal);
				});

				$(document).on('input', '#BrandingWhiteLabelForm_whitelabel_footer_ext_url', function(){
					var value = $(this).val();
					var lang = $('#customurl').val();
					$('input[name="valuesForUrl[' + lang + ']"]').val(value);
				});
				setTimeout(function() {
					initTinyMCE('#' + $('#tiny_whitelabel_footer_text').attr('id'), 150);
				}, 2000);

				tinymce.EditorManager.on("AddEditor", function (e) {
					var editor = e.editor;

					if (editor.id == 'tiny_whitelabel_footer_text') {
						editor.on('input change', function (e) {
							var value = editor.getContent();
							var lang = $('#customtext').val();
							$('input[name="valuesForText[' + lang + ']"]').val(value);
						});
					}
				});

				// create hidden fields for custom text inputs
				$.each(languages, function (index, lang) {
					var obj = translationsList[lang];
					if (obj != undefined) {
						$('<input>').attr({
							type: 'hidden',
							name: 'valuesForText[' + lang + ']',
							value: obj.value
						}).appendTo('div.max-input.show-hide-footer-text');
					}else{
						$('<input>').attr({
							type: 'hidden',
							name: 'valuesForText[' + lang + ']',
							value: ''
						}).appendTo('div.max-input.show-hide-footer-text');
					}
				});

				var time = setInterval(function () {
					if (tinymce.activeEditor != null) {
						$('#customtext').trigger('change');
						clearInterval(time);
					}
				}, 500);

				$(document).on('change','#customurlHeader',function(){
					var lang = $(this).val();
					var value = $('input[name="valuesForUrlHeader[' + lang + ']"').val();
					$('#BrandingWhiteLabelForm_whitelabel_header_ext_url').val(value);
				});

				$(document).on('input', '#BrandingWhiteLabelForm_whitelabel_header_ext_url', function(){
					var lang = $('#customurlHeader').val();
					$('input[name="valuesForUrlHeader[' + lang + ']"').val($(this).val());
				});
				if($('#BrandingWhiteLabelForm_whitelabel_header_1').is(':checked'))
					$('#customurlHeader').trigger('change')
			});
		})(jQuery);
</script>
<?php if(!$isInsideTabs): ?>
        </div>
    </div>
</div>
<?php endif; ?>