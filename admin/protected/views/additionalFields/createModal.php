<div class="modal hide fade new-field" id="field-modal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3><?php echo Yii::t('menu', '_FIELD_MANAGER');?> - <?php echo $model->getCategoryName(); ?></h3>
	</div>
	<div class="modal-body">
		<?php if ($model->getFieldType() == 'dropdown'): ?>
			<?php echo $this->renderPartial('_formDropdownV2', array('model' => $model, 'activeLanguagesList' => $activeLanguagesList)); ?>
		<?php else: ?>
			<?php echo $this->renderPartial('_form', array('model' => $model, 'activeLanguagesList' => $activeLanguagesList)); ?>
		<?php endif; ?>
	</div>
	<div class="modal-footer buttons">
		<a href="#" class="btn confirm-btn"><?php echo Yii::t('standard', '_SAVE');?></a>
		<a href="#" class="btn cancel" data-dismiss="modal"><?php echo Yii::t('standard', '_UNDO');?></a>
	</div>
</div>
<script type="text/javascript">
    $('select').styler();
	if ($('.modal-header').length == 2)
	{
		$('#field-modal')[0].classList.remove('fade');
		$('#field-modal')[0].classList.remove('hide');
		$($('.modal-header')[1]).hide();
		$('.modal-body')[0].style.padding = 0;
		$('.modal-body')[0].style.height = '160px';

		/*
		$('.modal')[1].style.marginLeft = '';
		$('.modal')[1].style.width = '';
		*/

		$('#field-modal').attr('class', '');
		$('.modal-footer')[0].style.paddingTop = '40px';
		$('#CoreField_lang_code')[0].style.marginTop = '27px';
		$('.modal-footer')[0].style.marginTop = '15px';
	}
</script>