<div class="modal hide fade new-field" id="field-modal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3><?php echo Yii::t('menu', '_FIELD_MANAGER');?> - <?php echo $model->getCategoryName(); ?></h3>
	</div>
	<div class="modal-body">
		<?php if ($model->getFieldType() == 'dropdown'): ?>
			<?php echo $this->renderPartial('_formDropdownV2', array(
				'model' => $model,
				'activeLanguagesList' => $activeLanguagesList,
				'translationsList' => $translationsList
			)); ?>
		<?php else: ?>
			<?php echo $this->renderPartial('_form', array(
				'model' => $model,
				'activeLanguagesList' => $activeLanguagesList,
				'translationsList' => $translationsList
			)); ?>
		<?php endif; ?>
	</div>
	<div class="modal-footer buttons">
		<a href="#" class="btn confirm-btn"><?php echo Yii::t('standard', '_SAVE');?></a>
		<a href="#" class="btn cancel" data-dismiss="modal"><?php echo Yii::t('standard', '_UNDO');?></a>
	</div>
</div>
<script type="text/javascript">
	$('select').styler();
</script>