<div class="form dropdown">
	<?php $defaultLanguage = CoreLangLanguage::getDefaultLanguage(); ?>
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'add-field-form',
		'action' => $model->isNewRecord ? array('additionalFields/createDropdown') : array('additionalFields/editDropdown', 'id' => $model->id_field),
	)); ?>


	<div class="dropdown_form_title"><?php echo Yii::t('branding', '_PAGE_LANGUAGE_LABEL'); ?></div>
	<div class="clearfix">
	<div class="languagesList">

		<?php echo $form->dropDownList($model, 'lang_code', $activeLanguagesList, array('class' => 'additionalFieldTypeDropdown', 'id' => 'CoreField_lang_code')); ?>
		<?php echo $form->error($model, 'lang_code'); ?>
        <div class="orgChart_languages">
        <div><?php echo Yii::t('admin_lang', '_LANG_ALL') . ': <span>' . count($activeLanguagesList).'</span>'; ?></div>
        <div id="orgChart_assignedCount"><?php echo Yii::t('standard', 'Filled languages') . ' <span>'.((!empty($translationsList))?count(array_filter($translationsList)):0).'</span>'; ?></div>
        </div>
	</div>

	<div class="orgChart_translationsList">
		<?php

		$currentLanguage = false;

		if (!empty($activeLanguagesList)) {

			$currentLanguage = (!empty($model->lang_code) ? $model->lang_code : $first_key);

			foreach ($activeLanguagesList as $key => $lang) {
				if (($key == $first_key && empty($model->lang_code)) || ($model->lang_code == $key)) {
					$class = 'show';
				} else {
					$class = 'hide';
				}
				?>
				<div id="CoreOrgChart_<?= $key ?>" class="languagesName <?php echo $key . ' ' . $class; ?>">
					<div class="field-name" style="position: relative;">
						<div class="orgChart_form_title"><?php echo Yii::t('standard', '_FIELD_NAME', array($lang)); ?></div>
						<?php echo $form->textField($model, 'translation[' . $key . '][field]', array('class' => 'orgChart_translation'.($defaultLanguage == $key ? ' default_lang' : ''), 'maxlength' => '255', 'value' => (!empty($translationsList[$key]['field']) ? $translationsList[$key]['field'] : ''))); ?>
					</div>
					<div id="field-options-<?= $key ?>" class="field-options" data-lang="<?php echo $key; ?>" style="position: relative;">
						<div class="orgChart_form_title"><?php echo Yii::t('test', '_TEST_QUEST_ELEM') , " ($lang)"; ?></div>
						<div id="field-options-inputs-<?= $key ?>">
							<!-- here the fields list to be printed by JS -->
						</div>
					</div>
				</div>
			<?php $i++; } ?>
		<?php } ?>
	</div>
</div>
<?php echo $this->renderPartial('_dropdownPreview'); ?>
	<div class="additional-checkboxes clearfix">
		<div class="item clearfix">
			<?php echo $form->checkBox($model, 'mandatory'); ?>
			<?php echo $form->labelEx($model, 'mandatory', array('label' => Yii::t('standard', '_MANDATORY') )); ?>
		</div>
		<div class="item clearfix">
			<?php echo $form->checkBox($model, 'invisible_to_user'); ?>
			<?php echo $form->labelEx($model, 'invisible_to_user', array('label' => Yii::t('organization_chart', '_ORG_CHART_FIELD_WRITE') )); ?>
		</div>
	</div>
	<?php echo $form->hiddenField($model, 'type_field', array('value' => $model->type)); ?>
	<?php $this->endWidget(); ?>
</div>
<div class="saving-dropdown-field" style="display:none">
	<img src="<?= Yii::app()->theme->baseUrl.'/images/loading_big.gif' ?>" />
</div>
<script type="text/javascript">
	(function ($) {

		<?php
		$jsObject = array();
		foreach ($activeLanguagesList as $key => $lang) {
			$jsObject[$key] = array(
				'lang_name' => $lang,
				'translations' => array()
			);
			if (!empty($translationsList[$key]) && is_array($translationsList[$key]['options'])) {
				foreach ($translationsList[$key]['options'] as $data) {
					$jsObject[$key]['translations'][] = array(
						'idSon' 		=> $data['idSon'],
						'translation' 	=> $data['translation'],
						'id_common_son' => $data['id_common_son'],
					);
				}
			}
			//echo $form->textField($model, 'translation[' . $key . '][options][translation][]', array('placeholder' => Yii::t('field', 'Type here to add an option...'), 'class' => 'dropdownOption', 'value' => '', 'id' => $idPrefix.'translation_'.$j));
		}
		echo 'var translations = '.CJavaScript::encode($jsObject).';';
		echo "\n";
		?>

		//======================

		//var DD is declared in dropdown_field_management.js
		DD = new dropdownFieldManager({
			<?php if ($model->id_field) : ?>idField: <?= $model->id_field ?>,<?php endif ?>
			currentActiveLanguage: <?= CJavaScript::encode($currentLanguage) ?>,
			translations: translations,
			filledLanguageTranslation: <?= CJavaScript::encode(Yii::t('standard', 'Filled languages')) ?>,
			newOptionPlaceholder: <?= CJSON::encode(Yii::t('field', 'Type here to add an option...')) ?>
		});

        /*
         On modal hide, remove the modal content in case there are js events or something else
         that may interrupt the work of the other parts of the code
         */
        $('#field-modal').on('hidden', function () {
            $('#field-modal').remove();
            /*
             When closing the dialog destroy the DD object
             because the object is needed only when this view is shown.
             */
            DD = null;
        });
	})(jQuery);
</script>