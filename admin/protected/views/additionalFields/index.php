<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('menu', '_LISTUSER') => array('userManagement/index'),
	Yii::t('menu', '_FIELD_MANAGER'),
); ?>

<?php //$this->renderPartial('//common/_mainActions'); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'additional-fields-form',
	'htmlOptions' => array('class' => 'ajax-grid-form', 'data-grid' => '#fields-grid'),
)); ?>
<div class="main-section additional-fields-wrapper">
	<h3 class="title-bold back-button">
		<a href="<?php echo Yii::app()->createUrl('userManagement/index'); ?>"><?php echo Yii::t('standard', '_BACK'); ?></a>
		<span><?php echo Yii::t('menu', '_FIELD_MANAGER'); ?></span>
	</h3>
	<div class="filters-wrapper">
        <div class="filters clearfix">
            <div class="input-wrapper">
            <input autocomplete="off" id="advanced-search-fields-grid" class="typeahead" data-url="<?=Docebo::createAppUrl('admin:additionalFields/fieldsAutocomplete')?>" type="text" name="filter" placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"/>
            <span class="search-icon"></span>
            </div>
        </div>
    </div>
</div>

<div class="add-field-section clearfix">
	<label for="field-type-select"><?php echo Yii::t('standard', '_ADD'); ?></label>
	<select name="filter-type" id="field-type-select">
		<?php foreach ($fieldTypes as $identifier => $fieldType): ?>
			<option value="<?php echo $identifier; ?>" <?= $fieldType == 'textfield' ? 'selected' : '' ?>><?php echo Yii::t('field', '_' . strtoupper($fieldType)); ?></option>
		<?php endforeach; ?>
	</select>
	<input type="button" value="<?php echo Yii::t('standard', '_CREATE'); ?>" class="create-additional-field">
</div>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<div class="bottom-section clearfix"></div>
	<?php $buttons[] = array(
		'class' => 'CButtonColumn',
		'buttons' => array(
			'order' => array(
				'url' => '"#" . $data->id_field',
				'label' => Yii::t('standard', '_MOVE'),
				'options' => array('class' => 'order-field move'),
			),
			'edit' => array(
				'url' => '$data->getFieldType() != "dropdown" ? Yii::app()->createUrl("' . $this->id . '/editField", array("id" => $data->id_field)) : Yii::app()->createUrl("' . $this->id . '/editDropdown", array("id" => $data->id_field))',
				'label' => Yii::t('standard', '_MOD'),
				'click' => 'js:function() {
						$(".modal.new-field").remove();

						$.get($(this).attr("href"), {}, function(data) {
							$("body").append(data.html);
							$("#field-modal").modal();

							window.setTimeout(function () {
								var assignedCount = 0;
								$(".modal.in .orgChart_translationsList .orgChart_translation").each(function (index) {
								    var explode = $(this).attr("id").split("_");
								    if(explode[explode.length - 1] == "field")
									var lang = explode[explode.length - 2];
								    else
									var lang = explode[explode.length - 1];
								    var lang_option = $("#CoreField_lang_code option[value="+lang+"]");
								    if ($(this).val() != "") {
								        assignedCount++;
								        var html = lang_option.html();
								        if (html && html.indexOf("* ") < 0)
								        	lang_option.html("* "+lang_option.html());
								    }
								    else {
								    	var html = lang_option.html();
								    	if (html && html.indexOf("* ") === 0) {
								    		lang_option.html(lang_option.html().replace("* ", ""));
								    	}
								    }
								});
								$("#orgChart_assignedCount").html("' . Yii::t('standard', 'Filled languages') . ' <span>" + assignedCount + "</span>");
							}, 300)
						});

						return false;
				}',
				'options' => array('class' => 'edit-field'),
			),
		),
		'template' => '{order}{edit}',
	); ?>
<div id="grid-wrapper">
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'fields-grid',
		'dataProvider' => $coreField->getAdditionalFieldsDataProvider(),
		'htmlOptions' => array('class' => 'grid-view clearfix'),
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
		'rowHtmlOptionsExpression' => 'array("data-fid" => $data->id_field);',
		'columns' => array_merge(
			array(
				array(
					'header' => Yii::t('standard', '_FIELD_NAME'),
					'name' => 'translation',
					'value' => array($this, 'gridRenderTranslation'),
					'htmlOptions' => array('class' => 'additional-field-name')
				),
				array(
					'header' => Yii::t('field', '_FIELD_TYPE'),
					'name' => 'type',
					'value' => '$data->getCategoryName();',
				),
			), $buttons, array(
				array(
					'name' => '',
					'type' => 'raw',
					'value' => 'CHtml::link("", "", array(
						"class" => "ajaxModal delete-action",
						"data-toggle" => "modal",
						"data-modal-class" => "delete-node",
						"data-modal-title" => "Delete Field",
						"data-buttons" => \'[
							{"type": "submit", "title": "Confirm"},
							{"type": "cancel", "title": "Cancel"}
						]\',
						"data-url" => "additionalFields/deleteField&id=$data->id_field",
						"data-after-loading-content" => "hideConfirmButton();",
						"data-after-submit" => "updateFieldContent",
					));',
					'htmlOptions' => array('class' => 'button-column-single')
				)
		)),
		'template' => '{items}{summary}{pager}',
		'pager' => array(
			'class' => 'DoceboCLinkPager',
			'maxButtonCount' => 8,
		),
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'afterAjaxUpdate' => 'function(){initFieldsSortable()}'
	)); ?>
</div>

<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input,select').styler();
			replacePlaceholder();
			initFieldsSortable();
			//$('#additional-fields-form').jClever({applyTo : {select : true, checkbox : false, radio : false, button : false, file : false, input : false, textarea : false}});
			$('.create-additional-field').click(function () {
				$(".modal.new-field").remove();
				var fieldType = $('#field-type-select').val();
				$.ajax({
					url: yii.urls.base + '/?r=additionalFields/createFieldModal',
					data: { type : fieldType },
					cache: false,
					success: function (data) {
						$('body').append(data.html);
						$('#field-modal').modal();
					}
				});
			});
	    });
	})(jQuery);


	setTimeout(checkLanguage, 1000);
	function checkLanguage()
	{
		if($(".modal.in .orgChart_translationsList .orgChart_translation"))
		{
			$(".modal.in .orgChart_translationsList .orgChart_translation").each(function (index) {
				var explode = $(this).attr("name").split("][");
				if(explode[explode.length - 1] == "field]")
					var lang = explode[explode.length - 2];
				else
					var lang = explode[explode.length - 1].replace(']', '');
				if (!lang) return;
				var lang_option = $("#CoreField_lang_code option[value="+lang+"]");
				if ($(this).val() != "") {
					var html = lang_option.html();
					if (html && html.indexOf("* ") < 0) {
						lang_option.html("* "+lang_option.html());
					}
				} else {
					var html = lang_option.html();
					if(html && html.indexOf("* ") === 0) {
						lang_option.html(lang_option.html().replace("* ", ""));
					}
				}
			});
		}
		setTimeout(checkLanguage, 1000);
	}

</script>
