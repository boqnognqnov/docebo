<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'field_form',
	));	?>

	<div class="row-fluid">
		<p class="additional-field-name">
			<?php echo Yii::t('standard', '_FIELD_NAME') . ': "' . $this->deleteRenderTranslation($model) . '"'; ?>
		</p>
	</div>

	<div class="clearfix">
		<?php echo $form->checkbox($model, 'confirm', array('id' => 'CoreField_confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed'), 'CoreField_confirm_'.$checkboxIdKey); ?>
		<?php echo $form->error($model, 'confirm'); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>