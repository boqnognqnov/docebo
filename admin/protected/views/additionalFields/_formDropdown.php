<div class="form dropdown">
	<?php $defaultLanguage = CoreLangLanguage::getDefaultLanguage(); ?>
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'add-field-form',
		'action' => $model->isNewRecord ? array('additionalFields/createDropdown') : array('additionalFields/editDropdown', 'id' => $model->idField),
	)); ?>


	<div class="dropdown_form_title"><?php echo Yii::t('branding', '_PAGE_LANGUAGE_LABEL'); ?></div>
	<div class="clearfix">
	<div class="languagesList">

		<?php echo $form->dropDownList($model, 'lang_code', $activeLanguagesList); ?>
		<?php echo $form->error($model, 'lang_code'); ?>
        <div class="orgChart_languages">
        <div><?php echo Yii::t('admin_lang', '_LANG_ALL') . ': <span>' . count($activeLanguagesList).'</span>'; ?></div>
        <div id="orgChart_assignedCount"><?php echo Yii::t('standard', 'Filled languages') . ' <span>'.((!empty($translationsList))?count(array_filter($translationsList)):0).'</span>'; ?></div>
        </div>
	</div>

	<div class="orgChart_translationsList">
		<?php if (!empty($activeLanguagesList)) {
			reset($activeLanguagesList);
			$first_key = key($activeLanguagesList);
			$i = 0;
			foreach ($activeLanguagesList as $key => $lang) {
				if (($key == $first_key && empty($model->lang_code)) || ($model->lang_code == $key)) {
					$class = 'show';
				} else {
					$class = 'hide';
				}
				?>
				<div id="CoreOrgChart_<?php echo $key; ?>" class="languagesName <?php echo $key . ' ' . $class; ?>">
					<div class="field-name" style="position: relative;">
						<div class="orgChart_form_title"><?php echo Yii::t('standard', '_FIELD_NAME', array($lang)); ?></div>
						<?php echo $form->textField($model, 'translation[' . $key . '][field]', array('class' => 'orgChart_translation'.($defaultLanguage == $key ? ' default_lang' : ''), 'value' => (!empty($translationsList[$key]['field']) ? $translationsList[$key]['field'] : ''))); ?>
					</div>
					<div class="field-options" data-lang="<?php echo $key; ?>" style="position: relative;">
						<div class="orgChart_form_title"><?php echo Yii::t('test', '_TEST_QUEST_ELEM') , " ($lang)"; ?></div>
						<?php 
							$j = 0;
							$idPrefix = 'translation_'.$key.'_options_';
							if (!empty($translationsList[$key]) && is_array($translationsList[$key]['options'])):
								foreach ($translationsList[$key]['options'] as $data):
									echo $form->textField($model, 'translation[' . $key . '][options][translation][]', array('class' => 'dropdownOption', 'value' => (!empty($data['translation']) ? $data['translation'] : ''), 'id' => $idPrefix.'translation_'.$j));
									echo $form->hiddenField($model, 'translation[' . $key . '][options][idSon][]', array('value' => (!empty($data['idSon']) ? $data['idSon'] : ''), 'id' => $idPrefix.'idSon_'.$j));
									echo $form->hiddenField($model, 'translation[' . $key . '][options][id_common_son][]', array('value' => (!empty($data['id_common_son']) ? $data['id_common_son'] : ''), 'id' => $idPrefix.'id_common_son_'.$j));
								endforeach;
							endif;
							echo $form->textField($model, 'translation[' . $key . '][options][translation][]', array('placeholder' => Yii::t('field', 'Type here to add an option...'), 'class' => 'dropdownOption', 'value' => '', 'id' => $idPrefix.'translation_'.$j));	
						?>
					</div>
				</div>
			<?php $i++; } ?>
		<?php } ?>
	</div>
</div>
<?php echo $this->renderPartial('_dropdownPreview'); ?>
	<div class="additional-checkboxes clearfix">
		<div class="item clearfix">
			<?php echo $form->checkBox($model, 'mandatory'); ?>
			<?php echo $form->labelEx($model, 'mandatory', array('label' => Yii::t('standard', '_MANDATORY') )); ?>
		</div>
		<div class="item clearfix">
			<?php echo $form->checkBox($model, 'invisible_to_user'); ?>
			<?php echo $form->labelEx($model, 'invisible_to_user', array('label' => Yii::t('organization_chart', '_ORG_CHART_FIELD_WRITE') )); ?>
		</div>
	</div>
	<?php echo $form->hiddenField($model, 'type_field', array('value' => $model->type_field)); ?>
	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
	(function ($) {

		$(function () {
			$('#add-field-form').closest('.modal').find('input,select').styler();
			//$('input, select').styler();
			replacePlaceholder();
			//$('#add-field-form').jClever({applyTo: {select: true,checkbox: false,radio: false,button: false,file: false,input: false,textarea: false}});
		});

		var assignedCount = 0;
		var isPlaceholderSupported = 'placeholder' in document.createElement('input');

		function appendToAllLanguages() {
			$('.languagesName').each(function() {
				var lastInput = $(this).find('.dropdownOption:last');

				//calculate progressive input ID attribute
				var oldId = lastInput.attr('id').split('_');
				var oldIndex = parseInt(oldId[oldId.length -1]);
				oldId[oldId.length - 1] = oldIndex + 1;
				var newId = oldId.join('_');
				
				//emulates ".clone()" method by creating a brand new input (because some IE varsions may not like cloning)
				var clone = $(document.createElement('input'));
				clone.attr('type', "text");
				clone.attr('id', newId);
				clone.attr('class', 'dropdownOption');
				clone.attr('value', "");
				clone.attr('name', lastInput.attr('name'));
				clone.attr('placeholder', "<?= Yii::t('field', 'Type here to add an option...') ?>");

				lastInput.after(clone);
				//lastInput.after(lastInput.clone());

				lastInput.attr('placeholder', '');

				//replacePlaceholder(); //IE8 seems not to like this one ...
				if (!isPlaceholderSupported) { //replace placeholders, if needed (depending on browser support)
					lastInput.textPlaceholder();
					var el = $('#'+newId);
					el.textPlaceholder();
				}
			});
		}

		$('.languagesName').each(function() {
			$(this).find('.field-options').on('keydown', '.dropdownOption:last', function(event) {

				var nextInput = $(this).next('.dropdownOption');

				if (nextInput.length == 0 && this.value == '') {
					appendToAllLanguages();
					event.stopPropagation();
				}
			});
		});

  	$('.modal.in .orgChart_translationsList .orgChart_translation').each(function (index) {
  	    if ($(this).val() != '') {
  	        assignedCount++;
  	    }
  	});
  	$('#orgChart_assignedCount').html('<?php echo Yii::t('standard', 'Filled languages'); ?>: <span>' + assignedCount + '</span>');

	})(jQuery);
</script>

