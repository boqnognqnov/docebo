<div class="form">
    <?php $defaultLanguage = CoreLangLanguage::getDefaultLanguage(); ?>
	<?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'add-field-form',
	)); ?>
    <div class="dropdown_form_title"><?php echo Yii::t('branding', '_PAGE_LANGUAGE_LABEL'); ?></div>
    <div class="clearfix">
	<div class="languagesList">
		<?= CHtml::dropDownList('CoreUserField[lang_code]', $model->translationEntry->lang_code, $activeLanguagesList, ['id' => 'CoreField_lang_code']) ?>
		<?php echo $form->error($model, 'lang_code'); ?>
	</div>

	<div class="orgChart_translationsList">
		<?php if (!empty($activeLanguagesList)) {
			reset($activeLanguagesList);
			$first_key = key($activeLanguagesList);

			?>
			<?php foreach ($activeLanguagesList as $key => $lang) {
				if (($key == $first_key && empty($model->translation->lang_code)) || ($model->translation->lang_code == $key)) {
					$class = 'show';
				} else {
					$class = 'hide';
				}
				?>
				<div id="NodeTranslation_<?php echo $key; ?>" class="languagesName <?php echo $class; ?>">
					<div class="orgChart_form_title"><?php echo Yii::t('standard', '_FIELD_NAME', array($lang)); ?></div>
					<?php/*?><?php echo $form->labelEx($model, 'translation[' . $key . ']', array('label' => 'Field name')); ?>   <?php*/?>
					<?php echo $form->textField($model, 'translation[' . $key . ']', array('class' => 'orgChart_translation'.($defaultLanguage == $key ? ' default_lang' : ''), 'maxlength' => '255', 'value' => (!empty($translationsList[$key]) ? $translationsList[$key] : ''))); ?>
					<?php echo $form->error($model, 'translation[' . $key . ']'); ?>
				</div>
			<?php } ?>
			<?php echo $form->error($model, 'translation'); ?>
		<?php } ?>
    </div>
	</div>

	<div class="orgChart_languages">
		<div><?php echo Yii::t('admin_lang', '_LANG_ALL').': <span>' . count($activeLanguagesList).'</span>'; ?></div>
		<div id="orgChart_assignedCount"><?php echo Yii::t('standard', 'Filled languages').' <span>'.((!empty($translationsList))?count(array_filter($translationsList)):0).'</span>'; ?></div>
	</div>

	<div class="additional-checkboxes">
		<div class="item">
			<?php echo $form->checkBox($model, 'mandatory'); ?>
			<?php echo $form->labelEx($model, 'mandatory'); ?>
		</div>
		<div class="item">
            <?php echo $form->checkBox($model, 'invisible_to_user'); ?>
			<?php echo $form->labelEx($model, 'invisible_to_user'); ?>
		</div>
	</div>

	<?php echo $form->hiddenField($model, 'type_field', array('value' => $model->getFieldType())); ?>
	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
    (function ($) {
        $(function () {
            $('input').styler();
            //$('#add-field-form').jClever({applyTo: {select: true,checkbox: false,radio: false,button: false,file: false,input: false,textarea: false},fileUploadText: "CHOOSE FILE"});

            /*
             On modal hide, remove the modal content in case there are js events or something else
             that may interrupt the work of the other parts of the code.
             */
            $('#field-modal').on('hidden', function () {
                $('#field-modal').remove();
            });
        });
    })(jQuery);
</script>

