<?php 
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'delete-menuItem',
		'htmlOptions' => array(
			'class' => 'ajax',
			'name' => 'delete-menuItem'
		),
	));
?>

	<div class="row-fluid">
		<div class="span12">
			<label class="checkbox">
				<?= CHtml::checkBox('agree_delete', false, array('value' => 'agree')) ?>
				<?= Yii::t('dashboard', 'I agree to delete the selected layout and I understand this operation <strong>cannot be undone!</strong>') ?>
			</label>
		</div>
	</div>
	


	<div class="form-actions">
    	<input class="btn-docebo green big" type="submit" name="confirm" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" />
    	<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
	</div>


<?php $this->endWidget(); ?>

<script type="text/javascript">
	$('input[name="agree_delete"]').styler();
</script>