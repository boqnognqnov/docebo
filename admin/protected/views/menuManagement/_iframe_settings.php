<?php /** @var CActiveForm $form */ ?>
<?php /** @var CoreMenuItem $model */ ?>

<style>
    
    #edit-iframe-menu {}
    
        .form-horizontal #edit-iframe-menu .control-label {width: 121px; text-align: left;}
        .form-horizontal #edit-iframe-menu .controls {margin-left: 120px;}
    
</style>

<div id="edit-iframe-menu" class="row-fluid">

	<br class="clearfix" />
	<br class="clearfix" />


	<div class="control-group">
		<?php echo $form->labelEx($model, 'iframe_height', array('class' => 'control-label', 'for' => 'iframe_height')); ?>
		<div class="controls">
			<?php echo $form->textField($model, 'iframe_height', array(
			)); ?>
			<?php echo $form->error($model, 'iframe_height'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model, 'useAccredIframe', array('class' => 'control-label', 'for' => 'useAccredIframe')); ?>
		<div class="controls">
			<?php echo $form->checkBox($model, 'useAccredIframe', array(
			    'id'         => 'useAccredIframe',
			)); ?>
			<?php echo $form->error($model, 'useAccredIframe'); ?>
		</div>
	</div>
	
	<div id="advanced-iframe-options">

		<div class="control-group">
			<?php echo $form->label($model, 'salt_secret', array('class' => 'control-label', 'for' => 'salt_secret')); ?>
			<div class="controls">
				<?php echo $form->passwordField($model, 'salt_secret', array(
				    'id'    => 'salt_secret'
				)); ?>
				<?php echo $form->error($model, 'salt_secret'); ?>
			</div>
		</div>
			
		<div class="control-group">
			<?php echo $form->labelEx($model, 'repeatSaltSecret', array('class' => 'control-label', 'for' => 'repeatSaltSecret')); ?>
			<div class="controls">
				<?php echo $form->passwordField($model, 'repeatSaltSecret', array(
				    'id'    => 'repeatSaltSecret',
				)); ?>
				<?php echo $form->error($model, 'repeatSaltSecret'); ?>
			</div>
		</div>
		
		<div class="control-group">
			<?php echo $form->labelEx($model, 'oauth_client', array('class' => 'control-label', 'for' => 'oauth_client')); ?>
			<div class="controls">
				<?php echo $form->dropDownList($model, 'oauth_client', $clientsList); ?>
				<?php echo $form->error($model, 'oauth_client'); ?>
			</div>
		</div>
	
	</div>


</div>


<script type="text/javascript">

	(function($){

		// Is IFRAME target ?
		var iframeType = $('#menuItemTargetType').val() === "iframe";
		
		// Initial statuses, when dialog is loaded
		var advanced = <?= json_encode((bool) $model->useAccredIframe) ?>;

		// Start with repeat==original secret
		$('#repeatSaltSecret').val($('#salt_secret').val());

		/**
		 * Update visual status
		 */
		var updateUi = function() {

			if (advanced) {
				$('#advanced-iframe-options').show();
			}
			else {
				$('#advanced-iframe-options').hide();
			}
			
			if(iframeType) {
				$('#edit-iframe-menu').show();
			} else {
				$('#edit-iframe-menu').hide();
			}
			
		}

		$(function(){

			$('#menuItemTargetType').on('change', function(){
				iframeType = $(this).val() === 'iframe';
				updateUi();
			});

			$('#useAccredIframe').on('change', function(){
				advanced = $(this).is(':checked');
				updateUi();
			});
			
			updateUi();
			
		});
		
	})(jQuery);

</script>