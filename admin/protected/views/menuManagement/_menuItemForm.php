<h1><?php echo Yii::t('standard', '_NEW') ?></h1>
<div class="form">

	<?php
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'menuItems_form',
		'method' => 'post',
		'htmlOptions' => array(
			'class' => 'ajax form-horizontal',
		)
	));
	?>

	<div class="clearfix">
		<div class="languagesList">
			<div class="orgChart_form_title"><?php echo Yii::t('standard', '_LANGUAGE'); ?></div>
			<?php echo $form->dropDownList($model, 'lang_code', $activeLanguagesList, array('id' => 'CoreOrgChart_lang_code')); ?>
		</div>

		<div class="orgChart_translationsList">
			<?php
			if (!empty($translations)) {
				reset($translations);
				$first_key = key($translations);
				?>
				<?php
				foreach ($translations as $key => $lang) {
					if (($key == $model->lang_code)) {
						$class = 'show';
					} else {
						$class = 'hide';
					}
					?>
					<div id="NodeTranslation_<?php echo $key; ?>" class="languagesName <?php echo $class; ?>">
						<div class="orgChart_form_title"><?php echo Yii::t('standard', '_NAME', array($lang)); ?>*</div>
						<?php echo CHtml::textField('CoreMenuTranslation[' . $key . ']', $translations[$key], array('class' => 'orgChart_translation' . (CoreLangLanguage::getDefaultLanguage() == $key ? ' default_lang' : ''), 'id' => 'CoreField_translation_' . $key)) ?>
						<?php echo $form->error($model, 'lang_code') ?>
					</div>
				<?php } ?>
			<?php } ?>
		</div>
	</div>

	<div class="menuItem_languages">
		<div><?php echo Yii::t('admin_lang', '_LANG_ALL') . ': <span>' . count($translations) . '</span>'; ?></div>
		<div id="orgChart_assignedCount"><?php echo Yii::t('standard', 'Filled languages') . ': <span>' . ((!empty($translations)) ? count(array_filter($translations)) : 0) . '</span>'; ?></div>
	</div>

	<hr>

	<div class="menuItemOptions">
		<div class="icon">
			<div class="selectIcon"><?php echo Yii::t('label', 'Icon'); ?>*</div>

			<button id="iconPicker" class="btn btn-default" data-search="true" data-search-text="<?=Yii::t('standard', '_SEARCH')?>..."
					data-placement="right" data-iconset="fontawesome" data-icon="<?=$model->icon?>" name="CoreMenuItem[icon]"></button>

			<script type="text/javascript">
				$(document).ready(function(){
					$("#iconPicker").each(function() {
						if($(this).children().length > 0)
							$(this).remove();
					});

					$("#iconPicker").iconpicker({
						arrowNextIconClass: 'fa fa-arrow-right',
						arrowPrevIconClass: 'fa fa-arrow-left'
					});
				});
			</script>
		</div>


		<div class="url">
			<div class="selectURL"> <?php echo Yii::t('standard', '_URL'); ?>*</div>
			<?php echo $form->textField($model, 'url', array('id' => 'menuItemUrl', 'style' => ((Settings::get('enable_legacy_menu') == 'on') ? '' : 'width: 348px'))) ?>
			<?php echo $form->error($model, 'url'); ?>
		</div>

		<div class="targetType">
			<div class="selectTargetType"><?php echo Yii::t('manmenu', 'Target'); ?>*</div>
			<?php echo $form->dropDownList($model, 'targetType', $model::getUrlTargetType(), array('id' => 'menuItemTargetType')); ?>
		</div>

	</div>

	<div class="menuItemAdditionalOptions">
		<div class="useAdvancedSSO">
			<div class="userAdvancedSSO_confirm">
				<?php echo $form->checkBox($model, 'useSecretKey', array('id' => 'useSecretKey')) ?>
			</div>
			<div class="userAdvancedSSO_info">
				<div class="userAdvanceSSOLabel"> <label for=""> <?php echo Yii::t('manmenu', 'Use advanced SSO URL'); ?></label></div>
				<div class="advancedSSOInfo">
					<?php echo Yii::t('manmenu', 'Yor URL will contain the following parameters: unix timestamp, e-mail and security hash. This is a preview of the URL:'); ?>
					http://www.mydomain.com/link?timestamp=1234567&username=john.doe&email=john.doe@company.com&hash=a71943mfio439djk3ck
				</div>

				<div class="secretKey">
					<div class="secretKeyLabel"><?php echo Yii::t('manmenu', 'Secret key'); ?>*</div>
					<div class="secretKeyInput">
						<?php echo $form->textField($model, 'secretKey', array('id' => 'secretKeyInput', 'disabled' => true)) ?>
						<?php echo $form->error($model, 'secretKey'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div id="edit-iframe-menu-wrapper">
		<?php $this->renderPartial("_iframe_settings", array(
		    "form"        => $form,
		    "model"       => $model,
		    "clientsList" => $clientsList,
		)); ?>
	</div>

	<div class="form-actions">
		<?php 
            echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
                'class' => 'btn btn-submit',
                'name'	=> 'save_single_button'
            ));
            echo CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog'));
        ?>
	</div>

	<?php $this->endWidget(); ?>
</div>

<script>
	$(function () {
		$('input').styler();


		$('div.btn-group ul.dropdown-menu li a').click(function (e) {
			var $div = $(this).parent().parent().parent();
			var $btn = $div.find('button');
			var $icon = $(this).attr('data-key');
			var checkClasses = 'home-ico ';
			if($icon.indexOf('fa ') != '-1'){
				checkClasses = '';
			}
			$btn.html($(this).text() + '<i class="'+ checkClasses + $icon + '"></i><span class="caret"></span>');
			$div.removeClass('open');
			$('#icon').val($icon);
			e.preventDefault();
			return false;
		});


		//Handle the use SSO checkbox
		var checkbox = $('#useSecretKey');
		if (checkbox.is(':checked'))
			$('#secretKeyInput').prop('disabled', false);
		else
			$('#secretKeyInput').prop('disabled', true);

		$('#useSecretKey').change(function () {
			var checkbox = $(this);
			if (checkbox.is(':checked'))
				$('#secretKeyInput').prop('disabled', false);
			else
				$('#secretKeyInput').prop('disabled', true);
		});

		setTimeout(checkLanguage, 1000);
		function checkLanguage()
		{
			if ($(".modal .orgChart_translatsion"))
			{
				$(".modal .orgChart_translation").each(function (index) {
					var explode = $(this).attr("name").split("[");
					if (explode[explode.length - 1] == "field]")
						var lang = explode[explode.length - 2];
					else
						var lang = explode[explode.length - 1].replace(']', '');
					if (!lang)
						return;
					var lang_option = $("#CoreOrgChart_lang_code option[value=" + lang + "]");
					if ($(this).val() != "") {
						var html = lang_option.html();
						if (html && html.indexOf("* ") < 0) {
							lang_option.html("* " + lang_option.html());
						}
					} else {
						var html = lang_option.html();
						if (html && html.indexOf("* ") === 0) {
							lang_option.html(lang_option.html().replace("* ", ""));
						}
					}
				});
			}
			setTimeout(checkLanguage, 1000);
		}
	})
</script>