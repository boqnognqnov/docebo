<div class="form-wrapper">
	<div class="main-actions clearfix" style="height: 150px;">
		<h3 class="title-bold"><?php echo Yii::t('manmenu', 'Manage Main Menu'); ?></h3>
		<ul class="clearfix">
			<li>
				<div>
					<?php
						echo CHtml::link('<span></span>'.Yii::t('standard', '_NEW'), Yii::app()->createUrl('menuManagement/createMenuItem'), array(
							'class' => 'new-node open-dialog',
							'alt' => Yii::t('manmenu', '_ADDCUSTOM'),
							'data-dialog-class' => 'manageMenuItem',
						));
					?>
				</div>
			</li>
		</ul>
		<div class="info">
			<div>
				<h4></h4>
				<p></p>
			</div>
		</div>
	</div>

	<div class="logOutMessage error">
		<?php echo Yii::t('manmenu', 'Please log into the system agin to see the changes')?>
	</div>

	<div id="grid-wrapper">
		<?php
			$columns = array();


			$columns[] = array(
				'header' => Yii::t('label', 'Icon'),
				'name' => 'icon',
				'value' => '$data->getIcon()',
				'type' => 'raw',
			);


			$columns = array_merge ($columns, array(
				array(
					'header' => Yii::t('standard', '_NAME'),
					'value' => '$data->getLabel()',
				),
				array(
					'header' => Yii::t('standard', '_URL'),
					'name' => 'url',
					'value' => '$data->is_custom ? CHtml::link($data->url, CHtml::normalizeUrl($data->getItemUrl()), array("target" => $data->getUrlTarget(), "class" => "fakeUrl" ))  : ""',
					'type' => 'raw',
				),
				array(
					'header' => Yii::t('manmenu', 'Target'),
					'value' => ' $data->is_custom ? ( isset($data->options["target"]) ? Yii::t("manmenu", $data::$urlTargetType[$data->options["target"]]) : "" ) : "" ',
					'type' => 'raw',
				),
				array(
					'header' => Yii::t('standard', '_FILTER'),
					'value' => '$data->getItemFilter()',
					'type' => 'raw',
				),
				array(
					'header' => Yii::t('standard', '_SHOW'),
					'value' => 'CHtml::link("", Yii::app()->createUrl("menuManagement/changeMenuItemStatus", array("menuItem" => $data->id)), array("class" => "module-status ".( (bool)$data->visible ? "suspend-action" : "activate-action" ))) ',
					'type' => 'raw'
				),
				array(
					'name' => 'default',
					'value' => '$data->getIsDefault()',
					'type' => 'raw',
					'header' => Yii::t('admin_webpages', '_ALT_HOME'),
					'htmlOptions' => array(
						"class" => "text-center",
					),
					'headerHtmlOptions' => array(
						"class" => "text-center",
					),
				),
				array(
					'class' => 'CButtonColumn',
					'template' => '{order}{edit}{delete}',
					'htmlOptions' => array('class' => 'button-column', 'style' => 'width: 100px'),
					'buttons' => array(
						'order' => array(
							'url' => '"#" . $data->id',
							'label' => Yii::t('standard', '_MOVE'),
							'options' => array('class' => 'order-field move'),
						),
						'edit' => array(
							'url' => 'Yii::app()->createUrl("menuManagement/updateMenuItem", array("id" => $data->id ) )',
							'label' => Yii::t('standard', '_MOD'),
							'options' => array('class' => 'edit-field open-dialog'),
							'visible' => '$data->is_custom'
						),
						'delete' => array(
							'url' => 'Yii::app()->createUrl("menuManagement/deleteMenuItem", array("id" => $data->id) )',
							'options' => array(
								"class" => 'delete open-dialog',
								"data-dialog-id" => "delete-dashboard-layout",
								"data-dialog-class" => "delete-dashboard-layout",
								"data-dialog-title"	=> Yii::t("standard", "_DEL"),
								"title" =>  Yii::t("standard", "_DEL"),
							),
							'visible' => '$data->is_custom'
						),
					),
				),
			));

			$this->widget('common.widgets.ComboGridView', array(
				'gridId' => 'menuItems-grid',
				'disableMassSelection' => true,
				'disableMassActions' => true,
				'disableSearch' => true,
				'sortableRows' => true,
				'sortableUpdateUrl' => Docebo::createAdminUrl('menuManagement/changeMenuItemPosition'),
				'dataProvider' => $model->getMenuItemsDataProvider(),
				'columns' => $columns
			));
		?>
	</div>
</div>

<script>
	$(document).delegate(".modal-body", "dialog2.content-update", function() {
		var e = $(this), autoclose = e.find("a.auto-close");
		if (autoclose.length > 0) {
			$('#menuItems-grid').comboGridView('updateGrid');
		}
	});

	/**
	 * Toggle/Change DEFAULT layout
	 */
	$(document).on('click', '.toggle-default-layout-link', function(e){
		var url = '<?php echo Docebo::createAdminUrl("menuManagement/changeDefaultMenuItem") ?>';
		var data = {id : $(e.currentTarget).data('id')};
		$.getJSON(url, data, function(response){
			if (response.success) {
				$('#menuItems-grid').comboGridView('updateGrid');
			}
		});
	});

	$(function(){
		$('.module-status').live('click', function(){
			var node = $(this);
			$.ajax({
				url: $(this).attr('href'),
				success: function(){
					if (node.hasClass('suspend-action')) {
						node.attr('class', 'module-status activate-action');
					} else {
						node.attr('class', 'module-status suspend-action');
					}
				}
			});
			return false;
		});
	});

</script>