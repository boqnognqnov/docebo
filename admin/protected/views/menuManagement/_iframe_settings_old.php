	<div class="accredIframeOptions" style="display: none;">
		<div class="useAccredIframeOptions">
			<div class="useAccredIframeOptions_confirm">
				<?php echo $form->checkBox($model, 'useAccredIframe', array('id' => 'useAccredIframe')); ?>
				<div class="useAccredIframeOptionsLabel"> <label for=""> <?php echo Yii::t('thomsonreuters', 'Enable advanced Iframe integration'); ?></label></div>
			</div>
			<div class="useAccredIframeOptions_info" style="display: none;">

				<div class="iframeHeight">
					<div class="iframeHeightLabel"><?= Yii::t('thomsonreuters', 'Iframe height (px)')?>*</div>
					<div class="iframeHeightInput">
						<?php echo $form->textField($model, 'iframe_height', array('id' => 'iframeHeightInput')) ?>
						<?php echo $form->error($model, 'iframe_height'); ?>
					</div>
				</div>

				<div class="saltSecret">
					<div class="saltSecretLabel"><?= Yii::t('thomsonreuters', 'Salt Secret') ?>*</div>
					<div class="saltSecretInput">
						<?php echo $form->passwordField($model, 'salt_secret', array('id' => 'saltSecretInput')) ?>
						<?php echo $form->error($model, 'salt_secret'); ?>
					</div>
				</div>

				<div class="repeatSaltSecret">
					<div class="repeatSaltSecretLabel"><?= Yii::t('thomsonreuters', 'Repeat Salt Secret') ?>*</div>
					<div class="repeatSaltSecretInput">
						<?php echo $form->passwordField($model, 'repeatSaltSecret', array('id' => 'repeatSaltSecretInput')) ?>
						<?php echo $form->error($model, 'repeatSaltSecret'); ?>
					</div>
				</div>

				<div class="oauthClient">
					<div class="oauthClientLabel"><?= Yii::t('thomsonreuters', 'OAuth Client') ?>*</div>
					<div class="oauthClientInput">
						<?php echo $form->dropDownList($model, 'oauth_client', $clientsList, array('id' => 'oauthClientInput')); ?>
						<?php echo $form->error($model, 'oauth_client'); ?>
					</div>
				</div>

			</div>
		</div>
	</div>

	
<script type="text/javascript">

	(function($){

		var updateUi = function() {
			
			if($('#menuItemTargetType').val() == "iframe") {
				$('.accredIframeOptions').show();
			} else {
				$('.accredIframeOptions').hide();
			}

			if($('.accredIframeOptions input#saltSecretInput').val().length > 0) {
				$('.accredIframeOptions input#repeatSaltSecretInput').val($('.accredIframeOptions input#saltSecretInput').val());
			}

			if($('#iframeHeightInput').val().length > 0 || $('#saltSecretInput').val().length > 0 || $('#repeatSaltSecretInput').val().length > 0) {
				if(!$('.accredIframeOptions input#useAccredIframe').is(':checked')) {
					$('.accredIframeOptions input#useAccredIframe').trigger('click');
				}
				$('.accredIframeOptions .useAccredIframeOptions_info').show();
			} else {
				$('.accredIframeOptions .useAccredIframeOptions_info').hide();
			}

			if($('.accredIframeOptions input#useAccredIframe').is(':checked')) {
				$('.accredIframeOptions .useAccredIframeOptions_info').show();
			} else {
				$('.accredIframeOptions .useAccredIframeOptions_info').hide();
			}

		}

		$(function(){



			$('#menuItemTargetType').on('change', function(){
				var targetType = $(this).val();
				if(targetType == "iframe") {
					$('.accredIframeOptions').show();
				} else {
					$('.accredIframeOptions').hide();
				}
			});


			$('.accredIframeOptions input#useAccredIframe').on('change', function(){
				if($(this).is(':checked')) {
					$(this).parent().next().show();
				} else {
					$(this).parent().next().hide();
				}
			});

			
			updateUi();
			
		});
		
	})(jQuery);

</script>