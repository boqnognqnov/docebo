<?php
/* @var $this MenuManagementController */
/* @var $model CoreMenuItem */
	$model = CoreMenuItem::model()->findByPk($this->idMenu);
?>

<div class="selector-top-panel">

	<br>
	<div class="row-fluid">
		<div class="span12">
			<?= Yii::t('manmenu','Apply this menu to all Groups/Branches or make a custom selection') ?>
		</div>
	</div>

	<?php if (Yii::app()->user->hasFlash('error')) : ?>
		<div class="row-fluid">
			<div class="span12">
	   			<div class='alert alert-error alert-compact text-left'>
    				<?= Yii::app()->user->getFlash('error'); ?>
    			</div>
    		</div>
    	</div>
		<?php endif; ?>
	<div class="row-fluid">
	
	<div class="row-fluid">
		<div class="grey-wrapper">
			<div class="row-fluid">
				<div class="span12">
					<div class="row-fluid">
						<div class="span4">
							<label class="radio pull-left">
								<?php
									echo CHtml::radioButton('ufilter_option', empty($model->filter), array('value' => DashboardLayout::LAYOUT_OPTION_ALL));
									echo Yii::t('standard', 'All branches and groups');
								?>
							</label>
						</div>
						<div class="span4">
							<label class="radio pull-left">
								<?php
									echo CHtml::radioButton('ufilter_option', !empty($model->filter) == DashboardLayout::LAYOUT_OPTION_SELECTED, array('value' => DashboardLayout::LAYOUT_OPTION_SELECTED));
									echo Yii::t('manmenu', 'Select groups and branches');
								?>
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$(function(){
		/* Local function to update UI  */
		var updateUi = function() {
			var elem = $('input[name="ufilter_option"]:checked');
			var modal = elem.closest('.modal');
			modal.css("min-height", "1px");
			modal.find('.modal-body').css("min-height", "1px");
			if (elem.val() == 1)
				$('.main-selector-area').hide();
			else
				$('.main-selector-area').fadeIn();
		}

		/* When Radio is changed */
		$('input[name="ufilter_option"]').on('change', function(){
			$(this).trigger('refresh');
			updateUi();
		});

		$('input[name="ufilter_option"]').trigger('change');

		// Style inputs (but see listeners, wehere we trigger "refresh"!
		$('.selector-top-panel :input[type="radio"]').styler();
	});

</script>