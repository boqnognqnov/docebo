<h1><?= Yii::t('sandbox', 'Activate sandbox') ?></h1>

<?php
$form = $this->beginWidget('CActiveForm', array(
	'method' => 'post',
	'htmlOptions' => array(
		'class' => 'form-inline ajax',
		'id' => 'create-sandbox-form',
	)
));
?>
<p>
	<b><?= Yii::t('sandbox', 'Sandbox conditions') ?></b>
</p>

<div>
	<p><?=Yii::t('sandbox', "You're about to activate your Docebo {newVersion} sandbox! The process should take about one hour to complete, and you'll receive an email when your sandbox is ready.", array('{newVersion}'=>$newVersion))?></p>
	<p><?=Yii::t('sandbox', 'What you may want to know about your sandbox')?>:</p>
	<ul style="padding-left: 20px; list-style: circle">
		<li><?=Yii::t('sandbox', 'Access the sandbox using this domain')?>: <strong><?=$sandboxDomain?></strong>.</li>
		<li><?=Yii::t('sandbox', 'The sandbox is a replica of your current production environment (with exceptions related to email addresses, content, non-admin users, custom and multidomain, SSO services) ')?>.</li>
		<li><?=Yii::t('sandbox', 'The sandbox will be available until')?> <strong><?=$expirationDate?></strong>.</li>
	</ul>

	<br/>
	<p><?=Yii::t('sandbox', 'Contact Docebo Learning and Support for questions and feedback. Sandbox-related tickets may experience a delayed resolution.')?></p>

	<p><?=Yii::t('sandbox', 'Enjoy your Docebo {newVersion} experience!', array('{newVersion}'=>$newVersion))?></p>
</div>

<br/>

<label>
	<?= CHtml::checkBox('tos_accept', false); ?> &nbsp;<?= Yii::t('sandbox', 'I accept the above conditions') ?>
</label>

<!-- For Dialog2 - they go in footer -->

<div class="form-actions">
	<!-- No SUBMIT BUTTON here! We handle 'clicks' and do AJAX form submition -->
	<?= CHtml::submitButton(Yii::t('standard', 'Confirm'), array('class' => 'confirm-save btn-primary btn btn-docebo green big')); ?>
	<?= CHtml::button(Yii::t('standard', '_CLOSE'), array('class' => 'btn-docebo black big close-dialog')); ?>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(function () {
		var dialogContainer = $('.create-sandbox-dialog');

		$('input,select', dialogContainer).styler();

		$('.confirm-save').click(function(e) {
			if($(this).hasClass('disabled')) {
				e.preventDefault();
				return;
			}
		});

		$('#tos_accept').change(function () {
			if ($(this).is(':checked')) {
				$('.confirm-save', dialogContainer).removeClass('disabled');
			} else {
				$('.confirm-save', dialogContainer).addClass('disabled');
			}
		}).trigger('change');
	});
</script>
