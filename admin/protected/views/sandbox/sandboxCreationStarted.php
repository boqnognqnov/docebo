<h1><?= Yii::t('sandbox', 'Activate sandbox') ?></h1>

<form method="post" action="" class="ajax">
	<div class="in-progress">
		<div class="check"></div>
		<div class="indent">
			<p>
				<? $currentAdminEmail = Yii::app()->user->getEmail(); ?>
				<?= Yii::t('sandbox', "We're creating your sandbox...") ?>
				<?= Yii::t('sandbox', "<b>An email will be sent to {email} as soon as it's ready.</b>", array(
					'{email}' => $currentAdminEmail ? $currentAdminEmail : '[No email for current user]',
				)) ?>
				<?= Yii::t('sandbox', 'Access your sandbox from the admin menu.') ?>
			</p>
		</div>

	</div>

	<div class="reload-after-close"></div>

	<div class="clearfix"></div>

	<div class="form-actions">
		<?= CHtml::button(Yii::t('standard', '_CLOSE'), array('class' => 'btn-docebo black big close-dialog')); ?>
	</div>
</form>
