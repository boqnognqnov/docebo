<?php
/* @var $widget DoceboLinkPager */

?>
<ul id="<?php $widget->id; ?>" class="docebo-pager">
	<?php foreach ($widget->buttons as $button) {
		$unique = substr(md5(uniqid(rand(), true)), 0, 15);
		?>
		<?php if ($button['url'] == '') { ?>
			<li class="<?php echo $button['class']; ?>"><?php echo $button['label']?></li>
		<?php } else { ?>
			<li class="<?php echo $button['class']; ?>">
				<a id="pager_button_<?php echo $unique; ?>" href="<?php echo $button['url']; ?>"><?php echo $button['label']?></a></li>
		<?php } ?>
	<?php } ?>
</ul>