<?php
	/* @var $model CoreHttps */ 
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'remove-ssl-certificate',
		'htmlOptions' => array(
			'class' => 'ajax',
			'name' => 'remove-ssl-certificate'
		),
	));
	
	$keyIsSystem = $model->keyFileOrigin == CoreHttps::KEYFILE_ORIGIN_CSRGEN;
	
?>

	<?php if ($keyIsSystem) : ?>
		<div class="row-fluid">
			<div class="span12">
				<label class="checkbox">
					<?= CHtml::checkBox('also_delete_system_key', false, array('value' => 'alsosystemkey')) ?>
					<?= Yii::t('configuration', 'Also delete system generated key') ?>
				</label>
			</div>
		</div>
	<?php endif; ?>

	<br>
	<div class="row-fluid">
		<div class="span12">
			<label class="checkbox">
				<?= CHtml::checkBox('agree_delete', false, array('value' => 'agree')) ?>
				<?= Yii::t('multidomain', 'I agree to remove the certifcate and I understand this operation <strong>cannot be undone!</strong>') ?>
			</label>
		</div>
	</div>
	
	
	<div class="form-actions">
    	<input class="btn-docebo green big" type="submit" name="confirm" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" />
    	<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
	</div>


<?php $this->endWidget(); ?>

<script type="text/javascript">
	$('input[name="also_delete_system_key"],input[name="agree_delete"]').styler();
</script>