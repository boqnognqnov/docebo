<style>
	.modal-header h3{padding-left: 0}

	.modal-body .questionbank-preview p {
		display: inline;
	}

	.modal-body .questionbank-preview {
		width: 80%;
	}
</style>
<h1><?=Yii::t('test', 'Question usage')?></h1>
<?php
$this->widget('root.common.widgets.TestQuestionPreview', array(
	'questionModel' => $question,
));
?>
<?php $this->widget('DoceboSelGridView', array(
	'id' => 'bank-test-grid',
	'htmlOptions' => array('class' => 'grid-view clearfix'),
	'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
	'dataProvider' => LearningTestQuestRel::dataProviderByQuestion($question->idQuest),
	'template' => '{items}{pager}',
	'pager' => array(
		'class' => 'DoceboCLinkPager',
		'maxButtonCount' => 8,
	),
	'columns' => array(
		array(
			'name' => Yii::t('test', 'Test name'),
			'value' => '$data->test->getTitleWithEditLink()',
			'type' => 'raw'
		),
		array(
			'name' => Yii::t('standard', '_COURSE_NAME'),
			'value' => '$data->test->getCourseName()',
		),
	),
)); ?>

<div class="form-actions">
	<?= CHtml::button(Yii::t('standard', '_CLOSE'), array('class' => 'btn-cancel close-dialog')); ?>
</div>