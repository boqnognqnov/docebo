<?php
$createUrl = Docebo::createAdminUrl('questionBank/axCreateQuestion');
?>
<div class="main-actions clearfix">
	<ul class="clearfix">
		<li>
			<div>
				<a href="<?= $createUrl ?>"
				   title="<?= Yii::t('test', 'New question') ?>"
				   alt="<?= Yii::t('test', 'New question') ?>"
				   class="new-node open-dialog"
				   rel="questcat-modal"
				   data-dialog-class="edit-questcat-modal">

					<span></span><?= Yii::t('test','New question') ?>
				</a>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>
			<p></p>
		</div>
	</div>
</div>
