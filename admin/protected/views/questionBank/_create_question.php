<style>
	.modal {
		width: 500px;
		margin-left: -250px;
	}
	.modal-header h3{padding-left: 0}
</style>
<h1><?=Yii::t('test', 'Create question')?></h1>
<?php
$form = $this->beginWidget('CActiveForm', array(
	'method' => 'POST',
	'id' => 'delete-category-form',
	'enableAjaxValidation' => false,
	'htmlOptions'=>array(
		'class' => 'ajax jwizard-form form-horizontal',
	),
));
$questionTypes = LearningTestquest::getQuestionTypesTranslationsList();
?>
<div class="row-fluid">
<?php $i = 0; ?>
<?php foreach ($questionTypes as $type => $name) { ?>
    <?php 
        // Remove Break Page from the list of the question types to be saved in the Question Bank
        if($type == 'break_page') {
            continue;
        }
    ?>
	<div class="span12 no-margin questionRow">
		<label class="radio pull-left" for="<?=$type?>"><?=$name?></label>
		<input class="pull-right" type="radio" id="<?=$type?>" name="questionType" value="<?= $type ?>">
	</div>
	<?php $i++; ?>
<?php } ?>
</div>

<div class="form-actions">
	<?= CHtml::submitButton(Yii::t('standard', '_CONTINUE'), array('class' => 'btn-submit')); ?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn-cancel close-dialog')); ?>
</div>

<?php $this->endWidget(); ?>

<style>
	.questionRow {
		border-bottom: 1px solid #e4e6e5;
		padding: 10px 10px 6px 0px;
	}

	.questionRow label {
		font-family: 'Open Sans', sans-serif;
		font-size: 13px;
		line-height: 16px;
	}

</style>

<script>
	$(function(){
		$('input').styler();
	});
</script>