<h1><?=Yii::t('test', 'Import questions from the question bank')?></h1>
<style>
	.modal {
		width: 900px;
	}
	.modal {
		margin-left: -450px;
	}
	.modal-header h3{padding-left: 0}
</style>
<?php
$form = $this->beginWidget('CActiveForm', array(
	'method' => 'POST',
	'id' => 'import-form',
	'enableAjaxValidation' => false,
));
?>

<div class="main-section questcat-management">
	<div class="filters-wrapper">
		<div class="filters clearfix">
			<div class="pull-left">
				<?php
				echo $form->dropDownList($model, 'idCategory', CHtml::listData(LearningQuestCategory::getAllCategoriesSorted(), 'idCategory', 'name'), array('prompt' => Yii::t('standard', '_ALL_CATEGORIES')));
				?>
			</div>
			<div class="input-wrapper" style="padding-top: 0px;">
				<?php
				echo $form->textField($model, 'search_input', array(
					'id' => 'search-input',
					'placeholder' => Yii::t('standard', '_SEARCH'),
				));
				?>
				<span class="search-icon" style="top: 9px;"></span>
			</div>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>
<?php
$form = $this->beginWidget('CActiveForm', array(
	'method' => 'POST',
	'id' => 'import-form-grid',
	'enableAjaxValidation' => false,
	'htmlOptions'=>array(
		'class' => 'ajax',
	),
));
?>
<div class="row-fluid">
	<?=Yii::t('standard', 'You have selected')?> <strong id="selectionCounter">0</strong> <strong><?=Yii::t('standard', 'items')?></strong>
	<a href="javascript:;" onclick="selectAll()" class="blue-link"><?=Yii::t('standard', '_SELECT_ALL')?></a>
	<a href="javascript:;" onclick="deselectAll()" class="blue-link"><?=Yii::t('standard', '_UNSELECT_ALL')?></a>
	<input type="hidden" name="selection" id="selectionVault" />
</div>
<?php
$this->widget('DoceboSelGridView', array(
	'id' => 'bank-import-grid',
	'afterAjaxUpdate' => 'function(){

	$(document).controls();}',
	'dataProvider' => $dataProvider,
	'enableSorting' => false,
	'selectableRows' => 2,
	'selVar' => 's',
	'selectionChanged' => 'function(id){updateSelectionCounter()}',
	'rowCssClassExpression' => '
		( isset($_GET["s"]) && is_array($_GET["s"]) &&  in_array( $data->primaryKey, $_GET["s"])) ? selected : "";
	',
	'columns' => array(
		array(
			'class'=>'CCheckBoxColumn',
			'id'=>'idQuest' // the columnID for getChecked
		),
		array(
			'name' => Yii::t('standard', '_QUESTION'),
			'value' => '$data->formatImportQuestionTitle()',
			'type' => 'raw',
		),
		array(
			'name' => Yii::t('standard', '_CATEGORY'),
			'value' => '$data->categoryName()'
		),
		array(
			'name' => Yii::t('standard', '_SCORE'),
			'value' => '$data->getScore()'
		),
		array(
			'name' => Yii::t('standard', '_TYPE'),
			'value' => '$data->getTypeName()'
		),
		array(
			'name' => '',
			'value' => '"<a href=\"javascript:;\" class=\"fa fa-chevron-down\" onclick=\"toggleQuestion(".$data->idQuest.", this)\"></a>"',
			'type' => 'raw',
		)
	),
));
?>
<div class="text-right form-actions">
	<? if ($dataProvider->totalItemCount > 0) : ?>
	<input type="submit" name="confirm_save_test" class="btn-docebo green big"
		   value="<?php echo Yii::t('standard', '_SAVE'); ?>">&nbsp;&nbsp;
	<? endif; ?>
	<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>


<?php $this->endWidget(); ?>

<script type="text/javascript">
	function toggleQuestion(idQuest, el)
	{
		var details = $("#toggleQuest_"+idQuest);
		var title = $("#toggleTitle_"+idQuest);
		if (details.is(":visible")) {
			details.hide();
			$(el).removeClass('fa-chevron-up');
			$(el).addClass('fa-chevron-down');
		}
		else {
			details.show();
			$(el).removeClass('fa-chevron-down');
			$(el).addClass('fa-chevron-up');
		}

		if (details.is(":visible"))
			title.hide();
		else
			title.show();

		return false;
	}

	$(function(){
		$('#import-form').on("keyup keypress", function(e) {
			var code = e.keyCode || e.which;
			if (code  == 13) {
				var search_input = $("#search-input").val();
				e.preventDefault();
				updateImportGrid();
				return false;
			}
		});
		$('#LearningTestquest_idCategory').change(function(){
			updateImportGrid();
		});

		//not selecting a row when the element is different than the checkbox column

	});
	function updateImportGrid()
	{
		$.fn.yiiGridView.update("bank-import-grid", {
			data: $("#import-form").serialize()
		});
	}

	function selectAll()
	{
		$("#bank-import-grid").find('tr.selected').each(function(){
			if ($(this).find(':checkbox').is(':checked'))
			{
				$(this).removeClass('selected');
			}
		});

		$("#bank-import-grid").selGridView("selectAllPages");
		$("#bank-import-grid").find('tr').each(function(){

				$(this).addClass('selected');
				if(!$(this).find(':checkbox').is(':checked')){
					$(this).find(':checkbox').prop('checked', true);
				}

		});
		updateSelectionCounter();
	}

	function deselectAll()
	{
		$("#bank-import-grid").find('tr.selected').each(function(){
			if ($(this).find(':checkbox').is(':checked'))
			{
				$(this).trigger('click');
				$(this).find(':checkbox').prop('checked', false);
				$(this).removeClass('selected');
			}
		});
		$("#bank-import-grid").selGridView("clearAllSelection");
		$("#bank-import-grid").find('.select-on-check').each(function(){
			if ($(this).is(':checked'))
			{
				$(this).trigger('click');
				$(this).prop('checked', false);
			}
		});
		updateSelectionCounter();
	}

	function updateSelectionCounter()
	{
		var items = $("#bank-import-grid").selGridView("getAllSelection");
		$("#selectionCounter").html(items.length);
		$("#selectionVault").val(items.join(','));
	}
</script>
<style>
	.blue-link {
		text-decoration: underline;
		color: #386eb0;
		font-weight: bold;
	}
	.blue-link:hover {
		color: #386eb0;
		text-decoration: none;
	}
</style>