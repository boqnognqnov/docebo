<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('test', '_TEST_MODALITY') => Docebo::createAdminUrl('questCategory/index'),
	Yii::t('test', 'Question bank'),
); ?>

<div class="admin-advanced-wrapper">
	<div id="sidebar" class="advanced-sidebar">
		<ul>
			<?
			echo '<li>' . CHtml::link('<i class="fa fa-database"></i></span>' . Yii::t('test', 'Question bank'), Docebo::createAdminUrl('questionBank/index'), array('class' => 'questionBank active')) . '</li>';
			echo '<li>' . CHtml::link('<i class="fa fa-folder-open"></i>' . Yii::t('menu', '_QUESTCATEGORY'), Docebo::createAdminUrl('questCategory/index'), array('class' => 'questCategory')) . '</li>';
			?>
		</ul>
	</div>
	<div id="main" class="advanced-main">
		<div class="content">
			<div class="content">

				<div class=main-actions clearfix">
				<h3 class="title-bold"><?php echo Yii::t('test', 'Question bank'); ?></h3>
				<?php $this->renderPartial('_mainActions'); ?>
			</div>

			<?php
			$form = $this->beginWidget('CActiveForm', array(
				'method' => 'POST',
				'id' => 'import-form',
				'enableAjaxValidation' => false,
			));
			?>

			<div class="main-section questcat-management">
				<div class="filters-wrapper">
					<div class="filters clearfix">
						<div class="pull-left">
							<?php
							echo $form->dropDownList($model, 'idCategory', CHtml::listData(LearningQuestCategory::getAllCategoriesSorted(), 'idCategory', 'name'), array('prompt' => Yii::t('standard', '_ALL_CATEGORIES'), 'style' => 'margin-top: 12px;'));
							?>
						</div>
						<div class="pull-left" style="margin-top: 16px; padding-left: 30px;">
							<?=$form->checkBox($model, 'unvisible')?>
							<label for="LearningTestquest_unvisible" style="display: inline-block; margin-left: 5px;"><?=Yii::t('test', 'Unarchived')?></label>
						</div>
						<div class="pull-left" style="margin-top: 16px; padding-left: 30px;">
							<?=$form->checkBox($model, 'visible')?>
							<label for="LearningTestquest_visible" style="display: inline-block; margin-left: 5px;"><?=Yii::t('test', 'Archived')?></label>
						</div>
						<div class="input-wrapper">
							<?php
							echo $form->textField($model, 'search_input', array(
								'id' => 'search-input',
								'placeholder' => Yii::t('standard', '_SEARCH'),
							));
							?>
							<span class="search-icon"></span>
						</div>
					</div>
				</div>
			</div>
			<?php $this->endWidget(); ?>

			<div class="bottom-section clearfix">
				<div id="grid-wrapper">
					<div class="row-fluid">
						<?=Yii::t('standard', 'You have selected')?> <strong id="selectionCounter">0</strong> <strong><?=Yii::t('standard', 'items')?></strong>
						<a href="javascript:;" onclick="selectAll()" class="blue-link"><?=Yii::t('standard', '_SELECT_ALL')?></a>
						<a href="javascript:;" onclick="deselectAll()" class="blue-link"><?=Yii::t('standard', '_UNSELECT_ALL')?></a>
						<select class="pull-right massive-action">
							<option><?=Yii::t('standard', 'Select action')?></option>
							<option value="invisible"><?=Yii::t('test', 'Archive questions')?></option>
						</select>
						<input type="hidden" name="selection" id="selectionVault" />
					</div>
					<?php $this->widget('DoceboSelGridView', array(
						'id' => 'bank-management-grid',
						'htmlOptions' => array('class' => 'grid-view clearfix'),
						'afterAjaxUpdate'=>'function(){$(document).controls();}',
						'dataProvider' => $model->search(),
						'enableSorting' => false,
						'selectableRows' => 2,
						'selVar' => 's',
						'itemsCssClass' => 'items non-quartz-items',
						'selectionChanged' => 'function(id){updateSelectionCounter()}',
						'template' => '{items}{pager}',
						'pager' => array(
							'class' => 'DoceboCLinkPager',
							'maxButtonCount' => 8,
						),
						'columns' => array(
							array(
								'class'=>'CCheckBoxColumn',
								'id'=>'idQuest' // the columnID for getChecked
							),
							array(
								'name' => Yii::t('standard', '_QUESTION'),
								'value' => '$data->getTitleQuestTruncated()'
							),
							array(
								'name' => Yii::t('standard', '_CATEGORY'),
								'value' => '$data->categoryName()'
							),
							array(
								'name' => Yii::t('standard', '_SCORE'),
								'value' => '$data->getScore()'
							),
							array(
								'name' => Yii::t('standard', '_TYPE'),
								'value' => '$data->getTypeName()'
							),
							array(
								'name' => Yii::t('coupon', 'Assigned to'),
								'value' => 'CHtml::link($data->getTestCount()."&nbsp;<i style=\"font-size: 18px; margin-top: 2px;\" class=\"fa fa-list\"></i>", array("questionBank/questionUsage", "idQuest" => $data->idQuest), array("class" => "open-dialog"))',
								'type' => 'raw',
								'cssClassExpression' => '"assigned-column"',
							),
							array(
								'name' => '',
								'value' => '$data->renderActionsColumn()',
								'type' => 'raw'
							),
						),
					)); ?>
				</div>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$("#LearningTestquest_visible").styler();
		$("#LearningTestquest_unvisible").styler();
	});
	function toggleVisibility(el)
	{
		$.ajax({
			url: $(el).attr('href')
		}).done(function() {
			$.fn.yiiGridView.update("bank-management-grid");
		});
	}
	$(document).delegate("#delete-question", "dialog2.content-update", function() {
		var e = $(this), autoclose = e.find("a.auto-close");
		if (autoclose.length > 0) {
			$.fn.yiiGridView.update("bank-management-grid");
		}
	});

	$(function(){
		$('#import-form').on("keyup keypress", function(e) {
			var code = e.keyCode || e.which;
			if (code  == 13) {
				var search_input = $("#search-input").val();
				e.preventDefault();
				updateImportGrid();
				return false;
			}
		});
		$('.search-icon').click(function(e){
			var search_input = $("#search-input").val();
			updateImportGrid();
			return false;
		});
		$('#LearningTestquest_idCategory').change(function(){
			updateImportGrid();
		});
		$('#LearningTestquest_visible, #LearningTestquest_unvisible').change(function(){
			updateImportGrid();
		});

		$('.massive-action').change(function(){
			var items = $("#bank-management-grid").selGridView("getAllSelection");
			if (items.length == 0)
			{
				bootbox.alert("<?=Yii::t('test', 'Please select at least one question')?>");
			}
			else
			{
				$.ajax({
					url: '<?=Docebo::createAdminUrl('questionBank/axMassAction')?>&items='+items.join(',')
				}).done(function() {
					$.fn.yiiGridView.update("bank-management-grid");
				});
			}
			$('.massive-action').val('');
		});

		//not selecting a row when the element is different than the checkbox column
		$('.items tr td').live('click', function (e) {
			if (!$(this).hasClass('checkbox-column')) {
				e.preventDefault();
				e.stopPropagation();
			}
		});

		//kill me but can't find the reason why by default the <a> does not work without any JS/events attached 0_o
		//so.. a workaround
		$(document).on('click', '.updateBankQuestion', function(e){
			window.location = $(this).attr('href');
		});

	});
	function updateImportGrid()
	{
		$.fn.yiiGridView.update("bank-management-grid", {
			data: $("#import-form").serialize()
		});
	}

	function selectAll()
	{
		$("#bank-management-grid").selGridView("selectAllPages");
		updateSelectionCounter();
	}

	function deselectAll()
	{
		$("#bank-management-grid").selGridView("clearAllSelection");
		updateSelectionCounter();
	}

	function updateSelectionCounter()
	{
		var items = $("#bank-management-grid").selGridView("getAllSelection");
		$("#selectionCounter").html(items.length);
		$("#selectionVault").val(items.join(','));
	}

</script>
<style>
	.blue-link {
		text-decoration: underline;
		color: #386eb0;
		font-weight: bold;
	}

	.blue-link:hover {
		color: #386eb0;
		text-decoration: none;
	}

	#bank-management-grid_c5{
		text-align: center !important;
	}

	.assigned-column {
		text-align: center !important;
	}
</style>

