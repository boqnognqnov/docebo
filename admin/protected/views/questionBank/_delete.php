<style>
	.modal-header h3{padding-left: 0}
</style>
<h1><?=Yii::t('test', 'Delete question')?></h1>
<?php
$form = $this->beginWidget('CActiveForm', array(
	'method' => 'POST',
	'id' => 'delete-category-form',
	'enableAjaxValidation' => false,
	'htmlOptions'=>array(
		'class' => 'ajax',
	),
));

?>
<input type="hidden" name="delete" />
<br>
<? if ($error) : ?>
	<?php echo $error?>
<? else : ?>
	<?php echo Yii::t('test','Are you sure you want to delete question <strong>{question}</strong>?', array('{question}' => $model->getTitleQuestTruncated())); ?>
<? endif; ?>

<div class="form-actions">
	<? if ($error == '') : ?>
		<?= CHtml::submitButton(Yii::t('standard', '_DEL'), array('class' => 'btn-submit')); ?>
	<? endif; ?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn-cancel close-dialog')); ?>
</div>

<?php $this->endWidget(); ?>
