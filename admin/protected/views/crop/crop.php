<div aria-labelledby="label-<?php echo $modalId; ?>" class="modal fade crop_thumbnail" id="modal-<?php echo $modalId; ?>">
	<div class="modal-header">
		<button data-dismiss="modal" class="close" type="button">×</button>
		<h3 id="label-<?php echo $modalId; ?>"><?php echo Yii::t('course_management', '_CROP_YOUR_THUMBNAIL'); ?></h3></div>
	<div class="modal-body">
		<div>
			<div style="width:<?php echo $widthNew; ?>px;">
				<img src="<?php echo $cropImage; ?>" id="cropbox" width="<?php echo $cropImageSize[0]; ?>" height="<?php echo $cropImageSize[1]; ?>"/>
			</div>
			<div class="form">
				<?php $form = $this->beginWidget('CActiveForm', array(
					'htmlOptions' => array('name' => 'CropParams', 'enctype' => 'multipart/form-data'),
				)); ?>

				<?php echo CHtml::hiddenField('CropParams[x]', ''); ?>
				<?php echo CHtml::hiddenField('CropParams[y]', ''); ?>
				<?php echo CHtml::hiddenField('CropParams[x2]', ''); ?>
				<?php echo CHtml::hiddenField('CropParams[y2]', ''); ?>
				<?php echo CHtml::hiddenField('CropParams[w]', ''); ?>
				<?php echo CHtml::hiddenField('CropParams[h]', ''); ?>
				<?php echo CHtml::hiddenField('CropParams[image]', $cropImage); ?>
				<?php echo CHtml::hiddenField('CropParams[full]', $full); ?>
				<?php echo CHtml::hiddenField('CropParams[name]', $name); ?>
				<?php echo CHtml::hiddenField('CropParams[imageType]', $imageType); ?>
				<?php echo CHtml::hiddenField('CropParams[cropCallback]', $cropCallback); ?>
				
				<?php echo CHtml::hiddenField('CropParams[full_path]', $full_path); ?>
				<?php echo CHtml::hiddenField('CropParams[crop_path]', $crop_path); ?>
				
				<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<input type="button" value="<?php echo Yii::t('standard', '_SELECT'); ?>" name="cropSelect" data-dismiss="" class="confirm-btn btn-docebo green big">
		<input type="button" value="<?php echo Yii::t('standard', '_CANCEL'); ?>" name="cropCancel" data-dismiss="modal" class="close-dialog btn-docebo black big">
	</div>

</div>

<script type="text/javascript">
	$(function(){
		$('#modal-<?php echo $modalId; ?> .btn-confirm').click(function(){
			var modal = $('#modal-<?php echo $modalId; ?>');
			modal.ajaxSubmit({
				success     : function (responseText, statusText, xhr, $form) {
					if (typeof window[responseText.cropCallback] === 'function'){
						modal[responseText.cropCallback]();
						e.preventDefault();
					}else{
						alert('Callback function does not exist: '. responseText.cropCallback);
					}
					alert(responseText.cropCallback);
					$('#modal-<?php echo $modalId; ?>').modal('hide');
				}
			});
		});
            
        $('.close-dialog').live('click', function(){
            $('.modal.hide.fade').modal('show');
        });
	})
</script>
