<?php
/* @var $this SetupController */
/* @var $infoData array */
/* @var $errors array */
/* @var $countries string[] */
/* @var $form CActiveForm */
?>

<div class="activation-screen">
	<h1><?= Yii::t('setup', 'Almost there') . '&hellip;' ?></h1>

	<h3><?= Yii::t('setup', 'Remember, we\'re here to help 24/7, also during the trial period') ?></h3>
	<h4 style="text-align: right; font-size: 0.9em; color: #999999;" class="text-uppercase"> <span style="color: red;">*</span> <?= Yii::t('setup', 'Required field') ?></h4>

	<?php
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'setup-info-form',
		'method' => 'post',
		'htmlOptions' => array(
			'class' => ''
		)
	));
	?>
		<div class="form-body">

			<div class="inner">

				<?php if (!in_array('name', $fieldsToSkip)) { ?>
				<div class="row-fluid">
					<div class="span7">
						<label for="info-name"><?= Yii::t('setup', 'What\'s your Name and Last name?') ?> <span class="colored-red">*</span></label>
					</div>
					<div class="span5">
						<input class="input-block-level" type="text" name="Setup[name]" id="info-name" value="<?= (!empty($infoData['name']) ? $infoData['name'] : '') ?>">
					</div>
				</div>
				<?php } ?>

				<?php if (!in_array('company_name', $fieldsToSkip)) { ?>
    				<div class="row-fluid">
    					<div class="span7">
    						<label for="info-company"><?= Yii::t('setup', 'What\'s your Company name?') ?> <span class="colored-red">*</span></label>
    					</div>
    					<div class="span5">
    						<input type="text" name="Setup[company_name]" id="info-company" value="<?= (!empty($infoData['company_name']) ? $infoData['company_name'] : '') ?>">
    					</div>
    				</div>
				<?php } ?>

				<?php if (!in_array('phone', $fieldsToSkip)) { ?>
    				<div class="row-fluid">
    					<div class="span7">
    						<label for="info-phone"><?= Yii::t('setup', 'What\'s your Phone or Skype ID?') ?></label>
    					</div>
    					<div class="span5">
    						<input type="text" name="Setup[phone]" id="info-phone" value="<?= (!empty($infoData['phone']) ? $infoData['phone'] : '') ?>">
    					</div>
    				</div>
				<?php } ?>

				<?php if (!in_array('company_size', $fieldsToSkip)) { ?>
    				<div class="row-fluid">
    					<div class="span7">
    						<label for="info-country"><?= Yii::t('setup', 'Company size') ?>? <span class="colored-red">*</span></label>
    					</div>
    					<div class="span5">
    						<?=CHtml::dropDownList('Setup[company_size]', $infoData['company_size'], ErpApiClient::getErpCrmCompanySizes(), array('prompt' => '-----', 'id' => 'info-size'))?>
    					</div>
    				</div>
				<?php } ?>

				<?php if (!in_array('area', $fieldsToSkip)) { ?>
    				<div class="row-fluid">
    					<div class="span7">
    						<label for="info-country"><?= Yii::t('setup', 'Industry') ?>?  <span class="colored-red">*</span></label>
    					</div>
    					<div class="span5">
    						<?=CHtml::dropDownList('Setup[area]', $infoData['area'], ErpApiClient::getErpCrmIndustries(), array('prompt' => '-----', 'id' => 'info-industry'))?>
    					</div>
    				</div>
				<?php } ?>

				<?php if (!in_array('department', $fieldsToSkip)) { ?>
    				<div class="row-fluid">
    					<div class="span7">
    						<label for="info-department"><?= Yii::t('setup', 'Department') ?>?  <span class="colored-red">*</span></label>
    					</div>
    					<div class="span5">
    						<?=CHtml::dropDownList('Setup[department]', $infoData['department'], ErpApiClient2::getDepartments(), array('prompt' => '-----', 'id' => 'info-department'))?>
    					</div>
    				</div>
				<?php } ?>
				
				<?php if (!in_array('country', $fieldsToSkip)) { ?>
    				<div class="row-fluid">
    					<div class="span7">
    						<label for="info-country"><?= Yii::t('setup', 'What\'s your Country?') ?> <span class="colored-red">*</span></label>
    					</div>
    					<div class="span5">
    						<select name="Setup[country]" id="info-country">
    							<?php if (!empty ($countries)) : ?>
    								<?php foreach ($countries as $k => $v) : ?>
    									<option value="<?= $k ?>" <?= (!empty($infoData['country']) && $infoData['country']==$k) ? 'selected="selected"' : '' ?>><?= ($v == '-' ? Yii::t('standard', '_SELECT') : $v) ?></option>
    								<?php endforeach; ?>
    							<?php endif; ?>
    						</select>
    					</div>
    				</div>
				<?php } ?>
				
				<?php if (!in_array('role', $fieldsToSkip)) { ?>
    				<div class="row-fluid">
    					<div class="span7">
    						<label for="info-role"><?= Yii::t('setup', 'Job Role') ?>?  <span class="colored-red">*</span></label>
    					</div>
    					<div class="span5">
    						<?=CHtml::dropDownList('Setup[role]', $infoData['role'], ErpApiClient2::getJobRoles(), array('prompt' => '-----', 'id' => 'info-role'))?>
    					</div>
    				</div>
				<?php } ?>
				
				<?php if (!in_array('pp_challenge', $fieldsToSkip)) { ?>
    				<div class="row-fluid">
    					<div class="span7">
    						<label for="info-pp_challenge"><?= Yii::t('setup', 'What is your biggest eLearning challenge') ?>?  <span class="colored-red">*</span></label>
    					</div>
    					<div class="span5">
    						<?=CHtml::dropDownList('Setup[pp_challenge]', $infoData['pp_challenge'], ErpApiClient2::getElearningChallenges(), array('prompt' => '-----', 'id' => 'info-pp_challenge'))?>
    					</div>
    				</div>
				<?php } ?>
				
				<?php if (!in_array('pp_already_lms', $fieldsToSkip) || !in_array('pp_end_users', $fieldsToSkip)) { ?>
    				<div class="row-fluid">
    					<div class="span7">
    						<label for="info-pp_already_lms"><?= Yii::t('setup', 'Do you already have an LMS in place') ?>?  <span class="colored-red">*</span></label>
    					</div>
    					<div class="span5">
    						<?=CHtml::dropDownList('Setup[pp_already_lms]', $infoData['pp_already_lms'], ErpApiClient2::getAlreadyLmsOprions(), array('prompt' => '-----', 'id' => 'info-pp_already_lms'))?>
    					</div>
    				</div>
					<div id="info-pp_end_users-wrapper">
    					<div class="row-fluid">
    						<div class="span7">
    							<label for="info-pp_end_users"><?= Yii::t('setup', 'How many end users does your eLearning project serve') ?>?  <span class="colored-red">*</span></label>
    						</div>
    						<div class="span5">
	    						<?=CHtml::dropDownList('Setup[pp_end_users]', $infoData['pp_end_users'], ErpApiClient2::getEndUsersServe(), array('prompt' => '-----', 'id' => 'info-pp_end_users'))?>
    						</div>
    					</div>
				</div>
    				
				<?php } ?>
				
				
				
				

				<div class="errors">
					<?php if(!empty($errors) && is_array($errors)) : ?>
					<?php foreach ($errors as $error) : ?>
						<p class="error"><?= $error ?></p>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="row-fluid text-right">
			<a class="btn submit btn-docebo green big" href="#"><?= Yii::t('course', 'Train your staff: Start Now') ?></a>
		</div>
	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
	var $form;

	var checkSetupStatus = function(){

		$form.submit();
	};

	var updateUi = function() {
		if (($('#info-pp_already_lms').val() === 'not_interested_in_lms') || !$('#info-pp_already_lms').val()) {
			$('#info-pp_end_users-wrapper').hide();
		}
		else {
			$('#info-pp_end_users-wrapper').show();
		}
	}
	
	$(document).ready(function() {

		updateUi();
		
		$('#info-pp_already_lms').on('change', function(){
			updateUi();
		});

		$form = $('#setup-info-form');

		$form.find('.submit').click(function(e){
			e.preventDefault();
			$form.find('.errors').html('');
			checkSetupStatus();
		});

		$(".menu").block({
			message: "",
			timeout: 0,
			overlayCSS:  { 
		        backgroundColor: 	'#CCC', 
		        opacity:         	0.6, 
		        cursor:          	'not-allowed' 
		    }, 
		});
		

	});
</script>