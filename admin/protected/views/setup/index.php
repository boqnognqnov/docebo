<?php
/* @var $this SetupController */
/* @var $colorschemes array */
/* @var $defaultImages array */
/* @var $logoSize array */
/* @var $loginImageSize array */
/* @var $customLoginImgUrl string */
/* @var $homeLoginImg string */
/* @var $installationUrl array */
/* @var $errors array */
/* @var $form CActiveForm */
?>

<div class="activation-screen">
    <h1><?= Yii::t('setup', 'Customize your new Docebo E-Learning Platform!') ?></h1>

	<h3><?= Settings::get('url'); ?></h3>

    <?php
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'setup-form',
		'method' => 'post',
		'htmlOptions' => array(
			//'enctype' => 'multipart/form-data',
			'class' => ''
		)
	));
	?>
    <input type="hidden" name="Setup[loginimage]" id="setup-loginimage">
    <input type="hidden" name="Setup[logo]" id="setup-logo">


	<div class="row-fluid" id="setup-errors">
		<?php if(!empty($errors) && is_array($errors)) : ?>
			<?php foreach ($errors as $error) : ?>
				<p class="error"><?= $error ?></p>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>

    <div class="row-fluid setup-row">
		<div class="span4"></div>
		<div class="span4 setup-row-header">
			<i class="step-number">1</i>
			<h3><?= Yii::t('setup', 'Upload your logo') ?></h3>
			<small><?= Yii::t('setup', 'If you don\'t have a logo, just skip this step') ?></small>
		</div>
		<div class="span8 setup-row-content">
			<div class="row-fluid">
				<div class="span6">
					<div class="logo-upload-container clearfix">
						<a href="#" id="picklogo" class="btn btn-docebo green big pull-left"><?= Yii::t('setup', 'Upload your logo') ?></a>
					</div>
					<div class="text-center" id="logo-upload-progress"></div>
				</div>
				<div class="span6">
					<div class="logo-preview-container">
						<?php echo CHtml::image(Yii::app()->theme->getLogoUrl()); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row-fluid setup-row">
		<div class="span4"></div>
		<div class="span4 setup-row-header">
			<i class="step-number">2</i>
			<h3><?= Yii::t('setup', 'Choose a cool background for your sign in page') ?></h3>
			<small class="help-signin"><i></i> <?= Yii::t('setup', 'How does the sign in page look like?') ?>
				<img src="<?= Yii::app()->theme->baseUrl . '/images/setup/x_background_image.png' ?>" alt=""/>
			</small>
		</div>
		<div class="span8 setup-row-content">
			<div class="row-fluid">
				<div class="span12">
					<div class="loginimage-upload-container clearfix">
						<a href="#" id="pickloginimage" class="btn btn-docebo green big pull-left"><?= Yii::t('setup', 'Upload your background') ?></a>
						<small ><?= Yii::t('setup', 'Max dimensions: <br>{w}x{h} px - {size}', array(
								'{w}' => $loginImageSize['width'],
								'{h}' => $loginImageSize['height'],
								'{size}' => $loginImageSize['filesize']
							)) ?></small>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6">
					<div class="loginimage-preview-container <?= (empty($customLoginImgUrl) ? 'hide' : '') ?>">
						<?php if (!empty($customLoginImgUrl)) echo CHtml::image($customLoginImgUrl) ?>
					</div>
				</div>
			</div>

			<div class="branding-manage-backgrounds setup-manage-backgrounds default-bg-container">

				<h3><?= Yii::t('setup', '&hellip; or choose a default background') ?></h3>

				<div id="defaultSlider" class="carousel slide thumbnails-carousel text-center">
					<!-- Carousel items -->
					<div class="carousel-inner">
						<?php if (!empty($defaultImages)) { ?>
							<?php foreach ($defaultImages as $key => $defaultThumbnail) { ?>
								<div class="item<?php echo ($key == 0) ? ' active' : ''; ?>">
									<?php foreach ($defaultThumbnail as $key => $imageUrl) {
										$html = CHtml::image($imageUrl);
										$html .= '<input type="radio" name="Setup[default_loginimage]" value="' . $key . '" '. ($key == $homeLoginImg ? 'checked="checked"' : '') . '/>';

										echo '<div class="sub-item ' . '">' . $html . '</div>';
									}
									?>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
					<!-- Carousel nav -->
					<a class="carousel-control left" href="#defaultSlider" data-slide="prev">&lsaquo;</a>
					<a class="carousel-control right" href="#defaultSlider" data-slide="next">&rsaquo;</a>
				</div>
			</div>
		</div>
	</div>

	<div class="row-fluid setup-row">
		<div class="span4"></div>
		<div class="span4 setup-row-header">
			<i class="step-number">3</i>
			<h3><?= Yii::t('setup', 'Choose the color scheme for your new Docebo') ?></h3>
		</div>
		<div class="span8 setup-row-content no-bottom-padding">
			<ul class="setup-color-schemes clearfix">
				<?php foreach ($colorschemes as $colorscheme) : ?>
					<?php $colorschemeId = str_replace(' ', '_', $colorscheme->title); ?>
					<li>
						<label class="checkbox <?= ($xx==1 ? 'checked' : '') ?>" for="colorscheme_<?= $colorschemeId ?>" title="<?= ucfirst($colorscheme->title) ?>">
                            <?php $palette = $colorscheme->getColorPalette(); ?>
							<div class="palette">
								<div class="primary" style="background-color: <?php echo $palette['dark'] ?>;"></div>
								<div class="secondary clearfix">
									<div style="background-color: <?php echo $palette['middle'] ?>;"></div>
									<div style="background-color: <?php echo $palette['light'] ?>;"></div>
									<div style="background-color: <?php echo $palette['title'] ?>;"></div>
								</div>
							</div>
							<div class="hover">
								<div class="arrow_box">
									<?= CHtml::image(Yii::app()->theme->baseUrl . '/images/setup/scheme/scheme'. $colorscheme->getPrimaryKey() .'.jpg') ?>
								</div>
							</div>
							<span class="check-mark"></span>
							<input type="radio" name="Setup[colorscheme]" id="colorscheme_<?= $colorschemeId ?>" value="<?= $colorscheme->getPrimaryKey() ?>">
						</label>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>

    <div class="row-fluid text-right">
        <input type="submit" class="btn btn-docebo green big" value="<?= Yii::t('standard', '_NEXT') ?>">
	</div>
	<?php $this->endWidget(); ?>
</div>


<script type="text/javascript">
<!--

	var oSetup = {

		logoPlUploader: false,
		loginImagePlUploader: false,

		refresh: function() {

			if (this.logoPlUploader)
				this.logoPlUploader.refresh();

			if (this.loginImagePlUploader)
				this.loginImagePlUploader.refresh();

						
		},
			
		initLogoUploader: function() {
			var that = this;
			var $btnPicklogo = $('#picklogo');
			var pluploader = new plupload.Uploader({
				chunk_size: '1mb',
				runtimes: 'html5,flash',
				browse_button: 'picklogo',
				container: 'setup-form',
				max_file_size: '<?= $logoSize['filesize'] ?>',
				url: '<?= Docebo::createAppUrl('lms:site/axUploadFile') ?>',
				flash_swf_url: '<?= Yii::app()->plupload->getAssetsUrl() . '/plupload.flash.swf' ?>',
				multipart_params: { <?php echo Yii::app()->request->csrfTokenName; ?>: '<?= Yii::app()->request->csrfToken ?>' },
				filters: [
					{title : "Image files", extensions : "jpg,jpeg,gif,png"}
				]
			});

			this.logoPlUploader = pluploader; 
			
			pluploader.bind('Init', function(up, params) {});

			pluploader.init();

			pluploader.bind('FilesAdded', function(up, files) {

				$('#setup-logo').val('');
				$btnPicklogo.addClass('disabled');

				pluploader.start();
				that.refresh(); // Reposition Flash/Silverlight
			});

			pluploader.bind('UploadProgress', function(up, file) {
				//$('#' + file.id + " b").html(file.percent + "%");
				//$('#logo-upload-progress').html(file.percent + "%");
				that.refresh();
			});

			pluploader.bind('Error', function(up, err) {
				that.refresh(); // Reposition Flash/Silverlight
			});

			pluploader.bind('FileUploaded', function(up, file) {
				$btnPicklogo.removeClass('disabled');
				//$('#logo-upload-progress').html("100%");

				var imgSrc = '<?= Docebo::createAbsoluteLmsUrl('stream/image') ?>' + '&fn=' + file.name + '&col=uploads';
				
				//$('.logo-preview-container').html('<img src="<?= Docebo::getUploadTmpUrl() ?>/'+file.name+'">');
				$('.logo-preview-container').html('<img src="' + imgSrc + '"/>');
				
				$('#setup-logo').val(file.name);
				that.refresh();
			});

		},
		initLoginImageUploader: function() {
			var that = this;
			var $btnPickloginimage = $('#pickloginimage');
			var pluploader = new plupload.Uploader({
				chunk_size: '1mb',
				runtimes: 'html5,flash',
				browse_button: 'pickloginimage',
				container: 'setup-form',
				max_file_size: '<?= $loginImageSize['filesize'] ?>',
				url: '<?= Docebo::createAppUrl('lms:site/axUploadFile') ?>',
				flash_swf_url: '<?= Yii::app()->plupload->getAssetsUrl() . '/plupload.flash.swf' ?>',
				multipart_params: { <?php echo Yii::app()->request->csrfTokenName; ?>: '<?= Yii::app()->request->csrfToken ?>' },
				filters : [
					{title : "Image files", extensions : "jpg,jpeg,gif,png"}
				]
			});

			this.loginImagePlUploader = pluploader;
			
			pluploader.bind('Init', function(up, params) {
			});

			pluploader.init();

			pluploader.bind('FilesAdded', function(up, files) {

				$('#setup-loginimage').val('');
				$btnPickloginimage.addClass('disabled');

				pluploader.start();
				that.refresh(); // Reposition Flash/Silverlight
			});

			pluploader.bind('UploadProgress', function(up, file) {
				//$('#' + file.id + " b").html(file.percent + "%");
				that.refresh();
			});

			pluploader.bind('Error', function(up, err) {
				that.refresh(); // Reposition Flash/Silverlight
			});

			pluploader.bind('FileUploaded', function(up, file) {
				$btnPickloginimage.removeClass('disabled');
				//$('#logo-upload-progress').html("100%");
				
				var imgSrc = '<?= Docebo::createAbsoluteLmsUrl('stream/image') ?>' + '&fn=' + file.name + '&col=uploads';

				//$('.loginimage-preview-container').html('<img src="<?= Docebo::getUploadTmpUrl() ?>/'+file.name+'">').removeClass('hide');
				$('.loginimage-preview-container').html('<img src="' + imgSrc + '"/>').show();
				
				$('#setup-loginimage').val(file.name);
				that.refresh();
			});
		},
		ready: function() {
			this.initLogoUploader();
			this.initLoginImageUploader();

			this.refresh();

			var $chkDefaultBg = $('.default-bg-container .sub-item input:checked');
			if ($chkDefaultBg.length) {
				$chkDefaultBg.parent().click();
			}
		}
	};

	$(document).ready(function(){

		$('.carousel').find('input,select').styler();

		$('.default-bg-container label.checkbox').click(function(){
			$(this).find('input').attr('checked', 'checked');
		});

		$('.default-bg-container .item > div').live('click', function() {
			$('.default-bg-container .item > div').removeClass('checked');
			$(this).addClass('checked');
			$(this).find('input[type="radio"]').prop('checked', true);
			$(this).parent().find('.jq-radio').removeClass('checked');
			$(this).find('.jq-radio').addClass('checked');
			var $img = $(this).find('img').clone();
			$('.loginimage-preview-container').addClass('hide');
			$('.loginimage-preview-container').hide();
			$('#setup-loginimage').val('');
		});

		$('.setup-color-schemes label').live('click', function(){
			$('.setup-color-schemes label').removeClass('checked');
			$(this).addClass('checked');
			$(this).find('input').attr('checked', 'checked');
		});

		oSetup.ready();

		$(".menu").block({
			message: "",
			timeout: 0,
			overlayCSS:  { 
		        backgroundColor: 	'#CCC', 
		        opacity:         	0.6, 
		        cursor:          	'not-allowed' 
		    }
		});
		
		
    });
    
//-->
</script>
	    