<style>
	.certificate-tags-div .certificate-tags-wrapper ul.nav.nav-tabs > li > a:hover {
		/*background-color: #58807F !important;*/
		color: #F1F3F2;
	}
	.certificate-tags-div .certificate-tags-wrapper ul.nav.nav-tabs{
		float: none !important;
		display: table-cell;
	}
	.certificate-tags-div .certificate-tags-wrapper div.tab-content{
		display: table-cell;
		float: none;
	}
	.certificate-tags-div .certificate-tags-wrapper{
		border-top: 0;
		display: table-row;
	}
	.certificate-tags-div .tag-header{
		color: #333333;
		padding-top: 5px;
	}
	.certificate-tags-div .nav-tabs.nav-stacked>li>a{
		border-radius: 0 !important;
	}
	.certificate-tags-div .certificate-tags-wrapper .advanced-sidebar .nav-tabs li a span{
		display: inline;
		background: none;
	}
	.certificate-tags-div .certificate-tags-wrapper > div{
		width: 100%;
	}
	.certificate-tags-div .certificate-tags-wrapper div.tab-content{
		display: block;
	}
	.certificate-tags-div .tag-left-side{
		margin-right: 0;
	}
	.certificate-tags-div .certificate-tags-wrapper .code {
		width: 34%;
	}

	.certificate-tags-div .certificate-tags-wrapper .description {
		width: 62%;
	}
	.certificate-tags-div .advanced-sidebar li a span{
		background: none;
		width: 100%;
		text-transform: capitalize
	}
	@media (max-width: 1200px){
		.certificate-important .important-text{
			width: 800px;
		}
	}
	@media (max-width: 979px){
		.certificate-tags-div .certificate-tags-wrapper .code {
			width: 50%;
		}
		.certificate-tags-div .certificate-tags-wrapper .description {
			width: 45%;
		}
		.certificate-important .important-text{
			width: 580px;
		}
	}

	@media (max-width: 767px){
		.certificate-tags-div .advanced-main .content{
			padding-left: 0;
		}
		.certificate-tags-div .advanced-sidebar{
			width: 180px;
		}
		.certificate-tags-div .advanced-main{
			margin-left: 180px;
		}
		.certificate-tags-div .certificate-tags-wrapper .code {
			width: 60%;
		}
		.certificate-tags-div .certificate-tags-wrapper .description {
			width: 35%;
		}
		.certificate-important .important-text{
			width: 520px;
		}
	}
	@media (max-width: 667px){
		.certificate-important .important-text{
			width: 440px;
		}
	}
</style>
<?php $uniqueId = substr(md5(uniqid(rand(), true)), 0, 10); ?>

<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', '_CERTIFICATES') => array('certificateManagement/index'),
	Yii::t('certificate', '_STRUCTURE_CERTIFICATE'),
); ?>

<h3 class="title-bold back-button">
	<?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('certificateManagement/index')); ?>
	<span><?php echo Yii::t('certificate', '_STRUCTURE_CERTIFICATE'); ?></span>
</h3>

<div class="certificate-important">
	<span class="important-message"><span class="important-icon"></span><span class="important-text"><?php echo CHtml::encode(Yii::t('certificate', '_CERTIFICATE_WARNING')); ?></span></span>
	<?php echo CHtml::link('close', 'javascript:void(0);', array('onClick' => '$(this).parents(\'.certificate-important\').hide();', 'class' => 'important-close')); ?>
</div>

<div class="form certificate-template-form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'certificate_template_form',
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
	)); ?>
		<?php echo $form->hiddenField($model, 'id_certificate'); ?>

		<?php echo $form->labelEx($model, Yii::t('certificate', '_STRUCTURE_CERTIFICATE')); ?>
		<?php echo $form->textArea($model, 'cert_structure', array('id' => 'tiny_'. $uniqueId)); ?>
		<?php echo $form->error($model, 'cert_structure'); ?>

	<div class="clearfix">
		<div class="certificate-orientation">
			<div>
				<label for="LearningCertificate_orientation"><?php echo Yii::t('certificate', '_ORIENTATION') . ':'; ?></label>
				<?php echo $form->radioButtonList($model, 'orientation', array('P' => '<span class="icon-portrait"></span>'.Yii::t('certificate', '_PORTRAIT'), 'L' => '<span class="icon-landscape"></span>'.Yii::t('certificate', '_LANDSCAPE')), array('separator' => '')); ?>
				<?php echo $form->error($model, 'orientation'); ?>
				<br />
				<br />
				<?php echo $form->dropDownList($model, 'base_language', CoreLangLanguage::getActiveLanguages()); ?>
			</div>
		</div>

		<div class="certificate-background">
			<div>
				<?php //echo $form->labelEx($model, 'orientation').':'; ?>
				<label for="LearningCertificate_orientation"><?php echo Yii::t('certificate', '_BACK_IMAGE') . ':'; ?></label>
				<?php echo $form->fileField($model, 'background_image', array()); ?>
				<div class="file-name-wrapper">
					<span class="file-name"></span>
				</div>
				<?php if (!empty($model->bgimage)) : ?>
					<a href="javascript:;" class="pull-right remove-background"
					   onclick="removeCertificateImg('<?= Docebo::createAdminUrl('certificateManagement/deleteImage', array('id' => $model->primaryKey)) ?>')"
					   style="margin-top: 8px;" title="<?= Yii::t('standard', '_DELETE_FILE') ?>">
						<i class="i-sprite is-remove red"></i>
					</a>
				<? endif; ?>
			</div>
		</div>
		<div class="background-wrapper">
			<div>
				<?php if (!empty($model->bgimage)) {
					echo CHtml::image($model->getBgImageUrl(CoreAsset::VARIANT_SMALL));
				} ?>
			</div>
		</div>
	</div>
	<div class="clearfix certificate-btns">
		<?php echo CHtml::link(Yii::t('standard', '_CONFIRM'), 'javascript:void(0);', array('class' => 'btn btn-submit', 'onClick' => '$(this).parents(\'form\').submit();')); ?>
		<?php echo CHtml::link(Yii::t('standard', '_CANCEL'), array('certificateManagement/index'), array('class' => 'btn btn-cancel')); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>

<h3 class="title-bold" style="margin-bottom: 10px"><?php echo Yii::t('certificate', '_TAG_LIST_CAPTION'); ?></h3>
<div class="content certificate-tags-div" style="display: table;width: 100%">
	<div class="certificate-tags-wrapper clearfix ">
		<div class="admin-advanced-wrapper">
			<div class="advanced-sidebar">
				<ul>
					<li class="active">
						<a data-toggle="tab" href="#user" class="active">
							<span><?= Yii::t('report_filters', '_FIELDS_USER') ?></span>
						</a>
					</li>
					<li>
						<a data-toggle="tab" href="#course">
							<span><?= Yii::t('report_filters', '_FIELDS_COURSE') ?></span>
						</a>
					</li>
					<li>
						<a data-toggle="tab" href="#other">
							<span><?= Yii::t('report_filters', 'Other fields') ?></span>
						</a>
					</li>
					<?php
					if (PluginManager::isPluginActive('CurriculaApp')) { ?>
						<li>
							<a data-toggle="tab" href="#coursepath">
								<span><?= Yii::t('report_filters', 'Learning plan fields') ?></span>
							</a>
						</li>
					<?php } ?>
				</ul>
			</div>
			<div class="advanced-main">
				<div class="content tab-content">
					<div id="user" class="tab-pane active">
						<div class="tag-left-side">
							<div class="tag-header clearfix">
								<div class="code"><?php echo Yii::t('certificate', '_TAG_CODE'); ?></div>
								<div class="description"><?php echo Yii::t('certificate', '_TAG_DESCRIPTION'); ?></div>
							</div>

							<?php if (!empty($tags['user']) && is_array($tags['user'])) { ?>
								<?php foreach ($tags['user'] as $tagCode => $tagDescription) { ?>
									<div class="tag-content">
										<div class="clearfix">
											<div class="code"><?php echo $tagCode; ?></div>
											<div class="description"><?php echo $tagDescription; ?></div>
										</div>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
					</div>
					<div id="course" class="tab-pane">
						<div class="tag-left-side">
							<div class="tag-header clearfix">
								<div class="code"><?php echo Yii::t('certificate', '_TAG_CODE'); ?></div>
								<div class="description"><?php echo Yii::t('certificate', '_TAG_DESCRIPTION'); ?></div>
							</div>

							<?php if (!empty($tags['course']) && is_array($tags['course'])) { ?>
								<?php foreach ($tags['course'] as $tagCode => $tagDescription) { ?>
									<div class="tag-content">
										<div class="clearfix">
											<div class="code"><?php echo $tagCode; ?></div>
											<div class="description"><?php echo $tagDescription; ?></div>
										</div>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
					</div>
					<div id="other" class="tab-pane">
						<div class="tag-left-side">
							<div class="tag-header clearfix">
								<div class="code"><?php echo Yii::t('certificate', '_TAG_CODE'); ?></div>
								<div class="description"><?php echo Yii::t('certificate', '_TAG_DESCRIPTION'); ?></div>
							</div>

							<?php if (!empty($tags['other']) && is_array($tags['other'])) { ?>
								<?php foreach ($tags['other'] as $tagCode => $tagDescription) { ?>
									<div class="tag-content">
										<div class="clearfix">
											<div class="code"><?php echo $tagCode; ?></div>
											<div class="description"><?php echo $tagDescription; ?></div>
										</div>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
					</div>
					<?php if (PluginManager::isPluginActive('CurriculaApp')) { ?>
						<div id="coursepath" class="tab-pane">
							<div class="tag-left-side">
								<div class="tag-header clearfix">
									<div class="code"><?php echo Yii::t('certificate', '_TAG_CODE'); ?></div>
									<div class="description"><?php echo Yii::t('certificate', '_TAG_DESCRIPTION'); ?></div>
								</div>

								<?php if (!empty($tags['coursepath']) && is_array($tags['coursepath'])) { ?>
									<?php foreach ($tags['coursepath'] as $tagCode => $tagDescription) { ?>
										<div class="tag-content">
											<div class="clearfix">
												<div class="code"><?php echo $tagCode; ?></div>
												<div class="description"><?php echo $tagDescription; ?></div>
											</div>
										</div>
									<?php } ?>
								<?php } ?>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		initTinyMCE('#' + $('#certificate_template_form textarea').attr('id'), 316);
		$('input').styler({browseText: "<?= Yii::t('organization', 'Upload File') ?>"});
		$(document).on('click', '.certificate-tags-div .admin-advanced-wrapper .advanced-sidebar ul li', function(){
			var that = $(this);
			$('.certificate-tags-div .admin-advanced-wrapper .advanced-sidebar ul li a').removeClass('active');
			that.find('a').addClass('active');
		})

	});
</script>
