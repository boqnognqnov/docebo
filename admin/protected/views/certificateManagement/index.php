<?php

$this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', '_CERTIFICATES'),
);

$canAddCertificates = Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/lms/admin/certificate/mod');
$canModCertificates = Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/lms/admin/certificate/mod');
$canDelCertificates = Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/lms/admin/certificate/mod');

$this->renderPartial('//common/mainAction/_mainCertificateActions');

?>
<div class="bottom-section-certificates clearfix">
	<div id="grid-wrapper">
		<?php

			$_columns = array(
				array(
                    'name' => 'code',
                    'header' => Yii::t('standard', '_CODE'),
                ),
				array(
                    'name' => 'name',
                    'header' => Yii::t('standard', '_NAME'),
                ),
				array(
                    'name' => 'description',
                    'type' => 'raw',
                    'header' => Yii::t('standard', '_DESCRIPTION'),
                )
			);

			if ($canModCertificates) {
				$_columns[] = array(
					'type' => 'raw',
					'value' => array($this, 'gridRenderDownloadTemplateButton'),
					'htmlOptions' => array('class' => 'button-column-single')
				);

				$_columns[] = array(
					'type' => 'raw',
					'value' => array($this, 'gridRenderTemplateButton'),
					'htmlOptions' => array('class' => 'button-column-single')
				);
			}

			$_columns[] = array(
				'header' => '',
				'type' => 'raw',
				'value' => array($this, 'gridRenderReleasedButton'),
				'htmlOptions' => array('class' => 'button-column-single')
			);

			if ($canModCertificates) {
				$_columns[] = array(
					'header' => '',
					'type' => 'raw',
					'value' => array($this, 'gridRenderUpdateButton'),
					'htmlOptions' => array('class' => 'button-column-single')
				);
			}

			if ($canDelCertificates) {
				$_columns[] = array(
					'name' => '',
					'type' => 'raw',
					'value' => array($this, 'gridRenderDeleteButton'),
					'htmlOptions' => array('class' => 'button-column-single')
				);
			}

			$this->widget('zii.widgets.grid.CGridView', array(
				'id' => 'certificate-management-grid',
				'htmlOptions' => array('class' => 'grid-view clearfix'),
				'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
				'dataProvider' => $certificateModel->dataProvider(),
				'template' => '{items}{summary}{pager}',
				'summaryText' => Yii::t('standard', '_TOTAL') . '',
				'pager' => array(
					'class' => 'DoceboCLinkPager',
					'maxButtonCount' => 8,
				),
				'ajaxUpdate' => 'all-items',
				'columns' => $_columns,
				'afterAjaxUpdate' => 'function(id, data){ $(\'a[rel="tooltip"]\').tooltip(); }',
			));
		?>
	</div>
</div>

<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input, select').styler();
			replacePlaceholder();
		});
	})(jQuery);
</script>
