<?php /* @var $form CActiveForm */ ?>
<?php /* @var $isCurriculaPluginActive bool */ ?>
<?php /* @var $certificateCourseList array */ ?>
<?php /* @var $certificateAssignList array */ ?>
<?php /* @var $learningCourseUser LearningCourseuser */ ?>
<?php /* @var $learningCoursepathUser LearningCoursepathUser */ ?>
<?php /* @var $dataProvider CActiveDataProvider */ ?>
<?php /* @var $canModCertificates bool */ ?>

<div class="form">
	<div id="certificate-flash-error"></div>
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'certificate_form',
		'htmlOptions' => array('class' => 'ajax-grid-form', 'data-grid' => '#certificate-release-management-grid'),
	)); ?>

	<?php echo CHtml::hiddenField('certificateId', $certificateId); ?>

    <?php if ($isCurriculaPluginActive) : ?>
    <div class="clearfix">
        <label><?= Yii::t('certificate', 'Issued certificates for') ?>:</label>
        <span>
            <?= CHtml::label(
                CHtml::radioButton('issue_for', true, array('id'=>'issue_for_course','value'=>'course')) . ' ' .
                Yii::t('standard', '_COURSE'), 'issue_for_course', array('class'=>'radio')); ?>
            <?= CHtml::label(
                CHtml::radioButton('issue_for', false, array('id'=>'issue_for_plan','value'=>'plan')) . ' ' .
                Yii::t('standard', '_COURSEPATH'), 'issue_for_plan', array('class'=>'radio')); ?>
        </span>
    </div>
    <br/>
    <?php endif; ?>

    <div class="clearfix course-dd-container">
        <label style="width:14%;"><?php echo Yii::t('standard', '_COURSE'); ?>:</label>
        <?php echo $form->dropDownList($learningCourseUser, 'idCourse', $certificateCourseList, array('empty' => Yii::t('course', '_COURSE_SELECTION'), 'onChange' => '$(this).parents(\'form\').submit();', 'style'=>'width:82%;float:right;')); ?>
    </div>

    <?php if ($isCurriculaPluginActive) : ?>
    <div class="clearfix plan-dd-container" style="display: none;">
        <label style="width:14%;"><?php echo Yii::t('standard', '_COURSEPATH'); ?>:</label>
        <?php echo $form->dropDownList($learningCoursepathUser, 'id_path', $certificateAssignList, array('empty' => Yii::t('standard', '_SELECT'), 'onChange' => '$(this).parents(\'form\').submit();', 'style'=>'width:82%;float:right;')); ?>
    </div>
    <?php endif; ?>

	<div class="main-section">
		<?php if($canModCertificates) : ?>
			<div class="filters-wrapper">
				<div class="selections" data-grid="certificate-release-management-grid">
					<div class="input-wrapper">
					<?php //pr($learningCourseUser,0); ?>
						<?php echo $form->textField($learningCourseUser->user, 'userid', array(
							'id' => 'advanced-search-certificate-user',
							//'name' => 'CoreUser[userid]',
							'class' => 'typeahead',
							'autocomplete' => 'off',
							'data-url' => Docebo::createAppUrl('admin:userManagement/usersAutocomplete'),
							'data-type' => 'core',
							'data-course' => -1,
							// 'data-onlyUsers' => $onlyUsers,
							'placeholder' => Yii::t('standard', '_SEARCH'),
							'data-source-desc'=>'true',
						));

	// pr($learningCourseUser->idCourse);
						?>
						<span class="search-icon"></span>
					</div>
					<div class="actions">
						<label for="certificate-release-management-massive-action"><?php echo Yii::t('standard', '_ON_SELECTED'); ?></label>
						<select name="massive_action" id="certificate-release-management-massive-action">
							<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
							<option value="generate"><?php echo Yii::t('certificate', '_GENERATE_CERTIFICATES'); ?></option>
							<option value="delete"><?php echo Yii::t('certificate', '_DELETE_ALL_GENERATED'); ?></option>
						</select>
					</div>

					<?php
					//TODO: when managing a high number of records, GridViewSelectAll component may take a too long time to be rendered (often resulting in page timeout)
					/*$this->widget('ext.local.widgets.GridViewSelectAll', array(
						'gridId' => 'certificate-release-management-grid',
						'class' => 'left-selections',
						'dataProvider' => $learningCourseUser->dataProviderCertificate(),
						'itemValue' => '$data->learningCertificateAssign->id_certificate',
					));*/
					?>

				</div>
			</div>
		<?php endif;?>
	</div>
	<input type="hidden" name="contentType" value="html"/>
	<?php $this->endWidget(); ?>

	<div id="grid-wrapper">
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'certificate-release-management-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
			'dataProvider' => $dataProvider,
			'template' => '{items}{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'pager' => array(
				'class' => 'DoceboCLinkPager',
				'maxButtonCount' => 8,
				'extraUrlParams' => array(
					'LearningCourseuser[idCourse]' => $idCourse,
					'LearningCoursepathUser[id_path]' => $id_path
				)
			),
			'ajaxUpdate' => 'certificate-release-management-grid-all-items',
			'cssFile' => false,
			'beforeAjaxUpdate' => 'function(id, options) {
				options.type = "POST";
				if (options.data == undefined) {
					options.data = {
						contentType: "html",
						selectedItems: $(\'#\'+id+\'-selected-items-list\').val()
					}
				}
			}',
			'afterAjaxUpdate' => 'function() {
				$(\'#certificate-release-management-grid input\').attr(\'checked\', false);
				var courseId = $(\'.modal.in\').find(\'#LearningCourseuser_idCourse\').val();
				$(\'.modal.in\').find(\'#advanced-search-certificate-user\').data(\'course\', courseId);
				$(\'input, select\').styler();
				$(\'[data-toggle=tooltip]\').tooltip();
			}',
			'columns' => array(
				array(
					'class' => 'CCheckBoxColumn',
					'selectableRows' => 2,
					'id' => 'certificate-release-management-grid-selected-items-list',
					'value' => '$data->idUser',
					'checked' => 'in_array($data->idUser, Yii::app()->session[\'selectedItems\'])',
					'visible' => $canModCertificates
				),
				array(
					'header' => Yii::t('standard', '_USERNAME'),
					'name' => 'user.userid',
					'value' => 'Yii::app()->user->getRelativeUsername($data->user->userid);'
				),
				array(
					'header' => Yii::t('standard', '_FULLNAME'),
					'name' => 'user.fullname',
					'value' => '$data->user->getFullName();'
				),
				array(
					'header' => Yii::t('standard', '_CERTIFICATES'),
					'type' => 'raw',
					'value' => '$data->renderCertificateStatus();',
					'htmlOptions' => array('class' => 'certificate-status'),
					'headerHtmlOptions' => array('class' => 'certificate-status')
				),
			),
		)); ?>
	</div>

</div>

<script type="text/javascript">
    $(document).ready(function() {

        //Certificates are assigned and saved through ajax calls above,
        //submit button in this case essentially does nothing
	    $("#certificate_form").submit(function(e) {
            $("#certificate-release-management-grid-all-items-list").val("");
            e.preventDefault();
	    });

        $('.btn-submit ').click(function() {
            $('.modal.in').modal('hide');
            $('.modal.in').remove();
            $('#bootstrapContainer').remove();
        });

        var $modal = $('.modal.release-node');

        $modal.find('input[name="issue_for"]').change(function() {
            var val = $(this).val();
            if (val == 'plan') {
                $modal.find('.course-dd-container').hide();
                $modal.find('.plan-dd-container').show();
            }
            else {
                $modal.find('.course-dd-container').show();
                $modal.find('.plan-dd-container').hide();
            }
        });
    });
</script>