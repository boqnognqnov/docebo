<?php $uniqueId = substr(md5(uniqid(rand(), true)), 0, 10); ?>

<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'certificate_form',
	)); ?>
		<?php echo $form->hiddenField($model, 'id_certificate'); ?>
		<div class="form-line">
			<?php echo $form->labelEx($model, 'code'); ?>
			<?php echo $form->textField($model, 'code'); ?>
			<?php echo $form->error($model, 'code'); ?>
		</div>
		<div class="form-line">
			<?php echo $form->labelEx($model, 'name'); ?>
			<?php echo $form->textField($model, 'name'); ?>
			<?php echo $form->error($model, 'name'); ?>
		</div>

		<?php echo $form->labelEx($model, 'description'); ?>
		<?php echo $form->textArea($model, 'description', array('id' => 'tiny_'. $uniqueId)); ?>
		<?php echo $form->error($model, 'description'); ?>

	<?php $this->endWidget(); ?>
</div>