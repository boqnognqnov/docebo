<?php
    /**
     * @var AdvancedSettingsELearningForm $model
     */
?>
<div id="tincan-urls-settings" class="row">
	<div class="setting-name"><?php echo Yii::t('admin', 'XAPI external content'); ?>
		<div class="elearning-iframes-whitelist-subtitle"><?php echo Yii::t('admin', 'A predefined list of URL to externally hosted XAPI content.'); ?></div>
	</div>
	
	<div class="values">

        <div class="row-fluid">
        
        	<div class="span8">
        		<?php 
                    echo CHtml::textField('add_tincan_url', '', array(
                        'placeholder' => 'http://',
                        'style' => 'width: 70%;',
                        'id' => 'add-tincan-url')); 
                ?> 
                <?php
                    echo CHtml::button(Yii::t('standard', '_ADD'), array(
                        'class' => 'btn btn-submit',
                        'style' => 'min-width: 60px; margin-left: 10px;',
                        'name' => 'add-tincan-url-button', 
                        'id' => 'add-tincan-url-button',
                        
                    )); 
                ?>
        		<div class="tincan-urls-error"><span class="text-error"></span><span class="text-success"></span></div>
        		
        	</div>
        	
        	<div class="span4 search-input-wrapper">
        		<?php 
                    echo CHtml::textField('search_tincan_url',$filter,array(
                        'placeholder' =>'Search',
                        'style'=>'width: 90%',
                        'id'=>'search-tincan-url',
                    )); 
                ?>
        		<button type="button" class="close clear-search">&times;</button>
        		<span id="perform-search-tincan-urls" class="perform-search search-icon"></span>
        	</div>
        	
        	
        	<div id="tincan-urls-list-wrapper">
        		<div id="tincan-urls-list">
        			
        		</div>
        		<div id="tincan-urls-inputs">
        			
        		</div>
        	</div>
        	
        	
        </div>

	</div>
</div>

<script type="text/javascript">

	(function($) {

		var urlInputName = 'AdvancedSettingsELearningForm[tincan_urls]';

		// Unique, temporary ID counter
		var tincanUrlSt = 0;

		// Load current list of URLs
		var currentList = <?= json_encode($model->getTinCanUrls()) ?>;

		/**
		 * Add new URL 
		 */
		function addUrl(url) {
			var newUrlId = "tincan-url-id-" + tincanUrlSt++;
			
			var urlElement = $('<div class="row-fluid iframe-source-row"><div class="span11 url-value">' + url + '</div><div class="span1"><i class="fa fa-times fa-lg text-colored-red delete-url"></i></div></div>');
			urlElement.attr("id", newUrlId);
			$('#tincan-urls-list').prepend(urlElement);

			var inputElement = $('<input type="hidden" />');
			inputElement.attr('name', urlInputName+'['+newUrlId+']').val(url);
			$('#tincan-urls-inputs').prepend(inputElement);
		}

		/**
		 * Remove URL by its temporary, ON screem ID
		 */
		function removeUrl(id) {
			$("#"+id).remove();
			$('input[name="'+urlInputName+'['+id+']"]').remove();
		}

		// Set current, loaded list of URLs
		currentList.forEach(function(item){
			addUrl(item);
		});
		
		// DOCUMENT READY? Set listeners!
		$(function(){

			// On ADD button click
			$('#add-tincan-url-button').on('click', function() {
				var url = $('#add-tincan-url').val().trim();
				if ( (!Docebo.isUrlValid(url)) || ($('input[value="'+url+'"]').length > 0) ) {
					return false;
				}
				addUrl(url);
				$('#add-tincan-url').val('');
			});

			// On delete icon click
			$('#tincan-urls-list').on('click', '.delete-url', function(){
				removeUrl($(this).closest('.iframe-source-row').attr('id'));
			});

			// On SEARCH
			$('#tincan-urls-settings').on('click', '.perform-search', function(){
				$('#tincan-urls-list .iframe-source-row').hide();
				var search = $('#tincan-urls-settings input[name="search_tincan_url"]').val();
				$('#tincan-urls-list .url-value:contains("'+search+'")').closest('.iframe-source-row').show();
			});

			// On clear search
			$('#tincan-urls-settings').on('click', '.clear-search', function(){
				$('#tincan-urls-settings input[name="search_tincan_url"]').val('');
				$('#tincan-urls-list .iframe-source-row').show();
			});

			// On SEARCH-keyup (search as you type)
			$('#tincan-urls-settings').on('keyup', 'input[name="search_tincan_url"]', function(){
				var search = $(this).val().trim();
				if (search == '') {
					$('#tincan-urls-list .iframe-source-row').show();
				}
				$('#tincan-urls-list .iframe-source-row').hide();
				$('#tincan-urls-list .url-value:contains("'+search+'")').closest('.iframe-source-row').show();
			});
			
			
		});
		
	})(jQuery);

</script>