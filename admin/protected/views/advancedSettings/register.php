<div class="form-wrapper">
	<?php $form = $this->beginWidget('CActiveForm', array('id' => 'settings-register-form')); ?>

	<div class="messages"><?php echo $form->errorSummary($model); ?></div>

	<div class="section">
		<h3><?php echo Yii::t('admin_directory', '_DIRECOTRY_SELFREGISTERED'); ?></h3>
		<div class="row odd">
			<div class="row">
				<div class="setting-name"><?php echo $form->labelEx($model, 'register_type'); ?></div>
				<div class="values">
					<?php echo $form->radioButtonList($model, 'register_type',
					array(
						'self' => Yii::t('configuration', '_REGISTER_TYPE_SELF'),
						'moderate' => Yii::t('configuration', '_REGISTER_TYPE_MODERATE'),
						'admin' => Yii::t('configuration', '_REGISTER_TYPE_ADMIN'),
					)); ?>
					<div class="checkbox-item">
						<br/>
						<?php echo $form->checkbox($model, 'disable_registration_email_confirmation', array('uncheckValue' => 'off', 'value' => 'on')); ?>
						<?php echo $form->labelEx($model, 'disable_registration_email_confirmation'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row even">
			<div class="row">
				<div class="setting-name"><?php echo $form->labelEx($model, 'allowed_domains'); ?>
					<p class="setting-description">
						<?= Yii::t('configuration', 'You can define which are the allowed domains that can self register in the LMS (e.g. @mycompany.com)') ?>
					</p>
				</div>
				<div class="values allowed_domains_container">
					<select multiple="multiple" id="allowed_domains" name="AdvancedSettingsRegisterForm[allowed_domains][]">
						<? if(count($model->allowed_domains)): ?>
							<? foreach($model->allowed_domains as $key=>$value): ?>
								<option selected="selected" class="selected" value="<?=$value?>"><?=$value?></option>
							<? endforeach; ?>
						<? endif; ?>
					</select>
				</div>
			</div>
		</div>
		<div class="row odd">
			<div class="row">
				<div class="setting-name"><?php echo $form->labelEx($model, 'mail_sender'); ?></div>
				<div class="values">
					<?php echo $form->textField($model, 'mail_sender'); ?>
				</div>
			</div>
		</div>
		<div class="row even">
			<div class="row">
				<div class="setting-name"><?php echo $form->labelEx($model, 'hour_request_limit'); ?></div>
				<div class="values min-input">
					<?php echo $form->numberField($model, 'hour_request_limit', array('min' => 1, 'max' => 720)); ?>
				</div>
			</div>
		</div>
		<div class="row odd">
			<div class="row">
				<div class="setting-name"><?php echo Yii::t('admin', 'Options'); ?></div>
				<div class="values">
					<div class="checkbox-item">
						<?php echo $form->checkbox($model, 'lastfirst_mandatory', array('uncheckValue' => 'off', 'value' => 'on')); ?>
						<?php echo $form->labelEx($model, 'lastfirst_mandatory'); ?>
					</div>
					<div class="checkbox-item">
						<?php echo $form->checkbox($model, 'privacy_policy', array('uncheckValue' => 'off', 'value' => 'on')); ?>
						<?php echo $form->labelEx($model, 'privacy_policy'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row even">
			<div class="row">
				<div class="setting-name"><?php echo $form->labelEx($model, 'registration_code_type'); ?></div>
				<div class="values">
					<?php echo $form->radioButtonList($model, 'registration_code_type',
					array(
						'0' => Yii::t('standard', '_NONE'),
						'tree_man' => Yii::t('configuration', '_ASK_FOR_MANUAL_TREE_CODE'),
						'tree_drop' => Yii::t('configuration', '_ASK_FOR_DROPDOWN_TREE_CODE'),
						'tree_course' => Yii::t('configuration', '_ASK_FOR_TREE_COURSE_CODE'),
					)); ?>
					<div class="checkbox-item">
					<br>
						<?php echo $form->checkbox($model, 'mandatory_code', array('uncheckValue' => 'off', 'value' => 'on')); ?>
						<?php echo $form->labelEx($model, 'mandatory_code'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="actions right-buttons">
			<?php echo CHtml::hiddenField('selectedTab', 'register'); ?>
			<?php echo CHtml::submitButton(Yii::t('admin', 'Save'), array('class' => 'btn-save')); ?>
		</div>
	</div>
	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
	$('input,select').styler();

	$('select#allowed_domains').fcbkcomplete({
		width: '98.5%',
		addontab: false, // <==buggy
		newel: true,
		cache: false,
		addonblurtext: true,
		input_name: 'maininput-name',
		filter_selected: true,
		maxitems: 9999,
		placeholder: '<?=Yii::t('standard', 'Type here...')?>',
	});

	/**
	 * Using jQuery blockUI plugin, block specific element
	 */
	var blockBlockable = function(elem) {
		elem.block({
			message: "",
			timeout: 0,
			overlayCSS:  {
				backgroundColor: 	'#FFF',
				opacity:         	0.6,
				cursor:          	'auto'
			}
		});
	};

	$('input[name*=register_type]:radio').on('change', function(){
		var disabled = $(this).val() == 'admin';
		if(disabled){
			blockBlockable($('div.allowed_domains_container').closest('div.row'));
		}else{
			$('div.allowed_domains_container').closest('div.row').unblock();
		}

	});

	$(function(){
		if($('input[name*=register_type]:checked').val() == 'admin'){
			blockBlockable($('div.allowed_domains_container').closest('div.row'))
		}
	})

</script>

<style>
	div.facebook-auto{
		display: none !important;
	}
</style>
