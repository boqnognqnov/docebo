<?php /* @var $model CoreHttps */ ?>
<?php /* @var $form CActiveForm */ ?>

<?php
	Yii::app()->clientScript->scriptMap['font-awesome.min.css'] = false;
	Yii::app()->clientScript->scriptMap['gamification.css'] = false;
	Yii::app()->clientScript->scriptMap['jquery.textPlaceholder.js'] = false;

	$removeCertificateUrl = Docebo::createAdminUrl('https/removeCertificate', array(
		'id'	=> $model->id,
	));

	$hasCsrPendingOrReady = $model->csrGenerationStatus;


	if ($hasCsrPendingOrReady) {
		$model->customCertificateOption = CoreHttps::CUSTOMCERT_NEEDS_CSR;
	}

?>

<div class="form-wrapper advanced-settings-https">
	<?php
		$params = array();
		$route = 'AdvancedSettings/https';

		$downloadCsrUrl = Docebo::createAdminUrl('https/download', array('type' => CoreHttps::FILETYPE_CSR, 'id' => $model->id));
		$downloadKeyUrl = Docebo::createAdminUrl('https/download', array('type' => CoreHttps::FILETYPE_KEY, 'id' => $model->id));

		$action = Docebo::createAdminUrl($route, $params);
		$form = $this->beginWidget('CActiveForm', array(
			'action'	=> $action,
			'id' 		=> 'settings-https-form',
		));

		echo $form->hiddenField($model, 'id');

	?>

	<div class="section">
		<h3>Https</h3>

		<div class="row odd">
			<div class="row">

				<div class="setting-name"><?php echo $form->labelEx($model, 'mode'); ?></div>
				<div class="values">
					<?php
						$params = array(
							CoreHttps::MODE_ON_DOCEBO_WILDCARD_SSL	 	=> Yii::t('configuration', 'Yes, on {domain}', array('{domain}' => Docebo::getOriginalDomain())),
						);
						if (PluginManager::isPluginActive('CustomdomainApp')) {
							$params[CoreHttps::MODE_ON_CUSTOM_SSL] =  Yii::t('configuration', 'Yes, on {domain}', array('{domain}' => trim(Docebo::getCurrentDomain(true), '/')));
						}
						echo $form->radioButtonList($model, 'mode', $params, array(
							'uncheckValue' => null,
						));
					?>
				</div>
			</div>
		</div>


		<div class="row even custom-certificate-data">

			<div class="row">

				<div class="setting-name"><label><?= Yii::t('player', 'Certificate') ?></label></div>

				<?php if (!$model->sslStatus || $model->sslStatus  == CoreHttps::SSL_STATUS_NOT_INSTALLED || $model->sslStatus == CoreHttps::SSL_STATUS_ERROR) : ?>
					<div class="values">

						<?php if ($model->sslStatus == CoreHttps::SSL_STATUS_ERROR) : ?>
							<div class="row-fluid">
								<div class="span12 ssl-pending-installation-error">
									<div class="alert alert-error">
										<?= $model->sslStatusLastError ?>
									</div>
								</div>
							</div>
							<br>
						<?php endif; ?>

						<div class="row-fluid">
							<div class="span12">
								<?= Yii::t('configuration', 'In order to activate Https protocol, <strong>you need an SSL certificate</strong>.') ?>
							</div>
						</div>

						<div class="row-fluid">
							<div class="span12">
								<?php
									echo $form->radioButton($model, 'customCertificateOption', array(
										'value'	=> 	CoreHttps::CUSTOMCERT_OWN_UPLOAD,
										'uncheckValue' => null,
										'id'	=> 'customCertificateOption_1'
									));
								?>
								<label for="customCertificateOption_1"><?= Yii::t('configuration', 'I have my certificate and I want to upload it') ?></label>
							</div>
						</div>
						<div class="row-fluid">

							<div class="span12 customer-has-ssl">

								<div class="customer-has-ssl-to-upload">

									<?php if (Yii::app()->user->hasFlash('error')) : ?>
										<div class="row-fluid">
											<div class="span12">
									   			<div class='alert alert-error text-left'>
								    				<?= Yii::app()->user->getFlash('error'); ?>
								    			</div>
								    		</div>
								    	</div>
									<?php endif; ?>

									<?php $model->scenario = CoreHttps::SCENARIO_UPLOAD_OWN_CERT; ?>

									<div class="row-fluid">
											<div class="span3">
												<?= $form->labelEx($model, 'certFile') ?>
											</div>
											<div class="span9">
												<div class="">
													<?php echo $form->fileField($model, 'certFile'); ?>
													<?php echo $form->error($model, 'certFile'); ?>
													<p class="setting-description">
														<?= ''// Description here ?>
													</p>
												</div>
											</div>
									</div>
									<br>

									<div class="row-fluid">
											<div class="span3">
												<?= $form->labelEx($model, 'keyFile') ?>
											</div>
											<div class="span9">
												<?php if ($model->keyFile && $model->keyFileOrigin == CoreHttps::KEYFILE_ORIGIN_CSRGEN): ?>
													<div class="keyfile-already-set">
														<label><strong><?= $model->keyFile ?></strong></label>
														<p class="setting-description">
															<?= Yii::t('configuration', 'We\'ve already uploaded this Key file as part of your CSR generation request. ') ?>
														</p>
													</div>
												<?php else: ?>
													<div class="keyfile-upload">
														<?php echo $form->fileField($model, 'keyFile'); ?>
														<?php echo $form->error($model, 'keyFile'); ?>
														<p class="setting-description">
															<?= '' //description here ?>
														</p>
													</div>
												<?php endif; ?>

											</div>
									</div>
									<br>

									<div class="row-fluid">
											<div class="span3">
												<?= $form->labelEx($model, 'intermCaFile') ?>
											</div>
											<div class="span9">
												<div class="">
													<?php echo $form->fileField($model, 'intermCaFile'); ?>
													<?php echo $form->error($model, 'intermCaFile'); ?>
													<p class="setting-description">
														<?= ''// Description here ?>
													</p>
												</div>
											</div>
									</div>
									<br>


								</div>

							</div>
						</div>

						<div class="row-fluid">
							<div class="span12">
								<?php
									echo $form->radioButton($model, 'customCertificateOption', array(
										'value'	=> 	CoreHttps::CUSTOMCERT_NEEDS_CSR,
										'uncheckValue' => null,
										'id'	=> 'customCertificateOption_2'
									));
								?>
								<label for="customCertificateOption_2"><?= Yii::t('configuration', 'I don\'t have any certificate and I need one') ?></label>
							</div>
						</div>

						<br>
						<div class="row-fluid">
							<div class="span12 customer-needs-csr">

								<?php if (Yii::app()->user->hasFlash('error_csr')) : ?>
									<div class="row-fluid">
										<div class="span12">
								   			<div class='alert alert-error text-left'>
							    				<?= Yii::app()->user->getFlash('error_csr'); ?>
							    			</div>
							    		</div>
							    	</div>
								<?php endif; ?>


								<?php if (!$model->csrGenerationStatus || $model->csrGenerationStatus == CoreHttps::CSRGEN_STATUS_ERROR) : ?>

									<div class="csr-form-data">
										<div class="row-fluid">
											<div class="span12">
												<?php
													$this->widget('common.widgets.InfoBox', array(
														'type' 		=> 'info',
														'text'		=> Yii::t('configuration','To activate Https you must buy an SSL Certificate from third party vendors. To do this, <strong>you must provide them a CSR file</strong> you can generate right here'),
													));
												?>
											</div>
										</div>
										<div class="row-fluid">
											<div class="span6">
												<?php $model->scenario = CoreHttps::SCENARIO_GENERATE_CSR; ?>
												<?= $form->labelEx($model, 'csrCountryName') ?>
												<?= $form->dropDownList($model, csrCountryName, CoreCountry::getCountryCountryList(), array(
													'class'	=> 'span12',
												)) ?>
												<div class="setting-description"><?= ''//Description here  ?></div>
											</div>
											<div class="span6">
												<?= $form->labelEx($model, 'csrOrgUnitName') ?>
												<?= $form->textField($model, 'csrOrgUnitName') ?>
												<div class="setting-description"><?= ''//Description here  ?></div>
											</div>
										</div>
										<div class="row-fluid">
											<div class="span6">
												<?= $form->labelEx($model, 'csrStateOrProvinceName') ?>
												<?= $form->textField($model, 'csrStateOrProvinceName') ?>
												<div class="setting-description"><?= ''//Description here  ?></div>
											</div>
											<div class="span6">
												<?= $form->labelEx($model, 'csrCommonName') ?>
												<?= $form->textField($model, 'csrCommonName') ?>
												<div class="setting-description"><?= ''//Description here  ?></div>
											</div>
										</div>
										<div class="row-fluid">
											<div class="span6">
												<?= $form->labelEx($model, 'csrLocalityName') ?>
												<?= $form->textField($model, 'csrLocalityName') ?>
												<div class="setting-description"><?= ''//Description here  ?></div>
											</div>
											<div class="span6">
												<?= $form->labelEx($model, 'csrEmail') ?>
												<?= $form->textField($model, 'csrEmail') ?>
												<div class="setting-description"><?= ''//Description here  ?></div>
											</div>
										</div>
										<div class="row-fluid">
											<div class="span6">
												<?= $form->labelEx($model, 'csrOrgName') ?>
												<?= $form->textField($model, 'csrOrgName') ?>
												<div class="setting-description"><?= ''//Description here  ?></div>
											</div>
											<div class="span6 text-right">
												<label>&nbsp;</label>
												<?php echo CHtml::submitButton(Yii::t('configuration', 'Generate CSR File'), array(
													'class' => 'btn-save',
													'name'	=> 'generate_csr_button',
												)); ?>
											</div>
										</div>
									</div>

								<?php elseif ($model->csrGenerationStatus == CoreHttps::CSRGEN_STATUS_PENDING_GENERATION || $model->csrGenerationStatus == CoreHttps::CSRGEN_STATUS_READY) : ?>

									<?php
										if ($model->csrGenerationStatus != CoreHttps::CSRGEN_STATUS_READY) {
											$style = "style='display: none;'";
										}
									?>
									<div class="customer-needs-csr-ready" <?= $style ?>>

										<div class="row-fluid">
											<div class="span12">
												<?php
													$this->widget('common.widgets.InfoBox', array(
														'type' 		=> 'info',
														'text'		=> Yii::t('configuration','If you have already bought your certificate, please select <strong>\'I have my certificate and I want to upload it\'</strong>'),
													));
												?>
											</div>
										</div>

										<br>
										<div class="row-fluid">


											<div class="span12 download-csr-generated-files">

												<div class="row-fluid">
													<?= Yii::t('configuration', 'You already generated a CSR file on <strong>{date}</strong>', array('{date}' => $model->csrGenerationTime)) ?>
												</div>
												<br>

												<div class="row-fluid">

													<div class="span4">
														<?= Chtml::link(Yii::t('configuration', 'Download CSR File'), $downloadCsrUrl, array(
															'class'	=> 'btn btn-save',
														)) ?>
													</div>
													<div class="span4">
														<?= Chtml::link(Yii::t('configuration', 'Download KEY File'), $downloadKeyUrl, array(
															'class'	=> 'btn btn-save',
														)) ?>
													</div>
													<div class="span4 text-right">
														<?= Chtml::submitButton(Yii::t('configuration', 'Generate New Request'), array(
															'class'	=> 'btn btn-cancel',
															'name'	=> 'generate_new_csr',
														)) ?>
														 <p class="setting-description text-left" style="color: red;">
														 	<?= Yii::t('configuration','Warning: previously generated Key and CSR files will be lost!') ?>
														 </p>
													</div>

												</div>

											</div>

										</div>



									</div>


									<?php if ($model->csrGenerationStatus == CoreHttps::CSRGEN_STATUS_PENDING_GENERATION): ?>
										<div class="customer-needs-csr-pending-generation">
											<div class="row-fluid csr-pending-message">
												<div class="span12">
													<label><span class="video-converting-animation"></span> <?= Yii::t('configuration', 'Your Certificate Request is being generated at the moment, please wait!') ?></label>
												</div>
											</div>
										</div>

										<div class="row-fluid">
											<div class="span12 csr-pending-generation-error" style="display: none;">
												<?= Yii::t('configuration', 'An error occured during status check') ?>
											</div>
										</div>
									<?php endif;?>

								<?php endif; ?>



							</div>
						</div>
					</div>


				<?php elseif (($model->sslStatus == CoreHttps::SSL_STATUS_PENDING_INSTALLATION) || ($model->sslStatus == CoreHttps::SSL_STATUS_INSTALLED)) : ?>

					<div class="values">

						<?php
							if ($model->sslStatus != CoreHttps::SSL_STATUS_INSTALLED) {
								$style = "style='display: none;'";
							}
						?>

						<div class="customer-has-ssl-successfully-uploaded" <?= $style ?>>
							<div class="row-fluid">
								<div class="span8">
									<div class="btn-save nouppercase"><i class="i-sprite is-check white"></i> <?= Yii::t('configuration', 'Your certificate has been uploaded correctly') ?></div>
								</div>
								<div class="span4 text-right">
									<a
										href="<?= $removeCertificateUrl ?>"
										class="open-dialog btn btn-cancel"
										data-dialog-class="medium"
										data-dialog-id="remove-certificate"
										data-dialog-title= "<?= Yii::t('standard', 'Remove SSL certificate') ?>"
									>
										<?= Yii::t('configuration', 'Remove certificate') ?>
									</a>
								</div>
							</div>
						</div>

						<?php if ($model->sslStatus == CoreHttps::SSL_STATUS_PENDING_INSTALLATION) :  ?>
							<div class="customer-has-ssl-pending-installation">
								<div class="row-fluid ssl-pending-message">
									<div class="span12">
										<label><span class="video-converting-animation"></span> <?= Yii::t('configuration', 'Your certificate is being installed at the moment, please wait!') ?></label>
									</div>
								</div>
							</div>

							<div class="row-fluid">
								<div class="span12 ssl-pending-installation-error" style="display: none;">
									<?= Yii::t('configuration', 'An error occured during SSL installation status check') ?>
								</div>
							</div>
						<?php endif;?>

					</div>
				<?php endif; ?>

			</div>
		</div>

		<div class="actions right-buttons">
			<?php echo CHtml::hiddenField('selectedTab', 'https'); ?>
			<?php echo CHtml::submitButton(Yii::t('admin', 'Save'), array(
				'class' => 'btn-save',
				'name'	=> 'save_https_settings_button',
			)); ?>
		</div>

	</div>
	<?php $this->endWidget(); ?>
</div>

<script type="text/javascript">

	var doPollSslInstallStatus 	= <?= json_encode($model->sslStatus == CoreHttps::SSL_STATUS_PENDING_INSTALLATION) ?>;
	var doPollCsrGenStatus 		= <?= json_encode($model->csrGenerationStatus == CoreHttps::CSRGEN_STATUS_PENDING_GENERATION) ?>;

	$(function(){
		var options = {
			sslStatusCheckUrl	: <?= json_encode(Docebo::createAdminUrl('https/checkSslInstallationStatus', array('id' => $model->id))) ?>,
			csrStatusCheckUrl	: <?= json_encode(Docebo::createAdminUrl('https/checkCsrStatus', array('id' => $model->id))) ?>,
			idHttps				: <?= json_encode($model->id) ?>
		};
		var AdvancedHttps = new AdvancedHttpsClass(options);

		$('.advanced-settings-https input').styler({browseText:Yii.t('organization', 'Upload File')});

		if(doPollSslInstallStatus) {
			AdvancedHttps.pollSslInstallStatus();
		}

		if (doPollCsrGenStatus) {
			AdvancedHttps.pollCsrGenStatus();
		}

	});


</script>
