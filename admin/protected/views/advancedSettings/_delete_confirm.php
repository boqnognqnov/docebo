<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'embed_url_delete_form',
		'htmlOptions' => array('class' => 'ajax')
	));	?>

	<p><?php echo Yii::t('standard', '_DEL').' '.Yii::t('standard', '_URL'); ?></p>

	<div class="clearfix">
		<?php echo CHtml::checkbox('confirm', false, array('id' => $checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('standard','Yes, I confirm I want to proceed'), $checkboxIdKey, array('style' => 'display: inline-block; padding-left: 10px;')); ?>

		<?php echo CHtml::hiddenField('removeUrl', $removeUrl); ?>
	</div>

	<div class="form-actions">
		<?= CHtml::button(Yii::t('standard', '_CONFIRM'), array('class' => 'btn btn-docebo green big btn-submit disabled')); ?>

		<?= CHtml::button(Yii::t('standard', '_CLOSE'), array('class' => 'btn btn-docebo black big close-dialog')); ?>
	</div>

	<?php $this->endWidget(); ?>
</div>

<script type="text/javascript">

	$('input').styler();

	function hideConfirmButton() {
		var confirmButton = $('.modal.embed_url_delete_form-dialog').find('.btn-submit');
		confirmButton.addClass('disabled');
		return true;
	}

	function showConfirmButton() {
		var confirmButton = $('.modal.embed_url_delete_form-dialog').find('.btn-submit');
		confirmButton.removeClass('disabled');
		return true;
	}

	$('.btn-submit').on('click', function() {
		if(!($('.btn-submit').hasClass('disabled'))) {
			//
			$( "#embed_url_delete_form" ).submit();
		}
	});

</script>