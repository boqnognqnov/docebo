<? /* @var $form CActiveForm */ ?>
<div class="form-wrapper">
	<?php $form = $this->beginWidget( 'CActiveForm', array( 'id' => 'email-settings-form' ) ); ?>

	<div class="messages"><?php echo $form->errorSummary( $model ); ?></div>

	<div class="section">
		<h3><?php echo Yii::t( 'standard', 'E-mail preferences' ); ?></h3>

		<div class="row odd">
			<div class="row">
				<div class="setting-name">
					<?php echo Yii::t( 'social_rating', 'Manage Admin E-Mails' ); ?>
					<p class="setting-description">
						<?php echo Yii::t( 'emails', 'Billing, product and technical e-mails are mandatory.<br/><br/>You can add more than one email, just separate them with a comma. E-mails can also belong to external LMS people.' ); ?>
					</p>
				</div>
				<div class="values">
					<div>
						<?=Yii::t('standard', 'Billing e-mail')?>*
					</div>
					<?php echo $form->textField( $model, 'billingEmails', array(
						'autocomplete'=>'off'
					)); ?>
					<p class="setting-description">
						<?=Yii::t('standard', 'Invoices and payments related messages will be sent to this e-mail')?>
					</p>

					<br/>
					<br/>

					<div>
						<?=Yii::t('standard', 'Product e-mail')?>*
					</div>
					<?php echo $form->textField( $model, 'productEmails', array(
						'autocomplete'=>'off'
					)); ?>
					<p class="setting-description">
						<?=Yii::t('standard', 'Product updates and important news will be sent to this e-mail address')?>
					</p>
				</div>
			</div>
		</div>


		<div class="actions right-buttons">
			<?php echo CHtml::hiddenField( 'selectedTab', 'emails' ); ?>
			<?php echo CHtml::submitButton( Yii::t( 'admin', 'Save' ), array( 'class' => 'btn-save' ) ); ?>
		</div>

	</div>
	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
	$(function(){
		$('input,select', '#email-settings-form').styler();

		// Prepare "typeahead" widgets
		$('#AdvancedSettingsEmailsForm_billingEmails').tokenInput('<?=Docebo::createAdminUrl('advancedSettings/emails')?>', {
			hintText: '<?=Yii::t('email','Start typing to find a LMS user or enter a custom email')?>',
			searchingText: '<?=Yii::t('standard', 'Searching')?>...',
			noResultsText: '<?=Yii::t('report', '_NULL_REPORT_RESULT')?>',
			resultsFormatter: function(item){
				return "<li>" + item.name + "</li>";
			},
			// Restore old data:
			prePopulate: <?=CJSON::encode($prepopulatedBillingEmailsTypeahead)?>
		});
		$('#AdvancedSettingsEmailsForm_productEmails').tokenInput('<?=Docebo::createAdminUrl('advancedSettings/emails')?>', {
			hintText: '<?=Yii::t('email','Start typing to find a LMS user or enter a custom email')?>',
			searchingText: '<?=Yii::t('standard', 'Searching')?>...',
			noResultsText: '<?=Yii::t('report', '_NULL_REPORT_RESULT')?>',
			resultsFormatter: function(item){
				return "<li>" + item.name + "</li>";
			},
			// Restore old data:
			prePopulate: <?=CJSON::encode($prepopulatedProductEmailsTypeahead)?>
		});
	})
</script>
