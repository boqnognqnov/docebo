<div class="form-wrapper">
	<div class="messages"><?php echo $message; ?></div>
</div>

<div class="form-actions">
	<?php echo CHtml::button(Yii::t('standard', '_CLOSE'), array('class'=> "btn-docebo black big close-dialog")); ?>
</div>
