<div class="form-wrapper">
	<?php $form = $this->beginWidget('CActiveForm', array('id' => 'settings-social-rating-form')); ?>

	<div class="messages"><?php echo $form->errorSummary($model); ?></div>

	<div class="section">
		<h3><?php echo Yii::t('social_rating', 'Social & Rating'); ?></h3>
		<div class="row odd">
			<div class="row">
				<div class="setting-name">
					<?php echo Yii::t('social_rating', 'Social sharing'); ?>
					<p class="setting-description"><?php echo Yii::t('social_rating', 'Enable users to share a course on the major social networks (this will be applied as default for all courses, you can specify a custom setting in the course advanced settings)');?></p>
				</div>
				<div class="values">
					<?php echo $form->radioButtonList($model, 'course_sharing_permission',
						array(
							AdvancedSettingsSocialRatingForm::COURSE_SHARE_DISABLED => Yii::t('social_rating', 'Disable courses social sharing'),
							AdvancedSettingsSocialRatingForm::COURSE_SHARE_IF_ENROLLED => Yii::t('social_rating', 'Users can share a course on social networks anytime, if enrolled'),
							AdvancedSettingsSocialRatingForm::COURSE_SHARE_IF_COMPLETED => Yii::t('social_rating', 'Users can share a course on social networks only if they completed the course'),
						)); ?>
					<div class="checkbox-item">
						<br/><br/>
						<?php echo $form->checkbox($model, 'course_user_share_permission', array('uncheckValue' => 'off', 'value' => 'on')); ?>
						<?php echo $form->labelEx($model, 'course_user_share_permission'); ?>
					</div>
					<br>
					<div class="row-fluid socialNetwork">
						<div class="span12 no-margin">
							<span><?php echo Yii::t('social_rating', 'Share on:');?></span>
						</div>
						<div class="no-margin span3">
							<?php echo $form->checkbox($model, 'course_share_facebook', array('uncheckValue' => 'off', 'value' => 'on')); ?>
							<?php echo $form->labelEx($model, 'course_share_facebook', array('class' => 'facebook-icon social-label')); ?>
						</div>
						<div class="no-margin span3">
							<?php echo $form->checkbox($model, 'course_share_twitter', array('uncheckValue' => 'off', 'value' => 'on')); ?>
							<?php echo $form->labelEx($model, 'course_share_twitter', array('class' => 'twitter-icon social-label')); ?>
						</div>
						<div class="no-margin span3">
							<?php echo $form->checkbox($model, 'course_share_linkedin', array('uncheckValue' => 'off', 'value' => 'on')); ?>
							<?php echo $form->labelEx($model, 'course_share_linkedin', array('class' => 'linkedin-icon social-label')); ?>
						</div>
						<div class="no-margin span3">
							<?php echo $form->checkbox($model, 'course_share_google', array('uncheckValue' => 'off', 'value' => 'on')); ?>
							<?php echo $form->labelEx($model, 'course_share_google', array('class' => 'google-icon social-label')); ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row even">
			<div class="setting-name">
				<?php echo Yii::t('social_rating', 'Rating'); ?>
				<p class="setting-description"><?php echo Yii::t('social_rating', 'Enable users to rate a course (this will be applied as default for all courses, you can specify a custom setting in the course advanced settings)'); ?></p>
			</div>
			<div class="values min-input">
				<?php echo $form->radioButtonList($model, 'course_rating_permission',
					array(
						AdvancedSettingsSocialRatingForm::COURSE_RATING_DISABLED => Yii::t('social_rating', 'Disable course rating'),
						AdvancedSettingsSocialRatingForm::COURSE_RATING_ENABLED => Yii::t('social_rating', 'Every user can rate a course, even if they\'re not enrolled'),
						AdvancedSettingsSocialRatingForm::COURSE_RATING_IF_ENROLLED => Yii::t('social_rating', 'Users can rate a course, only if enrolled'),
						AdvancedSettingsSocialRatingForm::COURSE_RATING_IF_COMPLETED => Yii::t('social_rating', 'Users can rate a course only if they completed the course'),
					)); ?>
			</div>
		</div>

		<div class="actions right-buttons">
			<?php echo CHtml::hiddenField('selectedTab', 'social-rating'); ?>
			<?php echo CHtml::submitButton(Yii::t('admin', 'Save'), array('class' => 'btn-save')); ?>
		</div>

	</div>
	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
	$('input,select').styler();
</script>
