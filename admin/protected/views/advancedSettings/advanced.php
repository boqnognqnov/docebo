<div class="form-wrapper">
	<?php $form = $this->beginWidget('CActiveForm', array('id' => 'settings-advanced-form')); ?>
		<div class="messages"><?php echo $form->errorSummary($model); ?></div>
		<div class="section">
			<h3><?php echo Yii::t('standard', '_ADVANCED'); ?></h3>

			<div class="row odd has-border">
                <div class="row">
				    <div class="setting-name"><?php echo $form->labelEx($model, 'send_cc_for_system_emails'); ?></div>
				    <div class="values">
					   <?php echo $form->textField($model, 'send_cc_for_system_emails'); ?>
    				</div>
				</div>
			</div>
			<div class="row even">
				<div class="row">
					<div class="setting-name"><?php echo $form->labelEx($model, 'sender_event'); ?>
                        <p class="setting-description">
                        	<?= Yii::t('configuration', 'Must be a valid e-mail') ?>
                        </p>
                    </div>
					<div class="values">
						<?php echo $form->emailField($model, 'sender_event'); ?>
					</div>
				</div>
			</div>
            <div class="row odd">
				<div class="row">
					<div class="setting-name"><?php echo $form->labelEx($model, 'return_path'); ?>
                        <p class="setting-description">
                        	<?= Yii::t('configuration', 'Also called "bounce address", where all bounce messages are sent') ?>
                        </p>
                    </div>
					<div class="values">
						<?php echo $form->emailField($model, 'return_path', array(
                            'class'=>'return_path',
                        ));
                        ?>
					</div>
				</div>
			</div>
			<div class="row even has-border">
			    <div class="row">
    				<div class="setting-name">
							<?php echo $form->labelEx($model, 'ttlSession'); ?>
							<p class="setting-description">
								<?= Yii::t('configuration', 'The user session will be terminated after this time. Valid time is between 15 minutes and 3 hours (in seconds).') ?>
							</p>
						</div>
	   		        <div class="values middle-input">
		  			     <?php echo $form->textField($model, 'ttlSession'); ?>
									<?= '&nbsp;('.strtolower(Yii::t('standard', '_SECONDS')).')' ?>
				    </div>
				</div>
			</div>
			<div class="row odd">
				<div class="row">
					<div class="setting-name"><?php echo $form->labelEx($model, 'ua_mobile'); ?></div>
					<div class="values max-input">
						<?php echo $form->textField($model, 'ua_mobile'); ?>
					</div>
				</div>
			</div>
            <div class="row even has-border elements-per-page">
                <div class="row">
				    <div class="setting-name"><?php echo $form->labelEx($model, 'elements_per_page'); ?></div>
				    <div class="values">
					   <div class="select-row">
						  <?php echo $form->listBox($model, 'elements_per_page', array(
							'10' => 10,
							'15' => 15,
							'20' => 20,
							'30' => 30,
							'50' => 50,
							'100' => 100,
						  ), array('size' => 1)); ?>
					   </div>
    				</div>
                </div>
			</div>
			<div class="row odd">
				<div class="row">
					<div class="setting-name"><?=Yii::t('storage', '_LONAME_scormorg')?></div>
					<div class="values">
						<div class="checkbox-item">
							<?php echo $form->checkbox($model, 'scorm_debug', array('uncheckValue' => 'off', 'value' => 'on')); ?>
							<?php echo $form->labelEx($model, 'scorm_debug'); ?>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row even">
				<div class="row">
					<div class="setting-name"><?=Yii::t('storage', 'XAPI/TinCan')?></div>
					<div class="values">
						<div class="checkbox-item">
							<?php echo $form->checkbox($model, 'extended_tincan_launch_params', array('uncheckValue' => 'off', 'value' => 'on')); ?>
							<?php echo $form->labelEx($model, 'extended_tincan_launch_params'); ?>
						</div>
					</div>
				</div>
			</div>
			
			
		</div>

		<div class="section">
			<h4><?php echo Yii::t('standard', '_NEWSLETTER');?></h4>
			<div class="row even has-border">
				<div class="row">
					<div class="setting-name"><?php echo $form->labelEx($model, 'nl_sendpercycle'); ?></div>
					<div class="values middle-input">
						<?php echo $form->textField($model, 'nl_sendpercycle'); ?>
					</div>
				</div>
			</div>
			<div class="row odd has-border  ">
                <div class="row">
                    <div class="setting-name">
                        <?php echo $form->labelEx($model, 'nl_sendpause'); ?>
                    </div>
                    <div class="values middle-input">
                        <?php echo $form->textField($model, 'nl_sendpause'); ?>
                    </div>
                </div>
			</div>
		</div>

		<div class="section">
			<h4><?php echo Yii::t('configuration', '_SECURITY'); ?></h4>
			<div class="row even has-border">
				<div class="row">
					<div class="setting-name"><?php echo Yii::t('admin', 'Options'); ?></div>
					<div class="values">
						<div class="checkbox-item">
							<?php echo $form->checkbox($model, 'session_ip_control', array('uncheckValue' => 'off', 'value' => 'on')); ?>
							<?php echo $form->labelEx($model, 'session_ip_control'); ?>
						</div>
						<div class="checkbox-item">
							<?php echo $form->checkbox($model, 'stop_concurrent_user', array('uncheckValue' => 'off', 'value' => 'on')); ?>
							<?php echo $form->labelEx($model, 'stop_concurrent_user'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php if(PluginManager::isPluginActive('SimpleSamlApp') && Yii::app()->user->getRelativeUsername(Yii::app()->user->username) === 'staff.docebo') : ?>
			<div class="section">
				<h4><?php echo Yii::t('simplesaml', 'Simple SAML loggly'); ?></h4>
				<div class="row odd has-border">
					<div class="row">
						<div class="setting-name"><?php echo Yii::t('configuration', '_DO_DEBUG'); ?></div>
						<div class="values">
							<div class="checkbox-item">
								<?php echo $form->checkbox($model, 'simplesaml_logging', array('uncheckValue' => 'off', 'value' => 'on')); ?>
								<?php echo $form->labelEx($model, 'simplesaml_logging'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<div class="actions right-buttons">
			<?php echo CHtml::hiddenField('selectedTab', 'advanced'); ?>
			<?php echo CHtml::submitButton(Yii::t('admin', 'Save'), array('class' => 'btn-docebo btn-save')); ?>
		</div>
	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
	$('input,select').styler();
</script>