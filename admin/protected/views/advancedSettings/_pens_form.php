<?php
$_uniqueId = substr(md5(uniqid(rand(), true)), 0, 10);
$_isEditing = (!$model->isNewRecord);

if (!$_isEditing) {
	$_generatedAccess = PensApplication::generateUniqueKey();
	$model->app_id = $_generatedAccess['app_id'];
	$model->user_id = $_generatedAccess['user_id'];
	$model->password = PensApplication::generatePassword();
}
?>

<div class="row-fluid">
	<?php
	/* @var $form CActiveForm */
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'pens_application_form',
		'htmlOptions' => array(
			'class' => 'docebo-form',
			'data-unique-id' => $_uniqueId
		),
	));

	if ($_isEditing) {
		echo CHtml::hiddenField('id', $model->getPrimaryKey(), array('id' => 'input-id-' . $_uniqueId));
	}
	echo CHtml::hiddenField('confirm', 1, array('id' => 'input-confirm-'.$_uniqueId));
	?>

	<div class="clearfix">
		<?php
		echo $form->labelEx($model, 'name');
		echo $form->textField($model, 'name', array('class' => 'input-block-level'));
		echo $form->error($model, 'name');
		?>
	</div>
	<br/>

	<div class="clearfix">
		<?php
		echo $form->labelEx($model, 'description');
		echo $form->textArea($model, 'description', array('class' => 'input-block-level'));
		echo $form->error($model, 'description');
		?>
	</div>
	<br/>

	<div class="clearfix">
		<?php
		echo $form->labelEx($model, 'category_id', array('class' => 'hide-required'));
		echo $form->dropDownList($model, 'category_id', LearningCourseCategory::getCategoriesList(), array('class' => 'input-block-level', 'encode' => false/*, 'prompt' => ''*/));
		echo $form->error($model, 'category_id');
		?>
	</div>
	<br/>

	<div class="clearfix">
		<?php
		echo $form->labelEx($model, 'app_id', array('class' => 'hide-required'));
		echo $form->textField($model, 'app_id', array('class' => 'input-block-level', 'readonly' => true, 'maxlength' => 8));
		echo $form->error($model, 'app_id');
		?>
	</div>
	<br/>

	<div class="clearfix">
		<?php
		echo $form->labelEx($model, 'user_id', array('class' => 'hide-required'));
		echo $form->textField($model, 'user_id', array('class' => 'input-block-level', 'readonly' => true, 'maxlength' => 8));
		echo $form->error($model, 'user_id');
		?>
	</div>
	<br/>

	<div class="clearfix">
		<?php
		echo $form->labelEx($model, 'password', array('class' => 'hide-required'));
		echo $form->textField($model, 'password', array('class' => 'input-block-level', 'readonly' => true));
		echo $form->error($model, 'password');
		?>
	</div>

	<?php $this->endWidget(); ?>
</div>
