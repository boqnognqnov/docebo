<?php
/* @var $model PensApplication */
$_uniqueId = substr(md5(uniqid(rand(), true)), 0, 10);
?>
<div class="row-fluid">

	<br/>
	<div class="clearfix">
		<?php
		echo CHtml::checkBox('confirm', false, array('id' => 'pens-delete-confirm-toggle-'.$_uniqueId, 'class' => 'pens-delete-application-confirm-checkbox'));
		echo '&nbsp;&nbsp;&nbsp;';
		echo CHtml::tag('label', array('for' => 'pens-delete-confirm-toggle-'.$_uniqueId));
		echo Yii::t('player', 'Are you sure you want to delete:');
		echo '&nbsp;';
		echo CHtml::tag('strong', array(), $model->name, true);
		echo '&nbsp;?';
		echo CHtml::closeTag('label');
		?>
	</div>

</div>
<script type="text/javascript">
	$(function() {

		function checkSaveButtonStatus() {
			var button = $('.modal.pens-delete-application-dialog').find('.btn.confirm-save');
			if (button.length > 0) {
				var checkbox = $('.pens-delete-application-confirm-checkbox');
				if (checkbox.length > 0) {
					var isChecked = checkbox.prop('checked');
					if (isChecked) {
						button.prop('disabled', false);
						button.removeClass('disabled');
					} else {
						button.prop('disabled', true);
						button.addClass('disabled');
					}
				}
			}
		}

		//initialize button
		checkSaveButtonStatus();

		//assign checkbox event handler
		$('.pens-delete-application-confirm-checkbox').on("change", function() {
			checkSaveButtonStatus();
		});

		$('.modal input[type="checkbox"]').styler();
	});
</script>