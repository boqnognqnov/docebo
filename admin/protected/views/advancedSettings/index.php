<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('menu', '_CONFIGURATION'),
); ?>

<?php $sidebarLinks = array(
	'register' => array(
		'class' => 'register',
		'title' => '<span></span>' . Yii::t('admin_directory', '_DIRECOTRY_SELFREGISTERED'),
		'data-tab' => 'register',
		'data-url' => 'advancedSettings/register',
	),
	'user' => array(
		'class' => 'user',
		'title' => '<span></span>' . Yii::t('standard', '_USERS'),
		'data-tab' => 'user',
		'data-url' => 'advancedSettings/user',
	),
	'password' => array(
		'class' => 'password',
		'title' => '<span></span>' . Yii::t('standard', '_PASSWORD'),
		'data-tab' => 'password',
		'data-url' => 'advancedSettings/passwordPolicy',
	),
	'e-learning' => array(
		'class' => 'e-learning',
		'title' => '<span></span>' . Yii::t('configuration', '_CONF_LMS'),
		'data-tab' => 'e-learning',
		'data-url' => 'advancedSettings/eLearning',
	),
	'pens' => array(
		'class' => 'pens',
		'title' => '<i class="fa fa-exchange"></i>' . Yii::t('pens', 'PENS'),
		'data-tab' => 'pens',
		'data-url' => 'advancedSettings/pens',
		'content' => $this->renderPartial('pens', array(), true)
	),
	'advanced' => array(
		'class' => 'advanced',
		'title' => '<span></span>' . Yii::t('standard', '_ADVANCED'),
		'data-tab' => 'advanced',
		'data-url' => 'advancedSettings/advanced',
	),
	'datetime' => array(
		'class' => 'datetime',
		'title' => '<span></span>' . Yii::t('standard', 'Date & Time'),
		'data-tab' => 'datetime',
		'data-url' => 'advancedSettings/datetime',
	),
	'social-rating' => array(
		'class' => 'social-rating',
		'title' => '<span></span>' . Yii::t('social_rating', 'Social & Rating'),
		'data-tab' => 'social-rating',
		'data-url' => 'advancedSettings/socialRating',
	),
	'emails' => array(
		'class' => 'emails',
		'title' => '<span class="i-sprite is-env-closed"></span> ' . Yii::t('standard', 'E-mail preferences'),
		'data-tab' => 'emails',
		'data-url' => 'advancedSettings/emails',
	),
);

if(PluginManager::isPluginActive('HttpsApp')) {
	$sidebarLinks['https'] = array(
		'class' => 'https',
		'title' => '<span></span> ' . 'Https',
		'data-tab' => 'https',
		'data-url' => 'advancedSettings/https',
	);
}
?>
<div class="admin-advanced-wrapper">
	<div id="sidebar" class="advanced-sidebar pull-left">
		<h3><?php echo Yii::t('admin', 'Main functions'); ?></h3>
		<ul>
			<?php
			//print all side menu items
			foreach ($sidebarLinks as $link) {
				$title = $link['title'];
				unset($link['title']);
				echo '<li>' . CHtml::link($title, '#'. $link['data-tab'], $link + array('data-loaded' => (isset($link['content']) ? 1 : 0))) . '</li>';
			}
			?>
		</ul>
	</div>
	<div id="main" class="advanced-main">
		<div class="content tab-content">
			<?php
			// Here is where loaded tabs are placed. Some tabs may need to be pre-loaded now.
			foreach ($sidebarLinks as $link) {
				if (isset($link['content'])) {
					echo '<div id="'.$link['data-tab'].'">';
					echo $link['content'];
					echo '</div>';
				}
			}
			?>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<?php echo CHtml::hiddenField('selectedTab', '', array('id' => 'selectedTab')); ?>
<script type="text/javascript">
	$(function(){

		//apply styler and controls to pre-loaded tabs
		$('.tab-content input, .tab-content select').styler();
		$(document).controls();

		//set tab loading handler
		$(window).hashchange(function(){
			var hash = location.hash,
				hasStr = "",
				link = null;

			hashStr = hash.substr(1);
			link = $('#sidebar a.' + hashStr);

			$('#sidebar a').removeClass('active');
			link.addClass('active');

			if (link.data('loaded') == 0) {
				loadTabContent(link);
			}

			$('.tab-content > div').hide();
			$('.tab-content').find(hash).show();
			$('#selectedTab').val(hashStr);
		});

		if (location.hash == "") {
			console.log(location);
			location.hash = '#' + $('#sidebar a:first').data('tab');
			location.reload();
		}

		$(window).hashchange();
	});
</script>