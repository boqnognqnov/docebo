<?php
/* @var $form CActiveForm */
/* @var $model AdvancedSettingsForm */
?>
<div class="form-wrapper">
    <?php $form = $this->beginWidget('CActiveForm', array('id' => 'settings-datetime-form')); ?>

    <div class="messages"><?php echo $form->errorSummary($model); ?></div>

    <div class="section">
        <h3><?= Yii::t('configuration', 'Date & time') ?></h3>
        <div class="row odd">
            <div class="row">
                <div class="setting-name"><?php echo $form->labelEx($model, 'timezone_default'); ?></div>
                <div class="values">
                    <p for=""><?= Yii::t('configuration', 'Use this default:') ?></p>
                    <div class="select-row">
                        <?php echo $form->listBox($model, 'timezone_default', Yii::app()->localtime->getTimezonesArray(), array('size'=>1, 'prompt'=>Yii::t('configuration', 'Select a time zone...'))); ?>
                    </div>
                    <br/>
                    <div class="checkbox-item">
                        <?php echo $form->checkbox($model, 'timezone_allow_user_override', array('uncheckValue' => 'off', 'value' => 'on')); ?>
                        <?php echo $form->labelEx($model, 'timezone_allow_user_override'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row even">
            <div class="setting-name"><?php echo $form->labelEx($model, 'date_format'); ?></div>
            <div class="values date-format-options">
                <?= CHtml::activeLabel($model, 'date_format', array(
                    'label' => $form->radioButton($model, 'date_format', array('value'=>'browser_code', 'uncheckValue'=>null, 'id'=>'date_format_radio_1')) . ' ' . Yii::t('configuration', 'Use selected language') . CHtml::tag('span', array('class'=>'help-block'), Yii::t('configuration', 'The date format will be set up according to your browser language')),
                    'for' => 'date_format_radio_1'
                )) ?>
                <?= CHtml::activeLabel($model, 'date_format', array(
                    'label' => $form->radioButton($model, 'date_format', array('value'=>'user_selected', 'uncheckValue'=>null, 'id'=>'date_format_radio_2')) . ' ' . Yii::t('configuration', 'User selected') . CHtml::tag('span', array('class'=>'help-block'), Yii::t('configuration', 'Each user can chose the date format from his own profile page')),
                    'for' => 'date_format_radio_2'
                )) ?>
                <?= CHtml::activeLabel($model, 'date_format', array(
                    'label' => $form->radioButton($model, 'date_format', array('value'=>'custom', 'uncheckValue'=>null, 'id'=>'date_format_radio_3', 'checked'=>(!in_array($model->date_format,array('browser_code', 'user_selected'))))) . ' ' . Yii::t('configuration', 'Force date format'),
                    'for' => 'date_format_radio_3'
                )) ?>
                <div class="select-row">
                    <?= CHtml::tag('p', array('class'=>'help-block','style'=>'margin:0 0 0 30px;'), Yii::t('standard', '_LANGUAGE')) ?>
                    <?= $form->listBox($model, 'date_format_custom_lang', CHtml::listData(Lang::getLanguages(), 'browsercode', function($lang) { return CHtml::encode($lang['description']); }), array('size'=>1)) ?>

                    <?= CHtml::tag('p', array('class'=>'help-block','style'=>'margin:0 0 0 30px;'), Yii::t('configuration', 'The date format will be forced as follows:')) ?>
                    <?= $form->listBox($model, 'date_format_custom', array(), array('size'=>1, 'prompt'=>Yii::t('configuration', 'Select a date format...'))); ?>
                </div>
            </div>
        </div>
        <div class="actions right-buttons">
            <?php echo CHtml::hiddenField('selectedTab', 'datetime'); ?>
            <?php echo CHtml::submitButton(Yii::t('admin', 'Save'), array('class' => 'btn-save')); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
$(document).on("tab.loaded", function() {
        $('input,select').styler();

        $('.date-format-options').find(':radio').change(function() {
            var $parent = $(this).closest('.values');
            var disabled = ($(this).attr('id') != 'date_format_radio_3');
            $parent.find('select').attr('disabled', disabled);
        }).filter(':checked').trigger('change');

        $('#<?= CHtml::activeId($model, 'date_format_custom_lang') ?>').change(function() {
            var url = '<?= $this->createUrl('getCustomDateFormats') ?>';
            $.get(url, {code: $(this).val()}, function(response) {
                if (response.success === true) {
                    var html = response.html.replace(/\u200f/g, '');
                    $('#<?= CHtml::activeId($model, 'date_format_custom') ?>').replaceWith(html);
                }
            }, 'json');
        }).trigger('change');
});
</script>
