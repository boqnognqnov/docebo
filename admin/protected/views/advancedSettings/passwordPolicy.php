<div class="form-wrapper">
	<?php $form = $this->beginWidget('CActiveForm', array('id' => 'settings-password-form')); ?>

	<div class="messages"><?php echo $form->errorSummary($model); ?></div>

	<div class="section password-policy">
		<h3><?php echo Yii::t('admin', 'Password policy'); ?></h3>
		<div class="row odd">
			<div class="row">
				<div class="setting-name"><?php echo Yii::t('admin', 'Options'); ?></div>
				<div class="values">
					<div class="checkbox-item">
						<?php echo $form->checkbox($model, 'pass_alfanumeric', array('uncheckValue' => 'off', 'value' => 'on')); ?>
						<?php echo $form->labelEx($model, 'pass_alfanumeric'); ?>
					</div>
					<!--
					<div class="checkbox-item">
						<?php echo $form->checkbox($model, 'pass_special_chars', array('uncheckValue' => 'off', 'value' => 'on')); ?>
						<?php echo $form->labelEx($model, 'pass_special_chars'); ?>
					</div>
					-->
					<div class="checkbox-item">
						<?php echo $form->checkbox($model, 'pass_not_username', array('uncheckValue' => 'off', 'value' => 'on')); ?>
						<?php echo $form->labelEx($model, 'pass_not_username'); ?>
					</div>
					<div class="checkbox-item">
						<?php echo $form->checkbox($model, 'pass_change_first_login', array('uncheckValue' => 'off', 'value' => 'on')); ?>
						<?php echo $form->labelEx($model, 'pass_change_first_login'); ?>
					</div>
                    <div class="checkbox-item">
                        <?php echo $form->checkbox($model, 'pass_dictionary_check', array('uncheckValue' => 'off', 'value' => 'on')); ?>
                        <?php echo $form->labelEx($model, 'pass_dictionary_check'); ?>
                        <span class="help-block">
                            <?=Yii::t('admin', 'Check is performed using a collection of common passwords and English words')?>
                        </span>
                    </div>
				</div>
			</div>
		</div>

		<div class="row even">
			<div class="setting-name"><?php echo $form->labelEx($model, 'pass_min_char'); ?></div>
			<div class="values">
				<?php echo $form->textfield($model, 'pass_min_char'); ?>
			</div>
		</div>

		<div class="row odd">
			<div class="row">
				<div class="setting-name"><?php echo $form->labelEx($model, 'pass_max_time_valid'); ?></div>
				<div class="values">
					<?php echo $form->textfield($model, 'pass_max_time_valid'); ?>
				</div>
			</div>
		</div>

		<div class="row even">
			<div class="setting-name"><?php echo $form->labelEx($model, 'user_pwd_history_length'); ?></div>
			<div class="values">
				<?php echo $form->textfield($model, 'user_pwd_history_length'); ?>
			</div>
		</div>

		<div class="actions right-buttons">
			<?php echo CHtml::hiddenField('selectedTab', 'password'); ?>
			<?php echo CHtml::submitButton(Yii::t('admin', 'Save'), array('class' => 'btn-save')); ?>
		</div>
		</div>
	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
	$('input,select').styler();
</script>
