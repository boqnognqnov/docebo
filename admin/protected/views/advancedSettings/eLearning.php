<?php $data = array(
	'' => Yii::t('standard', '_NONE'),
	'status' => Yii::t('standard', '_STATUS'),
	'name' => Yii::t('standard', '_NAME'),
	'code' => Yii::t('standard', '_CODE'),
);
?>
<div class="form-wrapper" style="min-height: 400px;">
	<?php $form = $this->beginWidget('CActiveForm', array('id' => 'settings-elearning-form')); ?>

	<div class="messages"><?php echo $form->errorSummary($model); ?></div>

	<div class="section">
		<h3>E-Learning</h3>

		<div class="row">
			<div class="row">
				<div class="setting-name"><?php echo Yii::t('admin', 'Options'); ?></div>
				<div class="values">
					<div class="checkbox-item">
						<?php echo $form->checkbox($model, 'no_answer_in_test', array('uncheckValue' => 'off', 'value' => 'on')); ?>
						<?php echo $form->labelEx($model, 'no_answer_in_test'); ?>
					</div>
					<div class="checkbox-item">
						<?php echo $form->checkbox($model, 'no_answer_in_poll', array('uncheckValue' => 'off', 'value' => 'on')); ?>
						<?php echo $form->labelEx($model, 'no_answer_in_poll'); ?>
					</div>
					<?php /*<div class="checkbox-item">
						<?php echo $form->checkbox($model, 'tracking', array('uncheckValue' => 'off', 'value' => 'on')); ?>
						<?php echo $form->labelEx($model, 'tracking'); ?>
					</div>*/ ?>
					<div class="checkbox-item">
						<?php echo $form->checkbox($model, 'test_force_multichoice', array('uncheckValue' => 'off', 'value' => 'on')); ?>
						<?php echo $form->labelEx($model, 'test_force_multichoice'); ?>
					</div>
					<div class="checkbox-item">
						<?php echo $form->checkbox($model, 'certificate_real_size', array('uncheckValue' => 'off', 'value' => 'on')); ?>
						<?php echo $form->labelEx($model, 'certificate_real_size'); ?>
					</div>

					<div class="checkbox-item">
						<?php echo $form->checkbox($model, 'show_course_after_enrollment', array('uncheckValue' => 'off', 'value' => 'on')); ?>
						<?php echo $form->labelEx($model, 'show_course_after_enrollment'); ?>
					</div>

					<div class="checkbox-item">
						<?php echo $form->checkbox($model, 'course_resume_autoplay', array('uncheckValue' => 'off', 'value' => 'on')); ?>
						<?php echo $form->labelEx($model, 'course_resume_autoplay'); ?>
					</div>
					<?php
					// Event raised for YnY app to hook on to
					Yii::app()->event->raise('RenderCustomElearningAdvancedSettings', new DEvent($this, array('form' => $form)));
					?>
				</div>
			</div>
		</div>
        <div id="soft-deadline-option-wrapper" class="row">
            <div class="row">
                <div class="setting-name"><?php echo Yii::t('admin', 'Soft Deadline'); ?></div>
                <div class="values">
                    <div class="checkbox-item">
                        <?php echo $form->checkbox($model, 'soft_deadline', array('uncheckValue' => 'off', 'value' => 'on')); ?>
                        <?php echo $form->labelEx($model, 'soft_deadline'); ?>
                    </div>
                </div>
            </div>
        </div>
		<?php if($model->canShowUseDashlets()):?>
		<div class="row">
			<div class="row">
				<div class="setting-name"><?php echo Yii::t('dashboard', '_DASHBOARD'); ?></div>
				<div class="values">
					<div class="checkbox-item">
						<?php echo $form->checkbox($model, 'use_dashlets', array('uncheckValue' => 'off', 'value' => 'on')); ?>
						<?php echo $form->labelEx($model, 'use_dashlets'); ?>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>

		<div class="row">
			<div id="whitelist" class="row">
				<div class="setting-name"><?php echo Yii::t('admin', 'Iframe whitelist'); ?>
					<div class="elearning-iframes-whitelist-subtitle"><?php echo Yii::t('admin', 'This whitelist defines URLs that can be used while inserting an <a href="http://www.w3schools.com/tags/tag_iframe.asp" target="_blank">IFRAME</a> in a HTML type learning object, in order to prevent security attacks.'); ?></div>
				</div>
				<div class="values">
					<div class="row-fluid">
						<div class="span8">
							<?php echo CHtml::textField('add_iframe_sourse', '', array('placeholder' => 'http://',
							     'style' => 'width: 70%;', 'id' => 'add_iframe_sourse')); ?> <?= CHtml::button(Yii::t('standard', '_ADD'),
							     array('class' => 'btn btn-submit', 'style' => 'min-width: 60px; margin-left: 10px;',
								'name' => 'add_iframe_sourse_btn', 'id' => 'add_iframe_sourse_btn')); ?>
							<div class="iframe-source-error"><span class="text-error"></span><span class="text-success"></span></div>
						</div>
						<div class="span4 search-input-wrapper">
							<?php echo CHtml::textField('search_iframe_sourse',$filter,array('placeholder' =>'Search','style'=>'width: 90%','id'=>'search_iframe_sourse')); ?>
							<button type="button" class="close clear-search">&times;</button>
							<span id="perform-search" class="perform-search search-icon"></span>
						</div>
					</div>

					<div class="row-fluid iframe-source-row">
						<div class="span12">
							<strong><?=Yii::t('admin', 'ALLOWED URLs'); ?></strong>
						</div>
					</div>
					<div id="iframe-sources" class="checkbox-item">
					<?php
					if(count($whitelist) > 0) {
						foreach($whitelist as $whitelistItem) {?>
							<div class="row-fluid iframe-source-row">
								<div class="span11"><?=$whitelistItem; ?></div>
								<div class="span1"><i class="fa fa-times fa-lg text-colored-red delete-url"></i></div>
							</div>
					<?php }}?>
					</div>
					<div class="pagination row-fluid">
					   <!-- the input fields that will hold the variables we will use -->
						<input type='hidden' id='current_page' />
						<input type='hidden' id='show_per_page' value="<?php echo $elements_per_page; ?>"/>
						<div  class="span3">Count: <strong id="item_counter"><?php echo count($whitelist); ?></strong></div>
						<div id='page_navigation' class="span8"></div>
					</div>

					<script type="text/javascript">

					$(document).ready(function(){
						paginationInit();
					});

					function paginationInit(){
						//how much items per page to show
						var show_per_page = parseInt($('#show_per_page').val());
						//getting the amount of elements inside content div
						var number_of_items = $('#iframe-sources').children().size();
						//calculate the number of pages we are going to have
						var number_of_pages = Math.ceil(number_of_items/show_per_page);

						//set the value of our hidden input fields
						$('#current_page').val(0);
						$('#show_per_page').val(show_per_page);

						//now when we got all we need for the navigation let's make it '

						/*
						 what are we going to have in the navigation?
						 - link to previous page
						 - links to specific pages
						 - link to next page
						 */
						var navigation_html = '<a class="first_link" href="javascript:go_to_page(0);">First</a>';
						var current_link = 0;
						var page_links = 0
						while(number_of_pages > current_link){
							if(page_links < 8) {
								navigation_html += '<a class="page_link" href="javascript:go_to_page(' + current_link + ')" longdesc="' + current_link + '">' + (current_link + 1) + '</a>';
							}
							page_links ++;
							current_link++;
						}
						navigation_html += '<a class="last_link" href="javascript:go_to_page('+ (number_of_pages - 1) +')">Last</a>';
						navigation_html += '<a class="previous" href="javascript:previous();">Prev</a>';
						navigation_html += '<a class="next" href="javascript:next();">Next</a>';

						$('#page_navigation').html(navigation_html);

						//add active_page class to the first page link
						$('#page_navigation .page_link:first').addClass('active_page');

						//hide all the elements inside content div
						$('#iframe-sources').children().css('display', 'none');

						//and show the first n (show_per_page) elements
						$('#iframe-sources').children().slice(0, show_per_page).css('display', 'block');


						if(show_per_page >= number_of_items ){
							$('#page_navigation').css('display', 'none');
						} else {
							$('#page_navigation').css('display', 'block');
						}
					}

					function previous(){

						new_page = parseInt($('#current_page').val()) - 1;
						//if there is an item before the current active link run the function
						if(new_page > -1){
							go_to_page(new_page);
						}

					}

					function next(){
						new_page = parseInt($('#current_page').val()) + 1;
						//if there is an item after the current active link run the function
						if($('.active_page').next('.page_link').length==true){
							go_to_page(new_page);
						}

					}
					function go_to_page(page_num){
						//get the number of items shown per page
						var show_per_page = parseInt($('#show_per_page').val());
						//getting the amount of elements inside content div
						var number_of_items = $('#iframe-sources').children().size();
						//calculate the number of pages we are going to have
						var number_of_pages = Math.ceil(number_of_items/show_per_page);

						//get the element number where to start the slice from
						start_from = page_num * show_per_page;

						//get the element number where to end the slice
						end_on = start_from + show_per_page;

						//hide all children elements of content div, get specific items and show them
						$('#iframe-sources').children().css('display', 'none').slice(start_from, end_on).css('display', 'block');

						/*get the page link that has longdesc attribute of the current page and add active_page class to it
						 and remove that class from previously active page link*/
						$('.page_link[longdesc=' + page_num +']').addClass('active_page').siblings('.active_page').removeClass('active_page');

						//update the current page input field
						$('#current_page').val(page_num);

						if(number_of_pages > 8) {
							var current_link = parseInt($('#current_page').val());
							if((number_of_pages - current_link) < 8) {
								var pager = number_of_pages - 8;
								var navigation_html = '<a class="first_link" href="javascript:go_to_page(0);">First</a>';
								while (number_of_pages > pager) {
									if(pager == current_link) {
										navigation_html += '<a class="page_link active_page" href="javascript:go_to_page(' + pager + ')" longdesc="' + pager + '">' + (pager + 1) + '</a>';
									} else {
										navigation_html += '<a class="page_link" href="javascript:go_to_page(' + pager + ')" longdesc="' + pager + '">' + (pager + 1) + '</a>';
									}
									pager++;
								}
								navigation_html += '<a class="last_link" href="javascript:go_to_page(' + (number_of_pages - 1) + ')">Last</a>';
								navigation_html += '<a class="previous" href="javascript:previous();">Prev</a>';
								navigation_html += '<a class="next" href="javascript:next();">Next</a>';

								$('#page_navigation').html(navigation_html);

							} else {
								var navigation_html = '<a class="first_link" href="javascript:go_to_page(0);">First</a>';
								var page_links = 0;
								while (number_of_pages > current_link) {
									if(page_links < 8) {
										navigation_html += '<a class="page_link" href="javascript:go_to_page(' + current_link + ')" longdesc="' + current_link + '">' + (current_link + 1) + '</a>';
									}
									page_links++;
									current_link++;
								}
								navigation_html += '<a class="last_link" href="javascript:go_to_page(' + (number_of_pages - 1) + ')">Last</a>';
								navigation_html += '<a class="previous" href="javascript:previous();">Prev</a>';
								navigation_html += '<a class="next" href="javascript:next();">Next</a>';

								$('#page_navigation').html(navigation_html);
								//add active_page class to the first page link
								$('#page_navigation .page_link:first').addClass('active_page');
							}
						}
					}



					/*<![CDATA[*/
					(function ($) {
						$('input,select').styler();

						function parseIframeUrl(url){
							var anchorUrl  = $('<a></a>'); // We are creating a DOM Element of type anchor so we can use its properties to get clean domain URL instead of using regex, parsers, etc..
							var currentUrl = url;
							var domainUrl  = url;
							var isValidUrl = true;

							// Here we are removing the last trailing slash so we will get this type of url [http://example.com] instead of this [http://example.com/]
							// This also will prevent duplicated domains like [http://example.com/] and [http://example.com]
							if(currentUrl.substr(-1) === '/') {
								currentUrl = currentUrl.substr(0, currentUrl.length - 1);
							}

							// If Url contains http or https
							if (currentUrl.indexOf("http://") == 0 || currentUrl.indexOf("https://") == 0) {

								anchorUrl.attr('href', currentUrl);

								// Here we can check if the users is provided full url or just a domain
								// For Example: http://example.com/ or http://example.com will be valid urls and pathname always will be "/"
								// If the url look like this http://example.com/some-title/id/5 pathname will be "/some-title/id/5" and the url will be not valid
								if(anchorUrl[0].pathname != '/'){
									isValidUrl = false;
								}

								if($.type(anchorUrl[0].origin) != 'undefined' || anchorUrl[0].origin != null){
									domainUrl = anchorUrl[0].origin;
								}else{
									domainUrl = anchorUrl[0].protocol + '//' + anchorUrl[0].hostname;
								}
							}

							return {
								isValidUrl: isValidUrl,
								newUrl: domainUrl,
								oldUrl: url
							}

						}

						// Do add URL here
						$('#add_iframe_sourse_btn').on('click', function() {
							if($.trim($('#add_iframe_sourse').val()) != '') {
								var addSource = $('#add_iframe_sourse').val();
								var domainUrl = parseIframeUrl(addSource);

								var sendData = {
									'add_iframe_sourse': domainUrl.newUrl
								};


								var jqxhr = $.ajax( {dataType: "json", url: "<?=Yii::app()->createUrl('advancedSettings/axAddIframeUrl') ?>", data: sendData, method: "POST"})
									.done(function(data) {
										if(data.success) { // if "success"

											$('#iframe-sources').prepend('<div class="row-fluid iframe-source-row"><div class="span11">' + domainUrl.newUrl + '</div><div class="span1"><i class="fa fa-times fa-lg text-colored-red delete-url"></i></div></div>');
											$('#item_counter').text(data.counter);
											addDeleteUrlListeners();
											paginationInit();

											$('#add_iframe_sourse').val('');

											if(domainUrl.isValidUrl == false){
												$('.iframe-source-error .text-success').html(Yii.t('admin', 'The URL you entered was trimmed so that it will include only the domain name.'));
												$('.iframe-source-error').fadeIn(1000);
												$('.iframe-source-error').fadeOut(5000, function() {
													$('.iframe-source-error .text-success').html('');
												});
											}else{
												$('.iframe-source-error .text-success').html(Yii.t('standard', 'Successfully added'));
												$('.iframe-source-error').fadeIn(1000);
												$('.iframe-source-error').fadeOut(5000, function() {
													$('.iframe-source-error .text-success').html('');
												});
											}


										} else { // Error !!!
											var message = Yii.t('standard', '_OPERATION_FAILURE');
											if (data.error != '') {
												message = data.error;
											}

											$('.iframe-source-error .text-error').html(message);
											$('.iframe-source-error').fadeIn(1000);
											$('.iframe-source-error').fadeOut(5000, function() {
												$('.iframe-source-error .text-error').html('');
											});
										}


									})
									.fail(function(data) {
										if (data.error != '') {
											$('.iframe-source-error .text-error').html(data.error);
											$('.iframe-source-error').fadeIn(1000);
											$('.iframe-source-error').fadeOut(5000, function() {
												$('.iframe-source-error .text-error').html('');
											});
										} else {
											$('.iframe-source-error .text-error').html(Yii.t('standard', '_OPERATION_FAILURE'));
											$('.iframe-source-error').fadeIn(1000);
											$('.iframe-source-error').fadeOut(5000, function() {
												$('.iframe-source-error .text-error').html('');
											});
										}
									})
									.always(function(data) {
										// NOTE: add here what to do always
									});
							}
						});

						$('#perform-search').on('click',function(){
							var hash = location.hash,
								hasStr = "",
								link = null;

							hashStr = hash.substr(1);
							link = $('#sidebar a.' + hashStr);
							var filter = $('#search_iframe_sourse').val();

							if(!filter){
								var data = {filter_clear:true};
							} else {
								var data = {filter:filter};
							}
							$.ajax({
								url: yii.urls.base + '/index.php?r=' + link.data('url'),
								cache: false,
								async: false,
								timeout: 4000,
								data:data,
								error: function(){
									return true;
								},
								success: function (result) {
									$('#whitelist').empty();
									$('#whitelist').html(result.html);

									// Suck Dialog2 controls
									$(document).controls();
								}
							});
						});

						$('.clear-search').on('click',function(){
							var hash = location.hash,
								hasStr = "",
								link = null;

							hashStr = hash.substr(1);
							link = $('#sidebar a.' + hashStr);
							$('#search_iframe_sourse').val('');

							$.ajax({
								url: yii.urls.base + '/index.php?r=' + link.data('url'),
								cache: false,
								async: false,
								timeout: 4000,
								data:{filter_clear:true},
								error: function(){
									return true;
								},
								success: function (result) {
									$('#whitelist').empty();
									$('#whitelist').html(result.html);

									// Suck Dialog2 controls
									$(document).controls();
								}
							});
						});

						$('#search_iframe_sourse').keypress(function(e) {
							if (e.which == 13) return false;
						});


						function addDeleteUrlListeners() {
							// Do the delete URL here
							$('.delete-url').off();
							$('.delete-url').on('click', function () {
								var deleteUrlRow = $(this).closest('.iframe-source-row');
								var deleteUrlContainer = $(deleteUrlRow).find('.span11');
								var deleteUrl = deleteUrlContainer.html();

								addData = 'removeUrl=' + deleteUrl;

								openDialog(false, // Event
									"<?= Yii::t('standard', '_DEL') . ' ' . Yii::t('standard', '_URL'); ?>", // Title
									"<?=Yii::app()->createUrl('advancedSettings/axDeleteIframeUrl') ?>", // Url
									'embed_url_delete_form-dialog', // Dialog-Id
									'embed_url_delete_form-dialog', // Dialog-Class
									'POST', // Ajax Method
									{// Ajax Data
										removeUrl: deleteUrl
									});

								$(document).controls();

								$(document).on("dialog2.closed", ".embed_url_delete_form-dialog", function () {
									// remove the URL from the list
									if ($(this).find("a.auto-close.success").length > 0) {
										$(deleteUrlRow).remove();
										$('#item_counter').text(parseInt($('#item_counter').text())-1);
										paginationInit();
									}
								});

							});
						}

						addDeleteUrlListeners();

					})(jQuery);

					/*]]>*/
					</script>
				</div>
			</div>
		</div>


		<div class="row">
			<?php $this->renderPartial("_tincan_urls", array(
                'model' => $model,
			), false, true);?>
		</div>

		<div class="actions right-buttons">
			<?php echo CHtml::hiddenField('selectedTab', 'e-learning'); ?>
			<?php echo CHtml::submitButton(Yii::t('admin', 'Save'), array('class' => 'btn-save')); ?>
		</div>

	</div>
	<?php $this->endWidget(); ?>
</div>



<script type="text/javascript">
	$('.advanced-main #e-learning .section > .row').each(function(index){
		index++;
		$(this).addClass((index % 2 == 1) ? "odd" : "even");
	});
</script>