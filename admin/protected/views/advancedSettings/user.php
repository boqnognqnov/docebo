<div class="form-wrapper">
	<?php $form = $this->beginWidget('CActiveForm', array('id' => 'settings-user-form')); ?>

	<div class="messages"><?php echo $form->errorSummary($model); ?></div>

	<div class="section">
		<h3><?php echo Yii::t('standard', '_USER'); ?></h3>
		<div class="row odd">
			<div class="row">
				<div class="setting-name"><?php echo Yii::t('admin', 'Options'); ?></div>
				<div class="values">
					<div class="checkbox-item">
						<?php echo $form->checkbox($model, 'enable_email_verification', array('uncheckValue' => 'off', 'value' => 'on')); ?>
						<?php echo $form->labelEx($model, 'enable_email_verification'); ?>
					</div>
					<div class="checkbox-item">
						<?php echo $form->checkbox($model, 'profile_only_pwd', array('uncheckValue' => 'off', 'value' => 'on')); ?>
						<?php echo $form->labelEx($model, 'profile_only_pwd'); ?>
					</div>
					<div class="checkbox-item">
						<?php echo $form->checkbox($model, 'request_mandatory_fields_compilation', array('uncheckValue' => 'off', 'value' => 'on')); ?>
						<?php echo $form->labelEx($model, 'request_mandatory_fields_compilation'); ?>
					</div>
					<div class="checkbox-item">
						<?php echo $form->checkbox($model, 'use_node_fields_visibility', array('uncheckValue' => 'off', 'value' => 'on')); ?>
						<?php echo $form->labelEx($model, 'use_node_fields_visibility'); ?>
						<?php echo CHtml::tag('span', array('class'=>'help-block'), Yii::t('configuration', 'Don\'t forget to explicitly configure the visibility of your additional fields, in Admin -> Users Management')); ?>
					</div>
                    <div class="checkbox-item">
                        <?php echo $form->checkbox($model, 'show_first_name_first', array('uncheckValue' => 'off', 'value' => 'on')); ?>
                        <?php echo $form->labelEx($model, 'show_first_name_first'); ?>
                    </div>
				</div>
			</div>
		</div>

		<div class="row even">
			<div class="setting-name"><?php echo $form->labelEx($model, 'max_log_attempt'); ?></div>
			<div class="values min-input">
				<?php echo $form->textfield($model, 'max_log_attempt'); ?>
			</div>
		</div>

		<div class="row odd">
			<div class="row">
				<div class="setting-name"><?php echo $form->labelEx($model, 'save_log_attempt'); ?></div>
				<div class="values">
					<?php echo $form->radioButtonList($model, 'save_log_attempt',
					array(
						'all' => Yii::t('configuration', '_SAVE_LA_ALL'),
						'after_max' => Yii::t('configuration', '_SAVE_LA_AFTER_MAX'),
						'no' => Yii::t('standard', '_NO'),
					)); ?>
				</div>
			</div>
		</div>

		<div class="row even">
			<div class="row">
				<div class="setting-name"><?=Yii::t('configuration', 'Redirect user on logout')?></div>
				<div class="values redirectUserOnLogout">
					<div class="checkbox-item">
						<?php echo $form->checkbox($model, 'user_logout_redirect', array('id' => 'user_logout_redirect', 'uncheckValue' => 'off', 'value' => 'on')); ?>
						<?php echo $form->labelEx($model, 'user_logout_redirect'); ?>
						<?php echo CHtml::tag('span', array('class'=>'help-block'), Yii::t('configuration', 'When turning on, the changes will take effect from the next login')); ?>
					</div>
					<div class="select-row redirectUserOnLogoutUrl">
						<?php echo $form->labelEx($model, 'user_logout_redirect_url'); ?>
						<?php echo $form->textField($model, 'user_logout_redirect_url', array('id' => 'user_logout_redirect_url')); ?>
					</div>
				</div>
			</div>
		</div>

		<div class="actions right-buttons">
			<?php echo CHtml::hiddenField('selectedTab', 'user'); ?>
			<?php echo CHtml::submitButton(Yii::t('admin', 'Save'), array('class' => 'btn-save')); ?>
		</div>

	</div>
	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
	$('input,select').styler();

	//Handle the use SSO checkbox
	var checkbox = $('#user_logout_redirect');
	if(checkbox.is(':checked'))
		$('#user_logout_redirect_url').prop('disabled',false);
	else
		$('#user_logout_redirect_url').prop('disabled',true);

	$('#user_logout_redirect').change(function(){
		var checkbox = $(this);
		if(checkbox.is(':checked'))
			$('#user_logout_redirect_url').prop('disabled',false);
		else
			$('#user_logout_redirect_url').prop('disabled',true);
	});
</script>
