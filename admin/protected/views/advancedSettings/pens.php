<div class="pens-wrapper">
	<?php //$form = $this->beginWidget('CActiveForm', array('id' => 'settings-pens-form')); ?>

	<h3><?= Yii::t('app', 'PENS Credentials'); ?></h3>
	<!--<hr />-->
	<div class="pens-url-container">
		<span class="url-label"><?= Yii::t('pens', 'Target System URL'); ?>:</span>
		&nbsp;&nbsp;&nbsp;
		<?php
		$_lmsUrl = Settings::get('url', '');
		if (!empty($_lmsUrl)) {
			if (substr($_lmsUrl, -1) != '/') { $_lmsUrl .= '/'; }
			$_lmsUrl .= 'pens';
		}
		echo $_lmsUrl;
		?>
	</div>
	<!--<hr />-->
	<div class="main-actions clearfix">
		<ul class="clearfix" data-bootstro-id="bootstroNewCourse">
			<li>
				<div>
					<?= CHtml::link(
						'<span class="clear-background"><i class="fa fa-plus-circle fa-3x"></i></span>' . Yii::t('pens', 'Add application'),
						'javascript:;',
						array(
							'id' => "pens-add-application-action",
							'class' => "pens-add-application-action"
						)
					); ?>
				</div>
			</li>
		</ul>
	</div>


	<div class="controls">
		<h3 class="pens-gridview-title">
			<?= Yii::t('pens', 'Applications'); ?>
		</h3>

		<?php

		$filter = new stdClass();
		$filter->search_input = Yii::app()->request->getParam('search_input', '');

		$columns = array(
			array(
				'name' => 'name',
				'type' => 'raw',
				'header' => Yii::t('standard', '_NAME'),
				'value' => '$data["name"]',
			),
			array(
				'name' => 'description',
				'type' => 'raw',
				'header' => Yii::t('standard', '_DESCRIPTION'),
				'value' => function($data, $index) {
					if (strlen($data['description']) > 40) {
						return CHtml::tag('span', array('title' => $data['description']), substr($data['description'], 0, 37).'...');
					} else {
						return $data['description'];
					}
				},
			),
			array(
				'name' => 'app_id',
				'type' => 'raw',
				'header' => Yii::t('pens', 'Application ID'),
				'value' => '$data["app_id"]',
			),
			array(
				'name' => 'user_id',
				'type' => 'raw',
				'header' => Yii::t('pens', 'User ID'),
				'value' => function($data, $index) {
					$output = '<div class="hidden-value-container hide-value">';
					for ($i=0; $i<8; $i++) { $output .= '<i class="fa fa-circle"></i>'; }
					$output .= '<span class="value">'.$data['user_id'].'</span>';
					$output .= '</div>';
					$output .= '<div class="hidden-value-toggler">';
					$output .= '<i class="fa fa-eye"></i>';
					$output .= '</div>';
					return $output;
				},
			),
			array(
				'name' => 'password',
				'type'	=> 'raw',
				'header' => Yii::t('standard', '_PASSWORD'),
				'value'	=> function($data, $index) {
					$output = '<div class="hidden-value-container hide-value">';
					for ($i=0; $i<8; $i++) { $output .= '<i class="fa fa-circle"></i>'; }
					$output .= '<span class="value">'.$data['password'].'</span>';
					$output .= '</div>';
					$output .= '<div class="hidden-value-toggler">';
					$output .= '<i class="fa fa-eye"></i>';
					$output .= '</div>';
					return $output;
				},
			),
			/*array(
				'type'	=> 'raw',
				'value'	=> function($data, $index) {
					return CHtml::link(Yii::t('pens', 'Show Logs'), 'javascript:;', array('class' => 'toggle-show'));
				},
				'header' => Yii::t('pens', 'Logs'),
			),*/
			array(
				'name' => 'enable',
				'type' => 'raw',
				'value' => function($data, $index) {
					$colorClass = ($data['enable'] > 0 ? 'green' : 'dark-gray');
					return CHtml::tag(
						'span',
						array('title' => Yii::t('pens', 'Enable/disable')),
						'<i class="fa fa-check-circle-o '.$colorClass.' action-icon toggle-enable" data-id="'.$data['id'].'"></i>',
						true
					);
				},
				'header' => ''
			),
			array(
				'type' => 'raw',
				'value' => function($data, $index) {
					if ($data['not_editable'] == 1) return '&nbsp;';
					return CHtml::link('', 'javascript:;', array('class' => 'edit-action', 'data-id' => $data['id'], 'title' => Yii::t('standard', '_MOD')));
				},
				'header' => '',
				'htmlOptions' => array(
					'class' => 'button-column-single'
				)
			),
			array(
				'type' => 'raw',
				'value' => function($data, $index) {
					if ($data['not_editable'] == 1) return '&nbsp;';
					return CHtml::link('', 'javascript:;', array('class' => 'delete-action', 'data-id' => $data['id'], 'title' => Yii::t('standard', '_DEL')));
				},
				'header' => '',
				'htmlOptions' => array(
					'class' => 'button-column-single'
				)
			)
		);

		$this->widget('common.widgets.ComboGridView', array(
			'gridId' => 'pens-applications-list',
			'dataProvider' => PensApplication::dataProvider($searchInput),
			'columns' => $columns,
			'disableMassSelection' => true,
			'disableMassActions' => true,
			'autocompleteRoute' => 'axPensApplicationsAutocomplete',
			'hiddenFields' => array(),
			'gridAjaxUrl' => Docebo::createAdminUrl('advancedSettings/pens')
		));

		?>
		<div class="clearfix"></div>
	</div>

	<?php //$this->endWidget(); ?>
</div>

<script type="text/javascript">
	//<![CDATA[

	//generic functions

	function getDialogLoadingSpinnerHtml() {
		return '<div class="spinner-container"><i class="fa fa-spin fa-spinner"></i></div>';
	}


	function updateGrid() {
		// Prepare the form data to be sent with gridView update
		var options = {
			data: 'content_format=html&' + $('#combo-grid-search-input-pens-applications-list').serialize()
		};
		// Do the yiiGridView update here
		$.fn.yiiGridView.update("pens-applications-list", options);
	}


	function pensOpenEditDialog(id) {
		//prepare variables
		var isEditing = (id && id > 0 ? true : false)
		var _url = (isEditing
			? '<?= Docebo::createAdminUrl('advancedSettings/axPensEditApplication') ?>'
			: '<?= Docebo::createAdminUrl('advancedSettings/axPensAddApplication') ?>');
		var _title = (isEditing
			? <?= CJSON::encode(Yii::t('pens', 'Edit application')) ?>
			: <?= CJSON::encode(Yii::t('pens', 'Add application')) ?>);
		var _id = (isEditing
			? 'pens-edit-application-dialog'
			: 'pens-add-application-dialog');
		var _class = (isEditing
			? 'pens-edit-application-dialog'
			: 'pens-add-application-dialog');
		//request content and fill the dialog
		$('<div/>').dialog2({
			id: _id,
			autoOpen: true,
			title: _title,
			modalClass: _class,
			showCloseHandle: true,
			removeOnClose: true,
			closeOnEscape: true,
			closeOnOverlayClick: true
		});
		var body = $('.modal.'+_class).find('.modal-body');
		var footer = $('.modal.'+_class).find('.modal-footer');
		body.html(getDialogLoadingSpinnerHtml());
		$.ajax({
			url: _url,
			type: 'post',
			dataType: 'json',
			data: (isEditing ? {id: id} : {})
		}).success(function(o) {
			if (o.success) {
				body.html(o.html);
				footer.html(<?php
						echo CJSON::encode(CHtml::button(Yii::t('standard', '_CONFIRM'), array(
							'class' => 'btn confirm-save btn-docebo green big',
							'onclick' => '(function() { pensSubmitEditDialog([[id]]); })()'
						))
						.CHtml::button(Yii::t('standard', '_CANCEL'), array(
							'class' => 'btn btn-docebo black big close-dialog',
							'onclick' => '(function() { $(\'a.close\').trigger(\'click\'); })()'
						)));
					?>.replace('[[id]]', (typeof id === 'undefined' ? '' : id+'')));
			} else {
				var message = (o.message ? o.message : <?= CJSON::encode(Yii::t('standard', '_OPERATION_FAILURE')) ?>);
				body.html(message);
			}
		}).fail(function() {
			body.html(<?= CJSON::encode(Yii::t('standard', '_OPERATION_FAILURE')) ?>);
		});
	}

	function pensSubmitEditDialog(id) {
		//prepare variables
		var isEditing = (id && id > 0 ? true : false);
		var _url = (isEditing
			? '<?= Docebo::createAdminUrl('advancedSettings/axPensEditApplication') ?>'
			: '<?= Docebo::createAdminUrl('advancedSettings/axPensAddApplication') ?>');
		var _class = (isEditing
			? 'pens-edit-application-dialog'
			: 'pens-add-application-dialog');
		//do effective submit request and handle response
		var body = $('.modal.'+_class).find('.modal-body');
		var footer = $('.modal.'+_class).find('.modal-footer');
		if (body.length > 0) { //make sure that the dialog exists
			var form = $('.modal.'+_class).find('.modal-body').find('form.docebo-form');
			if (form.length > 0) { //make sure that the form exists
				$.ajax({
					url: _url,
					method: 'post',
					dataType: 'json',
					data: form.serialize(),
					beforeSend: function() {
						body.html(getDialogLoadingSpinnerHtml());
						var btn = footer.find('.btn.confirm-save');
						if (btn.length > 0) {
							btn.prop('disabled', true);
							btn.addClass('disabled');
						}
					}
				}).success(function (o) {
					if (o.success) {
						if (typeof o.html !== 'undefined') {
							//data saving was not successful, but we were able to catch the errors and re-send the form content to the client
							body.html(o.html);
							var btn = footer.find('.btn.confirm-save');
							if (btn.length > 0) {
								btn.prop('disabled', false);
								btn.removeClass('disabled');
							}
						}
						if (typeof o.id !== 'undefined') {
							$('a.close').trigger('click'); //data has been successfully saved, so close the dialog
							updateGrid();
						}
					} else {
						var message = (o.message ? o.message : <?= CJSON::encode(Yii::t('standard', '_OPERATION_FAILURE')) ?>);
						body.html(message);
					}
				}).fail(function () {
					body.html(<?= CJSON::encode(Yii::t('standard', '_OPERATION_FAILURE')) ?>);
				});
			}
		}
	}


	function pensOpenDeleteDialog(id) {
		var _class = 'pens-delete-application-dialog';
		$('<div/>').dialog2({
			id: 'pens-delete-application-dialog',
			autoOpen: true,
			title: <?= CJSON::encode(Yii::t('pens', 'Delete application')) ?>,
			modalClass: _class,
			showCloseHandle: true,
			removeOnClose: true,
			closeOnEscape: true,
			closeOnOverlayClick: true
		});
		var body = $('.modal.'+_class).find('.modal-body');
		var footer = $('.modal.'+_class).find('.modal-footer');
		body.html(getDialogLoadingSpinnerHtml());
		$.ajax({
			url: '<?= Docebo::createAdminUrl('advancedSettings/axPensDeleteApplication') ?>',
			type: 'post',
			dataType: 'json',
			data: {id: id}
		}).success(function(o) {
			if (o.success) {
				footer.html(<?php
						echo CJSON::encode(CHtml::button(Yii::t('standard', '_CONFIRM'), array(
							'class' => 'btn confirm-save btn-docebo green big',
							'onclick' => '(function() { pensSubmitDeleteDialog([[id]]); })()'
						))
						.CHtml::button(Yii::t('standard', '_CANCEL'), array(
							'class' => 'btn btn-docebo black big close-dialog',
							'onclick' => '(function() { $(\'a.close\').trigger(\'click\'); })()'
						)));
				?>.replace('[[id]]', id+''));
				body.html(o.html);
			} else {
				var message = (o.message ? o.message : <?= CJSON::encode(Yii::t('standard', '_OPERATION_FAILURE')) ?>);
				body.html(message);
			}
		}).fail(function() {
			body.html(<?= CJSON::encode(Yii::t('standard', '_OPERATION_FAILURE')) ?>);
		});
	}

	function pensSubmitDeleteDialog(id) {
		var _class = 'pens-delete-application-dialog';
		//do effective submit request and handle response
		var body = $('.modal.'+_class).find('.modal-body');
		var footer = $('.modal.'+_class).find('.modal-footer');
		if (body.length > 0) { //make sure that the dialog exists
			$.ajax({
				url: '<?= Docebo::createAdminUrl('advancedSettings/axPensDeleteApplication') ?>',
				method: 'post',
				dataType: 'json',
				data: {
					id: id,
					confirm: 1
				},
				beforeSend: function() {
					body.html(getDialogLoadingSpinnerHtml());
					var btn = footer.find('.btn.confirm-save');
					if (btn.length > 0) {
						btn.prop('disabled', true);
						btn.addClass('disabled');
					}
				}
			}).success(function (o) {
				if (o.success) {
					$('a.close').trigger('click'); //data has been successfully saved, so close the dialog
					updateGrid();
				} else {
					var message = (o.message ? o.message : <?= CJSON::encode(Yii::t('standard', '_OPERATION_FAILURE')) ?>);
					body.html(message);
				}
			}).fail(function () {
				body.html(<?= CJSON::encode(Yii::t('standard', '_OPERATION_FAILURE')) ?>);
			});
		}
	}


	//page initialization (events, etc.)
	$(function () {

		//=== Add application dialog ===
		$('#pens-add-application-action').on('click', function(e) {
			e.preventDefault();
			e.stopPropagation();
			pensOpenEditDialog();
		});
		//===

		//=== Edit application dialog ===
		$('#combo-grid-view-container-pens-applications-list').on('click', 'a.edit-action', function(e) {
			e.preventDefault();
			e.stopPropagation();
			pensOpenEditDialog($(this).data('id'));
		});
		//===

		//=== Delete application dialog ===
		$('#combo-grid-view-container-pens-applications-list').on('click', 'a.delete-action', function(e) {
			e.preventDefault();
			e.stopPropagation();
			pensOpenDeleteDialog($(this).data('id'));
		});
		//===

		// Enable/disable applications
		$('#combo-grid-view-container-pens-applications-list').on('click', '.toggle-enable', function(event) {

			var $this = $(this), previousClass = false;
			if ($this.hasClass('fa-spin')) return;

			$.ajax({
				url: '<?= Docebo::createAdminUrl('advancedSettings/axPensToggleEnable') ?>',
				type: 'post',
				dataType: 'json',
				data: {
					id: $this.data('id'),
					new_value: ($this.hasClass('green') ? 0 : 1)
				},
				beforeSend: function() {
					previousClass = $this.attr('class');
					$this.removeClass('fa-check-circle-o green dark-gray');
					$this.addClass('fa-spin fa-spinner black');
				}
			}).success(function(o) {
				if (o.success) {
					if (o.new_value > 0) {
						$this.addClass('fa-check-circle-o green');
					} else {
						$this.addClass('fa-check-circle-o dark-gray');
					}
				}
			}).fail(function() {
				$this.attr('class', previousClass);
			}).always(function() {
				$this.removeClass('fa-spin fa-spinner black');
				previousClass = false;
			});

		});

		//grid password/IDs hide/show
		$('#combo-grid-view-container-pens-applications-list').on('click', '.fa.fa-eye, .fa.fa-eye-slash', function() {
			var t = $(this);
			var h = t.parent().parent().find('.hidden-value-container');
			if (t.hasClass('fa-eye')) {
				if (h.length > 0) {
					h.removeClass('hide-value');
					h.addClass('show-value');
					t.removeClass('fa-eye');
					t.addClass('fa-eye-slash');
				}
			} else if (t.hasClass('fa-eye-slash')) {
				if (h.length > 0) {
					h.removeClass('show-value');
					h.addClass('hide-value');
					t.removeClass('fa-eye-slash');
					t.addClass('fa-eye');
				}
			}
		});
	});
	//]]>
</script>