<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'user_form',
    )); ?>


    <?= Yii::t('organization_chart', "Branch \"{orgChartName}\" is not empty.", ['{orgChartName}' => $nodeName]) ?>
    
    <?php

    ?>

    <?php $this->endWidget(); ?>
</div>


<div id="confirm-tmp" style="display: none;">
    <a data-callback="cancel-testCallback" href="javascript:void(0);"
       class="btn btn-cancel"><?= Yii::t('standard', 'OK') ?></a>
</div>

<script>
    $(document).ready(function () {
        $('.modal-footer').empty();
        $('.modal-footer').append($('#confirm-tmp').html());

        $(".btn-cancel").on("click", function () {
            $('.btn-close').trigger("click");
        });
    });
</script>

