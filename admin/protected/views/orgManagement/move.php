<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'orgChart_move',
		'htmlOptions' => array(
			'class' => 'orgChart_move'
		),
	)); ?>

	<?php echo $form->hiddenField($model, 'idOrg', array('id' => 'currentNodeId')); ?>

	<div class="chart-tree">

		<?php if (!empty($fullOrgChartTree)) { ?>
			<ul class='node-tree'>
				<?php $level = 1; ?>
				<?php foreach($fullOrgChartTree as $node) {
					if (in_array($node->idOrg, $allChildrenNodesIds)) {
						continue;
					}
					$left = 10*$node->lev;
					$rootNode = '';
					$hasChildren = '';
					if (!$node->isLeaf()) {
						$hasChildren = 'hasChildren';
					}
					if ($node->isRoot()) {
						$rootNode = 'rootNode';
					}
					?>
					<?php if ($node->lev < $level) { ?>
						<?php for($i = $node->lev; $i < $level; $i++) {
							echo '</ul>';
						} ?>
					<?php } ?>

				<li class="node nodeTree-node <?php echo $hasChildren . ' ' . $rootNode; ?>">
					<div class="nodeTree-border">
						<div class="nodeTree-margin nodeTree-selectNode">
							<div class="clearfix"
								 <?php if (empty($rootNode)) { ?>style="margin: 0px <?php echo $left; ?>px;"<?php } ?>>
								<div class="label-wrapper">
									<!--<span class="collapse-node"></span>-->
									<span></span>

									<div class="label-for-radio"><?php
										echo ( $node->coreOrgChart->translation ? $node->coreOrgChart->translation : $defaultOrgChartTranslation[$node->idOrg] );
									?></div>
								</div>
								<?php echo $form->radioButton($model, 'idOrg[]', array('value' => $node->idOrg)); ?>
							</div>
						</div>
					</div>
					<?php if (!empty($hasChildren)) { ?>
					<ul class="nodeUl">
				<?php } ?>
					<?php if (empty($hasChildren)) { ?>
						</li>
					<?php } ?>
					<?php $level = $node->lev; ?>
				<?php } ?>
			</ul>
		<?php } ?>

	</div>
	<?php $this->endWidget(); ?>
</div>

<div class="nodeParentsContainer">
	<div class="nodeParentsTitle"><?php echo Yii::t('organization', 'Your Category will be moved to:'); ?></div>
	<div class="nodeParents">
		<?php echo $contentParents; ?>
	</div>
</div>

<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input').styler();
			$('#orgChart_move').jScrollPane({autoReinitialise: true});
		});
	})(jQuery);
</script>