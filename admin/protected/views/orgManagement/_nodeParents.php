<ul class="clearfix">
	<?php foreach ($nodeParents as $parent) { ?>
		<li><span><?php echo $parent->coreOrgChart->translation;?></span></li>
	<?php } ?>
	<?php if (!empty($selectedNode->coreOrgChart->translation)) { ?>
		<li><span><?php echo $selectedNode->coreOrgChart->translation; ?></span></li>
	<?php } ?>
	<li class="last-node"><span><?php echo $currentNode->coreOrgChart->translation; ?></span></li>
</ul>