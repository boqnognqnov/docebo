<?php
$dataProvider = CoreUserField::model()->getOrgChartVisibilityDataProvider();
/* @var $dataProvider CActiveDataProvider */
?>
<div class="form-wrapper">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'orgChart_form',
	)); ?>

	<?= CHtml::hiddenField('confirm', '1', array('id' => 'input-confirm')) ?>

	<h3><?= $node_name ?></h3>
	<div class="orgchart-fields-visibility-grid-wrapper">

		<!--<div class="selections clearfix">
			<?php $this->widget('ext.local.widgets.GridViewSelectAll', array(
				'gridId' => 'orgchart-fields-visibility-grid',
				'class' => 'left-selections clearfix',
				'dataProvider' => $dataProvider,
				'itemValue' => 'id_field',
			)); ?>
		</div>-->

		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'orgchart-fields-visibility-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
			'dataProvider' => $dataProvider,
			'columns' => array(
				array(
					'selectableRows' => 2,
					'id' => 'orgchart-fields-visibility-grid-checkboxes',
					//'type' => 'raw',
					'header' => '',
					'name' => 'id_field',
					'value' => '$data->id_field',
					'checked' => function($data, $index) use (&$assigned, &$inherited) {
							return (in_array($data->id_field, $inherited) || in_array($data->id_field, $assigned['oc']) || in_array($data->id_field, $assigned['ocd']));
						},
					'disabled' => function($data, $index) use (&$inherited) {
							return (in_array($data->id_field, $inherited));
						},
					'class' => 'CCheckBoxColumn',
				),
				array(
					'type' => 'raw',
					'header' => Yii::t('standard', '_NAME'),
					'name' => 'translation',
					'value' => function($data, $index) { return CHtml::encode($data->getTranslation()); }
				),
				array(
					'type' => 'raw',
					'header' => Yii::t('standard', '_TYPE'),
					'name' => 'type',
					'value' => function($data, $index) { return Yii::t('field', '_'.strtoupper($data->type)); }
				),
			),
			'template' => '{items}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'pager' => array('class' => 'ext.local.pagers.DoceboLinkPager'),
			'ajaxUpdate' => 'orgchart-fields-visibility-grid-all-items',
			'beforeAjaxUpdate' => 'function(id, options) {
				// The Request type MUST be POST!
				options.type = "POST";
				if (typeof options.data == "undefined") { options.data = {}; }
				// To allow passing selected items over pagination
				options.data.selectedItems = $(\'#\'+id+\'-selected-items-list\').val();
			}',
			'afterAjaxUpdate' => 'function(id, data){
				$(\'a[rel="tooltip"]\').tooltip();
				preventDisabledCheckboxesToggling();
			}'
		)); ?>
	</div>

	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
	function preventDisabledCheckboxesToggling() {
		$('#orgchart-fields-visibility-grid tr td input[type=checkbox]').each(function() {
			if ($(this).prop('disabled')) {
				$(this).closest('tr').on('click', function(e) { e.stopPropagation(); });
			}
		});
	};
	$(function() {
		preventDisabledCheckboxesToggling();
	});
</script>
