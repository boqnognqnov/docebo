<div id="assignUserContent">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'userAssign-user-form',
		'htmlOptions' => array('class' => 'ajax-grid-form', 'data-grid' => '#userAssign-user-grid'),
	)); ?>
		<div class="main-section">
			<h3 class="title-bold">Filters &amp; Search</h3>

			<div class="filters-wrapper">
				<div class="selections" data-grid="userAssign-user-grid">
					<div class="input-wrapper">
						<input id="assign-search-user" type="text" name="CoreUser[search_input]" placeholder="Search user"/>
						<span class="search-icon" onclick="$('#userAssign-user-grid').yiiGridView('update');"></span>
					</div>
					<?php $this->widget('ext.local.widgets.GridViewSelectAll', array(
						'gridId' => 'userAssign-user-grid',
						'class' => 'left-selections',
						'dataProvider' => $userModel->dataProviderNode($id),
						'itemValue' => 'idst',
					)); ?>
				</div>
			</div>
		</div>

		<input type="hidden" name="contentType" value="html"/>

	<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
	<?php $this->endWidget(); ?>
	<div id="grid-wrapper" class="userAssign-user-table">
		<h2><?=Yii::t('standard', '_ASSIGN_USERS')?></h2>
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'ajaxType' => 'POST',	
			'id' => 'userAssign-user-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
			'dataProvider' => $userModel->dataProviderNode($id),
			'cssFile' => false,
			'columns' => array(
				array(
					'class' => 'CCheckBoxColumn',
					'selectableRows' => 2,
					'id' => 'userAssign-user-grid-checkboxes',
					'value' => '$data->idst',
					'checked' => 'in_array($data->idst, Yii::app()->session[\'selectedItems\'])',
				),
				array(
					'header' => 'Username',
					'name' => 'userid',
					'value' => 'Yii::app()->user->getRelativeUsername($data->userid);',
					'type' => 'raw',
				),
				array(
					'name' => 'fullname',
					'header' => 'Full Name',
					'value' => '$data->fullname',
					'type' => 'raw',
				),
				'email',
			),
			'template' => '{items}{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'pager' => array(
				'class' => 'DoceboCLinkPager',
				'maxButtonCount' => 8,
			),
			'beforeAjaxUpdate' => 'function(id, options) {
								options.type = "POST";
								if (options.data == undefined) {
									options.data = {
										contentType: "html",
										selectedItems: $(\'#\'+id+\'-selected-items-list\').val(),
										search_input: $("#assign-search-user").val()
									}
								}
							}',
			'ajaxUpdate' => 'userAssign-user-grid-all-items',
			'afterAjaxUpdate' => 'function(id, data){$("#userAssign-user-grid input").styler(); afterGridViewUpdate(id);}',
		)); ?>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$('#assign-search-user').on("keydown", function(e) {
			var code = e.keyCode || e.which;
			if (code == 13) {
				$('#userAssign-user-grid').yiiGridView('update');
			}
		});
	});
</script>