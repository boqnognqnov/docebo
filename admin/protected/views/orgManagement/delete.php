<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'user_form',
	));	?>

	<p><?=Yii::t('player', "Are you sure you want to delete:") . ' "'. $nodeName .'"?';?></p>

	<div class="clearfix">

		<?php if ($currentNode->isLeaf()) { ?>
			<?php echo $form->checkbox($model, 'confirm', array('id' => 'CoreOrgChartTree_confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
			<?php echo $form->labelEx($model, Yii::t('standard', 'Yes, I confirm I want to proceed'), array('for' => 'CoreOrgChartTree_confirm_'.$checkboxIdKey)); ?>
			<?php echo $form->error($model, 'confirm'); ?>
		<?php } else { ?>
			<p><?=Yii::t('course', 'This Org Node has children nodes')?></p>
		<?php } ?>

	</div>

	<?php
	echo CHtml::hiddenField('treeGenerationTime', false, array('id' => 'delete-node-tree-generation-time-input', 'class' => 'delete-node-tree-generation-time-input'));
	?>

	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
	(function() {
		//copy the tree generation time value in the dialog form, to be checked server-side
		var tgt = $('#tree-generation-time');
		if (tgt.length > 0 && tgt.val() != '') { //make sure value exists and is valid
			var treeGenerationTime = '' + tgt.val(); //avoid possible type casting, since the value is numeric
			$('.delete-node-tree-generation-time-input').val('' + treeGenerationTime); //(as above)
		}
	})();
</script>