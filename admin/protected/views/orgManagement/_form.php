<script type="text/javascript">
	$(function(){
		//Enable confirm button if it's disabled
		$('.btn.confirm-btn').removeClass('disabled').removeAttr('disabled');
	});
</script>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'orgChart_form',
	)); ?>

<div class="clearfix">
	<div class="languagesList">
		<div class="orgChart_form_title"><?php echo Yii::t('branding', '_PAGE_LANGUAGE_LABEL'); ?></div>
		<?php echo $form->dropDownList($model, 'lang_code', $activeLanguagesList); ?>
		<?php echo $form->error($model, 'lang_code'); ?>
	</div>

	<div class="orgChart_translationsList">
		<?php if (!empty($activeLanguagesList)) {
			reset($activeLanguagesList);
			$first_key = key($activeLanguagesList);

			?>
			<?php foreach ($activeLanguagesList as $key => $lang) {
				if (($key == $first_key && empty($model->lang_code)) || ($model->lang_code == $key)) {
					$class = 'show';
				} else {
					$class = 'hide';
				}
				?>
				<div id="NodeTranslation_<?php echo $key; ?>" class="languagesName <?php echo $class; ?>">
					<div class="orgChart_form_title"><?php echo Yii::t('admin_directory', '_DIRECTORY_MEMBERTYPETREE', array(str_replace(' *', '', $lang))); ?></div>
					<?php echo $form->textField($model, 'translation[' . $key . ']', array('class' => 'orgChart_translation', 'value' => (!empty($translationsList[$key]) ? $translationsList[$key] : ''))); ?>
					<?php echo $form->error($model, 'translation[' . $key . ']'); ?>
				</div>
			<?php } ?>
			<?php echo $form->error($model, 'translation'); ?>
		<?php } ?>
    </div>
	</div>

	<div class="orgChart_languages">
		<div><?php echo Yii::t('admin_lang', '_LANG_ALL').': <span>' . count($activeLanguagesList).'</span>'; ?></div>
		<div id="orgChart_assignedCount"><?php echo Yii::t('standard', 'Filled languages').': <span>'.((!empty($translationsList))?count(array_filter($translationsList)):0).'</span>'; ?></div>
	</div>
    <div class="orgChart_code clearfix">
			<?php echo $form->labelEx($model2, 'code'); ?>
			<?php echo $form->textField($model2, 'code', array('class' => '')); ?>
			<div class="clearfix"></div><?php echo $form->error($model2, 'code'); ?>
			<div class="clearfix"></div><div style="display:none;" id="CoreOrgChartTree_code_warning" class="error"><?= Yii::t('organization_chart', 'You have allready used this organization code') ?></div>
    </div>

	<?php
	// Let the plugins add additional field for the branch when creating/editing
	$event = new DEvent($this, array(
		'form' => &$form,
		'org_chart' => $model,
		'org_chart_tree' => $model2
	));
	Yii::app()->event->raise('onBeforeRenderOrgChartSettingsFields', $event);
	if (!$event->shouldPerformAsDefault() && $event->return_value) {
		return $event->return_value;
	}

 	$html = '';
	// Event raised for YnY app to hook on to
 	Yii::app()->event->raise('OrgChartFormAfterRender', new DEvent($this, array(
		'html' => &$html,
		'idOrg' => Yii::app()->getRequest()->getParam('id'),
		'form' => $form
 	)));
 	echo $html;
	?>

	<?php
	echo CHtml::hiddenField('treeGenerationTime', false, array('id' => 'edit-node-tree-generation-time-input', 'class' => 'edit-node-tree-generation-time-input'));
	?>

	<?php $this->endWidget(); ?>
</div>



<script type="text/javascript">
<!--

	// Listen for CODE input/change. Make an ajax call to check if the code is already used and show warning
	$('#CoreOrgChartTree_code').on('input', function(){

		var url = '<?= Docebo::createAbsoluteAdminUrl('orgManagement/axCheckBranchCode') ?>';
		url += '&code=' + encodeURI($(this).val()) + '&idOrg=' + '<?= $model2->idOrg ?>';

		$.get(url, function(res){
			res = jQuery.parseJSON(res);
			if (res.success) {
				if (res.data.exists) {
					$('#CoreOrgChartTree_code_warning').show();		
				}
				else {
					$('#CoreOrgChartTree_code_warning').hide();
				}
			}
		});

	});

	(function() {
		//copy the tree generation time value in the dialog form, to be checked server-side
		var tgt = $('#tree-generation-time');
		if (tgt.length > 0 && tgt.val() != '') { //make sure value exists and is valid
			var treeGenerationTime = '' + tgt.val(); //avoid possible type casting, since the value is numeric
			$('.edit-node-tree-generation-time-input').val('' + treeGenerationTime); //(as above)
		}
	})();

//-->
</script>

