<?php
/* @var $this NotificationController */
/* @var $data CoreNotification */



$infoText = '';

if (!$data->hasUserFilter()) {
	$infoText = Yii::t('standard', 'All branches and groups');
}
else {
	$parts = array();
	$info = $data->getUserFilterInfo();
	
	$nUsers 			= count($info['users']);
	$nGroups 			= count($info['groups']);
	$nBraches 			= count($info['branchesFlatList']);
	//$parts[] = $nUsers . " " . Yii::t('standard', '_USERS');
	$parts[] = $nGroups . " " . Yii::t('standard', '_GROUPS');
	$parts[] = $nBraches . " " . Yii::t('standard', 'branches');
	
	if (count($parts) > 0) {
		$infoText = implode(', ', $parts);
	}
}


if (!$data->hasUserFilter())
	$infoText = Yii::t('standard', 'All branches and groups');


// Wizard's Dialog2 modal-body ID
$wizardDialogId 	= 'notification-modal';

// jWizard dispatcher where all request are sent to
$wizardUrl 			= Docebo::createAdminUrl('notification/wizard', array(
	// Hide Next/Prev and show SAVE button in Users Selector - we are in edit mode
	// 'nowiznav'			=> 1,
	'jump_in' 	=> 'step_user_filter',
	'id'		=> $data->id,
));
	
echo  CHtml::link($infoText, $wizardUrl, array(
		'class' => 'open-dialog underline',
		'rel' => 'tooltip',
		'data-html' => true,
		'data-dialog-class' => "edit-notification-modal",
		'data-dialog-id' => $wizardDialogId,
		'data-dialog-title' => Yii::t('standard', 'Filter'),
		'title'	=> $tooltipText
));


?>




