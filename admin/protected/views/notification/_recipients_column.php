<?php

// Wizard's Dialog2 modal-body ID
$wizardDialogId = 'notification-modal';

// jWizard dispatcher where all request are sent to
$wizardUrl = Docebo::createAdminUrl('notification/wizard', array(
			'jump_in' => 'step_recipients',
			'id' => $data->id,
		));

if ($data->recipient == CoreNotification::NOTIFY_RECIPIENTS_POWER_USER) {
	if (intval($data->puProfileId) === 0) {
		// that means all profiles are assigned
		$infoText = Yii::t('standard', 'All power user profiles');
	} else {
		// the only one PU profile is assigned
		$puProfile = CoreGroup::model()->findByPk($data->puProfileId);
		if ($puProfile) {
			$puProfileName = CoreGroup::model()->findByPk($data->puProfileId)->relativeProfileId();
			$infoText = CoreNotification::recipientName($data->recipient) . ' (' . $puProfileName . ')';
		} else {
			$data->puProfileId = 0;
			$data->save();
			$infoText = Yii::t('standard', 'All power user profiles');
		}
	}
} else {
	$infoText = CoreNotification::recipientName($data->recipient);
}

$tooltipText = "";
if (!strstr($data->type, "App7020")) {
echo CHtml::link($infoText, $wizardUrl, array(
	'class' => 'open-dialog underline',
	'data-html' => true,
	'data-dialog-class' => "edit-notification-modal",
	'data-dialog-id' => $wizardDialogId,
	'data-dialog-title' => Yii::t('notification', 'Edit notification'),
	'rel' => 'tooltip',
	'title' => $tooltipText,
	'data-html' => true,
));
} else {
	echo "N/A";
}
