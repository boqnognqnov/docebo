<?php
/* @var $this NotificationController */
/* @var $data CoreNotification */


// Wizard's Dialog2 modal-body ID
$wizardDialogId 	= 'notification-modal';

// jWizard dispatcher where all request are sent to
$wizardUrl 			= Docebo::createAdminUrl('notification/wizard', array(
	'jump_in' 	=> 'step_base',
	'id'		=> $data->id,
));

$infoText = !empty($data->code)?$data->code:'&nbsp;';

echo  CHtml::link($infoText, $wizardUrl, array(
	'class' => 'open-dialog',
	'rel' => 'tooltip',
	'data-html' => true,
	'data-dialog-class' => "edit-notification-modal",
	'data-dialog-id' => $wizardDialogId,
	'data-dialog-title' => Yii::t('notification', 'Edit notification'),
));


?>

