<?php
/* @var $this UsersSelectorController */
/* @var $model CoreNotification */
?>
<?php
	$model = CoreNotification::model()->findByPk($this->idGeneral);
	$hasAssoc = $model ? $model->hasAssoc() : false;
?>
	
	
	
<div class="notification-selector-top-panel">




	<br>
	<div class="row-fluid">
		<div class="span12">
			<?= Yii::t('notification','Apply this notification to all courses or choose specific courses/learning plans') ?>
		</div>
	</div>
	
	<?php if (Yii::app()->user->hasFlash('error')) : ?>
		<div class="row-fluid">
			<div class="span12">
	   			<div class='alert alert-error alert-compact text-left'>
    				<?= Yii::app()->user->getFlash('error'); ?>
    			</div>
    		</div>
    		</div>
		<?php endif; ?>
	<div class="row-fluid">
	
		<div class="grey-wrapper">
	
			<div class="row-fluid">
				<div class="span12">
					
					<div class="row-fluid">
						<?php
						if (isset($withoutCourses) && $withoutCourses === true){ ?>
							<div class="span6">
								<label class="radio pull-left">
									<?php
									echo CHtml::radioButton('cfilter_option', true, array('value' => CoreNotification::COURSES_DO_ASSOC));
									echo Yii::t('standard','_SELECT').' '.Yii::t('myactivities', 'Learning plans');
									?>
								</label>
							</div>
						<?php }else{ ?>
							<div class="span3">
								<label class="radio pull-left">
									<?php
									echo CHtml::radioButton('cfilter_option', !$hasAssoc, array('value' => CoreNotification::COURSES_NO_ASSOC));
									echo Yii::t('standard', '_ALL_COURSES');
									?>
								</label>
							</div>
							<div class="span3">
								<label class="radio pull-left">
									<?php
									echo CHtml::radioButton('cfilter_option', $hasAssoc, array('value' => CoreNotification::COURSES_DO_ASSOC));
									echo Yii::t('standard', 'Select courses');
									?>
								</label>
							</div>
						<?php }
						?>

					</div>
					
					
					
				</div>
			</div>
		
		</div>
	
	</div>


</div>

<script type="text/javascript">

	$(function(){
		// Call our Notification manager to signal it that filter panel is loaded. It will add/run some code.
		Notification.selectorTopPanelReady();
	});
	
</script>