<?php
/** @var $this NotificationController */
/** @var $model CoreNotification */

	// Wizard's Dialog2 modal-body ID
	$wizardDialogId 	= 'notification-modal';
	
	// jWizard dispatcher where all request are sent to
	$wizardUrl 			= Docebo::createAdminUrl('notification/wizard', array(
		'nowiznav'			=> 1,
		'jump_in' 			=> 'step_course_assoc',
		'id'				=> $model->id,
		'loadyiigridview'	=> 1, // VERY important; if not passed, Selector will NOT load yiigridview JS
	));
	
	// Notification descriptor info
	$notInfo = CoreNotification::getNotificationInfo($model->type);
	if (!$notInfo) {
		$title = $model->type;
	}
	else {
		$title = $notInfo['title'];
	}

	$this->breadcrumbs = array(
		Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
		Yii::t('notification', 'Notifications'),
		$title,
		Yii::t('standard', 'Assign courses')
	);

/**
 * If all courses are assigned to the notification, then the layout should change. The course list view will be
 * replaced with a message saying that all courses are assigned and no filters will be displayed on the page
 */
$isAllCoursesSelected = (intval($model->cfilter_option) === CoreNotification::COURSES_NO_ASSOC);

?>

<style type="text/css">
	.notification-assoc-courses .main-section .filters { padding: 10px; }
	#notification-assoc-courses-list .item .item-type {
		float: right;
		margin-right: 40px;
	}
	#notification-assoc-courses-list .item .item-name {
		margin-left: 30px;
	}
	#notification-assoc-courses-list .item .item-type i {
		display: inline-block;
		height: 20px;
		vertical-align: text-bottom;
		width: 22px;
		background: url("<?= Yii::app()->theme->baseUrl ?>/images/icons_elements.png") -252px -48px; no-repeat;
		margin-left: 10px;
	}
</style>

<div class="notification-assoc-courses">
	<h3 class="title-bold back-button">
		<?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('notification/index')); ?>
		<span><?= Yii::t('standard', 'Assign courses') . ": " . $title ?></span>
	</h3>

	<br>
	<div class="main-actions clearfix">
		<ul class="clearfix">
			<li>
				<div>
					<a href="<?= $wizardUrl ?>"
						alt="<?= Yii::t('standard', 'Assign courses');?>"
						class="open-dialog new-node"
						data-dialog-id = "<?= $wizardDialogId ?>"
						data-dialog-class = "edit-notification-modal"
						data-dialog-title="<?= Yii::t('standard', 'Assign courses') ?>"
						>
						<span></span> <?= Yii::t('standard', 'Assign courses') ?>
					</a>
				</div>
			</li>
		</ul>
		<div class="info">
			<div>
				<h4 class="clearfix"></h4>
				<p></p>
			</div>
		</div>
	</div>

	<?php if (!$isAllCoursesSelected) : ?>

		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'notification-assoc-courses-form',
			'htmlOptions' => array(
				'class' => 'ajax-list-form',
				'data-grid' => '#notification-assoc-courses-list'
			),
		)); ?>
		<div class="main-section">
			<div class="filters-wrapper">
				<div class="filters clearfix">
					<div class="row-fluid">
						<div class="span6">
							<label class="checkbox inline" for="">
								<input type="checkbox" value="elearning" name="CoreNotification[filterType][]" />
								<?= Yii::t('standard', '_ELEARNING') ?>
							</label>
							<label class="checkbox inline" for="">
								<input type="checkbox" value="classroom" name="CoreNotification[filterType][]" />
								<?= Yii::t('standard', '_CLASSROOM') ?>
							</label>
							<label class="checkbox inline" for="">
								<input type="checkbox" value="learning-plan" name="CoreNotification[filterType][]" />
								<?= Yii::t('standard', '_COURSEPATH') ?>
							</label>
						</div>
						<div class="span6">
							<div class="input-wrapper">
								<input type="text" class="typeahead pull-right" name="CoreNotification[search]" autocomplete="off" placeholder="<?= Yii::t('standard', '_SEARCH') ?>" data-url="<?= $this->createUrl('assocCoursesAutocomplete', array('id' => $model->id)) ?>"/>
								<span class="search-icon"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php $this->endWidget(); ?>

		<div class="selections clearfix">
			<?php $this->widget('ext.local.widgets.ListViewSelectAll', array(
				'gridId' => 'notification-assoc-courses-list',
				'class' => 'left-selections clearfix',
				'dataProvider' => $model->sqlDataProviderAssocCourses(),
				'itemValue' => '$data[\'pk\']',
			)); ?>

			<div class="right-selections clearfix">
				<select name="massive_action" id="notification-assoc-courses-management-massive-action">
					<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
					<option value="delete"><?php echo Yii::t('standard', '_UNASSIGN'); ?></option>
				</select>
			</div>
		</div>

		

		<div class="bottom-section">
			<div class="b-top p-top p-bottom">
				<div class="pull-right"><?= Yii::t('conference', 'Scheduled') ?>: <strong><?= $model->getScheduleText() ?></strong></div>
				<div class="title-bold"><?= Yii::t('standard', 'Assigned courses') .': ' . $title ?></div>
			</div>

			<div id="grid-wrapper">
				<?php $this->widget('ext.local.widgets.CXListView', array(
					'id' => 'notification-assoc-courses-list',
					'htmlOptions' => array('class' => 'list-view clearfix'),
					'dataProvider' => $model->sqlDataProviderAssocCourses(),
					'itemView' => '_notificationCourseItemView',
					'itemsCssClass' => 'items clearfix',
					'summaryText' => Yii::t('standard', '_TOTAL'),
					'pager' => array(
						'class' => 'ext.local.pagers.DoceboLinkPager',
					),
					'template' => '{items}{pager}{summary}',
					'ajaxUpdate' => 'poweruser-courses-management-list-all-items',
					'beforeAjaxUpdate' => 'function(id, options) {
						options.type = "POST";
						if (options.data == undefined) {
							options.data = {
								selectedItems: $(\'#\'+id+\'-selected-items-list\').val()
							}
						}
					}',
					'afterAjaxUpdate' => 'function(id, data) {
						$(document).controls();
						$(\'a[rel="tooltip"]\').tooltip();
						$(\'#\'+id).find(\':checkbox\').styler({});
						afterListViewUpdate(id);
						$.fn.updateListViewSelectPage();
					}',
				)); ?>
			</div>
		</div>

	<?php else: ?>

		<div class="b-top p-top p-bottom">
			<div class="pull-right"><?= Yii::t('conference', 'Scheduled') ?>: <strong><?= $model->getScheduleText() ?></strong></div>
			<div class="title-bold"><?= Yii::t('standard', 'Assigned courses') .': ' . $title ?></div>
		</div>

		<div class="bordered">
			<div>
				<?php echo Yii::t('standard', 'You have selected'); ?> <strong><?= Yii::t('standard', '_ALL_COURSES') ?></strong>
			</div>
		</div>

	<?php endif; ?>
</div>



<script type="text/javascript">
//<![CDATA[
           
	// Create Wizard object (themes/spt/js/jwizard.js)
	var jwizard = new jwizardClass({
		dialogId		: '<?= $wizardDialogId ?>',
		dispatcherUrl	: '<?= $wizardUrl ?>',
		alwaysPostGlobal: true,
		debug			: false
	});

	$(document).ready(function() {
		Notification.assignCoursesPageReady(<?= json_encode(Docebo::createAppUrl('admin:notification/unassignCourses')) ?>, <?= (int) CoreNotification::COURSES_DO_ASSOC ?>);
	});

	// This function must be global .. errm.. Quartz scriptModal.js
	$.fn.updateNotificationAssocCoursesList = function(data) {
		if (data.html) {
			$('.modal.in .modal-body').html(data.html);
		} else {
			$('#notification-assoc-courses-list-selected-items-list').val('');
			$.fn.yiiListView.update('notification-assoc-courses-list');
			$('.modal.in').modal('hide');
		}
	}

//]]>
</script>