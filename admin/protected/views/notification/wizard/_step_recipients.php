<?php 
	/* @var $model CoreNotification */

	$recipients = $notInfo['allowedRecipientTypes'];
	if ($model && !array_key_exists($model->recipient, $recipients)) {
		$model->recipient = key($recipients); // first key
	} 

?>

<div class="step-recipients-container">

	<?php 
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'edit-notification-recipients-form',
			'htmlOptions' => array(
				'class' => 'ajax form-horizontal jwizard-form',
				'name' => 'edit-notification-recipients-form'
			),
		));

		// Wizard related
		echo CHtml::hiddenField('from_step', 'step_recipients');
		echo CHtml::hiddenField('id', $model->id);

	?>


	<fieldset>

		<?php if (Yii::app()->user->hasFlash('error')) : ?>
			<div class='alert alert-error alert-compact text-center'>
				<?= Yii::app()->user->getFlash('error'); ?>
			</div>
		<?php endif; ?>


		<div class="row-fluid">
			<div class="span12">
				<?= Yii::t('notification', 'Select target role for your notification') ?>:
			</div>
		</div>


		<div class="row-fluid">
			<div class="span12 grey-wrapper">
			
				<div clss="row-fluid">
					<?php foreach ($recipients as $key => $recipient) : ?>
						<div class="span6 thumbnails fluid comfort">
							<?php $htmlId = 'recip-type-radio-' . $key; ?>
							<div class="control-group">
								<label class="radio" for="<?= $htmlId ?>">
									<?php
										echo $form->radioButton($model, 'recipient', array(
											'id' 					=> $htmlId,
											'uncheckValue' 			=> null,
											'value' 				=> $key,
										));
									?>
									<?= $recipient['name'] ?>
									<br>
									<span class="radio-option-descr-for-styler"><?= $recipient['descr'] ?></span>
								</label>
								<?php
								if ($key == 'poweruser') {
									echo CHtml::dropDownList('CoreNotification[puProfileId]', $model->puProfileId, $profilesList,array('style'=>'display:none', 'id'=>'powerUserProfile'));
								} ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>


		<!-- For Dialog2 - they go in footer -->
		<div class="form-actions jwizard">
			<?php
			if ($wizardMode) {
				echo CHtml::submitButton(Yii::t('standard', '_PREV'), array(
					'class' => 'btn btn-docebo green big jwizard-nav',
					'name' => 'prev_button',
				));

				echo CHtml::submitButton(Yii::t('standard', '_NEXT'), array(
					'class' => 'btn btn-docebo green big jwizard-nav',
					'name' => 'next_button',
				));
			} else {
				echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class' => 'btn btn-submit notification-submit'));
			}
			?>
			<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
		</div>

	</fieldset>


	<?php $this->endWidget(); ?>

</div>


<script type="text/javascript">
//<![CDATA[
           
	$(function () {
		$('.step-recipients-container :input').styler();

		var recipientType = '<?=$model->recipient;?>';
		if(recipientType == 'poweruser')
			$('#powerUserProfile').show();
		
		$('.jq-radio').click(function () {
			var input = $(this).prev();
			console.log(input[0].id);
			if (input[0].id == 'recip-type-radio-poweruser') {
				$('#powerUserProfile').show();
			} else {
				$('#powerUserProfile').hide();
			}
		});
	});
           
           
//]]>
</script>
