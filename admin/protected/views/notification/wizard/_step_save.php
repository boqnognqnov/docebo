<?php 
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'edit-notification-save-form',
		'htmlOptions' => array(
			'class' => 'ajax form-horizontal jwizard-form',
			'name' => 'edit-notification-save-form'
		),
	));

	// Wizard related
	echo CHtml::hiddenField('from_step', 'step_save');
	echo CHtml::hiddenField('id', $model->id);
?>

<div class="wrapper report-success row-fluid">

	<div class="span3">
		<div class="left success" style="margin-top: 40px;">
			<div class="copy-success-icon"></div><?php echo Yii::t('notification', 'Notification successfully completed'); ?>
		</div>
	</div>

	<div class="text-right span9">

		<div class="text-right">
			<p class="intro">
				<?php echo Yii::t('standard', 'What do you want to do next?'); ?>
			</p>

			<div class="row row-with-border">
				<?php
					echo CHtml::submitButton(Yii::t('notification', 'Save and activate'), array(
						'name' => 'finish_wizard_activate',
						'class' => 'btn btn-docebo green big jwizard-nav jwizard-finish',
					));
				?>
				<p><?php echo Yii::t('notification', '<strong>Save</strong> the notification and <strong>activate it</strong>'); ?></p>
			</div>
			<div class="row">
				<?php 
					echo CHtml::submitButton(Yii::t('notification', 'Save and back to list'), array(
						'name' => 'finish_wizard_list', 
						'class' => 'btn btn-docebo green big jwizard-nav jwizard-finish', 
					));
				?>
				<p><?php echo Yii::t('notification', '<strong>Just save</strong> the notification'); ?></p>
			</div>
		</div>

	</div>

</div>


<!-- For Dialog2 - they go in footer -->
<div class="form-actions jwizard">
	<?php
	echo CHtml::submitButton(Yii::t('standard', '_PREV'), array(
		'class' => 'btn btn-docebo green big jwizard-nav',
		'name' => 'prev_button',
	));
	?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
</div>

<?php $this->endWidget(); ?>




<script type="text/javascript">
//<![CDATA[
           
	$(function(){
		// We have special class for this dialog; ask jWizard to add it to .modal
		jwizard.setDialogClass('edit-notification-wizard-step-save');
	});


	$('input[name="finish_wizard_activate"], input[name="finish_wizard_list"]').on('click', function(e){
		var button = $(e.target);

		if(button.hasClass('inProgress')) {
			e.preventDefault();
		} else {
			button.addClass('inProgress');
		}
	});
           
//]]>
</script>
