<?php 
	/* @var $model CoreNotification */

	$schedTypeList = array();
	foreach ($notInfo['allowedScheduleTypes'] as $key) { 
		$schedTypeList[$key] = CoreNotification::scheduleTypeLabel($key);
	}

	if ($model && !array_key_exists($model->schedule_type, $schedTypeList)) {
		$model->schedule_type = key($schedTypeList); // first key
	}


	$beforeText = Yii::t('notification', 'before the event, at');
	$afterText 	= Yii::t('notification', 'after the event, at');
	
	$beforeTextNoAt = Yii::t('notification', 'before the event');
	$afterTextNoAt 	= Yii::t('notification', 'after the event');

	$everyText = Yii::t('notification', 'at');
	$onDayText = Yii::t('notification', 'on day');


	$hoursList = Yii::app()->localtime->hoursList(LocalTime::HOUR_24);

?>


<div class="step-schedule-container">

	<?php 
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'edit-notification-schedule-form',
			'htmlOptions' => array(
				'class' => 'ajax form-horizontal jwizard-form',
				'name' => 'edit-notification-schedule-form'
			),
		));

		// Wizard related
		echo CHtml::hiddenField('from_step', 'step_schedule');
		echo CHtml::hiddenField('id', $model->id);
	?>


	<fieldset>

		<div class="row-fluid">
			<div class="span12">
				<?= Yii::t('notification', 'Schedule notification') ?>

				<?php if (Yii::app()->user->hasFlash('error')) : ?>
				<div class='alert alert-error alert-compact text-left'>
					<?= Yii::app()->user->getFlash('error'); ?>
				</div>
				<?php endif; ?>

				<hr class="tiny">

			</div>
		</div>

		<div class="row-fluid">
			<div class="span4">
				<?php foreach ($schedTypeList as $key => $value) : ?>
					<?php
						$htmlId = 'schedule-type-radio-' . $key; 
					?>
					<div class="control-group">
						<label class="radio" for="<?= $htmlId ?>">
							<?php
								echo $form->radioButton($model, 'schedule_type', array(
									'id' 					=> $htmlId,
									'uncheckValue' 			=> null,
									'value' 				=> $key,
								));
								echo $value;
							?>
						</label>
					</div>
				<?php endforeach; ?>
			</div>

			<div class="span8">

				<div id="for-at">
					<?= Yii::t('notification', 'Send notification at the time of the event') ?>
				</div>

				<div class="row-fluid" id="for-shifted">

					<div class="span12">

					<table>
						<tr>
							<td colspan="3">
								<?= Yii::t('notification', 'Send notification') ?>:
							</td>
							<td>
								<div class="time-info">
								HH
								</div>
							</td>
							
						</tr>

						<tr>
							<td>
								<?php
									echo $form->textField($model, 'schedule_shift_number', array(
										'style' => 'width: 50px;'		
									));
								?>
								&nbsp;
							</td>
							<td>
								&nbsp;
								<?php
									echo $form->dropDownList(
										$model,
										'schedule_shift_period', 
										CoreNotification::scheduleShiftPeriods(), 
										array(
											'style' => 'width: 150px;'
										)
									);
								?>
								&nbsp;
							</td>
							<td>
								<span id="shift-text"></span>
								&nbsp; 
							</td>

							<td>
								<div class="time-info">
									<?php
										echo $form->dropDownList($model, 'schedule_time', $hoursList, array(
											'style' => 'width: 90px;'
										));
									?>
									&nbsp;
								</div>
							</td>

						</tr>
					</table>

					</div>

				</div>

				<div id="for-every">
					<div class="span12">
						<table>
							<tr>
								<td colspan="5">
									<?= Yii::t('notification', 'Send notification') ?>:
								</td>
								<td>
									<div class="time-info-every">
										HH
									</div>
								</td>

							</tr>

							<tr>
								<td>
									<?php
									echo $form->dropDownList(
										$model,
										'schedule_shift_period_every',
										CoreNotification::scheduleShiftPeriodsEvery(),
										array(
											'style' => 'width: 150px;'
										)
									);
									?>
									&nbsp;
								</td>
								<td>
									<div class="day-info">
										<?php
										echo $form->dropDownList(
											$model,
											'schedule_day',
											CoreNotification::scheduleShiftPeriodsDay(),
											array(
												'style' => 'width: 150px;'
											)
										);
										?>
										&nbsp;
									</div>
								</td>
								<td>
									<span id="shift-text-onday"></span>
									&nbsp;
								</td>
								<td>
									<div class="on-day">
										<?php
										$monthDays = array();
										for ($i = 1; $i < 32; $i++) {
											$monthDays[$i]=$i;
										}
										echo $form->dropDownList(
											$model,
											'schedule_on_day',
											$monthDays,
											array(
												'style' => 'width: 50px;'
											)
										);
										?>
										&nbsp;
									</div>
								</td>
								<td>
									<span id="shift-text-every"></span>
									&nbsp;
								</td>

								<td>
									<div class="time-info-every">
										<?php
										echo $form->dropDownList($model, 'schedule_time_every', $hoursList, array(
											'style' => 'width: 90px;'
										));
										?>
										&nbsp;
									</div>
								</td>

							</tr>
						</table>
					</div>
				</div>

			</div>
			
			
		</div>

		<!-- For Dialog2 - they go in footer -->
		<div class="form-actions jwizard">
			<?php
				if ($wizardMode) {
					echo CHtml::submitButton(Yii::t('standard', '_PREV'), array(
						'class'  => 'btn btn-docebo green big jwizard-nav',
						'name'	=> 'prev_button',
					));

					echo CHtml::submitButton(Yii::t('standard', '_NEXT'), array(
						'class'  => 'btn btn-docebo green big jwizard-nav',
						'name'	=> 'next_button',
					));
				} 
				else {
					echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class' => 'btn btn-submit notification-submit'));
				}
			?>
			<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
		</div>

	</fieldset>

	<?php $this->endWidget(); ?>


</div>

<script type="text/javascript">
	//<![CDATA[

	$(function () {

		var updateUi = function () {
			var type = $('[id^="schedule-type-radio-"]:checked').val();   	// after/before/at/every radio
			var period = $('select[name*="schedule_shift_period"]').val();  // hours, days, weeks
			var period_every = $('#for-every select[name*="schedule_shift_period"]').val();  // hours, days, weeks
			var shiftText = '';
			var shiftTextEvery = '';
			var shiftTextOnDay = '';

			switch (type) {
				case 'after':
					if (period == 'hour') {
						shiftText = <?= json_encode($afterTextNoAt) ?>;
						$('.time-info').hide();
					}
					else {
						$('.time-info').show();
						shiftText = <?= json_encode($afterText) ?>;
					}
					type = 'shifted';
					break;
				case 'before':
					if (period == 'hour') {
						$('.time-info').hide();
						shiftText = <?= json_encode($beforeTextNoAt) ?>;
					}
					else {
						$('.time-info').show();
						shiftText = <?= json_encode($beforeText) ?>;
					}
					type = 'shifted';
					break;
				case 'every':
					if (period_every == 'hour') {
						$(document).find('.time-info-every').hide();
						shiftTextEvery = '';
					}
					else {
						$(document).find('.time-info-every').show();
						shiftTextEvery = <?= json_encode($everyText) ?>;
					}
					if (period_every == 'week') {
						$(document).find('.day-info').show();
					}
					else {
						$(document).find('.day-info').hide();
					}
					if (period_every == 'month') {
						$(document).find('.on-day').show();
						shiftTextOnDay = <?= json_encode($onDayText) ?>;
					}
					else {
						$(document).find('.on-day').hide();
						shiftTextOnDay ='';
					}
					break;
			}

			$('#shift-text').text(shiftText);
			$('#shift-text-every').text(shiftTextEvery);
			$('#shift-text-onday').text(shiftTextOnDay);

			$('[id^="for-"]').hide();
			$('[id^="for-' + type + '"]').show();
		};


		$('.step-schedule-container :input').styler();

		$('[id^="schedule-type-radio-"]').on('change', function () {
			updateUi();
		});

		$('select[name*="schedule_shift_period"],select[name*="schedule_shift_period_every"]').on('change', function () {
			updateUi();
		});

		// Hide all and then force change
		$('[id^="for-"]').hide();
		$('[id^="schedule-type-radio-"]').trigger('change');

		$('.step-schedule-container :input').trigger('refresh');

	});

	//]]>
</script>
