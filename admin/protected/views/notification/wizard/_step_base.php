<?php

/* @var $this NotificationController */
/* @var $model CoreNotification */
/* @var $selectedTransports array */

$_textAreaIds = array();

$_transports = CoreNotificationTransport::getActiveTransportsList();

$_selectedTransportsIds = array();
foreach ($selectedTransports as $selectedTransport) {
	$_selectedTransportsIds[] = $selectedTransport['transport_id'];
}
?>

<div class="form">

	<?php
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'edit-notification-form',
		'htmlOptions' => array(
			'class' => 'ajax form-horizontal jwizard-form',
			'name' => 'edit-notification-form'
		),
	));

	// Wizard related
	echo CHtml::hiddenField('from_step', 'step_base');
	echo CHtml::hiddenField('id', $model->id);


	?>

	<div class="span12">
		<?php if (Yii::app()->user->hasFlash('error')) : ?>
			<div class='alert alert-error alert-compact text-left'>
				<?= Yii::app()->user->getFlash('error'); ?>
				<hr class="tiny">
			</div>
		<?php endif; ?>
	</div>


	<div class="tabs" id="notification-settings-tabs">
		<ul class="nav nav-tabs">

			<li class="active" id="menu-selector-general">
				<a data-target="tab-general" href="#">
					<?= Yii::t('notification', 'General settings') ?>
				</a>
			</li>

			<?php
			foreach ($_transports as $_transport) {
				?>
				<li id="menu-selector-<?= $_transport['id'] ?>"<?= !in_array($_transport['id'], $_selectedTransportsIds) ? ' class="disabled"' : '' ?>>
					<a data-target="tab-<?= $_transport['id'] ?>" href="#">
						<?= $_transport['editor_class']->getLabel() ?>
					</a>
				</li>
				<?php
			}
			?>

		</ul>


		<div class="tab-content edit-notification-form clearfix">

			<div class="tab-pane active" id="tab-general">
				<h3><?= Yii::t('notification', 'General settings') ?></h3>
				<hr/>
				<div class="row-fluid">
					<div class="control-group">
						<label><?= Yii::t('standard', 'Choose the event') ?></label>
						<?php
						echo CHtml::listBox('CoreNotification[type]', $model->type, array('0' => Yii::t('standard', '_SELECT')) + $notifyTypes,
							array(
								'size' => 1,
								'id' => 'notification-notifytype-select',
								'class' => 'span12',
							));
						?>
					</div>
				</div>

				<div class="row-fluid">
					<div class="control-group">
						<label><?= Yii::t('standard', '_CODE') ?></label>
						<?php
						echo CHtml::textField('CoreNotification[code]', $model->code, array('id' => 'notification-code', 'class' => 'span12'));
						?>
					</div>
				</div>

				<div class="row-fluid">
					<div class="control-group">
						<ul class="transports-list">
							<?php foreach ($_transports as $_transport) { ?>
								<li>
									<?php
									$_checkboxId = 'notification_transports_'.$_transport['id'];
									echo CHtml::checkBox('notification_transports['.$_transport['id'].']', in_array($_transport['id'], $_selectedTransportsIds), array(
										'id' => $_checkboxId,
										'value' => $_transport['id'],
										'data-menu' => 'menu-selector-'.$_transport['id']
									));
									echo CHtml::label($_transport['editor_class']->getLabel(), $_checkboxId);
									?>
								</li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>

			<?php foreach ($_transports as $_transport) { ?>
			<div class="tab-pane tab-transport-settings" id="tab-<?= $_transport['id'] ?>" data-transport_id="<?= $_transport['id'] ?>">
				<?php
				$this->renderPartial('transports/_tab_form', array(
					'notification' => $model,
					'transport' => CoreNotificationTransport::model()->findByPk($_transport['id']),
					'languages' => $languages,
					'shortcodes' => $shortcodes
				));
				$_textAreaIds['transport_'.$_transport['id']] = 'notification_message_'.$_transport['id'];
				?>
			</div>
			<?php } ?>

		</div>

	</div>


	<!-- For Dialog2 - they go in footer -->
	<div class="form-actions jwizard">

		<?php
		if ($wizardMode) {
			echo CHtml::submitButton(Yii::t('standard', '_NEXT'), array(
				'class'  => 'btn btn-docebo green big jwizard-nav notification-base-next-button',
				'name'	=> 'next_button',
			));
		}
		else {
			echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
				'class' => 'btn btn-submit notification-submit notification-base-next-button',
				'name'	=> 'save_single_button'
			));
		}
		?>

		<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>

	</div>

	<?php $this->endWidget(); ?>


</div>


<script type="text/javascript">
	//<![CDATA[


	Notification.languages = <?= CJSON::encode($languages) ?>;
	Notification.subjects = <?= CJSON::encode($subjects) ?>;
	Notification.messages = <?= CJSON::encode($messages) ?>;
	Notification.templates = <?= CJSON::encode($templates) ?>;
	Notification.shortcodes = <?= CJSON::encode($shortcodes) ?>;
	Notification.templates_apply_to = <?= CJSON::encode($templatesApplyTo) ?>;
	Notification.textAreaIds = [];


	$(function () {

		$('.notification-base-next-button').on('click', function(){
			$( "#edit-notification-form :input:visible" ).eq(1).focus();
		});

		//make sure "next" buttons starts with proper status
		Notification.checkNotificationTypeNextButton();

		//bind transport selector checkboxes to menu items
		$('#tab-general input[id^="notification_transports_"]').on('change', function() {
			var el = $(this);
			var target = el.data('menu');
			if (el.prop('checked')) {
				//enable menu voice
				$('li#'+target).removeClass('disabled');
			} else {
				//disable menu voice
				$('li#'+target).addClass('disabled');
			}
		});


		//prepare tabs
		$('#notification-settings-tabs ul.nav-tabs > li').on('click', function(e) {
			e.preventDefault();
			var li = $(this);
			if (li.hasClass('disabled')) { return; }
			var selector = li.find('a').data('target');
			li.parent().find('li').each(function(index, element) {
				var el = $(element);
				var a = el.find('a');
				if (!a) { return; }
				var target = a.data('target');
				if (!target) { return; }
				if (target == selector) {
					el.addClass('active');
				} else {
					el.removeClass('active');
				}
			});
			var panes = $('#notification-settings-tabs div.tab-pane');
			if (panes.length > 0) {
				panes.each(function(index, element) {
					var el = $(element);
					if (el.attr('id') == selector) {
						el.addClass('active');
					} else {
						el.removeClass('active');
					}
				});
			}
		});


		//apply stylers to inputs
		$('input, select').styler();


		Notification.isCleanStart = <?= ($model->id) ? "false" : "true" ?>;
		Notification.addHiddenFields();
		Notification.updateUi();
		Notification.updateShortcodes($('#notification-notifytype-select').val())

		$('.edit-notification-form textarea.notification-message-editor').each(function(index, element) {
			var textarea = $(element);
			switch (textarea.data('editor_tool')) {

				case 'tinymce':
					// This will create a tinymce editor attached to the Message textarea
					TinyMce.attach('#'+textarea.attr('id'), 'standard', {height: '250px', relative_urls: true, convert_urls: false, valid_children : "+body[style]"});
					// Tinymce is tricky: to add  listeners to the editor we need to wait until the editor is actually created
					tinymce.EditorManager.on("AddEditor", function (e) {
						// We are interested only on "OUR: textarea editor(s), ignore others
						if (e.editor.id == textarea.attr('id')) {
							Notification.addTinymceEditorListeners(e.editor);
							Notification.textAreaIds.push(e.editor.id);
						}
					});
					break;

				case 'none': //just to be sure ...
				default:
					//just leave the textarea as it is if none is specified
					break;
			}
		});

	});

	//]]>
</script>