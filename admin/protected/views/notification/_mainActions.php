<div class="main-actions clearfix">
	<ul class="clearfix">
		<li>
			<div>
				<a href="<?= $wizardUrl ?>"
				    alt="<?= Yii::t('helper', 'New notification');?>"
					class="open-dialog new-node"
					data-dialog-id = "<?= $wizardDialogId ?>"
					data-dialog-class = "edit-notification-modal"
					data-dialog-title="<?= Yii::t('event_manager', 'New notification') ?>"
					>
					<span></span> <?= Yii::t('event_manager', 'New notification') ?>
				</a>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>
			<p></p>
		</div>
	</div>
</div>
