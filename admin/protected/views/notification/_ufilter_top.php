<?php
/* @var $this UsersSelectorController */
/* @var $model CoreNotification */
?>
<?php
	$model = CoreNotification::model()->findByPk($this->idGeneral);
	$hasFiltering = $model ? $model->hasUserFilter() : false;
?>

<div class="notification-selector-top-panel">

	<br>
	<div class="row-fluid">
		<div class="span12">
			<?= Yii::t('standard','Apply this notification to all Branches or select custom Branches and/or groups') ?>
		</div>
	</div>

	<?php if (Yii::app()->user->hasFlash('error')) : ?>
		<div class="row-fluid">
			<div class="span12">
	   			<div class='alert alert-error alert-compact text-left'>
    				<?= Yii::app()->user->getFlash('error'); ?>
    			</div>
    		</div>
    	</div>
		<?php endif; ?>
	<div class="row-fluid">
	
	
	<div class="row-fluid">
	
		<div class="grey-wrapper">
	
			<div class="row-fluid">
				<div class="span12">
					
					<div class="row-fluid">
					
						<div class="span4">
							<label class="radio pull-left">
								<?php
									echo CHtml::radioButton('ufilter_option', !$hasFiltering, array('value' => CoreNotification::USER_NO_USER_FILTER)); 
									echo Yii::t('standard', 'All branches and groups');
								?>
							</label>
						</div>
						<div class="span4">
							<label class="radio pull-left">
								<?php
									echo CHtml::radioButton('ufilter_option', $hasFiltering, array('value' => CoreNotification::USER_DO_USER_FILTER)); 
									echo Yii::t('standard', 'Select branches and/or groups');
								?>
							</label>
						</div>
					</div>
					
					
					
				</div>
			</div>
		
		</div>
	
	</div>


</div>

<script type="text/javascript">

	$(function(){
		// Call our Notification manager to signal it that filter panel is loaded. It will add/run some code.
		Notification.selectorTopPanelReady();
	});
	
</script>