<?php
/* @var $notification CoreNotification */
/* @var $transport CoreNotificationTransport */

$_manager = $transport->getManagerObject();
?>

<h3>
	<?= $_manager->getLabel() ?>
</h3>

<hr />

<?php
echo $_manager->getAdditionalFormParameters($notification);
?>

<div class="row-fluid">
	<div class="span6">
		<div class="control-group">
			<?php
			echo CHtml::listBox(
				'languages[' . $transport->id . ']', '0', array_merge(
					array('0' => Yii::t('adminrules', '_SELECT_LANG_TO_ASSIGN')), $languages
				), array(
				'size' => 1,
				'id' => 'notification-lang-select_' . $transport->id,
				'class' => 'notification-lang-select span12',
				'data-transport_id' => $transport->id
				)
			);
			?>
		</div>
	</div>
	<div class="span6">
		<div><?= Yii::t('admin_lang', '_LANG_ALL'); ?>
			:&nbsp;
			<b class=""><?= count($languages) ?></b>
		</div>
		<div>
			<?= Yii::t('standard', 'Filled languages'); ?>&nbsp;
			<span class="assigned" id="assigned-<?= $transport->id ?>">
				<span><?= $countAssigned ?></span>
			</span>
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="span12">
		<div class="control-group">
			<label><?= Yii::t('standard', '_SUBJECT') ?></label>
			<?php
			$_prefix = 'notification_subject';
			echo CHtml::textField($_prefix . '[' . $transport->id . ']', '', array(
				'id' => $_prefix . '_' . $transport->id,
				'class' => 'notification-subject-editor span12',
				'data-transport_id' => $transport->id
			));
			?>
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="control-group">
		<label><?= Yii::t('standard', '_DESCRIPTION') ?></label>
		<?php
//check for additional stuff by manager class
		$_hintText = $_manager->getEditorHintText();
		if (!empty($_hintText)) {
			echo '<p class="notification-editor-hint-text">' . $_hintText . '</p>';
		}
//print text editor
		$_prefix = 'notification_message';
		$_params = array(
			'rows' => 50,
			'id' => $_prefix . '_' . $transport->id,
			'class' => 'notification-message-editor span12',
			'data-transport_id' => $transport->id
		);
		$_editorInfo = $transport->retrieveAdditionalInfo('editor_tool');
		if (!empty($_editorInfo)) {
			$_params['data-editor_tool'] = $_editorInfo;
		}
		echo CHtml::textArea($_prefix . '[' . $transport->id . ']', '', $_params);
		?>
	</div>
</div>

<div class="row-fluid">
	<div class="span12">
		<div class="docebo-alert notice repeatable-shortcodes hidden"  >
			This kind of notifications, require all the shortcodes enclose between [items] and [/items]
		</div>
		<strong><?= Yii::t('notification', 'Short codes') ?></strong>
		<br/>
		<?= Yii::t('notification', 'Use these shortcodes to insert data in the notification subject and/or message body') ?>
		<hr class="notification-shortcodes-separator"/>
		<ul id="notification_shortcodes_list_<?= $transport->id ?>" data-empty-text="" class="notification-shortcodes-list">
			<!-- content here will be filled by javascript in admin/js/notification.js-->
		</ul>
	</div>
</div>