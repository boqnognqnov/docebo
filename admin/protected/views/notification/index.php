<?php 
	$this->breadcrumbs = array(
		Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
		Yii::t('notification', 'Notifications'),
	);

	// Wizard's Dialog2 modal-body ID
	$wizardDialogId 	= 'notification-modal';

	// jWizard dispatcher where all request are sent to
	$wizardUrl 			= Docebo::createAdminUrl('notification/wizard', array());

	$this->renderPartial('_mainActions', array(
		'wizardDialogId' 	=> $wizardDialogId,
		'wizardUrl'		=> $wizardUrl,
	));

?>

<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'notificationsFilterForm',
	'htmlOptions' => array(
		'class' => 'form-inline',
	),
));
?>

<div class="filters-wrapper">
	<table class="filters">
		<tbody>
			<tr>
				<td class="table-padding">
					<table class="table-border">
						<tr>
							<td class="notificationFilterLeftTd">
								<label><?=Yii::t('audit_trail', 'Filter by event')?></label>
								<?php
								echo CHtml::listBox('notification-type-select', $model->type, array('0' => Yii::t('standard', '_SELECT')) + $notifyTypes,
									array(
										'size' => 1,
										'id' => 'notification-type-select',
										'class' => 'span3',
									));
								?>
							</td>
							<td class="notificationFilterRightTd">
								<div class="input-wrapper">
									<input data-url="<?= Docebo::createAppUrl('admin:notification/axNotificationsAutocomplete') ?>" class="typeahead"
										   id="notification-search-input" autocomplete="off" type="text"
										   name="notification-search-input"
										   value="<?=(!empty($model->code)?$model->code:'')?>"
										   placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"/>
									<button type="button" class="close clear-search">&times;</button>
									<span>
										<button class="search-btn-icon"></button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<?php
echo CHtml::submitButton('', array('style' => 'display: none;'));
$this->endWidget();
?>

<div class="grid-wrapper">

<!-- Grid -->
<?php
	$this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'notification-grid',
		'htmlOptions' => array(''),
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
 		'afterAjaxUpdate'=>'function(){
			$(document).controls();
			$(document).controls(); 
			$(\'a[rel="tooltip"]\').tooltip();
		}',
		//'dataProvider' => $model->notificationDataProvider(),
		'dataProvider' => $model->search(),

		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),

		'pager' => array(
			'class' => 'DoceboCLinkPager',
			'maxButtonCount' => 8,
		),
		'ajaxUpdate' => 'all-items',
		'beforeAjaxUpdate' => 'function(id, options) {
			resetSearchPlaceholder();
			options.data = $("#notificationsFilterForm").serialize();
		}',
		'columns' => array(
			array(
                'cssClassExpression' => '"code"',
				'name' => Yii::t('standard', '_CODE'),
				'type' => 'raw',
				'value' => array($this, 'renderGridCodeColumn'),
                'headerHtmlOptions' => array('class' => 'code'),
            ),

			array(
                'cssClassExpression' => '"event"',
				'name' => Yii::t('standard', '_EVENT'),
				'type' => 'raw',
				'value' => array($this, 'renderGridEventNameColumn'),
                'headerHtmlOptions' => array('class' => 'event'),
            ),
				
			array( 
				'name' => Yii::t('conference', 'Scheduled'),
				'type' => 'raw',
				'value' => array($this, 'renderGridScheduleColumn'),
								),
				
			array(
				'cssClassExpression' => '"text-center"',
				'name' => Yii::t('standard', 'Filter'),
				'type' => 'raw',
				'value' => array($this, 'renderGridUserFilterColumn'),
				'headerHtmlOptions' => array('class' => 'text-center'),					
			),
				
			array(
				'cssClassExpression' => '"text-center"',
				'name' => Yii::t('standard', 'Roles'),
				'type' => 'raw',
				'value' => array($this, 'renderGridRecipientsColumn'),
                'headerHtmlOptions' => array('class' => 'text-center'),
			),
				
			array(
				'cssClassExpression' => '"text-center"',
				'name' => Yii::t('standard', 'Associated courses'),
				'type' => 'raw',	
				'value' => array($this, 'renderGridAssocColumn'),
				'headerHtmlOptions' => array('class' => 'text-center'),
			),
				

			/*
			array(
				'cssClassExpression' => '"text-center"',
				'name' => Yii::t('admin_lang', '_LANG_TRANSLATION'),
				'value' => array($this, 'renderGridAssignedLanguages'),
                'headerHtmlOptions' => array('class' => 'center-aligned'),
				'type' => 'html',
			),
			*/


 			array(
 				'cssClassExpression' => '"text-right"',
 				'name' => '',
 				'type' => 'raw',
 				'value' => array($this, 'renderGridOperationsColumn')
 			),

		),

	));
?>

<br>
<br>

</div>

<script type="text/javascript">
//<![CDATA[
           
	// Create Wizard object (themes/spt/js/jwizard.js)
	var jwizard = new jwizardClass({
		dialogId: '<?= $wizardDialogId ?>',
		dispatcherUrl: '<?= $wizardUrl ?>',
		alwaysPostGlobal: true,
		debug: false
	});

//$(document).on('change', 'select#notification-type-select', function(){
//	$('#notification-grid').yiiGridView('update');
//});

//]]>

	<?php if (!Yii::app()->request->getIsAjaxRequest()) :?>
	function resetSearchPlaceholder() {
		var $search = $('#notification-search-input');
		if ($search.val() == "<?= Yii::t('standard', '_SEARCH'); ?>")
			$('#notification-search-input').val('');
	}

	<?php endif; ?>

	// Changing the dropdown list
	$('#notification-type-select').on('change', function(){
		$('#notificationsFilterForm').submit();
	})

	// Clearing the text box
	$('.clear-search').on('click', function(){
		$('#notification-search-input').val('');
		$('#notificationsFilterForm').submit();
	});
</script>