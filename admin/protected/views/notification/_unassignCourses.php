<?php
/** @var $this NotificationController */
/** @var $rows array */


$onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());';
$checkboxIdKey = time();
?>

<div class="form">
	<?php $form = $this->beginWidget('CActiveForm'); ?>

	<p><?php echo Yii::t('standard', '_UNASSIGN').': "'.count($rows).'" '.Yii::t('notification', 'Notifications'); ?></p>

	<div class="clearfix">
		<?php echo CHtml::checkBox('confirm', false, array('id' => 'confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed'), 'confirm_'.$checkboxIdKey); ?>
	</div>

	<?php foreach ($rows as $row) : ?>
		<?= CHtml::hiddenField('pk[]', $row['pk']) ?>
	<?php endforeach; ?>

	<?php $this->endWidget(); ?>
</div>