<?php
	$form = $this->beginWidget('CActiveForm', array(
		'method' => 'POST',
		'id' => 'delete-notification-form',
		'enableAjaxValidation' => false,
		'htmlOptions'=>array(
				'class' => 'ajax',
		),
	));

	$notInfo = CoreNotification::getNotificationInfo($model->type);
	if (!$notInfo) {
		$message = $model->type;
	}
	else {
		$message = $notInfo['title'];
	}
	
?>
<h1><?= Yii::t('standard','_DEL') ?></h1>

<label>
	<p><?= Yii::t('standard','_DEL') ?>: <?= $message ?></p>
</label>

<br>
<div class="clearfix"></div>
<label class="checkbox">
	<?php echo CHtml::checkBox('confirm_delete', false, array()); ?>&nbsp;
	<?php echo Yii::t('standard','Yes, I confirm I want to proceed'); ?>
</label>

<div class="form-actions">
	<?= CHtml::submitButton(Yii::t('standard', '_DEL'), array('class' => 'btn-submit disabled')); ?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn-cancel close-dialog')); ?>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
//<![CDATA[
	$(function(){
		$('input[type="checkbox"]').styler();

		$('input[name="confirm_delete"]').on('change', function() {
			$(this).prop("checked") ? $('.btn-submit').removeClass('disabled') : $('.btn-submit').addClass('disabled');
		});

	});
//]]>
</script>