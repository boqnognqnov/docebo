<?php
/* @var $this NotificationController */
/* @var $data CoreNotification */


// Wizard's Dialog2 modal-body ID
$wizardDialogId 	= 'notification-modal';

// jWizard dispatcher where all request are sent to
$wizardUrl 			= Docebo::createAdminUrl('notification/wizard', array(
		'jump_in' 	=> 'step_schedule',
		'id'		=> $data->id,
));


echo  CHtml::link($data->getScheduleText(), $wizardUrl, array(
		'class' => 'open-dialog underline',
		'rel' => 'tooltip',
		'data-html' => true,
		'data-dialog-class' => "edit-notification-modal",
		'data-dialog-id' => $wizardDialogId,
		'data-dialog-title' => Yii::t('notification', 'Edit notification'),
));


?>

