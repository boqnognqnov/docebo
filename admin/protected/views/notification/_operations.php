<?php

	$editUrl = $this->createUrl("notification/axEditBase", array('id' => $model->id));
	$deleteUrl = $this->createUrl("notification/axDelete", array('id' => $model->id));
	
	$isActive = $model->active > 0;
	$setActiveValue = $isActive ? '0' : '1';  
	$setActiveUrl = $this->createUrl("notification/axSetActive", array('id' => $model->id, 'active' => $setActiveValue));
	
	// Wizard's Dialog2 modal-body ID
	$wizardDialogId 	= 'notification-modal';
	
	// jWizard dispatcher where all request are sent to
	//$wizardUrl 			= Docebo::createAdminUrl('notification/wizard', array('id' => $model->id));
	$wizardUrl 			= Docebo::createAdminUrl('notification/wizard', array('id' => $model->id));

?>
<?php
	$color = ($model->active) ? "green" : "grey";  
?>

<a 
	href="<?= $setActiveUrl ?>" 
	class="change-active-status">
	<span class="i-sprite is-circle-check <?= $color ?>"></span>&nbsp;
</a>

<a 
	href="<?= $wizardUrl ?>" 
	class="open-dialog"
	data-dialog-id = "<?= $wizardDialogId ?>" 
	data-dialog-class="edit-notification-modal"
	data-dialog-title="<?= Yii::t('notification', 'Edit notification') ?>"
	>
	
	<span class="i-sprite is-edit"></span>
</a>

<a href="<?php echo $deleteUrl; ?>"
	class="open-dialog"
	data-dialog-class="delete-item-modal"
	closeOnEscape="false"
	closeOnOverlayClick="false"
	rel="delete-notification-modal">
	
	<span class="i-sprite is-remove red"></span>
</a>