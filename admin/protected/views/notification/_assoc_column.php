<?php
/* @var $this NotificationController */
/* @var $data CoreNotification */



$infoText = '';

if (!$data->hasAssoc()) {
	$infoText = '<span class="underline">' . Yii::t('standard', '_ALL_COURSES') . '</span>';
}
else {
	$parts = array();
	$info = $data->getAssocInfo();
	$allCourses = array_unique(array_merge($info['courses'], $info['plansCourses']));
	$nCourses = count($allCourses);
	
	$parts[] = $nCourses;
	if (count($parts) > 0) {
		$infoText = '<span class="underline">' . implode(', ', $parts) . '</span>&nbsp;&nbsp;<span class="i-sprite is-forum"></span>';
	}
}


$url = Docebo::createAdminUrl('notification/assocCourses', array('id' => $data->id));
echo  CHtml::link($infoText, $url, array(
	'class' => '',
	'rel' => 'tooltip',
	'data-html' => true,
));




/*
// Wizard's Dialog2 modal-body ID
$wizardDialogId 	= 'notification-modal';

// jWizard dispatcher where all request are sent to
$wizardUrl 			= Docebo::createAdminUrl('notification/wizard', array(
	'jump_in' 	=> 'step_course_assoc',
	'id'		=> $data->id,
));

echo  CHtml::link($infoText, $wizardUrl, array(
	'class' => 'open-dialog',
	'rel' => 'tooltip',
	'data-html' => true,
	'data-dialog-class' => "edit-notification-modal",
	'data-dialog-id' => $wizardDialogId,
	'data-dialog-title' => "<span class='icons-elements check-user white'></span>&nbsp;&nbsp;" . Yii::t('standard', 'Associated courses'),
));
*/

?>




