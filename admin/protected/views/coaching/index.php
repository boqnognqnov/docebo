<?php
	/* @var $this CoachingController */

	$this->breadcrumbs = array(
		Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
		Yii::t('standard', '_COURSES') => array('courseManagement/index'),
		Yii::t('standard', 'Enrollments') => Docebo::createAdminUrl('courseManagement/enrollment', array('id' => $this->idCourse)),
		Yii::t('coaching', 'Coaching management'),
	);


	$params = array(
		'course_id'	=> $this->idCourse,
	);
	$newSessionUrl = Docebo::createAdminUrl('coaching/axEditCoachingSession', $params);
    Docebo::createAdminUrl('coaching/axEditCoachingSession', array('course_id'	=> $this->idCourse,'idSession' => $data['idSession']));
    DatePickerHelper::registerAssets();

?>

<h3 class="title-bold back-button">
	<?php echo CHtml::link(Yii::t('standard', '_BACK'), Docebo::createAdminUrl('courseManagement/enrollment', array('id' => $this->idCourse))); ?>
	<span><?= $this->courseModel->name ?></span>
</h3>
<br>

<div class="main-actions clearfix">
	<ul class="clearfix">
		<li>
			<div>
				<a href="<?= $newSessionUrl ?>"
				    alt="<?= Yii::t('event_manager', 'New coaching session - lt') ?>"
					class="open-dialog new-node"
					data-dialog-id = "coaching-edit-session-dialog"
					data-dialog-class = "coaching-edit-session-dialog"
					data-dialog-title="<?= Yii::t('coaching', 'New coaching session'); ?>"
					>
					<span></span> <?= Yii::t('coaching', 'New coaching session') ?>
				</a>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>
			<p></p>
		</div>
	</div>
</div>

<?php
$gridId = 'coaching-sessions-managment';
$filter = new stdClass();
$filter->search_input = $search_input;
if(isset($show_available_only) && $show_available_only) {
    $filter->ignoreEnded = true;
    $filter->ignoreFull = true;
}
$filter->fromDate = $date_start;
$filter->toDate = $date_end;
$dataProvider = LearningCourse::coachingSessionsSqlDataProvider($this->idCourse,$filter);
$columns = array(
    array(
        'value' => '$data["idSession"]',
        'headerHtmlOptions' => array('style' => 'display:none'),
        'htmlOptions' => array('style' => 'display:none'),
    ),
    array(
        'name' => 'dates',
        'value' => array($this, 'renderTimeframe'),
        'type' => 'raw',
        'header' => Yii::t('coaching', 'Session dates'),
    ),
    array(
        'name' => 'coach',
        'value' => 'CHtml::encode(str_replace("/", "", $data["userid"]))',
        'type' => 'raw',
        'header' => Yii::t('coaching', 'Coach'),
    ),
    array(
        'name' => 'firstname',
        'value' => 'CHtml::encode($data["firstname"])',
        'type' => 'raw',
        'header' => Yii::t('standard', '_FIRSTNAME'),
    ),
    array(
        'name' => 'lastname',
        'value' => 'CHtml::encode($data["lastname"])',
        'type' => 'raw',
        'header' => Yii::t('standard', '_LASTNAME'),
    ),
    array(
        'name' => 'assigned',
        'value' => array($this, 'renderAssignColumn'),
        'type' => 'raw',
        'header' => Yii::t('standard', 'Assigned users'),
        'htmlOptions' => array(
			'class' => 'text-right'
        ),
        'headerHtmlOptions' => array(
            'class' => 'text-right'
        ),
    ),
		
	array(
		'type'	=> 'html',	
		'value'	=> array($this, 'renderOperationsColumn'),
		'htmlOptions' => array(
			'class' => 'text-right'
		)
	),	
);
$startHTML  = $this->widget(
    'common.widgets.DatePicker', array(
        'id' => 'date-start-container',
        'label' => Yii::t('report', 'Start date'),
        'fieldName' => 'date-start',
        'stackLabel' => false,
        //'value' => $date_start_local,
        'htmlOptions' => array(
            'id' => 'date-start',
            'class' => 'bdatepicker_coaching',
            'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')),
        ),
    ),TRUE
);
$endHTML    = $this->widget(
    'common.widgets.DatePicker', array(
        'id' => 'date-end-container',
        'label' => Yii::t('report', 'End date'),
        'fieldName' => 'date-end',
        'stackLabel' => false,
        //'value' => $date_end_local,
        'htmlOptions' => array(
            'id' => 'date-end',
            'class' => 'bdatepicker_coaching',
            'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')),
        ),
    ),TRUE
);
$checkShowAvailableHTML = CHtml::checkBox('show_available_only', false, array(
    'id' => 'show_available_only',
    'value' => true,
));
$checkShowAvailableHTML .= CHtml::label(Yii::t('coaching','Show available sessions only'), 'show_available_only',array(
    'id' => 'show_available_only_label',
));
$gridParams = array(
    'gridId' 				=> $gridId,
    'dataProvider'			=> $dataProvider,
    'columns'				=> $columns,
    'disableMassSelection' => true,
    'disableMassActions' => true,
    'customFilterGroups'	=> array(
        array(
            'groupClass'=>'show-available-only',
            'groupHtml' => $checkShowAvailableHTML,
        ),
        array(
            'groupClass'=>'show-from',
            'groupHtml'=>$startHTML,
        ),
        array(
            'groupClass'=>'show-to',
            'groupHtml'=>$endHTML,
        ),
    ),
    'autocompleteRoute'		=> '//Coaching/SessionsAutocomplete',
    'hiddenFields' => array(
            'course_id' => $this->idCourse,
            ),
);
$this->widget('common.widgets.ComboGridView', $gridParams);
?>
<div class="clearfix"></div>
<script type="text/javascript">
//<![CDATA[
	$(function () {
		// Create Coaching manager object. Make sure this happens only once!!!
		CoachingManager = new CoachingManagerClass();
        $("input#date-start,input#date-end,#show_available_only").on('change', function(){
            // Prepare the form data to be sent with gridView update
            var options = {
                data: $('#combo-grid-form').serialize()
            };

            // Do the yiiGridView update here
            $.fn.yiiGridView.update("coaching-sessions-managment", options);
        });
        $('#show_available_only').styler();
        $(document).on("dialog2.closed","#coaching-edit-session-dialog,#users-selector,#delete-certification-dialog", function(){
            // Prepare the form data to be sent with gridView update
            var options = {
                data: $('#combo-grid-form').serialize()
            };
            // Do the yiiGridView update here
            $.fn.yiiGridView.update("coaching-sessions-managment", options);
        });
	});
//]]>	
</script>

