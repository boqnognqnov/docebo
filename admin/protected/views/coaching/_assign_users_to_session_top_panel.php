<?php
	/* @var $this UsersSelectorController */
	/* @var $session LearningCourseCoachingSession */

	$session 		= LearningCourseCoachingSession::model()->findByPk($this->idGeneral);
	$start			= Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($session->datetime_start));
	$end 			= Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($session->datetime_end));
	$coachName		= $session->coach->lastname . ", " . $session->coach->firstname;
	$assignedCount 	= $session->countAssignedUsers();

?>
<div class="coaching-assign-users-top-panel">
	<div class="row-fluid">
		<div class="span12">
			
			<div class="row-fluid">
				<div class="span6">
					<strong><?= Yii::t('coaching', 'Coaching session') ?>:</strong>
				</div>
				<div class="span6 text-right">
					<?= Yii::t('coaching', 'Coach') ?>: <strong><?= $coachName ?></strong>
				</div>
			</div>
			 		
			<div class="row-fluid double-bordered-header comfort">
				<div class="span6">
					<?= Yii::t('report', 'Start date') ?>: <strong><?= $start ?></strong>
					&nbsp;&nbsp;
					<?= Yii::t('report', 'End date') ?>: <strong><?= $end ?></strong>
				</div>
				<div class="span6 text-right">
					<?= Yii::t('standard', 'Assigned users') ?>: <strong><?= $assignedCount ?></strong>
					&nbsp;&nbsp;
					<?= Yii::t('coaching', 'Max users') ?>: <strong><?= $session->max_users > 0 ? $session->max_users : Yii::t('standard', '_UNLIMITED') ?></strong>
				</div>				
								
			</div>
			
		</div>
	</div> 
	<br>
</div>
