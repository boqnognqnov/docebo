<?php if($idSession): ?>
    <?php
        $session = LearningCourseCoachingSession::model()->findByPk($idSession);
        $selectedCoach = $session->idCoach;
        $start_date = (!$start_date)?$session->datetime_start:$start_date;
        $end_date   = (!$end_date)?$session->datetime_end:$end_date;
    ?>
    <?php if($session->countAssignedUsers()>0): ?>
<div class="coaching-mode-selection-outer" style="display:none;">
    <div id="coaching-mode-selection-container">
        <div class="coaching-mode-radio-btn">
            <?php echo CHtml::radioButton("chooseMode", true, array(
                'id'    =>  'edit-Mode-radio-btn'
            )); ?>
            <?php echo CHtml::label(Yii::t('classroom','Edit session'), 'edit-Mode-radio-btn',array(
                'class'=>'coaching-inline-label'
            )); ?>
        </div>
        <div class="coaching-mode-radio-btn">
            <?php echo CHtml::radioButton("chooseMode", false, array(
                'id'    =>  'merge-mode-radio-btn'
            )); ?>
            <?php echo CHtml::label(Yii::t('coaching','Merge with another coaching session'), 'merge-mode-radio-btn',array(
                'class'=>'coaching-inline-label'
            )); ?>
        </div>
    </div>
</div>
    <?php endif; ?>
<?php endif; ?>
<?php $course = ($idCourse) ? LearningCourse::model()->findByPk($idCourse) : false;
    $startDateValid = ($course)?(isset($course->date_begin) && $course->date_begin!='0000-00-00'):false;
    $endDateValid = ($course)?(isset($course->date_end) && $course->date_end!='0000-00-00'):false;
    $DatePickerStartDate = ($startDateValid)? $course->date_begin : Yii::app()->localtime->toLocalDate();
    $DatePickerStartDate = (strtotime($DatePickerStartDate) > strtotime(Yii::app()->localtime->toLocalDate()))? $DatePickerStartDate : Yii::app()->localtime->toLocalDate();
    $DatePickerEndDate   = ($endDateValid)? $course->date_end : false;
if($startDateValid && $endDateValid): ?>
    <div id="coaching-session-edit-warning" class="row-fluid">
        <div id="exclamation-mark"><i class="fa fa-exclamation-circle"></i></div>
        <div id="warning-text"><?php echo Yii::t('coaching', 'Coaching session dates are limited due to course start/end date.<br /><strong>Start date:</strong> {start_date} <strong>End date:</strong> {end_date}',array(
            '{start_date}'  => $course->date_begin,
            '{end_date}'    => $course->date_end
        ));?></div>
    </div>
<?php endif; ?>
<?php

	echo CHtml::beginForm('', 'post', array(
		'id' 	=> 'coaching-edit-session-form',
		'name' 	=> 'coaching-edit-session-form',
		'class' => 'ajax',
	));
	
	echo CHtml::textField('dummy_field_for_date_picker_auto_show_bug','',array('style' => 'visibility: hidden;'));
	
?>

    <h6><?php echo Yii::t('coaching','Coaching session:'); ?></h6>

<div class="row-fluid filters-wrapper">

	<?php echo CHtml::hiddenField("LearningCourseCoachingSession[idCoach]", ($idSession)? $session->idCoach : ''); ?>

    <div class="coaching-session-filter-div coaching-session-to-left">
    	<?php 
    		$this->widget('common.widgets.DatePicker', array(
    			'id' => 'coaching-edit-date-start-container',
    			'label' => CHtml::activeLabel(LearningCourseCoachingSession::model(), 'start_date',array('id'=>'start-date-label')),
    			'fieldName' => CHtml::activeName(LearningCourseCoachingSession::model(), 'start_date'),
    			'value' => ($idSession)? Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($start_date)) : '',
                'iconClass' => 'p-sprite calendar-black-small date-icon',
    			'htmlOptions' => array(
    				'id' => CHtml::activeId(LearningCourseCoachingSession::model(), 'start_date'),
    				'class' => 'datepicker coaching-session-date',
    				'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')),
   					'data-date-language' => Yii::app()->getLanguage(),
                                'readonly' => 'readonly'
    			),
    			// set Yesterday as least start date in  date picker (substracting 1 Day from today)	
    			'pickerStartDate'	=> $DatePickerStartDate,
                'pickerEndDate'     => $DatePickerEndDate,
    		));
    	?>
    </div>

    <div class="coaching-session-filter-div coaching-session-to-left">
    	<?php 
    		$this->widget('common.widgets.DatePicker', array(
    			'id' => 'coaching-edit-date-end-container',
    			'label' => CHtml::activeLabel(LearningCourseCoachingSession::model(), 'end_date',array('id'=>'end-date-label')),
    			'fieldName' => CHtml::activeName(LearningCourseCoachingSession::model(), 'end_date'),
    			'value' => ($idSession) ? Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($end_date)) : '',
                'iconClass' => 'p-sprite calendar-black-small date-icon',
    			'htmlOptions' => array(
    				'id' => CHtml::activeId(LearningCourseCoachingSession::model(), 'end_date'),
    				'class' => 'datepicker coaching-session-date',
    				'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')),
   					'data-date-language' => Yii::app()->getLanguage(),
                            'readonly' => 'readonly'
    			),
    			// set Yesterday as least start date in  date picker (substracting 1 Day from today)	
    			'pickerStartDate'	=> $DatePickerStartDate,
                'pickerEndDate'     => $DatePickerEndDate,
    		));
    	?>
    </div>
    

    <div id="max-users-container" class="coaching-session-filter-div coaching-session-to-right">
        <div class="coaching-max-users-label">
            <div><?= Yii::t('coaching', 'Max users') ?></div>
            <div class="setting-description styler indented"><?= Yii::t('coaching', '0 for unlimited') ?></div>
        </div>
        <?php echo CHtml::textField(
            CHtml::activeName(LearningCourseCoachingSession::model(), 'max_users'),
            ($idSession) ? $session->max_users : '',
            array(
                'class' => 'coaching-max-users',
                'id' => CHtml::activeId(LearningCourseCoachingSession::model(), 'max_users'),
            )); ?>
    </div>
    
    <?php if($idSession): ?>
        <div id="assigned-users" class="coaching-session-filter-div coaching-session-to-right">
            <div class="coaching-max-users-label">
                <div class="coaching-inline-label"><?= Yii::t('standard', 'Assigned users') ?>:</div>
                <div class="coaching-inline-label"><strong><?= $session->countAssignedUsers(); ?></strong></div>
            </div>
        </div>
        <div id="currentSession-coach" class="coaching-session-filter-div coaching-session-to-right">
            <div class="coaching-max-users-label">
                <div class="coaching-inline-label"><?= Yii::t('coaching', 'Coach') ?>:</div>
                <div class="coaching-inline-label"><strong><?= ltrim(CoreUser::model()->findByPk($session->idCoach)->userid,"/"); ?></strong></div>
            </div>
        </div>
        
        <?php echo CHtml::hiddenField("targetSession", '', array('id' => 'targetSession')); ?>
        <?php echo CHtml::hiddenField("isMerging", '', array('id' => 'isMerging')); ?>
        
    <?php endif; ?>
</div>




<div class="form-actions">
    <?= CHtml::submitButton(Yii::t('standard', '_CONFIRM'), 		array('id'=>'coaching-session-managment-confirm','name'  => 'confirm_button','class' => 'confirm-save btn btn-docebo green big')); ?>
    <?= CHtml::button(		Yii::t('standard', '_CLOSE'), 			array('class' => 'btn btn-docebo black big close-dialog')); ?>
</div>

<?php echo CHtml::endForm(); ?>
<?php if (!$fromDelete): ?>
<div id="coaching-creation-edit-mode" style="display:none;">
    <h6><?php echo Yii::t('coaching','Available coaches for this session:'); ?></h6>
<?php
$gridId = 'coaches-managment';
$dataProvider = CoreUser::model()->sqlCoachesForCourses($idCourse,$search_input,$start_date,$end_date,$idSession);
$columns = array(
    array(
        'id' 	=> 'items-grid-checkboxes',
        'type' => 'raw',
        'value' => function($data) use ($selectedCoach) {
            $checkedCoachRadioBtn = ($data["idst"] == $selectedCoach) ? true : false;
            echo CHtml::radioButton("coachChoose", $checkedCoachRadioBtn, array('value'=>$data["idst"]));
        },
    ),
    array(
        'name' => 'name',
        'value' => 'ltrim($data["userid"],"/");',
        'type' => 'raw',
        'header' => Yii::t('standard', '_NAME')
    ),
    array(
        'name' => 'firstname',
        'value' => '$data["firstname"];',
        'type' => 'raw',
        'header' => Yii::t('standard', '_FIRSTNAME')
    ),
    array(
        'name' => 'lastname',
        'value' => '$data["lastname"];',
        'type' => 'raw',
        'header' => Yii::t('standard', '_LASTNAME')
    ),
    array(
        'name' => 'email',
        'value' => '$data["email"];',
        'type' => 'raw',
        'header' => Yii::t('standard', '_EMAIL')
    ),
);

$gridParams = array(
    'gridId' 				=> $gridId,
    'dataProvider'			=> $dataProvider,
    'columns'				=> $columns,
    'disableMassSelection' => true,
    'disableMassActions' => true,
    'autocompleteRoute'		=> '//Coaching/CoachesAutocomplete',
    'hiddenFields' => array(
        'course_id' => $idCourse,
        'idSession' => $idSession,
        'start_date' => '',
        'end_date' => ''
    ),
);
$this->widget('common.widgets.ComboGridView', $gridParams);
?>
</div>
<?php endif; ?>
<?php if($idSession && $session->countAssignedUsers()>0): ?>
<div id="coaching-merge-mode" style="display:none;">
    <h6><?php echo Yii::t('coaching','Merge {count} users with:',array('{count}'=>$session->countAssignedUsers())); ?></h6>
    <?php
    $gridId = 'coaching-sessions-merging';
    $filter = new stdClass();
    $filter->search_input = $search_input;
    $filter->minAvailableSeats = $session->countAssignedUsers();
    $filter->excludedSessions[] = $session->idSession;
    $dataProvider = LearningCourse::coachingSessionsSqlDataProvider($session->idCourse,$filter,5);
    $columns = array(
        array(
            'id' 	=> 'items-grid-checkboxes-merging',
            'type' => 'raw',
            'value' => function($data){
                echo CHtml::radioButton("sessionChoose", false, array('value'=>$data["idSession"]));
            },
        ),
        array(
            'name' => 'name',
            'value' => 'CHtml::encode(str_replace("/", "", $data["userid"]))',
            'type' => 'raw',
            'header' => Yii::t('standard', '_USERNAME'),
        ),
        array(
            'name' => 'firstname',
            'value' => 'CHtml::encode($data["firstname"])',
            'type' => 'raw',
            'header' => Yii::t('standard', '_FIRSTNAME'),
        ),
        array(
            'name' => 'lastname',
            'value' => 'CHtml::encode($data["lastname"])',
            'type' => 'raw',
            'header' => Yii::t('standard', '_LASTNAME'),
        ),
        array(
            'name' => 'dates',
            'value' => 'Yii::t("standard", "_FROM") . " " . Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($data["datetime_start"])) . " " . Yii::t("standard", "_TO") . "  " . Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($data["datetime_end"]))',
            'type' => 'raw',
            'header' => Yii::t('coaching', 'Session dates'),
        ),
        array(
            'name' => 'assigned',
            'value' => array($this, 'renderAssignColumn'),
            'type' => 'raw',
            'header' => Yii::t('standard', 'Assigned users')
        ),
    );

    $gridParams = array(
        'gridId' 				=> $gridId,
        'dataProvider'			=> $dataProvider,
        'columns'				=> $columns,
        'disableMassSelection' => true,
        'disableMassActions' => true,

        'autocompleteRoute'		=> '//Coaching/SessionsAutocomplete',
        'hiddenFields' => array(
            'idSession' => $idSession,
            'fromDelete'=> (($fromDelete) ? true : false)
        ),
    );
    $this->widget('common.widgets.ComboGridView', $gridParams);
    ?>
</div>
<?php endif; ?>

<script type="text/javascript">
//<![CDATA[
	$(function(){
    	CoachingManager.editCoachingSessionDialogReady(<?= json_encode( ($fromDelete) ? true : false ) ?>);
	});
//]]>        
</script>
