<?php
/* @var $coachName string */
/* @var $session LearningCourseCoachingSession */
/* @var $startDate string */
/* @var $endDate string */
/* @var $assignedCount string */
/* @var $deletionMode string */
?>

<?= CHtml::beginForm('', 'POST', array('class'	=> 'ajax')); ?>
<?= CHtml::hiddenField('idSession', $session->idSession) ?>

<?php if($assignedCount > 0): ?>
	<?= Yii::t('coaching', '<strong>{name}</strong>\'s coaching session (from <strong>{from}</strong> to <strong>{to}</strong>) has <strong>{number} users</strong> assigned', array(
		'{name}' 	=> $coachName,
		'{from}'	=> $startDate,
		'{to}'		=> $endDate,
		'{number}'	=> $assignedCount
	));?>
	<div class="coaching-mode-radio-btn" style="margin-top: 10px;">
		<?php echo CHtml::radioButton("deletionMode", $deletionMode == 'merge', array(
            'id'    => 'goto-merge-radio-btn',
			'class' => 'coaching-delete-radio',
			'value'	=> 'merge'
		)); ?>
		<?php echo CHtml::label(Yii::t('coaching','Merge <strong>{count} users</strong> with another coaching session and delete the old one',array('{count}'=>$assignedCount)), 'goto-merge-radio-btn',array(
			'class'=>'coaching-inline-label'
		)); ?>
	</div>
	<div class="coaching-mode-radio-btn">
		<?php echo CHtml::radioButton("deletionMode", $deletionMode == 'delete', array(
            'id'    => 'just-delete-radio-btn',
			'class' =>  'coaching-delete-radio',
			'value' => 'delete'
		)); ?>
		<?php echo CHtml::label(Yii::t('coaching','Just delete the session and <strong>unassign {count} users</strong>',array('{count}'=>$assignedCount)), 'just-delete-radio-btn',array(
			'class'=>'coaching-inline-label'
		)); ?>
	</div>
<?php else: ?>
	<p><?= Yii::t('coaching', 'You are about to delete <strong>{name}</strong>\'s coaching session (from <strong>{from}</strong> to <strong>{to}</strong>).', array(
	'{name}' 	=> $coachName,
	'{from}'	=> $startDate,
	'{to}'		=> $endDate
	)); ?></p>
	<?= CHtml::hiddenField('deletionMode', 'delete')?>
<?php endif; ?>

	<div id="proceed-check" class="coaching-mode-radio-btn" <?php if($deletionMode !== 'delete'): ?>style="display:none;"<?php endif; ?>>
		<?php echo CHtml::checkBox("agree_deletion", false, array(
			'id'    =>  'proceed-check-btn',
			'class' =>  'coaching-delete-check '. ($assignedCount == 0 ? 'no-users-assigned' : ''),
		)); ?>
		<?php echo CHtml::label(Yii::t('standard','Yes, I confirm I want to proceed'), 'proceed-check-btn',array('class'=>'coaching-inline-label')); ?>
	</div>

 	<!-- For Dialog2 - they go in footer -->
 	<div class="form-actions">
 		<?= CHtml::submitButton(Yii::t('standard', ($deletionMode == 'delete' ? '_CONFIRM' : '_NEXT')), array(
			'name'  => 'confirm_button',
			'class' => 'coaching-session-delete-confirm btn-docebo green big '. ($deletionMode == 'delete' ? 'disabled' : ''))
		); ?>
        <?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' 	=> 'btn-docebo black big close-dialog')); ?>
 	</div>
<?php echo CHtml::endForm(); ?>

<script type="text/javascript">
    $(function(){
        $('.coaching-delete-radio,.coaching-delete-check').styler();
        $('.coaching-delete-modal .coaching-delete-radio').on('change',function() {
			var selectedValue = $(this).val();
            if (selectedValue == 'merge') {
                $('#proceed-check-btn').removeAttr('checked');
                $('#proceed-check').hide();
				$('.coaching-session-delete-confirm').removeClass('disabled');
                $('.coaching-session-delete-confirm').text("<?php echo Yii::t('standard', '_NEXT') ?>");
            } else {
				$('#proceed-check').show();
				$('.coaching-session-delete-confirm').addClass('disabled');
				$('.btn.btn-docebo.green.big').text("<?php echo Yii::t('standard', '_CONFIRM') ?>");
			}
        });

		$('#proceed-check-btn').on('change', function() {
			if($(this).is(':checked'))
				$('.coaching-session-delete-confirm').removeClass('disabled');
			else
				$('.coaching-session-delete-confirm').addClass('disabled');
		});

		$('.coaching-session-delete-confirm').on('click', function() {
			if($(this).hasClass('disabled'))
				return false;
		});
    });
</script>