<div class="enroll-success">
    <div class="copy-success"><div class="copy-success-icon"></div> <?php echo Yii::t('standard', '_OPERATION_SUCCESSFUL'); ?></div>

	<div class="stats">
		<p><?php echo Yii::t('standard', '_USERS'); ?>: <strong><?php echo $result['imported']; ?></strong></p>
		<p><?php echo Yii::t('standard', 'Skipped'); ?>: <strong><?php echo $result['skipped']; ?></strong></p>
		<p>Users not found: <strong><?php echo $result['notFound']; ?></strong></p>
	</div>

	<?php echo CHtml::button('Ok', array('class' => 'btn ok', 'data-dismiss' => 'modal')); ?>
</div>