<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'group_form',
	)); ?>

	<p><?php echo Yii::t('organization_chart', '_GROUPUSER_groupid').': "'.$model->relativeId($model->groupid).'"'; ?></p>

	<div class="clearfix">
		<?php echo $form->checkbox($model, 'confirm', array('id' => 'CoreGroup_confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed', array($model->relativeId($model->groupid))), 'CoreGroup_confirm_'.$checkboxIdKey); ?>
		<?php echo $form->error($model, 'confirm'); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>