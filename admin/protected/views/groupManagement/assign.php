<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', '_GROUPS') => Yii::app()->createUrl('groupManagement/index'),
	CoreGroup::model()->relativeId($groupModel->groupid),
	Yii::t('standard', '_ASSIGN_USERS'),
); ?>

<?php //$this->renderPartial('//common/_mainGroupMemberActions', array('id' => $groupModel->idst)); ?>
<?php $this->renderPartial('_mainAssignActions', array('id' => $groupModel->idst)); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'group-member-management-form',
	'htmlOptions' => array(
		'class' => 'ajax-list-form',
		'data-grid' => '#group-member-management-list'
	),
)); ?>

<div class="main-section group-management">
	<div class="filters-wrapper">
		<div class="filters clearfix">
			<div class="input-wrapper">
				<?php echo $form->textField($groupMemberModel->users, 'search_input', array(
					'id' => 'advanced-search-group',
					'class' => 'typeahead',
					'autocomplete' => 'off',
					'data-url' => Docebo::createAppUrl('admin:userManagement/usersAutocomplete'),
					'data-group' => $groupMemberModel->idst,
					'placeholder' => Yii::t('standard', '_SEARCH'),
					'data-source-desc' => 'true',
				)); ?>
				<span class="search-icon"></span>
			</div>
		</div>
	</div>
</div>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<?php if (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/groupmanagement/mod')): ?>
<div class="selections clearfix">
	<?php $this->widget('ext.local.widgets.ListViewSelectAll', array(
		'gridId' => 'group-member-management-list',
		'class' => 'left-selections clearfix',
		'dataProvider' => $groupMemberModel->sqlDataProvider(),
		'itemValue' => 'idstMember',
	)); ?>

	<div class="right-selections clearfix">
		<select name="massive_action" id="group-member-management-massive-action">
			<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
			<option value="delete"><?php echo Yii::t('standard', '_DEL'); ?></option>
		</select>
	</div>
</div>
<?php endif; ?>

<div class="bottom-section">
	<div id="grid-wrapper">
		<?php $this->widget('ext.local.widgets.CXListView', array(
				'id' => 'group-member-management-list',
				'htmlOptions' => array('class' => 'list-view clearfix'),
				'dataProvider' => $groupMemberModel->sqlDataProvider(),
				'itemView' => '_viewSql',
				'itemsCssClass' => 'items clearfix',
				'summaryText' => Yii::t('standard', '_TOTAL'),
				'pager' => array(
					'class' => 'common.components.DoceboCLinkPager',
				),
				'template' => '{items}{pager}{summary}',
				'ajaxUpdate' => 'group-member-management-list-all-items',
				'beforeAjaxUpdate' => 'function(id, options) {
					options.type = "POST";
					options.data = $("#grid-filter-form").serialize();
					options.data += "&selectedItems=" + $(\'#\'+id+\'-selected-items-list\').val();
				}',
				'afterAjaxUpdate' => 'function(id, data) { $("#group-member-management-list input").styler();updateSelectedCheckboxesCounter("group-member-management-list"); $.fn.updateListViewSelectPage();}',
		)); ?>
	</div>
</div>

<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input, select').styler();
			replacePlaceholder();
		});
	})(jQuery);
</script>
