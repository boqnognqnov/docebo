<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'group_form',
		'htmlOptions' => array(
			'enctype' => 'multipart/form-data',
		),
	)); ?>

	<p><?php echo Yii::t('organization', 'UPLOAD'); ?></p>

	<div>
		<?php echo CHtml::fileField('ImportCsv', ''); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
	$('input').styler();
</script>