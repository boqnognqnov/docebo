<a class="auto-close"></a>
<script type='text/javascript'>
	$(function(){
		$.post('<?=Docebo::createLmsUrl('progress/run')?>', {
			'type': <?=Progress::JOBTYPE_AUTO_ASSIGN_USERS_TO_SINGLE_GROUP; ?>,
			'users': '<?= $users ?>',
			'group': '<?= $group ?>',
            'customDialogClass': 'new-group'
		}, function(data){
			var dialogId = 'auto-assugn-users-to-single-group';

			$('<div/>').dialog2({
				autoOpen: true, // We will open the dialog later
				id: dialogId,
				title: '<?=Yii::t('group_management', 'Assigning existing users')?>...'
			});
			$('#'+dialogId).html(data);
		});
	})
</script>
