<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'user_form',
	));	?>

	<p><?php echo Yii::t('standard', '_UNASSIGN').': "'.count($users).' '.Yii::t('standard', '_USERS').'"'; ?></p>

	<div class="clearfix">
		<?php echo $form->checkbox($model, 'confirm', array('id' => 'LearningCourseuser_confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed', array(count($users), '{username}' => Yii::app()->user->getRelativeUsername($users[0]->userid))), 'LearningCourseuser_confirm_'.$checkboxIdKey); ?>
		<?php echo $form->error($model, 'confirm'); ?>
	</div>

	<?php echo CHtml::hiddenField('idst', (is_array($idst) ? implode(',', $idst) : $idst)); ?>
	<?php $this->endWidget(); ?>
</div>