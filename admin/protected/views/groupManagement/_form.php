<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'group_form',
	)); ?>

		<?php echo $form->labelEx($model, 'groupid'); ?>
		<?php echo $form->textField($model, 'groupid', array('style'=>'width: 390px;')); ?>
		<?php echo $form->error($model, 'groupid'); ?>

		<?php echo $form->labelEx($model, 'description'); ?>
		<?php echo $form->textArea($model, 'description', array('style'=>'width: 390px;')); ?>
		<?php echo $form->error($model, 'description'); ?>

	<?php $this->endWidget(); ?>
</div>
<?php echo CHtml::link('process', Yii::app()->createUrl('groupManagement/processNewGroup'), array(
    'id' => 'process-new-group',
    'class' => 'new-group-success popup-handler',
    'data-modal-title' => $model->isNewRecord ? Yii::t('standard', '_CREATE') : Yii::t('standard', '_MOD'),
    'data-modal-class' => 'new-group-success',
    'style' => 'display:none;',
)); ?>