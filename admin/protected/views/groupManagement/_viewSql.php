<?php
//echo '<pre>'.print_r($data,true).'</pre>';die;
$canModGroup = Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/groupmanagement/mod');
?>
<div class="item  <?php echo (($index % 2) ? 'odd' : 'even'); ?>">
	<div class="checkbox-column">
		<?php
		if ($canModGroup) {
			echo '<input id="group-member-management-list-checkboxes_'. $data['idstMember'] .'" type="checkbox" name="group-member-management-list-checkboxes[]" value="'.$data['idstMember'].'" '.((CoreGroupMembers::inSelectList('selectedItems', $data['idstMember']))?'checked="checked"':'').'>';
		}
		?>
	</div>
	<div class="group-member-username"><?php echo CoreUser::getFullNameById($data['idstMember'])?> (<?php echo Yii::app()->user->getRelativeUsername($data['userid']);?>)</div>
	<div class="delete-button">
		<?php
		//if (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/groupmanagement/associate_user')) {
		if ($canModGroup) {
			$this->renderPartial('//common/_modal', array(
				'config' => array(
					'class' => 'delete-node',
					'modalTitle' => Yii::t('standard', '_UNASSIGN'),
					'linkTitle' => Yii::t('standard', '_DEL'),
					'url' => 'groupManagement/deleteGroupMember&idst='. $data['idstMember'],
					'buttons' => array(
						array(
							'type' => 'submit',
							'title' => Yii::t('standard', '_CONFIRM'),
						),
						array(
							'type' => 'cancel',
							'title' => Yii::t('standard', '_CANCEL'),
						),
					),
					'afterLoadingContent' => 'hideConfirmButton();',
					'afterSubmit' => 'updateGroupMemberContent',
				),
			));
		}
		?>
	</div>
</div>