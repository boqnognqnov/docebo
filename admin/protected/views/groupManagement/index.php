<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', '_GROUPS'),
); ?>

<?php $this->renderPartial('//common/_mainGroupActions'); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'group-management-form',
	'htmlOptions' => array(
		'class' => 'ajax-grid-form',
		'data-grid' => '#group-management-grid'
	),
)); ?>

<div class="main-section group-management">

	<div class="filters-wrapper">
		<div class="filters clearfix">
			<div class="input-wrapper">
				<?php echo $form->textField($groupModel, 'groupid', array(
					'id' => 'advanced-search-group',
					'class' => 'typeahead',
					'autocomplete' => 'off',
					'data-url' => Docebo::createAppUrl('admin:groupManagement/groupAutocomplete'),
					'placeholder' => Yii::t('standard', '_SEARCH'),
				)); ?>
				<span>
					<button class="search-btn-icon"></button>
				</span>
			</div>
		</div>
	</div>
</div>
<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<div class="bottom-section clearfix">
	<div id="grid-wrapper">
		<?php

			$_columns = array(
				array(
					'name' => 'groupid',
					'type' => 'raw',
					'value' => '$data->relativeId($data->groupid)',
				),
				'description');
			if(Yii::app()->user->getIsGodadmin())
				$_columns[] = array(
					'type' => 'raw',
					'header' => Yii::t('group_management', 'Users Assignment'),
					'value' => '$data->renderGroupAssigendRules()',
					'headerHtmlOptions' => array('class' => 'center-aligned'),
					'htmlOptions' => array('class' => 'center-aligned'),
				);
			$_columns[] = array(
					'header' => '',
					'type' => 'raw',
					'value' => '(empty($data->coreGroupMembers)?\'<span class="exclamation"></span>\':\'\');',
						'htmlOptions' => array('class' => 'single-action'),
				);

			$_columns[] = array(
				'header' => '<span class="count-group-members"></span>',
				'type' => 'raw',
				'value' => '$data->renderGroupMembers(true);',
				'htmlOptions' => array('class' => 'group-members'),
			);

			if (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/groupmanagement/mod')) {
				$_columns[] = array(
					'header' => '',
					'type' => 'raw',
					'value' => '$data->renderEditGroupButton();',
					'htmlOptions' => array('class' => 'button-column-single')
				);
			}

			if (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/groupmanagement/del')) {
				$_columns[] = array(
					'name' => '',
					'type' => 'raw',
					'value' => 'CHtml::link("", "", array(
						"class" => "ajaxModal delete-action",
						"data-toggle" => "modal",
						"data-modal-class" => "delete-node",
						"data-modal-title" => "'.Yii::t('standard', '_DEL').'",
						"data-buttons" => \'[
							{"type": "submit", "title": "'.Yii::t('standard', '_CONFIRM').'"},
							{"type": "cancel", "title": "'.Yii::t('standard', '_CANCEL').'"}
						]\',
						"data-url" => "groupManagement/deleteGroup&idst=$data->idst",
						"data-after-loading-content" => "hideConfirmButton();",
						"data-after-submit" => "updateGroupContent",
						"rel" => "tooltip",
						"title" => Yii::t("standard", "_DEL")
					));',
					'htmlOptions' => array('class' => 'button-column-single')
				);
			}

			$gridOptions = array(
				'id' => 'group-management-grid',
				'htmlOptions' => array('class' => 'grid-view clearfix'),
				'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
				'dataProvider' => $groupModel->dataProvider(),
				'template' => '{items}{summary}{pager}',
				'summaryText' => Yii::t('standard', '_TOTAL'),
				'pager' => array(
					'class' => 'DoceboCLinkPager',
					'maxButtonCount' => 8,
				),
				'ajaxUpdate' => 'all-items',
				'columns' => $_columns,
				'afterAjaxUpdate' => 'function(id, data){ $(\'a[rel="tooltip"]\').tooltip(); $(document).controls()}',
			);

			$this->widget('zii.widgets.grid.CGridView', $gridOptions);
		?>
	</div>
</div>

<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input').styler();
			replacePlaceholder();

			// Create Group Assign Rules manager. Make sure this happens only once!!!
			GroupAssignRulesManager = new GroupAssignRulesManagerClass();

		});
	})(jQuery);
</script>
