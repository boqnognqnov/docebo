<script type='text/javascript'>
	$(function(){
		$.post('<?=Docebo::createLmsUrl('progress/run')?>', {
			'type': <?=Progress::JOBTYPE_UNASSIGN_USERS_FROM_GROUP?>,
			'users': '<?= (is_array($users) ? implode(',', $users) : $users) ?>',
			'group': '<?= $group ?>',
		}, function(data){
			removeModal();
			var dialogId = 'unassign-users-progressive';

			$('<div/>').dialog2({
				autoOpen: true, // We will open the dialog later
				id: dialogId,
				title: '<?php echo Yii::t('standard', '_DEL_SELECTED')?>'
			});
			$('#'+dialogId).html(data);
		});
	});
</script>