<? if($stop && !empty($failedUsersFullNames)): ?>
	<p class="alert alert-error">
	<? echo Yii::t('standard', 'Can not unenroll users who have already used their seat').': '.implode(', ', $failedUsersFullNames); ?>
	</p>
<? endif; ?>

<div class="row-fluid">
	<div class="span12">

		<div class="pull-left sub-title" >
			<b><?=Yii::t('group_management', 'Applying rules to {x} users', array('{x}' => $totalUsers))?></b>
		</div>
		<div class="pull-right percentage-gauge green-text"><?=$completedPercent?>%</div>
	</div>
</div>
<div class="row-fluid">
	<div class="span12">
		<div class="docebo-progress progress progress-success">
			<div style="width: <?=$completedPercent?>%;" class="bar full-height" id="uploader-progress-bar"></div>
		</div>
	</div>
    <?php if($stop): ?>
        <div class="span12 text-success">
            <?=Yii::t('group_management', 'Done processing users');?>.
        </div>
    <?php endif; ?>
</div>
<? if($stop): ?>	    
<div class="form">
    <?php
        echo CHtml::beginForm('index.php?r=groupManagement/processGroupAfterAutoAssign', 'post', array(
            'id' => 'form-after-auto-assign',
            'class' => 'ajax',
            'data-dialog-class' => 'new-group'
        ));
    ?>
	<div class="form-actions">		
        <?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' 	=> 'btn-docebo black big close-dialog')); ?>
        <?= CHtml::submitButton(Yii::t('standard', '_NEXT'), array('class' 	=> 'confirm-save btn-docebo green big', 'name' => 'next')); ?>
	</div>
</div>
<?php CHtml::endForm(); ?>
<? else: ?>
<div class="form-actions">
    <?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' 	=> 'btn-docebo black big close-dialog')); ?>
</div>
<? endif; ?>
<? if($stop && isset($afterUpdateCallback) && $afterUpdateCallback): ?>
	<script type="text/javascript">
		$(function(){
			<?=$afterUpdateCallback?>
		});
	</script>
<? endif; ?>
