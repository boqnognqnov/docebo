<div class="main-actions clearfix">
	<h3 class="title-bold back-button">
		<?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('groupManagement/index')); ?>
		<span><?php echo Yii::t('standard', '_ASSIGN_USERS'); ?></span>
	</h3>
	
	
	<ul class="clearfix">
		<?php if (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/groupmanagement/mod')): ?>
		<li>
			<div>
				<?php
					$title 	= '<span></span>'.Yii::t('standard', '_ASSIGN_USERS'); 
					$url 	= Docebo::createLmsUrl('usersSelector/axOpen', array(
								'type' 	=> UsersSelector::TYPE_GROUPS_MANAGEMENT,
								'idGroup' 	=> $id,
							));
					
					echo CHtml::link($title, $url, array(
						'class' => 'open-dialog group-select-users',
						'rel' 	=> 'users-selector',	
						'alt' 	=> Yii::t('helper', 'Group assign users'),
						'data-dialog-title' => Yii::t('standard', '_ASSIGN_USERS'),
						'title'	=> Yii::t('standard', '_ASSIGN_USERS')
					)); 
				?>
			</div>
		</li>
		<?php endif; ?>
	</ul>
	<div class="info">
		<div>
			<h4></h4>
			<p></p>
		</div>
	</div>
	
</div>


<script>

	/**
	 * The name of this function is hardcoded in UsersSelector JS as a callback upon successfully closing the dialog
	 *  Use in on your discretion to update current UI
	 */
	function usersSelectorSuccessCallback(data) {
		$.fn.yiiListView.update('group-member-management-list');
	}
			
</script>
