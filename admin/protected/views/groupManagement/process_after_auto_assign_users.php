<div class="clearfix row-fluid">
	<div class="new-group-success span3">
		<div class="copy-success-icon"></div>
		<?php echo Yii::t('standard', '_OPERATION_SUCCESSFUL'); ?>
	</div>
	<div class="new-group-success-content span9">
		<h3><?php echo Yii::t('standard', 'What do you want to do next?'); ?></h3>

		<?php //if (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/groupmanagement/associate_user')): ?>
		<?php if (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/groupmanagement/mod')): ?>
		<div class="span12 new-course-info-row">
			<div class="span8">
				<span><?php echo Yii::t('group_management', 'I want to <strong>create another group</strong>'); ?></span>
			</div>
			<div class="span4">                
				<?php                
					echo CHtml::link(Yii::t('standard', '_CREATE'), $this->createUrl('groupManagement/createGroup'), array(
                            'class' => 'btn btn-submit ajax',
                            'id' => 'new-group_button',
                            'data-dialog-class' => 'new-group',                            
                        ));
				?>                
			</div>
		</div>
        <div class="span12 new-course-info-row">
			<div class="span7">
				<span><?php echo Yii::t('group_management', 'Done, <strong>I want to go back</strong>'); ?></span>
			</div>
			<div class="span5">
				<?php                
					echo CHtml::link(Yii::t('group_management', 'Save and go back'), Docebo::createAppUrl('admin:groupManagement/index'), array('class' => 'btn btn-save'));
				?>
			</div>
		</div>
		<?php endif; ?>        		
	</div>
    <div class="form-actions">
        <?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' 	=> 'btn-docebo grey big close-dialog')); ?>
    </div>
</div>
<script type="text/javascript">
	$(function(){        
		$('#process-new-group').remove(); //because if other closes - it always shows...        
        $.fn.yiiGridView.update('group-management-grid');
	})        
</script>
