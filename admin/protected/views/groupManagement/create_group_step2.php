<div class="form">
	<?php
    echo CHtml::beginForm('', 'post', array(
        'id' => 'group_form',
        'class' => 'ajax'
    ));
    ?>
    <div class="row-fluid">        
            <div class="form-group">
                <div class="span6 text-center">
                    <label for="sel1"><?=Yii::t('group_management', 'Automatically assign users to this group if');?>:</label>
                </div>
                <div class="span6">
                    <?=CHtml::dropDownList('name', '', $fields, array(
                        'id' => 'field_select',
                        'empty' => '---'
                        ));?>
                </div>
            </div>
    </div>
    <hr/>
    <div id="assign-rules" class="row-fluid assign-rules">
        <?php                                 
            if(is_array($ruleModels) && !empty($ruleModels)){
                foreach ($ruleModels as $id => $rule){
                    echo $rule->renderSingleRule($id);                    
                }
            }
        ?>
        <div id="empty-rules" class="span12 text-center" style="display: <?= !empty($ruleModels) ? 'none' : 'block'?>;">
            <i><?= Yii::t('group_management', 'Please add a condition by selecting one or more user field\'s above'); ?></i>
        </div>
    </div>
    <div id="rules-options" style="display:<?= (!empty($ruleModels)) ? "block" : "none"?> ">
        <br><br>            
         <?=CHtml::radioButtonList('CoreGroup[assign_rules_logic_operator]', $modelGroup->assign_rules_logic_operator,  array(
            CoreGroupAssignRules::LOGIC_OPERATOR_AND => Yii::t('group_management', 'All the conditions must be satisfied'),
            CoreGroupAssignRules::LOGIC_OPERATOR_OR => Yii::t('group_management', 'At least one condition must be satisfied')
        ), array(
            'labelOptions'=>array('style'=>'display:inline'), // add this code
            'separator'=>'  '
        ))?>
        <br><br>
        <label class="checkbox row-fluid" style="padding-left: 26px;">
            <?php
                echo CHtml::checkBox('apply_now');
                echo Yii::t('group_management', 'Apply also to existing users that satisfy the conditions above');
            ?>
            
        </label>
    </div>
    <hr/>
    <?php       
        echo CHtml::hiddenField('CoreGroup[groupid]', $modelGroup->groupid);
        echo CHtml::hiddenField('CoreGroup[description]', $modelGroup->description);
        echo CHtml::hiddenField('fromStep', 2);
        echo CHtml::hiddenField('CoreGroup[assign_rules]', 1);
        echo (!empty($nextId)) ? CHtml::hiddenField('next_id', $nextId) : "";
    ?>
    <div class="form-actions">
        <!-- No SUBMIT BUTTON here! We handle 'clicks' and do AJAX form submition -->
        <?= (empty($edit)) ? CHtml::submitButton(Yii::t('standard', '_PREV'), array('class' 	=> 'btn-docebo green big', 'name' => 'prev')) : ""; ?>
        <?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' 	=> 'btn-docebo black big close-dialog')); ?>
        <?= CHtml::submitButton(Yii::t('standard', '_NEXT'), array('class' 	=> 'confirm-save btn-docebo green big', 'name' => 'next', 'id' => 'button_step2')); ?>
    </div>
	<?php CHtml::endForm(); ?>    
</div>


<script type="text/javascript">
    $('form input').styler();
    $(function(){
        if($('#empty-rules').length === 0){
            $('#rules-options').show();
        }
    });
</script>
