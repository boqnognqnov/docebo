<h1><?= $model->isNewRecord ? Yii::t('group_management', 'Create group') : Yii::t('standard', '_MOD');?></h1>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'group_form',
        'htmlOptions' => array('class' => 'ajax', 'data-grid' => '#fields-grid')
	)); ?>

		<?php echo $form->labelEx($model, 'groupid'); ?>
		<?php echo $form->textField($model, 'groupid'); ?>
		<?php echo $form->error($model, 'groupid'); ?>

		<?php echo $form->labelEx($model, 'description'); ?>
		<?php echo $form->textArea($model, 'description', array('style'=>'padding: 8px;')); ?>
		<?php echo $form->error($model, 'description'); ?>
    <br><br>
	<?php if(Yii::app()->user->getIsGodadmin()) : ?>
        <?=  CHtml::radioButtonList('CoreGroup[assign_rules]', $model->assign_rules, array(
            0 => Yii::t('group_management', 'Manually assign users to this group'),
            1 => Yii::t('group_management', 'Define rules to automatically assign users to this group')
        ), array(
            'labelOptions' => array('style' => 'display: inline;')
        ))?>
	<?php else : ?>
		<?=CHtml::hiddenField('group_management', $model->assign_rules)?>
	<?php endif; ?>

        <?=CHtml::hiddenField('fromStep', 1)?>
        <div class="form-actions">
            <!-- No SUBMIT BUTTON here! We handle 'clicks' and do AJAX form submition -->
            <?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' 	=> 'btn-docebo black big close-dialog')); ?>
            <?= CHtml::submitButton(Yii::t('standard', '_NEXT'), array('class' 	=> 'confirm-save btn-docebo green big', 'name' => 'next')); ?>
        </div>
	<?php $this->endWidget(); ?>
</div>
<?php echo CHtml::link('process', Yii::app()->createUrl('groupManagement/processNewGroup'), array(
    'id' => 'process-new-group',
    'class' => 'new-group-success popup-handler',
    'data-modal-title' => $model->isNewRecord ? Yii::t('standard', '_CREATE') : Yii::t('standard', '_MOD'),
    'data-modal-class' => 'new-group-success',
    'style' => 'display:none;',
)); ?>

<script type="text/javascript">
    $('form#group_form input').styler();
</script>
