<?php

date_default_timezone_set('Asia/Hong_Kong');
$hk_ts = strtotime('now');
$hk = date('Y-m-d H:i:s');
$hk_tz = date('c');

date_default_timezone_set('Europe/Rome');
$rome_ts = strtotime('now');
$rome = date('Y-m-d H:i:s');
$rome_tz = date('c');

date_default_timezone_set('UTC');
$utc_ts = strtotime('now');
$utc = date('Y-m-d H:i:s');
$utc_tz = date('c');

date_default_timezone_set('US/Eastern');
$use_ts = strtotime('now');
$use = date('Y-m-d H:i:s');
$use_tz = date('c');

date_default_timezone_set('US/Pacific');
$usp_ts = strtotime('now');
$usp = date('Y-m-d H:i:s');
$usp_tz = date('c');

?>
<table class="table table-striped table-bordered">
	<thead>
	<tr>
		<th>Timezone</th>
		<th>Timestamp</th>
		<th>Datatime</th>
		<th>Datatime</th>
	</tr>
	</thead>
	<tr>
		<th>Asia/Hong_Kong</th>
		<td><?= $hk_ts;?></td>
		<td><?= $hk;?></td>
		<td><?= $hk_tz;?></td>
	</tr>
	<tr>
		<th>Europe/Rome</th>
		<td><?= $rome_ts;?></td>
		<td><?= $rome;?></td>
		<td><?= $rome_tz;?></td>
	</tr>
	<tr>
		<th>UTC</th>
		<td><?= $utc_ts;?></td>
		<td><?= $utc;?></td>
		<td><?= $utc_tz;?></td>
	</tr>
	<tr>
		<th>US/Eastern</th>
		<td><?= $use_ts;?></td>
		<td><?= $use;?></td>
		<td><?= $use_tz;?></td>
	</tr>
	<tr>
		<th>US/Pacific</th>
		<td><?= $usp_ts;?></td>
		<td><?= $usp;?></td>
		<td><?= $usp_tz;?></td>
	</tr>
</table>