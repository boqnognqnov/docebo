<div class="row">
	<div class="span12">
		<div class="hero">
			<?php echo 'id:' . Yii::app()->user->id; ?>
		</div>
		<table class="table table-hover">
			<thead>
			<tr>
				<th>Attività</th>
				<th>View</th>
				<th>Edit</th>
				<th>Delete</th>
				<th>Moderate</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>User</td>
				<td><?php echo Yii::app()->user->checkAccess('/framework/admin/usermanagement/view'); ?></td>
				<td><?php echo Yii::app()->user->checkAccess('/framework/admin/usermanagement/mod'); ?></td>
				<td><?php echo Yii::app()->user->checkAccess('/framework/admin/usermanagement/del'); ?></td>
				<td>na</td>
			</tr>
			</tbody>
		</table>

	</div>
</div>
