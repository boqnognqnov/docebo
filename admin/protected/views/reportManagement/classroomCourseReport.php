<?php
/* @var $this ReportManagementController */
/* @var $pieChartData array */
/* @var $singlePieChart string */
/* @var $course LearningCourse */
/* @var $session LtCourseSession */

$userIsSubscribed = LearningCourseuser::isSubscribed(Yii::app()->user->id, $course->idCourse);
if(!$userIsSubscribed) {
	$this->breadcrumbs[Yii::t('menu', 'Admin')] = Docebo::createAppUrl('admin:dashboard/index');
	$this->breadcrumbs[Yii::t('standard', '_COURSES')] = Docebo::createAppUrl('admin:courseManagement/index');
	$this->breadcrumbs[Docebo::ellipsis(Yii::t('player', $course->name), 60, '...')] = Docebo::createLmsUrl('player', array('course_id' => $course->idCourse));
} else {
	$this->breadcrumbs = array();
	$this->breadcrumbs[Yii::t('menu_over', '_MYCOURSES')] = Docebo::createLmsUrl('site/index');
	$this->breadcrumbs[] = array(
		'label' => Docebo::ellipsis(Yii::t('player', $course->name), 60, '...'),
		'url' => Docebo::createLmsUrl('player', array('course_id' => $course->idCourse))
	);
}
$this->breadcrumbs[] = Yii::t('standard', '_SESSION_STATISTICS');

Yii::app()->clientScript->scriptMap['jquery.js'] = false;
Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;


?>
<br/>
<br/>
<br/>

<div class="printable classroom-course-report-container">
<div class="clearfix">
    <h2 class="course-title pull-right" style="position: relative; top: 10px;word-break:normal;"><?php echo $course->name; ?></h2>
    <?php echo CHtml::image(Yii::app()->theme->getLogoUrl()); ?>
</div>
<hr style="margin: 10px 0;"/>

<div class="course-report-session-selector">
    <?= CHtml::label(Yii::t('classroom', 'Session') . ': ' .
        CHtml::dropDownList('session_id', ($session->isNewRecord ? '' : $session->getPrimaryKey()), CHtml::listData($course->getSessions(), 'id_session', function($session) {
            return $session->renderDetailedSessionName(' - ');
        }), array('empty'=>Yii::t('classroom', 'All sessions')))
        , 'session_id', array('class' => 'clearfix')) ?>
</div>
<hr style="margin: 10px 0;"/>

<!--course summary-->
<div class="course-report-summary clearfix">
    <div class="course-report-info-image">
        <?php echo CHtml::image($course->courseLogoUrl, '', array('width' => 150)); ?>
    </div>
    <div class="course-report-right-side clearfix">
        <div class="text-right courses-stat <?php echo (Yii::app()->chart->isIE8()?'IE8':''); ?>" style="margin-top: 10px;">
            <div class="completed">
                <p><?php echo Yii::t('stats', 'Users that have passed the session(s)'); ?></p>
                <div class="completed-stat donut-wrapper">
                    <?php echo $singlePieChart; ?>
                </div>
            </div>
            <div class="enrolled-users">
                <?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/enrolled-users.png');?>
                <div class="data"><?= $pieChartData['total'] ?></div>
                <div class="label"><?php echo Yii::t('standard', 'Enrolled users'); ?></div>
            </div>
            <div class="total-session-hours">
                <?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/session-hours.png');?>
                <div class="data"><?= $pieChartData['totalHours'] ?></div>
                <div class="label"><?php echo Yii::t('stats', 'Total session hours'); ?></div>
            </div>
        </div>
    </div>
</div>
<!--course summary end-->


<?php if (!$session->isNewRecord) : ?>

    <div class="course-report-session-details">
        <h2 class="title"><?php echo Yii::t('statistic', '_VIEW_SESSION_DETAILS'); ?></h2>

        <div class="row-fluid">
            <div class="span6 grid-view">
                <h4><?= Yii::t('standard', '_SCHEDULE') ?></h4>
                <table class="items">
                    <thead>
                    <tr>
                        <th><?= Yii::t('standard', '_NAME') ?></th>
                        <th><?= Yii::t('standard', '_DATE') ?></th>
                        <th><?= Yii::t('report', '_TOTAL_SESSION') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($session->getDates() as $date) : ?>
                        <?php /* @var $date LtCourseSessionDate */ ?>
                        <tr>
                            <td><?= $date->name ?></td>
                            <td><?= Yii::app()->localtime->toLocalDate($date->day) ?></td>
                            <td><?= $date->renderDaySchedule(' | ') ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="span6" style="padding-top: 15px;">
                <?php
                /* @var $classroom LtClassroom */
                $classroom = null;
                $sessionDates = $session->getDates();
                if (!empty($sessionDates)) {
                    $firstDate = $sessionDates[0];
                    $classroom = $firstDate->ltClassroom;
                }
                ?>

                <?php if ($classroom!==null) : ?>
                    <h4><?= Yii::t('classroom', 'Location & Classroom information') ?></h4>

                    <div class="row-fluid">
                        <div class="span6">
                            <div class="info-section">
                                <div class="info-section-icon">
                                    <span class="classroom-sprite marker-tiny"></span>
                                </div>
                                <div class="info-section-content has-icon">
                                    <?= $classroom->ltLocation->name ?><br/>
                                    <?= $classroom->ltLocation->renderAddress() ?>
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <?php if (!empty($classroom->ltLocation->telephone)) : ?>
                                <div class="info-section">
                                    <div class="info-section-icon">
                                        <span class="classroom-sprite phone"></span>
                                    </div>
                                    <div class="info-section-content has-icon" style="padding-top: 3px;"><?= $classroom->ltLocation->telephone ?><br/></div>
                                </div>
                            <?php endif; ?>
                            <?php if (!empty($classroom->ltLocation->email)) : ?>
                                <div class="info-section">
                                    <div class="info-section-icon">
                                        <span class="classroom-sprite mail"></span>
                                    </div>
                                    <div class="info-section-content has-icon text-colored"><?= $classroom->ltLocation->email ?></div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span6">
                            <div class="info-section">
                                <div class="info-section-icon">
                                    <span class="classroom-sprite classroom"></span>
                                </div>
                                <div class="info-section-content has-icon">
                                    <?= $classroom->name ?><br/>
                                    <?= DoceboHtml::readmore($classroom->details, 200) ?>
                                </div>
                            </div>
                        </div>
                        <div class="span6"></div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="user-report-session-statistics">
        <h2 class="course-title" style="margin: 20px 0 10px;"><?php echo Yii::t('stats', 'Session statistics'); ?></h2>
        <div class="courses-grid-wrapper">
					<?php
					$this->widget('zii.widgets.grid.CGridView', array(
						'id' => 'session-statistics-grid',
						'htmlOptions' => array('class' => 'grid-view clearfix', 'style' => 'padding-top: 0;border-top: 1px solid #E4E6E5;'),
						'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
						'dataProvider' => LtCourseuserSession::model()->dataProviderUserStatistics($session->getPrimaryKey()),
						'cssFile' => false,
						'template' => '<div class=scroll-content>{items}</div>{summary}{pager}',
						'summaryText' => Yii::t('standard', '_TOTAL'),
						'pager' => array(
							'class' => 'DoceboCLinkPager',
							'maxButtonCount' => 8,
						),
						'ajaxUpdate' => 'all-items',
						'beforeAjaxUpdate' => 'function(id, options) {
					options.type = "POST";
					if (options.data == undefined) {
						options.data = {
							contentType: "html",
						}
					}
				}',
						'columns' => array(
							array(
								'header' => Yii::t('standard', '_USERNAME'),
								'name' => 'user.userid',
								'value' => 'Yii::app()->user->getRelativeUsername($data->learningUser->userid)',
							),
							array(
								'header' => Yii::t('standard', '_ATTENDANCE'),
								'name' => 'session.attendance',
								'value' => '$data->ltCourseSession->renderAttendance($data->learningUser->idst)',
							),
							array(
								'name' => 'status',
								'value' => '$data->renderEvaluationStatus()'
							),
							array(
								'name' => 'score',
								'header' => Yii::t('standard', '_SCORE'),
								'value' => '$data->renderSessionScore()' //'intval($data->evaluation_score) . "/" . intval($data->ltCourseSession->score_base)'
							),
						),
					));
					?>
        </div>
    </div>

<?php else: ?>

    <div class="user-report-session-statistics">
        <h2 class="course-title" style="margin: 20px 0 10px;"><?php echo Yii::t('stats', 'Session statistics'); ?></h2>
        <div class="courses-grid-wrapper">

            <?php if (!empty($course->learningCourseSessions)) : ?>
							<?php
							$this->widget('zii.widgets.grid.CGridView', array(
								'id' => 'session-statistics-grid',
								'htmlOptions' => array('class' => 'grid-view clearfix', 'style' => 'padding-top: 0;border-top: 1px solid #E4E6E5;'),
								'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
								'dataProvider' => LtCourseuserSession::model()->dataProviderCourseStatistics($course->getPrimaryKey()),
								'cssFile' => false,
								'template' => '<div class=scroll-content>{items}</div>{summary}{pager}',
								'summaryText' => Yii::t('standard', '_TOTAL'),
								'pager' => array(
									'class' => 'DoceboCLinkPager',
									'maxButtonCount' => 8,
								),
								'ajaxUpdate' => 'all-items',
								'beforeAjaxUpdate' => 'function(id, options) {
                        options.type = "POST";
                        if (options.data == undefined) {
                            options.data = {
                                contentType: "html",
                            }
                        }
                    }',
								'columns' => array(
									array(
										'header' => Yii::t('standard', '_USERNAME'),
										'name' => 'user.userid',
										'value' => 'Yii::app()->user->getRelativeUsername($data->learningUser->userid)',
									),
									array(
										'header' => Yii::t('classroom', 'Session name'),
										'name' => 'session.name',
										'value' => '$data->ltCourseSession->renderDetailedSessionName()',
										'type' => 'raw'
									),
									array(
										'header' => Yii::t('standard', '_ATTENDANCE'),
										'name' => 'session.attendance',
										'value' => '$data->ltCourseSession->renderAttendance($data->id_user)',
									),
									array(
										'name' => 'session.status',
										'value' => '$data->renderEvaluationStatus()'
									),
									array(
										'name' => 'session.score',
										'header' => Yii::t('standard', '_SCORE'),
										'value' => '$data->renderSessionScore()'//'intval($data->evaluation_score) . "/" . intval($data->ltCourseSession->score_base)'
									),
								),
							));
							?>
            <?php else: ?>
							<?php
							$this->widget('zii.widgets.grid.CGridView', array(
								'id' => 'session-statistics-grid',
								'htmlOptions' => array('class' => 'grid-view clearfix', 'style' => 'padding-top: 0;border-top: 1px solid #E4E6E5;'),
								'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
								'dataProvider' => new CArrayDataProvider(array()),
								'cssFile' => false,
								'template' => '<div class=scroll-content>{items}</div>{summary}{pager}',
								'summaryText' => Yii::t('standard', '_TOTAL'),
								'pager' => array(
									'class' => 'DoceboCLinkPager',
									'maxButtonCount' => 8,
								),
								'ajaxUpdate' => 'all-items',
								'beforeAjaxUpdate' => 'function(id, options) {
										options.type = "POST";
										if (options.data == undefined) {
											options.data = {
												contentType: "html",
											}
										}
									}',
								'columns' => array(
									array(
										'header' => Yii::t('standard', '_USERNAME'),
										'name' => 'user.userid',
									),
									array(
										'header' => Yii::t('classroom', 'Session name'),
										'name' => 'session.name',
									),
									array(
										'header' => Yii::t('standard', '_ATTENDANCE'),
										'name' => 'session.attendance',
									),
									array(
										'name' => 'session.status',
									),
									array(
										'name' => 'session.score',
										'header' => Yii::t('standard', '_SCORE'),
									),
								),
							));
							?>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>

</div> <!-- End of .printable -->


<script type="text/javascript">
    var oClassroomCourseReport = {
        init: function() {
            applyDonut('.donutContainer');

            $('.report-action-print').click(function(){
                if ($(this).hasClass('disabled')) return false;
                window.print();
            });
            replacePlaceholder();
            $('.scroll-content').jScrollPane({
                autoReinitialise: true
            });

            if ($('.course-report-session-selector select').val().length) {
                $('.report-popup-actions a').removeClass('disabled');
            } else {
                $('.report-popup-actions a').addClass('disabled');
            }
            $('.course-report-session-selector select').change(function() {
                var sessionId = $(this).val();
                $('.donutContainer').html('');

                var url = '<?= $this->createUrl('classroomCourseReport', array('course_id'=>$course->idCourse)) ?>&session_id='+sessionId;
                $('.classroom-course-report-container').load(url+' .classroom-course-report-container', {session_id:sessionId}, function(response) {
                    oClassroomCourseReport.init();
                });
                //$('.summary-report-form .new-course-summary').data('session-id', sessionId).click();
            });
        }
    };
    $(document).ready(oClassroomCourseReport.init);
</script>
