<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', '_REPORTS') => array('reportManagement/index'),
	$reportFilterModel->filter_name,
); ?>

<h3 class="title-bold back-button">
	<?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('dashboard/index')); ?>
	<span><?php echo $reportFilterModel->filter_name; ?></span>
</h3>

<div class="report-bottom-side">
 		<div class="filters-wrapper">
        <div class="filters clearfix">
        	<?php /*<div class="select-wrapper">
        		<label>label</label>
						<select name="massive_action" id="group-member-management-massive-action">
							<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
							<option value="delete"><?php echo Yii::t('standard', '_DEL'); ?></option>
						</select>
        	</div>*/ ?>
         <?php /* ?>
					<div class="input-wrapper">
	            <input type="text" class="typeahead">
	            <span class="search-icon"></span>
            </div>
					<?php */ ?>
        </div>
	</div>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'report',
	'dataProvider' => $data['dataProvider'],
	'columns' => $data['columns'],
	'template' => '{items}{pager}',
	'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
	'pager' => array(
		'class' => 'DoceboCLinkPager',
		'maxButtonCount' => 8,
	),
)); ?>

<script type="text/javascript">
	$('input, select').styler();
	//$('#report').jScrollPane({});
</script>