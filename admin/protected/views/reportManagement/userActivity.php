<?php

$this->breadcrumbs = array(
    Yii::t('course', 'Your Activities'),
);

?>


<div class="printable new-user-summary">

    <div class="user-report-summary row-fluid">

        <div class="user-report-info span6">
            <div class="user-report-info-image">
                <?php echo CHtml::image($userModel->getAvatarImage(true), $userModel->firstname . " " . $userModel->lastname, array('width' => 150) ); ?>
            </div>
            <div class="user-report-info-data">
                <div class="fullname"><?php echo $userModel->firstname; ?> <?php echo $userModel->lastname; ?></div>
                <div class="username"><?php echo Yii::app()->user->getRelativeUsername($userModel->userid); ?></div>
                <div>
                    <div class="label"><?php echo Yii::t('standard', '_LEVEL'); ?></div>
                    <div class="data"><?php echo (!empty($userModel->adminLevel->groupid)? Yii::t('admin_directory', '_DIRECTORY_'.$userModel->adminLevel->groupid) :''); ?></div>
                </div>
                <div>
                    <div class="label"><?php echo Yii::t('standard', '_EMAIL'); ?></div>
                    <div class="data"><?php echo $userModel->email; ?></div>
                </div>
                <div>
                    <div class="label"><?php echo Yii::t('standard', '_GROUPS'); ?></div>
                    <div class="data">
                        <?php echo implode(', ', $userGroups); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="user-report-stat text-right span6">
            <div class="subscription-date">
                <?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/subscription-date.png');?>
                    <div class="data"><?php echo $userModel->register_date; ?></div>
                    <div class="label"><?php echo Yii::t('report', '_DATE_INSCR'); ?></div>
            </div>
            <div class="last-access">
                <?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/last-access-date.png');?>
                    <div class="data"><?php echo (!empty($userModel->lastenter)?$userModel->lastenter:Yii::t('standard', '_NEVER')); ?></div>
                    <div class="label"><?php echo Yii::t('standard', '_DATE_LAST_ACCESS'); ?></div>
            </div>
            <div class="total-time">
                <?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/total-time.png');?>
                <div class="data"><?php echo $totalTime; ?></div>
                <div class="label"><?php echo Yii::t('standard', '_TOTAL_TIME'); ?></div>
            </div>
            <div class="active-courses">
                <?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/active-courses.png');?>
                    <div class="data"><?php echo count($userModel->learningCourseusers); ?></div>
                    <div class="label"><?php echo Yii::t('dashboard', '_ACTIVE_COURSE'); ?></div>
            </div>
        </div>

    </div>

    <div class="user-additional-fields clearfix">
        <?php if (!empty($additionalFields)) {
            $i = 0;
            foreach ($additionalFields as $fieldName => $userValue) {
                $fieldClass = ($i % 2 == 0)? 'odd' : 'even';
                echo '<div class="field '.$fieldClass.' '. (($i <= 1) ? 'firstField' : '' ) .'"><div class="name">'.$fieldName.'</div><div class="value">'.$userValue.'</div></div>';
                $i++;
            }
        }	?>

    </div>

	<div class="report-courses-summary">

        <div class="row-fluid">
            <div class="report-courses-summary-legend span5">
				<div class="my-activities-stat-title row-fluid">
					<div class="span12"><?php echo Yii::t('standard', '_PROGRESS'); ?></div>
				</div>
				<div class="row-fluid">
                    <div class="donut-wrapper span5">
                        <?php echo $pieChart; ?>
                    </div>

                    <div class="summary-legend-wrapper span6">
                        <?php echo $pieChartLegend; ?>
                    </div>
                </div>
            </div>

            <div class="report-activities-summary span7">
                <div class="my-activities-stat-title row-fluid">
					<div class="span12"><?php echo Yii::t('report', '_YEARLY_ACTIVITIES_SUMMARY'); ?></div>
				</div>
				<div class="lineChart-wrapper">
                    <?php echo $lineChart; ?>
                </div>
            </div>
        </div>

	</div>


    <div class="user-report-courses" style="margin-bottom: 20px;">
        <div class="courses-title"><?php echo Yii::t('standard', '_COURSES'); ?></div>
        <div class="courses-grid-wrapper">
					<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id' => 'course-management-grid',
						'htmlOptions' => array('class' => 'grid-view clearfix'),
						'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
						'dataProvider' => $courseUserModel->dataProvider(),
						'cssFile' => false,
						'template' => '<div class=scroll-content>{items}</div>{summary}{pager}',
						'summaryText' => Yii::t('standard', '_TOTAL'),
						'pager' => array(
							'class' => 'DoceboCLinkPager',
							'maxButtonCount' => 8,
						),
						'ajaxUpdate' => 'all-items',
						'beforeAjaxUpdate' => 'function(id, options) {
                    options.type = "POST";
                    if (options.data == undefined) {
                        options.data = {
                            contentType: "html",
                        }
                    }
                }',
						'columns' => array(
							array(
								'name' => 'course.code',
								'header' => Yii::t('standard', '_COURSE_CODE'),
							),
							array(
								'name' => 'course.name',
								'value' => '$data->renderCourseLink($data->course->name)',
								'type' => 'raw'
							),
							array(
								'header' => Yii::t('profile', '_USERCOURSE_STATUS'),
								'name' => 'status',
								'value' => 'LearningCourseuser::getStatusLabel($data->status);'
							),
							array(
								'header' => Yii::t('standard', 'Enrolled'),
								'name' => 'date_inscr',
								'value' => '$data->date_inscr'
							),
							array(
								'header' => Yii::t('standard', '_DATE_FIRST_ACCESS'),
								'name' => 'date_first_access',
								'value' => '$data->date_first_access'
							),
							array(
								'header' => Yii::t('standard', 'Course completion'),
								'name' => 'date_complete',
								'value' => '$data->date_complete'
							),
							array(
								'header' => Yii::t('standard', '_CREDITS'),
								'name' => 'course.credits',
							),
							array(
								'header' => Yii::t('standard', '_TOTAL_TIME'),
								'value' => '$data->renderTimeInCourse();',
							),
							array(
								'header' => Yii::t('standard', '_SCORE'),
								'value' => 'LearningCourseuser::getLastScoreByUserAndCourse($data["idCourse"],$data["idUser"])'
							),
						),
					)); ?>
        </div>
    </div>

    <?php if(PluginManager::isPluginActive("ClassroomApp")): ?>
    <div class="user-report-classroom-courses">
        <div class="courses-title"><?php echo Yii::t('classroom', 'Classrooms'); ?></div>
        <div class="courses-grid-wrapper">
            <?php $this->widget('zii.widgets.grid.CGridView', array(
						'id' => 'classroom-course-management-grid',
						'htmlOptions' => array('class' => 'grid-view clearfix'),
						'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
						'dataProvider' => LtCourseuserSession::model()->dataProviderClassroomCoursesActivity($userModel->getPrimaryKey()),
						'cssFile' => false,
						'template' => '<div class=scroll-content>{items}</div>{summary}{pager}',
						'summaryText' => Yii::t('standard', '_TOTAL'),
						'pager' => array(
							'class' => 'DoceboCLinkPager',
							'maxButtonCount' => 8,
						),
						'ajaxUpdate' => 'all-items',
						'beforeAjaxUpdate' => 'function(id, options) {
                    options.type = "POST";
                    if (options.data == undefined) {
                        options.data = {
                            contentType: "html",
                        }
                    }
                }',
                'columns' => array(
					array(
						'name' => 'session.name',
						'header' => Yii::t('classroom', 'Course and session information'),
						'value' => '$data->renderCourseLink($data->session->course->name." - ".$data->session->name)',
						'type'=>'raw'
					),
                    array(
                        'header' => Yii::t('standard', '_START_DATE'),
                        'name' => 'date_subscribed',
                        'value' => '$data->date_subscribed'
                    ),
                    /*array(
                        'header' => Yii::t('standard', '_CREDITS'),
                        'name' => 'course.credits',
                    ),*/
                    array(
                        'header' => Yii::t('standard', '_ATTENDANCE'),
                        'name' => 'session.attendance',
                        'value' => '$data->session->renderAttendance($data->id_user)',
                    ),
                    array(
                        'header' => Yii::t('standard', '_STATUS'),
                        'name' => 'status',
                        'value' => 'Yii::t("standard", LtCourseuserSession::getStatusLangLabel($data->status))'
                    ),
                    array(
                        'header' => Yii::t('standard', '_SCORE'),
                        'value' => 'intval($data->evaluation_score) . "/" . intval($data->session->score_base)'
                    ),
                ),
            )); ?>
        </div>
    </div>
    <?php endif; ?>

</div> <!-- End of .printable -->

<script type="text/javascript">
	$(document).ready(function() {
		$('.report-action-print').click(function(){
			window.print();
		});
		$('.scroll-content').jScrollPane({
            autoReinitialise: true
		});
		replacePlaceholder();

        applyDonut('.donutContainer');
        applyLineCharts('.lineChartContainer');
	});
</script>
