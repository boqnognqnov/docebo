<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'user_form',
	));	?>

	<p><?php echo Yii::t('course', 'Send Email').' '.Yii::t('standard', '_TO').': "'.count($users).' '.Yii::t('standard', '_USERS').'"'; ?></p>

	<div class="clearfix">
		<?php echo $form->label($emailModel, 'subject'); ?>
		<?php echo $form->textField($emailModel, 'subject'); ?>
		<?php echo $form->error($emailModel, 'subject'); ?>
	</div>

	<div class="clearfix">
		<?php echo $form->label($emailModel, 'message'); ?>
		<?php echo $form->textArea($emailModel, 'message', array('id' => 'EmailForm_message_' . $checkboxIdKey)); ?>
		<?php echo $form->error($emailModel, 'message'); ?>
	</div>

	<div class="clearfix">
		<?php echo $form->checkbox($emailModel, 'confirm', array('id' => 'LearningCourseuser_confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo chtml::label(Yii::t('standard','Yes, I confirm I want to proceed', array(count($users), '{username}' => Yii::app()->user->getRelativeUsername($users[0]->userid))), 'LearningCourseuser_confirm_'.$checkboxIdKey); ?>
		<?php echo $form->error($emailModel, 'confirm'); ?>
	</div>

	<?php foreach ($users as $user) {
		echo CHtml::hiddenField('ids[]', $user->idst);
	}
	?>
	<?php $this->endWidget(); ?>
</div>