<div class="main-section report-management">
	<h3 class="title-bold"><?php echo Yii::t('report', '_GENERATE_STANDART_REPORTS'); ?></h3>
</div>

<div>

	<?php
		$_columns = array(
			array(
				'name' => 'filter_name',
                'value' => 'Yii::t("report", $data->filter_name)',
			)
		);
		if ($canModReports) {
			$_columns[] = array(
				'name' => 'is_public',
				'type' => 'raw',
				'value' => '"<span class=".($data->is_public ? \'public\' : \'not-public\').">".$data->is_public."</span>"',
				'htmlOptions' => array('class' => 'center-aligned'),
				'headerHtmlOptions' => array('class' => 'center-aligned'),
			);
		}
		$_columns[] = array(
			'class' => 'CButtonColumn',
			'buttons' => array(
				'view' => array(
					'url' => 'Yii::app()->createUrl(\'reportManagement/customView\', array(\'id\' => $data->id_filter))',
					'label' => 'view',
					'imageUrl' => false,
				),
			),
			'viewButtonOptions' => array('rel' => 'tooltip', 'title' => Yii::t("standard", "_VIEW"), 'class' => 'docebo-sprite ico-magnifier'),
			'template' => '{view}',
			'htmlOptions' => array('class' => 'button-column-single',),
			'headerHtmlOptions' => array('class' => 'button-column-single'),
		);
		$_columns['actions'] = array(
			'type' => 'raw',
			'value' => '$data->renderStandardReportActions();',
			'htmlOptions' => array('class' => 'button-column-single'),
			'headerHtmlOptions' => array('class' => 'button-column-single'),
		);

		//$this->widget('adminExt.local.widgets.CXGridView', array(
		$this->widget('zii.widgets.grid.CGridView', array(				
			'id' => 'standard-reports',
			'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
			'dataProvider' => $model->dataProviderStandard(true),
			'template' => '{items}',
			'columns' => $_columns
		));
	?>
</div>


<script type="text/javascript">

	$(function(){

		$('[id^="popover-"]').on('shown.bs.popover', function(){
			$(document).controls();
		});


	});


</script>
