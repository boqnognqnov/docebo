<div class="report-actions">
	<ul>
		<li class="report-action clearfix">
			<?php echo CHtml::link(Yii::t('standard', '_EXPORT_XLS'), Yii::app()->createUrl('reportManagement/exportCustomReport', array(
				'id' => $model->id_filter,
				'type' => 'Excel5',
			))); ?>
		</li>
		<li class="report-action clearfix">
			<?php echo CHtml::link(Yii::t('standard', '_EXPORT_CSV'), Yii::app()->createUrl('reportManagement/exportCustomReport', array(
				'id' => $model->id_filter,
				'type' => 'CSV',
			))); ?>
		</li>
		<li class="report-action clearfix">
			<?php echo CHtml::link(Yii::t('standard', '_EXPORT_HTML'), Yii::app()->createUrl('reportManagement/exportCustomReport', array(
				'id' => $model->id_filter,
				'type' => 'HTML',
			))); ?>
		</li>
	</ul>
</div>
