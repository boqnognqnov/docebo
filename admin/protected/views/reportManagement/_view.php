<div class="report-row">
	<div class="report-row-inner">
		<div class="column-field-checkbox column-field">
			<?php echo CHtml::checkBox('CoreUser[idst][]', in_array($data->idUser, Yii::app()->session['selectedItems']), array('value' => $data->idUser)); ?>
		</div>
		<?php if (!empty($fields)) { ?>
			<?php foreach ($fields as $key => $field) {
				$fieldData = '';
				if (is_array($field)) {
					eval('$fieldData = ' . $field['value']);
				} else {
					$relFieldPath = explode('.', $field);
					if (!empty($relFieldPath)) {
						$fieldData = $data;
						foreach ($relFieldPath as $key => $rel) {
							if (isset($fieldData->{$rel})) {
								$fieldData = $fieldData->{$rel};
							} else {
								$fieldData = null;
							}
						}
					}
				}
				
				echo '<div class="column-field">'. (!empty($fieldData) ? $fieldData : '&nbsp;') .'</div>';
			} ?>
		<?php } ?>
	</div>
</div>