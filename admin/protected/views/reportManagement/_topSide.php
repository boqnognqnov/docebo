<?php
$canModReports = (Yii::app()->user->isGodAdmin || (Yii::app()->user->isAdmin && Yii::app()->user->checkAccess('/lms/admin/report/mod')));
?>
<div class="main-section report-management">
	<h3 class="title"><span><?php echo Yii::t('app7020', 'Quick Summary Reports'); ?></span></h3>
	<div class="summary-user-report clearfix report-box text-center">
		<i class="fa fa-user"></i>
		<div class="summary-report-wrapper-new">
			<div class="summary-report-title"><?php echo Yii::t('report', 'User personal summary'); ?></div>
			<div class="summary-report-description"><?php echo Yii::t('report', 'User personal summary desc'); ?></div>

			<div class="summary-report-form text-left clearfix">
				<?php
				echo CHtml::textField('CoreUser[search_input]', '', array(
					'id' => 'advanced-search-user-report',
					'class' => 'typeahead ajaxGenerate',
					'placeholder' => Yii::t('report', 'Type user here')
				));
				?>

				<?php
				echo CHtml::link(
						Yii::t('certificate', '_GENERATE'), Docebo::createLmsUrl('myActivities/userReport', array('from' => 'reportManagement')), array('class' => 'new-user-summary', 'id' => 'generate-user-report')
				);
				?>
				<script>
					$(function () {
						$("#advanced-search-user-report").autocomplete({
							source: function (request, response) {

								$.ajax({
									url: "<?php echo Docebo::createAppUrl('admin:userManagement/usersAutocomplete') ?>",
									dataType: "json",
									type: "POST",
									data: {
										data: {
											query: request.term
										}
									},
									success: function (data) {
										var options = [];


										$.each(data.options, function (index, value) {
											options.push({
												"value": index,
												"label": value
											})
										});
										response(options);
									}
								});
							},
							minLength: 1,
							focus: function (event, ui) {
								$("#advanced-search-user-report").val(ui.item.label);
								return false;
							},
							select: function (event, ui) {
								if ($('#idUser')) {
									$('#idUser').remove();
								}
								var hiddenField = $('<input type="hidden" id="idUser" name="idUser" value="' + ui.item.value + '"/>');
								$(".summary-user-report").append(hiddenField);
								ui.item.value = ui.item.label;
							}
						});
						$('#generate-user-report').on('click', function (e) {
							e.preventDefault();

							function verifyInitData() {
								var input = $("#idUser");
								var data = input.val();
								if (!data) {
									$("#advanced-search-user-report").css('border-color', '#ff0000');
									return false;
								}
								input.css('border-color', '#E4E6E5');
								return true;
							}

							if (verifyInitData()) {
								var url = $(this).attr('href'), userid = $('#idUser').val();
								window.location.href = url + '&userid=' + encodeURIComponent(userid);
							}
						});
					});
				</script>

			</div>
		</div>
	</div>
	
	<div class="summary-course-report clearfix report-box text-center">
		<i class="fa fa-book"></i>
		<div class="summary-report-wrapper-new">
			<div class="summary-report-title"><?php echo Yii::t('report', '_SINGLE_COURSE_SUMMARY'); ?></div>
			<div class="summary-report-description"><?php echo Yii::t('report', 'Single course summary desc'); ?></div>

			<div id="advanced_search_course_report_container" class="summary-report-form text-left clearfix">
				<?php
				echo CHtml::textField('LearningCourse[name]', '', array(
					'id' => 'advanced-search-course-report',
					'class' => '',
					'autocomplete' => 'off',
					'data-url' => Docebo::createAppUrl('admin:courseManagement/courseAutocomplete'),
					'data-type' => 'core',
					'data-formatted-course-name' => true,
					'placeholder' => Yii::t('report', 'Type course here'),
				));
				?>

				<?php
				$this->renderPartial('//common/_modal', array(
					'config' => array(
						'class' => 'new-course-summary reportcoursesummary',
						'modalTitle' => Yii::t('report', '_SINGLE_COURSE_SUMMARY'),
						'linkTitle' => Yii::t('certificate', '_GENERATE'),
						'url' => 'reportManagement/createCourseReport',
						'buttons' => array(),
						'beforeLoadingContent' => 'function() { if (generateRaportVerifyInitData(config)) {hideOtherModals(config); return setCourseSummarySessionId(config); } else { return false; } }',
						'afterLoadingContent' => 'function() {applyTypeahead($(\'.modal.in .typeahead\')); applyDonut(\'.modal.in .donutContainer\'); applyLineCharts(\'.modal.in .lineChartContainer\');}',
					),
				));
				?>
			</div>
		</div>
	</div>
	
	<div class="summary-course-report clearfix report-box text-center" style="vertical-align: top;">
		<i class="zmdi zmdi-view-compact" style="font-size: 32px;"></i>
		<div class="summary-report-wrapper-new">
			<div class="summary-report-title"><?php echo Yii::t('report', 'Courses dashboard'); ?></div>
			<div class="summary-report-description"><?php echo Yii::t('report', 'View course dashboard and download it as a pdf'); ?></div>

			<div id="advanced_search_course_report_container" class="summer-report-actions" style="padding-top: 20px; text-align: center;">
				

				<?php
				
				echo CHtml::link(Yii::t('certificate', 'View'), rtrim(Yii::app()->getBaseUrl(), '/admin').'/learn/admin/reports',array(
						'class' => 'new-course-summary reportcoursesummary',
						'target' => '_top'
					));
				
				?>
			</div>
		</div>
	</div>
	
	<!-- App7020 reports -->
	<?php if (PluginManager::isPluginActive('Share7020App')) { ?>
		<div class="summary-course-report clearfix report-box text-center">
			<div class="summary-report-icon expert">
				<span class="fa-stack fa-lg">
					<i class="fa fa-user fa-stack-1x fa-3x"></i>
					<i class="fa fa-star fa-stack-2x"></i>
				</span>
			</div>
			<div class="summary-report-wrapper-new">
				<div class="summary-report-title"><?php echo Yii::t('report', 'Expert summary'); ?></div>
				<div class="summary-report-description"><?php echo Yii::t('report', 'View an expert summary report and download it as a PDF'); ?></div>

				<div id="advanced_search_expert_report_container" class="summary-report-form text-left clearfix">
					<?php
					echo CHtml::textField('Expert name', '', array(
						'id' => 'advanced-search-expert-report',
						'class' => 'typeahead ajaxGenerate',
						'placeholder' => Yii::t('report', 'Type expert name here'),
						'data-formatted-course-name' => true,
					));
					?>

					<?php
					echo CHtml::link(
							Yii::t('certificate', '_GENERATE'), Docebo::createAppUrl('app7020:expertSummary/index'), array('class' => 'new-user-summary', 'id' => 'generate-expert-report')
					);
					?>
				</div>

			</div>
		</div>

		<div class="summary-asset-report clearfix report-box text-center">
			<i class="fa fa-file" aria-hidden="true"></i>
			<div class="summary-report-wrapper-new">
				<div class="summary-report-title"><?php echo Yii::t('report', 'Asset summary'); ?></div>
				<div class="summary-report-description"><?php echo Yii::t('report', 'View an asset summary report and download it as a PDF'); ?></div>

				<div id="advanced_search_asset_report_container" class="summary-report-form text-left clearfix">
					<?php
					echo CHtml::textField('App7020Asset[search_input]', '', array(
						'id' => 'advanced-search-asset-report',
						'class' => 'typeahead ajaxGenerate',
						'autocomplete' => 'off',
						'data-url' => Docebo::createAppUrl('admin:reportManagement/assetsAutocomplete'),
						'data-type' => 'core',
						'placeholder' => Yii::t('report', 'Type asset name here'),
						'data-formatted-course-name' => true,
					));
					?>

					<?php
					echo CHtml::link(
							Yii::t('certificate', '_GENERATE'), Docebo::createApp7020Url('assetSummary/index'), array('class' => 'new-user-summary', 'id' => 'generate-assets-report')
					);
					?>
				</div>
			</div>
		</div>
		<div class="summary-channel-report clearfix report-box text-center">
			<i class="fa fa-television" aria-hidden="true"></i>
			<div class="summary-report-wrapper-new">
				<div class="summary-report-title"><?php echo Yii::t('report', 'Channel summary'); ?></div>
				<div class="summary-report-description"><?php echo Yii::t('report', 'View a channel summary report and download it as a PDF'); ?></div>

				<div id="advanced_search_channel_report_container" class="summary-report-form text-left clearfix">
					<?php
					echo CHtml::textField('App7020Asset[search_input]', '', array(
						'id' => 'advanced-search-channel-report',
						'class' => 'typeahead ajaxGenerate',
						'autocomplete' => 'off',
						'data-url' => Docebo::createAppUrl('admin:reportManagement/channelsAutocomplete'),
						'data-type' => 'core',
						'placeholder' => Yii::t('report', 'Type channel name here'),
						'data-formatted-course-name' => true,
					));
					?>

					<?php
					echo CHtml::link(
							Yii::t('certificate', '_GENERATE'), Docebo::createApp7020Url('channelSummary/index'), array('class' => 'new-user-summary', 'id' => 'generate-channel-report')
					);
					?>
				</div>
			</div>
		</div>
	<?php } ?>

	<!-- Skill reports -->
	<?php if (PluginManager::isPluginActive('SkillApp')) { ?>
		<div class="summary-skill-report clearfix report-box text-center"> <!-- SKILL REPORT START -->
			<i class="zmdi zmdi-graduation-cap"></i>
			<div class="summary-report-wrapper-new">
				<div class="summary-report-title"><?php echo Yii::t('report', 'Skill Summary'); ?></div>
				<div class="summary-report-description"><?php echo Yii::t('report', 'View a skill summary report amd download it as a PDF'); ?></div>

				<div id="advanced_search_skills_report_container" class="summary-report-form text-left clearfix">
					<?php
					echo CHtml::textField('Skill[name]', '', array(
							'id' => 'advanced-search-skill-report',
							'class' => 'typeahead',
							'autocomplete' => 'off',
							'data-url' => Docebo::createAppUrl('admin:reportManagement/skillsAutocomplete'),
							'data-type' => 'core',
							'data-formatted-course-name' => true,
							'placeholder' => Yii::t('report', 'Type skill here'),
					));
					?>

					<?php

					echo CHtml::link(Yii::t('certificate', '_GENERATE'), (Yii::app()->request->hostInfo . '/skill/admin/reports/skills/'),array(
							'class' => 'new-course-summary reportcoursesummary',
							'target' => '_top',
							'id' => 'skill-report'
					));

					?>
				</div>
			</div>
		</div> <!-- SKILL REPORT END -->

		<div class="summary-roles-report clearfix report-box text-center"> <!-- ROLE REPORT START -->
			<i class="zmdi zmdi-assignment-account"></i>
			<div class="summary-report-wrapper-new">
				<div class="summary-report-title"><?php echo Yii::t('report', 'Role Summary'); ?></div>
				<div class="summary-report-description"><?php echo Yii::t('report', 'View a role summary report amd download it as a PDF'); ?></div>

				<div id="advanced_search_roles_report_container" class="summary-report-form text-left clearfix">
					<?php
					echo CHtml::textField('Role[name]', '', array(
							'id' => 'advanced-search-role-report',
							'class' => 'typeahead ajaxGenerate',
							'autocomplete' => 'off',
							'data-url' => Docebo::createAppUrl('admin:reportManagement/rolesAutocomplete'),
							'data-type' => 'core',
							'data-formatted-course-name' => true,
							'placeholder' => Yii::t('report', 'Type role here'),
					));
					?>

					<?php

					echo CHtml::link(Yii::t('certificate', '_GENERATE'), (Yii::app()->request->hostInfo . '/skill/admin/reports/roles/'),array(
							'class' => 'new-course-summary reportcoursesummary',
							'target' => '_top',
							'id' => 'role-report'
					));

					?>
				</div>
			</div>
		</div> <!-- ROLE REPORT END -->
	<?php } ?>

	<script>
		$(function () {
			$('#generate-assets-report').on('click', function (e) {
				e.preventDefault();
				function verifyInitData() {
					var button = $('#generate-assets-report');
					var input = $("#idAsset");
					var data = input.val();

					if (!data) {
						$("#advanced-search-asset-report").css('border-color', '#ff0000');
						return false;
					}
					input.css('border-color', '#E4E6E5');
					return true;
				}

				if (verifyInitData()) {
					var url = $(this).attr('href'), assetId = $('#advanced_search_asset_report_container input[type=hidden]#idAsset').val();
					window.location.href = url + '&asset=' + encodeURIComponent(assetId);
				}
			});


			<?php if (PluginManager::isPluginActive('SkillApp')) { ?>
			$('#role-report').on('click', function (e) {
				e.preventDefault();

				function verifyInitData() {
					var input = $("#idRole");
					var idRole = input.val();

					if (!idRole) {
						$("#advanced-search-role-report").css('border-color', '#ff0000');
						return false;
					}
					$("#advanced-search-role-report").css('border-color', '#E4E6E5');
					return true;
				}

				if (verifyInitData()) {
					var url = $(this).attr('href');
					var input = $("#idRole");
					var idRole = input.val();
					top.location.href = url + idRole;
				}
			});


			$('#skill-report').on('click', function (e) {
				e.preventDefault();

				function verifyInitData() {
					var input = $("#idSkill");
					var idSkill = input.val();

					if (!idSkill) {
						$("#advanced-search-skill-report").css('border-color', '#ff0000');
						return false;
					}
					$("#advanced-search-skill-report").css('border-color', '#E4E6E5');
					return true;
				}

				if (verifyInitData()) {
					var url = $(this).attr('href');
					var idSkill = $("#idSkill").val();
					top.location.href = url + idSkill;
				}
			});

			<?php } ?>
		});
	</script>

</div>