<style>
	@media (min-width: 980px) {
		.customLoading{
			width: 900px;
			margin-left: -450px;
		}
	}
	@media (min-width: 769px) and (max-width: 979px) {
		.customLoading{
			width: 720px;
			margin-left: -360px;
		}
	}

</style>
<?php
$this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', '_REPORTS'),
);

// Wizard's Dialog2 modal-body ID
$wizardDialogId = 'custom-report-wizard';

// jWizard dispatcher where all request are sent to
$wizardUrl = Docebo::createAdminUrl('reportManagement/customReportWizard', array());
?>

<div class="report-sides">
	<div class="report-top-side clearfix">
		<div class="row-fluid">
			<?php $this->renderPartial('_topSide', array()); ?>
		</div>
		<!--
		<div class="report-left-side">
			<?php //$this->renderPartial('_leftSide', array()); ?>
		</div>
		<div class="report-right-side">
			<?php //$this->renderPartial('_rightSide', array('model' => $reportFilterModel)); ?>
		</div>
		-->
	</div>
	<div class="report-bottom-side">
		<?php
		$this->renderPartial('_bottomSide', array(
			'model' => $reportFilterModel,
			'users' => $users,
			'wizardDialogId' => $wizardDialogId,
			'wizardUrl' => $wizardUrl,
		));
		?>
	</div>
</div>

<!--[if IE & (lt IE 9)]>
<script src="<?= $assetsUrl ?>/charts/r2d3.js" charset="utf-8"></script>
<![endif]-->

<!--[if IE & (gte IE 9)]>
<script src="<?= $assetsUrl ?>/charts/d3.v3.min.js" charset="utf-8"></script>
<script src="<?= $assetsUrl ?>/charts/aight.d3.min.js" charset="utf-8"></script>
<![endif]-->
<!--[if !IE]> -->
<script src="<?= $assetsUrl ?>/charts/d3.v3.min.js" charset="utf-8"></script>
<script src="<?= $assetsUrl ?>/charts/aight.d3.min.js" charset="utf-8"></script>
<!-- <![endif]-->



<script type="text/javascript">

	// Create Wizard object (themes/spt/js/jwizard.js)
	var jwizard = new jwizardClass({
		dialogId: '<?= $wizardDialogId ?>',
		dispatcherUrl: '<?= $wizardUrl ?>',
		alwaysPostGlobal: true,
		debug: false
	});


	// Create Report Manager object  (admin/js/reportmanager.js)
	var ReportMan = new ReportManClass({
		jobStatusToggleBaseUrl: '<?= Docebo::createAbsoluteAdminUrl('reportManagement/axToggleJobActiveStatus') ?>',
		wizDialogId: '<?= $wizardDialogId ?>',
		wizDispatcherUrl: '<?= $wizardUrl ?>'
	});



	// READY
	$(document).ready(function () {
		replacePlaceholder();

		// Mandatory !!!!!
		ReportMan.ready();

		//doing it with jQuery autocomplete instead of typeahead because bootcrap sucks and using key=>value is pretty much impossible
		$("#advanced-search-course-report").autocomplete({
			source: function (request, response) {

				$.ajax({
					url: "<?= Docebo::createAppUrl('admin:courseManagement/courseAutocomplete') ?>",
					dataType: "json",
					type: "POST",
					data: {
						data:{
							formattedCourseName: true,
							query: request.term
						}
					},
					success: function( data ) {
						$('#idCourse').remove();
						var options = [];
						$.each(data.options, function (index, value) {
							options.push({
								"value": index,
								"label": value
							})
						});
						response(options);
					}
				});
			},
			minLength: 1,
			focus: function (event, ui) {
				$("#advanced-search-course-report").val(ui.item.label);
				return false;
			},
			select: function (event, ui) {
				if ($('#idCourse')) {
					$('#idCourse').remove();
				}
				var hiddenField = $('<input type="hidden" id="idCourse" name="idCourse" value="' + ui.item.value + '"/>');
				$("#advanced_search_course_report_container").append(hiddenField);
				ui.item.value = ui.item.label;
			}
		});

		$("#advanced-search-expert-report").autocomplete({
			source: function (request, response) {

				$.ajax({
					url: "<?= Docebo::createAppUrl('app7020:expertSummary/expertsAutocomplete') ?>",
					dataType: "json",
					type: "POST",
					data: {
						data: {
							query: request.term
						}
					},
					success: function (data) {
						var options = [];
						$.each(data.options, function (index, value) {
							options.push({
								"value": index,
								"label": value
							})
						});
						response(options);
					}
				});
			},
			minLength: 1,
			focus: function (event, ui) {
				$("#advanced-search-expert-report").val(ui.item.label);
				return false;
			},
			select: function (event, ui) {
					$('#idExpert').remove();
				var hiddenField = $('<input type="hidden" id="idExpert" name="idExpert" value="' + ui.item.value + '"/>');
				$("#advanced_search_expert_report_container").append(hiddenField);
				ui.item.value = ui.item.label;
			}
		});
	});

	$("#advanced-search-asset-report").autocomplete({
		source: function (request, response) {

			$.ajax({
				url: "<?php echo Docebo::createAppUrl('admin:reportManagement/assetsAutocomplete') ?>",
				dataType: "json",
				type: "POST",
				data: {
					data: {
						query: request.term
					}
				},
				success: function (data) {
					var options = [];


					$.each(data.options, function (index, value) {
						options.push({
							"value": index,
							"label": value
						})
					});
					response(options);
				}
			});
		},
		minLength: 1,
		focus: function (event, ui) {
				$("#advanced-search-asset-report").val(ui.item.label);
				return false;
			},
		select: function (event, ui) {
			if ($('#idAsset')) {
				$('#idAsset').remove();
			}
			var hiddenField = $('<input type="hidden" id="idAsset" name="idAsset" value="' + ui.item.value + '"/>');
			$("#advanced_search_asset_report_container").append(hiddenField);
			ui.item.value = ui.item.label;
		}
	});
	$("#advanced-search-channel-report").autocomplete({
		source: function (request, response) {

			$.ajax({
				url: "<?php echo Docebo::createAppUrl('admin:reportManagement/channelsAutocomplete') ?>",
				dataType: "json",
				type: "POST",
				data: {
					data: {
						query: request.term
					}
				},
				success: function (data) {
					var options = [];


					$.each(data.options, function (index, value) {
						options.push({
							"value": index,
							"label": value
						})
					});
					response(options);
				}
			});
		},
		minLength: 1,
		focus: function (event, ui) {
				$("#advanced-search-channel-report").val(ui.item.label);
				return false;
			},
		select: function (event, ui) {
			if ($('#idChannel')) {
				$('#idChannel').remove();
			}
			var hiddenField = $('<input type="hidden" id="idChannel" name="idChannel" value="' + ui.item.value + '"/>');
			$("#advanced_search_channel_report_container").append(hiddenField);
			ui.item.value = ui.item.label;
		}
	});


	// Role report autocomplete
	$("#advanced-search-role-report").autocomplete({
		source: function (request, response) {

			$.ajax({
				url: "<?php echo Docebo::createAppUrl('admin:reportManagement/rolesAutocomplete') ?>",
				dataType: "json",
				type: "POST",
				data: {
					data: {
						query: request.term
					}
				},
				success: function (data) {
					var options = [];


					$.each(data.options, function (index, value) {
						options.push({
							"value": index,
							"label": value
						})
					});
					response(options);
				}
			});
		},
		minLength: 1,
		focus: function (event, ui) {
			$("#advanced-search-role-report").val(ui.item.label);
			return false;
		},
		select: function (event, ui) {
			if ($('#idRole')) {
				$('#idRole').remove();
			}
			var hiddenField = $('<input type="hidden" id="idRole" name="idRole" value="' + ui.item.value + '"/>');
			$("#advanced_search_roles_report_container").append(hiddenField);
			ui.item.value = ui.item.label;
		}
	});


	// Skill report autocomplete
	$("#advanced-search-skill-report").autocomplete({
		source: function (request, response) {

			$.ajax({
				url: "<?php echo Docebo::createAppUrl('admin:reportManagement/skillsAutocomplete') ?>",
				dataType: "json",
				type: "POST",
				data: {
					data: {
						query: request.term
					}
				},
				success: function (data) {
					var options = [];


					$.each(data.options, function (index, value) {
						options.push({
							"value": index,
							"label": value
						})
					});
					response(options);
				}
			});
		},
		minLength: 1,
		focus: function (event, ui) {
			$("#advanced-search-skill-report").val(ui.item.label);
			return false;
		},
		select: function (event, ui) {
			if ($('#idSkill')) {
				$('#idSkill').remove();
			}
			var hiddenField = $('<input type="hidden" id="idSkill" name="idSkill" value="' + ui.item.value + '"/>');
			$("#advanced_search_skills_report_container").append(hiddenField);
			ui.item.value = ui.item.label;
		}
	});


	$(document).on('click', '.modal.customLoading ul#selector-tabs a', function () {
		setTimeout(function () {
			var h = $('ul#selector-tabs').next('.tab-content').height();
			$('ul#selector-tabs').height(h);
		}, 200);

	});

	$('#generate-expert-report').on('click', function (e) {
		e.preventDefault();

		function verifyInitExpertData() {
			var data = $('#idExpert').val();
			if (!data) {
				$('#advanced-search-expert-report').css('border-color', '#ff0000');
				return false;
			}
			$('#advanced-search-expert-report').css('border-color', '#E4E6E5');
			return true;
		}

		if (verifyInitExpertData()) {
			var url = $(this).attr('href'), expertid = $('#idExpert').val();
			window.location.href = url + '&id=' + encodeURIComponent(expertid);
		}
	});
	$('#generate-channel-report').on('click', function (e) {
		e.preventDefault();
		function verifyInitChannelData() {
			var data = $('#idChannel').val();
			if (!data) {
				$('#advanced-search-channel-report').css('border-color', '#ff0000');
				return false;
			}
			$('#advanced-search-channel-report').css('border-color', '#E4E6E5');
			return true;
		}

		if (verifyInitChannelData()) {
			var url = $(this).attr('href'), channelId = $('#idChannel').val();
			window.location.href = url + '&idChannel=' + encodeURIComponent(channelId);
		}
	});
</script>

<style>
	.ui-autocomplete .ui-menu-item .ui-state-focus {
		background-color: #0080bf;
		color: #fff;
		background-image: none;
	}
</style>