<?php
	Yii::app()->clientScript->scriptMap['jquery.js'] = false;
	Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
	Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
?>

<div class="user-report-actions">
	<div class="clearfix">
		<span><?php echo Yii::t('report', '_SELECT_USER'); ?></span>
	  <?php echo CHtml::textField('CoreUser[search_input]', $username, array(
			'id' => 'advanced-search-user-report',
			'class' => 'typeahead ajaxGenerate',
			'autocomplete' => 'off',
			'data-url' => Docebo::createAppUrl('admin:userManagement/usersAutocomplete'),
			'data-type' => 'core',
            'data-loader-show' => 'true',
			'placeholder' => Yii::t('report', 'Type user here'),
			'data-source-desc' => 'true',
		)); ?>

		<?php $this->renderPartial('//common/_modal', array(
			'config' => array(
				'class' => 'new-user-summary',
				'modalTitle' => Yii::t('report', 'User personal summary'),
				'linkTitle' => Yii::t('certificate', '_GENERATE'),
				'url' => 'reportManagement/createUserReport',
// 				'buttons' => array(
// 					array(
// 						'type' => 'submit',
// 						'title' => Yii::t('standard', '_CONFIRM'),
// 					),
// 					array(
// 						'type' => 'cancel',
// 						'title' => Yii::t('standard', '_CANCEL'),
// 					),
// 				),
				'buttons' => array(),
				'beforeLoadingContent' => 'function() { if(generateRaportVerifyInitData(config)) {$(\'.modal.in\').modal(\'hide\'); return true;} else {return false;}}',
				'afterLoadingContent' => 'function() {applyTypeahead($(\'.modal.in .typeahead\'));applyDonut(\'.modal.in .donutContainer\'); applyLineCharts(\'.modal.in .lineChartContainer\');}',
				// 'afterSubmit' => 'updateGroupContent',
			),
		)); ?>

		<div class="report-popup-actions">
			<?php #echo CHtml::link('Print<span></span>', 'javascript:void(0);', array('class' => 'report-action-print')); ?>
			<?php #echo CHtml::link('Download as PDF<span></span>', Yii::app()->createAbsoluteUrl('reportManagement/createUserReport', array('id' => Yii::app()->user->getRelativeUsername($userModel->userid), 'createPdf' => true)), array('class' => 'report-action-pdf')); ?>
		</div>
	</div>
</div>

<div class="printable">
    <hr>
    <center>
        <b><?php echo Yii::t('report', 'User with that username is not found') ?></b>
    </center>
</div> <!-- End of .printable -->

<script type="text/javascript">
	$(document).ready(function() {
		replacePlaceholder();
	});
</script>
