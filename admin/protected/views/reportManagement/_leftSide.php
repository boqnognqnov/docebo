<?php
$canModReports = (Yii::app()->user->isGodAdmin || (Yii::app()->user->isAdmin && Yii::app()->user->checkAccess('/lms/admin/report/mod')));
?>
<div class="main-section report-management">
	<h3 class="title-bold"><?php echo Yii::t('report', '_GENERATE_SUMMARY_REPORTS'); ?></h3>

	<div class="summary-user-report clearfix">
		<div class="summary-report-icon user"></div>
			<div class="summary-report-wrapper">
				<div class="summary-report-title"><?php echo Yii::t('report', 'User personal summary'); ?></div>
				<div class="summary-report-description"><?php echo Yii::t('report', 'User personal summary desc'); ?></div>

				<div class="summary-report-form clearfix">
					<span><?php echo Yii::t('report', '_SELECT_USER'); ?></span>
		      <?php echo CHtml::textField('CoreUser[search_input]', '', array(
						'id' => 'advanced-search-user-report',
						'class' => 'typeahead ajaxGenerate',
						'autocomplete' => 'off',
						'data-url' => Docebo::createAppUrl('admin:userManagement/usersAutocomplete'),
						'data-type' => 'core',
						'placeholder' => Yii::t('report', 'Type user here'),
						'data-source-desc' => 'true',
					)); ?>

					<?php
					echo CHtml::link(
						Yii::t('certificate', '_GENERATE'),
						Docebo::createLmsUrl('myActivities/userReport', array('from' => 'reportManagement')),
						array('class' => 'new-user-summary', 'id' => 'generate-user-report')
					);
					?>
					<script>
						$(function() {
							$('#generate-user-report').on('click', function(e) {
								e.preventDefault();

								function verifyInitData() {
									var button = $('#generate-user-report');
									var input = button.parent().find('input');
									var data = input.val();
									if (data == '') {
										input.css('border-color', '#ff0000');
										return false;
									}
									input.css('border-color', '#E4E6E5');
									return true;
								}

								if (verifyInitData()) {
									var url = $(this).attr('href'), userid = $('#advanced-search-user-report').val();
									window.location.href = url+'&userid='+encodeURIComponent(userid);
								}
							});
						});
					</script>

				</div>
		</div>
	</div>
	<div class="summary-course-report clearfix">
		<div class="summary-report-icon course"></div>
			<div class="summary-report-wrapper">
				<div class="summary-report-title"><?php echo Yii::t('report', '_SINGLE_COURSE_SUMMARY'); ?></div>
				<div class="summary-report-description"><?php echo Yii::t('report', 'Single course summary desc'); ?></div>

				<div id="advanced_search_course_report_container" class="summary-report-form clearfix">
					<span><?php echo Yii::t('course', '_COURSE_SELECTION'); ?></span>
		      <?php echo CHtml::textField('LearningCourse[name]', '', array(
						'id' => 'advanced-search-course-report',
						'class' => '',
						'autocomplete' => 'off',
						'data-url' => Docebo::createAppUrl('admin:courseManagement/courseAutocomplete'),
						'data-type' => 'core',
				  		'data-formatted-course-name' => true,
						'placeholder' => Yii::t('report', 'Type course here'),
					)); ?>

					<?php $this->renderPartial('//common/_modal', array(
						'config' => array(
							'class' => 'new-course-summary reportcoursesummary',
							'modalTitle' => Yii::t('report', '_SINGLE_COURSE_SUMMARY'),
							'linkTitle' => Yii::t('certificate', '_GENERATE'),
							'url' => 'reportManagement/createCourseReport',
// 							'buttons' => array(
// 								array(
// 									'type' => 'submit',
// 									'title' => Yii::t('standard', '_CONFIRM'),
// 								),
// 								array(
// 									'type' => 'cancel',
// 									'title' => Yii::t('standard', '_CANCEL'),
// 								),
// 							),
							'buttons' => array(),
							'beforeLoadingContent' => 'function() { if (generateRaportVerifyInitData(config)) { return setCourseSummarySessionId(config); } else { return false; } }',
							'afterLoadingContent' => 'function() {applyTypeahead($(\'.modal.in .typeahead\')); applyDonut(\'.modal.in .donutContainer\'); applyLineCharts(\'.modal.in .lineChartContainer\');}',
							// 'afterSubmit' => 'updateGroupContent',\
						),
					)); ?>
				</div>
		</div>
	</div>
<!-- App7020 reports -->
<?php if(PluginManager::isPluginActive('Share7020App')) { ?>
<div class="summary-course-report clearfix">
		<div class="summary-report-icon expert">
			<span class="fa-stack fa-lg">
				<i class="fa fa-user fa-stack-1x fa-3x"></i>
				<i class="fa fa-star fa-stack-2x"></i>
			</span>
		</div>
		<div class="summary-report-wrapper">
			<div class="summary-report-title"><?php echo Yii::t('report', 'Expert summary'); ?></div>
			<div class="summary-report-description"><?php echo Yii::t('report', 'View an expert summary report and download it as a PDF'); ?></div>

			<div id="advanced_search_expert_report_container" class="summary-report-form clearfix">
			<span><?php echo Yii::t('course', 'Select expert'); ?></span>
			<?php echo CHtml::textField('Expert name', '', array(
					'id' => 'advanced-search-expert-report',
					'class' => 'typeahead ajaxGenerate',
					'placeholder' => Yii::t('report', 'Type expert name here'),
					'data-formatted-course-name' => true,
				)); ?>

				<?php
				echo CHtml::link(
					Yii::t('certificate', '_GENERATE'),
					Docebo::createAppUrl('app7020:expertSummary/index'),
					array('class' => 'new-user-summary', 'id' => 'generate-expert-report')
				);
				?>
			</div>
			
		</div>
	</div>
	
<div class="summary-asset-report clearfix">
	<i class="fa fa-file" aria-hidden="true"></i>
	<div class="summary-report-wrapper">
		<div class="summary-report-title"><?php echo Yii::t('report', 'Asset summary'); ?></div>
		<div class="summary-report-description"><?php echo Yii::t('report', 'View an expert summary report and download it as a PDF'); ?></div>

		<div id="advanced_search_asset_report_container" class="summary-report-form clearfix">
			<span><?php echo Yii::t('app7020', 'Select asset'); ?></span>
			   <?php echo CHtml::textField('App7020Asset[search_input]', '', array(
			 'id' => 'advanced-search-asset-report',
			 'class' => 'typeahead ajaxGenerate',
			 'autocomplete' => 'off',
			 'data-url' => Docebo::createAppUrl('admin:reportManagement/assetsAutocomplete'),
			 'data-type' => 'core',
			 'placeholder' => Yii::t('report', 'Type asset name here'),
			 'data-formatted-course-name' => true,	   
			)); ?>

		 <?php
		 echo CHtml::link(
		  Yii::t('certificate', '_GENERATE'),
		  Docebo::createApp7020Url('assetSummary/index'),
		  array('class' => 'new-user-summary', 'id' => 'generate-assets-report')
		 );
		 ?>
		</div>
   </div>
 </div>
<div class="summary-channel-report clearfix">
	<i class="fa fa-television" aria-hidden="true"></i>
	<div class="summary-report-wrapper">
		<div class="summary-report-title"><?php echo Yii::t('report', 'Channel summary'); ?></div>
		<div class="summary-report-description"><?php echo Yii::t('report', 'View a channel summary report and download it as a PDF'); ?></div>

		<div id="advanced_search_channel_report_container" class="summary-report-form clearfix">
			<span><?php echo Yii::t('app7020', 'Select channel'); ?></span>
			   <?php echo CHtml::textField('App7020Asset[search_input]', '', array(
			 'id' => 'advanced-search-channel-report',
			 'class' => 'typeahead ajaxGenerate',
			 'autocomplete' => 'off',
			 'data-url' => Docebo::createAppUrl('admin:reportManagement/channelsAutocomplete'),
			 'data-type' => 'core',
			 'placeholder' => Yii::t('report', 'Type channel name here'),
			 'data-formatted-course-name' => true,	   
			)); ?>

		 <?php
		 echo CHtml::link(
		  Yii::t('certificate', '_GENERATE'),
		  Docebo::createApp7020Url('channelSummary/index'),
		  array('class' => 'new-user-summary', 'id' => 'generate-channel-report')
		 );
		 ?>
		</div>
   </div>
 </div>
<?php } ?>
<script>
	$(function() {
		$('#generate-assets-report').on('click', function(e) {
			e.preventDefault();

			function verifyInitData() {
				var button = $('#generate-assets-report');
				var input = button.parent().find('input');
				var data = input.val();
				if (data == '') {
					input.css('border-color', '#ff0000');
					return false;
				}
				input.css('border-color', '#E4E6E5');
				return true;
			}

			if (verifyInitData()) {
				var url = $(this).attr('href'), assetId = $('#advanced_search_asset_report_container input[type=hidden]#idAsset').val();
				window.location.href = url+'&asset='+encodeURIComponent(assetId);
			}
		});
	});
</script>
	
</div>