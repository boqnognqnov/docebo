<?php
/* @var $this ReportManagementController */
/* @var $pieChartData array */
/* @var $singlePieChart string */
/* @var $course LearningCourse */
/* @var $session LtCourseSession */

Yii::app()->clientScript->scriptMap['jquery.js'] = false;
Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
?>
<div class="user-report-actions">
	<div id="advanced_search_course_classroom_report_container" class="clearfix">
		<span><?php echo Yii::t('course', '_COURSE_SELECTION'); ?></span>
		<?php echo CHtml::textField('LearningCourse[name]', '', array(
			'id' => 'advanced-search-course-report-modal',
			'class' => '',
			'autocomplete' => 'off',
			'data-url' => Docebo::createAppUrl('admin:courseManagement/courseAutocomplete'),
			'data-type' => 'core',
			'data-formatted-course-name' => true,
			'placeholder' => Yii::t('report', 'Type course here'),
		)); ?>

		<?php $this->renderPartial('//common/_modal', array(
			'config' => array(
				'class' => 'new-user-summary',
				'modalTitle' => Yii::t('report', '_SINGLE_COURSE_SUMMARY'),
				'linkTitle' => Yii::t('certificate', '_GENERATE'),
				'url' => 'reportManagement/createCourseReport',
				'buttons' => array(),
				'beforeLoadingContent' => 'function() { if(generateRaportVerifyInitData(config)) {$(\'.modal.in\').modal(\'hide\'); return setCourseSummarySessionId(config);} else {return false;}}',
				'afterLoadingContent' => 'function() {applyTypeahead($(\'.modal.in .typeahead\'));applyDonut(\'.modal.in .donutContainer\'); applyLineCharts(\'.modal.in .lineChartContainer\');}',
				// 'afterSubmit' => 'updateGroupContent',
			),
		)); ?>

		<div class="report-popup-actions">
			<?php echo CHtml::link(Yii::t('report', 'Print') . '<span></span>', 'javascript:void(0);', array('class' => 'report-action-print')); ?>
			<?php echo CHtml::link(Yii::t('report', 'Download as PDF') . '<span></span>', Yii::app()->createAbsoluteUrl('reportManagement/createCourseReport', array('id' => $course->idCourse, 'session_id' => $session->getPrimaryKey(), 'createPdf' => true)), array('class' => 'report-action-pdf')); ?>
		</div>
	</div>
</div>

<div class="printable">
<div class="clearfix">
	<h2 class="course-title pull-right" style="position: relative; top: 10px;"><?php echo $course->name; ?></h2>
	<?php echo CHtml::image(Yii::app()->theme->getLogoUrl(), '', array(
		'class' => 'course-report-logo-img'
	)); ?>
</div>
<hr style="margin: 10px 0;"/>

<div class="course-report-session-selector">
	<?=
	CHtml::label(Yii::t('classroom', 'Session') . ': ' .
		CHtml::dropDownList('session_id', ($session->isNewRecord ? '' : $session->getPrimaryKey()), CHtml::listData($course->getSessions(), 'id_session', function ($session) {
			return $session->renderDetailedSessionName(' - ');
		}), array('empty' => Yii::t('classroom', 'All sessions')))
		, 'session_id', array('class' => 'clearfix')) ?>
</div>
<hr style="margin: 10px 0;"/>

<!--course summary-->
<div class="course-report-summary clearfix">
	<div class="course-report-info-image">
		<?php echo CHtml::image($course->courseLogoUrl, '', array('width' => 150)); ?>
	</div>
	<div class="course-report-right-side clearfix">
		<div class="text-right courses-stat <?php echo(Yii::app()->chart->isIE8() ? 'IE8' : ''); ?>"
				 style="margin-top: 10px;">
			<div class="completed">
				<p><?php echo Yii::t('stats', 'Users that have passed the session(s)'); ?></p>

				<div class="completed-stat donut-wrapper">
					<?php echo $singlePieChart; ?>
				</div>
			</div>
			<div class="enrolled-users">
				<?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/report/enrolled-users.png'); ?>
				<div class="data"><?= $pieChartData['total'] ?></div>
				<div class="label"><?php echo Yii::t('standard', 'Enrolled users'); ?></div>
			</div>
			<div class="total-session-hours">
				<?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/report/session-hours.png'); ?>
				<div class="data"><?= $pieChartData['totalHours'] ?></div>
				<div class="label"><?php echo Yii::t('stats', 'Total session hours'); ?></div>
			</div>
		</div>
	</div>
</div>
<!--course summary end-->

	<?php
	$event = new DEvent($this, array('controller' => $this, 'courseModel' => $courseModel));
	Yii::app()->event->raise('OnCourseSummaryRender', $event);
	?>


<?php if (!$session->isNewRecord) : ?>

	<div class="course-report-session-details">
		<h2 class="title"><?php echo Yii::t('statistic', '_VIEW_SESSION_DETAILS'); ?></h2>

		<div class="row-fluid">
			<div class="span6 grid-view">
				<h4><?= Yii::t('standard', '_SCHEDULE') ?></h4>
				<table class="items">
					<thead>
					<tr>
						<th><?= Yii::t('standard', '_NAME') ?></th>
						<th><?= Yii::t('standard', '_DATE') ?></th>
						<th><?= Yii::t('report', '_TOTAL_SESSION') ?></th>
					</tr>
					</thead>
					<tbody>
					<?php foreach ($session->getDates() as $date) : ?>
						<?php /* @var $date LtCourseSessionDate */ ?>
						<tr>
							<td><?= $date->name ?></td>
							<td><?= $date->day ?></td>
							<td><?= $date->renderDaySchedule(' | ') ?></td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
			</div>
			<div class="span6" style="padding-top: 15px;">
				<?php
				/* @var $classroom LtClassroom */
				$classroom = null;
				$sessionDates = $session->getDates();
				if (!empty($sessionDates)) {
					$firstDate = $sessionDates[0];
					$classroom = $firstDate->ltClassroom;
				}
				?>

				<?php if ($classroom !== null) : ?>
					<h4><?= Yii::t('classroom', 'Location & Classroom information') ?></h4>

					<div class="row-fluid">
						<div class="span6">
							<div class="info-section">
								<div class="info-section-icon">
									<span class="classroom-sprite marker-tiny"></span>
								</div>
								<div class="info-section-content has-icon">
                                    <?php if(!empty($classroom->ltLocation)): ?>
									<?= $classroom->ltLocation->name ?><br/>
									<?= $classroom->ltLocation->renderAddress() ?>
                                    <?php endif; ?>
								</div>
							</div>
						</div>
						<div class="span6">
							<?php if (!empty($classroom->ltLocation->telephone)) : ?>
								<div class="info-section">
									<div class="info-section-icon">
										<span class="classroom-sprite phone"></span>
									</div>
									<div class="info-section-content has-icon"
											 style="padding-top: 3px;"><?= $classroom->ltLocation->telephone ?><br/></div>
								</div>
							<?php endif; ?>
							<?php if (!empty($classroom->ltLocation->email)) : ?>
								<div class="info-section">
									<div class="info-section-icon">
										<span class="classroom-sprite mail"></span>
									</div>
									<div class="info-section-content has-icon text-colored"><?= $classroom->ltLocation->email ?></div>
								</div>
							<?php endif; ?>
						</div>
					</div>

					<div class="row-fluid">
						<div class="span6">
							<div class="info-section">
								<div class="info-section-icon">
									<span class="classroom-sprite classroom"></span>
								</div>
								<div class="info-section-content has-icon">
									<?= $classroom->name ?><br/>
									<?= DoceboHtml::readmore($classroom->details, 200) ?>
								</div>
							</div>
						</div>
						<div class="span6"></div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<div class="user-report-session-statistics">
		<h2 class="course-title" style="margin: 20px 0 10px;"><?php echo Yii::t('stats', 'Session statistics'); ?></h2>

		<div class="courses-grid-wrapper">
			<?php $this->widget('DoceboCGridView', array(
				'id' => 'session-statistics-grid',
				'htmlOptions' => array('class' => 'grid-view clearfix', 'style' => 'padding-top: 0;border-top: 1px solid #E4E6E5;'),
				'dataProvider' => LtCourseuserSession::model()->dataProviderUserStatistics($session->getPrimaryKey()),
				'cssFile' => false,
				'template' => '<div class=scroll-content>{items}</div>{summary}{pager}',
				'summaryText' => Yii::t('standard', '_TOTAL'),
				'pager' => array(
					'class' => 'DoceboCLinkPager',
					'maxButtonCount' => 8,
				),
				'ajaxUpdate' => 'all-items',
				'beforeAjaxUpdate' => 'function(id, options) {
					options.type = "POST";
					if (options.data == undefined) {
						options.data = {
							contentType: "html",
						}
					}
				}',
				'columns' => array(
					array(
						'header' => Yii::t('standard', '_USERNAME'),
						'name' => 'user.userid',
						'value' => 'Yii::app()->user->getRelativeUsername($data->learningUser->userid)',
					),
					array(
						'header' => Yii::t('standard', '_ATTENDANCE'),
						'name' => 'session.attendance',
						'value' => '$data->ltCourseSession->renderAttendance($data->learningUser->idst)',
					),
					array(
						'name' => 'status',
						'value' => '$data->renderEvaluationStatus()'
					),
					array(
						'name' => 'score',
						'header' => Yii::t('standard', '_SCORE'),
						'value' => '$data->renderSessionScore()'
					),
				),
                'loadIntoDialog' => true
			)); ?>
		</div>
	</div>

<?php else: ?>

	<div class="user-report-session-statistics">
		<h2 class="course-title" style="margin: 20px 0 10px;"><?php echo Yii::t('stats', 'Session statistics'); ?></h2>

		<div class="courses-grid-wrapper">

			<?php if (!empty($course->learningCourseSessions)) : ?>
				<?php $this->widget('DoceboCGridView', array(
					'id' => 'session-statistics-grid',
					'htmlOptions' => array('class' => 'grid-view clearfix', 'style' => 'padding-top: 0;border-top: 1px solid #E4E6E5;'),
					'dataProvider' => LtCourseuserSession::model()->dataProviderCourseStatistics($course->getPrimaryKey()),
					'cssFile' => false,
					'template' => '<div class=scroll-content>{items}</div>{summary}{pager}',
					'summaryText' => Yii::t('standard', '_TOTAL'),
					'pager' => array(
						'class' => 'DoceboCLinkPager',
						'maxButtonCount' => 8,
					),
					'ajaxUpdate' => 'all-items',
					'beforeAjaxUpdate' => 'function(id, options) {
                        options.type = "POST";
                        if (options.data == undefined) {
                            options.data = {
                                contentType: "html",
                            }
                        }
                    }',
					'columns' => array(
						array(
							'header' => Yii::t('standard', '_USERNAME'),
							'name' => 'user.userid',
							'value' => 'Yii::app()->user->getRelativeUsername($data->learningUser->userid)',
						),
						array(
							'header' => Yii::t('classroom', 'Session name'),
							'name' => 'session.name',
							'value' => '$data->ltCourseSession->renderDetailedSessionName()',
							'type' => 'raw'
						),
						array(
							'header' => Yii::t('standard', '_ATTENDANCE'),
							'name' => 'session.attendance',
							'value' => '$data->ltCourseSession->renderAttendance($data->id_user)',
						),
						array(
							'name' => 'session.status',
							'value' => '$data->renderEvaluationStatus()'
						),
						array(
							'name' => 'session.score',
							'header' => Yii::t('standard', '_SCORE'),
							'value' => '$data->renderSessionScore()'
						),
					),
                    'loadIntoDialog' => true
				)); ?>
			<?php else: ?>
				<?php $this->widget('DoceboCGridView', array(
					'id' => 'session-statistics-grid',
					'htmlOptions' => array('class' => 'grid-view clearfix', 'style' => 'padding-top: 0;border-top: 1px solid #E4E6E5;'),
					'dataProvider' => new CArrayDataProvider(array()),
					'cssFile' => false,
					'template' => '<div class=scroll-content>{items}</div>{summary}{pager}',
					'summaryText' => Yii::t('standard', '_TOTAL'),
					'pager' => array(
						'class' => 'DoceboCLinkPager',
						'maxButtonCount' => 8,
					),
					'ajaxUpdate' => 'all-items',
					'beforeAjaxUpdate' => 'function(id, options) {
					options.type = "POST";
					if (options.data == undefined) {
						options.data = {
							contentType: "html",
						}
					}
				}',
					'columns' => array(
						array(
							'header' => Yii::t('standard', '_USERNAME'),
							'name' => 'user.userid',
						),
						array(
							'header' => Yii::t('classroom', 'Session name'),
							'name' => 'session.name',
						),
						array(
							'header' => Yii::t('standard', '_ATTENDANCE'),
							'name' => 'session.attendance',
						),
						array(
							'name' => 'session.status',
						),
						array(
							'name' => 'session.score',
							'header' => Yii::t('standard', '_SCORE'),
						),
					),
                    'loadIntoDialog' => true
				)); ?>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>

</div> <!-- End of .printable -->


<script type="text/javascript">
	var oClassroomCourseReport = {
		init: function () {
			applyDonut('.donutContainer');

			$('.report-action-print').click(function () {
				if ($(this).hasClass('disabled')) return false;
				window.print();
			});
			replacePlaceholder();
			$('.scroll-content').jScrollPane({
				autoReinitialise: true
			});

			if ($('.course-report-session-selector select').val().length) {
				$('.report-popup-actions a').removeClass('disabled');
			} else {
				$('.report-popup-actions a').addClass('disabled');
			}
			$('.course-report-session-selector select').change(function () {
				var sessionId = $(this).val();
				$('.donutContainer').html('');
				$('.summary-report-form .new-course-summary').data('course-id', '<?=  $course->idCourse ?>');
				$('.summary-report-form .new-course-summary').data('session-id', sessionId).click();
			});
		}
	};
	$(document).ready(oClassroomCourseReport.init);

	$(document).ready(function(){

    $('body').on('show', '.modal', function(){
        var heightModal = parseInt($('.modal').height() * 1.2);
        $('body').css('height', heightModal);
        $('.modal').css('position', 'absolute');
    });

		$('.modal').on('hide', function () {
			id = '#'+$(this).attr('id');
			$(id).find('#advanced_search_course_classroom_report_container').remove();
		});

		$("#advanced-search-course-report-modal").autocomplete({
			source: function( request, response ) {
				$.ajax({
					url: "<?=Docebo::createAppUrl('admin:courseManagement/courseAutocomplete')?>",
					dataType: "json",
					type: "POST",
					data: {
						data:{
							formattedCourseName: true,
							query: request.term
						}
					},
					success: function( data ) {
						var options = [];
						$.each(data.options, function(index, value){
							options.push({
								"value" : index,
								"label" : value
							})
						});
						response( options );
					}
				});
			},
			minLength: 1,
			select: function( event, ui ) {
				if ($('#advanced_search_course_classroom_report_container').find('#idCourse')) {
					$('#advanced_search_course_classroom_report_container').find('#idCourse').remove();
				}
				var hiddenField = $('<input type="hidden" id="idCourse" name="idCourse" value="' + ui.item.value + '"/>');
				$("#advanced_search_course_classroom_report_container").append(hiddenField);
				ui.item.value = ui.item.label;
			}
		});

	});
</script>
