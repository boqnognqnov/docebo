<?php
// Stop jQuery loading
Yii::app()->clientScript->scriptMap['jquery.js'] = false;
Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;

// If user can EDIT reports
$canModReports 	= (Yii::app()->user->isGodAdmin || (Yii::app()->user->isAdmin && Yii::app()->user->checkAccess('/lms/admin/report/mod')));


$form = $this->beginWidget('CActiveForm', array(
    'id' => 'report-management-form',
	'htmlOptions' => array(
		'class' => 'ajax-grid-form',
		'data-grid' => '#report-management-grid'
	),
));

?>

<div class="main-section report-management">

    <div class="row-fluid">
        <div class="span6">
            <h3><span><?=Yii::t('report', '_CREATE_CUSTOM_REPORT')?></span></h3>
        </div>
        <div class="span6 text-right">
            <?php
				if($canModReports)
				{
					$title = '<span class="icon-create-new-report"></span>'.Yii::t('report', '_CREATE_CUSTOM_REPORT');
					echo CHtml::link($title, $wizardUrl, array(
						'class' => 'open-dialog btn btn-docebo green big',
						'rel' 	=> $wizardDialogId,
						'data-dialog-title' => Yii::t('report', '_CREATE_CUSTOM_REPORT'),
						'title'	=> Yii::t('report', '_CREATE_CUSTOM_REPORT')
					));
				}
            ?>
        </div>
    </div>


	<div class="filters-wrapper">
        <div class="filters clearfix">
			<?php if ($canModReports): ?>
        		<div class="select-wrapper">
        			<label for="LearningReportFilter_Created_by"><?= Yii::t('report', '_TAB_REP_CREATOR'); ?></label>
        			<?php echo $form->dropDownList($model, 'author', $users, array('empty' => '(' . Yii::t('standard', '_ALL') . ')', 'class' => 'ajaxChangeSubmit')); ?>
        		</div>
			<?php endif; ?>

			<div class="input-wrapper">
				<?php
					echo $form->textField($model, 'filter_name', array(
						'id' => 'advanced-search-report',
						'class' => 'typeahead',
						'autocomplete' => 'off',
						'data-url' => Docebo::createAppUrl('admin:reportManagement/reportAutocomplete'),
						'placeholder' => Yii::t('standard', '_SEARCH'),
					));
				?>
				<span>
					<button class="search-btn-icon"></button>
				</span>
			</div>
		</div>
	</div>
</div>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>


<?php

	// Grid column value : EDIT REPORT
	$title = '<span></span>'.Yii::t('standard', '_MOD');
	$editReportButtonLinkValue = 'CHtml::link("' . $title . '","' . $wizardUrl . '&idReport=" . "$data->id_filter", array(
        "class" => "open-dialog edit-action tooltipize",
        "rel" 	=> "' . $wizardDialogId . '",
        "data-dialog-title" => "'.$title.'",
        "title"	=> Yii::t("standard", "_MOD")
    ));';

	//in cases of some modules activated, the PU cannot publish their reports, so we need to check with an event
	$publishReportPerm = true;

	if (Yii::app()->user->isAdmin)
	{
		$event = new DEvent($this, array());
		Yii::app()->event->raise('PowerUserPreventReportPublish', $event);

		if (!$event->shouldPerformAsDefault())
			$publishReportPerm = false;
	}

?>

<div class="bottom-section clearfix">
	<div id="grid-wrapper">
		<?php

		$columns = array();

		$columns[] = 'filter_name';

		$columns[] = array(
			'header' => Yii::t('report', '_TAB_REP_CREATOR'),
			'type' => 'raw',
			'value' => 'Yii::app()->user->getRelativeUserName($data->user->userid);',
		);

		$columns[] = 'creation_date';
		$columns[] = array(
			'header' => Yii::t('dashboard', 'Filters'),
			'type'=>'raw',
			'value'=>function ($data) {
				$isOwner = $data->author == Yii::app()->user->id;
				$accessType = $data->renderNewAccessStatus($isOwner);
				return $accessType;
			},
			'htmlOptions' => array('class' => 'center-aligned'),
			'headerHtmlOptions' => array('class' => 'center-aligned'),
		);

		if($canModReports)
		{
			$columns[] = array(
				'header' 	=> Yii::t('conference', 'Scheduled'),
				'type'		=> 'raw',
				'value'		=> array($this, 'renderScheduledColumn'),
				'htmlOptions' => array('class' => 'center-aligned'),
				'headerHtmlOptions' => array('class' => 'center-aligned'),
			);

//			$columns[] = array(
//				'header' => Yii::t('report', '_TAB_REP_PUBLIC'),
//				'type' => 'raw',
//				'value' => 'Yii::app()->user->getIsGodadmin() || (Yii::app()->user->getIsAdmin() && $data->author==Yii::app()->user->getId()) ? CHtml::link("", "javascript:void(0);",
//						array(
//							"class" => "docebo-sprite ico-circle-check tooltipize toggle-active-state " . ($data->is_public == 1 ? "green" : ""),
//							"data-url" => "reportManagement/toggleReportPublic",
//							"data-id" => $data->id_filter,
//							"title" => Yii::t("standard", "_ACTIVATE") . "/" . Yii::t("standard", "_DEACTIVATE"),
//						)) : NULL;',
//				'htmlOptions' => array('class' => 'center-aligned'),
//				'headerHtmlOptions' => array('class' => 'center-aligned'),
//				'visible' => $publishReportPerm,
//			);
		}

		$columns[] = array(
			'type' => 'raw',
			'value' => 'CHtml::link("View", array(\'reportManagement/customView\', \'id\' => $data->id_filter),
							array(
								"class" => "tooltipize docebo-sprite ico-magnifier",
								"data-id" => $data->id_filter,
								"title" => Yii::t("standard", "_VIEW"),
							));',
			'headerHtmlOptions' => array('class' => 'button-column-single'),
			'htmlOptions' => array(
				'class' => 'button-column-single',
			)
		);

		$columns[] = array(
			'header' => '',
			'type' => 'raw',
			'value' => '$data->renderExportActions();',
			'htmlOptions' => array('class' => 'export'),
			'headerHtmlOptions' => array('class' => 'button-column-single'),
			'htmlOptions' => array(
				'class' => 'button-column-single report-export-actions',
			)
		);

		if($canModReports)
		{
			$columns[] = array(
				'type' => 'raw',
				'value' => array($this, 'renderReportManageActions'),
				'htmlOptions' => array('class' => 'button-column-single report-manage-actions')
			);
		}

		//$this->widget('DoceboCGridView', array(
		$this->widget('zii.widgets.grid.CGridView', array(
		//$this->widget('adminExt.local.widgets.CXGridView', array(
			'id' => 'report-management-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
			'dataProvider' => $model->dataProvider(),
			'template' => '{items}{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),

			'pager' => array(
				'class' => 'DoceboCLinkPager',
				'maxButtonCount' => 8,
			),

			'afterAjaxUpdate' => 'function() {
				$(document).controls();
			 	$(\'a.tooltipize\').tooltip();
			}',
			'beforeAjaxUpdate' => 'function(id, options) {
				options.type = "POST";
				options.data = $("#report-management-form").serialize()

			}',

			// 'ajaxUpdate' => 'all-items',
			'columns' => $columns,
		)); ?>
	</div>
</div>
<script>
	$(function(){
		$(document).controls();
	});
</script>
