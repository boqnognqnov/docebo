<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700" />
<!--<link type="text/css" rel="stylesheet"  href="<?php //echo Yii::app()->getBaseUrl(true); ?>/css/report-pdf.css" rel="stylesheet">-->
<?php
    /* @var $this ReportManagementController */
    /* @var $pieChartData array */
    /* @var $singlePieChart string */
    /* @var $course LearningCourse */
    /* @var $session LtCourseSession */

	Yii::app()->clientScript->scriptMap['jquery.js'] = false;
	Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
	Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;
	Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
?>
<div class="printable">
    <table width="100%">
        <tr>
            <td width="50%">
                <?php echo CHtml::image(Yii::app()->theme->getLogoUrl(), '', array(
									'class' => 'course-report-logo-img'
								)); ?>
            </td>
            <td width="50%">
                <h2 class="course-title"><?php echo $course->name; ?></h2>
            </td>
        </tr>
    </table>
	<hr/>
	<!--course summary-->
    <table>
        <tbody class="<?php echo (Yii::app()->chart->isIE8()?'IE8':''); ?>">
            <tr>
                <td><?= Yii::t('classroom', 'Session:') ?></td>
                <td><?= $session->renderDetailedSessionName(' - ') ?></td>
            </tr>
            <tr>
                <td class="image"><?php echo CHtml::image($course->courseLogoUrl, '', array('width' => 150)); ?></td>
                <td>
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="completed" valign="middle"><?php echo Yii::t('stats', 'Users that have passed the session(s)'); ?></td>
                                            <td class="completed-stat" valign="middle"><?php echo $singlePieChart; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td class="center-aligned">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td><?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/enrolled-users.png');?></td>
                                        </tr>
                                        <tr>
                                            <td class="data"><?php echo $pieChartData['total']; ?></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo Yii::t('standard', 'Enrolled users'); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td class="center-aligned">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td><?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/session-hours.png');?></td>
                                        </tr>
                                        <tr>
                                            <td class="data"><?php echo $pieChartData['totalHours']; ?></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo Yii::t('stats', 'Total session hours'); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

    <div class="course-report-session-details">
        <h2 class="title"><?php echo Yii::t('statistic', '_VIEW_SESSION_DETAILS'); ?></h2>

        <div class="row-fluid">
            <div class="span6 grid-view">
                <h4><?= Yii::t('standard', '_SCHEDULE') ?></h4>
                <table class="items">
                    <thead>
                    <tr>
                        <th><?= Yii::t('standard', '_NAME') ?></th>
                        <th><?= Yii::t('standard', '_DATE') ?></th>
                        <th><?= Yii::t('report', '_TOTAL_SESSION') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($session->getDates() as $date) : ?>
                        <?php /* @var $date LtCourseSessionDate */ ?>
                        <tr>
                            <td><?= $date->name ?></td>
                            <td><?= $date->day ?></td>
                            <td><?= $date->renderDaySchedule(' | ') ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="span6" style="padding-top: 15px;">
                <?php
                /* @var $classroom LtClassroom */
                $classroom = null;
                $sessionDates = $session->getDates();
                if (!empty($sessionDates)) {
                    $firstDate = $sessionDates[0];
                    $classroom = $firstDate->ltClassroom;
                }
                ?>

                <?php if ($classroom!==null) : ?>
                    <h4><?= Yii::t('classroom', 'Location & Classroom information') ?></h4>

                    <div class="row-fluid">
                        <div class="span6">
                            <div class="info-section">
                                <div class="info-section-icon">
                                    <span class="classroom-sprite marker-tiny"></span>
                                </div>
                                <div class="info-section-content has-icon">
                                    <?= $classroom->ltLocation->name ?><br/>
                                    <?= $classroom->ltLocation->renderAddress() ?>
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <?php if (!empty($classroom->ltLocation->telephone)) : ?>
                                <div class="info-section">
                                    <div class="info-section-icon">
                                        <span class="classroom-sprite phone"></span>
                                    </div>
                                    <div class="info-section-content has-icon" style="padding-top: 3px;"><?= $classroom->ltLocation->telephone ?><br/></div>
                                </div>
                            <?php endif; ?>
                            <?php if (!empty($classroom->ltLocation->email)) : ?>
                                <div class="info-section">
                                    <div class="info-section-icon">
                                        <span class="classroom-sprite mail"></span>
                                    </div>
                                    <div class="info-section-content has-icon text-colored"><?= $classroom->ltLocation->email ?></div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span6">
                            <div class="info-section">
                                <div class="info-section-icon">
                                    <span class="classroom-sprite classroom"></span>
                                </div>
                                <div class="info-section-content has-icon">
                                    <?= $classroom->name ?><br/>
                                    <?= DoceboHtml::readmore($classroom->details, 200) ?>
                                </div>
                            </div>
                        </div>
                        <div class="span6"></div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div> <!-- End of .printable -->