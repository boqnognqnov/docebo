<?php 

$this->breadcrumbs = array(
		Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
		Yii::t('standard', '_REPORTS') => array('reportManagement/index'),
		($reportFilterModel->is_standard) ? Yii::t("report", $reportFilterModel->filter_name) : CHtml::encode($reportFilterModel->filter_name),
);

?>

 
<h3 class="title-bold back-button">
	<?php echo CHtml::link(Yii::t('standard', '_BACK'), $back); ?>
	<span><?php echo ($reportFilterModel->is_standard) ? Yii::t("report", $reportFilterModel->filter_name) : CHtml::encode($reportFilterModel->filter_name); ?></span>
</h3>


<div class="report-bottom-side">
	<div class="filters-wrapper">
		<div class="filters clearfix">
			<div class="select-wrapper">
				<label><?php echo Yii::t('standard', '_EXPORT'); ?></label>
				<?php
					$actions = LearningReportFilter::exportTypes();

					Yii::app()->event->raise('ReportExportTypes', new DEvent($this, array(
						'actions' => &$actions,
						'report_filter' => $reportFilterModel,
					)));
					echo CHtml::dropDownList('custom_report_export_select', false, $actions, 
						array(
							'id' => 'custom-report-export-select', 
							'empty' => Yii::t('standard', 'Select action'), 
							'data-report-id' => $reportFilterModel->id_filter
						)
					);
				?>
			</div>

			<?=CHtml::form(Yii::app()->request->requestUri, 'post', array('class'=>'report-custom-filters'))?>
				<? Yii::app()->event->raise('getAdditionalCustomReportFilters', new DEvent($this, array(
					'model'=>$reportFilterModel
				)));?>
			<?=CHtml::endForm()?>
		</div>
	</div>
</div>



<div class="custom-report-wrapper">
<?php
if ($dataProvider) {
	$event = new DEvent($this, array('dataProvider' => $dataProvider, 'columns' => $gridColumns));
	if (Yii::app()->event->raise('OnBeforeRenderCustomReportGridView', $event)) {

		$rowHtmlOptionsExpression = '';
		if (isset($gridColumns[LearningReportFilter::F_COURSE_EXPIRED]))
			$rowHtmlOptionsExpression = 'isset($data["course.expired"]) && $data["course.expired"] == "_YES" ? array("class" => "courseHasExpired") : "" ';

		$this->widget('common.components.DoceboCGridView', array(
			'id' => 'custom-report-grid',
			'dataProvider' => $dataProvider,
			'columns' => $gridColumns,
			'rowHtmlOptionsExpression' => $rowHtmlOptionsExpression,
			'beforeAjaxUpdate' => 'function(id, options) {
				options.type = "POST";
				options.data = $(".report-custom-filters").serialize()
			}',
		));
	}
}
?>

</div>


<?php 
	$progressUrl = Docebo::createAbsoluteLmsUrl('progress/run', array(
		'type'		=> Progress::JOBTYPE_CUSTOM_REPORT,
		'idReport' 	=> $_REQUEST['id'],
	));
?>

<script type="text/javascript">

	$(function(){
	
		$('#custom-report-export-select').on('change', function(){
 			if ($(this).val() == '') {
 				return true; 
 			}
			// If plugins added any custom filters/inputs
			var exportData = $('form.report-custom-filters').serialize();

			var url = '<?= $progressUrl ?>' + '&exportType=' + $(this).val(); 
		    $('<div/>').dialog2({
		        title: '',
		        content: url,
			    ajaxType: 'post',
			    data: exportData,
		        id: "progressive-report-export-dialog"
		    });
			return true;
			
		});
	
	
	});



</script>
	