<style>
	<!--
    .semi-bold {font-weight: 600;}
    .semi-bold.title{font-size:15px;}
    .filter-box {border: 1px solid #DADADA;}
    .filter-box div.row-fluid.filter-box-row {
        padding-left: 20px;
        width: 95.8%;
    }
    .filter-box div.row-fluid.filter-box-row.odd {
        background-color: #F9F9FA;
        border-top: 1px solid #DADADA;
        padding-top: 14px;
        padding-bottom: 14px;
        padding-left: 27px;
    }
    .modal.report-wizard-step-4 {
        width: 960px;
        margin-<?= (Lang::isRTL(Yii::app()->getLanguage())) ? 'right' : 'left'; ?>: -480px;
        max-height: 700px;
    }

    .modal.report-wizard-step-4 .modal-body {
		height: 510px;
		max-height: 510px;
		padding: 30px;
		overflow-y: auto;
		overflow-x: hidden;
    }


    .modal.report-wizard-step-4 .filter-row {
		background: none repeat scroll 0 0 #F1F3F2;
		padding: 5px;
		margin-right: 5px;
    }

    .modal.report-wizard-step-4 .fields-column-heading {
		height: 35px;
    }
	-->
</style>


<?php
echo CHtml::beginForm('', 'POST', array(
	'id' => 'report-wizard-step-4-form',
	'name' => 'report-wizard-step-4-form',
	'class' => 'ajax jwizard-form form-horizontal',
));

// This input is added as FIRST input, to prevent Bootstrap Datepicker to open
echo CHtml::textField('dummy_ignore', '', array('style' => 'display: none;'));

echo CHtml::hiddenField('from_step', 'step4');
echo CHtml::hiddenField('idReportType', $_REQUEST['idReportType'], array('class' => 'jwizard-no-restore'));
echo CHtml::hiddenField('idReport', $_REQUEST['idReport']);


$dpFormat = Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short'));
?>	


<!-- OPTIONS -->
<?php
$m = new ReportFieldsForm();
$allFields = $m->prepareFields($_REQUEST['idReportType']);

?>


<div class="step4-container">

	<!-- FILTERING FIELDS -->

	<?php
	$isCustomReport = false;
	$event = new DEvent($this, array());
	Yii::app()->event->raise('CheckForTypeOfReport', $event);
	if (!$event->shouldPerformAsDefault()) {
		$isCustomReport = true;
	}
	?>
	<?php if ($_REQUEST['idReportType'] != LearningReportType::GROUPS_COURSES) : ?>

		<div class="row-fluid">

			<div class="span3">
				<h3><?php echo Yii::t('standard', '_FILTER'); ?></h3>
			</div>

			<div class="span9">

				<?php if ($_REQUEST['idReportType'] == LearningReportType::USERS_COURSES || $_REQUEST['idReportType'] == LearningReportType::COURSES_USERS || $_REQUEST['idReportType'] == LearningReportType::USERS_LEARNING): ?>
					<?php
					$this->renderPartial('wizard/_course_enrollment_conditions', array(
						'dpFormat' => $dpFormat,
						'form' => $form,
						'firstConditionText' => Yii::t('standard', 'Enrollment date'),
						'secondConditionText' => Yii::t('standard', 'Completion date')
					))
					?>
				<?php endif; ?>
				<?php if ($_REQUEST['idReportType'] == LearningReportType::USERS_SESSION): ?>
					<?php
					$this->renderPartial('wizard/_course_enrollment_conditions', array(
						'dpFormat' => $dpFormat,
						'form' => $form,
						'firstConditionText' => Yii::t('standard', 'Enrollment date'),
						'secondConditionText' => Yii::t('standard', 'Completion date')
					))
					?>
				<?php endif; ?>
				<?php if ($_REQUEST['idReportType'] == LearningReportType::CERTIFICATION_USERS || $_REQUEST['idReportType'] == LearningReportType::USERS_CERTIFICATION): ?>
					<?php
					$this->renderPartial('wizard/_course_enrollment_conditions', array(
						'dpFormat' => $dpFormat,
						'form' => $form,
						'firstConditionText' => Yii::t('certification', 'Certification date'),
						'secondConditionText' => Yii::t('certification', 'Expiration date')
					))
					?>
				<?php endif; ?>
				<?php if ($_REQUEST['idReportType'] == LearningReportType::USERS_BADGES): ?>
					<div class="row-fluid" style="margin-bottom: 10px;"><?= Yii::t('gamification', 'Issued on') ?></div>
					<div class="row-fluid">
						<div class="span4"> <!-- Begin Date -->
							<?= ucfirst($m->getAttributeLabel('date_from')) ?>
							<div class='input-append date' data-date-format="<?= $dpFormat ?>" data-date-language= "<?= Yii::app()->getLanguage() ?>" style="margin-left: 5px; width: 110px">
								<?php echo CHtml::textField('ReportFieldsForm[filters][date_from]', $form['filters']['date_from'], array('class' => 'span12')); ?>
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>

						</div>
						<div class="span4"> <!-- End Date -->
							<?= ucfirst($m->getAttributeLabel('date_to')) ?>

							<div class='input-append date' data-date-format="<?= $dpFormat ?>" data-date-language= "<?= Yii::app()->getLanguage() ?>" style="margin-left: 5px; width: 110px">
								<?php echo CHtml::textField('ReportFieldsForm[filters][date_to]', $form['filters']['date_to'], array('class' => 'span12')); ?>
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>

						</div>
						<div class="span6">
						</div>
					</div>
				<?php endif; ?>

				<?php if ($_REQUEST['idReportType'] == LearningReportType::USERS_COURSES || $_REQUEST['idReportType'] == LearningReportType::USERS_COURSEPATH || $_REQUEST['idReportType'] == LearningReportType::USERS_SESSION || $isCustomReport) : ?>

					<?php if ($_REQUEST['idReportType'] == LearningReportType::USERS_COURSES): ?>
						<div class="row-fluid">
							<p><?= Yii::t('standard', 'Enrollment status') ?></p>
						</div>
						<br>
					<?php endif; ?>
					<div class="row-fluid">

						<div class="span12">

							<?php
							if (!$form['filters']['subscription_status']) {
								$form['filters']['subscription_status'] = 'all';
							}
							?>

							<label class="radio pull-left" for="r_1">
								<?= CHtml::radioButton('ReportFieldsForm[filters][subscription_status]', $form['filters']['subscription_status'] == 'all', array('value' => 'all', 'id' => 'r_1', 'uncheckValue' => null)) . ' ' . Yii::t('standard', '_ALL') . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ?>
							</label>

							<label class="radio pull-left" for="r_2">
								<?= CHtml::radioButton('ReportFieldsForm[filters][subscription_status]', $form['filters']['subscription_status'] == 'not_started', array('value' => 'not_started', 'id' => 'r_2', 'uncheckValue' => null)) . ' ' . Yii::t('standard', '_NOT_STARTED') . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ?>
							</label>

							<label class="radio pull-left" for="r_3">
								<?= CHtml::radioButton('ReportFieldsForm[filters][subscription_status]', $form['filters']['subscription_status'] == 'in_progress', array('value' => 'in_progress', 'id' => 'r_3', 'uncheckValue' => null)) . ' ' . Yii::t('standard', '_USER_STATUS_BEGIN') . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ?>
							</label>
							<?php if ($_REQUEST['idReportType'] == LearningReportType::USERS_COURSES || $_REQUEST['idReportType'] == LearningReportType::USERS_COURSEPATH): ?>
								<label class="radio pull-left" for="r_4">
									<?= CHtml::radioButton('ReportFieldsForm[filters][subscription_status]', $form['filters']['subscription_status'] == 'completed', array('value' => 'completed', 'id' => 'r_4', 'uncheckValue' => null)) . ' ' . Yii::t('standard', 'Completed') . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ?>
								</label>
							<?php endif; ?>
						</div>

					</div>
					<br>

					<?php if ($_REQUEST['idReportType'] != LearningReportType::USERS_COURSEPATH) { ?>
						<div class="row-fluid">
							<div class="span5">
								<?= $m->getAttributeLabel('courses_expiring_in') ?>
							</div>
							<div class="span7">
								<?php
								echo CHtml::textField('ReportFieldsForm[filters][courses_expiring_in]', $form['filters']['courses_expiring_in'], array(
									'style' => 'width: 100px; height: 28px'
								));
								?>
								&nbsp;&nbsp;<?= Yii::t('standard', 'day(s)') ?>
							</div>

				</div>
				<div class="row-fluid">
					<div class="span5">
						<?= $m->getAttributeLabel('courses_expiring_before') ?>
					</div>
					<div class="span7">
						<div class='input-append date' data-date-format="<?= $dpFormat ?>" data-date-language= "<?= Yii::app()->getLanguage() ?>">
							<?php echo CHtml::textField('ReportFieldsForm[filters][courses_expiring_before]', $form['filters']['courses_expiring_before'], array(
								'style' => 'width: 100px; height: 28px'
							)); ?>
							<span class="add-on"><i class="icon-calendar"></i></span>
						</div>
					</div>
				</div>
				<?php } ?>
		<?php endif; ?>
		
		
		<?php if ($_REQUEST['idReportType'] == LearningReportType::USERS_DELAY) : ?>
            <div class="row-fluid">
                <div class="span5">
                    <?= $m->getAttributeLabel('courses_expiring_in') ?>
                </div>
                <div class="span7">
                    <?php echo CHtml::textField('ReportFieldsForm[filters][courses_expiring_in]', $form['filters']['courses_expiring_in'], array(
                        'style' => 'width: 100px; height: 28px'
                    )); ?>
                    &nbsp;&nbsp;<?= Yii::t('standard', 'day(s)') ?>
                </div>

            </div>
		<?php endif;?>

				<?php if ($_REQUEST['idReportType'] == LearningReportType::CERTIFICATION_USERS || $_REQUEST['idReportType'] == LearningReportType::USERS_CERTIFICATION): ?>

					<?php
					if (!$form['filters']['certification_status']) {
						$form['filters']['certification_status'] = 'all';
					}
					if (!$form['filters']['condition_status']) {
						$form['filters']['condition_status'] = 'and';
					}
					?>

					<div class="row-fluid">
						<div class="span12">
							<label class="radio pull-left" for="r_1">
								<?= CHtml::radioButton('ReportFieldsForm[filters][certification_status]', $form['filters']['certification_status'] == 'all', array('value' => 'all', 'id' => 'r_1', 'uncheckValue' => null)) . ' ' . Yii::t('standard', '_ALL') . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ?>
							</label>

							<label class="radio pull-left" for="r_2">
								<?= CHtml::radioButton('ReportFieldsForm[filters][certification_status]', $form['filters']['certification_status'] == 'active', array('value' => 'active', 'id' => 'r_2', 'uncheckValue' => null)) . ' ' . Yii::t('certification', 'Active certifications') . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ?>
							</label>

							<label class="radio pull-left" for="r_3">
								<?= CHtml::radioButton('ReportFieldsForm[filters][certification_status]', $form['filters']['certification_status'] == 'expired', array('value' => 'expired', 'id' => 'r_3', 'uncheckValue' => null)) . ' ' . Yii::t('certification', 'Expired certifications') . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ?>
							</label>
						</div>
					</div>
					<?php if ($_REQUEST['idReportType'] == LearningReportType::CERTIFICATION_USERS): ?>
						<br>
						<div class="row-fliud">
							<label class="checkbox">
								<?= CHtml::checkBox('ReportFieldsForm[filters][show_suspended_users]', $form['filters']['show_suspended_users']) ?>
								<?= $m->getAttributeLabel('show_suspended_users') ?>
							</label>
						</div>
					<?php endif; ?>

				<?php endif; ?>

				<?php if ($_REQUEST['idReportType'] == LearningReportType::USERS_LEARNING) : ?>

					<?php $m->setScenario('LO'); ?>
					<div class="row-fluid">
						<h5><?php echo $m->getAttributeLabel('lo_type'); ?></h5><br>

						<div class="row-fluid">

							<?php $i = 0; ?>
							<?php foreach ($m->getLearningObjectTypes() as $key => $title) { ?>
								<?php $i++; ?>
								<label class="checkbox pull-left" style="width: 30%;" for="<?= $key ?>">
									<?php
									echo CHtml::checkbox(
											"ReportFieldsForm[filters][lo_type][$key]", (isset($form['filters']['lo_type']) && in_array($key, $form['filters']['lo_type'])) ? true : false, array(
										'id' => $key,
											)
									);
									echo $title;
									?>
								</label>
								<?php if (($i % 3) == 0) echo '<div class="clearfix"></div>'; ?>
							<?php } ?>
						</div>
					</div>
				<?php endif; ?>

				<?php if ($_REQUEST['idReportType'] == LearningReportType::USERS_EXTERNAL_TRAINING) : ?>

					<div class="row-fluid" style="margin-bottom: 25px;">

						<div class="span12">
							<?php (!$form['filters']['subscription_status'] ? $form['filters']['subscription_status'] = 'approved' : ''); ?>
							<?php //if ( !$form['filters']['subscription_status'] ) $form['filters']['subscription_status'] = 'approved';  ?>

							<label class="radio pull-left" for="r_1">
								<?= CHtml::radioButton('ReportFieldsForm[filters][subscription_status]', $form['filters']['subscription_status'] == 'all', array('value' => 'all', 'id' => 'r_1', 'uncheckValue' => null)) . ' ' . Yii::t('standard', '_ALL') . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ?>
							</label>

							<label class="radio pull-left" for="r_2">
								<?= CHtml::radioButton('ReportFieldsForm[filters][subscription_status]', $form['filters']['subscription_status'] == 'approved', array('value' => 'approved', 'id' => 'r_2', 'uncheckValue' => null)) . ' ' . Yii::t('transcripts', 'Approved') . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ?>
							</label>

							<label class="radio pull-left" for="r_3">
								<?= CHtml::radioButton('ReportFieldsForm[filters][subscription_status]', $form['filters']['subscription_status'] == 'waiting', array('value' => 'waiting', 'id' => 'r_3', 'uncheckValue' => null)) . ' ' . Yii::t('catalogue', '_WAITING_APPROVAL') . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ?>
							</label>

						</div>

					</div>

					<div class="row-fluid" style="margin-bottom: 10px;"><?= Yii::t('standard', 'External training date') ?></div>
					<div class="row-fluid">
						<div class="span4"> <!-- Begin Date -->
							<?= $m->getAttributeLabel('date_from') ?>
							<div class='input-append date' data-date-format="<?= $dpFormat ?>" data-date-language= "<?= Yii::app()->getLanguage() ?>" style="margin-left: 5px; width: 110px">
								<?php echo CHtml::textField('ReportFieldsForm[filters][date_from]', $form['filters']['date_from'], array('class' => 'span12')); ?>
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>

						</div>
						<div class="span4"> <!-- End Date -->
							<?= $m->getAttributeLabel('date_to') ?>

							<div class='input-append date' data-date-format="<?= $dpFormat ?>" data-date-language= "<?= Yii::app()->getLanguage() ?>" style="margin-left: 5px; width: 110px">
								<?php echo CHtml::textField('ReportFieldsForm[filters][date_to]', $form['filters']['date_to'], array('class' => 'span12')); ?>
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>

						</div>
						<div class="span6">
						</div>
					</div>
				<?php endif; // END USERS_EXTERNAL_TRAINING  ?>

				<?php if (in_array($_REQUEST['idReportType'], array(LearningReportType::COURSES_USERS, LearningReportType::ECOMMERCE_TRANSACTIONS, LearningReportType::USERS_NOTIFICATIONS))) : ?>

					<?php if (in_array($_REQUEST['idReportType'], array(LearningReportType::ECOMMERCE_TRANSACTIONS))) : ?>
						<br/>
						<div>
							<label class="checkbox">
								<?= CHtml::checkBox('ReportFieldsForm[filters][show_deleted_too]', $form['filters']['show_deleted_too']) ?>
								<?= $m->getAttributeLabel('show_deleted_too') ?>
							</label>
						</div>
					<?php endif; ?>

					<?php if (in_array($_REQUEST['idReportType'], array(LearningReportType::COURSES_USERS))) : ?>
						<div class="row-fluid">
							<div class="span6">
								<div class="row-fluid">
									<label class="checkbox">
										<?= CHtml::checkBox('ReportFieldsForm[filters][consider_other_than_students]', $form['filters']['consider_other_than_students']) ?>
										<?= $m->getAttributeLabel('consider_other_than_students') ?>
									</label>
								</div>
								<div class="row-fluid">
									<label class="checkbox">
										<?= CHtml::checkBox('ReportFieldsForm[filters][consider_suspended_users]', $form['filters']['consider_suspended_users']) ?>
										<?= $m->getAttributeLabel('consider_suspended_users'); ?>
									</label>
								</div>
							</div>

							<div class="span6">
							</div>
						</div>
					<?php endif; ?>


				<?php endif; ?>

				<?php if ($_REQUEST['idReportType'] == LearningReportType::AUDIT_TRAIL): ?>
					<div class="audit-trail-config">
						<p class="accordion-header">
							<?= Yii::t('audit_trail', 'Event categories') ?>:
						</p>

						<div class="audit-trail-categories-accordion">
							<?php
							$event = new DEvent($this, array());
							Yii::app()->event->raise('getAuditTrailCategories', $event);
							if ($event->return_value && is_array($event->return_value)): $i = 0;
								$accordionContentIndex = 0;
								 foreach ($event->return_value as $catIdUnused => $auditTrailCategory): $i++; ?>
									<?php
									$accordionContentIndex++;
									?>
									<div class="header header-<?= $accordionContentIndex ?>" data-index="<?= $accordionContentIndex ?>">
										<div>
											<div class="pull-right">
												<?= Yii::t('audit_trail', '<span class="selected-events">{n}</span> selected') ?>

												&nbsp;&nbsp;&nbsp;

												<span class="toggler">
													<i class="fa fa-chevron-down"></i>
												</span>
											</div>

											<?php echo Yii::t('report_filters', $auditTrailCategory['name']); ?>

											<span class="audit-trail-select-all-events" data-index="<?= $accordionContentIndex ?>">
												&nbsp;
												<?php
												echo "<u>" . CHtml::link(Yii::t('standard', "_SELECT_ALL"), "javascript:void();", array(
													'data-index' => $accordionContentIndex,
													'class' => 'select-all',
												)) . "</u>";

												echo " &nbsp; ";

												echo "<u>" . CHtml::link(Yii::t('standard', "_UNSELECT_ALL"), "javascript:void();", array(
													'data-index' => $accordionContentIndex,
													'class' => 'unselect-all',
												)) . "</u>";
												?>
												&nbsp;
											</span>
										</div>
									</div>
									<div class="accordion-content-<?= $accordionContentIndex ?>" data-index="<?= $accordionContentIndex ?>">
										<div class="row-fluid">
											<?php
											$i = 0;
											foreach ($auditTrailCategory['events'] as $event):
												?>

												<div class="span6">
													<label class="checkbox">
														<?=
														CHtml::checkBox('ReportFieldsForm[filters][audit_trail][' . $event['event_name'] . ']', $form['filters']['audit_trail'][$event['event_name']], array(
															'class' => 'accordion-checkbox'
														))
														?>
														<?= $event['event_label'] ?>
													</label>

													<?php if ($event['event_description']): ?>
														<p class="setting-description styler indented">
															<?= $event['event_description'] ?>
														</p>
													<?php endif; ?>
												</div>

												<?php
												$i++;
												if ($i == 2): $i = 0; // break rows
													?>
												</div><!--.row-fluid end-->
												<br/>
												<div class="row-fluid">
												<?php endif; ?>

											<?php endforeach; ?>
										</div>
									</div>
								<?php endforeach; ?>
							<?php endif; ?>
						</div>
					</div>
					<script>
						$(function () {
							ReportMan.step4Ready();
						});
					</script>
				<?php endif; ?>
				<?php if ($_REQUEST['idReportType'] == LearningReportType::USERS_CONTESTS): ?>
					<div class="row-fluid">
						<p><?= Yii::t('gamification', 'Finish date') ?></p>
					</div>
					<br>
					<div class="row-fluid">
						<div class="span4">
							<?=
							CHtml::label('From', 'ReportFieldsForm_filters_date_from', array(
								'style' => 'display: inline-block; margin-right: 5px;'
							))
							?>
							<div class='input-append date' data-date-format="<?= $dpFormat ?>" data-date-language= "<?= Yii::app()->getLanguage() ?>">
								<?php
								echo CHtml::textField('ReportFieldsForm[filters][date_from]', $form['filters']['date_from'], array(
									'class' => 'span12',
									'style' => 'width: 110px;'
								));
								?>
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>
						</div>
						<div class="span8">
							<?=
							CHtml::label('To', 'ReportFieldsForm_filters_date_to', array(
								'style' => 'display: inline-block; margin-right: 5px;'
							))
							?>
							<div class='input-append date' data-date-format="<?= $dpFormat ?>" data-date-language= "<?= Yii::app()->getLanguage() ?>">
								<?php
								echo CHtml::textField('ReportFieldsForm[filters][date_to]', $form['filters']['date_to'], array('class' => 'span12',
									'style' => 'width: 110px;'));
								?>
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>
						</div>
					</div>
				<?php endif; ?>


				<div class="row-fluid"><div class="span12">
						<?php
						// Filtering by Custom (additional) fields
						foreach ($allFields as $group => $data) {
							if (isset($data['custom_fields']) && is_array($data['custom_fields']) && $group == 'user') {
								?>

								<div class="row-fluid">
									<?php
									$i = 0;

									// indicator is there custom Drop Down fields
									$dropdown_ind = 0;
									?>
									<?php foreach ($data['custom_fields'] as $idCommon => $label) : ?>
										<?php
										$fieldType = CoreUserField::getFieldTypeById($idCommon);
										$inputId = "{$idCommon}";

										// indicate first occurrence of custom drop-down field
										if ($fieldType == CoreUserField::TYPE_DROPDOWN) {
											$dropdown_ind++;
										}

										// display the custom fields label
										if ($dropdown_ind === 1) {
											?>
											<div class="report-wizard-custom-dropdown-label row-fluid fields-column-heading">
												<h5><?php echo Yii::t('report_filters', '_FIELDS_' . strtoupper($group)); ?></h5>
											</div>
											<?php
											//ensure the label will be shown just once
											$dropdown_ind++;
										}

										// Handle different types of fields
										switch ($fieldType) {

											case CoreUserField::TYPE_DROPDOWN:
												$i++;
												echo '<div class="pull-left" style="width: 30%;">';
												echo $label . "<br>";
												$html = $this->renderDropdownCustomField($idCommon, 'ReportFieldsForm[filters][custom_fields][' . $inputId . ']', $form['filters']['custom_fields']["$inputId"], Yii::t('standard', '_ALL'));
												echo $html;
												echo '</div>';
												break;
										}

										if (($i % 3) == 0)
											echo '<div class="clearfix"></div>';
										?>

									<?php endforeach; ?>
								</div>
								<?php
							}
						}
						?>
					</div></div>
				<?php if ($_REQUEST['idReportType'] == LearningReportType::APP7020_ASSETS): ?>
					<?php
					$this->renderPartial('wizard/_assets_created_conditions', array(
						'dpFormat' => $dpFormat,
						'form' => $form,
						'firstConditionText' => Yii::t('standard', 'Published date')
					))
					?>
				<?php endif; ?>
				<?php if ($_REQUEST['idReportType'] == LearningReportType::APP7020_EXPERTS): ?>
						<?php
					$this->renderPartial('wizard/_assets_created_conditions', array(
						'dpFormat' => $dpFormat,
						'form' => $form,
						'firstConditionText' => Yii::t('standard', 'Activity date')
					))
					?>
				<?php endif; ?>

				<?php if ($_REQUEST['idReportType'] == LearningReportType::APP7020_CHANNELS): ?>
					<?php
					$this->renderPartial('wizard/_assets_created_conditions', array(
						'dpFormat' => $dpFormat,
						'form' => $form,
						'firstConditionText' => Yii::t('standard', 'Date')
					))
					?>
				<?php endif; ?>

				<?php if ($_REQUEST['idReportType'] == LearningReportType::APP7020_USER_CONTRIBUTIONS): ?>
					<?php
					$this->renderPartial('wizard/_assets_created_conditions', array(
						'dpFormat' => $dpFormat,
						'form' => $form,
						'firstConditionText' => Yii::t('standard', 'Contributions Date')
					))
					?>
				<?php endif; ?>


				<?php if ($_REQUEST['idReportType'] == LearningReportType::GROUPS_COURSES) : ?>
					<?php echo CHtml::hiddenField('temp'); ?>
				<?php endif; ?>

			</div>
		</div>




	<?php endif; ?>




	<!-- SELECT FIELDS -->
	<br>
	<div class="clearfix"></div>
	<div class="row-fluid">
		<div class="span3">
			<h3><?php echo Yii::t('standard', '_SELECT'); ?></h3>
		</div>
		<div class="span9">
			<div class="row-fluid">
				<?php
				$counter = 0;
				foreach ($allFields as $group => $data) {
					if ((count($data['selectable']) > 0) || (count($data['aggregate'])) || (count($data['aggregate_3th']))) {
						$counter++;
					}
				}
				$percent = $counter > 0 ? 90 / $counter : 0;
				?>

				<?php

				foreach ($allFields as $group => $data) : ?>
					<?php $cols = $cols == false && empty($data['aggregate_3th']) ? 6 : 4; ?>
					<?php if (!empty($data['fixed']) || !empty($data['selectable']) || !empty($data['aggregate']) || !empty($data['aggregate_3th'])): ?>

						<div class="span6" style="width: <?= $percent ?>% !important;">


							<?php if (!empty($data['fixed'])) { ?>
								<?php foreach ($data['fixed'] as $key => $title) { ?>
									<?php echo CHtml::hiddenField("ReportFieldsForm[fields][$group][$key]", 1); ?>
								<?php } ?>
							<?php } ?>


							<?php if (!empty($data['selectable'])) { ?>
								<div class="row-fluid fields-column-heading">
									<?php if ($_REQUEST['idReportType'] == LearningReportType::APP7020_ASSETS): ?>
										<h5><?php echo ucfirst(Yii::t('app7020', 'Asset fields')); ?></h5>
									<?php elseif ($_REQUEST['idReportType'] == LearningReportType::APP7020_EXPERTS): ?>
										<h5><?php echo ucfirst(Yii::t('app7020', 'Expert fields')); ?></h5>
									<?php elseif ($_REQUEST['idReportType'] == LearningReportType::APP7020_USER_CONTRIBUTIONS): ?>
										<h5><?php echo ucfirst(Yii::t('app7020', 'User Fields')); ?></h5>
									<?php elseif ($_REQUEST['idReportType'] == LearningReportType::APP7020_CHANNELS): ?>
										<h5><?php echo ucfirst(Yii::t('app7020', 'Channel fields')); ?></h5>
									<?php else: ?>
										<h5><?php echo Yii::t('report_filters', '_FIELDS_' . strtoupper($group)); ?></h5>
									<?php endif; ?>
								</div>
								<?php foreach ($data['selectable'] as $key => $title) {
									if($key == "unique_id") {
										$key = "idst";
									}?>
									<?php $fullKey = "{$group}.{$key}"; ?>
									<?php $inputId = $group . '_' . $key; ?>
									<div class="row-fluid">
										<label class="checkbox" for="<?= $inputId ?>">
											<?php
											echo CHtml::checkbox("ReportFieldsForm[fields][$group][$key]", (isset($form['fields'][$fullKey]) && $form['fields'][$fullKey] == 1) ? true : false, array('id' => $inputId));
											echo CHtml::encode($title);
											?>
										</label>
									</div>
								<?php } ?>



							<?php } ?>

<!--							////////////////////////ADD ONLY ENROLLMENT CUSTOM SELECTABLE FIELDS////////////////////-->
							<?php if (!empty($data['custom_fields'])) { ?>
								<?php if ($group == 'enrollment') { ?>
									<?php foreach ($data['custom_fields'] as $key => $title) { ?>
										<?php $fullKey = "{$group}.{$key}"; ?>
										<?php $inputId = $group . '_' . $key; ?>
										<div class="row-fluid">
											<label class="checkbox" for="<?= $inputId ?>">
												<?php echo CHtml::checkbox("ReportFieldsForm[fields][$group][$key]", (isset($form['fields'][$fullKey]) && $form['fields'][$fullKey] == 1) ? true : false, array('id' => $inputId)); ?>
												<?php echo $title; ?>
											</label>
										</div>
									<?php } ?>
								<?php } ?>
							<?php } ?>
							<br>




							<?php if (!empty($data['aggregate'])) { ?>

								<div class="row-fluid fields-column-heading">
									<?php if ($_REQUEST['idReportType'] == LearningReportType::APP7020_EXPERTS): ?>
										<h5><?php echo ucfirst(Yii::t('app7020', 'Coaching Activity Fields')); ?></h5>
									<?php elseif ($_REQUEST['idReportType'] == LearningReportType::USERS_COURSES): ?>
										<h5><?php echo ucfirst(Yii::t('report_filters', '_FIELDS_STAT')); ?></h5>
									<?php elseif ($_REQUEST['idReportType'] == LearningReportType::APP7020_USER_CONTRIBUTIONS): ?>
										<h5><?php echo ucfirst(Yii::t('app7020', 'Contributions')); ?></h5>
									<?php else: ?>
										<h5><?php echo Yii::t('report_filters', '_FIELDS_' . strtoupper($group)); ?></h5>
									<?php endif; ?>
								</div>
								<?php foreach ($data['aggregate'] as $key => $title) { ?>
									<?php $fullKey = "{$group}.{$key}"; ?>
									<?php $inputId = $group . '_' . $key; ?>
									<div class="row-fluid">
										<label class="checkbox" for="<?= $inputId ?>">
											<?php echo CHtml::checkbox("ReportFieldsForm[fields][$group][$key]", (isset($form['fields'][$fullKey]) && $form['fields'][$fullKey] == 1) ? true : false, array('id' => $inputId)); ?>
											<?php echo $title; ?>
											<?php //echo CHtml::label($title, $group . '_' . $key);   ?>
										</label>
									</div>
								<?php } ?>
							<?php } ?>

							<?php if (!empty($data['aggregate_3th'])) { ?>

								<div class="row-fluid fields-column-heading">
									<?php if ($_REQUEST['idReportType'] == LearningReportType::APP7020_EXPERTS): ?>
										<h5><?php echo ucfirst(Yii::t('app7020', 'Peer Review Activity')); ?></h5>
									<?php else: ?>
										<h5><?php echo Yii::t('report_filters', '_FIELDS_' . strtoupper($group)); ?></h5>
									<?php endif; ?>
								</div>
								<?php foreach ($data['aggregate_3th'] as $key => $title) { ?>
									<?php $fullKey = "{$group}.{$key}"; ?>
									<?php $inputId = $group . '_' . $key; ?>
									<div class="row-fluid">
										<label class="checkbox" for="<?= $inputId ?>">
											<?php echo CHtml::checkbox("ReportFieldsForm[fields][$group][$key]", (isset($form['fields'][$fullKey]) && $form['fields'][$fullKey] == 1) ? true : false, array('id' => $inputId)); ?>
											<?php echo $title; ?>
											<?php //echo CHtml::label($title, $group . '_' . $key);  ?>
										</label>
									</div>
								<?php } ?>
							<?php } ?>



						</div>

					<?php endif; ?>

				<?php endforeach; ?>

			</div>
		</div>
	</div>

	<div class="form-actions jwizard">

		<?=
		CHtml::submitButton(Yii::t('standard', '_PREV'), array(
			'class' => 'btn btn-docebo green big jwizard-nav',
			'name' => 'prev_button',
		));
		?>

	<?= 
	   CHtml::submitButton(Yii::t('standard', '_NEXT'), array(
	       'class' => 'btn btn-docebo green big jwizard-nav next-page',
	       'name'	=> 'next_button',
	   )); 
	?>

		<?=
		CHtml::button(Yii::t('standard', '_CANCEL'), array(
			'class' => 'btn btn-docebo black big close-dialog',
			'name' => 'cancel_step'
		));
		?>
	</div>


	<?php echo CHtml::endForm(); ?>


</div>

<script type="text/javascript">


	jwizard.setDialogClass('report-wizard-step-4');

	$('.datepicker.dropdown-menu').remove();

	$(function () {
		// Initialize Date Picker
		// @TODO Somehow, prevent attaching datepicker on every re-load of this step or it starts slowing down the UI responsiveness
		$('.input-append.date').bdatepicker({
			autoclose: true
		});

		$('.step4-container input[type="checkbox"]').styler();
		$('.step4-container input[type="radio"]').styler();

		$('.step4-container label.checkbox').css({'min-height': '23px'});

	});
	var radio_ranges = [
		'input[type=radio][name="ReportFieldsForm[filters][start_date][type]"]',
		'input[type=radio][name="ReportFieldsForm[filters][end_date][type]"]'
	];
	$(document).on('change', radio_ranges.join(', '), function (e) {
		var datepicker = $(e.target).parent().parent().parent().find('.hidden-datepicker');
		var isHidden = $(datepicker).hasClass('hidden') && $(e.target).val() == 'range';
		if (isHidden) {
			if ($(datepicker).hasClass('hidden')) {
				$(datepicker).removeClass('hidden');
			}
		} else {
			if (!$(datepicker).hasClass('hidden') && $(e.target).val() !== 'range') {
				$(datepicker).addClass('hidden');
			}
		}
	});

</script>