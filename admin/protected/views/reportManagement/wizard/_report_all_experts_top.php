<?php
/**
 * Created by Ivaylo Ivanov
 * User: Ivo
 * Date: 10.05.2016
 * Time: 10:15
 *
 * @var $this UsersSelectorController
 *
 * @var $model LearningReportFilter
 */
?>
<?php
$model = LearningReportFilter::model()->findByPk($this->idReport);
$hasExperts = $model ? $model->hasExperts() : false;
?>
<style>
	
	#users-selector-container .tab-content > .tab-pane {
			    display: block;
		}
</style>
<div class="report_filter-selector-top-panel">
    <br>
    <div class="row-fluid">
        <div class="span12">
            <?= Yii::t('gamification','Apply this report to all experts or select custom experts') ?>
        </div>
    </div>
    <?php if (Yii::app()->user->hasFlash('error')) : ?>
        <div class="row-fluid">
            <div class="span12">
                <div class='alert alert-error alert-compact text-left'>
                    <?= Yii::app()->user->getFlash('error'); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="row-fluid">
            <div class="grey-wrapper">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <div class="span5">
                                <label class="radio" id="select-all-experts">
                                    <?php
                                    echo CHtml::radioButton('experts-filter', !$hasExperts, array('value' => LearningReportFilter::ALL_COURSES));
                                    echo Yii::t('standard', 'All Experts');
                                    ?>
                                </label>
                                <span class="help-block"><?= Yii::t('report', 'This option will include present and future experts.')?></span>
                            </div>
                            <div class="span5">
                                <label class="radio" id="select-some-experts">
                                    <?php
                                    echo CHtml::radioButton('experts-filter', $hasExperts, array('value' => LearningReportFilter::SELECT_COURSES));
                                    echo Yii::t('standard', 'Select Experts');
                                    ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(function(){
            // Call our Notification manager to signal it that filter panel is loaded. It will add/run some code.
            ReportMan.selectorTopPanelReady();
			
         $('#select-all-experts, #experts-filter').click(function() {
            $('.deselect-all').click();
        });

        $('#select-some-experts, #experts-filter').click(function() {
            $('#selected_assets').val('[]');
            $('#selected_channels').val('[]');
        });
			
        });

    </script>
