<?php
/* @var $reportModel LearningReportFilter */
?>
<style>
<!--

    .modal.report-wizard-step-1 {
        width: 680px;
        margin-left: -245px;
}

    .modal.report-wizard-step-1 .modal-body {
        height: auto;
    }

	.report-wizard-step-1 .types{
		margin:10px 0 30px;
	}

	.modal.report-wizard-step-1 h6{
		display:block;
	}

    .report-wizard-step-1 .types .row-fluid {
    	background: none repeat scroll 0 0 #FFFFFF;
    	border-bottom: 1px solid #E4E6E5;
    	padding: 12px 0px;
    	height: 17px;
		width:300px;
    }

    .report-wizard-step-1 .types .row-fluid input {
    	float: right;
    	margin-right: 5px;
    }

	.report-wizard-step-1 .report-name{
		padding-bottom:28px;
	}

	.report-wizard-step-1 .radio, .report-wizard-step-1 .checkbox{
		padding-left:10px;
	}

    .report-wizard-step-1 .report-name input {
    	width: 100%;
		background:#fff;
    }

-->
</style>

<?php
	$types = CHtml::listData(LearningReportType::model()->findAll(), 'id', 'title');
	$user = Yii::app()->user;
	if ($user->getIsPu()) {
		unset($types[LearningReportType::AUDIT_TRAIL]);
	}

    if(!PluginManager::isPluginActive('CurriculaApp')){
        unset($types[LearningReportType::USERS_COURSEPATH]);
    }
    if(!PluginManager::isPluginActive('GamificationApp')){
        unset($types[LearningReportType::USERS_BADGES]);
		unset($types[LearningReportType::USERS_CONTESTS]);
    }
	if(!PluginManager::isPluginActive('CertificationApp')){
		unset($types[LearningReportType::CERTIFICATION_USERS]);
		unset($types[LearningReportType::USERS_CERTIFICATION]);
	}
    if (!PluginManager::isPluginActive('TranscriptsApp')) {
        unset($types[LearningReportType::USERS_EXTERNAL_TRAINING]);
    }

	if (!PluginManager::isPluginActive('Share7020App')) {
		unset($types[LearningReportType::APP7020_ASSETS]);
		unset($types[LearningReportType::APP7020_EXPERTS]);
		unset($types[LearningReportType::APP7020_CHANNELS]);
	}

	if (!PluginManager::isPluginActive('EcommerceApp')) {
		unset($types[LearningReportType::ECOMMERCE_TRANSACTIONS]);
	}

	echo CHtml::beginForm('', 'POST', array(
 		'id' => 'report-wizard-step-1-form',
 		'name' => 'report-wizard-step-1-form',
		'class' => 'ajax jwizard-form form-horizontal',
	));

	echo CHtml::hiddenField('from_step', 'step1');
	echo CHtml::hiddenField('idReport', $reportModel->id_filter);

?>


<?php if (Yii::app()->user->hasFlash('error')) : ?>
	<div class='alert alert-error text-center'>
    	<?= Yii::app()->user->getFlash('error'); ?>
    	<button class="close" data-dismiss="alert" type="button">×</button>
    </div>
<?php endif; ?>

<div class="row-fluid report-name">
	<div class="span12">
		<br>
		<h6><?php echo Yii::t('standard', '_NAME'); ?></h6>
		<br>
		<?php echo CHtml::textField('filter_name', $reportModel->filter_name, array('class' => 'span12')); ?>
	</div>
</div>
<?php
if ($reportModel && $reportModel->report_type_id) {
	$currentReportId = $reportModel->report_type_id;
}
else {
	$currentReportId = LearningReportType::USERS_COURSES;
}
?>
<?php $i = 0; ?>
<div class="row-fluid">
	<div class="span6">
		<h6><?php echo Yii::t('app7020', 'Users reports'); ?></h6><br>
		<div class="types clearfix">
			<?php foreach ($types as $id => $type) { ?>
				<?php if(in_array($id,LearningReportType::$USERS_REPORTS)){ ?>
				<div class="row-fluid">
					<label class="radio" for="label_<?= $id ?>">
						<?= Yii::t("report", $type) ?>
						<input id="label_<?= $id ?>" class="pull-right" type="radio" name="idReportType" value="<?= $id ?>" <?= ($id==$currentReportId) ? 'checked="checked"' : '' ?>>
					</label>
				</div>
				<?php $i++; ?>
				<?php } ?>
			<?php } ?>
		</div>
	</div>

	<div class="span6">
		<h6><?php echo Yii::t('app7020', 'Apps reports'); ?></h6><br>
		<div class="types clearfix">
			<?php foreach ($types as $id => $type) { ?>
				<?php if(in_array($id,LearningReportType::$APPS_REPORTS)){ ?>
					<div class="row-fluid">
						<label class="radio" for="label_<?= $id ?>">
							<?= Yii::t("report", $type) ?>
							<input id="label_<?= $id ?>" class="pull-right" type="radio" name="idReportType" value="<?= $id ?>" <?= ($id==$currentReportId) ? 'checked="checked"' : '' ?>>
						</label>
					</div>
					<?php $i++; ?>
				<?php } ?>
			<?php } ?>
		</div>
		<?php if (PluginManager::isPluginActive('Share7020App')) { ?>
			<h6><?php echo Yii::t('app7020', 'Coach&Share reports'); ?></h6>
			<div class="types clearfix">
				<?php foreach ($types as $id => $type) { ?>
					<?php if(in_array($id,LearningReportType::$APP7020_REPORTS)){ ?>
						<div class="row-fluid">
							<label class="radio" for="label_<?= $id ?>">
								<?= Yii::t("report", $type) ?>
								<input id="label_<?= $id ?>" class="pull-right" type="radio" name="idReportType" value="<?= $id ?>" <?= ($id==$currentReportId) ? 'checked="checked"' : '' ?>>
							</label>
						</div>
						<?php $i++; ?>
					<?php } ?>
				<?php } ?>
			</div>
		<?php } ?>
	</div>
</div>


<div class="form-actions jwizard">
	<?=
	   CHtml::submitButton(Yii::t('standard', '_NEXT'), array(
	       'class'  => 'btn btn-docebo green big jwizard-nav next-page',
	       'name'	=> 'next_button',
	   ));
	?>

	<?=
	   CHtml::button(Yii::t('standard', '_CANCEL'), array(
	       'class' => 'btn btn-docebo black big close-dialog',
	       'name' => 'cancel_step'
	   ));
	?>
</div>


<?php echo CHtml::endForm(); ?>

<script type="text/javascript">
	jwizard.setDialogClass('report-wizard-step-1');
	$('#custom-report-wizard .types input[type="radio"]').styler();
</script>


