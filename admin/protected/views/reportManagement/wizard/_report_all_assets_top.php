<?php
/**
 * Created by Ivaylo Ivanov
 * User: Ivo
 * Date: 10.05.2016
 * Time: 10:15
 *
 * @var $this UsersSelectorController
 *
 * @var $model LearningReportFilter
 */
?>
<?php
$model = LearningReportFilter::model()->findByPk($this->idReport);
$hasAssets = $model ? $model->hasAssets() : false;
$hasChannels = $model ? $model->hasChannels() : false;
?>

<div class="report_filter-selector-top-panel">
    <br>
    <div class="row-fluid">
        <div class="span12">
            <?= Yii::t('gamification','Apply this report to all assets or select custom assets') ?>
        </div>
    </div>
    <?php if (Yii::app()->user->hasFlash('error')) : ?>
        <div class="row-fluid">
            <div class="span12">
                <div class='alert alert-error alert-compact text-left'>
                    <?= Yii::app()->user->getFlash('error'); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="row-fluid">
            <div class="grey-wrapper">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <div class="span5">
                                <label class="radio" id="select-all-assets">
                                    <?php
                                    echo CHtml::radioButton('assets-filter', !$hasAssets && !$hasChannels, array('value' => LearningReportFilter::ALL_COURSES));
                                    echo Yii::t('standard', 'All Assets');
                                    ?>
                                </label>
                                <span class="help-block"><?= Yii::t('report', 'This option will include present and future assets.')?></span>
                            </div>
                            <div class="span5">
                                <label class="radio"  id="select-some-assets">
                                    <?php
                                    echo CHtml::radioButton('assets-filter', $hasAssets || $hasChannels, array('value' => LearningReportFilter::SELECT_COURSES));
                                    echo Yii::t('standard', 'Select Assets');
                                    ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(function(){
            // Call our Notification manager to signal it that filter panel is loaded. It will add/run some code.
            ReportMan.selectorTopPanelReady();
        });
		
		

        $('#select-some-assets, #select-some-assets-styler').click(function() {
            $('.deselect-all').click();
        });

        $('#select-all-assets, #select-all-assets-styler').click(function() {
            $('#selected_assets').val('[]');
            $('#selected_channels').val('[]');
        });

    </script>
