<?php
$model = LearningReportFilter::model()->findByPk($this->idReport);
$hasChannels = $model ? $model->hasChannels() : false;
?>

<div class="report_filter-selector-top-panel">
    <br>
    <div class="row-fluid">
        <div class="span12">
            <?= Yii::t('standard','Apply this report for all channels or select channels') ?>
        </div>
    </div>
    <?php if (Yii::app()->user->hasFlash('error')) : ?>
        <div class="row-fluid">
            <div class="span12">
                <div class='alert alert-error alert-compact text-left'>
                    <?= Yii::app()->user->getFlash('error'); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="row-fluid">
            <div class="grey-wrapper">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <div class="span5">
                                <label class="radio">
                                    <?php
                                    echo CHtml::radioButton('channels-filter', !$hasChannels, array('value' => LearningReportFilter::ALL_COURSES));
                                    echo Yii::t('standard', 'All channels');
                                    ?>
                                </label>
                                <span class="help-block"><?= Yii::t('report', 'This option will include present and future channels.')?></span>
                            </div>
                            <div class="span5">
                                <label class="radio">
                                    <?php
                                    echo CHtml::radioButton('channels-filter', $hasChannels, array('value' => LearningReportFilter::SELECT_COURSES));
                                    echo Yii::t('standard', 'Select channels');
                                    ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(function(){
            // Call our Notification manager to signal it that filter panel is loaded. It will add/run some code.
            ReportMan.selectorTopPanelReady();
        });

    </script>