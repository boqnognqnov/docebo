<style>
<!--

    .modal.report-wizard-step-5 {
        width: 800px;
        margin-left: -400px;
}
    
    .modal.report-wizard-step-5 .modal-body {
        height: auto;
    }
    
    
    .modal.report-wizard-step-5 .intro {
    	border-bottom: 1px solid #E4E6E5;
    	font-size: 20px;
    	padding-bottom: 10px;
    	font-weight: 400;
    	margin-bottom: 0;
    }

-->
</style>


<?php

	echo CHtml::beginForm('', 'POST', array(
 		'id' => 'report-wizard-step-5-form',
 		'name' => 'report-wizard-step-5-form',		
		'class' => 'ajax jwizard-form form-horizontal',
	));
	
	echo CHtml::hiddenField('from_step', 'step5');
	echo CHtml::hiddenField('idReportType', $_REQUEST['idReportType'], array('class' => 'jwizard-no-restore'));
	echo CHtml::hiddenField('idReport', $_REQUEST['idReport']);
	
	
?>	


<div class="wrapper clearfix report-success row-fluid">

	<div class="span4">
		<div class="left success" style="margin-top: 40px;">
			<div class="copy-success-icon"></div>
            <?php echo Yii::t('standard', '_OPERATION_SUCCESSFUL'); ?>
		</div>
	</div>
	
	<div class="span8">
			
			<p class="intro">
				<?php echo Yii::t('standard', 'What do you want to do next?'); ?>
			</p>            
			<div class="row-fluid row-with-border">                
                <div class="span8">
                <p><?php echo Yii::t('report', '<strong>Schedule</strong> the report'); ?></p>
                </div>
                <div class="span4 text-right pull-right">
				<?php
					echo CHtml::submitButton(Yii::t('standard', '_SCHEDULE'), array(
						'name' => 'next_button',
						'class' => 'btn btn-docebo green big jwizard-nav pull-right',
					));
				?>				
                </div>
			</div>
			
			<div class="row-fluid row-with-border">
                <div class="span8">
                    <p><?php echo Yii::t('report', '_SAVE_SHOW_DESCRIPTION'); ?></p>
                </div>
                <div class="span4 text-right pull-right">
                    <?php
                        echo CHtml::submitButton(Yii::t('report', '_SAVE_SHOW'), array(
                            'name' => 'finish_wizard_show',
                            'class' => 'btn btn-docebo green big jwizard-nav jwizard-finish pull-right',
                        ));
                    ?>				
                </div>
			</div>
			<div class="row-fluid">
                <div class="span7">
                    <p><?php echo Yii::t('report', '_SAVE_DESCRIPTION'); ?></p>
                </div>
                <div class="span5 text-right pull-right">
                    <?php 
                        echo CHtml::submitButton(Yii::t('report', '_SAVE_BACK'), array(
                            'name' => 'finish_wizard_list', 
                            'class' => 'btn btn-docebo green big jwizard-nav jwizard-finish pull-right',
                        ));
                    ?>				
                </div>
			</div>		
	
	</div>
	
</div>


<?php if ($amountWarning) : ?> 
	<div class="alert warning alert-compact"><?= $amountWarning ?></div>
<?php  endif; ?>


	
<div class="form-actions jwizard">
	<?php 
	   echo CHtml::submitButton(Yii::t('standard', '_PREV'), array(
	       'class' => 'btn btn-docebo green big jwizard-nav',
	       'name' => 'prev_button', 
	   )); 
	   echo CHtml::button(Yii::t('standard', '_CANCEL'), array(
	       'class' => 'btn btn-docebo black big close-dialog', 
	       'name' => 'cancel_step'
	   )); 
	?>
</div>


<?php echo CHtml::endForm(); ?>
	
<script type="text/javascript">
	$(function(){
		// We have special class for this dialog; ask jWizard to add it to .modal
		jwizard.setDialogClass('report-wizard-step-5');

		$(document).on("dialog2.ajax-start", function () {
			$('.jwizard-nav').addClass('disabled');
		});
	});
</script>	
	

