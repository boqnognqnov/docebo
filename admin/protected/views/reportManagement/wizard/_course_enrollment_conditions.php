<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 24.11.2015 г.
 * Time: 10:59
 *
 * @var $firstConditionText
 * @var $secondConditionText
 */

if (!$form['filters']['condition_status']) {
    $form['filters']['condition_status'] = 'and';
}

if(empty($form['filters']['start_date'])) {
    $form['filters']['start_date'] = array(
        'type' => 'any',
        'data' => array()
    );
}

//set the default value for the enrollment date type
if(empty($form['filters']['start_date']['type'])) {
    $form['filters']['start_date']['type'] = 'any';
}

//set the default value for the enrollment date combobox
if(empty($form['filters']['start_date']['data']['combobox'])) {
    $form['filters']['start_date']['data']['combobox'] = '>';
}

if(!empty($form['filters']['start_date']['data']['from']) || !empty($form['filters']['start_date']['data']['to'])) {
    $form['filters']['start_date']['data']['from'] = !empty($form['filters']['start_date']['data']['from']) ? Yii::app()->localtime->toLocalDate($form['filters']['start_date']['data']['from'], 'short') : $form['filters']['start_date']['data']['from'];
    $form['filters']['start_date']['data']['to'] = !empty($form['filters']['start_date']['data']['to']) ? Yii::app()->localtime->toLocalDate($form['filters']['start_date']['data']['to'], 'short') : $form['filters']['start_date']['data']['to'];
}

if($_REQUEST['idReportType'] != LearningReportType::USERS_SESSION){

    if(empty($form['filters']['end_date'])) {
        $form['filters']['end_date'] = array(
            'type' => 'any',
            'data' => array()
        );
    }
    //set the default value for the completion date type
    if(empty($form['filters']['end_date']['type'])) {
        $form['filters']['end_date']['type'] = 'any';
    }

    //set the default value for the completion date combobox
    if(empty($form['filters']['end_date']['data']['combobox'])) {
        $form['filters']['end_date']['data']['combobox'] = '>';
    }

    if(!empty($form['filters']['end_date']['data']['from']) || !empty($form['filters']['end_date']['data']['to'])) {
        $form['filters']['end_date']['data']['from'] = !empty($form['filters']['end_date']['data']['from']) ? Yii::app()->localtime->toLocalDate($form['filters']['end_date']['data']['from'], 'short') : $form['filters']['end_date']['data']['from'];
        $form['filters']['end_date']['data']['to'] = !empty($form['filters']['end_date']['data']['to']) ? Yii::app()->localtime->toLocalDate($form['filters']['end_date']['data']['to'], 'short') : $form['filters']['end_date']['data']['to'];
    }

}

?>

<div class="row-fluid filter-box">
    <div class="row-fluid filter-box-row">
        <div class="span6">
            <div class="row-fluid span12">
                <p class="semi-bold title"><?= $firstConditionText ?></p>
            </div>
            <div class="row-fluid span12">
                <label class="radio pull-left" for="ed_1" style="line-height:31px;"><!-- Custom line-height because of the #ed_2 height -->
                    <?php /* Any enrollement */ ?>
                    <?= CHtml::radioButton('ReportFieldsForm[filters][start_date][type]', $form['filters']['start_date']['type']=='any', array('value' => 'any', 	'id' => 'ed_1', 'uncheckValue' => null)) . ' ' . Yii::t('reports', 'Any') . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ?>
                </label>
            </div>
            <div class="row-fluid span12">
                <label class="radio pull-left span12" for="ed_2">
                    <?php /* N Days Ago */ ?>
                    <?= CHtml::radioButton('ReportFieldsForm[filters][start_date][type]', $form['filters']['start_date']['type']=='ndago', array('value' => 'ndago', 	'id' => 'ed_2', 'uncheckValue' => null)) ?>
                    <?= CHtml::dropDownList('ReportFieldsForm[filters][start_date][data][combobox]', $form['filters']['start_date']['data']['combobox'], array(
                        '=' => '=',
                        '<' => '<',
                        '<=' => '<=',
                        '>' => '>',
                        '>=' => '>='
                    ), array(
                        'class' => 'span4'
                    )) ?>
                    <?= CHtml::textField('ReportFieldsForm[filters][start_date][data][days_count]', $form['filters']['start_date']['data']['days_count'], array(
                        'class' => 'span3'
                    ))?>
                    <?= '&nbsp;&nbsp;' . Yii::t('reports', 'days ago') ?>
                </label>
            </div>
            <div class="row-fluid span12">
                <label class="radio pull-left" for="ed_3" style="line-height:31px;"><!-- Custom line-height because of the #ed_2 height -->
                    <?php /* From Date -- To Date */ ?>
                    <?= CHtml::radioButton('ReportFieldsForm[filters][start_date][type]', $form['filters']['start_date']['type']=='range', array('value' => 'range', 	'id' => 'ed_3', 'uncheckValue' => null)) . ' ' . Yii::t('reports', 'Date range') . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ?><br />
                    <div class="hidden-datepicker <?= $form['filters']['start_date']['type'] != 'range' ? 'hidden' : ''?>">
                        <?= CHtml::label('From', 'ReportFieldsForm_filters_start_date_data_from', array(
                            'style' => 'display: inline-block; margin-right: 5px;padding-left: 9px;'
                        )) ?>
                        <div class='input-append date' data-date-format="<?= $dpFormat ?>" data-date-language= "<?= Yii::app()->getLanguage() ?>">
                            <?php echo CHtml::textField('ReportFieldsForm[filters][start_date][data][from]', $form['filters']['start_date']['data']['from'], array('class' => 'span12',
                                'style' => 'width: 110px;')); ?>
                            <span class="add-on" style="height: 21px;"><i class="icon-calendar"></i></span>
                        </div><br>

                        <?= CHtml::label('To', 'ReportFieldsForm_filters_start_date_data_to', array(
                            'style' => 'display: inline-block; margin-right: 5px;padding-left:26px;margin-top:12px;'
                        )) ?>
                        <div class='input-append date' data-date-format="<?= $dpFormat ?>" data-date-language= "<?= Yii::app()->getLanguage() ?>">
                            <?php echo CHtml::textField('ReportFieldsForm[filters][start_date][data][to]', $form['filters']['start_date']['data']['to'], array('class' => 'span12',
                                'style' => 'width: 110px;')); ?>
                            <span class="add-on" style="height: 21px;"><i class="icon-calendar"></i></span>
                        </div>
                    </div>
                </label>
            </div>
        </div>
<?php if($_REQUEST['idReportType'] != LearningReportType::USERS_SESSION) { ?>
        <div class="span6">
            <div class="row-fluid span12">
                <p class="semi-bold title"><?= $secondConditionText ?></p>
            </div>
            <div class="row-fluid span12">
                <label class="radio pull-left" for="sed_1" style="line-height:31px;"><!-- Custom line-height because of the #sed_2 height -->
                    <?= CHtml::radioButton('ReportFieldsForm[filters][end_date][type]', $form['filters']['end_date']['type']=='any', array('value' => 'any', 	'id' => 'sed_1', 'uncheckValue' => null)) . ' ' . Yii::t('reports', 'Any') . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ?>
                </label>
            </div>
            <div class="row-fluid span12">
                <label class="radio pull-left span12" for="sed_2">
                    <?= CHtml::radioButton('ReportFieldsForm[filters][end_date][type]', $form['filters']['end_date']['type']=='ndago', array('value' => 'ndago', 	'id' => 'sed_2', 'uncheckValue' => null)) ?>
                    <?= CHtml::dropDownList('ReportFieldsForm[filters][end_date][data][combobox]', $form['filters']['end_date']['data']['combobox'], array(
                        '=' => '=',
                        '<' => '<',
                        '<=' => '<=',
                        '>' => '>',
                        '>=' => '>='
                    ), array(
                        'class' => 'span4'
                    )) ?>
                    <?= CHtml::textField('ReportFieldsForm[filters][end_date][data][days_count]', $form['filters']['end_date']['data']['days_count'], array(
                        'class' => 'span3'
                    ))?>
                    <?= '&nbsp;&nbsp;' . Yii::t('reports', 'days ago') ?>
                </label>
            </div>
            <div class="row-fluid span12">
                <label class="radio pull-left" for="sed_3" style="line-height:31px;"><!-- Custom line-height because of the #sed_2 height -->
                    <?= CHtml::radioButton('ReportFieldsForm[filters][end_date][type]', $form['filters']['end_date']['type']=='range', array('value' => 'range', 	'id' => 'sed_3', 'uncheckValue' => null)) . ' ' . Yii::t('reports', 'Date range') . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ?><br />
                    <div class="hidden-datepicker <?= $form['filters']['end_date']['type'] != 'range' ? 'hidden' : ''?>">
                        <?= CHtml::label('From', 'ReportFieldsForm_filters_end_date_data_from', array(
                            'style' => 'display: inline-block; margin-right: 5px;padding-left: 9px;'
                        )) ?>
                        <div class='input-append date' data-date-format="<?= $dpFormat ?>" data-date-language= "<?= Yii::app()->getLanguage() ?>">
                            <?php echo CHtml::textField('ReportFieldsForm[filters][end_date][data][from]', $form['filters']['end_date']['data']['from'], array('class' => 'span12',
                                'style' => 'width: 110px;')); ?>
                            <span class="add-on" style="height: 21px;"><i class="icon-calendar"></i></span>
                        </div><br>

                        <?= CHtml::label('To', 'ReportFieldsForm_filters_end_date_data_to', array(
                            'style' => 'display: inline-block; margin-right: 5px;padding-left:26px;margin-top:12px;'
                        )) ?>
                        <div class='input-append date' data-date-format="<?= $dpFormat ?>" data-date-language= "<?= Yii::app()->getLanguage() ?>">
                            <?php echo CHtml::textField('ReportFieldsForm[filters][end_date][data][to]', $form['filters']['end_date']['data']['to'], array('class' => 'span12',
                                'style' => 'width: 110px;')); ?>
                            <span class="add-on" style="height: 21px;"><i class="icon-calendar"></i></span>
                        </div>
                    </div>
                </label>
            </div>
        </div>
<?php } ?>
    </div>
    <br>
<?php if($_REQUEST['idReportType'] != LearningReportType::USERS_SESSION) { ?>
    <div class="row-fluid filter-box-row odd">
        <div class="span12">
            <label class="radio" for="r_3" style="line-height:25px;">
                <?= CHtml::radioButton('ReportFieldsForm[filters][condition_status]', $form['filters']['condition_status']=='and',	array('value' => 'and',	'id' => 'r_3', 'uncheckValue' => null)) . ' ' . Yii::t('standard', 'All the above conditions must be satisfied') ?>
            </label>
            <label class="radio" for="r_4" style="line-height:25px;">
                <?= CHtml::radioButton('ReportFieldsForm[filters][condition_status]', $form['filters']['condition_status']=='or', 	array('value' => 'or', 'id' => 'r_4', 'uncheckValue' => null)) . ' ' . Yii::t('standard', 'At least one of the above conditions must be satisfied') ?>
            </label>
        </div>
    </div>
<?php } ?>
</div>
<br>