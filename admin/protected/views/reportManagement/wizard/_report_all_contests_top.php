<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 18.11.2015 г.
 * Time: 10:15
 *
 * @var $this UsersSelectorController
 *
 * @var $model LearningReportFilter
 */
?>
<?php
$model = LearningReportFilter::model()->findByPk($this->idReport);
$hasContests = $model ? $model->hasContests() : false;
?>

<div class="report_filter-selector-top-panel">
    <br>
    <div class="row-fluid">
        <div class="span12">
            <?= Yii::t('gamification','Apply this report to all contests or select custom contest') ?>
        </div>
    </div>
    <?php if (Yii::app()->user->hasFlash('error')) : ?>
        <div class="row-fluid">
            <div class="span12">
                <div class='alert alert-error alert-compact text-left'>
                    <?= Yii::app()->user->getFlash('error'); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="row-fluid">
            <div class="grey-wrapper">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <div class="span5">
                                <label class="radio">
                                    <?php
                                    echo CHtml::radioButton('contests-filter', !$hasContests, array('value' => LearningReportFilter::ALL_COURSES));
                                    echo Yii::t('standard', 'All Contests');
                                    ?>
                                </label>
                                <span class="help-block"><?= Yii::t('report', 'This option will include present and future contests.')?></span>
                            </div>
                            <div class="span5">
                                <label class="radio">
                                    <?php
                                    echo CHtml::radioButton('contests-filter', $hasContests, array('value' => LearningReportFilter::SELECT_COURSES));
                                    echo Yii::t('standard', 'Select contests');
                                    ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(function(){
            // Call our Notification manager to signal it that filter panel is loaded. It will add/run some code.
            ReportMan.selectorTopPanelReady();
        });

    </script>
