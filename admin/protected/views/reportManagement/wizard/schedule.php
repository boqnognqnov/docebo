<?php
/* @var $reportModel LearningReportFilter  */
/* @var $job CoreJob  */

/*
 * NOTE:  This view is used in two different environments: Wizard (edit/create report) and Non-Wizard (edit/create Report Schedule)
 * DO NOT REMOVE PARTS OF IT (fields, classes, ...) just because you don't know why they are here. They are ALL here for purpose!!!!!!
 *
 * See:
 * 		admin/controllers/ReportManagementController::actionCustomReportWizard  (wizard mode usage)
 * 		admin/controllers/ReportManagementController::actionAxEditSchedule	(standalone usage)
 *
 * [Plamen]
 */


	// This view can be used in both wizard and non-wizard environment; controller must pass this info to us
	// Other controls are ok to mention "wizard", but the important part is the FORM class!!!
	$formWizardClass = $wizardMode ? ' jwizard-form ' : '';
	echo CHtml::beginForm('', 'POST', array(
 		'id' => 'report-schedule-dialog-form',
 		'name' => 'report-schedule-dialog-form',
		'class' => 'ajax form-horizontal ' . $formWizardClass,
	));

	// Do not touch!
	echo CHtml::hiddenField('from_step', 'schedule');
	echo CHtml::hiddenField('idReportType', $idReportType, array('class' => 'jwizard-no-restore'));
	echo CHtml::hiddenField('idReport', $idReport);

	// A DOM marker helping us to find the '.modal' dialog element
	$markerId = 'id-marker-' . time();

	$recurringPeriodSelectData = CoreJob::getRecurringPeriodsList();

	$job = null;
	$currentPeriodMetaname = CoreJob::PERIOD_METANAME_DAILY;
	$currentDayOfWeek	= 1; // Monday
	$currentDayOfMonth 	= 1;
	$currentHour 		= 0; // 00:00|12:00am

	// Set the start date to the next day
	$currentStarting	= new DateTime(Yii::app()->localtime->getUTCNow('Y-m-d'), new DateTimeZone('UTC'));
	$currentStarting->add(new DateInterval('P1D'));
	$currentStarting	= Yii::app()->localtime->toLocalDate($currentStarting->format('Y-m-d'));
	$currentRecipients	= array();
	$currentTimezone = 'UTC';
	$compressed = false;

	if ($reportModel) {
		if ($reportModel->scheduledJob) {
			$job = $reportModel->scheduledJob;
			if ($job) {
				$currentPeriodMetaname = CoreJob::resolveRecurringPeriodMetaname($job->rec_period, $job->rec_length );
				$currentDayOfWeek 	= $job->rec_day_of_week;
				$currentDayOfMonth 	= $job->rec_day_of_month;
				$currentHour		= $job->rec_hour;  // UTC
				$currentTimezone	= (!is_null($job->rec_timezone) && $job->rec_timezone != '' ? $job->rec_timezone : 'UTC');
				$currentStarting	= is_null($job->start_date) || $job->start_date == '0000-00-00'  ? $currentStarting : $job->start_date;
				$params 			= json_decode($job->params, true);
				$currentRecipients	= $params['recipients'];
				$compressed			= isset($params['compressed']) && $params['compressed'] == 1 ? true : false;
			}
		}
	}


	$weekDays = Docebo::getWeekDays();
	$hoursList = Yii::app()->localtime->hoursList(LocalTime::HOUR_24);
	$daysOfMonth = array();
	foreach (range(1,31) as $d) {
		$daysOfMonth[$d] = $d;
	}

	$dpFormat = Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short'));

	$emailAutocompleteUrl = Docebo::createAbsoluteLmsUrl('usersSelector/axUsersAutocomplete', array(
		'fcbk' 		=> 1,  // ask for FCBK compliant answer
		'emails' 	=> 1,  // Return emails as label/value
	));

	// This input is added as FIRST input, to prevent Bootstrap Datepicker to open
	echo CHtml::textField('dummy_ignore', '', array('style' => 'display: none;'));

?>


<span id="<?= $markerId ?>"></span>


<!-- --------------------------------------------  -->
<div class="report-schedule-editor">

	<div class="row-fluid">
		<div class="span12">
			<div class="schedule-icon"><span class="i-sprite large is-stats"></span></div>
			<div class="schedule-info">
				<?php if ($reportModel->id_filter) : ?>
					<span class="schedule-name"><?= $reportModel->filter_name ?></span><br>
					<?= Yii::t('report', '_CREATION_DATE') ?>: <strong><?= $reportModel->creation_date ?></strong><br>
					<?= Yii::t('report', '_TAB_REP_CREATOR') ?>: <strong><?= trim($reportModel->user->userid,'/') ?></strong>
				<?php else: ?>
					<span class="schedule-name"><?= $reportModel->filter_name ?></span><br>
				<?php endif; ?>
			</div>
		</div>
	</div>


	<div class="row-fluid">
		<div class="span12">

			<div class="row-fluid">
				<?= Yii::t('report', 'Schedule this report') ?><br>
				<?php
					echo CHtml::dropDownList('recurring_period', $currentPeriodMetaname, $recurringPeriodSelectData, array(
						'class' => 'span12',
					));
				?>
			</div>


			<div class="row-fluid" id="dynamic-for-hourly">
			</div>


			<div class="row-fluid" id="dynamic-for-daily">
				<br>
				<table>
					<tr>
						<td></td>
						<td><?= Yii::t('standard', '_HOURS') ?></td>
					</tr>
					<tr>
						<td><?= Yii::t('report', 'at')  ?>&nbsp;&nbsp;</td>
						<td>
							<?php
								echo CHtml::dropDownList('daily_rec_hour', $currentHour, $hoursList, array(
									'style' => 'width: 100px;'
								));
							?>
							&nbsp;
							<?= CHtml::dropDownList('timezone_daily_rec_hour', $currentTimezone, Yii::app()->localtime->getTimezonesArray(), array()) ?>
						</td>
					</tr>
				</table>

			</div>



			<div class="row-fluid" id="dynamic-for-weekly">
				<br>
				<table>
					<tr>
						<td><?= Yii::t('report', 'On') ?></td>
						<td></td>
						<td colspan="2"><?= Yii::t('standard', '_HOURS') ?></td>
					</tr>
					<tr>
						<td>
							<?php
								echo CHtml::dropDownList('rec_day_of_week', $currentDayOfWeek, $weekDays, array(
									'class' => 'span12',
								));
							?>
						</td>
						<td>&nbsp;&nbsp;<?= Yii::t('report', 'at')  ?>&nbsp;&nbsp;</td>
						<td>
							<?php
								echo CHtml::dropDownList('weekly_rec_hour', $currentHour, $hoursList, array(
									'style' => 'width: 100px;'
								));
							?>
						</td>
						<td>
							<?= CHtml::dropDownList('timezone_weekly_rec_hour', $currentTimezone, Yii::app()->localtime->getTimezonesArray(), array('style' => 'margin-left:5px;')) ?>
						</td>
					</tr>
				</table>

			</div>




			<div class="row-fluid" id="dynamic-for-monthly">
				<br>
				<table>
					<tr>
						<td><?= Yii::t('report', 'On day') ?></td>
						<td></td>
						<td colspan="2"><?= Yii::t('standard', '_HOURS') ?></td>
					</tr>
					<tr>
						<td>
							<?php
								echo CHtml::dropDownList('rec_day_of_month', $currentDayOfMonth, $daysOfMonth, array(
									'class' => 'span12',
								));
							?>
						</td>
						<td>&nbsp;&nbsp;<?= Yii::t('report', 'at')  ?>&nbsp;&nbsp;</td>
						<td>
							<?php
								echo CHtml::dropDownList('monthly_rec_hour', $currentHour, $hoursList, array(
									'style' => 'width: 100px;'
								));
							?>
						</td>
						<td>
							<?= CHtml::dropDownList('timezone_monthly_rec_hour', $currentTimezone, Yii::app()->localtime->getTimezonesArray(), array('style' => 'margin-left:5px;')) ?>
						</td>
					</tr>
				</table>
			</div>


			<div class="row-fluid" id="dynamic-for-everynmonths">
				<br>
				<table>
					<tr>
						<td><?= Yii::t('standard', '_FROM') ?></td>
						<td></td>
						<td colspan="2"><?= Yii::t('standard', '_HOURS') ?></td>
					</tr>
					<tr>
						<td>
							<div class='input-append date' data-date-format="<?= $dpFormat ?>" data-date-language= "<?= Yii::app()->getLanguage() ?>">
								<?php echo CHtml::textField('start_date', $currentStarting, array('class' => 'span6')); ?>
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>
						</td>
						<td>&nbsp;&nbsp;<?= Yii::t('report', 'at')  ?>&nbsp;&nbsp;</td>
						<td>
							<?php
								echo CHtml::dropDownList('everynmonth_rec_hour', $currentHour, $hoursList, array(
									'style' => 'width: 100px;'
								));
							?>
						</td>
						<td>
							<?= CHtml::dropDownList('timezone_everynmonth_rec_hour', $currentTimezone, Yii::app()->localtime->getTimezonesArray(), array('style' => 'margin-left:5px;width:190px;')) ?>
						</td>
					</tr>
				</table>

			</div>



		</div>
	</div>


	<div class="row-fluid">
			<div><?= Yii::t('report', 'Send the report to (<strong>required</strong>):') ?></div>
			<div class="recipients-selector">
				<select id="email_recipients" name="email_recipients">
					<?php
						if (count($currentRecipients) > 0) {
							foreach ($currentRecipients as $email) {
								echo "<option class=\"selected\" value=\"$email\" selected=\"selected\">$email</options>";
							}
						}
					?>
				</select>
			</div>
			<div id="selected-emails"></div>
			<?= Yii::t('report', 'E-mail addresses can also be external to the LMS') ?>
			<?= '<br /><br />'.CHtml::checkBox('compressed', $compressed, array('value' => 1)).' '.Yii::t('report', 'Send compressed attachment'); ?>
	</div>

</div>
<!-- --------------------------------------------  -->


<div class="form-actions jwizard">
	<?php
		// Again, there is some difference in Wizard and Non-Wizard mode
		if ($wizardMode) {
			echo CHtml::submitButton(Yii::t('standard', '_CONFIRM'), array(
	      	 	'class' => 'btn btn-docebo green big jwizard-nav jwizard-finish',
		       	'name' => 'finish_wizard_schedule',
	   		));
		   	echo CHtml::submitButton(Yii::t('standard', '_CANCEL'), array(
	   			'class' => 'btn btn-docebo black big jwizard-nav',
	   			'name' => 'prev_button',
	   		));
		}
		else {
			echo CHtml::submitButton(Yii::t('standard', '_CONFIRM'), array(
					'class' => 'btn btn-docebo green big',
					'name' => 'confirm_button',
			));

			echo CHtml::submitButton(Yii::t('standard', 'Unschedule'), array(
					'class' => 'btn btn-docebo black big',
					'name' => 'unschedule_button',
			));
			echo CHtml::button(Yii::t('standard', '_CANCEL'), array(
					'class' => 'btn btn-docebo black big close-dialog',
					'name'	=> 'cancel_button',
			));
		}
	?>
</div>


<?php echo CHtml::endForm(); ?>

<script type="text/javascript">

$(function(){

	// Run Scheduler specific JS
	var $modalDialog 			= $('#' + '<?= $markerId ?>').closest('.modal');
	var wizardMode 				= <?= $wizardMode ? 'true' : 'false' ?>;
	var emailAutocompleteUrl 	= '<?= $emailAutocompleteUrl ?>';
	ReportMan.scheduleEditor(
		<?= $wizardMode ? 'true' : 'false' ?>,
		$('#' + '<?= $markerId ?>').closest('.modal'),
		'<?= $emailAutocompleteUrl ?>',
		'report-schedule-editor',
		jwizard  // global, should be created already in index.php
	);


});

</script>


