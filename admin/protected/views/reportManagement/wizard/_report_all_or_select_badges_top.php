<?php
/**
 * Created by PhpStorm.
 * User: NikVas
 * Date: 29-Nov-15
 * Time: 17:23
 *
 * @var $this UsersSelectorController
 *
 * @var $model LearningReportFilter
 */
?>

<?php
$model = LearningReportFilter::model()->findByPk($this->idReport);
$hasBadges = $model ? $model->hasBadges() : false;
?>

<div class="report_filter-selector-top-panel">
    <br>
    <div class="row-fluid">
        <div class="span12">
            <?= Yii::t('standard','Apply this report for all badges or select custom badges') ?>
        </div>
    </div>
    <?php if (Yii::app()->user->hasFlash('error')) : ?>
        <div class="row-fluid">
            <div class="span12">
                <div class='alert alert-error alert-compact text-left'>
                    <?= Yii::app()->user->getFlash('error'); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="row-fluid">
            <div class="grey-wrapper">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <div class="span5">
                                <label class="radio">
                                    <?php
                                    echo CHtml::radioButton('badges-filter', !$hasBadges, array('value' => LearningReportFilter::ALL_COURSES));
                                    echo Yii::t('standard', 'All badges');
                                    ?>
                                </label>
                                <span class="help-block"><?= Yii::t('report', 'This option will include present and future badges.')?></span>
                            </div>
                            <div class="span5">
                                <label class="radio">
                                    <?php
                                    echo CHtml::radioButton('badges-filter', $hasBadges, array('value' => LearningReportFilter::SELECT_COURSES));
                                    echo Yii::t('standard', 'Select badges');
                                    ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(function(){
            // Call our Notification manager to signal it that filter panel is loaded. It will add/run some code.
            ReportMan.selectorTopPanelReady();
        });

    </script>