<?php
/* @var $this UsersSelectorController */
/* @var $model LearningReportFilter */
?>
<?php
$model = LearningReportFilter::model()->findByPk($this->idReport);
$hasCourses = $model ? $model->hasCourses() : false;
$hasPlans = $model ? $model->hasPlans() : false;
?>

<div class="report_filter-selector-top-panel">

	<br>
	<div class="row-fluid">
		<div class="span12">
			<?= Yii::t('standard','Apply this report to all courses or select custom courses') ?>
		</div>
	</div>

	<?php if (Yii::app()->user->hasFlash('error')) : ?>
		<div class="row-fluid">
			<div class="span12">
	   			<div class='alert alert-error alert-compact text-left'>
    				<?= Yii::app()->user->getFlash('error'); ?>
    			</div>
    		</div>
    	</div>
		<?php endif; ?>
	<div class="row-fluid">
	
	
	<div class="row-fluid">
	
		<div class="grey-wrapper">
	
			<div class="row-fluid">
				<div class="span12">
					
					<div class="row-fluid">

                        <div class="span5">
                            <label class="radio" id="select-all-courses">
                                <?php
                                echo CHtml::radioButton('courses-filter', !$hasCourses && !$hasPlans, array('value' => LearningReportFilter::ALL_COURSES, 'id' => 'select-all-courses'));
                                echo Yii::t('standard', '_ALL_COURSES');
                                ?>
                            </label>
                            <span class="help-block"><?= Yii::t('report', 'This option will include present and future courses.')?></span>
                        </div>
                        <div class="span5">
                            <label class="radio" id="select-some-courses">
                                <?php
                                echo CHtml::radioButton('courses-filter',$hasCourses || $hasPlans, array('value' => LearningReportFilter::SELECT_COURSES, 'id' => 'select-some-courses'));
                                echo Yii::t('standard', 'Select courses');
                                ?>
                            </label>
                        </div>
					</div>
					
					
					
				</div>
			</div>
		
		</div>
	
	</div>


</div>

    <script type="text/javascript">

        $(function(){
            // Call our Notification manager to signal it that filter panel is loaded. It will add/run some code.
            ReportMan.selectorTopPanelReady();
        });


        $('#select-some-courses, #select-some-courses-styler').click(function() {
            $('.deselect-all').click();
        });

        $('#select-all-courses, #select-all-courses-styler').click(function() {
            $('#selected_courses').val('[]');
            $('#selected_plans').val('[]');
        });


    </script>