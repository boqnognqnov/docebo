<style>
	label{
		display: inline-block;
		margin-right: 10px;
		margin-top: 20px;
	}
</style>
<?php
/**
 * Created by PhpStorm.
 * User: Asen Nikolov
 * Date: 23-Oct-15
 * Time: 2:07 PM
 */
$m = new ReportFieldsForm();
echo CHtml::beginForm('', 'POST', array(
	'id' => 'report-wizard-order-form',
	'name' => 'report-wizard-order-form',
	'class' => 'ajax jwizard-form form-horizontal',
));


echo CHtml::hiddenField('from_step', 'order');
echo CHtml::hiddenField('idReportType', $_REQUEST['idReportType'], array('class' => 'jwizard-no-restore'));
echo CHtml::hiddenField('idReport', $_REQUEST['idReport']);

$select = '0';
if(!empty($fields)) {
    $fields = array_filter($fields);
    foreach($fields as $key=>$field){
        if (array_key_exists($selectedOrder[0], $field)) {
            $select = $selectedOrder[0];
            break;
        }
    }
}
array_unshift($fields, Yii::t('standard', 'Select Field'));

?>
<div class="row-fluid">
	<div class="span4"><h2><?=Yii::t('standard', '_ORDER_BY')?></h2></div>
	<div class="span8">
		<?=CHtml::dropDownList('ReportFieldsForm[filters][orderBy]', $select, $fields);?>
		<div>
			<label><input type="radio" name="ReportFieldsForm[filters][typeOfOrder]" value="ASC"/><?=Yii::t('standard', '_ORD_ASC_TITLE')?></label>
			<label><input type="radio" name="ReportFieldsForm[filters][typeOfOrder]" value="DESC"/><?=Yii::t('standard', '_ORD_DESC_TITLE')?></label>
		</div>
	</div>
</div>




<div class="form-actions jwizard">

	<?=	CHtml::submitButton(Yii::t('standard', '_PREV'), array(
		'class' => 'btn btn-docebo green big jwizard-nav',
		'name' => 'prev_button',
	));
	?>

	<?=	CHtml::submitButton(Yii::t('standard', '_NEXT'), array(
		'class' => 'btn btn-docebo green big jwizard-nav next-page',
		'name' => 'next_button',
	));
	?>

	<?=	CHtml::button(Yii::t('standard', '_CANCEL'), array(
		'class' => 'btn btn-docebo black big close-dialog',
		'name' => 'cancel_step'
	));
	?>
</div>

<?= CHtml::endForm(); ?>


<script type="text/javascript">
	$(function(){
		// We have special class for this dialog; ask jWizard to add it to .modal
		jwizard.setDialogClass('report-wizard-order');

		$('input, select').styler();
		var order = '<?=$selectedOrder[1]?>';
		setTimeout(function () {
			$('.modal option[selected]').prop('selected', true);
			if (order != '') {
				$('.modal input[type=radio][value="' + order + '"]').trigger('click');
			} else {
				$('.modal input[type=radio]').first().trigger('click');
			}
		}, 300);

	});
</script>