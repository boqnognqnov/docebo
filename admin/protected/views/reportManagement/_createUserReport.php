<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700" />

<div class="printable">
<?php echo CHtml::image(Yii::app()->theme->getLogoUrl()); ?>
<hr/>
<table>
	<tbody>
		<tr>
			<td class="image"><?php echo CHtml::image($userModel->getAvatarImage(true), $userModel->firstname . " " . $userModel->lastname, array('width' => 150) ); ?></td>
			<td>
				<table>
					<tbody>
						<tr>
							<td class="fullname"><?php echo $userModel->firstname; ?> <?php echo $userModel->lastname; ?></td>
						</tr>
						<tr>
							<td class="username"><?php echo Yii::app()->user->getRelativeUsername($userModel->userid); ?></td>
						</tr>
						<tr>
							<td>
								<table class="user-report-info-data">
									<tbody>
										<tr>
											<td class="bold"><?php echo Yii::t('standard', '_LEVEL'); ?></td>
											<td><?php echo (!empty($userModel->adminLevel->groupid)? Yii::t('admin_directory', '_DIRECTORY_'.$userModel->adminLevel->groupid) :''); ?></td>
										</tr>
										<tr>
											<td class="bold"><?php echo Yii::t('standard', '_EMAIL'); ?></td>
											<td><?php echo $userModel->email; ?></td>
										</tr>
										<tr>
											<td class="bold"><?php echo Yii::t('standard', '_GROUPS'); ?></td>
											<td><?php echo implode(', ', $userGroups); ?></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td>
				<table class="user-report-stat">
					<tbody>
						<tr>
							<td>
								<table class="subscription-date">
									<tbody>
										<tr>
											<td><?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/subscription-date.png');?></td>
										</tr>
										<tr>
											<td class="data"><?php echo $userModel->register_date; ?></td>
										</tr>
										<tr>
											<td class="label"><?php echo Yii::t('report', '_DATE_INSCR'); ?></td>
										</tr>
									</tbody>
								</table>
							</td>
							<td>
								<table class="last-access">
									<tbody>
										<tr>
											<td><?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/last-access-date.png');?></td>
										</tr>
										<tr>
											<td class="data"><?php echo (!empty($userModel->lastenter)?$userModel->lastenter:Yii::t('standard', '_NEVER')); ?></td>
										</tr>
										<tr>
											<td class="label"><?php echo Yii::t('standard', '_DATE_LAST_ACCESS'); ?></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table class="total-time">
									<tbody>
										<tr>
											<td><?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/total-time.png');?></td>
										</tr>
										<tr>
											<td class="data"><?php echo $totalTime; ?></td>
										</tr>
										<tr>
											<td class="label"><?php echo Yii::t('standard', '_TOTAL_TIME'); ?></td>
										</tr>
									</tbody>
								</table>
							</td>
							<td>
								<table class="active-courses">
									<tbody>
										<tr>
											<td><?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/active-courses.png');?></td>
										</tr>
										<tr>
											<td class="data"><?php echo count($userModel->learningCourseusers); ?></td>
										</tr>
										<tr>
											<td class="label"><?php echo Yii::t('dashboard', '_ACTIVE_COURSE'); ?></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>


<div class="user-additional-fields clearfix">
	<?php if (!empty($additionalFields)) {
		$i = 0;
		foreach ($additionalFields as $fieldName => $userValue) {
			$fieldClass = ($i % 2 == 0)? 'odd' : 'even';
			echo '<div class="field '.$fieldClass.' '. (($i <= 1) ? 'firstField' : '' ) .'">
				<div class="name">'.$fieldName.'</div>
				<div class="value">'.$userValue.'</div>
			</div>';
			$i++;
		}
	}	?>
</div>

<div style="clear: both;"></div>
<table class="report-courses-summary">
	<tbody>
		<tr>
			<td>
				<table class="report-courses-summary-legend">
					<tbody>
						<tr>
							<td colspan="2" class="report-info-title">
								<?php echo Yii::t('standard', '_PROGRESS'); ?>
							</td>
						</tr>
						<tr>
							<td class="td-middle">
							<div class="donut-wrapper">
								<div class="donutContainer">
									<?php echo $pieChart; ?>
								</div>
							</div>
							</td>
							<td class="td-middle">
								<div class="summary-legend-wrapper">
									<?php echo $pieChartLegend; ?>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<?php if($lineChart !== '') : ?>
				<td class="report-activities-summary">
					<table>
						<tbody>
							<tr>
								<td class="report-stat-title">
									<?php echo Yii::t('report', '_YEARLY_ACTIVITIES_SUMMARY'); ?>
								</td>
							</tr>
							<tr>
								<td>
									<div class="lineChart-wrapper">
										<div class="lineChartContainer">
											<?php echo $lineChart; ?>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			<?php endif; ?>
		</tr>
	</tbody>
</table>

	<h4><?php echo Yii::t('standard', '_COURSES'); ?></h4>
	<?php
		$dp = $courseUserModel->dataProvider();
		$dp->pagination = false;
	?>
		<?php $this->widget('DoceboCGridView', array(
			'id' => 'course-management-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'dataProvider' => $dp,
			'cssFile' => false,
			'enableSorting' => false,
			'enablePagination' => false,
			'template' => '<div class=scroll-content>{items}</div>{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'pager' => array(
				'class' => 'DoceboCLinkPager',
				'maxButtonCount' => 8,
			),
			'ajaxUpdate' => 'all-items',
			'beforeAjaxUpdate' => 'function(id, options) {
				options.type = "POST";
				if (options.data == undefined) {
					options.data = {
						contentType: "html",
					}
				}
			}',
            'afterAjaxUpdate' => 'function(id, options){$(\'#\' + id + \' .scroll-content\').jScrollPane()}',
			'columns' => array(
				array(
					'name' => 'course.code',
					'header' => Yii::t('standard', '_COURSE_CODE'),
				),
				array(
					'name' => 'course.name',
					// 'name' => $data->course->name,
				),
				array(
					'header' => Yii::t('profile', '_USERCOURSE_STATUS'),
					'name' => 'status',
					'value' => 'LearningCourseuser::getStatusLabel($data->status);'
				),
				array(
					'header' => Yii::t('standard', 'Enrolled'),
					'name' => 'date_inscr',
					'value' => '$data->date_inscr'
				),
				array(
					'header' => Yii::t('standard', '_DATE_FIRST_ACCESS'),
					'name' => 'date_first_access',
					'value' => '$data->date_first_access'
				),
				array(
					'header' => Yii::t('standard', 'Course completion'),
					'name' => 'date_complete',
					'value' => '$data->date_complete'
				),
				array(
					'header' => Yii::t('standard', '_CREDITS'),
					'name' => 'course.credits',
				),
				array(
					'header' => Yii::t('standard', '_TOTAL_TIME'),
					'value' => '$data->renderTimeInCourse();',
				),
				array(
					'name' => 'course.score',
					'header' => Yii::t('standard', '_SCORE'),
					'value' => 'LearningCourseuser::getLastScoreByUserAndCourse($data["idCourse"],$data["idUser"])'
				),
			),
		)); ?>


	<?php if (PluginManager::isPluginActive("ClassroomApp")): ?>
		<h4><?php echo Yii::t('classroom', 'Classrooms'); ?></h4>
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'classroom-course-management-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
			'dataProvider' => LtCourseuserSession::model()->dataProviderClassroomCoursesActivity($userModel->getPrimaryKey()),
			'cssFile' => false,
			'enableSorting' => false,
			'enablePagination' => false,
			'template' => '<div class=scroll-content>{items}</div>{summary}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'ajaxUpdate' => 'all-items',
			'beforeAjaxUpdate' => 'function(id, options) {
                options.type = "POST";
                if (options.data == undefined) {
                    options.data = {
                        contentType: "html",
                    }
                }
            }',
			'columns' => array(
				array(
					'name' => 'session.name',
					'header' => Yii::t('classroom', 'Course and session information'),
					'value' => '$data->session->course->name . " - " .$data->session->name'
				),
				array(
					'header' => Yii::t('standard', '_START_DATE'),
					'name' => 'date_subscribed',
					'value' => '$data->date_subscribed'
				),
				/*array(
						'header' => Yii::t('standard', '_CREDITS'),
						'name' => 'course.credits',
				),*/
				array(
					'header' => Yii::t('standard', '_ATTENDANCE'),
					'name' => 'session.attendance',
					'value' => '$data->session->renderAttendance($data->id_user)',
				),
				array(
					'header' => Yii::t('standard', '_STATUS'),
					'name' => 'status',
					'value' => 'Yii::t("standard", LtCourseuserSession::getStatusLangLabel($data->status))'
				),
				array(
					'header' => Yii::t('standard', '_SCORE'),
					'value' => 'intval($data->evaluation_score) . "/" . intval($data->session->score_base)'
				),
			),
		)); ?>
	<?php endif; ?>
</div>