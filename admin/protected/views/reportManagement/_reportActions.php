<?php
	// Wizard's Dialog2 modal-body ID
	$wizardDialogId 	= 'custom-report-wizard';
	// jWizard dispatcher where all request are sent to
	$wizardUrl 			= Docebo::createAdminUrl('reportManagement/customReportWizard', array());

?>


<div class="">
	<ul>
		<?php if($data->is_standard == 0) { ?>
        <li>
        	<?php
        		$editScheduleUrl = Docebo::createAdminUrl('reportManagement/axEditSchedule', array(
        			'id' => $data->id_filter	
        		));
            	echo CHtml::link(Yii::t('standard', '_SCHEDULE'), $editScheduleUrl, array(
                	'class' => 'open-dialog',
                	'rel' 	=> 'edit-schedule-report',	 
                	'data-dialog-title' => Yii::t('standard', '_SCHEDULE'),
                	'title'	=> Yii::t('standard', '_SCHEDULE')
            	)); 
            ?>
        	
        </li>
		<?php } ?>
        <li>
        	<?php
        		echo CHtml::link(Yii::t('standard', '_MOD'), $wizardUrl . '&idReport=' . $data->id_filter, array(
        			"class" => "open-dialog edit-action",
        			"rel" 	=> $wizardDialogId,
        			"data-dialog-title" => Yii::t('standard', '_MOD'),
        			"title"	=> Yii::t("standard", "_MOD")
        		));;
        	?>
        </li>
        
        <li>
        	<?php 
        	
        		echo CHtml::link(Yii::t('standard', '_DEL'), "", array(
        			"class" => "ajaxModal red",
        			"data-toggle" => "modal",
        			"data-modal-class" => "delete-node",
        			"data-modal-title" => "Delete Report",
        			"data-buttons" => '[
							{"type": "submit", "title": "'. Yii::t("standard", "_CONFIRM")  . '"},
							{"type": "cancel", "title": "'. Yii::t("standard", "_CANCEL")  . '"}
        					]',
        			"data-url" => "reportManagement/deleteReport&id=$data->id_filter",
        			"data-after-loading-content" => "hideConfirmButton();",
        			"data-after-submit" => "updateReportContent",
        			"title" => Yii::t("standard", "_DEL"),
        	));
        	
        	?>
        </li>
        

	</ul>
</div>


