<?php 
	echo CHtml::beginForm('', 'POST', array(
		'class'=>'ajax',
	));
	
	echo Chtml::hiddenField('fileName', $fileName);
	echo Chtml::hiddenField('exportType', $exportType);
	
	$downloadFileUrl = Docebo::createAbsoluteAdminUrl('reportManagement/progressiveExporter', array(
		'downloadGo' 	=> 1,
		'fileName'	=> $fileName, 
	));
	
	$progressClass = in_array($nextPhase, array('extract', 'convert')) ? 'progress-striped active' : '';

	// Dialog title is changinh during export...
	switch ($nextPhase) {
		case 'extract' 	: $dialogTitle = '<span></span>' . Yii::t('standard', '_EXPORT') . '...' ; break;
		case 'convert' 	: $dialogTitle = '<span></span>' . Yii::t('standard', 'Converting') . '...' ; break;
		case 'download' : $dialogTitle = '<span></span>' . Yii::t('standard', 'Export complete'); break;
	}
	
	
?>
<div id="report-exporter-container" class="report-exporter-container">


	<div class="row-fluid">
		<div class="span12">
			
			<?php if ($totalNumber > 0) : ?>
				<div class="pull-left sub-title" ><?= Yii::t('standard', '_EXPORT') ?>: <strong><?= $reportTitle ?></strong></div>
				<div class="pull-right percentage-gauge"><?= number_format($exportingPercent,2) ?>%</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<?php if ($totalNumber > 0) : ?>
				<div class="docebo-progress progress progress-success <?= $progressClass ?>">
   					<div style="width: <?= $exportingPercent ?>%;" class="bar full-height" id="uploader-progress-bar"></div>
				</div>
			<?php else: ?>
				<div class="text-center"><h2><?= Yii::t('standard', 'Report is empty') ?>!</h2></div>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="row-fluid">
		<div class="span12 action-undergoing">
			<?php if ($totalNumber > 0) : ?>
				<?php if ($nextPhase == 'extract') : ?>
					<?= Yii::t('standard', 'Exporting {x} out of {y}', array('{x}' => $exportingNumber, '{y}' => $totalNumber)) ?>
				<?php elseif ($nextPhase == 'convert') : ?>
					<?= Yii::t('standard', 'Now converting your report') ?> ...	
				<?php elseif ($nextPhase == 'download') : ?>	
					<div class="export-success"><?= Yii::t('standard', 'Your report has been exported successfully') ?></div>				  
					<div class="download-note"><?= Yii::t('standard', 'You can now download your file') ?></div>
					<?php if ($forceCsvMaxRows) : ?>
						<div class="forced-csv">
							<?= Yii::t('standard', 'Export is over the limit') ?>
						</div>
					<?php endif;?>	
					
					<?php if ($forceCsvMissingGnumeric) : ?>
						<div class="forced-csv">
							<?= Yii::t('standard', 'Conversion utility missing in this installation. Format changed to CSV') ?>!
						</div>
					<?php endif;?>	
					
					
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
	
</div>

<div class="form-actions">
	<?php
		if ($nextPhase == 'download') {
			echo CHtml::submitButton(Yii::t('standard', '_DOWNLOAD'), array(
				'class' => 'btn-docebo green big download-file',
				'name'	=> 'download',
			));
		} 
	?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn-docebo black big close-dialog')); ?>
</div>




<?= CHtml::endForm() ?>


<script type="text/javascript">

	$(function(){
		var $modal = $('#report-exporter-container').parents('.modal');
		// Set dialog title, depending on export phase we are in
		var dialogTitle = '<?= $dialogTitle ?>';
		$modal.find('.modal-header h3').html(dialogTitle);

		// On DOANLOAD click
		$('input[name="download"]').on('click', function(e) {
			e.preventDefault();
			$modal.find(".modal-body").dialog2("close");
			var url = '<?= $downloadFileUrl ?>';
			window.open(url);
		});
		
	});

</script>