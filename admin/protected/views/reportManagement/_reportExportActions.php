<ul>
	<li>
		<?php 
			//echo CHtml::link(Yii::t('standard', '_EXPORT_XLS'), array('reportManagement/exportCustomReport', 'type' => 'Excel5', 'id' => $id));
			$progressUrl = Docebo::createAbsoluteLmsUrl('progress/run', array(
				'type'		=> Progress::JOBTYPE_CUSTOM_REPORT,
				'idReport' 	=> $id,
				'exportType'=> LearningReportFilter::EXPORT_TYPE_XLS,					
			));
			echo CHtml::link(Yii::t('standard', '_EXPORT_XLS'), $progressUrl, array(
				'class' 	=> 'open-dialog',
				'rel' 		=> 'progressive-report-export-dialog',
				'data-dialog-title' => '',
			));
		?>
	</li>
	
	
	<li>
		<?php 
			//echo CHtml::link(Yii::t('standard', '_EXPORT_CSV'), array('reportManagement/exportCustomReport', 'type' => 'CSV', 'id' => $id));
			$progressUrl = Docebo::createAbsoluteLmsUrl('progress/run', array(
				'type'		=> Progress::JOBTYPE_CUSTOM_REPORT,
				'idReport' 	=> $id,
				'exportType'=> LearningReportFilter::EXPORT_TYPE_CSV,
			));
			echo CHtml::link(Yii::t('standard', '_EXPORT_CSV'), $progressUrl, array(
				'class' 	=> 'open-dialog',
				'rel' 		=> 'progressive-report-export-dialog',
				'data-dialog-title' => '',
			));
		?>
	</li>
	
	
	<li>
		<?php
			//echo CHtml::link(Yii::t('standard', '_EXPORT_HTML'), array('reportManagement/exportCustomReport', 'type' => 'HTML', 'id' => $id));
			$progressUrl = Docebo::createAbsoluteLmsUrl('progress/run', array(
				'type'		=> Progress::JOBTYPE_CUSTOM_REPORT,
				'idReport' 	=> $id,
				'exportType'=> LearningReportFilter::EXPORT_TYPE_HTML,
			));
			echo CHtml::link(Yii::t('standard', '_EXPORT_HTML'), $progressUrl, array(
				'class' 	=> 'open-dialog',
				'rel' 		=> 'progressive-report-export-dialog',
				'data-dialog-title' => '',
			));
		?>
	</li>
	
	
</ul>