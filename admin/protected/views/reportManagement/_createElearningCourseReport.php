<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700" />
<!--<link type="text/css" rel="stylesheet"  href="<?php //echo Yii::app()->getBaseUrl(true); ?>/css/report-pdf.css" rel="stylesheet">-->
<?php
	Yii::app()->clientScript->scriptMap['jquery.js'] = false;
	Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
	Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;
	Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
?>
<div class="printable">
	<?php echo CHtml::image(Yii::app()->theme->getLogoUrl(), '', array(
		'class' => 'course-report-logo-img'
	)); ?>

	<hr/>
	<!--course summary-->

	<table>
		<tbody class="<?php echo (Yii::app()->chart->isIE8()?'IE8':''); ?>">
			<tr>
				<td class="image"><?php echo CHtml::image($courseModel->courseLogoUrl, '', array('width' => 150)); ?></td>
				<td>
				<table>
				<tbody>
				<tr>
					<td class="fullname" colspan="2"><?php echo $courseModel->name; ?></td>
				</tr>
				<tr>
				<td>
					<table>
						<tbody>
							<tr>
								<td class="completed" valign="middle"><?php echo Yii::t('stats', 'Users that have completed the course after 30 days'); ?></td>
								<td class="completed-stat" valign="middle"><?php echo $singlePieChart; ?></td>
							</tr>
						</tbody>
					</table>
				</td>
				<td class="center-aligned">
					<table>
						<tbody>
							<tr>
								<td><?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/enrolled-users.png');?></td>
							</tr>
							<tr>
								<td class="data"><?php echo $totalCourseUsers; ?></td>
							</tr>
							<tr>
								<td><?php echo Yii::t('standard', 'Enrolled users'); ?></td>
							</tr>
						</tbody>
					</table>
				</td>
				<td class="center-aligned">
					<table>
						<tbody>
							<tr>
								<td><?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/days-since-launch.png');?></td>
							</tr>
							<tr>
								<td class="data"><?php echo $totalCourseDays; ?></td>
							</tr>
							<tr>
								<td><?php echo Yii::t('stats', 'Days of activity'); ?></td>
							</tr>
						</tbody>
					</table>
				</td>
				<td class="center-aligned">
					<table>
						<tbody>
							<tr>
								<td valign="top"><?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/active-courses.png');?></td>
							</tr>
							<tr>
								<td class="data"><?php echo $totalOrganizationCourses; ?></td>
							</tr>
							<tr>
								<td><?php echo Yii::t('standard', 'Training materials'); ?></td>
							</tr>
						</tbody>
					</table>
					</td>
					</tr>
					</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>


	<!--course summary end-->
	<table class="report-courses-summary">
		<tbody>
			<tr>
				<td>
					<table class="report-courses-summary-legend">
						<tbody>
							<tr>
								<td colspan="2" class="report-info-title">
									<?php echo Yii::t('standard', '_PROGRESS'); ?>
								</td>
							</tr>
							<tr>
								<td class="td-middle">
									<div class="donut-wrapper">
										<div class="donutContainer">
											<?php echo $pieChart; ?>
										</div>
									</div>
								</td>
								<td class="td-middle">
									<div class="summary-legend-wrapper">
										<?php echo $pieChartLegend; ?>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
				<td>
					<table>
						<tbody>
							<tr>
								<td class="report-stat-title">
									<?php echo Yii::t('report', '_YEARLY_ACTIVITIES_SUMMARY'); ?>
								</td>
							</tr>
							<tr>
								<td>
									<div class="lineChart-wrapper">
										<div class="lineChartContainer">
											<?php echo $lineChart; ?>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</div>