<?php
$headers = $rawData['headers'];
$rows = $rawData['excelData'];
?>



<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', '_REPORTS') => array('reportManagement/index'),
	$reportFilterModel->filter_name,
); ?>

<h3 class="title-bold back-button">
	<?php echo CHtml::link(Yii::t('standard', '_BACK'), $back); ?>
	<span><?php echo $reportFilterModel->filter_name; ?></span>
</h3>

<div class="report-bottom-side">
	<div class="filters-wrapper">
		<div class="filters clearfix">
			<div class="select-wrapper">
				<label><?php echo Yii::t('standard', '_EXPORT'); ?></label>
				<?php
					$actions = array(
						//'PDF' => Yii::t('standard', '_EXPORT_PDF'),
						'Excel5' => Yii::t('standard', '_EXPORT_XLS'),
						'CSV' => Yii::t('standard', '_EXPORT_CSV'),
						'HTML' => Yii::t('standard', '_EXPORT_HTML'),
					);

					echo CHtml::dropDownList('massive_action', false, $actions, array('id' => 'report-management-export-massive-action', 'empty' => Yii::t('standard', 'Select action'), 'data-report-id' => $reportFilterModel->id_filter));
					?>
			</div>
			<?php /* ?>
					<div class="input-wrapper">
	            <input type="text" class="typeahead">
	            <span class="search-icon"></span>
            </div>
					<?php */
			?>
		</div>
	</div>
</div>


<style>
<!--

	table.bigtable thead { 
		display:block; 
		margin:0px; 
		cell-spacing:0px; 
		left:0px;
	}
	
	table.bigtable tbody { 
		display:block; 
		overflow:auto; 
		height:600px;
	}

-->
</style>



<div class="row-fluid">

<table class="span12 table bigtable">

	<tr>
	
		<?php foreach ($headers as $header) : ?>
		<th  style="">
			<?= $header ?>
		</th>
		<?php endforeach;?>
			
	
	</tr>


	<?php foreach ($rows as $row) : ?>
		<tr>
			<?php foreach ($row as $cell) : ?>
				<td style="white-space: nowrap;"><?= $cell ?></td>
			<?php endforeach;?>
		</tr>
	<?php endforeach;?>	

</table>


</div>