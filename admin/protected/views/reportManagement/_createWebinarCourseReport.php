<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700" />
<!--<link type="text/css" rel="stylesheet"  href="<?php //echo Yii::app()->getBaseUrl(true); ?>/css/report-pdf.css" rel="stylesheet">-->
<?php
    /* @var $this ReportManagementController */
    /* @var $pieChartData array */
    /* @var $singlePieChart string */
    /* @var $course LearningCourse */
    /* @var $session LtCourseSession */

	Yii::app()->clientScript->scriptMap['jquery.js'] = false;
	Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
	Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;
	Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
?>
<div class="printable classroom-course-report-container">
	<table width="100%">
        <tr>
            <td width="50%">
                <?php echo CHtml::image(Yii::app()->theme->getLogoUrl(), '', array(
									'class' => 'course-report-logo-img'
								)); ?>
            </td>
            <td width="50%">
                <h2 class="course-title"><?php echo $course->name; ?></h2>
            </td>
        </tr>
    </table>
	<hr/>

	<!--course summary-->
	<table>
        <tbody class="<?php echo (Yii::app()->chart->isIE8()?'IE8':''); ?>">
            <tr>
                <td><?= Yii::t('classroom', 'Session:') ?></td>
                <td><?= $session->renderDetailedSessionName(' - '); ?></td>
            </tr>
            <tr>
                <td class="image"><?php echo CHtml::image($course->courseLogoUrl, '', array('width' => 150)); ?></td>
                <td>
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="completed" valign="middle"><?php echo Yii::t('stats', 'Users that have passed the session(s)'); ?></td>
                                            <td class="completed-stat" valign="middle"><?php echo $singlePieChart; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td class="center-aligned">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td><?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/enrolled-users.png');?></td>
                                        </tr>
                                        <tr>
                                            <td class="data"><?php echo $pieChartData['total']; ?></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo Yii::t('standard', 'Enrolled users'); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td class="center-aligned">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td><?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/session-hours.png');?></td>
                                        </tr>
                                        <tr>
                                            <td class="data"><?php echo $pieChartData['totalHours']; ?></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo Yii::t('stats', 'Total session hours'); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

	<!--course summary end-->
	<?php if (!$session->isNewRecord) : ?>
    <div class="course-report-session-details">
			<div class="row-fluid">
				<div class="span12" style="padding-top: 15px;">
					<h4><?= Yii::t('classroom', 'Webinar information') ?></h4>
                    <hr>
                    <table>
                        <tr>
                            <td style="width:  100%">
									<?=$session->getToolName()?>
                            </td>
                            <td style="width:  100%">
									<?=$session->date_begin?>
                            </td>
                            <td style="width:  100%">
									<?=$session->getTotalHours()?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
    </div>
    <br>
		<div class="user-report-session-statistics">
			<h2 class="course-title" style="margin: 20px 0 10px;"><?php echo Yii::t('stats', 'Session statistics'); ?></h2>
			<div class="courses-grid-wrapper">
				<?php
				$dataProvider1 = WebinarSessionUser::model()->dataProviderUserStatistics($session->getPrimaryKey());
				$dataProvider1->pagination = false;
				$dataProvider1->sort = false;
				$this->widget('zii.widgets.grid.CGridView', array(
					'id' => 'session-statistics-grid',
					'htmlOptions' => array('class' => 'grid-view clearfix', 'style'=>'padding-top: 0;border-top: 1px solid #E4E6E5;'),
					'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
					'dataProvider' => $dataProvider1,
					'cssFile' => false,
					'template' => '{items}',
					'summaryText' => Yii::t('standard', '_TOTAL'),
					'ajaxUpdate' => 'all-items',
					'beforeAjaxUpdate' => 'function(id, options) {
					options.type = "POST";
					if (options.data == undefined) {
						options.data = {
							contentType: "html",
						}
					}
				}',
					'columns' => array(
						array(
							'header' => Yii::t('standard', '_USERNAME'),
							'name' => 'user.userid',
							'value' => 'Yii::app()->user->getRelativeUsername($data->userModel->userid)',
                            'headerHtmlOptions'=>array('style'=>'width: 100%')
						),
						array(
							'name' => 'status',
							'value' => 'Yii::t("standard", WebinarSessionUser::getStatusLangLabel($data->status))',
                            'headerHtmlOptions'=>array('style'=>'width: 100%')
						),
						array(
							'name' => 'score',
							'header' => Yii::t('standard', '_SCORE'),
							'value' => '$data->renderSessionScore()', //'intval($data->evaluation_score) . "/" . intval($data->ltCourseSession->score_base)'
                            'headerHtmlOptions'=>array('style'=>'width: 100%')
						),
					),
				)); ?>
			</div>
		</div>

	<?php else: ?>

		<div class="user-report-session-statistics">
			<h2 class="course-title" style="margin: 20px 0 10px;"><?php echo Yii::t('stats', 'Session statistics'); ?></h2>
			<div class="courses-grid-wrapper">

				<?php if (!empty($course->learningWebinarSessions)) : ?>
					<?php
					$dataPrivider2 = WebinarSessionUser::model()->dataProviderCourseStatistics($course->getPrimaryKey());
					$dataPrivider2->pagination = false;
					$dataProvider2->sort = false;
					$this->widget('zii.widgets.grid.CGridView', array(
						'id' => 'session-statistics-grid',
						'htmlOptions' => array('class' => 'grid-view clearfix', 'style'=>'padding-top: 0;border-top: 1px solid #E4E6E5;'),
						'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
						'dataProvider' => $dataPrivider2,
						'cssFile' => false,
						'template' => '{items}',
						'summaryText' => Yii::t('standard', '_TOTAL'),
						'ajaxUpdate' => 'all-items',
						'beforeAjaxUpdate' => 'function(id, options) {
                        options.type = "POST";
                        if (options.data == undefined) {
                            options.data = {
                                contentType: "html",
                            }
                        }
                    }',
						'columns' => array(
							array(
								'header' => Yii::t('standard', '_USERNAME'),
								'name' => 'user.userid',
								'value' => 'Yii::app()->user->getRelativeUsername($data->userModel->userid)',
                                'headerHtmlOptions'=>array('style'=>'width: 100%')
							),
							array(
								'header' => Yii::t('classroom', 'Session name'),
								'name' => 'session.name',
								'value' => '$data->sessionModel->renderDetailedSessionName()',
								'type' => 'raw',
                                'headerHtmlOptions'=>array('style'=>'width: 100%')
							),
							array(
								'name' => 'session.status',
								'value' => 'Yii::t("standard", WebinarSessionUser::getStatusLangLabel($data->status))',
                                'headerHtmlOptions'=>array('style'=>'width: 100%')
							),
							array(
								'name' => 'session.score',
								'header' => Yii::t('standard', '_SCORE'),
								'value' => '$data->renderSessionScore()',
                                'headerHtmlOptions'=>array('style'=>'width: 100%')
							),
						),
					)); ?>
				<?php else: ?>
					<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id' => 'session-statistics-grid',
						'htmlOptions' => array('class' => 'grid-view clearfix', 'style'=>'padding-top: 0;border-top: 1px solid #E4E6E5;'),
						'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
						'dataProvider' => new CArrayDataProvider(array()),
						'cssFile' => false,
						'template' => '{items}',
						'summaryText' => Yii::t('standard', '_TOTAL'),
						'pager' => array(
							'class' => 'DoceboCLinkPager',
							'maxButtonCount' => 8,
						),
						'ajaxUpdate' => 'all-items',
						'beforeAjaxUpdate' => 'function(id, options) {
					options.type = "POST";
					if (options.data == undefined) {
						options.data = {
							contentType: "html",
						}
					}
				}',
						'columns' => array(
							array(
								'header' => Yii::t('standard', '_USERNAME'),
								'name' => 'user.userid',
                                'headerHtmlOptions'=>array('style'=>'width: 100%')
							),
							array(
								'header' => Yii::t('classroom', 'Session name'),
								'name' => 'session.name',
                                'headerHtmlOptions'=>array('style'=>'width: 100%')
							),
							array(
								'header' => Yii::t('standard', '_ATTENDANCE'),
								'name' => 'session.attendance',
                                'headerHtmlOptions'=>array('style'=>'width: 100%')
							),
							array(
								'name' => 'session.status',
                                'headerHtmlOptions'=>array('style'=>'width: 100%')
							),
							array(
								'name' => 'session.score',
								'header' => Yii::t('standard', '_SCORE'),
                                'headerHtmlOptions'=>array('style'=>'width: 100%')
							),
						),
					)); ?>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>

</div> <!-- End of .printable -->
