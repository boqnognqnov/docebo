<div class="report-actions">
	<ul>
		<li class="report-action clearfix">
			<?php
				$progressUrl = Docebo::createAbsoluteLmsUrl('progress/run', array(
					'type'		=> Progress::JOBTYPE_CUSTOM_REPORT,
					'idReport' 	=> $model->id_filter,
					'exportType'=> LearningReportFilter::EXPORT_TYPE_XLS,
				));
				echo CHtml::link(Yii::t('standard', '_EXPORT_XLS'), $progressUrl, array(
					'class' 	=> 'open-dialog',
					'rel' 		=> 'progressive-report-export-dialog',
					'data-dialog-title' => '',
				)); 
			?>
		</li>
		<li class="report-action clearfix">
			<?php
				$progressUrl = Docebo::createAbsoluteLmsUrl('progress/run', array(
					'type'		=> Progress::JOBTYPE_CUSTOM_REPORT,
					'idReport' 	=> $model->id_filter,
					'exportType'=> LearningReportFilter::EXPORT_TYPE_CSV,
				));
				echo CHtml::link(Yii::t('standard', '_EXPORT_CSV'), $progressUrl, array(
					'class' 	=> 'open-dialog',
					'rel' 		=> 'progressive-report-export-dialog',
					'data-dialog-title' => '',
				)); 
			?>
		</li>
		<li class="report-action clearfix">
			<?php
				$progressUrl = Docebo::createAbsoluteLmsUrl('progress/run', array(
					'type'		=> Progress::JOBTYPE_CUSTOM_REPORT,
					'idReport' 	=> $model->id_filter,
					'exportType'=> LearningReportFilter::EXPORT_TYPE_HTML,
				));
				echo CHtml::link(Yii::t('standard', '_EXPORT_HTML'), $progressUrl, array(
					'class' 	=> 'open-dialog',
					'rel' 		=> 'progressive-report-export-dialog',
					'data-dialog-title' => '',
				)); 
			?>
		</li>
	</ul>
</div>
