<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'group_form',
	)); ?>

	<p><?php echo Yii::t('standard', '_REPORT'). ': "<strong>'.$model->filter_name.'</strong>"'; ?></p>

	<div class="clearfix">
		<?php echo $form->checkbox($model, 'confirm', array('id' => 'LearningReportFilter_confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed', array($model->filter_name)), 'LearningReportFilter_confirm_'.$checkboxIdKey); ?>
		<?php echo $form->error($model, 'confirm'); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>