<?php
	/* @var $courseUserModel LearningCourseuser */

	Yii::app()->clientScript->scriptMap['jquery.js'] = false;
	Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
	Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
?>
<div class="user-report-actions">
	<div id="advanced_search_course_elearning_report_container" class="clearfix">
		<span><?php echo Yii::t('course', '_COURSE_SELECTION'); ?></span>
    <?php echo CHtml::textField('LearningCourse[name]', '', array(
			'id' => 'advanced-search-course-report-modal',
			'class' => '',
			'autocomplete' => 'off',
			'data-url' => Docebo::createAppUrl('admin:courseManagement/courseAutocomplete'),
			'data-type' => 'core',
			'data-formatted-course-name' => true,
			'placeholder' => Yii::t('report', 'Type course here'),
		)); ?>

		<?php $this->renderPartial('//common/_modal', array(
			'config' => array(
				'class' => 'new-user-summary',
				'modalTitle' => Yii::t('report', '_SINGLE_COURSE_SUMMARY'),
				'linkTitle' => Yii::t('certificate', '_GENERATE'),
				'url' => 'reportManagement/createCourseReport',
// 				'buttons' => array(
// 					array(
// 						'type' => 'submit',
// 						'title' => Yii::t('standard', '_CONFIRM'),
// 					),
// 					array(
// 						'type' => 'cancel',
// 						'title' => Yii::t('standard', '_CANCEL'),
// 					),
// 				),
				'buttons' => array(),
				'beforeLoadingContent' => 'function() { if(generateRaportVerifyInitData(config)) {$(\'.modal.in\').modal(\'hide\'); return true;} else {return false;}}',
				'afterLoadingContent' => 'function() {applyTypeahead($(\'.modal.in .typeahead\'));applyDonut(\'.modal.in .donutContainer\'); applyLineCharts(\'.modal.in .lineChartContainer\');}',
				// 'afterSubmit' => 'updateGroupContent',
			),
		)); ?>

		<div class="report-popup-actions">
			<?php echo CHtml::link(Yii::t('report', 'Print') . '<span></span>', 'javascript:void(0);', array('class' => 'report-action-print')); ?>
			<?php echo CHtml::link(Yii::t('report', 'Download as PDF') . '<span></span>', Yii::app()->createAbsoluteUrl('reportManagement/createCourseReport', array('id' => $courseModel->idCourse, 'createPdf' => true)), array('class' => 'report-action-pdf')); ?>
		</div>
	</div>
</div>

<div class="printable">
	<?php echo CHtml::image(Yii::app()->theme->getLogoUrl(), '', array(
		'class' => 'course-report-logo-img'
	)); ?>


	<hr/>
	<!--course summary-->
	<div class="course-report-summary clearfix">
		<div class="course-report-info-image">
				<?php echo CHtml::image($courseModel->courseLogoUrl, '', array('width' => 150)); ?>
		</div>
		<div class="course-report-right-side clearfix">
			<h2 class="course-title"><?php echo $courseModel->name; ?></h2>
			<div class="courses-stat <?php echo (Yii::app()->chart->isIE8()?'IE8':''); ?>">
				<div class="completed">
					<p><?php echo Yii::t('stats', 'Users that have completed the course after 30 days'); ?></p>
					<div class="completed-stat">
						<?php echo $singlePieChart; ?>
					</div>
				</div>
				<div class="enrolled-users">
					<?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/enrolled-users.png');?>
					<div class="data"><?php echo $totalCourseUsers; ?></div>
					<div class="label"><?php echo Yii::t('standard', 'Enrolled users'); ?></div>
				</div>
				<div class="days-since-launch">
					<?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/days-since-launch.png');?>
					<div class="data"><?php echo $totalCourseDays; ?></div>
					<div class="label"><?php echo Yii::t('stats', 'Days of activity'); ?></div>
				</div>
				<div class="training-resources">
					<?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/active-courses.png');?>
					<div class="data"><?php echo $totalOrganizationCourses; ?></div>
					<div class="label"><?php echo Yii::t('standard', 'Training materials'); ?></div>
				</div>
			</div>
		</div>
	</div>
	<!--course summary end-->

	<?php
	$event = new DEvent($this, array('controller' => $this, 'courseModel' => $courseModel, 'usersModel' => $courseUserModel));
	Yii::app()->event->raise('OnCourseSummaryRender', $event);
	?>

	<table class="report-courses-summary">
		<tbody>
			<tr>
				<td>
					<table class="report-courses-summary-legend">
						<tbody>
							<tr>
								<td colspan="2" class="report-info-title">
									<?php echo Yii::t('standard', '_PROGRESS'); ?>
								</td>
							</tr>
							<tr>
								<td class="td-middle">
									<div class="donut-wrapper">
										<?php echo $pieChart; ?>
									</div>
								</td>
								<td class="td-middle">
									<div class="summary-legend-wrapper">
										<?php echo $pieChartLegend; ?>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
				<td class="report-activities-summary">
					<table>
						<tbody>
							<tr>
								<td class="report-stat-title">
									<?php echo Yii::t('report', '_YEARLY_ACTIVITIES_SUMMARY'); ?>
								</td>
							</tr>
							<tr>
								<td>
									<div class="lineChart-wrapper">
										<?php echo $lineChart; ?>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>

	<div class="user-report-courses">
		<div class="courses-title"><?php echo Yii::t('standard', '_USERS'); ?></div>
		<div class="courses-grid-wrapper">
			<?php
			$event = new DEvent($this, array('controller' => $this, 'courseModel' => $courseModel));
			Yii::app()->event->raise('OnCourseReportUserGridRender', $event);
			if($event->shouldPerformAsDefault()) {
				$this->widget('DoceboCGridView', array(
					'id' => 'course-management-grid',
					'htmlOptions' => array('class' => 'grid-view clearfix'),
					'dataProvider' => $courseUserModel->dataProviderCourseReport(),
					'cssFile' => false,
					'template' => '<div class=scroll-content>{items}</div>{summary}{pager}',
					'summaryText' => Yii::t('standard', '_TOTAL'),
					'pager' => array(
						'class' => 'DoceboCLinkPager',
						'maxButtonCount' => 8,
					),
					'ajaxUpdate' => 'all-items',
					'beforeAjaxUpdate' => 'function(id, options) {
						options.type = "POST";
						if (options.data == undefined) {
							options.data = {
								contentType: "html",
							}
						}
					}',
					'columns' => array(
						array(
							'name' => 'user.userid',
							'value' => '
							(Yii::app()->user->getIsPu()  && !Yii::app()->user->checkAccess("/framework/admin/usermanagement/view")) ? Yii::app()->user->getRelativeUsername($data->user->userid) :
							CHtml::link(Yii::app()->user->getRelativeUsername($data->user->userid), Docebo::createAppUrl(\'lms:player/report/byUser\', array(\'course_id\'=>$data->idCourse, \'user_id\'=>$data->idUser)))',
							'type' => 'raw'
						),
						array(
							'name' => 'date_inscr',
							'value' => '$data->date_inscr'
						),
						array(
							'header' => Yii::t('standard', '_DATE_FIRST_ACCESS'),
							'name' => 'date_first_access',
							'value' => '$data->date_first_access'
						),
						array(
							'header' => Yii::t('standard', 'Course completion'),
							'name' => 'date_complete',
							'value' => '$data->date_complete'
						),
						array(
							'header' => Yii::t('standard', '_PROGRESS'),
							'value' => 'LearningOrganization::getProgressByUserAndCourse(' . $courseModel->idCourse . ', $data->user->idst)."%"',
						),
						array(
							'header' => Yii::t('standard', '_TOTAL_TIME'),
							'value' => '$data->renderTimeInCourse();',
						),
						array(
							'header' => Yii::t('profile', '_USERCOURSE_STATUS'),
							'name' => 'status',
							'value' => 'LearningCourseuser::getStatusLabel($data->status);'
						),
						array(
							'header' => Yii::t('standard', '_SCORE'),
							'value' => 'LearningCourseuser::getLastScoreByUserAndCourse($data["idCourse"],$data["idUser"])',
						),
					),
					'loadIntoDialog' => true
				));
			}
			?>
		</div>
	</div>

</div> <!-- End of .printable -->


<script type="text/javascript">
	$(document).ready(function() {
		$('.report-action-print').click(function(){
			window.print();
		});
		replacePlaceholder();
		$('.scroll-content').jScrollPane({
				autoReinitialise: true
		});

		$('.modal').on('hide', function () {
		    id = '#'+$(this).attr('id');
			$(id).find('#advanced_search_course_elearning_report_container').remove();
		});

		$("#advanced-search-course-report-modal").autocomplete({
			source: function( request, response ) {
				$.ajax({
					url: "<?=Docebo::createAppUrl('admin:courseManagement/courseAutocomplete')?>",
					dataType: "json",
					type: "POST",
					data: {
						data:{
							formattedCourseName: true,
							query: request.term
						}
					},
					success: function( data ) {
						var options = [];
						$.each(data.options, function(index, value){
							options.push({
								"value" : index,
								"label" : value
							})
						});
						response( options );
					}
				});
			},
			minLength: 1,
			select: function( event, ui ) {
				if ($('#advanced_search_course_elearning_report_container').find('#idCourse')) {
					$('#advanced_search_course_elearning_report_container').find('#idCourse').remove();
				}
				var hiddenField = $('<input type="hidden" id="idCourse" name="idCourse" value="' + ui.item.value + '"/>');
				$("#advanced_search_course_elearning_report_container").append(hiddenField);
				ui.item.value = ui.item.label;
			}
		});

	});
</script>
