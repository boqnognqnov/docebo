<?php
/**
 * Created by PhpStorm.
 * User: asen
 * Date: 07-Dec-15
 * Time: 2:44 PM
 */
?>
<style>
	.customLoading label {
		display: inline-block;
		/*font-weight: bold;*/
	}

	.customLoading .grey-wrapper .selection * {
		vertical-align: top;
	}

	.customLoading .selection {
		width: 33%;
		display: inline-block;
		vertical-align: top;
	}

	.customLoading .comment {
		font-size: 0.9em;
		color: grey;
	}

	.customLoading .tab-content {
		margin-left: 210px;
	}

	.customLoading.gridItemsContainer {
		clear: none !important;
	}

	.customLoading ul#selector-tabs {
		height: 500px;
	}
	ul#selector-tabs a{
		border: 0;
	}
	ul#selector-tabs a:hover{
		background-color: #666666 !important;
	}
	.customLoading div.tab-content{
		height: 500px;
		overflow-y: auto;
		overflow-x: hidden;
	}
	.customLoading .tab-content .filter-wrapper {
		border-bottom: 1px solid #E4E6E5;
		border-top: 1px solid #E4E6E5;
		padding: 7px 0px 7px 10px;
		background: none repeat scroll 0 0 #F1F3F2;
		position: relative;
	}
	.customLoading .labelBlock{
		display: inline-block;
		width: 88%;
	}

	.customLoading .icon-select-node-yes,
	.customLoading .icon-select-node-no,
	.customLoading .icon-select-node-descendants {
		height: 19px !important;
		width: 19px !important;
		display: inline-block !important;
		vertical-align: middle !important;
		margin: 0 5px 0 8px !important;

	}
	.customLoading .tab-content a{
		color: #0465AC;
		text-decoration: underline;
		font-weight: 600;
	}
	.customLoading .icon-select-node-yes {
		background: url(../themes/spt/images/icons_elements.png) -166px -200px no-repeat !important;;
	}

	.customLoading .icon-select-node-no {
		background: url(../themes/spt/images/icons_elements.png) -143px -200px no-repeat !important;;
	}

	.customLoading .icon-select-node-descendants {
		background: url(../themes/spt/images/icons_elements.png) -189px -200px no-repeat !important;;
	}
	.customLoading .i-sprite.is-search{
		margin-left: -25px;
	}
	.customLoading ul.ui-fancytree.fancytree-container{
		width: 100%;
	}
	.customLoading .filter-wrapper .span5.legend{
		margin-left: -5px;
		padding-top: 8px;
	}
	.customLoading .filter-wrapper .span3>input{
		position: absolute;
		right: 7px;
		top:11px;
		width: 120px;
		padding-right: 20px;
	}
	.customLoading .filter-wrapper .span3{
		margin-left: 0;
	}
	.customLoading .filter-wrapper .span3 #fancytree-filter-clear-button{
		margin-top: 10px;
		margin-right: -15px;
	}
	.customLoading .span6 #users-grid-search-clear-button,.span6  #groups-grid-search-clear-button{
		position: absolute;
		right: 30px;
		top: 14px;

	}
	.customLoading .filter-wrapper .span6:last-child{
		text-align: right;
		padding-right: 5px;
	}
	.customLoading .filter-wrapper .span6 #users-search-input, #groups-search-input{
		width: 73%;
		padding-right: 40px;
	}
	@media (min-width: 980px) {
		.customLoading{
			width: 900px;
			margin-left: -450px;
		}
	}

	@media (min-width: 769px) and (max-width: 979px){
		.customLoading{
			width: 720px;
			margin-left: -360px;
		}
		.customLoading .labelBlock{
			width: 85%;
		}
		.customLoading .selection{
			width: 32%;
		}
		.customLoading .filter-wrapper .span5.legend{
			padding-left: 6px;
		}
	}
	@media (min-width: 1200px) {
		.customLoading .filter-wrapper .span3 #fancytree-filter-clear-button{
			margin-right: -20px;
		}
	}
</style>
<h1><?= Yii::t('standard', 'Filter') ?></h1>
<?php
echo '<span style="font-weight: bold;text-decoration: underline;font-size: 1.1em">'.Yii::app()->user->getFlash('error').'</span>';
$model = LearningReportAccess::model()->findByAttributes(array(
	'id_report' => intval($_GET['id'])
));
if (!$model) {
	$model = new LearningReportAccess();
	$model->visibility_type = LearningReportAccess::TYPE_PRIVATE;
}
?>
<div style="margin-bottom: 20px">
	<?=Yii::t('report', 'Define visibility for this report:')?>
</div>
<div class="grey-wrapper">
	<div class="selection">
		<?= CHtml::radioButton('visibility_type', $model->visibility_type == LearningReportAccess::TYPE_PUBLIC, array(
			'class' => 'course-assign-mode',
			'value' => LearningReportAccess::TYPE_PUBLIC,
			'id' => LearningReportAccess::TYPE_PUBLIC,
		)) ?>
		<div class="labelBlock">
			<?= CHtml::label(Yii::t('report', '_TAB_REP_PUBLIC') . ': ' . Yii::t('standard', 'All users, groups and branches'), LearningReportAccess::TYPE_PUBLIC) ?>
			<br/><span
				class="comment"><?= Yii::t('report', 'Report will be visible to superadmins, the owner and power users') ?></span>
		</div>
	</div>
	<div class="selection">
		<?= CHtml::radioButton('visibility_type', $model->visibility_type == LearningReportAccess::TYPE_PRIVATE, array(
			'class' => 'course-assign-mode',
			'value' => LearningReportAccess::TYPE_PRIVATE,
			'id' => LearningReportAccess::TYPE_PRIVATE,
		)) ?>
		<div class="labelBlock">
			<?= CHtml::label(Yii::t('calendar', '_PRIVATE'), LearningReportAccess::TYPE_PRIVATE) ?>
			<br/><span
				class="comment"><?= Yii::t('report', 'Report will be visible to superadmins and the owner') ?></span>
		</div>
	</div>
	<div class="selection">
		<?= CHtml::radioButton('visibility_type', $model->visibility_type == LearningReportAccess::TYPE_SELECTION, array(
			'class' => 'course-assign-mode',
			'value' => LearningReportAccess::TYPE_SELECTION,
			'id' => LearningReportAccess::TYPE_SELECTION,
		)) ?>
		<div class="labelBlock">
			<?= CHtml::label(Yii::t('dashboard', 'Select users, groups and branches'), LearningReportAccess::TYPE_SELECTION) ?>
			<br/><span class="comment"></span>
		</div>
	</div>
	<div class="clearfix"></div>
</div>

<script>
	$('.grey-wrapper input').styler();
	function changeState(){
		if($(this).prop('id') == '<?=LearningReportAccess::TYPE_SELECTION?>'){
			$('.tabbable.main-selector-area').show();
		}else{
			$('.tabbable.main-selector-area').hide();
		}
	}

	$(function () {
		$('.selection>input[type="radio"]').change(changeState);
		$('.selection>input[type="radio"]:checked').trigger('change')
	})
</script>
