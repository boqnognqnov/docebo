<?php
	Yii::app()->clientScript->scriptMap['jquery.js'] = false;
	Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
	Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
?>

<div class="user-report-actions">
	<div class="clearfix">
		<span><?php echo Yii::t('report', '_SELECT_USER'); ?></span>
	  <?php echo CHtml::textField('CoreUser[search_input]', '', array(
			'id' => 'advanced-search-user-report',
			'class' => 'typeahead ajaxGenerate',
			'autocomplete' => 'off',
			'data-url' => Docebo::createAppUrl('admin:userManagement/usersAutocomplete'),
			'data-type' => 'core',
            'data-loader-show' => 'true',
			'placeholder' => Yii::t('report', 'Type user here'),
			'data-source-desc' => 'true',
		)); ?>

		<?php $this->renderPartial('//common/_modal', array(
			'config' => array(
				'class' => 'new-user-summary',
				'modalTitle' => Yii::t('report', 'User personal summary'),
				'linkTitle' => Yii::t('certificate', '_GENERATE'),
				'url' => 'reportManagement/createUserReport',
// 				'buttons' => array(
// 					array(
// 						'type' => 'submit',
// 						'title' => Yii::t('standard', '_CONFIRM'),
// 					),
// 					array(
// 						'type' => 'cancel',
// 						'title' => Yii::t('standard', '_CANCEL'),
// 					),
// 				),
				'buttons' => array(),
				'beforeLoadingContent' => 'function() { if(generateRaportVerifyInitData(config)) {$(\'.modal.in\').modal(\'hide\'); return true;} else {return false;}}',
				'afterLoadingContent' => 'function() {applyTypeahead($(\'.modal.in .typeahead\'));applyDonut(\'.modal.in .donutContainer\'); applyLineCharts(\'.modal.in .lineChartContainer\');}',
				// 'afterSubmit' => 'updateGroupContent',
			),
		)); ?>

		<div class="report-popup-actions">
			<?php echo CHtml::link(Yii::t('report', 'Print') . '<span></span>', 'javascript:void(0);', array('class' => 'report-action-print')); ?>
			<?php echo CHtml::link(Yii::t('report', 'Download as PDF') . '<span></span>', Yii::app()->createAbsoluteUrl('reportManagement/createUserReport', array('id' => Yii::app()->user->getRelativeUsername($userModel->userid), 'createPdf' => true)), array('class' => 'report-action-pdf')); ?>
		</div>
	</div>
</div>

<div class="printable">
<?php echo CHtml::image(Yii::app()->theme->getLogoUrl()); ?>
<hr/>
<div class="user-report-summary clearfix">
	<div class="user-report-info clearfix">
		<div class="user-report-info-image">
			<?php echo CHtml::image($userModel->getAvatarImage(true), $userModel->firstname . " " . $userModel->lastname, array('width' => 150) ); ?>
		</div>
		<div class="user-report-info-data">
			<div class="fullname"><?php echo $userModel->firstname; ?> <?php echo $userModel->lastname; ?></div>
			<div class="username"><?php echo Yii::app()->user->getRelativeUsername($userModel->userid); ?></div>
			<div>
				<div class="label"><?php echo Yii::t('standard', '_LEVEL'); ?></div>
				<div class="data"><?php echo (!empty($userModel->adminLevel->groupid)? Yii::t('admin_directory', '_DIRECTORY_'.$userModel->adminLevel->groupid) :''); ?></div>
			</div>
			<div>
				<div class="label"><?php echo Yii::t('standard', '_EMAIL'); ?></div>
                <div class="data" title="<?=$userModel->email;?>"><?php echo Docebo::ellipsis($userModel->email, 26); ?></div>
			</div>
			<div>
				<div class="label"><?php echo Yii::t('standard', '_GROUPS'); ?></div>
				<div class="data">
					<?php echo implode(', ', $userGroups); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="user-report-stat clearfix">
		<div class="subscription-date">
			<?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/subscription-date.png');?>
				<div class="data"><?php echo $userModel->register_date; ?></div>
				<div class="label"><?php echo Yii::t('report', '_DATE_INSCR'); ?></div>
		</div>
		<div class="last-access">
			<?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/last-access-date.png');?>
				<div class="data"><?php echo (!empty($userModel->lastenter)?$userModel->lastenter:Yii::t('standard', '_NEVER')); ?></div>
				<div class="label"><?php echo Yii::t('standard', '_DATE_LAST_ACCESS'); ?></div>
		</div>
		<div class="total-time">
			<?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/total-time.png');?>
			<div class="data"><?php echo $totalTime; ?></div>
			<div class="label"><?php echo Yii::t('standard', '_TOTAL_TIME'); ?></div>
		</div>
		<div class="active-courses">
			<?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/report/active-courses.png');?>
				<div class="data"><?php echo count($userModel->learningCourseusers); ?></div>
				<div class="label"><?php echo Yii::t('dashboard', '_ACTIVE_COURSE'); ?></div>
		</div>
	</div>
</div>
	<div class="user-additional-fields clearfix">
		<?php if (!empty($additionalFields)) {
			$i = 0;
			foreach ($additionalFields as $fieldName => $userValue) {
				$fieldClass = ($i % 2 == 0)? 'odd' : 'even';
				echo '<div class="field '.$fieldClass.' '. (($i <= 1) ? 'firstField' : '' ) .'"><div class="name">'.$fieldName.'</div><div class="value">'.$userValue.'</div></div>';
				$i++;
			}
		}	?>

	</div>

	<table class="report-courses-summary">
		<tbody>
			<tr>
				<td>
					<table class="report-courses-summary-legend">
						<tbody>
							<tr>
								<td colspan="2" class="report-info-title">
									<?php echo Yii::t('standard', '_PROGRESS'); ?>
								</td>
							</tr>
							<tr>
								<td class="td-middle">
									<div class="donut-wrapper">
										<?php echo $pieChart; ?>
									</div>
								</td>
								<td class="td-middle">
									<div class="summary-legend-wrapper">
										<?php echo $pieChartLegend; ?>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
				<td class="report-activities-summary">
					<table>
						<tbody>
							<tr>
								<td class="report-stat-title">
									<?php echo Yii::t('report', '_YEARLY_ACTIVITIES_SUMMARY'); ?>
								</td>
							</tr>
							<tr>
								<td>
									<div class="lineChart-wrapper">
										<?php echo $lineChart; ?>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>

<div class="user-report-courses" style="margin-bottom: 20px;">
	<div class="courses-title"><?php echo Yii::t('standard', '_COURSES'); ?></div>
	<div class="courses-grid-wrapper">
		<?php $this->widget('DoceboCGridView', array(
			'id' => 'course-management-grid'.$userModel->getPrimaryKey(),
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'dataProvider' => $courseUserModel->dataProvider(),
			'cssFile' => false,
			'template' => '<div class=scroll-content>{items}</div>{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'pager' => array(
				'class' => 'DoceboCLinkPager',
				'maxButtonCount' => 8,
			),
			'ajaxUpdate' => 'all-items',
			'beforeAjaxUpdate' => 'function(id, options) {
				options.type = "POST";
				if (options.data == undefined) {
					options.data = {
						contentType: "html",
					}
				}
			}',
            'afterAjaxUpdate' => 'function(id, options){$(\'#\' + id + \' .scroll-content\').jScrollPane()}',
			'loadIntoDialog' => true,
			'columns' => array(
				array(
					'name' => 'course.code',
					'header' => Yii::t('standard', '_COURSE_CODE'),
					// 'name' => $data->course->name,
				),
				array(
					'name' => 'course.name',
					// 'name' => $data->course->name,
				),
				array(
					'header' => Yii::t('profile', '_USERCOURSE_STATUS'),
					'name' => 'status',
					'value' => 'LearningCourseuser::getStatusLabel($data->status);'
				),
				array(
					'header' => Yii::t('standard', 'Enrolled'),
					'name' => 'date_inscr',
					'value' => '$data->date_inscr'
				),
				array(
					'header' => Yii::t('standard', '_DATE_FIRST_ACCESS'),
					'name' => 'date_first_access',
					'value' => '$data->date_first_access'
				),
				array(
					'header' => Yii::t('standard', 'Course completion'),
					'name' => 'date_complete',
					'value' => '$data->date_complete'
				),
				array(
					'header' => Yii::t('standard', '_CREDITS'),
					'name' => 'course.credits',
				),
				array(
					'header' => Yii::t('standard', '_TOTAL_TIME'),
					'value' => '$data->renderTimeInCourse();',
				),
				array(
					'name' => 'course.score',
					'header' => Yii::t('standard', '_SCORE'),
					'value' => 'LearningCourseuser::getLastScoreByUserAndCourse($data["idCourse"],$data["idUser"])',
				),


			),
		)); ?>
	</div>
</div>

<?php if(PluginManager::isPluginActive("ClassroomApp")): ?>
<div class="user-report-classroom-courses">
    <div class="courses-title"><?php echo Yii::t('classroom', 'Classrooms'); ?></div>
    <div class="courses-grid-wrapper">
        <?php $this->widget('DoceboCGridView', array(
            'id' => 'classroom-course-management-grid'.$userModel->getPrimaryKey(),
            'htmlOptions' => array('class' => 'grid-view clearfix'),
            'dataProvider' => LtCourseuserSession::model()->dataProviderClassroomCoursesActivity($userModel->getPrimaryKey()),
            'cssFile' => false,
            'template' => '<div class=scroll-content>{items}</div>{summary}{pager}',
            'summaryText' => Yii::t('standard', '_TOTAL'),
					'pager' => array(
						'class' => 'DoceboCLinkPager',
						'maxButtonCount' => 8,
					),
            'ajaxUpdate' => 'all-items',
            'beforeAjaxUpdate' => 'function(id, options) {
                    options.type = "POST";
                    if (options.data == undefined) {
                        options.data = {
                            contentType: "html",
                        }
                    }
                }',
			'loadIntoDialog' => true,
            'columns' => array(
                array(
                    'name' => 'session.name',
                    'header' => Yii::t('classroom', 'Course and session information'),
                    'value' => '$data->session->course->name . " - " .$data->session->name'
                ),
                array(
                    'header' => Yii::t('standard', '_START_DATE'),
                    'name' => 'date_subscribed',
                    'value' => '$data->date_subscribed'
                ),
                /*array(
                    'header' => Yii::t('standard', '_CREDITS'),
                    'name' => 'course.credits',
                ),*/
                array(
                    'header' => Yii::t('standard', '_ATTENDANCE'),
                    'name' => 'session.attendance',
                    'value' => '$data->session->renderAttendance($data->id_user)',
                ),
                array(
                    'header' => Yii::t('standard', '_STATUS'),
                    'name' => 'status',
                    'value' => 'Yii::t("standard", LtCourseuserSession::getStatusLangLabel($data->status))'
                ),
                array(
                    'header' => Yii::t('standard', '_SCORE'),
                    'value' => 'intval($data->evaluation_score) . "/" . intval($data->session->score_base)'
                ),
            ),
        )); ?>
    </div>
</div>
<?php endif; ?>
<div class="user-report-classroom-courses">
	<div class="courses-title"><?php echo Yii::t('webinar', 'Webinars'); ?></div>
	<div class="courses-grid-wrapper">
		<?php $this->widget('DoceboCGridView', array(
			'id' => 'webinar-course-management-grid'.$userModel->getPrimaryKey(),
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'dataProvider' => WebinarSessionUser::model()->dataProviderClassroomCoursesActivity($userModel->getPrimaryKey()),
			'cssFile' => false,
			'template' => '<div class=scroll-content>{items}</div>{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'pager' => array(
				'class' => 'DoceboCLinkPager',
				'maxButtonCount' => 8,
			),
			'ajaxUpdate' => 'all-items',
			'beforeAjaxUpdate' => 'function(id, options) {
                    options.type = "POST";
                    if (options.data == undefined) {
                        options.data = {
                            contentType: "html",
                        }
                    }
                }',
			'loadIntoDialog' => true,
			'columns' => array(
				array(
					'name' => 'sessionModel.name',
					'header' => Yii::t('classroom', 'Course and session information'),
					'value' => '$data->sessionModel->course->name . " - " .$data->sessionModel->name'
				),
				array(
					'header' => Yii::t('standard', '_START_DATE'),
					'name' => 'date_subscribed',
					'value' => '$data->date_subscribed'
				),
				array(
					'header' => Yii::t('standard', '_STATUS'),
					'name' => 'status',
					'value' => 'Yii::t("standard", WebinarSessionUser::getStatusLangLabel($data->status))'
				),
				array(
					'header' => Yii::t('standard', '_SCORE'),
					'value' => 'intval($data->evaluation_score) . "/" . intval($data->sessionModel->score_base)'
				),
			),
		)); ?>
	</div>
</div>
</div> <!-- End of .printable -->

<script type="text/javascript">
	$(document).ready(function() {
		$('.report-action-print').click(function(){
			window.print();
		});
		$('.scroll-content').jScrollPane({
            autoReinitialise: true
		});
		replacePlaceholder();
	});
</script>
