<?php
	/** @var $fieldModel LearningEnrollmentFieldIframe */
	$settings  = $fieldModel->getSettingsArray();
	$iframerId = "enrollment-field-iframe-". $fieldModel->id;
?>

<iframe 
	id="<?= $iframerId ?>" 
	src="<?= $fieldModel->getIframeUrl() ?>"
	width="100%" 
	height="<?= $settings["height"]?>" 
	style="padding:0; margin:0; border:0;"
	
	>
</iframe>

<?php echo CHtml::hiddenField("field-".$fieldModel->id, "", array()); ?>

<script type="text/javascript">

	$(function() {
		$("#<?= $iframerId ?>").enrollment_field_iframe({
			field_id	: <?= json_encode((int) $fieldModel->id) ?>,
			enrollments : <?= json_encode($enrollments) ?>
		});
	});

</script>