<h1><?=Yii::t('standard', 'Settings')?></h1>


<div class="course-desc-widget-settings">
    
    <?php if($error !== null): ?>
    <div class="course-desc-widget-info">
        <div class="alert-danger alert"><?=$error ?></div>
    </div>
    <?php endif; ?>
    
    <?php
    
    echo CHtml::beginForm('', 'post', array(
        'class' => 'ajax '
    ));
    ?>
    <div class="row-fluid">
        <div class="control-group">
            <div class="checkbox">
                <?php                
                    echo CHtml::checkBox('show_desc', ($showDesc) ? true : false, array(
                        'name' => 'show-description',                        
                    ));
                    echo CHtml::label(Yii::t('course', 'Show course description'), 'show_desc', array(
                        'style' => 'display: inline-block'
                    ));
                ?>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="control-group">
            <div class="control-label">
                <?=CHtml::label(Yii::t('course', 'Show the following additional fields'), $for)?>
            </div>
            <div class="radio">
                <select name="additional_fields" multiple="multiple" id="fcbk-additional-fields">
                    <?php                   
                        if(!empty($additionalFields)){
                            foreach ($additionalFields as $field){
                                $fieldModel = LearningCourseField::model()->findByPk($field);
                                if(($fieldModel instanceof LearningCourseField) === false){
                                    continue;
                                }
                                echo "<option class='selected' value='$field'>" . $fieldModel->getTranslation($field) . "</option>";
                            }
                        }                    
                    ?>
                </select>
            </div>
        </div>
    </div>
    
    <div class="form-actions">
        <?php
        echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
            'class' => 'btn btn-docebo green big',
            'name' => 'next_button',
        ));
        echo CHtml::button(Yii::t('standard', '_CANCEL'), array(
            'class' => 'btn btn-docebo black big close-dialog',
            'name' => 'cancel_step'
        ));
        ?>
    </div>
    
    <?= CHtml::endForm()?>
</div>

<script type="text/javascript">
    $('input[type=checkbox]').styler();
    
    $('select#fcbk-additional-fields').fcbkcomplete({
        width: "98.9%",
        json_url: "<?= Docebo::createAdminUrl('additionalCourseFields/fcbkAutocomplete', array(
            'course_id' => $course_id
        ))?>",
        filter_selected: true,
        cache: true,
        complete_text: '',
        placeholder: '<?=Yii::t('standard', 'Type here...')?>'
    });
</script>