<?php /* @var $model LearningEnrollmentFieldIframe */?>
<?php $settings     = $model->getSettingsArray(); ?>
<?php $clientsList  = $model->getOuathClients(); ?>
<style>
<!--
.course-fields-settings {
    margin-top: 10px;
    margin-bottom: 10px;
}

.course-fields-settings > span {
    margin-right: 30px;
    width: 300px;
    display: inline-block;
}

-->
</style>

<div class="course-fields-settings">
	<span><?= Yii::t('thomsonreuters', 'Iframe URL')?></span>
	<input type="text" name="LearningEnrollmentFieldIframe[settings][url]" value="<?= $settings['url'] ?>" />
</div>
<div class="course-fields-settings">
	<span><?= Yii::t('thomsonreuters', 'Iframe height (px)')?></span>
	<input type="text" name="LearningEnrollmentFieldIframe[settings][height]" value="<?= $settings['height'] ?>" />
</div>
<div class="course-fields-settings">
	<span><?= Yii::t('thomsonreuters', 'Salt Secret') ?></span>
	<input type="password" name="LearningEnrollmentFieldIframe[settings][hash_secret]" value="<?= $settings['hash_secret'] ?>" />
</div>
<div class="course-fields-settings">
	<span><?= Yii::t('thomsonreuters', 'Repeat Salt Secret') ?></span>
	<input type="password" name="LearningEnrollmentFieldIframe[settings][hash_secret_repeat]" value="<?= $settings['hash_secret_repeat'] ?>" />
</div>

<div class="course-fields-settings">
	<span><?= Yii::t('thomsonreuters', 'OAuth Client') ?></span>
	<?= CHtml::dropDownList("LearningEnrollmentFieldIframe[settings][client_id]", $settings['client_id'], $clientsList, array("prompt" => ""))	?>
</div>
