<?php $this->breadcrumbs = array(
    Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
    Yii::t('userlimit', 'Course management') => array('courseManagement/index'),
    Yii::t('menu', '_ENROLLMENT_FIELD_MANAGER'),
); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'additional-fields-form',
    'htmlOptions' => array('class' => 'ajax-grid-form', 'data-grid' => '#fields-grid'),
)); ?>
<div class="main-section additional-fields-wrapper">
    <h3 class="title-bold back-button">
        <a href="<?php echo Yii::app()->createUrl('courseManagement/index'); ?>">
            <?php echo Yii::t('standard', '_BACK'); ?>
        </a>
        <span><?php echo Yii::t('menu', '_ENROLLMENT_FIELD_MANAGER'); ?></span>
    </h3>
    <div class="filters-wrapper">
        <div class="filters clearfix">
            <div class="input-wrapper">
                <input data-url="<?= Docebo::createAppUrl('admin:additionalEnrollmentFields/fieldsAutocomplete') ?>" autocomplete="off" id="search-additional-fields-grid" class="typeahead" type="text" name="LearningCourseFieldTranslation[translation]" placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"/>
                <span class="search-icon"></span>
            </div>
        </div>
    </div>
</div>

    <div class="add-field-section clearfix">
        <label for="field-type-select"><?php echo Yii::t('standard', '_ADD'); ?></label>
        <select name="filter-type" id="field-type-select">
            <?php foreach ($fieldTypes as $val => $text): ?>
                <option value="<?php echo $val; ?>" <?= $val == 3 ? 'selected' : '' ?>><?= $text; ?></option>
            <?php endforeach; ?>
        </select>
        <input type="button" value="<?php echo Yii::t('standard', '_CREATE'); ?>" class="create-additional-field">
    </div>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>



<div class="bottom-section clearfix"></div>


<?php $buttons[] = array(
    'class' => 'CButtonColumn',
    'buttons' => array(
        'order' => array(
            'url' => '"#" . $data->id',
            'label' => Yii::t('standard', '_MOVE'),
            'options' => array('class' => 'order-field move'),
        ),
        'edit' => array(
            'url' => '$data->type != "2" ? Yii::app()->createUrl("' . $this->id . '/editField", array("id" => $data->id)) : Yii::app()->createUrl("' . $this->id . '/editDropdown", array("id" => $data->id))',
            'label' => Yii::t('standard', '_MOD'),
            'click' => 'js:function() {
						$(".modal.new-field").remove();

						$.get($(this).attr("href"), {}, function(data) {
							$("body").append(data.html);
							$("#field-modal").modal();

							window.setTimeout(function () {
								var assignedCount = 0;
								$(".modal.in .orgChart_translationsList .orgChart_translation").each(function (index) {
								    var explode = $(this).attr("id").split("_");
								    if(explode[explode.length - 1] == "field")
									var lang = explode[explode.length - 2];
								    else
									var lang = explode[explode.length - 1];
								    var lang_option = $("#LearningCourseFieldTranslation_lang_code option[value="+lang+"]");
								    if ($(this).val() != "") {
								        assignedCount++;
								        var html = lang_option.html();
								        if (html && html.indexOf("* ") < 0)
								        	lang_option.html("* "+lang_option.html());
								    }
								    else {
								    	var html = lang_option.html();
								    	if (html && html.indexOf("* ") === 0) {
								    		lang_option.html(lang_option.html().replace("* ", ""));
								    	}
								    }
								});
								$("#orgChart_assignedCount").html("' . Yii::t('standard', 'Filled languages') . ' <span>" + assignedCount + "</span>");
							}, 300)
						});

						return false;
				}',
            'options' => array('class' => 'edit-field'),
        ),
    ),
    'template' => '{order}{edit}',
); ?>

<div id="grid-wrapper">
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'fields-grid',
        'htmlOptions' => array('class' => 'grid-view clearfix additional-enrollment-fields'),
        'dataProvider' => $coreField->getAdditionalEnrollmentFieldsDataProvider(),
        'rowHtmlOptionsExpression' => 'array("data-fid" => $data->id);',
        'itemsCssClass' => 'items non-quartz-items',
        'enablePagination'  => 'true',
        'afterAjaxUpdate' => 'function(id, data) {
					$(\'a[rel="tooltip"]\').tooltip();
					$(document).controls();
					initCourseFieldsSortable();
					$("#course-management-grid input").styler();
				}',
        'columns' => array_merge(
            array(
                array(
                    'header' => Yii::t('standard', '_FIELD_NAME'),
                    'name' => 'translation',
                    'value' => '$data->translation',
					'htmlOptions' => array('class' => 'additional-field-name')
                    //'value' => array($this, 'gridRenderTranslation')
                ),
                array(
                    'header' => Yii::t('field', '_FIELD_TYPE'),
                    'name' => 'type',
                    'value' => '$data->getCategoryName();',
                ),
            ), $buttons, array(
            array(
                'name' => '',
                'type' => 'raw',
                'value' => 'CHtml::link("", "index.php?r=additionalEnrollmentFields/deleteField&id=$data->id", array(
						"class" => "CButtonColumn ajax open-dialog pull-right delete-action",
						"data-dialog-class" => "delete-field",
						"data-dialog-id"    => "delete-custom-field-dialog",
					));',
                'htmlOptions' => array('class' => 'CButtonColumn button-column-single')
            )
        )),
        'template' => '{items}{summary}{pager}',
        'pager' => array(
            'class' => 'DoceboCLinkPager',
            'maxButtonCount' => 8,
        ),
        'summaryText' => Yii::t('standard', '_TOTAL'),
    )); ?>
</div>

<?php $this->endWidget(); ?>



<script type="text/javascript">
    (function ($) {
        $(function () {
            $('input,select').styler();
            resetSearchPlaceholder();
            replacePlaceholder();
            initCourseFieldsSortable();

            $('.create-additional-field').click(function () {
                $(".modal.new-field").remove();
                var fieldType = $('#field-type-select').val();
                $.ajax({
                    url: yii.urls.base + '/?r=additionalEnrollmentFields/createFieldModal',
                    data: { type : fieldType },
                    cache: false,
                    success: function (data) {
                        $('body').append(data.html);
                        $('#field-modal').modal();
                    }
                });
            });
        });
    })(jQuery);

    $(document).on("dialog2.closed", "#delete-custom-field-dialog",
        function(){
            $.fn.yiiGridView.update("fields-grid", {
                data: $("#ajax-grid-form").serialize()
            });
                $('input,select').styler();
        });

    function resetSearchPlaceholder() {
        var $search = $('#search-additional-fields-grid');
        if($search.val() == "<?= Yii::t('standard', '_SEARCH'); ?>")
            $('#search-additional-fields-grid').val('');
    }
    setTimeout(checkLanguage, 1000);
    function checkLanguage()
    {
        if($(".modal.in .orgChart_translationsList .orgChart_translation"))
        {
            $(".modal.in .orgChart_translationsList .orgChart_translation").each(function (index) {
                var explode = $(this).attr("name").split("][");
                if(explode[explode.length - 1] == "field]")
                    var lang = explode[explode.length - 2];
                else
                    var lang = explode[explode.length - 1].replace(']', '');
                if (!lang) return;

                var lang_option = $("#LearningCourseFieldTranslation_lang_code option[value="+lang+"]");
               
                if ($(this).val() != "") {
                    var html = lang_option.html();
                    if (html && html.indexOf("* ") < 0) {
                        lang_option.html("* "+lang_option.html());
                    }
                } else {
                    var html = lang_option.html();
                    if(html && html.indexOf("* ") === 0) {
                        lang_option.html(lang_option.html().replace("* ", ""));
                    }
                }
            });
        }
        setTimeout(checkLanguage, 1000);
    }

</script>
