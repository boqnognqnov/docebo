<div class="form dropdown enrollmentAdditionalFieldForm">
	<?php 
        $defaultLanguage = Yii::app()->getLanguage();
        $fieldClass =  get_class($model);
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'add-field-form',
            'action' => $model->isNewRecord ? array('additionalEnrollmentFields/createDropdown') : array('additionalEnrollmentFields/editDropdown', 'id' => $model->id),
        )); 
    ?>


<div class="dropdown_form_title"><?php echo Yii::t('branding', '_PAGE_LANGUAGE_LABEL'); ?></div>
<div class="clearfix">
    <div class="languagesList">

      <?php $translations = json_decode($model->translation);?>
            <select class="additionalFieldTypeDropdown" name="lang" id="LearningCourseFieldTranslation_lang_code">
            <?php foreach(CoreLangLanguage::getActiveLanguagesByBrowsercode() as $key => $value) {
                $has_translation = isset(json_decode($model->translation)->$key) ? '*' : '';
                ?>
                <option value="<?php echo $key;?>" <?php echo Yii::app()->getLanguage() === $key ? 'selected="selected"' : ''?> >
                     <?php echo $value . $has_translation;?>  <?php echo Yii::app()->getLanguage() === $key ? " (".Yii::t('standard', 'Default language').")" : ''?>
                </option>
                <?php
            }
            ?>
            </select>
            <?php // echo $form->dropDownList($modelTranslation, 'lang_code', $activeLanguagesList); ?>
            <?php //echo $form->error($modelTranslation, 'lang_code'); ?>
    </div>

    <div class="orgChart_translationsList">
        <?php

        $currentLanguage = false;

        if (!empty($activeLanguagesList)) {

            $currentLanguage = Yii::app()->getLanguage();

            if ( !empty($model->id))
                $preparedData = LearningEnrollmentFieldDropdown::getDropdownData($model->id);
            // var_dump($preparedData);die();
            foreach (CoreLangLanguage::getActiveLanguagesByBrowsercode() as $key => $lang) {

                  if ($key == Yii::app()->getLanguage()) {
                        $class = 'show';
                    } else {
                        $class = 'hide';
                    }
                ?>
                <div id="CoreOrgChart_<?= $key ?>" class="languagesName <?php echo $key . ' ' . $class; ?>">
                    <div class="field-name" style="position: relative;">
                        <div class="orgChart_form_title"><?php echo Yii::t('standard', '_FIELD_NAME', array($lang)); ?></div>
                        <?php echo $form->textField($model, 'translation[' . $key . '][field]', array('class' => 'orgChart_translation'.($defaultLanguage == $key ? ' default_lang' : ''), 'maxlength' => '255', 'value' => (!empty($translationsList[$key]['field']) ? $translationsList[$key]['field'] : !empty($preparedData[$key]['options']) ? $preparedData[$key]['label'] : ''))); ?>
                    </div>
                    <div id="field-options-<?= $key ?>" class="field-options" data-lang="<?php echo $key; ?>" style="position: relative;">
                        <div class="orgChart_form_title"><?php echo Yii::t('test', '_TEST_QUEST_ELEM') , " ($lang)"; ?></div>
                        <div id="field-options-inputs-<?= $key ?>">
                            <!-- here the fields list to be printed by JS -->
                            <?php
                                $i=0;
                            if ( !empty($model->id) && is_array($preparedData[$key]['options'])) {                               
                                    foreach ( $preparedData[$key]['options'] as $optionKey => $optionItem) {
                                        echo '<input
                                       type="text"
                                       placeholder=""
                                       class="dropdownOption"
                                       value="'.$optionItem.'"
                                       data-id-option="'.$optionKey.'"
                                       id="translation_'.$key.'_options_translation_'.$i.'" name="LearningCourseFieldTranslation[translation]['.$key.'][options][translation][]"
                                            >';
                                        $i++;
                                    }
                                } else { 
                                   
                                    }
                                 ?>
                        </div>
                    </div>
                </div>
                <?php $i++; } ?>
        <?php } ?>
    </div>
</div>

<?php // echo $model->getSubclassedInstance()->renderFieldSettings(); ?>

<?php echo $this->renderPartial('_dropdownPreview'); ?>
<div class="additional-checkboxes clearfix" style="position: static;">
<?php
	$categorySelected = 0;
	if($model->courses){
		$catArray = json_decode($model->courses);
		if(is_array($catArray) && $catArray){
			$categorySelected = $catArray[0];
		}
	}
	$model->course = $categorySelected ? 1 : 0;
?>
   <div class="item clearfix">
        <?php echo $form->radioButtonList($model, 'course', array('All courses','Category of courses')); ?>
    </div>
    
    <div class="orgChart_categoriesList" style="<?php echo !$categorySelected ? 'display: none' : ''?>">
         <select name="course_categories">
        <?php foreach ($categoriesDropdown as $key => $category) { ?>
            <option value="<?php echo $key; ?>" <?php echo $categorySelected && $categorySelected == $key ? 'selected="selected"' : ''?>><?php echo $category; ?></option>
        <?php }?>
        </select>
    </div>

     <div class="item clearfix" style="margin-top: 20px;margin-left: 6px;">
        <?php echo $form->checkBox($model, 'mandatory'); ?>
        <?php echo $form->labelEx($model, 'mandatory', array(
            'label' => Yii::t('standard', 'Mandatory'),
            'style' => 'width: 100px'
        )); ?>

        <?php echo $form->checkBox($model, 'visible_to_user'); ?>
        <?php echo $form->labelEx($model, 'visible_to_user', array(
            'label' => Yii::t('standard', 'Visible to the users'),
            'style' => 'width: 280px'
        )); ?>
    </div>
<?php echo $form->hiddenField($model, 'type', array('value' => $model->type)); ?>
<?php $this->endWidget(); ?>
</div>
<div class="saving-dropdown-field" style="display:none">
    <img src="<?= Yii::app()->theme->baseUrl.'/images/ajax-loader10.gif' ?>" />
</div>


</div>

<script type="text/javascript">
    //var DD;
    (function ($) {
        
        $('input[name="<?= $fieldClass ?>[course]"]').change(function() {
            $('.orgChart_categoriesList')[parseInt($(this).val()) === 1 ? 'show' : 'hide' ]();
        });

        <?php
      $jsObject = array();
        foreach (CoreLangLanguage::getActiveLanguagesByBrowsercode() as $key => $lang) {
            $jsObject[$key] = array(
                'lang_name' => $lang,
                'translations' => array(),
                'idSon' => array(),
            );
$translation = array();
            $translation[$model->id];

        }
        echo 'var translations = '.CJavaScript::encode($jsObject).';';
        echo "\n";

     ?>

        DD = new dropdownFieldManager({
            <?php if ( !empty($model->id)) {

            ?>
            idField: <? echo $model->id ?>,
            <?php } else { ?>
            idField: '',
            <?php } ?>
            currentActiveLanguage: <?= CJavaScript::encode($currentLanguage) ?>,
            translations: translations,
            filledLanguageTranslation: <?= CJavaScript::encode(Yii::t('standard', 'Filled languages')) ?>,
            newOptionPlaceholder: <?= CJSON::encode(Yii::t('field', 'Type here to add an option...')) ?>
        }); 

        // $('input[name="LearningEnrollmentField[course]"]').change(function() {
        //     $('.orgChart_categoriesList')[parseInt($(this).val()) === 1 ? 'show' : 'hide' ]();
        // });

        /*
         On modal hide, remove the modal content in case there are js events or something else
         that may interrupt the work of the other parts of the code
         */
        $('#field-modal').on('hidden', function () {
            $('#field-modal').remove();
            /*
             When closing the dialog destroy the DD object
             because the object is needed only when this view is shown.
             */
            DD = null;
        });
    })(jQuery);
</script>