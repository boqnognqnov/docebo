<div class="form enrollmentAdditionalFieldForm">
    <?php $defaultLanguage = CoreLangLanguage::getDefaultLanguage(); ?>
    <?php $fieldClass =  get_class($modelField); ?>
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'add-field-form',
    )); ?>
    <div class="dropdown_form_title"><?php echo Yii::t('branding', '_PAGE_LANGUAGE_LABEL'); ?></div>
    <div class="clearfix">
        <div class="languagesList">
        <?php $translations = json_decode($modelField->translation);?>
            <select name="lang">
            <?php foreach(CoreLangLanguage::getActiveLanguagesByBrowsercode() as $key => $value) {
                $has_translation = isset(json_decode($modelField->translation)->$key) ? '' : '';
                ?>
                <option value="<?php echo $key;?>" <?php echo Yii::app()->getLanguage() === $key ? 'selected="selected"' : ''?> >
                    <?php echo $value . $has_translation;?>  <?php echo Yii::app()->getLanguage() === $key ? " (".Yii::t('standard', 'Default language').")" : ''?>
                        
                </option>
                <?php
            }
            ?>
            </select>
            <?php // echo $form->dropDownList($modelTranslation, 'lang_code', $activeLanguagesList); ?>
            <?php echo $form->error($modelField, 'lang_code'); ?>
        </div>

        <div class="orgChart_translationsList">
            <?php if (!empty($activeLanguagesList)) {
                // reset($activeLanguagesList);
                $first_key = key(CoreLangLanguage::getActiveLanguagesByBrowsercode());
                
                ?>
                <?php 
                    foreach (CoreLangLanguage::getActiveLanguagesByBrowsercode() as $key => $lang) {
                        
                    if ($key == Yii::app()->getLanguage()) {
                        $class = 'show';
                    } else {
                        $class = 'hide';
                    }
                    ?>
                    <div id="NodeTranslation_<?php echo $key; ?>" class="languagesName <?php echo $class; ?>">
                        <div class="orgChart_form_title"><?php echo Yii::t('standard', '_FIELD_NAME', array($lang)); ?></div>

                        <input type="text" class="<?php echo $key == Yii::app()->getLanguage() ? 'default_lang' : ''?>" name="translation[<?php echo $key ;?>]" value="<?php echo $translations->$key ?>" />
                      
                        <?php  echo $form->error($modelField, 'translation[' . $key . ']'); ?>
                    </div>
                <?php } ?>
                <?php  echo $form->error($modelField, 'translation'); ?>
            <?php } ?>
        </div>
    </div>

    <div class="orgChart_languages">
        <div><?php echo Yii::t('admin_lang', '_LANG_ALL').': <span>' . count($activeLanguagesList).'</span>'; ?></div>
        <div id="orgChart_assignedCount"><?php echo Yii::t('standard', 'Filled languages').' <span>'.((!empty($translationsList))?count(array_filter($translationsList)):0).'</span>'; ?></div>
    </div>


    <?php 
        echo $modelField->renderFieldSettings();
    ?>

   <div class="additional-checkboxes clearfix" style="position: static;">
   <?php $modelField->course = $modelField->courses[0] ? 1 : 0;?>
    <div class="item clearfix">
        <?php echo $form->radioButtonList($modelField, 'course', array('All courses','Category of courses')); ?>
    </div>

    <div class="orgChart_categoriesList" style="<?php echo !$modelField->courses[0] ? 'display: none' : ''?>">
		<?php
			$categorySelected = 0;
			if($modelField->courses){
				$catArray = json_decode($modelField->courses);
				if(is_array($catArray) && $catArray){
					$categorySelected = $catArray[0];
				}
			}
		?>
        <select name="course_categories">
        <?php 
			foreach ($categoriesDropdown as $key => $category) { ?>
            <option value="<?php echo $key; ?>" <?php echo ($categorySelected && (int) $categorySelected == (int) $key) ? 'selected="selected"' : ''?>><?php echo $category; ?></option>
        <?php }?>
        </select>
    </div>

     <div class="item clearfix" style="margin-top: 20px;margin-left: 6px;">
        <?php echo $form->checkBox($modelField, 'mandatory'); ?>
        <?php
		 echo $form->labelEx($modelField, 'mandatory', array('label' => Yii::t('standard', 'Mandatory'),'style' => 'width: 100px')); 
		?>

        <?php echo $form->checkBox($modelField, 'visible_to_user'); ?>
        <?php
			 echo $form->labelEx($modelField, 'visible_to_user', array('label' => Yii::t('standard', 'Visible to the users'),'style' => 'width: 280px')); 
		?>
    </div>
</div>

    <?php
		echo $form->hiddenField($modelField, 'type', array('value' => $modelField->type));
	?>

	
    <?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
    (function ($) {
        $(function () {
            $('input').styler();

            $('input[name="<?= $fieldClass ?>[course]"]').change(function() {
                $('.orgChart_categoriesList')[parseInt($(this).val()) === 1 ? 'show' : 'hide' ]();
            });
            $('select[name="lang"]').change(function() {
                $('.languagesName').hide();
                $('input[name="translation['+$(this).val()+']"]').closest('.languagesName').show();
            });

			// $("#temp_category_id").on("change", function(e, data){
			// 	$('#courses').val($(this).val());
			// });
            /*
             On modal hide, remove the modal content in case there are js events or something else
             that may interrupt the work of the other parts of the code.
             */
            $('#field-modal').on('hidden', function () {
                $('#field-modal').remove();
            });
        });
    })(jQuery);
</script>

