<?php
$userIsSubscribed = LearningCourseuser::isSubscribed(Yii::app()->user->id, $model->idCourse);
if(!$userIsSubscribed) {
    $this->breadcrumbs[Yii::t('menu', 'Admin')] = Docebo::createAppUrl('admin:dashboard/index');
    $this->breadcrumbs[Yii::t('standard', '_COURSES')] = Docebo::createAppUrl('admin:courseManagement/index');
} else {
    $this->breadcrumbs = array(
        Yii::t('menu_over', '_MYCOURSES') => Docebo::createLmsUrl('site/index'),
    );
}

$command = Yii::app()->db->createCommand();
$command->select = 'name';
$command->from = LearningCourse::model()->tableName();
$command->where = "idCourse = :id";
$command->params = array(
    ':id' => $model->idCourse
);
$cmdCourse = $command->queryRow();
$this->breadcrumbs[Docebo::ellipsis($cmdCourse['name'], 60, '...')] = Docebo::createLmsUrl('player', array('course_id' => $model->idCourse));
$this->breadcrumbs[] = Yii::t('standard', '_WAITING_USERS');
?>


<?php
	$allowApproval = true;
	if (Yii::app()->user->getIsAdmin()) { 
		$allowApproval = CoreUser::adminRuleEnabled(Yii::app()->user->id, 'direct_course_subscribe');
	}
		 
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'course-waitingenrollment-form',
	'htmlOptions' => array(
		'class' => 'ajax-grid-form',
		'data-grid' => '#course-waitingenrollment-grid',
		'itemValue' => '$data->idUser',
	),
)); ?>

<div class="main-section waiting-users">
	<h3 class="title-bold"><?php echo Yii::t('course', '_ALLOW_OVERBOOKING'); ?></h3>

	<div class="filters-wrapper">
		<table class="filters">
			<tbody>
			<tr>
				<td class="table-padding">
					<table class="table-border">
						<tr>
							<td class="group select-enrollment-type">
								<table>
									<tr>
										<td><?php echo Yii::t('standard', '_SHOW'); ?></td>
										<td>
											<table class="overbooked-users">
												<tr>
													<td>
														<?php echo $form->radioButtonList($model, 'overbooked', array(1 => Yii::t('admin', 'Overbooked'), 2 => Yii::t('admin', 'Not overbooked'), 3 => Yii::t('standard', '_ALL')), array('separator' => '')); ?>
													</td>
												</tr>
											</table>
										</td>

									</tr>
								</table>
							</td>
							<td class="group">
								<table>
									<tr>
										<td>
											<div class="input-wrapper">
												<input data-url="<?= Docebo::createAppUrl('admin:userManagement/usersAutocomplete') ?>" data-type="core"
														data-source-desc="true"
														data-course="<?php echo $model->idCourse; ?>" data-waiting="1"
														class="typeahead" id="advanced-search-user" autocomplete="off"
														type="text" name="CoreUser[search_input]"
														placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"/>
												<span class="search-icon"></span>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
</div>
<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<div class="selections clearfix" data-grid="course-waitingenrollment-grid">
	<?php $this->widget('ext.local.widgets.GridViewSelectAll', array(
		'gridId' => 'course-waitingenrollment-grid',
		'class' => 'left-selections clearfix',
		'dataProvider' => $model->dataProvider(),
		'itemValue' => 'idUser',
	)); ?>

	<?php

	// IF user is PU show dropdown only if he has subscribe permission
	if(Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess("enrollment/update")) {
	?>
	<div class="right-selections clearfix">
		<select name="massive_action" id="waitingEnrollments-massive-action">
			<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
			<?php if ($allowApproval) : ?>
				<option value="confirm"><?php echo Yii::t('standard', '_APPROVE'); ?></option>
			<?php endif; ?>
			<option value="cancel"><?php echo Yii::t('standard', '_DENY'); ?></option>
		</select>
	</div>
	<?php } ?>
	
</div>

<div id="grid-wrapper">

	<?php 
	
		if ($allowApproval && Yii::app()->user->checkAccess("enrollment/update")) {
			$approveColumn = array(
				'name' => '',
				'type' => 'raw',
				'value' => 'CHtml::link("", "", array(
					"class" => "ajaxModal confirm",
					"data-toggle" => "modal",
					"data-modal-class" => "delete-node",
					"data-modal-title" => "'.Yii::t('standard', '_APPROVE').'",
					"data-buttons" => \'[
						{"type": "submit", "title": "' . Yii::t('standard', '_CONFIRM') . '"},
						{"type": "cancel", "title": "' . Yii::t('standard', '_UNDO') . '"}
					]\',
					"data-url" => "courseManagement/confirmEnrollment&id=$data->idUser",
					"data-after-loading-content" => "hideConfirmButton();",
					"data-after-submit" => "updateWaitingEnrollmentContent",
				));',
				'htmlOptions' => array('class' => 'button-column-single')
			);
		}
		else {
			$approveColumn = array(
				'name' => '',
				'value' => '',
			);
		}
	
	?>

	<?php

	// If user is PU and has not subscribe permission don't create checkboxes
	if(Yii::app()->user->checkAccess("enrollment/update")) {
		$checkBxsColumn = array(
			'class' => 'CCheckBoxColumn',
			'selectableRows' => 2,
			'id' => 'course-waitingenrollment-grid-checkboxes',
			'value' => '$data->idUser',
			'checked' => 'in_array($data->idUser, Yii::app()->session[\'selectedItems\'])',
		);
	} else {
		$checkBxsColumn = array(
			'name' => '',
			'value' => ''
		);
	}

	?>

	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'course-waitingenrollment-grid',
		'htmlOptions' => array('class' => 'grid-view clearfix'),
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
		'dataProvider' => $model->dataProvider(),
		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'pager' => array(
			'class' => 'DoceboCLinkPager',
			'maxButtonCount' => 8,
		),
		'ajaxUpdate' => 'course-waitingenrollment-grid-all-items',
		'beforeAjaxUpdate' => 'function(id, options) {
				options.type = "POST";
				if (options.data == undefined) {
					options.data = {
						selectedItems: $(\'#\'+id+\'-selected-items-list\').val()
					}
				}
			}',
		'afterAjaxUpdate' => 'function(id, data){
			$("#course-waitingenrollment-grid input").styler();
			afterGridViewUpdate(\'course-waitingenrollment-grid\');
				$(\'.popover-action\').popover({
					placement: \'bottom\',
					html: true
				});
		}',
		'columns' => array(
			$checkBxsColumn,
			array(
				'name' => 'user.userid',
				'header' => Yii::t('standard', '_USERNAME'),
				'value' => 'Yii::app()->user->getRelativeUsername($data->user->userid)',
				'htmlOptions' => array(
					'class' => 'td-username',
				),
			),
			array(
				'name' => 'user.fullname',
				'header' => Yii::t('admin_directory', '_DIRECTORY_FULLNAME'),
				'value' => '$data->user->fullName',
			),
			array(
				'name' => 'level',
				'header' => Yii::t('standard', '_LEVEL'),
				'value' => 'Yii::t(\'levels\', \'_LEVEL_\'.$data->level)',
			),
			array(
				'name' => 'subscribed_by',
				'header' => Yii::t('standard', '_SUBSCRIBED_BY'),
				'type' => 'raw',
				'value' => '$data->subscribedBy->fullName.\'<br/>\'.$data->subscribedBy->email',
			),
			array(
				'name' => 'date_inscr',
				'header' => Yii::t('certificate', '_DATE_ENROLL'),
				'value' => '$data->date_inscr',
			),

			$approveColumn,

			array(
				'name' => '',
				'type' => 'raw',
				'value' => function ($data){
					//Check if user is subscribed to a Learning Plan and if has enroll permission if he is PU
					if($data->isSubscribedtoLearningPlan($data->idUser) === false && Yii::app()->user->checkAccess("enrollment/update")){
						return '<a href="" class="ajaxModal delete-action" data-toggle = "modal" data-modal-class = "delete-node" data-modal-title ="'.Yii::t('admin_directory', '_REFUSE_USER').'" data-buttons = \'[
										{"type": "submit", "title": "' . Yii::t('standard', '_CONFIRM') . '"},
										{"type": "cancel", "title": "' . Yii::t('standard', '_UNDO') . '"}
									]\' data-url = "' . Docebo::createAbsoluteUrl('courseManagement/cancelEnrollment',array( 'id' => $data->idUser)) . '", data-after-loading-content = "hideConfirmButton();" data-after-submit = "updateWaitingEnrollmentContent"></a>';
					}
				},
				'htmlOptions' => array('class' => 'button-column-single')
			),
		),
	)); ?>
</div>

<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input, select').styler();
			replacePlaceholder();
			$('.filters .jq-radio, .filters .jq-checkbox').click(function () {
				$('.ajax-grid-form').submit();
			});
		});
	})(jQuery);
</script>