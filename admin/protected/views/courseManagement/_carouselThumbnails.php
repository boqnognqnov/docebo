<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2/15/16
 * Time: 10:45 AM
 */
$html = '';
 if (!empty($userImages)) {

    echo '<div class="item active">' ;
		 foreach ($userImages as $key => $imageUrl) {
			 $checked = '';
			 if($selectedImageId && ((int)$selectedImageId == $key)){
				 $checked = 'checked';
			 }elseif($key == $model->img_course && !$selectedImageId){
				 $checked = 'checked';
			 }
			 $html = CHtml::image($imageUrl);
			 $html .= '<input type="radio" name="LearningCourse[selectedImage]" value="' . $key . '" '. ($key == $model->img_course ? 'checked="checked"' : '') .' />';
			 echo '<div class="sub-item ' . $checked .'">' . $html;
			 echo '<span class="jq-radio ' . $checked .'" style="display: inline-block"><span></span></span>';
			 echo '<span class="deleteicon"><span class="i-sprite is-remove red"></span></span>';
			 echo '</div>';
		}

    echo	'</div>';

}

