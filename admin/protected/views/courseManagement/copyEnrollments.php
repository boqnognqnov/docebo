<div id="copy-course-enrollments-container" class="form">
	<?php
	$uniqueCopyEnrollmentsId = uniqid();

	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'course_enrollments_form',
	)); ?>

	<div class="copy-course-line">
		<label for="advanced-search-course"><?php echo Yii::t('standard', '_COURSE'); ?></label>
		<?php echo $form->error($learningCourse, 'name'); ?>
        <input data-url="<?= Docebo::createAppUrl('admin:courseManagement/courseEnrollmentsAutocomplete') ?>" class="typeahead" id="advanced-search-course" autocomplete="off" type="text" name="LearningCourse[name]" placeholder="<?php echo Yii::t('course_management', '_TYPE_HERE_TO_SELECT_COURSES'); ?>" value="<?php echo $learningCourse->name; ?>"/>
	</div>

	<div class="copy-course-line">
		<label for="LearningCourseuser_level"><?php echo Yii::t('standard', '_LEVEL'); ?></label>
		<?php echo $form->dropDownList($model, 'level', $courseUserlevels, array('prompt'=>Yii::t('standard', '_ALL'))); ?>
	</div>

	<div class="copy-checkboxes">
        <p><?php echo Yii::t('standard', '_STATUS'); ?></p>
		<?php echo $form->radioButtonList($model, 'status', array(
			'' => Yii::t('standard', '_ALL'),
			LearningCourseuser::$COURSE_USER_SUBSCRIBED => Yii::t('standard', '_USER_STATUS_SUBS'),
			LearningCourseuser::$COURSE_USER_BEGIN => Yii::t('standard', '_USER_STATUS_BEGIN'),
			LearningCourseuser::$COURSE_USER_END => Yii::t('standard', 'Completed'),
		), array(
			'template' => "<td>{input}</td><td>{label}</td>",
			'container' => '',
			'separator' => ''
		));
		?>
	</div>

	<?php if(!$skipSoftDeadline) { ?>
		<br />
		<div class="row-fluid" id="choose-deadline">
			<div class="span1">
				<?php
				$checked = Yii::app()->request->getParam('set_enrollment_deadlines', false);
				echo CHtml::checkBox("set_enrollment_deadlines", $checked, array(
					'id'    =>  'set_enrollment_deadlines-check-btn'.$uniqueCopyEnrollmentsId,
					'class' =>  'set_enrollment_deadlines-check', //. ($assignedCount == 0 ? 'no-users-assigned' : ''),
				)); ?>
			</div>
			<div  class="span11" id="set_enrollment_deadlines-hint">
				<div><?php echo CHtml::label(Yii::t('standard','Set enrollment deadlines'),
						'set_enrollment_deadlines-check-btn'.$uniqueCopyEnrollmentsId,
						array('class'=>'single-course-mass-enrollment-inline-label')); ?>
				</div>
				<div id="set_enrollment_deadlines-label-hint"><?= Yii::t('coaching', 'Leave blank for no deadline') ?></div>
			</div>
		</div>

		<br />
		<div id="datepickers-wrapper" class="datepickers-wrapper row-fluid">
			<div class="span12">
				<?php echo $form->error($learningCourse, 'status'); ?>
				<div style="margin-right: 20px;" class='input-append date clearfix' data-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>' data-date-language='<?= Yii::app()->getLanguage() ?>'>
					<?php
					$this->widget('common.widgets.DatePicker', array(
						'id' => 'single-course-mass-enrollment-deadline-start-date'.$uniqueCopyEnrollmentsId,
						'label' => CHtml::activeLabel(LearningCourseuser::model(), 'date_begin_validity',array('id'=>'single-course-mass-enrollment-date_begin_validity-label')),
						'fieldName' => CHtml::activeName(LearningCourseuser::model(), 'date_begin_validity'),
						'value' => ($course->date_begin && $course->date_begin != '0000-00-00') ? Yii::app()->localtime->toLocalDate($course->date_begin) : $date_begin_validity,
						'htmlOptions' => array(
							'id' => CHtml::activeId(LearningCourseuser::model(), 'single-course-mass-enrollment-date_begin_validity'.$uniqueCopyEnrollmentsId),
							'class' => 'datepicker datepicker-input',
							'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')),
							'data-date-language' => Yii::app()->getLanguage(),

						),
						'pickerStartDate'	=> ($course->date_begin && $course->date_begin!='0000-00-00') ? $course->date_begin : '',
						'pickerEndDate'     => ($course->date_end && $course->date_end!='0000-00-00') ? $course->date_end : '',
					));
					?>
				</div>
				<div class='input-append date clearfix' data-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>' data-date-language='<?= Yii::app()->getLanguage() ?>'>
					<?php
					$this->widget('common.widgets.DatePicker', array(
						'id' => 'single-course-mass-enrollment-deadline-end-date'.$uniqueCopyEnrollmentsId,
						'label' => CHtml::activeLabel(LearningCourseuser::model(), 'date_expire_validity',array('id'=>'single-course-mass-enrollment-date_expire_validity-label')),
						'fieldName' => CHtml::activeName(LearningCourseuser::model(), 'date_expire_validity'),
						'value' => ($course->date_end && $course->date_end != '0000-00-00') ? Yii::app()->localtime->toLocalDate($course->date_end) : $date_expire_validity,
						'htmlOptions' => array(
							'id' => CHtml::activeId(LearningCourseuser::model(), 'single-course-mass-enrollment-date_expire_validity'.$uniqueCopyEnrollmentsId),
							'class' => 'datepicker datepicker-input',
							'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')),
							'data-date-language' => Yii::app()->getLanguage(),

						),
						'pickerStartDate'	=> ($course->date_begin && $course->date_begin!='0000-00-00') ? $course->date_begin : '',
						'pickerEndDate'     => ($course->date_end && $course->date_end!='0000-00-00') ? $course->date_end : '',
					));
					?>
				</div>
			</div>

		</div>

	<?php } ?>

	<?php $this->endWidget(); ?>
</div>

<?php if(!$skipSoftDeadline) { ?>
<script type="text/javascript">

	$('#set_enrollment_deadlines-check-btn<?=$uniqueCopyEnrollmentsId ?>').styler();
	$(document).on('change', '#set_enrollment_deadlines-check-btn<?=$uniqueCopyEnrollmentsId ?>',function(){
		if($(this).is(':checked')) {
			$('.datepickers-wrapper').show();
		}
		else {
			$('#datepickers-wrapper').hide();
		}

	});

	if ($('#set_enrollment_deadlines-check-btn<?=$uniqueCopyEnrollmentsId ?>').is(':checked')) {
		$('.datepickers-wrapper').show();
	} else {
		$('.datepickers-wrapper').hide();
	}

</script>
<?php } ?>