<?php
if ($course->course_type == LearningCourse::TYPE_CLASSROOM)
{
	 $this->widget('root.common.widgets.SessionGrid', array(
		'course' => $course,
		'title' => Yii::t('classroom', 'Select session'),
		'ajaxUrl' => Docebo::createAdminUrl('courseManagement/renderSessionsGridForMassEnrollment', array(
				'idCourse' => $idCourse,
			)),
		'enablePagination' => false, // Still trying to fix this in Wizrd..
		'showExpiredSessions' => Yii::app()->user->getIsGodadmin(),
		'puFilter' => (Yii::app()->user->getIsPu() && !Yii::app()->user->checkAccess("/lms/admin/classroomsessions/mod")  && !Yii::app()->user->checkAccess("/lms/admin/classroomsessions/assign"))
	));
}
else
{
	 $this->widget('root.common.widgets.WebinarSessionGrid', array(
		'course' => $course,
		'title' => Yii::t('classroom', 'Select session'),
		'ajaxUrl' => Docebo::createAdminUrl('courseManagement/renderSessionsGridForMassEnrollment', array(
				'idCourse' => $idCourse,
			)),
		'enablePagination' => false,
		'puFilter' => (Yii::app()->user->getIsPu() && !Yii::app()->user->checkAccess("/lms/admin/webinarsessions/mod") && !Yii::app()->user->checkAccess("/lms/admin/webinarsessions/assign"))
	));
}