<?php $uniqueId = substr(md5(uniqid(rand(), true)), 0, 10); ?>

<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'course_form',
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
	));
	/* @var $form CActiveForm */
	?>
	<?php echo $form->hiddenField($model, 'idCourse'); ?>

	<div class="course-title-wrapper clearfix">
		<?php echo CHtml::label(Yii::t('standard', '_COURSE_NAME') . ' <span class="required">*</span>', CHtml::activeId($model, 'name')); ?>
		<?php echo $form->textField($model, 'name'); ?>
		<?php echo $form->error($model, 'name'); ?>
	</div>

	<?php
		$_typesList = array();
		$_typesList[LearningCourse::TYPE_ELEARNING] = '<i class="icon"></i>' . CHtml::tag('span', array(), Yii::t('standard', '_ELEARNING'));
		// removed this course type // $_typesList[LearningCourse::TYPE_MOBILE] = '<i class="icon"></i>' . CHtml::tag('span', array(), Yii::t('standard', 'Smartphone'));

		//if we are using classroom courses, add an option for course type
		if (PluginManager::isPluginActive('ClassroomApp')) {
            $_typesList[LearningCourse::TYPE_CLASSROOM] = '<i class="icon"></i>' . CHtml::tag('span', array(), Yii::t('standard', 'Classroom Course'));
        }

		$_typesList[LearningCourse::TYPE_WEBINAR] = '<i class="icon webinar-icon-small"></i>' . CHtml::tag('span', array(), Yii::t('webinar', 'Webinar'));

		// Let custom plugins add their own custom course types
		$customCourseDescriptors = array();
		Yii::app()->event->raise('CollectCustomCourseTypes', new DEvent($this, array('types' => &$customCourseDescriptors)));
		foreach($customCourseDescriptors as $id => $descriptor)
			$_typesList[$id] = $descriptor['new_course_radio'];

		$numTypes = count($_typesList);
		if ($numTypes > 1) {
            echo '<div class="course-type-wrapper clearfix">';
			// Draw a list of course types, selectable by user
			echo CHtml::label(Yii::t('course', '_COURSE_TYPE'), CHtml::activeId($model, 'course_type'));
            echo '<div class="table-container"><table style="width: 100%;"><tbody>';
			$chunks = array_chunk($_typesList, 3, true);
			foreach($chunks as $chunk) {
				echo '<tr>';
				echo $form->radioButtonList($model, 'course_type', $chunk,
					array('template' => '<td>{input}</td><td>{label}</td>', 'uncheckValue' => null,
						'container' => '', 'separator' => '', 'disabled' => ($model->idCourse ? 'disabled' : '')));
				$chunkSize = count($chunk);
				if($chunkSize < 3) {
					for($i=$chunkSize-1; $i<3; $i++)
						echo "<td></td>";
				}
				echo '</tr>';
			}
            echo '</tbody></table></div>';
            echo '</div>';
		} else {
			//put an hidden field with the only selectable value for "course type"
			echo $form->hiddenField($model, 'course_type', array('value' => $_typesList[0]));
		}
	?>

	<h3><?php echo Yii::t('standard', 'Thumbnail'); ?></h3>

	<div class="courseSlider">
		<?php
		// default crop size
		$cropSize = array(
			'width' => 300,
			'height' => 300,
		);
		Yii::app()->event->raise('OverrideCourseImageCropSize', new DEvent($this, array(
			'cropSize' => &$cropSize
		)));
		$this->renderPartial('_sliders', array(
			'model' => $model,
			'defaultImages' => $defaultImages,
			'userImages' => $userImages,
			'count' => $count,
			'selectedTab' => $selectedTab,
			'uniqueId' => $uniqueId,
			'cropSize' => $cropSize,
		), false, true); ?>
	</div>

	<div class="clearfix">
		<!--<div class="clearfix" style="height:250px;"> -->
		<?php echo CHtml::label(Yii::t('standard', '_DESCRIPTION') . ' <span class="required">*</span>', CHtml::activeId($model, 'description')); ?>
		<div class="textarea-wrapper">
			<?php echo $form->textArea($model, 'description', array('id' => 'tiny_'. $uniqueId)); ?>
		</div>
		<?php echo $form->error($model, 'description'); ?>
	</div>

	<div class="clearfix course-code-wrapper">
		<?php echo CHtml::label(Yii::t('standard', '_COURSE_CODE'), CHtml::activeId($model, 'code')); ?>
		<?php echo $form->textField($model, 'code'); ?>
		<?php echo $form->error($model, 'code'); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>
<?php echo CHtml::link('process', Yii::app()->createUrl('courseManagement/processNewCourse'), array(
	'id' => 'process-new-course',
	'class' => 'new-course-success popup-handler',
	'data-modal-title' => $model->idCourse ? Yii::t('standard', '_MOD') : Yii::t('standard', '_NEW_COURSE'),
	'data-modal-class' => 'new-course-success',
	'style' => 'display:none;',
)); ?>
<style>
	/* otherwise it's not scrollable and with more content the buttons cannot be shown */
	.modal {
		position: absolute !important;

		/* when top is in percentage it depends on height of page and on big height the modal is very low */
		top: 130px !important;
	}
</style>
<script>
$(function(){
	$('.nav.nav-tabs li').bind('click', function (e) {
		if ($(e.target).attr('data-target') == '.courseEnroll-page-groups')
			$('.upload-form-wrapper').show();
		else
			$('.upload-form-wrapper').hide();
	});

	// Make the course description TinyMCE
	TinyMce.attach('textarea#tiny_<?=$uniqueId?>', {height: 150});
});
</script>