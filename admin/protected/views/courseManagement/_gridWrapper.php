<?php $selectedColumns[] = array(
	'class' => 'CButtonColumn',
	'buttons' => array(
		'suspend' => array(
			'url' => 'Yii::app()->createUrl("' . $this->id . '/deactivateUser", array("idst" => $data->idst))',
			'label' => 'Suspend',
			'click' => 'js:function() {
						if (confirm("Really?")) {
							$("#users-management-grid").yiiGridView("update", {
								type: "POST",
								dataType: "json",
								url: $(this).attr(\'href\'),
								success: function(data) {
									$(\'#users-management-grid\').yiiGridView(\'update\');
								}
							});
						}
						return false;
					}',
			'visible' => '$data->valid == CoreUser::STATUS_VALID && Yii::app()->user->checkAccess(\'/framework/admin/usermanagement/deactivate_user\') && Yii::app()->user->id != $data->idst',
		),
		'activate' => array(
			'url' => 'Yii::app()->createUrl("' . $this->id . '/activateUser", array("idst" => $data->idst))',
			'label' => 'Reactivate',
			'click' => 'js:function() {
						if (confirm("Really?")) {
							$(\'#users-management-grid\').yiiGridView(\'update\', {
								type: \'POST\',
								dataType: \'json\',
								url: $(this).attr(\'href\'),
								success: function(data) {
									$(\'#users-management-grid\').yiiGridView(\'update\');
								}
							});
						}
						return false;
					}',
			'visible' => '$data->valid == CoreUser::STATUS_NOTVALID && Yii::app()->user->checkAccess(\'/framework/admin/usermanagement/deactivate_user\') && Yii::app()->user->id != $data->idst',
		),
		'update' => array(
			'url' => 'Yii::app()->createUrl("' . $this->id . '/updateUser", array("idst" => $data->idst))',
			'options' => array(
				'class' => '',
				'modal-title' => 'Edit User',
			),
		),
		'delete' => array(
			'url' => 'Yii::app()->createUrl("' . $this->id . '/deleteUser", array("idst" => $data->idst))',
			'visible' => 'Yii::app()->user->id != $data->idst',
		),
	),
	'template' => '{suspend}{activate}{update}{delete}',
);

$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'courses-management-grid',
	'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
	'dataProvider' => $userModel->dataProvider(),
	'columns' => $selectedColumns,
	'template' => '{items}{pager}',
	'pager' => array(
		'class' => 'DoceboCLinkPager',
		'maxButtonCount' => 8,
	),
	'afterAjaxUpdate' => 'function(id, data){ filterColumns(); $("#users-management-grid input").styler(); }'
));
?>
