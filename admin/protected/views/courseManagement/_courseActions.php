<?php
$isTeacher = (LearningCourseuser::userLevel(Yii::app()->user->id, $id) > LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
$hasViewingPermission = Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/lms/admin/course/view');
$hasEditingPermission = Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/lms/admin/course/mod');
$hasReportViewingPermission = Yii::app()->user->getIsGodadmin() || (Yii::app()->user->getIsPu() && CoreUserPuCourse::isCourseAssignedToPU(Yii::app()->user->id, $id));
$hasAddPermission = Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/lms/admin/course/add');
$hasDeletePermission = Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/lms/admin/course/del');

$nodeActions_1 = $nodeActions_2 = $nodeActions_3 = array();

$isClassroomOrWebinar = ($courseType == LearningCourse::TYPE_CLASSROOM || $courseType == LearningCourse::TYPE_WEBINAR);
$isWebinar = ($courseType == LearningCourse::TYPE_WEBINAR);
Yii::app()->event->raise('CollectCustomCourseTypes', new DEvent($this, array('types' => &$customCourseDescriptors)));
if(isset($customCourseDescriptors[$courseType]))
	$isClassroomOrWebinar = $isClassroomOrWebinar || $customCourseDescriptors[$courseType]['session_based'];

if (!$isClassroomOrWebinar) {
	$nodeActions_1[] = '<li class="node-action clearfix"><span class="student-level"></span>'.Yii::t('levels', '_LEVEL_3').'<br>'.$countUsers.'</li>';
	$nodeActions_1[] = '<li class="node-action clearfix"><span class="admin-level"></span>'.Yii::t('course', '_COURSE_TEACHERS').'<br>'.$countAdmins.'</li>';
}
//price will be not visible if it's selling AND it's Shopify price
if ($isSelling && !$isMarketplaceCourse && !PluginManager::isPluginActive('ShopifyApp')) {
	$nodeActions_1[] = '<li class="node-action blue clearfix"><span class="selling-price"></span>'.$sellingPrice.'</li>';
}

if (!$isClassroomOrWebinar && ($hasEditingPermission || $isTeacher) && !in_array('training-materials', $disabledActions))
	$nodeActions_2[] = '<li class="node-action">'.CHtml::link(Yii::t('standard', 'Training materials'), Docebo::createLmsUrl('player/training', array('course_id' => $id, 'ref_src' => 'admin')), array('class' => 'training-materials')).'</li>';
if (!$isClassroomOrWebinar && $hasReportViewingPermission)
	$nodeActions_2[] = '<li class="node-action">'.CHtml::link(Yii::t('standard', '_REPORTS'), Docebo::createLmsUrl('player/report', array('course_id' => $id, 'ref_src' => 'admin')), array('class' => 'course-report')).'</li>';

// Start Manage Seats for god admins
if ($isSelling && Yii::app()->user->getIsGodadmin()) {
	$nodeActions_2[] = '<li class="node-action course-manage-seats-item"><span class="maxAvailable"></span>'.
		CHtml::link(Yii::t('course', 'Manage Seats'),
			Docebo::createAdminUrl('courseManagement/manageSeats',
				array('id' => $id, 'ref_src' => 'admin')
			),
			array(
				'class'             => 'open-dialog course-manage-seats',
				'data-dialog-title' => Yii::t('course', 'Manage Seats'),
				'data-dialog-class' => "manage-pu-seats-dialog",
				'data-dialog-id'    => "manage-pu-seats-dialog"
				)
			).'</li>';
} // End Manage Seats

if (($hasEditingPermission || $isTeacher) && !in_array('advanced-settings', $disabledActions))
	$nodeActions_2[] = '<li class="node-action">'.CHtml::link(Yii::t('menu', '_CONFIGURATION'), Docebo::createLmsUrl('player/coursesettings', array('course_id' => $id, 'ref_src' => 'admin')), array('class' => 'advanced-settings')).'</li>';


if (!$isClassroomOrWebinar && $hasAddPermission && !in_array('duplicate', $disabledActions)) {
	$nodeActions_3[] = '<li class="node-action">'.
		CHtml::link(Yii::t('standard', '_MAKE_A_COPY'), Yii::app()->createUrl('courseManagement/copyCourse', array('id' => $id)), array(
			'class' => 'course-copy course-default-handler',
			'modal-title' => Yii::t('standard', '_MAKE_A_COPY'),
			'data-modal-class' => 'copy-course',
		)).'</li>';
}
if ($hasEditingPermission && !in_array('edit', $disabledActions)) {
	$nodeActions_3[] = '<li class="node-action">'.
		$this->renderPartial(($isWebinar && false ? '//common/_disabledButton' : '//common/_modal'), array(
			'config' => array(
				'iconCls' => 'node-edit',
				'class' => 'new-course node-edit',
				'modalTitle' => Yii::t('standard', '_MOD'),
				'linkTitle' => '<i class="fa fa-edit"></i>' . Yii::t('standard', '_MOD'),
				'url' => 'courseManagement/updateCourse&id=' . $id,
				'buttons' => array(
					array(
						'type' => 'submit',
						'title' => Yii::t('standard', '_CONFIRM'),
						'formId' => 'course_form',
					),
					array(
						'type' => 'cancel',
						'title' => Yii::t('standard', '_UNDO'),
					),
				),
				'afterLoadingContent' => 'courseFormInit();',
				'beforeSubmit' => 'beforeCourseSubmit',
				'afterSubmit' => 'updateCourseContent',
			),
		), true).
        '</li>';
}
if ($hasDeletePermission && !in_array('delete', $disabledActions)) {
	$nodeActions_3[] = '<li class="node-action">'.
		$this->renderPartial(($isWebinar && false ? '//common/_disabledButton' : '//common/_modal'), array(
			'config' => array(
				'class' => 'delete-node',
				'iconCls' => 'delete-node',
				'linkTitle' => Yii::t('standard', '_DEL'),
				'modalTitle' => Yii::t('standard', '_DEL'),
				'url' => 'courseManagement/deleteCourse&id='.$id,
				'buttons' => array(
					array(
						'type' => 'submit',
						'title' => Yii::t('standard', '_CONFIRM'),
					),
					array(
						'type' => 'cancel',
						'title' => Yii::t('standard', '_CANCEL'),
					),
				),
				'afterLoadingContent' => 'hideConfirmButton();',
				'afterSubmit' => 'updateCourseContent',
			),
		), true).
	'</li>';
}

?>
<div class="node-actions">
	<ul>
		<?php
			echo implode('', $nodeActions_1);

			if (!empty($nodeActions_2)) {
				$hr = count($nodeActions_1) ? '<li class="node-action"><hr></li>' : '';
				echo '<li class="node-action">'.$hr.'</li>'
					.implode('', $nodeActions_2);
			}

			// Trigger event to let plugins show their custom popover actions
			$event = new DEvent($this, array (
				'courseId' => $id,
				'courseType' => $courseType
			));
			Yii::app()->event->raise('OnCourseListActionsRender', $event);

			if($event->shouldPerformAsDefault() && isset($event->return_value['extra_actions']))
				echo $event->return_value['extra_actions'];

			if (!empty($nodeActions_3)) {
				$hr = (count($nodeActions_2) || count($nodeActions_1)) ? '<hr>' : '';
				echo '<li class="node-action">'.$hr.'</li>'
					.implode('', $nodeActions_3);
			}
        ?>
	</ul>
</div>
