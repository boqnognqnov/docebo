<?php
	$model = new LearningCourseuser;
?>

<h3 class="title-bold">
	<?= Yii::t('standard', 'Change status') ?>
	<?= '&nbsp;('.count($ids).' '.strtolower(Yii::t('standard', '_USERS')).')' ?>
</h3>

<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'massive-change-status-form'
	)); ?>

	<?= CHtml::hiddenField('confirm', 1, array('id' => 'confirm-input')) ?>
	<?= CHtml::hiddenField('id-list', implode(',',$ids), array('id' => 'id-list-input')) ?>

	<div class="clearfix">
		<?php
		$statuses = LearningCourseuser::getStatusesArray();
		if (Yii::app()->user->getIsPu()) {
			unset($statuses[LearningCourseuser::$COURSE_USER_CONFIRMED]);
			unset($statuses[LearningCourseuser::$COURSE_USER_WAITING_LIST]);
		}
		echo CHtml::dropDownList('new-status', LearningCourseuser::$COURSE_USER_SUBSCRIBED, $statuses, array('id' => 'new-status-input'));
		echo CHtml::label(Yii::t('standard', '_STATUS'), 'to');
		?>
	</div>

	<fieldset class="forced-values">
		<div class='input-append date clearfix'
			 ddata-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>'
			 data-date-format='m/d/yyyy'
			 data-date-language='<?= Yii::app()->getLanguage() ?>'>
			<div>
				<?php echo CHtml::textField('forced_completion_details[date_complete]', '', ['placeholder' => Yii::t('standard', 'Set automatically')]); ?>
				<span class="add-on"><i class="icon-calendar"></i></span>
				<?php echo CHtml::link(Yii::t('standard', '_RESET'), 'javascript:void(0);', array(
					'class' => 'clear-input',
					'data-input-id' => chtml::activeId($model, 'date_complete'),
				)); ?>
			</div>
			<?php echo $form->label($model, 'date_complete'); ?>
		</div>

		<div class="input-append block clearfix">
			<div>
				<?php
				$scoreFieldParams = [
					'min' => '0',
					'step' => '0.01',
					'id' => 'LearningCourseuser_score_given',
					'placeholder' => 'Calculate automatically',
					'disabled' => ((boolean) $model->forced_score_given) ? null : 'disabled',
					'required' => 'required',
				];
				?>
				<?php echo CHtml::numberField('forced_completion_details[score_given]', '', $scoreFieldParams) ?>
			</div>
			<?php echo $form->label($model, 'score_given'); ?>
		</div>

		<div class="input-append block clearfix">
			<div>
				<label class="self-containing-checkbox">
					<?php echo CHtml::checkBox('forced_completion_details[forced_score_given]', false, [
						'data-enable-target' => '#LearningCourseuser_score_given',
						'data-enable-box' => '1',
					]) ?>
					<?php echo Yii::t('course_management', 'Set score manually'); ?>
				</label>
			</div>
		</div>
	</fieldset>

	<?php $this->endWidget(); ?>
</div>

<script type="text/javascript">
	$('.modal.edit-enrollment .date').bdatepicker();

	// Completion fields (completion date and forced score)
	function updateCompletionFieldAccessability()
	{
		var statusDropdown = $('#new-status-input'),
			form = statusDropdown.parents('form'),
			completionFields = form.find('.forced-values'),
			currentStatus = statusDropdown.val(),
			completedStatus = 2;

		if (currentStatus == completedStatus) {
			// Enable completion fields
			completionFields.removeAttr('disabled').show(0).find('.jq-checkbox').removeClass('disabled');
		} else {
			// Disable completion fields
			completionFields.attr('disabled', 'disabled').hide(0);
		}
	}
	updateCompletionFieldAccessability();

	var completionFields = $('fieldset.forced-values'),
		statusSwitch = $('#new-status-input'),
		enableToggleSwitch = completionFields.find('input[data-enable-box="1"]');
	;
	statusSwitch.change(updateCompletionFieldAccessability);
	enableToggleSwitch.each(function() {
		$(this).change(function() {
			var thisSwitch = $(this),
				form = thisSwitch.parents('form'),
				inputSelector = thisSwitch.data('enable-target'),
				toggleTarget = form.find(inputSelector);

			if (!toggleTarget.length) {
				console.warn('Availability toggle target with selector "' + inputSelector + '" could not be found!');
			}

			if (thisSwitch.prop('checked') == false) {
				toggleTarget.attr('disabled', 'disabled');
			} else {
				toggleTarget.removeAttr('disabled');
			}
		});
	});
</script>