<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php
$checkboxIdKey = time();
$countUsers = count($courseUser);
?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'course_form',
	));?>

	<?php if($usersInLearningPlan === $countUsers): ?>

		<p><?php echo Yii::t('curricula', '{n} selected user(s) have been assigned to a learning plan and cannot be unenrolled from course', array('{n}' => '<strong>'.$usersInLearningPlan.'</strong>')); ?></p>

	<?php elseif($usersInLearningPlan > 0 && $usersInLearningPlan < $countUsers): ?>
		<p><?php echo Yii::t('curricula', '{n} selected user(s) have been assigned to a learning plan and cannot be unenrolled from course', array('{n}' => '<strong>'.$usersInLearningPlan.'</strong>')); ?></p>
		<p><?php echo Yii::t('course_management', 'Reject {count} users(s) from course', array('{count}' => ($countUsers - $usersInLearningPlan))); ?></p>

		<div class="clearfix">
			<?php echo $form->checkbox($model, 'confirm', array('id' => 'LearningCourseuser_confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
			<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed', array(count($courseUser), '{username}' => $courseUser[0]->user->fullname)), 'LearningCourseuser_confirm_'.$checkboxIdKey); ?>
			<?php echo $form->error($model, 'confirm'); ?>
		</div>
	<?php elseif($usersInLearningPlan === 0): ?>
		<p><?php echo Yii::t('course_management', 'Reject {count} users(s) from course', array('{count}' => $countUsers)); ?></p>

		<div class="clearfix">
			<?php echo $form->checkbox($model, 'confirm', array('id' => 'LearningCourseuser_confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
			<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed', array(count($courseUser), '{username}' => $courseUser[0]->user->fullname)), 'LearningCourseuser_confirm_'.$checkboxIdKey); ?>
			<?php echo $form->error($model, 'confirm'); ?>
		</div>
	<?php endif; ?>

	<?php foreach ($courseUser as $user) {
		echo CHtml::hiddenField('ids[]', $user->idUser);
	} ?>
	<?php $this->endWidget(); ?>
</div>