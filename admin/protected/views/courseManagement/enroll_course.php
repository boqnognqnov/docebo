<?php
//exclude courses default value
$excludeCourses = array();
Yii::app()->event->raise('OnBeforeEnrollCourseRender',
    new DEvent($this, array('excludeCourses' => &$excludeCourses, 'usersCount' => &$usersCount)));

	Yii::app()->clientScript->scriptMap['jquery.js'] = false;
	Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
	if (empty($isNeedRegisterYiiGridJs)) {
		Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;
		Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
	}
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'courseEnroll-course-form',
	'htmlOptions' => array('class' => 'ajax-grid-form', 'data-grid' => '#courseEnroll-course-grid'),
)); ?>
<div class="main-section">

	<div class="filters-wrapper">
		<div class="selections" data-grid="courseEnroll-course-grid">
			<div class="input-wrapper">
				<input data-url="<?= Docebo::createAppUrl('admin:courseManagement/courseAutocomplete') ?>" data-type="core" class="typeahead" id="enroll-advanced-search-course" autocomplete="on" type="text" name="LearningCourse[name]" placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"/>
				<span class="search-icon"></span>
			</div>
			<?php $this->widget('ext.local.widgets.GridViewSelectAll', array(
				'gridId' => 'courseEnroll-course-grid',
				'class' => 'left-selections',
				// 'dataProvider' => $model->dataProvider(false),
				'dataProvider' => $model->dataProviderCatalogue($excludeCourses),
			)); ?>
		</div>
	</div>
</div>

<input type="hidden" name="contentType" value="html"/>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'select-user-form',
	'htmlOptions' => array(
		'data-grid-items' => 'true',
	),
)); ?>

	<div id="grid-wrapper" class="courseEnroll-course-table">
		<h2><?php echo Yii::t('report', '_REPORT_COURSE_SELECTION'); ?></h2>
		<?php /*$this->widget('zii.widgets.grid.CGridView', array(*/ ?>
		<?php $this->widget('adminExt.local.widgets.CXGridView', array(
			'id' => 'courseEnroll-course-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			// 'dataProvider' => $model->dataProvider(),
			'dataProvider' => $model->dataProviderCatalogue($excludeCourses),
			'columns' => array(
				array(
					'class' => 'CCheckBoxColumn',
					'selectableRows' => 2,
					'id' => 'courseEnroll-course-grid-checkboxes',
					'value' => '$data->idCourse',
					'checked' => 'in_array($data->idCourse, Yii::app()->session[\'selectedItems\'])',
				),
				'code',
				'name',
				array(
                    'name' => 'course_type',
                    'value' => 'ucFirst($data->course_type)'
                )
			),
			'template' => '{items}{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'pager' => array(
				'class' => 'DoceboCLinkPager',
				'maxButtonCount' => 8,
			),
			'beforeAjaxUpdate' => 'function(id, options) {
							options.type = "POST";
							if (options.data == undefined) {
								options.data = {
									contentType: "html",
									selectedItems: $(\'#\'+id+\'-selected-items-list\').val()
								}
							}
						}',
			'ajaxUpdate' => 'courseEnroll-course-grid-all-items',
			'afterAjaxUpdate' => 'function(id, data){$("#courseEnroll-course-grid input").styler(); afterGridViewUpdate(id);}',
			'cssFile' => false,
		)); ?>
	</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
    (function(){
        replacePlaceholder();
    })();
</script>