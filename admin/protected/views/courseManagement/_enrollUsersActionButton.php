<?php
	/**
	 * Diaplay a LINK (action) button to open a Dialog2/jWizard 
	 */ 
?>
<div>
<?php 

	$title 	= '<span></span>'.Yii::t('standard', 'Enroll users');
	$url = Docebo::createAdminUrl('courseManagement/axEnrollUsersToSingleCourse', array('idCourse' => $course->idCourse));
	
	echo CHtml::link($title, $url, array(
			'data-dialog-class' => 'users-selector',
			'class' => 'open-dialog course-enrollment',
			'rel' 	=> 'users-selector',
			'alt' => Yii::t('helper', 'Enroll users single'),
	));
	


?>

</div>


<script type="text/javascript">
//<![CDATA[
	/**
	 * jWizard Finish & Success callback
	 */
	function jWizardFinishSuccessCallback(data) {
		$.fn.yiiListView.update('course-enrollment-list');
	};   

	// jWizard Options
	var options = {
		dialogId: 'users-selector',
		successCallback: 'jWizardFinishSuccessCallback'    // this is hardcoded; define this function somewhere; Called by jWizard upon FINISH * SUCCESS 	
	};
	var jwizard = new jwizardClass(options);
//]]>
</script>