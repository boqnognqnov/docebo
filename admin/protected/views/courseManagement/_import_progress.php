<style>
<!--

	.modal.progressive-dialog .failed-lines {
		color: red;
	}

	.modal.progressive-dialog .failed-lines a {
		text-decoration: underline;
	}

-->
</style>
<?php
/* @var $this MigrationController */
/* @var $importForm SessionImportForm */

	echo CHtml::beginForm('', 'POST', array(
		'class'=>'ajax',
	));
	
	
	$progressClass = in_array($nextPhase, array('import')) ? 'progress-striped active' : '';

	// Dialog title is changing during progress
	switch ($nextPhase) {
		case 'import' 	: $dialogTitle = '<span></span>' . Yii::t('standard', '_IMPORT') . '...' ; break;
		case 'finish' 	: $dialogTitle = '<span></span>' . Yii::t('standard', 'Complete') . '...' ; break;
	}
	
?>
<div id="import-branches-container" class="import-users-container">

	<div class="row-fluid">
		<div class="span12">
			<?php if ($totalNumber > 0) : ?>
				<?php if ($nextPhase == 'import') : ?>
					<div class="pull-left sub-title" ><strong><?= Yii::t('standard', 'Importing {x} out of {y}', array('{x}' => $number, '{y}' => $totalNumber)) ?></strong></div>
					<div class="pull-right percentage-gauge"><?= number_format($percent,2) ?>%</div>
				<?php elseif ($nextPhase == 'finish') : ?>
					<div class="pull-left sub-title" ><strong><?= Yii::t('standard', 'Import completed') ?></strong></div>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="row-fluid">
		<div class="span12">
			<?php if ($totalNumber > 0) : ?>
				<div class="docebo-progress progress progress-success <?= $progressClass ?>">
   					<div style="width: <?= $percent ?>%;" class="bar full-height" id="uploader-progress-bar"></div>
				</div>
			<?php else: ?>
				<div class="text-center"><h2><?= Yii::t('standard', 'Nothing to import') ?>!</h2></div>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="row-fluid">
		<div class="span12 action-undergoing">
			<?php if ($totalNumber > 0) : ?>
				<?php if ($nextPhase == 'finish') : ?>
					<div class="row-fluid">
						<div class="span12">
							<?= Yii::t('standard', '{N} item(s) successfully created', array('{N}' => $importForm->successCounter)); ?>
						</div>
					</div>
					<?php 
						$failedCount = count($importForm->cumulativeFailedLines);
					?>
					<?php if ($failedCount > 0) : ?>
						<div class="row-fluid">
							<div class="span12 failed-lines">
								<?php 
									echo Yii::t('standard', '{N} item(s) failed to import, saved in a separate CSV file and available for <strong><a href="{url}">download</a></strong>',array(
										'{N}' 	=> $failedCount,
										'{url}'	=> Docebo::createAbsoluteLmsUrl('stream/file') . '&fn=' . $csvErrorFileName . '&col=uploads',		
									));
								?>
							</div>
						</div>
					<?php  endif; ?>
					
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
	
</div>

<div class="form-actions">
	<?php
		if ($nextPhase == 'finish') {
			echo CHtml::textField('phase', 'finish');
			echo CHtml::submitButton(Yii::t('standard', '_CLOSE'), array(
				'class' => 'btn-docebo green big finishing-button',
			));
		} 
		else {
			echo CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn-docebo black big close-dialog'));
		}
	?>
</div>




<?= CHtml::endForm() ?>


<script type="text/javascript">

	$(function(){
		//closing the dialog will reload main page anyway
		$(document).undelegate('.modal.progressive-dialog', 'dialog2.closed'); //clean previously attached events, if any
		$(document).delegate('.modal.progressive-dialog', 'dialog2.closed', function(e) {
			window.location.href = '<?php echo Docebo::createAdminUrl('courseManagement/index'); ?>'
		});
	});

</script>