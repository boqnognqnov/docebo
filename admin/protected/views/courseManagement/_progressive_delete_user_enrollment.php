<div class="row-fluid">
	<div class="span12">

		<div class="pull-left sub-title" >
			<b><?=Yii::t('course_management', 'Unenrolling {x} out of {y}', array('{x}' => $processedUsers, '{y}' => $totalUsers))?></b>
		</div>
		<div class="pull-right percentage-gauge"><?=$completedPercent?>%</div>
	</div>
</div>
<div class="row-fluid">
	<div class="span12">
		<div class="docebo-progress progress progress-success">
			<div style="width: <?=$completedPercent?>%;" class="bar full-height" id="uploader-progress-bar"></div>
		</div>
	</div>
</div>
<?php if($stop): ?>
	<div class="form-actions">
		<input class="btn btn-docebo black big close-dialog" type="submit" value=<?= Yii::t('standard', '_CLOSE') ?>>
	</div>
<?php endif; ?>
<?php if($stop): // Called only once when the progressive operation reaches 100% ?>
	<script type="text/javascript">
		$(function(){
			var gridId = "course-enrollment-list";
			updateSelectAllCounter(gridId,"");
			$.fn.yiiListView.update(gridId);

			<?php if(CoreUserPU::isPUAndSeatManager() && Yii::app()->user->getState('reload_page_after_dialog_close')):
			// PU is a "seat manager" and when he started this progressive unenrollment
			// process he had no free seats to distribute. Now that the operation ended,
			// he most likely has seats (but not necessarily), so we better reload the page
			// so the PU can see updated UIs and buttons, allowing him to enroll users again
			?>
				$(document).on('dialog2.closed', '#delete-user-enrollment-progressive', function(){
					window.location.reload();
				});
			<?php endif; ?>
		});
	</script>
<?php endif; ?>