<script type='text/javascript'>
	$(function(){
		$.post('<?=Docebo::createLmsUrl('progress/run')?>', {
			'type': <?=Progress::JOBTYPE_SEND_EMAIL_TO_ENROLLED_USERS?>,
			'idst': '<?= $users ?>',
			'courseId': '<?= $courseId ?>',
			'sessionId': '<?= $sessionId ?>',
			'ccMails': '<?= $ccMails ?>',
			'from': '<?= $from ?>',
			'subject': "<?= $subject ?>",
			'message': "<?= preg_replace("/\r|\n/", "", $message) ?>"
		}, function(data){
			var dialogId = 'delete-user-enrollment-progressive';

			$('<div/>').dialog2({
				autoOpen: true, // We will open the dialog later
				id: dialogId,
				title: '<?=addslashes(Yii::t('course', 'Send Email'))?>'
			});
			$('#'+dialogId).html(data);
		});
	})
</script>