<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'course_form',
	));?>

	<p><?php echo Yii::t('course_management', '_UNENROLL_USER_FROM_COURSE_POPUP_QUESTION', array('{count}' => $courseUser )); ?><?php if ($showAutoEnrollMessage) echo "*";?></p>

	<?php
	if (PluginManager::isPluginActive('CurriculaApp') && $excludedUsers > 0) {
		echo '<p class="note">';
		echo Yii::t('curricula', '{n} selected user(s) have been assigned to a learning plan and cannot be unenrolled from course', array('{n}' => '<strong>'.$excludedUsers.'</strong>'));
		echo '</p>';
	}
	?>

	<div class="clearfix">
		<?php echo $form->checkbox($model, 'confirm', array('id' => 'LearningCourseuser_confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed'), 'LearningCourseuser_confirm_'.$checkboxIdKey); ?>
		<?php echo $form->error($model, 'confirm'); ?>
	</div>

	<?=CHtml::hiddenField('ids', $ids, array('id'=>false))?>
	<?php $this->endWidget(); ?>

	<?php if ($showAutoEnrollMessage) echo  '<br><p><small>* ' . Yii::t('classroom', 'Remember, if the course has the "automatic enrollment of users from waiting list when a seat is freed" setting enabled, users from the waiting list automatically become enrolled, taking the seats of the unenrolled users.') . '</small></p>';?>
</div>