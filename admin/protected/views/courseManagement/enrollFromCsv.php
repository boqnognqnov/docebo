<div id="enroll-users-from-csv-single-course-form" class="form">
	<?php
	$uniqueImportFromCsvId = uniqid();

	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'course_form',
		'htmlOptions' => array(
			'enctype' => 'multipart/form-data',
		),
	)); ?>
	<div class="row-fluid select-file-wrapper">
		<div class="span12">
			<p><?php echo Yii::t('course', 'Enroll users by uploading a CSV file'); ?></p>

			<div>
				<?php echo CHtml::fileField('EnrollCsv', ''); ?>
				<?php echo $form->hiddenField($learningCourse, 'status', array('value' => 1)); ?>
				<?php echo $form->error($learningCourse, 'status'); ?>
			</div>
		</div>
	</div>

	<div style="margin-top: 15px; margin-bottom: 20px; margin-left: 0px;" class="span12">
		<?= Yii::t('standard', 'Choose separator').':' ?>
	</div>
	<div class="span12 separators" style=" margin-bottom: 20px;">
		<label class="radio pull-left" for="r_1">
			<?= CHtml::radioButton('separator', true, array(
				'value' => '1',
				'id' => 'r_1',
				'class' => 'radioButtons',
				'uncheckValue' => null)) . ' ,' ?>
		</label>
		<label class="radio pull-left" for="r_2">
			<?= CHtml::radioButton('separator', false, array(
				'value' => '2',
				'id' => 'r_2',
				'class' => 'radioButtons',
				'uncheckValue' => null)) . ' ;' ?>
		</label>
		<label style="margin-top: -8px" class="radio pull-left" for="r_3">
			<?= CHtml::radioButton('separator', false, array(
				'value' => '3',
				'id' => 'r_3',
				'class' => 'radioButtons',
				'uncheckValue' => null)) . Yii::t('standard', '_MANUAL') .': ' .CHtml::textField('manual_separator', '', array(
				'maxlength' => '1') ) ?>
		</label>
	</div>



	<div style=" margin-bottom: 10px; margin-left: 0px" class="span12">
		<label class="checkbox pull-left">
			<?= CHtml::checkBox('enroll_header', false, array('class' => 'checkBoxes'))  ?>
			<span style="padding-left: 5px"><?= Yii::t('admin_directory', '_GROUP_USER_IMPORT_HEADER') ?></span>
		</label>
	</div>


	<?php if(!$skipSoftDeadline) { ?>

		<div class="row-fluid" id="choose-deadline">
			<div class="span1">
				<?php
				$checked = Yii::app()->request->getParam('set_enrollment_deadlines', false);
				echo CHtml::checkBox("set_enrollment_deadlines", $checked, array(
					'id'    =>  'set_enrollment_deadlines-check-btn'.$uniqueImportFromCsvId,
					'class' =>  'set_enrollment_deadlines-check', //. ($assignedCount == 0 ? 'no-users-assigned' : ''),
				)); ?>
			</div>
			<div  class="span11" id="set_enrollment_deadlines-hint">
				<div><?php echo CHtml::label(Yii::t('standard','Set enrollment deadlines'), 'set_enrollment_deadlines-check-btn'.$uniqueImportFromCsvId,array('class'=>'single-course-mass-enrollment-inline-label')); ?></div>
				<div id="set_enrollment_deadlines-label-hint"><?= Yii::t('coaching', 'Leave blank for no deadline') ?></div>
			</div>
		</div>

		<br />
		<div id="datepickers-wrapper" class="datepickers-wrapper row-fluid">
			<div class="span12">
				<div style="margin-right: 20px;" class='input-append date clearfix' data-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>' data-date-language='<?= Yii::app()->getLanguage() ?>'>
					<?php
					$this->widget('common.widgets.DatePicker', array(
						'id' => 'single-course-mass-enrollment-deadline-start-date'.$uniqueImportFromCsvId,
						'label' => CHtml::activeLabel(LearningCourseuser::model(), 'date_begin_validity',array('id'=>'single-course-mass-enrollment-date_begin_validity-label')),
						'fieldName' => CHtml::activeName(LearningCourseuser::model(), 'date_begin_validity'),
						'value' => ($learningCourse->date_begin && $learningCourse->date_begin != '0000-00-00') ? Yii::app()->localtime->toLocalDate($learningCourse->date_begin) : $date_begin_validity,
						'htmlOptions' => array(
							'id' => CHtml::activeId(LearningCourseuser::model(), 'single-course-mass-enrollment-date_begin_validity'.$uniqueImportFromCsvId),
							'class' => 'datepicker datepicker-input',
							'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')),
							'data-date-language' => Yii::app()->getLanguage(),

						),
						'pickerStartDate'	=> ($learningCourse->date_begin && $learningCourse->date_begin!='0000-00-00') ? $learningCourse->date_begin : '',
						'pickerEndDate'     => ($learningCourse->date_end && $learningCourse->date_end!='0000-00-00') ? $learningCourse->date_end : '',
					));
					?>
				</div>
				<div class='input-append date clearfix' data-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>' data-date-language='<?= Yii::app()->getLanguage() ?>'>
					<?php
					$this->widget('common.widgets.DatePicker', array(
						'id' => 'single-course-mass-enrollment-deadline-end-date'.$uniqueImportFromCsvId,
						'label' => CHtml::activeLabel(LearningCourseuser::model(), 'date_expire_validity',array('id'=>'single-course-mass-enrollment-date_expire_validity-label')),
						'fieldName' => CHtml::activeName(LearningCourseuser::model(), 'date_expire_validity'),
						'value' => ($learningCourse->date_end && $learningCourse->date_end != '0000-00-00') ? Yii::app()->localtime->toLocalDate($learningCourse->date_end) : $date_expire_validity,
						'htmlOptions' => array(
							'id' => CHtml::activeId(LearningCourseuser::model(), 'single-course-mass-enrollment-date_expire_validity'.$uniqueImportFromCsvId),
							'class' => 'datepicker datepicker-input',
							'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')),
							'data-date-language' => Yii::app()->getLanguage(),

						),
						'pickerStartDate'	=> ($learningCourse->date_begin && $learningCourse->date_begin!='0000-00-00') ? $learningCourse->date_begin : '',
						'pickerEndDate'     => ($learningCourse->date_end && $learningCourse->date_end!='0000-00-00') ? $learningCourse->date_end : '',
					));
					?>
				</div>
			</div>
		</div>
	<?php } ?>

	<div class="row-fluid" id="enrollment-fields-chechbox">
		<div class="span1">
			<?php
			$checked = Yii::app()->request->getParam('set_enrollment_fields', false);
			echo CHtml::checkBox("set_enrollment_fields", $checked, array(
				'id'    =>  'set_enrollment_fields-check-btn'.$uniqueImportFromCsvId,
				'class' =>  'set_enrollment_fields-check', //. ($assignedCount == 0 ? 'no-users-assigned' : ''),
			)); ?>
		</div>
		<div  class="span11" id="set_enrollment_deadlines-hint">
			<div><?php echo CHtml::label(Yii::t('standard','Set enrollment additional fields'), 'set_enrollment_fields-check-btn'.$uniqueImportFromCsvId,array('class'=>'single-course-mass-enrollment-inline-label')); ?></div>
		</div>
	</div>

	<br />

	<div id="enrollment-fields" class="enrollment-fields row-fluid">
		<?php foreach($enrollmentFields as $field){
			$mandatory = $field['mandatory'] == 1 ? '*' : '';
			if($field['type'] == 1){?>
 				<div style="margin-right: 20px;" class='input-append date clearfix' data-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>' data-date-language='<?= Yii::app()->getLanguage() ?>'>
					<?php
					$this->widget('common.widgets.DatePicker', array(
						'id' => 'field-'.$field['id'],
						'label' => '<div class="span2">' . $field['title'].$mandatory . '</div>',
						'fieldName' => 'field-'.$field['id'],
						'value' => date(Yii::app()->localtime->getPHPLocalDateFormat('short')),
						'htmlOptions' => array(
							'id' => CHtml::activeId(LearningCourseuser::model(), 'field-'.$field['id']),
							'class' => 'datepicker datepicker-input',
							'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')),
							'data-date-language' => Yii::app()->getLanguage(),
						),
					));
					?>
				</div>
			<?php }elseif($field['type'] == 2){  ?>
				<div class="enrollment-dropdown-field clearfix">
					<?php
						echo CHtml::label(Yii::t('standard', $field['title'].$mandatory), 'select-level-input', array('class'=>'span2'));
						echo CHtml::dropDownList('field-'.$field['id'], '', $field['options'], array('class'=>'span10') );
					?>
				</div>
			<?php }elseif($field['type'] == 3){?>
				<div class="NodeTranslation">
					<?php
						echo CHtml::label(Yii::t('standard', $field['title'].$mandatory), 'field-'.$field['id'], array('class'=>'span2'));
						echo CHtml::textField('field-'.$field['id'], '', array('class'=>'span10'));
					?>
				</div>
			<?php }else{
				?>
				<div class="NodeTranslation">
					<?php
						echo CHtml::label(Yii::t('standard', $field['title'].$mandatory), 'field-'.$field['id'], array('class'=>'span2'));
						echo CHtml::textArea('field-'.$field['id'],'', array('class'=>'span10'));
					?>
				</div>
				<?php
			}
		}?>
	</div>

	<?php $this->endWidget(); ?>
</div>

<script type="text/javascript">

	$('#set_enrollment_deadlines-check-btn<?=$uniqueImportFromCsvId ?>').styler();
	$(document).on('change', '#set_enrollment_deadlines-check-btn<?=$uniqueImportFromCsvId ?>',function(){
		if($(this).is(':checked')) {
			$('.datepickers-wrapper').show();
		}
		else {
			$('.datepickers-wrapper').hide();
		}

	});

	if ($('#set_enrollment_deadlines-check-btn<?=$uniqueImportFromCsvId ?>').is(':checked')) {
		$('.datepickers-wrapper').show();
	} else {
		$('.datepickers-wrapper').hide();
	}

	// Enrollment additional fields
	$('.set_enrollment_fields-check').styler();
	$(document).on('change', '.set_enrollment_fields-check',function(){
		if($(this).is(':checked')) {
			$('.enrollment-fields').show();
		}
		else {
			$('.enrollment-fields').hide();
		}

	});

	if ($('.set_enrollment_fields-check').is(':checked')) {
		$('.enrollment-fields').show();
	} else {
		$('.enrollment-fields').hide();
	}

	$().ready(function () {
		var separator = $("#manual_separator");
		var confirmButton = $(".course-enrollment-from-csv").find(".btn.confirm-btn");
		if(confirmButton){
			confirmButton.on("click",
				function () {
					if ($("span#r_3-styler").hasClass("checked") && separator.val().length < 1 && $("span#separator-error").length < 1) {
						separator
							.addClass("error")
							.focus()
							.after("<span id='separator-error' style='margin-left: 7px;' class='error'><?= Yii::t('standard', 'Please enter a separator') ?></span>");
						return false;
					}
					if ($("span#separator-error").length) {
						return false;
					}
				});
		}

		separator.on('blur', function () {
			if ($(this).hasClass("error")) {
				$(this).removeClass("error");
				$("span#separator-error").remove();
			}
		});
	});

</script>

<style>
	.radio.pull-left {
		margin-right: 20px;
	}

	#manual_separator {
		width: 50px;
		margin-left: 10px;
	}
</style>