<script type='text/javascript'>
    $(function(){
        $.post('<?=Docebo::createLmsUrl('progress/run')?>', {
            'type': <?=Progress::JOBTYPE_CHANGE_ENROLLMENT_DEADLINE?>,
            'dateBegin': <?='"'.$dateBegin.'"'?>,
            'dateEnd': <?='"'.$dateEnd.'"'?>,
            'idst': '<?=is_array($users) ? implode(',', $users) : $users?>',
            'idCourse': <?=$currentCourseId?>
        }, function(data){
            var dialogId = 'enroll-users-progressive';

            $('<div/>').dialog2({
                autoOpen: true, // We will open the dialog later
                id: dialogId,
                title: '<?=addslashes(Yii::t('standard', 'Change enrollment deadline'))?>'
            });
            $('#'+dialogId).html(data);
        });
    })
</script>