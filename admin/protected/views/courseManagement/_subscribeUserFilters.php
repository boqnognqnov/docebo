<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'courseEnroll-user-form',
	'htmlOptions' => array('class' => 'ajax-grid-form', 'data-grid' => '#courseEnroll-user-grid'),
)); ?>
<div class="main-section">

	<div class="filters-wrapper">
		<div class="selections" data-grid="courseEnroll-user-grid">
			<div class="input-wrapper">
				<input data-url="<?= Docebo::createAppUrl('admin:courseManagement/usersAutocomplete') ?>" data-type="core" data-source-desc="true" class="typeahead" data-courseId="<?php echo $courseId; ?>" id="enroll-advanced-search-user" autocomplete="on" type="text" name="CoreUser[search_input]" placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"/>
				<span class="search-icon"></span>
			</div>
			<?php $this->widget('ext.local.widgets.GridViewSelectAll', array(
				'gridId' => 'courseEnroll-user-grid',
				'class' => 'left-selections',
				'dataProvider' => $model->dataProvider(),
				'itemValue' => 'idst',
			)); ?>
		</div>
	</div>
</div>

<input type="hidden" name="contentType" value="html"/>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>
<script type="text/javascript">
    (function ($) {
        $(function () {
            replacePlaceholder();
        });
    });
</script>