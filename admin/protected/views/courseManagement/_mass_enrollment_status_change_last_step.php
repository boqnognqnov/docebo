<script type='text/javascript'>
	$(function(){
        var processData = {
            'type': <?=Progress::JOBTYPE_CHANGE_ENROLLMENT_STATUS?>,
            'newStatus': <?=$newStatus?>,
            'idst': '<?=is_array($users) ? implode(',', $users) : $users?>',
            'idCourse': <?=$currentCourseId?>
        };

        <?php

        if ($isScoreForced !== null) {
            echo 'processData.isScoreForced = ' . ($isScoreForced ? 1 : 0) . ';';
        }

        if ($forcedScoreValue !== null) {
            echo 'processData.forcedScoreValue = ' . $forcedScoreValue . ';';
        }

        if ($forcedDateComplete !== null) {
            echo 'processData.forcedDateComplete = \'' . $forcedDateComplete . '\';';
        }
        ?>

		$.post('<?=Docebo::createLmsUrl('progress/run')?>', processData, function(data){
			var dialogId = 'enroll-users-progressive';

			$('<div/>').dialog2({
				autoOpen: true, // We will open the dialog later
				id: dialogId,
				title: '<?=addslashes(Yii::t('standard', 'Change status'))?>'
			});
			$('#'+dialogId).html(data);
		});
	})
</script>