<div class="clearfix row-fluid">
	<?php if($categoryIssue): ?>
	<div class="new-user-issue span3">
		<div class="register-message-icon">
	        <span>
				<i class="fa  fa-2x fa-warning"></i>
			</span>
		</div>
		<?php echo Yii::t('standard', 'The course was not created in the selected category because, the category was modified'); ?>
	</div>
	<?php else : ?>
		<div class="new-course-success span3">
			<div class="register-message-icon">
		        <span>
					<i class="fa  fa-2x fa-check-circle-o"></i>
				</span>
			</div>
			<?php echo Yii::t('standard', '_OPERATION_SUCCESSFUL'); ?>
		</div>
	<?php endif;?>

	<div class="new-course-success-content span9">
		<h3><?php echo Yii::t('standard', 'What do you want to do next?'); ?></h3>

		<?php if ($courseModel->course_type == LearningCourse::TYPE_CLASSROOM  && PluginManager::isPluginActive('ClassroomApp')): ?>

		<div class="span12 new-course-info-row">
			<div class="span8">
				<span><?php echo Yii::t('course_management', 'I want to add a Session'); ?></span>
			</div>
			<div class="span4">
				<?php echo CHtml::link(Yii::t('standard', '_ADD'), Docebo::createAppUrl('lms:player/training/session', array('course_id'=>$courseId)), array('class' => 'btn btn-submit')); ?>
			</div>
		</div>

		<?php elseif ( in_array($courseModel->course_type, array(LearningCourse::TYPE_ELEARNING, LearningCourse::TYPE_MOBILE)) ): //TO DO: check if this option is for other course types too ?>

		<div class="span12 new-course-info-row">
			<div class="span8">
				<span><?php echo Yii::t('course_management', '_I_WANT_ADD_TRAINING_RESOURCES'); ?></span>
			</div>
			<div class="span4">
				<?php echo CHtml::link(Yii::t('standard', '_ADD'), Docebo::createLmsUrl('player/training', array('course_id' => $courseId)), array('class' => 'btn btn-submit')); ?>
			</div>
		</div>

		<?php elseif ($courseModel->course_type == LearningCourse::TYPE_WEBINAR) : ?>
			<div class="span12 new-course-info-row">
				<div class="span8">
					<span><?php echo Yii::t('course_management', 'I want to add a Session'); ?></span>
				</div>
				<div class="span4">
					<?php echo CHtml::link(Yii::t('standard', '_ADD'), Docebo::createLmsUrl('player/training/webinarSession', array('course_id'=>$courseId)), array('class' => 'btn btn-submit')); ?>
				</div>
			</div>
		<?php endif; ?>

		<div class="span12 new-course-info-row">
			<div class="span8">
				<span><?php echo Yii::t('course_management', '_I_WANT_EDIT_ADVANCED_SETTINGS'); ?></span>
			</div>
			<div class="span4">
				<?php echo CHtml::link(Yii::t('standard', '_MOD'), Docebo::createLmsUrl('player/coursesettings', array('course_id' => $courseId)), array('class' => 'btn btn-submit')); ?>
			</div>
		</div>

		<div class="go-back span12 new-course-info-row">
			<div class="span8">
				<span><?php echo Yii::t('standard', 'i want go back'); ?></span>
			</div>
			<div class="span4">
				<?php echo CHtml::link(Yii::t('standard', '_BACK'), 'javascript:void(0);', array(
					'class' => 'btn close-btn',
					'data-dismiss' => 'modal',
				)); ?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('#process-new-course').remove(); //because if other closes - it always shows...
	})
</script>
