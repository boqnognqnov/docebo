<?php

	$this->renderPartial('_form', array(
		'model' => $model,
		'userImages' => $userImages,
		'defaultImages' => $defaultImages,
		'count' => $count,
		'selectedTab' => $selectedTab,
	));
