<?php

//$columns                = $coursesGridParams['columns'];
//$customCourseTypeJsCall = $coursesGridParams['customCourseTypeJsCall'];
//$courseModel            = $coursesGridParams['courseModel'];


$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'course-management-grid',
    'htmlOptions' => array('class' => 'grid-view clearfix'),
    'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
    'dataProvider' => $courseModel->dataProvider(true),
    'template' => '{items}{summary}{pager}',
    'summaryText' => Yii::t('standard', '_TOTAL'),
    'pager' => array(
        'class' => 'DoceboCLinkPager',
        'maxButtonCount' => 8,
    ),
    'ajaxUpdate' => 'all-items',
    'beforeAjaxUpdate' => 'function(id, options) {
		        resetSearchPlaceholder();
                options.data = $("#course-management-form").serialize();
            }',
    'afterAjaxUpdate' => 'function(id, data){
				$("#course-management-grid input").styler();

                //NOTE: this is to fix removing the tooltip attributes from the links
                if($(\'a.lps-list\').length) {
                    $(\'a.lps-list\').attr(\'rel\', \'tooltip\');
                    $(\'a.lps-list\').attr(\'onclick\', \'javascript:;\');
                    $(\'a.lps-list\').attr(\'trigger\', \'click hover focus\');
                    $("a[rel=\'tooltip\']").tooltip({\'html\': \'true\', delay: { "show": 0, "hide": 1000 }});
                }

				$(\'.popover-action\').popover({
					placement: \'bottom\',
					html: true
				});
				UpdateClassroomCells();
                $(document).controls();
				UpdateWebinarCells();'.$customCourseTypeJsCall.'
			 }',
    'columns' => $columns,
));
