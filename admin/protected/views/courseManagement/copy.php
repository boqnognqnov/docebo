<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'course_form',
		'htmlOptions' => array(
			'class' => '',
		)		
	)); ?>
	
	<div class="row-fluid">
		
		<div class="control-group">
			<div class="controls">
				<label for="<?= CHtml::activeId($model, 'name') ?>"></label>
				<?php echo Yii::t('course_management', '_NEW_COURSE_TITLE'); ?>
				<?php echo $form->textField($model, 'name'); ?>
				<?php echo $form->error($model, 'name'); ?>
			</div>
		</div>
		
		<div class="control-group">
			<div class="controls">
			   	<label class="checkbox" for="<?= CHtml::activeId($model, 'copyTraining') ?>">
					<?php echo $form->checkbox($model, 'copyTraining'); ?>
			    	&nbsp;<?php echo Yii::t('course_management', '_COPY_TRAINING_COURSE'); ?>
			    	<?php echo $form->error($model, 'copyTraining'); ?>
			    </label>
		    </div>	
		</div>
		    
		<div class="control-group">
			<div class="controls">    
		    	<label class="checkbox" for="<?= CHtml::activeId($model, 'copyEnrollments') ?>">
					<?php echo $form->checkbox($model, 'copyEnrollments'); ?>
			    	&nbsp;<?php echo Yii::t('course_management', '_COPY_ENROLLMENTS'); ?>
			    	<?php echo $form->error($model, 'copyEnrollments'); ?>
			    </label>
		    </div>
		</div>	    
		    
		<div class="control-group">
			<div class="controls">    
			    <label class="checkbox" for="<?= CHtml::activeId($model, 'copyBlockWidgets') ?>"">
					<?php echo $form->checkbox($model, 'copyBlockWidgets'); ?>
			    	&nbsp;<?php echo Yii::t('course_management', '_COPY_WIDGETS'); ?>
			    	<?php echo $form->error($model, 'copyBlockWidgets'); ?>
			    </label>
		    </div>
	    </div>
	    
	    
		<?php $this->endWidget(); ?>
		
		
		
		
	</div>
</div>
