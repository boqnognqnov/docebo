<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAdminUrl('dashboard/index'),
	Yii::t('standard', '_COURSES') => array('courseManagement/index'),
	Yii::t('help', 'Import ILT sessions'),
); ?>

<div class="main-section import">
	<h3 class="title-bold" style="margin-bottom: 10px;"><?php echo Yii::t('helper', 'Import ILT sessions');  ?></h3>

	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'session-import-form',
		'htmlOptions' => array(
			'enctype' => 'multipart/form-data',
		),
	)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row has-border">
		<div class="row-border clearfix">
			<?php echo $form->labelEx($model, 'file'); ?>
			<?php echo $form->fileField($model, 'file'); ?>
			<div>
				<span class="no-file"></span>
				<span class="file-name"></span>
				<div>(Max. <?php echo $model->getMaxFileSize('MB'); ?> Mb)</div>
			</div>
			<div class="download-file"><span><span class="download-icon"></span><?php echo CHtml::link(Yii::t('standard', '_DOWNLOAD'), $this->createUrl('courseManagement/importSessions', array('download' => 'sample'))); ?></span> <?php echo Yii::t('standard', '_A_SAMPLE_CSV_FILE'); ?></div>
		</div>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'separator'); ?><?php echo $form->radioButtonList($model, 'separator', array(
			'auto' => Yii::t('standard', '_AUTODETECT'),
			'comma' => ',',
			'semicolon' => ';',
			'manual' => Yii::t('standard', '_MANUAL').':',
		), array(
			'separator' => '',
		)); ?>
		<?php echo $form->textField($model, 'manualSeparator'); ?>

		<div>
			<?php echo $form->checkBox($model, 'firstRowAsHeader'); ?>
			<?php echo $form->labelEx($model, 'firstRowAsHeader'); ?>
		</div>
	</div>

	<div class="row has-border">
		<div class="row-border clearfix">
			<?php echo $form->labelEx($model, 'charset'); ?>
			<?php echo $form->textField($model, 'charset'); ?>
		</div>
	</div>

	<div class="row btns">
		<?php echo CHtml::link(Yii::t('standard', '_CANCEL'), array('courseManagement/index'), array('class' => 'btn close-btn')); ?>
		<?php echo CHtml::submitButton(Yii::t('standard', '_NEXT'), array('class' => 'btn confirm-btn')); ?>
	</div>

	<?php $this->endWidget(); ?>

</div>
<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input,select').styler({browseText:'<?=Yii::t('course_management', 'CHOOSE FILE')?>'});
		});
	})(jQuery);
</script>
