<div class="node-actions clearfix">
	<ul class="node-enrollment-users">
		<li class="node-action">
			<h5>
				<?php echo Yii::t('standard', '_DETAILS') ?>
			</h5>
		</li>
		<li class="node-action">
			<div class="node-email">
				<strong><?php echo Yii::t('standard', '_EMAIL') ?>:</strong>
				<p title="<?= $model->user->email ?>"><?php
                    echo (strlen($model->user->email) > 25) ? substr($model->user->email, 0, 25) . '&hellip;' : $model->user->email;
                ?></p>
			</div>
		</li>
		<li class="node-action">
			<div class="enrollment-date">
				<strong><?php echo Yii::t('standard', 'Enrollment date') ?>:</strong>
				<p title="<?= $model->date_inscr ?>"><?= $model->date_inscr ?></p>
			</div>
		</li>
		<li class="node-action">
			<div class="active-from">
				<p><?php echo Yii::t('subscribe', '_DATE_BEGIN_VALIDITY') ?>:</p> <?php echo $activeFrom; ?>
			</div>
		</li>
		<li class="node-action">
			<div class="active-untill">
				<p><?php echo Yii::t('subscribe', '_DATE_EXPIRE_VALIDITY') ?>:</p> <?php echo $activeTo; ?>
			</div>
		</li>
	</ul>
	<ul class="node-enrollment-actions">
		<li class="node-action">
			<h5>
				<?php
				echo strip_tags(Yii::t('dashboard', '_QUICK_LINKS'));
				//NOTE: the text has been stripped, since this text key may contain some html tags which interfere with the page styles.
				?>
			</h5>
		</li>
		<li class="node-action">
			<?php echo CHtml::link(
				Yii::t('stats', '_STATS_USERS'),
				str_replace('/admin/', '/lms/', Yii::app()->createUrl('player/report/byUser', array('course_id' => $model->idCourse, 'user_id' => $model->idUser))),
				array('class' => 'node-statisctics')
			); ?>
		</li>
		<?php if(Yii::app()->user->checkAccess('enrollment/update')) { ?>
		<li class="node-action">
			<?php echo CHtml::link(Yii::t('course_management', '_ENROLLMENT_EDIT'), Yii::app()->createUrl('courseManagement/editEnrollment', array(
				'userId' => $model->idUser,
				'courseId' => $model->idCourse,
			)), array(
				'class' => 'node-edit popup-handler',
				'data-modal-title' => Yii::t('course_management', '_ENROLLMENT_EDIT'),
				'data-modal-class' => 'edit-enrollment',
				'data-after-send' => 'courseEditEnrollmentAfterSubmit(data);',
			)); ?>
		</li>

		
		<li class="node-action">
			<?php echo CHtml::link(
				Yii::t('player', 'Reset active dates'),
				Yii::app()->createUrl('courseManagement/resetDates', array('course_id' => $model->idCourse, 'user_id' => $model->idUser)),
				array('class' => 'node-reset ajaxLink', 'data-callback' => 'function(){$.fn.yiiListView.update(\'course-enrollment-list\')}')
			); ?>
		</li>
		<?php } ?>
		<li class="node-action">
			<?php
				if(LearningCertificateAssign::model()->countByAttributes(array('id_course'=>$model->idCourse,'id_user'=>$model->idUser)) > 0)
					echo CHtml::link(
						Yii::t('course_management', '_COURSE_DELETE_CERTIFICATE'),
						Yii::app()->createUrl('courseManagement/deleteCertificate', array('id_user' => $model->idUser, 'id_course' => $model->idCourse)),
						array('class' => 'node-reset ajaxLink delete-certificate', 'data-callback' => 'function(){$.fn.yiiListView.update(\'course-enrollment-list\')}')
					);
			?>
		</li>
		<?php
			Yii::app()->event->raise('BeforeEditEnrollmentMenu', new DEvent($this, array(
				'id_user' => $model->idUser,
				'id_course' => $model->idCourse
			)));
		?>
	</ul>
</div>



