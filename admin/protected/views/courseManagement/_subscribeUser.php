<div id="grid-wrapper" class="courseEnroll-user-table">
<h2><?php echo Yii::t('standard', 'Enroll users'); ?></h2>
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'ajaxType' => 'POST',	
		'id' => 'courseEnroll-user-grid',
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
		'htmlOptions' => array('class' => 'grid-view clearfix'),
		'dataProvider' => $model->dataProviderEnroll($courseId),
		'cssFile' => false,
		'columns' => array(
			array(
				'class' => 'CCheckBoxColumn',
				'selectableRows' => 2,
				'id' => 'courseEnroll-user-grid-checkboxes',
				'value' => '$data->idst',
				'checked' => 'in_array($data->idst, Yii::app()->session[\'selectedItems\'])',
			),
			array(
				'name' => 'userid',
				'value' => 'Yii::app()->user->getRelativeUsername($data->userid);',
				'type' => 'raw',
			),
			array(
				'name' => 'fullname',
				'header' => Yii::t('standard', '_FULLNAME'),
				'value' => '$data->fullname',
				'type' => 'raw',
			),
			'email',
		),
		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'pager' => array(
			'class' => 'DoceboCLinkPager',
			'maxButtonCount' => 8,
		),
		'beforeAjaxUpdate' => 'function(id, options) {
			if (typeof options.data == "undefined") {
				options.data = {};
			}
			if (typeof options.data == "string") {
				options.data = $.deparam(options.data);
			}
			// For some reason, typeahead adds this parameter twice; Lets delete and it will be added by the ajaxFilter
			delete options.data["YII_CSRF_TOKEN"];
			
			// Pass Search text; Controller is expecting: CoreUser[search_input]
			var searched = $("#enroll-advanced-search-user").val();
			if($("#enroll-advanced-search-user").attr("placeholder") && $.trim($("#enroll-advanced-search-user").attr("placeholder")) === $.trim($("#enroll-advanced-search-user").val())){
				searched = "";
			}
			options.data["CoreUser[search_input]"] = searched;
			
			options.data.contentType = "html";
			options.data.selectedItems = $(\'#\'+id+\'-selected-items-list\').val();
		}',
		'ajaxUpdate' => 'courseEnroll-user-grid-all-items',
		'afterAjaxUpdate' => 'function(id, data){ $("#courseEnroll-user-grid input").styler(); afterGridViewUpdate(id);}',
	)); ?>
</div>
