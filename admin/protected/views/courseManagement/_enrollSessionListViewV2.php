<?
$event = new DEvent($this, array('course'=>$data));
Yii::app()->event->raise('onRenderSessionPickerMassEnrollment', $event);

if(!$event->shouldPerformAsDefault()){
	// Don't render this View (the raised event already handled the rendering)
	return;
}
?>

<div class="row-fluid"><div class="span12">


<div class="item" data-course-id="<?= $data->idCourse ?>">
    <div class="item-primary clearfix">
        <div class="item-col col-toggle-handle">
            <?= CHtml::tag('span', array('class'=>'toggle-handle docebo-sprite ico-arrow-bottom', 'data-expand-class'=>'ico-arrow-top', 'data-retract-class'=>'ico-arrow-bottom')) ?>
        </div>
        <div class="item-col col-session">
            <div class="empty"><em><?= Yii::t('classroom', 'No session selected') ?></em></div>
            <div class="details"></div>
        </div>
        <div class="item-col col-coursename"><?= $data->name ?></div>
    </div>
    <form class="item-secondary">
        <?= CHtml::label(
            CHtml::radioButton('enrollCourse['.$data->idCourse.'][option]',false,array(
                'id'=>'enroll-option-'.$data->idCourse.'-session',
                'class'=>'open-session-grid'
            ), 'session') . ' ' . Yii::t('classroom', 'I want to enroll user(s) <strong>to a session</strong>'),
            'enroll-option-'.$data->idCourse.'-session', array('class'=>'')) ?>
        <?= CHtml::label(
            CHtml::radioButton('enrollCourse['.$data->idCourse.'][option]', true, array(
                'id'=>'enroll-option-'.$data->idCourse.'-course',
                'class'=>'close-session-grid'
            ), 'course') . ' ' . Yii::t('classroom', 'I want to enroll user(s) <strong>to the course</strong>'),
            'enroll-option-'.$data->idCourse.'-course',  array('class'=>'')) ?>
    </form>
    <div class="item-tertiary">
    	<div id="sessiongrid-container-<?= $data->idCourse ?>"></div>
        <?php
	        $idCourse 	= $data->idCourse;
	        $course = LearningCourse::model()->findByPk($idCourse);
	        if ($course->course_type == LearningCourse::TYPE_CLASSROOM)
	        {
		        $this->widget('root.common.widgets.SessionGrid', array(
			        'course' => $course,
			        'title' => Yii::t('classroom', 'Select session'),
			        'ajaxUrl' => Docebo::createAdminUrl('courseManagement/renderSessionsGridForMassEnrollment', array(
					        'idCourse' => $idCourse,
				        )),
			        'enablePagination' => false, // Still trying to fix this in Wizrd..
			        'showExpiredSessions' => Yii::app()->user->getIsGodadmin(),
			        'puFilter' => (Yii::app()->user->getIsPu() && !Yii::app()->user->checkAccess("/lms/admin/classroomsessions/mod"))
		        ));
	        }
	        else
	        {
		        $this->widget('root.common.widgets.WebinarSessionGrid', array(
			        'course' => $course,
			        'title' => Yii::t('classroom', 'Select session'),
			        'ajaxUrl' => Docebo::createAdminUrl('courseManagement/renderSessionsGridForMassEnrollment', array(
					        'idCourse' => $idCourse,
				        )),
			        'enablePagination' => false,
			        'puFilter' => (Yii::app()->user->getIsPu() && !Yii::app()->user->checkAccess("/lms/admin/webinarsessions/mod"))
		        ));
	        }
        ?>
    </div>
</div>


</div></div>

