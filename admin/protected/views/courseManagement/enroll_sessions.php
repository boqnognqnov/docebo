<?php
/* @var $dataProvider CActiveDataProvider */

	Yii::app()->clientScript->scriptMap['jquery.js'] = false;
	Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
?>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'select-user-form',
    'htmlOptions' => array(
    ),
));

    foreach ($dataProvider->getData() as $data) {
        echo CHtml::hiddenField('enrollCourse['.$data->getPrimaryKey().'][session]', '');
    }
?>

<?php $this->endWidget(); ?>

    <div id="grid-wrapper" class="courseEnroll-sessions-table">

        <?php $this->widget('DoceboCListView', array(
            'id' => 'courseEnroll-session-list',
            'htmlOptions' => array('class' => 'docebo-list-view'),
            'template' => '{header}{items}',
            'dataProvider' => $dataProvider,
            'itemView' => '_enrollSessionListView',
            'columns' => array(
                array(
                    'name' => 'name',
                    'header' => Yii::t('standard', '_NAME')
                )
            )
        )); ?>

    </div>

<script type="text/javascript">
    (function(){
        replacePlaceholder();
        //$(':radio').styler({});

        var oEnrollSessionList = {
            $list: null,
            $wizard: null,

            init: function() {
                oEnrollSessionList.$list = $('#courseEnroll-session-list');

                oEnrollSessionList.$wizard = $('#select-user-form');

                // css class name for a selected list row
                var cssItemSelected = 'selected';

                oEnrollSessionList.$list.find('.item')
                    .bind('expand-row', function() {
                        var $item = $(this);
                        var $handle = $item.find('.toggle-handle');

                        // retract all other rows
                        $item.siblings().trigger('retract-row');
                        $item.addClass(cssItemSelected);

                        $handle
                            .removeClass($handle.data('retract-class'))
                            .addClass($handle.data('expand-class'));

                        $item.find('.item-secondary').slideDown().removeClass('hide');
                        if ( $item.find('.item-secondary .open-session-grid').is(':checked') )
                            $item.find('.item-tertiary').slideDown();

                    })
                    .bind('retract-row', function() {
                        var $item = $(this);
                        var $handle = $item.find('.toggle-handle');

                        $item.removeClass(cssItemSelected);

                        $handle
                            .removeClass($handle.data('expand-class'))
                            .addClass($handle.data('retract-class'));

                        $item.find('.item-secondary').hide();
                        $item.find('.item-tertiary').hide();
                    })
                    .find('.item-primary').click(function() {
                        var $item = $(this).closest('.item');

                        var fnName = $item.hasClass(cssItemSelected)
                            ? 'retract-row'
                            : 'expand-row';

                        $item.trigger(fnName);
                    });

                oEnrollSessionList.$list.find('.item-secondary :radio').change(function() {
                    var $item = $(this).closest('.item');
                    if ($(this).hasClass('open-session-grid')) {
                        $item.find('.item-tertiary').slideDown()
                            .find(':radio:checked').trigger('change');
                    } else {
                        $item.find('.item-tertiary').hide();

                        var $colSession = $item.find('.col-session');
                        $colSession.find('.details').hide();
                        $colSession.find('.empty').show();

                        // save course option for the current course
                        var courseId = $item.data('course-id');
                        oEnrollSessionList.$wizard.find('#enrollCourse_'+courseId+'_session').val('');
                    }
                });

                oEnrollSessionList.$list.find('.item-tertiary :radio').live('change', function() {
                    var sessionId = $(this).val();
                    var $td = $(this).closest('tr').find('td');
                    var $item = $(this).closest('.item');

                    var details = $td.eq(1).text() + ' - ' + $td.eq(4).text() + '<br/><?= ucfirst(Yii::t('standard', '_FROM')) ?> '
                        + $td.eq(2).text() + ' <?= Yii::t('standard', '_TO') ?> ' + $td.eq(3).text();

                    var $colSession = $item.find('.col-session');
                    $colSession.find('.empty').hide();
                    $colSession.find('.details').html(details).show();

                    // save session id into the hidden field
                    $item.find('.input-session-id').val(sessionId);


                    // save course option and session id for the current course
                    var courseId = $item.data('course-id');
                    oEnrollSessionList.$wizard.find('#enrollCourse_'+courseId+'_session').val(sessionId);
                });
            }
        };

        $(document).ready(oEnrollSessionList.init);
    })();
</script>