<div class="node-actions">
	<ul>
		<?php /* ***** TEMPORARY: TO BE REMOVED ****** */ ?>
		<?php if(Settings::get('enable_category_advanced_settings') == 'on'): ?>
		<li class="node-action">
			<?php
			echo CHtml::link(Yii::t('menu', '_CONFIGURATION'), Yii::app()->createUrl('courseCategoryManagement/settings', array('id' => $id)), array(
				'class' => 'advanced-settings open-dialog',
				'data-dialog-class' => 'settings-category',
				'title' => Yii::t('menu', '_CONFIGURATION')
			));
			?>
		</li>
		<?php endif; ?>

		<?php if(Yii::app()->user->getIsGodadmin() || $canPUModifyCategory):?>
		<li class="node-action">
			<?php
			echo CHtml::link(Yii::t('standard', '_MOD'), Yii::app()->createUrl('courseCategoryManagement/edit', array('id' => $id)), array(
				'class' => 'node-edit new-category-handler',
				'data-modal-class' => 'edit-category',
				'modal-title' => Yii::t('standard', '_MOD'),
			));
			?>
		</li>
		<?php endif; ?>

		<?php if (!$isRoot): ?>
			<?php if (Yii::app()->user->getIsGodadmin()): ?>
			<li class="node-action">
				<?php
				echo CHtml::link(Yii::t('standard', '_MOVE'), Yii::app()->createUrl('courseCategoryManagement/move', array('id' => $id)), array(
					'class' => 'node-move new-category-handler',
					'data-modal-class' => 'move',
					'modal-title' => Yii::t('standard', '_MOVE'),
				));
				?>
			</li>
			<?php endif; ?>

			<?php if(Yii::app()->user->getIsGodadmin() || $canPUModifyCategory): ?>
			<li class="node-action">
				<?php $this->renderPartial('//common/_modal', array(
					'config' => array(
						'class' => 'delete-node',
						'linkTitle' => Yii::t('standard', '_DEL'),
						'modalTitle' => Yii::t('standard', '_DEL'),
						'url' => 'courseCategoryManagement/delete&id=' . $id,
						'buttons' => array(
							array(
								'type' => 'submit',
								'title' => Yii::t('standard', '_CONFIRM'),
							),
							array(
								'type' => 'cancel',
								'title' => Yii::t('standard', '_CANCEL'),
							),
						),
						'afterLoadingContent' => 'if (data && ((data.data && data.data.update_tree) || data.update_tree)) { '
							.'$(".modal.in.delete-node").modal("hide"); '
							.'courseCategoriesTreeHelper.reloadCoursesCategoriesTree({'
							.' title: '.CJSON::encode(Yii::t('course_management', 'Courses categories tree update')).', '
							.' text: '.CJSON::encode(Yii::t('course_management', 'Courses categories tree has changed and needs to be updated')).', '
							.' button: '.CJSON::encode(Yii::t('standard', '_CONFIRM'))
							.'}); '
							.'}'
							.'hideConfirmButton();',
						'afterSubmit' => 'updateOrgTreeContent',
					),
				)); ?>
			</li>
			<?php endif; ?>
		<?php endif; ?>

		<?php
		// Trigger event so the plugins can add more items here
		$event = new DEvent($this, array('node' => $node));
		Yii::app()->event->raise('OnRenderCourseCategoryNodeActions', $event);
		if ($event->return_value)
			echo $event->return_value;
		?>

	</ul>
</div>
