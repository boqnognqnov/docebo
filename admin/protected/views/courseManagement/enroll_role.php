<div class="select-role-wrapper">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'select-user-form',
	)); ?>

	<p><?php echo Yii::t('course_management', '_YOU_ARE_GOING_TO_SUBSCRIBE', array(
		'{users}' => count($userlist),
		'{courses}' => count($courselist),
	)); ?></p>
    <div>
	    <p><?php echo Yii::t('standard', '_LEVEL', array(count($userlist))); ?></p>
	    <?php echo CHtml::dropDownList('enrollment_role', '', $userLevels, array('prompt' => Yii::t('standard', '_SELECT').'...')); ?>

	    <?php $this->endWidget(); ?>
    </div>
</div>
<script type="text/javascript">
	$('select').styler();
	//$('#select-user-form select').styler();
    //$('#select-user-form').jClever({applyTo: {select: true,checkbox: false,radio: false,button: false,file: false,input: false,textarea: false}});
</script>