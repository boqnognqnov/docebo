<div id="enroll-user-id">
<?php 

	$title 	= '<span></span>'.Yii::t('standard', 'Enroll users');
	
	// Dialog content is taken from this action
	$url = Docebo::createAdminUrl('courseManagement/axEnrollUsersToManyCourses');
	
	echo CHtml::link($title, $url, array(
			'data-dialog-class' => 'users-selector',
			'class' => 'open-dialog course-enrollment',
			'rel' 	=> 'mass-enrollment-wizard',
			'alt' => Yii::t('helper', 'Enroll users'),
			'data-dialog-title' => Yii::t('standard', 'Enroll users'),
			'title'	=> Yii::t('standard', 'Enroll users')
	));
	


?>

</div>


<script type="text/javascript">
//<![CDATA[

	// jWizard Options
	var options = {
		dialogId: 'mass-enrollment-wizard',
		alwaysPostGlobal: true,
		dispatcherUrl: '<?= $url ?>'  // All Wizard requests go here
	};
	var jwizardMassEnrollment = new jwizardClass(options);

	// Upon closing (with CONFIRM, i.e. on SUCCESS), jWizard fires a JS event
	$(document).on('jwizard.closed.success', '', function(e, data){
		$.fn.yiiGridView.update('course-management-grid');
	});

	
//]]>
</script>


