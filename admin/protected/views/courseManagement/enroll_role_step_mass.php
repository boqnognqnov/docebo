<h1><?= Yii::t('subscribe', '_CAPTION_SELECT_LEVELS') ?></h1>

<div class="select-role-wrapper">

<?php
	echo CHtml::beginForm('', 'POST', array(
		'id' => 'enroll-role-step-form',
		'name' => 'enroll-role-step-form',
		'class' => 'ajax jwizard-form',
	));

	echo CHtml::hiddenField('from_step', 'enroll-role-step');

?>

<p>
<?= Yii::t('course_management', '_YOU_ARE_GOING_TO_SUBSCRIBE', array('{users}' => $usersCount,'{courses}' => $coursesCount)) ?>
</p>

<div class="alert alert-error alert-compact text-center">
	<p>
		<?php 
			if($errors){
				foreach($errors as $v){
					print_r($v);
				}
			}
		?>
	</p>
</div>

<br>
<div class="row-fluid">
	<div class="span2 text-left"><p><?= Yii::t('standard', '_LEVEL', array($usersCount)) ?></p></div>
	<div id="level-wrapper" class="span10">
		<?= CHtml::dropDownList('enrollment_role', -1, $userLevels, array('prompt'=>Yii::t('standard', '_SELECT').'...')) ?>
	</div>
</div>

<?php if(!$skipSoftDeadline) { ?>

	<div class="row-fluid" id="choose-deadline">
		<div class="span12">
	<?php echo CHtml::checkBox("set_enrollment_deadlines", false, array(
		'id'    =>  'set_enrollment_deadlines-check-btn',
		'class' =>  'set_enrollment_deadlines-check', //. ($assignedCount == 0 ? 'no-users-assigned' : ''),
	)); ?>
			<div id="set_enrollment_deadlines-hint">
				<div><?php echo CHtml::label(Yii::t('standard','Set enrollment deadlines'), 'set_enrollment_deadlines-check-btn',array('class'=>'single-course-mass-enrollment-inline-label')); ?></div>
				<div id="set_enrollment_deadlines-label-hint"><?= Yii::t('coaching', 'Leave blank for no deadline') ?></div>
			</div>
		</div>
	</div>
	<br />
	<div id="datepickers-wrapper" class="row-fluid">
		<div class="span12">
			<div class='input-append date clearfix' data-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>' data-date-language='<?= Yii::app()->getLanguage() ?>'>
				<?php
				$this->widget('common.widgets.DatePicker', array(
					'id' => 'single-course-mass-enrollment-deadline-start-date',
					'label' => CHtml::activeLabel(LearningCourseuser::model(), 'date_begin_validity',array('id'=>'single-course-mass-enrollment-date_begin_validity-label')),
					'fieldName' => CHtml::activeName(LearningCourseuser::model(), 'date_begin_validity'),
					'value' => Yii::app()->localtime->toLocalDate(Yii::app()->localtime->getUTCNow()),
					'htmlOptions' => array(
						'id' => CHtml::activeId(LearningCourseuser::model(), 'single-course-mass-enrollment-date_begin_validity'),
						'class' => 'datepicker',
						'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')),
						'data-date-language' => Yii::app()->getLanguage(),

					),
					'pickerStartDate'	=> '',
					'pickerEndDate'     => '',
				));
				?>
			</div>
			<div class='input-append date clearfix' data-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>' data-date-language='<?= Yii::app()->getLanguage() ?>'>
				<?php
				$this->widget('common.widgets.DatePicker', array(
					'id' => 'single-course-mass-enrollment-deadline-end-date',
					'label' => CHtml::activeLabel(LearningCourseuser::model(), 'date_expire_validity',array('id'=>'single-course-mass-enrollment-date_expire_validity-label')),
					'fieldName' => CHtml::activeName(LearningCourseuser::model(), 'date_expire_validity'),
					'value' => '',
					'htmlOptions' => array(
						'id' => CHtml::activeId(LearningCourseuser::model(), 'single-course-mass-enrollment-date_expire_validity'),
						'class' => 'datepicker',
						'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')),
						'data-date-language' => Yii::app()->getLanguage(),

					),
					'pickerStartDate'	=> '',
					'pickerEndDate'     => '',
				));
				?>
			</div>
		</div>

	</div>

<?php } ?>
	
	<div id="enrollment-fields" class="enrollment-fields row-fluid">
		<?php 
		if(!empty($fields)){
			foreach($fields as $field){
				$mandatoryClass = " ";
				$mandatory = '';
				if($field['mandatory'] == 1){
					$mandatory = "*";
					$mandatoryClass = " mandatory";
				}

				if($field['type'] == LearningEnrollmentField::TYPE_FIELD_DATE){?>
					<div style="margin-right: 20px;" class='input-append date clearfix <?= $mandatoryClass; ?>' data-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>' data-date-language='<?= Yii::app()->getLanguage() ?>'>
						<?php
						$this->widget('common.widgets.DatePicker', array(
							'id' => 'field-'.$field['id'],
							'label' => '<div class="span2 select-level-label">'.$field['title'].$mandatory.'</div>',
							'fieldName' => 'field-'.$field['id'],
							'value' => date(Yii::app()->localtime->getPHPLocalDateFormat('short')),
							'htmlOptions' => array(
								'id' => CHtml::activeId(LearningCourseuser::model(), 'field-'.$field['id']),
								'class' => 'datepicker datepicker-input',
								'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')),
								'data-date-language' => Yii::app()->getLanguage(),
							),
						));
						?>
					</div>
				<?php }elseif($field['type'] == LearningEnrollmentField::TYPE_FIELD_DROPDOWN){  ?>
					<div class="enrollment-dropdown-field clearfix <?= $mandatoryClass; ?>">
						<?php
							echo CHtml::label(Yii::t('standard', $field['title'].$mandatory), 'select-level-input', array('class' => 'span2'));
							echo CHtml::dropDownList('field-'.$field['id'], '', $field['options'], array('class' => 'span10') );
						?>
					</div>
				<?php }elseif($field['type'] == LearningEnrollmentField::TYPE_FIELD_TEXTFIELD){?>
					<div class="NodeTranslation <?= $mandatoryClass; ?>">
						<?php
							echo CHtml::label(Yii::t('standard', $field['title'].$mandatory), 'field-'.$field['id'], array('class' => 'span2'));
							echo CHtml::textField('field-'.$field['id'], '', array('class' => 'span10'));
						?>
					</div>
				<?php }elseif($field['type'] == LearningEnrollmentField::TYPE_FIELD_IFRAME){?>
						<?php // SKIP IFRAME ?>
				<?php }else{
					?>
					<div class="NodeTranslation <?= $mandatoryClass; ?>">
						<?php
							echo CHtml::label(Yii::t('standard', $field['title'].$mandatory), 'field'.$key, array('class' => 'span2'));
							echo CHtml::textArea('field-'.$field['id'], '', array('class' => 'span10'));
						?>
					</div>
					<?php
				}
			}
		}?>
	</div>

<div class="clearfix"></div>
<div class="form-actions jwizard">
	<?= CHtml::submitButton(Yii::t('standard', '_PREV'), 		array('name'  => 'prev_button', 'class' => 'btn btn-docebo green big jwizard-nav')); ?>
	<?= CHtml::submitButton(Yii::t('standard', '_CONFIRM'), 	array('name'  => 'finish_wizard', 'class' => 'btn btn-docebo green big jwizard-nav jwizard-finish', 'data-jwizard-url' => $finishUrl)); ?>
	<?= CHtml::button(		Yii::t('standard', '_CANCEL'), 		array('class' => 'btn btn-docebo black big close-dialog', 'name' => 'cancel_step')); ?>
</div>


<?php echo CHtml::endForm(); ?>


</div>


<script type="text/javascript">

	// Style this dialog. It is part of jWizard, so use it to rpeserve class switching between steps
	jwizardMassEnrollment.setDialogClass('enroll-role-step');

	// Disable FINISH button if NO users and NO role are selected
	var usersCount = parseInt('<?= (int) $usersCount ?>');
	usersCount = 1;


	var selectedRole = $('select[name="enrollment_role"]').val();
	if ( (usersCount <= 0) || (selectedRole <= 0)) {
		$('input[name="finish_wizard"]').attr('disabled','disabled').addClass('disabled');
	}


	// Listen for ROLE dropdown change and adjust FINISH button, counting number of users as well
	$(document).off('change', 'select[name="enrollment_role"]')
		.on('change', 'select[name="enrollment_role"]', function(){
		if ($(this).val() <= 0 || (usersCount <= 0)) {
			$('input[name="finish_wizard"]').attr('disabled','disabled').addClass('disabled');
			$('a.jwizard-finish').addClass('disabled');
		}
		else {
			$('input[name="finish_wizard"]').removeAttr('disabled').removeClass('disabled');
			$('a.jwizard-finish').removeClass('disabled');
		}
	});

	$('#set_enrollment_deadlines-check-btn').styler();
	$(document).on('change', '#set_enrollment_deadlines-check-btn',function(){
		if($(this).is(':checked')) {
			$('#datepickers-wrapper').show();
		}
		else {
			$('#datepickers-wrapper').hide();
		}

	});



</script>
<style type="text/css">
	#choose-deadline {
		margin-top: 24px;
	}

	<?php if(!$skipSoftDeadline) { ?>
	.users-selector.modal {
		min-height: auto!important;
	}

	

	.modal.users-selector.enroll-role-step {
		min-height: initial!important;
	
	}

	<?php } else { ?>

	.users-selector.modal {
		min-height: auto!important;
	}

	.modal.users-selector.enroll-role-step .modal-body.opened {
	
	}

	.modal.users-selector.enroll-role-step {
		min-height: initial!important;
	
	}
	<?php }  ?>
</style>
