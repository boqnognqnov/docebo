<div class="copy-success-wrapper">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'course_enrollments_form',
	)); ?>
    <div class="copy-success"><div class="copy-success-icon"></div> <?php echo Yii::t('standard', '_OPERATION_SUCCESSFUL'); ?></div>
    <div>
	    <span><?php echo Yii::t('standard', 'Enrolled users'); ?>:</span>
	    <strong><?php echo (int)$countEnrolled[1]; ?></strong>
    </div>
    <div>
	    <span><?php echo Yii::t('standard', 'Skipped'); ?>:</span>
	    <strong><?php echo (int)$countEnrolled[0]; ?></strong>
    </div>
    <input class="btn confirm-btn" type="button" value="<?php echo Yii::t('standard', '_CONTINUE'); ?>" name="yt123" data-dismiss="modal" onclick="setTimeout('$(\'.modal-footer\').show();', 1000);">
	<?php $this->endWidget(); ?>
</div>