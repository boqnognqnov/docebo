<h1><?=Yii::t('course', 'Manage Seats') ?></h1>
<?php
// Start the Form
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'seats-management-form',
	'htmlOptions' => array(
		'class' => 'form ajax',
		'data-grid-items' => 'true',
		'method' => 'POST'
	)
));
?>

<div class="manage-pu-seats-header">
	<div class="add-pu-users-free-seats-label" style="margin-bottom: 0px; padding-bottom: 2px;">
		<strong><?=Yii::t('standard', 'Add Power Users with Free Extra Seats') ?></strong>
	</div>

	<hr class="header-hr" />

	<div class="assign-free-seats-row">
		<?= Yii::t('standard', '_ASSIGN') ?>
		<?= CHtml::textField('add_seats', '', array('id' => 'add_seats')) ?>
		&nbsp;&nbsp;
		<?= Yii::t('course', 'free extra seats to') . ' '. Yii::t('standard', '_PUBLIC_ADMIN_USER') ?>
		&nbsp;


		<div style="display: inline-block">
		<?php
			echo CHtml::dropDownList('choose_pus', '', array(), array(
			'id' => 'choose_pus',
			'empty' => '---',
			'multiple' => 'multiple',
			'class' => 'fbkcompleate'
		));
		?>
		</div>

		<?= CHtml::submitButton(Yii::t('standard', '_ASSIGN'), array(
			'id' => 'assign_button',
			'name' => 'assign_button',
			'class' => 'btn btn-docebo confirm-btn green big',
			'style'	=> 'float: right'
		));
		?>

	</div>


	<div class="manage-pu-extra-seats-subtitle">
		<div class="row-fluid">
			<div class="pull-left span9" style="padding-top: 13px">
				<strong><?= Yii::t('course', 'Manage Power Users Seats') ?></strong> <?= Yii::t('course', 'for course') ?>: <?=$course->name ?>
			</div>
			<div class="pull-right span3 input-wrapper">
				<?= CHtml::textField('filter', '',
					array('id' => 'filter_pus')) ?>
				<span class="search-icon" style="display: block;"></span>
			</div>
		</div>
	</div>
	<hr class="header-hr" />

	<div class="bottom-section border-bottom-section clearfix">
		<div id="grid-wrapper">
			<?php

			$_columns = array();

			$_columns[] = array(
				'name' => 'userid',
				'value' => 'substr($data["userid"],1)'//'Yii::app()->user->getRelativeUsername($data->userid)',
			);
			$_columns[] = array(
				'name' => 'firstname',
				'header' => Yii::t('standard', '_FIRSTNAME'),
				'value' => '$data["firstname"]',
			);
			$_columns[] = array(
				'name' => 'lastname',
				'header' => Yii::t('standard', '_LASTNAME'),
				'value' => '$data["lastname"]',
			);
			$_columns[] = array(
				'type' => 'raw',
				'header' => Yii::t('course', 'Used seats'),
				'value' => function($data) use ($course) {
					echo CoreUserPuCourse::renderSeats($course->idCourse, $data['idst'], 'used_seats', true);
				},
				'htmlOptions' => array('class' => 'center-aligned power-users-used-seats'),
			);

			$_columns[] = array(
				'type' => 'raw',
				'header' => Yii::t('audit_trail', 'Purchased seats'),
				'value' => function($data) use ($course) {
					echo CoreUserPuCourse::renderSeats($course->idCourse, $data['idst'], 'purchased_seats', true);
				},
				'htmlOptions' => array('class' => 'center-aligned power-users-purchased-seats'),
			);

			$_columns[] = array(
				'type' => 'raw',
				'value' => function($data) use ($course) {
					$currentValue = CoreUserPuCourse::renderExtraSeats($course->idCourse, $data['idst'], true);
					echo '<input value="' . $currentValue . '" type="text" style="display: none;width: 30px;" name="extra_seats['
						. $data['idst'] .']" id="extra_seats' . $data['idst'] .'" class="hide-on-edit" />';
					echo '<span id="extra_seats_show' . $data['idst'] . '" class="show-on-edit">' . $currentValue . '</span>';
				},
				'htmlOptions' => array('class' => 'power-users-extra-seats center-aligned'),
				'header' => Yii::t('course', 'Extra Seats'),
				'headerHtmlOptions' => array('class' => 'center-aligned'),
			);
			$_columns[] = array(
				'type' => 'raw',
				'value' => function($data) use ($course) {
					echo '<span class="green-text"><strong>';
					echo CoreUserPuCourse::renderTotalSeats($course->idCourse, $data['idst'], true);
					echo '</strong></span>';
				},
				'htmlOptions' => array('class' => 'power-users-total-seats center-aligned'),
				'header' => Yii::t('course', 'Total Seats'),
				'headerHtmlOptions' => array('class' => 'center-aligned'),
			);
			$_columns[] = array(
				'type' => 'raw',
				'value' => function($data) {
					echo Chtml::link(
						'<span class="i-sprite is-edit"></span>','#'.$data['idst'],
						array(
							'id' => $data['idst'],
							'class' => 'do-edit'
						)
					);
					echo '<a id="edit'.$data['idst'].'" href="javascript:;" class="green-text apply-edit hidden_" style="display: none;"><i class="fa fa-check fa-lg"></i></a>';
				},
				'htmlOptions' => array(
					'class' => 'power-users-extra-seats-edit'
				),
				'headerHtmlOptions' => array('class' => 'center-aligned'),
			);

			// Render the PU List here
			$this->widget('DoceboCGridView', array(
				'id' => 'power-user-seats-management-grid',
				'htmlOptions' => array('class' => 'grid-view clearfix'),
				'dataProvider' => $coreUserModel->dataProviderPowerUser($course->idCourse, true), // Fixme exclude non-seat-managers and with no seats!!!
				'template' => '{items}{summary}{pager}',
				'summaryText' => Yii::t('standard', '_TOTAL'),
				'pager' => array(
					'class' => 'ext.local.pagers.DoceboLinkPager',
				),
				'ajaxUpdate' => 'power-user-seats-management-grid',
				'afterAjaxUpdate' => 'function(id, data){

					$(document).controls();
					//applyDoEditListeners(); // fixme not working as not defined !!!
					// Set edit row extra seats on click event listeners
					$(".do-edit").each(function(index, elem) {
						$("#"+elem.id).on("click", function(){
							// Show extra seats as plain text
							$(".show-on-edit").show();

							// Hide any shown extra seats text fields
							$(".hide-on-edit").hide();

							// Reset not saved edit extra seats
							$(".hide-on-edit").each(function(index2, elem2) {
								// Get current power user row id
								var str = elem.id;

								// Get the exact PU id
								var currentId = str.replace("extra_seats", "");

								// Reset the input value
								$("#extra_seats"+currentId).val($("#extra_seats_show"+currentId).text());
							});

							// Hide apply icon
							$(".apply-edit").hide();

							// Show edit icon
							$(".do-edit").show();

							// Hide exact row edit icon
							$("#"+elem.id).hide();

							// Show exact row apply icon
							$("#edit"+elem.id).show();

							// Hide Extra Seats number in the column
							$("#extra_seats_show"+elem.id).hide();

							// Show Extra Seats input box in the column
							$("#extra_seats"+elem.id).show();

							// Add event on Apply button click
							$("#edit"+elem.id).on("click", function() {
								$("#seats-management-form").submit();
							});

							return false;
						});
					});
				}',
				'columns' => $_columns,
				'cssFile' => false

			));

			?>
		</div>
	</div>

	<div class="form-actions">
		<?= CHtml::button(Yii::t('standard', '_CLOSE'), array(
			'class' => 'btn close-btn close-dialog'
		)) ?>

	</div>
</div>

<?php
// end the form
$this->endWidget();
?>

<script type="text/javascript">
	(function ($) {
		$(function () {
			// NOTE: this will be moved in a separate js
			$(document).controls();

			// Load FcBkAutoComplete here
			$('#choose_pus').fcbkcomplete(
				{
					json_url: "<?=Docebo::createAdminUrl("courseManagement/coursePuAutocomplete&idCourse=".($course->idCourse)) ?>",
					width: '221px',
					height: '28px',
					complete_text: '',
					cache: true,
					filter_selected: true
				}
			);

			function applyDoEditListeners() {
				// Set edit row extra seats on click event listeners
				$('.do-edit').each(function(index, elem) {
					$('#'+elem.id).on('click', function(){
						// Show extra seats as plain text
						doShowOnEdit();

						// Hide any shown extra seats text fields
						doHideOnEdit();

						// Reset not saved edit extra seats
						$('.hide-on-edit').each(function(index2, elem2) {
							// Get current power user row id
							var str = elem.id;

							// Get the exact PU id
							var currentId = str.replace("extra_seats", "");

							// Reset the input value
							$('#extra_seats'+currentId).val($('#extra_seats_show'+currentId).text());
						});

						// Hide apply icon
						$('.apply-edit').hide();

						// Show edit icon
						$('.do-edit').show();

						// Hide exact row edit icon
						$('#'+elem.id).hide();

						// Show exact row apply icon
						$('#edit'+elem.id).show();

						// Hide Extra Seats number in the column
						$('#extra_seats_show'+elem.id).hide();

						// Show Extra Seats input box in the column
						$('#extra_seats'+elem.id).show();

						// Add event on Apply button click
						$('#edit'+elem.id).on('click', function() {
							$("#seats-management-form").submit();
						});

						return false;
					});
				});
			}

			// Apply onClick event listeners here
			applyDoEditListeners();

			// Hide extra seats textfields in the extra seats column
			function doHideOnEdit() {
				$('.hide-on-edit').hide();
			}

			// Show extra seats number in the extra seats column
			function doShowOnEdit() {
				$('.show-on-edit').show();
			}

			// Apply PU filter
			$('#filter_pus').on('keyup', function(){
				// Prepare filter by data array
				var options = {
					'data': {'filter': $('#filter_pus').val()}
				};

				// do gridView update with the data to filter
				$.fn.yiiGridView.update("power-user-seats-management-grid", options);
			});

			$('.search-icon').on('click', function() {
				// Prepare filter by data array
				var options = {
					'data': {'filter': $('#filter_pus').val()}
				};

				// do gridView update with the data to filter
				$.fn.yiiGridView.update("power-user-seats-management-grid", options);
			});

		});
	})(jQuery);
</script>
