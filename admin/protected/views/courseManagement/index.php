<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', '_COURSES'),
); ?>

<?php $this->renderPartial('//common/_mainCourseActions'); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'course-management-form',
	'htmlOptions' => array(
		'class' => 'ajax-grid-form',
		'data-grid' => '#course-management-grid'
	),
)); ?>

<div class="main-section">

	<div class="filters-wrapper">
		<table id="course-filter-id" class="filters filters_course_management">
			<tbody>
			<tr>
				<td class="table-padding">
					<table class="table-border">
						<tr>
							<td class="group children-node-content">
								<table>
									<tr>
										<td><?php echo Yii::t('standard', '_INHERIT'); ?></td>
										<td class="yes-no">
											<?php echo Yii::t('standard', '_YES'); ?><?php echo $form->checkbox($courseModel, 'containChildren', array('value' => 1)); ?><?php echo Yii::t('standard', '_NO'); ?>
										</td>
									</tr>
								</table>
							</td>
							<td class="group select-course-type">
								<table>
									<tr>
										<?php $courseFilters = array(
											LearningCourse::COURSE_TYPE_WAITING => Yii::t('standard', '_WAITING_USERS'),
											LearningCourse::COURSE_TYPE_ECOMMERCE => Yii::t('app', 'ecommerce'),
											LearningCourse::COURSE_TYPE_MARKETPLACE => Yii::t('menu_over', '_MARKETPLACE'),
											'enrolled_only' => Yii::t('menu_over', 'Courses with enrollments'),
										);

										$event = new DEvent($this, array('filters' => &$courseFilters));
										Yii::app()->event->raise('OnRenderCourseManagementFilters', $event);

										echo $form->checkboxList($courseModel, 'filterType', $courseFilters, array(
											'template' => "<td>{input}</td><td>{label}</td>",
											'container' => '',
											'separator' => ''
										));
										?>
									</tr>
								</table>
							</td>
							<td class="group advanced-search-box clearfix">
								<table>
									<tr>
										<td>
											<div class="input-wrapper">
												<input data-url="<?= Docebo::createAppUrl('admin:courseManagement/courseAutocomplete') ?>" class="typeahead"
													   id="advanced-search-course" autocomplete="off" type="text"
													   name="LearningCourse[name]"
													   placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"/>
												<!--<label for="advanced-search-course">Search course</label>   -->
												<span>
													<button class="search-btn-icon"></button>
												</span>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
</div>
<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<div id="tree-breadcrumbs" class="node-line"></div>

<div id="tree-wrapper" class="course"></div>
<div class="show-hide-tree hide-tree"><span></span><?= Yii::t('standard', '_COLLAPSE') ?></div>
<?php
$treeGenerationTime = '';//at the time of printing this view the tree has not been generated yet. It will be later in an ajax request
echo CHtml::hiddenField('tree-generation-time', $treeGenerationTime, array('id' => 'tree-generation-time'));
?>

<div class="bottom-section clearfix">
	<div id="grid-wrapper">
		<?php $this->renderPartial('_coursesGrid', $coursesGridParams); ?>
	</div>
</div>

<?php
if (!empty($showHints)) {
	$this->widget('OverlayHints', array('hints' => implode(',', $showHints)));
}
?>
<?php
if ($openModal == 'edit' && $idCourse){

			echo $this->renderPartial( 'admin.protected.views.common._modal', array(
					'config' => array(
							'iconCls' => 'node-edit',
							'class' => 'new-course node-edit',
							'modalTitle' => Yii::t('standard', '_MOD'),
							'linkTitle' => '<div class="row-fluid">
								<div class="span2 font-awesome"><i class="fa fa-pencil-square-o" ></i></div>
								<div class="span10">
									<strong>'.Yii::t('course', 'Course properties').'</strong>
									<p>'.Yii::t('course', 'Edit course <strong>type, code, title, description</strong> and <strong>description</strong>').'</p>
								</div>
								<div class="clearfix"></div>
							</div>',
							'url' => Docebo::createAbsoluteUrl('admin:courseManagement/updateCourse',array('id'=>$idCourse)),
							'buttons' => array(
									array(
											'type' => 'submit',
											'title' => Yii::t('standard', '_CONFIRM'),
											'formId' => 'course_form',
									),
									array(
											'type' => 'cancel',
											'title' => Yii::t('standard', '_UNDO'),
									),
							),
							'afterLoadingContent' => 'courseEditFormInit();',
							'beforeSubmit' => 'beforeCourseCreateSubmit',
							'afterSubmit' => 'updateCourseContent',
							'linkOptions'	=>	array(
									'style' => 'cursor: pointer;display:none;',
									'id'	=> 'courseEditModalAutoOpener'
							)
					),
			), true);

		 } ?>
<script type="text/javascript">
	function resetSearchPlaceholder() {
		var $search = $('#advanced-search-course');
		if ($search.val() == "<?= Yii::t('standard', '_SEARCH'); ?>")
			$('#advanced-search-course').val('');
	}

	function UpdateClassroomCells() {
		<?
		$puWithClassroomSessionAccess = (Yii::app()->user->getIsPu() && (Yii::app()->user->checkAccess('/lms/admin/classroomsessions/view')));
		if (PluginManager::isPluginActive('ClassroomApp') && (Yii::app()->user->getIsGodAdmin() || Yii::app()->user->checkAccess('/lms/admin/course/mod') || $puWithClassroomSessionAccess)): ?>
		$("td.classroom-substitution-main").each(function (index, element) {
			var id = $(this).find('span[id^=tmp-classroom-]')[0].id.replace('tmp-classroom-', '');
			var text = '<?=Yii::t('classroom', 'Sessions Management')?>';
			var url = '<?=Docebo::createAppUrl('lms:player/training/session', array('ref_src' => 'admin'))?>&course_id='+id;
			$(this).html('<a class="sessions" href="'+url+'">'+text+'</a>');
            <?php if ($showLPColumn): ?>
			$(this).attr('colspan', 2);
			$('td.lp-substitution-main').attr('colspan', 2);
            <?php else: ?>
            $(this).attr('colspan', 3);
            <?php endif; ?>
		});
		$("td.classroom-substitution-del").each(function (index, element) {
			$(this).remove();
		});

		<?php endif; ?>
	}

	function UpdateWebinarCells()
	{
		<?
		$puWithWebinarSessionAccess = (Yii::app()->user->getIsPu() && (Yii::app()->user->checkAccess('/lms/admin/webinarsessions/view')));
		if (Yii::app()->user->getIsGodAdmin() || Yii::app()->user->checkAccess('/lms/admin/course/mod') || $puWithWebinarSessionAccess): ?>
		$("td.webinar-substitution-main").each(function (index, element) {
			var id = $(this).find('span[id^=tmp-webinar-]')[0].id.replace('tmp-webinar-', '');
			var text = '<?=Yii::t('classroom', 'Sessions Management')?>';
			var url = '<?=Docebo::createAppUrl('lms:player/training/webinarSession')?>&course_id='+id;
			$(this).html('<a class="sessions" href="'+url+'">'+text+'</a>');
            <?php if ($showLPColumn): ?>
            $(this).attr('colspan', 2);
            $('td.lp-substitution-main').attr('colspan', 2);
            <?php else: ?>
            $(this).attr('colspan', 3);
            <?php endif; ?>
		});
		$("td.webinar-substitution-del").each(function (index, element) {
			$(this).remove();
		});

		<?php endif; ?>
	}

	<?= $customCourseTypeJsCode ?>

	function checkIfModalLoaded(){
		if (typeof $('a#courseEditModalAutoOpener').showModal === 'undefined')
			setTimeout(checkIfModalLoaded, 200);
		else
			$('a#courseEditModalAutoOpener').trigger('click');
	}


	function courseEditFormInit() {
		var modal = $('.modal.in');
		modal.find('.select-user-form-wrapper .item > div').live('click', function () {
			modal.find('.select-user-form-wrapper .item > div').removeClass('checked');
			// $('.thumbnailSlider .sub-item').removeClass('checked');

			$(this).addClass('checked');

			// $(this).find('input[type="radio"]').trigger('click');
			$(this).find('input[type="radio"]').prop('checked', true);
			$(this).parent().find('.jq-radio').removeClass('checked');
			$(this).find('.jq-radio').addClass('checked');
		});

		if (!tinymce.editors[modal.find('textarea').attr('id')]) {
			initTinyMCE('#' + modal.find('textarea').attr('id'), 150);
		}

	}


	(function ($) {
		$(function () {
			$(document).controls();
            <?php if ($tooltip): ?>
            //NOTE: this is to fix removing the tooltip attributes from the links
            if($('a.lps-list').length) {
                $('a.lps-list').attr('rel', 'tooltip');
                $('a.lps-list').attr('onclick', 'javascript:;');
                $('a.lps-list').attr('trigger', 'click hover focus');
                $('a[rel="tooltip"]').tooltip({'html': 'true', delay: { "show": 0, "hide": 1000}});
            }

            <?php endif; ?>

			$('input, select').styler();
			$('.select-course-type .jq-checkbox, .children-node-content .jq-checkbox').click(function () {
				$('#course-management-form').submit();
			});
			replacePlaceholder();
			showHideTree();

			UpdateClassroomCells();
			UpdateWebinarCells();

			<? if (isset($_GET['enrollUsers']) && $_GET['enrollUsers'] == 1) : //if you need to open automatically the enrollment dialog: ?>
				if ($('.course-enrollment').length) {
					$('.course-enrollment').trigger('click');
				}
			<? endif; ?>
			<?= $customCourseTypeJsCall ?>
		});

		$(document).on('dialog2.content-update', '.modal.course-buy-seats', function(){
			var autoCloseAndRedirect = $(this).find('.auto-close.redirect');

			if(autoCloseAndRedirect.length > 0){
				var href = autoCloseAndRedirect.attr('href');
				window.location = href;
			}
		});
		<?php
		if ($openModal == 'edit' && $idCourse) {?>
		checkIfModalLoaded();

		<?php }?>
	})(jQuery);

</script>
