<?php
/** @var $course LearningCourse */
/** @var $showDeadlineSelector boolean */
/** @var $iframeField LearningEnrollmentFieldIframe */
/** @var $model LearningCourseuser */
?>

<h3><?php echo Yii::t('course_management', '_ENROLLMENT_EDIT', array($model->user->fullName)); ?></h3>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'enrollment_form',
	)); ?>

	<div class="clearfix">
		<?php echo $form->dropDownList($model, 'level', $userLevels); ?>
		<?php echo $form->label($model, 'level'); ?>
	</div>

	<div class="clearfix">
		<?php
		$statuses = LearningCourseuser::getStatusesArray();
		if (Yii::app()->user->getIsPu()) {
			unset($statuses[LearningCourseuser::$COURSE_USER_CONFIRMED]);
			unset($statuses[LearningCourseuser::$COURSE_USER_WAITING_LIST]);
		}
		?>
		<?php echo $form->error($model, 'status'); ?>
		<?php echo $form->dropDownList($model, 'status', $statuses); ?>
		<?php echo $form->label($model, 'status'); ?>
	</div>

    <fieldset class="forced-values">
        <div class='input-append date clearfix' data-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>' data-date-language='<?= Yii::app()->getLanguage() ?>'>
            <div>
                <?php echo CHtml::textField('forced_completion_details[date_complete]', ($model->date_complete != '0000-00-00 00:00:00' ? Yii::app()->localtime->stripTimeFromLocalDateTime($model->date_complete) : ''), ['placeholder' => Yii::t('standard', 'Set automatically')]); ?>
                <span class="add-on"><i class="icon-calendar"></i></span>
                <?php echo CHtml::link(Yii::t('standard', '_RESET'), 'javascript:void(0);', array(
                    'class' => 'clear-input',
                    'data-input-id' => chtml::activeId($model, 'date_complete'),
                )); ?>
            </div>
            <?php echo $form->label($model, 'date_complete'); ?>
        </div>

        <div class="input-append block clearfix">
            <div>
                <?php
                $scoreFieldParams = [
                    'min' => '0',
                    'step' => '0.01',
                    'id' => 'LearningCourseuser_score_given',
                    'placeholder' => 'Calculate automatically',
                    'disabled' => ((boolean) $model->forced_score_given) ? null : 'disabled',
                    'required' => 'required',
                ];
                ?>
                <?php echo CHtml::numberField('forced_completion_details[score_given]', $model->score_given, $scoreFieldParams) ?>
            </div>
            <?php echo $form->label($model, 'score_given'); ?>
        </div>

        <div class="input-append block clearfix">
            <div>
                <label class="self-containing-checkbox">
                    <?php echo CHtml::checkBox('forced_completion_details[forced_score_given]', ((boolean) $model->forced_score_given), [
                        'data-enable-target' => '#LearningCourseuser_score_given',
                        'data-enable-box' => '1',
                    ]) ?>
                    <?php echo Yii::t('course_management', 'Set score manually'); ?>
                </label>
            </div>
        </div>
    </fieldset>

    <?php if(!$showDeadlineSelector)
        $this->widget('common.widgets.warningStrip.WarningStrip', array('message'=>Yii::t('standard','You cannot set enrollment deadlines for the selected users because the course end date has expired on {date}.',array('{date}'=>$course->date_end)),'type'=>'warning',));
    else {
    ?>
		<div class='input-append date clearfix' data-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>' data-date-language='<?= Yii::app()->getLanguage() ?>'>
			<div>
				<?php echo CHtml::textField(CHtml::activeName($model, 'date_begin_validity'), ($model->date_begin_validity != '0000-00-00 00:00:00' ? Yii::app()->localtime->stripTimeFromLocalDateTime($model->date_begin_validity) : '')); ?>
				<span class="add-on"><i class="icon-calendar"></i></span>
				<?php echo CHtml::link(Yii::t('standard', '_RESET'), 'javascript:void(0);', array(
				'class' => 'clear-input',
				'data-input-id' => chtml::activeId($model, 'date_begin_validity'),
			)); ?>
			</div>
			<?php echo $form->label($model, 'date_begin_validity'); ?>
		</div>

		<div class='input-append date clearfix' data-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>' data-date-language='<?= Yii::app()->getLanguage() ?>'>
			<div>
				<?php echo CHtml::textField(CHtml::activeName($model, 'date_expire_validity'), ($model->date_expire_validity != '0000-00-00 00:00:00' ? Yii::app()->localtime->stripTimeFromLocalDateTime($model->date_expire_validity) : '')); ?>
				<span class="add-on"><i class="icon-calendar"></i></span>
				<?php echo CHtml::link(Yii::t('standard', '_RESET'), 'javascript:void(0);', array(
				'class' => 'clear-input',
				'data-input-id' => chtml::activeId($model, 'date_expire_validity'),
			)); ?>
			</div>
			<?php echo $form->label($model, 'date_expire_validity'); ?>
		</div>
    <?php }?>

	<?php echo $form->error($model, 'enrollment_fields'); ?>
	<?php echo $form->hiddenField($model, 'enrollment_fields', $statuses); ?>

	<?php if (empty($enrollment_fields) === false) : ?>
		<div class="enrollment-fields-values">
		<?php
			$modelValues = json_decode($model->enrollment_fields, true);
			foreach($enrollment_fields as $field){
				$mandatory = $field['mandatory'] == 1 ? '*' : '';
				if($field['type'] == 1){?>
					<div class='input-append date clearfix' data-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>' data-date-language='<?= Yii::app()->getLanguage() ?>'>
						<div>
							<?php echo CHtml::textField('field-'.$field['id'], ($modelValues[$field['id']] && $modelValues[$field['id']] != '0000-00-00') ? Yii::app()->localtime->toLocalDate($modelValues[$field['id']]) : ''); ?>
							<span class="add-on"><i class="icon-calendar"></i></span>
							<?php echo CHtml::link(Yii::t('standard', '_RESET'), 'javascript:void(0);', array(
								'class' => 'clear-input',
								'data-input-id' => 'field-'.$field['id'],
							)); ?>
						</div>
						<label><?php echo $field['title'].$mandatory; ?></label>
					</div>
				<?php }elseif($field['type'] == 2){  ?>
					<div  class="clearfix">
						<?php
							echo CHtml::dropDownList('field-'.$field['id'], $modelValues[$field['id']], $field['options'] );
							echo CHtml::label(Yii::t('standard', $field['title'].$mandatory), 'select-level-input');
						?>
					</div>
				<?php }elseif($field['type'] == 3){?>
					<div class="clearfix">
						<?php
							echo CHtml::textField('field-'.$field['id'], $modelValues[$field['id']]);
							echo CHtml::label(Yii::t('standard', $field['title'].$mandatory), 'field-'.$field['id']);
						?>
				</div>
				<?php }elseif($field['type'] == 5){?>
					<?php
				    	// for now, we only display/save the value of IFRAME fields
				    	echo CHtml::hiddenField("field-".$field['id'], json_encode($modelValues[$field['id']])); 
					?>
					<?php // SKIP IFRAME render for now ?>
					<?php if (false) : ?> 
						<?php $iframeField = LearningEnrollmentFieldIframe::model()->findByPk($field['id']); ?>
						<div class="clearfix">
							<?php
					   			// Array of enrollment(s) information passed down to be used/shown in enrollments related UI
						   		// Like: Edit single enrollment, mass enrollment oprations and so on
						   		$enrollments[] = $iframeField->prepareSingleEnrollmentInfo($model);
							?>
							<div class="iframe-field"><?php echo $iframeField->renderIframe($enrollments) ?></div>
							<?php echo CHtml::label(Yii::t('standard', $field['title'].$mandatory), 'field-'.$field['id']); ?>
						</div>
					<?php endif; ?>
				<?php }else{ ?>
					<div class="clearfix">
						<?php
							echo CHtml::textArea('field-'.$field['id'], $modelValues[$field['id']]);
							echo CHtml::label(Yii::t('standard', $field['title'].$mandatory), 'field-'.$field['id']);
						?>
					</div>
				<?php } ?>
			<?php } ?>
		</div>
	<?php endif ?>

	<?php $this->endWidget(); ?>
</div>

	<div class="modal-footer">
		<input class="btn confirm-btn" type="button" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" name="submit" data-submit-form="enrollment_form"/>
		<input class="btn close-btn" type="button" value="<?php echo Yii::t('standard', '_CLOSE'); ?>"/>
	</div>
<script type="text/javascript">
	$('select').styler();
    $('#popup .date').bdatepicker('setStartDate', <?='"'.(($course->date_begin && $course->date_begin!='0000-00-00')?$course->date_begin:'').'"';?>);
    $('#popup .date').bdatepicker('setEndDate', <?='"'.(($course->date_end && $course->date_end!='0000-00-00')?$course->date_end:'').'"';?>);

	// Completion fields (completion date and forced score)
	function updateCompletionFieldAccessability()
	{
		var statusDropdown = $('#LearningCourseuser_status'),
            form = statusDropdown.parents('form'),
			completionFields = form.find('.forced-values'),
			currentStatus = statusDropdown.val(),
			completedStatus = 2;

		if (currentStatus == completedStatus) {
			// Enable completion fields
			completionFields.removeAttr('disabled').show(0).find('.jq-checkbox').removeClass('disabled');
		} else {
			// Disable completion fields
			completionFields.attr('disabled', 'disabled').hide(0);
		}
	}
	updateCompletionFieldAccessability();

    var completionFields = $('fieldset.forced-values'),
        statusSwitch = $('#LearningCourseuser_status'),
        enableToggleSwitch = completionFields.find('input[data-enable-box="1"]');
    ;
    statusSwitch.change(updateCompletionFieldAccessability);
    enableToggleSwitch.each(function() {
        $(this).change(function() {
            var thisSwitch = $(this),
                form = thisSwitch.parents('form'),
                inputSelector = thisSwitch.data('enable-target'),
                toggleTarget = form.find(inputSelector);

            if (!toggleTarget.length) {
                console.warn('Availability toggle target with selector "' + inputSelector + '" could not be found!');
            }

            if (thisSwitch.prop('checked') == false) {
                toggleTarget.attr('disabled', 'disabled');
            } else {
                toggleTarget.removeAttr('disabled');
            }
        });
    });
</script>
