<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'course_form',
	)); ?>
	<div class="clearfix">
		<p>
			<?php echo Yii::t('standard', '_COURSE_NAME') .': "' . Docebo::ellipsis($model->name, 30, '...') . '"'; ?>
		</p>
		<?php echo $form->checkbox($model, 'confirm', array('id' => 'LearningCourse_confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('course', 'I understand that deleting this course also deletes all related material and tracking data'), 'LearningCourse_confirm_'.$checkboxIdKey); ?>
		<?php echo $form->error($model, 'confirm'); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>