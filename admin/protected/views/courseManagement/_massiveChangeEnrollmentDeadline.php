<?php
/** @var $course LearningCourse */
/** @var $initialDateBegin string */
/** @var $initialDateEnd string */
/** @var $showDeadlineSelector boolean */
?>

<?php if($showDeadlineSelector) echo Yii::t('standard','Leave blank for no deadline'); ?>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'massive-change-enrollment-deadline-form'
    )); ?>

    <?= CHtml::hiddenField('confirm', 1, array('id' => 'confirm-input')) ?>
    <?= CHtml::hiddenField('id-list', implode(',',$ids), array('id' => 'id-list-input')) ?>

    <?php if(!$showDeadlineSelector)
        $this->widget('common.widgets.warningStrip.WarningStrip', array('message'=>Yii::t('standard','You cannot set enrollment deadlines for the selected users because the course end date has expired on {date}.',array('{date}'=>$course->date_end)),'type'=>'warning',));
    else {
    ?>
    <div class='input-append date clearfix' data-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>' data-date-language='<?= Yii::app()->getLanguage() ?>'>

        <?php

        // Should we outline the datepicker inputs in order to show which date caused the warning
        $outlineDateBeginClass  = ($outlineDateBegin) ? ' outline-datepicker' : '';
        $outlineDateEndClass    = ($outlineDateEnd) ? ' outline-datepicker' : '';

        $this->widget('common.widgets.DatePicker', array(
            'id' => 'massive-change-enrollment-deadline-start-date',
            'label' => CHtml::activeLabel(LearningCourseuser::model(), 'date_begin_validity',array('id'=>'date_begin_validity-label')),
            'fieldName' => CHtml::activeName(LearningCourseuser::model(), 'date_begin_validity'),
            'value' => $initialDateBegin,
            'htmlOptions' => array(
                'id' => CHtml::activeId(LearningCourseuser::model(), 'date_begin_validity_mass'),
                'class' => 'datepicker' . $outlineDateBeginClass,
                'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')),
                'data-date-language' => Yii::app()->getLanguage(),
            ),
            'pickerStartDate'	=> ($course->date_begin && $course->date_begin!='0000-00-00')?$course->date_begin:'',
            'pickerEndDate'     => ($course->date_end && $course->date_end!='0000-00-00')?$course->date_end:'',
        ));
        ?>
    </div>
    <div class='input-append date clearfix' data-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>' data-date-language='<?= Yii::app()->getLanguage() ?>'>

        <?php
        $this->widget('common.widgets.DatePicker', array(
            'id' => 'massive-change-enrollment-deadline-end-date',
            'label' => CHtml::activeLabel(LearningCourseuser::model(), 'date_expire_validity',array('id'=>'date_expire_validity-label')),
            'fieldName' => CHtml::activeName(LearningCourseuser::model(), 'date_expire_validity'),
            'value' => $initialDateEnd,
            'htmlOptions' => array(
                'id' => CHtml::activeId(LearningCourseuser::model(), 'date_expire_validity_mass'),
                'class' => 'datepicker' . $outlineDateEndClass,
                'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')),
                'data-date-language' => Yii::app()->getLanguage(),
            ),
            'pickerStartDate'	=> ($course->date_begin && $course->date_begin!='0000-00-00')?$course->date_begin:'',
            'pickerEndDate'     => ($course->date_end && $course->date_end!='0000-00-00')?$course->date_end:'',
        ));
        ?>
    </div>
    <?php }?>

    <?php if ($showMultipleDeadlinesAssignedWarning) { ?>
        <div class="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong><?= Yii::t('standard', '_WARNING'); ?>:</strong> <i><?= Yii::t('standard','Multiple deadlines assigned'); ?></i>
        </div>
    <?php } ?>

    <?php $this->endWidget(); ?>
</div>

<script type="text/javascript">
    $(function(){
        <?php if(!$showDeadlineSelector) echo "$('.btn-submit').remove();"; ?>
    })
</script>