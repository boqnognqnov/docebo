<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'course_form',
	));?>

	<p><?php echo Yii::t('course_management', 'Approve {count} users(s) from course', array('{count}' => count($courseUser))); ?></p>

	<div class="clearfix">
		<?php echo $form->checkbox($model, 'confirm', array('id' => 'LearningCourseuser_confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed', array(count($courseUser), '{username}' => $courseUser[0]->user->fullname)), 'LearningCourseuser_confirm_'.$checkboxIdKey); ?>
		<?php echo $form->error($model, 'confirm'); ?>
	</div>

	<?php foreach ($courseUser as $user) {
		echo CHtml::hiddenField('ids[]', $user->idUser);
	} ?>
	<?php $this->endWidget(); ?>
</div>