<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAdminUrl('dashboard/index'),
	Yii::t('standard', '_COURSES') => array('courseManagement/index'),
	Yii::t('help', 'Import ILT sessions'),
); ?>

<?php //$this->renderPartial('/common/_mainActions'); ?>

<div class="main-section import">

	<?php 
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'session-import-form-step2',
		)); 
	?>

	<h3 class="title-bold"><?php echo Yii::t('organization_chart', '_IMPORT_MAP'); ?></h3>

	<?php echo $form->errorSummary($model); ?>

	<?php
		$this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'session-management-import-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
			'dataProvider' => $dataProvider,
			'enablePagination' => false,	
			'columns' => $gridColumns,
			'template' => '<div class="table-scroll">{items}</div>',
			'summaryText' => Yii::t('standard', '_TOTAL'),
		)); 
	?>

	<div class="row-fluid">
		<?php
			echo Yii::t('standard', '{N} sessions to be imported. Displaying few of them for review only', array('{N}' => $model->getTotalRowCount()));
		?>
	</div>

	<div class="row btns">
		<?php 
			echo CHtml::link(Yii::t('standard', '_CANCEL'), array('courseManagement/index'), array('class' => 'btn close-btn'));
			 
			$progressUrl = Docebo::createAbsoluteLmsUrl('progress/run', array(
				'type' => Progress::JOBTYPE_IMPORT_SESSIONS,
			));
        	echo CHtml::link(Yii::t('standard', 'Submit'), $progressUrl, array(
        		'rel'				=> 'progressive-import-dialog',
        		'class' 			=> 'open-dialog btn confirm-btn',
        		'data-ajaxType' 	=> 'POST',	// see events below!
        		'data-dialog-title'	=> Yii::t('help', 'Import ILT sessions'),
        	))
		?>
	</div>

	<?php $this->endWidget(); ?>
</div>


<script type="text/javascript">
	
	(function ($) {
		$(function () {
            $('input, select').styler();
            autoSelectWidth();
			$('.table-scroll').jScrollPane({
				autoReinitialise : true
			});
		});

		//Add additional data to the importing, such as should the record be updated or not
		$(document).on('dialog2.before-send', '#progressive-import-dialog', function(e, xhr, settings) {
			settings.data = $('form').serialize()
		});

	})(jQuery);
	
</script>
