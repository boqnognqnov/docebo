<form id="iframe-enrollment-field-mass-form" method="POST">
	<div class="iframe-field"><?php echo $iframeField->renderIframe($enrollments) ?></div>
	<input type="hidden" name="fieldId" value="<?= $iframeField->id ?>" />
	<input type="hidden" name="courseId" value="<?= $courseId ?>" />
	<input type="hidden" name="sessionId" value="<?= $sessionId ?>" />
	<input type="hidden" name="data_submitted" value="1" />
	<input type="hidden" name="ids" value='<?= implode(",",$userIds) ?>' />
</form>
