<?
$event = new DEvent($this, array('course'=>$data));
Yii::app()->event->raise('onRenderSessionPickerMassEnrollment', $event);

if(!$event->shouldPerformAsDefault()){
	// Don't render this View (the raised event already handled the rendering)
	return;
}
?>

<?php
/* @var $data LearningCourse */
?>
<div class="item" data-course-id="<?= $data->idCourse ?>">
    <div class="item-primary clearfix">
        <div class="item-col col-toggle-handle">
            <?= CHtml::tag('span', array('class'=>'toggle-handle docebo-sprite ico-arrow-bottom', 'data-expand-class'=>'ico-arrow-top', 'data-retract-class'=>'ico-arrow-bottom')) ?>
        </div>
        <div class="item-col col-session">
            <div class="empty"><em><?= Yii::t('classroom', 'No session selected') ?></em></div>
            <div class="details"></div>
        </div>
        <div class="item-col col-coursename"><?= $data->name ?></div>
    </div>
    <form class="item-secondary">
        <?= CHtml::label(
            CHtml::radioButton('enrollCourse['.$data->idCourse.'][option]',false,array(
                'id'=>'enroll-option-'.$data->idCourse.'-session',
                'class'=>'open-session-grid'
            ), 'session') . ' ' . Yii::t('classroom', 'I want to enroll user(s) <strong>to a session</strong>'),
            'enroll-option-'.$data->idCourse.'-session', array('class'=>'radio')) ?>
        <?= CHtml::label(
            CHtml::radioButton('enrollCourse['.$data->idCourse.'][option]', true, array(
                'id'=>'enroll-option-'.$data->idCourse.'-course',
                'class'=>'close-session-grid'
            ), 'course') . ' ' . Yii::t('classroom', 'I want to enroll user(s) <strong>to the course</strong>'),
            'enroll-option-'.$data->idCourse.'-course',  array('class'=>'radio')) ?>
    </form>
    <div class="item-tertiary">
        <?php
        $this->widget('root.common.widgets.SessionGrid', array(
            'course'=>$data,
            'title'=>Yii::t('classroom', 'Select session')
        ));
        ?>
    </div>
</div>