<?php
/* @var $form CActiveForm */

// don't allow PU to enroll users to a course, that belongs to one or more learning plans
$disallowPUEnrollCoursesWithLPs = 0; //default allow PU to enroll users to courses, that belongs to one or more LPs
$pUserRights = Yii::app()->user->checkPURights($courseModel->idCourse);
$canEnroll = (Yii::app()->user->isGodAdmin || ($pUserRights->all && Yii::app()->user->checkAccess('enrollment/view')));

Yii::app()->event->raise('OnCourseManagementEnrollmentRender',
    new DEvent($this, array('canEnroll' => &$canEnroll, 'disallowPUEnrollCoursesWithLPs' => &$disallowPUEnrollCoursesWithLPs, 'model' => $courseModel)));

$moreEnrollments = true;
if($courseModel->selling && CoreUserPU::isPUAndSeatManager()) {
	if(!CoreUserPuCourse::checkIfPuHasEnoughSeatsInCourse(1, $courseModel->idCourse))
		$moreEnrollments = false;
}

//in some cases PUs may have enrollment permission for courses, but not the view permission (weird, but possible)
$isPUWithNoCourseViewPermission = false;
if ($pUserRights->isPu && !Yii::app()->user->checkAccess('/lms/admin/course/view')) {
	$isPUWithNoCourseViewPermission = true;
}

$userIsSubscribed = LearningCourseuser::isSubscribed(Yii::app()->user->id, $courseModel->idCourse);
if(!$userIsSubscribed) {
    $this->breadcrumbs[Yii::t('menu', 'Admin')] = Docebo::createAppUrl('admin:dashboard/index');
    $this->breadcrumbs[Yii::t('standard', '_COURSES')] = Docebo::createAppUrl('admin:courseManagement/index');
} else {
    $this->breadcrumbs = array(
        Yii::t('menu_over', '_MYCOURSES') => Docebo::createLmsUrl('site/index'),
    );
}
$this->breadcrumbs[Docebo::ellipsis(Yii::t('player', $courseModel->name), 60, '...')] = Docebo::createLmsUrl('player', array('course_id' => $courseModel->idCourse));
$this->breadcrumbs[] = Yii::t('standard', 'Enrollments');

if ($moreEnrollments && $canEnroll) {
	$this->renderPartial('//common/_mainCourseEnrollmentActions', array('course' => $model->learningCourse, 'courseModel' => $courseModel));
} elseif($pUserRights->isPu && !$moreEnrollments && $canEnroll) {
	$this->renderPartial('//common/_mainCourseEnrollmentActions', array('course' => $model->learningCourse, 'courseModel' => $courseModel));
}

// Get list of enrollment fields of type IFRAME
$iframeEnrollmentFields = LearningEnrollmentField::getFieldsByCourse($courseModel->idCourse, LearningEnrollmentField::TYPE_FIELD_IFRAME);
// Build list of value->text array, used later in Mass Actions dropdown
$massIframeActions = array();
if (is_array($iframeEnrollmentFields)) {
    foreach ($iframeEnrollmentFields as $iframeField) {
        $massIframeActions[] = array(
            'value' => $iframeField['id'],
            'text'  => $iframeField['title'],
        );
    }
}
?>

<div class="bootstroEnrollUsers-hide-content">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'course-enrollment-form',
		'htmlOptions' => array('class' => 'ajax-list-form', 'data-grid' => '#course-enrollment-list'),
	)); ?>

<div class="main-section course-enrollments">

		<div class="filters-wrapper">
			<table class="filters">
				<tbody>
				<tr>
					<td class="table-padding">
						<table class="table-border">
							<tr>
								<td class="group" style="text-align: left; width: 458px;"><?php
									if(PluginManager::isPluginActive('CoachingApp') && $courseModel->enable_coaching) {
										echo "<strong>" . Yii::t('coaching', 'Coaching session') . "</strong>&nbsp;";
										$coachingSelectOptions = $courseModel->buildCoachingSessionFilteringArrayForEnrollments();
										$htmlOptions = array();
										echo $form->dropDownList($model, 'coachingSessionFilter', $coachingSelectOptions, $htmlOptions);
									}?>
								</td>
								
								<td class="group seats">
									<?php if ($courseModel->mpidCourse > 0 && ($courseModel->abs_max_subscriptions > 0)) : ?>
										<div></div>
										<p>
                                            <?php echo Yii::t('course_management', '_YOU_ARE_USING_AVAILABLE_SEATS', array('{takenSeats}' => $courseModel->takenSeats, '{abs_max_subscriptions}' => $courseModel->abs_max_subscriptions)) ?>
                                        </p>
										<?php if ( Yii::app()->user->getIsGodadmin()) : ?>
											<?php echo Yii::t('course_management', '_NEED_MORE_SEATS') ?> <?php echo CHtml::link(Yii::t('player', 'Go to the marketplace'), Docebo::createLmsUrl('mainmenu/marketplace')); ?>
										<?php endif;?>
									<?php endif; ?>
								</td>
								<td class="group" style="width: 450px;">
									<table>
										<tr>
											<td>
												<div class="input-wrapper">
													<input data-url="<?= Docebo::createAppUrl('admin:userManagement/usersAutocomplete') ?>" data-type="core" data-source-desc="true" data-course="<?php echo $model->learningCourse->idCourse; ?>" data-waiting="0" class="typeahead" id="advanced-search-user" autocomplete="off" type="text" name="CoreUser[search_input]" placeholder="<?php echo Yii::t('standard', '_SEARCH') ?>"/>
													<span class="search-icon"></span>
												</div>
											</td>
											<td>
												<table class="search-links">
													<tr>
														<td>
															<a href="javascript:void(0);" class="reset-search-link"><?php echo Yii::t('course_management', '_RESET_SEARCH') ?></a>
														</td>
													</tr>
													<tr>
														<td>
															<a href="javascript:void(0);" class="advanced-search-link"><?php echo Yii::t('standard', '_ADVANCED_SEARCH') ?></a>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="4" class="table-padding">
						<div id="advanced-search" style="display: none;">
							<div class="clearfix">
							</div>
							<div class=advanced-search-label>
								<h4><?php echo Yii::t('standard', '_ADVANCED_SEARCH') ?></h4>
							</div>
							<div class="enrollments-search-wrapper clearfix">
								<div class="enrollments-order-by">
									<label for="order_by"><?= Yii::t('social', 'Sort by'); ?>:</label>
									<?php echo CHtml::dropDownList('CoreUser[order_by]', false, array( Yii::t('standard', '_SELECT') ) + LearningCourseuser::getElearningEnrollmentsOrderList()); ?>
								</div>
                                <div class="enrollments-group-filter">
                                    <label><?php echo Yii::t('standard', 'Filter by group') ?>:</label>
                                    <?php
                                    $groups = CoreGroup::getGroupsList();
									if(empty($groups))
										$groups = array();
                                    $groups =   array_merge(array('-1'=>''),$groups);
                                    echo CHtml::dropDownlist('group', 0, CHtml::listData($groups, 'idst', 'gid'), array('encode' => false));
                                    ?>
                                </div>
								<div class="enrollments-branch-filter">
									<label><?php echo Yii::t('standard', 'Filter by branch') ?>:</label>
									<?php
										$orgChartsInfo = CoreOrgChartTree::getFullOrgChartTree(true, true, true);
										echo CHtml::dropDownlist('node', 1, CHtml::listData($orgChartsInfo['list'], 'idOrg', 'coreOrgChart.translation'), array('encode' => false));
									?>
								</div>
								<div class="descendants-checkbox">
									<?php echo $form->checkBox($model, 'containChildren'); ?>
									<label for="<?php echo CHtml::activeId($model, 'containChildren') ?>"><?php echo Yii::t('standard', '_INHERIT') ?></label>
								</div>

							</div>
							<div class="enrollments-search-wrapper deadline-filter clearfix">
								<div class="enrollments-deadline-filter">
									<div class="mainLabel"><?php echo Yii::t('course_management', 'Enrollment deadline:'); ?> </div>
										<?php echo $form->radioButtonList($model->learningCourse, 'deadline', array(
											LearningCourseuser::$DEADLINE_SELECT_ALL => Yii::t('standard', '_ALL'),
											LearningCourseuser::$DEADLINE_SELECT_WITHOUT => Yii::t('course_management', 'Without deadline only'),
											LearningCourseuser::$DEADLINE_DATE_RANGE => Yii::t('course_management', 'Deadline date range'),
										), array('template' => "<span class='radiobtn'>{input}</span> <span class='label'>{label}</span>", 'container' => '', 'separator' => '')); ?>
								</div>
								<div class="enrollments-date-range" style="display: none;">
									<span class="label"><?php echo Yii::t('standard', '_FROM'); ?></span>
									<div class="input-append date clearfix" data-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>' data-date-language='<?= Yii::app()->getLanguage() ?>'>
										<div>
											<?php echo CHtml::textField(CHtml::activeName($model->learningCourse, 'fromDate'), (Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:sO')))); ?>
											<span class="add-on"><i class="icon-calendar"></i></span>
										</div>
									</div>
									<span class="label"><?php echo Yii::t('standard', '_TO'); ?></span>
									<div class="input-append date clearfix" data-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>' data-date-language='<?= Yii::app()->getLanguage() ?>'>
										<div>
											<?php echo CHtml::textField(CHtml::activeName($model->learningCourse, 'toDate'), (Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:sO')))); ?>
											<span class="add-on"><i class="icon-calendar"></i></span>
										</div>
									</div>
								</div>
							</div>

						</div>
					</td>
				</tr>
				<tr>
					<td class="table-padding bottom-table">
						<table>
							<tr>
								<td>
									<table class="table-border">
										<tr>
											<td class="level-title">
												<span><strong><?php echo Yii::t('standard', '_LEVEL');?></strong></span>
											</td>
												<?php
													$levels = array(
														'' => Yii::t('standard', '_ALL'),
														LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR => Yii::t('levels', '_LEVEL_6'),
														LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR => Yii::t('levels', '_LEVEL_4'),
														LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT => Yii::t('levels', '_LEVEL_3')
													);

													if(PluginManager::isPluginActive('CoachingApp'))
														$levels[LearningCourseuser::$USER_SUBSCR_LEVEL_COACH] = Yii::t('levels', '_LEVEL_8');
												?>

												<?php echo $form->radioButtonList($model->learningCourse, 'level', $levels, array('template' => "<td>{input}</td><td>{label}</td>", 'container' => '', 'separator' => ''));?>

											<td class="status-title">
												<span><strong><?php echo Yii::t('standard', '_STATUS');?></strong></span>
											</td>
												<?php echo $form->radioButtonList($model->learningCourse, 'status', array(
													'' => Yii::t('standard', '_ALL'),
													LearningCourseuser::$COURSE_USER_SUBSCRIBED => Yii::t('standard', '_USER_STATUS_SUBS'),
													LearningCourseuser::$COURSE_USER_BEGIN => Yii::t('standard', '_USER_STATUS_BEGIN'),
													LearningCourseuser::$COURSE_USER_END => Yii::t('standard', 'Completed'),
												), array('template' => "<td>{input}</td><td>{label}</td>", 'container' => '', 'separator' => ''));
												?>

										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
	<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
	<?php $this->endWidget(); ?>

	<?php if ($canEnroll): ?>
	<div class="selections clearfix" data-grid="course-enrollment-list">
		<?php $this->widget('ext.local.widgets.ListViewSelectAll', array(
			'gridId' => 'course-enrollment-list',
			'class' => 'left-selections clearfix',
				
			// Stop using Active Data Provider	
			// 'dataProvider' => $model->dataProviderEnrollmentSelector(),
			'dataProvider' => $model->dataSqlProviderEnrollmentSelector(),
				
			'itemValue' => 'idst',
		)); ?>
		<div class="right-selections clearfix">
			<select name="massive_action" id="enrollments-massive-action">
				<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
				<?php if(Yii::app()->user->checkAccess('enrollment/update')) { ?>
				<option value="change-status"><?php echo Yii::t('standard', 'Change status'); ?></option>
                <option value="change-enrollment-deadline"><?php echo Yii::t('standard', 'Change enrollment deadline'); ?></option>
				<?php } ?>
				<?php if(Yii::app()->user->checkAccess('enrollment/delete')) { ?>
				<option value="delete"><?php echo Yii::t('standard', 'Uneroll'); ?></option>
				<?php } ?>
				<option value="mass-email"><?=Yii::t('course', 'Send Email')?></option>
				<?php foreach ($massIframeActions as $iframeAction) { ?>
			        <option value="iframe-field-<?= $iframeAction['value'] ?>"><?= $iframeAction['text'] ?></option>
		        <?php } ?> 
			</select>
		</div>
	</div>
	<?php endif; ?>

	<h3 class="title-bold title-with-border" style="word-break:normal;"><?= Yii::t('course', 'Users enrolled to', array('{course_name}' => $courseModel->name)) ?></h3>
	<div id="grid-wrapper">
		<?php $this->widget('ext.local.widgets.CXListView', array(
			'id' => 'course-enrollment-list',
			'htmlOptions' => array('class' => 'list-view clearfix'),
			'dataProvider' => $model->dataProviderEnrollment(),
			'itemView' => '_viewEnroll',
			'itemsCssClass' => 'items clearfix',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'pager' => array(
				'class' => 'DoceboCLinkPager',
				'maxButtonCount' => 8,
			),
			'template' => '{items}{pager}{summary}',
			'ajaxUpdate' => 'course-enrollment-list-all-items',
			'beforeAjaxUpdate' => 'function(id, options) {
					options.type = "POST";
					if (options.data == undefined) {
						options.data = {
							selectedItems: $(\'#\'+id+\'-selected-items-list\').val()
						}

					}
				}',
			'afterAjaxUpdate' => 'function(id, data) { '
				.' $(".tooltipize").tooltip(); '
				.' $("#course-enrollment-list input").styler(); '
				.' $(\'.popover-action\').popover({placement: \'bottom\', html: true}); '
				.' updateSelectedCheckboxesCounter(id); '
				.' $(\'a[rel="tooltip"]\').tooltip(); '
				.' $.fn.updateListViewSelectPage(); '
				.' }',//$(".deselect-all").click();
		)); ?>
	</div>
</div>

<?php
	if ($showHints) {
		$this->widget('OverlayHints', array('hints'=>'bootstroEnrollUsers'));
	}
?>

<script type="text/javascript">
//<![CDATA[
(function ($) {
	$(function () {

		$('.course-enrollments .jq-radio').live('click', function () {
			$('.ajax-list-form').submit();
		});

		$('input, select').styler();
		replacePlaceholder();

		$('.ajax-grid-form').find('input, select').live('change', function() {
			if ($(this).closest('.ajax-grid-form').length == 0) {
				return false;
			}
			$(this).closest('form').submit();
		});

		$('.reset-search-link').click(function() {
			var form = $(this).closest('form');
			form.get(0).reset();
			form.find('input, select').trigger('refresh');
			form.find('.enrollments-date-range').hide();
			//form.jCleverAPI('reset');
		});

		$('.search-icon').click(function(){
			$(this).closest('form').submit();
		});

		jQuery('form#course-enrollment-form').changeCausesFormSubmit({excludeSelector: '#advanced-search-user'});

		$(".tooltipize").tooltip();
		var jobType = <?= json_encode(md5(EnrollUsersToManyCourses::JOB_HANDLER)) ?>;
		var courseName = <?= json_encode($courseModel->name)?>;

		var lastUpdate = Math.floor(Date.now() / 1000);
		$(document).on('backgroundjobUpdate', function(event, hash, percent, type, nameParams) {
			if(type === jobType && typeof nameParams.course_name !== "undefined" && nameParams.course_name === courseName) {
				var nowUpdate = Math.floor(Date.now() / 1000);

				if((nowUpdate - lastUpdate) > 10){
					$.fn.yiiListView.update('course-enrollment-list');
					lastUpdate = nowUpdate;
				}
			}
		});

		$(document).on('backgroundjobFinished', function(event, hash, percent, type, nameParams){
			if(type === jobType && typeof nameParams.course_name !== "undefined" && nameParams.course_name === courseName) {
				$.fn.yiiListView.update('course-enrollment-list');
			}
		});

		// Show/hide date range fields
		jQuery('.enrollments-deadline-filter input[type=radio]').on('change', function(){
			var deadlineVal = jQuery(this).val();
			var dateRangeContainer = jQuery('.enrollments-date-range');
			if(deadlineVal == 2) {
				dateRangeContainer.show();
			} else {
				dateRangeContainer.hide();
			}
		});

		// Datepicker for Advanced Filter
		jQuery('.enrollments-date-range .date').bdatepicker('setStartDate', <?='"'.(($learningCourse->fromDate && $learningCourse->fromDate!='0000-00-00')?$learningCourse->fromDate:'').'"';?>);
		jQuery('.enrollments-date-range .date').bdatepicker('setEndDate', <?='"'.(($learningCourse->toDate && $learningCourse->toDate!='0000-00-00')?$learningCourse->toDate:'').'"';?>);
	});
})(jQuery);
//]]>
</script>