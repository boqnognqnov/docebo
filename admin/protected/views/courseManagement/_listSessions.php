<?php
/**
 * Created by PhpStorm.
 * User: Asen Nikolov
 * Date: 31-Aug-15
 * Time: 1:03 PM
 */
?>
	<style>
		#label {
			margin-bottom: 10px;
			margin-top: 20px;
		}

		#sessionList {
			padding-left: 3px;
			width: 76%;
		}
	</style>
<?php
if ($courseType == LearningCourse::TYPE_ELEARNING) {
	echo '';
} elseif (empty($sessions)) {
	echo Yii::t('classroom', 'No sessions found.');
} else {
	echo CHtml::label(Yii::t('classroom', 'Select session'), 'sessionList', array('id' => 'label'));
	echo CHtml::dropDownList('sessionId', '', $sessions, array('id' => 'sessionList', 'prompt' => (Yii::t('course', '_SESSION_SELECT') . '...')));
	echo CHtml::hiddenField('courseType', $courseType);
}
?>
<div id="messageToFullSession" style="display: none">
	<?=Yii::t('classroom', 'Limit of max enrollable users reached').'<br/>'.Yii::t('standard','_USER').' '.Yii::t('course','Go to').' '.Yii::t('course','_ALLOW_OVERBOOKING')?>
</div>
<script>
<?php
	$js_array = json_encode($fullSessionsIds);
	echo "var fullSessionsIds = ". $js_array . ";\n";
?>
	$('#sessionList').change(function(){
		var id = $(this).val();
		for (var i = 0; i < fullSessionsIds.length; i++) {
			if (id == fullSessionsIds[i]) {
				$('#messageToFullSession').show();
				break;
			}
			$('#messageToFullSession').hide();
		}
	});
$('#sessionList').trigger('change');
</script>
