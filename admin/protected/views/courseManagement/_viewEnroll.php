<?php
        $pUserRights = Yii::app()->user->checkPURights($data->learningCourse->course->getPrimaryKey(), $data->getPrimaryKey());
        
        $canEnroll = (Yii::app()->user->isGodAdmin || ($pUserRights->all && Yii::app()->user->checkAccess('enrollment/create')));
	
	//-------- Next several rows are related to COACHING sessions
	
	// Prepare for building the tooltip
	$coachingTooltipContent = false;
	$coachingLink = "";
	
	// Preoare COACHING icon, only if COURSE couaching is enabled
	if (PluginManager::isPluginActive('CoachingApp') && $data->learningCourse->course->enable_coaching) {
		// Coaching Sessions this user is assigne to
		$coachingSessions = LearningCourseCoachingSession::userAssignedSessions($data->idst, true, $data->learningCourse->idCourse);
		
		// If this user is a Coach, get list of sessions he is leading
		$coachSessions = $data->learningCourse->course->getCoachSessions($data->idst, true);
		
		// Any session(s) the user is assigned to???
		if (!empty($coachingSessions)) {
			// Get the first session. It must be ONE, but... whatever
			$session = $coachingSessions[0];
			$coachingTooltipContent = Yii::t('coaching', 'Session: from {start} to {end}<br> Coach: {coach}', array(
				'{start}' 	=> Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($session->datetime_start)),
				'{end}' 	=> Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($session->datetime_end)),
				'{coach}' 	=> $session->coach->getFullName(),
			));
		}
		// Or maybe a coach having Coaching session to lead on ??
		else if ($coachSessions) {
			// A coach MAY have multiple sessions in the same course. Build a string of all of them, one session per line, separated by a <BR> 
			$sessionLines = array();
			foreach ($coachSessions as $session) {
				$sessionLines[] = Yii::t('coaching', 'Session: from {start} to {end}', array(
					'{start}' 	=> Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($session->datetime_start)),
					'{end}' 	=> Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($session->datetime_end)),
				)); 			
			}
			$coachingTooltipContent = implode('<br>', $sessionLines);
		}
		
		// Any resulting coaching tooltip content ??
		if ($coachingTooltipContent) {
			$coachingLink = CHtml::link('<span class="fa fa-comments-o fa-lg"></span>', 'javascript:void();', array(
				'class'			=>	'tooltipize',
				'data-template' => '<div class="tooltip coaching-enrollment-session-tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
				'data-html'		=> 'true',
				'title'			=> $coachingTooltipContent,
			));
		}
	}


$canUnenroll = Yii::app()->user->checkAccess('enrollment/delete');
$canUnenrollMessage = '';

if (PluginManager::isPluginActive('CurriculaApp')) {
	$lpAssigned = LearningCoursepathCourses::isCourseAssignedToLearningPaths($data->learningCourse->idCourse);
	if (!empty($lpAssigned)) {
		$lpEnrollments = LearningCoursepath::checkEnrollment($data->idst, $data->learningCourse->idCourse, true);
		if (!empty($lpEnrollments)) {
			$canUnenroll = false;
			$canUnenrollMessage = '<div class="cannot-unenroll-tooltip">';
			$canUnenrollMessage .= '<span>' . Yii::t('myactivities', 'Learning plans') . ':</span><br />';
			$canUnenrollMessage .= '<ul>';
			foreach ($lpEnrollments as $lpId => $lpInfo) {
				$canUnenrollMessage .= '<li>- ' . $lpInfo['path_name'] . '</li>';
			}
			$canUnenrollMessage .= '</ul>';
			$canUnenrollMessage .= '</div>';
		}
	}
}
?>
<div class="item <?php echo (($index % 2) ? 'odd' : 'even'); ?>">
	<?php if ($canEnroll): ?>
	<div class="checkbox-column">
		<?php echo '<input id="group-member-management-list-checkboxes_' . $data->idst . '" type="checkbox" name="group-member-management-list-checkboxes[]" value="' . $data->idst . '" ' . (($data->inSessionList('selectedItems', 'idst')) ? 'checked="checked"' : '') . '>'; ?>
	</div>
	<?php endif; ?>
	<div class="course-enrollment-username"><?php echo Yii::app()->user->getRelativeUsername($data->fullName).' ('.Yii::app()->user->getRelativeUsername($data->userid).')';?></div>
	<div class="course-level">
		<span><?php echo Yii::t('levels', '_LEVEL_' . $data->learningCourse->level);?></span>
		<?= $coachingLink ?>
	</div>
	<div class="course-status"><?php echo '<span class="courseEnrollmentStatus_'.(int)$data->learningCourse->status.'"></span>';?></div>
	
	
	<?php if ($canEnroll): ?>
	<div class="course-actions"><?php echo $data->learningCourse->renderEnrollmentActions();?></div>
	<?php if($canUnenroll) { ?>
	<div class="delete-button" id="delete-button-<?= $data->idst ?>">
		<?php
		if ($canUnenroll) {
			$this->renderPartial('//common/_modal', array(
				'config' => array(
					'class' => 'delete-node',
					'modalTitle' => Yii::t('standard', 'Uneroll'),
					'linkTitle' => 'Delete',
					'url' => 'courseManagement/deleteEnrollment&id=' . $data->idst,
					'buttons' => array(
						array(
							'type' => 'submit',
							'title' => Yii::t('standard', '_CONFIRM'),
						),
						array(
							'type' => 'cancel',
							'title' => Yii::t('standard', '_CANCEL'),
						),
					),
					'afterLoadingContent' => 'hideConfirmButton();',
					'afterSubmit' => 'updateEnrollmentContent',
				),
			));
		} elseif (!empty($canUnenrollMessage)) {
			echo CHtml::link('', 'javascript:return false;', array(
				'rel' => 'tooltip',
				'data-html' => 'true',
				'data-toggle' => 'tooltip',
				'data-placement' => 'top',
				'title' => $canUnenrollMessage,
				'class' => 'cannot-unenroll'
			));
		}
		?>
	</div>
	<?php } ?>
	<?php endif; ?>
</div>