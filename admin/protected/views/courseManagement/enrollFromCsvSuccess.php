<div class="enroll-success">
    <div class="copy-success"><div class="copy-success-icon"></div>
    	<?= Yii::t('course', 'Users enrolled to', array('{course_name}' =>'<br>"' . $course->name . '"')) ?>
    </div>

	<div class="stats">
		<p><?= Yii::t('standard', 'Enrolled users') ?>: <strong><?php echo $result['enrolled']; ?></strong></p>
		<p><?= Yii::t('standard', 'Skipped') ?>: <strong><?php echo $result['skipped']; ?></strong></p>
		<p><?= Yii::t('standard', 'Users not found') ?>: <strong><?php echo $result['notFound']; ?></strong></p>
	</div>

	<?php echo CHtml::button('Ok', array('class' => 'btn ok', 'data-dismiss' => 'modal')); ?>
</div>