<?php if (!empty($fullCourseTree)) { ?>
	<ul class='node-tree'>
		<?php $level = 1; ?>
		<?php foreach ($fullCourseTree as $node) {
			$left        = 10 * $node->lev;
			$rootNode    = '';
			$hasChildren = '';
			if (!$node->isLeaf()) {
				if ($pu) {
					foreach ($_categoryNodes as $id => $_node) {
						if ($_node['lev'] > $node->lev && $_node['iLeft'] > $node->iLeft && $_node['iRight'] < $node->iRight) {
							$hasChildren = 'hasChildren';
							break;
						}
					}
				} else
					$hasChildren = 'hasChildren';
			}
			if ($node->isRoot()) {
				$rootNode = 'rootNode';
			}
			?>
			<?php if ($node->lev < $level) { ?>
				<?php for ($i = $node->lev; $i < $level; $i++) {
					echo '</ul>';
				} ?>
			<?php } ?>

		<?php $getTreeUrl = Yii::app()->createUrl('courseManagement/getCategoryTree', array('id' => $node->idCategory)); ?>

		<li class="node nodeTree-node <?php echo $hasChildren . ' ' . $rootNode; ?>">
			<div class="nodeTree-border">
				<div class="nodeTree-margin ajaxLinkNode <?php echo (($currentNodeId == $node->idCategory) ? 'checked-node' : ''); ?>" data-url="<?= $getTreeUrl ?>">
					<div class="clearfix" <?php if (empty($rootNode)) { ?>style="margin: 0px <?php echo $left; ?>px;"<?php } ?>>
						<div class="label-wrapper">
							<span class="collapse-node"></span>
							<div class="label-for-radio"><?php
								if ($node->coursesCategory->translation != '') {
									echo $node->coursesCategory->translation;
								} else {
									echo $defaultCourseTreeTranslation[$node->idCategory];
								} ?></div>
						</div>
						<?php
							$canPUModifyCategory = (!empty($disableNodeActions) && !in_array($node->idCategory, $disableNodeActions));
							if (Yii::app()->user->getIsGodAdmin() || (Settings::get('enable_category_advanced_settings') == 'on') || $canPUModifyCategory) {
								// NOTE: at the moment power users cannot create/edit/move/delete course folders
								$nodeActions = $this->renderPartial('_nodeActions', array(
									'node' => $node,
									'id' => $node->idCategory,
									'name' => $node->coursesCategory->translation,
									'isRoot' => $node->isRoot(),
									'canPUModifyCategory' => $canPUModifyCategory
								), true);
								echo CHtml::link('', 'javascript:void(0);', array(
									'id' => 'popover-'.uniqid(),
									'class' => 'popover-trigger popover-bottom select-columns',
									'data-toggle' => 'popover',
									'data-content' => $nodeActions,
								));
							}
						?>
					</div>
				</div>
			</div>
			<?php if (!empty($hasChildren)) { ?>
			<ul class="nodeUl">
		<?php } ?>
			<?php if (empty($hasChildren)) { ?>
				</li>
			<?php } ?>
			<?php $level = $node->lev; ?>
		<?php } ?>
	</ul>
<?php } ?>