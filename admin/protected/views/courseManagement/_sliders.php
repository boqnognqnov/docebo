<div class="courseEnroll-tabs">
	<ul class="nav nav-tabs">
		<li class="<?php echo $selectedTab == 'default' ? 'active' : ''; ?>">
			<a data-target=".courseEnroll-page-users" href="#">
				<?php echo Yii::t('course', 'Choose a thumbnail for your course'); ?> (<?php echo $count['default']; ?>)
			</a>
		</li>
		<li class="<?php echo $selectedTab == 'user' ? 'active' : ''; ?>">
			<a data-target=".courseEnroll-page-groups" href="#">
				<?php echo Yii::t('course', 'Upload Your Own Thumbnail'); ?> (<span id="user-images-count"><?php echo $count['user']; ?></span>)
			</a>
		</li>
	</ul>

	<div class="select-user-form-wrapper">
		<div class="upload-form-wrapper" style="display: <?php echo $selectedTab == 'user' ? 'block' : 'none';?>">
			<div class="upload-form">
				<div class="fileUploadContent">
					<label for="upload-btn"><?php echo Yii::t('branding', '_UPLOAD_BACKGROUND_BTN'); ?></label>
					<?php echo CHtml::fileField('LearningCourse[attachments]', '', array(
						'class' => 'ajaxCrop custom-image',
						'data-class' => 'LearningCourse',
						'data-width' => $cropSize['width'],
						'data-height' => $cropSize['height'],
						'data-image-type' => CoreAsset::TYPE_COURSELOGO,
						'data-callback' => 'updateSliderContent',
					)); ?>
				</div>
			</div>
		</div>
		<div class="tab-content">
			<div class="courseEnroll-page-users tab-pane <?php echo $selectedTab == 'default' ? 'active' : ''; ?>">
				<div id="defaultSlider_<?php echo $uniqueId; ?>" class="carousel slide thumbnails-carousel">
					<!-- Carousel items -->
					<div class="carousel-inner">
						<?php if (!empty($defaultImages)) { ?>
							<?php foreach ($defaultImages as $key => $defaultThumbnail) { ?>
								<div class="item<?php echo ($key == 0) ? ' active' : ''; ?>">
									<?php foreach ($defaultThumbnail as $key => $imageUrl) {
										$html = CHtml::image($imageUrl);
										$html .= '<input type="radio" name="LearningCourse[selectedImage]" value="' . $key . '" '. ($key == $model->img_course ? 'checked="checked"' : '') .' />';
										echo '<div class="sub-item ' . ($key == $model->img_course ? 'checked' : '') .'">' . $html . '</div>';
									}
									?>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
					<!-- Carousel nav -->
					<a class="carousel-control left" href="#defaultSlider_<?php echo $uniqueId; ?>" data-slide="prev">&lsaquo;</a>
					<a class="carousel-control right" href="#defaultSlider_<?php echo $uniqueId; ?>" data-slide="next">&rsaquo;</a>
				</div>
			</div>
			<div class="courseEnroll-page-groups tab-pane <?php echo $selectedTab == 'user' ? 'active' : ''; ?>">
				<div id="userSlider_<?php echo $uniqueId; ?>" class="carousel slide thumbnails-carousel custom-thumbnails-carousel ">
					<!-- Carousel items -->
					<div class="carousel-inner">
						<?php if (!empty($userImages)) { ?>
							<?php foreach ($userImages as $key => $userThumbnail) { ?>
								<div class="item<?php echo ($key == 0) ? ' active' : ''; ?>">
									<?php foreach ($userThumbnail as $key => $imageUrl) {
										$html = CHtml::image($imageUrl);
										$html .= '<input type="radio" name="LearningCourse[selectedImage]" value="' . $key . '" '. ($key == $model->img_course ? 'checked="checked"' : '') .' />';
										echo '<div class="sub-item ' . ($key == $model->img_course ? 'checked' : '') .'">' . $html;
										echo '<span class="deleteicon"><span class="i-sprite is-remove red"></span></span>';
										echo '</div>';
									}
									?>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
					<input class="hidden" value="0" id="currentPage"/>
					<input class="hidden" value="<?php echo $count['user'];?>" id="totalPages"/>
					<input class="hidden" value="<?php echo $model->idCourse ? $model->idCourse : false;?>" id="courseId"/>
					<input class="hidden" name="selectedImage" value="<?php echo $model->img_course ? $model->img_course : false;?>" id="selectedImageId"/>
					<!-- Carousel nav -->
					<a class="carousel-control left  carousel-control-left" href="#userSlider_<?php echo $uniqueId; ?>" data-slide="prev">&lsaquo;</a>
					<a class="carousel-control right  carousel-control-right" href="#userSlider_<?php echo $uniqueId; ?>" data-slide="next">&rsaquo;</a>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$('.custom-thumbnails-carousel').carousel({
			interval:false
		});

		$(".carousel-control-left").click(function(event){
			event.preventDefault();
			getThumnbnails(false);
		})

		$(".carousel-control-right").click(function(event){
			event.preventDefault();
			getThumnbnails(true);
		})
	});

	function getThumnbnails(forward){
		var modal = $('.modal[aria-hidden="false"]');
		var count = modal.find('#totalPages').val();
		var nextPage = 0;
		var currentPageVal =  modal.find('#currentPage').val();
		var courseId = modal.find("#courseId").val();
		if(!currentPageVal){
			currentPageVal = 0;
		}
		var selectedImageId = '';
		if(modal.find('.sub-item.checked').find('.jq-radio.checked').parent().find('input').val()){
			selectedImageId = modal.find('.sub-item.checked').find('.jq-radio.checked').parent().find('input').val();
			modal.find('#selectedImageId').attr('value', selectedImageId);
		}else{
			selectedImageId = modal.find('#selectedImageId').val();
		}
		if(forward){
			nextPage =  parseInt(currentPageVal) +1;
		}else{
			nextPage = parseInt(currentPageVal)-1;
		}
		modal.find('.custom-thumbnails-carousel  .carousel-inner').html('<div class="ajaxloader" style="display: block;"></div>');
		$.ajax({
			url: "<?php echo Yii::app()->createUrl('courseManagement/getThumbnails'); ?>",
			type: "POST",
			data: {
				pageId: nextPage,
				totalCount: count,
				courseId: courseId,
				selectedImageId: selectedImageId
			},
			success: function(result){
				var courseId = modal.find('.custom-thumbnails-carousel  .carousel-inner').html(result.html);
				if(result.currentPage){
					modal.find('#currentPage').attr('value', result.currentPage);
				}else{
					modal.find('#currentPage').attr('value',0);
				}
				$('.select-user-form-wrapper .item > div').live('click', function () {
					$('.select-user-form-wrapper .item > div').removeClass('checked');
					// $('.thumbnailSlider .sub-item').removeClass('checked');

					$(this).addClass('checked');

					// $(this).find('input[type="radio"]').trigger('click');
					$(this).find('input[type="radio"]').prop('checked', true);
					$(this).parent().find('.jq-radio').removeClass('checked');
					$(this).find('.jq-radio').addClass('checked');
				});
			},
			error: function() {
			}
		});
	}


	function updateControls(nav) {
	if (nav == 'show' ) {
		$('[id*="userSlider"] .carousel-control.right').show();
		$('[id*="userSlider"] .carousel-control.left').show();
	}
	else if (nav == 'hide' ) {
		$('[id*="userSlider"] .carousel-control.right').hide();
		$('[id*="userSlider"] .carousel-control.left').hide();
	}	
}	
var navStatus = $('[id*="userSlider"] .sub-item').length > 0 ? 'show' : 'hide';
updateControls(navStatus);
									

									
/**
 * DELETE User image
 */

$(document).off('click', '[id*="userSlider"] .deleteicon').on('click', '[id*="userSlider"] .deleteicon', function(e) {

	var imageId = $(this).closest('.sub-item').find('input[type="radio"]').val();
	var form = $(this).closest('form');

    var options = {
		type        : 'post', 
		dataType    : 'json',
		data: {
			delete_image	: true,
			image_id		: imageId
		}, 
		success     : function (res) {
			if (res && (res.success == true)) {
				$('[id*="userSlider"]').find('input[type="radio"][value="'+ imageId +'"]').closest('.sub-item').remove();
				imagesCount = parseInt($('#totalPages').val())-1;
				$('#user-images-count').text(imagesCount);
				$('#totalPages').val(imagesCount);
				var navStatus = $('[id*="userSlider"] .sub-item').length > 0 ? 'show' : 'hide';
				updateControls(navStatus);
			}
		}
	};

    e.stopPropagation();
    form.ajaxForm(options).submit();
	return false;
	
});


								
</script>
