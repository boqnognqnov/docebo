<style>
	.course-enrollments-send-email{
		width: 67%;
		position: absolute !important;
	}
	label{
		float: none !important;
	}
	input[type="text"]{
		margin-top: 3px;
		width: 97%;
	}
	.ccLink{
		text-decoration: underline !important;
		color: #0465ac !important;
		font-weight: 300 !important;
	}
	.row-fluid{
		margin-bottom: 15px;
	}
	#ccDescription{
		display: none;
		opacity: 0.6;
	}
	
</style>
<h3 class="title-bold"><?php echo Yii::t('standard', '_NEWSLETTER'); ?></h3>

<div class="newsletter-form-wrapper">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'newsletter-form',
		'htmlOptions' => array(
			'enctype' => 'multipart/form-data',
		),
	)); ?>

	<? if(!empty($errors)):
		foreach ($errors as $err) {
			echo '<p><span class="errorMessage">'.$err.'</span></p>';
		}
	endif; ?>

	<?php //echo $form->errorSummary($model); ?>

	<div class="row-fluid recipients">
		<div class="span9">
			<?= CHtml::label(Yii::t('standard', '_RECIPIENTS'), 'to'); ?>
			<?= CHtml::textField('to', $emails, array('disabled' => 'disabled', 'class' => 'muted')); ?>
			<?= CHtml::link(Yii::t('standard', 'Add CC'), '#', array('id' => 'ccLink', 'class'=>'ccLink')) ?>
			<?= CHtml::label(Yii::t('standard', 'CC'), 'ccMails',array('style' => 'display:none; margin-top:15px')); ?>
			<?= CHtml::textField('ccMails', isset($_POST['ccMails']) ? $_POST['ccMails'] : '', array('style' => 'display:none', 'id'=>'ccMails', 'disabled'=>'disabled', 'placeholder'=>'email1@company.com,email2@company.com')) ?>
			<div id="ccDescription"><?=Yii::t('standard','Enter email address(es) separated by commas')?></div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span9">
			<?php echo CHtml::label(Yii::t('standard', '_FROM'), 'from'); ?>
			<?php echo CHtml::textField('from', isset($_POST['from']) ? $_POST['from'] : Yii::app()->user->getEmail()); ?>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span9">
			<?php echo CHtml::label(Yii::t('standard', '_SUBJECT'), 'subject'); ?>
			<?php echo CHtml::textField('subject', isset($_POST['subject']) ? $_POST['subject'] : null, array('id' => false)); ?>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span9">
			<?php echo CHtml::label(Yii::t('htmlframechat', '_MSGTXT'), 'body'); ?>
			<?php echo CHtml::textArea('body', isset($_POST['body']) ? $_POST['body'] : null); ?>
		</div>
		<div class="span3">
			<strong style="margin-bottom: 5px"><?= Yii::t('notification', 'Short codes') ?></strong>
			<br>
			<?= Yii::t('notification', 'Use these shortcodes to insert data in the notification subject and/or message body') ?>
			<br /><br />
			<ul id="notification_shortcodes_list" data-empty-text="">
				<?php foreach ($shortcodes as $shortcode) {
					echo CHtml::tag('li', array(), $shortcode);
				} ?>
			</ul>
		</div>
	</div>

	<?=CHtml::hiddenField('ids', $ids, array('id'=>false))?>
	<?=CHtml::hiddenField('courseId', $courseId, array('id'=>false))?>
	<?=CHtml::hiddenField('sessionId', $sessionId, array('id'=>false))?>

	<?php $this->endWidget(); ?>
</div>

<script type="text/javascript">
	$(function(){
		TinyMce.attach('textarea', {height: '250px'});
		//self remove from DOM, because when the dialog is closed with different thatn "Cancel click" way - it is only hidden and the problems started
		$("#bootstrapContainer .modal").on('hidden', function () {
			$("#bootstrapContainer").remove();
		});

		$('#ccLink').click(function () {
			var linkText = $(this).text();
			if (linkText == '<?=Yii::t('standard','Add CC')?>') {
				$(this).text('<?=Yii::t('standard','Remove CC')?>');
				$('#ccMails').prop('disabled', false).show();
				$('#ccMails').prev().show();
				$('#ccDescription').show();
			} else {
				$(this).text('<?=Yii::t('standard','Add CC')?>');
				$('#ccMails').val('');
				$('#ccMails').prop('disabled', true).hide();
				$('#ccMails').prev().hide();
				$('#ccDescription').hide();
			}
		});
	})
</script>