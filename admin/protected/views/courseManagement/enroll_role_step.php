<?php
/** @var $course LearningCourse */
/** @var $initialDateBegin string */
/** @var $initialDateEnd string */
/** @var $showDeadlineSelector boolean */
?>
<h1><?= Yii::t('subscribe', '_CAPTION_SELECT_LEVELS') ?></h1>

<div class="select-role-wrapper">

<?php
    $uniqueEnrollRoleStepId = uniqid();

	echo CHtml::beginForm('', 'POST', array(
		'id' => 'enroll-role-step-form',
		'name' => 'enroll-role-step-form',		
		'class' => 'ajax jwizard-form',
	));
	
	echo CHtml::hiddenField('from_step', 'enroll-role-step');
	echo CHtml::hiddenField('idCourse', $course->idCourse);
?>

<p>
<?= Yii::t('course_management', '_YOU_ARE_GOING_TO_SUBSCRIBE', array('{users}' => $usersCount,'{courses}' => 1)) ?>
</p>


<div class="alert alert-error alert-compact text-center">
	<p>
		<?php 
			if($errors){
				foreach($errors as $v){
					print_r($v);
				}
			}
		?>
	</p>
</div>

<br><br>
<div class="row-fluid">
	<div class="span2 text-left"><p><?= Yii::t('standard', '_LEVEL', array($usersCount)) ?></p></div>
	<div id="level-wrapper" class="span10">
		<?= CHtml::dropDownList('enrollment_role', -1, $userLevels, array('prompt'=>Yii::t('standard', '_SELECT').'...')) ?>
	</div>
    <br />
    <br />
    <br />
    <?php echo CHtml::checkBox("set_enrollment_deadlines", false, array(
        'id'    =>  'set_enrollment_deadlines-check-btn'.$uniqueEnrollRoleStepId,
        'class' =>  'set_enrollment_deadlines-check'. ($assignedCount == 0 ? 'no-users-assigned' : ''),
    )); ?>
    <div id="set_enrollment_deadlines-hint">
        <div><?php echo CHtml::label(Yii::t('standard','Set enrollment deadlines'), 'set_enrollment_deadlines-check-btn'.$uniqueEnrollRoleStepId,array('class'=>'single-course-mass-enrollment-inline-label')); ?></div>
        <div id="set_enrollment_deadlines-label-hint"><?= Yii::t('coaching', 'Leave blank for no deadline') ?></div>
    </div>
    <div id="datepickers-wrapper" class="datepickers-wrapper">
        <br />
        <?php
        ?>
        <?php if(!$showDeadlineSelector)
            $this->widget('common.widgets.warningStrip.WarningStrip', array('message'=>Yii::t('standard','You cannot set enrollment deadlines for the selected users because the course end date has expired on {date}.',
				array(
					'{date}' => $course->date_end)),
					'type'=>'warning'
				)
			);
        else {
        ?>
    <div class='input-append date clearfix' data-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>' data-date-language='<?= Yii::app()->getLanguage() ?>'>
        <?php
        $this->widget('common.widgets.DatePicker', array(
            'id' => 'single-course-mass-enrollment-deadline-start-date'.$uniqueEnrollRoleStepId,
            'label' => CHtml::activeLabel(LearningCourseuser::model(), 'date_begin_validity',array('id'=>'single-course-mass-enrollment-date_begin_validity-label')),
            'fieldName' => CHtml::activeName(LearningCourseuser::model(), 'date_begin_validity'),
            'value' => $initialDateBegin,
            'htmlOptions' => array(
                'id' => CHtml::activeId(LearningCourseuser::model(), 'single-course-mass-enrollment-date_begin_validity'.$uniqueEnrollRoleStepId),
                'class' => 'datepicker datepicker-input',
                'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')),
                'data-date-language' => Yii::app()->getLanguage(),

            ),
            'pickerStartDate'	=> ($course->date_begin && $course->date_begin!='0000-00-00')?$course->date_begin:'',
            'pickerEndDate'     => ($course->date_end && $course->date_end!='0000-00-00')?$course->date_end:'',
        ));
        ?>
    </div>
    <div class='input-append date clearfix' data-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>' data-date-language='<?= Yii::app()->getLanguage() ?>'>
        <?php
        $this->widget('common.widgets.DatePicker', array(
            'id' => 'single-course-mass-enrollment-deadline-end-date'.$uniqueEnrollRoleStepId,
            'label' => CHtml::activeLabel(LearningCourseuser::model(), 'date_expire_validity',array('id'=>'single-course-mass-enrollment-date_expire_validity-label')),
            'fieldName' => CHtml::activeName(LearningCourseuser::model(), 'date_expire_validity'),
            'value' => $initialDateEnd,
            'htmlOptions' => array(
                'id' => CHtml::activeId(LearningCourseuser::model(), 'single-course-mass-enrollment-date_expire_validity'.$uniqueEnrollRoleStepId),
                'class' => 'datepicker datepicker-input',
                'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')),
                'data-date-language' => Yii::app()->getLanguage(),

            ),
            'pickerStartDate'	=> ($course->date_begin && $course->date_begin!='0000-00-00')?$course->date_begin:'',
            'pickerEndDate'     => ($course->date_end && $course->date_end!='0000-00-00')?$course->date_end:'',
        ));
        ?>
    </div>
        <?php }?>
    </div>
</div>

<div id="enrollment-fields" class="enrollment-fields row-fluid">
		<?php 
		if(!empty($fields)){
			foreach($fields as $field){
				$mandatory = $field['mandatory'] == 1 ? '*' : '';
				
				
				switch ($field['type']) {
				    case LearningEnrollmentField::TYPE_FIELD_DATE:
				        ?>
    					<div class='input-append date clearfix' data-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>' data-date-language='<?= Yii::app()->getLanguage() ?>'>
    						<?php
    						$this->widget('common.widgets.DatePicker', array(
    							'id' => 'field-'.$field['id'],
    							'label' => '<div class="span3 date-picker-label">' . $field['title'].$mandatory . '</div>',
    							'fieldName' => 'field-'.$field['id'],
    							'value' => date(Yii::app()->localtime->getPHPLocalDateFormat('short')),
    							'htmlOptions' => array(
    								'id' => CHtml::activeId(LearningCourseuser::model(), 'field-'.$field['id']),
    								'class' => 'datepicker datepicker-input',
    								'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')),
    								'data-date-language' => Yii::app()->getLanguage(),
    							),
    						));
    						?>
    					</div>
				        <?php     
				        break;
				        
				    case LearningEnrollmentField::TYPE_FIELD_DROPDOWN:
				        ?>
    					<div class="enrollment-dropdown-field clearfix">
    						<?php
    							echo CHtml::label(Yii::t('standard', $field['title'].$mandatory), 'select-level-input', array('class' => 'span3'));
    							echo CHtml::dropDownList('field-'.$field['id'], '', $field['options'] , array('class' => 'span9') );
    						?>
    					</div>
				        <?php 
				        break;
				        
				    case LearningEnrollmentField::TYPE_FIELD_TEXTFIELD:
				        ?>
    					<div class="NodeTranslation">
    						<?php
    							echo CHtml::label(Yii::t('standard', $field['title'].$mandatory), 'field-'.$field['id'], array('class' => 'span3'));
    							echo CHtml::textField('field-'.$field['id'], '' , array('class'=>'span9'));
    						?>
    					</div>
				        <?php 
				        break;
				        
				    case LearningEnrollmentField::TYPE_FIELD_TEXTAREA:
				        ?>
    					<div class="NodeTranslation">
    						<?php
    							echo CHtml::label(Yii::t('standard', $field['title'].$mandatory), 'field'.$key, array('class'=> 'span3'));
    							echo CHtml::textArea('field-'.$field['id'], '', array('class' => 'span9'));
    						?>
    					</div>
				        <?php 
				        break;
				        
                    case LearningEnrollmentField::TYPE_FIELD_IFRAME:
                        if (false) {
                            $iframeField = LearningEnrollmentFieldIframe::model()->findByPk($field['id']);
                            ?>
        					<div class="clearfix">
        						<?php
        		  	   		    $enrollments[] = array();
        			 	   	    ?>
        						<?php echo CHtml::label(Yii::t('standard', $field['title'].$mandatory), 'field-'.$field['id'], array(
        					       "class" => "span3"
        					    )); ?>
        						<div class="span9 iframe-field"><?php echo $iframeField->renderIframe($enrollments) ?></div>
        					</div>
                        	<?php
                        }
				        break;
				}
			}
		}?>
	</div>

	
<div class="form-actions jwizard">
	<?= CHtml::submitButton(Yii::t('standard', '_PREV'), 		array('class' => 'btn btn-docebo green big jwizard-nav', 'data-jwizard-url' => $usersSelectorOpenUrl)); ?>
	<?= CHtml::submitButton(Yii::t('standard', '_CONFIRM'), 	array('name' =>  'finish_wizard', 'class' => 'btn btn-docebo green big jwizard-nav jwizard-finish', 'data-jwizard-url' => $finishUrl)); ?>
	<?= CHtml::button(		Yii::t('standard', '_CANCEL'), 		array('class' => 'btn btn-docebo black big close-dialog', 'name' => 'cancel_step')); ?>
</div>


<?php echo CHtml::endForm(); ?>


</div>


<script type="text/javascript">

	// Style this dialog. It is part of jWizard, so use it to rpeserve class switching between steps
	jwizard.setDialogClass('enroll-role-step');

	// Disable FINISH button if NO users and NO role are selected 
	var usersCount = parseInt('<?= (int) $usersCount ?>');
	var selectedRole = $('select[name="enrollment_role"]').val();
	if ( (usersCount <= 0) || (selectedRole <= 0)) {
		$('input[name="finish_wizard"]').attr('disabled','disabled').addClass('disabled');
	}


	// Listen for ROLE dropdown change and adjust FINISH button, counting number of users as well
	$(document).off('change', 'select[name="enrollment_role"]')
		.on('change', 'select[name="enrollment_role"]', function(){
		if ($(this).val() <= 0 || (usersCount <= 0)) {
			$('input[name="finish_wizard"]').attr('disabled','disabled').addClass('disabled');
			$('a.jwizard-finish').addClass('disabled');
		}
		else {
			$('input[name="finish_wizard"]').removeAttr('disabled').removeClass('disabled');
			$('a.jwizard-finish').removeClass('disabled');
		}
	});

    $('#set_enrollment_deadlines-check-btn<?=$uniqueEnrollRoleStepId?>').styler();
    $(document).on('change', '#set_enrollment_deadlines-check-btn<?=$uniqueEnrollRoleStepId?>',function(){
        if($(this).is(':checked')) {
            $('.datepickers-wrapper').show();
        }
        else {
            $('.datepickers-wrapper').hide();
        }

    });

    if ($('#set_enrollment_deadlines-check-btn<?=$uniqueEnrollRoleStepId?>').is(':checked')) {
        $('.datepickers-wrapper').show();
    } else {
        $('.datepickers-wrapper').hide();
    }
	
</script>