<script type='text/javascript'>
	$(function(){
		$.post('<?=Docebo::createLmsUrl('progress/run')?>', {
			'type': <?=Progress::JOBTYPE_DELETE_USER_ENROLLMENT?>,
			'idst': '<?= $users ?>',
			'idCourse': <?= $currentCourseId ?>
		}, function(data){
			var dialogId = 'delete-user-enrollment-progressive';

			$('<div/>').dialog2({
				autoOpen: true, // We will open the dialog later
				id: dialogId,
				title: '<?=addslashes(Yii::t('standard', '_DEL_SELECTED'))?>'
			});
			$('#'+dialogId).html(data);
		});
	})
</script>