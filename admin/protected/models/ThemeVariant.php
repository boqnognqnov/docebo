<?php

/**
 * This is the model class for table "theme_variant".
 *
 * The followings are the available columns in table 'theme_variant':
 * @property string $theme_code
 * @property integer $id_multidomain
 * @property integer $author_id
 * @property string $date_created
 * @property string $name
 * @property string $image
 * @property string $description
 * @property integer $builtin
 * @property integer $active
 * @property string $settings
 */
class ThemeVariant extends CActiveRecord
{

    const LEGACY_THEME_CODE = 'legacy';
    const LEGACY_THEME_NAME = 'Docebo 6.9';
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'theme_variant';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('theme_code, date_created', 'required'),
			array('id_multidomain, author_id, builtin, active', 'numerical', 'integerOnly'=>true),
			array('theme_code, name, image, description', 'length', 'max'=>255),
			array('settings', 'length', 'max'=>65535),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('theme_code, id_multidomain, author_id, date_created, name, image, description, builtin, active, settings', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'theme_code' => 'Theme Code',
			'id_multidomain' => 'Id Multidomain',
			'author_id' => 'Author',
			'date_created' => 'Date Created',
			'name' => 'Name',
			'image' => 'Image',
			'description' => 'Description',
			'builtin' => 'Builtin',
			'active' => 'Active',
			'settings' => 'Settings',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('theme_code',$this->theme_code,true);
		$criteria->compare('id_multidomain',$this->id_multidomain);
		$criteria->compare('author_id',$this->author_id);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('builtin',$this->builtin);
		$criteria->compare('active',$this->active);
		$criteria->compare('settings',$this->settings,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ThemeVariant the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Get all available themes.
     * Returned values is : theme_key => them_name
     *
     * @return array
     */
	public static function getAvailableThemes(){
	    $result = array(
	        'legacy' => 'Docebo 6.9'
        );
	    $themes = self::model()->findAllByAttributes(array(
	        'id_multidomain' => 0,
        ));

	    if(!empty($themes)){
	        foreach ($themes as $theme){
                /**
                 * @var $theme ThemeVariant
                 */
	            $result[$theme->theme_code] = $theme->name;
            }
        }

        return $result;

    }

    /**
     * Get current active theme for the client
     *
     * @param $idMultidomain
     * @return static
     */
    public static function getMultidomainTheme($idMultidomain){
	    $theme = self::model()->findByAttributes(array(
	        'id_multidomain' => $idMultidomain,
            'active' => 1
        ));

	    return $theme;
    }

    /**
     * Extends the base theme and assign the variant to the multidomain client
     *
     * @param ThemeVariant $baseTheme
     * @param $idMultidomain
     * @return ThemeVariant|static
     */
    public static function extendsTheme(ThemeVariant $baseTheme, $idMultidomain){
        $theme = self::model()->findByAttributes(array(
            'theme_code' => $baseTheme->theme_code,
            'id_multidomain' => $idMultidomain
        ));
        if(!$theme){
            $theme = new self();
            $theme->theme_code = $baseTheme->theme_code;
            $theme->id_multidomain = $idMultidomain;
            $theme->author_id = Yii::app()->user->id;
            $theme->date_created = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
            $theme->name = $baseTheme->name;
            $theme->image = $baseTheme->image;
            $theme->description = $baseTheme->description;
            $theme->builtin = 0;
            $theme->active = 1;
            $theme->settings = CJSON::encode(array());
        } else{
            if($theme->active == 0){
                $theme->active = 1;
            }
        }

        $res = $theme->save();
        if(!$res){
            Yii::log(print_r($theme->getErrors(), 1), 'error');
        }
        return $theme;
    }

    /**
     * Set all theme variants for this multidomain client to 'active => 0'
     *
     * @param $themeCodeName
     * @param $idMultidomainClient
     */
    public static function deactiveAllThemesForClient($themeCodeName, $idMultidomainClient){
        $existing = self::model()->findAllByAttributes(array(
            'id_multidomain' => $idMultidomainClient
        ));

        if(!empty($existing)){
            foreach ($existing as $theme){
                $theme->active = 0;
                $theme->save();
            }
        }
    }

    /**
     * Assign theme to multidomain client
     *
     * Function first will set all existing variants to 'active => 0' then will create a new one (if 'legacy' - do nothing)
     *
     * @param $idClient
     * @param $themeCode
     */
    public static function saveClientTheme($idClient, $themeCode){
        $baseTheme = self::model()->findByAttributes(array(
            'theme_code' => $themeCode,
            'id_multidomain' => 0
        ));

        if($baseTheme){
            //Deactivate first all other themes
            self::deactiveAllThemesForClient($themeCode, $idClient);

            //Create a new variant and extends base options
            self::extendsTheme($baseTheme, $idClient);
        } else{
            self::deactiveAllThemesForClient($themeCode, $idClient);
        }
    }
}
