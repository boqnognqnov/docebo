$(document).ready(function() {
	$('#top-left, #top-right, #bottom-left, #bottom-right').sortable({
		handle: '.move',
		connectWith: ".section",
		tolerance: 'pointer',
		update: function(event, ui) {
			var blocks = {};

			$('.section').not('#middle').each(function() {
				var _section = $(this);
				var id = _section.attr('id');

				blocks[id] = [];
				$(this).find('.item').each(function() {
					blocks[id].push($(this).data('template'));
				})
			})

		  $.post(HTTP_HOST + '?r=dashboard/reorder', { blocks: blocks });
		}
	});

	$('.generate-reports .export-report').popover({
		html: true,
		placement: 'bottom',
		container: '.generate-reports .links .popover-container',
		content: function() {
			var content = '';
			var id = $('#LearningReportFilter_id_filter').val();

			if (id == "") {
				return false;
			}

			$.ajax({
				url: HTTP_HOST + '?r=reportManagement/getStandardActions',
				data: {'id': id},
				async: false,
				timeout: 4000,
				error: function(){
					return '';
				},
				success: function (result) {
					if (result.html) {
						content = result.html;
					}
				}
			});

			if (content != '') {
				return content;
			}

		}
	});

	/**
	 * Listen for SHOWN event for the popover and apply controls()
	 */
	$(document).on('shown.bs.popover', '.generate-reports .export-report', function(e){
		$(document).controls();
	});
	

	$('.generate-reports .links').on('click', '.view-report', function() {
		var id = $('#LearningReportFilter_id_filter').val();

		if (id == "") {
			return false;
		}

		window.location.href = HTTP_HOST + '?r=reportManagement/customView&id=' + id;
	});

	applyDonut('.donutContainer');
	applyMultiSeriesCharts('.multiSeriesLineChartContainer');
})