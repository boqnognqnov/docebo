$(document).ready(function () {
	$('.advanced-search-link').click(function () {
		$('.group').toggleClass('group-padding');
	});

	$('.main-actions ul.clearfix li a').live('mouseenter', function () {
		var attrAlt = $(this).attr('alt');
		var aText = $(this).text();

		$('.main-actions .info p').text(attrAlt);
		$('.main-actions .info h4').append('<span></span>' + aText);
		$('.main-actions .info').show();
	});

	$('.main-actions li a').live('mouseleave', function () {
		var attrAlt = $(this).attr('alt');

		$('.main-actions .info p').text('');
		$('.main-actions .info h4').text('');
		$('.main-actions .info').hide();
	});

	$('.nodeTree-selectNode').live('click', function () {
		$(this).find('input').prop('checked', true).trigger('refresh');
		$('.nodeTree-margin').removeClass('checked-node');
		$(this).addClass('checked-node');
		var currentId = $('#currentNodeId').val();
		var url = '';
		if ($(this).parents('form').attr('id') == 'orgChart_move') {
			var selectedId = $(this).find('#CoreOrgChartTree_idOrg').val();
			url = HTTP_HOST + '?r=orgManagement/getNodeParents';
		}
		else if ($(this).parents('form').attr('id') == 'courseCategory_move') {
			var selectedId = $(this).find('#LearningCourseCategoryTree_idCategory').val();
			url = HTTP_HOST + '?r=courseCategoryManagement/getNodeParents';
		}
		$.get(url, {'selectedId' : selectedId, 'currentId' : currentId}, function (data) {
			$('.nodeParents').html(data.content);
		}, 'json');
		return false;

	});

	$('.currentNodes > ul > li, .ajaxLinkNode').live('mouseenter', function (e) {
		$(this).addClass('highlighted-node');
	});
	$('.currentNodes > ul > li, .ajaxLinkNode').live('mouseleave', function (e) {
		$(this).removeClass('highlighted-node');
	});

	$(window).bind('ready resize', function(){
		setTimeout(function(){
			mainActionsResponsify();
		}, 0);
	}).resize();

});


// Make main actions (the square buttons at the top
// on almost all admin pages) equal height and vertically centered
var mainActionsResponsify = function(){

	// Equalize height for action items
	var maxHeight = 0;
	$('.main-actions li > div, .main-actions .info').each(function(){
		if($(this).height() > maxHeight){
			maxHeight = $(this).height();
		}
	}).css('height', maxHeight+'px');

	// Vertically center action item contents
	$('.main-actions li > div > a').each(function(){
		var el = $(this);
		var p = $(this).parent();

		if(p.height() > el.height()){
			// Only if child smaller than container
			el.css('padding-top', (p.height()-el.height())/2+'px');
		}
	});

	// Make clickable area full height
	$('.main-actions li > div > a').each(function(){
		var elPadding = $(this).outerHeight()-$(this).height();
		var parentH = $(this).parent().height();

		$(this).height(parentH - elPadding);
	});
};


function progressLineWidth() {
	var endWidth = $('.end-quantity').width();
	$('.progress-line-wrapper').css({"margin-right" : endWidth + 10});
};


function showHideTree() {
	$('.show-hide-tree').on('click', function () {

		if ($(this).hasClass('hide-tree')) {
			$('#tree-wrapper').hide();
			$(this).text(Yii.t('standard', '_EXPAND')).prepend('<span></span>').removeClass('hide-tree');
		} else {
			$('#tree-wrapper').show();
			$(this).text(Yii.t('standard', '_COLLAPSE')).prepend('<span></span>').addClass('hide-tree');
		}

	});
};


function OrgChartTree() {
	$('.nodeUl').children('li:last-child').addClass('last-child');
	$('.hasChildren').children('ul').hide();
	$('.rootNode').children('ul').show();
	$('.node-tree').children('li').show();
	$('.rootNode').next('ul').show();
	$('.open-close-link').click(function () {
		$(this).parent().next().toggle().parent().toggleClass('opened-tree');
	});
}
function autoSelectWidth() {
	var selectWidth = $('select').outerWidth(),
		selectStylerContainer = $('.jqselect.powerUserProfileSelect, #user-import-form-step2 .jqselect'),
		selectStyler = $('.powerUserProfileSelect .jq-selectbox__select,#user-import-form-step2 .jq-selectbox__select');
	selectStylerContainer.css('width',selectWidth);	
	selectStyler.css('width','auto');
};	

function stopScroll() {
	$('.jq-selectbox__dropdown ul').on('DOMMouseScroll mousewheel', function(ev){
			ev.stopPropagation();
	});
};