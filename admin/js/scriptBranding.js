$.fn.externalPageCallback = function(response) {
	if (response.success) {
		$(".modal.in").modal("hide");
		$.fn.yiiListView.update("external-pages-management-list");
	} else {
		$('.modal.in .modal-body').find('.error').remove();
		$('.modal.in .languages').before($('<div/>', { 'class': 'error', 'text': response.message }));
		console.warn(response.message);
	}
}

$.fn.editPageVisibilityCallback = function(response) {
    if (response.success) {
        $(".modal.in").modal("hide");
        //$.fn.yiiListView.update("external-pages-management-list");
    } else {
        $('.modal.in .modal-body').find('.error').remove();
        $('.modal.in .languages').before($('<div/>', { 'class': 'error', 'text': response.message }));
        console.warn(response.message);
    }
}

$.fn.schemeCallback = function(response) {
	if (response.success) {
		$(".modal.in").modal("hide");
		$.fn.yiiListView.update("schemes-management-list");
	} else {
		$('.modal.in .modal-body').find('.error').remove();
		$('.modal.in .modal-body').prepend($('<div/>', { 'class': 'error', 'text': response.message }));
		console.warn(response.message);
	}	
}

function externalPageFormInit() {
	var id = $('.modal.in .input-fields textarea').attr('id');
	initTinyMCE('#' + id, 275);
	populateAssignedLanguages();
	$('.modal.in select').styler();
}

var minImgUploader = false;
var minVideoUploader = false;
var minVideoFallbackUploader = false;
var removePrevFarbtasticListeners = false;
var detectedIE = false;

function detectIE() {
	var ua = window.navigator.userAgent;

	var msie = ua.indexOf('MSIE ');
	if (msie > 0) {
		// IE 10 or older => return version number
		return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
	}

	var trident = ua.indexOf('Trident/');
	if (trident > 0) {
		// IE 11 => return version number
		var rv = ua.indexOf('rv:');
		return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
	}

	var edge = ua.indexOf('Edge/');
	if (edge > 0) {
		// Edge (IE 12+) => return version number
		return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
	}

	// other browser
	return false;
}

function showLayoutImage(radio) {
	if ($('.login-images').length == 0) {
		return false;
	}

	var radio = radio || $('input[name="BrandingSigninForm[login_layout]"]:checked'),
		type;

	switch (radio.val()) {
		case 'layout1':
			type = 'side';
			break;
		case 'layout2':
			type = 'top';
			break;
		case 'layout3':
			type = 'bottom';
			break;
		case 'layout4':
			type = 'minimal';
			break;
	}

	if (type == 'minimal') {
		$('#branding-signin-form .blockable').block({
			message: "",
			timeout: 0,
			overlayCSS:  {
				backgroundColor: 	'#FFF',
				opacity:         	0.6,
				cursor:          	'auto'
			}
		});

		$('#branding-signin-form #show-hide-webpages-warning').show();
		$('#home-login-img .description-regular').hide();
		$('#home-login-img .description-minimal').show();

		$('#home-login-img .values-regular').hide();
		$('#home-login-img .values-minimal').show();
	} else {
		$('#branding-signin-form .blockable').unblock();
		$('#branding-signin-form #show-hide-webpages-warning').hide();
		$('#home-login-img .description-regular').show();
		$('#home-login-img .description-minimal').hide();

		$('#home-login-img .values-minimal').hide();
		$('#home-login-img .values-regular').show();
	}

	$('.login-images > div').hide();
	$('.login-images').find('.' + type).show();

	// Init the color picker tooltip
	$('#branding-signin-form .color-tooltip').tooltip({
		title: function() {
			return $(this).closest('.wrap').find('.farbtastic-tooltip').html();
		},
		trigger:  'focus',
		html: true
	});

	// Init farbtastic picker
	$('#branding-signin-form .color-tooltip').on('shown.bs.tooltip', function() {
		// Add color picker
		$('.colors .color-item input').each(function() {
			var _this = $(this);
			var _preview = $(this).parents('.wrap').find('.color-preview');

			var f = $.farbtastic($(this).parents('.wrap').find('.colorpicker'), function(data) {
				_this.val(data);
				_preview.css('background-color', data);
			})

			// Initializes colorpicker color
			f.setColor($(this).val());

			$(this).on('keyup', farbtasticUpdateCallback(f, $(this)));
		});
	});

	$('.colors').on('click', '.color-preview', function() {
		$(this).parents('.wrap').find('input').focus();
	});


	if ($('#minimal_login_type_color').is(':checked')) {
		$('.color-selector').show();
	} else {
		$('.color-selector').hide();
	}

	if ($('#minimal_login_type_image').is(':checked')) {
		$('.minimal-layout-image-selector').removeClass('visibility-hidden');
	} else {
		$('.minimal-layout-image-selector').addClass('visibility-hidden');
	}

	if ($('#minimal_login_type_video').is(':checked')) {
		$('.minimal-layout-video-selector').removeClass('visibility-hidden');
	} else {
		$('.minimal-layout-video-selector').addClass('visibility-hidden');
	}

	$('input[name="BrandingSigninForm[minimal_login_type]"]').on('change', function() {
		if ($('#minimal_login_type_color').is(':checked')) {
			$('.color-selector').show();
		} else {
			$('.color-selector').hide();
		}

		if ($('#minimal_login_type_image').is(':checked')) {
			$('.minimal-layout-image-selector').removeClass('visibility-hidden');
		} else {
			$('.minimal-layout-image-selector').addClass('visibility-hidden');
		}

		if ($('#minimal_login_type_video').is(':checked')) {
			$('.minimal-layout-video-selector').removeClass('visibility-hidden');
		} else {
			$('.minimal-layout-video-selector').addClass('visibility-hidden');
		}
	});

	detectedIE = detectIE();
	
	// Add PlUpload to the upload buttons here
	if (!minImgUploader) {
		if(detectedIE == false || detectedIE > 10){
			minImgUploader = addSignInTabUploadBtns('minimal_bg_img', ['png', 'jpg', 'jpeg', 'gif']);
		}else{
			addSignInTabUploadBtns('minimal_bg_img', ['png', 'jpg', 'jpeg', 'gif']);
		}
	}

	if (!minVideoUploader) {
		if(detectedIE == false || detectedIE > 10){
			minVideoUploader = addSignInTabUploadBtns('minimal_bg_video', ['mp4']);
		}else{
			addSignInTabUploadBtns('minimal_bg_video', ['mp4']);
		}

	}

	if (!minVideoFallbackUploader) {
		if(detectedIE == false || detectedIE > 10){
			minVideoFallbackUploader = addSignInTabUploadBtns('minimal_video_fallback_img', ['png', 'jpg', 'jpeg', 'gif']);
		}else{
			addSignInTabUploadBtns('minimal_video_fallback_img', ['png', 'jpg', 'jpeg', 'gif']);
		}
	}

}

function populateAssignedLanguages() {
	if ($('.section.signin').length == 0) {
		return false;
	}

	var count = 0;

	if ($('.modal.in').length > 0) {
		var set = $('.modal.in').find('.translations > div');
		var assigned = $('.modal.in').find('.assigned span');
	} else {
		var assigned = $('.assigned span');
		var set = $('.translations > div');
	}

	set.each(function() {
		if ($(this).find('input').val() != "" || $(this).find('textarea').val() != "") {
			count++;
		}
	});

	assigned.html(count);
}

function updateFarbtasticColor(farbtasticObj, input) {
	farbtasticObj.setColor(input.val());
}

function farbtasticUpdateCallback(farbtasticObj, input) {
	return function() {
		updateFarbtasticColor(farbtasticObj, input);
	}
}

function schemeFormInit() {
	$('.colors .color-item input').each(function() {
		var _this = $(this);
		var _preview = $(this).parents('.wrap').find('.color-preview');

		var f = $.farbtastic($(this).parents('.wrap').find('.colorpicker'), function(data) {
			_this.val(data);
			_preview.css('background-color', data);
		})

		// Initializes colorpicker color
		f.setColor($(this).val());

		// Remove previous farbtastic listeners
		if(removePrevFarbtasticListeners) {
			$(this).off('keyup');
		}

		$(this).on('keyup', farbtasticUpdateCallback(f, $(this)));
	});

	$('.color-item input').tooltip({ position: 'right' });

	$('.colors').on('click', '.color-preview', function() {
		$(this).parents('.wrap').find('input').focus();
	});
}

function useImageCallback() {
	var radio = $('.modal.in').find('.select-user-form-wrapper .sub-item input[type="radio"]:checked');

	$.ajax({
		type: 'get',
		url: HTTP_HOST + '?r=branding/getBackground&id=' + radio.val(),
		dataType: 'json',
		success: function(data) {
			$('.current.background').find('img').attr('src', data.imageUrl);
			$('.modal.in').modal('hide');

			$('.login-img-position').find('input[type="radio"]:checked').trigger('change');
		}
	})
}


function setExternalPagesListSortable() {
	$('#external-pages-management-list .items').sortable({
		handle: '.move',
		update: function(event, ui) {
			var rows = $('#external-pages-management-list .items > div');
			var ids = [];
			rows.each(function() { ids.push($(this).data('page-id')); })
			$.post(HTTP_HOST + '?r=branding/orderPages', { ids: ids });
		}
	});
}

function updateLanguageSelection() {
	var container = $(this).closest('.languages');

	var prevSelection = $(this).data('prevSelection');
	var selected = $(this).val();
	var inputTitle = container.find('.input-fields input[type="text"]');
	var inputText = container.find('.input-fields textarea');
	var tinyMCEContent = tinymce.activeEditor.getContent();

	if (prevSelection) {
		container.find('.translations .' + prevSelection).find('input[type="text"]').val(inputTitle.val());
		container.find('.translations .' + prevSelection).find('textarea').val(tinyMCEContent);
	}

	if (selected != 0) {
		var selectedLanguage = container.find('.translations .' + selected);
		inputTitle.val(selectedLanguage.find('input[type="text"]').val());
		tinymce.activeEditor.setContent(selectedLanguage.find('textarea').val());
	} else {
		inputTitle.val('');
		tinymce.activeEditor.setContent('');
	}

	$(this).data('prevSelection', $(this).val());
}

$(function(){
	$('input,select').styler();
	initTinyMCE('.input-fields textarea', 150);

    $('.admin-advanced-wrapper li > a').hover(
        function() {
            if (!$(this).hasClass('active'))
                $(this).addClass('active');
        },
        function() {
            if ( ! $(this).hasClass('hover-cancel'))
                $(this).removeClass('active');
        }
    );

	$(window).hashchange(function(){
		var hash = location.hash,
			hasStr = "",
			link = null;

		if(hash.length)
		{

			hashStr = hash.substr(1);
			link = $('#sidebar a.' + hashStr);

			$('#sidebar a').removeClass('active').removeClass('hover-cancel');
			link.addClass('active hover-cancel');

			if (link.data('loaded') == 0) {
				loadTabContent(link);
			}

			$('.tab-content > div').hide();
			$('.tab-content').find(hash).show();
			showLayoutImage();
			populateAssignedLanguages();
			$('.tab-content #selectedTab').val(hashStr);

			/*
			$('#external-pages-management-list .items').sortable({
				handle: '.move',
				update: function(event, ui) {
					var rows = $('#external-pages-management-list .items > div');
					var ids = [];

					rows.each(function() {
						ids.push($(this).data('page-id'));
					})

					$.post(HTTP_HOST + '?r=branding/orderPages', { ids: ids });
				}
			});
			*/
			setExternalPagesListSortable();
		}
	});
	
	if (location.hash == "") {
		location.hash = '#' + $('#sidebar a:first').data('tab');
	}

	$(window).hashchange();

	$('.language-selector').live('change', updateLanguageSelection);

	$('#branding-signin-form').live('submit', function() {
		$('.language-selector').trigger('change');
		return true;
	});

	$('.values .input-fields .title, .input-fields .text').live('blur', function() {
		$(this).closest('.languages').find('select.language-selector').trigger('change');
		populateAssignedLanguages();
	});

	$('.select-user-form-wrapper .item > div').live('click', function() {
	  $('.select-user-form-wrapper .item > div').removeClass('checked');
	  $(this).addClass('checked');
	  $(this).find('input[type="radio"]').prop('checked', true);
	  $(this).parent().find('.jq-radio').removeClass('checked');
	  $(this).find('.jq-radio').addClass('checked');
	});

    $('#schemes-management-list .preview-scheme').live('click', function(){
        var scheme = $(this).attr('data-url');
        $('#colorSchemeCssFile').attr("href", scheme)
        return false;
    });

	$('#main').on('click', '.list-view .toggle-active-state', function(event) {
		var _this = $(this);
		var listId = $(this).parents('.list-view').attr('id');

		$.get(HTTP_HOST + '?r=' + $(this).data('url'), { 'id': $(this).data('id') })
			.done(function(data) {
				if (data.success) {
					var li = _this.parent('li');

					if (li.hasClass('disabled')) {
						li.removeClass('disabled').addClass('enabled');
					} else {
						li.removeClass('enabled').addClass('disabled');
					}
                    
                    if (_this.attr('scheme-url')) {
                        $('#colorSchemeCssFile').attr("href", _this.attr('scheme-url'));
                    }

					li = null;
					_this = null;

					$.fn.yiiListView.update(listId);
				}
			});

		event.preventDefault();
	});

	$('#main').on('click', '.list-view .delete-link', function(event) {
		var _this = $(this);
		$.get(HTTP_HOST + '?r=' + $(this).data('url'), { 'id': $(this).data('id') })
			.done(function(data) {
				if (data.success) {
					_this.parents('.row').remove();
				}

				_this = null;
			});

		event.preventDefault();
	});

	$('input[name="BrandingSigninForm[login_layout]"]').on('change', function() {
		showLayoutImage($(this));
	})
});


function addSignInTabUploadBtns(uploadBtnId, extensions) {
	var allFilesSize = 0; //Current files size;
	var maxFilesSize = 10*1024*1024; //10MB
	var upUrl =  yii.urls.base + '/index.php?r=branding/handleFile&thread_id=47';

	var uploader = new plupload.Uploader({
		chunk_size: '10mb',
		runtimes: 'html5,flash,html4,gears,silverlight,browserplus',
		browse_button: 'pickfiles_'+ uploadBtnId, //minimal_bg_video',//'pickfiles',
		container: 'container_'+ uploadBtnId, //'container',
		max_file_size: '10mb',
		url : upUrl,
		flash_swf_url: yii.urls.base + '/../common/extensions/plupload/assets/plupload.flash.swf',
		silverlight_xap_url: '/plupload/js/plupload.silverlight.xap',
		multipart_params: {
				YII_CSRF_TOKEN: $('input[name=YII_CSRF_TOKEN]').val(),
				fileId: '',
				extensions: extensions,
			}
		});

	uploader.bind('Init', function (up, params) {
		//$('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
	});

	uploader.init();

	uploader.bind('BeforeUpload', function (up, files) {
	});

	uploader.bind('FilesAdded', function (up, files) {

		$('#fileserror').html("");
		$.each(files, function (i, file) {

			//Calculate new total files size!
			allFilesSize += file.size;

			//If we outweigh the files limits!
			if(allFilesSize > maxFilesSize){
				$('#fileserror').append([
					'<div>Error: '+Yii.t('forum', 'Max. file size {size} MB', {'{size}': 3}) + '</div>'
				].join(''));

				//Remove the uploaded file and change the files size!
				uploader.removeFile(file);
				allFilesSize -= file.size;
			} else {
				var fileSize = '';
				if (file.size != undefined) {
					fileSize = '(' + plupload.formatSize(file.size) + ')';
				}

				// Add file id as additional param in order to be used for temp file name(it is an unique hash)
				uploader.settings.multipart_params.fileId = file.id;

				$('#'+uploadBtnId+'_file').html(file.name+'&nbsp;<i class="fa fa-refresh fa-spin"></i>');
				$('input[name="BrandingSigninForm['+uploadBtnId+'_path]"]').val(file.id);
				$('input[name="BrandingSigninForm['+uploadBtnId+'_original_filename]"]').val(file.name);
			}
		});

		up.refresh(); // Reposition Flash/Silverlight

		uploader.start();
	});

	uploader.bind('UploadProgress', function (up, file) {
	});

	uploader.bind('Error', function (up, err) {
		Docebo.Feedback.show('error', Yii.t('app7020', 'Error') + ': ' + err.message);

		up.refresh(); // Reposition Flash/Silverlight
	});

	uploader.bind('FileUploaded', function (up, file, response) {
		try {
			$('#'+uploadBtnId+'_file .fa-refresh').remove();
			response = jQuery.parseJSON(response.response);

			if (response.error.code == '500'){

				Docebo.Feedback.show('error', Yii.t('app7020', 'Error') + ': ' + response.error.message);

				file.status = plupload.FAILED;


				$('#'+uploadBtnId+'_file').html('&nbsp;');
				$('input[name="BrandingSigninForm['+uploadBtnId+'_path]"]').val('');
				$('input[name="BrandingSigninForm['+uploadBtnId+'_original_filename]"]').val('');
			}
			else {
				// Success
				file.status = plupload.DONE;
			}
		}
		catch (e) {
			Docebo.log('Code Error: ' + e);
		}

		//$('#filelist .filename .fa-refresh').remove();
	});

	return uploader;
}


