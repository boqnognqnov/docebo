/**
 * A QuestionCategory controller Javascript compagnion/servant
 */
var QuestionCategory = function (options) {
	// Default settings
	this.settings = {};
	this.init(options);
};


(function ($) {

	/**
	 * Define prototype
	 */
	QuestionCategory.prototype = {

		init: function (options) {
			this.settings = $.extend({}, this.settings, options);
		},

		addListeners: function () {

			// Clicks on disabled SUBMIT buttons must be ignored
			$(document).on('click', '.disabled', function () {
				return false;
			});

			// Listen for SEARCH icon clicks
			$(".filters-wrapper .search-icon").on('click', function () {
				$("#questcat-management-form").submit();
			});

			// Handle a.auto-close
			$(document).on("dialog2.content-update", ".modal", function () {
				if ($(this).find("a.auto-close").length > 0) {
					$(this).find(".modal-body").dialog2("close");
				}
			});

			// ... and auto-close successes
			$(document).on("dialog2.content-update", '#questcat-modal, #delete-questcat-modal', function () {
				if ($(this).find("a.auto-close.success").length > 0) {
					$.fn.yiiGridView.update('questcat-management-grid', {
						data: $("#questcat-management-form").serialize()
					});
				}
			});

		}

	};

	$(function() {
		QuestCat.addListeners();
		bootbox.animate(false);
	});

}(jQuery));