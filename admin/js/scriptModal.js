$(function () {

	var config;

	$('.ajaxModal').live('click', function() {
		$(this).showModal();
	});

	/**
	 * Update Course Category Tree in Course Management
	 */
	$.fn.updateOrgTreeContent = function(data) {
		if (data.html) {
			$('.modal.in .modal-body').html(data.html);
		} else {
			$('.modal.in').modal('hide');
			if ($('#tree-wrapper').length) {
				$('#tree-wrapper').loadTree('', true, false);
			}
		}
	}

	/**
	 * Update Orgchart in Users Management
	 */
	$.fn.updateUsersOrgChartTree = function(data) {
		if (data.html) {
			$('.modal.in .modal-body').html(data.html);
		} else {
			$('.modal.in').modal('hide');
			if (typeof UserMan !== "undefined") {
				UserMan.loadOrgChartTree();
			}
		}
	}


  $.fn.updateUserContent = function(data) {
		if (data.html) {
			if ($("#assignUserContent").length)
				$("#assignUserContent").remove();//remove assign modal dialog content to avoid doubling elements with same ID
			$('input').styler();
			$('.modal.in .modal-body').html(data.html);
		} else {
			$('#users-management-grid-selected-items-list').val('');
			$('#users-management-grid').yiiGridView('update');
			// USERMAN-v2
			if (typeof UserMan !== "undefined") {
                UserMan.deSelectAllUsers();
				UserMan.updateUsersGrid();
			}
			removeModal();
		}
	}

    $.fn.updateUserContentAndFilters = function(data) {
        if (data.html) {
            if ($("#assignUserContent").length)
                $("#assignUserContent").remove();//remove assign modal dialog content to avoid doubling elements with same ID
            $('input').styler();
            $('.modal.in .modal-body').html(data.html);
        } else {
            $('#users-management-grid-selected-items-list').val('');
            $('#users-management-grid').yiiGridView('update');
            // USERMAN-v2

            if (typeof UserMan !== "undefined") {
                UserMan.selectedUsers = {};
                UserMan.updateUsersGrid();
                UserMan.updateSelectionCounter();
                $(".userman-deselect-all").click();
            }

            removeModal();
        }
    }

  $.fn.updateCourseContent = function(data) {
    if (data.html) {
      $('.modal.in .modal-body').html(data.html);
    } else {
      $('#course-management-grid-selected-items-list').val('');
      if ($('#course-management-grid').length > 0) {
        $('#course-management-grid').yiiGridView('update');
      }
      $('.modal.in').modal('hide');
      setTimeout(function() {
        $('#process-new-course').click();
      }, 300);
    }
  };

  $.fn.updateSliderContent = function() {
    var id = $(this).find('#LearningCourse_idCourse').val();
    $.post(HTTP_HOST + '?r=courseManagement/getSlidersContent', {'id' : id}, function(ajaxData){
      if (ajaxData.content) {
        $('.modal.in').find('.courseSlider').html(ajaxData.content);
        $('input, select').styler();
      }
    });
  }

	/**
	 * A more abstract version of $.fn.updateSliderContent
	 */
	$.fn.updateSliderCarousel = function() {
		// $('.loSlider') is the context here
        var thisdata = null;
        if ($(this).data() == null) {
            thisdata = $('.loSlider');
        } else {
            thisdata = $(this);
        }
		var _this = thisdata;

		var loId = thisdata.data('lo-id');
		var courseId = thisdata.data('idcourse');

		var ajaxUrl = thisdata.data('refresh-url');

		if(!ajaxUrl){
			console.error('No ajax refresh URL defined');
			return;
		}

		// if(!courseId){
		// 	console.error('Invalid course ID provided as data parameter to slider carousel');
		// 	return;
		// }

		$.post(ajaxUrl, {'loId' : loId, 'course_id': courseId}, function(ajaxData){
			if (ajaxData.content) {
				_this.html(ajaxData.content);
				$('input,select', _this).styler();
			}
		}, 'json');
	}

  $.fn.updateAjaxCarousel = function(){
      var modal = $('.custom-thumbnails-carousel');
      var count = $.trim($('#user-images-count').html());
      var id_object = modal.find('#objectId').val();
      var nextPage = 0;
      var currentPageVal =  modal.find('#currentPage').val();
      var courseId = modal.find("#courseId").val();
      if(!currentPageVal){
        currentPageVal = 0;
      }
      var selectedImageId = '';
      if(modal.find('.sub-item.checked').find('.jq-radio.checked').parent().find('input').val()){
        selectedImageId = modal.find('.sub-item.checked').find('.jq-radio.checked').parent().find('input').val();
        modal.find('#selectedImageId').attr('value', selectedImageId);
      }else{
        selectedImageId = modal.find('#selectedImageId').val();
      }

      modal.find('.custom-thumbnails-carousel  .carousel-inner').html('<div class="ajaxloader" style="display: block;"></div>');
      $.ajax({
        url: $("#thumbnailRequestUrl").val(),
        type: "POST",
        data: {
          pageId: currentPageVal,
          totalCount: count,
          course_id: courseId,
          selectedImageId: selectedImageId,
          id_object: id_object
        },
        success: function(result){

          var imagesCount = parseInt(count) + 1;
          $('#user-images-count').text(imagesCount);


          var courseId = modal.find('.carousel-inner').html(result.html);
          if(result.currentPage){
            modal.find('#currentPage').attr('value', result.currentPage);
          }else{
            modal.find('#currentPage').attr('value',0);
          }
          $('.select-user-form-wrapper .item > div').live('click', function () {
            $('.select-user-form-wrapper .item > div').removeClass('checked');
            // $('.thumbnailSlider .sub-item').removeClass('checked');

            $(this).addClass('checked');

            // $(this).find('input[type="radio"]').trigger('click');
            $(this).find('input[type="radio"]').prop('checked', true);
            $(this).parent().find('.jq-radio').removeClass('checked');
            $(this).find('.jq-radio').addClass('checked');
          });
        },
        error: function() {
        }
      }).done(function(){
        var modal = $('.modal.edit-lo-dialog');
        var count = modal.find('#totalPages').val();
        var cnt = parseInt(count) + 1;
        $('#totalPages').val(cnt);
      });
  };

  $.fn.updateEnrollmentContent = function(data) {
    if (data.html) {
      $('.modal.in .modal-body').html(data.html);

      if($('.modal.in .modal-body').find('.auto-close').length > 0){
        $('.modal.in').modal('hide');
        removeModal();
      }
    } else {
			if (!data.preserveSelection) $('#course-enrollment-list-selected-items-list').val('');
//      $('#course-enrollment-list').yiiGridView('update');
      $.fn.yiiListView.update('course-enrollment-list');

      $('.modal.in').modal('hide');
      removeModal();
    }
  };

	$.fn.updateSessionEnrollmentContent = function(data) {

    if (data.html) {
      $('.modal.in .modal-body').html(data.html);

	    if($('.modal.in .modal-body').find('.auto-close').length > 0){
		    $('.modal.in').modal('hide');
	    }
    } else {
		$('#session-enrollment-list-selected-items-list').val('');
      	$.fn.yiiListView.update('session-enrollment-list');
		$('.modal.in').find(".btn-cancel").trigger('click');

	    if(data.status && data.status==='error'){
		    if(data.message){
			    Docebo.Feedback.show('error', data.message);
		    }
	    }
    }
  };

  $.fn.updateWaitingEnrollmentContent = function(data) {
    if (data.html) {
      $('.modal.in .modal-body').html(data.html);
    } else {
      if($.type(data.data) != 'undefined'){
        if(data.data.error){
          Docebo.Feedback.show('error', data.data.error);
        }
      }
	  $('#course-waitingenrollment-grid-selected-items-list').val('');
      $('#course-waitingenrollment-grid').yiiGridView('update');
      $('.modal.in').modal('hide');
    }
  };

  $.fn.updateFieldContent = function(data) {
    if (data.html) {
      $('.modal.in .modal-body').html(data.html);
    } else {
      $('#fields-grid').yiiGridView('update');
      $('.modal.in').modal('hide');
      //initFieldSortable();
    }
  };

  $.fn.updateGroupContent = function(data) {
    if (data.html) {
      $('.modal.in .modal-body').html(data.html);
    } else {
      $('#group-management-grid').yiiGridView('update');
      $('.modal.in').modal('hide');
        setTimeout(function() {
            $('#process-new-group').click();
        }, 300);
    }
  }

  $.fn.updateCurriculaContent = function(data) {
    if (!data.html) {
      $('#curricula-management-grid').yiiGridView('update');
      $('.modal.in').modal('hide');
    }
  }

  $.fn.updateCurriculaCoursesContent = function(data) {
    if (!data.html) {
        if (!data.preserveSelection)
            $('#curricula-course-list-selected-items-list').val('');

        $.fn.yiiListView.update('curricula-course-list');
        $('.modal.in').modal('hide');
    }
  }

  $.fn.updateCurriculaUsersContent = function(data) {
    if (!data.html) {
			$.fn.yiiListView.update('curricula-user-list');
      $('.modal.in').modal('hide');
    }
  }

    $.fn.updateCurriculaUsersContent_new = function(data) {
        if (data.html) {
            $('.modal.in .modal-body').html(data.html);
        }else{
            //hide dialog and manually update selector
            $('#curricula-user-list-all-items-list').val('');
            $.fn.yiiListView.update('curricula-user-list');
            $('.modal.in').modal('hide');
            $('#curricula-user-massive-action').val('');
            $('#curricula-user-list-selected-items-count').text('0');

            //Restore the page selector
            $("a.deselect-all").removeClass('active');
            $("a.deselect-this").removeClass('active');
            $("a.select-all").addClass('active');
            $("a.select-this").addClass('active');
        }

    }

  $.fn.updateGroupMemberContent = function(data) {
    if (data.html) {
    	$('.modal.in .modal-body').html(data.html);
    } else if (data.status == 'saved') {
    	$('#group-member-management-list-selected-items-list').val('');
    	$.fn.yiiListView.update('group-member-management-list');
//      $('#group-member-management-grid').yiiGridView('update');
    	$('.modal.in').modal('hide');
    } else {
//      $('#group-member-management-grid').yiiGridView('update');
    	$('#group-member-management-list-selected-items-list').val('');
    	$.fn.yiiListView.update('group-member-management-list');
    	$('.modal.in').modal('hide');
    }
    if (data.status == 'success') {
    	$('.modal.in .modal-footer').hide();
    	$('.modal.in').addClass('success');
//      $('#group-member-management-grid').yiiGridView('update');
    	$('#group-member-management-list-selected-items-list').val('');
    	$.fn.yiiListView.update('group-member-management-list');
    }
    updateSelectedCheckboxesCounter('group-member-management-list');
  }

  $.fn.initGroupMemberData = function(formData, jqForm) {
    formData.push({
      name: 'select-user-grid-selected-items',
      value: $('.modal.in').find('#select-user-grid-selected-items-list').val()
    });
    formData.push({
      name: 'select-group-grid-selected-items',
      value: $('.modal.in').find('#select-group-grid-selected-items-list').val()
    });
    formData.push({
      name: 'select-course-grid-selected-items',
      value: $('.modal.in').find('#courseEnroll-course-grid-selected-items-list').val()
    });
    return true;
  }

  $.fn.beforeCourseSubmit = function(formData, jqForm) {
    formData.push({
      name: 'LearningCourse[description]',
      value: tinymce.activeEditor.getContent()
    });

    return true;
  };

  $.fn.beforeCourseCreateSubmit = function(formData, jqForm) {
    return false;
  };

  $.fn.updatePowerUserContent = function(data) {
    if (data.html) {
      $('.modal.in .modal-body').html(data.html);
    } else {
      $('#power-user-management-grid').yiiGridView('update');
      $('.modal.in').modal('hide');
    }
  }

  $.fn.updatePowerUserAsignUsersContent = function(data) {
    if (data.html) {
      $('.modal.in .modal-body').html(data.html);
    } else {
			$('#users-management-grid-selected-items-list').val('');
			$('input[name="assign-power-user-management-list-selected-items-list"]').val('');
			$('#assign-power-user-management-list-selected-items-count').html('0');
			$.fn.yiiListView.update('assign-power-user-management-list');
      $('.modal.in').modal('hide');
    }
  }

	$.fn.updatePowerUserCoursesList = function(data) {
		if (data.html) {
			$('.modal.in .modal-body').html(data.html);
		} else {
			$('#poweruser-courses-management-list-selected-items-list').val('');
			$.fn.yiiListView.update('poweruser-courses-management-list');
			$('.modal.in').modal('hide');
		}
	}

	$.fn.updatePowerUserLocationsList = function(data) {
		if (data.html) {
			$('.modal.in .modal-body').html(data.html);
		} else {
			$('#poweruser-locations-management-list-selected-items-list').val('');
			$.fn.yiiListView.update('poweruser-locations-management-list');
			$('.modal.in').modal('hide');
		}
	}

  $.fn.updateProfileContent = function(data) {
    if (data.html) {
      $('.modal.in .modal-body').html(data.html);
    } else {
      $('#profile-management-grid').yiiGridView('update');
      $('.modal.in').modal('hide');
       removeModal();
    }
  }

  $.fn.updateLabelContent = function(data) {
    if (data.html) {
      $('.modal.in .modal-body').html(data.html);
    } else {
      $('#label-management-grid').yiiGridView('update');
      $('.modal.in').modal('hide');
    }
  }


  $.fn.createReportFinishCallback = function(data) {
    if (data.redirect) {
      window.location = data.redirect;
    } else {
      $('#report-management-grid').yiiGridView('update');
      $('.modal.in').modal('hide');
      $('#bootstrapContainer').remove();
      $('.modal-backdrop').remove();
    }
  }

  $.fn.updateReportContent = function(data) {
    if (data.redirect) {
      window.location = data.redirect;
    } else if (data.html) {
      $('.modal.in .modal-body').html(data.html);
    } else {
      $('.modal.in').modal('hide');
      $('#bootstrapContainer').remove();
      $('.modal-backdrop').remove();
      $('#report-management-grid').yiiGridView('update', {authorsUpdate : true});
    }
  }

  $.fn.beforeReportSendSubmit = function(formData, jqForm) {
    formData.push({
      name: 'EmailForm[message]',
      value: tinymce.activeEditor.getContent()
    });

    return true;
  }

  $.fn.updateReportSendContent = function(data) {
    if (data.redirect) {
      window.location = data.redirect;
    } else if (data.html) {
      $('.modal.in .modal-body').html(data.html);
      if ($('.modal.in .modal-body').find(':checkbox').prop('checked')) {
        setTimeout(function() { 
          showConfirmButton();
        }, 1000);
      }
    } else {
      $('.modal.in').modal('hide');
    }
  }

  $.fn.updateExternalPagesContent = function(data) {
    if (data.html) {
      $('.modal.in .modal-body').html(data.html);
    } else {
      $('.modal.in').modal('hide');
      $.fn.yiiListView.update('external-pages-management-list');
    }
  }

  $.fn.updateCourseCatalogContent = function(data) {
    if (data.html) {
      $('.modal.in .modal-body').html(data.html);
    } else {
      $('#catalogue-management-grid').yiiGridView('update');
      $('.modal.in').modal('hide');
    }
  };

  $.fn.courseCatalogBeforeSubmit = function(formData, jqForm) {
    formData.push({
      name: 'LearningCatalogue[description]',
      value: tinymce.activeEditor.getContent()
    });
  };

  function courseCatalogLoadCallback() {
    initTinyMCE('#courseCatalog_form textarea', 128);
  }

  $.fn.updateCourseCatalogAssignUsersContent = function(data) {
    if (data.html) {
      $('.modal.in .modal-body').html(data.html);
    } else {
      $('#assign-course-catalog-management-user-list-selected-items-list').val('');
      $.fn.yiiListView.update('assign-course-catalog-management-user-list');
      $('.modal.in').modal('hide');
    }
  }

  $.fn.initCourseCatalogAssignCoursesData = function(formData, jqForm) {
    formData.push({
      name: 'course-catalog-management-assign-course-grid-selected-items',
      value: $('#course-catalog-management-assign-course-grid-selected-items-list').val()
    });
    return true;
  }

  $.fn.updateCourseCatalogAssignCoursesContent = function(data) {
    if (data.html) {
      $('.modal.in .modal-body').html(data.html);
    } else {
      $('#assign-course-catalog-management-course-list-selected-items-list').val('');
        setTimeout(function(){
            $.fn.yiiListView.update('assign-course-catalog-management-course-list');
        }, 1000);
      $('.modal.in').modal('hide');
    }
  }

  $.fn.updateCouponAssignCoursesContent = function(data) {
    if (data.html) {
      $('.modal.in .modal-body').html(data.html);
    } else {
      $('#coupon-assigned-courses-list-selected-items-list').val('');

      var deSelectPage = $($("#coupon-assigned-courses-list-all-items").parents('.list-selections').find('.deselect-this'));
      var deSelectAll = $($("#coupon-assigned-courses-list-all-items").parents('.list-selections').find('.deselect-all'));

      $.fn.yiiListView.update('coupon-assigned-courses-list');
      $('.modal.in').modal('hide');

      deSelectPage.trigger('click');
      deSelectAll.trigger('click');
    }
  }
  
  $.fn.createReportBeforeSubmit = function(formData, jqForm) {
    if ($('.modal.in .courseEnroll-page-users').length || $('.modal.in .courseEnroll-course-table').length || $('.modal.in .users-selector').length) {
      $(document).initGroupMemberData(formData, jqForm);
    }
    return true;
  }

  $.fn.externalPageFormPrepare = function(formData, jqForm) {
	  var lang = jqForm.find('select').val();
	  formData.push({
		  name: 'LearningWebpageTranslation['+lang+'][description]',
		  value: tinymce.activeEditor.getContent()
	  });
	  formData.push({
		name: 'LearningWebpageTranslation['+lang+'][title]',
		value: jqForm.find('.title').val()
	  });
  }


  function createReportBeforeLoadingContent(config) {
    return true;
  }

  function createReportAfterLoadingContent() {
    replacePlaceholder(); 
    applyTypeahead($('.typeahead'));
    $('.input-append.date').bdatepicker();
    if ($('.modal.in').find('.grid-view').length > 0) {
      afterGridViewUpdate($('.modal.in').find('.grid-view').prop('id'));
    }
  }

  $.fn.beforeCertificateSubmit = function(formData, jqForm) {
    formData.push({
      name: 'LearningCertificate[description]',
      value: tinymce.activeEditor.getContent()
    });
    
    $('#bootstrapContainer .btn-submit').addClass('disabled');

    return true;
  }

  $.fn.updateCertificateContent = function(data) {
    if (data.html) {
      $('.modal.in .modal-body').html(data.html);
    } else {
      $('#certificate-management-grid').yiiGridView('update');
      $('.modal.in').modal('hide');
    }
    $('#bootstrapContainer .btn-submit').removeClass('disabled');
  }

function generateRaportVerifyInitData(config) {
    var button = $('#handler-' + config.id);
    var userInput = button.parent().find('input[id="advanced-search-user-report"]');

    if(userInput.length){
        var input = userInput;
    }else{
        var input = button.parent().find('input[id="idCourse"]');
		if(!input.length)
        	var input = button.parent().find('input[id="advanced-search-course-report"]');
    }

  var data = input.val();
  if (data == '') {
    input.css('border-color', '#ff0000');
    return false;
  }
  input.css('border-color', '#E4E6E5');
  config.modalRequestData = {'id':data};
  return true;
}

function setCourseSummarySessionId(config) {


    var button = $('#handler-' + config.id);
    var sessionId = button.data('session-id');
    var courseId = button.data('course-id');
    if(courseId){
        config.modalRequestData['id'] = courseId;
    }
    button.data('course-id', false);
    config.modalRequestData['session_id'] = sessionId;

    return true;
}

function hideOtherModals(config){
 
    $(".modal").not('#'+config.id).modal('hide');
    $(".modal").not('#'+config.id).remove();
    return true;
}

  /*******************************************************************************************************************/
    // buttons callbacks

	function allModalCallback(modalId) {

		var stylerOptions = (typeof(Yii) !== 'undefined'
			? {browseText: Yii.t('course_management', 'CHOOSE FILE')}
			: {});

		// Style ONLY elements inside modal, if ID is provided
		if (typeof modalId == "string") {
			$('#' + modalId).find('input,select').styler(stylerOptions);
		}
		else {
			$('input, select').styler(stylerOptions);
		}
		stopScroll();
		modalPosition();

		$(window).resize(function () {
			modalPosition();

		}).trigger('resize');
		return true;
	}



  /*******************************************************************************************************************/
// Bootstrap Ready functions

	$.fn.htmlCallback = function (html, callback) {
		this.html(html);
		// run the callback (if it is defined)
		if (typeof callback == "function") {
			callback();
		}
	}


	$.fn.getConfig = function () {
		var url = ($(this).data('url').substr(0, 4) == 'http') ? $(this).data('url') : HTTP_HOST + '?r=' + $(this).data('url');
		if (!$(this).data('id')) {
			var key = getRandKey();
			$(this).data('id', 'modal-' + key);
			$(this).attr('id', 'handler-modal-' + key);
			url += '&firstShow=1';
		}

		return {
			'id': $(this).data('id'),
			'modalClass': $(this).data('modal-class'),
			'modalTitle': $(this).data('modal-title'),
			'modalUrl': url,
			'modalRequestType': $(this).data('modal-request-type'),
			'modalRequestData': $(this).data('modal-request-data'),
			'buttons': $(this).data('buttons'),
			'beforeLoadingContent': $(this).data('before-loading-content'),
			'afterLoadingContent': $(this).data('after-loading-content'),
			'beforeSubmit': $(this).data('before-submit'),
			'afterSubmit': $(this).data('after-submit')
		}
	};

	$.fn.showModal = function (initConfig) {

		var _this = $(this);
		if (typeof initConfig != 'undefined') {
			config = initConfig;
		} else {
			config = $(this).getConfig();
		}
		$(this).getBootstrapModal(config);

		if (config.beforeLoadingContent && config.beforeLoadingContent.length) {
			if (!processCallback(config.beforeLoadingContent)) {
				return false;
			}
		}

		$.ajax({
			type : config.modalRequestType,
			url : config.modalUrl,
			data : config.modalRequestData
		}).done(function (data) {

			$('#' + config.id + ' .modal-body').html(data.html);
			if (data.modalTitle !== undefined) {
				$('#' + config.id + ' .modal-header h3').html('' + data.modalTitle);
			}
			if (data.confirmBtnTitle !== undefined) {
				$('#' + config.id + ' .btn-submit').html(data.confirmBtnTitle);
			}
			if (data.cancelBtnTitle !== undefined) {
				$('#' + config.id + ' .btn-cancel').html(data.cancelBtnTitle);
			}

			$('#' + config.id).one('shown', function (e) {
				// stop if this not modal event (tab pane)
				if (e.relatedTarget) {
					return;
				}

				if (config.afterLoadingContent && config.afterLoadingContent.length) {
					processCallback(config.afterLoadingContent, data);
				}

				allModalCallback(config.id);
			});

			$('#' + config.id).modal('show');

		}, 'json');
    
    return true;
  };

  $.fn.getBootstrapModal = function (config) {
    var bootstrapContainer = $('#bootstrapContainer');
    if (!bootstrapContainer.length) {
      bootstrapContainer = $('<div/>', { id: 'bootstrapContainer' });
      $('body').append(bootstrapContainer);
    }
    if (!$('#' + config.id).length) {
      bootstrapContainer.createBootstrapModal(config);
    }
    return true;
  };

  
  var defaultCallbacks = {
    'cancel': function () {
    	
        $('.modal.in').modal('hide');

        // Hiding only is destructive although it may seem it saves resources and time for next loading.
        // Javascript loaded with modal HTML starts intefering and everything is ruined! 
        // So, at least for Reports (where the problem was spotted), remove the following elemenents
        $('#bootstrapContainer').remove();
    },
    'submit': function () {
    	
      var button = arguments[0];

      if (typeof button.formId != 'undefined') {
        var form = $('.modal.in').find('#' + button.formId);
      }

      if (typeof form == 'undefined' || form.length == 0) {
       	// FUCK-UP Fix: 
      	// Commented code returns 2 or more forms, i.e. ALL forms in the modal, including Grid Search form (ajax-grid-form).
      	// This makes this code to execute 2 or more Submits, consequently executing callbacks.  Abso-fucking-lutely Quartz attack!
    	//
    	// Addendum: Yes, but looks like ALL (or many?) FORMS are passng-through this callback in a complex callback setting (not clear to me yet).. 
    	// And there is a little chance to know WHICH form is actually called.... class/id naming is just untraceable 
      	var form = $('.modal.in').find('form');
        // var form = $('.modal.in').find('form:not(.ajax-grid-form)');
      	
      	// Another try...: the next code covers scenario: Report Creation -> Course USers -> Select courses dialog
      	if (form.length == 2) {
      		if ($(form[1]).attr('id') == 'select-user-form') {
      			// Lets show some debug and watch the console.. if we see this in another scenario .. will check again
      			Docebo.log('scriptModal.js double form patch');
      			form.splice(0,1);  
      		} 
      	}
      }
      
      var url = form.attr('action');
      if (!url) {
        url = config.modalUrl;
      }
      
      var options = {
        beforeSubmit: showRequest,
        success     : showResponse,
        url         : url,
        type        : 'post',
        dataType    : 'json'
      };
      form.ajaxForm(options).submit();
    }
  }

  // not submit form by key enter
  $('.modal.in form').live("keydown", function(e) {
    var code = e.keyCode || e.which;
    if (code == 13) {
      e.preventDefault();
      return false;
    }
  });

	function processCallback(callback, data) {
		if (typeof callback != 'undefined') {
			if (callback.match(/^function.*$/)) {
				callback = '(' + callback + '())';
			}
			return eval(callback);
		}
		return true;
	}

  function processButtonsCallback(button) {
    return function () {
		  $('#submitFlag').val(true);
		  $('#coursePathSubmitFlag').val(true);
      if ($(this).hasClass('disabled')) {
		  return false;
      }

      if (processCallback(button.callback) !== false && typeof defaultCallbacks[button.type] != 'undefined') {
        defaultCallbacks[button.type](button);
      }
    }
  }

  $.fn.generateButtons = function (buttons) {
    var buttonsContent = [];
    for (i = 0; i < buttons.length; i++) {
      var key = buttons[i].type;
      var btnClass = buttons[i].btnClass || '';
      buttonsContent.push(
        $('<a/>', {'class': 'btn btn-' + key + ' ' + btnClass, 'text': buttons[i].title, 'href': 'javascript:void(0);', 'data-callback': key + '-testCallback'})
          .click(processButtonsCallback(buttons[i]))
      );
    }
    return this.append(buttonsContent);
  }

  $.fn.createBootstrapModal = function (config) {
    var modalContainer = $('<div/>', { 'id': config.id, 'class': 'modal hide fade ' + config.modalClass});
    var modalFooter = $('<div/>', {'class': 'modal-footer'}).generateButtons(config.buttons);

    modalContainer
      .append($('<div/>', {'class': 'modal-header'})
        .append($('<button/>', {'type': 'button', 'class': 'close btn-close', 'text': 'x', 'data-dismiss': 'modal'}))
        .append($('<h3/>', {'text': config.modalTitle }).prepend($('<span/>')))
        .append($('<div/>', {'class': 'contentLoading'}))
      )
      .append($('<div/>', {'class': 'modal-body'}))
      .append(modalFooter);

    $(this).append(modalContainer);
    return true;
  };

  /*******************************************************************************************************************/

  function showRequest(formData, jqForm, options) {
//    var queryString = $.param(formData);

    if (config.beforeSubmit && config.beforeSubmit.length) {
      var returnRequest = $(this)[config.beforeSubmit](formData, jqForm);
//      processCallback(config.beforeSubmit);
    }

    showLoading('contentLoading');
    return returnRequest;
  }

  function showResponse(responseText, statusText, xhr, $form) {
	  
    if (config.afterSubmit && config.afterSubmit.length) {
      var callback =
          $(this)[config.afterSubmit];
      if (!callback)
          window[config.afterSubmit](responseText);
      else
          $(this)[config.afterSubmit](responseText);
    }
    if (config.afterLoadingContent && config.afterLoadingContent.length) {
      setTimeout(function () {
        processCallback(config.afterLoadingContent, responseText);
        allModalCallback(config.id);
      }, 500);
    }
    hideLoading('contentLoading');
  }


});

/*******************************************************************************************************************/
//    Empty


/*******************************************************************************************************************/
//END READY
/*******************************************************************************************************************/
/*******************************************************************************************************************/


/*******************************************************************************************************************/

function showLoading(className) {
  $('.' + className).show();
}

function hideLoading(className) {
  $('.' + className).hide();
}

function getRandKey() {
  return Math.floor(Math.random() * 10000);
}


/**
 * This is ugly, but is the only way to clean the DOM from remaining modal that interfere with NEXT modals
 * Sorry.
 */
function removeModal() {
      if ($('.modal.in').attr("id") && $('.modal.in').attr("id") == "reinsertCCDataModal") {
        return; //do not close modal Reinsert Credit Card Modal
      }

      $('.modal.in').modal('hide');
      $('.modal.in').remove();
      $('#bootstrapContainer').remove();
      //$('.modal-backdrop').remove();

}

/* Fully remove modal container from DOM */

// upon pressing ESC
$('.modal.in form').live("keydown", function(e) {
    var code = e.keyCode || e.which;
    if(code == 27) {
        removeModal();
    }
});

// upon pressing the upper x button
$('.modal.in').find("[data-dismiss='modal']").live('mousedown', function () {
    removeModal();
});

// upon clicking outside of the panel
$('.modal-backdrop').live('mousedown', function () {
    removeModal();
});

/*******************************************************************************************************************/
