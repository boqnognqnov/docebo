$(document).ready(function () {

  var treeWrapper = $('#tree-wrapper') || false;
  if ($('#tree-wrapper').length) {
    treeWrapper.loadTree('', true, false);
  }

	if($('.ajaxLinkNode.checked-node').length > 0){
		toggleShowChildNodeUsers($('.ajaxLinkNode.checked-node'));
	}else{
		// No node was clicked in the org chart. Assume that root node
		// was clicked and hide the "show users from descendants - yes/no" switch
		// for now. It should reappear when a child node is clicked
		// (by invoking toggleShowChildNodeUsers())
		$('.children-node-content').css('opacity', '0');

		// A bit hacky, but looks ugly otherwise
		$('.group.select-users').css('border-left', '0');
	}

  /************************************************************************************************************/

    // Modal content will be loaded via AJAX request the next time
    // after closing it manually with close buttons
  $('.modal.in').find("[data-dismiss='modal']").live('mousedown', function () {
    $(this).parents('.modal.in').data('firstShow', true);
  });
  $('.modal.in').find(".confirm-btn").live('mousedown', function () {
    $(this).parents('.modal.in').data('firstShow', true);
  });

  /************************************************************************************************************/

  // Performs AJAX request with a callback if provided
  $(document).on('click', '.ajaxLink', function () {
    var url = $(this).attr('href');
    var callback = $(this).data('callback');
    $.get(url, {}, function (data) {
      if (callback) {
        processCallback(callback);
      }
    }, 'json');
    return false;
  });

  // Filters for Grid or ListView, submit form on change
  $('.ajaxChangeSubmit').on('change', function () {
    var form = $(this).closest('form');

    if (form.length) {
      form.submit();
    }
  });

  $('.ajaxLinkNode').live('click', function () {
    var url = $(this).data('url');
    $('.nodeTree-margin').removeClass('checked-node');
    $(this).addClass('checked-node');
    $('.popover.in').prev().popover('hide');
    
    if ($('#tree-wrapper').length) {
    	$('#tree-wrapper').loadTree(url, false, true);
    }

	toggleShowChildNodeUsers($(this));
    return false;
  });

  if ($('#tree-wrapper').length > 0) {
    $('.hasChildren .collapse-node').live('click', function (e) {
      var nodeUl = $(this).parents('.nodeTree-border').next();
      nodeUl.toggle();
      nodeUl.prev().find('.collapse-node').toggleClass('active');
      $('.popover.in').prev().popover('hide');
      e.stopPropagation();
    });
    $('#tree-wrapper .popover-trigger').live('click', function (e) {
      e.stopPropagation();
    });
  }

  $('#CoreOrgChart_lang_code, #CoreField_lang_code, #LearningCourseCategory_lang_code').live('change', function () {
    $('.languagesName').removeClass('show').addClass('hide');
    if ($('#NodeTranslation_' + $(this).val()).length > 0) $('#NodeTranslation_' + $(this).val()).addClass('show').removeClass('hide');
    else $('#CoreOrgChart_' + $(this).val()).addClass('show').removeClass('hide');
    $('.show > input').focus();
  });
        /* Additional fields in Courses */
    $('#CoreOrgChart_lang_code, #LearningCourseFieldTranslation_lang_code, #LearningCourseCategory_lang_code').live('change', function () {
        $('.languagesName').removeClass('show').addClass('hide');
        if ($('#NodeTranslation_' + $(this).val()).length > 0) $('#NodeTranslation_' + $(this).val()).addClass('show').removeClass('hide');
        else $('#CoreOrgChart_' + $(this).val()).addClass('show').removeClass('hide');
        $('.show > input').focus();
    });

  $('.orgChart_translation').live('keyup', function () {
    var assignedCount = 0;
    $('.orgChart_translationsList .orgChart_translation').each(function (index) {
      if ($(this).val() != '') {
        assignedCount++;
      }
    });
    $('#orgChart_assignedCount').html(yiiLang.standardFilledLanguages+': <span>' + assignedCount + '</span>');
  });


  /******************************************************************************/
    // $('#LearningLabel_color').live('change', function() {
    //   $('.modal.in').find('.preview-content').css('background-color', $(this).val());
    // });

  $('#LearningLabel_lang_code').live('change', function () {
    $('.languagesName').removeClass('show').addClass('hide');
    $('.modal.in').find('#labelTranslation_' + $(this).val()).addClass('show').removeClass('hide');

    $('.modal.in').find('.preview-content-title').html($('.modal.in .label_title:visible').val());
    $('.modal.in').find('.preview-content-description').html($('.modal.in .label_description:visible').val());

    // $('.show > input').focus();
  });

  $('.modal.in .label_title').live('keyup', function () {
    $('.preview-content-title').html($(this).val());
    updateLabelCounter();
  });

  $('.modal.in .label_description').live('keyup', function () {
    $('.preview-content-description').html($(this).val());
    updateLabelCounter();
  });

  function updateLabelCounter() {
    var assignedCount = 0;
    $('.modal.in .label_translationsList > div').each(function (index) {
      if ($(this).find('input').val() != '' || $(this).find('textarea').val() != '') {
        assignedCount++;
      }
    });
    $('.modal.in #label_assignedCount').html(yiiLang.standardFilledLanguages+': <span>' + assignedCount + '</span>');
  }

  if ($('#label-management-grid tbody').length > 0) {
    labelSortInit();
  }

  /******************************************************************************/


  $('.libraryLink').live('click', function () {
    $('.libraryLink').removeClass('active');
    $(this).addClass('active');
    $('.thumbnailSlider .carousel').hide();
    $('.modal.fade.in').find($(this).data('id')).show();
    return true;
  });

  $('.powerUserProfileSelect').hide();
  $('.powerUserProfileLink').live('click', function () {
    $(this).parent().parent().removeClass('selected');
    $('.powerUserProfileSelect').hide();
    $('.powerUserProfileLink').show().parent().parent().removeClass('withSelect selected');
    $(this).hide();
    $(this).parent().children('.powerUserProfileSelect').show().parent().parent().addClass('withSelect selected');
    //$(this).next().next().show();
  });

  $('.powerUserProfileSelect').live('change', function () {
	var assignedCoursesCount = $(this).closest('tr').find('td:nth-child(5) a').text();
    var puserId = $(this).data('puser-id');
    var profileId = $(this).val();
    $.post(HTTP_HOST + '?r=PowerUserApp/powerUserManagement/assignProfile', {'puserId': puserId, 'profileId': profileId, 'assignedCoursesCount': assignedCoursesCount}, function (data) {
      if (data.status == 'success') {
        $('#power-user-management-grid').yiiGridView('update');
      }
    }, 'json');
  });

  if ($('.donutContainer').length) {
    var donutId = $(this).prop('id');
  }

  $(document).on('click', '.report-action-pdf', function () {
    if ($(this).hasClass('disabled')) return false;
    if ($(this).hasClass('disabled-pdf')) return false;

    var modalBody = $(this).parents('.modal-body');

    var donutChartImageId = modalBody.find('.donut-wrapper').find('.donutContainer').prop('id');
    var donutChart = modalBody.find('#' + donutChartImageId).html();

    var lineChartImageId = modalBody.find('.report-activities-summary').find('.lineChartContainer').prop('id');
    var lineChart = modalBody.find('#' + lineChartImageId).html();

    var singleDonutImageId = modalBody.find('.completed-stat').find('.donutContainer').prop('id');
    var singleDonut = modalBody.find('#' + singleDonutImageId).parent().html();

    showLoading('contentLoading');
    $.post($(this).prop('href'), {'donutContent': donutChart, 'lineContent': lineChart, 'singleDonutContent': singleDonut}, function (data) {
      if (data.fileName) {
        window.location = HTTP_HOST + '?r=reportManagement/downloadPdf&fileName=' + data.fileName;
      }
      hideLoading('contentLoading');
    }, 'json');

    return false;
  });

});

(function ($) {
	$.fn.loadTree = function (inputUrl, updateTree, updateGrid) {
		// console.log('update tree');
		var params = {};

		//---allow to specify separate url and/or url parameters
		var url = '';
		if (($.isPlainObject(inputUrl))) {
			if (inputUrl.url) { url = inputUrl.url; }
			if (inputUrl.params && $.isPlainObject(inputUrl.params)) {
				$.extend(params, inputUrl.params);
			}
		} else if ($.type(inputUrl) == 'string') {
			url = inputUrl;
		}
		//---

		if (url == '') {
			if ($(this).hasClass('course')) {
				url = HTTP_HOST + '?r=courseManagement/getCategoryTree';
			}
		} else {
			params['ajaxUpdate'] = true;
		}

		if (updateTree) {
			params['updateTree'] = true;
		}

		var tgt = $('#tree-generation-time');
		if (tgt.length > 0 && tgt.val() != '') {
			params['treeGenerationTime'] = '' + tgt.val();
		}

		$.get(url, params, function (data) {
			$('#tree-breadcrumbs').html(data.breadcrumbs);

			//--- update tree generation time
			if (data.treeGenerationTime) {
				var tgt = $('#tree-generation-time');
				if (tgt.length > 0) {
					tgt.val('' + data.treeGenerationTime);
				}
			}
			//---

			if (updateTree) {
				var treeWrapper = $('#tree-wrapper');
				treeWrapper.html(data.content);
				treeWrapper.hideChidrenNodes();
				$('input').styler({
					//browseText: 'Choose file'
				});
				$('.popover-bottom').popover({
					placement: 'bottom',
					html: true
				});
				$('.tooltip-enabled').tooltip();
			}

			if ($('.ajaxLinkNode.checked-node').length > 0) {
				toggleShowChildNodeUsers($('.ajaxLinkNode.checked-node'));
			} else {
				// No node was clicked in the org chart. Assume that root node
				// was clicked and hide the "show users from descendants - yes/no" switch
				// for now. It should reappear when a child node is clicked
				// (by invoking toggleShowChildNodeUsers())
				$('.children-node-content').css('opacity', '0');

				// A bit hacky, but looks ugly otherwise
				// $('.group.select-users').css('border-left', '0');
			}

			if (updateGrid) {
				var ajaxGridForm = $('.ajax-grid-form');
				$(ajaxGridForm.data('grid')).yiiGridView('update', {
					data: ajaxGridForm.serialize()
				});
			}
		}, 'json');

		$.fn.hideChidrenNodes = function () {
			$('.hasChildren').children('ul').hide();
			$('.rootNode').children('ul').show();
			$('.node-tree').children('li').show();
			$('.rootNode').next('ul').show();
			$('.checked-node').parents('.nodeUl').show();
//      $('.nodeUl .collapse-node').removeClass('active');
			$('.nodeUl ul:visible').prev().find('.collapse-node').addClass('active');
		}
	};

  /************************************************************************************************************/

  $('.ajaxCrop').live('change', function (e) {
    var _this = $(this);
    var className = $(this).data('class');
    var form = $(this).parents('form');
    var width = $(this).data('width');
    var height = $(this).data('height');
    var imageType = $(this).data('image-type');
    var callback = $(this).data('callback');

    var url = HTTP_HOST
      + '?r=crop&class=' + className
      + '&width=' + width
      + '&height=' + height
      + '&imageType=' + imageType
      + '&cropCallback=' + callback;

    if (!$(this).parents('.fileUploadContent').next().hasClass('contentUploadLoading')) {
      $(this).parents('.fileUploadContent').after($('<div/>', {'class': 'contentUploadLoading'}));
    }
    $(this).parents('.fileUploadContent').hide();
    showLoading('contentUploadLoading');


    var options = {
      url         : url,
      type        : 'post',        // 'get' or 'post', override for form's 'method' attribute
      dataType    : 'json',        // 'xml', 'script', or 'json' (expected server response type)
      beforeSubmit: showRequest,  // pre-submit callback
      success     : function (responseText, statusText, xhr, $form) {
        hideLoading('contentUploadLoading');
        $('.fileUploadContent').show();
        showResponse(responseText, statusText, xhr, $form, _this)
      }  // post-submit callback
      //clearForm: true        // clear all form fields after successful submit
      //resetForm: true        // reset the form after successful submit
      //$.ajax options can be used here too, for example:
      //timeout:   3000
    };
    e.stopPropagation();
    // bind form using 'ajaxForm' and submit
    form.ajaxForm(options).submit().unbind('submit');
    return false;
  });

	$('.ajaxCropNoParentForm').live('change', function (e) {
		var _this = $(this);
		var className = $(this).data('class');
		var form = $(this).parents('form');
		var width = $(this).data('width');
		var height = $(this).data('height');
		var imageType = $(this).data('image-type');
		var callback = $(this).data('callback');

		var url = $(this).data('url');

		if(!url) {
			url = HTTP_HOST
				+ '?r=crop&class=' + className
				+ '&width=' + width
				+ '&height=' + height
				+ '&imageType=' + imageType
				+ '&cropCallback=' + callback;
		}

		if (!$(this).parents('.fileUploadContent').next().hasClass('contentUploadLoading')) {
			$(this).parents('.fileUploadContent').after($('<div/>', {'class': 'contentUploadLoading'}));
		}
		$(this).parents('.fileUploadContent').hide();
		showLoading('contentUploadLoading');

		var options = {
			url         : url,
			type        : 'post',        // 'get' or 'post', override for form's 'method' attribute
			dataType    : 'json',        // 'xml', 'script', or 'json' (expected server response type)
			beforeSubmit: showRequest,  // pre-submit callback
			success     : function (responseText, statusText, xhr, $form) {
				hideLoading('contentUploadLoading');
				$('.fileUploadContent').show();

				if (responseText.content) {
					$('body').append(responseText.content);
					if (responseText.modalId) {
						var modalObj = $('#' + responseText.modalId);
						var mainModal = _this.closest('.modal');
						var loSlider = mainModal.find('.loSlider');
						modalObj.modal();
						modalObj.cropInit(responseText.aspectRatioX, responseText.aspectRatioY, responseText.x, responseText.y, responseText.x2, responseText.y2);
						modalObj.find('.confirm-btn').on('click', function (e) {
							e.preventDefault();

							// Check if we are already IN crop processing
							if (cropProcessing) return false;
							cropProcessing = true;

							$.ajax({
								'dataType': 'json',
								'type'    : 'POST',
								'url'     : modalObj.find('form').attr('action'),
								'cache'   : false,
								'data'    : modalObj.find("form").serialize(),
								'success' : function (data) {
									modalObj.modal('hide');
									if (data.cropCallback) {
										loSlider[data.cropCallback]();
									} else {
										modalRefresh(mainModal);
									}
								},
								'complete': function() {
									cropProcessing = false;
								}
							});
						});
					}
				}
			}  // post-submit callback
			//clearForm: true        // clear all form fields after successful submit
			//resetForm: true        // reset the form after successful submit
			//$.ajax options can be used here too, for example:
			//timeout:   3000
		};
		e.stopPropagation();
		// bind form using 'ajaxForm' and submit

		form.ajaxSubmit(options);
		return false;
	});

  $.fn.cropInit = function (aspectRatioX, aspectRatioY, x, y, x2, y2) {
    var modal = $(this);
    var jcrop_api;
    var bounds, boundx, boundy;

    var cropBox = $(this).find('#cropbox');
    boundx = cropBox.attr('width');
    boundy = cropBox.css('height');

    cropBox.Jcrop({
      onChange   : saveCoords,
      onSelect   : saveCoords,
      aspectRatio: aspectRatioX / aspectRatioY,
      //    setSelect: [ 50, 50, 200, 300 ],
      setSelect  : [x, y, x2, y2],
      addClass   : 'jcrop_custom',
      bgColor    : '#333333',
      bgOpacity  : .5,
      sideHandles: false,
      allowSelect: false,
      allowMove  : true,
      allowResize: true

    }, function () {
      jcrop_api = this;
      bounds = jcrop_api.getBounds();
      boundx = bounds[0];
      boundy = bounds[1];
    });
    function saveCoords(coords) {
      if (parseInt(coords.w) > 0) {
        modal.find('#CropParams_x').val(coords.x);
        modal.find('#CropParams_y').val(coords.y);
        modal.find('#CropParams_w').val(coords.w);
        modal.find('#CropParams_h').val(coords.h);

        modal.find('#CropParams_x2').val(coords.x2);
        modal.find('#CropParams_y2').val(coords.y2);
        return true;
      }
      return false;
    }

    return true;
  }

  /************************************************************************************************************/


})(jQuery);

/**
 * Hides the "show users from child nodes yes/no" switch
 * if the root node is currently selected (since root node
 * always displays all users, including those from child nodes)
 *
 * @param elem - a jQuery object of the '.ajaxLinkNode' elem that was clicked
 */
function toggleShowChildNodeUsers(elem){
	if(typeof elem === "undefined")
		return;
	if(elem.length==0)
		return;

	if(elem.closest('li.nodeTree-node').hasClass('rootNode')){
		// The root node was clicked. Hide the "show descendants yes/no" switch
		$('.children-node-content').css('opacity', '0');

		// A bit hacky, but looks ugly otherwise
		// $('.group.select-users').css('border-left', '0');
	}else{
		$('.children-node-content').css('opacity', '1');
		// $('.group.select-users').css('border-left', '1px solid #fefefe');
	}
}

function strpos(haystack, needle, offset) {
  var i = (haystack+'').indexOf(needle, (offset || 0));
  return i === -1 ? false : i;
}

function hideConfirmButton() {
//  var confirmButton = $('.modal.fade.in, #confirmBox').find('.btn-submit');
  var confirmButton = $('.modal.fade.in').find('.btn-submit');
//  console.log(confirmButton);
  confirmButton.addClass('disabled');
//  confirmButton.attr('disabled', 'disabled');
  return true;
}

function showConfirmButton() {
//  var confirmButton = $('.modal.fade.in, #confirmBox').find('.btn-submit');
  var confirmButton = $('.modal.fade.in').find('.btn-submit');
//  confirmButton.removeAttr('disabled');
  confirmButton.removeClass('disabled');
  return true;
}

function removeConfirmButton() {
	var confirmButton = $('.modal.fade.in').find('.btn-submit');
	if (confirmButton.length > 0) { confirmButton.remove; }
	return true;
}

function courseFormInit() {
  var modal = $('.modal.in');
  modal.find('.select-user-form-wrapper .item > div').live('click', function () {
    modal.find('.select-user-form-wrapper .item > div').removeClass('checked');
    // $('.thumbnailSlider .sub-item').removeClass('checked');

    $(this).addClass('checked');

    // $(this).find('input[type="radio"]').trigger('click');
    $(this).find('input[type="radio"]').prop('checked', true);
    $(this).parent().find('.jq-radio').removeClass('checked');
    $(this).find('.jq-radio').addClass('checked');
  });

  if (!tinymce.editors[modal.find('textarea').attr('id')]) {
    initTinyMCE('#' + modal.find('textarea').attr('id'), 150);
  }

  //$('input, select').styler();
}

/*******************************************************************************************************************/

// pre-submit callback
function showRequest(formData, jqForm, options) {
  var queryString = $.param(formData);
  return true;
}

// post-submit callback
// A flag to raise during Ajax crop processing, to prevent multiple requests.
var cropProcessing = false;
function showResponse(responseText, statusText, xhr, $form, $this) {
  if (responseText.content) {
    $('body').append(responseText.content);
    if (responseText.modalId) {
      var modalObj = $('#' + responseText.modalId);
      var mainModal = $('.modal.fade.in').modal('hide');
      modalObj.modal();
      modalObj.cropInit(responseText.aspectRatioX, responseText.aspectRatioY, responseText.x, responseText.y, responseText.x2, responseText.y2);
      modalObj.find('.confirm-btn').unbind('click').live('click', function () {
    	
    	// Check if we are already IN crop processing 
    	if (cropProcessing) return false;  
    	cropProcessing = true;
    	
        $.ajax({
          'dataType': 'json',
          'type'    : 'POST',
          'url'     : modalObj.find('form').attr('action'),
          'cache'   : false,
          'data'    : modalObj.find("form").serialize(),
          'success' : function (data) {
        	  modalObj.modal('hide');
        	  if (data.cropCallback) {
        		  mainModal.modal('show');
        		  mainModal[data.cropCallback]();
        	  } else {
        		  modalRefresh(mainModal);
        	  }
          },
          'complete': function() {
        	  cropProcessing = false;
          }
        });
      });
    }
  }
}

function labelSortInit() {
  $('#label-management-grid tbody').sortable({
    handle: '.move',
    update: function (event, ui) {
      var rows = $('#label-management-grid tbody tr');
      var ids = [];

      rows.each(function () {
        ids.push($(this).find('.move > div').html());
      })
      $.post(HTTP_HOST + '?r=LabelApp/labelManagement/order', { ids: ids });
    }
  });
}


// Somehow emulates modal refresh behavior
function modalRefresh(modal) {
  if (modal.hasClass('in')) {
    modal.modal('hide');
  }
  $("#handler-" + modal.attr('id')).click();
}


/*******************************************************************************************************************/
// Charts


function applyDonut(donutClass) {
  $(donutClass).each(function () {
    if ($(this).html() == '') {
      createDonut($(this).prop('id'), $(this).data('dataset'));
    }
  });
}

function createDonut(containerId, data) {
  var margin = 3;
  var boxSize = 2 * data.initRadius;
  var dataset = [];
  for (var key in data.data) {
    if (key != 'last') {
      dataset[key] = data.data[key].data;
      boxSize += 2 * (margin + data.thickness);
    }
  }
  dataset = dataset.reverse();

  boxSize -= 2 * margin;

	if(data["boxSize"]){
		// Allow the boxSize (width and height) to be
		// overwritten via the data attributes (since IE8
		// doesn't calculate it correctly sometimes)
		boxSize = data["boxSize"];
	}

  var color = d3.scale.category20();

  var pie = d3.layout.pie().sort(null);
  var arc = d3.svg.arc();
  var svg = d3.select("#" + containerId).append("svg")
    .attr("width", boxSize)
    .attr("height", boxSize)
    .append("g")
    .attr("transform", "translate(" + boxSize / 2 + "," + boxSize / 2 + ")");

  var gs = svg.selectAll("g").data(dataset).enter().append("g");
  var path = gs.selectAll("path")
    .data(function (d) {
      return pie(d);
    })
    .enter().append("path")
    .attr("fill", function (d, i, j) {
      return data.data[j].color[i];
      // return color(i);
    })
    .attr("d", function (d, i, j) {
      if (j == 0) {
        return arc.innerRadius(data.initRadius).outerRadius(data.initRadius + data.thickness)(d);
      } else {
        return arc.innerRadius(data.initRadius + (margin + data.thickness) * j).outerRadius(data.initRadius + data.thickness + (margin + data.thickness) * j)(d);
      }
    });

}

function applyMultiSeriesCharts(selector) {
  $(selector).each(function () {
    if ($(this).html() == '') {
      createMultiSeriesChart($(this).prop('id'), $(this).data('dataset'), $(this).data('chart-colors'));
    }
  });
}

function applyLineCharts(donutClass) {
	$(donutClass).each(function () {
		$this = $(this);
		if ($this.html() == '') {
			// console.log($(this).html());
			var others = {};
			if ($this.css('width') && $this.css('width').indexOf('px') >= 0) {
				others.width = parseInt($(this).css('width').replace('px', ''));
			}
			if ($(this).css('height') && $this.css('height').indexOf('px') >= 0) {
				others.height = parseInt($(this).css('height').replace('px', ''));
			}
            if ($(this).css('overflow')) {
                others.overflow = $(this).css('overflow');
            }
			createLineCharts($(this).prop('id'), $(this).data('dataset'), $(this).data('subtitle'), $(this).data('categories'), others);
		}
	});
}

function createLineCharts(containerId, dataset, subtitle, categories, otherProperties) {
  // X axis labels format
  var format = d3.time.format("%b");

  var w = (otherProperties && otherProperties.width ? otherProperties.width : 560);
  var h = (otherProperties && otherProperties.height ? otherProperties.height : 250);
  var o = (otherProperties && otherProperties.overflow ? otherProperties.overflow : 'visible');
  var t = null;

  var maxDataPointsForDots = 100,
    transitionDuration = 1000,
    svg = null,
    yAxisGroup = null,
    xAxisGroup = null,
    dataCirclesGroup = null,
    dataLinesGroup = null,
    margin = 40,
    min = 0,
    pointRadius = 4,
    data = generateData(dataset),
    max = d3.max(data, function (d) {
      return d.value
    });

  var x = d3.time.scale().range([0, w - margin]).domain([new Date('1899-12-31'), new Date('1900-12-31')]);
  var y = d3.scale.linear().range([h - margin * 2, 0]).domain([min, max]);

  var xAxis = d3.svg.axis().scale(x).tickSize(h - margin * 2).tickPadding(20).tickFormat(function (d) {
    return categories[d.getMonth()];
  });

  var yAxis = d3.svg.axis().scale(y).orient('left').tickSize(-w + margin * 2).tickPadding(20);

  svg = d3.select('#' + containerId).select('svg').select('g');
  if (svg.empty()) {
    svg = d3.select('#' + containerId)
      .append('svg:svg')
      .attr('width', w)
      .attr('height', h)
      .attr('overflow', o)
      .attr('class', 'viz')
      // .attr('fill', 'blue')
      .append('svg:g')
      .attr('transform', 'translate(' + margin + ',' + margin + ')');
  }

  t = svg.transition().duration(transitionDuration);

  // y ticks and labels
  if (!yAxisGroup) {
    yAxisGroup = svg.append('svg:g')
      .attr('class', 'yTick')
      .call(yAxis);
  }
  else {
    t.select('.yTick').call(yAxis);
  }

  // x ticks and labels
  if (!xAxisGroup) {
    xAxisGroup = svg.append('svg:g')
      .attr('class', 'xTick')
      .call(xAxis);
  }
  else {
    t.select('.xTick').call(xAxis);
  }

  // Draw the lines
  if (!dataLinesGroup) {
    dataLinesGroup = svg.append('svg:g');
  }

  var dataLines = dataLinesGroup.selectAll('.data-line')
    .data([data]);

  var line = d3.svg.line()
    .x(function (d, i) {
      return x(d.date);
    })
    .y(function (d) {
      return y(d.value);
    })
    .interpolate("linear");

  var garea = d3.svg.area()
    .interpolate("linear")
    .x(function (d) {
      return x(d.date);
    })
    .y0(h - margin * 2)
    .y1(function (d) {
      return y(d.value);
    });

  dataLines
    .enter()
    .append('svg:path')
    .attr("class", "area")
    // .attr('fill', 'white')
    .attr("d", garea(data));

  dataLines.enter().append('path')
    .attr('class', 'data-line')
    .style('opacity', 0.3)
    .attr("d", line(data));

  dataLines.transition()
    .attr("d", line)
    .duration(transitionDuration)
    .style('opacity', 1)
    .attr("transform", function (d) {
        var _x = x(d.date);
        var _y = y(d.value)
        if (isNaN(_x))
            _x = 0;
        if (isNaN(_y))
            _y = 0;
        return "translate(" + _x + "," + _y + ")";
    });

  dataLines.exit()
    .transition()
    .attr("d", line)
    .duration(transitionDuration)
    .attr("transform", function (d) {
        var _x = x(d.date);
        var _y = y(0)
        if (isNaN(_x))
            _x = 0;
        if (isNaN(_y))
            _y = 0;
        return "translate(" + _x + "," + _y + ")";
    })
    .style('opacity', 1e-6)
    .remove();

  d3.selectAll(".area").transition()
    .duration(transitionDuration)
    .attr("d", garea(data));

  // Draw the points
  if (!dataCirclesGroup) {
    dataCirclesGroup = svg.append('svg:g');
  }

  var circles = dataCirclesGroup.selectAll('.data-point')
    .data(data);

  circles
    .enter()
    .append('svg:circle')
    .attr('class', 'data-point')
    .style('opacity', 1e-6)
    .attr('title', function (d) {
      return d.value
    })
    .attr('cx', function (d) {
      return x(d.date)
    })
    .attr('cy', function () {
      return y(0)
    })
    .attr('r', function () {
      return pointRadius
    })
    .transition()
    .duration(transitionDuration)
    .style('opacity', 1)
    .attr('cx', function (d) {
      return x(d.date)
    })
    .attr('cy', function (d) {
      return y(d.value)
    });

  circles
    .transition()
    .duration(transitionDuration)
    .attr('cx', function (d) {
      return x(d.date)
    })
    .attr('cy', function (d) {
      return y(d.value)
    })
    .attr('r', function () {
      return pointRadius
    })
    .style('opacity', 1);

  circles
    .exit()
    .transition()
    .duration(transitionDuration)
    .attr('cy', function () {
      return y(0)
    })
    .style("opacity", 1e-6)
    .remove();

  svg.append("text")
    .attr('x', 0)
    .attr('y', -(margin / 2))
    .style('font-size', '13px')
    .text(subtitle);

  $('svg circle').tipsy({
    gravity: 's',
    html   : true,
    title  : function () {
      var d = this.__data__;
      return '<strong>' + d.value + '</strong>';
    }
  });

  $('.lineChartContainer path').attr('fill', 'none');
  $('.lineChartContainer .data-line').attr('fill', 'none').attr('stroke', '#333333').attr('stroke-width', 2).attr('stroke-dasharray', 0);
  $('.lineChartContainer line').attr('fill', 'none').attr('stroke', '#cccccc').attr('stroke-width', 0.5);
  $('.lineChartContainer text').attr('fill', '#333333').attr('font-family', '"Open Sans", sans-serif').attr('font-size', '13px');
  $('.yTick text').attr('text-anchor', 'end');
  $('.xTick text').attr('text-anchor', 'middle');
  $('.lineChartContainer .data-point').attr('fill', '#333333');
}

function generateData(dataset) {
  var data = [];

  for (var i = 0; i < dataset.length; i++) {
    var date = new Date();
    date.setYear(1900);
    date.setMonth(i);
    date.setHours(0, 0, 0, 0);
    date.setDate(1);
    data.push({ 'value': dataset[i], 'date': date });
  }

  return data;
}

function createMultiSeriesChart(containerId, dataset, colors) {
  var data = dataset;

  var margin = {top: 20, right: 20, bottom: 30, left: 10},
    width = $('#'+containerId).width() - margin.left - margin.right,
    height = $('#'+containerId).height() - margin.top - margin.bottom;

  var parseDate = d3.time.format("%Y-%m-%d").parse;
  var parseMonth = d3.time.format('%b');

  var x = d3.time.scale()
    .range([0, width]);

  var y = d3.scale.linear()
    .range([height, 0]);

  var color = d3.scale.category10();

  var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom")
    .ticks(31)
    .tickPadding(12)
    .tickFormat(function (d) {
      if (d.getDate() == 1) {
        return parseMonth(d);
      } else {
        return d.getDate();
      }
    });

  var yAxis = d3.svg.axis()
    .scale(y)
    .tickPadding(12)
    .orient("left");

  var line = d3.svg.line()
    .interpolate("linear")
    .x(function (d) {
      return x(d.date);
    })
    .y(function (d) {
      return y(d.value);
    });

  function make_x_axis(ticksCount) {
    return d3.svg.axis()
      .scale(x)
      .orient("bottom")
      .ticks(ticksCount)
  }

  function make_y_axis(ticksCount) {
    return d3.svg.axis()
      .scale(y)
      .orient("left")
      .ticks(ticksCount)
  }

  var svg = d3.select('#' + containerId).append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  color.domain(d3.keys(data[0]).filter(function (key) {
    return key !== "date";
  }));

  data.forEach(function (d) {
    d.date = parseDate(d.date);
  });

  var categories = color.domain().map(function (name, index) {
    return {
      name  : name,
      index : index,
      values: data.map(function (d) {
        return {date: d.date, value: +d[name]};
      })
    };
  });

  x.domain(d3.extent(data, function (d) {
    return d.date;
  }));

  y.domain([
    d3.min(categories, function (c) {
      return d3.min(c.values, function (v) {
        return v.value;
      });
    }),
    d3.max(categories, function (c) {
      return d3.max(c.values, function (v) {
        return v.value;
      });
    }) + 1
  ]);

  svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis);

  svg.append("g")     // Draw the x Grid lines
    .attr("class", "grid")
    .attr("transform", "translate(0," + height + ")")
    .call(make_x_axis(11)
      .tickSize(-height, 0, 0)
      .tickFormat("")
    )

  svg.append("g")     // Draw the y Grid lines
    .attr("class", "grid")
    .call(make_y_axis(9)
      .tickSize(-width, 0, 0)
      .tickFormat("")
    )

  var category = svg.selectAll(".category")
    .data(categories)
    .enter().append("g")
    .attr('data-color', function (d) {
      return colors[d.index];
    })
    .attr("class", function (d) {
      return 'category chart-' + (d.index + 1)
    });

  category.append("g").selectAll("circle")
    .attr('class', 'data-point')
    .data(function (d) {
      return d.values
    })
    .enter()
    .append("circle")
    .attr("r", 2)
    .attr("cx", function (dd) {
      return x(dd.date)
    })
    .attr("cy", function (dd) {
      return y(dd.value)
    })
    .attr("fill", '#cccccc')
    .attr("stroke", '#cccccc')
    .attr("stroke-width", '4px')

  category.append("path")
    .attr("class", "line")
    .attr("d", function (d) {
      return line(d.values);
    })
    .style("fill", 'none')
    .style('stroke-width', '2px')
    .style("stroke", '#cccccc');

  $('svg circle').tipsy({
    gravity: 's',
    html   : true,
    title  : function () {
      var d = this.__data__;
      return '<strong>' + d.value + '</strong>';
    }
  });

  $('.category .line, .category circle').hover(function () {
    var circles = $(this).closest('.category').find('circle');
    var line = $(this).closest('.category').children('.line');
    var color = $(this).closest('.category').data('color');
    circles.css('stroke', color);
    circles.css('fill', color);
    line.css('stroke', color);
  }, function () {
    var circles = $(this).closest('.category').find('circle');
    var line = $(this).closest('.category').children('.line');
    circles.css('stroke', '#ccc');
    circles.css('fill', '#ccc');
    line.css('stroke', '#ccc');
  });

  // .style("stroke", function(d) { return colors[this.parentNode.__data__.index]});

  $('.multiSeriesLineChartContainer path').attr('fill', 'none');
  $('.multiSeriesLineChartContainer .data-line').attr('fill', 'none').attr('stroke', '#333333').attr('stroke-width', 2).attr('stroke-dasharray', 0);
  $('.multiSeriesLineChartContainer line').attr('fill', 'none').attr('stroke', '#ffffff').attr('stroke-width', 0.5);
  $('.multiSeriesLineChartContainer text').attr('fill', '#333333').attr('font-family', 'Open Sans, sans-serif').attr('font-size', '14px');
  $('.yTick text').attr('text-anchor', 'end');
  $('.xTick text').attr('text-anchor', 'middle');
  $('.multiSeriesLineChartContainer .data-point').attr('fill', '#333333');
}


var courseCategoriesTreeHelper = {
	isRequestingUpdate: false,
	reloadCoursesCategoriesTree: function (params) {

		if (!courseCategoriesTreeHelper.isRequestingUpdate) {
			//opening a dialog informing the user that the tree has changed and needs to be reloaded
			courseCategoriesTreeHelper.isRequestingUpdate = true;

			//create the reload message dialog
			var unique = '_' + (new Date().getTime());
			var dialogId = 'update_tree_confirm' + unique;
			$('<div/>').dialog2({
				autoOpen: true,
				id: dialogId,
				modalClass: 'modal-update-tree',
				title: params.title || '',
				showCloseHandle: true,
				removeOnClose: false,
				closeOnEscape: true,
				closeOnOverlayClick: true
			});
			$('.modal.modal-update-tree').find('.modal-body').html(params.text || '');
			$('.modal.modal-update-tree').find('.modal-footer').html('<input class="btn confirm-btn" type="button" value="' + (params.button || '') + '" name="confirm" id="update-tree-confirm-button">');

			//manage the dialog closing
			$('.modal.modal-update-tree').off('dialog2.closed');
			$('.modal.modal-update-tree').on('dialog2.closed', function () {
				courseCategoriesTreeHelper.isRequestingUpdate = false;
				//allow to call a custom function for tree reloading
				var params = {reload: 1};
				$('#tree-wrapper').loadTree({params: params}, true, true);
			});

			//manage dialog confirm button click
			$('.modal.modal-update-tree .modal-footer .confirm-btn').off('click');
			$('.modal.modal-update-tree .modal-footer .confirm-btn').on('click', function () {
				courseCategoriesTreeHelper.isRequestingUpdate = false;
				$('#' + dialogId).dialog2('close');
			});

		} else {
			//avoid the opening of multiple confirm dialog, we need just one !
			Docebo.log("Tree updating dialog already opened ...");
		}
	}
};