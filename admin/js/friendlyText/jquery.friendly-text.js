(function( $ ){
   $.fn.friendlyText = function(options) {
        var node = $(this);
        var defaults = {
            width: null,
            speed: 10,
            speedBack: 5,
            slideStep: 1,
            slideStepBack: 5,
            css: 'friendly-text',
            cssActive: 'friendly-text-hiding',
            cssScroll: 'friendly-text-area',
        };
        options = $.extend(defaults, options);

        return this.each(function() {
            var slide_timer;
            var obj = $(this);
            var scrollLeft = 0;
            var slider = function () {
                var area = $(obj).find('span.' + defaults.cssScroll);
                var max = area.width();
                scrollLeft += defaults.slideStep;
                obj.find('.' + defaults.css).scrollLeft(scrollLeft);
                if (scrollLeft < max) {
                    slide_timer = setTimeout(slider, defaults.speed);
                }
            };
            var sliderBack = function () {
                var area = $(obj).find('span.' + defaults.cssScroll);
                var max = area.width();
                scrollLeft -= defaults.slideStepBack;
                if (scrollLeft < 0)
                    scrollLeft = 0;
                obj.find('.' + defaults.css).scrollLeft(scrollLeft);
                if (scrollLeft > 0) {
                    clearTimeout(slide_timer);
                    slide_timer = setTimeout(sliderBack, defaults.speedBack);
                }
            };
            var event = function (e) {
                e = e || window.event;
                e = e.type === 'mouseover';
                clearTimeout(slide_timer);

                if (!e) obj.find('.' + defaults.css).addClass(defaults.cssActive);
                else obj.find('.' + defaults.css).removeClass(defaults.cssActive);

                if (e) {
                    slider();
                } else {
                    //obj.find('.' + defaults.css).scrollLeft(0);
                    //scrollLeft = 0;
                    sliderBack();
                }
            }

            $(this).wrapInner($('<span>', {"class": defaults.cssScroll}));
            $(this).wrapInner($('<div>', {"class": defaults.css + ' ' + defaults.cssActive}));
            $(this).mouseover(event).mouseout(event);
        });
    };
})( jQuery );