/**
 * Admin/ReportManagement controller Javascript compagnion
 */

var ReportManClass = false;

(function ( $ ) {
	
	/**
	 * 
	 */
	ReportManClass = function(options) {
		this.options = $.extend({}, this.defaultOptions, options);
	}
	
	/**
	 * 
	 */
	ReportManClass.prototype = {

			defaultOptions: {
				jobStatusToggleBaseUrl: null				
			},
			
			/**
                        * Call this when Notifications -> Users/Courses Selector's TOP panel is ready (the all/selected one)
                        * Serves both Users and Courses.
                        */
                       selectorTopPanelReady: function() {

                               /* Local function to update UI  */
                               var updateUi = function(xtype) {
                                       var elem;
                                       if (xtype == 'courses') 
                                               elem = $('input[name="courses-filter"]:checked');

										if(xtype == 'certifications'){
											elem = $('input[name="certifications-filter"]:checked');
										}

                                        if(xtype== 'badges'){
                                            elem = $('input[name="badges-filter"]:checked');
                                        }

									   if(xtype== 'contests'){
										   elem = $('input[name="contests-filter"]:checked');
									   }
									   
									   if(xtype== 'assets'){
										   elem = $('input[name="assets-filter"]:checked');
									   }
									   
									   if(xtype== 'experts'){
										   elem = $('input[name="experts-filter"]:checked');
									   }
									   
									   if(xtype== 'channels'){
										   elem = $('input[name="channels-filter"]:checked');
									   }

                                       var modal = elem.closest('.modal'); 
                                       modal.css("min-height", "1px");
                                       modal.find('.modal-body').css("min-height", "1px");


                                       if (elem.val() == 1) {
										   $('.main-selector-area').hide();
									   }
                                       else if(elem.val() == 0){
										   //$('.tab-pane').show();
										   $('.main-selector-area').fadeIn();
									   }
                               }


                               // Detect type (Courses ?) by available input option 
                               var xtype;
                               var elem;
//                               }		

                               if ($('input[name="courses-filter"]').length > 0) {
                                       xtype = 'courses';
                                       elem = $('input[name="courses-filter"]:checked');
                               }

								if ($('input[name="certifications-filter"]').length > 0) {
									xtype = 'certifications';
									elem = $('input[name="certifications-filter"]:checked');
								}

                                if ($('input[name="badges-filter"]').length > 0) {
                                    xtype = 'badges';
                                    elem = $('input[name="badges-filter"]:checked');
                                }

								if ($('input[name="contests-filter"]').length > 0) {
									xtype = 'contests';
									elem = $('input[name="contests-filter"]:checked');
								}
								
								if ($('input[name="assets-filter"]').length > 0) {
									xtype = 'assets';
									elem = $('input[name="assets-filter"]:checked');
								}
								if ($('input[name="experts-filter"]').length > 0) {
									xtype = 'assets';
									elem = $('input[name="experts-filter"]:checked');
								}
								
								if ($('input[name="channels-filter"]').length > 0) {
									xtype = 'channels';
									elem = $('input[name="channels-filter"]:checked');
								}

                               $('input[name="courses-filter"]').on('change', function(){
                                       $(this).trigger('refresh');
                                       updateUi('courses');
                               });

								$('input[name="certifications-filter"]').on('change', function(){
									$(this).trigger('refresh');
									updateUi('certifications');
								});

                                $('input[name="badges-filter"]').on('change', function(){
                                    $(this).trigger('refresh');
                                    updateUi('badges');
                                });

								$('input[name="contests-filter"]').on('change', function(){
									$(this).trigger('refresh');
									updateUi('contests');
								});
								
								$('input[name="assets-filter"]').on('change', function(){
									$(this).trigger('refresh');
									updateUi('assets');
								});
								$('input[name="experts-filter"]').on('change', function(){
									$(this).trigger('refresh');
									updateUi('experts');
								});
								
								$('input[name="channels-filter"]').on('change', function(){
									$(this).trigger('refresh');
									updateUi('channels');
								});

                                $('input[name="courses-filter"]').trigger('change');
                                $('input[name="certifications-filter"]').trigger('change');
                                $('input[name="badges-filter"]').trigger('change');
								$('input[name="contests-filter"]').trigger('change');
								$('input[name="assets-filter"]').trigger('change');
								$('input[name="experts-filter"]').trigger('change');
								$('input[name="channels-filter"]').trigger('change');

                               // Style inputs (but see listeners, wehere we trigger "refresh"!
                               $('.report_filter-selector-top-panel :input[type="radio"]').styler();


                               updateUi(xtype);


                       },
			/**
			 * Run this when sheduler view is opened (on ready)
			 */
			scheduleEditor: function(wizardMode, $modalDialog, emailAutoCompleteUrl, containerClass, jwizard) {
				containerClass = '.' + containerClass;

				$(containerClass + ' .input-append.date').bdatepicker({
					autoclose: true
				});
				
				if (wizardMode) {
					jwizard.setDialogClass('report-schedule-dialog');
				}
				else {
					$modalDialog.addClass('report-schedule-dialog');
				}
				
				/* Hide all dynamic- elements first */
				$(containerClass + ' [id^="dynamic-"]').hide();	

				// Now show the current one (depending on exisitng JOB, or daily, for no job)
				$(containerClass + ' #dynamic-for-daily').show();
				
				
				/* Listen for recurring period Change and show related controls */
				$(containerClass + ' select[name="recurring_period"]').on('change', function(){
					
					$(containerClass + ' [id^="dynamic-"]').hide();
					switch ($(this).val())  {

						case 'hourly':
							break;
					
						case 'daily':
							$(containerClass + ' #dynamic-for-daily').show(); 
							break;

						case 'weekly':
							$(containerClass + ' #dynamic-for-weekly').show(); 
							break;

						case 'monthly':
							$(containerClass + ' #dynamic-for-monthly').show(); 
							break;

						case 'every3months':
						case 'every6months':
						case 'yearly':
							$(containerClass + ' #dynamic-for-everynmonths').show(); 
							break;
					}
				});
				
				
				// Force SELECT to change and thus showing correct dynamic section
				$(containerClass + ' select[name="recurring_period"]').trigger('change');

				// Initialize emails selector: FCBK 
				$(containerClass + ' select[name="email_recipients"]').fcbkcomplete({
					json_url: emailAutoCompleteUrl,
					addontab: true,
			        width: '98.5%',
					cache: true,
					complete_text: '',
					newel: true,
					maxshownitems: 5,
					input_name: 'maininput-name',
					filter_selected: true
				});

				
				// Closing the dialog, update the grid (only if NOT in Wizard mode, where Wizard will update it)
				var modalBodyId = $modalDialog.find('.modal-body').attr('id');
				$(document).off("dialog2.content-update", '[id^="'+modalBodyId+'"]')
					.on("dialog2.content-update", '[id^="'+modalBodyId+'"]', function () {
						if ($(this).find("a.auto-close").length > 0) {
							$(this).dialog2("close");
							if (!wizardMode) {
								$.fn.yiiGridView.update('report-management-grid');
							}
						}
				});
				
				
				// Validate the form 
				$('input[name="confirm_button"], input[name="finish_wizard_schedule"]').on('click', function(){
					var modalBody = $(this).closest('.modal-body');
					
					// VALIDATE EMAIL SELECTOR
					var emails = [];
					var validEmails = true;
					$('#email_recipients :selected').each(function(i, selected){
						validEmails = Docebo.validateEmail($(selected).val());  
						emails[i] = $(selected).val();
						if (!validEmails) {
							return false;
						} 
					});
					if (!validEmails) {
						modalBody.feedback('error', Yii.t('report', 'Please enter valid emails'), false, 'alert-compact', true);
						return false;
					}
					if (emails.length <= 0) {
						modalBody.feedback('error', Yii.t('report', 'Please select at least one recipient'), false, 'alert-compact', true);
						return false;
					}
					
				});
				
				
				
				
			},
			
			
			/**
			 * Call this once only in main page, in Document Ready section.
			 * This is important method, you MUST call it!! 
			 */
			ready: function() {
				
				var that = this;
				
			 	// Can't use rel=tooltip because we need rel for Dialog2 modal ID
			 	$('a.tooltipize').tooltip();
			 	
				// When custom reports grid is updated, popover contents (export, report actions, ...) have "dialog2" opening links that need to be ajaxyfied
				$(document).on('shown.bs.popover', '[id^="popover-"]', function(e){
					$(document).controls();
				});

				// Listen for clicks on "Scheduled" column icons: toggle status and update grid  
				$(document).on('click', '.toggle-scheduled-job-status', function(e){
					var target = e.currentTarget;
					var hash_id = $(target).data('job-hash');
					var url = that.options.jobStatusToggleBaseUrl + '&hash_id=' + hash_id;
					$.get(url, function(res){
						$.fn.yiiGridView.update('report-management-grid');
					});  
				});

				// Remove course summary modal from DOM when it is hidden 
				$('.new-course-summary').live('hidden', function () {
					$(".modal.new-course-summary").remove();
				});
				

				// Wizard related: Upon closing (with CONFIRM, i.e. on SUCCESS), jWizard fires a JS event
				$(document).on('jwizard.closed.success', '', function(e, data){
					$.fn.yiiGridView.update('report-management-grid', {authorsUpdate : true});
				});

				// Wizard related: Listen for jWizard event on form data restore and fire ().styler refresh
				// When jWizard travels from dialog to dialog it saves and restores data of all forms.
				// Since we have STYLED radio & checkboxes we have to "refresh" them to reflect the changes
				$(document).on('jwizard.form-restored', function(e, jwizard, form){
					form.find('input[type="radio"], input[type="checkbox"]').trigger('refresh');
				});
				

			},
			
			/**
			 * Helper for AuditTrails filtering: arrow icons
			 */
			updateArrowIconsInAccordionHeadersAuditTrailFilter: function(){
				var holders = $('.audit-trail-categories-accordion .ui-accordion-header');
				holders.find('.toggler i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
				holders.filter('.ui-accordion-header-active').find('.toggler i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
				
				$('.ui-accordion-header .audit-trail-select-all-events').hide();
				$('.ui-accordion-header-active .audit-trail-select-all-events').show();
			},
			
			/**
			 * Helper for AuditTrails filtering: selection counters
			 */
			updateSelectionCountersAuditTrailFilter: function(index) {
				var countCheckedEvents = $('.audit-trail-categories-accordion .accordion-content-' + index + ' .accordion-checkbox').filter(':checked').length;
				$('.audit-trail-categories-accordion .ui-accordion-header').filter('.header-'+index).find('.selected-events').html(countCheckedEvents);
				
			},
			
			/**
			 * "Filters" step HTML is loaded
			 */
			step4Ready: function(){
				
				
				// AuditTrail categories and events selection (accordeon)
				var that = this;
				$('.audit-trail-categories-accordion').accordion({
					header: ".header",
					icons: false,
					heightStyle: "content",
					create: function(event, ui){
						that.updateArrowIconsInAccordionHeadersAuditTrailFilter();
					},
					activate: function(event, ui){
						that.updateArrowIconsInAccordionHeadersAuditTrailFilter();
					}
				});
				
				/**
				 * On Audit trail event checkbox changed
				 */
				$('.audit-trail-categories-accordion .accordion-checkbox').change(function(){
					var container = $(this).closest('.ui-accordion-content');
					var index = container.data('index');
					that.updateSelectionCountersAuditTrailFilter(index);
				}).trigger('change');
				
				/**
				 * On Audit trail category select all clicks
				 */	
				$('.audit-trail-select-all-events a.select-all').on('click', function(){
					var index = $(this).data('index');
					$('.accordion-content-' + index + ' input[type="checkbox"]').prop('checked', true).trigger('refresh');
					that.updateSelectionCountersAuditTrailFilter(index);
				});

				/**
				 * On Audit trail category unselect all clicks
				 */	
				$('.audit-trail-select-all-events a.unselect-all').on('click', function(){
					var index = $(this).data('index');
					$('.accordion-content-' + index + ' input[type="checkbox"]').prop('checked', false).trigger('refresh');
					that.updateSelectionCountersAuditTrailFilter(index);
				});
				
			}


	}
	
	
}( jQuery ));




