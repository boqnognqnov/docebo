var GroupAssignRulesManagerClass = false;

(function ($) {
	
	/**
	 * Constructor
	 */
	GroupAssignRulesManagerClass = function(options) {
		
		this.defaultOptions = {
			debug: 	true,
		};
		
		if (typeof options === "undefined" ) {
			var options = {};
		}
			
		this.init(options);
	};

	
	/**
	 * Prototype
	 */
	GroupAssignRulesManagerClass.prototype = {
		
		/**
		 * Initialize class object
		 */
		init: function(options) {
			this.options = $.extend({}, this.defaultOptions, options);
			this.debug('Initialize');                        
			this.addListeners();
		},
		
		/**
		 * Make our live easier and provide some debug, if enabled
		 */
		debug: function(message) {
			if (this.options.debug === true) {
				console.log('GroupAssignRulesManager:', message);
			}
		},

		/**
		 * Add Event listeners
		 */
		addListeners: function() {
                    //This is index for generating the array of models
                    var index = 0;                                       
                    
                    this.debug('Add listeners');                        
                    
                    //If we open a dialog, reset the index!!
                    $(document).delegate('#new-group_button',  "click", function() {
                        index = 0;
                    });        
                    
                    //Initializing the fbkcompleate fields
                    $(document).delegate('.new-group',  "dialog2.content-update", function() {                        
                        if($('input[name=next_id]').length > 0){
                            index = $('input[name=next_id]').val();                            
                        }                        
                        $("select[id^=fcbk]").fcbkcomplete({                                                                                        
                            width: '75%',
                            complete_text: '',
                            filter_hide: true,
                            cache: true,
                            filter_selected: true
                        });                        
                        
                        $( ".datepicker" ).bdatepicker({
                            autoclose: true
                        });
                    });

                    //Handling the event when we select an additional field from the dropdown
                    $(document).on('change', '#field_select', function(e){
                        $('#empty-rules').hide();
                        $('#rules-options').show();

                        $.ajax({
                            //Make an ajax post to generate the additional field HTML
                            url: HTTP_HOST + '?r=groupManagement/getFieldData',
                            data: {'id': $(this).children(":selected").attr('value'), 'index': index++},
                            async: false,
                            success: function(result){
                                if(result.html){                                    
                                    $('#assign-rules').append(result.html);
                                    $('#field_select').prop('selectedIndex',0);                                        
                                    if(result.type == 'country' || result.type == 'dropdown'){
                                        $('#' + result.id, '#assign-rules').fcbkcomplete({                                                                                        
                                            width: '75%',
                                            complete_text: '',
                                            cache: true,
                                            filter_selected: true
                                        });
                                    }
                                }
                            }
                        });                 
                    });
                    
                    //Remove button event
                    $(document).on('click', '#assign-rules a.delete-action', function(e){                        
                        $(this).parent().parent().remove();
                        if($("#assign-rules").children('.controls').length == 0) {
                            $('#empty-rules').show();
                            $('#rules-options').hide();
                        }
                    });
                    
                    //If there are some data
                    $(document).on('click', '#button_step2', function(e){
                        if($('input[name^=CoreGroupAssignRules]').length <= 0){                        
                            return false;
                        }                        
                    });
                    
                    //Handling the event, when we choose the date interval
                    $(document).on('change', 'select[id^=date_select_]', function(e){
                        var idx = $(this).data('index');
                        var value = this.value;
                        var fieldId = $(this).data('div');
                        var format = $(this).data('date_format');
                        switch(value){
                            case 'from-to':                                
                                $('#date_dates_' + fieldId).html(
                                        "<div class='span6'><label for=''>From</label>&nbsp&nbsp<input type='text' size='12' class='datepicker input-small' name='CoreGroupAssignRules["+ idx +"][condition_value][]'><i class='p-sprite calendar-black-small date-icon'></i></div><div class='span6'><label for=''>To</label>&nbsp&nbsp<input type='text' class='datepicker input-small' name='CoreGroupAssignRules["+ idx +"][condition_value][]'><i class='p-sprite calendar-black-small date-icon'></i></div>"
                                );
                                break;
                            case 'before':
                                $('#date_dates_' + fieldId).html(
                                        "<label for=''>Before</label>&nbsp&nbsp<input style='width: 100px;' type='text' size='12' class='datepicker' name='CoreGroupAssignRules["+ idx +"][condition_value]'><i class='p-sprite calendar-black-small date-icon'></i>"
                                );
                                break;     
                            case 'after':
                                $('#date_dates_' + fieldId).html(
                                        "<label for=''>After</label>&nbsp&nbsp<input style='width: 100px;' type='text' size='12' class='datepicker' name='CoreGroupAssignRules["+ idx +"][condition_value]'><i class='p-sprite calendar-black-small date-icon'></i>"
                                );
                                break;
                            case 'is':
                                $('#date_dates_' + fieldId).html(
                                        "<label for=''>Is</label>&nbsp&nbsp<input style='width: 100px;' type='text' size='12' class='datepicker' name='CoreGroupAssignRules["+ idx +"][condition_value]'><i class='p-sprite calendar-black-small date-icon'></i>"
                                );
                                break;
                        }
                        $( ".datepicker" ).bdatepicker({
                            autoclose: true,
                            format: format
                        });
                    })                                

		},

	};
	

})(jQuery);

