/*
Various functions to manage dropdown field creating/editing
*/

var DD = null; //prepare global variable for dropdown dialog

//some generic event handling (to be loaded every time when the page (_formDropDownV2) is shown)
$('.additionalFieldTypeDropdown').live('change', function () {
    //var DD is initialized in _formDropdownV2.php (when loading drodpdown create/edit field)
	if (DD && DD.changeLanguage) {
        DD.changeLanguage();
    }
});


//manager object (to be instantiated every time a dialog is opened)
function dropdownFieldManager(options) {

	var scope = this;

	//analyze options input
	for (option in options) {
		switch (option) {
			case 'idField': this.idField = options[option]; break;
			case 'translations': this.translations = options[option]; break;
			case 'filledLanguageTranslation': this.filledLanguageTranslation = options[option]; break;
			case 'newOptionPlaceholder': this.newOptionPlaceholder = options[option]; break;
			case 'currentActiveLanguage': this.currentActiveLanguage = options[option]; break;
		}
	}

	//draw inputs
	if (this.currentActiveLanguage) {
		this.fillLanguage(this.currentActiveLanguage);
		this.refreshDropdownPreview();
	}

	//do some general checkings
	this.isPlaceholderSupported = 'placeholder' in document.createElement('input');

	//apply styler and placeholders
	$('#add-field-form').closest('.modal').find('input,select').styler();
	replacePlaceholder();

	//calculate assigned languages count
	$('.modal.in .orgChart_translationsList .orgChart_translation').each(function (index) {
		if ($(this).val() != '') { scope.assignedCount++; }
	});
	$('#orgChart_assignedCount').html(this.filledLanguageTranslation + ': <span>' + this.assignedCount + '</span>');


}




dropdownFieldManager.prototype = {

	idField: 0,

	assignedCount: 0,
	isPlaceholderSupported: false, //this is initialized when constructor is called
	translations: false,
	currentActiveLanguage: false,

	//some translations
	filledLanguageTranslation: '',
	newOptionPlaceholder: '',


	addInputEvents: function(lang_code) {
		$('#field-options-inputs-'+lang_code).on('keydown', '.dropdownOption:last', $.proxy(function(event) {
			$element = $(event.target);
			var nextInput = $element.next('.dropdownOption');
			if (nextInput.length == 0 && $element.val() == '') {
				this.refreshDropdownPreview();
				this.appendToLanguage(lang_code);
				event.stopPropagation();
			}
		}, this));
	},


	//this function will print input text fields in options box
	fillLanguage: function(key) {
		var t = this.translations[key];
		if (!t) { return; }
		var container = $('#field-options-inputs-'+key);
		if (!container) { return; }
		var prefix = 'translation_'+key+'_options_', i=0;
		var name = 'CoreField[translation]['+key+'][options]';
		$.each(t.translations, function(index, value) {
			var toAppend = $('<input/>', {
				id: prefix+'translation_'+i,
				type: 'text',
				class: 'dropdownOption',
				value: value['translation'],
				name: name+'[translation][]'
			});
			//toAppend.data({index: i, idSon: value['idSon'], id_common_son: value['id_common_son']});
			toAppend.attr('data-idson', value['idSon']);
			toAppend.attr('data-id_common_son', value['id_common_son']);
			toAppend.attr('data-index', i);
			container.append(toAppend);
			container.append($('<input/>', {id: prefix+'idSon_'+i, type: 'hidden', value: value['idSon'], name: name+'[idSon][]'}));
			container.append($('<input/>', {id: prefix+'id_common_son_'+i, type: 'hidden', value: value['id_common_son'], name: name+'[id_common_son][]'}));
			i++;
		});
		container.append($('<input/>', {
			type: 'text',
			placeholder: this.newOptionPlaceholder,
			class: 'dropdownOption',
			value: '',
			maxlength: '255',
			id: prefix+'translation_'+i,
			name: name+'[translation][]'
		}));
		this.addInputEvents(key);
	},


	//this function will update translations var accordingly to inputs content
	updateLanguagePage: function(key) {

		//retrieve all translations input fields and sort them by index
		var list = $('.orgChart_translationsList input[name^="CoreField[translation]['+key+'][options][translation]"]');
		if (list.length <= 0) { return; }

		//enlist the translations retrieved
		var newTranslations = [];
		var lastElement = $('.orgChart_translationsList').find('input.dropdownOption:last');
		list.each(function(index, value) {

			//do not consider last input
			if ($(value).attr('id') == lastElement.attr('id')) { return; }

			//enlist translations
			var params = {
				translation: $(value).val()
			};
			var data = $(value).data();
			if (data['idson']) { params.idSon = data['idson']; }
			if (data['id_common_son']) { params.id_common_son = data['id_common_son']; }
			newTranslations.push(params);
		});

		this.translations[key]['translations'] = newTranslations; //update old values
	},


	//when changing selected language
	changeLanguage: function() {
		var selectedLanguage = $('#CoreField_lang_code').val();
        if ( typeof  selectedLanguage == 'undefined' || selectedLanguage == '' || selectedLanguage.length < 2 )
            selectedLanguage = $('#LearningCourseFieldTranslation_lang_code').val();
		if (selectedLanguage != this.currentActiveLanguage) {

			//save current values in "global" translations var
			this.updateLanguagePage(this.currentActiveLanguage);

			//AJAX to server
			//this.saveLanguage(this.currentActiveLanguage);

			//hide old tab
			$('#CoreOrgChart_' + this.currentActiveLanguage).addClass('hide').removeClass('show');

			//clean options inputs of languages tabs
			$('.languagesName div[id^="field-options-inputs-"]').each(function() { $(this).html(''); });

			//update current active language
			this.currentActiveLanguage = selectedLanguage;

			//create new input fields
			this.fillLanguage(selectedLanguage);

			//refresh preview
			this.refreshDropdownPreview();

			//show correct tab
			$('#CoreOrgChart_' + selectedLanguage).addClass('show').removeClass('hide');
			$('.show > input').focus();
		}
	},


	appendToLanguage: function(lang_code) {
		var lastInput = $('#field-options-inputs-'+lang_code).find('.dropdownOption:last');

		if (!lastInput) { return; }

		var lastValue = lastInput.val();

		//calculate progressive input ID attribute
		var oldId = lastInput.attr('id').split('_');
		var oldIndex = parseInt(oldId[oldId.length -1]);
		oldId[oldId.length - 1] = oldIndex + 1;
		var newId = oldId.join('_');

		//emulates ".clone()" method by creating a brand new input (because some IE varsions may not like cloning)
		var clone = $(document.createElement('input'));
		clone.attr('type', "text");
		clone.attr('id', newId);
		clone.attr('class', 'dropdownOption');
		clone.attr('value', "");
		clone.attr('maxlength', '255');
		clone.attr('name', lastInput.attr('name'));
		clone.attr('placeholder', this.newOptionPlaceholder);

		lastInput.after(clone);
		//lastInput.after(lastInput.clone());

		lastInput.attr('placeholder', '');

		//replacePlaceholder(); //IE8 seems not to like this one ...
		if (!this.isPlaceholderSupported) { //replace placeholders, if needed (depending on browser support)
			lastInput.textPlaceholder();
			var el = $('#'+newId);
			el.textPlaceholder();
		}

		//update translations global var
		for (var x in this.translations) {
			if (x == this.currentActiveLanguage) {
				this.translations[x]['translations'].push({
					translation: lastValue
				});
			} else {
				this.translations[x]['translations'].push({
					translation: "" //push an empty value in other languages
				});
			}
		}

	},


	refreshDropdownPreview: function() {
		var selectedLanguage = $('#CoreField_lang_code').val();
		var data = $('.orgChart_translationsList .' + selectedLanguage);
		var inputTitle = data.find('.field-name input').val();
		var options = $('#field-options-inputs-'+selectedLanguage).find('.dropdownOption').slice(0, -1); //remove last empty input
		var label = $('<label/>', { 'for': 'preview-select', 'text': inputTitle });
		var select = $('<select/>', { 'id': 'preview-select' });

		var optArray = new Array(); //empty array for option values

		options.each(function(indx) {
			optArray[indx] = $(this).val(); //put option values in array
		});

		optArray.sort(function(a, b) {
			var textA = a.toLowerCase();
			var textB = b.toLowerCase();
			return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
		}); //sorting array alphabetical

		for (var i = 0; i < optArray.length; i++) {
			select.append($('<option/>', { 'value': '', 'text': optArray[i] })); //append options to select
		}

		var previewViewport = $('.preview-wrapper .preview-viewport');

		$('.lang-code').html(selectedLanguage.capitalize());

		previewViewport.empty();
		previewViewport.append(label);
		label.wrap('<td></td>');
		previewViewport.append(select);
		select.wrap('<td></td>');
		previewViewport.find('select').styler();
	}
	/*,
	ajaxing: false,
	beforeAjaxRequest: function() {
		this.ajaxing = true;
		$('.confirm-btn').prop('disabled', true);
		$('.confirm-btn').addClass('disabled');
	},
	afterAjaxRequest: function() {
		this.ajaxing = false;
		$('.confirm-btn').prop('disabled', false);
		$('.confirm-btn').removeClass('disabled');
	},

	saveLanguage: function(lang_code) {

		var i, input = [];
		var that = this;

		if (!this.translations[lang_code]) return;
		for (i=0; i<this.translations[lang_code].translations.length; i++) {
			input.push(this.translations[lang_code].translations[i]);
		}

		//preliminary operations
		this.beforeAjaxRequest();

		//make ajax request
		$.ajax({
			url: HTTP_HOST + '?r=additionalFields/saveDropdownSingleLanguage',
			type: "POST",
			data: {
				id_field: this.idField,
				language: lang_code,
				jsonLanguageData: JSON.stringify(input)
			},
			success: function (result) {

			},
			error: function() {

			},
			complete: function() {
				that.afterAjaxRequest();
			}
		});
	}
	*/

};



