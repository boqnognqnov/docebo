var EnrollmentRules = function (options) {
	// Default settings
	this.settings = {};
	this.init(options);
};


(function ($) {

	/**
	 * Define class
	 */
	EnrollmentRules.prototype = {

		init: function (options) {
			this.settings = $.extend({}, this.settings, options);
		},

		toggleActive: function (url) {
			$.ajax({
				url: url,
				success: function (result) {
					$('#enrollment-rules-grid').yiiGridView('update');
				}
			});
		},

		toggleConfirmButton: function(obj)
		{
			if ($(obj).is(':checked'))
				$(".confirm-dialog-button").removeClass('disabled');
			else
				$(".confirm-dialog-button").addClass('disabled');
		}
	}
}(jQuery));

$(function(){
	$(document).on("dialog2.content-update", ".modal", function () {
		var that = this;
		if ($(this).find("a.auto-close").length > 0) {
			if ($("#enrollment-rules-grid").length)
				$("#enrollment-rules-grid").yiiGridView('update');
			if ($("#rules-log-grid").length)
				$("#rules-log-grid").yiiGridView('update');
			if ($("#log-items-grid").length)
				$("#log-items-grid").yiiGridView('update');
			$(this).find(".modal-body").dialog2("close");
		}
		if ($(this).find("a.navigate-to").length > 0) {
			$(this).find("a.navigate-to").trigger('click');
		}
	});
	$(".enrollrule-prev").live('click', function(){
		$("a.previous-step").trigger('click');
	});

	$('#search-input').live("keydown", function(e) {
		var code = e.keyCode || e.which;
		var search_input = $("#search-input").val();
		var grid_id = $('.enrollment-rules-grid').attr('id');
		var selected_items = $("#"+grid_id + "-selected-items-list").val();
		if (code == 13) {
			$.fn.yiiGridView.update(
				grid_id,
				{data: 'search_input=' + search_input + '&selected_items='+selected_items}
			);
			return false
		}else {
			return true;
		}
	});

	$('.confirm-dialog-button').live('click', function(){
		if ($(this).hasClass('disabled'))
			return false;
	});


});