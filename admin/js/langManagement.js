/**
 * 
 */
var LangManagementAdmin = function(options) {
	// Default settings
	this.settings = {};
	this.init(options);
};


(function ( $ ) {

	/**
	 * Define class 
	 */
	LangManagementAdmin.prototype = {
	
		init: function(options) {
			this.settings = $.extend({}, this.settings, options);
		},
		
		
		/**
		 * Remove a whole transaction. ID is in the HREF of the element
		 * @param e Event
		 * @returns {Boolean}
		 */
		deleteTransaction: function(e) {
			var elem = $(e.currentTarget);
			var url = elem.attr('href');
			
			// Show Prompt
			var dialogMessage = Yii.t('standard', 'Delete this transaction and all its content? <br><br>' + 
			'<span class="i-sprite is-solid-exclam red"></span> <strong>Warning! This operation can not be undone!</strong>');
			
			// Show prompt dialog and handle answer
			bootbox.dialog(dialogMessage,
				[
				 {
					 label: Yii.t('standard', '_DEL'),
					 class: 'btn-submit',
					 callback: function() {
						    $.post(url, {}, function(res) {
						        if (res.success == true) {
						        	Docebo.Feedback.show('success', res.message);
								    $.fn.yiiGridView.update('transactions-grid', {
								    	data: $("#transaction-search-form").serialize()
								    });
						        }
						        else {
						        	Docebo.Feedback.show('error', res.message);
						        }
						    }, 'json');
					 }
				 },
				 {
					 label: Yii.t('standard', '_CANCEL'),
					 class: 'btn-cancel',
					 callback: function() {
						 
					 }
				 }
				 
				],
				{
					header: '<i class="i-sprite is-solid-exclam large white"></i>' + Yii.t('standard', '_DEL')
				}
			);
			
			return false;
		},
		
		

		addListeners: function() {

			// Listen for SEARCH icon clicks
			$("#search-icon").on('click', function() {
				$("#transaction-search-form").submit();
			});

			// Clear search form and submit
			$("#clear-search-icon").on('click', function() {
				$("#transaction-search-form input[type='text']").val("");
				$("#transaction-search-form").submit();
			});

			// Listen for Grid Search FORM submition
			$("#transaction-search-form").submit(function(){
				var data = $(this).serialize();
				$.fn.yiiGridView.update('transactions-grid', {
					data: data
				});
				return false;
			});
			
			// Delete Transaction icon in grid view
			$('#transactions-grid').on("click", '.delete-transaction', $.proxy(this.deleteTransaction, this));
			
			
			
		}
		
	};



	
	$(function(){
		Transaction.addListeners();
		bootbox.animate(false);
	});
	
	
}(jQuery));
