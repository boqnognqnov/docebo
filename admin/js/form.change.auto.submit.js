/**
 * @author ColorWP
 * @link http://colorwp.com
 * @link https://github.com/ColorWP/Form-Change-Auto-Submit
 *
 * A simple jQuery plugin that will cause changing of a input element
 * inside a HTML form to submit that form. Currently <select>, <radio> and <input type=text>
 * types of elements are supported.
 *
 * To make the plugin automatically initialize on a form in the page, add
 * a "changeCausesFormSubmit" class to that form. Alternatively, you may
 * run the changeCausesFormSubmit() function on the form like so:
 * $('form#theForm').changeCausesFormSubmit();
 */
jQuery.fn.changeCausesFormSubmit = function(options){

	// Initialize default options
	var settings = $.extend({
		excludeSelector: null
	}, options );

	this.each(function(){
		// inner anonymous functions will need reference to this
		var form = this;

		// Make each <select> changes submit the current form
		jQuery(this).on('change', 'select, :checkbox, :radio', function(e){
			if(settings.excludeSelector){
				// Check if the selected/changed element is in the "exclude"
				// selector and if yes (length turns zero), don't submit the form
				if($(this).not(settings.excludeSelector).length > 0)
					jQuery(form).submit();
			}else{
				jQuery(form).submit();
			}
		});
		jQuery(this).on('input change', 'input[type=text]', function(e){
			if(settings.excludeSelector){
				// Check if the selected/changed element is in the "exclude"
				// selector and if yes (length turns zero), don't submit the form
				if($(this).not(settings.excludeSelector).length > 0)
					jQuery(form).submit();
			}else{
				jQuery(form).submit();
			}
		});
	});

};
// Initialize the custom jQuery plugin for all forms having this specific class
jQuery(function(){
	jQuery('form.changeCausesFormSubmit').changeCausesFormSubmit();
});