/**
 * 
 */
var TransactionAdmin = function(options) {
	// Default settings
	this.settings = {};
	this.init(options);
};


(function ( $ ) {

	/**
	 * Define class 
	 */
	TransactionAdmin.prototype = {
	
		init: function(options) {
			this.settings = $.extend({}, this.settings, options);
		},
		
		
		
		// This must be called at the begining and after grid is paginated 
		harvestGridPopovers: function() {
			that = this;
			$('#transactions-grid table tr').popover({
				trigger: 'hover',
				placement: 'top',
				html: true
			});
		},
		
		/**
		 * Remove a whole transaction. ID is in the HREF of the element
		 * @param e Event
		 * @returns {Boolean}
		 */
		deleteTransaction: function(e) {
			var elem = $(e.currentTarget);
			var url = elem.attr('href');
			
			// Show Prompt
			var dialogMessage = Yii.t('standard', 'Delete this transaction and all its content? <br><br>' + 
			'<span class="i-sprite is-solid-exclam red"></span> <strong>Warning! This operation can not be undone!</strong>');
			
			// Show prompt dialog and handle answer
			bootbox.dialog(dialogMessage,
				[
				 {
					 label: Yii.t('standard', '_DEL'),
					 'class': 'btn-submit',
					 callback: function() {
						    $.post(url, {}, function(res) {
						        if (res.success == true) {
						        	Docebo.Feedback.show('success', res.message);
								    $.fn.yiiGridView.update('transactions-grid', {
								    	data: $("#transaction-search-form").serialize()
								    });
						        }
						        else {
						        	Docebo.Feedback.show('error', res.message);
						        }
						    }, 'json');
					 }
				 },
				 {
					 label: Yii.t('standard', '_CANCEL'),
					 class: 'btn-cancel',
					 callback: function() {
						 
					 }
				 }
				 
				],
				{
					header: '<i class="i-sprite is-solid-exclam large white"></i>' + Yii.t('standard', '_DEL')
				}
			);
			
			return false;
		},
		
		

		addListeners: function() {

			// Clicks on disabled SUBMIT buttons must be ignored
			$(document).on('click', '.disabled', function(){return false;});
			
			// Listen for SEARCH icon clicks
			$(".search-icon").on('click', function() {
				$("#transaction-search-form").submit();
			});
			
			
			// Listen for EXPORT select change
			$("#export_transactions").on('change', function() {
				if ($(this).val() == '') return false;
				var url = $(this).data('export-url') + '&' + $("#transaction-search-form").serialize();
				
				// Use a temporary IFRAME, though simple windows.location.href does the same... but Iwant to use $.post() ... hmmmm
				//var $iframe = $('<iframe name="exportFrame" id="exportFrame" width="1" height="1" style="visibility:hidden;position:absolute;display:none"></iframe>');
				//$("#exportFrame").remove();
				//$iframe.appendTo('body').attr('src', url);
				
				window.location.href = url;
			});

			
			
			// Submit search on ENTER
			$("#transaction-search-form input[type='text']").keypress(function(event) {
			    if (event.which == 13) {
			        event.preventDefault();
			        $("#transaction-search-form").submit();
			    }
			});
			

			// Clear search form and submit
			$("#clear-search-icon").on('click', function() {
				$("#transaction-search-form input[type='text']").val("");
				$("#transaction-search-form").submit();
			});

			// Listen for Grid Search FORM submition
			$("#transaction-search-form").submit(function(){
				var data = $(this).serialize();
				$.fn.yiiGridView.update('transactions-grid', {
					data: data
				});
				return false;
			});
			
			// Delete Transaction icon in grid view
			$('#transactions-grid').on("click", '.delete-transaction', $.proxy(this.deleteTransaction, this));
			
			// Handle a.auto-close 
			$(document).on("dialog2.content-update", ".modal", function () {
				if ($(this).find("a.auto-close").length > 0) {
					$(this).find(".modal-body").dialog2("close");
				}
			});
			
			// ... and auto-close successes 
			$(document).on("dialog2.content-update", '[id^="delete-transaction-modal-"], [id^="edit-transaction-modal-"]', function () {
				if ($(this).find("a.auto-close.success").length > 0) {
				    $.fn.yiiGridView.update('transactions-grid', {
				    	data: $("#transaction-search-form").serialize()
				    });
				}
			});

			
		}
		
	};



	
	$(function(){
		Transaction.addListeners();
		bootbox.animate(false);
	});
	
	
}(jQuery));
