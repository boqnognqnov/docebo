var NotificationClass = function (options) {
    // Default settings
    this.settings = {};
    this.init(options);
};

(function ($) {
    /**
     * Which notifications require [items][/items] 
     * @type Array
     */
    var repeatableRequirement = [
        'DigestCourseHasExpired',
        'DigestUserCompleteCourse',
        'DigestUserEnrolledToCourse'
    ];


    /**
     * Define prototype
     */
    NotificationClass.prototype = {

        init: function (options) {
            this.settings = $.extend({}, this.settings, options);
        },

        checkNotificationTypeNextButton: function () {
            var notificationType = $('#notification-notifytype-select').val();
            var btn = $('.edit-notification-modal .notification-base-next-button');
            if (notificationType == '0') {
                //a selected type is required so disable "next" button
                btn.prop('disabled', true);
                btn.addClass('disabled');
            } else {
                //a type has been selected so make sure user can go to next step
                btn.prop('disabled', false);
                btn.removeClass('disabled');
            }
        },

        addListeners: function () {

            var that = this;

            // Clicks on disabled SUBMIT buttons must be ignored
            $(document).on('click', '.disabled', function () {
                return false;
            });

            // Notif. type changed
            $(document).on('change', '#notification-notifytype-select', function () {
                that.updateUi($(this));
                var notificationType = $(this).val();
                that.updateShortcodes(notificationType);
                //handle "next" button
                that.checkNotificationTypeNextButton();
            });

            // Language changed
            $(document).on('change', '#notification-modal select.notification-lang-select', function () {
                that.updateUi($(this));
                return false;
            });

            // Update internall arrays of Subjects and Messages upon KEYUP in Input and Textarea
            // Subjects
            $(document).on('keyup change input', 'input.notification-subject-editor', function () {
                var transport_id = $(this).data('transport_id');
                var language = $('select#notification-lang-select_' + transport_id).val();
                Notification.subjects['transport_' + transport_id][language] = $(this).val();
                $('input[name="subjects[' + transport_id + '][' + language + ']"]').val($(this).val());
                Notification.isCleanStart = false;
            });

            // Messages
            // "keyup" makes TinyMce editor veeery slow
            // $(document).on('keyup change input', 'textarea[name="tmp_message"]', function () {
            $(document).on('input change', 'textarea.notification-message-editor', function () {
                var transport_id = $(this).data('transport_id');
                var language = $('select#notification-lang-select_' + transport_id).val();
                Notification.messages['transport_' + transport_id][language] = $(this).val();
                $('input[name="messages[' + transport_id + '][' + language + ']"]').val($(this).val());
                Notification.isCleanStart = false;
            });

            // Clicks on Activate/Deactivate
            $(document).on('click', 'a.change-active-status', function () {
                $.get($(this).attr('href'), {}, function () {
                    $('#notification-grid').yiiGridView('update', {});
                });
                return false;
            });

            var removeTinymceEditors = function () {
                if (typeof Notification.textAreaIds !== "undefined" && Notification.textAreaIds !== null && Notification.textAreaIds.length > 0) {
                    $.each(Notification.textAreaIds, function (index, id) {
                        TinyMce.removeEditorById(id);
                    });
                }
            };

            // Handle a.auto-close
            $(document).on("dialog2.content-update", ".modal", function () {
                if ($(this).find("a.auto-close").length > 0) {
                    // Remove editor
                    removeTinymceEditors();
                    $(this).find(".modal-body").dialog2("close");
                }
            });

            // Handle close button
            $(document).on("dialog2.closed", ".modal", function () {
                // Remove editor
                try {
                    removeTinymceEditors();
                } catch (e) {
                    tinyMCE.editors = [];
                }
            });

            // ... and auto-close successes
            $(document).on("dialog2.content-update", '[id^="notification-modal"], [id^="delete-notification-modal"]', function () {
                if ($(this).find("a.auto-close.success").length > 0) {
                    $('#notification-grid').yiiGridView('update', {data: {}});
                }
            });


            // Wizard related: Listen for jWizard event on form data restore and fire ().styler refresh
            // When jWizard travels from dialog to dialog it saves and restores data of all forms.
            // Since we have STYLED radio & checkboxes we have to "refresh" them to reflect the changes
            $(document).on('jwizard.form-restored', function (e, jwizard, form) {

                // Because of STYLER, we need to call this... to refresh the styled radios/checkboxes
                form.find('input[type="radio"], input[type="checkbox"]').trigger('refresh');

                // Rebuild Internal Subjects/Messages by taking them from RESTORED by the wizard hidden fields
                // This is just the reverse operation done at loading the form
                form.find('input[name^="subjects"]').each(function (index) {
                    var name = $(this).attr('name');
                    var res = name.split('][');
                    res[0] = res[0].replace('subjects[', '');
                    res[1] = res[1].replace(']', '');
                    Notification.subjects['transport_' + res[0]][res[1]] = $(this).val();
                });
                form.find('input[name^="messages"]').each(function (index) {
                    var name = $(this).attr('name');
                    var res = name.split('][');
                    res[0] = res[0].replace('messages[', '');
                    res[1] = res[1].replace(']', '');
                    Notification.messages['transport_' + res[0]][res[1]] = $(this).val();
                });
                Notification.isCleanStart = false;

                setTimeout(function () {
                    $('.notification-lang-select').trigger('change');
                }, 0);
            });


            $(document).on('input change blur', 'input.notification-subject-editor', function () {
                var transport_id = $(this).data('transport_id');
                that.langChangeCountIncrease(transport_id);
            })

        },

        /**
         * Add listeners to TinyMce editor events
         * @param editor
         */
        addTinymceEditorListeners: function (editor) {

            setTimeout(function () {
                // Empty content on dialog load, because we
                // have no language selected initially
                editor.setContent('');
            }, 0);

            // On Editor content change
            editor.on('keyup change blur', function (e) {
                var transport_id = $(editor.getElement()).data('transport_id');
                var tr_index = 'transport_' + transport_id;
                var language = $('select#notification-lang-select_' + transport_id).val();
                Notification.messages[tr_index][language] = editor.getContent();
                $('input[name="messages[' + transport_id + '][' + language + ']"]').val(Notification.messages[tr_index][language]);
                Notification.isCleanStart = false;
                Notification.langChangeCountIncrease(transport_id);
            });

        },

        /**
         * Called when either the subject or message is changed (on any given language).
         * May be even called before the hidden fields that hold the values are updated,
         * so we must implement a delay (hence the setTimeout)
         */
        langChangeCountIncrease: function (transport_id) {
            setTimeout(function () {
                var langsFilled = 0, tr_index = 'transport_' + transport_id;
                $.each(Notification.languages, function (index, value) {
                    if (Notification.messages[tr_index][index] || Notification.subjects[tr_index][index]) {
                        langsFilled++;
                    }

                    var theOption = $('select#notification-lang-select_' + transport_id + ' option[value=' + index + ']');

                    if (theOption.length > 0) {
                        if (Notification.subjects[tr_index][index].length === 0 && Notification.messages[tr_index][index].length === 0) {
                            // Remove the trailing asterix, since that lang is now blank
                            theOption.html(theOption.html().replace(/.\*$/, ''));
                        } else {
                            // Add an asterix to the name of the language we just filled
                            if (theOption.html().indexOf("*") === -1) {
                                theOption.html(theOption.html() + ' *');
                            }
                        }
                    }
                });
                $('#edit-notification-form span#assigned-' + transport_id + ' span').html(langsFilled);
            }, 100);
        },

        /**
         * Convert incoming arrays of subjects anbd messages into a HIDDEN form fields,
         * later updated on the input change.
         *
         */
        addHiddenFields: function () {
            $.each(this.subjects, function (tr_index, languages) {
                var transport_id = tr_index.replace('transport_', '');
                $.each(languages, function (language, value) {
                    $('<input>').attr({
                        type: 'hidden',
                        name: 'subjects[' + transport_id + '][' + language + ']',
                        value: value
                    }).appendTo('#edit-notification-form');
                });
            });



            $.each(this.messages, function (tr_index, languages) {
                var transport_id = tr_index.replace('transport_', '');
                $.each(languages, function (language, value) {
                    $('<input>').attr({
                        type: 'hidden',
                        name: 'messages[' + transport_id + '][' + language + ']',
                        value: value
                    }).appendTo('#edit-notification-form');
                });
            });
        },

        canApplyTemplate: function (transport_id) {
            var i, found = false;
            for (i = 0; i < this.templates_apply_to.length && !found; i++) {
                if (this.templates_apply_to[i] == transport_id) {
                    found = true;
                }
            }
            return found;
        },

        /**
         * Update dialog interface according to current status: inputs, text areas, etc.
         *
         */
        updateUi: function ($element) {

            var notificationType = $('#notification-notifytype-select').val();

            //check if we are updating a specific tab or we need a genreic update for all transport tabs
            if ($element) {
                var current_tab = $element.closest('div.tab-pane.tab-transport-settings');
                var current_transport_id = current_tab.data('transport_id');
                var transport_tabs = current_tab;
            } else {
                var transport_tabs = $('div.tab-pane.tab-transport-settings');
            }

            //each transport tab is updated independently
            var that = this;
            transport_tabs.each(function (index, element) {

                var tab = $(element);
                var transport_id = tab.data('transport_id');
                var tr_index = 'transport_' + transport_id;
                var language = tab.find('select#notification-lang-select_' + transport_id).val();
                var subject_editor = tab.find('#notification_subject_' + transport_id);
                var message_editor = tab.find('#notification_message_' + transport_id);

                // If we changed the language or notification type, update INPUT and TEXTAREA with the current Subject and Message
                if ((language != 0) && (notificationType != 0)) {
                    var newDescription = '';
                    var newSubject = '';

                    // We need to check if we're creating a new notification (first step -> clean start)
                    if (that.isCleanStart) {

                        // Find a template for the selected notification
                        if (Notification.canApplyTemplate(transport_id) && notificationType && Notification.templates[notificationType]) {

                            // Apply the current template for subjects & messages
                            $.each(Notification.templates[notificationType].subjects, function (language, value) {
                                $('input[name="subjects[' + transport_id + '][' + language + ']"]').val(value);
                                Notification.subjects[tr_index][language] = value;
                            });

                            $.each(Notification.templates[notificationType].messages, function (language, value) {
                                $('input[name="messages[' + transport_id + '][' + language + ']"]').val(value);
                                Notification.messages[tr_index][language] = value;
                            });

                            // Extract the values to apply to the INPUT & TEXTAREA
                            newSubject = Notification.templates[notificationType].subjects[language] ? Notification.templates[notificationType].subjects[language] : '';
                            newDescription = Notification.templates[notificationType].messages[language] ? Notification.templates[notificationType].messages[language] : '';

                        } else {

                            // Reset the current template for subjects & messages
                            $.each(Notification.subjects[tr_index], function (language, value) {
                                $('input[name="subjects[' + transport_id + '][' + language + ']"]').val("");
                                Notification.subjects[tr_index][language] = "";
                            });

                            $.each(Notification.messages[tr_index], function (language, value) {
                                $('input[name="messages[' + transport_id + '][' + language + ']"]').val("");
                                Notification.messages[tr_index][language] = "";
                            });

                            // Clean up input and textarea
                            newDescription = newSubject = '';

                        }
                    } else {
                        newSubject = Notification.subjects[tr_index][language] ? Notification.subjects[tr_index][language] : '';
                        newDescription = Notification.messages[tr_index][language] ? Notification.messages[tr_index][language] : '';
                    }

                    subject_editor.val(newSubject);
                    message_editor.val(newDescription);
                }

                var disabledInput = language <= 0 || notificationType <= 0;
                var disabledSave = notificationType <= 0;

                // Disable entering Subject and Message if there is No selected language
                subject_editor.prop('disabled', disabledInput);
                message_editor.prop('disabled', disabledInput);

                // Disable Save if no certain info is selected
                if (disabledSave) {
                    $('.notification-submit').addClass('disabled');
                } else {
                    $('.notification-submit').removeClass('disabled');
                }

                //that.updateShortcodes(notificationType);
            });
        },

        /**
         * Update shortcodes for the current Notification Type
         *
         * @param notificationType
         */
        updateShortcodes: function (notificationType) {
            var list = $('ul[id^="notification_shortcodes_list_"]');
            if (!(notificationType in this.shortcodes)) {
                list.each(function (index, element) {
                    var $el = $(element), html = '';
                    if ($el.data('empty-text')) {
                        html = '<li><strong>' + $el.data('empty-text') + '</strong></li>';
                    }
                    $el.html(html);
                });
            } else {
                var notificationShortcodes = this.shortcodes[notificationType];
                var options = {}, html = '';
                $.each(notificationShortcodes, function (key, value) {
                    html += '<li>' + key + '</li>';
                });
                list.each(function (index, element) {
                    var $el = $(element);
                    $el.html(html);
                });
            }
        },

        /**
         * Call this when Notifications -> Users/Courses Selector's TOP panel is ready (the all/selected one)
         * Serves both Users and Courses.
         */
        selectorTopPanelReady: function () {

            /* Local function to update UI  */
            var updateUi = function (xtype) {
                var elem;
                if (xtype == 'courses')
                    elem = $('input[name="cfilter_option"]:checked');
                else if (xtype == 'users')
                    elem = $('input[name="ufilter_option"]:checked');

                var modal = elem.closest('.modal');
                modal.css("min-height", "1px");
                modal.find('.modal-body').css("min-height", "1px");

                if (elem.val() == 1)
                    $('.main-selector-area').hide();
                else
                    $('.main-selector-area').fadeIn();
            }

            // Detect type (users ? Courses ?) by available input option 
            var xtype;
            var elem;
            if ($('input[name="ufilter_option"]').length > 0) {
                xtype = 'users';
                elem = $('input[name="ufilter_option"]:checked');
            }

            if ($('input[name="cfilter_option"]').length > 0) {
                xtype = 'courses';
                elem = $('input[name="cfilter_option"]:checked');
            }

            /* When Radio is changed */
            $('input[name="ufilter_option"]').on('change', function () {
                $(this).trigger('refresh');
                updateUi('users');
            });

            $('input[name="cfilter_option"]').on('change', function () {
                $(this).trigger('refresh');
                updateUi('courses');
            });

            $('input[name="ufilter_option"]').trigger('change');
            $('input[name="cfilter_option"]').trigger('change');

            // Style inputs (but see listeners, wehere we trigger "refresh"!
            $('.notification-selector-top-panel :input[type="radio"]').styler();

            updateUi(xtype);

            // On Userses Selector CONFIRM (which s NOT the same as Wizard FInish/Next/Previous!!!)
            // Update the notification grid to make changes visible 
            var modalId = elem.closest('.modal-body').attr('id');
            $(document).off('userselector.closed.success', '#' + modalId)
                    .on('userselector.closed.success', '#' + modalId, function () {
                        $('#notification-grid').yiiGridView('update', {});
                    });
        },

        /**
         * Call this in assoc_courses.php view to indicate the page is loaded
         * We got some stuff to add there... 
         */
        assignCoursesPageReady: function (unassignCoursesUrl, cFilterOptionDoAssocValue) {

            $('.notification-assoc-courses :checkbox').styler({});

            // PowerUserManagement Courses: actions with selected courses
            $('#notification-assoc-courses-management-massive-action').change(function () {
                var $this = $(this);
                var selectedCourses = getSelectedCheckboxes('notification-assoc-courses-list', '');

                if (selectedCourses.length > 0) {
                    switch ($(this).val()) {
                        case 'delete':
                            config = {
                                'id': 'modal-' + getRandKey(),
                                'modalClass': 'delete-node',
                                'modalTitle': Yii.t("standard", "_DEL_SELECTED"),
                                'modalUrl': unassignCoursesUrl,
                                'modalRequestType': 'POST',
                                'modalRequestData': {'pk': selectedCourses},
                                'buttons': [
                                    {"type": "submit", "title": Yii.t("standard", "_CONFIRM")},
                                    {"type": "cancel", "title": Yii.t("standard", "_CLOSE")}
                                ],
                                'afterLoadingContent': 'hideConfirmButton();',
                                'afterSubmit': 'updateNotificationAssocCoursesList'
                            }
                            $('body').showModal(config);
                            break;
                    }
                }
                $('#' + $(this).attr('id') + '-styler ul li:first').click();
                $this.find('option:first-child').attr('selected', 'selected');
            });


            $('.notification-assoc-courses .main-section .jq-checkbox').click(function () {
                $('#notification-assoc-courses-form').submit();
            });


            /**
             * On wizard close (happens when user launch "Assign courses" dialog-wizard and clicks "Save"
             * check the Filtering option selected and update the interface
             */
            $(document).on('jwizard.closed.success', '', function (e, data) {

                var cfilter_option = null;
                if (data.globalData['selector-courses'] != null) {
                    $.each(data.globalData['selector-courses'], function (index, field) {
                        if (field.name == 'cfilter_option' && field.checked) {
                            cfilter_option = field.value;
                        }
                    });
                }
                // If user selected some courses...
                if (cfilter_option == cFilterOptionDoAssocValue) {
                    // If list view is currently availabale, just update it..
                    if ($('#notification-assoc-courses-list').length > 0) {
                        $.fn.yiiListView.update('notification-assoc-courses-list');
                    }
                    // Otherwise reload page to reflect the new status
                    else {
                        location.reload();
                    }
                }
                // Or.. if the other option is selected ("all courses") && list IS visaible, again, reload the page.. 
                else if ((cfilter_option != null) && ($('#notification-assoc-courses-list').length > 0)) {
                    location.reload();
                }
            });

        }

    };


    $(function () {
        Notification.addListeners();
        bootbox.animate(false);
    });


}(jQuery));
