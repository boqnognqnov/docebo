$(document).keyup(function(e) {
	if (e.keyCode == 27) { // ESC
		if ($('#reinsertCCDataModal.modal.in').length > 0) {
			return ;
		}
		if ($('.modal.in .popover.in').length > 0) {
			$('.modal.in .popover.in').prev().popover('close');
			return true;
		}

		if ($('.modal.in').length > 0) {
			$('.modal.in').modal('hide');
			return true;
		}

		if ($('.popover.in').length > 0) {
			$('.popover.in').prev().popover('close');
			return true;
		}
	}
});

$.fn.CleanSelectedListItems = function(data) {
    $("div#poweruser-courses-management-list").comboListView('setSelection', []);
    $.fn.updatePowerUserCoursesList(data);
}
// Copy the original method and override it below (calling the parent implemetation in a suitable place)
// Note that the IF is needed because we don't want to duplicate the logic in the overwriting
// method in case this script is included twice on the page (happens occasionally)
$.fn.popover.Constructor.prototype.toggle = function(e) {
    var self = e ? $(e.currentTarget)[this.type](this._options).data(this.type) : this
    if (self.tip().hasClass('in')) {
    	self.$element.hasClass('popover-destroy') ? self.destroy() : self.hide();
    } else {
    	self.show();
    }
}

// Copy the original method and override it below (calling the parent implemetation in a suitable place)
// Note that the IF is needed because we don't want to duplicate the logic in the overwriting
// method in case this script is included twice on the page (happens occasionally)
if(!$.fn.popover.Constructor._hide)
	$.fn.popover.Constructor._hide = $.fn.popover.Constructor.prototype.hide;
$.fn.popover.Constructor.prototype.hide = function() {
	if (this.$element.hasClass('popover-destroy') && !this.destroyed) {
		this.destroyed = true;
		return this.destroy();
	} else {
		return $.fn.popover.Constructor._hide.call(this);
	}
}

// Copy the original method and override it below (calling the parent implemetation in a suitable place)
// Note that the IF is needed because we don't want to duplicate the logic in the overwriting
// method in case this script is included twice on the page (happens occasionally)
if(!$.fn.popover.Constructor._close)
	$.fn.popover.Constructor._close = $.fn.popover.Constructor.prototype.close;
$.fn.popover.Constructor.prototype.close = function() {
	if (this.$element.hasClass('popover-destroy')) {
		this.destroy();
	} else {
		this.hide();
	}
}

// Copy the original method and override it below (calling the parent implemetation in a suitable place)
// Note that the IF is needed because we don't want to duplicate the logic in the overwriting
// method in case this script is included twice on the page (happens occasionally)
if(!$.fn.popover.Constructor._destroy)
	$.fn.popover.Constructor._destroy = $.fn.popover.Constructor.prototype.destroy;
$.fn.popover.Constructor.prototype.destroy = function () {
	if (this.options.callback) {
		this.options.callback();
	}

	$.fn.popover.Constructor._destroy.call(this);

	if (this.options.afterDestroy) {
		this.options.afterDestroy();
	}
}


// Copy the original method and override it below (calling the parent implemetation in a suitable place)
// Note that the IF is needed because we don't want to duplicate the logic in the overwriting
// method in case this script is included twice on the page (happens occasionally)
if(!$.fn.popover.Constructor._show)
	$.fn.popover.Constructor._show = $.fn.popover.Constructor.prototype.show;
$.fn.popover.Constructor.prototype.show = function() {
	this.destroyed = false;

	$('.popover.in').each(function() {
		$(this).prev().popover('close');
	});

	$.fn.popover.Constructor._show.call(this);

	// Install open dialog actions on popover menu actions
	$("div.node-actions ul li a.open-dialog").each(function (index, elem) {
		var a = $(elem).removeClass("open-dialog");

		var id = a.attr("rel");
		var content = a.attr("href");

		var options = {
			modal: true
		};

		var element;

		if (id) {
			var e = $("#" + id);
			if (e.length) element = e;
		}

		if (!element) {
			if (id) {
				options.id = id;
			}
		}

		if (a.attr("data-dialog-class")) {
			options.modalClass = a.attr("data-dialog-class");
		}

		if (a.attr("title")) {
			options.title = a.attr("title");
		}

		$.each($.fn.dialog2.defaults, function (key, value) {
			if (a.attr(key)) {
				options[key] = a.attr(key) == "true";
			}
		});
		if (content && content != "#") {
			options.content = content;

			a.click(function (event) {
				event.preventDefault();
				$(element || "<div></div>").dialog2(options);
			});
		} else {
			options.removeOnClose = false;
			options.autoOpen = false;

			element = element || "<div></div>";

			// Pre initialize dialog
			$(element).dialog2(options);

			a.attr("href", "#")
				.click(function (event) {
					event.preventDefault();
					$(element).dialog2("open");
				});
		}
	});

	if (this.options.afterRender) {
		this.options.afterRender();
	}
}

$(document).on('click', function(event) {
	if ($('.popover.in').length > 0) {
		if ($(event.target).hasClass('popover-trigger') == false) {
			if ($(event.target).closest('.popover').length == 0) {
				$('.popover.in').prev().popover('close');
			}
		}
	}
});

function loadTabContent(link) {
	$.ajax({
		url: yii.urls.base + '/index.php?r=' + link.data('url'),
		cache: false,
		async: false,
		timeout: 4000,
		error: function(){
			return true;
		},
		success: function (result) {
			link.data('loaded', 1);

			var div = $('<div/>', { 'id': link.data('tab') });
			$('.tab-content').append(div);
			if (link.data('tab') != 'logo')
				$('.tab-content input, .tab-content select').styler();

			$('div#' + link.data('tab')).html(result.html);
			// Suck Dialog2 controls
			$(document).controls();

			// Trigger event to let tab perform their actions
			$(document).trigger("tab.loaded");
		}
	});
}

function initTinyMCE(selector, height) {
	TinyMce.attach(selector, {height: height});
}

function saveFieldsSelection() {
	var selectedFields = $('.select-columns .selected, .select-columns .locked');
	var selected = [];

	$(selectedFields).each(function () {
		selected.push($(this).data('column-key'));
	});

	var content = $('.popover.in .popover-content').html();

	$.ajax({
		url: yii.urls.base + '?r=userManagement/saveFilterSelection',
		data: { fields: selected },
		async: false,
		cache: false,
		timeout: 4000,
		error: function(){
			return true;
		},
		success: function (result) {
			$('.filters .popover-trigger.select-columns').data('content', content);
		}
	});
}

function initFieldsPopover() {
	$('.filters .popover-trigger.select-columns').popover({
		placement: 'bottom',
		html: true,
		callback: function() {
			saveFieldsSelection();
		},
		afterDestroy: function() {
			initFieldsPopover();
		}
	});
}

function initFieldsSortable() {
	$('#fields-grid tbody').sortable({
	handle: '.move',
	update: function(event, ui) {
		var rows = $('#fields-grid tbody tr');
	    var ids = [];

	    rows.each(function() {
	      ids.push($(this).data('fid'));
	    })

	    $.post(HTTP_HOST + '?r=additionalFields/orderFields', { ids: ids });
	},
	helper: function(e, tr) {
		var $originals = tr.children();
		var $helper = tr.clone();

		$helper.children().each(function(index) {
			// Set helper cell sizes to match the original sizes
			$(this).width($originals.eq(index).width());
		});
		return $helper;
	}
	});
}

function initCourseFieldsSortable() {

    $('#fields-grid tbody').sortable({

        handle: '.move',
        update: function(event, ui) {
            var rows = $('#fields-grid tbody tr');
            var ids = [];

            rows.each(function() {
                ids.push($(this).data('fid'));
            })

			if ( $('#fields-grid').hasClass('additional-enrollment-fields') ) {
				$.post(HTTP_HOST + '?r=additionalEnrollmentFields/orderEnrollmentFields', { ids: ids });
			} else {
				$.post(HTTP_HOST + '?r=additionalCourseFields/orderCourseFields', { ids: ids });
			}

            
        },
        helper: function(e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();

            $helper.children().each(function(index) {
                // Set helper cell sizes to match the original sizes
                $(this).width($originals.eq(index).width());
            });
            return $helper;
        }
    });
}

function curriculaCourseSortInit() {
	var sequences = [];
	$('#curricula-course-list div.items div.item div.sequence').each(function() {
		sequences.push($(this).html());
	});

	$('#curricula-course-list div.items').sortable({
		handle: '.move',
		update: function (event, ui) {
			var rows = $('#curricula-course-list div.items div.item');
			var ids = {};

			var i = 0;
			rows.each(function () {
				ids[sequences[i]] = $(this).find('.move > div').html();
				i++;
			});
			$.post(HTTP_HOST + '?r=CurriculaApp/curriculaManagement/order&id='+getUrlParam('id'), { ids: ids });
		}
	});
}

function updateFarbtasticColor(farbtasticObj, input) {
	farbtasticObj.setColor(input.val());
}

function farbtasticUpdateCallback(farbtasticObj, input) {
	return function() {
		updateFarbtasticColor(farbtasticObj, input);
	}
}

function schemeFormInit() {
	$('.modal.in').find('.colors .color-item input').each(function() {
		var _this = $(this);
		var _preview = $(this).parents('.wrap').find('.color-preview');

		var f = $.farbtastic($(this).parents('.wrap').find('.colorpicker'), function(data) {
			_this.val(data);
			_preview.css('background-color', data);
			$('.modal.in').find('.colorable').css('background-color', data);
            $('.modal.in').find('.colorable-text').css('color', data);
		})

		// Initializes colorpicker color
		f.setColor($(this).val());

		$(this).live('keyup', farbtasticUpdateCallback(f, $(this)));
	});

	$('.modal.in').find('.color-item input').tooltip({ position: 'right' });

	$('.modal.in').find('.colors').on('click', '.color-preview', function() {
		$(this).parents('.wrap').find('input').focus();
	});
}

$(document).ready(function () {

	/**
	 * GLOBAL LIVE event to listen for ToggleStatus clicks in a GRID [column]
	 * data-url : controller/action to call
	 * data-id  : ID of the item to toggle
	 *
	 */
	$('#content').on('click', '.grid-view .toggle-active-state', function(event) {
		var _this = $(this);
		var gridId = $(this).parents('.grid-view').attr('id');

		// Check for available URL
		var url = $(this).data('url');
		if (url) {
			$.get(HTTP_HOST + '?r=' + url, { 'id': $(this).data('id') })
				.done(function(data) {
					if (data.success) {
						if (_this.hasClass('disabled')) {
							_this.removeClass('disabled').addClass('enabled');
						} else {
							_this.removeClass('enabled').addClass('disabled');
						}
						_this = null;
						//Why listId?? God knows! Another FUCK UP
						//$.fn.yiiGridView.update(listId);
						$.fn.yiiGridView.update(gridId);
					}
				});
		}
		event.preventDefault();
	});

	initFieldsPopover();

	// Initialize Popover elements for those selectors on the fly when they are first clicked
	// Used this instead of the regular $(selector).popover({}) version because it doesn't work
	// for dynamically added elements. Even if they have Popover initialized for them, they will just
	// be skipped
	$(document).on('click', '.popover-bottom, .popover-action', function(e){
			$(this).popover('destroy');
			$(this).popover({
				placement: 'bottom',
				html:      true
			}).popover('show');
	});

	if ($('#users-management-grid').length != 0) {
		filterColumns();
	}

	// Advanced search

	$('.advanced-search-link').click(function () {

		var hidden = $('#advanced-search');

		if(hidden.is(':visible') == true){
			// Also reset JQ-styler type inputs (will uncheck them)
			$('#advanced-search .jq-checkbox.checked').click();
			// Reset <select> dropdowns to their initial value and checkboxes to unchecked state
			$('#advanced-search').resetInputs();
		}

		hidden.toggle();
		$('#advancedSearchVisibility').val(hidden.css('display') == 'none' ? 0 : 1);
	});

	// Advanded search filters management
	var advancedSearchFilters = {};

	$('#select-filter').change(function (event) {
		var selected = $(this).find('option:selected');

		if (selected.val() === "") {
			return true;
		}

		if (selected.hasClass('additional')) {
			addAdditionalFilter(selected, advancedSearchFilters);
		} else {
			addFilter(selected, advancedSearchFilters);
		}
	});

	// End of Advanced search

	$('.select-columns li').live('click', function () {
		if ($(this).hasClass('locked')) {
			return true;
		}

		var column = $(this).data('column');

		if ($(this).hasClass('selected')) {
			$(this).removeClass('selected');
			$('#users-management-grid').find('.' + column).hide();
			userManagement.hiddenFields.push($(this).data('column-key'));
		} else {
			if ($(this).closest('.select-columns').find('li.selected').length < 5) {
				$(this).addClass('selected');
				$('#users-management-grid').find('.' + column).show();

				for (var i = userManagement.hiddenFields.length; i >= 0; i--) {
					if (userManagement.hiddenFields[i] === $(this).data('column-key')) {
						delete userManagement.hiddenFields[i];
						break;
					}
				}
			}
		}
	});

	applyTypeahead($('.typeahead'));

	$('.modal.in .nav-tabs a').live('click', function(e) {
		e.stopPropagation();
		e.preventDefault();
		$(this).tab('show');
	});

	$('.ajax-grid-form').live('submit', function() {
		$($(this).data('grid')).yiiGridView('update', {
			type: 'POST',
			data: $(this).serialize()
		});
		return false;
	});

    $('.ajax-list-form').live('submit', function() {
        $.fn.yiiListView.update($($(this).data('grid')).prop('id'), {
            data: $(this).serialize()
        });
        return false;
    });


	$('#enrollment-management-massive-action').live('change', function() {
		var selectedUsers = getSelectedCheckboxes('course-enrollment-grid', 'course-enrollment-grid-checkboxes');
		if (selectedUsers.length > 0) {
			switch ($(this).val()) {
				case 'delete':
				config = {
					'id'                 : 'modal-' + getRandKey(),
					'modalClass'         : 'delete-node',
					'modalTitle'         : Yii.t('standard', '_DEL_SELECTED'),
					'modalUrl'           : HTTP_HOST + '?r=courseManagement/deleteEnrollment',
					'modalRequestType'   : 'POST',
					'modalRequestData'   : {'ids' : selectedUsers},
					'buttons'            : [
            {"type": "submit", "title":  Yii.t('standard', "_CONFIRM") },
            {"type": "cancel", "title":  Yii.t('standard', "_UNDO") }
					],
					'afterLoadingContent': 'hideConfirmButton();',
					'afterSubmit'        : 'updateEnrollmentContent'
				}
				$('body').showModal(config);
				break;
			}
		}
		$('#'+$(this).attr('id')+'-styler ul li:first').click();
	});

	// actions with selected users
	$('#users-management-massive-action').live('change', function () {
		var selectedUsers = getSelectedCheckboxes('users-management-grid', 'users-management-grid-checkboxes');

		if (selectedUsers.length > 0) {
			switch ($(this).val()) {
				case 'deactivate':
          config = {
            'id'                 : 'modal-' + getRandKey(),
            'modalClass'         : 'deactivate-action',
            'modalTitle'         : Yii.t('standard', '_DEACTIVATE'),
            'modalUrl'           : HTTP_HOST + '?r=userManagement/changeStatus&status=0',
            'modalRequestType'   : 'POST',
            'modalRequestData'   : {'idst' : selectedUsers},
            'buttons'            : [
              {"type": "submit", "title": Yii.t("standard", Yii.t("standard", "_CONFIRM"))},
              {"type": "cancel", "title": Yii.t("standard", Yii.t("standard", "_CLOSE"))}
            ],
            'afterLoadingContent': 'hideConfirmButton();',
            'afterSubmit'        : 'updateUserContent'
          }
          $('body').showModal(config);
					break;
				case 'activate':
          config = {
            'id'                 : 'modal-' + getRandKey(),
            'modalClass'         : 'activate-action',
            'modalTitle'         : Yii.t("standard", "_ACTIVATE"),
            'modalUrl'           : HTTP_HOST + '?r=userManagement/changeStatus&status=1',
            'modalRequestType'   : 'POST',
            'modalRequestData'   : {'idst' : selectedUsers},
            'buttons'            : [
              {"type": "submit", "title": Yii.t("standard", "_CONFIRM")},
              {"type": "cancel", "title": Yii.t("standard", "_CLOSE")}
            ],
            'afterLoadingContent': 'hideConfirmButton();',
            'afterSubmit'        : 'updateUserContent'
          }
          $('body').showModal(config);
					break;
				case 'delete':
          config = {
            'id'                 : 'modal-' + getRandKey(),
            'modalClass'         : 'delete-node',
            'modalTitle'         : Yii.t('standard', '_DEL_SELECTED'),
            'modalUrl'           : HTTP_HOST + '?r=userManagement/deleteUser',
            'modalRequestType'   : 'POST',
            'modalRequestData'   : {'idst' : selectedUsers},
            'buttons'            : [
              {"type": "submit", "title": Yii.t("standard", "_CONFIRM")},
              {"type": "cancel", "title": Yii.t("standard", "_CLOSE")}
            ],
            'afterLoadingContent': 'hideConfirmButton();',
            'afterSubmit'        : 'updateUserContent'
          }
          $('body').showModal(config);
					break;
				case 'edit':
					$('#user-massive-update').data('modal-send-data', {idst : selectedUsers});
					$('#user-massive-update').click();
					break;
			}
		}
		$('#'+$(this).attr('id')+'-styler ul li:first').click();
        $('#'+$(this).attr('id')+'').val(1);
	});

	// actions with selected waiting enrollments
	$('#waitingEnrollments-massive-action').change(function() {
		var selectedEnrollments = getSelectedCheckboxes('course-waitingenrollment-grid', 'course-waitingenrollment-grid-checkboxes');

		if (selectedEnrollments.length > 0) {
			switch ($(this).val()) {
				case 'confirm':
          config = {
            'id'                 : 'modal-' + getRandKey(),
            'modalClass'         : 'delete-node',
            'modalTitle'         : Yii.t("standard", "_APPROVE"),
            'modalUrl'           : HTTP_HOST + '?r=courseManagement/confirmEnrollment',
            'modalRequestType'   : 'POST',
            'modalRequestData'   : {'ids' : selectedEnrollments},
            'buttons'            : [
              {"type": "submit", "title": Yii.t("standard", "_CONFIRM")},
              {"type": "cancel", "title": Yii.t("standard", "_CANCEL")}
            ],
            'afterLoadingContent': 'hideConfirmButton();',
            'afterSubmit'        : 'updateWaitingEnrollmentContent'
          }
          $('body').showModal(config);
				break;
				case 'cancel':
          config = {
            'id'                 : 'modal-' + getRandKey(),
            'modalClass'         : 'delete-node',
            'modalTitle'         : Yii.t("standard", "_DEL_SELECTED"),
            'modalUrl'           : HTTP_HOST + '?r=courseManagement/cancelEnrollment',
            'modalRequestType'   : 'POST',
            'modalRequestData'   : {'ids' : selectedEnrollments},
            'buttons'            : [
              {"type": "submit", "title": Yii.t("standard", "_CONFIRM")},
              {"type": "cancel", "title": Yii.t("standard", "_CANCEL")}
            ],
            'afterLoadingContent': 'hideConfirmButton();',
            'afterSubmit'        : 'updateWaitingEnrollmentContent'
          }
          $('body').showModal(config);
				break;
			}
		}
		$('#'+$(this).attr('id')+'-styler ul li:first').click();
	});

	// actions with selected enrollments
	$('#enrollments-massive-action').change(function() {
		var selectedEnrollments = getSelectedCheckboxes('course-enrollment-list', 'course-enrollment-list-checkboxes');

		if (selectedEnrollments.length > 0) {
			
			
			var selectedValue = $(this).val();
			
			switch (selectedValue) {
				case 'change-status':
					config = {
						'id' : 'modal-' + getRandKey(),
						'modalClass' : 'edit-enrollment',
						'modalTitle' : Yii.t('standard', 'Change status'),
						'modalUrl' : HTTP_HOST + '?r=courseManagement/axMassiveChangeStatus',
						'modalRequestType' : 'POST',
						'modalRequestData' : {'ids' : selectedEnrollments.join(',')},
						'buttons' : [
							{"type" : "submit", "title" : Yii.t('standard', '_CONFIRM') },
							{"type" : "cancel", "title" : Yii.t('standard', '_CANCEL') }
						],
						'afterLoadingContent' : function() {},
						'afterSubmit' : 'updateEnrollmentContent'
					}
					$('body').showModal(config);
					break;
                case 'change-enrollment-deadline':
                    config = {
                        'id' : 'modal-' + getRandKey(),
                        'modalClass' : 'edit-enrollment',
                        'modalTitle' : Yii.t('standard', 'Change enrollment deadline'),
                        'modalUrl' : HTTP_HOST + '?r=courseManagement/axMassiveChangeEnrollmentDeadline',
                        'modalRequestType' : 'POST',
                        'modalRequestData' : {'ids' : selectedEnrollments.join(',')},
                        'buttons' : [
                            {"type" : "submit", "title" : Yii.t('standard', '_CONFIRM') },
                            {"type" : "cancel", "title" : Yii.t('standard', '_CANCEL') }
                        ],
                        'afterLoadingContent' : function() {},
                        'afterSubmit' : 'updateEnrollmentContent'
                    }
                    $('body').showModal(config);
                    break;
				case 'delete':
					config = {
						'id' : 'modal-' + getRandKey(),
						'modalClass' : 'delete-node',
						'modalTitle' : Yii.t('standard', '_DEL_SELECTED'),
						'modalUrl' : HTTP_HOST + '?r=courseManagement/deleteEnrollment',
						'modalRequestType' : 'POST',
						'modalRequestData' : {'ids' : selectedEnrollments.join(',')},
						'buttons' : [
							{"type" : "submit", "title" : Yii.t('standard', '_CONFIRM') },
							{"type" : "cancel", "title" : Yii.t('standard', '_CANCEL') }
						],
						'afterLoadingContent' : 'hideConfirmButton();',
						'afterSubmit' : 'updateEnrollmentContent'
					}
					$('body').showModal(config);
					break;
				case 'mass-email':
					var params = getParamsFromUrl();
					config = {
						'id': 'modal-' + getRandKey(),
						'modalClass':              'course-enrollments-send-email',
						'modalTitle':              Yii.t('course', 'Send Email'),
						'modalUrl': HTTP_HOST + '?r=courseManagement/sendEmails',
						'modalRequestType':        'POST',
						'modalRequestData':        {'ids': selectedEnrollments.join(','), 'courseId': params.courseId, 'sessionId': params.sessionId},
						'buttons':                 [
							{"type": "submit", "title": Yii.t('standard', '_CONFIRM')},
							{"type": "cancel", "title": Yii.t('standard', '_CANCEL')}
						],
						'afterLoadingContent':     function () {},
						'afterSubmit':             'updateEnrollmentContent'
					}
					$('body').showModal(config);
					break;
				
				// Match "iframe-field-....." options
				case (selectedValue.match(/iframe-field-\w+/) || {}).input:
					var params = getParamsFromUrl();
					var match = selectedValue.match(/iframe-field-(\w+)/);
					var fieldId = match[1];
					config = {
						'id' : 'modal-' + getRandKey(),
						'modalClass' : 'iframe-enrollment-field-mass-action',
						'modalTitle' : Yii.t('standard', 'Edit'),
						'modalUrl' : HTTP_HOST + '?r=courseManagement/iframeEnrollmentFieldMassAction',
						'modalRequestType' : 'POST',
						'modalRequestData' : {
							'ids' : selectedEnrollments.join(','), 
							'fieldId': fieldId, 
							'courseId': params.courseId, 
							'sessionId': params.sessionId
						 },
						'buttons' : [
							// {"type" : "submit", "title" : Yii.t('standard', '_CONFIRM') },
							// {"type" : "cancel", "title" : Yii.t('standard', '_CANCEL') }
						],
						'afterLoadingContent' : function () {},
						'afterSubmit' : 'updateEnrollmentContent'
					}
					$('body').showModal(config);
					break;
					
					break;
			}
		}
		$('#'+$(this).attr('id')+'-styler ul li:first').click();
		$('option:first-child', this).attr("selected", "selected");
	});

	// actions with selected enrollments (for classroom sessions)
	$('#session-enrollments-massive-action').change(function() {
		var selectedEnrollments = getSelectedCheckboxes('session-enrollment-list', 'session-enrollment-list-checkboxes');
		var idCourse = $(this).data('id-course'), idSession = $(this).data('id-session');

        var takeAction = false;

		if (selectedEnrollments.length > 0) {
			switch ($(this).val()) {
				case 'delete':
					config = {
						'id' : 'modal-' + getRandKey(),
						'modalClass' : 'delete-node',
						'modalTitle' :  Yii.t('standard', '_DEL_SELECTED'),
						'modalUrl' : HTTP_HOST + '?r=ClassroomApp/enrollment/removeUserMultiple',
						'modalRequestType' : 'POST',
						'modalRequestData' : {
							ids : selectedEnrollments.join(','),
							course_id : idCourse,
							id_session : idSession
						},
						'buttons' : [
                            {"type" : "submit", "title" : Yii.t('standard', '_CONFIRM') },
                            {"type" : "cancel", "title" : Yii.t('standard', '_CANCEL') }
						],
						'afterLoadingContent' : 'hideConfirmButton();',
						'afterSubmit' : 'updateSessionEnrollmentContent'
					}
					$('body').showModal(config);
                    takeAction = true;
					break;
				case 'mass-email':
					var params = getParamsFromUrl();
					config = {
						'id': 'modal-' + getRandKey(),
						'modalClass':              'course-enrollments-send-email',
						'modalTitle':              Yii.t('course', 'Send Email'),
						'modalUrl': HTTP_HOST + '?r=courseManagement/sendEmails',
						'modalRequestType':        'POST',
						'modalRequestData':        {'ids': selectedEnrollments.join(','), 'courseId': params.courseId, 'sessionId': params.sessionId},
						'buttons':                 [
							{"type": "submit", "title": Yii.t('standard', '_CONFIRM')},
							{"type": "cancel", "title": Yii.t('standard', '_CANCEL')}
						],
						'afterLoadingContent':     function () {},
						'afterSubmit':             'updateEnrollmentContent'
					}
					$('body').showModal(config);
                    takeAction = true;
					break;
			}
		}

        if (takeAction) {
            $('#'+$(this).attr('id')+'-styler ul li:first').click();
            $('option:first-child', this).attr("selected", "selected");
        }
	});

	// actions with selected enrollments (for webinar sessions)
	$('#webinar-enrollments-massive-action').change(function() {
		var selectedEnrollments = getSelectedCheckboxes('session-enrollment-list', 'session-enrollment-list-checkboxes');
		var idCourse = $(this).data('id-course'), idSession = $(this).data('id-session');

		var takeAction = false;

		if (selectedEnrollments.length > 0) {
			switch ($(this).val()) {
				case 'delete':
					config = {
						'id' : 'modal-' + getRandKey(),
						'modalClass' : 'delete-node',
						'modalTitle' :  Yii.t('standard', '_DEL_SELECTED'),
						'modalUrl' : HTTP_HOST + '?r=webinar/enrollment/removeUserMultiple',
						'modalRequestType' : 'POST',
						'modalRequestData' : {
							ids : selectedEnrollments.join(','),
							course_id : idCourse,
							id_session : idSession
						},
						'buttons' : [
							{"type" : "submit", "title" : Yii.t('standard', '_CONFIRM') },
							{"type" : "cancel", "title" : Yii.t('standard', '_CANCEL') }
						],
						'afterLoadingContent' : 'hideConfirmButton();',
						'afterSubmit' : 'updateSessionEnrollmentContent'
					}
					$('body').showModal(config);
					takeAction = true;
					break;
				case 'mass-email':
					var params = getParamsFromUrl();
					config = {
						'id': 'modal-' + getRandKey(),
						'modalClass':              'course-enrollments-send-email',
						'modalTitle':              Yii.t('course', 'Send Email'),
						'modalUrl': HTTP_HOST + '?r=courseManagement/sendEmails',
						'modalRequestType':        'POST',
						'modalRequestData':        {'ids': selectedEnrollments.join(','), 'courseId': params.courseId, 'sessionId': params.sessionId},
						'buttons':                 [
							{"type": "submit", "title": Yii.t('standard', '_CONFIRM')},
							{"type": "cancel", "title": Yii.t('standard', '_CANCEL')}
						],
						'afterLoadingContent':     function () {},
						'afterSubmit':             'updateEnrollmentContent'
					}
					$('body').showModal(config);
					takeAction = true;
					break;
			}
		}

		if (takeAction) {
			$('#'+$(this).attr('id')+'-styler ul li:first').click();
			$('option:first-child', this).attr("selected", "selected");
		}
	});

	// curricula: actions with selected courses
	$('#curricula-course-massive-action').change(function() {
		var selectedCourses = getSelectedCheckboxes('curricula-course-list', 'curricula-course-list-checkboxes');

		if (selectedCourses.length > 0) {
			switch ($(this).val()) {
				case 'delete':
					config = {
						'id' : 'modal-' + getRandKey(),
						'modalClass' : 'delete-node',
						'modalTitle' : Yii.t("standard", "_UNASSIGN"),
						'modalUrl' : HTTP_HOST + '?r=CurriculaApp/curriculaManagement/unassignCourse&id='+getUrlParam('id'),
						'modalRequestType' : 'POST',
						'modalRequestData' : {'id_item' : selectedCourses},
						'buttons' : [
							{"type" : "submit", "title" : Yii.t("standard", "_CONFIRM")},
							{"type" : "cancel", "title" : Yii.t("standard", "_CLOSE")}
						],
						'afterLoadingContent' : 'hideConfirmButton();',
						'afterSubmit' : 'updateCurriculaCoursesContent'
					}
					$('body').showModal(config);
					break;
			}
		}

		$('#'+$(this).attr('id')+'-styler ul li:first').click();
        $('option:first-child', this).attr("selected", "selected");
	});

	// curricula: actions with selected users
	$('#curricula-user-massive-action').change(function() {
		var selectedUsers = getSelectedCheckboxes('curricula-user-list', 'curricula-user-list-checkboxes');
		if (selectedUsers.length > 0) {
			switch ($(this).val()) {
				case 'delete':
					config = {
						'id' : 'modal-' + getRandKey(),
						'modalClass' : 'delete-node',
						'modalTitle' : Yii.t("standard", "_UNASSIGN"),
						'modalUrl' : HTTP_HOST + '?r=CurriculaApp/curriculaManagement/unassignUser&id='+getUrlParam('id'),
						'modalRequestType' : 'POST',
						'modalRequestData' : {'ids' : selectedUsers.join(",")},
						'buttons' : [
							{"type" : "submit", "title" : Yii.t("standard", "_CONFIRM")},
							{"type" : "cancel", "title" : Yii.t("standard", "_CLOSE")}
						],
						'afterLoadingContent' : 'hideConfirmButton();',
						'afterSubmit' : 'updateCurriculaUsersContent_new'
					}
					$('body').showModal(config);
					break;
			}
		}

		$('#'+$(this).attr('id')+'-styler ul li:first').click();
	});

	// PowerUserManagement Courses: actions with selected courses
	$('#poweruser-courses-management-list_massive_action').change(function() {
		var selectedCourses = $('#poweruser-courses-management-list').comboListView('getSelection');

		if (selectedCourses.length > 0) {
			switch ($(this).val()) {
				case 'delete':
					config = {
						'id' : 'modal-' + getRandKey(),
						'modalClass' : 'delete-node',
						'modalTitle' : Yii.t("standard", "_DEL_SELECTED"),
						'modalUrl' : HTTP_HOST + '?r=PowerUserApp/powerUserManagement/unassignCoursesAndCoursepaths',
						'modalRequestType' : 'POST',
						'modalRequestData' : {'id' : selectedCourses.join(',')},
						'buttons' : [
							{"type" : "submit", "title" : Yii.t("standard", "_CONFIRM")},
							{"type" : "cancel", "title" : Yii.t("standard", "_CLOSE")}
						],
						'afterLoadingContent' : 'hideConfirmButton();',
						//'afterSubmit' : 'updatePowerUserCoursesList'
                        'afterSubmit' : 'CleanSelectedListItems' // This function call "updatePowerUserCoursesList" + Cleaning the already deleted items from the array[]

					};
					$('body').showModal(config);
					break;
			}
		}
		$(this).val('');
		$('#poweruser-courses-management-massive-action option:first-child').attr("selected", "selected");
	});

	// PowerUserManagement Locations: actions with selected locations
	$('#poweruser-locations-management-massive-action').change(function() {
		var selectedLocations = getSelectedCheckboxes('poweruser-locations-management-list', 'poweruser-locations-management-list-checkboxes');

		if (selectedLocations.length > 0) {
			switch ($(this).val()) {
				case 'delete':
					config = {
						'id' : 'modal-' + getRandKey(),
						'modalClass' : 'delete-node',
						'modalTitle' : Yii.t("standard", "_DEL_SELECTED"),
						'modalUrl' : HTTP_HOST + '?r=PowerUserApp/powerUserManagement/unassignLocations',
						'modalRequestType' : 'POST',
						'modalRequestData' : {'id' : selectedLocations},
						'buttons' : [
							{"type" : "submit", "title" : Yii.t("standard", "_CONFIRM")},
							{"type" : "cancel", "title" : Yii.t("standard", "_CLOSE")}
						],
						'afterLoadingContent' : 'hideConfirmButton();',
						'afterSubmit' : 'updatePowerUserLocationsList'
					}
					$('body').showModal(config);
					break;
			}
		}
		$('#'+$(this).attr('id')+'-styler ul li:first').click();
	});

$('#waiting-users-massive-action').change(function() {
	var selectedUsers = getSelectedCheckboxes('users-management-grid', 'users-management-grid-checkboxes');

	if (selectedUsers.length > 0) {
		switch ($(this).val()) {
			case 'confirm':
        config = {
          'id'                 : 'modal-' + getRandKey(),
          'modalClass'         : 'delete-node',
          'modalTitle'         : Yii.t("standard", "_APPROVE"),
          'modalUrl'           : HTTP_HOST + '?r=userManagement/confirmUser',
          'modalRequestType'   : 'POST',
          'modalRequestData'   : {'idst' : selectedUsers},
          'buttons'            : [
            {"type": "submit", "title": Yii.t("standard", "_CONFIRM")},
            {"type": "cancel", "title": Yii.t("standard", "_CLOSE")}
          ],
          'afterLoadingContent': 'hideConfirmButton();',
          'afterSubmit'        : 'updateUserContent'
        }
        $('body').showModal(config);
			break;
			case 'decline':
        config = {
          'id'                 : 'modal-' + getRandKey(),
          'modalClass'         : 'delete-node',
          'modalTitle'         : Yii.t("standard", "_DEL_SELECTED"),
          'modalUrl'           : HTTP_HOST + '?r=userManagement/deleteTempUser',
          'modalRequestType'   : 'POST',
          'modalRequestData'   : {'idst' : selectedUsers},
          'buttons'            : [
            {"type": "submit", "title": Yii.t("standard", "_CONFIRM")},
            {"type": "cancel", "title": Yii.t("standard", "_CLOSE")}
          ],
          'afterLoadingContent': 'hideConfirmButton();',
          'afterSubmit'        : 'updateUserContent'
        }
        $('body').showModal(config);
      break;
		}
	}
	$('#'+$(this).attr('id')+'-styler ul li:first').click();
});

$('#group-member-management-massive-action').live('change', function() {
	var selectedUsers = getSelectedCheckboxes('group-member-management-list', 'group-member-management-list-checkboxes');
	if (selectedUsers.length > 0) {
		switch ($(this).val()) {
			case 'delete':
				selectedUsers = selectedUsers.join();
        config = {
          'id'                 : 'modal-' + getRandKey(),
          'modalClass'         : 'delete-node',
          'modalTitle'         : Yii.t("standard", "_DEL_SELECTED"),
          'modalUrl'           : HTTP_HOST + '?r=groupManagement/deleteGroupMember',
          'modalRequestType'   : 'POST',
          'modalRequestData'   : {'idst' : selectedUsers},
          'buttons'            : [
            {"type": "submit", "title": Yii.t("standard", "_CONFIRM")},
            {"type": "cancel", "title": Yii.t("standard", "_CLOSE")}
          ],
          'afterLoadingContent': 'hideConfirmButton();',
          'afterSubmit'        : 'updateGroupMemberContent'
        }
        $('body').showModal(config);
			break;
		}
	}
	//$('#'+$(this).attr('id')+'-styler ul li:first').click();
	$(this).val('');
})

$('#assign-power-user-management-massive-action').live('change', function() {
	var selectedUsers = getSelectedCheckboxes('assign-power-user-management-list', 'assign-power-user-management-list-checkboxes');
	if (selectedUsers.length > 0) {
		switch ($(this).val()) {
			case 'delete':
        config = {
          'id'                 : 'modal-' + getRandKey(),
          'modalClass'         : 'delete-node',
          'modalTitle'         : Yii.t("standard", "_DEL_SELECTED"),
          'modalUrl'           : HTTP_HOST + '?r=PowerUserApp/powerUserManagement/deleteAssignedUser',
          'modalRequestType'   : 'POST',
          'modalRequestData'   : {'idst_list': selectedUsers.join(',') },//{'idst' : selectedUsers},
          'buttons'            : [
            {"type": "submit", "title": Yii.t("standard", "_CONFIRM")},
            {"type": "cancel", "title": Yii.t("standard", "_CLOSE")}
          ],
          'afterLoadingContent': 'hideConfirmButton();',
          'afterSubmit'        : 'updatePowerUserAsignUsersContent'
        }
        $('body').showModal(config);
			break;
		}
	}
	$('#'+$(this).attr('id')+'-styler ul li:first').click();
	$(this).val($("option:first", this).val());
})
	/*$('#assign-course-catalog-management-user-massive-action').live('change', function() {
		var selectedUsers = getSelectedCheckboxes('assign-course-catalog-management-user-list', 'assign-course-catalog-management-user-list-checkboxes');
		if (selectedUsers.length > 0) {
			switch ($(this).val()) {
				case 'delete':
				config = {
					'id'                 : 'modal-' + getRandKey(),
					'modalClass'         : 'delete-node',
					'modalTitle'         : Yii.t("standard", "_DEL_SELECTED"),
					'modalUrl'           : HTTP_HOST + '?r=CoursecatalogApp/CourseCatalogManagement/deleteAssignedUser',
					'modalRequestType'   : 'POST',
					'modalRequestData'   : {'ids' : selectedUsers},
					'buttons'            : [
	          {"type": "submit", "title": Yii.t("standard", "_CONFIRM")},
	          {"type": "cancel", "title": Yii.t("standard", "_CLOSE")}
					],
					'afterLoadingContent': 'hideConfirmButton();',
					'afterSubmit'        : 'updateCourseCatalogAssignUsersContent'
				}
				$('body').showModal(config);
				break;
			}
		}
		$('#'+$(this).attr('id')+'-styler ul li:first').click();
	});*/
	$('#assign-course-catalog-management-course-massive-action').live('change', function() {
		var selectedUsers = getSelectedCheckboxes('assign-course-catalog-management-course-list', 'assign-course-catalog-management-course-list-checkboxes');
		if (selectedUsers.length > 0) {
			switch ($(this).val()) {
				case 'delete':
				config = {
					'id'                 : 'modal-' + getRandKey(),
					'modalClass'         : 'delete-node',
					'modalTitle'         : 'Delete assigned courses',
					'modalUrl'           : HTTP_HOST + '?r=CoursecatalogApp/CourseCatalogManagement/deleteAssignedCourse',
					'modalRequestType'   : 'POST',
					'modalRequestData'   : {'ids' : selectedUsers},
					'buttons'            : [
	          {"type": "submit", "title": Yii.t("standard", "_CONFIRM")},
	          {"type": "cancel", "title": Yii.t("standard", "_CANCEL")}
					],
					'afterLoadingContent': 'hideConfirmButton();',
					'afterSubmit'        : 'updateCourseCatalogAssignCoursesContent'
				}
				$('body').showModal(config);
				break;
			}
		}
		$('#'+$(this).attr('id')+'-styler ul li:first').click();
        $(this).val('');
	});
	$('#coupon-assigned-courses-massive-action').live('change', function() {
		var selectedCourses = getSelectedCheckboxes('coupon-assigned-courses-list', 'coupon-assigned-courses-list-checkboxes');
        var idCoupon = $('#coupon-assigned-courses-list').data('coupon-id');
		if (selectedCourses.length > 0) {
			switch ($(this).val()) {
				case 'delete':
				config = {
					'id'                 : 'modal-' + getRandKey(),
					'modalClass'         : 'delete-node modal-delete-coupon-assigned-courses',
					'modalTitle'         : 'Delete assigned courses',
					'modalUrl'           : HTTP_HOST + '?r=EcommerceApp/EcommerceApp/axMassiveDeleteAssignedCourse',
					'modalRequestType'   : 'POST',
					'modalRequestData'   : {'ids' : selectedCourses, 'id_coupon' : idCoupon},
					'buttons'            : [
	          {"type": "submit", "title": Yii.t("standard", "_CONFIRM")},
	          {"type": "cancel", "title": Yii.t("standard", "_CANCEL")}
					],
					'afterLoadingContent': 'hideConfirmButton();',
					'afterSubmit'        : 'updateCouponAssignCoursesContent'
				}
				$('body').showModal(config);
				break;
			}
		}
		$('#'+$(this).attr('id')+'-styler ul li:first').click();
        $(this).val('');
	});
	$('#certificate-release-management-massive-action').live('change', function() {

		// Check if we are issuing/deleting/downloading certificates
		// for a Course or Learning Plan
		switch($('input[name=issue_for]:checked').val()){
			case 'plan':
				var learningPlanId = $('.modal.in').find('#LearningCoursepathUser_id_path').val();
				break;
			case 'course':default:
				var courseId = $('.modal.in').find('#LearningCourseuser_idCourse').val();
				break;
		}

		if(!courseId && !learningPlanId){
			alert('No plan or course selected');
			return;
		}

		var certificateId = $('.modal.in').find('#certificateId').val();
		var selectedUsers = getSelectedCheckboxes('certificate-release-management-grid', 'certificate-release-management-grid-checkboxes');
		if (selectedUsers.length > 0) {
			switch ($(this).val()) {
				case 'generate':
					switch($('input[name=issue_for]:checked').val()){
						case 'plan':
							var url = '?r=certificateManagement/generateCertificate&id=' + certificateId + '&plan_id='+learningPlanId;
							break;
						case 'course':
						default:
							var url = '?r=certificateManagement/generateCertificate&id=' + certificateId + '&course_id='+courseId;
						break;
					}
					$.ajax({
						type: "POST",
						url: HTTP_HOST + url,
						data: {'user_ids' : selectedUsers},
						dataType: 'json',
						beforeSend: function(xhr) {
							$('#certificate-release-management-grid').addClass('grid-view-loading');
						},
						success: function(data){
							$.fn.yiiGridView.update('certificate-release-management-grid', {
								data: $('#certificate_form').serialize()
							});
							if(data.messages)
							{
								var html = '<div class="alert alert-error in alert-block fade">';
								html += '<button type="button" class="close" data-dismiss="alert">x</button>';
								html += data.messages;
								html += '</div>';

								$('#certificate-flash-error').html(html);
							}
						},
						done: function(xhr) {
							$('#certificate-release-management-grid').removeClass('grid-view-loading');
						}
					});
				break;
				case 'delete':
					switch($('input[name=issue_for]:checked').val()){
						case 'plan':
							var url = '?r=certificateManagement/removeCertificate&id=' + certificateId + '&plan_id='+learningPlanId;
							break;
						case 'course':
						default:
							var url = '?r=certificateManagement/removeCertificate&id=' + certificateId + '&course_id='+courseId;
							break;
					}
					$.ajax({
						type: "POST",
						url: HTTP_HOST + url,
						data: {'user_ids' : selectedUsers},
						dataType: 'json',
						beforeSend: function(xhr) {
							$('#certificate-release-management-grid').addClass('grid-view-loading');
						},
						success: function(data){
							$.fn.yiiGridView.update('certificate-release-management-grid', {
								data: $('#certificate_form').serialize()
							});
						},
						done: function(xhr) {
							$('#certificate-release-management-grid').removeClass('grid-view-loading');
						}
					});
				break;
			}
		}
		$('#'+$(this).attr('id')+'-styler ul li:first').click();
		$('#certificate-release-management-massive-action option').first().attr('selected', true);
	});

  $('#report-management-export-massive-action').live('change', function () {
    document.location.href = HTTP_HOST + '?r=reportManagement/exportCustomReport&type=' + $(this).val() + '&id=' + $(this).data('report-id');
    return true;
  });

	$('#report-management-send-email-action').live('click', function() {
    var selectedUsers = getSelectedCheckboxes('custom-report-list', 'custom-report-list-selected-items-list-checkboxes');
		if (selectedUsers.length > 0) {
			config = {
				'id'                 : 'modal-' + getRandKey(),
				'modalClass'         : 'send-email',
				'modalTitle'         : 'Send Email',
				'modalUrl'           : HTTP_HOST + '?r=reportManagement/sendEmail',
				'modalRequestType'   : 'POST',
				'modalRequestData'   : {'ids' : selectedUsers},
				'buttons'            : [
          {"type": "submit", "title": Yii.t("standard", "_SEND")},
          {"type": "cancel", "title": Yii.t("standard", "_CANCEL")}
				],
				'afterLoadingContent': 'hideConfirmButton(); courseFormInit();',
				'beforeSubmit'			 : 'beforeReportSendSubmit',
				'afterSubmit'        : 'updateReportSendContent'
			}
			$('body').showModal(config);
		}
	});

  /********************************************************************************************************************/
   //GRID VIEW SELECT
    // todo igor/ need to be improved
	// Revised: andy
	// click on checkbox for selecting whole page
	$('.grid-view thead .checkbox-column input[type=checkbox]').live('change', function() {
		var checkboxes = $(this).parents('table').find('tbody span.jq-checkbox');

		if (this.checked) {
			checkboxes = checkboxes.not('.checked');
		} else {
			checkboxes = checkboxes.filter('.checked');
		}

		checkboxes.each(function() {
			$(this).click();
		});
	});

	// Links Select/Deselect all for GridView
	$('div.grid-selections a.select-all').live('click', function() {
		var gridId = $(this).closest('div.grid-selections').data('grid');
		if ($('.modal.in').length > 0) {
			var gridView = $('.modal.in').find('#' + gridId);
		} else {
			var gridView = $('#' + gridId);
		}

		if (!gridView.find('thead .checkbox-column input[type=checkbox]').get(0).checked) {
			gridView.find('thead .checkbox-column > span').click();
		}
		if ($('.modal.in').length > 0) {
			$('.modal.in').find('#'+gridView.attr('id')+'-selected-items-list').val($('.modal.in').find('#'+gridView.attr('id')+'-all-items-list').val());
		} else {
			$('#'+gridView.attr('id')+'-selected-items-list').val($('#'+gridView.attr('id')+'-all-items-list').val());
		}
		updateSelectedCheckboxesCounter(gridView.attr('id'));
		return false;
	});

	$('#CoreUser_selectAll').live('change', function() {
		var _this = $(this);
		var gridId = $(this).closest('div.grid-selections').data('grid');
		if ($('.modal.in').length > 0) {
			var gridView = $('.modal.in').find('#' + gridId);
		} else {
			var gridView = $('#' + gridId);
		}

		gridView.find('.column-field-checkbox input[type=checkbox]').each(function(key, item){
			if (_this.prop('checked') !== $(this).prop('checked')) {
				$(this).next('span').click();
				// gridView.find('.column-field-checkbox > span').click();
			}
		});

		if (_this.prop('checked')) {
			$('#'+gridView.attr('id')+'-selected-items-list').val($('#'+gridView.attr('id')+'-all-items-list').val());
		} else {
			$('#'+gridView.attr('id')+'-selected-items-list').val('');
		}
		updateSelectedCheckboxesCounter(gridView.attr('id'));
		return false;
	});



	$('div.grid-selections a.deselect-all').live('click', function() {
		var gridId = $(this).closest('div.grid-selections').data('grid');
		if ($('.modal.in').length > 0) {
			var gridView = $('.modal.in').find('#' + gridId);
		} else {
			var gridView = $('#' + gridId);
		}

		if (gridView.find('thead .checkbox-column input[type=checkbox]').get(0).checked) {
			gridView.find('thead .checkbox-column > span').click();
		} else {
			gridView.find('tr td input:checked').attr('checked', false).trigger('refresh');
		}

		if ($('.modal.in').length > 0) {
			$('.modal.in').find('#'+gridView.attr('id')+'-selected-items-list').val('');
		} else {
			$('#'+gridView.attr('id')+'-selected-items-list').val('');
		}
		updateSelectedCheckboxesCounter(gridView.attr('id'));
		return false;
	});

	// Update checkbox counter for GridView xxxx


	/**
	 * FUCK-UP: Exclude 'non-quartz' items table  (grid view) from this event listener.
	 */
	//$('div.grid-view td input[type=checkbox]').live('change', function() {
	$('div.grid-view td:not(.non-quartz-items tr td) input[type=checkbox]').live('change', function() {
		var gridView = $(this).closest('.grid-view');

		saveCurrentCheckboxState(gridView.prop('id'), $(this));

		if ($(this).is(":checked")) {
			$(this).closest('tr').addClass('selected');
		} else {
			$(this).closest('tr').removeClass('selected');
		}

		var rowCheckboxes = gridView.find('td input.select-on-check');
		if (rowCheckboxes.not(':checked').length == 0) {
			var checkAll = gridView.find('th input.select-on-check-all');
			checkAll.attr('checked', true).trigger('refresh');
		} else if (rowCheckboxes.is(':checked'))  {
			var checkAll = gridView.find('th input.select-on-check-all');
			checkAll.attr('checked', false).trigger('refresh');
		}
		updateSelectedCheckboxesCounter(gridView.attr('id'));
	});

  function saveCurrentCheckboxState(containerId, checkboxObj) {

  	if ($('.modal.in').length > 0) {
    	var selectedItemsObj = $('.modal.in').find('#' + containerId + '-selected-items-list');
  	} else {
    	var selectedItemsObj = $('#' + containerId + '-selected-items-list');
  	}

    var selectedItems = selectedItemsObj.val();
    if (selectedItems != '') {
      selectedItems += ',';
    }
    if (checkboxObj.is(':checked')) {
      selectedItems += checkboxObj.val() + ',';
    } else {
        selectedItems = selectedItems.split(',').filter(Boolean);
        var index = selectedItems.indexOf(checkboxObj.val());
        if (index > -1) {
            selectedItems.splice(index, 1);
        }
        selectedItems = selectedItems.toString() + ',';
      //selectedItems = selectedItems.replace(checkboxObj.val() + ',', '');
    }
    selectedItemsObj.val(selectedItems.slice(0, -1));
    return true;
  }

  jQuery.fn.updateCheckboxesAfterChangeStatus = function(){
    if($('div.list-selections a.deselect-this').hasClass('active') && $('div.list-selections a.select-all').hasClass('active')){
        $("a.deselect-this").trigger('click');
    }else if($('div.list-selections a.deselect-this').hasClass('active') && $('div.list-selections a.deselect-all').hasClass('active')){
        $("a.deselect-all").trigger('click');
    } else{
        var listView = $('#' + $('div.list-selections').data('grid'));
        listView.find('.checkbox-column input[type=checkbox]').removeAttr('checked');
        listView.find('.checkbox-column .jq-checkbox').removeClass('checked');
        $('#' + listView.attr('id') + '-selected-items-list').val('');
        updateSelectedCheckboxesCounter(listView.attr('id'));
    }
  }

  jQuery.fn.updateListViewSelectPage = function(){
            var gridId = $('.list-view').prop('id');
            var checkeddItemsOnPage = $('#' + gridId + '.list-view .item .checkbox-column input[type=checkbox]:checked').length;
            //var checkedItems = $('.checkbox-column').children("input:checked").length;
            var totalItems = $('.checkbox-column').closest('.item').length;
            if(checkeddItemsOnPage < totalItems || totalItems == 0){
                $('div.list-selections a.select-this').addClass('active');
                $('div.list-selections a.deselect-this').removeClass('active');
            } else{
                $('div.list-selections a.select-this').removeClass('active');
                $('div.list-selections a.deselect-this').addClass('active');
            }
            $('#assign-course-catalog-management-user-massive-action').val('');
  }

  /********************************************************************************************************************/
  //LIST VIEW SELECT

  $('div.list-selections a.select-all, div.list-selections a.deselect-all').live('click', function () {
    var listView = $('#' + $(this).closest('div.list-selections').data('grid'));
    $(this).removeClass('active');
    if ($(this).hasClass('select-all')) {
      listView.find('.checkbox-column input[type=checkbox]').attr('checked', 'checked');
      listView.find('.checkbox-column .jq-checkbox').addClass('checked');
      $('#' + listView.attr('id') + '-selected-items-list').val($('#' + listView.attr('id') + '-all-items-list').val());
      $('div.list-selections a.deselect-all').addClass('active');
      $('div.list-selections a.select-this').removeClass('active');
      $('div.list-selections a.deselect-this').addClass('active');
    } else {
      listView.find('.checkbox-column input[type=checkbox]').removeAttr('checked');
      listView.find('.checkbox-column .jq-checkbox').removeClass('checked');
      $('#' + listView.attr('id') + '-selected-items-list').val('');
      $('div.list-selections a.select-all').addClass('active');
      $('div.list-selections a.select-this').addClass('active');
      $('div.list-selections a.deselect-this').removeClass('active');
    }
    updateSelectedCheckboxesCounter(listView.attr('id'));
    return false;
  });

  $('div.list-selections a.select-this, div.list-selections a.deselect-this').live('click', function () {
    var containerId = $(this).closest('div.list-selections').data('grid');
    var listView = $('#' + containerId);
    var checkboxes = listView.find('.checkbox-column input[type=checkbox]');
    $(this).removeClass('active');
    if ($(this).hasClass('select-this')) {
      checkboxes = checkboxes.not(':checked');
      $('div.list-selections a.deselect-this').addClass('active');
      checkboxes.attr('checked', 'checked');
      checkboxes.next().addClass('checked');
    } else {
      checkboxes = checkboxes.filter(':checked');
      $('div.list-selections a.select-this').addClass('active');
      checkboxes.removeAttr('checked');
      checkboxes.next().removeClass('checked');
    }
    checkboxes.each(function(){
      saveCurrentCheckboxState(containerId, $(this));
    });
    updateSelectedCheckboxesCounter(containerId);

	  // Update Select/Unselect all related to Select/Deselect page
	  var allListView = $('#' + $(this).closest('div.list-selections').data('grid'));
	  var testGridId = allListView.attr('id');
	  var testSelectedItems = Number($('#'+testGridId+'-selected-items-count').text());
	  if(testGridId == "badge-assign-list") {
		  var allItemsCount = $('#' + testGridId + '-all-items-list').val().split(',').length;
	  } else if(testGridId == "curricula-user-list") {
		  var allItemsCount = $('#' + testGridId + '-all-items-list').val().split(',').length;
	  } else {
		  var allItemsCount = Number($('#' + testGridId + '-all-items-count').val());
	  }
	  if(testSelectedItems >= allItemsCount) {
		  $('div.list-selections .deselect-all').addClass('active');
		  $('div.list-selections .select-all').removeClass('active');
	  } else {
		  $('div.list-selections .deselect-all').removeClass('active');
		  $('div.list-selections .select-all').addClass('active');
	  }
  });

  // Links Select/Deselect all for ListView
 /* $('div.list-selections a.select-all').live('click', function () {
    var listView = $('#' + $(this).closest('div.list-selections').data('grid'));
    listView.find('.checkbox-column input[type=checkbox]').attr('checked', 'checked');
    listView.find('.checkbox-column .jq-checkbox').addClass('checked');
    $('#' + listView.attr('id') + '-selected-items-list').val($('#' + listView.attr('id') + '-all-items-list').val());
    $(this).toggleClass('active');
    updateSelectedCheckboxesCounter(listView.attr('id'));
    return false;
  });

  $('div.list-selections a.deselect-all').live('click', function () {
    var listView = $('#' + $(this).closest('div.list-selections').data('grid'));
    listView.find('.checkbox-column input[type=checkbox]').attr('checked', '');
    listView.find('.checkbox-column .jq-checkbox').removeClass('checked');
    $('#' + listView.attr('id') + '-selected-items-list').val('');
    $(this).toggleClass('active');
    updateSelectedCheckboxesCounter(listView.attr('id'));
    return false;
  });*/

  $('div.list-view input[type=checkbox]').live('change', function () {
    var listView = $(this).closest('.list-view');
    var selectedItems = $('#' + listView.prop('id') + '-selected-items-list').val();

    if (selectedItems != '') {
      selectedItems += ',';
    }

    if ($(this).is(':checked')) {
      selectedItems += $(this).val() + ',';
    } else {
      selectedItems = selectedItems.split(',').filter(Boolean);
      var index = selectedItems.indexOf($(this).val());
      if (index > -1) {
          selectedItems.splice(index, 1);
      }
      selectedItems = selectedItems.toString() + ',';
      //selectedItems = selectedItems.replace($(this).val() + ',', '');
    }

    $('#' + listView.prop('id') + '-selected-items-list').val(selectedItems.slice(0, -1));

    updateSelectedCheckboxesCounter(listView.attr('id'));

	  // Update Select/Deselect All and Select/Deselect Page related to one checkbox select/deselect
	  var gridId = listView.prop('id');
	  var oneSelectedItems = Number($('#'+gridId+'-selected-items-count').text());
	  var allItemsPerPage = $('#' + gridId + '.list-view .item').length;
	  var checkedOnPage = $('#' + gridId + '.list-view .item .checkbox-column input[type=checkbox]:checked').length;
	  if(gridId == "badge-assign-list") {
		  var allItemsCount2 = $('#' + gridId + '-all-items-list').val().split(',').length;
	  } else if(gridId == "curricula-user-list") {
		  var allItemsCount2 = $('#' + gridId + '-all-items-list').val().split(',').length;
	  } else {
		  var allItemsCount2 = Number($('#' + gridId + '-all-items-count').val());
	  }

	  if(checkedOnPage < allItemsPerPage && $('div.list-selections .deselect-this').hasClass('active')) {
		  $('div.list-selections .deselect-this').removeClass('active');
		  $('div.list-selections .select-this').addClass('active');
	  } else if(checkedOnPage == allItemsPerPage && $('div.list-selections .select-this').hasClass('active')) {
		  $('div.list-selections .deselect-this').addClass('active');
		  $('div.list-selections .select-this').removeClass('active');
	  }

	  if(oneSelectedItems < allItemsCount2 && $('div.list-selections .deselect-all').hasClass('active')) {
		  $('div.list-selections .deselect-all').removeClass('active');
		  $('div.list-selections .select-all').addClass('active');
	  } else if(oneSelectedItems == allItemsCount2 && $('div.list-selections .select-all').hasClass('active')) {
		  $('div.list-selections .deselect-all').addClass('active');
		  $('div.list-selections .select-all').removeClass('active');
	  }

  });


  /********************************************************************************************************************/

	// Links Select/Deselect all for radioButtons
	$('div.selections a.select-all-radio').live('click', function() {
		var container = $('#'+$(this).closest('div.selections').data('container'));

		container.find('input[value='+$(this).data('value')+']').each(function() {
			$(this).click();
		});
		updateSelectedRadiobuttonsCounter(container.attr('id'));
		return false;
	});

	$('div.selections a.deselect-all-radio').live('click', function() {
		var container = $('#'+$(this).closest('div.selections').data('container'));

		container.find('input[value='+$(this).data('value')+']').each(function() {
			$(this).click();
		});
		updateSelectedRadiobuttonsCounter(container.attr('id'));
		return false;
	});

	// Select descendants radiobutton
	$('#courseEnroll-orgchart-table input[type=radio]').live('change', function() {
		if ($(this).val() == 2) {
			$(this).closest('li').children('ul.nodeUl').addClass('select-descendants');
		} else {
			$(this).closest('li').children('ul.nodeUl').removeClass('select-descendants');
		}
	});

	// Users massive edit. Choosing fields for update
	$('input.checker').live('change', function() {
		var boxContent = $(this).parents('form').find('div.'+$(this).attr('box-content'));
		if (this.checked) {
			boxContent.removeClass('disabled');
			boxContent.find('input, select').removeAttr('disabled');
		} else {
			boxContent.addClass('disabled');
			boxContent.find('input, select').attr('disabled', 'disabled');
		}
		boxContent.find('input, select').trigger('refresh');
	});

	// Users import. Select of node.
	$('#user-import-form select').change(function() {
		$('#user-import-form select').trigger('refresh');
		$('#user-import-form div.jq-selectbox__select-text').html($(this).find('option:selected').text().replace(/\u00a0/g, ""));
	});

	// User import. Select of file.
	$('#user-import-form input[type=file]').change(function() {
		var noFile =  $(this).parents('div.row').find('span.no-file');
		var fileName =  $(this).parents('div.row').find('span.file-name');

		var name = $(this).val().replace(/.+[\\\/]/, "");

		fileName.html(name);
		if (name == '') {
			fileName.hide();
			noFile.show();
		} else {
			noFile.hide();
			fileName.show();
		}
		$('#user-import-form div.jq-file__name').css({position: 'absolute', left: '-9999px'});
	});

	// Users import. Select of node.
	$('#session-import-form select').change(function() {
		$('#session-import-form select').trigger('refresh');
		$('#session-import-form div.jq-selectbox__select-text').html($(this).find('option:selected').text().replace(/\u00a0/g, ""));
	});

	// User import. Select of file.
	$('#session-import-form input[type=file]').change(function() {
		var noFile =  $(this).parents('div.row').find('span.no-file');
		var fileName =  $(this).parents('div.row').find('span.file-name');

		var name = $(this).val().replace(/.+[\\\/]/, "");

		fileName.html(name);
		if (name == '') {
			fileName.hide();
			noFile.show();
		} else {
			noFile.hide();
			fileName.show();
		}
		$('#session-import-form div.jq-file__name').css({position: 'absolute', left: '-9999px'});
	});
//	certificate template background-image
	$('.certificate-background input[type=file]').change(function() {
		var fileName =  $(this).parents('.certificate-background').find('span.file-name');

		var name = $(this).val().replace(/.+[\\\/]/, "");

		fileName.html(name);
		if (name == '') {
			fileName.hide();
		} else {
			fileName.show();
			$('.max-dimensions').css({maxWidth: '190px',width:'auto',display: 'block'});
			$('.file-name').css({display: 'block'});
		}
		$('.certificate-background div.jq-file__name').css({position: 'absolute', left: '-9999px'});
	});


	// Validate input right before Additional field create/edit
	$(document).on("click", '#field-modal .confirm-btn', function(e) {
		var modal = $(this).closest('.modal');

        modal.find('.error').remove();

        
		// IFRAME: check slating secret
		if ($('input[name*="[settings][hash_secret]"]').length > 0) {
			var $secret 		= $('input[name*="[settings][hash_secret]"]').val();
			var $secretRepeat   = $('input[name*="[settings][hash_secret_repeat]"]').val();
			if ($secret != $secretRepeat || !$secret) {
				modal.find('.dropdown_form_title').after($('<span/>', { 'class' : 'error', 'text' : Yii.t('thomsonreuters', 'Secrets do not match or empty')}));
				return false;
			}
		}

		// IFRAME: settings, all fields are required
		var requiredFieldsOk = true;
		$(':input[name*="[settings]"]').each(function(){
			if (!$(this).val()) {
				requiredFieldsOk = false;
				return false
			}
			
		});
		if (!requiredFieldsOk) {
			modal.find('.dropdown_form_title').after($('<span/>', { 'class' : 'error', 'text' : Yii.t('thomsonreuters', 'All fields are required')}));
			return false;
		}
			
        
		if ($(".orgChart_translationsList input.default_lang").length > 0)
		{
			if ($('.orgChart_translationsList input.default_lang').val() == '') {
				modal.find('.dropdown_form_title').after($('<span/>', { 'class' : 'error', 'text' : Yii.t('user_management', '_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED')}));
				return false;
			}
		}
		else
		{
			if ($('.orgChart_translationsList input.default_lang').val() == '') {
				modal.find('.dropdown_form_title').after($('<span/>', { 'class' : 'error', 'text' : Yii.t('user_management', '_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED')}));
				return false;
			}
		}

		if ( ($('input.dropdownOption').length > 0)  && ($('#field-modal select#preview-select option').length <= 0)) {
			modal.find('.dropdown_form_title').after($('<span/>', { 'class' : 'error', 'text' : Yii.t('standard', 'You must define at least one element')}));
			return false;
    	}

		var form = modal.find("form");

		// Make a copy of the whole form, because for dropdown type
		// additional fields we JSON encode the entered dropdown son<==>language
		// pairs in a array and send it that way instead of separate input
		// $_POST fields due to server limits. In order to exclude the actual
		// fields from the $_POST request - we remove them from the DOM
		// (of the copied form, so the original form is never touched)

		// The native clone() does not copy dynamic states for elements select and textarea !!!
		// var formCopy = form.clone(true, true);
		var formCopy = Docebo.cloneForm(form);
		
		/*
		if($('[name="CoreField[type_field]"]', formCopy).val()==='dropdown'){
			var dropdownElems = $('[name^="CoreField[translation]"]', formCopy);
			//var serializedDropdowns = JSON.stringify(dropdownElems.serializeArray());
			var tmp = dropdownElems.serializeArray();
			var objectArray = {};
			for(var i = 0; i < tmp.length; i++)
			{
				var object = tmp[i];
				var explode = object.name.split('[');
				explode[1] = explode[1].replace(']', '');
				explode[2] = explode[2].replace(']', '');
				explode[3] = explode[3].replace(']', '');
				if(!objectArray.hasOwnProperty(explode[0]))
					objectArray[explode[0]] = {};
				if(!objectArray[explode[0]].hasOwnProperty(explode[1]))
					objectArray[explode[0]][explode[1]] = {};
				if(!objectArray[explode[0]][explode[1]].hasOwnProperty(explode[2]))
					objectArray[explode[0]][explode[1]][explode[2]] = {};
				if(explode.length == 6)
				{
					if(!objectArray[explode[0]][explode[1]][explode[2]].hasOwnProperty(explode[3]))
						objectArray[explode[0]][explode[1]][explode[2]][explode[3]] = {};
					explode[4] = explode[4].replace(']', '');
					if(!objectArray[explode[0]][explode[1]][explode[2]][explode[3]].hasOwnProperty(explode[4]))
						objectArray[explode[0]][explode[1]][explode[2]][explode[3]][explode[4]] = [];
					objectArray[explode[0]][explode[1]][explode[2]][explode[3]][explode[4]].push(object.value);
				}
				else
					if(!objectArray[explode[0]][explode[1]][explode[2]].hasOwnProperty(explode[3]))
						objectArray[explode[0]][explode[1]][explode[2]][explode[3]] = object.value;
			}

			var serializedDropdowns = JSON.stringify(objectArray);

			var hiddenJsonElem = $('<input type="hidden" name="dropdownElemsJson"/>').val(serializedDropdowns);
			formCopy.append(hiddenJsonElem);

			dropdownElems.remove();
		}
		*/
		if (DD && DD.translations) {

			DD.updateLanguagePage(DD.currentActiveLanguage);

			var i, x, objectArray = { CoreField: { translation: {} } };
			for (x in DD.translations) { //x is lang_code
				//prepare a temporary variable as options list
				var tmp = {
					field: "",
					options: {
						translation: [],
						idSon: [],
						id_common_son: []
					}};
				tmp.field = $('#CoreField_translation_'+x+'_field').val();
				for (i=0; i<DD.translations[x].translations.length; i++) {
					tmp.options.translation.push(DD.translations[x].translations[i].translation);
					if (DD.translations[x].translations[i].idSon) {
						tmp.options.idSon.push(DD.translations[x].translations[i].idSon);
					}
					if (DD.translations[x].translations[i].id_common_son) {
						tmp.options.id_common_son.push(DD.translations[x].translations[i].id_common_son);
					}
				}
				tmp.options.translation.push(""); //this is required to save correctly the translations, although "useless"
				//read from global translations and prepare to send
				objectArray.CoreField.translation[x] = tmp;
			}

			var dropdownElems = $('[name^="CoreField[translation]"]', formCopy);
			var serializedDropdowns = JSON.stringify(objectArray);
			var hiddenJsonElem = $('<input type="hidden" name="dropdownElemsJson"/>').val(serializedDropdowns);
			formCopy.append(hiddenJsonElem);
			dropdownElems.remove();
		}


		//before sending ajax request, set up a loading icon (will be removed after request)

		var setupLoadingIcon = function() {
			$('.form.dropdown').css('display', 'none');
			$('.saving-dropdown-field').css('display', 'block');
		};
		var unsetLoadingIcon = function() {
			$('.form.dropdown').css('display', 'block');
			$('.saving-dropdown-field').css('display', 'none');
		};

		setupLoadingIcon();

		var btn = $(this);
		btn.addClass('disabled');
		btn.prop('disabled', true);
		// $('input[type=hidden][name=courses]').val();
		if($('select[name=course_categories').length) {
			$('<input name="courses" value="'+$('select[name=course_categories').val()+'">').attr('type','hidden').appendTo(formCopy);
		}

		$.ajax({
			'dataType' : 'json',
			'type' : 'POST',
			'url' : modal.find('form').attr('action'),
			'cache' : false,
			'data' : formCopy.serialize(),
			'success' : function (data) {
				btn.removeClass('disabled');
				btn.prop('disabled', false);

				if (data.redirect) {
					window.location = data.redirect;
				}
				modal.find('.btn.cancel').click();
				$('#fields-grid').yiiGridView('update');
				if (DD) { DD = null; } //if we were editing a dropdown field, clear the editor manager object
			},
			'error': function() {
				btn.removeClass('disabled');
				btn.prop('disabled', false);
				unsetLoadingIcon();
			}
		});
	});

	$('.additional-dropdown').live('change', function() {
		$(this).parent().find('.additional-dropdown-hidden').val($(this).val());
	});

	// .radioButtonTrigger
	$('.radioButtonTrigger').live('click', function() {
		if ($(this).parents('ul.nodeUl').hasClass('select-descendants')) {
			return false;
		}
		buttons = $('.modal.in').find('#'+$(this).attr('for')).find('input[type=radio]');
		checked = buttons.filter(':checked');
		next = checked.nextAll('input[type=radio]').get(0);
		if (next === undefined) {
			next = buttons.get(0);
		}
		next.click();

		updateSelectedRadiobuttonsCounter('courseEnroll-orgchart-table');
		return false;
	});

	$('div.radio-selectors span.jq-radio').live('click', function() {
		$(this).closest('div.radio-selectors').find('.radioButtonTrigger').click();
		return false;
	});

	/**
	 * FUCK-UP: Exclude 'non-quartz' items table  (grid view) from this event listener.
	 * See the next "FUCK-UP" fix for more info.
	 */
	$('.items tr td a:not(.non-quartz-items tr td a)').live('click', function(e) {
		e.stopPropagation();
	});

	//--- For GridView
	/**
	 * FUCK-UP: Before the fix (by adding ":not(.non-quartz-items tr td)"),
	 * ALL Yii grids, ANY Yii grid *row selection* was NOT working, because of this stupid LIVE event!
	 *
	 * Additionally, in your grid widgets (CGridView) config you MUST add the following option:
	 *    'itemsCssClass' 	=> 'items non-quartz-items',
	 */
	$('.items tr td:not(.non-quartz-items tr td)').live('click', function () {
		var tr = $(this).closest('tr');
		if (tr.find('td').is('.checkbox-column')) {
			if (tr.find('input').is(":checked")) {
				tr.find('input').attr('checked', false);
				tr.find('input').trigger('refresh');
				tr.find('input').trigger('change');
				tr.removeClass('selected')
			} else {
				tr.find('input').attr('checked', true);
				tr.find('input').trigger('refresh');
				tr.find('input').trigger('change');
				tr.addClass('selected');
			}

			updateSelectedCheckboxesCounter(tr.closest('div.grid-view').prop('id'));
			return false;
		}
	});
	//--- For ListView
	$('.items .report-row').live('click', function () {
		var input = $(this).find('.column-field-checkbox input');
		if (input.is(":checked")) {
			input.attr('checked', false);
			input.trigger('refresh');
			input.trigger('change');
			$(this).removeClass('selected');
		} else {
			input.attr('checked', true);
			input.trigger('refresh');
			input.trigger('change');
			$(this).addClass('selected');
		}
		updateSelectedCheckboxesCounter($(this).closest('div.list-view').prop('id'));
		return false;
	});
	// ------------

	// newsletter delete file
	$('#filelist span.close').live('click', function() {
		$(this).closest('.file').remove();
		return false;
	});

	// element for clear specific textField
	$('.clear-input').live('click', function() {
		var input = $('#'+$(this).data('input-id'));
		if (input.length) {
			input.val('');
		}
		return false;
	});

});

String.prototype.capitalize = function() {
	return this.charAt(0).toUpperCase() + this.slice(1);
}

function addFilter(selected, advancedSearchFilters) {
	advancedSearchFilters[selected.val()] = selected.text();

	var label = $('<label/>', { 'for': selected.val(), 'text': selected.text() });
	var select = $('<select/>', {
		'id': selected.val(),
		'name': 'advancedSearch[fields][' + selected.val() + '][condition]'
	});
	var options = [
		{ value: 'contains', 'text': 'Contains' },
		{ value: 'equal', 'text': 'Equal to' },
		{ value: 'not-equal', 'text': 'Not equal to' }
	];

	$(options).each(function () {
		select.append($('<option/>', { 'value': this.value, 'text': this.text }));
	});

	var input = $('<input/>', {
		'type': 'textfield',
		'name': 'advancedSearch[fields][' + selected.val() + '][keywords]'
	});

	var removeLink = $('<a/>', {
		'class': 'remove-filter-link',
		'data-filter-value': selected.val(),
		'data-filter-text': selected.text(),
		'click': function () {
			removeFilter($(this), advancedSearchFilters);
		},
		'text': 'X'
	});

	var filterRow = $('<div/>', { 'class': 'filter-row clearfix' }).append(label).append(select).append(input).append(removeLink);

	$('#selected-filters').append(filterRow);

	selected.remove();
	$('select').trigger('refresh');

	$('.filter-row select').styler();
}

function addAdditionalFilter(selected, advancedSearchFilters) {
	advancedSearchFilters[selected.val()] = selected.text();

	var input = $('#advanced-search .elements').find('.' + selected.val()).html();

	/*  $('<input/>', {
		'type': 'textfield',
		'name': 'advancedSearch[additional][' + selected.val() + '][keywords]'
	});*/

	var removeLink = $('<a/>', {
		'class': 'remove-filter-link additional',
		'data-filter-value': selected.val(),
		'data-filter-text': selected.text(),
		'click': function () {
			removeFilter($(this), advancedSearchFilters);
		},
		'text': 'X'
	});

	var filterRow = $('<div/>', { 'class': 'filter-row clearfix' }).append(input).append(removeLink);

	// Remove styled select, so it can be re-initialized
	filterRow.find('.jq-radio, .jq-selectbox').remove();
	filterRow.find('select, input, textarea').removeAttr('disabled');

	$('#selected-filters').append(filterRow);

	selected.remove();
	$('select').trigger('refresh');

	$('.filter-row select, .filter-row input').styler();
}

function removeFilter(link, advancedSearchFilters) {
	delete advancedSearchFilters[link.data('filter-value')];

	var option = $('<option/>', { 'value': link.data('filter-value'), 'text': link.data('filter-text'), 'class': link.hasClass('additional') ? 'additional' : '' });

	$('#select-filter').append(option);
	link.closest('.filter-row').remove();
	$('#select-filter').trigger('refresh');
}

function ajaxGridUpdateCallback() {
	filterColumns();
	$("#users-management-grid input").styler();
}

function filterColumns() {
	if (typeof userManagement != "undefined") {
		for (var i = 0; i < userManagement.hiddenFields.length; i++) {
			$('#users-management-grid').find('.td-' + userManagement.hiddenFields[i]).hide();
		}
	}
}

function getSelectedCheckboxes(gridId, columnId) {
	var selectedItems = $('#'+gridId+'-selected-items-list').val();
	if (selectedItems == '') {
		return [];
	} else {
		return selectedItems.split(',');
	}
}

function updateSelectAllCounter(id,value){
	//Reset the Counter
	$('#' + id + '-selected-items-list').val(value);
	//Reset Select page/all link
	$('div.list-selections a.select-all').addClass('active');
	$('div.list-selections a.deselect-all').removeClass('active');
	$('div.list-selections a.select-this').addClass('active');
	$('div.list-selections a.deselect-this').removeClass('active');
}

function getSelectedCheckboxesCount(gridId, columnId) {
	if ($('.modal.in').length > 0) {
		var selectedItems = $('.modal.in').find('#'+gridId+'-selected-items-list').val();
	} else {
		var selectedItems = $('#'+gridId+'-selected-items-list').val();
	}

	if (selectedItems == '') {
		return 0;
	} else {
		var cnt = 0;
		try{
			cnt = selectedItems.split(',').length;
		}catch(e){}
		return cnt;
	}
}

function afterGridViewUpdate(gridId) {
	if ($('.modal.in').length > 0) {
		var gridView = $('.modal.in').find('#'+gridId);
	} else {
		var gridView = $('#'+gridId);
	}
	gridView.find('select,input').styler();
	if (gridView.find('td').is('.checkbox-column')) {
		$('#'+gridId+' input:checked').closest('tr').addClass('selected');
		if (gridView.find('td input').not(':checked').length == 0) {
			gridView.find('th input').attr('checked', true);
			gridView.find('th input').trigger('refresh');
		}
		updateSelectedCheckboxesCounter(gridId);
	}
}

function afterListViewUpdate(listId) {
	/*var listView = $('#'+listId);

	listView.find('div.checkbox-column input').each(function() {
		$(this).attr('checked', false);
		$(this).trigger('refresh');
	});*/
	updateSelectedCheckboxesCounter(listId);
}

function updateSelectedCheckboxesCounter(gridId) {
	var columnId = gridId + '-checkboxes';
	if ($('.modal.in').length > 0) {
		$('.modal.in').find('#'+gridId+'-selected-items-count').html(getSelectedCheckboxesCount(gridId, columnId));
	} else {
		$('#'+gridId+'-selected-items-count').html(getSelectedCheckboxesCount(gridId, columnId));
	}
}

function getSelectedRadiobuttonsCount(containerId) {
	var count = 0;
	return $('#'+containerId).find('span.select-node-yes.checked').length
		+ $('#'+containerId).find('span.select-node-descendants.checked').length
		+ $('#'+containerId).find('ul.select-descendants span.select-node-no').length
		- $('#'+containerId).find('ul.select-descendants span.select-node-yes.checked').length;
		- $('#'+containerId).find('ul.select-descendants span.select-node-descendants.checked').length;
	/*
	$('#'+containerId).find('input[type=radio]:checked').each(function() {
		if ($(this).closest('ul.select-descendants').get(0) !== undefined) {
			count++;
		} else if (($(this).val() != 0) && ($(this).attr('name') != 'courseEnroll-orgchart[1]')) {
			count++;
		}
	});
*/
	return count;
}

function updateSelectedRadiobuttonsCounter(containerId) {
	if ($('.modal.in').length > 0) {
		$('.modal.in').find('#'+containerId+'-selected-items-count').html(getSelectedRadiobuttonsCount(containerId));
	} else {
		$('#'+containerId+'-selected-items-count').html(getSelectedRadiobuttonsCount(containerId));
	}
}

function refreshDropdownPreview() {
	var selectedLanguage = $('#CoreField_lang_code').val();
	var data = $('.orgChart_translationsList .' + selectedLanguage);
	var inputTitle = data.find('.field-name input').val();
	var options = data.find('.field-options .dropdownOption').slice(0, -1);
	var label = $('<label/>', { 'for': 'preview-select', 'text': inputTitle });
	var select = $('<select/>', { 'id': 'preview-select' });

	var optArray = new Array(); //empty array for option values

	options.each(function(indx) {
		optArray[indx] = $(this).val(); //put option values in array
	});
	optArray.sort(function(a, b) {
		var textA = a.toLowerCase();
		var textB = b.toLowerCase();
		return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
	}); //sorting array alphabetical

	for (var i = 0; i < optArray.length; i++){
		select.append($('<option/>', { 'value': '', 'text': optArray[i] })); //append options to select
	};

	var previewViewport = $('.preview-wrapper .preview-viewport');

	$('.lang-code').html(selectedLanguage.capitalize());

	previewViewport.empty();
	previewViewport.append(label);
	label.wrap('<td></td>');
	previewViewport.append(select);
	select.wrap('<td></td>');
	previewViewport.find('select').styler();
}

function jqconfirm(_options) {
	var options = {
		title : 'Title',
		message : 'Message',
		actionYes : function () {},
		actionNo : function () {},
		afterShow : function() {
			$('#confirmBox input').styler();
		}
	}
	$.extend(options, _options);

	options.buttons = {
		'Yes' : {
			'class' : 'btn confirm-btn',
			'action' : options.actionYes
		},
		'No' : {
			'class' : 'btn close-btn',
			'action' : options.actionNo
		}
	};

	$.confirm(options);
}

function populateAdditionalDropdowns() {
	$('select.additional-dropdown').each(function() {
		var option = $(this).find('option:first');
		$(this).parent().find('.additional-dropdown-hidden').val(option.attr('value'));
	})
}

function applyTypeahead(elements) {
	elements.each(function() {
		var th = $(this);
        var convertList = new Object();
		th.typeahead({
			source: function (query, process) {
				var input = th;
				var icon = input.closest('.input-wrapper').find('.search-icon');

				var data = input.data();
				delete data.typeahead;

				data.query = query;

				input.addClass('loading');
				icon.hide();

				return $.post(data.url, { data: data }, function (data) {
					input.removeClass('loading');
					icon.show();

                    if (th.attr('data-source-desc')) {
	                    // If the typeahead input receives AJAX results in the
	                    // format: [username]=>[username - firstname lastname]
                        var x = 0;
                        var result = [];
                        convertList = new Object();
                        $.each(data.options, function(index, value) {
                            convertList[value] = index;
                            result[x] = value;
                            x++;
                        });
                        data.options = result;

                    }else{
	                    // If the typeahead input receives AJAX results in the
	                    // format: [index/int]=>[username - firstname lastname]
	                    var x = 0;
	                    var result = [];
	                    $.each(data.options, function(index, value) {
		                    result[x] = value;
		                    x++;
	                    });
	                    data.options = result;
                    }

                    return process(data.options);
				});
			},
			updater: function(title) {
                if (th.attr('data-source-desc') && (title in convertList)) {
                    title = convertList[title];
                }
				th.val(title);
				th.closest('.jwizard-form').submit();
				th.closest('.ajax-grid-form').submit();
				th.closest('.ajax-list-form').submit();
				return title;
			},
			matcher: function(title) {
				return true;
			}
		});
	});
}
// fix for typeahead
$.fn.typeahead.Constructor.prototype.render = function(items) {
    var that = this;
    items = $(items).map(function (i, item) {
        i = $(that.options.item).attr('data-value', item);
        i.find('a').html(that.highlighter(item));
        return i[0];
    });
    //items.first().addClass('active');
    this.$menu.html(items);
    return this;
};
$.fn.typeahead.Constructor.prototype.select = function () {
	if (this.$menu.find('.active').length) {
		var val = this.$menu.find('.active').attr('data-value');
        var idCourse = this.$menu.find('.active').attr('data-id');
		if(!idCourse && val){
            idCourse = val;
        }
		this.$menu.find('.active').remove();
	} else {
		var val = this.$element.val();
	}

	this.$element.val(this.updater(val)).change();
	return this.hide();
};
$.fn.typeahead.Constructor.prototype.keyup = function (e) {
	switch (e.keyCode) {
		case 40:
		case 38:
		case 16:
		case 17:
		case 18:
			break;
		case 9:
		case 13:
			//if (!this.shown) return;
			this.select();
			break;
		case 27:
			if (!this.shown) return;
			this.hide();
			break;
		default:
			this.lookup();
	}
	e.stopPropagation();
	e.preventDefault();
};

function enrollFromCsvCallback(responseText, statusText, xhr, $modal) {
	var modal = $modal.parents('.modal');
	var body = modal.find('.modal-body');
	var footer = modal.find('.modal-footer');

	body.empty().append(responseText.html);
	if (responseText.status == 'success') {
		modal.addClass('success');
		footer.hide();
	} else {
		$('input').styler({browseText: "BROWSE"});
	}
}

function parseGetParams() {
	var $_GET = {};
	var __GET = window.location.search.substring(1).split("&");
	for(var i=0; i<__GET.length; i++) {
		var getVar = __GET[i].split("=");
		$_GET[getVar[0]] = typeof(getVar[1])=="undefined" ? "" : getVar[1];
	}
	return $_GET;
}

function getUrlParam(name) {
	var params = parseGetParams();
	return params[name];
}

function modalPosition() {
  var modalTopPx = 10,
  	modalTop = modalTopPx + '%',
  	modalWidth = $('.modal.in').width(),
  	modalHeight = $('.modal.in').height() + ($('.modal.in').height()/100)*modalTopPx,
    windowWidth = $(window).width(),
    windowHeight = $(window).height(),
    modalMargin = -modalWidth / 2;
    if($('body').css('direction')!=='rtl'){
        $('.modal.fade.in').css({'top': modalTop});
    }
  if (windowWidth < modalWidth) {
    $('.modal.in').css({'margin-left': '0', 'left': '0'});
	$('#wrapper').css({'position': 'fixed'});
  } else {
    $('.modal.in').css({'margin-left': modalMargin, 'left': '50%'});
	$('#wrapper').css({'position': 'relative'});
  }
	if($('.modal.in').hasClass('force-absolute')){
		$('.modal.in').css({'position': 'absolute'});
	}else{
		if (windowHeight < modalHeight || windowWidth < modalWidth) {
			$('.modal.in').css({'position': 'absolute'});
		} else {
			$('.modal.in').css({'position': 'fixed'});
		}
	}
}

function sendNewsletter(ajaxUrl) {
	$.ajax({
		url: ajaxUrl,
		dataType: 'json',
		type: 'post',
		data: {
			sendTo: true
		},
		success: function(data) {
			if (data.status == 'send') {
				$('.progress-line').css({width: data.progress + '%'});
				setTimeout(function() {
					$('#current-chunk').html(data.currentChunk);
					sendNewsletter(data.sendTo);
				}, data.pause);
			} else if (data.status == 'done') {
				$('.progress-line').css({width: data.progress + '%'});
				$('.sending-progress.process').hide();
				$('.sent-success').show();
			} else{
                $('.progress-line').css({width: data.progress + '%'});
                $('.sending-progress.process').hide();
                $('#sent-error').show();
				if(data.max_file_size == 'error')
					$('#max_file_size').show();
            }
		}
	});
}

function initReportScroll() {
	var reportFieldWidth = $('.report-header-field').outerWidth(),
		reportCheckboxWidth =  $('.report-header-inner').find('.column-field-checkbox').outerWidth(),
		reportCount = $('.report-header-field').length;
		reportFullWidth = (reportFieldWidth*reportCount) + reportCheckboxWidth;
		$('.report-header-inner').width(reportFullWidth);
		$('.items').width(reportFullWidth);
		reportHeaderWidth = reportFullWidth-8; //8-jspVerticalBar width

	$reportHeader = $('.report-header').jScrollPane();
	$reportList = $('.report-list-items').jScrollPane();

    //sync the two boxes to scroll together
    //bind the scroll to reportHeader
    $(".report-header").bind('jsp-scroll-x', function(event, scrollPositionX, isAtTop, isAtBottom) {
        if ($(this).find(".jspDrag").hasClass("jspActive")) {
            $reportList.data('jsp').scrollToX(scrollPositionX)
        }
    });
    //bind the jump when clicking on the track
    $(".jspTrack",".report-header").bind('mousedown', function() {
        $reportList.data('jsp').scrollToX($reportHeader.data('jsp').getContentPositionX());
    });
    //bind the scroll to reportList
    $(".report-list-items").bind('jsp-scroll-x', function(event, scrollPositionX, isAtTop, isAtBottom) {
        if ($(this).find(".jspDrag").hasClass("jspActive")) {
            $reportHeader.data('jsp').scrollToX(scrollPositionX)
        }
    });
    //bind the jump when clicking on the track
    $(".jspTrack",".report-list-items").bind('mousedown', function() {
        $reportHeader.data('jsp').scrollToX($reportList.data('jsp').getContentPositionX());
    });

   var reportHeaderHeight = $('.report-header-field').outerHeight();
   $('.report-header').find('.jspHorizontalBar').hide();
   $('.report-header').find('.jspContainer').height(reportHeaderHeight);
}

function removeCertificateImg(url)
{


	bootbox.dialog(Yii.t('standard', '_AREYOUSURE'),
		[
			{
				label: Yii.t('standard', '_DEL'),
				'class': 'btn-submit',
				callback: function() {
					$.post(url, {}, function(res) {
						if (res.success == true) {
							Docebo.Feedback.show('success', res.message);
							$('.background-wrapper div').html('');
							$('.remove-background').hide();
						}
						else {
							Docebo.Feedback.show('error', res.message);
						}
					}, 'json');
				}
			},
			{
				label: Yii.t('standard', '_CANCEL'),
				'class': 'btn-cancel',
				callback: function() {

				}
			}

		],
		{
			header: '<i class="i-sprite is-solid-exclam large white"></i>' + Yii.t('standard', '_DEL')
		}
	);
}


/********/
//other grid selection management functions
function removeFromGridSelection(id, toBeRemoved) {

	//check input first
	if (!toBeRemoved || toBeRemoved.length <= 0) return;

	//retrieve existing lists
	var all = $('#'+id+'-all-items-list').val();
	var sel = $('#'+id+'-selected-items-list').val();

	//transform lists into actual data arrays
	if (all) { all = all.split(','); } else { all = []; }
	if (sel) { sel = sel.split(','); } else { sel = []; }

	//subtract items to be removed from lists
	if (all.length > 0) { all = $(all).not(toBeRemoved).get(); } else { all = []; }
	if (sel.length > 0) { sel = $(sel).not(toBeRemoved).get(); } else { sel = []; }

	//update inputs and values
	$('#'+id+'-all-items-list').val(all.join(','));
	$('#'+id+'-selected-items-list').val(sel.join(','));
	$('#'+id+'-all-items-count').val(all.length);
	$('#'+id+'-selected-items-count').html(sel.length);
};
/**********/

/**
 * Finds Orgchart node which is selected as "branch && descendants" (values=2) and then 'select' all childrens as "branch but NO descendants"
 *
 */
function visualizeOrgchartDescendants() {
	$('#courseEnroll-orgchart-table input[type=radio]:checked[value="2"]').each(function() {
		$(this).closest('li').children('ul.nodeUl').addClass('select-descendants');
	});
}

function getParamsFromUrl(){
	var link = $(location).attr('search');
	var start = link.indexOf('&id=');
	if(start == -1) {
		start = link.indexOf('&course_id=');
		start+=11;
	}else{
		start += 4;
	}
	var courseId = 0;
	var end = link.indexOf('&id_session=');
	if(end != -1){
		courseId = link.substr(start, end - start);
	}else{
		courseId = link.substr(start);
	}
	sessionId = null;
	start = link.indexOf('&id_session=');
	if (start != -1) {
		start += 12;
		var sessionId = link.substr(start);
		sessionId = sessionId.match(/\d/g);
		sessionId = sessionId.join('');
	}
	var result = {'courseId': courseId, 'sessionId': sessionId};
	return result;
}

/**
 * Reset all <select>s to the first element, all checkboxes as unchecked
 * and radios to the first value
 *
 * @param e
 */
jQuery.fn.resetInputs = function(){
	this.each(function(){
		$('select option', this).attr('selected', false);
		$('select option:first-child', this).attr("selected", "selected");
		$('select', this).trigger('change');

		$('input[type=checkbox]').attr('checked', false);
	});
};
