var CoachingManagerClass = false;
/**
 * "Coaching" JavaScript compagnion.
 * Load this in main pages, not in AJAX calls!
 * 
 * @param $
 */
(function ($) {
	
	/**
	 * Constructor
	 */
	CoachingManagerClass = function(options) {
        this.defaultOptions = {
			debug: 	true
		};
		
		if (typeof options === "undefined" ) {
			var options = {};
		}
			
		this.init(options);
	};

    /**
	 * Prototype
	 */
	CoachingManagerClass.prototype = {
		
		/**
		 * Initialize class object
		 */
		init: function(options) {
			this.options = $.extend({}, this.defaultOptions, options);

            if($('input#LearningCourse_enable_coaching').length > 0) {
                this.advancedSettingsUiReady();
            }

			this.debug('Initialize');
			this.addListeners();
		},
		
		/**
		 * Make our live easier and provide some debug, if enabled
		 */
		debug: function(message) {
			if (this.options.debug === true) {
				console.log('CoachingManager:', message);
			}
		},

		/**
		 * Add Event listeners
		 */
		addListeners: function() {
			this.debug('Add listeners');

            // On ENEABLE/DISABLE checkbox change for coaching settings
            $('input#LearningCourse_enable_coaching[type="checkbox"]').on('change', 	$.proxy(this.updateBlockableSections, this));
            $(document).on('combogridview.grid-ajax-updated', function(element, options){
                      if(options.gridId === 'coaches-managment'){
                          $('#coaching-edit-session-dialog #combo-grid-form input[type="radio"]').click(function(){
                              if ($(this).is(':checked'))
                              {
                                  $('#LearningCourseCoachingSession_idCoach').val($(this).val());
                              }
                          });
                          var selectedCoach = false;
                          $('input[name="coachChoose"]').each(function(){
                              if ($(this).is(':checked')){
                                  selectedCoach = $(this).val();
                              }
                          });
                          if (!selectedCoach) {
                              $('#LearningCourseCoachingSession_idCoach').val("");
                          }
                          else {
                              $('#LearningCourseCoachingSession_idCoach').val(selectedCoach);
                          }
                      }
                 });
            
            
            // Establish various bundle of listeners::
            
            // Edit Coaching session dialog
            this.editCoachingSessionListeners();	
            
            
        },

        
        /**
         * Called when Edit Coaching Session dialog is ready 
         */
        editCoachingSessionDialogReady: function(fromDelete) {
        	
            $('#edit-Mode-radio-btn,#merge-mode-radio-btn').styler();
            
            var collectionShowOnMerge   = $('#coaching-merge-mode,#currentSession-coach');
            var collectionShowOnEdit    = $('#coaching-creation-edit-mode,#start-date-sprite,#end-date-sprite,#max-users-container,input.datepicker#LearningCourseCoachingSession_start_date,input.datepicker#LearningCourseCoachingSession_end_date');

            $('#start-date-label').html($('#start-date-label').html()+'<span></span>');
            $('#end-date-label').html($('#end-date-label').html()+'<span></span>');

            // Are we coming from DELETE dialog?  (requesting a merge)
            if ( fromDelete ) {
                collectionShowOnEdit.hide();
                collectionShowOnMerge.show();
                $('#merge-mode-radio-btn').trigger('click');
            }
            else {
                $('.coaching-mode-selection-outer').show();
                collectionShowOnMerge.hide();
                collectionShowOnEdit.show();
            }
        	
        },
        
        /**
         * Set listeners specific to EDIT Coaching session (incl. merge)
         */
        editCoachingSessionListeners: function() {
        	
            /**
             * When user selects a Coach in EDIT coaching session dialog
             */
            $(document).on('click', '#coaching-edit-session-dialog #coaching-creation-edit-mode input[type="radio"]', function(){
                if ($(this).is(':checked')) {
                    $('#LearningCourseCoachingSession_idCoach').val($(this).val());
                }
            });

            /**
             * When user select a TARGET SESSION where currently edited sessions to be merged into (EDIT session dialog) 
             */
            $(document).on('click', '#coaching-edit-session-dialog #coaching-merge-mode input[type="radio"], #delete-certification-dialog #coaching-merge-mode input[type="radio"]', function(){
                if ($(this).is(':checked')) {
                    $('#targetSession').val($(this).val());
                }
            });

            /**
             * When start/end date is changed in EDIT SESSION dialog, force grid filtering to run
             */
            $(document).on('change', 'input.datepicker#LearningCourseCoachingSession_start_date', function() {
                $('input[type="hidden"]#start_date').val($(this).val());
                $('input[type="hidden"]#start_date').trigger( "change" );
            });
            $(document).on('change', 'input.datepicker#LearningCourseCoachingSession_end_date', function() {
                $('input[type="hidden"]#end_date').val($(this).val());
                $('input[type="hidden"]#end_date').trigger( "change" );
            });
            
            
            /**
             * Changing HIDDEN fields attached to Combo Grid FORM triggers grid view update
             */
            $(document).on('change', 'input[type="hidden"]#start_date,input[type="hidden"]#end_date', function() {
                var options = {
                		data: $('#combo-grid-view-container-coaches-managment #combo-grid-form').serialize()
                };
                // Do the yiiGridView update here
                $.fn.yiiGridView.update("coaches-managment", options);
            })
            

            /**
             * Edit mode button selected (as opposed to Merge)
             */
            $(document).on('change', '#edit-Mode-radio-btn', function() {
                if ($(this).is(':checked')) {
                	
                    var collectionShowOnMerge   = $('#coaching-merge-mode,#currentSession-coach');
                    var collectionShowOnEdit    = $('.input-append .add-on, #coaching-creation-edit-mode,#start-date-sprite,#end-date-sprite,#max-users-container,input.datepicker#LearningCourseCoachingSession_start_date,input.datepicker#LearningCourseCoachingSession_end_date');
                            
                    $('#start-date-label span').html('');
                    $('#end-date-label span').html('');
                    collectionShowOnEdit.show();
                    collectionShowOnMerge.hide();
                    $('input.datepicker#LearningCourseCoachingSession_start_date,input.datepicker#LearningCourseCoachingSession_end_date').removeAttr('disabled');
                    $('#targetSession').val('');
                    $('#isMerging').val('');

                }
            });
            

            /**
             * Merge radio button has been selected
             */
            $(document).on('change', '#merge-mode-radio-btn', function() {
                if ($(this).is(':checked')) {
                	
                    var collectionShowOnMerge   = $('#coaching-merge-mode,#currentSession-coach');
                    var collectionShowOnEdit    = $('.input-append .add-on, #coaching-creation-edit-mode,#start-date-sprite,#end-date-sprite,#max-users-container,input.datepicker#LearningCourseCoachingSession_start_date,input.datepicker#LearningCourseCoachingSession_end_date');
                    
                	$('#start-date-label span').html(': <strong>'+$('input.datepicker#LearningCourseCoachingSession_start_date').val()+'</strong>');
                    $('#end-date-label span').html(': <strong>'+$('input.datepicker#LearningCourseCoachingSession_end_date').val()+'</strong>');
                    collectionShowOnEdit.hide();
                    collectionShowOnMerge.show();
                    $('input.datepicker#LearningCourseCoachingSession_start_date,input.datepicker#LearningCourseCoachingSession_end_date').attr('disabled','disabled');
                    $('#isMerging').val('true');
                }
            });
            $(document).on('click',"input.datepicker#LearningCourseCoachingSession_start_date, input.datepicker#LearningCourseCoachingSession_end_date",function(){
                var clicked = $(this);
                $('.coaching-session-date').each(function(){
                    if($(this).attr('id') != clicked.attr('id'))
                        $(this).bdatepicker('hide');
                });
            });

        },
        
        
        /**
         * Some sections get visually and UI blocked if certain checkbox is not checked
         */
        updateBlockableSections: function() {

            // COURSE ADVANCED SETTINGS
            if ($('input[name="LearningCourse[enable_coaching]"]').is(':checked')) {
                $('#coaching .blockable').unblock();
            }
            else {
                this.blockBlockable($('#coaching .blockable'));
            }

        },

        /**
         * Using jQuery blockUI plugin, block specific element
         */
        blockBlockable: function(elem) {
            elem.block({
                message: "",
                timeout: 0,
                overlayCSS:  {
                    backgroundColor: 	'#FFF',
                    opacity:         	0.6,
                    cursor:          	'auto'
                }
            });
        },

        /**
         * Called when Course -> Advanced settings page is loaded
         */
        advancedSettingsUiReady: function() {
            // COURSE ADVANCED SETTINGS
            if ($('input#LearningCourse_enable_coaching').is(':checked')) {
                $('#coaching .blockable').unblock();
            } else {
                this.blockBlockable($('#coaching .blockable'));
            }
        }



    };
	

})(jQuery);

