<?php

if (YII_APP_RUN_TYPE !== "console") {
	defined("IN_DOCEBO") or die('Direct access is forbidden.');
}

define('_DOCEBO_SESSION_LENGHT', 	7200);
define('_DOCEBO_SESSION_SAVE_PATH', false);
define('DS', 						DIRECTORY_SEPARATOR);


define( '_folder_addons_', 			'addons' );
define( '_folder_adm_', 			'doceboCore' );
define( '_folder_lms_', 			'doceboLms' );
define( '_folder_cms_', 			'doceboCms' );
define( '_folder_scs_', 			'doceboScs' );
define( '_folder_i18n_', 			'i18n' );
define( '_folder_lib_', 			'lib' );
define( '_folder_mobile_', 			'mobile' );
define( '_folder_tcapi_', 			'tcapi' );
define( '_folder_plugins_', 		'plugins' );
define( '_folder_custom_plugins_', 	'custom_plugins' );
define( '_folder_tos_',     		'tos' );

define( '_base_', 			realpath(__DIR__));

define( '_addons_',			_base_.'/'._folder_addons_ );
define( '_adm_', 			_base_.'/'._folder_adm_ );
define( '_lms_', 			_base_.'/'._folder_lms_ );
define( '_cms_', 			_base_.'/'._folder_cms_ );
define( '_scs_', 			_base_.'/'._folder_scs_ );
define( '_i18n_', 			_base_.'/'._folder_i18n_ );
define( '_lib_', 			_base_.'/'._folder_lib_ );
define( '_mobile_',			_base_.'/'._folder_mobile_ );
define( '_tcapi_',			_base_.'/'._folder_tcapi_ );
define( '_plugins_', 		_base_.'/'._folder_plugins_ );
define( '_custom_plugins_', _base_.'/'._folder_custom_plugins_ );
define( '_tos_', 	    	_base_.'/'._folder_tos_ );
define( '_lmsassets_', 		_base_.'/lms/assets');



/**
 * other nice setting
 */
define( '_after_login_', 'lms/elearning/show' );

/**
 * List the devices for which you want to use the mobile version
 */
define('_ua_mobile_', "['android','iphone','ipod','ipad','opera mini','blackberry','webOS']");//