<?php

/**
 * Component for converting pdf files to jpgs and jpeg resizing using ImageMagick
 */
class ImagickComponent extends CApplicationComponent
{
	public $convertCommand = '"C:\Program Files (x86)\ImageMagick-6.8.4-Q16\convert.exe"';

	/**
	 * Jpgs will be saved as <slideName>-0.jpg, <slideName>-1.jpg ...
	 * @var string
	 */
	public $slideName  = 'Slide';

	/**
	 * Converts pdf file to jpgs
	 *
	 * @param string $inputFile Path to pdf file
	 * @param string $outputFile Path to jpg file
	 */
	public function pdfToJpg($inputFile, $outputFile)
	{
		$cmd = $this->convertCommand . ' -colorspace rgb -density 200 ' . escapeshellarg($inputFile) . ' ' . escapeshellarg($outputFile);
		exec($cmd);
	}

	/**
	 * Converts pdf file to jpgs in subdir named same as input file, and returns
	 * count of jpg files
	 *
	 * @param type $inputFile Pdf file
	 * @param type $outputPath Path to output directory
	 * @return integer Count of slides
	 */
	public function convertPdf($inputFile, $outputPath)
	{
		if (!file_exists($outputPath))
		{
			mkdir($outputPath);
		}
		$jpgPath = $outputPath . DIRECTORY_SEPARATOR . $this->slideName . '.jpg';
		$this->pdfToJpg($inputFile, $jpgPath);
	}

	/**
	 * Resizes image
	 *
	 * @param string $inputFile Path to input file
	 * @param string $outputFile Path to output file
	 * @param string $resolution Resolution. Example: 800x600
	 */
	public function resizeImage($inputFile, $outputFile, $resolution)
	{
		$cmd = $this->convertCommand . ' ' . escapeshellarg($inputFile) . ' -resize ' . escapeshellarg($resolution) . ' ' . escapeshellarg($outputFile);
		exec($cmd);
	}
}
