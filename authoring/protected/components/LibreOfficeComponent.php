<?php

/**
 * Component for converting odp files to pdf using LibreOffice
 */
class LibreOfficeComponent extends CApplicationComponent
{
	/**
	 * Path to soffice.exe
	 * @var string
	 */
	public $soffice = 'C:\Program Files (x86)\LibreOffice 4.0\program\soffice.exe';
	/**
	 * ImageMagick component name
	 * @var string
	 */
	public $imagickComponentName = 'imagick';
	/**
	 * Jpgs will be saved as <slideName>-0.jpg, <slideName>-1.jpg ...
	 * @var string
	 */
	public $slideName  = 'Slide';
	
	/**
	 * Converts odp file to pdf
	 * 
	 * @param string $inputFile Path to input odp file
	 * @param string $outputDir Path to output dir
	 * @return integer Count of slides
	 */
	public function convertPresentation($inputFile, $outputDir)
	{		
		$cmd = escapeshellarg($this->soffice) . ' --headless --invisible --convert-to pdf --outdir ' . escapeshellarg($outputDir) . ' ' . escapeshellarg($inputFile);
		exec($cmd);
		
		$info = pathinfo($inputFile);		
		$pdfPath = $outputDir . DIRECTORY_SEPARATOR . $info['filename'] . '.pdf';
		$imagick = Yii::app()->getComponent($this->imagickComponentName);
		$imagick->convertPdf($pdfPath, $outputDir);
		if (file_exists($pdfPath))
		{
			unlink($pdfPath);
		}
	}
}
