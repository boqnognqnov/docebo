<?php

/**
 * Component for converting pptx and ppt files to jpg using MS Office
 */
class PowerPointComponent extends CApplicationComponent
{
	const PP_SAVE_AS_JPG = 17;
	const MSO_TRUE = -1;
	const MSO_FALSE = 0;

	/**
	 * Converts ppt, pptx files into jpgs
	 *
	 * @param string $inputFile Path to presentation file
	 * @param string $outputFile Path to jpgs files
	 *
	 * @return integer Count of slides
	 */
	public function convertPresentation($inputFile, $outputFile)
	{
		$powerPoint = new COM('PowerPoint.Application');
		$presentation = $powerPoint->presentations->Open($inputFile, self::MSO_FALSE, self::MSO_FALSE, self::MSO_FALSE);
		$presentation->saveAs($outputFile, self::PP_SAVE_AS_JPG);

		$windowsCount = $presentation->Windows->Count;
		for ($i = 1; $i <= $windowsCount; $i++)
		{

			$presentation->Windows[$i]->Close();
		}

		return $presentation->Slides->Count;
	}
}
