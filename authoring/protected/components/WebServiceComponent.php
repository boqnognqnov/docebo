<?php
/**
 * Component for delegating image conversion to an external server
 */
class WebServiceComponent extends CApplicationComponent {
	
	public $active = false;
	public $convertUrl = '';
	public $domain = '';
	public $authKey = '';
	
	public function externalConversion($params) {
		if (!$this->active) { return false; }
		if (empty($this->authKey)) { return false; }
		if (empty($this->convertUrl)) { return false; }
		if (empty($this->domain)) { return false; }
		
		if (!is_array($params) || empty($params)) { return false; }
		if (!isset($params['domain']) || empty($params['domain'])) { return false; }
		if (!isset($params['id_object']) || (int)$params['id_object'] <= 0) { return false; }
		if (!isset($params['bucket']) || empty($params['bucket'])) { return false; }
		if (!isset($params['file']) || empty($params['file'])) { return false; }
		
		$call_url = $this->convertUrl.'/authoring/?r=webservice/convert';
		
		/* curl will accept an array here too.
		 * Many examples I found showed a url-encoded string instead.
		 * Take note that the 'key' in the array will be the key that shows up in the
		 * $_FILES array of the accept script. and the at sign '@' is required before the
		 * file name.
		*/		
		
		// @file is deprecated, depending on PHP version
		if (version_compare(phpversion(), '5.5.0', '>=')) {
			$fileParam = new CURLFile($params['file']);
		}
		else {
			$fileParam = '@'.$params['file'];
		}
		
		$call_params = array(
			'auth_key' => sha1($this->authKey),
			'id_user' => (int)$params['id_user'],//Yii::app()->user->id,
			'domain' => $this->domain,
			'id_object' => (int)$params['id_object'],
			'bucket' => $params['bucket'],
			'file_to_convert' => $fileParam
		);
		
		//TO DO : check for safe mode
		set_time_limit(1000);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $call_url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 1000);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $call_params);
		$jsonResult = curl_exec($ch);

		$curlError = curl_error($ch);

		if($curlError){
			Yii::log('CURL error while connecting to authoring service: '.$curlError);
		}

		curl_close($ch);

		if ($jsonResult) { $result = CJSON::decode($jsonResult, true); }
		
		return $result;
	}
	
	
}