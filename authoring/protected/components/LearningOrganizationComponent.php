<?php
/**
 * Component for inserting info on Organization tables for authoring LOs
 */
class LearningOrganizationComponent extends CApplicationComponent
{
	
	protected $idCourse = NULL;
	
	
	public function setIdCourse($idCourse) {
		if ((int)$idCourse > 0) {
			$this->idCourse = (int)$idCourse;
		}
	}
	
	public function getIdCourse() {
		if (isset($_SESSION['idCourse']) && $_SESSION['idCourse'] > 0) { return (int)$_SESSION['idCourse']; }
		return $this->idCourse;
	}
	
	
	public function addLearningObject($object_type, $id_resource, $title, $id_parent, $lo_params = array()) {

		$id_course = $this->getIdCourse();
		
		// Create an entry in the learning_organization and related tables
		
		if (!empty($id_parent)) {
			$parentNode = LearningOrganization::model()->findByPk($id_parent);
		} else {
			$parentNode = LearningOrganization::model()->getRoot($id_course);
		}
		if (!$parentNode) { throw new CException('Invalid parent node'); }
		
		//insert new object
		$lmodel = new LearningOrganization();
		//$lmodel->idParent = (int) $id_parent;
		$lmodel->title = (string) $title;
		$lmodel->objectType = (string) $object_type;
		$lmodel->idResource = (int)$id_resource;
		$lmodel->idAuthor = (int) Yii::app()->user->id;
		$lmodel->dateInsert = Yii::app()->localtime->toLocalDateTime();
		$lmodel->idCourse = $id_course;
		/*if(!$lmodel->saveNode(false)) {
			return FALSE;
		}*/
		if (!$lmodel->appendTo($parentNode)) { return false; }

		$id_organization = $lmodel->getPrimaryKey();

		if (!$id_organization) return false;

		return $id_organization;
	}
	
	
	
	
	public function setLoParam($id_param, $param_name, $param_value) {

		if( $id_param == NULL ) {

			$qry = "INSERT INTO learning_lo_param (
					idParam, param_name, param_value
				) VALUES (
					0, '".$param_name."', '".$param_value."'
				)";
			
			$rs = Yii::app()->db->createCommand($qry)
				//->bindValues(array(':param_name' => $param_name, ':param_value' => $param_value))
				->query();

			if($rs) {
				$id_param = Yii::app()->db->getLastInsertID();
				$qry = "UPDATE learning_lo_param SET idParam = ".(int)$id_param." WHERE id = ".(int)$id_param." ";
			} else {
				return false;
			}
		} else {
			$qry = "REPLACE learning_lo_param (
				idParam, param_name, param_value
			) VALUES (
				" . (int)$id_param . ", '" . $param_name . "', '" . $param_value . "'
			)";
		}
		
		$rs = Yii::app()->db->createCommand($qry)->query();
		if (!$rs) {
			return false;
		}
		return $id_param;
	}

}