<?php

class SiteController extends Controller
{

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}

	/**
	 * Basic action to process login attempt
	 * For testing only.
	 */
	public function actionLogin()
	{
		$this->actionIndex();
	}

	public function actionIndex()
	{
		$this->redirect('../');
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
        if (Yii::app()->user->id && !Yii::app()->user->getIsGuest()) {
			LearningTracksession::model()->updateAll(array('active' => 0), 'active = 1 AND idUser = '.Yii::app()->user->id);
		}

		$previouslyLoggedUser = Yii::app()->user->loadUserModel();
        Yii::app()->user->logout();

        // Trigger event for plugins post-logout processing. This call may never return.
        Yii::app()->event->raise("UserLogout", new DEvent($this, array('user' => $previouslyLoggedUser)));

		if(Settings::get('user_logout_redirect') == 'on' && Settings::get('user_logout_redirect_url')){
			$url = Settings::get('user_logout_redirect_url');
			if(strpos($url , 'http') === false)
				$url = 'http://' . $url;

			$this->redirect($url);
		}

		$this->redirect(Docebo::getRelativeUrl('.'));
	}

	public function actionColorschemeCss()
	{
		header("Content-type: text/css");
		echo CoreScheme::generateCss();
		Yii::app()->end();
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo CHtml::encode($error['message']);
			} else {
				$this->renderPartial('error', $error);
			}
		}
	}
	
		
}