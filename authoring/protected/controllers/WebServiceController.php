<?php

class WebServiceController extends Controller {


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array(
					'convert'
				),
				'users'=>array('@'),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}



	public function actionConvert() {
		
		try {
			
			$authKey = Yii::app()->request->getParam('auth_key' , '');
			$checker = sha1(Yii::app()->authoringWebService->authKey); 

			if ($authKey != $checker) { throw new CException("Invalid auth code."); }
			
			$idObject = Yii::app()->request->getParam('id_object', 0);
			if ((int)$idObject <= 0) { throw new CException("Invalid Authoring ID."); }

	//save_path: /m/y/mylms_docebosaas_com/authoring/
			$domain = Yii::app()->request->getParam('domain', ''); //usually is something like 'mylms_docebosaas_com'
			if (empty($domain)) { throw new CException("Invalid specified domain."); }

			$bucket = Yii::app()->request->getParam('bucket', ''); //(bucket name)
			if (empty($bucket)) { throw new CException("Invalid specified bucket."); }

			$idUser = Yii::app()->request->getParam('id_user', 0);
			
			$uploadDir = Yii::app()->params['pluploadTempPath'];
			$uploadFile = $uploadDir.basename($_FILES['file_to_convert']['name']);
			if (!move_uploaded_file($_FILES['file_to_convert']['tmp_name'], $uploadFile)) {
				throw new CException("Error uploading file.");
			}
			$fileToConvert = new CUploadedFile(
				basename($uploadFile), 
				$uploadFile, 
				CFileHelper::getMimeType($uploadFile), 
				filesize($uploadFile), 
				UPLOAD_ERR_OK
			);

			$bucketObj = Yii::app()->getComponent('fileStorage')->getBucket(Yii::app()->params['Authoring']['bucketName']);
			if (!$bucketObj) { throw new CException("Specified bucket does not exists."); }
			$bucketObj->setUrlName($bucket);
			
			$amodel = new LearningAuthoring();
			$amodel->primaryKey = $idObject;
			$amodel->setDomain($domain);
			$amodel->setUsingWebService(true);
			$rs = $amodel->saveFile($fileToConvert);

			if (!$rs) { throw new CException("Error while converting file."); }
			
			$output = array(
				'success' => true,
				'slides' => $amodel->getLastNumConvertedSlides()
			);

		} catch(CException $e) {
			$output = array(
				'success' => false,
				'message' => $e->getMessage()
			);
		}
		$this->renderPartial('convert', array('info' => $output));
	}
	

}