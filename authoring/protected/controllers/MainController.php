<?php

class MainController extends Controller
{

	protected $courseModel = NULL;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array(
					'create',
					'converting',
					'convert',
					'copy',
					'edit',
					'additionalInfo',
					'publish',
					'uploadFile',
					'updateConversion',
					'axCheckStatus',
					//'complete',
					'createBookmark',
					'play',
				),
				'users'=>array('@'),
			),
			array('allow',
					'actions'=>array(
							'updateConversion',
					),
					'users'=>array('*'),
			),
			array('deny',
				'users'=>array('*'),
			),

		);
	}

	/**
	 * Returns list of allowed actions.
	 * @return array actions configuration.
	 */
	public function actions() {
		return array(
			'uploadFile' => 'ext.local.plupload.ActionUploadFile',
		);
	}


	/**
	 * Initialization of the controller
	 */
	public function init() {
		$idCourse = Yii::app()->request->getParam('id_course', false);
		$opt = Yii::app()->request->getParam('opt', false);

		if ($idCourse !== false && (($opt === false || strlen($opt) == 0) && $opt !== 'centralrepo')) {
			$courseModel = LearningCourse::model()->findByPk($idCourse);
			if (empty($courseModel)) { throw new CException('Invalid specified course'); }
			$this->courseModel = $courseModel;
		}

		parent::init();
	}

	/**
	 * Insert automatically id_course parameters in urls by default, if a course is present in the controller
	 * @param type $route
	 * @param type $params
	 * @param type $ampersand
	 * @return type
	 */
	public function createUrl($route, $params = array(), $ampersand = '&') {
		if (!empty($this->courseModel) && !isset($params['id_course'])) {
			$params['id_course'] = $this->courseModel->getPrimaryKey();
		}
		return parent::createUrl($route, $params, $ampersand);
	}


	protected function initBreadcrumbs() {
		if ($this->courseModel) {
			$this->breadcrumbs[] = Yii::t('menu_over', '_MYCOURSES');
			$this->breadcrumbs[$this->courseModel->name] = Docebo::createAppUrl('lms:player/training/index', array(
				'course_id' => $this->courseModel->getPrimaryKey()
			));
		}
		$this->breadcrumbs[] = Yii::t('standard', 'Training materials');
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionPlay($id)
	{


		//Owl Carousel js
		Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/owl/owl.carousel.min.js');
		//Yii::app()->clientScript->registerCssFile($this->getAssetsUrl() . '/css/owl/owl.carousel.css');
        Yii::app()->clientScript->registerScriptFile('js/hammer.min.js');
		Yii::app()->clientScript->registerScriptFile('js/owlCarousel.js');
		Yii::app()->clientScript->registerCssFile('css/owl.carousel.css');

		$playView = "play";

		// If we get 'stripped' parameter from the request, use the ... stripped layout
		// (no menu, no header/footer/crumbs whatsoever. This is used by Player to
		// show the Authoring LO inside lightbox and inline
		$stripped = Yii::app()->request->getParam('stripped', false);
		if ($stripped) {
			$this->layout = "//layouts/authoring_stripped";
			$playView = "play_stripped";
		} else {
			$this->initBreadcrumbs();
		}

		$stdBackUrl = $this->courseModel
			? Docebo::createAppUrl('lms:player/training/index', array(
					'course_id' => $this->courseModel->getPrimaryKey(),
					'mode' => 'view_course'
				))
			: Docebo::createAppUrl('lms:site/index');

		if(isset($_SESSION['resource_type'])){
			unset($_SESSION['resource_type']);
		}

		$model = $this->loadModel($id);

		switch ($model->status_id)
		{
			case LearningAuthoringStatus::PENDING:
				$params = array('edit', 'id' => $id);
				if ($this->courseModel) { $params['id_course'] = $this->courseModel->getPrimaryKey(); }
				$this->redirect($params);
				break;
			/*
			case LearningAuthoringStatus::CONFIRMED:
				$this->redirect(array('publish', 'id' => $id));
				break;
			//*/
			default:
				$model->track(Yii::app()->user->id, $this->courseModel ? $this->courseModel->getPrimaryKey() : null);

				// If the presentation only has 1 slide, mark it as completed on open
				if($model->slide_count==1){
					$learningConversion = LearningConversion::model()->findByAttributes(array(
						'authoring_id' => $id,
						'user_id' => Yii::app()->user->id,
					));
					if($learningConversion){
						$learningConversion->idCourse = $this->courseModel ? $this->courseModel->getPrimaryKey() : null;
						$learningConversion->completion_status = LearningConversion::STATUS_COMPLETED;
						$learningConversion->save();
					}
				}

				// If this Authoring resourse is a copy of another one, lets use the source JPG urls
				$tempId = $model->authoring_id;
				if ($model->copy_of > 0) {
					$model->authoring_id = $model->copy_of;
				}

				// Collect JPG URLs
				$jpgUrls = $model->getJpgUrls('full');

				// Restore model id. This is important, as otherwise TRACKING will be broken
				$model->authoring_id = $tempId;


				$this->render($playView,array(
					'jpgUrls' => $jpgUrls,
					'model' => $model,
					'backUrl' => Yii::app()->request->getParam('back_url', $stdBackUrl),
					'fullscreen' => Yii::app()->request->getParam('fullscreen', false),
					'courseModel' => $this->courseModel
				));
				break;
		}
	}

	/**
	 * Renders create page
	 *
	 * @param integer $id Authoring id
	 */
	public function actionCreate($id = null)
	{
		$opt = Yii::app()->request->getParam('opt', false);

		$error = Yii::app()->request->getParam('error', 0);
		$id_parent = Yii::app()->request->getParam('id_parent', null);
		$editSlideFile = Yii::app()->request->getParam('editSlideFile', null);
		$idObject = Yii::app()->request->getParam('idObject', false);
		$model = LearningAuthoring::model()->findByPk($id);


		if($editSlideFile && $model && $model->status_id == LearningAuthoringStatus::PENDING){
			$params = array('edit', 'id' => $id);
			if ($this->courseModel && !$opt){
				$params['id_course'] = $this->courseModel->getPrimaryKey();
			}else{
				$params['opt'] = $opt;
			}
			$this->redirect($params);
		}
		$errorMessage = "";
		if ($error != 0) {
			if (Yii::app()->user->hasFlash('authoring-conversion-error') ) {
				$errorMessage = Yii::app()->user->getFlash('authoring-conversion-error');
			} else {
				$errorMessage = Yii::t('authoring', 'Unable to perform file conversion');
			}
		}
		$this->initBreadcrumbs();
		$this->render('create', array(
			'idObject' => $idObject,
			'editSlideFile' => $editSlideFile,
			'id' => $id,
			'errorMessage' => $errorMessage,
			'courseModel' => $this->courseModel,
			'opt' => $opt,
			'id_parent' => $id_parent
		));
	}

	/**
	 * Makes a copy of an existing object from a course inside the same course
	 * @param int $id - Old LO object ID
	 */
	public function actionCopy($id = null){
		$oldModel = $this->loadModel($id);

		// Copy the old model into a new one
		$newModel = new LearningAuthoring();
		$newModel->attributes = $oldModel->attributes;
		unset($newModel->authoring_id);
		$newModel->isNewRecord = true;
		$newModel->scenario = 'publish';
		$newModel->title = "Copy of " . $newModel->title;
		$newModel->publish();

		// Copy the remote slides (jpg files) of the old authoring to the new one
		$oldModel->copyRemoteAuthoring($newModel);

		// Redirect back to the LO list
		$backUrl = $this->courseModel
			? Docebo::createAppUrl('lms:player/training/index', array('course_id' => $this->courseModel->getPrimaryKey()))
			: Docebo::createAppUrl('lms:site/index');
		$this->redirect($backUrl);
	}

	/**
	 * Displays converting page
	 *
	 * @param integer $id Authoring id
	 */
	public function actionConverting($id = null)
	{
		
		$opt = Yii::app()->request->getParam('opt', false);

		if (isset($_POST['uploader_0_tmpname']) && (isset($_GET['idAuthoring']) || isset($_GET['idObject'])))
		{
			if(isset($_GET['idAuthoring'])){
				$model = LearningAuthoring::model()->findByPk($_GET['idAuthoring']);
				if( $opt !== false && $opt === 'centralrepo'){
					$model->organizationSync = false;
				}
				if($model){
					$model->applyStatusPending();
				}
			}

			$this->initBreadcrumbs();

			$orgSync = true;
			$idAuthoring = (int) $_GET['idAuthoring'];
			if($opt !== false && $opt === 'centralrepo'){
				$orgSync = false;
 			}

			//CloudConvert
			$cloudConvertObject = new CloudConvert(CFileStorage::COLLECTION_LO_AUTHORING);
			$cloudConvertObject->slidesConvertFromS3ToS3($idAuthoring, 'jpg', $orgSync);

			$id_course = ($this->courseModel)? $this->courseModel->getPrimaryKey() : false;
			$this->render('converting', array(
				'idAuthoring' => $idAuthoring,
					'id_course'		=> $id_course,
				'opt' => $opt
//				'fileName' => $_POST['uploader_0_tmpname'],
//				'id' => $id,
//				'courseModel' => $this->courseModel
			));
		}
		else if( isset($_GET['pending']) && $_GET['pending']){
			$id_course = ($this->courseModel)? $this->courseModel->getPrimaryKey() : false;
			$this->render('converting', array(
					'idAuthoring' => (int)$_GET['idAuthoring'],
					'id_course'		=> $id_course,
					'opt' => $opt
//					'fileName' => $_POST['uploader_0_tmpname'],
//					'id' => $id,
//					'courseModel' => $this->courseModel
			));
		}
		else
		{
			$urlParams = ($this->courseModel ? array('id_course' => $this->courseModel->getPrimaryKey()) : array());
			if($opt !== false && $opt === 'centralrepo'){
				$urlParams['opt'] = $opt;
			}
			$this->redirect(Yii::app()->createUrl('main/create', $urlParams));
		}
	}

	public function actionAxCheckStatus(){
		$idAuthoring = Yii::app()->request->getParam('idAuthoring', false);
		$response = new AjaxResult();
		$data = array('isConfirmed' => false);
		$authoringModel = LearningAuthoring::model()->findByPk($idAuthoring);
		if($authoringModel && ($authoringModel->status_id == LearningAuthoringStatus::CONFIRMED || $authoringModel->status_id == LearningAuthoringStatus::PUBLISHED)){
			$data['isConfirmed'] = true;
		}
		$response->setStatus(true)->setData($data)->toJSON();
		Yii::app()->end();
	}


	public function actionUpdateConversion(){
		if (!empty($_REQUEST['step']) && !empty($_REQUEST['id'])) {
			$step = $_REQUEST['step'];
			$idPipe = $_REQUEST['id'];
			$authoringObject = LearningAuthoring::model()->findByAttributes(array('pipeline_id' => $idPipe));//LearningAuthoring::model()->findByPk(32);//
			if ($authoringObject) {

					//Get detailed info about the conversion
					$options = array(
							CURLOPT_RETURNTRANSFER => true,   // return web page
							CURLOPT_HEADER         => false,  // don't return headers
							CURLOPT_FOLLOWLOCATION => true,   // follow redirects
							CURLOPT_MAXREDIRS      => 1,     // stop after 10 redirects
//								CURLOPT_ENCODING       => "",     // handle compressed
//								CURLOPT_USERAGENT      => "test", // name of client
							CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
							CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
							CURLOPT_TIMEOUT        => 120,    // time-out on response
					);
					if ($step == 'finished') {
						$authoringObject->status_id = LearningAuthoringStatus::CONFIRMED;
						$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_AUTHORING);

						$curlCall = curl_init("http:".$_REQUEST['url']);
						curl_setopt_array($curlCall, $options);
						$detailedResponse  = curl_exec($curlCall);
						curl_close($curlCall);

						$detailedResponse = CJSON::decode($detailedResponse);
						//the response is different for 1 and more than 1 files
						if(isset($detailedResponse['output']['files']))
							$files = $detailedResponse['output']['files'];
						else
							$files = array($detailedResponse['output']['filename']);

						$slideCountOld = $authoringObject->slide_count;
						$authoringObject->slide_count = count($files);
						if($slideCountOld >= 1 ){
							if($authoringObject->slider_offset>=1){
								$authoringObject->slider_offset = ($authoringObject->slider_offset+$slideCountOld)+1;
							}else{
								$authoringObject->slider_offset = $slideCountOld+1;
							}
						}

						if($authoringObject->slider_offset >= 1){
							$i = $authoringObject->slider_offset;
						}else{
							$i = 1;
						}
						//Rename the converted files according to old convention
						foreach($files as $filename){
							$storage->rename((string)($authoringObject->authoring_id)."/".$filename, (string)($authoringObject->authoring_id)."/".$authoringObject->authoring_id.'_full_slide'.$i.'_1.jpg');
							$i++;
						}
					}
					else if($step == 'error'){
						$curlCall = curl_init("http:".$_REQUEST['url']);
						curl_setopt_array($curlCall, $options);
						$detailedResponse  = curl_exec($curlCall);
						curl_close($curlCall);

						$detailedResponse = CJSON::decode($detailedResponse);
						$message = isset($detailedResponse['message'])? $detailedResponse['message'] : '';
						$code = isset($detailedResponse['code'])? $detailedResponse['code'] : '';
						Yii::log("Authoring conversion error: $message, code: $code, pipeline_id = $idPipe", CLogger::LEVEL_ERROR);

						$authoringObject->status_id = LearningAuthoringStatus::FAILED;
					}
					else {
						$authoringObject->status_id = LearningAuthoringStatus::PENDING;
					}

					if(isset($_REQUEST['opt']) && $_REQUEST['opt'] === 'centralrepo'){
						$authoringObject->organizationSync = false;
					}

					$authoringObject->save();
			}
		}
	}


	/**
	 * Creates new LearningAuthoring model or load existing and attaches presentation file to it
	 */
	public function actionConvert()
	{
		$model = null;
		if (isset($_POST['id']))
		{
			$id = $_POST['id'];
			$model = LearningAuthoring::model()->findByPk($id);
		}
		if ($model == null || $model->status_id == LearningAuthoringStatus::PUBLISHED)
		{
			$model = new LearningAuthoring();
			$model->title = Yii::t('standard', '_TITLE');
			$model->parentFolder = Yii::app()->request->getParam('id_parent', false);
		}

		$trans = Yii::app()->db->beginTransaction();
		try {
			$model->file = Yii::app()->params['pluploadTempPath'] . DIRECTORY_SEPARATOR . $_POST['fileName'];

			$rs = $model->save();

			//echo $model->authoring_id;
			if ($rs) {
				$trans->commit();
				$output = array(
					'success' => true,
					'id' => $model->authoring_id
				);
			} else {
				$trans->rollback();
				$output = array(
					'success' => false,
					'message' => Yii::t('authoring', 'Error while converting file')//$model->getErrors()
				);
				Yii::app()->user->setFlash('authoring-conversion-error', $message);
			}
		} catch (CException $e) {
			$trans->rollback();
			$message = $e->getMessage();
			$output = array(
				'success' => false,
				'message' => $message
			);
			Yii::app()->user->setFlash('authoring-conversion-error', $message);
		}

		echo CJSON::encode($output);
	}

	/**
	 * Edit page
	 *
	 * @param integer $id Authoring id
	 */
	public function actionEdit($id)
	{
		$model = null;
		$model=$this->loadModel($id);
		$model->scenario = 'edit';

		$opt = Yii::app()->request->getParam('opt', false);

		if($opt !== false && $opt === 'centralrepo'){
			$model->organizationSync = false;
		}

		if($model->status_id != LearningAuthoringStatus::CONFIRMED && $model->status_id != LearningAuthoringStatus::PUBLISHED){
			$params = array('converting', 'pending' => true, 'idAuthoring' => $model->authoring_id);
			if ($this->courseModel) { $params['id_course'] = $this->courseModel->getPrimaryKey(); }
			if($opt !== false && $opt === 'centralrepo'){
				$params['opt'] = $opt;
			}
			$this->redirect($params);
		}

		if (isset($_POST['continue']) || isset($_GET['continue']))
		{
			if (isset($_POST['LearningAuthoring']))
			{
				$model->attributes = $_POST['LearningAuthoring'];
				if ($model->confirm())
					$params = array('additionalInfo', 'id' => $model->authoring_id);
					if ($this->courseModel) { $params['id_course'] = $this->courseModel->getPrimaryKey(); }
					if($opt !== false && $opt === 'centralrepo'){
						$params['opt'] = $opt;
					}
					$this->redirect($params);
			}
		}

		if (isset($_POST['save']))
		{
			if (isset($_POST['LearningAuthoring']))
			{
				$model->attributes = $_POST['LearningAuthoring'];
				if ($model->confirm()) {
					$params = array('play', 'id' => $model->authoring_id);
					if ($this->courseModel) { $params['id_course'] = $this->courseModel->getPrimaryKey(); }
					if($opt !== false && $opt === 'centralrepo'){
						$params['opt'] = $opt;
					}
					$this->redirect($params);
				}
			}
		}

		if (isset($_POST['edit']))
		{
			if ($model->status_id != LearningAuthoringStatus::PENDING)
			{
				$model->applyStatusPending();
			}
			$params = array('create', 'id' => $model->authoring_id);
			if ($this->courseModel) { $params['id_course'] = $this->courseModel->getPrimaryKey(); }
			if($opt !== false && $opt === 'centralrepo'){
				$params['opt'] = $opt;
			}
			$this->redirect($params);
		}

		$this->initBreadcrumbs();
		$this->render('edit', array(
			'model' => $model,
			'courseModel' => $this->courseModel,
		));
	}

	public function actionAdditionalInfo($id){
		$model=$this->loadModel($id);
		$model->scenario = 'edit';

		$cs = Yii::app()->getClientScript();

		$adminJs = Yii::app()->assetManager->publish(Yii::getPathOfAlias('admin.js'));

		$opt = Yii::app()->request->getParam('opt', false);

		// We need some helper methods located in these files in the
		// Player List View layout LO add/edit UI: Additional info tab, custom thumbnail uploading logic
		$cs->registerScriptFile($adminJs.'/scriptTur.js');
		$cs->registerScriptFile($adminJs.'/scriptModal.js');
		$cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/Jcrop/jquery.Jcrop.min.js');
		$cs->registerCssFile(Yii::app()->theme->baseUrl.'/js/Jcrop/jquery.Jcrop.min.css');

		if (isset($_POST['continue']))
		{
			$short_description = Yii::app()->getRequest()->getParam('short_description');
			$thumb = Yii::app()->getRequest()->getParam('thumb');

			if($short_description || $thumb) {
				if($opt == false || $opt !== 'centralrepo'){
					$model->organization->short_description = Yii::app()->htmlpurifier->purify($short_description);
					if($thumb){
						$model->organization->resource          = intval($thumb);
					}elseif(!$thumb && $model->organization->resource){
						$model->organization->resource          = 0;
					}
					$model->organization->saveNode();
				} else {
					$versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
						'id_resource' => $id,
						'object_type' => LearningOrganization::OBJECT_TYPE_AUTHORING
					));

					if($versionModel){
						$repoObject = LearningRepositoryObject::model()->findByPk($versionModel->id_object);

						if($repoObject){
							$repoObject->short_description = Yii::app()->htmlpurifier->purify($short_description);
							if($thumb){
								$repoObject->resource = intval($thumb);
							}elseif($repoObject->resource){
								$repoObject->resource = 0;
							}

							$repoObject->save();
						}
					}
				}

				$params = array('publish', 'id' => $model->authoring_id);
				if ($this->courseModel) { $params['id_course'] = $this->courseModel->getPrimaryKey(); }
				if($opt !== false && $opt === 'centralrepo'){
					$params['opt'] = $opt;
				}
				$this->redirect($params);
			}

			/*
			if (isset($_POST['LearningAuthoring']))
			{
				$model->attributes = $_POST['LearningAuthoring'];
				if ($model->confirm())
					$params = array('additionalInfo', 'id' => $model->authoring_id);
				if ($this->courseModel) { $params['id_course'] = $this->courseModel->getPrimaryKey(); }
				$this->redirect($params);
			}*/
		}

		$loModel = $model->organization;

		if($opt !== false && $opt === 'centralrepo'){
			$versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
				'id_resource' => $model->authoring_id,
				'object_type' => LearningOrganization::OBJECT_TYPE_AUTHORING
			));

			if($versionModel){
				$repoObject = LearningRepositoryObject::model()->findByPk($versionModel->id_object);

				if($repoObject){
					$loModel = $repoObject;
				}
			}
		}

		$this->initBreadcrumbs();
		$this->render('additionalInfo', array(
			'model' => $model,
			'courseModel' => $this->courseModel,
			'loModel' => $loModel
		));
	}

	/**
	 * Publish page
	 *
	 * @param integer $id Authoring id
	 */
	public function actionPublish($id)
	{
		Yii::app()->clientScript->registerScriptFile('js/core.js', CClientScript::POS_HEAD);
		Yii::app()->clientScript->registerScriptFile('js/common.js', CClientScript::POS_HEAD);

		//Owl Carousel js
		Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/owl/owl.carousel.min.js');
		//Yii::app()->clientScript->registerCssFile($this->getAssetsUrl() . '/css/owl/owl.carousel.css');
        Yii::app()->clientScript->registerScriptFile('js/hammer.min.js');
		Yii::app()->clientScript->registerScriptFile('js/owlCarousel.js');
		Yii::app()->clientScript->registerCssFile('css/owl.carousel.css');

		$opt = Yii::app()->request->getParam('opt', false);

		$model=$this->loadModel($id);
		$model->scenario = 'publish';
		$centralRepoContext = false;

		if($opt !== false && $opt === 'centralrepo'){
			$model->organizationSync = false;
			$centralRepoContext = true;
		}

		if ($model->status_id == LearningAuthoringStatus::CONFIRMED)
		{
			if (isset($_POST['publish']))
			{
				if (isset($_POST['LearningAuthoring']))
				{
					$model->attributes = $_POST['LearningAuthoring'];

					if ($model->publish()){
						if($centralRepoContext){
							$versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
								'id_resource' => $model->authoring_id,
								'object_type' => LearningOrganization::OBJECT_TYPE_AUTHORING
							));

							if($versionModel){
								$repoObject = LearningRepositoryObject::model()->findByPk($versionModel->id_object);

								if($repoObject){
									$repoObject->title = $model->title;

									$repoObject->save();
								}
							}
						}
						$params = array('play', 'id' => $model->authoring_id);
					}
					if ($this->courseModel) { $params['id_course'] = $this->courseModel->getPrimaryKey(); }
					$this->redirect($params);
				}
			}

			if (isset($_POST['save']))
			{
				if (isset($_POST['LearningAuthoring']))
				{
					$model->attributes = $_POST['LearningAuthoring'];
					if ($model->save()){
						if($centralRepoContext){
							$versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
								'id_resource' => $model->authoring_id,
								'object_type' => LearningOrganization::OBJECT_TYPE_AUTHORING
							));

							if($versionModel){
								$repoObject = LearningRepositoryObject::model()->findByPk($versionModel->id_object);

								if($repoObject){
									$repoObject->title = $model->title;

									$repoObject->save();
								}
							}
						}
						$params = array('play', 'id' => $model->authoring_id);
						if ($this->courseModel) { $params['id_course'] = $this->courseModel->getPrimaryKey(); }
						$this->redirect($params);
					}
				}
			}

			if (isset($_POST['edit']))
			{
				$params = array('edit', 'id' => $model->authoring_id);
				if ($this->courseModel) { $params['id_course'] = $this->courseModel->getPrimaryKey(); }
				$this->redirect($params);
			}

			$this->initBreadcrumbs();
			$this->render('publish', array(
				'model' => $model,
				'courseModel' => $this->courseModel
			));
		}
		else
		{
			if ($model->status_id == LearningAuthoringStatus::PENDING)
			{
				$params = array('edit', 'id' => $id);
				if ($this->courseModel) { $params['id_course'] = $this->courseModel->getPrimaryKey(); }
				$this->redirect($params);
			}
			else
			{
				$params = array('play', 'id' => $id);
				if ($this->courseModel) { $params['id_course'] = $this->courseModel->getPrimaryKey(); }
				$this->redirect($params);
			}
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return LearningAuthoring the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=LearningAuthoring::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param LearningAuthoring $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='learning-authoring-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/**
	 * Sets status completed for authoring with id = $_POST['id'] and current user
	 */
	/*
	public function actionComplete()
	{
		if (isset($_POST['id']))
		{
			$model = $this->loadModel($_POST['id']);
			if ($model)
			{
				$model->complete(Yii::app()->user->id);
			}
		}
	}
	 */

	/**
	 * Creates bookmark for current user and learning authoring with id = $_POST['id']
	 */
	public function actionCreateBookmark()
	{
		if (isset($_POST['id']) && isset($_POST['suspendData']))
		{
			$model = $this->loadModel($_POST['id']);
			if ($model)
			{
				$model->createBookmark($_POST['suspendData'], Yii::app()->user->id, $this->courseModel ? $this->courseModel->getPrimaryKey() : null);
			}
		}
	}
}
