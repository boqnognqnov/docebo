<?php
$this->breadcrumbs[] = $model->title;
$this->breadcrumbs[] = Yii::t('standard', '_PUBLISH');
?>

<div class="authoring_wrap <?php echo Yii::app()->getLanguage(); ?>">
 <div class="steps">
  <ul>
   <li class="passed"><div class="step_wrap"><?php echo Yii::t('standard', '_UPLOAD'); ?><div class="step">1<em>&nbsp;</em></div></div></li>
   <li class="passed"><div class="step_wrap"><?php echo Yii::t('authoring', 'CONVERT'); ?><div class="step">2<em>&nbsp;</em></div></div></li>
   <li class="passed"><div class="step_wrap"><?php echo Yii::t('standard', '_PREVIEW'); ?><div class="step">3<em>&nbsp;</em></div></div></li>
	<li class="passed"><div class="step_wrap"><?=Yii::t('myactivities', 'Additional info')?><div class="step">4<em>&nbsp;</em></div></div></li>
   <li class="passed"><div class="step_wrap"><?php echo Yii::t('standard', '_PUBLISH'); ?><div class="step">5<em>&nbsp;</em></div></div></li>
  </ul>
  <div class="fixer">&nbsp;</div>
  <div class="progress">
   <div class="bar bar-success" style="width: 100%;"></div>
  </div>
 </div>

 <h1 class="text-center"><?php echo $model->title; ?></h1>
 <p class="lead text-center"><?php echo Yii::t('authoring', 'WOW_IT_LOOKS_AWERSOME'); ?></p>
 <?php
 /**
  * @var $model LearningAuthoring
  */
 ?>
 <?php $jpgUrls = $model->getJpgUrls('full');

 $classes = array();
 if (!$model->slide_number)
 {
	 $classes[] = 'pager_hidden';
 }
 if (!$model->playbar)
 {
	 $classes[] = 'bar_hidden';
 }
 if (!$model->fullscreen)
 {
	 $classes[] = 'fullscreen_hidden';
 }
 $classesString = implode(' ', $classes);

 $suspendData = CJSON::decode($model->getSuspendData());
 $isRTL = Lang::isRTL(Yii::app()->getLanguage());
 ?>

 <div id="authoring-owl-wrapper" class="<?=$classesString?>">
  <div class="move-left-slide">
   <div class="move-left-box">
    <i class="fa fa-chevron-<?=$isRTL? 'right' : 'left'?>"></i>
   </div>
  </div>
  <div id="authoring-owl-player" class="owl-carousel">
   <?php
   foreach ($jpgUrls as $jpgUrl): ?>
    <div class="item"><img src="<?= $jpgUrl?>" alt="The Last of us"></div>
   <?php
   endforeach;
   ?>
  </div>
  <?php
  $startSlide = !empty($suspendData) ? $suspendData['slideNumber'] : 1;
  $perc = (($startSlide)/count($jpgUrls))*100;
  ?>
  <div class="custom-owl-progress-bar"><div class="progress-bar" style="width: <?=$perc.'%'?>"></div> </div>
  <div class="custom-owl-nav">
   <a href="javascript:void(0)" class="owl-prev-custom fa fa-chevron-<?=$isRTL? 'right' : 'left'?>"></a>
   <ul id="info" class="info">
    <li class="currp"><?= $startSlide; ?></li>
    <li class="allp">/ <strong><?php echo count($jpgUrls); ?></strong> <?= Yii::t('standard','_PAGES'); ?></li>
   </ul>
   <div class="expand fa fa-expand"></div>
   <div class="compress fa fa-compress"></div>
   <a href="javascript:void(0)" class="owl-next-custom fa fa-chevron-<?=$isRTL? 'left' : 'right'?>"></a>
  </div>
  <div class="move-right-slide">
   <div class="move-right-box pull-right"><i class="fa fa-chevron-<?=$isRTL? 'left' : 'right'?>"></i> </div>
  </div>
 </div>

 <?php echo $this->renderPartial('_publishForm', array('model'=>$model)); ?>

</div>

<script type="text/javascript">
 $(function() {

  var options = {
   element: '#authoring-owl-player',
   navElement: '.custom-owl-nav',
   wrapper: '#authoring-owl-wrapper',
   debug: false,
   modelId: '<?= $model->authoring_id?>',
   suspendData: '<?=$model->getSuspendData()?>',
   presentationParams: {
    backgroundColor: '<?=$model->background_color?>',
    textColor: '<?=$model->text_color?>',
    hoverColor: '<?=$model->hover_color?>'
   },
   totalItems: <?=count($jpgUrls)?>,
	rtl:<?=json_encode($isRTL)?>
  }; 

  owlPlayer = new OwlPlayer(options);  
 });
</script>