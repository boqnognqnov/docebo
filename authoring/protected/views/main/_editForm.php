<?php
/* @var $this LearningAuthoringController */
/* @var $model LearningAuthoring */
/* @var $form CActiveForm */
?>

<h2><span id="editable"><?php echo $model->title ?></span>&nbsp;<a href="#" class="edit icon-edit">&nbsp;</a></h2>

<div class="form">

<?php $form=$this->beginWidget('QsActiveForm', array(
	'id'=>'learning-authoring-form',
	'enableAjaxValidation'=>false,
)); ?>

<?php echo $form->errorSummary($model); ?>

	<div class="row editable">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

<?php
  $jpgUrls = $model->getJpgUrls('full');
?>

	<div class="thumbs">
	 <div class="in">
       <ul>
	   <?php foreach ($jpgUrls as $jpgUrl) {echo "<li>".CHtml::image($jpgUrl)."</li>";} ?>
	  </ul>
	 </div>
	</div>

    <div class="preview">
     <ul>
      <?php foreach ($jpgUrls as $jpgUrl) {echo "<li>".CHtml::image($jpgUrl)."</li>";} ?>
	 </ul>
	</div>

	</div>
	<br/>
	<div class="row buttons">
	 <?php echo CHtml::submitButton(Yii::t('standard', '_CONTINUE'), array('name' => 'continue', "class" => "btn-docebo green big fr")); ?>
	</div>

<?php $this->endWidget(); ?>

</div>
<!-- form -->

<script>
 $(function(){
  $("h2 .edit").click(function(){
   $(".editable").toggle();
   return false;
  });

  $(".editable input").keyup(function(){
   $("#editable").html($(this).val());
  });

  $(".editable input").keypress(function(event){
   if (event.which == 13) {
	 $(".editable").toggle();
     return false;
   }
  });
 });
</script>