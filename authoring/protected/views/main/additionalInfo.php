<?php
/* @var $this LearningAuthoringController */
/* @var $model LearningAuthoring */
/* @var $form CActiveForm */

$this->breadcrumbs[] = $model->title;
$this->breadcrumbs[] = Yii::t('standard', '_MOD');
?>

<?php Yii::app()->clientScript->registerScriptFile('js/core.js', CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScriptFile('js/common.js', CClientScript::POS_HEAD); ?>

<div class="authoring_wrap pr <?php echo Yii::app()->getLanguage(); ?>">

	<div class="steps">
		<ul>
			<li class="passed"><div class="step_wrap"><?php echo Yii::t('standard', '_UPLOAD'); ?><div class="step">1<em>&nbsp;</em></div></div></li>
			<li class="passed"><div class="step_wrap"><?php echo Yii::t('authoring', 'CONVERT'); ?><div class="step">2<em>&nbsp;</em></div></div></li>
			<li class="passed"><div class="step_wrap"><?php echo Yii::t('standard', '_PREVIEW'); ?><div class="step">3<em>&nbsp;</em></div></div></li>
			<li class="passed"><div class="step_wrap"><?=Yii::t('myactivities', 'Additional info')?><div class="step">4<em>&nbsp;</em></div></div></li>
			<li><div class="step_wrap"><?php echo Yii::t('standard', '_PUBLISH'); ?><div class="step">5<em>&nbsp;</em></div></div></li>
		</ul>
		<div class="fixer">&nbsp;</div>
		<div class="progress">
			<div class="bar bar-success" style="width: 75%;"></div>
		</div>
	</div>

	<h2><span id="editable"><?php echo $model->title ?></span></h2>

	<div class="form">

		<?php $form=$this->beginWidget('QsActiveForm', array(
			'id'=>'player-arena-uploader',
			'enableAjaxValidation'=>false,
			'htmlOptions'=>array('class'=>'form-horizontal')
		)); ?>

		<div id="lo-additional-information">
			<? $this->widget('common.widgets.PlayerLayoutAdditionalInfo', array(
				'loModel'=> $loModel,
				'idCourse' => $model->organization->idCourse,
			)); ?>
		</div>
		<br/>
		<div class="row buttons">
			<?php echo CHtml::submitButton(Yii::t('standard', '_CONTINUE'), array('name' => 'continue', "class" => "btn-docebo green big fr")); ?>
		</div>

		<?php $this->endWidget(); ?>
	</div>

</div>
<!-- form -->

<script>
	$(function(){
		$('#lo-additional-information input, select').styler();
	});
</script>