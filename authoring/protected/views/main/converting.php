<?php
$this->breadcrumbs[] = Yii::t('authoring', 'Slides converter');
$this->breadcrumbs[] = Yii::t('authoring', 'CONVERSION_IN_PROGRESS');
?>

<div class="authoring_wrap <?php echo Yii::app()->getLanguage(); ?>">

	<div class="steps">
		<ul>
			<li class="passed">
				<div class="step_wrap"><?php echo Yii::t('storage', '_LONAME_item'); ?>
					<div class="step">1<em>&nbsp;</em></div>
				</div>
			</li>
			<li class="passed">
				<div class="step_wrap"><?php echo Yii::t('authoring', 'CONVERT'); ?>
					<div class="step">2<em>&nbsp;</em></div>
				</div>
			</li>
			<li>
				<div class="step_wrap"><?php echo Yii::t('standard', '_PREVIEW'); ?>
					<div class="step">3<em>&nbsp;</em></div>
				</div>
			</li>
			<li><div class="step_wrap"><?=Yii::t('myactivities', 'Additional info')?><div class="step">4<em>&nbsp;</em></div></div></li>
			<li>
				<div class="step_wrap"><?php echo Yii::t('standard', '_PUBLISH'); ?>
					<div class="step">5<em>&nbsp;</em></div>
				</div>
			</li>
		</ul>
		<div class="fixer">&nbsp;</div>
		<div class="progress">
			<div class="bar bar-success" style="width: 27%;"></div>
		</div>
	</div>

	<h1 class="text-center"><?php echo Yii::t('authoring', 'CONVERSION_IN_PROGRESS'); ?></h1>

	<p class="lead text-center"><?php echo Yii::t('authoring', 'POWER_POINT_IS_BECOMING'); ?></p>

	<div id="ajaxBusy">
		<p>&nbsp;</p>
		<p class="text-center"><img src="images/preloader.gif"></p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p class="lead text-center"><?php echo Yii::t('authoring', 'WAITING_CONVERSION'); ?></p>
	</div>

</div>

<?php
//we shuld always pass course_id parameter to urls
//$urlParams = (isset($courseModel) && $courseModel ? array('id_course' => $courseModel->getPrimaryKey()) : array());
?>
<script>
	function checkStatus(){
		$.ajax({
			dataType: 'json',
			url: "<?php echo Yii::app()->createUrl('main/axCheckStatus'); ?>",
			type: "POST",
			data: {
				idAuthoring : '<?= $idAuthoring ?>',
				opt: <?=json_encode($opt) ?>
			},
			success: function(data){
				if(!data.data.isConfirmed)
					setTimeout(function(){checkStatus();}, 10000);
				else if(data.data.isConfirmed){
					window.location.href = "<?php
					echo Yii::app()->createUrl('main/edit',array( 'id' => $idAuthoring, 'id_course' => $id_course, 'opt' => $opt)); ?>"
				}
			},
		});
	}

$(function() {
	checkStatus();
//	$.ajax({
//		url: "<?php //echo Yii::app()->createUrl('main/convert', $urlParams); ?>//",
//		type: "POST",
//		data: {
//			fileName: "<?php //echo $fileName ?>//",
//			id: "<?php //echo $id ?>//",
//			<?php //echo Yii::app()->request->csrfTokenName ?>//: "<?php //echo Yii::app()->request->csrfToken ?>//"
//		},
//		success: function(o) {
//			var t = $.parseJSON(o);
//			if (t.success) {
//				var id = t.id;
//				$(location).attr('href', '<?php //echo Yii::app()->createAbsoluteUrl('main/edit', $urlParams); ?>//&id=' + id);
//			} else {
//				$(location).attr('href', '<?php //echo Yii::app()->createAbsoluteUrl('main/create', array_merge($urlParams, array('error' => 1))); ?>//');
//			}
//		},
//		error: function() {
//			$(location).attr('href', '<?php //echo Yii::app()->createAbsoluteUrl('main/create', array_merge($urlParams, array('error' => -1))); ?>//');
//		}
//	});
});
</script>
