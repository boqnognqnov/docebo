<?php

//Docebo Course Menu

$course_id = (int) Yii::app()->request->getParam('id_course', $_SESSION['idCourse']);
$user_id = Yii::app()->user->id;

// Check for valid course id (int and not 0)
if ( $course_id <= 0 ) {
	throw new CHttpException(404, Yii::t('standard', '_OPERATION_FAILURE'));
}

$courseUserModel = LearningCourseuser::model()->findByAttributes(array("idUser" => $user_id, "idCourse" => $course_id));
$isSubscribed = !empty($courseUserModel);
if (!$isSubscribed) {
	throw new CHttpException(404, Yii::t('standard', 'You must be subscribed to this course.'));
}

$courseModel = LearningCourse::model()->findByPk($course_id);
$userModel = CoreUser::model()->findByPk($user_id);
$authManager = Yii::app()->authManager;

$this->widget('common.widgets.CourseMenu', array(
	"userModel" => $userModel,
	"courseModel" => $courseModel,
	"courseUserModel" => $courseUserModel,
	"authManager" => $authManager,
));

?>