<?php

$this->breadcrumbs[] = Yii::t('authoring', 'Slides converter'); ?>
<!-- Load Queue widget CSS and jQuery -->
<?php Yii::app()->clientScript->registerCssFile('plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css', CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScriptFile('js/browserplus.js', CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScriptFile('plupload/js/plupload.full.js', CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScriptFile('plupload/js/jquery.plupload.queue/jquery.plupload.queue.js', CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScript('translations', "plupload.addI18n({
	'Select files' : '".'Select files'."',
	'Add files to the upload queue and click the start button.' : '".'Add files to the upload queue and click the start button.'."',
	'Filename' : '".'_FILENAME'."',
	'Status' : '".'_STATUS'."',
	'Size' : '".'Size'."',
	'Add Files' : '".'Add Files'."',
	'Stop current upload' : '".'Stop current upload'."',
	'Start uploading queue' : '".'Start uploading queue'."',
	'Uploaded %d/%d files': '".'Uploaded %d/%d files'."',
	'N/A' : '".'N/A'."',
	'Drag files here.' : '".'Drag files here.'."',
	'File extension error.' : '".'_INVALID_EXTENSION'."',
	'File size error.' : '".'File size error.'."',
	'Init error.' : '".'Init error.'."',
	'HTTP Error.' : '".'HTTP Error.'."',
	'Security error.' : '".'Security error.'."',
	'Generic error.' : '".'Generic error.'."',
	'IO error.' : '".'IO error.'."',
	'Stop Upload' : '".'Stop Upload'."',
	'Start Upload' : '".'Start Upload'."',
	'%d files queued' : '".'%d files queued'."',
	'Drag &amp; Drop your Power Point Presentations in this area' : '".addslashes(Yii::t('authoring', 'Drag &amp; Drop your Power Point Presentations in this area'))."',
	'or' : '".Yii::t('authoring', 'or')."',
	'BROWSE FILES...' : '".addslashes(Yii::t('organization', 'Upload File'))."'
});", CClientScript::POS_HEAD); ?>

<?php
if ($errorMessage) {
	echo '<div class="alert alert-error">'
		.'<button type="button" class="close" data-dismiss="alert">&times;</button>'
		.$errorMessage.'</div>';
}
?>

<div class="authoring_wrap <?php echo Yii::app()->getlanguage(); ?>">

 <div class="steps">
  <ul>
   <li class="passed"><div class="step_wrap"><?php echo Yii::t('standard', '_UPLOAD'); ?><div class="step">1<em>&nbsp;</em></div></div></li>
   <li><div class="step_wrap"><?php echo Yii::t('authoring', 'CONVERT'); ?><div class="step">2<em>&nbsp;</em></div></div></li>
   <li><div class="step_wrap"><?php echo Yii::t('standard', '_PREVIEW'); ?><div class="step">3<em>&nbsp;</em></div></div></li>
   <li><div class="step_wrap"><?=Yii::t('myactivities', 'Additional info')?><div class="step">4<em>&nbsp;</em></div></div></li>
	  <li><div class="step_wrap"><?php echo Yii::t('standard', '_PUBLISH'); ?><div class="step">5<em>&nbsp;</em></div></div></li>
  </ul>
  <div class="fixer">&nbsp;</div>
  <div class="progress">
   <div class="bar bar-success" style="width: 20px;"></div>
  </div>
 </div>

 <h1 class="text-center"><?php echo Yii::t('authoring', 'Slides converter'); ?></h1>
 <p class="lead text-center"><?php echo Yii::t('authoring', 'LOAD_AND_CONVERT'); ?></p>

<?php
//we should always pass course_id parameter to urls
$urlParams = (isset($courseModel) && $courseModel ? array('id_course' => $courseModel->getPrimaryKey()) : array());
$urlParams['slidesConverter'] = true;
if(isset($opt) && $opt === 'centralrepo'){
	$urlParams['opt'] = $opt;
	if($idObject){
		$urlParams['idObject'] = $idObject;
	}
}

$urlParams['id_parent'] = $id_parent;
if($editSlideFile && $id){
	$urlParams['idAuth'] = $id;
}
?>
<script type="text/javascript">
  // Convert divs to queue widgets when the DOM is ready
  $(function() {
	var maxfiles = 1;
	$("#uploader").pluploadQueue({
		// General settings
		runtimes : 'html5,flash',
		url : '<?php echo Yii::app()->createUrl('main/uploadFile', $urlParams); ?>',
		max_file_size : '50mb',
		chunk_size : '1mb',
		unique_names : true,
		multi_selection:false,

		// Specify what files to browse for
		//filters : [
		//	{title : "Presentation files", extensions : "ppt,pptx,odp,pdf"},
		//],

		multipart_params : {
        "<?php echo Yii::app()->request->csrfTokenName ?>" : "<?php echo Yii::app()->request->csrfToken ?>"
		},

		// Flash settings
		flash_swf_url : 'plupload/js/plupload.flash.swf',

		init : {

			FilesAdded: function(up, files) {
				var fileExtension = files[0].name.split('.').pop();
				var extensions = ['ppt', 'pptx', 'pdf', 'odp', 'doc', 'docx'];
				if ($.inArray(fileExtension, extensions) < 0) {
					this.removeFile(files[0]);
					alert('*.' + fileExtension + ' <?php echo addslashes( Yii::t('authoring', 'IS_NOT_SUPPORTED') ); ?>');
				} else {
					up.start();
                    $("#uploading_text, .authoring_wrap .plupload_file_status, .authoring_wrap .plupload_total_status").show();
                    $(".plupload_progress").addClass("progress").show();
                    $(".plupload_progress_bar").addClass("bar bar-success");
				}
			},
			FileUploaded: function(uploader, file, response) {
				console.log(response);
				var res = JSON.parse(response.response);
				var idObject = false;
	  			var idAuthoring = res.idAuthoring;
				var urlParams = '&idAuthoring='+idAuthoring;
				if(typeof idAuthoring === "undefined"){
					idObject = res.idObject;
					urlParams = '&idObject=' + idObject;
				}
				$('#authoring-uploader-form').attr('action', $('#authoring-uploader-form').attr('action')+urlParams);
				$('#authoring-uploader-form').submit();
			}
		}
	});

	// Client side form validation
	$('#authoring-uploader-form').submit(function(e) {
        var uploader = $('#uploader').pluploadQueue();

        // Files in queue upload them first
        if (uploader.files.length > 0) {
            // When all files are uploaded submit form
            uploader.bind('StateChanged', function() {
                if (uploader.files.length === (uploader.total.uploaded + uploader.total.failed)) {
                    $('#authoring-uploader-form')[0].submit();
                }
            });

            uploader.start();
        } else {
            alert('You must queue at least one file.');
        }

        return false;
    });


		$('.plupload_button').addClass('btn-docebo big green');

		$('#continue_to_edit_button').on('click', function(e){
		    e.preventDefault();
			if('<?php echo $opt; ?>'){
				window.location.replace("<?php echo Yii::app()->createUrl('main/edit', array('id'=>$id,  'continue'=>true, 'opt'=>$opt)); ?>");
			}else{
				window.location.replace("<?php echo Yii::app()->createUrl('main/edit', array('id'=>$id, 'id_course'=>$courseModel->idCourse, 'continue'=>true)); ?>");
			}

		    return false;

		});

	  $('#back_to_course_button').on('click', function(e){
		  e.preventDefault();
		  if('<?php echo $opt; ?>'){
			  window.location.replace("<?php echo Docebo::createLmsUrl("centralrepo/centralRepo/index") ?>");
		  }else{
			  window.location.replace("<?php echo Docebo::createAppUrl('lms:player/training/index', array('course_id'=>$courseModel->idCourse)); ?>");
		  }
		  return false;

	  });


  });




 </script>

 <?php
	$params = array();

	if ($id)
		$params['id'] = $id;
	echo CHtml::form(Yii::app()->createUrl('main/converting', array_merge($urlParams, $params)), 'POST', array('id' => 'authoring-uploader-form'));
 ?>
   <div class="upload_wrap text-center">
	<div id="uploader"><?php echo Yii::t('authoring', 'CANT_LOAD_UPLOADER'); ?></div>
    <div class="fixer">&nbsp;</div>
    <div id="uploading_text"><?php echo ucfirst(Yii::t('calendar', '_PLS_WAIT')); ?></div>

		<div class="split-documents">
			<div class="pages-img"></div>
			<?=Yii::t('authoring', 'Converter warning')?>
		</div>
   </div>
	<?php if($editSlideFile): ?>
	<div class="text-right authoring-edit-buttons">

		<a  id="continue_to_edit_button"   href="#" class="btn-docebo green big" ><?php echo Yii::t('standard', '_CONTINUE'); ?></a>&nbsp;&nbsp;
		<a id="back_to_course_button" href="#" class="btn-docebo black big"><?php echo Yii::t('standard', '_CANCEL'); ?></a>

	</div>
	<?php endif;?>

 <?php echo CHtml::endForm(); ?>
</div>

