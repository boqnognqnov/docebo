<?php
$this->breadcrumbs[] = $model->title;
$this->breadcrumbs[] = Yii::t('standard', '_MOD');
?>

<?php Yii::app()->clientScript->registerScriptFile('js/core.js', CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScriptFile('js/common.js', CClientScript::POS_HEAD); ?>

<div class="authoring_wrap pr <?php echo Yii::app()->getLanguage(); ?>">

 <div class="steps">
  <ul>
   <li class="passed"><div class="step_wrap"><?php echo Yii::t('standard', '_UPLOAD'); ?><div class="step">1<em>&nbsp;</em></div></div></li>
   <li class="passed"><div class="step_wrap"><?php echo Yii::t('authoring', 'CONVERT'); ?><div class="step">2<em>&nbsp;</em></div></div></li>
   <li class="passed"><div class="step_wrap"><?php echo Yii::t('standard', '_PREVIEW'); ?><div class="step">3<em>&nbsp;</em></div></div></li>
   <li><div class="step_wrap"><?=Yii::t('myactivities', 'Additional info')?><div class="step">4<em>&nbsp;</em></div></div></li>
   <li><div class="step_wrap"><?php echo Yii::t('standard', '_PUBLISH'); ?><div class="step">5<em>&nbsp;</em></div></div></li>
  </ul>
  <div class="fixer">&nbsp;</div>
  <div class="progress">
   <div class="bar bar-success" style="width: 52%;"></div>
  </div>
 </div>

 <h1 class="text-center"><?php echo Yii::t('authoring', 'EDIT_YOUR_LEARNING_OBJECT'); ?><?php // echo $model->authoring_id; ?></h1>
 <p class="lead text-center"><?php echo Yii::t('authoring', 'POWER_POINT_IS_BECOMING'); ?></p>

 <?php echo $this->renderPartial('_editForm', array('model'=>$model)); ?>