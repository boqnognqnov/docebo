<?php
//we shuld always pass course_id parameter to urls
$urlParams = (isset($courseModel) && $courseModel ? array('id_course' => $courseModel->getPrimaryKey()) : array());
?>


<div class="authoring_wrap">

 <?php

 $classes = array();
 if (!$model->slide_number)
 {
	 $classes[] = 'pager_hidden';
 }
 if (!$model->playbar)
 {
	 $classes[] = 'bar_hidden';
 }
 if (!$model->fullscreen)
 {
	 $classes[] = 'fullscreen_hidden';
 }
 $classesString = implode(' ', $classes);

 $suspendData = CJSON::decode($model->getSuspendData());
 $isRTL = Lang::isRTL(Yii::app()->getLanguage());
 ?>

    <div id="authoring-owl-wrapper" class="<?=$classesString?>">
        <div class="move-left-slide">
            <div class="move-left-box">
                <i class="fa fa-chevron-<?=$isRTL? 'right' : 'left'?>"></i>
            </div>
        </div>
      <div id="authoring-owl-player" class="owl-carousel" style="width:1px;min-width:100%">
          <?php
          foreach($jpgUrls as $image): ?>
              <div class="item"><img src="<?=$image?>"></div>
          <?php endforeach;
          ?>
    </div>
        <?php
        $startSlide = !empty($suspendData) ? $suspendData['slideNumber'] : 1;
        $perc = (($startSlide)/count($jpgUrls))*100;
        ?>
    <div class="custom-owl-progress-bar"><div class="progress-bar" style="width: <?=$perc.'%'?>"></div> </div>
    <div class="custom-owl-nav">
        <a href="javascript:void(0)" class="owl-prev-custom fa fa-chevron-<?=$isRTL? 'right' : 'left'?>"></a>
        <ul id="info" class="info">
            <li class="currp"><?= $startSlide; ?></li>
            <li class="allp">/ <strong><?php echo count($jpgUrls); ?></strong> <?= Yii::t('standard','_PAGES'); ?></li>
        </ul>
        <div class="expand fa fa-expand"></div>
        <div class="compress fa fa-compress"></div>
        <a href="javascript:void(0)" class="owl-next-custom fa fa-chevron-<?=$isRTL? 'left' : 'right'?>"></a>
    </div>
        <div class="move-right-slide">
            <div class="move-right-box pull-right"><i class="fa fa-chevron-<?=$isRTL? 'left' : 'right'?>"></i> </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    $(function() {

        var options = {
            element: '#authoring-owl-player',
            navElement: '.custom-owl-nav',
            wrapper: '#authoring-owl-wrapper',
            debug: false,
            bookmarkUrl: '<?=Yii::app()->createUrl('main/CreateBookmark', $urlParams) ?>',
            modelId: '<?= $model->authoring_id?>',
            suspendData: '<?=$model->getSuspendData()?>',
            presentationParams: {
                backgroundColor: '<?=$model->background_color?>',
                textColor: '<?=$model->text_color?>',
                hoverColor: '<?=$model->hover_color?>'
            },
            totalItems: <?=count($jpgUrls)?>,
			rtl:<?=json_encode($isRTL)?>
        };

        owlPlayer = new OwlPlayer(options);
    });
</script>