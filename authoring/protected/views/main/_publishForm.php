<?php
/* @var $this LearningAuthoringController */
/* @var $model LearningAuthoring */
/* @var $form CActiveForm */
?>

<h3><?php echo Yii::t('authoring', 'Player Setting &amp; Navigation Policy'); ?></h3>
<div class="form adjusted">

<?php $form=$this->beginWidget('QsActiveForm', array(
	'id'=>'learning-authoring-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

    <fieldset class="fl">
     <h4><?php echo Yii::t('authoring', 'Customize buttons'); ?></h4>
     <div class="in">
	  <div class="row" id="pager_toggle">
		<?php echo $form->labelEx($model,'slide_number'); ?>
		<?php echo $form->checkBox($model,'slide_number'); ?>
		<?php echo $form->error($model,'slide_number'); ?>
	  </div>

	  <div class="row" id="bar_toggle">
		<?php echo $form->labelEx($model,'playbar'); ?>
		<?php echo $form->checkBox($model,'playbar'); ?>
		<?php echo $form->error($model,'playbar'); ?>
	  </div>

 	  <div class="row" id="fullscreen_toggle">
		<?php echo $form->labelEx($model,'fullscreen'); ?>
		<?php echo $form->checkBox($model,'fullscreen'); ?>
		<?php echo $form->error($model,'fullscreen'); ?>
	  </div>
      <div class="fixer">&nbsp;</div>
     </div>
    </fieldset>

    <fieldset class="fr">
     <h4><?php echo Yii::t('authoring', 'Customize colors'); ?></h4>
     <div class="in">

      <label><?php echo Yii::t('templatemanager', 'Background color'); ?></label>
	  <?php echo $form->textField($model,'background_color',array('maxlength' => 20, 'id' => 'bgcolor', 'class' => 'color_picker')); ?>
      <div id="bgcolorSelector" class="color_preview"><div style="background-color: <?php echo $model->background_color; ?>"></div></div>
      <div id="bgcolorHolder"></div>
      <div class="fixer">&nbsp;</div>

      <label><?php echo Yii::t('templatemanager', 'Generic text color'); ?></label>
	  <?php echo $form->textField($model,'text_color',array('maxlength' => 20, 'id' => 'textcolor', 'class' => 'color_picker')); ?>
      <div id="textcolorSelector" class="color_preview"><div style="background-color: <?php echo $model->text_color; ?>"></div></div>
      <div id="textcolorHolder"></div>
      <div class="fixer">&nbsp;</div>

      <label><?php echo Yii::t('authoring', 'HOVER_COLOR'); ?></label>
	  <?php echo $form->textField($model,'hover_color',array('maxlength' => 20, 'id' => 'hovercolor', 'class' => 'color_picker')); ?>
      <div id="hovercolorSelector" class="color_preview"><div style="background-color: <?php echo $model->hover_color; ?>"></div></div>
      <div id="hovercolorHolder"></div>
      <div class="fixer">&nbsp;</div>

     </div>
    </fieldset>

    <div class="fixer">&nbsp;</div>

	<div class="row buttons">
     <?php echo CHtml::submitButton(Yii::t('standard', '_PUBLISH'), array('name' => 'publish', "class" => "btn-docebo green big fr")); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
 $(function(){
  $("h4 .edit").click(function(e){
   $(".editable").toggle();
   e.preventDefault();
   e.stopPropagation();
  });

  $(".editable input").keyup(function(){
   $("#editable").html($(this).val());
  });

  $(".editable input").keypress(function(event){
   if (event.which == 13) {
	 $(".editable").toggle();
		event.preventDefault();
		event.stopPropagation();
   }
  });
 });
</script>