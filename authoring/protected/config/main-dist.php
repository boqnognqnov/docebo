<?php

$materials_web_path = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']
	? 'https://devel.docebo/files/doceboLms/authoring'//'https://win-uno.quart-soft.com:8080/docebo/materials'
	: 'http://devel.docebo/files/doceboLms/authoring';//'http://win-uno.quart-soft.com:8080/docebo/materials';

$materials = array(
	'path' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../../files/doceboLms/authoring',//'C:\www\docebo\materials',
	'url' => $materials_web_path
);

//if (!defined('DIRECTORY_SEPARATOR')) define('DIRECTORY_SEPARATOR', '\\');
if (!defined('DIRECTORY_SEPARATOR')) define('DIRECTORY_SEPARATOR', '/');

$domain_code = 'unknow';
// this is needed also on premium in order to understand where we have to upload files on the storage
if (empty($_SESSION['domain_code']) || empty($_SESSION['domain'])) {
	$domain =preg_replace('/^www\\./', '', strtolower($_SERVER['HTTP_HOST']));
	$domain_code =preg_replace('/(\W)/is', '_', $domain);
}
else { // set elsewhere, for example from API
	$domain =$_SESSION['domain'];
	$domain_code =$_SESSION['domain_code'];
}

return ConfigHelper::merge(
		require(dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../../common/config/main.php'), array(
			'name' => 'authoring',
			'basePath' => realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..'),
			'defaultController' => 'main',
			'import' => array(
				'application.models.*',
				'application.components.*',
				'ext.qs.lib.email.*',
				'ext.qs.lib.files.storages.*',
				'ext.qs.lib.files.storages.filesystem.*',
				'ext.qs.lib.files.storages.amazon.*',
				'ext.qs.lib.files.storages.seeweb.*',
				'ext.qs.lib.web.widgets.QsActiveForm',
			),
			'components' => array(
				//filesystem storage
				/*
				'fileStorage' => array(
					'class' => 'QsFileStorageFileSystem',
					'basePath' => $materials['path'],
					'baseUrl' => $materials['url'],
					'filePermission' => 0777,
					'buckets' => array(
						'files' => array(
							'baseSubPath' => 'files',
						),
					)
				),
				//*/

				//S3 filestorage
				/*
				'fileStorage' => array(
					'class' => 'ext.qs.lib.files.storages.amazon.QsFileStorageAmazonS3',
					'awsKey' => 'cos0466',//'{{Aws Key}}',
					'awsSecretKey' => 'po3fui9r',//'{{Aws Secret Key}}',
					'buckets' => array(
						'files' => array(
							'urlName' => 'test-docebo',
							'acl'=>'public-read',
							//'region' => 'files'
						),
					),
				),
				*/

				//Seeweb S3 filestorage
				///*
				'fileStorage' => array(
					'class' => 'ext.qs.lib.files.storages.seeweb.QsFileStorageSeewebS3',
					'awsKey' => '',//'{{Aws Key}}',
					'awsSecretKey' => '',//'{{Aws Secret Key}}',
					'buckets' => array(
						'files' => array(
							'urlName' => 'test-docebo',
							'acl' => 'public-read',
							'region' => 'files'
						),
					),
					'cloudfrontServers' => array(
						'docebo' => 'd2co7zukdjp0d9.cloudfront.net', //Live Bucket
						'sb-docebo' => 'dvy33ao1k10w3.cloudfront.net', //Sandbox bucket
						'test-docebo' => 'd1epwpxl8a4uda.cloudfront.net', //Test bucket
					),
				),
				//*/

				'powerPoint' => array(
					'class' => 'PowerPointComponent',
				),
				'libreOffice' => array(
					'class' => 'LibreOfficeComponent',
					'soffice' => 'C:\Program Files (x86)\LibreOffice 4.0\program\soffice.exe',//'soffice' => '{{Full path to soffice.exe}}',
				),
				'imagick' => array(
					'class' => 'ImagickComponent',
					'convertCommand' => 'C:\Programmi\ImageMagick-6.8.5-Q16\convert.exe',//'convertCommand' => '{{Full path to convert.exe}}',
				),

				'learningOrganization' => array(
					'class' => 'LearningOrganizationComponent'
				),
				'authoringWebService' => array(
					'active' => true,
					'class' => 'WebServiceComponent',
					'convertUrl' => 'http://devel.docebo',//'http://authoring.docebo.com',
					'authKey' => 'b84t68rh3f5te331u7jjhg0pl3j95zx',
					'domain' => isset($GLOBALS['cfg']['valid_domains'][0]) ? $GLOBALS['cfg']['valid_domains'][0] : $domain_code
				)
			),
			'params' => array(
				'Authoring' => array(
					'bucketName' => 'files',
					'subDirTemplate' => '{^__domain__}'.'/'.'{^^__domain__}'.'/'.'{__domain__}'.'/authoring/'.'{pk}',
					'imageResolutions' => array(
						'full' => '1280x960',
						'mini' => '200x150'
					),
				),
				'pluploadTempPath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../../files/tmp',
						//'D:\xampp\htdocs\docebo_devel\files\tmp',
						//'C:\www\docebo\authoring\protected\runtime\plupload',
			),



		)
);