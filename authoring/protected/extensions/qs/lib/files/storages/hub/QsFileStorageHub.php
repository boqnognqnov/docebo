<?php
/**
 * QsFileStorageHub class file.
 *
 * @author Paul Klimov <pklimov@quartsoft.com>
 * @link http://www.quartsoft.com/
 * @copyright Copyright &copy; 2010-2012 QuartSoft ltd.
 * @license http://www.quartsoft.com/license/
 */

/** 
 * QsFileStorageHub introduces the complex file storage, which combines
 * several different file storages in the single facade.
 * While getting the particular bucket from this storage, you may never know
 * it is consist of several ones.
 * Note: to avoid any problems make sure all buckets from all storages have
 * unique name.
 *
 * @author Paul Klimov <pklimov@quartsoft.com>
 * @package qs.files.storages.hub
 */
class QsFileStorageHub extends CApplicationComponent implements IQsFileStorage {
	/**
	 * @var array list of storages.
	 */
	protected $_storages = array();

	/**
	 * Creates file storage instance based on the configuration array.
	 * @param array $storageConfig - configuration array for the file storage.
	 * @return IQsFileStorage file storage instance.
	 */
	protected function createStorageInstance(array $storageConfig) {
		return Yii::createComponent($storageConfig);
	}

	/**
	 * Sets the list of available file storages.
	 * @param array $storages - set of file storage instances or file storage configurations.
	 * @return boolean success.
	 */
	public function setStorages(array $storages) {
		$this->_storages = array();
		foreach ($storages as $storageKey => $storageValue) {
			if ( is_numeric($storageKey) && is_string($storageValue) ) {
				$storageName = $storageValue;
				$storageData = array();
			} else {
				$storageName = $storageKey;
				$storageData = $storageValue;
			}
			$this->addStorage($storageName, $storageData);
		}
		return true;
	}

	/**
	 * Gets the list of available file storage instances.
	 * @return array set of file storage instances.
	 */
	public function getStorages() {
		$result = array();
		foreach ($this->_storages as $storageName => $storageData) {
			$result[$storageName] = $this->getStorage($storageName);
		}
		return $result;
	}

	/**
	 * Gets the file storage instance by name.
	 * @param string $storageName - name of the storage.
	 * @return array set of storage instances.
	 */
	public function getStorage($storageName) {
		if (!array_key_exists($storageName, $this->_storages)) {
			throw new CException("Storage named '{$storageName}' does not exists in the file storage hub '".get_class($this)."'");
		}
		$storageData = $this->_storages[$storageName];
		if (is_object($storageData)) {
			$storageInstance = $storageData;
		} else {
			$storageInstance = $this->createStorageInstance($storageData);
			$this->_storages[$storageName] = $storageInstance;
		}
		return $storageInstance;
	}

	/**
	 * Adds the storage to the storages list.
	 * @param string $storageName - name of the storage.
	 * @param mixed $storageData - storage instance or configuration array.
	 * @return boolean success.
	 */
	public function addStorage($storageName, $storageData=array()) {
		if (!is_string($storageName)) {
			throw new CException('Name of the storage should be a string!');
		}
		if (is_scalar($storageData) || empty ($storageData) ) {
			throw new CException('Data of the storage should be an file storage object or configuration array!');
		}
		$this->_storages[$storageName] = $storageData;
		return true;
	}

	/**
	 * Indicates if the storage has been set up in the storage hub.
	 * @param string $storageName - name of the storage.
	 * @return boolean success.
	 */
	public function hasStorage($storageName) {
		return array_key_exists($storageName, $this->_storages);
	}

	/**
	 * Returns the default file storage, meaning the first one in the
	 * {@link storages} list.
	 * @return IQsFileStorage file storage instance.
	 */
	protected function getDefaultStorage() {
		$storagesList = $this->_storages;
		$defaultStorageName = array_shift( array_keys($storagesList) );
		if (empty($defaultStorageName)) {
			throw new CException('Unable to determine default storage in the hub!');
		}
		$storage = $this->getStorage($defaultStorageName);
		return $storage;
	}

	/**
	 * Sets the list of available buckets.
	 * @param array $buckets - set of bucket instances or bucket configurations.
	 * @return boolean success.
	 */
	public function setBuckets(array $buckets) {
		$storage = $this->getDefaultStorage();
		return $storage->setBuckets($buckets);
	}

	/**
	 * Gets the list of available bucket instances.
	 * @return array set of bucket instances.
	 */
	public function getBuckets() {
		$buckets = array();
		foreach ($this->getStorages() as $storage) {
			$buckets = array_merge($storage->getBuckets(), $buckets);
		}
		return $buckets;
	}

	/**
	 * Gets the bucket intance by name.
	 * @param string $bucketName - name of the bucket.
	 * @return array set of bucket instances.
	 */
	public function getBucket($bucketName) {
		$storagesList = $this->_storages;
		foreach ($storagesList as $storageName => $storageData) {
			$storage = $this->getStorage($storageName);
			if ($storage->hasBucket($bucketName)) {
				return $storage->getBucket($bucketName);
			}
		}
		throw new CException("Bucket named '{$bucketName}' does not exists in any file storage of the hub '".get_class($this)."'");
	}

	/**
	 * Adds the bucket to the buckets list.
	 * @param string $bucketName - name of the bucket.
	 * @param mixed $bucketData - bucket instance or configuration array.
	 * @return boolean success.
	 */
	public function addBucket($bucketName, $bucketData=array()) {
		$storage = $this->getDefaultStorage();
		return $storage->addBucket($bucketName, $bucketData);
	}

	/**
	 * Indicates if the bucket has been set up in the storage.
	 * @param string $bucketName - name of the bucket.
	 * @return boolean success.
	 */
	public function hasBucket($bucketName) {
		$storagesList = $this->_storages;
		foreach ($storagesList as $storageName => $storageData) {
			$storage = $this->getStorage($storageName);
			if ($storage->hasBucket($bucketName)) {
				return true;
			}
		}
		return false;
	}
}