<?php
/**
 * QsFileStorageAmazonS3 class file.
 *
 * @author Paul Klimov <pklimov@quartsoft.com>
 * @link http://www.quartsoft.com/
 * @copyright Copyright &copy; 2008-2013 QuartSoft ltd.
 * @license http://www.quartsoft.com/license/
 */

Yii::import('ext.qs.lib.files.storages.*');

/**
 * QsFileStorageAmazonS3 introduces the file storage based on
 * Amazon S3 service.
 *
 * @see QsFileStorageBucketAmazonS3
 *
 * @property string $amazonSdkLibraryPath public alias of {@link _amazonSdkLibraryPath}.
 * @property AmazonS3 $amazonS3 public alias of {@link _amazonS3}.
 * @property string $awsKey public alias of {@link _awsKey}.
 * @property string $awsSecretKey public alias of {@link _awsSecretKey}.
 *
 * @author Paul Klimov <pklimov@quartsoft.com>
 * @package qsextra.files.storages.amazon
 */
class QsFileStorageAmazonS3 extends QsFileStorage {
	/**
	 * @var string name of the bucket class.
	 */
	protected $_bucketClassName = 'QsFileStorageBucketAmazonS3';
	/**
	 * @var string real path to the amazon SDK library.
	 * By default this will be set to the path of alias "ext.vendors.amazon".
	 */
	protected $_amazonSdkLibraryPath = '';
	/**
	 * @var AmazonS3 instance of the Amazon S3 gateway.
	 */
	protected $_amazonS3 = null;
	/**
	 * @var string AWS (Amazon Web Service) key.
	 * If constant 'AWS_KEY' has been defined, this field can be left blank.
	 */
	protected $_awsKey = null;
	/**
	 * @var string AWS (Amazon Web Service) secret key.
	 * If constant 'AWS_SECRET_KEY' has been defined, this field can be left blank.
	 */
	protected $_awsSecretKey = null;

	// Set / Get :

	public function setAmazonSdkLibraryPath($amazonSdkLibraryPath) {
		if (!is_string($amazonSdkLibraryPath)) {
			throw new CException('"'.get_class($this).'::amazonSdkLibraryPath" should be a string!');
		}
		$this->_amazonSdkLibraryPath = $amazonSdkLibraryPath;
		return true;
	}

	public function getAmazonSdkLibraryPath() {
		if (empty($this->_amazonSdkLibraryPath)) {
			$this->initAmazonSdkLibraryPath();
		}
		return $this->_amazonSdkLibraryPath;
	}

	public function setAmazonS3($amazonS3) {
		if (!is_object($amazonS3)) {
			throw new CException('"' . get_class($this) . '::amazonS3" should be an object!');
		}
		$this->_amazonS3 = $amazonS3;
		return true;
	}

	public function getAmazonS3() {
		if (!is_object($this->_amazonS3)) {
			$this->initAmazonS3();
		}
		return $this->_amazonS3;
	}

	public function setAwsKey($awsKey) {
		$this->_awsKey = $awsKey;
		return true;
	}

	public function getAwsKey() {
		return $this->_awsKey;
	}

	public function setAwsSecretKey($awsSecretKey) {
		$this->_awsSecretKey = $awsSecretKey;
		return true;
	}

	public function getAwsSecretKey() {
		return $this->_awsSecretKey;
	}

	/**
	 * Initializes the path to Amazon SDK library.
	 * @return boolean success.
	 */
	protected function initAmazonSdkLibraryPath() {
		$libraryRootPath = realpath(dirname(__FILE__).'/../../../');
		$amazonSdkLibraryPath = $libraryRootPath.'/vendors/amazon';
		$this->_amazonSdkLibraryPath = $amazonSdkLibraryPath;
		return true;
	}

	/**
	 * Initializes the instance of the Amazon S3 service gateway.
	 * @return boolean success.
	 */
	protected function initAmazonS3() {
		require_once($this->getAmazonSdkLibraryPath().DIRECTORY_SEPARATOR.'sdk.class.php');
		$amazonAutoloaderCallback = array('CFLoader', 'autoloader');
		Yii::registerAutoloader($amazonAutoloaderCallback);
		$amazonS3Options = array(
			'key' => $this->getAwsKey(),
			'secret' => $this->getAwsSecretKey(),
		);
		$this->_amazonS3 = new AmazonS3($amazonS3Options);
		$this->defineAmazonConstants();
		return true;
	}

	/**
	 * Defines Amazon constants, which are required for the process.
	 * @return boolean success.
	 */
	protected function defineAmazonConstants() {
		defined('S3_ACL_PRIVATE') or define('S3_ACL_PRIVATE', 'private');
		defined('S3_ACL_PUBLIC') or define('S3_ACL_PUBLIC', 'public-read');
		defined('S3_ACL_OPEN') or define('S3_ACL_OPEN', 'public-read-write');
		defined('S3_ACL_AUTH_READ') or define('S3_ACL_AUTH_READ', 'authenticated-read');
		return true;
	}
}