<?php
/**
 * QsFileStorageBucketAmazonS3 class file.
 *
 * @author Paul Klimov <pklimov@quartsoft.com>
 * @link http://www.quartsoft.com/
 * @copyright Copyright &copy; 2008-2013 QuartSoft ltd.
 * @license http://www.quartsoft.com/license/
 */

Yii::import('ext.qs.lib.files.storages.*');

/**
 * QsFileStorageBucketAmazonS3 introduces the file storage based on
 * Amazon S3 service.
 *
 * @see QsFileStorageAmazonS3
 *
 * @property string $urlName public alias of {@link _urlName}.
 * @property string $region public alias of {@link _region}.
 * @property mixed $acl public alias of {@link _acl}.
 * @method QsFileStorageAmazonS3 getStorage()
 *
 * @author Paul Klimov <pklimov@quartsoft.com>
 * @package qsextra.files.storages.amazon
 */
class QsFileStorageBucketSeewebS3 extends QsFileStorageBucket {
	/**
	 * @var string Storage DNS URL name.
	 * This name will be used as actual bucket name in Amazon S3.
	 * If this field is left blank its value will be
	 * generated using {@link name}.
	 * @see AmazonS3::validate_bucketname_support()
	 */
	protected $_urlName = '';
	/**
	 * @var string Amazon region name of the bucket.
	 * You can setup this value as a short alias of the real region name
	 * according the following map:
	 * <pre>
	 * 'us_e1' => AmazonS3::REGION_US_E1
	 * 'us_w1' => AmazonS3::REGION_US_W1
	 * 'eu_w1' => AmazonS3::REGION_EU_W1
	 * 'apac_se1' => AmazonS3::REGION_APAC_SE1
	 * 'apac_ne1' => AmazonS3::REGION_APAC_NE1
	 * </pre>
	 * @see AmazonS3
	 */
	protected $_region = '';
	/**
	 * @var mixed bucket ACL policy.
	 * You can setup this value as a short alias of the real region name
	 * according the following map:
	 * <pre>
	 * 'private' => AmazonS3::ACL_PRIVATE
	 * '' =>
	 * '' =>
	 * '' => 
	 * </pre>
	 * @see AmazonS3
	 */
	protected $_acl = '';
	/**
	 * @var array internal cache data.
	 * This field is for the internal usage only.
	 */
	protected $_internalCache = array();
	
	// Set / Get ;

	public function setUrlName($urlName) {
		if (!is_string($urlName)) {
			throw new CException('"'.get_class($this).'::urlName" should be a string!');
		}
		$this->_urlName = $urlName;
		return true;
	}

	public function getUrlName() {
		if (empty($this->_urlName)) {
			$this->initUrlName();
		}
		return $this->_urlName;
	}

	public function setRegion($region) {
		$this->_region = $region;
		return true;
	}

	public function getRegion() {
		return $this->_region;
	}

	public function setAcl($acl) {
		$this->_acl = $acl;
		return true;
	}

	public function getAcl() {
		if (empty($this->_acl)) {
			$this->initAcl();
		}
		return $this->_acl;
	}

	/**
	 * Initializes URL name using {@link name}.
	 * @return boolean success.
	 */
	protected function initUrlName() {
		$urlName = $this->getName();
		$urlName = preg_replace('/([^A-Z|^0-9|^-])/is', '-', $urlName);
		$this->_urlName = $urlName;
		return true;
	}

	/**
	 * Initializes ACL default value.
	 * @return bool
	 */
	protected function initAcl() {
		$this->_acl = 'private';
		return true;
	}

	/**
	 * Clears internal cache data.
	 * @return boolean success.
	 */
	public function clearInternalCache() {
		$this->_internalCache = array();
		return true;
	}

	/**
	 * Checks if the response given by xml contains the successful status.
	 * @param SimpleXMLElement $xmlResponse - response xml object.
	 * @return boolean response successful.
	 */
	protected function isResponseSuccessful($xmlResponse) {
		$successfulStatusPrefix = '20';
		$status = $this->findXmlResponseStatus($xmlResponse);
		return (strpos($status, $successfulStatusPrefix) === 0);
	}

	/**
	 * Finds the value of status in the xml response object.
	 * @param SimpleXMLElement $xmlResponse - response xml object.
	 * @return string status value.
	 */
	protected function findXmlResponseStatus($xmlResponse) {
		return $xmlResponse->status;
	}

	/**
	 * Finds the value of status explain message in the xml response object.
	 * @param SimpleXMLElement $xmlResponse - response xml object.
	 * @return string status message.
	 */
	protected function findXmlResponseStatusMessage($xmlResponse) {
		$statusMessage = ''.$xmlResponse->body->Code;
		if (!empty($statusMessage)) {
			$statusMessage .= "\n";
		}
		$statusMessage .= $xmlResponse->body->Message;
		return $statusMessage;
	}

	/**
	 * Creates Amazon S3 file complex reference, which includes bucket name and file self name.
	 * Such reference can be passed to {@link AmazonS3::copy_object()}.
	 * File can be passed as string, which means the internal bucket file,
	 * or as an array of 2 elements: first one - the name of the bucket,
	 * the second one - name of the file in this bucket
	 * @param mixed $file - this bucket existing file name or array reference to another bucket file name.
	 * @return array Amazon S3 file complex reference.
	 */
	protected function getFileAmazonComplexReference($file) {
		if (is_array($file)) {
			list($bucketName, $fileName) = $file;
		} else {
			$bucketName = $this->getName();
			$fileName = $file;
		}
		$result = array(
			'bucket' => $bucketName,
			'filename' => $fileName
		);
		return $result;
	}

	/**
	 * Returns the actual Amazon region value from the {@link region}.
	 * @throws CException on invalid region.
	 * @return string actual Amazon region.
	 */
	protected function fetchActualRegion() {
		$region = $this->getRegion();
		if (empty($region)) {
			throw new CException('"'.get_class($this).'::region" can not be empty.');
		}
		$this->getStorage()->getAmazonS3(); // make sure class "AmazonS3" has been loaded
		switch ($region) {
			case 'us_e1': {
				return AmazonS3::REGION_US_E1;
			}
			case 'us_w1': {
				return AmazonS3::REGION_US_W1;
			}
			case 'eu_w1': {
				return AmazonS3::REGION_EU_W1;
			}
			case 'apac_se1': {
				return AmazonS3::REGION_APAC_SE1;
			}
			case 'apac_ne1': {
				return AmazonS3::REGION_APAC_NE1;
			}
			default: {
				return $region;
			}
		}
	}

	/**
	 * Returns the actual Amazon bucket ACL value from the {@link acl}.
	 * @return string actual Amazon bucket ACL.
	 */
	protected function fetchActualAcl() {
		$acl = $this->getAcl();
		$this->getStorage()->getAmazonS3(); // make sure class "AmazonS3" has been loaded
		switch ($acl) {
			case 'private': {
				return AmazonS3::ACL_PRIVATE;
			}
			case 'public': {
				return AmazonS3::ACL_PUBLIC;
			}
			case 'open': {
				return AmazonS3::ACL_OPEN;
			}
			case 'auth_read': {
				return AmazonS3::ACL_AUTH_READ;
			}
			case 'owner_read': {
				return AmazonS3::ACL_OWNER_READ;
			}
			case 'owner_full_control': {
				return AmazonS3::ACL_OWNER_FULL_CONTROL;
			}
			default: {
				return $acl;
			}
		}
	}

	/**
	 * Creates this bucket.
	 * @return boolean success.
	 */
	public function create() {
		$amazonS3 = $this->getStorage()->getAmazonS3();
		$xmlResponse = $amazonS3->create_bucket($this->getUrlName(), $this->fetchActualRegion(), $this->fetchActualAcl());
		if (!$this->isResponseSuccessful($xmlResponse)) {
			$status = $this->findXmlResponseStatus($xmlResponse);
			$statusMessage = $this->findXmlResponseStatusMessage($xmlResponse);
			throw new CException("Unable to create bucket named '".$this->getName()."', status = ".$status.", message='".$statusMessage."'!");
		}
		$this->log('bucket has been created with URL name "' . $this->getUrlName() . '"');
		return true;
	}

	/**
	 * Destroys this bucket.
	 * @return boolean success.
	 */
	public function destroy() {
		$amazonS3 = $this->getStorage()->getAmazonS3();
		$xmlResponse = $amazonS3->delete_bucket($this->getUrlName(), true);
		if (!$this->isResponseSuccessful($xmlResponse)) {
			$status = $this->findXmlResponseStatus($xmlResponse);
			$statusMessage = $this->findXmlResponseStatusMessage($xmlResponse);
			throw new CException("Unable to destroy bucket named '" . $this->getName() . "', status = " . $status . ", message='" . $statusMessage . "'!");
		}
		$this->clearInternalCache();
		$this->log('bucket has been destroyed with URL name "' . $this->getUrlName() . '"');
		return true;
	}

	/**
	 * Checks is bucket exists.
	 * @return boolean success.
	 */
	public function exists() {
		if ($this->_internalCache['exists']) {
			return true;
		}
		$amazonS3 = $this->getStorage()->getAmazonS3();
		$result = $amazonS3->if_bucket_exists($this->getUrlName());
		$this->_internalCache['exists'] = $result;
		return $result;
	}

	/**
	 * Saves content as new file.
	 * @param string $fileName - new file name.
	 * @param string $content - new file content.
	 * @return boolean success.
	 */
	public function saveFileContent($fileName, $content) {
		if (!$this->exists()) {
			$this->create();
		}
		$amazonS3 = $this->getStorage()->getAmazonS3();
		$options = array(
			'body' => $content,
			'acl' => $this->getAcl()
		);
		$xmlResponse = $amazonS3->create_object($this->getUrlName(), $fileName, $options);
		$result = $this->isResponseSuccessful($xmlResponse);
		if ($result) {
			$this->log("file '{$fileName}' has been saved");
		} else {
			$this->log("Unable to save file '{$fileName}'!", CLogger::LEVEL_ERROR);
		}
		return $result;
	}

	/**
	 * Returns content of an existing file.
	 * @param string $fileName - new file name.
	 * @return string $content - file content.
	 */
	public function getFileContent($fileName) {
		if (!$this->exists()) {
			$this->create();
		}
		$amazonS3 = $this->getStorage()->getAmazonS3();
		$xmlResponse = $amazonS3->get_object($this->getUrlName(), $fileName);
		if (!$this->isResponseSuccessful($xmlResponse)) {
			$status = $this->findXmlResponseStatus($xmlResponse);
			$statusMessage = $this->findXmlResponseStatusMessage($xmlResponse);
			throw new CException("Unable to get file from the bucket named '" . $this->getName() . "', status = " . $status . ", message='" . $statusMessage . "'!");
		}
		$fileContent = $xmlResponse->body;
		$this->log("content of file '{$fileName}' has been returned");
		return $fileContent;
	}

	/**
	 * Deletes an existing file.
	 * @param string $fileName - new file name.
	 * @return boolean success.
	 */
	public function deleteFile($fileName) {
		if (!$this->exists()) {
			$this->create();
		}
		$amazonS3 = $this->getStorage()->getAmazonS3();
		$xmlResponse = $amazonS3->delete_object($this->getUrlName(), $fileName);
		$result = $this->isResponseSuccessful($xmlResponse);
		if ($result) {
			$this->log("file '{$fileName}' has been deleted");
		} else {
			$this->log("unable to delete file '{$fileName}'!", CLogger::LEVEL_ERROR);
		}
		return $result;
	}

	/**
	 * Checks if the file exists in the bucket.
	 * @param string $fileName - searching file name.
	 * @return boolean file exists.
	 */
	public function fileExists($fileName) {
		if (!$this->exists()) {
			$this->create();
		}
		$amazonS3 = $this->getStorage()->getAmazonS3();
		return $amazonS3->if_object_exists($this->getUrlName(), $fileName);
	}

	/**
	 * Copies file from the OS file system into the bucket.
	 * @param string $srcFileName - OS full file name.
	 * @param string $fileName - new bucket file name.
	 * @return boolean success.
	 */
	public function copyFileIn($srcFileName, $fileName) {
		$fileContent = file_get_contents($srcFileName);
		$fileName = str_replace('\\', '/', $fileName); //brutally avoid backslashes in the file name, they may cause problems in many cases
		return $this->saveFileContent($fileName, $fileContent);
	}

	/**
	 * Copies file from the bucket into the OS file system.
	 * @param string $fileName - bucket existing file name.
	 * @param string $destFileName - new OS full file name.
	 * @return boolean success.
	 */
	public function copyFileOut($fileName, $destFileName) {
		$fileContent = $this->getFileContent($fileName);
		$bytesWritten = file_put_contents($destFileName, $fileContent);
		return ($bytesWritten > 0);
	}

	/**
	 * Copies file inside this bucket or between this bucket and another
	 * bucket of this file storage.
	 * File can be passed as string, which means the internal bucket file,
	 * or as an array of 2 elements: first one - the name of the bucket,
	 * the second one - name of the file in this bucket
	 * @param mixed $srcFile - this bucket existing file name or array reference to another bucket file name.
	 * @param mixed $destFile - this bucket existing file name or array reference to another bucket file name.
	 * @return boolean success.
	 */
	public function copyFileInternal($srcFile, $destFile) {
		if (!$this->exists()) {
			$this->create();
		}

		$storage = $this->getStorage();
		$amazonS3 = $storage->getAmazonS3();

		$srcFileRef = $this->getFileAmazonComplexReference($srcFile);
		$destFileRef = $this->getFileAmazonComplexReference($destFile);
		//throw new Exception( CVarDumper::dumpAsString($srcFileRef)."\n".CVarDumper::dumpAsString($destFileRef) );

		$srcBucket = $storage->getBucket($srcFileRef['bucket']);

		if (!$srcBucket->exists()) {
			$srcBucket->create();
			$srcFileRef['bucket'] = $srcBucket->getUrlName();
		}else{
			$srcFileRef['bucket'] = $srcBucket->getUrlName();
		}

		$destBucket = $storage->getBucket($destFileRef['bucket']);
		if (!$destBucket->exists()) {
			$destBucket->create();
			$destFileRef['bucket'] = $destBucket->getUrlName();
		}else{
			$destFileRef['bucket'] = $destBucket->getUrlName();
		}
		
		$options = array(
			'acl' => $this->getAcl()
		);

		$xmlResponse = $amazonS3->copy_object($srcFileRef, $destFileRef, $options);

		$result = $this->isResponseSuccessful($xmlResponse);
		if ($result) {
			$this->log("file '{$srcFileRef['bucket']}/{$srcFileRef['filename']}' has been copied to '{$destFileRef['bucket']}/{$destFileRef['filename']}'");
		} else {
			$this->log("Unable to copy file from '{$srcFileRef['bucket']}/{$srcFileRef['filename']}' to '{$destFileRef['bucket']}/{$destFileRef['filename']}'!", CLogger::LEVEL_ERROR);
		}
		return $result;
	}

	/**
	 * Copies file from the OS file system into the bucket and
	 * deletes the source file.
	 * @param string $srcFileName - OS full file name.
	 * @param string $fileName - new bucket file name.
	 * @return boolean success.
	 */
	public function moveFileIn($srcFileName, $fileName) {
		return ($this->copyFileIn($srcFileName, $fileName) && unlink($srcFileName));
	}

	/**
	 * Copies file from the bucket into the OS file system and
	 * deletes the source bucket file.
	 * @param string $fileName - bucket existing file name.
	 * @param string $destFileName - new OS full file name.
	 * @return boolean success.
	 */
	public function moveFileOut($fileName, $destFileName) {
		$result = $this->copyFileOut($fileName, $destFileName);
		if ($result) {
			$result = $result && $this->deleteFile($fileName);
		}
		return $result;
	}

	/**
	 * Moves file inside this bucket or between this bucket and another
	 * bucket of this file storage.
	 * File can be passed as string, which means the internal bucket file,
	 * or as an array of 2 elements: first one - the name of the bucket,
	 * the second one - name of the file in this bucket
	 * @param mixed $srcFile - this bucket existing file name or array reference to another bucket file name.
	 * @param mixed $destFile - this bucket existing file name or array reference to another bucket file name.
	 * @return boolean success.
	 */
	public function moveFileInternal($srcFile, $destFile) {
		$result = $this->copyFileInternal($srcFile, $destFile);
		if ($result) {
			$srcFileRef = $this->getFileAmazonComplexReference($srcFile);
			$bucketName = $srcFileRef['bucket'];
			$fileName = $srcFileRef['filename'];
			$bucket = $this->getStorage()->getBucket($bucketName);
			$result = $result && $bucket->deleteFile($fileName);
		}
		return $result;
	}

	/**
	 * Gets web URL of the file.
	 * @param string $fileName - self file name.
	 * @return string file web URL.
	 */
	public function getFileUrl($fileName) {
		if (!$this->exists()) {
			$this->create();
		}
		$amazonS3 = $this->getStorage()->getAmazonS3();
		$objectUrl = $amazonS3->get_object_url($this->getUrlName(), $fileName);
		
		if (array_key_exists($this->getUrlName(), $this->getStorage()->cloudfrontServers)) {
			$cs = $this->getStorage()->cloudfrontServers[$this->getUrlName()];
			$objectUrl = str_replace(
				array($this->getUrlName().'.'.'seewebstorage.it', 'seewebstorage.it'.'/'.$this->getUrlName()), 
				array($cs, $cs), 
				$objectUrl
			);
		}
		
		return $objectUrl;
	}
}