<?php

/**
 * Action for uploading files by plupload
 */
class ActionUploadFile extends CAction
{
	public function run()
	{
		// HTTP headers for no cache etc
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		// Settings
		$targetDir = Yii::app()->params['pluploadTempPath'];
		//$targetDir = 'uploads';

		$cleanupTargetDir = true; // Remove old files
		$maxFileAge = 5 * 3600; // Temp file age in seconds
		// 5 minutes execution time
		@set_time_limit(5 * 60);

		// Uncomment this one to fake upload time
		// usleep(5000);
		// Get parameters
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
		$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

		// Clean the fileName for security reasons
		$fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

		// Make sure the fileName is unique but only if chunking is disabled
		if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName))
		{
			$ext = strrpos($fileName, '.');
			$fileName_a = substr($fileName, 0, $ext);
			$fileName_b = substr($fileName, $ext);

			$count = 1;
			while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
				$count++;

			$fileName = $fileName_a . '_' . $count . $fileName_b;
		}

		$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

		// Create target dir
		if (!file_exists($targetDir))
			@mkdir($targetDir);

		// Remove old temp files	
		if ($cleanupTargetDir)
		{
			if (is_dir($targetDir) && ($dir = opendir($targetDir)))
			{
				while (($file = readdir($dir)) !== false)
				{
					$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

					// Remove temp file if it is older than the max age and is not the current file
					if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part"))
					{
						@unlink($tmpfilePath);
					}
				}
				closedir($dir);
			}
			else
			{
				die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
			}
		}

		// Look for the content type header
		if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
			$contentType = $_SERVER["HTTP_CONTENT_TYPE"];

		if (isset($_SERVER["CONTENT_TYPE"]))
			$contentType = $_SERVER["CONTENT_TYPE"];

		// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
		if (strpos($contentType, "multipart") !== false)
		{
			if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name']))
			{
				// Open temp file
				$out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
				if ($out)
				{
					// Read binary input stream and append it to temp file
					$in = @fopen($_FILES['file']['tmp_name'], "rb");

					if ($in)
					{
						while ($buff = fread($in, 4096))
							fwrite($out, $buff);
					}
					else
						die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
					@fclose($in);
					@fclose($out);
					@unlink($_FILES['file']['tmp_name']);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
		} else
		{
			// Open temp file
			$out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
			if ($out)
			{
				// Read binary input stream and append it to temp file
				$in = @fopen("php://input", "rb");

				if ($in)
				{
					while ($buff = fread($in, 4096))
						fwrite($out, $buff);
				}
				else
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

				@fclose($in);
				@fclose($out);
			}
			else
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
		}

		// Check if file has been uploaded
		if (!$chunks || $chunk == $chunks - 1)
		{
			// Strip the temp .part suffix off 
			rename("{$filePath}.part", $filePath);

			if(isset($_REQUEST["slidesConverter"]) && $_REQUEST["slidesConverter"]==true){
				// Check if uploaded file exists
				$uploadTmpFile = $filePath;//Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $file['name'];
				if (!is_file($uploadTmpFile)) {
					throw new CException("File '"  /*.$file['name']*/ . "' does not exist.");
				}

				// Create storage Object, use it to determine the VIDEO whitelist type to use
				$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_AUTHORING);
				$whitelistType = FileTypeValidator::TYPE_AUTHORING;//($storage->type == CFileStorage::TYPE_S3) ? FileTypeValidator::TYPE_VIDEO_EXTENDED_LIST : FileTypeValidator::TYPE_VIDEO;

				/*** File extension type validate ************************/
				$fileValidator = new FileTypeValidator($whitelistType);
				if (!$fileValidator->validateLocalFile($uploadTmpFile))
				{
					FileHelper::removeFile($uploadTmpFile);
					throw new CException($fileValidator->errorMsg);
				}
				/*********************************************************/

				//create the LearningAuthoring
				$idAuthoring = isset($_REQUEST["idAuth"]) ? intval($_REQUEST["idAuth"]) : 0;
				$idObject = Yii::app()->request->getParam('idObject', false);
				if($idAuthoring != 0){
					$authoringModel = LearningAuthoring::model()->findByPk($idAuthoring);
				}
				else{
					$authoringModel = new LearningAuthoring();
					$authoringModel->title = Yii::t('standard', '_TITLE');
					$authoringModel->parentFolder = Yii::app()->request->getParam('id_parent', false);
				}

				$trans = Yii::app()->db->beginTransaction();
				try {
					$authoringModel->file_extension = strtolower(CFileHelper::getExtension($fileName));//$filePath;
					$centralRepoContext = false;
					if(isset($_REQUEST['opt']) && $_REQUEST['opt'] === 'centralrepo'){
						$centralRepoContext = true;
					}

					if($centralRepoContext){
						$authoringModel->organizationSync = false;
					}
					$rs = $authoringModel->save();
					$repoObject = null;

					if($centralRepoContext && !$idAuthoring){
						$repoObject = new LearningRepositoryObject();
						$repoObject->object_type = LearningOrganization::OBJECT_TYPE_AUTHORING;
						$categoryNodeId = LearningCourseCategoryTree::getRootNodeId();
						if(isset(Yii::app()->session['currentCentralRepoNodeId'])){
							$categoryNodeId = Yii::app()->session['currentCentralRepoNodeId'];
						}
						$repoObject->id_category = $categoryNodeId;

						if($repoObject->save()){
							$versionModel = new LearningRepositoryObjectVersion();
							$versionModel->id_object = $repoObject->id_object;
							$versionModel->id_resource = $authoringModel->authoring_id;
							$versionModel->object_type = LearningOrganization::OBJECT_TYPE_AUTHORING;
							$versionModel->version = 1;
							$versionModel->version_name = 'V1';
							$versionModel->version_description = '';
							$userModel = Yii::app()->user->loadUserModel();
							/**
							 * @var $userModel CoreUser
							 */
							$authorData = array(
								'idUser' => $userModel->idst,
								'username' => $userModel->getUserNameFormatted()
							);
							$versionModel->author = CJSON::encode($authorData);
							$versionModel->create_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');

							if(!$versionModel->save()){
								Yii::log("Error messages:" . var_export($versionModel->errors,true), CLogger::LEVEL_ERROR);
							}
						} else{
							Yii::log("Error messages:" . var_export($repoObject->errors,true), CLogger::LEVEL_ERROR);
						}
					}elseif($centralRepoContext && !$authoringModel->isNewRecord && $idObject){
						$repoObject = LearningRepositoryObject::model()->findByPk($idObject);
					}

					if ($rs) {
						$trans->commit();
						$output = array(
								'success' => true,
								'id' => $authoringModel->authoring_id
						);

					} else {
						$trans->rollback();
						$output = array(
								'success' => false,
								'message' => Yii::t('authoring', 'Error while converting file')//$model->getErrors()
						);
						Yii::app()->user->setFlash('authoring-conversion-error', 'Error saving file');
					}
				} catch (CException $e) {
					$trans->rollback();
					$message = $e->getMessage();
					$output = array(
							'success' => false,
							'message' => $message
					);
					Yii::app()->user->setFlash('authoring-conversion-error', $message);
				}


				// Rename uploaded file
				$extension = strtolower(CFileHelper::getExtension($fileName));
				$newFileName = $authoringModel->authoring_id . "_1." . $extension;//Docebo::randomHash() . "." . $extension;
				$newTmpFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $newFileName;
				if (!@rename($uploadTmpFile, $newTmpFile)) {
					throw new CException("Error renaming uploaded file.");
				}


				// Move uploaded file to final storage for later usage
				// The result from store() is FALSE OR the path (key for S3) to the file
				$destFilePath = $storage->store($newTmpFile,(string)$authoringModel->authoring_id);

				if (!$destFilePath) {
					throw new CException("Error while saving file to final storage.");
				}
				// store() MAY change format and extension of the file! Lets get them back
//				$destExtension =  strtolower(CFileHelper::getExtension(pathinfo($destFilePath, PATHINFO_BASENAME)));
//				$destFilename  =  pathinfo($destFilePath, PATHINFO_BASENAME);
				$outputData = '{"jsonrpc" : "2.0", "result" : null, "id" : "id", "idAuthoring" : '.$authoringModel->authoring_id.'}';
				if($centralRepoContext && isset($repoObject->id_object)){
					$outputData = '{"jsonrpc" : "2.0", "result" : null, "id" : "id", "idObject" : '.$repoObject->id_object.', "idAuthoring" : '.$authoringModel->authoring_id.'}';
				}
				die($outputData);
			}

		}

		die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
	}
}
