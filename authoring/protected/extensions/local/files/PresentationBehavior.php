<?php
Yii::import('qsExt.qs.lib.db.ar.QsActiveRecordBehaviorFile');

/**
 * Behavior for the {@link CActiveRecord}, which allows to save a presentation file and converted jpgs per each table record.
 * Behavior tracks the file extension and manage file version in order to prevent cache problems.
 * Due to this the database table, which the model refers to, must contain fields {@link fileExtensionAttributeName} and {@link fileVersionAttributeName}.
 * On the model save behavior will automatically search for the attached file in $_FILES.
 * However you can manipulate attached file using property {@link uploadedFile}.
 * For the tabular file input use {@link fileTabularInputIndex} property.
 *
 * Note: you can always use {@link saveFile} method to attach any file (not just uploaded one) to the model.
 *
 * Attention: this extension requires the extension "ext.qs.lib.files.storages" to be attached to the application!
 * Files will be saved using file storage component.
 * @see IQsFileStorage
 * @see IQsFileStorageBucket
 */
class PresentationBehavior extends QsActiveRecordBehaviorFile
{
	/**
	 * @var string name of the application component, which should be used to convert presentation files with PowerPoint
	 */
	public $powerPointComponentName = 'powerPoint';
	/**
	 * @var string name of the application component, which should be used to convert presentation files with LibreOffice
	 */
	public $libreOfficeComponentName = 'libreOffice';
	/**
	 * ImageMagick component name
	 * @var string
	 */
	public $imagickComponentName = 'imagick';
	/**
	 * Name template for slide files, {d} would be replaced with slide number
	 *
	 * @var string
	 */
	public $fileNameTemplate = 'slide{d}';
	/**
	 * Slide file extension
	 *
	 * @var string
	 */
	public $convertedFilesExtension = 'jpg';
	/**
	 * Name of the property wich stores number of converted files
	 *
	 * @var string
	 */
	public $fileCountPropertyName = 'slide_count';
	/**
	 * Array of image resolutions, for example: full => 800x600
	 *
	 * @var array
	 */
	public $imageResolutions = array();

	/**
	 * Creates converted file itself name (without path) including version and extension.
	 *
	 * @param string $transformName Image transformation name
	 * @param integer $fileNumber Number of converted slide
	 * @param integer $fileVersion Presentation file version
	 * @param string $fileExtension Converted file extension
	 * @return string Converted file name
	 */
	public function getConvertedFileSelfName($transformName, $fileNumber, $fileVersion = null, $fileExtension = null)
	{
		$owner = $this->getOwner();

		$imageNamePrefix = str_replace('{d}', $fileNumber	, $this->fileNameTemplate);

		if (is_null($fileVersion))
		{
			$fileVersion = $this->getFileVersionCurrent();
		}
		if (is_null($fileExtension))
		{
			$fileExtension = $owner->getAttribute($this->getFileExtensionAttributeName());
		}
		return $this->getFileBaseName() . '_' . $transformName . '_' . $imageNamePrefix . '_' . $fileVersion . '.' . $fileExtension;
	}

	/**
	 * Creates the converted file name in the file storage.
	 * This name contains the sub directory, resolved by {@link subDirTemplate}.
	 *
	 * @param string $transformName Image transformation name
	 * @param integer $fileNumber Number of converted slide
	 * @param integer $fileVersion Presentation file version
	 * @param string $fileExtension Converted file extension
	 * @return string Converted file name
	 */
	public function getConvertedFileFullName($transformName, $fileNumber, $fileVersion = null, $fileExtension = null)
	{
		if (!$fileExtension)
		{
			$fileExtension = $this->convertedFilesExtension;
		}
		$fileName = $this->getConvertedFileSelfName($transformName, $fileNumber, $fileVersion, $fileExtension);
		$subDir = $this->getActualSubDir();
		if (!empty($subDir))
		{
			$fileName = $subDir . '/'/*DIRECTORY_SEPARATOR*/ . $fileName;
		}
		return $fileName;
	}
	
	/**
	 * Create/copy existing slides from $this model to the $newAuthoringModel
	 * @param LearningAuthoring $newAuthoringModel - The new model for which to
	 * create copies of the current presentation
	 */
	public function copyRemoteAuthoring($newAuthoringModel){
		$bucket = $this->getFileStorageBucket();
		
		$fileCountPropertyName = $this->fileCountPropertyName;
		
		// How much slides the current authoring has
		$slideCount = $this->owner->$fileCountPropertyName;
		
		// Copy each of the slides from the current authoring to the
		// new authoring in the cloud
		for ($i = 1; $i <= $slideCount; $i++)
		{
			
			// Make sure we copy the slide images in all resolutions
			foreach ($this->imageResolutions as $transformName => $imageResolution) {
				
				// Get the full path to the current slide image on the cloud
				// in the current resolution (minus the domain) for the existing authoring
				$oldFile = $this->getConvertedFileFullName($transformName, $i);
				
				// Get the full path to the current slide image on the cloud
				// in the current resolution (minus the domain) for the NEW authoring
				// This fill will be created/copied with contents of $oldFile
				$newFile = $newAuthoringModel->getConvertedFileFullName($transformName, $i);

				// Calls the S3 api with the old image (existing)
				// and the new one (not existing yet)
				$bucket->copyFileInternal($oldFile, $newFile);
			}
			
		}
	}

	/**
	 * Creates the file for the model from the source file and converts it.
	 * File version and extension are passed to this method.
	 *
	 * @param string $sourceFileName Source full file name.
	 * @param integer $fileVersion File version number.
	 * @param string $fileExtension File extension.
	 * @return boolean Success.
	 */
	protected function newFile($sourceFileName, $fileVersion, $fileExtension)
	{
		$fileStorageBucket = $this->getFileStorageBucket();
		$result = true;

		$convertedFiles = $this->convert($sourceFileName, $fileExtension);

		$fileFullName = $this->getFileFullName($fileVersion, $fileExtension);
		$copyResult = $fileStorageBucket->copyFileIn($sourceFileName, $fileFullName);

		$result = $result && $copyResult;

		//for ($i = 1; $i <= count($convertedFiles); $i++)
		natsort($convertedFiles);
		$i = 1;
		foreach($convertedFiles as $convertedFile)
		{
			//$convertedFile = $convertedFiles[$i - 1];

			foreach ($this->imageResolutions as $imageResolutionName => $imageResolution)
			{
				if (is_numeric($imageResolutionName))
				{
					$imageResolutionName = $imageResolution;
					$imageResolution = null;
				}

				$pathInfo = pathinfo($convertedFile);
				$filePath = $pathInfo['dirname'];
				$fileName = $pathInfo['filename'];
				$fileExtension = $pathInfo['extension'];
				$resizedFileFullName = $filePath . DIRECTORY_SEPARATOR . $fileName . $imageResolutionName . '.' . $fileExtension;

				if ($imageResolution == null)
				{
					copy($convertedFile, $resizedFileFullName);
				}
				else
				{
					$imagick = $this->imagickComponentName;
					Yii::app()->$imagick->resizeImage($convertedFile, $resizedFileFullName, $imageResolution);
				}

				$convertedFileFullName = $this->getConvertedFileFullName($imageResolutionName, $i, $fileVersion, $this->convertedFilesExtension);
				$copyResult = $fileStorageBucket->copyFileIn($resizedFileFullName, $convertedFileFullName);
				$result = $result && $copyResult;

				if (file_exists($convertedFileFullName))
				{
					unlink($convertedFileFullName);
				}
			}

			if (file_exists($convertedFile))
			{
				unlink($convertedFile);
			}
			$i++;
		}

		$this->updateSlideCount(count($convertedFiles));

		return $result;
	}

	/**
	 * Selects convert method for file
	 *
	 * @param string $sourceFileName Source full file name
	 * @param string $fileExtension File extension
	 * @return array Array of full converted file names
	 */
	protected function convert($sourceFileName, $fileExtension)
	{
		$convertedFiles = array();
		switch ($fileExtension)
		{
			case 'ppt':
			case 'pptx':
				$convertedFiles = $this->convertWithPowerPoint($sourceFileName);
				break;
			case 'odp':
				$convertedFiles = $this->convertWithLibreOffice($sourceFileName);
				break;
			case 'pdf':
				$convertedFiles = $this->convertPdf($sourceFileName);
				break;
		}


		return $convertedFiles;
	}

	/**
	 * Converts PowerPoint presentation to jpgs
	 *
	 * @param string $filePath Path to PowerPoint presentation
	 * @return array Array of full converted file names
	 */
	protected function convertWithPowerPoint($filePath)
	{
		$tempFilePath = $this->resolveTempFilePath();
		$imagesDir = $tempFilePath . DIRECTORY_SEPARATOR . uniqid(rand());
		$imagesPath = $imagesDir . '.jpg';

		$powerPoint = Yii::app()->getComponent($this->powerPointComponentName);
		$powerPoint->convertPresentation($filePath, $imagesPath);

		return CFileHelper::findFiles($imagesDir);
	}

	/**
	 * Converts LibreOffice presentation to jpgs
	 *
	 * @param string $filePath Path to LibreOffice presentation
	 * @return array Array of full converted file names
	 */
	protected function convertWithLibreOffice($filePath)
	{
		$tempFilePath = $this->resolveTempFilePath();
		$outputPath = $tempFilePath . DIRECTORY_SEPARATOR . uniqid(rand());

		$libreOffice = Yii::app()->getComponent($this->libreOfficeComponentName);
		$libreOffice->convertPresentation($filePath, $outputPath);

		return CFileHelper::findFiles($outputPath);
	}

	/**
	 * Converts pdf file to jpgs
	 *
	 * @param string $filePath Path to pdf file
	 * @return array Array of full converted file names
	 */
	protected function convertPdf($filePath)
	{
		$tempFilePath = $this->resolveTempFilePath();
		$outputPath = $tempFilePath . DIRECTORY_SEPARATOR . uniqid(rand());

		$imagick = Yii::app()->getComponent($this->imagickComponentName);
		$imagick->convertPdf($filePath, $outputPath);

		return CFileHelper::findFiles($outputPath);
	}

	/**
	 * Sets slide count property of the model
	 *
	 * @param integer $slideCount Slide count
	 */
	protected function updateSlideCount($slideCount)
	{
		if ($this->getUsingWebService()) {
			//if we are using web service, we don't need to phisically update data, since that is not located in this database
			$this->setLastNumConvertedSlides($slideCount);
		} else {
			$this->owner->updateByPk($this->owner->getPrimaryKey(), array($this->fileCountPropertyName => $slideCount));
		}
	}

	/**
	 * Generates the temporary file path
	 * and makes sure it exists.
	 * @throws CException if fails.
	 * @return string temporary full file path.
	 */
	protected function resolveTempFilePath() {
		$filePath = Yii::getPathOfAlias('application.runtime').DIRECTORY_SEPARATOR.get_class($this).DIRECTORY_SEPARATOR.get_class($this->getOwner());
		if (!file_exists($filePath)) {
			$oldUmask = umask(0);
			@mkdir($filePath, 0777, true);
			umask($oldUmask);
		}

		if ( !file_exists($filePath) || !is_dir($filePath) ) {
			throw new CException("Unable to resolve temporary file path: '{$filePath}'!");
		} elseif (!is_writable($filePath)) {
			throw new CException("Path: '{$filePath}' should be writeable!");
		}

		return $filePath;
	}

	/**
	 * Returns presentation file url if $fileNumber is null or jpg url of the slide
	 * with number $fileNumber
	 *
	 * @param string $transformName Image transformation name
	 * @param integer $fileNumber Number of the jpg slide file
	 * @return string File url
	 */
	public function getFileUrl($transformName = null, $fileNumber = null) {
		$fileStorageBucket = $this->getFileStorageBucket();

		if ($fileNumber && $transformName)
		{
			$fileFullName = $this->getConvertedFileFullName($transformName, $fileNumber);
		}
		else
		{
			$fileFullName = $this->getFileFullName();
		}

		$fileUrl = $fileStorageBucket->getFileUrl($fileFullName);
		$fileUrl = str_replace('\\', '/', $fileUrl);

		return $fileUrl;
	}

	/**
	 * Returns array of all jpg urls
	 *
	 * @param string $transformName Image transformation name
	 * @return array
	 */
	public function getJpgUrls($transfornName)
	{

		// Use Cached array of JPG Urls, if any 
		$cacheKey = md5('cached_jpg_urls_' . $this->owner->getPrimaryKey() . '_' . $transfornName);
		if (isset(Yii::app()->cache_authoring)) {
			$cached = Yii::app()->cache_authoring->get($cacheKey);
			if ($cached) {
				return $cached;
			}
		}
		
		$jpgUrls = array();
		$fileCountPropertyName = $this->fileCountPropertyName;
		$slideCount = $this->owner->$fileCountPropertyName;
		for ($i = 1; $i <= $slideCount; $i++)
		{
			$jpgUrls[] = $this->getFileUrl($transfornName, $i);
		}

		// Save JPG URLs in a cache
		if (isset(Yii::app()->cache_authoring)) {
			Yii::app()->cache_authoring->set($cacheKey, $jpgUrls);
		}
		
		return $jpgUrls;
	}

	/**
	 * Removes files associated with the owner model.
	 *
	 * @return boolean success.
	 */
	protected function unlinkFile()
	{
		$result = parent::unlinkFile();
		$fileStorageBucket = $this->getFileStorageBucket();

		$fileCountPropertyName = $this->fileCountPropertyName;
		$slideCount = $this->owner->$fileCountPropertyName;
		for ($i = 1; $i <= $slideCount; $i++)
		{
			foreach ($this->imageResolutions as $imageResolutionName => $imageResolution)
			{
				if (is_numeric($imageResolutionName))
				{
					$imageResolutionName = $imageResolution;
					$imageResolution = null;
				}

				$convertedFileName = $this->getConvertedFileFullName($imageResolutionName, $i);
				$deleteResult = $fileStorageBucket->deleteFile($convertedFileName);
				$result = $result && $deleteResult;
			}
		}

		return $result;
	}
}
