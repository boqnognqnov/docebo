<?php

class AuthoringTrackBehavior extends CBehavior {

	public $idCoursePropertyName = '';

	/**
	 * Declares events and the corresponding event handler methods.
	 * @return array events (array keys) and the corresponding event handler methods (array values).
	 */
	public function events() {
		return array(
			'onAfterSave' => 'afterSave',
		);
	}
	
	/**
	 * This event raises after owner saved.
	 * It saves data in learning_commontrack table.
	 * @param CEvent $event - event instance.
	 */
	public function afterSave($event) {
		$owner = $this->getOwner();
		$idTrack = $owner->getPrimaryKey();
		$idAuthoring = $owner->authoring_id;
		$idCourse = $owner->{$this->idCoursePropertyName};

		$org = LearningOrganization::model()->findByAttributes(array('idResource' => (int)$idAuthoring, 'objectType' => 'authoring', 'idCourse' => $idCourse));
		if (!$org)
			return false;


		// Adjust the object reference (in case this is a central repo LO)
		$org = $org->getMasterForCentralLo($owner->user_id);

		$commontrackStatus = ($owner->completion_status == 'completed') ? 'completed' : 'attempted';
		CommonTracker::track($org->idOrg, $owner->user_id, $commontrackStatus);
	}
}