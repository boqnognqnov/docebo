<?php


require_once dirname(__FILE__) . '/OAuthStoreMySQL.php';


class OAuthStoreCustomMySQL extends OAuthStoreMySQL
{
	/**
	 * The MySQL connection 
	 */
	protected $conn;

	
	public function getTokenAccessScope($requested_scope,$token) {
		$rs = $this->query_row_assoc('
				SELECT	ost_token as token,
						ost_scope as scope
				FROM oauth_server_token
				WHERE ost_token_type = \'access\'
				  AND ost_token      = \'%s\'
				', $token);
		
		if (empty($rs))
		{
			throw new OAuthException2('No server_token "'.$token);
		}
		
		return $rs["scope"];
	}
		
	
	// We store the scope associated to this token
	public function updateScope($scope,$token,$type) {
		$this->query('
					UPDATE oauth_server_token
					SET ost_scope = \'%s\'
					WHERE ost_token      = \'%s\'
					AND ost_token_type = \'%s\'
					', $scope, $token, $type);
		
	}
	
}


?>