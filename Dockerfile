FROM 426132435523.dkr.ecr.eu-west-1.amazonaws.com/docebo-git:70
WORKDIR /var/www/httpdocs

RUN git reset --hard && \
	git checkout maintenance/weekly-70 && \ 
	git pull && \
	cd lms/protected && \
	REDIS_RW=eu-rg01.glgchy.ng.0001.euw1.cache.amazonaws.com sh yiic-domain-db-migration.sh qa-ecs-hydra.docebosaas.com && \
        chown www-data.www-data -R /var/www/httpdocs/lmstmp
