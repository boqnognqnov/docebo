require.config({
	baseUrl: app7020Options.assetUrl + '/js',
	waitSeconds: 0,
	paths: {
		app: 'app/app_askTheExpert',
		angular: '/themes/spt/js/angularJs/angular',
		angularRoute: '/themes/spt/js/angularJs/angular-route',
		angularCookies: '/themes/spt/js/angularJs/angular-cookies.min',
		ddSlick: '/themes/spt/js/jquery.ddslick.min',
		angularInfiniteScroll: '/themes/spt/js/angularJs/ng-infinite-scroll.min',
		angularSanitize: '/themes/spt/js/angularJs/angular-sanitize.min'
	},
	shim: {
		angular: {
			exports: 'angular'
		},
		angularRoute: {
			exports: 'angular',
			deps: ['angular']
		},
		angularCookies: {
			exports: 'angular',
			deps: ['angular']
		},
		angularInfiniteScroll: {
			exports: 'infinite-scroll',
			deps: ['angular']
		},
		angularSanitize: {
			'exports': 'ngSanitize',
			'deps': ['angular']
		}
	},
	priority: [
		'angular',
		'angularRoute',
		'angularCookies',
		'angularInfiniteScroll',
		'angularSanitize',
		'app'
	]
});
define('jquery', [], function () {
	return jQuery;
});

require([
	'angular',
	'angularRoute',
	'angularCookies',
	'angularInfiniteScroll',
	'angularSanitize',
	'app',
], function () {
	angular.element(document).ready(function () {
		angular.bootstrap(document, ['app7020Questions']);
	});
});
