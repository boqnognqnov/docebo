define(function(){
	
	function HtmlToPlainTextFilter(){
		return function(input){
			var output = input ? String(input).replace(/<[^>]+>/gm, '') : '';
			return output;
		};
	}
	HtmlToPlainTextFilter.$inject = [];
	return HtmlToPlainTextFilter;
	
});