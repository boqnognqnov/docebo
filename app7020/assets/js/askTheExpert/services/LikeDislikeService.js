define(function () {

	function LikeDislikeService($http) {
		/**
		 * ENDPOINT for set like/dislike to Server
		 */
		this.ENDPOINT = 'like_answer';
		/**
		 * Method for set data by some options and endpoint
		 */
		this.setData = function (opt) {
			var opt = typeof opt !== 'undefined' ? opt : {};

			/* Prepare request params structure */
			var requestOptions = {
				endpoint: this.ENDPOINT,
				params: {
					id_answer: typeof opt.id !== 'undefined' ? opt.id : null,
					like_vector: typeof opt.type !== 'undefined' ? opt.type : null
				}
			};

			return $http.post('/lms/index.php?r=restdata', requestOptions);
		};
	}

	LikeDislikeService.$inject = ['$http'];
	return LikeDislikeService;

});