define(function () {

	function QuestionService($http, GetDataService) {
		/**
		 * ENDPOINT for set follow/unfollow to Server
		 */
		this.ENUM_ENDPOINT = {
			ENDPOINT_SAVE_QUESTION: "save_question",
			ENDPOINT_SINGLE_QUESTION: "single_question",
			ENDPOINT_SAVE_ANSWER: "save_answer",
			ENDPOINT_DELETE_ANSWER: "delete_answer",
			ENDPOINT_MARK_ANSWER: "mark_answer"
		};

		this.storages = {
			question: []
		};

		this.successCallback = function (success) {
			console.log("Success :: QuestionService: ", success);
		};

		this.errorCallback = function (error) {
			console.log("Error :: QuestionService: ", error);
			if(error.status === 400){
				window.location.href = 'index.php?r=site/index';
			}
		};

		/**
		 * Method for set data by some options and endpoint
		 */
		this.addQuestion = function (opt) {
			var opt = typeof opt !== 'undefined' ? opt : {};

			if (typeof opt.successCallback !== 'function') {
				opt.successCallback = this.successCallback;
			}

			if (typeof opt.errorCallback !== 'function') {
				opt.errorCallback = this.errorCallback;
			}

			/* Prepare request params structure */
			var requestOptions = {
				endpoint: this.ENUM_ENDPOINT.ENDPOINT_SAVE_QUESTION,
				params: {
					title: typeof opt.title !== 'undefined' ? opt.title : null,
					is_request: typeof opt.is_request !== 'undefined' ? opt.is_request : '',
					id_question: typeof opt.id_question !== 'undefined' ? opt.id_question : '',
					id_channel: typeof opt.id_channel !== 'undefined' ? opt.id_channel : '',
					id_content: typeof opt.id_content !== 'undefined' ? opt.id_content : '',
					tags: typeof opt['hidden-tags'] !== 'undefined' ? opt['hidden-tags'] : '',
				}
			};

			$http.post('/lms/index.php?r=restdata', requestOptions).then(function ($data) {
				if ($data.data.success) {
					if ($data.data.question) {
						if (GetDataService.getAllQuestions().length)
							GetDataService.getAllQuestions('items').unshift($data.data.question);
						++GetDataService.getAllQuestions('head').questions_count;
						if (GetDataService.getMyQuestions().length)
							GetDataService.getMyQuestions('items').unshift($data.data.question);
					} else {
						GetDataService.updateQuestionTitle({
							id_question: opt.id_question,
							title: opt.title
						});
					}
				}
				opt.successCallback($data);
			}, function ($error) {
				opt.errorCallback($error);
			});
		};

		this.getQuestionById = function (opt) {
			var $this = this;
			var response = {};

			if (typeof opt.id === 'undefined')
				return;

			var opt = typeof opt !== 'undefined' ? opt : {};

			if (typeof opt.successCallback !== 'function') {
				opt.successCallback = this.successCallback;
			}

			if (typeof opt.errorCallback !== 'function') {
				opt.errorCallback = this.errorCallback;
			}

			/* Prepare request params structure */
			var requestOptions = {
				endpoint: this.ENUM_ENDPOINT.ENDPOINT_SINGLE_QUESTION,
				params: {
					id_question: opt.id
				}
			};

			/* Try to get from DATA Factory */
			// TODO

			/* Get from server when not already getted */
			$http.post('/lms/index.php?r=restdata', requestOptions).then(function ($data) {
				angular.merge(response, $data.data);
				if (response.success) {
					var item = response.section.items[0];
					delete response.section.items;
					var head = response.section;
					$this.storages.question = [];
					$this.storages.question.push({head: head, item: item});
				}
				opt.successCallback($data);
			}, function ($error) {
				opt.errorCallback($error);
			});
		};

		this.getQuestion = function (param) {
			if (typeof param === 'string' && this.storages.question.length)
				return this.storages.question[0][param];
			return this.storages.question;
		};

		this.editAnswer = function (opt) {
			var $this = this;
			var response = {};

			if (typeof opt.id === 'undefined')
				return;

			var opt = typeof opt !== 'undefined' ? opt : {};

			if (typeof opt.successCallback !== 'function') {
				opt.successCallback = this.successCallback;
			}

			if (typeof opt.errorCallback !== 'function') {
				opt.errorCallback = this.errorCallback;
			}

			/* Prepare request params structure */
			var requestOptions = {
				endpoint: this.ENUM_ENDPOINT.ENDPOINT_SAVE_ANSWER,
				params: {
					id_answer: opt.id,
					title: opt.title
				}
			};

			$http.post('/lms/index.php?r=restdata', requestOptions).then(function ($data) {
				angular.merge(response, $data.data);
				opt.successCallback($data);
				if (response.success) {
					$this.storages.question[0].item.answers.forEach(function (answer) {
						if (answer.id_answer === opt.id)
							answer = response.answer;
						GetDataService.updateQuestionsLastAnswerById(response.question.question_id, response.question.answer);
					});
				}
			}, function ($error) {
				opt.errorCallback($error);
			});
		};

		this.deleteAnswer = function (opt) {
			var $this = this;
			var response = {};

			if (typeof opt.id === 'undefined')
				return;

			var opt = typeof opt !== 'undefined' ? opt : {};

			if (typeof opt.successCallback !== 'function') {
				opt.successCallback = this.successCallback;
			}

			if (typeof opt.errorCallback !== 'function') {
				opt.errorCallback = this.errorCallback;
			}
			/* Prepare request params structure */
			var requestOptions = {
				endpoint: this.ENUM_ENDPOINT.ENDPOINT_DELETE_ANSWER,
				params: {
					id_answer: opt.id
				}
			};

			$http.post('/lms/index.php?r=restdata', requestOptions).then(function ($data) {
				angular.merge(response, $data.data);
				if (response.success) {
					if ($this.storages.question.length !== 0) {
						$this.storages.question[0].item.answers.forEach(function (item, index) {
							if (item.id_answer === opt.id) {
								$this.storages.question[0].item.answers.splice(index, 1);
								return;
							}
						});
						$this.storages.question[0].head.answers_count = $this.storages.question[0].item.answers.length;
					}
					GetDataService.updateQuestionsLastAnswerById(response.question.question_id, response.question.answer);
				}
				opt.successCallback($data);
			}, function ($error) {
				opt.errorCallback($error);
			});
		};

		this.addAnswer = function (opt) {
			var $this = this;
			var response = {};

			if (typeof opt.id === 'undefined')
				return;

			var opt = typeof opt !== 'undefined' ? opt : {};

			if (typeof opt.successCallback !== 'function') {
				opt.successCallback = this.successCallback;
			}

			if (typeof opt.errorCallback !== 'function') {
				opt.errorCallback = this.errorCallback;
			}

			/* Prepare request params structure */
			var requestOptions = {
				endpoint: this.ENUM_ENDPOINT.ENDPOINT_SAVE_ANSWER,
				params: {
					id_question: opt.id,
					title: opt.title
				}
			};

			$http.post('/lms/index.php?r=restdata', requestOptions).then(function ($data) {
				angular.merge(response, $data.data);
				if (response.success) {
					$this.storages.question[0].item.answers.unshift(response.answer);
					$this.storages.question[0].head.answers_count = $this.storages.question[0].item.answers.length;
					GetDataService.updateQuestionsLastAnswerById(response.question.question_id, response.question.answer, $this.storages.question[0].item.answers.length);
				}
				opt.successCallback($data);
			}, function ($error) {
				opt.errorCallback($error);
			});
		};

		this.markAnswer = function (opt) {
			var $this = this;
			var response = {};

			if (typeof opt.id_question === 'undefined' || typeof opt.id_answer === 'undefined')
				return;

			var opt = typeof opt !== 'undefined' ? opt : {};

			if (typeof opt.successCallback !== 'function') {
				opt.successCallback = this.successCallback;
			}

			if (typeof opt.errorCallback !== 'function') {
				opt.errorCallback = this.errorCallback;
			}

			/* Prepare request params structure */
			var requestOptions = {
				endpoint: this.ENUM_ENDPOINT.ENDPOINT_MARK_ANSWER,
				params: {
					id_question: opt.id_question,
					id_answer: opt.id_answer
				}
			};

			$http.post('/lms/index.php?r=restdata', requestOptions).then(function ($data) {
				angular.merge(response, $data.data);
				if (response.success) {
					$this.storages.question[0].item.answers.forEach(function (item, index) {
						if (item.id_answer === opt.id_answer) {
							item.is_best_answer = !item.is_best_answer;
							return;
						} else {
							item.is_best_answer = false;
						}
					});
					GetDataService.updateQuestionsLastAnswerById(opt.id_question, response.question.answer);
				}
				opt.successCallback($data);
			}, function ($error) {
				opt.errorCallback($error);
			});
		};
	}

	QuestionService.$inject = ['$http', 'GetDataService'];
	return QuestionService;

});