define(function(){
	
	function FollowUnfollowService($http){
		/**
		 * ENDPOINT for set follow/unfollow to Server
		 */
		this.ENDPOINT = 'follow_question';
		/**
		 * Method for set data by some options and endpoint
		 */
		this.setData = function (opt) {
			var opt = typeof opt !== 'undefined' ? opt : {};

			/* Prepare request params structure */
			var requestOptions = {
				endpoint: this.ENDPOINT,
				params: {
					id_question: typeof opt.id !== 'undefined' ? opt.id : null,
				}
			};

			return $http.post('/lms/index.php?r=restdata', requestOptions);
		};
	}
	
	FollowUnfollowService.$inject = ['$http'];
	return FollowUnfollowService;
	
});