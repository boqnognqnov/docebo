define(function () {

	function DeleteQuestionService($http) {
		/**
		 * ENDPOINT for set like/dislike to Server
		 */
		this.ENDPOINT = 'delete_question';
		/**
		 * Method for set data by some options and endpoint
		 */
		this.setData = function (opt) {
			var opt = typeof opt !== 'undefined' ? opt : {};

			/* Prepare request params structure */
			var requestOptions = {
				endpoint: this.ENDPOINT,
				params: {
					id_question: typeof opt.id !== 'undefined' ? opt.id : null,
				}
			};

			return $http.post('/lms/index.php?r=restdata', requestOptions);
		};
	}

	DeleteQuestionService.$inject = ['$http'];
	return DeleteQuestionService;

});