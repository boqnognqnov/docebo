define(function () {

	function GetFiltersService() {
		var self = {};
		
		/**
		 * Predefine arrays for filters tab data
		 */
		self.storages = {
			filters: [{
				name: 'Sort:',
				typename: 'sort',
				spantype: 'span3',
				options: [
					{
						id: 0,
						value: 0,
						type: 'radio',
						label: 'Date',
						selected: true
					},
					{
						id: 1,
						value: 1,
						type: 'radio',
						label: 'Answers number',
						selected: false
					},
					{
						id: 2,
						value: 2,
						type: 'radio',
						label: 'Views',
						selected: false
					}
				]
			},
			{
				name: 'Show:',
				typename: 'show',
				spantype: 'span3',
				options: [
					{
						id: 0,
						value: 0,
						type: 'radio',
						label: 'All questions',
						selected: true
					},
					{
						id: 1,
						value: 1,
						type: 'radio',
						label: 'Open questions',
						selected: false
					},
					{
						id: 2,
						value: 2,
						type: 'radio',
						label: 'Closed questions',
						selected: false
					},
					{
						id: 3,
						value: 3,
						type: 'radio',
						label: 'New questions',
						selected: false
					}
				]
			},
			{
				name: 'Question type:',
				typename: 'qtype',
				spantype: 'span6',
				options: [
					{
						id: 0,
						value: 0,
						type: 'radio',
						label: 'All types',
						selected: true
					},
					{
						id: 1,
						value: 1,
						type: 'radio',
						label: 'Linked to users contributed assets',
						subtitle: 'Videos, documents, links and others',
						selected: false
					}
					//{
					//	id: 2,
					//	value: 2,
					//	type: 'radio',
					//	label: 'Linked to Learning Objects',
					//	subtitle: 'Formal learning objects that are part of an E-Learning course',
					//	selected: false
					//}
				]
			}],
			selectedfilters: []
		};

		

		/**
		 * Data getters
		 */
		self.getFilters = function () {
			return self.storages.filters;
		};
		self.getSelectedFilters = function () {
			return self.storages.selectedfilters;
		};
		
		/**
		 * Data setters
		 */
		self.setSelectedFilters = function (selectedfilters) {
			self.storages.selectedfilters = selectedfilters;
		}
		self.setFilters = function (filters) {
			self.storages.filters = filters;
		}

		return self;
	}
	GetFiltersService.$inject = [];
	return GetFiltersService;

});