define(function () {

	function GetDataService($q, $http) {
		var self = {};
		/**
		 * ENUM with available endpoints
		 */
		self.ENUM_ENDPOINT = {
			ALL_QUESTIONS: "all_questions",
			MY_QUESTIONS: "my_questions",
			MY_ANSWERS: "my_answers",
			FOLLOWING: "following",
			DELETE_QUESTION : "delete_question"
		};

		/**
		 * Predefine arrays for every tab data
		 * Variables name need setted same as ENUM_ENDPOINT values
		 */
		self.storages = {
			all_questions: [],
			my_questions: [],
			my_answers: [],
			following: []
		};

		/**
		 * Method for getting data by some options and endpoint
		 */
		self.getData = function (opt) {
			var opt = typeof opt !== 'undefined' ? opt : {};
			var requestParams = {};

			if (typeof opt.successCallback !== 'function') {
				opt.successCallback = function (success) {
					console.log("Success :: GetDataService: ", success);
				};
			}

			if (typeof opt.errorCallback !== 'function') {
				opt.errorCallback = function (error) {
					console.log("Error :: GetDataService: ", error);
				};
			}
			/* Dublicate variables before remove */
			angular.merge(requestParams, opt);

			/* Remove some params from request object */
			delete requestParams.endpoint;
			delete requestParams.successCallback;
			delete requestParams.errorCallback;

			/* Prepare request params structure */
			var requestOptions = {
				endpoint: typeof opt.endpoint !== 'undefined' ? opt.endpoint : null,
				params: requestParams
			};

			$http.post('/lms/index.php?r=restdata', requestOptions)
					.then(function ($response) {
						if ($response.data.success) {
							var items = $response.data.section.items;
							delete $response.data.section.items;
							var head = $response.data.section;

							/* initial load */
							if (!opt.from || !opt.count){
								self.storages[opt.endpoint] = [];
								self.storages[opt.endpoint].push({head: head, items: items});
							}
							
							/* on infinite scroll */
							if (opt.from && opt.count)
								items.forEach(function (item) {
									self.storages[opt.endpoint][0].items.push(item);
								});
						}
						opt.successCallback($response);
					}, function ($response) {
						opt.errorCallback($response);
					});
		};

		/**
		 * Data getters
		 */
		self.getAllQuestions = function (param) {
			if (typeof param === 'string' && self.storages.all_questions.length)
				return self.storages.all_questions[0][param];
			return self.storages.all_questions;
		};
		self.getMyQuestions = function (param) {
			if (typeof param === 'string' && self.storages.my_questions.length)
				return self.storages.my_questions[0][param];
			return self.storages.my_questions;
		};
		self.getMyAnswers = function (param) {
			if (typeof param === 'string' && self.storages.my_answers.length)
				return self.storages.my_answers[0][param];
			return self.storages.my_answers;
		};
		self.getFollowing = function (param) {
			if (typeof param === 'string' && self.storages.following.length)
				return self.storages.following[0][param];
			return self.storages.following;
		};
		self.getFollowingById = function (id) {
			if (typeof id !== 'undefined')
				self.getFollowing('items').forEach(function (q) {
					if (q.question_id === parseInt(id))
						return q;
				});
			return false;
		};

		self.updateQuestionsFollow = function (data) {
			var pendingQuestion = null;
			var readyToAddInFollowing = (!self.getFollowingById(data.id_question) && data.followed === true);

			/* Loop all storages and apply changes of all */
			angular.forEach(self.storages, function (storage, key) {
				if (storage.length) {
					storage[0].items.forEach(function (q, index) {
						if (q.question_id === parseInt(data.id_question)) {

							/* If unfollow question from following tab remove this question from list */
							if (key === 'following' && data.followed === false) {
								storage[0].items.splice(index, 1);
								return;
							}

							/* Make question follow/unfollow */
							q.is_following = data.followed;
							q.actions.forEach(function (action) {
								/* Change follow label inside dropdown for this question */
								if (action.action_uid === 'follow')
									action.label = data.followLabel;
							});

							/* Grab first finded after upside changes */
							if (pendingQuestion === null)
								pendingQuestion = q;
						}
					});
				}
			});

			/**
			 * After follow changed in all storages
			 * check if question is followed and not present into following tab
			 * adding into storage
			 */
			if (readyToAddInFollowing && self.getFollowing().length) {
				self.getFollowing('items').push(pendingQuestion);
			}
		};

		self.updateQuestionsLastAnswerById = function (idQuestion, answerObj, answersCount) {
			var total_number_answers;
			
			/* Loop all storages and apply changes of all */
			angular.forEach(self.storages, function (storage, key) {
				if (storage.length) {
					storage[0].items.forEach(function (q, index) {
						if (q.question_id === parseInt(idQuestion)) {
							q.answer = answerObj;
							q.have_best_answer = (typeof answerObj !== 'undefined') ? answerObj.is_best_answer : false;
							if(typeof answersCount === 'undefined'){
								total_number_answers = (q.total_number_answers) ? q.total_number_answers-1 : 0;
							}else{
								total_number_answers = answersCount;
							}
							q.total_number_answers = total_number_answers;
						}
						if(key === 'my_answers' && answerObj.length === 0){
							storage[0].items.splice(index, 1);
						}
					});
				}
			});
		};
		
		self.updateQuestionsAfterDelete = function (data) {

			/* Loop all storages and apply changes of all */
			angular.forEach(self.storages, function (storage, key) {
				if (storage.length) {
					/* Update counters */
					if(storage[0].head.questions_count){
						--storage[0].head.questions_count;
					}
					storage[0].items.forEach(function (q, index) {
						if (q.question_id === parseInt(data.id_question)) {
							storage[0].items.splice(index, 1);
						}
					});
				}
			});
		};
		
		self.updateQuestionTitle = function(data){
			/* Loop all storages and apply changes of all */
			angular.forEach(self.storages, function (storage, key) {
				if (storage.length) {
					storage[0].items.forEach(function (q, index) {
						if (q.question_id === parseInt(data.id_question)) {
							q.question = data.title;
						}
					});
				}
			});
		};

		return self;
	}
	GetDataService.$inject = ['$q', '$http'];
	return GetDataService;

});