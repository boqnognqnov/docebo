/* global $scope, angular */

define(['./../modules/breadcrumps'], function (breadcrumps) {
	function Run($rootScope, $templateRequest, $location) {
		
		$templateRequest('index.php?r=askTheExpert/view&view_name=templates.confirmDelete');
		
		/* Toggle navigation from tabs to back button and over */
		$rootScope.navbarClass = '';
		$rootScope.breadcrumb_question = false;
		$rootScope.$on("$routeChangeStart", function (event, next, current) {
			if (next) {
				switch (next.originalPath) {
					case '/question/:id':
						$rootScope.navbarClass = 'show-back';
						$rootScope.prevUrl = current ? '#' + current.originalPath : '#/';
						$rootScope.breadcrumb_question = false;
						break;
					case '/allQuestions/:method':
						$rootScope.addQuestion = 'openDialog';
						$location.path(current ? current.originalPath : '/');
						angular.element('#menu .second-menu').switchClass('open', 'close');
						$rootScope.breadcrumb_question = false;
						break;
					default:
						$rootScope.navbarClass = '';
						$rootScope.prevUrl = '#/';
						$rootScope.breadcrumb_question = false;
						break;
				}
			}
		});
		$rootScope.$watch('breadcrumb_question', function(newValue){
			new breadcrumps({
				type: (newValue) ? 'question' : '',
				breadcrumps: newValue
			});
		});
	}
	Run.$inject = ['$rootScope', '$templateRequest', '$location'];
	return Run;
});