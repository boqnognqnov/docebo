define(function () {

	function MyQuestionsController($scope, GetDataService, $timeout) {
		/* use this to show/hide some partials from question template */
		$scope.questionAllow = {
			footer: [
				'related',
				'author',
				'views',
				'time',
				'answers'
			]
		};
		/* Stored data from service */
		$scope.head = GetDataService.getMyQuestions('head');
		$scope.items = GetDataService.getMyQuestions('items');
		$scope.openClose = '';

		$scope.init = function () {
			if (!$scope.items.length) {
				GetDataService.getData({
					endpoint: GetDataService.ENUM_ENDPOINT.MY_QUESTIONS,
					successCallback: function (data) {
						$scope.head = GetDataService.getMyQuestions('head');
						$scope.items = GetDataService.getMyQuestions('items');
					}
				});
			}
		};

		$scope.busyInfinite = false;

		$scope.runInfinite = function () {
			if (!$scope.items.length || $scope.head.questions_count == $scope.items.length)
				return false;

			$scope.busyInfinite = true;
			GetDataService.getData({
				endpoint: GetDataService.ENUM_ENDPOINT.MY_QUESTIONS,
				from: $scope.items.length,
				count: 5,
				successCallback: function () {
					$timeout(function () {
						$scope.busyInfinite = false;
					}, 1000);
				}
			});
		}

		/**
		 * 
		 * @returns {Number|$c}
		 */
		$scope.foundOpenQuestions = function () {
			var $c = 0;
			angular.forEach($scope.items, function (value, index) {
				if (angular.isDefined(value.have_best_answer) && value.have_best_answer === false) {
					$c++;
				}
			});

			return $c;
		}
 	}

	MyQuestionsController.$inject = ['$scope', 'GetDataService', '$timeout'];
	return MyQuestionsController;

});