define(function () {

	function FollowingController($scope, GetDataService, $timeout) {
		/* use this to show/hide some partials from question template */
		$scope.questionAllow = {
			footer: [
				'related',
				'author',
				'views',
				'time',
				'answers'
			]
		};
		/* Stored data from service */
		$scope.head = GetDataService.getFollowing('head');
		$scope.items = GetDataService.getFollowing('items');

		$scope.init = function () {
			if (!$scope.items.length) {
				GetDataService.getData({
					endpoint: GetDataService.ENUM_ENDPOINT.FOLLOWING,
					successCallback: function (data) {
						$scope.head = GetDataService.getFollowing('head');
						$scope.items = GetDataService.getFollowing('items');
					}
				});
			}
		};

		$scope.busyInfinite = false;

		$scope.runInfinite = function () {
			if (!$scope.items.length || $scope.head.questions_count == $scope.items.length)
				return false;

			$scope.busyInfinite = true;
			GetDataService.getData({
				endpoint: GetDataService.ENUM_ENDPOINT.FOLLOWING,
				from: $scope.items.length,
				count: 5,
				successCallback: function () {
					$timeout(function(){
						$scope.busyInfinite = false;
					}, 1000);
				}
			});
		}
	}

	FollowingController.$inject = ['$scope', 'GetDataService', '$timeout'];
	return FollowingController;

});