define(['../../modules/tinymce'], function (tinymce) {

	function QuestionController($scope, $rootScope, $routeParams, QuestionService, FollowUnfollowService, GetDataService, AJAX, $sce) {
		var mce = new tinymce;
		$scope.head = [];
		$scope.question = [];
		$scope.params = [];

		$scope.showRelated = true;

		/* Get initial data and store to storage */
		/**
		 * Inital doing
		 * @param {Boolean} hasRelated Swith ON/OFF the related content 
		 * @param {Boolean} hasTags Swith ON/OFF the related tags 
		 * @returns {undefined}
		 */
		$scope.init = function (hasRelated, hasTags) {

			if (typeof (hasRelated) !== 'undefined') {
				$scope.showRelated = hasRelated;
			}

			if (typeof (hasTags) !== 'undefined') {
				$scope.showTags = hasTags;
			}

			QuestionService.getQuestionById({
				id: $routeParams.id,
				successCallback: function () {
					$scope.head = QuestionService.getQuestion('head');
					$scope.question = QuestionService.getQuestion('item');
					$rootScope.breadcrumb_question = $scope.question.question;
					$scope.params = {
						logged_user: $scope.head.logged_user,
						owner_id: $scope.question.owner_id
					};
				}
			});
		};

		/* Make question follow / unfollow */
		var followBusy = false;
		$scope.following = function () {
			/* prevent multiple clicking */
			if (followBusy)
				return;

			followBusy = true;
			var $this = this;
			FollowUnfollowService.setData({
				id: $this.question.question_id
			}).then(function ($data) {
				followBusy = false;
				if ($data.data.success === true) {
					$this.question.is_following = $data.data.followed;
					GetDataService.updateQuestionsFollow($data.data);
				}
			}, function () {
				followBusy = false;
			});
		};

		/* Scroll to bottom of the page */
		$scope.gotoAnswerNow = function () {
			$("html, body").animate({scrollTop: $(document).height() - $(window).height()});
		};

		/* Edit answer */
		$scope.$on('dropdown.edit', function (e, data) {
			var generateClass = 'build-tinymce-' + e.targetScope.$id;
			var answerContainer = data.target.closest('.content-wrapper').find('[build-tinymce]');
			if (answerContainer.hasClass(generateClass))
				return;

			answerContainer.addClass(generateClass);
			mce.build('.' + generateClass, {
				plugins: ["moxiemanager image link autoresize mention"],
				setup: function (ed) {
					ed.addButton('custom-save', {
						text: Yii.t('app7020', 'Save'),
						classes: 'custom-save-wrapper',
						onclick: function () {
							this.disabled(true);
							QuestionService.editAnswer({
								id: e.targetScope.$parent.item.id_answer,
								title: ed.getContent(),
								successCallback: function ($data) {
									ed.destroy();
									answerContainer.removeClass(generateClass);
								}
							});
						}
					});
				},
				mentions: {
					delay: 200,
					insert: function (item) {
						return '<span data-userId=' + item.id + ' class="tagged-name>@' + item.name + '</span>';
					},
					render: function (item) {
						var avatar, firstname, lastname, name;
						if (item.avatar.length > 0) {
							avatar = '<img src=' + item.avatar + '>';
						} else {
							firstname = item.firstname.substring(0, 1).toUpperCase();
							lastname = item.lastname.substring(0, 1).toUpperCase();
							avatar = '<span class="circle"><span class="text">' + firstname + lastname + '</text></span>';
						}

						if (item.isExpert) {
							name = '<span class="expert">' + item.name + ' <span>' + Yii.t('app7020', 'Expert') + '</span></span>';
						} else {
							name = '<span>' + item.name + '</span>';
						}

						return '<li class="mention-autocomplete">' +
								'<a href="javascript:;"> \n\
										<span class="avatar">' + avatar + '</span>'
								+ name +
								'<span> (' + item.username + ')</span>\n\
									</a>' +
								'</li>';
					},
					source: function (query, process, delimiter) {
						if (delimiter === '@' && query.length >= 1) {
							AJAX.post(app7020Options.AskTheExpertController + '/AxGetUsers',
									{
										name: query,
										id_question: $scope.question.question_id
									}).then(function (response) {
								$scope.mentions = response;
								process($scope.mentions.data)
							});
						}
					}
				}
			});
		});

		/* Add answer*/
		mce.build('textarea.add-new-answer', {
			plugins: ["moxiemanager image link autoresize mention"],
			setup: function (ed) {
				ed.addButton('custom-save', {
					text: Yii.t('app7020', 'Save'),
					classes: 'custom-save-wrapper',
					onclick: function () {
						var thisButton = this;
						thisButton.disabled(true);
						QuestionService.addAnswer({
							id: $scope.question.question_id,
							title: ed.getContent(),
							successCallback: function ($data) {
								ed.setContent('');
								thisButton.disabled(false);
							},
							errorCallback: function () {
								thisButton.disabled(false);
							}
						});
					}
				});
			},
			mentions: {
				delay: 200,
				insert: function (item) {
					return '<span class="tagged-name userId-' + item.id +'">@' + item.name + '</span>';
				},
				render: function (item) {
					var avatar, firstname, lastname, name;
					if (item.avatar.length > 0) {
						avatar = '<img src=' + item.avatar + '>';
					} else {
						firstname = item.firstname.substring(0, 1).toUpperCase();
						lastname = item.lastname.substring(0, 1).toUpperCase();
						avatar = '<span class="circle"><span class="text">' + firstname + lastname + '</text></span>';
					}

					if (item.isExpert) {
						name = '<span class="expert">' + item.name + ' <span>' + Yii.t('app7020', 'Expert') + '</span></span>';
					} else {
						name = '<span>' + item.name + '</span>';
					}

					return '<li class="mention-autocomplete">' +
							'<a href="javascript:;"> \n\
										<span class="avatar">' + avatar + '</span>'
							+ name +
							'<span> (' + item.username + ')</span>\n\
									</a>' +
							'</li>';
				},
				source: function (query, process, delimiter) {
					if (delimiter === '@' && query.length >= 1) {
						AJAX.post(app7020Options.AskTheExpertController + '/AxGetUsers',
								{
									name: query,
									id_question: $scope.question.question_id
								}).then(function (response) {
							$scope.mentions = response;
							process($scope.mentions.data)
						});
					}
				}
			}
		});

		var markAsBestBusy = false;
		$scope.$on('answer.mark', function (e, data) {
			/* prevent multiple clicking */
			if (markAsBestBusy)
				return;
			var $this = this;
			markAsBestBusy = true;
			QuestionService.markAnswer({
				id_question: e.targetScope.$parent.question.question_id,
				id_answer: data.item.id_answer,
				successCallback: function ($data) {
					markAsBestBusy = false;
				},
				errorCallback: function () {
					markAsBestBusy = false;
				}
			});
		});
        $scope.to_trusted = function(html_code){
            return $sce.trustAsHtml(html_code);
        }
	}

	QuestionController.$inject = ['$scope', '$rootScope', '$routeParams', 'QuestionService', 'FollowUnfollowService', 'GetDataService', 'AJAX', '$sce'];
	return QuestionController;

});