define(function () {

	function FiltersController($scope, GetFiltersService) {
		$scope.filters = GetFiltersService.getFilters();

		$scope.selectedfilters = GetFiltersService.getSelectedFilters();
	}

	FiltersController.$inject = ['$scope', 'GetFiltersService'];
	return FiltersController;

});