define(function () {

	function MyAnswersController($scope, GetDataService, $timeout) {
		/* use this to show/hide some partials from question template */
		$scope.questionAllow = {
			title_prefix: true,
			footer: [
				'time',
				'likes'
			]
		};
		/* Stored data from service */
		$scope.head = GetDataService.getMyAnswers('head');
		$scope.items = GetDataService.getMyAnswers('items');

		$scope.title_prefix = '';

		$scope.init = function () {
			if (!$scope.items.length) {
				GetDataService.getData({
					endpoint: GetDataService.ENUM_ENDPOINT.MY_ANSWERS,
					successCallback: function (data) {
						$scope.head = GetDataService.getMyAnswers('head');
						$scope.items = GetDataService.getMyAnswers('items');
					}
				});
			}
		};

		$scope.busyInfinite = false;

		$scope.runInfinite = function () {
			if (!$scope.items.length || $scope.head.questions_count == $scope.items.length)
				return false;

			$scope.busyInfinite = true;
			GetDataService.getData({
				endpoint: GetDataService.ENUM_ENDPOINT.MY_ANSWERS,
				from: $scope.items.length,
				count: 5,
				successCallback: function () {
					$timeout(function(){
						$scope.busyInfinite = false;
					}, 1000);
				}
			});
		}

	}

	MyAnswersController.$inject = ['$scope', 'GetDataService', '$timeout'];
	return MyAnswersController;

});