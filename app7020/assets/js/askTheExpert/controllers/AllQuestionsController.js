define(function () {
    function AllQuestionsController($scope, GetDataService, $timeout, $routeParams) {
        /* use this to show/hide some partials from question template */
        $scope.questionAllow = {
            footer: [
                'related',
                'author',
                'views',
                'time',
                'answers'
            ]
        };
        /* Stored data from service */
        $scope.head = GetDataService.getAllQuestions('head');
        $scope.items = GetDataService.getAllQuestions('items');

        $scope.init = function () {
            if (!$scope.items.length) {
                GetDataService.getData({
                    endpoint: GetDataService.ENUM_ENDPOINT.ALL_QUESTIONS,
                    successCallback: function (data) {
                        $scope.head = GetDataService.getAllQuestions('head');
                        $scope.items = GetDataService.getAllQuestions('items');
                    }
                });
            }
        }

        $scope.busyInfinite = false;


        $scope.runInfinite = function () {
            if (!$scope.items.length || $scope.head.questions_count == $scope.items.length)
                return false;

            var filters = [];

            if ($scope.selectedfilters !== 'undefined') {
                filters = $scope.selectedfilters;
            }

            $scope.busyInfinite = true;
            GetDataService.getData({
                endpoint: GetDataService.ENUM_ENDPOINT.ALL_QUESTIONS,
                from: $scope.items.length,
                count: 5,
                options: JSON.stringify(filters),
                successCallback: function () {
                    $timeout(function () {
                        $scope.busyInfinite = false;
                    }, 1000);
                }
            });
        }

    }

    AllQuestionsController.$inject = ['$scope', 'GetDataService', '$timeout', '$routeParams'];
    return AllQuestionsController;
});