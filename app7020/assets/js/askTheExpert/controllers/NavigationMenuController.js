define(['../../modules/appendCSS', 'angular'], function (css, angular) {
	new css(app7020Options.assetUrl + '/css/mce_mention/autocomplete.css');
	new css(app7020Options.assetUrl + '/css/mce_mention/rte-content.css');
	
	function NavigationMenuController($rootScope, QuestionService, $http, $scope) {
		/* Submit new Question form */
		$scope.addQuestionData = {};
		$scope.questionSubmitDisabled = false;
		$scope.questionSubmit = function (questionForm, target) {
			var el = angular.element(target.currentTarget);
			var form = el.closest('form');
			$scope.questionSubmitDisabled = true;

			var fromData = form.serializeArray();
			var prepareRequestData = {
				successCallback: function ($data) {
					$scope.questionSubmitDisabled = false;
					form.find('.content-wrapper dd').removeClass('errorStyle');
					form.find('.error-text').remove();
					if ($data.data.success !== true) {
						angular.forEach($data.data.errors, function (value, key) {
							if (key === 'title') {
								form.find('.content-wrapper .question').addClass('errorStyle');
								value.forEach(function (errorValue) {
									form.find('.content-wrapper .question > .wrapper').prepend('<p class="error-text">' + errorValue + '</p>');
								});
							}
							if (key === 'channel') {
								form.find('.content-wrapper .assignChannel').addClass('errorStyle');
								value.forEach(function (errorValue) {
									form.find('.content-wrapper .assignChannel > .wrapper').prepend('<p class="error-text">' + errorValue + '</p>');
								});
							}
						});
					} else {
						if($('.add-question-dialog #add-question-dialog').length)
						$('.add-question-dialog #add-question-dialog').dialog2('close');
					}
				}
			};

			fromData.forEach(function (data) {
				prepareRequestData[data.name] = data.value;
			});
			QuestionService.addQuestion(prepareRequestData);
		};
	}

	angular.element(document).on('dialog2.opened', '#add-question-dialog', function () {
		/* Reset checkbox to unchecked */
		$('input[name="is_request"]').prop('checked', false).trigger('change');
		if($('.assignChannel .headline .required').hasClass('allways')){
			$('.assignChannel .headline .required').show();
			$('.assignChannel .channelsSelect').attr('required', '');
		}

		/* Reset tags */
		$('#add-question-dialog input[name="hidden-tags"]').val('');

		/* Reset errors */
		$(this).find('.content-wrapper dd').removeClass('errorStyle');
		$(this).find('.error-text').remove();
	});

	angular.element(document).on('change', '#add-question-dialog input[name="is_request"]', function () {
		if ($(this).is(':checked')) {
			$('#add-question-dialog input[name="is_request"]').prop('checked', true);
			if($('.assignChannel .headline .required').hasClass('allways') == false) {
				$('.assignChannel .headline .required').show();
				$('.assignChannel .channelsSelect').attr('required', '');
			}
		} else {
			$('#add-question-dialog input[name="is_request"]').prop('checked', false);
			if($('.assignChannel .headline .required').hasClass('allways') == false) {
				$('.assignChannel .headline .required').hide();
				$('.assignChannel .channelsSelect').removeAttr('required');
			}
		}
	});

	angular.element(document).on('click', '#add-question-dialog .dialog-close', function () {
		$('.add-question-dialog #add-question-dialog').dialog2('close');
	});

	angular.element(document).on('click', '.fa-sliders', function () {
		if ($('#filtersBox').is(":visible")) {
			$('#filtersBox').hide();
		} else {
			$('#filtersBox').show();
		}
	});
	
	NavigationMenuController.$inject = ['$rootScope', 'QuestionService', '$http', '$scope'];
	return NavigationMenuController;

});