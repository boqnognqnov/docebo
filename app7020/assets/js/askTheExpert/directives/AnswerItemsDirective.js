define(function(){
	
	function AnswerItemsDirective($sce){
		
		return {
			restrict: 'A',
			templateUrl: 'index.php?r=askTheExpert/view&view_name=templates.answerTemplate',
			scope: {
				items: '=',
				itemsFilter: '=',
				params: '='
			},
			link: function(scope){
				scope.markAsBest = function(){
					scope.$emit('answer.mark', this);
				}
                scope.to_trusted = function(html_code){
                    return $sce.trustAsHtml(html_code);
                }
			}
		};
		
	}
	
	AnswerItemsDirective.$inject = ['$sce'];
	return AnswerItemsDirective;
	
});