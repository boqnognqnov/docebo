define(function () {

	function FiltersDirective(GetDataService, GetFiltersService) {
		return {
			restrict: 'E',
			replace: true,
			controller: 'FiltersController',
			templateUrl: 'index.php?r=askTheExpert/view&view_name=directives.filters',
			scope: {
				filters: '=',
				selectedfilters: '=',
			},
			link: function (scope, iElem, iAttrs) {

				//apply filters
				iElem.on('click', '.apply-filters', function () {
					scope.selectedfilters = [];
					jQuery.each(scope.filters, function (index, value) {
						var filterValue = $("input[name^='" + value.typename + "']:checked").val();
						var optionName = $("input[name^='" + value.typename + "']:checked").attr("data-label");
						if (parseInt(filterValue) > 0) {
							scope.selectedfilters.push({
								name: value.typename,
								value: filterValue,
								groupname: value.name,
								optionname: optionName
							});
						}
						;

						jQuery.each(value.options, function (optionIndex, option) {
							if (parseInt(option.value) === parseInt(filterValue)) {
								scope.filters[index].options[optionIndex].selected = true;
							} else {
								scope.filters[index].options[optionIndex].selected = false;
							}
						});


					});
					GetFiltersService.setSelectedFilters(scope.selectedfilters);
					GetFiltersService.setFilters(scope.filters);
					GetDataService.storages['all_questions'] = [];
					scope.$parent.busyInfinite = true;
					GetDataService.getData({
						endpoint: GetDataService.ENUM_ENDPOINT.ALL_QUESTIONS,
						options: JSON.stringify(scope.selectedfilters),
						successCallback: function () {
							scope.$parent.head = GetDataService.getAllQuestions('head');
							scope.$parent.items = GetDataService.getAllQuestions('items');
							scope.$parent.busyInfinite = false;
						}
					});
					$('#filtersBox').hide();

				});

				//close filters box
				iElem.on('click', '.close-dialog', function () {
					$('#filtersBox').hide();
				});

				//clear filters
				iElem.on('click', '.clearFilters', function () {
					scope.selectedfilters = [];
					GetFiltersService.setSelectedFilters([]);

					jQuery.each(scope.filters, function (index, value) {
						jQuery.each(value.options, function (optionIndex, option) {
							if (optionIndex === 0) {
								scope.filters[index].options[optionIndex].selected = true;
							} else {
								scope.filters[index].options[optionIndex].selected = false;
							}
						});
					});
					GetFiltersService.setFilters(scope.filters);
					$('#filtersBox').hide();

					GetDataService.storages['all_questions'] = [];
					scope.$parent.busyInfinite = true;
					GetDataService.getData({
						endpoint: GetDataService.ENUM_ENDPOINT.ALL_QUESTIONS,
						successCallback: function () {
							scope.$parent.head = GetDataService.getAllQuestions('head');
							scope.$parent.items = GetDataService.getAllQuestions('items');
							scope.$parent.busyInfinite = false;
						}
					});
				});

				//remove 1 filter from selected filters
				iElem.on('click', '.remove-filter', function () {
					var filterName = this.getAttribute("data-value");

					scope.selectedfilters = scope.selectedfilters.filter(function (obj) {
						return obj.name !== filterName;
					});
					jQuery.each(scope.filters, function (index, value) {
						if (value.typename === filterName) {
							jQuery.each(value.options, function (optionIndex, option) {
								if (optionIndex === 0) {
									scope.filters[index].options[optionIndex].selected = true;
								} else {
									scope.filters[index].options[optionIndex].selected = false;
								}
							});
						};
					});
					
					GetFiltersService.setSelectedFilters(scope.selectedfilters);
					GetFiltersService.setFilters(scope.filters);

					GetDataService.storages['all_questions'] = [];
					scope.$parent.busyInfinite = true;
					GetDataService.getData({
						endpoint: GetDataService.ENUM_ENDPOINT.ALL_QUESTIONS,
						options: JSON.stringify(scope.selectedfilters),
						successCallback: function () {
							scope.$parent.head = GetDataService.getAllQuestions('head');
							scope.$parent.items = GetDataService.getAllQuestions('items');
							scope.$parent.busyInfinite = false;
						}
					});
				});
			}
		};
	}

	FiltersDirective.$inject = ['GetDataService', 'GetFiltersService'];
	return FiltersDirective;

});