/* global angular, document */
/*
 * Expandable texts directive
 */
'use strict';
define(function () {
	function ModalDirective($rootScope, $templateRequest, $compile, $timeout) {
		return {
			restrict: 'A',
			link: function (scope, element, attrs) {
				/* Setting up dialog2 options */
				scope.options = {
					modalClass: typeof attrs.modalClass !== 'undefined' ? attrs.modalClass : '',
					id: typeof attrs.modalId !== 'undefined' ? attrs.modalId : '',
					autoOpen: false,
					removeOnClose: true,
				};
				/* Define dialog instance */
				scope.dialogInstance = null;
				/* Preload template */
				$templateRequest(attrs.href);
				/* Prevent redirect when click on "<a>" tag */
				element.click(function (e) {
					e.preventDefault();
					scope.initDialog();
				});
				/* trigger from outside **rootScope** */
				$rootScope.$watch('addQuestion', function (value) {
					if (value === 'openDialog')
						scope.initDialog();
					$rootScope.addQuestion = false;
				});
				scope.initDialog = function () {
					/* Get template from url of "<a>" tag attribute "href" and initialize */
					$templateRequest(attrs.href).then(function (data) {
						scope.dialogInstance = $($compile(data)(scope)).dialog2(scope.options);
						/* When all is ready and click on directive element trigger open modal */
						scope.dialogInstance.data('dialog2').open();
					});
				}
			}
		};
	}
	ModalDirective.$inject = ['$rootScope', '$templateRequest', '$compile', '$timeout'];
	return ModalDirective;
});