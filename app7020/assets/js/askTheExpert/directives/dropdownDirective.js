define(['../../modules/tinymce', 'modules/notification'], function (tinymce) {

	function dropdownDirective(FollowUnfollowService, GetDataService, DeleteQuestionService, QuestionService, $templateRequest, $compile) {
		var mce = new tinymce;
		return {
			restrict: "E",
			replace: true,
			template: '<div class="customDropdown-widget" ng-show="actions.length">\n\
								<div class="wrapper">\n\
									<div class="awesomeIcon-dots">\n\
										<ul class="listArray">\n\
											<li ng-repeat="action in actions">\n\
												<div class="link" ng-style="{color: action.text_color}" type="{{action.action_uid}}">\n\
													{{action.label}}\n\
												</div>\n\
											</li>\n\
										</ul>\n\
									</div>\n\
								</div>\n\
							</div>',
			scope: {
				actions: '='
			},
			link: function (scope, iElem, iAttrs) {
				iElem.on('click', function (e) {
					e.stopPropagation();
				});

				/* Make follow / unfollow */
				iElem.on('click', '.link[type="follow"]', function () {
					/* prevent multiple clicking :*/
					var $this = $(this);
					if ($this.is('.stop'))
						return false;
					$this.addClass('stop');

					FollowUnfollowService.setData({
						id: scope.$parent.item.question_id
					}).then(function ($data) {
						$this.removeClass('stop');
						if ($data.data.success === true) {
							GetDataService.updateQuestionsFollow($data.data);
						}
					}, function () {
						$this.removeClass('stop');
					});
				});

				/* Edit |show/hide tinymce| */
				iElem.on('click', '.link[type="edit"]', function () {
					scope.$emit('dropdown.edit', {target: $(this)});
				});

				/* Edit |show/hide tinymce| */
				iElem.on('click', '.link[type="editq"]', function () {
					/* prevent multiple clicking :*/
					var $this = $(this);
					if ($this.is('.stop'))
						return false;
					$this.addClass('stop');

					var generateClass = 'build-tinymce-' + scope.$id;
					var questionContainer = iElem.closest('.content-wrapper').find('.title [build-tinymce]');
					questionContainer.addClass(generateClass);

					mce.build('.' + generateClass, {
						plugins: ["moxiemanager image link autoresize"],
						setup: function (ed) {
							ed.addButton('custom-save', {
								text: Yii.t('app7020', 'Save'),
								classes: 'custom-save-wrapper',
								onclick: function () {
									var $thisButton = this;
									$thisButton.disabled(true);
									QuestionService.addQuestion({
										title: ed.getContent(),
										id_question: scope.$parent.item.question_id,
										successCallback: function (data) {
											$('.docebo-alert').remove();
											$(ed.editorContainer).closest('h3').find('.error-text').remove();
											if(data.data.success){
												$('.docebo-alert').remove();
												$(ed.editorContainer).closest('.questionBoxModel ').doceboNotice(Yii.t('app7020', 'The question has been edited successfully!'), 'success');
												setTimeout(function(){
													$('.docebo-alert').fadeOut('slow');
													$('.docebo-alert').remove();
												}, 5000);
												ed.destroy();
												questionContainer.removeClass(generateClass);
												$this.removeClass('stop');
											}else{
												angular.forEach(data.data.errors, function(value, key){
													if (key === 'title'){
														value.forEach(function (errorValue) {
															$(ed.editorContainer).closest('h3').prepend('<p class="error-text">' + errorValue + '</p>');
															$(ed.editorContainer).closest('.questionBoxModel ').doceboNotice(Yii.t('app7020', errorValue), 'error');
														});
													}
												});
												$thisButton.disabled(false);
											}											
										},
										errorCallback: function () {
											$('.docebo-alert').remove();
											ed.destroy();
											questionContainer.removeClass(generateClass);
											$(ed.editorContainer).closest('h3').find('.error-text').remove();
											$(ed.editorContainer).closest('.questionBoxModel ').doceboNotice(Yii.t('app7020', 'Something went wrong!'), 'error');
											$this.removeClass('stop');
										}
									});
								}
							});
						},
						init_instance_callback: function (editor) {
							$(editor.editorContainer).on('click', function (e) {
								e.stopPropagation();
							});
						}
					});
				});


				/* Delete question */
				iElem.on('click', '.link[type="deleteQuestion"]', function () {
					/* prevent multiple clicking :*/
					var $this = $(this);
					if ($this.is('.stop'))
						return false;
					$this.addClass('stop');

					/* Prepare and init confirm delete dialog */
					$templateRequest('index.php?r=askTheExpert/view&view_name=templates.confirmDelete').then(function (data) {
						var dialogOptions = {
							title: Yii.t('app7020', 'Delete'),
							modalClass: 'confirm-delete-dialog',
							id: 'confirm-delete-dialog',
							autoOpen: true,
							removeOnClose: true
						};
						var dialogInstance = $($compile(data)(scope)).dialog2(dialogOptions);
                        $(document).find('.modal-footer .save-dialog').addClass("disabled");
						dialogInstance.find('.save-dialog').click(function () {
							if (!dialogInstance.find('[type="checkbox"]').prop('checked')) {
								//dialogInstance.doceboNotice(Yii.t('app7020', 'Please check "Yes, I want to proceed!"'), 'error');
//								dialogInstance.dialog2('close');
                                return;
							} else {
								DeleteQuestionService.setData({
									id: scope.$parent.item.question_id
								}).then(function ($data) {
									if ($data.data.success === true) {
										GetDataService.updateQuestionsAfterDelete($data.data);
									}
									dialogInstance.dialog2('close');
								}, function () {
									dialogInstance.dialog2('close');
								});
							}
						});
                        dialogInstance.find('[type="checkbox"]').on('change', function(){
                            if($(this).prop('checked')){
                                $(document).find('.modal-footer .save-dialog').removeClass("disabled");
                            }else{
                                $(document).find('.modal-footer .save-dialog').addClass("disabled");
                            }
                        });
						dialogInstance.on('dialog2.closed', function () {
							$this.removeClass('stop');
						});
					});
				});

				/* Delete answer */
				iElem.on('click', '.link[type="deleteAnswer"]', function () {
					/* prevent multiple clicking :*/
					var $this = $(this);
					if ($this.is('.stop'))
						return false;
					$this.addClass('stop');

					/* Prepare and init confirm delete dialog */
					$templateRequest('index.php?r=askTheExpert/view&view_name=templates.confirmDelete').then(function (data) {
						var dialogOptions = {
							title: Yii.t('app7020', 'Delete'),
							modalClass: 'confirm-delete-dialog',
							id: 'confirm-delete-dialog',
							autoOpen: true,
							removeOnClose: true
						};

                        var dialogInstance = $($compile(data)(scope)).dialog2(dialogOptions);
                        $(document).find('.confirm-delete-dialog .save-dialog').addClass('disabled');
                        $(document).find('.confirm-delete-dialog').on('click', '[type="checkbox"]', function () {
                            if ($(this).prop('checked')) {
                                $(document).find('.modal-footer .save-dialog').removeClass("disabled");
                            } else {
                                $(document).find('.modal-footer .save-dialog').addClass("disabled");
                            }
                        });
						dialogInstance.find('.save-dialog').click(function () {
							if (!dialogInstance.find('[type="checkbox"]').prop('checked')) {
                                //dialogInstance.dialog2('close');
                                return;
							} else {
								QuestionService.deleteAnswer({
									id: scope.$parent.item.id_answer || scope.$parent.item.answer.answer_id || '',
									successCallback: function () {
										dialogInstance.dialog2('close');
									},
									errorCallback: function () {
										dialogInstance.dialog2('close');
									}
								});
							}
						});
						dialogInstance.on('dialog2.closed', function () {
							$this.removeClass('stop');
						});
					});

				});
			}
		};
	}
	$(document).on('dialog2.opened', '#confirm-delete-dialog', function(){
		$(this).closest('.modal').css({
			top: ($(document).scrollTop()+$(window).height()/2)- $(this).closest('.modal').height()/2
		});
		$("html, body").animate({scrollTop: $(document).scrollTop()});
		
	});

	dropdownDirective.$inject = ['FollowUnfollowService', 'GetDataService', 'DeleteQuestionService', 'QuestionService', '$templateRequest', '$compile'];
	return dropdownDirective;

});