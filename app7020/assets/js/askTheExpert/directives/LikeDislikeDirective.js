define(function () {

	function LikeDislikeDirective(LikeDislikeService) {
		return {
			restrict: 'A',
			replace: true,
			scope: {
				id: '@',
				likes: "@",
				dislikes: "@",
				readOnly: '='
			},
			template:	'<div class="like-dislike-wrapper" ng-class="{\'readOnly\': readOnly}">\n\
							<span class="likes-wrapper" type="1">\n\
								<i class="fa fa-thumbs-o-up"></i>\n\
								<span class="count">{{likes}}</span>\n\
							</span>\n\
							<span class="dislikes-wrapper" type="2">\n\
								<i class="fa fa-thumbs-o-down"></i>\n\
								<span class="count">{{dislikes}}</span>\n\
							</span>\n\
						</div>',
			link: function (scope, iElem, iAttrs) {
				iElem.on('click', function(e){
					e.stopPropagation();
				});

				if(scope.readOnly)
					return;
				
				iElem.find(' > span').click(function () {
					/* prevent multiple clicking :*/
					var $this = $(this);
					if ($this.is('.stop'))
						return false;
					$this.addClass('stop');

					LikeDislikeService.setData({
						id: scope.id,
						type: $this.attr('type')
					}).then(function ($data) {
						$this.removeClass('stop');
						if ($data.data.success === true) {
							scope.likes = $data.data.likes;
							scope.dislikes = $data.data.dislikes;
						}
					}, function () {
						$this.removeClass('stop');
					});
				});
			}
		};
	}

	LikeDislikeDirective.$inject = ['LikeDislikeService'];
	return LikeDislikeDirective;
});