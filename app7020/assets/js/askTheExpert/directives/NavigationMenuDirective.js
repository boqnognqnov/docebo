define(function () {
	function NavigationMenuDirective($location, $routeParams, $timeout) {
		return {
			restrict: 'E',
			scope: false,
			replace: true,
			template: '<div class="tab-navigation">\n\
							<ul class="app7020BootstrapTabsExtended {{navbarClass}}">\n\
								<li>\n\
									<a href="#/allQuestions">' + Yii.t("app7020", "All Questions") + '</a>\n\
								</li>\n\
								<li>\n\
									<a href="#/myQuestions">' + Yii.t("app7020", "My Questions") + '</a>\n\
								</li>\n\
								<li>\n\
									<a href="#/myAnswers">' + Yii.t("app7020", "My Answers") + '</a>\n\
								</li>\n\
								<li>\n\
									<a href="#/following">' + Yii.t("app7020", "Following") + '</a>\n\
								</li>\n\
								<li class="back-arrow skip">\n\
									<a href="{{prevUrl}}"><i class="fa fa-angle-left"></i></a>\n\
								</li>\n\
							</ul>\n\
							<a href="index.php?r=askTheExpert/view&view_name=directives.addQuestion" modal \n\
							   data-modal-class="boingInUp add-question-dialog" data-modal-id="add-question-dialog" class="add-question-button fa-stack">\n\
								<i class="fa fa-user fa-stack-1x"></i>\n\
								<i class="fa fa-comment-o fa-stack-1x"></i>\n\
							</a>\n\
						</div>',
			controller: 'NavigationMenuController',
			link: function (rootScope, iElem, iAttrs) {

				$timeout(function () {
					rootScope.$on('$locationChangeStart', function (event, next, current) {
						changeActiveClass($location.path());
					});
					changeActiveClass($location.path());
				});

				function changeActiveClass(path) {
					var links = iElem.find('ul.app7020BootstrapTabsExtended li:not(.skip)').removeClass('active').find('a[href="#' + path + '"]');
					if (links.length > 0)
						links.closest('li').addClass('active');
					iElem.find('ul.app7020BootstrapTabsExtended').activeLine();
				}

				$(document).on('click', '.breadcrumb a', function () {
					iElem.find('li').removeClass('active');

				});
				/* Toggle classes when click on any link from navigation menu */
				iElem.on('click', 'ul.app7020BootstrapTabsExtended li:not(.skip)', function () {
					iElem.find('li').removeClass('active');
					$(this).addClass('active');
				});
				/* Open Additional menu links */
				iElem.on('click', '.additionLinksMenu .toggle:not(.in)', function () {
					$(this).addClass('in rotate');
					$('body').addClass('backdrop');
					$(this).closest('.additionLinksMenu').find('> li:not(.toggle)').each(function () {
						$(this).css({display: 'block'});
						$(this).stop().animate({
							opacity: 1,
							top: $(this).index() * $(this).height() + 10 * $(this).index()
						}, 500);
					});
				});
				/* Close Additional menu links */
				iElem.on('click', '.additionLinksMenu .toggle.in', function () {
					$(this).closest('.additionLinksMenu').find('> li:not(.toggle)').css({opacity: '', top: '', display: ''});
					$(this).removeClass('in rotate');
					$('body').removeClass('backdrop');
				});

				iElem.on('mouseenter', '.additionLinksMenu li[data-toggle="tooltip"]', function () {
					var currentTooltip = $(this).parent().find('.tooltip.in');
					currentTooltip.css({
						left: currentTooltip.position().left - 20
					});
				});
			}
		};
	}
	NavigationMenuDirective.$inject = ['$location', '$routeParams', '$timeout'];
	return NavigationMenuDirective;
});