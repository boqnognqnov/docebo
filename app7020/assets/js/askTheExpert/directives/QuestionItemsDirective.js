define(function(){
	
	function QuestionItemsDirective($location, $window, $sce){
		
		return {
			restrict: 'EA',
			templateUrl: 'index.php?r=askTheExpert/view&view_name=templates.questionTemplate',
			scope: {
				items: '=',
				itemsFilter: '='
			},
			link: function(scope, iElem, iAttrs){
				scope.questionAllow = scope.$parent.questionAllow;
				
				scope.location = function(path, e){
					e.stopPropagation();
					window.location.href = '/app7020/index.php?r=askTheExpert/index#' + path;
					// Hide notifications' tooltip menu
					$("body").trigger( "click" );
				};
				scope.redirect = function(path, e){
					e.stopPropagation();
					$window.location.href = path;
				};
                scope.to_trusted = function(html_code){
                    return $sce.trustAsHtml(html_code);
                }
				
			}
		};
		
	}
	
	QuestionItemsDirective.$inject = ['$location', '$window', '$sce'];
	return QuestionItemsDirective;
	
});