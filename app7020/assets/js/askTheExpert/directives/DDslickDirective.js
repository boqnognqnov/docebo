define(['ddSlick'], function (ddslick) {

	function DDslickDirective($timeout) {

		return {
			restrict: 'A',
			link: function (scope, iElem, iAttrs) {
				var inputName = iAttrs.name;
				var form = iElem.closest('form');
				if(!form.find('input[name="'+inputName+'"]').length){
					form.append('<input type="hidden" name="'+inputName+'" value="" />');
				}
				var hiddenInput = form.find('input[name="'+inputName+'"]');
				var element = iAttrs.id;
				$timeout(function () {
					iElem.ddslick({
						width: '100%',
						background: '#fff',
						onSelected: function (data) {
							hiddenInput.val(data.selectedData.value);
						}
					});
				});
			}
		};

	}

	DDslickDirective.$inject = ['$timeout'];
	return DDslickDirective;

});