'use strict'
define(function (require) {
	require('gapi');
	require('gclient');
	require('gdrive');
	/* Definition of Angular Application **************************************/
	return require('angular').module('app7020Contribute', ['emguo.poller', 'ngOrderObjectBy', 'lk-google-picker'])
			.run(require('run'))
			.config(require('conf'))
			/* Angular controllers ****************************************************/
			.controller('clearList', require('controllers/assets/contributeClearList'))
			.controller('asset', require('controllers/assets/contributeAsset'))
			.controller('inviteController', require('controllers/inviteToWatch'))

			/* Angular directives *****************************************************/
			.directive('assetModal', require('directives/directiveAssetModalContribute'))
			.directive('assetRetry', require('directives/directiveAssetRetryContribute'))
			.directive('doceboTags', require('directives/directiveDocceboTags'))

			/* Angular filters ********************************************************/
			.filter('HtmlToPlainText', require('askTheExpert/filters/HtmlToPlainTextFilter'))

			/* Angular services for plUpload2 *****************************************/

			/* Angular services *******************************************************/
			.factory('Request', require('factory/ngRequestContribute'))
			.factory('RealTime', require('factory/ngRealTimeContribute'))
			.factory('WSPoller', require('factory/ngWSPollerContribute'))
			.factory('WSClient', require('factory/ngWSClientContribute'))
			.factory('WSMessageAsset', require('factory/ngWSMessageAssetContribute'))
			/* Angular services for plUpload2 *****************************************/
			.factory('Uploader', require('factory/ngUploaderContribute'))
			.factory('dataFeed', require('factory/ngDataFeed'))
			.factory('AJAX', require('factory/ngAjax'));

});