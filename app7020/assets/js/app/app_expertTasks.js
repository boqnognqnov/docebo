/* global require, angular, app7020Options, jQuery */
'use strict';

/***************************************************/
/************** REQUIREJS DEFINITIONS **************/
/***************************************************/
require.config({
	baseUrl: app7020Options.assetUrl + '/js',
	waitSeconds: 0,
	paths: {
		angular: '/themes/spt/js/angularJs/angular',
		angularRoute: '/themes/spt/js/angularJs/angular-route',
		angularCookies: '/themes/spt/js/angularJs/angular-cookies.min',
		angularInfiniteScroll: '/themes/spt/js/angularJs/ng-infinite-scroll.min',
		angularSanitize: '/themes/spt/js/angularJs/angular-sanitize.min',
		loadingBar: '/themes/spt/js/angularJs/loading-bar.min'
	},
	shim: {
		angular: {
			exports: 'angular'
		},
		angularRoute: {
			exports: 'angular',
			deps: ['angular']
		},
		angularCookies: {
			exports: 'angular',
			deps: ['angular']
		},
		angularInfiniteScroll: {
			exports: 'infinite-scroll',
			deps: ['angular']
		},
		angularSanitize: {
			exports: 'ngSanitize',
			deps: ['angular']
		},
		loadingBar: {
			exports: 'angular',
			deps: ['angular']
		}
	},
	priority: [
		'angular',
		'angularRoute',
		'angularCookies',
		'angularInfiniteScroll',
		'angularSanitize',
		'loadingBar',
		'myExpertTasks/app'
	]
});
define('jquery', [], function () {
	return jQuery;
});

require([
	'angular',
	'angularRoute',
	'angularCookies',
	'angularInfiniteScroll',
	'angularSanitize',
	'loadingBar',
	'myExpertTasks/app'
], function () {
	angular.element(document).ready(function () {
		angular.bootstrap(document, ['app7020MyExpertTasks']);
	});
});

/***************************************************/
/************** ANGULARJS DEFINITIONS **************/
/**************** +little requirejs ****************/

define("myExpertTasks/run", function () {
	function Run($rootScope, $templateRequest, $location) {
		$rootScope.subnav = [];
		$rootScope.filter = false;
		$rootScope.lastRoute = false;
		$rootScope.$on("$routeChangeStart", function (event, next, current) {
			$rootScope.lastRoute = $rootScope.subnav;
			if (typeof next !== 'undefined' && typeof next.redirectTo === 'undefined') {
				if (next.originalPath.indexOf('requestToSatisfy') > -1) {
					$rootScope.subnav = ['request'];
					if (typeof next.params.filter === 'undefined')
						$rootScope.filter = 'newRequests';
				}

				if (next.originalPath.indexOf('assetsToReview') > -1) {
					$rootScope.subnav = ['assets'];
					if (typeof next.params.filter === 'undefined')
						$rootScope.filter = 'inReview';
				}

				if (next.originalPath.indexOf('questionsToAnswers') > -1) {
					$rootScope.subnav = ['questions'];
					if (typeof next.params.filter === 'undefined')
						$rootScope.filter = 'newOnly';
				}

				if ($rootScope.filter && typeof next.params.filter === 'undefined')
					$rootScope.subnav.push($rootScope.filter);

				if (typeof next.params.filter !== 'undefined') {
					$rootScope.subnav.push(next.params.filter);
					$rootScope.filter = next.params.filter;
				}
			}
		});
	}
	Run.$inject = ['$rootScope', '$templateRequest', '$location'];
	return Run;
});

define("myExpertTasks/config", function () {
	function Config($routeProvider, cfpLoadingBarProvider) {
		cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
		cfpLoadingBarProvider.includeSpinner = false;
		$routeProvider
			.when('/requestToSatisfy/:filter?', {
				templateUrl: 'index.php?r=guruDashboard/view&view_name=requestToSatisfy',
				controller: 'MyExpertTasksController'
			})
			.when('/assetsToReview/:filter?', {
				templateUrl: 'index.php?r=guruDashboard/view&view_name=assetsToReview',
				controller: 'MyExpertTasksController'
			})
			.when('/questionsToAnswers/:filter?', {
				templateUrl: 'index.php?r=guruDashboard/view&view_name=questionsToAnswers',
				controller: 'MyExpertTasksController'
			})
			.otherwise({
				redirectTo: '/assetsToReview'
			});
	}
	Config.$inject = ['$routeProvider', 'cfpLoadingBarProvider'];
	return Config;
});

define("myExpertTasks/app", function (require) {
	/* Definition of Angular Application **************************************/
	require('angular').module('app7020MyExpertTasks', ['ngRoute', 'ngCookies', 'infinite-scroll', 'ngSanitize', 'angular-loading-bar'])
		.run(require('myExpertTasks/run'))
		.config(require('conf'))
		.config(require('myExpertTasks/config'))
		.value('uiTinymceConfig', {
			selector: 'span.text[build-tinymce]',
			plugins: ["moxiemanager image link"],
			toolbar: "bold italic custom-save underline link image",
			autoresize_min_height: 55,
			autoresize_bottom_margin: 10,
			language: yii.language,
			menubar: false,
			statusbar: false
		})
		/* Angular controllers ****************************************************/
		.controller('MyExpertTasksController', require('myExpertTasks/MyExpertTasksController'))

		/* Angular directives *****************************************************/
		.directive('navigationMenu', require('myExpertTasks/NavigationMenuDirective'))
		.directive('assetsItems', require('myExpertTasks/AssetsItemsDirective'))
		.directive('questionsItems', require('myExpertTasks/QuestionsItemsDirective'))
		.directive('actionIcons', require('myExpertTasks/ActionIconsDirective'))
		.directive('silenceDirective', require('myExpertTasks/SilenceDirective'))
		.directive('assignDirective', require('myExpertTasks/AssignDirective'))
		.directive('uiTinymce', require('directives/directiveTinymce'))

		/* Angular filters ********************************************************/
		.filter('HtmlToPlainText', require('askTheExpert/filters/HtmlToPlainTextFilter'))

		/* Angular (services|factories) *******************************************************/
		.factory('DataFactory', require('myExpertTasks/DataFactory'))
});