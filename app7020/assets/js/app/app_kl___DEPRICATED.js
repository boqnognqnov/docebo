'use strict';
define(function (require) {
	/* Definition of Angular Application **************************************/
	return require('angular').module('app7020AssetItem', ['emguo.poller', 'ngRoute', 'ngCookies', 'infinite-scroll'])
			.config(require('conf'))
			.config(require('conf/kl_conf'))
			/* Angular controllers ****************************************************/
			.controller('inviteController', require('controllers/inviteToWatch'))
			.controller('app7020AssetController', require('controllers/assets/singleAssets'))
			.controller('PeerReviewChat', require('controllers/assets/peerReview'))
			.controller('app7020DeleteAsset', require('controllers/app7020DeleteAsset'))
			.factory('dataFeed', require('factory/ngDataFeed'))
			.filter('NormalDate', require('filters/PRNormalDate'))
			.factory('Dates', require('filters/PRDates'))
			.factory('AJAX', require('factory/ngAjax'))
			.directive('doceboTags', require('directives/directiveDocceboTags'))


			/* Angular controllers ****************************************************/
//					.controller('AllQuestionsController', require('askTheExpert/controllers/AllQuestionsController'))
			.controller('DropdownController', require('askTheExpert/controllers/DropdownController'))
			.controller('QuestionController', require('askTheExpert/controllers/QuestionController'))
			.controller('TimelineController', require('tooltipsManagament/controllers/TimelineController'))

			/* Angular directives *****************************************************/
			.directive('modal', require('askTheExpert/directives/ModalDirective'))
			.directive('likeDislike', require('askTheExpert/directives/LikeDislikeDirective'))
			.directive('questionItems', require('directives/direcitveQuestionsWithAddQuetsion'))
			.directive('dropdown', require('askTheExpert/directives/dropdownDirective'))
			.directive('answerItems', require('askTheExpert/directives/AnswerItemsDirective'))
			.directive('simpleAnswer', require('directives/directiveAddNewAnswerSimple'))
			.directive('editor', require('tooltipsManagament/directives/editor'))
			.directive('seeker', require('tooltipsManagament/directives/seeker'))
			.directive('timeline', require('tooltipsManagament/directives/timeline'))

			/* Angular filters ********************************************************/
			.filter('HtmlToPlainText', require('askTheExpert/filters/HtmlToPlainTextFilter'))

			/* Angular services *******************************************************/
			.factory('GetDataService', require('askTheExpert/services/GetDataService'))
			.service('LikeDislikeService', require('askTheExpert/services/LikeDislikeService'))
			.service('DeleteQuestionService', require('askTheExpert/services/DeleteQuestionService'))
			.service('FollowUnfollowService', require('askTheExpert/services/FollowUnfollowService'))
			.service('QuestionService', require('askTheExpert/services/QuestionService'))
			.service('TimelineService', require('tooltipsManagament/services/TimelineService'))
			.factory('GetFiltersService', require('askTheExpert/services/GetFiltersService'))
});