'use strict';
define(function (require) {
	/* Definition of Angular Application **************************************/
	require('angular').module('app7020GeneralModule', ['ngRoute', 'ngCookies', 'ngCookies', 'infinite-scroll', 'ngSanitize', 'emguo.poller', 'ngOrderObjectBy'])
		.run(require('run'))
		.config(require('conf'))
		.config(require('routers'))
		.config(require('conf_asset_play'))
 
		.value('uiTinymceConfig', {
			selector: 'span.text[build-tinymce]',
			plugins: ["moxiemanager image link"],
			toolbar: "bold italic custom-save underline link image",
			autoresize_min_height: 55,
			autoresize_bottom_margin: 10,
			language: yii.language,
			menubar: false,
			statusbar: false
		})

		/* Angular controllers ****************************************************/
		.controller('AssetPlayController', require('controllers/assets/assetPlay'))
		.controller('AssetPlayAskTheExpert', require('controllers/assets/assetPlayAskExpert'))
		.controller('inviteController', require('controllers/inviteToWatch')) // Invite to watch controller
		.controller('app7020DeleteAsset', require('controllers/app7020DeleteAsset'))

		/* Angular directives *****************************************************/
		.directive('assetPlayer', require('directives/directivePlayer'))
//			.directive('assetPlayer', require('directives/directiveAssetPlayer'))
		.directive('peerReview', require('directives/directivePeerReview'))
		.directive('similarAssets', require('directives/directiveSimilarAssets'))
		.directive('navigationMenu', require('directives/directiveAssetNavigation'))
		.directive('assetDetails', require('directives/directiveAssetDetails'))
		.directive('assetEditDetails', require('directives/directiveAssetEditDetails'))
		.directive('assetActions', require('directives/directiveAssetActions'))
		.directive('assetRating', require('directives/directiveAssetRating'))
		.directive('assetStatusBar', require('directives/directiveAssetStatusBar'))
		.directive('assetEdit', require('directives/directiveAssetEdit'))
		.directive('doceboTags', require('directives/directiveDocceboTags'))
		.directive('tooltipSidebar', require('directives/directiveAssetTooltipSidebar'))
		.directive('tooltipTimeline', require('directives/directiveTooltipTimeline'))
		.directive('uiTinymce', require('directives/directiveTinymce'))

		.directive('questionItems', require('directives/direcitveQuestionsWithAddQuetsion'))
		.directive('dropdown', require('askTheExpert/directives/dropdownDirective'))
		.directive('answerItems', require('askTheExpert/directives/AnswerItemsDirective'))
		.directive('simpleAnswer', require('directives/directiveAddNewAnswerSimple'))
		//.directive('editor', require('tooltipsManagament/directives/editor'))

		/* Angular filters ********************************************************/
		.filter('startFrom', require('filters/startFrom'))
		.filter('HtmlToPlainText', require('askTheExpert/filters/HtmlToPlainTextFilter'))

		/* Angular services & factories *******************************************/
		.service('AJAX', require('factory/ngAjax'))
 		.factory('Request', require('factory/ngRequestContribute'))
 		.factory('GetDataService', require('askTheExpert/services/GetDataService'))
		.service('FollowUnfollowService', require('askTheExpert/services/FollowUnfollowService'))
		.service('DeleteQuestionService', require('askTheExpert/services/DeleteQuestionService'))
		.service('QuestionService', require('askTheExpert/services/QuestionService'))
		.factory('GetFiltersService', require('askTheExpert/services/GetFiltersService'))
		.factory('RealTime', require('factory/ngRealTimeContribute'))
		.factory('WSPoller', require('factory/ngWSPollerContribute'))
		.factory('WSClient', require('factory/ngWSClientContribute'))
		.factory('WSMessageAsset', require('factory/ngWSMessageAssetContribute'))


		.factory('dataFeed', require('factory/ngDataFeed')) // Needed for Invite to Watch Func.
		.factory('GetDataService', require('askTheExpert/services/GetDataService'))
});