'use strict';
define(function (require) {
	/* Definition of Angular Application **************************************/
	require('angular').module('app7020Questions', ['ngRoute', 'ngCookies', 'infinite-scroll', 'ngSanitize'])
		.run(require('askTheExpert/run'))
		.config(require('askTheExpert/config'))
		.value('uiTinymceConfig', {
			selector: 'textarea.tinymce',
			plugins: ["moxiemanager image link"],
			toolbar: "bold italic custom-save underline link image",
			autoresize_min_height: 55,
			autoresize_bottom_margin: 10,
			language: yii.language,
			menubar: false,
			statusbar: false
		})
		/* Angular controllers ****************************************************/
		.controller('AllQuestionsController', require('askTheExpert/controllers/AllQuestionsController'))
		.controller('MyQuestionsController', require('askTheExpert/controllers/MyQuestionsController'))
		.controller('MyAnswersController', require('askTheExpert/controllers/MyAnswersController'))
		.controller('FollowingController', require('askTheExpert/controllers/FollowingController'))
		.controller('NavigationMenuController', require('askTheExpert/controllers/NavigationMenuController'))
		.controller('DropdownController', require('askTheExpert/controllers/DropdownController'))
		.controller('QuestionController', require('askTheExpert/controllers/QuestionController'))
		.controller('FiltersController', require('askTheExpert/controllers/FiltersController'))

		/* Angular directives *****************************************************/
		.directive('navigationMenu', require('askTheExpert/directives/NavigationMenuDirective'))
		.directive('modal', require('askTheExpert/directives/ModalDirective'))
		.directive('filters', require('askTheExpert/directives/FiltersDirective'))
		.directive('likeDislike', require('askTheExpert/directives/LikeDislikeDirective'))
		.directive('questionItems', require('askTheExpert/directives/QuestionItemsDirective'))
		.directive('dropdown', require('askTheExpert/directives/dropdownDirective'))
		.directive('answerItems', require('askTheExpert/directives/AnswerItemsDirective'))
		.directive('doceboTags', require('directives/directiveDocceboTags'))
		.directive('uiTinymce', require('directives/directiveTinymce'))
		.directive('ddslick', require('askTheExpert/directives/DDslickDirective'))

		/* Angular filters ********************************************************/
		.filter('HtmlToPlainText', require('askTheExpert/filters/HtmlToPlainTextFilter'))

		/* Angular services *******************************************************/
		.factory('GetDataService', require('askTheExpert/services/GetDataService'))
		.factory('AJAX', require('askTheExpert/../factory/ngAjax'))
		.service('LikeDislikeService', require('askTheExpert/services/LikeDislikeService'))
		.service('FollowUnfollowService', require('askTheExpert/services/FollowUnfollowService'))
		.service('DeleteQuestionService', require('askTheExpert/services/DeleteQuestionService'))
		.service('QuestionService', require('askTheExpert/services/QuestionService'))
		.factory('GetFiltersService', require('askTheExpert/services/GetFiltersService'))
});