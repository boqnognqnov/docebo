'use strict';
define(function () {
	function Config($routeProvider) {
		
		$routeProvider//Routing
				.when('/assets/view/:id', {
					templateUrl: 'index.php?r=site/view&view_name=assetPlay.view',
 					controller: 'AssetPlayController'
				})
				.when('/assets/view/:id/question/:idQuestion', {
					templateUrl: 'index.php?r=site/view&view_name=assetPlay.view&mode=single',
 					controller: 'AssetPlayController'
				})
				.when('/assets/view/question/:questionID/:id', {
					templateUrl: 'index.php?r=site/view&view_name=assetPlay.view',
 					controller: 'AssetPlayController'
				})
				.when('/assets/edit/:id', {
					templateUrl: 'index.php?r=site/view&view_name=assetPlay.edit',
 					controller: 'AssetPlayController'
				})
				.when('/assets/peerReview/:id', {
					templateUrl: 'index.php?r=site/view&view_name=assetPlay.peerReview',
 					controller: 'AssetPlayController'
				})
				.when('/assets/tooltips/:id', {
					templateUrl: 'index.php?r=site/view&view_name=assetPlay.tooltips',
 					controller: 'AssetPlayController'
				})
				.when('/assets/tooltips/:id/review/:reviewId/time/:time', {
					templateUrl: 'index.php?r=site/view&view_name=assetPlay.tooltips',
 					controller: 'AssetPlayController'
				})
				.when('/assets/reuploadVideo/:id', {
					templateUrl: 'index.php?r=site/view&view_name=assetPlay.reuploadVideo',
 					controller: 'AssetPlayController'
				})
				.when('/assets/settings/:id', {
					templateUrl: 'index.php?r=site/view&view_name=assetPlay.settings',
 					controller: 'AssetPlayController'
				})
				.otherwise({
					redirectTo: function () {
                        window.location = "/lms/index.php?r=channels/index";
					}
				});
	};

	Config.$inject = ['$routeProvider'];
	return Config;
});