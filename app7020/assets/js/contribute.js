require.config({
	baseUrl: app7020Options.assetUrl + '/js',
	waitSeconds: 0,
	paths: {
		app: 'app/app_contribute',
		angular: '/themes/spt/js/angularJs/angular',
		pollerLib: '/themes/spt/js/angular-poller-master/angular-poller',
		moment: '/themes/spt/js/angularJs/angular-moment',
		angularResource: '/themes/spt/js/angularJs/angular-resource',
		moxie: '/themes/spt/js/plupload2/moxie.min',
		plUploader: '/themes/spt/js/plupload2/plupload.min',
		plUploaderUI: '/themes/spt/js/plupload2/jquery.ui.plupload/jquery.ui.plupload.min',
		lookingfor: '/themes/spt/js/lookingfor/jquery.lookingfor.min',
		ngOrder: '/themes/spt/js/angularJs/ng-order-object-by.js',
		
		lookingfor: '/themes/spt/js/lookingfor/jquery.lookingfor.min',
		gclient:  '/themes/spt/js/googleApi/client',
		 gapi: 'https://apis.google.com/js/client',
		gdrive: '/themes/spt/js/angular-google-picker-master/src/google-picker'
		
	},
//	/packages: ['tags', 'typeahead'],
	shim: {
		angular: {
			exports: 'angular',
//            deps: ['moment']
		},
		gdrive: {
			exports: 'angular',
            deps: ['angular']
		},
		pollerLib: {
			//exports: 'poller',
			deps: ['angular']
		},
		moxie: {
			exports: 'mOxie ',
			deps: ['angular']
		},
		plUploader: {
			exports: 'plupload',
			deps: ['moxie']
		},
		plUploaderUI: {
			exports: 'plupload',
			deps: ['plUploader']
		},
		ngOrder: {
			exports: 'ngOrderObjectBy',
			deps: ['angular']
		},
	},
	priority: [
		'angular',
		'gdrive',
		'app'
	]
});
define('jquery', [], function () {
	return jQuery;
});

require(['angular', 'ngOrder', 'controllers/inviteToWatch', 'factory/ngDataFeed', 'app'], function () {
	/* Run of Angular Application *********************************************/
	angular.bootstrap(document, ['app7020Contribute']);

	$(document).on('dialog2.ajax-complete', '#app7020-invite-dialog', function () {
		angular.element(document.body).injector().invoke(function ($compile, $rootScope) {
			$compile($("#invite_to_watch"))($rootScope);
			$rootScope.$apply();
		});
	})

});

