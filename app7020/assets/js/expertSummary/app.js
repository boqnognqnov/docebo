define([
	'require',
	'conf',
	'factory/ngActivity',
	'controllers/summary/controllerExpertSummary',
	'directives/chartjs-directive',
	'directives/directiveHBarChart'
], function (require) {
	/* Definition of Angular Application **************************************/
	return  require('angular').module('activity', ['ngSanitize'])

			.config(require('conf'))
			/* Angular controllers ****************************************************/
			.controller('expertSummary', require('controllers/summary/controllerExpertSummary'))

			/* Angular directives *****************************************************/
			.directive('chart', require('directives/chartjs-directive'))
			.directive('hBarChart', require('directives/directiveHBarChart'))

			/* Angular services *******************************************************/
			.factory('ngActivity', require('factory/ngActivity'))
});