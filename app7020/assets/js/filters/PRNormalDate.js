'use strict';
define(function (p) {
	return function () {
		return function (dateString) {
			var localDate = new Date();
			var formatedDate;
			if (typeof dateString == "string") {
				var dateChunks = dateString.split('-');
				formatedDate = dateChunks[0] + "/" + dateChunks[1] + "/" + dateChunks[2];
			} else {
				formatedDate = dateString;
			}
			var newDate = new Date(formatedDate);
			var offset = (-1 * ((localDate.getTimezoneOffset()) / 60));
			var Y, M, D, H, i, s = null;
			newDate.setHours(newDate.getHours() + offset);
			Y = newDate.getFullYear();
			M = ("0" + newDate.getMonth()).slice(-2);
			D = ("0" + newDate.getDate()).slice(-2);
			H = ("0" + newDate.getHours()).slice(-2);
			i = ("0" + newDate.getMinutes()).slice(-2);
			s = ("0" + newDate.getSeconds()).slice(-2);
			return Y + '-' + M + '-' + D + ' ' + H + ':' + i + ':' + s
		}
	};

});