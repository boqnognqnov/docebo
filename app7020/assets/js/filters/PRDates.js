'use strict';
define(function () {

	return function () {
		var factory = {};
		/**
		 * Return Utc timestamp
		 * @returns {Number|Boolean}
		 */
		factory.getUtcTime = function () {
			try {
				var d = new Date();
				d.setHours(d.getHours() + d.getTimezoneOffset() / 60);
				return d.getTime();
			} catch (e) {
				throw 'Wrong Date params!'
				return false;
			}
		}
		return factory;
	}


})