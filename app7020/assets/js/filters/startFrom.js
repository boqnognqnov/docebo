define(function () {

	function Filter() {
		return function (input, start) {
			start = +start;
			return input.slice(start);
		};
	}

	Filter.$inject = [];
	return Filter;

});