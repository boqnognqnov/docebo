'use strict';
require.config({
	baseUrl: app7020Options.assetUrl + '/js',
	paths: {
		ecController: 'expertsChannels/ecController',
	}
});
define('jquery', [], function() { return jQuery; });
require(['ecController']);