/**
 * 
 * @param {type} d
 * @param {type} t
 * @returns {undefined}
 */
'use strict';
define(['modules/notification'], function (css, angular) {

	function expertsChannelsClass(options) {
		return this.init(options);
	}

	expertsChannelsClass.prototype = {
		options: {
			container: '#app7020_Coach_Experts_Channels',
			selectChannels: '#selectChannels',
			singleDelete: '.singleListItem .customControls',
			deleteChannelsDialog: 'app7020-deleteExpertChannels-dialog', // this string will be set on dialog class and id
			debug: false,
		},
		init: function (opt) {
			if (typeof (opt) === 'object') {
				this.options = $.extend({}, this.options, opt);
			}
			if (this.options.debug) {
				debug('ExpertChannels :: Init() options');
				debug(this.options);
			}
			this.manageDialogs();
			this.attachListeners();
		},
		manageDialogs: function () {
			var that = this;
			var container = $(that.options.container);

			/* ASSIGN Channels DIALOG */
			$(document).on('click', that.options.container + ' ' + that.options.selectChannels, function () {
				openDialog(false, // Event
						Yii.t('app7020', 'Select channels to assign'), // Title
						app7020Options.axExpertsChannelsController + '/AxDialogSelectChannels', // Url
						'app7020-selectChannels-dialog', // Dialog-Id
						'app7020-selectChannels-dialog', // Dialog-Class
						'GET', // Ajax Method
						{// Ajax Data
							loadJs: 1
						}
				);
			});
			

			/* DELETE SELECTED CHANNELS DIALOG */
			$(document).on('dialog2.content-update', '#' + this.options.deleteChannelsDialog, function () {
				if ($(this).find('a.auto-close.success').length > 0) {
					$('#channelsComboListView').comboListView('updateListView');
					$('#channelsComboListView').closest('#combo-list-form').find('.combo-list-deselect-all').triggerHandler('click');
				}
				if ($(this).find('a.auto-close.unsuccess').length > 0) {
					$('#channelsComboListView').closest('#combo-list-form').find('.combo-list-deselect-all').triggerHandler('click');
				}
			});

			/* ADD SELECTED CHANNELS */
			$(document).on('submit', '#dialog-channels-selection-form', function () {
				var channels = $('#channelsGridView').comboGridView('getSelection');
				$.ajax({
					url: app7020Options.axExpertsChannelsController + '/AxAddChannels',
					method: "POST",
					data: {channels: channels},
					dataType: 'json',
				}).done(function (data) {
					$('#channelsComboListView').comboListView('updateListView');
				});
			});

		},
		attachListeners: function () {
			var that = this;
			var container = $(that.options.container);
			if ($(container).length > 0) {
				$(document).on('combolistview.massive-action-selected', function (e, data) {
					this.activeElement.value = '';
					if (data.listId == 'channelsComboListView') {
						if (data.action == 'deleteExperts' && data.selection.length > 0) {
							that.deleteItems(data.selection);
						}
					}
				});
			}
			$(document).on('click', that.options.container + ' ' + that.options.singleDelete, function () {
				var itemKey = [];
				itemKey.push($(this).closest('.combo-list-item').attr('data-key'));
				that.deleteItems(itemKey);
			});
			$(document).on('keyup', that.options.container + ' #combo-list-search-input', function () {
				if ($(this).val().length == 0) {
					$('#channelsComboListView').comboListView('updateListView');
				}
			});
		},
		deleteItems: function (items) {
			openDialog(false, // Event
					Yii.t('app7020', 'Delete Channels'), // Title
					app7020Options.axExpertsChannelsController + '/AxDeleteChannels', // Url
					this.options.deleteChannelsDialog, // Dialog-Id
					this.options.deleteChannelsDialog, // Dialog-Class
					'GET', // Ajax Method
					{// Ajax Data
						items: items
					}
			);
		}
	}

	var expertsChannels = new expertsChannelsClass;
});

