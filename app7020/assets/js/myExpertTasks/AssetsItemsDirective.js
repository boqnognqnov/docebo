define(function(){
	
	function Directive($timeout, $window, $location, $route){
		return {
			restrict: 'EA',
			templateUrl: 'index.php?r=guruDashboard/view&view_name=assetTemplate',
			scope: {
				items: '=',
				itemsFilter: '='
			},
			link: link
		};
		
		function link(scope, iElem, iAttrs){
			scope.assetAllow = scope.$parent.assetAllow;
			
			scope.$on('action-icon', function(e, data){
				if(data.action.action_uid === 'edit_asset'){
					$window.location.href = e.targetScope.$parent.item.link_to_asset;
				}
				if (data.action.action_uid === 'ignore_asset') {
					e.targetScope.$parent.item.ignore_state = 1;
				}
				if (data.action.action_uid === 'unignore_asset'){
					e.targetScope.$parent.item.ignore_state = 1;
				}
			});
			scope.redirect = function (path, e, isLocal) {
				e.stopPropagation();
				if (isLocal === true) {
					/* local redirect*/
					if(typeof path === 'object'){
						$location.path($route.current.originalPath.replace(path[0], path[1]));
					}else{
						$location.path(path);
					}					
				} else {
					/* external redirect */
					$window.location.href = path;
				}
				/* Hide notifications' tooltip menu */
				$("body").trigger("click");
			};
		}
	}
	Directive.$inject = ['$timeout', '$window', '$location', '$route'];
	return Directive;
	
});