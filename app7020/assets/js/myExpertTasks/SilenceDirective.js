define(function () {

	function Directive($timeout, DataFactory, $http) {
		return {
			restrict: 'E',
			transclude: true,
			replace: true,
			scope: {},
			template: '<div class="silence-template-container" ng-transclude></div>',
			link: link
		};

		function link(scope, iElem, iAttrs) {
			$timeout(function () {
				/* prevent redirect */
				iElem.on('click', function (e) {
					e.stopPropagation();
				});
				/* Close */
				iElem.on('click', '.fa-times:not(.stop)', function () {
					var $this = $(this);
					$this.addClass('stop');
					if (scope.$parent.item.ignore_state == 1){
						scope.$parent.item.ignore_state = 0;
						scope.$parent.$digest();
						$this.removeClass('stop');
					}
					if (scope.$parent.item.ignore_state == 2){
						scope.$emit('force.init');
					}
				});
				/* Send for ingoring */
				iElem.on('click', '.before button:not(.stop)', function () {
					var $this = $(this);
					$this.addClass('stop');
					if(scope.$parent.item.type_uid === 'question' || scope.$parent.item.type_uid === 'request'){
						$http.post('/lms/index.php?r=restdata', {
							endpoint: 'et_silence_crud',
							params: {
								type: 'question',
								action: (scope.$parent.item.ignored) ? 'unsilence' : 'silence',
								id: scope.$parent.item.question_id
							}
						}).then(function($result){
							if($result.data.success)
								scope.$parent.item.ignore_state = 2;
							$this.removeClass('stop');
						}, function($error){
							$this.removeClass('stop');
						});
					}
					if(scope.$parent.item.type_uid === 'asset'){
						$http.post('/lms/index.php?r=restdata', {
							endpoint: 'et_silence_crud',
							params: {
								type: 'asset',
								action: (scope.$parent.item.ignored) ? 'unsilence' : 'silence',
								id: scope.$parent.item.asset_id
							}
						}).then(function($result){
							if($result.data.success)
								scope.$parent.item.ignore_state = 2;
							$this.removeClass('stop');
						}, function($error){
							$this.removeClass('stop');
						});
					}			
				});
				/* Undo for un-ignoring */
				iElem.on('click', '.after button:not(.stop)', function () {
					var $this = $(this);
					$this.addClass('stop');
					if(scope.$parent.item.type_uid === 'question' || scope.$parent.item.type_uid === 'request'){
						$http.post('/lms/index.php?r=restdata', {
							endpoint: 'et_silence_crud',
							params: {
								type: 'question',
								action: (!scope.$parent.item.ignored) ? 'unsilence' : 'silence',
								id: scope.$parent.item.question_id
							}
						}).then(function($result){
							if($result.data.success)
								scope.$parent.item.ignore_state = 0;
							$this.removeClass('stop');
						}, function($error){
							scope.$parent.item.ignore_state = 0;
							$this.removeClass('stop');
						});
					}
					if(scope.$parent.item.type_uid === 'asset'){
						$http.post('/lms/index.php?r=restdata', {
							endpoint: 'et_silence_crud',
							params: {
								type: 'asset',
								action: (!scope.$parent.item.ignored) ? 'unsilence' : 'silence',
								id: scope.$parent.item.asset_id
							}
						}).then(function($result){
							if($result.data.success)
								scope.$parent.item.ignore_state = 0;
							$this.removeClass('stop');
						}, function($error){
							scope.$parent.item.ignore_state = 0;
							$this.removeClass('stop');
						});
					}
				});

			});
		}
	}

	Directive.$inject = ['$timeout', 'DataFactory', '$http'];
	return Directive;

});