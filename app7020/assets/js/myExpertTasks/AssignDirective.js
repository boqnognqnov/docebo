define(function () {

	function Directive($timeout, DataFactory, $http) {
		return {
			restrict: 'E',
			transclude: true,
			replace: true,
			scope: {},
			template: '<div class="assign-wrapper-container" ng-transclude></div>',
			link: link
		};

		function link(scope, iElem, iAttrs) {
			scope.agreeAssign = null;
			$timeout(function () {
				/* prevent redirect */
				iElem.on('click', function (e) {
					e.stopPropagation();
				});
				
				scope.$watch(function () {
					return iElem.find('.before-state [name="agreeAssign"]').prop('checked');
				}, function (value) {
					scope.agreeAssign = value;
				});
				
				iElem.on('click', '.before-state button.assign:not(.stop)', function () {
					/* Check is agree checkbox is checked */
					if(!scope.agreeAssign)
						return;
					
					var $this = $(this);
					$this.addClass('stop');
					$http.post('/lms/index.php?r=restdata', {
						endpoint: 'et_assign_crud',
						params: {
							id: scope.$parent.item.question_id
						}
					}).then(function ($result) {
						if ($result.data.success) {
							scope.$parent.item.assign_state = 2;
							$timeout(function () {
								scope.$emit('force.init');
							}, 3000);
						}else{
							scope.$parent.item.assign_state = 0;
						}
						$this.removeClass('stop');
					}, function ($error) {
						scope.$parent.item.assign_state = 0;
						$this.removeClass('stop');
					});
				});
			});
		}
	}

	Directive.$inject = ['$timeout', 'DataFactory', '$http'];
	return Directive;

});