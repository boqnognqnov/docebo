define(['modules/notification'],function () {

	function Controller($scope, DataFactory, $location, $http, $timeout) {

		$scope.init = function (force) {
			DataFactory.prepareItems();
			$scope.data = DataFactory.getData();
			$scope.loaded = !($scope.data === false);
			if ($scope.data === false || force === true)
				DataFactory.apiData({
					endpoint: 'et_all'
				}, force).then(function ($result) {
					$scope.data = DataFactory.getData($result.data);
					$scope.loaded = true;
					$scope.$emit('data.loaded', $scope.data);
				}, function ($result) {
					DataFactory.clear();
				});
		};

		/* Events */
		$scope.$on('force.init', function () {
			$scope.init(true);
		});
		$scope.$on('update.data', function () {
			$scope.data = DataFactory.getData();
			$scope.$emit('data.loaded', $scope.data);
		});

		/* Clear all from silence list */
		$scope.clearSilenced = function (type) {
			$http.post('/lms/index.php?r=restdata', {
				endpoint: 'et_clean_silenced',
				params: {
					type: type
				}
			}).then(function ($result) {
				if ($result.data.success)
					$scope.init(true);
			}, function ($error) {
				console.log($error);
			});
		};


		/* Permissions of view extension per location path */
		switch ($scope.$root.subnav.join('-').toLowerCase()) {
			case "request-incharge":
				$scope.questionAllow = {footer: ['related', 'author', 'views', 'time', 'answers', 'assign']};
				break;
			case "request-newrequests":
				$scope.questionAllow = {footer: ['related', 'author', 'views', 'time', 'answers', 'assign']};
				break;
			case "request-silenced":
				$scope.questionAllow = {footer: ['related', 'author', 'views', 'time', 'answers']};
				break;
			case "assets-inreview":
				$scope.assetAllow = {footer: ['new', 'peerReviews', 'author', 'time']};
				$scope.itemsCount = ($scope.data) ? $scope.data.assets.inreview.items.length : 0;
				break;
			case "assets-newonly":
				$scope.assetAllow = {footer: ['new', 'peerReviews', 'author', 'time']};
				$scope.itemsCount = ($scope.data) ? $scope.data.assets.new.items.length : 0;
				break;
			case "assets-silenced":
				$scope.assetAllow = {footer: ['new', 'peerReviews', 'author', 'time']};
				$scope.itemsCount = ($scope.data) ? $scope.data.assets.ignored.items.length : 0;
				break;
			case "questions-openquestions":
				$scope.questionAllow = {footer: ['related', 'author', 'views', 'time', 'answers']};
				$scope.itemsCount = ($scope.data) ? $scope.data.questions.open.items.length : 0;
				break;
			case "questions-newonly":
				$scope.questionAllow = {footer: ['related', 'author', 'views', 'time', 'answers']};
				$scope.itemsCount = ($scope.data) ? $scope.data.questions.new.items.length : 0;
				break;
			case "questions-silenced":
				$scope.questionAllow = {footer: ['related', 'author', 'views', 'time', 'answers']};
				$scope.itemsCount = ($scope.data) ? $scope.data.questions.ignored.items.length : 0;
				break;
		}

		/* Infinite scroll for all tabs */
		$scope.busyInfinite = false;
		$scope.infiniteStep = 5; // items on request
		$scope.infiniteHttpParams = false;
		$scope.runInfinite = function () {
			/* prevent infinite before data loaded */
			if (!$scope.loaded || $scope.busyInfinite)
				return;

			/* get case of current infinite tab */
			switch ($scope.$root.subnav.join('-').toLowerCase()) {
				case "request-incharge":
					/* prevent infinite if no have more items */
					if (!$scope.data.requests.incharge.items.length || $scope.data.requests.incharge.questions_count == $scope.data.requests.incharge.items.length)
						return;
					$scope.busyInfinite = true;
					$scope.infiniteHttpParams = {
						endpoint: 'et_requests',
						params: {
							from: $scope.data.requests.incharge.items.length,
							count: $scope.infiniteStep
						}
					};
					$scope.success = function ($result) {
						$result.data.items.forEach(function (item) {
							$scope.data.requests.incharge.items.push(item);
						});
						DataFactory.getData($scope.data);
						$scope.$emit('data.loaded', $scope.data);
						$timeout(function () {
							$scope.busyInfinite = false;
						}, 1000);
					};
					break;
				case "request-newrequests":
					/* prevent infinite if no have more items */
					if (!$scope.data.requests.new.items.length || $scope.data.requests.new.questions_count == $scope.data.requests.new.items.length)
						return;
					$scope.busyInfinite = true;
					$scope.infiniteHttpParams = {
						endpoint: 'et_requests',
						params: {
							only_new: true,
							from: $scope.data.requests.new.items.length,
							count: $scope.infiniteStep
						}
					};
					$scope.success = function ($result) {
						$result.data.items.forEach(function (item) {
							$scope.data.requests.new.items.push(item);
						});
						DataFactory.getData($scope.data);
						$scope.$emit('data.loaded', $scope.data);
						$timeout(function () {
							$scope.busyInfinite = false;
						}, 1000);
					};
					break;
				case "request-silenced":
					/* prevent infinite if no have more items */
					if (!$scope.data.requests.ignored.items.length || $scope.data.requests.ignored.questions_count == $scope.data.requests.ignored.items.length)
						return;
					$scope.busyInfinite = true;
					$scope.infiniteHttpParams = {
						endpoint: 'et_requests',
						params: {
							ignored: true,
							from: $scope.data.requests.ignored.items.length,
							count: $scope.infiniteStep
						}
					};
					$scope.success = function ($result) {
						$result.data.items.forEach(function (item) {
							$scope.data.requests.ignored.items.push(item);
						});
						DataFactory.getData($scope.data);
						$scope.$emit('data.loaded', $scope.data);
						$timeout(function () {
							$scope.busyInfinite = false;
						}, 1000);
					};
					break;
				case "assets-inreview":
					/* prevent infinite if no have more items */
					if (!$scope.data.assets.inreview.items.length || $scope.data.assets.inreview.total_asset_count == $scope.data.assets.inreview.items.length)
						return;
					$scope.busyInfinite = true;
					$scope.infiniteHttpParams = {
						endpoint: 'asset_ra_in_review',
						params: {
							from: $scope.data.assets.inreview.items.length,
							count: $scope.infiniteStep
						}
					};
					$scope.success = function ($result) {
						$result.data.items.forEach(function (item) {
							$scope.data.assets.inreview.items.push(item);
						});
						DataFactory.getData($scope.data);
						$scope.$emit('data.loaded', $scope.data);
						$timeout(function () {
							$scope.busyInfinite = false;
						}, 1000);
					};
					break;
				case "assets-newonly":
					/* prevent infinite if no have more items */
					if (!$scope.data.assets.new.items.length || $scope.data.assets.new.total_asset_count == $scope.data.assets.new.items.length)
						return;
					$scope.busyInfinite = true;
					$scope.infiniteHttpParams = {
						endpoint: 'asset_ra_new_only',
						params: {
							from: $scope.data.assets.new.items.length,
							count: $scope.infiniteStep
						}
					};
					$scope.success = function ($result) {
						$result.data.items.forEach(function (item) {
							$scope.data.assets.new.items.push(item);
						});
						DataFactory.getData($scope.data);
						$scope.$emit('data.loaded', $scope.data);
						$timeout(function () {
							$scope.busyInfinite = false;
						}, 1000);
					};
					break;
				case "assets-silenced":
					/* prevent infinite if no have more items */
					if (!$scope.data.assets.ignored.items.length || $scope.data.assets.ignored.total_asset_count == $scope.data.assets.ignored.items.length)
						return;
					$scope.busyInfinite = true;
					$scope.infiniteHttpParams = {
						endpoint: 'asset_ra_silenced',
						params: {
							from: $scope.data.assets.ignored.items.length,
							count: $scope.infiniteStep
						}
					};
					$scope.success = function ($result) {
						$result.data.items.forEach(function (item) {
							$scope.data.assets.ignored.items.push(item);
						});
						DataFactory.getData($scope.data);
						$scope.$emit('data.loaded', $scope.data);
						$timeout(function () {
							$scope.busyInfinite = false;
						}, 1000);
					};
					break;
				case "questions-openquestions":
					/* prevent infinite if no have more items */
					if (!$scope.data.questions.open.items.length || $scope.data.questions.open.questions_count == $scope.data.questions.open.items.length)
						return;
					$scope.busyInfinite = true;
					$scope.infiniteHttpParams = {
						endpoint: 'et_questions',
						params: {
							from: $scope.data.questions.open.items.length,
							count: $scope.infiniteStep
						}
					};
					$scope.success = function ($result) {
						$result.data.items.forEach(function (item) {
							$scope.data.questions.open.items.push(item);
						});
						DataFactory.getData($scope.data);
						$scope.$emit('data.loaded', $scope.data);
						$timeout(function () {
							$scope.busyInfinite = false;
						}, 1000);
					};
					break;
				case "questions-newonly":
					/* prevent infinite if no have more items */
					if (!$scope.data.questions.new.items.length || $scope.data.questions.new.questions_count == $scope.data.questions.new.items.length)
						return;
					$scope.busyInfinite = true;
					$scope.infiniteHttpParams = {
						endpoint: 'et_questions',
						params: {
							only_new: true,
							from: $scope.data.questions.new.items.length,
							count: $scope.infiniteStep
						}
					};
					$scope.success = function ($result) {
						$result.data.items.forEach(function (item) {
							$scope.data.questions.new.items.push(item);
						});
						DataFactory.getData($scope.data);
						$scope.$emit('data.loaded', $scope.data);
						$timeout(function () {
							$scope.busyInfinite = false;
						}, 1000);
					};
					break;
				case "questions-silenced":
					/* prevent infinite if no have more items */
					if (!$scope.data.questions.ignored.items.length || $scope.data.questions.ignored.questions_count == $scope.data.questions.ignored.items.length)
						return;
					$scope.busyInfinite = true;
					$scope.infiniteHttpParams = {
						endpoint: 'et_questions',
						params: {
							ignored: true,
							from: $scope.data.questions.ignored.items.length,
							count: $scope.infiniteStep
						}
					};
					$scope.success = function ($result) {
						$result.data.items.forEach(function (item) {
							$scope.data.questions.ignored.items.push(item);
						});
						DataFactory.getData($scope.data);
						$scope.$emit('data.loaded', $scope.data);
						$timeout(function () {
							$scope.busyInfinite = false;
						}, 1000);
					};
					break;
			}

			if ($scope.busyInfinite === true)
				$http.post('/lms/index.php?r=restdata', $scope.infiniteHttpParams).then(function ($result) {
					$scope.success($result);
				}, function ($error) {
					$scope.busyInfinite = false;
				});
		};
	}
	Controller.$inject = ['$scope', 'DataFactory', '$location', '$http', '$timeout'];
	return Controller;

});