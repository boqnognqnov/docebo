/* global Yii, app7020Options, angular */
define(function () {
	function Directive($location, $timeout, $rootScope) {
		return {
			restrict: 'E',
			replace: true,
			scope: true,
			template:
				'<div class="tab-navigation">\n\
					<ul class="app7020BootstrapTabsExtended">\n\
						<li>\n\
							<a href="#/requestToSatisfy">' + Yii.t("app7020", "Request to satisfy") + '<i>{{data.requests.new.questions_count}}</i></a>\n\
						</li>\n\
						<li>\n\
							<a href="#/assetsToReview">' + Yii.t("app7020", "Assets to review/approve") + '<i>{{data.assets.new.total_asset_count}}</i></a>\n\
						</li>\n\
						<li>\n\
							<a href="#/questionsToAnswers">' + Yii.t("app7020", "Questions to answers") + '<i>{{data.questions.new.questions_count}}</i></a>\n\
						</li>\n\
					</ul>\n\
					<ul class="sub-navigation clearfix" ng-if="subnav.indexOf(\'request\') > -1">\n\
						<li ng-class="{\'active\': subnav.indexOf(\'inCharge\') > -1}">\n\
							<a href="#/requestToSatisfy/inCharge">' + Yii.t("app7020", "In charge") + ' <span ng-show="loaded">({{data.requests.incharge.questions_count}})</span></a></li>\n\
						<li ng-class="{\'active\': subnav.indexOf(\'newRequests\') > -1}">\n\
							<a href="#/requestToSatisfy/newRequests">' + Yii.t("app7020", "New requests") + ' <span ng-show="loaded">({{data.requests.new.questions_count}})</span></a></li>\n\
						<li ng-class="{\'active\': subnav.indexOf(\'silenced\') > -1}">\n\
							<a href="#/requestToSatisfy/silenced">' + Yii.t("app7020", "Silenced") + ' <span ng-show="loaded">({{data.requests.ignored.questions_count}})</span></a></li>\n\
					</ul>\n\
					<ul class="sub-navigation clearfix" ng-if="subnav.indexOf(\'assets\') > -1">\n\
						<li ng-class="{\'active\': subnav.indexOf(\'inReview\') > -1}">\n\
							<a href="#/assetsToReview/inReview">' + Yii.t("app7020", "In review") + ' <span ng-show="loaded">({{data.assets.inreview.total_asset_count}})</span></a></li>\n\
						<li ng-class="{\'active\': subnav.indexOf(\'newOnly\') > -1}">\n\
							<a href="#/assetsToReview/newOnly">' + Yii.t("app7020", "New Only") + ' <span ng-show="loaded">({{data.assets.new.total_asset_count}})</span></a></li>\n\
						<li ng-class="{\'active\': subnav.indexOf(\'silenced\') > -1}">\n\
							<a href="#/assetsToReview/silenced">' + Yii.t("app7020", "Silenced") + ' <span ng-show="loaded">({{data.assets.ignored.total_asset_count}})</span></a></li>\n\
					</ul>\n\
					<ul class="sub-navigation clearfix" ng-if="subnav.indexOf(\'questions\') > -1">\n\
						<li ng-class="{\'active\': subnav.indexOf(\'openQuestions\') > -1}">\n\
							<a href="#/questionsToAnswers/openQuestions">' + Yii.t("app7020", "Open Questions") + ' <span ng-show="loaded">({{data.questions.open.questions_count}})</span></a></li>\n\
						<li ng-class="{\'active\': subnav.indexOf(\'newOnly\') > -1}">\n\
							<a href="#/questionsToAnswers/newOnly">' + Yii.t("app7020", "New only") + ' <span ng-show="loaded">({{data.questions.new.questions_count}})</span></a></li>\n\
						<li ng-class="{\'active\': subnav.indexOf(\'silenced\') > -1}">\n\
							<a href="#/questionsToAnswers/silenced">' + Yii.t("app7020", "Silenced") + ' <span ng-show="loaded">({{data.questions.ignored.questions_count}})</span></a></li>\n\
					</ul>\n\
				</div>',
			link: link
		};

		function link(scope, iElem, iAttrs) {
			$timeout(function () {
				scope.subnav = $rootScope.subnav;

				/* Add active class on init */
				angular.forEach(iElem.find('ul.app7020BootstrapTabsExtended li a'), function (link) {
					if (("#" + $location.path()).indexOf($(link).attr('href')) > -1)
						$(link).closest('li').addClass('active');
				});

				/* Init SmoothActiveLine */
				iElem.find('ul.app7020BootstrapTabsExtended').boostrapSmoothActiveLine({
					height: '5px',
					background: '#0465AC'
				});

				$rootScope.$on("$routeChangeStart", function (event, next, current) {
					if(typeof next.redirectTo === 'undefined'){
						iElem.find('ul.app7020BootstrapTabsExtended li').removeClass('active');
						angular.forEach(iElem.find('ul.app7020BootstrapTabsExtended li a'), function (link) {
							if (("#" + $location.path()).indexOf($(link).attr('href')) > -1)
								$(link).closest('li').addClass('active');
						});
						if(typeof next.redirectTo === 'undefined')
						iElem.find('ul.app7020BootstrapTabsExtended').boostrapSmoothActiveLine('recalc');
					}
				});
			});

			$rootScope.$watch('subnav', function (value) {
				scope.subnav = value;
			});

			scope.loaded = false;
			$rootScope.$on('data.loaded', function (e, value) {
				scope.data = value;
				scope.loaded = true;
			});

			/* Toggle classes when click on any link from navigation menu */
//			iElem.on('click', 'ul.app7020BootstrapTabsExtended li:not(.skip)', function () {
//				iElem.find('ul.app7020BootstrapTabsExtended li').removeClass('active');
//				$(this).addClass('active');
//			});
		}
	}

	Directive.$inject = ['$location', '$timeout', '$rootScope'];
	return Directive;

});