define(function(){
	
	function Directive($timeout){
		return {
			restrict: 'AE',
			replace: true,
			scope: {
				actions: '='
			},
			link: link,
			template:
				'<div class="action-icons-wrapper">\n\
					<i class="fa" ng-repeat="item in actions" ng-class="item.icon_name" ng-click="action(item, $event)" ng-style="{color: item.icon_color}"\n\
					data-original-title="{{item.label}}" ng-if="checkPermissions(item)"></i>\n\
				</div>'
		};
		
		function link(scope, iElem, iAttrs){
			scope.action = function(item, e){
				e.stopPropagation();
				scope.$emit('action-icon', {event: e, action: item});
			};
			scope.checkPermissions = function(item){
				if(scope.$parent.item.ignored){
					if(item.action_uid == 'ignore_question' || item.action_uid == 'ignore_asset')
						return false;
				}else{
					if(item.action_uid == 'unignore_question' || item.action_uid == 'unignore_asset')
						return false;
				}
				return true;
			};
			$timeout(function () {
				iElem.find('i').tooltip({
					placement: 'top',
					trigger: 'hover'
				});
			});
		}
	}
	
	Directive.$inject = ['$timeout'];
	return Directive;
	
});