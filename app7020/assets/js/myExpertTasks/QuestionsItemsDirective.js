define(function () {

	function Directive($timeout, $location, $window, $route, $http, $sce) {
		return {
			restrict: 'EA',
			templateUrl: 'index.php?r=guruDashboard/view&view_name=questionTemplate',
			scope: {
				items: '=',
				itemsFilter: '='
			},
			link: link
		};

		function link(scope, iElem, iAttrs) {
			scope.questionAllow = scope.$parent.questionAllow;
			/* this triggering (silence|unsilence) */

			scope.$on('action-icon', function (e, data) {
				if (data.action.action_uid === 'view_question') {
					$window.location.href = e.targetScope.$parent.item.link_to_question;
				}
				if (data.action.action_uid === 'ignore_question') {
					e.targetScope.$parent.item.ignore_state = 1;
				}
				if (data.action.action_uid === 'unignore_question') {
					e.targetScope.$parent.item.ignore_state = 1;
				}
				if (data.action.action_uid === 'quick_answer') {
					e.targetScope.$parent.item.show_quick_answer = true;
				}
			});
			scope.redirect = function (path, e, isLocal) {
				e.stopPropagation();
				if (isLocal === true) {
					/* local redirect*/
					if (typeof path === 'object') {
						$location.path($route.current.originalPath.replace(path[0], path[1]));
					} else {
						$location.path(path);
					}
				} else {
					/* external redirect */
					$window.location.href = path;
				}
				/* Hide notifications' tooltip menu */
				$("body").trigger("click");
			};

			scope.tinymceOptions = {
				plugins: ["moxiemanager image link autoresize"],
				setup: function (ed) {
					ed.addButton('custom-save', {
						text: Yii.t('app7020', 'Answer'),
						classes: 'custom-save-wrapper',
						onclick: function (e) {
							var thisButton = this;
							thisButton.disabled(true);
							$http.post('/lms/index.php?r=restdata', {
								endpoint: 'save_answer',
								params: {
									id_question: $(e.currentTarget).closest('.quick_answer').data('question-id'),
									title: ed.getContent()
								}
							}).then(function($result){
								scope.$emit('force.init');
							}, function($error){
								thisButton.disabled(false);
							});
						}
					});
				},
				init_instance_callback: function (editor) {
					$(editor.editorContainer).on('click', function (e) {
						e.stopPropagation();
					});
				}
			};
            scope.to_trusted = function(html_code){
                return $sce.trustAsHtml(html_code);
            }
		}

	}
	Directive.$inject = ['$timeout', '$location', '$window', '$route', '$http', '$sce'];
	return Directive;

});