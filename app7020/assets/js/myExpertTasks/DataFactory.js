/* global angular */
define(function () {

	function Factory($http, $rootScope, $timeout) {

		var self = {};

		self.data = false;

		self.apiData = function (requestParams, force) {
			if (self.data.length && typeof force == false) {
				return self.data;
			}
			return $http.post('/lms/index.php?r=restdata', requestParams);
		};

		self.clear = function () {
			self.data = false;
		};

		self.getData = function (data) {
			if (typeof data !== 'undefined')
				self.data = data;
			return self.data;
		};

		self.prepareItems = function () {
			if ($rootScope.lastRoute.length && self.data !== false) {
				switch ($rootScope.lastRoute.join('-').toLowerCase()) {
					case "request-incharge":
						var updateApi = false;
						self.data.requests.incharge.items.forEach(function (item) {
							if (!updateApi && item.ignore_state === 2 || !updateApi && item.assign_state === 2)
								updateApi = true;
							item.ignore_state = 0;
							item.assign_state = 0;
							item.show_quick_answer = false;
							delete item.quick_answer;
						});
						break;
					case "request-newrequests":
						var updateApi = false;
						self.data.requests.new.items.forEach(function (item) {
							if (!updateApi && item.ignore_state === 2 || !updateApi && item.assign_state === 2)
								updateApi = true;
							item.ignore_state = 0;
							item.assign_state = 0;
							item.show_quick_answer = false;
							delete item.quick_answer;
						});
						break;
					case "request-silenced":
						var updateApi = false;
						self.data.requests.ignored.items.forEach(function (item) {
							if (!updateApi && item.ignore_state === 2 || !updateApi && item.assign_state === 2)
								updateApi = true;
							item.ignore_state = 0;
							item.assign_state = 0;
							item.show_quick_answer = false;
							delete item.quick_answer;
						});
						break;
					case "assets-inreview":
						var updateApi = false;
						self.data.assets.inreview.items.forEach(function (item) {
							if (!updateApi && item.ignore_state === 2 || !updateApi && item.assign_state === 2)
								updateApi = true;
							item.ignore_state = 0;
						});
						break;
					case "assets-newonly":
						var updateApi = false;
						self.data.assets.new.items.forEach(function (item) {
							if (!updateApi && item.ignore_state === 2 || !updateApi && item.assign_state === 2)
								updateApi = true;
							item.ignore_state = 0;
						});
						break;
					case "assets-silenced":
						var updateApi = false;
						self.data.assets.ignored.items.forEach(function (item) {
							if (!updateApi && item.ignore_state === 2 || !updateApi && item.assign_state === 2)
								updateApi = true;
							item.ignore_state = 0;
						});
						break;
					case "questions-openquestions":
						var updateApi = false;
						self.data.questions.open.items.forEach(function (item) {
							if (!updateApi && item.ignore_state === 2 || !updateApi && item.assign_state === 2)
								updateApi = true;
							item.ignore_state = 0;
							item.show_quick_answer = false;
							delete item.quick_answer;
						});
						break;
					case "questions-newonly":
						var updateApi = false;
						self.data.questions.new.items.forEach(function (item) {
							if (!updateApi && item.ignore_state === 2 || !updateApi && item.assign_state === 2)
								updateApi = true;
							item.ignore_state = 0;
							item.show_quick_answer = false;
							delete item.quick_answer;
						});
						break;
					case "questions-silenced":
						var updateApi = false;
						self.data.questions.ignored.items.forEach(function (item) {
							if (!updateApi && item.ignore_state === 2 || !updateApi && item.assign_state === 2)
								updateApi = true;
							item.ignore_state = 0;
							item.show_quick_answer = false;
							delete item.quick_answer;
						});
						break;
				}
				if(updateApi){
					/* Update data using REST api */
					$rootScope.$broadcast('force.init');
				}else{
					/* Update locally data */
					$rootScope.$broadcast('update.data');
				}
			}
		};

		return self;
	}
	Factory.$inject = ['$http', '$rootScope', '$timeout'];
	return Factory;

});