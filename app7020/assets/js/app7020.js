/**
 * App7020 Javascript Library
 * 
 * Loaded when main Yii layouts are executed (adm and lms) Creates global
 * variable: app7020 - Put every JS related to 7020 in this file - do NOT create
 * other global JS objects unless it is strictly required
 * 
 */
// Stop Jquery Migation Logging //
jQuery.migrateMute = true;
// **************************** //
var app7020 = false || app7020;
/**
 * @param $jQuery Object
 * @param options Options object tp pass various options from outside world
 */
(function ($, options) {
	var checkingTimer;
	var globalTagsContainer = null;
	var checkTopicTreeInit = false;
	// Prevent this whole JS to execute more than one time
	if (app7020) {
		debug('app7020 JS already loaded & executed!');
		return;
	}

	/**
	 * Lets have a debug function
	 */
	function debug() {
		if (options.debug)
			Docebo.log.apply(this, arguments);
	}

	/**
	 * Constructor (calls init())
	 */
	var app7020Class = function (options) {
		return this.init(options);
	}

	/**
	 * Constructor (calls init())
	 */
	var app7020Publish = function (options) {
		return this.init(options);
	}

	function preventDisabledlinks() {
		$(document).on('click', 'a.disabled', function (a) {
			a.preventDefault();
		});
	}

	/**
	 * Prototype
	 */
	app7020Class.prototype =
			{
				defaultOptions: {},
				/**
				 * Initialize
				 */
				init: function (options) {
					// Merge incoming options to default ones
					this.options = $.extend(this.options, this.defaultOptions, options);
					// prevent all disabled links
					preventDisabledlinks();
					// Do all stuff that does NOT require DOM to be ready yet
					// Better please Add functions (methods) please, do NOT put
					// all
					// your
					// --- return the object itself
					return this;
				},
				/**
				 * Add all possible GLOBAL listeners here. It doesn't harm to
				 * put REALLY ALL of them here, for all UIs, but you can create
				 * more "add listeners" methods for different UIs for better
				 * management and call them when UI is loaded.
				 */
				addGlobalListeners: function () {
					$(document).on('click', '.customDropdown-widget .listArray li div.link', $.proxy(this.dropdownControls, this));
					this.generalSettings.init();
					this.knowledgeSingle.init();
					this.app7020GlobalMethods.init();
					this.publishEvents();
					this.app7020RadioSwitches.init();
					this.sharePermissions.init();
					this.customSettings.init();
					this.app7020FadeOut.init();
					this.app7020KLeditSingle.init();
					this.guruDashboard.init();
					this.assetSummaryReport.init();
					this.assetExpertReport.init();
					this.userReportQuestionsAndAnswers.init();
					this.userReportSharingActivity.init();
					this.userReportAssetsRanks.init();
					return this;
				},
				/**
				 * Lisener for publishing
				 * @returns {undefined}
				 */
				publishEvents: function () {
					var pub = new app7020Publish({});
					$(document).on('click', '.publishing', function () {
						$('#metaInformation a[href="#details"]').tab('show');
						pub.publish();
					});
					$(document).on('click', '.unpublish', function () {
						$('#metaInformation a[href="#details"]').tab('show');
						pub.unpublish();
					});
					if ($('#addnewAnswerArea').length > 0) {
						new app7020.editor({textarea: 'addnewAnswerArea'});
					}
				},
				treeWidgetInit: function (event, data) {
					var tree, preSelected = [];
					data.tree.visit(function (node) {
						node.setSelected(false);
					});
					if (data.tree.$div.is('[data-preselected]')) {
						preSelected = data.tree.$div.attr('data-preselected').split(",");
						data.tree.visit(function (node) {
							if ($.inArray(node.key, preSelected) != '-1') {
								node.setSelected(true);
							}
						});
					}
					$(".topicWidgetSearch").keyup(function (e) {
						tree = $(this).closest('.widgetBody').find('table[id^="treeWidgetContainer"]').fancytree('getTree');
						var match = $(this).val();
						var opts = {
							autoExpand: true,
							leavesOnly: false
						}
						if (e && e.which === $.ui.keyCode.ESCAPE || $.trim(match) === "") {
							$(".topicWidgetSearch").val("");
							tree.clearFilter();
							return;
						}
						tree.filterNodes(match, opts);
					});
				},
				treeWidgetRenderColumns: function (event, data) {
					var node = data.node;
					var nodeHtmlObj = $(node.tr);
					if (node.data.level == 1) {
						nodeHtmlObj.hide();
					}
				},
				customTooltipReady: function () {
					// Topics main - action buttons
					var tooltip = $('#expertsManagement .main-actions .info, #topicsManagement .main-actions .info');
					$(document).on('mouseenter', '.main-actions li', function () {
						tooltip.find('h4').html(jQuery(this).find('a').data('dialog-title'));
						tooltip.find('p').html(jQuery(this).find('a').attr('alt'));
						tooltip.show();
					});
					$(document).on('mouseleave', '.main-actions li', function () {
						tooltip.find('h4, p').html('');
						tooltip.hide();
					});
				},
				assetSummaryReport: {
					init: function () {
						var that = this;

						if ($('#App7020AssetSummaryReport').length > 0) {

							this.autocompleteAsset();
							this.searchAsset();
							this.overviewChart();
							this.socialChart();
							this.invitationDoughnut();
							this.expertBarChart();
						}
						if ($('#App7020ChannelSummaryReport').length > 0) {
							this.channelAutocomplete();
							this.channelSearch();
							this.channelAssets();
							this.channelAssetsViews();
							this.channelQuestionsAnswers();
							this.downloadChannelOverviewPdf();
						}
					},
					overviewChart: function () {
						// Overview chart
						var points = $.map(app7020AssetSummaryOverview.chartData.points, function (el) {
							return el;
						});
						var labels = $.map(app7020AssetSummaryOverview.chartData.labels, function (el) {
							return el;
						});
						var scaleoptions = [];
						function maxpoints() {
							if (Math.max.apply(Math, points) % 10 === 0 || Math.max.apply(Math, points) < 10) {
								scaleoptions.push({
									scaleSteps: Math.max.apply(Math, points) + 1,
									scaleOverride: true,
									scaleStartValue: 0,
									scaleStepWidth: 1
								});
								scaleoptions.forEach(function (a) {
									scaleoptions.scaleSteps = a.scaleSteps;
									scaleoptions.scaleOverride = a.scaleOverride;
									scaleoptions.scaleStartValue = a.scaleStartValue;
									scaleoptions.scaleStepWidth = a.scaleStepWidth;
								});

							} else {
								scaleoptions.push({
									scaleOverride: false
								});
								scaleoptions.forEach(function (a) {
									scaleoptions.scaleOverride = a.scaleOverride;
								});
							}
							return scaleoptions;
						}
						;
						//run the maxpoints

						var data = {
							labels: labels,
							datasets: [
								{
									fillColor: "rgba(4,100,174,0)",
									strokeColor: "rgba(4,100,174,1)",
									pointColor: "rgba(255,255,255,1)",
									pointStrokeColor: "rgba(4,100,174,1)",
									highlightFill: "rgba(4,100,174,0)",
									highlightStroke: "rgba(4,100,174,1)",
									data: points,
									bezierCurve: false,
								}
							]
						};
						var options = {
							scaleOverride: scaleoptions.scaleOverride,
							scaleStartValue: scaleoptions.scaleStartValue,
							scaleStepWidth: scaleoptions.scaleStepWidth,
							scaleSteps: scaleoptions.scaleSteps,
							bezierCurve: false,
							pointDotRadius: 6,
							showTooltips: false,
							scaleShowLabels: false,
							onAnimationComplete: function () {
								var ctx = this.chart.ctx;
								ctx.font = this.scale.font;
								ctx.fillStyle = this.scale.textColor
								ctx.textAlign = "center";
								ctx.textBaseline = "bottom";

								this.datasets.forEach(function (dataset) {
									dataset.points.forEach(function (points) {
										if (points.value != "0") {
											ctx.fillText(points.value, points.x, points.y - 10);
										}
									});
								})
							}
						};
						ctxOverview = new Chart(document.getElementById("myChartOverview").getContext("2d")).Line(data, options);
						$("#overviewTimeframe").change(function () {
							var assetId = $("#App7020AssetSummaryReport").data('asset-id');
							$.ajax({
								url: app7020Options.assetSummaryController + '/AxTimeOverview',
								data: 'json',
								type: "POST",
								data: {
									assetId: assetId,
									timeframe: $(this).val()
								},
								success: function (result) {
									var result = JSON.parse(result);
									$("#totalViews").html(result.totalViews);
									data.labels = $.map(result.chartData.labels, function (el) {
										return el;
									});
									data.datasets[0].data = $.map(result.chartData.points, function (el) {
										return el;
									});
									if (typeof (ctxOverviewNew) == "object") {
										ctxOverviewNew.destroy();
									}
									maxpoints();
									ctxOverview.destroy();
									ctxOverviewNew = new Chart(document.getElementById("myChartOverview").getContext("2d")).Line(data, options);
								}
							});
						});
					},
					socialChart: function () {
						// Social Chart
						var points = $.map(app7020AssetSummarySocial.chartData.points, function (el) {
							return el
						});
						var labels = $.map(app7020AssetSummarySocial.chartData.labels, function (el) {
							return el
						});
						var scaleoptions = [];
						function maxpoints(a) {
							if (Math.max.apply(Math, points) % 10 === 0 || Math.max.apply(Math, points) < 10) {
								scaleoptions.push({
									scaleSteps: Math.max.apply(Math, points) + 1,
									scaleOverride: true,
									scaleStartValue: 0,
									scaleStepWidth: 1
								});
								scaleoptions.forEach(function (a) {
									scaleoptions.scaleSteps = a.scaleSteps;
									scaleoptions.scaleOverride = a.scaleOverride;
									scaleoptions.scaleStartValue = a.scaleStartValue;
									scaleoptions.scaleStepWidth = a.scaleStepWidth;
								});

							} else {
								scaleoptions.push({
									scaleOverride: false
								});
								scaleoptions.forEach(function (a) {
									scaleoptions.scaleOverride = a.scaleOverride;
								});
							}
							return scaleoptions;
						}
						;
						//run the maxpoints

						var data = {
							labels: labels,
							datasets: [
								{
									fillColor: "rgba(4,100,174,0)",
									strokeColor: "rgba(4,100,174,1)",
									pointColor: "rgba(255,255,255,1)",
									pointStrokeColor: "rgba(4,100,174,1)",
									highlightFill: "rgba(4,100,174,0)",
									highlightStroke: "rgba(4,100,174,1)",
									data: points
								}
							]
						}
						var options = {
							bezierCurve: false,
							pointDotRadius: 6,
							showTooltips: false,
							scaleOverride: scaleoptions.scaleOverride,
							scaleStartValue: scaleoptions.scaleStartValue,
							scaleStepWidth: scaleoptions.scaleStepWidth,
							scaleSteps: scaleoptions.scaleSteps,
							onAnimationComplete: function () {
								var ctx = this.chart.ctx;
								ctx.font = this.scale.font;
								ctx.fillStyle = this.scale.textColor
								ctx.textAlign = "center";
								ctx.textBaseline = "bottom";

								this.datasets.forEach(function (dataset) {
									dataset.points.forEach(function (points) {
										if (points.value != "0") {
											ctx.fillText(points.value, points.x, points.y - 10);
										}
									});
								})
							}
						};
						ctxSocial = new Chart(document.getElementById("myChartSocial").getContext("2d")).Line(data, options);
						$("#socialTimeframe").change(function () {
							var assetId = $("#App7020AssetSummaryReport").data('asset-id');
							$.ajax({
								url: app7020Options.assetSummaryController + '/AxTimeSocial',
								data: 'json',
								type: "POST",
								data: {
									assetId: assetId,
									timeframe: $(this).val()
								},
								success: function (result) {
									var result = JSON.parse(result);
									$("#totalInvitedPeople").html(result.totalInvitations);
									data.labels = $.map(result.chartData.labels, function (el) {
										return el;
									});
									data.datasets[0].data = $.map(result.chartData.points, function (el) {
										return el;
									});
									if (typeof (ctxSocialNew) == "object") {
										ctxSocialNew.destroy();
									}
									ctxSocial.destroy();
									//$("#myChartSocial").remove();
									maxpoints();
									ctxSocialNew = new Chart(document.getElementById("myChartSocial").getContext("2d")).Line(data, options);
								}
							});
						});
					},
					invitationDoughnut: function () {
						// Invitations Doughnut  
						if (typeof (app7020WatchPercentage) == 'object') {
							var doughnutOptions = {
								//Boolean - Whether we should show a stroke on each segment
								segmentShowStroke: true,
								segmentStrokeColor: "#fff",
								segmentStrokeWidth: 2,
								percentageInnerCutout: 50,
								animationSteps: 100,
								animateRotate: true,
								animateScale: true,
								showTooltips: false
							};
							// Doughnut Chart Data
							var doughnutData = [
								{
									value: app7020WatchPercentage.watched,
									color: "#5ABF59"
								},
								{
									value: app7020WatchPercentage.notWatched,
									color: "#CCCCCC"
								}

							];
							var ctx = document.getElementById("DoughnutInvitation").getContext("2d");
							var mydoughnutChart = new Chart(ctx).Doughnut(doughnutData, doughnutOptions);
						}
					},
					expertBarChart: function () {


						// Expert chart 
						var pointsQuestions = $.map(app7020StatsExpert.questionsChartData.points, function (el) {
							return el;
						});
						var pointsAnswers = $.map(app7020StatsExpert.answersChartData.points, function (el) {
							return el;
						});
						var labels = $.map(app7020StatsExpert.answersChartData.labels, function (el) {
							return el;
						});
						var data = {
							labels: labels,
							datasets: [
								{
									label: "Questions",
									fillColor: "rgba(173,223,172,1)",
									strokeColor: "rgba(173,223,172,1)",
									pointColor: "rgba(255,255,255,1)",
									pointStrokeColor: "rgba(173,223,172,1)",
									highlightFill: "rgba(173,223,172,1)",
									highlightStroke: "rgba(173,223,172,1)",
									data: pointsQuestions

								},
								{
									label: "Answers",
									fillColor: "rgba(229,239,247,1)",
									strokeColor: "rgba(229,239,247,1)",
									pointColor: "rgba(255,255,255,1)",
									pointStrokeColor: "rgba(229,239,247,1)",
									highlightFill: "rgba(229,239,247,1)",
									highlightStroke: "rgba(229,239,247,1)",
									data: pointsAnswers
								}
							]
						};
						var options = {
							scales: {
								xAxes: [{
										categorySpacing: 2
									}]
							},
							showInlineValues: true,
							showTooltips: false,
							categoryPercentage: 0.1,
							onAnimationComplete: function () {
								if (this.options.showInlineValues) {
									if (this.name == "Bar") {
										this.eachBars(function (bar) {
											var tooltipPosition = bar.tooltipPosition();
											new Chart.Tooltip({
												x: Math.round(tooltipPosition.x),
												y: Math.round(tooltipPosition.y),
												xPadding: this.options.tooltipXPadding,
												yPadding: this.options.tooltipYPadding,
												//fillColor: bar.datasetLabel != "Answers" ? "rgba(173,223,172,1)" : "rgba(229,239,247,1)" , //fill bg 
												fillColor: "rgba(0,0,0,0)",
												textColor: "rgba(0,0,0,1)", //set text color to black
												fontFamily: this.options.tooltipFontFamily,
												fontStyle: this.options.tooltipFontStyle,
												fontSize: 12, //set font size
												caretHeight: this.options.tooltipCaretSize,
												text: bar.value != '0' ? bar.value : '',
												chart: this.chart
											}).draw();
										});
									}
								}
							}
						};
						ctxExpert = new Chart(document.getElementById("expertChart").getContext("2d")).Bar(data, options);
						$("#askTheExpert").change(function () {
							var assetId = $("#App7020AssetSummaryReport").data('asset-id');
							$.ajax({
								url: app7020Options.assetSummaryController + '/AxTimeFrameExpert',
								data: 'json',
								type: "POST",
								data: {
									assetId: assetId,
									timeframe: $(this).val()
								},
								success: function (result) {
									var result = JSON.parse(result);
									$("#totalQuestions").html(result.totalQuestions);
									$("#totalAnswers").html(result.totalAnswers);
									$("#bestAnswers").html(result.bestAnswers);
									$("#answerLikes").html(result.likes);
									$("#answerDislikes").html(result.dislikes);
									data.labels = $.map(result.questionsChartData.labels, function (el) {
										return el
									});
									data.datasets[0].data = $.map(result.questionsChartData.points, function (el) {
										return el
									});
									data.datasets[1].data = $.map(result.answersChartData.points, function (el) {
										return el
									});
									if (typeof (ctxExpertNew) == "object") {
										ctxExpertNew.destroy();
									}
									ctxExpert.destroy();
									ctxExpertNew = new Chart(document.getElementById("expertChart").getContext("2d")).Bar(data, options);
								}
							});
						});
					},
					autocompleteAsset: function () {
						$("#asset-autocomplete").autocomplete({
							source: function (request, response) {
								$.ajax({
									url: app7020Options.reportManagementController + '/assetsAutocomplete',
									dataType: "json",
									type: "POST",
									data: {
										data: {
											query: request.term
										}
									},
									success: function (data) {
										var options = [];
										$.each(data.options, function (index, value) {
											options.push({
												"value": index,
												"label": value
											})
										});
										response(options);
									}
								});
							},
							minLength: 1,
							select: function (event, ui) {
									$('#idAsset').remove();
								var hiddenField = $('<input type="hidden" id="idAsset" name="idAsset" value="' + ui.item.value + '"/>');
								$(".report-form").append(hiddenField);
								ui.item.value = ui.item.label;
							},
							focus: function (event, ui) {
								$('#idAsset').remove();
								event.preventDefault();
								$("#asset-autocomplete").val(ui.item.label);
								var hiddenField = $('<input type="hidden" id="idAsset" name="idAsset" value="' + ui.item.value + '"/>');
								$(".report-form").append(hiddenField);
							}
						});
					},
					channelAutocomplete: function () {
						$("#channel-autocomplete").autocomplete({
							source: function (request, response) {
								$.ajax({
									url: app7020Options.reportManagementController + '/channelsAutocomplete',
									dataType: "json",
									type: "POST",
									data: {
										data: {
											query: request.term
										}
									},
									success: function (data) {
										var options = [];
										$.each(data.options, function (index, value) {
											options.push({
												"value": index,
												"label": value
											})
										});
										response(options);
									}
								});
							},
							minLength: 1,
							select: function (event, ui) {
									$('#idChannel').remove();
								var hiddenField = $('<input type="hidden" id="idChannel" name="idChannel" value="' + ui.item.value + '"/>');
								$(".report-form").append(hiddenField);
								ui.item.value = ui.item.label;
							},
							focus: function (event, ui) {
								$('#idChannel').remove();
								event.preventDefault();
								$("#channel-autocomplete").val(ui.item.label);
								var hiddenField = $('<input type="hidden" id="idChannel" name="idChannel" value="' + ui.item.value + '"/>');
								$(".report-form").append(hiddenField);
							}
						});
					},
					searchAsset: function () {
						$('#generateAssetSummaryReport').on('click', function (e) {
							e.preventDefault();

							function verifyInitData() {
								var input = $("#idAsset");
								var data = input.val();
								if (!data) {
									$("#asset-autocomplete").css('border-color', '#ff0000');
									return false;
								}
								input.css('border-color', '#E4E6E5');
								return true;
							}

							if (verifyInitData()) {
								var url = $(this).attr('href'), assetId = $('input[type=hidden]#idAsset').val();
								window.location.href = url + '&asset=' + encodeURIComponent(assetId);
							}
						});
					},
					channelSearch: function () {
						$('#generateChannelSummaryReport').on('click', function (e) {
							e.preventDefault();

							function verifyInitData() {
								var input = $("#idChannel");
								var data = input.val();
								if (!data) {
									input.css('border-color', '#ff0000');
									return false;
								}
								input.css('border-color', '#E4E6E5');
								return true;
							}

							if (verifyInitData()) {
								var url = $(this).attr('href'), channelId = $('input[type=hidden]#idChannel').val();
								window.location.href = url + '&idChannel=' + encodeURIComponent(channelId);
							} else {
								$("#channel-autocomplete").css('border-color', '#ff0000');
							}
						});
					},
					channelAssets: function () {
						// Overview chart

						var points = $.map(app7020channelPublishedAssets.chartData.points, function (el) {
							return el;
						});
						var labels = $.map(app7020channelPublishedAssets.chartData.labels, function (el) {
							return el;
						});
						var scaleoptions = [];
						function maxpoints(a) {
							if (Math.max.apply(Math, points) % 10 === 0 || Math.max.apply(Math, points) < 10) {
								scaleoptions.push({
									scaleSteps: Math.max.apply(Math, points) + 1,
									scaleOverride: true,
									scaleStartValue: 0,
									scaleStepWidth: 1
								});
								scaleoptions.forEach(function (a) {
									scaleoptions.scaleSteps = a.scaleSteps;
									scaleoptions.scaleOverride = a.scaleOverride;
									scaleoptions.scaleStartValue = a.scaleStartValue;
									scaleoptions.scaleStepWidth = a.scaleStepWidth;
								});

							} else {
								scaleoptions.push({
									scaleOverride: false
								});
								scaleoptions.forEach(function (a) {
									scaleoptions.scaleOverride = a.scaleOverride;
								});
							}
							return scaleoptions;
						}
						;


						var data = {
							labels: labels,
							datasets: [
								{
									label: "My First dataset",
									fillColor: "rgba(4,100,174,0)",
									strokeColor: "rgba(245, 179, 56, 1)",
									pointColor: "rgba(255,255,255,1)",
									pointStrokeColor: "rgba(245, 179, 56, 1)",
									highlightFill: "rgba(4,100,174,0)",
									highlightStroke: "rgba(245, 179, 56, 1)",
									data: points
								}
							]
						};
						var options = {
							scaleOverride: scaleoptions.scaleOverride,
							scaleStartValue: scaleoptions.scaleStartValue,
							scaleStepWidth: scaleoptions.scaleStepWidth,
							scaleSteps: scaleoptions.scaleSteps,
							scaleShowVerticalLines: false,
							bezierCurve: false,
							pointDotRadius: 6,
							showTooltips: false,
							scaleShowLabels: false,
							onAnimationComplete: function () {
								var ctx = this.chart.ctx;
								ctx.font = this.scale.font;
								ctx.fillStyle = this.scale.textColor
								ctx.textAlign = "center";
								ctx.textBaseline = "bottom";

								this.datasets.forEach(function (dataset) {
									dataset.points.forEach(function (points) {
										if (points.value != "0") {
											ctx.fillText(points.value, points.x, points.y - 10);
										}
									});
								})
							}
						};
						ctxPublishedAssets = new Chart(document.getElementById("channelAssets").getContext("2d")).Line(data, options);
						$("#channelOverviewTimeframe").change(function () {
							var idChannel = $("#App7020ChannelSummaryReport").data('channel-id');
							$.ajax({
								url: app7020Options.channelSummaryController + '/AxTimeOverview',
								data: 'json',
								type: "POST",
								data: {
									idChannel: idChannel,
									timeframe: $(this).val()
								},
								success: function (result) {
									var result = JSON.parse(result);
									$('.raty-icons').raty('set', {
										readOnly: true, score: result.publishedAssets.averageRating
									});
									$("#publishedAssets").html(result.publishedAssets.count);
									$("#totalContributors").html(result.totalContributors);
									$("#totalAskers").html(result.totalAskers);
									$("#averageRating").html((Math.round(result.publishedAssets.averageRating * 2) / 2).toFixed(1));
									$("#currentVsAll").html(result.sharedAssetsVsTotalChannels.channelPerc + "%");
									data.labels = $.map(result.channelContributions.chartData.labels, function (el) {
										return el;
									});
									data.datasets[0].data = $.map(result.channelContributions.chartData.points, function (el) {
										return el;
									});
									data.datasets[0].pointStrokeColor = "rgba(245, 179, 56, 1)";
									data.datasets[0].highlightStroke = "rgba(245, 179, 56, 1)";
									data.datasets[0].strokeColor = "rgba(245, 179, 56, 1)";
									if (typeof (ctxPublishedAssetsNew) == "object") {
										ctxPublishedAssetsNew.destroy();
									}
									ctxPublishedAssets.destroy();
									ctxPublishedAssetsNew = new Chart(document.getElementById("channelAssets").getContext("2d")).Line(data, options);
									data.labels = $.map(result.channelAssetViews.chartData.labels, function (el) {
										return el
									});
									data.datasets[0].data = $.map(result.channelAssetViews.chartData.points, function (el) {
										return el
									});
									data.datasets[0].pointStrokeColor = "rgba(4,100,174,1)";
									data.datasets[0].highlightStroke = "rgba(4,100,174,1)";
									data.datasets[0].strokeColor = "rgba(4,100,174,1)";
									if (typeof (ctxAssetViewsNew) == "object") {
										ctxAssetViewsNew.destroy();
									}
									ctxAssetViews.destroy();
									//run the maxpoints
									maxpoints();
									ctxAssetViewsNew = new Chart(document.getElementById("channelAssetsViews").getContext("2d")).Line(data, options);
								}
							});
						});
					},
					channelAssetsViews: function () {
						// channelAssetsViews Chart

						var points = $.map(app7020channelAssetViews.chartData.points, function (el) {
							return el;
						});
						var labels = $.map(app7020channelAssetViews.chartData.labels, function (el) {
							return el;
						});
						var scaleoptions = [];
						function maxpoints(a) {
							if (Math.max.apply(Math, points) % 10 === 0 || Math.max.apply(Math, points) < 10) {
								scaleoptions.push({
									scaleSteps: Math.max.apply(Math, points) + 1,
									scaleOverride: true,
									scaleStartValue: 0,
									scaleStepWidth: 1
								});
								scaleoptions.forEach(function (a) {
									scaleoptions.scaleSteps = a.scaleSteps;
									scaleoptions.scaleOverride = a.scaleOverride;
									scaleoptions.scaleStartValue = a.scaleStartValue;
									scaleoptions.scaleStepWidth = a.scaleStepWidth;
								});

							} else {
								scaleoptions.push({
									scaleOverride: false
								});
								scaleoptions.forEach(function (a) {
									scaleoptions.scaleOverride = a.scaleOverride;
								});
							}
							return scaleoptions;
						}
						;
						//run the maxpoints
						maxpoints();

						var data = {
							labels: labels,
							datasets: [
								{
									label: "My First dataset",
									fillColor: "rgba(4,100,174,0)",
									strokeColor: "rgba(4,100,174,1)",
									pointColor: "rgba(255,255,255,1)",
									pointStrokeColor: "rgba(4,100,174,1)",
									highlightFill: "rgba(4,100,174,0)",
									highlightStroke: "rgba(4,100,174,1)",
									data: points
								}
							]
						};
						var options = {
							scaleShowVerticalLines: false,
							scaleShowLabels: false,
							bezierCurve: false,
							pointDotRadius: 6,
							showTooltips: false,
							scaleOverride: scaleoptions.scaleOverride,
							scaleStartValue: scaleoptions.scaleStartValue,
							scaleStepWidth: scaleoptions.scaleStepWidth,
							scaleSteps: scaleoptions.scaleSteps,
							onAnimationComplete: function () {
								var ctx = this.chart.ctx;
								ctx.font = this.scale.font;
								ctx.fillStyle = this.scale.textColor
								ctx.textAlign = "center";
								ctx.textBaseline = "bottom";

								this.datasets.forEach(function (dataset) {
									dataset.points.forEach(function (points) {
										if (points.value != "0") {
											ctx.fillText(points.value, points.x, points.y - 5);
										}
									});
								})
							}
						};
						ctxAssetViews = new Chart(document.getElementById("channelAssetsViews").getContext("2d")).Line(data, options);
					},
					channelQuestionsAnswers: function () {
						// channelQuestionsVsAnswers BarChart
						var pointsQuestions = $.map(app7020channelQuestionAnswers.questionAnswersChart.questionsData.points, function (el) {
							return el;
						});
						var pointsAnswers = $.map(app7020channelQuestionAnswers.questionAnswersChart.answersData.points, function (el) {
							return el;
						});
						var labels = $.map(app7020channelQuestionAnswers.questionAnswersChart.answersData.labels, function (el) {
							return el;
						});
						var data = {
							labels: labels,
							datasets: [
								{
									label: "Questions",
									fillColor: "rgba(173,223,172,1)",
									strokeColor: "rgba(173,223,172,1)",
									pointColor: "rgba(255,255,255,1)",
									pointStrokeColor: "rgba(173,223,172,1)",
									highlightFill: "rgba(173,223,172,1)",
									highlightStroke: "rgba(173,223,172,1)",
									data: pointsQuestions

								},
								{
									label: "Answers",
									fillColor: "rgba(229,239,247,1)",
									strokeColor: "rgba(229,239,247,1)",
									pointColor: "rgba(255,255,255,1)",
									pointStrokeColor: "rgba(229,239,247,1)",
									highlightFill: "rgba(229,239,247,1)",
									highlightStroke: "rgba(229,239,247,1)",
									data: pointsAnswers
								}
							]
						};
						var options = {
							scaleShowVerticalLines: false,
							scaleShowLabels: false,
							scales: {
								xAxes: [{
										categorySpacing: 2
									}]
							},
							showInlineValues: true,
							showTooltips: false,
							categoryPercentage: 0.1,
							onAnimationComplete: function () {
								if (this.options.showInlineValues) {
									if (this.name == "Bar") {
										this.eachBars(function (bar) {
											var tooltipPosition = bar.tooltipPosition();
											new Chart.Tooltip({
												x: Math.round(tooltipPosition.x),
												y: Math.round(tooltipPosition.y),
												xPadding: this.options.tooltipXPadding,
												yPadding: this.options.tooltipYPadding,
												//fillColor: bar.datasetLabel != "Answers" ? "rgba(173,223,172,1)" : "rgba(229,239,247,1)" , //fill bg 
												fillColor: "rgba(0,0,0,0)",
												textColor: "rgba(0,0,0,1)", //set text color to black
												fontFamily: this.options.tooltipFontFamily,
												fontStyle: this.options.tooltipFontStyle,
												fontSize: 12, //set font size
												caretHeight: this.options.tooltipCaretSize,
												text: bar.value != '0' ? bar.value : '',
												chart: this.chart
											}).draw();
										});
									}
								}
							}
						};
						ctxQuestionsAnswers = new Chart(document.getElementById("channelQuestionsAnswers").getContext("2d")).Bar(data, options);
						$("#channelQaTimeFrame").change(function () {
							var idChannel = $("#App7020ChannelSummaryReport").data('channel-id');
							$.ajax({
								url: app7020Options.channelSummaryController + '/AxTimeFrameQa',
								data: 'json',
								type: "POST",
								data: {
									idChannel: idChannel,
									timeframe: $(this).val()
								},
								success: function (result) {
									var result = JSON.parse(result);
									$("#totalQuestions").html(result.questions.totalQuestions);
									$("#totalAnswers").html(result.answers.totalAnswers);
									$("#bestAnswers").html(result.bestAnswers.bestAnswers);
									$("#reviewPercentage").html(result.reviews.reviewPercentage + '%');
									$("#qaPercentage").html(result.qaPercentage + '%');
									data.datasets[0].data = $.map(result.questionAnswersChart.questionsData.points, function (el) {
										return el
									});
									data.datasets[1].data = $.map(result.questionAnswersChart.answersData.points, function (el) {
										return el
									});
									data.labels = $.map(result.questionAnswersChart.answersData.labels, function (el) {
										return el
									});
									ctxQuestionsAnswersNew = new Chart(document.getElementById("channelQuestionsAnswers").getContext("2d")).Bar(data, options);
								}
							});
						});

					},
					downloadChannelOverviewPdf: function () {
						$(document).on('click', '.downloadPdf', function (e) {
							var channelId = $("#App7020ChannelSummaryReport").attr("data-channel-id");
							var timeframe = $('select#channelOverviewTimeframe option:selected').val();
							var channelAssetsChart = document.getElementById("channelAssets").toDataURL();
							var channelAssetsViewsChart = document.getElementById("channelAssetsViews").toDataURL();
							var questionsVsAnswersChart = document.getElementById("channelQuestionsAnswers").toDataURL();
							$.ajax({
								url: app7020Options.channelSummaryController + '/GenerateChannelReport',
								dataType: 'json',
								type: "POST",
								data: {
									channelId: channelId,
									timeframe: timeframe,
									channelAssetsChart: channelAssetsChart,
									channelAssetsViewsChart: channelAssetsViewsChart,
									questionsVsAnswersChart: questionsVsAnswersChart
								},
								success: function (data) {
									if (data.fileName) {
										var currentDomain = app7020Options.currentDomain;
										window.location =  app7020Options.channelSummaryController + '/downloadPdf&fileName=' + data.fileName;
									}
								}
							});
						});

					}

				},
				assetExpertReport: {
					init: function () {

						if ($('#expertSummaryReport').length > 0) {
							this.downloadPdf();
						}

					},
					downloadPdf: function () {
						$(document).on('click', '#expertSummaryPdf', function (e) {
							e.preventDefault();
							var expertId = $(this).attr("data-expertid");
							var timeframe = $('select#filter option:selected').val();
							var chartQuestions = document.getElementById("coachb-canvas").toDataURL();
							var chartAssets = document.getElementById("peerb-canvas").toDataURL();

							$.ajax({
								url: app7020Options.reportManagementController + '/GenerateExpertReport',
								dataType: 'json',
								type: "POST",
								data: {
									expertId: expertId,
									timeframe: timeframe,
									chartQuestions: chartQuestions,
									chartAssets: chartAssets
								},
								success: function (data) {
									if (data.fileName) {
										var currentDomain = app7020Options.currentDomain;
//										currentDomain = currentDomain.substring(0, currentDomain.length - 1);
										window.location = app7020Options.reportManagementController + '/downloadPdf&fileName=' + data.fileName;
									}
								}
							});
						});

					}
				},
				userReportQuestionsAndAnswers: {
					init: function () {
						var that = this;
						if ($('#userReportQuestionsAndAnswers').length > 0 && $('.user-report-summary').length == 0) {
							this.preventReload();
							this.loadBarchart();
							this.updateQuestionsAndAnswersPage(that);
							this.downloadPdf();

						}
					},
					/**
					 * 'Questions And Answers' chart 
					 * @returns {app7020_L17.app7020Class.prototype.userReport}
					 */
					loadBarchart: function (questions, answers) {
						// Set default values
						questions = typeof questions !== 'undefined' ? questions : app7020UserReportChartQuestionsAndAnswers.questions;
						answers = typeof answers !== 'undefined' ? answers : app7020UserReportChartQuestionsAndAnswers.answers;

						var pointsQuestions = $.map(questions.points, function (el) {
							return el;
						});
						var pointsAnswers = $.map(answers.points, function (el) {
							return el;
						});
						var labels = $.map(answers.labels, function (el) {
							return el;
						});
						var data = {
							labels: labels,
							datasets: [
								{
									label: "Questions",
									fillColor: "rgba(173,223,172,1)",
									strokeColor: "rgba(173,223,172,1)",
									pointColor: "rgba(255,255,255,1)",
									pointStrokeColor: "rgba(173,223,172,1)",
									highlightFill: "rgba(173,223,172,1)",
									highlightStroke: "rgba(173,223,172,1)",
									data: pointsQuestions

								},
								{
									label: "Answers",
									fillColor: "rgba(229,239,247,1)",
									strokeColor: "rgba(229,239,247,1)",
									pointColor: "rgba(255,255,255,1)",
									pointStrokeColor: "rgba(229,239,247,1)",
									highlightFill: "rgba(229,239,247,1)",
									highlightStroke: "rgba(229,239,247,1)",
									data: pointsAnswers
								}
							]
						};

						var options = {
							responsive: true,
							scales: {
								xAxes: [{
										categorySpacing: 2
									}]
							},
							showInlineValues: true,
							showTooltips: false,
							categoryPercentage: 0.1,
							onAnimationComplete: function () {
								if (this.options.showInlineValues && this.name == "Bar") {
									this.eachBars(function (bar) {
										var tooltipPosition = bar.tooltipPosition();
										new Chart.Tooltip({
											x: Math.round(tooltipPosition.x),
											y: Math.round(tooltipPosition.y),
											xPadding: this.options.tooltipXPadding,
											yPadding: this.options.tooltipYPadding,
											fillColor: "rgba(0,0,0,0)",
											textColor: "rgba(0,0,0,1)",
											fontFamily: this.options.tooltipFontFamily,
											fontStyle: this.options.tooltipFontStyle,
											fontSize: 12,
											caretHeight: this.options.tooltipCaretSize,
											text: bar.value != '0' ? bar.value : '',
											chart: this.chart
										}).draw();
									});
								}
							}
						};
						if(document.getElementById("app7020ChartQuestionsAndAnswers"))
							new Chart(document.getElementById("app7020ChartQuestionsAndAnswers").getContext("2d")).Bar(data, options);
						if(document.getElementById("app7020ChartQnA"))
							new Chart(document.getElementById("app7020ChartQnA").getContext("2d")).Bar(data, {showTooltips: false});

						return this;
					},
					/**
					 * Update all info boxes and charts on change of dropdown "Timeframe"
					 * @returns {app7020_L17.app7020Class.prototype.userReport}
					 */
					updateQuestionsAndAnswersPage: function (that) {
						$('#userReportQuestionsAndAnswers select#timeframeSelect').on('change', function () {
							var timeframe = $(this).val();
							var userId = $('#userReportQuestionsAndAnswers').data("user-id");

							$.ajax({
								url: app7020Options.myActivitiesController + '/axGetQuestionsAndAnswersContent',
								dataType: 'json',
								type: "POST",
								data: {
									userId: userId,
									timeframe: timeframe
								},
								success: function (data) {
									// Change info boxes content
									$('#answeredQuestions').text(data.userReportData.answeredQuestions);
									$('#questionsMade').text(data.userReportData.questionsMade);
									$('#answersLikes').text(data.userReportData.likes);
									$('#answersDislekes').text(data.userReportData.dislikes);
									$('#bestAnswers').text(data.userReportData.bestAnswers);
									// Change chart 'Activity per channel'
									$('.activity-per-channel-container').html(data.userReportData.chartMyActivityPerChannelHTML);
									// Change chart 'Questions vs Answers'
									that.loadBarchart(data.userReportData.chartQuestionsAnswers.questions, data.userReportData.chartQuestionsAnswers.answers);
								}
							});




						});
						return this;
					},
					preventReload: function () {
						$(document).on('click', '.sidebar a', function (e) {
							if ($(this).hasClass('active')) {
								e.preventDefault();
							}
						});
					},
					downloadPdf: function () {
						var pdfButton = '.report-action-pdf';
						// Prevent using standart LMS generation of PDF
						$(pdfButton).addClass('disabled-pdf');
						$(document).on('click', pdfButton, function () {
							var userId = $('#userReportQuestionsAndAnswers').attr('data-user-id');
							var timeframe = $('select#timeframeSelect option:selected').val();
							if(document.getElementById("app7020ChartQuestionsAndAnswers"))
								var chartQuestionsAnswers = document.getElementById("app7020ChartQuestionsAndAnswers").toDataURL();
							if(document.getElementById("app7020ChartQnA"))
								var chartQuestionsAnswers = document.getElementById("app7020ChartQnA").toDataURL();

							$.ajax({
								url: app7020Options.reportManagementController + '/generateQuestionsAnswersPdf',
								dataType: 'json',
								type: "POST",
								data: {
									userId: userId,
									timeframe: timeframe,
									chartQuestionsAnswers: chartQuestionsAnswers
								},
								success: function (data) {
									if (data.fileName) {
										var currentDomain = app7020Options.currentDomain;
										window.location = currentDomain + app7020Options.reportManagementController + '/downloadPdf&fileName=' + data.fileName;
									}
								}
							});
						});

					}
				},
				/**
				 * ChartJS wraper
				 */
				chartJsWraper: {
					/**
					 * Load ChartJs Line chart to given element
					 * @param str !!!without '#' !!! elementToAttach
					 * @param obj pointsData
					 * @param obj labelsData
					 * @param str color
					 * @returns {app7020_L17.app7020Class.prototype.userReportSharingActivity}
					 */
					lineChart: function (idElementToAttach, pointsData, labelsData, color) {
						// Set default color
						color = typeof color !== 'undefined' ? color : 'rgba(4,100,174,1)';

						var points = $.map(pointsData, function (el) {
							return el;
						});
						var labels = $.map(labelsData, function (el) {
							return el;
						});
						var scaleoptions = [];
						function maxpoints(a) {
							if (Math.max.apply(Math, points) % 10 === 0 || Math.max.apply(Math, points) < 10) {
								scaleoptions.push({
									scaleSteps: Math.max.apply(Math, points) + 1,
									scaleOverride: true,
									scaleStartValue: 0,
									scaleStepWidth: 1
								});
								scaleoptions.forEach(function (a) {
									scaleoptions.scaleSteps = a.scaleSteps;
									scaleoptions.scaleOverride = a.scaleOverride;
									scaleoptions.scaleStartValue = a.scaleStartValue;
									scaleoptions.scaleStepWidth = a.scaleStepWidth;
								});

							} else {
								scaleoptions.push({
									scaleOverride: false
								});
								scaleoptions.forEach(function (a) {
									scaleoptions.scaleOverride = a.scaleOverride;
								});
							}
							return scaleoptions;
						}
						;
						//run the maxpoints
						maxpoints();

						var data = {
							labels: labels,
							datasets: [
								{
									fillColor: "rgba(4,100,174,0)",
									strokeColor: color,
									pointColor: "rgba(255,255,255,1)",
									pointStrokeColor: color,
									highlightFill: "rgba(4,100,174,0)",
									highlightStroke: "rgba(4,100,174,1)",
									data: points
								}
							]
						};
						var options = {
							scaleOverride: scaleoptions.scaleOverride,
							scaleStartValue: scaleoptions.scaleStartValue,
							scaleStepWidth: scaleoptions.scaleStepWidth,
							scaleSteps: scaleoptions.scaleSteps,
							scaleShowVerticalLines: false,
							responsive: true,
							bezierCurve: false,
							pointDotRadius: 6,
							showTooltips: false,
							onAnimationComplete: function () {
//								var ctxmax =Math.max.apply(Math, points);
								var ctx = this.chart.ctx;
								ctx.font = this.scale.font;
								ctx.fillStyle = this.scale.textColor
								ctx.textAlign = "center";
								ctx.textBaseline = "bottom";

								this.datasets.forEach(function (dataset) {
									dataset.points.forEach(function (points) {
										if (points.value != "0") {
											ctx.fillText(points.value, points.x, points.y - 10);
										}
									});
								});
							}
						};
						new Chart(document.getElementById(idElementToAttach).getContext("2d")).Line(data, options);
						return this;
					}
				},
				userReportSharingActivity: {
					init: function () {
						var that = this;
						if ($('#userReportSharingActivity').length > 0) {
							// Prevent reload of page if user clicks on current tab link
							app7020.userReportQuestionsAndAnswers.preventReload();
							// Load initialy of chart "Shared assets (contributions)"
							app7020.chartJsWraper.lineChart('app7020ChartSharedAssets', app7020UserReportSharingActivity.assets, app7020UserReportSharingActivity.periods);
							// Load initialy of chart "Assets views"
							app7020.chartJsWraper.lineChart('app7020ChartAssetsViews', app7020UserReportSharingActivity.views, app7020UserReportSharingActivity.periods, '#F4B043');
							this.updateSharingActivityPage();
							this.downloadPdf();
						}
					},
					updateSharingActivityPage: function (that) {
						$('#userReportSharingActivity select#timeframeSelect').on('change', function () {
							var timeframe = $(this).val();
							var userId = $('#userReportSharingActivity').data("user-id");

							$.ajax({
								url: app7020Options.myActivitiesController + '/axGetSharingActivityContent',
								dataType: 'json',
								type: "POST",
								data: {
									userId: userId,
									timeframe: timeframe
								},
								success: function (data) {
									// Change rating stars
									$('.raty-icons').raty('set', {
										readOnly: true, score: data.averageRating
									});
									// Change info boxes content
									$('.average-rating-container .num').text(data.averageRating);
									$('#sharedContents').text(data.sharedContents);
									$('#totalViews').text(data.totalViews);
									$('#averageWatchRate').text(data.averageWatchRate);
									// Change chart 'Activity per channel'
									$('.activity-per-channel-container').html(data.chartMyActivityPerChannelHTML);
									// Change chart "Shared assets (contributions)"
									app7020.chartJsWraper.lineChart('app7020ChartSharedAssets', data.chartsSharedAssetsAndViews.assets, data.chartsSharedAssetsAndViews.periods);
									// Change chart "Assets views"
									app7020.chartJsWraper.lineChart('app7020ChartAssetsViews', data.chartsSharedAssetsAndViews.views, data.chartsSharedAssetsAndViews.periods, '#F4B043');
								}
							});
						});
						return this;
					},
					downloadPdf: function () {
						var pdfButton = '.report-action-pdf';
						// Prevent using standart LMS generation of PDF
						$(pdfButton).addClass('disabled-pdf');
						$(document).on('click', pdfButton, function () {
							var userId = $('#userReportSharingActivity').attr('data-user-id');
							var timeframe = $('select#timeframeSelect option:selected').val();

							// Chart "Shared assets (contributions)"
							var sharedAssets = document.getElementById("app7020ChartSharedAssets").toDataURL();
							// Chart "Assets views"
							var assetsViews = document.getElementById("app7020ChartAssetsViews").toDataURL();

							$.ajax({
								url: app7020Options.reportManagementController + '/generateSharingActivityPdf',
								dataType: 'json',
								type: "POST",
								data: {
									userId: userId,
									timeframe: timeframe,
									chartSharedAssets: sharedAssets,
									chartAssetsViews: assetsViews
								},
								success: function (data) {
									if (data.fileName) {
										var currentDomain = app7020Options.currentDomain;
										window.location = currentDomain + app7020Options.reportManagementController + '/downloadPdf&fileName=' + data.fileName;
									}
								}
							});
						});
					}
				},
				/**
				 * JS for page 'lms/index.php?r=myActivities/userReport&from=reportManagement&tab=assets-rank'
				 */
				userReportAssetsRanks: {
					order: '#combo-list-view-container-assets-ranks input#orderASC',
					type: '#combo-list-view-container-assets-ranks input#orderType',
					viewColumn: '#assets-ranks .assets-ranks-header .views',
					ratingColumn: '#assets-ranks .assets-ranks-header .rating',
					invitationColumn: '#assets-ranks .assets-ranks-header .invitation',
					watchRateColumn: '#assets-ranks .assets-ranks-header .watch-rate',
					init: function () {
						if ($('#userReportAssetsRanks').length > 0) {
							this.toggleOrderBy(this.order, this.type, this.viewColumn);
							this.toggleOrderBy(this.order, this.type, this.ratingColumn);
							this.toggleOrderBy(this.order, this.type, this.invitationColumn);
							this.toggleOrderBy(this.order, this.type, this.watchRateColumn);
							$('.report-action-pdf').addClass('hidden');

						}
					},
					/**
					 * Change value of hidden field
					 * @param str order
					 * @param str viewColumn
					 * @returns {app7020_L17.app7020Class.prototype.userReportAssetsRanks}
					 */
					toggleOrderBy: function (order, type, viewColumn) {
						$(document).on('click', viewColumn, function () {
							$('#assets-ranks .assets-ranks-header > div').removeClass('current');
							$(this).addClass('current');
							$(this).toggleClass('asc');
							var this_type = $(this).data('type');
							$(type).val(this_type);
							if ($(this).hasClass('asc')) {
								$(order).val(1);
							} else {
								$(order).val(0);
							}
							$('#combo-list-form .list-view').comboListView('updateListView');
						});
						return this;
					},
					/**
					 * Toggle class of 'VIEW' coloumn after ajax update
					 * @returns {app7020_L17.app7020Class.prototype.userReportAssetsRanks}
					 */
					toggleClass: function () {
						var viewColumn = '#assets-ranks .assets-ranks-header .views';
						var ratingColumn = '#assets-ranks .assets-ranks-header .rating';
						var invitationColumn = '#assets-ranks .assets-ranks-header .invitation';
						var watchRateColumn = '#assets-ranks .assets-ranks-header .watch-rate';
						if ($('#combo-list-view-container-assets-ranks input#orderASC').val() == 1) {
							if ($('#combo-list-view-container-assets-ranks input#orderType').val() == 'rating') {
								$(ratingColumn).addClass('asc').addClass('current');
							} else if ($('#combo-list-view-container-assets-ranks input#orderType').val() == 'invitation') {
								$(invitationColumn).addClass('asc').addClass('current');
							} else if ($('#combo-list-view-container-assets-ranks input#orderType').val() == 'wr') {
								$(watchRateColumn).addClass('asc').addClass('current');
							} else {
								$(viewColumn).addClass('asc').addClass('current');
							}

						} else {

							if ($('#combo-list-view-container-assets-ranks input#orderType').val() == 'rating') {
								$(ratingColumn).removeClass('asc').addClass('current');
							} else if ($('#combo-list-view-container-assets-ranks input#orderType').val() == 'invitation') {
								$(invitationColumn).removeClass('asc').addClass('current');
							} else if ($('#combo-list-view-container-assets-ranks input#orderType').val() == 'wr') {
								$(watchRateColumn).removeClass('asc').addClass('current');
							} else {
								$(viewColumn).removeClass('asc').addClass('current');
							}
						}
						return this;
					}



				},
				/**
				 * Manage Custom Settings
				 */
				customSettings: {
					init: function () {
						var that = this;
						if ($('#app7020-general-settings-form').length > 0) {
							that.showHide();
						}
					},
					/**
					 * Show/Hide checkboxes 'Invite to watch' & 'Enable contributor to answer questions'
					 * @returns {app7020_L17.app7020Class.prototype.customSettings}
					 */
					showHide: function () {
						$('#App7020SettingsPeerReview_enableCustomSettings').change(function () {
							if ($(this).is(':checked')) {
								$('.dissabled-container').removeClass('dissabled');
							} else {
								$('.dissabled-container').addClass('dissabled');
//								$('#App7020SettingsPeerReview_inviteToWatch, #App7020SettingsPeerReview_allowContributorAnswer').prop('checked', false);
							}
						});
						return this;
					},
				},
				dropdownControls: function (e) {
					e.preventDefault();
					e.stopPropagation();
					var $target = $(e.currentTarget);
					if (!$target.is('.stop')) {
						var data = {
							action: $target.data('method'),
							selection: $target,
							event: e
						};
						// Trigger event to allow listeners to react
						$(document).trigger('customdropdown.action', data);
					}
				},
				generalSettings: {
					init: function () {
						// Make some checkboxes and radio buttons checked if
						// relevant text fields are NOT empty
						$(document).on('focusout', 'input[type="text"]#video_not_shorter_time, input[type="text"]#video_not_longer_time, input[type="text"]#limit_characters', function () {
							if ($(this).val() !== '') {
								var inputId = $(this).attr('id');
								$('input[type="text"]' + '#' + inputId).siblings('input[type="checkbox"]').attr({"checked": "checked"});
								if (inputId === 'video_not_shorter_time' || inputId === 'video_not_longer_time') {
									$('input[type="radio"]#App7020SettingsGeneralForm_video_contribute_1').attr({"checked": "checked"});
								}
							}
						});
//						$(document).on('focusout', 'input[type="text"]#file_extentions', function () {
//							if ($(this).val() !== '') {
//								$('input[type="checkbox"]#App7020SettingsGeneralForm_other_contribution').attr({"checked": "checked"});
//							}
//						});
						// If checkbox is NOT checked -> Hide container for
						// empty textfields and clear data in them
						$(document).on('click touchstart', 'input[data-act="additional"]', function (e) {
							var parent = $(this).parents('.logical-group');
							if ($(this).is(':checked') && $(this).val() !== 'allow_any_length' && $(this).val() != 0) {
								parent.find('span.additional_container').show();
							} else {
								parent.find('span.additional_container').hide();
								var add = parent.find('span.additional_container');
								add.find('input').val('');
								add.find('input[type="checkbox"]').attr({"checked": false});
							}
						});
						$(document).on('click touchstart', 'input[data-act="related"]', function (e) {
							var parent = $(this).parents('.related-items');
							if (!$(this).is(':checked')) {
								parent.find('input[type="text"]').val('');
							}
						});
					},
					ajax: function (data) {
						if (data.success === true) {
							Docebo.Feedback.show('success', Yii.t("app7020", Yii.t("app7020", "General Settings were saved successfully")));
							$(".btn.save.app7020-button").next('i').remove();
						} else {
							Docebo.Feedback.show('error', data.errorMsg, true, true, true);
							$(".btn.save.app7020-button").next('i').remove();
						}
					},
					addLoaderIcon: function () {
						$(".btn.save.app7020-button").parent().append('<i class="fa fa-spinner fa-pulse"></i>');
					}

				},
				peerReviewSettings: {
					/**
					 * Default container for proccess
					 */
					formContainer: $('#app7020-peer-review-settings'),
					/**
					 *
					 * @param {Promise} responses
					 * @returns {app7020_L19.app7020Class.prototype.generalSettings.peerReviewSettings}
					 */
					ajax: function (responses) {
						if (responses.status) {
							Docebo.Feedback.show('success', Yii.t("app7020", Yii.t("app7020", "General Settings were saved successfully")));
						} else {
							if (typeof (responses.errors) != "undefined") {
								$.each(responses.errors, function (index, error) {
									Docebo.Feedback.show('error', error, true, false, true);
								});
							}
							// we must to replace old form with new
							if (typeof (responses.form) != "undefined") {
								this.replaceForm(responses.form);
							}

						}
						this.removeLoader();
						return this;
					},
					/**
					 * Add loader into dom of button
					 * @returns {app7020_L19.app7020Class.prototype.generalSettings.peerReviewSettings}
					 */
					addLoader: function () {
						$(".btn.save.app7020-button").parent().append('<i class="fa fa-spinner fa-pulse"></i>');
						return this;
					},
					/**
					 * Remove loader
					 * @returns {app7020_L19.app7020Class.prototype.generalSettings.peerReviewSettings}
					 */
					removeLoader: function () {
						$('i.fa.fa-spinner.fa-pulse').remove();
						return this;
					},
					/**
					 * Replace form after any call to action
					 * @param {Object} html
					 * @returns {app7020_L19.app7020Class.prototype.generalSettings.peerReviewSettings}
					 */
					replaceForm: function (html) {
						this.formContainer.empty().append($(html));
						return this;
					},
					/**
					 * add new container for proccess
					 * @param {string|object} data
					 * @returns {app7020_L19.app7020Class.prototype.generalSettings.peerReviewSettings}�
					 */
					setContainer: function (data) {
						this.formContainer = $(data);
						return this;
					}
				},
				convertToArrayOfObjects: function (data) {
					var keys = data.shift(),
							i = 0, k = 0,
							obj = null,
							output = [];
					for (i = 0; i < data.length; i++) {
						obj = {};
						for (k = 0; k < keys.length; k++) {
							obj[keys[k]] = data[i][k];
						}

						output.push(obj);
					}

					return output;
				},
				app7020KLeditSingle: {
					init: function () {
						if ($('#knowledgeLibrarySingle.editSingle').length > 0) {
							this.toggleAccordion();
						}
					},
					/**
					 * Toggle Accordion
					 * @returns {app7020_L17.app7020Class.prototype.app7020KLeditSingle}
					 */
					toggleAccordion: function () {
						$(document).on('click', '.accordion-toggle.hide-details', function () {
							$('.accordion-toggle.show-details').removeClass('hide');
						});

						$(document).on('click', '.accordion-toggle.show-details', function () {
							$(this).addClass('hide');
						});
						return this;
					}
				},
				knowledgeSingle: {
					init: function () {
						if ($('#knowledgeLibrarySingle').length > 0) {

							// Switch between views to add new tooltip
							this.switchTooltipsView();
							this.toolbarPopovers();
							this.publishUnpublish();
							this.hideUnpublishMsg();

							// Bootstrap register tabs
							$('#metaInformation a').click(function (e) {
								e.preventDefault();
							});


							$('#togglePeerReview, #app7020PeerReview .headline .fa').click(function (e) {
								e.preventDefault();
								if ($(this).has('#togglePeerReview')) {
									$(this).parent().parent().addClass("active");
									var el = $('#app7020PeerReview');
									var mainContent = $('.main-content .leftSide');
									if ($('#app7020PeerReview').is(':hidden')) {
										el.show();
										mainContent.removeClass('span12').addClass('span8');
									} else {
										$(this).parent().parent().removeClass("active");
										el.hide();
										mainContent.removeClass('span8').addClass('span12');
									}
								}
							});

							$('#app7020Tooltips .headline .fa').click(function (e) {
								var leftSide = $('.main-content .leftSide');
								leftSide.removeClass('span8').addClass('span12');
								$('.rightSide').addClass('hide');
							});
							$('#tooltips').click(function () {
								if ($('#app7020Tooltips').length > 0) {
									$('.main-content .leftSide').removeClass('span12').addClass('span8');
									$('.rightSide').removeClass('hide');
								}
							});
							$('#accordionContainer').on('show', function (obj) {
								$('#accordionContainer').find('.accordion-group').attr('data-show', false);
								$(obj.target).closest('.accordion-group').attr('data-show', true);
							});
							$('#details').on('show', function () {
								$('#details-container').attr('data-show', true);

							});
							$('#details').on('hide', function () {
								$('#details-container').attr('data-show', false);
							});
							$('#accordionContainer .accordion-toggle[data-toggle="collapse"]').on('click', function (e) {
								var currentTabClass = $(this).closest('.accordion-group').attr('class').split(' ')[1];
								if ($(this).closest('.accordion-group').children('.accordion-body').hasClass('in')) { // If
									// Open
									$(this).closest('.accordion-group').children('.accordion-body').collapse('hide');
									$('#accordionContainer .accordion-group:not(.' + currentTabClass + ')').find('.accordion-body').collapse('show');
								} else { // If Closed
									$(this).closest('.accordion-group').children('.accordion-body').collapse('show');
									$('#accordionContainer .accordion-group:not(.' + currentTabClass + ')').find('.accordion-body').collapse('hide');
								}
							});
							$('.askSomething .askBtn').click(function () {
								$('html, body').animate({scrollTop: $('#questionsApp').offset().top}, 1000);
							});
							this.detectInitalTextAreaBehavior();
						}
					},
					/**
					 * Event lsitener to Switch between views to add new tooltip
					 */
					switchTooltipsView: function () {
						$('.app7020SwitchToAddNewTooltip').on('addNewTooltip', function () {
							app7020.knowledgeSingle.showTooltipsForm();
						});
					},
					/**
					 * Switch between views to add new tooltip
					 */
					showTooltipsForm: function () {
						if ($('.app7020AddNewTooltip').hasClass('hide')) {
							$('.app7020Info').addClass('hide');
							$('.app7020AddNewTooltip').removeClass('hide');
						} else {
							$('.app7020AddNewTooltip').addClass('hide');
							$('.app7020Info').removeClass('hide');
						}
					},
					/**
					 * Detect all doms needed for assign
					 *
					 * @todo
					 * @returns {undefined}
					 */
					detectInitalTextAreaBehavior: function () {
						var that = this;
						$('textarea[id^="app7020AddNew-"]').each(function (index, DOM) {
							app7020.autoResizeTextarea(DOM);
						});
					},
					/**
					 * Attach Editor to textarea dom
					 *
					 * @todo
					 * @param {type} DOM
					 * @returns {undefined}
					 */
					appendSingleTinyMce: function (DOM) {
						var id = $(DOM).attr('id');
						app7020.tinyMCE.removeEditors(id);
						TinyMce.attach("#" + id, {height: 0,
							plugins: ["image moxiemanager"],
							toolbar: "bold italic underline image",
							menubar: false,
							statusbar: false,
							language: yii.language  // this one is set in
									// /common/controllers/Controller.php,
									// beforeRender()
						});
					},
					addContentQuestion: function (contentId) {
						var question = new app7020.question({contentId: contentId, textarea: 'addnewAnswerArea'});
						question.insert();
						return this;
					},
					toolbarPopovers: function () {
						$('.tools .fa').popover({
							placement: 'top',
							trigger: 'hover',
						});
					},
					publishUnpublish: function () {
						$(document).on('click', '.unpublished', function () {
							if ($('#switch').prop('checked')) {
								$('#switch').prop('checked', false).trigger("change");
							}
						});

						$(document).on('click', '.published', function () {
							if (!$('#switch').prop('checked')) {
								$('#switch').prop('checked', true).trigger("change");
							}
						});

						$(document).on('change', '#switch', function () {
							var pub = new app7020Publish({});
							if ($(this).prop('checked')) {
								pub.publish();

							} else {
								pub.unpublish();
							}
						});
						return this;
					},
					/**
					 * Hide message for successful Unpublishing of content
					 * @returns {app7020_L17.app7020Class.prototype.knowledgeSingle}
					 */
					hideUnpublishMsg: function () {
						var msgContainer = '#knowledgeLibrarySingle #unblublishMsgContainer';
						$(document).on('click', msgContainer + ' i.fa-times', function () {
							$(msgContainer).fadeOut();
						});
						return this;
					},
				},
				tinyMCE: {
					/**
					 * Remove(DESTROY) all editors by prefixID
					 * @param {string} prefixID
					 * @param {bool} isFullID
					 */
					removeEditors: function (prefixID, isFullID) {
						if (typeof (prefixID) !== 'undefined' && typeof (tinymce) !== 'undefined') {
							$.each(tinymce.editors, function (i, obj) {
								var editorIndex = i;
								if (typeof obj !== 'undefined') {
									var checkingElm = $(obj.targetElm).is('[id^="' + prefixID + '"]');
									if (isFullID === true) {
										checkingElm = $(obj.targetElm).is('[id="' + prefixID + '"]');
									}
									if (checkingElm) {
										try {
											obj.remove();
										} catch (e) {
											tinymce.editors = jQuery.grep(tinymce.editors, function (n, i) {
												return (i !== editorIndex);
											});
										}
									}

								}
							});
						}
					}
				},
				rate: {
					/**
					 * Reinitialize rating control (dzRaty)
					 */
					init: function (score, contentId, rateObject) {
						var result = $.ajax({
							url: app7020Options.knowledgeLibraryController + '/axRate',
							method: "POST",
							data: {score: score, contentId: contentId},
							dataType: 'json',
						});
						result.complete(function () {
							result.done(function () {
								var arrayData = JSON.parse(result.responseText);
								var newScore = parseFloat(arrayData.score);
								rateObject.raty('set', {readOnly: true, score: newScore}); // Prevent rate more than once and set selected stars
								$("#rating_score_" + contentId).text(newScore);
							});
						});
					}
				},
				assignKnowledgeGurus: {
					resetCounter: function () {
						$('#combo-list-selected-count').text(0);
						$('.combo-list-deselect-all').click();
						$('#expertsComboListView_massive_action').val('');
					}
				},
				autoResizeTextarea: function (DOM) {
					var offset = $(DOM).height();
					var resizeTextarea = function (el) {
						$(el).css('height', 'auto').css('height', ((el.scrollHeight + offset) - el.clientHeight));
					};
					$(DOM).on('keyup input', function () {
						if (((this.scrollHeight + offset) - this.clientHeight) > offset) {
							resizeTextarea(this);
						}
						if ($(this).val() === '') {
							$(this).css('height', 'auto');
							$(this).attr('data-autoresize', '');
						}
					}).removeAttr('data-autoresize');
				},
				app7020SideBar: {
					default: {
						class: 'app7020SideBar '
					},
					options: {
						event: false,
						id: 'defaultDialog2ID',
						class: '',
						title: Yii.t('app7020', 'Questions & Answers'),
						action: app7020Options.askGuruController + '/renderSidebar',
						method: 'GET',
						ajaxData: {},
					},
					init: function (opt) {
						if (typeof (opt) === 'object') {
							this.options = $.extend(this.options, opt);
						}
						this.listeners();
						this.open();
					},
					open: function () {
						openDialog(this.options.event, this.options.title, this.options.action, this.options.id, this.default.class + this.options.class, this.options.method, this.options.ajaxData);
						$(document).controls();
					},
					listeners: function () {
						$(document).one('dialog2.opened', '.' + this.default.class, function () {
							$('body').css({'overflow': 'hidden'});
						});
						$(document).one('dialog2.closed', '.' + this.default.class, function () {
							$('body').css({'overflow': ''});
						});
					}
				},
				app7020Tooltips: {
					options: {
						animation: true,
						selector: $(this),
						html: true,
						placement: 'top',
						trigger: 'hover',
						title: function () {
							return $(this).closest('.customGuruCircles-widget').find('.tooltipHtml').html();
						}
					},
					init: function (opt) {
						if (typeof (opt) === 'object') {
							this.options = $.extend(this.options, opt);
						}
						if ($('.customGuruCircles-widget [data-toggle="tooltip"]').length > 0) {
							$('.customGuruCircles-widget [data-toggle="tooltip"]').tooltip(this.options);
						}
					},
				},
//				loApp7020TagsAndTopics: {
//					init: function () {
//						$(document).on('change', '#enableApp7020', function () {
//							if ($(this).is(':checked')) {
//								$('#lo-7020-tags-topics #lo-7020-tags-topics-container').show();
//								$('#lo-7020-tags-topics #checkBoxHint').hide();
//							} else {
//								$('#lo-7020-tags-topics #lo-7020-tags-topics-container').hide();
//								$('#lo-7020-tags-topics #checkBoxHint').show();
//							}
//						});
//						$(document).on('focus', '.tagsField.tags-input.tag-input, [data-custom="treeWidgetContainer"]', function () {
//							$('.app7020-lo-topics-hint').hide();
//						});
//					}
//				},
				/**
				 * More information you can find here
				 * WIDGET -> common.widgets.app7020RadioSwitches
				 * CSSC -> app7020.scss.custom.templates._app7020RadioSwitches.scss
				 */
				app7020RadioSwitches: {
					/**
					 * Default Options
					 */
					options: {
						containerClass: '.app7020RadioSwitches', // Widget container class
						switchName: 'switch-',
						eventName: 'app7020RadioSwitches',
						debug: false
					},
					/**
					 * Attach listeners and call to fireEvent
					 */
					listener: function () {
						$(document).on('change', this.options.containerClass + ' input[name^="' + this.options.switchName + '"]', $.proxy(this.fireEvent, this));
					},
					/**
					 * Fire trigger Event with name predefined in "options"
					 * when any listener make it call
					 */
					fireEvent: function (e) {
						if (e.type === 'change') {
							var data = {
								action: e.type,
								selection: $(e.currentTarget),
								event: e
							};
							// Trigger event to allow listeners to react
							$(document).trigger(this.options.eventName, data);
						}
					},
					/**
					 * Initial method of object
					 */
					init: function (opt) {
						var that = this;
						if (typeof (opt) === 'object') {
							this.options = $.extend({}, this.options, opt);
						}
						if (this.options.debug) {
							debug('app7020RadioSwitches :: Init() options');
							debug(this.options);
						}
						that.listener();
						$(document).on(this.options.eventName, function (e, data) {
							if (that.options.debug) {
								debug('app7020RadioSwitches :: Debug Data');
								debug(data);
							}
						});
					}
				},
				app7020GlobalMethods: {
					init: function () {
						/**
						 * Hide parent element when click on
						 * <i class="fa fa-times app7020-button-hideThis"></i>
						 * Place this html button as first child of element who need to close when clicked on button
						 */
						$(document).on('click', '.app7020-button-hideThis', function (e) {
							e.preventDefault();
							e.stopPropagation();
							$(this).parent().fadeOut(500);
						});
						$(document).on('mouseover', '.tookInChargeLabel .fa', function (e) {
							if (e.type == 'mouseover') {
								$(this).tooltip({
									placement: 'top',
									title: Yii.t('app7020', 'Upload asset'),
								});
								$(this).tooltip('show');
							}
						});
					}
				},
				sharePermissions: {
					options: {
						container: '#app7020-share-permissions-form',
						debug: false,
					},
					init: function (opt) {
						if (typeof (opt) === 'object') {
							$.extend(this.options, opt);
						}
						if (this.options.debug) {
							debug('Share permissions :: Init() options');
							debug(this.options);
						}
						this.manageDialogs();
						$(document).on('click', this.options.container + ' #share7020-save-permissions', function () {
							var dataP = $('#app7020-share-permissions-form').serialize();

							$.ajax({
								url: app7020Options.share7020AppController + '/axSavePermissions',
								method: "POST",
								data: dataP,
								dataType: 'json'
							}).done(function (response) {
								Docebo.Feedback.show('success', Yii.t("app7020", "Settings were saved successfully"));
							});
						});

					},
					manageDialogs: function () {
						var that = this;

						$(document).on('click', that.options.container + ' #app7020-select-groups-branches', function () {
							$("#ShareApp7020Permissions_contribute_permissions_3").attr("checked", "checked");
							openDialog(false, // Event
									Yii.t('app7020', 'Contribution permissions'), // Title
									app7020Options.share7020AppController + '/axGroupsBranches', // Url
									'app7020-share-permissions-dialog', // Dialog-Id
									'app7020-share-permissions-dialog', // Dialog-Class
									'GET', // Ajax Method
									{// Ajax Data

									}
							);
						});

						$(document).on("dialog2.content-update", '#app7020-share-permissions-dialog', function (data) {
							if ($(this).find(".auto-close").length > 0) {
								var branches = $(this).find(".auto-close").attr('data-branches');
								var groups = $(this).find(".auto-close").attr('data-groups');
								$('#permissions-branches').val(branches);
								$('#permissions-groups').val(groups);
								var branchesArray = branches.split(',');
								var groupsArray = groups.split(',');
								$('#share-permissions-counter').text(branchesArray.length + groupsArray.length);
							}
						});
					},
				},
				guruDashboard: {
					comboLists: {
						request: '#activityRequestsSatisfy #app7020TabsPlus',
						asset: '#activityAssetsReview #app7020TabsPlus',
						question: '#activityQuestions #app7020TabsPlus',
					},
					init: function () {
						if ($('#guruDashboard').length > 0) {
							$('#guruDashboard #app7020TabsPlus .app7020BootstrapTabsExtended').boostrapSmoothActiveLine();
							$('a[data-toggle="tab"]').on('shown', function (e) {
								$(document).find('li.active').trigger('click');
							});
//							$(document).on('click', this.comboLists.question + ' .media .answerNowButton, ' + this.comboLists.question + ' .media .answerCountButton', function (e) {
//								e.preventDefault();
//								var idQuestion = $(this).closest('.itemGrid').attr('data-idObject');
//								var dataWidthClass = $(this).closest('.itemGrid').attr('data-quickViewSize');
//								openDialog(false, // Event
//										Yii.t('app7020', 'Quick question view'), // Title
//										app7020Options.guruDashboardController + '/quickQuestionView', // Url
//										'app7020-quickQuestionView-dialog', // Dialog-Id
//										'app7020-quickQuestionView-dialog ' + dataWidthClass, // Dialog-Class
//										'GET', // Ajax Method
//										{// Ajax Data
//											'idQuestion': idQuestion
//										});
//								$(document).controls();
//							});
							// Quick question view MODAL (Dialog)
							$(document).on('dialog2.opened', '#app7020-quickQuestionView-dialog', function () {
								var that = $(this);
								$(document).one('dialog2.ajax-complete', $(this), function () {
									// Close dialog manualy because buttons not
									// in controll container in modal
									that.find('.close-dialog').click(function (e) {
										e.preventDefault();
										that.dialog2("close");
									});
									// Init tinymce on add new answer textarea
									app7020.tinyMCE.removeEditors('addNewAnswerField');
									(function () {
										TinyMce.attach("#addNewAnswerField", {height: 0,
											plugins: ["image moxiemanager"],
											toolbar: "bold italic underline image",
											menubar: false,
											statusbar: false,
											language: yii.language  // this one
													// is set in
													// /common/controllers/Controller.php,
													// beforeRender()
										});
									})();
									that.find('.addNewAnswerForm .sendAnswerButton').click(function (e) {
										var thatForm = $(this).closest('.addNewAnswerForm').find('#addNewAnswerField');
										if (thatForm.val().length > 0) {
											var comboListForm = $(this).closest('#app7020-quickQuestionView-dialog').find('#combo-list-form');
											comboListForm.find('#replacedAnswerFromTextarea').val(thatForm.val());
											comboListForm.find('.list-view').comboListView('updateListView');
											comboListForm.find('#replacedAnswerFromTextarea').val('');
											tinymce.get("addNewAnswerField").setContent('');
										}
									});
									app7020.mCustomScrollbar.init(that.find('.items'));
									that.find('.items').mCustomScrollbar('scrollTo', 'last');
								});
							});
							$(document).on('dialog2.closed', '#app7020-quickQuestionView-dialog', function (e) {
								$('div[role="tabpanel"].active #combo-list-form .list-view').comboListView('updateListView');
							});

							this.takeInCharge();
							this.tookInCharge();
							this.gotIt();
							this.addToIgnoreList();
							this.removeFromIgnoreList();
						}
					},
					listViewBeforeUpdate: function ($this) {
						$('body #' + $this.attr('ajaxUpdate')[0] + ' .backdropV2').addClass('fadePlusSpinIconEffect');
					},
					listViewAfterUpdate: function ($this) {
						var items = $('#listView_answers .items');
						$('#app7020-quickQuestionView-dialog .currentQuestion .countAnswers span').text($('#app7020-quickQuestionView-dialog #listView_answers .summary strong').text());
						app7020.mCustomScrollbar.init(items);
						items.mCustomScrollbar('scrollTo', items.find('.item:eq(-3)'), {
							scrollInertia: 0
						});
						$('body #' + $this.attr('ajaxUpdate')[0] + ' .backdropV2').removeClass('fadePlusSpinIconEffect');
						items.mCustomScrollbar('scrollTo', 'last', {
							scrollInertia: 250,
							timeout: 500
						});
					},
					takeInCharge: function () {
						var that = this;
						$(document).on('click', '.takeInChargeLabel a', function (e) {
							e.preventDefault();
							var questionId = $(this).closest('.itemGrid').attr('data-idObject');
							openDialog(false, // Event
									Yii.t('app7020', 'Take in charge this request'), // Title
									app7020Options.guruDashboardController + '/axConfirmTakeInCharge', // Url
									'app7020-confirmTakeInCharge-dialog', // Dialog-Id
									'app7020-confirmTakeInCharge-dialog', // Dialog-Class
									'GET', // Ajax Method
									{// Ajax Data
										'idQuestion': questionId
									});
							$(document).controls();

							$(document).on('click', '#app7020-confirmTakeInCharge-dialog .save-dialog.green.disabled', function (e) {
								e.preventDefault();
							});

							$(document).on('change', '#confirm', function (e) {
								if ($(this).is(":checked")) {
									$('.save-dialog').removeClass("disabled");
								} else {
									$('.save-dialog').addClass("disabled");
								}
							});

							$(document).on('dialog2.closed', '#app7020-confirmTakeInCharge-dialog', function () {
								if ($(this).find('a.auto-close.success').length > 0) {
									$(that.comboLists.request + ' .list-view').each(function () {
										var id = $(this).attr('id');
										$('#' + id).comboListView('updateListView');

									});
									var badge = parseInt($(that.comboLists.request + ' .app7020BootstrapTabsExtended li:eq(1) a span.badge').text(), 10);
									$(that.comboLists.request + ' .app7020BootstrapTabsExtended li:eq(1) a span.badge').text(++badge);
								}
							});
						});

						$(document).on('click', '.takeInChargeLabel.disabled a', function (e) {
							e.preventDefault();
						});
					},
					tookInCharge: function () {
						$(document).on('click', '.tookInChargeLabel .fa', function (e) {
							e.preventDefault();
							var questionId = $(this).closest('.itemGrid').attr('data-idObject');
							openDialog(false, // Event
									Yii.t('app7020', 'Upload a requested asset'), // Title
									app7020Options.guruDashboardController + '/axConfirmTookInCharge', // Url
									'app7020-confirmTookInCharge-dialog', // Dialog-Id
									'app7020-confirmTookInCharge-dialog', // Dialog-Class
									'GET', // Ajax Method
									{// Ajax Data
										'idQuestion': questionId
									});
							$(document).controls();

							$(document).on('dialog2.closed', '#app7020-confirmTookInCharge-dialog', function () {
								if ($(this).find('a.auto-close.success').length > 0) {
									window.location = app7020Options.mainContributeController + "/index&idQuestion=" + questionId;
								}
							});
						});
					},
					gotIt: function () {
						var that = this;
						var selectors = [
							this.comboLists.request + ' .itemGrid .app7020GotItInformation .gotIt',
							this.comboLists.asset + ' .items .app7020GotItInformation .gotIt',
							this.comboLists.question + ' .itemGrid .app7020GotItInformation .gotIt'
						];
						$(document).on('click', selectors.join(','), function (e) {
							e.preventDefault();
							var el = $(this);
							var request = app7020.ajaxRequest(
									$gotItUrl,
									{tabType: $(document).find('.active[data-reference]').attr('data-reference')},
									'JSON');
							request.done(function (response) {
								if (response.success === true) {
									el.closest('.app7020GotItInformation').remove();
								}
							});
						});
					},
					addToIgnoreList: function () {
						var that = this;
						var selectors = [
							that.comboLists.request + ' .itemGrid .addToIgnore',
							that.comboLists.asset + ' .itemGrid .addToIgnore',
							that.comboLists.question + ' .itemGrid .addToIgnore'
						];
						$(document).on('click', selectors.join(','), function (e) {
							e.preventDefault();
							var el = $(this);
							openDialog(false, // Event
									Yii.t('app7020', 'Ignore'), // Title
									app7020Options.guruDashboardController + '/AxConfirmAddInIgnoreList', // Url
									'app7020-confirmAddInIgnoreList-dialog', // Dialog-Id
									'app7020-confirmAddInIgnoreList-dialog', // Dialog-Class
									'GET', // Ajax Method
									{
										tabType: $(document).find('.active[data-reference]').attr('data-reference'),
										objectId: $(this).closest('.itemGrid').attr('data-idObject')
									});
						});
						$(document).on('dialog2.content-update', '#app7020-confirmAddInIgnoreList-dialog', function () {
							if ($(this).find('a.auto-close.success').length > 0) {
								var listReference = $(this).find('a.auto-close.success').attr('data-listReference');
								if (listReference == 'assets') {
									$(that.comboLists.asset + ' .list-view').each(function () {
										var id = $(this).attr('id');
										$('#' + id).comboListView('updateListView');

									});
									var badge = parseInt($(that.comboLists.asset + ' .app7020BootstrapTabsExtended li:eq(1) a span.badge').text(), 10);
									$(that.comboLists.asset + ' .app7020BootstrapTabsExtended li:eq(1) a span.badge').text(--badge);
								} else {

									$(that.comboLists.request + ' .list-view').each(function () {
										var id = $(this).attr('id');
										$('#' + id).comboListView('updateListView');
									});


									$(that.comboLists.question + ' .list-view').each(function () {
										var id = $(this).attr('id');
										$('#' + id).comboListView('updateListView');

									});
									var badge = parseInt($(that.comboLists.question + ' .app7020BootstrapTabsExtended li:eq(1) a span.badge').text(), 10);
									$(that.comboLists.question + ' .app7020BootstrapTabsExtended li:eq(1) a span.badge').text(--badge);
								}
							}
						});
					},
					removeFromIgnoreList: function () {
						var that = this;
						var selectors = [
							that.comboLists.request + ' .itemGrid .removeFromIgnore',
							that.comboLists.asset + ' .itemGrid .removeFromIgnore',
							that.comboLists.question + ' .itemGrid .removeFromIgnore'
						];
						$(document).on('click', selectors.join(','), function (e) {
							e.preventDefault();
							var el = $(this);
							var request = app7020.ajaxRequest(
									app7020Options.guruDashboardController + '/AxRemoveFromIgnoreList',
									{
										tabType: $(document).find('.active[data-reference]').attr('data-reference'),
										objectId: $(this).closest('.itemGrid').attr('data-idObject')
									},
									'JSON');
							request.done(function (response) {
								if (response.success === true) {


									var listReference = $(document).find('.active[data-reference]').attr('data-reference');

									if (listReference == 'assets') {

										$(that.comboLists.asset + ' .list-view').each(function () {
											var id = $(this).attr('id');
											$('#' + id).comboListView('updateListView');

										});
										var badge = parseInt($(that.comboLists.asset + ' .app7020BootstrapTabsExtended li:eq(1) a span.badge').text(), 10);
										$(that.comboLists.asset + ' .app7020BootstrapTabsExtended li:eq(1) a span.badge').text(++badge);
									} else {

										$(that.comboLists.request + ' .list-view').each(function () {
											var id = $(this).attr('id');
											$('#' + id).comboListView('updateListView');

										});


										$(that.comboLists.question + ' .list-view').each(function () {
											var id = $(this).attr('id');
											$('#' + id).comboListView('updateListView');

										});
										var badge = parseInt($(that.comboLists.question + ' .app7020BootstrapTabsExtended li:eq(1) a span.badge').text(), 10);
										$(that.comboLists.question + ' .app7020BootstrapTabsExtended li:eq(1) a span.badge').text(++badge);
									}

								}
							});
						});
					}
				},
				/**
				 * More information you can find here
				 * WIDGET -> common.widgets.app7020QuestionListView
				 * CSSC -> app7020.scss.custom.templates._app7020QuestionListView.scss
				 */
				app7020QuestionListView: {
					/**
					 * Default Options
					 */
					options: {
						containerClass: '.app7020QuestionListView', // Widget container class
						pageBlankClass: '.app7020QuestionListView_pageBlank',
						searchBlankClass: '.app7020QuestionListView_searchBlank',
						eventName: 'app7020QuestionListView',
						debug: false,
						topicFilter: false,
						mainFilter: false
					},
					/**
					 * Initial method of object
					 */
					init: function (opt) {
						if (typeof (opt) === 'object') {
							this.options = $.extend({}, this.options, opt);
						}
						if (this.options.debug) {
							debug('app7020QuestionListView :: Init() options');
							debug(this.options);
						}
						this.eventListenOut();
					},
					/**
					 * Listen events outside this object
					 */
					eventListenOut: function () {
						/*
						 * Listen for app7020SearchEngine
						 * and make hidden field in app7020QuestionListView if not exist
						 */
						var that = this;
						$(document).on('app7020SearchEngine', function (e, data) {
							var forListView = [];
							$.each(data.for.split(','), function (index, item) {
								forListView.push('#combo-list-form .list-view#' + item);
							});
							forListView = $(forListView.join(', '));
							$.each(forListView, function () {
								var container = $(this).closest(that.options.containerClass);
								if (data.type === 'submit' || data.type === 'reset') {
									if (container.find('#combo-list-form input[name="search_input"]').length > 0) {
										container.find('#combo-list-form input[name="search_input"]').first().val(data.value);
									} else {
										var hiddenSearch = '<input type="hidden" name="search_input" value="' + data.value + '">';
										container.find('#combo-list-form').prepend(hiddenSearch);
									}
									container.find('#combo-list-form .list-view').comboListView('updateListView');
								}
							});
						});
						$(document).on('customdropdown.action', function (e, data) {
							if (data.action == 'follow') {
								var that = data.selection;
								var questionId = that.closest('[data-idobject]').attr('data-idobject');
								var followMethod = 1;
								if (that.closest('[data-idobject]').attr('data-follow') == 'true') {
									followMethod = 2;
								}
								that.addClass('stop');
								var result = $.ajax({
									url: app7020Options.askGuruController + '/makeFollowQuestion',
									method: "POST",
									data: {question: questionId, type: followMethod},
									dataType: 'json'
								});
								result.done(function (response) {
									var questionId = $(that).closest('[data-idobject]').attr('data-idobject');
									$('[data-idobject="' + questionId + '"]').each(function () {
										if (response.follow) {
											$('[data-idobject="' + questionId + '"]')
													.attr('data-follow', 'true')
													.find('.customDropdown-widget .link[data-method="follow"]')
													.text(Yii.t('app7020', 'Unfollow'));
										} else {
											$('[data-idobject="' + questionId + '"]')
													.attr('data-follow', 'false')
													.find('.customDropdown-widget .link[data-method="follow"]')
													.text(Yii.t('app7020', 'Follow'));
										}
									});
									$(document).trigger('askGuru_UpdateFollowing');
									that.removeClass('stop');
								});
							}
						});

						$(document).on('app7020RadioSwitches', function (e, data) {

							var container = $(that.options.containerClass);
							var cobmoListViewIds = $(data.selection).data('combolistid');
							var containerArr = [];

							if (cobmoListViewIds) {
								var cobmoListViewIdsArr = [];
								if (cobmoListViewIds.search(",") > 0) {
									cobmoListViewIdsArr = cobmoListViewIds.split(',');
								} else {
									cobmoListViewIdsArr = [cobmoListViewIds];
								}
								$.each(cobmoListViewIdsArr, function (index, value) {
									containerArr.push($(container).find('#combo-list-view-container-' + value));
								});
							} else {
								containerArr.push($(container).first().find('[id^="combo-list-view-container-"]'));
							}

							if (data.action === 'change') {
								var hiddenRadioSwitch;
								$.each(containerArr, function (index, value) {
									if (value.find('#combo-list-form input[name="radioSwitch"]').length > 0) {
										value.find('#combo-list-form input[name="radioSwitch"]').first().val(data.selection.val());
									} else {
										hiddenRadioSwitch = '<input type="hidden" name="radioSwitch" value="' + data.selection.val() + '">';
										value.find('#combo-list-form').prepend(hiddenRadioSwitch);
									}
									value.find('#combo-list-form .list-view').comboListView('updateListView');
								});

							}
						});
					},
					listViewBeforeUpdate: function (obj, id, options) {

					},
					listViewAfterUpdate: function (obj, id, data) {
						/* IF this combolist is inside app7020TabsPlus Widget
						 * after update change tab link counter
						 * with correct value */
						var currentList = $('#' + id).closest('div[id^="app7020TabsPlus_"]');
						if (currentList.length > 0) {
							var currentCounter = parseInt($('#' + id + ' .summary strong').text());
							var badge = $('a[href="#' + currentList.attr('id') + '"] .badge');
							if (currentCounter > 0) {
								badge.removeClass('hide show').addClass('show');
							} else {
								badge.removeClass('hide show').addClass('hide');
							}
							badge.text(currentCounter);
							$('#askGuru #app7020TabsPlus .app7020BootstrapTabsExtended').boostrapSmoothActiveLine('recalc');
						}
						this.pageBlank(obj, id, options);

					},
					pageBlank: function (obj, id, options) {
						var container = $('#' + id).closest(this.options.containerClass);
						/**
						 * If not use search hide blank search
						 */
						container.find(this.options.searchBlankClass).removeClass('show hide').addClass('hide');
						/**
						 * Remove search title if have
						 */
//						this.searchHeadline('remove');
						/**
						 * IF blank pages not disabled and
						 * have results hide blank page
						 */
						if (container.find('.list-view .summary').length > 0 && container.attr('data-prevent-pageBlank') != 'true') {
							container.find(this.options.pageBlankClass).removeClass('show hide').addClass('hide');
						} else
						/**
						 * IF blank pages not disabled and
						 * no have results show blank page
						 */
						if (container.find('.list-view .summary').length == 0 && container.attr('data-prevent-pageBlank') != 'true') {
							container.find('.list-view .empty').css({display: 'none'});
							container.find(this.options.pageBlankClass).removeClass('show hide').addClass('show');
						}
						/**
						 * IF blank pages is disabled and
						 * have or no results make empty label available to show
						 */
						else {
							container.find('.list-view .empty').css({display: 'block'});
						}
					},
					searchBlank: function (obj, id, options) {
						var container = $('#' + id).closest(this.options.containerClass);
						/**
						 * IF use search hide blank page
						 */
						container.find(this.options.pageBlankClass).removeClass('show hide').addClass('hide');
						/**
						 * IF have results hide blank search template
						 */
						if (container.find('.list-view .summary').length > 0) {
							container.find(this.options.searchBlankClass).removeClass('show hide').addClass('hide');
//							this.searchHeadline('result');
						}
						/**
						 * IF not have results show blank search template
						 */
						else
						if (container.find('.list-view .summary').length == 0) {
							container.find('.list-view .empty').css({display: 'none'});
							container.find(this.options.searchBlankClass).removeClass('show hide').addClass('show');
//							this.searchHeadline('noResult');
						}
						/**
						 * Rollback .empty available to show
						 */
						else {
							container.find('.list-view .empty').css({display: 'block'});
						}
					},
				},
				app7020ContentListView: {
					/**
					 * Default Options
					 */
					options: {
						containerClass: '.app7020ContentListView', // Widget container class
						pageBlankClass: '.app7020ContentListView_pageBlank',
						searchBlankClass: '.app7020ContentListView_searchBlank',
						eventName: 'app7020ContentListView',
						debug: false,
						topicFilter: false,
						mainFilter: false
					},
					/**
					 * Initial method of object
					 */
					init: function (opt) {
						if (typeof (opt) === 'object') {
							this.options = $.extend({}, this.options, opt);
						}
						if (this.options.debug) {
							debug('app7020ContentListView :: Init() options');
							debug(this.options);
						}
						this.eventListenOut();
					},
					/**
					 * Listen events outside this object
					 */
					eventListenOut: function () {
						/*
						 * Listen for app7020SearchEngine
						 * and make hidden field in app7020ContentListView if not exist
						 */
						var that = this;
						$(document).on('app7020SearchEngine', function (e, data) {
							var forListView = [];
							$.each(data.for.split(','), function (index, item) {
								forListView.push('#combo-list-form .list-view#' + item);
							});
							forListView = $(forListView.join(', '));
							$.each(forListView, function () {
								var container = $(this).closest(that.options.containerClass);
								if (data.type === 'submit' || data.type === 'reset') {
									if (container.find('#combo-list-form input[name="search_input"]').length > 0) {
										container.find('#combo-list-form input[name="search_input"]').first().val(data.value);
									} else {
										var hiddenSearch = '<input type="hidden" name="search_input" value="' + data.value + '">';
										container.find('#combo-list-form').prepend(hiddenSearch);
									}
									container.find('#combo-list-form .list-view').comboListView('updateListView');
								}
							});
						});

						/*
						 * Listen for app7020RadioSwitches
						 * and make hidden field in app7020ContentListView if not exist
						 */
						$(document).on('app7020RadioSwitches', function (e, data) {
							var container = $(that.options.containerClass);
							var cobmoListViewIds = $(data.selection).data('combolistid');
							var containerArr = [];

							if (cobmoListViewIds) {
								var cobmoListViewIdsArr = [];
								if (cobmoListViewIds.search(",") > 0) {
									cobmoListViewIdsArr = cobmoListViewIds.split(',');
								} else {
									cobmoListViewIdsArr = [cobmoListViewIds];
								}
								$.each(cobmoListViewIdsArr, function (index, value) {
									containerArr.push($(container).find('#combo-list-view-container-' + value));
								});
							} else {
								containerArr.push($(container).first().find('[id^="combo-list-view-container-"]'));
							}


							if (data.action === 'change') {
								var hiddenRadioSwitch;
								$.each(containerArr, function (index, value) {
									if (value.find('#combo-list-form input[name="radioSwitch"]').length > 0) {
										value.find('#combo-list-form input[name="radioSwitch"]').first().val(data.selection.val());
									} else {
										hiddenRadioSwitch = '<input type="hidden" name="radioSwitch" value="' + data.selection.val() + '">';
										value.find('#combo-list-form').prepend(hiddenRadioSwitch);
									}
									value.find('#combo-list-form .list-view').comboListView('updateListView');
								});

							}
						});
					},
					listViewBeforeUpdate: function (obj, id, options) {
					},
					listViewAfterUpdate: function (obj, id, data) {
						var container = $(this.options.containerClass).first();
						container.find('.itemGrid').removeClass(function (index, css) {
							return (css.match(/(^|\s)span\S+/g) || []).join(' ');
						});
						container.find('.itemGrid').addClass('span' + 12 / container.attr('data-app7020-gridColumns'));
						this.pageBlank(obj, id, data);
					},
					pageBlank: function (obj, id, data) {
						var container = $('#' + id).closest(this.options.containerClass);
						/**
						 * If not use search hide blank search
						 */
						container.find(this.options.searchBlankClass).removeClass('show hide').addClass('hide');
						/**
						 * Remove search title if have
						 */
						this.searchHeadline('remove');
						/**
						 * IF blank pages not disabled and
						 * have results hide blank page
						 */
						if (container.find('.list-view .summary').length > 0 && container.attr('data-prevent-pageBlank') != 'true') {
							container.find(this.options.pageBlankClass).removeClass('show hide').addClass('hide');
						} else
						/**
						 * IF blank pages not disabled and
						 * no have results show blank page
						 */
						if (container.find('.list-view .summary').length == 0 && container.attr('data-prevent-pageBlank') != 'true') {
							container.find('.list-view .empty').css({display: 'none'});
							container.find(this.options.pageBlankClass).removeClass('show hide').addClass('show');
						}
						/**
						 * IF blank pages is disabled and
						 * have or no results make empty label available to show
						 */
						else {
							container.find('.list-view .empty').css({display: 'block'});
						}
					},
					searchBlank: function (obj, id, data) {
						var container = $('#' + id).closest(this.options.containerClass);
						/**
						 * IF use search hide blank page
						 */
						container.find(this.options.pageBlankClass).removeClass('show hide').addClass('hide');
						/**
						 * IF have results hide blank search template
						 */
						if (container.find('.list-view .summary').length > 0) {
							container.find(this.options.searchBlankClass).removeClass('show hide').addClass('hide');
							this.searchHeadline('result');
						}
						/**
						 * IF not have results show blank search template
						 */
						else
						if (container.find('.list-view .summary').length == 0) {
							container.find('.list-view .empty').css({display: 'none'});
							container.find(this.options.searchBlankClass).removeClass('show hide').addClass('show');
							this.searchHeadline('noResult');
						}
						/**
						 * Rollback .empty available to show
						 */
						else {
							container.find('.list-view .empty').css({display: 'block'});
						}
					},
					searchHeadline: function (method) {
						var container = $(this.options.containerClass).first();
						var headline = container.siblings('.app7020SearchHeadline');
						if (headline.length > 0) {
							/* Show "Results" label and count of results */
							if (method == 'result') {
								container.siblings('.app7020-headline').hide();
								headline.show();
								headline.find('.searchLabel').hide();
								headline.find('.searchLabel.result').show();
								headline.find('.searchCount .countValue').text(container.find('.list-view .summary strong').text());
							} else
							/* Show "No Results" label and count of results */
							if (method == 'noResult') {
								container.siblings('.app7020-headline').hide();
								headline.show();
								headline.find('.searchLabel').hide();
								headline.find('.searchLabel.noResult').show();
								headline.find('.searchCount .countValue').text('0');
							}
							/* Remove added search labels from .app7020-headline */
							else {
								container.siblings('.app7020-headline').show();
								headline.hide();
							}
						} else {
							if (this.options.debug) {
								debug('Cant find sibling .app7020SearchHeadline on same level of ' + this.options.containerClass);
							}
						}
					}
				},
				ajaxRequest: function (path, data, dataType, async) {
					if (async === undefined) {
						async = true
					}
					var request = $.ajax({
						url: path,
						method: "POST",
						data: data,
						dataType: dataType
					});
					return request;
				},
				// ------------------------------------------------------------
				// -------------- GENERAL CLASSSES ----------------------------
				//-------------------------------------------------------------
				/**
				 * Notify plugin for add messages about success and corrupted proccess
				 * @param {object} options eg.{type: 'error', message: 'Lorem ipsum'}
				 * @returns {unresolved}
				 */
				notify: function (options) {
					var that = this;
					that.options = {
						type: 'error',
						message: ''
					};
					/**
					 * Initial method of object
					 * @returns {app7020_L17.app7020Class}
					 */
					that.init = function () {
						that.options = $.extend({}, that.options, options);
						Docebo.Feedback.show(that.options.type, Yii.t('app7020', that.options.message));
						return that;
					}
					return that.init();
				},
				/**
				 * tinyMCE instance WRAPER for usage in app7020 modules
				 * @param {type} options
				 * @returns {app7020_L17.app7020Class.prototype}
				 */
				editor: function (options) {
					var that = this;
					/**
					 * general options
					 * @var {object}
					 */
					that.options = {
						textarea: '', // id wirhout sharp "#"
						value: '', //current value
					}
					/**
					 * Set Id or class for current textarea
					 * @param {string} idString
					 * @returns {app7020_L17.app7020Class.prototype.askGuru.editor}
					 */
					that.setTextarea = function (idString) {
						that.textarea = idString;
						return that;
					};
					/**
					 * Return current value of tinyMCE
					 * @returns {@var;value|$@call;extend.value|String}
					 */
					that.getContent = function () {
						that._getCurrentContent();
						return that.options.value;
					};
					/**
					 * Set new Content into editor
					 * @returns {app7020_L17.app7020Class}
					 */
					that.setContent = function (string) {
						tinymce.get(that.options.textarea).setContent(string);
						return that;
					};
					/**
					 * After tinyMCE is builded we init and current value into object
					 * @returns {app7020_L17.app7020Class}
					 */
					that._getCurrentContent = function () {
						var value = tinymce.get(that.options.textarea).getContent();
						that.options.value = value;
						return that;
					}
					/**
					 * Build TinyMCE
					 * @returns {app7020_L17.app7020Class.prototype.askGuru.editor}
					 */
					that.setupTextarea = function () {
						var ed = TinyMce.attach('#' + this.textarea, {height: 0,
							toolbar: "bold italic underline image",
							plugins: ["image moxiemanager"],
							language: yii.language
						});
						return that;
					};
					/**
					 * Start implementation of new editor
					 * @param {strint} idString
					 * @returns {app7020_L17.app7020Class.prototype.askGuru.editor|Boolean}
					 */
					that.init = function () {
						$.extend(that.options, options);
						this.setTextarea(that.options.textarea);
						this.setupTextarea();
						//this._getCurrentContent();
						return that;
					};
					return that.init();
				},
				/**
				 * app7020AlertBox can be used as alternative of app7020.notify.
				 * The deference is that it MUST be called first in DOM as widget,
				 * this way options can be provided in PHP and/or JS
				 * For more info see common.widgets.app7020AlertBox
				 * @param obj options
				 */
				app7020AlertBox: function (options) {
					var that = this;
					that.alertBoxOptions = {
						id: '',
						containerClass: 'app7020AlertBox',
						success: true,
						textSuccess: '',
						textError: '',
					};

					that.containerClass = 'app7020AlertBox';

					that.init = function () {
						$.extend(that.alertBoxOptions, options);
						return that;
					};

					that.show = function () {
						var id = that.alertBoxOptions.id ? '#' + that.alertBoxOptions.id : '';
						var containerClass = '.' + that.containerClass;
						var alertBox = id + containerClass;


						if (that.alertBoxOptions.success === true) {
							$(alertBox).removeClass('error');
							$(alertBox).addClass('success');
							if (that.alertBoxOptions.textSuccess) {
								$(alertBox + ' .content.success').html(that.alertBoxOptions.textSuccess);
							} else {
								$(alertBox + ' .content.success').html($(alertBox).find('input[type="hidden"]#successText').val());
							}
						} else {
							$(alertBox).removeClass('success');
							$(alertBox).addClass('error');
							if (that.alertBoxOptions.textError) {
								$(alertBox + ' .content.error').html(that.alertBoxOptions.textError);
							} else {
								$(alertBox + ' .content.error').html($(alertBox).find('input[type="hidden"]#errorText').val());
							}
						}

						$(alertBox).fadeIn().css("display", "table");



					};

					return that.init();
				},
				/**
				 * FadeOut parent div
				 */
				app7020FadeOut: {
					init: function () {
						$(document).on('click', '#app7020FadeOut.fa-times', function () {
							$(this).parent('div').fadeOut();
						});
					}
				},
			}


	app7020Publish.prototype = {
		additionalControllers: [
			{icon: '.fa-share-square'}
		],
		ajax: function (url) {
			var request = $.ajax({
				url: url,
				method: "POST",
				data: {},
				dataType: 'json'
			});
			return request;
		},
		options: {
			publishController: '.readToPublish',
			publishUrl: app7020Options.peerReviewControllerPublish,
			unpublishUrl: app7020Options.peerReviewControllerUnPublish
		},
		/**
		 * Publish content
		 * @returns {this|jQuery|$|@exp;_$|Window.$}
		 */
		publish: function () {
			var request = this.ajax(this.options.publishUrl);
			var that = this;
			var alertBox;
//			that.proccessControllersPublish();
			request.done(function (response) {
				if (response.result) {

					alertBox = new app7020.app7020AlertBox({
						id: 'publishUnpublish',
						textSuccess: 'Content was successfully published!',
					});
					alertBox.show();
					// Redirect to View Mode
					//window.location.replace(app7020Options.knowledgeLibraryController + '/item&id=' + $('.app7020AssetModal').data('asset-id'));
					$assetId = $('.app7020AssetModal').data('asset-id')


					var url = app7020Options.inviteToWatch + '/index&id=' + $assetId;
					var id = 'app7020-invite-dialog';
					var clases = 'app7020-invite-dialog wide';
					var data = {};
					var title = Yii.t('app7020', 'Invite to Watch');
					openDialog(false, title, url, id, clases, 'GET', data);

				} else {

					alertBox = new app7020.app7020AlertBox({
						id: 'publishUnpublish',
						success: false,
						textSuccess: 'File cannot be published!'
					});
					alertBox.show();
				}
			});
			return this;
		},
		/**
		 * Unpublish the asset
		 * @returns {this|jQuery|$|@exp;_$|Window.$}
		 */
		unpublish: function () {
			var request = this.ajax(this.options.unpublishUrl);
			var that = this;
			var alertBox;
			request.done(function (response) {
				if (response.result) {
					that.removeTabContent();
					that.removeAdditionalControllers();

					alertBox = new app7020.app7020AlertBox({
						id: 'publishUnpublish',
					});
					alertBox.show();
					$('#App7020KLeditPanel .fa-bar-chart').hide();
					$('#App7020KLeditPanel .hint-exit').hide();
					$('#App7020KLeditPanel .readyToPublish').removeClass('hidden');
					$('#App7020KLeditPanel .publishedBy').addClass('hidden');
					$('#App7020KLeditPanel .editedBy').addClass('hidden');
					$('#App7020KLeditPanel .tools .fa-times').addClass('hidden');
				} else {
					alertBox = new app7020.app7020AlertBox({
						id: 'publishUnpublish',
						success: false
					});
					alertBox.show();
				}
			});
			return this;
		},
		/**
		 * Remove all additional controllers
		 * @returns {app7020_L17.app7020Publish.prototype}
		 */
		removeAdditionalControllers: function () {
			$.each(this.additionalControllers, function (index, value) {
				$('.contentInformation ' + value.icon).remove();
			});
			return this;
		},
		/**
		 * remove tab content after is unbpublish asset
		 * @returns {this|jQuery|$|@exp;_$|Window.$}
		 */
		removeTabContent: function () {
			$('li.askTheGuru').remove();
			$('#askTheGuru').remove();
			return this;
		},
		/**
		 * Replace content of publish controller
		 * @param {jXH} response
		 * @returns {this|jQuery|$|@exp;_$|Window.$}
		 */
		append: function (response) {
			var tab = $(response.ask_guru.tab);
			var tab_layout = $(response.ask_guru.tab_layout);
			tab.appendTo('#metaInformation');
			tab_layout.appendTo('.metaInformation  .tab-content');
			new app7020.editor({textarea: 'addnewAnswerArea'});
			$('#questionsComboListView').comboListView();
			return this;
		},
		/**
		 * Change controllers
		 * @param {jXH} response
		 * @returns {this|jQuery|$|@exp;_$|Window.$}
		 */
		proccessControllersPublish: function (response) {
//						$(this.options.publishController).replaceWith($(response.publish_dom));
			$(this.options.publishController).text(Yii.t('app7020', 'Please wait'));
			return this;
		},
		proccessControllersUnpublish: function (response) {
			$(this.options.publishController).replaceWith($(response.publish_dom));
			return this;
		},
		/**
		 * @todo Cosntructor
		 * @autohor @stanimir
		 */
		init: function (options) {
			return this;
		}
	};

	// --------------------------------
	// Set this GLOBAL variable to true
	app7020 = new app7020Class(options);
	// app7020.contributeCheckVideoStatus(0);

	$(function () {
		app7020.addGlobalListeners();
	});
})(jQuery, app7020Options); 