'use strict';
require.config({
	baseUrl: app7020Options.assetUrl + '/js',
	waitSeconds: 200,
	paths: {
		angular: '/themes/spt/js/angularJs/angular',
		ngOrder: '/themes/spt/js/angularJs/ng-order-object-by.js',
		angularRouting: '/themes/spt/js/angularJs/angular-route',
		assetDelete: 'assetDelete/assetDeletePlugin',
		app7020PDeleteAsset: 'plugins/app7020PDeleteAsset',
		lookingfor: '/themes/spt/js/lookingfor/jquery.lookingfor.min',
		angularResource: '/themes/spt/js/angularJs/angular-resource',
		angularCookies: '/themes/spt/js/angularJs/angular-cookies.min',
		ddSlick: '/themes/spt/js/jquery.ddslick.min',
		angularInfiniteScroll: '/themes/spt/js/angularJs/ng-infinite-scroll.min',
		pollerLib: '/themes/spt/js/angular-poller-master/angular-poller.min',
		angularSanitize: '/themes/spt/js/angularJs/angular-sanitize.min',
		flowplayerLib : '/themes/spt/js/flowplayer/flowplayer',
		frog  : '/themes/spt/js/froogaloop2.min',
		wistia: '/themes/spt/js/E-v1',
		visjsTimeline: '/themes/spt/js/visjs/vis'
 	},
	shim: {
		angular: {
			exports: 'angular'
		},
		angularRouting: {
			exports: 'angular',
			deps: ['angular']
		},
		angularCookies: {
			exports: 'angular',
			deps: ['angular']
		},
		'plugins/documentReader': {
			exports: 'app7020documentContentReader',
		},
		angularInfiniteScroll: {
			exports: 'infinite-scroll',
			deps: ['angular']
		},
		pollerLib: {
			exports: 'poller',
			deps: ['angular']
		},
		ngOrder: {
			exports: 'ngOrderObjectBy',
			deps: ['angular']
		},
		angularSanitize: {
			'exports': 'ngSanitize',
			'deps': ['angular']
		},
		flowplayerLib: {
			'exports': 'flowplayer'
		 },
		 frog: {
			exports: 'Froogaloop' 
		 }, 
		angularResource: {exports: 'ngResource', deps: ['angular']},
		priority: [
			'angular',
			'angularRouting',
			'angularCookies',
			'angularResource',
			'angularInfiniteScroll',
			'angularSanitize',
			'ngOrder',
			'pollerLib',
			'app',
		]

	}
});
define('jquery', [], function () {
	return jQuery;
});

require([
	'angular',
	'ngOrder',
	'angularRouting',
	'angularCookies',
	'angularInfiniteScroll',
	'angularSanitize',
	'app/app_AssetPlay',
	'plugins/documentReader',
 ], function (angular) {
	angular.element(document).ready(function () {
		angular.bootstrap(document, ['app7020GeneralModule']);

		$(document).on('dialog2.ajax-complete', '#app7020-invite-dialog', function () {
			angular.element(document.body).injector().invoke(function ($compile, $rootScope) {
				$compile($("#invite_to_watch"))($rootScope);
				$rootScope.$apply();
			});
		});

		$(document).on('dialog2.ajax-complete', '#app7020-confirm-delete-modal-dialog', function () {
			angular.element(document.body).injector().invoke(function ($compile, $rootScope) {
				$compile($("#confirmDeleteAssetDialog"))($rootScope);
				$rootScope.$apply();
			});
		});
	});

});