'use strict';
define(['modules/request', 'modules/lookingfor'], function (Request) {
	function Directive($timeout, $routeParams, AJAX, $rootScope, $compile) {
		return {
			restrict: 'E',
			template: '<div>Loading....</div>',
			link: function (scope, iElem, attr, ctrl) {
				Request(this).ajax('index.php?r=site/view&view_name=assetPlay.assetEdit&idAsset=' + $routeParams.id, {},
					function (response) {
						iElem.html($compile(response)(scope));
						$timeout(function () {
							if ($('#App7020Assets_description').length > 0) {
								app7020.tinyMCE.removeEditors('App7020Assets_description');
								new app7020.editor({textarea: 'App7020Assets_description'});
							}
						});
					},
					function () {
					}, 'html');

				scope.submitAsset = function (index) {
					var assetData = $('.app7020AssetModal > form:first-child').serializeArray();
					assetData.push({'name': 'assetId', 'value': $routeParams.id});
					assetData.push({'name': 'status', 'value': status});  

					Request(this).ajax(app7020Options.assetsAxController + '/axSubmitAsset', assetData,
						function (response) {
							if (response.res === true) {
								scope.$emit('update.details', {
									description: response.itemInfo.description,
									title: response.itemInfo.title,
									channels: response.itemInfo.is_private ? [] : response.itemInfo.channels,
									tags: response.itemInfo.tags,
									last_edit_by: response.itemInfo.last_edit_by,
									last_edit_datetime: response.itemInfo.last_edit_datetime
								});
								$('#edit-notice').doceboNotice(Yii.t('app7020', 'Your changes has been successfully saved'), 'success');
							} else {
								$('#edit-notice').doceboNotice(Yii.t('app7020', 'Channel is required for an asset! '), 'error');
							}
						},
						function () {
						});
				};
			}
		};
	}
	Directive.$inject = ['$timeout', '$routeParams', 'AJAX', '$rootScope', '$compile'];
	return Directive;
});