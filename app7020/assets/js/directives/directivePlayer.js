/* global Yii, document, YT, angular */
define(function (require) {

	function Directive($timeout, $http, $compile, $window, $interval, poller) {
		return {
			restrict: 'E',
			link: link,
			template: '<h3 class="text-center" style="padding: 50px 0;"><i class="fa fa-cog fa-spin"></i> Loading ... </h3>'
		};
		function link(scope, iElem, iAttrs) {
			scope.errors = [];
			scope.config = false;
			/* Get config params from another scope */
			if (iAttrs.external == 'true') {
				scope.$watch('player', function (value) {
					if (value !== false) {
						scope.config = value;
						scope.renderTemplate();
					}
				});
			}
			scope.$emit('spend.stop');
			if (typeof (iAttrs.assetId) == 'undefined' && iAttrs.external != 'true') {
				scope.errors.push({
					code: 404,
					message: 'The asset argument isn\'t apply'
				});
			}

			/* Get config params from REST */
			if (!scope.errors.length && iAttrs.external != 'true' && scope.config === false) {
				$http.post('/lms/index.php?r=restdata', {
					endpoint: 'asset_data',
					params: {
						id_asset: iAttrs.assetId
					}
				}).then(function ($result) {
					scope.config = $result.data.player;
					scope.renderTemplate();
				}, function ($error) {
					console.log($error);
				});
			}

			scope.ttCuesFilter = function (cue) {
				return cue.type == 'setTooltip' && cue.action == 'show';
			};

			scope.alreadyClicked = false;
			scope.setViewOnClick = function (conf) {
				//var w = window.open('', 'MyWindow');
				if (scope.alreadyClicked || scope.details.is_viewed_already) {
					//setTimeout(function () {
					//	w.location.href = conf.filename;
					//}, 80);
					return;
				}
				if (scope.details.owner_id != scope.details.logged_user) {
					scope.details.total_views++;
				}

				scope.alreadyClicked = true;
				$http.post('/lms/index.php?r=restdata', {
					endpoint: 'asset_view',
					params: {
						id_asset: conf.id
					}
				}).then(function ($result) {
					setTimeout(function () {
						w.location.href = conf.filename;
					}, 80);

				}, function ($error) {
					console.log($error);
				});
			};

			/* This method match asset type and compile specific template */
			scope.renderTemplate = function () {

				if (!scope.errors.length && scope.config !== false) {
					switch (scope.config.type) {

						/* Lms uploaded Video asset */
						case '1':
						case '12':
							iElem.html($compile(renderVideo())(scope));
							$timeout(function () {

								new scope.flowPlayer();
							});
							break;
							/* Lms uploaded Image asset */
						case '7':
							iElem.html($compile(renderImage())(scope));
							var $hasInitStart = false;
							$timeout(function () {
								if ($hasInitStart === false) {
									scope.$emit('spend.resume');
									$hasInitStart = true;
								}
								window.addEventListener('blur', function () {
									scope.$emit('spend.stop');
									$hasInitStart = false;
								});
 
								window.addEventListener('focus', function () {
									if ($hasInitStart === false) {
										scope.$emit('spend.resume');
										$hasInitStart = true;
									}
								});
								iElem.app7020documentContentReader();
							});
							break;
							/* Lms shared link */
						case '15':
							iElem.html($compile(renderLink())(scope));
							break;
							/* All convertable types files */
						case 'convertable':
							iElem.html($compile(renderConvertable())(scope));
							$timeout(function () {
								iElem.app7020documentContentReader();
							});
							break;
							/* Force download files from S3 storage */
						case 'download':
							iElem.html($compile(renderDownload())(scope));
							break;
						case 'google_drive_file':
							$http.post('/lms/index.php?r=restdata', {
								endpoint: 'asset_view',
								params: {
									id_asset: scope.config.contentId
								}
							}).then(function ($result) {
								if ($result.data) {
									/*
									 * On track add +1 view instant
									 */
									if (scope.alreadyClicked || scope.details.is_viewed_already) {
										return;
									}
									if (scope.details.owner_id != scope.details.logged_user) {
										scope.details.total_views++;
										return scope.alreadyClicked = true;
									}
								}
							});
							if (app7020Options.isActiveGoogleDrive) {
								iElem.html($compile(renderGoogleDrive(scope.config))(scope));
							} else {
								iElem.html($compile(renderLinkGoogle(scope.config.title))(scope));
							}
							require('modules/notification');
							$('.404-error').text("The supplied URL is not valid Google Drive document.");
							break;
						case 'youtube':
							iElem.html($compile(renderYoutube())(scope));
							scope.youTubeAPI();
							break;
						case 'vimeo':

							iElem.html($compile(renderVimeo())(scope));
							$timeout(function () {
								new scope.vimeoPlayer();
							});
							break;
						case 'wistia':
							iElem.html($compile(renderWistia())(scope));
							$timeout(function () {
								scope.wistiaPlayer();
							});
							break;
					}
				}
			};

			/**
			 * Generate youtube player via api
			 * @returns {undefined}
			 */
			scope.youTubeAPI = function () {
				var $this = this;
				var $trackTimer = undefined;
				var $hasInitStart = false;
				this.buildIframe = function () {
					scope.$root.YTPlayer = new $window.YT.Player(iElem.find('#YTPlayer')[0], {
						videoId: scope.config.hash,
						playerVars: {
							autoplay: 0,
							html5: 1,
							theme: "light",
							modesbranding: 0,
							color: "white",
							iv_load_policy: 3,
							showinfo: 1,
							controls: 1
						},
						events: {
							'onReady': function (event) {
								
								$trackTimer = $interval(function () {
									if (event.target.getCurrentTime() > 0.75 * event.target.getDuration())
										$this.trackYoutube();
								}, 500);
							},
							'onStateChange': function(event){
								if(event.data == 0 || event.data == 2 || event.data == 3){
									scope.$emit('spend.stop');
								}
									
								if(event.data == 1){
									scope.$emit('spend.resume');
								}
									
							}
							
						}
					});
				};
				this.trackYoutube = function () {
					if (angular.isDefined($trackTimer)) {
						$interval.cancel($trackTimer);
						$trackTimer = undefined;
						$http.post('/lms/index.php?r=restdata', {
							endpoint: 'asset_view',
							params: {
								id_asset: scope.assetId
							}
						}).then(function ($result) {
							if ($result.data) {
								/*
								 * On track add +1 view instant
								 */
								if (scope.alreadyClicked || scope.details.is_viewed_already) {
									return;
								}
								if (scope.details.owner_id != scope.details.logged_user) {
									scope.details.total_views++;
									return scope.alreadyClicked = true;
								}
							}
						});
					}
				};
				if (typeof scope.$root.YTPlayer !== 'undefined') {
					$this.buildIframe();
				} else {
					var tag = document.createElement('script');
					tag.src = "https://www.youtube.com/iframe_api";
					var firstScriptTag = document.getElementsByTagName('script')[0];
					firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
				}

				$window.onYouTubeIframeAPIReady = function () {
					$this.buildIframe();
				};
			};

			/**
			 * Flow player definition
			 * @returns {directivePlayer_L2.Directive.link.scope.flowPlayer}
			 */
			scope.flowPlayer = function () {
				var $this = this;
				$this.container = false;
				$this.content_id = false;
				$this.flowplayer = false;
				$this.flowplayerObject = false;
 
				/**
				 *
				 * @returns {directivePlayer_L2.Directive.link.scope.flowPlayer}
				 */
				$this.init = function () {
					//when we have already started player  and evend is fired once 
					var $hasInitStart = false;
					var $hasFocused = true;

					$this.container = document.getElementById(scope.config.containerId);
					$this.content_id = parseInt(scope.config.contentId);
					$this.flowplayer = require('flowplayerLib');
					$this.flowplayer.conf = scope.config.conf;
					$this.flowplayerObject = $this.flowplayer($this.container, scope.config.init);


					$this.flowplayerObject.on('seek', function (method, obj, value) {
						if (typeof value === 'undefined') {
							value = 0.001;
						}
						scope.$broadcast('timeline.seekTo', Math.round(value));
						$this.showHideCuepoints(obj.cuepoints, value);


					});

					/**
					 * 
					 */
					scope.tracked = false;
					$this.flowplayerObject.on('progress', function (method, obj, value) {
						$this.showHideCuepoints(obj.cuepoints, Math.round(value));
						scope.$broadcast('timeline.seekTo', Math.round(value));

 						/*
						 * On track add +1 view instant
						 */

						if(scope.details.type === 12 && scope.tracked  === false && value >= Math.round($this.flowplayerObject.video.duration)*0.75){
							track();
							scope.tracked = true;
						}

						if ($hasInitStart === false) {
							scope.$emit('spend.resume');
							$hasInitStart = true;
						}
					});

					/**
					 * 
					 */
					$this.flowplayerObject.on("ready", function () {
						scope.flowplayer_is_loaded = true;
						scope.$digest();
					});

					/**
					 * What does is happen when  someone pause the asset
					 */
					$this.flowplayerObject.on("pause", function () {
						$hasInitStart = false;
						scope.$emit('spend.stop');
					});

					/**
					 * What does is happen when player is resume 
					 * 
					 * case 1: when already seeker was invoked, only then we can resume spender
					 */
					$this.flowplayerObject.on("finish", function () {
						scope.$emit('spend.stop');
						$hasInitStart = false;
					});


					function track() {
						$http.post('/lms/index.php?r=restdata', {
							endpoint: 'asset_view',
							params: {
								id_asset: scope.config.contentId
							}
						}).then(function ($result) {
							if ($result.data) {

								if (scope.alreadyClicked || scope.details.is_viewed_already) {
									return;
								}
								if (scope.details.owner_id != scope.details.logged_user) {
									scope.details.total_views++;
									return scope.alreadyClicked = true;
								}
							}
						});
					}
					$this.flowplayerObject.on("cuepoint", function (e, api, cuepoint) {
						if (cuepoint.type == 'app7020ViewContent') {
 							track();
 						}
					});
					return $this;
				};

				$this.showHideCuepoints = function (cuepoints, time) {
					var cues = {};
					cuepoints.forEach(function (cue) {
						if (cue.type == "setTooltip") {
							if (typeof cues[parseInt(cue.tooltipId)] === 'undefined')
								cues[parseInt(cue.tooltipId)] = {};
							if (cue.action == 'show')
								cues[parseInt(cue.tooltipId)].start = cue.time;
							if (cue.action == 'hide')
								cues[parseInt(cue.tooltipId)].end = cue.time;
							cues[parseInt(cue.tooltipId)].hash = cue.hash;
						}
					});
					angular.forEach(cues, function (v) {
						if (time >= v.start && v.end >= time) {
							iElem.find('#docebo-flowplayer-tooltip-' + v.hash)
									.switchClass('docebo-flowplayer-hidden-tooltip', 'docebo-flowplayer-visable-tooltip');
						} else {
							iElem.find('#docebo-flowplayer-tooltip-' + v.hash)
									.switchClass('docebo-flowplayer-visable-tooltip', 'docebo-flowplayer-hidden-tooltip');
						}
					});
				};

				scope.$on('flowplayer.seekTo', function (e, v) {
					$this.flowplayerObject.seek(v, function (method, obj, value) {
						obj.pause();
					});
				});

				scope.$on('flowplayer.pause', function (e, v) {
					if (!$this.flowplayerObject.paused)
						$this.flowplayerObject.pause();

				});


				scope.$on('flowplayer.replace.cuepoints', function (e, cues) {
					$this.flowplayerObject.setCuepoints(cues);
					scope.config.init.cuepoints = cues;
					$timeout(function () {
						$this.showHideCuepoints(cues, Math.round($this.flowplayerObject.video.time));
					});
				});

				return $this.init();
			};

			/**
			 * Vimeo Player
			 * @returns {directivePlayer_L2.Directive.link.scope.init}
			 */
			scope.vimeoPlayer = function () {
				var hasTrack = false;
				var $hasInitStart = false;
				this.init = function () {
 
					$('<iframe />', {
						'id': 'player-' + scope.config.contentId,
						'src': 'https://player.vimeo.com/video/' + scope.config.hash + '?api=1&player_id=player-' + scope.config.contentId,
						'frameborder': 0,
						'webkitallowfullscreen': true,
						'mozallowfullscreen': true,
						'allowfullscreen': true
					}).appendTo('#VimeoPlayer-' + scope.config.contentId);
					var iframe = $('#player-' + scope.config.contentId)[0];
					var player = require('frog')(iframe);
					// When the player is ready, add listeners for pause, finish, and playProgress
					player.addEvent('ready', function () {
						player.addEvent('pause', onPause);
						player.addEvent('finish', onFinish);
						player.addEvent('playProgress', onPlayProgress);
					});
					function onPause() {
						scope.$emit('spend.stop');
						$hasInitStart = false;
					}

					function onFinish() {
						scope.$emit('spend.stop');
						$hasInitStart = false;
					}

					function onPlayProgress(data) {
						if (data.percent >= scope.config.cuepoint_view && hasTrack === false) {
							trackIsViewed();
							hasTrack = true;
						}
						if ($hasInitStart === false) {
							scope.$emit('spend.resume');
							$hasInitStart = true;
						}
					}

					/**
					 * Track that video already is watched
					 * @returns {directivePlayer_L2.Directive.link.scope.vimeoPlayer}
					 */
					function trackIsViewed() {
						$http.post('/lms/index.php?r=restdata', {
							endpoint: 'asset_view',
							params: {
								id_asset: scope.config.contentId
							}
						}).then(function ($result) {
							if ($result.data) {
								/*
								 * On track add +1 view instant
								 */
								if (scope.alreadyClicked || scope.details.is_viewed_already) {
									return;
								}
								if (scope.details.owner_id != scope.details.logged_user) {
									scope.details.total_views++;
									return scope.alreadyClicked = true;
								}
							}
						});
						return this;
					}
				}
				return this.init();
			};

			/**
			 * Wistia player definitions
			 * @returns {directivePlayer_L2.Directive.link.scope.init}
			 */
			scope.wistiaPlayer = function () {
				require('wistia');
				var $hasInitStart = false;

				this.wistiaEmbed = {};
				this.init = function () {
 
					
					this.wistiaEmbed = Wistia.embed(scope.config.hash, {
						autoPlay: true,
						controlsVisibleOnLoad: false
					});


					this.wistiaEmbed.hasData(function () {
						var api = Wistia.api(scope.config.hash);
						var hasTrack = false;

						/**
						 * track video has been watched
						 * @returns {directivePlayer_L2.Directive.link.scope.wistiaPlayer.directivePlayer_L310}
						 */
						function trackIsViewed() {
							$http.post('/lms/index.php?r=restdata', {
								endpoint: 'asset_view',
								params: {
									id_asset: scope.config.contentId
								}
							}).then(function ($result) {
								/*
								 * On track add +1 view instant
								 */
								if (scope.alreadyClicked || scope.details.is_viewed_already) {
									return;
								}
								if (scope.details.owner_id != scope.details.logged_user) {
									scope.details.total_views++;
									return scope.alreadyClicked = true;
								}
							});
							return this;
						}

						/**
						 * Event that has been fired on changed second
						 */
						api.bind("secondchange", function (s) {
							if (api.percentWatched() >= scope.config.cuepoint_view && hasTrack === false) {
								trackIsViewed();
								hasTrack = true;
							}
						});

						api.bind("end", function () {
							scope.$emit('spend.stop');
							$hasInitStart = false;
						});


						api.bind("pause", function () {
							scope.$emit('spend.stop');
							$hasInitStart = false;
						});

						api.bind("play", function () {
							if ($hasInitStart === false) {
								scope.$emit('spend.resume');
								$hasInitStart = true;
							}
						});
					});


				};

				return this.init();
			}


		}

		// removed <div class="owl-title">{{config.title}}</div> after owl-wrap
		function renderImage() {
			return	'<div id="owl-wrap">\n\
						<div id="owl-carousel" class="owl-carousel">\n\
							<div class="item" ng-bind-html="config.image"></div>\n\
						</div>\n\
						<div class="custom-owl-nav" tabindex="1">\n\
							<ul id="info" class="info"></ul>\n\
							<div class="expand fa fa-expand"></div>\n\
							<div class="compress fa fa-compress"></div>\n\
						</div>\n\
					</div>';
		}

		function renderVideo() {
			return '<div id="app7020-flowplayer" class="f-player-container" style="overflow-y: hidden;" data-has-cue="{{config.has_cue}}" >\n\
							<div id="{{config.containerId}}" data-content-id="{{config.contentId}}">\n\
								<div class="docebo-player-tooltips-container">\n\
								<div class="docebo-player-prewrapper-tooltips">\n\
									<div ng-repeat="cue in config.init.cuepoints | filter: ttCuesFilter" ng-attr-id="{{\'docebo-flowplayer-tooltip-\' + cue.hash}}" ng-class="cue.style" class="app7020tooltip media">\n\
									<div ng-bind-html="cue.avatar" class="avatar-wrapper"></div>\n\
										<div class="media-body"><div ng-bind-html="cue.text"></div><div class="clear clearfix"></div></div>\n\
									</div>\n\
								</div>\n\
							</div>\n\
						</div>\n\
					</div>';
		}

		/**
		 *
		 * @returns {String}
		 * removed <div class="owl-title">{{config.title}}</div> after owl-wrap
		 */
		function renderConvertable() {
			return	'<div id="owl-wrap">\n\
						<div id="owl-carousel" class="owl-carousel" data-page="{{config.page}}" data-objectid="{{config.id}}">\n\
							<div class="item" ng-repeat="image in config.items"><img ng-src="{{config.url + image.imageName}}"></div>\n\
						</div>\n\
						<div class="custom-owl-nav" tabindex="1">\n\
							<a href="javascript:void(0)" class="owl-prev-custom fa fa-angle-left"></a>\n\
							<ul id="info" class="info">\n\
								<li class="currp">{{config.pagecurr}}</li>\n\
								<li class="allp">/ <strong>{{config.items.length}}</strong></li>\n\
							</ul>\n\
							<div class="expand fa fa-expand"></div>\n\
							<div class="compress fa fa-compress"></div>\n\
							<a href="javascript:void(0)" class="owl-next-custom fa fa-angle-right"></a>\n\
						</div>\n\
					</div>';
		}

		function renderLink() {
			return	'<div class="share-link-wrapper">\n\
						<div class="wrapper">\n\
							<a href="{{config.filename}}" target="_blank" class="content-wrapper">\n\
								<div class="text-wrapper clearfix">\n\
									<div class="thumb" ng-bind-html="config.image"></div>\n\
									<div class="textcountainer">\n\
										<p class="sharelinkstittle">{{config.title}}</p>\n\
										<p class="sharelinksdescription" >{{config.description}}</p>\n\
										<p class="sharelinksfilename">{{config.filename}}</p>\n\
									</div>\n\
								</div>\n\
							</a>\n\
						</div>\n\
					</div>';
		}
		/**
		 *
		 * @returns {String}
		 */
		function renderLinkGoogle(url) {
			return	'<div class="share-link-wrapper">\n\
						<div class="wrapper">\n\
							<a href="'+url+'" target="_blank" class="content-wrapper">\n\
								<div class="text-wrapper clearfix">\n\
									<div class="thumb" ng-bind-html="config.image"></div>\n\
									<div class="textcountainer">\n\
										<p class="sharelinkstittle">{{config.title}}</p>\n\
										<p class="sharelinksdescription" >{{config.description}}</p>\n\
										<p class="sharelinksfilename">{{config.filename}}</p>\n\
									</div>\n\
								</div>\n\
							</a>\n\
						</div>\n\
					</div>';
		}
		/**
		 *
		 * @returns {String}
		 */
		function renderDownload() {
			return	'<div data-custom="contentFileItem">\n\
						<div class="contentFileItemRow">\n\
							<div class="contentFileItemVertical">\n\
								<div class="iconType">\n\
									<i class="{{config.icon}}"></i>\n\
								</div>\n\
								<div class="content">\n\
									<p class="filename">{{config.filename}}</p>\n\
								</div>\n\
								<div class="iconDownload">\n\
									<i class="fa fa-download"></i>\n\
								</div>\n\
								<div class="downloadText">\n\
									<span>\n\
										<a ng-click="setViewOnClick(config)" href="{{config.url}}" >' + Yii.t("app7020", "Download") + '</a>\n\
									</span>\n\
								</div>\n\
							</div>\n\
						</div>\n\
					</div>';
		}

		/**
		 *
		 * @returns {String}
		 */
		function renderGoogleDrive(config) {
			if (config.status == 200) {
				return '<iframe height="560" width="100%" frameborder="0" src="' + config.title + '"></iframe>';
			}
			else {
				return '<div class="error-404-container" style="background:#fff; height:560px; width:100%; margin-bottom:15px; border: 1px solid #eee; -webkit-box-shadow: 0px 1px 1px -1px rgba(0, 0, 0, 0.2);    -moz-box-shadow: 0px 1px 1px -1px rgba(0, 0, 0, 0.2);    box-shadow: 0px 1px 1px -1px rgba(0, 0, 0, 0.2)"  >\n\
						<div class="404-error" style="padding: 50px;font-size: 24px;"></div>\n\
 						</div><br />';
			}
		}
		/**
		 *
		 * @returns {String}
		 */
		function renderYoutube() {
			return '<div class="iframe-wrapper"><div id="YTPlayer"></div></div>';
		}

		/**
		 *
		 * @returns {String}
		 */
		function renderVimeo() {
			return '<div class="iframe-wrapper" >\n\
						<div id="VimeoPlayer-{{config.contentId}}">\n\
						</div>\n\
					</div>';
		}

		/**
		 *
		 * @returns {String}
		 */
		function renderWistia() {
			return '<div class="iframe-wrapper">\n\
						<div id="wistia_{{config.hash}}" class="wistia_embed" style="width:100%;height:100%;position:absolute;">&nbsp;\n\
						</div>\n\
					</div> ';
		}
	}

	Directive.$inject = ['$timeout', '$http', '$compile', '$window', '$interval', 'poller'];
	return Directive;
});