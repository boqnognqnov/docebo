'use strict';
define(function () {
	function Directive($timeout) {
		var link = function (scope, element, attr, ctrl) { };
		var template = '<div class="asset-details" ng-show="dataIsLoaded" ng-cloak>\
							<div class="meta-details">\
								<div class="title-details">\
									<h1>{{details.title}}</h1>\n\
									<div ng-bind-html="details.avatar" style="float:right;">\n\
									</div>\n\
								</div>\n\
								<div class="asset-metas">\n\
									<span>' + Yii.t('app7020', 'by') + '</span>\n\
									<a href="{{details.owner_channel_url}}" >{{details.owner}}</a>\n\
									<span id="totalContentViews">{{details.total_views}}</span><span> ' + Yii.t('app7020', 'views') + '</span>\n\
									<span> | {{details.uploaded_date}}  ago </span>\n\
								</div>\n\
							</div>\n\
							<!-- applicable on view mode -->\n\
							<div class="content-toggle" data-mode="view" ng-show="collapsed">\n\
								<div class="asset-description container-line">\n\
									<h3>' + Yii.t('app7020', 'Description') + '</h3>\n\
									<div ng-bind-html="details.description"></div>\n\
								</div>\n\
								<div class="asset-tags container-line">\n\
									<h3>' + Yii.t('app7020', 'Tags') + '</h3>\n\
									<ul>\n\
										<li ng-repeat="tag in details.tags">\n\
											{{tag}}\n\
										</li>\n\
									</ul>\n\
								</div>\n\
								<div class="asset-channels container-line" ng-if="details.channels.length">\n\
									<h3>' + Yii.t('app7020', 'Channels') + '</h3>\n\
									<ul>\n\
										<li ng-repeat="channel in details.channels">\n\
											<a href="{{channel.channel_url}}">{{channel.channel_name}} </a>\n\
										</li>\n\
									</ul>\n\
								</div>\n\
							</div>\n\
							<div class="angle-toggle text-center" ng-click="collapsed=!collapsed">\n\
								<i ng-class="(collapsed) ? \'fa fa-angle-up\' : \'fa fa-angle-down\'"> </i>\n\
							</div>\n\
							<!-- Aassets actions -->\n\
							<div class="asset-details-footer" > \n\
									<!-- Asset rating -->\n\
									<div class="rating-directive">\n\
										<span class="blue bold">' + Yii.t('app7020', 'Rate this asset') + '</span>\n\
										<asset-rating></asset-rating>\n\
										<div class="rated-check" data-rated="1">\n\
											<!-- if asset not rated yet -->\n\
											<span class="gray not-rated" ng-if="ratingValue1<=0">' + Yii.t('app7020', 'Not yet rated') + '</span>\n\
											<!-- if asset rated  -->\n\
											<span ng-if="ratingValue1>0" class="rated-value">{{ratingValue1}}</span>\n\
											<span ng-if="ratingValue1>0" class="thankyoutext"><p ng-class="isvisible ? \'visiblefadein\' : \'hiddenfadeout\'" >Thank you!</p></span>\n\
										</div>\n\
									</div>\n\
								<asset-actions > </asset-actions>\n\
							</div>\n\
						</div>';
		return {
			restrict: '',
			link: link,
			template: template
		};
	}
	Directive.$inject = ['$timeout'];
	return Directive;
});