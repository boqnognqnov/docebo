'use strict';
define(function () {
	function Directive($timeout) {
		var link = function (scope, element, attr, ctrl) { };
		var template = '<div class="asset-details" ng-cloak>\
							<div class="meta-details">\
								<div class="title-details">\
									<h1>{{details.title}}</h1>\n\
									<div class="author-avatar">\n\
										<span class="avatar-thumb" ng-bind-html="details.avatar"></span>\n\
									</div>\n\
								</div>\n\
								<div class="asset-metas">\n\
									<span>' + Yii.t('app7020', 'by') + '</span>\n\
									<a href="{{details.owner_channel_url}}" >{{details.owner}}</a>\n\
									<span>{{details.total_views}} ' + Yii.t('app7020', 'views') + '</span>\n\
									<span>{{details.uploaded_date}} </span>\n\
								</div>\n\
							</div>\n\
							<!-- Aassets actions -->\n\
							<div class="asset-details-footer" > \n\
									<!-- Asset rating -->\n\
									<div class="rating-directive">\n\
										<span class="blue bold">' + Yii.t('app7020', 'Rate this asset') + '</span>\n\
										<asset-rating></asset-rating>\n\
										<div class="rated-check" data-rated="1">\n\
											<!-- if asset not rated yet -->\n\
											<span class="gray not-rated" ng-if="ratingValue1<=0">' + Yii.t('app7020', 'Not yet rated') + '</span>\n\
											<!-- if asset rated  -->\n\
											<span ng-if="ratingValue1>0" class="rated-value">{{ratingValue1}}</span>\n\
										</div>\n\
									</div>\n\
								<asset-actions > </asset-actions>\n\
							</div>\n\
						</div>';
		return {
			restrict: '',
			link: link,
			template: template
		};
	}
	Directive.$inject = ['$timeout'];
	return Directive;
});