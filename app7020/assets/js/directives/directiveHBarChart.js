'use strict';
define(function () {
	function Directive() {
		return {
			restrict: 'E',
			link: link,
			template: '<div class="h-bar-chart-wrapper"></div>',
			scope: {
				chartid: '=chartid'
			}
		}
		function link(scope, iElem, iAttrs){
			scope.$parent.$watch(iAttrs.chartid, function(value){
				if(typeof value !== 'undefined'){
					var chartHtml = $(scope.chartid);
					iElem.html(chartHtml);
				}
			});
			
		}
	}
	Directive.$inject = []
	return Directive;
});