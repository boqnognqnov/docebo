/* global Yii */
define(['modules/tinymce', 'modules/notification'], function (tinymce, notification) {
	function Directive(QuestionService, $routeParams, $timeout) {
		return {
			restrict: 'AEC',
			scope: {},
			template: '<h1 style="margin-top:25px; font-size:20px;">' + Yii.t('app7020', 'Ask the Experts!') + '</h1> \n\
						<div class="new-answer-wrapper"> \n\
						<div class="questionBoxModelInAssets">\n\
							<div class="avatar-wrapper"></div>\n\
							<div class="content-wrapper">\n\
								<textarea class="add-new-question"></textarea>\n\
							</div>\n\
						</div>\n\
					</div>',
			link: link
		};

		function link(scope, element, attr) {
			scope.avatar = '';
			$timeout(function () {
				scope.$watch(function () {
					return scope.$parent.$parent.head;
				}, function (value) {
//					element.find('.avatar-wrapper').empty().append(value.logged_user.avatar);
 						if(element.find('.avatar-wrapper').length && scope.$parent.$parent.questionsLoaded) {
							element.find('.avatar-wrapper').html(value.logged_user.avatar);
						}
					var mce = new tinymce;
					mce.build('textarea.add-new-question', {
						plugins: ["moxiemanager image link autoresize"],
						setup: function (ed) {
							ed.addButton('custom-save', {
								text: Yii.t('app7020', 'Ask'),
								classes: 'custom-save-wrapper',
								onclick: function () {
 									var thisButton = this;
									thisButton.disabled(true);
									QuestionService.addQuestion({
										title: ed.getContent(),
										id_content: $routeParams.id,
										successCallback: function ($data) {
											if ($data.data.success !== true) {
												$('#questionsApp .alertWrapper').doceboNotice($data.data.errors.title, 'error');
												thisButton.disabled(false);
											} else {
												ed.setContent('');
												$('#questionsApp .alertWrapper').doceboNotice(Yii.t('app7020', 'Your question was added successfully!'), 'success');
												thisButton.disabled(false);
											}
										}
									});
								}
							});
						}

					});

				});
			});
		}
	}
	Directive.$inject = ['QuestionService', '$routeParams', '$timeout'];
	return Directive;
});