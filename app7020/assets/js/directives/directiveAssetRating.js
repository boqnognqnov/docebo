'use strict';
define(function () {
	function DirectiveAssetRating($timeout, AJAX, $routeParams) {

		return {
			restrict: 'EA',
			template:
					'<ul ng-class="details.canrate ? \'rating-directive \' : \'rating-directive readonly\'"><!-- Add class readonly for deactivating -->' +
					'  <li ng-repeat="star in stars" class="star" ng-class="{filled: star.filled, half: star.half, empty: star.empty, hover: hoverValue > $index}" ng-disabled="!details.canrate" ng-click="toggle($index)" ng-mouseenter="isolatedMouseHover($index + 1)" ng-mouseleave="isolatedMouseLeave($index + 1)" id="step-{{$index+1}}">' +
					'    <input type="radio" value="{{$index+1}}" id="star-{{$index+1}}" name="rating" />' +
					'    <label for="star-{{$index+1}}" class="fa star-label"></label>' +
					'  </li>' +
					'</ul>',
			link: function (scope, elem, attrs) {
				function updateStars() {
					scope.max = 5;
					scope.stars = [];
					for (var i = 0; i < scope.max; i++) {

						if (scope.ratingValue % 1 !== 0) {
							var valFloating = scope.ratingValue;
							var decimalsValue = (valFloating % 1).toFixed(2);
						}

						scope.stars.push({
							filled: i < scope.ratingValue,
							half: i == Math.floor(scope.ratingValue) && scope.ratingValue % 1 !== 0 && decimalsValue <= '0.75' && decimalsValue >= '0.25',
							empty: i == Math.floor(scope.ratingValue) && scope.ratingValue % 1 !== 0 && decimalsValue < '0.25'
						});
					}
				}
				;
				/* Rate the asset */
				scope.toggle = function (index) {
					if (app7020Options.user.id === scope.details.owner_id || !scope.details.canrate) {
						return ;
					}
					AJAX.post('/lms/index.php?r=restdata', {
						endpoint: 'rate_asset',
						params: {
							id_asset: $routeParams.id,
							rating: index,
							userId: app7020Options.user.id
						}
					}, function (response) {
						scope.isvisible = true;
						$timeout(function () {
							scope.isvisible = false;
						}, 2000);
						scope.details.canrate = false;

						/* update ratingvalue */
						scope.ratingValue1 = response.data;
					});
					if (!angular.element('.rating-directive').hasClass('readonly')) {
						scope.ratingValue = index + 1;
						updateStars();
					}

				};

				/* Fill the stars on hover */
				scope.isolatedMouseHover = function (param) {
					if (!angular.element('.rating-directive').hasClass('readonly')) {
						angular.element('.star.filled').addClass('disabled');
						scope.hoverValue = param;
					}
				};
				scope.isolatedMouseLeave = function (param) {
					scope.hoverValue = 0;
					angular.element('.star.filled').removeClass('disabled');
				};

				scope.$watch('ratingValue1', function (value1) {
					if (value1 !== 'undefined') {
						scope.ratingValue = scope.ratingValue1;
						updateStars();
					}
				});

			}
		};
	}
	DirectiveAssetRating.$inject = ['$timeout', 'AJAX', '$routeParams'];
	return DirectiveAssetRating;
});