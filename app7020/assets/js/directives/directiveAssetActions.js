'use strict';
define(function () {
	function Directive($timeout) {
		var link = function (scope, element, attr, ctrl) {
			scope.getMenuClass = function (param) {
				if (typeof (menus[param]) != 'undefined') {
					return menus[param].class;
				} else {
					return false;
				}
			}

			scope.getKey = function (param) {
				if (param == 1) {
					return menus['invite'].key;
				} else {
					return 'undefined';
				}
			}

		};
		var menus = new Array();
		menus['download'] = {
			'class': 'fa-download',
			'key': 'download',
			'href': 'http://dir.bg'
		};
		menus['invite'] = {
			'class': 'fa-user-plus',
			'key': 'invite'
		};
		menus['report'] = {
			'class': 'fa-exclamation-triangle',
			'key': 'report'
		};
		menus['watch_later'] = {
			'class': 'fa-history',
			'key': 'watch_later'
		};
		menus['sharing'] = {
			'class': 'fa-share-alt',
			'key': 'sharing'
		};
		var template = '<!-- Asset action buttons -->\n\
						<ul ng-if="details.published || inviteButton" class="asset-actions">\n\
							<li ng-repeat="action in actions"> \n\
								<a ng-if="action.action_uid == getKey(1)" data-placement="top" rel="tooltip" data-toggle="tooltip" data-original-title="{{action.tooltip_text}}" ng-click="openInviteToWatchModal(action.action_params.asset_id)" href="">\n\
									<i class="fa  {{getMenuClass(action.action_uid)}} "></i>\n\
								</a>\n\
								<a ng-if="action.action_uid != getKey(1) && details.asset_type != 15" data-placement="top" rel="tooltip" data-toggle="tooltip" data-original-title="{{action.tooltip_text}}"  href="{{action.action_params.url}}">\n\
									<i class="fa  {{getMenuClass(action.action_uid)}} "></i>\n\
								</a>\n\
							</li>\n\
 						</ul>';
		return {
			restrict: '',
			link: link,
			template: template
		};
	}
	Directive.$inject = ['$timeout'];
	return Directive;
});