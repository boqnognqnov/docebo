define(function () {

	function directive($timeout, AJAX, $routeParams, $location) {

		return {
			restrict: 'E',
			link: link,
			template:
					'<div class="asset-status-bar" ng-if="statusBarMode && data">\n\
				<div class="docebo-row" ng-hide="statusBarMode === \'view\' && !data.published && !data.last_edited">\n\
					<div class="docebo-col-sm-12 box-sizing">\n\
						<div class="status-bar" ng-attr-data-mode="{{statusBarMode}}">\n\
							<div class="status-bar-wrapper" ng-show="statusBarMode === \'edit\' || statusBarMode === \'view\'">\n\
								<div ng-show="data.status_bar && statusBarMode === \'edit\'" class="publish-state-label gray">' + Yii.t('app7020', 'Unpublish') + '</div>\n\
								<div ng-show="data.status_bar && statusBarMode === \'edit\'" class="publish-button-bar">\n\
									<input type="checkbox" class="toggle-state" id="toggle-state-publish" ng-model="data.published" />\n\
									<div class="toggle"><label ng-click="changeState()"><i></i></label></div>\n\
								</div>\n\
								<div ng-show="data.status_bar && statusBarMode === \'edit\'" class="publish-state-label green">' + Yii.t('app7020', 'Publish') + '</div>\n\
								<div ng-if="!data.last_edited && !data.published && statusBarMode === \'edit\'" class="status-ready-text green bold">' + Yii.t('app7020', 'This asset is ready to be published') + '</div>\n\
								<div ng-if="data.last_edited || data.published || statusBarMode === \'view\'" ng-class="{\'status-ready-text\': data.status_bar && statusBarMode === \'edit\'}">\n\
									<div class="publish-details-container" ng-show="data.published">\n\
										<div class="publish-meta-label green bold">' + Yii.t('app7020', 'Published by') + ':</div>\n\
										<div class="publish-meta-value"> \n\
											<span class="meta-expert">' + Yii.t('app7020', 'Expert') + '</span>\n\
											<span class="meta-expert-link bold blue" >{{data.published_by}}</span>\n\
											<span class="meta-date">' + Yii.t('app7020', 'on') + '\n\
												<span class="black light">{{data.published_datetime}}</span>\n\
											</span>\n\
										</div>\n\
									</div>\n\
									<div class="edit-details-container" ng-show="data.last_edited">\n\
										<div class="blue bold">' + Yii.t('app7020', 'Last edit by') + ':</div>\n\
										<div class="edit-details-meta"> \n\
											<span class="meta-expert">' + Yii.t('app7020', 'Expert') + '</span>\n\
											<span class="meta-expert-link bold blue">{{data.last_edit_by}}</span>\n\
											<span class="meta-date">' + Yii.t('app7020', 'on') + '\n\
												<span class="black light">{{data.last_edit_datetime}}</span>\n\
											</span>\n\
										</div>\n\
									</div>\n\
								</div>\n\
							</div>\n\
							<div ng-show="statusBarMode === \'highlight-published\'" class="published-bar-wrapper clearfix">\n\
								<i class="fa fa-check-circle"></i>\n\
								<p class="text">' + Yii.t('app7020', 'Asset has been <strong>PUBLISHED in the Knowledge Library.</strong> <span ng-if="invitedText !== false">[and 123 people <strong>have been invited to watch it!</strong>]</span>') + '</p>\n\
								<i class="fa fa-times" ng-click="forceHideHighlight()"></i>\n\
							</div>\n\
							<div ng-show="statusBarMode === \'highlight-unpublished\'" class="unpublished-bar-wrapper clearfix">\n\
								<i class="fa fa-exclamation-circle"></i>\n\
								<p class="text">' + Yii.t('app7020', 'Asset has been <strong>UNPUBLISHED</strong> from the Knowledge Library. You can find your asset in <strong><u>My Expert Tasks > Assets to approve/review</u></strong>') + '</p>\n\
								<i class="fa fa-times" ng-click="forceHideHighlight()"></i>\n\
							</div>\n\
						</div>\n\
				</div>\n\
			</div>\n\
		</div>'
		};

		function link(scope, iElem, iAttrs) {
			scope.data = false;
			scope.highlightTimer;
			scope.toggleStateDisable = false;
			/* This need implement after magane callback of invite to watch 
			 * to redirect to view mode */
			scope.invitedText = false;
			scope.firstTimePublish = false;
			scope.inviteButton = false;
			scope.$on('loaded.details', function (e, data) {
				scope.data = data;
			});

			scope.$on('updated.details', function (e, data) {
				scope.data = data;
				scope.changedStateFn();
			});
			scope.forceHideHighlight = function () {
				$timeout.cancel(scope.highlightTimer);
			};

			scope.changeState = function () {
				if (scope.toggleStateDisable === true)
					return;

				scope.toggleStateDisable = true;
				/* Send request for publish/unpublish asset */
				AJAX.post('/lms/index.php?r=restdata', {
					endpoint: !scope.data.published ? 'asset_published' : 'asset_unpublished',
					params: {
						id_asset: $routeParams.id
					}
				}, function (response) {
					if (response.data.status === true) {
						if (scope.data.published === false && scope.data.last_edited === false) {
							scope.$broadcast('open.inviteToWatch', {id_asset: $routeParams.id});
							scope.firstTimePublish = true;
							
						}
						if (response.data.published === true) {
							scope.inviteButton = true;
						} else {
							scope.inviteButton = false;
						}
						scope.$broadcast('update.details', response.data);
					}
				}, function (response) {
					scope.toggleStateDisable = false;
				});

			};

			scope.changedStateFn = function () {
				if (scope.data.published)
					scope.statusBarMode = 'highlight-published';
				if (!scope.data.published)
					scope.statusBarMode = 'highlight-unpublished';

				/* Start timer */
				scope.highlightTimer = $timeout(2000);

				/* When timer finish */
				scope.highlightTimer.finally(function () {
					if(scope.firstTimePublish === false){
						scope.statusBarMode = 'edit';
					}else{
						$location.path('/assets/view/'+$routeParams.id);
						scope.statusBarMode = 'view';
					}
					scope.firstTimePublish = false;
					scope.toggleStateDisable = false;
				});
			};
		}
	}

	directive.$inject = ['$timeout', 'AJAX', '$routeParams', '$location'];
	return directive;

});