/* global Yii, angular */
define(['visjsTimeline', 'modules/appendCSS'], function (vis, css) {
	new css('/themes/spt/css/vis.min.css');
	function Directive($timeout, $filter, $routeParams, $window) {
		return {
			restrict: 'E',
			scope: {},
			link: link,
			template: '<div id="assetTooltipTimeline">\n\
							<h3>' + Yii.t('app7020', 'Video Timeline') + '</h3>\n\
							<div class="timeline-wrapper"></div>\n\
						</div>'
		};

		function link(scope, iElem, iAttrs) {


			/* Convert Date Object to miliseconds */
			scope.CTMSeconds = function (date) {
				var date = new Date(date);
				return Math.round(date.getTime());
			};

			scope.CMSecondsToMMSS = function (date) {
				var date = new Date(date);
				var s = Math.round(date.getTime() / 1000);
				return (s - (s %= 60)) / 60 + (9 < s ? ':' : ':0') + s;
			};

			/* Compare all items ranges as item range and return (bool)(true|false) */
			scope.calcMaxRows = function (item, items, maxRows) {
				var rowsMatch = [], result = [];
				items.get($window.timeline.getVisibleItems()).forEach(function (arr_item) {
					if (scope.CTMSeconds(item.start) <= scope.CTMSeconds(arr_item.end) &&
							scope.CTMSeconds(item.end) >= scope.CTMSeconds(arr_item.start) &&
							arr_item.id != item.id)
						rowsMatch.push(iElem.find('.vis-item[data-id="' + arr_item.id + '"]').position().top);
				});
				/* Remove dublicated values from array */
				$.each(rowsMatch, function (i, e) {
					if ($.inArray(e, result) == -1)
						result.push(e);
				});
				return result.length >= maxRows;
			};

			scope.calcSeekerTimes = function (start, end) {
				return {
					start: scope.CMSecondsToMMSS(start),
					end: scope.CMSecondsToMMSS(end),
					duration: scope.CMSecondsToMMSS(scope.CTMSeconds(end) - scope.CTMSeconds(start))
				};
			};

			scope.toggleAvailablePosiotion = function (start, end, items, length) {
				/* Checks for available position */
				scope.$parent.$broadcast('timeline.available.position',
						(!scope.calcMaxRows({id: 0, start: start, end: end}, items, 3) && length * 1000 >= end));
			};

			/* DataSet items */
			scope.tooltips = [];

			/* Dinamization of timeline with REST */
			scope.$watch(function () {
				return scope.$parent.dataIsLoaded;
			}, function (value) {
				if (value) {
					scope.$parent.tooltips.forEach(function (item) {
						scope.tooltips.push({id: item.id, class: item.tooltipStyle, content: item.tooltip_text, start: item.from * 1000, end: item.to * 1000});
					});

					var videoLength = scope.$parent.details.duration; // in seconds
					var items = new vis.DataSet(scope.tooltips);

					/* Configuration for the Timeline */
					var options = {
						showCurrentTime: false,
						showMajorLabels: false,
						showMinorLabels: true,
						minHeight: "129px",
						maxHeight: "129px",
						start: 0,
						end: 1000 * videoLength,
						min: 0,
						max: 1000 * videoLength,
						zoomMin: videoLength >= 120 ? 120000 : videoLength * 1000,
						zoomMax: videoLength >= 120 ? 120000 : videoLength * 1000,
						zoomable: false,
						type: 'range',
						stack: true,
						throttleRedraw: 0,
						dataAttributes: ['id', 'start', 'end'],
						timeAxis: {scale: 'second', step: 5},
						template: function (item) {
							return $filter('HtmlToPlainText')(item.content);
						},
						editable: {
							add: false,
							remove: false,
							updateTime: true,
							updateGroup: false
						},
						format: {
							minorLabels: {
								millisecond: 'SSS',
								second: 'm:ss',
								minute: 'HH:mm',
								hour: 'HH:mm',
								weekday: 'ddd D',
								day: 'D',
								month: 'MMM',
								year: 'YYYY'
							}
						},
						margin: {
							item: {
								vertical: 9
							},
							axis: 4
						},
						snap: function (date, scale, step) {
							/* Set snaping items in grid per 1 second */
							var clone = new Date(date.valueOf());
							return vis.timeline.TimeStep.snap(clone, scale, step);
						},
						onMoving: function (item, callback) {
							/* Limit minimum time on tooltip and prevent dragging outside video length time range */
							if (scope.CTMSeconds(item.end) - scope.CTMSeconds(item.start) < 3000 ||
									scope.CTMSeconds(item.start) < 0 || scope.CTMSeconds(item.end) > videoLength * 1000)
								return callback(null);

							var output = scope.calcMaxRows(item, items, 3) ? null : item;
							/* Update seeker tooltip labels */
							if (output !== null) {
								var seekerTimes = scope.calcSeekerTimes(item.start, item.end);
								iElem.find('.vis-item[data-id="' + item.id + '"] .seeker-item-tooltip.start').html(seekerTimes.start + ' <span>(' + seekerTimes.duration + 's)</span>');
								iElem.find('.vis-item[data-id="' + item.id + '"] .seeker-item-tooltip.end').html(seekerTimes.end + ' <span>(' + seekerTimes.duration + 's)</span>');
							}

							/* prevent dragging if items in same time is more than 3 */
							return callback(output);
						},
						onMove: function (item, callback) {
							/* Hide item seeker tooltips */
							iElem.find('.seeker-item-tooltip').removeClass('show');

							/* Update tooltip editor directive */
							scope.$parent.$broadcast('update.range', {
								durationFrom: scope.CTMSeconds(item.start) / 1000,
								durationTo: scope.CTMSeconds(item.end) / 1000
							});
							return callback(item);
						}
					};

					/* Build a Timeline */
                    $window.timeline = new vis.Timeline(iElem.find('.timeline-wrapper')[0]);

					/* Set config options */
					$window.timeline.setOptions(options);

					/* Set data in lines */
					$window.timeline.setItems(items);

					/* Add custom current time */
					$window.timeline.addCustomTime(1, 'current-time-player');
					$window.timeline.setCustomTimeTitle('', 'current-time-player');
					$($window.timeline.customTimes[0].bar).children().html('<span>' + scope.CMSecondsToMMSS(0) + '</span>');

					/* Prevent selectable */
					scope.$on('update.selectable', function (e, v) {
						$window.timeline.setOptions({selectable: !v});
					});

					$window.timeline.on('select', function (props) {
						/* On select any item, create tooltips (start|end) */
                        if($window.timeline.getSelection().length){
                            var itemDom = iElem.find('.vis-item[data-id="' + $window.timeline.getSelection()[0] + '"]');
                            var seekerTimes = scope.calcSeekerTimes(items.get($window.timeline.getSelection())[0].start, items.get($window.timeline.getSelection())[0].end);
                            if (!itemDom.find('.seeker-item-tooltip').length) {
                                itemDom.find('.vis-drag-left').after('<div class="seeker-item-tooltip start">' + seekerTimes.start + ' <span>(' + seekerTimes.duration + 's)</span></div>');
                                itemDom.find('.vis-drag-right').after('<div class="seeker-item-tooltip end">' + seekerTimes.end + ' <span>(' + seekerTimes.duration + 's)</span></div>');
                                itemDom.on('mousedown', '.vis-drag-left, .vis-drag-right', function (e) {
                                    $('#assetTooltipTimeline .seeker-item-tooltip').removeClass('show');
                                    $(e.delegateTarget).find('.seeker-item-tooltip.' + ($(this).hasClass('vis-drag-left') ? 'start' : 'end')).addClass('show');
                                });
                                itemDom.on('mouseup', '.vis-drag-left, .vis-drag-right', function () {
                                    $('#assetTooltipTimeline .seeker-item-tooltip').removeClass('show');
                                });
                            } else {
                                itemDom.find('.vis-item[data-id="' + items.get($window.timeline.getSelection())[0].id + '"] .seeker-item-tooltip.start').html(seekerTimes.start + ' <span>(' + seekerTimes.duration + 's)</span>');
                                itemDom.find('.vis-item[data-id="' + items.get($window.timeline.getSelection())[0].id + '"] .seeker-item-tooltip.end').html(seekerTimes.end + ' <span>(' + seekerTimes.duration + 's)</span>');
                            }

                            /* Pause flowplayer */
                            scope.$parent.$broadcast('flowplayer.pause');

                            /* Move custom time bar to start time of tooltip */
                            scope.$parent.$broadcast('flowplayer.seekTo', Math.round(scope.CTMSeconds(items.get($window.timeline.getSelection())[0].start) / 1000));
                        }

						/* Send item to edit tooltip directive */
						scope.$parent.$broadcast('current.tooltip', {tooltip: items.get($window.timeline.getSelection())[0], items: items.get()});
					});

					/* Check range for new tooltip adding */
					scope.$on('request.timeline.add.item', function (e, v) {
						var startTooltip, endTooltip, content = 'Tooltip text';

						if (scope.$root.createPRtoTT.length > 0) {
							startTooltip = scope.$root.createPRtoTT[0].time * 1000;
							endTooltip = startTooltip + 3000;
							content = scope.$parent.peer_reviews.items[scope.$root.createPRtoTT[0].id].title;
							scope.$root.createPRtoTT = [];
						} else {
							startTooltip = scope.CTMSeconds($window.timeline.getCustomTime('current-time-player'));
							endTooltip = startTooltip + 3000;
						}
						if (!scope.calcMaxRows({id: 0, start: startTooltip, end: endTooltip}, items, 3) && videoLength * 1000 >= endTooltip) {
							items.add([{
									id: '0',
									content: content,
									class: "app7020-tooltip-light",
									start: startTooltip,
									end: endTooltip
								}]);
							$window.timeline.setSelection(0);
							$window.timeline.emit('select');
						}
					});

					/* (Update/Revert) item after save in database */
					scope.$on('update.timeline.item', function (e, v) {
						if (v.id == 0) {
							items.remove(v.id);
							items.add({
								id: v._id,
								content: v.text,
								class: v.tooltipStyle ? "app7020-tooltip-light" : "app7020-tooltip-dark",
								start: v.durationFrom * 1000,
								end: v.durationTo * 1000
							});
						} else {
							items.update({
								id: v.id,
								content: v.text,
								class: v.tooltipStyle ? "app7020-tooltip-light" : "app7020-tooltip-dark",
								start: v.durationFrom * 1000,
								end: v.durationTo * 1000
							});
						}
						$window.timeline.setSelection();
						scope.toggleAvailablePosiotion(scope.CTMSeconds($window.timeline.getCustomTime('current-time-player')),
						scope.CTMSeconds($window.timeline.getCustomTime('current-time-player')) + 3000, items, videoLength);
					});

					/* Delete item from timeline after deleted from database */
					scope.$on('delete.timeline.item', function (e, v) {
						items.remove(v.id);
						$window.timeline.setSelection();
						scope.toggleAvailablePosiotion(scope.CTMSeconds($window.timeline.getCustomTime('current-time-player')),
						scope.CTMSeconds($window.timeline.getCustomTime('current-time-player')) + 3000, items, videoLength);
					});

					/* Custom time bar change label */
					$window.timeline.on('timechange', function (props) {
						iElem.find('.current-time-player').children().find('span').html(scope.CMSecondsToMMSS(props.time));
						scope.$parent.$broadcast('flowplayer.pause');
					});

					/* trigger seek when cursor drop finish */
					$window.timeline.on('timechanged', function (props) {
						var date = props.time;
						var time = Math.round(date.getTime() / 1000);
						if (time < 0 || isNaN(time)) {
							time = 0;
						}
						scope.$parent.$broadcast('flowplayer.seekTo', time);
					});

					/* Seek custom time bar from flowplayer */
					scope.$on('timeline.seekTo', function (e, v) {
						$window.timeline.setCustomTime(v * 1000, 'current-time-player');
						iElem.find('.current-time-player').children().find('span').html(scope.CMSecondsToMMSS(v * 1000));
						$window.timeline.moveTo(v * 1000);
						scope.toggleAvailablePosiotion(v * 1000, v * 1000 + 3000, items, videoLength);
					});
					
					/* Initial toggle position */
					$timeout(function(){
						scope.toggleAvailablePosiotion(0, 3000, items, videoLength);
					}, 1000);

					/* Url params add tooltip */
					scope.$watch(function () {
						return scope.$parent.flowplayer_is_loaded;
					}, function (v) {
						if (v && scope.$root.createPRtoTT.length > 0) {
							if (videoLength - 3 >= scope.$root.createPRtoTT[0].time) {
								scope.$broadcast('request.timeline.add.item');
							} else {
								alert(Yii.t('app7020', 'Invalid tooltip range! Something went wrong! Contact your Super Admin or Administrator!'));
							}
						}
					});



				}
			});
		}
	}

	Directive.$inject = ['$timeout', '$filter', '$routeParams', '$window'];
	return Directive;
});