define(function(){
	
	function QuestionItemsDirective($location, $window, $sce){
		
		return {
			restrict: 'EA',
			templateUrl: 'index.php?r=askTheExpert/view&view_name=templates.questionTemplateAddQuestion',
			scope: {
				items: '=',
				itemsFilter: '=',
			},
			link: function(scope, iElem, iAttrs){
				scope.questionAllow = scope.$parent.questionAllow;
				scope.location = function(path, e){
					e.stopPropagation();
					$window.location.href = app7020Options.AskTheExpertControllerAbs+'/#'+path;
				}
                scope.to_trusted = function(html_code){
                    return $sce.trustAsHtml(html_code);
                }
			}
		};
		
	}
	
	QuestionItemsDirective.$inject = ['$location', '$window', '$sce'];
	return QuestionItemsDirective;
	
});