'use strict';
define(function (require) {
	function Directive($timeout, $routeParams, AJAX, $window, $location) {
		return {
			restrict: '',
			templateUrl: 'index.php?r=site/view&view_name=assetPlay._peer',
			link: function (scope, iElem, attr, ctrl) {
				scope.$watch('peer_reviews.items', function (value1) {
					if (value1 !== null) {
						scope.peerData = {};
						angular.copy(scope.peer_reviews, scope.peerData);
						scope.peerData.items = scope.convertTo(scope.peerData.items, 'date', true);
					}
				});
				scope.$watch('newId', function (value1) {
					if (value1 !== null) {
						scope.peerData = {};
						angular.copy(scope.peer_reviews, scope.peerData);
						scope.peerData.items = scope.convertTo(scope.peerData.items, 'date', true);
						$('.peer-reviews-wrapper .content').mCustomScrollbar('scrollTo', 'bottom');
					}
				});
				scope.action = "pr_added";
				/* Show chat date (reorder array and group by date) */
				scope.convertTo = function (arr, key, dayWise) {
					if (typeof arr === 'undefined')
						return;
					var groups = {};
					var data;
					$.each(arr, function (key, value) {
						if (value) {
							data = new Date(value.date).toLocaleDateString();
							groups[data] = groups[data] || [];
							groups[data].push(value);
						}
					});
					return groups;
				};
				// Add new peer review
				scope.addNew = function () {
					if (!scope.title) {
						angular.element('#peerMessage').css('border', '1px solid red');
						return false;
					}
					scope.isDisabled = true;
					angular.element('#peerMessage').css('border', 'none');
					AJAX.post('/lms/index.php?r=restdata', {
						endpoint: scope.action,
						params: {
							id_asset: $routeParams.id,
							id: scope.reviewId,
							message: scope.title
						}
					}, function (response) {
						scope.peerId = response.data.reviewId;
						var type = 'mine';
						if (scope.action == "pr_added") {
							var actions = [];
							actions[0] = {action_type: "modal", action_uid: "edit", label: "Edit", text_color: "#333333"};
							actions[1] = {action_type: "modal", action_uid: "delete", label: "Delete", text_color: "#e84b3c"};
							actions[2] = {action_type: "make_tooltip", action_uid: "make_tooltip", label: "Make tooltip", text_color: "#333333"};
							var message = {
								idAsset: $routeParams.id,
								id: scope.peerId,
								title: scope.title,
								avatar: app7020Options.userAvatar,
								date: dateFormat(),
								//update_date: dateFormat(),
								hour: hourFormat(),
								type: type,
								pr_owner: "Me",
								idUser: parseInt(app7020Options.user.id),
								actions: actions,
								status : "0"
							}
							scope.peer_reviews.items[scope.peerId] = message;
							scope.peer_reviews.ids = scope.peer_reviews.ids || []
							scope.peer_reviews.ids.push(scope.peerId);
							$timeout(function () {
								$('.peer-reviews-wrapper .content').mCustomScrollbar('scrollTo', 'bottom');
							});
						} else {
							scope.peer_reviews.items[scope.reviewId].title = scope.title;
						}
						scope.$watch('peer_reviews.items', function (value1) {
							angular.copy(scope.peer_reviews, scope.peerData);
							scope.peerData.items = scope.convertTo(scope.peerData.items, 'date', true);
							$("#peerMessage").val("");
						});
						scope.action = "pr_added";
						scope.reviewId = "";
						scope.title = "";
						scope.isDisabled = false;
					});


				}
				// Edit peer review
				iElem.on('click', 'span[data-id="edit"]', function () {
					scope.action = "pr_updated";
					var $this = $(this);
					$this.closest(".peer-review-actions").hide();
					var itemId = $this.attr("data-index");
					scope.itemId = itemId;
					var reviewDate = $this.attr("data-date");
					scope.reviewDate = reviewDate;
					var reviewId = $this.attr("data-review");
					scope.reviewId = reviewId;
					var edited = scope.peer_reviews.items[reviewId];
					scope.title = edited.title;
					scope.$digest();
				});
				
					// Edit peer review
				iElem.on('click', 'span[data-id="make_tooltip"]', function () {
					scope.$root.createPRtoTT = [];
					scope.$root.createPRtoTT.push({
						id: parseInt($(this).data('review')),
						time: Math.round(require('flowplayerLib')($('#'+scope.player.containerId)).video.time)
					});
					window.location.href  =  '#/assets/tooltips/'+$routeParams.id;
				});
				
				// Delete peer review
				iElem.on('click', 'span[data-id="delete"]', function () {
					var $this = $(this);
					var review = $this.attr("data-review");
					var deleted = scope.peer_reviews.items[review];
					delete scope.peer_reviews.items[review];
					scope.$watch('peer_reviews.items', function (value1) {
						angular.copy(scope.peer_reviews, scope.peerData);
						scope.peerData.items = scope.convertTo(scope.peerData.items, 'date', true);
						$("#peerMessage").val("");
					});
					AJAX.post('/lms/index.php?r=restdata', {
						endpoint: 'pr_deleted',
						params: {
							id: review
						}
					}, function (response) {
					});
				});
				function dateFormat() {
					var localDate = new Date();

					var formatedDate;
					if (typeof localDate == "string") {
						var dateChunks = localDate.split('-');
						formatedDate = dateChunks[0] + "/" + dateChunks[1] + "/" + dateChunks[2];
					} else {
						formatedDate = localDate;
					}
					var newDate = new Date(formatedDate);
					var offset = (-1 * ((localDate.getTimezoneOffset()) / 60));

					var Y, M, D, H, i, s = null;
					newDate.setHours(newDate.getHours() + offset);
 					newDate.setMonth(newDate.getMonth( ) + 1);
					Y = newDate.getFullYear();
					M = ("0" + newDate.getMonth()).slice(-2);
 					D = ("0" + newDate.getDate()).slice(-2);
					H = ("0" + newDate.getHours()).slice(-2);
					i = ("0" + newDate.getMinutes()).slice(-2);
					s = ("0" + newDate.getSeconds()).slice(-2);
					return Y + '-' + M + '-' + D
				}
				function hourFormat() { // Separate date and hours, because of grouping by date
					var localDate = new Date();

					var formatedDate;
					if (typeof localDate == "string") {
						var dateChunks = localDate.split('-');
						formatedDate = dateChunks[0] + "/" + dateChunks[1] + "/" + dateChunks[2];
					} else {
						formatedDate = localDate;
					}
					var newDate = new Date(formatedDate);
					var offset = (-1 * ((localDate.getTimezoneOffset()) / 60));
					var H, i, s = null;
					newDate.setHours(newDate.getHours());
					H = ("0" + newDate.getHours()).slice(-2);
					i = ("0" + newDate.getMinutes()).slice(-2);
					s = ("0" + newDate.getSeconds()).slice(-2);
					return H + ':' + i
				}
				$timeout(function () {
					/* Tiny scroll */
					$(".peer-reviews-wrapper .content").mCustomScrollbar({
						theme: "light",
						callbacks: {
							onTotalScrollBack: function () { /*
							 AJAX.post('/lms/index.php?r=restdata', {
							 endpoint: "asset_pr",
							 params: {
							 id_asset: $routeParams.id,
							 from : scope.peer_reviews.items.length
							 }
							 }, function (response) {
							 //scope.peer_reviews.items.push(response.data.items);
							 var newItems = scope.peer_reviews.items;
							 angular.forEach(newItems, function(value, key){
							 var message = {
							 idAsset : $routeParams.id,
							 id : value.peerId,
							 title: value.title,
							 avatar: value.avatar,
							 date: value.date,
							 type: value.type,
							 pr_owner: "Me",
							 idUser: parseInt(app7020Options.user.id)
							 }
							 scope.peer_reviews.items.unshift(message);
							 scope.$watch('peer_reviews.items', function (value1){
							 angular.copy(scope.peer_reviews, scope.peerData);
							 scope.peerData.items = scope.convertTo(scope.peerData.items, 'date', true);
							 $("#peerMessage").val("");
							 });
							 });	
							 });	*/
							}
						}
					});
					$('.peer-reviews-wrapper .content').mCustomScrollbar('scrollTo', 'bottom');
				});

			}
		};
	}
	Directive.$inject = ['$timeout', '$routeParams', 'AJAX', '$window', '$location'];
	return Directive;
});