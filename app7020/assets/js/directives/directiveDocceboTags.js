'use strict';
require.config({
	shim: {
		'libs/tags/typeahead.jquery': {
			init: function () {
				return require.s.contexts._.registry['typeahead.js'].factory($);
			}
		},
		'libs/tags/typeahead.bundle': {
			init: function () {
				return require.s.contexts._.registry['bloodhound.js'].factory($);
			}
		}
	}
});
define(function (require) {
	new require('modules/appendCSS')(app7020Options.assetUrl + '/css/tags/app7020-tags.css');
	function Directive(AJAX, $timeout) {
		var link = function (scope, element, attr) {
			/* Setup params */
			scope.tagContainer = null;
			scope.prefilled = new Array();
			scope.elm = false;
			scope.conf = {
				id: 'tags',
				classes: 'tags'
			};
			$timeout(function () {
				require('libs/tags/handlebars');
				require('libs/tags/typeahead.jquery');
				require('libs/tags/typeahead.bundle');
				require('libs/tags/tagmanager');

				/* Bloodhound for this tags container */
				scope.bloodhound = function () {
					scope.tagContainer = new Bloodhound({
						datumTokenizer: Bloodhound.tokenizers.obj.whitespace('tagText'),
						queryTokenizer: Bloodhound.tokenizers.whitespace,
						limit: 10,
						prefetch: {
							url: $fcbkAutocompleteUrl
						}
					});
					scope.tagContainer.clearPrefetchCache();
					scope.tagContainer.initialize(true);
					return this;
				};
				
				var init = function () {
					if (scope.tagContainer === null) {
						scope.bloodhound();
					}

					if (scope.elm === false) {
						scope.elm = element.find('input[name="tags"]')
					}

					if (typeof (attr.id) !== 'undefined') {
						scope.conf.id = attr.id;
					}

					if (typeof (attr.name) !== 'undefined') {
						scope.conf.name = attr.name;
					}

					if (typeof (attr.class) !== 'undefined') {
						scope.conf.classes = attr.class;
					}

					if (typeof (attr.tags) !== 'undefined' && attr.tags.length > 0) {
						scope.prefilled = attr.tags.split(',');
					}

					var tagApi = scope.elm.tagsManager({
						prefilled: scope.prefilled,
						AjaxPush: app7020Options.typehead,
						tagsContainer: element.find('.tagsContainer'),
						backspace: []
					});

					var typeahead = scope.elm.typeahead({
						hint: true,
						highlight: true,
						minLength: 1
					}, {
						displayKey: 'tagText',
						name: 'tagsName',
						source: scope.tagContainer.ttAdapter()
					});

					typeahead.on('typeahead:selected', function (e, d) {

						tagApi.tagsManager("pushTag", d.tagText);
						scope.elm.typeahead('val', '');
						scope.elm.typeahead('close');
						element.find('.tt-menu').css({display: 'none'});
					});

					scope.elm.blur(function () {
						scope.elm.typeahead('val', '');
						element.find('.tt-menu').css({display: 'none'});
					});
					return this;
				};
				init();
			});
		}
		return {
			restrict: 'E',
//					require: 'ngModel',
			link: link,
			template: '<div class="tag-widget-container"> \n\
							<div class="tagsContainer"></div>\n\
							<input type="text" id="{{conf.id}}" class="{{conf.classes}}" name="tags" placeholder="' + Yii.t('app7020', 'Add tags here (separated by a comma)') + '" />\n\
					</div>'
		}
	}
	Directive.$inject = ['AJAX', '$timeout'];
	return Directive;
});