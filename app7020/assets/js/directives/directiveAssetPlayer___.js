'use strict';
define(function () {
	function Directive($timeout) {
		return {
			restrict: '',
			link: link,
			template: '<div class="asset-player-wrapper"></div>'
		}

		function link(scope, iElem, iAttrs) {
			scope.$watch('playerHtml', function (value) {
				if (typeof value !== 'undefined')
					scope.render();
			});

			scope.render = function () {
				var playerHtml = $(scope.playerHtml);
				iElem.html(playerHtml);
				$timeout(function () {
					playerHtml.app7020documentContentReader();
				});

			}
		}
	}
	Directive.$inject = ['$timeout']
	return Directive;
});