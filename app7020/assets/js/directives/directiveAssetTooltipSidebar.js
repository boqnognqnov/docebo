/* global Yii, angular */
define(function () {
	function Directive($timeout, $http, $filter, $routeParams, $compile, $window) {
		return {
			restrict: 'E',
			link: link,
			scope: {},
			template: '<div id="assetTooltipSidebar">\n\
							<div class="header">\n\
								<h3>' + Yii.t('app7020', 'Tooltips editor') + '</h3>\n\
								<i class="tooltip-control noselect" ng-show="tooltip.id != 0" ng-class="{\'tt_add\': !edit, \'tt_close\': edit}" ng-click="switchMode()" ng-disabled="!availablePosition && !edit">+</i>\n\
							</div>\n\
							<div class="content">\n\
								<div class="information" ng-show="!edit">\n\
									' + Yii.t('app7020', 'Click the plus round button to add a <br/> new tooltip at playbar position') + '\
									<br /><br />\n\
									' + Yii.t('app7020', 'or') + '\
									<br /><br />\n\
									' + Yii.t('app7020', 'Select a tooltip from the video <br /> timeline to edit position and resize it.') + '\
								</div>\n\
								<div class="editing" ng-show="edit">\n\
									<p>' + Yii.t('app7020', 'Text') + '</p>\n\
									<p style="color: red;" ng-show="errors.textLimit">{{errors.textLimit[0]}}</p>\n\
									<textarea class="tooltip-text" ui-tinymce="$parent.tooltipEditorMCE" ng-model="tooltip.text">The part highlighted in blue is the new flange component.</textarea>\n\
									<p>' + Yii.t('app7020', 'Style (dark/light)') + '</p>\n\
									<div class="thumbs-wrapper clearfix">\n\
										<div class="dark-thumb clearfix" ng-click="tooltip.tooltipStyle = 0"><span class="circle"></span><div class="lines-wrapper"><span class="line1"></span><span class="line2"></span></div><i class="radio-dark" ng-class="{\'fa fa-check-circle-o\': tooltip.tooltipStyle == 0, \'fa fa-circle-o\': tooltip.tooltipStyle == 1}"></i></div>\n\
										<div class="light-thumb clearfix" ng-click="tooltip.tooltipStyle = 1"><span class="circle"></span><div class="lines-wrapper"><span class="line1"></span><span class="line2"></span></div><i class="radio-light" ng-class="{\'fa fa-check-circle-o\': tooltip.tooltipStyle == 1, \'fa fa-circle-o\': tooltip.tooltipStyle == 0}"></i></div>\n\
									</div>\n\
									<div class="footer">\n\
										<button ng-click="submitTooltip(tooltip)" ng-disabled="disableClick" class="save">' + Yii.t('app7020', 'Save') + '</button>\n\
										<button ng-click="deleteTooltip(tooltip)" ng-disabled="disableClick" class="delete">' + Yii.t('app7020', 'Delete') + '</button>\n\
									</div>\n\
								</div>\n\
							</div>\n\
						</div>'
		};
		function link(scope, iElem, iAttrs) {

			scope.edit = false;
			scope.modified = false;
			scope.blank = undefined;
			scope.tooltip = undefined;
			scope.disableClick = false;
			scope.items = undefined;
			scope.modal = undefined;
			scope.availablePosition = true;
			scope.errors = undefined;

			scope.switchMode = function () {

				if (scope.edit) {
					/* If click close button revert all changes in timeline */
					scope.$parent.$broadcast('update.timeline.item', scope.blank);
					scope.errors = undefined;
					scope.modified = false;
					scope.blank = undefined;
					scope.tooltip = undefined;
					scope.edit = !scope.edit;
				} else {
                    /* Create new one blank tooltip */
					scope.$parent.$broadcast('request.timeline.add.item');
					scope.errors = undefined;
				}
			};

			scope.$on('current.tooltip', function (e, v) {
				if (scope.modified)
					return;

				if (typeof v.tooltip === 'undefined' && !scope.modified) {
					scope.edit = false;
					scope.tooltip = undefined;
					scope.$digest();
					return;
				}

				/* Edit selected tooltip */
				if (typeof v.tooltip !== 'undefined') {
					if (typeof scope.tooltip !== 'undefined' && v.tooltip.id != scope.tooltip.id) {
						scope.tooltip = undefined;
						scope.$digest();
					}
					scope.items = v.items;
					scope.tooltip = {
						id: v.tooltip.id,
						text: v.tooltip.content,
						tooltipStyle: v.tooltip.class == "app7020-tooltip-light" ? 1 : 0,
						durationFrom: Math.round(v.tooltip.start / 1000),
						durationTo: Math.round(v.tooltip.end / 1000)
					};
					scope.blank = angular.copy(scope.tooltip);
					scope.edit = true;

					if (v.tooltip.id == 0) {
						scope.modified = true;
					} else {
						scope.$digest();
					}
				}
			});

			scope.$watch('tooltip.text', function (n, o) {
				if (typeof n !== 'undefined' && typeof o !== 'undefined')
					scope.modified = true;
			});

			scope.$watch('tooltip.tooltipStyle', function (n, o) {
				if (typeof n !== 'undefined' && typeof o !== 'undefined')
					scope.modified = true;
			});

			scope.$on('update.range', function (e, v) {
				scope.tooltip.durationFrom = v.durationFrom;
				scope.tooltip.durationTo = v.durationTo;
				scope.modified = true;
				scope.$digest();
			});

			scope.$watch('modified', function (n) {
				scope.$parent.$broadcast('update.selectable', n);
			}, true);

			scope.$on('timeline.available.position', function (e, v) {
				scope.availablePosition = v;
				if (!scope.$parent.$$phase)
					scope.$digest();
			});

			scope.submitTooltip = function (tooltip) {
				scope.disableClick = true;

				var prepareRows = [], rows = [], itemPerRow = [];
                $window.timeline.itemsData.get($window.timeline.getVisibleItems()).forEach(function (item) {
					prepareRows.push($('#assetTooltipTimeline .vis-item[data-id="' + item.id + '"]').position().top);
				});
				/* Remove dublicated values from array */
				$.each(prepareRows, function (i, e) {
					if ($.inArray(e, rows) == -1)
						rows.push(e);
				});
				rows.sort(function (a, b) {
					return b - a;
				});

                $window.timeline.itemsData.get($window.timeline.getVisibleItems()).forEach(function (item) {
					itemPerRow.push({
						id: item.id,
						row: rows.indexOf($('#assetTooltipTimeline .vis-item[data-id="' + item.id + '"]').position().top) + 1
					});
				});

                tooltip.tooltipRow = 1;
                if($('#assetTooltipTimeline .vis-item[data-id="' + tooltip.id + '"]').length)
                    tooltip.tooltipRow = rows.indexOf($('#assetTooltipTimeline .vis-item[data-id="' + tooltip.id + '"]').position().top) + 1;

				$http.post('/lms/index.php?r=restdata', {
					endpoint: 'tooltips_crud',
					params: {
						action: 'edit',
						assetId: $routeParams.id,
						tooltip: tooltip,
						items: scope.convertArrayToObject(itemPerRow)
					}
				}).then(function ($result) {
					if ($result.data.success) {
						if (scope.tooltip.id == 0)
							scope.tooltip._id = $result.data.itemId;

						scope.tooltip.text = $result.data.tooltip_text;
						scope.$parent.$broadcast('update.timeline.item', scope.tooltip);
						scope.$emit('flowplayer.replace.cuepoints', $result.data.cuepoints);
						scope.$emit('flowplayer.replace.tooltips', $result.data.tooltips);
						scope.tooltip = undefined;
						scope.errors = undefined;

						scope.modified = false;
						scope.edit = false;
						scope.disableClick = false;
					} else {
						scope.errors = $result.data.errors;
						if (typeof scope.errors !== 'undefined') {
							if (typeof scope.errors.textLimit !== 'undefined') {
								scope.disableClick = false;
							}
						} else {
							scope.$parent.$broadcast('delete.timeline.item', scope.tooltip);
							scope.tooltip = undefined;
							scope.modified = false;
							scope.edit = false;
							scope.disableClick = false;
						}
					}
				}, function ($error) {
					scope.disableClick = false;
				});
			};

			scope.deleteTooltip = function (tooltip, force) {
				if (typeof scope.modal === 'undefined' && force !== true) {
					var d2Options = {
						modalClass: 'confirm_delete_tooltip',
						id: 'confirm_delete_tooltip',
						title: 'Delete',
						autoOpen: true,
						removeOnClose: true
					};
					/* Build dialog2 instance */
					scope.modal = new Object({
						body: {
							text: $filter('HtmlToPlainText')(tooltip.text),
							agree: false
						},
						onSubmit: function () {
							if (scope.modal.body.agree) {
								scope.modal.instance.dialog2('close');
								scope.modal = undefined;
								scope.deleteTooltip(scope.tooltip, true);
							}
						}
					});
					scope.modal.instance = $($compile(renderDeleteModal())(scope)).dialog2(d2Options);
                    $(document).find('.confirm_delete_tooltip .save-dialog').addClass('disabled');
                    $(document).find('.confirm_delete_tooltip').on('click', '[type="checkbox"]',function(){
                        if($(this).prop('checked')){
                            $(document).find('.confirm_delete_tooltip .save-dialog').removeClass("disabled");
                        } else {
                            $(document).find('.confirm_delete_tooltip .save-dialog').addClass("disabled");
                        }
                    });
					/* Destroy dialog instance after close a dialog */
					angular.element(document).one('dialog2.closed', '#confirm_delete_tooltip', function () {
						scope.modal = undefined;
					});
					return;
				}

				if (tooltip.id == 0) {
					scope.edit = false;
					scope.modified = false;
					scope.$parent.$broadcast('delete.timeline.item', scope.tooltip);
					scope.tooltip = undefined;
					return;
				}
				scope.disableClick = true;

				$http.post('/lms/index.php?r=restdata', {
					endpoint: 'tooltips_crud',
					params: {
						action: 'delete',
						assetId: $routeParams.id,
						tooltip: tooltip
					}
				}).then(function ($result) {
					if ($result.data.success) {
						scope.edit = false;
						scope.modified = false;
						scope.$parent.$broadcast('delete.timeline.item', scope.tooltip);
						scope.$emit('flowplayer.replace.cuepoints', $result.data.cuepoints);
						scope.$emit('flowplayer.replace.tooltips', $result.data.tooltips);
						scope.tooltip = undefined;
					}
					scope.disableClick = false;
				}, function ($error) {
					scope.disableClick = false;
				});
			};

			scope.convertArrayToObject = function (arr) {
				var rv = {};
				for (var i = 0; i < arr.length; ++i)
					rv[i] = arr[i];
				return rv;
			};
		}

		function renderDeleteModal() {
			return	'<div class="content-wrapper">\n\
						<p>' + Yii.t('app7020', 'Delete tooltip: ') + '<span ng-bind-html="modal.body.text"></span></p>\n\
						<label>\n\
							<input type="checkbox" ng-model="modal.body.agree"/>\n\
							<span class="label-text"> ' + Yii.t('app7020', 'Yes I want to proceed!') + ' </span>\n\
						</label>\n\
					</div>\n\
					<div class="form-actions">\n\
						<input class="save-dialog green" type="submit" value="' + Yii.t('app7020', 'Confirm') + '" name="yt0" ng-click="modal.onSubmit()" />\n\
						<input class="close-dialog" type="button" value="' + Yii.t('app7020', 'Cancel') + '" name="yt1" />\n\
					</div>';
		}
	}

	Directive.$inject = ['$timeout', '$http', '$filter', '$routeParams', '$compile', '$window'];
	return Directive;
});