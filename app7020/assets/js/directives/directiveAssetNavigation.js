define(function () {

	function directiveAssetNavigation($location, $timeout) {
		return {
			restrict: 'E',
			scope: false,
			replace: true,
			template:
					'<div class="tab-navigation">\n\
					<ul class="app7020BootstrapTabsExtended {{navbarClass}}">\n\
						<li ng-show="navButtons.indexOf(\'menu_peerReview\') > -1">\n\
							<a href="#/assets/peerReview/{{currentAsset}}">' + Yii.t("app7020", "Peer Review") + '</a>\n\
						</li>\n\
						<li ng-show="navButtons.indexOf(\'menu_tooltips\') > -1">\n\
							<a href="#/assets/tooltips/{{currentAsset}}">' + Yii.t("app7020", "Tooltips") + '</a>\n\
						</li>\n\
						<!--COMENTED BECAUSE NOT READY<li>\n\
							<a href="#/assets/reuploadVideo/{{currentAsset}}">' + Yii.t("app7020", "Re-Upload Video") + '</a>\n\
						</li>\n\
						<li>\n\
							<a href="#/assets/settings/{{currentAsset}}">' + Yii.t("app7020", "Custom setting") + '</a>\n\
						</li>COMENTED BECAUSE NOT READY-->\n\
						<li class="back-arrow skip">\n\
							<a href="{{channelPath}}"><i class="fa fa-angle-left"></i></a>\n\
						</li>\n\
					</ul>\n\
					<div class="buttons-controls">\n\
						<a href="javascript:;" ng-show="navButtons.indexOf(\'ask\') > -1" class="fa-stack green scrollToAsk" data-original-title="' + Yii.t('app7020', 'Ask the Expert') + '">\n\
							<i class="fa fa-user fa-stack-1x"></i><i class="fa fa-comment-o fa-stack-1x"></i>\n\
						</a>\n\
						<a href="' + app7020Options.analyticsController + '/index&id={{currentAsset}}" ng-show="navButtons.indexOf(\'stats\') > -1" class="grey" data-original-title="' + Yii.t('app7020', 'Analytics') + '">\n\
							<i class="fa fa-bar-chart"></i>\n\
						</a>\n\
						<a href="#/assets/peerReview/{{currentAsset}}" ng-show="navButtons.indexOf(\'edit\') > -1" class="blue" data-original-title="' + Yii.t('app7020', 'Edit mode') + '">\n\
							<i class="fa fa-pencil"></i>\n\
						</a>\n\
						<a href="#/assets/view/{{currentAsset}}" ng-show="navButtons.indexOf(\'view\') > -1" class="blue" data-original-title="' + Yii.t('app7020', 'Back to view mode') + '">\n\
							<i class="fa fa-share-square-o"></i>\n\
						</a>\n\
						<a href="javascript:;" ng-show="navButtons.indexOf(\'delete\') > -1" data-content-id="{{currentAsset}}"  data-method="delete" ng-click="delete()" class="red" data-original-title="' + Yii.t('app7020', 'Delete') + '">\n\
							<i class="fa fa-trash-o"></i>\n\
						</a>\n\
					</div>\n\
				</div>',
			link: link
		};

		function link(rootScope, iElem, iAttrs) {
			$timeout(function () {
				iElem.find('.scrollToAsk').click(function () {
					$('html, body').animate({
						scrollTop: $('#questionsApp').offset().top
					}, 1000);
				});

				iElem.find('.buttons-controls a').tooltip({
					placement: 'top',
					trigger: 'hover'
				});

				rootScope.$on('$locationChangeStart', function (event, next, current) {
					changeActiveClass($location.path());
				});
				changeActiveClass($location.path());

			});

			function changeActiveClass(path) {
				var links = iElem.find('ul.app7020BootstrapTabsExtended li:not(.skip)').removeClass('active');
				links.each(function () {
					if(('#' + path).indexOf($(this).find('a').attr('href')) > -1){
						links = $(this).find('a');
						return false;
					}
				});
				if (links.length > 0)
					links.closest('li').addClass('active');
				iElem.find('ul.app7020BootstrapTabsExtended').activeLine();
			}
		}
	}

	directiveAssetNavigation.$inject = ['$location', '$timeout'];
	return directiveAssetNavigation;

});