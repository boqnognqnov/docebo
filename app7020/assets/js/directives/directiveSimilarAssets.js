'use strict';
define(function () {
	function Directive($timeout, AJAX, $routeParams, $location) {
		return {
			restrict: '',
			link: link,
			template:
					'<div class="similar-assets-wrapper" ng-show="dataIsLoaded">\n\
				<button class="prev fa fa-angle-up" ng-click="prev($event)" ng-disabled="currentPage == 1" ng-hide="total < 6"></button>\n\
				<div class="items-wrapper">\n\
					<div class="item" ng-repeat="item in items | startFrom: (currentPage - 1) * pageSize | limitTo: pageSize" ng-click="goToAsset()">\n\
						<figure>\n\
							<img ng-src="{{item.thumbnail_url}}" alt="" />\n\
							<!--<span class="label-top-left fa fa-clock-o" style="color: #fff; border-top-color: #EA4B35;"></span>-->\n\
							<span class="label-type">{{item.asset_type}}</span>\n\
							<!--<span class="label-time" ng-if="item.asset_type == \'video\'">{{item.asset_id}}</span>-->\n\
						</figure>\n\
						<section>\n\
							<p class="info">\n\
								<!--<span class="inProgress">In progress</span>-->\n\
								<span class="views">{{item.views}} ' + Yii.t('app7020', 'views') + '</span>\n\
								<span class="ago">{{item.update_date}}</span>\n\
							</p>\n\
							<p class="title">{{item.title}}</p>\n\
						</section>\n\
					</div>\n\
				</div>\n\
				<button class="next fa fa-angle-down" ng-disabled="currentPage >= similar_total/pageSize || forceDisableNext" ng-click="next($event)" ng-hide="total < 6"></button>\n\
			</div>',
		};

		function link(scope, iElem, iAttrs) {
			scope.currentPage = 1;
			scope.pageSize = 5;
			scope.forceDisableNext = false;
			scope.items = [];
			scope.$watch('similar', function (value) {

				if (typeof value === 'undefined'){
					return;
				}
				if (value.length) {
					scope.items = value;
					scope.total = scope.similar_total;
				}
			});

			scope.prev = function () {
				scope.currentPage = scope.currentPage - 1;
			};

			scope.next = function (e) {
				if (scope.items.length >= (scope.currentPage + 1) * scope.pageSize) {
					scope.currentPage = scope.currentPage + 1;
				} else if (scope.items.length < scope.similar_total) {
					scope.forceDisableNext = true;
					$(e.currentTarget).addClass('loading');
					AJAX.post('/lms/index.php?r=restdata', {
						endpoint: 'asset_similar',
						params: {
							id_asset: $routeParams.id,
							from: scope.items.length,
							count: scope.pageSize
						}
					}, function (response) {
						scope.forceDisableNext = false;
						$(e.currentTarget).removeClass('loading');
						if (response.data.length) {
							response.data.forEach(function (item) {
								scope.items.push(item);
							});
							scope.currentPage = scope.currentPage + 1;
						}
					}, function (response) {
						console.log(response.data);
					});
				} else if (Math.ceil(scope.items.length / scope.pageSize) <= scope.currentPage + 1) {
					scope.currentPage = scope.currentPage + 1;
				}
			};

			scope.goToAsset = function () {
				$location.path('/assets/view/' + this.item.asset_id);
			};
		}
	}

	Directive.$inject = ['$timeout', 'AJAX', '$routeParams', '$location'];
	return Directive;
});