define(['angular'], function (angular) {
	function directiveAssetModal($rootScope) {
		return {
			restrict: 'A',
			scope: false,
			link: function ($scope, element, attr) {
				element.bind('click', function () {
					/* Get container and assetId */
					var container = $(this).closest('.assetActiveStatusesContainer');
					var id = container.attr('data-asset-id');

					/* Open Asset Modal for specific assetId */
					$scope.openMetaformDialogModal(id);

					$(document).on('dialog2.ajax-complete', '#app7020-asset-modal-dialog', function () {
						angular.element(document.body).injector().invoke(function ($compile, $rootScope) {
							$compile($(".app7020-asset-modal-dialog"))($rootScope);
							$rootScope.$apply();
						});
					});
				});
			}
		}
	}

	directiveAssetModal.$inject = ['$rootScope']
	return directiveAssetModal;
});