'use strict';
require.config({
	baseUrl: app7020Options.assetUrl + '/js',
	paths: {
//		analyticsController: 'controllers/analytics/analyticsController',
		angular: '/themes/spt/js/angularJs/angular',
		angularResource: '/themes/spt/js/angularJs/angular-resource',
		analyticsPage: 'modules/analyticsPage',
		chart: '/themes/spt/js/Chart.min',
		pollerLib: '/themes/spt/js/angular-poller-master/angular-poller',
		moment: '/themes/spt/js/angularJs/angular-moment',
		manageInvitations: 'modules/manageInvitations',
		conf: 'conf',
		ngOrder: '/themes/spt/js/angularJs/ng-order-object-by.js',
	},
	shim: {
		angular: {
			exports: 'angular',
//            deps: ['moment']
		},
		angularResource: {
			deps: ['angular']
		},
		analyticsPage: {
			deps: ['angular']
		},
		'plugins/app7020AnalyticStatisticsPlugin': {
			exports: 'app7020AnalyticStatistics',
		},
		manageInvitations: {
			deps: ['analyticsPage', 'chart']
		},
		ngOrder: {
			exports: 'ngOrderObjectBy',
			deps: ['angular']
		},
	},
	priority: [
		'angular',
		'ngOrder',
		'analyticsPage'
	]
});
define('jquery', [], function () {
	return jQuery;
});
define(
		['angular',
			'ngOrder',
			'plugins/app7020AnalyticStatisticsPlugin',
			'manageInvitations',
			'analyticsPage',
			'controllers/inviteToWatch',
			'factory/ngDataFeed',
			'conf',
		],
		function (angular, ngOrder) {

			var app = angular.module('app7020ManageInvitations', ['ngOrderObjectBy']);
			/**
			 * 
			 */
			app.controller('AnalytcsManageInvitations', function () {

			});

			app.config(require('conf'));

			/**
			 * 
			 */
			app.controller('inviteController', require('controllers/inviteToWatch'));

			/**
			 * 
			 */
			app.factory('dataFeed', ('dataFeed', require('factory/ngDataFeed')));


			/**
			 * 
			 */
			$(document).on('dialog2.ajax-complete', '#app7020-invite-dialog', function () {
				angular.element(document.body).injector().invoke(function ($compile, $rootScope) {
					$compile($("#invite_to_watch"))($rootScope);
					$rootScope.$apply();
				});
			});




			/* Run of Angular Application *********************************************/
			angular.bootstrap(document, ['app7020ManageInvitations']);
		}
);