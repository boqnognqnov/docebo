'use strict';
define(function () {
	function Config($httpProvider) {
 		 
		$httpProvider.interceptors.push(function ($q) {  //Interceptor to bind YII_CSRF_TOKEN to every ajax post request
			var csrfInterceptor = {
				param: function (obj) {
					var query = '', name, value, fullSubName, subName, subValue, innerObj, i;
					for (name in obj) {
						value = obj[name];
						if (value instanceof Array) {
							for (i = 0; i < value.length; ++i) {
								subValue = value[i];
								fullSubName = name + '[' + i + ']';
								innerObj = {};
								innerObj[fullSubName] = subValue;
								query += param(innerObj) + '&';
							}
						}
						else if (value instanceof Object) {
							for (subName in value) {
								subValue = value[subName];
								fullSubName = name + '[' + subName + ']';
								innerObj = {};
								innerObj[fullSubName] = subValue;
								query += csrfInterceptor.param(innerObj) + '&';
							}
						}
						else if (value !== undefined && value !== null)
							query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
					}

					return query.length ? query.substr(0, query.length - 1) : query;
				},
				request: function (config) {
					if (config.method === "POST") {
						config.headers['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
						config.data['YII_CSRF_TOKEN'] = window.YII_CSRF_TOKEN;
						config.transformRequest = [function (data) {
								return angular.isObject(data) && String(data) !== '[object File]' ? csrfInterceptor.param(data) : data;
							}];
					}
					return config;
				},
				response: function (response) {
					return response;
				}
			};
			return csrfInterceptor;
		});
	}
	Config.$inject = ['$httpProvider'];
	return Config;

});