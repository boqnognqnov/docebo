/* global app7020Options */
require.config({
	baseUrl: app7020Options.assetUrl + '/js',
	paths: {
		app: 'app7020Activity/app',
		angular: '/themes/spt/js/angularJs/angular',
		moment: '/themes/spt/js/angularJs/angular-moment',
		angularResource: '/themes/spt/js/angularJs/angular-resource',
		ngDataFeed: 'factory/ngDataFeed',
		angularSanitize: '/themes/spt/js/angularJs/angular-sanitize.min',
		chartMin: '/themes/spt/js/Chart.min'
	},
	shim: {
		angular: {
			exports: 'angular'
		},
		chartMin: {
			exports: 'chartMin'
		},
		app: {
			deps: ['angular', 'chartMin']
		},
		angularSanitize: {
			exports: 'ngSanitize',
			deps: ['angular']
		}
	},
	priority: [
		'angular',
		'factory/ngActivity',
		'angularSanitize',
		'app',
		'chartMin'
	]
});

define('jquery', [], function () {
	return jQuery;
});

require(['angular', 'app', 'angularSanitize'], function () {
	angular.bootstrap(document, ['activity']);
});