define([
	'require',
	'conf',
	'factory/ngActivity',
	'controllers/activity/controllerCoachActivity',
	'controllers/activity/controllerShareActivity',
	'directives/chartjs-directive'
], function (require) {
	/* Definition of Angular Application **************************************/
	return  require('angular').module('activity', ['ngSanitize'])

			.config(require('conf'))
			/* Angular controllers ****************************************************/
			.controller('coachActivity', require('controllers/activity/controllerCoachActivity'))
			.controller('shareActivity', require('controllers/activity/controllerShareActivity'))

			/* Angular directives *****************************************************/
			.directive('chart', require('directives/chartjs-directive'))

			/* Angular services *******************************************************/
			.factory('ngActivity', require('factory/ngActivity'))
});