'use strict';
define(function () {
	function Config(pollerConfig) {
		pollerConfig.stopOn = '$routeChangeStart';
		pollerConfig.handleVisibilityChange = true;
		pollerConfig.neverOverwrite = true;
	}
	Config.$inject = ['pollerConfig'];
	return Config;

});