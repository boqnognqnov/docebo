/**
 * 
 * @param {type} d
 * @param {type} t
 * @returns {undefined}
 */
'use strict';
define(['modules/notification'], function () {

	function expertsClass(options) {
		return this.init(options);
	}

	expertsClass.prototype = {
		options: {
			container: '#app7020_Coach_Experts_Channels',
			selectExperts: '#selectExperts',
			multipleAssign: '#multipleAssign',
			debug: false,
		},
		init: function (opt) {
			if (typeof (opt) === 'object') {
				$.extend(this.options, opt);
			}
			if (this.options.debug) {
				debug('Experts :: Init() options');
				debug(this.options);
			}

			$(document).on('click', '.app7020_expert_channels', function (e) {
				e.stopPropagation();
			});
			var that = this;
			$(document).on('click', '.app7020_expert_channles_delete', function () {
				var expertId = $(this).attr('data-expert-id');
				UserMan.selectedUsers = [expertId];
				UserMan.onMassiveActionChange("deleteExperts");
				UserMan.selectedUsers = [];
			});


			this.manageDialogs();
			this.attachListeners();

		},
		manageDialogs: function () {
			var that = this;
			var container = $(that.options.container);
			
			/* EMPTY TIPICS DIALOG */
			$(document).on('click', that.options.container + ' ' + that.options.selectExperts, function () {
				openDialog(false, // Event
						Yii.t('app7020', 'Select users you want to make Experts'), // Title
						app7020Options.axExpertsChannelsController + '/dialogSelectExperts', // Url
						'app7020-selectExperts-dialog', // Dialog-Id
						'app7020-selectExperts-dialog', // Dialog-Class
						'GET', // Ajax Method
						{// Ajax Data

						}
				);
			});

			/* EMPTY EXPERTS DIALOG */
			$(document).on('click', '#app7020-multipleAssign-dialog .prev-dialog', function () {
				var experts = $('#app7020-multipleAssign-dialog #experts').val();
				var channels = $('#channelsGridView').comboGridView('getSelection');
				$('#app7020-multipleAssign-dialog').dialog2("close");
				openDialog(false, // Event
						Yii.t('app7020', 'Select Experts'), // Title
						app7020Options.axExpertsChannelsController + '/dialogMultipleAssign', // Url
						'app7020-multipleAssign-dialog', // Dialog-Id
						'app7020-multipleAssign-dialog', // Dialog-Class
						'GET', // Ajax Method
						{// Ajax Data
							'preselectedUsers': experts,
							'preselectedChannels': channels
						}
				);
			});

			/* Change label on "Confirm" button with "Next" */
			$(document).on('dialog2.content-update', '#app7020-selectExperts-dialog', function () {
				$(this).closest('.modal').find('.modal-footer a.green').text(Yii.t('app7020', 'Next'));
			});

			/* EMPTY EXPERTS DIALOG */
			$(document).on('click', that.options.container + ' ' + that.options.multipleAssign, function () {
				openDialog(false, // Event
						Yii.t('app7020', 'Select Experts'), // Title
						app7020Options.axExpertsChannelsController + '/dialogMultipleAssign', // Url
						'app7020-multipleAssign-dialog', // Dialog-Id
						'app7020-multipleAssign-dialog', // Dialog-Class
						'GET', // Ajax Method
						{// Ajax Data

						}
				);
			});

			/* Change label on "Confirm" button with "Next" */
			$(document).on('dialog2.content-update', '#app7020-multipleAssign-dialog', function () {
				$(".btn.user-selector-confirm").removeClass("disabled");
				$(this).closest('.modal').find('.modal-footer a.btn-docebo.green, .modal-footer a.save-dialog.green').text(Yii.t('app7020', 'Next'));
			});

			/* When click "SELECT EXPERTS" button inside dialog */
			$(document).on('click', '#app7020-multipleAssign-dialog .app7020-dialogEmptyExperts .action a', function () {
				$(that.options.container + ' ' + that.options.selectExperts).trigger("click");
				$('#app7020-multipleAssign-dialog').dialog2('close');
			});
			
			/* When click "CREATE CHANNELS" button inside dialog */
			$(document).on('click', '#app7020-multipleAssign-dialog .app7020-dialogEmptyChannels .action a', function () {
				$('#app7020-multipleAssign-dialog').dialog2('close');
			});
            $(document).on('click', '.modal.app7020-multipleAssign-dialog.users-selector a.btn.close-dialog', function () {
                window.location.reload();
            });
			//when deletion is finished - update experts list
			$(document).on('dialog2.content-update', '#app7020-multipleDeleteExpertsChannels-dialog', function () {
                $(document).on('click', '.app7020-multipleDeleteExpertsChannels-dialog [type="checkbox"]', function(){
                    if($(this).prop('checked')){
                        $(document).find('.app7020-multipleDeleteExpertsChannels-dialog .btn.save-dialog').removeClass('disabled');
                        $(document).find('.app7020-multipleDeleteExpertsChannels-dialog .btn.save-dialog').css('pointer-events', 'auto');
                    }else{
                        $(document).find('.app7020-multipleDeleteExpertsChannels-dialog .btn.save-dialog').addClass('disabled');
                        $(document).find('.app7020-multipleDeleteExpertsChannels-dialog .btn.save-dialog').css('pointer-events', 'none');
                    }
                });
                $(this).closest('.modal').find('.save-dialog').addClass('disabled');
                $(this).closest('.modal').find('.save-dialog').css('pointer-events', 'none');
				var deletedUsers = $(this).find("a.auto-close.success").data("deleted-ids");
				if ($(this).find("a.auto-close.success").length > 0) {
					$('#userman-users-management-grid').yiiGridView('update');
					UserMan.selectedUsers = [];
					$('#userman-users-selected-count').text('0');
				}
			});

			$(document).on('click', '#dialog-channels-selection-form .save-dialog', function () {
				var channels = $('#channelsGridView').comboGridView('getSelection');
				$('#channelsSelected').val(channels);
			});

			$(document).on('dialog2.closed', '#app7020-selectExperts-dialog', function () {
				if($(this).find('.progressBarContentStyle').length > 0){
					$('#userman-users-management-grid').yiiGridView('update');
					$('#app7020_Coach_Experts_Channels .emptyTopicsAndExpertsHintSection').hide();
				}
			});
			
			$(document).on('dialog2.closed', '#app7020-multipleAssign-dialog', function () {
				if($(this).find('.progressBarContentStyle').length > 0){
					$('#userman-users-management-grid').yiiGridView('update');
					$('#app7020_Coach_Experts_Channels .emptyTopicsAndExpertsHintSection').hide();
				}
			});

		},
		deleteChannelsExperts: function (experts) {
			openDialog(false, // Event
					Yii.t('app7020', 'Delete Experts'), // Title
					app7020Options.axExpertsChannelsController + '/deleteExpert', // Url
					'app7020-multipleDeleteExpertsChannels-dialog', // Dialog-Id
					'app7020-multipleDeleteExpertsChannels-dialog', // Dialog-Class
					'GET', // Ajax Method
					{// Ajax Data
						'idUser': experts
					}
			);
		},
		attachListeners: function () {
			var that = this;
			var container = $(that.options.container);
			
			if ($(container).length > 0) {
				$(document).on('combogridview.massive-action-selected', function (e, data) {
					
				});
			}
			
		}

	}

	var experts = new expertsClass;
});

