'use strict';
require.config({
	baseUrl: app7020Options.assetUrl + '/js',
	paths: {
		expertsController: 'experts/expertsController',
	}
});
define('jquery', [], function() { return jQuery; });
require(['expertsController']);