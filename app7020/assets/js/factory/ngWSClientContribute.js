define([], function () {
    'use strict'
    /**
     * 
     * @param {type} $rootScope
     * @param {type} WSMessageAsset
     * @returns {serviceWSClient_L1.WSClient.serviceWSClientAnonym$1}
     */
    function WSClient($rootScope, WSMessageAsset) {
        return  {
            socket: function (server) {

                /**
                 * Store the server address
                 */
                this.server = false;
                /**
                 * strore the server WS handler
                 */
                this.websocket = false;

                /**
                 * Construct of the WebSocket Client 
                 * @param {string} server
                 * @returns {serviceWSClient_L1.WSClient.serviceWSClientAnonym$1.socket}
                 */
                this.init = function (server) {
                    if (typeof (server) != 'undefined') {
                        this.server = server;
                        this.connect();
                    }
                    return this;
                };

                /**
                 * Connection method for WS Client
                 * @returns {serviceWSClient_L1.WSClient.serviceWSClientAnonym$1.socket}
                 */
                this.connect = function () {
                    var that = this;
                    this.websocket = new WebSocket(this.server);
                    this.websocket.onopen = function (ev) {
                        that.onOpen(ev);
                    };

                    this.websocket.onmessage = function (ev) {
                        that.onMessage(ev);
                    };
                    return this;
                };

                /**
                 * What must be do open event
                 * @param {WebSocket} ev
                 * @returns {serviceWSClient_L1.WSClient.serviceWSClientAnonym$1.socket}
                 */
                this.onOpen = function (ev) {
                    var initM = new WSMessageAsset.getMessage([80, 81]);
                    this.send(initM);
                    return this;
                };
                /**
                 * What must be done in message fired event
                 * @param {WebSocket} ev
                 * @returns {serviceWSClient_L1.WSClient.serviceWSClientAnonym$1.socket}
                 */
                this.onMessage = function (ev) {
                    //  callback.apply($rootScope, [ev.data]);
                    return this;
                };

                /**
                 * Method to send data to server
                 * @param {Object} object
                 * @returns {serviceWSClient_L1.WSClient.serviceWSClientAnonym$1.socket}
                 */
                this.send = function (object) {
                    var normalized = this._normalizeToString(object);
                    this.websocket.send(normalized);
                    return this;
                };

                /**
                 * Convert JSON to String data type 
                 * @param {Object} object
                 * @returns {String}
                 */
                this._normalizeToString = function (object) {
                    return JSON.stringify(object);
                };
                return this.init(server);
            }
        }


    }
    WSClient.$inject = ['$rootScope', 'WSMessageAsset'];
    return WSClient;
});