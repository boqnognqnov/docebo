'use strict';
define(function () {
	function CoachActivityService($http) {
		return {
			get: function (url,endpoint,timeframe, userid) {

				/* Prepare request params structure */
				var requestOptions = {
					endpoint: endpoint,
					params: {
						timeframe: timeframe,
						id_user: userid
					}
				};
				return $http.post(url, requestOptions).then(function (resp) {
					return resp.data; // success callback returns this
				});
			}
		}
	}

	CoachActivityService.$inject = ['$http'];
	return CoachActivityService;
});