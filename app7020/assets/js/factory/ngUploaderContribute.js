'use strict';
define(['plUploader', 'plUploaderUI', 'moxie'], function (plUploader, Request) {

	function service($rootScope) {
		return {
			plUploader: function (options) {
				/**
				 * 
				 */
				this.uploadingFileName = false;
				/**
				 * 
				 */
				this.options = {
					runtimes: 'html5,flash',
					url: null,
					multipart: true,
					// chunk_size: '8mb',
					multi_selection: ($rootScope.$$childHead.getHasRequest() > 0) ? false : true,
					browse_button: 'app7020-browse_',
					container: 'app7020-pl-uploader',
					drop_element: 'app7020-pl-uploader-dropzone',
					multipart_params: {
						'key': '${Filename}', // use filename as a key
						'Filename': '${id}', // adding this to keep consistency across the runtimes
						'acl': 'private',
						'Content-Type': '',
						'AWSAccessKeyId': null,
						'policy': null,
						'signature': null
					},
					file_data_name: 'file',
					filters: {
						max_file_size: null,
						mime_types: []
					}
				};
				/**
				 * 
				 * @type 
				 */
				var that = this;


				this.init = function (options) {
					this.options = $.extend({}, this.options, options);
					var uploadObject = new plUploader.Uploader(this.options);
					return uploadObject;

				};

				return this.init(options);
			}
		};
	}
	service.$inject = ['$rootScope'];
	return service;
});