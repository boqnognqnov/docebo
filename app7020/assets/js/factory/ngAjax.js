/**
 * Make Async calls to server with included YII LMS CRSF token 
 * @version 1.1
 * @author Stanimir
 * @returns {ngAjax_L5.AJAX}
 */
'use strict';
define(function () {
	/**
	 * 
	 * @param {type} $http
	 * @param {type} $q
	 * @returns {ngAjax_L8.AJAX.ngAjaxAnonym$0}
	 */
	function AJAX($http, $q) {
		return {
			conf: {},
			/**
			 * Set config before send the request
			 * @param {Object} conf
			 * @returns {ngAjax_L8.AJAX.ngAjaxAnonym$0}
			 */
			setConf: function (conf) {
				this.conf = conf;
				return this;
			},
			/**
			 * Get conf 
			 * @returns {Object}
			 */
			getConf: function () {
				return this.conf;
			},
			/**
			 * 
			 * @param {String} url
			 * @param {Function} success
			 * @param {Function} fail
			 * @returns {Object}
			 */
			get: function (url, success, fail) {
				return $http.get(url, this.getConf()).then(success, fail);
			},
			/**
			 * 
			 * @param {type} url
			 * @param {type} data
			 * @param {type} success
			 * @param {type} fail
			 * @returns {unresolved}
			 */
			post: function (url, data, success, fail) {
				return $http.post(url, data, this.getConf()).then(success, fail);
			}
		}
	}
	AJAX.$inject = ["$http", '$q'];
	return AJAX;
});