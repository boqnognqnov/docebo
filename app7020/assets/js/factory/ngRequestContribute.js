'use strict';
define(['angular'], function (angular) {

    function request($rootScope) {
        return {
            ajax: function (endpoint, data, onSuccess, onError) {
                /**
                 * url fot http request
                 * @type @String
                 */
                this.endpoint = '';
                /**
                 * sended data to endpoint
                 * @type 
                 */
                this.data = {};
                /**
                 * callback on Success stage
                 * @returns {@callback onSuccess}
                 */
                this.onSuccess = function () {};
                /**
                 * Callback On error stage
                 * @returns {@callback onError)}
                 */
                this.onError = function () { }

                this.init = function (endpoint, data, onSuccess, onError) {
                    this.endpoint = endpoint;
                    this.data = data;
                    this.onSuccess = onSuccess;
                    this.onError = onError;
                    this.response();
                };
                /**
                 * General request callback to endpoint
                 * @returns {jqXHR}
                 */
                this.response = function () {
                    var that = this;
                    var request = $.ajax({
                        url: that.endpoint,
                        data: that.data,
                        type: "POST",
                        dataType: "json",
                        success: function (object) {
                            that.onSuccess.apply($rootScope, [object]);
                        },
                        error: function (xhr, status, errorThrown) {
                            console.log("Error: " + errorThrown);
                            console.log("Status: " + status);
                            console.dir(xhr);
                            that.onError.apply($rootScope, [xhr, status, errorThrown]);
                        }
                    });
                    return request;
                };
                return this.init(endpoint, data, onSuccess, onError);
            }
        }
    }
    request.$inject = ["$rootScope"];
    return request;
})