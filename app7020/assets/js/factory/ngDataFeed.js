'use strict';
define(function () {
	function dataFeed($http, $q) {
		return {
			get: function (url) {
				return $http.get(url).then(function (resp) {
					return resp.data; // success callback returns this
				});
			},
			post: function (url, data) {
				return $http.post(url, data).then(function (resp) {
					return resp; // success callback returns this
				});
			}
		}
	}
	dataFeed.$inject = ["$http", '$q'];
	return dataFeed;
});