/**
 * 
 * @param {type} angular
 * @param {type} pollerLib
 * @returns {serviceWSPoller_L5.WSPoller}
 */
'use strict';
define(['angular', 'pollerLib'], function (angular, pollerLib) {
	function WSPoller(poller, WSMessageAsset, $rootScope) {
		return function (server) {

			var $scope = $rootScope.$$childHead;
			var myPoller = false;
			this.init = function (server) {

				myPoller = poller.get(server, {
					action: 'jsonp',
					delay: 15000,
					argumentsArray: [
						{
							params: {
								callback: 'JSON_CALLBACK',
								//assetsId: WSMessageAsset.ids
							}
						}
					]
				});
				return myPoller.promise.then(null, null, function (response) {
					$scope.onServerAnswer(response)
					//return response.data;
				});
			};



			this.init(server);
			return  myPoller;
		}



	}
	WSPoller.$inject = ['poller', 'WSMessageAsset', '$rootScope'];

	return WSPoller;
});