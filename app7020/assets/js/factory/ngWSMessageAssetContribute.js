/**
 * 
 * @param {type} angular
 * @returns {serviceWSMessageAsset_L4.message}
 */
define(['angular'], function (angular) {
	var instance = false;
	/**
	 * 
	 * @returns {Function}
	 */
	return function () {
		/**
		 * General model for structure for send data to server poller or WebSockets
		 * @param {Array} ids All proccessed id 
		 * @param {String} vector
		 * @returns {serviceWSMessageAsset_L1.message.serviceWSMessageAssetAnonym$1.getMessage|serviceWSMessageAsset_L6.message.serviceWSMessageAsset_L15}
		 */
		return  new function () {
			/**
			 * The scope of connection
			 * @type {String}
			 */
			this.connectionType = 'listener';
			/**
			 * The store of Lms instance id
			 * @type {Number}
			 */
			this.lmsId = null;
			/**
			 * 
			 */
			this.object = 'content';
			/**
			 * 
			 */
			this.ids = [];

			/**
			 * 
			 */
			this.vector = 'init';

			/**
			 * 
			 * @param {type} ids
			 * @param {type} vector
			 * @returns {serviceWSMessageAsset_L1.message.serviceWSMessageAssetAnonym$1.getMessage}
			 */
			this.init = function ( ) {
				this.setLmsId();
				return this;
			}

			/**
			 * 
			 * @returns {app7020Options.lmsID}
			 */
			this.getLmsId = function () {
				return this.lmsId;
			};

			/**
			 * 
			 * @param {type} id
			 * @returns {serviceWSMessageAsset_L1.message.serviceWSMessageAssetAnonym$1.getMessage}
			 */
			this.setLmsId = function (id) {
				if (typeof (id) != 'undefined') {
					this.lmsId = id;
				} else {
					this.lmsId = app7020Options.lmsID;
				}
				return this;
			};

			/**
			 * 
			 * @param {type} id
			 * @returns {serviceWSMessageAsset_L1.message.serviceWSMessageAssetAnonym$1.getMessage}
			 */
			this.pushId = function (id) {
				this.ids.push(id);
				return this;
			};

			this.removeId = function (id) {

			};

			/**
			 * 
			 * @param {type} string
			 * @returns {serviceWSMessageAsset_L1.message.serviceWSMessageAssetAnonym$1.getMessage}
			 */
			this.setVector = function (string) {
				if (typeof (string) != 'undefined') {
					this.vector = string;
				} else {
					throw 'Undefined vector for proccess!';
				}
				return this;
			};
			
			

			return this.init();
		}
	}

	 

});