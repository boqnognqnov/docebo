/**
 * General service for wrapp all methods for real time changing of file statuses
 * @author  Stanimir Simeonov
 * @param {Angular} angular
 * @returns {serviceRealTime_L5.RealTime}
 */
'use strict';
define(['angular'], function (angular) {

	/**
	 * Real time model service 
	 * @param {serviceWSPoller_L5.WSMessageAsset} WSMessageAsset
	 * @param {serviceWSPoller_L5.WSClient} WSClient
	 * @param {serviceWSPoller_L5.WSPoller} WSPoller
	 * @returns {Function}
	 */
	function RealTime(WSMessageAsset, WSClient, WSPoller, $rootScope) {
		/**
		 * 
		 * @param {String} server
		 * @param {String} method
		 * @returns {serviceRealTime_L1.RealTime.serviceRealTime_L8}
		 */
		return function (server, method) {
			var $scope = $rootScope.$$childHead;
			this.proccessor = {};
			this.server = '';
			this.method = 'poller'; // allowed sockets and pollers,
			this.setMethod = function (method) {
				this.method = method;
				return this;
			};

			/**
			 * Set the new server for this service 
			 * @param {type} server
			 * @returns {serviceRealTime_L8.RealTime.serviceRealTime_L24}
			 */
			this.setServer = function (server) {
				this.server = server;
				return this;
			};

			/**
			 * 
			 * @param {String} server
			 * @param {type} method
			 * @returns {serviceRealTime_L8.RealTime.serviceRealTime_L24}
			 */
			this.init = function (server, method) {

				if (typeof (method) !== 'undefined') {
					this.method = method;
				}

				if (typeof (server) !== 'undefined') {
					this.server = server;
				}



				/*
				 * Init message for sent to server
				 */
				//WSMessageAsset.setVector('add');
				WSMessageAsset.ids = this.getAllAssetIds();

				/*
				 * Start poller ot WebSocket
				 */
				switch (this.method) {
					case 'poller':
					default:
						this.proccessor = new WSPoller(server);
						break;
					case "socket" :
						new WSClient(server);
						break;

				}
				return this;
			};

			/**
			 * Get all assets id for proccess from general scope
			 * @returns {Array}
			 */
			this.getAllAssetIds = function () {
				var tmpArrayWithIds = new Array();
				var assets = $scope.getAssets();
				if (assets.length > 0) {
					assets.forEach(function (value, index) {
						tmpArrayWithIds.push(parseInt(value.id));
					});
				}
				return tmpArrayWithIds;
			};

			this.getInstance = function () {
				if (this.proccessor !== false) {
					return this.proccessor;
				}
			};

			return this.init(server, method);
		}


	}
	RealTime.$inject = ['WSMessageAsset', 'WSClient', 'WSPoller', '$rootScope'];
	return RealTime;
});