require.config({
	shim: {
		'libs/tags/typeahead.jquery': {
			init: function () {
				return require.s.contexts._.registry['typeahead.js'].factory($);
			}
		},
		'libs/tags/typeahead.bundle': {
			init: function () {
				return require.s.contexts._.registry['bloodhound.js'].factory($);
			}
		}
	}
});
define([
	'require',
	'modules/appendCSS',
	'libs/tags/handlebars',
	'libs/tags/typeahead.jquery',
	'libs/tags/typeahead.bundle',
	'libs/tags/tagmanager'
], function (require, css) {
	new css(app7020Options.assetUrl + '/css/tags/app7020-tags.css')
	function tagsPlugin(element, options) {
		this.$element = null;
		this.$tagsContainer = null;
		this.$options = null;
		this.init(element, options);
	}

	tagsPlugin.prototype = {
		/* Version of module */
		VERSION: '0.1',
		/* Default options for init */
		DEFAULTS: {
			debug: false
		},
		/* Init() to prepare options for using */
		init: function (element, options) {
			/* Set current element to instance */
			this.$element = $(element);
			/* Merge default options with internal unique and external options */
			this.$options = $.extend({}, this.DEFAULTS, options);

			/* Console inited options for current instance */
			if (this.getOptions().debug) {
				console.log("tagsPlugin.init() :: ", this.getOptions());
			}

			/* Run all stuff */
			this.typehead();
		},
		/* Getter for default options */
		getDefaults: function () {
			return this.DEFAULTS;
		},
		/* Getter for merged init options */
		getOptions: function () {
			return this.$options;
		},
		bloodhound: function () {
			this.$tagsContainer = new Bloodhound({
				datumTokenizer: Bloodhound.tokenizers.obj.whitespace('tagText'),
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				limit: 10,
				prefetch: {
					url: $fcbkAutocompleteUrl
				}
			});
			this.$tagsContainer.clearPrefetchCache();
			this.$tagsContainer.initialize(true);
		},
		typehead: function () {
			// if we have blank container wit tags
			var $this = this;
			if (this.$tagsContainer === null) {
				this.bloodhound();
			}
			// proccess prefilled tags
			var prefilled_data = this.$element.data('included');
			var prefilled = new Array();
			if (prefilled_data) {
				prefilled = prefilled_data.split(',');
			}
			var tagApi = this.$element.tagsManager({
				prefilled: prefilled,
				AjaxPush: app7020Options.typehead,
				tagsContainer: this.$element.closest('.tags').find('.tagsContainer'),
				backspace: []
			});
			// typehead
			var typeahead = this.$element.typeahead({
				hint: true,
				highlight: true,
				minLength: 1
			}, {
				displayKey: 'tagText',
				name: 'tagsName',
				source: this.$tagsContainer.ttAdapter()
			});
			typeahead.on('typeahead:selected', function (e, d) {
				tagApi.tagsManager("pushTag", d.tagText);
				$this.$element.typeahead('val', '');
				$this.$element.typeahead('close');
			});
			this.$element.blur(function () {
				$this.$element.typeahead('val', '');
			});
		},
		/* Destroy() to remove inited object into tagsPlugin table */
		destroy: function () {
			this.$element.removeData('tagsPlugin');
		}
	};

	/* Multi instances Plugin definition */
	function tagsPluginDefinition(option) {
		return this.each(function () {
			var $this = $(this);
			var data = $this.data('tagsPlugin');
			var options = typeof option == 'object' && option;
			if (!data)
				$this.data('tagsPlugin', (data = new tagsPlugin(this, options)))

			if (typeof option == 'string')
				data[option]()
		});
	}

	/* Append this module to jQuery API */
	$.fn.app7020TagsPlugin = tagsPluginDefinition;
});