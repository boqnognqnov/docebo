define(['modules/appendCSS', '/themes/spt/js/owl/owl.carousel.min.js'], function (appendCSS) {
	/**
	 * Load needed stylesheets
	 * @param url
	 */
	function loadCss(url) {
		var link = document.createElement("link");
		link.type = "text/css";
		link.rel = "stylesheet";
		link.href = url;
		document.getElementsByTagName("head")[0].appendChild(link);
	}
	loadCss(app7020Options.assetUrl + '/css/owl/owl.carousel.css');
	/**
	 * Init
	 * @param element
	 * @param options
	 */
	var app7020documentContentReader = function (element, options) {
		this.$element = null;
		this.$options = null;
		this.init(element, options);
	}
	/**
	 * Version
	 * @type {string}
	 */
	app7020documentContentReader.prototype.VERSION = '0.1';

	/**
	 * Default options
	 * @type {{target: string, container: string, customNav: string, fullScreenTarget: string, debug: boolean}}
	 */
	app7020documentContentReader.prototype.DEFAULTS = {
		target: '#owl-carousel',
		container: '.app7020ReaderGlobalClass',
		customNav: '.custom-owl-nav',
		fullScreenTarget: '.owl-wrap',
		debug: false
	}

	/**
	 * Init
	 * @param element
	 * @param options
	 */
	app7020documentContentReader.prototype.init = function (element, options) {
		this.$element = $(element);
		this.$options = $.extend({}, this.DEFAULTS, options);
		this.owl();
	}

	/**
	 * Get default options function
	 * @returns {{target: string, container: string, customNav: string, fullScreenTarget: string, debug: boolean}|*}
	 */
	app7020documentContentReader.prototype.getDefaults = function () {
		return this.DEFAULTS;
	}

	/**
	 * Get extended options
	 * @returns {null|*}
	 */
	app7020documentContentReader.prototype.getOptions = function () {
		return this.$options;
	}

	/**
	 * Owl carousel functionality container
	 */
	app7020documentContentReader.prototype.owl = function () {
		var $this = this;
		var contentId = $("#App7020Asset").attr('data-asset-id');
		/**
		 * Get Last Viewed Page
		 */
//		console.log(contentId);
		$this.$element.find($this.getDefaults().target).on('initialized.owl.carousel', function (e) {
//			console.log(e.item);
			if ($(document).data('$scope').data.logged_user != $(document).data('$scope').data.owner_id) {
				$.ajax(
						{
							url: app7020Options.contributeController + '/AxCreateDocumentTracking',
							method: 'GET',
							dataType: 'json',
							data: {contentId: contentId}
						}
				).success(function (data) {
					if (data) {
						if (e.item.count == 1) {
							$('#totalContentViews').html(data.views);
						}
					}
				});
			}
			var count = e.item.count - 1;

			if (count == 0) {
				$(".custom-owl-nav .owl-prev-custom,.custom-owl-nav .owl-next-custom").addClass('hidden');
				//$('#info li.currp').text(++item);
			} else {
				if (e.item.index == 0) {

					$(".custom-owl-nav .owl-prev-custom").addClass('disabled');
				} else {
					$(".custom-owl-nav .owl-prev-custom").removeClass('disabled');
				}

				if (e.item.index == count) {
					$(".custom-owl-nav .owl-next-custom").addClass('disabled');

				} else {
					$(".custom-owl-nav .owl-next-custom").removeClass('disabled');
				}
			}


			var startpos = $this.$element.find($this.getDefaults().target).attr('data-page');
			if (!startpos) {
				$this.$element.find($this.getDefaults().target).trigger('to.owl.carousel', [0, 0, true]);
			} else {
				$this.$element.find($this.getDefaults().target).trigger('to.owl.carousel', [startpos, 0, true]);
			}

		});

		/**
		 * Initialize owl carousel with options
		 */
		$this.$element.find($this.getDefaults().target).owlCarousel({
			loop: false,
			center: true,
			nav: false,
			dots: false,
			navContainer: $this.getDefaults().customNav,
			responsive: {
				0: {
					items: 1
				},
				600: {
					items: 1
				},
				1000: {
					items: 1
				}
			}
		});

		/**
		 * Refresh owl carousel container on dom change
		 */
		$this.$element.on('click', function (e) {
			$this.$element.find($this.getDefaults().target).trigger('refresh.owl.carousel');
			if (document.createEvent) {
				var ev = document.createEvent('Event');
				ev.initEvent('resize', true, true);
				window.dispatchEvent(ev);
//                window.dispatchEvent( new Event('resize') );
			} else {
				document.body.fireEvent('onresize');
			}

		});

		$this.$element.find($this.getDefaults().target).on('resized.owl.carousel', function (e) {
			$this.$element.find($this.getDefaults().target).trigger('refresh.owl.carousel');
			if (document.createEvent) {
				window.dispatchEvent(new Event('resize'));
			} else {
				document.body.fireEvent('onresize');
			}

		});


		/**
		 * Custom prev hook
		 */
		$this.$element.find($this.getDefaults().customNav).on('click', 'a.owl-prev-custom', function () {
			$this.$element.find($this.getDefaults().target).trigger('prev.owl.carousel', [300]);
		});

		/**
		 * Custom next hook
		 */
		$this.$element.find($this.getDefaults().customNav).on('click', 'a.owl-next-custom', function () {
			$this.$element.find($this.getDefaults().target).trigger('next.owl.carousel', [300]);
		});

		/**
		 * Keyboar arrow navigation for owl carousel
		 */
		$this.$element.keyup(function (e) {
			if (e.keyCode == 37) {
				$this.$element.find($this.getDefaults().target).trigger('prev.owl.carousel', [300]);
			}
			if (e.keyCode == 39) {
				$this.$element.find($this.getDefaults().target).trigger('next.owl.carousel', [300]);
			}
		});

		/**
		 * Refresh Last Viewed Page
		 *
		 */
		$this.$element.find($this.getDefaults().target).on('translated.owl.carousel', function (e) {
			var currPage = $this.$element.find($this.getDefaults().target).attr('data-page');
			var count = e.item.count - 1;
			if (e.item.index == 0) {

				$(".custom-owl-nav .owl-prev-custom").addClass('disabled');
			} else {
				$(".custom-owl-nav .owl-prev-custom").removeClass('disabled');
			}

			if (e.item.index == count) {
				$(".custom-owl-nav .owl-next-custom").addClass('disabled');

			} else {
				$(".custom-owl-nav .owl-next-custom").removeClass('disabled');
			}
			var item = e.item.index;
			if (item > currPage) {
				if ($(document).data('$scope').data.logged_user != $(document).data('$scope').data.owner_id) {
					$.ajax(
							{
								url: app7020Options.contributeController + '/AxUpdateDocumentTracking',
								method: 'GET',
								dataType: 'json',
								data: {contentId: contentId, page: item}
							}
					).success(function (data) {
						$('#totalContentViews').html(data.views);
					});
				}
				$this.$element.find($this.getDefaults().target).attr('data-page', item);
			}

			$('#info li.currp').text(++item);
		});


		/**
		 * Full screen functionality
		 */

		$this.$element.find($this.getDefaults().customNav).on('click', '.expand', function () {
			doc_reader_full_screen('owl-wrap');
		});

		$this.$element.find($this.getDefaults().customNav).on('click', '.compress', function () {
			doc_reader_full_screen_exit();
		});

		function doc_reader_full_screen(id) {
			var i = document.getElementById(id);
			// go full-screen
			if (i.requestFullscreen) {
				i.requestFullscreen();
			} else if (i.webkitRequestFullscreen) {
				i.webkitRequestFullscreen();
			} else if (i.mozRequestFullScreen) {
				i.mozRequestFullScreen();
			} else if (i.msRequestFullscreen) {
				i.msRequestFullscreen();
			}

		}

		function doc_reader_full_screen_exit() {
			if (document.exitFullscreen) {
				document.exitFullscreen();
			} else if (document.webkitExitFullscreen) {
				document.webkitExitFullscreen();
			} else if (document.mozCancelFullScreen) {
				document.mozCancelFullScreen();
			} else if (document.msExitFullscreen) {
				document.msExitFullscreen();
			}
		}

		document.addEventListener("fullscreenchange", FShandler);
		document.addEventListener("webkitfullscreenchange", FShandler);
		document.addEventListener("mozfullscreenchange", FShandler);
		document.addEventListener("MSFullscreenChange", FShandler);

		function FShandler() {
			if (
					document.fullscreenElement ||
					document.webkitFullscreenElement ||
					document.mozFullScreenElement ||
					document.msFullscreenElement
					) {

				$('#owl-wrap').addClass('full-screen-enabled');
				$('.custom-owl-nav').focus();

			} else {
				$('#owl-wrap').removeClass('full-screen-enabled');
				$('.custom-owl-nav').focus();
			}
		}

	}

	/**
	 * Get Parameter from url
	 * @param sParam
	 * @returns {*}
	 */
	app7020documentContentReader.prototype.parameter = function (sParam) {
		var sPageURL = window.location.search.substring(1);
		var sURLVariables = sPageURL.split('&');
		for (var i = 0; i < sURLVariables.length; i++)
		{
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] == sParam)
			{
				return sParameterName[1];
			}
		}
	}

	function Plugin(option) {
		return this.each(function () {
			var $this = $(this);
			var data = $this.data('app7020documentContentReader');
			var options = typeof option == 'object' && option;
			if (!data) {
				$this.data('app7020documentContentReader', (data = new app7020documentContentReader(this, options)));
			}
		})
	}

	$.fn.app7020documentContentReader = Plugin;

});
