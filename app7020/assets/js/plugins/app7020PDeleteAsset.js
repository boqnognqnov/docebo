define(function ( ) {
	return  function (assetId) {
		this.init = function (assetId) {
			if (assetId !== false && typeof (assetId) == 'number') {
				this.openPromptModal(assetId);
			}
			return this;
		};

		/**
		 * Open prompt modal for ask user for laterinstructions
		 * 
		 * @param {Number} assetId
		 * @returns {app7020DeleteAsset_L1.assetDeletePlugin}
		 */
		this.openPromptModal = function (assetId) {
			var that = this;

			var dialog = {
				title: Yii.t('app7020', 'Delete'),
				url: app7020Options.axAssetsController + '/axShowConfirmDeleteAssetModal&assetId=' + parseInt(assetId),
				id: 'app7020-confirm-delete-modal-dialog',
				class: 'app7020-confirm-delete-modal-dialog',
				data: {assetId: assetId, showName: true}
			};
			openDialog(false, dialog.title, dialog.url, dialog.id, dialog.class, 'POST', dialog.data);
			$("html, body").animate({scrollTop: 0});
			return this;
		};

		this.sendToDelete = function (id) {
			var $this = this;
			var response = $.ajax({
				method: "POST",
				url: app7020Options.axAssetsController + '/AxDeleteFile',
				data: {
					id: id,
					local: false
				}
			});
			response.complete(function (data) {
				var pattern1 = '/assets/view/';
				var pattern2 = '/assets/edit/';
				var element1 = $(".single-channel-app.widget-channels-functions");
				var element2 = $(".channels-app.widget-channels-functions")
				if (element1.length || element2.length) {
					var itemsCount = parseInt($(".items-count").html());
					var newItems = itemsCount - 1 ;
					$(".asset-id-"+id).remove();
					if(newItems >=0) {
						$(".items-count").html(newItems);
					}
				} else {
					if (top.location.hash.indexOf(pattern1) || top.location.hash.indexOf(pattern2)) {
						window.location.href = app7020Options.channels;
					} else {
						top.location.reload();
					}
				}
			});
		};

		return this;
	}

});