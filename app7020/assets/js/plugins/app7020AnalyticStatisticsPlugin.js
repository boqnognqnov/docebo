define(function () {

	function app7020AnalyticStatisticsPlugin(element, options) {
		this.$element = null;
		this.$options = null;
		this.XHR = {};
		this.init(element, options);
	}

	app7020AnalyticStatisticsPlugin.prototype = {
		/* Version of module */
		VERSION: '0.3',
		/* Default options for init */
		DEFAULTS: {
			containerClass: 'app7020AnalyticStatistics',
			containerId: null,
			ajaxUrl: null,
			debug: false
		},
		/* Init() to prepare options for using */
		init: function (element, options) {
			/* Set current element to instance */
			this.$element = $(element);

			/* Merge default options with internal unique and external options */
			this.$options = $.extend({}, this.getDefaults(), options, {containerId: this.$element.attr('id')});

			/* Console inited options for current instance */
			if (this.getOptions('debug')) {
				console.log("app7020AnalyticStatistics.init() :: ", this.getOptions());
			}
			if (typeof this.getOptions('init') !== 'undefined')
				this.getOptions('init')(this);
		},
		/* Getter for default options */
		getDefaults: function (optionName) {
			if (typeof optionName !== 'undefined') {
				return this.DEFAULTS[optionName];
			}
			return this.DEFAULTS;
		},
		/* Getter for merged init options */
		getOptions: function (optionName) {
			if (typeof optionName !== 'undefined') {
				return this.$options[optionName];
			}
			return this.$options;
		},
		getAjaxOptions: function () {
			var $this = this;
			return {
				url: $this.getOptions('ajaxUrl'),
				method: "GET",
				dataType: 'json',
				data: {
					id: $this.getOptions('containerId'),
					idContent: $($this.$element).attr('data-idObject')
				},
				complete: function () {
					$this.XHR[$this.getOptions('containerId')] = null;
				},
				success: function (data, status) {
					if ($this.getOptions('debug'))
					if (typeof $this.getOptions('afterAjaxUpdate') !== 'undefined')
						$this.getOptions('afterAjaxUpdate')($this.getOptions('containerId'), $this.getAjaxOptions(), data);
				}
			};
		},
		update: function () {
			if (this.getOptions('debug'))
			if (typeof this.getOptions('beforeAjaxUpdate') !== 'undefined')
				this.getOptions('beforeAjaxUpdate')(this.getOptions('containerId'), this.getAjaxOptions());

			if (this.XHR[this.getOptions('containerId')] != null) {
				this.XHR[this.getOptions('containerId')].abort();
			}
			this.XHR[this.getOptions('containerId')] = $.ajax(this.getAjaxOptions());
		}
	}
	/* Multi instances Plugin definition */
	function app7020AnalyticStatisticsPluginDefinition(option) {
		var options = typeof option == 'object' && option;
		return this.each(function () {
			var $this = $(this);
			var data = $this.data('app7020AnalyticStatistics');
			if (!data)
				$this.data('app7020AnalyticStatistics', (data = new app7020AnalyticStatisticsPlugin(this, options)))

			if (typeof option == 'string')
				data[option]()
		});
	}
	$.fn.app7020AnalyticStatistics = app7020AnalyticStatisticsPluginDefinition;
	/* Append this module to jQuery API */
	return;
});
