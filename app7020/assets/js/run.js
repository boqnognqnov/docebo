define(['modules/breadcrumps'], function (breadcrumps) {

	function Run($rootScope, $templateRequest, $location, AJAX, $filter) {

		/* Toggle navigation from tabs to back button and over */
		$rootScope.navbarClass = '';
		$rootScope.defaultPath = '/assets/view/';
		$rootScope.channelPath = '/lms/index.php?r=channels/index/#/';
		$rootScope.currentAsset = '';
		$rootScope.prevUrl = '#/';
		$rootScope.navButtons = [];
		$rootScope.statusBarMode = false;
		$rootScope.breadcrumb_asset = false;
		$rootScope.createPRtoTT = [];
		$rootScope.$on("$routeChangeStart", function (event, next, current) {
			if (next) {
				$rootScope.currentAsset = next.params.id;
				var buildedDefPath = $rootScope.defaultPath + $rootScope.currentAsset;
				switch (next.originalPath) {
					case '/assets/view/:id':
						$rootScope.navbarClass = 'show-back';
						AJAX.post('/lms/index.php?r=restdata', {
							endpoint: 'asset_view_nav_buttons',
							params: {
								id_asset: $rootScope.currentAsset,
								mode: 'view'
							}
						}, function (response) {
							$rootScope.navButtons = response.data.actions;

						}, function (response) {
							console.log(response.actions);
						});
						$rootScope.statusBarMode = 'view';
						$rootScope.breadcrumb_asset = false;
						break;
					case '/assets/edit/:id':
						$location.path(buildedDefPath);
						AJAX.post('/lms/index.php?r=restdata', {
							endpoint: 'asset_view_nav_buttons',
							params: {
								id_asset: $rootScope.currentAsset,
								mode: 'edit'
							}
						}, function (response) {
							$rootScope.navButtons = response.data.actions;

						}, function (response) {
							console.log(response.actions);
						});
						$rootScope.breadcrumb_asset = false;
						break;
					default:
						AJAX.post('/lms/index.php?r=restdata', {
							endpoint: 'asset_view_nav_buttons',
							params: {
								id_asset: $rootScope.currentAsset,
								mode: 'edit'
							}
						}, function (response) {
							if (response.data.actions.indexOf("menu_peerReview") <= 0) {
								window.location.href = $rootScope.channelPath;
								return false;
							}
							$rootScope.navButtons = response.data.actions;

						}, function (response) {
							console.log(response.actions);
						});
						$rootScope.statusBarMode = 'edit';
						$rootScope.navbarClass = '';
						$rootScope.breadcrumb_asset = false;
						break;
				}
			}
		});

		$rootScope.$watch('breadcrumb_asset', function (newValue) {
			new breadcrumps({
				type: (newValue) ? 'asset' : '',
				breadcrumps: $filter('HtmlToPlainText')(newValue)
			});
		});
	}
	Run.$inject = ['$rootScope', '$templateRequest', '$location', 'AJAX', '$filter'];
	return Run;


});