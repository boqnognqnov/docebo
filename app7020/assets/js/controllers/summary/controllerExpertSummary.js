define(['modules/notification'],function () {
	function expertSummary($scope, ngActivity) {
		$scope.text_color = null;
		var length = null;
		$scope.idUser = $(document).find('#myActivity').attr('data-userid');
		$('#generate-expert-report').on('click', function (e) {
			e.preventDefault();

			function verifyInitExpertData() {
				var data = $('#idExpert').val();
				if (!data) {
					$('#advanced-search-expert-report').css('border-color', '#ff0000');
					return false;
				}
				$('#advanced-search-expert-report').css('border-color', '#E4E6E5');
				return true;
			}

			if (verifyInitExpertData()) {
				var url = $(this).attr('href'), expertid = $('#idExpert').val();
				window.location.href = url + '&id=' + encodeURIComponent(expertid);
			}
		});

		$('.report-action-print').click(function () {
			window.print();
		});

		$("#advanced-search-expert-report").autocomplete({
			source: function (request, response) {

				$.ajax({
					url: app7020Options.expertsAutoComplete,
					dataType: "json",
					type: "POST",
					data: {
						data: {
							query: request.term
						}
					},
					success: function (data) {
						var options = [];
						$.each(data.options, function (index, value) {
							options.push({
								"value": index,
								"label": value
							})
						});
						response(options);
					}
				});
			},
			minLength: 1,
			select: function (event, ui) {
				$('#idExpert').remove();
				var hiddenField = $('<input type="hidden" id="idExpert" name="idExpert" value="' + ui.item.value + '"/>');
				$("#advanced_search_expert_report_container").append(hiddenField);
				ui.item.value = ui.item.label;
			},
			focus: function (event, ui) {
				
				event.preventDefault();
				$("#advanced-search-expert-report").val(ui.item.label);
				$('#idExpert').remove();
				var hiddenField = $('<input type="hidden" id="idExpert" name="idExpert" value="' + ui.item.value + '"/>');
				$("#advanced_search_expert_report_container").append(hiddenField);
			}
		});
		$scope.PointsMax = $scope.data;
		$scope.lineoptions = {
			//Boolean - Whether the line is curved between points
			bezierCurve: false,
			//Number - Radius of each point dot in pixels
			pointDotRadius: 6,
			//Number - Pixel width of point dot stroke
			pointDotStrokeWidth: 2,
			scaleStartValue: 0,
			scaleStepWidth: 1,
			//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
			pointHitDetectionRadius: 8,
			showTooltips: false,
			onAnimationComplete: function () {
				var ctx = this.chart.ctx;
				ctx.font = this.scale.font;
				ctx.fillStyle = this.scale.textColor
				ctx.textAlign = "center";
				ctx.textBaseline = "bottom";
				this.datasets.forEach(function (dataset) {
					dataset.points.forEach(function (points) {
						if (points.value != "0") {
							ctx.fillText(points.value, points.x, points.y - 10);
						}
					});
				})
			}


		}

		$scope.lineoptions2 = {
			//Boolean - Whether the line is curved between points
			bezierCurve: false,
			//Number - Radius of each point dot in pixels
			pointDotRadius: 6,
			//Number - Pixel width of point dot stroke
			pointDotStrokeWidth: 2,
			//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
			pointHitDetectionRadius: 8,
			scaleStartValue: 0,
			scaleStepWidth: 1,
			showTooltips: false,
			onAnimationComplete: function () {
				var ctx = this.chart.ctx;
				ctx.font = this.scale.font;
				ctx.fillStyle = this.scale.textColor
				ctx.textAlign = "center";
				ctx.textBaseline = "bottom";

				this.datasets.forEach(function (dataset) {
					dataset.points.forEach(function (points) {
						if (points.value != "0") {
							ctx.fillText(points.value, points.x, points.y - 10);
						}
					});
				})
			}
		}

		$scope.coachb = {data: [], options: []};
		$scope.peerb = {data: [], options: []};
		$scope.barchart = "";
		$scope.barchart2 = "";
		$scope.timeframes = [{timeframe: 7, value: 'Weekly'}, {timeframe: 30, value: 'Monthly'}, {timeframe: 365, value: "Yearly"}];
		$scope.timeframe = {timeframe: 30, value: 'Monthly'};
		$scope.tab = 1;

		ngActivity.get('/lms/index.php?r=restdata', 'coach_activity', $scope.timeframe.timeframe, $scope.idUser).then(function (data) {
			$scope.barchart = data.activityData.chartMyActivityPerChannelHTML;
			$scope.barchart2 = data.activityData.chartMyPeerActivityPerChannelHTML;
			$scope.coachb = {data: [], options: []};
			$scope.peerb = {data: [], options: []};
			$scope.coach_data = data.activityData;
			if (data.activityData.likes == 0) {
				$scope.likes = 0.1;
			} else {
				$scope.likes = data.activityData.likes;
			}
			if (data.activityData.dislikes == 0) {
				$scope.dislikes = 0.1;
			} else {
				$scope.dislikes = data.activityData.dislikes;
			}


			$scope.coachb.data = {
				labels: data.activityData.chartAnsweredQuestions.labels,
				datasets: [
					{
						fillColor: "rgba(4,100,174,0)",
						strokeColor: "rgba(4,100,174,1)",
						pointColor: "rgba(255,255,255,1)",
						pointStrokeColor: "rgba(4,100,174,1)",
						highlightFill: "rgba(4,100,174,0)",
						highlightStroke: "rgba(4,100,174,1)",
						data: data.activityData.chartAnsweredQuestions.points
					}
				]
			};
			$scope.coachb.options = $scope.lineoptions;
			$scope.coachb.options.scaleOverride = Math.max.apply(Math, data.activityData.chartAnsweredQuestions.points) % 10 === 0 || Math.max.apply(Math, data.activityData.chartAnsweredQuestions.points) < 10 ? true : false;
			$scope.coachb.options.scaleSteps = Math.max.apply(Math, data.activityData.chartAnsweredQuestions.points) + 1;
			$scope.peerb.data = {
				labels: data.activityData.chartReviewedAssets.labels,
				datasets: [
					{
						fillColor: "rgba(4,100,174,0)",
						strokeColor: "rgba(4,100,174,1)",
						pointColor: "rgba(255,255,255,1)",
						pointStrokeColor: "rgba(4,100,174,1)",
						highlightFill: "rgba(4,100,174,0)",
						highlightStroke: "rgba(4,100,174,1)",
						data: data.activityData.chartReviewedAssets.points
					}
				]
			};
			$scope.peerb.options = $scope.lineoptions2;
			$scope.peerb.options.scaleOverride = Math.max.apply(Math, data.activityData.chartReviewedAssets.points) % 10 === 0 || Math.max.apply(Math, data.activityData.chartAnsweredQuestions.points) < 10 ? true : false;
			$scope.peerb.options.scaleSteps = Math.max.apply(Math, data.activityData.chartReviewedAssets.points) + 1;
		});
		$scope.newSelection = function () {
			$scope.coachb = {data: [], options: []};
			$scope.peerb = {data: [], options: []};
			ngActivity.get('/lms/index.php?r=restdata', 'coach_activity', $scope.timeframe.timeframe, $scope.idUser).then(function (data) {
				$scope.barchart = data.activityData.chartMyActivityPerChannelHTML;
				$scope.barchart2 = data.activityData.chartMyPeerActivityPerChannelHTML;
				if (data.activityData.likes == 0) {
					$scope.likes = 0.1;
				} else {
					$scope.likes = data.activityData.likes;
				}
				if (data.activityData.dislikes == 0) {
					$scope.dislikes = 0.1;
				} else {
					$scope.dislikes = data.activityData.dislikes;
				}

				$scope.coach_data = data.activityData;
				$scope.coachb.data = {
					labels: data.activityData.chartAnsweredQuestions.labels,
					datasets: [
						{
							fillColor: "rgba(4,100,174,0)",
							strokeColor: "rgba(4,100,174,1)",
							pointColor: "rgba(255,255,255,1)",
							pointStrokeColor: "rgba(4,100,174,1)",
							highlightFill: "rgba(4,100,174,0)",
							highlightStroke: "rgba(4,100,174,1)",
							data: data.activityData.chartAnsweredQuestions.points
						}
					]
				};

				$scope.coachb.options = $scope.lineoptions;
				$scope.peerb.data = {
					labels: data.activityData.chartReviewedAssets.labels,
					datasets: [
						{
							fillColor: "rgba(4,100,174,0)",
							strokeColor: "rgba(4,100,174,1)",
							pointColor: "rgba(255,255,255,1)",
							pointStrokeColor: "rgba(4,100,174,1)",
							highlightFill: "rgba(4,100,174,0)",
							highlightStroke: "rgba(4,100,174,1)",
							data: data.activityData.chartReviewedAssets.points
						}
					]
				};
				$scope.peerb.options = $scope.lineoptions2;
			});

		};
		$scope.setTab = function (tabId) {
			$scope.tab = tabId;

		};

		$scope.isSet = function (tabId) {
			return $scope.tab === tabId;
		};
		$scope.increment = function (item) {
			return +item + 1;
		}
	}

	expertSummary.$inject = ['$scope', 'ngActivity', '$compile', '$rootScope'];
	return expertSummary;
});