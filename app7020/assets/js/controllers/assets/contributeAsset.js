define(['angular', 'modules/lookingfor', 'modules/notification'], function (angular, t) {
	require('modules/lookingfor');

	function Asset($scope, Uploader, Request, RealTime, WSMessageAsset, $timeout) {
//var a = require('lookingfor');
		var CONVERSION_STATUS_PENDING = 0;
		var CONVERSION_STATUS_INPROGRESS = 1;
		var CONVERSION_STATUS_S3 = 3;
		var CONVERSION_STATUS_FROM_MOBILE = 4;
		var CONVERSION_STATUS_SNS_SUCCESS = 6;
		var CONVERSION_STATUS_SNS_WARNING = 7;
		var CONVERSION_STATUS_SNS_ERROR = 8;
		var CONVERSION_STATUS_VIDEO_LENGTH_ERROR = 9;
		var CONVERSION_STATUS_FINISHED = 10;
		var CONVERSION_STATUS_INREVIEW = 15;
		var CONVERSION_STATUS_UNPUBLISH = 18;
		var CONVERSION_STATUS_APPROVED = 20;

		var AUTOMATIC_TABS_TRIGGER = true;
		var realTime = false;


		$scope.publishProccessCriteria = [20];
		$scope.finishProccessCriteria = [10, 15, 18];
		$scope.activeProccessCriteria = [0, 1, 3, 6, 7, 8, 9];

		/**
		 * 
		 */
		$scope.hasRequest = 0;

		/**
		 * Rule for show the uplaod form
		 */
		$scope.showUploadForm = true;


		/**
		 * Store global var for URL to general Ajax Controller
		 * @type String
		 */
		$scope.axUrl = app7020Options.assetsAxController;
		/**
		 * Store all assets in scope model to use in FE
		 * @type {Array}
		 */
		$scope.assets = [];
		/**
		 * Store instance of uploader
		 * @var {plUploader.this}
		 */
		$scope.uploader = {};

		/**
		 * Global instance of plUpload
		 */
		$scope.app7020Uploader = {};
		$scope.showShareButton = true;
		/**
		 * When we add file foreach the objects but this is one time with other fired events. 
		 * This flag will wait the events
		 */
		$scope.fileAddedProccessFinished = 0;

		/**
		 * Workflow start all events
		 * We need from any delay for file looping 
		 * @var {Number}
		 */
		$scope.delay = 500; //ms for delay of procces events
		/**
		 * On "BeforeUnload" show alert with number of active (incomplete) uploads;
		 * On Unload make Ajax call to delete failed uploads from DB 
		 * @returns {controllerAsset_L1.Asset.$scope}
		 */
		$scope.unloadActions = function () {
			var counter;
			var singleFileDom = '#app7020TabsPlus .tab-content .tab-pane .assetActiveStatusesContainer';
			var idContentToDelete = [];

			var activeUploadsStatuses = [
				CONVERSION_STATUS_PENDING,
				CONVERSION_STATUS_INPROGRESS,
				CONVERSION_STATUS_S3,
				CONVERSION_STATUS_SNS_WARNING,
				CONVERSION_STATUS_SNS_ERROR,
				CONVERSION_STATUS_VIDEO_LENGTH_ERROR
			];


			$(window).bind('beforeunload', function () {
				$(singleFileDom).each(function (index, element) {
					if ($.inArray(parseInt($(element).attr('data-conversion-status')), activeUploadsStatuses) > -1) {
						idContentToDelete.push(parseInt($(element).attr('data-asset-id')));
					}
				});

				counter =
						parseInt($(singleFileDom + "[data-conversion-status='" + CONVERSION_STATUS_PENDING + "']").length) +
						parseInt($(singleFileDom + "[data-conversion-status='" + CONVERSION_STATUS_INPROGRESS + "']").length) +
						parseInt($(singleFileDom + "[data-conversion-status='" + CONVERSION_STATUS_S3 + "']").length);
				counter = counter == 0 ? 1 : counter;

				return Yii.t('app7020', 'WARNING! You have : ') + counter + Yii.t('app7020', ' active uploads that will be interruped if you leave this page');
				//return Yii.t('app7020', 'WARNING! You have : ') + counter + Yii.t('app7020', ' active uploads that will be interruped if you leave this page');
			});

			$(window).unload(function () {
				new Request.ajax($scope.axUrl + '/axDeleteUnuploaded', {idContent: idContentToDelete});
			});
		};


		/**
		 * Inital point of the controller invoked by partial View
		 * @param {Object} object
		 * @returns {controllerAsset_L1.Asset.$scope}
		 */
		$scope.init = function (object, hasRequest, slackShare) {
			if (typeof (hasRequest) != 'undefined') {
				$scope.setHasRequest(hasRequest);
			}

			if (slackShare) {
				$scope.addLink(slackShare);
			}

			/**
			 * Create new instance of plUploader Wrapper
			 * @type Uploader.plUploader
			 */
			$scope.app7020Uploader = new Uploader.plUploader(object);

			/**
			 * 
			 */
			$scope.app7020Uploader.bind('FilesAdded', function (up, files) {
				$scope.uploader = up;
				$scope.filesAdded(up, files);
			});

			/**
			 * 
			 */
			$scope.app7020Uploader.bind('BeforeUpload', function (up, file) {
				$scope.uploader = up;
				$scope.beforeUpload(up, file);
			});

			/**
			 * 
			 */
			$scope.app7020Uploader.bind('ChunkUploaded', function (up, file, info) {
				$scope.uploader = up;
				$scope.chunkUploaded(up, file, info);
			});
			/**
			 * 
			 */
			$scope.app7020Uploader.bind('UploadProgress', function (up, file) {
				$scope.uploader = up;
				$scope.uploadProgress(up, file);
			});
			/**
			 * 
			 */
			$scope.app7020Uploader.bind('FileUploaded', function (up, file, info) {
				$scope.uploader = up;
				$scope.fileUploaded(up, file);
			});


			/**
			 * 
			 */
			$scope.app7020Uploader.bind('Error', function (up, err) {
				$scope.uploader = up;
				$('#contributeMasterContainer').doceboNotice(err.message, 'error');
//				$scope.error(up, err);
			});
			$scope.app7020Uploader.init();
		};
		/**
		 * Add into scope on init all existings assets 
		 * 
		 * @param {Array} assets
		 * @returns {controllerAsset_L1.Asset.$scope}
		 */
		$scope.assetsInit = function (assets) {
			$scope.assets = removeFailedFiles(assets);
			realTime = new RealTime(app7020Options.axAssetsController + '/axPollerAssetStatuses');
			if (hasProccessedFiles() === false) {
				realTime.getInstance().stop();
			}
			$scope.checkListsCounts();
			return this;
		};

		/**
		 * When have changes of statuses inside contribute lists run this method
		 * he will be counted assets from 0~8 and 10~20 conversion statuses and will be auto move tab and active line correctly
		 */
		$scope.checkListsCounts = function () {
			/* [0] = Active | [1] = Finish | [2] = Publish */
			var proccessingCounts = [{count: 0}, {count: 0}, {count: 0}];
			var isTriggered = false;
			if (AUTOMATIC_TABS_TRIGGER) {
				angular.forEach($scope.assets, function (item) {
					if (item.conversion_status < CONVERSION_STATUS_FINISHED) {
						++proccessingCounts[0].count;
					}
					if (item.conversion_status >= CONVERSION_STATUS_FINISHED && item.conversion_status < CONVERSION_STATUS_APPROVED) {
						++proccessingCounts[1].count;
					}
					if (item.conversion_status >= CONVERSION_STATUS_APPROVED) {
						++proccessingCounts[2].count;
					}
				});
				angular.forEach(proccessingCounts, function (object, index) {
					if (!isTriggered && object.count > 0) {
						$('#contributeMasterContainer .app7020BootstrapTabsExtended li:eq(' + index + ')').trigger('click').find('a').tab('show');
						$('#contributeMasterContainer .app7020BootstrapTabsExtended').boostrapSmoothActiveLine();
						isTriggered = true;
					}
				});
			}
		};

		/**
		 * Cancel functionality of current uploading asset and remove from db and scope
		 * @param {plupload.up} up
		 * @param {plupload.file} file
		 * @param {int} fileId
		 */
		$scope.cancelUploadEvent = function (up, file, assetId) {
			$('#contributeMasterContainer #app7020TabsPlus').on('click', '.assetActiveStatusesContainer[data-asset-id="' + assetId + '"] a[data-cancel-upload]', function () {
				$scope.canceUpload($(this).closest('.assetActiveStatusesContainer').attr('data-asset-id'));
			});
		};

		$scope.canceUpload = function (assetId) {
			$scope.uploader.stop();
			var file;
			angular.forEach($scope.uploader.files, function (item, key) {
				if (item.assetId === assetId) {
					file = item;
				}
			});
			var data = {
				local: true,
				id: parseInt(assetId)
			};
			new Request.ajax($scope.axUrl + '/AxDeleteFile', data,
					function (object) {
						if (object.status === true) {
							$scope.showUploadForm = true;
							$scope.uploader.removeFile(file);
							var index = findAssetPerAssetId(file.assetId);
							$scope.assets.splice(index, 1);
							$scope.$digest();
							$scope.uploader.start();
						}
					},
					function (object) {
						$scope.uploader.start();
					});
		};

		/**
		 * Filter criteria
		 * This will filter result in repeater in published items
		 * Server side manipulations 
		 * 
		 * @param {object} asset
		 * @returns {Boolean}
		 */
		$scope.filterPublishProccessing = function (asset) {
			var current_status = parseInt(asset.conversion_status);
			if ($scope.publishProccessCriteria.indexOf(current_status) !== -1) {
				return true;
			}
			return false;
		};
		/**
		 * Filter criteria
		 * This will filter result in repeater in finished items
		 * Server side manipulations 
		 * 
		 * @param {object} asset
		 * @returns {Boolean}
		 */
		$scope.filterAfterProccessing = function (asset) {
			var current_status = parseInt(asset.conversion_status);
			if ($scope.finishProccessCriteria.indexOf(current_status) !== -1) {
				return true;
			}
			return false;
		};
		/**
		 * Filter criteria
		 * This will filter result in repeater in active items
		 * Server side manipulations 
		 * 
		 * @param {Object} asset
		 * @returns {Boolean}
		 */
		$scope.filterOnProccessing = function (asset) {
			var current_status = parseInt(asset.conversion_status);
			if ($scope.activeProccessCriteria.indexOf(current_status) !== -1) {
				return true;
			}
			return false;
		};

		/**
		 * Find key of object 
		 * @param {type} assetId
		 * @returns {Number|Boolean}
		 */
		$scope.findAsset = function (assetId) {
			var id = parseInt(assetId);
			var indexTmp = false;
			$.each($scope.assets, function (index, asset) {
				if (id === parseInt(asset.id)) {
					indexTmp = index;
					return false;
				}
			});
			return indexTmp;
		};


		/**
		 * Change the status into progressing DOM
		 * @param {Number} assetId
		 * @param {Number} conversionStatus
		 * @returns {controllerAsset_L1.Asset.$scope}
		 */
		$scope.changeConversionStatus = function (assetId, conversionStatus) {
			this.init = function (assetId, conversionStatus) {
				this.index = false;
				if (this.index === false) {
					this.index = $scope.findAsset(assetId);
				}
				//start diung if we have any index of object scope
				if (this.index !== false) {
					$scope.assets[this.index]['conversion_status'] = conversionStatus;

				}
				//force update status into db if have any error with file
				if ([7, 8, 9].indexOf($scope.assets[this.index]['conversion_status']) > -1) {
					if ($scope.getHasRequest() > 0) {
						$scope.showUploadForm = true;
					}
					new Request.ajax($scope.axUrl + '/axAssetChangeStatusError', {assetId: assetId}, this._axSuccess, $scope.errorUploader);
				}
				$scope.checkListsCounts();
				$scope.$apply();
			};

			this._axSuccess = function (object) {
				;
			};

			return this.init(assetId, conversionStatus);
		};
		$scope.changeAssetThumb = function (assetId, assetThumb) {
			this.index = false;
			if (this.index === false) {
				this.index = $scope.findAsset(assetId);
			}
			//start diung if we have any index of object scope
			if (this.index !== false) {
				$scope.assets[this.index]['thumb'] = assetThumb;
			}
			$scope.$apply();

		};

		/**
		 * Event with doing on file Added 
		 * @param {Uploader.plUploader} up
		 * @param {Array} files
		 * @returns {controllerAsset_L1.Asset.$scope.filesAdded}
		 */
		$scope.filesAdded = function (up, files) {

			/**
			 * What will be successd procedure after ajax call
			 * @param {Object} object
			 * @returns {controllerAsset_L1.Asset.$scope.filesAdded|Boolean}
			 */
			this._axSuccess = function (object) {
				if (object.result) {
					/* Set AUTO TABS TRIGGER again to ON */
					AUTOMATIC_TABS_TRIGGER = true;
					//proccess new one item in queue;
					$.each(object.assets, function (index, item) {
						//attach to file array id from DB, then append to general scope
						$.each(files, function (index_j, file) {
							if (item.asset.filename.toLowerCase().indexOf(file.id) > -1) {
								files[index_j].assetId = item.asset.id;
								files[index_j].key = item.asset.outputPath;

//								up.setOption('assetId', item.asset.id);
//								up.setOption('key', item.asset.outputPath);
								item.asset['percent'] = 0;
								$scope.assets.push(item.asset);
								$scope.checkListsCounts();
								$scope.cancelUploadEvent(up, file, item.asset['id']);

								return;
							}
						});
					});
					$scope.$digest();
					$scope.app7020Uploader.refresh();
					setTimeout(function () {
						$scope.uploader.start();
					}, 300);
					if ($scope.getHasRequest() > 0) {
						$scope.showUploadForm = false;
					}
					$scope.fileAddedProccessFinished = 1;
					$scope.unloadActions();
					return this;
				} else {
					return false;
				}
			};
			/**
			 * Init of procedure Added File on queue
			 * @param {Uploader.plUploader} up
			 * @param {Array} files
			 * @returns {controllerAsset_L1.Asset.$scope.filesAdded}
			 */
			this.init = function (up, files) {
				new Request.ajax($scope.axUrl + '/axAddAsset', {data: JSON.stringify(files)}, this._axSuccess, $scope.errorUploader);
				return this;
			};
			return this.init(up, files);
		};
		/**
		 * Before upload event and all actions 
		 * 
		 * @param {Uploader.plUploader} up
		 * @param {Uploader.plUploader.oFile} file
		 * @returns {controllerAsset_L1.Asset.$scope.beforeUpload}
		 */
		$scope.beforeUpload = function (up, file) {
			/**
			 * Invoke construct for before upload event
			 * @param {Uploader.plUploader} up
			 * @param {Uploader.plUploader.oFile} file
			 * @returns {controllerAsset_L1.Asset.$scope.beforeUpload}
			 */

			this.init = function (up, file) {
				var that = this;
				up.settings.multipart_params.key = file.key;
//				setTimeout(function () {
				var objectIndex = findAssetPerFileName(file.id);
				var assetId = $scope.assets[objectIndex].id;

				new Request.ajax($scope.axUrl + '/axStartUploadAsset', {assetId: assetId}, that._axSuccess, $scope.errorUploader);
				return this;
//				}, ($scope.delay + 500));

			};
			/**
			 * 
			 * @param {Object} object
			 * @returns {controllerAsset_L1.Asset.$scope.beforeUpload}
			 */
			this._axSuccess = function (object) {
				return $scope.changeConversionStatus(file.assetId, 1);
			};
			return this.init(up, file);
		};


		/**
		 * Server side erros will be throw here
		 * @param {type} xhr
		 * @param {Number} status
		 * @param {String} errorThrown
		 * @returns {controllerAsset_L1.Asset.$scope}
		 */
		$scope.errorUploader = function (xhr, status, errorThrown) {
			//$scope.changeConversionStatus(file.assetId, CONVERSION_STATUS_SNS_WARNING);
			if(angular.isNumber(status)){
				throw errorThrown + ' error code[' + status + ']';
			}
			return this;
		};

		$scope.chunkUploaded = function (up, file, info) {
			Docebo.log('Chunk has been uploaded');
		};


		/**
		 * All actions in Proccessing fired evenet
		 * 
		 * @param {Uploader.plUpload} up
		 * @param {Uploader.plUpload.oFile} file
		 * @returns {controllerAsset_L1.Asset.$scope.uploadProgress}
		 */
		$scope.uploadProgress = function (up, file) {

			/**
			 * Store the working DOM from queue 
			 */
			this.index = false;

			/**
			 * Construct the event Proccessing
			 * @param {Uploader.plUpload} up
			 * @param {ploader.plUpload.oFile} file
			 * @returns {controllerAsset_L1.Asset.$scope.uploadProgress}
			 */
			this.init = function (up, file) {
				if (this.index === false) {
					this.index = $scope.findAsset(file.assetId);
				}
				$scope.assets[this.index]['percent'] = file.percent;
				$scope.$digest();
				return this;
			};

			return this.init(up, file);
		};

		/**
		 * All events when file is uploaded
		 * @param {type} up
		 * @param {type} file
		 * @param {type} info
		 * @returns {undefined}
		 */
		$scope.fileUploaded = function (up, file, info) {



			this._axSuccess = function (object) {
				if (object.result) {
					realTime.getInstance().restart();
					$scope.changeConversionStatus(file.assetId, CONVERSION_STATUS_S3);

				} else {
					$scope.changeConversionStatus(file.assetId, CONVERSION_STATUS_SNS_ERROR);
				}
			};

			/**
			 * Mark as error corrupted file
			 * @param {type} xhr
			 * @param {type} status
			 * @param {type} errorThrown
			 * @returns {controllerAsset_L1.Asset.$scope.fileUploaded}
			 */
			this._axError = function (xhr, status, errorThrown) {
				if (status !== 200) {
					$scope.changeConversionStatus(file.assetId, CONVERSION_STATUS_SNS_WARNING);
				}
				return this;
			};
			this.init = function () {
				var that = this;
//				setTimeout(function () {
				new Request.ajax($scope.axUrl + '/axAssetUploaded', {assetId: file.assetId}, that._axSuccess, that._axError);
//				}, $scope.delay);

				return  this;
			};

			return this.init(up, file, info);
		};


		$scope.error = function (up, e) {
			this.error = {
				code: '',
				message: '',
				file: '',
				response: '',
				status: '',
				responseHeaders: ''

			};
			this.error = e;
			return $scope.changeConversionStatus(e.file.assetId, CONVERSION_STATUS_SNS_ERROR);
		};

		/* Disable AUTOMATIC_TABS_TRIGGER IF manual switch tabs */
		$('#contributeMasterContainer .app7020BootstrapTabsExtended li a').click(function () {
			AUTOMATIC_TABS_TRIGGER = false;
		});

		/* Init on Bootstrap tooltip */
		$('#contributeMasterContainer .assetActiveStatusesContainer .controllIcon i').tooltip({
			trigger: 'hover',
			placament: 'top'
		});

		$scope.deleteCustomAsset = function ($event) {
			var parent = $($event.target).closest('.assetActiveStatusesContainer');
			var assetID = parent.data('asset-id');
			$scope.deleteAsset(assetID);
		};

		/* When Modal is open attach Some Listeners */
		var dialogBaseClass = '.app7020AssetModal';
		angular.element(document).on('dialog2.content-update', '#app7020-asset-modal-dialog', function () {
			var dialog = $(this);
			/* Close Button */
			dialog.find(dialogBaseClass + ' .footerButtons .cancel').click(function () {
				dialog.dialog2('close');
			});
			/* ForReview Button */
			dialog.find(dialogBaseClass + ' .footerButtons .SubmitForReview').click(function () {
				$scope.submitAsset(10, $(this));
			});
			/* Publish Button */
			dialog.find(dialogBaseClass + ' .footerButtons .publishNow').on('click', function () {
				$(this).off('click');
				$scope.submitAsset(20, $(this));
			});
			/* Delete Button */
			dialog.find(dialogBaseClass + ' .footerButtons .deleteAsset').click(function () {
				$scope.deleteAsset($(this));
			});
			/* Cancel Button */
			dialog.find(dialogBaseClass + ' .footerButtons .cancelUpload').click(function () {
				$scope.canceUpload($(this).closest('.app7020AssetModal').attr('data-asset-id'));
				dialog.closest('#app7020-asset-modal-dialog').dialog2('close');
			});

			if ($('#App7020Assets_description').length > 0) {
				app7020.tinyMCE.removeEditors('App7020Assets_description');
				new app7020.editor({textarea: 'App7020Assets_description'});
			}

//			dialog.find('table[id^=treeWidgetContainer]').app7020FancytreePlugin();
//		/	dialog.find('.tags #tags').app7020TagsPlugin();
		});

		angular.element(document).on('click', '#confirmInviteToWatchContainer .save-dialog', function () {
			idContent = $(this).closest("#confirmInviteToWatchContainer").attr('data-idContent');
			$('#app7020-confirm-invite-modal-dialog').dialog2("close");
			var url = app7020Options.inviteToWatch + '/index&id=' + idContent;
			var id = 'app7020-invite-dialog';
			var clases = 'app7020-invite-dialog wide';
			var data = {};
			var title = Yii.t('app7020', 'Invite to Watch');
			openDialog(false, title, url, id, clases, 'GET', data);
//			app7020.inviteUsers.openDialog(idContent);
		});

		/**
		 * Delete button inside asset modal for delete local and s3 asset + record from DB
		 * @param {type} dialog
		 * @returns {controllerAsset_L1.Asset.$scope.axAssetDelete}
		 * Update $scope after asset be removed
		 */


		var selected = [];
		var tempidarr = [];




		$scope.deleteAsset = function (assetId) {
			var that = this;
			this.openConfirmDialog = function (assetId) {
				openDialog(
						false,
						Yii.t('app7020', 'Confirm Delete'),
						app7020Options.axAssetsController + '/axShowConfirmDeleteAssetModal',
						'app7020-confirm-delete-modal-dialog',
						'app7020-confirm-delete-modal-dialog',
						'POST',
						{assetId: assetId}
				);

			};
			/* Change Z-index when try to open modal for confirm delete */
			$(document).one('dialog2.before-open', '#app7020-confirm-delete-modal-dialog', function () {
				$(this).closest('.modal').prev('.modal-backdrop').css({'z-index': '1050'});
				$(this).closest('.modal').unbind('click').on('click', 'a.save-dialog', function (e) {
					e.preventDefault();
					if ($(this).closest('.modal').find('#confirm[type="checkbox"]').is(':checked')) {
						that.axAssetDelete($(this).closest('.modal').find('#confirmDeleteAssetDialog').attr('data-content-id'));
						$('#app7020-confirm-delete-modal-dialog').dialog2('close');
					}
				});
			});
            $(document).on('dialog2.content-update', '#app7020-confirm-delete-modal-dialog', function () {
                $(this).closest('.modal').find('.save-dialog').addClass('disabled');

                //console.log($(document).find('#confirm[type="checkbox"]'));

			});
            $(document).on('click', '.app7020-confirm-delete-modal-dialog [type="checkbox"]', function(){
                if($(this).prop('checked')){
                    $(document).find('.app7020-confirm-delete-modal-dialog .btn.save-dialog').removeClass('disabled');
                }else{
                    $(document).find('.app7020-confirm-delete-modal-dialog .btn.save-dialog').addClass('disabled');
                }
            });
			this._axSuccess = function (object) {
				if (object.status === true) {
					var index = findAssetPerAssetId(assetId);
					$scope.showUploadForm = true;
					$scope.assets.splice(index, 1);
					$scope.checkListsCounts();
					$scope.$digest();
				}
			};
			this._axError = function (object) {
				$scope.changeConversionStatus(assetId, CONVERSION_STATUS_SNS_ERROR);
			};

			this.axAssetDelete = function (id) {
				var data = {
					local: false,
					id: id
				};
				new Request.ajax($scope.axUrl + '/AxDeleteFile', data, this._axSuccess, this._axError);
			};
			return this.openConfirmDialog(assetId);
		};




		/**
		 * Submit metaform for BL proccerssing
		 * 
		 * @param {Number} status Temp status
		 * @param {String} buttonId
		 * @returns {controllerAsset_L1.Asset.$scope.submitAsset}
		 */
		$scope.submitAsset = function (status, buttonId) {

			/**
			 * Global store of asset id for this action
			 * @var {Number}
			 */
			this.assetId = false;
			/**
			 * Submit metaform for BL proccerssing
			 * 
			 * @param {Number} status
			 * @param {String} buttonId
			 * @returns {controllerAsset_L1.Asset.$scope.submitAsset}
			 */
			this.init = function (status, buttonId) {
				var dialog = $('#app7020-asset-modal-dialog');
				var id = dialog.find(dialogBaseClass).attr("data-asset-id");
				this.assetID = parseInt(id);
//				var topics = $scope.getSelectedTopics(buttonId, dialogBaseClass);
				var assetData = $('.app7020AssetModal > form:first-child').serializeArray();
//				assetData.push({'name': 'topics', 'value': topics});
				assetData.push({'name': 'assetId', 'value': id});
				assetData.push({'name': 'status', 'value': status});
				new Request.ajax($scope.axUrl + '/axSubmitAsset', assetData, this._axSuccess, $scope.errorUploader);
				return this;
			};
			/**
			 * What must be du after ajax request
			 * @param {Object} object
			 * @returns {controllerAsset_L1.Asset.$scope.submitAsset}
			 */
			this._axSuccess = function (object) {
				var dialog = $('#app7020-asset-modal-dialog');
				if (!object.res) {
					$('#app7020-asset-modal-dialog #error-container').doceboNotice(object.errors, 'error');

					var objectAssetsIndex = findAssetPerAssetId(objectAssetsIndex);
					if (objectAssetsIndex) {
						$scope.assets[objectAssetsIndex].conversion_status = object.assetStatus;
					}
					/* Publish Button */
					dialog.find(dialogBaseClass + ' .footerButtons .publishNow').on('click', function () {
						$(this).off('click');
						$scope.submitAsset(20, $(this));
					});
				}
				if (object.res) {

					$('#app7020-asset-modal-dialog').dialog2('close');
					//TODO update tabs if object.assetStatus == 10 or object.assetStatus == 20
					//you can use also object.assetId and object.assetOriginalName
					var assetIndex = findAssetPerAssetId(parseInt(object.assetId));
					if (parseInt($scope.assets[assetIndex].conversion_status) === CONVERSION_STATUS_SNS_SUCCESS) {
						$scope.changeConversionStatus(object.assetId, parseInt(object.assetStatus));
						$scope.changeAssetThumb(object.assetId, object.assetThumb);
					}
					if (status == CONVERSION_STATUS_APPROVED) {
						openDialog(
								false,
								Yii.t('app7020', 'Published!'),
								app7020Options.axAssetsController + '/axShowConfirmInviteModal',
								'app7020-confirm-invite-modal-dialog',
								'app7020-confirm-invite-modal-dialog',
								'POST',
								{assetId: object.assetId}
						);
					}
				}
				return this;
			};
			return this.init(status, buttonId);
		};

		/**
		 * get All assets
		 * @returns {Array|controllerAsset_L1.Asset.$scope.assets}
		 */
		$scope.getAssets = function () {
			return $scope.assets;
		};

		/**
		 * 
		 * @param {Object} response
		 * @returns {undefined}
		 */
		$scope.onServerAnswer = function (response) {
			if (response.status === 200) {
				var data = response.data;
				if (typeof (data.activeAssets) === 'undefined') {
					console.log('SERVER ERROR[  statusText: Missing value from server response ]');
				}
				data.activeAssets.forEach(function (value, index) {
					var objectAssetsID = findAssetPerAssetId(value.assetID);
					console.log(value);
					realTimeProccessingAssets(value, objectAssetsID);
				});
			} else {
				throw 'SERVER ERROR[ status: ' + response.status + '; statusText: ' + response.status + ' ]';
			}
		}

		$scope.addLinkKeypress = function ($event) {
			if ($event.keyCode == 13) {
				$scope.addLink();
			}
		}

		/**
		 * 
		 * @param {Object} response
		 * @returns {undefined}
		 */
		$scope.addLink = function (sharedLink) {
			console.log(sharedLink);
			/**
			 *Successed procedure after ajax call
			 * @param {Object} object
			 * @returns {controllerAsset_L1.Asset.$scope.filesAdded|Boolean}
			 */
			$("#sharedLink").blur();
			$scope.showShareButton = false;
			$("#sharedLink").attr("placeholder", Yii.t('app7020', 'Please wait for the uploading of the asset') + '...');
			this._axSuccess = function (object) {
				if (object.result) {
					//proccess new link;
					$.each(object.assets, function (index, item) {
						//attach to file array id from DB, then append to general scope
						item.asset['percent'] = 0;
						$scope.assets.push(item.asset);
						$scope.checkListsCounts();
						$scope.$digest();
					});
					realTime.getInstance().restart();

					if ($scope.getHasRequest() > 0) {
						$scope.showUploadForm = false;
					}
					return this;
				} else {
                    $scope.showShareButton = true;
                    $scope.$digest();
// 					Docebo.Feedback.show('error', 'Link is a not valid url!', true, true, false);
					$('#contributeMasterContainer').doceboNotice(Yii.t('app7020', 'Link is not a valid url!'), 'error');
					return false;
				}
			};
			/**
			 * Init of procedure Added File on queue
			 * @param {Uploader.plUploader} up
			 * @param {Array} files
			 * @returns {controllerAsset_L1.Asset.$scope.filesAdded}
			 */
			this.init = function (sharedLink) {
				var link = (sharedLink) ? sharedLink : $('#sharedLink').val();
				$('#sharedLink').val('');
				new Request.ajax($scope.axUrl + '/axAddLink', {data: link}, this._axSuccess, $scope.errorUploader);
				return this;
			};
			return this.init(sharedLink);
		};
		/**
		 * 
		 * @param {Object} response
		 * @returns {undefined}
		 */
		$scope.addGoogleDrive = function (file) {
 			/**
			 *Successed procedure after ajax call
			 * @param {Object} object
			 * @returns {controllerAsset_L1.Asset.$scope.filesAdded|Boolean}
			 */
			this._axSuccess = function (object) {
				if (object) {

					//proccess new link;
					$.each(object.assets, function (index, item) {
						if(item.error) {
							$('#contributeMasterContainer').doceboNotice(item.error, 'error');
							return false;
						}else{
							//attach to file array id from DB, then append to general scope
							item.asset['percent'] = 100;
							$scope.assets.push(item.asset);
							$scope.checkListsCounts();
							$scope.$digest();
							if (object.assets.length === 1 && howManyInActiveListHas() === 1) {
								$scope.openMetaformDialogModal(item.asset.id);
							}
						}
					});
					realTime.getInstance().restart();

					if ($scope.getHasRequest() > 0) {
						$scope.showUploadForm = false;
					}
					return this;
				} else {
// 					Docebo.Feedback.show('error', 'Link is a not valid url!', true, true, false);
					$('#contributeMasterContainer').doceboNotice(Yii.t('app7020', 'Link is not a valid url!'), 'error');
					return false;
				}
			};
			/**
			 * Init of procedure Added File on queue
			 * @param {Uploader.plUploader} up
			 * @param {Array} files
			 * @returns {controllerAsset_L1.Asset.$scope.filesAdded}
			 */
			this.init = function (file) {
				new Request.ajax($scope.axUrl + '/axAddGoogleDrive', {data: file}, this._axSuccess, $scope.errorUploader);
				return this;
			};
			return this.init(file);
		};

		/**
		 * Set value for exist request
		 * @param {Boolean|Number} param
		 * @returns {Boolean}
		 */
		$scope.setHasRequest = function (param) {
			$scope.hasRequest = param;
		};

		/**
		 * Get defined value for existing request
		 * @returns {Boolean}
		 */
		$scope.getHasRequest = function () {
			return $scope.hasRequest;
		};

		// Callback triggered after Picker is shown
		$scope.onLoaded = function () {
			console.log('Google Picker loaded!');
		}

		// Callback triggered after selecting files
		$scope.onPicked = function (docs) {
			angular.forEach(docs, function (file, index) {
				$scope.addGoogleDrive(file);
			});
		}

		// Callback triggered after clicking on cancel
		$scope.onCancel = function () {
			console.log('Google picker close/cancel!');
		}

		// Define the locale to use
		$scope.changeLocale = function (locale) {
			lkGoogleSettings.locale = locale.code;
		};
		/**
		 * 
		 * @param {Object} responseObject
		 * @param {Number} assetIndex
		 * @returns {controllerAsset_L1.Asset}
		 */
		function realTimeProccessingAssets(responseObject, assetIndex) {
			if (typeof ($scope.assets[assetIndex]) !== 'undefined' && parseInt($scope.assets[assetIndex].conversion_status) !== parseInt(responseObject.conversion_status)) {
				$scope.assets[assetIndex].conversion_status = responseObject.conversion_status;

				$scope.checkListsCounts();
				if (parseInt(responseObject.conversion_status) === CONVERSION_STATUS_SNS_SUCCESS && hasProccessedFiles() === false) {
					$(window).unbind('beforeunload');
					realTime.getInstance().stop();
				}
				console.log('sfsafsd->' + parseInt($scope.assets[assetIndex].contentType));
				if (parseInt($scope.assets[assetIndex].contentType) === 15 && parseInt($scope.assets[assetIndex].conversion_status) === CONVERSION_STATUS_SNS_SUCCESS) {
					$scope.openMetaformDialogModal($scope.assets[assetIndex].id);
					$('#sharedLink').val('');
					$("#sharedLink").attr("placeholder", Yii.t('app7020', 'Paste here the URL you want to share') + '...');
					$(".docebo-alert").hide();
				}

				else {
					if (howManyInActiveListHas() === 1) {
						$timeout(function () {
							$scope.openMetaformDialogModal($scope.assets[assetIndex]['id']);

						});
					}
				}
			}
			return this;
		}

		/**
		 * 
		 */
		$scope.selectePrChannels = new Array();

		/**
		 * Fire open modal of meta data form
		 * @param {Number} assetId
		 * @returns {void}
		 */
		$scope.openMetaformDialogModal = function (assetId) {
			/* Open Asset Modal for specific assetId */
			$scope.showShareButton = true;
			openDialog(
					false,
					Yii.t('app7020', 'Finalize publishing'),
					app7020Options.axAssetsController + '/GetAssetModal',
					'app7020-asset-modal-dialog',
					'app7020-asset-modal-dialog',
					'POST',
					{assetId: assetId}
			);

			/**
			 * 
			 */
			$(document).on('dialog2.content-update', '#app7020-asset-modal-dialog', function () {
				$(".app7020-asset-modal-dialog").each(function () {
					var content = $(this);
					angular.element(document).injector().invoke(function ($compile) {
						var scope = angular.element(content).scope();
						$compile(content)(scope);
					});
				});
			});



			$scope.selectePrChannels = new Array();
			$(document).on('change', '.checkbox-item input.is_pr', function () {
				$scope.reverseThePublishButtons();
				if ($(this).is(':checked')) {
					$scope.selectePrChannels.push($(this).val());
					$scope.showGotItMessage();
					$scope.reverseThePublishButtons();
				} else {
					$scope.hideGotItMessage();
					if ($scope.selectePrChannels.indexOf($(this).val()) > -1) {
						$scope.selectePrChannels.splice($scope.selectePrChannels.indexOf($(this).val()), 1);
						$scope.reverseThePublishButtons();
					}

				}
			});

			$(document).on('change', 'input[name="App7020Assets[is_private]"]', function () {
				var vis_val = parseInt($(this).val());
				if (vis_val == 0) {
					$('[data-custom="channelWidgetContainer"]').css('display', 'block');
				} else {
					$('[data-custom="channelWidgetContainer"]').css('display', 'none');
				}
			});

		};

		/**
		 * If exist some got it messages this method should display  it 
		 * @returns {undefined}
		 */
		$scope.showGotItMessage = function () {
			$('#gotItChannelFeatures').css('display', 'block');
		};
		/**
		 * Hide the notice during someone use pr channel 
		 * @returns {undefined}
		 */
		$scope.hideGotItMessage = function () {
			$('#gotItChannelFeatures').css('display', 'none');
		};

		$scope.reverseThePublishButtons = function () {
			if (existPrChannel()) {
				$('.publishNow').css('display', 'none');
				$('.SubmitForReview').css('display', 'block');
			} else {
				$('.publishNow').css('display', 'block');
				$('.SubmitForReview').css('display', 'none');
			}
		};

		/**
		 * Return the Key of PR channel if exist or Boolean false if he doesn't
		 * @param {Number} $val
		 * @returns {Boolean|Number}
		 */
		function existPrChannel() {
			if ($scope.selectePrChannels.length > 0) {
				return true;
			} else {
				return false;
			}
		}


		/**
		 * Find the key from general scope.assets array
		 * @param {Number} assetID
		 * @returns {Number|Boolean}
		 */
		function findAssetPerAssetId(assetID) {
			var id = parseInt(assetID);
			var obejctAssetID = false;
			$scope.assets.forEach(function (object, index) {

				if (parseInt(object.id) === id) {
					obejctAssetID = index;
					return obejctAssetID;
				}
			});
			return obejctAssetID;
		}

		/**
		 * Find Asset Array index per file name of asset
		 * @param {String} name
		 * @returns {Number|Boolean}
		 */
		function findAssetPerFileName(name) {
			var obejctAssetID = false;
			$scope.assets.forEach(function (object, index) {
				if (object.filename.toLowerCase().indexOf(name) > -1) {
					obejctAssetID = index;
					return true;
				}
			});
			return obejctAssetID;
		}

		/**
		 * Is valide rule for unbind UNLOAD event
		 * @returns {Boolean}
		 */
		function hasProccessedFiles() {
			var result = false;
			$scope.assets.forEach(function (object, index) {
				if (
						parseInt($scope.assets[index].conversion_status) === parseInt(CONVERSION_STATUS_PENDING) ||
						parseInt($scope.assets[index].conversion_status) === parseInt(CONVERSION_STATUS_INPROGRESS) ||
						parseInt($scope.assets[index].conversion_status) === parseInt(CONVERSION_STATUS_S3)
						)
				{
					result = true;
					return;
				}
			});

			return result;
		}

		function howManyInActiveListHas() {
			var result = 0;
			$scope.assets.forEach(function (object, index) {
				if (
						parseInt($scope.assets[index].conversion_status) === parseInt(CONVERSION_STATUS_PENDING) ||
						parseInt($scope.assets[index].conversion_status) === parseInt(CONVERSION_STATUS_INPROGRESS) ||
						parseInt($scope.assets[index].conversion_status) === parseInt(CONVERSION_STATUS_SNS_SUCCESS) ||
						parseInt($scope.assets[index].conversion_status) === parseInt(CONVERSION_STATUS_S3)
						)
				{
					result++;
				}
			});

			return result;
		}

		/**
		 * Remove failed files
		 * @returns {Boolean}
		 */
		function removeFailedFiles(assets) {
			var tmp_array = [];
			assets.forEach(function (object, index) {
				if (
						parseInt(object.conversion_status) !== parseInt(CONVERSION_STATUS_PENDING) &&
						parseInt(object.conversion_status) !== parseInt(CONVERSION_STATUS_INPROGRESS)
						)
				{
					tmp_array.push(object);
				}

			});
			return tmp_array;
		}
	}


	Asset.$inject = ['$scope', 'Uploader', 'Request', 'RealTime', 'WSMessageAsset', '$timeout'];
	return Asset;
});
 