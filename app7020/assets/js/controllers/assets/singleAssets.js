
define([
	'modules/fancyScroller',
	'modules/notification',
	'modules/appendCSS',
	'modules/request',
],
		function (fancyScrollerLib, n, appendCSS, request) {
			new appendCSS('/themes/spt/css/jquery.mCustomScrollbar.css');
			
			// Scroll to top functionality
			$(function ($) {
				$(document).scroll(function () {
					if ($(this).height() - ($(this).scrollTop() + $(window).height()) < 100) {
						$('#knowledgeLibrarySingle .go-to-top').css({
							display: 'block'
						});
					} else {
						$('#knowledgeLibrarySingle .go-to-top').css({
							display: 'none'
						});
					}
				});
				
				$(document).on('click', '.go-to-top', function(){
					$("html, body").animate({
						scrollTop: 0 
					}, 500);
				});
			});



			//append channels scroller
			$(function ($) {
				$('#channnelsCheckList').lookingfor({
					input: $('input.channelWidgetSearch'),
					items: 'div'
				});
				var fancyScroller = new fancyScrollerLib();
				fancyScroller.init($('#channnelsCheckList'), 0);
			});

			//append editor for description
			if ($('#App7020Assets_description').length > 0) {
				app7020.tinyMCE.removeEditors('App7020Assets_description');
				new app7020.editor({textarea: 'App7020Assets_description'});
			}


			var publishController = '.readToPublish';
			var publishUrl = app7020Options.peerReviewControllerPublish;
			var unpublishUrl = app7020Options.peerReviewControllerUnPublish;
			
			var singleAssets = function ($scope, AJAX, GetDataService) {

				$scope.head = [];

				
				/**
				 * Open invite modal
				 * @param {Number} $assetId
				 * @returns {singleAssets_L1.singleAssets.$scope}
				 */
				$scope.openInviteToWatchModal = function ($assetId) {
					var url = app7020Options.inviteToWatch + '/index&id=' + $assetId;
					var id = 'app7020-invite-dialog';
					var clases = 'app7020-invite-dialog wide';
					var data = {};
					var title = Yii.t('app7020', 'Invite to Watch');
					openDialog(false, title, url, id, clases, 'GET', data);
					return this;
				};
				$scope.submitAsset = function () {
					/**
					 * Global store of asset id for this action
					 * @var {Number}
					 */
					this.assetId = false;
					/**
					 * Submit metaform for BL proccerssing
					 * 
					 * @param {Number} status
					 * @param {String} buttonId
					 * @returns {controllerAsset_L1.Asset.$scope.submitAsset}
					 */
					this.init = function () {
						var assetData = $('.app7020AssetModal  form').serializeArray();
						assetData.push({name: 'assetId', value: parseInt($('.app7020AssetModal').data('asset-id'))});
						request(this).ajax(app7020Options.assetsAxController + '/axSubmitAsset', assetData, this._axSuccess, this.error);
						return this;
					};

					/**
					 * 
					 * @param {type} object
					 * @returns {singleAssets_L21.singleAssets.$scope.submitAsset}
					 */
					this._axSuccess = function (object) {
						if (!object.res) {
							$('#knowledgeLibrarySingle').doceboNotice(object.errors, 'error');
						}
						if (object.res) {
							$('#knowledgeLibrarySingle').doceboNotice(object.message, 'success');
						}
						return this;
					};
					/**
					 * 
					 * @param {type} object
					 * @returns {singleAssets_L21.singleAssets.$scope.submitAsset}
					 */
					this._error = function (xhr, status, errorThrown) {
						console.log('error', xhr, status, errorThrown);
						$('#app7020-asset-modal-dialog #error-container').doceboNotice(errorThrown, 'error');
						return this;
					};

					return this.init();
				};


				/* use this to show/hide some partials from question template */
				$scope.questionAllow = {
					footer: [
						'author',
						'views',
						'time',
						'answers'
					]
				};

				$scope.head = GetDataService.getAllQuestions('head');
				$scope.items = GetDataService.getAllQuestions('items');
				$scope.busyInfinite = false;
				$scope.showRelated = false;
				$scope.init = function () {
					if (!$scope.items.length) {
						GetDataService.getData({
							endpoint: GetDataService.ENUM_ENDPOINT.ALL_QUESTIONS,
							idAsset: parseInt($('#knowledgeLibrarySingle').data('asset-id')),
							successCallback: function (data) {
								$scope.head = GetDataService.getAllQuestions('head');
								$scope.items = GetDataService.getAllQuestions('items');
							}
						});
					}
				};



				$scope.runInfinite = function () {
					if (!$scope.items.length || $scope.head.questions_count == $scope.items.length)
						return false;

					var filters = [];

					if ($scope.selectedfilters !== 'undefined') {
						filters = $scope.selectedfilters;
					}

					$scope.busyInfinite = true;
					GetDataService.getData({
						endpoint: GetDataService.ENUM_ENDPOINT.ALL_QUESTIONS,
						from: $scope.items.length,
						count: 5,
						options: JSON.stringify(filters),
						idAsset: parseInt($('#knowledgeLibrarySingle').data('asset-id')),
						successCallback: function () {
							$scope.busyInfinite = false;
						}
					});
				};

			};
			singleAssets.$inject = ['$scope', 'AJAX', 'GetDataService']
			return singleAssets;
		});