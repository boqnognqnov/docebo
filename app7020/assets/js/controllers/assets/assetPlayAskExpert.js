define(['modules/notification'],function () {
	function AllQuestionsController($scope, GetDataService, $timeout, $routeParams) {

		/* use this to show/hide some partials from question template */
		$scope.questionAllow = {
			footer: [
				'author',
				'views',
				'time',
				'answers'
			]
		};
		/* Stored data from service */
//		$scope.head = [];
//		$scope.items = [];
 
		
//		$scope.$watch(function(){
//			return $scope.$parent.dataIsLoaded;
//		}, function(v){
//			if(v){
//				var questions = angular.copy($scope.$parent.questions);
//				$scope.items = questions.items;
//				delete questions.items;
//				$scope.head = questions;
//			}
//		});

		/* Stored data from service */
		$scope.head = GetDataService.getAllQuestions('head');
		$scope.items = GetDataService.getAllQuestions('items');
		$scope.questionsLoaded = false;
		$scope.init = function () {
 				GetDataService.getData({
					endpoint: GetDataService.ENUM_ENDPOINT.ALL_QUESTIONS,
					idAsset: $routeParams.id,
					successCallback: function (data) {
						$scope.head = GetDataService.getAllQuestions('head');
						$scope.items = GetDataService.getAllQuestions('items');
						$scope.questionsLoaded = true;
					}
				});
 		}

		$scope.busyInfinite = false;


		$scope.runInfinite = function () {
			if (!$scope.items.length || $scope.head.questions_count == $scope.items.length)
				return false;

			var filters = [];

			if ($scope.selectedfilters !== 'undefined') {
				filters = $scope.selectedfilters;
			}

			$scope.busyInfinite = true;
			GetDataService.getData({
				endpoint: GetDataService.ENUM_ENDPOINT.ALL_QUESTIONS,
				from: $scope.items.length,
				count: 5,
				options: JSON.stringify(filters),
				successCallback: function () {
					$timeout(function () {
						$scope.busyInfinite = false;
					}, 1000);
				}
			});
		}

	}

	AllQuestionsController.$inject = ['$scope', 'GetDataService', '$timeout', '$routeParams'];
	return AllQuestionsController;
});