define([
	'modules/fancyScroller',
	'modules/notification',
	'modules/appendCSS',
	'modules/request',
],
		function (fancyScrollerLib, n, appendCSS, request) {

			new appendCSS('/themes/spt/css/jquery.mCustomScrollbar.css');
			var input = {
				year: 0,
				month: 0,
				day: 0,
				hours: 0,
				minutes: 0,
				seconds: 0
			};


			var interval = 1;
			var assetData = false;
			var realTime = false;
			var Controller = function ($rootScope, $scope, $timeout, poller, $routeParams, AJAX, GetDataService, RealTime, WSMessageAsset, $sce) {
				$scope.to_trusted = function (html_code) {
					return $sce.trustAsHtml(html_code);
				}

				$scope.timer = null;
				$scope.spendTimeSecond = 0;
				$scope.startingSpendUpdate = false;
				$scope.error = false;
				/**
				 *
				 * @returns {undefined}
				 */
				$scope.startCountingSpendTime = function () {
					console.log('INIT::startCountingSpendTime');
					$scope.timer = setInterval(function () {
						$scope.spendTimeSecond++;
						$scope.$digest();
					}, Math.abs(interval) * 1000);
				};
				/**
				 *
				 * @returns {undefined}
				 */
				$scope.stopCountingSpendTime = function () {
					console.log('INIT::stopCountingSpendTime');
					if (!$scope.startingSpendUpdate && $scope.spendTimeSecond > 0) {
						$scope.updateSpendTime($scope.spendTimeSecond);
					}
					$scope.spendTimeSecond = 0;
					clearTimeout($scope.timer);
					$scope.timer = null;
				};

				/**
				 *
				 * @param {type} $second
				 * @returns {undefined}
				 */
				$scope.updateSpendTime = function ($second) {
					console.log('INIT::updateSpendTime');
					AJAX.post('/lms/index.php?r=restdata', {
						endpoint: 'spent_time',
						params: {
							id_asset: $routeParams.id,
							time: $second
						}
					}, function () {
						clearInterval($scope.timer);
						$scope.spendTimeSecond = 0;
						$scope.startingSpendUpdate = false;
					}, function () {

					});
				};


				/**
				 *
				 */
				$scope.$watch('spendTimeSecond', function (newest, older) {
					// console.log('spend time -> ', $scope.spendTimeSecond);
					if (parseInt(newest) % 30 === 0 && $scope.startingSpendUpdate === false && $scope.spendTimeSecond > 0) {
						$scope.updateSpendTime(newest);
						$scope.startCountingSpendTime();
					}
				});

				/**
				 * Stop counter on call to action behaviour
				 */
				$scope.$on('spend.stop', function () {
					$scope.stopCountingSpendTime();
				});
				/**
				 * Resume the counter
				 */
				$scope.$on('spend.resume', function () {
					$scope.startCountingSpendTime();
				});
				/**
				 * Resume the counter on maximize window  with focused tab
				 */
				$scope.$on('spend.mindow.maximize', function () {
					console.log('spend.resume');
					$scope.startCountingSpendTime();
				});
				/**
				 * Resume the counter on  focused tab
				 */
				$scope.$on('spend.tab.focus', function () {
					console.log('spend.resume');
					$scope.startCountingSpendTime();
				});
				/**
				 * we stop the counter when user click on some other tab
				 */
				$scope.$on('spend.tab.blur', function () {
					console.log('spend.tab.blur');
					$scope.stopCountingSpendTime();
				});
				/**
				 * We stop counter on windows close event
				 */
				$scope.$on('spend.window.close', function () {
					console.log('spend.tab.blur');
					$scope.stopCountingSpendTime();
				});
				/**
				 * We stop counter during window is minimized
				 */
				$scope.$on('spend.window.minimize', function () {
					console.log('spend.tab.blur');
					$scope.stopCountingSpendTime();
				});



				/* Force restart assetData when */
				if (assetData.asset_id != $routeParams.id)
					assetData = false;

				/* This flag is used for recognize the loading status after xHR */
				$scope.dataIsLoaded = false;

				$scope.axUrl = app7020Options.assetsAxController;

				/* Assets' details */
				$scope.details = {
					last_edit_by: false,
					last_edit_datetime: false,
					published_by: false,
					published_datetime: false,
					status: false,
					avatar: false,
					title: false,
					description: false,
					owner: false,
					owner_channel_url: false,
					tags: [],
					channels: [],
					uploaded_date: false
				};
				//fix for asset visibility private / publick display the channels
				$(document).on('change', 'input[name="App7020Assets[is_private]"]', function () {
					var vis_val = parseInt($(this).val());
					if (vis_val == 0) {
						$('[data-custom="channelWidgetContainer"]').css('display', 'block');
					} else {
						$('[data-custom="channelWidgetContainer"]').css('display', 'none');
					}
				});

				/* All Possible actions for an asset */
				$scope.actions = [];
 
				/* All current Peer Reviews */
				$scope.peer_reviews = {};

				/* All questions for an asset */
				$scope.questions = [];

				/* First Chunk with similar videos */
				$scope.similar = [];
				$scope.similar_total = 0;

				/* All current tooltips */
				$scope.tooltips = [];

				$scope.rating1 = '';

				$scope.player = false;

				if (typeof (assetData) !== 'object') {
					AJAX.post('/lms/index.php?r=restdata', {
						endpoint: 'asset_data',
						params: {
							id_asset: $routeParams.id
						}
					}, function (response) {
						//						if (!response.data.success || response.data.details.isvisibletothisuser == false) {
//							if(response.data.error)
//								Docebo.Feedback.show('error', response.data.error);
//							$scope.channelPath = '/lms/index.php?r=channels/index/#/';
//							window.location.href = $scope.channelPath;
//							return false;
//						}
						if (response.data.success) {
							$scope.error = false;
							$scope.dataIsLoaded = true;
							assetData = response.data;
							//Loading data into Object
							$scope.assetId = assetData.asset_id;
							$scope.details = assetData.details;
							$scope.actions = assetData.actions;
							$scope.peer_reviews = assetData.peer_reviews;
							$scope.similar = assetData.similar;
							$scope.similar_total = assetData.similar_total;
							$scope.tooltips = assetData.tooltips;
							$scope.ratingValue1 = assetData.details.rating;
							$scope.player = assetData.player;
							$scope.$root.breadcrumb_asset = $scope.details.title;
							$scope.dateFormat = app7020Options.DateFormat;
							$scope.questions = assetData.questions;

							/* Emit event upside for listener ouside ngView */
							$scope.$emit('loaded.details', $scope.details);
						}
						else {
							$scope.error = response.data.error;
						}

					}, function (response) {
						console.log(response.data);
					});
				} else {
					$scope.dataIsLoaded = true;
					/* Load scope data from local storage */
					$scope.assetId = assetData.asset_id;
					$scope.details = assetData.details;
					$scope.actions = assetData.actions;
					$scope.peer_reviews = assetData.peer_reviews;
					$scope.similar = assetData.similar;
					$scope.similar_total = assetData.similar_total;
					$scope.tooltips = assetData.tooltips;
					$scope.ratingValue1 = assetData.details.rating;
					$scope.player = assetData.player;
					$scope.$root.breadcrumb_asset = assetData.details.title;
					$scope.questions = assetData.questions;
				}
				/* Open invite modal */
				$scope.$on('open.inviteToWatch', function (e, data) {
					$scope.openInviteToWatchModal(data.id_asset);
				});

				$scope.$on('flowplayer.replace.tooltips', function (e, tooltips) {
					assetData.tooltips = tooltips;
				});

				$scope.$on('flowplayer.replace.cuepoints', function (e, cuepoints) {
					assetData.player.init.cuepoints = cuepoints;
				});

				$scope.$watch('peer_reviews', function (n, o) {
					assetData.peer_reviews = n;
				}, true);

				$scope.$watch('ratingValue1', function (n, o) {
					if (n != o)
						assetData.details.rating = n;
				}, true);
				$scope.$watch('details.canrate', function (n, o) {
					if (n != o)
						assetData.details.canrate = n;
				}, true);

				$scope.openInviteToWatchModal = function ($assetId) {
					var url = app7020Options.inviteToWatch + '/index&id=' + $assetId;
					var id = 'app7020-invite-dialog';
					var clases = 'app7020-invite-dialog wide';
					var data = {};
					var title = Yii.t('app7020', 'Invite to Watch');
					openDialog(false, title, url, id, clases, 'GET', data);
					return this;
				};

				/* Update assetData.details from outside */
				$scope.$on('update.details', function (e, data) {
					assetData.details.description = data.description;
					assetData.details.title = data.title;
					assetData.details.channels = data.channels;
					assetData.details.tags = data.tags;  
					assetData.details.last_edit_by = data.last_edit_by;
					assetData.details.last_edit_datetime = data.last_edit_datetime;
					$scope.$emit('updated.details', assetData.details);
				});

				/*
				 * if given group is the selected group, deselect it
				 * else, select the given group
				 */
				$scope.toggleGroup = function (group) {
					if ($scope.isGroupShown(group)) {
						$scope.shownGroup = null;
					} else {
						$scope.shownGroup = group;
					}
				};
				$scope.isGroupShown = function (group) {
					return $scope.shownGroup === group;
				};
//				$timeout(function () {
//					var requestParams = {
//						params: {
//							id_asset: $routeParams.id
//						},
//						endpoint: 'asset_pr'
//					}
//						if($scope.$root.statusBarMode == "edit") {
//							var myPoller = poller.get("/lms/index.php?r=restdata", {
//								action: 'post',
//								delay: 5000,
//								endpoint: 'asset_pr',
//								argumentsArray: [requestParams],
//								smart : true
//							});
//							/**
//							 * Callbacks and behavior after PROMISE from ajax was 
//							 * go to server and return his response 
//							 */
//							$scope.peer_reviews.ids = $scope.peer_reviews.ids || [];
//
//							myPoller.promise.then(null, null, function (response) {
//								$.each(response.data.items, function (key, value) {
//									// if new id is not presented in the array with peer reviews
//										if ($scope.peer_reviews.ids.indexOf(value.id) === -1) {
//											$scope.peer_reviews.ids.push(value.id);
//											$scope.peer_reviews.items[value.id] = value
//											$scope.newId = value.id
//										}
//								});
//							});
//						} else {
//							if(myPoller!="undefined") {
//								poller.stopAll();
//							}
//						}
//					
//				}, 10000);

				$scope.tooltipEditorMCE = {
					toolbar: "bold italic underline",
					formats: {
						bold: {inline: 'strong'},
						italic: {inline: 'em'},
						underline: {inline: 'u'}
					}
				};
			};


			Controller.$inject = ['$rootScope', '$scope', '$timeout', 'poller', '$routeParams', 'AJAX', 'GetDataService', 'RealTime', 'WSMessageAsset'];
			return Controller;
		});