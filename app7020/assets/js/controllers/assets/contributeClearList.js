'use strict';
define([], function () {
	/**
	 * 
	 * @param {ngScope} $scope
	 * @param {request} Request
	 * @returns {undefined}
	 */
	function clearList($scope, Request) {

		$scope.clearAll = function (param) {
			var url = '';
			if (parseInt(param) === 1) {
				url = 'axListPRAll';
			} else if (parseInt(param) === 2) {
				url = 'axListPublishAll';
			}
			new Request.ajax(app7020Options.assetsAxController + '/' + url, {}, $scope.success, $scope.error);
		};

		/**
		 * 
		 * @param {type} obejct
		 * @returns {undefined}
		 */
		$scope.success = function (object) {
			if (object.status) {
				//alert('SUCCESS CLEARED LIST');
				top.location.reload();
			} else {
				alert('FAILED CLEARING LIST');
			}
			return this;
		}

		/**
		 * 
		 * @param {type} xhr
		 * @param {type} status
		 * @param {type} errorThrown
		 * @returns {undefined}
		 */
		$scope.error = function (xhr, status, errorThrown) {
			console.log(xhr, status, errorThrown);
			alert(errorThrown);
		}

	}
	clearList.$inject = ['$scope', 'Request'];
	return clearList;
});