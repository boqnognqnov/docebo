
define(function () {
	'use strict';
	var PeerReview = function ($scope, poller, $http, Dates, $compile) {
		/**
		 * Array with all messages
		 */
		$scope.messages = new Array();
		/**
		 * Object with current message
		 * @var Object
		 */
		$scope.newMessage = {};
		/**
		 * All my current messages, stored as ID, This id must be excluded by genedal model 
		 * @var Array
		 */
		$scope.myMessagesIds = [];
		/**
		 * All current messages
		 * @var Array 
		 */
		$scope.currentMessage = new Array();
		/**
		 * Default avatar for logged user
		 * @var String
		 */
		$scope.avatar = app7020Options.userAvatar;
		/**
		 * Default kind for logged user
		 * @var String
		 */
		$scope.kind = app7020PeerReview.kind;
		/**
		 * Default full name for logged user
		 * @var String
		 */
		$scope.fullname = app7020PeerReview.userFullName;
		/**
		 * Store logged user id
		 * @var Number
		 */
		$scope.userId = app7020PeerReview.userId;
		/**
		 * Curren operation for call to action button
		 * @type string 
		 */
		$scope.actionCrud = 'insert';
		/**
		 * Define method for add new peer review
		 */
		$scope.addNew = addNew;
		/**
		 * Detection flag about new messages on load pages, or push new meesages on polling
		 * @type boolean
		 */
		$scope.isInitial = true;
		/**
		 * Scope var for force hide controllers on click any call
		 */
		$scope.hideControllers = [];
		/**
		 * General scope for poller with all options
		 * @type Object
		 */
		$scope.pollerObject = {
			params: {
				callback: 'JSON_CALLBACK',
				idFrom: 0
			},
			/**
			 * Update Current scope object needed by poller instance
			 * @param Number newIdFrom
			 * @returns {app_L9.$scope.pollerObject}
			 */
			update: function (newIdFrom) {
				//prevent to add undefined value
				if (typeof (newIdFrom) === "undefined") {
					return this;
				}
				this.params.idFrom = newIdFrom;
				return this;
			}
		}
		/**
		 * Update index on messages
		 */
		$scope.currentUpdateItem = 0;
		/**
		 * Messages for update 
		 * @type {array}
		 */
		$scope.updateMessages = new Array();
		$scope.updateMessagesProccess = {
			update: function (newIdFrom) {
				if (($.inArray(parseInt(newIdFrom), $scope.myMessagesIds) === -1) && parseInt(newIdFrom) > 0) {
					$scope.myMessagesIds.push(parseInt(newIdFrom));
					return this;
				}
			}
		}

		/**
		 * 
		 * @type Audio
		 */
		var audio = new Audio(app7020Options.peerReviewSong);
		/**
		 * Automaticly poller for new messages 
		 * @type @exp;poller@call;get
		 */
		var myPoller = poller.get(app7020Options.peerReviewController, {
			action: 'jsonp',
			delay: 40000,
			argumentsArray: [$scope.pollerObject]
		});
		/**
		 * Callbacks and behavior after PROMISE from ajax was 
		 * go to server and return his response 
		 */
		myPoller.promise.then(null, null, function (response) {
			if (response.data.status === true) {
				response.data.reviews.forEach(function (value, index) {
					//we chek for local message by model, if not exist, then will be shown by poller 
					if ($.inArray(parseInt(value.id), $scope.myMessagesIds) === -1) {
						$scope.messages.push(value);

						if ($scope.isInitial === false) {
							audio.play();
						}
						$scope.pollerObject.update(response.data.idFrom);
					}
				});
				$('.app7020PeerMessages > ul').mCustomScrollbar('init');
				$('.app7020PeerMessages > ul').mCustomScrollbar('scrollTo', 'last');
			}

		});


		/**
		 * Checking logic for shown controllers edit and delete
		 * @param Object object
		 * @returns {Boolean}
		 */
		$scope.checkForControlls = function (object) {
			if (typeof (object.idUser) === "undefined") {
				return false;
			}
			if (parseInt(object.idUser) === parseInt($scope.userId)) {
				return true;
			} else {
				return false;
			}
		}

		/**
		 * add new item to $scope model 
		 * @returns {app_L9}
		 */
		function addNew() {
			if ($scope.app7020AddNewAnswerForm.$valid) {
				var addDate = Dates.getUtcTime();
				//generate new message for shown immediatly 

				var message = {
					"message": $scope.newMessage.message,
					"avatar": $scope.avatar,
					"created": addDate,
					"lastUpdated": addDate,
					"kind": $scope.kind,
					"fullname": $scope.fullname,
					"idUser": parseInt($scope.userId),
				}
				//add in global array with all messages;
				var lastAddedMessageId = $scope.messages.push(message);
				$('.app7020PeerMessages > ul').mCustomScrollbar('update');
				$('.app7020PeerMessages > ul').mCustomScrollbar('scrollTo', 'last');
				$http({
					url: app7020Options.peerReviewControllerAdd,
					method: "GET",
					params: {"message": $scope.newMessage.message}
				}).success(function (data, status, headers, config) {
					$scope.newReview = data.reviewId;
					$scope.myMessagesIds.push(parseInt(data.reviewId));
					$scope.messages[(lastAddedMessageId - 1)].id = parseInt(data.reviewId);
				}).error(function (data, status, headers, config) {
					this.showError(Yii.t('app7020', 'The application detect problem. Please connect with Administrator'));
				});
				$scope.newMessage = {}; //resseting on model 
			}
			return this;
		}

		/**
		 * Invoked method by drop-down
		 * @param Number item
		 * @returns $scope.messages[Number]
		 */
		$scope.edit = function (item) {
			$scope.actionCrud = 'update';
			$scope.currentUpdateItem = item;
			this.pushHideControlls(item);
			$scope.newMessage.message = $scope.messages[item].message;
			return this;
		}

		// Create Tooltip from Peer Review
		$scope.reviewToTooltip = function (item) {
			var id = $scope.messages[item].id;
			var contentId = $scope.messages[item].idContent;
			var reviewId = $scope.messages[item].id;
			var vm = this;
			var invalid;
			var player = flowplayer($('.flowplayer'));
			var currentTime = player.ready ? player.video.time : 0;
			player.pause();
			var durationFrom = Math.round(currentTime);
			var durationTo = durationFrom + 5;
			$http({
				url: app7020Options.axAssetsController + '/axCheckValidTooltip',
				method: "GET",
				params: {
					"idContent": $scope.messages[item].idContent,
					"durationFrom": durationFrom,
					"durationTo": durationTo,
					"reviewId": reviewId
				}
			}).success(function (data) {
				invalid = data.manyTooltips;
				if (invalid == "1") { // If there are more than 2 tooltips at a time
					Docebo.Feedback.show('error', Yii.t('app7020', "Only 3 tooltips can overlap at the same time"));
					return false;
				}
				$http({
					url: app7020Options.knowledgeLibraryController + '/axSaveTooltip',
					method: "GET",
					params: {
						"content": $scope.messages[item].message,
						"peerReview": "1",
						"durationFrom": durationFrom,
						"durationTo": durationTo,
						"contentId": $scope.messages[item].idContent,
						"tooltipStyle": 0,
						"reviewId": reviewId
					}
				}).success(function (data, status, headers, config) {
					$scope.messages[item].IdTooltip = (data.id);
					$scope.messages[item].tooltipAt = (data.mmss);
					$scope.updateActionTooltip(item);
					Docebo.Feedback.show('success', Yii.t('app7020', "Tooltip added successfully"));
					window.location.href = app7020Options.knowledgeLibraryController + '/tooltips&id=' + data.contentId + '#' + data.id;

				}).error(function (data, status, headers, config) {
					this.showError(Yii.t('app7020', 'The application detect problem. Please connect with Administrator'));
				});
			});

			return false;
		}
		/**
		 * Invoked method by drop-down
		 * @param Number item
		 * @returns $scope.messages[Number]
		 */
		$scope.delete = function (item) {
			var id = $scope.messages[item].id;
			$scope.messages.splice(item, 1);
			$http({
				url: app7020Options.peerReviewControllerDelete,
				method: "GET",
				params: {"id": id},
			}).success(function (data, status, headers, config) {
				if (data.result === false) {
					this.showError(Yii.t('app7020', 'The application detect problem. Please connect with Administrator'));
				}
			}).error(function (data, status, headers, config) {
				this.showError(Yii.t('app7020', 'The application detect problem. Please connect with Administrator'));
			});
			return this;
		}

		/**
		 * 
		 * @returns {undefined}
		 */
		$scope.updateAction = function () {
			$scope.messages[$scope.currentUpdateItem].message = $scope.newMessage.message;
			$scope.newMessage = {};
			$scope.actionCrud = 'insert';
			$http({
				url: app7020Options.peerReviewControllerUpdate,
				method: "GET",
				params: {
					"message": $scope.messages[$scope.currentUpdateItem].message,
					"id": $scope.messages[$scope.currentUpdateItem].id
				}
			}).success(function (data, status, headers, config) {
				if (data.result) {
					$scope.myMessagesIds.push(parseInt(data.reviewId));
					$scope.resetHideControlls();
					var editDate = Dates.getUtcTime();
					$scope.messages[$scope.currentUpdateItem].lastUpdated = editDate;
				}
			}).error(function (data, status, headers, config) {
				this.showError(Yii.t('app7020', 'The application detect problem. Please connect with Administrator'));
			});
		};
		$scope.updateActionTooltip = function (item) {
 			$scope.currentUpdateItem = item;
			$scope.messages[$scope.currentUpdateItem].idTooltip = $scope.messages[item].IdTooltip;
			$scope.newMessage = {};
			$scope.actionCrud = 'insert';
			$http({
				url: app7020Options.peerReviewControllerUpdate,
				method: "GET",
				params: {
					"message": $scope.messages[$scope.currentUpdateItem].message,
					"id": $scope.messages[$scope.currentUpdateItem].id
				}
			}).success(function (data, status, headers, config) {
				if (data.result) {
					$scope.myMessagesIds.push(parseInt(data.reviewId));
					$scope.resetHideControlls();
					var editDate = Dates.getUtcTime();
					$scope.messages[$scope.currentUpdateItem].lastUpdated = editDate;
				}
			}).error(function (data, status, headers, config) {
				this.showError(Yii.t('app7020', 'The application detect problem. Please connect with Administrator'));
			});
		};
		/**
		 * Show insert button clause
		 * @returns {Boolean}
		 */
		$scope.isShowInsert = function () {
			if ($scope.actionCrud === 'insert') {
				return true;
			} else {
				return false;
			}
		};
		/**
		 * Show update button clause
		 * @returns {Boolean}
		 */
		$scope.isShowUpdate = function () {
			if ($scope.actionCrud === 'update') {
				return true;
			} else {
				return false;
			}
		}

		/**
		 * Show error bar for success
		 * @param String message
		 * @returns {app_L14.$scope}
		 */
		$scope.showError = function (message) {
			var defaultMessage = 'File was not published';
			if (typeof (message) != "undefined") {
				Docebo.Feedback.show('error', Yii.t('app7020', message));
			} else {
				Docebo.Feedback.show('error', Yii.t('app7020', defaultMessage));
			}
			return this;
		};
		/**
		 * Show success bar 
		 * @param String message
		 * @returns {app_L14.$scope}
		 */
		$scope.showSuccess = function (message) {
			var defaultMessage = 'File was published successfully';
			if (typeof (message) != "undefined") {
				Docebo.Feedback.show('success', Yii.t('app7020', message));
			} else {
				Docebo.Feedback.show('success', Yii.t('app7020', defaultMessage));
			}
			return this;
		};
		/**
		 * Set into scope array what element from message array must be proccessed for hidden controllers 
		 * @param {number} index
		 * @returns {app_L17.$scope}
		 */
		$scope.pushHideControlls = function (index) {
			this.resetHideControlls();
			$scope.hideControllers[index] = true;
			return this;
		}

		/**
		 * 
		 * @param {number} id
		 * @returns {app_L17.$scope}
		 */
		$scope.resetHideControlls = function () {
			$scope.hideControllers = [];
			return this;
		}

		/**
		 * Watch the general messages scope
		 */
		$scope.$watch('messages', function (newValue, oldValue) {
			$('.app7020PeerMessages > ul').mCustomScrollbar('update');
			$('.app7020PeerMessages > ul').mCustomScrollbar('scrollTo', 'last');
		});
		/**
		 * Push existing message in general steack; On each one poller will compare for updates and deletes 
		 * @param {object} message
		 * @returns {app_L17.$scope}
		 */
		$scope.pushMessageForUpdate = function (message) {
			if (typeof (message) === "undefined") {
				this.showError(Yii.t('app7020', 'The application detect problem. Please connect with Administrator'));
				return this;
			}
			if (typeof (message.id) === "undefined") {
				return this;
			}
			this.updateMessages[message.id] = message.created;
			return this;
		}
		function pad(num) {
			return ("0" + num).slice(-2);
		}
		function hhmmss(secs) {
			var minutes = Math.floor(secs / 60);
			secs = secs % 60;
			var hours = Math.floor(minutes / 60)
			minutes = minutes % 60;
			return pad(hours) + ":" + pad(minutes) + ":" + pad(secs);
		}
		$scope.jumpTo = function (tooltip) {
			var player = flowplayer($('.flowplayer'));
			$http({
				url: app7020Options.axAssetsController + '/axJumpToMoment',
				method: "GET",
				params: {
					"tooltip": tooltip
				}
			}).success(function (data, status, headers, config) {
				player.pause();
				player.seek(data.moment - 1);

			}).error(function (data, status, headers, config) {
				this.showError(Yii.t('app7020', 'The application detect problem. Please connect with Administrator'));
			});
		};

		$scope.NormalDate = function (dateString) {
			var localDate = new Date();
			var formatedDate;
			if (typeof dateString == "string") {
				var dateChunks = dateString.split('-');
				formatedDate = dateChunks[0] + "/" + dateChunks[1] + "/" + dateChunks[2];
			} else {
				formatedDate = dateString;
			}
			var newDate = new Date(formatedDate);
			var offset = (-1 * ((localDate.getTimezoneOffset()) / 60));
			var Y, M, D, H, i, s = null;
			newDate.setHours(newDate.getHours() + offset);
			Y = newDate.getFullYear();
			M = ("0" + newDate.getMonth()).slice(-2);
			D = ("0" + newDate.getDate()).slice(-2);
			H = ("0" + newDate.getHours()).slice(-2);
			i = ("0" + newDate.getMinutes()).slice(-2);
			s = ("0" + newDate.getSeconds()).slice(-2);
			return Y + '-' + M + '-' + D + ' ' + H + ':' + i + ':' + s
		};
	}

	PeerReview.$inject = ['$scope', 'poller', '$http', 'Dates', '$compile'];
	return PeerReview;

});