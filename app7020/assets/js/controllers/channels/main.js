'use strict';
require.config({
	baseUrl: app7020Options.assetUrl + '/js',
	
 	paths: {
		channelsController: 'controllers/channels/channelsController'
	},
	shim: {
		channelsController: {
			deps: []
		}
	}
});
define('jquery', [], function() { return jQuery; });
require(['channelsController']);