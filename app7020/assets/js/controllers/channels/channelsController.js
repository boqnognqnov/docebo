/**
 *
 * @param {type} d
 * @param {type} t
 * @returns {undefined}
 */
'use strict';
define(['plugins/app7020Tags','modules/notification','modules/appendCSS'], function (d, t, appendCSS) {

	new appendCSS('/themes/spt/css/fcbkcomplete.css');

	function channelsClass(options) {
		return this.init(options);
	}

	channelsClass.prototype = {
		options: {
			debug: false
		},
		init: function (options) {
			if (this.options.debug) {
				console.log('Channels Controller: initOptions');
				console.log(this.getOptions());
			}

		},
		openModal: function (step, title, id, prev) {
			var dialogClass = 'app7020-edit-channel-dialog';
			switch (step) {
				case "delete":
					dialogClass = 'app7020-delete-channel-dialog';
					break;
			}
			openDialog(false, // Event
					title, // Title
					app7020Options.axChannelsController + '/AxEditChannel', // Url
					'app7020-edit-channel-dialog', // Dialog-Id
					dialogClass, // Dialog-Class
					'POST', // Ajax Method
					{// Ajax Data
						'nextStep': step,
						'id': id,
						'prevStep': prev,
						'prev': prev
					});
			$(document).controls();
			setTimeout(function(){
				$("#channnelsCheckList").mCustomScrollbar();
			}, 1600);
		},
		tinymceInit: function () {
			
			try {
				tinymce.remove();
			} catch (e) {
			}

			tinymce.init({
				selector: "#channelDescription",
				inline: false,
				height: 0,
				parser: true,
				plugins: [
					"advlist autolink lists link image charmap print preview anchor",
					"searchreplace visualblocks code fullscreen",
					"insertdatetime media contextmenu paste moxiemanager textcolor"
				],
				toolbar: "bold italic underline image",
				menubar: false,
				statusbar: false,
				language: yii.language,
				setup: function (editor) {
					editor.on('blur', function (e) {
						var inputValueDescription = tinyMCE.activeEditor.getContent();
							var langCode = jQuery("option:selected", dialogId + ' #channelLangSelect').val();
							jQuery(dialogId + ' input#langArr_' + langCode + '_description').val(inputValueDescription.trim());
					});
				}
			});
		},
		tinymceSetContent: function (value) {
			if (tinyMCE.activeEditor != null) {
				tinyMCE.activeEditor.setContent(value);
			}
		},
		showHideChannel: function ($element) {
			// TODO
			var $this = this;
			var $element = $($element);
			$element.addClass('stop');
			$.ajax({
				url: app7020Options.axChannelsController + '/AxToggleShowHide',
				method: "POST",
				data: {channelId: $element.data('channel-id')},
				dataType: 'json',
			}).done(function (data) {
				if (data.result === true) {
					if (data.enabled == 0) {
						$element.addClass('disabled');
					} else {
						$element.removeClass('disabled');
					}
				}
				$element.removeClass('stop');
			});
		},
		updateAssignedLanguages: function () {
			var count = 0;
			var fieldsArr = [];
			jQuery('.app7020-edit-channel-dialog input[name*="[title]"]').each(function () {
				if (jQuery(this).val().trim().length > 0) {
					fieldsArr.push(jQuery(this).attr('name').match(/^\w+\[([\w+-]+)\]/i)[1]);
					++count;
				}
			});
			jQuery('.app7020-edit-channel-dialog #channelLangSelect option span').remove();
			jQuery.each(fieldsArr, function (index, value) {
				jQuery('.app7020-edit-channel-dialog #channelLangSelect option[value=' + value + ']').append("<span style=\"color: red;\"> *</span>");
			})
			jQuery('.app7020-edit-channel-dialog .channelLangInfo .assignedLang strong').html(count);
		},
		customTooltipReady: function () {
			// Topics main - action buttons
			var tooltip = $('#expertsManagement .main-actions .info');
			$(document).on('mouseenter', '.main-actions li', function () {
				tooltip.find('h4').html(jQuery(this).find('a').data('dialog-title'));
				tooltip.find('p').html(jQuery(this).find('a').attr('alt'));
				tooltip.show();
			});
			$(document).on('mouseleave', '.main-actions li', function () {
				tooltip.find('h4, p').html('');
				tooltip.hide();
			});
		},
		mainExpertsReady: function () {
			var that = this;
			that.customTooltipReady();
			$(document).on('dialog2.closed', '#app7020-unassign-experts-dialog, .app7020-assign-experts-dialog', function (e) {
				app7020.assignKnowledgeGurus.resetCounter()
				// Do the yiiGridView update here
				$.fn.yiiListView.update("expertsComboListView", $('#combo-list-view-container-expertsComboListView #combo-list-form'));
			});
            $(document).on('dialog2.content-update', '#app7020-unassign-experts-dialog', function () {
                $(this).closest('.modal').find('.save-dialog').addClass('disabled');
                $(this).closest('.modal').find('.save-dialog').css('pointer-events', 'none');
                $(document).on('click', '.app7020-unassign-experts-dialog [type="checkbox"]', function () {
                    if ($(this).prop('checked')) {
                        $(document).find('.app7020-unassign-experts-dialog .btn.save-dialog').removeClass('disabled');
                        $(document).find('.app7020-unassign-experts-dialog .btn.save-dialog').css('pointer-events', 'auto');
                    } else {
                        $(document).find('.app7020-unassign-experts-dialog .btn.save-dialog').css('pointer-events', 'none');
                        $(document).find('.app7020-unassign-experts-dialog .btn.save-dialog').addClass('disabled');
                    }
                });
            });
			$(document).on('combolistview.massive-action-selected', function (e, data) {
				var idChannel = $('#combo-list-view-container-expertsComboListView input[name=id]').val();
				var handler = data.listId;
				// Check is selected items OR set default option
				// from selection
				if (data.selection.length === 0) {
					this.activeElement.value = '';
				}

				// Switch all actions from selection
				switch (data.action) {
					case "delete":
						if (data.selection.length > 0 && handler != 'null') {
							openDialog(false, // Event
									Yii.t('app7020', 'Unassign experts'), // Title
									app7020Options.axChannelsController + '/unAssignExperts', // Url
									'app7020-unassign-experts-dialog', // Dialog-Id
									'app7020-unassign-experts-dialog', // Dialog-Class
									'POST', // Ajax Method
									{// Ajax Data
										'idChannel': idChannel,
										'idExperts': data.selection.join(","),
									});
						}
				}
			});
		},
	}

	// from here down call functions
	var channels = new channelsClass();
	channels.mainExpertsReady();

	$(document).on('click touchstart', 'a[data-modal="openModal"]', function (e) {
		e.preventDefault();
		var title = $(this).attr('data-dialog-title');
		channels.openModal('initial_create', title);
	});
	$(document).on('click', '#channelsGridView .channel_show .fa:not(.stop)', function () {
		channels.showHideChannel(this);
	});
	$(document).on('click touchstart', '.link[data-method="edit"]', function (e) {
		var title = $(this).attr('data-dialog-title');
		var id = $(this).parents('tr').find('.channel_show i').attr('data-channel-id');
		channels.openModal('initial_create', title, id);
	});

	$(document).on('click touchstart', '.link[data-method="delete"]', function (e) {
		var title = $(this).attr('data-dialog-title');
		var id = $(this).parents('tr').find('.channel_show i').attr('data-channel-id');
		channels.openModal('delete', title, id);
	});

	$(document).on('click touchstart', '.link[data-method="clone"]', function (e) {
		var title = $(this).attr('data-dialog-title');
		var id = $(this).parents('tr').find('.channel_show i').attr('data-channel-id');
		channels.openModal('clone', title, id);
	});

	$(document).on('click touchstart', '.channel_visibility', function (e) {
		var title = Yii.t('app7020', 'Edit Channel Visibility');
		var id = $(this).parents('tr').find('.channel_show i').attr('data-channel-id');
		channels.openModal('init_visibility', title, id);
	});

	$(document).on('click touchstart', '.channel_upload_permissions span.not_predefined', function (e) {
		var title = Yii.t('app7020', 'Edit Channel Permissions');
		var id = $(this).parents('tr').find('.channel_show i').attr('data-channel-id');
		channels.openModal('init_permissions', title, id);
	});

	$(document).on('click touchstart', '.prev-button', function (e) {
		var title = $("#step-title").html();
		var step = $(this).parents('.modal').find('#prevStep').val();
		if (typeof step == 'undefined') {
			var step = 'step2';
		}
		$('#app7020-edit-channel-dialog').dialog2('close');
		channels.openModal(step, title, false, true);
	});
	$(document).on('hover', '#app7020ChannelsContainer #channelsGridView .channel_dropdownMenu .customDropdown-widget', function (e) {
		var toToggle = $(this).closest('tr').find('.channel_show i, .channel_dnd i');
		if (e.type === 'mouseenter') {
			toToggle.css({
				fontSize: 0
			});
		}
		if (e.type === 'mouseleave') {
			toToggle.css({
				fontSize: ''
			});
		}
	});

	//$(document).on

	function updateFarbtasticColor(farbtasticObj, input) {
		farbtasticObj.setColor(input.val());
	}

	function farbtasticUpdateCallback(farbtasticObj, input) {
		return function () {
			updateFarbtasticColor(farbtasticObj, input);
		}
	}
	
	$(document).on('dialog2.content-update', '#app7020-edit-channel-dialog', function(){
		channels.updateAssignedLanguages();
	});
	
	$(document).on('dialog2.content-update', function (data) {
		var stepTitle = $("#step-title").html();
		$(".modal-header h3").html(stepTitle);
		//if ($(this).find('#sort').length > 0) {
		//	$(this).find('#sort').app7020TagsPlugin();
		//}

		var resp = $('<div>' + data.target.innerHTML + '</div');
		if (resp.find('form#users-selector-form').length == 0) {
			$('.modal.app7020-edit-channel-dialog').removeClass('users-selector');
		}
		if (resp.find("a.auto-close.success").length > 0) {
			$('#channelsGridView').yiiGridView('update');

			if (resp.find("a.auto-close.success.new").length > 0) {
				var title = $(document).find('#openNewChannelDialog').attr('data-dialog-title');
				channels.openModal('initial_create', title, false);
			}

			if (resp.find("a.auto-close.success.assign").length > 0) {
				window.location.href = app7020Options.channelsController + '/Experts&id=' + $.parseJSON(resp.find('a.data').text());
			}

			if (resp.find("a.auto-close.success.clone").length > 0) {
				var id = $.parseJSON(resp.find('a.data').text());
				var title = $('[data-channel-id="' + $.parseJSON(resp.find('a.data').text()) + '"').parents('tr').find('.link[data-method="clone"]').attr('data-dialog-title');
				if (typeof title === 'undefined') {
					var title = Yii.t('app7020', 'Clone Channel');
				}
				channels.openModal('clone', title, id);
			}

			if (resp.find("a.auto-close.success.cloned").length > 0) {
				//$(this).dialog2("close");
			}
			if (resp.find("a.auto-close.success.delete").length > 0) {
				$(this).dialog2("close");
			}

		} else if (resp.find("a.auto-close.unsuccess").length > 0) {
			$(this).dialog2("close");
		}

		if ($('#users-selector-container').length > 0) {
			$(document).find('.modal:not(.app7020-assign-experts-dialog) .modal-footer a.green:not(.prev-button)').text(Yii.t('app7020', 'Next'));
			$(document).find('.modal.users-selector:not(.app7020-assign-experts-dialog) .modal-footer a.green:not(.prev-button)').parents('.modal-footer').prepend('<a href="#" data-prev="step2" class="btn btn-docebo green big prev-button">' + Yii.t('standard', '_PREV') + '</a>');
		}

		if ($('#channelDescription').length > 0) {
			channels.tinymceInit();
		}

		if ($('#color-picker-section').length > 0) {
			$('select').styler();

			$(document).find('.colors .color-item input').each(function () {
				var $this = $(this);
				var textColorWrapper = $this.closest('.wrap[data-type="color"]');
				var backgroundColorWrapper = $this.closest('.wrap[data-type="background-color"]');
				var previewBox = $this.closest('.wrap').find('.color-preview');
				var previewSection = $this.closest('.channelLookAndFell').find('.preview-content');
				/* Icon Color */
				if (textColorWrapper.length) {
					var farbtasticTextColor = $.farbtastic(textColorWrapper.find('.colorpicker'), function (hexColor) {
						previewSection.find('.preview-tile .fa').css({
							color: hexColor
						});
						$this.val(hexColor);
						previewBox.css({
							backgroundColor: hexColor
						});
					}).setColor($this.val());
					$this.on('change keyup', function () {
						farbtasticTextColor.setColor($(this).val());
					});
				}
				/* Background Color */
				if (backgroundColorWrapper.length) {
					var farbtasticBackgroundColor = $.farbtastic(backgroundColorWrapper.find('.colorpicker'), function (hexColor) {
						previewSection.find('.preview-tile').css({
							backgroundColor: hexColor
						});
						$this.val(hexColor);
						previewBox.css({
							backgroundColor: hexColor
						});
					}).setColor($this.val());
					$this.on('change keyup', function () {
						farbtasticBackgroundColor.setColor($(this).val());
					});
				}
			});

			$(document).find('.color-item input').tooltip({position: 'right'});
			$(document).find('.colors').on('click', '.color-preview', function () {
				$(this).closest('.wrap').find('input').focus();
			});

			$("#channelIconPicker").each(function () {
				if ($(this).children().length > 0)
					$(this).remove();
			});

			$("#channelIconPicker").iconpicker({
				arrowNextIconClass: 'fa fa-arrow-right',
				arrowPrevIconClass: 'fa fa-arrow-left'
			});

			$("#channelIconPicker").on('change', function (e) {
				$('.preview-tile > i').attr('class', 'colorable fa ' + e.icon + ' fa-3x fa-inverse');
			});
		}

		var dialogId = '#app7020-edit-channel-dialog .form_step1';
		var inputLangValue = jQuery(dialogId + ' #channelTitle');
		var inputLangValueDescription = jQuery(dialogId + ' #channelDescription');
		channels.updateAssignedLanguages();
		if (jQuery("option:selected", jQuery(this)).val() != '0') {
			var hiddenValue = jQuery(dialogId + ' input#langArr_' + jQuery("option:selected", jQuery(dialogId + ' #channelLangSelect')).val() + '_title').val();
			var hiddenValueDescription = jQuery(dialogId + ' input#langArr_' + jQuery("option:selected", jQuery(dialogId + ' #channelLangSelect')).val() + '_description').val();
			inputLangValue.prop("disabled", false);
			if (hiddenValue) {
				inputLangValue.val(hiddenValue.trim());
				inputLangValueDescription.val(hiddenValueDescription.trim());
			}
			if (hiddenValueDescription) {
				channels.tinymceSetContent(hiddenValueDescription);
			}
		}
		else {
			inputLangValue.prop("disabled", true);
		}

	});
	var dialogId = '#app7020-edit-channel-dialog .form_step1';
	jQuery(document).on('change', dialogId + ' #channelLangSelect', function () {
		var langText = jQuery("option:selected", jQuery(this)).text();
		var inputLangValue = jQuery(dialogId + ' #channelTitle');
		var inputLangValueDescription = jQuery(dialogId + ' #channelDescription');
		channels.tinymceSetContent('');
		if (jQuery("option:selected", jQuery(this)).val() != '0') {
			var hiddenValue = jQuery(dialogId + ' input#langArr_' + jQuery("option:selected", jQuery(this)).val() + '_title').val();
			var hiddenValueDescription = jQuery(dialogId + ' input#langArr_' + jQuery("option:selected", jQuery(this)).val() + '_description').val();
			inputLangValue.prop("disabled", false);
			inputLangValue.val(hiddenValue.trim());
			inputLangValueDescription.val(hiddenValueDescription.trim());
			channels.tinymceSetContent(hiddenValueDescription);
		}
		else {
			inputLangValue.prop("disabled", true);
		}
//		$(dialogId).find('span.current-lang-selected').each(function () {
//			$(this).text(langText);
//		});
	});
	// Channel dialog - get title and save in hidden field
	jQuery(document).on('keyup', dialogId + ' #channelTitle', function () {
		var inputValue = jQuery(this).val();
			var langCode = jQuery("option:selected", dialogId + ' #channelLangSelect').val();
			jQuery(dialogId + ' input#langArr_' + langCode + '_title').val(inputValue.trim());
		channels.updateAssignedLanguages();
	});

	// Channel dialog - Check Before Submit form the default
	// laguage
	// is
	// translated
	jQuery(document).on('click', dialogId + ' .save-dialog', function (e) {
		e.preventDefault();
		if (jQuery(dialogId + ' input#langArr_' + jQuery(this).data('default-lang') + '_title').val().trim().length <= 0) {
			jQuery(dialogId + ' .channelLangInfo .requiredLang').show(0).stop(true, true).delay(3000).fadeOut(500).hide(0);
		}
		else if (jQuery(dialogId + ' input#langArr_' + jQuery(this).data('default-lang') + '_title').val().trim().length >= 255) {
			jQuery(dialogId + ' .channelLangInfo .maxLetters').show(0).stop(true, true).delay(3000).fadeOut(500).hide(0);
		}
		else {
			jQuery(dialogId + ' .channelLangInfo .requiredLang').hide();
			jQuery(this).parents('form#editChannelForm').submit();
		}
	});

});