define(['modules/notification'], function () {
	function coachActivity($scope, ngActivity) {
		$scope.text_color = null;
		var length = null;

		$scope.lineoptions = {
			//Boolean - Whether the line is curved between points
			bezierCurve: false,
			//Number - Radius of each point dot in pixels
			pointDotRadius: 6,
			//Number - Pixel width of point dot stroke
			pointDotStrokeWidth: 2,
			//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
			pointHitDetectionRadius: 8,
			tooltipTemplate: function (d) {
				var template = "" + d.value + ":" + d.label + ":" + d.strokeColor + "";
				return template;
			},
			customTooltips: function (tooltip) {
				var tooltipEl = $('#line-tooltip');

				if (!tooltip) {
					tooltipEl.css({
						opacity: 0
					});
					return;
				}

				tooltipEl.removeClass('above below');
				tooltipEl.addClass(tooltip.yAlign);

				// split out the label and value and make your own tooltip here
				var parts = tooltip.text.split(":");
				var innerHtml = '<div style="background-color:' + parts[2].trim() + ';color:#fff" class="tooltip-container"><span>' + parts[0].trim() + '</span><br /><span>' + parts[1].trim() + '</span></div>';
				tooltipEl.html(innerHtml);

				tooltipEl.css({
					opacity: 1,
					left: tooltip.x - 100 + 'px',
					top: tooltip.y + 'px',
				});
			}


		}

		$scope.lineoptions2 = {
			//Boolean - Whether the line is curved between points
			bezierCurve: false,
			//Number - Radius of each point dot in pixels
			pointDotRadius: 6,
			//Number - Pixel width of point dot stroke
			pointDotStrokeWidth: 2,
			//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
			pointHitDetectionRadius: 8,
			tooltipTemplate: function (d) {
				var template = "" + d.value + ":" + d.label + ":" + d.strokeColor + "";
				return template;
			},
			customTooltips: function (tooltip) {
				var tooltipEl = $('#line-tooltip2');

				if (!tooltip) {
					tooltipEl.css({
						opacity: 0
					});
					return;
				}

				tooltipEl.removeClass('above below');
				tooltipEl.addClass(tooltip.yAlign);

				// split out the label and value and make your own tooltip here
				var parts = tooltip.text.split(":");
				var innerHtml = '<div style="background-color:' + parts[2].trim() + ';color:#fff" class="tooltip-container"><span>' + parts[0].trim() + '</span><br /><span>' + parts[1].trim() + '</span></div>';
				tooltipEl.html(innerHtml);

				tooltipEl.css({
					opacity: 1,
					left: tooltip.x - 100 + 'px',
					top: tooltip.y + 'px',
				});
			}


		}



		$scope.options1 = {
			percentageInnerCutout: 75,
			tooltipTemplate: function (d) {

				angular.forEach($scope.coachd.data, function (value, key) {
					if (value.label == d.label && value.value == d.value) {
						$scope.text_color = value.text_color;
					}
				});
				var template = "" + d.value + "%:" + d.label + ":" + d.fillColor + ":" + $scope.text_color + "";
				return template;
			},
			customTooltips: function (tooltip) {
				var tooltipEl = $('#coachd-tooltip');

				if (!tooltip) {
					tooltipEl.css({
						opacity: 0
					});
					return;
				}

				tooltipEl.removeClass('above below');
				tooltipEl.addClass(tooltip.yAlign);

				// split out the label and value and make your own tooltip here
				var parts = tooltip.text.split(":");
				var tooltip_text = parts[0].trim()
				if (tooltip_text == 0.1)
				{
					var tooltip_text = "0";
				}
				var innerHtml = '<div style="background-color:' + parts[2].trim() + ';color:' + parts[3].trim() + '" class="tooltip-container"><span>' + tooltip_text + '</span><br /><span>' + parts[1].trim() + '</span></div>';
				tooltipEl.html(innerHtml);

				tooltipEl.css({
					opacity: 1,
					left: tooltip.chart.canvas.offsetLeft + tooltip.x + 'px',
					top: tooltip.chart.canvas.offsetTop + tooltip.y + 'px',
				});
			}
		};

		$scope.options2 = {
			percentageInnerCutout: 75,
			tooltipTemplate: function (d) {

				angular.forEach($scope.peerd.data, function (value, key) {
					if (value.label == d.label && value.value == d.value) {
						$scope.text_color = value.text_color;
					}
				});
				var template = "" + d.value + "%:" + d.label + ":" + d.fillColor + ":" + $scope.text_color + "";
				return template;
			},
			customTooltips: function (tooltip) {
				var tooltipEl = $('#peerd-tooltip');

				if (!tooltip) {
					tooltipEl.css({
						opacity: 0
					});
					return;
				}

				tooltipEl.removeClass('above below');
				tooltipEl.addClass(tooltip.yAlign);

				// split out the label and value and make your own tooltip here
				var parts = tooltip.text.split(":");
				var tooltip_text = parts[0].trim()
				if (tooltip_text == 0.1)
				{
					var tooltip_text = "0";
				}
				var innerHtml = '<div style="background-color:' + parts[2].trim() + ';color:' + parts[3].trim() + '" class="tooltip-container"><span>' + tooltip_text + '</span><br /><span>' + parts[1].trim() + '</span></div>';
				tooltipEl.html(innerHtml);

				tooltipEl.css({
					opacity: 1,
					left: tooltip.chart.canvas.offsetLeft + tooltip.x + 'px',
					top: tooltip.chart.canvas.offsetTop + tooltip.y + 'px',
				});
			}
		};
		$scope.options3 = {
			percentageInnerCutout: 75,
			tooltipTemplate: function (d) {

				angular.forEach($scope.peerd.data, function (value, key) {
					if (value.label == d.label && value.value == d.value) {
						$scope.text_color = value.text_color;
					}
				});
				var template = "" + d.value + ":" + d.label + ":" + d.fillColor + ":#fff";
				return template;
			},
			customTooltips: function (tooltip) {
				var tooltipEl = $('#coachl-tooltip');

				if (!tooltip) {
					tooltipEl.css({
						opacity: 0
					});
					return;
				}

				tooltipEl.removeClass('above below');
				tooltipEl.addClass(tooltip.yAlign);

				// split out the label and value and make your own tooltip here
				var parts = tooltip.text.split(":");
				var tooltip_text = parts[0].trim()
				if (tooltip_text == 0.1)
				{
					tooltip_text = 0;
				}
				var innerHtml = '<div style="background-color:' + parts[2].trim() + ';color:' + parts[3].trim() + '" class="tooltip-container"><span>' + tooltip_text + '</span><br /><span>' + parts[1].trim() + '</span></div>';
				tooltipEl.html(innerHtml);

				tooltipEl.css({
					opacity: 1,
					left: tooltip.chart.canvas.offsetLeft + tooltip.x + 'px',
					top: tooltip.chart.canvas.offsetTop + tooltip.y + 'px',
				});
			}
		};


		$scope.coachb = {data: [], options: []};
		$scope.coachd = {data: [], options: []};
		$scope.coachl = {data: [], options: []};
		$scope.peerb = {data: [], options: []};
		$scope.peerd = {data: [], options: []};
		$scope.timeframes = [{timeframe: 7, value: 'Weekly'}, {timeframe: 30, value: 'Monthly'}, {timeframe: 365, value: "Yearly"}];
		$scope.timeframe = {timeframe: 30, value: 'Monthly'};
		$scope.tab = 1;

		ngActivity.get('/lms/index.php?r=restdata', 'coach_activity', $scope.timeframe.timeframe).then(function (data) {
			$scope.coachb = {data: [], options: []};
			$scope.coachd = {data: [], options: []};
			$scope.coachl = {data: [], options: []};
			$scope.peerb = {data: [], options: []};
			$scope.peerd = {data: [], options: []};
			$scope.coach_data = data.activityData;
			if (data.activityData.likes == 0) {
				$scope.likes = 0.1;
			} else {
				$scope.likes = data.activityData.likes;
			}
			if (data.activityData.dislikes == 0) {
				$scope.dislikes = 0.1;
			} else {
				$scope.dislikes = data.activityData.dislikes;
			}
			$scope.coachb.data = {
				labels: data.activityData.chartAnsweredQuestions.labels,
				datasets: [
					{
						fillColor: "rgba(4,100,174,0)",
						strokeColor: "rgba(4,100,174,1)",
						pointColor: "rgba(255,255,255,1)",
						pointStrokeColor: "rgba(4,100,174,1)",
						highlightFill: "rgba(4,100,174,0)",
						highlightStroke: "rgba(4,100,174,1)",
						data: data.activityData.chartAnsweredQuestions.points,
					}
				]
			};
			$scope.coachd.data = data.activityData.chartMyActivityPerChannel;
			$scope.coachl.data = [
				{
					value: $scope.likes,
					color: "#5ebe5d",
					highlight: "#5ebe5d",
					label: "My answer's likes",
				},
				{
					value: $scope.dislikes,
					color: "#e84c3d",
					highlight: "#e84c3d",
					label: "My answer's dislikes",
				},
			];
			$scope.coachb.options = $scope.lineoptions;
			$scope.coachd.options = $scope.options1;
			$scope.coachl.options = $scope.options3;
			$scope.peerb.data = {
				labels: data.activityData.chartReviewedAssets.labels,
				datasets: [
					{
						fillColor: "rgba(4,100,174,0)",
						strokeColor: "rgba(4,100,174,1)",
						pointColor: "rgba(255,255,255,1)",
						pointStrokeColor: "rgba(4,100,174,1)",
						highlightFill: "rgba(4,100,174,0)",
						highlightStroke: "rgba(4,100,174,1)",
						data: data.activityData.chartAnsweredQuestions.points
					}
				]
			};
			$scope.peerd.data = data.activityData.chartMyPeerActivityPerChannel;
			$scope.peerb.options = $scope.lineoptions2;
			$scope.peerd.options = $scope.options2;
		});
		$scope.newSelection = function () {
			$scope.coachb = {data: [], options: []};
			$scope.coachd = {data: [], options: []};
			$scope.coachl = {data: [], options: []};
			$scope.peerb = {data: [], options: []};
			$scope.peerd = {data: [], options: []};
			ngActivity.get('/lms/index.php?r=restdata', 'coach_activity', $scope.timeframe.timeframe).then(function (data) {

				if (data.activityData.likes == 0) {
					$scope.likes = 0.1;
				} else {
					$scope.likes = data.activityData.likes;
				}
				if (data.activityData.dislikes == 0) {
					$scope.dislikes = 0.1;
				} else {
					$scope.dislikes = data.activityData.dislikes;
				}

				$scope.coach_data = data.activityData;
				$scope.coachb.data = {
					labels: data.activityData.chartAnsweredQuestions.labels,
					datasets: [
						{
							fillColor: "rgba(4,100,174,0)",
							strokeColor: "rgba(4,100,174,1)",
							pointColor: "rgba(255,255,255,1)",
							pointStrokeColor: "rgba(4,100,174,1)",
							highlightFill: "rgba(4,100,174,0)",
							highlightStroke: "rgba(4,100,174,1)",
							data: data.activityData.chartAnsweredQuestions.points
						}
					]
				};
				$scope.coachd.data = data.activityData.chartMyActivityPerChannel;
				$scope.coachl.data = [
					{
						value: $scope.likes,
						color: "#5ebe5d",
						highlight: "#5ebe5d",
						label: "My answer's likes",
					},
					{
						value: $scope.dislikes,
						color: "#e84c3d",
						highlight: "#e84c3d",
						label: "My answer's dislikes",
					},
				];
				$scope.coachb.options = $scope.lineoptions;
				$scope.coachd.options = $scope.options1;
				$scope.coachl.options = $scope.options3;
				$scope.peerb.data = {
					labels: data.activityData.chartReviewedAssets.labels,
					datasets: [
						{
							fillColor: "rgba(4,100,174,0)",
							strokeColor: "rgba(4,100,174,1)",
							pointColor: "rgba(255,255,255,1)",
							pointStrokeColor: "rgba(4,100,174,1)",
							highlightFill: "rgba(4,100,174,0)",
							highlightStroke: "rgba(4,100,174,1)",
							data: data.activityData.chartReviewedAssets.points
						}
					]
				};
				$scope.peerd.data = data.activityData.chartMyPeerActivityPerChannel;
				$scope.peerb.options = $scope.lineoptions2;
				$scope.peerd.options = $scope.options2;
			});

		};
		$scope.setTab = function (tabId) {
			$scope.tab = tabId;

		};

		$scope.isSet = function (tabId) {
			return $scope.tab === tabId;
		};
	}
	coachActivity.$inject = ['$scope', 'ngActivity', '$compile', '$rootScope'];
	return coachActivity;
});