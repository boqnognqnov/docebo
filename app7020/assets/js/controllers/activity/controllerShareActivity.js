define(['modules/notification'],function () {
	function shareActivity($scope, ngActivity) {

		$scope.lineoptions={

			//Boolean - Whether the line is curved between points
			bezierCurve : false,

			//Number - Radius of each point dot in pixels
			pointDotRadius : 6,

			//Number - Pixel width of point dot stroke
			pointDotStrokeWidth : 2,

			//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
			pointHitDetectionRadius : 8,

			//Boolean - Whether to fill the dataset with a colour
			datasetFill : false,

			multiTooltipTemplate : function(d){
				var template = ""+d.value+":"+d.label+":"+d.strokeColor+"";
				return template;
			},

			customTooltips: function (tooltip) {
				var tooltipEl = $('#line-tooltip');

				if (!tooltip) {
					tooltipEl.css({
						opacity: 0
					});
					return;
				}

				tooltipEl.removeClass('above below');
				tooltipEl.addClass(tooltip.yAlign);

				// split out the label and value and make your own tooltip here
				var innerHtml ='';
				angular.forEach(tooltip.labels,function(value, key) {
					var parts = value.split(":");
					innerHtml += '<div style="background-color:' + parts[2].trim() + ';color:#fff" class="tooltip-container"><span>' + parts[0].trim() + '</span><br /><span>' + parts[1].trim() + '</span></div>';
				});

				tooltipEl.html(innerHtml);

				tooltipEl.css({
					opacity: 1,
					left: tooltip.x+120 + 'px',
					top: tooltip.y + 'px',
				});
			}

		}


		$scope.options1 = {
			percentageInnerCutout: 75,
			tooltipTemplate : function(d){

				angular.forEach($scope.shared.data,function(value, key) {
					if(value.label == d.label && value.value == d.value ){
						$scope.text_color =value.text_color;
					}
				});
				var template = ""+d.value+"%:"+d.label+":"+d.fillColor+":"+$scope.text_color+"";
				return template;
			},
			customTooltips: function (tooltip) {
				var tooltipEl = $('#shared-tooltip');

				if (!tooltip) {
					tooltipEl.css({
						opacity: 0
					});
					return;
				}

				tooltipEl.removeClass('above below');
				tooltipEl.addClass(tooltip.yAlign);

				// split out the label and value and make your own tooltip here
				var parts = tooltip.text.split(":");
				var innerHtml = '<div style="background-color:' + parts[2].trim() + ';color:' + parts[3].trim() + '" class="tooltip-container"><span>' + parts[0].trim() + '</span><br /><span>' + parts[1].trim() + '</span></div>';
				tooltipEl.html(innerHtml);

				tooltipEl.css({
					opacity: 1,
					left: tooltip.chart.canvas.offsetLeft + tooltip.x + 'px',
					top: tooltip.chart.canvas.offsetTop + tooltip.y + 'px',
				});
			}
		};

		$scope.shareb = {data: [], options: []};
		$scope.shared = {data: [], options: []};
		$scope.timeframes = [{timeframe: 7, value: 'Weekly'}, {timeframe: 30, value: 'Monthly'}, {timeframe: 365, value: "Yearly"}];
		$scope.timeframe = {timeframe: 30, value: 'Monthly'};
		$scope.tab = 1;


		ngActivity.get('/lms/index.php?r=restdata', 'sharing_activity', $scope.timeframe.timeframe).then(function (data) {
			$scope.shareb = {data: [], options: []};
			$scope.shared = {data: [], options: []};
			$scope.share_data = data.sharingData;
			$scope.shareb.data = {
				labels: data.sharingData.sharedAssetsViews.periods,
				datasets: [
					{
						fillColor: "rgba(174,222,174,1)",
						strokeColor: "rgba(174,222,174,1)",
						pointColor: "rgba(255,255,255,1)",
						highlightFill: "rgba(174,222,174,1)",
						highlightStroke: "rgba(174,222,174,1)",
						pointStrokeColor: "rgba(174,222,174,1)",
						data: data.sharingData.sharedAssetsViews.views,
					},
					{
						fillColor: "rgba(4,100,174,1)",
						strokeColor: "rgba(4,100,174,1)",
						pointColor: "rgba(255,255,255,1)",
						highlightFill: "rgba(4,100,174,1)",
						highlightStroke: "rgba(4,100,174,1)",
						pointStrokeColor: "rgba(4,100,174,1)",
						data: data.sharingData.sharedAssetsViews.assets,
					}
				]
			};
			$scope.shared.data = data.sharingData.chartMyActivityPerChannel;
			$scope.shareb.options = $scope.lineoptions;
			$scope.shared.options = $scope.options1;
		});
		$scope.newSelection = function () {
			$scope.shareb = {data: [], options: []};
			$scope.shared = {data: [], options: []};
			ngActivity.get('/lms/index.php?r=restdata', 'sharing_activity', $scope.timeframe.timeframe).then(function (data) {
				$scope.share_data = data.sharingData;
				$scope.shareb.data = {
					labels: data.sharingData.sharedAssetsViews.periods,
					datasets: [
						{
							fillColor: "rgba(174,222,174,1)",
							strokeColor: "rgba(174,222,174,1)",
							pointColor: "rgba(255,255,255,1)",
							highlightFill: "rgba(174,222,174,1)",
							highlightStroke: "rgba(174,222,174,1)",
							pointStrokeColor: "rgba(174,222,174,1)",
							data: data.sharingData.sharedAssetsViews.views,
						},
						{
							fillColor: "rgba(4,100,174,1)",
							strokeColor: "rgba(4,100,174,1)",
							pointColor: "rgba(255,255,255,1)",
							highlightFill: "rgba(4,100,174,1)",
							highlightStroke: "rgba(4,100,174,1)",
							pointStrokeColor: "rgba(4,100,174,1)",
							data: data.sharingData.sharedAssetsViews.assets,
						}
					]
				};
				$scope.shared.data = data.sharingData.chartMyActivityPerChannel;
				$scope.shareb.options = $scope.lineoptions;
				$scope.shared.options = $scope.options1;
			});

		};
		$scope.setTab = function (tabId) {
			$scope.tab = tabId;

		};

		$scope.isSet = function (tabId) {
			return $scope.tab === tabId;
		};
	}
	shareActivity.$inject = ['$scope', 'ngActivity'];
	return shareActivity;
});