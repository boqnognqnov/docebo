'use strict';
define(['modules/notification'], function ( ) {

	function  inviteController($scope, dataFeed, $timeout, $compile, $rootScope, $http) {

		/**
		 *
		 */
		//	$scope.currentAsset = false;

		$scope.suggestions = {};
		$scope.invitedUsers = 0;
		$scope.type = null;
		$scope.prepare = {};
		$scope.req = 0;
		$scope.errors = [];
		$scope.rest_query = false;
		$scope.name = '';
		$scope.onItemSelected = function () { // this gets executed when an item is selected
			$scope.name = '';
		};

		$scope.populate = function (response) {
			angular.forEach(response, function (object, index) {
				if (typeof ($scope[index]) != 'undefined') {
					$scope[index] = object;
				}
			});
			return this;
		};




		$scope.setCurrentAsset = function (param) {
			$http.post('/lms/index.php?r=restdata', {
				endpoint: 'invite_to_watch',
				params: {
					action: 'initial',
					id_asset: param
				}
			}).then(function ($result) {
				if ($result.data.success == true) {
					$scope.populate($result.data);
				} else {
					$('#invite_to_watch').doceboNotice($result.data.message, 'error');
				}
				$scope.req = 1;
			}, function ($error) {
				console.log($error);
			});
		};

		$scope.invitedPeople = 0;
		$scope.inviteToWatchForm = null;
		$scope.inviteForm = false;
		$scope.loadingContent = true;
		$scope.showLoadingF = true;

		/**
		 * Call invite users and proccess whole flow
		 * @returns {inviteToWatch_L2.inviteController.$scope}
		 */
		$scope.inviteUsers = function (param, inviter) {
			$scope.inviteForm = true;
			$scope.loadingContent = false;
			$scope.showLoadingF = true;
			$scope.invitedPeople = 0;
			$http.post('/lms/index.php?r=restdata', {
				endpoint: 'invite_to_watch',
				params: {
					action: 'invite',
					id_asset: param,
					id_inviter: inviter,
					items: $scope.prepare
				}
			}).then(function ($result) {
				if ($result.data.success == true) {
					$scope.inviteSuccess($result.data);
				} else {
					$scope.invitedPeople = 0;
					$scope.inviteToWatchForm = null;
					$scope.inviteForm = false;
					$scope.loadingContent = true;
					$scope.showLoadingF = true;
					$('#invite_to_watch').doceboNotice($result.data.message, 'error');
				}
			}, function ($error) {
				$scope.inviteFail($error)
			});

			return this;
		};

		$scope.change = function (param) {
			if (($scope.model.length > 2 && $scope.rest_query == false) || $scope.model.length == 0) {
				$scope.rest_query = true;
				$scope.req = 0;
				$http.post('/lms/index.php?r=restdata', {
					endpoint: 'invite_to_watch',
					params: {
						action: 'search',
						search: $scope.model,
						id_asset: param,
						items: $scope.prepare
					}
				}).then(function ($result) {
					if ($result.data.success == true) {
						$scope.populate($result.data);

					} else {
						$('#invite_to_watch').doceboNotice($result.data.message, 'error');
					}
					$scope.req = 1;
					$scope.rest_query = false;
				}, function ($error) {
					console.log($error);
				});
			}
		}

		$scope.keypress = function ($event, param) {
			if ($event.keyCode == 13) {
				$event.preventDefault();
			}
		};


		$scope.hasError = function (obj) {
			//console.log(typeof (obj.message));
			if (typeof (obj.message) === 'undefined') {
				return false;
			}
			return true;
		}

		/**
		 * Close the modal
		 * @returns {inviteToWatch_L2.inviteController.$scope}
		 */
		$scope.close = function () {
			$('#app7020-invite-dialog').dialog2('close');
			$(".modal").remove();
			$(".modal-backdrop").remove();
			return this;
		};
		/**
		 * Flow when invited users are successfull done
		 * @param {Object} data
		 * @returns {inviteToWatch_L2.inviteController.$scope}
		 */
		$scope.inviteSuccess = function (data) {
			$scope.inviteForm = true;
			$scope.loadingContent = true;
			$scope.showLoadingF = false;
			$scope.invitedPeople = data.invitedUsers;

			$compile($("#modalContainer"))($scope);
			//	$scope.$apply();
			setTimeout(function () {
				$scope.close();
			}, 3000);
			return this;
		}

		/**
		 * Notify the usser for an error
		 * @param {text} errorThrown
		 * @returns {inviteToWatch_L2.inviteController.$scope}
		 */
		$scope.inviteFail = function (errorThrown) {
			alert(errorThrown);
			return this;
		}
		/**
		 *
		 * @param {type} selectedItem
		 * @param {type} invited_items
		 * @returns {inviteToWatch_L33.$scope}
		 */
		$scope.handleSelection = function (selectedItem, param) {
			$scope.req = 0;
			$http.post('/lms/index.php?r=restdata', {
				endpoint: 'invite_to_watch',
				params: {
					action: 'subscribe',
					invite: {
						id: selectedItem.id,
						name: selectedItem.name.length > 0 ? selectedItem.name : selectedItem.username,
						type: selectedItem.type,
						typeId: selectedItem.typeId
					},
					id_asset: param,
					items: $scope.prepare
				}
			}).then(function ($result) {
				if ($result.data.success == true) {
					$scope.populate($result.data);
				} else {
					$('#invite_to_watch').doceboNotice($result.data.message, 'error');
				}
				$scope.req = 1;
			}, function ($error) {
				console.log($error);
			});
			$scope.model = '';
			$scope.current = 0;
			$scope.selected = true;

			return this;
		};
		$scope.showForm = function () {
			return $scope.inviteForm;
		}
		$scope.showLoading = function () {
			return $scope.loadingContent;
		}
		$scope.showComplete = function () {
			return $scope.showLoadingF;
		}

		/**
		 *
		 * @param {type} selectedItem
		 * @param {type} invited_items
		 * @returns {inviteToWatch_L33.$scope}
		 */
		$scope.unhandleSelection = function (key, param) {

			delete $scope.prepare[key];

			$scope.req = 0;

			$http.post('/lms/index.php?r=restdata', {
				endpoint: 'invite_to_watch',
				params: {
					action: 'unsubscribe',
					id_asset: param,
					items: $scope.prepare
				}
			}).then(function ($result) {
				if ($result.data.success == true) {
					$scope.populate($result.data);
				} else {
					$('#invite_to_watch').doceboNotice($result.data.message, 'error');
				}
				$scope.req = 1;
			}, function ($error) {
				console.log($error);
			});



			$scope.model = '';
			$scope.current = 0;
			$scope.selected = true;

			return this;
		};
		/**
		 *
		 * @param {type} invited_items
		 * @returns {inviteToWatch_L33.$scope}
		 */
		$scope.unhandleAll = function (param) {
			$scope.req = 0;
			$scope.prepare = [];

			$http.post('/lms/index.php?r=restdata', {
				endpoint: 'invite_to_watch',
				params: {
					action: 'clear_all',
					id_asset: param
				}
			}).then(function ($result) {
				if ($result.data.success == true) {
					$scope.populate($result.data);
				} else {
					$('#invite_to_watch').doceboNotice($result.data.message, 'error');
				}
				$scope.req = 1;
			}, function ($error) {
				console.log($error);
			});
			$scope.model = '';
			$scope.current = 0;
			$scope.selected = true;

			return this;
		};
		$scope.current = 0;
		$scope.selected = true; // hides the list initially

		/**
		 *
		 * @param {type} index
		 * @returns {Boolean}
		 */
		$scope.isCurrent = function (index) {
			return $scope.current == index;
		};
		/**
		 * Set index to current
		 * @param {type} index
		 * @returns {inviteToWatch_L33.$scope}
		 */
		$scope.setCurrent = function (index) {
			$scope.current = index;
			return this;
		};



	}

	inviteController.$inject = ['$scope', 'dataFeed', '$timeout', '$compile', '$rootScope', '$http']; //Injecting dependencies
	return inviteController; //Return Class definition
});