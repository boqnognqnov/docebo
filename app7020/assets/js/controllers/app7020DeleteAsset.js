define(['app7020PDeleteAsset', 'modules/notification'], function () {
	var deleteSingleton = new require('app7020PDeleteAsset');
	var deleteSingleton = new deleteSingleton;



	$(document).on('click touchstart', '[data-method="delete"]', function (e) {
		var title = $(this).attr('data-dialog-title');
		var id = $(this).data('content-id');
		deleteSingleton.init(id);
	});


	var Controller = function ($scope) {

		$scope.init = function (param) {
            $(document).find('.app7020-confirm-delete-modal-dialog .save-dialog').addClass('disabled');

			$(document).on('click', '.app7020-confirm-delete-modal-dialog .save-dialog', function (e) {
				if ($('input[name="agreeCheck"]').is(':checked')) {
 					e.preventDefault();
					$('#app7020-confirm-delete-modal-dialog').dialog2('close');
					deleteSingleton.sendToDelete(param);
				}
				else {
					//$('#app7020-confirm-delete-modal-dialog').doceboNotice(Yii.t('app7020', 'Please check "Yes, I want to proceed!"'), 'error');
                    return;
				}

			});
		}
        $(document).find('.app7020-confirm-delete-modal-dialog').on('click', '[type="checkbox"]', function () {
            if ($(this).prop('checked')) {
                $(document).find('.modal-footer .save-dialog').removeClass("disabled");
            } else {
                $(document).find('.modal-footer .save-dialog').addClass("disabled");
            }
        });
	}


    Controller.$inject = ['$scope'];
	return Controller;
});