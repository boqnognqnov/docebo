define([], function () {
	function questionDeletePlugin(element, options) {
		this.$element = null;
		this.$options = null;
		this.init(element, options);
	}

	questionDeletePlugin.prototype = {
		/* Version of module */
		VERSION: '0.1',
		/* Default options for init */
		DEFAULTS: {
			selector: '[data-method="question_delete"]',
			idAttr: 'data-idObject',
			debug: false
		},
		/* Init() to prepare options for using */
		init: function (element, options) {
			/* Set current element to instance */
			this.$element = $(element);

			/* Merge default options with internal unique and external options */
			this.$options = $.extend({}, this.DEFAULTS, options);

			/* Console inited options for current instance */
			if (this.getOptions().debug) {
				console.log("questionDeletePlugin.init() :: ", this.getOptions());
			}
			this.attachListeners();
		},
		/* Getter for default options */
		getDefaults: function () {
			return this.DEFAULTS;
		},
		/* Getter for merged init options */
		getOptions: function () {
			return this.$options;
		},
		/* Listeners */
		attachListeners: function () {
			var $this = this;
			if (this.$element.length) {
				this.$element.on('click', $.proxy(this.modalPrepare, this));
			} else {
				$(document).on('click', this.getOptions().selector, $.proxy(this.modalPrepare, this));
			}
			$(document).on('dialog2.before-open', '#app7020-confirm-delete-modal-dialog-question', function (){
				$(this).closest('.modal').unbind('click').on('click', 'a.save-dialog', function(e){
					e.preventDefault();
					if($(this).closest('.modal').find('#confirm[type="checkbox"]').is(':checked')){
						var id = $(this).closest('.modal').find('#confirmDeleteQuestionDialog').attr('data-content-id');
						
						$('#app7020-confirm-delete-modal-dialog-question').dialog2('close');
						$this.sendToDelete(id);
					}
				});
			});
		},
		modalPrepare: function (e) {
			var id = $(e.currentTarget).closest('['+this.getOptions().idAttr+']').attr(this.getOptions().idAttr);
			/* With high priority is if current element have attribute */
			if ($(e.currentTarget).is('['+this.getOptions().idAttr+']')){
				id = $(e.currentTarget).attr(this.getOptions().idAttr);
			}
			/* Dialog2 setting params */
			var dialog = {
				title:	Yii.t('app7020', 'Delete'),
				url:	app7020Options.askGuruController + '/axShowConfirmDeleteQuestionModal',
				id:		'app7020-confirm-delete-modal-dialog-question',
				class:	'app7020-confirm-delete-modal-dialog',
				data:	{questionId: id}
			};
			this.modalOpen(dialog);
		},
		modalOpen: function (dialog){
			openDialog(false, dialog.title, dialog.url, dialog.id, dialog.class, 'POST', dialog.data);
//			$("html, body").animate({scrollTop: 0});
		},
		sendToDelete: function(id){
			var response = $.ajax({
				method: "POST",
				url: app7020Options.askGuruController + '/AxDeleteQuestion',
				data: {
					id: id,
				}
			});

			response.complete(function(data){
				//refresh combolist
				var combolistid = $('#filterActions').attr('data-for');
				if($('#filterActions').length >0 ){
					$.each(combolistid.split(','), function (index, value) {
						$('#combo-list-form #' + value + '.list-view').comboListView('updateListView');
					});
				} else {
					$('#combo-list-form .list-view').comboListView('updateListView');
					var newQuestions = $('#newQuestionsCounter').text();
					newQuestions = parseInt(newQuestions) -1;
					$("#newQuestionsCounter").text(newQuestions);
					$("#newQuestionsCounter").css("display", "inline");
				}


			});
		}
	}

	/* Multi instances Plugin definition */
	function questionDeletePluginDefinition(option) {
		var options = typeof option == 'object' && option;
		if (this.length)
			return this.each(function () {
				var $this = $(this);
				var data = $this.data('questionDeletePlugin');
				if (!data)
					$this.data('questionDeletePlugin', (data = new questionDeletePlugin(this, options)))

				if (typeof option == 'string')
					data[option]()
			});
		if (!this.length)
			return new questionDeletePlugin(this, options);
	}

	/* Append this module to jQuery API */
	$.fn.app7020QuestionDeletePlugin = questionDeletePluginDefinition;
});