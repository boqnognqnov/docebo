define(function (require) {

	var app7020Cue = function (options) {

		this.container_id = null;
		this.container = null;
		this.player = null;
		this.cue = {
			/**
			 * Cuepoint for new view of video content
			 * @returns {app7020_L17.app7020Cue.prototype.cue}
			 */
			app7020ViewContent: function (cue) {
				/**
				 * Call an AJAX method to create record in content_history
				 * table (mark the content as viewed)
				 */
				this.container_id = $('.f-player-container > div').attr('id');
				var result = $.ajax({
					url: '/lms/index.php?r=restdata',
					method: "POST",
					data: {
						endpoint: 'asset_view',
						params: {
							id_asset: $('#'+this.container_id).attr('data-content-id')
						}
					},
					dataType: 'json',
				});
				result.complete(function (response) {
					result.done(function (response) {
						$('#totalContentViews').html(response);
					});
				});
				return this;
			},
			setTooltip: function (cue) {
				if (typeof (cue.hash) == 'undefined') {
					throw "Please set hash param for tooltip";
				}
				var object = $('#docebo-flowplayer-tooltip-' + cue.hash);
				if (cue.action === 'show') {
					object.switchClass('docebo-flowplayer-hidden-tooltip', 'docebo-flowplayer-visable-tooltip');
				} else {
					object.switchClass('docebo-flowplayer-visable-tooltip', 'docebo-flowplayer-hidden-tooltip');
				}
			}
		};

		/**
		 * Bind new quepoint based on  constuctor
		 * @param {sting} callback
		 * @returns {app7020_L17.app7020Cue.prototype}
		 */
		this.bind = function () {
			var that = this;
			this.player.bind("cuepoint", function (e, api2, cue) {
				if (typeof (cue.type) != 'undefined' && cue.type !== '') {
					that.cue[cue.type](cue);
				}
				return that;
			});
		};


		/**
		 * Init of cuepoint object
		 * @param {type} callback
		 * @returns {app7020_L17.app7020Cue.prototype}
		 */
		this.init = function () {
			if ($('.f-player-container').length > 0) {
				this.container_id = $('.f-player-container > div').attr('id');
				this.container = document.getElementById(this.container_id);
				
				this.player = require('flowplayerLib')(this.container);
				this.bind();
			}
			return this;
		};
		return this.init(options);
	}

	return app7020Cue;

});