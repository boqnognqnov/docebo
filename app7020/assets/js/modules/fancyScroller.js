require.config({
	baseUrl: app7020Options.assetUrl + '/js',
	paths: {
		lookingforlib: '/themes/spt/js/lookingfor/jquery.lookingfor.min',
		scrollerLib: '/themes/spt/js/jquery.mCustomScrollbar.concat.min'
	},
	shim: {
		lookingforlib: {
			deps: ['scrollerLib']
		},
		scrollerLib: {
			exports: 'mCustomScrollbar'
		}
	}
});
define(['require',
	'scrollerLib',
	'lookingforlib'], function (require) {
	fancyScroller = function () {
		/**
		 * @type {String}
		 */
		this.theme = 'dark-3';
		/**
		 * Contain the Dom indificator by ID or Class
		 * @type {string}
		 */
		this.dom = '.app7020PeerMessages > ul';
		/**
		 * set new dom for Scroller instance
		 * @param String domString
		 * @returns {app7020_L19.app7020Class.prototype.app7020FancyScroller}
		 */
		this.setDom = function (domString) {
			this.dom = domString;
			return this;
		};
		/**
		 * Update scroller to last item  on any cal to action
		 * @returns {app7020_L19.app7020Class.prototype.app7020FancyScroller}
		 */
		this.update = function () {
			$(this.dom).mCustomScrollbar('update');
			$(this.dom).mCustomScrollbar('scrollTo', 'bottom');
			return this;
		};
		/**
		 * Init the scroller
		 * @param  {Object}
		 * @returns {app7020_L19.app7020Class.prototype.app7020FancyScroller}
		 */
		this.init = function (dom, index) {
			this.dom = dom;
			$(this.dom).mCustomScrollbar({
				theme: this.theme,
				scrollbarPosition: "outside",
				autoHideScrollbar: false
			});
			return this;
		};
	}
	return fancyScroller;
});