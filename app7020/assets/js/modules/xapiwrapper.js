'use strict';
var is_require = false;
if (typeof require === "function" && typeof require.specified === "function" && require.specified("jquery")) {
	is_require = true;
	require.config({
		paths: {
			xApiJs: '/themes/spt/js/xapiwrapper.min',
			xApiWrapper: 'xApiWrapper/xApiWrapper'
		},
		shim: {
			xApiWrapper: {
				deps: ['xApiJs']
			}
		}
	});


	define(['xApiWrapper'], function () {

		/**
		 * Init
		 * @param element
		 * @param options
		 */
		var app7020xApiWrapper = function (options) {
			this.$options = {};

			this.DEFAULTS = {
				config: {
					endpoint: app7020Options.xApiEndpoint,
					user: app7020Options.user.username,
					password: app7020Options.user.pass
				},
				statement: {
					actor: {
						name: app7020Options.user.firstname + ' ' + app7020Options.user.lastname,
						mbox: "mailto:" + app7020Options.user.email
					},
					verb: {
						id: "http://id.tincanapi.com/verb/viewed",
						display: {
							"en-US": "viewed"
						}
					},
					object: {
						id: "", //  url of asset
						definition: {
							name: {
								"en-US": "" //  asset Title
							}
						}
					},
					registration: '' // registration
				}
			};


			this.init = function (options) {
				this.$options = $.extend(true, this.DEFAULTS, options);
				return this;
			};

			this.getDefaults = function () {
				return this.DEFAULTS;
			};

			this.getOptions = function () {
				return this.$options;
			};

			this.send = function () {
				// Make needed config
				ADL.XAPIWrapper.changeConfig(this.getDefaults().config);
				// Send Request
				var result = ADL.XAPIWrapper.sendStatement(this.getDefaults().statement);
				if (result.xhr.statusText === "OK") {
					console.log('Everything is OK with xAPI request');
				} else {
					console.log('Error with xAPI request');
				}
			};

			this.init(options);
		};
		return app7020xApiWrapper;
	});
}