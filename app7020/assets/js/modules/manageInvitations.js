define([], function () {
	/* Initialize app7020AnalyticStatistics */
	var analyticsWatched;

	$('#manageInvitationStatistic').app7020AnalyticStatistics({
		/* Define ajax url to get data on update */
		ajaxUrl: app7020Options.axAnalyticsController + '/AxGetStatisticsData',
		/* On init attach custom javascript */
		init: function (instance) {
			var ctx = $("#jsCounter").get(0).getContext("2d");
			var customTooltip = $('#jsCounterTooltip');
			var canvasJsCounter = $('canvas#jsCounter');
			var watchedCount = 0;
			var notWatchedCount = 0;
			if (parseInt(canvasJsCounter.attr('data-watched')) + parseInt(canvasJsCounter.attr('data-notWatched')) == 0) {
				watchedCount = 0.1;
				notWatchedCount = 0.1;
			}
			if (parseInt(canvasJsCounter.attr('data-watched')) > 0) {
				watchedCount = parseInt(canvasJsCounter.attr('data-watched'));
			}
			if (parseInt(canvasJsCounter.attr('data-notWatched')) > 0) {
				notWatchedCount = parseInt(canvasJsCounter.attr('data-notWatched'));
			}
			var data = [
				{value: watchedCount, color: '#5EBE5E', highlight: '#5EBE5E', label: 'watched'},
				{value: notWatchedCount, color: '#333', highlight: '#333', label: 'notWatched'}
			];
			var dataOptions = {
				percentageInnerCutout: 70,
				segmentShowStroke: false,
				customTooltips: function (tooltip) {
					if (!tooltip) {
						customTooltip.find('.count').html('');
						customTooltip.removeClass('watched notWatched').css({opacity: 0});
						return;
					}
					var label = $.parseJSON(tooltip.text).label;
					var value = Math.floor($.parseJSON(tooltip.text).value);
					if (label == 'watched') {
						customTooltip.addClass('watched').find('.count').html(value);
						customTooltip.css({opacity: 1});
					}
					if (label == 'notWatched') {
						customTooltip.addClass('notWatched').find('.count').html(value);
						customTooltip.css({opacity: 1});
					}
				},
				tooltipTemplate: '{"label": "<%=label%>", "value": "<%=value%>"}',
			};
			analyticsWatched = new Chart(ctx).Doughnut(data, dataOptions);
		},
		/* On after update replace new counters with counters from |data| */
		afterAjaxUpdate: function (id, ajaxOptions, data) {
			var container = $('#' + id);
			var totalInvited = container.find('.totalInvitedPeople .count');
			var globalWatch = container.find('.globalWatchRate .count');
			var avgTime = container.find('.avgReactionTime .count');

			totalInvited.html(data.totalInvitedPeople);
			globalWatch.html(data.globalWatchRate + '<span>%</span>');
			avgTime.html(data.avgReactionTime);
			/* Update Chart Circle */
			var watchedCount = 0;
			var notWatchedCount = 0;
			if (parseInt(data.watched) + parseInt(data.notWatched) == 0) {
				watchedCount = 0.1;
				notWatchedCount = 0.1;
			}
			if (parseInt(data.watched) > 0) {
				watchedCount = parseInt(data.watched);
			}
			if (parseInt(data.notWatched) > 0) {
				notWatchedCount = parseInt(data.notWatched);
			}
			analyticsWatched.segments[0].value = watchedCount;
			analyticsWatched.segments[1].value = notWatchedCount;
			analyticsWatched.update();
		}
	});
});