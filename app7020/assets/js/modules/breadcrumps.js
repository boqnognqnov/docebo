
'use strict';
define(function () {

	function breadcrumps(options) {

		this.commonBread = {
			question: '',
			asset: ''
		};
		this.init(options);
	}

	breadcrumps.prototype = {
		init: function (options) {
			switch (options.type) {
				case 'question':
					$(document).find('#navigation .breadcrumb .breadcrumpjs').remove();
					return this.setCurrentBreadcrump('question', this.questionBreadcrump(options.breadcrumps));
					break;
				case 'asset':
					$(document).find('#navigation .breadcrumb .breadcrumpjs').remove();
					return $(document).find('#navigation .breadcrumb').append('<li class="breadcrumpjs"><span class="divider"> &rsaquo; </span> ' + options.breadcrumps + ' </li>');
					break;
				default:
					$(document).find('#navigation .breadcrumb .breadcrumpjs').remove();
					this.setCurrentBreadcrump('asset','');
					this.setCurrentBreadcrump('question','');
					break;
			}
		},
		truncate: function (text, limit, append) {
			if (typeof text !== 'string') {
				return '';
			}
			if (typeof append === 'undefined') {
				append = '...';
			}
			var parts = text.split(' ');
			if (parts.length > limit && parts.length > 0) {
				// loop backward through the string
				for (var i = parts.length - 1; i > -1; --i) {
					// if i is over limit, drop this word from the array
					if (i + 1 > limit) {
						parts.length = i;
					}
				}
				// add the truncate append text
				parts.push(append);
			}else{
				var limit_text = 80;
				var cuttedtext = text.substring(0, limit_text);
				return (text.length > limit_text) ? cuttedtext + '...' : text;
			}
			// join the array back into a string
			return parts.join(' ');
		},
		getCurrentBr: function (page) {
			if (typeof (page) != 'undefined') {
				if (page == 'asset') {
					return this.commonBread.asset;
				} else {
					return this.commonBread.question;
				}
			}
		},
		setCurrentBreadcrump: function (word, type) {
			if (type == 'asset') {
				this.commonBread.asset = word;
			} else {
				this.commonBread.question = word;
			}
			return this;
		},
		questionBreadcrump: function(param){
			param = param.replace(/<[^>]+>/gm, "");
			return $(document).find('#navigation .breadcrumb').append('<li class="breadcrumpjs"><span class="divider"> &rsaquo; </span> ' + this.truncate(param, 10, '...') + '</li>');
		}
		
	};
	return breadcrumps;
}
);
