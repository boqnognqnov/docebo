'use strict';

require.config({
	baseUrl: app7020Options.assetUrl + '/js',
	paths: {
		lookingforlib: '/themes/spt/js/lookingfor/jquery.lookingfor.min',
	}
});
define('jquery', [], function () {
	return jQuery;
});
require(['modules/fancyScroller', 'lookingforlib', 'modules/appendCSS'], function (fancyScrollerLib, l, appendCSS) {

	new appendCSS('/themes/spt/css/jquery.mCustomScrollbar.css');
	//reload 
	jQuery(function ($) {
		$('#channnelsCheckList').lookingfor({
			input: $('input.channelWidgetSearch'),
			items: 'div'
		});
	});
	jQuery(document).on('dialog2.opened', '.app7020-asset-modal-dialog', function () {
		jQuery(document).on('dialog2.ajax-complete', '.app7020-asset-modal-dialog', function () {
			jQuery(function ($) {
				$('#channnelsCheckList').lookingfor({
					input: $('input.channelWidgetSearch'),
					items: 'div'
				});
				var fancyScroller = new fancyScrollerLib;
				fancyScroller.init($('#channnelsCheckList'), 0);

			});
		});
	});
	jQuery('body').on('focus', 'input.channelWidgetSearch', function (){
			jQuery(function ($) {
				$('#channnelsCheckList').lookingfor({
					input: $('input.channelWidgetSearch'),
					items: 'div'
				});
				var fancyScroller = new fancyScrollerLib;
				fancyScroller.init($('#channnelsCheckList'), 0);
		});
	});
});