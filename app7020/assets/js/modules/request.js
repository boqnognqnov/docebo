'use strict';
define([], function () {

	/**
	 * 
	 * @param {type} context
	 * @returns {request.requestAnonym$0}
	 */
	function request(context) {
		if (typeof (context) !== 'undefined') {
			context = false;
		}
 		return {
			ajax: function (endpoint, data, onSuccess, onError, $type) {
				
				if(typeof $type == 'undefined') {
					$type = 'json';
				}
				/**
				 * url fot http request
				 * @type @String
				 */
				this.endpoint = '';
				/**
				 * sended data to endpoint
				 * @type 
				 */
				this.data = {};
				/**
				 * callback on Success stage
				 * @returns {@callback onSuccess}
				 */
				this.onSuccess = function () {
				};
				/**
				 * Callback On error stage
				 * @returns {@callback onError)}
				 */
				this.onError = function () {
				}

				this.init = function (endpoint, data, onSuccess, onError) {
					this.endpoint = endpoint;
					this.data = data;
 					this.onSuccess = onSuccess;
					this.onError = onError;
					this.response();
				};
				/**
				 * General request callback to endpoint
				 * @returns {jqXHR}
				 */
				this.response = function () {
					var that = this;
 					var request = $.ajax({
						url: that.endpoint,
						data: that.data,
						type: "POST",
						dataType: $type,
						success: that.onSuccess,
						error: that.onError
					});
					return request;
				};
				return this.init(endpoint, data, onSuccess, onError);
			}
		}
	}
	return request;
});





