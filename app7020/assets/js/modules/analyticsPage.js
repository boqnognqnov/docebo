'use strict';
define(['angular'], function (angular) {
	function analyticsClass(options) {
		return this.init(options);
	}

	analyticsClass.prototype = {
		options: {
			debug: false
		},
		init: function (options) {
			if (this.options.debug) {
				console.log('Analytics Controller: initOptions');
				console.log(this.getOptions());
			}

			this.listeners();
		},
		listeners: function () {
			var $this = this;
			/* Single resend invitation */
			$('#analyticsContainer').on('click', '.inivitationsGridView .app7020_invitation_resend', function (e) {
				$this.openConfirmModal($(e.delegateTarget).attr('data-idObject'), $(this).attr('data-invited-id'), false, 'resend');
			});
			/* Single delete invitation */
			$('#analyticsContainer').on('click', '.inivitationsGridView .app7020_invitation_delete', function (e) {
				$this.openConfirmModal($(e.delegateTarget).attr('data-idObject'), $(this).attr('data-invited-id'), false, 'cancel');
			});
			/* Mass resend invitations */
			$('#analyticsContainer').on('click', '.action_buttons_combolist .app7020-buttons-blue', function (e) {
				$this.openConfirmModal($(e.delegateTarget).attr('data-idObject'), false, true, 'resend');
			});
			/* Mass delete invitations */
			$('#analyticsContainer').on('click', '.action_buttons_combolist .app7020-buttons-red', function (e) {
				$this.openConfirmModal($(e.delegateTarget).attr('data-idObject'), false, true, 'cancel');
			});
			/* Change invitations watched/not watched */
			$('#analyticsContainer').on('change', '#notWatchedCheckbox', function (e) {
				$('#userman-users-management-grid').yiiGridView('update');
			});
			this.confirmModalListeners();
			/* Show invite dialog */
			$('#analyticsContainer').on('click', '#showInviteToWatch', function (e) {
				var idContent = parseInt($("#analyticsContainer").data('idobject'));
				$('#app7020-confirm-invite-modal-dialog').dialog2("close");
				var url = app7020Options.inviteToWatch + '/index&id=' + idContent;
				var id = 'app7020-invite-dialog';
				var clases = 'app7020-invite-dialog wide';
				var data = {};
				var title = Yii.t('app7020', 'Invite to Watch');
				openDialog(false, title, url, id, clases, 'GET', data);
			});
			$('#analyticsContainer').on('click', '#userman-users-management-grid .items .empty .gridViewOpenInvitePeople', function (e) {
//				app7020.inviteUsers.openDialog($(e.delegateTarget).attr('data-idObject'));
				var idContent = parseInt($("#analyticsContainer").data('idobject'));
				$('#app7020-confirm-invite-modal-dialog').dialog2("close");
				var url = app7020Options.inviteToWatch + '/index&id=' + idContent;
				var id = 'app7020-invite-dialog';
				var clases = 'app7020-invite-dialog wide';
				var data = {};
				var title = Yii.t('app7020', 'Invite to Watch');
				openDialog(false, title, url, id, clases, 'GET', data);
			});
			var selected = [];
			var tempidarr = [];
			$(document).on('dialog2.closed', '#app7020-invite-dialog', function () {
				$('#userman-users-management-grid').yiiGridView('update');
				$('#manageInvitationStatistic').app7020AnalyticStatistics('update');
			});
		},
		/**
		 * Resend invitatibns
		 * @param {Number} assetId
		 * @param {Number} userId
		 * @param {type} mass
		 * @returns {analyticsController_L2.analyticsClass.prototype}
		 */
		ajaxResendInvitation: function (assetId, userId, mass) {
			var params = {};
			if (mass === 'true') {
				params.url = app7020Options.axAnalyticsController + '/AxReinviteAll';
				params.data = {idContent: assetId};
			} else {
				params.url = app7020Options.axAnalyticsController + '/AxReinvite';
				params.data = {idContent: assetId, idInvited: userId};
			}
			$.ajax({
				url: params.url,
				method: "POST",
				data: params.data,
				dataType: 'json',
			}).done(function (response) {
				if (response.result == true) {
					$('#userman-users-management-grid').yiiGridView('update');
					$('#manageInvitationStatistic').app7020AnalyticStatistics('update');
				}
			});
			return this;
		},
		ajaxDeleteInvitation: function (assetId, userId, mass) {
			var params = {};
			if (mass === 'true') {
				params.url = app7020Options.axAnalyticsController + '/AxRemoveAllInvitations';
				params.data = {idContent: assetId};
			} else {
				params.url = app7020Options.axAnalyticsController + '/AxRemoveInvitation';
				params.data = {idContent: assetId, idInvited: userId};
			}
			$.ajax({
				url: params.url,
				method: "POST",
				data: params.data,
				dataType: 'json',
			}).done(function (response) {
				if (response.result == true) {
					$('#userman-users-management-grid').yiiGridView('update');
					$('#manageInvitationStatistic').app7020AnalyticStatistics('update');
				}
			});
		},
		openConfirmModal: function (assetId, userId, mass, method) {
			/* Dialog2 setting params */
			var dialog = {
				url: app7020Options.axAnalyticsController + '/axShowConfirmInvitationModal',
				id: 'app7020-confirm-invitation-dialog',
				class: 'app7020-confirm-invitation-dialog',
				data: {assetId: assetId, userId: userId, mass: mass, method: method}
			};
			if (method === 'resend') {
				dialog.title = Yii.t('app7020', 'Resend invitations');
			} else {
				dialog.title = Yii.t('app7020', 'Cancel invitation');
			}
			openDialog(false, dialog.title, dialog.url, dialog.id, dialog.class, 'POST', dialog.data);
		},
		confirmModalListeners: function () {
			var $this = this;
			$(document).on('dialog2.opened', '#app7020-confirm-invitation-dialog', function () {
				$(this).closest('.modal').unbind('click').on('click', 'a.save-dialog', function (e) {
					e.preventDefault();
					if ($(this).closest('.modal').find('#confirm[type="checkbox"]').is(':checked')) {
						var container = $(this).closest('.modal').find('#confirmInvitationDialog');
						var assetId = container.attr('data-assetId');
						var userId = container.attr('data-userId');
						var mass = container.attr('data-mass');
						var method = container.attr('data-method');
						if (mass === 'true') {
							if (method === 'resend') {
								/* MASS RESEND */
								$this.ajaxResendInvitation(assetId, userId, mass);
							} else {
								/* MASS CANCEL */
								$this.ajaxDeleteInvitation(assetId, userId, mass);
							}
						} else {
							if (method === 'resend') {
								/* SINGLE RESEND */
								$this.ajaxResendInvitation(assetId, userId, mass);
							} else {
								/* SINGLE CANCEL */
								$this.ajaxDeleteInvitation(assetId, userId, mass);
							}
						}
						$('#app7020-confirm-invitation-dialog').dialog2('close');
					}
				});
			});
		},
	}

	var analytics = new analyticsClass;
});