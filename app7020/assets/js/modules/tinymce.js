define(function () {
	function buildTinymce(options) {
		this.$selector = null;
		this.$options = null;
		this.init(options);
	}

	buildTinymce.prototype = {
		/* Version of module */
		VERSION: '0.2',
		/* Default options for init */
		DEFAULTS: {
			selector: 'textarea.tinymce',
			plugins: ["moxiemanager image link autoresize"],
			toolbar: "bold italic custom-save underline link image",
			autoresize_min_height: 55,
			autoresize_bottom_margin : 10,
			language: yii.language,
			menubar: false,
			statusbar: false
		},
		/* Init() to prepare options for using */
		init: function (options) {
			/* Merge default options with internal unique and external options */
			this.$options = $.extend({}, this.DEFAULTS, options);
		},
		/* Getter for default options */
		getDefaults: function (optionName) {
			if (typeof optionName !== 'undefined') {
				return this.DEFAULTS[optionName];
			}
			return this.DEFAULTS;
		},
		/* Getter for merged init options */
		getOptions: function (optionName) {
			if (typeof optionName !== 'undefined') {
				return this.$options[optionName];
			}
			return this.$options;
		},
		/* This method will convert all textareas with class "tinymce" to tinymce editor */
		buildDefault: function () {
			TinyMce.attach(this.getDefaults('selector'), this.getDefaults());
		},
		/* Variable "selector" is a String who need to specify element 
		 * and after that init tinymce editor
		 */
		build: function (selector, additionalOptions) {
			if (typeof selector === 'string' && selector.length != 0)
				this.$options.selector = selector;
			if(this.getInstanceBySelector(this.getOptions('selector')))
				return false;
			TinyMce.attach(this.getOptions('selector'), $.extend({}, this.getOptions(), additionalOptions));
		},
		/* Variable "selector" is a String who need to specify element 
		 * and after that return editor object
		 */
		getInstanceBySelector: function (selector) {
			if (typeof selector === 'string' && selector.length != 0)
				return tinyMCE.get($(selector).attr('id'));
			return false;
		}
	}

	return buildTinymce;
});

