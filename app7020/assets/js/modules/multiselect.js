function multiselect(url){

	this.url = null;



	this.init = function(url){
		this.url = $(url);
		return this;
	}

	this.init();
}

/* Multi instances Plugin definition */
function multiselectDefinition() {

	return this.each(function () {
		var $this = $(this);
		var data = $this.data('multiselect');
		if (!data)
			$this.data('multiselect', (data = new multiselect()))
	});
}

'use strict';

if (typeof require === "function" && typeof require.specified === "function" && require.specified("jquery")) {
	require.config({
		baseUrl: app7020Options.assetUrl + '/js',
		paths: {},
		shim: {}
	});
	define("jquery", [], function ( ) {
		return jQuery;
	});
	define([], function () {
		jQuery.fn.doceboMultiselect = multiselectDefinition;
	});
}
else {
	$.fn.doceboMultiselect = multiselectDefinition;
}
