'use strict';

define(function (require) {
	$(document).on('click', '.docebo-alert .docebo-got-it a, .splash-screen .docebo-got-it .gotit', function () {
		var ajaxURL = false;
		var $el = $(this);
		var $dom = $el.closest('.alert');

		if ($el.data('ajax-url') === 'false' || typeof ($el.data('ajax-url')) == 'undefined') {
			ajaxURL = $gotItUrl;
		}
		else {
			ajaxURL = $el.data('ajax-url');
		}
		$.ajax({
			url: ajaxURL,
			data: {method: $dom.attr('id')},
			type: "POST",
			dataType: "json",
			success: function () {
				if($el.data('close-modal') == true){
					$(document).find('.modal-splash-screen').remove();
					$(document).find('.modal-backdrop').remove();
				}
			},
			error: function () {

			}
		});
		$dom.alert('close');
	});

	$(document).on('click', '#app7020-splash-screen  .contact,#app7020-splash-screen .schedule', function () {
		$(document).find('.modal-splash-screen').remove();
		$(document).find('.modal-backdrop').remove();
		openDialog(null,'','/lms/index.php?r=site/axHelpDesk&YII_CSRF_TOKEN='+window.YII_CSRF_TOKEN+'&only_sales=1','helpdesk-modal','helpdesk-modal users-info-dialogs');
	});


	$(document).on('click', '.alert:not(.docebo-got-it-container) .docebo-alert-close a', function () {
		var $el = $(this);
		var $dom = $el.closest('.alert');
		$dom.alert('close');
	});




	/**
	 * Build notification Alert DOM for any parent
	 * @param {String} element
	 * @param {String} message
	 * @param {String} type
	 * @returns {notification}
	 */
	function notification(element, message, type, options) {

		this.message = null;
		this.message_type = 'warning';
		this.$element = null;
		this.messageDOM = null;
		this.options = {
			isGotIt: false,
			id: false
		}


		this.init = function (element, message, type, options) {

			if (typeof (element) == 'undefined') {
				throw '[NOTIFICATION] - Is not set element';
			}
			if (typeof (message) == 'undefined') {
				throw '[NOTIFICATION] - Is not set message';
			}
			if (typeof (type) == 'undefined') {
				this.setErrorType('warning');
			}
			else if (['warning', 'error', 'success'].indexOf(type) < 0) {
				throw '[NOTIFICATION] - The notice type is not allowed';
			}
			else {
				this.setErrorType(type);
			}


			if (typeof (options) == 'object') {
				this.options = $.extend({}, this.options, options);
			}


			this.$element = $(element);
			this.setMessage(message);
			var alert = this.getMessageDOM();


// 		$('.docebo_alert.error').remove();
			if (this.$element.find('.docebo-alert.error').hasClass('alert')) {
				this.$element.find('.docebo-alert.error').replaceWith(alert);
			}
			else if (this.$element.find('.docebo-alert.success').hasClass('alert')) {
				this.$element.find('.docebo-alert.success').replaceWith(alert);
			}

			else {
				alert.prependTo(this.$element);
			}

			var dissmiss = alert.find('.close');
			var that = this;
			dissmiss.click(function () {
				if (alert.hasClass('docebo-got-it-container') && typeof (that.options.id) == 'string' && typeof (that.options.id) != 'undefined') {
					new request(false).ajax($gotItUrl, {method: that.options.id});
				}
			});

			return this;
		};

		this.getMessage = function () {
			if ($.isArray(this.message)) {
				return this.message.join('<br />');
			}
			return this.message;
		};

		/**
		 * Set new Message into object
		 * @param {String} message
		 * @returns {notification_L3.notification}
		 */
		this.setMessage = function (message) {
			if (typeof (message) == 'undefined') {
				throw '[NOTIFICATION] - Can\'t use set of messages with "undefined" argument into setter ';
			}
			this.message = message;
			return this;
		};

		/**
		 * Set the notice type for current instance 
		 * @param {String} Set the notice types. Allowed should be error, warning and success
		 * @returns {notification_L3.notification}
		 */
		this.setErrorType = function (type) {
			if (typeof (type) == 'undefined') {
				throw '[NOTIFICATION] - Can\'t use set of message type  with "undefined" argument into setter ';
			}
			this.message_type = type;
			return this;
		};
		/**
		 * Return the current message (notice) type
		 * @returns {String}
		 */
		this.getErrorType = function () {
			return this.message_type;
		};

		this.getMessageDOM = function () {

			var that = this;
			var gotItText = null;
			if (!that.options.isGotIt) {
				gotItText = '&times;';
			}
			else {
				gotItText = 'GOT IT!';
			}


			//build general DOM
			var general_classes = that.options.isGotIt ? ' docebo-got-it-container ' : ''
			general_classes += this.getErrorType();
			general_classes += ' alert docebo-alert ';

			var parentDIV = $('<div />', {
				'class': general_classes,
				'id': that.options.id
			}).append(
					$('<div />', {
						'class': 'table-row'
					})
					.append(
							that.options.isGotIt ? (
									$('<div />', {
										'class': 'table-cell docebo-alert-icon',
										'html': '<i class="fa fa-exclamation-circle"></i>'
									})
									) : ''
							)
					.append(
							$('<div />', {
								'class': 'table-cell'
							})
							.append(
									$('<p />', {
										'class': '',
										'html': that.getMessage()
									})
									)
							)
					.append(
							$('<div />', {
								'class': (that.options.isGotIt ? 'docebo-got-it' : '') + ' table-cell docebo-alert-close '
							})
							.append(
									$('<a  />', {
										'class': 'close',
//									'data-dismiss': 'alert',
										html: gotItText
									})
									)
							)
					);
			return parentDIV;
		};

		this.init(element, message, type, options)
	}

	/* Multi instances Plugin definition */
	function notificationDefinition(message, type, options) {

		return this.each(function () {
			var $this = $(this);
			var data1 = new notification(this, message, type, options);
			var alertDom = $this.find(data1.messageDOM);
			var data = alertDom.data('notification');

			if (!data)
				alertDom.data('notification', data1);
		});
	}


	jQuery.fn.doceboNotice = notificationDefinition;
});

 