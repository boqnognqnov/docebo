/**
 * Append CSS to HEAD
 * @returns {appendCSS_L5.loadCss}
 */
'use strict';
define(function () {
	function loadCss(url) {
		var link = document.createElement("link");
		link.type = "text/css";
		link.rel = "stylesheet";
		link.href = url;
		document.getElementsByTagName("head")[0].appendChild(link);
	}
	return loadCss;
});