<?php

class InviteToWatchController extends App7020BaseController {

	public $id;
	public $modal = true;

	function init() {

		parent::init();
	}

	public function actionIndex() {
		$idContent = Yii::app()->request->getParam('id', false);
		if ((int) $idContent > 0) {
			$assetsURL = App7020Helpers::getAssetsUrl();
			App7020Helpers::registerApp7020Options();

//			Yii::app()->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl . '/js/angularJs/angular.js', CClientScript::POS_END);
//			Yii::app()->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl . '/js/angularJs/angular-resource.js', CClientScript::POS_END);
//			Yii::app()->getClientScript()->registerScriptFile($assetsURL . '/js/controllers/inviteToWatch.js', CClientScript::POS_END);
			$this->renderPartial('index', array('idContent' => $idContent), false, true);
		}
	}

	function actionAxInviteUsers() {
		$invitedPeople = array();
//		$idContent = $_SESSION['app7020ContentIdForInvitation'];
		$idContent = Yii::app()->request->getParam('idContent', false);
		$idInviter = Yii::app()->request->getParam('idInviter', false);
		$items = json_decode(Yii::app()->request->getParam('invited_items', false));
		foreach ($items as $item) {
			if ($item->invited == 3 && $item->type == 2 && !in_array($item->id, $invitedPeople)) {
				$invitedPeople[] = $item->id;
				$invitationObject = new App7020Invitations();
				$invitationObject->idInvited = $item->uid;
				$invitationObject->idContent = $item->idcontent;
				$invitationObject->idInviter = $item->idinviter;
				$invitationObject->save(false);
			}
		}
//		Yii::app()->event->raise('App7020Invitation', new DEvent($this, array('users' => $invitedPeople,'contentId' => $idContent,'idInviter' => $idInviter)));

		$redirect = Yii::app()->request->getParam('redirectToEditMode', 0);

		die(json_encode(array('invitedPeople' => count($invitedPeople), 'contentId' => $idContent, 'redirect' => $redirect)));
	}

	public function actionAxGetUsersForInvite() {

		$idContent = Yii::app()->request->getParam('id', false);
		$filter = Yii::app()->request->getParam('filter', false);

		if ((int) $idContent > 0) {
			$users = App7020Invitations::getVisibleUsers($idContent);
			$users = array_slice($users, 0, 2000);
			die(json_encode($users));
		} else {
			die(json_encode(array(
				'status' => false,
				'message' => Yii::t('ápp7020', 'Somethimg went wrong!')
			)));
		}
	}

}
