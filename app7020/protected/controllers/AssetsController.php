<?php

/**
 * 
 *
 */
class AssetsController extends App7020BaseController {
    //for internal usage

    /**
     * Instance ot storage class. Needed when get any s3 content
     * @var CS3StorageSt
     */
    protected $storage;

    /**
     *
     * @see CController::filters()
     */
    public function filters() {
        return array(
            'accessControl'
        );
    }

    /**
     *
     * @see CController::init()
     */
    public function init() {
		Yii::app()->tinymce;
        $this->storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);
        Yii::app()->getClientScript()->registerCoreScript('bbq');
        parent::init();
    }

    /**
     *
     * @see CController::accessRules()
     */
    public function accessRules() {
        $res = array();
        $res[] = array(
            'allow',
            'users' => array(
                '*'
            ),
            'actions' => array(
                'login'
            )
        );
        $res[] = array(
            'allow',
            'users' => array(
                '@'
            ),
            'actions' => array(
                'logout',
                'index'
            )
        );
        return $res;
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        /* depricated in new version  */
        App7020Helpers::registerApp7020UploadSettings();

        $amdController = array();
        $amdController['data']['data-main'] = $this->getAssetsUrl() . '/js/contribute';
        Yii::app()->event->raise('registerAMDScript', new DEvent($this, $amdController));


        $this->breadcrumbs = array(
            Yii::t('app7020', 'Contribute')
        );

		$showBrowseButton = true;
//        /* If Open Page with question reference */
		$idQuestion = Yii::app()->request->getParam('idQuestion', false);
//        if ($idQuestion) {
//            $requestModel = App7020QuestionRequest::model()->findByAttributes(array('idQuestion' => $idQuestion, 'idExpert' => Yii::app()->user->idst));
//            if (!$requestModel->id || $requestModel->idContent) {
//                $this->redirect(Docebo::createApp7020Url('assets/index'));
//            }
//        }
		$slackShare = Yii::app()->request->getParam('shareUrl', false);
        /* Tab links */
        $navigationMenuLinks = array(
            array(
                'tabTitle' => Yii::t('app7020', 'To finalize'),
                'tabContent' => $this->renderPartial('activeList', array(), true),
            ),
            array(
                'tabTitle' => Yii::t('app7020', 'Submitted for peer review'),
                'tabContent' => $this->renderPartial('finishList', array(), true),
            ),
			array(
				'tabTitle' => Yii::t('app7020', 'Published'),
                'tabContent' => $this->renderPartial('publishList', array(), true),
			)
        );

        $this->render('index', array(
            'showBrowseButton' => $showBrowseButton,
            'navigationMenuLinks' => $navigationMenuLinks,
			'hasRequest' => $idQuestion,
			'slackShare'=>$slackShare
        ));
    }

    /**
     * Delete local LMS file 
     * @param App7020Content $content model with contents
     * @return boolean
     */
    private function _deleteLocalFile(App7020Assets $content) {
        try {
            $file = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $content->filename;
            return @unlink($file);
        } catch (Exception $ex) {
            Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
            return false;
        }
    }

    /**
     * Delete folder from S3 Colection with all included files
     * @param App7020Content $content model with contents
     * @return boolean
     */
    private function _deleteCloudFile(App7020Assets $content) {
        try {
            $storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);
            return $storage->removeFolder(App7020Helpers::getAmazonInputKey($content->filename));
        } catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
            return false;
        }
    }

    /**
     * Save file per file type and prevent elastic transcoder if is not needed
     * @param App7020Content $content
     * @return boolean
     */
    private function _proccessFileStoring(App7020Assets $content) {
        // Save to S3
        $tempFolder = Docebo::getUploadTmpPath();
        $tempFilePath = realpath($tempFolder . DIRECTORY_SEPARATOR . $content->filename);
        $storage = $this->storage;

        try {
            if ($storage->store($tempFilePath)) {
                $content->conversion_status = $this->_proccessSuccessFileStatus($content);

                if (App7020Assets::isConvertableDocument($content->filename)) {
                    $cloudConvertObject = new CloudConvert();
                    $cloudConvertObject->convertFromS3ToS3($content->id);
                }

                if ((int) $content->contentType == App7020Assets::CONTENT_TYPE_IMAGE) {
                    if ($coreAsset->id != $content->idThumbnail && (int) $content->idThumbnail > 0) {
                        CoreAsset::model()->findByPk($content->idThumbnail)->delete();
                    }
                    $coreAsset = new CoreAsset();
                    $coreAsset->type = CoreAsset::TYPE_APP7020;
                    $coreAsset->sourceFile = $tempFilePath;

                    if ($coreAsset->save()) {
                        $content->idThumbnail = $coreAsset->id;
                    }
                }
                return $content->save(false);
            } else {
                return false;
            }
        } catch (Exception $ex) {
        	Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
            return false;
        }
    }

    /**
     * Return question id by Referer from AJAX
     * @return boolean|integer
     */
    public function _detectQuestionRefererId() {

        if (Yii::app()->request->getParam('idQuestion', false)) {
            $id = Yii::app()->request->getParam('idQuestion');
        } else if (isset($_SERVER['HTTP_REFERER'])) {
            $url = $_SERVER['HTTP_REFERER'];
            preg_match('/idQuestion=[0-9A-Za-z]*/', $url, $matches);
            $value = explode("=", $matches[0]);
            $id = $value[1];
        } else {
            return false;
        }
        return (int) $id;
    }

    public function actionShowSplashScreen() {
        //$message = Yii::app()->params['maintenance_mode']['message'];
        //$dateFrom = Yii::app()->params['maintenance_mode']['from'];
        $smb = false;
        if(Docebo::isSMB()){
            $smb = true;
        }
        $this->renderPartial('_splashModal', array('smb'=>$smb));
    }

    public function actionSplashSetGotIt() {

        //if(App7020SplashScreenTracking::splashCheck()){
            $gotit = new CoreSettingUser();
            $gotit->id_user = Yii::app()->user->idst;
            $gotit->path_name = 'cs_splash_screen';
            $gotit->value = 1;
            $gotit->save(false);
            return true;
       // }
        //return false;
    }


}
