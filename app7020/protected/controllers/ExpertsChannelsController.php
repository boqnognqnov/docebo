<?php

class ExpertsChannelsController extends App7020BaseController {

	public function init() {

		parent::init();
		$themeUrl = Yii::app()->theme->baseUrl;
		Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/combogridview.js', CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/user_management.js', CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/confirm/jquery.confirm.js', CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerCssFile($themeUrl . '/css/user_management.css');
		Yii::app()->getClientScript()->registerCssFile($themeUrl . '/css/DoceboCGridView.css');
		$ft = new FancyTree();
		Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree-all.js', CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree.docebo.js', CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerCssFile($ft->getAssetsUrl() . '/skin-docebo/ui.fancytree.css');
		$this->getClientScript()->registerCssFile($themeUrl . '/css/DoceboCGridView.css');
		UsersSelector::registerClientScripts();
		$amdController = array();
		$amdController['data']['data-main'] = $this->getAssetsUrl() . '/js/app7020Activity/main';
		Yii::app()->event->raise('registerAMDScript', new DEvent($this, $amdController));
	}

	/**
	 * Return id by Referer from AJAX
	 * @return boolean|integer
	 */
	private function _detectExpertRefererId() {

		if (Yii::app()->request->getParam('expertId', false)) {
			$id = Yii::app()->request->getParam('expertId');
		} else if (isset($_SERVER['HTTP_REFERER'])) {
			$url = $_SERVER['HTTP_REFERER'];
			preg_match('/expertId=[0-9A-Za-z]*/', $url, $matches);
			$value = explode("=", $matches[0]);
			$id = $value[1];
		} else {
			return false;
		}
		return (int) $id;
	}

	public function actionIndex() {
		// Register Fancytree JS/CSS
		
		$amdController = array();
		$amdController['data']['data-main'] = $this->getAssetsUrl() . '/js/experts/main';
		
		Yii::app()->event->raise('registerAMDScript', new DEvent($this, $amdController));

		$this->breadcrumbs = array(
			Yii::t('app7020', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
			Yii::t('app7020', 'Experts & Channels'),
		);

		if (isset($_REQUEST['selectAll'])) {
			$dataProvider = App7020Experts::sqlDataProvider();
			$dataProvider->pagination = false;
			$keys = $dataProvider->keys;
			foreach ($keys as $key => $value) {
				$keys[$key] = (int) $value;
			}
			$this->sendJSON(CJSON::encode($keys));
			Yii::app()->end();
		}

		// Autocomplete request?
		if (isset($_REQUEST['autocomplete'])) {
			$dataProvider = App7020Experts::sqlDataProvider(array('search_input' => $_REQUEST['term']));
			$result = array();
			foreach ($dataProvider->data as $user) {
				$result[] = array('label' => $user['expertNames'] . ' (' . $user['username'] . ')', 'value' => $user['expertNames']);
			}
			$this->sendJSON($result);
		}

		$selectedColumns = $this->getSelectedColumns();
		$selectedColumns['channels'] = array(
			'type' => 'raw',
			'header' => Yii::t('app7020', "Assigned channels"),
			'value' => 'App7020Experts::renderChannelsLink($data["idExpert"], $data["channelsCount"]);',
			'htmlOptions' => array(
				'class' => 'td-cchannels text-center'
			),
			'headerHtmlOptions' => array(
				'width' => '132px;',
				'class' => 'text-center'
			)
		);
		$selectedColumns['delete'] = array(
			'type' => 'raw',
			'value' => 'App7020Experts::renderDeleteLink($data["expertId"]);',
			'htmlOptions' => array(
				'class' => 'td-cdelete text-center'
			),
			'headerHtmlOptions' => array(
				'width' => '40px;',
			)
		);

		$usersGridId = "userman-users-management-grid";
		$userManAction = Yii::app()->controller->id . "/" . Yii::app()->controller->action->id;
		if (isset($_REQUEST['ajax']) && ($_REQUEST['ajax'] == $usersGridId)) {
			$dataProvider = App7020Experts::sqlDataProvider();
			$this->renderPartial('_usersGrid', array(
				'usersGridId' => $usersGridId,
				'selectedColumns' => $selectedColumns,
				'dataProvider' => $dataProvider
			));
		} else {
			$this->render('index', array(
				'selectedColumns' => $selectedColumns,
				'usersGridId' => $usersGridId,
				'userManAction' => $userManAction
			));
		}
	}

	public function actionChannels() {
		// Register Fancytree JS/CSS
		$amdController = array();
		$amdController['data']['data-main'] = $this->getAssetsUrl() . '/js/expertsChannels/main';
		Yii::app()->event->raise('registerAMDScript', new DEvent($this, $amdController));

		$this->breadcrumbs = array(
			Yii::t('app7020', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
			Yii::t('app7020', 'Experts & Channels'),
			Yii::t('app7020', 'Assign channels'),
		);

		$expertId = $this->_detectExpertRefererId();
		$userId = App7020Experts::model()->findByAttributes(array('id' => $expertId))->idUser;

		if (isset($_REQUEST['comboListViewSelectAll'])) {
			$dataProvider = App7020ChannelExperts::getExpertsChannels($expertId, array('pagination' => false));
			$keys = $dataProvider->keys;
			foreach ($keys as $key => $value) {
				$keys[$key] = (int) $value;
			}
			$this->sendJSON(CJSON::encode($keys));
			Yii::app()->end();
		}

		$expertName = CoreUser::model()->getFullNameById($userId);
		$provider = App7020ChannelExperts::getExpertsChannels($expertId);
		$listParams = array(
			'massiveActions' => array('deleteExperts' => Yii::t('standard', '_DEL')),
			'listId' => "channelsComboListView",
			'dataProvider' => $provider,
			'restyleAfterUpdate' => false,
			'listItemView' => 'app7020.protected.views.expertsChannels._channelsListViewItem',
			'columns' => 1,
			'template' => '<div class="expertLabel">' . Yii::t('app7020', 'Assigned channels to:') . ' <span>' . $expertName . '</span></div>'
			. '{items}{pager}{summary}'
		);
		if (isset($_REQUEST['ajax'])) {
			$this->widget('common.widgets.ComboListView', $listParams);
			Yii::app()->end();
		}


		$this->render('channels', array('listParams' => $listParams, 'expertId' => $expertId));
	}

	public function getSelectedColumns() {
		$columnsArray = array(
			array(
				'class' => 'CCheckBoxColumn',
				'id' => 'userman-grid-checkboxes'
			),
			array(
				'name' => 'username',
				'header' => Yii::t('standard', "_USERNAME"),
				'htmlOptions' => array
					(
					'class' => 'td-username'
				),
				'headerHtmlOptions' => array
					(
					'class' => 'td-username'
				)
			),
			array(
				'name' => 'expertNames',
				'header' => Yii::t('app7020', "Expert full name"),
				'htmlOptions' => array
					(
					'class' => 'td-expertnames'
				),
				'headerHtmlOptions' => array
					(
					'class' => 'td-expertnames'
				)
			),
			array(
				'name' => 'email',
				'header' => Yii::t('standard', "_EMAIL"),
				'htmlOptions' => array
					(
					'class' => 'td-email'
				),
				'headerHtmlOptions' => array
					(
					'class' => 'td-email'
				)
			),
		);
		return $columnsArray;
	}

}
