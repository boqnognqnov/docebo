<?php

/**
 * Description of QuestionsController
 *
 * @author Kristian
 */
class AskTheExpertController extends App7020BaseController {

	public function init() {
		// Load TinyMCE
		Yii::app()->tinymce;
		parent::init();


		$amdController = array();
		$amdController['data']['data-main'] = $this->getAssetsUrl() . '/js/askTheExpert/main';
		Yii::app()->event->raise('registerAMDScript', new DEvent($this, $amdController));
	}

	/**
	 *
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		$res = array();
		$res[] = array(
			'allow',
			'users' => array(
				'*'
			),
			'actions' => array(
				'login'
			)
		);
		$res[] = array(
			'allow',
			'users' => array(
				'@'
			),
			'actions' => array(
				'logout',
				'index'
			)
		);
		return $res;
	}

	public function actionIndex() {
		$themeUrl = Yii::app()->theme->baseUrl;
		$this->getClientScript()->registerCssFile($this->getAssetsUrl() . '/css/mce_mention/autocomplete.css');
		$this->getClientScript()->registerCssFile($this->getAssetsUrl() . '/css/mce_mention/rte-content.css');
		$this->breadcrumbs = array(
			Yii::t('app7020', 'Questions and Answers') => Docebo::createApp7020Url('askTheExpert/index').'#/',
		);
		$this->render('layout', array());
	}

	/**
	 * Returns view HTML for angular JS
	 * @param $viewName
	 */
	public function actionView() {
		$viewName = Yii::app()->request->getParam('view_name');
		$this->getClientScript()->registerCssFile($this->getAssetsUrl() . '/css/mce_mention/rte-content.css');
		$this->getClientScript()->registerCssFile($this->getAssetsUrl() . '/css/mce_mention/autocomplete.css');
		try {
			$this->renderPartial('app7020.protected.views.askTheExpert.' . $viewName);
		} catch (Exception $e) {
			echo "<h4>" . $e->getMessage() . "</h4>";
			Yii::app()->end();
		}
	}

	public function actionAxGetUsers() {
		$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
		$name = Yii::app()->request->getParam('name', false);
		$idQuestion = Yii::app()->request->getParam('id_question', false);

		$questionModel = App7020Question::model()->findByPk($idQuestion);
		if (!$questionModel->id) {
			echo json_encode(array());
			return;
		}
		$users = App7020Question::getUsersWhichCanViewAQuestion($questionModel, $name);
		$array = array();

		// This counter is added temporary to demonstrate to set the last user to expert
		$counter = 0;
		$numUsers = count($users);
		foreach ($users as $user) {
			$counter++;
			$array[] = array(
				'id' => $user['idst'],
				"name" => $user["firstname"] . ' ' . $user["lastname"],
				'avatar' => $user['avatar'] ? $storage->absoluteBaseUrl . '/avatar/' . $user['avatar'] : '',
				'username' => ltrim($user['userid'], "/"),
				'firstname' => $user['firstname'],
				'lastname' => $user['lastname'],
				'isExpert' => $counter == $numUsers ? true : false,
			);
		}

		echo json_encode($array);
	}

	public function actionGetFilters() {
		$filters = array(
			array(
				'name' => 'Sort:',
				'typename' => 'sort',
				'spantype' => 'span3',
				'options' =>
				array(
					array(
						'id' => 0,
						'value' => 0,
						'type' => 'radio',
						'label' => 'Date',
						'selected' => true
					),
					array(
						'id' => 1,
						'value' => 1,
						'type' => 'radio',
						'label' => 'Answers number',
						'selected' => false
					),
					array(
						'id' => 2,
						'value' => 2,
						'type' => 'radio',
						'label' => 'Views',
						'selected' => false
					)
				)
			),
			array(
				'name' => 'Show',
				'typename' => 'show',
				'spantype' => 'span3',
				'options' => array(
					array(
						'id' => 0,
						'value' => 0,
						'type' => 'radio',
						'label' => 'All questions',
						'selected' => true
					),
					array(
						'id' => 1,
						'value' => 1,
						'type' => 'radio',
						'label' => 'Open questions',
						'selected' => false
					),
					array(
						'id' => 2,
						'value' => 2,
						'type' => 'radio',
						'label' => 'Closed questions',
						'selected' => false
					),
					array(
						'id' => 3,
						'value' => 3,
						'type' => 'radio',
						'label' => 'New questions',
						'selected' => false
					)
				)
			),
			array(
				'name' => 'Question type',
				'typename' => 'qtype',
				'spantype' => 'span6',
				'options' => array(
					array(
						'id' => 0,
						'value' => 0,
						'type' => 'radio',
						'label' => 'All types',
						'selected' => true
					),
					array(
						'id' => 1,
						'value' => 1,
						'type' => 'radio',
						'label' => 'Linked to users contributed assets',
						'subtitle' => 'Videos, documents, links and others',
						'selected' => false
					),
					/*
					array(
						'id' => 2,
						'value' => 2,
						'type' => 'radio',
						'label' => 'Linked to Learning Objects',
						'subtitle' => 'Formal learning objects that are part of an E-Learning course',
						'selected' => false
					)
					*/
				)
			)
		);

		echo json_encode($filters);
	}

}
