<?php

/**
 * Description of GuruDashboardController
 *
 * @author Kristian
 */
class GuruDashboardController extends App7020BaseController {

	public function init() {
		// Load TinyMCE
		Yii::app()->tinymce;
		parent::init();


		$amdController = array();
		$amdController['data']['data-main'] = $this->getAssetsUrl() . '/js/app/app_expertTasks';
		Yii::app()->event->raise('registerAMDScript', new DEvent($this, $amdController));
	}
	
	public function actionIndex() {
		$themeUrl = Yii::app()->theme->baseUrl;
		$this->getClientScript()->registerCssFile($this->getAssetsUrl() . '/css/mce_mention/autocomplete.css');
		$this->getClientScript()->registerCssFile($this->getAssetsUrl() . '/css/mce_mention/rte-content.css');
		$this->getClientScript()->registerCssFile($themeUrl . '/css/loading-bar.min.css');
		$this->breadcrumbs = array(
			Yii::t('app7020', 'My expert tasks') => Docebo::createApp7020url('askTheExpert/index'),
		);
		$this->render('layout', array());
	}
	
	/**
	 * Returns view HTML for angular JS
	 * @param $viewName
	 */
	public function actionView() {
		$viewName = Yii::app()->request->getParam('view_name');
		try {
			$this->renderPartial('app7020.protected.views.guruDashboard.' . $viewName);
		} catch (Exception $e) {
			echo "<h4>" . $e->getMessage() . "</h4>";
			Yii::app()->end();
		}
	}
//	public function init() {
//		parent::init();
//		if (App7020Experts::isUserExpert(Yii::app()->user->idst) == false && Yii::app()->user->getIsGodadmin() == false) {
//			$this->adminHomepageRedirect();
//		}
//		Yii::import('app7020.protected.controllers.KnowledgeLibraryController');
//		$themeUrl = Yii::app()->theme->baseUrl;
//		$this->getClientScript()->registerScriptFile($themeUrl . '/js/jquery.mCustomScrollbar.concat.min.js', CClientScript::POS_END);
//		$this->getClientScript()->registerCssFile($themeUrl . '/css/' . (YII_DEBUG ? 'jquery.mCustomScrollbar.min.css' : 'jquery.mCustomScrollbar.css'));

//		$ft = new FancyTree();
//		Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree-all.js', CClientScript::POS_HEAD);
//		Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree.docebo.js', CClientScript::POS_HEAD);
//		Yii::app()->getClientScript()->registerCssFile($ft->getAssetsUrl() . '/skin-docebo/ui.fancytree.css');
		// Load TinyMCE
//		Yii::app()->tinymce;
//
//		Flowplayer::registerCssFile(Flowplayer::SKIN_FUNCTIONAL);
//
//		$amdController = array();
//		$amdController['data']['data-main'] = $this->getAssetsUrl() . '/js/filterTopics/main';
//		Yii::app()->event->raise('registerAMDScript', new DEvent($this, $amdController));
//	}

//	public function actionIndexOld() {
//		if (!empty($_GET['tab'])) {
//			$_SESSION['got'] = $_GET['tab'];
//			$this->redirect(array('guruDashboard/index'));
//		}
//
//		if (!empty($_SESSION['got'])) {
//			$_GET['tab'] = $_SESSION['got'];
//			unset($_SESSION['got']);
//		}
//		$this->breadcrumbs = array(
//			Yii::t('app7020', 'My Expert Dashboard'),
//		);
//
//		// FILTER PARAMS AND COMBO LIST PARAMS
//		$listView_ajax = Yii::app()->request->getParam('ajax', false);
//
//
//		// ******************************************** //
//		// ------------ SATISFY COMBO LIST ------------ //
//		// ******************************************** //
//		if (stripos($listView_ajax, 'activityRequestsSatisfy_list') !== false) {
//			$requestStatus = Yii::app()->request->getParam('dropdownSwitcher', 0) + 1;
//		} else {
//			$requestStatus = 1;
//			$requestsExtensions = 'takeInCharge,ignore';
//		}
//
//		if ($requestStatus == 3) {
//			$dataProvider_satisfy = App7020IgnoreList::getIgnoredExpertsRequests(
//							Yii::app()->user->idst, array('field' => 'created', 'vector' => 'DESC'), true, array('comboListViewId' => 'activityRequestsSatisfy_list')
//			);
//		} else {
//			$dataProvider_satisfy = App7020QuestionRequest::getExpertsRequests(
//							Yii::app()->user->idst, $requestStatus = $requestStatus, array('field' => 'created', 'vector' => 'DESC'), true, array('comboListViewId' => 'activityRequestsSatisfy_list')
//			);
//		}
//
//		$listViewParams_satisfy = array(
//			'listId' => 'activityRequestsSatisfy_list',
//			'dataProvider' => $dataProvider_satisfy,
//			'template' => '<div class="backdropV2"></div>{items}{pager}{summary}',
//			'beforeAjaxUpdate' => "$('body #'+$(this).attr('ajaxUpdate')[0]+' .backdropV2').addClass('fadePlusSpinIconEffect');",
//			'afterAjaxUpdate' => "$('body #'+$(this).attr('ajaxUpdate')[0]+' .backdropV2').removeClass('fadePlusSpinIconEffect');",
//			'viewData' => array('listGotItType' => 'guru_dashboard_satisfy_gotit'),
//		);
//
//		switch ($requestStatus) {
//			case 1:
//				$requestsExtensions = 'takeInCharge,ignore';
//				break;
//			case 2:
//				$requestsExtensions = 'tookInCharge';
//				break;
//			case 3:
//				$requestsExtensions = 'takeInChargeDisabled,unIgnore,gotIt';
//				break;
//			default :
//				$requestsExtensions = 'takeInCharge,ignore';
//				break;
//		}
//
//		if (stripos($listView_ajax, 'activityRequestsSatisfy_list') !== false) {
//			$this->widget('common.widgets.app7020QuestionListView', array('listParams' => $listViewParams_satisfy, 'extensions' => $requestsExtensions));
//			Yii::app()->end();
//		}
//
//
//		// ******************************************* //
//		// ------------ ASSETS COMBO LIST ------------
//		// ******************************************* //
//
//		$pageSize_assets = 5;
//		if (stripos($listView_ajax, 'assetsToReviewApprove_list') !== false) {
//
//			$requestStatus = Yii::app()->request->getParam('dropdownSwitcher', 1) + 1;
//		} else {
//			$requestStatus = 2;
//			$assetStatusParam_assets = array(
//				App7020Assets::CONVERSION_STATUS_FINISHED,
//				App7020Assets::CONVERSION_STATUS_INREVIEW,
//				App7020Assets::CONVERSION_STATUS_APPROVED,
//				App7020Assets::CONVERSION_STATUS_UNPUBLISH
//			);
//		}
//
//
//		if ($requestStatus == 1) {
//			$assetStatusParam_assets = array(
//				App7020Assets::CONVERSION_STATUS_FINISHED,
//			);
//		}
//
//
//		if ($requestStatus == 2) {
//			$assetStatusParam_assets = array(
//				App7020Assets::CONVERSION_STATUS_FINISHED,
//				App7020Assets::CONVERSION_STATUS_INREVIEW,
//				App7020Assets::CONVERSION_STATUS_APPROVED,
//				App7020Assets::CONVERSION_STATUS_UNPUBLISH
//			);
//		}
//
//
//
//		if ($requestStatus == 3) {
//
//			$dataProvider_assets = App7020IgnoreList::getIgnoredExpertsAssets(
//							Yii::app()->user->idst, array('field' => 'created', 'vector' => 'DESC'), true, array('comboListViewId' => 'assetsToReviewApprove_list')
//			);
//		} else {
//			$dataProvider_assets = App7020Assets::getExpertsReviewedAssets(
//							Yii::app()->user->idst, $assetStatus = $assetStatusParam_assets, $orderField = array('field' => 'created', 'vector' => 'DESC'), $asArrayDataProvider = true, $providerConfig = array('comboListViewId' => 'assetsToReviewApprove_list', 'pagination' => array('pageSize' => $pageSize_assets))
//			);
//		}
//
//		$pageSize_assets = 5;
//
//		switch ($requestStatus) {
//			case 1:
//				$requestsExtensions = 'topics,ignore';
//				break;
//			case 2:
//				$requestsExtensions = 'topics,ignore';
//				break;
//			case 3:
//				$requestsExtensions = 'topics,unIgnore,gotIt';
//				break;
//			default :
//				$requestsExtensions = 'topics,ignore';
//				break;
//		}
//
//
//		$listViewParams_assets = array(
//			'listId' => 'assetsToReviewApprove_list',
//			'dataProvider' => $dataProvider_assets,
//			'template' => '<div class="backdropV2"></div>{items}{pager}{summary}',
//			'beforeAjaxUpdate' => "$('body #'+$(this).attr('ajaxUpdate')[0]+' .backdropV2').addClass('fadePlusSpinIconEffect');",
//			'afterAjaxUpdate' => "$('body #'+$(this).attr('ajaxUpdate')[0]+' .backdropV2').removeClass('fadePlusSpinIconEffect');",
//		);
//		if (stripos($listView_ajax, 'assetsToReviewApprove_list') !== false) {
//			$this->widget('common.widgets.app7020ContentListView', array('listParams' => $listViewParams_assets, 'gridColumns' => 1, 'extensions' => $requestsExtensions, 'titleAsLink' => true, 'linkToEditMode' => true));
//			Yii::app()->end();
//		}
//
//
//		// ******************************************* //
//		// ----------- QUESTIONS COMBO LIST ----------
//		// ******************************************* //
//		$pageSize_questions = 5;
//
//		if (stripos($listView_ajax, 'questionsYouHaveToAnswer_list') !== false) {
//			$questionStatus_questions = Yii::app()->request->getParam('dropdownSwitcher', 0) + 1;
//		} else {
//			$questionStatus_questions = 1;
//		}
//
//		if ($questionStatus_questions == 3) {
//			//replace with new data provider from ignore list
//			$dataProvider_questions = App7020IgnoreList::getIgnoredExpertsQuestions(
//							Yii::app()->user->idst, $orderField = array('field' => 'created', 'vector' => 'DESC'), $asArrayDataProvider = true, $providerConfig = array('comboListViewId' => 'questionsYouHaveToAnswer_list', 'pagination' => array('pageSize' => $pageSize_questions))
//			);
//		} else {
//			$dataProvider_questions = App7020Question::getExpertsQuestions(
//							Yii::app()->user->idst, $questionStatus = $questionStatus_questions, $orderField = array('field' => 'created', 'vector' => 'DESC'), $asArrayDataProvider = true, $providerConfig = array('comboListViewId' => 'questionsYouHaveToAnswer_list', 'pagination' => array('pageSize' => $pageSize_questions))
//			);
//		}
//
//		switch ($questionStatus_questions) {
//			case 1:
//				$requestsExtensions = 'topics,ignore';
//				break;
//			case 2:
//				$requestsExtensions = 'topics,ignore';
//				break;
//			case 3:
//				$requestsExtensions = 'topics,unIgnore,gotIt';
//				break;
//			default :
//				$requestsExtensions = 'topics,ignore';
//				break;
//		}
//
//		$listViewParams_questions = array(
//			'listId' => 'questionsYouHaveToAnswer_list',
//			'dataProvider' => $dataProvider_questions,
//			'template' => '<div class="backdropV2"></div>{items}{pager}{summary}',
//			'beforeAjaxUpdate' => "$('body #'+$(this).attr('ajaxUpdate')[0]+' .backdropV2').addClass('fadePlusSpinIconEffect');",
//			'afterAjaxUpdate' => "$('body #'+$(this).attr('ajaxUpdate')[0]+' .backdropV2').removeClass('fadePlusSpinIconEffect');",
//			'viewData' => array('listGotItType' => 'guru_dashboard_questions_gotit'),
//		);
//
//		if (stripos($listView_ajax, 'questionsYouHaveToAnswer_list') !== false) {
//			$this->widget('common.widgets.app7020QuestionListView', array('listParams' => $listViewParams_questions, 'extensions' => $requestsExtensions));
//			Yii::app()->end();
//		}
//
//
//		/* RENDER MAIN VIEW */
//		$this->render('index_old', array(
//			'listViewParams_assets' => $listViewParams_assets,
//			'listViewParams_questions' => $listViewParams_questions,
//			'listViewParams_satisfy' => $listViewParams_satisfy,
//			'countResults' => $dataProvider_questions->getTotalItemCount(),
//		));
//	}

//	public function actionQuickQuestionView() {
//		$idQuestion = (int) Yii::app()->request->getParam('idQuestion', false);
//		$hidePlayer = (bool) Yii::app()->request->getParam('hidePlayer', false);
//		$newAnswer = trim(Yii::app()->request->getParam('replacedAnswerFromTextarea', false));
//		$clearAnswer = trim(preg_replace('!\s+!', ' ', strip_tags(preg_replace("/&#?[a-z0-9]+;/i", "", $newAnswer))));
//
//		// ---------- UPDATE VIEW QUESTION ---------
//		$questionHistory = new App7020QuestionHistory();
//		$questionHistory->idQuestion = $idQuestion;
//		$questionHistory->idUser = Yii::app()->user->idst;
//		$questionHistory->secureSave();
//
//		// ------------ ADD NEW ANSWERS ------------
//		if (!empty($clearAnswer)) {
//			$answerModel = new App7020Answer();
//			$answerModel->idQuestion = $idQuestion;
//			$answerModel->idUser = Yii::app()->user->idst;
//			$answerModel->content = preg_replace('!\s+!', ' ', html_entity_decode($newAnswer));
//			$answerModel->bestAnswer = 1; // 1 -> No ||| 2 -> Yes
//			$answerModel->save();
//		}
//
//		// ---------------- ANSWERS ----------------
//		$dataProvider_answers = App7020Answer::sqlDataProvider(array('questionId' => $idQuestion, 'pagination' => false, 'order' => 'created Asc'));
//		$listItemView_answers = 'app7020.protected.views.guruDashboard._comboListViewItem_answers';
//		$countOfAnswers = count(App7020Question::model()->findByPk($idQuestion)->answers);
//		// ----------- ANSWERS COMBO LIST ----------
//		$listViewParams_answers = array(
//			'listId' => 'listView_answers',
//			'dataProvider' => $dataProvider_answers,
//			'listItemView' => $listItemView_answers,
//			'hiddenFields' => array(
//				'idQuestion' => $idQuestion,
//				'replacedAnswerFromTextarea' => ''
//			),
//			'noItemViewContainer' => true,
//			'disableMassSelection' => true,
//			'disableMassActions' => true,
//			'disableCheckBox' => true,
//			'disableFiltersWrapper' => true,
//			'noYiiListViewJS' => true,
//			'itemsCssClass' => 'mCustomScrollbar',
//			'template' => '<div class="backdropV2"></div>{items}{summary}',
//			'beforeAjaxUpdate' => "app7020.guruDashboard.listViewBeforeUpdate($(this));",
//			'afterAjaxUpdate' => "app7020.guruDashboard.listViewAfterUpdate($(this));",
//		);
//		$this->renderPartial('app7020.protected.views.guruDashboard._quickQuestionView', array(
//			'idQuestion' => $idQuestion,
//			'listViewParams_answers' => $listViewParams_answers,
//			'player' => 'Player View Goes Here!',
//			'hidePlayer' => $hidePlayer,
//			'canAnswer' => App7020Question::canAnswerQuestion($idQuestion)
//				), false, true);
//	}

	public function actionIndex__() {
		if (!empty($_GET['tab'])) {
			$_SESSION['got'] = $_GET['tab'];
			$this->redirect(array('guruDashboard/index'));
		}
		if (!empty($_SESSION['got'])) {
			$_GET['tab'] = $_SESSION['got'];
			unset($_SESSION['got']);
		}
		$this->breadcrumbs = array(
			Yii::t('app7020', 'My Expert Tasks'),
		);

		// FILTER PARAMS AND COMBO LIST PARAMS
		$listView_ajax = Yii::app()->request->getParam('ajax', false);
		$blankTemplates = array(
			'page' => 'app7020.protected.views.knowledgeLibrary.listViewBlankTemplates.assetsPageBlank'
		);

		// ******************************************** //
		// ------------ SATISFY COMBO LIST ------------ //
		// ******************************************** //


		/**
		 * New only
		 */
		$listViewParams_newOnly_satisfy = array(
			'listParams' => array(
				'listId' => "newOnlySatisfyComboListView",
				'dataProvider' => App7020QuestionRequest::getExpertsRequests(
						Yii::app()->user->idst, $requestStatus = 1, array('field' => 'created', 'vector' => 'DESC'), true, array('comboListViewId' => 'activityRequestsSatisfy_list')
				),
				'template' => '{items}{pager}{summary}',
				'viewData' => array('listGotItType' => 'guru_dashboard_questions_gotit'),
			),
			'extensions' => 'takeInCharge,ignore',
			'blankTemplates' => $blankTemplates,
		);
		/**
		 * In charge
		 */
		$listViewParams_incharge_satisfy = array(
			'listParams' => array(
				'listId' => "inChargeComboListView",
				'dataProvider' => App7020QuestionRequest::getExpertsRequests(
						Yii::app()->user->idst, $requestStatus = 2, array('field' => 'created', 'vector' => 'DESC'), true, array('comboListViewId' => 'activityRequestsSatisfy_list')
				),
				'template' => '{items}{pager}{summary}',
				'viewData' => array('listGotItType' => 'guru_dashboard_questions_gotit'),
			),
			'extensions' => 'tookInCharge',
			'blankTemplates' => $blankTemplates,
		);

		/**
		 * Ignored
		 */
		$listViewParams_ignored_satisfy = array(
			'listParams' => array(
				'listId' => "ignoredSatisfyComboListView",
				'dataProvider' => App7020IgnoreList::getIgnoredExpertsRequests(
						Yii::app()->user->idst, array('field' => 'created', 'vector' => 'DESC'), true, array('comboListViewId' => 'activityRequestsSatisfy_list')
				),
				'template' => '{items}{pager}{summary}',
				'viewData' => array('listGotItType' => 'guru_dashboard_questions_gotit'),
			),
			'extensions' => 'takeInChargeDisabled,unIgnore,gotIt',
			'blankTemplates' => $blankTemplates,
		);

		$navigationMenuLinks_satisfy = array(
			array(
				'tabTitle' => Yii::t('app7020', 'New Only'),
				'tabContent' => $this->widget('common.widgets.app7020QuestionListView', $listViewParams_newOnly_satisfy, true),
			),
			array(
				'tabTitle' => Yii::t('app7020', 'In charge'),
				'tabContent' => $this->widget('common.widgets.app7020QuestionListView', $listViewParams_incharge_satisfy, true),
				'tabTitleCounter' => $listViewParams_incharge_satisfy['listParams']['dataProvider']->getTotalItemCount(),
			),
			array(
				'tabTitle' => Yii::t('app7020', 'Ignored'),
				'tabContent' => $this->widget('common.widgets.app7020QuestionListView', $listViewParams_ignored_satisfy, true),
			),
		);







		// ******************************************* //
		// ------------ ASSETS COMBO LIST ------------
		// ******************************************* //

		$pageSize_assets = 5;

		/**
		 * New only
		 */
		$listViewParams_newOnly_assets = array(
			'listParams' => array(
				'listId' => "newOnlyAssetsComboListView",
				'dataProvider' => App7020Assets::getExpertsReviewedAssets(
						Yii::app()->user->idst, $assetStatus = array(
					App7020Assets::CONVERSION_STATUS_FINISHED,
						), $orderField = array('field' => 'created', 'vector' => 'DESC'), $asArrayDataProvider = true, $providerConfig = array('comboListViewId' => 'assetsToReviewApprove_list', 'pagination' => array('pageSize' => $pageSize_assets))
				),
				'template' => '{items}{pager}{summary}',
				'viewData' => array('listGotItType' => 'guru_dashboard_questions_gotit'),
			),
			'gridColumns' => 1,
			'blankTemplates' => $blankTemplates,
			'extensions' => 'topics,ignore,new',
			'titleAsLink' => true,
			'linkToEditMode' => true
		);
		/**
		 * To review
		 */
		$listViewParams_toreview_assets = array(
			'listParams' => array(
				'listId' => "reviewAssetsComboListView",
				'dataProvider' => App7020Assets::getExpertsReviewedAssets(
						Yii::app()->user->idst, $assetStatus = array(
					App7020Assets::CONVERSION_STATUS_FINISHED,
					App7020Assets::CONVERSION_STATUS_INREVIEW,
					//App7020Assets::CONVERSION_STATUS_APPROVED,
					App7020Assets::CONVERSION_STATUS_UNPUBLISH
						), $orderField = array('field' => 'created', 'vector' => 'DESC'), $asArrayDataProvider = true, $providerConfig = array('comboListViewId' => 'assetsToReviewApprove_list', 'pagination' => array('pageSize' => $pageSize_assets))
				),
				'template' => '{items}{pager}{summary}',
				'viewData' => array('listGotItType' => 'guru_dashboard_questions_gotit'),
			),
			'gridColumns' => 1,
			'blankTemplates' => $blankTemplates,
			'extensions' => 'topics,ignore,asterisk,count',
			'titleAsLink' => true,
			'linkToEditMode' => true
		);

		/**
		 * Ignored
		 */
		$listViewParams_ignored_assets = array(
			'listParams' => array(
				'listId' => "ignoredAssetsComboListView",
				'dataProvider' => App7020IgnoreList::getIgnoredExpertsAssets(
						Yii::app()->user->idst, array('field' => 'created', 'vector' => 'DESC'), true, array('comboListViewId' => 'assetsToReviewApprove_list')
				),
				'template' => '{items}{pager}{summary}',
				'viewData' => array('listGotItType' => 'guru_dashboard_questions_gotit'),
			),
			'gridColumns' => 1,
			'blankTemplates' => $blankTemplates,
			'extensions' => 'topics,unIgnore,gotIt,asterisk,count',
			'titleAsLink' => true,
			'linkToEditMode' => true
		);

		$navigationMenuLinks_assets = array(
			array(
				'tabTitle' => Yii::t('app7020', 'New Only'),
				'tabContent' => $this->widget('common.widgets.app7020ContentListView', $listViewParams_newOnly_assets, true),
			),
			array(
				'tabTitle' => Yii::t('app7020', 'Review in progress'),
				'tabContent' => $this->widget('common.widgets.app7020ContentListView', $listViewParams_toreview_assets, true),
				'tabTitleCounter' => $listViewParams_toreview_assets['listParams']['dataProvider']->getTotalItemCount(),
			),
			array(
				'tabTitle' => Yii::t('app7020', 'Ignored'),
				'tabContent' => $this->widget('common.widgets.app7020ContentListView', $listViewParams_ignored_assets, true),
			),
		);



		// ******************************************* //
		// ----------- QUESTIONS COMBO LIST ----------
		// ******************************************* //
		$pageSize_questions = 5;

		/**
		 * New only
		 */
		$listViewParams_newOnly_questions = array(
			'listParams' => array(
				'listId' => "newOnlyQuestionsComboListView",
				'dataProvider' => App7020Question::getExpertsQuestions(
						Yii::app()->user->idst, $questionStatus = 1, $orderField = array('field' => 'created', 'vector' => 'DESC'), $asArrayDataProvider = true, $providerConfig = array('comboListViewId' => 'questionsYouHaveToAnswer_list', 'pagination' => array('pageSize' => $pageSize_questions))
				),
				'template' => '{items}{pager}{summary}',
				'viewData' => array('listGotItType' => 'guru_dashboard_questions_gotit'),
			),
			'extensions' => 'topics,ignore',
			'blankTemplates' => $blankTemplates,
		);

		/**
		 * Review in progress
		 */
		$listViewParams_open_questions = array(
			'listParams' => array(
				'listId' => "reviewQuestionsComboListView",
				'dataProvider' => App7020Question::getExpertsQuestions(
						Yii::app()->user->idst, $questionStatus = 2, $orderField = array('field' => 'created', 'vector' => 'DESC'), $asArrayDataProvider = true, $providerConfig = array('comboListViewId' => 'questionsYouHaveToAnswer_list', 'pagination' => array('pageSize' => $pageSize_questions))
				),
				'template' => '{items}{pager}{summary}',
				'viewData' => array('listGotItType' => 'guru_dashboard_questions_gotit'),
			),
			'extensions' => 'topics,ignore',
			'blankTemplates' => $blankTemplates,
		);

		/**
		 * Ignored
		 */
		$listViewParams_ignored_questions = array(
			'listParams' => array(
				'listId' => "ignoredQuestionsComboListView",
				'dataProvider' => App7020IgnoreList::getIgnoredExpertsQuestions(
						Yii::app()->user->idst, $orderField = array('field' => 'created', 'vector' => 'DESC'), $asArrayDataProvider = true, $providerConfig = array('comboListViewId' => 'questionsYouHaveToAnswer_list', 'pagination' => array('pageSize' => $pageSize_questions))
				),
				'template' => '{items}{pager}{summary}',
				'viewData' => array('listGotItType' => 'guru_dashboard_questions_gotit'),
			),
			'extensions' => 'topics,unIgnore,gotIt',
			'blankTemplates' => $blankTemplates,
		);

		$navigationMenuLinks_questions = array(
			array(
				'tabTitle' => Yii::t('app7020', 'New Only'),
				'tabContent' => $this->widget('common.widgets.app7020QuestionListView', $listViewParams_newOnly_questions, true),
			),
			array(
				'tabTitle' => Yii::t('app7020', 'Open questions'),
				'tabContent' => $this->widget('common.widgets.app7020QuestionListView', $listViewParams_open_questions, true),
				'tabTitleCounter' => $listViewParams_open_questions['listParams']['dataProvider']->getTotalItemCount(),
			),
			array(
				'tabTitle' => Yii::t('app7020', 'Ignored'),
				'tabContent' => $this->widget('common.widgets.app7020QuestionListView', $listViewParams_ignored_questions, true),
			),
		);


		/* RENDER MAIN VIEW */
		$this->render('index', array(
			'navigationMenuLinks_assets' => $navigationMenuLinks_assets,
			'navigationMenuLinks_questions' => $navigationMenuLinks_questions,
			'navigationMenuLinks_satisfy' => $navigationMenuLinks_satisfy,
		));
	}

	public function actionAxConfirmTakeInCharge() {
		$idQuestion = Yii::app()->request->getParam('idQuestion', false);
		$confirm = Yii::app()->request->getParam("confirm", FALSE);

		if ($confirm) {
			$requestModel = App7020QuestionRequest::model()->findByAttributes(array('idQuestion' => $idQuestion));
			if ($requestModel->id) {
				$requestModel->idExpert = Yii::app()->user->idst;
				$requestModel->tooked = Yii::app()->localtime->getUTCNow();
				$requestModel->save();
			}
			echo '<a class="auto-close success"></a>';
			Yii::app()->end();
		}

		$this->renderPartial('app7020.protected.views.guruDashboard._confirmTakeInCharge', array(
			'idQuestion' => $idQuestion), false, true);
	}

	public function actionAxConfirmTookInCharge() {
		$idQuestion = Yii::app()->request->getParam('idQuestion', false);
		$confirm = Yii::app()->request->getParam("upload-now", false);

		if ($confirm) {
			$autoPublish = Yii::app()->request->getParam("autoPublish", false);
			$autoInvite = Yii::app()->request->getParam("autoInvite", false);
			$_SESSION['requestedAssets'][$idQuestion]['autoPublish'] = !empty($autoPublish) ? 1 : 0;
			$_SESSION['requestedAssets'][$idQuestion]['autoInvite'] = !empty($autoInvite) ? 1 : 0;

			echo '<a class="auto-close success"></a>';
			Yii::app()->end();
		}

		$requestModel = App7020QuestionRequest::model()->findByAttributes(array('idQuestion' => $idQuestion));

		$userModel = CoreUser::model()->findByPk($requestModel->question->idUser);
		$userNames = $userModel->firstname . " " . $userModel->lastname;

		$this->renderPartial('app7020.protected.views.guruDashboard._confirmTookInCharge', array(
			'idQuestion' => $idQuestion, 'userNames' => $userNames), false, true);
	}

	public function actionAxSetGotItInformation() {
		$tabType = Yii::app()->request->getParam('tabType', false);
		$coreUserSettings = new CoreSettingUser();
		$coreUserSettings->id_user = Yii::app()->user->idst;
		$coreUserSettings->value = 1;

		if ($tabType == 'requests') {
			if (!CoreSettingUser::model()->findByAttributes(array('path_name' => 'guru_dashboard_satisfy_gotit', 'id_user' => Yii::app()->user->idst))) {
				$coreUserSettings->path_name = 'guru_dashboard_satisfy_gotit';
				$coreUserSettings->save();
			}
			$this->sendJSON(array('success' => true));
			Yii::app()->end();
		} else if ($tabType == 'assets') {
			if (!CoreSettingUser::model()->findByAttributes(array('path_name' => CoreSettingUser::GURUDASHBOARD_ASSETS_GOTIT, 'id_user' => Yii::app()->user->idst))) {
				$coreUserSettings->path_name = CoreSettingUser::GURUDASHBOARD_ASSETS_GOTIT;
				$coreUserSettings->save();
			}
			$this->sendJSON(array('success' => true));
			Yii::app()->end();
		} else if ($tabType == 'questions') {
			if (!CoreSettingUser::model()->findByAttributes(array('path_name' => 'guru_dashboard_questions_gotit', 'id_user' => Yii::app()->user->idst))) {
				$coreUserSettings->path_name ='guru_dashboard_questions_gotit';
				$coreUserSettings->save();
			}
			$this->sendJSON(array('success' => true));
			Yii::app()->end();
		} else {
			$this->sendJSON(array('success' => false));
			Yii::app()->end();
		}
	}

	public function actionAxConfirmAddInIgnoreList() {
		$tabType = Yii::app()->request->getParam('tabType', false);
		$objectId = (int) Yii::app()->request->getParam('objectId', false);
		$confirm = Yii::app()->request->getParam("yt0", false);
		if ($confirm) {
			/* Add REQUEST OR QUESTION TO IGNORE LIST TABLE if not exist */
			if (($tabType == 'requests' || $tabType == 'questions') && App7020Question::model()->findByPk($objectId)) {
				if (!App7020IgnoreList::model()->findByAttributes(
								array(
									'idUser' => Yii::app()->user->idst,
									'idObject' => $objectId,
									'objectType' => App7020IgnoreList::OBJECT_TYPE_QUESTION))) {

					$ignoreListModel = new App7020IgnoreList();
					$ignoreListModel->idUser = Yii::app()->user->idst;
					$ignoreListModel->idObject = $objectId;
					$ignoreListModel->objectType = App7020IgnoreList::OBJECT_TYPE_QUESTION;
					$ignoreListModel->save();
					echo '<a class="auto-close success" data-listReference="' . $tabType . '"></a>';
					Yii::app()->end();
				}
			}
			/* Add ASSET TO IGNORE LIST TABLE if not exist */
			if ($tabType == 'assets' && App7020Assets::model()->findByPk($objectId)) {
				if (!App7020IgnoreList::model()->findByAttributes(
								array(
									'idUser' => Yii::app()->user->idst,
									'idObject' => $objectId,
									'objectType' => App7020IgnoreList::OBJECT_TYPE_ASSET))) {

					$ignoreListModel = new App7020IgnoreList();
					$ignoreListModel->idUser = Yii::app()->user->idst;
					$ignoreListModel->idObject = $objectId;
					$ignoreListModel->objectType = App7020IgnoreList::OBJECT_TYPE_ASSET;
					$ignoreListModel->save();
					echo '<a class="auto-close success" data-listReference="' . $tabType . '"></a>';
					Yii::app()->end();
				}
			}
			echo '<a class="auto-close unsuccess"></a>';
			Yii::app()->end();
		}
		$this->renderPartial('_confirmAddInIgnoreList', array(
			'tabType' => $tabType, 'objectId' => $objectId), false, true);
	}

	public function actionAxRemoveFromIgnoreList() {
		$tabType = Yii::app()->request->getParam('tabType', false);
		$objectId = (int) Yii::app()->request->getParam('objectId', false);

		if ($tabType == 'requests' || $tabType == 'questions') {
			$objectType = App7020IgnoreList::OBJECT_TYPE_QUESTION;
		}
		if ($tabType == 'assets') {
			$objectType = App7020IgnoreList::OBJECT_TYPE_ASSET;
		}
		$ignoreListModel = App7020IgnoreList::model()->findByAttributes(array('idUser' => Yii::app()->user->idst, 'idObject' => $objectId, 'objectType' => $objectType));
		if ($ignoreListModel->id) {
			$ignoreListModel->delete();
			$this->sendJSON(array('success' => true));
			Yii::app()->end();
		}
		$this->sendJSON(array('success' => false));
		Yii::app()->end();
	}

}
