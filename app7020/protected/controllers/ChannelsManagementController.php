<?php

/**
 * Description of ChannelsController
 *
 * @author Kristian
 */
class ChannelsManagementController extends App7020BaseController {

    public function init() {
        parent::init();
    }

    public function filters() {
        return array(
            'accessControl'
        );
    }

    public function accessRules() {
        $res = array();
        $res[] = array(
            'allow',
            'users' => array(
                '*'
            ),
            'actions' => array(
                'login'
            )
        );
        $res[] = array(
            'allow',
            'users' => array(
                '@'
            ),
            'actions' => array(
                'logout',
                'index'
            )
        );
        return $res;
    }

    public function actionIndex() {
        if (!Yii::app()->user->isGodAdmin) {
            $this->redirect(array('site/index'));
            Yii::app()->end();
        }
        $themeUrl = Yii::app()->theme->baseUrl;
        Yii::app()->tinymce;
        Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/user_management.js', CClientScript::POS_HEAD);

        Yii::app()->getClientScript()->registerCssFile($themeUrl . '/css/user_management.css');
        Yii::app()->getClientScript()->registerCssFile($themeUrl . '/css/DoceboCGridView.css');
        // Register Fancytree JS/CSS
        $ft = new FancyTree();
        Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree-all.js', CClientScript::POS_HEAD);
        Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree.docebo.js', CClientScript::POS_HEAD);
        Yii::app()->getClientScript()->registerCssFile($ft->getAssetsUrl() . '/skin-docebo/ui.fancytree.css');
        UsersSelector::registerClientScripts();
        Yii::app()->fontawesome->loadIconPicker();
        Yii::app()->clientScript->registerCssFile($this->getAssetsUrl() . '/js/libs/farbtastic/farbtastic.css');
        Yii::app()->clientScript->registerScriptFile($this->getAssetsUrl() . '/js/libs/farbtastic/farbtastic.min.js');
        //CRASHING TOOLTIPS IN THE MENU
        //Yii::app()->clientScript->registerScriptFile($this->getAssetsUrl() . '/js/libs/farbtastic/jquery.tools.min.js');
        $this->getClientScript()->registerScriptFile($themeUrl . '/js/jquery.mCustomScrollbar.concat.min.js', CClientScript::POS_END);
        $this->getClientScript()->registerScriptFile($themeUrl . '/js/lookingfor/jquery.lookingfor.min.js', CClientScript::POS_HEAD);
        $this->getClientScript()->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js', CClientScript::POS_HEAD);
        $this->getClientScript()->registerCssFile($themeUrl . '/css/' . (YII_DEBUG ? 'jquery.mCustomScrollbar.min.css' : 'jquery.mCustomScrollbar.css'));
        $amdController = array();
        $amdController['data']['data-main'] = $this->getAssetsUrl() . '/js/controllers/channels/main';
        Yii::app()->event->raise('registerAMDScript', new DEvent($this, $amdController));

        $this->breadcrumbs = array(
            Yii::t('app7020', 'Admin'),
            Yii::t('app7020', 'Channels'),
        );
        $this->breadcrumbsHtmlOptions = array('class' => 'breadcrumb');

        $channelsGridViewColumns = array();

        if (PluginManager::isPluginActive('Share7020App')) {
            $channelsGridViewColumns = array(
                array(
                    'name' => 'name',
                    'value' => 'app7020Channels::channelColumnName($data, true, 1);',
                    'type' => 'raw',
                    'header' => Yii::t('app7020', 'Channel'),
                    'htmlOptions' => array(
                        'class' => 'channel_name'),
                    'headerHtmlOptions' => array()
                ),
                array(
                    'name' => 'visibility',
                    'value' => 'app7020Channels::channelColumnVisibility($data);',
                    'type' => 'raw',
                    'header' => Yii::t('app7020', 'Visibility'),
                    'htmlOptions' => array(
                        "class" => "channel_visibility",
                    ),
                    'headerHtmlOptions' => array(
                        "class" => "text-center",
                        'width' => '170px;'
                    )
                ),
                array(
                    'name' => 'upload_permissions',
                    'value' => 'app7020Channels::channelColumnUploadPermission($data);',
                    'type' => 'raw',
                    'header' => Yii::t('app7020', 'Upload Permissions'),
                    'htmlOptions' => array(
                        "class" => "channel_upload_permissions",
                    ),
                    'headerHtmlOptions' => array(
                        "class" => "text-center"
                    )
                ),
                array(
                    'name' => 'experts',
                    'value' => 'app7020Channels::channelColumnExperts($data);',
                    'type' => 'raw',
                    'header' => Yii::t('app7020', 'Experts'),
                    'htmlOptions' => array(
                        "class" => "channel_experts",
                    ),
                    'headerHtmlOptions' => array(
                        "class" => "text-center",
                    )
                ),
                array(
                    'name' => 'show',
                    'value' => 'app7020Channels::channelColumnShow($data);',
                    'type' => 'raw',
                    'header' => Yii::t('app7020', 'Show'),
                    'htmlOptions' => array(
                        "class" => "channel_show",
                    ),
                    'headerHtmlOptions' => array(
                        "class" => "text-center",
                    ),
                ),
                array(
                    'name' => 'dnd',
                    'value' => 'app7020Channels::channelColumnDnd($data);',
                    'type' => 'raw',
                    'header' => '',
                    'htmlOptions' => array(
                        "class" => "channel_dnd",
                    ),
                    'headerHtmlOptions' => array(
                        'width' => '32px;'
                    )
                ),
                array(
                    'name' => 'dropdownMenu',
                    'value' => 'app7020Channels::channelColumnDropdownMenu($data);',
                    'type' => 'raw',
                    'header' => '',
                    'htmlOptions' => array(
                        'class' => 'channel_dropdownMenu',
                    ),
                    'headerHtmlOptions' => array(
                        'width' => '32px;'
                    )
                )
            );
        } else {

            $channelsGridViewColumns = array(
                array(
                    'name' => 'name',
                    'value' => 'app7020Channels::channelColumnName($data, true, 1);',
                    'type' => 'raw',
                    'header' => Yii::t('app7020', 'Channel'),
                    'htmlOptions' => array(
                        'class' => 'channel_name'),
                    'headerHtmlOptions' => array()
                ),
                array(
                    'name' => 'visibility',
                    'value' => 'app7020Channels::channelColumnVisibility($data);',
                    'type' => 'raw',
                    'header' => Yii::t('app7020', 'Visibility'),
                    'htmlOptions' => array(
                        "class" => "channel_visibility",
                    ),
                    'headerHtmlOptions' => array(
                        "class" => "text-center",
                        'width' => '170px;'
                    )
                ),
                array(
                    'name' => 'show',
                    'value' => 'app7020Channels::channelColumnShow($data);',
                    'type' => 'raw',
                    'header' => Yii::t('app7020', 'Show'),
                    'htmlOptions' => array(
                        "class" => "channel_show",
                    ),
                    'headerHtmlOptions' => array(
                        "class" => "text-center",
                    ),
                ),
                array(
                    'name' => 'dnd',
                    'value' => 'app7020Channels::channelColumnDnd($data);',
                    'type' => 'raw',
                    'header' => '',
                    'htmlOptions' => array(
                        "class" => "channel_dnd",
                    ),
                    'headerHtmlOptions' => array(
                        'width' => '32px;'
                    )
                ),
                array(
                    'name' => 'dropdownMenu',
                    'value' => 'app7020Channels::channelColumnDropdownMenu($data);',
                    'type' => 'raw',
                    'header' => '',
                    'htmlOptions' => array(
                        'class' => 'channel_dropdownMenu',
                    ),
                    'headerHtmlOptions' => array(
                        'width' => '32px;'
                    )
                )
            );
        }
        $custom_Select = App7020Channels::customProviderSelect(array('num_experts' => true, 'num_visibility_structures' => true, 'friendly_permissions' => true));
        $custom_channel_visibility_select = array(
            "idUser" => Yii::app()->user->idst,
            "ignoreVisallChannels" => false,
            "ignoreGodAdminEffect" => false,
            'appendEnabledEffect' => false,
            'ignoreSystemChannels' => false,
            'customSelect' => $custom_Select,
            'sort' => array(
                'channelOrdering' => array(
                    CSort::SORT_ASC => 'channelOrdering',
                    CSort::SORT_DESC => 'channelOrdering DESC',
                    'default' => CSort::SORT_ASC
                )
            ),
            'return' => 'provider',
        );
        $dp = App7020Channels::extractUserChannels($custom_channel_visibility_select);
        $array = array();
        foreach ($dp->getData() as $key => $data) {
            $array[$key] = $data;
            if (isset($data['channelDescription'])) {
                $array[$key]['channelDescription'] = strip_tags(App7020Helpers::truncateAbstractHTMLContents($data['channelDescription']), '<strong><u><em><b><b><i>');
            }
        }
        $dp->setData($array);
        $channelsGridViewParams = array(
            'gridId' => 'channelsGridView',
            'dataProvider' => $dp,
            'columns' => $channelsGridViewColumns,
            'massiveActions' => null,
            'disableMassSelection' => true,
            'disableMassActions' => true,
            'sortableRows' => true,
            'sortableUpdateUrl' => Docebo::createApp7020Url('AxChannels/AxChannelsDranAndDrop'),
            'dragHandlerSelector' => '.fa-arrows',
            'gridTemplate' => '{items}{summary}{pager}',
            'infinite' => true
        );

        $this->render('index', array(
            'channelsGridViewParams' => $channelsGridViewParams
        ));
    }

    public function actionExperts() {
        $idChannel = Yii::app()->request->getParam('id', false);
        $selectAll = Yii::app()->request->getParam('comboListViewSelectAll', false);
        UsersSelector::registerClientScripts();
        Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/css/DoceboCGridView.css');

        $amdController = array();
        $amdController['data']['data-main'] = $this->getAssetsUrl() . '/js/controllers/channels/main';
        Yii::app()->event->raise('registerAMDScript', new DEvent($this, $amdController));

        $data = array();
        $channelTitle = Yii::t('app7020', 'Channel') . " # " . $idChannel;
        $data['trans'] = App7020Channels::getChannelTranslationsById($idChannel);
        $langBrowserCode = Yii::app()->getLanguage();
		$defaultLang =  Lang::getBrowserCodeByCode(Settings::get('default_language', 'english'));
        foreach ($data['trans'] as $v) {
			switch ($v['lang'])
			{
				case $langBrowserCode:
					$channelTitle = $v['name'];
					break 2; //yes break 2, it brakes 2 times, the case and the cycle
				case $defaultLang:
					$channelTitle = $v['name'];
					break;
			}
        }

        $this->breadcrumbs = array(
            Yii::t('app7020', 'Admin'),
            Yii::t('app7020', 'Channels'),
            $channelTitle,
            Yii::t('app7020', 'Assigned Experts'),
        );
        //$this->breadcrumbsHtmlOptions = array('class' => 'breadcrumb_channels_style');

        $dataProvider = App7020ChannelExperts::model()->sqlDataProvider();

        // Make all check boxes CHECKED
        if ($selectAll) {
            $dataProvider->setPagination(false);
            $keys = $dataProvider->keys;
            foreach ($keys as $index => $key) {
                $keys[$index] = (int) $key;
            }
            echo json_encode($keys);
            Yii::app()->end();
        }

        // Autocomplete request?
        if (isset($_REQUEST['autocomplete'])) {
            $dataProvider = App7020Experts::sqlDataProvider(array('search_input' => $_REQUEST['term']));
            $result = array();
            foreach ($dataProvider->data as $user) {
                $result[] = array('label' => $user['expertNames'] . ' (' . $user['username'] . ')', 'value' => $user['expertNames']);
            }
            $this->sendJSON($result);
        }

        $listViewParams = array(
            'autocompleteRoute' => 'searchAutocomplete',
            'massiveActions' => array('delete' => Yii::t('standard', '_DEL')),
            'listId' => "expertsComboListView",
            'dataProvider' => $dataProvider,
            'listItemView' => 'app7020.protected.views.channelsManagement._comboListViewItem',
            'columns' => 2,
            'hiddenFields' => array('id' => $idChannel),
            'afterAjaxUpdate' => "", //"app7020.assignKnowledgeGurus.resetCounter()",
            'title' => Yii::t('app7020', 'Experts assigned to channel:') . $channelTitle
        );
        $this->render('experts', array(
            'listViewParams' => $listViewParams,
        ));
    }

    public function actionSearchAutocomplete() {
        $maxNumber = Yii::app()->request->getParam('max_number', 10);
        $term = Yii::app()->request->getParam('term', false);

        $model = new App7020TopicExpert();
        $model->search_input = $term ? $term : null;

        $dataProvider = $model->sqlDataProvider();
        $dataProvider->setPagination(array(
            'pageSize' => $maxNumber,
        ));
        $data = $dataProvider->getData();
        $result = array();
        if ($data) {
            foreach ($data as $item) {
                $result[] = array(
                    'label' => $item['firstname'] . ' ' . $item['lastname'],
                    'value' => $item['firstname'] . ' ' . $item['lastname'],
                );
            }
        }
        echo CJSON::encode($result);
    }

}
