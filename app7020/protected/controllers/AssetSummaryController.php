<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AssetSummaryReportController
 *
 * @author Trayan
 */
class AssetSummaryController extends App7020BaseController {

	public function filters() {
		return array(
			'accessControl'
		);
	}

	/**
	 *
	 * @see CController::init()
	 */
	public function init() {
		parent::init();
		$this->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl . '/js/Chart.min.js', CClientScript::POS_HEAD);
		Yii::app()->clientScript->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl() . '/jui/css/base/jquery-ui.css');

		$amdController = array();
		$amdController['data']['data-main'] = $this->getAssetsUrl() . '/js/app7020Activity/main';
		Yii::app()->event->raise('registerAMDScript', new DEvent($this, $amdController));
	}

	public function accessRules() {
		$res = array();
		$res[] = array(
			'allow',
			'users' => array(
				'*'
			),
			'actions' => array(
				'login'
			)
		);
		$res[] = array(
			'allow',
			'users' => array(
				'@'
			),
			'actions' => array(
				'logout',
				'index'
			)
		);
		return $res;
	}

	public function actionIndex() {

		$asset = Yii::app()->request->getParam('asset');
		$assetsUrl = App7020Helpers::getAssetsUrl();
		$singleContent = App7020Assets::model()->findByPk($asset);
		if ((!empty($singleContent) && $singleContent instanceof App7020Assets)) {
			$timeInterval = Yii::app()->request->getParam('timeframe');
			$statsOverview = App7020AssetSummary::viewsStat($asset, $timeInterval);
			$statsExpert = App7020AssetSummary::askExpertStat($asset, $timeInterval);
			$statsSocial = App7020AssetSummary::socialStat($asset, $timeInterval);
			$invitations = App7020Invitations::getInvitationsDataProvider($asset);
			$watchRate = App7020Invitations::getStatisticsData($asset);
			$timeframes = array('7' => Yii::t('app7020', 'Weekly'), '30' => Yii::t('app7020', 'Monthly'), '365' => Yii::t('app7020', 'Yearly'));
			if ($watchRate["data"]["totalInvitedPeople"]) {
				$watchPercentage["watched"] = ($watchRate["data"]["watched"] / $watchRate["data"]["totalInvitedPeople"]) * 100;
				$watchPercentage["notWatched"] = ($watchRate["data"]["notWatched"] / $watchRate["data"]["totalInvitedPeople"]) * 100;
				Yii::app()->clientScript->registerScript('app7020WatchRate', "var app7020WatchPercentage = " . CJSON::encode($watchPercentage), CClientScript::POS_HEAD);
			}
			$channels = $singleContent->asset_channels;
			$tags = $singleContent->tags;
			$tagsArray = array();
			if ($tags) {
				foreach ($tags as $tag) {
					$tagsArray[] = App7020Tag::model()->findByPk($tag->idTag)->tagText;
				}
			}
			$channelsArray = array();
			foreach ($channels as $channels) {
				$channelTranslation = App7020Channels::model()->findByPk($channels->idChannel)->translation();
				$channelsArray[] = array(
					'channel_name' => $channelTranslation['name'],
				    'channel_url' => Docebo::createLmsUrl('channels/index', array('#' => "/channel/".$channels->idChannel))
				);
			}
			$assetContributor = CoreUser::getUserIdByIdst($singleContent->userId);
			$thumb = App7020Assets::getAssetThumbnailUri($asset);
			$avatar = CoreUser::getAvatarByUserId($assetContributor);

			$this->breadcrumbs = array(
				Yii::t('app7020', 'Admin') => Docebo::createAppUrl('admin:site/index'),
				Yii::t('app7020', 'Reports') => Docebo::createAppUrl('admin:reportManagement/index'),
				'Asset summary: '.$singleContent->title,
			);
			$assetData["assetId"] = $singleContent->id;
			$assetData["userId"] = $singleContent->userId;
			$assetData["title"] = $singleContent->title;
			$assetData["description"] = $singleContent->description;
			$assetData["contributor"] = $assetContributor;
			$assetData["thumb"] = $thumb;
			Yii::app()->clientScript->registerScript('app7020AssetSummaryOverview', "var app7020AssetSummaryOverview = " . CJSON::encode($statsOverview), CClientScript::POS_HEAD);
			Yii::app()->clientScript->registerScript('app7020AssetSummarySocial', "var app7020AssetSummarySocial = " . CJSON::encode($statsSocial), CClientScript::POS_HEAD);
			Yii::app()->clientScript->registerScript('app7020StatsExpert', "var app7020StatsExpert = " . CJSON::encode($statsExpert), CClientScript::POS_HEAD);
			$this->render('index', array(
				'assetData' => $assetData,
				'contributor' => $assetContributor,
				'avatar' => $avatar,
				'channels' => $channelsArray,
				'tags' => $tagsArray,
				'invitations' => $invitations,
				'statsOverview' => $statsOverview,
				'statsSocial' => $statsSocial,
				'watchrate' => $watchRate,
				'statsExpert' => $statsExpert,
				'timeframes' => $timeframes
			));
		} else {
			$this->redirect(Docebo::createLmsUrl('reportManagement/index'));
		}
	}

	public function actionAxTimeOverview() {
		if (Yii::app()->request->isAjaxRequest) {
			$asset = Yii::app()->request->getParam('assetId', array());
			$timeInterval = Yii::app()->request->getParam('timeframe', array());
			$statsOverview = App7020AssetSummary::viewsStat($asset, intval($timeInterval));
			echo CJSON::encode($statsOverview);
		}
	}

	public function actionAxTimeFrameExpert() {
		if (Yii::app()->request->isAjaxRequest) {
			$asset = Yii::app()->request->getParam('assetId', array());
			$timeInterval = Yii::app()->request->getParam('timeframe', array());
			$statsExpert = App7020AssetSummary::askExpertStat($asset, intval($timeInterval));
			echo CJSON::encode($statsExpert);
		}
	}

	public function actionAxTimeSocial() {
		if (Yii::app()->request->isAjaxRequest) {
			$asset = Yii::app()->request->getParam('assetId', array());
			$timeInterval = Yii::app()->request->getParam('timeframe', array());
			$statsSocial = App7020AssetSummary::socialStat($asset, intval($timeInterval));
			echo CJSON::encode($statsSocial);
		}
	}

}
