<?php

class AxAnalyticsController extends AnalyticsController {

	public function accessRules() {
		return parent::accessRules();
	}

	public function actionAxReinvite() {
		$idContent = Yii::app()->request->getParam('idContent', false);
		$idInvited = Yii::app()->request->getParam('idInvited', array());
		if (!$idContent || !$idInvited) {
			echo (json_encode(
					array(
						'result' => false
					)
			));
			Yii::app()->end();
		}

		$users = array();

		if (!is_array($idInvited)) {
			$users[] = $idInvited;
		} else {
			$users = $idInvited;
		}
		$invitedPeople = array();
		foreach ($users as $value) {
			App7020Invitations::model()->deleteAllByAttributes(array('idInvited' => $value, 'idContent' => $idContent));
			$invitationObject = new App7020Invitations();
			$invitationObject->idContent = $idContent;
			$invitationObject->idInvited = $value;
			$invitationObject->save();
			$invitedPeople[] = $value;
		}

		Yii::app()->event->raise('App7020Invitation', new DEvent($this, array('users' => $invitedPeople,
			'contentId' => $idContent)));

		echo (json_encode(
				array(
					'result' => true
				)
		));
		Yii::app()->end();
	}

	public function actionAxReinviteAll() {
		$idContent = Yii::app()->request->getParam('idContent', false);

		if (!$idContent) {
			echo (json_encode(
					array(
						'result' => false
					)
			));
			Yii::app()->end();
		}

		$invitations = App7020Invitations::getInvitationsDataProvider($idContent, '', 1)->rawData;
		$invitedPeople = array();
		foreach ($invitations as $value) {
			App7020Invitations::model()->deleteAllByAttributes(array('idInvited' => $value['idi'], 'idContent' => $idContent));
			$invitationObject = new App7020Invitations();
			$invitationObject->idContent = $idContent;
			$invitationObject->idInvited = $value['idi'];
			$invitationObject->save();
			$invitedPeople[] = $value['idi'];
		}

		Yii::app()->event->raise('App7020Invitation', new DEvent($this, array('users' => $invitedPeople,
			'contentId' => $idContent)));

		echo (json_encode(
				array(
					'result' => true
				)
		));
	}

	public function actionAxRemoveInvitation() {
		$idContent = Yii::app()->request->getParam('idContent', false);
		$idInvited = Yii::app()->request->getParam('idInvited', array());
		if (!$idContent || !$idInvited) {
			echo (json_encode(
					array(
						'result' => false
					)
			));
			Yii::app()->end();
		}
		$users = array();

		if (!is_array($idInvited)) {
			$users[] = $idInvited;
		} else {
			$users = $idInvited;
		}
		foreach ($users as $value) {
			App7020Invitations::model()->deleteAllByAttributes(array('idInvited' => $value, 'idContent' => $idContent));
		}
		echo (json_encode(
				array(
					'result' => true
				)
		));
		Yii::app()->end();
	}

	public function actionAxRemoveAllInvitations() {
		$idContent = Yii::app()->request->getParam('idContent', false);

		if (!$idContent) {
			echo (json_encode(
					array(
						'result' => false
					)
			));
			Yii::app()->end();
		}

		$invitations = App7020Invitations::getInvitationsDataProvider($idContent, '', 1)->rawData;
		foreach ($invitations as $value) {
			App7020Invitations::model()->deleteAllByAttributes(array('idInvited' => $value['idi'], 'idContent' => $idContent));
		}

		echo (json_encode(
				array(
					'result' => true
				)
		));
		Yii::app()->end();
	}

	public function actionAxGetStatisticsData() {
		$idContent = Yii::app()->request->getParam('idContent', false);
		$timeWords = array('d' => '<span>d</span>', 'h' => '<span>h</span>', 'm' => '<span>m</span>');
		$dataTmp = App7020Invitations::getStatisticsData($idContent);
		$data = $dataTmp['data'];
		$data['avgReactionTime'] = App7020Helpers::secondsToWords($data['avgReactionTime'], $timeWords, true, true, true, false);
		echo json_encode($data);
		Yii::app()->end();
	}

	public function actionAxShowConfirmInvitationModal() {
		$assetId = Yii::app()->request->getParam('assetId', false);
		$userId = Yii::app()->request->getParam('userId', false);
		$mass = Yii::app()->request->getParam('mass', false);
		$method = Yii::app()->request->getParam('method', false);

		if ($mass === 'true') {
			$usersCount = count(App7020Invitations::getInvitationsDataProvider($assetId, '', 1)->rawData);
		} else {
			$usersCount = 1;
		}

		$this->renderPartial('app7020.protected.views.analytics._confirmInvitationModal', array(
			'assetId' => $assetId,
			'userId' => $userId,
			'mass' => $mass,
			'method' => $method,
			'usersCount' => $usersCount
		));
	}

}
