<?php

/**
 * Description of AxChannelsController
 *
 * @author Kristian
 */
class AxChannelsController extends ChannelsManagementController {

	//put your code here

	public function accessRules() {
		return parent::accessRules();
	}

	public function actionAxEditChannel() {
		$fromStep = Yii::app()->request->getParam('nextStep', 'initial_create');
		$prev = Yii::app()->request->getParam('prev', '');
		$prevStep = Yii::app()->request->getParam('prevStep', false);
		$currentLanguage = Yii::app()->getLanguage();

		$langBrowserCode = Lang::getBrowserCodeByCode(CoreLangLanguage::getDefaultLanguage());
		// Get Active languages and add text "Default"
		$languages = Lang::getAllNamesByBrowserCode();
		$activeLanguagesSetDefault = array();
		foreach ($languages as $key => $language) {
			if ($key == $langBrowserCode) {
				$language = $language . Yii::t('app7020', '{space}(Default)', array('{space}' => ' '));
			}
			$activeLanguagesSetDefault[$key] = $language;
		}

		$data = array(
			'step' => $fromStep,
		);

		/**
		 * Unset session if exists
		 */
		switch ($fromStep) {
			/**
			 * Add/Edit channel Initial modal step
			 */
			case 'initial_create':
				/**
				 * Unset session if exists
				 */
				if (empty($prev)) {
					$_SESSION['app7020ChannelData'] = array(
						'id' => (int) Yii::app()->request->getParam('id', false)
					);
				}


				// SELECT Information from DB to EDIT channel
				//if ($_SESSION['app7020ChannelData']['id'] != 0 && empty($prev)) {
				if (!empty($_SESSION['app7020ChannelData']['id']) && empty($prev)) {
					$_SESSION['app7020ChannelData']['trans'] = App7020Channels::getChannelTranslationsById($_SESSION['app7020ChannelData']['id']);
					$data['channel'] = App7020Channels::model()->findByPk($_SESSION['app7020ChannelData']['id']);
					$preselected = App7020ChannelVisibility::getAxSelectedGroupsAndBranchesForSelector($_SESSION['app7020ChannelData']['id']);
					$_SESSION['app7020ChannelData']['icon'] = $data['channel']['icon_fa'];
					$_SESSION['app7020ChannelData']['color'] = $data['channel']['icon_color'];
					$_SESSION['app7020ChannelData']['bgrcolor'] = $data['channel']['icon_bgr'];
					$_SESSION['app7020ChannelData']['permissions'] = $data['channel']['permission_type'];
					$_SESSION['app7020ChannelData']['sort'] = $data['channel']['sort_items'];
					$_SESSION['app7020ChannelData']['groups'] = $preselected['groups'];
					$_SESSION['app7020ChannelData']['branches'] = $preselected['branches'];
				}
				$partial = 'app7020.protected.views.channelsManagement.modals._addChannelStep1';
				$this->renderPartial($partial, array(
					'data' => $data,
					'session' => $_SESSION['app7020ChannelData'],
					'defaultBrowseLangCode' => $langBrowserCode,
					'activeLanguages' => $activeLanguagesSetDefault,
					)
				);
				break;
			case 'step2':
				if (empty($prev)) {
					$_SESSION['app7020ChannelData']['langs'] = Yii::app()->request->getParam('langArr', false);
				} else {
					$langs = array();
					if ($_SESSION['app7020ChannelData']['trans'] && !$_SESSION['app7020ChannelData']['langs']) {
						foreach ($_SESSION['app7020ChannelData']['trans'] as $trans) {
							$langs[$trans['lang']] = array();
							$langs[$trans['lang']]['title'] = $trans['name'];
							$langs[$trans['lang']]['description'] = $trans['description'];
						}
						$_SESSION['app7020ChannelData']['langs'] = $langs;
					}
				}
				$partial = 'app7020.protected.views.channelsManagement.modals._addChannelStep2';
				$this->renderPartial($partial, array(
					'data' => $data,
					'session' => $_SESSION['app7020ChannelData'],
					'defaultBrowseLangCode' => $langBrowserCode,
					'activeLanguages' => $activeLanguagesSetDefault,
					'currentLanguage' => $currentLanguage
					)
				);
				break;
			/**
			 * Add channel visibility modal
			 */
			case 'step_visibility':
				$data['channel'] = App7020Channels::model()->findByPk($_SESSION['app7020ChannelData']['id']);
				Log::_(print_r($_SESSION['app7020ChannelData']['id'], 1));
				if (empty($prev)) {
					$_SESSION['app7020ChannelData']['icon'] = Yii::app()->request->getParam('channelIcon', false);
					$_SESSION['app7020ChannelData']['color'] = Yii::app()->request->getParam('color', false);
					$_SESSION['app7020ChannelData']['bgrcolor'] = Yii::app()->request->getParam('background-color', false);
					$hiddenSort = Yii::app()->request->getParam('hidden-sort', false);
					$_SESSION['app7020ChannelData']['sort'] = $hiddenSort[0];
				}

				if (!empty($_SESSION['app7020ChannelData']['id']) && empty($prev)) {
//                    if (!isset($_SESSION['app7020ChannelData']['groups']) && !isset($_SESSION['app7020ChannelData']['branches'])) {
					$preselected = App7020ChannelVisibility::getAxSelectedGroupsAndBranchesForSelector($_SESSION['app7020ChannelData']['id']);
					$_SESSION['app7020ChannelData']['groups'] = $preselected['groups'];
					$_SESSION['app7020ChannelData']['branches'] = $preselected['branches'];
//                    }
				}

				$step = 'step_permissions';
				if (!empty($data['channel']['predefined_uid'])) {
					$step = 'step_finish';
				} else {
					if (!PluginManager::isPluginActive('Share7020App'))
						$step = 'step_finish';
				}
				//$step = (empty($data['channel']['predefined_uid']) && !PluginManager::isPluginActive('Share7020App')) ? 'step_finish' :'step_permissions';
				//$step = (!empty($data['channel']['predefined_uid'])) ? 'step_finish' :'step_permissions';

				$collectorUrl = Docebo::createApp7020Url('axChannels/axEditChannel', array('nextStep' => $step, 'prevStep' => 'step2'));
				$render = Docebo::createLmsUrl('usersSelector/axOpen', array(
						'type' => UsersSelector::TYPE_APP7020_TOPICS,
						'collectorUrl' => $collectorUrl, // Make this action also collector of the users selector submition
						'tabs' => 'groups,orgcharts',
						'preselectedGroups' => $_SESSION['app7020ChannelData']['groups'],
						'preselectedBranches' => $_SESSION['app7020ChannelData']['branches'],
						'showPrevious' => 1,
						'noyiixviewjs' => true,
						'tp' => 'app7020.protected.views.channelsManagement.modals._userSelectorFilterForm',
						'noyiixviewjs' => true,
				));
				$this->redirect($render, true);

				break;
			/**
			 * Add channel permission step
			 */
			case 'init_visibility':
				/**
				 * Unset session if exists
				 */
				$_SESSION['app7020ChannelData'] = array();

				$_SESSION['app7020ChannelData']['id'] = (int) Yii::app()->request->getParam('id', false);

				// SELECT Information from DB to EDIT topic
				if (!empty($_SESSION['app7020ChannelData']['id'])) {
					$_SESSION['app7020ChannelData']['trans'] = App7020Channels::getChannelTranslationsById($_SESSION['app7020ChannelData']['id']);
					$data['channel'] = App7020Channels::model()->findByPk($_SESSION['app7020ChannelData']['id']);
					$preselected = App7020ChannelVisibility::getAxSelectedGroupsAndBranchesForSelector($_SESSION['app7020ChannelData']['id']);
					$_SESSION['app7020ChannelData']['icon'] = $data['channel']['icon_fa'];
					$_SESSION['app7020ChannelData']['color'] = $data['channel']['icon_color'];
					$_SESSION['app7020ChannelData']['bgrcolor'] = $data['channel']['icon_bgr'];
					$_SESSION['app7020ChannelData']['permissions'] = $data['channel']['permission_type'];
					$_SESSION['app7020ChannelData']['sort'] = $data['channel']['sort_items'];
					$_SESSION['app7020ChannelData']['groups'] = $preselected['groups'];
					$_SESSION['app7020ChannelData']['branches'] = $preselected['branches'];
				}

				$step = (!empty($data['channel']['predefined_uid'])) ? 'step_finish' : 'step_permissions';
				$collectorUrl = Docebo::createApp7020Url('axChannels/axEditChannel', array('nextStep' => $step, 'prevStep' => 'step2'));
				$render = Docebo::createLmsUrl('usersSelector/axOpen', array(
						'id' => $_SESSION['app7020ChannelData']['id'],
						'type' => UsersSelector::TYPE_APP7020_TOPICS,
						'collectorUrl' => $collectorUrl, // Make this action also collector of the users selector submition
						'tabs' => 'groups,orgcharts',
						'preselectedGroups' => $_SESSION['app7020ChannelData']['groups'],
						'preselectedBranches' => $_SESSION['app7020ChannelData']['branches'],
						'showPrevious' => 1,
						'noyiixviewjs' => true,
						'tp' => 'app7020.protected.views.channelsManagement.modals._userSelectorFilterForm',
				));
				$this->redirect($render, true);
				break;
			case 'step_permissions':
				$groups = Yii::app()->request->getParam('selected_groups', false);
				$vt = Yii::app()->request->getParam('visibility_type', false);
				$branchjson = json_decode(Yii::app()->request->getParam('orgcharts_selected-json', false));
				if (count($branchjson) > 0) {
					foreach ($branchjson as $elem) {
						$branches[] = $elem;
					}
				}
				if ($vt == "selection" && strlen($groups) <= 2 && empty($branches)) {
					$_SESSION['app7020ChannelData']['groups'] = '';
					$_SESSION['app7020ChannelData']['branches'] = '';
					$step = (!empty($data['channel']['predefined_uid'])) ? 'step_finish' : 'step_permissions';
					$collectorUrl = Docebo::createApp7020Url('axChannels/axEditChannel', array('nextStep' => $step, 'prevStep' => 'step2'));

					$render = Docebo::createLmsUrl('usersSelector/axOpen', array(
							'type' => UsersSelector::TYPE_APP7020_TOPICS,
							'collectorUrl' => $collectorUrl, // Make this action also collector of the users selector submition
							'tabs' => 'groups,orgcharts',
							'preselectedGroups' => $_SESSION['app7020ChannelData']['groups'],
							'preselectedBranches' => $_SESSION['app7020ChannelData']['branches'],
							'showPrevious' => 1,
							'noyiixviewjs' => true,
							'tp' => 'app7020.protected.views.channelsManagement.modals._userSelectorFilterForm',
							'error' => true,
					));
					$this->redirect($render, true);
					break;
				}
				if (empty($prev)) {
					$_SESSION['app7020ChannelData']['vt'] = Yii::app()->request->getParam('visibility_type', false);
					$_SESSION['app7020ChannelData']['groups'] = json_decode(Yii::app()->request->getParam('selected_groups', false));
					$_SESSION['app7020ChannelData']['branches'] = $branches;
				}
				$partial = 'app7020.protected.views.channelsManagement.modals._addChannelPermissions';
				$this->renderPartial($partial, array(
					'data' => $data,
					'session' => $_SESSION['app7020ChannelData'],
					'defaultBrowseLangCode' => $langBrowserCode,
					'activeLanguages' => $activeLanguagesSetDefault,
					)
				);
				break;
			/**
			 * Add/Edit channel finish step
			 */
			case 'init_permissions':
				/**
				 * Unset session if exists
				 */
				$_SESSION['app7020ChannelData'] = array();

				$_SESSION['app7020ChannelData']['id'] = (int) Yii::app()->request->getParam('id', false);

				// SELECT Information from DB to EDIT topic
				if (!empty($_SESSION['app7020ChannelData']['id'])) {
					$_SESSION['app7020ChannelData']['trans'] = App7020Channels::getChannelTranslationsById($_SESSION['app7020ChannelData']['id']);
					$data['channel'] = App7020Channels::model()->findByPk($_SESSION['app7020ChannelData']['id']);
					$preselected = App7020ChannelVisibility::getAxSelectedGroupsAndBranchesForSelector($_SESSION['app7020ChannelData']['id']);

					$_SESSION['app7020ChannelData']['icon'] = $data['channel']['icon_fa'];
					$_SESSION['app7020ChannelData']['color'] = $data['channel']['icon_color'];
					$_SESSION['app7020ChannelData']['bgrcolor'] = $data['channel']['icon_bgr'];
					$_SESSION['app7020ChannelData']['permissions'] = $data['channel']['permission_type'];
					$_SESSION['app7020ChannelData']['groups'] = $preselected['groups'];
					$_SESSION['app7020ChannelData']['branches'] = $preselected['branches'];
				}
				$partial = 'app7020.protected.views.channelsManagement.modals._addChannelPermissions';
				$this->renderPartial($partial, array(
					'data' => $data,
					'session' => $_SESSION['app7020ChannelData'],
					'defaultBrowseLangCode' => $langBrowserCode,
					'activeLanguages' => $activeLanguagesSetDefault,
					)
				);
				break;
			case 'step_finish':
				$data['channel'] = App7020Channels::model()->findByPk($_SESSION['app7020ChannelData']['id']);
				if (!empty($data['channel']['predefined_uid'])) {
					$_SESSION['app7020ChannelData']['vt'] = Yii::app()->request->getParam('visibility_type', false);
					$_SESSION['app7020ChannelData']['groups'] = json_decode(Yii::app()->request->getParam('selected_groups', false));

					$branches = array();
					$branchjson = json_decode(Yii::app()->request->getParam('orgcharts_selected-json', false));
					if (count($branchjson) > 0) {
						foreach ($branchjson as $elem) {
							$branches[] = $elem;
						}
					}
					$_SESSION['app7020ChannelData']['branches'] = $branches;
				} else {
					$_SESSION['app7020ChannelData']['permissions'] = Yii::app()->request->getParam('permissions', false);
				}


				$partial = 'app7020.protected.views.channelsManagement.modals._addChannelFinish';
				$this->renderPartial($partial, array(
					'data' => $data,
					'session' => $_SESSION['app7020ChannelData'],
					'defaultBrowseLangCode' => $langBrowserCode,
					'activeLanguages' => $activeLanguagesSetDefault,
					)
				);
				break;
			/**
			 * Save channel changes
			 */
			case 'step_save':
				if (isset($_POST['save_new'])) {
					$action = 'new';
				}

				if (isset($_POST['save_assign'])) {
					$action = 'assign';
				}

				if (isset($_POST['save_clone'])) {
					$action = 'clone';
				}

				if (isset($_POST['finish_step'])) {
					$action = 'finish';
				}

				$c = App7020Channels::model()->findByAttributes(array('id' => $_SESSION['app7020ChannelData']['id']));

				if (empty($c)) {
					$c = new App7020Channels();
					$c->type = 2;
					$c->ordering = App7020Channels::getOrdering();
				}
				$c->icon_fa = $_SESSION['app7020ChannelData']['icon'];
				$c->icon_color = $_SESSION['app7020ChannelData']['color'];
				$c->icon_bgr = $_SESSION['app7020ChannelData']['bgrcolor'];
				$c->sort_items = $_SESSION['app7020ChannelData']['sort'];
				$c->permission_type = $_SESSION['app7020ChannelData']['permissions'];
				$c->save();

				$_SESSION['app7020ChannelData']['id'] = $c->id;
				if (count($_SESSION['app7020ChannelData']['langs']) > 0) {

					foreach ($_SESSION['app7020ChannelData']['langs'] as $lang => $trans) {
						$trimmed = trim($trans['title']);
						$m = App7020ChannelTranslation::model()->findByAttributes(array('idChannel' => $_SESSION['app7020ChannelData']['id'], 'lang' => $lang));
						if (!$m) {
							if (!empty($trimmed)) {
								$m = new App7020ChannelTranslation();
								$m->lang = $lang;
								$m->idChannel = $_SESSION['app7020ChannelData']['id'];
								$m->name = $trans['title'];
								$m->description = $trans['description'];
								$m->save(false);
							}
						} else {
							if (!empty($trimmed)) {
								$m->name = $trans['title'];
								$m->description = $trans['description'];
								$m->save(false);
							} else {
								$m->delete();
							}
						}
					}
				}

				if ($_SESSION['app7020ChannelData']['vt'] == 'selection') {
					App7020ChannelVisibility::secureDelete((int) $_SESSION['app7020ChannelData']['id']);
					if (count($_SESSION['app7020ChannelData']['groups']) > 0) {
						foreach ($_SESSION['app7020ChannelData']['groups'] as $key => $value) {
							$n = new App7020ChannelVisibility();
							$n->idChannel = $_SESSION['app7020ChannelData']['id'];
							$n->idObject = $value;
							$n->selectState = 0;
							$n->type = 1;
							$n->save();
						}
					}
					if (count($_SESSION['app7020ChannelData']['branches']) > 0) {
						foreach ($_SESSION['app7020ChannelData']['branches'] as $key => $value) {
							$n = new App7020ChannelVisibility();
							$n->idChannel = $_SESSION['app7020ChannelData']['id'];
							if (is_array($value)) {
								$n->idObject = $value['key'];
								$n->selectState = $value['selectState'];
							} else {
								$n->idObject = $value->key;
								$n->selectState = $value->selectState;
							}
							$n->type = 2;
							$n->save();
						}
					}
				}
				else if ($_SESSION['app7020ChannelData']['vt'] == 'public') {
					App7020ChannelVisibility::secureDelete((int) $_SESSION['app7020ChannelData']['id']);
 				}

				echo '<a class="auto-close success ' . $action . '"></a>';
				echo '<a class="data">' . CJSON::encode($_SESSION['app7020ChannelData']['id']) . '</a>';
				if ($action != 'clone') {
					unset($_SESSION['app7020ChannelData']);
				}
				Yii::app()->end();
				break;
			/**
			 * Delete Channel
			 */
			case 'delete' :
				$_SESSION['app7020ChannelData'] = array();
				$_SESSION['app7020ChannelData']['id'] = (int) Yii::app()->request->getParam('id', false);
				$data['id'] = $_SESSION['app7020ChannelData']['id'];
				$trans = App7020Channels::getChannelTranslationsById($_SESSION['app7020ChannelData']['id']);
				foreach ($trans as $t) {
					$lang = $t['lang'];
					$_SESSION['app7020ChannelData']['trans'][$lang]['name'] = $t['name'];
				}
				$data['assets'] = App7020ChannelAssets::getAssetsByChannelId($_SESSION['app7020ChannelData']['id']);
				$_SESSION['app7020ChannelData']['assets'] = $data['assets'];

				$partial = 'app7020.protected.views.channelsManagement.modals._deleteChannel';
				$this->renderPartial($partial, array(
					'data' => $data,
					'session' => $_SESSION['app7020ChannelData'],
					'defaultBrowseLangCode' => $langBrowserCode,
					'currentLanguage' => $currentLanguage
					)
				);
				break;
			case 'delete_finish':

				$channels = Yii::app()->request->getParam('channnelsCheckList', false);
				$hasAssets = App7020ChannelAssets::model()->findByAttributes(array("idChannel" => $_SESSION['app7020ChannelData']['id']));
				if (($_POST['confirmDelete'] == 1 && $channels) || !$hasAssets) {
					if (count($_SESSION['app7020ChannelData']['assets']) > 0) {
						if (count($channels) > 0) {
							foreach ($channels as $channel) {
								foreach ($_SESSION['app7020ChannelData']['assets'] as $asset) {
									$m = new App7020ChannelAssets();
									$m->idChannel = $channel;
									$m->idAsset = $asset['idAsset'];
									$m->asset_type = $asset['asset_type'];
									$m->save();
								}
							}
						}
					}
					App7020Channels::model()->findByPk($_SESSION['app7020ChannelData']['id'])->delete();

					echo '<a class="auto-close success delete"></a>';
					echo '<a class="data">' . CJSON::encode($_SESSION['app7020ChannelData']['id']) . '</a>';

					unset($_SESSION['app7020ChannelData']);
					Yii::app()->end();
				} else {
					$data['id'] = $_SESSION['app7020ChannelData']['id'];
					$trans = App7020Channels::getChannelTranslationsById($_SESSION['app7020ChannelData']['id']);
					foreach ($trans as $t) {
						$lang = $t['lang'];
						$_SESSION['app7020ChannelData']['trans'][$lang]['name'] = $t['name'];
					}
					$data['assets'] = App7020ChannelAssets::getAssetsByChannelId($_SESSION['app7020ChannelData']['id']);
					$_SESSION['app7020ChannelData']['assets'] = $data['assets'];

					$partial = 'app7020.protected.views.channelsManagement.modals._deleteChannel';
					$this->renderPartial($partial, array(
						'data' => $data,
						'session' => $_SESSION['app7020ChannelData'],
						'defaultBrowseLangCode' => $langBrowserCode,
						)
					);
				}
				break;
			/**
			 * Clone channel
			 */
			case 'clone' :

				if (empty($prev)) {
					if (isset($_SESSION['app7020ChannelData'])) {
						unset($_SESSION['app7020ChannelData']);
						$_SESSION['app7020ChannelData'] = array();
					}

					$id = (int) Yii::app()->request->getParam('id', false);
					$data['channel'] = App7020Channels::model()->findByPk($id);
					$preselected = App7020ChannelVisibility::getAxSelectedGroupsAndBranchesForSelector($id);
					$_SESSION['app7020ChannelData'] = array(
						'id' => $id,
						'trans' => App7020Channels::getChannelTranslationsById($id),
						'icon' => $data['channel']['icon_fa'],
						'color' => $data['channel']['icon_color'],
						'bgrcolor' => $data['channel']['icon_bgr'],
						'permissions' => $data['channel']['permission_type'],
						'sort' => $data['channel']['sort_items'],
						'groups' => $preselected['groups'],
						'branches' => $preselected['branches']
					);
				}

				$partial = 'app7020.protected.views.channelsManagement.modals._addChannelStep1';
				$this->renderPartial($partial, array(
					'data' => $data,
					'session' => $_SESSION['app7020ChannelData'],
					'defaultBrowseLangCode' => $langBrowserCode,
					'activeLanguages' => $activeLanguagesSetDefault,
					)
				);
				break;
			/**
			 * Clone channel
			 */
			case 'clone_step2' :
				if (empty($prev)) {
					$_SESSION['app7020ChannelData']['langs'] = Yii::app()->request->getParam('langArr', false);
				}
				$partial = 'app7020.protected.views.channelsManagement.modals._addChannelStep2';
				$this->renderPartial($partial, array(
					'data' => $data,
					'session' => $_SESSION['app7020ChannelData'],
					'defaultBrowseLangCode' => $langBrowserCode,
					'activeLanguages' => $activeLanguagesSetDefault,
					'currentLanguage' => $currentLanguage
					)
				);
				break;
			case 'clone_finish' :

				$_SESSION['app7020ChannelData']['icon'] = Yii::app()->request->getParam('channelIcon', false);
				$_SESSION['app7020ChannelData']['color'] = Yii::app()->request->getParam('color', false);
				$_SESSION['app7020ChannelData']['bgrcolor'] = Yii::app()->request->getParam('background-color', false);
				$_SESSION['app7020ChannelData']['sort'] = Yii::app()->request->getParam('hidden-sort', false);

				$experts = App7020ChannelExperts::model()->findAllByAttributes(array('idChannel' => $_SESSION['app7020ChannelData']['id']));

				$c = new App7020Channels();
				$c->type = 2;
				$c->ordering = App7020Channels::getOrdering();
				$c->icon_fa = $_SESSION['app7020ChannelData']['icon'];
				$c->icon_color = $_SESSION['app7020ChannelData']['color'];
				$c->icon_bgr = $_SESSION['app7020ChannelData']['bgrcolor'];
				$c->permission_type = $_SESSION['app7020ChannelData']['permissions'];
				$c->sort_items = $_SESSION['app7020ChannelData']['sort'];
				$c->save();

				$_SESSION['app7020ChannelData']['id'] = $c->id;

				if (count($_SESSION['app7020ChannelData']['langs']) > 0) {

					foreach ($_SESSION['app7020ChannelData']['langs'] as $lang => $trans) {
						$trimmed = trim($trans['title']);
						if (!empty($trimmed)) {
							$m = new App7020ChannelTranslation();
							$m->lang = $lang;
							$m->idChannel = $_SESSION['app7020ChannelData']['id'];
							$m->name = $trans['title'];
							$m->description = $trans['description'];
							$m->save();
						}
					}
				}

				if (count($_SESSION['app7020ChannelData']['groups']) > 0) {
					foreach ($_SESSION['app7020ChannelData']['groups'] as $key => $value) {
						$n = new App7020ChannelVisibility();
						$n->idChannel = $_SESSION['app7020ChannelData']['id'];
						$n->idObject = $value;
						$n->type = 1;
						$n->save();
					}
				}
				if (count($_SESSION['app7020ChannelData']['branches']) > 0) {
					foreach ($_SESSION['app7020ChannelData']['branches'] as $key => $value) {
						$n = new App7020ChannelVisibility();
						$n->idChannel = $_SESSION['app7020ChannelData']['id'];
						$n->idObject = $value;
						$n->type = 2;
						$n->save();
					}
				}


				if ($experts) {
					foreach ($experts as $expert) {
						$ex = new App7020ChannelExperts();
						$ex->idChannel = $_SESSION['app7020ChannelData']['id'];
						$ex->idExpert = $expert->idExpert;
						$ex->save();
					}
				}


				echo '<a class="auto-close success cloned"></a>';
				echo '<a class="data">' . CJSON::encode($_SESSION['app7020ChannelData']['id']) . '</a>';
				unset($_SESSION['app7020ChannelData']);
				Yii::app()->end();

				break;
		}
	}

	public function actionAxToggleShowHide() {
		/* Show: 1 | Hide: 0 */
		$channelId = Yii::app()->request->getParam('channelId', false);
		$channel = App7020Channels::model()->findByPk($channelId);
		if ($channel) {
			$channel->enabled = ($channel->enabled == 0) ? 1 : 0;
			$channel->save();
			echo json_encode(array('result' => true, 'enabled' => $channel->enabled));
			Yii::app()->end();
		}
		echo json_encode(array('result' => false));
		Yii::app()->end();
	}

	public function actionAxChannelsDranAndDrop() {
		$sortedChannels = Yii::app()->request->getParam('sorted', array());
		if ($sortedChannels) {
			foreach ($sortedChannels as $key => $value) {
				$idChannel = (int) $value;
				$channelModel = App7020Channels::model()->findByPk($idChannel);
				if ($channelModel->id) {
					$channelModel->ordering = $key + 1;
					$channelModel->save();
				}
			}
			echo json_encode(array('success' => true));
			Yii::app()->end();
		}
		echo json_encode(array('success' => false));
		Yii::app()->end();
	}

	/**
	 * Provides selector with users to assign to a particular channels (as experts)
	 *
	 * Apply assignment upon submition (as a Collector of the selector)
	 *
	 */
	public function actionAxAssignExperts() {

		$idChannel = Yii::app()->request->getParam("idChannel", FALSE);
		$confirm = Yii::app()->request->getParam("confirm", FALSE);

		$transaction = null;
		if (Yii::app()->db->getCurrentTransaction() === NULL) {
			$transaction = Yii::app()->db->beginTransaction();
		}

		try {
			$model = App7020Channels::model()->findByPk($idChannel);
			if (!$model) {
				throw new Exception('Invalid session or course');
			}

			// Confirm button has been clicked ?
			if ($confirm) {
				$selectedUsers = UsersSelector::getUsersListOnly($_REQUEST);
				$model->assignExperts(UsersSelector::getUsersListOnly($_REQUEST));
				echo '<a class="auto-close"></a>';
				if ($transaction)
					$transaction->commit();

				Yii::app()->event->raise(EventManager::EVENT_APP7020_EXPERTS_ASSIGNED_TO_CHANNEL, new DEvent($this, array(
					'channelModel' => $model,
					'experts' => $selectedUsers,
					'superAdminId' => Yii::app()->user->idst
				)));


				Yii::app()->end();
			}

			// Where selector will submit its data to be collected (THIS action again)
			$collectorUrl = Docebo::createApp7020Url('axChannels/axAssignExperts', array(
					'idChannel' => $idChannel,
			));

			$selectorDialogUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
					'idGeneral' => $idChannel, // The session ID, passed along the AJAX request
					'type' => UsersSelector::TYPE_APP7020_ASSIGN_EXPERTS,
					'collectorUrl' => $collectorUrl, // Make this action also collector of the users selector submition
			));

			// Redirect to UsersSelector
			$this->redirect($selectorDialogUrl, true);
			Yii::app()->end();
		} catch (Exception $e) {
			if ($transaction)
				$transaction->rollback();
			$this->renderPartial('admin.protected.views.common/_dialog2_error', array(
				'message' => $e->getMessage(),
				'type' => 'error',
			));
			Yii::app()->end();
		}
	}

	public function actionUnAssignExperts() {
		$idChannel = (int) Yii::app()->request->getParam("idChannel", false);
		$idExperts = explode(",", Yii::app()->request->getParam("idExperts", false));

		$channelModel = App7020Channels::model()->findByPk($idChannel);

		if (isset($_POST['yt0']) && $_POST['agreeCheck'] == 1 && $channelModel) {
			if (count($idExperts) == 1) {
				$m = App7020ChannelExperts::model()->findByAttributes(array(
					'idChannel' => $idChannel,
					'idExpert' => $idExperts,
				));
				$m->delete();
				echo '<a class="auto-close"></a>';
			} elseif (count($idExperts) > 1) {
				$m = App7020ChannelExperts::model()->deleteAllByAttributes(array('idChannel' => $idChannel), 'idExpert IN (' . implode(',', $idExperts) . ')');
				echo '<a class="auto-close"></a>';
			}

			if (is_array($idExperts) && !empty($idExperts)) {
				Yii::app()->end();
			}
		}
		$this->renderPartial('app7020.protected.views.channelsManagement._unAssignExpertDialog', array(
			'idChannel' => $idChannel,
			'idExperts' => $idExperts,
		));
	}

	public function actionMassActionExperts() {
		$data = Yii::app()->request->getParam('data', false);
		$dataProvider = App7020ChannelExperts::model()->sqlDataProvider();
		$result = Null;

		switch ($data['action']) {
			default : break;

			case 'delete':
				if ($data['listId'] == 'expertsComboListView') {
					$result['data'] = $data['selection'];
					$result['count'] = count($data['selection']);
				}
				break;
		}
		echo json_encode($result);
	}

	/**
	 * Get all mached tags
	 */
	public function actionSortAutoComplete() {
		// Get search tag
		$tag = Yii::app()->request->getParam('tag', false);
		$sort = array(
			'0' => array(
				'key' => 'name_asc',
				'value' => Yii::t('app7020', 'Item name (Ascending)')
			),
			'1' => array(
				'key' => 'name_desc',
				'value' => Yii::t('app7020', 'Item name (Descending)')
			),
			'2' => array(
				'key' => 'date_asc',
				'value' => Yii::t('app7020', 'Creation Date (Ascending)')
			),
			'3' => array(
				'key' => 'date_desc',
				'value' => Yii::t('app7020', 'Creation Date (Descending)')
			),
		);
		echo CJSON::encode($sort);
	}

}
