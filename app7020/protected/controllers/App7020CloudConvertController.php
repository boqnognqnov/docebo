<?php

/**
 * App7020AmazonSnsController
 * This class will wait for SNS response from amazon service about jobs posted by App7020
 * @author Stanimir
 */
class App7020CloudConvertController extends Controller {

	public function actionWebConvertNotify() {
		$storage = CFileStorage::getSystemFilesStorage(CFileStorage::COLLECTION_NONE);

		if (!empty($_REQUEST['step']) && !empty($_REQUEST['id'])) {
			$step = $_REQUEST['step'];
			$idPipe = $_REQUEST['id'];
			$convertionObject = App7020DocumentConversions::model()->findByAttributes(array('idPipe' => $idPipe));
			if ($convertionObject->id) {
				$contentObject = App7020Assets::model()->findByPk($convertionObject->idContent);
				if ($contentObject->id) {
					if ($step == 'finished') {

						$url = $_REQUEST['url'];

						if (strpos($url, 'http') === false) {
							$url = "https:" . $url;
						}

						$cc = new CloudConvert();
						$httpRequest = $cc->httpRequest($url);

						$newFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $contentObject->filename;
						$storage = CFileStorage::getSystemFilesStorage(CFileStorage::COLLECTION_NONE);
						$storage->downloadAs($contentObject->filename, $newFilePath, Yii::app()->params['cloudconvert']['web_convert_s3_temp_folder']);


						$coreAsset = new CoreAsset();
						$coreAsset->type = CoreAsset::TYPE_APP7020;
						$coreAsset->sourceFile = $newFilePath;
						$coreAsset->save();
						$contentObject->conversion_status = App7020Assets::CONVERSION_STATUS_SNS_SUCCESS;
						$contentObject->idThumbnail = $coreAsset->id;
					} else {
						$contentObject->conversion_status = App7020Assets::CONVERSION_STATUS_SNS_ERROR;
					}
					$contentObject->save(false);
					$convertionObject->used = 1;
					$convertionObject->save();
				}
			}
		}
		//App7020DocumentConversions::model()->deleteAllByAttributes(array('used' => 1));
	}

	public function actionDocConvertNotify() {

		if (!empty($_REQUEST['step']) && !empty($_REQUEST['id'])) {
			$step = $_REQUEST['step'];
			$idPipe = $_REQUEST['id'];
			$convertionObject = App7020DocumentConversions::model()->findByAttributes(array('idPipe' => $idPipe));
			if ($convertionObject->id) {
				$contentObject = App7020Assets::model()->findByPk($convertionObject->idContent);
				if ($contentObject->id) {
					if ($step == 'finished') {
						$contentObject->conversion_status = App7020Assets::CONVERSION_STATUS_SNS_SUCCESS;

						$images = App7020DocumentImages::getDocumentImagesFromS3($contentObject->filename);
						foreach ($images as $value) {
							$imageModel = App7020DocumentImages::model()->findByAttributes(array('imageName' => basename($value)));
							if (!$imageModel->id) {
								$imageObject = new App7020DocumentImages();
								$imageObject->idContent = $contentObject->id;
								$imageObject->imageName = basename($value);
								$imageObject->save();
							}
						}
					} else {
						$contentObject->conversion_status = App7020Assets::CONVERSION_STATUS_SNS_ERROR;
					}
					$contentObject->save(false);
					$convertionObject->used = 1;
					$convertionObject->save();
				}
			}
		}

		App7020DocumentConversions::model()->deleteAllByAttributes(array('used' => 1));
	}

}
