<?php

class ExpertsController extends App7020BaseController {

	public function init() {
		$themeUrl = Yii::app()->theme->baseUrl;
		Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/user_management.js', CClientScript::POS_HEAD);
		//Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/scriptModal.js', CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/confirm/jquery.confirm.js', CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerCssFile($themeUrl . '/css/user_management.css');
		$ft = new FancyTree();
		Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree-all.js', CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree.docebo.js', CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerCssFile($ft->getAssetsUrl() . '/skin-docebo/ui.fancytree.css');
		UsersSelector::registerClientScripts();
		parent::init();
	}

	public function actionIndex() {
		// Register Fancytree JS/CSS


		$this->breadcrumbs = array(
			Yii::t('app7020', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
			Yii::t('app7020', 'Coach'),
			Yii::t('app7020', 'Experts'),
		);

		if (isset($_REQUEST['selectAll'])) {
			$dataProvider = App7020Experts::sqlDataProvider();
			$dataProvider->pagination = false;
			$keys = $dataProvider->keys;
			foreach ($keys as $key => $value) {
				$keys[$key] = (int) $value;
			}
			$this->sendJSON(CJSON::encode($keys));
			Yii::app()->end();
		}

		// Autocomplete request?
		if (isset($_REQUEST['autocomplete'])) {
			$dataProvider = App7020Experts::sqlDataProvider(array('search_input' => $_REQUEST['term']));
			$result = array();
			foreach ($dataProvider->data as $user) {
				$result[] = array('label' => $user['expertNames'] . ' (' . $user['username'] . ')', 'value' => $user['expertNames']);
			}
			$this->sendJSON($result);
		}

		$selectedColumns = $this->getSelectedColumns();
		$selectedColumns['topics'] = array(
			'type' => 'raw',
			'header' => Yii::t('app7020', "Assigned topics"),
			'value' => 'App7020Experts::renderTopicsLink($data["expertId"], $data["topicsCount"]);',
			'htmlOptions' => array
				(
				'class' => 'td-ctopics'
			),
		);
		$selectedColumns['delete'] = array(
			'type' => 'raw',
			'value' => 'App7020Experts::renderDeleteLink($data["expertId"]);',
			'htmlOptions' => array
				(
				'class' => 'td-cdelete'
			),
		);

		$usersGridId = "userman-users-management-grid";
		$userManAction = Yii::app()->controller->id . "/" . Yii::app()->controller->action->id;
		if (isset($_REQUEST['ajax']) && ($_REQUEST['ajax'] == $usersGridId)) {
			$dataProvider = App7020Experts::sqlDataProvider();
			$this->renderPartial('_usersGrid', array(
				'usersGridId' => $usersGridId,
				'selectedColumns' => $selectedColumns,
				'dataProvider' => $dataProvider
			));
		} else {
			$this->render('index', array(
				'selectedColumns' => $selectedColumns,
				'usersGridId' => $usersGridId,
				'userManAction' => $userManAction
			));
		}
	}

	public function actionTopics() {
		// Register Fancytree JS/CSS


		$this->breadcrumbs = array(
			Yii::t('app7020', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
			Yii::t('app7020', 'Coach'),
			Yii::t('app7020', 'Settings') => Docebo::createAppUrl('app7020:experts/index'),
			Yii::t('app7020', 'Assign topics'),
		);

		$expertId = $this->_detectExpertRefererId();

		if (isset($_REQUEST['comboListViewSelectAll'])) {
			$dataProvider = App7020TopicExpert::getExpertsTopicsTree($expertId, array('pagination' => false));
			$keys = $dataProvider->keys;
			foreach ($keys as $key => $value) {
				$keys[$key] = (int) $value;
			}
			$this->sendJSON(CJSON::encode($keys));
			Yii::app()->end();
		}

		$expertName = CoreUser::model()->getFullNameById($expertId);
		$provider = App7020TopicExpert::getExpertsTopicsTree($expertId);
		$listParams = array(
			'massiveActions' => array('deleteExperts' => Yii::t('standard', '_DEL')),
			'listId' => "topicsComboListView",
			'dataProvider' => $provider,
			'restyleAfterUpdate' => false,
			'listItemView' => 'app7020.protected.views.experts._topicsListViewItem',
			'columns' => 2,
			'template' => '<div class="expertLabel">' . Yii::t('app7020', 'Assigned topics to:') . ' <span>' . $expertName . '</span></div>'
			. '{items}{pager}{summary}'
		);
		if (isset($_REQUEST['ajax'])) {
			$this->widget('common.widgets.ComboListView', $listParams);
			Yii::app()->end();
		}


		$this->render('topics', array('listParams' => $listParams, 'expertId' => $expertId));
	}

	public function getSelectedColumns() {
		$columnsArray = array(
			array(
				'class' => 'CCheckBoxColumn',
				'id' => 'userman-grid-checkboxes'
			),
			array(
				'name' => 'username',
				'header' => Yii::t('standard', "_USERNAME"),
				'htmlOptions' => array
					(
					'class' => 'td-username'
				),
				'headerHtmlOptions' => array
					(
					'class' => 'td-username'
				)
			),
			array(
				'name' => 'expertNames',
				'header' => Yii::t('app7020', "Expert full name"),
				'htmlOptions' => array
					(
					'class' => 'td-expertnames'
				),
				'headerHtmlOptions' => array
					(
					'class' => 'td-expertnames'
				)
			),
			array(
				'name' => 'email',
				'header' => Yii::t('standard', "_EMAIL"),
				'htmlOptions' => array
					(
					'class' => 'td-email'
				),
				'headerHtmlOptions' => array
					(
					'class' => 'td-email'
				)
			),
		);
		return $columnsArray;
	}

	public function actionDialogSelectExperts() {
		$confirm = Yii::app()->request->getParam("confirm", FALSE);

		try {
			// Confirm button has been clicked ?
			if ($confirm) {
				$selectedUsers = UsersSelector::getUsersListOnly($_REQUEST);
				foreach ($selectedUsers as $value) {
					$expertModel = App7020Experts::model()->findByAttributes(array('idUser' => $value));
					if (!$expertModel->id) {
						$newExpert = new App7020Experts();
						$newExpert->idUser = $value;
						$newExpert->idAdmin = Yii::app()->user->idst;
						$newExpert->save();
					}
				}
				if ($selectedUsers) {
					$this->renderPartial("_progressAddExperts", array('expertsCount' => count($selectedUsers)));
				} else {
					Yii::app()->user->setFlash('error', Yii::t('app7020', 'Select at least one user'));
					$collectorUrl = Docebo::createApp7020Url('experts/dialogSelectExperts', array());

					$selectorDialogUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
								'type' => UsersSelector::TYPE_APP7020_ADD_EXPERTS,
								'collectorUrl' => $collectorUrl, // Make this action also collector of the users selector submition
					));
					// Redirect to UsersSelector
					$this->redirect($selectorDialogUrl, true);
					//echo '<a class="auto-close"></a>'; s
				}
				Yii::app()->end();
			}

			// Where selector will submit its data to be collected (THIS action again)
			$collectorUrl = Docebo::createApp7020Url('experts/dialogSelectExperts', array());

			$selectorDialogUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
						'type' => UsersSelector::TYPE_APP7020_ADD_EXPERTS,
						'collectorUrl' => $collectorUrl, // Make this action also collector of the users selector submition
			));
			// Redirect to UsersSelector
			$this->redirect($selectorDialogUrl, true);
			Yii::app()->end();
		} catch (Exception $e) {

			$this->renderPartial('admin.protected.views.common/_dialog2_error', array(
				'message' => $e->getMessage(),
				'type' => 'error',
			));
			Yii::app()->end();
		}
	}

	/**
	 * @depricated 
	 */
	public function ______actionDialogMultipleAssign() {
		if (!App7020TopicTree::model()->findAll()) {
			$this->renderPartial('_dialogEmptyTopics');
		} elseif (!App7020Experts::model()->findAll()) {
			$this->renderPartial('_dialogEmptyExperts');
		} else {
			$confirm = Yii::app()->request->getParam("confirm", FALSE);

			try {
				// Confirm button has been clicked ?
				if ($confirm) {
					$selectedUsers = Yii::app()->request->getParam("selected_users", false);
					$selectedUsersDecoded = json_decode($selectedUsers);
					if (!$selectedUsersDecoded) {

						$collectorUrl = Docebo::createApp7020Url('experts/dialogMultipleAssign', array());
						$preselectedUsers = Yii::app()->request->getParam("preselectedUsers", array());
						$_SESSION['app7020_preselected_topics'] = Yii::app()->request->getParam("preselectedTopics", false);

						if ($preselectedUsers) {
							$preselectedUsers = json_decode($preselectedUsers);
						}

						$selectorDialogUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
									'type' => UsersSelector::TYPE_APP7020_EXPERTS,
									'preselectedExperts' => $preselectedUsers,
									'collectorUrl' => $collectorUrl, // Make this action also collector of the users selector submition
						));
						Yii::app()->user->setFlash('error', Yii::t('app7020', 'Select at least one user'));
						// Redirect to UsersSelector
						$this->redirect($selectorDialogUrl, true);
						Yii::app()->end();
					}

					$url = Docebo::createAppUrl('app7020:experts/AxMultipleSelectTopics');
					$preselected = empty($_SESSION['app7020_preselected_topics']) ? array() : $_SESSION['app7020_preselected_topics'];
					$preselectedTopics = array();
					foreach ($preselected as $value) {
						$preselectedTopics[] = array("key" => $value, "selectState" => 1);
					}

					if (isset($_SESSION['app7020_preselected_topics'])) {
						unset($_SESSION['app7020_preselected_topics']);
					}

					$this->renderPartial('_dialogSelectTopics', array(
						'expertId' => $selectedUsers,
						'url' => $url,
						'preselected' => $preselectedTopics,
						'showPrevious' => 1
					));

					Yii::app()->end();
				}

				// Where selector will submit its data to be collected (THIS action again)
				$collectorUrl = Docebo::createApp7020Url('experts/dialogMultipleAssign', array());
				$preselectedUsers = Yii::app()->request->getParam("preselectedUsers", array());
				$_SESSION['app7020_preselected_topics'] = Yii::app()->request->getParam("preselectedTopics", false);

				if ($preselectedUsers) {
					$preselectedUsers = json_decode($preselectedUsers);
				}

				$selectorDialogUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
							'type' => UsersSelector::TYPE_APP7020_EXPERTS,
							'preselectedExperts' => $preselectedUsers,
							'collectorUrl' => $collectorUrl, // Make this action also collector of the users selector submition
				));

				// Redirect to UsersSelector
				$this->redirect($selectorDialogUrl, true);
				Yii::app()->end();
			} catch (Exception $e) {

				$this->renderPartial('admin.protected.views.common/_dialog2_error', array(
					'message' => $e->getMessage(),
					'type' => 'error',
				));
				Yii::app()->end();
			}
		}
	}

	/**
	 * @param $idUser Primary Keys of users
	 * @return string String of json code with success and message
	 */
	public function actionDeleteExpert($idUser = null) {
		$users = array();
		if (!empty($_POST['idUser'])) {
			$idUser = $_POST['idUser'];
			if (is_string($idUser)) {
				if (stripos($idUser, ',') !== false)
					$users = explode(',', $idUser);
				else
					$users = array($idUser);
			}
		} elseif (!empty($idUser)) {
			$users = array($idUser);
		}

		if (empty($users)) {
			$this->sendJSON(array());
		}

		if (!empty($_POST['agreeCheck'])) {
			$experts = explode(",", $_POST['idUser']);
			foreach ($experts as $expertId) {
				$expertObject = App7020Experts::model()->findByAttributes(array('idUser' => $expertId));
				if ($expertObject->id) {
					$expertObject->delete();
				}
			}
			echo '<a class="auto-close success"></a>';
			Yii::app()->end();
		}

		if (Yii::app()->request->isAjaxRequest) {
			$content = $this->renderPartial('_deleteExperts', array(
				'users' => $users,
				'idUser' => $idUser
					), false, true);
		}
		Yii::app()->end();
	}

	public function actionAxDialogSelectTopics() {
		$idExpert = $this->_detectExpertRefererId();
		$selected_nodes = Yii::app()->request->getParam('fancytree_selected_nodes', false);
		$url = Docebo::createAppUrl('app7020:experts/AxDialogSelectTopics');
		if (Yii::app()->request->getParam('yt0', false)) {
			App7020TopicExpert::model()->deleteAllByAttributes(array('idExpert' => $idExpert));
			if ($selected_nodes) {
				foreach ($selected_nodes as $key => $value) {
					$expertTopics = new App7020TopicExpert();
					$expertTopics->idExpert = $idExpert;
					$expertTopics->idTopic = $key;
					$expertTopics->save();
				}
			}
			echo '<a class="auto-close success"></a>';
			Yii::app()->end();
		}

		$preselected = array();
		$selectedTopics = App7020TopicExpert::getExpertsTopicsTree($idExpert, array('pagination' => false));
		foreach ($selectedTopics->data as $value) {
			$preselected[] = array("key" => $value['id'], "selectState" => 1);
		}
		$this->renderPartial('_dialogSelectTopics', array(
			'expertId' => $idExpert,
			'url' => $url,
			'preselected' => $preselected
		));
	}

	/**
	 * Return id by Referer from AJAX
	 * @return boolean|integer
	 */
	private function _detectExpertRefererId() {

		if (Yii::app()->request->getParam('expertId', false)) {
			$id = Yii::app()->request->getParam('expertId');
		} else if (isset($_SERVER['HTTP_REFERER'])) {
			$url = $_SERVER['HTTP_REFERER'];
			preg_match('/expertId=[0-9A-Za-z]*/', $url, $matches);
			$value = explode("=", $matches[0]);
			$id = $value[1];
		} else {
			return false;
		}
		return (int) $id;
	}

	public function actionAxDeleteTopics() {
		$items = Yii::app()->request->getParam('items', false);
		$agreeCheck = Yii::app()->request->getParam('agreeCheck', false);
		$forDelete = Yii::app()->request->getParam('forDelete', false);
		$expertId = $this->_detectExpertRefererId();
		if ($forDelete) {
			$items = explode(',', $forDelete);
		}
		$this->renderPartial('_deleteExpertTopics', array('items' => $items));

		if (Yii::app()->request->isAjaxRequest && $agreeCheck && $forDelete) {
			$transaction = Yii::app()->db->beginTransaction();
			try {
				foreach ($items as $topicId) {
					$topicObject = App7020TopicExpert::model()->findByAttributes(array('idTopic' => $topicId, 'idExpert' => $expertId));
					if ($topicObject) {
						$topicObject->delete();
					} else {
						throw new Exception;
					}
				}
				$transaction->commit();
				echo '<a class="auto-close success"></a>';
				Yii::app()->end();
			} catch (Exception $e) {
				$transaction->rollback();
				echo '<a class="auto-close unsuccess"></a>';
				Yii::app()->end();
			}
		}
	}

	/**
	 * get selected experts and topics, and assigng it
	 */
	public function actionAxMultipleSelectTopics() {
		if (Yii::app()->request->getParam('yt1', false)) {
			$expertsString = Yii::app()->request->getParam('experts');
			if ($expertsString) {
				$experts = json_decode($expertsString);
			}
			$topics = Yii::app()->request->getParam('fancytree_selected_nodes');
			if (!$topics) {
				Yii::app()->user->setFlash('error', Yii::t('app7020', 'Select at least one topic'));

				$url = Docebo::createAppUrl('app7020:experts/AxMultipleSelectTopics');
				$preselectedTopics = array();

				$this->renderPartial('_dialogSelectTopics', array(
					'expertId' => $expertsString,
					'url' => $url,
					'preselected' => $preselectedTopics,
					'showPrevious' => 1
				));

				Yii::app()->end();
			}

			foreach ($experts as $expert) {
				foreach ($topics as $topic => $selected) {
					$model = App7020TopicExpert::model()->findByAttributes(array('idTopic' => $topic, 'idExpert' => $expert));
					if (!$model->id) {
						$newModel = new App7020TopicExpert();
						$newModel->idExpert = $expert;
						$newModel->idTopic = $topic;
						$newModel->save();
					}
				}
			}
			$this->renderPartial("_progressMultiple", array('expertsCount' => count($experts), 'topicsCount' => count($topics)));
			Yii::app()->end();
		}
	}

}
