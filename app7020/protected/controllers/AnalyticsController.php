<?php

class AnalyticsController extends App7020BaseController {

	public function init() {
		$themeUrl = Yii::app()->theme->baseUrl;
		Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/user_management.js', CClientScript::POS_HEAD);
		//Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/scriptModal.js', CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/confirm/jquery.confirm.js', CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerCssFile($themeUrl . '/css/user_management.css');
// @depricated
		
		$ft = new FancyTree();
		Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree-all.js', CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree.docebo.js', CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerCssFile($ft->getAssetsUrl() . '/skin-docebo/ui.fancytree.css');
		
		UsersSelector::registerClientScripts();
		parent::init();
	}

	/**
	 *
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl'
		);
	}

	/**
	 *
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		$res = array();
		$res[] = array(
			'allow',
			'users' => array(
				'*'
			),
			'actions' => array(
				'login'
			)
		);
		$res[] = array(
			'allow',
			'users' => array(
				'@'
			),
			'actions' => array(
				'logout',
				'index'
			)
		);
		return $res;
	}

	public function actionIndex() {
		
		$themeUrl = Yii::app()->theme->baseUrl;
		$this->getClientScript()->registerScriptFile($themeUrl . '/js/typehead/handlebars.js', CClientScript::POS_HEAD);
		$this->getClientScript()->registerScriptFile($themeUrl . '/js/tags/tagmanager.js', CClientScript::POS_HEAD);
		$this->getClientScript()->registerScriptFile($themeUrl . '/js/typehead/handlebars.js', CClientScript::POS_HEAD);
		$this->getClientScript()->registerScriptFile($themeUrl . '/js/typehead/' . (YII_DEBUG ? 'typeahead.jquery.min.js' : 'typeahead.jquery.js'), CClientScript::POS_HEAD);
		$this->getClientScript()->registerScriptFile($themeUrl . '/js/typehead/typeahead.bundle.js', CClientScript::POS_HEAD);
		$this->getClientScript()->registerCssFile($this->getAssetsUrl() . '/css/tags/app7020-tags.css');
		
		/* INIT REQUIREJS */
		$amdController = array();
		$amdController['data']['data-main'] = $this->getAssetsUrl() . '/js/analytics/main';
		Yii::app()->event->raise('registerAMDScript', new DEvent($this, $amdController));
		$assetId = Yii::app()->request->getParam('id', false);
		$asset = App7020Assets::model()->findByPk($assetId);
		if (!$asset && !Yii::app()->request->isAjaxRequest) {
			$this->redirect(array('knowledgeLibrary/index'));
			Yii::app()->end();
		}
		$this->breadcrumbs = array(
			Yii::t('app7020', 'Channels') => Docebo::createApp7020Url('channels/index'),
			Yii::t('app7020', $asset->title),
			Yii::t('app7020', 'Analytics')
		);

		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/admin.css');

		$selectedColumns = $this->getSelectedColumns();
		$selectedColumns['resend'] = array(
			'type' => 'raw',
			'header' => '',
			'value' => 'App7020Invitations::renderReinviteLink($data["idi"], $data["watched"]);',
			'htmlOptions' => array (
				'class' => 'td-reinvite'
			),
		);
		$selectedColumns['delete'] = array(
			'type' => 'raw',
			'value' => 'App7020Invitations::renderDeleteLink($data["idi"], $data["watched"]);',
			'htmlOptions' => array (
				'class' => 'td-delete'
			),
		);



		// Autocomplete request?
		if (isset($_REQUEST['autocomplete'])) {
			$dataProvider = App7020Invitations::getInvitationsDataProvider($assetId, $_REQUEST['term']);
			$result = array();
			foreach ($dataProvider->data as $user) {
				$result[] = array('label' => $user['fullName'] . ' (' . $user['username'] . ')', 'value' => $user['fullName']);
			}
			$this->sendJSON($result);
			Yii::app()->end();
		} else {
			$dataProvider = App7020Invitations::getInvitationsDataProvider($assetId);
		}

		$searchAction = Yii::app()->controller->id . "/" . Yii::app()->controller->action->id;

		$data_for_view = array(
			'asset' => $asset,
			'customTabsForm' => array(
				//'menuHeading' => '<span class="title-bold back-button title"><a href="' . Docebo::createAbsoluteApp7020Url("site/index" ,array('#'=>'/assets/view/'.$asset->id)) . '">back</a></span>Analytics',
				'activeTab' => 'manageInvitations',
				'menu' => array(
//					array('link-id' => 'overView',
//						'name' => Yii::t('app7020', 'Overview'),
//						'icon-id' => ''),
//					array('link-id' => 'social',
//						'name' => Yii::t('app7020', 'Social'),
//						'icon-id' => ''),
//					array('link-id' => 'askTheExpert',
//						'name' => Yii::t('app7020', 'Ask the expert'),
//						'icon-id' => ''),
					array('link-id' => 'manageInvitations',
						'name' => Yii::t('app7020', 'Manage invitations'),
						'icon-id' => ''),
				),
				'content' => array(
					array(
						'contentHeading' => Yii::t('app7020', 'Overview'),
						'viewName' => 'overView',
						'link-id' => 'overView',
						'sendDataToView' => '',
					),
					array(
						'contentHeading' => Yii::t('app7020', 'Social'),
						'viewName' => 'social',
						'link-id' => 'social',
						'sendDataToView' => '',
					),
					array(
						'contentHeading' => Yii::t('app7020', 'Ask the expert'),
						'viewName' => 'askTheExpert',
						'link-id' => 'askTheExpert',
						'sendDataToView' => '',
					),
					array(
						'contentHeading' => Yii::t('app7020', 'Manage invitations'),
						'viewName' => 'manageInvitations',
						'link-id' => 'manageInvitations',
						'sendDataToView' => array(
							'selectedColumns' => $selectedColumns,
							'idContent' => $assetId,
							'dataProvider' => $dataProvider,
							'searchAction' => $searchAction
						),
					),
				)
			)
		);

		$this->render('index', $data_for_view);
	}

	public function getSelectedColumns() {
		$columnsArray = array(
			array(
				'name' => 'username',
				'header' => Yii::t('standard', "_USERNAME"),
				'htmlOptions' => array
					(
					'class' => 'td-username'
				),
				'headerHtmlOptions' => array
					(
					'class' => 'td-username'
				)
			),
			array(
				'name' => 'fullName',
				'header' => Yii::t('app7020', "Full name"),
				'htmlOptions' => array
					(
					'class' => 'td-fullname'
				),
				'headerHtmlOptions' => array
					(
					'class' => 'td-fullname'
				)
			),
			array(
				'name' => 'created',
				'header' => Yii::t('standard', "Sent"),
				'htmlOptions' => array
					(
					'class' => 'td-email'
				),
				'headerHtmlOptions' => array
					(
					'class' => 'td-email'
				)
			),
			array(
				'name' => 'watched',
				'header' => Yii::t('standard', "Watched"),
				'htmlOptions' => array
					(
					'class' => 'td-email'
				),
				'headerHtmlOptions' => array
					(
					'class' => 'td-email'
				)
			),
		);
		return $columnsArray;
	}

}
