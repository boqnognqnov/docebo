<?php

/**
 * General controller for video library page
 * This class will store all methods for render this page and proccessing and pre-proccessing on needed params for build view
 * @author Kristian Dikov
 * @ver 1.0
 */
class KnowledgeLibraryController extends App7020BaseController {

// Controlers and Actions that are valid to write in Session for filtering results
	public static $allowedSessionArr = array(
		'knowledgeLibraryIndexMainFilter', 'knowledgeLibraryIndexTopicsTree', //knowledgeLibrary
		'askGuruIndexMainFilter', 'askGuruIndexTopicsTree', //askGuru-Index
		'askGuruMyQuestionsMainFilter', 'askGuruMyQuestionsTopicsTree', //askGuru-MyQuestions
		'askGuruMyFollowingMainFilter', 'askGuruMyFollowingTopicsTree', //askGuru-MyFollowing
	);
	public static $KIND_GURU = 'guru';
	public static $KIND_CONTRIBUTOR = 'contributor';
	public static $navigationMenuLinks = array();

	public function init() {
		parent::init();

		$amdController = array();
		$amdController['data']['data-main'] = $this->getAssetsUrl() . '/js/knowledgeLibrary/main';
		Yii::app()->event->raise('registerAMDScript', new DEvent($this, $amdController));

		$themeUrl = Yii::app()->theme->baseUrl;
		Yii::app()->tinymce;
	}

	/**
	 * Partial view folder for admin views
	 * @var string
	 */
	const VIEW_FOLDER_ADMIN = 'admin';
	const VIEW_FOLDER_LEARNER = 'learner';

	/**
	 * Open single contribution and behavior for them
	 */
	function actionItem() {
		$this->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl . '/js/owl/owl.carousel.min.js', CClientScript::POS_HEAD);
//		$this->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl . '/js/angularJs/angular-resource.js', CClientScript::POS_HEAD);
//		$this->getClientScript()->registerScriptFile('//cdnjs.cloudflare.com/ajax/libs/angular-poller/0.3.3/angular-poller.js', CClientScript::POS_HEAD);
//		$this->getClientScript()->registerScriptFile($this->getAssetsUrl() . '/js/peerReview/app.js', CClientScript::POS_END);
		$this->getClientScript()->registerCssFile($this->getAssetsUrl() . '/css/owl/owl.carousel.css');

		try {
			$id = Yii::app()->request->getParam('id', false);
			$singleContent = App7020Assets::model()->findByPk($id);
			
			if (empty($singleContent)) {
				$this->redirect(Docebo::createApp7020Url('channels/index'));
			};
			$isEditMode = Yii::app()->request->getParam('editMode', false);
			$singleContent = App7020Assets::model()->findByPk($id);
			
			//get the user IDs which can view the asset
			//if the asset is private - the result array will include the invited users, the contributor and the admin's ID, if the logged user is admin
			//if the asset is public - the result array will include the users which can view the asset's channels, 
			//   the contributor and the admin's ID, if the logged user is admin
			$idsWhichCanViewTheAsset = App7020Assets::getUsersWhichCanViewAnAsset($singleContent->id, true);

			if ((!empty($singleContent) && $singleContent instanceof App7020Assets && in_array(Yii::app()->user->idst, $idsWhichCanViewTheAsset))) {
				$this->breadcrumbs = array(
					Yii::t('app7020', 'Channels') => Docebo::createApp7020Url('channels/index'),
					($singleContent->title) ? $singleContent->title : Yii::t('app7020', 'Empty'),
				);

				$cuepoints = array();
				if ($singleContent->contentType <> App7020Assets::CONTENT_TYPE_VIDEO && !App7020Assets::isConvertableDocument($singleContent->filename)) {
 					App7020Assets::viewContent($id);
				} else {
					$cuepoints[] = array(
						'time' => $singleContent->duration * App7020Assets::VIDEO_VIEW_PERCENTAGE,
						'type' => App7020Assets::CUEPOINT_VIDEO_VIEW,
					);
				}

				
				$data['cuepoints'] = $cuepoints;
				$data['data'] = $singleContent;
				$data['rightSidebar'] = $this->renderPartial('learner/_rightSidebar', array(
					'arraySimilr' => App7020Assets::getSimilarContents($singleContent->id, 5)
						), true);
				$data['bottomMeta'] = $this->_getAuthorMetabox($singleContent);


				$data_tmp = array();
				$published = $singleContent->published;

				$contentEditBy = App7020ContentPublished::getLastEdited($id);
				$editBy = array();

				if ($contentEditBy) {

					$editBy['time'] = $contentEditBy->getLocalTime();
					$editBy['date'] = $contentEditBy->getLocalDate();
					$editBy['editBy'] = $contentEditBy->publisher->getFullName();
				}
				$data['editBy'] = $editBy;

				if (!empty($published)) {
					$data_tmp['time'] = $singleContent->published->getLocalTime();
					$data_tmp['date'] = $singleContent->published->getLocalDate();
					$data_tmp['publisher'] = $singleContent->published->publisher->getFullName();
				} else {
					$data_tmp = array();
				}
				$data['unpublished'] = $data_tmp;

				if ($singleContent['conversion_status'] == App7020Assets::CONVERSION_STATUS_APPROVED) {
					$isChecked = 'checked';
				} else {
					$isChecked = '';
				}

				$data['publishAndEdit'] = array(
					'publisher' => $data_tmp,
					'editBy' => $editBy,
					'isChecked' => $isChecked,
					'isEditMode' => $isEditMode,
					'idContent' => $singleContent->id,
					'contentStatus' => $singleContent->conversion_status
				);
				$this->render('item', $data);
			} else {
				$this->redirect(array('channels/index'));
				Yii::app()->end();
			}
		} catch (Exception $ex) {
			throw new CException($ex->getMessage());
		}
	}

	/**
	 * Open single contribution in EDIT MODE and behavior for them
	 */
	function actionEditItem() {
		App7020Helpers::registerApp7020UploadSettings();
		try {

			$id = Yii::app()->request->getParam('id', false);

			$singleContent = App7020Assets::model()->findByPk($id);
			if (empty($singleContent)) {
				$this->redirect(Docebo::createApp7020Url('channels/index'));
			};
			$selfJavaScript = array();
			if ($singleContent->isContributor) {
				//we have contributor
				$selfJavaScript['kind'] = self::$KIND_GURU;
			}
			if ($singleContent->hasReviewsAccess(Yii::app()->user->id)) {
				//if is admin access is guru
				$selfJavaScript['kind'] = self::$KIND_CONTRIBUTOR;
			}

			/* @var $userModel CoreUser */
			$userModel = CoreUser::model()->findByPk(Yii::app()->user->id);
			$selfJavaScript['userFullName'] = $userModel->getFullName();
			$selfJavaScript['userId'] = Yii::app()->user->id;
			//attach kind of current user
			$js = " var app7020PeerReview = " . json_encode($selfJavaScript);
			Yii::app()->getClientScript()->registerScript("app7020PeerReview" . null, $js, CClientScript::POS_HEAD);
			
			//check if the logged user have rights to edit the asset
			if (!$singleContent->isContributor && !$singleContent->hasReviewsAccess() && !Yii::app()->user->isGodAdmin) {
				$this->redirect(array('channels/index'));
				Yii::app()->end();
			}

			if ((!empty($singleContent) && $singleContent instanceof App7020Assets)) {
				$this->breadcrumbs = array(
					Yii::t('app7020', 'Channels') => Docebo::createApp7020Url('channels/index'),
					($singleContent->title) ? $singleContent->title : Yii::t('app7020', 'Empty'),
				);

				$data["ie"] = $ie;
				$data['data'] = $singleContent;
				$data['rightSidebar'] = $this->_rightSidebar($singleContent);
				$data['canInviteGlobal'] = ShareApp7020Settings::canInviteGlobal();
				$data['disableRate'] = $singleContent->isRatedByMe() || $singleContent->isContributor ? true : false;
				$data_tmp = array();
				$published = $singleContent->published;

				$contentEditBy = App7020ContentPublished::getLastEdited($id);
				$editBy = array();
				if ($contentEditBy) {
					$editBy['time'] = $contentEditBy->getLocalTime();
					$editBy['date'] = $contentEditBy->getLocalDate();
					$editBy['editBy'] = $contentEditBy->publisher->getFullName();
				}
				$data['editBy'] = $editBy;

				if (!empty($published)) {
					$data_tmp['time'] = $singleContent->published->getLocalTime();
					$data_tmp['date'] = $singleContent->published->getLocalDate();
					$data_tmp['publisher'] = $singleContent->published->publisher->getFullName();
				} else {
					$data_tmp = array();
				}
				$data['unpublished'] = $data_tmp;

				if ($singleContent['conversion_status'] == App7020Assets::CONVERSION_STATUS_APPROVED) {
					$isChecked = 'checked';
				} else {
					$isChecked = '';
				}
				$data['isVideo'] = App7020Assets::model()->isVideo($singleContent->filename);
				$data['publishAndEdit'] = array(
					'publisher' => $data_tmp,
					'editBy' => $editBy,
					'isChecked' => $isChecked,
					'isEditMode' => true,
					'idContent' => $singleContent->id,
					'hidePublishUnpublishSwitch' => $singleContent->getUserRole() === App7020Assets::USER_ROLE_EXPERT ? false : true,
					'contentStatus' => $singleContent->conversion_status
				);
				$this->render('editItem', $data);
			} else {
//				$this->redirect(array('knowledgeLibrary/index'));
//				Yii::app()->end();
			}
		} catch (Exception $ex) {
			echo 'System problem !!!';
		}
	}

	public function actionTooltips() {
		App7020Helpers::registerApp7020UploadSettings();
		$id = Yii::app()->request->getParam('id', false);
		$singleContent = App7020Assets::model()->findByPk($id);
		
		if (!$singleContent) {
			$this->redirect(Docebo::createApp7020Url('channels/index'));
			return;
		}
		
		if (!empty($singleContent) && $singleContent instanceof App7020Assets) {
			$this->breadcrumbs = array(
				Yii::t('app7020', 'Channels') => Docebo::createApp7020Url('channels/index'),
				($singleContent->title) ? $singleContent->title : Yii::t('app7020', 'Empty'),
			);

			if ($singleContent['conversion_status'] == App7020Assets::CONVERSION_STATUS_APPROVED) {
				$isChecked = 'checked';
			} else {
				$isChecked = '';
			}

			if (!empty($singleContent->published)) {
				$data_tmp['time'] = $singleContent->published->getLocalTime();
				$data_tmp['date'] = $singleContent->published->getLocalDate();
				$data_tmp['publisher'] = $singleContent->published->publisher->getFullName();
			} else {
				$data_tmp = array();
			}


			$contentEditBy = App7020ContentPublished::getLastEdited($id);
			$editBy = array();
			if ($contentEditBy) {
				$editBy['time'] = $contentEditBy->getLocalTime();
				$editBy['date'] = $contentEditBy->getLocalDate();
				$editBy['editBy'] = $contentEditBy->publisher->getFullName();
			}
			$data['editBy'] = $editBy;
			if (strpos($_SERVER['HTTP_USER_AGENT'], 'rv:11.0')) {
				$ie = "ie";
			}
			$data["ie"] = $ie;
			$data['publishAndEdit'] = array(
				'publisher' => $data_tmp,
				'editBy' => $editBy,
				'isChecked' => $isChecked,
				'isEditMode' => true,
				'idContent' => $singleContent->id,
				'contentStatus' => $singleContent->conversion_status,
				'hidePublishUnpublishSwitch' => false,
				'contentStatus' => $singleContent->conversion_status,
				'tooltipsPage' => true
			);
			$data['data'] = $singleContent;
			$data['rightSidebar'] = $this->_getTooltipsContainer();
			$data['bottomFluid'] = $this->_getTimelineContainer($singleContent);
			//$data['detailsForm'] = $this->_detailsForm($singleContent);
			$data['commentsList'] = $this->_commentsList($singleContent);
			//$data['ask_guru'] = $this->_getAskGuruTab($singleContent);
			$data['canInviteGlobal'] = ShareApp7020Settings::canInviteGlobal();
			$data['isActive'] = true;
			$data['peerReviewLink'] = true;
			$data['ngController'] = 'TimelineController';
			$data['ngInit'] = (!empty($singleContent->duration) ? $singleContent->duration : 0);
			$data['timelineTooltips'] = Yii::app()->db->createCommand()
							->select('id,durationFrom, durationTo, idContent, tooltiprow, text, tooltipstyle, idexpert')
							->from('app7020_tooltips')
							->where('idContent=:id', array(':id' => $id))->queryAll();
			$newTooltips = array();
			foreach ($data['timelineTooltips'] as $obj) {
				$newTooltips[] = array_merge($obj, array('normalText' => strip_tags($obj['text'])));
			}
			$data['timelineTooltips'] = $newTooltips;
			$data['duration'] = $singleContent->duration;
			Yii::app()->clientScript->registerScript('timelineTooltips', "var app7020Tooltips = " . CJSON::encode($data['timelineTooltips']), CClientScript::POS_HEAD);
			Yii::app()->clientScript->registerScript('videoDuration', "var app7020VideoDur = " . CJSON::encode($data['duration']), CClientScript::POS_HEAD);
			$selfJavaScript = array();
			if ($singleContent->isContributor) {
				//we have contributor
				$selfJavaScript['kind'] = self::$KIND_GURU;
			}
			if ($singleContent->hasReviewsAccess(Yii::app()->user->id)) {
				//if is admin access is guru
				$selfJavaScript['kind'] = self::$KIND_CONTRIBUTOR;
			}

			/* @var $userModel CoreUser */
			
			$userModel = CoreUser::model()->findByPk(Yii::app()->user->id);
			$selfJavaScript['userFullName'] = $userModel->getFullName();
			$selfJavaScript['userId'] = Yii::app()->user->id;
			//attach kind of current user
			$js = " var app7020PeerReview = " . json_encode($selfJavaScript);
			Yii::app()->getClientScript()->registerScript("app7020PeerReview" . null, $js, CClientScript::POS_HEAD);
//			

			$this->render('indexSingle_tooltips', $data);
		}
	}

	/**
	 * Tooltip save action
	 * @return CHttpResponse JSON
	 */
	public function actionAxSaveTooltip() {
		$id = Yii::app()->request->getParam('id', NULL);
		$contentId = $this->_detectContentRefererId();
		if (Yii::app()->request->getParam('peerReview')) {
			$reviewId = Yii::app()->request->getParam('reviewId');
			$contentId = Yii::app()->db->createCommand()
					->select('*')
					->from('app7020_content_reviews')
					->where('id=:id', array(':id' => $reviewId))
					->queryRow();
			$contentId = $contentId["idContent"];
		}
		$userID = Yii::app()->user->id;
		if (!empty($id)) {
			$tooltip = App7020Tooltips::model()->findByPK($id);
		} else {
			$tooltip = new App7020Tooltips();
		}
		$durationFrom = Yii::app()->request->getParam('durationFrom');
		$mmss = date('H:i:s', mktime(0, 0, $durationFrom));
		$tooltip->durationFrom = Yii::app()->request->getParam('durationFrom');
		$tooltip->durationTo = Yii::app()->request->getParam('durationTo');
		$tooltip->idContent = $contentId;
		$tooltip->idExpert = $userID;
		$tooltip->tooltipStyle = Yii::app()->request->getParam('tooltipStyle');
		$tooltip->text = Yii::app()->request->getParam('content');
		if ($tooltip->validate()) {
			$tooltip->save();
			if (Yii::app()->request->getParam('peerReview')) {
				$review = App7020ContentReview::model()->findByPK($reviewId);
				$review->idTooltip = $tooltip->id;
				$review->systemMessage = "1";
				$review->tooltipAt = $mmss;
				$review->save();
			}
			echo CJSON::encode(array(
				'success' => true,
				'id' => $tooltip->id,
				'contentId' => $contentId,
				'mmss' => $mmss
			));
		} else {
			echo CJSON::encode(array(
				'errors' => $tooltip->getErrors()
			));
		}
	}

	/**
	 * View renderer for dialog 2 html
	 * @return CHttpResponse HTML
	 */
	public function actionAxDeleteTooltipDialog() {
		$this->renderPartial('admin/_deleteToolTipDialog');
	}

	/**
	 * Ajax action to render tooltip widget for cuepoints on real time
	 * @return CHttpResponse HTML
	 */
	public function actionAxRenderTooltipWidget() {
		$tooltipId = Yii::app()->request->getParam('tooltipId', false);
		$hash = Yii::app()->request->getParam('tooltipHash');
		if ($tooltipId != false) {
			$tooltip = App7020Tooltips::model()->findByPK($tooltipId);
			$this->renderPartial('common.widgets.views.app7020Tooltips.tooltipItem', array('style' => $tooltip->themeClass, 'idHash' => $hash, 'tooltip' => $tooltip));
		}
	}

	/**
	 * Ajax action for tooltip delete
	 * @return CHttpResponse JSON Object
	 */
	public function actionAxDeleteTooltip() {
		$id = Yii::app()->request->getParam('id', NULL);
		if (!empty($id)) {
			$tooltip = App7020Tooltips::model()->findByPK($id);
			if (!is_null($tooltip)) {
				$tooltip->delete();
			}
		}
		echo CJSON::encode(array(
			'success' => true
		));
	}

//	/**
//	 * By rules get Ask guru tab and tab layout
//	 * @param App7020Content $content
//	 * @return array
//	 */
//	private function _getAskGuruTab($content) {
//		try {
//			if ($content->conversion_status == App7020Assets::CONVERSION_STATUS_APPROVED) {
//				$data = array();
//				$newQuestions = App7020Question::sqlDataProvider(false, array('contentId' => $content->id, 'onlyNew' => true));
//				$data['tab'] = $this->renderPartial('general/_askGuruTab', array('sount' => $newQuestions->totalItemCount), true);
//				$data['tab_layout'] = $this->_askTheGuruContent($content);
//			} else {
//				$data['tab'] = '';
//				$data['tab_layout'] = '';
//			}
//			return $data;
//		} catch (Exception $ex) {
//			return array('status' => false);
//		}
//	}

	/* ----------------------------------------------------------------------------
	 * GETTERS HTML CONTENT FOR ASK GURU DETAILS 
	 * --------------------------------------------------------------------------- */

//	private function _askTheGuruProviderParams($content) {
//		$listParams = array(
//			'listParams' => array(
//				'listId' => "questionsComboListView",
//				'afterAjaxUpdate' => 'if($("#knowledgeLibrarySingle #questionsComboListView .items .empty").length == 0){$("#knowledgeLibrarySingle .bottom-hint").remove();}',
//				'dataProvider' => App7020Question::sqlDataProvider(false, array(
//					'contentId' => $content->id,
//				)),
//				'infinite' => true
//			),
//			'extensions' => 'lastAnswer'
//		);
//		return $listParams;
//	}

	/**
	 * Get HTML and UX for Ask the guru  behavior
	 * @param App7020Content $content
	 * @return string
	 */
	private function _askTheGuruContent($content) {
		$html = $this->renderPartial("general/_askGuruTabContent", array(
			'listViewParams' => $this->_askTheGuruProviderParams($content),
			'arrayGurus' => $this->_getGurus($content),
			'contentId' => $content->id), true);

		return $html;
	}

	function _getGurus() {
		return array();
	}

	/* ----------------------------------------------------------------------------
	 * GETTER  HTML CONTENT FOR AUTHOR METABOX
	 * --------------------------------------------------------------------------- */

	/**
	 * Return content with author information also rates
	 * @param App7020Content $content
	 * @return string
	 */
	private function _getAuthorMetabox($content) {
		return $this->renderPartial('general/_contributionAuthorMeta', array('data' => $content), true);
	}

	/* ----------------------------------------------------------------------------
	 * GETTER HTML CONTENT FOR TIMELINE CONTAINER
	 * --------------------------------------------------------------------------- */

	/**
	 *
	 * @param App7020Content $content
	 * @return type
	 */
	private function _getTimelineContainer($content) {
		return $this->renderPartial('admin/_timeline', array('content' => $content), true);
	}

	/* ----------------------------------------------------------------------------
	 * GETTER HTML CONTENT FOR TOOLTIPS CONTAINER
	 * --------------------------------------------------------------------------- */

	private function _getTooltipsContainer() {
		return $this->renderPartial('admin/_tooltips', array('content' => $content), true);
	}

	/* ----------------------------------------------------------------------------
	 * GETTERS HTML CONTENT FOR DETAILS
	 * --------------------------------------------------------------------------- */

	/**
	 * Automaticly proccess partial view for render in Details tab
	 * @param App7020Content $content
	 * @return string
	 */
	private function _detailsForm($content) {
		$html = '';
		/* if ($content->defineContentAdminAccess()) {
		  $html .= $this->_getDetailsAdmin($content);
		  } else {
		  $html .= $this->_getDetailsLearner($content);
		  } */
		return $html;
	}

//	/**
//	 * Get ddetails tab for contributor or guru
//	 * @param App7020Content $content
//	 * @return string
//	 */
//	private function _getDetailsAdmin($content) {
//		// Register Fancytree JS/CSS
//
//		$ft = new FancyTree();
//		Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree-all.js', CClientScript::POS_HEAD);
//		Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree.docebo.js', CClientScript::POS_HEAD);
//		Yii::app()->getClientScript()->registerCssFile($ft->getAssetsUrl() . '/skin-tree-widget/ui.fancytree.css');
//		//assts for this page
//		$themeUrl = Yii::app()->theme->baseUrl;
//		$this->getClientScript()->registerScriptFile($themeUrl . '/js/typehead/handlebars.js', CClientScript::POS_HEAD);
//		$this->getClientScript()->registerScriptFile($themeUrl . '/js/tags/tagmanager.js', CClientScript::POS_HEAD);
//		$this->getClientScript()->registerScriptFile($themeUrl . '/js/typehead/handlebars.js', CClientScript::POS_HEAD);
//		$this->getClientScript()->registerScriptFile($themeUrl . '/js/typehead/' . (YII_DEBUG ? 'typeahead.jquery.min.js' : 'typeahead.jquery.js'), CClientScript::POS_HEAD);
//		$this->getClientScript()->registerScriptFile($themeUrl . '/js/typehead/typeahead.bundle.js', CClientScript::POS_HEAD);
//		$this->getClientScript()->registerCssFile($this->getAssetsUrl() . '/css/tags/app7020-tags.css');
//		App7020Helpers::registerApp7020UploadSettings();
//		$form = new App7020MetadataForm($content);
//		$form->setIsHide(false);
//		return $form->init();
//	}

	/**
	 * Get Details tab content for learner role
	 * @param App7020Content $content
	 * @return string
	 */
	private function _getDetailsLearner($content) {
		$this->getClientScript()->registerCssFile($this->getAssetsUrl() . '/css/tags/app7020-tags.css');
		return $this->renderPartial(self::VIEW_FOLDER_LEARNER . '/_learnerDetaisTab', array('content' => $content), true);
	}

	/* ----------------------------------------------------------------------------
	 * GETTERS HTML CONTENT FOR RIGHT SIDEBAR 
	 * --------------------------------------------------------------------------- */

	/**
	 * Get sidebar for single contribution
	 * @param App7020Content $content
	 * @return type
	 */
	private function _rightSidebar($content) {

		$html = '';
		if ($content->defineContentAdminAccess()) {
			$html .= $this->_getRightSidebarAdmin($content);
		} else {
			$html .= $this->_getRightSidebarLearner($content);
		}
		return $html;
	}

	/**
	 * Get ddetails tab for contributor or guru
	 * @param App7020Content $content
	 * @return string
	 */
	private function _getRightSidebarAdmin($content) {
		$controlParams = array(
			'containerClass' => 'peerReviewDropdown',
			'module' => 'peerReviewControlls',
			'arrayData' => array(
				array('directive' => 'click', 'action' => 'reviewToTooltip($index)', 'text' => 'Make Tooltip'),
				array('directive' => 'click', 'action' => 'edit($index)', 'text' => 'Edit'),
				array('directive' => 'click', 'action' => 'delete($index)', 'text' => 'Delete', 'class' => 'delete-control'),
			)
		);
		$workingFolder = $this->_getPartialViewFolder($content);
		return $this->renderPartial($workingFolder . '/_rightSidebar', array(
					'arrayGurus' => $this->_getGurus($content),
					'controlParams' => $controlParams,
					'initMessages' => $this->_extraxctPeerReviews($content->id, 0, 0, "id ASC")
						), true);
	}

	/**
	 * Get Details tab content for learner role
	 * @param App7020Content $content
	 * @return string
	 */
	private function _getRightSidebarLearner($content) {
		$workingFolder = $this->_getPartialViewFolder($content);
		return $this->renderPartial(
						$workingFolder . '/_rightSidebar', array(
					'arrayGurus' => $this->_getGurus($content),
					'arraySimilr' => App7020Assets::getSimilarContents($content->id, 5)
						), true);
	}

	/* ----------------------------------------------------------------------------
	 * GENERAL METHODS AND GETTERS 
	 * --------------------------------------------------------------------------- */

	/**
	 * Return avatar image or avatar  placeholder
	 * @return string
	 */
	private function _getAvatar() {
		$add_nameOfAvatar = basename(CoreUser::model()->findByPk(Yii::App()->user->idst)->getAvatarImage(true));
		return ($add_nameOfAvatar && $add_nameOfAvatar != 'user.png') ? CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS)->fileUrl($add_nameOfAvatar) : 'https://placeholdit.imgix.net/~text?txtsize=8&txt=64%C3%9764&w=64&h=64';
	}

	/**
	 * Return internal folder for subviews
	 * @return string
	 */
	private function _getPartialViewFolder($content) {
		try {
			if ($content->defineContentAdminAccess()) {
				return self::VIEW_FOLDER_ADMIN;
			} else {
				return self::VIEW_FOLDER_LEARNER;
			}
		} catch (CException $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return self::VIEW_FOLDER_LEARNER;
		}
	}

	/* ----------------------------------------------------------------------------
	 * ACTIONS 
	 * --------------------------------------------------------------------------- */

	public function actionForceDownload() {
		$id = Yii::app()->request->getParam('file', false);
		$assetObject = App7020Assets::model()->findByPk($id);
		if ($assetObject) {
			$fileStorageModel = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);
			if ($assetObject->contentType == App7020Assets::CONTENT_TYPE_VIDEO) {
				if(App7020Helpers::getExtentionByFilename($assetObject->filename) != 'mp4'){
					$assetObject->originalFilename = str_replace(App7020Helpers::getExtentionByFilename($assetObject->filename), 'mp4', $assetObject->originalFilename);
					$assetObject->filename = str_replace(App7020Helpers::getExtentionByFilename($assetObject->filename), 'mp4', $assetObject->filename);
					$fileStorageModel->sendFile($assetObject->filename, $assetObject->originalFilename, CS3StorageSt::getConventionFolder($assetObject->filename) . "/" . App7020Helpers::getAmazonInputKey($assetObject->filename));
				}elseif(App7020Helpers::getExtentionByFilename($assetObject->filename) == 'mp4'){
                    $fileStorageModel->sendFile($assetObject->filename, $assetObject->originalFilename, CS3StorageSt::getConventionFolder($assetObject->filename) . "/" . App7020Helpers::getAmazonInputKey($assetObject->filename));
                }
			} else {
				$fileStorageModel->sendFile($assetObject->filename, $assetObject->originalFilename, CS3StorageSt::getConventionFolder($assetObject->filename));
			}
		} else {
			return false;
		}
	}

	/**
	 * Gets the content ID and logged user's score, writes the vote into DB and recalculates the content rating
	 * @param integer contentId
	 * @param integer score between 1 and 5
	 * @return json encoded array - new content rating
	 */
	public function actionAxRate() {

		$contentId = Yii::app()->request->getParam('contentId');
		$score = Yii::app()->request->getParam('score');
		$contentRatingObject = new App7020ContentRating();
		$contentRatingObject->idContent = $contentId;
		$contentRatingObject->idUser = Yii::app()->user->idst;
		$contentRatingObject->rating = $score;
		$contentRatingObject->secureSave();
		echo json_encode(array('score' => App7020ContentRating::calculateContentRating($contentId)));
	}

	/**
	 * Gets the content ID and makes a record in content view history
	 * @param integer contentId
	 */
	public function actionAxViewContent() {
		$contentId = $this->_detectContentRefererId();
		App7020Assets::viewContent($contentId);

//todo - return current views count of the content
		echo json_encode(array('status' => true));
	}

	/**
	 * Get Controler and Action (e.g. "knowledgeLibraryIndex")
	 * @return string
	 */
	public static function getControlerAction() {
		$subject = $_SERVER['HTTP_REFERER'];
		$pattern = '/(.*)\?r\=(.*)\/(.*)$/';
		preg_match($pattern, $subject, $matches);
		$conrolerAction = $matches[2] . ucfirst($matches[3]);
		return $conrolerAction;
	}

	/**
	 * Returns all peer reviews by given content
	 * @param integer $idContent
	 * @param number $idFrom
	 * @param number $limit
	 * @param string $order
	 * @return json encoded array
	 */
	public function actionAxGetReviewes() {
		if (Yii::app()->request->getParam('login') != 1) {
			$idContent = $this->_detectContentRefererId();
			$tmp = Yii::app()->request->getParam('idFrom');
			$idFrom = empty($tmp) ? 0 : Yii::app()->request->getParam('idFrom');
			$tmp = Yii::app()->request->getParam('limit');
			$limit = empty($tmp) ? 0 : Yii::app()->request->getParam('limit');
			$tmp = Yii::app()->request->getParam('order');
			$order = empty($tmp) ? "id ASC" : Yii::app()->request->getParam('order');

			if (empty($idContent)) {
				die(Yii::app()->request->getParam('callback') . "(" . json_encode(array('status' => false)) . ")");
			}

			$allReviews = $this->_extraxctPeerReviews($idContent, 0, $limit, $order);

			if (empty($allReviews)) {
				if ($idFrom == 0) {
					$data = array();
					$data['status'] = false;
					die(Yii::app()->request->getParam('callback') . "(" . json_encode($data) . ")");
				} else {
					$data = array();
					$data['status'] = false;
					$data['reviews'] = $allReviews;
					die(Yii::app()->request->getParam('callback') . "(" . json_encode($data) . ")");
				}
			} else {
				$countKey = (count($allReviews) - 1);
				$countLastId = $allReviews[$countKey]['id'];
				$data['status'] = true;
				$data['idFrom'] = $countLastId;
				$data['reviews'] = $allReviews;
				echo (Yii::app()->request->getParam('callback') . "(" . json_encode($data) . ")");
			}
		} else {
			$this->redirect(array('knowledgeLibrary/index'));
		}
	}

	/**
	 * Return from database all peer reviews
	 * @param int $idContent Content id for peeer reviews
	 * @param int $idFrom start extract from ID >
	 * @param int $limit Limit for extract
	 * @param int $order Order clause
	 * @return App7020ContentReview[]
	 */
	private function _extraxctPeerReviews($idContent, $idFrom, $limit, $order) {
		try {
			return App7020ContentReview::getPeerReviewesByContent($idContent, $idFrom, $limit, $order);
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return array();
		}
	}

	/**
	 * Return id by Referer from AJAX
	 * @return boolean|integer
	 */
	private function _detectContentRefererId() {

		if (Yii::app()->request->getParam('idContent', false)) {
			$id = Yii::app()->request->getParam('idContent');
		} else if (isset($_SERVER['HTTP_REFERER'])) {
			$url = $_SERVER['HTTP_REFERER'];
			preg_match('/id=[0-9A-Za-z]*/', $url, $matches);
			$value = explode("=", $matches[0]);
			$id = $value[1];
		} else {
			return false;
		}
		return (int) $id;
	}

	/**
	 * Creates new peer review linked to the specified content.
	 * @param integer idContent
	 * @param text message
	 * @return json encoded array - new question ID
	 */
	public function actionAxAddReview() {
		$data['idContent'] = $this->_detectContentRefererId();
		$data['message'] = Yii::app()->request->getParam('message');
		$tmp = Yii::app()->request->getParam('idTooltip');
		$data['idTooltip'] = empty($tmp) ? null : Yii::app()->request->getParam('idTooltip');
		$tmp = Yii::app()->request->getParam('systemMessage');
		$data['systemMessage'] = empty($tmp) ? 0 : Yii::app()->request->getParam('systemMessage');
		$data['idUser'] = Yii::app()->user->idst;

		if (empty($data['idContent'])) {
			echo json_encode(array('reviewId' => 0));
			return;
		}

		$contentObject = App7020Assets::model()->findByPk($data['idContent']);

		if (empty($contentObject->id) && $contentObject->defineContentAdminAccess() == false) {
			echo json_encode(array('reviewId' => 0));
			return;
		}

		$reviewObject = new App7020ContentReview();
		$reviewObject->setAttributes($data);
		$reviewObject->save(false);
		if ($reviewObject->id) {
			Yii::app()->event->raise('App7020NewPeerReview', new DEvent($this, array('prId' => $reviewObject->id)));
			echo json_encode(array('reviewId' => (int) $reviewObject->id));
		} else {
			echo json_encode(array('reviewId' => 0));
		}
	}

	/**
	 * Updates a peer review
	 * @param integer id
	 * @param text message
	 * @return json encoded array - the result of update proccess
	 */
	public function actionAxUpdateReview() {

		$id = Yii::app()->request->getParam('id');
		$message = Yii::app()->request->getParam('message');

		$reviewObject = App7020ContentReview::model()->findByPk($id);

		if (empty($reviewObject->id)) {
			echo json_encode(array('result' => 0));
			return;
		}


		$reviewObject->message = $message;

		echo json_encode(array('result' => $reviewObject->update()));
	}

	/**
	 * Deletes a peer review
	 * @param integer id
	 * @return json encoded array - the result of deletion proccess
	 */
	public function actionAxDeleteReview() {

		$id = Yii::app()->request->getParam('id');

		$tmp = intval($id);
		if (empty($tmp)) {
			echo json_encode(array('result' => false));
			return;
		}
		$reviewObject = App7020ContentReview::model()->findByPk($id);
		echo json_encode(array('result' => (bool) App7020ContentReview::model()->deleteByPk($id)));
	}

	/**
	 * Publish a content
	 * @param integer id
	 * @return json encoded array - the result of update operation
	 */
	public function actionAxPublishContent() {
		$data = array();
		$id = $this->_detectContentRefererId();
		$tmp = intval($id);
		if (empty($tmp)) {
			$data['result'] = false;
			die(json_encode($data));
		}
		try {
			$contentObject = App7020Assets::model()->findByPk($id);

			if (
					($contentObject->hasReviewsAccess() || Yii::app()->user->isGodAdmin) &&
					(int) $contentObject->conversion_status >= App7020Assets::CONVERSION_STATUS_FINISHED &&
					(int) $contentObject->conversion_status < App7020Assets::CONVERSION_STATUS_APPROVED
			) {
				$contentObject->conversion_status = App7020Assets::CONVERSION_STATUS_APPROVED;

				$requestModel = App7020QuestionRequest::model()->findByAttributes(array('idContent' => $id, 'autoInvite' => 1));
				if ($requestModel->id) {
					$questionModel = App7020Question::model()->findByPk($requestModel->idQuestion);
					$invitedUsers = array($questionModel->idUser);
					foreach ($invitedUsers as $value) {
						$invitationObject = new App7020Invitations();
						$invitationObject->idInvited = $value;
						$invitationObject->idContent = $id;
						$invitationObject->save();
					}
					Yii::app()->event->raise('App7020Invitation', new DEvent($this, array('users' => $invitedUsers,
						'contentId' => $id)));
				}


				$result = $contentObject->save(false);
				$data_tmp = array();
				$published = $contentObject->published;
				if (!empty($published)) {
					$data_tmp['time'] = $contentObject->published->getLocalTime();
					$data_tmp['date'] = $contentObject->published->getLocalDate();
					$data_tmp['publisher'] = $contentObject->published->publisher->getFullName();
				} else {
					$data_tmp = array();
				}

				$data['result'] = true;
				$data['publish_dom'] = $this->renderPartial('admin/_unPublishBlock', $data_tmp, true);
			} else {
				$data['publish_dom'] = $this->renderPartial('admin/_publishBlock', array(), true);
				$data['result'] = false;
			}
			$data['result'] = $result;
			Yii::app()->event->raise('App7020Publishing', new DEvent($this, array('contentId' => $id, 'idPublisher' => Yii::app()->user->idst)));
			die(json_encode($data));
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			$data['result'] = false;
			die(json_encode($data));
		}
	}

	/**
	 * Publish a content
	 * @param integer id
	 * @return json encoded array - the result of update operation
	 */
	public function actionAxUnPublishContent() {
		$data = array();
		$id = $this->_detectContentRefererId();
		$tmp = intval($id);
		if (empty($tmp)) {
			$data['result'] = false;
			die(json_encode($data));
		}
		try {
			$contentObject = App7020Assets::model()->findByPk($id);
			if (
					($contentObject->hasReviewsAccess() || Yii::app()->user->isGodAdmin) &&
					$contentObject->conversion_status == App7020Assets::CONVERSION_STATUS_APPROVED
			) {
				$contentObject->conversion_status = App7020Assets::CONVERSION_STATUS_UNPUBLISH;

				$publishedObject = App7020ContentPublished::model()->findByAttributes(array('idContent' => $id, 'actionType' => App7020ContentPublished::ACTION_TYPE_PUBLISH));

				$publishedObject->delete();
				$result = $contentObject->save(false);
//				$data['publish_dom'] = $this->renderPartial('admin/_publishBlock', array(), true);
			} else {
				$data_tmp = array();
				$published = $contentObject->published;
				if (!empty($published)) {
					$data_tmp['time'] = $contentObject->published->getLocalTime();
					$data_tmp['date'] = $contentObject->published->getLocalDate();
					$data_tmp['publisher'] = $contentObject->published->publisher->getFullName();
				} else {
					$data_tmp = array();
				}
//				$data['publish_dom'] = $this->renderPartial('admin/_unPublishBlock', $data_tmp, true);
				$result = false;
			}
			$data['result'] = $result;
			die(json_encode($data));
		} catch (Exception $ex) {
			$data['result'] = false;
			die(json_encode($data));
		}


		die(json_encode(array('result' => $result)));
	}

	/**
	 * Ajax method for get via  server side UTC date
	 */
	public function actionAxGetUtcTime() {
		die(json_encode(array('date' => Yii::app()->localtime->getLocalNow())));
	}

	/**
	 * Get a list of all groups
	 * @return array
	 */
	public static function getGroupsList($includeHidden = false, $contentId) {
		$searchInput = trim(Yii::app()->request->getParam('search_input', false));
		$hidden = ($includeHidden) ? 'true' : 'false';
		$command = Yii::app()->db->createCommand();
		$command->select('g.idst, TRIM(LEADING "/" FROM g.groupid) as gid, (SELECT COUNT(idstMember) from ' . CoreGroupMembers::model()->tableName() . ' WHERE idst=g.idst) AS 
members');
		$command->from(CoreGroup::model()->tableName() . ' AS g');
		$command->where("g.hidden =:hidden", array(':hidden' => $hidden));
		if ($searchInput) {

			$command->andWhere("g.groupid  LIKE :search", array(':search' => '%' . $searchInput . '%'));
		}
		$visibleGroups = $command->queryAll();
		foreach ($visibleGroups as $key => $value) {
			$visibleGroups[$key]['contentId'] = $contentId;
		}
		$config['pagination'] = false;
		$dataProvider = new CArrayDataProvider($visibleGroups, $config);
		return $dataProvider;
	}

	function actionAxGetToken() {
		$token = Yii::app()->request->csrfToken;
		die(json_encode(array('token' => $token)));
	}

	public function actionSettings() {
		$content_id = Yii::app()->request->getParam("id", 0);
		$content = App7020Assets::model()->findByPk($content_id);
		$modelForm = App7020SettingsPeerReview::model()->find('idContent=:idContent', array('idContent' => $content_id));

// If Custom settings for current asset are not set, we get them from core_settings table
		if (!$modelForm) {
// core_settings 'invite_watch' value
			$csInviteWatch = Settings::get('invite_watch') == ShareApp7020Settings::ON ? 1 : 0;
// core_settings 'enable_answer_questions' value
			$csEnableAnswerQuestions = Settings::get('enable_answer_questions') == CoachApp7020Settings::ON ? 1 : 0;

			$modelForm = new App7020SettingsPeerReview();
//			$modelForm->enableCustomSettings = $csInviteWatch || $csEnableAnswerQuestions ? 1 : 0;
			$modelForm->inviteToWatch = $csInviteWatch;
			$modelForm->allowContributorAnswer = $csEnableAnswerQuestions;
		}


		$this->breadcrumbs = array(
			Yii::t('app7020', 'Channels') => Docebo::createApp7020Url('channels/index'),
			Yii::t('app7020', $content->title) => Docebo::createApp7020Url('knowledgeLibrary/item', array('id' => $content->id)),
			Yii::t('app7020', 'Custom settings')
		);
		if (empty($modelForm)) {
			$modelForm = new App7020SettingsPeerReview();
		}
		$data['form'] = $this->renderPartial('_settings_form', array('model' => $modelForm), true);
		$data['id'] = $content_id;
		$this->render('settings', $data);
	}

	/**
	 *
	 */
	public function actionAxValidateSettings() {
		$submitedParams = Yii::app()->request->getParam("App7020SettingsPeerReview", false);
		$content_id = $this->_detectContentRefererId();
		$modelForm = App7020SettingsPeerReview::model()->find('idContent=:idContent', array('idContent' => $content_id));
		if (empty($modelForm)) {
			$modelForm = new App7020SettingsPeerReview();
			$modelForm->setAttribute('idContent', $content_id);
		}
		$data = array();
		$data['status'] = false;

		if (!empty($submitedParams)) {
			$modelForm->setAttributes($submitedParams);
			if ($modelForm->validate() && $modelForm->save()) {
				$data['status'] = true;
			} else {
				$data['status'] = false;
				$data['errors'] = $modelForm->getErrors();
			}
		}
		$data['form'] = $this->renderPartial('_settings_form', array('model' => $modelForm, 'id' => $content_id), true);
		die(json_encode($data));
	}

	/**
	 * 
	 */
	public function actionAxRemoveWatched() {

		$watchedAssets = App7020Invitations::getInvitationsByInvited(Yii::app()->user->idst, 2);
		foreach ($watchedAssets->rawData as $value) {

			App7020Invitations::model()->deleteAllByAttributes(array("idContent" => $value['id'], "idInvited" => Yii::app()->user->idst));
		}
		echo json_encode(array('result' => true));
	}

	/**
	 * 
	 */
	public function actionAxSetGotItInformation() {
		$method = Yii::app()->request->getParam('method', false);
		$coreUserSettings = new CoreSettingUser();
		$coreUserSettings->id_user = Yii::app()->user->idst;
		$coreUserSettings->value = 1;
		if ($method == 'contributeShareMessage') {
			if (!CoreSettingUser::model()->findByAttributes(array('path_name' => 'knowledgelibrary_contribute_banner_gotit', 'id_user' => Yii::app()->user->idst))) {
				$coreUserSettings->path_name = 'knowledgelibrary_contribute_banner_gotit';
				$coreUserSettings->save();
			}
			$this->sendJSON(array('success' => true));
			Yii::app()->end();
		}
		$this->sendJSON(array('success' => false));
		Yii::app()->end();
	}

	function _commentsList() {
		return array();
	}

}
