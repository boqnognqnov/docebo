<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AssetSummaryReportController
 *
 * @author Trayan
 */
class ChannelSummaryController extends App7020BaseController {

	public function filters() {
		return array(
			'accessControl'
		);
	}

	/**
	 *
	 * @see CController::init()
	 */
	public function init() {
		parent::init();
		$this->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl . '/js/Chart.min.js', CClientScript::POS_HEAD);
		Yii::app()->clientScript->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl() . '/jui/css/base/jquery-ui.css');

		$amdController = array();
		$amdController['data']['data-main'] = $this->getAssetsUrl() . '/js/app7020Activity/main';
		Yii::app()->event->raise('registerAMDScript', new DEvent($this, $amdController));
	}

	public function accessRules() {
		$res = array();
		$res[] = array(
			'allow',
			'users' => array(
				'*'
			),
			'actions' => array(
				'login'
			)
		);
		$res[] = array(
			'allow',
			'users' => array(
				'@'
			),
			'actions' => array(
				'logout',
				'index'
			)
		);
		return $res;
	}

	public function actionIndex() {
		$channel = Yii::app()->request->getParam('idChannel');
		$singleChannel = App7020Channels::model()->findByPk($channel);
		if (!empty($singleChannel)) {
			$translation = $singleChannel->translation();
		}
		$channelExperts = App7020ChannelExperts::getChannelExpertIds($channel);
		$dataChannel["title"] = $translation["name"];
		$dataChannel["description"] = $translation["description"];
		$dataChannel["attributes"] = $singleChannel;
		$statsOverview = App7020ChannelSummary::channelSummaryOverviewData($channel, 30);
		$statsQa = App7020ChannelSummary::channelSummaryQaData($channel, 30);
		$statsExperts["channelExperts"] = App7020ChannelExperts::getChannelExpertIds($channel, false);
		$statsExperts["topCoachExperts"] = App7020ChannelSummary::ExpertsData($channel);
		Yii::app()->clientScript->registerScript('app7020channelPublishedAssets', "var app7020channelPublishedAssets = " . CJSON::encode($statsOverview["channelContributions"]), CClientScript::POS_HEAD);
		Yii::app()->clientScript->registerScript('app7020channelAssetViews', "var app7020channelAssetViews = " . CJSON::encode($statsOverview["channelAssetViews"]), CClientScript::POS_HEAD);
		Yii::app()->clientScript->registerScript('app7020channelQuestionAnswers', "var app7020channelQuestionAnswers = " . CJSON::encode($statsQa), CClientScript::POS_HEAD);
		if ((!empty($singleChannel) && $singleChannel instanceof App7020Channels)) {
			$this->breadcrumbs = array(
				Yii::t('app7020', 'Admin') => Docebo::createAppUrl('admin:site/index'),
				Yii::t('app7020', 'Reports') => Docebo::createAppUrl('admin:reportManagement/index')
			);
			$this->render('index', array(
				'channelData' => $dataChannel,
				'channelExperts' => $channelExperts,
				'statsOverview' => $statsOverview,
				'statsQa' => $statsQa,
				'statsExperts' => $statsExperts,
			));
		}
	}

	public function actionAxTimeOverview() {
		if (Yii::app()->request->isAjaxRequest) {
			$channel = Yii::app()->request->getParam('idChannel', array());
			$timeInterval = Yii::app()->request->getParam('timeframe', array());
			$statsOverview = App7020ChannelSummary::channelSummaryOverviewData($channel, intval($timeInterval));
			echo CJSON::encode($statsOverview);
		}
	}

	public function actionAxTimeFrameQa() {
		if (Yii::app()->request->isAjaxRequest) {
			$channel = Yii::app()->request->getParam('idChannel', array());
			$timeInterval = Yii::app()->request->getParam('timeframe', array());
			$statsQa = App7020ChannelSummary::channelSummaryQaData($channel, intval($timeInterval));
			echo CJSON::encode($statsQa);
		}
	}

	public function actionGenerateChannelReport() {
		$channelId = Yii::app()->request->getParam('channelId', false);
		$timeframe = Yii::app()->request->getParam('timeframe', 7);
		$chartAssets = Yii::app()->request->getParam('channelAssetsChart', false);
		$chartAssetsViews = Yii::app()->request->getParam('channelAssetsViewsChart', false);
		$chartQa = Yii::app()->request->getParam('questionsVsAnswersChart', false);
		$path = dirname(Yii::app()->request->scriptFile);
		$path = str_replace("app7020", "admin", $path);
		if ($chartAssetsViews) {
			$imgQuestionsData = str_replace(' ', '+', $chartAssetsViews);
			$imgQuestionsData = substr($imgQuestionsData, strpos($imgQuestionsData, ",") + 1);
			$imgQuestionsData = base64_decode($imgQuestionsData);
			$chartQuestionsImgName = 'chartAssetViews_' . microtime(true) * 10000 . '_' . substr(md5(uniqid(rand(), true)), 0, 5) . '.jpg';
			//		// Path where the image is going to be saved
			$fileQuestionsPath = $path . '/protected/runtime/tmpPdf/' . $chartQuestionsImgName;
			//		// Write $imgData into the image file
			$fileQuestions = fopen($fileQuestionsPath, 'w');
			fwrite($fileQuestions, $imgQuestionsData);
			fclose($fileQuestions);
		}

		if ($chartAssets) {
			$imgAssetsData = str_replace(' ', '+', $chartAssets);
			$imgAssetsData = substr($imgAssetsData, strpos($imgAssetsData, ",") + 1);
			$imgAssetsData = base64_decode($imgAssetsData);
			$chartAssetsImgName = 'chartAssets_' . microtime(true) * 10000 . '_' . substr(md5(uniqid(rand(), true)), 0, 5) . '.jpg';
			//		// Path where the image is going to be saved
			$fileAssetsPath = $path . '/protected/runtime/tmpPdf/' . $chartAssetsImgName;
			//		// Write $imgData into the image file
			$fileAssets = fopen($fileAssetsPath, 'w');
			fwrite($fileAssets, $imgAssetsData);
			fclose($fileAssets);
		}
		if ($chartQa) {
			$imgAssetsData = str_replace(' ', '+', $chartQa);
			$imgAssetsData = substr($imgAssetsData, strpos($imgAssetsData, ",") + 1);
			$imgAssetsData = base64_decode($imgAssetsData);
			$chartAssetsImgName = 'chartAssets_' . microtime(true) * 10000 . '_' . substr(md5(uniqid(rand(), true)), 0, 5) . '.jpg';
			//		// Path where the image is going to be saved
			$fileQaPath = $path . '/protected/runtime/tmpPdf/' . $chartAssetsImgName;
			//		// Write $imgData into the image file
			$fileQa = fopen($fileQaPath, 'w');
			fwrite($fileQa, $imgAssetsData);
			fclose($fileQa);
		}
		$allChartsImgs = array('allChartsImgs' => array(
				'chartAssetViews' => $fileQuestionsPath,
				'chartAssets' => $fileAssetsPath,
				'chartQa' => $fileQaPath
		));

		$overviewData = App7020ChannelSummary::channelSummaryOverviewData($channelId, intval($timeframe));
		$channelQaData = App7020ChannelSummary::channelSummaryQaData($channelId, intval($timeframe));
		$expertData = App7020ChannelSummary::ExpertsData($channelId, intval($timeframe));
		$channel = array('channel' => App7020Channels::model()->findByPk($channelId));
		$additionalInfo = array('timeframeStr' => App7020Helpers::timeframeStr($timeframe));

		$renderData = array_merge($allChartsImgs, $overviewData, $channelQaData, $expertData, $channel, $additionalInfo);

		$content = $this->renderPartial('channelPdf', array('renderData' => $renderData), true);

		$tempKey = substr(md5(uniqid(rand(), true)), 0, 5);
		$pdfFileName = 'channelReport' . date('m_d_Y') . '_' . $tempKey . '.pdf';

		$css = Yii::app()->theme->basePath . '/css/expertSummary.css';
		$pdfFileName = ReportManagementController::createPdf($content, $pdfFileName, 'P', $css);
		$this->sendJSON(array('fileName' => $pdfFileName));
	}

	public function actionDownloadPdf($fileName = null) {
		$pdfFullPath = Yii::app()->basePath . '/runtime/tmpPdf/' . basename($fileName);


		Yii::app()->request->sendFileDirect($pdfFullPath, $fileName);


		if (!empty($fileName) && file_exists($pdfFullPath)) {
			/* $isIE8 = preg_match('/(?i)msie [6-8]/',$_SERVER['HTTP_USER_AGENT']); */ //TODO: IE8 detection seems to not correctly work this way
			if (/* $isIE8 && */isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) != 'off') {
				//--- IE-8 compatibility headers for HTTPS connections ---
				//content type set to generic fiel download
				header("Content-type: application/download");
				//cache control
				header("Cache-control: private");
				header('Pragma: private');
				//sending creation time
				header('Expires: ' . gmdate('D, d M Y H: i: s') . ' GMT');
			} else {
				//--- normal headers ---
				// We'll be outputting a PDF
				header('Content-type: application/pdf');
				// It will be called downloaded.pdf
				header('Content-Disposition: attachment; filename="' . $fileName . '"');
			}
			// The PDF source is in original.pdf
			readfile($pdfFullPath);
		}
		Yii::app()->end();
	}

}
