<?php

class AxAssetsController extends AssetsController {

    public function accessRules() {
        return parent::accessRules();
    }

    /**
     * Generate  modal partial view and return it to render from FE part of the project
     * @author Kristian Dikov
     * @version 1.0
     */
    public function actionGetAssetModal() {
        $assetId = Yii::app()->request->getParam('assetId', false);
        $asset = App7020Assets::model()->with('question_channels.channel_translations')->findByPk($assetId);
        $checkDownload = App7020CustomContentSettings::getContentCustomSetting(App7020Assets::SETTING_ALLOW_DOWNLOAD, $assetId) == 'on' ? true : false;
        $this->renderPartial('app7020.protected.views.assets._assetModal', array('asset' => $asset, 'checkDownload' => $checkDownload));
    }

    /**
     * Clear assets from PR tab
     * @version 1.0
     * @author Stanimir Simeonov <stanimir@pandacrew.eu>
     */
    public function actionAxListPRAll() {
        $clear = App7020Assets::clearPeerReviewAssetsList();
        if ($clear) {
            echo json_encode(array(
                'status' => true,
            ));
        } else {
            echo json_encode(array(
                'status' => false,
            ));
        }
    }

    /**
     * Clear assets from publish tab
     * @version 1.0
     * @author Stanimir Simeonov <stanimir@pandacrew.eu>
     */
    public function actionAxListPublishAll() {
        $clear = App7020Assets::clearPublishedAssetsList();
        if ($clear) {
            echo json_encode(array(
                'status' => true,
            ));
        } else {
            echo json_encode(array(
                'status' => false,
            ));
        }
    }

    /**
     * Add trash row into db for new files added in queue;
     */
    public function actionAxAddAsset() {

		Yii::app()->session->close();
		$files = json_decode(Yii::app()->request->getParam('data', false));
		$objects = array();
		try {
			foreach ($files as $key => $file) {
				$extension = strtolower(App7020Helpers::getExtentionByFilename($file->name));
				$fileType = App7020Assets::getContentCodeByExt($extension);

                if (!$fileType) {
                    die(json_encode(array('result' => false, 'error' => Yii::t("app7020", "File type is not allowed for upload"))));
                }


                $filename = $file->id . "." . App7020Helpers::getExtentionByFilename($file->name); //App7020Helpers::generateHashedFilename($file->name);

                $assetObject = new App7020Assets();
                $assetObject->conversion_status = App7020Assets::CONVERSION_STATUS_PENDING;
                $assetObject->originalFilename = $file->name;
                $assetObject->filename = $filename;
                $assetObject->contentType = $fileType;
                $assetObject->idSource = 1;
                $assetObject->userId = Yii::app()->user->idst;
                $assetObject->save(false);

                $fileStorageObject = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);
                $fileStorageObject->setKey($assetObject->filename);
                $outputPath = ($assetObject->contentType == App7020Assets::CONTENT_TYPE_VIDEO) ? $fileStorageObject->getKey() : $fileStorageObject->getOutputKey();



                if ($assetObject->id) {

                    $this->_proceedRequest($assetObject->id);

                    $assetObject->refresh();

                    $objects[$key]['asset'] = (array) $assetObject->getAttributes();
                    $objects[$key]['status'] = true;
                    $objects[$key]['asset'] ['outputPath'] = ltrim($outputPath, "/");
                    //	$objects[$key]['asset'] ['filename'] = $assetObject->filename;
                } else {
                    $objects[$key]['error'] = Yii::t("app7020", "Cannot add the selected asset");
                    $objects[$key]['status'] = false;
                }
            }
            echo (json_encode(
                array(
                    'result' => !empty($objects) ? true : false,
                    'assets' => $objects,
                )
            ));
            Yii::app()->end();
        } catch (Exception $ex) {
            Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
            echo (json_encode(
                array(
                    'result' => false,
                    'error' => $ex->getMessage(),
                )
            ));
            Yii::app()->end();
        }
    }

    /**
     * Add trash row into db for new files added in queue;
     */
    public function actionAxAddLink() {
        Yii::app()->session->close();
        $sharedLink = Yii::app()->request->getParam('data', false);
        $objects = array();
        $key = 0;
        if ($sharedLink = App7020Helpers::isValidUrl($sharedLink)) {
            if (strpos($sharedLink, 'https://') === false) {
                $sharedLink = str_replace("http://", "", $sharedLink);
                $sharedLink = "http://" . $sharedLink;
            }
            $content = new App7020Assets();
            $content->title = '';
            $content->description = '';
            $content->filename = md5($sharedLink . time() . Yii::app()->user->idst);
            $content->originalFilename = $sharedLink;
            $content->contentType = App7020Assets::CONTENT_TYPE_LINKS;
            $content->idSource = 1;
            $content->conversion_status = App7020Assets::CONVERSION_STATUS_INPROGRESS;
            $content->userId = Yii::app()->user->idst;
            $content->duration = 0;
            $content->save(false);


            if ($content->id) {
                $this->_proceedRequest($content->id);
                App7020WebsiteProceeder::saveWebsiteInitData($content->id);
                $objects[$key]['asset'] = (array) $content->getAttributes();
                $objects[$key]['status'] = true;
            } else {
                $objects[$key]['error'] = Yii::t("app7020", "Cannot add the selected asset");
                $objects[$key]['status'] = false;
            }

            echo (json_encode(
                array(
                    'result' => !empty($objects) ? true : false,
                    'assets' => $objects,
                )
            ));
            Yii::app()->end();
        } else {
            $this->sendJSON(array('result' => false, 'errors' => array(Yii::t("app7020", "Invalid Link"))));
        }
    }

    public function actionAxAddGoogleDrive() {
        Yii::app()->session->close();
        $sharedFile = Yii::app()->request->getParam('data', false);
        $contentType = $this->getGoogleDriveContentType($sharedFile["mimeType"]);
        $objects = array();
        $key = 0;

        if(!empty($sharedFile["embedUrl"])){
            $content = new App7020Assets();
            $content->title = $sharedFile["name"];
            $content->description = $sharedFile["description"];
            $content->filename = md5($sharedFile["name"] . time() . Yii::app()->user->idst);
            $content->originalFilename = $sharedFile["embedUrl"];
            $content->contentType = $contentType;
            $content->idSource = 1;
            $content->conversion_status = App7020Assets::CONVERSION_STATUS_SNS_SUCCESS;
            $content->userId = Yii::app()->user->idst;
            $content->duration = 0;
            $content->save(false);

            if ($content->id) {
                $this->_proceedRequest($content->id);
                //App7020WebsiteProceeder::saveWebsiteInitData($content->id);
                $objects[$key]['asset'] = (array) $content->getAttributes();
                $objects[$key]['status'] = true;
            }
        }else {
            $objects[$key]['error'] = Yii::t("app7020", "Cannot add the selected asset.");
            $objects[$key]['status'] = false;
        }

        echo (json_encode(
            array(
                'result' => !empty($objects) ? true : false,
                'assets' => $objects,
            )
        ));
        Yii::app()->end();
    }

    public function getGoogleDriveContentType($mimeType) {
        switch ($mimeType) {
            case "application/msword":
            case "text/plain":
            case "application/pdf":
            case "application/vnd.google-apps.file":
            case "application/vnd.google-apps.document":
                $contentType = App7020Assets::CONTENT_TYPE_GOOGLE_DRIVE_DOCS;
                break;
            case "application/vnd.google-apps.spreadsheet":
            case "application/vnd.ms-excel":
            case "application/vnd.google-apps.form":
                $contentType = App7020Assets::CONTENT_TYPE_GOOGLE_DRIVE_SHEETS;
                break;
            case "application/vnd.ms-powerpoint":
            case "application/vnd.google-apps.presentation":
            case "application/vnd.ms-powerpoint.presentation.macroEnabled.12":
                $contentType = App7020Assets::CONTENT_TYPE_GOOGLE_DRIVE_SLIDES;
                break;
        }
        return $contentType;
    }

    public function actionAxStartUploadAsset() {
        Yii::app()->session->close();
        $idAsset = (int) (Yii::app()->request->getParam('assetId', false));
        $assetObject = App7020Assets::model()->findByPk($idAsset);
        if ($assetObject->id) {
            $assetObject->conversion_status = App7020Assets::CONVERSION_STATUS_INPROGRESS;
            $assetObject->save(false);
            $fileStorageObject = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);
            $fileStorageObject->setKey($assetObject->filename);
            $outputPath = ($assetObject->contentType == App7020Assets::CONTENT_TYPE_VIDEO) ? $fileStorageObject->getKey() : $fileStorageObject->getOutputKey();
            echo json_encode(array(
                'result' => true,
                'outputPath' => ltrim($outputPath, "/"),
                'assetId' => $assetObject->id));
            Yii::app()->end();
        } else {
            echo json_encode(array('result' => false, 'error' => Yii::t("app7020", "Cannot upload the selected asset"), 'assetId' => $idAsset));
            Yii::app()->end();
        }
        Yii::app()->end();
    }

    /**
     *
     * @return type
     */
    public function actionAxAssetUploaded() {
        Yii::app()->session->close();
        $idAsset = (int) Yii::app()->request->getParam('assetId', false);
        $assetObject = App7020Assets::model()->findByPk($idAsset);
        if ($assetObject->id) {
            $assetObject->conversion_status = App7020Assets::CONVERSION_STATUS_S3;
            $assetObject->startProccessingAfterFileUploaded();
            $assetObject->save(false);
            echo json_encode(array(
                'result' => true,
                'assetId' => $assetObject->id)
            );
            Yii::app()->end();
        } else {
            echo json_encode(array('result' => false, 'error' => Yii::t("app7020", "Failed uploading the selected asset"), 'assetId' => $idAsset));
            Yii::app()->end();
        }
        Yii::app()->end();
    }

    /**
     *
     * @return type
     */
    public function actionAxAssetChangeStatusError() {
        $idAsset = (int) Yii::app()->request->getParam('assetId', false);
        $assetObject = App7020Assets::model()->findByPk($idAsset);
        if ($assetObject->id) {
            $assetObject->conversion_status = App7020Assets::CONVERSION_STATUS_SNS_ERROR;
            $assetObject->save(false);
            echo json_encode(array(
                'result' => true,
                'assetId' => $assetObject->id)
            );
            Yii::app()->end();
        } else {
            echo json_encode(array('result' => false, 'error' => Yii::t("app7020", "Failed uploading the selected asset"), 'assetId' => $idAsset));
            Yii::app()->end();
        }
        Yii::app()->end();
    }

    /**
     *
     * @return type
     */
    public function actionAxGetActiveAssetListData() {

        $activeAssetsConversionStatuses = array(0, 1, 3, 6, 7, 8, 9);
        $assets = App7020Assets::getUploadedFiles(array('proccessing_criteria' => $activeAssetsConversionStatuses));
        return $assets;
    }

    /**
     *
     * @return type
     */
    public function actionAxPollerAssetStatuses() {
        Yii::app()->session->close();
        $activeAssetsConversionStatuses = array(
            App7020Assets::CONVERSION_STATUS_PENDING,
            App7020Assets::CONVERSION_STATUS_INPROGRESS,
            App7020Assets::CONVERSION_STATUS_S3,
            App7020Assets::CONVERSION_STATUS_FROM_MOBILE,
            App7020Assets::CONVERSION_STATUS_SNS_SUCCESS,
            App7020Assets::CONVERSION_STATUS_SNS_WARNING,
            App7020Assets::CONVERSION_STATUS_SNS_ERROR,
            App7020Assets::CONVERSION_STATUS_VIDEO_LENGTH_ERROR,
            App7020Assets::CONVERSION_STATUS_FINISHED,
            App7020Assets::CONVERSION_STATUS_INREVIEW,
            App7020Assets::CONVERSION_STATUS_UNPUBLISH,
            App7020Assets::CONVERSION_STATUS_APPROVED
        );
        $activeAssets = App7020Assets::getUploadedFiles(
                array(
                    'proccessing_criteria' => $activeAssetsConversionStatuses
                )
        );

        $tmpAsset = array();

        foreach ($activeAssets as $asset) {
            $tmpAsset[] = array(
                'assetID' => $asset['id'],
                'assetFilename' => basename($asset['filename'], '.' . App7020Helpers::getExtentionByFilename($asset['filename'])),
                'conversion_status' => (int) $asset['conversion_status']
            );
        }

        echo (
        Yii::app()->request->getParam('callback') . "(" . json_encode(
            array(
                'result' => empty($tmpAsset) ? false : true,
                'activeAssets' => (array) $tmpAsset)
        ) . ")"
        );
        Yii::app()->end();
    }

    /**
     * Saves the asset's metadata, whished status (to be published or to be reviewed) and the custom setting for downloading
     * @author Stanimir Simeonov
     * @return type
     */
    public function actionAxSubmitAsset() {
        App7020Helpers::redirectToIndexIfSessionExpired('assets');
        try {
            Yii::app()->session->close();

            $assetId = Yii::app()->request->getParam('assetId', false);
            $assetDetails = Yii::app()->request->getParam('App7020Assets', array());
            $tagsToAssign = Yii::app()->request->getParam('hidden-tags', false);


            $assetDetails = filter_var_array($assetDetails, array('title' => FILTER_SANITIZE_FULL_SPECIAL_CHARS, 'description' => FILTER_SANITIZE_FULL_SPECIAL_CHARS, 'is_private' => FILTER_SANITIZE_NUMBER_INT));
            $assetId = filter_var($assetId, FILTER_SANITIZE_NUMBER_INT);
             $tagsToAssign = filter_var($tagsToAssign, FILTER_SANITIZE_STRING);

            $activeThumbnail = Yii::app()->request->getParam('thumbs', false);
            $assetObject = App7020Assets::model()->with('question_channels')->findByPk($assetId);
            $assetDownload = Yii::app()->request->getParam('assetDownload', false);
            $channnelsCheckList = Yii::app()->request->getParam('channnelsCheckList', array());
            if (!empty($assetObject->request)) {
                foreach ($assetObject->question_channels as $r_channles) {
                    $channnelsCheckList[] = $r_channles->id;
                }
            }
            //save the 'allow_asstes_download' custom setting if the option is different from the global setting
            $assetDownload = !empty($assetDownload) ? 'on' : 'off';



            if (empty($assetId) || empty($assetObject)) {
                die(json_encode(
                        array(
                            'res' => false,
                            'errors' => array(Yii::t('app7020', 'The asset was not found!')
                            )
                        )
                    )
                );
            }

            $assetObject->setAttribute("tagsToAssign", $tagsToAssign);
            $assetObject->setAttribute("activeThumbnail", $activeThumbnail);
            $assetObject->setAttribute("title", $assetDetails['title']);
            $assetObject->setAttribute("description", strip_tags(htmlspecialchars_decode($assetDetails['description']), '<span><p><strong><img><em>'));

            if ($assetObject->isSuperAdminAccess() && $assetObject->conversion_status >= App7020Assets::CONVERSION_STATUS_FINISHED) {
                $assetObject->setAttribute("is_private", ShareApp7020Settings::isGlobalAllowPrivateAssets() == false ? App7020Assets::PRIVATE_STATUS_INIT : $assetDetails['is_private']);
                $assetObject->setAttribute("relatedChannels", $channnelsCheckList);
            } elseif ($assetObject->conversion_status < App7020Assets::CONVERSION_STATUS_FINISHED) {
                $assetObject->setAttribute("is_private", ShareApp7020Settings::isGlobalAllowPrivateAssets() == false ? App7020Assets::PRIVATE_STATUS_INIT : $assetDetails['is_private']);
                $assetObject->setAttribute("relatedChannels", $channnelsCheckList);
            }


            //if is checked the allow download for any asset
            if ($assetDownload != Settings::get(App7020Assets::SETTING_ALLOW_DOWNLOAD)) {
                $customSettingObject = new App7020CustomContentSettings();
                $customSettingObject->idContent = $assetId;
                $customSettingObject->settingName = App7020Assets::SETTING_ALLOW_DOWNLOAD;
                $customSettingObject->settingValue = $assetDownload;
                $customSettingObject->save();
            }

            $isPeerReview = false;
            if ($assetObject->validate()) {
                //if validation is ok then will going to save methods
                try {
                    if ($assetObject->conversion_status == App7020Assets::CONVERSION_STATUS_SNS_SUCCESS) {
                        $assetObject->conversion_status = (int) $assetObject->is_private == App7020Assets::PRIVATE_STATUS_PRIVATE ? App7020Assets::CONVERSION_STATUS_APPROVED : App7020Assets::getFinalConversionStatus($channnelsCheckList);
//						Yii::app()->event->raise('App7020NewContent', new DEvent($assetObject, array('contentId' => $assetObject->id)));
                    }
                    // The notification should be raised ONLY if an asset goes for a "Peer review"
                    if ($assetObject->conversion_status == App7020Assets::CONVERSION_STATUS_FINISHED) {
                        //Yii::app()->event->raise('App7020NewContent', new DEvent($assetObject, array('contentId' => $assetObject->id)));
                        $isPeerReview = true;
                        Yii::app()->event->raise('App7020NewAssetToReview', new DEvent($assetObject, array('contentId' => $assetObject->id, 'author' => CoreUser::model()->findByPk(Yii::app()->user->idst))));
                    }

                    // The notification should be raised ONLY if an asset goes for a "Published"
                    if ($assetObject->conversion_status == App7020Assets::CONVERSION_STATUS_APPROVED) {

                        if (!$isPeerReview) {
                            Yii::app()->event->raise('App7020NewContent', new DEvent($assetObject, array('contentId' => $assetObject->id)));
                            //Yii::app()->event->raise('App7020NewAssetToReview', new DEvent($assetObject, array('contentId' => $assetObject->id, 'author' => CoreUser::model()->findByPk(Yii::app()->user->idst))));
                        }
                    }


                    $save = $assetObject->save();
                    $lastEdit = App7020ContentPublished::getLastEdited($assetObject->id);
                    $channelsArray = array();
                    if ($channnelsCheckList) {
                        foreach ($channnelsCheckList as $value) {
                            $channelTranslation = App7020Channels::model()->findByPk($value)->translation();

                            $channelsArray[] = array(
                                'channel_name' => $channelTranslation['name'],
                                'channel_url' => Docebo::createLmsUrl('channels/index', array('#' => "/channel/" . $value))
                            );
                        }
                    }

                    $tagsArray = array();
                    if ($assetObject->tags) {
                        foreach ($assetObject->tags as $value) {
                            $tagsArray[] = App7020Tag::model()->findByPk($value->idTag)->tagText;
                        }
                    }
                    $itemInfo = array(
                        'channels' => $channelsArray,
                        'description' => $assetObject->description,
                        'last_edit_by' => $lastEdit ? CoreUser::getForamattedNames($lastEdit->idUser, ".", false) : '',
                        'last_edit_datetime' => $lastEdit ? $lastEdit->datePublished : '',
                        'title' => $assetObject->title,
                        'tags' => $tagsArray,
                        'is_private' => $assetObject->is_private == 1
                    );
                    die(
                        json_encode(
                            array(
                                'res' => true,
                                'assetId' => $assetObject->id,
                                'assetStatus' => $assetObject->conversion_status,
                                'assetThumb' => App7020Assets::getAssetThumbnailUri($assetObject->id),
                                'assetOriginalName' => $assetObject->originalFilename,
                                'message' => Yii::t('app7020', 'The asset was updated successfull!'),
                                'itemInfo' => $itemInfo
                            )
                        )
                    );
                }
                //detect any problem with env
                catch (Exception $ex) {
                    Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
                    die(json_encode(array('res' => false, 'errors' => array(Yii::t('app7020', 'You must select a channel!')))));
                }
            }
            die(json_encode(array('res' => false, 'errors' => App7020Helpers::convertErrorsToFlatArray($assetObject->getErrors()))));
        } catch (Exception $ex) {
            Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
            die(json_encode(array('res' => false, 'errors' => array(Yii::t('app7020', 'Something went wrong!')))));
        }
    }

    /* -------------------------------------------------------------------------
     * DELETE OF FILE FROM ASSET PAGE
     * ----------------------------------------------------------------------- */

    /**
     * General invoked method from AJAX
     */
    public function actionAxDeleteFile() {
        Yii::app()->session->close();
        $local = Yii::app()->request->getParam('local', false);
        $pk = Yii::app()->request->getParam('id', false);
        $content = App7020Assets::model()->findByPk($pk);

        $output = array();
        if (!empty($content) && App7020Assets::canUserDeleteContent($pk)) {
            if ($local == 'true') {
                $output['status'] = $content->delete();
            } else {
                CoreAsset::model()->deleteByPk($content->idThumbnail);
                App7020Helpers::deleteCloudFile($content);
                App7020Helpers::deleteLocalFile($content);
                $output['status'] = $content->delete();
            }
        } else {
            Yii::t('app7020', 'Invalid User for content or content') . $content->title;
            $output['status'] = false;
        }

        echo CJSON::encode($output);
        Yii::app()->end();
    }

    /**
     * Generate  modal partial view for invite confirmation and return it to render from FE part of the project
     * @version 1.0
     */
    public function actionAxShowConfirmInviteModal() {
        $assetId = Yii::app()->request->getParam('assetId', false);
        $redirect = Yii::app()->request->getParam('redirectToEditMode', 0);
        $asset = App7020Assets::model()->findByPk($assetId);
        $this->renderPartial('app7020.protected.views.assets._confirmInviteDialog', array('asset' => $asset, 'redirect' => $redirect));
    }

    /**
     *
     */
    public function actionAxShowConfirmDeleteAssetModal() {
        $assetId = Yii::app()->request->getParam('assetId', false);
        $showName = Yii::app()->request->getParam('showName', false);
        $asset = App7020Assets::model()->findByPk($assetId);
        $this->renderPartial('app7020.protected.views.assets._confirmDeleteAssetDialog', array('asset' => $asset, 'showName' => $showName));
    }

    /**
     *
     * @param type $assetId
     */
    private function _proceedRequest($assetId) {
        $idQuestion = $this->_detectQuestionRefererId();

        $requestModel = App7020QuestionRequest::model()->findByAttributes(array('idQuestion' => $idQuestion));
        if ($requestModel->id) {

            $autoPublish = empty($_SESSION['requestedAssets'][$idQuestion]['autoPublish']) ? 0 : $_SESSION['requestedAssets'][$idQuestion]['autoPublish'];
            $autoInvite = empty($_SESSION['requestedAssets'][$idQuestion]['autoInvite']) ? 0 : $_SESSION['requestedAssets'][$idQuestion]['autoInvite'];

            $requestModel->idContent = $assetId;
            $requestModel->autoPublish = $autoPublish;
            $requestModel->autoInvite = $autoInvite;
            $requestModel->save();
            // Raise event for expert satisfied request badge.
            Yii::app()->event->raise('ExpertReachedAGoal', new DEvent($this, array('userId' => Yii::app()->user->id, 'contentId' => $assetId, 'goal' => 0)));


            $questionObject = App7020Question::model()->findByPk($idQuestion);
            if ($questionObject->id) {
                $questionObject->setScenario(App7020Question::SCENARIO_OPEN_CLOSE);
                $questionObject->idContent = $assetId;
                $questionObject->save(false);
            }
        }
    }

    public function actionAxSetGotItInformation() {
        $method = Yii::app()->request->getParam('method', false);
        $coreUserSettings = new CoreSettingUser();
        $coreUserSettings->id_user = Yii::app()->user->idst;
        $coreUserSettings->value = 1;
        if ($method == 'contributeMobileApp') {
            if (!CoreSettingUser::model()->findByAttributes(array('path_name' => 'contribute_mobile_app_gotit', 'id_user' => Yii::app()->user->idst))) {
                $coreUserSettings->path_name = 'contribute_mobile_app_gotit';
                $coreUserSettings->save();
            }
            $this->sendJSON(array('success' => true));
            Yii::app()->end();
        }
        $this->sendJSON(array('success' => false));
        Yii::app()->end();
    }

    /**
     * TAGS MODULE
     */

    /**
     * tagManager
     * This action was called from tagmanager on add new tag
     */
    function actionAxAddTag() {
        if (Yii::app()->request->isAjaxRequest) {
            $tag = trim(Yii::app()->request->getParam('tag', true));
            $tag = filter_var($tag, FILTER_SANITIZE_STRING);
            echo App7020Tag::addNewTags($tag);
        }
    }

    /**
     * Get all mached tags
     */
    public function actionAxGetAutoCompleteTags() {
        // Get search tag
        $tag = Yii::app()->request->getParam('tag', false);
        echo CJSON::encode(App7020Tag::getMachingTagsTo($tag));
    }

    /**
     * Ajax to delete only content which is no uploaded successfull to S3
     * @param - idContent array
     * @return boolean
     */
    public function actionAxDeleteUnuploaded() {
        App7020Helpers::redirectToIndexIfSessionExpired('assets');
        $idContent = Yii::app()->request->getParam('idContent', array());
        // Delete only content which is no uploaded successfull to S3
        $statuses = array(
            App7020Assets::CONVERSION_STATUS_PENDING,
            App7020Assets::CONVERSION_STATUS_FILESTORAGE,
            App7020Assets::CONVERSION_STATUS_SNS_WARNING,
            App7020Assets::CONVERSION_STATUS_SNS_ERROR,
            App7020Assets::CONVERSION_STATUS_VIDEO_LENGTH_ERROR
        );
        if ($idContent) {
            try {
                $criteria = new CDbCriteria();
                $criteria->condition = 'userId = ' . Yii::app()->user->idst;
                $criteria->addInCondition('id', $idContent);
                $criteria->addInCondition('conversion_status', $statuses);
                $model = App7020Assets::model()->deleteAll($criteria);
            } catch (Exception $ex) {
                $this->sendJSON(array('success' => false));
                Yii::app()->end();
            }
        }

        $this->sendJSON(array('success' => true));
        Yii::app()->end();
    }

    // Check if position of the new tooltip is valid
    public function actionAxCheckValidTooltip() {
        $manyTooltips = "";
        $idContent = Yii::app()->request->getParam('idContent');
        if (!$idContent) {
            $reviewId = Yii::app()->request->getParam('reviewId');
            $idContent = Yii::app()->db->createCommand()
                ->select('*')
                ->from('app7020_content_reviews')
                ->where('id=:id', array(':id' => $reviewId))
                ->queryRow();
            $idContent = $idContent["idContent"];
        }
        $durationFrom = Yii::app()->request->getParam('durationFrom');
        $durationTo = Yii::app()->request->getParam('durationTo');
        $count = App7020Tooltips::getPositionsInRange($durationFrom, $durationTo, $idContent, 0);
        if ($count >= 3) {
            $manyTooltips = 1;
        }
        echo CJSON::encode(array(
            'manyTooltips' => $manyTooltips
        ));
    }

    // Jump to selected tooltip durationFrom
    public function actionAxJumpToMoment() {
        $tooltip = Yii::app()->request->getParam('tooltip');
        $tooltip = App7020Tooltips::model()->findByPk($tooltip);
        echo CJSON::encode(array(
            'moment' => $tooltip->durationFrom
        ));
    }

    /**
     * Generate  modal partial view for invite confirmation and return it to render from FE part of the project
     * @version 1.0
     */
    public function actionAxShowMetadataModal() {
        $assetId = Yii::app()->request->getParam('assetId', false);
        $asset = App7020Assets::model()->findByPk($assetId);
        $this->renderPartial('app7020.protected.views.assets._confirmInviteDialog', array('asset' => $asset, 'redirect' => $redirect));
    }

    /**
     * Document Tracking Actions
     */
    public function actionAxUpdateDocumentTracking() {
        $contentId = Yii::app()->request->getParam('contentId');
        $page = Yii::app()->request->getParam('page');
        $userID = Yii::app()->user->idst;
        $allPages = count(App7020DocumentImages::model()->findAllByAttributes(array('idContent' => $contentId)));
        $documentTrackingObject = new App7020DocumentTracking();
        $documentTrackingObject->idContent = $contentId;
        $documentTrackingObject->idUser = $userID;
        $contentObject = App7020Assets::model()->findAllByPk($contentId);
        $currPage = $documentTrackingObject->getLastViewedPage($contentId, $userID);
        if ($page && ($page > $currPage[Page])) {
            $documentTrackingObject->Page = $page;
            $documentTrackingObject->secureSave();

            if ($page + 1 >= ((3 / 4) * $allPages) && $contentObject['userId'] != $userID) {
                App7020Assets::viewContent($contentId);
            }
        }

        echo json_encode(array('cid' => $contentId, 'uid' => $userID, 'page' => $page, 'views' => count(App7020Assets::model()->findByPk($contentId)->viewes)));
    }

    public function actionAxCreateDocumentTracking() {
        $contentId = Yii::app()->request->getParam('contentId');
        $userID = Yii::app()->user->idst;
        $content = App7020Assets::model()->findByPk($contentId);
        $documentGetTrackingObject = new App7020DocumentTracking();
        $page = $documentGetTrackingObject->getLastViewedPage($contentId, $userID);
        $isViewed = App7020ContentHistory::model()->findByAttributes(array('idContent' => $contentId, 'idUser' => $userID));
        if (App7020DocumentImages::countDocumentImages($contentId) == 1 && $content->contentType == App7020Assets::CONTENT_TYPE_PDF && $content->userId != $userID && !$isViewed) {
            $view = new App7020ContentHistory();
            $view->idContent = $contentId;
            $view->idUser = $userID;
            $view->save();
        }
        if ($content->contentType == App7020Assets::CONTENT_TYPE_IMAGE && $content->userId != $userID && !$isViewed) {
            $view = new App7020ContentHistory();
            $view->idContent = $contentId;
            $view->idUser = $userID;
            $view->save();
        }

        if (!$page) {
            $documentGetTrackingObject->idContent = $contentId;
            $documentGetTrackingObject->idUser = $userID;
            $documentGetTrackingObject->secureSave();
            echo json_encode(array('cid' => $contentId, 'uid' => $userID, 'page' => 0, 'views' => count(App7020Assets::model()->findByPk($contentId)->viewes)));
        }
    }

}
