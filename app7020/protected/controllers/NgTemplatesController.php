<?php

/**
 *
 *
 */
class NgTemplatesController extends App7020BaseController {

	/**
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		$res = array();

		$res[] = array(
			'allow',
			'users' => array('*'),
			'actions' => array('login'),
		);
 
		return $res;
	}
 
	
	/**
	 * Returns view HTML for angular JS
	 * @param $viewName
	 */
	public function actionView() {
		$viewName = Yii::app()->request->getParam('view_name');
		try {
			$this->renderPartial('app7020.protected.views.' . $viewName);
		} catch (Exception $e) {
			Yii::app()->end();
		}
	}
	

}
