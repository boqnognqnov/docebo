<?php

class AxExpertsChannelsController extends ExpertsChannelsController {

	public function accessRules() {
		return parent::accessRules();
	}

	public function actionDialogSelectExperts() {
		$confirm = Yii::app()->request->getParam("confirm", FALSE);

		try {
			// Confirm button has been clicked ?
			if ($confirm) {
				$selectedUsers = UsersSelector::getUsersListOnly($_REQUEST);
				foreach ($selectedUsers as $value) {
					$expertModel = App7020Experts::model()->findByAttributes(array('idUser' => $value));
					if (!$expertModel->id) {
						$newExpert = new App7020Experts();
						$newExpert->idUser = $value;
						$newExpert->idAdmin = Yii::app()->user->idst;
						$newExpert->save();
					}
				}
				if ($selectedUsers) {
					$this->renderPartial("_progressAddExperts", array('expertsCount' => count($selectedUsers)));
				} else {
					Yii::app()->user->setFlash('error', Yii::t('app7020', 'Select at least one user'));
					$collectorUrl = Docebo::createApp7020Url('experts/dialogSelectExperts', array());

					$selectorDialogUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
								'type' => UsersSelector::TYPE_APP7020_ADD_EXPERTS,
								'collectorUrl' => $collectorUrl, // Make this action also collector of the users selector submition
								'itemsPerPage' => 8
					));
					// Redirect to UsersSelector
					$this->redirect($selectorDialogUrl, true);
					//echo '<a class="auto-close"></a>'; s
				}
				Yii::app()->end();
			}

			// Where selector will submit its data to be collected (THIS action again)
			$collectorUrl = Docebo::createApp7020Url('experts/dialogSelectExperts', array());

			$selectorDialogUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
						'type' => UsersSelector::TYPE_APP7020_ADD_EXPERTS,
						'collectorUrl' => $collectorUrl, // Make this action also collector of the users selector submition
						'itemsPerPage' => 8
			));
			// Redirect to UsersSelector
			$this->redirect($selectorDialogUrl, true);
			Yii::app()->end();
		} catch (Exception $e) {

			$this->renderPartial('admin.protected.views.common/_dialog2_error', array(
				'message' => $e->getMessage(),
				'type' => 'error',
			));
			Yii::app()->end();
		}
	}

	public function actionDialogMultipleAssign() {

		$custom_Select = App7020Channels::customProviderSelect(array('num_experts' => false, 'num_visibility_structures' => true, 'friendly_permissions' => true));
		$custom_channel_visibility_select = array(
			"idUser" => Yii::app()->user->idst,
			"ignoreVisallChannels" => false,
			"ignoreGodAdminEffect" => false,
			'appendEnabledEffect' => true,
			'ignoreSystemChannels' => false,
			'customSelect' => $custom_Select,
			'return' => 'provider'
		);

		$dp = App7020Channels::extractUserChannels($custom_channel_visibility_select);
		if ((int) $dp->totalItemCount === 0) {
			$this->renderPartial('app7020.protected.views.expertsChannels._dialogEmptyChannels');
		} elseif (!App7020Experts::model()->findAll()) {
			$this->renderPartial('app7020.protected.views.expertsChannels._dialogEmptyExperts');
		} else {

			$confirm = Yii::app()->request->getParam("confirm", FALSE);
			$url = Docebo::createAppUrl('app7020:axExpertsChannels/AxMultipleSelectChannels');

			$columns = $this->_getChannelsColums();

			$channelsGridViewParams = array(
				'gridId' => 'channelsGridView',
				'dataProvider' => $dp,
				'columns' => $columns,
				'disableMassSelection' => false,
				'disableMassActions' => true,
				//'preselectedItems' => $preselectedChannels,
				'massSelectorUrl' => $url,
				'noYiiGridViewJS' => true,
				'gridTemplate' => '{items}{summary}{pager}',
			);

			// Confirm button has been clicked ?
			if ($confirm) {
				$selectedUsers = Yii::app()->request->getParam("selected_users", false);
				$selectedUsersDecoded = json_decode($selectedUsers);
				if (!$selectedUsersDecoded) {

					$collectorUrl = Docebo::createApp7020Url('axExpertsChannels/dialogMultipleAssign', array());
					$preselectedUsers = Yii::app()->request->getParam("preselectedUsers", array());
					$_SESSION['app7020_preselected_channels'] = Yii::app()->request->getParam("preselectedChannels", false);

					if (!empty($preselectedUsers)) {
						$preselectedUsers = json_decode($preselectedUsers);
					}

					$selectorDialogUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
								'type' => UsersSelector::TYPE_APP7020_EXPERTS,
								'preselectedExperts' => $preselectedUsers,
								'collectorUrl' => $collectorUrl, // Make this action also collector of the users selector submition
								'itemsPerPage' => 8
					));
					Yii::app()->user->setFlash('error', Yii::t('app7020', 'Select at least one user'));
					// Redirect to UsersSelector
					$this->redirect($selectorDialogUrl, true);
					Yii::app()->end();
				}


				$preselectedChannels = empty($_SESSION['app7020_preselected_channels']) ? array() : $_SESSION['app7020_preselected_channels'];

				if (!empty($preselectedChannels)) {
					foreach ($preselectedChannels as $key => $value) {
						$preselectedChannels[$key] = (int) $value;
					}
				}

				if (isset($_SESSION['app7020_preselected_channels'])) {
					unset($_SESSION['app7020_preselected_channels']);
				}

				$channelsGridViewParams['preselectedItems'] = $preselectedChannels;



				$this->renderPartial('app7020.protected.views.expertsChannels._dialogSelectChannels', array(
					'expertId' => $selectedUsers,
					'url' => $url,
					'showPrevious' => 1,
					'channelsGridViewParams' => $channelsGridViewParams,
						), false, true);

				Yii::app()->end();
			}
			//pagination of channels list
			if ($_REQUEST['ajax'] && $_REQUEST['ajax'] == 'channelsGridView') {

				$this->renderPartial('app7020.protected.views.expertsChannels._dialogSelectChannels', array(
					'expertId' => $selectedUsers,
					'url' => $url,
					'showPrevious' => 1,
					'channelsGridViewParams' => $channelsGridViewParams,
						), false, true);

				Yii::app()->end();
			}

			// Where selector will submit its data to be collected (THIS action again)
			$collectorUrl = Docebo::createApp7020Url('axExpertsChannels/dialogMultipleAssign', array());
			$preselectedUsers = Yii::app()->request->getParam("preselectedUsers", array());
			$_SESSION['app7020_preselected_channels'] = Yii::app()->request->getParam("preselectedChannels", false);

			if ($preselectedUsers) {
				$preselectedUsers = json_decode($preselectedUsers);
			}

			$selectorDialogUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
						'type' => UsersSelector::TYPE_APP7020_EXPERTS,
						'preselectedExperts' => $preselectedUsers,
						'collectorUrl' => $collectorUrl, // Make this action also collector of the users selector submition
						'noyiixviewjs' => true,
						'itemsPerPage' => 8
			));

			// Redirect to UsersSelector
			$this->redirect($selectorDialogUrl, true);
			Yii::app()->end();
		}
	}

	/**
	 * @param $idUser Primary Keys of users
	 * @return string String of json code with success and message
	 */
	public function actionDeleteExpert($idUser = null) {
		$users = array();
		if (!empty($_POST['idUser'])) {
			$idUser = $_POST['idUser'];
			if (is_string($idUser)) {
				if (stripos($idUser, ',') !== false)
					$users = explode(',', $idUser);
				else
					$users = array($idUser);
			}
		} elseif (!empty($idUser)) {
			$users = array($idUser);
		}

		if (empty($users)) {
			$this->sendJSON(array());
		}

		if (!empty($_POST['agreeCheck'])) {
			$experts = explode(",", $_POST['idUser']);
			foreach ($experts as $expertId) {
				$expertObject = App7020Experts::model()->findByAttributes(array('idUser' => $expertId));
				if ($expertObject->id) {
					$expertObject->delete();
				}
			}
			echo '<a class="auto-close success"></a>';
			Yii::app()->end();
		}

		if (Yii::app()->request->isAjaxRequest) {
			$this->renderPartial('app7020.protected.views.expertsChannels._deleteExperts', array(
				'users' => $users,
				'idUser' => $idUser
					), false, true);
		}
		Yii::app()->end();
	}

	public function actionAxDialogSelectChannels() {
 		//if "Select all" from modal channels list is clicked
		if (isset($_REQUEST['comboGridViewSelectAll'])) {
 			$custom_channel_visibility_select = array(
				"idUser" => Yii::app()->user->idst,
				"ignoreVisallChannels" => false,
				"ignoreGodAdminEffect" => !Yii::app()->user->getIsGodadmin(),
				'appendEnabledEffect' => true,
				'ignoreSystemChannels' => true,
 				'return' => 'data'
			);
			
 			$channels = App7020Channels::extractUserChannels($custom_channel_visibility_select);
				
			$keys = array();
			foreach ($channels as $value) {
				$keys[] = (int) $value['id'];
			}

			$this->sendJSON(CJSON::encode($keys));
			Yii::app()->end();
		}

		$themeUrl = Yii::app()->theme->baseUrl;
		Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/user_management.js', CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerCssFile($themeUrl . '/css/user_management.css');


		$idExpert = $this->_detectExpertRefererId();

		$url = Docebo::createAppUrl('app7020:axExpertsChannels/AxDialogSelectChannels', array('idExpert' => $idExpert));
		$confirm = Yii::app()->request->getParam('yt0', false);

		if ($confirm == 'Confirm') {
			echo '<a class="auto-close success"></a>';
			Yii::app()->end();
		}

		$columns = $this->_getChannelsColums();
		$existingChannels = App7020ChannelExperts::getExpertsChannels($idExpert)->rawData;
		$excludeChannels = array();
		foreach ($existingChannels as $value) {
			$excludeChannels[] = $value['id'];
		}
		$custom_select_args = array('num_experts' => false, 'num_visibility_structures' => true, 'friendly_permissions' => true);
		$custom_channel_visibility_select = array(
				"idUser" => Yii::app()->user->idst,
				"ignoreVisallChannels" => false,
				"ignoreGodAdminEffect" => !Yii::app()->user->getIsGodadmin(),
				'appendEnabledEffect' => true,
				'ignoreSystemChannels' => true,
				'ignoreChannels' => $excludeChannels,
				'customSelect' => App7020Channels::customProviderSelect($custom_select_args),
 				'return' => 'provider'
			);
 			$dp = App7020Channels::extractUserChannels($custom_channel_visibility_select);
			
		$channelsGridViewParams = array(
			'gridId' => 'channelsGridView',
			'dataProvider' => $dp,
			'columns' => $columns,
			'disableMassSelection' => false,
			'disableMassActions' => true,
			'gridTemplate' => '{items}{summary}{pager}',
			'massSelectorUrl' => $url,
		);

		if (!Yii::app()->request->getParam('loadJs', false)) {
			$channelsGridViewParams['noYiiGridViewJS'] = true;
		}

		$this->renderPartial('app7020.protected.views.expertsChannels._dialogSelectChannels', array(
			'channelsGridViewParams' => $channelsGridViewParams,
			'url' => $url,
			'expertId' => $idExpert
				), false, true);
	}

	public function actionAxDeleteChannels() {
		$items = Yii::app()->request->getParam('items', false);
		$agreeCheck = Yii::app()->request->getParam('agreeCheck', false);
		$forDelete = Yii::app()->request->getParam('forDelete', false);
		$expertId = $this->_detectExpertRefererId();
		if ($forDelete) {
			$items = explode(',', $forDelete);
		}
		$this->renderPartial('app7020.protected.views.expertsChannels._deleteExpertChannels', array('items' => $items));

		if (Yii::app()->request->isAjaxRequest && $agreeCheck && $forDelete) {
			foreach ($items as $idChannel) {
				App7020ChannelExperts::model()->deleteAllByAttributes(array('idChannel' => $idChannel, 'idExpert' => $expertId));
			}
			echo '<a class="auto-close success"></a>';
			Yii::app()->end();
		}
	}

	/**
	 * get selected experts and topics, and assigng it
	 */
	public function actionAxMultipleSelectChannels() {

		//if "Select all" from modal channels list is clicked
		if (isset($_REQUEST['comboGridViewSelectAll'])) {
			
			$custom_channel_visibility_select = array(
				"idUser" => Yii::app()->user->idst,
				"ignoreVisallChannels" => false,
				"ignoreGodAdminEffect" => !Yii::app()->user->getIsGodadmin(),
				'appendEnabledEffect' => true,
				'ignoreSystemChannels' => true,
 				'return' => 'data'
			);
 			$channels = App7020Channels::extractUserChannels($custom_channel_visibility_select);
		
 			$keys = array();
			foreach ($channels as $value) {
				$keys[] = (int) $value['id'];
			}

			$this->sendJSON(CJSON::encode($keys));
			Yii::app()->end();
		}

		if (Yii::app()->request->getParam('yt1', false)) {
			$expertsString = Yii::app()->request->getParam('experts');
			if ($expertsString) {
				$experts = json_decode($expertsString);
			}

			$channels = Yii::app()->request->getParam('channelsSelected');
			$channels = explode(",", $channels);

			if (!$channels) {
				Yii::app()->user->setFlash('error', Yii::t('app7020', 'Select at least one channel'));

				$url = Docebo::createAppUrl('app7020:experts/AxMultipleSelectChannels');

				$this->renderPartial('_dialogSelectTopics', array(
					'expertId' => $expertsString,
					'url' => $url,
					'preselected' => array(),
					'showPrevious' => 1
				));

				Yii::app()->end();
			}

			foreach ($experts as $expert) {
				$expertId = App7020Experts::model()->findByAttributes(array('idUser' => $expert))->id;
				if ($expertId) {
					foreach ($channels as $channel) {
						$model = App7020ChannelExperts::model()->findByAttributes(array('idChannel' => $channel, 'idExpert' => $expertId));
						if (!$model->id) {
							$newModel = new App7020ChannelExperts();
							$newModel->idExpert = $expertId;
							$newModel->idChannel = $channel;
							$newModel->save();
						}
					}
				}
			}
			$this->renderPartial("app7020.protected.views.expertsChannels._progressMultiple", array('expertsCount' => count($experts), 'topicsCount' => count($channels)));
			Yii::app()->end();
		}
	}

	/**
	 * Return id by Referer from AJAX
	 * @return boolean|integer
	 */
	private function _detectExpertRefererId() {

		if (Yii::app()->request->getParam('expertId', false)) {
			$id = Yii::app()->request->getParam('expertId');
		} else if (isset($_SERVER['HTTP_REFERER'])) {
			$url = $_SERVER['HTTP_REFERER'];
			preg_match('/expertId=[0-9A-Za-z]*/', $url, $matches);
			$value = explode("=", $matches[0]);
			$id = $value[1];
		} else {
			return false;
		}
		return (int) $id;
	}

	public function actionAxAddChannels() {
		$idExpert = $this->_detectExpertRefererId();
		$selectedChannels = Yii::app()->request->getParam('channels', array());
		foreach ($selectedChannels as $value) {
			$ecModel = App7020ChannelExperts::model()->findByAttributes(array('idExpert' => $idExpert, 'idChannel' => $value));
			if (!$ecModel->id) {
				$ecModel = new App7020ChannelExperts();
				$ecModel->idExpert = $idExpert;
				$ecModel->idChannel = $value;
				$ecModel->save();
			}

			/* Yii::app()->event->raise(EventManager::EVENT_APP7020_EXPERTS_ASSIGNED_TO_CHANNEL, new DEvent($this, array(
			  'channelModel' 	=> $value,
			  'experts'	=> $idExpert,
			  'superAdminId' => Yii::app()->user->idst
			  )));
			 */
		}
	}

	private function _getChannelsColums() {
		return array(
			array(
				'class' => 'CCheckBoxColumn',
				'id' => 'userman-grid-checkboxes'
			),
			array(
				'name' => 'name',
				'value' => 'app7020Channels::channelColumnName($data, false);',
				'type' => 'raw',
				'header' => Yii::t('app7020', 'Channel'),
				'htmlOptions' => array('class' => 'channel_name'),
				'headerHtmlOptions' => array()
			),
			array(
				'name' => 'upload_permissions',
				'value' => 'app7020Channels::channelColumnUploadPermission($data);',
				'type' => 'raw',
				'header' => Yii::t('app7020', 'Upload Permissions'),
				'htmlOptions' => array(
					"class" => "channel_upload_permissions text-center",
				),
				'headerHtmlOptions' => array(
					"class" => "text-center"
				)
			),
			array(
				'name' => 'visibility',
				'value' => 'app7020Channels::channelColumnVisibility($data);',
				'type' => 'raw',
				'header' => Yii::t('app7020', 'Visibility'),
				'htmlOptions' => array(
					"class" => "channel_visibility text-center",
				),
				'headerHtmlOptions' => array(
					"class" => "text-center",
				)
			)
		);
	}

}
