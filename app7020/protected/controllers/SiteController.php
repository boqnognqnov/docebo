<?php

/**
 *
 *
 */
class SiteController extends App7020BaseController {

	/**
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		$res = array();

		$res[] = array(
			'allow',
			'users' => array('*'),
			'actions' => array('login'),
		);

		$res[] = array(
			'allow',
			'users' => array('@'),
			'actions' => array('logout', 'index', 'view'),
		);

		$res[] = array(
			'deny',
			'users' => array('*'),
			'deniedCallback' => array($this, 'accessDeniedCallback'),
		);

		return $res;
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo CHtml::encode($error['message']);
			} else {
				$this->renderPartial('error', $error);
			}
		}
	}
	
	
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout() {
		if (Yii::app()->user->id && !Yii::app()->user->getIsGuest()) {
			LearningTracksession::model()->updateAll(array('active' => 0), 'active = 1 AND idUser = ' . Yii::app()->user->id);
		}

		$previouslyLoggedUser = Yii::app()->user->loadUserModel();
		Yii::app()->user->logout();

		// Trigger event for plugins post-logout processing. This call may never return.
		Yii::app()->event->raise("UserLogout", new DEvent($this, array('user' => $previouslyLoggedUser)));

		if (Settings::get('user_logout_redirect') == 'on' && Settings::get('user_logout_redirect_url')) {
			$url = Settings::get('user_logout_redirect_url');
			if (strpos($url, 'http') === false)
				$url = 'http://' . $url;

			$this->redirect($url);
		}

		$this->redirect(Docebo::createAbsoluteLmsUrl('site/index'));
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
		Yii::app()->tinymce;
		//$this->redirect(Docebo::createLmsUrl('site/index'));
		$amdController = array();
		$amdController['data']['data-main'] = $this->getAssetsUrl() . '/js/assetPlay';
		Yii::app()->event->raise('registerAMDScript', new DEvent($this, $amdController));
		$themeUrl = Yii::app()->theme->baseUrl;
		$this->getClientScript()->registerCssFile($this->getAssetsUrl() . '/css/mce_mention/rte-content.css');
		$this->getClientScript()->registerCssFile($this->getAssetsUrl() . '/css/mce_mention/autocomplete.css');
		$this->breadcrumbs = array(
			Yii::t('app7020', 'Channels') => Docebo::createLmsUrl('channels/index'),
		);
		$this->render('ng-layout', array());
	}

	/**
	 * @see Controller::resolveLayout()
	 */
	public function resolveLayout() {
		$this->layout = "app7020Fluid";
	}

	/**
	 * Returns view HTML for angular JS
	 * @param $viewName
	 */
	public function actionView() {
		$viewName = Yii::app()->request->getParam('view_name');

		try {
			$this->renderPartial('app7020.protected.views.' . $viewName);
		} catch (Exception $e) {
			Yii::app()->end();
		}
	}

	/**
	 * Returns view HTML for angular JS
	 * @param $viewName
	 */
	public function actionViewDetails() {
		$viewName = Yii::app()->request->getParam('view_name');
		$idAsset = Yii::app()->request->getParam('idAsset');
		$content = App7020Assets::model()->findByPk($idAsset);
		try {

			if ($content instanceof App7020Assets) {
				$this->renderPartial('app7020.protected.views.' . $viewName, array('content' => $content, 'form_args' => array()));
			} else {
				die('Unknown Asset id or missing asset');
			}
		} catch (Exception $e) {
			Yii::app()->end();
		}
	}

}
