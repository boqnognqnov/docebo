<?php

/**
 * 
 *
 */
class SettingsController extends App7020BaseController {
    
    private $show = 'show';

    /**
     *
     * @see CController::filters()
     */
    public function filters() {
        return array(
            'accessControl'
        ); // perform access control for CRUD operations
    }

    /**
     *
     * @see CController::accessRules()
     */
    public function accessRules() {
        $res = array();
        $res[] = array(
            'allow',
            'users' => array(
                '*'
            ),
            'actions' => array(
                'login'
            )
        );
        $res[] = array(
            'allow',
            'users' => array(
                '@'
            ),
            'actions' => array(
                'logout',
                'index'
            )
        );
        return $res;
    }

    /**
     *  
     *  
     */
    public function actionIndex() {
        $form = new App7020SettingsGeneralForm();
        $radio_list_args = array(
            'data-act' => 'additional',
            'labelOptions' => array(
                'class' => 'control-label',
            ),
            'template' => '<div class="control-group">{input}{label}</div>',
        );
         
        $sendDataToView = array(
            'model' => $form,
            'showAdditionalContainer' => array(
                'video_contribute' => $form->video_contribute == App7020SettingsGeneralForm::VIDEO_DURATION ? $this->show : '',
                'other_contribution' => $form->other_contribution == App7020SettingsGeneralForm::ON ? $this->show : '',
                'limit_characters' => $form->limit_questions == App7020SettingsGeneralForm::ON ? $this->show : '',
            ),
            'radio_list_args' => $radio_list_args,
        );

        $data_for_view = array(
            'radio_list_args' => $radio_list_args,
            'customTabsForm' => array(
                'menuHeading' => Yii::t('app7020', 'Settings'),
                'menu' => array(
                    array('link-id' => 'general-settings',
                        'name' => Yii::t('app7020', 'General Settings'),
                        'icon-id' => 'fa-cog'),
                ),
                'content' => array(
                    array(
                        'contentHeading' => Yii::t('app7020', 'General Settings'),
                        'viewName' => '_general_settings',
                        'link-id' => 'general-settings',
                        'sendDataToView' => $sendDataToView,
                    ),
                )
            )
        );

        $this->render('index', $data_for_view);
    }

    public function registerResources() {
        parent::registerResources();
    }

    public function actionProcessForm() {
        $submitedParams = Yii::app()->request->getParam("App7020SettingsGeneralForm", 0);
        $ajaxSuccess['success'] = false;
        if ($submitedParams && Yii::app()->request->isAjaxRequest) {
            $form = new App7020SettingsGeneralForm();
            //$form->attributes = $submitedParams;
			$form->setAttributes($submitedParams, $safeOnly);
            if ($form->validate()) {
                if ($form->save()) {
                    $ajaxSuccess['success'] = true;
                } else {
                    $ajaxSuccess['errorMsg'] = Yii::t('app7020', yii::t('app7020', 'Error saving'));
                }
            } else {
                $errorMsg = array();
                foreach ($form->getErrors() as $error) {
                    foreach ($error as $err) {
                        $errorMsg[] = $err;
                    }
                }
                $ajaxSuccess['errorMsg'] = $errorMsg;
            }
        } else {
            return false;
        }
        echo json_encode($ajaxSuccess);
    }

}
