<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AssetSummaryReportController
 *
 * @author Trayan
 */
class ExpertSummaryController extends App7020BaseController {
	
	public function init(){
		parent::init();
		$amdController = array();
		$amdController['data']['data-main'] = $this->getAssetsUrl() . '/js/expertSummary/main';
		Yii::app()->event->raise('registerAMDScript', new DEvent($this, $amdController));
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/admin.css');
		
		DatePickerHelper::registerAssets();

		$assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin.js'));

		Yii::app()->clientScript->registerScriptFile($assetsUrl . '/charts/aight.min.js');
		Yii::app()->clientScript->registerScriptFile($assetsUrl . '/charts/jquery.tipsy.min.js');
		Yii::app()->clientScript->registerCssFile($assetsUrl . '/charts/charts.css');
		Yii::app()->clientScript->registerScriptFile($assetsUrl . '/print/jquery.print-preview.js');
		Yii::app()->clientScript->registerCssFile($assetsUrl . '/print/css/print-preview.css');
		Yii::app()->clientScript->registerCssFile(Yii::app()->theme->getBaseUrl() . '/css/classroom.css');
		Yii::app()->clientScript->registerCssFile(
			Yii::app()->clientScript->getCoreScriptUrl().
			'/jui/css/base/jquery-ui.css'
		);
	}

	/**
	 *
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl'
		);
	}

	/**
	 *
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		$res = array();
		$res[] = array(
			'allow',
			'users' => array(
				'*'
			),
			'actions' => array(
				'login'
			)
		);
		$res[] = array(
			'allow',
			'users' => array(
				'@'
			),
			'actions' => array(
				'logout',
				'index'
			)
		);
		return $res;
	}


	public function actionIndex(){
		if(!$idExpert = Yii::app()->request->getParam("id", false))
				Yii::app()->end();
		
		if(!$expertModel = App7020Experts::model()->findByAttributes(array('idUser' => $idExpert)))
				Yii::app()->end();
		$userModel = CoreUser::model()->findByPk($expertModel->idUser);
		$expertChannels = App7020ChannelExperts::model()->getExpertsChannels($expertModel->id,array());
		
		$data['expertModel'] = $expertModel;
		$data['userModel'] = $userModel;
		$data["channels"] = $expertChannels->rawData;
		
		$this->breadcrumbs[Yii::t('menu', 'Admin')] = Docebo::createAppUrl('admin:dashboard/index');
		$this->breadcrumbs[Yii::t('standard', '_REPORTS')] = Docebo::createAdminUrl('reportManagement/index');
		
		$data['activityChannels'] = array(
			'data' => App7020QuestionAnswerStatistic::chartMyActivityPerChannel(null, Yii::app()->request->getParam('id', false))
		);
		
		$data['activityChannelsPR'] = array(
			'data' => App7020PeerReviewStatistic::chartMyActivityPerChannel(null, Yii::app()->request->getParam('id', false))
		);
		$data["countExperts"] = App7020ChannelExperts::countExpertsPerChannelList($expertChannels->rawData);
		$this->render('app7020.protected.views.summary.coach_activity', $data);
	}

	

	public function actionAxGetCoachActivity() {
		$currentUser = Yii::app()->request->getParam('id', false);
		$timeframe = (int) Yii::app()->request->getParam('timeframe', false);
		$statistic = new App7020QuestionAnswerStatistic();
		$ca = $statistic->getAllCoachActivity($timeframe,$currentUser,true);
		die(json_encode($ca));
	}
	
	public function actionExpertsAutocomplete() {
		
		$searchText = $_REQUEST['data']['query'];

		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->select("IF(cu.firstname = '' AND cu.lastname = '', "
				. "TRIM(LEADING '/' FROM userid), TRIM(CONCAT(cu.firstname, ' ',cu.lastname))) as names, cu.idst as ID");

		$dbCommand->from(App7020Experts::model()->tableName()." e");
		$dbCommand->join(CoreUser::model()->tableName()." cu", "e.idUser = cu.idst");
		$dbCommand->where("CONCAT(cu.firstname, ' ', cu.lastname, ' ', cu.userid) LIKE :searchText", array(':searchText' => "%".$searchText."%"));
		$experts = $dbCommand->queryAll();
		$returnArray = array();
		
		if ($experts) {
			foreach ($experts as $value) {
				$returnArray[$value['ID']] = $value['names'];
			}
		}

		$this->sendJSON(array('options' => $returnArray));
	}

}