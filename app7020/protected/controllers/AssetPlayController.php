<?php

/**
 * Description of AssetPlayController
 *
 * @author Stanimir
 */
class AssetPlayController extends App7020BaseController {

	function init() {
		// Load TinyMCE
		Yii::app()->tinymce;
		parent::init();
	}

	/**
	 * Access filters
	 * @return array
	 */
	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}

	/**
	 * Access rules for this controller
	 * @return array
	 */
	public function accessRules() {
		$res = array();

		// Allow access only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);
		return $res;
	}

	function actionIndex() {
		$amdController = array();
		$amdController['data']['data-main'] = $this->getAssetsUrl() . '/js/assetPlay';
		Yii::app()->event->raise('registerAMDScript', new DEvent($this, $amdController));
		$themeUrl = Yii::app()->theme->baseUrl;
		$this->getClientScript()->registerCssFile($this->getAssetsUrl() . '/css/mce_mention/rte-content.css');
		$this->breadcrumbs = array(
			Yii::t('app7020', 'Channels') => Docebo::createLmsUrl('channels/index'),
		);
		$this->render('layout', array());
	}

}
