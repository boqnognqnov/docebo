<?php

/**
 * App7020AmazonSnsController
 * This class will wait for SNS response from amazon service about jobs posted by App7020
 * @author Stanimir
 */
class App7020AmazonSnsController extends AmazonSnsController {

	/**
	 * Store all requests posted in system from  AWS
	 * @var array 
	 */
	private $_request;

	/**
	 *
	 * @var string 
	 */
	private $_snsType = '';

	/**
	 * Store all returned headers from s3
	 * @var array
	 */
	private $_s3Headers = array();

	/**
	 * When we have success notification response, here will be stored array converted 
	 * data from s3, posted in JSON object with key MESSAGES
	 * 
	 * @var array 
	 */
	private $_s3Model = array();

	/**
	 * Storage instance
	 * @var CS3StorageSt 
	 */
	private $_storage;

	/**
	 * Video Convertion instance
	 * @var VideoConverterST
	 */
	private $_video_convert;

	/**
	 * Store file input key
	 * @var string 
	 */
	private $_inputPrefix;

	/**
	 * Store content model 
	 * @var App7020Assets
	 */
	private $_contentModel;

	function __construct($mvc_name = '') {

		parent::__construct($mvc_name);
		//read posted request
		$this->_request = $this->_readRequest();
		$this->_s3Headers = $this->_readHeaders();


		//we must to valide response
		if (!$this->_valideResponse()) {
			Yii::app()->end();
		} else {
			$this->_inputPrefix = $this->_getInputFileName();
			$this->_storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);
			$this->_video_convert = new VideoConverterST($this->_storage);
		}
	}

	public function actionTranscoding() {

		switch ($this->_snsType) {
			case 'SubscriptionConfirmation':
				$this->_getContentModel();

				if (($this->_contentModel instanceof App7020Assets) === false) {
					try {
						Yii::log('CONFIRM UNREAL SUBSCRIBE FOR "' . Yii::app()->request->getParam("marker", false) . '": -----', CLogger::LEVEL_INFO);
						Yii::log('UNREAL COMPLETE : ' . $this->_video_convert->snsTopicCompleted . ' -----', CLogger::LEVEL_INFO);
						Yii::log('UNREAL COMPLETE : ' . $this->_video_convert->snsTopicError . ' -----', CLogger::LEVEL_INFO);
						$this->_snsConfirm();
						Yii::log('UNSUBSCRIBE UNREAL "' . Yii::app()->request->getParam("marker", false) . '" : -----', CLogger::LEVEL_INFO);
						$url = VideoConverterST::getSnsUrl(Yii::app()->request->getParam("marker", false));
						AmazonSnsHelper::unsubscribeUrlFromTopic($this->_storage->getS3Key(), $this->_storage->getS3Secret(), $this->_storage->getS3Region(), $url, $this->_video_convert->snsTopicCompleted);
						AmazonSnsHelper::unsubscribeUrlFromTopic($this->_storage->getS3Key(), $this->_storage->getS3Secret(), $this->_storage->getS3Region(), $url, $this->_video_convert->snsTopicError);
						Yii::log('UNREAL UNSUBSCRIBE : ' . $this->_video_convert->snsTopicCompleted . ' -----', CLogger::LEVEL_INFO);
						Yii::log('UNREAL UNSUBSCRIBE : ' . $this->_video_convert->snsTopicError . ' -----', CLogger::LEVEL_INFO);
						Yii::app()->end();
					} catch (Exception $ex) {
						Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
						Yii::app()->end();
					}
				} elseif (($this->_contentModel instanceof App7020Assets)) {
					try {
						$this->_contentModel->sns = 1;
						$this->_contentModel->save(false);
						$this->_snsConfirm();
						Yii::app()->end();
					} catch (Exception $ex) {
						Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
						$url = VideoConverterST::getSnsUrl(Yii::app()->request->getParam("marker", false));
						AmazonSnsHelper::unsubscribeUrlFromTopic($this->_storage->getS3Key(), $this->_storage->getS3Secret(), $this->_storage->getS3Region(), $url, $this->_video_convert->snsTopicCompleted);
						AmazonSnsHelper::unsubscribeUrlFromTopic($this->_storage->getS3Key(), $this->_storage->getS3Secret(), $this->_storage->getS3Region(), $url, $this->_video_convert->snsTopicError);
						Yii::app()->end();
					}
				} elseif (!($this->_contentModel instanceof App7020Assets)) {
					Yii::app()->end();
				}
				Yii::app()->end();
				break;
			case 'Notification':
				$this->_s3Model = $this->_proccessAWSResponse();
				$key = basename($this->_s3Model['input']['key']);
				sleep(1);
				if ($key != $this->_inputPrefix) {
					Yii::app()->end();
				}
				$this->_getContentModel();
				if (($this->_contentModel instanceof App7020Assets)) {

					switch ($this->_s3Model['state']) {
						case 'COMPLETED':
							Yii::log('COMPLETED: ' . $this->_s3Model['jobId'], CLogger::LEVEL_INFO);
							$this->_contentModel->duration = (int) $this->_s3Model['outputs'][0]['duration'];

							$fileParts = explode('.', $key);
							$fileNameWithoutExtension = reset($fileParts);
							Yii::app()->event->raise('onApp7020TranscodingCompleted', new DEvent($this, array(
								'marker' => $fileNameWithoutExtension
							)));

							$path = $this->_getWorkingFolder();
							foreach ($this->_getThumbnails($path . '/thumb') as $thumbnailIterationKey => $filename) {
								preg_match('/(.*)\-(\d+)\.(\w+)$/i', $filename, $file_count);
								$thumb = new App7020ContentThumbs();
								$thumb->file = $file_count[2];
								$thumb->idContent = $this->_contentModel->id;
								//add first thumbnail as default(always)
								if ($thumbnailIterationKey == 0) {
									$thumb->is_active = 1;
								} else {
									$thumb->is_active = 0;
								}
								$thumb->save(false);

								if ($thumb->is_active == 1) {
									$thumbObject = App7020ContentThumbs::model()->findByPk($thumb->id);

									$generateImage = App7020VideoProceeder::generateThumbCoreAsset($this->_contentModel, $thumbObject);
									$this->_contentModel->idThumbnail = $generateImage->id;
								}
							}
 							if (App7020Assets::validateCustomDuration($this->_contentModel)) {
								$this->_contentModel->conversion_status = App7020Assets::CONVERSION_STATUS_SNS_SUCCESS;
								$this->_contentModel->save(false);
							} else {
								$this->_contentModel->conversion_status = App7020Assets::CONVERSION_STATUS_VIDEO_LENGTH_ERROR;
								$this->_contentModel->save(false);
							}
							break;
						case 'ERROR':
							Yii::log(print_r('ERROR: ' . $this->_s3Model['jobId'], true));
							//$this->_contentModel->sns = 3;
							$this->_contentModel->conversion_status = App7020Assets::CONVERSION_STATUS_SNS_ERROR;
							$this->_contentModel->save(false);

							break;
						case 'WARNING':
							Yii::log(print_r('WARNING: ' . $this->_s3Model['jobId'], true));
							//$this->_contentModel->sns = 3;
							$this->_contentModel->conversion_status = App7020Assets::CONVERSION_STATUS_SNS_WARNING;
							$this->_contentModel->save(false);

							break;
						default:
							Yii::app()->end();
							break;
					}
					$url = VideoConverterST::getSnsUrl(Yii::app()->request->getParam("marker", false));
					AmazonSnsHelper::unsubscribeUrlFromTopic($this->_storage->getS3Key(), $this->_storage->getS3Secret(), $this->_storage->getS3Region(), $url, $this->_video_convert->snsTopicCompleted);
					AmazonSnsHelper::unsubscribeUrlFromTopic($this->_storage->getS3Key(), $this->_storage->getS3Secret(), $this->_storage->getS3Region(), $url, $this->_video_convert->snsTopicError);
				} elseif (!($this->_contentModel instanceof App7020Assets)) {
					$url = VideoConverterST::getSnsUrl(Yii::app()->request->getParam("marker", false));
					AmazonSnsHelper::unsubscribeUrlFromTopic($this->_storage->getS3Key(), $this->_storage->getS3Secret(), $this->_storage->getS3Region(), $url, $this->_video_convert->snsTopicCompleted);
					AmazonSnsHelper::unsubscribeUrlFromTopic($this->_storage->getS3Key(), $this->_storage->getS3Secret(), $this->_storage->getS3Region(), $url, $this->_video_convert->snsTopicError);
					Yii::app()->end();
				} else {
					Yii::app()->end();
				}
				break;
		}
		// Unsubscribe from SNS

		Yii::app()->end();
	}

	/**
	 * Confirm SNS topic to AWS
	 * @return array|boolean
	 */
	private function _snsConfirm() {
		try {
			// Get storage; it will give us the S3 credentials and region
			$subscriptionResult = AmazonSnsHelper::confirmSubscription($this->_storage->getS3Key(), $this->_storage->getS3Secret(), $this->_storage->getS3Region(), $this->_request['Token'], $this->_request['TopicArn']);
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * return model by thier filename
	 * @return boolean|App7020Content
	 */
	private function _getContentModel() {
		try {

			$criteria = (array(
				'condition' => 'filename=:filename',
				'params' => array(':filename' => $this->_inputPrefix),
			));
			$this->_contentModel = App7020Assets::model()->find($criteria);
			return $this->_contentModel;
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			Yii::log($$this->_contentModel, CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Return file name by SNS marker param
	 * @return string
	 */
	private function _getInputFileName() {
		$filename = Yii::app()->request->getParam("marker", false);
		if (!$filename) {
			return Yii::app()->end();
		} else {
			return $filename;
		}
	}

	/**
	 * Read request returned by Amazon service
	 * @return array
	 */
	private function _readRequest() {
		// Get data from the request payload
		$payload = file_get_contents('php://input');
		return CJSON::decode($payload);
	}

	/**
	 * Make headers's keys and values LOWER case.
	 * also save it in prop 
	 * 
	 * @return array|boolean
	 */
	private function _readHeaders() {
		try {
			// Make headers's keys and values LOWER case.
			$headersOriginal = getallheaders();
			$headers = array();
			foreach ($headersOriginal as $key => $value) {
				$headers[strtolower($key)] = $value;
			}
			return $headers;
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Valide and attach current sns type
	 * @return boolean
	 */
	private function _valideResponse() {
		if (isset($this->_s3Headers['x-amz-sns-message-type'])) {
			$this->_snsType = $this->_s3Headers['x-amz-sns-message-type'];
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Convert JSON Message form AWS to array
	 * @return array
	 */
	private function _proccessAWSResponse() {
		return CJSON::decode($this->_request['Message']);
	}

	/**
	 * Return all created thumbnails, if not exist or prefix is wrong will return blank array
	 * @param $prefix Relative path to working files
	 * @return array
	 */
	private function _getThumbnails($prefix) {
		$allThumbs = $this->_storage->findFiles($prefix);
		//get only 4 thumnails which are equidistant from each other
		return App7020Helpers::reduceArray($allThumbs, 4);
	}

	/**
	 * NOTE: USE AFTER SUCCESSFULL RESPONSE FROM AWS
	 * Return working relative prefix
	 * 
	 * @string
	 */
	private function _getWorkingFolder() {
		$contentType = App7020Assets::$contentTypes[App7020Assets::getContentCodeByExt(App7020Helpers::getExtentionByFilename($this->_inputPrefix))]['outputPrefix'];
		return CS3StorageSt::CONTRIBUTE_FOLDER . $contentType . "/" . basename($this->_s3Model['outputKeyPrefix'] . '/');
	}

}
