<?php

class ActivityController extends App7020BaseController {

	public function init() {
		parent::init();
		$amdController = array();
		$amdController['data']['data-main'] = $this->getAssetsUrl() . '/js/app7020Activity/main';
		Yii::app()->event->raise('registerAMDScript', new DEvent($this, $amdController));
	}

	/**
	 *
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl'
		);
	}

	/**
	 *
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		$res = array();
		$res[] = array(
			'allow',
			'users' => array(
				'*'
			),
			'actions' => array(
				'login'
			)
		);
		$res[] = array(
			'allow',
			'users' => array(
				'@'
			),
			'actions' => array(
				'logout',
				'index'
			)
		);
		return $res;
	}

	public function actionMyCoachActivity() {

		$idExpert = App7020Experts::model()->findByAttributes(array('idUser' => Yii::app()->user->idst));
		$expertChannels = App7020ChannelExperts::model()->getExpertsChannels($idExpert->id, array());
		$data["channels"] = $expertChannels->rawData;
		$this->breadcrumbs = array(Yii::t('app7020', 'My Coaching Activities') => Docebo::createApp7020Url('guruDashboard/myActivity'));
		$this->render('app7020.protected.views.activity.my_coach_activity', $data);
	}

	public function actionMyShareActivity() {
		$idExpert = App7020Experts::model()->findByAttributes(array('idUser' => Yii::app()->user->idst));
		$expertChannels = App7020ChannelExperts::model()->getExpertsChannels($idExpert->id, array());
		$data["channels"] = $expertChannels->rawData;
		$this->breadcrumbs = array(Yii::t('app7020', 'My Sharing Activities') => Docebo::createApp7020Url('guruDashboard/myActivity'));

		$data["mostVotedList"] = array(
			'listParams' => array(
				'listId' => "mostVotedComboListView",
				'dataProvider' => App7020SharingStatistics::getMostVottedAsset(null, true)
			)
		);
		$data["mostWatchedList"] = array(
			'listParams' => array(
				'listId' => "mostWatchedComboListView",
				'dataProvider' => App7020SharingStatistics::getMostWatchedAsset(null, true)
			)
		);
		$data["mostSharedList"] = array(
			'listParams' => array(
				'listId' => "mostSharedComboListView",
				'dataProvider' => App7020SharingStatistics::getMostSharedAsset(null, true)
			)
		);

		if (Yii::app()->request->getParam('ajax', false) === 'mostVotedComboListView') {
			$this->widget('common.widgets.app7020MyMostAssetsListView', $data["mostVotedList"]);
			Yii::app()->end();
		}
		if (Yii::app()->request->getParam('ajax', false) === 'mostWatchedComboListView') {
			$this->widget('common.widgets.app7020MyMostAssetsListView', $data["mostWatchedList"]);
			Yii::app()->end();
		}
		if (Yii::app()->request->getParam('ajax', false) === 'mostSharedComboListView') {
			$this->widget('common.widgets.app7020MyMostAssetsListView', $data["mostSharedList"]);
			Yii::app()->end();
		}

		$this->render('my_share_activity', $data);
	}

	public function actionAxGetCoachActivity() {
		$currentUser = Yii::app()->user->idst;
		$timeframe = (int) Yii::app()->request->getParam('timeframe', false);
		$statistic = new App7020QuestionAnswerStatistic();
		$ca = $statistic->getAllCoachActivity($timeframe, $currentUser, true);
		die(json_encode($ca));
	}

	public function actionAxGetShareActivity() {
		$timeframe = (int) Yii::app()->request->getParam('timeframe', false);
		die(json_encode(true));
	}

}
