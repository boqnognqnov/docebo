<?php

/**
 * Extending the base websocket client for App7020Assets management. 
 * 
 * @author Ivaylo Ivanov
 */
class App7020ContentWSClient extends DoceboWebSocketClient {

	protected $connectionType = 'reporter';
	protected $lmsId;
	protected $object = 'content';
	protected $id;
	protected $data;




	public function __construct() {
		$this->lmsId = Docebo::getErpInstallationId();
		parent::__construct();
	}

    /**
     * 
     * @param type $contentId
     */
	public function sendChangedStatus($contentId){
		$contentModel = App7020Assets::model()->findByPk($contentId);
		if($contentModel->id){
			$this->data =  array('status' => $contentModel->conversion_status);
			$this->id = $contentId;
			$this->_report();
		}
	}
	/**
     * 
     */
	private function _report() {
		$payload = json_encode(array(
				'connectionType' => $this->connectionType,
				'lmsId' => $this->lmsId,
				'object' => $this->object,
				'id' => $this->id,
				'data' => $this->data
					));
			
		$this->sendData($payload);
	}

}
