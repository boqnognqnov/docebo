<?php

/**
 * VideoConverterST
 * This class proccess all data for transcode in  HLS and generate playlist with thumbnails.
 *
 *
 * @author Stanimir Simeonov
 */
use Aws\ElasticTranscoder\ElasticTranscoderClient,
    Aws\Sns\SnsClient;

class VideoConverterST extends VideoConverter {

    /**
     * Store all Job configurations
     * @var array 
     */
    private $_job_config = array();

    /**
     * Working pipline
     * @var string 
     */
    private $_pipeline;

    /**
     * Working presetns for transcode
     * @var string 
     */
    private $_presets;

    /**
     * Segment duration in transcoding
     * @var int|float
     */
    private $_segment_duration = 5;

    /**
     * Store all proccessed 
     * @var array 
     */
    private $_playlist_keys = array();

    public function __construct($storage, $params = array()) {
        parent::__construct($storage, $params);

        try {
            $this->setPipeline($this->storage->serviceParams['app7020']['transcode_pipeline_id']);
            $this->setPresets($this->storage->serviceParams['app7020']['transcode_preset_id']);
        } catch (CException $ex) {
            throw new CException($ex->getMessage());
        }
    }

    /**
     * Initial prepare of data and states
     * @return \VideoConverterST
     */
    function init() {
        parent::init();
        //owerwrite with our pipline for app7002
        $pipeline = $this->etClient->readPipeline(array('Id' => $this->storage->serviceParams['app7020']['transcode_pipeline_id']));
        $this->snsTopicCompleted = isset($pipeline['Pipeline']['Notifications']['Completed']) ? $pipeline['Pipeline']['Notifications']['Completed'] : "";
        $this->snsTopicError = isset($pipeline['Pipeline']['Notifications']['Error']) ? $pipeline['Pipeline']['Notifications']['Error'] : "";
        return $this;
    }

    /**
     * Start Elastic service and proccessing data
     * @param string $pipelineId
     * @throws CException
     */
    protected function runElasticTranscoderJob($pipelineId = false) {
        if ($pipelineId) {
            $this->setPipeline($pipelineId);
        }
        $this->setJobConfig('Input', array('Key' => ltrim($this->params['inputKey'], "/"))); // we dont neet root slash, this is reason for remove it if exist
        $this->setS3Output($this->getOuputTrascodePrefix('h'), $this->getPreset('h_quality'), $this->getOuputTrascodePrefix());
        $this->setS3Output($this->getOuputTrascodePrefix('m'), $this->getPreset('n_quality'));
        $this->setS3Output($this->getOuputTrascodePrefix('l'), $this->getPreset('l_quality'));
        $this->setS3Output($this->getOuputTrascodePrefix(), $this->getPreset('general'), '', false);
        $this->setJobConfig('OutputKeyPrefix', $this->getOutputKeyPrefix());
        $this->setS3Playlist('HLSv3');
        $result1 = AmazonSnsHelper::subscribeUrlToTopic($this->storage->getS3Key(), $this->storage->getS3Secret(), $this->storage->getS3Region(), self::getSnsUrl(basename($this->params['outputKey'])), $this->snsTopicCompleted);
        $result2 = AmazonSnsHelper::subscribeUrlToTopic($this->storage->getS3Key(), $this->storage->getS3Secret(), $this->storage->getS3Region(), self::getSnsUrl(basename($this->params['outputKey'])), $this->snsTopicError);

        if (!$result1 || !$result2) {
            throw new CException(Yii::t('app7020', 'Error while trying to subscribe an endpoint to Amazon SNS topic.'));
        }

        try {
            // Create the job and get response
            $job = $this->etClient->createJob($this->_job_config);
            if ($job instanceof Guzzle\Service\Resource\Model) {
                if ($job['Job']['Status'] != 'Error') {
                    Yii::log('-------- Elastic Transcoder Job successfully submitted. Pipeline Id: ' . $job['Job']['PipelineId'] . ', Job Id: ' . $job['Job']['Id'] . ' KEY: ' . $this->params['inputKey'], CLogger::LEVEL_INFO);
                    return true;
                } else {
                    Yii::log("Elastic Transcoder Job returned status 'ERROR'.", CLogger::LEVEL_INFO);
                    throw new CException("Elastic Transcoder Job returned status 'ERROR'.");
                }
            } else {
                throw new CException("Invalid response object from S3 storage (Guzzle\Service\Resource\Model expected)");
            }
        } catch (CException $ex) {
            throw new CException($ex->getMessage());
        }
    }

    /**
     * Get sns URL for answer on Finish or Error state.
     * @param string $marker
     * @return type
     */
    public static function getSnsUrl($marker) {
        $url = Docebo::createAbsoluteApp7020Url("app7020AmazonSns/transcoding", array('marker' => $marker));
        if (YII_DEBUG && preg_match('/(.*)(lms.+)\.(.+)\.scavaline\.(\w{2,4})\/(.*)$/i', $url, $debugUrl)) {
            $port = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020)->serviceParams['app7020']['debug_port'];
            $url = $debugUrl[1] . $debugUrl[2] . '.' . $debugUrl[3] . '.scavaline.' . $debugUrl[4] . ':' . $port . '/' . $debugUrl[5];
            return $url;
        } elseif (YII_DEBUG && preg_match('/(.*)(lms.+)\.scavaline\.(\w{2,4})\/(.*)$/i', $url, $debugUrl)) {
            $port = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020)->serviceParams['app7020']['debug_port'];
            $url = $debugUrl[1] . $debugUrl[2] . '.scavaline.' . $debugUrl[3] . ':' . $port . '/' . $debugUrl[4];
            return $url;
        }
        return $url;
    }

    /**
     * Define and attach output 
     * @param string $keyPrefix name on transcoded file
     * @param string $presetId chosen preset workflow
     * @param string $thumbnail name on thumbnails(empty for none)
     * @return type
     */
    function setS3Output($keyPrefix, $presetId, $thumbnail = '', $inPlaylist = true) {
        $ext = '';
        if ($inPlaylist == true) {
            $this->setPlaylistKeys($keyPrefix, true);
        } else {
            $ext .= '.mp4';
        }

        $args['Key'] = $keyPrefix . $ext;
        $args['ThumbnailPattern'] = !empty($thumbnail) ? 'thumb/' . $thumbnail . '-{count}' : $thumbnail;
        $args['PresetId'] = $presetId;
        if ($inPlaylist) {
            $args['SegmentDuration'] = $this->getSegmentDuration();
        }

        return $this->setJobConfig('Outputs', $args, true);
    }

    /**
     * Set type of playlists
     * @example $type HLSv3, HLSv4, and Smooth.
     * @return type
     */
    function setS3Playlist($type = false) {
        $needed = array('HLSv3', 'HLSv4', 'Smooth');
        if (empty($type) || !in_array($type, $needed)) {
            $type = 'HLSv4'; //reseting if not exist in allowed types
        }
        $args = array(
            'Name' => $this->getOuputTrascodePrefix(),
            'Format' => $type,
            'OutputKeys' => $this->getPlaylistKeys()
        );

        return $this->setJobConfig('Playlists', $args, true);
    }

    /**
     * Return Output prefix(destination for transcode videos)
     * @return string
     */
    function getOutputKeyPrefix() {
 
        return ltrim(dirname($this->params['outputKey']) . '/' . pathinfo($this->params['inputKey'], PATHINFO_FILENAME) . '/', "/"); //we must to finish with slash 
    }

    /**
     * Get Prefix used for transcoded file names 
     * @param string $quality
     * @return string
     */
    function getOuputTrascodePrefix($quality = false) {
 
        if ($quality) {
              return  pathinfo($this->params['inputKey'], PATHINFO_FILENAME). '_' . $quality . '_';
        } else {
             return pathinfo($this->params['inputKey'], PATHINFO_FILENAME);
        }
    }

    /**
     * Get Generated job config
     * @return array
     */
    public function getJobConfig() {
        return $this->_job_config;
    }

    /**
     * Get used Pipe
     * @return string
     */
    public function getPipeline() {
        return $this->_pipeline;
    }

    /**
     * Get preset for current job
     * @param type $key
     * @return string|boolean
     */
    public function getPreset($key) {
        return isset($this->_presets[$key]) ? $this->_presets[$key] : false;
    }

    /**
     * Set current segment duration for work
     * @return float|int
     */
    public function getSegmentDuration() {
        return $this->_segment_duration;
    }

    /**
     * Get all keys stored for playlist
     * @return array
     */
    public function getPlaylistKeys() {
        return $this->_playlist_keys;
    }

    /**
     * Set config for operation
     * @param string $workflow this is the key on array 
     * @param array $job_config settings for chosen operation
     * @return \VideoConverterST
     */
    public function setJobConfig($workflow, $job_config, $append = false) {
        if ($append) {
            $this->_job_config[$workflow][] = $job_config;
        } else {
            $this->_job_config[$workflow] = $job_config;
        }
        return $this;
    }

    /**
     * set used pipe in class
     * @param string $pipe
     * @return \VideoConverterST
     */
    public function setPipeline($pipe) {
        $this->_pipeline = $pipe;
        $this->setJobConfig('PipelineId', $pipe);
        return $this;
    }

    /**
     * Set all pipes for work
     * @param arrray $preset
     * @return \VideoConverterST
     */
    public function setPresets($preset) {
        $this->_presets = $preset;
        return $this;
    }

    /**
     * Set video Segment Duration
     * @param int|float $segment_duration
     * @return \VideoConverterST
     */
    public function setSegmentDuration($segment_duration) {
        $this->_segment_duration = $segment_duration;
        return $this;
    }

    /**
     * Set or append playlist 
     * @param string|array $playlist_keys
     * @return \VideoConverterST
     */
    public function setPlaylistKeys($playlist_keys, $append = false) {
        if ($append) {
            $this->_playlist_keys[] = $playlist_keys;
        } else {
            $this->_playlist_keys = $playlist_keys;
        }
        return $this;
    }

    function __destruct() {
        unset($this);
    }

}
