<?php

/**
 * CS3StorageST
 * This class  implement all feature for uplaoding and transcode the video on S3
 * Implement JOBS requets for 
 *  - Video Quality of 3 stream types
 *  - Video Thumbnails 
 *  - Change resolutions on thumbnails and crop in dimensions needed for app7020
 * 
 * 
 * @author Stanimir Simeonov
 */
use Aws\S3\Sync\UploadSyncBuilder;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Guzzle\Service\Exception\CommandTransferException;
use Guzzle\Http\EntityBody;
use Guzzle\Http\Url;

class CS3StorageSt extends CS3Storage {

    /**
     * filename with abstract path for uplaod/copy
     * @var string
     */
    private $_key;

    /**
     * permissions for manipulationss
     * @var string
     */
    private $_acl;

    /**
     * Working file AWS hendler
     * @var EntityBody
     */
    private $_body;

    /**
     * List with params for working proccess
     * @var array
     */
    private $_params;

    /**
     * Set custom folder if is preset
     * @var string | bool
     */
    private $_customSubFolder = false;

    /**
     * Set convention folder
     * @var string
     */
    const CONTRIBUTE_FOLDER = "";

    /**
     * All egines types needed for video player and content types assigned to them
     */
    static $ENGINES = array(
        'flash' => array(
            'type' => 'application/x-mpegurl',
            'ext' => '.m3u8',
        ),
        'html5' => array(
            'type' =>
                array(
                    'video' => 'video/mp4',
                    'audio' => 'audio/*'
                ),
            'ext' => array(
                'video' => '.mp4',

            )
        ),
    );

    /**
     * make instance for storage with needed params
     * @param string $collection
     * @param array $params
     */
    public function __construct($collection = self::COLLECTION_NONE, $params = array()) {
        parent::__construct($collection, $params);
    }

    /**
     * (non-PHPdoc)
     * @see IFileStorage::storeAs()
     *
     * @return boolean|string   True, False OR the KEY to the file inside the bucket that just has been deployed/stored
     */
    public function storeAs($sourcePath, $filename, $customSubfolder = "", $params = array()) {
        //prepare of data
        $this->setParams($params);
        $this->setAcl();
        if (!empty($customSubfolder)) {
            $this->setCustomSubFolder($customSubfolder);
        }

        // If the source path is a directory, use special method
        if (is_dir($sourcePath)) {
            return $this->uploadFolder($sourcePath, $filename, $customSubfolder, $this->getAcl(), $this->getParams());
        }

        //detect exeptions when open the file
        try {
            //AWS Entity Handler

            $this->setBody(EntityBody::factory(fopen($sourcePath, 'r')));
        } catch (Exception $ex) {
            throw new CException("Unable to open local file $sourcePath");
        }

        try {
            // In case this is a VIDEO From App7020  **AND** Video transcoding/conversion is enabled, use the "Video Converting" process
            $fileValidator = new FileTypeValidator(FileTypeValidator::TYPE_VIDEO_EXTENDED_LIST);
            $flag = ($this->collection == CFileStorage::COLLECTION_7020 && $fileValidator->validateLocalFile($filename) && $this->transcodeVideo);
            //proccess key
            $this->setKey($filename);
            return $this->_uploadToS3($flag);
        } catch (CException $ex) {
            throw new CException($ex->getMessage());
        }
    }

    /**
     * Start video converting proccess
     * @param string $inKey
     * @param string|boolean $outKey is not required for this method
     */
    function convertVideo($inKey, $outKey = false) {

        $vc = new VideoConverterST($this, array(
            'inputKey' => $inKey,
            'outputKey' => $this->getOutputKey($inKey)
        ));

        $result = $vc->runTranscoding();
        return $result;
    }

    /**
     * Return Output key(output destionation)
     * @param string $key
     * @return string
     */
    function getOutputKey($key = false) {
        if (!empty($key)) {
            $filename = basename($key);
        } else {
            $filename = basename($this->getKey());
        }
        $conventionFolder = (self::getConventionFolder($this->getKey())) ? "/" . self::getConventionFolder($this->getKey()) : "";
        return $this->absoluteBasePath . ($this->subFolder ? "/" . $this->subFolder : "") . ($this->getCustomSubFolder() ? "/" . $this->getCustomSubFolder() : "") . $conventionFolder . "/" . $filename;
    }

    /**
     * Encode S3 key into UTF in case 'bad' Unicode characters are used in filenames (well, they are NOT bad, just not UTF8).
     * Example: Unicode(ó)=F3, but UTF8(ó)=C3B3. If F3 is left in the key, S3 will not be happy and return InvalidUriException
     * @param string $key
     * @retrurn string On output will have normalized filename
     */
    private function _normalizeUTF($key) {
        if (strtolower(mb_detect_encoding($key, mb_detect_order(), true)) != 'utf-8') {
            $key = utf8_encode($key);
        }
        return $key;
    }

    /**
     * Upload to S3 local file
     * @return boolean
     * @throws CException
     */
    private function _uploadToS3($isVideo = false) {
        try {
            $resModel = $this->getClient()->upload($this->bucket_name, $isVideo ? $this->getKey() : $this->getOutputKey(), $this->getBody(), $this->getAcl(), $this->getParams());

            if ($isVideo && $resModel instanceof Guzzle\Service\Resource\Model) {
                $this->convertVideo($this->getKey());
                return true;
            } else if (!$isVideo && $resModel instanceof Guzzle\Service\Resource\Model) {
                return true;
            } else {
                throw new CException("Invalid response object from S3 storage (Guzzle\Service\Resource\Model expected)");
            }
        } catch (CException $ex) {
            throw new CException($ex->getMessage());
        }
    }

    /**
     * Get AWS file key
     * @return string
     */
    public function getKey() {
        return $this->_key;
    }

    /**
     * Get permisions for operation
     * @return string
     */
    public function getAcl() {
        return $this->_acl;
    }

    /**
     * Get proccessed AWS body hendler
     * @return EntityBody
     */
    public function getBody() {
        return $this->_body;
    }

    /**
     * Get current params
     * @return array|string
     */
    public function getParams($keyArray = false) {
        if (!empty($keyArray) && isset($this->_params[$keyArray])) {
            return $this->_params[$keyArray];
        } else {
            return $this->_params;
        }
    }

    /**
     * Get Custom preset custom folder
     * @return string
     */
    public function getCustomSubFolder() {
        return $this->_customSubFolder;
    }

    /**
     * Check for assigned param for proccessing
     * @param string $string
     * @return boolean
     */
    function isAssignedParam($string = '') {
        if (!empty($string) && isset($this->_params[$string])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get Video In "KEY"(AWS Path to file)
     * We upload the file to a special temporary folder into the object storage
     * where it will be taken from and converted by the Elastic transcoding a bit later.
     * @param string $filename reanemed(hashed) video name
     * @return \CS3StorageSt
     */
    public function setKey($filename) {
        //prededefined var
        $key = '';
        if (!empty($this->filesBaseRelPath)) {
            $key .= '/' . $this->filesBaseRelPath;
        }
        $key .= "/" . $this->transcodeVideoIncomingFolder;
        $key .= "/" . $filename;
        $this->_key = $this->_normalizeUTF($key);
        return $this;
    }

    /**
     * Set ACL for  usage in later proccess
     * @param string $acl
     * @return \CS3StorageSt
     */
    public function setAcl($acl = false) {
        if (!empty($acl)) {
            $this->_acl = $acl;
        } else {
            if ($this->isAssignedParam('ACL')) {
                $this->_acl = $this->getParams('ACL');
            } else {
                $this->_acl = $this->defaultAcl;
            }
        }
        return $this;
    }

    /**
     * Set handler for working file EntityBody
     *
     * @param EntityBody $body
     * @return \CS3StorageSt
     */
    public function setBody(EntityBody $body) {
        $this->_body = $body;
        return $this;
    }

    /**
     * Set params for work
     * @param array $params
     * @return \CS3StorageSt
     */
    public function setParams($params) {
        $this->_params = $params;
        return $this;
    }

    /**
     * Set filename on s3 proccessed file
     * @param string $filename
     * @return \CS3StorageSt
     */
    public function setFilename($filename) {
        $this->_filename = $filename;
        return $this;
    }

    /**
     * Set custom subfolder if is preset
     * @param string $customSubFolder
     * @return \CS3StorageSt
     */
    public function setCustomSubFolder($customSubFolder) {
        $this->_customSubFolder = $customSubFolder;
        return $this;
    }

    /**
     * Set convention folder if is preset
     * @param string $conventionFolder
     * @return \CS3StorageSt
     */
    public function setConventionFolder($conventionFolder) {
        $this->_conventionFolder = $conventionFolder;
        return $this;
    }

    /**
     * Construct convention folder
     * @return string
     */
    public static function getConventionFolder($filename) {
		$ctype = App7020Assets::getContentCodeByExt(App7020Helpers::getExtentionByFilename(basename($filename)));
        $contentType = App7020Assets::$contentTypes[$ctype]['outputPrefix'];
        return (!$contentType) ? false : self::CONTRIBUTE_FOLDER . $contentType;
    }

    /**
     * Get relative path to S3 file in collection
     * @param string $filename
     * @return string
     */
    public static function relativePathByAbs($filename) {
        $instance = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);
        $removePart = ltrim($instance->path(), '/');
        return ltrim(str_replace($removePart, '', $filename), '/');
    }

    public static function getPlayerSources($filename, $storage = false) {
        if (!$storage) {
            $storage = CS3Storage::getDomainStorage(self::COLLECTION_7020);
        }
        $ext = App7020Helpers::getExtentionByFilename($filename);
        if(App7020Helpers::isAudioFile($ext)) {
            $array = array(
                'audio' => $storage->fileUrl($storage->getConventionFolder($filename).'/'.$filename )
             );
        }
        else {
            $mediaUrlMp4 = $storage->fileUrl(self::getRelativPath($filename, $storage) . self::$ENGINES['html5']['ext']['video']);
            $mediaUrlHlsPlaylist = $storage->fileUrl(self::getRelativPath($filename, $storage) . self::$ENGINES['flash']['ext']);
            $array = array(
                'mediaUrlMp4' => $mediaUrlMp4,
                'mediaUrlHlsPlaylist' => $mediaUrlHlsPlaylist,
            );
        }

        return $array;
    }

    /**
     * Return relative path to files without any ext.
     * @param CS3StorageSt $storage
     * @return string
     */
    public static function getRelativPath($filename, $storage = false) {
        if (!$storage) {
            $storage = CS3Storage::getDomainStorage(self::COLLECTION_7020);
        }
        return $storage->getConventionFolder($filename) . '/' . App7020Helpers::getAmazonInputKey($filename) . '/' . App7020Helpers::getAmazonInputKey($filename);
    }

    /**
     * Return file folder(output destionation)
     * @param string $key
     * @return string
     */
    function getOutputFolder($filename, $startSlash = false, $withFileName = false) {
        $this->setKey($filename);
        $conventionFolder = (self::getConventionFolder($this->getKey())) ? "/" . self::getConventionFolder($this->getKey()) : "";
        $folder = $this->absoluteBasePath . ($this->subFolder ? "/" . $this->subFolder : "") . ($this->getCustomSubFolder() ? "/" . $this->getCustomSubFolder() : "") . $conventionFolder . "/";
        if (!$startSlash) {
            $folder = ltrim($folder, "/");
        }
        if ($withFileName) {
            $folder = $folder . $filename;
        }
        return $folder;
    }
	
	

}
