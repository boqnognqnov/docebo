<?php

/**
 * 
 *
 */
class App7020BaseController extends Controller {

	/**
	 * List of routes (controller/action) which is considered as administrative in App7020 
	 * @var array
	 */
	protected $adminRoutes = array();

	/**
	 * Assets url
	 * @var string
	 */
	protected $_assetsUrl;

	/**
	 * Store already loaded CApplication instance on getClientScript()
	 * @var CApplication 
	 */
	protected $_client_script;

	/**
	 * @see CController::init()
	 */
	public function homeRedirect($rule = false) {
		parent::accessDeniedCallback($rule);
	}

	public function accessRules() {
		die('accessRules');
		$res = array();


		$res[] = array(
			'deny',
			// deny all
			'users' => array('*'),
			'deniedCallback' => array($this, 'homeRedirect'),
		);

		return $res;
	}


	public function init() {

		if (Yii::app()->user->isGuest) {
			if (Yii::app()->request->isAjaxRequest && !headers_sent()) {

				// Send custom header.
				header('X-Ajax-Redirect-Url: ' . 'index.php?r=site/index');

				if ( Yii::app()->user->getIsGuest() ) {
					$message = Yii::t( 'standard', 'Your session is expired. Redirecting ... ' );
				} else {
					if ( ! empty( $message ) ) {
						Yii::app()->user->setFlash( 'error', $message );
					}
				}


				// Try to determine the expected content type and echo empty JSON or just a text
				echo (stripos(Yii::app()->request->acceptTypes, 'json') === false) ? $message : CJSON::encode(array('html' => $message));
				Yii::app()->end();
			}
		}

		parent::init();

		JsTrans::addCategories("app7020");


		//prepare new eveent
		$helper = new App7020Helpers();
		Yii::app()->event->on('registerAMDScript', array($helper, 'registerAMDScript')); //used for invoke new script dinamicly from everywhere
		$helper::loadTimelineCss();

		// When Session time is finished, user must be redirected to Login page
		if (Yii::app()->user->isGuest) {
 			$this->redirect(Docebo::createAppUrl('lms:site/index'));
		}
		$this->_client_script = Yii::app()->getClientScript();
		$themeUrl = Yii::app()->theme->baseUrl;
		App7020Helpers::registerApp7020Options();
		$token = Yii::app()->request->csrfToken;
		$this->getClientScript()->registerScript('$fcbkAutocompleteUrl', 'window.$fcbkAutocompleteUrl = "' . Docebo::createApp7020Url('axAssets/axGetAutoCompleteTags') . '";');
		$this->getClientScript()->registerScript('csrfTokenAngular', 'window.YII_CSRF_TOKEN = "' . $token . '";');
		$this->getClientScript()->registerCssFile($this->getAssetsUrl() . '/css/app7020.css');
		$this->getClientScript()->registerCssFile($themeUrl . '/css/bootstrap4-grid.css');
		$this->getClientScript()->registerScriptFile($this->getAssetsUrl() . '/js/app7020.js', CClientScript::POS_END);
		$this->getClientScript()->registerScriptFile($themeUrl . '/js/jquery.cookie.js');

	}

	/**
	 * @see Controller::resolveLayout()
	 */
	public function resolveLayout() {
		$this->layout = "app7020";
	}

	/**
	 * @see CController::beforeAction()
	 */
	public function beforeAction($action) {
		return parent::beforeAction($action);
	}

	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl() {
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = App7020Helpers::getAssetsUrl();
		}
		return $this->_assetsUrl;
	}

	/**
	 * Already loaded Cliet Script
	 * @see Yii::app()->getClientScript()
	 */
	function getClientScript() {
		return $this->_client_script;
	}


}
