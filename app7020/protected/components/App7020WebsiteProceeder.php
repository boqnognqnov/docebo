<?php

/**
 *  
 *
 * @author Ivaylo Ivanov
 */
class App7020WebsiteProceeder extends CComponent {

	public static function saveWebsiteInitData($idContent) {

		$contentObject = App7020Assets::model()->findByPk($idContent);
		$httpObj = new HttpRequest($contentObject->originalFilename);
		$elements = $httpObj->getMainElements();
		$contentObject->title = $elements['title'];
		$contentObject->description = $elements['description'];
		if ($elements['image'] = App7020Helpers::isValidUrl($elements['image'])) {
			$ext = pathinfo($elements['image'], PATHINFO_EXTENSION);
			$contentObject->filename = $contentObject->filename . '.' . $ext;

			$ogImageContent = file_get_contents($elements['image']);
			$newFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $contentObject->filename;
			file_put_contents($newFilePath, $ogImageContent);

			$coreAsset = new CoreAsset();
			$coreAsset->type = CoreAsset::TYPE_APP7020;
			$coreAsset->sourceFile = $newFilePath;
			$coreAsset->save();

			$contentObject->idThumbnail = $coreAsset->id;
			$contentObject->conversion_status = App7020Assets::CONVERSION_STATUS_SNS_SUCCESS;
			$contentObject->save(false);
		} else {

			$contentObject->filename = $contentObject->filename . '.png';
			$contentObject->save(false);
			$convertObject = new CloudConvert();
			$convertObject->convertWebsite($contentObject->id);
		}
	}

	public static function isYoutubeLink($link) {
		$youtubePattern = "/(youtube.com|youtu.be)\/(watch)?(\?v=)?([a-zA-Z0-9\+\-\_]+)?/";
		return preg_match($youtubePattern, $link, $match) ? $match[4] : false;
	}

	public static function isVimeoLink($link) {
		$vimeoPattern = "/(?:https?:\/\/)?(?:www\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/";
		return preg_match($vimeoPattern, $link, $match) ? $match[3] : false;
	}

	/**
	 * Check if is Wistia's url
	 * @param str $url
	 * @param str $noImg
	 * @return array:
	 * 	1) if not Wistia's url - empty array
	 * 	2) if is. It checks if video exists:
	 * 		2.1) If exists - returns all info provided by Wistia's API
	 * 		2.2) If not - error = true and path to "no img". Example: ['error'=>true, 'errorImg'=> 'https://placeholdit.imgix.net/~text?txtsize=33&txt=No%20Video&w=350&h=200']
	 */
	public static function isWistiaLink($url, $noImg = 'https://placeholdit.imgix.net/~text?txtsize=100&txt=Deleted+video&w=1160&h=650', $onlyValidation = false) {
		// Check if is Wistia's url
		$isWistia = preg_match('/https?:\/\/(.+)?(wistia\.com|wi\.st)\/.*/', $url, $match);
		if ($isWistia) {
			// Get video hashed id
			$parsedUrl = parse_url($url);
			$exploadedPath = explode('/', $parsedUrl['path']);
			$hashedId = $exploadedPath[2];
			// Make cURL
			$curlUrl = 'http://fast.wistia.net/oembed?url=https%3A%2F%2F' . $match[1] . 'wistia.com%2Fmedias%2F' . $hashedId . '&embedType=async';
			$curlResult = App7020Helpers::curl($curlUrl);

			if (!$curlResult['errno'] && $curlResult['http_code'] == 200) {
//				$result = json_decode($curlResult['content']);
				$result['hashedId'] = $hashedId;
			} else {
				$result['error'] = true;
				$result['errorImg'] = $noImg;
			}
		} else {
			$result = array();
		}

		return $result;
	}

	/**
	 * 
	 * @param type $url
	 * @return type
	 */
	public static function isValidWistiaLink($url) {
		// Check if is Wistia's url
		return preg_match('/(.+)\:\/\/(.*)\.(wistia)\.(com|wi\.st)\/(medias|\w+)(.*)/', $url, $match) ? $match[6] : false;
	}

}
