<?php

/**
 *  
 *
 * @author Ivaylo Ivanov
 */
class App7020VideoProceeder extends CComponent {

    /**
     * Generate a CoreAsset model (database, converted image and so on) out of an exisitng thumb [model]
     *  
     * @param App7020Assets $contentObject
     * @param App7020ContentThumbs $realThumbModel
     * 
     * @return CoreAsset
     */
    public static function generateThumbCoreAsset(App7020Assets $contentObject, App7020ContentThumbs $realThumbModel) {

        // Filepath to local file wher we shall save the thumbnail used to generate a CoreAsset
        $localPath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . App7020Helpers::getAmazonInputKey($contentObject->filename) . ".png";

        // Get storage and relative filepath to the thumbnail stored on S3
        $storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);
        $relativePathToImage = $realThumbModel->getS3RelativePath();

        //Download the thumbnail
        /* @var $storage CS3StorageST */
        $isDownloaded = $storage->downloadAs($relativePathToImage, $localPath);
        Log:_('FILE URL DOWNLOADING' . $storage->fileUrl($relativePathToImage));
        if ($isDownloaded) {

            $coreAsset = new CoreAsset();
            $coreAsset->type = CoreAsset::TYPE_APP7020;
            $coreAsset->sourceFile = $localPath;

            // Save this for later cleanup (on success only)
            $oldThumbnailId = $contentObject->idThumbnail;

            if (!$coreAsset->save()) {
                return false;
            } else {
                // Do some clean up
                FileHelper::removeFile($localPath);
                CoreAsset::model()->deleteAllByAttributes(array(
                    'id' => $oldThumbnailId, // apparently no more needed
                    'type' => CoreAsset::TYPE_APP7020,
                ));
                return $coreAsset;
            }
        } else {
            Yii::log('Failed  download App7020 Image ' . $relativePathToImage . PHP_EOL, CLogger::LEVEL_ERROR);
            return false;
        }
    }

    public static function startVideoConversion($idContent) {
        $contentObject = App7020Assets::model()->findByPk($idContent);
        if ($contentObject->id) {
            /* @var $fileStorageObject CS3StorageST */
            $fileStorageObject = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);
            $fileStorageObject->setKey($contentObject->filename);
            return $fileStorageObject->convertVideo($fileStorageObject->getKey());
        }
        return false;
    }

    /**
     * Return public thumb URL for Video Content by their counter
     * @param int $countImg ex. 000001
     * @param App7020Content $content
     * @return string
     */
    public static function getImageInputUrl($filename, $countImg) {
        $url = CFileStorage::getS3DomainStorage(CFileStorage::COLLECTION_7020)->fileUrl(CS3StorageSt::getConventionFolder($filename) .
                '/' . App7020Helpers::getAmazonInputKey($filename) .
                '/thumb/' .
                App7020Helpers::getAmazonInputKey($filename) .
                '-' . $countImg);
        return $url . '.png';
    }

    /**
     * Return the Label arguments for form
     * @param App7020Content $content
     * @return array
     */
    public static function getAssetThumbs($idContent) {
        $contentObject = App7020Assets::model()->findByPk($idContent);

        $returnArray = array();
        if ($contentObject->id && $contentObject->contentType == App7020Assets::CONTENT_TYPE_VIDEO) {
            foreach ($contentObject->thumbs as $thumbnail) {
                $returnArray[$thumbnail->id] = '<img src="' . self::getImageInputUrl($contentObject->filename, $thumbnail->file) . '"/>';
            }
        }

        return $returnArray;
    }

    /**
     * Return the active thumbnail
     * @param App7020Content $content
     * @return array
     */
    public static function getActiveThumb($idContent) {
        $contentObject = App7020Assets::model()->findByPk($idContent);
        if ($contentObject->id && $contentObject->contentType == App7020Assets::CONTENT_TYPE_VIDEO) {
            foreach ($contentObject->thumbs as $thumbnail) {
                if ($thumbnail->is_active) {
                    return $thumbnail->id;
                }
            }
        }

        return 0;
    }

}
