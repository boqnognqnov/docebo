<?php

/**
 *  
 *
 * @author Ivaylo Ivanov
 */
class App7020ImageProceeder extends CComponent {

	public static function generateCoreAssets($idContent) {
		$contentObject = App7020Assets::model()->findByPk($idContent);
		if(!$contentObject->id){
			return false;
		}
		
		$localPath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $contentObject->filename;
		$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);
		
		$isDownloaded = $storage->downloadAs($contentObject->filename, $localPath, 'images');
		if ($isDownloaded) {
			$coreAsset = new CoreAsset();
			$coreAsset->type = CoreAsset::TYPE_APP7020;
			$coreAsset->sourceFile = $localPath;
			if (!$coreAsset->save()) {
				return false;
			} else {
				FileHelper::removeFile($localPath);
				//delete old image from server
				if ($coreAsset->id != $contentObject->idThumbnail && (int) $contentObject->idThumbnail > 0) {
					CoreAsset::model()->findByPk($contentObject->idThumbnail)->delete();
				}
				return $coreAsset->id;
				
			}
		} else {
			return false;
		}
	}

}
