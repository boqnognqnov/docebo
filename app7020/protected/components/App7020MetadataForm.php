<?php

/**
 * This Class generatge metadata form for 
 * uploading proccess also for editing and review. 
 * Here is all logic for generate 
 *
 * @author Stanimir
 */
class App7020MetadataForm extends CComponent {

	/**
	 * Store Conrent model 
	 * @var App7020Assets 
	 */
	public $content = false;

	/**
	 * Store current status for hide on form 
	 * @var boolean
	 */
	public $isHide = false;

	function __construct(App7020Assets $content, $isHide = true) {
		try {
			$this->content = $content;
			$this->isHide = $isHide;
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
		}
	}

	/**
	 * Get Hide status on form 
	 * @return boolean
	 */
	function getIsHide() {
		return $this->isHide;
	}

	/**
	 * Set new hide status on form 
	 * @param boolean $isHide
	 * @return \App7020MetadataForm
	 */
	function setIsHide($isHide) {
		$this->isHide = $isHide;
		return $this;
	}

	/**
	 * 
	 * @param App7020Assets $content
	 * @return string
	 */
	function init($content = false) {

		if ($content == false) {
			$content = $this->content;
		}
 		return $this->_generateUploadMetaform($content);
	}

	/**
	 * Generate Form per presaved file in DB
	 *
	 * @param App7020Assets $content            
	 * @return string Generated HTML directly for append or native render
	 */
	protected function _generateUploadMetaform(App7020Assets $content) {
		try {
			$view_params = array(
				'isHide'=> (bool)$this->isHide,
				'form_args' => $this->_getFormArguments($content),
				'radio_list_args' => $content->contentType == App7020Assets::CONTENT_TYPE_VIDEO ? $this->_getRadioListArgs($content) : false,
				'labels_args' => $this->_getLabelArguments($content),
				'content' => $content,
			);
			// get views
			return Yii::app()->controller->renderPartial('app7020.protected.views.contribute._metaform_container', $view_params, true);
		} catch (Exception $ex) {
			throw new Exception($ex->getMessage());
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
		}
	}

	/* -------------------------------------------------------------------------
	 * GENERAL WORKING METHODS
	 * ----------------------------------------------------------------------- */

	/**
	 * Return Predefined arry with arguments for CHTML::RadioList()
	 * @param App7020Content $content
	 * @return array
	 */
	protected function _getRadioListArgs(App7020Assets $content) {
		$radio_list_args = array(
			'container' => 'ul class="row-fluid" id="App7020ContentThumbs_id-' . $content->id . '"',
			'separator' => '',
			'template' => '<li class="span6">{input}{label}</li>',
			'baseID' => 'App7020ContentThumbs_id_' . $content->id
		);
		return $radio_list_args;
	}

	/**
	 * Return Active form arguments for creation
	 * @param App7020Content $content
	 * @return array
	 */
	protected function _getFormArguments(App7020Assets $content) {

		$form_args = array(
			'action' => CHtml::normalizeUrl(array('contribute/saveContent', 'id' => $content->id)),
			'method' => 'post',
			'enableAjaxValidation' => true,
			//'enableClientValidation' => true,
			'htmlOptions' => array(
				'id' => 'app7020-upload-contribute-form' . $content->id,
				'class' => 'app7020-metadata-form app7020-metadata-form-' . $content->id
			)
		);
		return $form_args;
	}

	/**
	 * Return the Label arguments for form
	 * @param App7020Content $content
	 * @return array
	 */
	protected function _getLabelArguments(App7020Assets $content) {
		$labels_args = array();
		foreach ($content->thumbs as $th) {
			$labels_args[$th->id] = '<img src="' . $content->getImageInputUrl($th->file) . '"/>';
		}
		return $labels_args;
	}

}
