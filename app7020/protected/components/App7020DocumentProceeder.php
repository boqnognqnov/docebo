<?php

/**
 *  
 *
 * @author Ivaylo Ivanov
 */
class App7020DocumentProceeder extends CComponent {

	public static function startDocumentConversion($idContent){
		$contentObject = App7020Assets::model()->findByPk($idContent);
		if($contentObject->id){
			$cloudConvertObject = new CloudConvert();
			$cloudConvertObject->convertFromS3ToS3($contentObject->id);
			return true;
		}
		return false;
	}
	

}
