<div id="expertSummaryReport">
	<div class="user-report-actions">
		<div id="advanced_search_expert_report_container" class="summary-report-form clearfix">
			<div class="clearfix"></div>
			<span><?= Yii::t('report', 'Select expert'); ?></span>
			<?php
			echo CHtml::textField('Expert name', '', array(
				'id' => 'advanced-search-expert-report',
				'class' => 'ypeahead ajaxGenerate',
				'placeholder' => Yii::t('report', 'Type expert name here'),
				'data-formatted-course-name' => true,
			));
			?>

			<?php
			echo CHtml::link(
					Yii::t('certificate', '_GENERATE'), Docebo::createAppUrl('app7020:expertSummary/index'), array('class' => 'new-user-summary', 'id' => 'generate-expert-report')
			);
			$this->breadcrumbs[] = Yii::t('report', 'Expert summary') . ': ' . Yii::app()->user->getRelativeUsername($userModel->userid);
			?>


			<div class="report-popup-actions">
				<?php
				echo CHtml::link(
						Yii::t('report', 'Print') . '<span></span>', 'javascript:void(0);', array('class' => 'report-action-print')
				);

				echo CHtml::link(
						Yii::t('report', 'Download as PDF') . '<span></span>', Docebo::createAbsoluteUrl('admin:reportManagement/generateExpertReport', array('id' => $userModel->idst, 'createPdf' => true)), array('class' => 'report-action-pdf', 'data-expertId' => $userModel->idst, 'id' => 'expertSummaryPdf')
				);
				?>
			</div>
		</div>

	</div>

	<div class="container-fluid allbordersgrey">
		<div class="row-fluid">
			<div class="span2 avatar big">
				<?php
				$this->widget('common.widgets.App7020Avatar', array('userId' => $expertModel->idUser, 'size' => 'large'))
				?>

			</div>
			<div class="span10">
				<div class="row-fluid">
					<div class="span6">
						<div class="bigname bottombordergrey"><?php echo CoreUser::getForamattedNames($expertModel->idUser, " ", false) ?></div>
						<div class="clear"></div>
						<div class="block-info bottombordergrey">
							<div><?php echo Yii::t("standard", "Username") ?></div>
							<div><?php echo ltrim($userModel->userid, "/") ?></div>
						</div>
						<div class="block-info">
							<div><?php echo Yii::t("standard", "Email") ?></div>
							<div class="email"><?php echo $userModel->email ?></div>
						</div>
						<div class="clear"></div>
					</div>
					<div class="span5 expert-times">	
						<div class="span6">
							<div class="span12" style="text-align: center;">
								<i class="fa fa-user fa-2x"></i>
							</div>

							<div class="span12 bigdate" style="text-align: center; line-height: 25px;">
								<?php echo $userModel->getLoadedValue('register_date') ?>
							</div>
							<div class="span12" style="text-align: center;">
								<?php echo Yii::t("standard", "Expert appointment") ?>
							</div>
						</div>

						<div class="span6">
							<div class="span12" style="text-align: center;">
								<i class="fa fa-calendar-check-o fa-2x"></i>
							</div>
							<div class="span12 bigdate" style="text-align: center; line-height: 25px;">
								<?php echo $userModel->getLoadedValue('lastenter') ?>
							</div>
							<div class="span12" style="text-align: center;">
								<?php echo Yii::t("standard", "Last access date") ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div id="myActivity" ng-controller="expertSummary" data-userid="<?php echo $_GET['id']; ?>">
		<?php if (count($channels) > 0) { ?>
			<div class="expert-info">
				<div class="expert-title">
					<h1><?= Yii::t('app7020', 'Expert in the following channels'); ?> :</h1>
				</div>
				<div class="expert-count" ><i class="fa fa-users" aria-hidden="true"></i><span class="count"><?= $countExperts; ?></span><span class="count-text"><?= Yii::t('app7020', 'Total experts in these channels'); ?></span></div>
			</div>
			<div class="clear"></div>
			<div class="myChannelsList">
				<?php
				foreach ($channels as $channel) {
					$countExperts += count(App7020ChannelExperts::getChannelExpertIds($channel["id"]));
					?>
					<span><?php echo $channel["translation"]["name"]; ?></span>
				<?php } ?>
			</div>
		<?php } ?>
		<div class="angular-tabs row-fluid">
			<div class="tabs span3">
				<ul class="ang-tabs span2">
					<li ng-class="{active:isSet(1)}"><a ng-click="setTab(1)" href="javascript:void(0)"><?= Yii::t('app7020', 'Coaching Activity'); ?></a></li>
					<li ng-class="{active:isSet(2)}"><a ng-click="setTab(2)" href="javascript:void(0)"><?= Yii::t('app7020', 'Peer Review Activity'); ?></a></li>
				</ul>
			</div>
			<div class="tabs-content span9">
				<h2 ng-show="isSet(1)"><?= Yii::t('app7020', 'Coaching Activity'); ?></h2>
				<h2 ng-show="isSet(2)"><?= Yii::t('app7020', 'Peer review Activity'); ?></h2>
				<div class="filter">
					<label for="filter"><?php echo Yii::t('app7020', 'Timeframe'); ?></label>
					<select name="filter" id="filter" ng-model="timeframe" ng-options="tf.value for tf in timeframes track by tf.timeframe" ng-init="timeframe = timeframes[1]" ng-change = "newSelection()">
					</select>
				</div>
				<div ng-show="isSet(1)" id="coaching-tab" class="tab-content ng-cloak">
					<h3><?php echo Yii::t('app7020', 'Overview'); ?></h3>
					<ul class="data-boxes row-fluid">
						<li class="span4">
							<span class="num">{{coach_data.answeredQuestions}}</span>
							<span class="text"><?php echo Yii::t("app7020", "Answered questions"); ?></span>
						</li>
						<li class="span4">
							<span class="num">{{coach_data.avgAnswerTime.hours}} <strong>h</strong> {{coach_data.avgAnswerTime.minutes}} <strong>m</strong></span>
							<span class="text"><?php echo Yii::t("app7020", "Avg. answer time"); ?></span>
						</li>
						<li class="span4">
							<span class="num">{{coach_data.requestsIsatisfy}}</span>
							<span class="text"><?php echo Yii::t("app7020", "Satisfied requests"); ?></span>
						</li>
					</ul>
					<h3><?php echo Yii::t('app7020', 'Quality'); ?></h3>
					<ul class="data-boxes row-fluid">
						<li class="span4">
							<span class="num">{{coach_data.likes}}</span>
							<span class="text"><?php echo Yii::t("app7020", "Answers' likes"); ?></span>
						</li>
						<li class="span4">
							<span class="num">{{coach_data.dislikes}}</span>
							<span class="text"><?php echo Yii::t("app7020", "Answers' dislikes"); ?></span>
						</li>
						<li class="span4">
							<span class="num">{{coach_data.bestAnswers}}</span>
							<span class="text"><?php echo Yii::t('app7020', 'Answers marked as "best answer"'); ?></span>
						</li>
					</ul>
					<div ng-if="coachb.data.labels.length > 0 || coachd.data.length > 0" class="charts">
						<h3><?php echo Yii::t('app7020', 'Activity per channel'); ?></h3>
						<h-bar-chart chartid="barchart"></h-bar-chart>

						<div class="row-fluid">
							<div ng-if="coachb.data.labels.length > 0" class="span12">
								<h2><?php echo Yii::t('app7020', "Questions i've answered"); ?></h2>
								<div class="bar-chart">
									<chart id="coachb" value="coachb" type="Line" width="825" height="170"></chart>
									<div id="line-tooltip" class="chartjs-tooltip"></div>
								</div>
							</div>
						</div>
					</div>
					<ul class="table-top5">
						<li class="table-row .row-header row-fluid">
							<span class="cell cell-index span1"></span>
							<span class="cell cell-name span8"><h2><?php echo Yii::t('app7020', "Rank by answers quality"); ?></h2> <span class="text subtitle">Social score based on the quality of the given answers</span></span>
							<span class="cell cell-best-answer span1"><i class="fa fa-check-circle" data-placement="top" rel="tooltip" data-toggle="tooltip" data-original-title="Best answers"></i></span>
							<span class="cell cell-like span1"><i class="fa fa-thumbs-o-up"></i></span>
							<span class="cell cell-dislike span1"><i class="fa fa-thumbs-o-down"></i></span>
						</li>
						<li ng-repeat="(key, value) in coach_data.expertsByAnswersQuality| limitTo: 3" ng-show="value != null" class="table-row row-fluid {{(coach_data.idst == value.idst) ? 'current' : ''}}">
							<span class="cell cell-index span1">{{increment(key)}}</span>
							<span class="cell cell-name span8">
								<div class="avatar"><div class="app7020_signature" ng-bind-html="value.avatar" ></div ></div> 
								<span class='username'>{{(value.expert_name).length>1 ? value.expert_name : value.username}}</span>	
							</span>
							<span class="cell cell-best-answer span1">{{value.count_bestAnswers}}</span>
							<span class="cell cell-like span1">{{value.count_likes}}</span>
							<span class="cell cell-dislike span1">{{value.count_dislikes}}</span>
						</li>
					</ul>
					<ul class="table-top5">
						<li class="table-row .row-header row-fluid">
							<span class="cell cell-index span1"></span>
							<span class="cell cell-name span9"><h2><?php echo Yii::t('app7020', "Rank by first to answer rate"); ?></h2><span class="text subtitle">How many questions expert answered first</span></span>
							<div class="cell cell-question-count span2"><span class="subtitle-alone">Questions answered first</span></div>
						</li>
						<li ng-repeat="(key, value) in coach_data.expertsByFirstToAnswer| limitTo: 3" ng-show="value != null" class="table-row row-fluid {{(coach_data.idst == value.idst) ? 'current' : ''}}">
							<span class="cell cell-index span1">{{increment(key)}}</span>
							<span class="cell cell-name span8">
								<div class="avatar"><div class="app7020_signature" ng-bind-html="value.avatar" ></div ></div> 
								<span class='username'>{{(value.expert_name).length>1 ? value.expert_name : value.username}}</span>	
							</span>
							<span class="cell cell-question-count span2">{{value.count_answers}}</span>
						</li>
					</ul>
					<ul class="table-top5">
						<li class="table-row .row-header row-fluid">
							<span class="cell cell-index span1"></span>
							<span class="cell cell-name span9"><h2><?php echo Yii::t('app7020', "Rank by participation rate"); ?></h2> 
								<span class="text subtitle">How many questions expert answered (in total)</span>
							</span>
							<div class="cell cell-question-count span2"><span class="subtitle-alone">Answered questions</span></div>
						</li>
						<li ng-repeat="(key, value) in coach_data.expertsBypartecipationRate| limitTo: 3" ng-show="value != null" class="table-row row-fluid {{(coach_data.idst == value.idst) ? 'current' : ''}}">
							<span class="cell cell-index span1">{{increment(key)}}</span>
							<span class="cell cell-name span8">
								<div class="avatar"><div class="app7020_signature" ng-bind-html="value.avatar" ></div ></div> 
								<span class='username'>{{(value.expert_name).length>1 ? value.expert_name : value.username}}</span>	
							</span>
							<span class="cell cell-question-count span2">{{value.count_answers}}</span>
						</li>
					</ul>
				</div>
				<div ng-show="isSet(2)" id="peer-tab" class="tab-content ng-cloak">
					<h3><?php echo Yii::t('app7020', 'Overview'); ?></h3>
					<ul class="data-boxes row-fluid">
						<li class="span4">
							<span class="num">{{coach_data.reviewed_assets}}</span>
							<span class="text"><?php echo Yii::t('app7020', "Reviewed assets"); ?></span>
						</li>
						<li class="span4">
							<span class="num">{{coach_data.peer_reviews}}</span>
							<span class="text"><?php echo Yii::t('app7020', "Written peer reviews"); ?></span>
						</li>
						<li class="span4">
							<span class="num">{{coach_data.assetsIpublished}}</span>
							<span class="text"><?php echo Yii::t('app7020', "Assets i've published"); ?></span>
						</li>
					</ul>
					<div ng-if="peerb.data.labels.length > 0 || peerd.data.length > 0" class="charts">
						<h3><?php echo Yii::t('app7020', 'Activity per channel'); ?></h3>
						<h-bar-chart chartid="barchart2"></h-bar-chart>
						<div class="row-fluid">
							<div ng-if="peerb.data.labels.length > 0" class="span12">
								<h2><?php echo Yii::t('app7020', "Reviewed assets"); ?></h2>
								<div class="bar-chart">
									<chart id="peerb" value="peerb" type="Line" width="825" height="170"></chart>
									<div id="line-tooltip2" class="chartjs-tooltip"></div>
								</div>
							</div>
						</div>
					</div>
					<ul class="table-top5">
						<li class="table-row .row-header row-fluid">
							<span class="cell cell-index span1"></span>
							<span class="cell cell-name span9"><h2><?php echo Yii::t('app7020', "Rank by participation rate"); ?></h2>
								<span class="text subtitle">How many assets expert reviewed (in total)</span>
							</span>
							<div class="cell cell-question-count span2"><span class="subtitle-alone">Reviewed assets</span></div>
						</li>
						<li ng-repeat="(key, value) in coach_data.topExpertsByPartecipationRate| limitTo:3 track by $index" class="table-row row-fluid {{(coach_data.idst == value.idst) ? 'current' : ''}}">
							<span class="cell cell-index span1">{{increment(key)}}</span>
							<span class="cell cell-name span8">
								<span class="avatar" style="float:left;"><div class="app7020_signature" ng-bind-html="value.avatar" ></div ></span> 
								<span class='username'>{{(value.expert_name).length>1 ? value.expert_name : value.username}}</span>	
							</span>
							<span class="cell cell-question-count span2">{{value.count_reviews}}</span>
						</li>
					</ul>
					<ul class="table-top5">
						<li class="table-row .row-header row-fluid">
							<span class="cell cell-index span1"></span>
							<span class="cell cell-name span9"><h2><?php echo Yii::t('app7020', "Rank by first to review"); ?></h2>
								<span class="text subtitle">How many assets expert reviewed first</span>
							</span>
							<div class="cell cell-question-count span2"><span class="subtitle-alone">Assets reviewed first</span></div>
						</li>
						<li ng-repeat="(key, value) in coach_data.topExpertsByFirstToReview| limitTo:3 track by $index" class="table-row row-fluid {{(coach_data.idst == value.idst) ? 'current' : ''}}">
							<span class="cell cell-index span1">{{increment(key)}}</span>
							<span class="cell cell-name span8">
								<span class="avatar" style="float:left;"><div class="app7020_signature" ng-bind-html="value.avatar" ></div ></span> 
								<span class='username'>{{(value.expert_name).length>1 ? value.expert_name : value.username}}</span>	
							</span>
							<span class="cell cell-question-count span2">{{value.count_reviews}}</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>