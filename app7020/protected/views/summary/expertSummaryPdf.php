<div id="expertSummaryPdfFile">
	<?php
	$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
	$standartAvatar = Yii::app()->theme->baseUrl . '/images/standard/user.png';
	$avatarImg = $storage->absoluteBaseUrl . '/avatar/' . $renderData['userModel']->avatar;
	?>

	<table>
		<tbody>
			<tr>
				<td rowspan="3" class="centerText">
					<?php if ($renderData['userModel']->avatar): ?>
						<img id="expertAvatarImg" src="<?php echo $avatarImg; ?>">
					<?php else: ?>
						<div id="expertAvatarNoImg">
							<?php
							if ($renderData['userModel']->firstname && $renderData['userModel']->lastname) {
								echo mb_substr($renderData['userModel']->firstname, 0, 1) . mb_substr($renderData['userModel']->lastname, 0, 1);
							}
							?>

						</div>
					<?php endif; ?>
				</td>
				<td class="fullName">
					<?php echo $renderData['userModel']->firstname . ' ' . $renderData['userModel']->lastname; ?>
				</td>
				<td rowspan="3" class="centerText">
					<?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/report/subscription-date.png'); ?>
					<div class="fullName">
						<?php echo $renderData['userModel']->getLoadedValue('register_date') ?>
					</div>
					<?php echo Yii::t("standard", "Expert appointment") ?>
				</td>
				<td rowspan="3" class="centerText">
					<?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/report/last-access-date.png'); ?>
					<div class="fullName">
						<?php echo $renderData['userModel']->getLoadedValue('lastenter') ?>
					</div>
					<div>
						<?php echo Yii::t("standard", "Last access date") ?>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<?php echo Yii::t('app7020', 'Username') . ': ' . ltrim($renderData['userModel']->userid, '/'); ?>
				</td>
			</tr>
			<tr>
				<td>
					<?php echo Yii::t('app7020', 'Email') . ': ' . $renderData['userModel']->email; ?>
				</td>
			</tr>
		</tbody>
	</table>

	<!-- TAB Coaching Activity -->
	<h2><?php echo Yii::t('app7020', 'Coaching Activity'); ?></h2>
	<h3><?php echo Yii::t('app7020', 'Timeframe') . ': ' . $renderData['timeframeStr']; ?></h3>
	<h4><?php echo Yii::t('app7020', 'Overview'); ?></h4>
	<table>
		<tr>
			<td class="boxes centerText">
				<div class="fullName">
					<?php echo $renderData['answeredQuestions']; ?>
				</div>
				<div>
					<?php echo Yii::t('app7020', 'Answered questions'); ?>
				</div>
			</td>
			<td class="boxes centerText">
				<div>
					<span class="fullName"><?php echo $renderData['avgAnswerTime']['hours']; ?></span><strong>h</strong><span class="fullName"> <?php echo $renderData['avgAnswerTime']['minutes']; ?></span><strong>m</strong>
				</div>
				<div>
					<?php echo Yii::t("app7020", "Avg. answer time"); ?>
				</div>
			</td>
			<td class="boxes centerText">
				<div class="fullName">
					<?php echo $renderData['requestsIsatisfy']; ?>
				</div>
				<div>
					<?php echo Yii::t("app7020", "Satisfied requests"); ?>
				</div>
			</td>
		</tr>
	</table>
	<h4><?php echo Yii::t('app7020', 'Quality'); ?></h4>
	<table>
		<tr>
			<td class="boxes centerText">
				<div class="fullName">
					<?php echo $renderData['likes']; ?>
				</div>
				<div>
					<?php echo Yii::t('app7020', "Answers' likes"); ?>
				</div>
			</td>
			<td class="boxes centerText">
				<div class="fullName">
					<?php echo $renderData['dislikes']; ?>
				</div>
				<div>
					<?php echo Yii::t('app7020', "Answers' dislikes"); ?>
				</div>
			</td>
			<td class="boxes centerText">
				<div class="fullName">
					<?php echo $renderData['bestAnswers']; ?>
				</div>
				<div>
					<?php echo Yii::t("app7020", 'Answers marked as "best answer"'); ?>
				</div>
			</td>
		</tr>
	</table>
	<!-- Chart Activity per channel -->
	<h4><?php echo Yii::t('app7020', 'Activity per channel'); ?></h4>
	<div class="chartContainer">
		<?php foreach ($renderData['chartMyActivityPerChannel'] as $myActivityPerChannel): ?>
			<div class="chartRow">
				<div>
					<?php echo $myActivityPerChannel['label'] . ': ' . $myActivityPerChannel['value'] . '%'; ?>
				</div>
				<div style="width: <?php echo $myActivityPerChannel['value']; ?>%; height: 12px; background-color: <?php echo $myActivityPerChannel['color']; ?>;"></div>
			</div>
		<?php endforeach; ?>
	</div>
	<!-- Chart Questions i've answered-->
	<h4><?php echo Yii::t('app7020', "Questions I've answered"); ?></h4>
	<img src="<?php echo $renderData['allChartsImgs']['chartQuestions']; ?>">
	<!-- Rank by answers quality -->
	<table class="tableRanckings">
		<tr>
			<td></td>
			<td  colspan="2">
				<h4>
					<?php echo Yii::t('app7020', "Rank by answers quality"); ?>
				</h4>	
				<div class="subTitle">
					<?php echo Yii::t('app7020', "Social score based on the quality of the given answers"); ?>
				</div>
			</td>
			<td>
				<h4>
					<?php echo Yii::t('app7020', "Best answers"); ?>
				</h4>	
			</td>
			<td>
				<h4>
					<?php echo Yii::t('app7020', "Likes"); ?>
				</h4>
			</td>
			<td>
				<h4>
					<?php echo Yii::t('app7020', "Dislikes"); ?>
				</h4>
			</td>
		</tr>
		<?php
		$counterExpertsByAnswersQuality = 0;
		?>
		<?php foreach ($renderData['expertsByAnswersQuality'] as $expertsByAnswersQuality): ?>
			<?php
			$counterExpertsByAnswersQuality++;
			$currentUserClass = $expertsByAnswersQuality['idst'] == $renderData['userModel']->idst ? 'currentUser' : '';
			?>
			<tr>
				<td class="<?php echo $currentUserClass; ?>">
					<?php echo $counterExpertsByAnswersQuality; ?>
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<img class="ratingsAvatarImg" src="<?php echo $expertsByAnswersQuality['avatar']; ?>">
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<?php
					if ($expertsByAnswersQuality['expert_name']) {
						echo $expertsByAnswersQuality['expert_name'];
					} else {
						echo $expertsByAnswersQuality['username'];
					}
					?>
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<?php echo $expertsByAnswersQuality['count_bestAnswers']; ?>
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<?php echo $expertsByAnswersQuality['count_likes']; ?>
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<?php echo $expertsByAnswersQuality['count_dislikes']; ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>

	<!-- Rank by first to answer rate -->
	<table class="tableRanckings">
		<tr>
			<td></td>
			<td  colspan="2">
				<h4>
					<?php echo Yii::t('app7020', "Rank by first to answer rate"); ?>
				</h4>	
				<div class="subTitle">
					<?php echo Yii::t('app7020', "How many questions expert answered first"); ?>
				</div>
			</td>
			<td>
				<h4>
					<?php echo Yii::t('app7020', "Questions answered first"); ?>
				</h4>	
			</td>
		</tr>
		<?php
		$counterExpertsByFirstToAnswer = 0;
		?>
		<?php foreach ($renderData['expertsByFirstToAnswer'] as $expertsByFirstToAnswer): ?>
			<?php
			$counterExpertsByFirstToAnswer++;
			$currentUserClass = $expertsByFirstToAnswer['idst'] == $renderData['userModel']->idst ? 'currentUser' : '';
			?>
			<tr class="currentUserRow">
				<td class="<?php echo $currentUserClass; ?>">
					<?php echo $counterExpertsByFirstToAnswer; ?>
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<img class="ratingsAvatarImg" src="<?php echo $expertsByFirstToAnswer['avatar']; ?>">
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<?php
					if ($expertsByFirstToAnswer['expert_name']) {
						echo $expertsByFirstToAnswer['expert_name'];
					} else {
						echo $expertsByFirstToAnswer['username'];
					}
					?>
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<?php echo $expertsByFirstToAnswer['count_answers']; ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>


	<!-- Rank by participation rate -->
	<table class="tableRanckings">
		<tr>
			<td></td>
			<td  colspan="2">
				<h4>
					<?php echo Yii::t('app7020', "Rank by participation rate"); ?>
				</h4>	
				<div class="subTitle">
					<?php echo Yii::t('app7020', "How many questions expert answered (in total)"); ?>
				</div>
			</td>
			<td>
				<h4>
					<?php echo Yii::t('app7020', "Answered questions"); ?>
				</h4>	
			</td>
		</tr>
		<?php
		$counterExpertsByPartecipationRate = 0;
		?>
		<?php foreach ($renderData['expertsBypartecipationRate'] as $expertsByPartecipationRate): ?>
			<?php
			$counterExpertsByPartecipationRate++;
			$currentUserClass = $expertsByPartecipationRate['idst'] == $renderData['userModel']->idst ? 'currentUser' : '';
			?>
			<tr class="currentUserRow">
				<td class="<?php echo $currentUserClass; ?>">
					<?php echo $counterExpertsByPartecipationRate; ?>
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<img class="ratingsAvatarImg" src="<?php echo $expertsByPartecipationRate['avatar']; ?>">
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<?php
					if ($expertsByPartecipationRate['expert_name']) {
						echo $expertsByPartecipationRate['expert_name'];
					} else {
						echo $expertsByPartecipationRate['username'];
					}
					?>
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<?php echo $expertsByPartecipationRate['count_answers']; ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>

	<!-- TAB Peer review Activity -->
	<h2><?php echo Yii::t('app7020', 'Peer Review Activity') ?></h2>
	<h3><?php echo Yii::t('app7020', 'Timeframe') . ': ' . $renderData['timeframeStr']; ?></h3>
	<h4><?php echo Yii::t('app7020', 'Overview'); ?></h4>
	<table>
		<tr>
			<td class="boxes centerText">
				<div class="fullName">
					<?php echo $renderData['reviewed_assets']; ?>
				</div>
				<div>
					<?php echo Yii::t('app7020', 'Reviewed assets'); ?>
				</div>
			</td>
			<td class="boxes centerText">
				<div class="fullName">
					<?php echo $renderData['peer_reviews']; ?>
				</div>
				<div>
					<?php echo Yii::t("app7020", "Written peer reviews"); ?>
				</div>
			</td>
			<td class="boxes centerText">
				<div class="fullName">
					<?php echo $renderData['assetsIpublished']; ?>
				</div>
				<div>
					<?php echo Yii::t("app7020", "Assets i've published"); ?>
				</div>
			</td>
		</tr>
	</table>
	<h4><?php echo Yii::t('app7020', 'Activity per channel'); ?></h4>
	<div class="chartContainer">
		<?php foreach ($renderData['chartMyPeerActivityPerChannel'] as $chartReviewedAssets): ?>
			<div class="chartRow">
				<div>
					<?php echo $chartReviewedAssets['label'] . ': ' . $chartReviewedAssets['value'] . '%'; ?>
				</div>
				<div style="width: <?php echo $chartReviewedAssets['value']; ?>%; height: 12px; background-color: <?php echo $chartReviewedAssets['color']; ?>;"></div>
			</div>
		<?php endforeach; ?>
	</div>
	<!-- Chart Reviewed assets -->
	<h4><?php echo Yii::t('app7020', "Reviewed assets"); ?></h4>
	<img src="<?php echo $renderData['allChartsImgs']['chartAssets']; ?>">
	
<!-- Rank by partecipation rate (Peer review Activity) -->
	<table class="tableRanckings">
		<tr>
			<td></td>
			<td  colspan="2">
				<h4>
					<?php echo Yii::t('app7020', "Rank by participation rate"); ?>
				</h4>	
				<div class="subTitle">
					<?php echo Yii::t('app7020', "How many assets expert reviewed (in total)"); ?>
				</div>
			</td>
			<td>
				<h4>
					<?php echo Yii::t('app7020', "Reviewed assets"); ?>
				</h4>	
			</td>
		</tr>
		<?php
		$counterTopExpertsByPartecipationRate = 0;
		?>
		<?php foreach ($renderData['topExpertsByPartecipationRate'] as $topExpertsByPartecipationRate): ?>
			<?php
			$counterTopExpertsByPartecipationRate++;
			$currentUserClass = $topExpertsByPartecipationRate['idst'] == $renderData['userModel']->idst ? 'currentUser' : '';
			?>
			<tr class="currentUserRow">
				<td class="<?php echo $currentUserClass; ?>">
					<?php echo $counterTopExpertsByPartecipationRate; ?>
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<img class="ratingsAvatarImg" src="<?php echo $topExpertsByPartecipationRate['avatar']; ?>">
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<?php
					if ($topExpertsByPartecipationRate['expert_name']) {
						echo $topExpertsByPartecipationRate['expert_name'];
					} else {
						echo $topExpertsByPartecipationRate['username'];
					}
					?>
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<?php echo $topExpertsByPartecipationRate['count_reviews']; ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>

<!-- Rank by first to review (Peer review Activity) -->
	<table class="tableRanckings">
		<tr>
			<td></td>
			<td  colspan="2">
				<h4>
					<?php echo Yii::t('app7020', "Rank by first to review"); ?>
				</h4>	
				<div class="subTitle">
					<?php echo Yii::t('app7020', "How many assets expert reviewed first"); ?>
				</div>
			</td>
			<td>
				<h4>
					<?php echo Yii::t('app7020', "Assets reviewed first"); ?>
				</h4>	
			</td>
		</tr>
		<?php
		$counterTopExpertsByFirstToReview = 0;
		?>
		<?php foreach ($renderData['topExpertsByFirstToReview'] as $topExpertsByFirstToReview): ?>
			<?php
			$counterTopExpertsByFirstToReview++;
			$currentUserClass = $topExpertsByFirstToReview['idst'] == $renderData['userModel']->idst ? 'currentUser' : '';
			?>
			<tr class="currentUserRow">
				<td class="<?php echo $currentUserClass; ?>">
					<?php echo $counterTopExpertsByFirstToReview; ?>
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<img class="ratingsAvatarImg" src="<?php echo $topExpertsByFirstToReview['avatar']; ?>">
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<?php
					if ($topExpertsByFirstToReview['expert_name']) {
						echo $topExpertsByFirstToReview['expert_name'];
					} else {
						echo $topExpertsByFirstToReview['username'];
					}
					?>
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<?php echo $topExpertsByFirstToReview['count_reviews']; ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
	
</div>


