<?php
$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
$avatarImg = $storage->absoluteBaseUrl . '/avatar/';
?>

<div id="userReportQuestionsAndAnswersPdf">
	<h3><?php echo Yii::t('app7020', 'Timeframe') . ': ' . $renderData['timeframeStr']; ?></h3>

	<!--Info Boxes -->
	<h2><?php echo Yii::t('app7020', 'Overview'); ?></h2>
	<div class="row-fluid">
		<div class="data-box span4 left">
			<span class="num"><?php echo $renderData['publishedAssets']['count']; ?></span><br>
			<span><?php echo Yii::t('app7020', 'Published assets'); ?></span>
		</div>
		<div class="data-box span4">
			<span class="num"><?php echo $renderData['totalContributors']; ?></span><br>
			<span><?php echo Yii::t('app7020', 'Total contributors'); ?></span>
		</div>
	</div>
	<div class="row-fluid">
		<h4><?php echo Yii::t('app7020', 'Quality'); ?></h4>
		<div class="clear"></div>
		<div class="data-box span4">
			<span class="num"><?php echo $renderData['totalAskers']; ?></span><br>
			<span><?php echo Yii::t('app7020', "Askers involved"); ?></span>
		</div>
		<div class="left data-box span4">
			<span class="num"><?php echo $renderData['publishedAssets']['averageRating']; ?></span><br>
			<span><?php echo Yii::t('app7020', "Average Asset Rating"); ?></span>
		</div>
		<div class="left data-box span4">
			<span class="num"><?php echo $renderData['sharedAssetsVsTotalChannels']['channelPerc']; ?>%</span><br>
			<span><?php echo Yii::t('app7020', 'Shared assets vs total channels'); ?></span>
		</div>
	</div>
	<h4><?php echo Yii::t('app7020', 'Submitted vs Published Assets'); ?></h4>
	<div class="app7020ActivityChannels">
		<div class="grid-wrapper">
			<table class="submittedVsPublished">
				<tbody>
					<tr class="">
						<td class="title"><?= $renderData["submittedVsPublished"]["submittedAssets"]["label"]; ?></td>
						<td class="line-pre-wrapper">
							<div class="line-wrapper" style="width: <?= $renderData["submittedVsPublished"]["submittedAssets"]["percentage"] ?>%; background: #4EA0DF;">
								<span class="line-text"><?= $renderData["submittedVsPublished"]["submittedAssets"]["count"] ?></span>
							</div>
						</td>
					</tr>
					<tr class="">
						<td class="title"><?= $renderData["submittedVsPublished"]["publishedAssets"]["label"]; ?></td>
						<td class="line-pre-wrapper">
							<div class="line-wrapper" style="width: <?= $renderData["submittedVsPublished"]["publishedAssets"]["percentage"] ?>%; background: #51D78B;">
								<span class="line-text"><?= $renderData["submittedVsPublished"]["publishedAssets"]["count"] ?></span>
							</div>
						</td>
					</tr>
					<tr class="">
						<td class="title"><?= $renderData["submittedVsPublished"]["unsubmittedAssets"]["label"]; ?></td>
						<td class="line-pre-wrapper">
							<div class="line-wrapper" style="width: <?= $renderData["submittedVsPublished"]["unsubmittedAssets"]["percentage"] ?>%; background: #CCCCCC;">
								<span class="line-text"><?= $renderData["submittedVsPublished"]["unsubmittedAssets"]["count"] ?></span>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="separators">
				<div><span></span></div>
				<div><span></span></div>
				<div><span></span></div>
				<div><span></span></div>
				<div><span></span></div>
			</div>
		</div>
	</div>
	<h4><?php echo Yii::t('app7020', 'Latest 3 uploaded assets'); ?></h4>
	<div class="last-assets">
		<?php
		$i = 0;
		foreach ($renderData["lastAssets"] as $asset) {
			?>
			<div class="<?php
			if ($i == 0) {
				echo 'left';
			}
			?> span4 last-asset-block">
				<a href="<?= Docebo::createAbsoluteUrl('site/index/', array('#' => '/assets/view/' . $asset['id'])); ?>"><img src="<?= App7020Assets::getAssetThumbnailUri($asset["id"]); ?>"></a>
				<div class="asset-info">
					<span class="last3-views"><?= $asset["contentViews"]; ?> <?= Yii::t('app7020', 'Views'); ?></span>
					<span><?= App7020Helpers::timeAgo($asset["created"]); ?> <?= Yii::t('app7020', 'ago'); ?></span>
					<span class="last3-rating"><i class="fa fa-star-half-o" aria-hidden="true"></i><?= round($asset["contentRating"], 1); ?></span>
					<div class="asset-title"><?= substr($asset["title"], 0, 40) . '...'; ?></div>
					<div class="asset-contributor"><?= Yii::t('app7020', 'by'); ?> <span class="contributor-name"><?= CoreUser::getFullNameById($asset["userId"]); ?></div>
				</div>
			</div>
			<?php
			$i++;
		}
		?>
	</div>
	<h4><?php echo Yii::t('app7020', 'Channel assets published contributions'); ?></h4>
	<img src="<?php echo $renderData["allChartsImgs"]["chartAssets"]; ?>">
	<h4><?php echo Yii::t('app7020', 'Channel assets views'); ?></h4>
	<img src="<?php echo $renderData["allChartsImgs"]["chartAssetViews"]; ?>">
	<h2><?php echo Yii::t('app7020', 'Questions and Answers'); ?></h2>
	<div class="row-fluid">
		<div class="data-box span4 left">
			<span class="num"><?php echo $renderData['questions']['totalQuestions']; ?></span><br>
			<span><?php echo Yii::t('app7020', 'Questions'); ?></span>
		</div>
		<div class="data-box span4">
			<span class="num"><?php echo $renderData['answers']['totalAnswers']; ?></span><br>
			<span><?php echo Yii::t('app7020', 'Answers'); ?></span>
		</div>
		<div class="data-box span4">
			<span class="num"><?php echo $renderData['bestAnswers']['bestAnswers']; ?></span><br>
			<span><?php echo Yii::t('app7020', 'Best answers'); ?></span>
		</div>
	</div>
	<div class="row-fluid">
		<div class="clear"></div>
		<div class="data-box span5">
			<span class="num"><?php echo $renderData['reviews']['reviewPercentage']; ?>%</span><br>
			<span><?php echo Yii::t('app7020', "Peer reviews compared to total channels"); ?></span>
		</div>
		<div class="left data-box span5">
			<span class="num"><?php echo $renderData['qaPercentage']; ?>%</span><br>
			<span><?php echo Yii::t('app7020', "Q&A compared to total channels"); ?></span>
		</div>
	</div>
	<h4><?php echo Yii::t('app7020', 'Questions vs Answers'); ?></h4>
	<img src="<?php echo $renderData["allChartsImgs"]["chartQa"]; ?>">