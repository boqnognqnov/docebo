<h3><?= Yii::t('app7020', 'Overview'); ?></h3>
<div class="timeframe-selector">
	<label for="timeframe-selector"><?= Yii::t('app7020', 'Timeframe'); ?></label>
	<?php
	echo CHtml::dropDownList('channelOverviewTimeframe', '12', $timeframes, array(
		'options' => array('30' => array('selected' => true)),
	));
	?>
</div>
<div class="clear"></div>
<div class="left span4 data-box">
	<span id="publishedAssets" class="num "><?= $statsOverview["publishedAssets"]["count"]; ?></span>
	<span class="text"><?= Yii::t('app7020', 'Published assets'); ?></span>
</div>
<div class=" span4 data-box">
	<span id="totalContributors" class="num "><?= $statsOverview["totalContributors"]; ?></span>
	<span class="text"><?= Yii::t('app7020', 'Total contributors'); ?></span>
</div>
<div class="span4 data-box">
	<span id="totalAskers" class="num "><?= $statsOverview["totalAskers"]; ?></span>
	<span class="text"><?= Yii::t('app7020', 'Askers involved'); ?></span>
</div>

<div class="left span4 data-box">
	<span id="averageRating" class="num rateNum"><?= $statsOverview["publishedAssets"]["averageRating"]; ?></span>
	<div class="rating-directive">
		<?php
		$this->widget('common.widgets.ContentRating', array('contentId' => 99999, 'readOnly' => true, 'avgRating' => $statsOverview["publishedAssets"]["averageRating"]));
		?>
	</div>
	<div class="clear"></div>
	<span class="text"><?= Yii::t('app7020', 'Average asset rating'); ?></span>
</div>
<div class=" span4 data-box">
	<span id="currentVsAll" class="num "><?= $statsOverview["sharedAssetsVsTotalChannels"]["channelPerc"]; ?>%</span>
	<span class="text"><?= Yii::t('app7020', 'Shared assets vs total channels'); ?></span>
</div>
<div class="clear"></div>

<h3><?= Yii::t('app7020', 'Submitted vs Published Assets'); ?></h3>

<div class="app7020ActivityChannels">
	<div class="grid-wrapper">
		<table class="submittedVsPublished">
			<tbody>
				<tr class="">
					<td class="title"><?= $statsOverview["submittedVsPublished"]["submittedAssets"]["label"]; ?></td>
					<td class="line-pre-wrapper">
						<div class="line-wrapper" style="width: <?= $statsOverview["submittedVsPublished"]["submittedAssets"]["percentage"] ?>%; background: #4EA0DF;">
							<span class="line-text"><?= $statsOverview["submittedVsPublished"]["submittedAssets"]["count"] ?></span>
						</div>
					</td>
				</tr>
				<tr class="">
					<td class="title"><?= $statsOverview["submittedVsPublished"]["publishedAssets"]["label"]; ?></td>
					<td class="line-pre-wrapper">
						<div class="line-wrapper" style="width: <?= $statsOverview["submittedVsPublished"]["publishedAssets"]["percentage"] ?>%; background: #51D78B;">
							<span class="line-text"><?= $statsOverview["submittedVsPublished"]["publishedAssets"]["count"] ?></span>
						</div>
					</td>
				</tr>
				<tr class="">
					<td class="title"><?= $statsOverview["submittedVsPublished"]["unsubmittedAssets"]["label"]; ?></td>
					<td class="line-pre-wrapper">
						<div class="line-wrapper" style="width: <?= $statsOverview["submittedVsPublished"]["unsubmittedAssets"]["percentage"] ?>%; background: #CCCCCC;">
							<span class="line-text"><?= $statsOverview["submittedVsPublished"]["unsubmittedAssets"]["count"] ?></span>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		<div class="separators">
			<div><span></span></div>
			<div><span></span></div>
			<div><span></span></div>
			<div><span></span></div>
			<div><span></span></div>
		</div>
	</div>
</div>
<div class="clear"></div>
<h3><?= Yii::t('app7020', 'Latest 3 uploaded Assets'); ?></h3>
<div class="last-assets">
	<?php
	$i = 0;
	foreach ($statsOverview["lastAssets"] as $asset) {
		?>
		<div class="<?php
		if ($i == 0) {
			echo 'left';
		}
		?> span4 last-asset-block">
			<a href="<?= Docebo::createAbsoluteUrl('site/index/', array('#' => '/assets/view/' . $asset['id'])); ?>"><img src="<?= App7020Assets::getAssetThumbnailUri($asset["id"]); ?>"></a>
			<div class="asset-info">
				<span class="last3-views"><?= $asset["contentViews"]; ?> <?= Yii::t('app7020', 'Views'); ?></span>
				<span><?= App7020Helpers::timeAgo($asset["created"]); ?> <?= Yii::t('app7020', 'ago'); ?></span>
				<span class="last3-rating"><i class="fa fa-star-half-o" aria-hidden="true"></i><?= round($asset["contentRating"], 1); ?></span>
				<div class="asset-title"><?= substr($asset["title"], 0, 40) . '...'; ?></div>
				<div class="asset-contributor"><?= Yii::t('app7020', 'by'); ?> <span class="contributor-name"><?= CoreUser::getFullNameById($asset["userId"]); ?></div>
			</div>
		</div>
		<?php
		$i++;
	}
	?>
</div>
<div class="clear"></div>
<h3><?= Yii::t('app7020', 'Channel assets published contributions'); ?></h3>
<canvas id="channelAssets" width="816" height="200"></canvas>
<h3><?= Yii::t('app7020', 'Channel assets views'); ?></h3>
<canvas id="channelAssetsViews" width="816" height="200"></canvas>