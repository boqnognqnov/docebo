<h3><?= Yii::t('app7020', 'Questions and Answers'); ?></h3>
<div class="timeframe-selector">
	<label for="timeframe-selector"><?= Yii::t('app7020', 'Timeframe'); ?></label>
	<?php echo CHtml::dropDownList('channelQaTimeFrame', '12', $timeframes,array(
                'options' => array('30'=>array('selected'=>true)),
    )); ?>
</div>
<div class="clear"></div>
<div class="left span4 data-box">
	<span id="totalQuestions" class="num "><?= $statsQa["questions"]["totalQuestions"]; ?></span>
	<span class="text"><?= Yii::t('app7020', 'Questions'); ?></span>
</div>
<div class=" span4 data-box">
	<span id="totalAnswers" class="num "><?= $statsQa["answers"]["totalAnswers"]; ?></span>
	<span class="text"><?= Yii::t('app7020', 'Answers'); ?></span>
</div>
<div class=" span4 data-box">
	<span id="bestAnswers" class="num "><?= $statsQa["bestAnswers"]["bestAnswers"]; ?></span>
	<span class="text"><?= Yii::t('app7020', 'Best answers'); ?></span>
</div>
<div class="left span6 data-box">
	<span id="reviewPercentage" class="num "><?= $statsQa["reviews"]["reviewPercentage"]; ?>%</span>
	<span class="text"><?= Yii::t('app7020', 'Peer reviews compared to total channels'); ?></span>
</div>
<div class="span6 data-box">
	<span id="qaPercentage" class="num "><?= $statsQa["qaPercentage"]; ?>%</span>
	<span class="text"><?= Yii::t('app7020', 'Q&A compared to total channels'); ?></span>
</div>
<div class="legend">
	<div class="legend-question">
		<span class="square-green"></span><label>Questions</label>
	</div>
	<div class="legend-answer">
		<span class="square-blue"></span><label>Answers</label>
	</div>
</div>
<h3><?= Yii::t('app7020', 'Questions vs Answers'); ?></h3>

<canvas id="channelQuestionsAnswers" width="820" height="200"></canvas>