<div data-channel-id="<?= $channelData["attributes"]->id; ?>" id="App7020ChannelSummaryReport">

	<div class="generate-report">
		<div class="report-form">
			<input id="channel-autocomplete" placeholder="Type channel name here" type="text">
			<a class="new-channel-summary" id="generateChannelSummaryReport" href="<?php echo Docebo::createApp7020Url('channelSummary/index'); ?>	"><?= Yii::t('app7020', 'Generate'); ?></a>
		</div>
		<div class="print-actions">
			<div class="printChannel"><span></span>Print</div>
			<div class="downloadPdf"><span></span>Download pdf</div>
		</div>
	</div>
	<div id="App7020ChannelSummaryReportContent">
		<div class="docebo-col-md-12 channel-summary-info">
			<div style="background:<?= $channelData["attributes"]->icon_bgr; ?>;color:<?= $channelData["attributes"]->icon_color; ?>" class="channel-icon"><i class="fa <?= $channelData["attributes"]->icon_fa ?>" aria-hidden="true"></i></div>
			<?php 
			$this->breadcrumbs[] = Yii::t('report', 'Channel summary').': '.$channelData["title"];
			?>
			<h1><?= $channelData["title"]; ?></h1>
			<div class="docebo-col-md-12 channel-description"><p><?= $channelData["description"]; ?></p></div>
		</div>
	</div>
	<div id="sidebar" class="docebo-col-md-3 advanced-sidebar">
		<ul>
			<li class="active"><a data-toggle="tab" href="#overview"><?= Yii::t('app7020', 'Overview'); ?></a></li>
			<li><a data-toggle="tab" href="#qa"><?= Yii::t('app7020', 'Questions and Answers'); ?></a></li>
			<li><a data-toggle="tab" href="#expert"><?= Yii::t('app7020', 'Experts'); ?></a></li>
		</ul>
	</div>
	<div class="tab-content  docebo-col-md-9">
		<div id="overview" class="row-fluid tab-pane active">
			<?php
			$timeframes = array('7' => Yii::t('app7020', 'Weekly'), '30' => Yii::t('app7020', 'Monthly'), '365' => Yii::t('app7020', 'Yearly'));
			$this->renderPartial('_overview', array(
				'timeframes' => $timeframes,
				'lastAssets' => $lastAssets,
				'statsOverview' => $statsOverview
				));
			?>
		</div>
		<div id="qa" class="row-fluid tab-pane fade">
			<?php
			$this->renderPartial('_questionsAndAnswers', array(
				'statsQa' => $statsQa, 'timeframes' => $timeframes));
			?>
		</div>
		<div id="expert" class="row-fluid tab-pane fade">
			<?php
			$this->renderPartial('_experts', array(
				'statsExperts' => $statsExperts));
			?>
		</div>
	</div>
</div>